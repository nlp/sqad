<s>
Rusko-turecká	rusko-turecký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
1877	#num#	k4
<g/>
–	–	k?
<g/>
1878	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rusko-turecká	rusko-turecký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
1877	#num#	k4
<g/>
–	–	k?
<g/>
1878	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Velká	velká	k1gFnSc1
východní	východní	k2eAgFnSc2d1
krize	krize	k1gFnSc2
</s>
<s>
Bitvy	bitva	k1gFnPc1
v	v	k7c6
průsmyku	průsmyk	k1gInSc6
Šipka	šipka	k1gFnSc1
<g/>
,	,	kIx,
srpen	srpen	k1gInSc1
1877	#num#	k4
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1877	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1878	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Balkán	Balkán	k1gInSc1
<g/>
,	,	kIx,
Kavkaz	Kavkaz	k1gInSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
</s>
<s>
Sanstefanská	Sanstefanský	k2eAgFnSc1d1
mírová	mírový	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
</s>
<s>
Berlínská	berlínský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
</s>
<s>
změny	změna	k1gFnPc1
území	území	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
*	*	kIx~
Znovuustanovení	Znovuustanovení	k1gNnSc6
Bulharského	bulharský	k2eAgInSc2d1
státu	stát	k1gInSc2
</s>
<s>
De	De	k?
jure	jurat	k5eAaPmIp3nS
nezávislost	nezávislost	k1gFnSc4
Rumunska	Rumunsko	k1gNnSc2
<g/>
,	,	kIx,
Srbska	Srbsko	k1gNnSc2
a	a	k8xC
Černé	Černé	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
na	na	k7c6
Osmanské	osmanský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
</s>
<s>
Karská	karský	k2eAgFnSc1d1
a	a	k8xC
Batumská	batumský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
součástí	součást	k1gFnSc7
Ruska	Ruska	k1gFnSc1
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Finské	finský	k2eAgNnSc1d1
velkoknížectví	velkoknížectví	k1gNnSc1
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
Bulharsko	Bulharsko	k1gNnSc1
Srbsko	Srbsko	k1gNnSc1
Černohorské	Černohorská	k1gFnSc2
knížectví	knížectví	k1gNnSc2
gruzínské	gruzínský	k2eAgFnSc2d1
milice	milice	k1gFnSc2
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Egypt	Egypt	k1gInSc1
</s>
<s>
Osmanské	osmanský	k2eAgNnSc1d1
Bulharsko	Bulharsko	k1gNnSc1
Osmanské	osmanský	k2eAgNnSc1d1
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
čečenští	čečenský	k2eAgMnPc1d1
a	a	k8xC
dagestánští	dagestánský	k2eAgMnPc1d1
povstalci	povstalec	k1gMnPc1
</s>
<s>
abcházští	abcházský	k2eAgMnPc1d1
povstalci	povstalec	k1gMnPc1
</s>
<s>
polské	polský	k2eAgFnPc4d1
legie	legie	k1gFnPc4
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Gorčakov	Gorčakov	k1gInSc1
Alexandr	Alexandr	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikolajevič	Nikolajevič	k1gMnSc1
Petko	Petko	k1gNnSc1
Karavelov	Karavelovo	k1gNnPc2
N.	N.	kA
P.	P.	kA
Ignatěv	Ignatěv	k1gMnSc1
Nikolaj	Nikolaj	k1gMnSc1
Nikolajevič	Nikolajevič	k1gMnSc1
Michail	Michail	k1gMnSc1
Nikolajevič	Nikolajevič	k1gMnSc1
M.	M.	kA
I.	I.	kA
Dragomir	Dragomira	k1gFnPc2
J.	J.	kA
V.	V.	kA
Gurk	Gurk	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alexandr	Alexandr	k1gMnSc1
I.	I.	kA
Bulharský	bulharský	k2eAgMnSc1d1
Vasil	Vasil	k1gMnSc1
Levski	Levsk	k1gFnSc2
Nikola	Nikola	k1gMnSc1
I.	I.	kA
Milan	Milan	k1gMnSc1
I.	I.	kA
</s>
<s>
Saffet	Saffet	k1gMnSc1
paša	paša	k1gMnSc1
Midhat	Midhat	k1gMnSc1
paša	paša	k1gMnSc1
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
*	*	kIx~
<g/>
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
<g/>
:	:	kIx,
185	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
dunajská	dunajský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
75	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
kavkazská	kavkazský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Finsko	Finsko	k1gNnSc1
<g/>
:	:	kIx,
1,000	1,000	k4
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
<g/>
:	:	kIx,
66	#num#	k4
000	#num#	k4
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
<g/>
:	:	kIx,
20	#num#	k4
000	#num#	k4
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
:	:	kIx,
45	#num#	k4
000	#num#	k4
</s>
<s>
190	#num#	k4
děl	dělo	k1gNnPc2
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
<g/>
:	:	kIx,
81	#num#	k4
500	#num#	k4
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
:	:	kIx,
281	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
*	*	kIx~
<g/>
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
</s>
<s>
15	#num#	k4
567	#num#	k4
mrtvých	mrtvý	k1gMnPc2
</s>
<s>
56	#num#	k4
652	#num#	k4
zraněných	zraněný	k1gMnPc2
</s>
<s>
6	#num#	k4
824	#num#	k4
mrtvých	mrtvý	k1gMnPc2
na	na	k7c4
následky	následek	k1gInPc4
zranění	zranění	k1gNnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
4	#num#	k4
302	#num#	k4
mrtvých	mrtvý	k1gMnPc2
a	a	k8xC
pohřešovaných	pohřešovaný	k1gMnPc2
</s>
<s>
3	#num#	k4
316	#num#	k4
zraněných	zraněný	k1gMnPc2
</s>
<s>
19	#num#	k4
904	#num#	k4
nemocných	nemocný	k1gMnPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
2	#num#	k4
456	#num#	k4
mrtvých	mrtvý	k1gMnPc2
a	a	k8xC
zraněných	zraněný	k1gMnPc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
a	a	k8xC
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
2	#num#	k4
400	#num#	k4
mrtvých	mrtvý	k1gMnPc2
a	a	k8xC
zraněných	zraněný	k1gMnPc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
*	*	kIx~
<g/>
30	#num#	k4
000	#num#	k4
mrtvých	mrtvý	k1gMnPc2
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
90	#num#	k4
000	#num#	k4
mrtvých	mrtvý	k1gMnPc2
a	a	k8xC
následky	následek	k1gInPc1
zranění	zranění	k1gNnSc2
a	a	k8xC
epidemie	epidemie	k1gFnSc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rusko-turecká	rusko-turecký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
1877	#num#	k4
<g/>
–	–	k?
<g/>
1878	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
také	také	k9
jako	jako	k8xS,k8xC
bulharská	bulharský	k2eAgFnSc1d1
osvobozenecká	osvobozenecký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
válečný	válečný	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
mezi	mezi	k7c7
Ruskou	ruský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
a	a	k8xC
Osmanskou	osmanský	k2eAgFnSc7d1
říší	říše	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
straně	strana	k1gFnSc6
Ruska	Rusko	k1gNnSc2
se	se	k3xPyFc4
do	do	k7c2
něj	on	k3xPp3gNnSc2
zapojilo	zapojit	k5eAaPmAgNnS
Rumunsko	Rumunsko	k1gNnSc1
<g/>
,	,	kIx,
Srbsko	Srbsko	k1gNnSc1
<g/>
,	,	kIx,
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
a	a	k8xC
bulharští	bulharský	k2eAgMnPc1d1
opolčenci	opolčenec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osmanské	osmanský	k2eAgInPc4d1
Turky	turek	k1gInPc4
nepřímo	přímo	k6eNd1
podporovali	podporovat	k5eAaImAgMnP
hlavně	hlavně	k9
Britové	Brit	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Propukla	propuknout	k5eAaPmAgFnS
jako	jako	k9
vyústění	vyústění	k1gNnSc4
Velké	velký	k2eAgFnSc2d1
východní	východní	k2eAgFnSc2d1
krize	krize	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
skládala	skládat	k5eAaImAgFnS
z	z	k7c2
dalších	další	k2eAgInPc2d1
konfliktů	konflikt	k1gInPc2
a	a	k8xC
povstání	povstání	k1gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
bylo	být	k5eAaImAgNnS
hercegovské	hercegovský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
<g/>
,	,	kIx,
dubnové	dubnový	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
<g/>
,	,	kIx,
srbsko-turecká	srbsko-turecký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
či	či	k8xC
razložsko-kresenské	razložsko-kresenský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
<g/>
,	,	kIx,
a	a	k8xC
i	i	k9
konferencí	konference	k1gFnPc2
a	a	k8xC
mírových	mírový	k2eAgFnPc2d1
dohod	dohoda	k1gFnPc2
jako	jako	k8xS,k8xC
byla	být	k5eAaImAgFnS
Cařihradská	cařihradský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
<g/>
,	,	kIx,
Londýnská	londýnský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
či	či	k8xC
Berlínský	berlínský	k2eAgInSc1d1
kongres	kongres	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
ruským	ruský	k2eAgNnSc7d1
vítězstvím	vítězství	k1gNnSc7
<g/>
,	,	kIx,
uzavřením	uzavření	k1gNnSc7
předběžného	předběžný	k2eAgInSc2d1
míru	mír	k1gInSc2
v	v	k7c6
San	San	k1gMnSc6
Stefanu	Stefan	k1gMnSc6
<g/>
,	,	kIx,
kterým	který	k3yQgFnPc3,k3yIgFnPc3,k3yRgFnPc3
bylo	být	k5eAaImAgNnS
vytvořeno	vytvořit	k5eAaPmNgNnS
osvobozené	osvobozený	k2eAgNnSc1d1
novodobé	novodobý	k2eAgNnSc1d1
Velké	velký	k2eAgNnSc1d1
Bulharsko	Bulharsko	k1gNnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
hranice	hranice	k1gFnSc1
a	a	k8xC
suverenita	suverenita	k1gFnSc1
byly	být	k5eAaImAgFnP
drasticky	drasticky	k6eAd1
omezeny	omezit	k5eAaPmNgFnP
nátlakem	nátlak	k1gInSc7
velmocí	velmoc	k1gFnPc2
během	během	k7c2
Berlínského	berlínský	k2eAgInSc2d1
kongresu	kongres	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Předcházející	předcházející	k2eAgFnPc1d1
události	událost	k1gFnPc1
<g/>
,	,	kIx,
motivace	motivace	k1gFnSc1
důležitých	důležitý	k2eAgMnPc2d1
účastníků	účastník	k1gMnPc2
</s>
<s>
Velká	velký	k2eAgFnSc1d1
východní	východní	k2eAgFnSc1d1
krize	krize	k1gFnSc1
<g/>
,	,	kIx,
povstání	povstání	k1gNnSc1
v	v	k7c6
Bosně	Bosna	k1gFnSc6
a	a	k8xC
Hercegovině	Hercegovina	k1gFnSc6
<g/>
,	,	kIx,
Dubnové	dubnový	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
</s>
<s>
Již	již	k6eAd1
v	v	k7c6
zimě	zima	k1gFnSc6
1874	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
se	se	k3xPyFc4
hospodářství	hospodářství	k1gNnSc3
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
dostalo	dostat	k5eAaPmAgNnS
do	do	k7c2
hluboké	hluboký	k2eAgFnSc2d1
krize	krize	k1gFnSc2
<g/>
,	,	kIx,
ke	k	k7c3
které	který	k3yQgFnSc3,k3yRgFnSc3,k3yIgFnSc3
spělo	spět	k5eAaImAgNnS
již	již	k6eAd1
od	od	k7c2
krymské	krymský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokusy	pokus	k1gInPc4
stabilizovat	stabilizovat	k5eAaBmF
státní	státní	k2eAgFnSc4d1
pokladnu	pokladna	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
velice	velice	k6eAd1
významně	významně	k6eAd1
zatěžovalo	zatěžovat	k5eAaImAgNnS
splácení	splácení	k1gNnSc3
zahraničních	zahraniční	k2eAgMnPc2d1
dluhů	dluh	k1gInPc2
<g/>
,	,	kIx,
novými	nový	k2eAgFnPc7d1
dávkami	dávka	k1gFnPc7
<g/>
,	,	kIx,
tuhá	tuhý	k2eAgFnSc1d1
zima	zima	k1gFnSc1
a	a	k8xC
neúroda	neúroda	k1gFnSc1
vedly	vést	k5eAaImAgInP
k	k	k7c3
nepokojům	nepokoj	k1gInPc3
v	v	k7c6
národnostně	národnostně	k6eAd1
a	a	k8xC
nábožensky	nábožensky	k6eAd1
různorodém	různorodý	k2eAgInSc6d1
státě	stát	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povstání	povstání	k1gNnSc1
v	v	k7c6
Bosně	Bosna	k1gFnSc6
a	a	k8xC
Hercegovině	Hercegovina	k1gFnSc6
začalo	začít	k5eAaPmAgNnS
v	v	k7c6
červnu	červen	k1gInSc6
1875	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulhaři	Bulhar	k1gMnPc1
inspirováni	inspirovat	k5eAaBmNgMnP
bosenskými	bosenský	k2eAgMnPc7d1
událostmi	událost	k1gFnPc7
se	se	k3xPyFc4
připravili	připravit	k5eAaPmAgMnP
k	k	k7c3
povstání	povstání	k1gNnSc3
v	v	k7c6
jaře	jaro	k1gNnSc6
1876	#num#	k4
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnSc4
dubnové	dubnový	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
bylo	být	k5eAaImAgNnS
drasticky	drasticky	k6eAd1
potlačeno	potlačit	k5eAaPmNgNnS
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
příklonu	příklon	k1gInSc3
evropského	evropský	k2eAgNnSc2d1
veřejného	veřejný	k2eAgNnSc2d1
mínění	mínění	k1gNnSc2
k	k	k7c3
podpoře	podpora	k1gFnSc3
národně-osvobozeneckých	národně-osvobozenecký	k2eAgInPc2d1
bojů	boj	k1gInPc2
či	či	k8xC
alespoň	alespoň	k9
požadavků	požadavek	k1gInPc2
větší	veliký	k2eAgFnSc2d2
míry	míra	k1gFnSc2
autonomie	autonomie	k1gFnSc2
a	a	k8xC
náboženské	náboženský	k2eAgFnSc2d1
svobody	svoboda	k1gFnSc2
pro	pro	k7c4
balkánské	balkánský	k2eAgMnPc4d1
křesťany	křesťan	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ač	ač	k8xS
se	se	k3xPyFc4
mínění	mínění	k1gNnSc1
širokých	široký	k2eAgFnPc2d1
vrstev	vrstva	k1gFnPc2
evropské	evropský	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
stavělo	stavět	k5eAaImAgNnS
k	k	k7c3
problémům	problém	k1gInPc3
téměř	téměř	k6eAd1
jednoznačně	jednoznačně	k6eAd1
<g/>
,	,	kIx,
tak	tak	k6eAd1
postoje	postoj	k1gInPc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
velmocí	velmoc	k1gFnPc2
ke	k	k7c3
krizi	krize	k1gFnSc3
byly	být	k5eAaImAgInP
velice	velice	k6eAd1
různorodé	různorodý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInPc1d1
rozpory	rozpor	k1gInPc1
mezi	mezi	k7c7
velmocemi	velmoc	k1gFnPc7
se	se	k3xPyFc4
táhly	táhnout	k5eAaImAgInP
již	již	k6eAd1
od	od	k7c2
krymské	krymský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko	Rusko	k1gNnSc1
se	se	k3xPyFc4
snažilo	snažit	k5eAaImAgNnS
obnovit	obnovit	k5eAaPmF
minimálně	minimálně	k6eAd1
své	svůj	k3xOyFgFnPc4
předkrymské	předkrymský	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
na	na	k7c6
Balkáně	Balkán	k1gInSc6
a	a	k8xC
v	v	k7c6
oblasti	oblast	k1gFnSc6
Černého	Černý	k1gMnSc2
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
požadovalo	požadovat	k5eAaImAgNnS
autonomii	autonomie	k1gFnSc4
pro	pro	k7c4
Bulharsko	Bulharsko	k1gNnSc4
či	či	k8xC
Bosnu	Bosna	k1gFnSc4
a	a	k8xC
Hercegovinu	Hercegovina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Británie	Británie	k1gFnSc1
měla	mít	k5eAaImAgFnS
naopak	naopak	k6eAd1
snahu	snaha	k1gFnSc4
čelit	čelit	k5eAaImF
ruskému	ruský	k2eAgInSc3d1
vlivu	vliv	k1gInSc3
<g/>
,	,	kIx,
zastavit	zastavit	k5eAaPmF
rozrůstání	rozrůstání	k1gNnSc4
ruského	ruský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
směrem	směr	k1gInSc7
na	na	k7c4
jih	jih	k1gInSc4
a	a	k8xC
udržování	udržování	k1gNnSc4
nárazníkových	nárazníkový	k2eAgInPc2d1
států	stát	k1gInPc2
mezi	mezi	k7c7
britskou	britský	k2eAgFnSc7d1
a	a	k8xC
ruskou	ruský	k2eAgFnSc7d1
sférou	sféra	k1gFnSc7
vlivu	vliv	k1gInSc2
bylo	být	k5eAaImAgNnS
součástí	součást	k1gFnSc7
jejích	její	k3xOp3gInPc2
geopolitických	geopolitický	k2eAgInPc2d1
plánů	plán	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalších	další	k2eAgFnPc6d1
evropských	evropský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
si	se	k3xPyFc3
často	často	k6eAd1
protiřečily	protiřečit	k5eAaImAgInP
zájmy	zájem	k1gInPc4
finančníků	finančník	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
potřebovali	potřebovat	k5eAaImAgMnP
udržet	udržet	k5eAaPmF
tureckou	turecký	k2eAgFnSc4d1
říši	říše	k1gFnSc4
už	už	k9
kvůli	kvůli	k7c3
příjmům	příjem	k1gInPc3
ze	z	k7c2
zahraničních	zahraniční	k2eAgFnPc2d1
půjček	půjčka	k1gFnPc2
<g/>
,	,	kIx,
s	s	k7c7
názory	názor	k1gInPc7
široké	široký	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Srbsko-turecká	srbsko-turecký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gInSc1
ohlas	ohlas	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Srbsko-turecká	srbsko-turecký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
1876	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
knížectvích	knížectví	k1gNnPc6
Srbsko	Srbsko	k1gNnSc1
a	a	k8xC
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
se	se	k3xPyFc4
asi	asi	k9
dva	dva	k4xCgInPc4
měsíce	měsíc	k1gInPc4
po	po	k7c6
nezdaru	nezdar	k1gInSc6
Dubnového	dubnový	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
prosadila	prosadit	k5eAaPmAgFnS
vůle	vůle	k1gFnSc1
pomoci	pomoct	k5eAaPmF
bosenskohercegovským	bosenskohercegovský	k2eAgMnPc3d1
povstalcům	povstalec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
16	#num#	k4
<g/>
.	.	kIx.
červnu	červen	k1gInSc6
1876	#num#	k4
uzavřela	uzavřít	k5eAaPmAgFnS
knížectví	knížectví	k1gNnSc4
spojenectví	spojenectví	k1gNnSc2
a	a	k8xC
již	již	k6eAd1
26	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1876	#num#	k4
vyhlásil	vyhlásit	k5eAaPmAgMnS
kníže	kníže	k1gMnSc1
Nikola	Nikola	k1gMnSc1
válku	válka	k1gFnSc4
Turecku	Turecko	k1gNnSc3
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
následoval	následovat	k5eAaImAgInS
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1876	#num#	k4
srbský	srbský	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
Milan	Milan	k1gMnSc1
Obrenovič	Obrenovič	k1gMnSc1
stejným	stejný	k2eAgInSc7d1
krokem	krok	k1gInSc7
<g/>
.	.	kIx.
„	„	k?
<g/>
Přes	přes	k7c4
značnou	značný	k2eAgFnSc4d1
materiální	materiální	k2eAgFnSc4d1
i	i	k8xC
morální	morální	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
Ruska	Rusko	k1gNnSc2
se	se	k3xPyFc4
válka	válka	k1gFnSc1
nevyvíjela	vyvíjet	k5eNaImAgFnS
ve	v	k7c4
prospěch	prospěch	k1gInSc4
slovanských	slovanský	k2eAgMnPc2d1
spojenců	spojenec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turci	Turek	k1gMnPc1
měli	mít	k5eAaImAgMnP
značnou	značný	k2eAgFnSc4d1
převahu	převaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kritických	kritický	k2eAgFnPc6d1
chvílích	chvíle	k1gFnPc6
zasáhlo	zasáhnout	k5eAaPmAgNnS
Rusko	Rusko	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
ultimativně	ultimativně	k6eAd1
vyzvalo	vyzvat	k5eAaPmAgNnS
Turecko	Turecko	k1gNnSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
zastavilo	zastavit	k5eAaPmAgNnS
boje	boj	k1gInPc4
a	a	k8xC
uzavřelo	uzavřít	k5eAaPmAgNnS
příměří	příměří	k1gNnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tato	tento	k3xDgFnSc1
srbsko-turecká	srbsko-turecký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
měla	mít	k5eAaImAgFnS
značný	značný	k2eAgInSc4d1
ohlas	ohlas	k1gInSc4
v	v	k7c6
bulharské	bulharský	k2eAgFnSc6d1
politické	politický	k2eAgFnSc6d1
emigraci	emigrace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
1876	#num#	k4
vyslal	vyslat	k5eAaPmAgInS
Slovanský	slovanský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
Vladimira	Vladimir	k1gMnSc2
Jonina	Jonin	k1gMnSc2
do	do	k7c2
Bukurešti	Bukurešť	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
10	#num#	k4
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
července	červenec	k1gInSc2
probíhalo	probíhat	k5eAaImAgNnS
zasedání	zasedání	k1gNnSc1
bulharských	bulharský	k2eAgInPc2d1
dobročinných	dobročinný	k2eAgInPc2d1
spolků	spolek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
se	se	k3xPyFc4
sjednotily	sjednotit	k5eAaPmAgFnP
do	do	k7c2
Bulharské	bulharský	k2eAgFnSc2d1
ústřední	ústřední	k2eAgFnSc2d1
dobročinné	dobročinný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
(	(	kIx(
<g/>
bulharsky	bulharsky	k6eAd1
Bălgarsko	Bălgarsko	k1gNnSc1
centralno	centralno	k1gNnSc1
blagotvoritelno	blagotvoritelna	k1gFnSc5
obštetstvo	obštetstvo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gMnSc7
předsedou	předseda	k1gMnSc7
byl	být	k5eAaImAgMnS
Kirjak	Kirjak	k1gMnSc1
Cankov	Cankov	k1gInSc1
a	a	k8xC
jednatelem	jednatel	k1gMnSc7
Petr	Petr	k1gMnSc1
Enčev	Enčvo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
organizace	organizace	k1gFnSc1
se	se	k3xPyFc4
snažila	snažit	k5eAaImAgFnS
plnit	plnit	k5eAaImF
funkce	funkce	k1gFnSc1
BRÚV	BRÚV	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
po	po	k7c6
Dubnovém	dubnový	k2eAgNnSc6d1
povstání	povstání	k1gNnSc6
v	v	k7c6
jistém	jistý	k2eAgInSc6d1
stupni	stupeň	k1gInSc6
rozkladu	rozklad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
BÚDS	BÚDS	kA
vyzýval	vyzývat	k5eAaImAgMnS
Bulhary	Bulhar	k1gMnPc4
i	i	k8xC
turecké	turecký	k2eAgMnPc4d1
rolníky	rolník	k1gMnPc4
k	k	k7c3
dalšímu	další	k2eAgNnSc3d1
povstání	povstání	k1gNnSc3
na	na	k7c4
pomoc	pomoc	k1gFnSc4
Srbům	Srb	k1gMnPc3
a	a	k8xC
Černohorcům	Černohorec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	to	k9
však	však	k9
vzhledem	vzhledem	k7c3
k	k	k7c3
situaci	situace	k1gFnSc3
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
značná	značný	k2eAgFnSc1d1
část	část	k1gFnSc1
bulharského	bulharský	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
stižena	stižen	k2eAgFnSc1d1
následky	následek	k1gInPc7
tureckých	turecký	k2eAgFnPc2d1
represí	represe	k1gFnPc2
<g/>
,	,	kIx,
nebylo	být	k5eNaImAgNnS
příliš	příliš	k6eAd1
reálné	reálný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinak	jinak	k6eAd1
však	však	k9
byli	být	k5eAaImAgMnP
velmi	velmi	k6eAd1
aktivní	aktivní	k2eAgMnPc1d1
bulharští	bulharský	k2eAgMnPc1d1
emigranti	emigrant	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
dali	dát	k5eAaPmAgMnP
dohromady	dohromady	k6eAd1
v	v	k7c6
několika	několik	k4yIc6
dnech	den	k1gInPc6
5	#num#	k4
000	#num#	k4
dobrovolníků	dobrovolník	k1gMnPc2
v	v	k7c6
Srbsku	Srbsko	k1gNnSc6
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
ruskými	ruský	k2eAgMnPc7d1
dobrovolníky	dobrovolník	k1gMnPc7
vytvořili	vytvořit	k5eAaPmAgMnP
úplnou	úplný	k2eAgFnSc4d1
brigádu	brigáda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
srbsko-tureckém	srbsko-turecký	k2eAgNnSc6d1
příměří	příměří	k1gNnSc6
se	se	k3xPyFc4
přesunuli	přesunout	k5eAaPmAgMnP
dobrovolníci	dobrovolník	k1gMnPc1
do	do	k7c2
Rumunska	Rumunsko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
zůstali	zůstat	k5eAaPmAgMnP
v	v	k7c6
pohotovosti	pohotovost	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
očekávání	očekávání	k1gNnSc6
dalšího	další	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
1876	#num#	k4
schválil	schválit	k5eAaPmAgInS
Bulharský	bulharský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
sněm	sněm	k1gInSc1
v	v	k7c6
Bukurešti	Bukurešť	k1gFnSc6
nový	nový	k2eAgInSc4d1
politický	politický	k2eAgInSc4d1
program	program	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
bylo	být	k5eAaImAgNnS
rozesláno	rozeslán	k2eAgNnSc1d1
tzv.	tzv.	kA
bulharské	bulharský	k2eAgNnSc1d1
memorandum	memorandum	k1gNnSc1
(	(	kIx(
<g/>
hlavně	hlavně	k9
velmocem	velmoc	k1gFnPc3
<g/>
,	,	kIx,
delegace	delegace	k1gFnSc1
Dragana	Dragana	k1gFnSc1
Cankova	Cankův	k2eAgFnSc1d1
a	a	k8xC
Marka	marka	k1gFnSc1
Balabanova	Balabanův	k2eAgFnSc1d1
směřovala	směřovat	k5eAaImAgFnS
do	do	k7c2
několika	několik	k4yIc2
evropských	evropský	k2eAgNnPc2d1
hlavních	hlavní	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
něm	on	k3xPp3gMnSc6
se	se	k3xPyFc4
již	již	k6eAd1
v	v	k7c6
úvodu	úvod	k1gInSc6
pravilo	pravit	k5eAaBmAgNnS,k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
turecké	turecký	k2eAgFnPc1d1
reformy	reforma	k1gFnPc1
jsou	být	k5eAaImIp3nP
k	k	k7c3
ničemu	nic	k3yNnSc3
<g/>
,	,	kIx,
že	že	k8xS
ochranu	ochrana	k1gFnSc4
křesťanů	křesťan	k1gMnPc2
a	a	k8xC
pořádek	pořádek	k1gInSc4
v	v	k7c6
zemi	zem	k1gFnSc6
může	moct	k5eAaImIp3nS
zajistit	zajistit	k5eAaPmF
jedině	jedině	k6eAd1
přítomnost	přítomnost	k1gFnSc4
cizích	cizí	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
<g/>
,	,	kIx,
také	také	k6eAd1
bylo	být	k5eAaImAgNnS
žádáno	žádat	k5eAaImNgNnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
nové	nový	k2eAgFnPc1d1
hranice	hranice	k1gFnPc1
byly	být	k5eAaImAgFnP
určeny	určit	k5eAaPmNgFnP
mezinárodní	mezinárodní	k2eAgFnSc7d1
komisí	komise	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tisíce	tisíc	k4xCgInPc1
Bulharů	Bulhar	k1gMnPc2
podepsaly	podepsat	k5eAaPmAgInP
slavnostní	slavnostní	k2eAgFnSc4d1
adresu	adresa	k1gFnSc4
ruskému	ruský	k2eAgMnSc3d1
carovi	car	k1gMnSc3
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
nadšeně	nadšeně	k6eAd1
vítala	vítat	k5eAaImAgFnS
rozhodný	rozhodný	k2eAgInSc4d1
postoj	postoj	k1gInSc4
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
sděloval	sdělovat	k5eAaImAgMnS
car	car	k1gMnSc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
projevu	projev	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Cařihradská	cařihradský	k2eAgFnSc1d1
a	a	k8xC
londýnská	londýnský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
<g/>
,	,	kIx,
vyhlášení	vyhlášení	k1gNnSc1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
bulharští	bulharský	k2eAgMnPc1d1
opolčenci	opolčenec	k1gMnPc1
</s>
<s>
Zvěrstva	zvěrstvo	k1gNnPc1
tureckých	turecký	k2eAgMnPc2d1
bašibozuků	bašibozuk	k1gMnPc2
v	v	k7c6
Makedonii	Makedonie	k1gFnSc6
</s>
<s>
Velmoci	velmoc	k1gFnPc1
se	se	k3xPyFc4
snažily	snažit	k5eAaImAgFnP
vyřešit	vyřešit	k5eAaPmF
krizi	krize	k1gFnSc4
během	během	k7c2
konference	konference	k1gFnSc2
v	v	k7c6
Cařihradě	Cařihrad	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
na	na	k7c6
konci	konec	k1gInSc6
prosince	prosinec	k1gInSc2
1876	#num#	k4
(	(	kIx(
<g/>
začala	začít	k5eAaPmAgFnS
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
prosince	prosinec	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zájmy	zájem	k1gInPc4
Ruska	Rusko	k1gNnSc2
zastupoval	zastupovat	k5eAaImAgMnS
cařihradský	cařihradský	k2eAgMnSc1d1
vyslanec	vyslanec	k1gMnSc1
hrabě	hrabě	k1gMnSc1
N.	N.	kA
P.	P.	kA
Ignaťjev	Ignaťjev	k1gFnPc2
<g/>
,	,	kIx,
britský	britský	k2eAgInSc4d1
pohled	pohled	k1gInSc4
prosazoval	prosazovat	k5eAaImAgMnS
ministr	ministr	k1gMnSc1
pro	pro	k7c4
Indii	Indie	k1gFnSc4
lord	lord	k1gMnSc1
Salisbury	Salisbura	k1gFnSc2
<g/>
,	,	kIx,
Osmanskou	osmanský	k2eAgFnSc4d1
říši	říše	k1gFnSc4
hájil	hájit	k5eAaImAgMnS
turecký	turecký	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
Saffet	Saffet	k1gMnSc1
paša	paša	k1gMnSc1
a	a	k8xC
dále	daleko	k6eAd2
se	se	k3xPyFc4
účastnili	účastnit	k5eAaImAgMnP
zástupci	zástupce	k1gMnPc7
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc2
a	a	k8xC
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
bylo	být	k5eAaImAgNnS
dosaženo	dosáhnout	k5eAaPmNgNnS
shody	shoda	k1gFnSc2
na	na	k7c6
nutnosti	nutnost	k1gFnSc6
autonomie	autonomie	k1gFnSc2
Bosny	Bosna	k1gFnSc2
<g/>
,	,	kIx,
Hercegoviny	Hercegovina	k1gFnSc2
a	a	k8xC
Bulharska	Bulharsko	k1gNnSc2
(	(	kIx(
<g/>
její	její	k3xOp3gNnSc1
provádění	provádění	k1gNnSc1
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
v	v	k7c6
rukou	ruka	k1gFnPc6
zahraničních	zahraniční	k2eAgMnPc2d1
komisařů	komisař	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dlouhém	dlouhý	k2eAgNnSc6d1
<g/>
,	,	kIx,
těžkém	těžký	k2eAgNnSc6d1
<g/>
,	,	kIx,
obtížném	obtížný	k2eAgNnSc6d1
jednání	jednání	k1gNnSc6
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
dosaženo	dosáhnout	k5eAaPmNgNnS
společného	společný	k2eAgNnSc2d1
rozhodnutí	rozhodnutí	k1gNnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
navrhl	navrhnout	k5eAaPmAgMnS
francouzský	francouzský	k2eAgMnSc1d1
delegát	delegát	k1gMnSc1
<g/>
:	:	kIx,
autonomní	autonomní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
Bulharska	Bulharsko	k1gNnSc2
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
rozdělena	rozdělit	k5eAaPmNgFnS
do	do	k7c2
dvou	dva	k4xCgFnPc2
oblasti	oblast	k1gFnSc6
(	(	kIx(
<g/>
východní	východní	k2eAgInSc4d1
vilájet	vilájet	k5eAaImF,k5eAaPmF
v	v	k7c6
Trnově	trnově	k6eAd1
<g/>
,	,	kIx,
západní	západní	k2eAgInPc4d1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Sofii	Sofia	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
každé	každý	k3xTgNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
křesťanským	křesťanský	k2eAgInSc7d1
valijou	valijou	k?
(	(	kIx(
<g/>
hlavní	hlavní	k2eAgMnSc1d1
guvernér	guvernér	k1gMnSc1
<g/>
)	)	kIx)
jmenovaným	jmenovaný	k1gMnSc7
sultánem	sultán	k1gMnSc7
(	(	kIx(
<g/>
k	k	k7c3
tomu	ten	k3xDgMnSc3
byl	být	k5eAaImAgInS
nutný	nutný	k2eAgInSc1d1
souhlas	souhlas	k1gInSc1
velmocí	velmoc	k1gFnPc2
<g/>
)	)	kIx)
na	na	k7c4
5	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
obyvatelstvo	obyvatelstvo	k1gNnSc1
mělo	mít	k5eAaImAgNnS
mít	mít	k5eAaImF
podíl	podíl	k1gInSc4
na	na	k7c4
rozhodování	rozhodování	k1gNnSc4
prostřednictvím	prostřednictvím	k7c2
oblastního	oblastní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
(	(	kIx(
<g/>
to	ten	k3xDgNnSc1
mělo	mít	k5eAaImAgNnS
určovat	určovat	k5eAaImF
výši	výše	k1gFnSc4
daně	daň	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
30	#num#	k4
%	%	kIx~
daní	daň	k1gFnPc2
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
odvedeno	odvést	k5eAaPmNgNnS
turecké	turecký	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policie	policie	k1gFnSc1
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
tvořena	tvořit	k5eAaImNgFnS
muslimy	muslim	k1gMnPc7
a	a	k8xC
křesťany	křesťan	k1gMnPc7
<g/>
,	,	kIx,
političtí	politický	k2eAgMnPc1d1
trestanci	trestanec	k1gMnPc1
(	(	kIx(
<g/>
i	i	k9
vyhnanci	vyhnanec	k1gMnPc1
<g/>
)	)	kIx)
amnestováni	amnestován	k2eAgMnPc1d1
a	a	k8xC
kontrolu	kontrola	k1gFnSc4
provedení	provedení	k1gNnSc2
všech	všecek	k3xTgFnPc2
podmínek	podmínka	k1gFnPc2
měla	mít	k5eAaImAgFnS
zajistit	zajistit	k5eAaPmF
mezinárodní	mezinárodní	k2eAgFnSc1d1
komise	komise	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Lord	lord	k1gMnSc1
Salisbury	Salisbura	k1gFnSc2
sice	sice	k8xC
dohodu	dohoda	k1gFnSc4
odsouhlasil	odsouhlasit	k5eAaPmAgMnS
<g/>
,	,	kIx,
avšak	avšak	k8xC
mnohé	mnohý	k2eAgInPc1d1
britské	britský	k2eAgInPc1d1
kruhy	kruh	k1gInPc1
se	se	k3xPyFc4
chovaly	chovat	k5eAaImAgInP
značně	značně	k6eAd1
turkofilně	turkofilně	k6eAd1
a	a	k8xC
vyslovily	vyslovit	k5eAaPmAgFnP
podporu	podpora	k1gFnSc4
Turecku	Turecko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiný	jiný	k2eAgInSc1d1
britský	britský	k2eAgInSc1d1
diplomat	diplomat	k1gInSc1
Elliot	Elliota	k1gFnPc2
silně	silně	k6eAd1
ovlivnil	ovlivnit	k5eAaPmAgInS
další	další	k2eAgNnSc4d1
sultánovo	sultánův	k2eAgNnSc4d1
rozhodnutí	rozhodnutí	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
velice	velice	k6eAd1
překvapivé	překvapivý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sultán	sultán	k1gMnSc1
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
velkým	velký	k2eAgMnSc7d1
vezírem	vezír	k1gMnSc7
Midhata	Midhat	k1gMnSc4
pašu	paša	k1gMnSc4
(	(	kIx(
<g/>
konstitucionalistu	konstitucionalista	k1gMnSc4
<g/>
)	)	kIx)
a	a	k8xC
vyhlásil	vyhlásit	k5eAaPmAgMnS
ústavu	ústava	k1gFnSc4
(	(	kIx(
<g/>
tou	ten	k3xDgFnSc7
podle	podle	k7c2
tureckého	turecký	k2eAgInSc2d1
názoru	názor	k1gInSc2
zajistil	zajistit	k5eAaPmAgMnS
reformy	reforma	k1gFnPc4
<g/>
:	:	kIx,
obyvatelé	obyvatel	k1gMnPc1
měli	mít	k5eAaImAgMnP
volit	volit	k5eAaImF
sandžakovou	sandžakový	k2eAgFnSc4d1
(	(	kIx(
<g/>
krajskou	krajský	k2eAgFnSc4d1
<g/>
)	)	kIx)
radu	rada	k1gFnSc4
a	a	k8xC
ta	ten	k3xDgFnSc1
měla	mít	k5eAaImAgFnS
vybrat	vybrat	k5eAaPmF
zástupce	zástupce	k1gMnSc4
do	do	k7c2
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
v	v	k7c6
Cařihradu	Cařihrad	k1gInSc2
<g/>
;	;	kIx,
za	za	k7c4
bulharskou	bulharský	k2eAgFnSc4d1
provincii	provincie	k1gFnSc4
to	ten	k3xDgNnSc1
měli	mít	k5eAaImAgMnP
být	být	k5eAaImF
4	#num#	k4
křesťané	křesťan	k1gMnPc1
a	a	k8xC
4	#num#	k4
muslimové	muslim	k1gMnPc1
<g/>
,	,	kIx,
obdobné	obdobný	k2eAgInPc1d1
počty	počet	k1gInPc1
měly	mít	k5eAaImAgInP
zastupovat	zastupovat	k5eAaImF
Bosnu	Bosna	k1gFnSc4
a	a	k8xC
Hercegovinu	Hercegovina	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Bulharsku	Bulharsko	k1gNnSc6
byla	být	k5eAaImAgFnS
muslimskými	muslimský	k2eAgFnPc7d1
kruhy	kruh	k1gInPc1
zorganizována	zorganizován	k2eAgFnSc1d1
„	„	k?
<g/>
děkovná	děkovný	k2eAgFnSc1d1
akce	akce	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
ne	ne	k9
autonomii	autonomie	k1gFnSc4
<g/>
,	,	kIx,
ano	ano	k9
konstituci	konstituce	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
účastnil	účastnit	k5eAaImAgMnS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
jen	jen	k9
turkofilní	turkofilní	k2eAgInSc4d1
zlomek	zlomek	k1gInSc4
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ignaťjev	Ignaťjev	k1gFnPc2
nato	nato	k6eAd1
navrhl	navrhnout	k5eAaPmAgMnS
donutit	donutit	k5eAaPmF
silou	síla	k1gFnSc7
Turky	turek	k1gInPc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
rozhodnutí	rozhodnutí	k1gNnSc1
konference	konference	k1gFnSc2
realizovali	realizovat	k5eAaBmAgMnP
<g/>
,	,	kIx,
proti	proti	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
se	se	k3xPyFc4
postavil	postavit	k5eAaPmAgInS
Salisbury	Salisbura	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
konference	konference	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
bezvýsledně	bezvýsledně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
března	březen	k1gInSc2
1877	#num#	k4
se	se	k3xPyFc4
ještě	ještě	k6eAd1
konala	konat	k5eAaImAgFnS
londýnská	londýnský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
bylo	být	k5eAaImAgNnS
úsilí	úsilí	k1gNnSc1
diplomatů	diplomat	k1gMnPc2
završeno	završen	k2eAgNnSc4d1
19	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
31	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
března	březen	k1gInSc2
uzavřením	uzavření	k1gNnSc7
protokolu	protokol	k1gInSc2
<g/>
,	,	kIx,
kterým	který	k3yIgMnPc3,k3yRgMnPc3,k3yQgMnPc3
bylo	být	k5eAaImAgNnS
Turecko	Turecko	k1gNnSc1
vyzváno	vyzván	k2eAgNnSc1d1
k	k	k7c3
provedení	provedení	k1gNnSc3
reforem	reforma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
opět	opět	k6eAd1
zásluhou	zásluha	k1gFnSc7
Británie	Británie	k1gFnSc2
vyšly	vyjít	k5eAaPmAgFnP
snahy	snaha	k1gFnPc1
naprázdno	naprázdno	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Pod	pod	k7c7
vlivem	vliv	k1gInSc7
neúspěchů	neúspěch	k1gInPc2
obou	dva	k4xCgFnPc2
konferencí	konference	k1gFnPc2
zorganizovalo	zorganizovat	k5eAaPmAgNnS
Rusko	Rusko	k1gNnSc1
dvě	dva	k4xCgFnPc4
armády	armáda	k1gFnPc4
(	(	kIx(
<g/>
dunajskou	dunajský	k2eAgFnSc4d1
o	o	k7c4
185	#num#	k4
<g/>
–	–	k?
<g/>
190	#num#	k4
tisících	tisící	k4xOgMnPc6
mužích	muž	k1gMnPc6
/	/	kIx~
<g/>
postupně	postupně	k6eAd1
pak	pak	k6eAd1
během	během	k7c2
války	válka	k1gFnSc2
byla	být	k5eAaImAgFnS
doplňována	doplňován	k2eAgFnSc1d1
<g/>
/	/	kIx~
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
vedl	vést	k5eAaImAgMnS
carův	carův	k2eAgMnSc1d1
bratr	bratr	k1gMnSc1
Nikolaj	Nikolaj	k1gMnSc1
Nikolajevič	Nikolajevič	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kavkazskou	kavkazský	k2eAgFnSc7d1
s	s	k7c7
60	#num#	k4
<g/>
–	–	k?
<g/>
75	#num#	k4
tisíci	tisíc	k4xCgInPc7
muži	muž	k1gMnPc7
<g/>
,	,	kIx,
již	již	k6eAd1
vedl	vést	k5eAaImAgMnS
carův	carův	k2eAgMnSc1d1
bratr	bratr	k1gMnSc1
Michail	Michail	k1gMnSc1
Nikolajevič	Nikolajevič	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
uzavřelo	uzavřít	k5eAaPmAgNnS
dohody	dohoda	k1gFnSc2
s	s	k7c7
Rumunskem	Rumunsko	k1gNnSc7
(	(	kIx(
<g/>
o	o	k7c6
průchodu	průchod	k1gInSc6
vojsk	vojsko	k1gNnPc2
a	a	k8xC
posléze	posléze	k6eAd1
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
stát	stát	k5eAaImF,k5eAaPmF
i	i	k9
spojencem	spojenec	k1gMnSc7
Rusů	Rus	k1gMnPc2
v	v	k7c6
plánované	plánovaný	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
souhlasilo	souhlasit	k5eAaImAgNnS
s	s	k7c7
navýšením	navýšení	k1gNnSc7
stavu	stav	k1gInSc2
armády	armáda	k1gFnSc2
na	na	k7c4
59	#num#	k4
700	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k1gNnSc4
vojska	vojsko	k1gNnSc2
<g/>
,	,	kIx,
s	s	k7c7
nimiž	jenž	k3xRgNnPc7
Rusové	Rusová	k1gFnSc2
počítali	počítat	k5eAaImAgMnP
v	v	k7c6
připravované	připravovaný	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
černohorské	černohorský	k2eAgNnSc1d1
o	o	k7c6
cca	cca	kA
30	#num#	k4
tisících	tisící	k4xOgInPc2
a	a	k8xC
srbské	srbský	k2eAgNnSc4d1
o	o	k7c4
56	#num#	k4
tisících	tisící	k4xOgMnPc6
mužích	muž	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s>
Turecké	turecký	k2eAgNnSc1d1
odmítnutí	odmítnutí	k1gNnSc1
londýnského	londýnský	k2eAgInSc2d1
protokolu	protokol	k1gInSc2
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
záminkou	záminka	k1gFnSc7
pro	pro	k7c4
následné	následný	k2eAgNnSc4d1
vyhlášení	vyhlášení	k1gNnSc4
války	válka	k1gFnSc2
Alexandrem	Alexandr	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
proběhlo	proběhnout	k5eAaPmAgNnS
12	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
dubna	duben	k1gInSc2
1877	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ihned	ihned	k6eAd1
byla	být	k5eAaImAgNnP
vytvářen	vytvářen	k2eAgInSc4d1
bulharská	bulharský	k2eAgFnSc1d1
dobrovolnická	dobrovolnický	k2eAgFnSc1d1
domobrana	domobrana	k1gFnSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
opolčení	opolčení	k1gNnSc1
<g/>
,	,	kIx,
proto	proto	k8xC
byli	být	k5eAaImAgMnP
nazýváni	nazýván	k2eAgMnPc1d1
opolčenci	opolčenec	k1gMnPc1
<g/>
)	)	kIx)
–	–	k?
jejím	její	k3xOp3gNnSc7
sídlem	sídlo	k1gNnSc7
byl	být	k5eAaImAgInS
nejprve	nejprve	k6eAd1
Kišiněv	Kišiněv	k1gFnSc4
<g/>
,	,	kIx,
poté	poté	k6eAd1
Ploieș	Ploieș	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlásili	hlásit	k5eAaImAgMnP
se	se	k3xPyFc4
k	k	k7c3
ní	on	k3xPp3gFnSc3
dřívější	dřívější	k2eAgMnPc1d1
bulharští	bulharský	k2eAgMnPc1d1
dobrovolníci	dobrovolník	k1gMnPc1
ze	z	k7c2
Srbska	Srbsko	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
rumunské	rumunský	k2eAgFnSc2d1
emigrace	emigrace	k1gFnSc2
a	a	k8xC
odjinud	odjinud	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výcvik	výcvik	k1gInSc4
postupovali	postupovat	k5eAaImAgMnP
v	v	k7c6
Craiově	Craiův	k2eAgNnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
války	válka	k1gFnSc2
měli	mít	k5eAaImAgMnP
Rusové	Rus	k1gMnPc1
v	v	k7c6
bulharské	bulharský	k2eAgFnSc6d1
dobrovolnické	dobrovolnický	k2eAgFnSc6d1
domobraně	domobrana	k1gFnSc6
k	k	k7c3
dispozici	dispozice	k1gFnSc3
7	#num#	k4
500	#num#	k4
mužů	muž	k1gMnPc2
v	v	k7c6
6	#num#	k4
družinách	družina	k1gFnPc6
<g/>
,	,	kIx,
velel	velet	k5eAaImAgMnS
jim	on	k3xPp3gMnPc3
generálmajor	generálmajor	k1gMnSc1
N.	N.	kA
G.	G.	kA
Stoletov	Stoletov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
obdarováni	obdarován	k2eAgMnPc1d1
samarským	samarský	k2eAgInSc7d1
praporem	prapor	k1gInSc7
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
jim	on	k3xPp3gMnPc3
sloužil	sloužit	k5eAaImAgInS
jako	jako	k8xS,k8xC
bojová	bojový	k2eAgFnSc1d1
zástava	zástava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Bulharská	bulharský	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
etapa	etapa	k1gFnSc1
od	od	k7c2
překročení	překročení	k1gNnSc2
Dunaje	Dunaj	k1gInSc2
po	po	k7c4
bitvu	bitva	k1gFnSc4
u	u	k7c2
Staré	Staré	k2eAgFnSc2d1
Zagory	Zagora	k1gFnSc2
</s>
<s>
Mapa	mapa	k1gFnSc1
války	válka	k1gFnSc2
</s>
<s>
Zajatí	zajatý	k2eAgMnPc1d1
bašibozukové	bašibozuk	k1gMnPc1
</s>
<s>
Bašibozukové	bašibozuk	k1gMnPc1
se	se	k3xPyFc4
vracejí	vracet	k5eAaImIp3nP
s	s	k7c7
kořistí	kořist	k1gFnSc7
z	z	k7c2
rumunských	rumunský	k2eAgInPc2d1
břehů	břeh	k1gInPc2
Dunaje	Dunaj	k1gInSc2
</s>
<s>
Překročení	překročení	k1gNnSc1
Dunaje	Dunaj	k1gInSc2
se	se	k3xPyFc4
odehrálo	odehrát	k5eAaPmAgNnS
v	v	k7c6
prostoru	prostor	k1gInSc6
Zimnice	zimnice	k1gFnSc2
a	a	k8xC
Svištova	Svištův	k2eAgInSc2d1
<g/>
,	,	kIx,
začalo	začít	k5eAaPmAgNnS
o	o	k7c6
půlnoci	půlnoc	k1gFnSc6
na	na	k7c4
15	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
června	červen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
přípravě	příprava	k1gFnSc6
operace	operace	k1gFnSc2
byly	být	k5eAaImAgInP
zničeny	zničit	k5eAaPmNgInP
turecké	turecký	k2eAgInPc1d1
říční	říční	k2eAgInPc1d1
čluny	člun	k1gInPc1
a	a	k8xC
zaminováno	zaminován	k2eAgNnSc1d1
ústí	ústí	k1gNnSc1
Dunaje	Dunaj	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
přepravovanou	přepravovaný	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
nemohl	moct	k5eNaImAgMnS
překvapit	překvapit	k5eAaPmF
námořní	námořní	k2eAgInSc4d1
útok	útok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operaci	operace	k1gFnSc6
velel	velet	k5eAaImAgMnS
generál	generál	k1gMnSc1
M.	M.	kA
I.	I.	kA
Dragomir	Dragomir	k1gMnSc1
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
14	#num#	k4
<g/>
.	.	kIx.
pěší	pěší	k2eAgFnSc2d1
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rusové	k2eAgFnSc1d1
pak	pak	k6eAd1
obsadili	obsadit	k5eAaPmAgMnP
přístav	přístav	k1gInSc4
Svištov	Svištovo	k1gNnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
vytvořili	vytvořit	k5eAaPmAgMnP
předmostí	předmostí	k1gNnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Turci	Turek	k1gMnPc1
ustupovali	ustupovat	k5eAaImAgMnP
k	k	k7c3
Nikopoli	Nikopole	k1gFnSc3
<g/>
,	,	kIx,
Tǎ	Tǎ	k1gInSc3
a	a	k8xC
Vardimu	Vardim	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnPc1d1
ruské	ruský	k2eAgFnPc1d1
síly	síla	k1gFnPc1
byly	být	k5eAaImAgFnP
přesunuty	přesunout	k5eAaPmNgFnP
po	po	k7c6
pontonech	ponton	k1gInPc6
<g/>
,	,	kIx,
rozdělily	rozdělit	k5eAaPmAgFnP
se	se	k3xPyFc4
na	na	k7c4
předvoj	předvoj	k1gInSc4
generála	generál	k1gMnSc2
J.	J.	kA
V.	V.	kA
Gurka	Gurka	k1gFnSc1
s	s	k7c7
12	#num#	k4
000	#num#	k4
vojáky	voják	k1gMnPc7
a	a	k8xC
40	#num#	k4
děly	dělo	k1gNnPc7
<g/>
,	,	kIx,
západní	západní	k2eAgFnSc1d1
část	část	k1gFnSc1
vojska	vojsko	k1gNnSc2
s	s	k7c7
30	#num#	k4
000	#num#	k4
muži	muž	k1gMnPc7
a	a	k8xC
108	#num#	k4
děly	dělo	k1gNnPc7
<g/>
;	;	kIx,
a	a	k8xC
východní	východní	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
následníkem	následník	k1gMnSc7
Alexandrem	Alexandr	k1gMnSc7
Alexandrovičem	Alexandrovič	k1gMnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
tvořilo	tvořit	k5eAaImAgNnS
70	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
vybavených	vybavený	k2eAgMnPc2d1
246	#num#	k4
děly	dělo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
mířili	mířit	k5eAaImAgMnP
do	do	k7c2
prostoru	prostor	k1gInSc2
Ruse	ruse	k6eAd1
<g/>
–	–	k?
<g/>
Silistra	Silistra	k1gFnSc1
<g/>
–	–	k?
<g/>
Varna	Varna	k1gFnSc1
<g/>
–	–	k?
<g/>
Šumen	šumno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
<g/>
)	)	kIx)
1877	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
předvoj	předvoj	k1gInSc1
rychlého	rychlý	k2eAgInSc2d1
postupu	postup	k1gInSc2
a	a	k8xC
po	po	k7c6
krátkém	krátký	k2eAgInSc6d1
boji	boj	k1gInSc6
Veliko	Velika	k1gFnSc5
Tǎ	Tǎ	k2eAgNnSc4d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
jim	on	k3xPp3gMnPc3
pomohli	pomoct	k5eAaPmAgMnP
místní	místní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
nepozorovaným	pozorovaný	k2eNgNnSc7d1
navedením	navedení	k1gNnSc7
dragounů	dragoun	k1gInPc2
k	k	k7c3
tureckému	turecký	k2eAgInSc3d1
táboru	tábor	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
července	červenec	k1gInSc2
1877	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
na	na	k7c6
západním	západní	k2eAgNnSc6d1
křídle	křídlo	k1gNnSc6
bitva	bitva	k1gFnSc1
o	o	k7c4
Nikopol	Nikopol	k1gInSc4
<g/>
,	,	kIx,
po	po	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
byli	být	k5eAaImAgMnP
Turci	Turek	k1gMnPc1
rozprášeni	rozprášen	k2eAgMnPc1d1
<g/>
,	,	kIx,
Rusové	Rus	k1gMnPc1
pochytali	pochytat	k5eAaPmAgMnP
7	#num#	k4
000	#num#	k4
zajatců	zajatec	k1gMnPc2
a	a	k8xC
zmocnili	zmocnit	k5eAaPmAgMnP
se	se	k3xPyFc4
značné	značný	k2eAgFnPc4d1
materiální	materiální	k2eAgFnSc4d1
kořist	kořist	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Již	již	k6eAd1
7	#num#	k4
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
července	červenec	k1gInSc2
1877	#num#	k4
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
Gurka	Gurka	k1gMnSc1
(	(	kIx(
<g/>
jeho	jeho	k3xOp3gInSc1
předvoj	předvoj	k1gInSc1
se	se	k3xPyFc4
tehdy	tehdy	k6eAd1
ocitl	ocitnout	k5eAaPmAgInS
nekrytý	krytý	k2eNgInSc1d1
<g/>
,	,	kIx,
bez	bez	k7c2
příslušných	příslušný	k2eAgFnPc2d1
záloh	záloha	k1gFnPc2
<g/>
,	,	kIx,
poněvadž	poněvadž	k8xS
bylo	být	k5eAaImAgNnS
upuštěno	upustit	k5eAaPmNgNnS
od	od	k7c2
záměru	záměr	k1gInSc2
rychlého	rychlý	k2eAgInSc2d1
postupu	postup	k1gInSc2
na	na	k7c4
jih	jih	k1gInSc4
od	od	k7c2
Staré	Staré	k2eAgFnSc2d1
planiny	planina	k1gFnSc2
a	a	k8xC
na	na	k7c4
Cařihrad	Cařihrad	k1gInSc4
a	a	k8xC
ruské	ruský	k2eAgNnSc1d1
velení	velení	k1gNnSc1
se	s	k7c7
<g />
.	.	kIx.
</s>
<s hack="1">
přiklonilo	přiklonit	k5eAaPmAgNnS
k	k	k7c3
postupnému	postupný	k2eAgNnSc3d1
dobývání	dobývání	k1gNnSc3
pevností	pevnost	k1gFnPc2
a	a	k8xC
ohnisek	ohnisko	k1gNnPc2
odporu	odpor	k1gInSc2
<g/>
)	)	kIx)
až	až	k9
do	do	k7c2
okolí	okolí	k1gNnSc2
Karlova	Karlův	k2eAgNnSc2d1
(	(	kIx(
<g/>
Levskigrad	Levskigrada	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pak	pak	k6eAd1
obsadil	obsadit	k5eAaPmAgMnS
průsmyk	průsmyk	k1gInSc4
a	a	k8xC
postoupil	postoupit	k5eAaPmAgInS
ke	k	k7c3
Staré	Staré	k2eAgFnSc3d1
Zagoře	Zagora	k1gFnSc3
(	(	kIx(
<g/>
na	na	k7c4
tuto	tento	k3xDgFnSc4
událost	událost	k1gFnSc4
vzpomínal	vzpomínat	k5eAaImAgMnS
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
pamětech	paměť	k1gFnPc6
komunistický	komunistický	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
Dimităr	Dimităr	k1gMnSc1
Blagoev	Blagoev	k1gFnSc1
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
pak	pak	k6eAd1
na	na	k7c6
dojetí	dojetí	k1gNnSc6
děda	děd	k1gMnSc2
Slavejkova	Slavejkov	k1gInSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
bylo	být	k5eAaImAgNnS
vzbuzováno	vzbuzovat	k5eAaImNgNnS
dlouho	dlouho	k6eAd1
očekávaným	očekávaný	k2eAgNnSc7d1
osvobozením	osvobození	k1gNnSc7
Bulharska	Bulharsko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Již	již	k6eAd1
na	na	k7c6
počátku	počátek	k1gInSc6
války	válka	k1gFnSc2
byla	být	k5eAaImAgFnS
ustavena	ustavit	k5eAaPmNgFnS
při	při	k7c6
hlavním	hlavní	k2eAgNnSc6d1
velení	velení	k1gNnSc6
Dunajské	dunajský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
zvláštní	zvláštní	k2eAgFnSc4d1
kancelář	kancelář	k1gFnSc4
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
organizovat	organizovat	k5eAaBmF
na	na	k7c6
osvobozeném	osvobozený	k2eAgNnSc6d1
území	území	k1gNnSc6
civilní	civilní	k2eAgFnSc4d1
správu	správa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
jejího	její	k3xOp3gNnSc2
čela	čelo	k1gNnSc2
byl	být	k5eAaImAgMnS
jmenován	jmenován	k2eAgMnSc1d1
Vladimír	Vladimír	k1gMnSc1
Alexandrovič	Alexandrovič	k1gMnSc1
Čerkasský	Čerkasský	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Směřoval	směřovat	k5eAaImAgMnS
většinu	většina	k1gFnSc4
svých	svůj	k3xOyFgInPc2
kroků	krok	k1gInPc2
k	k	k7c3
zachování	zachování	k1gNnSc3
starého	starý	k2eAgNnSc2d1
tureckého	turecký	k2eAgNnSc2d1
zřízení	zřízení	k1gNnSc2
<g/>
,	,	kIx,
spolupracoval	spolupracovat	k5eAaImAgInS
hlavně	hlavně	k9
s	s	k7c7
rusofilskými	rusofilský	k2eAgMnPc7d1
bulharskými	bulharský	k2eAgMnPc7d1
velkostatkáři	velkostatkář	k1gMnPc7
<g/>
,	,	kIx,
snažil	snažit	k5eAaImAgMnS
se	se	k3xPyFc4
vyhnout	vyhnout	k5eAaPmF
všem	všecek	k3xTgInPc3
nestabilním	stabilní	k2eNgInPc3d1
lidovým	lidový	k2eAgInPc3d1
prvkům	prvek	k1gInPc3
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
byl	být	k5eAaImAgInS
rozpuštěn	rozpuštěn	k2eAgInSc4d1
BRÚV	BRÚV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývalé	bývalý	k2eAgInPc1d1
sandžaky	sandžak	k1gInPc1
byly	být	k5eAaImAgInP
přejmenovány	přejmenovat	k5eAaPmNgInP
na	na	k7c4
gubernie	gubernie	k1gFnPc4
(	(	kIx(
<g/>
vedené	vedený	k2eAgMnPc4d1
gubernátory	gubernátor	k1gMnPc4
<g/>
)	)	kIx)
–	–	k?
první	první	k4xOgFnSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
Svištov	Svištov	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
jejím	její	k3xOp3gNnSc6
čele	čelo	k1gNnSc6
stanul	stanout	k5eAaPmAgMnS
Najden	Najdna	k1gFnPc2
Genov	Genov	k1gInSc1
<g/>
,	,	kIx,
ruský	ruský	k2eAgMnSc1d1
vicekonzul	vicekonzul	k1gMnSc1
v	v	k7c6
Plovdivu	Plovdiv	k1gInSc6
bulharského	bulharský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
létě	léto	k1gNnSc6
1878	#num#	k4
se	se	k3xPyFc4
odtud	odtud	k6eAd1
prozatímní	prozatímní	k2eAgFnSc1d1
ruská	ruský	k2eAgFnSc1d1
správa	správa	k1gFnSc1
přemístila	přemístit	k5eAaPmAgFnS
do	do	k7c2
Plovdivu	Plovdiv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
daní	daň	k1gFnPc2
nechal	nechat	k5eAaPmAgMnS
odstranit	odstranit	k5eAaPmF
kníže	kníže	k1gMnSc1
Čerkasský	Čerkasský	k1gMnSc1
pouze	pouze	k6eAd1
bedel	bedla	k1gFnPc2
(	(	kIx(
<g/>
daň	daň	k1gFnSc4
z	z	k7c2
mužů	muž	k1gMnPc2
–	–	k?
za	za	k7c2
zproštění	zproštění	k1gNnSc2
vojenské	vojenský	k2eAgFnSc2d1
povinnosti	povinnost	k1gFnSc2
křesťanů	křesťan	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
naturální	naturální	k2eAgFnSc4d1
daň	daň	k1gFnSc4
nahradil	nahradit	k5eAaPmAgInS
peněžní	peněžní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
smrti	smrt	k1gFnSc6
Čerkasského	Čerkasský	k2eAgMnSc2d1
knížete	kníže	k1gMnSc2
stanul	stanout	k5eAaPmAgInS
v	v	k7c6
čele	čelo	k1gNnSc6
správy	správa	k1gFnSc2
Dondukov-Korsakov	Dondukov-Korsakov	k1gInSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
nejvyšší	vysoký	k2eAgFnSc1d3
výkonná	výkonný	k2eAgFnSc1d1
moc	moc	k1gFnSc1
ve	v	k7c6
Východní	východní	k2eAgFnSc6d1
Rumélii	Rumélie	k1gFnSc6
byla	být	k5eAaImAgFnS
svěřena	svěřen	k2eAgFnSc1d1
generálu	generál	k1gMnSc3
Alexandru	Alexandr	k1gMnSc3
Dmitrijeviči	Dmitrijevič	k1gMnSc3
Stolypinovi	Stolypin	k1gMnSc3
(	(	kIx(
<g/>
říjen	říjen	k1gInSc1
1878	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
byl	být	k5eAaImAgMnS
značně	značně	k6eAd1
oblíben	oblíbit	k5eAaPmNgMnS
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
staral	starat	k5eAaImAgMnS
o	o	k7c4
péči	péče	k1gFnSc4
o	o	k7c4
uprchlíky	uprchlík	k1gMnPc4
z	z	k7c2
Makedonie	Makedonie	k1gFnSc2
a	a	k8xC
Thrákie	Thrákie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
bitvy	bitva	k1gFnSc2
u	u	k7c2
Staré	Staré	k2eAgFnSc2d1
Zagory	Zagora	k1gFnSc2
a	a	k8xC
bitvy	bitva	k1gFnSc2
v	v	k7c6
průsmyku	průsmyk	k1gInSc6
Šipka	šipka	k1gFnSc1
až	až	k8xS
po	po	k7c4
dobytí	dobytí	k1gNnSc4
Plevna	Plevno	k1gNnSc2
</s>
<s>
Velení	velení	k1gNnSc1
Turků	Turek	k1gMnPc2
po	po	k7c6
prvních	první	k4xOgInPc6
úspěších	úspěch	k1gInPc6
Ruska	Rusko	k1gNnSc2
rozhodlo	rozhodnout	k5eAaPmAgNnS
rychle	rychle	k6eAd1
odvolat	odvolat	k5eAaPmF
Sulejmana	Sulejman	k1gMnSc4
pašu	paša	k1gMnSc4
a	a	k8xC
nejlepší	dobrý	k2eAgFnPc4d3
jednotky	jednotka	k1gFnPc4
z	z	k7c2
Černé	Černé	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
a	a	k8xC
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
nejrychleji	rychle	k6eAd3
je	být	k5eAaImIp3nS
přepravit	přepravit	k5eAaPmF
pomocí	pomocí	k7c2
britského	britský	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
do	do	k7c2
Bulharska	Bulharsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
síla	síla	k1gFnSc1
o	o	k7c4
40	#num#	k4
000	#num#	k4
mužích	muž	k1gMnPc6
(	(	kIx(
<g/>
spolu	spolu	k6eAd1
s	s	k7c7
bašibozuky	bašibozuk	k1gMnPc7
<g/>
)	)	kIx)
pak	pak	k6eAd1
ihned	ihned	k6eAd1
táhla	táhnout	k5eAaImAgFnS
ke	k	k7c3
Staré	Staré	k2eAgFnSc3d1
Zagoře	Zagora	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
31	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
července	červenec	k1gInSc2
1877	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
bitva	bitva	k1gFnSc1
u	u	k7c2
Staré	Staré	k2eAgFnSc2d1
Zagory	Zagora	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
3500	#num#	k4
Rusů	Rus	k1gMnPc2
a	a	k8xC
opolčenců	opolčenec	k1gMnPc2
čelilo	čelit	k5eAaImAgNnS
přesile	přesila	k1gFnSc6
15	#num#	k4
000	#num#	k4
Turků	Turek	k1gMnPc2
<g/>
,	,	kIx,
Rusům	Rus	k1gMnPc3
pomáhali	pomáhat	k5eAaImAgMnP
také	také	k9
místní	místní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generál	generál	k1gMnSc1
Gurko	Gurka	k1gFnSc5
jim	on	k3xPp3gMnPc3
vyrazil	vyrazit	k5eAaPmAgMnS
na	na	k7c6
pomoc	pomoc	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
přišel	přijít	k5eAaPmAgMnS
pozdě	pozdě	k6eAd1
<g/>
,	,	kIx,
neboť	neboť	k8xC
musel	muset	k5eAaImAgMnS
nejprve	nejprve	k6eAd1
u	u	k7c2
Džuranli	Džuranl	k1gMnPc7
<g/>
(	(	kIx(
<g/>
Kalitinovo	Kalitinův	k2eAgNnSc4d1
<g/>
)	)	kIx)
porazit	porazit	k5eAaPmF
Reuf	Reuf	k1gInSc4
pašu	paša	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tu	ten	k3xDgFnSc4
chvíli	chvíle	k1gFnSc4
již	již	k6eAd1
byla	být	k5eAaImAgFnS
Stará	starý	k2eAgFnSc1d1
i	i	k8xC
Nová	nový	k2eAgFnSc1d1
Zagora	Zagora	k1gFnSc1
a	a	k8xC
blízké	blízký	k2eAgFnPc1d1
vesnice	vesnice	k1gFnPc1
vypáleny	vypálen	k2eAgFnPc1d1
<g/>
,	,	kIx,
taktéž	taktéž	k?
Kazanlăk	Kazanlăk	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
14	#num#	k4
500	#num#	k4
lidí	člověk	k1gMnPc2
pobito	pobít	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obrana	obrana	k1gFnSc1
průsmyků	průsmyk	k1gInPc2
přes	přes	k7c4
pohoří	pohoří	k1gNnSc4
Balkán	Balkán	k1gInSc1
byla	být	k5eAaImAgFnS
svěřena	svěřit	k5eAaPmNgFnS
VIII	VIII	kA
<g/>
.	.	kIx.
sboru	sbor	k1gInSc2
generála	generál	k1gMnSc2
Radeckého	Radecký	k1gMnSc2
a	a	k8xC
části	část	k1gFnSc2
Gurkových	Gurkův	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
dnech	den	k1gInPc6
9	#num#	k4
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
srpna	srpen	k1gInSc2
–	–	k?
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
srpna	srpen	k1gInSc2
probíhala	probíhat	k5eAaImAgFnS
obrana	obrana	k1gFnSc1
průsmyku	průsmyk	k1gInSc2
Šipky	šipka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rus	k1gMnPc1
měli	mít	k5eAaImAgMnP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
5	#num#	k4
družin	družina	k1gFnPc2
opolčenců	opolčenec	k1gMnPc2
<g/>
,	,	kIx,
Orlovský	orlovský	k2eAgInSc4d1
pluk	pluk	k1gInSc4
a	a	k8xC
27	#num#	k4
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ač	ač	k8xS
byli	být	k5eAaImAgMnP
později	pozdě	k6eAd2
posíleni	posílen	k2eAgMnPc1d1
Brjanským	Brjanský	k2eAgInSc7d1
plukem	pluk	k1gInSc7
tak	tak	k9
jejich	jejich	k3xOp3gFnSc2
síly	síla	k1gFnSc2
tvořilo	tvořit	k5eAaImAgNnS
jen	jen	k9
cca	cca	kA
5500	#num#	k4
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velel	velet	k5eAaImAgMnS
jim	on	k3xPp3gMnPc3
generál	generál	k1gMnSc1
N.	N.	kA
G.	G.	kA
Stoletov	Stoletov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
němu	on	k3xPp3gMnSc3
táhl	táhnout	k5eAaImAgMnS
Sulejman	Sulejman	k1gMnSc1
paša	paša	k1gMnSc1
s	s	k7c7
27	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
a	a	k8xC
s	s	k7c7
42	#num#	k4
děly	dělo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
vyvrcholení	vyvrcholení	k1gNnSc3
bojů	boj	k1gInPc2
o	o	k7c4
tzv.	tzv.	kA
Orlí	orlí	k2eAgNnSc1d1
hnízdo	hnízdo	k1gNnSc1
na	na	k7c6
hoře	hora	k1gFnSc6
sv.	sv.	kA
Mikuláše	Mikuláš	k1gMnSc2
(	(	kIx(
<g/>
později	pozdě	k6eAd2
přejmenované	přejmenovaný	k2eAgInPc1d1
na	na	k7c4
vrch	vrch	k1gInSc4
Stoletov	Stoletov	k1gInSc4
<g/>
)	)	kIx)
došlo	dojít	k5eAaPmAgNnS
10	#num#	k4
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
srpna	srpen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konec	konec	k1gInSc1
obrany	obrana	k1gFnSc2
nastal	nastat	k5eAaPmAgInS
pátý	pátý	k4xOgInSc1
dnem	dnem	k7c2
bojů	boj	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
Rusům	Rus	k1gMnPc3
povedl	povést	k5eAaPmAgMnS
výpad	výpad	k1gInSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
odradil	odradit	k5eAaPmAgInS
Turky	Turek	k1gMnPc4
od	od	k7c2
dalších	další	k2eAgInPc2d1
útoků	útok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruské	ruský	k2eAgFnSc2d1
ztráty	ztráta	k1gFnSc2
dosáhly	dosáhnout	k5eAaPmAgFnP
3773	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
turecké	turecký	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
o	o	k7c4
dost	dost	k6eAd1
vyšší	vysoký	k2eAgFnSc4d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgNnSc7
vítězstvím	vítězství	k1gNnSc7
byla	být	k5eAaImAgFnS
zmařena	zmařen	k2eAgFnSc1d1
snaha	snaha	k1gFnSc1
Sulejmana	Sulejman	k1gMnSc2
paši	paša	k1gMnSc2
posílit	posílit	k5eAaPmF
posádky	posádka	k1gFnPc4
v	v	k7c6
severním	severní	k2eAgNnSc6d1
Bulharsku	Bulharsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Český	český	k2eAgMnSc1d1
inženýr	inženýr	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Prošek	Prošek	k1gMnSc1
tehdy	tehdy	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c6
stavbě	stavba	k1gFnSc6
železniční	železniční	k2eAgFnSc2d1
tratě	trať	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
měla	mít	k5eAaImAgFnS
využít	využít	k5eAaPmF
turecká	turecký	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
přítel	přítel	k1gMnSc1
PhMr	PhMr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nádherný	nádherný	k2eAgInSc4d1
(	(	kIx(
<g/>
sofijský	sofijský	k2eAgMnSc1d1
starousedlík	starousedlík	k1gMnSc1
s	s	k7c7
tureckým	turecký	k2eAgNnSc7d1
občanstvím	občanství	k1gNnSc7
<g/>
,	,	kIx,
musel	muset	k5eAaImAgInS
narukovat	narukovat	k5eAaPmF
do	do	k7c2
zdravotnické	zdravotnický	k2eAgFnSc2d1
služby	služba	k1gFnSc2
tureckého	turecký	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
působil	působit	k5eAaImAgMnS
jako	jako	k8xC,k8xS
štábní	štábní	k2eAgMnSc1d1
hekim-paša	hekim-paša	k1gMnSc1
/	/	kIx~
<g/>
lékař	lékař	k1gMnSc1
<g/>
/	/	kIx~
<g/>
)	)	kIx)
měl	mít	k5eAaImAgInS
značný	značný	k2eAgInSc4d1
přístup	přístup	k1gInSc4
k	k	k7c3
velení	velení	k1gNnSc3
v	v	k7c6
Šumenu	Šumeno	k1gNnSc6
a	a	k8xC
zajišťoval	zajišťovat	k5eAaImAgMnS
proto	proto	k8xC
zpravodajství	zpravodajství	k1gNnSc1
po	po	k7c6
tajných	tajný	k2eAgMnPc6d1
kurýrech	kurýr	k1gMnPc6
ruskému	ruský	k2eAgMnSc3d1
velení	velení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
získal	získat	k5eAaPmAgInS
přesné	přesný	k2eAgInPc4d1
údaje	údaj	k1gInPc4
o	o	k7c6
přepravě	přeprava	k1gFnSc6
tureckých	turecký	k2eAgFnPc2d1
posil	posila	k1gFnPc2
na	na	k7c4
sever	sever	k1gInSc4
<g/>
,	,	kIx,
měly	mít	k5eAaImAgInP
být	být	k5eAaImF
přepraveny	přepravit	k5eAaPmNgInP
částečně	částečně	k6eAd1
po	po	k7c6
dráze	dráha	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
ihned	ihned	k6eAd1
uvědomil	uvědomit	k5eAaPmAgMnS
Ing.	ing.	kA
Jiřího	Jiří	k1gMnSc2
Proška	Prošek	k1gMnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc3
dělníci	dělník	k1gMnPc1
pak	pak	k6eAd1
během	během	k7c2
vyvrcholení	vyvrcholení	k1gNnSc2
bojů	boj	k1gInPc2
v	v	k7c6
Šipce	šipka	k1gFnSc6
rozmontovali	rozmontovat	k5eAaPmAgMnP
trať	trať	k1gFnSc4
nad	nad	k7c7
Odrinem	Odrin	k1gInSc7
a	a	k8xC
zabarikádovali	zabarikádovat	k5eAaPmAgMnP
ji	on	k3xPp3gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
hrozby	hrozba	k1gFnPc4
Turků	turek	k1gInPc2
pozdrželi	pozdržet	k5eAaPmAgMnP
posily	posila	k1gFnSc2
skoro	skoro	k6eAd1
o	o	k7c4
30	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
Proškova	Proškův	k2eAgFnSc1d1
statečnost	statečnost	k1gFnSc1
tak	tak	k6eAd1
tehdy	tehdy	k6eAd1
přispěla	přispět	k5eAaPmAgFnS
k	k	k7c3
vítězství	vítězství	k1gNnSc3
Rusů	Rus	k1gMnPc2
při	při	k7c6
obraně	obrana	k1gFnSc6
Šipky	šipka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bulhaři	Bulhar	k1gMnPc1
organizovali	organizovat	k5eAaBmAgMnP
ozbrojené	ozbrojený	k2eAgFnPc4d1
družiny	družina	k1gFnPc4
v	v	k7c6
tureckém	turecký	k2eAgInSc6d1
týlu	týl	k1gInSc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
působily	působit	k5eAaImAgFnP
v	v	k7c6
pohoří	pohoří	k1gNnSc6
Rodopy	Rodopa	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
okolí	okolí	k1gNnSc6
Gjumjurdžiny	Gjumjurdžina	k1gFnSc2
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc2d1
Komotini	Komotin	k2eAgMnPc1d1
v	v	k7c6
Západní	západní	k2eAgFnSc6d1
Thrákii	Thrákie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
snažili	snažit	k5eAaImAgMnP
se	se	k3xPyFc4
vytvářet	vytvářet	k5eAaImF
milice	milice	k1gFnPc4
na	na	k7c6
osvobozeném	osvobozený	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
by	by	kYmCp3nP
chránily	chránit	k5eAaImAgFnP
bulharské	bulharský	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
proti	proti	k7c3
útokům	útok	k1gInPc3
bašibozuků	bašibozuk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
<g/>
,	,	kIx,
z	z	k7c2
něhož	jenž	k3xRgMnSc2
Rusové	Rus	k1gMnPc1
ustoupili	ustoupit	k5eAaPmAgMnP
<g/>
,	,	kIx,
totiž	totiž	k9
začaly	začít	k5eAaPmAgFnP
probíhat	probíhat	k5eAaImF
rozsáhlé	rozsáhlý	k2eAgFnPc4d1
turecké	turecký	k2eAgFnPc4d1
represálie	represálie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
v	v	k7c6
jižním	jižní	k2eAgNnSc6d1
Bulharsku	Bulharsko	k1gNnSc6
bylo	být	k5eAaImAgNnS
pobito	pobít	k5eAaPmNgNnS
18	#num#	k4
000	#num#	k4
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
vypáleno	vypálen	k2eAgNnSc4d1
40	#num#	k4
860	#num#	k4
obydlí	obydlí	k1gNnPc2
<g/>
,	,	kIx,
925	#num#	k4
kostelů	kostel	k1gInPc2
a	a	k8xC
120	#num#	k4
škol	škola	k1gFnPc2
(	(	kIx(
<g/>
hlavně	hlavně	k9
v	v	k7c6
okolí	okolí	k1gNnSc6
Plovdivu	Plovdiv	k1gInSc2
a	a	k8xC
Odrinu	Odrin	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Cařihradu	Cařihrad	k1gInSc3
propukla	propuknout	k5eAaPmAgFnS
téměř	téměř	k6eAd1
„	„	k?
<g/>
bartolomějská	bartolomějský	k2eAgFnSc1d1
noc	noc	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
zmizel	zmizet	k5eAaPmAgInS
značný	značný	k2eAgInSc1d1
počet	počet	k1gInSc1
Bulharů	Bulhar	k1gMnPc2
<g/>
,	,	kIx,
mnozí	mnohý	k2eAgMnPc1d1
byli	být	k5eAaImAgMnP
povražděni	povraždit	k5eAaPmNgMnP
a	a	k8xC
jejich	jejich	k3xOp3gFnPc1
ženy	žena	k1gFnPc1
byly	být	k5eAaImAgFnP
unášeny	unášen	k2eAgFnPc1d1
a	a	k8xC
znásilňovány	znásilňován	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvěrstva	zvěrstvo	k1gNnSc2
dokonce	dokonce	k9
převyšovala	převyšovat	k5eAaImAgFnS
i	i	k9
odvetu	odveta	k1gFnSc4
za	za	k7c4
Dubnové	dubnový	k2eAgNnSc4d1
povstání	povstání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zločiny	zločin	k1gInPc1
byly	být	k5eAaImAgInP
konány	konat	k5eAaImNgInP
i	i	k9
v	v	k7c6
přítomnosti	přítomnost	k1gFnSc6
anglických	anglický	k2eAgMnPc2d1
instruktorů	instruktor	k1gMnPc2
tureckého	turecký	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
byl	být	k5eAaImAgMnS
jistý	jistý	k2eAgMnSc1d1
major	major	k1gMnSc1
St.	st.	kA
Clair	Clair	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruští	ruský	k2eAgMnPc1d1
zajatci	zajatec	k1gMnPc1
byli	být	k5eAaImAgMnP
na	na	k7c6
místě	místo	k1gNnSc6
pobíjeni	pobíjet	k5eAaImNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takové	takový	k3xDgNnSc1
jednání	jednání	k1gNnSc1
pak	pak	k6eAd1
mělo	mít	k5eAaImAgNnS
zákonitou	zákonitý	k2eAgFnSc4d1
odezvu	odezva	k1gFnSc4
i	i	k9
naopak	naopak	k6eAd1
na	na	k7c6
území	území	k1gNnSc6
obsazeném	obsazený	k2eAgNnSc6d1
opačnou	opačný	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Plevno	Plevno	k1gNnSc4
držel	držet	k5eAaImAgMnS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejschopnějších	schopný	k2eAgMnPc2d3
tureckých	turecký	k2eAgMnPc2d1
velitelů	velitel	k1gMnPc2
<g/>
,	,	kIx,
Osman	Osman	k1gMnSc1
paša	paša	k1gMnSc1
<g/>
,	,	kIx,
nejprve	nejprve	k6eAd1
proti	proti	k7c3
němu	on	k3xPp3gInSc3
vyrazilo	vyrazit	k5eAaPmAgNnS
západní	západní	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
Rusů	Rus	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
července	červenec	k1gInSc2
podnikla	podniknout	k5eAaPmAgFnS
útok	útok	k1gInSc4
divize	divize	k1gFnSc2
generála	generál	k1gMnSc2
Schilder-Schuldnera	Schilder-Schuldner	k1gMnSc2
o	o	k7c4
9	#num#	k4
000	#num#	k4
vojácích	voják	k1gMnPc6
<g/>
,	,	kIx,
avšak	avšak	k8xC
bez	bez	k7c2
řádného	řádný	k2eAgInSc2d1
průzkumu	průzkum	k1gInSc2
<g/>
,	,	kIx,
Schilder-Schuldner	Schilder-Schuldner	k1gInSc1
neznal	neznat	k5eAaImAgInS,k5eNaImAgInS
počet	počet	k1gInSc4
Turků	Turek	k1gMnPc2
(	(	kIx(
<g/>
15	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
doplněných	doplněný	k2eAgInPc2d1
početným	početný	k2eAgNnSc7d1
dělostřelectvem	dělostřelectvo	k1gNnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
Rusům	Rus	k1gMnPc3
připravili	připravit	k5eAaPmAgMnP
krutou	krutý	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
v	v	k7c6
okolí	okolí	k1gNnSc6
Bukovlăku	Bukovlăk	k1gInSc2
<g/>
,	,	kIx,
Janăkbairu	Janăkbair	k1gInSc2
a	a	k8xC
Grivice	Grivice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
započalo	započnout	k5eAaPmAgNnS
obléhání	obléhání	k1gNnSc1
Plevna	Plevno	k1gNnSc2
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgInSc3
obě	dva	k4xCgFnPc1
síly	síla	k1gFnPc1
dopravily	dopravit	k5eAaPmAgFnP
postupně	postupně	k6eAd1
velké	velký	k2eAgInPc1d1
počty	počet	k1gInPc1
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
30	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
července	červenec	k1gInSc2
byl	být	k5eAaImAgInS
připraven	připravit	k5eAaPmNgInS
opětovný	opětovný	k2eAgInSc1d1
útok	útok	k1gInSc1
Rusů	Rus	k1gMnPc2
proti	proti	k7c3
posíleným	posílený	k2eAgInPc3d1
Turkům	turek	k1gInPc3
(	(	kIx(
<g/>
24	#num#	k4
000	#num#	k4
+	+	kIx~
56	#num#	k4
děl	dělo	k1gNnPc2
a	a	k8xC
zesílené	zesílený	k2eAgNnSc1d1
opevnění	opevnění	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
však	však	k9
vinou	vina	k1gFnSc7
velitelské	velitelský	k2eAgFnSc2d1
neschopnosti	neschopnost	k1gFnSc2
generála	generál	k1gMnSc2
N.	N.	kA
P.	P.	kA
Krüdenera	Krüdener	k1gMnSc2
neuspěl	uspět	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
tomuto	tento	k3xDgInSc3
neúspěchu	neúspěch	k1gInSc3
musel	muset	k5eAaImAgInS
být	být	k5eAaImF
zastaven	zastavit	k5eAaPmNgInS
ruský	ruský	k2eAgInSc1d1
postup	postup	k1gInSc1
na	na	k7c4
jih	jih	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
třetím	třetí	k4xOgInSc6
útoku	útok	k1gInSc6
na	na	k7c4
Plevno	Plevno	k1gNnSc4
29	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
31	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
společně	společně	k6eAd1
s	s	k7c7
vojskem	vojsko	k1gNnSc7
Rumunů	Rumun	k1gMnPc2
<g/>
,	,	kIx,
pronikl	proniknout	k5eAaPmAgInS
generál	generál	k1gMnSc1
M.	M.	kA
D.	D.	kA
Skobelev	Skobelev	k1gMnSc1
až	až	k6eAd1
na	na	k7c6
předměstí	předměstí	k1gNnSc6
Plevna	Plevno	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
tvrdošíjný	tvrdošíjný	k2eAgInSc1d1
odpor	odpor	k1gInSc1
Osmana	Osman	k1gMnSc2
paši	paša	k1gMnSc2
jeho	jeho	k3xOp3gMnSc1
postup	postup	k1gInSc4
zastavil	zastavit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
útok	útok	k1gInSc1
byl	být	k5eAaImAgInS
nejkrvavější	krvavý	k2eAgFnSc7d3
bitvou	bitva	k1gFnSc7
celé	celý	k2eAgFnSc2d1
války	válka	k1gFnSc2
–	–	k?
Rusové	Rus	k1gMnPc1
byli	být	k5eAaImAgMnP
odraženi	odražen	k2eAgMnPc1d1
se	s	k7c7
ztrátou	ztráta	k1gFnSc7
13	#num#	k4
000	#num#	k4
Rusů	Rus	k1gMnPc2
a	a	k8xC
3	#num#	k4
000	#num#	k4
Rumunů	Rumun	k1gMnPc2
během	během	k7c2
5	#num#	k4
dnů	den	k1gInPc2
bojů	boj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
neúspěchů	neúspěch	k1gInPc2
bylo	být	k5eAaImAgNnS
přistoupeno	přistoupen	k2eAgNnSc1d1
k	k	k7c3
uzavření	uzavření	k1gNnSc3
Plevna	Plevno	k1gNnSc2
v	v	k7c6
neproniknutelném	proniknutelný	k2eNgNnSc6d1
obležení	obležení	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yQgNnSc4,k3yIgNnSc4
řídil	řídit	k5eAaImAgMnS
generál	generál	k1gMnSc1
E.	E.	kA
I.	I.	kA
Totleben	Totleben	k2eAgMnSc1d1
<g/>
,	,	kIx,
proslulý	proslulý	k2eAgMnSc1d1
ženijní	ženijní	k2eAgMnSc1d1
odborník	odborník	k1gMnSc1
krymské	krymský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
držení	držení	k1gNnSc6
obklíčení	obklíčení	k1gNnSc2
proběhly	proběhnout	k5eAaPmAgFnP
boje	boj	k1gInPc4
v	v	k7c6
Grivici	Grivice	k1gFnSc6
<g/>
,	,	kIx,
Gorném	Gorný	k2eAgInSc6d1
Dăbniku	Dăbnik	k1gInSc6
<g/>
,	,	kIx,
Dolném	Dolný	k2eAgInSc6d1
Dăbniku	Dăbnik	k1gInSc6
<g/>
,	,	kIx,
Teliši	Teliše	k1gFnSc4
ad	ad	k7c4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turci	Turek	k1gMnPc1
byli	být	k5eAaImAgMnP
při	při	k7c6
pokusech	pokus	k1gInPc6
zabezpečit	zabezpečit	k5eAaPmF
zásobovací	zásobovací	k2eAgFnPc1d1
linie	linie	k1gFnPc1
do	do	k7c2
obklíčeného	obklíčený	k2eAgNnSc2d1
města	město	k1gNnSc2
poraženi	porazit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
zorganizovali	zorganizovat	k5eAaPmAgMnP
armádu	armáda	k1gFnSc4
52	#num#	k4
000	#num#	k4
Turků	Turek	k1gMnPc2
v	v	k7c6
okolí	okolí	k1gNnSc6
Sofie	Sofia	k1gFnSc2
a	a	k8xC
Ochranu	ochrana	k1gFnSc4
(	(	kIx(
<g/>
Botevgrad	Botevgrad	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
pokusili	pokusit	k5eAaPmAgMnP
se	se	k3xPyFc4
Plevno	Plevno	k1gNnSc4
vysvobodit	vysvobodit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rusová	k1gFnSc6
se	se	k3xPyFc4
tehdy	tehdy	k6eAd1
rozdělili	rozdělit	k5eAaPmAgMnP
na	na	k7c4
2	#num#	k4
částí	část	k1gFnPc2
<g/>
:	:	kIx,
obléhací	obléhací	k2eAgInSc4d1
(	(	kIx(
<g/>
Totleben	Totleben	k2eAgInSc4d1
<g/>
)	)	kIx)
a	a	k8xC
obrannou	obranný	k2eAgFnSc4d1
(	(	kIx(
<g/>
té	ten	k3xDgFnSc2
velel	velet	k5eAaImAgInS
Gurko	Gurko	k1gNnSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
proti	proti	k7c3
Turkům	Turek	k1gMnPc3
vytáhl	vytáhnout	k5eAaPmAgMnS
a	a	k8xC
uvolnil	uvolnit	k5eAaPmAgMnS
cesty	cesta	k1gFnSc2
ke	k	k7c3
Zlatici	zlatice	k1gFnSc3
a	a	k8xC
Sofii	Sofia	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavení	postavení	k1gNnSc1
plevenské	plevenský	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
se	se	k3xPyFc4
zhoršilo	zhoršit	k5eAaPmAgNnS
(	(	kIx(
<g/>
„	„	k?
<g/>
Pád	Pád	k1gInSc1
Plevna	Plevna	k1gFnSc1
bude	být	k5eAaImBp3nS
pro	pro	k7c4
naši	náš	k3xOp1gFnSc4
zem	zem	k1gFnSc4
velkým	velký	k2eAgNnSc7d1
neštěstím	neštěstí	k1gNnSc7
<g/>
,	,	kIx,
<g/>
“	“	k?
psal	psát	k5eAaImAgInS
tehdy	tehdy	k6eAd1
Disraeli	Disrael	k1gInPc7
královně	královna	k1gFnSc3
Viktorii	Viktoria	k1gFnSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nedostávalo	dostávat	k5eNaImAgNnS
se	se	k3xPyFc4
munice	munice	k1gFnSc2
a	a	k8xC
jiných	jiný	k2eAgFnPc2d1
zásob	zásoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
noci	noc	k1gFnSc6
na	na	k7c4
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
)	)	kIx)
1877	#num#	k4
se	se	k3xPyFc4
Turci	Turek	k1gMnPc1
snažili	snažit	k5eAaImAgMnP
o	o	k7c4
únik	únik	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
tehdy	tehdy	k6eAd1
bylo	být	k5eAaImAgNnS
již	již	k6eAd1
příliš	příliš	k6eAd1
pozdě	pozdě	k6eAd1
<g/>
,	,	kIx,
neboť	neboť	k8xC
ruské	ruský	k2eAgNnSc1d1
postavení	postavení	k1gNnSc1
bylo	být	k5eAaImAgNnS
velice	velice	k6eAd1
silné	silný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osmanské	osmanský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
se	se	k3xPyFc4
pokusilo	pokusit	k5eAaPmAgNnS
využít	využít	k5eAaPmF
mlhy	mlha	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
byli	být	k5eAaImAgMnP
zpozorováni	zpozorovat	k5eAaPmNgMnP
a	a	k8xC
odhaleni	odhalit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rus	k1gMnPc1
zaútočili	zaútočit	k5eAaPmAgMnP
na	na	k7c4
Turky	Turek	k1gMnPc4
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
brodili	brodit	k5eAaImAgMnP
přes	přes	k7c4
řeku	řeka	k1gFnSc4
Vit	vit	k2eAgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
způsobili	způsobit	k5eAaPmAgMnP
jim	on	k3xPp3gMnPc3
těžkou	těžký	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
Turci	Turek	k1gMnPc1
ztratili	ztratit	k5eAaPmAgMnP
5	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
a	a	k8xC
utekli	utéct	k5eAaPmAgMnP
v	v	k7c6
panice	panika	k1gFnSc6
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
zdálo	zdát	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
raněný	raněný	k1gMnSc1
Osman	Osman	k1gMnSc1
paša	paša	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
nato	nato	k6eAd1
proběhla	proběhnout	k5eAaPmAgFnS
kapitulace	kapitulace	k1gFnSc1
<g/>
,	,	kIx,
po	po	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
dostalo	dostat	k5eAaPmAgNnS
do	do	k7c2
zajetí	zajetí	k1gNnSc2
43	#num#	k4
338	#num#	k4
Turků	Turek	k1gMnPc2
a	a	k8xC
Rusové	Rus	k1gMnPc1
se	se	k3xPyFc4
zmocnili	zmocnit	k5eAaPmAgMnP
značné	značný	k2eAgFnPc4d1
kořisti	kořist	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitulací	kapitulace	k1gFnPc2
Plevna	Plevna	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
2	#num#	k4
<g/>
.	.	kIx.
fáze	fáze	k1gFnSc2
války	válka	k1gFnSc2
a	a	k8xC
rozběhly	rozběhnout	k5eAaPmAgFnP
se	se	k3xPyFc4
operace	operace	k1gFnSc2
vedoucí	vedoucí	k1gFnSc2
k	k	k7c3
osvobození	osvobození	k1gNnSc3
celého	celý	k2eAgNnSc2d1
Bulharska	Bulharsko	k1gNnSc2
a	a	k8xC
ohrožení	ohrožení	k1gNnSc2
Cařihradu	Cařihrad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Třetí	třetí	k4xOgFnSc1
fáze	fáze	k1gFnSc1
války	válka	k1gFnSc2
od	od	k7c2
dobytí	dobytí	k1gNnSc2
Plevna	Plevno	k1gNnSc2
až	až	k8xS
po	po	k7c4
Sanstefanský	Sanstefanský	k2eAgInSc4d1
mír	mír	k1gInSc4
</s>
<s>
Turecká	turecký	k2eAgFnSc1d1
kapitulace	kapitulace	k1gFnSc1
u	u	k7c2
Nikopole	Nikopole	k1gFnSc2
</s>
<s>
Pád	Pád	k1gInSc1
Plevna	Plevno	k1gNnSc2
znamenal	znamenat	k5eAaImAgInS
výrazný	výrazný	k2eAgInSc1d1
pokles	pokles	k1gInSc1
prestiže	prestiž	k1gFnSc2
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
národy	národ	k1gInPc1
pod	pod	k7c7
osmanským	osmanský	k2eAgNnSc7d1
područím	područí	k1gNnSc7
se	se	k3xPyFc4
chopily	chopit	k5eAaPmAgFnP
šance	šance	k1gFnPc1
k	k	k7c3
osvobození	osvobození	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
prosince	prosinec	k1gInSc2
<g/>
,	,	kIx,
několik	několik	k4yIc4
dní	den	k1gInPc2
po	po	k7c6
pádu	pád	k1gInSc6
Plevna	Plevno	k1gNnSc2
vstoupilo	vstoupit	k5eAaPmAgNnS
do	do	k7c2
války	válka	k1gFnSc2
Srbsko	Srbsko	k1gNnSc1
<g/>
,	,	kIx,
podpořené	podpořený	k2eAgNnSc1d1
ruským	ruský	k2eAgInSc7d1
finančním	finanční	k2eAgInSc7d1
příspěvkem	příspěvek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
vojska	vojsko	k1gNnSc2
rychle	rychle	k6eAd1
postoupila	postoupit	k5eAaPmAgFnS
k	k	k7c3
Niši	Niš	k1gInSc3
a	a	k8xC
Pirotu	Pirot	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
brzy	brzy	k6eAd1
úspěchu	úspěch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Překročení	překročení	k1gNnSc1
Staré	Staré	k2eAgFnSc2d1
planiny	planina	k1gFnSc2
a	a	k8xC
vpád	vpád	k1gInSc4
do	do	k7c2
jižního	jižní	k2eAgNnSc2d1
Bulharska	Bulharsko	k1gNnSc2
proběhl	proběhnout	k5eAaPmAgInS
opět	opět	k6eAd1
v	v	k7c6
rozdělení	rozdělení	k1gNnSc6
na	na	k7c4
3	#num#	k4
proudy	proud	k1gInPc4
<g/>
:	:	kIx,
západní	západní	k2eAgInPc4d1
(	(	kIx(
<g/>
71	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
s	s	k7c7
velícím	velící	k2eAgInSc7d1
Gurkem	Gurek	k1gInSc7
se	se	k3xPyFc4
až	až	k9
19	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
31	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
12	#num#	k4
<g/>
.	.	kIx.
1877	#num#	k4
dostalo	dostat	k5eAaPmAgNnS
po	po	k7c6
značných	značný	k2eAgFnPc6d1
obtížích	obtíž	k1gFnPc6
průsmykem	průsmyk	k1gInSc7
u	u	k7c2
Vrace	Vrace	k1gFnSc2
a	a	k8xC
Etropole	Etropole	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
do	do	k7c2
jižního	jižní	k2eAgNnSc2d1
Bulharska	Bulharsko	k1gNnSc2
<g/>
;	;	kIx,
ke	k	k7c3
dni	den	k1gInSc3
23	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
4.1	4.1	k4
<g/>
.1878	.1878	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
osvobozena	osvobozen	k2eAgFnSc1d1
Sofie	Sofie	k1gFnSc1
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
byl	být	k5eAaImAgMnS
zmařen	zmařen	k2eAgInSc4d1
turecký	turecký	k2eAgInSc4d1
plán	plán	k1gInSc4
na	na	k7c4
její	její	k3xOp3gNnSc4
vypálení	vypálení	k1gNnSc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
již	již	k6eAd1
mířili	mířit	k5eAaImAgMnP
Rusové	Rus	k1gMnPc1
v	v	k7c6
5	#num#	k4
kolonách	kolona	k1gFnPc6
k	k	k7c3
Plovdivu	Plovdiv	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
obsazen	obsadit	k5eAaPmNgInS
4	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
(	(	kIx(
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
1	#num#	k4
<g/>
.	.	kIx.
1878	#num#	k4
<g/>
,	,	kIx,
při	při	k7c6
následující	následující	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Plovdivu	Plovdiv	k1gInSc2
Turci	Turek	k1gMnPc1
opustili	opustit	k5eAaPmAgMnP
děla	dělo	k1gNnSc2
a	a	k8xC
prchli	prchnout	k5eAaPmAgMnP
ke	k	k7c3
Gjumjurdžině	Gjumjurdžina	k1gFnSc3
(	(	kIx(
<g/>
Komoténé	Komoténé	k2eAgInSc4d1
<g/>
)	)	kIx)
a	a	k8xC
k	k	k7c3
Dedeagači	Dedeagač	k1gInSc3
<g/>
,	,	kIx,
mohli	moct	k5eAaImAgMnP
dát	dát	k5eAaPmF
v	v	k7c4
tu	ten	k3xDgFnSc4
chvíli	chvíle	k1gFnSc4
dohromady	dohromady	k6eAd1
již	již	k6eAd1
jen	jen	k9
30	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
trojanský	trojanský	k2eAgMnSc1d1
(	(	kIx(
<g/>
6	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
gnrl	gnrl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karcov	Karcov	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
šipenský	šipenský	k2eAgMnSc1d1
(	(	kIx(
<g/>
54	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
a	a	k8xC
opolčenců	opolčenec	k1gMnPc2
pod	pod	k7c7
velením	velení	k1gNnSc7
Fjodora	Fjodor	k1gMnSc2
Radeckého	Radecký	k1gMnSc2
<g/>
;	;	kIx,
operovali	operovat	k5eAaImAgMnP
v	v	k7c6
prostoru	prostor	k1gInSc6
Šipka	šipka	k1gFnSc1
<g/>
–	–	k?
<g/>
Šejnovo	Šejnův	k2eAgNnSc4d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Šejnova	Šejnov	k1gInSc2
obklíčili	obklíčit	k5eAaPmAgMnP
během	během	k7c2
27	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
28	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
/	/	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
<g/>
/	/	kIx~
armádu	armáda	k1gFnSc4
Wessel	Wessela	k1gFnPc2
paši	paše	k1gFnSc4
a	a	k8xC
zajali	zajmout	k5eAaPmAgMnP
30	#num#	k4
000	#num#	k4
tureckých	turecký	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
se	se	k3xPyFc4
všechny	všechen	k3xTgFnPc1
skupiny	skupina	k1gFnPc1
spojily	spojit	k5eAaPmAgFnP
u	u	k7c2
Odrinu	Odrin	k1gInSc2
a	a	k8xC
8	#num#	k4
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
1	#num#	k4
<g/>
.	.	kIx.
1878	#num#	k4
se	se	k3xPyFc4
zmocnily	zmocnit	k5eAaPmAgFnP
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
utrpěli	utrpět	k5eAaPmAgMnP
Turci	Turek	k1gMnPc1
těžké	těžký	k2eAgFnSc2d1
ztráty	ztráta	k1gFnSc2
v	v	k7c6
severovýchodním	severovýchodní	k2eAgNnSc6d1
Bulharsku	Bulharsko	k1gNnSc6
(	(	kIx(
<g/>
prostor	prostor	k1gInSc1
Ruse	ruse	k6eAd1
<g/>
–	–	k?
<g/>
Silistra	Silistra	k1gFnSc1
<g/>
–	–	k?
<g/>
Varna	Varna	k1gFnSc1
<g/>
–	–	k?
<g/>
Šumen	šumno	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
Marmarskému	Marmarský	k2eAgNnSc3d1
moři	moře	k1gNnSc3
pak	pak	k6eAd1
již	již	k6eAd1
Rusové	Rus	k1gMnPc1
táhli	táhnout	k5eAaImAgMnP
od	od	k7c2
Odrinu	Odrin	k1gInSc2
bez	bez	k7c2
většího	veliký	k2eAgInSc2d2
odporu	odpor	k1gInSc2
nepřítele	nepřítel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Kavkazská	kavkazský	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
</s>
<s>
Ruské	ruský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
odráží	odrážet	k5eAaImIp3nP
turecký	turecký	k2eAgInSc4d1
útok	útok	k1gInSc4
proti	proti	k7c3
pevnosti	pevnost	k1gFnSc3
Beyazid	Beyazid	k1gInSc1
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1877	#num#	k4
<g/>
,	,	kIx,
olej	olej	k1gInSc1
od	od	k7c2
Lva	lev	k1gInSc2
Lagoria	Lagorium	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
1891	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
Kavkaze	Kavkaz	k1gInSc6
byly	být	k5eAaImAgInP
na	na	k7c6
počátku	počátek	k1gInSc6
války	válka	k1gFnSc2
síly	síla	k1gFnSc2
obou	dva	k4xCgFnPc2
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
na	na	k7c6
Dunaji	Dunaj	k1gInSc6
<g/>
,	,	kIx,
vyrovnané	vyrovnaný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruská	ruský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
Kavkazská	kavkazský	k2eAgFnSc1d1
pod	pod	k7c7
velením	velení	k1gNnSc7
velkoknížete	velkokníže	k1gMnSc2
Michaila	Michail	k1gMnSc2
Nikolajeviče	Nikolajevič	k1gMnSc2
brzy	brzy	k6eAd1
dosáhla	dosáhnout	k5eAaPmAgFnS
početního	početní	k2eAgInSc2d1
stavu	stav	k1gInSc2
asi	asi	k9
100	#num#	k4
tisíc	tisíc	k4xCgInPc2
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
osmanská	osmanský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
Muchdar	Muchdara	k1gFnPc2
paši	paša	k1gMnSc2
(	(	kIx(
<g/>
známého	známý	k2eAgMnSc2d1
také	také	k9
jako	jako	k8xC,k8xS
Ahmed	Ahmed	k1gMnSc1
Muhtar	Muhtar	k1gMnSc1
paša	paša	k1gMnSc1
nebo	nebo	k8xC
Muktar	Muktar	k1gMnSc1
paša	paša	k1gMnSc1
<g/>
)	)	kIx)
měla	mít	k5eAaImAgFnS
okolo	okolo	k7c2
90	#num#	k4
tisíc	tisíc	k4xCgInPc2
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lepší	lepšit	k5eAaImIp3nS
bojovou	bojový	k2eAgFnSc4d1
připravenost	připravenost	k1gFnSc4
měla	mít	k5eAaImAgFnS
ruská	ruský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
měla	mít	k5eAaImAgFnS
horší	zlý	k2eAgFnSc1d2
výzbroj	výzbroj	k1gFnSc1
než	než	k8xS
Turci	Turek	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turci	Turek	k1gMnPc1
byli	být	k5eAaImAgMnP
vyzbrojení	vyzbrojení	k1gNnSc6
nejnovějšími	nový	k2eAgFnPc7d3
opakovacími	opakovací	k2eAgFnPc7d1
puškami	puška	k1gFnPc7
anglické	anglický	k2eAgFnSc2d1
a	a	k8xC
americké	americký	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruskou	ruský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
aktivně	aktivně	k6eAd1
podporovaly	podporovat	k5eAaImAgInP
i	i	k9
zakavkazské	zakavkazský	k2eAgInPc1d1
národy	národ	k1gInPc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
Rusům	Rus	k1gMnPc3
zvyšovalo	zvyšovat	k5eAaImAgNnS
morálku	morálka	k1gFnSc4
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
na	na	k7c6
Balkáně	Balkán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
ruské	ruský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
byly	být	k5eAaImAgInP
i	i	k9
gruzínské	gruzínský	k2eAgInPc1d1
vojenské	vojenský	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
a	a	k8xC
milice	milice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
V	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
vedly	vést	k5eAaImAgFnP
v	v	k7c6
Bulharsku	Bulharsko	k1gNnSc6
rozhodující	rozhodující	k2eAgInPc4d1
boje	boj	k1gInPc4
<g/>
,	,	kIx,
dosáhla	dosáhnout	k5eAaPmAgFnS
ruská	ruský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc4
bojových	bojový	k2eAgInPc2d1
úspěchů	úspěch	k1gInPc2
také	také	k9
na	na	k7c6
kavkazské	kavkazský	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Michal	Michal	k1gMnSc1
Loris-Melikov	Loris-Melikov	k1gInSc4
ve	v	k7c6
dnech	den	k1gInPc6
13	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1877	#num#	k4
vyhrál	vyhrát	k5eAaPmAgMnS
bitvu	bitva	k1gFnSc4
u	u	k7c2
Aladža	Aladž	k1gInSc2
<g/>
–	–	k?
<g/>
dagu	dagus	k1gInSc2
<g/>
,	,	kIx,
dobyl	dobýt	k5eAaPmAgInS
Kars	Kars	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
zajal	zajmout	k5eAaPmAgMnS
tureckou	turecký	k2eAgFnSc4d1
posádku	posádka	k1gFnSc4
(	(	kIx(
<g/>
17	#num#	k4
tisíc	tisíc	k4xCgInSc4
vojáků	voják	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
obléhal	obléhat	k5eAaImAgMnS
Erzurum	Erzurum	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oběti	oběť	k1gFnPc1
války	válka	k1gFnSc2
</s>
<s>
„	„	k?
<g/>
Bulharské	bulharský	k2eAgFnSc2d1
mučednice	mučednice	k1gFnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
malba	malba	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1877	#num#	k4
od	od	k7c2
ruského	ruský	k2eAgMnSc2d1
malíře	malíř	k1gMnSc2
Konstantina	Konstantin	k1gMnSc2
Makovského	Makovský	k1gMnSc2
<g/>
,	,	kIx,
zobrazující	zobrazující	k2eAgNnSc4d1
znásilnění	znásilnění	k1gNnSc4
bulharských	bulharský	k2eAgFnPc2d1
žen	žena	k1gFnPc2
osmanskými	osmanský	k2eAgMnPc7d1
bašibozuky	bašibozuk	k1gMnPc7
při	při	k7c6
potlačení	potlačení	k1gNnSc6
Dubnového	dubnový	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
historiografii	historiografie	k1gFnSc6
Bulharska	Bulharsko	k1gNnSc2
a	a	k8xC
Turecka	Turecko	k1gNnSc2
stále	stále	k6eAd1
přetrvávají	přetrvávat	k5eAaImIp3nP
pohledy	pohled	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
zvýrazňují	zvýrazňovat	k5eAaImIp3nP
utrpení	utrpení	k1gNnSc4
vlastního	vlastní	k2eAgInSc2d1
lidu	lid	k1gInSc2
a	a	k8xC
zvláště	zvláště	k6eAd1
u	u	k7c2
Turků	Turek	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
vidí	vidět	k5eAaImIp3nP
válku	válka	k1gFnSc4
jako	jako	k8xC,k8xS
křivdu	křivda	k1gFnSc4
na	na	k7c6
své	svůj	k3xOyFgFnSc6
veliké	veliký	k2eAgFnSc6d1
říši	říš	k1gFnSc6
a	a	k8xC
na	na	k7c6
své	svůj	k3xOyFgFnSc6
menšině	menšina	k1gFnSc6
v	v	k7c6
Bulharsku	Bulharsko	k1gNnSc6
<g/>
,	,	kIx,
jasně	jasně	k6eAd1
přetrvává	přetrvávat	k5eAaImIp3nS
ublížený	ublížený	k2eAgInSc4d1
postoj	postoj	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
zveličit	zveličit	k5eAaPmF
oběti	oběť	k1gFnPc4
Turků	turek	k1gInPc2
a	a	k8xC
marginalizovat	marginalizovat	k5eAaImF,k5eAaBmF,k5eAaPmF
ztráty	ztráta	k1gFnPc4
bulharského	bulharský	k2eAgInSc2d1
národa	národ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
tak	tak	k6eAd1
je	být	k5eAaImIp3nS
pravda	pravda	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
nakonec	nakonec	k6eAd1
bylo	být	k5eAaImAgNnS
po	po	k7c6
prohrané	prohraný	k2eAgFnSc6d1
válce	válka	k1gFnSc6
postiženo	postihnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
hlavně	hlavně	k9
uprchlictvím	uprchlictví	k1gNnSc7
a	a	k8xC
ztrátou	ztráta	k1gFnSc7
majetku	majetek	k1gInSc2
<g/>
,	,	kIx,
větší	veliký	k2eAgNnSc1d2
množství	množství	k1gNnSc1
Turků	Turek	k1gMnPc2
než	než	k8xS
Bulharů	Bulhar	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Represálie	represálie	k1gFnPc1
v	v	k7c6
Zagoře	Zagora	k1gFnSc6
a	a	k8xC
okolí	okolí	k1gNnSc6
</s>
<s>
Dne	den	k1gInSc2
31	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1877	#num#	k4
po	po	k7c6
šestihodinovém	šestihodinový	k2eAgInSc6d1
boji	boj	k1gInSc6
o	o	k7c4
Starou	starý	k2eAgFnSc4d1
Zagoru	Zagora	k1gFnSc4
ustoupili	ustoupit	k5eAaPmAgMnP
ruští	ruský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
a	a	k8xC
bulharští	bulharský	k2eAgMnPc1d1
dobrovolníci	dobrovolník	k1gMnPc1
tlaku	tlak	k1gInSc2
turecké	turecký	k2eAgFnSc2d1
přesily	přesila	k1gFnSc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
umožnili	umožnit	k5eAaPmAgMnP
Turkům	Turek	k1gMnPc3
rozpoutat	rozpoutat	k5eAaPmF
nejtragičtější	tragický	k2eAgFnPc4d3
události	událost	k1gFnPc4
v	v	k7c6
historii	historie	k1gFnSc6
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
celé	celý	k2eAgNnSc1d1
spáleno	spálen	k2eAgNnSc1d1
a	a	k8xC
zničeno	zničen	k2eAgNnSc1d1
do	do	k7c2
základů	základ	k1gInPc2
během	během	k7c2
dalších	další	k2eAgInPc2d1
tří	tři	k4xCgInPc2
dnů	den	k1gInPc2
po	po	k7c6
bitvě	bitva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediná	jediný	k2eAgFnSc1d1
veřejná	veřejný	k2eAgFnSc1d1
budova	budova	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zůstala	zůstat	k5eAaPmAgFnS
stát	stát	k1gInSc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
mešita	mešita	k1gFnSc1
Eski	Esk	k1gFnSc2
Džamija	Džamija	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
obětí	oběť	k1gFnPc2
ve	v	k7c6
Staré	Staré	k2eAgFnSc6d1
Zagoře	Zagora	k1gFnSc6
a	a	k8xC
o	o	k7c6
sousedních	sousední	k2eAgFnPc6d1
vesnicích	vesnice	k1gFnPc6
dosáhl	dosáhnout	k5eAaPmAgInS
14	#num#	k4
500	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Populace	populace	k1gFnSc1
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
největšího	veliký	k2eAgNnSc2d3
města	město	k1gNnSc2
na	na	k7c6
bulharském	bulharský	k2eAgNnSc6d1
území	území	k1gNnSc6
se	se	k3xPyFc4
významně	významně	k6eAd1
snížila	snížit	k5eAaPmAgFnS
a	a	k8xC
po	po	k7c6
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
čítala	čítat	k5eAaImAgFnS
jen	jen	k9
16	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
pak	pak	k6eAd1
bylo	být	k5eAaImAgNnS
znovu	znovu	k6eAd1
vybudováno	vybudovat	k5eAaPmNgNnS
podle	podle	k7c2
plánů	plán	k1gInPc2
českého	český	k2eAgMnSc2d1
architekta	architekt	k1gMnSc2
Lubora	Lubor	k1gMnSc2
Bayera	Bayer	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Oběti	oběť	k1gFnPc1
odvetných	odvetný	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
v	v	k7c6
jižním	jižní	k2eAgNnSc6d1
Bulharsku	Bulharsko	k1gNnSc6
</s>
<s>
Jen	jen	k9
v	v	k7c6
jižním	jižní	k2eAgNnSc6d1
Bulharsku	Bulharsko	k1gNnSc6
bylo	být	k5eAaImAgNnS
pobito	pobít	k5eAaPmNgNnS
18	#num#	k4
000	#num#	k4
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
vypáleno	vypálen	k2eAgNnSc4d1
40	#num#	k4
860	#num#	k4
obydlí	obydlí	k1gNnPc2
<g/>
,	,	kIx,
925	#num#	k4
kostelů	kostel	k1gInPc2
a	a	k8xC
120	#num#	k4
škol	škola	k1gFnPc2
(	(	kIx(
<g/>
hlavně	hlavně	k9
v	v	k7c6
okolí	okolí	k1gNnSc6
Plovdivu	Plovdiv	k1gInSc2
a	a	k8xC
Odrinu	Odrin	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Zvláště	zvláště	k6eAd1
v	v	k7c6
údolí	údolí	k1gNnSc6
Marice	Marica	k1gFnSc2
byli	být	k5eAaImAgMnP
lidé	člověk	k1gMnPc1
věšeni	věsit	k5eAaImNgMnP
systémově	systémově	k6eAd1
na	na	k7c6
každém	každý	k3xTgInSc6
rohu	roh	k1gInSc6
<g/>
,	,	kIx,
při	při	k7c6
každém	každý	k3xTgNnSc6
podezření	podezření	k1gNnSc6
z	z	k7c2
pomoci	pomoc	k1gFnSc2
Rusům	Rus	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
vesnice	vesnice	k1gFnSc2
byly	být	k5eAaImAgFnP
vypáleny	vypálen	k2eAgFnPc1d1
i	i	k9
bez	bez	k7c2
jakéhokoliv	jakýkoliv	k3yIgNnSc2
podezření	podezření	k1gNnSc2
z	z	k7c2
kolaborace	kolaborace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bulharští	bulharský	k2eAgMnPc1d1
uprchlíci	uprchlík	k1gMnPc1
</s>
<s>
Na	na	k7c4
Rusy	Rus	k1gMnPc4
okupovaná	okupovaný	k2eAgFnSc1d1
území	území	k1gNnSc6
uprchlo	uprchnout	k5eAaPmAgNnS
z	z	k7c2
dnešních	dnešní	k2eAgFnPc2d1
thráckých	thrácký	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
Řecka	Řecko	k1gNnSc2
a	a	k8xC
Turecka	Turecko	k1gNnSc2
asi	asi	k9
100	#num#	k4
000	#num#	k4
osob	osoba	k1gFnPc2
bulharské	bulharský	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Turečtí	turecký	k2eAgMnPc1d1
uprchlíci	uprchlík	k1gMnPc1
a	a	k8xC
vyhnanci	vyhnanec	k1gMnPc1
</s>
<s>
Počet	počet	k1gInSc1
tureckých	turecký	k2eAgMnPc2d1
uprchlíků	uprchlík	k1gMnPc2
je	být	k5eAaImIp3nS
odhadován	odhadovat	k5eAaImNgInS
od	od	k7c2
130	#num#	k4
000	#num#	k4
až	až	k6eAd1
po	po	k7c4
asi	asi	k9
750	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
mrtvých	mrtvý	k1gMnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnPc1
příčiny	příčina	k1gFnPc1
úmrtí	úmrtí	k1gNnSc2
byly	být	k5eAaImAgFnP
zřejmě	zřejmě	k6eAd1
různorodé	různorodý	k2eAgFnPc1d1
<g/>
,	,	kIx,
od	od	k7c2
vyvražďování	vyvražďování	k1gNnSc2
<g/>
,	,	kIx,
přes	přes	k7c4
epidemie	epidemie	k1gFnPc4
až	až	k9
po	po	k7c4
nedostatek	nedostatek	k1gInSc4
potravin	potravina	k1gFnPc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
odhadovány	odhadovat	k5eAaImNgFnP
až	až	k9
na	na	k7c4
200	#num#	k4
000	#num#	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ztráty	ztráta	k1gFnPc4
majetku	majetek	k1gInSc2
tureckých	turecký	k2eAgMnPc2d1
velkostatkářů	velkostatkář	k1gMnPc2
byly	být	k5eAaImAgFnP
v	v	k7c6
menší	malý	k2eAgFnSc6d2
míře	míra	k1gFnSc6
nahrazovány	nahrazovat	k5eAaImNgFnP
<g/>
,	,	kIx,
hlavně	hlavně	k9
ve	v	k7c6
Východní	východní	k2eAgFnSc6d1
Rumélii	Rumélie	k1gFnSc6
a	a	k8xC
v	v	k7c6
jihozápadním	jihozápadní	k2eAgNnSc6d1
Bulharsku	Bulharsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
turečtí	turecký	k2eAgMnPc1d1
vlastníci	vlastník	k1gMnPc1
díky	díky	k7c3
prostřednictví	prostřednictví	k1gNnSc3
zvláštních	zvláštní	k2eAgMnPc2d1
zplnomocněnců	zplnomocněnec	k1gMnPc2
prodávali	prodávat	k5eAaImAgMnP
svou	svůj	k3xOyFgFnSc4
půdu	půda	k1gFnSc4
bulharským	bulharský	k2eAgMnSc7d1
rolníkům	rolník	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
vykoupili	vykoupit	k5eAaPmAgMnP
Bulhaři	Bulhar	k1gMnPc1
od	od	k7c2
Turků	Turek	k1gMnPc2
téměř	téměř	k6eAd1
jednu	jeden	k4xCgFnSc4
čtvrtinu	čtvrtina	k1gFnSc4
obhospodařovávané	obhospodařovávaný	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Poválečné	poválečný	k2eAgFnPc1d1
události	událost	k1gFnPc1
</s>
<s>
Předběžný	předběžný	k2eAgInSc1d1
mír	mír	k1gInSc1
v	v	k7c6
San	San	k1gMnSc6
Stefanu	Stefan	k1gMnSc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Sanstefanská	Sanstefanský	k2eAgFnSc1d1
mírová	mírový	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
<g/>
)	)	kIx)
1878	#num#	k4
–	–	k?
hrabě	hrabě	k1gMnSc1
Ignaťjev	Ignaťjev	k1gMnSc1
uzavřel	uzavřít	k5eAaPmAgMnS
předběžnou	předběžný	k2eAgFnSc4d1
mírovou	mírový	k2eAgFnSc4d1
dohodu	dohoda	k1gFnSc4
v	v	k7c6
městečku	městečko	k1gNnSc6
San	San	k1gFnSc2
Stefano	Stefana	k1gFnSc5
(	(	kIx(
<g/>
13	#num#	k4
km	km	kA
od	od	k7c2
Cařihradu	Cařihrad	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
byli	být	k5eAaImAgMnP
Rusové	Rus	k1gMnPc1
nuceni	nucen	k2eAgMnPc1d1
se	se	k3xPyFc4
zastavit	zastavit	k5eAaPmF
pod	pod	k7c7
hrozbou	hrozba	k1gFnSc7
zásahu	zásah	k1gInSc2
britského	britský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
(	(	kIx(
<g/>
vyslaného	vyslaný	k2eAgInSc2d1
do	do	k7c2
Marmarského	Marmarský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rozsah	rozsah	k1gInSc1
touto	tento	k3xDgFnSc7
dohodu	dohoda	k1gFnSc4
vytvořeného	vytvořený	k2eAgNnSc2d1
Velkého	velký	k2eAgNnSc2d1
Bulharska	Bulharsko	k1gNnSc2
byl	být	k5eAaImAgInS
následující	následující	k2eAgInSc1d1
<g/>
:	:	kIx,
Mizie	Mizie	k1gFnSc1
(	(	kIx(
<g/>
sev	sev	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulharsko	Bulharsko	k1gNnSc1
–	–	k?
sev	sev	k?
<g/>
.	.	kIx.
hranicí	hranice	k1gFnSc7
Dunaj	Dunaj	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jižní	jižní	k2eAgFnSc1d1
Dobrudža	Dobrudža	k1gFnSc1
po	po	k7c6
linii	linie	k1gFnSc6
Černá	černý	k2eAgFnSc1d1
voda	voda	k1gFnSc1
<g/>
–	–	k?
<g/>
Mangalia	Mangalia	k1gFnSc1
<g/>
,	,	kIx,
Thrákie	Thrákie	k1gFnSc1
a	a	k8xC
Makedonie	Makedonie	k1gFnSc1
(	(	kIx(
<g/>
jižní	jižní	k2eAgNnSc1d1
Bulharsko	Bulharsko	k1gNnSc1
<g/>
,	,	kIx,
okolí	okolí	k1gNnSc1
Lüleburgasu	Lüleburgas	k1gInSc2
/	/	kIx~
<g/>
většinově	většinově	k6eAd1
národnostně	národnostně	k6eAd1
bulharské	bulharský	k2eAgFnSc2d1
<g/>
/	/	kIx~
–	–	k?
vých	vých	k1gInSc1
<g/>
.	.	kIx.
hranice	hranice	k1gFnSc1
Černé	Černé	k2eAgNnSc2d1
moře	moře	k1gNnSc2
po	po	k7c4
Achtopol	Achtopol	k1gInSc4
<g/>
,	,	kIx,
jižní	jižní	k2eAgNnSc4d1
egejské	egejský	k2eAgNnSc4d1
/	/	kIx~
<g/>
bělomořské	bělomořský	k2eAgNnSc4d1
<g/>
/	/	kIx~
pobřeží	pobřeží	k1gNnSc4
od	od	k7c2
ústí	ústí	k1gNnSc2
Mesty	Mesty	k?
až	až	k9
po	po	k7c6
ústí	ústí	k1gNnSc6
Vardaru	Vardar	k1gInSc2
–	–	k?
včetně	včetně	k7c2
Kavaly	kaval	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
bez	bez	k7c2
Chalkidiké	Chalkidiká	k1gFnSc2
a	a	k8xC
Soluně	Soluň	k1gFnSc2
<g/>
;	;	kIx,
na	na	k7c6
západě	západ	k1gInSc6
k	k	k7c3
Albánii	Albánie	k1gFnSc3
<g/>
)	)	kIx)
a	a	k8xC
Pirotsko	Pirotsko	k1gNnSc1
</s>
<s>
Konečné	Konečné	k2eAgNnSc1d1
uspořádání	uspořádání	k1gNnSc1
z	z	k7c2
Berlínského	berlínský	k2eAgInSc2d1
kongresu	kongres	k1gInSc2
</s>
<s>
Kvůli	kvůli	k7c3
neshodám	neshoda	k1gFnPc3
velmocí	velmoc	k1gFnPc2
byl	být	k5eAaImAgInS
svolán	svolat	k5eAaPmNgInS
díky	díky	k7c3
nabídce	nabídka	k1gFnSc3
kancléře	kancléř	k1gMnSc2
Bismarcka	Bismarcka	k1gFnSc1
a	a	k8xC
13	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1878	#num#	k4
zahájen	zahájen	k2eAgInSc4d1
Berlínský	berlínský	k2eAgInSc4d1
kongres	kongres	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
něm	on	k3xPp3gMnSc6
bylo	být	k5eAaImAgNnS
nakonec	nakonec	k6eAd1
rozhodnuto	rozhodnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
Bulharské	bulharský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
s	s	k7c7
dědičným	dědičný	k2eAgMnSc7d1
křesťanským	křesťanský	k2eAgMnSc7d1
panovníkem	panovník	k1gMnSc7
bude	být	k5eAaImBp3nS
závislý	závislý	k2eAgInSc1d1
stát	stát	k1gInSc1
mezi	mezi	k7c7
Dunajem	Dunaj	k1gInSc7
a	a	k8xC
Starou	starý	k2eAgFnSc7d1
planinou	planina	k1gFnSc7
(	(	kIx(
<g/>
bez	bez	k7c2
Pirotska	Pirotsk	k1gInSc2
<g/>
)	)	kIx)
pod	pod	k7c7
pravomocí	pravomoc	k1gFnSc7
tureckého	turecký	k2eAgMnSc2d1
sultána	sultán	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Části	část	k1gFnSc2
okresů	okres	k1gInPc2
Vranja	Vranj	k1gInSc2
a	a	k8xC
Pirot	Pirot	k1gInSc1
byly	být	k5eAaImAgFnP
přiřknuty	přiřknout	k5eAaPmNgInP
Srbsku	Srbsko	k1gNnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgInS
zamítnut	zamítnout	k5eAaPmNgInS
jeho	jeho	k3xOp3gInSc1
požadavek	požadavek	k1gInSc1
na	na	k7c4
Kosovo	Kosův	k2eAgNnSc4d1
Pole	pole	k1gNnSc4
a	a	k8xC
Novopazarský	Novopazarský	k2eAgInSc4d1
sandžak	sandžak	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
získalo	získat	k5eAaPmAgNnS
na	na	k7c4
30	#num#	k4
let	léto	k1gNnPc2
do	do	k7c2
správy	správa	k1gFnSc2
Bosnu	Bosna	k1gFnSc4
i	i	k8xC
Hercegovinu	Hercegovina	k1gFnSc4
<g/>
,	,	kIx,
Británii	Británie	k1gFnSc4
byl	být	k5eAaImAgMnS
jako	jako	k9
kompenzace	kompenzace	k1gFnPc4
věnován	věnován	k2eAgInSc4d1
Kypr	Kypr	k1gInSc4
a	a	k8xC
Rusku	Ruska	k1gFnSc4
byl	být	k5eAaImAgInS
přiřknut	přiřknout	k5eAaPmNgInS
Kars	Kars	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
držení	držení	k1gNnSc6
ponechána	ponechán	k2eAgNnPc1d1
Batumi	Batumi	k1gNnPc1
a	a	k8xC
Ardachan	Ardachan	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
muselo	muset	k5eAaImAgNnS
vrátit	vrátit	k5eAaPmF
Bajazid	Bajazid	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruská	ruský	k2eAgFnSc1d1
správa	správa	k1gFnSc1
Bulharska	Bulharsko	k1gNnSc2
byla	být	k5eAaImAgFnS
zkrácena	zkrátit	k5eAaPmNgFnS
na	na	k7c4
pouhých	pouhý	k2eAgFnPc2d1
9	#num#	k4
měsíců	měsíc	k1gInPc2
(	(	kIx(
<g/>
z	z	k7c2
původně	původně	k6eAd1
plánovaných	plánovaný	k2eAgNnPc2d1
24	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
největší	veliký	k2eAgFnSc7d3
křivdou	křivda	k1gFnSc7
pro	pro	k7c4
Bulhary	Bulhar	k1gMnPc4
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
vytvoření	vytvoření	k1gNnSc1
nového	nový	k2eAgInSc2d1
autonomního	autonomní	k2eAgInSc2d1
útvaru	útvar	k1gInSc2
Východní	východní	k2eAgFnSc2d1
Rumélie	Rumélie	k1gFnSc2
v	v	k7c6
jižním	jižní	k2eAgNnSc6d1
Bulharsku	Bulharsko	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
čele	čelo	k1gNnSc6
měl	mít	k5eAaImAgInS
stát	stát	k5eAaImF,k5eAaPmF
vždy	vždy	k6eAd1
křesťanský	křesťanský	k2eAgMnSc1d1
guvernér	guvernér	k1gMnSc1
(	(	kIx(
<g/>
jmenovaný	jmenovaný	k2eAgMnSc1d1
na	na	k7c4
5	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osmanům	Osman	k1gMnPc3
zůstalo	zůstat	k5eAaPmAgNnS
právo	právo	k1gNnSc4
mít	mít	k5eAaImF
rozmístěné	rozmístěný	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
v	v	k7c6
bývalých	bývalý	k2eAgFnPc6d1
tureckých	turecký	k2eAgFnPc6d1
pevnostech	pevnost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Razložsko-kresenské	Razložsko-kresenský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
</s>
<s>
Koncem	koncem	k7c2
září	září	k1gNnSc2
1878	#num#	k4
pronikl	proniknout	k5eAaPmAgMnS
do	do	k7c2
severní	severní	k2eAgFnSc2d1
Makedonie	Makedonie	k1gFnSc2
oddíl	oddíl	k1gInSc1
polského	polský	k2eAgMnSc2d1
důstojníka	důstojník	k1gMnSc2
Ludwiga	Ludwig	k1gMnSc2
Wojtkewicze	Wojtkewicze	k1gFnSc2
(	(	kIx(
<g/>
rozehnán	rozehnán	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
poté	poté	k6eAd1
oddíl	oddíl	k1gInSc1
Adama	Adam	k1gMnSc2
Ivanoviče	Ivanovič	k1gMnSc2
Kalmykova	Kalmykův	k2eAgMnSc2d1
do	do	k7c2
okolí	okolí	k1gNnSc2
Melniku	Melnik	k1gInSc2
(	(	kIx(
<g/>
Kresna	Kresna	k1gFnSc1
<g/>
,	,	kIx,
Vlachi	Vlachi	k1gNnSc1
<g/>
,	,	kIx,
Oštava	Oštava	k1gFnSc1
<g/>
,	,	kIx,
Sărbinovo	Sărbinův	k2eAgNnSc4d1
<g/>
)	)	kIx)
a	a	k8xC
po	po	k7c6
tuhém	tuhý	k2eAgInSc6d1
boji	boj	k1gInSc6
kapitulovala	kapitulovat	k5eAaBmAgFnS
turecká	turecký	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
v	v	k7c6
Kresně	Kresně	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povstání	povstání	k1gNnSc1
se	se	k3xPyFc4
rozšířilo	rozšířit	k5eAaPmAgNnS
po	po	k7c6
celém	celý	k2eAgInSc6d1
Melnicku	Melnick	k1gInSc6
a	a	k8xC
do	do	k7c2
okolí	okolí	k1gNnSc2
Razlogu	Razlog	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povstalci	povstalec	k1gMnPc1
se	se	k3xPyFc4
vzdali	vzdát	k5eAaPmAgMnP
turecké	turecký	k2eAgFnSc3d1
přesile	přesila	k1gFnSc3
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
britské	britský	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
přepravilo	přepravit	k5eAaPmAgNnS
turecké	turecký	k2eAgFnPc4d1
posily	posila	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Ustavení	ustavení	k1gNnSc1
nezávislého	závislý	k2eNgNnSc2d1
Bulharska	Bulharsko	k1gNnSc2
a	a	k8xC
situace	situace	k1gFnSc2
ve	v	k7c6
Východní	východní	k2eAgFnSc6d1
Rumélii	Rumélie	k1gFnSc6
</s>
<s>
Po	po	k7c6
Berlínském	berlínský	k2eAgInSc6d1
kongresu	kongres	k1gInSc6
panovala	panovat	k5eAaImAgFnS
mezi	mezi	k7c7
Bulhary	Bulhar	k1gMnPc7
značná	značný	k2eAgFnSc1d1
nespokojenost	nespokojenost	k1gFnSc1
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
ocitli	ocitnout	k5eAaPmAgMnP
mimo	mimo	k7c4
hranice	hranice	k1gFnPc4
Bulharského	bulharský	k2eAgNnSc2d1
knížectví	knížectví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruská	ruský	k2eAgFnSc1d1
okupace	okupace	k1gFnSc1
byla	být	k5eAaImAgFnS
zkrácena	zkrátit	k5eAaPmNgFnS
z	z	k7c2
plánovaných	plánovaný	k2eAgNnPc2d1
dvou	dva	k4xCgNnPc2
let	léto	k1gNnPc2
na	na	k7c6
devět	devět	k4xCc4
měsíců	měsíc	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
s	s	k7c7
ustavením	ustavení	k1gNnSc7
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
muselo	muset	k5eAaImAgNnS
pospíchat	pospíchat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInPc1d3
boje	boj	k1gInPc1
se	se	k3xPyFc4
rozhořely	rozhořet	k5eAaPmAgInP
o	o	k7c6
znění	znění	k1gNnSc6
ústavy	ústava	k1gFnSc2
<g/>
,	,	kIx,
spory	spor	k1gInPc1
probíhaly	probíhat	k5eAaImAgInP
mezi	mezi	k7c7
konzervativci	konzervativec	k1gMnPc7
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnPc7
vůdčími	vůdčí	k2eAgFnPc7d1
osobnostmi	osobnost	k1gFnPc7
byli	být	k5eAaImAgMnP
Konstantin	Konstantin	k1gMnSc1
Stoilov	Stoilov	k1gInSc4
<g/>
,	,	kIx,
Dimitǎ	Dimitǎ	k1gInSc1
Grekov	Grekov	k1gInSc1
a	a	k8xC
první	první	k4xOgMnSc1
bulharský	bulharský	k2eAgMnSc1d1
ministerský	ministerský	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
Todor	Todora	k1gFnPc2
Stojanov	Stojanov	k1gInSc1
Burmov	Burmov	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
národními	národní	k2eAgMnPc7d1
liberály	liberál	k1gMnPc7
vedenými	vedený	k2eAgFnPc7d1
Stefanem	Stefan	k1gMnSc7
Stambolovem	Stambolov	k1gInSc7
<g/>
,	,	kIx,
Petkem	Petek	k1gInSc7
Karavelovem	Karavelov	k1gInSc7
či	či	k8xC
Petkem	Petek	k1gMnSc7
Slavejkovem	Slavejkov	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečná	konečný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
textu	text	k1gInSc2
ústavy	ústava	k1gFnSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
Trnovská	Trnovská	k1gFnSc1
ústava	ústava	k1gFnSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
schválena	schválit	k5eAaPmNgFnS
16	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1879	#num#	k4
a	a	k8xC
na	na	k7c4
tehdejší	tehdejší	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
byla	být	k5eAaImAgFnS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejdemokratičtějších	demokratický	k2eAgFnPc2d3
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Přes	přes	k7c4
veškerou	veškerý	k3xTgFnSc4
tureckou	turecký	k2eAgFnSc4d1
snahu	snaha	k1gFnSc4
se	se	k3xPyFc4
Východní	východní	k2eAgFnSc1d1
Rumélie	Rumélie	k1gFnSc1
nestala	stát	k5eNaPmAgFnS
opět	opět	k6eAd1
plně	plně	k6eAd1
ovládaným	ovládaný	k2eAgNnSc7d1
tureckým	turecký	k2eAgNnSc7d1
územím	území	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
byla	být	k5eAaImAgFnS
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
řízena	řízen	k2eAgFnSc1d1
Bulhary	Bulhar	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generálním	generální	k2eAgMnSc7d1
guvernérem	guvernér	k1gMnSc7
Východní	východní	k2eAgFnSc2d1
Rumélie	Rumélie	k1gFnSc2
byl	být	k5eAaImAgInS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
poturčený	poturčený	k2eAgMnSc1d1
Bulhar	Bulhar	k1gMnSc1
<g/>
,	,	kIx,
diplomat	diplomat	k1gMnSc1
Aleko	Aleka	k1gFnSc5
Bogoridi	Bogorid	k1gMnPc1
(	(	kIx(
<g/>
po	po	k7c6
letech	léto	k1gNnPc6
v	v	k7c6
tureckých	turecký	k2eAgFnPc6d1
službách	služba	k1gFnPc6
se	se	k3xPyFc4
už	už	k6eAd1
ani	ani	k8xC
nedokázal	dokázat	k5eNaPmAgMnS
vyjádřit	vyjádřit	k5eAaPmF
bulharsky	bulharsky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
podle	podle	k7c2
vzpomínek	vzpomínka	k1gFnPc2
současníků	současník	k1gMnPc2
již	již	k6eAd1
při	při	k7c6
příjezdu	příjezd	k1gInSc6
podlehl	podlehnout	k5eAaPmAgMnS
strachu	strach	k1gInSc3
z	z	k7c2
nespokojenosti	nespokojenost	k1gFnSc2
bulharského	bulharský	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
,	,	kIx,
projevované	projevovaný	k2eAgFnSc2d1
v	v	k7c6
rozvášněných	rozvášněný	k2eAgFnPc6d1
demonstracích	demonstrace	k1gFnPc6
<g/>
,	,	kIx,
a	a	k8xC
rychle	rychle	k6eAd1
skryl	skrýt	k5eAaPmAgMnS
turecký	turecký	k2eAgInSc4d1
fez	fez	k1gInSc4
a	a	k8xC
nasadil	nasadit	k5eAaPmAgMnS
bulharskou	bulharský	k2eAgFnSc4d1
beranici	beranice	k1gFnSc4
<g/>
,	,	kIx,
tzv.	tzv.	kA
kalpak	kalpak	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
zástupcem	zástupce	k1gMnSc7
<g/>
,	,	kIx,
hlavním	hlavní	k2eAgMnSc7d1
tajemníkem	tajemník	k1gMnSc7
generálního	generální	k2eAgMnSc4d1
guvernéra	guvernér	k1gMnSc4
a	a	k8xC
direktorem	direktor	k1gMnSc7
vnitřních	vnitřní	k2eAgFnPc2d1
záležitostí	záležitost	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Gavril	Gavril	k1gMnSc1
Krǎ	Krǎ	k1gMnSc1
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
církevních	církevní	k2eAgInPc2d1
bojů	boj	k1gInPc2
v	v	k7c6
Cařihradě	Cařihrad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generálního	generální	k2eAgMnSc2d1
guvernéra	guvernér	k1gMnSc4
podporovala	podporovat	k5eAaImAgFnS
strana	strana	k1gFnSc1
státoprávní	státoprávní	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
vedení	vedení	k1gNnSc6
byl	být	k5eAaImAgInS
např.	např.	kA
Stojan	stojan	k1gInSc1
Čomakov	Čomakov	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
proti	proti	k7c3
ní	on	k3xPp3gFnSc6
stála	stát	k5eAaImAgFnS
strana	strana	k1gFnSc1
konzervativní	konzervativní	k2eAgFnSc1d1
<g/>
,	,	kIx,
čili	čili	k8xC
sjednocovací	sjednocovací	k2eAgInPc1d1
<g/>
,	,	kIx,
jejímiž	jejíž	k3xOyRp3gMnPc7
vůdčími	vůdčí	k2eAgMnPc7d1
představiteli	představitel	k1gMnPc7
byli	být	k5eAaImAgMnP
Ivan	Ivan	k1gMnSc1
Estatiev	Estativa	k1gFnPc2
Gešov	Gešov	k1gInSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
Stojanov	Stojanov	k1gInSc1
Gešov	Gešovo	k1gNnPc2
či	či	k8xC
Michail	Michaila	k1gFnPc2
Madžarov	Madžarovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
nejvýznamnějších	významný	k2eAgNnPc2d3
střetnutí	střetnutí	k1gNnPc2
</s>
<s>
1877	#num#	k4
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
–	–	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Kizil-Tepe	Kizil-Tep	k1gInSc5
–	–	k?
ruský	ruský	k2eAgInSc4d1
pokus	pokus	k1gInSc4
dobýt	dobýt	k5eAaPmF
Kars	Kars	k1gInSc4
byl	být	k5eAaImAgInS
odražen	odražen	k2eAgInSc1d1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
–	–	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Zimnice	zimnice	k1gFnSc2
–	–	k?
Rusové	Rus	k1gMnPc1
připravují	připravovat	k5eAaImIp3nP
překročení	překročení	k1gNnSc4
Dunaje	Dunaj	k1gInSc2
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
–	–	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Svištov	Svištov	k1gInSc4
–	–	k?
Rusové	Rus	k1gMnPc1
dobyli	dobýt	k5eAaPmAgMnP
přístav	přístav	k1gInSc4
a	a	k8xC
pokračovali	pokračovat	k5eAaImAgMnP
k	k	k7c3
Nikopoli	Nikopole	k1gFnSc3
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
–	–	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Nikopole	Nikopole	k1gFnSc2
–	–	k?
Rusové	Rus	k1gMnPc1
porazili	porazit	k5eAaPmAgMnP
Turky	Turek	k1gMnPc4
a	a	k8xC
obsadili	obsadit	k5eAaPmAgMnP
Nikopol	Nikopol	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c4
tu	ten	k3xDgFnSc4
chvíli	chvíle	k1gFnSc4
mohou	moct	k5eAaImIp3nP
bez	bez	k7c2
překážky	překážka	k1gFnSc2
vtáhnout	vtáhnout	k5eAaPmF
do	do	k7c2
Bulharska	Bulharsko	k1gNnSc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
–	–	k?
První	první	k4xOgFnSc1
bitva	bitva	k1gFnSc1
v	v	k7c6
průsmyku	průsmyk	k1gInSc6
Šipka	šipka	k1gFnSc1
–	–	k?
Rusové	Rus	k1gMnPc1
obsadili	obsadit	k5eAaPmAgMnP
průsmyk	průsmyk	k1gInSc4
Šipka	šipka	k1gFnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
–	–	k?
začátek	začátek	k1gInSc1
obléhání	obléhání	k1gNnSc2
Plevna	Plevno	k1gNnSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
–	–	k?
První	první	k4xOgFnSc1
bitva	bitva	k1gFnSc1
u	u	k7c2
Plevna	Plevno	k1gNnSc2
–	–	k?
Osmané	Osman	k1gMnPc1
odrazili	odrazit	k5eAaPmAgMnP
málo	málo	k6eAd1
připravený	připravený	k2eAgInSc4d1
ruský	ruský	k2eAgInSc4d1
útok	útok	k1gInSc4
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
–	–	k?
Druhá	druhý	k4xOgFnSc1
bitva	bitva	k1gFnSc1
u	u	k7c2
Plevna	Plevno	k1gNnSc2
–	–	k?
Osmané	Osman	k1gMnPc1
nechali	nechat	k5eAaPmAgMnP
vykrvácet	vykrvácet	k5eAaPmF
útok	útok	k1gInSc4
Rusů	Rus	k1gMnPc2
a	a	k8xC
Rumunů	Rumun	k1gMnPc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
25	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
–	–	k?
Druhá	druhý	k4xOgFnSc1
bitva	bitva	k1gFnSc1
v	v	k7c6
průsmyku	průsmyk	k1gInSc6
Šipka	šipka	k1gFnSc1
–	–	k?
Rusové	Rusová	k1gFnSc2
odrážejí	odrážet	k5eAaImIp3nP
vlny	vlna	k1gFnPc1
útoků	útok	k1gInPc2
osmanského	osmanský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
v	v	k7c6
průsmyku	průsmyk	k1gInSc6
Šipka	šipka	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
–	–	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Lovče	lovec	k1gMnSc5
–	–	k?
Rusové	Rus	k1gMnPc1
omezili	omezit	k5eAaPmAgMnP
zásobování	zásobování	k1gNnSc4
Osmanů	Osman	k1gMnPc2
porážkou	porážka	k1gFnSc7
a	a	k8xC
dobytím	dobytí	k1gNnSc7
Lovče	lovec	k1gMnSc5
během	běh	k1gInSc7
obléhání	obléhání	k1gNnSc2
Plevna	Plevno	k1gNnSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
–	–	k?
Třetí	třetí	k4xOgFnSc1
bitva	bitva	k1gFnSc1
u	u	k7c2
Plevna	Plevno	k1gNnSc2
–	–	k?
Osmané	Osman	k1gMnPc1
odrazili	odrazit	k5eAaPmAgMnP
ruský	ruský	k2eAgInSc4d1
útok	útok	k1gInSc4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
–	–	k?
Třetí	třetí	k4xOgFnSc1
bitva	bitva	k1gFnSc1
v	v	k7c6
průsmyku	průsmyk	k1gInSc6
Šipka	šipka	k1gFnSc1
–	–	k?
Rusové	Rus	k1gMnPc1
odrazili	odrazit	k5eAaPmAgMnP
útok	útok	k1gInSc4
Osmanů	Osman	k1gMnPc2
na	na	k7c4
průsmyk	průsmyk	k1gInSc4
Šipka	šipka	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
–	–	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Yahni	Yahen	k2eAgMnPc1d1
–	–	k?
Rusové	Rus	k1gMnPc1
dosáhli	dosáhnout	k5eAaPmAgMnP
sice	sice	k8xC
jistých	jistý	k2eAgInPc2d1
zisků	zisk	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
nebyli	být	k5eNaImAgMnP
schopní	schopný	k2eAgMnPc1d1
je	on	k3xPp3gNnSc4
udržet	udržet	k5eAaPmF
<g/>
,	,	kIx,
turecké	turecký	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
15	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
–	–	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Aladja	Aladja	k1gMnSc1
Dag	dag	kA
–	–	k?
Osmanská	osmanský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Hadži	hadži	k1gMnSc1
Resit	Resit	k1gMnSc1
Pašou	paša	k1gMnSc7
se	se	k3xPyFc4
vzdala	vzdát	k5eAaPmAgFnS
Rusům	Rus	k1gMnPc3
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
–	–	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Gorného	Gorný	k1gMnSc2
Dǎ	Dǎ	k1gMnSc2
–	–	k?
Rusové	Rus	k1gMnPc1
obsadili	obsadit	k5eAaPmAgMnP
tureckou	turecký	k2eAgFnSc4d1
pevnůstku	pevnůstka	k1gFnSc4
strážící	strážící	k2eAgFnSc2d1
zásobovací	zásobovací	k2eAgFnSc2d1
linie	linie	k1gFnSc2
ke	k	k7c3
Plevnu	Plevno	k1gNnSc3
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
–	–	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Deve	Deve	k1gFnSc6
Boyun	Boyun	k1gNnSc1
–	–	k?
ruský	ruský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Heimann	Heimann	k1gMnSc1
vyhrál	vyhrát	k5eAaPmAgMnS
další	další	k2eAgFnSc4d1
bitvu	bitva	k1gFnSc4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
–	–	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Erzurum	Erzurum	k1gInSc4
–	–	k?
Rusové	Rus	k1gMnPc1
nezvládli	zvládnout	k5eNaPmAgMnP
dobýt	dobýt	k5eAaPmF
Erzurum	Erzurum	k1gInSc4
při	při	k7c6
mohutném	mohutný	k2eAgInSc6d1
útoku	útok	k1gInSc6
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
–	–	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Kars	Kars	k1gInSc4
–	–	k?
Rusové	Rus	k1gMnPc1
dobyli	dobýt	k5eAaPmAgMnP
důležitou	důležitý	k2eAgFnSc4d1
osmanskou	osmanský	k2eAgFnSc4d1
pevnost	pevnost	k1gFnSc4
v	v	k7c6
Zakavkazsku	Zakavkazsko	k1gNnSc6
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
–	–	k?
konec	konec	k1gInSc1
obléhání	obléhání	k1gNnSc2
Plevna	Plevna	k1gFnSc1
<g/>
,	,	kIx,
turecká	turecký	k2eAgFnSc1d1
kapitulace	kapitulace	k1gFnSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
–	–	k?
Battle	Battle	k1gFnSc2
of	of	k?
Tashkessen	Tashkessen	k1gInSc1
<g/>
;	;	kIx,
při	při	k7c6
krytí	krytí	k1gNnSc6
všeobecného	všeobecný	k2eAgInSc2d1
ústupu	ústup	k1gInSc2
se	se	k3xPyFc4
Turci	Turek	k1gMnPc1
krátce	krátce	k6eAd1
postavili	postavit	k5eAaPmAgMnP
ruskému	ruský	k2eAgInSc3d1
předvoji	předvoj	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
zprvu	zprvu	k6eAd1
odrazili	odrazit	k5eAaPmAgMnP
</s>
<s>
1878	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
–	–	k?
Čtvrtá	čtvrtá	k1gFnSc1
bitva	bitva	k1gFnSc1
v	v	k7c6
průsmyku	průsmyk	k1gInSc6
Šipka	šipka	k1gFnSc1
–	–	k?
generál	generál	k1gMnSc1
Gurko	Gurko	k1gNnSc4
rozdrtil	rozdrtit	k5eAaPmAgMnS
Turky	Turek	k1gMnPc4
v	v	k7c6
průsmyku	průsmyk	k1gInSc6
Šipka	šipka	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
–	–	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Šejnova	Šejnov	k1gInSc2
–	–	k?
šipenská	šipenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
jižní	jižní	k2eAgFnSc1d1
<g/>
)	)	kIx)
skupina	skupina	k1gFnSc1
gnrl	gnrla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fjodora	Fjodora	k1gFnSc1
Radeckého	Radeckého	k2eAgMnPc2d1
zajala	zajmout	k5eAaPmAgFnS
30	#num#	k4
000	#num#	k4
Turků	turek	k1gInPc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
–	–	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Plovdivu	Plovdiv	k1gInSc2
–	–	k?
Gurko	Gurko	k1gNnSc1
obrátil	obrátit	k5eAaPmAgInS
k	k	k7c3
útěku	útěk	k1gInSc3
turecké	turecký	k2eAgFnSc2d1
síly	síla	k1gFnSc2
a	a	k8xC
přesouvá	přesouvat	k5eAaImIp3nS
se	se	k3xPyFc4
do	do	k7c2
nebezpečné	bezpečný	k2eNgFnSc2d1
vzdálenosti	vzdálenost	k1gFnSc2
od	od	k7c2
Istanbulu	Istanbul	k1gInSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
–	–	k?
dobyt	dobyt	k2eAgInSc1d1
Odrin	Odrin	k1gInSc1
(	(	kIx(
<g/>
Drinopol	Drinopol	k1gInSc1
<g/>
,	,	kIx,
Edirne	Edirn	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Stara	Star	k1gMnSc2
Zagora	Zagor	k1gMnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
,	,	kIx,
Russo-Turkish	Russo-Turkish	k1gMnSc1
War	War	k1gMnSc1
(	(	kIx(
<g/>
1877	#num#	k4
<g/>
–	–	k?
<g/>
1878	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
,	,	kIx,
Battles	Battles	k1gMnSc1
of	of	k?
the	the	k?
Russo-Turkish	Russo-Turkish	k1gMnSc1
War	War	k1gMnSc1
(	(	kIx(
<g/>
1877	#num#	k4
<g/>
–	–	k?
<g/>
1878	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
X	X	kA
wojna	wojna	k1gFnSc1
rosyjsko-turecka	rosyjsko-turecka	k1gFnSc1
na	na	k7c6
polské	polský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Timothy	Timotha	k1gFnSc2
C.	C.	kA
Dowling	Dowling	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Russia	Russia	k1gFnSc1
at	at	k?
War	War	k1gMnSc1
<g/>
:	:	kIx,
From	From	k1gMnSc1
the	the	k?
Mongol	Mongol	k1gMnSc1
Conquest	Conquest	k1gMnSc1
to	ten	k3xDgNnSc1
Afghanistan	Afghanistan	k1gInSc1
<g/>
,	,	kIx,
Chechnya	Chechnya	k1gFnSc1
<g/>
,	,	kIx,
and	and	k?
Beyond	Beyond	k1gInSc1
<g/>
.	.	kIx.
2	#num#	k4
Volumes	Volumesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ABC-CLIO	ABC-CLIO	k1gFnPc2
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
P.	P.	kA
748	#num#	k4
<g/>
↑	↑	k?
М	М	k?
<g/>
,	,	kIx,
А	А	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
–	–	k?
c.	c.	k?
376	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Russian	Russian	k1gInSc1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Citation	Citation	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
У	У	k?
Б	Б	k?
Ц	Ц	k?
<g/>
.	.	kIx.
В	В	k?
и	и	k?
н	н	k?
Е	Е	k?
<g/>
.	.	kIx.
Л	Л	k?
п	п	k?
в	в	k?
с	с	k?
е	е	k?
с	с	k?
в	в	k?
в	в	k?
XVII	XVII	kA
<g/>
—	—	k?
<g/>
XX	XX	kA
в	в	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
И	И	k?
и	и	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
С	С	k?
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
В	В	k?
в	в	k?
п	п	k?
д	д	k?
к	к	k?
(	(	kIx(
<g/>
Ч	Ч	k?
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
104	#num#	k4
<g/>
–	–	k?
<g/>
105	#num#	k4
<g/>
,	,	kIx,
129	#num#	k4
§	§	k?
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Scafes	Scafes	k1gMnSc1
<g/>
,	,	kIx,
Cornel	Cornel	k1gMnSc1
<g/>
,	,	kIx,
et	et	k?
<g/>
.	.	kIx.
al	ala	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Armata	Armata	k1gFnSc1
Romania	Romanium	k1gNnSc2
in	in	k?
Razvoiul	Razvoiul	k1gInSc1
de	de	k?
Independenta	independent	k1gMnSc4
1877	#num#	k4
<g/>
–	–	k?
<g/>
1878	#num#	k4
(	(	kIx(
<g/>
The	The	k1gFnSc6
Romanian	Romaniana	k1gFnPc2
Army	Arma	k1gFnSc2
in	in	k?
the	the	k?
War	War	k1gMnSc7
of	of	k?
Independence	Independence	k1gFnSc1
1877	#num#	k4
<g/>
–	–	k?
<g/>
1878	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bucuresti	Bucurest	k1gFnPc1
<g/>
,	,	kIx,
Editura	Editura	k1gFnSc1
Sigma	sigma	k1gNnSc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
149	#num#	k4
(	(	kIx(
<g/>
Romence	Romenec	k1gInSc2
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Б	Б	k?
У	У	k?
<g/>
,	,	kIx,
В	В	k?
и	и	k?
н	н	k?
Е	Е	k?
<g/>
,	,	kIx,
Ч	Ч	k?
II	II	kA
<g/>
,	,	kIx,
Г	Г	k?
II	II	kA
http://scepsis.net/library/id_2140.html1	http://scepsis.net/library/id_2140.html1	k4
2	#num#	k4
М	М	k?
А	А	k?
Г	Г	k?
<g/>
;	;	kIx,
С	С	k?
А	А	k?
А	А	k?
В	В	k?
и	и	k?
в	в	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Х	Х	k?
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
985	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
-	-	kIx~
<g/>
2607	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Amort	Amort	k1gInSc1
<g/>
,	,	kIx,
Čestmír	Čestmír	k1gMnSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
Bulharska	Bulharsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakladatelství	nakladatelství	k1gNnSc1
Svoboda	svoboda	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1980	#num#	k4
<g/>
.	.	kIx.
s.	s.	k?
266	#num#	k4
<g/>
↑	↑	k?
Amort	Amort	k1gInSc1
<g/>
,	,	kIx,
Čestmír	Čestmír	k1gMnSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
Bulharska	Bulharsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakladatelství	nakladatelství	k1gNnSc1
Svoboda	svoboda	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1980	#num#	k4
<g/>
.	.	kIx.
s.	s.	k?
303	#num#	k4
<g/>
–	–	k?
<g/>
304	#num#	k4
<g/>
↑	↑	k?
Amort	Amort	k1gInSc1
<g/>
,	,	kIx,
Čestmír	Čestmír	k1gMnSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
Bulharska	Bulharsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakladatelství	nakladatelství	k1gNnSc1
Svoboda	svoboda	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1980	#num#	k4
<g/>
.	.	kIx.
s.	s.	k?
281	#num#	k4
<g/>
↑	↑	k?
Ottův	Ottův	k2eAgInSc4d1
slovník	slovník	k1gInSc4
naučný	naučný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
díl	díl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
J.	J.	kA
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
1888	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
675	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Devatenácté	devatenáctý	k4xOgFnPc1
století	století	k1gNnPc2
slovem	slovo	k1gNnSc7
i	i	k8xC
obrazem	obraz	k1gInSc7
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
politické	politický	k2eAgFnPc1d1
a	a	k8xC
kulturní	kulturní	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
I.	I.	kA
Svazek	svazek	k1gInSc1
druhý	druhý	k4xOgInSc1
<g/>
,	,	kIx,
nakladatel	nakladatel	k1gMnSc1
J.	J.	kA
<g/>
R.	R.	kA
<g/>
Vilímek	Vilímek	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
690	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Amort	Amort	k1gInSc1
<g/>
,	,	kIx,
Čestmír	Čestmír	k1gMnSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
Bulharska	Bulharsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakladatelství	nakladatelství	k1gNnSc1
Svoboda	svoboda	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1980	#num#	k4
<g/>
.	.	kIx.
s.	s.	k?
278	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Čestmír	Čestmír	k1gMnSc1
Amort	Amort	k1gInSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
Bulharska	Bulharsko	k1gNnSc2
<g/>
,	,	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1980	#num#	k4
</s>
<s>
Jan	Jan	k1gMnSc1
Rychlík	Rychlík	k1gMnSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
Bulharska	Bulharsko	k1gNnSc2
<g/>
,	,	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2002	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7106-497-1	80-7106-497-1	k4
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Mayerhofer	Mayerhofer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Illustrovaná	Illustrovaný	k2eAgFnSc1d1
kroniíka	kroniík	k1gMnSc2
války	válka	k1gFnSc2
východní	východní	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
V.	V.	kA
Mayerhofer	Mayerhofer	k1gMnSc1
<g/>
,	,	kIx,
1879	#num#	k4
<g/>
.	.	kIx.
573	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Eduard	Eduard	k1gMnSc1
Rüffer	Rüffer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Der	drát	k5eAaImRp2nS
Russisch-Türkische	Russisch-Türkische	k1gNnSc4
Krieg	Krieg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Alois	Alois	k1gMnSc1
Hynek	Hynek	k1gMnSc1
<g/>
,	,	kIx,
[	[	kIx(
<g/>
187	#num#	k4
<g/>
-	-	kIx~
<g/>
]	]	kIx)
<g/>
.	.	kIx.
400	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Bulharské	bulharský	k2eAgNnSc4d1
národní	národní	k2eAgNnSc4d1
obrození	obrození	k1gNnSc4
</s>
<s>
Georgi	Georg	k1gMnSc3
Rakovski	Rakovsk	k1gMnSc3
</s>
<s>
Vasil	Vasil	k1gMnSc1
Levski	Levsk	k1gFnSc2
</s>
<s>
Christo	Christa	k1gMnSc5
Botev	Botev	k1gFnSc1
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
I.	I.	kA
Bulharský	bulharský	k2eAgInSc1d1
</s>
<s>
Sjednocení	sjednocení	k1gNnSc1
Bulharska	Bulharsko	k1gNnSc2
</s>
<s>
Stefan	Stefan	k1gMnSc1
Stambolov	Stambolov	k1gInSc4
</s>
<s>
Ivan	Ivan	k1gMnSc1
Vazov	Vazov	k1gInSc4
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Thurn-Taxis	Thurn-Taxis	k1gFnSc2
</s>
<s>
Konstantin	Konstantin	k1gMnSc1
Jireček	Jireček	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Škorpil	Škorpil	k1gMnSc1
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1
makedonsko-odrinská	makedonsko-odrinský	k2eAgFnSc1d1
revoluční	revoluční	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
Rusko-turecká	rusko-turecký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Obléhání	obléhání	k1gNnSc1
Plevna	Plevno	k1gNnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
rusko-turecká	rusko-turecký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Historie	historie	k1gFnSc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
|	|	kIx~
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
