<s desamb="1">
Propukla	propuknout	k5eAaPmAgFnS
jako	jako	k9
vyústění	vyústění	k1gNnSc4
Velké	velký	k2eAgFnSc2d1
východní	východní	k2eAgFnSc2d1
krize	krize	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
skládala	skládat	k5eAaImAgFnS
z	z	k7c2
dalších	další	k2eAgInPc2d1
konfliktů	konflikt	k1gInPc2
a	a	k8xC
povstání	povstání	k1gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
bylo	být	k5eAaImAgNnS
hercegovské	hercegovský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
<g/>
,	,	kIx,
dubnové	dubnový	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
<g/>
,	,	kIx,
srbsko-turecká	srbsko-turecký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
či	či	k8xC
razložsko-kresenské	razložsko-kresenský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
<g/>
,	,	kIx,
a	a	k8xC
i	i	k9
konferencí	konference	k1gFnPc2
a	a	k8xC
mírových	mírový	k2eAgFnPc2d1
dohod	dohoda	k1gFnPc2
jako	jako	k8xS,k8xC
byla	být	k5eAaImAgFnS
Cařihradská	cařihradský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
<g/>
,	,	kIx,
Londýnská	londýnský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
či	či	k8xC
Berlínský	berlínský	k2eAgInSc1d1
kongres	kongres	k1gInSc1
<g/>
.	.	kIx.
</s>