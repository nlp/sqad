<s>
Nehodě	nehoda	k1gFnSc3	nehoda
se	se	k3xPyFc4	se
podrobně	podrobně	k6eAd1	podrobně
věnuje	věnovat	k5eAaPmIp3nS	věnovat
samostatný	samostatný	k2eAgInSc4d1	samostatný
článek	článek	k1gInSc4	článek
Letecká	letecký	k2eAgFnSc1d1	letecká
nehoda	nehoda	k1gFnSc1	nehoda
generála	generál	k1gMnSc2	generál
Štefánika	Štefánik	k1gMnSc2	Štefánik
Milan	Milan	k1gMnSc1	Milan
Rastislav	Rastislav	k1gMnSc1	Rastislav
Štefánik	Štefánik	k1gMnSc1	Štefánik
zahynul	zahynout	k5eAaPmAgMnS	zahynout
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1919	[number]	k4	1919
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
domů	domů	k6eAd1	domů
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
při	při	k7c6	při
leteckém	letecký	k2eAgNnSc6d1	letecké
neštěstí	neštěstí	k1gNnSc6	neštěstí
<g/>
.	.	kIx.	.
</s>
