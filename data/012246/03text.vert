<p>
<s>
Luňák	Luňák	k1gMnSc1	Luňák
červený	červený	k2eAgMnSc1d1	červený
(	(	kIx(	(
<g/>
Milvus	Milvus	k1gMnSc1	Milvus
milvus	milvus	k1gMnSc1	milvus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgMnSc1d1	velký
dravec	dravec	k1gMnSc1	dravec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
jestřábovitých	jestřábovitý	k2eAgMnPc2d1	jestřábovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Většinu	většina	k1gFnSc4	většina
areálu	areál	k1gInSc2	areál
obývá	obývat	k5eAaImIp3nS	obývat
luňák	luňák	k1gMnSc1	luňák
červený	červený	k2eAgMnSc1d1	červený
evropský	evropský	k2eAgMnSc1d1	evropský
(	(	kIx(	(
<g/>
Milvus	Milvus	k1gMnSc1	Milvus
milvus	milvus	k1gMnSc1	milvus
milvus	milvus	k1gMnSc1	milvus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
další	další	k2eAgInSc4d1	další
poddruh	poddruh	k1gInSc4	poddruh
bývá	bývat	k5eAaImIp3nS	bývat
považován	považován	k2eAgMnSc1d1	považován
l.	l.	k?	l.
č.	č.	k?	č.
kapverdský	kapverdský	k2eAgMnSc1d1	kapverdský
(	(	kIx(	(
<g/>
M.	M.	kA	M.
m.	m.	k?	m.
fasciicauda	fasciicauda	k1gFnSc1	fasciicauda
<g/>
)	)	kIx)	)
obývající	obývající	k2eAgInPc1d1	obývající
Kapverské	Kapverský	k2eAgInPc1d1	Kapverský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
původně	původně	k6eAd1	původně
patrně	patrně	k6eAd1	patrně
hybridní	hybridní	k2eAgFnSc7d1	hybridní
populací	populace	k1gFnSc7	populace
s	s	k7c7	s
luňákem	luňák	k1gMnSc7	luňák
hnědým	hnědý	k2eAgMnSc7d1	hnědý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
luňák	luňák	k1gMnSc1	luňák
hnědý	hnědý	k2eAgMnSc1d1	hnědý
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
61	[number]	k4	61
<g/>
–	–	k?	–
<g/>
72	[number]	k4	72
cm	cm	kA	cm
<g/>
,	,	kIx,	,
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
140	[number]	k4	140
<g/>
–	–	k?	–
<g/>
165	[number]	k4	165
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
bílá	bílý	k2eAgNnPc1d1	bílé
pole	pole	k1gNnPc1	pole
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
části	část	k1gFnSc2	část
ručních	ruční	k2eAgFnPc2d1	ruční
letek	letka	k1gFnPc2	letka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
našich	náš	k3xOp1gMnPc2	náš
dravců	dravec	k1gMnPc2	dravec
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
dlouhým	dlouhý	k2eAgMnPc3d1	dlouhý
<g/>
,	,	kIx,	,
hluboce	hluboko	k6eAd1	hluboko
vykrojeným	vykrojený	k2eAgInSc7d1	vykrojený
ocasem	ocas	k1gInSc7	ocas
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
shora	shora	k6eAd1	shora
rezavě	rezavě	k6eAd1	rezavě
zbarvený	zbarvený	k2eAgInSc1d1	zbarvený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
evropský	evropský	k2eAgInSc4d1	evropský
typ	typ	k1gInSc4	typ
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
oblastech	oblast	k1gFnPc6	oblast
tažný	tažný	k2eAgInSc1d1	tažný
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
stálý	stálý	k2eAgInSc1d1	stálý
nebo	nebo	k8xC	nebo
potulný	potulný	k2eAgInSc1d1	potulný
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
lesích	les	k1gInPc6	les
blízko	blízko	k7c2	blízko
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
s	s	k7c7	s
volnými	volný	k2eAgNnPc7d1	volné
prostranstvími	prostranství	k1gNnPc7	prostranství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
začal	začít	k5eAaPmAgInS	začít
hnízdit	hnízdit	k5eAaImF	hnízdit
po	po	k7c6	po
stoleté	stoletý	k2eAgFnSc6d1	stoletá
pauze	pauza	k1gFnSc6	pauza
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
roztroušeně	roztroušeně	k6eAd1	roztroušeně
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
celých	celý	k2eAgFnPc6d1	celá
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
nepravidelně	pravidelně	k6eNd1	pravidelně
také	také	k9	také
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
částech	část	k1gFnPc6	část
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Početnost	početnost	k1gFnSc1	početnost
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
na	na	k7c4	na
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
jednotlivě	jednotlivě	k6eAd1	jednotlivě
i	i	k9	i
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
pouze	pouze	k6eAd1	pouze
samostatné	samostatný	k2eAgInPc4d1	samostatný
páry	pár	k1gInPc4	pár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc4	hnízdo
staví	stavit	k5eAaBmIp3nP	stavit
oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
z	z	k7c2	z
větví	větev	k1gFnPc2	větev
na	na	k7c6	na
jehličnatých	jehličnatý	k2eAgInPc6d1	jehličnatý
i	i	k8xC	i
listnatých	listnatý	k2eAgInPc6d1	listnatý
stromech	strom	k1gInPc6	strom
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
velmi	velmi	k6eAd1	velmi
vysoko	vysoko	k6eAd1	vysoko
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
základ	základ	k1gInSc1	základ
jim	on	k3xPp3gMnPc3	on
někdy	někdy	k6eAd1	někdy
slouží	sloužit	k5eAaImIp3nS	sloužit
stará	starý	k2eAgNnPc4d1	staré
hnízda	hnízdo	k1gNnPc4	hnízdo
vran	vrána	k1gFnPc2	vrána
nebo	nebo	k8xC	nebo
dravců	dravec	k1gMnPc2	dravec
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
mívá	mívat	k5eAaImIp3nS	mívat
jednu	jeden	k4xCgFnSc4	jeden
snůšku	snůška	k1gFnSc4	snůška
po	po	k7c6	po
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
bělavých	bělavý	k2eAgFnPc2d1	bělavá
<g/>
,	,	kIx,	,
řídce	řídce	k6eAd1	řídce
hnědě	hnědě	k6eAd1	hnědě
čárkovaných	čárkovaný	k2eAgFnPc2d1	čárkovaná
nebo	nebo	k8xC	nebo
skvrnitých	skvrnitý	k2eAgNnPc6d1	skvrnité
vejcích	vejce	k1gNnPc6	vejce
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
56,6	[number]	k4	56,6
×	×	k?	×
45,0	[number]	k4	45,0
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
sezení	sezení	k1gNnSc2	sezení
je	být	k5eAaImIp3nS	být
28	[number]	k4	28
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
sedí	sedit	k5eAaImIp3nS	sedit
výhradně	výhradně	k6eAd1	výhradně
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
samec	samec	k1gMnSc1	samec
většinově	většinově	k6eAd1	většinově
krmí	krmit	k5eAaImIp3nS	krmit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
krmí	krmit	k5eAaImIp3nS	krmit
mláďata	mládě	k1gNnPc1	mládě
pouze	pouze	k6eAd1	pouze
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
porcuje	porcovat	k5eAaImIp3nS	porcovat
potravu	potrava	k1gFnSc4	potrava
přinášenou	přinášený	k2eAgFnSc4d1	přinášená
samcem	samec	k1gInSc7	samec
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
loví	lovit	k5eAaImIp3nP	lovit
již	již	k6eAd1	již
oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc4	hnízdo
opouštějí	opouštět	k5eAaImIp3nP	opouštět
po	po	k7c6	po
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
dnech	den	k1gInPc6	den
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
jeden	jeden	k4xCgMnSc1	jeden
až	až	k9	až
dva	dva	k4xCgInPc1	dva
týdny	týden	k1gInPc1	týden
se	se	k3xPyFc4	se
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
ve	v	k7c6	v
větvoví	větvoví	k1gNnSc6	větvoví
poblíž	poblíž	k7c2	poblíž
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
převládají	převládat	k5eAaImIp3nP	převládat
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
savci	savec	k1gMnPc1	savec
a	a	k8xC	a
ryby	ryba	k1gFnPc1	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Požírá	požírat	k5eAaImIp3nS	požírat
hlavně	hlavně	k9	hlavně
mršiny	mršina	k1gFnPc4	mršina
<g/>
,	,	kIx,	,
častý	častý	k2eAgInSc1d1	častý
je	být	k5eAaImIp3nS	být
také	také	k9	také
potravní	potravní	k2eAgInSc1d1	potravní
parazitismus	parazitismus	k1gInSc1	parazitismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
požírá	požírat	k5eAaImIp3nS	požírat
i	i	k9	i
odpadky	odpadek	k1gInPc4	odpadek
<g/>
,	,	kIx,	,
za	za	k7c7	za
kterými	který	k3yRgMnPc7	který
zaletuje	zaletovat	k5eAaPmIp3nS	zaletovat
na	na	k7c4	na
smetiště	smetiště	k1gNnSc4	smetiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
luňák	luňák	k1gMnSc1	luňák
červený	červený	k2eAgMnSc1d1	červený
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
luňák	luňák	k1gMnSc1	luňák
červený	červený	k2eAgMnSc1d1	červený
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Milvus	Milvus	k1gInSc1	Milvus
milvus	milvus	k1gInSc1	milvus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Milvus	Milvus	k1gInSc1	Milvus
milvus	milvus	k1gInSc4	milvus
na	na	k7c4	na
BioLib	BioLib	k1gInSc4	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Luňák	Luňák	k1gMnSc1	Luňák
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Liberec	Liberec	k1gInSc1	Liberec
</s>
</p>
