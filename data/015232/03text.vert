<s>
Ústav	ústav	k1gInSc1
zdravotnických	zdravotnický	k2eAgFnPc2d1
informací	informace	k1gFnPc2
a	a	k8xC
statistiky	statistika	k1gFnSc2
ČR	ČR	kA
</s>
<s>
Ústav	ústav	k1gInSc1
zdravotnických	zdravotnický	k2eAgFnPc2d1
informací	informace	k1gFnPc2
a	a	k8xC
statistiky	statistika	k1gFnSc2
ČR	ČR	kA
Vznik	vznik	k1gInSc1
</s>
<s>
1960	#num#	k4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
organizační	organizační	k2eAgFnSc1d1
složka	složka	k1gFnSc1
státu	stát	k1gInSc2
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Palackého	Palackého	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
375	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
128	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
23,51	23,51	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
55,63	55,63	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Předseda	předseda	k1gMnSc1
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Dušek	Dušek	k1gMnSc1
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.uzis.cz	www.uzis.cz	k1gInSc1
Datová	datový	k2eAgFnSc1d1
schránka	schránka	k1gFnSc1
</s>
<s>
kb	kb	kA
<g/>
9	#num#	k4
<g/>
egte	egt	k1gFnSc2
IČO	IČO	kA
</s>
<s>
00023833	#num#	k4
(	(	kIx(
<g/>
VR	vr	k0
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ústav	ústav	k1gInSc1
zdravotnických	zdravotnický	k2eAgFnPc2d1
informací	informace	k1gFnPc2
a	a	k8xC
statistiky	statistika	k1gFnSc2
ČR	ČR	kA
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
ÚZIS	ÚZIS	kA
ČR	ČR	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
organizační	organizační	k2eAgFnSc1d1
složka	složka	k1gFnSc1
státu	stát	k1gInSc2
zřízená	zřízený	k2eAgFnSc1d1
Ministerstvem	ministerstvo	k1gNnSc7
zdravotnictví	zdravotnictví	k1gNnSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
ÚZIS	ÚZIS	kA
ČR	ČR	kA
existuje	existovat	k5eAaImIp3nS
od	od	k7c2
svého	svůj	k3xOyFgNnSc2
zřízení	zřízení	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
a	a	k8xC
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
státní	státní	k2eAgFnSc2d1
statistické	statistický	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
,	,	kIx,
svoji	svůj	k3xOyFgFnSc4
činnost	činnost	k1gFnSc4
vykonává	vykonávat	k5eAaImIp3nS
na	na	k7c6
podkladě	podklad	k1gInSc6
zákona	zákon	k1gInSc2
číslo	číslo	k1gNnSc4
89	#num#	k4
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
státní	státní	k2eAgFnSc6d1
statistické	statistický	k2eAgFnSc6d1
službě	služba	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předsedou	předseda	k1gMnSc7
ústavu	ústav	k1gInSc2
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
biostatistik	biostatistika	k1gFnPc2
Ladislav	Ladislav	k1gMnSc1
Dušek	Dušek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
počátku	počátek	k1gInSc2
své	svůj	k3xOyFgFnSc2
existence	existence	k1gFnSc2
slouží	sloužit	k5eAaImIp3nS
pro	pro	k7c4
sběr	sběr	k1gInSc4
a	a	k8xC
zpracování	zpracování	k1gNnSc4
zdravotnických	zdravotnický	k2eAgFnPc2d1
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
zejména	zejména	k9
v	v	k7c6
rámci	rámec	k1gInSc6
Národního	národní	k2eAgInSc2d1
zdravotnického	zdravotnický	k2eAgInSc2d1
informačního	informační	k2eAgInSc2d1
systému	systém	k1gInSc2
(	(	kIx(
<g/>
NZIS	NZIS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
k	k	k7c3
vedení	vedení	k1gNnSc3
národních	národní	k2eAgInPc2d1
zdravotních	zdravotní	k2eAgInPc2d1
registrů	registr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Činnost	činnost	k1gFnSc1
NZIS	NZIS	kA
je	být	k5eAaImIp3nS
tímto	tento	k3xDgInSc7
ústavem	ústav	k1gInSc7
řízena	řídit	k5eAaImNgFnS
a	a	k8xC
koordinována	koordinovat	k5eAaBmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
NZIS	NZIS	kA
je	být	k5eAaImIp3nS
definován	definovat	k5eAaBmNgInS
zákonem	zákon	k1gInSc7
číslo	číslo	k1gNnSc1
372	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
zdravotních	zdravotní	k2eAgFnPc6d1
službách	služba	k1gFnPc6
a	a	k8xC
podmínkách	podmínka	k1gFnPc6
jejich	jejich	k3xOp3gNnSc2
poskytování	poskytování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ÚZIS	ÚZIS	kA
ČR	ČR	kA
je	být	k5eAaImIp3nS
oprávněn	oprávněn	k2eAgInSc1d1
předkládat	předkládat	k5eAaImF
oficiální	oficiální	k2eAgFnPc4d1
informace	informace	k1gFnPc4
získané	získaný	k2eAgFnPc4d1
z	z	k7c2
NZIS	NZIS	kA
za	za	k7c4
celou	celý	k2eAgFnSc4d1
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Jakožto	jakožto	k8xS
orgán	orgán	k1gInSc1
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
spolupracuje	spolupracovat	k5eAaImIp3nS
s	s	k7c7
ostatními	ostatní	k2eAgInPc7d1
státními	státní	k2eAgInPc7d1
orgány	orgán	k1gInPc7
<g/>
,	,	kIx,
zde	zde	k6eAd1
zejména	zejména	k9
s	s	k7c7
Českým	český	k2eAgInSc7d1
statistickým	statistický	k2eAgInSc7d1
úřadem	úřad	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
sběr	sběr	k1gInSc4
dat	datum	k1gNnPc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
spolupracuje	spolupracovat	k5eAaImIp3nS
s	s	k7c7
nemocnicemi	nemocnice	k1gFnPc7
<g/>
,	,	kIx,
sdruženími	sdružení	k1gNnPc7
lékařů	lékař	k1gMnPc2
a	a	k8xC
farmaceutů	farmaceut	k1gMnPc2
<g/>
,	,	kIx,
</s>
<s>
zdravotními	zdravotní	k2eAgFnPc7d1
pojišťovnami	pojišťovna	k1gFnPc7
<g/>
,	,	kIx,
odbornými	odborný	k2eAgFnPc7d1
lékařskými	lékařský	k2eAgFnPc7d1
společnostmi	společnost	k1gFnPc7
<g/>
,	,	kIx,
na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
pak	pak	k6eAd1
s	s	k7c7
organizacemi	organizace	k1gFnPc7
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
WHO	WHO	kA
(	(	kIx(
<g/>
Světová	světový	k2eAgFnSc1d1
zdravotnická	zdravotnický	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
OECD	OECD	kA
(	(	kIx(
<g/>
Organizace	organizace	k1gFnSc1
pro	pro	k7c4
hospodářskou	hospodářský	k2eAgFnSc4d1
spolupráci	spolupráce	k1gFnSc4
a	a	k8xC
rozvoj	rozvoj	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
OSN	OSN	kA
<g/>
,	,	kIx,
Eurostat	Eurostat	k1gInSc1
a	a	k8xC
dalšími	další	k2eAgMnPc7d1
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
veškeré	veškerý	k3xTgFnSc6
své	svůj	k3xOyFgFnSc6
činnosti	činnost	k1gFnSc6
je	být	k5eAaImIp3nS
povinen	povinen	k2eAgMnSc1d1
dodržovat	dodržovat	k5eAaImF
platné	platný	k2eAgFnPc4d1
právní	právní	k2eAgFnPc4d1
normy	norma	k1gFnPc4
na	na	k7c4
ochranu	ochrana	k1gFnSc4
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
jednotlivých	jednotlivý	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
vyplývající	vyplývající	k2eAgInPc1d1
ze	z	k7c2
zákona	zákon	k1gInSc2
101	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
ochraně	ochrana	k1gFnSc6
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
</s>
<s>
Státní	státní	k2eAgFnSc1d1
statistická	statistický	k2eAgFnSc1d1
služba	služba	k1gFnSc1
</s>
<s>
Národní	národní	k2eAgInSc1d1
zdravotnický	zdravotnický	k2eAgInSc1d1
informační	informační	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Státní	státní	k2eAgInSc1d1
zdravotní	zdravotní	k2eAgInSc1d1
ústav	ústav	k1gInSc1
</s>
<s>
Státní	státní	k2eAgInSc1d1
ústav	ústav	k1gInSc1
pro	pro	k7c4
kontrolu	kontrola	k1gFnSc4
léčiv	léčivo	k1gNnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Ústav	ústav	k1gInSc1
zdravotnických	zdravotnický	k2eAgFnPc2d1
informací	informace	k1gFnPc2
a	a	k8xC
statistiky	statistika	k1gFnSc2
ČR	ČR	kA
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Koordinační	koordinační	k2eAgNnSc4d1
středisko	středisko	k1gNnSc4
pro	pro	k7c4
rezortní	rezortní	k2eAgInPc4d1
zdravotnické	zdravotnický	k2eAgInPc4d1
informační	informační	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
,	,	kIx,
KSRZIS	KSRZIS	kA
—	—	k?
přesměrování	přesměrování	k1gNnSc1
na	na	k7c4
uzis	uzis	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
nebo	nebo	k8xC
vstup	vstup	k1gInSc1
do	do	k7c2
zdravotnických	zdravotnický	k2eAgInPc2d1
registrů	registr	k1gInPc2
(	(	kIx(
<g/>
eREG	eREG	k?
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20020321021	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
10003463-9	10003463-9	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2231	#num#	k4
0366	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
93002989	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
156416744	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
93002989	#num#	k4
</s>
