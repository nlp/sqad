<s>
Beyblade	Beyblad	k1gInSc5	Beyblad
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
Japonec	Japonec	k1gMnSc1	Japonec
Aoki	Aok	k1gFnSc2	Aok
Takao	Takao	k1gMnSc1	Takao
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
souboji	souboj	k1gInSc6	souboj
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
beybladeů	beyblade	k1gMnPc2	beyblade
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
čti	číst	k5eAaImRp2nS	číst
bejblejdů	bejblejd	k1gInPc2	bejblejd
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Beyblade	Beyblad	k1gInSc5	Beyblad
je	on	k3xPp3gNnPc4	on
v	v	k7c6	v
principu	princip	k1gInSc6	princip
káča	káča	k1gFnSc1	káča
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
ovšem	ovšem	k9	ovšem
dřevěný	dřevěný	k2eAgMnSc1d1	dřevěný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
plastový	plastový	k2eAgInSc1d1	plastový
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
kovový	kovový	k2eAgMnSc1d1	kovový
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
několika	několik	k4yIc2	několik
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
Face	Face	k1gInSc4	Face
bolt	bolt	k2eAgInSc4d1	bolt
(	(	kIx(	(
<g/>
Krycí	krycí	k2eAgInSc4d1	krycí
šroub	šroub	k1gInSc4	šroub
<g/>
)	)	kIx)	)
Energy	Energ	k1gInPc4	Energ
ring	ring	k1gInSc1	ring
(	(	kIx(	(
<g/>
Energo	Energo	k6eAd1	Energo
prsten	prsten	k1gInSc1	prsten
<g/>
)	)	kIx)	)
Fusion	Fusion	k1gInSc1	Fusion
wheel	wheel	k1gInSc1	wheel
(	(	kIx(	(
<g/>
Kovový	kovový	k2eAgInSc1d1	kovový
prstenec	prstenec	k1gInSc1	prstenec
<g/>
)	)	kIx)	)
Spin	spin	k1gInSc1	spin
track	track	k1gInSc1	track
(	(	kIx(	(
<g/>
Rotační	rotační	k2eAgInSc1d1	rotační
nástavec	nástavec	k1gInSc1	nástavec
<g/>
)	)	kIx)	)
Performance	performance	k1gFnSc1	performance
tip	tip	k1gInSc1	tip
(	(	kIx(	(
<g/>
Výkonnostní	výkonnostní	k2eAgFnSc1d1	výkonnostní
špička	špička	k1gFnSc1	špička
<g/>
,	,	kIx,	,
<g/>
nebo	nebo	k8xC	nebo
<g/>
-li	i	k?	-li
typ	typ	k1gInSc1	typ
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
)	)	kIx)	)
Lze	lze	k6eAd1	lze
jej	on	k3xPp3gInSc4	on
kombinovat	kombinovat	k5eAaImF	kombinovat
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
částmi	část	k1gFnPc7	část
kotoučů	kotouč	k1gInPc2	kotouč
<g/>
.	.	kIx.	.
</s>
<s>
Roztáčí	roztáčet	k5eAaImIp3nS	roztáčet
se	se	k3xPyFc4	se
speciálním	speciální	k2eAgMnSc7d1	speciální
roztáčečem	roztáčeč	k1gMnSc7	roztáčeč
či	či	k8xC	či
vystřelovačem	vystřelovač	k1gInSc7	vystřelovač
(	(	kIx(	(
<g/>
Launcher	Launchra	k1gFnPc2	Launchra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
ozubeného	ozubený	k2eAgInSc2d1	ozubený
drátku	drátek	k1gInSc2	drátek
(	(	kIx(	(
<g/>
tip	tip	k1gInSc1	tip
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
roztáčeče	roztáčeč	k1gMnSc2	roztáčeč
vsune	vsunout	k5eAaPmIp3nS	vsunout
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
trhnutím	trhnutí	k1gNnSc7	trhnutí
tohoto	tento	k3xDgInSc2	tento
provázku	provázek	k1gInSc2	provázek
se	se	k3xPyFc4	se
beyblade	beyblad	k1gInSc5	beyblad
roztočí	roztočit	k5eAaPmIp3nS	roztočit
<g/>
.	.	kIx.	.
</s>
<s>
Hru	hra	k1gFnSc4	hra
vyhrává	vyhrávat	k5eAaImIp3nS	vyhrávat
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
vyhodí	vyhodit	k5eAaPmIp3nS	vyhodit
soupeřův	soupeřův	k2eAgInSc1d1	soupeřův
beyblade	beyblad	k1gInSc5	beyblad
ven	ven	k6eAd1	ven
ze	z	k7c2	z
speciální	speciální	k2eAgFnSc2d1	speciální
beyarény	beyaréna	k1gFnSc2	beyaréna
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
typy	typ	k1gInPc1	typ
beybladů	beyblad	k1gInPc2	beyblad
jsou	být	k5eAaImIp3nP	být
útočné	útočný	k2eAgFnPc1d1	útočná
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
obranné	obranný	k2eAgFnPc1d1	obranná
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgMnSc7d1	oficiální
výrobcem	výrobce	k1gMnSc7	výrobce
beybladů	beyblad	k1gInPc2	beyblad
<g/>
,	,	kIx,	,
beyarén	beyarén	k1gInSc1	beyarén
a	a	k8xC	a
příslušenství	příslušenství	k1gNnSc1	příslušenství
je	být	k5eAaImIp3nS	být
firma	firma	k1gFnSc1	firma
Hasbro	Hasbro	k1gNnSc4	Hasbro
<g/>
.	.	kIx.	.
</s>
<s>
Nadšenci	nadšenec	k1gMnPc5	nadšenec
do	do	k7c2	do
beybladů	beyblad	k1gInPc2	beyblad
<g/>
,	,	kIx,	,
upřednostňují	upřednostňovat	k5eAaImIp3nP	upřednostňovat
firmu	firma	k1gFnSc4	firma
Takara	Takar	k1gMnSc4	Takar
Tomy	Tomy	k1gMnSc1	Tomy
<g/>
.	.	kIx.	.
</s>
<s>
Takara	Takara	k1gFnSc1	Takara
Tomy	Tomy	k1gMnSc1	Tomy
dělá	dělat	k5eAaImIp3nS	dělat
ty	ten	k3xDgInPc4	ten
nejrealističtější	realistický	k2eAgInPc4d3	nejrealističtější
beyblady	beyblad	k1gInPc4	beyblad
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hra	hra	k1gFnSc1	hra
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
hru	hra	k1gFnSc4	hra
hraje	hrát	k5eAaImIp3nS	hrát
asi	asi	k9	asi
10	[number]	k4	10
miliónů	milión	k4xCgInPc2	milión
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
rozšíření	rozšíření	k1gNnSc2	rozšíření
hry	hra	k1gFnSc2	hra
byl	být	k5eAaImAgMnS	být
anime	animat	k5eAaPmIp3nS	animat
seriál	seriál	k1gInSc4	seriál
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velice	velice	k6eAd1	velice
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
byl	být	k5eAaImAgMnS	být
Aoki	Aoke	k1gFnSc4	Aoke
Takao	Takao	k6eAd1	Takao
nucen	nutit	k5eAaImNgMnS	nutit
vytvořit	vytvořit	k5eAaPmF	vytvořit
další	další	k2eAgFnPc4d1	další
tři	tři	k4xCgFnPc4	tři
sezóny	sezóna	k1gFnPc4	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
každou	každý	k3xTgFnSc7	každý
novou	nový	k2eAgFnSc7d1	nová
sezónou	sezóna	k1gFnSc7	sezóna
byl	být	k5eAaImAgMnS	být
vidět	vidět	k5eAaImF	vidět
technologický	technologický	k2eAgInSc4d1	technologický
krok	krok	k1gInSc4	krok
vpřed	vpřed	k6eAd1	vpřed
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
animaci	animace	k1gFnSc6	animace
soubojů	souboj	k1gInPc2	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
obyčejný	obyčejný	k2eAgMnSc1d1	obyčejný
kluk	kluk	k1gMnSc1	kluk
a	a	k8xC	a
beybladový	beybladový	k2eAgMnSc1d1	beybladový
nadšenec	nadšenec	k1gMnSc1	nadšenec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
tvrdohlavý	tvrdohlavý	k2eAgMnSc1d1	tvrdohlavý
a	a	k8xC	a
zbrklý	zbrklý	k2eAgMnSc1d1	zbrklý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
skvělý	skvělý	k2eAgMnSc1d1	skvělý
kamarád	kamarád	k1gMnSc1	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Bladeové	Bladeová	k1gFnSc2	Bladeová
Smršti	smršť	k1gFnSc3	smršť
<g/>
,	,	kIx,	,
žil	žít	k5eAaImAgMnS	žít
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
dědou	děda	k1gMnSc7	děda
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gMnSc4	on
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gFnSc3	jeho
vůli	vůle	k1gFnSc3	vůle
učil	učít	k5eAaPmAgMnS	učít
kendo	kendo	k1gNnSc4	kendo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
pak	pak	k6eAd1	pak
v	v	k7c6	v
soubojích	souboj	k1gInPc6	souboj
hodně	hodně	k6eAd1	hodně
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Ducha	duch	k1gMnSc2	duch
Draka	drak	k1gMnSc2	drak
získal	získat	k5eAaPmAgMnS	získat
ze	z	k7c2	z
starodávného	starodávný	k2eAgInSc2d1	starodávný
rodinného	rodinný	k2eAgInSc2d1	rodinný
meče	meč	k1gInSc2	meč
vystaveného	vystavený	k2eAgInSc2d1	vystavený
v	v	k7c6	v
tělocvičně	tělocvična	k1gFnSc6	tělocvična
kenda	kendo	k1gNnSc2	kendo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odhodlaný	odhodlaný	k2eAgMnSc1d1	odhodlaný
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
hráčem	hráč	k1gMnSc7	hráč
beybladu	beyblad	k1gInSc2	beyblad
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
Tysonových	Tysonův	k2eAgMnPc2d1	Tysonův
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
kamarádů	kamarád	k1gMnPc2	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
Kenny	Kenna	k1gFnPc4	Kenna
je	být	k5eAaImIp3nS	být
počítačový	počítačový	k2eAgMnSc1d1	počítačový
maniak	maniak	k1gMnSc1	maniak
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
beybladeový	beybladeový	k2eAgMnSc1d1	beybladeový
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
duchem	duch	k1gMnSc7	duch
Dizzy	Dizza	k1gFnSc2	Dizza
<g/>
,	,	kIx,	,
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
náhodou	náhoda	k1gFnSc7	náhoda
uvězněném	uvězněný	k2eAgInSc6d1	uvězněný
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
laptopu	laptop	k1gInSc6	laptop
<g/>
,	,	kIx,	,
neustále	neustále	k6eAd1	neustále
získávají	získávat	k5eAaImIp3nP	získávat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
soupeřích	soupeř	k1gMnPc6	soupeř
<g/>
,	,	kIx,	,
vylepšují	vylepšovat	k5eAaImIp3nP	vylepšovat
beyblady	beyblad	k1gInPc4	beyblad
celého	celý	k2eAgInSc2d1	celý
týmu	tým	k1gInSc2	tým
Bladeové	Bladeové	k2eAgFnSc3d1	Bladeové
Smršti	smršť	k1gFnSc3	smršť
a	a	k8xC	a
dávají	dávat	k5eAaImIp3nP	dávat
ostatním	ostatní	k2eAgMnPc3d1	ostatní
užitečné	užitečný	k2eAgFnPc4d1	užitečná
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
něj	on	k3xPp3gNnSc2	on
si	se	k3xPyFc3	se
tým	tým	k1gInSc1	tým
beybladeové	beybladeová	k1gFnPc4	beybladeová
smršti	smršť	k1gFnSc3	smršť
nedovede	dovést	k5eNaPmIp3nS	dovést
nikdo	nikdo	k3yNnSc1	nikdo
představit	představit	k5eAaPmF	představit
<g/>
.	.	kIx.	.
</s>
<s>
Kai	Kai	k?	Kai
je	být	k5eAaImIp3nS	být
záhadný	záhadný	k2eAgInSc1d1	záhadný
<g/>
,	,	kIx,	,
samotářský	samotářský	k2eAgInSc1d1	samotářský
<g/>
,	,	kIx,	,
vysoký	vysoký	k2eAgInSc1d1	vysoký
<g/>
,	,	kIx,	,
tichý	tichý	k2eAgInSc1d1	tichý
a	a	k8xC	a
sebevědomý	sebevědomý	k2eAgMnSc1d1	sebevědomý
kluk	kluk	k1gMnSc1	kluk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vždy	vždy	k6eAd1	vždy
zachová	zachovat	k5eAaPmIp3nS	zachovat
chladnou	chladný	k2eAgFnSc4d1	chladná
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Nebýt	být	k5eNaImF	být
jeho	jeho	k3xOp3gMnSc4	jeho
dědečka	dědeček	k1gMnSc4	dědeček
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
by	by	kYmCp3nS	by
s	s	k7c7	s
beybladingem	beyblading	k1gInSc7	beyblading
nezačal	začít	k5eNaPmAgMnS	začít
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Nejdřív	dříve	k6eAd3	dříve
byl	být	k5eAaImAgMnS	být
vůdcem	vůdce	k1gMnSc7	vůdce
gangu	gang	k1gInSc2	gang
Žraloků	žralok	k1gMnPc2	žralok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obíral	obírat	k5eAaImAgInS	obírat
a	a	k8xC	a
ničil	ničit	k5eAaImAgInS	ničit
beyblady	beyblad	k1gInPc4	beyblad
ostatním	ostatní	k2eAgMnPc3d1	ostatní
hráčům	hráč	k1gMnPc3	hráč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
beybladeového	beybladeový	k2eAgInSc2d1	beybladeový
turnaje	turnaj	k1gInSc2	turnaj
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
po	po	k7c6	po
návrhu	návrh	k1gInSc6	návrh
pana	pan	k1gMnSc2	pan
Dickinsona	Dickinson	k1gMnSc2	Dickinson
kapitánem	kapitán	k1gMnSc7	kapitán
týmu	tým	k1gInSc2	tým
Bladeová	Bladeový	k2eAgFnSc1d1	Bladeový
Smršť	smršť	k1gFnSc1	smršť
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
duchem	duch	k1gMnSc7	duch
je	být	k5eAaImIp3nS	být
ohnivý	ohnivý	k2eAgMnSc1d1	ohnivý
fénix	fénix	k1gMnSc1	fénix
Dranzer	Dranzer	k1gMnSc1	Dranzer
<g/>
.	.	kIx.	.
</s>
<s>
Kai	Kai	k?	Kai
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
duch	duch	k1gMnSc1	duch
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
s	s	k7c7	s
beybladingem	beyblading	k1gInSc7	beyblading
neskončí	skončit	k5eNaPmIp3nS	skončit
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
všem	všecek	k3xTgMnPc3	všecek
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
Dranzerem	Dranzero	k1gNnSc7	Dranzero
dokáže	dokázat	k5eAaPmIp3nS	dokázat
velké	velký	k2eAgFnPc4d1	velká
věci	věc	k1gFnPc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
amerického	americký	k2eAgNnSc2d1	americké
výrobce	výrobce	k1gMnPc4	výrobce
a	a	k8xC	a
prodavače	prodavač	k1gMnPc4	prodavač
beybladů	beyblad	k1gInPc2	beyblad
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
máma	máma	k1gFnSc1	máma
pracuje	pracovat	k5eAaImIp3nS	pracovat
ve	v	k7c6	v
výzkumném	výzkumný	k2eAgNnSc6d1	výzkumné
středisku	středisko	k1gNnSc6	středisko
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
nestýká	stýkat	k5eNaImIp3nS	stýkat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Maxe	Max	k1gMnSc2	Max
moc	moc	k6eAd1	moc
mrzí	mrzet	k5eAaImIp3nS	mrzet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
dílech	dílo	k1gNnPc6	dílo
se	se	k3xPyFc4	se
přestěhuje	přestěhovat	k5eAaPmIp3nS	přestěhovat
a	a	k8xC	a
pozná	poznat	k5eAaPmIp3nS	poznat
s	s	k7c7	s
Tysonem	Tyson	k1gMnSc7	Tyson
a	a	k8xC	a
Kennym	Kennym	k1gInSc4	Kennym
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
energický	energický	k2eAgMnSc1d1	energický
kluk	kluk	k1gMnSc1	kluk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
rád	rád	k6eAd1	rád
srandu	sranda	k1gFnSc4	sranda
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
skvělý	skvělý	k2eAgMnSc1d1	skvělý
kamarád	kamarád	k1gMnSc1	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
duch	duch	k1gMnSc1	duch
Draciel	Draciel	k1gMnSc1	Draciel
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
jeho	on	k3xPp3gInSc2	on
beybladu	beyblad	k1gInSc2	beyblad
pomocí	pomocí	k7c2	pomocí
přívěsku	přívěsek	k1gInSc2	přívěsek
pro	pro	k7c4	pro
štěstí	štěstí	k1gNnSc4	štěstí
po	po	k7c6	po
babičce	babička	k1gFnSc6	babička
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Tysonem	Tyson	k1gMnSc7	Tyson
je	být	k5eAaImIp3nS	být
skvělá	skvělý	k2eAgFnSc1d1	skvělá
dvojka	dvojka	k1gFnSc1	dvojka
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
týmu	tým	k1gInSc2	tým
Bladeová	Bladeová	k1gFnSc1	Bladeová
Smršť	smršť	k1gFnSc1	smršť
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
čínském	čínský	k2eAgInSc6d1	čínský
týmu	tým	k1gInSc6	tým
Bílých	bílý	k2eAgMnPc2d1	bílý
Tygrů	tygr	k1gMnPc2	tygr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
šel	jít	k5eAaImAgMnS	jít
hledat	hledat	k5eAaImF	hledat
soupeře	soupeř	k1gMnPc4	soupeř
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jej	on	k3xPp3gMnSc4	on
dokáží	dokázat	k5eAaPmIp3nP	dokázat
porazit	porazit	k5eAaPmF	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
na	na	k7c6	na
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gMnSc4	on
dodal	dodat	k5eAaPmAgMnS	dodat
pan	pan	k1gMnSc1	pan
Dickinson	Dickinson	k1gMnSc1	Dickinson
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
týmu	tým	k1gInSc2	tým
Bladeová	Bladeový	k2eAgFnSc1d1	Bladeový
Smršť	smršť	k1gFnSc1	smršť
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
potká	potkat	k5eAaPmIp3nS	potkat
s	s	k7c7	s
Týmem	tým	k1gInSc7	tým
Tygrů	tygr	k1gMnPc2	tygr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
všemi	všecek	k3xTgFnPc7	všecek
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
zrádce	zrádce	k1gMnSc4	zrádce
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgMnSc4	svůj
ducha	duch	k1gMnSc4	duch
Drigera	Driger	k1gMnSc4	Driger
zdědil	zdědit	k5eAaPmAgMnS	zdědit
po	po	k7c6	po
svých	svůj	k3xOyFgInPc6	svůj
předcích	předek	k1gInPc6	předek
<g/>
.	.	kIx.	.
</s>
<s>
Ray	Ray	k?	Ray
je	být	k5eAaImIp3nS	být
rozvážný	rozvážný	k2eAgMnSc1d1	rozvážný
<g/>
,	,	kIx,	,
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
a	a	k8xC	a
kamarádský	kamarádský	k2eAgMnSc1d1	kamarádský
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
vedení	vedení	k1gNnSc2	vedení
BBA	BBA	kA	BBA
(	(	kIx(	(
<g/>
beybladové	beybladový	k2eAgFnSc2d1	beybladový
aliance	aliance	k1gFnSc2	aliance
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
on	on	k3xPp3gMnSc1	on
založil	založit	k5eAaPmAgMnS	založit
tým	tým	k1gInSc4	tým
Bladeová	Bladeový	k2eAgFnSc1d1	Bladeový
Smršť	smršť	k1gFnSc1	smršť
<g/>
.	.	kIx.	.
</s>
<s>
Metal	metal	k1gInSc1	metal
fusion	fusion	k1gInSc1	fusion
je	být	k5eAaImIp3nS	být
pokračování	pokračování	k1gNnSc4	pokračování
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
postavami	postava	k1gFnPc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
Beybladu	Beyblad	k1gInSc2	Beyblad
Před	před	k7c7	před
dávnými	dávný	k2eAgInPc7d1	dávný
časy	čas	k1gInPc7	čas
<g/>
,	,	kIx,	,
<g/>
dlouho	dlouho	k6eAd1	dlouho
před	před	k7c7	před
existencí	existence	k1gFnSc7	existence
beybladové	beybladový	k2eAgFnSc2d1	beybladový
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
spadla	spadnout	k5eAaPmAgFnS	spadnout
velká	velký	k2eAgFnSc1d1	velká
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
<g/>
tahle	tenhle	k3xDgFnSc1	tenhle
hvězda	hvězda	k1gFnSc1	hvězda
nebyla	být	k5eNaImAgFnS	být
jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
,	,	kIx,	,
<g/>
která	který	k3yRgFnSc1	který
spadla	spadnout	k5eAaPmAgFnS	spadnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bezejmenné	bezejmenný	k2eAgFnSc6d1	bezejmenná
daleké	daleký	k2eAgFnSc6d1	daleká
zemi	zem	k1gFnSc6	zem
spadla	spadnout	k5eAaPmAgFnS	spadnout
druhá	druhý	k4xOgFnSc1	druhý
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hvězdě	hvězda	k1gFnSc6	hvězda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spadla	spadnout	k5eAaPmAgFnS	spadnout
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ukryta	ukryt	k2eAgFnSc1d1	ukryta
zlá	zlý	k2eAgFnSc1d1	zlá
temná	temný	k2eAgFnSc1d1	temná
moc	moc	k1gFnSc1	moc
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tam	tam	k6eAd1	tam
žili	žít	k5eAaImAgMnP	žít
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
touto	tento	k3xDgFnSc7	tento
mocí	moc	k1gFnSc7	moc
očarováni	očarován	k2eAgMnPc1d1	očarován
a	a	k8xC	a
z	z	k7c2	z
úlomku	úlomek	k1gInSc2	úlomek
hvězdy	hvězda	k1gFnSc2	hvězda
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
špičku	špička	k1gFnSc4	špička
(	(	kIx(	(
<g/>
základ	základ	k1gInSc4	základ
<g/>
)	)	kIx)	)
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
L-draga	Lrag	k1gMnSc2	L-drag
<g/>
.	.	kIx.	.
</s>
<s>
Zlá	zlý	k2eAgFnSc1d1	zlá
špička	špička	k1gFnSc1	špička
byla	být	k5eAaImAgFnS	být
nějak	nějak	k6eAd1	nějak
schopna	schopen	k2eAgFnSc1d1	schopna
pohlcovat	pohlcovat	k5eAaImF	pohlcovat
pocity	pocit	k1gInPc4	pocit
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
touhu	touha	k1gFnSc4	touha
<g/>
,	,	kIx,	,
<g/>
vztek	vztek	k1gInSc4	vztek
a	a	k8xC	a
nenávist	nenávist	k1gFnSc4	nenávist
a	a	k8xC	a
proměnit	proměnit	k5eAaPmF	proměnit
tyto	tento	k3xDgInPc4	tento
pocity	pocit	k1gInPc4	pocit
v	v	k7c4	v
temnou	temný	k2eAgFnSc4d1	temná
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
ohromnou	ohromný	k2eAgFnSc7d1	ohromná
silou	síla	k1gFnSc7	síla
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
svět	svět	k1gInSc4	svět
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
čas	čas	k1gInSc4	čas
plynul	plynout	k5eAaImAgMnS	plynout
<g/>
.	.	kIx.	.
</s>
<s>
Ďábelský	ďábelský	k2eAgMnSc1d1	ďábelský
Bey	Bey	k1gMnSc1	Bey
přecházel	přecházet	k5eAaImAgMnS	přecházet
z	z	k7c2	z
člověka	člověk	k1gMnSc2	člověk
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
věků	věk	k1gInPc2	věk
si	se	k3xPyFc3	se
uchovával	uchovávat	k5eAaImAgMnS	uchovávat
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
měnil	měnit	k5eAaImAgMnS	měnit
formu	forma	k1gFnSc4	forma
až	až	k9	až
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
L-drago	Lrago	k6eAd1	L-drago
se	se	k3xPyFc4	se
zrodil	zrodit	k5eAaPmAgInS	zrodit
z	z	k7c2	z
chamtivosti	chamtivost	k1gFnSc2	chamtivost
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
stával	stávat	k5eAaImAgInS	stávat
se	se	k3xPyFc4	se
mocnějším	mocný	k2eAgInPc3d2	mocnější
a	a	k8xC	a
mocnějším	mocný	k2eAgInPc3d2	mocnější
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
porážce	porážka	k1gFnSc3	porážka
bylo	být	k5eAaImAgNnS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
postavit	postavit	k5eAaPmF	postavit
se	se	k3xPyFc4	se
temné	temný	k2eAgFnSc3d1	temná
síle	síla	k1gFnSc3	síla
světlem	světlo	k1gNnSc7	světlo
naděje	naděje	k1gFnSc2	naděje
(	(	kIx(	(
<g/>
čistou	čistý	k2eAgFnSc4d1	čistá
duši	duše	k1gFnSc4	duše
která	který	k3yRgFnSc1	který
miluje	milovat	k5eAaImIp3nS	milovat
beyblade	beyblad	k1gInSc5	beyblad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
L-draga	Lrag	k1gMnSc2	L-drag
naši	náš	k3xOp1gMnPc1	náš
předchůdci	předchůdce	k1gMnPc1	předchůdce
<g/>
,	,	kIx,	,
také	také	k9	také
použili	použít	k5eAaPmAgMnP	použít
úlomek	úlomek	k1gInSc4	úlomek
hvězdy	hvězda	k1gFnSc2	hvězda
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
špičky	špička	k1gFnSc2	špička
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
dnešní	dnešní	k2eAgInSc4d1	dnešní
beye	beye	k1gInSc4	beye
<g/>
.	.	kIx.	.
<g/>
Tenhle	tenhle	k3xDgMnSc1	tenhle
bey	bey	k?	bey
(	(	kIx(	(
<g/>
Storm	Storm	k1gInSc1	Storm
Pegasus	Pegasus	k1gMnSc1	Pegasus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
který	který	k3yRgMnSc1	který
schraňuje	schraňovat	k5eAaImIp3nS	schraňovat
světlo	světlo	k1gNnSc4	světlo
lidských	lidský	k2eAgFnPc2d1	lidská
nadějí	naděje	k1gFnPc2	naděje
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
největším	veliký	k2eAgNnSc7d3	veliký
úsilím	úsilí	k1gNnSc7	úsilí
schopen	schopen	k2eAgInSc1d1	schopen
vyhrát	vyhrát	k5eAaPmF	vyhrát
nad	nad	k7c7	nad
L-dragem	Lrag	k1gInSc7	L-drag
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
L-drago	Lrago	k6eAd1	L-drago
znám	znám	k2eAgInSc1d1	znám
jako	jako	k8xC	jako
zakázaný	zakázaný	k2eAgInSc1d1	zakázaný
bey	bey	k?	bey
a	a	k8xC	a
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Koma	koma	k1gFnSc1	koma
byl	být	k5eAaImAgInS	být
zapečetěný	zapečetěný	k2eAgMnSc1d1	zapečetěný
ve	v	k7c6	v
skále	skála	k1gFnSc6	skála
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
<g/>
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
špička	špička	k1gFnSc1	špička
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
našimi	náš	k3xOp1gInPc7	náš
předky	předek	k1gInPc7	předek
se	se	k3xPyFc4	se
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
beyblady	beyblad	k1gInPc1	beyblad
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
Metal	metal	k1gInSc1	metal
Fusion	Fusion	k1gInSc1	Fusion
Příběh	příběh	k1gInSc1	příběh
začíná	začínat	k5eAaImIp3nS	začínat
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Koma	koma	k1gNnSc1	koma
<g/>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
Gingka	Gingka	k1gMnSc1	Gingka
Hagane	Hagan	k1gMnSc5	Hagan
<g/>
.	.	kIx.	.
<g/>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
při	při	k7c6	při
oslavě	oslava	k1gFnSc6	oslava
<g/>
,	,	kIx,	,
<g/>
přiletí	přiletět	k5eAaPmIp3nS	přiletět
v	v	k7c6	v
helikoptéře	helikoptéra	k1gFnSc6	helikoptéra
Ryuga	Ryuga	k1gFnSc1	Ryuga
s	s	k7c7	s
Dojim	Dojim	k?	Dojim
<g/>
,	,	kIx,	,
<g/>
aby	aby	kYmCp3nP	aby
ukradli	ukradnout	k5eAaPmAgMnP	ukradnout
zapovězený	zapovězený	k2eAgInSc4d1	zapovězený
bey	bey	k?	bey
(	(	kIx(	(
<g/>
Lightning	Lightning	k1gInSc1	Lightning
L-drago	Lrago	k1gNnSc1	L-drago
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
zapečetěn	zapečetěn	k2eAgMnSc1d1	zapečetěn
ve	v	k7c6	v
skále	skála	k1gFnSc6	skála
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
jim	on	k3xPp3gInPc3	on
postaví	postavit	k5eAaPmIp3nS	postavit
Rio	Rio	k1gMnSc5	Rio
Hagane	Hagan	k1gMnSc5	Hagan
(	(	kIx(	(
<g/>
Gingkův	Gingkův	k2eAgMnSc1d1	Gingkův
otec	otec	k1gMnSc1	otec
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyzve	vyzvat	k5eAaPmIp3nS	vyzvat
Ryugu	Ryuga	k1gFnSc4	Ryuga
k	k	k7c3	k
zápasu	zápas	k1gInSc3	zápas
<g/>
.	.	kIx.	.
<g/>
Ryuga	Ryuga	k1gFnSc1	Ryuga
mezitím	mezitím	k6eAd1	mezitím
vezme	vzít	k5eAaPmIp3nS	vzít
a	a	k8xC	a
rozbije	rozbít	k5eAaPmIp3nS	rozbít
minerál	minerál	k1gInSc4	minerál
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
byl	být	k5eAaImAgInS	být
L-drago	Lrago	k6eAd1	L-drago
zapečetěný	zapečetěný	k2eAgInSc1d1	zapečetěný
<g/>
.	.	kIx.	.
<g/>
Rio	Rio	k1gFnSc1	Rio
zápas	zápas	k1gInSc1	zápas
prohraje	prohrát	k5eAaPmIp3nS	prohrát
<g/>
,	,	kIx,	,
<g/>
skála	skála	k1gFnSc1	skála
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
<g />
.	.	kIx.	.
</s>
<s>
hroutit	hroutit	k5eAaImF	hroutit
a	a	k8xC	a
tak	tak	k6eAd1	tak
Rio	Rio	k1gFnSc1	Rio
Hagane	Hagan	k1gMnSc5	Hagan
předá	předat	k5eAaPmIp3nS	předat
Storm	Storm	k1gInSc1	Storm
Pegasuse	Pegasuse	k1gFnSc2	Pegasuse
(	(	kIx(	(
<g/>
bouřlivého	bouřlivý	k2eAgInSc2d1	bouřlivý
Pegasuse	Pegasuse	k1gFnPc4	Pegasuse
<g/>
)	)	kIx)	)
Gingkovi	Gingkův	k2eAgMnPc1d1	Gingkův
<g/>
.	.	kIx.	.
<g/>
Skála	Skála	k1gMnSc1	Skála
se	se	k3xPyFc4	se
hroutí	hroutit	k5eAaImIp3nS	hroutit
a	a	k8xC	a
pohřbí	pohřbít	k5eAaPmIp3nS	pohřbít
Ria	Ria	k1gFnSc1	Ria
<g/>
.	.	kIx.	.
<g/>
Gingka	Gingka	k1gFnSc1	Gingka
se	se	k3xPyFc4	se
hned	hned	k6eAd1	hned
ráno	ráno	k6eAd1	ráno
vydá	vydat	k5eAaPmIp3nS	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
potká	potkat	k5eAaPmIp3nS	potkat
spoustu	spousta	k1gFnSc4	spousta
přátel	přítel	k1gMnPc2	přítel
i	i	k8xC	i
rivalů	rival	k1gMnPc2	rival
ale	ale	k8xC	ale
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
Gingki	Gingk	k1gFnSc2	Gingk
je	být	k5eAaImIp3nS	být
najít	najít	k5eAaPmF	najít
a	a	k8xC	a
porazit	porazit	k5eAaPmF	porazit
L-draga	Lrag	k1gMnSc4	L-drag
<g/>
.	.	kIx.	.
</s>
<s>
Postavy	postava	k1gFnPc1	postava
Gingka	Gingko	k1gNnSc2	Gingko
Hagane	Hagan	k1gMnSc5	Hagan
-	-	kIx~	-
Storm	Storm	k1gInSc1	Storm
Pegasus	Pegasus	k1gMnSc1	Pegasus
Ryuga	Ryuga	k1gFnSc1	Ryuga
-	-	kIx~	-
Lightning	Lightning	k1gInSc1	Lightning
L-drago	Lrago	k1gNnSc1	L-drago
Myreille	Myreille	k1gInSc1	Myreille
Psychiokieus	Psychiokieus	k1gMnSc1	Psychiokieus
-	-	kIx~	-
Hellhound	Hellhound	k1gMnSc1	Hellhound
Kenta	Kenta	k1gMnSc1	Kenta
-	-	kIx~	-
Flame	Flam	k1gInSc5	Flam
saggitarioS	saggitarioS	k?	saggitarioS
Kyoya	Kyoyum	k1gNnPc1	Kyoyum
-	-	kIx~	-
Rock	rock	k1gInSc1	rock
Leon	Leona	k1gFnPc2	Leona
Hyoma	Hyomum	k1gNnSc2	Hyomum
-	-	kIx~	-
Rock	rock	k1gInSc1	rock
Aries	Ariesa	k1gFnPc2	Ariesa
Benkey	Benkea	k1gFnSc2	Benkea
-	-	kIx~	-
Dark	Dark	k1gMnSc1	Dark
Bull	bulla	k1gFnPc2	bulla
Madoka	Madoka	k1gMnSc1	Madoka
-	-	kIx~	-
opravářka	opravářka	k1gFnSc1	opravářka
beybladů	beyblad	k1gInPc2	beyblad
Hikaru	Hikar	k1gInSc2	Hikar
-	-	kIx~	-
Storm	Storm	k1gInSc1	Storm
Aquario	Aquario	k1gNnSc1	Aquario
Doji	Doj	k1gFnSc2	Doj
-	-	kIx~	-
Dark	Dark	k1gMnSc1	Dark
Wolf	Wolf	k1gMnSc1	Wolf
Yu	Yu	k1gMnSc1	Yu
-	-	kIx~	-
Flame	Flam	k1gInSc5	Flam
Libra	libra	k1gFnSc1	libra
Tsubasa	Tsubasa	k1gFnSc1	Tsubasa
-	-	kIx~	-
Earth	Earth	k1gInSc1	Earth
Eagle	Eagle	k1gInSc1	Eagle
Phoenix	Phoenix	k1gInSc4	Phoenix
(	(	kIx(	(
<g/>
Rio	Rio	k1gMnSc5	Rio
Hagane	Hagan	k1gMnSc5	Hagan
<g/>
)	)	kIx)	)
-	-	kIx~	-
Burn	Burn	k1gInSc1	Burn
Fireblaze	Fireblaha	k1gFnSc3	Fireblaha
Beyblade	Beyblad	k1gInSc5	Beyblad
<g/>
:	:	kIx,	:
Metal	metal	k1gInSc1	metal
Masters	Mastersa	k1gFnPc2	Mastersa
je	být	k5eAaImIp3nS	být
přímé	přímý	k2eAgNnSc4d1	přímé
pokračování	pokračování	k1gNnSc4	pokračování
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Beyblade	Beyblad	k1gInSc5	Beyblad
<g/>
:	:	kIx,	:
Metal	metal	k1gInSc1	metal
Fury	Fura	k1gFnSc2	Fura
je	být	k5eAaImIp3nS	být
přímé	přímý	k2eAgNnSc4d1	přímé
pokračování	pokračování	k1gNnSc4	pokračování
série	série	k1gFnSc2	série
Metal	metat	k5eAaImAgMnS	metat
Masters	Masters	k1gInSc4	Masters
V	v	k7c4	v
Metal	metal	k1gInSc4	metal
Fusion	Fusion	k1gInSc1	Fusion
Gingka	Gingek	k1gInSc2	Gingek
porazil	porazit	k5eAaPmAgInS	porazit
Ryugu	Ryuga	k1gFnSc4	Ryuga
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Pegasus	Pegasus	k1gMnSc1	Pegasus
se	se	k3xPyFc4	se
přeměnil	přeměnit	k5eAaPmAgMnS	přeměnit
na	na	k7c4	na
modrý	modrý	k2eAgInSc4d1	modrý
prach	prach	k1gInSc4	prach
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
metal	metal	k1gInSc4	metal
masters	masters	k6eAd1	masters
získal	získat	k5eAaPmAgMnS	získat
Galaxy	Galax	k1gInPc4	Galax
Pegasuse	Pegasuse	k1gFnSc2	Pegasuse
a	a	k8xC	a
Ryuga	Ryug	k1gMnSc2	Ryug
získal	získat	k5eAaPmAgMnS	získat
Meteo	Meteo	k1gMnSc1	Meteo
L-Draga	L-Drag	k1gMnSc2	L-Drag
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Metal	metal	k1gInSc4	metal
Fury	Fura	k1gFnSc2	Fura
spadl	spadnout	k5eAaPmAgMnS	spadnout
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
meteorit	meteorit	k1gInSc1	meteorit
(	(	kIx(	(
<g/>
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
<g/>
/	/	kIx~	/
<g/>
á	á	k0	á
se	se	k3xPyFc4	se
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
<g/>
/	/	kIx~	/
<g/>
a	a	k8xC	a
na	na	k7c4	na
10	[number]	k4	10
menších	malý	k2eAgInPc2d2	menší
úlomků	úlomek	k1gInPc2	úlomek
a	a	k8xC	a
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
<g/>
/	/	kIx~	/
<g/>
a	a	k8xC	a
do	do	k7c2	do
10	[number]	k4	10
beyů	bey	k1gInPc2	bey
<g/>
.	.	kIx.	.
<g/>
Z	Z	kA	Z
Galaxy	Galax	k1gInPc1	Galax
Pegasuse	Pegasuse	k1gFnSc2	Pegasuse
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Cosmic	Cosmic	k1gMnSc1	Cosmic
Pegasus	Pegasus	k1gMnSc1	Pegasus
z	z	k7c2	z
Meteo	Meteo	k1gMnSc1	Meteo
L-draga	Lrag	k1gMnSc2	L-drag
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
L-drago	Lrago	k1gMnSc1	L-drago
Destructor	Destructor	k1gMnSc1	Destructor
<g/>
.	.	kIx.	.
</s>
<s>
BEYBLADE	BEYBLADE	kA	BEYBLADE
BEYBLADE	BEYBLADE	kA	BEYBLADE
na	na	k7c6	na
TV	TV	kA	TV
Pětka	pětka	k1gFnSc1	pětka
</s>
