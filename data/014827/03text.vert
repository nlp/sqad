<s>
Bionika	Bionik	k1gMnSc4
</s>
<s>
Je	být	k5eAaImIp3nS
navrženo	navržen	k2eAgNnSc4d1
začlenění	začlenění	k1gNnSc4
celého	celý	k2eAgInSc2d1
obsahu	obsah	k1gInSc2
článku	článek	k1gInSc2
Kyberimplantát	Kyberimplantát	k1gInSc1
do	do	k7c2
tohoto	tento	k3xDgInSc2
článku	článek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odtamtud	odtamtud	k6eAd1
sem	sem	k6eAd1
pak	pak	k6eAd1
má	mít	k5eAaImIp3nS
vést	vést	k5eAaImF
přesměrování	přesměrování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
návrhu	návrh	k1gInSc3
se	se	k3xPyFc4
můžete	moct	k5eAaImIp2nP
vyjádřit	vyjádřit	k5eAaPmF
v	v	k7c6
diskusi	diskuse	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Bionika	Bionik	k1gMnSc4
je	být	k5eAaImIp3nS
hraniční	hraniční	k2eAgInSc1d1
obor	obor	k1gInSc1
<g/>
,	,	kIx,
systematicky	systematicky	k6eAd1
zaměřený	zaměřený	k2eAgMnSc1d1
na	na	k7c6
uplatňování	uplatňování	k1gNnSc6
poznatků	poznatek	k1gInPc2
ze	z	k7c2
stavby	stavba	k1gFnSc2
a	a	k8xC
funkcí	funkce	k1gFnPc2
živých	živý	k2eAgInPc2d1
organismů	organismus	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnPc2
struktur	struktura	k1gFnPc2
při	při	k7c6
vývoji	vývoj	k1gInSc6
nových	nový	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teda	Ted	k1gMnSc2
spáji	spáje	k1gFnSc4
biologi	biologie	k1gFnSc4
s	s	k7c7
technikou	technika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
relativně	relativně	k6eAd1
mladý	mladý	k2eAgInSc1d1
interdisciplinární	interdisciplinární	k2eAgInSc1d1
vědní	vědní	k2eAgInSc1d1
obor	obor	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
na	na	k7c6
přelomu	přelom	k1gInSc6
50	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
především	především	k9
zásluhou	zásluha	k1gFnSc7
kvalitativně	kvalitativně	k6eAd1
nového	nový	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
v	v	k7c6
biologii	biologie	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
umožněn	umožnit	k5eAaPmNgInS
prudkým	prudký	k2eAgInSc7d1
vývojem	vývoj	k1gInSc7
technických	technický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemůžeme	moct	k5eNaImIp1nP
si	se	k3xPyFc3
ovšem	ovšem	k9
myslet	myslet	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
konala	konat	k5eAaImAgFnS
první	první	k4xOgFnSc1
konference	konference	k1gFnSc1
mající	mající	k2eAgFnSc1d1
na	na	k7c6
programu	program	k1gInSc6
zhodnocení	zhodnocení	k1gNnSc2
využití	využití	k1gNnSc2
biologických	biologický	k2eAgInPc2d1
poznatků	poznatek	k1gInPc2
v	v	k7c6
technice	technika	k1gFnSc6
a	a	k8xC
když	když	k8xS
se	se	k3xPyFc4
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
poprvé	poprvé	k6eAd1
objevuje	objevovat	k5eAaImIp3nS
pro	pro	k7c4
nový	nový	k2eAgInSc4d1
vědní	vědní	k2eAgInSc4d1
obor	obor	k1gInSc4
pojmenování	pojmenování	k1gNnSc2
bionics	bionicsa	k1gFnPc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
bionika	bionik	k1gMnSc2
zrodila	zrodit	k5eAaPmAgFnS
jen	jen	k6eAd1
tak	tak	k6eAd1
ze	z	k7c2
dne	den	k1gInSc2
na	na	k7c4
den	den	k1gInSc4
během	během	k7c2
pouhých	pouhý	k2eAgNnPc2d1
několika	několik	k4yIc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
samozřejmě	samozřejmě	k6eAd1
již	již	k6eAd1
odedávna	odedávna	k6eAd1
pozorovali	pozorovat	k5eAaImAgMnP
přírodu	příroda	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	on	k3xPp3gNnSc4
obklopovala	obklopovat	k5eAaImAgFnS
<g/>
,	,	kIx,
a	a	k8xC
nacházeli	nacházet	k5eAaImAgMnP
v	v	k7c6
ní	on	k3xPp3gFnSc6
inspiraci	inspirace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napodobování	napodobování	k1gNnSc1
živých	živý	k2eAgInPc2d1
organismů	organismus	k1gInPc2
technickými	technický	k2eAgFnPc7d1
konstrukcemi	konstrukce	k1gFnPc7
mnohem	mnohem	k6eAd1
dříve	dříve	k6eAd2
před	před	k7c7
tím	ten	k3xDgNnSc7
než	než	k8xS
bionika	bionik	k1gMnSc2
vůbec	vůbec	k9
vznikla	vzniknout	k5eAaPmAgFnS
lze	lze	k6eAd1
najít	najít	k5eAaPmF
v	v	k7c6
letectví	letectví	k1gNnSc6
či	či	k8xC
ve	v	k7c6
stavitelství	stavitelství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Zde	zde	k6eAd1
stačí	stačit	k5eAaBmIp3nS
jmenovat	jmenovat	k5eAaBmF,k5eAaImF
například	například	k6eAd1
legendárního	legendární	k2eAgMnSc4d1
Leonarda	Leonardo	k1gMnSc4
da	da	k?
Vinciho	Vinci	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
létající	létající	k2eAgInSc1d1
stroj	stroj	k1gInSc1
inspirovaný	inspirovaný	k2eAgInSc1d1
netopýrem	netopýr	k1gMnSc7
z	z	k7c2
počátku	počátek	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
či	či	k8xC
pro	pro	k7c4
stavitelství	stavitelství	k1gNnSc4
tak	tak	k8xS,k8xC
významnou	významný	k2eAgFnSc4d1
stavbu	stavba	k1gFnSc4
<g/>
,	,	kIx,
jakou	jaký	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
je	být	k5eAaImIp3nS
„	„	k?
<g/>
Crystal	Crystal	k1gFnSc1
Palace	Palace	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
Křišťálový	křišťálový	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
Londýně	Londýn	k1gInSc6
z	z	k7c2
let	léto	k1gNnPc2
1850	#num#	k4
až	až	k6eAd1
1851	#num#	k4
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
obr	obr	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autor	autor	k1gMnSc1
této	tento	k3xDgFnSc2
pozoruhodné	pozoruhodný	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
<g/>
,	,	kIx,
Sir	sir	k1gMnSc1
Joseph	Joseph	k1gMnSc1
Paxton	Paxton	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
nechal	nechat	k5eAaPmAgMnS
při	při	k7c6
jejím	její	k3xOp3gInSc6
návrhu	návrh	k1gInSc6
inspirovat	inspirovat	k5eAaBmF
studiem	studio	k1gNnSc7
listů	list	k1gInPc2
viktorie	viktorie	k1gFnSc2
královské	královský	k2eAgInPc4d1
-	-	kIx~
obrovského	obrovský	k2eAgInSc2d1
leknínu	leknín	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
listy	list	k1gInPc1
dosahují	dosahovat	k5eAaImIp3nP
průměru	průměr	k1gInSc3
až	až	k6eAd1
přes	přes	k7c4
dva	dva	k4xCgInPc4
metry	metr	k1gInPc4
a	a	k8xC
jejichž	jejichž	k3xOyRp3gFnSc1
žebrovitá	žebrovitý	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
jim	on	k3xPp3gMnPc3
propůjčuje	propůjčovat	k5eAaImIp3nS
vysokou	vysoký	k2eAgFnSc4d1
nosnost	nosnost	k1gFnSc4
a	a	k8xC
odolnosti	odolnost	k1gFnSc2
proti	proti	k7c3
poškození	poškození	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
tak	tak	k9
nejen	nejen	k6eAd1
proslulá	proslulý	k2eAgFnSc1d1
střešní	střešní	k2eAgFnSc1d1
konstrukce	konstrukce	k1gFnSc1
budovy	budova	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
zrodil	zrodit	k5eAaPmAgInS
se	se	k3xPyFc4
nový	nový	k2eAgInSc1d1
způsob	způsob	k1gInSc1
stavění	stavění	k1gNnSc2
pomocí	pomocí	k7c2
montážních	montážní	k2eAgInPc2d1
panelů	panel	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
tyto	tento	k3xDgFnPc4
technické	technický	k2eAgFnPc4d1
konstrukce	konstrukce	k1gFnPc4
inspirované	inspirovaný	k2eAgFnPc4d1
přírodou	příroda	k1gFnSc7
byly	být	k5eAaImAgFnP
spíše	spíše	k9
věcí	věc	k1gFnSc7
náhody	náhoda	k1gFnSc2
než	než	k8xS
nějakého	nějaký	k3yIgNnSc2
cílenějšího	cílený	k2eAgNnSc2d2
úsilí	úsilí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Crystal	Crystat	k5eAaPmAgMnS,k5eAaImAgMnS
Palace	Palace	k1gFnPc4
v	v	k7c6
Londýně	Londýn	k1gInSc6
-	-	kIx~
ukázka	ukázka	k1gFnSc1
bioniky	bionik	k1gInPc1
ve	v	k7c6
stavitelství	stavitelství	k1gNnSc6
</s>
<s>
První	první	k4xOgMnSc1
již	již	k6eAd1
skutečně	skutečně	k6eAd1
bionické	bionický	k2eAgFnPc1d1
studie	studie	k1gFnPc1
a	a	k8xC
myšlenky	myšlenka	k1gFnPc1
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
až	až	k9
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jmenujme	jmenovat	k5eAaBmRp1nP,k5eAaImRp1nP
například	například	k6eAd1
práce	práce	k1gFnPc4
G.	G.	kA
Lilienthala	Lilienthal	k1gMnSc2
„	„	k?
<g/>
Biotechnika	biotechnika	k1gFnSc1
létání	létání	k1gNnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
a	a	k8xC
neméně	málo	k6eNd2
významný	významný	k2eAgInSc4d1
průkopník	průkopník	k1gMnSc1
jeho	jeho	k3xOp3gFnSc6
bratr	bratr	k1gMnSc1
<g/>
,	,	kIx,
<g/>
Otto	Otto	k1gMnSc1
Lilienthal	Lilienthal	k1gMnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
A.	A.	kA
Niklitschenka	Niklitschenka	k1gFnSc1
„	„	k?
<g/>
Technika	technik	k1gMnSc4
živého	živý	k2eAgMnSc4d1
<g/>
“	“	k?
(	(	kIx(
<g/>
1940	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Prvním	první	k4xOgInSc7
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
problematiku	problematika	k1gFnSc4
bioniky	bionik	k1gInPc1
poměrně	poměrně	k6eAd1
široce	široko	k6eAd1
rozpracoval	rozpracovat	k5eAaPmAgInS
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
ovšem	ovšem	k9
R.	R.	kA
H.	H.	kA
Francé	Francé	k1gNnSc4
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
pracích	práce	k1gFnPc6
„	„	k?
<g/>
Technické	technický	k2eAgInPc1d1
výkony	výkon	k1gInPc1
rostlin	rostlina	k1gFnPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
„	„	k?
<g/>
Rostlina	rostlina	k1gFnSc1
jako	jako	k8xS,k8xC
vynálezce	vynálezce	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francé	Francé	k1gNnSc1
pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
„	„	k?
<g/>
nový	nový	k2eAgInSc1d1
vědní	vědní	k2eAgInSc1d1
obor	obor	k1gInSc1
<g/>
“	“	k?
zvolil	zvolit	k5eAaPmAgInS
pojmenování	pojmenování	k1gNnSc4
„	„	k?
<g/>
biotechnika	biotechnika	k1gFnSc1
<g/>
“	“	k?
a	a	k8xC
snažil	snažit	k5eAaImAgMnS
se	se	k3xPyFc4
jej	on	k3xPp3gInSc2
náležitě	náležitě	k6eAd1
propagovat	propagovat	k5eAaImF
<g/>
,	,	kIx,
ovšem	ovšem	k9
jeho	jeho	k3xOp3gFnPc1
práce	práce	k1gFnPc1
zůstaly	zůstat	k5eAaPmAgFnP
nepovšimnuty	povšimnut	k2eNgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
se	se	k3xPyFc4
zájem	zájem	k1gInSc1
o	o	k7c4
bionickou	bionický	k2eAgFnSc4d1
problematiku	problematika	k1gFnSc4
začíná	začínat	k5eAaImIp3nS
zvyšovat	zvyšovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Velký	velký	k2eAgInSc1d1
kus	kus	k1gInSc1
práce	práce	k1gFnSc2
v	v	k7c6
popularizaci	popularizace	k1gFnSc6
bioniky	bionik	k1gInPc4
odvedl	odvést	k5eAaPmAgMnS
především	především	k9
Max	Max	k1gMnSc1
O.	O.	kA
Kramer	Kramer	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgInS
studiem	studio	k1gNnSc7
kůže	kůže	k1gFnSc2
delfínů	delfín	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
svém	svůj	k3xOyFgInSc6
výzkumu	výzkum	k1gInSc6
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
kůže	kůže	k1gFnSc1
delfína	delfín	k1gMnSc2
má	mít	k5eAaImIp3nS
speciální	speciální	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
delfínovi	delfín	k1gMnSc3
umožňuje	umožňovat	k5eAaImIp3nS
pohlcovat	pohlcovat	k5eAaImF
část	část	k1gFnSc4
energie	energie	k1gFnSc2
turbulentního	turbulentní	k2eAgNnSc2d1
proudění	proudění	k1gNnSc2
a	a	k8xC
při	při	k7c6
plavání	plavání	k1gNnSc6
tak	tak	k6eAd1
výrazně	výrazně	k6eAd1
snížit	snížit	k5eAaPmF
odpor	odpor	k1gInSc4
protisměrně	protisměrně	k6eAd1
proudící	proudící	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
důkladných	důkladný	k2eAgNnPc6d1
studiích	studio	k1gNnPc6
přikročil	přikročit	k5eAaPmAgInS
Kramer	Kramer	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
k	k	k7c3
praktickým	praktický	k2eAgInPc3d1
pokusům	pokus	k1gInPc3
se	s	k7c7
speciálními	speciální	k2eAgInPc7d1
potahy	potah	k1gInPc7
na	na	k7c4
ponorky	ponorka	k1gFnPc4
a	a	k8xC
dosahoval	dosahovat	k5eAaImAgInS
s	s	k7c7
nimi	on	k3xPp3gInPc7
až	až	k9
padesátiprocentního	padesátiprocentní	k2eAgNnSc2d1
snížení	snížení	k1gNnSc2
třecího	třecí	k2eAgInSc2d1
odporu	odpor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
práce	práce	k1gFnSc2
je	být	k5eAaImIp3nS
významná	významný	k2eAgFnSc1d1
především	především	k9
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
usiloval	usilovat	k5eAaImAgMnS
o	o	k7c4
aplikaci	aplikace	k1gFnSc4
principu	princip	k1gInSc2
a	a	k8xC
ne	ne	k9
o	o	k7c6
vytvoření	vytvoření	k1gNnSc6
přesné	přesný	k2eAgFnSc2d1
kopie	kopie	k1gFnSc2
kůže	kůže	k1gFnSc2
delfína	delfín	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
mu	on	k3xPp3gMnSc3
totiž	totiž	k9
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
plně	plně	k6eAd1
kopírovat	kopírovat	k5eAaImF
stavbu	stavba	k1gFnSc4
kůže	kůže	k1gFnSc2
delfína	delfín	k1gMnSc2
nelze	lze	k6eNd1
a	a	k8xC
že	že	k8xS
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
využít	využít	k5eAaPmF
především	především	k6eAd1
principu	princip	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
příroda	příroda	k1gFnSc1
nabízí	nabízet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zrodila	zrodit	k5eAaPmAgFnS
se	se	k3xPyFc4
tak	tak	k9
v	v	k7c6
podstatě	podstata	k1gFnSc6
hlavní	hlavní	k2eAgFnSc2d1
koncepce	koncepce	k1gFnSc2
bioniky	bionik	k1gInPc1
-	-	kIx~
studiem	studio	k1gNnSc7
živé	živý	k2eAgFnSc2d1
přírody	příroda	k1gFnSc2
nacházet	nacházet	k5eAaImF
vhodná	vhodný	k2eAgNnPc4d1
principiální	principiální	k2eAgNnPc4d1
řešení	řešení	k1gNnPc4
a	a	k8xC
na	na	k7c6
jejich	jejich	k3xOp3gInSc6
základě	základ	k1gInSc6
vytvořit	vytvořit	k5eAaPmF
vhodné	vhodný	k2eAgNnSc4d1
technické	technický	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Definitivně	definitivně	k6eAd1
se	se	k3xPyFc4
bionika	bionik	k1gMnSc2
zařadila	zařadit	k5eAaPmAgFnS
do	do	k7c2
systému	systém	k1gInSc2
věd	věda	k1gFnPc2
díky	díky	k7c3
neúnavné	únavný	k2eNgFnSc3d1
práci	práce	k1gFnSc3
vědců	vědec	k1gMnPc2
z	z	k7c2
Aeronautical	Aeronautical	k1gFnPc2
Systems	Systemsa	k1gFnPc2
Division	Division	k1gInSc4
vedených	vedený	k2eAgFnPc2d1
Johnem	John	k1gMnSc7
Keto	Keto	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
vědci	vědec	k1gMnPc1
společně	společně	k6eAd1
s	s	k7c7
několika	několik	k4yIc7
dalšími	další	k1gNnPc7
uspořádali	uspořádat	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
první	první	k4xOgNnSc4
výše	vysoce	k6eAd2
zmiňovanou	zmiňovaný	k2eAgFnSc4d1
vědeckou	vědecký	k2eAgFnSc4d1
konferenci	konference	k1gFnSc4
zaměřenou	zaměřený	k2eAgFnSc4d1
na	na	k7c6
zhodnocení	zhodnocení	k1gNnSc6
možností	možnost	k1gFnPc2
využití	využití	k1gNnSc2
biologických	biologický	k2eAgInPc2d1
poznatků	poznatek	k1gInPc2
v	v	k7c6
technické	technický	k2eAgFnSc6d1
praxi	praxe	k1gFnSc6
a	a	k8xC
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1957	#num#	k4
byly	být	k5eAaImAgInP
pak	pak	k6eAd1
vytyčeny	vytyčit	k5eAaPmNgInP
hlavní	hlavní	k2eAgInPc1d1
výzkumné	výzkumný	k2eAgInPc1d1
cíle	cíl	k1gInPc1
nového	nový	k2eAgInSc2d1
vědního	vědní	k2eAgInSc2d1
oboru	obor	k1gInSc2
a	a	k8xC
byly	být	k5eAaImAgInP
určeny	určen	k2eAgInPc1d1
způsoby	způsob	k1gInPc1
práce	práce	k1gFnSc2
v	v	k7c6
této	tento	k3xDgFnSc6
nové	nový	k2eAgFnSc6d1
vědní	vědní	k2eAgFnSc6d1
disciplíně	disciplína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
pak	pak	k6eAd1
dochází	docházet	k5eAaImIp3nS
na	na	k7c6
prvním	první	k4xOgNnSc6
bionickém	bionický	k2eAgNnSc6d1
sympoziu	sympozion	k1gNnSc6
za	za	k7c2
účasti	účast	k1gFnSc2
více	hodně	k6eAd2
než	než	k8xS
700	#num#	k4
delegátů	delegát	k1gMnPc2
k	k	k7c3
oficiálnímu	oficiální	k2eAgInSc3d1
zrodu	zrod	k1gInSc3
bioniky	bionik	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Náplň	náplň	k1gFnSc1
bioniky	bionik	k1gInPc1
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1
náplní	náplň	k1gFnSc7
bioniky	bionik	k1gInPc4
je	on	k3xPp3gFnPc4
vytvořit	vytvořit	k5eAaPmF
velmi	velmi	k6eAd1
úzkou	úzký	k2eAgFnSc4d1
vzájemnou	vzájemný	k2eAgFnSc4d1
vazbu	vazba	k1gFnSc4
mezi	mezi	k7c7
biologií	biologie	k1gFnSc7
a	a	k8xC
technikou	technika	k1gFnSc7
s	s	k7c7
přirozenou	přirozený	k2eAgFnSc7d1
vazbou	vazba	k1gFnSc7
na	na	k7c4
další	další	k2eAgFnPc4d1
hraniční	hraniční	k2eAgFnPc4d1
obory	obora	k1gFnPc4
biologie	biologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
vzájemná	vzájemný	k2eAgFnSc1d1
vazba	vazba	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
jednak	jednak	k8xC
pomocí	pomocí	k7c2
biologie	biologie	k1gFnSc2
a	a	k8xC
jí	jíst	k5eAaImIp3nS
příbuzných	příbuzný	k2eAgInPc2d1
vědních	vědní	k2eAgInPc2d1
oborů	obor	k1gInPc2
rozvoj	rozvoj	k1gInSc4
v	v	k7c6
technických	technický	k2eAgFnPc6d1
vědách	věda	k1gFnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k6eAd1
využitím	využití	k1gNnSc7
inženýrských	inženýrský	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
a	a	k8xC
postupů	postup	k1gInPc2
pokrok	pokrok	k1gInSc1
ve	v	k7c6
vědách	věda	k1gFnPc6
biologických	biologický	k2eAgFnPc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bionika	Bionik	k1gMnSc2
má	mít	k5eAaImIp3nS
tedy	tedy	k9
umožňovat	umožňovat	k5eAaImF
především	především	k6eAd1
úzký	úzký	k2eAgInSc4d1
vzájemný	vzájemný	k2eAgInSc4d1
kontakt	kontakt	k1gInSc4
při	při	k7c6
poznávání	poznávání	k1gNnSc6
zákonitostí	zákonitost	k1gFnPc2
živé	živý	k2eAgFnSc2d1
a	a	k8xC
neživé	živý	k2eNgFnSc2d1
přírody	příroda	k1gFnSc2
a	a	k8xC
zajistit	zajistit	k5eAaPmF
oboustranně	oboustranně	k6eAd1
výhodné	výhodný	k2eAgNnSc1d1
využití	využití	k1gNnSc1
výsledků	výsledek	k1gInPc2
tohoto	tento	k3xDgInSc2
výzkumu	výzkum	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
soustřeďuje	soustřeďovat	k5eAaImIp3nS
především	především	k9
na	na	k7c4
studium	studium	k1gNnSc4
živých	živý	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
a	a	k8xC
procesů	proces	k1gInPc2
probíhajících	probíhající	k2eAgInPc2d1
v	v	k7c6
těchto	tento	k3xDgFnPc6
strukturách	struktura	k1gFnPc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
na	na	k7c4
podněty	podnět	k1gInPc4
pro	pro	k7c4
budoucí	budoucí	k2eAgFnPc4d1
technické	technický	k2eAgFnPc4d1
aplikace	aplikace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
získané	získaný	k2eAgInPc4d1
poznatky	poznatek	k1gInPc4
systematicky	systematicky	k6eAd1
shromažďuje	shromažďovat	k5eAaImIp3nS
a	a	k8xC
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
je	být	k5eAaImIp3nS
v	v	k7c6
průmyslové	průmyslový	k2eAgFnSc6d1
výrobě	výroba	k1gFnSc6
buď	buď	k8xC
formou	forma	k1gFnSc7
nových	nový	k2eAgInPc2d1
výrobních	výrobní	k2eAgInPc2d1
procesů	proces	k1gInPc2
nebo	nebo	k8xC
přímo	přímo	k6eAd1
formou	forma	k1gFnSc7
nových	nový	k2eAgInPc2d1
konkrétních	konkrétní	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Dělení	dělení	k1gNnSc1
</s>
<s>
Ze	z	k7c2
systematického	systematický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
můžeme	moct	k5eAaImIp1nP
bioniku	bionik	k1gMnSc3
rozdělit	rozdělit	k5eAaPmF
na	na	k7c6
bioniku	bionik	k1gInSc6
<g/>
:	:	kIx,
</s>
<s>
obecnou	obecný	k2eAgFnSc7d1
<g/>
,	,	kIx,
</s>
<s>
systematickou	systematický	k2eAgFnSc4d1
</s>
<s>
a	a	k8xC
specificky	specificky	k6eAd1
použitou	použitý	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Úkolem	úkol	k1gInSc7
obecné	obecný	k2eAgInPc4d1
bioniky	bionik	k1gInPc4
je	být	k5eAaImIp3nS
především	především	k6eAd1
vyhledávat	vyhledávat	k5eAaImF
ty	ten	k3xDgFnPc4
biologické	biologický	k2eAgFnPc4d1
struktury	struktura	k1gFnPc4
a	a	k8xC
procesy	proces	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
by	by	kYmCp3nP
mohly	moct	k5eAaImAgInP
mít	mít	k5eAaImF
význam	význam	k1gInSc4
jako	jako	k8xC,k8xS
podnět	podnět	k1gInSc4
pro	pro	k7c4
realizaci	realizace	k1gFnSc4
technických	technický	k2eAgFnPc2d1
a	a	k8xC
technologických	technologický	k2eAgFnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
,	,	kIx,
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
je	být	k5eAaImIp3nS
daný	daný	k2eAgInSc4d1
biologický	biologický	k2eAgInSc4d1
systém	systém	k1gInSc4
již	již	k6eAd1
dostatečně	dostatečně	k6eAd1
znám	znám	k2eAgMnSc1d1
a	a	k8xC
prozkoumán	prozkoumán	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecná	obecná	k1gFnSc1
bionika	bionik	k1gMnSc4
tedy	tedy	k9
především	především	k9
studuje	studovat	k5eAaImIp3nS
všechny	všechen	k3xTgInPc4
dosud	dosud	k6eAd1
poznané	poznaný	k2eAgInPc4d1
rostlinné	rostlinný	k2eAgInPc4d1
a	a	k8xC
živočišné	živočišný	k2eAgInPc4d1
druhy	druh	k1gInPc4
a	a	k8xC
vyhledává	vyhledávat	k5eAaImIp3nS
nejnadějnější	nadějný	k2eAgInPc4d3
biologické	biologický	k2eAgInPc4d1
principy	princip	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
tomto	tento	k3xDgInSc6
studiu	studio	k1gNnSc6
spolupracuje	spolupracovat	k5eAaImIp3nS
s	s	k7c7
mnoha	mnoho	k4c7
dalšími	další	k2eAgFnPc7d1
biologickými	biologický	k2eAgFnPc7d1
vědami	věda	k1gFnPc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
cytologie	cytologie	k1gFnSc1
<g/>
,	,	kIx,
histologie	histologie	k1gFnSc1
<g/>
,	,	kIx,
fyziologie	fyziologie	k1gFnSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Systematická	systematický	k2eAgFnSc1d1
bionika	bionik	k1gMnSc2
systematicky	systematicky	k6eAd1
zpracovává	zpracovávat	k5eAaImIp3nS
a	a	k8xC
třídí	třídit	k5eAaImIp3nP
poznatky	poznatek	k1gInPc1
obecné	obecný	k2eAgInPc1d1
bioniky	bionik	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
často	často	k6eAd1
také	také	k9
zpracovává	zpracovávat	k5eAaImIp3nS
informace	informace	k1gFnPc4
a	a	k8xC
dokumentaci	dokumentace	k1gFnSc4
o	o	k7c6
problematice	problematika	k1gFnSc6
bioniky	bionik	k1gMnPc4
jako	jako	k8xS,k8xC
celku	celek	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poznatky	poznatek	k1gInPc1
získané	získaný	k2eAgInPc1d1
obecnou	obecný	k2eAgFnSc7d1
bionikou	bionika	k1gFnSc7
jsou	být	k5eAaImIp3nP
většinou	většinou	k6eAd1
tříděny	třídit	k5eAaImNgInP
podle	podle	k7c2
oborů	obor	k1gInPc2
použití	použití	k1gNnSc2
a	a	k8xC
podle	podle	k7c2
tematických	tematický	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
na	na	k7c4
systémy	systém	k1gInPc4
aktivního	aktivní	k2eAgNnSc2d1
a	a	k8xC
pasivního	pasivní	k2eAgNnSc2d1
létání	létání	k1gNnSc2
<g/>
,	,	kIx,
tvarové	tvarový	k2eAgInPc4d1
profily	profil	k1gInPc4
organismů	organismus	k1gInPc2
<g/>
,	,	kIx,
submikroskopické	submikroskopický	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
apod.	apod.	kA
<g/>
)	)	kIx)
</s>
<s>
Specificky	specificky	k6eAd1
použitá	použitý	k2eAgFnSc1d1
bionika	bionik	k1gMnSc2
již	již	k6eAd1
zajišťuje	zajišťovat	k5eAaImIp3nS
pro	pro	k7c4
jednotlivé	jednotlivý	k2eAgInPc4d1
obory	obor	k1gInPc4
podrobné	podrobný	k2eAgNnSc4d1
studium	studium	k1gNnSc4
podnětů	podnět	k1gInPc2
<g/>
,	,	kIx,
vypracování	vypracování	k1gNnSc1
modelů	model	k1gInPc2
či	či	k8xC
prototypů	prototyp	k1gInPc2
výrobků	výrobek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
pak	pak	k6eAd1
zajišťuje	zajišťovat	k5eAaImIp3nS
v	v	k7c6
těchto	tento	k3xDgInPc6
jednotlivých	jednotlivý	k2eAgInPc6d1
oborech	obor	k1gInPc6
rozvíjení	rozvíjení	k1gNnSc2
vhodných	vhodný	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
pro	pro	k7c4
zpracování	zpracování	k1gNnSc4
bionických	bionický	k2eAgInPc2d1
poznatků	poznatek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
shromažďuje	shromažďovat	k5eAaImIp3nS
požadavky	požadavek	k1gInPc4
průmyslu	průmysl	k1gInSc2
a	a	k8xC
předává	předávat	k5eAaImIp3nS
je	být	k5eAaImIp3nS
dále	daleko	k6eAd2
obecné	obecný	k2eAgFnSc3d1
bionice	bionika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Aplikace	aplikace	k1gFnSc1
</s>
<s>
Bionicky	Bionicky	k6eAd1
rozšíření	rozšíření	k1gNnSc4
<g/>
/	/	kIx~
<g/>
vylepšení	vylepšený	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgFnPc2
by	by	kYmCp3nS
elektronické	elektronický	k2eAgFnPc1d1
či	či	k8xC
mechanické	mechanický	k2eAgFnPc1d1
součástky	součástka	k1gFnPc1
tvořily	tvořit	k5eAaImAgFnP
část	část	k1gFnSc4
jejich	jejich	k3xOp3gNnPc2
těl	tělo	k1gNnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
dosud	dosud	k6eAd1
vyskytovali	vyskytovat	k5eAaImAgMnP
pouze	pouze	k6eAd1
v	v	k7c6
sci-fi	sci-fi	k1gFnSc6
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
už	už	k6eAd1
dnes	dnes	k6eAd1
jsou	být	k5eAaImIp3nP
lidé	člověk	k1gMnPc1
s	s	k7c7
endoprotézou	endoprotéza	k1gFnSc7
<g/>
,	,	kIx,
naslouchátkem	naslouchátko	k1gNnSc7
(	(	kIx(
<g/>
kochleární	kochleární	k2eAgInSc4d1
implantát	implantát	k1gInSc4
<g/>
)	)	kIx)
nebo	nebo	k8xC
srdečním	srdeční	k2eAgInSc7d1
stimulátorem	stimulátor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1998	#num#	k4
na	na	k7c4
sebe	sebe	k3xPyFc4
upozornil	upozornit	k5eAaPmAgMnS
Kevin	Kevin	k1gMnSc1
Warwick	Warwick	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
kybernetiky	kybernetika	k1gFnSc2
při	při	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Readingu	Reading	k1gInSc6
<g/>
,	,	kIx,
když	když	k8xS
si	se	k3xPyFc3
nechal	nechat	k5eAaPmAgMnS
pod	pod	k7c4
kůži	kůže	k1gFnSc4
vložit	vložit	k5eAaPmF
radio-identifikátor	radio-identifikátor	k1gInSc4
RFID	RFID	kA
a	a	k8xC
prohlásil	prohlásit	k5eAaPmAgInS
o	o	k7c6
sobě	sebe	k3xPyFc6
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
kyborg	kyborg	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Experimentální	experimentální	k2eAgFnPc1d1
kybernetické	kybernetický	k2eAgFnPc1d1
protézy	protéza	k1gFnPc1
již	již	k6eAd1
existují	existovat	k5eAaImIp3nP
<g/>
,	,	kIx,
ale	ale	k8xC
především	především	k9
jejich	jejich	k3xOp3gFnPc1
hmatové	hmatový	k2eAgFnPc1d1
možnosti	možnost	k1gFnPc1
jsou	být	k5eAaImIp3nP
výrazně	výrazně	k6eAd1
horší	zlý	k2eAgFnPc1d2
než	než	k8xS
schopnosti	schopnost	k1gFnPc1
přirozených	přirozený	k2eAgFnPc2d1
končetin	končetina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potíž	potíž	k1gFnSc1
je	být	k5eAaImIp3nS
s	s	k7c7
tzv.	tzv.	kA
neurálním	neurální	k2eAgNnSc7d1
rozhraním	rozhraní	k1gNnSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
naše	náš	k3xOp1gFnSc1
dnešní	dnešní	k2eAgFnSc1d1
technika	technika	k1gFnSc1
sice	sice	k8xC
už	už	k6eAd1
do	do	k7c2
určité	určitý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
je	být	k5eAaImIp3nS
schopná	schopný	k2eAgFnSc1d1
číst	číst	k5eAaImF
signály	signál	k1gInPc4
z	z	k7c2
těla	tělo	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
nervů	nerv	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
informační	informační	k2eAgInPc1d1
vzruchy	vzruch	k1gInPc1
do	do	k7c2
ní	on	k3xPp3gFnSc2
<g/>
,	,	kIx,
zpět	zpět	k6eAd1
do	do	k7c2
těla	tělo	k1gNnSc2
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
neposkytuje	poskytovat	k5eNaImIp3nS
<g/>
:	:	kIx,
Napojení	napojení	k1gNnSc1
elektroniky	elektronika	k1gFnSc2
na	na	k7c4
neurony	neuron	k1gInPc4
a	a	k8xC
tímto	tento	k3xDgNnSc7
poskytování	poskytování	k1gNnSc4
nové	nový	k2eAgFnSc2d1
zpětné	zpětný	k2eAgFnSc2d1
vazby	vazba	k1gFnSc2
a	a	k8xC
vjemů	vjem	k1gInPc2
do	do	k7c2
mozku	mozek	k1gInSc2
zůstává	zůstávat	k5eAaImIp3nS
stále	stále	k6eAd1
nevyřešenou	vyřešený	k2eNgFnSc7d1
technologickou	technologický	k2eAgFnSc7d1
výzvou	výzva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Fiktivní	fiktivní	k2eAgFnPc1d1
aplikace	aplikace	k1gFnPc1
</s>
<s>
Protetické	protetický	k2eAgNnSc1d1
</s>
<s>
Darth	Darth	k1gMnSc1
Vader	Vader	k1gMnSc1
–	–	k?
záporná	záporný	k2eAgFnSc1d1
postava	postava	k1gFnSc1
z	z	k7c2
filmového	filmový	k2eAgInSc2d1
seriálu	seriál	k1gInSc2
Star	Star	kA
Wars	Wars	k1gInSc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
usekané	usekaný	k2eAgFnPc4d1
končetiny	končetina	k1gFnPc4
nahrazené	nahrazený	k2eAgNnSc1d1
bionickou	bionický	k2eAgFnSc7d1
protetikou	protetika	k1gFnSc7
<g/>
,	,	kIx,
oblek	oblek	k1gInSc1
mu	on	k3xPp3gMnSc3
chrání	chránit	k5eAaImIp3nS
popálenou	popálený	k2eAgFnSc4d1
kůži	kůže	k1gFnSc4
<g/>
,	,	kIx,
helma	helma	k1gFnSc1
pomáhá	pomáhat	k5eAaImIp3nS
dýchat	dýchat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Robocop	Robocop	k1gInSc1
-	-	kIx~
policista	policista	k1gMnSc1
ve	v	k7c6
stejnojmenném	stejnojmenný	k2eAgInSc6d1
filmu	film	k1gInSc6
<g/>
,	,	kIx,
pomezí	pomezí	k1gNnSc6
mezi	mezi	k7c7
bionickým	bionický	k2eAgNnSc7d1
vylepšením	vylepšení	k1gNnSc7
člověka	člověk	k1gMnSc2
a	a	k8xC
androidním	androidní	k2eAgInSc7d1
kyborgem	kyborg	k1gInSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nižší	nízký	k2eAgFnPc4d2
pohybové	pohybový	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
téměř	téměř	k6eAd1
mrtvého	mrtvý	k2eAgInSc2d1
mozku	mozek	k1gInSc2
byly	být	k5eAaImAgFnP
využity	využít	k5eAaPmNgFnP
pro	pro	k7c4
řízení	řízení	k1gNnSc4
bionického	bionický	k2eAgNnSc2d1
těla	tělo	k1gNnSc2
<g/>
:	:	kIx,
Hrdina	Hrdina	k1gMnSc1
nakonec	nakonec	k6eAd1
získal	získat	k5eAaPmAgMnS
sebeuvědomění	sebeuvědomění	k1gNnSc4
a	a	k8xC
nové	nový	k2eAgNnSc4d1
tělo	tělo	k1gNnSc4
plně	plně	k6eAd1
ovládl	ovládnout	k5eAaPmAgInS
<g/>
,	,	kIx,
z	z	k7c2
otrockého	otrocký	k2eAgInSc2d1
stavu	stav	k1gInSc2
kyberneticky	kyberneticky	k6eAd1
řízeného	řízený	k2eAgInSc2d1
organismu	organismus	k1gInSc2
<g/>
,	,	kIx,
kyborga	kyborga	k1gFnSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
svou	svůj	k3xOyFgFnSc4
situaci	situace	k1gFnSc4
přeměnil	přeměnit	k5eAaPmAgMnS
do	do	k7c2
stavu	stav	k1gInSc2
svobodného	svobodný	k2eAgInSc2d1
bionicky	bionicky	k6eAd1
vylepšeného	vylepšený	k2eAgMnSc2d1
invalidy	invalida	k1gMnSc2
<g/>
,	,	kIx,
těžce	těžce	k6eAd1
vyzbrojeného	vyzbrojený	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Dvěstěletý	dvěstěletý	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
-	-	kIx~
příběh	příběh	k1gInSc1
rozvoje	rozvoj	k1gInSc2
umělé	umělý	k2eAgFnSc2d1
mysli	mysl	k1gFnSc2
v	v	k7c6
těle	tělo	k1gNnSc6
robota	robot	k1gMnSc2
<g/>
,	,	kIx,
jak	jak	k6eAd1
se	se	k3xPyFc4
bionickou	bionický	k2eAgFnSc7d1
protetikou	protetika	k1gFnSc7
mění	měnit	k5eAaImIp3nS
na	na	k7c4
androida	android	k1gMnSc4
nerozlišitelného	rozlišitelný	k2eNgInSc2d1
od	od	k7c2
lidí	člověk	k1gMnPc2
a	a	k8xC
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
uznání	uznání	k1gNnSc4
za	za	k7c4
člověka	člověk	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Eticky	eticky	k6eAd1
pochybné	pochybný	k2eAgNnSc1d1
</s>
<s>
generál	generál	k1gMnSc1
Grievous	Grievous	k1gMnSc1
-	-	kIx~
další	další	k2eAgFnSc1d1
bionicky	bionicky	k6eAd1
vylepšená	vylepšený	k2eAgFnSc1d1
postava	postava	k1gFnSc1
z	z	k7c2
filmového	filmový	k2eAgInSc2d1
seriálu	seriál	k1gInSc2
Star	Star	kA
Wars	Wars	k1gInSc1
<g/>
,	,	kIx,
z	z	k7c2
jejíhož	jejíž	k3xOyRp3gNnSc2
těla	tělo	k1gNnSc2
už	už	k6eAd1
zbyla	zbýt	k5eAaPmAgFnS
jen	jen	k9
hlava	hlava	k1gFnSc1
a	a	k8xC
srdce	srdce	k1gNnSc1
<g/>
,	,	kIx,
do	do	k7c2
bitev	bitva	k1gFnPc2
takticky	takticky	k6eAd1
chráněné	chráněný	k2eAgNnSc1d1
pancéřovým	pancéřový	k2eAgNnSc7d1
robotickým	robotický	k2eAgNnSc7d1
tělem	tělo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Caina	Caina	k1gFnSc1
<g/>
,	,	kIx,
další	další	k2eAgFnSc4d1
postavu	postava	k1gFnSc4
z	z	k7c2
pokračování	pokračování	k1gNnSc2
filmu	film	k1gInSc2
<g/>
,	,	kIx,
RoboCop	RoboCop	k1gInSc1
2	#num#	k4
–	–	k?
zdravého	zdravý	k1gMnSc4
živého	živý	k2eAgMnSc4d1
člověka	člověk	k1gMnSc4
"	"	kIx"
<g/>
vylepšili	vylepšit	k5eAaPmAgMnP
<g/>
"	"	kIx"
tak	tak	k9
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
něj	on	k3xPp3gMnSc2
úmyslně	úmyslně	k6eAd1
zbyla	zbýt	k5eAaPmAgFnS
už	už	k6eAd1
jen	jen	k9
vypreparovaná	vypreparovaný	k2eAgFnSc1d1
nervová	nervový	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
<g/>
,	,	kIx,
stále	stále	k6eAd1
živá	živý	k2eAgFnSc1d1
<g/>
:	:	kIx,
mozek	mozek	k1gInSc1
<g/>
,	,	kIx,
mícha	mícha	k1gFnSc1
a	a	k8xC
oči	oko	k1gNnPc4
<g/>
,	,	kIx,
zbytek	zbytek	k1gInSc1
těla	tělo	k1gNnSc2
byl	být	k5eAaImAgInS
cíleně	cíleně	k6eAd1
nahrazen	nahrazen	k2eAgInSc1d1
robotickým	robotický	k2eAgNnSc7d1
tělem	tělo	k1gNnSc7
<g/>
,	,	kIx,
tedy	tedy	k8xC
opět	opět	k6eAd1
extrémní	extrémní	k2eAgFnSc7d1
bionikou	bionika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Postava	postava	k1gFnSc1
„	„	k?
<g/>
pouličního	pouliční	k2eAgMnSc2d1
kazatele	kazatel	k1gMnSc2
<g/>
“	“	k?
ve	v	k7c6
filmu	film	k1gInSc6
Johnny	Johnna	k1gFnSc2
Mnemonic	Mnemonice	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
své	svůj	k3xOyFgFnPc4
bionicky	bionicky	k6eAd1
neustále	neustále	k6eAd1
vylepšované	vylepšovaný	k2eAgNnSc4d1
tělo	tělo	k1gNnSc4
využíval	využívat	k5eAaPmAgInS,k5eAaImAgInS
jako	jako	k8xS,k8xC
výhodu	výhoda	k1gFnSc4
při	při	k7c6
svých	svůj	k3xOyFgInPc6
pouličních	pouliční	k2eAgInPc6d1
zločinech	zločin	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Současné	současný	k2eAgFnPc1d1
aplikace	aplikace	k1gFnPc1
</s>
<s>
kardiostimulátor	kardiostimulátor	k1gInSc1
</s>
<s>
kochleární	kochleární	k2eAgInSc1d1
implantát	implantát	k1gInSc1
</s>
<s>
protetika	protetika	k1gFnSc1
končetin	končetina	k1gFnPc2
</s>
<s>
robotický	robotický	k2eAgInSc1d1
exoskelet	exoskelet	k1gInSc1
</s>
<s>
hmatová	hmatový	k2eAgFnSc1d1
náhrada	náhrada	k1gFnSc1
zraku	zrak	k1gInSc2
na	na	k7c4
jazyk	jazyk	k1gInSc4
</s>
<s>
aj.	aj.	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Kevin	Kevin	k1gMnSc1
Warwick	Warwick	k1gMnSc1
si	se	k3xPyFc3
nechal	nechat	k5eAaPmAgMnS
pod	pod	k7c4
kůži	kůže	k1gFnSc4
vložit	vložit	k5eAaPmF
RFID	RFID	kA
a	a	k8xC
prohlašuje	prohlašovat	k5eAaImIp3nS
o	o	k7c6
sobě	sebe	k3xPyFc6
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
kyborg	kyborg	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
30	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
<g/>
,	,	kIx,
kevinwarwick	kevinwarwick	k6eAd1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
web	web	k1gInSc1
stránky	stránka	k1gFnSc2
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Eck	Eck	k1gMnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Skripta	skriptum	k1gNnSc2
<g/>
:	:	kIx,
Bionika	Bionik	k1gMnSc2
<g/>
,	,	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
ČVUT	ČVUT	kA
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
biomechatronika	biomechatronika	k1gFnSc1
</s>
<s>
kybernetika	kybernetika	k1gFnSc1
</s>
<s>
kyborg	kyborg	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
bionika	bionik	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Kopírujeme	kopírovat	k5eAaImIp1nP
přírodu	příroda	k1gFnSc4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc4d1
zdroj	zdroj	k1gInSc4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Chip	Chip	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Biologie	biologie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
963	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85014250	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85014250	#num#	k4
</s>
