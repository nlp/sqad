<s>
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
není	být	k5eNaImIp3nS	být
mořem	moře	k1gNnSc7	moře
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
–	–	k?	–
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
slané	slaný	k2eAgNnSc1d1	slané
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
např.	např.	kA	např.
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
<g/>
.	.	kIx.	.
</s>
