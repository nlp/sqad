<s>
Andorra	Andorra	k1gFnSc1	Andorra
la	la	k1gNnSc2	la
Vella	Vello	k1gNnSc2	Vello
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
státu	stát	k1gInSc2	stát
Andorra	Andorra	k1gFnSc1	Andorra
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
22884	[number]	k4	22884
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
30	[number]	k4	30
km2	km2	k4	km2
a	a	k8xC	a
hustotu	hustota	k1gFnSc4	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
762,8	[number]	k4	762,8
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
katalánština	katalánština	k1gFnSc1	katalánština
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
používány	používán	k2eAgInPc1d1	používán
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
španělština	španělština	k1gFnSc1	španělština
a	a	k8xC	a
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Andorra	Andorra	k1gFnSc1	Andorra
la	la	k1gNnSc2	la
Vella	Vello	k1gNnSc2	Vello
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
obchodním	obchodní	k2eAgNnSc7d1	obchodní
střediskem	středisko	k1gNnSc7	středisko
Andorry	Andorra	k1gFnSc2	Andorra
<g/>
.	.	kIx.	.
80	[number]	k4	80
<g/>
%	%	kIx~	%
veškerého	veškerý	k3xTgNnSc2	veškerý
HDP	HDP	kA	HDP
Andorry	Andorra	k1gFnSc2	Andorra
pochází	pocházet	k5eAaImIp3nS	pocházet
pravě	pravě	k6eAd1	pravě
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
také	také	k9	také
centrem	centr	k1gInSc7	centr
několika	několik	k4yIc2	několik
nadnárodních	nadnárodní	k2eAgFnPc2d1	nadnárodní
bank	banka	k1gFnPc2	banka
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
francouzských	francouzský	k2eAgInPc2d1	francouzský
a	a	k8xC	a
španělských	španělský	k2eAgInPc2d1	španělský
<g/>
.	.	kIx.	.
</s>
<s>
Valls	Valls	k1gInSc1	Valls
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Andorra	Andorra	k1gFnSc1	Andorra
la	la	k1gNnSc7	la
Vella	Vello	k1gNnSc2	Vello
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Andorra	Andorra	k1gFnSc1	Andorra
la	la	k1gNnSc7	la
Vella	Vello	k1gNnSc2	Vello
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
