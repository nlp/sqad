<s>
Zažívací	zažívací	k2eAgFnSc1d1	zažívací
soustava	soustava	k1gFnSc1	soustava
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
jedinečná	jedinečný	k2eAgFnSc1d1	jedinečná
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
hrdelní	hrdelní	k2eAgInSc4d1	hrdelní
vak	vak	k1gInSc4	vak
nebo	nebo	k8xC	nebo
vole	vole	k1gNnSc4	vole
pro	pro	k7c4	pro
uskladnění	uskladnění	k1gNnSc4	uskladnění
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
žaludky	žaludek	k1gInPc4	žaludek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
ve	v	k7c6	v
druhém	druhý	k4xOgMnSc6	druhý
jsou	být	k5eAaImIp3nP	být
spolykané	spolykaný	k2eAgInPc4d1	spolykaný
kamínky	kamínek	k1gInPc4	kamínek
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
absenci	absence	k1gFnSc3	absence
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
ptákům	pták	k1gMnPc3	pták
drtit	drtit	k5eAaImF	drtit
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
