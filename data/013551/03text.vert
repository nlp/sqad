<s>
Giffardova	Giffardův	k2eAgFnSc1d1
vzducholoď	vzducholoď	k1gFnSc1
</s>
<s>
Giffardova	Giffardův	k2eAgFnSc1d1
vzducholoď	vzducholoď	k1gFnSc1
Určení	určení	k1gNnSc2
</s>
<s>
experimentální	experimentální	k2eAgFnSc1d1
vzducholoď	vzducholoď	k1gFnSc1
Šéfkonstruktér	šéfkonstruktér	k1gMnSc1
</s>
<s>
Henri	Henri	k1gNnSc1
Giffard	Giffard	k1gMnSc1
První	první	k4xOgMnSc1
let	léto	k1gNnPc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1852	#num#	k4
Vyrobeno	vyrobit	k5eAaPmNgNnS
kusů	kus	k1gInPc2
</s>
<s>
1	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Giffardova	Giffardův	k2eAgFnSc1d1
vzducholoď	vzducholoď	k1gFnSc1
byla	být	k5eAaImAgFnS
první	první	k4xOgInSc4
motorem	motor	k1gInSc7
poháněná	poháněný	k2eAgFnSc1d1
řiditelná	řiditelný	k2eAgFnSc1d1
vzducholoď	vzducholoď	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
vznesla	vznést	k5eAaPmAgFnS
24	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1852	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1
vynálezce	vynálezce	k1gMnSc1
Henri	Henri	k1gNnSc2
Giffard	Giffard	k1gMnSc1
ji	on	k3xPp3gFnSc4
vybavil	vybavit	k5eAaPmAgMnS
speciálně	speciálně	k6eAd1
vylehčeným	vylehčený	k2eAgInSc7d1
parním	parní	k2eAgInSc7d1
strojem	stroj	k1gInSc7
o	o	k7c6
výkonu	výkon	k1gInSc6
2	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
3	#num#	k4
k	k	k7c3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
poháněl	pohánět	k5eAaImAgMnS
vrtuli	vrtule	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
110	#num#	k4
otáček	otáčka	k1gFnPc2
za	za	k7c4
minutu	minuta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palivem	palivo	k1gNnSc7
byl	být	k5eAaImAgInS
koks	koks	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vřetenovité	vřetenovitý	k2eAgNnSc1d1
těleso	těleso	k1gNnSc1
vzducholodi	vzducholoď	k1gFnSc2
bylo	být	k5eAaImAgNnS
plněné	plněný	k2eAgNnSc1d1
vodíkem	vodík	k1gInSc7
<g/>
,	,	kIx,
řízena	řídit	k5eAaImNgFnS
byla	být	k5eAaImAgFnS
trojúhelníkovým	trojúhelníkový	k2eAgNnSc7d1
plachtovým	plachtový	k2eAgNnSc7d1
kormidlem	kormidlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Vzducholoď	vzducholoď	k1gFnSc1
ke	k	k7c3
svému	svůj	k3xOyFgInSc3
prvnímu	první	k4xOgInSc3
a	a	k8xC
jedinému	jediný	k2eAgInSc3d1
letu	let	k1gInSc3
odstartovala	odstartovat	k5eAaPmAgFnS
z	z	k7c2
dostihového	dostihový	k2eAgNnSc2d1
závodiště	závodiště	k1gNnSc2
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosáhla	dosáhnout	k5eAaPmAgFnS
rychlosti	rychlost	k1gFnPc4
10	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
a	a	k8xC
vystoupila	vystoupit	k5eAaPmAgFnS
do	do	k7c2
výšky	výška	k1gFnSc2
1	#num#	k4
800	#num#	k4
m.	m.	k?
Byla	být	k5eAaImAgFnS
schopná	schopný	k2eAgNnPc4d1
manévrování	manévrování	k1gNnSc4
<g/>
,	,	kIx,
dokázala	dokázat	k5eAaPmAgFnS
letět	letět	k5eAaImF
šikmo	šikmo	k6eAd1
na	na	k7c4
směr	směr	k1gInSc4
větru	vítr	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
let	let	k1gInSc4
proti	proti	k7c3
větru	vítr	k1gInSc3
a	a	k8xC
návrat	návrat	k1gInSc1
na	na	k7c4
místo	místo	k1gNnSc4
startu	start	k1gInSc2
slabý	slabý	k2eAgInSc4d1
výkon	výkon	k1gInSc4
motoru	motor	k1gInSc2
nestačil	stačit	k5eNaBmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzducholoď	vzducholoď	k1gFnSc1
uletěla	uletět	k5eAaPmAgFnS
vzdálenost	vzdálenost	k1gFnSc4
27	#num#	k4
km	km	kA
a	a	k8xC
přistála	přistát	k5eAaImAgFnS,k5eAaPmAgFnS
v	v	k7c6
Élancourtu	Élancourt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
první	první	k4xOgFnSc4
plně	plně	k6eAd1
řiditelnou	řiditelný	k2eAgFnSc4d1
vzducholoď	vzducholoď	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
dokázala	dokázat	k5eAaPmAgFnS
vrátit	vrátit	k5eAaPmF
na	na	k7c4
místo	místo	k1gNnSc4
vzletu	vzlet	k1gInSc2
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
až	až	k8xS
La	la	k1gNnSc1
France	Franc	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1884	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Posádka	posádka	k1gFnSc1
<g/>
:	:	kIx,
1	#num#	k4
pilot	pilot	k1gMnSc1
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
:	:	kIx,
44	#num#	k4
m	m	kA
</s>
<s>
Průměr	průměr	k1gInSc1
<g/>
:	:	kIx,
12	#num#	k4
m	m	kA
</s>
<s>
Objem	objem	k1gInSc1
<g/>
:	:	kIx,
2	#num#	k4
500	#num#	k4
m³	m³	k?
</s>
<s>
Pohonná	pohonný	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
:	:	kIx,
parní	parní	k2eAgInSc1d1
motor	motor	k1gInSc1
o	o	k7c6
výkonu	výkon	k1gInSc6
2	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
3	#num#	k4
k	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Dostup	dostup	k1gInSc1
<g/>
:	:	kIx,
1	#num#	k4
800	#num#	k4
m	m	kA
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
10	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
NĚMEČEK	Němeček	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojenská	vojenský	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
letadla	letadlo	k1gNnSc2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Naše	náš	k3xOp1gNnSc1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
206	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
115	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
s.	s.	k?
29	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
,	,	kIx,
86-87	86-87	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Giffardova	Giffardův	k2eAgFnSc1d1
vzducholoď	vzducholoď	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Aerial	Aerial	k1gInSc1
Navigation	Navigation	k1gInSc1
by	by	kYmCp3nS
Octave	Octav	k1gInSc5
Chanute	Chanut	k1gInSc5
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Les	les	k1gInSc1
ballons	ballons	k1gInSc1
dirigeables	dirigeables	k1gInSc1
:	:	kIx,
expériences	expériences	k1gInSc1
de	de	k?
m.	m.	k?
Henri	Henri	k1gNnPc2
Giffard	Giffard	k1gInSc1
en	en	k?
1852	#num#	k4
et	et	k?
en	en	k?
1855	#num#	k4
et	et	k?
de	de	k?
m.	m.	k?
Dupuy	Dupua	k1gFnSc2
de	de	k?
Lôme	Lôme	k1gInSc1
en	en	k?
1872	#num#	k4
na	na	k7c4
Internet	Internet	k1gInSc4
Archive	archiv	k1gInSc5
</s>
<s>
Detailní	detailní	k2eAgFnSc1d1
fotografie	fotografie	k1gFnSc1
modelu	model	k1gInSc2
Giffardovy	Giffardův	k2eAgFnSc2d1
vzducholodi	vzducholoď	k1gFnSc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
