<p>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
(	(	kIx(	(
<g/>
NaOH	NaOH	k1gFnSc1	NaOH
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
zásaditá	zásaditý	k2eAgFnSc1d1	zásaditá
anorganická	anorganický	k2eAgFnSc1d1	anorganická
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Zastaralé	zastaralý	k2eAgInPc1d1	zastaralý
triviální	triviální	k2eAgInPc1d1	triviální
názvy	název	k1gInPc1	název
této	tento	k3xDgFnSc2	tento
látky	látka	k1gFnSc2	látka
jsou	být	k5eAaImIp3nP	být
natron	natron	k1gInSc4	natron
nebo	nebo	k8xC	nebo
louh	louh	k1gInSc4	louh
sodný	sodný	k2eAgInSc4d1	sodný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
kódem	kód	k1gInSc7	kód
E	E	kA	E
524	[number]	k4	524
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pevná	pevný	k2eAgFnSc1d1	pevná
bílá	bílý	k2eAgFnSc1d1	bílá
látka	látka	k1gFnSc1	látka
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
malých	malý	k2eAgFnPc2d1	malá
perliček	perlička	k1gFnPc2	perlička
<g/>
,	,	kIx,	,
peciček	pecička	k1gFnPc2	pecička
<g/>
,	,	kIx,	,
lístečků	lísteček	k1gInPc2	lísteček
nebo	nebo	k8xC	nebo
granulí	granule	k1gFnPc2	granule
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
hygroskopická	hygroskopický	k2eAgFnSc1d1	hygroskopická
a	a	k8xC	a
pohlcující	pohlcující	k2eAgInSc1d1	pohlcující
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
uhličitan	uhličitan	k1gInSc1	uhličitan
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
uchovávána	uchovávat	k5eAaImNgFnS	uchovávat
v	v	k7c6	v
hermeticky	hermeticky	k6eAd1	hermeticky
uzavřených	uzavřený	k2eAgInPc6d1	uzavřený
obalech	obal	k1gInPc6	obal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příprava	příprava	k1gFnSc1	příprava
==	==	k?	==
</s>
</p>
<p>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
(	(	kIx(	(
<g/>
NaOH	NaOH	k1gFnSc1	NaOH
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
bouřlivou	bouřlivý	k2eAgFnSc7d1	bouřlivá
exotermní	exotermní	k2eAgFnSc7d1	exotermní
reakcí	reakce	k1gFnSc7	reakce
kovového	kovový	k2eAgInSc2d1	kovový
sodíku	sodík	k1gInSc2	sodík
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
<g/>
;	;	kIx,	;
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
kyslíku	kyslík	k1gInSc2	kyslík
vytvářející	vytvářející	k2eAgFnSc1d1	vytvářející
se	se	k3xPyFc4	se
vodík	vodík	k1gInSc1	vodík
vzplane	vzplanout	k5eAaPmIp3nS	vzplanout
</s>
</p>
<p>
<s>
2	[number]	k4	2
Na	na	k7c4	na
+	+	kIx~	+
2	[number]	k4	2
H2O	H2O	k1gFnSc2	H2O
→	→	k?	→
2	[number]	k4	2
NaOH	NaOH	k1gFnSc1	NaOH
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
také	také	k9	také
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
oxidu	oxid	k1gInSc2	oxid
sodného	sodný	k2eAgInSc2d1	sodný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
</s>
</p>
<p>
<s>
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
→	→	k?	→
2	[number]	k4	2
NaOH	NaOH	k1gFnPc2	NaOH
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
se	se	k3xPyFc4	se
také	také	k9	také
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
ze	z	k7c2	z
solí	sůl	k1gFnPc2	sůl
velmi	velmi	k6eAd1	velmi
slabých	slabý	k2eAgFnPc2d1	slabá
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
;	;	kIx,	;
příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
hydrolýza	hydrolýza	k1gFnSc1	hydrolýza
alkoholátů	alkoholát	k1gInPc2	alkoholát
sodných	sodný	k2eAgInPc2d1	sodný
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ethoxidu	ethoxid	k1gInSc2	ethoxid
(	(	kIx(	(
<g/>
ethanolátu	ethanolát	k1gInSc2	ethanolát
<g/>
)	)	kIx)	)
sodného	sodný	k2eAgInSc2d1	sodný
</s>
</p>
<p>
<s>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
ONa	onen	k3xDgFnSc1	onen
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
→	→	k?	→
CH3CH2OH	CH3CH2OH	k1gFnSc1	CH3CH2OH
+	+	kIx~	+
NaOH	NaOH	k1gFnSc1	NaOH
<g/>
.	.	kIx.	.
<g/>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
také	také	k9	také
v	v	k7c6	v
katodovém	katodový	k2eAgInSc6d1	katodový
prostoru	prostor	k1gInSc6	prostor
při	při	k7c6	při
elektrolýze	elektrolýza	k1gFnSc6	elektrolýza
vodných	vodný	k2eAgInPc2d1	vodný
roztoků	roztok	k1gInPc2	roztok
sodných	sodný	k2eAgFnPc2d1	sodná
solí	sůl	k1gFnPc2	sůl
<g/>
,	,	kIx,	,
především	především	k9	především
chloridu	chlorid	k1gInSc2	chlorid
sodného	sodný	k2eAgInSc2d1	sodný
</s>
</p>
<p>
<s>
2	[number]	k4	2
Na	na	k7c4	na
<g/>
+	+	kIx~	+
+	+	kIx~	+
2	[number]	k4	2
H2O	H2O	k1gFnPc2	H2O
+	+	kIx~	+
2	[number]	k4	2
e-	e-	k?	e-
→	→	k?	→
H2	H2	k1gFnSc2	H2
+	+	kIx~	+
2	[number]	k4	2
NaOH	NaOH	k1gFnPc2	NaOH
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
tzv.	tzv.	kA	tzv.
kaustifikačním	kaustifikační	k2eAgInSc7d1	kaustifikační
procesem	proces	k1gInSc7	proces
z	z	k7c2	z
vodného	vodné	k1gNnSc2	vodné
roztoku	roztok	k1gInSc3	roztok
uhličitanu	uhličitan	k1gInSc2	uhličitan
sodného	sodný	k2eAgInSc2d1	sodný
(	(	kIx(	(
<g/>
sody	soda	k1gFnSc2	soda
<g/>
)	)	kIx)	)
působením	působení	k1gNnPc3	působení
mírného	mírný	k2eAgInSc2d1	mírný
nadbytku	nadbytek	k1gInSc2	nadbytek
hydroxidu	hydroxid	k1gInSc2	hydroxid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
<g/>
,	,	kIx,	,
přidávaného	přidávaný	k2eAgInSc2d1	přidávaný
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
suspenze	suspenze	k1gFnSc2	suspenze
hašeného	hašený	k2eAgNnSc2d1	hašené
vápna	vápno	k1gNnSc2	vápno
(	(	kIx(	(
<g/>
vápenného	vápenný	k2eAgNnSc2d1	vápenné
mléka	mléko	k1gNnSc2	mléko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
CO	co	k9	co
<g/>
3	[number]	k4	3
+	+	kIx~	+
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
→	→	k?	→
2	[number]	k4	2
NaOH	NaOH	k1gFnSc1	NaOH
+	+	kIx~	+
CaCO	CaCO	k1gFnSc1	CaCO
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
Vznikající	vznikající	k2eAgMnSc1d1	vznikající
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
rozpustný	rozpustný	k2eAgInSc4d1	rozpustný
uhličitan	uhličitan	k1gInSc4	uhličitan
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
se	se	k3xPyFc4	se
oddělil	oddělit	k5eAaPmAgInS	oddělit
sedimentací	sedimentace	k1gFnSc7	sedimentace
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
následnou	následný	k2eAgFnSc7d1	následná
filtrací	filtrace	k1gFnSc7	filtrace
a	a	k8xC	a
odpařením	odpaření	k1gNnSc7	odpaření
vody	voda	k1gFnSc2	voda
ze	z	k7c2	z
zbývajícího	zbývající	k2eAgInSc2d1	zbývající
roztoku	roztok	k1gInSc2	roztok
se	se	k3xPyFc4	se
získal	získat	k5eAaPmAgInS	získat
surový	surový	k2eAgInSc1d1	surový
hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
dále	daleko	k6eAd2	daleko
čistil	čistit	k5eAaImAgInS	čistit
rozpuštěním	rozpuštění	k1gNnSc7	rozpuštění
v	v	k7c6	v
ethanolu	ethanol	k1gInSc6	ethanol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
veškerý	veškerý	k3xTgInSc1	veškerý
hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
elektrolytickým	elektrolytický	k2eAgInSc7d1	elektrolytický
rozkladem	rozklad	k1gInSc7	rozklad
roztoku	roztok	k1gInSc2	roztok
chloridu	chlorid	k1gInSc2	chlorid
sodného	sodný	k2eAgInSc2d1	sodný
(	(	kIx(	(
<g/>
solanky	solanka	k1gFnSc2	solanka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
produktem	produkt	k1gInSc7	produkt
elektrolýzy	elektrolýza	k1gFnSc2	elektrolýza
je	být	k5eAaImIp3nS	být
plynný	plynný	k2eAgInSc1d1	plynný
chlor	chlor	k1gInSc1	chlor
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
tří	tři	k4xCgFnPc2	tři
různých	různý	k2eAgFnPc2d1	různá
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
se	se	k3xPyFc4	se
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
zpětné	zpětný	k2eAgFnSc3d1	zpětná
reakci	reakce	k1gFnSc3	reakce
chlóru	chlór	k1gInSc2	chlór
s	s	k7c7	s
vzniklým	vzniklý	k2eAgInSc7d1	vzniklý
hydroxidem	hydroxid	k1gInSc7	hydroxid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
amalgamové	amalgamový	k2eAgFnSc6d1	amalgamová
metodě	metoda	k1gFnSc6	metoda
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
Castner-Kellnerův	Castner-Kellnerův	k2eAgInSc1d1	Castner-Kellnerův
elektrolyzér	elektrolyzér	k1gInSc1	elektrolyzér
(	(	kIx(	(
<g/>
vynalezený	vynalezený	k2eAgMnSc1d1	vynalezený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k8xS	jako
katoda	katoda	k1gFnSc1	katoda
slouží	sloužit	k5eAaImIp3nS	sloužit
kovová	kovový	k2eAgFnSc1d1	kovová
rtuť	rtuť	k1gFnSc1	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
redukce	redukce	k1gFnSc1	redukce
sodných	sodný	k2eAgInPc2d1	sodný
iontů	ion	k1gInPc2	ion
na	na	k7c4	na
elementární	elementární	k2eAgInSc4d1	elementární
sodík	sodík	k1gInSc4	sodík
</s>
</p>
<p>
<s>
Na	na	k7c4	na
<g/>
+	+	kIx~	+
+	+	kIx~	+
e-	e-	k?	e-
→	→	k?	→
Na	na	k7c6	na
<g/>
,	,	kIx,	,
<g/>
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
rtuti	rtuť	k1gFnSc6	rtuť
rozpustí	rozpustit	k5eAaPmIp3nS	rozpustit
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
kapalného	kapalný	k2eAgInSc2d1	kapalný
amalgámu	amalgám	k1gInSc2	amalgám
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zabrání	zabránit	k5eAaPmIp3nS	zabránit
okamžité	okamžitý	k2eAgFnSc3d1	okamžitá
reakci	reakce	k1gFnSc3	reakce
kovového	kovový	k2eAgInSc2d1	kovový
sodíku	sodík	k1gInSc2	sodík
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
na	na	k7c4	na
hydroxid	hydroxid	k1gInSc4	hydroxid
sodný	sodný	k2eAgInSc4d1	sodný
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc4	tento
rozklad	rozklad	k1gInSc4	rozklad
probíhá	probíhat	k5eAaImIp3nS	probíhat
následně	následně	k6eAd1	následně
v	v	k7c6	v
oddělené	oddělený	k2eAgFnSc6d1	oddělená
reakční	reakční	k2eAgFnSc6d1	reakční
prostoře	prostora	k1gFnSc6	prostora
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
rozkladač	rozkladač	k1gInSc1	rozkladač
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
kapalný	kapalný	k2eAgInSc1d1	kapalný
amalgam	amalgam	k1gInSc1	amalgam
přečerpává	přečerpávat	k5eAaImIp3nS	přečerpávat
<g/>
.	.	kIx.	.
</s>
<s>
Rtuť	rtuť	k1gFnSc1	rtuť
zbavená	zbavený	k2eAgFnSc1d1	zbavená
sodíku	sodík	k1gInSc2	sodík
v	v	k7c6	v
rozkladači	rozkladač	k1gInSc6	rozkladač
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
elektrolyzéru	elektrolyzér	k1gInSc2	elektrolyzér
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
postup	postup	k1gInSc4	postup
užívali	užívat	k5eAaImAgMnP	užívat
naši	náš	k3xOp1gMnPc1	náš
dva	dva	k4xCgMnPc1	dva
největší	veliký	k2eAgMnPc1d3	veliký
výrobci	výrobce	k1gMnPc1	výrobce
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
,	,	kIx,	,
Spolana	Spolana	k1gFnSc1	Spolana
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
Neratovice	Neratovice	k1gFnSc1	Neratovice
a	a	k8xC	a
Spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
chemickou	chemický	k2eAgFnSc4d1	chemická
a	a	k8xC	a
hutní	hutní	k2eAgFnSc4d1	hutní
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
a	a	k8xC	a
také	také	k9	také
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
výrobců	výrobce	k1gMnPc2	výrobce
chlóru	chlór	k1gInSc2	chlór
a	a	k8xC	a
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
(	(	kIx(	(
<g/>
EU	EU	kA	EU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
tohoto	tento	k3xDgInSc2	tento
postupu	postup	k1gInSc2	postup
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
velmi	velmi	k6eAd1	velmi
čisté	čistý	k2eAgFnPc4d1	čistá
suroviny	surovina	k1gFnPc4	surovina
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
vysoký	vysoký	k2eAgInSc1d1	vysoký
rozkladný	rozkladný	k2eAgInSc1d1	rozkladný
potenciál	potenciál	k1gInSc1	potenciál
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
vyšší	vysoký	k2eAgInPc4d2	vyšší
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
<g/>
)	)	kIx)	)
a	a	k8xC	a
použití	použití	k1gNnSc1	použití
jedovaté	jedovatý	k2eAgFnSc2d1	jedovatá
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
druhé	druhý	k4xOgFnSc6	druhý
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
diafragmové	diafragmový	k2eAgFnSc3d1	diafragmový
metodě	metoda	k1gFnSc3	metoda
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Griesheimově	Griesheimův	k2eAgInSc6d1	Griesheimův
článku	článek	k1gInSc6	článek
anodový	anodový	k2eAgInSc4d1	anodový
a	a	k8xC	a
katodový	katodový	k2eAgInSc4d1	katodový
prostor	prostor	k1gInSc4	prostor
vzájemně	vzájemně	k6eAd1	vzájemně
odděleny	oddělit	k5eAaPmNgFnP	oddělit
polopropustnou	polopropustný	k2eAgFnSc7d1	polopropustná
stěnou	stěna	k1gFnSc7	stěna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sice	sice	k8xC	sice
dovolí	dovolit	k5eAaPmIp3nS	dovolit
putovat	putovat	k5eAaImF	putovat
v	v	k7c6	v
elektrostatickém	elektrostatický	k2eAgNnSc6d1	elektrostatické
poli	pole	k1gNnSc6	pole
malým	malý	k2eAgInPc3d1	malý
iontům	ion	k1gInPc3	ion
Na	na	k7c4	na
<g/>
+	+	kIx~	+
a	a	k8xC	a
molekulám	molekula	k1gFnPc3	molekula
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zabrání	zabránit	k5eAaPmIp3nS	zabránit
prostupu	prostup	k1gInSc2	prostup
molekul	molekula	k1gFnPc2	molekula
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
chlóru	chlór	k1gInSc2	chlór
Cl	Cl	k1gFnSc2	Cl
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
anody	anoda	k1gFnSc2	anoda
se	se	k3xPyFc4	se
kontinuálně	kontinuálně	k6eAd1	kontinuálně
přivádí	přivádět	k5eAaImIp3nS	přivádět
roztok	roztok	k1gInSc1	roztok
(	(	kIx(	(
<g/>
solanka	solanka	k1gFnSc1	solanka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
anodě	anoda	k1gFnSc6	anoda
ionty	ion	k1gInPc4	ion
chloru	chlor	k1gInSc2	chlor
odevzdávají	odevzdávat	k5eAaImIp3nP	odevzdávat
elektron	elektron	k1gInSc1	elektron
a	a	k8xC	a
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
na	na	k7c4	na
atomární	atomární	k2eAgInSc4d1	atomární
chlor	chlor	k1gInSc4	chlor
a	a	k8xC	a
následně	následně	k6eAd1	následně
na	na	k7c4	na
molekulární	molekulární	k2eAgInSc4d1	molekulární
chlór	chlór	k1gInSc4	chlór
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
</s>
</p>
<p>
<s>
2	[number]	k4	2
Cl-	Cl-	k1gFnSc1	Cl-
-	-	kIx~	-
2	[number]	k4	2
e-	e-	k?	e-
→	→	k?	→
2	[number]	k4	2
Cl	Cl	k1gFnSc2	Cl
→	→	k?	→
Cl	Cl	k1gMnSc1	Cl
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
<g/>
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
katodě	katoda	k1gFnSc6	katoda
se	se	k3xPyFc4	se
redukují	redukovat	k5eAaBmIp3nP	redukovat
přibráním	přibrání	k1gNnSc7	přibrání
elektronu	elektron	k1gInSc2	elektron
oxoniové	oxoniový	k2eAgInPc4d1	oxoniový
kationty	kation	k1gInPc4	kation
na	na	k7c4	na
vodík	vodík	k1gInSc4	vodík
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
</s>
</p>
<p>
<s>
2	[number]	k4	2
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
+	+	kIx~	+
+	+	kIx~	+
2	[number]	k4	2
e-	e-	k?	e-
→	→	k?	→
2	[number]	k4	2
H2O	H2O	k1gFnPc2	H2O
+	+	kIx~	+
2	[number]	k4	2
H	H	kA	H
→	→	k?	→
2	[number]	k4	2
H2O	H2O	k1gFnSc1	H2O
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
katodovém	katodový	k2eAgInSc6d1	katodový
prostoru	prostor	k1gInSc6	prostor
se	se	k3xPyFc4	se
hromadí	hromadit	k5eAaImIp3nP	hromadit
sodné	sodný	k2eAgInPc1d1	sodný
kationty	kation	k1gInPc1	kation
Na	na	k7c4	na
<g/>
+	+	kIx~	+
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
kladný	kladný	k2eAgInSc1d1	kladný
náboj	náboj	k1gInSc1	náboj
je	být	k5eAaImIp3nS	být
kompenzován	kompenzovat	k5eAaBmNgInS	kompenzovat
růstem	růst	k1gInSc7	růst
koncentrace	koncentrace	k1gFnSc2	koncentrace
záporných	záporný	k2eAgInPc2d1	záporný
hydroxylových	hydroxylový	k2eAgInPc2d1	hydroxylový
aniontů	anion	k1gInPc2	anion
OH-	OH-	k1gMnPc2	OH-
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zde	zde	k6eAd1	zde
efektivně	efektivně	k6eAd1	efektivně
vzniká	vznikat	k5eAaImIp3nS	vznikat
hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
.	.	kIx.	.
</s>
<s>
Odpouštěný	odpouštěný	k2eAgInSc1d1	odpouštěný
roztok	roztok	k1gInSc1	roztok
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vedle	vedle	k7c2	vedle
sebe	se	k3xPyFc2	se
zbytky	zbytek	k1gInPc4	zbytek
rozpuštěného	rozpuštěný	k2eAgInSc2d1	rozpuštěný
chloridu	chlorid	k1gInSc2	chlorid
sodného	sodný	k2eAgInSc2d1	sodný
a	a	k8xC	a
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
následně	následně	k6eAd1	následně
oddělit	oddělit	k5eAaPmF	oddělit
<g/>
.	.	kIx.	.
</s>
<s>
Diafragma	diafragma	k1gFnSc1	diafragma
(	(	kIx(	(
<g/>
polopropustná	polopropustný	k2eAgFnSc1d1	polopropustná
stěna	stěna	k1gFnSc1	stěna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
zhotovována	zhotovovat	k5eAaImNgFnS	zhotovovat
z	z	k7c2	z
azbestu	azbest	k1gInSc2	azbest
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
ekologicky	ekologicky	k6eAd1	ekologicky
nevhodný	vhodný	k2eNgInSc1d1	nevhodný
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
hledají	hledat	k5eAaImIp3nP	hledat
a	a	k8xC	a
zkoušejí	zkoušet	k5eAaImIp3nP	zkoušet
jeho	jeho	k3xOp3gFnPc4	jeho
jiné	jiný	k2eAgFnPc4d1	jiná
náhrady	náhrada	k1gFnPc4	náhrada
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
tímto	tento	k3xDgInSc7	tento
postupem	postup	k1gInSc7	postup
se	se	k3xPyFc4	se
produkuje	produkovat	k5eAaImIp3nS	produkovat
asi	asi	k9	asi
75	[number]	k4	75
%	%	kIx~	%
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgFnSc7	třetí
<g/>
,	,	kIx,	,
nejmodernější	moderní	k2eAgFnSc7d3	nejmodernější
metodou	metoda	k1gFnSc7	metoda
zavedenou	zavedený	k2eAgFnSc7d1	zavedená
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
a	a	k8xC	a
používanou	používaný	k2eAgFnSc4d1	používaná
v	v	k7c4	v
90	[number]	k4	90
%	%	kIx~	%
továren	továrna	k1gFnPc2	továrna
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
membránová	membránový	k2eAgFnSc1d1	membránová
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
modifikací	modifikace	k1gFnSc7	modifikace
předchozího	předchozí	k2eAgInSc2d1	předchozí
postupu	postup	k1gInSc2	postup
<g/>
.	.	kIx.	.
</s>
<s>
Anodový	anodový	k2eAgInSc4d1	anodový
a	a	k8xC	a
katodový	katodový	k2eAgInSc4d1	katodový
prostor	prostor	k1gInSc4	prostor
jsou	být	k5eAaImIp3nP	být
odděleny	oddělen	k2eAgInPc1d1	oddělen
ionexovou	ionexový	k2eAgFnSc7d1	ionexová
membránou	membrána	k1gFnSc7	membrána
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přestup	přestup	k1gInSc4	přestup
pouze	pouze	k6eAd1	pouze
kationtům	kation	k1gInPc3	kation
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
O	O	kA	O
<g/>
+	+	kIx~	+
a	a	k8xC	a
Na	na	k7c4	na
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
anody	anoda	k1gFnSc2	anoda
se	se	k3xPyFc4	se
kontinuálně	kontinuálně	k6eAd1	kontinuálně
přidává	přidávat	k5eAaImIp3nS	přidávat
nasycená	nasycený	k2eAgFnSc1d1	nasycená
solanka	solanka	k1gFnSc1	solanka
a	a	k8xC	a
odčerpává	odčerpávat	k5eAaImIp3nS	odčerpávat
se	se	k3xPyFc4	se
vyčerpaná	vyčerpaný	k2eAgFnSc1d1	vyčerpaná
<g/>
;	;	kIx,	;
naopak	naopak	k6eAd1	naopak
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
katody	katoda	k1gFnSc2	katoda
se	se	k3xPyFc4	se
čerpá	čerpat	k5eAaImIp3nS	čerpat
zředěný	zředěný	k2eAgInSc1d1	zředěný
cca	cca	kA	cca
30	[number]	k4	30
%	%	kIx~	%
roztok	roztok	k1gInSc1	roztok
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
a	a	k8xC	a
odčerpává	odčerpávat	k5eAaImIp3nS	odčerpávat
se	se	k3xPyFc4	se
roztok	roztok	k1gInSc1	roztok
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
o	o	k7c6	o
koncentraci	koncentrace	k1gFnSc6	koncentrace
32,5	[number]	k4	32,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
zavedena	zavést	k5eAaPmNgFnS	zavést
právě	právě	k9	právě
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
a	a	k8xC	a
určitě	určitě	k6eAd1	určitě
není	být	k5eNaImIp3nS	být
bez	bez	k7c2	bez
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
,	,	kIx,	,
že	že	k8xS	že
membránová	membránový	k2eAgFnSc1d1	membránová
elektrolýza	elektrolýza	k1gFnSc1	elektrolýza
ve	v	k7c6	v
Spolchemiije	Spolchemiije	k1gFnSc6	Spolchemiije
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
s	s	k7c7	s
elektrolyzéry	elektrolyzér	k1gInPc7	elektrolyzér
firmy	firma	k1gFnSc2	firma
Bluestar	Bluestara	k1gFnPc2	Bluestara
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
kompaktní	kompaktní	k2eAgFnSc7d1	kompaktní
kombinovanou	kombinovaný	k2eAgFnSc7d1	kombinovaná
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
(	(	kIx(	(
<g/>
KOH	KOH	kA	KOH
i	i	k8xC	i
NaOH	NaOH	k1gFnSc1	NaOH
integrované	integrovaný	k2eAgNnSc4d1	integrované
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
výrobně	výrobna	k1gFnSc6	výrobna
<g/>
)	)	kIx)	)
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k8xC	i
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
finální	finální	k2eAgInSc1d1	finální
produkt	produkt	k1gInSc1	produkt
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
prvovýroby	prvovýroba	k1gFnSc2	prvovýroba
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
vodní	vodní	k2eAgInSc1d1	vodní
roztok	roztok	k1gInSc1	roztok
obsahující	obsahující	k2eAgInSc1d1	obsahující
50	[number]	k4	50
%	%	kIx~	%
NaOH	NaOH	k1gFnSc2	NaOH
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
odvodňuje	odvodňovat	k5eAaImIp3nS	odvodňovat
a	a	k8xC	a
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
do	do	k7c2	do
formy	forma	k1gFnSc2	forma
peciček	pecička	k1gFnPc2	pecička
<g/>
,	,	kIx,	,
granulí	granule	k1gFnPc2	granule
nebo	nebo	k8xC	nebo
lístků	lístek	k1gInPc2	lístek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
chemických	chemický	k2eAgFnPc6d1	chemická
laboratořích	laboratoř	k1gFnPc6	laboratoř
<g/>
,	,	kIx,	,
v	v	k7c6	v
chemické	chemický	k2eAgFnSc6d1	chemická
malovýrobě	malovýroba	k1gFnSc6	malovýroba
a	a	k8xC	a
ve	v	k7c6	v
farmaceutickém	farmaceutický	k2eAgInSc6d1	farmaceutický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vodném	vodné	k1gNnSc6	vodné
roztoku	roztok	k1gInSc2	roztok
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
molekuly	molekula	k1gFnPc4	molekula
<g/>
"	"	kIx"	"
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
plně	plně	k6eAd1	plně
disociovány	disociován	k2eAgInPc1d1	disociován
na	na	k7c4	na
sodné	sodný	k2eAgInPc4d1	sodný
ionty	ion	k1gInPc4	ion
a	a	k8xC	a
hydroxidové	hydroxidový	k2eAgInPc4d1	hydroxidový
anionty	anion	k1gInPc4	anion
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
silnou	silný	k2eAgFnSc7d1	silná
zásadou	zásada	k1gFnSc7	zásada
<g/>
.	.	kIx.	.
</s>
<s>
Vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
(	(	kIx(	(
<g/>
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
49	[number]	k4	49
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
čirá	čirý	k2eAgFnSc1d1	čirá
viskózní	viskózní	k2eAgFnSc1d1	viskózní
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
pod	pod	k7c4	pod
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
<g/>
.	.	kIx.	.
</s>
<s>
Bezvodý	bezvodý	k2eAgMnSc1d1	bezvodý
NaOH	NaOH	k1gMnSc1	NaOH
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
nebo	nebo	k8xC	nebo
amorfní	amorfní	k2eAgFnSc1d1	amorfní
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
ethanolu	ethanol	k1gInSc6	ethanol
<g/>
,	,	kIx,	,
methanolu	methanol	k1gInSc6	methanol
a	a	k8xC	a
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nerozpustný	rozpustný	k2eNgMnSc1d1	nerozpustný
v	v	k7c6	v
diethyletheru	diethylether	k1gInSc6	diethylether
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rozpouštění	rozpouštění	k1gNnSc6	rozpouštění
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
hygroskopický	hygroskopický	k2eAgMnSc1d1	hygroskopický
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ponechán	ponechat	k5eAaPmNgInS	ponechat
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
brzy	brzy	k6eAd1	brzy
rozteče	roztéct	k5eAaPmIp3nS	roztéct
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
koncentrovaný	koncentrovaný	k2eAgInSc4d1	koncentrovaný
roztok	roztok	k1gInSc4	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Reaguje	reagovat	k5eAaBmIp3nS	reagovat
i	i	k9	i
s	s	k7c7	s
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
uhličitanu	uhličitan	k1gInSc2	uhličitan
sodného	sodný	k2eAgInSc2d1	sodný
</s>
</p>
<p>
<s>
2	[number]	k4	2
NaOH	NaOH	k1gFnSc1	NaOH
+	+	kIx~	+
CO2	CO2	k1gFnSc1	CO2
→	→	k?	→
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
CO	co	k9	co
<g/>
3	[number]	k4	3
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
bývala	bývat	k5eAaImAgFnS	bývat
využívána	využívat	k5eAaPmNgFnS	využívat
ve	v	k7c6	v
starších	starý	k2eAgFnPc6d2	starší
filtroventilačních	filtroventilační	k2eAgFnPc6d1	filtroventilační
jednotkách	jednotka	k1gFnPc6	jednotka
a	a	k8xC	a
systémech	systém	k1gInPc6	systém
s	s	k7c7	s
uzavřeným	uzavřený	k2eAgInSc7d1	uzavřený
koloběhem	koloběh	k1gInSc7	koloběh
vzduchu	vzduch	k1gInSc2	vzduch
k	k	k7c3	k
zachycování	zachycování	k1gNnSc3	zachycování
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
;	;	kIx,	;
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
bylo	být	k5eAaImAgNnS	být
spékání	spékání	k1gNnSc1	spékání
absorpčního	absorpční	k2eAgNnSc2d1	absorpční
činidla	činidlo	k1gNnSc2	činidlo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
při	při	k7c6	při
aplikacích	aplikace	k1gFnPc6	aplikace
v	v	k7c6	v
pilotované	pilotovaný	k2eAgFnSc6d1	pilotovaná
kosmonautice	kosmonautika	k1gFnSc6	kosmonautika
nahrazován	nahrazovat	k5eAaImNgInS	nahrazovat
hydroxidem	hydroxid	k1gInSc7	hydroxid
lithným	lithný	k2eAgInSc7d1	lithný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
reaguje	reagovat	k5eAaBmIp3nS	reagovat
i	i	k9	i
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
plynnými	plynný	k2eAgInPc7d1	plynný
oxidy	oxid	k1gInPc7	oxid
<g/>
,	,	kIx,	,
např.	např.	kA	např.
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
váže	vázat	k5eAaImIp3nS	vázat
jako	jako	k9	jako
siřičitan	siřičitan	k1gInSc1	siřičitan
sodný	sodný	k2eAgInSc1d1	sodný
</s>
</p>
<p>
<s>
2	[number]	k4	2
NaOH	NaOH	k1gFnSc1	NaOH
+	+	kIx~	+
SO2	SO2	k1gFnSc1	SO2
→	→	k?	→
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
3	[number]	k4	3
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
</s>
</p>
<p>
<s>
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
využito	využít	k5eAaPmNgNnS	využít
k	k	k7c3	k
odstraňování	odstraňování	k1gNnSc3	odstraňování
tohoto	tento	k3xDgInSc2	tento
jedovatého	jedovatý	k2eAgInSc2d1	jedovatý
plynu	plyn	k1gInSc2	plyn
z	z	k7c2	z
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
oxidem	oxid	k1gInSc7	oxid
křemičitým	křemičitý	k2eAgInSc7d1	křemičitý
reaguje	reagovat	k5eAaBmIp3nS	reagovat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
rozpustného	rozpustný	k2eAgInSc2d1	rozpustný
ortokřemičitanu	ortokřemičitan	k1gInSc2	ortokřemičitan
sodného	sodný	k2eAgInSc2d1	sodný
</s>
</p>
<p>
<s>
4	[number]	k4	4
NaOH	NaOH	k1gFnSc1	NaOH
+	+	kIx~	+
SiO	SiO	k1gFnSc1	SiO
<g/>
2	[number]	k4	2
→	→	k?	→
Na	na	k7c4	na
<g/>
4	[number]	k4	4
<g/>
SiO	SiO	k1gFnPc2	SiO
<g/>
4	[number]	k4	4
+	+	kIx~	+
2	[number]	k4	2
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
<g/>
Protože	protože	k8xS	protože
sklo	sklo	k1gNnSc1	sklo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
značný	značný	k2eAgInSc4d1	značný
podíl	podíl	k1gInSc4	podíl
oxidu	oxid	k1gInSc2	oxid
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
<g/>
,	,	kIx,	,
dlouhým	dlouhý	k2eAgNnSc7d1	dlouhé
působením	působení	k1gNnSc7	působení
koncentrovaného	koncentrovaný	k2eAgInSc2d1	koncentrovaný
roztoku	roztok	k1gInSc2	roztok
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
jeho	jeho	k3xOp3gInSc2	jeho
povrch	povrch	k1gInSc4	povrch
matní	matnět	k5eAaImIp3nP	matnět
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
závažné	závažný	k2eAgNnSc1d1	závažné
je	být	k5eAaImIp3nS	být
působení	působení	k1gNnSc1	působení
NaOH	NaOH	k1gFnSc2	NaOH
na	na	k7c4	na
skleněné	skleněný	k2eAgFnPc4d1	skleněná
laboratorní	laboratorní	k2eAgFnPc4d1	laboratorní
aparatury	aparatura	k1gFnPc4	aparatura
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgInPc2	který
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
"	"	kIx"	"
<g/>
zamrznutí	zamrznutí	k1gNnSc3	zamrznutí
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
uváděn	uváděn	k2eAgInSc1d1	uváděn
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
zakousnutí	zakousnutí	k1gNnSc1	zakousnutí
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
skleněných	skleněný	k2eAgInPc2d1	skleněný
kohoutů	kohout	k1gInPc2	kohout
<g/>
,	,	kIx,	,
či	či	k8xC	či
zabroušených	zabroušený	k2eAgInPc2d1	zabroušený
spojů	spoj	k1gInPc2	spoj
nebo	nebo	k8xC	nebo
i	i	k9	i
skleněných	skleněný	k2eAgFnPc2d1	skleněná
zátek	zátka	k1gFnPc2	zátka
u	u	k7c2	u
zásobních	zásobní	k2eAgFnPc2d1	zásobní
lahví	lahev	k1gFnPc2	lahev
s	s	k7c7	s
roztokem	roztok	k1gInSc7	roztok
hydroxidu	hydroxid	k1gInSc2	hydroxid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
kyselinami	kyselina	k1gFnPc7	kyselina
tvoří	tvořit	k5eAaImIp3nS	tvořit
reakcí	reakce	k1gFnSc7	reakce
nazývanou	nazývaný	k2eAgFnSc7d1	nazývaná
neutralizace	neutralizace	k1gFnPc4	neutralizace
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
např.	např.	kA	např.
s	s	k7c7	s
chlorovodíkovou	chlorovodíkový	k2eAgFnSc7d1	chlorovodíková
(	(	kIx(	(
<g/>
solnou	solný	k2eAgFnSc7d1	solná
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgInSc1d1	sodný
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
</s>
</p>
<p>
<s>
NaOH	NaOH	k?	NaOH
+	+	kIx~	+
HCl	HCl	k1gMnSc1	HCl
→	→	k?	→
NaCl	NaCl	k1gMnSc1	NaCl
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.S	O.S	k1gMnPc2	O.S
vícesytnými	vícesytný	k2eAgFnPc7d1	vícesytný
kyselinami	kyselina	k1gFnPc7	kyselina
tvoří	tvořit	k5eAaImIp3nS	tvořit
podle	podle	k7c2	podle
množství	množství	k1gNnSc2	množství
normální	normální	k2eAgFnSc2d1	normální
nebo	nebo	k8xC	nebo
kyselé	kyselý	k2eAgFnSc2d1	kyselá
soli	sůl	k1gFnSc2	sůl
(	(	kIx(	(
<g/>
hydrogensoli	hydrogensoli	k6eAd1	hydrogensoli
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
nejprve	nejprve	k6eAd1	nejprve
hydrogensíran	hydrogensíran	k1gInSc1	hydrogensíran
sodný	sodný	k2eAgInSc1d1	sodný
</s>
</p>
<p>
<s>
NaOH	NaOH	k?	NaOH
+	+	kIx~	+
H2SO4	H2SO4	k1gMnSc1	H2SO4
→	→	k?	→
NaHSO	NaHSO	k1gMnSc1	NaHSO
<g/>
4	[number]	k4	4
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
Oa	Oa	k1gFnSc2	Oa
po	po	k7c6	po
dalším	další	k2eAgNnSc6d1	další
přidání	přidání	k1gNnSc6	přidání
hydroxidu	hydroxid	k1gInSc2	hydroxid
síran	síran	k1gInSc1	síran
sodný	sodný	k2eAgInSc1d1	sodný
</s>
</p>
<p>
<s>
NaOH	NaOH	k?	NaOH
+	+	kIx~	+
NaHSO	NaHSO	k1gFnSc1	NaHSO
<g/>
4	[number]	k4	4
→	→	k?	→
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
OPodobně	OPodobně	k1gMnPc2	OPodobně
reaguje	reagovat	k5eAaBmIp3nS	reagovat
i	i	k9	i
s	s	k7c7	s
organickými	organický	k2eAgFnPc7d1	organická
(	(	kIx(	(
<g/>
karboxylovými	karboxylový	k2eAgFnPc7d1	karboxylová
<g/>
)	)	kIx)	)
kyselinami	kyselina	k1gFnPc7	kyselina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
octovou	octový	k2eAgFnSc7d1	octová
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
octan	octan	k1gInSc1	octan
sodný	sodný	k2eAgInSc1d1	sodný
</s>
</p>
<p>
<s>
NaOH	NaOH	k?	NaOH
+	+	kIx~	+
CH3COOH	CH3COOH	k1gFnSc1	CH3COOH
→	→	k?	→
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COONa	COON	k1gInSc2	COON
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
(	(	kIx(	(
<g/>
tuto	tento	k3xDgFnSc4	tento
neutralizační	neutralizační	k2eAgFnSc4d1	neutralizační
reakci	reakce	k1gFnSc4	reakce
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
jako	jako	k9	jako
nouzovou	nouzový	k2eAgFnSc4d1	nouzová
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
,	,	kIx,	,
zasažené	zasažený	k2eAgFnSc2d1	zasažená
roztokem	roztok	k1gInSc7	roztok
NaOH	NaOH	k1gMnPc2	NaOH
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
tvoří	tvořit	k5eAaImIp3nS	tvořit
soli	sůl	k1gFnPc4	sůl
i	i	k9	i
s	s	k7c7	s
tak	tak	k6eAd1	tak
slabými	slabý	k2eAgFnPc7d1	slabá
organickými	organický	k2eAgFnPc7d1	organická
kyselinami	kyselina	k1gFnPc7	kyselina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
fenoly	fenol	k1gInPc1	fenol
<g/>
,	,	kIx,	,
např.	např.	kA	např.
s	s	k7c7	s
fenolem	fenol	k1gInSc7	fenol
dává	dávat	k5eAaImIp3nS	dávat
fenolát	fenolát	k1gInSc1	fenolát
sodný	sodný	k2eAgInSc1d1	sodný
</s>
</p>
<p>
<s>
NaOH	NaOH	k?	NaOH
+	+	kIx~	+
C6H5-OH	C6H5-OH	k1gFnSc1	C6H5-OH
→	→	k?	→
C	C	kA	C
<g/>
6	[number]	k4	6
<g/>
H	H	kA	H
<g/>
5	[number]	k4	5
<g/>
-ONa	-ON	k1gInSc2	-ON
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
<g/>
Železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
ušlechtilých	ušlechtilý	k2eAgInPc2d1	ušlechtilý
kovů	kov	k1gInPc2	kov
hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
nerozpouští	rozpouštět	k5eNaImIp3nS	rozpouštět
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
s	s	k7c7	s
amfoterními	amfoterní	k2eAgInPc7d1	amfoterní
kovy	kov	k1gInPc7	kov
reaguje	reagovat	k5eAaBmIp3nS	reagovat
za	za	k7c2	za
vývinu	vývin	k1gInSc2	vývin
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
tvorby	tvorba	k1gFnSc2	tvorba
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
kovový	kovový	k2eAgInSc1d1	kovový
hliník	hliník	k1gInSc1	hliník
se	se	k3xPyFc4	se
v	v	k7c6	v
koncentrovaném	koncentrovaný	k2eAgInSc6d1	koncentrovaný
roztoku	roztok	k1gInSc6	roztok
NaOH	NaOH	k1gFnSc2	NaOH
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
hlinitanu	hlinitan	k1gInSc2	hlinitan
sodného	sodný	k2eAgInSc2d1	sodný
</s>
</p>
<p>
<s>
2	[number]	k4	2
Al	ala	k1gFnPc2	ala
+	+	kIx~	+
6	[number]	k4	6
NaOH	NaOH	k1gFnSc2	NaOH
→	→	k?	→
3	[number]	k4	3
H2	H2	k1gFnPc2	H2
+	+	kIx~	+
2	[number]	k4	2
Na	na	k7c4	na
<g/>
3	[number]	k4	3
<g/>
AlO	ala	k1gFnSc5	ala
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
Podobně	podobně	k6eAd1	podobně
reaguje	reagovat	k5eAaBmIp3nS	reagovat
se	s	k7c7	s
zinkem	zinek	k1gInSc7	zinek
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
zinečnatanu	zinečnatan	k1gInSc2	zinečnatan
sodného	sodný	k2eAgInSc2d1	sodný
</s>
</p>
<p>
<s>
Zn	zn	kA	zn
+	+	kIx~	+
2	[number]	k4	2
NaOH	NaOH	k1gMnSc1	NaOH
→	→	k?	→
H2	H2	k1gMnSc1	H2
+	+	kIx~	+
Na	na	k7c6	na
<g/>
2	[number]	k4	2
<g/>
ZnO	ZnO	k1gFnSc6	ZnO
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Obě	dva	k4xCgFnPc1	dva
uvedené	uvedený	k2eAgFnPc1d1	uvedená
soli	sůl	k1gFnPc1	sůl
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
i	i	k9	i
působením	působení	k1gNnSc7	působení
nadbytku	nadbytek	k1gInSc2	nadbytek
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
soli	sůl	k1gFnSc6	sůl
těchto	tento	k3xDgInPc2	tento
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
např.	např.	kA	např.
</s>
</p>
<p>
<s>
ZnCl	ZnCl	k1gInSc1	ZnCl
<g/>
2	[number]	k4	2
+	+	kIx~	+
4	[number]	k4	4
NaOH	NaOH	k1gFnSc2	NaOH
→	→	k?	→
2	[number]	k4	2
NaCl	NaCl	k1gInSc1	NaCl
+	+	kIx~	+
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
ZnO	ZnO	k1gFnPc2	ZnO
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
reaguje	reagovat	k5eAaBmIp3nS	reagovat
i	i	k9	i
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
nekovy	nekov	k1gInPc7	nekov
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
s	s	k7c7	s
fosforem	fosfor	k1gInSc7	fosfor
dává	dávat	k5eAaImIp3nS	dávat
fosfornan	fosfornan	k1gInSc1	fosfornan
sodný	sodný	k2eAgInSc1d1	sodný
</s>
</p>
<p>
<s>
2	[number]	k4	2
P	P	kA	P
+	+	kIx~	+
2	[number]	k4	2
NaOH	NaOH	k1gFnPc2	NaOH
+	+	kIx~	+
2	[number]	k4	2
H2O	H2O	k1gFnSc2	H2O
→	→	k?	→
2	[number]	k4	2
NaH	naho	k1gNnPc2	naho
<g/>
2	[number]	k4	2
<g/>
PO	Po	kA	Po
<g/>
2	[number]	k4	2
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
<g/>
s	s	k7c7	s
křemíkem	křemík	k1gInSc7	křemík
ortokřemičitan	ortokřemičitan	k1gInSc1	ortokřemičitan
sodný	sodný	k2eAgInSc1d1	sodný
</s>
</p>
<p>
<s>
Si	se	k3xPyFc3	se
+	+	kIx~	+
4	[number]	k4	4
NaOH	NaOH	k1gFnSc2	NaOH
→	→	k?	→
Na	na	k7c6	na
<g/>
4	[number]	k4	4
<g/>
SiO	SiO	k1gFnSc6	SiO
<g/>
4	[number]	k4	4
+	+	kIx~	+
2	[number]	k4	2
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Průmyslově	průmyslově	k6eAd1	průmyslově
důležitou	důležitý	k2eAgFnSc7d1	důležitá
reakcí	reakce	k1gFnSc7	reakce
je	být	k5eAaImIp3nS	být
působení	působení	k1gNnSc1	působení
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
na	na	k7c4	na
estery	ester	k1gInPc4	ester
organických	organický	k2eAgFnPc2d1	organická
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
štěpí	štěpit	k5eAaImIp3nP	štěpit
na	na	k7c4	na
alkoholy	alkohol	k1gInPc4	alkohol
a	a	k8xC	a
volné	volný	k2eAgFnPc4d1	volná
kyseliny	kyselina	k1gFnPc4	kyselina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
jejich	jejich	k3xOp3gFnPc4	jejich
sodné	sodný	k2eAgFnPc4d1	sodná
soli	sůl	k1gFnPc4	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
zmýdelňování	zmýdelňování	k1gNnSc1	zmýdelňování
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
působením	působení	k1gNnPc3	působení
vodného	vodný	k2eAgInSc2d1	vodný
roztoku	roztok	k1gInSc2	roztok
NaOH	NaOH	k1gFnSc2	NaOH
na	na	k7c4	na
ethylacetát	ethylacetát	k1gInSc4	ethylacetát
získáme	získat	k5eAaPmIp1nP	získat
ethanol	ethanol	k1gInSc4	ethanol
a	a	k8xC	a
octan	octan	k1gInSc4	octan
sodný	sodný	k2eAgInSc4d1	sodný
</s>
</p>
<p>
<s>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
CO	co	k8xS	co
<g/>
–	–	k?	–
<g/>
O	o	k7c4	o
<g/>
–	–	k?	–
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
+	+	kIx~	+
NaOH	NaOH	k1gFnSc1	NaOH
→	→	k?	→
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COONa	COON	k1gInSc2	COON
+	+	kIx~	+
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
OH	OH	kA	OH
<g/>
.	.	kIx.	.
<g/>
Zmýdelňování	zmýdelňování	k1gNnSc1	zmýdelňování
esterů	ester	k1gInPc2	ester
vyšších	vysoký	k2eAgFnPc2d2	vyšší
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
neboli	neboli	k8xC	neboli
tuků	tuk	k1gInPc2	tuk
a	a	k8xC	a
olejů	olej	k1gInPc2	olej
je	být	k5eAaImIp3nS	být
chemickou	chemický	k2eAgFnSc7d1	chemická
podstatou	podstata	k1gFnSc7	podstata
výroby	výroba	k1gFnSc2	výroba
mýdla	mýdlo	k1gNnSc2	mýdlo
a	a	k8xC	a
glycerolu	glycerol	k1gInSc2	glycerol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Působením	působení	k1gNnSc7	působení
na	na	k7c4	na
soli	sůl	k1gFnPc4	sůl
jiných	jiný	k2eAgInPc2d1	jiný
kovů	kov	k1gInPc2	kov
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
výměně	výměna	k1gFnSc3	výměna
iontů	ion	k1gInPc2	ion
těchto	tento	k3xDgInPc2	tento
kovů	kov	k1gInPc2	kov
za	za	k7c4	za
sodík	sodík	k1gInSc4	sodík
a	a	k8xC	a
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
hydroxidů	hydroxid	k1gInPc2	hydroxid
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
působením	působení	k1gNnSc7	působení
NaOH	NaOH	k1gFnSc7	NaOH
na	na	k7c4	na
síran	síran	k1gInSc4	síran
hlinitý	hlinitý	k2eAgInSc4d1	hlinitý
získáme	získat	k5eAaPmIp1nP	získat
hydroxid	hydroxid	k1gInSc4	hydroxid
hlinitý	hlinitý	k2eAgInSc4d1	hlinitý
</s>
</p>
<p>
<s>
6	[number]	k4	6
NaOH	NaOH	k1gFnPc2	NaOH
+	+	kIx~	+
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
→	→	k?	→
3	[number]	k4	3
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
+	+	kIx~	+
2	[number]	k4	2
Al	ala	k1gFnPc2	ala
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
<g/>
používaný	používaný	k2eAgInSc1d1	používaný
ve	v	k7c6	v
vodárenství	vodárenství	k1gNnSc6	vodárenství
k	k	k7c3	k
čeření	čeření	k1gNnSc3	čeření
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
má	mít	k5eAaImIp3nS	mít
velice	velice	k6eAd1	velice
široké	široký	k2eAgNnSc1d1	široké
použití	použití	k1gNnSc1	použití
v	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
mýdel	mýdlo	k1gNnPc2	mýdlo
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
povrchově	povrchově	k6eAd1	povrchově
aktivních	aktivní	k2eAgFnPc2d1	aktivní
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
příprava	příprava	k1gFnSc1	příprava
dalších	další	k2eAgFnPc2d1	další
sloučenin	sloučenina	k1gFnPc2	sloučenina
sodíku	sodík	k1gInSc2	sodík
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
reakční	reakční	k2eAgFnSc1d1	reakční
složka	složka	k1gFnSc1	složka
při	při	k7c6	při
organických	organický	k2eAgFnPc6d1	organická
a	a	k8xC	a
anorganických	anorganický	k2eAgFnPc6d1	anorganická
syntézách	syntéza	k1gFnPc6	syntéza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
textilním	textilní	k2eAgInSc6d1	textilní
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
celulózy	celulóza	k1gFnSc2	celulóza
a	a	k8xC	a
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
v	v	k7c6	v
hutnictví	hutnictví	k1gNnSc6	hutnictví
a	a	k8xC	a
hliníkárenství	hliníkárenství	k1gNnSc6	hliníkárenství
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodárenství	vodárenství	k1gNnSc6	vodárenství
při	při	k7c6	při
úpravách	úprava	k1gFnPc6	úprava
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
potravinářském	potravinářský	k2eAgInSc6d1	potravinářský
průmyslu	průmysl	k1gInSc6	průmysl
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
tuků	tuk	k1gInPc2	tuk
a	a	k8xC	a
olejů	olej	k1gInPc2	olej
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jako	jako	k9	jako
desinfekční	desinfekční	k2eAgNnSc4d1	desinfekční
činidlo	činidlo	k1gNnSc4	činidlo
pro	pro	k7c4	pro
vymývání	vymývání	k1gNnSc4	vymývání
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
užít	užít	k5eAaPmF	užít
při	při	k7c6	při
čištění	čištění	k1gNnSc6	čištění
odpadních	odpadní	k2eAgNnPc2d1	odpadní
potrubí	potrubí	k1gNnPc2	potrubí
a	a	k8xC	a
při	při	k7c6	při
praní	praní	k1gNnSc6	praní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
chemických	chemický	k2eAgFnPc6d1	chemická
laboratořích	laboratoř	k1gFnPc6	laboratoř
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
kalibrovaný	kalibrovaný	k2eAgInSc1d1	kalibrovaný
roztok	roztok	k1gInSc1	roztok
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgMnSc2d1	sodný
jako	jako	k8xS	jako
titrační	titrační	k2eAgNnSc1d1	titrační
činidlo	činidlo	k1gNnSc1	činidlo
při	při	k7c6	při
kvantitativním	kvantitativní	k2eAgNnSc6d1	kvantitativní
stanovování	stanovování	k1gNnSc6	stanovování
obsahu	obsah	k1gInSc3	obsah
kyselin	kyselina	k1gFnPc2	kyselina
ve	v	k7c6	v
vzorcích	vzorec	k1gInPc6	vzorec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fyziologické	fyziologický	k2eAgNnSc4d1	fyziologické
působení	působení	k1gNnSc4	působení
==	==	k?	==
</s>
</p>
<p>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
silně	silně	k6eAd1	silně
leptá	leptat	k5eAaImIp3nS	leptat
veškeré	veškerý	k3xTgFnPc4	veškerý
tkáně	tkáň	k1gFnPc4	tkáň
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Zmýdelňuje	zmýdelňovat	k5eAaImIp3nS	zmýdelňovat
tuky	tuk	k1gInPc4	tuk
<g/>
,	,	kIx,	,
koaguluje	koagulovat	k5eAaBmIp3nS	koagulovat
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
,	,	kIx,	,
odnímá	odnímat	k5eAaImIp3nS	odnímat
z	z	k7c2	z
tkání	tkáň	k1gFnPc2	tkáň
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
požití	požití	k1gNnSc6	požití
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zejména	zejména	k9	zejména
poleptání	poleptání	k1gNnSc1	poleptání
jícnu	jícen	k1gInSc2	jícen
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
žaludku	žaludek	k1gInSc2	žaludek
jako	jako	k8xC	jako
u	u	k7c2	u
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
;	;	kIx,	;
při	při	k7c6	při
větších	veliký	k2eAgNnPc6d2	veliký
požitých	požitý	k2eAgNnPc6d1	požité
množstvích	množství	k1gNnPc6	množství
však	však	k8xC	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vážně	vážně	k6eAd1	vážně
poleptán	poleptat	k5eAaPmNgInS	poleptat
i	i	k9	i
žaludek	žaludek	k1gInSc1	žaludek
a	a	k8xC	a
tenké	tenký	k2eAgNnSc1d1	tenké
střevo	střevo	k1gNnSc1	střevo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
rizikem	riziko	k1gNnSc7	riziko
nekrózy	nekróza	k1gFnSc2	nekróza
<g/>
,	,	kIx,	,
žilní	žilní	k2eAgFnSc2d1	žilní
trombózy	trombóza	k1gFnSc2	trombóza
nebo	nebo	k8xC	nebo
až	až	k9	až
perforace	perforace	k1gFnPc1	perforace
jícnu	jícen	k1gInSc2	jícen
<g/>
.	.	kIx.	.
</s>
<s>
Častá	častý	k2eAgFnSc1d1	častá
jsou	být	k5eAaImIp3nP	být
pozdější	pozdní	k2eAgNnSc4d2	pozdější
zúžení	zúžení	k1gNnSc4	zúžení
jícnu	jícen	k1gInSc2	jícen
<g/>
,	,	kIx,	,
komplikující	komplikující	k2eAgNnSc1d1	komplikující
polykání	polykání	k1gNnSc1	polykání
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
i	i	k9	i
pozdní	pozdní	k2eAgInPc1d1	pozdní
následky	následek	k1gInPc1	následek
(	(	kIx(	(
<g/>
po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
let	léto	k1gNnPc2	léto
od	od	k7c2	od
poleptání	poleptání	k1gNnSc2	poleptání
<g/>
)	)	kIx)	)
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
rakoviny	rakovina	k1gFnSc2	rakovina
jícnu	jícen	k1gInSc2	jícen
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
0,8	[number]	k4	0,8
až	až	k9	až
4	[number]	k4	4
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
vážné	vážný	k2eAgInPc4d1	vážný
následky	následek	k1gInPc4	následek
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
při	při	k7c6	při
zasažení	zasažení	k1gNnSc6	zasažení
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
proniká	pronikat	k5eAaImIp3nS	pronikat
do	do	k7c2	do
rohovky	rohovka	k1gFnSc2	rohovka
a	a	k8xC	a
přední	přední	k2eAgFnSc2d1	přední
oční	oční	k2eAgFnSc2d1	oční
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
i	i	k9	i
přes	přes	k7c4	přes
zdánlivě	zdánlivě	k6eAd1	zdánlivě
malé	malý	k2eAgInPc4d1	malý
časné	časný	k2eAgInPc4d1	časný
příznaky	příznak	k1gInPc4	příznak
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
slepotu	slepota	k1gFnSc4	slepota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
==	==	k?	==
</s>
</p>
<p>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
není	být	k5eNaImIp3nS	být
sice	sice	k8xC	sice
hořlavý	hořlavý	k2eAgInSc1d1	hořlavý
ani	ani	k8xC	ani
výbušný	výbušný	k2eAgInSc1d1	výbušný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgFnSc1d1	silná
žíravina	žíravina	k1gFnSc1	žíravina
a	a	k8xC	a
zdraví	zdravit	k5eAaImIp3nS	zdravit
škodlivá	škodlivý	k2eAgFnSc1d1	škodlivá
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
poleptání	poleptání	k1gNnSc6	poleptání
okamžitě	okamžitě	k6eAd1	okamžitě
omývejte	omývat	k5eAaImRp2nP	omývat
napadené	napadený	k2eAgNnSc4d1	napadené
místo	místo	k1gNnSc4	místo
pokožky	pokožka	k1gFnSc2	pokožka
proudem	proud	k1gInSc7	proud
studené	studený	k2eAgFnSc2d1	studená
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
následně	následně	k6eAd1	následně
neutralizujte	neutralizovat	k5eAaBmRp2nP	neutralizovat
poleptané	poleptaný	k2eAgNnSc4d1	poleptané
místo	místo	k1gNnSc4	místo
slabou	slabý	k2eAgFnSc7d1	slabá
kyselinou	kyselina	k1gFnSc7	kyselina
(	(	kIx(	(
<g/>
zředěný	zředěný	k2eAgInSc1d1	zředěný
ocet	ocet	k1gInSc1	ocet
nebo	nebo	k8xC	nebo
kyselina	kyselina	k1gFnSc1	kyselina
citronová	citronový	k2eAgFnSc1d1	citronová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zasažení	zasažení	k1gNnSc6	zasažení
očí	oko	k1gNnPc2	oko
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
nikdy	nikdy	k6eAd1	nikdy
neutralizovat	neutralizovat	k5eAaBmF	neutralizovat
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
pouze	pouze	k6eAd1	pouze
neustále	neustále	k6eAd1	neustále
vymývat	vymývat	k5eAaImF	vymývat
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
poleptání	poleptání	k1gNnSc4	poleptání
většího	veliký	k2eAgInSc2d2	veliký
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
,	,	kIx,	,
zasažení	zasažení	k1gNnSc1	zasažení
očí	oko	k1gNnPc2	oko
či	či	k8xC	či
přetrvávající	přetrvávající	k2eAgFnPc4d1	přetrvávající
zdravotní	zdravotní	k2eAgFnPc4d1	zdravotní
potíže	potíž	k1gFnPc4	potíž
<g/>
,	,	kIx,	,
navštivte	navštívit	k5eAaPmRp2nP	navštívit
ihned	ihned	k6eAd1	ihned
lékaře	lékař	k1gMnPc4	lékař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
VOHLÍDAL	VOHLÍDAL	kA	VOHLÍDAL
<g/>
,	,	kIx,	,
JIŘÍ	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
ŠTULÍK	štulík	k1gInSc1	štulík
<g/>
,	,	kIx,	,
KAREL	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
JULÁK	JULÁK	kA	JULÁK
<g/>
,	,	kIx,	,
ALOIS	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
a	a	k8xC	a
analytické	analytický	k2eAgFnPc1d1	analytická
tabulky	tabulka	k1gFnPc1	tabulka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7169	[number]	k4	7169
<g/>
-	-	kIx~	-
<g/>
855	[number]	k4	855
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
