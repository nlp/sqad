<s>
Almanach	almanach	k1gInSc1	almanach
Ruch	ruch	k1gInSc1	ruch
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
poprvé	poprvé	k6eAd1	poprvé
vyšel	vyjít	k5eAaPmAgMnS	vyjít
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
položení	položení	k1gNnSc2	položení
základního	základní	k2eAgInSc2d1	základní
kamene	kámen	k1gInSc2	kámen
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nápadně	nápadně	k6eAd1	nápadně
odlišoval	odlišovat	k5eAaImAgInS	odlišovat
od	od	k7c2	od
almanachu	almanach	k1gInSc2	almanach
Máje	máj	k1gInSc2	máj
<g/>
.	.	kIx.	.
</s>
