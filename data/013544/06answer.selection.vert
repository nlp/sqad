<s>
Rosettská	rosettský	k2eAgFnSc1d1
deska	deska	k1gFnSc1
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
la	la	k1gNnSc1
pierre	pierr	k1gInSc5
de	de	k?
Rosette	Rosett	k1gMnSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
tradiční	tradiční	k2eAgInSc4d1
název	název	k1gInSc4
fragmentu	fragment	k1gInSc2
staroegyptské	staroegyptský	k2eAgFnPc4d1
kamenné	kamenný	k2eAgFnPc4d1
stély	stéla	k1gFnPc4
s	s	k7c7
třemi	tři	k4xCgFnPc7
variantami	varianta	k1gFnPc7
téhož	týž	k3xTgInSc2
nápisu	nápis	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
umožnily	umožnit	k5eAaPmAgInP
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
rozluštit	rozluštit	k5eAaPmF
egyptské	egyptský	k2eAgInPc4d1
hieroglyfy	hieroglyf	k1gInPc4
a	a	k8xC
položit	položit	k5eAaPmF
tak	tak	k6eAd1
základy	základ	k1gInPc4
moderní	moderní	k2eAgFnSc2d1
egyptologie	egyptologie	k1gFnSc2
<g/>
.	.	kIx.
</s>