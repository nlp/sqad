<s>
Rosettská	rosettský	k2eAgFnSc1d1
deska	deska	k1gFnSc1
</s>
<s>
Rosettská	rosettský	k2eAgFnSc1d1
deska	deska	k1gFnSc1
v	v	k7c6
Britském	britský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
</s>
<s>
Rosettská	rosettský	k2eAgFnSc1d1
deska	deska	k1gFnSc1
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
la	la	k1gNnSc1
pierre	pierr	k1gInSc5
de	de	k?
Rosette	Rosett	k1gMnSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
tradiční	tradiční	k2eAgInSc4d1
název	název	k1gInSc4
fragmentu	fragment	k1gInSc2
staroegyptské	staroegyptský	k2eAgFnPc4d1
kamenné	kamenný	k2eAgFnPc4d1
stély	stéla	k1gFnPc4
s	s	k7c7
třemi	tři	k4xCgFnPc7
variantami	varianta	k1gFnPc7
téhož	týž	k3xTgInSc2
nápisu	nápis	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
umožnily	umožnit	k5eAaPmAgInP
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
rozluštit	rozluštit	k5eAaPmF
egyptské	egyptský	k2eAgInPc4d1
hieroglyfy	hieroglyf	k1gInPc4
a	a	k8xC
položit	položit	k5eAaPmF
tak	tak	k6eAd1
základy	základ	k1gInPc4
moderní	moderní	k2eAgFnSc2d1
egyptologie	egyptologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Deska	deska	k1gFnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
podle	podle	k7c2
francouzského	francouzský	k2eAgInSc2d1
názvu	název	k1gInSc2
města	město	k1gNnSc2
Rosette	Rosett	k1gInSc5
<g/>
,	,	kIx,
dnešního	dnešní	k2eAgInSc2d1
Ar-Rašídu	Ar-Rašíd	k1gInSc2
při	při	k7c6
ústí	ústí	k1gNnSc6
Nilu	Nil	k1gInSc2
<g/>
,	,	kIx,
poblíž	poblíž	k7c2
kterého	který	k3yIgMnSc2,k3yQgMnSc2,k3yRgMnSc2
ji	on	k3xPp3gFnSc4
našel	najít	k5eAaPmAgInS
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1799	#num#	k4
Pierre-François-Xavier	Pierre-François-Xavier	k1gMnSc1
Bouchard	Bouchard	k1gMnSc1
<g/>
,	,	kIx,
důstojník	důstojník	k1gMnSc1
dělostřelectva	dělostřelectvo	k1gNnSc2
za	za	k7c2
Napoleonova	Napoleonův	k2eAgNnSc2d1
tažení	tažení	k1gNnSc2
do	do	k7c2
Egypta	Egypt	k1gInSc2
<g/>
,	,	kIx,
při	při	k7c6
opevňovacích	opevňovací	k2eAgFnPc6d1
pracích	práce	k1gFnPc6
na	na	k7c6
pevnosti	pevnost	k1gFnSc6
Fort	Fort	k?
Julien	Julina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Deska	deska	k1gFnSc1
z	z	k7c2
černé	černý	k2eAgFnSc2d1
žuly	žula	k1gFnSc2
o	o	k7c6
výšce	výška	k1gFnSc6
114	#num#	k4
cm	cm	kA
a	a	k8xC
šířce	šířka	k1gFnSc6
71	#num#	k4
cm	cm	kA
obsahuje	obsahovat	k5eAaImIp3nS
nápis	nápis	k1gInSc1
kněze	kněz	k1gMnSc2
děkujícího	děkující	k2eAgInSc2d1
roku	rok	k1gInSc2
196	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
králi	král	k1gMnSc3
Ptolemaiovi	Ptolemaios	k1gMnSc3
V.	V.	kA
Epifanovi	Epifan	k1gMnSc3
vytesaný	vytesaný	k2eAgInSc1d1
ve	v	k7c6
třech	tři	k4xCgFnPc6
shodných	shodný	k2eAgFnPc6d1
verzích	verze	k1gFnPc6
<g/>
:	:	kIx,
</s>
<s>
egyptské	egyptský	k2eAgInPc1d1
hieroglyfy	hieroglyf	k1gInPc1
–	–	k?
starodávné	starodávný	k2eAgNnSc4d1
a	a	k8xC
náročné	náročný	k2eAgNnSc4d1
obrázkové	obrázkový	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
</s>
<s>
démotické	démotický	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
–	–	k?
pozdní	pozdní	k2eAgMnSc1d1
zjednodušené	zjednodušený	k2eAgNnSc4d1
<g/>
,	,	kIx,
„	„	k?
<g/>
lidové	lidový	k2eAgFnSc2d1
<g/>
“	“	k?
egyptské	egyptský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
řecká	řecký	k2eAgFnSc1d1
alfabeta	alfabeta	k1gFnSc1
–	–	k?
překlad	překlad	k1gInSc4
textu	text	k1gInSc2
do	do	k7c2
starořečtiny	starořečtina	k1gFnSc2
</s>
<s>
Jde	jít	k5eAaImIp3nS
vlastně	vlastně	k9
o	o	k7c4
„	„	k?
<g/>
kopii	kopie	k1gFnSc4
<g/>
“	“	k?
dekretu	dekret	k1gInSc2
<g/>
,	,	kIx,
kterým	který	k3yRgMnPc3,k3yQgMnPc3,k3yIgMnPc3
synoda	synoda	k1gFnSc1
zahrnula	zahrnout	k5eAaPmAgFnS
panovníka	panovník	k1gMnSc4
poctami	pocta	k1gFnPc7
v	v	k7c6
devátém	devátý	k4xOgInSc6
roce	rok	k1gInSc6
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
panování	panování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
ptolemaiovského	ptolemaiovský	k2eAgInSc2d1
Egypta	Egypt	k1gInSc2
byla	být	k5eAaImAgFnS
řečtina	řečtina	k1gFnSc1
úředním	úřední	k2eAgMnSc7d1
jazykem	jazyk	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Deska	deska	k1gFnSc1
pomohla	pomoct	k5eAaPmAgFnS
k	k	k7c3
rozluštění	rozluštění	k1gNnSc3
démotického	démotický	k2eAgNnSc2d1
písma	písmo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yQgNnSc4,k3yIgNnSc4
dokončil	dokončit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1814	#num#	k4
britský	britský	k2eAgMnSc1d1
polyhistor	polyhistor	k1gMnSc1
T.	T.	kA
Young	Young	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1822	#num#	k4
pomocí	pomocí	k7c2
kopie	kopie	k1gFnSc2
desky	deska	k1gFnSc2
rozluštil	rozluštit	k5eAaPmAgInS
Jean-François	Jean-François	k1gInSc1
Champollion	Champollion	k1gInSc4
hieroglyfické	hieroglyfický	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
<g/>
,	,	kIx,
přestože	přestože	k8xS
deska	deska	k1gFnSc1
je	být	k5eAaImIp3nS
právě	právě	k9
v	v	k7c6
této	tento	k3xDgFnSc6
části	část	k1gFnSc6
nejvíc	hodně	k6eAd3,k6eAd1
poškozená	poškozený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozluštění	rozluštění	k1gNnSc1
hieroglyfů	hieroglyf	k1gInPc2
zpřístupnilo	zpřístupnit	k5eAaPmAgNnS
písemné	písemný	k2eAgInPc4d1
prameny	pramen	k1gInPc4
z	z	k7c2
období	období	k1gNnSc2
více	hodně	k6eAd2
než	než	k8xS
tří	tři	k4xCgNnPc2
tisíciletí	tisíciletí	k1gNnPc2
egyptské	egyptský	k2eAgFnSc2d1
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Deska	deska	k1gFnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1802	#num#	k4
uložená	uložený	k2eAgFnSc1d1
v	v	k7c6
Britském	britský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
,	,	kIx,
protože	protože	k8xS
po	po	k7c6
porážce	porážka	k1gFnSc6
fr.	fr.	k?
expedice	expedice	k1gFnSc2
do	do	k7c2
Egypta	Egypt	k1gInSc2
r.	r.	kA
1801	#num#	k4
ji	on	k3xPp3gFnSc4
Angličané	Angličan	k1gMnPc1
ukořistili	ukořistit	k5eAaPmAgMnP
s	s	k7c7
francouzskou	francouzský	k2eAgFnSc7d1
kapitulací	kapitulace	k1gFnSc7
zbytku	zbytek	k1gInSc2
expedičního	expediční	k2eAgInSc2d1
sboru	sbor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Egypťané	Egypťan	k1gMnPc1
desku	deska	k1gFnSc4
považují	považovat	k5eAaImIp3nP
za	za	k7c4
součást	součást	k1gFnSc4
národního	národní	k2eAgNnSc2d1
bohatství	bohatství	k1gNnSc2
a	a	k8xC
chtěli	chtít	k5eAaImAgMnP
by	by	kYmCp3nS
ji	on	k3xPp3gFnSc4
získat	získat	k5eAaPmF
zpět	zpět	k6eAd1
<g/>
,	,	kIx,
mezinárodní	mezinárodní	k2eAgNnSc4d1
právo	právo	k1gNnSc4
již	již	k6eAd1
však	však	k9
tehdejší	tehdejší	k2eAgInPc4d1
nálezy	nález	k1gInPc4
klasifikuje	klasifikovat	k5eAaImIp3nS
jako	jako	k9
vlastnictví	vlastnictví	k1gNnSc1
současných	současný	k2eAgMnPc2d1
majitelů	majitel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Démotické	démotický	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
na	na	k7c6
desce	deska	k1gFnSc6
</s>
<s>
Český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
nápisu	nápis	k1gInSc2
na	na	k7c6
Rosettské	rosettský	k2eAgFnSc6d1
desce	deska	k1gFnSc6
</s>
<s>
Překlad	překlad	k1gInSc1
anglického	anglický	k2eAgInSc2d1
překladu	překlad	k1gInSc2
od	od	k7c2
Edwyna	Edwyn	k1gInSc2
R.	R.	kA
Bevana	Bevan	k1gMnSc2
publikovaného	publikovaný	k2eAgMnSc2d1
1927	#num#	k4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c2
vlády	vláda	k1gFnSc2
mladého	mladý	k2eAgMnSc4d1
–	–	k?
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
získal	získat	k5eAaPmAgMnS
království	království	k1gNnSc4
od	od	k7c2
svého	své	k1gNnSc2
otce	otec	k1gMnSc2
–	–	k?
pána	pán	k1gMnSc2
korun	koruna	k1gFnPc2
<g/>
,	,	kIx,
slavného	slavný	k2eAgInSc2d1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
založil	založit	k5eAaPmAgInS
Egypt	Egypt	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
zbožný	zbožný	k2eAgInSc1d1
k	k	k7c3
Bohům	bůh	k1gMnPc3
<g/>
,	,	kIx,
nadřazený	nadřazený	k2eAgMnSc1d1
svým	svůj	k3xOyFgMnPc3
nepřátelům	nepřítel	k1gMnPc3
<g/>
,	,	kIx,
a	a	k8xC
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
obnovil	obnovit	k5eAaPmAgMnS
civilizovaný	civilizovaný	k2eAgInSc4d1
život	život	k1gInSc4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
pána	pán	k1gMnSc2
Třicetiletého	třicetiletý	k2eAgInSc2d1
Svátku	svátek	k1gInSc2
<g/>
,	,	kIx,
dokonce	dokonce	k9
jako	jako	k9
Hephaista	Hephaista	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
velkého	velký	k2eAgMnSc4d1
<g/>
;	;	kIx,
krále	král	k1gMnSc4
podobného	podobný	k2eAgMnSc4d1
Slunci	slunce	k1gNnSc3
<g/>
,	,	kIx,
velkého	velký	k2eAgMnSc4d1
krále	král	k1gMnSc4
horní	horní	k2eAgFnSc2d1
a	a	k8xC
dolní	dolní	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
,	,	kIx,
potomka	potomek	k1gMnSc4
Bohů	bůh	k1gMnPc2
Philopatores	Philopatoresa	k1gFnPc2
<g/>
,	,	kIx,
jediného	jediný	k2eAgInSc2d1
koho	kdo	k3yInSc4,k3yQnSc4,k3yRnSc4
Hephaistos	Hephaistos	k1gInSc1
schválil	schválit	k5eAaPmAgInS
<g/>
,	,	kIx,
kterému	který	k3yQgMnSc3,k3yIgMnSc3,k3yRgMnSc3
Slunce	slunce	k1gNnSc1
věnovalo	věnovat	k5eAaImAgNnS,k5eAaPmAgNnS
vítězství	vítězství	k1gNnSc4
<g/>
,	,	kIx,
živoucí	živoucí	k2eAgInSc4d1
obraz	obraz	k1gInSc4
Dia	Dia	k1gMnSc2
<g/>
,	,	kIx,
syna	syn	k1gMnSc2
Slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
Ptolemaia	Ptolemaios	k1gMnSc2
<g/>
,	,	kIx,
na	na	k7c4
věky	věk	k1gInPc4
milovaného	milovaný	k1gMnSc2
Ptah	Ptah	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
devátém	devátý	k4xOgInSc6
roce	rok	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Aëtus	Aëtus	k1gMnSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
Aëtuse	Aëtuse	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
kněz	kněz	k1gMnSc1
Alexandra	Alexandr	k1gMnSc2
…	…	k?
<g/>
;	;	kIx,
</s>
<s>
Velekněží	velekněz	k1gMnPc1
a	a	k8xC
proroci	prorok	k1gMnPc1
<g/>
,	,	kIx,
a	a	k8xC
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
vstupují	vstupovat	k5eAaImIp3nP
do	do	k7c2
vnitřní	vnitřní	k2eAgFnSc2d1
svatyně	svatyně	k1gFnSc2
a	a	k8xC
šatny	šatna	k1gFnSc2
Bohů	bůh	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
Držitelé	držitel	k1gMnPc1
Per	pero	k1gNnPc2
a	a	k8xC
Svatých	svatý	k2eAgInPc2d1
Textů	text	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
všichni	všechen	k3xTgMnPc1
ostatní	ostatní	k2eAgMnPc1d1
kněží	kněz	k1gMnPc1
…	…	k?
shromáždění	shromáždění	k1gNnSc2
v	v	k7c6
chrámu	chrám	k1gInSc6
v	v	k7c6
Memphis	Memphis	k1gFnSc6
v	v	k7c4
tento	tento	k3xDgInSc4
den	den	k1gInSc4
<g/>
,	,	kIx,
prohlásili	prohlásit	k5eAaPmAgMnP
<g/>
:	:	kIx,
</s>
<s>
Od	od	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
král	král	k1gMnSc1
Ptolemaios	Ptolemaios	k1gMnSc1
<g/>
,	,	kIx,
nesmrtelný	nesmrtelný	k1gMnSc1
<g/>
,	,	kIx,
milovaný	milovaný	k1gMnSc1
Ptah	Ptah	k1gMnSc1
<g/>
,	,	kIx,
Bůh	bůh	k1gMnSc1
Epiphanes	Epiphanes	k1gMnSc1
Eucharistos	Eucharistos	k1gMnSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
krále	král	k1gMnSc2
Ptolemaia	Ptolemaios	k1gMnSc2
a	a	k8xC
královny	královna	k1gFnSc2
Arsinoe	Arsinoe	k1gFnPc2
<g/>
,	,	kIx,
Bohů	bůh	k1gMnPc2
Philopatores	Philopatoresa	k1gFnPc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
hodně	hodně	k6eAd1
výhod	výhod	k1gInSc4
z	z	k7c2
obou	dva	k4xCgInPc2
chrámů	chrám	k1gInPc2
a	a	k8xC
od	od	k7c2
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
v	v	k7c6
nich	on	k3xPp3gFnPc6
sídlí	sídlet	k5eAaImIp3nS
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
od	od	k7c2
<g />
.	.	kIx.
</s>
<s hack="1">
všech	všecek	k3xTgMnPc2
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
jsou	být	k5eAaImIp3nP
jim	on	k3xPp3gMnPc3
podřízeni	podřízen	k2eAgMnPc1d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
Boží	boží	k2eAgInSc1d1
dar	dar	k1gInSc1
od	od	k7c2
Boha	bůh	k1gMnSc2
a	a	k8xC
Bohyně	bohyně	k1gFnSc2
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
Horus	Horus	k1gMnSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
Isis	Isis	k1gFnSc1
a	a	k8xC
Osiris	Osiris	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
pomstil	pomstít	k5eAaPmAgMnS,k5eAaImAgMnS
svého	svůj	k1gMnSc4
otce	otec	k1gMnSc4
Osiris	Osiris	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
jimž	jenž	k3xRgMnPc3
jsou	být	k5eAaImIp3nP
Bohové	bůh	k1gMnPc1
shovívavě	shovívavě	k6eAd1
nakloněni	naklonit	k5eAaPmNgMnP
<g/>
,	,	kIx,
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
chrámům	chrám	k1gInPc3
příjmy	příjem	k1gInPc1
v	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
penězích	peníze	k1gInPc6
a	a	k8xC
obilí	obilí	k1gNnSc6
<g/>
,	,	kIx,
a	a	k8xC
činí	činit	k5eAaImIp3nS
mnoho	mnoho	k4c1
výdajů	výdaj	k1gInPc2
pro	pro	k7c4
přinesení	přinesení	k1gNnSc4
prosperity	prosperita	k1gFnSc2
do	do	k7c2
Egypta	Egypt	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
pro	pro	k7c4
založení	založení	k1gNnSc4
chrámů	chrám	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
po	po	k7c6
všech	všecek	k3xTgFnPc6
stranách	strana	k1gFnPc6
velkorysý	velkorysý	k2eAgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
z	z	k7c2
příjmů	příjem	k1gInPc2
a	a	k8xC
daní	daň	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
dostává	dostávat	k5eAaImIp3nS
z	z	k7c2
Egypta	Egypt	k1gInSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
promíjí	promíjet	k5eAaImIp3nS
a	a	k8xC
část	část	k1gFnSc1
ulevuje	ulevovat	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
lidé	člověk	k1gMnPc1
a	a	k8xC
všichni	všechen	k3xTgMnPc1
ostatní	ostatní	k2eAgMnPc1d1
mohli	moct	k5eAaImAgMnP
být	být	k5eAaImF
za	za	k7c2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
v	v	k7c6
prosperitě	prosperita	k1gFnSc6
…	…	k?
<g/>
;	;	kIx,
</s>
<s>
Budiž	budiž	k9
ke	k	k7c3
cti	čest	k1gFnSc3
kněžím	kněz	k1gMnPc3
všech	všecek	k3xTgInPc2
chrámů	chrám	k1gInPc2
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
výrazně	výrazně	k6eAd1
zvyšují	zvyšovat	k5eAaImIp3nP
stávající	stávající	k2eAgFnSc4d1
slávu	sláva	k1gFnSc4
krále	král	k1gMnSc2
Ptolemaia	Ptolemaios	k1gMnSc2
<g/>
,	,	kIx,
nesmrtelného	nesmrtelný	k1gMnSc2
<g/>
,	,	kIx,
milovaného	milovaný	k1gMnSc2
Ptah	Ptaha	k1gFnPc2
…	…	k?
a	a	k8xC
svátek	svátek	k1gInSc1
budiž	budiž	k9
slaven	slaven	k2eAgMnSc1d1
pro	pro	k7c4
krále	král	k1gMnSc4
Ptolemaia	Ptolemaios	k1gMnSc4
<g/>
,	,	kIx,
nesmrtelného	nesmrtelný	k1gMnSc4
<g/>
,	,	kIx,
milovaného	milovaný	k1gMnSc4
Ptah	Ptah	k1gInSc4
<g/>
,	,	kIx,
Boha	bůh	k1gMnSc2
Epiphana	Epiphana	k1gFnSc1
Eucharista	Eucharista	k1gMnSc1
<g/>
,	,	kIx,
každoročně	každoročně	k6eAd1
ve	v	k7c6
všech	všecek	k3xTgInPc6
chrámech	chrám	k1gInPc6
země	zem	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
od	od	k7c2
prvního	první	k4xOgInSc2
Thoth	Thotha	k1gFnPc2
po	po	k7c4
dobu	doba	k1gFnSc4
pěti	pět	k4xCc2
dnů	den	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
budou	být	k5eAaImBp3nP
nosit	nosit	k5eAaImF
věnce	věnec	k1gInPc4
a	a	k8xC
provádět	provádět	k5eAaImF
oběti	oběť	k1gFnPc4
a	a	k8xC
další	další	k2eAgFnPc4d1
obvyklé	obvyklý	k2eAgFnPc4d1
pocty	pocta	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
kněží	kněz	k1gMnPc1
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
nazýváni	nazývat	k5eAaImNgMnP
kněžími	kněz	k1gMnPc7
Boha	bůh	k1gMnSc2
Epiphana	Epiphan	k1gMnSc2
Eucharista	Eucharista	k1gMnSc1
<g/>
,	,	kIx,
kromě	kromě	k7c2
jmen	jméno	k1gNnPc2
ostatních	ostatní	k2eAgMnPc2d1
bohů	bůh	k1gMnPc2
kterým	který	k3yIgMnPc3,k3yRgMnPc3,k3yQgMnPc3
slouží	sloužit	k5eAaImIp3nP
<g/>
,	,	kIx,
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
kněžství	kněžství	k1gNnSc1
se	se	k3xPyFc4
zapisuje	zapisovat	k5eAaImIp3nS
na	na	k7c4
všechny	všechen	k3xTgFnPc4
formální	formální	k2eAgFnPc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
dokumenty	dokument	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
soukromým	soukromý	k2eAgFnPc3d1
osobám	osoba	k1gFnPc3
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
také	také	k9
umožněno	umožněn	k2eAgNnSc1d1
slaviti	slavit	k5eAaImF
slavnost	slavnost	k1gFnSc4
a	a	k8xC
zřizovat	zřizovat	k5eAaImF
svatyně	svatyně	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
jejich	jejich	k3xOp3gInPc6
domech	dům	k1gInPc6
<g/>
,	,	kIx,
vykonávat	vykonávat	k5eAaImF
obvyklé	obvyklý	k2eAgFnPc4d1
pocty	pocta	k1gFnPc4
během	během	k7c2
svátků	svátek	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
měsíčně	měsíčně	k6eAd1
a	a	k8xC
ročně	ročně	k6eAd1
<g/>
,	,	kIx,
s	s	k7c7
cílem	cíl	k1gInSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
známa	známo	k1gNnSc2
všem	všecek	k3xTgMnPc3
lidem	člověk	k1gMnPc3
z	z	k7c2
Egypta	Egypt	k1gInSc2
velikost	velikost	k1gFnSc1
a	a	k8xC
sláva	sláva	k1gFnSc1
Boha	bůh	k1gMnSc2
Epiphana	Epiphana	k1gFnSc1
Eucharista	Eucharista	k1gMnSc1
<g/>
,	,	kIx,
krále	král	k1gMnSc4
podle	podle	k7c2
zákona	zákon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Rosettská	rosettský	k2eAgFnSc1d1
doska	doska	k1gFnSc1
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
http://www.allaboutarchaeology.org/rosetta-stone-english-translation-faq.htm	http://www.allaboutarchaeology.org/rosetta-stone-english-translation-faq.htm	k6eAd1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Behistunský	Behistunský	k2eAgInSc1d1
nápis	nápis	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Rosettská	rosettský	k2eAgFnSc1d1
deska	deska	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Databáze	databáze	k1gFnPc1
Britského	britský	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
obrázky	obrázek	k1gInPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
|	|	kIx~
Starověký	starověký	k2eAgInSc1d1
Egypt	Egypt	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
685659	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4199489-9	4199489-9	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
84160110	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
177423312	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
84160110	#num#	k4
</s>
