<s>
Kodomo	Kodomo	k6eAd1	Kodomo
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
子	子	k?	子
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonský	japonský	k2eAgInSc1d1	japonský
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
i	i	k9	i
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
mang	mango	k1gNnPc2	mango
(	(	kIx(	(
<g/>
子	子	k?	子
<g/>
,	,	kIx,	,
kodomomuke	kodomomuke	k1gFnSc1	kodomomuke
manga	mango	k1gNnSc2	mango
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
anime	animat	k5eAaPmIp3nS	animat
(	(	kIx(	(
<g/>
子	子	k?	子
<g/>
,	,	kIx,	,
kodomomuke	kodomomuke	k6eAd1	kodomomuke
anime	animat	k5eAaPmIp3nS	animat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jsou	být	k5eAaImIp3nP	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
dětské	dětský	k2eAgNnSc4d1	dětské
publikum	publikum	k1gNnSc4	publikum
(	(	kIx(	(
<g/>
děti	dítě	k1gFnPc4	dítě
do	do	k7c2	do
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
veselá	veselý	k2eAgFnSc1d1	veselá
a	a	k8xC	a
poučná	poučný	k2eAgFnSc1d1	poučná
<g/>
.	.	kIx.	.
</s>
<s>
Astro	astra	k1gFnSc5	astra
Beyblade	Beyblad	k1gInSc5	Beyblad
BuBu	bubu	k1gNnSc2	bubu
ChaCha	ChaCha	k1gMnSc1	ChaCha
D.I.C.E.	D.I.C.E.	k1gMnSc1	D.I.C.E.
Digimon	Digimon	k1gMnSc1	Digimon
Doraemon	Doraemon	k1gMnSc1	Doraemon
Hamtaro	Hamtara	k1gFnSc5	Hamtara
Nils	Nils	k1gInSc4	Nils
Holgersson	Holgersson	k1gInSc4	Holgersson
Pokémon	Pokémona	k1gFnPc2	Pokémona
Pugyuru	Pugyur	k1gInSc2	Pugyur
Včelka	včelka	k1gFnSc1	včelka
Mája	Mája	k1gFnSc1	Mája
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Kodomo	Kodoma	k1gFnSc5	Kodoma
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc3d1	slovenská
Wikipedii	Wikipedie	k1gFnSc3	Wikipedie
<g/>
.	.	kIx.	.
</s>
