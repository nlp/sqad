<s>
Růže	růže	k1gFnSc1	růže
(	(	kIx(	(
<g/>
Rosa	Rosa	k1gFnSc1	Rosa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
keřovitých	keřovitý	k2eAgFnPc2d1	keřovitá
rostlin	rostlina	k1gFnPc2	rostlina
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
druhy	druh	k1gInPc7	druh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
většinou	většinou	k6eAd1	většinou
keře	keř	k1gInPc1	keř
<g/>
,	,	kIx,	,
popínavé	popínavý	k2eAgFnPc1d1	popínavá
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
rostliny	rostlina	k1gFnPc4	rostlina
plazivé	plazivý	k2eAgFnPc4d1	plazivá
<g/>
.	.	kIx.	.
</s>
<s>
Dosahují	dosahovat	k5eAaImIp3nP	dosahovat
délky	délka	k1gFnPc1	délka
2-5	[number]	k4	2-5
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
až	až	k9	až
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Růže	růže	k1gFnSc1	růže
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
a	a	k8xC	a
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
počtu	počet	k1gInSc6	počet
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jako	jako	k8xS	jako
okrasné	okrasný	k2eAgFnPc1d1	okrasná
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
růže	růž	k1gFnSc2	růž
je	být	k5eAaImIp3nS	být
souplodí	souplodí	k1gNnSc1	souplodí
nažek	nažka	k1gFnPc2	nažka
uzavřené	uzavřený	k2eAgFnPc4d1	uzavřená
v	v	k7c6	v
šípku	šípek	k1gInSc6	šípek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6
heraldice	heraldika	k1gFnSc6
je	on	k3xPp3gInPc4
růže	růže	k1gFnSc1
častým	častý	k2eAgInSc7d1
motivem	motiv	k1gInSc7
<g/>
,	,	kIx,
kreslí	kreslit	k5eAaImIp3nS
se	se	k3xPyFc4
zpravidla	zpravidla	k6eAd1
s	s	k7c7
pěti	pět	k4xCc7
okvětními	okvětní	k2eAgInPc7d1
lístky	lístek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2	podle
odlišného	odlišný	k2eAgInSc2d1	odlišný
přístupu	přístup	k1gInSc2	přístup
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
botaniků	botanik	k1gMnPc2	botanik
k	k	k7c3	k
vymezení	vymezení	k1gNnSc3	vymezení
je	být	k5eAaImIp3nS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
100	[number]	k4	100
až	až	k9	až
300	[number]	k4	300
samostatných	samostatný	k2eAgInPc2d1	samostatný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Určování	určování	k1gNnSc1	určování
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
růží	růž	k1gFnPc2	růž
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
značné	značný	k2eAgFnSc3d1	značná
morfologické	morfologický	k2eAgFnSc3d1	morfologická
mnohotvárnosti	mnohotvárnost	k1gFnSc3	mnohotvárnost
a	a	k8xC	a
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
porovnání	porovnání	k1gNnSc4	porovnání
mimo	mimo	k6eAd1	mimo
květů	květ	k1gInPc2	květ
<g/>
,	,	kIx,	,
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
ostnů	osten	k1gInPc2	osten
a	a	k8xC	a
šípků	šípek	k1gInPc2	šípek
také	také	k9	také
i	i	k9	i
kališních	kališní	k2eAgInPc2d1	kališní
lístků	lístek	k1gInPc2	lístek
po	po	k7c6	po
odkvětu	odkvět	k1gInSc6	odkvět
či	či	k8xC	či
sterilních	sterilní	k2eAgInPc2d1	sterilní
letorostů	letorost	k1gInPc2	letorost
u	u	k7c2	u
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
vedeny	veden	k2eAgInPc1d1	veden
v	v	k7c6	v
nižší	nízký	k2eAgFnSc6d2	nižší
taxonomické	taxonomický	k2eAgFnSc6d1	Taxonomická
jednotce	jednotka	k1gFnSc6	jednotka
<g/>
;	;	kIx,	;
tedy	tedy	k9	tedy
jako	jako	k9	jako
poddruhy	poddruh	k1gInPc1	poddruh
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
za	za	k7c4	za
samostatné	samostatný	k2eAgInPc4d1	samostatný
druhy	druh	k1gInPc4	druh
považují	považovat	k5eAaImIp3nP	považovat
i	i	k9	i
mezidruhoví	mezidruhový	k2eAgMnPc1d1	mezidruhový
kříženci	kříženec	k1gMnPc1	kříženec
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc4d3	nejstarší
doklady	doklad	k1gInPc4	doklad
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
růží	růž	k1gFnPc2	růž
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
období	období	k1gNnSc2	období
před	před	k7c7	před
40	[number]	k4	40
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Růže	růž	k1gFnPc1	růž
jsou	být	k5eAaImIp3nP	být
opadavé	opadavý	k2eAgFnPc1d1	opadavá
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
stálezelené	stálezelený	k2eAgFnPc4d1	stálezelená
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Narůstají	narůstat	k5eAaImIp3nP	narůstat
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
nižší	nízký	k2eAgFnSc6d2	nižší
až	až	k8xS	až
středně	středně	k6eAd1	středně
vysoké	vysoký	k2eAgInPc4d1	vysoký
keře	keř	k1gInPc4	keř
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
prutnatě	prutnatě	k6eAd1	prutnatě
převislé	převislý	k2eAgFnPc1d1	převislá
<g/>
,	,	kIx,	,
poléhavé	poléhavý	k2eAgFnPc1d1	poléhavá
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
opírají	opírat	k5eAaImIp3nP	opírat
o	o	k7c4	o
okolní	okolní	k2eAgFnPc4d1	okolní
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
nabývají	nabývat	k5eAaImIp3nP	nabývat
charakter	charakter	k1gInSc4	charakter
lian	liana	k1gFnPc2	liana
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
úponky	úponka	k1gFnSc2	úponka
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
popínání	popínání	k1gNnSc3	popínání
ovšem	ovšem	k9	ovšem
růže	růž	k1gInPc1	růž
nemají	mít	k5eNaImIp3nP	mít
a	a	k8xC	a
nemají	mít	k5eNaImIp3nP	mít
ani	ani	k8xC	ani
ovíjivé	ovíjivý	k2eAgFnPc1d1	ovíjivá
lodyhy	lodyha	k1gFnPc1	lodyha
<g/>
.	.	kIx.	.
</s>
<s>
Kořenová	kořenový	k2eAgFnSc1d1	kořenová
soustava	soustava	k1gFnSc1	soustava
se	s	k7c7	s
zřetelným	zřetelný	k2eAgInSc7d1	zřetelný
hlavním	hlavní	k2eAgInSc7d1	hlavní
kořenem	kořen	k1gInSc7	kořen
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
růží	růžit	k5eAaImIp3nS	růžit
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c4	o
mělce	mělce	k6eAd1	mělce
kořenící	kořenící	k2eAgInPc4d1	kořenící
podzemní	podzemní	k2eAgInPc4d1	podzemní
výběžky	výběžek	k1gInPc4	výběžek
<g/>
.	.	kIx.	.
</s>
<s>
Větve	větev	k1gFnPc1	větev
a	a	k8xC	a
větvičky	větvička	k1gFnPc1	větvička
jsou	být	k5eAaImIp3nP	být
až	až	k9	až
na	na	k7c4	na
nemnohé	mnohý	k2eNgFnPc4d1	nemnohá
výjimky	výjimka	k1gFnPc4	výjimka
ostnité	ostnitý	k2eAgFnPc4d1	ostnitá
<g/>
.	.	kIx.	.
</s>
<s>
Ostny	osten	k1gInPc1	osten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
vylomit	vylomit	k5eAaPmF	vylomit
(	(	kIx(	(
<g/>
výraz	výraz	k1gInSc1	výraz
trn	trn	k1gInSc1	trn
není	být	k5eNaImIp3nS	být
morfologicky	morfologicky	k6eAd1	morfologicky
správný	správný	k2eAgInSc1d1	správný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
druhů	druh	k1gInPc2	druh
liší	lišit	k5eAaImIp3nP	lišit
tvarem	tvar	k1gInSc7	tvar
<g/>
,	,	kIx,	,
velikostí	velikost	k1gFnSc7	velikost
i	i	k8xC	i
hustotou	hustota	k1gFnSc7	hustota
umístění	umístění	k1gNnSc2	umístění
na	na	k7c6	na
větvích	větev	k1gFnPc6	větev
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jak	jak	k8xS	jak
všechny	všechen	k3xTgFnPc4	všechen
takřka	takřka	k6eAd1	takřka
stejné	stejný	k2eAgFnPc4d1	stejná
<g/>
,	,	kIx,	,
tak	tak	k9	tak
výrazně	výrazně	k6eAd1	výrazně
různotvaré	různotvarý	k2eAgNnSc1d1	různotvarý
<g/>
.	.	kIx.	.
</s>
<s>
Nalezneme	naleznout	k5eAaPmIp1nP	naleznout
i	i	k9	i
typy	typ	k1gInPc1	typ
jemně	jemně	k6eAd1	jemně
jehličnaté	jehličnatý	k2eAgInPc1d1	jehličnatý
až	až	k8xS	až
štětinovité	štětinovitý	k2eAgInPc1d1	štětinovitý
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
však	však	k9	však
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
na	na	k7c4	na
hmat	hmat	k1gInSc4	hmat
velmi	velmi	k6eAd1	velmi
nepříjemné	příjemný	k2eNgFnPc1d1	nepříjemná
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
růží	růž	k1gFnPc2	růž
jsou	být	k5eAaImIp3nP	být
střídavé	střídavý	k2eAgInPc1d1	střídavý
se	s	k7c7	s
zubatým	zubatý	k2eAgInSc7d1	zubatý
okrajem	okraj	k1gInSc7	okraj
<g/>
,	,	kIx,	,
zakončené	zakončený	k2eAgInPc4d1	zakončený
jedním	jeden	k4xCgInSc7	jeden
lístkem	lístek	k1gInSc7	lístek
–	–	k?	–
tedy	tedy	k9	tedy
lichozpeřené	lichozpeřený	k2eAgNnSc1d1	lichozpeřené
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
5	[number]	k4	5
<g/>
četné	četný	k2eAgInPc1d1	četný
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
chlupaté	chlupatý	k2eAgInPc1d1	chlupatý
až	až	k9	až
přisedle	přisedle	k6eAd1	přisedle
žlaznaté	žlaznatý	k2eAgNnSc1d1	žlaznaté
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
vytrvalé	vytrvalý	k2eAgInPc1d1	vytrvalý
palisty	palist	k1gInPc1	palist
jsou	být	k5eAaImIp3nP	být
přirostlé	přirostlý	k2eAgFnPc1d1	přirostlá
k	k	k7c3	k
řapíku	řapík	k1gInSc3	řapík
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
popisu	popis	k1gInSc2	popis
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
u	u	k7c2	u
prošlechtěných	prošlechtěný	k2eAgFnPc2d1	prošlechtěná
růží	růž	k1gFnPc2	růž
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
i	i	k9	i
sytost	sytost	k1gFnSc1	sytost
vybarvení	vybarvení	k1gNnSc2	vybarvení
a	a	k8xC	a
lesk	lesk	k1gInSc1	lesk
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
třeba	třeba	k6eAd1	třeba
vždy	vždy	k6eAd1	vždy
přihlédnout	přihlédnout	k5eAaPmF	přihlédnout
k	k	k7c3	k
výživě	výživa	k1gFnSc3	výživa
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
oboupohlavné	oboupohlavný	k2eAgInPc1d1	oboupohlavný
<g/>
;	;	kIx,	;
u	u	k7c2	u
planě	planě	k6eAd1	planě
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
druhů	druh	k1gInPc2	druh
miskovitého	miskovitý	k2eAgInSc2d1	miskovitý
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
jednotlivě	jednotlivě	k6eAd1	jednotlivě
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
víceméně	víceméně	k9	víceméně
chudých	chudý	k2eAgMnPc2d1	chudý
(	(	kIx(	(
<g/>
výjimečně	výjimečně	k6eAd1	výjimečně
bohatých	bohatý	k2eAgMnPc2d1	bohatý
<g/>
)	)	kIx)	)
květenstvích	květenství	k1gNnPc6	květenství
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
se	se	k3xPyFc4	se
u	u	k7c2	u
divoce	divoce	k6eAd1	divoce
rostoucích	rostoucí	k2eAgFnPc2d1	rostoucí
růží	růž	k1gFnPc2	růž
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
2	[number]	k4	2
do	do	k7c2	do
asi	asi	k9	asi
10	[number]	k4	10
cm	cm	kA	cm
<g/>
;	;	kIx,	;
tyčinek	tyčinka	k1gFnPc2	tyčinka
mnoho	mnoho	k6eAd1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
(	(	kIx(	(
<g/>
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
4	[number]	k4	4
<g/>
)	)	kIx)	)
korunních	korunní	k2eAgInPc2d1	korunní
plátků	plátek	k1gInPc2	plátek
je	být	k5eAaImIp3nS	být
vejčitého	vejčitý	k2eAgInSc2d1	vejčitý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
mělce	mělce	k6eAd1	mělce
dvoulaločného	dvoulaločný	k2eAgNnSc2d1	dvoulaločný
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
divokých	divoký	k2eAgFnPc2d1	divoká
růží	růž	k1gFnPc2	růž
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
růžová	růžový	k2eAgFnSc1d1	růžová
v	v	k7c6	v
různě	různě	k6eAd1	různě
sytých	sytý	k2eAgInPc6d1	sytý
odstínech	odstín	k1gInPc6	odstín
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
čistě	čistě	k6eAd1	čistě
bílá	bílý	k2eAgNnPc1d1	bílé
<g/>
,	,	kIx,	,
fialově	fialově	k6eAd1	fialově
červená	červený	k2eAgFnSc1d1	červená
<g/>
;	;	kIx,	;
jen	jen	k9	jen
málo	málo	k4c4	málo
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
žlutých	žlutý	k2eAgFnPc2d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
dužnatých	dužnatý	k2eAgNnPc6d1	dužnaté
plodenstvích	plodenství	k1gNnPc6	plodenství
–	–	k?	–
šípcích	šípek	k1gInPc6	šípek
(	(	kIx(	(
<g/>
hypanthium	hypanthium	k1gNnSc4	hypanthium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
u	u	k7c2	u
původních	původní	k2eAgInPc2d1	původní
druhů	druh	k1gInPc2	druh
růží	růžit	k5eAaImIp3nP	růžit
značnou	značný	k2eAgFnSc4d1	značná
tvarovou	tvarový	k2eAgFnSc4d1	tvarová
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
šípky	šípka	k1gFnPc1	šípka
kulovité	kulovitý	k2eAgFnPc1d1	kulovitá
<g/>
,	,	kIx,	,
vejčité	vejčitý	k2eAgFnPc1d1	vejčitá
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
5	[number]	k4	5
do	do	k7c2	do
50	[number]	k4	50
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zabarvení	zabarvení	k1gNnSc6	zabarvení
jednoznačně	jednoznačně	k6eAd1	jednoznačně
převládá	převládat	k5eAaImIp3nS	převládat
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
vzácné	vzácný	k2eAgNnSc1d1	vzácné
je	být	k5eAaImIp3nS	být
zbarvené	zbarvený	k2eAgFnPc4d1	zbarvená
černé	černý	k2eAgFnPc4d1	černá
<g/>
,	,	kIx,	,
oranžové	oranžový	k2eAgFnPc4d1	oranžová
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgFnPc4d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
šípků	šípek	k1gInPc2	šípek
jsou	být	k5eAaImIp3nP	být
drobné	drobný	k2eAgInPc1d1	drobný
ochmýřené	ochmýřený	k2eAgInPc1d1	ochmýřený
plody	plod	k1gInPc1	plod
-	-	kIx~	-
nažky	nažka	k1gFnPc1	nažka
<g/>
.	.	kIx.	.
</s>
<s>
Růže	růž	k1gFnPc1	růž
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
v	v	k7c6	v
květních	květní	k2eAgInPc6d1	květní
plátcích	plátek	k1gInPc6	plátek
směsi	směs	k1gFnSc2	směs
aromatických	aromatický	k2eAgFnPc2d1	aromatická
silic	silice	k1gFnPc2	silice
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
množství	množství	k1gNnSc1	množství
velmi	velmi	k6eAd1	velmi
kolísá	kolísat	k5eAaImIp3nS	kolísat
nejen	nejen	k6eAd1	nejen
u	u	k7c2	u
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
počasí	počasí	k1gNnSc6	počasí
a	a	k8xC	a
denní	denní	k2eAgFnSc6d1	denní
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
vonnou	vonný	k2eAgFnSc7d1	vonná
složkou	složka	k1gFnSc7	složka
jsou	být	k5eAaImIp3nP	být
alkoholy	alkohol	k1gInPc1	alkohol
geraniol	geraniola	k1gFnPc2	geraniola
a	a	k8xC	a
l-citronellol	litronellola	k1gFnPc2	l-citronellola
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
botanických	botanický	k2eAgInPc2d1	botanický
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
růží	růžit	k5eAaImIp3nS	růžit
jim	on	k3xPp3gMnPc3	on
blízkých	blízký	k2eAgMnPc2d1	blízký
nejvíce	hodně	k6eAd3	hodně
voní	vonět	k5eAaImIp3nS	vonět
Rosa	Rosa	k1gMnSc1	Rosa
gallica	gallica	k1gMnSc1	gallica
<g/>
,	,	kIx,	,
R.	R.	kA	R.
x	x	k?	x
centifolia	centifolia	k1gFnSc1	centifolia
<g/>
,	,	kIx,	,
R.	R.	kA	R.
x	x	k?	x
damascena	damascen	k2eAgFnSc1d1	damascena
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
moderními	moderní	k2eAgFnPc7d1	moderní
odrůdami	odrůda	k1gFnPc7	odrůda
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
význačně	význačně	k6eAd1	význačně
vonící	vonící	k2eAgFnPc1d1	vonící
odrůdy	odrůda	k1gFnPc1	odrůda
růží	růžit	k5eAaImIp3nP	růžit
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
čajohybridů	čajohybrid	k1gInPc2	čajohybrid
(	(	kIx(	(
<g/>
ČH	ČH	kA	ČH
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Duftwolke	Duftwolke	k1gFnSc1	Duftwolke
<g/>
,	,	kIx,	,
Eminence	eminence	k1gFnSc1	eminence
<g/>
,	,	kIx,	,
Christian	Christian	k1gMnSc1	Christian
Dior	Dior	k1gMnSc1	Dior
<g/>
,	,	kIx,	,
Mainzer	Mainzer	k1gMnSc1	Mainzer
Fastnacht	Fastnacht	k1gMnSc1	Fastnacht
<g/>
,	,	kIx,	,
Papa	papa	k1gMnSc1	papa
Meilland	Meilland	k1gInSc1	Meilland
<g/>
,	,	kIx,	,
Silver	Silver	k1gInSc1	Silver
Star	Star	kA	Star
<g/>
,	,	kIx,	,
Sutter	Sutter	k1gInSc1	Sutter
́	́	k?	́
<g/>
s	s	k7c7	s
Gold	Gold	k1gInSc4	Gold
Sterling	sterling	k1gInSc4	sterling
Silver	Silvra	k1gFnPc2	Silvra
<g/>
,	,	kIx,	,
Whisky	whisky	k1gFnPc1	whisky
Mezi	mezi	k7c7	mezi
sadovými	sadový	k2eAgInPc7d1	sadový
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
voní	vonět	k5eAaImIp3nP	vonět
mnohé	mnohý	k2eAgFnPc1d1	mnohá
tzv.	tzv.	kA	tzv.
anglické	anglický	k2eAgFnSc2d1	anglická
růže	růž	k1gFnSc2	růž
<g/>
.	.	kIx.	.
</s>
<s>
Světoznámé	světoznámý	k2eAgNnSc1d1	světoznámé
je	být	k5eAaImIp3nS	být
pěstování	pěstování	k1gNnSc1	pěstování
olejodárných	olejodárný	k2eAgFnPc2d1	olejodárná
růží	růž	k1gFnPc2	růž
(	(	kIx(	(
<g/>
R.	R.	kA	R.
x	x	k?	x
damascena	damascen	k2eAgFnSc1d1	damascena
var	var	k1gInSc4	var
trigintipetala	trigintipetat	k5eAaImAgFnS	trigintipetat
<g/>
)	)	kIx)	)
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
kryje	krýt	k5eAaImIp3nS	krýt
70	[number]	k4	70
až	až	k9	až
80	[number]	k4	80
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Střediskem	středisko	k1gNnSc7	středisko
120	[number]	k4	120
km	km	kA	km
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
údolí	údolí	k1gNnSc2	údolí
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
Kazanläk	Kazanläka	k1gFnPc2	Kazanläka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
1	[number]	k4	1
kg	kg	kA	kg
oleje	olej	k1gInSc2	olej
(	(	kIx(	(
<g/>
silice	silice	k1gFnSc1	silice
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
3000	[number]	k4	3000
kg	kg	kA	kg
růžových	růžový	k2eAgInPc2d1	růžový
plátků	plátek	k1gInPc2	plátek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
celková	celkový	k2eAgFnSc1d1	celková
sklizeň	sklizeň	k1gFnSc1	sklizeň
ze	z	k7c2	z
období	období	k1gNnSc2	období
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
května	květen	k1gInSc2	květen
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
června	červen	k1gInSc2	červen
z	z	k7c2	z
1	[number]	k4	1
ha	ha	kA	ha
plantáže	plantáž	k1gFnSc2	plantáž
<g/>
.	.	kIx.	.
</s>
<s>
Sbírá	sbírat	k5eAaImIp3nS	sbírat
se	se	k3xPyFc4	se
od	od	k7c2	od
svítání	svítání	k1gNnSc2	svítání
jen	jen	k9	jen
do	do	k7c2	do
10	[number]	k4	10
hodin	hodina	k1gFnPc2	hodina
dopoledne	dopoledne	k6eAd1	dopoledne
<g/>
.	.	kIx.	.
</s>
<s>
Denní	denní	k2eAgInSc1d1	denní
výkon	výkon	k1gInSc1	výkon
jednoho	jeden	k4xCgMnSc2	jeden
pracovníka	pracovník	k1gMnSc2	pracovník
je	být	k5eAaImIp3nS	být
15	[number]	k4	15
až	až	k9	až
20	[number]	k4	20
kg	kg	kA	kg
růžových	růžový	k2eAgMnPc2d1	růžový
plátku	plátek	k1gInSc2	plátek
<g/>
.	.	kIx.	.
</s>
<s>
Plochy	plocha	k1gFnPc1	plocha
růží	růž	k1gFnPc2	růž
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
dostatečné	dostatečný	k2eAgInPc1d1	dostatečný
<g/>
,	,	kIx,	,
problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
však	však	k9	však
soustředit	soustředit	k5eAaPmF	soustředit
dostatek	dostatek	k1gInSc4	dostatek
pracovníků	pracovník	k1gMnPc2	pracovník
na	na	k7c4	na
jen	jen	k6eAd1	jen
měsíc	měsíc	k1gInSc4	měsíc
trvající	trvající	k2eAgFnSc1d1	trvající
sklizeň	sklizeň	k1gFnSc1	sklizeň
<g/>
.	.	kIx.	.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1	čistý
růžový	růžový	k2eAgInSc1d1	růžový
olej	olej	k1gInSc1	olej
je	být	k5eAaImIp3nS	být
nepostradatelnou	postradatelný	k2eNgFnSc7d1	nepostradatelná
součástí	součást	k1gFnSc7	součást
mnoha	mnoho	k4c2	mnoho
světových	světový	k2eAgInPc2d1	světový
parfémů	parfém	k1gInPc2	parfém
<g/>
.	.	kIx.	.
</s>
<s>
Růže	růž	k1gFnPc1	růž
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
v	v	k7c6	v
šípcích	šípek	k1gInPc6	šípek
karoten	karoten	k1gInSc1	karoten
<g/>
,	,	kIx,	,
vitamíny	vitamín	k1gInPc1	vitamín
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
P	P	kA	P
a	a	k8xC	a
K.	K.	kA	K.
Nejvíce	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
cení	cenit	k5eAaImIp3nS	cenit
ovšem	ovšem	k9	ovšem
obsah	obsah	k1gInSc1	obsah
vitamínu	vitamín	k1gInSc2	vitamín
C	C	kA	C
<g/>
;	;	kIx,	;
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
růží	růž	k1gFnPc2	růž
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgInSc1d1	následující
<g/>
:	:	kIx,	:
Nejnižší	nízký	k2eAgInSc1d3	nejnižší
obsah	obsah	k1gInSc1	obsah
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
šípků	šípek	k1gInPc2	šípek
růží	růž	k1gFnPc2	růž
kolem	kolem	k7c2	kolem
200	[number]	k4	200
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
<g/>
g.	g.	k?	g.
Nejvíce	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
šípky	šípka	k1gFnPc1	šípka
sbírají	sbírat	k5eAaImIp3nP	sbírat
na	na	k7c4	na
známý	známý	k2eAgInSc4d1	známý
a	a	k8xC	a
chutný	chutný	k2eAgInSc4d1	chutný
šípkový	šípkový	k2eAgInSc4d1	šípkový
čaj	čaj	k1gInSc4	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Sušením	sušení	k1gNnSc7	sušení
a	a	k8xC	a
vařením	vaření	k1gNnSc7	vaření
sice	sice	k8xC	sice
obsah	obsah	k1gInSc1	obsah
vitamínu	vitamín	k1gInSc2	vitamín
může	moct	k5eAaImIp3nS	moct
klesat	klesat	k5eAaImF	klesat
až	až	k9	až
o	o	k7c4	o
80	[number]	k4	80
%	%	kIx~	%
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
není	být	k5eNaImIp3nS	být
zanedbatelný	zanedbatelný	k2eAgInSc1d1	zanedbatelný
<g/>
.	.	kIx.	.
</s>
<s>
Šetrněji	šetrně	k6eAd2	šetrně
se	se	k3xPyFc4	se
vitamín	vitamín	k1gInSc1	vitamín
zachová	zachovat	k5eAaPmIp3nS	zachovat
v	v	k7c6	v
šípkovém	šípkový	k2eAgNnSc6d1	šípkové
víně	víno	k1gNnSc6	víno
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Rosa	Rosa	k1gFnSc1	Rosa
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
4	[number]	k4	4
podrody	podrod	k1gInPc4	podrod
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
tři	tři	k4xCgFnPc1	tři
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
malý	malý	k2eAgInSc4d1	malý
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
zařazována	zařazovat	k5eAaImNgFnS	zařazovat
do	do	k7c2	do
podrodu	podrod	k1gInSc2	podrod
Rosa	Rosa	k1gFnSc1	Rosa
(	(	kIx(	(
<g/>
též	též	k9	též
Eurosa	Eurosa	k1gFnSc1	Eurosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
největší	veliký	k2eAgInSc1d3	veliký
podrod	podrod	k1gInSc1	podrod
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
sekce	sekce	k1gFnPc4	sekce
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
nejdůležitější	důležitý	k2eAgInPc1d3	nejdůležitější
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgFnPc7	jenž
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
v	v	k7c6	v
ČR	ČR	kA	ČR
ať	ať	k8xS	ať
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
zahradách	zahrada	k1gFnPc6	zahrada
a	a	k8xC	a
parcích	park	k1gInPc6	park
<g/>
.	.	kIx.	.
</s>
<s>
Podrod	podrod	k1gInSc1	podrod
Hulthemia	Hulthemium	k1gNnSc2	Hulthemium
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgMnSc1d1	jediný
druh	druh	k1gMnSc1	druh
Rosa	Rosa	k1gMnSc1	Rosa
persica	persica	k1gMnSc1	persica
(	(	kIx(	(
<g/>
R.	R.	kA	R.
simplicifolia	simplicifolius	k1gMnSc2	simplicifolius
<g/>
,	,	kIx,	,
Hulthemia	Hulthemius	k1gMnSc2	Hulthemius
persica	persicus	k1gMnSc2	persicus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
pěstována	pěstován	k2eAgFnSc1d1	pěstována
Podrod	podrod	k1gInSc4	podrod
Platyrhodon	Platyrhodon	k1gMnSc1	Platyrhodon
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgMnSc1d1	jediný
druh	druh	k1gMnSc1	druh
Rosa	Rosa	k1gMnSc1	Rosa
roxburghii	roxburghie	k1gFnSc4	roxburghie
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
kaštanová	kaštanový	k2eAgFnSc1d1	Kaštanová
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
Podrod	podrod	k1gInSc1	podrod
Hesperhodos	Hesperhodos	k1gInSc1	Hesperhodos
<g/>
,	,	kIx,	,
2	[number]	k4	2
druhy	druh	k1gInPc1	druh
Rosa	Rosa	k1gMnSc1	Rosa
<g />
.	.	kIx.	.
</s>
<s>
minutifolia	minutifolia	k1gFnSc1	minutifolia
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
Rosa	Rosa	k1gFnSc1	Rosa
stellata	stelle	k1gNnPc1	stelle
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
Podrod	podrod	k1gInSc1	podrod
Rosa	Rosa	k1gFnSc1	Rosa
(	(	kIx(	(
<g/>
Eurosa	Eurosa	k1gFnSc1	Eurosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asi	asi	k9	asi
200	[number]	k4	200
druhů	druh	k1gInPc2	druh
Sekce	sekce	k1gFnSc1	sekce
Banksianae	Banksianae	k1gFnSc1	Banksianae
<g/>
,	,	kIx,	,
2	[number]	k4	2
druhy	druh	k1gInPc1	druh
Rosa	Rosa	k1gMnSc1	Rosa
banksiana	banksiana	k1gFnSc1	banksiana
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
R.	R.	kA	R.
banksiae	banksia	k1gFnSc2	banksia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
Sekce	sekce	k1gFnSc1	sekce
Bracteatae	Bracteatae	k1gFnSc1	Bracteatae
<g/>
,	,	kIx,	,
2	[number]	k4	2
druhy	druh	k1gInPc1	druh
Rosa	Rosa	k1gMnSc1	Rosa
bracteata	bracteat	k1gMnSc2	bracteat
<g />
.	.	kIx.	.
</s>
<s>
Sekce	sekce	k1gFnSc1	sekce
Caninae	Caninae	k1gFnSc1	Caninae
<g/>
,	,	kIx,	,
60	[number]	k4	60
druhů	druh	k1gInPc2	druh
Rosa	Rosa	k1gFnSc1	Rosa
agrestis	agrestis	k1gFnSc1	agrestis
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
polní	polní	k2eAgFnSc1d1	polní
<g/>
,	,	kIx,	,
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
Asie	Asie	k1gFnSc1	Asie
Rosa	Rosa	k1gFnSc1	Rosa
canina	canina	k1gFnSc1	canina
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
šípková	šípkový	k2eAgFnSc1d1	šípková
<g/>
,	,	kIx,	,
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
Asie	Asie	k1gFnSc1	Asie
Rosa	Rosa	k1gFnSc1	Rosa
coriifolia	coriifolia	k1gFnSc1	coriifolia
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
šedá	šedá	k1gFnSc1	šedá
<g/>
,	,	kIx,	,
Evropa	Evropa	k1gFnSc1	Evropa
Rosa	Rosa	k1gFnSc1	Rosa
dumalis	dumalis	k1gFnSc1	dumalis
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
<g />
.	.	kIx.	.
</s>
<s>
podhorská	podhorský	k2eAgFnSc1d1	podhorská
<g/>
,	,	kIx,	,
Evropa	Evropa	k1gFnSc1	Evropa
Rosa	Rosa	k1gMnSc1	Rosa
dumetorum	dumetorum	k1gInSc1	dumetorum
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
R.	R.	kA	R.
corymbifera	corymbifera	k1gFnSc1	corymbifera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
křovištní	křovištní	k2eAgFnSc1d1	křovištní
<g/>
,	,	kIx,	,
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
Asie	Asie	k1gFnSc1	Asie
Rosa	Rosa	k1gFnSc1	Rosa
glauca	glauca	k1gFnSc1	glauca
(	(	kIx(	(
<g/>
R.	R.	kA	R.
rubrifolia	rubrifolia	k1gFnSc1	rubrifolia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
sivá	sivá	k1gFnSc1	sivá
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
Evropa	Evropa	k1gFnSc1	Evropa
Rosa	Rosa	k1gFnSc1	Rosa
inodora	inodora	k1gFnSc1	inodora
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
oválnolistá	oválnolistý	k2eAgFnSc1d1	oválnolistý
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
Rosa	Rosa	k1gFnSc1	Rosa
jundzillii	jundzillie	k1gFnSc4	jundzillie
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
Jundzillova	Jundzillův	k2eAgFnSc1d1	Jundzillův
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
a	a	k8xC	a
severní	severní	k2eAgFnSc1d1	severní
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
Asie	Asie	k1gFnSc1	Asie
Rosa	Rosa	k1gFnSc1	Rosa
micrantha	micrantha	k1gFnSc1	micrantha
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
malokvětá	malokvětý	k2eAgFnSc1d1	malokvětá
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
Evropa	Evropa	k1gFnSc1	Evropa
Rosa	Rosa	k1gFnSc1	Rosa
rubiginosa	rubiginosa	k1gFnSc1	rubiginosa
(	(	kIx(	(
<g/>
R.	R.	kA	R.
eglanteria	eglanterium	k1gNnSc2	eglanterium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
<g />
.	.	kIx.	.
</s>
<s>
vinná	vinný	k2eAgFnSc1d1	vinná
<g/>
,	,	kIx,	,
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
Asie	Asie	k1gFnSc1	Asie
Rosa	Rosa	k1gFnSc1	Rosa
serafinii	serafinie	k1gFnSc4	serafinie
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
sicilská	sicilský	k2eAgFnSc1d1	sicilská
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
Evropa	Evropa	k1gFnSc1	Evropa
Rosa	Rosa	k1gFnSc1	Rosa
sherardii	sherardie	k1gFnSc4	sherardie
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
přehlížená	přehlížený	k2eAgFnSc1d1	přehlížená
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
Evropa	Evropa	k1gFnSc1	Evropa
Rosa	Rosa	k1gFnSc1	Rosa
tomentosa	tomentosa	k1gFnSc1	tomentosa
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
plstnatá	plstnatý	k2eAgFnSc1d1	plstnatá
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
Evropa	Evropa	k1gFnSc1	Evropa
Rosa	Rosa	k1gFnSc1	Rosa
villosa	villosa	k1gFnSc1	villosa
(	(	kIx(	(
<g/>
R.	R.	kA	R.
<g />
.	.	kIx.	.
</s>
<s>
pomifera	pomifera	k1gFnSc1	pomifera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
jablíčkonosná	jablíčkonosný	k2eAgFnSc1d1	jablíčkonosný
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
Evropa	Evropa	k1gFnSc1	Evropa
Sekce	sekce	k1gFnSc1	sekce
Carolinae	Carolinae	k1gFnSc1	Carolinae
<g/>
,	,	kIx,	,
6	[number]	k4	6
druhů	druh	k1gInPc2	druh
Rosa	Rosa	k1gFnSc1	Rosa
carolina	carolina	k1gFnSc1	carolina
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
karolínská	karolínský	k2eAgFnSc1d1	karolínská
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
Rosa	Rosa	k1gFnSc1	Rosa
nitida	nitida	k1gFnSc1	nitida
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
lesklá	lesklý	k2eAgFnSc1d1	lesklá
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
Rosa	Rosa	k1gFnSc1	Rosa
palustris	palustris	k1gFnSc1	palustris
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
bahenní	bahenní	k2eAgFnSc1d1	bahenní
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
Rosa	Rosa	k1gFnSc1	Rosa
<g />
.	.	kIx.	.
</s>
<s>
virginiina	virginiin	k2eAgFnSc1d1	virginiin
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
virginská	virginský	k2eAgFnSc1d1	virginská
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
Sekce	sekce	k1gFnSc1	sekce
Cinnamomeae	Cinnamomeae	k1gFnSc1	Cinnamomeae
<g/>
,	,	kIx,	,
85	[number]	k4	85
druhů	druh	k1gInPc2	druh
Rosa	Rosa	k1gFnSc1	Rosa
acicularis	acicularis	k1gFnSc1	acicularis
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
jehličkovitá	jehličkovitý	k2eAgFnSc1d1	jehličkovitá
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
Asie	Asie	k1gFnSc1	Asie
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
Evropa	Evropa	k1gFnSc1	Evropa
Rosa	Rosa	k1gFnSc1	Rosa
arkansana	arkansana	k1gFnSc1	arkansana
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
Rosa	Rosa	k1gFnSc1	Rosa
beggeriana	beggeriana	k1gFnSc1	beggeriana
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
Bergerova	Bergerův	k2eAgFnSc1d1	Bergerova
<g/>
,	,	kIx,	,
Persie	Persie	k1gFnSc1	Persie
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Altaj	Altaj	k1gInSc1	Altaj
Rosa	Rosa	k1gMnSc1	Rosa
blanda	blanda	k1gFnSc1	blanda
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
něžná	něžný	k2eAgFnSc1d1	něžná
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
Rosa	Rosa	k1gFnSc1	Rosa
holodonta	holodonta	k1gFnSc1	holodonta
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
zoubkovaná	zoubkovaný	k2eAgFnSc1d1	zoubkovaná
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
Čína	Čína	k1gFnSc1	Čína
Rosa	Rosa	k1gFnSc1	Rosa
majalis	majalis	k1gFnSc1	majalis
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
májová	májová	k1gFnSc1	májová
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
Evropa	Evropa	k1gFnSc1	Evropa
Rosa	Rosa	k1gFnSc1	Rosa
moyesii	moyesie	k1gFnSc4	moyesie
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
Moyesova	Moyesův	k2eAgFnSc1d1	Moyesova
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
Čína	Čína	k1gFnSc1	Čína
Rosa	Rosa	k1gFnSc1	Rosa
multibracteata	multibracteata	k1gFnSc1	multibracteata
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
západní	západní	k2eAgFnSc1d1	západní
Čína	Čína	k1gFnSc1	Čína
Rosa	Rosa	k1gFnSc1	Rosa
nutkana	nutkana	k1gFnSc1	nutkana
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
Rosa	Rosa	k1gMnSc1	Rosa
oxyodon	oxyodon	k1gMnSc1	oxyodon
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
ostřezubatá	ostřezubatá	k1gFnSc1	ostřezubatá
<g/>
,	,	kIx,	,
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
Rosa	Rosa	k1gFnSc1	Rosa
pendulina	pendulin	k2eAgFnSc1d1	pendulina
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
R.	R.	kA	R.
alpina	alpinum	k1gNnSc2	alpinum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
alpská	alpský	k2eAgFnSc1d1	alpská
<g/>
,	,	kIx,	,
r.	r.	kA	r.
převislá	převislý	k2eAgFnSc1d1	převislá
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
Evropa	Evropa	k1gFnSc1	Evropa
Rosa	Rosa	k1gFnSc1	Rosa
pisocarpa	pisocarpa	k1gFnSc1	pisocarpa
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
Rosa	Rosa	k1gFnSc1	Rosa
rugosa	rugosa	k1gFnSc1	rugosa
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
svraskalá	svraskalý	k2eAgFnSc1d1	svraskalá
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc1d1	východní
Asie	Asie	k1gFnSc1	Asie
Rosa	Rosa	k1gFnSc1	Rosa
setipoda	setipoda	k1gFnSc1	setipoda
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
Čína	Čína	k1gFnSc1	Čína
Rosa	Rosa	k1gFnSc1	Rosa
sweginzowii	sweginzowie	k1gFnSc4	sweginzowie
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
Sweginzovova	Sweginzovův	k2eAgFnSc1d1	Sweginzovův
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
Čína	Čína	k1gFnSc1	Čína
Rosa	Rosa	k1gFnSc1	Rosa
willmottiae	willmottiae	k1gFnSc1	willmottiae
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
Willmottova	Willmottův	k2eAgFnSc1d1	Willmottův
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
Čína	Čína	k1gFnSc1	Čína
Rosa	Rosa	k1gFnSc1	Rosa
woodsii	woodsie	k1gFnSc4	woodsie
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
Sekce	sekce	k1gFnSc1	sekce
Gallicanae	Gallicanae	k1gFnSc1	Gallicanae
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
druh	druh	k1gInSc4	druh
Rosa	Rosa	k1gFnSc1	Rosa
x	x	k?	x
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
růže	růže	k1gFnSc1	růže
bílá	bílý	k2eAgFnSc1d1	bílá
Rosa	Rosa	k1gFnSc1	Rosa
x	x	k?	x
centifolia	centifolia	k1gFnSc1	centifolia
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
stolistá	stolistý	k2eAgFnSc1d1	stolistá
Rosa	Rosa	k1gFnSc1	Rosa
x	x	k?	x
damascena	damastit	k5eAaPmNgFnS	damastit
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
damascenská	damascenský	k2eAgFnSc1d1	damascenská
Rosa	Rosa	k1gFnSc1	Rosa
gallica	gallica	k1gFnSc1	gallica
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
keltská	keltský	k2eAgFnSc1d1	keltská
<g/>
,	,	kIx,	,
r.	r.	kA	r.
francouzská	francouzský	k2eAgFnSc1d1	francouzská
<g/>
,	,	kIx,	,
r.	r.	kA	r.
galská	galský	k2eAgFnSc1d1	galská
<g/>
,	,	kIx,	,
r.	r.	kA	r.
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
provensálská	provensálský	k2eAgFnSc1d1	provensálská
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
Asie	Asie	k1gFnSc1	Asie
Sekce	sekce	k1gFnSc1	sekce
Indiceae	Indiceae	k1gFnSc1	Indiceae
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
3	[number]	k4	3
druhy	druh	k1gInPc7	druh
Rosa	Rosa	k1gMnSc1	Rosa
gigantea	gigantea	k1gMnSc1	gigantea
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
Rosa	Rosa	k1gFnSc1	Rosa
chinensis	chinensis	k1gFnSc1	chinensis
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
R.	R.	kA	R.
indica	indica	k1gFnSc1	indica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
čínská	čínský	k2eAgFnSc1d1	čínská
<g/>
,	,	kIx,	,
bengálka	bengálka	k1gFnSc1	bengálka
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
Rosa	Rosa	k1gFnSc1	Rosa
x	x	k?	x
odorata	odorata	k1gFnSc1	odorata
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
vonná	vonný	k2eAgFnSc1d1	vonná
<g/>
,	,	kIx,	,
r.	r.	kA	r.
čajová	čajový	k2eAgFnSc1d1	čajová
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
Sekce	sekce	k1gFnSc1	sekce
Laevigatae	Laevigatae	k1gFnSc1	Laevigatae
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
druh	druh	k1gInSc4	druh
Rosa	Rosa	k1gFnSc1	Rosa
laevigata	laevigata	k1gFnSc1	laevigata
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
Čína	Čína	k1gFnSc1	Čína
Sekce	sekce	k1gFnSc1	sekce
Pimpinellifoliae	Pimpinellifoliae	k1gFnSc1	Pimpinellifoliae
<g/>
,	,	kIx,	,
15	[number]	k4	15
druhů	druh	k1gInPc2	druh
Rosa	Rosa	k1gFnSc1	Rosa
ecae	ecae	k1gFnSc1	ecae
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
žlutokvětá	žlutokvětý	k2eAgFnSc1d1	žlutokvětá
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
Asie	Asie	k1gFnSc1	Asie
Rosa	Rosa	k1gFnSc1	Rosa
foetida	foetida	k1gFnSc1	foetida
(	(	kIx(	(
<g/>
R.	R.	kA	R.
lutea	lutea	k1gFnSc1	lutea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
kapucínská	kapucínský	k2eAgFnSc1d1	Kapucínská
<g/>
,	,	kIx,	,
r.	r.	kA	r.
páchnoucí	páchnoucí	k2eAgFnSc1d1	páchnoucí
<g/>
,	,	kIx,	,
r.	r.	kA	r.
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
Asie	Asie	k1gFnSc1	Asie
Rosa	Rosa	k1gFnSc1	Rosa
hugonis	hugonis	k1gFnSc1	hugonis
růže	růže	k1gFnSc1	růže
Hugova	Hugův	k2eAgFnSc1d1	Hugova
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
střední	střední	k2eAgFnSc1d1	střední
Čína	Čína	k1gFnSc1	Čína
Rosa	Rosa	k1gFnSc1	Rosa
omeiensis	omeiensis	k1gFnSc1	omeiensis
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
R.	R.	kA	R.
sericea	sericea	k1gFnSc1	sericea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
omejská	omejský	k2eAgFnSc1d1	omejský
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
Čína	Čína	k1gFnSc1	Čína
Rosa	Rosa	k1gFnSc1	Rosa
pimpinellifolia	pimpinellifolia	k1gFnSc1	pimpinellifolia
(	(	kIx(	(
<g/>
R.	R.	kA	R.
spinosissima	spinosissima	k1gNnSc4	spinosissima
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
bedrníkolistá	bedrníkolistý	k2eAgFnSc1d1	bedrníkolistá
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
Asie	Asie	k1gFnSc1	Asie
Rosa	Rosa	k1gFnSc1	Rosa
primula	primula	k?	primula
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
Čína	Čína	k1gFnSc1	Čína
Rosa	Rosa	k1gFnSc1	Rosa
xanthina	xanthina	k1gFnSc1	xanthina
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
zlatožlutá	zlatožlutý	k2eAgFnSc1d1	zlatožlutá
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Korea	Korea	k1gFnSc1	Korea
Sekce	sekce	k1gFnSc1	sekce
Synstylae	Synstylae	k1gFnSc1	Synstylae
<g/>
,	,	kIx,	,
30	[number]	k4	30
druhů	druh	k1gInPc2	druh
Rosa	Rosa	k1gFnSc1	Rosa
arvensis	arvensis	k1gFnSc1	arvensis
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
rolní	rolní	k2eAgFnSc1d1	rolní
<g/>
,	,	kIx,	,
r.	r.	kA	r.
plazivá	plazivý	k2eAgFnSc1d1	plazivá
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
Rosa	Rosa	k1gMnSc1	Rosa
filipes	filipes	k1gMnSc1	filipes
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
Rosa	Rosa	k1gFnSc1	Rosa
helenae	helenae	k1gFnSc1	helenae
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
Helenina	Helenin	k2eAgFnSc1d1	Helenina
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
Čína	Čína	k1gFnSc1	Čína
Rosa	Rosa	k1gFnSc1	Rosa
moschata	moschata	k1gFnSc1	moschata
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
mošusová	mošusový	k2eAgFnSc1d1	mošusová
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
Rosa	Rosa	k1gFnSc1	Rosa
multiflora	multiflora	k1gFnSc1	multiflora
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
mnohokvětá	mnohokvětý	k2eAgFnSc1d1	mnohokvětá
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Korea	Korea	k1gFnSc1	Korea
Rosa	Rosa	k1gMnSc1	Rosa
sempervirens	sempervirens	k1gInSc1	sempervirens
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
vždyzelená	vždyzelený	k2eAgFnSc1d1	vždyzelená
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
Afrika	Afrika	k1gFnSc1	Afrika
Rosa	Rosa	k1gFnSc1	Rosa
setigera	setigera	k1gFnSc1	setigera
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
prérijní	prérijní	k2eAgFnSc1d1	prérijní
<g/>
,	,	kIx,	,
r.	r.	kA	r.
štětinatá	štětinatý	k2eAgFnSc1d1	štětinatá
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
<g/>
.	.	kIx.	.
<g/>
Amerika	Amerika	k1gFnSc1	Amerika
Rosa	Rosa	k1gFnSc1	Rosa
wichuraiana	wichuraiana	k1gFnSc1	wichuraiana
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
Wichurova	Wichurův	k2eAgFnSc1d1	Wichurův
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc1d1	východní
Asie	Asie	k1gFnSc2	Asie
Růže	růž	k1gFnSc2	růž
rostou	růst	k5eAaImIp3nP	růst
divoce	divoce	k6eAd1	divoce
takřka	takřka	k6eAd1	takřka
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Nerostou	růst	k5eNaImIp3nP	růst
pochopitelně	pochopitelně	k6eAd1	pochopitelně
v	v	k7c6	v
arktických	arktický	k2eAgFnPc6d1	arktická
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
na	na	k7c6	na
pouštích	poušť	k1gFnPc6	poušť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
subtropech	subtropy	k1gInPc6	subtropy
i	i	k8xC	i
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
některé	některý	k3yIgInPc4	některý
druhy	druh	k1gInPc4	druh
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
(	(	kIx(	(
<g/>
Etiopie	Etiopie	k1gFnPc4	Etiopie
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
oblastem	oblast	k1gFnPc3	oblast
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
druhů	druh	k1gInPc2	druh
patří	patřit	k5eAaImIp3nP	patřit
Středomoří	středomoří	k1gNnSc4	středomoří
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Asie	Asie	k1gFnSc2	Asie
<g/>
;	;	kIx,	;
naopak	naopak	k6eAd1	naopak
jen	jen	k9	jen
málo	málo	k4c1	málo
růží	růž	k1gFnPc2	růž
roste	růst	k5eAaImIp3nS	růst
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
růže	růž	k1gInSc2	růž
překračují	překračovat	k5eAaImIp3nP	překračovat
polární	polární	k2eAgInSc4d1	polární
kruh	kruh	k1gInSc4	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
růží	růž	k1gFnPc2	růž
roste	růst	k5eAaImIp3nS	růst
ve	v	k7c6	v
"	"	kIx"	"
<g/>
starém	starý	k2eAgInSc6d1	starý
světě	svět	k1gInSc6	svět
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
pochází	pocházet	k5eAaImIp3nS	pocházet
jen	jen	k9	jen
asi	asi	k9	asi
20	[number]	k4	20
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nezúčastnily	zúčastnit	k5eNaPmAgInP	zúčastnit
vzniku	vznik	k1gInSc3	vznik
kulturních	kulturní	k2eAgFnPc2d1	kulturní
růží	růž	k1gFnPc2	růž
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
sbírku	sbírka	k1gFnSc4	sbírka
planých	planý	k2eAgFnPc2d1	planá
i	i	k8xC	i
kulturních	kulturní	k2eAgFnPc2d1	kulturní
růží	růž	k1gFnPc2	růž
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
navštívit	navštívit	k5eAaPmF	navštívit
v	v	k7c6	v
Botanické	botanický	k2eAgFnSc6d1	botanická
zahradě	zahrada	k1gFnSc6	zahrada
Chotobuz	Chotobuz	k1gInSc4	Chotobuz
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
kolem	kolem	k7c2	kolem
850	[number]	k4	850
odrůd	odrůda	k1gFnPc2	odrůda
kulturních	kulturní	k2eAgFnPc2d1	kulturní
růží	růž	k1gFnPc2	růž
a	a	k8xC	a
přes	přes	k7c4	přes
100	[number]	k4	100
taxonů	taxon	k1gInPc2	taxon
původních	původní	k2eAgInPc2d1	původní
botanických	botanický	k2eAgInPc2d1	botanický
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
významná	významný	k2eAgNnPc1d1	významné
rozária	rozárium	k1gNnPc1	rozárium
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
soutěžní	soutěžní	k2eAgNnSc4d1	soutěžní
rozárium	rozárium	k1gNnSc4	rozárium
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
růžový	růžový	k2eAgInSc1d1	růžový
sad	sad	k1gInSc1	sad
v	v	k7c6	v
Lidicích	Lidice	k1gInPc6	Lidice
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rajhradě	Rajhrad	k1gInSc6	Rajhrad
<g/>
,	,	kIx,	,
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Petříně	Petřín	k1gInSc6	Petřín
<g/>
,	,	kIx,	,
Růžová	růžový	k2eAgFnSc1d1	růžová
zahrada	zahrada	k1gFnSc1	zahrada
na	na	k7c6	na
Konopišti	Konopiště	k1gNnSc6	Konopiště
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
.	.	kIx.	.
</s>
