<s>
Antonín	Antonín	k1gMnSc1	Antonín
Panenka	panenka	k1gFnSc1	panenka
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1948	[number]	k4	1948
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
československý	československý	k2eAgMnSc1d1	československý
fotbalista	fotbalista	k1gMnSc1	fotbalista
a	a	k8xC	a
reprezentant	reprezentant	k1gMnSc1	reprezentant
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
Bělehradu	Bělehrad	k1gInSc2	Bělehrad
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Členem	člen	k1gMnSc7	člen
prestižního	prestižní	k2eAgInSc2d1	prestižní
Klubu	klub	k1gInSc2	klub
ligových	ligový	k2eAgMnPc2d1	ligový
kanonýrů	kanonýr	k1gMnPc2	kanonýr
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
pravidel	pravidlo	k1gNnPc2	pravidlo
klubu	klub	k1gInSc2	klub
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
známého	známý	k2eAgInSc2d1	známý
pokutového	pokutový	k2eAgInSc2d1	pokutový
kopu	kop	k1gInSc2	kop
(	(	kIx(	(
<g/>
penalty	penalta	k1gFnSc2	penalta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
i	i	k9	i
španělský	španělský	k2eAgInSc1d1	španělský
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
časopis	časopis	k1gInSc1	časopis
Panenka	panenka	k1gFnSc1	panenka
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
k	k	k7c3	k
ligovému	ligový	k2eAgInSc3d1	ligový
zápasu	zápas	k1gInSc3	zápas
v	v	k7c6	v
necelých	celý	k2eNgInPc6d1	necelý
dvaceti	dvacet	k4xCc6	dvacet
letech	let	k1gInPc6	let
za	za	k7c4	za
Bohemians	Bohemians	k1gInSc4	Bohemians
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
prvním	první	k4xOgInSc7	první
reprezentačním	reprezentační	k2eAgInSc7d1	reprezentační
startem	start	k1gInSc7	start
byl	být	k5eAaImAgMnS	být
kvalifikační	kvalifikační	k2eAgInSc4d1	kvalifikační
zápas	zápas	k1gInSc4	zápas
na	na	k7c4	na
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
proti	proti	k7c3	proti
Skotsku	Skotsko	k1gNnSc3	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
díky	díky	k7c3	díky
jím	on	k3xPp3gMnSc7	on
proměněné	proměněný	k2eAgFnSc6d1	proměněná
rozhodující	rozhodující	k2eAgFnSc6d1	rozhodující
penaltě	penalta	k1gFnSc6	penalta
ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
proti	proti	k7c3	proti
Západnímu	západní	k2eAgNnSc3d1	západní
Německu	Německo	k1gNnSc3	Německo
získalo	získat	k5eAaPmAgNnS	získat
Československo	Československo	k1gNnSc1	Československo
titul	titul	k1gInSc4	titul
mistrů	mistr	k1gMnPc2	mistr
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc3	jeho
slavné	slavný	k2eAgFnSc3d1	slavná
penaltě	penalta	k1gFnSc3	penalta
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
vršovický	vršovický	k2eAgInSc1d1	vršovický
dloubák	dloubák	k1gInSc1	dloubák
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
Panenka	panenka	k1gFnSc1	panenka
goal	goal	k1gMnSc1	goal
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Panenka	panenka	k1gFnSc1	panenka
kick	kick	k1gMnSc1	kick
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
byl	být	k5eAaImAgMnS
vyhlášen	vyhlásit	k5eAaPmNgMnS
československým	československý	k2eAgMnSc7d1
fotbalistou	fotbalista	k1gMnSc7
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
kopajícího	kopající	k2eAgNnSc2d1	kopající
penaltu	penalta	k1gFnSc4	penalta
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
roli	role	k1gFnSc6	role
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
filmu	film	k1gInSc6	film
Sonáta	sonáta	k1gFnSc1	sonáta
pro	pro	k7c4	pro
zrzku	zrzka	k1gFnSc4	zrzka
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
Bohemians	Bohemiansa	k1gFnPc2	Bohemiansa
do	do	k7c2	do
Rapidu	rapid	k1gInSc2	rapid
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
odchodu	odchod	k1gInSc2	odchod
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
získal	získat	k5eAaPmAgInS	získat
dva	dva	k4xCgInPc4	dva
tituly	titul	k1gInPc4	titul
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Poháru	pohár	k1gInSc6	pohár
vítězů	vítěz	k1gMnPc2	vítěz
pohárů	pohár	k1gInPc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvouletém	dvouletý	k2eAgNnSc6d1	dvouleté
účinkování	účinkování	k1gNnSc6	účinkování
v	v	k7c6	v
rakouské	rakouský	k2eAgFnSc6d1	rakouská
druhé	druhý	k4xOgFnSc6	druhý
lize	liga	k1gFnSc6	liga
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Bohemians	Bohemiansa	k1gFnPc2	Bohemiansa
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
asistentem	asistent	k1gMnSc7	asistent
trenéra	trenér	k1gMnSc2	trenér
a	a	k8xC	a
funkcionářem	funkcionář	k1gMnSc7	funkcionář
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
získal	získat	k5eAaPmAgMnS	získat
Cenu	cena	k1gFnSc4	cena
Václava	Václav	k1gMnSc2	Václav
Jíry	Jíra	k1gMnSc2	Jíra
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
mu	on	k3xPp3gNnSc3	on
prezident	prezident	k1gMnSc1	prezident
ČR	ČR	kA	ČR
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
udělil	udělit	k5eAaPmAgMnS	udělit
státní	státní	k2eAgNnSc4d1	státní
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
-	-	kIx~	-
Medaili	medaile	k1gFnSc4	medaile
Za	za	k7c2	za
zásluhy	zásluha	k1gFnSc2	zásluha
I.	I.	kA	I.
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
doplňovacích	doplňovací	k2eAgFnPc6d1	doplňovací
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
jako	jako	k9	jako
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
subjekt	subjekt	k1gInSc4	subjekt
"	"	kIx"	"
<g/>
Nezávislí	závislý	k2eNgMnPc1d1	nezávislý
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
10	[number]	k4	10
-	-	kIx~	-
Hnutí	hnutí	k1gNnSc1	hnutí
pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
Desítku	desítka	k1gFnSc4	desítka
<g/>
"	"	kIx"	"
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
č.	č.	k?	č.
22	[number]	k4	22
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
13,44	[number]	k4	13,44
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
nepostoupil	postoupit	k5eNaPmAgMnS	postoupit
tak	tak	k6eAd1	tak
ani	ani	k8xC	ani
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
a	a	k8xC	a
předsedou	předseda	k1gMnSc7	předseda
Nezávislých	závislý	k2eNgMnPc2d1	nezávislý
je	být	k5eAaImIp3nS	být
starosta	starosta	k1gMnSc1	starosta
Prahy	Praha	k1gFnSc2	Praha
10	[number]	k4	10
Bohumil	Bohumil	k1gMnSc1	Bohumil
Zoufalík	Zoufalík	k1gMnSc1	Zoufalík
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Panenka	panenka	k1gFnSc1	panenka
debutoval	debutovat	k5eAaBmAgInS	debutovat
v	v	k7c6	v
A-mužstvu	Aužstvo	k1gNnSc6	A-mužstvo
Československa	Československo	k1gNnSc2	Československo
26	[number]	k4	26
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1973	[number]	k4	1973
v	v	k7c6	v
kvalifikačním	kvalifikační	k2eAgInSc6d1	kvalifikační
zápase	zápas	k1gInSc6	zápas
v	v	k7c6	v
Glasgowě	Glasgow	k1gInSc6	Glasgow
proti	proti	k7c3	proti
reprezentaci	reprezentace	k1gFnSc3	reprezentace
Skotska	Skotsko	k1gNnSc2	Skotsko
(	(	kIx(	(
<g/>
prohra	prohra	k1gFnSc1	prohra
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
se	se	k3xPyFc4	se
mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
1976	[number]	k4	1976
v	v	k7c6	v
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
(	(	kIx(	(
<g/>
zisk	zisk	k1gInSc1	zisk
titulu	titul	k1gInSc2	titul
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
1980	[number]	k4	1980
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
zisk	zisk	k1gInSc1	zisk
bronzové	bronzový	k2eAgFnSc2d1	bronzová
medaile	medaile	k1gFnSc2	medaile
<g/>
)	)	kIx)	)
a	a	k8xC	a
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
1982	[number]	k4	1982
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
odehrál	odehrát	k5eAaPmAgMnS	odehrát
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1973	[number]	k4	1973
<g/>
-	-	kIx~	-
<g/>
1982	[number]	k4	1982
za	za	k7c4	za
československý	československý	k2eAgInSc4d1	československý
národní	národní	k2eAgInSc4d1	národní
tým	tým	k1gInSc4	tým
59	[number]	k4	59
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
17	[number]	k4	17
gólů	gól	k1gInPc2	gól
<g/>
.	.	kIx.	.
</s>
<s>
Slavná	slavný	k2eAgFnSc1d1	slavná
proměněná	proměněný	k2eAgFnSc1d1	proměněná
penalta	penalta	k1gFnSc1	penalta
Antonína	Antonín	k1gMnSc2	Antonín
Panenky	panenka	k1gFnSc2	panenka
se	se	k3xPyFc4	se
zapsala	zapsat	k5eAaPmAgFnS	zapsat
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
fotbalu	fotbal	k1gInSc2	fotbal
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
se	se	k3xPyFc4	se
československý	československý	k2eAgInSc1d1	československý
tým	tým	k1gInSc1	tým
střetl	střetnout	k5eAaPmAgInS	střetnout
s	s	k7c7	s
mužstvem	mužstvo	k1gNnSc7	mužstvo
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
stavu	stav	k1gInSc2	stav
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
po	po	k7c6	po
prodloužení	prodloužení	k1gNnSc6	prodloužení
následoval	následovat	k5eAaImAgInS	následovat
penaltový	penaltový	k2eAgInSc1d1	penaltový
rozstřel	rozstřel	k1gInSc1	rozstřel
(	(	kIx(	(
<g/>
první	první	k4xOgFnPc4	první
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
finále	finále	k1gNnSc2	finále
evropských	evropský	k2eAgInPc2d1	evropský
šampionátů	šampionát	k1gInPc2	šampionát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1	čtyři
českoslovenští	československý	k2eAgMnPc1d1	československý
a	a	k8xC	a
tři	tři	k4xCgMnPc1	tři
němečtí	německý	k2eAgMnPc1d1	německý
fotbalisté	fotbalista	k1gMnPc1	fotbalista
proměnili	proměnit	k5eAaPmAgMnP	proměnit
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
německý	německý	k2eAgMnSc1d1	německý
hráč	hráč	k1gMnSc1	hráč
Uli	Uli	k1gMnSc1	Uli
Hoeneß	Hoeneß	k1gMnSc1	Hoeneß
přestřelil	přestřelit	k5eAaPmAgMnS	přestřelit
bránu	brána	k1gFnSc4	brána
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Panenka	panenka	k1gFnSc1	panenka
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mohl	moct	k5eAaImAgMnS	moct
Československu	Československo	k1gNnSc3	Československo
zajistit	zajistit	k5eAaPmF	zajistit
první	první	k4xOgInSc4	první
titul	titul	k1gInSc4	titul
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Rozběhl	rozběhnout	k5eAaPmAgInS	rozběhnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
předstíral	předstírat	k5eAaImAgMnS	předstírat
prudkou	prudký	k2eAgFnSc4d1	prudká
ránu	rána	k1gFnSc4	rána
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
brankář	brankář	k1gMnSc1	brankář
Sepp	Sepp	k1gMnSc1	Sepp
Maier	Maier	k1gMnSc1	Maier
skočil	skočit	k5eAaPmAgMnS	skočit
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Antonín	Antonín	k1gMnSc1	Antonín
Panenka	panenka	k1gFnSc1	panenka
míč	míč	k1gInSc4	míč
lehce	lehko	k6eAd1	lehko
podkopl	podkopnout	k5eAaPmAgMnS	podkopnout
a	a	k8xC	a
poslal	poslat	k5eAaPmAgMnS	poslat
jej	on	k3xPp3gMnSc4	on
dloubáčkem	dloubáček	k1gInSc7	dloubáček
doprostřed	doprostřed	k7c2	doprostřed
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
tak	tak	k9	tak
získalo	získat	k5eAaPmAgNnS	získat
Československo	Československo	k1gNnSc1	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc4d1	stejný
trik	trik	k1gInSc4	trik
Panenka	panenka	k1gFnSc1	panenka
použil	použít	k5eAaPmAgInS	použít
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgNnSc7	první
"	"	kIx"	"
<g/>
exekutorem	exekutor	k1gMnSc7	exekutor
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
kolega	kolega	k1gMnSc1	kolega
z	z	k7c2	z
Bohemians	Bohemiansa	k1gFnPc2	Bohemiansa
Štefan	Štefan	k1gMnSc1	Štefan
Ivančík	Ivančík	k1gMnSc1	Ivančík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sběratelské	sběratelský	k2eAgFnSc2d1	sběratelská
série	série	k1gFnSc2	série
medailí	medaile	k1gFnPc2	medaile
Československé	československý	k2eAgFnSc2d1	Československá
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
legendy	legenda	k1gFnSc2	legenda
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
Česká	český	k2eAgFnSc1d1	Česká
mincovna	mincovna	k1gFnSc1	mincovna
zlatou	zlatý	k2eAgFnSc4d1	zlatá
čtvrtuncovou	čtvrtuncový	k2eAgFnSc4d1	čtvrtuncový
medaili	medaile	k1gFnSc4	medaile
i	i	k8xC	i
Antonínu	Antonín	k1gMnSc3	Antonín
Panenkovi	Panenek	k1gMnSc3	Panenek
<g/>
.	.	kIx.	.
</s>
<s>
Medaile	medaile	k1gFnPc1	medaile
byly	být	k5eAaImAgFnP	být
vyraženy	vyrazit	k5eAaPmNgFnP	vyrazit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2016	[number]	k4	2016
v	v	k7c6	v
omezeném	omezený	k2eAgNnSc6d1	omezené
množství	množství	k1gNnSc6	množství
200	[number]	k4	200
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
averzu	averz	k1gInSc2	averz
medaile	medaile	k1gFnSc2	medaile
je	být	k5eAaImIp3nS	být
Martin	Martin	k1gMnSc1	Martin
Dašek	Dašek	k1gMnSc1	Dašek
a	a	k8xC	a
reverzu	reverz	k1gInSc2	reverz
Asamat	Asamat	k1gInSc4	Asamat
Baltaev	Baltavo	k1gNnPc2	Baltavo
<g/>
.	.	kIx.	.
</s>
