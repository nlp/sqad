<s>
Slaná	slaný	k2eAgFnSc1d1	slaná
nebo	nebo	k8xC	nebo
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
oceánu	oceán	k1gInSc2	oceán
nebo	nebo	k8xC	nebo
slaného	slaný	k2eAgNnSc2d1	slané
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
chemické	chemický	k2eAgFnPc4d1	chemická
látky	látka	k1gFnPc4	látka
způsobující	způsobující	k2eAgFnSc1d1	způsobující
její	její	k3xOp3gFnSc4	její
slanost	slanost	k1gFnSc4	slanost
(	(	kIx(	(
<g/>
salinitu	salinita	k1gFnSc4	salinita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozdílnou	rozdílný	k2eAgFnSc4d1	rozdílná
hustotu	hustota	k1gFnSc4	hustota
a	a	k8xC	a
například	například	k6eAd1	například
i	i	k9	i
rozdílnou	rozdílný	k2eAgFnSc4d1	rozdílná
teplotu	teplota	k1gFnSc4	teplota
mrznutí	mrznutí	k1gNnSc2	mrznutí
než	než	k8xS	než
u	u	k7c2	u
sladké	sladký	k2eAgFnSc2d1	sladká
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Slaná	slaný	k2eAgFnSc1d1	slaná
voda	voda	k1gFnSc1	voda
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c1	mnoho
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
výrazněji	výrazně	k6eAd2	výrazně
(	(	kIx(	(
<g/>
ostatní	ostatní	k2eAgMnSc1d1	ostatní
jen	jen	k9	jen
v	v	k7c6	v
minimálních	minimální	k2eAgFnPc6d1	minimální
koncentracích	koncentrace	k1gFnPc6	koncentrace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
,	,	kIx,	,
sírany	síran	k1gInPc4	síran
<g/>
,	,	kIx,	,
uhličitany	uhličitan	k1gInPc4	uhličitan
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
soli	sůl	k1gFnPc4	sůl
(	(	kIx(	(
<g/>
které	který	k3yIgInPc1	který
všechny	všechen	k3xTgInPc1	všechen
dohromady	dohromady	k6eAd1	dohromady
tvoří	tvořit	k5eAaImIp3nP	tvořit
mořskou	mořský	k2eAgFnSc4d1	mořská
sůl	sůl	k1gFnSc4	sůl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
plyny	plyn	k1gInPc1	plyn
jako	jako	k8xC	jako
dusík	dusík	k1gInSc1	dusík
<g/>
,	,	kIx,	,
kyslík	kyslík	k1gInSc1	kyslík
či	či	k8xC	či
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
pozemských	pozemský	k2eAgInPc6d1	pozemský
oceánech	oceán	k1gInPc6	oceán
a	a	k8xC	a
mořích	moře	k1gNnPc6	moře
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
salinitu	salinita	k1gFnSc4	salinita
kolem	kolem	k7c2	kolem
3,5	[number]	k4	3,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc4	každý
kilogram	kilogram	k1gInSc4	kilogram
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přibližně	přibližně	k6eAd1	přibližně
35	[number]	k4	35
gramů	gram	k1gInPc2	gram
rozpuštěné	rozpuštěný	k2eAgFnSc2d1	rozpuštěná
soli	sůl	k1gFnSc2	sůl
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
ionty	ion	k1gInPc4	ion
chloridu	chlorid	k1gInSc2	chlorid
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
:	:	kIx,	:
Na	na	k7c4	na
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
Cl-	Cl-	k1gMnSc1	Cl-
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
oceánu	oceán	k1gInSc2	oceán
je	být	k5eAaImIp3nS	být
1,025	[number]	k4	1,025
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
ml	ml	kA	ml
<g/>
;	;	kIx,	;
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgFnSc4d2	veliký
hustotu	hustota	k1gFnSc4	hustota
než	než	k8xS	než
sladká	sladký	k2eAgFnSc1d1	sladká
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
maximální	maximální	k2eAgFnPc4d1	maximální
hustoty	hustota	k1gFnPc4	hustota
1,000	[number]	k4	1,000
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
ml	ml	kA	ml
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
4	[number]	k4	4
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takováto	takovýto	k3xDgFnSc1	takovýto
slaná	slaný	k2eAgFnSc1d1	slaná
voda	voda	k1gFnSc1	voda
mrzne	mrznout	k5eAaImIp3nS	mrznout
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
až	až	k6eAd1	až
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
-2	-2	k4	-2
°	°	k?	°
<g/>
C.	C.	kA	C.
Světový	světový	k2eAgInSc1d1	světový
oceán	oceán	k1gInSc1	oceán
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
různé	různý	k2eAgInPc4d1	různý
kovy	kov	k1gInPc4	kov
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
jsou	být	k5eAaImIp3nP	být
podmořské	podmořský	k2eAgInPc1d1	podmořský
hydrotermální	hydrotermální	k2eAgInPc1d1	hydrotermální
prameny	pramen	k1gInPc1	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Salinita	salinita	k1gFnSc1	salinita
<g/>
.	.	kIx.	.
</s>
<s>
Mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množství	množství	k1gNnSc4	množství
iontů	ion	k1gInPc2	ion
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
buď	buď	k8xC	buď
kladný	kladný	k2eAgInSc1d1	kladný
a	a	k8xC	a
nebo	nebo	k8xC	nebo
záporný	záporný	k2eAgInSc1d1	záporný
náboj	náboj	k1gInSc1	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
zastoupeny	zastoupen	k2eAgInPc1d1	zastoupen
jsou	být	k5eAaImIp3nP	být
záporné	záporný	k2eAgInPc4d1	záporný
ionty	ion	k1gInPc4	ion
(	(	kIx(	(
<g/>
anionty	anion	k1gInPc4	anion
<g/>
)	)	kIx)	)
chloru	chlor	k1gInSc2	chlor
a	a	k8xC	a
kladné	kladný	k2eAgInPc1d1	kladný
ionty	ion	k1gInPc1	ion
(	(	kIx(	(
<g/>
kationty	kation	k1gInPc1	kation
<g/>
)	)	kIx)	)
sodíku	sodík	k1gInSc2	sodík
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dohromady	dohromady	k6eAd1	dohromady
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
mořskou	mořský	k2eAgFnSc4d1	mořská
sůl	sůl	k1gFnSc4	sůl
<g/>
,	,	kIx,	,
známou	známý	k2eAgFnSc4d1	známá
jako	jako	k9	jako
kuchyňská	kuchyňská	k1gFnSc1	kuchyňská
sůl	sůl	k1gFnSc1	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
tvoří	tvořit	k5eAaImIp3nS	tvořit
85	[number]	k4	85
%	%	kIx~	%
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
rozpuštěných	rozpuštěný	k2eAgFnPc2d1	rozpuštěná
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
tání	tání	k1gNnSc2	tání
(	(	kIx(	(
<g/>
bod	bod	k1gInSc1	bod
tuhnutí	tuhnutí	k1gNnSc1	tuhnutí
<g/>
)	)	kIx)	)
mořské	mořský	k2eAgFnPc1d1	mořská
vody	voda	k1gFnPc1	voda
se	se	k3xPyFc4	se
vlivem	vliv	k1gInSc7	vliv
salinity	salinita	k1gFnSc2	salinita
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
-1,8	-1,8	k4	-1,8
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
