<s>
Spalničky	spalničky	k1gFnPc1	spalničky
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Morbilli	Morbill	k1gMnSc5	Morbill
<g/>
,	,	kIx,	,
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
Measles	Measles	k1gInSc1	Measles
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
virové	virový	k2eAgNnSc1d1	virové
infekční	infekční	k2eAgNnSc1d1	infekční
onemocnění	onemocnění	k1gNnSc1	onemocnění
provázené	provázený	k2eAgNnSc1d1	provázené
charakteristickou	charakteristický	k2eAgFnSc7d1	charakteristická
vyrážkou	vyrážka	k1gFnSc7	vyrážka
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
tohoto	tento	k3xDgNnSc2	tento
onemocnění	onemocnění	k1gNnSc2	onemocnění
je	být	k5eAaImIp3nS	být
virus	virus	k1gInSc1	virus
spalniček	spalničky	k1gFnPc2	spalničky
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
přirozeným	přirozený	k2eAgMnSc7d1	přirozený
hostitelem	hostitel	k1gMnSc7	hostitel
je	být	k5eAaImIp3nS	být
výhradně	výhradně	k6eAd1	výhradně
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nákaza	nákaza	k1gFnSc1	nákaza
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
cestou	cesta	k1gFnSc7	cesta
-	-	kIx~	-
kapénkami	kapénka	k1gFnPc7	kapénka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
povinnému	povinný	k2eAgNnSc3d1	povinné
očkování	očkování	k1gNnSc3	očkování
vakcínou	vakcína	k1gFnSc7	vakcína
MMR	MMR	kA	MMR
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
minimální	minimální	k2eAgMnSc1d1	minimální
<g/>
,	,	kIx,	,
hlášeno	hlásit	k5eAaImNgNnS	hlásit
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
do	do	k7c2	do
dvaceti	dvacet	k4xCc2	dvacet
případů	případ	k1gInPc2	případ
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
dospělých	dospělý	k2eAgFnPc2d1	dospělá
nebo	nebo	k8xC	nebo
dospívajících	dospívající	k2eAgFnPc2d1	dospívající
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nebyly	být	k5eNaImAgFnP	být
očkovány	očkovat	k5eAaImNgFnP	očkovat
vůbec	vůbec	k9	vůbec
nebo	nebo	k8xC	nebo
jenom	jenom	k6eAd1	jenom
jednou	jeden	k4xCgFnSc7	jeden
dávkou	dávka	k1gFnSc7	dávka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
očkování	očkování	k1gNnSc1	očkování
není	být	k5eNaImIp3nS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
spalničky	spalničky	k1gFnPc1	spalničky
nebezpečným	bezpečný	k2eNgNnPc3d1	nebezpečné
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
ohrožujícím	ohrožující	k2eAgNnSc7d1	ohrožující
onemocněním	onemocnění	k1gNnSc7	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
pohledu	pohled	k1gInSc2	pohled
patří	patřit	k5eAaImIp3nP	patřit
toto	tento	k3xDgNnSc4	tento
onemocnění	onemocnění	k1gNnSc4	onemocnění
mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgFnPc4d3	nejčastější
příčiny	příčina	k1gFnPc4	příčina
úmrtí	úmrtí	k1gNnPc2	úmrtí
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
95	[number]	k4	95
<g/>
%	%	kIx~	%
úmrtí	úmrtí	k1gNnSc3	úmrtí
na	na	k7c4	na
spalničky	spalničky	k1gFnPc4	spalničky
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
HDP	HDP	kA	HDP
a	a	k8xC	a
špatnou	špatný	k2eAgFnSc7d1	špatná
zdravotní	zdravotní	k2eAgFnSc7d1	zdravotní
infrastrukturou	infrastruktura	k1gFnSc7	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Nesmí	smět	k5eNaImIp3nP	smět
být	být	k5eAaImF	být
zaměnovány	zaměnovat	k5eAaPmNgInP	zaměnovat
s	s	k7c7	s
pojmy	pojem	k1gInPc7	pojem
Rubella	Rubello	k1gNnSc2	Rubello
a	a	k8xC	a
Roseola	roseola	k1gFnSc1	roseola
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
viru	vir	k1gInSc2	vir
spalniček	spalničky	k1gFnPc2	spalničky
je	být	k5eAaImIp3nS	být
6-19	[number]	k4	6-19
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
průměrně	průměrně	k6eAd1	průměrně
13	[number]	k4	13
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
první	první	k4xOgInPc4	první
projevy	projev	k1gInPc4	projev
patří	patřit	k5eAaImIp3nP	patřit
kašel	kašel	k1gInSc4	kašel
<g/>
,	,	kIx,	,
zánět	zánět	k1gInSc4	zánět
spojivek	spojivka	k1gFnPc2	spojivka
a	a	k8xC	a
rýma	rýma	k1gFnSc1	rýma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
sliznici	sliznice	k1gFnSc4	sliznice
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
ústní	ústní	k2eAgFnPc1d1	ústní
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
charakteristické	charakteristický	k2eAgFnPc4d1	charakteristická
bílé	bílý	k2eAgFnPc4d1	bílá
skvrny	skvrna	k1gFnPc4	skvrna
na	na	k7c6	na
červeném	červený	k2eAgInSc6d1	červený
podkladě	podklad	k1gInSc6	podklad
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Koplikovy	Koplikův	k2eAgFnSc2d1	Koplikův
skvrny	skvrna	k1gFnSc2	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
makulopapulózní	makulopapulózní	k2eAgFnSc1d1	makulopapulózní
vyrážka	vyrážka	k1gFnSc1	vyrážka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
typicky	typicky	k6eAd1	typicky
za	za	k7c7	za
ušima	ucho	k1gNnPc7	ucho
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
odtud	odtud	k6eAd1	odtud
šíří	šířit	k5eAaImIp3nS	šířit
přes	přes	k7c4	přes
obličej	obličej	k1gInSc4	obličej
na	na	k7c4	na
celé	celý	k2eAgNnSc4d1	celé
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
špatně	špatně	k6eAd1	špatně
<g/>
;	;	kIx,	;
stoupající	stoupající	k2eAgFnSc1d1	stoupající
teplota	teplota	k1gFnSc1	teplota
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
<g/>
;	;	kIx,	;
6	[number]	k4	6
<g/>
.	.	kIx.	.
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
den	den	k1gInSc1	den
vyrážka	vyrážka	k1gFnSc1	vyrážka
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
<g/>
;	;	kIx,	;
9	[number]	k4	9
<g/>
.	.	kIx.	.
den	den	k1gInSc1	den
dítě	dítě	k1gNnSc1	dítě
není	být	k5eNaImIp3nS	být
infekční	infekční	k2eAgNnSc1d1	infekční
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Nemocný	nemocný	k1gMnSc1	nemocný
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
infekcí	infekce	k1gFnSc7	infekce
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
izolaci	izolace	k1gFnSc6	izolace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
spalničky	spalničky	k1gFnPc4	spalničky
není	být	k5eNaImIp3nS	být
žádný	žádný	k3yNgInSc1	žádný
specifický	specifický	k2eAgInSc1d1	specifický
lék	lék	k1gInSc1	lék
<g/>
.	.	kIx.	.
</s>
<s>
Léčí	léčit	k5eAaImIp3nS	léčit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
projevy	projev	k1gInPc1	projev
onemocnění	onemocnění	k1gNnSc1	onemocnění
-	-	kIx~	-
horečka	horečka	k1gFnSc1	horečka
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
nepříjemné	příjemný	k2eNgInPc1d1	nepříjemný
symptomy	symptom	k1gInPc1	symptom
<g/>
.	.	kIx.	.
</s>
<s>
Kontrola	kontrola	k1gFnSc1	kontrola
teploty	teplota	k1gFnSc2	teplota
alespoň	alespoň	k9	alespoň
dvakrát	dvakrát	k6eAd1	dvakrát
denně	denně	k6eAd1	denně
<g/>
;	;	kIx,	;
snižovat	snižovat	k5eAaImF	snižovat
teplotu	teplota	k1gFnSc4	teplota
-	-	kIx~	-
léky	lék	k1gInPc1	lék
<g/>
,	,	kIx,	,
zábaly	zábal	k1gInPc1	zábal
<g/>
,	,	kIx,	,
omývání	omývání	k1gNnPc1	omývání
<g/>
;	;	kIx,	;
dostatek	dostatek	k1gInSc1	dostatek
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
;	;	kIx,	;
jestliže	jestliže	k8xS	jestliže
dítě	dítě	k1gNnSc1	dítě
pálí	pálit	k5eAaImIp3nP	pálit
oči	oko	k1gNnPc4	oko
-	-	kIx~	-
omývat	omývat	k5eAaImF	omývat
kouskem	kousek	k1gInSc7	kousek
vaty	vata	k1gFnSc2	vata
namočeným	namočený	k2eAgInSc7d1	namočený
ve	v	k7c6	v
studené	studený	k2eAgFnSc6d1	studená
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
udělat	udělat	k5eAaPmF	udělat
pološero	pološero	k1gNnSc4	pološero
<g/>
;	;	kIx,	;
klid	klid	k1gInSc1	klid
na	na	k7c6	na
lůžku	lůžko	k1gNnSc6	lůžko
<g/>
;	;	kIx,	;
rekonvalescence	rekonvalescence	k1gFnSc1	rekonvalescence
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nemocných	nemocný	k1gMnPc2	nemocný
se	s	k7c7	s
sníženou	snížený	k2eAgFnSc7d1	snížená
obranyschopností	obranyschopnost	k1gFnSc7	obranyschopnost
lze	lze	k6eAd1	lze
podávat	podávat	k5eAaImF	podávat
ribavirin	ribavirin	k1gInSc4	ribavirin
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
onemocnění	onemocnění	k1gNnSc4	onemocnění
mohou	moct	k5eAaImIp3nP	moct
provázet	provázet	k5eAaImF	provázet
komplikace	komplikace	k1gFnPc4	komplikace
postihující	postihující	k2eAgFnSc4d1	postihující
dýchací	dýchací	k2eAgFnSc4d1	dýchací
a	a	k8xC	a
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
sekundární	sekundární	k2eAgFnSc2d1	sekundární
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
infekce	infekce	k1gFnSc2	infekce
<g/>
,	,	kIx,	,
tracheitis	tracheitis	k1gFnSc1	tracheitis
<g/>
,	,	kIx,	,
febrilní	febrilní	k2eAgFnPc1d1	febrilní
křeče	křeč	k1gFnPc1	křeč
<g/>
,	,	kIx,	,
abnormality	abnormalita	k1gFnPc1	abnormalita
na	na	k7c4	na
EEG	EEG	kA	EEG
<g/>
,	,	kIx,	,
encefalitida	encefalitida	k1gFnSc1	encefalitida
<g/>
,	,	kIx,	,
subakutní	subakutní	k2eAgFnSc1d1	subakutní
sklerotizující	sklerotizující	k2eAgFnSc1d1	sklerotizující
panencefalitida	panencefalitida	k1gFnSc1	panencefalitida
<g/>
,	,	kIx,	,
průjem	průjem	k1gInSc1	průjem
<g/>
,	,	kIx,	,
hepatitida	hepatitida	k1gFnSc1	hepatitida
<g/>
,	,	kIx,	,
apendicitida	apendicitida	k1gFnSc1	apendicitida
a	a	k8xC	a
myokarditida	myokarditida	k1gFnSc1	myokarditida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pravidelného	pravidelný	k2eAgNnSc2d1	pravidelné
očkování	očkování	k1gNnSc2	očkování
se	se	k3xPyFc4	se
podává	podávat	k5eAaImIp3nS	podávat
MMR	MMR	kA	MMR
vakcína	vakcína	k1gFnSc1	vakcína
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
oslabené	oslabený	k2eAgInPc4d1	oslabený
viry	vir	k1gInPc4	vir
spalniček	spalničky	k1gFnPc2	spalničky
<g/>
,	,	kIx,	,
příušnic	příušnice	k1gFnPc2	příušnice
a	a	k8xC	a
zarděnek	zarděnky	k1gFnPc2	zarděnky
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
dávka	dávka	k1gFnSc1	dávka
se	se	k3xPyFc4	se
podává	podávat	k5eAaImIp3nS	podávat
po	po	k7c6	po
patnáctém	patnáctý	k4xOgInSc6	patnáctý
měsíci	měsíc	k1gInSc6	měsíc
věku	věk	k1gInSc2	věk
dítěte	dítě	k1gNnSc2	dítě
a	a	k8xC	a
přeočkování	přeočkování	k1gNnSc2	přeočkování
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
za	za	k7c4	za
6	[number]	k4	6
až	až	k9	až
10	[number]	k4	10
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
provedeném	provedený	k2eAgNnSc6d1	provedené
základním	základní	k2eAgNnSc6d1	základní
očkování	očkování	k1gNnSc6	očkování
<g/>
.	.	kIx.	.
</s>
<s>
LISSAUER	LISSAUER	kA	LISSAUER
<g/>
,	,	kIx,	,
Tom	Tom	k1gMnSc1	Tom
<g/>
;	;	kIx,	;
CLAYDEN	CLAYDEN	kA	CLAYDEN
<g/>
,	,	kIx,	,
Graham	graham	k1gInSc1	graham
<g/>
.	.	kIx.	.
</s>
<s>
Illustrated	Illustrated	k1gInSc1	Illustrated
Textbook	Textbook	k1gInSc1	Textbook
of	of	k?	of
Paediatrics	Paediatrics	k1gInSc1	Paediatrics
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Spain	Spain	k1gMnSc1	Spain
:	:	kIx,	:
Elsevier	Elsevier	k1gMnSc1	Elsevier
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7234	[number]	k4	7234
<g/>
-	-	kIx~	-
<g/>
3398	[number]	k4	3398
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Zarděnky	zarděnky	k1gFnPc1	zarděnky
Příušnice	příušnice	k1gFnPc1	příušnice
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Spalničky	spalničky	k1gFnPc1	spalničky
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
