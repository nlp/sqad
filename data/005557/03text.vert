<s>
Závist	závist	k1gFnSc1	závist
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
invidia	invidium	k1gNnSc2	invidium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
lidských	lidský	k2eAgFnPc2d1	lidská
emocí	emoce	k1gFnPc2	emoce
<g/>
,	,	kIx,	,
spočívající	spočívající	k2eAgFnSc2d1	spočívající
v	v	k7c6	v
touze	touha	k1gFnSc6	touha
po	po	k7c6	po
něčem	něco	k3yInSc6	něco
<g/>
,	,	kIx,	,
co	co	k9	co
má	mít	k5eAaImIp3nS	mít
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
a	a	k8xC	a
snaze	snaha	k1gFnSc3	snaha
získat	získat	k5eAaPmF	získat
předmět	předmět	k1gInSc4	předmět
této	tento	k3xDgFnSc2	tento
touhy	touha	k1gFnSc2	touha
<g/>
,	,	kIx,	,
či	či	k8xC	či
druhého	druhý	k4xOgMnSc4	druhý
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
připravit	připravit	k5eAaPmF	připravit
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
zavrženíhodných	zavrženíhodný	k2eAgInPc2d1	zavrženíhodný
činů	čin	k1gInPc2	čin
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
žárlivostí	žárlivost	k1gFnSc7	žárlivost
či	či	k8xC	či
nenávistí	nenávist	k1gFnSc7	nenávist
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
konceptuální	konceptuální	k2eAgFnPc4d1	konceptuální
lidské	lidský	k2eAgFnPc4d1	lidská
emoce	emoce	k1gFnPc4	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
náboženského	náboženský	k2eAgNnSc2d1	náboženské
bývá	bývat	k5eAaImIp3nS	bývat
odsuzována	odsuzován	k2eAgFnSc1d1	odsuzována
jako	jako	k8xC	jako
hřích	hřích	k1gInSc4	hřích
<g/>
,	,	kIx,	,
např.	např.	kA	např.
katolická	katolický	k2eAgFnSc1d1	katolická
nauka	nauka	k1gFnSc1	nauka
ji	on	k3xPp3gFnSc4	on
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
hlavních	hlavní	k2eAgInPc2d1	hlavní
hříchů	hřích	k1gInPc2	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Opakem	opak	k1gInSc7	opak
závisti	závist	k1gFnSc2	závist
je	být	k5eAaImIp3nS	být
přejícnost	přejícnost	k1gFnSc1	přejícnost
<g/>
.	.	kIx.	.
</s>
<s>
Sebevztažné	Sebevztažný	k2eAgFnPc1d1	Sebevztažný
emoce	emoce	k1gFnPc1	emoce
Žárlivost	žárlivost	k1gFnSc1	žárlivost
Soutěž	soutěž	k1gFnSc1	soutěž
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Závist	závist	k1gFnSc1	závist
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
závist	závist	k1gFnSc1	závist
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Slovníkové	slovníkový	k2eAgFnSc2d1	slovníková
heslo	heslo	k1gNnSc1	heslo
závist	závist	k1gFnSc1	závist
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
