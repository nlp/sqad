<p>
<s>
Otáčkoměr	otáčkoměr	k1gInSc1	otáčkoměr
je	být	k5eAaImIp3nS	být
přístroj	přístroj	k1gInSc4	přístroj
sloužící	sloužící	k2eAgInSc4d1	sloužící
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
otáček	otáčka	k1gFnPc2	otáčka
motoru	motor	k1gInSc2	motor
či	či	k8xC	či
jiných	jiný	k2eAgNnPc2d1	jiné
rotačních	rotační	k2eAgNnPc2d1	rotační
těles	těleso	k1gNnPc2	těleso
a	a	k8xC	a
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
ukazatel	ukazatel	k1gInSc1	ukazatel
uvádí	uvádět	k5eAaImIp3nS	uvádět
počet	počet	k1gInSc4	počet
otáček	otáčka	k1gFnPc2	otáčka
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
na	na	k7c6	na
kalibrovaném	kalibrovaný	k2eAgInSc6d1	kalibrovaný
analogovém	analogový	k2eAgInSc6d1	analogový
či	či	k8xC	či
digitálním	digitální	k2eAgInSc6d1	digitální
displeji	displej	k1gInSc6	displej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
mechanické	mechanický	k2eAgInPc1d1	mechanický
otáčkoměry	otáčkoměr	k1gInPc1	otáčkoměr
byly	být	k5eAaImAgInP	být
založeny	založit	k5eAaPmNgInP	založit
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
odstředivé	odstředivý	k2eAgFnSc2d1	odstředivá
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
roztáčí	roztáčet	k5eAaImIp3nS	roztáčet
Wattův	Wattův	k2eAgInSc4d1	Wattův
odstředivý	odstředivý	k2eAgInSc4d1	odstředivý
regulátor	regulátor	k1gInSc4	regulátor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vynálezce	vynálezce	k1gMnSc4	vynálezce
otáčkoměru	otáčkoměr	k1gInSc2	otáčkoměr
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
německý	německý	k2eAgMnSc1d1	německý
inženýr	inženýr	k1gMnSc1	inženýr
Diedrich	Diedrich	k1gMnSc1	Diedrich
Uhlhorn	Uhlhorn	k1gMnSc1	Uhlhorn
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gMnSc4	on
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1817	[number]	k4	1817
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
k	k	k7c3	k
měření	měření	k1gNnSc3	měření
rychlosti	rychlost	k1gFnSc2	rychlost
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otáčkoměry	otáčkoměr	k1gInPc1	otáčkoměr
v	v	k7c6	v
automobilech	automobil	k1gInPc6	automobil
<g/>
,	,	kIx,	,
letadlech	letadlo	k1gNnPc6	letadlo
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc6d1	ostatní
dopravních	dopravní	k2eAgInPc6d1	dopravní
prostředcích	prostředek	k1gInPc6	prostředek
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
počet	počet	k1gInSc4	počet
otáček	otáčka	k1gFnPc2	otáčka
klikové	klikový	k2eAgFnSc2d1	kliková
hřídele	hřídel	k1gFnSc2	hřídel
motoru	motor	k1gInSc2	motor
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
mívají	mívat	k5eAaImIp3nP	mívat
tzv.	tzv.	kA	tzv.
červené	červený	k2eAgNnSc1d1	červené
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
na	na	k7c4	na
oblast	oblast	k1gFnSc4	oblast
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
otáček	otáčka	k1gFnPc2	otáčka
<g/>
.	.	kIx.	.
</s>
<s>
Řidič	řidič	k1gMnSc1	řidič
či	či	k8xC	či
pilot	pilot	k1gMnSc1	pilot
tak	tak	k6eAd1	tak
mohou	moct	k5eAaImIp3nP	moct
ubráním	ubrání	k1gNnSc7	ubrání
plynu	plyn	k1gInSc2	plyn
snížit	snížit	k5eAaPmF	snížit
otáčky	otáčka	k1gFnPc4	otáčka
motoru	motor	k1gInSc2	motor
a	a	k8xC	a
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
bezpečné	bezpečný	k2eAgFnSc2d1	bezpečná
zóny	zóna	k1gFnSc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
prodlévání	prodlévání	k1gNnSc4	prodlévání
v	v	k7c6	v
příliš	příliš	k6eAd1	příliš
vysokých	vysoký	k2eAgFnPc6d1	vysoká
otáčkách	otáčka	k1gFnPc6	otáčka
motorům	motor	k1gInPc3	motor
škodí	škodit	k5eAaImIp3nS	škodit
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
něm	on	k3xPp3gMnSc6	on
totiž	totiž	k9	totiž
k	k	k7c3	k
nedostatečnému	dostatečný	k2eNgNnSc3d1	nedostatečné
mazání	mazání	k1gNnSc3	mazání
motoru	motor	k1gInSc2	motor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
za	za	k7c4	za
následek	následek	k1gInSc4	následek
přehřívání	přehřívání	k1gNnSc2	přehřívání
jeho	jeho	k3xOp3gFnPc2	jeho
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
dojde	dojít	k5eAaPmIp3nS	dojít
vlivem	vliv	k1gInSc7	vliv
zvýšených	zvýšený	k2eAgFnPc2d1	zvýšená
teplot	teplota	k1gFnPc2	teplota
též	též	k9	též
k	k	k7c3	k
nedostatečnému	dostatečný	k2eNgNnSc3d1	nedostatečné
chlazení	chlazení	k1gNnSc3	chlazení
rotujících	rotující	k2eAgFnPc2d1	rotující
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
motor	motor	k1gInSc4	motor
i	i	k9	i
zadřít	zadřít	k5eAaPmF	zadřít
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
pokud	pokud	k8xS	pokud
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
zadření	zadření	k1gNnSc3	zadření
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
běh	běh	k1gInSc4	běh
motoru	motor	k1gInSc2	motor
v	v	k7c6	v
příliš	příliš	k6eAd1	příliš
vysokých	vysoký	k2eAgFnPc6d1	vysoká
otáčkách	otáčka	k1gFnPc6	otáčka
způsobit	způsobit	k5eAaPmF	způsobit
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
opotřebení	opotřebení	k1gNnSc4	opotřebení
součástek	součástka	k1gFnPc2	součástka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
si	se	k3xPyFc3	se
vyžádá	vyžádat	k5eAaPmIp3nS	vyžádat
neplánované	plánovaný	k2eNgFnPc4d1	neplánovaná
a	a	k8xC	a
finančně	finančně	k6eAd1	finančně
nákladné	nákladný	k2eAgFnPc4d1	nákladná
opravy	oprava	k1gFnPc4	oprava
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Tachometer	Tachometra	k1gFnPc2	Tachometra
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Tachometr	tachometr	k1gInSc1	tachometr
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Otáčkoměr	otáčkoměr	k1gInSc1	otáčkoměr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
otáčkoměr	otáčkoměr	k1gInSc1	otáčkoměr
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
