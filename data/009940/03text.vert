<p>
<s>
Andrzej	Andrzat	k5eAaPmRp2nS	Andrzat
Sebastian	Sebastian	k1gMnSc1	Sebastian
Duda	Duda	k1gMnSc1	Duda
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1972	[number]	k4	1972
Krakov	Krakov	k1gInSc1	Krakov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
polský	polský	k2eAgMnSc1d1	polský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2015	[number]	k4	2015
prezident	prezident	k1gMnSc1	prezident
Polské	polský	k2eAgFnSc2d1	polská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
květnových	květnový	k2eAgFnPc2d1	květnová
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
2015	[number]	k4	2015
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
nad	nad	k7c7	nad
úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
prezidentem	prezident	k1gMnSc7	prezident
Bronisławem	Bronisław	k1gMnSc7	Bronisław
Komorowskim	Komorowskima	k1gFnPc2	Komorowskima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
byl	být	k5eAaImAgMnS	být
poslancem	poslanec	k1gMnSc7	poslanec
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
zvolení	zvolení	k1gNnSc2	zvolení
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
strany	strana	k1gFnSc2	strana
Právo	právo	k1gNnSc4	právo
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Andrzej	Andrzat	k5eAaPmRp2nS	Andrzat
Duda	Duda	k1gMnSc1	Duda
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
Jana	Jan	k1gMnSc2	Jan
Tadeusze	Tadeusze	k1gFnSc2	Tadeusze
Dudy	Duda	k1gMnSc2	Duda
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
inženýra	inženýr	k1gMnSc2	inženýr
<g/>
,	,	kIx,	,
informatika	informatik	k1gMnSc2	informatik
a	a	k8xC	a
profesora	profesor	k1gMnSc2	profesor
technických	technický	k2eAgFnPc2d1	technická
věd	věda	k1gFnPc2	věda
na	na	k7c6	na
Hornicko-hutnické	hornickoutnický	k2eAgFnSc6d1	hornicko-hutnický
akademii	akademie	k1gFnSc6	akademie
Stanisława	Stanisław	k2eAgFnSc1d1	Stanisława
Staszica	Staszica	k1gFnSc1	Staszica
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Janiny	Janin	k2eAgFnSc2d1	Janina
Milewské-Dudové	Milewské-Dudová	k1gFnSc2	Milewské-Dudová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesorky	profesorka	k1gFnSc2	profesorka
chemie	chemie	k1gFnSc2	chemie
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
akademii	akademie	k1gFnSc6	akademie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1984	[number]	k4	1984
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
skautingu	skauting	k1gInSc3	skauting
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
dokončil	dokončit	k5eAaPmAgMnS	dokončit
studia	studio	k1gNnSc2	studio
práv	právo	k1gNnPc2	právo
na	na	k7c6	na
krakovské	krakovský	k2eAgFnSc6d1	Krakovská
Jagellonské	jagellonský	k2eAgFnSc6d1	Jagellonská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgMnS	začít
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
univerzitě	univerzita	k1gFnSc6	univerzita
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výuky	výuka	k1gFnSc2	výuka
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
technologie	technologie	k1gFnSc2	technologie
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
jako	jako	k8xS	jako
zdejší	zdejší	k2eAgMnSc1d1	zdejší
asistent	asistent	k1gMnSc1	asistent
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
středové	středový	k2eAgFnSc2d1	středová
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
Unia	Unium	k1gNnSc2	Unium
Wolności	Wolnośec	k1gInSc6	Wolnośec
(	(	kIx(	(
<g/>
Unie	unie	k1gFnSc1	unie
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
začal	začít	k5eAaPmAgMnS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
se	se	k3xPyFc4	se
stranou	stranou	k6eAd1	stranou
Právo	právo	k1gNnSc1	právo
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
a	a	k8xC	a
2007	[number]	k4	2007
pracoval	pracovat	k5eAaImAgInS	pracovat
na	na	k7c6	na
Ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
legislativu	legislativa	k1gFnSc4	legislativa
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
proces	proces	k1gInSc4	proces
informatizace	informatizace	k1gFnSc2	informatizace
soudů	soud	k1gInPc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
prezidentské	prezidentský	k2eAgFnSc3d1	prezidentská
kanceláři	kancelář	k1gFnSc3	kancelář
jako	jako	k8xC	jako
poradce	poradce	k1gMnSc2	poradce
Lecha	Lech	k1gMnSc2	Lech
Kaczyńského	Kaczyńský	k1gMnSc2	Kaczyńský
a	a	k8xC	a
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
odešel	odejít	k5eAaPmAgMnS	odejít
po	po	k7c4	po
zvolení	zvolení	k1gNnSc4	zvolení
nového	nový	k2eAgMnSc2d1	nový
prezidenta	prezident	k1gMnSc2	prezident
Bronisława	Bronisławus	k1gMnSc2	Bronisławus
Komorowského	Komorowský	k1gMnSc2	Komorowský
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
na	na	k7c4	na
starostu	starosta	k1gMnSc4	starosta
Krakova	Krakov	k1gInSc2	Krakov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jen	jen	k6eAd1	jen
do	do	k7c2	do
rady	rada	k1gFnSc2	rada
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
poslancem	poslanec	k1gMnSc7	poslanec
Sejmu	Sejm	k1gInSc2	Sejm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
poslancem	poslanec	k1gMnSc7	poslanec
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
Evropské	evropský	k2eAgMnPc4d1	evropský
konzervativce	konzervativec	k1gMnPc4	konzervativec
a	a	k8xC	a
reformisty	reformista	k1gMnPc4	reformista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
jej	on	k3xPp3gInSc4	on
strana	strana	k1gFnSc1	strana
Právo	právo	k1gNnSc1	právo
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
jako	jako	k9	jako
svého	svůj	k3xOyFgMnSc4	svůj
kandidáta	kandidát	k1gMnSc4	kandidát
do	do	k7c2	do
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
a	a	k8xC	a
Duda	Duda	k1gMnSc1	Duda
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
nad	nad	k7c7	nad
dosavadním	dosavadní	k2eAgMnSc7d1	dosavadní
prezidentem	prezident	k1gMnSc7	prezident
Bronisławem	Bronisław	k1gMnSc7	Bronisław
Komorowskim	Komorowskima	k1gFnPc2	Komorowskima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
získal	získat	k5eAaPmAgMnS	získat
34,76	[number]	k4	34,76
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
51,55	[number]	k4	51,55
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kampaně	kampaň	k1gFnSc2	kampaň
byl	být	k5eAaImAgInS	být
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
podpořil	podpořit	k5eAaPmAgInS	podpořit
návrh	návrh	k1gInSc4	návrh
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
trestní	trestní	k2eAgFnSc6d1	trestní
odpovědnosti	odpovědnost	k1gFnSc6	odpovědnost
pro	pro	k7c4	pro
lékaře	lékař	k1gMnPc4	lékař
<g/>
,	,	kIx,	,
provádějící	provádějící	k2eAgNnSc4d1	provádějící
oplodnění	oplodnění	k1gNnSc4	oplodnění
ve	v	k7c6	v
zkumavce	zkumavka	k1gFnSc6	zkumavka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
složení	složení	k1gNnSc6	složení
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
slibu	slib	k1gInSc2	slib
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
současným	současný	k2eAgMnSc7d1	současný
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
prezidentem	prezident	k1gMnSc7	prezident
evropského	evropský	k2eAgInSc2d1	evropský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soukromý	soukromý	k2eAgInSc4d1	soukromý
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Andrzej	Andrzat	k5eAaImRp2nS	Andrzat
Duda	Duda	k1gMnSc1	Duda
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
sourozence	sourozenec	k1gMnPc4	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Agatou	Agatý	k2eAgFnSc7d1	Agatý
Kornhauserovou	Kornhauserová	k1gFnSc7	Kornhauserová
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgFnSc7d2	pozdější
středoškolskou	středoškolský	k2eAgFnSc7d1	středoškolská
profesorkou	profesorka	k1gFnSc7	profesorka
němčiny	němčina	k1gFnSc2	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
manželství	manželství	k1gNnSc2	manželství
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Kinga	Kinga	k1gFnSc1	Kinga
Dudová	Dudová	k1gFnSc1	Dudová
<g/>
.	.	kIx.	.
</s>
<s>
Hlásí	hlásit	k5eAaImIp3nS	hlásit
se	se	k3xPyFc4	se
k	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gMnSc7	jeho
tchánem	tchán	k1gMnSc7	tchán
je	být	k5eAaImIp3nS	být
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
Julian	Julian	k1gMnSc1	Julian
Kornhauser	Kornhauser	k1gMnSc1	Kornhauser
(	(	kIx(	(
<g/>
*	*	kIx~	*
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
knih	kniha	k1gFnPc2	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
srbské	srbský	k2eAgFnSc2d1	Srbská
a	a	k8xC	a
chorvatské	chorvatský	k2eAgFnSc2d1	chorvatská
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
působí	působit	k5eAaImIp3nS	působit
také	také	k9	také
jako	jako	k9	jako
profesor	profesor	k1gMnSc1	profesor
na	na	k7c6	na
Jagellonské	jagellonský	k2eAgFnSc6d1	Jagellonská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Julian	Julian	k1gMnSc1	Julian
Kornhauser	Kornhauser	k1gMnSc1	Kornhauser
je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
otce	otec	k1gMnSc2	otec
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
matky	matka	k1gFnSc2	matka
katoličky	katolička	k1gFnSc2	katolička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Andrzej	Andrzej	k1gMnSc1	Andrzej
Duda	Duda	k1gMnSc1	Duda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Andrzej	Andrzej	k1gMnSc1	Andrzej
Duda	Duda	k1gMnSc1	Duda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Andrzej	Andrzat	k5eAaPmRp2nS	Andrzat
Duda	Duda	k1gMnSc1	Duda
–	–	k?	–
oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
</s>
</p>
