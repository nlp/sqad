<s>
Megadeth	Megadeth	k1gInSc1	Megadeth
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
heavy	heava	k1gFnSc2	heava
metalová	metalový	k2eAgFnSc1d1	metalová
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
založil	založit	k5eAaPmAgMnS	založit
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
kytarista	kytarista	k1gMnSc1	kytarista
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Dave	Dav	k1gInSc5	Dav
Mustaine	Mustain	k1gInSc5	Mustain
a	a	k8xC	a
basový	basový	k2eAgMnSc1d1	basový
kytarista	kytarista	k1gMnSc1	kytarista
Dave	Dav	k1gInSc5	Dav
Ellefson	Ellefson	k1gNnSc4	Ellefson
<g/>
.	.	kIx.	.
</s>
<s>
Mustaine	Mustainout	k5eAaPmIp3nS	Mustainout
chtěl	chtít	k5eAaImAgMnS	chtít
vytvořit	vytvořit	k5eAaPmF	vytvořit
rychlejší	rychlý	k2eAgFnSc4d2	rychlejší
a	a	k8xC	a
lepší	dobrý	k2eAgFnSc4d2	lepší
kapelu	kapela	k1gFnSc4	kapela
než	než	k8xS	než
Metallica	Metallic	k2eAgNnPc4d1	Metallic
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
byl	být	k5eAaImAgInS	být
vyhozen	vyhodit	k5eAaPmNgMnS	vyhodit
kvůli	kvůli	k7c3	kvůli
závažnému	závažný	k2eAgInSc3d1	závažný
alkoholismu	alkoholismus	k1gInSc3	alkoholismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
Get	Get	k1gMnSc1	Get
Trashed	Trashed	k1gMnSc1	Trashed
James	James	k1gMnSc1	James
Hetfield	Hetfield	k1gMnSc1	Hetfield
však	však	k9	však
jako	jako	k9	jako
skutečný	skutečný	k2eAgInSc1d1	skutečný
důvod	důvod	k1gInSc1	důvod
uvádí	uvádět	k5eAaImIp3nS	uvádět
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
nevraživost	nevraživost	k1gFnSc4	nevraživost
mezi	mezi	k7c7	mezi
Cliffem	Cliff	k1gInSc7	Cliff
Burtonem	Burton	k1gInSc7	Burton
a	a	k8xC	a
Davem	Dav	k1gInSc7	Dav
Mustainem	Mustaino	k1gNnSc7	Mustaino
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
kapely	kapela	k1gFnSc2	kapela
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
patnáct	patnáct	k4xCc1	patnáct
studiových	studiový	k2eAgFnPc2d1	studiová
alb	alba	k1gFnPc2	alba
<g/>
.	.	kIx.	.
</s>
<s>
Megadeth	Megadeth	k1gMnSc1	Megadeth
jsou	být	k5eAaImIp3nP	být
označování	označování	k1gNnSc4	označování
za	za	k7c4	za
pionýry	pionýr	k1gInPc4	pionýr
thrashmetalového	thrashmetalový	k2eAgNnSc2d1	thrashmetalové
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
"	"	kIx"	"
<g/>
velké	velký	k2eAgFnPc4d1	velká
thrashové	thrashová	k1gFnPc4	thrashová
čtyřky	čtyřka	k1gFnSc2	čtyřka
<g/>
"	"	kIx"	"
-	-	kIx~	-
skupinami	skupina	k1gFnPc7	skupina
Metallica	Metallica	k1gFnSc1	Metallica
<g/>
,	,	kIx,	,
Slayer	Slayer	k1gInSc1	Slayer
a	a	k8xC	a
Anthrax	Anthrax	k1gInSc1	Anthrax
<g/>
.	.	kIx.	.
</s>
<s>
Megadeth	Megadeth	k1gInSc1	Megadeth
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
díky	díky	k7c3	díky
textům	text	k1gInPc3	text
zabývajícím	zabývající	k2eAgInPc3d1	zabývající
se	se	k3xPyFc4	se
politikou	politika	k1gFnSc7	politika
<g/>
,	,	kIx,	,
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
závislostí	závislost	k1gFnSc7	závislost
<g/>
,	,	kIx,	,
vztahy	vztah	k1gInPc7	vztah
a	a	k8xC	a
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Sestava	sestava	k1gFnSc1	sestava
Megadeth	Megadetha	k1gFnPc2	Megadetha
se	se	k3xPyFc4	se
mnohokrát	mnohokrát	k6eAd1	mnohokrát
měnila	měnit	k5eAaImAgFnS	měnit
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgInSc1d1	jediný
Mustaine	Mustain	k1gInSc5	Mustain
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
kapely	kapela	k1gFnSc2	kapela
po	po	k7c4	po
celou	celá	k1gFnSc4	celá
její	její	k3xOp3gFnSc4	její
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nalezení	nalezení	k1gNnSc6	nalezení
stabilního	stabilní	k2eAgNnSc2d1	stabilní
obsazení	obsazení	k1gNnSc2	obsazení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
Megadeth	Megadetha	k1gFnPc2	Megadetha
nahráli	nahrát	k5eAaBmAgMnP	nahrát
zlatá	zlatý	k2eAgNnPc4d1	Zlaté
a	a	k8xC	a
platinová	platinový	k2eAgNnPc4d1	platinové
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Rust	Rust	k1gMnSc1	Rust
in	in	k?	in
Peace	Peace	k1gMnSc1	Peace
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
či	či	k8xC	či
na	na	k7c4	na
Grammy	Gramma	k1gFnPc4	Gramma
nominované	nominovaný	k2eAgFnSc2d1	nominovaná
multi-platinové	multilatinový	k2eAgFnSc2d1	multi-platinový
Countdown	Countdown	k1gMnSc1	Countdown
to	ten	k3xDgNnSc1	ten
Extinction	Extinction	k1gInSc1	Extinction
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
ukončila	ukončit	k5eAaPmAgFnS	ukončit
činnost	činnost	k1gFnSc1	činnost
kvůli	kvůli	k7c3	kvůli
zranění	zranění	k1gNnSc3	zranění
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
Dave	Dav	k1gInSc5	Dav
Mustaine	Mustain	k1gMnSc5	Mustain
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
terapie	terapie	k1gFnSc2	terapie
Mustaine	Mustain	k1gInSc5	Mustain
kapelu	kapela	k1gFnSc4	kapela
znovu	znovu	k6eAd1	znovu
oživil	oživit	k5eAaPmAgMnS	oživit
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
The	The	k1gFnSc4	The
System	Syst	k1gInSc7	Syst
Has	hasit	k5eAaImRp2nS	hasit
Failed	Failed	k1gInSc4	Failed
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
United	United	k1gInSc1	United
Abominations	Abominations	k1gInSc4	Abominations
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
kytaristou	kytarista	k1gMnSc7	kytarista
Chrisem	Chris	k1gInSc7	Chris
Broderickem	Broderick	k1gInSc7	Broderick
<g/>
,	,	kIx,	,
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
album	album	k1gNnSc4	album
Endgame	Endgam	k1gInSc5	Endgam
<g/>
,	,	kIx,	,
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
Thirteen	Thirtena	k1gFnPc2	Thirtena
a	a	k8xC	a
za	za	k7c4	za
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
pak	pak	k6eAd1	pak
Super	super	k2eAgInSc1d1	super
Collider	Collider	k1gInSc1	Collider
<g/>
.	.	kIx.	.
</s>
<s>
Technickým	technický	k2eAgInSc7d1	technický
vrcholem	vrchol	k1gInSc7	vrchol
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
všeobecně	všeobecně	k6eAd1	všeobecně
nejuzávanějším	uzávaný	k2eAgNnSc7d3	uzávaný
albem	album	k1gNnSc7	album
kapely	kapela	k1gFnSc2	kapela
je	být	k5eAaImIp3nS	být
Rust	Rust	k1gMnSc1	Rust
in	in	k?	in
Peace	Peace	k1gMnSc1	Peace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
objevil	objevit	k5eAaPmAgMnS	objevit
Mustaine	Mustain	k1gInSc5	Mustain
bez	bez	k7c2	bez
drogové	drogový	k2eAgFnSc2d1	drogová
závislosti	závislost	k1gFnSc2	závislost
a	a	k8xC	a
talentovaný	talentovaný	k2eAgMnSc1d1	talentovaný
kytarový	kytarový	k2eAgMnSc1d1	kytarový
virtuos	virtuos	k1gMnSc1	virtuos
Marty	Marta	k1gFnSc2	Marta
Friedman	Friedman	k1gMnSc1	Friedman
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Mustaine	Mustain	k1gInSc5	Mustain
slavnou	slavný	k2eAgFnSc4d1	slavná
kytarovou	kytarový	k2eAgFnSc4d1	kytarová
dvojici	dvojice	k1gFnSc4	dvojice
<g/>
.	.	kIx.	.
</s>
<s>
Friedman	Friedman	k1gMnSc1	Friedman
vydržel	vydržet	k5eAaPmAgMnS	vydržet
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
odešli	odejít	k5eAaPmAgMnP	odejít
dva	dva	k4xCgInPc4	dva
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
Shawn	Shawn	k1gMnSc1	Shawn
Drover	Drover	k1gMnSc1	Drover
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
Chris	Chris	k1gFnSc2	Chris
Broderick	Broderick	k1gMnSc1	Broderick
<g/>
.	.	kIx.	.
</s>
<s>
Megadeth	Megadeth	k1gMnSc1	Megadeth
mění	měnit	k5eAaImIp3nS	měnit
sestavy	sestava	k1gFnPc4	sestava
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
členů	člen	k1gInPc2	člen
Megadeth	Megadetha	k1gFnPc2	Megadetha
<g/>
.	.	kIx.	.
</s>
<s>
Dave	Dav	k1gInSc5	Dav
Mustaine	Mustain	k1gInSc5	Mustain
-	-	kIx~	-
hlavní	hlavní	k2eAgInSc1d1	hlavní
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
Dave	Dav	k1gInSc5	Dav
Ellefson	Ellefson	k1gNnSc1	Ellefson
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
Kiko	Kiko	k6eAd1	Kiko
Loureiro	Loureiro	k1gNnSc1	Loureiro
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
-současnost	oučasnost	k1gFnSc1	-současnost
<g/>
)	)	kIx)	)
Dirk	Dirk	k1gInSc1	Dirk
Verbeuren	Verbeurna	k1gFnPc2	Verbeurna
-	-	kIx~	-
bicí	bicí	k2eAgInPc1d1	bicí
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnPc1	perkuse
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
-současnost	oučasnost	k1gFnSc4	-současnost
<g/>
)	)	kIx)	)
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
Megadeth	Megadetha	k1gFnPc2	Megadetha
<g/>
.	.	kIx.	.
</s>
<s>
Killing	Killing	k1gInSc1	Killing
Is	Is	k1gFnSc2	Is
My	my	k3xPp1nPc1	my
Business	business	k1gInSc4	business
<g/>
...	...	k?	...
And	Anda	k1gFnPc2	Anda
Business	business	k1gInSc1	business
Is	Is	k1gMnSc1	Is
Good	Good	k1gMnSc1	Good
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Peace	Peaec	k1gInSc2	Peaec
Sells	Sells	k1gInSc1	Sells
<g/>
...	...	k?	...
But	But	k1gMnSc1	But
Who	Who	k1gMnSc1	Who
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Buying	Buying	k1gInSc1	Buying
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
So	So	kA	So
Far	fara	k1gFnPc2	fara
<g/>
,	,	kIx,	,
So	So	kA	So
Good	Good	k1gInSc1	Good
<g/>
...	...	k?	...
So	So	kA	So
What	What	k1gInSc1	What
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Rust	Rust	k1gMnSc1	Rust
in	in	k?	in
Peace	Peace	k1gMnSc1	Peace
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Countdown	Countdown	k1gMnSc1	Countdown
to	ten	k3xDgNnSc4	ten
Extinction	Extinction	k1gInSc1	Extinction
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Youthanasia	Youthanasium	k1gNnSc2	Youthanasium
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Cryptic	Cryptice	k1gFnPc2	Cryptice
Writings	Writings	k1gInSc1	Writings
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Risk	risk	k1gInSc1	risk
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
World	World	k1gMnSc1	World
Needs	Needs	k1gInSc1	Needs
a	a	k8xC	a
Hero	Hero	k1gNnSc1	Hero
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
The	The	k?	The
System	Syst	k1gInSc7	Syst
Has	hasit	k5eAaImRp2nS	hasit
Failed	Failed	k1gMnSc1	Failed
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
United	United	k1gInSc1	United
Abominations	Abominations	k1gInSc1	Abominations
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Endgame	Endgam	k1gInSc5	Endgam
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Thirteen	Thirtena	k1gFnPc2	Thirtena
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Super	super	k2eAgInSc1d1	super
Collider	Collider	k1gInSc1	Collider
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Dystopia	Dystopium	k1gNnSc2	Dystopium
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Megadeth	Megadetha	k1gFnPc2	Megadetha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
kapely	kapela	k1gFnSc2	kapela
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
)	)	kIx)	)
</s>
