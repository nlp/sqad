<p>
<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
(	(	kIx(	(
<g/>
KSČM	KSČM	kA	KSČM
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
komunistická	komunistický	k2eAgFnSc1d1	komunistická
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
KSČM	KSČM	kA	KSČM
v	v	k7c6	v
Evropském	evropský	k2eAgInSc6d1	evropský
parlamentu	parlament	k1gInSc6	parlament
spolutvoří	spolutvořit	k5eAaImIp3nS	spolutvořit
seskupení	seskupení	k1gNnSc1	seskupení
Evropské	evropský	k2eAgFnSc2d1	Evropská
sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
levice	levice	k1gFnSc2	levice
a	a	k8xC	a
Severské	severský	k2eAgFnPc1d1	severská
zelené	zelený	k2eAgFnPc1d1	zelená
levice	levice	k1gFnPc1	levice
<g/>
.	.	kIx.	.
</s>
<s>
KSČM	KSČM	kA	KSČM
je	být	k5eAaImIp3nS	být
také	také	k9	také
pozorovatelem	pozorovatel	k1gMnSc7	pozorovatel
ve	v	k7c6	v
Straně	strana	k1gFnSc6	strana
evropské	evropský	k2eAgFnSc2d1	Evropská
levice	levice	k1gFnSc2	levice
<g/>
.	.	kIx.	.
<g/>
Některá	některý	k3yIgNnPc1	některý
média	médium	k1gNnPc1	médium
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
strana	strana	k1gFnSc1	strana
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
nástupce	nástupce	k1gMnSc4	nástupce
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
rozdělení	rozdělení	k1gNnSc6	rozdělení
na	na	k7c4	na
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
KSS	KSS	kA	KSS
a	a	k8xC	a
českou	český	k2eAgFnSc7d1	Česká
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
před	před	k7c7	před
rozdělením	rozdělení	k1gNnSc7	rozdělení
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Československa	Československo	k1gNnSc2	Československo
byla	být	k5eAaImAgFnS	být
dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
protiprávnosti	protiprávnost	k1gFnSc6	protiprávnost
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
o	o	k7c6	o
odporu	odpor	k1gInSc6	odpor
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
organizací	organizace	k1gFnPc2	organizace
zločinnou	zločinný	k2eAgFnSc4d1	zločinná
a	a	k8xC	a
zavrženíhodnou	zavrženíhodný	k2eAgFnSc4d1	zavrženíhodná
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
KSČM	KSČM	kA	KSČM
se	se	k3xPyFc4	se
od	od	k7c2	od
činnosti	činnost	k1gFnSc2	činnost
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
několikrát	několikrát	k6eAd1	několikrát
zásadně	zásadně	k6eAd1	zásadně
distancovala	distancovat	k5eAaBmAgFnS	distancovat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	s	k7c7	s
KSČM	KSČM	kA	KSČM
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
sociálnímu	sociální	k2eAgInSc3d1	sociální
liberalismu	liberalismus	k1gInSc3	liberalismus
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Symbolika	symbolika	k1gFnSc1	symbolika
strany	strana	k1gFnSc2	strana
==	==	k?	==
</s>
</p>
<p>
<s>
Logem	log	k1gInSc7	log
a	a	k8xC	a
volebním	volební	k2eAgInSc7d1	volební
znakem	znak	k1gInSc7	znak
KSČM	KSČM	kA	KSČM
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
červené	červený	k2eAgFnPc1d1	červená
třešně	třešeň	k1gFnPc1	třešeň
se	s	k7c7	s
zeleným	zelený	k2eAgInSc7d1	zelený
lístkem	lístek	k1gInSc7	lístek
a	a	k8xC	a
zkratka	zkratka	k1gFnSc1	zkratka
názvu	název	k1gInSc2	název
strany	strana	k1gFnSc2	strana
s	s	k7c7	s
rudou	rudý	k2eAgFnSc7d1	rudá
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Symbolika	symbolika	k1gFnSc1	symbolika
třešní	třešeň	k1gFnPc2	třešeň
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
v	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
lidovém	lidový	k2eAgNnSc6d1	lidové
povstání	povstání	k1gNnSc6	povstání
proti	proti	k7c3	proti
Thiersově	Thiersův	k2eAgFnSc3d1	Thiersova
měšťácké	měšťácký	k2eAgFnSc3d1	měšťácká
vládě	vláda	k1gFnSc3	vláda
po	po	k7c6	po
prohrané	prohraný	k2eAgFnSc6d1	prohraná
francouzsko-pruské	francouzskoruský	k2eAgFnSc6d1	francouzsko-pruská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Povstalci	povstalec	k1gMnPc1	povstalec
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
za	za	k7c2	za
zpěvu	zpěv	k1gInSc2	zpěv
Marseillaisy	Marseillaisa	k1gFnSc2	Marseillaisa
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1871	[number]	k4	1871
Pařížskou	pařížský	k2eAgFnSc4d1	Pařížská
komunu	komuna	k1gFnSc4	komuna
a	a	k8xC	a
vytrvali	vytrvat	k5eAaPmAgMnP	vytrvat
v	v	k7c6	v
odporu	odpor	k1gInSc6	odpor
proti	proti	k7c3	proti
Thiersově	Thiersův	k2eAgFnSc3d1	Thiersova
vládě	vláda	k1gFnSc3	vláda
až	až	k6eAd1	až
do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
bojovníci	bojovník	k1gMnPc1	bojovník
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
komuny	komuna	k1gFnSc2	komuna
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
v	v	k7c6	v
boji	boj	k1gInSc6	boj
na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
hřbitově	hřbitov	k1gInSc6	hřbitov
Pè	Pè	k1gFnSc2	Pè
Lachaise	Lachaise	k1gFnSc2	Lachaise
<g/>
,	,	kIx,	,
na	na	k7c6	na
výšině	výšina	k1gFnSc6	výšina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nachází	nacházet	k5eAaImIp3nS	nacházet
Zeď	zeď	k1gFnSc1	zeď
komunardů	komunard	k1gMnPc2	komunard
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
hrob	hrob	k1gInSc1	hrob
Jeana	Jean	k1gMnSc2	Jean
Baptisty	baptista	k1gMnSc2	baptista
Clémenta	Clément	k1gMnSc2	Clément
<g/>
,	,	kIx,	,
autora	autor	k1gMnSc2	autor
poémy	poéma	k1gFnSc2	poéma
Čas	čas	k1gInSc1	čas
třešní	třešeň	k1gFnPc2	třešeň
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Le	Le	k1gFnSc4	Le
Temps	Temps	k1gInSc1	Temps
des	des	k1gNnSc2	des
Cerises	Cerisesa	k1gFnPc2	Cerisesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
básník	básník	k1gMnSc1	básník
věnoval	věnovat	k5eAaPmAgMnS	věnovat
své	svůj	k3xOyFgFnSc3	svůj
lásce	láska	k1gFnSc3	láska
Luise	Luisa	k1gFnSc3	Luisa
<g/>
,	,	kIx,	,
ošetřovatelce	ošetřovatelka	k1gFnSc3	ošetřovatelka
raněných	raněný	k2eAgInPc2d1	raněný
na	na	k7c6	na
barikádě	barikáda	k1gFnSc6	barikáda
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Fontaine-Au-Roi	Fontaine-Au-Ro	k1gFnSc2	Fontaine-Au-Ro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Československa	Československo	k1gNnSc2	Československo
===	===	k?	===
</s>
</p>
<p>
<s>
KSČ	KSČ	kA	KSČ
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
14	[number]	k4	14
<g/>
.	.	kIx.	.
až	až	k9	až
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1921	[number]	k4	1921
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ideologického	ideologický	k2eAgInSc2d1	ideologický
konfliktu	konflikt	k1gInSc2	konflikt
uvnitř	uvnitř	k7c2	uvnitř
Československé	československý	k2eAgFnSc2d1	Československá
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
i	i	k8xC	i
odštěpení	odštěpení	k1gNnSc2	odštěpení
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc3	její
větší	veliký	k2eAgFnSc3d2	veliký
části	část	k1gFnSc3	část
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
Prosincová	prosincový	k2eAgFnSc1d1	prosincová
generální	generální	k2eAgFnSc1d1	generální
stávka	stávka	k1gFnSc1	stávka
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
prvním	první	k4xOgMnSc7	první
hlavním	hlavní	k2eAgMnSc7d1	hlavní
řečníkem	řečník	k1gMnSc7	řečník
a	a	k8xC	a
poté	poté	k6eAd1	poté
vůdčím	vůdčí	k2eAgMnSc7d1	vůdčí
představitelem	představitel	k1gMnSc7	představitel
byl	být	k5eAaImAgMnS	být
Bohumír	Bohumír	k1gMnSc1	Bohumír
Šmeral	Šmeral	k1gMnSc1	Šmeral
<g/>
.	.	kIx.	.
</s>
<s>
Programem	program	k1gInSc7	program
jejího	její	k3xOp3gMnSc2	její
zakladatele	zakladatel	k1gMnSc2	zakladatel
byla	být	k5eAaImAgFnS	být
koaliční	koaliční	k2eAgFnSc1d1	koaliční
vláda	vláda	k1gFnSc1	vláda
socialistických	socialistický	k2eAgFnPc2d1	socialistická
stran	strana	k1gFnPc2	strana
s	s	k7c7	s
levicí	levice	k1gFnSc7	levice
agrárníků	agrárník	k1gMnPc2	agrárník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
absolutních	absolutní	k2eAgFnPc6d1	absolutní
i	i	k8xC	i
relativních	relativní	k2eAgFnPc6d1	relativní
číslech	číslo	k1gNnPc6	číslo
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
komunistických	komunistický	k2eAgFnPc2d1	komunistická
stran	strana	k1gFnPc2	strana
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
proces	proces	k1gInSc1	proces
"	"	kIx"	"
<g/>
bolševizace	bolševizace	k1gFnSc1	bolševizace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
utužení	utužení	k1gNnSc1	utužení
programové	programový	k2eAgFnSc2d1	programová
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
Komunistické	komunistický	k2eAgFnSc6d1	komunistická
internacionále	internacionála	k1gFnSc6	internacionála
(	(	kIx(	(
<g/>
KI	KI	kA	KI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
V.	V.	kA	V.
sjezdu	sjezd	k1gInSc2	sjezd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
se	se	k3xPyFc4	se
jejího	její	k3xOp3gNnSc2	její
vedení	vedení	k1gNnSc2	vedení
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
karlínští	karlínský	k2eAgMnPc1d1	karlínský
kluci	kluk	k1gMnPc1	kluk
<g/>
"	"	kIx"	"
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Klementem	Klement	k1gMnSc7	Klement
Gottwaldem	Gottwald	k1gMnSc7	Gottwald
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
KSČ	KSČ	kA	KSČ
odešla	odejít	k5eAaPmAgFnS	odejít
většina	většina	k1gFnSc1	většina
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
volbách	volba	k1gFnPc6	volba
ji	on	k3xPp3gFnSc4	on
opustila	opustit	k5eAaPmAgFnS	opustit
i	i	k9	i
většina	většina	k1gFnSc1	většina
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
provedl	provést	k5eAaPmAgMnS	provést
Gottwald	Gottwald	k1gMnSc1	Gottwald
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
KSČ	KSČ	kA	KSČ
řadu	řada	k1gFnSc4	řada
změn	změna	k1gFnPc2	změna
podle	podle	k7c2	podle
změn	změna	k1gFnPc2	změna
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
politiky	politika	k1gFnSc2	politika
lidové	lidový	k2eAgFnSc2d1	lidová
fronty	fronta	k1gFnSc2	fronta
a	a	k8xC	a
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
fašismu	fašismus	k1gInSc3	fašismus
určené	určený	k2eAgNnSc1d1	určené
na	na	k7c6	na
VII	VII	kA	VII
<g/>
.	.	kIx.	.
kongresu	kongres	k1gInSc6	kongres
Kominterny	Kominterna	k1gFnSc2	Kominterna
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
a	a	k8xC	a
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
hlavním	hlavní	k2eAgMnPc3d1	hlavní
představitelům	představitel	k1gMnPc3	představitel
opozice	opozice	k1gFnSc2	opozice
proti	proti	k7c3	proti
přijetí	přijetí	k1gNnSc3	přijetí
Mnichovského	mnichovský	k2eAgInSc2d1	mnichovský
diktátu	diktát	k1gInSc2	diktát
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
KSČ	KSČ	kA	KSČ
stala	stát	k5eAaPmAgFnS	stát
významnou	významný	k2eAgFnSc7d1	významná
silou	síla	k1gFnSc7	síla
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
domácího	domácí	k2eAgInSc2d1	domácí
i	i	k8xC	i
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
protinacistického	protinacistický	k2eAgInSc2d1	protinacistický
odboje	odboj	k1gInSc2	odboj
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
po	po	k7c6	po
přepadení	přepadení	k1gNnSc6	přepadení
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
Německem	Německo	k1gNnSc7	Německo
zapojila	zapojit	k5eAaPmAgFnS	zapojit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
získali	získat	k5eAaPmAgMnP	získat
komunisté	komunista	k1gMnPc1	komunista
na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
rozhodující	rozhodující	k2eAgNnSc1d1	rozhodující
slovo	slovo	k1gNnSc1	slovo
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
v	v	k7c6	v
exilovém	exilový	k2eAgInSc6d1	exilový
i	i	k8xC	i
domácím	domácí	k2eAgInSc6d1	domácí
odboji	odboj	k1gInSc6	odboj
a	a	k8xC	a
prosadili	prosadit	k5eAaPmAgMnP	prosadit
obnovení	obnovení	k1gNnSc4	obnovení
poválečného	poválečný	k2eAgNnSc2d1	poválečné
Československa	Československo	k1gNnSc2	Československo
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
30	[number]	k4	30
000	[number]	k4	000
československých	československý	k2eAgMnPc2d1	československý
komunistů	komunista	k1gMnPc2	komunista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Převzetí	převzetí	k1gNnSc1	převzetí
moci	moc	k1gFnSc2	moc
KSČ	KSČ	kA	KSČ
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
vítězství	vítězství	k1gNnSc1	vítězství
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získala	získat	k5eAaPmAgFnS	získat
40	[number]	k4	40
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
v	v	k7c6	v
celostátním	celostátní	k2eAgNnSc6d1	celostátní
měřítku	měřítko	k1gNnSc6	měřítko
pak	pak	k6eAd1	pak
komunisté	komunista	k1gMnPc1	komunista
získali	získat	k5eAaPmAgMnP	získat
38	[number]	k4	38
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Převzít	převzít	k5eAaPmF	převzít
moc	moc	k6eAd1	moc
se	s	k7c7	s
KSČ	KSČ	kA	KSČ
podařilo	podařit	k5eAaPmAgNnS	podařit
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1948	[number]	k4	1948
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Vítězný	vítězný	k2eAgInSc1d1	vítězný
únor	únor	k1gInSc1	únor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
přijal	přijmout	k5eAaPmAgMnS	přijmout
veškeré	veškerý	k3xTgInPc4	veškerý
komunistické	komunistický	k2eAgInPc4d1	komunistický
požadavky	požadavek	k1gInPc4	požadavek
a	a	k8xC	a
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1948	[number]	k4	1948
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
vládě	vláda	k1gFnSc3	vláda
Klementa	Klement	k1gMnSc2	Klement
Gottwalda	Gottwald	k1gMnSc2	Gottwald
důvěru	důvěra	k1gFnSc4	důvěra
všemi	všecek	k3xTgFnPc7	všecek
hlasy	hlas	k1gInPc4	hlas
230	[number]	k4	230
přítomných	přítomný	k2eAgMnPc2d1	přítomný
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
"	"	kIx"	"
<g/>
třídního	třídní	k2eAgInSc2d1	třídní
boje	boj	k1gInSc2	boj
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
období	období	k1gNnSc6	období
"	"	kIx"	"
<g/>
rudého	rudý	k2eAgInSc2d1	rudý
teroru	teror	k1gInSc2	teror
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
popraveno	popravit	k5eAaPmNgNnS	popravit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
soudních	soudní	k2eAgInPc2d1	soudní
výnosů	výnos	k1gInPc2	výnos
241	[number]	k4	241
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
komunistů	komunista	k1gMnPc2	komunista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
stovky	stovka	k1gFnPc1	stovka
byly	být	k5eAaImAgFnP	být
povražděny	povražděn	k2eAgFnPc1d1	povražděna
nebo	nebo	k8xC	nebo
zemřely	zemřít	k5eAaPmAgFnP	zemřít
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
,	,	kIx,	,
desetitisíce	desetitisíce	k1gInPc1	desetitisíce
byly	být	k5eAaImAgInP	být
vězněny	věznit	k5eAaImNgInP	věznit
nebo	nebo	k8xC	nebo
uprchly	uprchnout	k5eAaPmAgFnP	uprchnout
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
často	často	k6eAd1	často
jen	jen	k9	jen
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
třídní	třídní	k2eAgInSc1d1	třídní
boj	boj	k1gInSc1	boj
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
komunistů	komunista	k1gMnPc2	komunista
běžné	běžný	k2eAgFnSc2d1	běžná
i	i	k8xC	i
o	o	k7c4	o
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
boj	boj	k1gInSc4	boj
konkurenční	konkurenční	k2eAgFnSc2d1	konkurenční
–	–	k?	–
např.	např.	kA	např.
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1952	[number]	k4	1952
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
vykonstruovaném	vykonstruovaný	k2eAgInSc6d1	vykonstruovaný
procesu	proces	k1gInSc6	proces
popraven	popraven	k2eAgInSc4d1	popraven
oběšením	oběšení	k1gNnSc7	oběšení
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
Rudolf	Rudolf	k1gMnSc1	Rudolf
Slánský	Slánský	k1gMnSc1	Slánský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Stalina	Stalin	k1gMnSc4	Stalin
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obecně	obecně	k6eAd1	obecně
výraznému	výrazný	k2eAgNnSc3d1	výrazné
zmírnění	zmírnění	k1gNnSc3	zmírnění
<g/>
.	.	kIx.	.
</s>
<s>
Reforma	reforma	k1gFnSc1	reforma
KSČ	KSČ	kA	KSČ
začala	začít	k5eAaPmAgFnS	začít
až	až	k9	až
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
za	za	k7c2	za
pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Obrodný	obrodný	k2eAgInSc1d1	obrodný
proces	proces	k1gInSc1	proces
byl	být	k5eAaImAgInS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
invazí	invaze	k1gFnSc7	invaze
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
KSČ	KSČ	kA	KSČ
vydala	vydat	k5eAaPmAgFnS	vydat
Provolání	provolání	k1gNnSc4	provolání
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
z	z	k7c2	z
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
1969	[number]	k4	1969
bylo	být	k5eAaImAgNnS	být
reformistické	reformistický	k2eAgNnSc1d1	reformistické
vedení	vedení	k1gNnSc1	vedení
nahrazeno	nahrazen	k2eAgNnSc1d1	nahrazeno
centristy	centrista	k1gMnPc7	centrista
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Gustávem	Gustáv	k1gMnSc7	Gustáv
Husákem	Husák	k1gMnSc7	Husák
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
neostalinských	neostalinský	k2eAgMnPc2d1	neostalinský
ideologů	ideolog	k1gMnPc2	ideolog
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgMnS	být
Vasil	Vasil	k1gMnSc1	Vasil
Biľak	Biľak	k1gMnSc1	Biľak
<g/>
,	,	kIx,	,
posílili	posílit	k5eAaPmAgMnP	posílit
dogmatickou	dogmatický	k2eAgFnSc4d1	dogmatická
orientaci	orientace	k1gFnSc4	orientace
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
umírněnějšího	umírněný	k2eAgInSc2d2	umírněnější
stalinismu	stalinismus	k1gInSc2	stalinismus
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
okupace	okupace	k1gFnSc1	okupace
byla	být	k5eAaImAgFnS	být
vládními	vládní	k2eAgFnPc7d1	vládní
komunisty	komunista	k1gMnPc4	komunista
prezentována	prezentován	k2eAgFnSc1d1	prezentována
jako	jako	k8xC	jako
bratrská	bratrský	k2eAgFnSc1d1	bratrská
a	a	k8xC	a
internacionální	internacionální	k2eAgFnSc1d1	internacionální
pomoc	pomoc	k1gFnSc1	pomoc
a	a	k8xC	a
obrodný	obrodný	k2eAgInSc1d1	obrodný
proces	proces	k1gInSc1	proces
jako	jako	k8xS	jako
kontrarevoluce	kontrarevoluce	k1gFnSc1	kontrarevoluce
<g/>
.	.	kIx.	.
</s>
<s>
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČ	KSČ	kA	KSČ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
již	již	k6eAd1	již
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
odvrácení	odvrácení	k1gNnSc4	odvrácení
dosud	dosud	k6eAd1	dosud
nejvážnějšího	vážní	k2eAgNnSc2d3	nejvážnější
ohrožení	ohrožení	k1gNnSc2	ohrožení
mocenských	mocenský	k2eAgFnPc2d1	mocenská
pozic	pozice	k1gFnPc2	pozice
komunistů	komunista	k1gMnPc2	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
bylo	být	k5eAaImAgNnS	být
odstranění	odstranění	k1gNnSc1	odstranění
většiny	většina	k1gFnSc2	většina
svobod	svoboda	k1gFnPc2	svoboda
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
stranických	stranický	k2eAgFnPc2d1	stranická
čistek	čistka	k1gFnPc2	čistka
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
členů	člen	k1gInPc2	člen
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
či	či	k8xC	či
vyškrtnuta	vyškrtnout	k5eAaPmNgFnS	vyškrtnout
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
počátky	počátek	k1gInPc1	počátek
KSČM	KSČM	kA	KSČM
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Mimořádném	mimořádný	k2eAgInSc6d1	mimořádný
sjezdu	sjezd	k1gInSc6	sjezd
KSČ	KSČ	kA	KSČ
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
proces	proces	k1gInSc1	proces
symetrické	symetrický	k2eAgFnSc2d1	symetrická
federalizace	federalizace	k1gFnSc2	federalizace
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
vedle	vedle	k7c2	vedle
existující	existující	k2eAgFnSc2d1	existující
KSS	KSS	kA	KSS
komunistickou	komunistický	k2eAgFnSc4d1	komunistická
stranu	strana	k1gFnSc4	strana
pro	pro	k7c4	pro
české	český	k2eAgFnPc4d1	Česká
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
zamýšleno	zamýšlet	k5eAaImNgNnS	zamýšlet
již	již	k9	již
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
federalizací	federalizace	k1gFnSc7	federalizace
státu	stát	k1gInSc2	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
ustavujícím	ustavující	k2eAgInSc6d1	ustavující
sjezdu	sjezd	k1gInSc6	sjezd
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
KSS	KSS	kA	KSS
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
stále	stále	k6eAd1	stále
existujícího	existující	k2eAgNnSc2d1	existující
Československa	Československo	k1gNnSc2	Československo
zastřešována	zastřešován	k2eAgFnSc1d1	zastřešována
federální	federální	k2eAgFnSc7d1	federální
Komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
KSČS	KSČS	kA	KSČS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stranu	strana	k1gFnSc4	strana
mezitím	mezitím	k6eAd1	mezitím
opustilo	opustit	k5eAaPmAgNnS	opustit
statisíce	statisíce	k1gInPc4	statisíce
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnových	červnový	k2eAgFnPc6d1	červnová
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
Sněmovny	sněmovna	k1gFnPc1	sněmovna
lidu	lid	k1gInSc2	lid
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
a	a	k8xC	a
Sněmovny	sněmovna	k1gFnSc2	sněmovna
národů	národ	k1gInPc2	národ
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
skončila	skončit	k5eAaPmAgFnS	skončit
KSČ	KSČ	kA	KSČ
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
odstupem	odstup	k1gInSc7	odstup
po	po	k7c6	po
OF	OF	kA	OF
druhým	druhý	k4xOgInSc7	druhý
nejsilnějším	silný	k2eAgInSc7d3	nejsilnější
politickým	politický	k2eAgInSc7d1	politický
subjektem	subjekt	k1gInSc7	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
pouze	pouze	k6eAd1	pouze
přes	přes	k7c4	přes
13	[number]	k4	13
%	%	kIx~	%
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
téměř	téměř	k6eAd1	téměř
milion	milion	k4xCgInSc1	milion
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
skončili	skončit	k5eAaPmAgMnP	skončit
komunisté	komunista	k1gMnPc1	komunista
s	s	k7c7	s
obdobným	obdobný	k2eAgInSc7d1	obdobný
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
skončili	skončit	k5eAaPmAgMnP	skončit
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
a	a	k8xC	a
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
dokonce	dokonce	k9	dokonce
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1990	[number]	k4	1990
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
první	první	k4xOgInSc1	první
řádný	řádný	k2eAgInSc1d1	řádný
sjezd	sjezd	k1gInSc1	sjezd
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
strana	strana	k1gFnSc1	strana
přijala	přijmout	k5eAaPmAgFnS	přijmout
svůj	svůj	k3xOyFgInSc4	svůj
politický	politický	k2eAgInSc4d1	politický
program	program	k1gInSc4	program
a	a	k8xC	a
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
její	její	k3xOp3gMnSc1	její
druhý	druhý	k4xOgMnSc1	druhý
předseda	předseda	k1gMnSc1	předseda
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
Jiří	Jiří	k1gMnSc1	Jiří
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
působilo	působit	k5eAaImAgNnS	působit
v	v	k7c6	v
ČSFR	ČSFR	kA	ČSFR
vedle	vedle	k7c2	vedle
KSČM	KSČM	kA	KSČM
dalších	další	k2eAgInPc2d1	další
16	[number]	k4	16
levicových	levicový	k2eAgInPc2d1	levicový
subjektů	subjekt	k1gInPc2	subjekt
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
Levá	levý	k2eAgFnSc1d1	levá
alternativa	alternativa	k1gFnSc1	alternativa
<g/>
,	,	kIx,	,
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
socialistická	socialistický	k2eAgFnSc1d1	socialistická
<g/>
,	,	kIx,	,
Kluby	klub	k1gInPc4	klub
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
OF	OF	kA	OF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
celostátní	celostátní	k2eAgInSc1d1	celostátní
sjezd	sjezd	k1gInSc1	sjezd
<g/>
,	,	kIx,	,
konaný	konaný	k2eAgInSc1d1	konaný
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
KSČS	KSČS	kA	KSČS
za	za	k7c4	za
federaci	federace	k1gFnSc4	federace
dvou	dva	k4xCgInPc2	dva
samostatných	samostatný	k2eAgInPc2d1	samostatný
politických	politický	k2eAgInPc2d1	politický
subjektů	subjekt	k1gInPc2	subjekt
<g/>
,	,	kIx,	,
vůči	vůči	k7c3	vůči
nimž	jenž	k3xRgInPc3	jenž
neměla	mít	k5eNaImAgFnS	mít
KSČS	KSČS	kA	KSČS
pravomoci	pravomoc	k1gFnSc2	pravomoc
nadřízeného	nadřízený	k2eAgInSc2d1	nadřízený
orgánu	orgán	k1gInSc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sjezdu	sjezd	k1gInSc6	sjezd
tedy	tedy	k8xC	tedy
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
ČR	ČR	kA	ČR
zaregistrovalo	zaregistrovat	k5eAaPmAgNnS	zaregistrovat
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
a	a	k8xC	a
KSS	KSS	kA	KSS
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
transformovala	transformovat	k5eAaBmAgFnS	transformovat
na	na	k7c4	na
Stranu	strana	k1gFnSc4	strana
demokratické	demokratický	k2eAgFnSc2d1	demokratická
levice	levice	k1gFnSc2	levice
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
září	září	k1gNnSc6	září
1991	[number]	k4	1991
se	se	k3xPyFc4	se
poslanecký	poslanecký	k2eAgInSc1d1	poslanecký
klub	klub	k1gInSc1	klub
KSČS	KSČS	kA	KSČS
ve	v	k7c6	v
Federálním	federální	k2eAgNnSc6d1	federální
shromáždění	shromáždění	k1gNnSc6	shromáždění
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
SDĽ	SDĽ	kA	SDĽ
pak	pak	k6eAd1	pak
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1991	[number]	k4	1991
zcela	zcela	k6eAd1	zcela
opustila	opustit	k5eAaPmAgFnS	opustit
KSČS	KSČS	kA	KSČS
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
právním	právní	k2eAgInSc7d1	právní
výmazem	výmaz	k1gInSc7	výmaz
z	z	k7c2	z
registru	registr	k1gInSc2	registr
politických	politický	k2eAgInPc2d1	politický
subjektů	subjekt	k1gInPc2	subjekt
k	k	k7c3	k
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubnu	duben	k1gInSc3	duben
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
již	již	k6eAd1	již
zcela	zcela	k6eAd1	zcela
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
SDĽ	SDĽ	kA	SDĽ
získala	získat	k5eAaPmAgFnS	získat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
třech	tři	k4xCgFnPc6	tři
volbách	volba	k1gFnPc6	volba
nad	nad	k7c7	nad
14	[number]	k4	14
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
tak	tak	k8xS	tak
druhou	druhý	k4xOgFnSc7	druhý
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
stranou	strana	k1gFnSc7	strana
po	po	k7c6	po
HZDS	HZDS	kA	HZDS
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
stejných	stejný	k2eAgFnPc2d1	stejná
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
KSČM	KSČM	kA	KSČM
(	(	kIx(	(
<g/>
též	též	k9	též
již	již	k6eAd1	již
jako	jako	k8xS	jako
zcela	zcela	k6eAd1	zcela
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
Levého	levý	k2eAgInSc2d1	levý
bloku	blok	k1gInSc2	blok
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Demokratickou	demokratický	k2eAgFnSc7d1	demokratická
levicí	levice	k1gFnSc7	levice
a	a	k8xC	a
společně	společně	k6eAd1	společně
získali	získat	k5eAaPmAgMnP	získat
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
taktéž	taktéž	k?	taktéž
více	hodně	k6eAd2	hodně
než	než	k8xS	než
14	[number]	k4	14
%	%	kIx~	%
voličských	voličský	k2eAgInPc2d1	voličský
hlasů	hlas	k1gInPc2	hlas
jako	jako	k9	jako
druhé	druhý	k4xOgNnSc4	druhý
nejsilnější	silný	k2eAgNnSc4d3	nejsilnější
politické	politický	k2eAgNnSc4d1	politické
seskupení	seskupení	k1gNnSc4	seskupení
po	po	k7c6	po
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
sjezd	sjezd	k1gInSc1	sjezd
KSČM	KSČM	kA	KSČM
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
komunistickou	komunistický	k2eAgFnSc4d1	komunistická
orientaci	orientace	k1gFnSc4	orientace
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
nových	nový	k2eAgFnPc6d1	nová
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KSČM	KSČM	kA	KSČM
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
ČSFR	ČSFR	kA	ČSFR
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
opozičních	opoziční	k2eAgFnPc6d1	opoziční
lavicích	lavice	k1gFnPc6	lavice
<g/>
.	.	kIx.	.
</s>
<s>
Silně	silně	k6eAd1	silně
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
privatizace	privatizace	k1gFnSc1	privatizace
<g/>
,	,	kIx,	,
návrat	návrat	k1gInSc1	návrat
ke	k	k7c3	k
kapitalismu	kapitalismus	k1gInSc3	kapitalismus
a	a	k8xC	a
"	"	kIx"	"
<g/>
rozbíjení	rozbíjení	k1gNnSc1	rozbíjení
<g/>
"	"	kIx"	"
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
KSČM	KSČM	kA	KSČM
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
III	III	kA	III
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc6	sjezd
KSČM	KSČM	kA	KSČM
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
byl	být	k5eAaImAgInS	být
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1993	[number]	k4	1993
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
Miroslav	Miroslav	k1gMnSc1	Miroslav
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
<g/>
.	.	kIx.	.
</s>
<s>
Sjezd	sjezd	k1gInSc1	sjezd
také	také	k9	také
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c6	o
zachování	zachování	k1gNnSc6	zachování
názvu	název	k1gInSc2	název
a	a	k8xC	a
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
se	se	k3xPyFc4	se
ke	k	k7c3	k
kladenskému	kladenský	k2eAgInSc3d1	kladenský
programu	program	k1gInSc3	program
z	z	k7c2	z
II	II	kA	II
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc2	sjezd
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
KSČM	KSČM	kA	KSČM
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
oddělily	oddělit	k5eAaPmAgFnP	oddělit
menší	malý	k2eAgFnPc1d2	menší
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nebyly	být	k5eNaImAgFnP	být
s	s	k7c7	s
výsledky	výsledek	k1gInPc7	výsledek
sjezdu	sjezd	k1gInSc6	sjezd
spokojeny	spokojen	k2eAgFnPc1d1	spokojena
a	a	k8xC	a
založily	založit	k5eAaPmAgFnP	založit
vlastní	vlastní	k2eAgFnPc4d1	vlastní
strany	strana	k1gFnPc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
česká	český	k2eAgFnSc1d1	Česká
Strana	strana	k1gFnSc1	strana
demokratické	demokratický	k2eAgFnSc2d1	demokratická
levice	levice	k1gFnSc2	levice
(	(	kIx(	(
<g/>
SDL	SDL	kA	SDL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Strana	strana	k1gFnSc1	strana
československých	československý	k2eAgMnPc2d1	československý
komunistů	komunista	k1gMnPc2	komunista
(	(	kIx(	(
<g/>
SČK	SČK	kA	SČK
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
Levý	levý	k2eAgInSc1d1	levý
blok	blok	k1gInSc1	blok
<g/>
,	,	kIx,	,
koalice	koalice	k1gFnSc1	koalice
KSČM	KSČM	kA	KSČM
a	a	k8xC	a
SDL	SDL	kA	SDL
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1994	[number]	k4	1994
a	a	k8xC	a
KSČM	KSČM	kA	KSČM
si	se	k3xPyFc3	se
musela	muset	k5eAaImAgFnS	muset
založit	založit	k5eAaPmF	založit
vlastní	vlastní	k2eAgInSc4d1	vlastní
poslanecký	poslanecký	k2eAgInSc4d1	poslanecký
klub	klub	k1gInSc4	klub
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
ČR	ČR	kA	ČR
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
podpořila	podpořit	k5eAaPmAgFnS	podpořit
kandidátku	kandidátka	k1gFnSc4	kandidátka
LB	LB	kA	LB
Marii	Maria	k1gFnSc4	Maria
Stiborovou	Stiborová	k1gFnSc4	Stiborová
<g/>
.	.	kIx.	.
</s>
<s>
KSČM	KSČM	kA	KSČM
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
obecně	obecně	k6eAd1	obecně
trvala	trvat	k5eAaImAgFnS	trvat
na	na	k7c6	na
přímé	přímý	k2eAgFnSc6d1	přímá
volbě	volba	k1gFnSc6	volba
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
jednokomorovém	jednokomorový	k2eAgInSc6d1	jednokomorový
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
IV	IV	kA	IV
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc6	sjezd
KSČM	KSČM	kA	KSČM
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1995	[number]	k4	1995
se	se	k3xPyFc4	se
strana	strana	k1gFnSc1	strana
stabilizovala	stabilizovat	k5eAaBmAgFnS	stabilizovat
<g/>
.	.	kIx.	.
</s>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
stalinista	stalinista	k1gMnSc1	stalinista
<g/>
,	,	kIx,	,
však	však	k9	však
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
zastavit	zastavit	k5eAaPmF	zastavit
prudký	prudký	k2eAgInSc4d1	prudký
úbytek	úbytek	k1gInSc4	úbytek
členské	členský	k2eAgFnSc2d1	členská
základny	základna	k1gFnSc2	základna
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
měla	mít	k5eAaImAgFnS	mít
strana	strana	k1gFnSc1	strana
přes	přes	k7c4	přes
310	[number]	k4	310
000	[number]	k4	000
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
jich	on	k3xPp3gMnPc2	on
měla	mít	k5eAaImAgNnP	mít
necelých	celý	k2eNgInPc2d1	necelý
155	[number]	k4	155
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
tedy	tedy	k9	tedy
opustilo	opustit	k5eAaPmAgNnS	opustit
stranu	strana	k1gFnSc4	strana
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
%	%	kIx~	%
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
skončila	skončit	k5eAaPmAgFnS	skončit
KSČM	KSČM	kA	KSČM
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
s	s	k7c7	s
pouhými	pouhý	k2eAgNnPc7d1	pouhé
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
KSČM	KSČM	kA	KSČM
stavěla	stavět	k5eAaImAgFnS	stavět
k	k	k7c3	k
připravovanému	připravovaný	k2eAgInSc3d1	připravovaný
vstupu	vstup	k1gInSc3	vstup
ČR	ČR	kA	ČR
do	do	k7c2	do
EU	EU	kA	EU
spíše	spíše	k9	spíše
negativně	negativně	k6eAd1	negativně
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
k	k	k7c3	k
připravovanému	připravovaný	k2eAgInSc3d1	připravovaný
vstupu	vstup	k1gInSc3	vstup
ČR	ČR	kA	ČR
do	do	k7c2	do
NATO	NATO	kA	NATO
zcela	zcela	k6eAd1	zcela
kriticky	kriticky	k6eAd1	kriticky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1997	[number]	k4	1997
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
o	o	k7c6	o
začlenění	začlenění	k1gNnSc6	začlenění
Jozefa	Jozef	k1gMnSc2	Jozef
Wagnéra	Wagnér	k1gMnSc2	Wagnér
do	do	k7c2	do
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
Wagnér	Wagnér	k1gInSc4	Wagnér
vždy	vždy	k6eAd1	vždy
odmítal	odmítat	k5eAaImAgMnS	odmítat
a	a	k8xC	a
požadoval	požadovat	k5eAaImAgMnS	požadovat
za	za	k7c4	za
to	ten	k3xDgNnSc1	ten
omluvu	omluva	k1gFnSc4	omluva
a	a	k8xC	a
odškodnění	odškodnění	k1gNnSc4	odškodnění
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
od	od	k7c2	od
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
tvrzením	tvrzení	k1gNnSc7	tvrzení
přišel	přijít	k5eAaPmAgInS	přijít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Růst	růst	k1gInSc1	růst
KSČM	KSČM	kA	KSČM
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1998	[number]	k4	1998
postavila	postavit	k5eAaPmAgFnS	postavit
KSČM	KSČM	kA	KSČM
proti	proti	k7c3	proti
Václavu	Václav	k1gMnSc3	Václav
Havlovi	Havel	k1gMnSc3	Havel
neúspěšně	úspěšně	k6eNd1	úspěšně
svého	svůj	k3xOyFgMnSc4	svůj
kandidáta	kandidát	k1gMnSc4	kandidát
Stanislava	Stanislav	k1gMnSc4	Stanislav
Fischera	Fischer	k1gMnSc4	Fischer
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
taktéž	taktéž	k?	taktéž
neúspěšně	úspěšně	k6eNd1	úspěšně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
nominovala	nominovat	k5eAaBmAgFnS	nominovat
do	do	k7c2	do
doplňovacích	doplňovací	k2eAgFnPc2d1	doplňovací
senátních	senátní	k2eAgFnPc2d1	senátní
voleb	volba	k1gFnPc2	volba
za	za	k7c4	za
obvod	obvod	k1gInSc4	obvod
č.	č.	k?	č.
27	[number]	k4	27
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnových	červnový	k2eAgFnPc6d1	červnová
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
mírně	mírně	k6eAd1	mírně
posílila	posílit	k5eAaPmAgFnS	posílit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
hrozil	hrozit	k5eAaImAgInS	hrozit
Iráku	Irák	k1gInSc2	Irák
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Saddáma	Saddám	k1gMnSc2	Saddám
Husajna	Husajn	k1gMnSc2	Husajn
útok	útok	k1gInSc1	útok
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
západních	západní	k2eAgFnPc2d1	západní
mocností	mocnost	k1gFnPc2	mocnost
za	za	k7c4	za
odmítání	odmítání	k1gNnSc4	odmítání
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
inspektory	inspektor	k1gMnPc7	inspektor
Zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
komise	komise	k1gFnSc2	komise
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
Miroslav	Miroslav	k1gMnSc1	Miroslav
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
stanovisko	stanovisko	k1gNnSc4	stanovisko
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Životy	život	k1gInPc1	život
Iráčanů	Iráčan	k1gMnPc2	Iráčan
jsou	být	k5eAaImIp3nP	být
ohroženy	ohrozit	k5eAaPmNgFnP	ohrozit
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Bill	Bill	k1gMnSc1	Bill
Clinton	Clinton	k1gMnSc1	Clinton
chce	chtít	k5eAaImIp3nS	chtít
zastínit	zastínit	k5eAaPmF	zastínit
nebo	nebo	k8xC	nebo
<g />
.	.	kIx.	.
</s>
<s>
dokonce	dokonce	k9	dokonce
odsunout	odsunout	k5eAaPmF	odsunout
své	svůj	k3xOyFgNnSc4	svůj
odvolání	odvolání	k1gNnSc4	odvolání
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
sexuální	sexuální	k2eAgFnSc7d1	sexuální
aférou	aféra	k1gFnSc7	aféra
se	s	k7c7	s
stážistkou	stážistka	k1gFnSc7	stážistka
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
Monikou	Monika	k1gFnSc7	Monika
Lewinskou	Lewinská	k1gFnSc7	Lewinská
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
připustil	připustit	k5eAaPmAgMnS	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
odstoupit	odstoupit	k5eAaPmF	odstoupit
<g/>
,	,	kIx,	,
výzkum	výzkum	k1gInSc1	výzkum
IVVM	IVVM	kA	IVVM
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
sympatizantů	sympatizant	k1gMnPc2	sympatizant
KSČM	KSČM	kA	KSČM
by	by	kYmCp3nP	by
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
rezignací	rezignace	k1gFnSc7	rezignace
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1999	[number]	k4	1999
se	se	k3xPyFc4	se
Daniel	Daniel	k1gMnSc1	Daniel
Herman	Herman	k1gMnSc1	Herman
v	v	k7c6	v
narážce	narážka	k1gFnSc6	narážka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
komisi	komise	k1gFnSc6	komise
pro	pro	k7c4	pro
otázky	otázka	k1gFnPc4	otázka
církví	církev	k1gFnPc2	církev
objevil	objevit	k5eAaPmAgInS	objevit
za	za	k7c4	za
stát	stát	k1gInSc4	stát
poslanec	poslanec	k1gMnSc1	poslanec
za	za	k7c4	za
KSČM	KSČM	kA	KSČM
Dalibor	Dalibor	k1gMnSc1	Dalibor
Matulka	Matulka	k1gFnSc1	Matulka
<g/>
,	,	kIx,	,
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
zločineckou	zločinecký	k2eAgFnSc7d1	zločinecká
organizací	organizace	k1gFnSc7	organizace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
církve	církev	k1gFnPc4	církev
jen	jen	k6eAd1	jen
pronásledovala	pronásledovat	k5eAaImAgFnS	pronásledovat
a	a	k8xC	a
škodila	škodit	k5eAaImAgFnS	škodit
jim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Matulka	Matulka	k1gFnSc1	Matulka
nebyl	být	k5eNaImAgMnS	být
členem	člen	k1gMnSc7	člen
této	tento	k3xDgFnSc2	tento
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
chvíle	chvíle	k1gFnSc2	chvíle
poslanci	poslanec	k1gMnPc1	poslanec
KSČM	KSČM	kA	KSČM
požadovali	požadovat	k5eAaImAgMnP	požadovat
snížit	snížit	k5eAaPmF	snížit
výdaje	výdaj	k1gInPc4	výdaj
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zabránit	zabránit	k5eAaPmF	zabránit
vstupu	vstup	k1gInSc3	vstup
ČR	ČR	kA	ČR
do	do	k7c2	do
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1999	[number]	k4	1999
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
NATO	NATO	kA	NATO
Miroslav	Miroslav	k1gMnSc1	Miroslav
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
ujistil	ujistit	k5eAaPmAgMnS	ujistit
novináře	novinář	k1gMnPc4	novinář
<g/>
,	,	kIx,	,
že	že	k8xS	že
KSČM	KSČM	kA	KSČM
využije	využít	k5eAaPmIp3nS	využít
své	svůj	k3xOyFgInPc4	svůj
styky	styk	k1gInPc4	styk
s	s	k7c7	s
komunistickými	komunistický	k2eAgFnPc7d1	komunistická
stranami	strana	k1gFnPc7	strana
v	v	k7c6	v
členských	členský	k2eAgFnPc6d1	členská
zemích	zem	k1gFnPc6	zem
aliance	aliance	k1gFnSc2	aliance
ke	k	k7c3	k
koordinaci	koordinace	k1gFnSc3	koordinace
"	"	kIx"	"
<g/>
antinatovské	antinatovský	k2eAgFnSc2d1	antinatovský
<g/>
"	"	kIx"	"
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
také	také	k6eAd1	také
vychvaloval	vychvalovat	k5eAaImAgInS	vychvalovat
Zjuganovu	Zjuganův	k2eAgFnSc4d1	Zjuganova
Komunistickou	komunistický	k2eAgFnSc4d1	komunistická
stranu	strana	k1gFnSc4	strana
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
stranou	strana	k1gFnSc7	strana
v	v	k7c6	v
Dumě	duma	k1gFnSc6	duma
a	a	k8xC	a
u	u	k7c2	u
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
jejího	její	k3xOp3gInSc2	její
sjezdu	sjezd	k1gInSc2	sjezd
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
postoje	postoj	k1gInPc1	postoj
vyvolaly	vyvolat	k5eAaPmAgInP	vyvolat
u	u	k7c2	u
politiků	politik	k1gMnPc2	politik
obavy	obava	k1gFnSc2	obava
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Nečas	Nečas	k1gMnSc1	Nečas
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
bezpečnostního	bezpečnostní	k2eAgInSc2d1	bezpečnostní
výboru	výbor	k1gInSc2	výbor
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
stínový	stínový	k2eAgMnSc1d1	stínový
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chtějí	chtít	k5eAaImIp3nP	chtít
postupně	postupně	k6eAd1	postupně
demontovat	demontovat	k5eAaBmF	demontovat
či	či	k8xC	či
rozložit	rozložit	k5eAaPmF	rozložit
Severoatlantickou	severoatlantický	k2eAgFnSc4d1	Severoatlantická
alianci	aliance	k1gFnSc4	aliance
zevnitř	zevnitř	k6eAd1	zevnitř
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Titz	Titz	k1gMnSc1	Titz
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c4	o
stanovení	stanovení	k1gNnSc4	stanovení
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
z	z	k7c2	z
poslanců	poslanec	k1gMnPc2	poslanec
k	k	k7c3	k
informacím	informace	k1gFnPc3	informace
z	z	k7c2	z
Bruselu	Brusel	k1gInSc2	Brusel
přístup	přístup	k1gInSc1	přístup
<g/>
.	.	kIx.	.
<g/>
Poslanci	poslanec	k1gMnPc1	poslanec
KSČM	KSČM	kA	KSČM
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1999	[number]	k4	1999
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
pro	pro	k7c4	pro
zamítnutí	zamítnutí	k1gNnSc4	zamítnutí
novely	novela	k1gFnSc2	novela
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
mimosoudních	mimosoudní	k2eAgFnPc6d1	mimosoudní
rehabilitacích	rehabilitace	k1gFnPc6	rehabilitace
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
by	by	kYmCp3nS	by
řešila	řešit	k5eAaImAgFnS	řešit
navrácení	navrácení	k1gNnSc4	navrácení
majetku	majetek	k1gInSc2	majetek
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
neměli	mít	k5eNaImAgMnP	mít
české	český	k2eAgNnSc4d1	české
občanství	občanství	k1gNnSc4	občanství
a	a	k8xC	a
trvalý	trvalý	k2eAgInSc4d1	trvalý
pobyt	pobyt	k1gInSc4	pobyt
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kterým	který	k3yQgMnPc3	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
majetek	majetek	k1gInSc1	majetek
státem	stát	k1gInSc7	stát
zkonfiskován	zkonfiskován	k2eAgMnSc1d1	zkonfiskován
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
agentury	agentura	k1gFnSc2	agentura
Sofres-Factum	Sofres-Factum	k1gNnSc1	Sofres-Factum
v	v	k7c6	v
průzkumu	průzkum	k1gInSc6	průzkum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
uvedlo	uvést	k5eAaPmAgNnS	uvést
73,9	[number]	k4	73,9
%	%	kIx~	%
příznivců	příznivec	k1gMnPc2	příznivec
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
že	že	k8xS	že
Havel	Havel	k1gMnSc1	Havel
pracuje	pracovat	k5eAaImIp3nS	pracovat
hůře	zle	k6eAd2	zle
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
KSČM	KSČM	kA	KSČM
stala	stát	k5eAaPmAgFnS	stát
nejkritičtější	kritický	k2eAgFnSc1d3	nejkritičtější
k	k	k7c3	k
tehdejšímu	tehdejší	k2eAgMnSc3d1	tehdejší
prezidentovi	prezident	k1gMnSc3	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Průzkum	průzkum	k1gInSc1	průzkum
přiznal	přiznat	k5eAaPmAgInS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
příznivci	příznivec	k1gMnPc1	příznivec
KSČM	KSČM	kA	KSČM
jsou	být	k5eAaImIp3nP	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
výrazně	výrazně	k6eAd1	výrazně
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Havel	Havel	k1gMnSc1	Havel
měl	mít	k5eAaImAgMnS	mít
odstoupit	odstoupit	k5eAaPmF	odstoupit
<g/>
.	.	kIx.	.
<g/>
Poslední	poslední	k2eAgNnSc4d1	poslední
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
mj.	mj.	kA	mj.
i	i	k9	i
pro	pro	k7c4	pro
komunistického	komunistický	k2eAgMnSc4d1	komunistický
poslance	poslanec	k1gMnSc4	poslanec
Dalibora	Dalibor	k1gMnSc4	Dalibor
Matulku	Matulka	k1gFnSc4	Matulka
<g/>
,	,	kIx,	,
známého	známý	k1gMnSc4	známý
z	z	k7c2	z
obhajování	obhajování	k1gNnSc2	obhajování
politiky	politika	k1gFnSc2	politika
své	svůj	k3xOyFgFnSc2	svůj
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
brojení	brojený	k2eAgMnPc1d1	brojený
proti	proti	k7c3	proti
navracení	navracení	k1gNnSc3	navracení
církevního	církevní	k2eAgInSc2d1	církevní
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
díky	díky	k7c3	díky
ohnivému	ohnivý	k2eAgNnSc3d1	ohnivé
vystupování	vystupování	k1gNnSc3	vystupování
například	například	k6eAd1	například
i	i	k9	i
proti	proti	k7c3	proti
snaze	snaha	k1gFnSc3	snaha
některých	některý	k3yIgMnPc2	některý
zákonodárců	zákonodárce	k1gMnPc2	zákonodárce
odškodnit	odškodnit	k5eAaPmF	odškodnit
vězně	vězně	k6eAd1	vězně
komunistických	komunistický	k2eAgInPc2d1	komunistický
lágrů	lágr	k1gInPc2	lágr
z	z	k7c2	z
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
navzdory	navzdory	k7c3	navzdory
obvinění	obvinění	k1gNnSc3	obvinění
církví	církev	k1gFnPc2	církev
za	za	k7c4	za
nedodržení	nedodržení	k1gNnSc4	nedodržení
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
komisi	komise	k1gFnSc6	komise
budou	být	k5eAaImBp3nP	být
jen	jen	k6eAd1	jen
osoby	osoba	k1gFnPc4	osoba
přijatelné	přijatelný	k2eAgFnPc4d1	přijatelná
pro	pro	k7c4	pro
obě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
<g/>
,	,	kIx,	,
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
ochotna	ochoten	k2eAgFnSc1d1	ochotna
změnit	změnit	k5eAaPmF	změnit
složení	složení	k1gNnSc4	složení
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
komisi	komise	k1gFnSc6	komise
pro	pro	k7c4	pro
otázky	otázka	k1gFnPc4	otázka
církví	církev	k1gFnPc2	církev
objevil	objevit	k5eAaPmAgMnS	objevit
i	i	k8xC	i
Matulka	Matulka	k1gFnSc1	Matulka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
rozhněvalo	rozhněvat	k5eAaPmAgNnS	rozhněvat
i	i	k9	i
kardinála	kardinál	k1gMnSc4	kardinál
Miloslava	Miloslav	k1gMnSc4	Miloslav
Vlka	Vlk	k1gMnSc4	Vlk
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1999	[number]	k4	1999
poslanci	poslanec	k1gMnPc1	poslanec
KSČM	KSČM	kA	KSČM
souhlasily	souhlasit	k5eAaImAgFnP	souhlasit
s	s	k7c7	s
vládní	vládní	k2eAgFnSc7d1	vládní
zprávou	zpráva	k1gFnSc7	zpráva
o	o	k7c6	o
stavu	stav	k1gInSc6	stav
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
nacházela	nacházet	k5eAaImAgFnS	nacházet
přibližně	přibližně	k6eAd1	přibližně
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Zprávě	zpráva	k1gFnSc3	zpráva
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
například	například	k6eAd1	například
předseda	předseda	k1gMnSc1	předseda
poslanců	poslanec	k1gMnPc2	poslanec
ODS	ODS	kA	ODS
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Tlustý	tlustý	k2eAgMnSc1d1	tlustý
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zprávu	zpráva	k1gFnSc4	zpráva
považujeme	považovat	k5eAaImIp1nP	považovat
za	za	k7c4	za
matení	matení	k1gNnSc4	matení
naší	náš	k3xOp1gFnSc2	náš
nedávné	dávný	k2eNgFnSc2d1	nedávná
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1999	[number]	k4	1999
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
stran	strana	k1gFnPc2	strana
KSČM	KSČM	kA	KSČM
nevyslala	vyslat	k5eNaPmAgFnS	vyslat
své	svůj	k3xOyFgMnPc4	svůj
zástupce	zástupce	k1gMnPc4	zástupce
na	na	k7c4	na
schůzku	schůzka	k1gFnSc4	schůzka
v	v	k7c6	v
Lánech	lán	k1gInPc6	lán
s	s	k7c7	s
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Projednávala	projednávat	k5eAaImAgFnS	projednávat
se	se	k3xPyFc4	se
především	především	k9	především
budoucnost	budoucnost	k1gFnSc1	budoucnost
ČR	ČR	kA	ČR
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
faktorech	faktor	k1gInPc6	faktor
<g/>
.12	.12	k4	.12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1999	[number]	k4	1999
během	během	k7c2	během
vstupu	vstup	k1gInSc2	vstup
ČR	ČR	kA	ČR
do	do	k7c2	do
NATO	NATO	kA	NATO
mladí	mladý	k2eAgMnPc1d1	mladý
komunisté	komunista	k1gMnPc1	komunista
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
rozvinuli	rozvinout	k5eAaPmAgMnP	rozvinout
plakáty	plakát	k1gInPc4	plakát
a	a	k8xC	a
rozdávali	rozdávat	k5eAaImAgMnP	rozdávat
letáky	leták	k1gInPc4	leták
s	s	k7c7	s
"	"	kIx"	"
<g/>
antinatovskou	antinatovský	k2eAgFnSc7d1	antinatovský
<g/>
"	"	kIx"	"
tématikou	tématika	k1gFnSc7	tématika
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
symbolickém	symbolický	k2eAgNnSc6d1	symbolické
začlenění	začlenění	k1gNnSc6	začlenění
ČR	ČR	kA	ČR
do	do	k7c2	do
NATO	NATO	kA	NATO
na	na	k7c6	na
konci	konec	k1gInSc6	konec
oslavy	oslava	k1gFnSc2	oslava
pískali	pískat	k5eAaImAgMnP	pískat
na	na	k7c4	na
píšťalky	píšťalka	k1gFnPc4	píšťalka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ostravě-Porubě	Ostravě-Poruba	k1gFnSc6	Ostravě-Poruba
jeden	jeden	k4xCgMnSc1	jeden
účastník	účastník	k1gMnSc1	účastník
komunistické	komunistický	k2eAgFnSc2d1	komunistická
demonstrace	demonstrace	k1gFnSc2	demonstrace
proti	proti	k7c3	proti
vstupu	vstup	k1gInSc3	vstup
ČR	ČR	kA	ČR
do	do	k7c2	do
aliance	aliance	k1gFnSc2	aliance
spálil	spálit	k5eAaPmAgMnS	spálit
výkres	výkres	k1gInSc4	výkres
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
průzkumu	průzkum	k1gInSc6	průzkum
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
agentura	agentura	k1gFnSc1	agentura
Sofres-Factum	Sofres-Factum	k1gNnSc4	Sofres-Factum
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
voliči	volič	k1gMnPc1	volič
KSČM	KSČM	kA	KSČM
řadí	řadit	k5eAaImIp3nP	řadit
Klementa	Klement	k1gMnSc4	Klement
Gottwalda	Gottwald	k1gMnSc4	Gottwald
za	za	k7c4	za
zřetelně	zřetelně	k6eAd1	zřetelně
kladnou	kladný	k2eAgFnSc4d1	kladná
postavu	postava	k1gFnSc4	postava
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1999	[number]	k4	1999
během	během	k7c2	během
návštěvy	návštěva	k1gFnSc2	návštěva
Jan	Jan	k1gMnSc1	Jan
Kasal	Kasal	k1gMnSc1	Kasal
(	(	kIx(	(
<g/>
KDU-ČSL	KDU-ČSL	k1gMnSc1	KDU-ČSL
<g/>
)	)	kIx)	)
ujistil	ujistit	k5eAaPmAgMnS	ujistit
chilského	chilský	k2eAgMnSc4d1	chilský
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
strana	strana	k1gFnSc1	strana
výslovně	výslovně	k6eAd1	výslovně
nekomunistická	komunistický	k2eNgFnSc1d1	nekomunistická
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
nepůjde	jít	k5eNaImIp3nS	jít
do	do	k7c2	do
takové	takový	k3xDgFnSc2	takový
koalice	koalice	k1gFnSc2	koalice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
musela	muset	k5eAaImAgFnS	muset
spoléhat	spoléhat	k5eAaImF	spoléhat
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
komunistů	komunista	k1gMnPc2	komunista
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ČSSD	ČSSD	kA	ČSSD
naopak	naopak	k6eAd1	naopak
někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
jako	jako	k9	jako
Ivan	Ivan	k1gMnSc1	Ivan
Havlíček	Havlíček	k1gMnSc1	Havlíček
uvažovali	uvažovat	k5eAaImAgMnP	uvažovat
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
bohumínského	bohumínský	k2eAgNnSc2d1	Bohumínské
usnesení	usnesení	k1gNnSc2	usnesení
<g/>
,	,	kIx,	,
zakazující	zakazující	k2eAgFnSc4d1	zakazující
vládní	vládní	k2eAgFnSc4d1	vládní
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
extremistickými	extremistický	k2eAgFnPc7d1	extremistická
stranami	strana	k1gFnPc7	strana
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
možnou	možný	k2eAgFnSc4d1	možná
budoucí	budoucí	k2eAgFnSc4d1	budoucí
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
KSČM	KSČM	kA	KSČM
<g/>
.27	.27	k4	.27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1999	[number]	k4	1999
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
sešli	sejít	k5eAaPmAgMnP	sejít
poslanci	poslanec	k1gMnPc1	poslanec
<g/>
,	,	kIx,	,
senátoři	senátor	k1gMnPc1	senátor
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
ÚV	ÚV	kA	ÚV
KSČM	KSČM	kA	KSČM
s	s	k7c7	s
jejich	jejich	k3xOp3gMnPc7	jejich
sympatizanty	sympatizant	k1gMnPc7	sympatizant
na	na	k7c6	na
demonstraci	demonstrace	k1gFnSc6	demonstrace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
čítala	čítat	k5eAaImAgFnS	čítat
500	[number]	k4	500
až	až	k9	až
2	[number]	k4	2
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Požadovali	požadovat	k5eAaImAgMnP	požadovat
zastavení	zastavení	k1gNnSc3	zastavení
leteckých	letecký	k2eAgMnPc2d1	letecký
úderů	úder	k1gInPc2	úder
na	na	k7c6	na
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
,	,	kIx,	,
zrušení	zrušení	k1gNnSc6	zrušení
NATO	NATO	kA	NATO
a	a	k8xC	a
odstoupení	odstoupení	k1gNnSc2	odstoupení
prezidenta	prezident	k1gMnSc2	prezident
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
demonstrace	demonstrace	k1gFnSc1	demonstrace
přesunula	přesunout	k5eAaPmAgFnS	přesunout
z	z	k7c2	z
Václavského	václavský	k2eAgNnSc2d1	Václavské
náměstí	náměstí	k1gNnSc2	náměstí
před	před	k7c4	před
americkou	americký	k2eAgFnSc4d1	americká
ambasádu	ambasáda	k1gFnSc4	ambasáda
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
komunisté	komunista	k1gMnPc1	komunista
házeli	házet	k5eAaImAgMnP	házet
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
rozbili	rozbít	k5eAaPmAgMnP	rozbít
jednu	jeden	k4xCgFnSc4	jeden
okenní	okenní	k2eAgFnSc4d1	okenní
tabulku	tabulka	k1gFnSc4	tabulka
<g/>
.3	.3	k4	.3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1999	[number]	k4	1999
se	se	k3xPyFc4	se
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Filip	Filip	k1gMnSc1	Filip
pokusil	pokusit	k5eAaPmAgMnS	pokusit
prosadit	prosadit	k5eAaPmF	prosadit
debatu	debata	k1gFnSc4	debata
o	o	k7c6	o
balkánské	balkánský	k2eAgFnSc6d1	balkánská
krizi	krize	k1gFnSc6	krize
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
záminku	záminka	k1gFnSc4	záminka
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
byl	být	k5eAaImAgInS	být
spatřen	spatřit	k5eAaPmNgInS	spatřit
vlak	vlak	k1gInSc4	vlak
s	s	k7c7	s
britskými	britský	k2eAgMnPc7d1	britský
vojáky	voják	k1gMnPc7	voják
i	i	k8xC	i
s	s	k7c7	s
armádní	armádní	k2eAgFnSc7d1	armádní
technikou	technika	k1gFnSc7	technika
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
může	moct	k5eAaImIp3nS	moct
dát	dát	k5eAaPmF	dát
souhlas	souhlas	k1gInSc4	souhlas
pouze	pouze	k6eAd1	pouze
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vetchý	vetchý	k2eAgMnSc1d1	vetchý
mu	on	k3xPp3gMnSc3	on
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlak	vlak	k1gInSc1	vlak
i	i	k9	i
s	s	k7c7	s
vojáky	voják	k1gMnPc7	voják
patřil	patřit	k5eAaImAgInS	patřit
filmařům	filmař	k1gMnPc3	filmař
<g/>
.11	.11	k4	.11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1999	[number]	k4	1999
na	na	k7c4	na
29	[number]	k4	29
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc2	sjezd
ČSSD	ČSSD	kA	ČSSD
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
vyloučil	vyloučit	k5eAaPmAgMnS	vyloučit
koalici	koalice	k1gFnSc4	koalice
s	s	k7c7	s
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
neomluvila	omluvit	k5eNaPmAgFnS	omluvit
za	za	k7c4	za
porušování	porušování	k1gNnSc4	porušování
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
letech	let	k1gInPc6	let
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
ho	on	k3xPp3gMnSc4	on
sjezd	sjezd	k1gInSc1	sjezd
odměnil	odměnit	k5eAaPmAgInS	odměnit
potleskem	potlesk	k1gInSc7	potlesk
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
KSČM	KSČM	kA	KSČM
nerozešla	rozejít	k5eNaPmAgFnS	rozejít
se	s	k7c7	s
stalinismem	stalinismus	k1gInSc7	stalinismus
<g/>
,	,	kIx,	,
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
Zemana	Zeman	k1gMnSc4	Zeman
i	i	k9	i
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Klanica	Klanica	k1gMnSc1	Klanica
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
základní	základní	k2eAgInSc1d1	základní
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
komunistickými	komunistický	k2eAgFnPc7d1	komunistická
a	a	k8xC	a
sociálnědemokratickými	sociálnědemokratický	k2eAgFnPc7d1	sociálnědemokratická
stranami	strana	k1gFnPc7	strana
je	být	k5eAaImIp3nS	být
rozdílem	rozdíl	k1gInSc7	rozdíl
mezi	mezi	k7c7	mezi
diktaturou	diktatura	k1gFnSc7	diktatura
a	a	k8xC	a
demokracií	demokracie	k1gFnSc7	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
KSČM	KSČM	kA	KSČM
nepřizná	přiznat	k5eNaPmIp3nS	přiznat
zločiny	zločin	k1gInPc4	zločin
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgInSc2	svůj
čtyřicetiletého	čtyřicetiletý	k2eAgInSc2d1	čtyřicetiletý
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
nezreformuje	zreformovat	k5eNaPmIp3nS	zreformovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
nikdy	nikdy	k6eAd1	nikdy
pro	pro	k7c4	pro
ČSSD	ČSSD	kA	ČSSD
koaličním	koaliční	k2eAgMnSc7d1	koaliční
partnerem	partner	k1gMnSc7	partner
<g/>
.	.	kIx.	.
</s>
<s>
KSČM	KSČM	kA	KSČM
se	se	k3xPyFc4	se
pochopitelně	pochopitelně	k6eAd1	pochopitelně
ani	ani	k8xC	ani
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
na	na	k7c6	na
svém	své	k1gNnSc6	své
V.	V.	kA	V.
sjezdu	sjezd	k1gInSc2	sjezd
nijak	nijak	k6eAd1	nijak
zvlášť	zvlášť	k6eAd1	zvlášť
nereformovala	reformovat	k5eNaBmAgFnS	reformovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
brala	brát	k5eAaImAgFnS	brát
za	za	k7c4	za
nepřijatelné	přijatelný	k2eNgFnPc4d1	nepřijatelná
změnu	změna	k1gFnSc4	změna
názvu	název	k1gInSc2	název
a	a	k8xC	a
přikročila	přikročit	k5eAaPmAgFnS	přikročit
k	k	k7c3	k
zaměření	zaměření	k1gNnSc3	zaměření
na	na	k7c4	na
sociálně	sociálně	k6eAd1	sociálně
slabší	slabý	k2eAgFnPc4d2	slabší
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
.	.	kIx.	.
<g/>
Druhá	druhý	k4xOgFnSc1	druhý
skupina	skupina	k1gFnSc1	skupina
pro	pro	k7c4	pro
otázky	otázka	k1gFnPc4	otázka
církvím	církev	k1gFnPc3	církev
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
protest	protest	k1gInSc4	protest
církvím	církev	k1gFnPc3	církev
již	již	k9	již
bez	bez	k1gInSc1	bez
zástupce	zástupce	k1gMnSc2	zástupce
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
sociologa	sociolog	k1gMnSc2	sociolog
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Sýkory	sýkora	k1gFnSc2	sýkora
sympatizovalo	sympatizovat	k5eAaImAgNnS	sympatizovat
s	s	k7c7	s
KSČM	KSČM	kA	KSČM
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1999	[number]	k4	1999
jen	jen	k6eAd1	jen
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
přitom	přitom	k6eAd1	přitom
KSČM	KSČM	kA	KSČM
volilo	volit	k5eAaImAgNnS	volit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
18	[number]	k4	18
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
8	[number]	k4	8
%	%	kIx~	%
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
vojáků	voják	k1gMnPc2	voják
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
sociologů	sociolog	k1gMnPc2	sociolog
a	a	k8xC	a
analytiků	analytik	k1gMnPc2	analytik
těšila	těšit	k5eAaImAgFnS	těšit
vysoké	vysoký	k2eAgInPc4d1	vysoký
společenské	společenský	k2eAgInPc4d1	společenský
prestiže	prestiž	k1gInPc4	prestiž
a	a	k8xC	a
smysluplnosti	smysluplnost	k1gFnPc4	smysluplnost
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
předchozí	předchozí	k2eAgInSc1d1	předchozí
režim	režim	k1gInSc1	režim
nedokázal	dokázat	k5eNaPmAgInS	dokázat
naplnit	naplnit	k5eAaPmF	naplnit
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kareš	Kareš	k1gMnSc1	Kareš
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
roje	roj	k1gInSc2	roj
elitních	elitní	k2eAgMnPc2d1	elitní
stíhačů	stíhač	k1gMnPc2	stíhač
<g/>
,	,	kIx,	,
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Při	při	k7c6	při
střelbách	střelba	k1gFnPc6	střelba
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
jeho	jeho	k3xOp3gFnPc6	jeho
částech	část	k1gFnPc6	část
chvílemi	chvílemi	k6eAd1	chvílemi
připadali	připadat	k5eAaPmAgMnP	připadat
jako	jako	k9	jako
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Sociolog	sociolog	k1gMnSc1	sociolog
Libor	Libor	k1gMnSc1	Libor
Konvička	konvička	k1gFnSc1	konvička
ze	z	k7c2	z
Střediska	středisko	k1gNnSc2	středisko
empirických	empirický	k2eAgInPc2d1	empirický
výzkumů	výzkum	k1gInPc2	výzkum
(	(	kIx(	(
<g/>
STEM	sto	k4xCgNnSc7	sto
<g/>
)	)	kIx)	)
shledal	shledat	k5eAaPmAgInS	shledat
značku	značka	k1gFnSc4	značka
NATO	NATO	kA	NATO
vůči	vůči	k7c3	vůči
Varšavské	varšavský	k2eAgFnSc3d1	Varšavská
smlouvě	smlouva	k1gFnSc3	smlouva
kladně	kladně	k6eAd1	kladně
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
dle	dle	k7c2	dle
něj	on	k3xPp3gMnSc2	on
důstojníci	důstojník	k1gMnPc1	důstojník
ČSLA	ČSLA	kA	ČSLA
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
chtěli	chtít	k5eAaImAgMnP	chtít
sloužit	sloužit	k5eAaImF	sloužit
národu	národ	k1gInSc3	národ
<g/>
,	,	kIx,	,
sklízeli	sklízet	k5eAaImAgMnP	sklízet
jen	jen	k9	jen
rozladění	rozladění	k1gNnSc4	rozladění
a	a	k8xC	a
těžkou	těžký	k2eAgFnSc4d1	těžká
depresi	deprese	k1gFnSc4	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Obranu	obrana	k1gFnSc4	obrana
socialistické	socialistický	k2eAgFnSc2d1	socialistická
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
navíc	navíc	k6eAd1	navíc
okupovala	okupovat	k5eAaBmAgNnP	okupovat
sovětská	sovětský	k2eAgNnPc1d1	sovětské
vojska	vojsko	k1gNnPc1	vojsko
<g/>
,	,	kIx,	,
chápali	chápat	k5eAaImAgMnP	chápat
většinou	většinou	k6eAd1	většinou
jako	jako	k8xS	jako
frašku	fraška	k1gFnSc4	fraška
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
STEM	sto	k4xCgNnSc7	sto
KSČM	KSČM	kA	KSČM
tradičně	tradičně	k6eAd1	tradičně
oslovuje	oslovovat	k5eAaImIp3nS	oslovovat
důchodce	důchodce	k1gMnPc4	důchodce
a	a	k8xC	a
voliče	volič	k1gMnPc4	volič
se	s	k7c7	s
špatnou	špatný	k2eAgFnSc7d1	špatná
životní	životní	k2eAgFnSc7d1	životní
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
lidi	člověk	k1gMnPc4	člověk
se	s	k7c7	s
základním	základní	k2eAgNnSc7d1	základní
vzděláním	vzdělání	k1gNnSc7	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Naději	nadát	k5eAaBmIp1nS	nadát
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
upírali	upírat	k5eAaImAgMnP	upírat
také	také	k9	také
dělníci	dělník	k1gMnPc1	dělník
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
nízkými	nízký	k2eAgInPc7d1	nízký
příjmy	příjem	k1gInPc7	příjem
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnPc1d1	žijící
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.21	.21	k4	.21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1999	[number]	k4	1999
poslanci	poslanec	k1gMnPc1	poslanec
KSČM	KSČM	kA	KSČM
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
pár	pár	k4xCyI	pár
sociálními	sociální	k2eAgMnPc7d1	sociální
demokraty	demokrat	k1gMnPc7	demokrat
neúspěšně	úspěšně	k6eNd1	úspěšně
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
proti	proti	k7c3	proti
poskytnutí	poskytnutí	k1gNnSc3	poskytnutí
českých	český	k2eAgNnPc2d1	české
letišť	letiště	k1gNnPc2	letiště
silám	síla	k1gFnPc3	síla
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
IVVM	IVVM	kA	IVVM
z	z	k7c2	z
května	květen	k1gInSc2	květen
1999	[number]	k4	1999
mají	mít	k5eAaImIp3nP	mít
sympatizanti	sympatizant	k1gMnPc1	sympatizant
KSČM	KSČM	kA	KSČM
lepší	dobrý	k2eAgInSc4d2	lepší
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
a	a	k8xC	a
Číně	Čína	k1gFnSc3	Čína
než	než	k8xS	než
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc3	Rakousko
nebo	nebo	k8xC	nebo
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1999	[number]	k4	1999
Miroslav	Miroslav	k1gMnSc1	Miroslav
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
se	se	k3xPyFc4	se
zprávou	zpráva	k1gFnSc7	zpráva
o	o	k7c6	o
stavu	stav	k1gInSc6	stav
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
neobjektivní	objektivní	k2eNgFnSc4d1	neobjektivní
a	a	k8xC	a
chybí	chybět	k5eAaImIp3nS	chybět
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
potřebná	potřebné	k1gNnPc1	potřebné
stanoviska	stanovisko	k1gNnPc1	stanovisko
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
popisovala	popisovat	k5eAaImAgFnS	popisovat
krizovou	krizový	k2eAgFnSc4d1	krizová
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dle	dle	k7c2	dle
Grebeníčka	Grebeníčko	k1gNnSc2	Grebeníčko
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
<g/>
Poslanci	poslanec	k1gMnPc1	poslanec
KSČM	KSČM	kA	KSČM
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1999	[number]	k4	1999
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
proti	proti	k7c3	proti
novele	novela	k1gFnSc3	novela
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
střelných	střelný	k2eAgFnPc6d1	střelná
zbraních	zbraň	k1gFnPc6	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
KSČM	KSČM	kA	KSČM
také	také	k9	také
vedla	vést	k5eAaImAgFnS	vést
polemiky	polemika	k1gFnPc4	polemika
k	k	k7c3	k
osobnostem	osobnost	k1gFnPc3	osobnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
by	by	kYmCp3nS	by
poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
doporučila	doporučit	k5eAaPmAgFnS	doporučit
prezidentovi	prezident	k1gMnSc3	prezident
k	k	k7c3	k
udělení	udělení	k1gNnSc3	udělení
nebo	nebo	k8xC	nebo
propůjčení	propůjčení	k1gNnSc4	propůjčení
na	na	k7c4	na
státní	státní	k2eAgNnSc4d1	státní
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Sporem	spor	k1gInSc7	spor
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
osobností	osobnost	k1gFnPc2	osobnost
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gFnSc1	jejich
prokomunistická	prokomunistický	k2eAgFnSc1d1	prokomunistická
minulost	minulost	k1gFnSc1	minulost
<g/>
.5	.5	k4	.5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1999	[number]	k4	1999
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
příznivci	příznivec	k1gMnPc1	příznivec
KSČM	KSČM	kA	KSČM
sešli	sejít	k5eAaPmAgMnP	sejít
na	na	k7c6	na
osmi	osm	k4xCc6	osm
tisícové	tisícový	k2eAgFnSc6d1	tisícová
demonstraci	demonstrace	k1gFnSc6	demonstrace
pořádané	pořádaný	k2eAgFnSc6d1	pořádaná
anarchistickou	anarchistický	k2eAgFnSc7d1	anarchistická
Street	Street	k1gInSc1	Street
Party	part	k1gInPc1	part
'	'	kIx"	'
<g/>
99	[number]	k4	99
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
9	[number]	k4	9
policistů	policista	k1gMnPc2	policista
(	(	kIx(	(
<g/>
3	[number]	k4	3
vážně	vážně	k6eAd1	vážně
a	a	k8xC	a
6	[number]	k4	6
lehce	lehko	k6eAd1	lehko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Demonstrace	demonstrace	k1gFnSc1	demonstrace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
zasahovalo	zasahovat	k5eAaImAgNnS	zasahovat
na	na	k7c4	na
1	[number]	k4	1
tisíc	tisíc	k4xCgInPc2	tisíc
policistů	policista	k1gMnPc2	policista
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
také	také	k9	také
proměnila	proměnit	k5eAaPmAgFnS	proměnit
na	na	k7c4	na
útok	útok	k1gInSc4	útok
na	na	k7c4	na
americkou	americký	k2eAgFnSc4d1	americká
ambasádu	ambasáda	k1gFnSc4	ambasáda
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
protestující	protestující	k2eAgInPc4d1	protestující
rozbili	rozbít	k5eAaPmAgMnP	rozbít
mnoho	mnoho	k4c1	mnoho
oken	okno	k1gNnPc2	okno
pomocí	pomocí	k7c2	pomocí
lahví	lahev	k1gFnPc2	lahev
a	a	k8xC	a
kamenů	kámen	k1gInPc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
zadržela	zadržet	k5eAaPmAgFnS	zadržet
67	[number]	k4	67
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
Průzkum	průzkum	k1gInSc1	průzkum
IVVM	IVVM	kA	IVVM
z	z	k7c2	z
června	červen	k1gInSc2	červen
1999	[number]	k4	1999
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
komunisty	komunista	k1gMnPc4	komunista
volilo	volit	k5eAaImAgNnS	volit
17	[number]	k4	17
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
těsně	těsně	k6eAd1	těsně
za	za	k7c4	za
ČSSD	ČSSD	kA	ČSSD
třetí	třetí	k4xOgFnSc7	třetí
nejpopulárnější	populární	k2eAgFnSc7d3	nejpopulárnější
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1999	[number]	k4	1999
neúspěšně	úspěšně	k6eNd1	úspěšně
navrhovali	navrhovat	k5eAaImAgMnP	navrhovat
poslanci	poslanec	k1gMnPc1	poslanec
KSČM	KSČM	kA	KSČM
poslat	poslat	k5eAaPmF	poslat
místo	místo	k1gNnSc4	místo
průzkumné	průzkumný	k2eAgFnSc2d1	průzkumná
roty	rota	k1gFnSc2	rota
protichemickou	protichemický	k2eAgFnSc4d1	protichemická
jednotku	jednotka	k1gFnSc4	jednotka
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgNnP	mít
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
ekologické	ekologický	k2eAgFnPc4d1	ekologická
škody	škoda	k1gFnPc4	škoda
"	"	kIx"	"
<g/>
agrese	agrese	k1gFnSc1	agrese
NATO	NATO	kA	NATO
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
se	s	k7c7	s
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1999	[number]	k4	1999
před	před	k7c7	před
opakovanými	opakovaný	k2eAgFnPc7d1	opakovaná
volbami	volba	k1gFnPc7	volba
<g/>
,	,	kIx,	,
způsobených	způsobený	k2eAgInPc6d1	způsobený
odstoupením	odstoupení	k1gNnSc7	odstoupení
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nebyli	být	k5eNaImAgMnP	být
jejich	jejich	k3xOp3gMnPc7	jejich
zástupci	zástupce	k1gMnPc7	zástupce
v	v	k7c6	v
radě	rada	k1gFnSc6	rada
s	s	k7c7	s
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
komunisty	komunista	k1gMnPc7	komunista
<g/>
,	,	kIx,	,
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
i	i	k9	i
Miroslav	Miroslav	k1gMnSc1	Miroslav
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
na	na	k7c6	na
předvolebním	předvolební	k2eAgInSc6d1	předvolební
mítinku	mítink	k1gInSc6	mítink
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
nebudeme	být	k5eNaImBp1nP	být
pletichařit	pletichařit	k5eAaImF	pletichařit
<g/>
"	"	kIx"	"
k	k	k7c3	k
davu	dav	k1gInSc3	dav
složenému	složený	k2eAgInSc3d1	složený
spíše	spíše	k9	spíše
z	z	k7c2	z
důchodců	důchodce	k1gMnPc2	důchodce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1999	[number]	k4	1999
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
zkritizoval	zkritizovat	k5eAaPmAgMnS	zkritizovat
ČSSD	ČSSD	kA	ČSSD
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zrazuje	zrazovat	k5eAaImIp3nS	zrazovat
svůj	svůj	k3xOyFgInSc4	svůj
program	program	k1gInSc4	program
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
pravicově	pravicově	k6eAd1	pravicově
<g/>
.	.	kIx.	.
</s>
<s>
Miloslav	Miloslav	k1gMnSc1	Miloslav
Ransdorf	Ransdorf	k1gMnSc1	Ransdorf
charakterizoval	charakterizovat	k5eAaBmAgMnS	charakterizovat
počínání	počínání	k1gNnSc4	počínání
vlády	vláda	k1gFnSc2	vláda
ČSSD	ČSSD	kA	ČSSD
leninským	leninský	k2eAgInSc7d1	leninský
termínem	termín	k1gInSc7	termín
"	"	kIx"	"
<g/>
krok	krok	k1gInSc1	krok
vpřed	vpřed	k6eAd1	vpřed
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
kroky	krok	k1gInPc4	krok
vzad	vzad	k6eAd1	vzad
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
červencového	červencový	k2eAgInSc2d1	červencový
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
průzkumu	průzkum	k1gInSc2	průzkum
STEM	sto	k4xCgNnSc7	sto
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
KSČM	KSČM	kA	KSČM
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
17,8	[number]	k4	17,8
%	%	kIx~	%
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
strana	strana	k1gFnSc1	strana
stala	stát	k5eAaPmAgFnS	stát
po	po	k7c6	po
ODS	ODS	kA	ODS
druhou	druhý	k4xOgFnSc7	druhý
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
stranou	strana	k1gFnSc7	strana
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Ruml	Ruml	k1gMnSc1	Ruml
(	(	kIx(	(
<g/>
US	US	kA	US
<g/>
)	)	kIx)	)
to	ten	k3xDgNnSc4	ten
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
selhání	selhání	k1gNnSc4	selhání
sociálnědemokratické	sociálnědemokratický	k2eAgFnSc2d1	sociálnědemokratická
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
se	se	k3xPyFc4	se
pokusit	pokusit	k5eAaPmF	pokusit
rozbít	rozbít	k5eAaPmF	rozbít
opoziční	opoziční	k2eAgFnSc4d1	opoziční
smlouvu	smlouva	k1gFnSc4	smlouva
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
vládu	vláda	k1gFnSc4	vláda
společně	společně	k6eAd1	společně
se	s	k7c7	s
středopravicovými	středopravicový	k2eAgFnPc7d1	středopravicová
stranami	strana	k1gFnPc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Červencový	červencový	k2eAgInSc1d1	červencový
vzrůst	vzrůst	k1gInSc1	vzrůst
podpory	podpora	k1gFnSc2	podpora
KSČM	KSČM	kA	KSČM
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
i	i	k9	i
průzkum	průzkum	k1gInSc1	průzkum
IVVM	IVVM	kA	IVVM
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgInSc2	který
by	by	kYmCp3nS	by
KSČM	KSČM	kA	KSČM
získala	získat	k5eAaPmAgFnS	získat
17	[number]	k4	17
%	%	kIx~	%
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
by	by	kYmCp3nS	by
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
mohla	moct	k5eAaImAgFnS	moct
za	za	k7c4	za
výrazný	výrazný	k2eAgInSc4d1	výrazný
vzestup	vzestup	k1gInSc4	vzestup
špatná	špatný	k2eAgFnSc1d1	špatná
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
STEM	sto	k4xCgNnSc7	sto
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
v	v	k7c6	v
preferenčních	preferenční	k2eAgInPc6d1	preferenční
výzkumech	výzkum	k1gInPc6	výzkum
druhou	druhý	k4xOgFnSc4	druhý
nejpopulárnější	populární	k2eAgFnSc4d3	nejpopulárnější
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc4	třetí
pozici	pozice	k1gFnSc4	pozice
těsně	těsně	k6eAd1	těsně
za	za	k7c4	za
ČSSD	ČSSD	kA	ČSSD
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
i	i	k9	i
průzkum	průzkum	k1gInSc4	průzkum
agentury	agentura	k1gFnSc2	agentura
Sofres-Factum	Sofres-Factum	k1gNnSc4	Sofres-Factum
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
výzkumu	výzkum	k1gInSc2	výzkum
IVVM	IVVM	kA	IVVM
ze	z	k7c2	z
srpna	srpen	k1gInSc2	srpen
1999	[number]	k4	1999
nevěřilo	věřit	k5eNaImAgNnS	věřit
bankám	banka	k1gFnPc3	banka
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc1	třetina
přiznivců	přiznivec	k1gMnPc2	přiznivec
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
Sofres-Factum	Sofres-Factum	k1gNnSc4	Sofres-Factum
z	z	k7c2	z
konce	konec	k1gInSc2	konec
července	červenec	k1gInSc2	červenec
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
by	by	kYmCp3nP	by
KSČM	KSČM	kA	KSČM
volilo	volit	k5eAaImAgNnS	volit
20,4	[number]	k4	20,4
%	%	kIx~	%
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
stala	stát	k5eAaPmAgFnS	stát
druhou	druhý	k4xOgFnSc7	druhý
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
stranou	strana	k1gFnSc7	strana
po	po	k7c6	po
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
agentura	agentura	k1gFnSc1	agentura
APA	APA	kA	APA
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
vytkla	vytknout	k5eAaPmAgFnS	vytknout
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
kandidátní	kandidátní	k2eAgFnSc4d1	kandidátní
zemi	zem	k1gFnSc4	zem
do	do	k7c2	do
EU	EU	kA	EU
<g/>
,	,	kIx,	,
pomalé	pomalý	k2eAgNnSc1d1	pomalé
přejímání	přejímání	k1gNnSc1	přejímání
legislativy	legislativa	k1gFnSc2	legislativa
<g/>
,	,	kIx,	,
špatné	špatný	k2eAgNnSc1d1	špatné
fungování	fungování	k1gNnSc1	fungování
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
korupci	korupce	k1gFnSc6	korupce
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
také	také	k9	také
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
Sofres-Factum	Sofres-Factum	k1gNnSc4	Sofres-Factum
KSČM	KSČM	kA	KSČM
stala	stát	k5eAaPmAgFnS	stát
též	též	k9	též
druhou	druhý	k4xOgFnSc7	druhý
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
stranou	strana	k1gFnSc7	strana
v	v	k7c6	v
poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
doplňovacích	doplňovací	k2eAgFnPc6d1	doplňovací
senátních	senátní	k2eAgFnPc6d1	senátní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
kandidát	kandidát	k1gMnSc1	kandidát
KSČM	KSČM	kA	KSČM
Stanislav	Stanislav	k1gMnSc1	Stanislav
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
někdejší	někdejší	k2eAgMnSc1d1	někdejší
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
obdržel	obdržet	k5eAaPmAgMnS	obdržet
necelých	celý	k2eNgNnPc2d1	necelé
5,5	[number]	k4	5,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
přijal	přijmout	k5eAaPmAgInS	přijmout
kladně	kladně	k6eAd1	kladně
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
dle	dle	k7c2	dle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
se	se	k3xPyFc4	se
alespoň	alespoň	k9	alespoň
voliči	volič	k1gMnPc1	volič
obrací	obracet	k5eAaImIp3nP	obracet
od	od	k7c2	od
podpory	podpora	k1gFnSc2	podpora
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
když	když	k8xS	když
zvolili	zvolit	k5eAaPmAgMnP	zvolit
již	již	k6eAd1	již
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
nezávislého	závislý	k2eNgMnSc2d1	nezávislý
kandidáta	kandidát	k1gMnSc2	kandidát
Václava	Václav	k1gMnSc2	Václav
Fischera	Fischer	k1gMnSc2	Fischer
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
STEM	sto	k4xCgNnSc7	sto
voličům	volič	k1gMnPc3	volič
KSČM	KSČM	kA	KSČM
nejvíce	nejvíce	k6eAd1	nejvíce
vadil	vadit	k5eAaImAgInS	vadit
způsob	způsob	k1gInSc1	způsob
volební	volební	k2eAgFnSc2d1	volební
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
v	v	k7c6	v
inzerátu	inzerát	k1gInSc6	inzerát
ODS	ODS	kA	ODS
nařknul	nařknout	k5eAaPmAgInS	nařknout
Václava	Václav	k1gMnSc4	Václav
Fischera	Fischer	k1gMnSc4	Fischer
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přijal	přijmout	k5eAaPmAgMnS	přijmout
podporu	podpora	k1gFnSc4	podpora
komunistů	komunista	k1gMnPc2	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
to	ten	k3xDgNnSc4	ten
tvrdě	tvrdě	k6eAd1	tvrdě
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
září	září	k1gNnSc2	září
1999	[number]	k4	1999
měla	mít	k5eAaImAgFnS	mít
KSČM	KSČM	kA	KSČM
mítink	mítink	k1gInSc4	mítink
na	na	k7c6	na
Kunětické	kunětický	k2eAgFnSc6d1	Kunětická
hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
prodeje	prodej	k1gInSc2	prodej
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
koupit	koupit	k5eAaPmF	koupit
pěticípé	pěticípý	k2eAgFnPc4d1	pěticípá
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
kompaktní	kompaktní	k2eAgInSc4d1	kompaktní
disk	disk	k1gInSc4	disk
s	s	k7c7	s
revolučními	revoluční	k2eAgFnPc7d1	revoluční
písněmi	píseň	k1gFnPc7	píseň
a	a	k8xC	a
propagandistické	propagandistický	k2eAgInPc4d1	propagandistický
časopisy	časopis	k1gInPc4	časopis
Naše	náš	k3xOp1gNnSc1	náš
Pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
Pravda	pravda	k1gFnSc1	pravda
a	a	k8xC	a
Severočeská	severočeský	k2eAgFnSc1d1	Severočeská
Pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
plné	plný	k2eAgFnPc1d1	plná
stanov	stanova	k1gFnPc2	stanova
Společnosti	společnost	k1gFnSc3	společnost
česko-kubánského	českoubánský	k2eAgNnSc2d1	česko-kubánský
přátelství	přátelství	k1gNnSc2	přátelství
a	a	k8xC	a
Die	Die	k1gMnSc5	Die
Rote	Rotus	k1gMnSc5	Rotus
Fahne	Fahn	k1gMnSc5	Fahn
<g/>
,	,	kIx,	,
ústředního	ústřední	k2eAgInSc2d1	ústřední
orgánu	orgán	k1gInSc2	orgán
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Přítomna	přítomen	k2eAgFnSc1d1	přítomna
byla	být	k5eAaImAgFnS	být
i	i	k9	i
výstava	výstava	k1gFnSc1	výstava
fotografií	fotografia	k1gFnPc2	fotografia
"	"	kIx"	"
<g/>
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
varuje	varovat	k5eAaImIp3nS	varovat
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
pódiem	pódium	k1gNnSc7	pódium
byla	být	k5eAaImAgFnS	být
vystavěna	vystavět	k5eAaPmNgFnS	vystavět
typicky	typicky	k6eAd1	typicky
komunistická	komunistický	k2eAgNnPc4d1	komunistické
hesla	heslo	k1gNnPc4	heslo
"	"	kIx"	"
<g/>
Za	za	k7c4	za
občanskou	občanský	k2eAgFnSc4d1	občanská
sociální	sociální	k2eAgFnSc4d1	sociální
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Socialismus	socialismus	k1gInSc1	socialismus
šance	šance	k1gFnSc2	šance
pro	pro	k7c4	pro
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
setkání	setkání	k1gNnSc6	setkání
<g/>
,	,	kIx,	,
složeného	složený	k2eAgNnSc2d1	složené
spíše	spíše	k9	spíše
z	z	k7c2	z
šedesátiletých	šedesátiletý	k2eAgMnPc2d1	šedesátiletý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
nechyběla	chybět	k5eNaImAgFnS	chybět
ani	ani	k8xC	ani
dechovka	dechovka	k1gFnSc1	dechovka
<g/>
.	.	kIx.	.
</s>
<s>
Moderátorka	moderátorka	k1gFnSc1	moderátorka
v	v	k7c6	v
tričku	tričko	k1gNnSc6	tričko
KSČM	KSČM	kA	KSČM
promluvila	promluvit	k5eAaPmAgFnS	promluvit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Za	za	k7c4	za
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
zničeno	zničen	k2eAgNnSc1d1	zničeno
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
bývalo	bývat	k5eAaImAgNnS	bývat
chloubou	chlouba	k1gFnSc7	chlouba
a	a	k8xC	a
zárukou	záruka	k1gFnSc7	záruka
soběstačnosti	soběstačnost	k1gFnSc2	soběstačnost
našeho	náš	k3xOp1gInSc2	náš
státu	stát	k1gInSc2	stát
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mítinku	mítink	k1gInSc3	mítink
se	se	k3xPyFc4	se
samozřejmě	samozřejmě	k6eAd1	samozřejmě
účastnil	účastnit	k5eAaImAgMnS	účastnit
i	i	k9	i
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
Miroslav	Miroslav	k1gMnSc1	Miroslav
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
kolegové	kolega	k1gMnPc1	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc1	jejich
vláda	vláda	k1gFnSc1	vláda
může	moct	k5eAaImIp3nS	moct
za	za	k7c4	za
horší	zlý	k2eAgFnPc4d2	horší
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
slušný	slušný	k2eAgInSc4d1	slušný
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Publikum	publikum	k1gNnSc1	publikum
reagovalo	reagovat	k5eAaBmAgNnS	reagovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pryč	pryč	k6eAd1	pryč
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
<g/>
!	!	kIx.	!
</s>
<s>
Hanba	hanba	k6eAd1	hanba
jim	on	k3xPp3gMnPc3	on
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
dopustila	dopustit	k5eAaPmAgFnS	dopustit
neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
sprostého	sprostý	k2eAgInSc2d1	sprostý
podvodu	podvod	k1gInSc2	podvod
na	na	k7c6	na
občanech	občan	k1gMnPc6	občan
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
navážení	navážení	k1gNnSc4	navážení
se	se	k3xPyFc4	se
do	do	k7c2	do
sociálnědemokratické	sociálnědemokratický	k2eAgFnSc2d1	sociálnědemokratická
vlády	vláda	k1gFnSc2	vláda
svolal	svolat	k5eAaPmAgInS	svolat
"	"	kIx"	"
<g/>
Severoatlantický	severoatlantický	k2eAgInSc1d1	severoatlantický
pakt	pakt	k1gInSc1	pakt
je	být	k5eAaImIp3nS	být
zločinecká	zločinecký	k2eAgFnSc1d1	zločinecká
organizace	organizace	k1gFnSc1	organizace
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
odměnou	odměna	k1gFnSc7	odměna
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
silný	silný	k2eAgInSc4d1	silný
aplaus	aplaus	k1gInSc4	aplaus
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
ujišťoval	ujišťovat	k5eAaImAgMnS	ujišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
KSČM	KSČM	kA	KSČM
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
nezlomná	zlomný	k2eNgFnSc1d1	nezlomná
a	a	k8xC	a
neměla	mít	k5eNaImAgFnS	mít
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
vládami	vláda	k1gFnPc7	vláda
"	"	kIx"	"
<g/>
neprosperity	neprosperit	k1gInPc7	neprosperit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavní	hlavní	k2eAgFnSc7d1	hlavní
myšlenkou	myšlenka	k1gFnSc7	myšlenka
komunismu	komunismus	k1gInSc2	komunismus
nebyly	být	k5eNaImAgInP	být
jen	jen	k9	jen
gulagy	gulag	k1gInPc1	gulag
a	a	k8xC	a
politické	politický	k2eAgInPc1d1	politický
procesy	proces	k1gInPc1	proces
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
sociální	sociální	k2eAgFnSc1d1	sociální
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
a	a	k8xC	a
demokracie	demokracie	k1gFnSc1	demokracie
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
bohaté	bohatý	k2eAgMnPc4d1	bohatý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
dostal	dostat	k5eAaPmAgInS	dostat
řeč	řeč	k1gFnSc4	řeč
mj.	mj.	kA	mj.
i	i	k9	i
první	první	k4xOgMnSc1	první
rada	rada	k1gMnSc1	rada
jugoslávského	jugoslávský	k2eAgNnSc2d1	jugoslávské
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
děkoval	děkovat	k5eAaImAgMnS	děkovat
za	za	k7c4	za
pomoc	pomoc	k1gFnSc4	pomoc
srbskému	srbský	k2eAgInSc3d1	srbský
lidu	lid	k1gInSc3	lid
<g/>
,	,	kIx,	,
stiženému	stižený	k2eAgMnSc3d1	stižený
barbarskou	barbarský	k2eAgFnSc7d1	barbarská
agresí	agrese	k1gFnSc7	agrese
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Politická	politický	k2eAgFnSc1d1	politická
část	část	k1gFnSc1	část
schůze	schůze	k1gFnSc1	schůze
skončila	skončit	k5eAaPmAgFnS	skončit
Internacionálou	Internacionála	k1gFnSc7	Internacionála
z	z	k7c2	z
reproduktorů	reproduktor	k1gInPc2	reproduktor
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
září	září	k1gNnSc6	září
1999	[number]	k4	1999
by	by	k9	by
podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
STEM	sto	k4xCgNnSc7	sto
volilo	volit	k5eAaImAgNnS	volit
KSČM	KSČM	kA	KSČM
20,5	[number]	k4	20,5
%	%	kIx~	%
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
strana	strana	k1gFnSc1	strana
stala	stát	k5eAaPmAgFnS	stát
po	po	k7c6	po
ODS	ODS	kA	ODS
s	s	k7c7	s
20,9	[number]	k4	20,9
%	%	kIx~	%
druhou	druhý	k4xOgFnSc7	druhý
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
stranou	strana	k1gFnSc7	strana
v	v	k7c6	v
poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
i	i	k9	i
výzkum	výzkum	k1gInSc1	výzkum
Sofres-Factum	Sofres-Factum	k1gNnSc1	Sofres-Factum
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
září	září	k1gNnSc2	září
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Účinně	účinně	k6eAd1	účinně
čelit	čelit	k5eAaImF	čelit
sílícímu	sílící	k2eAgInSc3d1	sílící
vlivu	vliv	k1gInSc3	vliv
komunistů	komunista	k1gMnPc2	komunista
byl	být	k5eAaImAgInS	být
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
smyslů	smysl	k1gInPc2	smysl
dohody	dohoda	k1gFnSc2	dohoda
Čtyřkoalice	Čtyřkoalice	k1gFnSc2	Čtyřkoalice
<g/>
,	,	kIx,	,
podepsané	podepsaný	k2eAgInPc4d1	podepsaný
v	v	k7c6	v
září	září	k1gNnSc6	září
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
<g/>
KSČM	KSČM	kA	KSČM
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nelíbilo	líbit	k5eNaImAgNnS	líbit
právo	právo	k1gNnSc1	právo
prezidenta	prezident	k1gMnSc2	prezident
udílet	udílet	k5eAaImF	udílet
milosti	milost	k1gFnSc2	milost
a	a	k8xC	a
zastavovat	zastavovat	k5eAaImF	zastavovat
trestní	trestní	k2eAgNnSc4d1	trestní
stíhání	stíhání	k1gNnSc4	stíhání
<g/>
.	.	kIx.	.
<g/>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Gross	Gross	k1gMnSc1	Gross
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
iDNES	iDNES	k?	iDNES
v	v	k7c6	v
září	září	k1gNnSc6	září
1999	[number]	k4	1999
vzpomínal	vzpomínat	k5eAaImAgInS	vzpomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
žádala	žádat	k5eAaImAgFnS	žádat
zákaz	zákaz	k1gInSc4	zákaz
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
<s>
Osobně	osobně	k6eAd1	osobně
doufal	doufat	k5eAaImAgMnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
preference	preference	k1gFnSc1	preference
KSČM	KSČM	kA	KSČM
budou	být	k5eAaImBp3nP	být
při	při	k7c6	při
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
skutečným	skutečný	k2eAgInSc7d1	skutečný
volebním	volební	k2eAgInSc7d1	volební
výsledkem	výsledek	k1gInSc7	výsledek
podstatně	podstatně	k6eAd1	podstatně
nižší	nízký	k2eAgInPc4d2	nižší
<g/>
.2	.2	k4	.2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1999	[number]	k4	1999
Miroslav	Miroslav	k1gMnSc1	Miroslav
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
situace	situace	k1gFnPc4	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
s	s	k7c7	s
KSČM	KSČM	kA	KSČM
nikdo	nikdo	k3yNnSc1	nikdo
nechce	chtít	k5eNaImIp3nS	chtít
do	do	k7c2	do
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
získat	získat	k5eAaPmF	získat
většinu	většina	k1gFnSc4	většina
národa	národ	k1gInSc2	národ
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
prý	prý	k9	prý
spatřují	spatřovat	k5eAaImIp3nP	spatřovat
v	v	k7c6	v
KSČM	KSČM	kA	KSČM
určitou	určitý	k2eAgFnSc4d1	určitá
naději	naděje	k1gFnSc4	naděje
<g/>
.	.	kIx.	.
</s>
<s>
KSČM	KSČM	kA	KSČM
zase	zase	k9	zase
spatřuje	spatřovat	k5eAaImIp3nS	spatřovat
"	"	kIx"	"
<g/>
strašáka	strašák	k1gMnSc4	strašák
<g/>
"	"	kIx"	"
ve	v	k7c6	v
Václavu	Václav	k1gMnSc6	Václav
Klausovi	Klaus	k1gMnSc6	Klaus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
viníkem	viník	k1gMnSc7	viník
za	za	k7c4	za
současnou	současný	k2eAgFnSc4d1	současná
neutěšenou	utěšený	k2eNgFnSc4d1	neutěšená
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
Petra	Petr	k1gMnSc2	Petr
Hlásenského	Hlásenský	k2eAgInSc2d1	Hlásenský
mají	mít	k5eAaImIp3nP	mít
komunisté	komunista	k1gMnPc1	komunista
podporu	podpor	k1gInSc2	podpor
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
nad	nad	k7c4	nad
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
prostě	prostě	k9	prostě
nikdo	nikdo	k3yNnSc1	nikdo
nezmění	změnit	k5eNaPmIp3nS	změnit
<g/>
.12	.12	k4	.12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
úmysl	úmysl	k1gInSc4	úmysl
ODS	ODS	kA	ODS
vytvořit	vytvořit	k5eAaPmF	vytvořit
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
třemi	tři	k4xCgFnPc7	tři
různými	různý	k2eAgFnPc7d1	různá
možnostmi	možnost	k1gFnPc7	možnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
rostoucímu	rostoucí	k2eAgInSc3d1	rostoucí
vlivu	vliv	k1gInSc3	vliv
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
předčasným	předčasný	k2eAgFnPc3d1	předčasná
volbám	volba	k1gFnPc3	volba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
STEM	sto	k4xCgNnSc7	sto
uvedlo	uvést	k5eAaPmAgNnS	uvést
50	[number]	k4	50
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
socialismus	socialismus	k1gInSc1	socialismus
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xC	jako
současný	současný	k2eAgInSc1d1	současný
režim	režim	k1gInSc1	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1999	[number]	k4	1999
se	se	k3xPyFc4	se
preference	preference	k1gFnPc1	preference
KSČM	KSČM	kA	KSČM
pohybovaly	pohybovat	k5eAaImAgFnP	pohybovat
mezi	mezi	k7c4	mezi
17	[number]	k4	17
až	až	k9	až
20	[number]	k4	20
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
říjnových	říjnový	k2eAgFnPc2d1	říjnová
preferencí	preference	k1gFnPc2	preference
IVVM	IVVM	kA	IVVM
by	by	kYmCp3nP	by
komunisté	komunista	k1gMnPc1	komunista
získali	získat	k5eAaPmAgMnP	získat
23	[number]	k4	23
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
nejoblíbenější	oblíbený	k2eAgFnSc7d3	nejoblíbenější
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
předseda	předseda	k1gMnSc1	předseda
US	US	kA	US
Jan	Jan	k1gMnSc1	Jan
Ruml	Ruml	k1gMnSc1	Ruml
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
dialogu	dialog	k1gInSc6	dialog
s	s	k7c7	s
demokratickými	demokratický	k2eAgFnPc7d1	demokratická
stranami	strana	k1gFnPc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Unie	unie	k1gFnSc1	unie
Svobody	Svoboda	k1gMnSc2	Svoboda
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
uvažovala	uvažovat	k5eAaImAgFnS	uvažovat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
koalici	koalice	k1gFnSc4	koalice
ODS	ODS	kA	ODS
a	a	k8xC	a
ČSSD	ČSSD	kA	ČSSD
nebo	nebo	k8xC	nebo
předčasných	předčasný	k2eAgFnPc6d1	předčasná
volbách	volba	k1gFnPc6	volba
<g/>
.25	.25	k4	.25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
Miloslav	Miloslav	k1gMnSc1	Miloslav
Ransdorf	Ransdorf	k1gMnSc1	Ransdorf
v	v	k7c4	v
rozhovor	rozhovor	k1gInSc4	rozhovor
iDNES	iDNES	k?	iDNES
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
KSČM	KSČM	kA	KSČM
již	již	k6eAd1	již
omluvila	omluvit	k5eAaPmAgFnS	omluvit
slušným	slušný	k2eAgInSc7d1	slušný
lidem	lid	k1gInSc7	lid
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
zločiny	zločin	k1gInPc4	zločin
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
je	on	k3xPp3gNnSc4	on
to	ten	k3xDgNnSc4	ten
není	být	k5eNaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
stalo	stát	k5eAaPmAgNnS	stát
dávno	dávno	k6eAd1	dávno
<g/>
.	.	kIx.	.
</s>
<s>
Současní	současný	k2eAgMnPc1d1	současný
komunisté	komunista	k1gMnPc1	komunista
prý	prý	k9	prý
stalinismus	stalinismus	k1gInSc1	stalinismus
překonali	překonat	k5eAaPmAgMnP	překonat
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
křesťanů	křesťan	k1gMnPc2	křesťan
s	s	k7c7	s
inkvizicí	inkvizice	k1gFnSc7	inkvizice
<g/>
,	,	kIx,	,
křížovými	křížový	k2eAgFnPc7d1	křížová
výpravami	výprava	k1gFnPc7	výprava
apod.	apod.	kA	apod.
Únor	únor	k1gInSc4	únor
1948	[number]	k4	1948
opakovat	opakovat	k5eAaImF	opakovat
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
chladno	chladno	k6eAd1	chladno
<g/>
.	.	kIx.	.
</s>
<s>
Politické	politický	k2eAgInPc1d1	politický
procesy	proces	k1gInPc1	proces
z	z	k7c2	z
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
neobhajuje	obhajovat	k5eNaImIp3nS	obhajovat
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
něj	on	k3xPp3gMnSc2	on
daly	dát	k5eAaPmAgInP	dát
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
srovnat	srovnat	k5eAaPmF	srovnat
se	s	k7c7	s
zločiny	zločin	k1gInPc7	zločin
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
po	po	k7c6	po
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
KSČM	KSČM	kA	KSČM
stojí	stát	k5eAaImIp3nS	stát
proti	proti	k7c3	proti
politickému	politický	k2eAgInSc3d1	politický
monopolu	monopol	k1gInSc3	monopol
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
marxisticky	marxisticky	k6eAd1	marxisticky
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
proevropská	proevropský	k2eAgFnSc1d1	proevropská
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
se	s	k7c7	s
KSČM	KSČM	kA	KSČM
se	se	k3xPyFc4	se
nepostavila	postavit	k5eNaPmAgFnS	postavit
pro	pro	k7c4	pro
bránění	bránění	k1gNnSc4	bránění
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
v	v	k7c6	v
Čečensku	Čečensko	k1gNnSc6	Čečensko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
islámské	islámský	k2eAgNnSc1d1	islámské
právo	právo	k1gNnSc1	právo
nesrovnává	srovnávat	k5eNaImIp3nS	srovnávat
s	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
občanského	občanský	k2eAgInSc2d1	občanský
principu	princip	k1gInSc2	princip
<g/>
.	.	kIx.	.
</s>
<s>
KSČM	KSČM	kA	KSČM
není	být	k5eNaImIp3nS	být
nacionalistickou	nacionalistický	k2eAgFnSc7d1	nacionalistická
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odmítá	odmítat	k5eAaImIp3nS	odmítat
globalizaci	globalizace	k1gFnSc4	globalizace
<g/>
.26	.26	k4	.26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
navrhlo	navrhnout	k5eAaPmAgNnS	navrhnout
v	v	k7c6	v
novele	novela	k1gFnSc6	novela
trestního	trestní	k2eAgInSc2d1	trestní
zákona	zákon	k1gInSc2	zákon
pět	pět	k4xCc1	pět
poslanců	poslanec	k1gMnPc2	poslanec
z	z	k7c2	z
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
Unie	unie	k1gFnSc2	unie
svobody	svoboda	k1gFnSc2	svoboda
důraznější	důrazný	k2eAgInSc4d2	důraznější
postup	postup	k1gInSc4	postup
státních	státní	k2eAgInPc2d1	státní
orgánů	orgán	k1gInPc2	orgán
při	při	k7c6	při
stíhání	stíhání	k1gNnSc6	stíhání
projevů	projev	k1gInPc2	projev
fašismu	fašismus	k1gInSc2	fašismus
i	i	k8xC	i
komunistické	komunistický	k2eAgFnSc2d1	komunistická
ideologie	ideologie	k1gFnSc2	ideologie
<g/>
.	.	kIx.	.
</s>
<s>
Důvodová	důvodový	k2eAgFnSc1d1	Důvodová
zpráva	zpráva	k1gFnSc1	zpráva
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
současná	současný	k2eAgFnSc1d1	současná
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
od	od	k7c2	od
třídní	třídní	k2eAgFnSc2d1	třídní
teorie	teorie	k1gFnSc2	teorie
doposud	doposud	k6eAd1	doposud
zřetelně	zřetelně	k6eAd1	zřetelně
nedistancovala	distancovat	k5eNaBmAgFnS	distancovat
<g/>
.	.	kIx.	.
<g/>
Organizátoři	organizátor	k1gMnPc1	organizátor
výročí	výročí	k1gNnSc2	výročí
listopadové	listopadový	k2eAgFnSc2d1	listopadová
revoluce	revoluce	k1gFnSc2	revoluce
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
totalitní	totalitní	k2eAgInSc1d1	totalitní
režim	režim	k1gInSc1	režim
připomene	připomenout	k5eAaPmIp3nS	připomenout
i	i	k9	i
"	"	kIx"	"
<g/>
Rudý	rudý	k2eAgInSc1d1	rudý
koutek	koutek	k1gInSc1	koutek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
najdou	najít	k5eAaPmIp3nP	najít
například	například	k6eAd1	například
Leninovy	Leninův	k2eAgInPc4d1	Leninův
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc4	obraz
komunistických	komunistický	k2eAgMnPc2d1	komunistický
pohlavárů	pohlavár	k1gMnPc2	pohlavár
<g/>
,	,	kIx,	,
rudé	rudý	k2eAgFnPc4d1	rudá
vlaječky	vlaječka	k1gFnPc4	vlaječka
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
relikvie	relikvie	k1gFnPc4	relikvie
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
koutku	koutek	k1gInSc2	koutek
není	být	k5eNaImIp3nS	být
zvýšit	zvýšit	k5eAaPmF	zvýšit
preference	preference	k1gFnSc1	preference
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
připomenout	připomenout	k5eAaPmF	připomenout
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
<g/>
Snahou	snaha	k1gFnSc7	snaha
čtyřkoalice	čtyřkoalice	k1gFnSc2	čtyřkoalice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přistoupila	přistoupit	k5eAaPmAgFnS	přistoupit
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
vyjednávání	vyjednávání	k1gNnSc3	vyjednávání
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
poslankyně	poslankyně	k1gFnSc2	poslankyně
KSČM	KSČM	kA	KSČM
spíše	spíše	k9	spíše
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
zviditelnění	zviditelnění	k1gNnSc1	zviditelnění
než	než	k8xS	než
skutečná	skutečný	k2eAgFnSc1d1	skutečná
snaha	snaha	k1gFnSc1	snaha
po	po	k7c6	po
svržení	svržení	k1gNnSc6	svržení
této	tento	k3xDgFnSc2	tento
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
nastolení	nastolení	k1gNnSc4	nastolení
vlády	vláda	k1gFnSc2	vláda
jiné	jiný	k2eAgInPc1d1	jiný
<g/>
.	.	kIx.	.
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
R.	R.	kA	R.
Caletka	Caletka	k?	Caletka
zhodnotil	zhodnotit	k5eAaPmAgMnS	zhodnotit
odpovídání	odpovídání	k1gNnSc4	odpovídání
p.	p.	k?	p.
Ransdorfa	Ransdorf	k1gMnSc2	Ransdorf
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
iDNES	iDNES	k?	iDNES
rčením	rčení	k1gNnPc3	rčení
vlk	vlk	k1gMnSc1	vlk
se	se	k3xPyFc4	se
nažral	nažrat	k5eAaPmAgMnS	nažrat
a	a	k8xC	a
koza	koza	k1gFnSc1	koza
zůstala	zůstat	k5eAaPmAgFnS	zůstat
celá	celý	k2eAgFnSc1d1	celá
<g/>
.	.	kIx.	.
</s>
<s>
Randsdorf	Randsdorf	k1gInSc1	Randsdorf
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
na	na	k7c4	na
otázky	otázka	k1gFnPc4	otázka
bez	bez	k7c2	bez
jasného	jasný	k2eAgNnSc2d1	jasné
jasného	jasný	k2eAgNnSc2d1	jasné
vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
zadaných	zadaný	k2eAgFnPc2d1	zadaná
otázek	otázka	k1gFnPc2	otázka
a	a	k8xC	a
šlo	jít	k5eAaImAgNnS	jít
mu	on	k3xPp3gMnSc3	on
především	především	k9	především
o	o	k7c4	o
svojí	svojit	k5eAaImIp3nS	svojit
osobní	osobní	k2eAgFnSc1d1	osobní
prestiž	prestiž	k1gFnSc1	prestiž
a	a	k8xC	a
výhody	výhoda	k1gFnPc1	výhoda
pro	pro	k7c4	pro
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
<g/>
František	František	k1gMnSc1	František
Kroupa	Kroupa	k1gMnSc1	Kroupa
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
senátním	senátní	k2eAgNnSc7d1	senátní
schválením	schválení	k1gNnSc7	schválení
vlastního	vlastní	k2eAgInSc2d1	vlastní
návrhu	návrh	k1gInSc2	návrh
zákona	zákon	k1gInSc2	zákon
na	na	k7c6	na
zřízení	zřízení	k1gNnSc6	zřízení
Památníku	památník	k1gInSc2	památník
doby	doba	k1gFnSc2	doba
nesvobody	nesvoboda	k1gFnSc2	nesvoboda
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
komunismus	komunismus	k1gInSc4	komunismus
po	po	k7c6	po
nacismu	nacismus	k1gInSc6	nacismus
nové	nový	k2eAgNnSc4d1	nové
otroctví	otroctví	k1gNnSc4	otroctví
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
z	z	k7c2	z
listopadu	listopad	k1gInSc2	listopad
1999	[number]	k4	1999
od	od	k7c2	od
agentury	agentura	k1gFnSc2	agentura
Sofres-Factum	Sofres-Factum	k1gNnSc1	Sofres-Factum
považuje	považovat	k5eAaImIp3nS	považovat
32	[number]	k4	32
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
Sametovou	sametový	k2eAgFnSc4d1	sametová
revoluci	revoluce	k1gFnSc4	revoluce
za	za	k7c4	za
špatnou	špatný	k2eAgFnSc4d1	špatná
událost	událost	k1gFnSc4	událost
<g/>
.	.	kIx.	.
58	[number]	k4	58
procent	procento	k1gNnPc2	procento
voličů	volič	k1gMnPc2	volič
KSČM	KSČM	kA	KSČM
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
Sametová	sametový	k2eAgFnSc1d1	sametová
revoluce	revoluce	k1gFnSc1	revoluce
špatně	špatně	k6eAd1	špatně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
30	[number]	k4	30
%	%	kIx~	%
společnosti	společnost	k1gFnSc2	společnost
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
po	po	k7c6	po
listopadu	listopad	k1gInSc6	listopad
měla	mít	k5eAaImAgFnS	mít
udělat	udělat	k5eAaPmF	udělat
tlustá	tlustý	k2eAgFnSc1d1	tlustá
čára	čára	k1gFnSc1	čára
za	za	k7c7	za
minulostí	minulost	k1gFnSc7	minulost
komunistických	komunistický	k2eAgInPc2d1	komunistický
zločinů	zločin	k1gInPc2	zločin
<g/>
.	.	kIx.	.
18	[number]	k4	18
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
by	by	kYmCp3nP	by
chtělo	chtít	k5eAaImAgNnS	chtít
žít	žít	k5eAaImF	žít
v	v	k7c6	v
minulém	minulý	k2eAgInSc6d1	minulý
režimu	režim	k1gInSc6	režim
a	a	k8xC	a
27	[number]	k4	27
%	%	kIx~	%
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
55	[number]	k4	55
%	%	kIx~	%
v	v	k7c6	v
současném	současný	k2eAgInSc6d1	současný
<g/>
.17	.17	k4	.17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
při	při	k7c6	při
10	[number]	k4	10
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
Sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
rozvinuli	rozvinout	k5eAaPmAgMnP	rozvinout
komunisté	komunista	k1gMnPc1	komunista
transparent	transparent	k1gInSc4	transparent
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
Pryč	pryč	k1gFnSc4	pryč
s	s	k7c7	s
kapitalismem	kapitalismus	k1gInSc7	kapitalismus
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
projevu	projev	k1gInSc2	projev
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
při	při	k7c6	při
výročí	výročí	k1gNnSc6	výročí
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
znamenal	znamenat	k5eAaImAgInS	znamenat
pád	pád	k1gInSc1	pád
komunismu	komunismus	k1gInSc2	komunismus
osvobození	osvobození	k1gNnSc2	osvobození
milionů	milion	k4xCgInPc2	milion
utlačených	utlačený	k2eAgInPc2d1	utlačený
a	a	k8xC	a
ponížených	ponížený	k2eAgFnPc2d1	ponížená
lidských	lidský	k2eAgFnPc2d1	lidská
bytostí	bytost	k1gFnPc2	bytost
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
STEM	sto	k4xCgNnSc7	sto
by	by	kYmCp3nS	by
KSČM	KSČM	kA	KSČM
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
volby	volba	k1gFnPc4	volba
se	s	k7c7	s
23	[number]	k4	23
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Jan	Jan	k1gMnSc1	Jan
Ruml	Ruml	k1gMnSc1	Ruml
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rezignuje	rezignovat	k5eAaBmIp3nS	rezignovat
na	na	k7c4	na
funkci	funkce	k1gFnSc4	funkce
předsedy	předseda	k1gMnSc2	předseda
Unie	unie	k1gFnSc2	unie
svobody	svoboda	k1gFnSc2	svoboda
kvůli	kvůli	k7c3	kvůli
současné	současný	k2eAgFnSc3d1	současná
politické	politický	k2eAgFnSc3d1	politická
situaci	situace	k1gFnSc3	situace
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
KSČM	KSČM	kA	KSČM
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
myslí	myslet	k5eAaImIp3nS	myslet
tentokrát	tentokrát	k6eAd1	tentokrát
vážně	vážně	k6eAd1	vážně
<g/>
,	,	kIx,	,
tak	tak	k9	tak
asi	asi	k9	asi
přece	přece	k9	přece
jen	jen	k9	jen
někde	někde	k6eAd1	někde
uvnitř	uvnitř	k7c2	uvnitř
má	mít	k5eAaImIp3nS	mít
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
mu	on	k3xPp3gMnSc3	on
velí	velet	k5eAaImIp3nS	velet
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
veřejně	veřejně	k6eAd1	veřejně
přiznat	přiznat	k5eAaPmF	přiznat
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
tady	tady	k6eAd1	tady
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
.2	.2	k4	.2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
novelu	novela	k1gFnSc4	novela
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
stanovující	stanovující	k2eAgInPc1d1	stanovující
že	že	k8xS	že
zločiny	zločin	k1gInPc1	zločin
komunismu	komunismus	k1gInSc2	komunismus
nebudou	být	k5eNaImBp3nP	být
promlčeny	promlčen	k2eAgInPc1d1	promlčen
<g/>
,	,	kIx,	,
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
137	[number]	k4	137
ze	z	k7c2	z
179	[number]	k4	179
přítomných	přítomný	k2eAgMnPc2d1	přítomný
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
před	před	k7c7	před
sjezdem	sjezd	k1gInSc7	sjezd
nezměnil	změnit	k5eNaPmAgInS	změnit
názor	názor	k1gInSc1	názor
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
NATO	nato	k6eAd1	nato
transformovat	transformovat	k5eAaBmF	transformovat
nebo	nebo	k8xC	nebo
opustit	opustit	k5eAaPmF	opustit
a	a	k8xC	a
mezitím	mezitím	k6eAd1	mezitím
<g />
.	.	kIx.	.
</s>
<s>
pozastavit	pozastavit	k5eAaPmF	pozastavit
zapojení	zapojení	k1gNnSc4	zapojení
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
do	do	k7c2	do
vojensko-politických	vojenskoolitický	k2eAgFnPc2d1	vojensko-politická
aktivit	aktivita	k1gFnPc2	aktivita
této	tento	k3xDgFnSc2	tento
aliance	aliance	k1gFnSc2	aliance
<g/>
.	.	kIx.	.
54	[number]	k4	54
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
KSČM	KSČM	kA	KSČM
reformovala	reformovat	k5eAaBmAgFnS	reformovat
na	na	k7c4	na
moderní	moderní	k2eAgFnSc4d1	moderní
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
28	[number]	k4	28
%	%	kIx~	%
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
postavena	postavit	k5eAaPmNgFnS	postavit
mimo	mimo	k7c4	mimo
zákon	zákon	k1gInSc4	zákon
a	a	k8xC	a
až	až	k9	až
57	[number]	k4	57
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
s	s	k7c7	s
tvrzení	tvrzení	k1gNnPc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
stranu	strana	k1gFnSc4	strana
zastává	zastávat	k5eAaImIp3nS	zastávat
poctivě	poctivě	k6eAd1	poctivě
pracujících	pracující	k2eAgMnPc2d1	pracující
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
50	[number]	k4	50
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
si	se	k3xPyFc3	se
nemyslí	myslet	k5eNaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
KSČM	KSČM	kA	KSČM
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
,	,	kIx,	,
58	[number]	k4	58
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
že	že	k8xS	že
by	by	kYmCp3nS	by
vláda	vláda	k1gFnSc1	vláda
KSČM	KSČM	kA	KSČM
oživila	oživit	k5eAaPmAgFnS	oživit
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
,	,	kIx,	,
54	[number]	k4	54
%	%	kIx~	%
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
snáz	snadno	k6eAd2	snadno
získat	získat	k5eAaPmF	získat
byty	byt	k1gInPc4	byt
<g/>
,	,	kIx,	,
45	[number]	k4	45
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nevyplatilo	vyplatit	k5eNaPmAgNnS	vyplatit
poctivě	poctivě	k6eAd1	poctivě
pracovat	pracovat	k5eAaImF	pracovat
a	a	k8xC	a
36	[number]	k4	36
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
znárodňoval	znárodňovat	k5eAaImAgInS	znárodňovat
majetek	majetek	k1gInSc1	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
315	[number]	k4	315
dotázaných	dotázaný	k2eAgMnPc2d1	dotázaný
dělníků	dělník	k1gMnPc2	dělník
si	se	k3xPyFc3	se
43	[number]	k4	43
%	%	kIx~	%
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
KSČM	KSČM	kA	KSČM
neměla	mít	k5eNaImAgFnS	mít
vládnout	vládnout	k5eAaImF	vládnout
<g/>
,	,	kIx,	,
57	[number]	k4	57
%	%	kIx~	%
že	že	k8xS	že
se	se	k3xPyFc4	se
nezastává	zastávat	k5eNaImIp3nS	zastávat
poctivě	poctivě	k6eAd1	poctivě
pracujících	pracující	k2eAgMnPc2d1	pracující
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
50	[number]	k4	50
%	%	kIx~	%
že	že	k8xS	že
by	by	kYmCp3nS	by
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
KSČM	KSČM	kA	KSČM
neklesla	klesnout	k5eNaPmAgFnS	klesnout
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
a	a	k8xC	a
46	[number]	k4	46
%	%	kIx~	%
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
takové	takový	k3xDgFnSc2	takový
vlády	vláda	k1gFnSc2	vláda
víc	hodně	k6eAd2	hodně
vyplatilo	vyplatit	k5eAaPmAgNnS	vyplatit
poctivě	poctivě	k6eAd1	poctivě
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vítězství	vítězství	k1gNnSc2	vítězství
KSČM	KSČM	kA	KSČM
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
pro	pro	k7c4	pro
18	[number]	k4	18
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
V.	V.	kA	V.
sjezdu	sjezd	k1gInSc2	sjezd
KSČM	KSČM	kA	KSČM
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
Miroslav	Miroslav	k1gMnSc1	Miroslav
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
počet	počet	k1gInSc1	počet
místopředsedů	místopředseda	k1gMnPc2	místopředseda
z	z	k7c2	z
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
čtyř	čtyři	k4xCgInPc2	čtyři
na	na	k7c4	na
pět	pět	k4xCc4	pět
<g/>
.	.	kIx.	.
</s>
<s>
Sjezdu	sjezd	k1gInSc2	sjezd
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
i	i	k9	i
Miloš	Miloš	k1gMnSc1	Miloš
Jakeš	Jakeš	k1gMnSc1	Jakeš
<g/>
,	,	kIx,	,
Jiřina	Jiřina	k1gFnSc1	Jiřina
Švorcová	švorcový	k2eAgFnSc1d1	Švorcová
nebo	nebo	k8xC	nebo
čínský	čínský	k2eAgMnSc1d1	čínský
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
občanů	občan	k1gMnPc2	občan
Žďáru	Žďár	k1gInSc2	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
protestovalo	protestovat	k5eAaBmAgNnS	protestovat
proti	proti	k7c3	proti
sjezdu	sjezd	k1gInSc3	sjezd
<g/>
.	.	kIx.	.
</s>
<s>
Moderátor	moderátor	k1gInSc1	moderátor
protikomunistické	protikomunistický	k2eAgFnSc2d1	protikomunistická
demonstrace	demonstrace	k1gFnSc2	demonstrace
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Komunismus	komunismus	k1gInSc1	komunismus
rozpoutal	rozpoutat	k5eAaPmAgInS	rozpoutat
válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
vlastnímu	vlastní	k2eAgInSc3d1	vlastní
lidu	lid	k1gInSc3	lid
a	a	k8xC	a
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
popravil	popravit	k5eAaPmAgMnS	popravit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
lidí	člověk	k1gMnPc2	člověk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nápisech	nápis	k1gInPc6	nápis
demonstrujících	demonstrující	k2eAgMnPc2d1	demonstrující
bylo	být	k5eAaImAgNnS	být
napsáno	napsat	k5eAaBmNgNnS	napsat
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Pohrobci	pohrobek	k1gMnPc1	pohrobek
Stalina	Stalin	k1gMnSc2	Stalin
<g/>
,	,	kIx,	,
nedělejte	dělat	k5eNaImRp2nP	dělat
nám	my	k3xPp1nPc3	my
zde	zde	k6eAd1	zde
ostudu	ostuda	k1gFnSc4	ostuda
a	a	k8xC	a
táhněte	táhnout	k5eAaImRp2nP	táhnout
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
,	,	kIx,	,
Zrůdy	zrůda	k1gFnPc4	zrůda
rudý	rudý	k2eAgInSc1d1	rudý
<g/>
,	,	kIx,	,
Zdravíme	zdravit	k5eAaImIp1nP	zdravit
starý	starý	k2eAgInSc4d1	starý
bolševiky	bolševik	k1gMnPc7	bolševik
<g/>
,	,	kIx,	,
Zločiny	zločin	k1gInPc7	zločin
komunismu	komunismus	k1gInSc2	komunismus
nesmí	smět	k5eNaImIp3nP	smět
být	být	k5eAaImF	být
zapomenuty	zapomenout	k5eAaPmNgFnP	zapomenout
nebo	nebo	k8xC	nebo
Mítink	mítink	k1gInSc1	mítink
proti	proti	k7c3	proti
skleróze	skleróza	k1gFnSc3	skleróza
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
se	se	k3xPyFc4	se
iniciativa	iniciativa	k1gFnSc1	iniciativa
Děkujeme	děkovat	k5eAaImIp1nP	děkovat
<g/>
,	,	kIx,	,
odejděte	odejít	k5eAaPmRp2nP	odejít
<g/>
!	!	kIx.	!
</s>
<s>
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
distancovala	distancovat	k5eAaBmAgFnS	distancovat
od	od	k7c2	od
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
nedemokratickou	demokratický	k2eNgFnSc4d1	nedemokratická
politickou	politický	k2eAgFnSc4d1	politická
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
těží	těžet	k5eAaImIp3nS	těžet
z	z	k7c2	z
neúspěšné	úspěšný	k2eNgFnSc2d1	neúspěšná
politické	politický	k2eAgFnSc2d1	politická
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
výzvě	výzva	k1gFnSc6	výzva
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1999	[number]	k4	1999
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
udělají	udělat	k5eAaPmIp3nP	udělat
vše	všechen	k3xTgNnSc4	všechen
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
občané	občan	k1gMnPc1	občan
ČR	ČR	kA	ČR
nevydali	vydat	k5eNaPmAgMnP	vydat
cestou	cesta	k1gFnSc7	cesta
ke	k	k7c3	k
komunismu	komunismus	k1gInSc3	komunismus
<g/>
.10	.10	k4	.10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vyzvat	vyzvat	k5eAaPmF	vyzvat
předsedu	předseda	k1gMnSc4	předseda
ČSSD	ČSSD	kA	ČSSD
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
o	o	k7c6	o
většinové	většinový	k2eAgFnSc6d1	většinová
vládě	vláda	k1gFnSc6	vláda
otevřeným	otevřený	k2eAgInSc7d1	otevřený
dopisem	dopis	k1gInSc7	dopis
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
od	od	k7c2	od
opuštění	opuštění	k1gNnSc2	opuštění
myšlenky	myšlenka	k1gFnSc2	myšlenka
vytvořit	vytvořit	k5eAaPmF	vytvořit
vládu	vláda	k1gFnSc4	vláda
koalice	koalice	k1gFnSc2	koalice
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
a	a	k8xC	a
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
IVVM	IVVM	kA	IVVM
z	z	k7c2	z
prosince	prosinec	k1gInSc2	prosinec
1999	[number]	k4	1999
by	by	kYmCp3nS	by
KSČM	KSČM	kA	KSČM
dostala	dostat	k5eAaPmAgFnS	dostat
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
hlasů	hlas	k1gInPc2	hlas
jako	jako	k8xC	jako
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
nejsilnější	silný	k2eAgNnSc4d3	nejsilnější
stran	strana	k1gFnPc2	strana
v	v	k7c6	v
poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Totožný	totožný	k2eAgInSc1d1	totožný
výsledek	výsledek	k1gInSc1	výsledek
KSČM	KSČM	kA	KSČM
a	a	k8xC	a
ODS	ODS	kA	ODS
předvídal	předvídat	k5eAaImAgInS	předvídat
i	i	k9	i
průzkum	průzkum	k1gInSc1	průzkum
STEM	sto	k4xCgNnSc7	sto
též	též	k9	též
z	z	k7c2	z
prosince	prosinec	k1gInSc2	prosinec
<g/>
.21	.21	k4	.21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c6	na
demonstraci	demonstrace	k1gFnSc6	demonstrace
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nedostali	dostat	k5eNaPmAgMnP	dostat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
mzdu	mzda	k1gFnSc4	mzda
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nepřímo	přímo	k6eNd1	přímo
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
odborového	odborový	k2eAgMnSc4d1	odborový
předáka	předák	k1gMnSc4	předák
Richarda	Richard	k1gMnSc4	Richard
Falbra	Falbr	k1gMnSc4	Falbr
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
"	"	kIx"	"
<g/>
kolaboraci	kolaborace	k1gFnSc4	kolaborace
<g/>
"	"	kIx"	"
s	s	k7c7	s
vládami	vláda	k1gFnPc7	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stagnace	stagnace	k1gFnSc2	stagnace
KSČM	KSČM	kA	KSČM
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
I	i	k9	i
když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
mnohdy	mnohdy	k6eAd1	mnohdy
preference	preference	k1gFnPc4	preference
KSČM	KSČM	kA	KSČM
vysoké	vysoká	k1gFnSc2	vysoká
<g/>
,	,	kIx,	,
KSČM	KSČM	kA	KSČM
postupně	postupně	k6eAd1	postupně
začala	začít	k5eAaPmAgFnS	začít
klesat	klesat	k5eAaImF	klesat
popularita	popularita	k1gFnSc1	popularita
a	a	k8xC	a
nebyla	být	k5eNaImAgFnS	být
již	již	k6eAd1	již
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
průzkumech	průzkum	k1gInPc6	průzkum
uváděna	uvádět	k5eAaImNgFnS	uvádět
za	za	k7c4	za
nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
politickou	politický	k2eAgFnSc4d1	politická
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
se	se	k3xPyFc4	se
ODS	ODS	kA	ODS
vzdala	vzdát	k5eAaPmAgFnS	vzdát
snahy	snaha	k1gFnSc2	snaha
vytvořit	vytvořit	k5eAaPmF	vytvořit
tzv.	tzv.	kA	tzv.
superkoalici	superkoalice	k1gFnSc4	superkoalice
nekomunistických	komunistický	k2eNgFnPc2d1	nekomunistická
stran	strana	k1gFnPc2	strana
v	v	k7c6	v
poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
Sofres-Factum	Sofres-Factum	k1gNnSc1	Sofres-Factum
a	a	k8xC	a
STEM	sto	k4xCgNnSc7	sto
z	z	k7c2	z
ledna	leden	k1gInSc2	leden
2000	[number]	k4	2000
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
KSČM	KSČM	kA	KSČM
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2000	[number]	k4	2000
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
MF	MF	kA	MF
DNES	dnes	k6eAd1	dnes
Vladimír	Vladimír	k1gMnSc1	Vladimír
Špidla	Špidla	k1gMnSc1	Špidla
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nespolupracování	nespolupracování	k1gNnSc4	nespolupracování
s	s	k7c7	s
KSČM	KSČM	kA	KSČM
správná	správný	k2eAgFnSc1d1	správná
věc	věc	k1gFnSc1	věc
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
komunisté	komunista	k1gMnPc1	komunista
proti	proti	k7c3	proti
zákonu	zákon	k1gInSc3	zákon
o	o	k7c6	o
státním	státní	k2eAgInSc6d1	státní
rozpočtu	rozpočet	k1gInSc6	rozpočet
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
předem	předem	k6eAd1	předem
silně	silně	k6eAd1	silně
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
února	únor	k1gInSc2	únor
jeden	jeden	k4xCgMnSc1	jeden
zástupce	zástupce	k1gMnSc1	zástupce
KSČM	KSČM	kA	KSČM
podpořil	podpořit	k5eAaPmAgMnS	podpořit
senátní	senátní	k2eAgInSc4d1	senátní
návrh	návrh	k1gInSc4	návrh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
posílil	posílit	k5eAaPmAgInS	posílit
některé	některý	k3yIgFnPc4	některý
pravomoci	pravomoc	k1gFnPc4	pravomoc
Senátu	senát	k1gInSc2	senát
a	a	k8xC	a
zrovnoprávnil	zrovnoprávnit	k5eAaPmAgMnS	zrovnoprávnit
obě	dva	k4xCgFnPc4	dva
parlamentní	parlamentní	k2eAgFnPc4d1	parlamentní
komory	komora	k1gFnPc4	komora
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
IVVM	IVVM	kA	IVVM
by	by	kYmCp3nS	by
KSČM	KSČM	kA	KSČM
získala	získat	k5eAaPmAgFnS	získat
20,5	[number]	k4	20,5
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
získala	získat	k5eAaPmAgFnS	získat
neobvyklou	obvyklý	k2eNgFnSc4d1	neobvyklá
podporu	podpora	k1gFnSc4	podpora
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
únorového	únorový	k2eAgInSc2d1	únorový
průzkumu	průzkum	k1gInSc2	průzkum
voliči	volič	k1gMnPc1	volič
KSČM	KSČM	kA	KSČM
nedůvěřují	důvěřovat	k5eNaImIp3nP	důvěřovat
armádě	armáda	k1gFnSc3	armáda
a	a	k8xC	a
chtějí	chtít	k5eAaImIp3nP	chtít
zavést	zavést	k5eAaPmF	zavést
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
KSČM	KSČM	kA	KSČM
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
po	po	k7c6	po
ODS	ODS	kA	ODS
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
byla	být	k5eAaImAgFnS	být
KSČM	KSČM	kA	KSČM
ohledně	ohledně	k7c2	ohledně
zákazu	zákaz	k1gInSc2	zákaz
dodávek	dodávka	k1gFnPc2	dodávka
pro	pro	k7c4	pro
Írán	Írán	k1gInSc4	Írán
skeptická	skeptický	k2eAgFnSc1d1	skeptická
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
se	se	k3xPyFc4	se
před	před	k7c4	před
pražské	pražský	k2eAgNnSc4d1	Pražské
sídlo	sídlo	k1gNnSc4	sídlo
KSČM	KSČM	kA	KSČM
sešli	sejít	k5eAaPmAgMnP	sejít
stoupenci	stoupenec	k1gMnPc1	stoupenec
krajně	krajně	k6eAd1	krajně
pravicové	pravicový	k2eAgFnSc2d1	pravicová
Národní	národní	k2eAgFnSc2d1	národní
aliance	aliance	k1gFnSc2	aliance
<g/>
,	,	kIx,	,
policie	policie	k1gFnSc1	policie
však	však	k9	však
nařídila	nařídit	k5eAaPmAgFnS	nařídit
jejich	jejich	k3xOp3gFnSc4	jejich
demonstraci	demonstrace	k1gFnSc4	demonstrace
proti	proti	k7c3	proti
KSČM	KSČM	kA	KSČM
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
rozpustit	rozpustit	k5eAaPmF	rozpustit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
i	i	k9	i
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
aliance	aliance	k1gFnSc1	aliance
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
,	,	kIx,	,
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
chvíle	chvíle	k1gFnSc2	chvíle
však	však	k9	však
působila	působit	k5eAaImAgFnS	působit
antikomunisticky	antikomunisticky	k6eAd1	antikomunisticky
<g/>
.5	.5	k4	.5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
obdržela	obdržet	k5eAaPmAgFnS	obdržet
KSČM	KSČM	kA	KSČM
od	od	k7c2	od
postarších	postarší	k2eAgMnPc2d1	postarší
manželů	manžel	k1gMnPc2	manžel
z	z	k7c2	z
Otrokovic	Otrokovice	k1gFnPc2	Otrokovice
osm	osm	k4xCc4	osm
set	sto	k4xCgNnPc2	sto
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zdědili	zdědit	k5eAaPmAgMnP	zdědit
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Filip	Filip	k1gMnSc1	Filip
skončil	skončit	k5eAaPmAgMnS	skončit
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
hlasů	hlas	k1gInPc2	hlas
na	na	k7c6	na
funkci	funkce	k1gFnSc6	funkce
místopředsedy	místopředseda	k1gMnSc2	místopředseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dvaadvaceti	dvaadvacet	k4xCc7	dvaadvacet
denní	denní	k2eAgFnSc7d1	denní
stávkou	stávka	k1gFnSc7	stávka
horníků	horník	k1gMnPc2	horník
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
skončila	skončit	k5eAaPmAgFnS	skončit
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
v	v	k7c6	v
dole	dol	k1gInSc6	dol
Kohinoor	Kohinoor	k1gInSc1	Kohinoor
v	v	k7c6	v
Mariánských	mariánský	k2eAgFnPc6d1	Mariánská
Radčicích	Radčice	k1gFnPc6	Radčice
na	na	k7c6	na
Mostecku	Mostecko	k1gNnSc6	Mostecko
souhlasilo	souhlasit	k5eAaImAgNnS	souhlasit
podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
67	[number]	k4	67
%	%	kIx~	%
příznivců	příznivec	k1gMnPc2	příznivec
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
<s>
KSČM	KSČM	kA	KSČM
se	se	k3xPyFc4	se
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
návrhu	návrh	k1gInSc6	návrh
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
dvojicím	dvojice	k1gFnPc3	dvojice
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
umožnil	umožnit	k5eAaPmAgInS	umožnit
uzavřít	uzavřít	k5eAaPmF	uzavřít
před	před	k7c7	před
notářem	notář	k1gMnSc7	notář
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
jim	on	k3xPp3gMnPc3	on
zaručovala	zaručovat	k5eAaImAgFnS	zaručovat
většinu	většina	k1gFnSc4	většina
práv	právo	k1gNnPc2	právo
podobným	podobný	k2eAgNnPc3d1	podobné
právům	právo	k1gNnPc3	právo
manželů	manžel	k1gMnPc2	manžel
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
nároku	nárok	k1gInSc2	nárok
na	na	k7c4	na
osvojení	osvojení	k1gNnSc4	osvojení
či	či	k8xC	či
adopci	adopce	k1gFnSc4	adopce
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
předseda	předseda	k1gMnSc1	předseda
KSČM	KSČM	kA	KSČM
Miroslav	Miroslav	k1gMnSc1	Miroslav
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Letenské	letenský	k2eAgFnSc6d1	Letenská
pláni	pláň	k1gFnSc6	pláň
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
Václava	Václav	k1gMnSc4	Václav
Havla	Havel	k1gMnSc4	Havel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
vstup	vstup	k1gInSc4	vstup
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
do	do	k7c2	do
stávající	stávající	k2eAgFnSc2d1	stávající
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nastal	nastat	k5eAaPmAgInS	nastat
čas	čas	k1gInSc4	čas
pro	pro	k7c4	pro
vítězství	vítězství	k1gNnSc4	vítězství
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
akce	akce	k1gFnPc4	akce
občanské	občanský	k2eAgFnSc2d1	občanská
neposlušnosti	neposlušnost	k1gFnSc2	neposlušnost
<g/>
.	.	kIx.	.
</s>
<s>
Příchozí	příchozí	k1gMnPc1	příchozí
mohli	moct	k5eAaImAgMnP	moct
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
ve	v	k7c6	v
stáncích	stánek	k1gInPc6	stánek
dostat	dostat	k5eAaPmF	dostat
občerstvení	občerstvení	k1gNnSc4	občerstvení
včetně	včetně	k7c2	včetně
moravské	moravský	k2eAgFnSc2d1	Moravská
slivovice	slivovice	k1gFnSc2	slivovice
či	či	k8xC	či
propagační	propagační	k2eAgFnSc2d1	propagační
tiskoviny	tiskovina	k1gFnSc2	tiskovina
včetně	včetně	k7c2	včetně
Černé	Černé	k2eAgFnSc2d1	Černé
knihy	kniha	k1gFnSc2	kniha
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
protiváhou	protiváha	k1gFnSc7	protiváha
Černé	Černé	k2eAgFnSc2d1	Černé
knize	kniha	k1gFnSc3	kniha
komunismu	komunismus	k1gInSc2	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Nechyběl	chybět	k5eNaImAgMnS	chybět
ani	ani	k8xC	ani
zpěvák	zpěvák	k1gMnSc1	zpěvák
řeckého	řecký	k2eAgInSc2d1	řecký
původu	původ	k1gInSc2	původ
Statis	Statis	k1gFnSc1	Statis
Prusalis	Prusalis	k1gFnSc1	Prusalis
<g/>
,	,	kIx,	,
obdivovatel	obdivovatel	k1gMnSc1	obdivovatel
budovatele	budovatel	k1gMnSc2	budovatel
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
Josifa	Josif	k1gMnSc2	Josif
Vissarionoviče	Vissarionovič	k1gMnSc2	Vissarionovič
Stalina	Stalin	k1gMnSc2	Stalin
<g/>
,	,	kIx,	,
a	a	k8xC	a
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Jiřina	Jiřina	k1gFnSc1	Jiřina
Švorcová	švorcový	k2eAgFnSc1d1	Švorcová
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
zveřejněného	zveřejněný	k2eAgInSc2d1	zveřejněný
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
KSČM	KSČM	kA	KSČM
se	s	k7c7	s
21,9	[number]	k4	21,9
%	%	kIx~	%
po	po	k7c6	po
ODS	ODS	kA	ODS
a	a	k8xC	a
Čtyřkoalici	Čtyřkoalice	k1gFnSc4	Čtyřkoalice
nejpopulárnějším	populární	k2eAgInSc7d3	nejpopulárnější
politickým	politický	k2eAgInSc7d1	politický
subjektem	subjekt	k1gInSc7	subjekt
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
hlasovala	hlasovat	k5eAaImAgFnS	hlasovat
KSČM	KSČM	kA	KSČM
proti	proti	k7c3	proti
reformě	reforma	k1gFnSc3	reforma
soudnictví	soudnictví	k1gNnSc2	soudnictví
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
se	se	k3xPyFc4	se
poslanci	poslanec	k1gMnPc1	poslanec
KSČM	KSČM	kA	KSČM
postavili	postavit	k5eAaPmAgMnP	postavit
proti	proti	k7c3	proti
škrtnutí	škrtnutí	k1gNnSc3	škrtnutí
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
jako	jako	k8xC	jako
významného	významný	k2eAgInSc2d1	významný
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
Slovenské	slovenský	k2eAgNnSc1d1	slovenské
národní	národní	k2eAgNnSc1d1	národní
povstání	povstání	k1gNnSc1	povstání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Martina	Martin	k1gMnSc2	Martin
Komárka	Komárek	k1gMnSc2	Komárek
je	být	k5eAaImIp3nS	být
srp	srp	k1gInSc4	srp
a	a	k8xC	a
kladivo	kladivo	k1gNnSc4	kladivo
stejný	stejný	k2eAgInSc1d1	stejný
symbol	symbol	k1gInSc1	symbol
jako	jako	k8xC	jako
hákový	hákový	k2eAgInSc1d1	hákový
kříž	kříž	k1gInSc1	kříž
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Filip	Filip	k1gMnSc1	Filip
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
navýšit	navýšit	k5eAaPmF	navýšit
rychlost	rychlost	k1gFnSc4	rychlost
na	na	k7c6	na
dálnicích	dálnice	k1gFnPc6	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
přijalo	přijmout	k5eAaPmAgNnS	přijmout
novelizovaný	novelizovaný	k2eAgInSc1d1	novelizovaný
zákoník	zákoník	k1gInSc1	zákoník
práce	práce	k1gFnSc2	práce
69	[number]	k4	69
procent	procento	k1gNnPc2	procento
příznivců	příznivec	k1gMnPc2	příznivec
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
měla	mít	k5eAaImAgFnS	mít
KSČM	KSČM	kA	KSČM
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
masivní	masivní	k2eAgFnSc3d1	masivní
členské	členský	k2eAgFnSc3d1	členská
základně	základna	k1gFnSc3	základna
23	[number]	k4	23
%	%	kIx~	%
příjmů	příjem	k1gInPc2	příjem
<g/>
.	.	kIx.	.
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Pehe	Pehe	k1gNnSc4	Pehe
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zakrnělost	zakrnělost	k1gFnSc1	zakrnělost
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
je	být	k5eAaImIp3nS	být
velkou	velký	k2eAgFnSc7d1	velká
překážkou	překážka	k1gFnSc7	překážka
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
rozvoji	rozvoj	k1gInSc3	rozvoj
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Filip	Filip	k1gMnSc1	Filip
varoval	varovat	k5eAaImAgMnS	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
nového	nový	k2eAgInSc2d1	nový
volebního	volební	k2eAgInSc2d1	volební
zákona	zákon	k1gInSc2	zákon
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
faktické	faktický	k2eAgFnSc3d1	faktická
diktatuře	diktatura	k1gFnSc3	diktatura
dvou	dva	k4xCgFnPc2	dva
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
narážka	narážka	k1gFnSc1	narážka
na	na	k7c6	na
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
květnového	květnový	k2eAgInSc2d1	květnový
průzkumu	průzkum	k1gInSc2	průzkum
je	být	k5eAaImIp3nS	být
50	[number]	k4	50
%	%	kIx~	%
příznivců	příznivec	k1gMnPc2	příznivec
KSČM	KSČM	kA	KSČM
rozhodně	rozhodně	k6eAd1	rozhodně
připraveno	připraven	k2eAgNnSc1d1	připraveno
jít	jít	k5eAaImF	jít
ke	k	k7c3	k
krajským	krajský	k2eAgFnPc3d1	krajská
volbám	volba	k1gFnPc3	volba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
u	u	k7c2	u
ČSSD	ČSSD	kA	ČSSD
nebo	nebo	k8xC	nebo
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
krajích	kraj	k1gInPc6	kraj
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
drtivé	drtivý	k2eAgNnSc1d1	drtivé
vítězství	vítězství	k1gNnSc1	vítězství
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
byla	být	k5eAaImAgFnS	být
KSČM	KSČM	kA	KSČM
připravena	připravit	k5eAaPmNgFnS	připravit
hlasovat	hlasovat	k5eAaImF	hlasovat
proti	proti	k7c3	proti
novému	nový	k2eAgInSc3d1	nový
volebnímu	volební	k2eAgInSc3d1	volební
zákonu	zákon	k1gInSc3	zákon
<g/>
,	,	kIx,	,
zvýhodňující	zvýhodňující	k2eAgInPc1d1	zvýhodňující
populárnější	populární	k2eAgInPc1d2	populárnější
politické	politický	k2eAgInPc1d1	politický
subjekty	subjekt	k1gInPc1	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
belgický	belgický	k2eAgInSc1d1	belgický
list	list	k1gInSc1	list
La	la	k1gNnSc2	la
Libre	Libr	k1gInSc5	Libr
Belgique	Belgique	k1gNnSc2	Belgique
naznačil	naznačit	k5eAaPmAgMnS	naznačit
možné	možný	k2eAgNnSc1d1	možné
spojení	spojení	k1gNnSc1	spojení
ODS	ODS	kA	ODS
s	s	k7c7	s
KSČM	KSČM	kA	KSČM
po	po	k7c6	po
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
IVVM	IVVM	kA	IVVM
je	být	k5eAaImIp3nS	být
předseda	předseda	k1gMnSc1	předseda
KSČM	KSČM	kA	KSČM
Miroslav	Miroslav	k1gMnSc1	Miroslav
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
nedůvěryhodný	důvěryhodný	k2eNgInSc4d1	nedůvěryhodný
pro	pro	k7c4	pro
61	[number]	k4	61
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
STEM	sto	k4xCgNnSc7	sto
by	by	kYmCp3nS	by
KSČM	KSČM	kA	KSČM
získala	získat	k5eAaPmAgFnS	získat
18,5	[number]	k4	18,5
%	%	kIx~	%
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tak	tak	k9	tak
po	po	k7c6	po
ODS	ODS	kA	ODS
druhou	druhý	k4xOgFnSc7	druhý
nejpopulárnější	populární	k2eAgFnSc7d3	nejpopulárnější
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
30	[number]	k4	30
%	%	kIx~	%
společnosti	společnost	k1gFnPc1	společnost
hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
současný	současný	k2eAgInSc4d1	současný
režim	režim	k1gInSc4	režim
za	za	k7c4	za
horší	zlý	k2eAgInSc4d2	horší
a	a	k8xC	a
20	[number]	k4	20
%	%	kIx~	%
za	za	k7c4	za
stejný	stejný	k2eAgInSc4d1	stejný
<g/>
.	.	kIx.	.
64	[number]	k4	64
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
průzkumu	průzkum	k1gInSc6	průzkum
uvedlo	uvést	k5eAaPmAgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
předlistopadovém	předlistopadový	k2eAgInSc6d1	předlistopadový
režimu	režim	k1gInSc6	režim
lepší	dobrý	k2eAgNnSc4d2	lepší
sociální	sociální	k2eAgNnSc4d1	sociální
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc4	možnost
dovolat	dovolat	k5eAaPmF	dovolat
se	se	k3xPyFc4	se
práva	právo	k1gNnSc2	právo
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
60	[number]	k4	60
procent	procento	k1gNnPc2	procento
lidí	člověk	k1gMnPc2	člověk
pořád	pořád	k6eAd1	pořád
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
49	[number]	k4	49
procent	procento	k1gNnPc2	procento
občanů	občan	k1gMnPc2	občan
se	se	k3xPyFc4	se
nezměnila	změnit	k5eNaPmAgFnS	změnit
ani	ani	k9	ani
možnost	možnost	k1gFnSc1	možnost
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
politické	politický	k2eAgNnSc4d1	politické
dění	dění	k1gNnSc4	dění
<g/>
.	.	kIx.	.
</s>
<s>
Nejméně	málo	k6eAd3	málo
spokojeni	spokojen	k2eAgMnPc1d1	spokojen
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
republiky	republika	k1gFnSc2	republika
jsou	být	k5eAaImIp3nP	být
voliči	volič	k1gMnPc1	volič
a	a	k8xC	a
sympatizanti	sympatizant	k1gMnPc1	sympatizant
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
vedení	vedení	k1gNnSc1	vedení
ODS	ODS	kA	ODS
označilo	označit	k5eAaPmAgNnS	označit
prodej	prodej	k1gInSc4	prodej
Investiční	investiční	k2eAgFnSc1d1	investiční
a	a	k8xC	a
poštovní	poštovní	k2eAgFnSc1d1	poštovní
banky	banka	k1gFnPc1	banka
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
ČSOB	ČSOB	kA	ČSOB
za	za	k7c4	za
bankovní	bankovní	k2eAgInSc4d1	bankovní
zločin	zločin	k1gInSc4	zločin
za	za	k7c2	za
bílého	bílý	k2eAgInSc2d1	bílý
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
poslankyně	poslankyně	k1gFnSc1	poslankyně
KSČM	KSČM	kA	KSČM
Zuzka	Zuzka	k1gFnSc1	Zuzka
Rujbrová	Rujbrová	k1gFnSc1	Rujbrová
se	se	k3xPyFc4	se
lapidárně	lapidárně	k6eAd1	lapidárně
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zloděj	zloděj	k1gMnSc1	zloděj
křičí	křičet	k5eAaImIp3nS	křičet
<g/>
:	:	kIx,	:
chyťte	chytit	k5eAaPmRp2nP	chytit
zloděje	zloděj	k1gMnPc4	zloděj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
Miroslav	Miroslav	k1gMnSc1	Miroslav
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
marně	marně	k6eAd1	marně
protestoval	protestovat	k5eAaBmAgInS	protestovat
proti	proti	k7c3	proti
vypuštění	vypuštění	k1gNnSc3	vypuštění
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
jako	jako	k8xS	jako
významného	významný	k2eAgInSc2d1	významný
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
odkazující	odkazující	k2eAgNnSc1d1	odkazující
na	na	k7c4	na
Slovenské	slovenský	k2eAgNnSc4d1	slovenské
národní	národní	k2eAgNnSc4d1	národní
povstání	povstání	k1gNnSc4	povstání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
by	by	kYmCp3nS	by
KSČM	KSČM	kA	KSČM
získala	získat	k5eAaPmAgFnS	získat
17	[number]	k4	17
%	%	kIx~	%
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
by	by	kYmCp3nS	by
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
Sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
stíhání	stíhání	k1gNnSc2	stíhání
proti	proti	k7c3	proti
osobě	osoba	k1gFnSc3	osoba
propagující	propagující	k2eAgInSc4d1	propagující
komunismus	komunismus	k1gInSc4	komunismus
<g/>
.	.	kIx.	.
<g/>
Vyšetřovatelé	vyšetřovatel	k1gMnPc1	vyšetřovatel
v	v	k7c6	v
Šumperku	Šumperk	k1gInSc6	Šumperk
obvinili	obvinit	k5eAaPmAgMnP	obvinit
z	z	k7c2	z
propagace	propagace	k1gFnSc2	propagace
hnutí	hnutí	k1gNnSc2	hnutí
směřujícího	směřující	k2eAgInSc2d1	směřující
k	k	k7c3	k
potlačení	potlačení	k1gNnSc3	potlačení
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
občanů	občan	k1gMnPc2	občan
Davida	David	k1gMnSc2	David
Pěchu	pěch	k1gInSc2	pěch
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
samizdatovém	samizdatový	k2eAgInSc6d1	samizdatový
časopise	časopis	k1gInSc6	časopis
Pochodeň	pochodeň	k1gFnSc4	pochodeň
a	a	k8xC	a
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
starých	starý	k2eAgInPc2d1	starý
pořádků	pořádek	k1gInPc2	pořádek
<g/>
,	,	kIx,	,
k	k	k7c3	k
diktatuře	diktatura	k1gFnSc3	diktatura
proletariátu	proletariát	k1gInSc2	proletariát
<g/>
,	,	kIx,	,
znárodnění	znárodnění	k1gNnSc2	znárodnění
a	a	k8xC	a
revoluci	revoluce	k1gFnSc4	revoluce
–	–	k?	–
a	a	k8xC	a
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
i	i	k8xC	i
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
ozbrojeného	ozbrojený	k2eAgInSc2d1	ozbrojený
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Komunisté	komunista	k1gMnPc1	komunista
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pěchovy	Pěchův	k2eAgFnPc1d1	Pěchův
pohnutky	pohnutka	k1gFnPc1	pohnutka
chápou	chápat	k5eAaImIp3nP	chápat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
srpnového	srpnový	k2eAgInSc2d1	srpnový
průzkumu	průzkum	k1gInSc2	průzkum
IVVM	IVVM	kA	IVVM
podporují	podporovat	k5eAaImIp3nP	podporovat
armádu	armáda	k1gFnSc4	armáda
nejméně	málo	k6eAd3	málo
voliči	volič	k1gMnPc1	volič
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
<s>
Profesionalizaci	profesionalizace	k1gFnSc4	profesionalizace
armády	armáda	k1gFnSc2	armáda
nejméně	málo	k6eAd3	málo
podporují	podporovat	k5eAaImIp3nP	podporovat
voliči	volič	k1gMnPc7	volič
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
<s>
KSČM	KSČM	kA	KSČM
by	by	kYmCp3nS	by
získala	získat	k5eAaPmAgFnS	získat
podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
Sofres-factum	Sofresactum	k1gNnSc4	Sofres-factum
pouze	pouze	k6eAd1	pouze
14,6	[number]	k4	14,6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Počet	počet	k1gInSc1	počet
členů	člen	k1gMnPc2	člen
===	===	k?	===
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
členů	člen	k1gMnPc2	člen
KSČM	KSČM	kA	KSČM
klesá	klesat	k5eAaImIp3nS	klesat
už	už	k6eAd1	už
od	od	k7c2	od
jejího	její	k3xOp3gInSc2	její
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
měla	mít	k5eAaImAgFnS	mít
KSČM	KSČM	kA	KSČM
zhruba	zhruba	k6eAd1	zhruba
51	[number]	k4	51
000	[number]	k4	000
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
klesá	klesat	k5eAaImIp3nS	klesat
o	o	k7c4	o
zhruba	zhruba	k6eAd1	zhruba
15	[number]	k4	15
členů	člen	k1gInPc2	člen
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Členskou	členský	k2eAgFnSc4d1	členská
základnu	základna	k1gFnSc4	základna
si	se	k3xPyFc3	se
strana	strana	k1gFnSc1	strana
přináší	přinášet	k5eAaImIp3nS	přinášet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
členů	člen	k1gInPc2	člen
byl	být	k5eAaImAgInS	být
74	[number]	k4	74
let	léto	k1gNnPc2	léto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
a	a	k8xC	a
průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
voliče	volič	k1gInSc2	volič
byl	být	k5eAaImAgInS	být
60	[number]	k4	60
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
sněmovny	sněmovna	k1gFnSc2	sněmovna
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2002	[number]	k4	2002
získala	získat	k5eAaPmAgFnS	získat
KSČM	KSČM	kA	KSČM
18,5	[number]	k4	18,5
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
882	[number]	k4	882
653	[number]	k4	653
hlasů	hlas	k1gInPc2	hlas
<g/>
;	;	kIx,	;
41	[number]	k4	41
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
druhá	druhý	k4xOgFnSc1	druhý
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
20,3	[number]	k4	20,3
%	%	kIx~	%
a	a	k8xC	a
6	[number]	k4	6
z	z	k7c2	z
24	[number]	k4	24
poslaneckých	poslanecký	k2eAgNnPc2d1	poslanecké
křesel	křeslo	k1gNnPc2	křeslo
(	(	kIx(	(
<g/>
za	za	k7c7	za
vítěznou	vítězný	k2eAgFnSc7d1	vítězná
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
získala	získat	k5eAaPmAgFnS	získat
11,27	[number]	k4	11,27
%	%	kIx~	%
(	(	kIx(	(
<g/>
589	[number]	k4	589
765	[number]	k4	765
hlasů	hlas	k1gInPc2	hlas
<g/>
;	;	kIx,	;
26	[number]	k4	26
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
uspěla	uspět	k5eAaPmAgFnS	uspět
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
krajských	krajský	k2eAgNnPc2d1	krajské
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
pořadí	pořadí	k1gNnSc6	pořadí
stran	strana	k1gFnPc2	strana
jako	jako	k8xC	jako
druhá	druhý	k4xOgFnSc1	druhý
za	za	k7c4	za
ČSSD	ČSSD	kA	ČSSD
se	s	k7c7	s
20,43	[number]	k4	20,43
%	%	kIx~	%
a	a	k8xC	a
ziskem	zisk	k1gInSc7	zisk
182	[number]	k4	182
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
Ústeckém	ústecký	k2eAgInSc6d1	ústecký
a	a	k8xC	a
Karlovarském	karlovarský	k2eAgInSc6d1	karlovarský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
sebeprezentace	sebeprezentace	k1gFnSc2	sebeprezentace
se	se	k3xPyFc4	se
KSČM	KSČM	kA	KSČM
nejdůsledněji	důsledně	k6eAd3	důsledně
zastává	zastávat	k5eAaImIp3nS	zastávat
zájmů	zájem	k1gInPc2	zájem
sociálně	sociálně	k6eAd1	sociálně
slabých	slabý	k2eAgInPc2d1	slabý
a	a	k8xC	a
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
tradičních	tradiční	k2eAgInPc2d1	tradiční
levicových	levicový	k2eAgInPc2d1	levicový
principů	princip	k1gInPc2	princip
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c4	o
co	co	k9	co
největší	veliký	k2eAgFnSc4d3	veliký
míru	míra	k1gFnSc4	míra
přerozdělování	přerozdělování	k1gNnSc2	přerozdělování
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
však	však	k9	však
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
životní	životní	k2eAgFnSc1d1	životní
úroveň	úroveň	k1gFnSc1	úroveň
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
výrazně	výrazně	k6eAd1	výrazně
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
má	mít	k5eAaImIp3nS	mít
stabilní	stabilní	k2eAgFnSc4d1	stabilní
voličskou	voličský	k2eAgFnSc4d1	voličská
i	i	k8xC	i
členskou	členský	k2eAgFnSc4d1	členská
základnu	základna	k1gFnSc4	základna
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
strana	strana	k1gFnSc1	strana
stále	stále	k6eAd1	stále
nezbavila	zbavit	k5eNaPmAgFnS	zbavit
stigmatizujícího	stigmatizující	k2eAgNnSc2d1	stigmatizující
spojení	spojení	k1gNnSc2	spojení
s	s	k7c7	s
totalitním	totalitní	k2eAgInSc7d1	totalitní
režimem	režim	k1gInSc7	režim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
v	v	k7c4	v
opozici	opozice	k1gFnSc4	opozice
vůči	vůči	k7c3	vůči
sociálně	sociálně	k6eAd1	sociálně
demokratickým	demokratický	k2eAgFnPc3d1	demokratická
vládám	vláda	k1gFnPc3	vláda
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
poslanci	poslanec	k1gMnPc1	poslanec
KSČM	KSČM	kA	KSČM
hlasovali	hlasovat	k5eAaImAgMnP	hlasovat
pro	pro	k7c4	pro
kandidáta	kandidát	k1gMnSc4	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
za	za	k7c2	za
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Prvek	prvek	k1gInSc1	prvek
separace	separace	k1gFnSc2	separace
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
v	v	k7c6	v
letech	let	k1gInPc6	let
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
poněkud	poněkud	k6eAd1	poněkud
oslaben	oslaben	k2eAgInSc1d1	oslaben
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
sbližováním	sbližování	k1gNnSc7	sbližování
opoziční	opoziční	k2eAgFnSc2d1	opoziční
KSČM	KSČM	kA	KSČM
a	a	k8xC	a
vládní	vládní	k2eAgFnSc2d1	vládní
ČSSD	ČSSD	kA	ČSSD
na	na	k7c6	na
parlamentní	parlamentní	k2eAgFnSc6d1	parlamentní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
spolupráce	spolupráce	k1gFnSc1	spolupráce
způsobila	způsobit	k5eAaPmAgFnS	způsobit
rozpory	rozpor	k1gInPc4	rozpor
i	i	k9	i
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
komentátorů	komentátor	k1gMnPc2	komentátor
přispěla	přispět	k5eAaPmAgFnS	přispět
i	i	k9	i
k	k	k7c3	k
volebnímu	volební	k2eAgInSc3d1	volební
propadu	propad	k1gInSc3	propad
ve	v	k7c6	v
sněmovních	sněmovní	k2eAgFnPc6d1	sněmovní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
když	když	k8xS	když
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
voličů	volič	k1gMnPc2	volič
KSČM	KSČM	kA	KSČM
přešla	přejít	k5eAaPmAgFnS	přejít
k	k	k7c3	k
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stranická	stranický	k2eAgFnSc1d1	stranická
struktura	struktura	k1gFnSc1	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
vedení	vedení	k1gNnSc1	vedení
bylo	být	k5eAaImAgNnS	být
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
na	na	k7c4	na
10	[number]	k4	10
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc2	sjezd
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Ústřední	ústřední	k2eAgInSc1d1	ústřední
výbor	výbor	k1gInSc1	výbor
a	a	k8xC	a
sjezdy	sjezd	k1gInPc1	sjezd
bývají	bývat	k5eAaImIp3nP	bývat
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
sešlo	sejít	k5eAaPmAgNnS	sejít
celkem	celkem	k6eAd1	celkem
10	[number]	k4	10
sjezdů	sjezd	k1gInPc2	sjezd
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc1	žádný
nebyl	být	k5eNaImAgInS	být
mimořádný	mimořádný	k2eAgMnSc1d1	mimořádný
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc6	sjezd
KSČS	KSČS	kA	KSČS
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
KSČM	KSČM	kA	KSČM
a	a	k8xC	a
KSS	KSS	kA	KSS
společný	společný	k2eAgMnSc1d1	společný
a	a	k8xC	a
právě	právě	k6eAd1	právě
10	[number]	k4	10
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc2	sjezd
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČS	KSČS	kA	KSČS
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
Společný	společný	k2eAgInSc1d1	společný
s	s	k7c7	s
KSS	KSS	kA	KSS
)	)	kIx)	)
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČM	KSČM	kA	KSČM
-	-	kIx~	-
Olomouc	Olomouc	k1gFnSc1	Olomouc
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČM	KSČM	kA	KSČM
-	-	kIx~	-
Kladno	Kladno	k1gNnSc1	Kladno
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČM	KSČM	kA	KSČM
-	-	kIx~	-
Prostějov	Prostějov	k1gInSc1	Prostějov
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČM	KSČM	kA	KSČM
-	-	kIx~	-
Liberec	Liberec	k1gInSc1	Liberec
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČM	KSČM	kA	KSČM
-	-	kIx~	-
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČM	KSČM	kA	KSČM
-	-	kIx~	-
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČM	KSČM	kA	KSČM
-	-	kIx~	-
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČM	KSČM	kA	KSČM
-	-	kIx~	-
Liberec	Liberec	k1gInSc1	Liberec
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČM	KSČM	kA	KSČM
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSČM	KSČM	kA	KSČM
-	-	kIx~	-
Nymburk	Nymburk	k1gInSc1	Nymburk
</s>
</p>
<p>
<s>
===	===	k?	===
Komunistický	komunistický	k2eAgInSc1d1	komunistický
svaz	svaz	k1gInSc1	svaz
mládeže	mládež	k1gFnSc2	mládež
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
orgánem	orgán	k1gInSc7	orgán
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
mládeží	mládež	k1gFnSc7	mládež
KSČM	KSČM	kA	KSČM
je	být	k5eAaImIp3nS	být
Komise	komise	k1gFnSc1	komise
mládeže	mládež	k1gFnSc2	mládež
ÚV	ÚV	kA	ÚV
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnSc1	jejíž
vedení	vedení	k1gNnSc4	vedení
převzal	převzít	k5eAaPmAgMnS	převzít
po	po	k7c6	po
poslankyni	poslankyně	k1gFnSc6	poslankyně
za	za	k7c4	za
KSČM	KSČM	kA	KSČM
Ing.	ing.	kA	ing.
Kateřině	Kateřina	k1gFnSc3	Kateřina
Konečné	Konečné	k2eAgFnPc2d1	Konečné
Zdeněk	Zdeňka	k1gFnPc2	Zdeňka
Milata	Milata	k1gFnSc1	Milata
<g/>
.	.	kIx.	.
</s>
<s>
Neoficiální	neoficiální	k2eAgFnSc7d1	neoficiální
mládežnickou	mládežnický	k2eAgFnSc7d1	mládežnická
organizací	organizace	k1gFnSc7	organizace
byl	být	k5eAaImAgInS	být
také	také	k9	také
Komunistický	komunistický	k2eAgInSc1d1	komunistický
svaz	svaz	k1gInSc1	svaz
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vedl	vést	k5eAaImAgMnS	vést
Milan	Milan	k1gMnSc1	Milan
Krajča	Krajča	k1gMnSc1	Krajča
a	a	k8xC	a
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Štefek	Štefek	k1gMnSc1	Štefek
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
však	však	k9	však
byla	být	k5eAaImAgFnS	být
kvůli	kvůli	k7c3	kvůli
údajnému	údajný	k2eAgInSc3d1	údajný
rozporu	rozpor	k1gInSc3	rozpor
jejích	její	k3xOp3gFnPc6	její
stanovách	stanova	k1gFnPc6	stanova
s	s	k7c7	s
Ústavou	ústava	k1gFnSc7	ústava
ČR	ČR	kA	ČR
pravomocně	pravomocně	k6eAd1	pravomocně
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
rozpuštěna	rozpustit	k5eAaPmNgFnS	rozpustit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několikaletém	několikaletý	k2eAgInSc6d1	několikaletý
soudním	soudní	k2eAgInSc6d1	soudní
procesu	proces	k1gInSc6	proces
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
Komunistickému	komunistický	k2eAgInSc3d1	komunistický
svazu	svaz	k1gInSc3	svaz
mládeže	mládež	k1gFnSc2	mládež
povoleno	povolit	k5eAaPmNgNnS	povolit
obnovit	obnovit	k5eAaPmF	obnovit
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Členství	členství	k1gNnSc1	členství
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
politických	politický	k2eAgFnPc6d1	politická
strukturách	struktura	k1gFnPc6	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
KSČM	KSČM	kA	KSČM
je	být	k5eAaImIp3nS	být
členkou	členka	k1gFnSc7	členka
frakce	frakce	k1gFnSc2	frakce
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
Evropská	evropský	k2eAgFnSc1d1	Evropská
sjednocené	sjednocený	k2eAgFnPc4d1	sjednocená
levice	levice	k1gFnPc4	levice
a	a	k8xC	a
Severské	severský	k2eAgFnPc4d1	severská
zelené	zelený	k2eAgFnPc4d1	zelená
levice	levice	k1gFnPc4	levice
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
není	být	k5eNaImIp3nS	být
členem	člen	k1gInSc7	člen
Strany	strana	k1gFnSc2	strana
evropské	evropský	k2eAgFnSc2d1	Evropská
levice	levice	k1gFnSc2	levice
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
KSČM	KSČM	kA	KSČM
stala	stát	k5eAaPmAgFnS	stát
společně	společně	k6eAd1	společně
s	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
desítkami	desítka	k1gFnPc7	desítka
dalších	další	k2eAgFnPc2d1	další
evropských	evropský	k2eAgFnPc2d1	Evropská
komunistických	komunistický	k2eAgFnPc2d1	komunistická
stran	strana	k1gFnPc2	strana
zakladatelem	zakladatel	k1gMnSc7	zakladatel
Iniciativy	iniciativa	k1gFnSc2	iniciativa
komunistických	komunistický	k2eAgFnPc2d1	komunistická
a	a	k8xC	a
dělnických	dělnický	k2eAgFnPc2d1	Dělnická
stran	strana	k1gFnPc2	strana
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
a	a	k8xC	a
vyhodnocování	vyhodnocování	k1gNnSc4	vyhodnocování
evropských	evropský	k2eAgFnPc2d1	Evropská
otázek	otázka	k1gFnPc2	otázka
a	a	k8xC	a
pro	pro	k7c4	pro
koordinaci	koordinace	k1gFnSc4	koordinace
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
zakládajícím	zakládající	k2eAgNnSc6d1	zakládající
prohlášení	prohlášení	k1gNnSc6	prohlášení
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Domníváme	domnívat	k5eAaImIp1nP	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
je	být	k5eAaImIp3nS	být
nástrojem	nástroj	k1gInSc7	nástroj
kapitálu	kapitál	k1gInSc2	kapitál
<g/>
.	.	kIx.	.
</s>
<s>
Prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
opatření	opatření	k1gNnPc4	opatření
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
monopolů	monopol	k1gInPc2	monopol
a	a	k8xC	a
koncentrace	koncentrace	k1gFnSc2	koncentrace
a	a	k8xC	a
centralizace	centralizace	k1gFnSc2	centralizace
kapitálu	kapitál	k1gInSc2	kapitál
<g/>
.	.	kIx.	.
</s>
<s>
Posiluje	posilovat	k5eAaImIp3nS	posilovat
se	se	k3xPyFc4	se
její	její	k3xOp3gInSc1	její
charakter	charakter	k1gInSc1	charakter
coby	coby	k?	coby
imperialistického	imperialistický	k2eAgInSc2d1	imperialistický
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
<g/>
,	,	kIx,	,
politického	politický	k2eAgInSc2d1	politický
a	a	k8xC	a
vojenského	vojenský	k2eAgInSc2d1	vojenský
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
zájmy	zájem	k1gInPc7	zájem
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
lidových	lidový	k2eAgFnPc2d1	lidová
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Analyzujeme	analyzovat	k5eAaImIp1nP	analyzovat
Evropskou	evropský	k2eAgFnSc4d1	Evropská
unii	unie	k1gFnSc4	unie
jako	jako	k8xS	jako
evropské	evropský	k2eAgNnSc4d1	Evropské
imperialistické	imperialistický	k2eAgNnSc4d1	imperialistické
centrum	centrum	k1gNnSc4	centrum
<g/>
,	,	kIx,	,
spojence	spojenka	k1gFnSc6	spojenka
USA	USA	kA	USA
a	a	k8xC	a
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
podporující	podporující	k2eAgInPc1d1	podporující
jejich	jejich	k3xOp3gInPc1	jejich
agresivní	agresivní	k2eAgInPc1d1	agresivní
plány	plán	k1gInPc1	plán
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
předsedy	předseda	k1gMnSc2	předseda
zahraničně-politické	zahraničněolitický	k2eAgFnSc2d1	zahraničně-politická
komise	komise	k1gFnSc2	komise
Ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
KSČM	KSČM	kA	KSČM
Milana	Milana	k1gFnSc1	Milana
Krajči	Krajč	k1gFnSc3	Krajč
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
této	tento	k3xDgFnSc2	tento
iniciativy	iniciativa	k1gFnSc2	iniciativa
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
"	"	kIx"	"
<g/>
důležitý	důležitý	k2eAgInSc1d1	důležitý
krok	krok	k1gInSc1	krok
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
sjednocování	sjednocování	k1gNnSc3	sjednocování
komunistického	komunistický	k2eAgNnSc2d1	komunistické
hnutí	hnutí	k1gNnSc2	hnutí
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
zintenzivnění	zintenzivnění	k1gNnSc4	zintenzivnění
spolupráce	spolupráce	k1gFnSc2	spolupráce
evropských	evropský	k2eAgFnPc2d1	Evropská
komunistických	komunistický	k2eAgFnPc2d1	komunistická
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Ideologie	ideologie	k1gFnSc2	ideologie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Neokomunisté	Neokomunista	k1gMnPc1	Neokomunista
a	a	k8xC	a
eurokomunisté	eurokomunista	k1gMnPc1	eurokomunista
===	===	k?	===
</s>
</p>
<p>
<s>
Takzvané	takzvaný	k2eAgNnSc4d1	takzvané
"	"	kIx"	"
<g/>
liberální	liberální	k2eAgNnSc4d1	liberální
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
"	"	kIx"	"
KSČM	KSČM	kA	KSČM
(	(	kIx(	(
<g/>
názorově	názorově	k6eAd1	názorově
bližší	blízký	k2eAgMnSc1d2	bližší
k	k	k7c3	k
sociální	sociální	k2eAgFnSc3d1	sociální
demokracii	demokracie	k1gFnSc3	demokracie
<g/>
)	)	kIx)	)
reprezentoval	reprezentovat	k5eAaImAgMnS	reprezentovat
Miloslav	Miloslav	k1gMnSc1	Miloslav
Ransdorf	Ransdorf	k1gMnSc1	Ransdorf
či	či	k8xC	či
Jiří	Jiří	k1gMnSc1	Jiří
Dolejš	Dolejš	k1gMnSc1	Dolejš
<g/>
.	.	kIx.	.
</s>
<s>
Ideologií	ideologie	k1gFnSc7	ideologie
této	tento	k3xDgFnSc2	tento
frakce	frakce	k1gFnSc2	frakce
je	být	k5eAaImIp3nS	být
neokomunismus	neokomunismus	k1gInSc1	neokomunismus
<g/>
,	,	kIx,	,
idea	idea	k1gFnSc1	idea
navrácení	navrácení	k1gNnSc2	navrácení
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
komunismu	komunismus	k1gInSc3	komunismus
<g/>
,	,	kIx,	,
doplněného	doplněný	k2eAgInSc2d1	doplněný
ideami	idea	k1gFnPc7	idea
eurokomunismu	eurokomunismus	k1gInSc2	eurokomunismus
<g/>
.	.	kIx.	.
</s>
<s>
Neokomunisté	Neokomunista	k1gMnPc1	Neokomunista
se	se	k3xPyFc4	se
zasazovali	zasazovat	k5eAaImAgMnP	zasazovat
o	o	k7c4	o
vstup	vstup	k1gInSc4	vstup
ČR	ČR	kA	ČR
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
následně	následně	k6eAd1	následně
o	o	k7c6	o
přijetí	přijetí	k1gNnSc6	přijetí
KSČM	KSČM	kA	KSČM
do	do	k7c2	do
Strany	strana	k1gFnSc2	strana
evropské	evropský	k2eAgFnSc2d1	Evropská
levice	levice	k1gFnSc2	levice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naopak	naopak	k6eAd1	naopak
stalinistické	stalinistický	k2eAgNnSc1d1	stalinistické
křídlo	křídlo	k1gNnSc1	křídlo
(	(	kIx(	(
<g/>
podporující	podporující	k2eAgInSc1d1	podporující
Gottwaldův	Gottwaldův	k2eAgInSc1d1	Gottwaldův
režim	režim	k1gInSc1	režim
<g/>
)	)	kIx)	)
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
Marta	Marta	k1gFnSc1	Marta
Semelová	Semelová	k1gFnSc1	Semelová
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
stalinistům	stalinista	k1gMnPc3	stalinista
bývají	bývat	k5eAaImIp3nP	bývat
médii	médium	k1gNnPc7	médium
řazeni	řazen	k2eAgMnPc1d1	řazen
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
kritiku	kritika	k1gFnSc4	kritika
minulého	minulý	k2eAgInSc2d1	minulý
režimu	režim	k1gInSc2	režim
Miroslav	Miroslav	k1gMnSc1	Miroslav
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
a	a	k8xC	a
Stanislav	Stanislav	k1gMnSc1	Stanislav
Grospič	Grospič	k1gMnSc1	Grospič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Karla	Karel	k1gMnSc2	Karel
Klimši	Klimše	k1gFnSc4	Klimše
KSČM	KSČM	kA	KSČM
v	v	k7c6	v
Straně	strana	k1gFnSc6	strana
evropské	evropský	k2eAgFnSc2d1	Evropská
levice	levice	k1gFnSc2	levice
hledá	hledat	k5eAaImIp3nS	hledat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Od	od	k7c2	od
zrodu	zrod	k1gInSc2	zrod
nám	my	k3xPp1nPc3	my
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sjednocovala	sjednocovat	k5eAaImAgFnS	sjednocovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
dělila	dělit	k5eAaImAgFnS	dělit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nebyla	být	k5eNaImAgFnS	být
sevřená	sevřený	k2eAgFnSc1d1	sevřená
a	a	k8xC	a
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
do	do	k7c2	do
hranic	hranice	k1gFnPc2	hranice
EU	EU	kA	EU
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
stranou	stranou	k6eAd1	stranou
evropské	evropský	k2eAgFnPc4d1	Evropská
levice	levice	k1gFnPc4	levice
od	od	k7c2	od
Atlantiku	Atlantik	k1gInSc2	Atlantik
po	po	k7c4	po
Ural	Ural	k1gInSc4	Ural
bez	bez	k7c2	bez
jakýchkoliv	jakýkoliv	k3yIgInPc2	jakýkoliv
selektivních	selektivní	k2eAgInPc2d1	selektivní
přístupů	přístup	k1gInPc2	přístup
a	a	k8xC	a
omezení	omezení	k1gNnPc2	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k9	že
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
dokládá	dokládat	k5eAaImIp3nS	dokládat
prý	prý	k9	prý
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
chyběly	chybět	k5eAaImAgFnP	chybět
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
SEL	sít	k5eAaImAgInS	sít
v	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
některé	některý	k3yIgFnSc2	některý
silné	silný	k2eAgFnSc2d1	silná
<g/>
,	,	kIx,	,
vlivné	vlivný	k2eAgFnSc2d1	vlivná
a	a	k8xC	a
akceschopné	akceschopný	k2eAgFnSc2d1	akceschopná
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
Kypru	Kypr	k1gInSc2	Kypr
<g/>
,	,	kIx,	,
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
bývalých	bývalý	k2eAgFnPc2d1	bývalá
sovětských	sovětský	k2eAgFnPc2d1	sovětská
republik	republika	k1gFnPc2	republika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
z	z	k7c2	z
Balkánu	Balkán	k1gInSc2	Balkán
a	a	k8xC	a
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Naší	náš	k3xOp1gFnSc7	náš
snahou	snaha	k1gFnSc7	snaha
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
strany	strana	k1gFnPc1	strana
a	a	k8xC	a
proudy	proud	k1gInPc1	proud
našly	najít	k5eAaPmAgInP	najít
společnou	společný	k2eAgFnSc4d1	společná
řeč	řeč	k1gFnSc4	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Naplnění	naplnění	k1gNnSc1	naplnění
hlavního	hlavní	k2eAgInSc2d1	hlavní
sloganu	slogan	k1gInSc2	slogan
sjezdu	sjezd	k1gInSc2	sjezd
Strany	strana	k1gFnSc2	strana
evropské	evropský	k2eAgFnSc2d1	Evropská
levice	levice	k1gFnSc2	levice
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zní	znět	k5eAaImIp3nS	znět
'	'	kIx"	'
<g/>
Můžeme	moct	k5eAaImIp1nP	moct
Evropu	Evropa	k1gFnSc4	Evropa
změnit	změnit	k5eAaPmF	změnit
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
společný	společný	k2eAgInSc4d1	společný
aktivní	aktivní	k2eAgInSc4d1	aktivní
postup	postup	k1gInSc4	postup
všech	všecek	k3xTgMnPc2	všecek
levicových	levicový	k2eAgMnPc2d1	levicový
<g/>
,	,	kIx,	,
protikapitalistických	protikapitalistický	k2eAgInPc2d1	protikapitalistický
a	a	k8xC	a
sociálních	sociální	k2eAgInPc2d1	sociální
subjektů	subjekt	k1gInPc2	subjekt
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
KSČM	KSČM	kA	KSČM
vnímá	vnímat	k5eAaImIp3nS	vnímat
SEL	sít	k5eAaImAgInS	sít
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
možných	možný	k2eAgInPc2d1	možný
a	a	k8xC	a
nadějných	nadějný	k2eAgInPc2d1	nadějný
projektů	projekt	k1gInPc2	projekt
posilování	posilování	k1gNnSc2	posilování
akční	akční	k2eAgFnSc2d1	akční
jednoty	jednota	k1gFnSc2	jednota
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
užitečné	užitečný	k2eAgNnSc1d1	užitečné
fórum	fórum	k1gNnSc1	fórum
setkávání	setkávání	k1gNnSc2	setkávání
<g/>
,	,	kIx,	,
výměny	výměna	k1gFnSc2	výměna
názorů	názor	k1gInPc2	názor
<g/>
,	,	kIx,	,
koordinace	koordinace	k1gFnSc1	koordinace
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
,	,	kIx,	,
solidarity	solidarita	k1gFnSc2	solidarita
a	a	k8xC	a
jednotného	jednotný	k2eAgInSc2d1	jednotný
postupu	postup	k1gInSc2	postup
proti	proti	k7c3	proti
ofenzivě	ofenziva	k1gFnSc3	ofenziva
neoliberalismu	neoliberalismus	k1gInSc2	neoliberalismus
a	a	k8xC	a
globálního	globální	k2eAgInSc2d1	globální
kapitálu	kapitál	k1gInSc2	kapitál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
sjednocen	sjednocen	k2eAgMnSc1d1	sjednocen
je	být	k5eAaImIp3nS	být
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Volební	volební	k2eAgInSc1d1	volební
program	program	k1gInSc1	program
===	===	k?	===
</s>
</p>
<p>
<s>
Program	program	k1gInSc1	program
KSČM	KSČM	kA	KSČM
"	"	kIx"	"
<g/>
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
marxistické	marxistický	k2eAgFnSc2d1	marxistická
teorie	teorie	k1gFnSc2	teorie
otevřené	otevřený	k2eAgFnSc2d1	otevřená
dialogu	dialog	k1gInSc2	dialog
s	s	k7c7	s
mezinárodním	mezinárodní	k2eAgNnSc7d1	mezinárodní
komunistickým	komunistický	k2eAgNnSc7d1	komunistické
a	a	k8xC	a
levicovým	levicový	k2eAgNnSc7d1	levicové
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
,	,	kIx,	,
novým	nový	k2eAgFnPc3d1	nová
myšlenkám	myšlenka	k1gFnPc3	myšlenka
a	a	k8xC	a
poznatkům	poznatek	k1gInPc3	poznatek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Dlouhodobým	dlouhodobý	k2eAgInSc7d1	dlouhodobý
programem	program	k1gInSc7	program
strany	strana	k1gFnSc2	strana
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Naděje	naděje	k1gFnSc1	naděje
pro	pro	k7c4	pro
ČR	ČR	kA	ČR
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něho	on	k3xPp3gNnSc2	on
"	"	kIx"	"
<g/>
za	za	k7c4	za
základní	základní	k2eAgFnSc4d1	základní
příčinu	příčina	k1gFnSc4	příčina
stupňujících	stupňující	k2eAgInPc2d1	stupňující
se	se	k3xPyFc4	se
problémů	problém	k1gInPc2	problém
světa	svět	k1gInSc2	svět
a	a	k8xC	a
společnosti	společnost	k1gFnSc2	společnost
považuje	považovat	k5eAaImIp3nS	považovat
KSČM	KSČM	kA	KSČM
kapitalismus	kapitalismus	k1gInSc4	kapitalismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
chce	chtít	k5eAaImIp3nS	chtít
vystřídat	vystřídat	k5eAaPmF	vystřídat
"	"	kIx"	"
<g/>
novou	nový	k2eAgFnSc7d1	nová
<g/>
,	,	kIx,	,
pokrokovější	pokrokový	k2eAgFnSc7d2	pokrokovější
společensko-ekonomickou	společenskokonomický	k2eAgFnSc7d1	společensko-ekonomická
formací	formace	k1gFnSc7	formace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
materiálů	materiál	k1gInPc2	materiál
strany	strana	k1gFnSc2	strana
je	být	k5eAaImIp3nS	být
programovým	programový	k2eAgInSc7d1	programový
cílem	cíl	k1gInSc7	cíl
KSČM	KSČM	kA	KSČM
socialismus	socialismus	k1gInSc1	socialismus
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
definuje	definovat	k5eAaBmIp3nS	definovat
sama	sám	k3xTgFnSc1	sám
jako	jako	k9	jako
"	"	kIx"	"
<g/>
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
společnost	společnost	k1gFnSc4	společnost
svobodných	svobodný	k2eAgMnPc2d1	svobodný
<g/>
,	,	kIx,	,
rovnoprávných	rovnoprávný	k2eAgMnPc2d1	rovnoprávný
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
politicky	politicky	k6eAd1	politicky
a	a	k8xC	a
hospodářsky	hospodářsky	k6eAd1	hospodářsky
pluralitní	pluralitní	k2eAgFnSc1d1	pluralitní
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
na	na	k7c6	na
maximální	maximální	k2eAgFnSc6d1	maximální
občanské	občanský	k2eAgFnSc6d1	občanská
samosprávě	samospráva	k1gFnSc6	samospráva
<g/>
,	,	kIx,	,
prosperující	prosperující	k2eAgFnSc4d1	prosperující
a	a	k8xC	a
sociálně	sociálně	k6eAd1	sociálně
spravedlivá	spravedlivý	k2eAgFnSc1d1	spravedlivá
<g/>
,	,	kIx,	,
pečující	pečující	k2eAgFnSc1d1	pečující
o	o	k7c4	o
zachování	zachování	k1gNnSc4	zachování
a	a	k8xC	a
zlepšování	zlepšování	k1gNnSc4	zlepšování
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
zabezpečující	zabezpečující	k2eAgFnSc2d1	zabezpečující
lidem	lid	k1gInSc7	lid
důstojnou	důstojný	k2eAgFnSc4d1	důstojná
životní	životní	k2eAgFnSc4d1	životní
úroveň	úroveň	k1gFnSc4	úroveň
a	a	k8xC	a
prosazující	prosazující	k2eAgFnSc4d1	prosazující
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
mír	mír	k1gInSc4	mír
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
"	"	kIx"	"
<g/>
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
úlohu	úloha	k1gFnSc4	úloha
společenského	společenský	k2eAgNnSc2d1	společenské
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
a	a	k8xC	a
protesty	protest	k1gInPc1	protest
==	==	k?	==
</s>
</p>
<p>
<s>
KSČM	KSČM	kA	KSČM
je	být	k5eAaImIp3nS	být
brána	brán	k2eAgFnSc1d1	brána
jako	jako	k8xC	jako
legální	legální	k2eAgFnSc1d1	legální
strana	strana	k1gFnSc1	strana
působící	působící	k2eAgFnSc1d1	působící
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ústavního	ústavní	k2eAgInSc2d1	ústavní
řádu	řád	k1gInSc2	řád
<g/>
;	;	kIx,	;
ostatní	ostatní	k2eAgFnPc1d1	ostatní
strany	strana	k1gFnPc1	strana
a	a	k8xC	a
političtí	politický	k2eAgMnPc1d1	politický
činitelé	činitel	k1gMnPc1	činitel
ji	on	k3xPp3gFnSc4	on
verbálně	verbálně	k6eAd1	verbálně
odsuzují	odsuzovat	k5eAaImIp3nP	odsuzovat
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
ale	ale	k9	ale
často	často	k6eAd1	často
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
úrovních	úroveň	k1gFnPc6	úroveň
jednají	jednat	k5eAaImIp3nP	jednat
a	a	k8xC	a
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
časté	častý	k2eAgFnSc2d1	častá
kritiky	kritika	k1gFnSc2	kritika
zejména	zejména	k9	zejména
z	z	k7c2	z
pravicových	pravicový	k2eAgFnPc2d1	pravicová
pozic	pozice	k1gFnPc2	pozice
<g/>
,	,	kIx,	,
významným	významný	k2eAgMnSc7d1	významný
kritikem	kritik	k1gMnSc7	kritik
KSČM	KSČM	kA	KSČM
je	být	k5eAaImIp3nS	být
senátor	senátor	k1gMnSc1	senátor
Jaromír	Jaromír	k1gMnSc1	Jaromír
Štětina	štětina	k1gFnSc1	štětina
<g/>
.	.	kIx.	.
</s>
<s>
Kritika	kritika	k1gFnSc1	kritika
strany	strana	k1gFnSc2	strana
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
také	také	k9	také
antikomunistickými	antikomunistický	k2eAgFnPc7d1	antikomunistická
organizacemi	organizace	k1gFnPc7	organizace
bez	bez	k7c2	bez
stranické	stranický	k2eAgFnSc2d1	stranická
orientace	orientace	k1gFnSc2	orientace
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Konfederace	konfederace	k1gFnPc1	konfederace
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
nebo	nebo	k8xC	nebo
Svaz	svaz	k1gInSc1	svaz
PTP	PTP	kA	PTP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předmětem	předmět	k1gInSc7	předmět
kritiky	kritika	k1gFnSc2	kritika
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
polistopadová	polistopadový	k2eAgFnSc1d1	polistopadová
liknavost	liknavost	k1gFnSc1	liknavost
ve	v	k7c6	v
stíhání	stíhání	k1gNnSc6	stíhání
komunistických	komunistický	k2eAgInPc2d1	komunistický
zločinů	zločin	k1gInPc2	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
došlo	dojít	k5eAaPmAgNnS	dojít
jen	jen	k9	jen
k	k	k7c3	k
zabavení	zabavení	k1gNnSc3	zabavení
majetku	majetek	k1gInSc2	majetek
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
KSČM	KSČM	kA	KSČM
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
jednou	jednou	k9	jednou
mnoha	mnoho	k4c2	mnoho
evropských	evropský	k2eAgFnPc2d1	Evropská
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
netransformovaly	transformovat	k5eNaBmAgFnP	transformovat
ve	v	k7c4	v
standardní	standardní	k2eAgFnSc4d1	standardní
levicovou	levicový	k2eAgFnSc4d1	levicová
stranu	strana	k1gFnSc4	strana
a	a	k8xC	a
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
si	se	k3xPyFc3	se
komunismus	komunismus	k1gInSc4	komunismus
v	v	k7c6	v
názvu	název	k1gInSc6	název
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
KSČM	KSČM	kA	KSČM
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
:	:	kIx,	:
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
komunistická	komunistický	k2eAgFnSc1d1	komunistická
Dělnická	dělnický	k2eAgFnSc1d1	Dělnická
strana	strana	k1gFnSc1	strana
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
italská	italský	k2eAgFnSc1d1	italská
Strana	strana	k1gFnSc1	strana
komunistické	komunistický	k2eAgFnSc2d1	komunistická
obnovy	obnova	k1gFnSc2	obnova
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
britská	britský	k2eAgFnSc1d1	britská
komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
Švédská	švédský	k2eAgFnSc1d1	švédská
strana	strana	k1gFnSc1	strana
komunistická	komunistický	k2eAgFnSc1d1	komunistická
a	a	k8xC	a
mnoho	mnoho	k6eAd1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
)	)	kIx)	)
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
nebo	nebo	k8xC	nebo
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c6	na
vládách	vláda	k1gFnPc6	vláda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
<g/>
,	,	kIx,	,
v	v	k7c6	v
Moldávii	Moldávie	k1gFnSc6	Moldávie
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Ponechání	ponechání	k1gNnSc1	ponechání
komunismu	komunismus	k1gInSc2	komunismus
v	v	k7c6	v
názvu	název	k1gInSc6	název
je	být	k5eAaImIp3nS	být
jejími	její	k3xOp3gMnPc7	její
kritiky	kritik	k1gMnPc7	kritik
často	často	k6eAd1	často
označováno	označovat	k5eAaImNgNnS	označovat
za	za	k7c4	za
pars	pars	k1gInSc4	pars
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
jejího	její	k3xOp3gNnSc2	její
nedostatečného	dostatečný	k2eNgNnSc2d1	nedostatečné
zavržení	zavržení	k1gNnSc2	zavržení
totalitní	totalitní	k2eAgFnSc2d1	totalitní
praxe	praxe	k1gFnSc2	praxe
KSČ	KSČ	kA	KSČ
<g/>
;	;	kIx,	;
podobně	podobně	k6eAd1	podobně
zásadní	zásadní	k2eAgInSc1d1	zásadní
symbolický	symbolický	k2eAgInSc1d1	symbolický
význam	význam	k1gInSc1	význam
však	však	k9	však
názvu	název	k1gInSc2	název
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
i	i	k9	i
členská	členský	k2eAgFnSc1d1	členská
základna	základna	k1gFnSc1	základna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
vznikají	vznikat	k5eAaImIp3nP	vznikat
projekty	projekt	k1gInPc4	projekt
(	(	kIx(	(
<g/>
např.	např.	kA	např.
akce	akce	k1gFnSc2	akce
"	"	kIx"	"
<g/>
Trikem	trik	k1gInSc7	trik
proti	proti	k7c3	proti
komunismu	komunismus	k1gInSc3	komunismus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
i	i	k9	i
s	s	k7c7	s
motivy	motiv	k1gInPc7	motiv
"	"	kIx"	"
<g/>
nenech	nechat	k5eNaPmRp2nS	nechat
vládnout	vládnout	k5eAaImF	vládnout
rudé	rudý	k2eAgFnPc4d1	rudá
svině	svině	k1gFnPc4	svině
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
zabij	zabít	k5eAaPmRp2nS	zabít
komunistu	komunista	k1gMnSc4	komunista
<g/>
,	,	kIx,	,
posílíš	posílit	k5eAaPmIp2nS	posílit
mír	mír	k1gInSc1	mír
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgMnPc4	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
upozornit	upozornit	k5eAaPmF	upozornit
na	na	k7c4	na
údajnou	údajný	k2eAgFnSc4d1	údajná
nebezpečnost	nebezpečnost	k1gFnSc4	nebezpečnost
komunistické	komunistický	k2eAgFnSc2d1	komunistická
ideologie	ideologie	k1gFnSc2	ideologie
a	a	k8xC	a
na	na	k7c4	na
krvavou	krvavý	k2eAgFnSc4d1	krvavá
minulost	minulost	k1gFnSc4	minulost
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Konají	konat	k5eAaImIp3nP	konat
se	se	k3xPyFc4	se
i	i	k9	i
antikomunistické	antikomunistický	k2eAgFnPc1d1	antikomunistická
demonstrace	demonstrace	k1gFnPc1	demonstrace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
máje	máj	k1gInSc2	máj
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
proti	proti	k7c3	proti
akcím	akce	k1gFnPc3	akce
KSČM	KSČM	kA	KSČM
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Šinágl	Šinágl	k?	Šinágl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Iniciativa	iniciativa	k1gFnSc1	iniciativa
Zrušme	zrušit	k5eAaPmRp1nP	zrušit
komunisty	komunista	k1gMnPc7	komunista
žádá	žádat	k5eAaImIp3nS	žádat
o	o	k7c4	o
"	"	kIx"	"
<g/>
zákaz	zákaz	k1gInSc4	zákaz
propagace	propagace	k1gFnSc2	propagace
nacismu	nacismus	k1gInSc2	nacismus
<g/>
,	,	kIx,	,
komunismu	komunismus	k1gInSc2	komunismus
a	a	k8xC	a
fašismu	fašismus	k1gInSc2	fašismus
v	v	k7c6	v
názvech	název	k1gInPc6	název
a	a	k8xC	a
programech	program	k1gInPc6	program
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
řadou	řada	k1gFnSc7	řada
známých	známý	k2eAgFnPc2d1	známá
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2005	[number]	k4	2005
a	a	k8xC	a
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
získala	získat	k5eAaPmAgFnS	získat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
000	[number]	k4	000
podpisů	podpis	k1gInPc2	podpis
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
internetu	internet	k1gInSc6	internet
se	se	k3xPyFc4	se
také	také	k9	také
objevil	objevit	k5eAaPmAgInS	objevit
útok	útok	k1gInSc1	útok
na	na	k7c4	na
komunistickou	komunistický	k2eAgFnSc4d1	komunistická
stranu	strana	k1gFnSc4	strana
pomocí	pomocí	k7c2	pomocí
tzv.	tzv.	kA	tzv.
Google	Google	k1gFnSc1	Google
bomby	bomba	k1gFnSc2	bomba
zločinci	zločinec	k1gMnPc1	zločinec
a	a	k8xC	a
vrazi	vrah	k1gMnPc1	vrah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Odstoupení	odstoupení	k1gNnSc1	odstoupení
tiskového	tiskový	k2eAgMnSc2d1	tiskový
mluvčího	mluvčí	k1gMnSc2	mluvčí
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
vlnu	vlna	k1gFnSc4	vlna
kritiky	kritika	k1gFnSc2	kritika
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
mluvčím	mluvčí	k1gMnSc7	mluvčí
KSČM	KSČM	kA	KSČM
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Josef	Josef	k1gMnSc1	Josef
Tomáš	Tomáš	k1gMnSc1	Tomáš
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
vedení	vedení	k1gNnSc1	vedení
KSČM	KSČM	kA	KSČM
vědělo	vědět	k5eAaImAgNnS	vědět
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
odsouzení	odsouzení	k1gNnSc4	odsouzení
k	k	k7c3	k
podmíněnému	podmíněný	k2eAgInSc3d1	podmíněný
trestu	trest	k1gInSc3	trest
za	za	k7c4	za
hanobení	hanobení	k1gNnSc4	hanobení
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
rasy	rasa	k1gFnSc2	rasa
a	a	k8xC	a
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
za	za	k7c4	za
antisemitismus	antisemitismus	k1gInSc4	antisemitismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
počátkem	počátkem	k7c2	počátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vydával	vydávat	k5eAaPmAgMnS	vydávat
a	a	k8xC	a
řídil	řídit	k5eAaImAgMnS	řídit
extrémně	extrémně	k6eAd1	extrémně
pravicový	pravicový	k2eAgMnSc1d1	pravicový
a	a	k8xC	a
antisemitský	antisemitský	k2eAgInSc1d1	antisemitský
Týdeník	týdeník	k1gInSc1	týdeník
Politika	politikum	k1gNnSc2	politikum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
postavil	postavit	k5eAaPmAgMnS	postavit
předseda	předseda	k1gMnSc1	předseda
KSČM	KSČM	kA	KSČM
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Filip	Filip	k1gMnSc1	Filip
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
9	[number]	k4	9
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
krok	krok	k1gInSc4	krok
odůvodnil	odůvodnit	k5eAaPmAgMnS	odůvodnit
negativní	negativní	k2eAgFnSc7d1	negativní
publicitou	publicita	k1gFnSc7	publicita
vůči	vůči	k7c3	vůči
němu	on	k3xPp3gNnSc3	on
a	a	k8xC	a
KSČM	KSČM	kA	KSČM
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
dřívějším	dřívější	k2eAgNnSc7d1	dřívější
odsouzením	odsouzení	k1gNnSc7	odsouzení
za	za	k7c4	za
hanobení	hanobení	k1gNnSc4	hanobení
národa	národ	k1gInSc2	národ
a	a	k8xC	a
rasy	rasa	k1gFnSc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Tomáš	Tomáš	k1gMnSc1	Tomáš
označení	označení	k1gNnSc2	označení
za	za	k7c4	za
antisemitu	antisemita	k1gMnSc4	antisemita
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
současnou	současný	k2eAgFnSc4d1	současná
mediální	mediální	k2eAgFnSc4d1	mediální
kampaň	kampaň	k1gFnSc4	kampaň
proti	proti	k7c3	proti
němu	on	k3xPp3gNnSc3	on
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
permanentně	permanentně	k6eAd1	permanentně
vedené	vedený	k2eAgFnSc2d1	vedená
kampaně	kampaň	k1gFnSc2	kampaň
proti	proti	k7c3	proti
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Úplatkářská	úplatkářský	k2eAgFnSc1d1	úplatkářská
aféra	aféra	k1gFnSc1	aféra
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2009	[number]	k4	2009
byli	být	k5eAaImAgMnP	být
místopředsedové	místopředseda	k1gMnPc1	místopředseda
KSČM	KSČM	kA	KSČM
Jiří	Jiří	k1gMnSc1	Jiří
Dolejš	Dolejš	k1gMnSc1	Dolejš
a	a	k8xC	a
Čeněk	Čeněk	k1gMnSc1	Čeněk
Milota	milota	k1gFnSc1	milota
tajně	tajně	k6eAd1	tajně
nahráni	nahrát	k5eAaBmNgMnP	nahrát
provokatérem	provokatér	k1gMnSc7	provokatér
deníku	deník	k1gInSc2	deník
MF	MF	kA	MF
DNES	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vydával	vydávat	k5eAaImAgInS	vydávat
za	za	k7c2	za
majitele	majitel	k1gMnSc2	majitel
sítě	síť	k1gFnSc2	síť
heren	herna	k1gFnPc2	herna
Jackpot	jackpot	k1gInSc1	jackpot
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
byli	být	k5eAaImAgMnP	být
údajně	údajně	k6eAd1	údajně
ochotni	ochoten	k2eAgMnPc1d1	ochoten
prosazovat	prosazovat	k5eAaImF	prosazovat
změnu	změna	k1gFnSc4	změna
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
loteriích	loterie	k1gFnPc6	loterie
za	za	k7c4	za
úplatek	úplatek	k1gInSc4	úplatek
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
případnému	případný	k2eAgNnSc3d1	případné
dárci	dárce	k1gMnPc1	dárce
peněz	peníze	k1gInPc2	peníze
skrytí	skrytí	k1gNnPc2	skrytí
jeho	jeho	k3xOp3gFnSc2	jeho
identity	identita	k1gFnSc2	identita
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
finance	finance	k1gFnPc1	finance
nebyly	být	k5eNaImAgFnP	být
vykázány	vykázat	k5eAaPmNgFnP	vykázat
jako	jako	k8xS	jako
dar	dar	k1gInSc1	dar
pro	pro	k7c4	pro
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xC	jako
inzerce	inzerce	k1gFnSc1	inzerce
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
Haló	haló	k0	haló
noviny	novina	k1gFnPc1	novina
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
Milota	milota	k1gFnSc1	milota
poslal	poslat	k5eAaPmAgMnS	poslat
redaktorovi	redaktor	k1gMnSc3	redaktor
vydávajícímu	vydávající	k2eAgInSc3d1	vydávající
se	se	k3xPyFc4	se
za	za	k7c4	za
majitele	majitel	k1gMnSc4	majitel
herny	herna	k1gFnSc2	herna
návrh	návrh	k1gInSc4	návrh
smlouvy	smlouva	k1gFnSc2	smlouva
na	na	k7c4	na
inzerci	inzerce	k1gFnSc4	inzerce
ve	v	k7c6	v
stranickém	stranický	k2eAgInSc6d1	stranický
deníku	deník	k1gInSc6	deník
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
zveřejnění	zveřejnění	k1gNnSc6	zveřejnění
kauzy	kauza	k1gFnSc2	kauza
Dolejš	Dolejš	k1gMnSc1	Dolejš
a	a	k8xC	a
Milota	milota	k1gFnSc1	milota
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
rezignovali	rezignovat	k5eAaBmAgMnP	rezignovat
na	na	k7c4	na
funkce	funkce	k1gFnPc4	funkce
místopředsedů	místopředseda	k1gMnPc2	místopředseda
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
<s>
Dolejš	Dolejš	k1gMnSc1	Dolejš
i	i	k8xC	i
Milota	milota	k1gFnSc1	milota
korupci	korupce	k1gFnSc4	korupce
popírali	popírat	k5eAaImAgMnP	popírat
a	a	k8xC	a
trvali	trvat	k5eAaImAgMnP	trvat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
nevině	nevina	k1gFnSc6	nevina
<g/>
.	.	kIx.	.
</s>
<s>
Dolejš	Dolejš	k1gMnSc1	Dolejš
vydal	vydat	k5eAaPmAgMnS	vydat
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
podal	podat	k5eAaPmAgMnS	podat
u	u	k7c2	u
Městského	městský	k2eAgInSc2d1	městský
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
žalobu	žaloba	k1gFnSc4	žaloba
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
osobnosti	osobnost	k1gFnSc2	osobnost
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
do	do	k7c2	do
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
soudu	soud	k1gInSc3	soud
pozastavení	pozastavení	k1gNnSc2	pozastavení
výkonu	výkon	k1gInSc2	výkon
funkce	funkce	k1gFnSc2	funkce
místopředsedy	místopředseda	k1gMnSc2	místopředseda
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Případ	případ	k1gInSc4	případ
korupce	korupce	k1gFnSc2	korupce
vyšetřovala	vyšetřovat	k5eAaImAgFnS	vyšetřovat
policie	policie	k1gFnSc1	policie
a	a	k8xC	a
ani	ani	k8xC	ani
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
místopředsedů	místopředseda	k1gMnPc2	místopředseda
nakonec	nakonec	k6eAd1	nakonec
z	z	k7c2	z
korupce	korupce	k1gFnSc2	korupce
neobvinila	obvinit	k5eNaPmAgFnS	obvinit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kondolence	kondolence	k1gFnSc2	kondolence
KSČM	KSČM	kA	KSČM
k	k	k7c3	k
úmrtí	úmrtí	k1gNnSc3	úmrtí
Kim	Kim	k1gMnSc2	Kim
Čong-ila	Čongl	k1gMnSc2	Čong-il
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2011	[number]	k4	2011
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
mrtvici	mrtvice	k1gFnSc6	mrtvice
vůdce	vůdce	k1gMnSc2	vůdce
Severní	severní	k2eAgFnSc2d1	severní
Koreje	Korea	k1gFnSc2	Korea
Kim	Kim	k1gMnSc1	Kim
Čong-il	Čongl	k1gMnSc1	Čong-il
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
obviňován	obviňovat	k5eAaImNgInS	obviňovat
ze	z	k7c2	z
soustavného	soustavný	k2eAgNnSc2d1	soustavné
porušování	porušování	k1gNnSc2	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
a	a	k8xC	a
občanských	občanský	k2eAgNnPc2d1	občanské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
<g/>
KSČM	KSČM	kA	KSČM
reagovala	reagovat	k5eAaBmAgFnS	reagovat
slovy	slovo	k1gNnPc7	slovo
svého	svůj	k3xOyFgMnSc2	svůj
předsedy	předseda	k1gMnSc2	předseda
Vojtěcha	Vojtěch	k1gMnSc4	Vojtěch
Filipa	Filip	k1gMnSc2	Filip
na	na	k7c4	na
úmrtí	úmrtí	k1gNnSc4	úmrtí
Kim	Kim	k1gFnSc2	Kim
Čong-ila	Čongl	k1gMnSc2	Čong-il
kondolencí	kondolence	k1gFnPc2	kondolence
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
uznání	uznání	k1gNnSc4	uznání
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c4	o
blaho	blaho	k1gNnSc4	blaho
Korejců	Korejec	k1gMnPc2	Korejec
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kondolenci	kondolence	k1gFnSc3	kondolence
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
připojily	připojit	k5eAaPmAgFnP	připojit
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
samotné	samotný	k2eAgFnPc1d1	samotná
stranické	stranický	k2eAgFnPc1d1	stranická
buňky	buňka	k1gFnPc1	buňka
KSČM	KSČM	kA	KSČM
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Stranicky	stranicky	k6eAd1	stranicky
profilovaný	profilovaný	k2eAgInSc1d1	profilovaný
deník	deník	k1gInSc1	deník
Haló	haló	k0	haló
noviny	novina	k1gFnPc4	novina
pak	pak	k6eAd1	pak
referoval	referovat	k5eAaBmAgInS	referovat
o	o	k7c6	o
"	"	kIx"	"
<g/>
zármutku	zármutek	k1gInSc6	zármutek
Severokorejců	Severokorejec	k1gMnPc2	Severokorejec
<g/>
"	"	kIx"	"
a	a	k8xC	a
vřelých	vřelý	k2eAgFnPc6d1	vřelá
kondolencích	kondolence	k1gFnPc6	kondolence
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
<g/>
Část	část	k1gFnSc1	část
politické	politický	k2eAgFnSc2d1	politická
scény	scéna	k1gFnSc2	scéna
požadovala	požadovat	k5eAaImAgFnS	požadovat
prošetření	prošetření	k1gNnSc4	prošetření
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
kondolence	kondolence	k1gFnSc1	kondolence
neporušila	porušit	k5eNaPmAgFnS	porušit
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyjadřovala	vyjadřovat	k5eAaImAgFnS	vyjadřovat
sympatie	sympatie	k1gFnSc1	sympatie
k	k	k7c3	k
hnutí	hnutí	k1gNnSc3	hnutí
založenému	založený	k2eAgInSc3d1	založený
na	na	k7c4	na
potlačování	potlačování	k1gNnPc4	potlačování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
politici	politik	k1gMnPc1	politik
naopak	naopak	k6eAd1	naopak
nebyli	být	k5eNaImAgMnP	být
pro	pro	k7c4	pro
podání	podání	k1gNnSc4	podání
trestního	trestní	k2eAgNnSc2d1	trestní
oznámení	oznámení	k1gNnSc2	oznámení
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
strachu	strach	k1gInSc2	strach
z	z	k7c2	z
nárůstu	nárůst	k1gInSc2	nárůst
popularity	popularita	k1gFnSc2	popularita
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
<g/>
Předseda	předseda	k1gMnSc1	předseda
KSČM	KSČM	kA	KSČM
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Filip	Filip	k1gMnSc1	Filip
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Hydepark	Hydepark	k1gInSc1	Hydepark
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
poslání	poslání	k1gNnSc1	poslání
kondolence	kondolence	k1gFnSc2	kondolence
faktem	fakt	k1gInSc7	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
smrt	smrt	k1gFnSc4	smrt
Kim	Kim	k1gMnPc2	Kim
Čong-Ila	Čong-Ilo	k1gNnSc2	Čong-Ilo
drželo	držet	k5eAaImAgNnS	držet
minutu	minuta	k1gFnSc4	minuta
ticha	ticho	k1gNnSc2	ticho
i	i	k8xC	i
OSN	OSN	kA	OSN
a	a	k8xC	a
kondolenci	kondolence	k1gFnSc4	kondolence
poslal	poslat	k5eAaPmAgMnS	poslat
např.	např.	kA	např.
i	i	k8xC	i
rakouský	rakouský	k2eAgMnSc1d1	rakouský
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Volební	volební	k2eAgInPc1d1	volební
výsledky	výsledek	k1gInPc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Sněmovní	sněmovní	k2eAgFnPc1d1	sněmovní
volby	volba	k1gFnPc1	volba
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Senátní	senátní	k2eAgFnPc1d1	senátní
volby	volba	k1gFnPc1	volba
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Komunální	komunální	k2eAgFnPc1d1	komunální
volby	volba	k1gFnPc1	volba
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Krajské	krajský	k2eAgFnSc2d1	krajská
volby	volba	k1gFnSc2	volba
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
předsedů	předseda	k1gMnPc2	předseda
ÚV	ÚV	kA	ÚV
KSČM	KSČM	kA	KSČM
==	==	k?	==
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Machalík	Machalík	k1gMnSc1	Machalík
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1990	[number]	k4	1990
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1993	[number]	k4	1993
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Filip	Filip	k1gMnSc1	Filip
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
Šablona	šablona	k1gFnSc1	šablona
<g/>
:	:	kIx,	:
<g/>
Seznam	seznam	k1gInSc1	seznam
předsedů	předseda	k1gMnPc2	předseda
ÚV	ÚV	kA	ÚV
KSČM	KSČM	kA	KSČM
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Balík	Balík	k1gMnSc1	Balík
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
KSČM	KSČM	kA	KSČM
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
minulosti	minulost	k1gFnSc3	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Středoevropské	středoevropský	k2eAgFnPc1d1	středoevropská
politické	politický	k2eAgFnPc1d1	politická
studie	studie	k1gFnPc1	studie
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
politologický	politologický	k2eAgInSc1d1	politologický
ústav	ústav	k1gInSc1	ústav
MU	MU	kA	MU
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
VI	VI	kA	VI
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
od	od	k7c2	od
s.	s.	k?	s.
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
13	[number]	k4	13
s.	s.	k?	s.
ISSN	ISSN	kA	ISSN
1212	[number]	k4	1212
<g/>
-	-	kIx~	-
<g/>
7817	[number]	k4	7817
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
URL	URL	kA	URL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Drda	Drda	k1gMnSc1	Drda
<g/>
,	,	kIx,	,
Adam	Adam	k1gMnSc1	Adam
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Dudek	Dudek	k1gMnSc1	Dudek
<g/>
:	:	kIx,	:
Kdo	kdo	k3yRnSc1	kdo
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
komunisté	komunista	k1gMnPc1	komunista
po	po	k7c6	po
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
Paseka	paseka	k1gFnSc1	paseka
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
802	[number]	k4	802
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Stránka	stránka	k1gFnSc1	stránka
na	na	k7c6	na
webu	web	k1gInSc6	web
nakladatele	nakladatel	k1gMnSc2	nakladatel
s	s	k7c7	s
recenzemi	recenze	k1gFnPc7	recenze
<g/>
,	,	kIx,	,
kritika	kritika	k1gFnSc1	kritika
Štěpána	Štěpána	k1gFnSc1	Štěpána
Kotrby	kotrba	k1gFnSc2	kotrba
(	(	kIx(	(
<g/>
Britské	britský	k2eAgInPc1d1	britský
listy	list	k1gInPc1	list
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
–	–	k?	–
Holzer	Holzer	k1gMnSc1	Holzer
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
–	–	k?	–
Mareš	Mareš	k1gMnSc1	Mareš
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
–	–	k?	–
Pšeja	Pšeja	k1gMnSc1	Pšeja
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Komunismus	komunismus	k1gInSc1	komunismus
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vývojové	vývojový	k2eAgInPc4d1	vývojový
<g/>
,	,	kIx,	,
systémové	systémový	k2eAgInPc4d1	systémový
a	a	k8xC	a
ideové	ideový	k2eAgInPc4d1	ideový
aspekty	aspekt	k1gInPc4	aspekt
působení	působení	k1gNnSc2	působení
KSČM	KSČM	kA	KSČM
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
komunistických	komunistický	k2eAgFnPc2d1	komunistická
organizací	organizace	k1gFnPc2	organizace
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
MPÚ	MPÚ	kA	MPÚ
MU	MU	kA	MU
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
315	[number]	k4	315
s.	s.	k?	s.
monografie	monografie	k1gFnSc2	monografie
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
210	[number]	k4	210
<g/>
-	-	kIx~	-
<g/>
2249	[number]	k4	2249
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Československa	Československo	k1gNnSc2	Československo
</s>
</p>
<p>
<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Československa	Československo	k1gNnSc2	Československo
–	–	k?	–
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
práce	práce	k1gFnSc2	práce
</s>
</p>
<p>
<s>
Komunistický	komunistický	k2eAgInSc1d1	komunistický
svaz	svaz	k1gInSc1	svaz
mládeže	mládež	k1gFnSc2	mládež
</s>
</p>
<p>
<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Slovenska	Slovensko	k1gNnSc2	Slovensko
</s>
</p>
<p>
<s>
Kominterna	Kominterna	k1gFnSc1	Kominterna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gFnSc1	téma
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
<p>
<s>
Video	video	k1gNnSc1	video
s	s	k7c7	s
přednáškou	přednáška	k1gFnSc7	přednáška
bývalého	bývalý	k2eAgMnSc2d1	bývalý
předsedy	předseda	k1gMnSc2	předseda
KSČM	KSČM	kA	KSČM
Miroslava	Miroslav	k1gMnSc4	Miroslav
Grebeníčka	Grebeníčko	k1gNnSc2	Grebeníčko
o	o	k7c6	o
vniku	vnik	k1gInSc6	vnik
strany	strana	k1gFnSc2	strana
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
vlastně	vlastně	k9	vlastně
vypadá	vypadat	k5eAaImIp3nS	vypadat
dnešní	dnešní	k2eAgNnSc4d1	dnešní
KSČM	KSČM	kA	KSČM
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
idea	idea	k1gFnSc1	idea
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
reflexe	reflexe	k1gFnSc1	reflexe
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
i	i	k8xC	i
teorii	teorie	k1gFnSc6	teorie
</s>
</p>
<p>
<s>
Svět	svět	k1gInSc1	svět
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
KSČM	KSČM	kA	KSČM
u	u	k7c2	u
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
Češi	Čech	k1gMnPc1	Čech
ne	ne	k9	ne
</s>
</p>
<p>
<s>
https://www.kscm.cz/sites/default/files/soubory/Stanovy/stanovy_kscm_2016.pdf	[url]	k1gMnSc1	https://www.kscm.cz/sites/default/files/soubory/Stanovy/stanovy_kscm_2016.pdf
</s>
</p>
