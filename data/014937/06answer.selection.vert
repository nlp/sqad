<s desamb="1">
Náčelníkova	náčelníkův	k2eAgFnSc1d1
dcera	dcera	k1gFnSc1
Vaiana	Vaiana	k1gFnSc1
(	(	kIx(
<g/>
Moana	Moana	k1gFnSc1
-	-	kIx~
havajsky	havajsky	k6eAd1
moře	moře	k1gNnSc1
<g/>
)	)	kIx)
od	od	k7c2
malička	maličko	k1gNnSc2
vyrůstá	vyrůstat	k5eAaImIp3nS
na	na	k7c6
příbězích	příběh	k1gInPc6
své	svůj	k3xOyFgFnSc2
babičky	babička	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgNnPc6,k3yRgNnPc6,k3yQgNnPc6
polobůh	polobůh	k1gMnSc1
Maui	Maui	k1gNnSc2
uloupil	uloupit	k5eAaPmAgMnS
bohyni	bohyně	k1gFnSc4
života	život	k1gInSc2
a	a	k8xC
rostlin	rostlina	k1gFnPc2
Te	Te	k1gFnSc1
Fiti	Fit	k1gFnSc2
její	její	k3xOp3gNnSc4
srdce	srdce	k1gNnSc4
<g/>
.	.	kIx.
</s>