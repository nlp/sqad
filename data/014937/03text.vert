<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Moana	Moana	k1gFnSc1
Země	zem	k1gFnSc2
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Jazyk	jazyk	k1gInSc4
</s>
<s>
angličtina	angličtina	k1gFnSc1
Délka	délka	k1gFnSc1
</s>
<s>
113	#num#	k4
minut	minuta	k1gFnPc2
Scénář	scénář	k1gInSc4
</s>
<s>
Jared	Jared	k1gMnSc1
Bush	Bush	k1gMnSc1
Režie	režie	k1gFnSc1
</s>
<s>
Ron	Ron	k1gMnSc1
Clements	Clementsa	k1gFnPc2
John	John	k1gMnSc1
Musker	Musker	k1gMnSc1
Obsazení	obsazení	k1gNnPc2
a	a	k8xC
filmový	filmový	k2eAgInSc1d1
štáb	štáb	k1gInSc1
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Auli	Auli	k1gNnSc1
<g/>
'	'	kIx"
<g/>
i	i	k9
CravalhoDwayne	CravalhoDwayn	k1gInSc5
JohnsonRachel	JohnsonRachela	k1gFnPc2
HouseTemuera	HouseTemuer	k1gMnSc2
MorrisonJemaine	MorrisonJemain	k1gInSc5
ClementNicole	ClementNicole	k1gFnSc1
ScherzingerováAlan	ScherzingerováAlan	k1gInSc4
Tudyk	Tudyk	k1gInSc1
Produkce	produkce	k1gFnSc1
</s>
<s>
Osnat	Osnat	k2eAgInSc1d1
Shurer	Shurer	k1gInSc1
Hudba	hudba	k1gFnSc1
</s>
<s>
Mark	Mark	k1gMnSc1
Mancina	Mancina	k1gMnSc1
Lin-Manuel	Lin-Manuel	k1gMnSc1
Miranda	Miranda	k1gFnSc1
Opetaia	Opetaius	k1gMnSc2
Foa	Foa	k1gMnSc2
<g/>
'	'	kIx"
<g/>
i	i	k9
Střih	střih	k1gInSc1
</s>
<s>
Jeff	Jeff	k1gMnSc1
Draheim	Draheim	k1gMnSc1
Výroba	výroba	k1gFnSc1
a	a	k8xC
distribuce	distribuce	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
Česká	český	k2eAgFnSc1d1
premiéra	premiéra	k1gFnSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
Produkční	produkční	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
</s>
<s>
Walt	Walt	k1gMnSc1
Disney	Disnea	k1gFnSc2
Pictures	Pictures	k1gMnSc1
Walter	Walter	k1gMnSc1
Disney	Disnea	k1gFnSc2
Animation	Animation	k1gInSc1
Studios	Studios	k?
Distribuce	distribuce	k1gFnSc1
</s>
<s>
Walt	Walt	k1gMnSc1
Disney	Disnea	k1gFnSc2
Studios	Studios	k?
Motion	Motion	k1gInSc1
Pictures	Pictures	k1gInSc1
Česká	český	k2eAgFnSc1d1
distribuce	distribuce	k1gFnSc1
</s>
<s>
Falcon	Falcon	k1gInSc1
Rozpočet	rozpočet	k1gInSc1
</s>
<s>
150	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
Tržby	tržba	k1gFnSc2
</s>
<s>
533,7	533,7	k4
milionu	milion	k4xCgInSc2
dolarů	dolar	k1gInPc2
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
na	na	k7c6
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
Kinoboxu	Kinobox	k1gInSc2
<g/>
,	,	kIx,
FDb	FDb	k1gFnPc1
<g/>
,	,	kIx,
IMDbNěkterá	IMDbNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
(	(	kIx(
<g/>
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
originále	originál	k1gInSc6
Moana	Moana	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americký	americký	k2eAgInSc1d1
animovaný	animovaný	k2eAgInSc1d1
muzikálový	muzikálový	k2eAgInSc1d1
film	film	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
<g/>
,	,	kIx,
produkovaný	produkovaný	k2eAgMnSc1d1
studiem	studio	k1gNnSc7
Walt	Walta	k1gFnPc2
Disney	Disnea	k1gFnSc2
Animation	Animation	k1gInSc1
Studios	Studios	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
56	#num#	k4
<g/>
.	.	kIx.
animovaný	animovaný	k2eAgInSc1d1
Disney	Disnea	k1gFnPc4
film	film	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Režie	režie	k1gFnSc1
se	se	k3xPyFc4
ujali	ujmout	k5eAaPmAgMnP
Ron	Ron	k1gMnSc1
Clements	Clements	k1gInSc1
a	a	k8xC
John	John	k1gMnSc1
Musker	Musker	k1gMnSc1
<g/>
,	,	kIx,
spolu-režírovali	spolu-režírovat	k5eAaImAgMnP,k5eAaPmAgMnP,k5eAaBmAgMnP
Don	Don	k1gMnSc1
Hall	Hall	k1gMnSc1
a	a	k8xC
Chris	Chris	k1gFnSc1
Williams	Williamsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
hudbě	hudba	k1gFnSc6
k	k	k7c3
filmu	film	k1gInSc3
se	se	k3xPyFc4
podíleli	podílet	k5eAaImAgMnP
Lin-Manuel	Lin-Manuel	k1gMnSc1
Miranda	Miranda	k1gFnSc1
<g/>
,	,	kIx,
Opetaia	Opetaia	k1gFnSc1
Foa	Foa	k1gFnSc1
<g/>
'	'	kIx"
<g/>
i	i	k9
a	a	k8xC
Mark	Mark	k1gMnSc1
Mancina	Mancina	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgInPc4
hlasy	hlas	k1gInPc4
ve	v	k7c6
filmu	film	k1gInSc6
poskytli	poskytnout	k5eAaPmAgMnP
Auli	Aul	k1gMnPc1
<g/>
'	'	kIx"
<g/>
i	i	k8xC
Cravalho	Craval	k1gMnSc2
<g/>
,	,	kIx,
Dwayne	Dwayn	k1gInSc5
Johnson	Johnson	k1gMnSc1
<g/>
,	,	kIx,
Rachel	Rachel	k1gMnSc1
House	house	k1gNnSc1
<g/>
,	,	kIx,
Temuera	Temuera	k1gFnSc1
Morrison	Morrison	k1gInSc1
<g/>
,	,	kIx,
Jemaine	Jemain	k1gInSc5
Clement	Clement	k1gInSc1
<g/>
,	,	kIx,
Nicole	Nicole	k1gFnSc1
Scherzinger	Scherzinger	k1gMnSc1
a	a	k8xC
Alan	Alan	k1gMnSc1
Tudyk	Tudyk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Film	film	k1gInSc1
byl	být	k5eAaImAgInS
oficiálně	oficiálně	k6eAd1
do	do	k7c2
kin	kino	k1gNnPc2
uveden	uvést	k5eAaPmNgInS
23	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
získal	získat	k5eAaPmAgInS
velmi	velmi	k6eAd1
pozitivní	pozitivní	k2eAgFnPc1d1
recenze	recenze	k1gFnPc1
od	od	k7c2
kritiků	kritik	k1gMnPc2
a	a	k8xC
za	za	k7c4
první	první	k4xOgInPc4
tři	tři	k4xCgInPc4
dny	den	k1gInPc4
v	v	k7c6
kinech	kino	k1gNnPc6
vydělal	vydělat	k5eAaPmAgInS
47	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Auli	Auli	k1gNnSc1
<g/>
'	'	kIx"
<g/>
i	i	k8xC
Cravalho	Craval	k1gMnSc2
na	na	k7c6
premiéře	premiéra	k1gFnSc6
filmu	film	k1gInSc2
v	v	k7c6
Samoi	Samoa	k1gFnSc6
v	v	k7c6
prosinci	prosinec	k1gInSc6
roku	rok	k1gInSc2
2016	#num#	k4
</s>
<s>
Děj	děj	k1gInSc1
</s>
<s>
Příběh	příběh	k1gInSc1
začíná	začínat	k5eAaImIp3nS
na	na	k7c6
ostrově	ostrov	k1gInSc6
Motunui	Motunu	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
žijí	žít	k5eAaImIp3nP
lidé	člověk	k1gMnPc1
podle	podle	k7c2
staré	starý	k2eAgFnSc2d1
<g/>
,	,	kIx,
havajské	havajský	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náčelníkova	náčelníkův	k2eAgFnSc1d1
dcera	dcera	k1gFnSc1
Vaiana	Vaiana	k1gFnSc1
(	(	kIx(
<g/>
Moana	Moana	k1gFnSc1
-	-	kIx~
havajsky	havajsky	k6eAd1
moře	moře	k1gNnSc1
<g/>
)	)	kIx)
od	od	k7c2
malička	maličko	k1gNnSc2
vyrůstá	vyrůstat	k5eAaImIp3nS
na	na	k7c6
příbězích	příběh	k1gInPc6
své	svůj	k3xOyFgFnSc2
babičky	babička	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgNnPc6,k3yRgNnPc6,k3yQgNnPc6
polobůh	polobůh	k1gMnSc1
Maui	Maui	k1gNnSc2
uloupil	uloupit	k5eAaPmAgMnS
bohyni	bohyně	k1gFnSc4
života	život	k1gInSc2
a	a	k8xC
rostlin	rostlina	k1gFnPc2
Te	Te	k1gFnSc1
Fiti	Fit	k1gFnSc2
její	její	k3xOp3gNnSc4
srdce	srdce	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domníval	domnívat	k5eAaImAgMnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
srdce	srdce	k1gNnSc2
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
<g/>
,	,	kIx,
získá	získat	k5eAaPmIp3nS
moc	moc	k6eAd1
tvořit	tvořit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
se	se	k3xPyFc4
však	však	k9
střetl	střetnout	k5eAaPmAgInS
v	v	k7c6
líté	lítý	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
s	s	k7c7
démonem	démon	k1gMnSc7
lávy	láva	k1gFnSc2
jménem	jméno	k1gNnSc7
Te	Te	k1gFnPc2
Ka	Ka	k1gMnPc2
<g/>
,	,	kIx,
přišel	přijít	k5eAaPmAgMnS
o	o	k7c4
srdce	srdce	k1gNnSc4
Te	Te	k1gFnSc2
Fiti	Fit	k1gFnSc2
<g/>
,	,	kIx,
o	o	k7c4
svůj	svůj	k3xOyFgInSc4
magický	magický	k2eAgInSc4d1
hák	hák	k1gInSc4
a	a	k8xC
byl	být	k5eAaImAgMnS
vypovězen	vypovědět	k5eAaPmNgMnS
na	na	k7c4
pustý	pustý	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
do	do	k7c2
zapomnění	zapomnění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
prý	prý	k9
však	však	k9
někdo	někdo	k3yInSc1
z	z	k7c2
lidu	lid	k1gInSc2
Motunui	Motunu	k1gFnSc2
vypluje	vyplout	k5eAaPmIp3nS
jako	jako	k9
první	první	k4xOgFnSc7
po	po	k7c6
generacích	generace	k1gFnPc6
za	za	k7c4
útesy	útes	k1gInPc4
ostrova	ostrov	k1gInSc2
<g/>
,	,	kIx,
najde	najít	k5eAaPmIp3nS
poloboha	polobůh	k1gMnSc4
Mauiho	Maui	k1gMnSc4
<g/>
,	,	kIx,
převeze	převézt	k5eAaPmIp3nS
ho	on	k3xPp3gMnSc4
přes	přes	k7c4
oceán	oceán	k1gInSc4
<g/>
,	,	kIx,
vrátí	vrátit	k5eAaPmIp3nS
Te	Te	k1gFnSc1
Fiti	Fit	k1gFnSc2
její	její	k3xOp3gNnSc4
srdce	srdce	k1gNnSc4
a	a	k8xC
zachrání	zachránit	k5eAaPmIp3nP
svět	svět	k1gInSc4
od	od	k7c2
kletby	kletba	k1gFnSc2
Te	Te	k1gMnSc2
Ka	Ka	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Maličká	maličký	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
je	být	k5eAaImIp3nS
legendami	legenda	k1gFnPc7
unešena	unešen	k2eAgMnSc4d1
a	a	k8xC
ze	z	k7c2
srdce	srdce	k1gNnSc2
jim	on	k3xPp3gMnPc3
věří	věřit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
když	když	k8xS
se	se	k3xPyFc4
pak	pak	k6eAd1
jednoho	jeden	k4xCgInSc2
dne	den	k1gInSc2
přes	přes	k7c4
zákaz	zákaz	k1gInSc4
rodičů	rodič	k1gMnPc2
vypraví	vypravit	k5eAaPmIp3nS
na	na	k7c4
břeh	břeh	k1gInSc4
a	a	k8xC
spatří	spatřit	k5eAaPmIp3nP
překrásnou	překrásný	k2eAgFnSc4d1
mušli	mušle	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zachránila	zachránit	k5eAaPmAgFnS
z	z	k7c2
ptačích	ptačí	k2eAgInPc2d1
spárů	spár	k1gInPc2
malou	malý	k2eAgFnSc4d1
želvičku	želvička	k1gFnSc4
<g/>
,	,	kIx,
nechá	nechat	k5eAaPmIp3nS
mušli	mušle	k1gFnSc4
uplavat	uplavat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oceán	oceán	k1gInSc1
tak	tak	k6eAd1
spatří	spatřit	k5eAaPmIp3nS
její	její	k3xOp3gFnSc4
dobrou	dobrý	k2eAgFnSc4d1
duši	duše	k1gFnSc4
a	a	k8xC
svěří	svěřit	k5eAaPmIp3nS
jí	on	k3xPp3gFnSc3
srdce	srdce	k1gNnSc4
Te	Te	k1gMnSc3
Fiti	Fit	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
ne	ne	k9
čtyřletá	čtyřletý	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
ho	on	k3xPp3gMnSc4
však	však	k9
ztratí	ztratit	k5eAaPmIp3nS
a	a	k8xC
nevědomky	nevědomky	k6eAd1
ho	on	k3xPp3gMnSc4
tak	tak	k9
svěří	svěřit	k5eAaPmIp3nS
do	do	k7c2
opatrování	opatrování	k1gNnSc4
své	svůj	k3xOyFgFnSc3
moudré	moudrý	k2eAgFnSc3d1
babičce	babička	k1gFnSc3
<g/>
,	,	kIx,
matce	matka	k1gFnSc3
náčelníka	náčelník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
té	ten	k3xDgFnSc2
chvíle	chvíle	k1gFnSc2
Vaianě	Vaiaň	k1gFnSc2
v	v	k7c6
mysli	mysl	k1gFnSc6
zakořenila	zakořenit	k5eAaPmAgFnS
myšlenka	myšlenka	k1gFnSc1
na	na	k7c4
oceán	oceán	k1gInSc4
za	za	k7c4
útesy	útes	k1gInPc4
ostrova	ostrov	k1gInSc2
Motunui	Motunu	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
místo	místo	k1gNnSc4
všem	všecek	k3xTgMnPc3
z	z	k7c2
lidu	lid	k1gInSc2
zapovězené	zapovězený	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jejím	její	k3xOp3gNnSc6
podvědomí	podvědomí	k1gNnSc6
stále	stále	k6eAd1
zůstává	zůstávat	k5eAaImIp3nS
vzpomínka	vzpomínka	k1gFnSc1
na	na	k7c4
oživlý	oživlý	k2eAgInSc4d1
a	a	k8xC
laskavý	laskavý	k2eAgInSc4d1
oceán	oceán	k1gInSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
jí	jíst	k5eAaImIp3nS
předal	předat	k5eAaPmAgMnS
mnoha	mnoho	k4c3
příšerami	příšera	k1gFnPc7
usilovně	usilovně	k6eAd1
hledané	hledaný	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
Te	Te	k1gFnSc2
Fiti	Fit	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
Vaiana	Vaiana	k1gFnSc1
prostě	prostě	k6eAd1
smíří	smířit	k5eAaPmIp3nS
s	s	k7c7
myšlenkou	myšlenka	k1gFnSc7
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
volá	volat	k5eAaImIp3nS
obzor	obzor	k1gInSc1
a	a	k8xC
oceán	oceán	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
že	že	k8xS
na	na	k7c4
lákavé	lákavý	k2eAgFnPc4d1
krásné	krásný	k2eAgFnPc4d1
plachetnice	plachetnice	k1gFnPc4
zkrátka	zkrátka	k6eAd1
nesmí	smět	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
ale	ale	k8xC
následnice	následnice	k1gFnSc1
trůnu	trůn	k1gInSc2
Motunui	Motunu	k1gFnSc2
dospěje	dochvít	k5eAaPmIp3nS
<g/>
,	,	kIx,
udeří	udeřit	k5eAaPmIp3nS
na	na	k7c4
ostrov	ostrov	k1gInSc4
kletba	kletba	k1gFnSc1
Te	Te	k1gFnSc1
Ka	Ka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ryby	Ryby	k1gFnPc1
se	se	k3xPyFc4
z	z	k7c2
moře	moře	k1gNnSc2
vytratí	vytratit	k5eAaPmIp3nP
a	a	k8xC
rostliny	rostlina	k1gFnPc1
začnou	začít	k5eAaPmIp3nP
usychat	usychat	k5eAaImF
a	a	k8xC
černat	černat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přežití	přežití	k1gNnSc1
lidu	lid	k1gInSc2
visí	viset	k5eAaImIp3nS
na	na	k7c6
vlásku	vlásek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
je	být	k5eAaImIp3nS
už	už	k6eAd1
konečně	konečně	k6eAd1
na	na	k7c6
čase	čas	k1gInSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
téměř	téměř	k6eAd1
dospělá	dospělý	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
dozvěděla	dozvědět	k5eAaPmAgFnS
vše	všechen	k3xTgNnSc4
o	o	k7c6
svých	svůj	k3xOyFgFnPc6
odvážných	odvážný	k2eAgFnPc6d1
prapředcích	prapředek	k1gMnPc6
mořeplavcích	mořeplavec	k1gMnPc6
<g/>
,	,	kIx,
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
má	mít	k5eAaImIp3nS
srdce	srdce	k1gNnSc4
Te	Te	k1gFnSc2
Fiti	Fit	k1gFnSc2
<g/>
,	,	kIx,
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
děje	dít	k5eAaImIp3nS
s	s	k7c7
ostrovem	ostrov	k1gInSc7
<g/>
,	,	kIx,
o	o	k7c6
bájných	bájný	k2eAgFnPc6d1
schopnostech	schopnost	k1gFnPc6
oceánu	oceán	k1gInSc2
i	i	k8xC
o	o	k7c6
svém	svůj	k3xOyFgNnSc6
vlastním	vlastní	k2eAgNnSc6d1
poslání	poslání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
to	ten	k3xDgNnSc1
vše	všechen	k3xTgNnSc1
jí	on	k3xPp3gFnSc7
může	moct	k5eAaImIp3nS
říci	říct	k5eAaPmF
pouze	pouze	k6eAd1
jediná	jediný	k2eAgFnSc1d1
osoba	osoba	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
tou	ten	k3xDgFnSc7
je	být	k5eAaImIp3nS
babička	babička	k1gFnSc1
Tala	Tala	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ní	on	k3xPp3gFnSc6
teď	teď	k6eAd1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
si	se	k3xPyFc3
uvědomila	uvědomit	k5eAaPmAgFnS
včas	včas	k6eAd1
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
děje	dít	k5eAaImIp3nS
a	a	k8xC
svěřila	svěřit	k5eAaPmAgFnS
Vaianě	Vaiaň	k1gFnPc4
dávné	dávný	k2eAgNnSc4d1
tajemství	tajemství	k1gNnSc4
<g/>
,	,	kIx,
než	než	k8xS
přijde	přijít	k5eAaPmIp3nS
její	její	k3xOp3gInSc4
čas	čas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
A	a	k9
tak	tak	k6eAd1
se	se	k3xPyFc4
mladá	mladý	k2eAgFnSc1d1
dívka	dívka	k1gFnSc1
s	s	k7c7
obrovským	obrovský	k2eAgInSc7d1
břímě	břímě	k1gNnSc4
na	na	k7c6
bedrech	bedra	k1gNnPc6
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
za	za	k7c4
útes	útes	k1gInSc4
<g/>
,	,	kIx,
najít	najít	k5eAaPmF
poloboha	polobůh	k1gMnSc2
Mauiho	Maui	k1gMnSc2
a	a	k8xC
vrátit	vrátit	k5eAaPmF
bohyni	bohyně	k1gFnSc4
Te	Te	k1gMnSc2
Fiti	Fit	k1gFnSc2
její	její	k3xOp3gNnSc1
srdce	srdce	k1gNnSc1
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
bude	být	k5eAaImBp3nS
pozdě	pozdě	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Auli	Auli	k1gNnSc1
<g/>
'	'	kIx"
<g/>
i	i	k9
Cravalho	Craval	k1gMnSc2
</s>
<s>
Vaiana	Vaiana	k1gFnSc1
</s>
<s>
Louise	Louis	k1gMnSc2
Bush	Bush	k1gMnSc1
</s>
<s>
malá	malý	k2eAgFnSc1d1
Moana	Moana	k1gFnSc1
(	(	kIx(
<g/>
zpěv	zpěv	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Dwayne	Dwaynout	k5eAaPmIp3nS
Johnson	Johnson	k1gMnSc1
</s>
<s>
Maui	Maui	k6eAd1
</s>
<s>
Rachel	Rachel	k1gInSc1
House	house	k1gNnSc1
</s>
<s>
babička	babička	k1gFnSc1
Tala	Tal	k1gInSc2
</s>
<s>
Temuera	Temuera	k1gFnSc1
Morrison	Morrisona	k1gFnPc2
</s>
<s>
Tui	Tui	k?
Waialiki	Waialiki	k1gNnSc1
</s>
<s>
Christopher	Christophra	k1gFnPc2
Jackson	Jackson	k1gMnSc1
</s>
<s>
Tui	Tui	k?
(	(	kIx(
<g/>
zpěv	zpěv	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Nicole	Nicole	k1gFnSc1
Scherzinger	Scherzingra	k1gFnPc2
</s>
<s>
Sina	sino	k1gNnPc1
Waialiki	Waialik	k1gFnSc2
</s>
<s>
Jemaine	Jemainout	k5eAaPmIp3nS
Clement	Clement	k1gInSc1
</s>
<s>
Tamatoa	Tamatoa	k6eAd1
</s>
<s>
Alan	Alan	k1gMnSc1
Tudyk	Tudyk	k1gMnSc1
</s>
<s>
Heihei	Heihei	k1gNnSc1
/	/	kIx~
osadník	osadník	k1gMnSc1
</s>
<s>
Oscar	Oscar	k1gInSc1
Kightley	Kightlea	k1gFnSc2
</s>
<s>
rybář	rybář	k1gMnSc1
</s>
<s>
Troy	Troa	k1gFnPc1
Polamalu	Polamal	k1gInSc2
</s>
<s>
osadník	osadník	k1gMnSc1
</s>
<s>
Puanani	Puanan	k1gMnPc1
Cravalho	Craval	k1gMnSc2
</s>
<s>
osadník	osadník	k1gMnSc1
/	/	kIx~
Pua	Pua	k1gMnSc1
</s>
<s>
České	český	k2eAgNnSc1d1
znění	znění	k1gNnSc1
</s>
<s>
Michaela	Michaela	k1gFnSc1
Tomešová	Tomešová	k1gFnSc1
</s>
<s>
Vaiana	Vaiana	k1gFnSc1
</s>
<s>
Klára	Klára	k1gFnSc1
Gibišová	Gibišová	k1gFnSc1
</s>
<s>
malinká	malinký	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
</s>
<s>
Martin	Martin	k1gMnSc1
Zounar	Zounar	k1gMnSc1
</s>
<s>
Maui	Maui	k6eAd1
</s>
<s>
Ludmila	Ludmila	k1gFnSc1
Molínová	Molínová	k1gFnSc1
</s>
<s>
babička	babička	k1gFnSc1
Tala	Tal	k1gInSc2
</s>
<s>
Petr	Petr	k1gMnSc1
Gelnar	Gelnar	k1gMnSc1
</s>
<s>
Tui	Tui	k?
Waialiki	Waialiki	k1gNnSc1
</s>
<s>
Jana	Jana	k1gFnSc1
Zenáhlíková	Zenáhlíková	k1gFnSc1
</s>
<s>
Sina	sino	k1gNnPc1
Waialiki	Waialik	k1gFnSc2
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Racek	racek	k1gMnSc1
</s>
<s>
Tamatoa	Tamatoa	k6eAd1
</s>
<s>
Milan	Milan	k1gMnSc1
Slepička	Slepička	k1gMnSc1
</s>
<s>
Heihei	Heihei	k1gNnSc1
/	/	kIx~
osadník	osadník	k1gMnSc1
</s>
<s>
David	David	k1gMnSc1
Voráček	Voráček	k1gMnSc1
</s>
<s>
osadník	osadník	k1gMnSc1
</s>
<s>
Barbora	Barbora	k1gFnSc1
Mošnová	Mošnová	k1gFnSc1
</s>
<s>
farmářka	farmářka	k1gFnSc1
</s>
<s>
Do	do	k7c2
češtiny	čeština	k1gFnSc2
film	film	k1gInSc1
přeložil	přeložit	k5eAaPmAgMnS
Vojtěch	Vojtěch	k1gMnSc1
Kostiha	Kostiha	k1gMnSc1
a	a	k8xC
režie	režie	k1gFnSc1
českého	český	k2eAgNnSc2d1
znění	znění	k1gNnSc2
se	se	k3xPyFc4
ujal	ujmout	k5eAaPmAgMnS
Zdeněk	Zdeněk	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Texty	text	k1gInPc4
k	k	k7c3
českým	český	k2eAgFnPc3d1
verzím	verze	k1gFnPc3
písní	píseň	k1gFnPc2
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
Robin	robin	k2eAgMnSc1d1
Král	Král	k1gMnSc1
a	a	k8xC
hudební	hudební	k2eAgFnSc4d1
režii	režie	k1gFnSc4
provedl	provést	k5eAaPmAgMnS
Ondřej	Ondřej	k1gMnSc1
Izdný	Izdný	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Přijetí	přijetí	k1gNnSc1
</s>
<s>
Film	film	k1gInSc1
byl	být	k5eAaImAgInS
oficiálně	oficiálně	k6eAd1
uveden	uvést	k5eAaPmNgInS
do	do	k7c2
kin	kino	k1gNnPc2
o	o	k7c6
víkendu	víkend	k1gInSc6
po	po	k7c6
Dni	den	k1gInSc6
díkuvzdání	díkuvzdání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrál	hrát	k5eAaImAgMnS
se	se	k3xPyFc4
ve	v	k7c4
3	#num#	k4
875	#num#	k4
kinech	kino	k1gNnPc6
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yQgNnPc2,k3yRgNnPc2,k3yIgNnPc2
80	#num#	k4
<g/>
%	%	kIx~
bylo	být	k5eAaImAgNnS
promítáno	promítat	k5eAaImNgNnS
ve	v	k7c4
3	#num#	k4
<g/>
D.	D.	kA
Za	za	k7c4
první	první	k4xOgInPc4
tři	tři	k4xCgInPc4
dny	den	k1gInPc4
byl	být	k5eAaImAgInS
plánován	plánován	k2eAgInSc1d1
výdělek	výdělek	k1gInSc1
50	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
,	,	kIx,
za	za	k7c4
pět	pět	k4xCc4
dní	den	k1gInPc2
75	#num#	k4
<g/>
–	–	k?
<g/>
85	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc7d3
výzvou	výzva	k1gFnSc7
byl	být	k5eAaImAgInS
film	film	k1gInSc4
Fantastická	fantastický	k2eAgNnPc4d1
zvířata	zvíře	k1gNnPc4
a	a	k8xC
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
najít	najít	k5eAaPmF
<g/>
,	,	kIx,
kde	kde	k6eAd1
projektovali	projektovat	k5eAaBmAgMnP
výdělek	výdělek	k1gInSc4
za	za	k7c4
druhý	druhý	k4xOgInSc4
víkend	víkend	k1gInSc4
tu	tu	k6eAd1
samou	samý	k3xTgFnSc4
částku	částka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgInSc1
promítací	promítací	k2eAgInSc1d1
den	den	k1gInSc1
vydělal	vydělat	k5eAaPmAgInS
15,68	15,68	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
nový	nový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
pro	pro	k7c4
Walt	Walt	k1gInSc4
Disney	Disnea	k1gFnSc2
Animation	Animation	k1gInSc1
Studios	Studios	k?
při	při	k7c6
premiéře	premiéra	k1gFnSc6
ve	v	k7c6
středu	střed	k1gInSc6
(	(	kIx(
<g/>
zlomil	zlomit	k5eAaPmAgMnS
rekord	rekord	k1gInSc4
filmu	film	k1gInSc2
Ledové	ledový	k2eAgNnSc4d1
království	království	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Den	den	k1gInSc4
Díkuvzdání	díkuvzdání	k1gNnPc2
film	film	k1gInSc1
získal	získat	k5eAaPmAgInS
9,9	9,9	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
a	a	k8xC
na	na	k7c4
Černý	černý	k2eAgInSc4d1
pátek	pátek	k1gInSc4
21,8	21,8	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Film	film	k1gInSc1
získal	získat	k5eAaPmAgInS
pozitivní	pozitivní	k2eAgFnPc4d1
recenze	recenze	k1gFnPc4
od	od	k7c2
kritiků	kritik	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
recenzní	recenzní	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
Rotten	Rotten	k2eAgInSc1d1
Tomatoes	Tomatoes	k1gInSc1
získal	získat	k5eAaPmAgInS
z	z	k7c2
120	#num#	k4
započtených	započtený	k2eAgFnPc2d1
recenzí	recenze	k1gFnPc2
98	#num#	k4
procent	procento	k1gNnPc2
s	s	k7c7
průměrným	průměrný	k2eAgInSc7d1
ratingem	rating	k1gInSc7
8	#num#	k4
bodů	bod	k1gInPc2
z	z	k7c2
deseti	deset	k4xCc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
serveru	server	k1gInSc6
Metacritic	Metacritice	k1gFnPc2
snímek	snímka	k1gFnPc2
získal	získat	k5eAaPmAgInS
z	z	k7c2
41	#num#	k4
recenzí	recenze	k1gFnPc2
81	#num#	k4
bodů	bod	k1gInPc2
ze	z	k7c2
sta	sto	k4xCgNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
CinemaScore	CinemaScor	k1gInSc5
snímku	snímek	k1gInSc2
udělil	udělit	k5eAaPmAgInS
známku	známka	k1gFnSc4
1	#num#	k4
na	na	k7c6
škále	škála	k1gFnSc6
1	#num#	k4
<g/>
+	+	kIx~
až	až	k9
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
snímek	snímek	k1gInSc1
získal	získat	k5eAaPmAgInS
83	#num#	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Datum	datum	k1gNnSc1
ceremoniálu	ceremoniál	k1gInSc2
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Nominovaní	nominovaný	k2eAgMnPc1d1
</s>
<s>
Výsledek	výsledek	k1gInSc1
</s>
<s>
AARP	AARP	kA
Annual	Annual	k1gMnSc1
Movies	Movies	k1gMnSc1
for	forum	k1gNnPc2
Grownups	Grownupsa	k1gFnPc2
Awards	Awardsa	k1gFnPc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
film	film	k1gInSc4
pro	pro	k7c4
dospělé	dospělí	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
odmítají	odmítat	k5eAaImIp3nP
vyrůst	vyrůst	k5eAaPmF
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
ACE	ACE	kA
Eddie	Eddie	k1gFnSc1
Awards	Awards	k1gInSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
střih	střih	k1gInSc4
v	v	k7c6
animovaném	animovaný	k2eAgInSc6d1
filmu	film	k1gInSc6
</s>
<s>
Draheim	Draheim	k1gMnSc1
Jeff	Jeff	k1gMnSc1
Draheim	Draheim	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Alliance	Alliance	k1gFnSc1
of	of	k?
Women	Women	k1gInSc1
Film	film	k1gInSc1
Journalist	Journalist	k1gInSc4
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2016	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Ron	ron	k1gInSc1
Clements	Clements	k1gInSc1
<g/>
,	,	kIx,
Don	Don	k1gMnSc1
Hall	Hall	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Musker	Musker	k1gMnSc1
a	a	k8xC
Chris	Chris	k1gFnSc1
Williams	Williamsa	k1gFnPc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
dabing	dabing	k1gInSc1
–	–	k?
žena	žena	k1gFnSc1
</s>
<s>
Auli	Auli	k1gNnSc1
<g/>
'	'	kIx"
<g/>
i	i	k9
Cravalho	Craval	k1gMnSc2
</s>
<s>
vítězství	vítězství	k1gNnSc1
</s>
<s>
Annie	Annie	k1gFnSc1
Awards	Awardsa	k1gFnPc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInPc1d3
animované	animovaný	k2eAgInPc1d1
efekty	efekt	k1gInPc1
v	v	k7c6
animované	animovaný	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
</s>
<s>
Coony	Cooen	k2eAgFnPc1d1
Ian	Ian	k1gFnPc1
J.	J.	kA
Coony	Cooen	k2eAgFnPc1d1
<g/>
,	,	kIx,
John	John	k1gMnSc1
M.	M.	kA
Kosnik	Kosnik	k1gMnSc1
<g/>
,	,	kIx,
Blair	Blair	k1gMnSc1
Pierpont	Pierpont	k1gMnSc1
<g/>
,	,	kIx,
Erin	Erin	k1gInSc1
V.	V.	kA
Ramos	Ramos	k1gInSc1
a	a	k8xC
Marlon	Marlon	k1gInSc1
West	Westa	k1gFnPc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
návrh	návrh	k1gInSc4
postavy	postava	k1gFnSc2
v	v	k7c6
animované	animovaný	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
</s>
<s>
Kim	Kim	k?
Jin	Jin	k1gMnSc1
Kim	Kim	k1gMnSc1
a	a	k8xC
Bill	Bill	k1gMnSc1
Schwab	Schwab	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
příběh	příběh	k1gInSc4
v	v	k7c6
animované	animovaný	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
</s>
<s>
Lemay	Lema	k2eAgInPc1d1
Normand	Normand	k?
Lemay	Lemay	k1gInPc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
dabing	dabing	k1gInSc4
v	v	k7c6
animované	animovaný	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
</s>
<s>
Cravalho	Cravalze	k6eAd1
Auli	Auli	k1gNnSc1
<g/>
'	'	kIx"
<g/>
i	i	k9
Cravalho	Craval	k1gMnSc2
</s>
<s>
vítězství	vítězství	k1gNnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
střih	střih	k1gInSc4
v	v	k7c6
animované	animovaný	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
</s>
<s>
Draheim	Draheim	k1gMnSc1
Jeff	Jeff	k1gMnSc1
Draheim	Draheim	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Austin	Austin	k2eAgInSc1d1
Film	film	k1gInSc1
Critics	Criticsa	k1gFnPc2
Association	Association	k1gInSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2016	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Billboard	billboard	k1gInSc1
Music	Musice	k1gInPc2
Awards	Awardsa	k1gFnPc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
soundtrack	soundtrack	k1gInSc4
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Black	Black	k6eAd1
Reel	Reel	k1gInSc1
Awards	Awardsa	k1gFnPc2
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
dabing	dabing	k1gInSc4
</s>
<s>
Dwayne	Dwaynout	k5eAaPmIp3nS
Johnson	Johnson	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1
cena	cena	k1gFnSc1
Britské	britský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Clements	Clements	k6eAd1
Ron	ron	k1gInSc1
Clements	Clementsa	k1gFnPc2
a	a	k8xC
John	John	k1gMnSc1
Musker	Musker	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Chicago	Chicago	k1gNnSc1
Film	film	k1gInSc1
Critics	Critics	k1gInSc1
Association	Association	k1gInSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2016	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Cinema	Cinema	k1gFnSc1
Audio	audio	k2eAgFnSc2d1
Society	societa	k1gFnSc2
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
zvuk	zvuk	k1gInSc4
ve	v	k7c6
filmu	film	k1gInSc6
–	–	k?
animovaném	animovaný	k2eAgNnSc6d1
filmu	film	k1gInSc2
</s>
<s>
Boucher	Bouchra	k1gFnPc2
David	David	k1gMnSc1
Boucher	Bouchra	k1gFnPc2
<g/>
,	,	kIx,
Scott	Scott	k2eAgInSc1d1
Curtis	Curtis	k1gInSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
E.	E.	kA
Fluhr	Fluhr	k1gMnSc1
<g/>
,	,	kIx,
Gabriel	Gabriel	k1gMnSc1
Guy	Guy	k1gMnSc1
a	a	k8xC
Paul	Paul	k1gMnSc1
McGrath	McGrath	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
Saturn	Saturn	k1gInSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
Grammy	Gramma	k1gFnSc2
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2018	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
soundtrack	soundtrack	k1gInSc4
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
píseň	píseň	k1gFnSc1
</s>
<s>
„	„	k?
<g/>
How	How	k1gFnSc1
Far	fara	k1gFnPc2
I	I	kA
<g/>
'	'	kIx"
<g/>
ll	ll	k?
Go	Go	k1gFnSc1
<g/>
“	“	k?
–	–	k?
Lin-Manuel	Lin-Manuel	k1gInSc1
Miranda	Miranda	k1gFnSc1
</s>
<s>
vítězství	vítězství	k1gNnSc1
</s>
<s>
Critics	Critics	k1gInSc1
<g/>
'	'	kIx"
Choice	Choice	k1gFnSc1
Movie	Movie	k1gFnSc2
Awards	Awardsa	k1gFnPc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2016	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
píseň	píseň	k1gFnSc1
</s>
<s>
„	„	k?
<g/>
How	How	k1gFnSc1
Far	fara	k1gFnPc2
I	I	kA
<g/>
'	'	kIx"
<g/>
ll	ll	k?
Go	Go	k1gFnSc1
<g/>
“	“	k?
–	–	k?
Lin-Manuel	Lin-Manuel	k1gInSc1
Miranda	Miranda	k1gFnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Dallas	Dallas	k1gInSc1
<g/>
–	–	k?
<g/>
Fort	Fort	k?
Worth	Worth	k1gInSc1
Film	film	k1gInSc1
Critics	Critics	k1gInSc1
Association	Association	k1gInSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2016	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Denver	Denver	k1gInSc1
Film	film	k1gInSc1
Critics	Critics	k1gInSc4
Society	societa	k1gFnSc2
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Empire	empir	k1gInSc5
Awards	Awards	k1gInSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
soundtrack	soundtrack	k1gInSc4
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Florida	Florida	k1gFnSc1
Film	film	k1gInSc1
Critics	Critics	k1gInSc1
Circle	Circle	k1gInSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2016	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Georgia	Georgia	k1gFnSc1
Film	film	k1gInSc1
Critics	Critics	k1gInSc1
Association	Association	k1gInSc1
</s>
<s>
TBA	TBA	kA
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Golden	Goldna	k1gFnPc2
Tomato	Tomat	k2eAgNnSc4d1
Awards	Awardsa	k1gFnPc2
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgNnSc1d3
uvedení	uvedení	k1gNnSc1
do	do	k7c2
kin	kino	k1gNnPc2
2016	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
roku	rok	k1gInSc2
2016	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Hollywood	Hollywood	k1gInSc1
Music	Music	k1gMnSc1
in	in	k?
Media	medium	k1gNnSc2
Awards	Awardsa	k1gFnPc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2016	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgNnSc1d3
původní	původní	k2eAgNnSc1d1
skóré	skórý	k2eAgNnSc1d1
-	-	kIx~
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Mancina	Mancina	k1gMnSc1
Mark	Mark	k1gMnSc1
Mancina	Mancina	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
píseň	píseň	k1gFnSc1
–	–	k?
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
„	„	k?
<g/>
We	We	k1gMnSc1
Know	Know	k1gMnSc1
the	the	k?
Way	Way	k1gMnSc1
<g/>
“	“	k?
–	–	k?
Foa	Foa	k1gFnSc2
<g/>
'	'	kIx"
<g/>
i	i	k8xC
Opetaia	Opetaia	k1gFnSc1
Foa	Foa	k1gFnSc2
<g/>
'	'	kIx"
<g/>
i	i	k9
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
Mancina	Mancina	k1gMnSc1
a	a	k8xC
Lin-Manuel	Lin-Manuel	k1gMnSc1
Miranda	Mirando	k1gNnSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Houston	Houston	k1gInSc1
Film	film	k1gInSc1
Critics	Critics	k1gInSc4
Society	societa	k1gFnSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
píseň	píseň	k1gFnSc1
</s>
<s>
„	„	k?
<g/>
How	How	k1gFnSc1
Far	fara	k1gFnPc2
I	I	kA
<g/>
'	'	kIx"
<g/>
ll	ll	k?
Go	Go	k1gFnSc1
<g/>
“	“	k?
–	–	k?
Lin-Manuel	Lin-Manuel	k1gInSc1
Miranda	Miranda	k1gFnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Kids	Kids	k1gInSc1
<g/>
'	'	kIx"
Choice	Choice	k1gFnSc1
Awards	Awardsa	k1gFnPc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
hlas	hlas	k1gInSc4
</s>
<s>
Johnson	Johnson	k1gMnSc1
Dwayne	Dwayn	k1gInSc5
Johnson	Johnson	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgMnPc1d3
nepřátelé	nepřítel	k1gMnPc1
</s>
<s>
Auli	Auli	k1gNnSc1
<g/>
'	'	kIx"
<g/>
i	i	k8xC
Cravalho	Craval	k1gMnSc4
a	a	k8xC
Dwayne	Dwayn	k1gInSc5
Johnson	Johnson	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
soundtrack	soundtrack	k1gInSc4
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
NAACP	NAACP	kA
Image	image	k1gFnSc1
Awards	Awards	k1gInSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
dabing	dabing	k1gInSc4
</s>
<s>
Johnson	Johnson	k1gMnSc1
Dwayne	Dwayn	k1gInSc5
Johnson	Johnson	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
North	Northa	k1gFnPc2
Carolina	Carolina	k1gFnSc1
Film	film	k1gInSc1
Critics	Critics	k1gInSc1
Association	Association	k1gInSc4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Online	Onlinout	k5eAaPmIp3nS
Film	film	k1gInSc1
Critics	Criticsa	k1gFnPc2
Society	societa	k1gFnSc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Oscar	Oscar	k1gInSc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Clements	Clements	k6eAd1
Ron	ron	k1gInSc1
Clements	Clementsa	k1gFnPc2
<g/>
,	,	kIx,
John	John	k1gMnSc1
Musker	Musker	k1gMnSc1
aOsnat	aOsnat	k5eAaImF,k5eAaPmF
Shurer	Shurer	k1gInSc4
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
píseň	píseň	k1gFnSc1
</s>
<s>
„	„	k?
<g/>
How	How	k1gFnSc1
Far	fara	k1gFnPc2
I	I	kA
<g/>
'	'	kIx"
<g/>
ll	ll	k?
Go	Go	k1gFnSc1
<g/>
“	“	k?
<g/>
–	–	k?
Lin-Manuel	Lin-Manuel	k1gInSc1
Miranda	Miranda	k1gFnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Producers	Producers	k6eAd1
Guild	Guild	k1gMnSc1
of	of	k?
America	America	k1gMnSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Osnat	Osnat	k2eAgInSc1d1
Shurer	Shurer	k1gInSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
San	San	k?
Diego	Diego	k6eAd1
Film	film	k1gInSc1
Critics	Criticsa	k1gFnPc2
Society	societa	k1gFnSc2
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2016	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
San	San	k?
Francisco	Francisco	k6eAd1
Film	film	k1gInSc1
Critics	Criticsa	k1gFnPc2
Circle	Circle	k1gFnSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2016	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Satellite	Satellit	k1gInSc5
Awards	Awardsa	k1gFnPc2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
animovaný	animovaný	k2eAgMnSc1d1
nebo	nebo	k8xC
namixovaný	namixovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
St.	st.	kA
Louis	Louis	k1gMnSc1
Gateway	Gatewaa	k1gFnSc2
Film	film	k1gInSc1
Critics	Critics	k1gInSc1
Association	Association	k1gInSc4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2016	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
soundtrack	soundtrack	k1gInSc4
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
píseň	píseň	k1gFnSc1
</s>
<s>
„	„	k?
<g/>
How	How	k1gFnSc1
Far	fara	k1gFnPc2
I	I	kA
<g/>
'	'	kIx"
<g/>
ll	ll	k?
Go	Go	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
„	„	k?
<g/>
You	You	k1gFnSc1
<g/>
'	'	kIx"
<g/>
re	re	k9
Welcome	Welcom	k1gInSc5
<g/>
“	“	k?
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Teen	Teen	k1gInSc1
Choice	Choice	k1gFnSc2
Awards	Awardsa	k1gFnPc2
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInPc1d3
fantasy	fantas	k1gInPc1
film	film	k1gInSc4
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
herec	herec	k1gMnSc1
ve	v	k7c4
fantasy	fantas	k1gInPc4
filmu	film	k1gInSc2
</s>
<s>
Dwayne	Dwaynout	k5eAaPmIp3nS
Johnson	Johnson	k1gMnSc1
</s>
<s>
vítězství	vítězství	k1gNnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
filmová	filmový	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
</s>
<s>
Auli	Auli	k1gNnSc1
<g/>
'	'	kIx"
<g/>
i	i	k9
Cravalho	Craval	k1gMnSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Objev	objev	k1gInSc1
roku	rok	k1gInSc2
</s>
<s>
vítězství	vítězství	k1gNnSc1
</s>
<s>
Visual	Visuat	k5eAaPmAgInS,k5eAaBmAgInS,k5eAaImAgInS
Effects	Effects	k1gInSc1
Society	societa	k1gFnSc2
Awards	Awardsa	k1gFnPc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInPc1d3
vizuální	vizuální	k2eAgInPc1d1
efekty	efekt	k1gInPc1
v	v	k7c6
animovaném	animovaný	k2eAgInSc6d1
filmu	film	k1gInSc6
</s>
<s>
Driskill	Driskill	k1gMnSc1
Hank	Hank	k1gMnSc1
Driskill	Driskill	k1gMnSc1
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
Gooding	Gooding	k1gInSc1
<g/>
,	,	kIx,
Nicole	Nicola	k1gFnSc6
P.	P.	kA
Hearon	Hearon	k1gInSc1
a	a	k8xC
Kyle	Kyle	k1gInSc1
Odermatt	Odermatta	k1gFnPc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgNnSc1d3
animované	animovaný	k2eAgNnSc1d1
vystoupení	vystoupení	k1gNnSc1
v	v	k7c6
animovaném	animovaný	k2eAgInSc6d1
filmu	film	k1gInSc6
</s>
<s>
Maui	Maui	k6eAd1
–	–	k?
Kablan	Kablan	k1gMnSc1
Mack	Mack	k1gMnSc1
Kablan	Kablan	k1gMnSc1
<g/>
,	,	kIx,
Nikki	Nikk	k1gMnPc1
Mull	Mull	k1gMnSc1
<g/>
,	,	kIx,
Matthew	Matthew	k1gMnSc1
Schiller	Schiller	k1gMnSc1
a	a	k8xC
Marc	Marc	k1gFnSc1
Thyng	Thynga	k1gFnPc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlépe	dobře	k6eAd3
vytvořené	vytvořený	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
v	v	k7c6
animovaném	animovaný	k2eAgInSc6d1
filmu	film	k1gInSc6
</s>
<s>
Motonui	Motonui	k6eAd1
Island	Island	k1gInSc1
–	–	k?
Dressel	Dressel	k1gInSc1
Rob	roba	k1gFnPc2
Dressel	Dressela	k1gFnPc2
<g/>
,	,	kIx,
Andy	Anda	k1gFnPc1
Harkness	Harkness	k1gInSc1
<g/>
,	,	kIx,
Brien	Brien	k2eAgMnSc1d1
Hindman	Hindman	k1gMnSc1
a	a	k8xC
Larry	Larra	k1gMnSc2
Wu	Wu	k1gMnSc2
</s>
<s>
vítězství	vítězství	k1gNnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgFnPc1d3
efektové	efektový	k2eAgFnPc1d1
simulace	simulace	k1gFnPc1
v	v	k7c6
animovaném	animovaný	k2eAgInSc6d1
filmu	film	k1gInSc6
</s>
<s>
Bryant	Bryant	k1gInSc1
Marc	Marc	k1gFnSc1
Henry	Henry	k1gMnSc1
Bryant	Bryant	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
Hutchins	Hutchins	k1gInSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
M.	M.	kA
Kosnik	Kosnik	k1gMnSc1
a	a	k8xC
Dale	Dale	k1gFnSc1
Mayeda	Mayed	k1gMnSc2
</s>
<s>
vítězství	vítězství	k1gNnSc1
</s>
<s>
Washington	Washington	k1gInSc1
D.C.	D.C.	k1gMnSc2
Area	area	k1gFnSc1
Film	film	k1gInSc1
Critics	Critics	k1gInSc1
Association	Association	k1gInSc4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2016	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
dabing	dabing	k1gInSc4
</s>
<s>
Cravalho	Cravalze	k6eAd1
Auli	Auli	k1gNnSc1
<g/>
'	'	kIx"
<g/>
i	i	k9
Cravalho	Craval	k1gMnSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Village	Villag	k1gFnPc1
Voice	Voiec	k1gInSc2
Film	film	k1gInSc1
Poll	Poll	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Women	Women	k2eAgInSc1d1
Film	film	k1gInSc1
Critics	Criticsa	k1gFnPc2
Circle	Circle	k1gFnSc2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2016	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
vítězství	vítězství	k1gNnSc1
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1
glóbus	glóbus	k1gInSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2017	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Odvážná	odvážný	k2eAgFnSc1d1
Vaiana	Vaiana	k1gFnSc1
<g/>
:	:	kIx,
Legenda	legenda	k1gFnSc1
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
píseň	píseň	k1gFnSc1
</s>
<s>
„	„	k?
<g/>
How	How	k1gFnSc1
Far	fara	k1gFnPc2
I	I	kA
<g/>
'	'	kIx"
<g/>
ll	ll	k?
Go	Go	k1gFnSc1
<g/>
“	“	k?
–	–	k?
Lin-Manuel	Lin-Manuel	k1gInSc1
Miranda	Miranda	k1gFnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Moana	Moan	k1gInSc2
(	(	kIx(
<g/>
2016	#num#	k4
film	film	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Will	Will	k1gInSc1
‘	‘	k?
<g/>
Moana	Moana	k1gFnSc1
<g/>
’	’	k?
Slay	Slay	k1gInPc1
‘	‘	k?
<g/>
Fantastic	Fantastice	k1gFnPc2
Beasts	Beasts	k1gInSc1
<g/>
’	’	k?
Over	Over	k1gInSc4
The	The	k1gMnPc2
Thanksgiving	Thanksgiving	k1gInSc1
Stretch	Stretcha	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Thanksgiving	Thanksgiving	k1gInSc1
Box	box	k1gInSc1
Office	Office	kA
<g/>
:	:	kIx,
'	'	kIx"
<g/>
Moana	Moana	k1gFnSc1
<g/>
'	'	kIx"
Tops	Tops	k1gInSc1
With	With	k1gInSc1
$	$	kIx~
<g/>
10	#num#	k4
<g/>
M	M	kA
<g/>
,	,	kIx,
'	'	kIx"
<g/>
Fantastic	Fantastice	k1gFnPc2
Beasts	Beasts	k1gInSc1
<g/>
'	'	kIx"
Scores	Scores	k1gInSc1
Big	Big	k1gMnSc2
China	China	k1gFnSc1
Debut	debut	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
‘	‘	k?
<g/>
Moana	Moana	k1gFnSc1
<g/>
’	’	k?
Rings	Rings	k1gInSc1
Up	Up	k1gFnSc1
$	$	kIx~
<g/>
21.8	21.8	k4
<g/>
M	M	kA
On	on	k3xPp3gMnSc1
Black	Black	k1gMnSc1
Friday	Fridaa	k1gFnSc2
<g/>
;	;	kIx,
Hopes	Hopes	k1gInSc1
To	to	k9
Break	break	k1gInSc1
‘	‘	k?
<g/>
Toy	Toy	k1gFnSc1
Story	story	k1gFnSc1
2	#num#	k4
<g/>
’	’	k?
As	as	k9
Disney	Disne	k2eAgInPc4d1
<g/>
’	’	k?
<g/>
s	s	k7c7
2	#num#	k4
<g/>
nd	nd	k?
Best	Best	k2eAgInSc4d1
Thanksgiving	Thanksgiving	k1gInSc4
Debut	debut	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2016133041	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2016133041	#num#	k4
</s>
