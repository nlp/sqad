<s>
Abraham	Abraham	k1gMnSc1	Abraham
Zapruder	Zapruder	k1gMnSc1	Zapruder
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1905	[number]	k4	1905
Kovel	Kovela	k1gFnPc2	Kovela
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
−	−	k?	−
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1970	[number]	k4	1970
Dallas	Dallas	k1gInSc1	Dallas
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
výrobce	výrobce	k1gMnSc1	výrobce
dámského	dámský	k2eAgNnSc2d1	dámské
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
filmoval	filmovat	k5eAaImAgMnS	filmovat
svou	svůj	k3xOyFgFnSc7	svůj
amatérskou	amatérský	k2eAgFnSc7d1	amatérská
kamerou	kamera	k1gFnSc7	kamera
průjezd	průjezd	k1gInSc1	průjezd
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kolony	kolona	k1gFnSc2	kolona
Johna	John	k1gMnSc2	John
F.	F.	kA	F.
Kennedyho	Kennedy	k1gMnSc2	Kennedy
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Dealey	Dealea	k1gMnSc2	Dealea
Plaza	plaz	k1gMnSc2	plaz
v	v	k7c6	v
Dallasu	Dallas	k1gInSc6	Dallas
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
natáčení	natáčení	k1gNnSc2	natáčení
nečekaně	nečekaně	k6eAd1	nečekaně
zachytil	zachytit	k5eAaPmAgMnS	zachytit
i	i	k9	i
atentát	atentát	k1gInSc4	atentát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
následkům	následek	k1gInPc3	následek
prezident	prezident	k1gMnSc1	prezident
Kennedy	Kenneda	k1gMnSc2	Kenneda
takřka	takřka	k6eAd1	takřka
okamžitě	okamžitě	k6eAd1	okamžitě
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
Zapruderův	Zapruderův	k2eAgInSc1d1	Zapruderův
film	film	k1gInSc1	film
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
a	a	k8xC	a
nejkompletnějším	kompletní	k2eAgInSc7d3	nejkompletnější
záznamem	záznam	k1gInSc7	záznam
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Abraham	Abraham	k1gMnSc1	Abraham
Zapruder	Zapruder	k1gMnSc1	Zapruder
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
židovské	židovský	k2eAgFnSc6d1	židovská
rodině	rodina	k1gFnSc6	rodina
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Kovelu	Kovel	k1gInSc2	Kovel
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
spadala	spadat	k5eAaImAgFnS	spadat
pod	pod	k7c4	pod
Ruské	ruský	k2eAgNnSc4d1	ruské
impérium	impérium	k1gNnSc4	impérium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ruské	ruský	k2eAgFnSc2d1	ruská
školy	škola	k1gFnSc2	škola
chodil	chodit	k5eAaImAgMnS	chodit
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
málo	málo	k4c4	málo
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
uprchnout	uprchnout	k5eAaPmF	uprchnout
před	před	k7c7	před
ruskou	ruský	k2eAgFnSc7d1	ruská
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
našel	najít	k5eAaPmAgMnS	najít
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
oděvní	oděvní	k2eAgFnSc6d1	oděvní
továrně	továrna	k1gFnSc6	továrna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
a	a	k8xC	a
narodily	narodit	k5eAaPmAgFnP	narodit
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
se	se	k3xPyFc4	se
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Dallasu	Dallas	k1gInSc2	Dallas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
práci	práce	k1gFnSc4	práce
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
se	s	k7c7	s
sportovním	sportovní	k2eAgNnSc7d1	sportovní
oblečením	oblečení	k1gNnSc7	oblečení
Nardis	Nardis	k1gFnSc2	Nardis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
společnosti	společnost	k1gFnSc2	společnost
Jennifer	Jennifra	k1gFnPc2	Jennifra
Juniors	Juniors	k1gInSc1	Juniors
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
produkovala	produkovat	k5eAaImAgFnS	produkovat
dvě	dva	k4xCgFnPc4	dva
značky	značka	k1gFnPc4	značka
oblečení	oblečení	k1gNnSc2	oblečení
Chalet	Chalet	k1gInSc1	Chalet
a	a	k8xC	a
Jennifer	Jennifer	k1gInSc1	Jennifer
Juniors	Juniorsa	k1gFnPc2	Juniorsa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kancelář	kancelář	k1gFnSc1	kancelář
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Dal-Tex	Dal-Tex	k1gInSc1	Dal-Tex
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přímo	přímo	k6eAd1	přímo
naproti	naproti	k7c3	naproti
Texaskému	texaský	k2eAgInSc3d1	texaský
knižnímu	knižní	k2eAgInSc3d1	knižní
velkoskladu	velkosklad	k1gInSc3	velkosklad
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zapruderův	Zapruderův	k2eAgInSc4d1	Zapruderův
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Abraham	Abraham	k1gMnSc1	Abraham
Zapruder	Zapruder	k1gMnSc1	Zapruder
neměl	mít	k5eNaImAgMnS	mít
původně	původně	k6eAd1	původně
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
průjezd	průjezd	k1gInSc1	průjezd
kolony	kolona	k1gFnSc2	kolona
filmovat	filmovat	k5eAaImF	filmovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
si	se	k3xPyFc3	se
ovšem	ovšem	k9	ovšem
později	pozdě	k6eAd2	pozdě
rozmyslel	rozmyslet	k5eAaPmAgMnS	rozmyslet
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
kanceláře	kancelář	k1gFnSc2	kancelář
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
kameru	kamera	k1gFnSc4	kamera
Bell	bell	k1gInSc1	bell
&	&	k?	&
Howell	Howell	k1gInSc1	Howell
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
kamerou	kamera	k1gFnSc7	kamera
nečekaně	nečekaně	k6eAd1	nečekaně
zachytil	zachytit	k5eAaPmAgMnS	zachytit
i	i	k9	i
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Kennedyho	Kennedy	k1gMnSc4	Kennedy
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
jako	jako	k8xS	jako
Zapruderův	Zapruderův	k2eAgMnSc1d1	Zapruderův
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejpodrobněji	podrobně	k6eAd3	podrobně
prostudovaných	prostudovaný	k2eAgInPc2d1	prostudovaný
filmů	film	k1gInPc2	film
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jej	on	k3xPp3gMnSc4	on
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
šetření	šetření	k1gNnSc6	šetření
využila	využít	k5eAaPmAgFnS	využít
Warrenova	Warrenův	k2eAgFnSc1d1	Warrenova
komise	komise	k1gFnSc1	komise
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
opěrných	opěrný	k2eAgInPc2d1	opěrný
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Zapruder	Zapruder	k1gMnSc1	Zapruder
natočil	natočit	k5eAaBmAgMnS	natočit
atentát	atentát	k1gInSc4	atentát
kamerou	kamera	k1gFnSc7	kamera
Model	model	k1gInSc1	model
414	[number]	k4	414
PD	PD	kA	PD
8	[number]	k4	8
mm	mm	kA	mm
Bell	bell	k1gInSc1	bell
&	&	k?	&
Howell	Howell	k1gInSc1	Howell
Zoomatic	Zoomatice	k1gFnPc2	Zoomatice
Director	Director	k1gInSc1	Director
Series	Series	k1gMnSc1	Series
movie	movie	k1gFnSc1	movie
camera	camera	k1gFnSc1	camera
zakoupenou	zakoupený	k2eAgFnSc4d1	zakoupená
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
barevného	barevný	k2eAgInSc2d1	barevný
filmu	film	k1gInSc2	film
Kodak	Kodak	kA	Kodak
Kodachrome	Kodachrom	k1gInSc5	Kodachrom
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sekvence	sekvence	k1gFnSc1	sekvence
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgFnSc1d1	sestávající
ze	z	k7c2	z
486	[number]	k4	486
snímků	snímek	k1gInPc2	snímek
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Zapruderův	Zapruderův	k2eAgInSc4d1	Zapruderův
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Abraham	Abraham	k1gMnSc1	Abraham
Zapruder	Zapruder	k1gMnSc1	Zapruder
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
Johna	John	k1gMnSc4	John
Fitzgeralda	Fitzgerald	k1gMnSc2	Fitzgerald
Kennedyho	Kennedy	k1gMnSc2	Kennedy
Warrenova	Warrenův	k2eAgFnSc1d1	Warrenova
komise	komise	k1gFnSc1	komise
</s>
