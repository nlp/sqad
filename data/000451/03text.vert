<s>
Emil	Emil	k1gMnSc1	Emil
Zátopek	Zátopka	k1gFnPc2	Zátopka
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1922	[number]	k4	1922
Kopřivnice	Kopřivnice	k1gFnSc2	Kopřivnice
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2000	[number]	k4	2000
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
čtyřnásobný	čtyřnásobný	k2eAgMnSc1d1	čtyřnásobný
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
ve	v	k7c6	v
vytrvalostním	vytrvalostní	k2eAgInSc6d1	vytrvalostní
běhu	běh	k1gInSc6	běh
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
atletky	atletka	k1gFnSc2	atletka
Dany	Dana	k1gFnSc2	Dana
Zátopkové	Zátopková	k1gFnSc2	Zátopková
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
uběhl	uběhnout	k5eAaPmAgInS	uběhnout
trať	trať	k1gFnSc4	trať
10	[number]	k4	10
km	km	kA	km
pod	pod	k7c4	pod
29	[number]	k4	29
minut	minuta	k1gFnPc2	minuta
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
:	:	kIx,	:
<g/>
54,2	[number]	k4	54,2
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1954	[number]	k4	1954
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
)	)	kIx)	)
a	a	k8xC	a
trať	trať	k1gFnSc1	trať
20	[number]	k4	20
km	km	kA	km
pod	pod	k7c4	pod
jednu	jeden	k4xCgFnSc4	jeden
hodinu	hodina	k1gFnSc4	hodina
(	(	kIx(	(
<g/>
59	[number]	k4	59
<g/>
:	:	kIx,	:
<g/>
51,8	[number]	k4	51,8
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1951	[number]	k4	1951
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
třináct	třináct	k4xCc4	třináct
světových	světový	k2eAgInPc2d1	světový
rekordů	rekord	k1gInPc2	rekord
na	na	k7c6	na
kilometrových	kilometrový	k2eAgFnPc6d1	kilometrová
a	a	k8xC	a
pět	pět	k4xCc4	pět
na	na	k7c6	na
mílových	mílový	k2eAgFnPc6d1	Mílová
tratích	trať	k1gFnPc6	trať
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
atletů	atlet	k1gMnPc2	atlet
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
ale	ale	k8xC	ale
proslul	proslout	k5eAaPmAgMnS	proslout
během	během	k7c2	během
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
1952	[number]	k4	1952
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
běh	běh	k1gInSc4	běh
na	na	k7c4	na
5	[number]	k4	5
km	km	kA	km
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
6,72	[number]	k4	6,72
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
10	[number]	k4	10
km	km	kA	km
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
:	:	kIx,	:
<g/>
17,0	[number]	k4	17,0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
maratón	maratón	k1gInSc4	maratón
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
3,2	[number]	k4	3,2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tehdy	tehdy	k6eAd1	tehdy
běžel	běžet	k5eAaImAgMnS	běžet
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
disciplin	disciplina	k1gFnPc2	disciplina
tehdy	tehdy	k6eAd1	tehdy
zároveň	zároveň	k6eAd1	zároveň
ustavil	ustavit	k5eAaPmAgMnS	ustavit
nový	nový	k2eAgInSc4d1	nový
olympijský	olympijský	k2eAgInSc4d1	olympijský
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
"	"	kIx"	"
<g/>
trojboj	trojboj	k1gInSc1	trojboj
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
žádnému	žádný	k3yNgMnSc3	žádný
vytrvalci	vytrvalec	k1gMnSc3	vytrvalec
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zopakovat	zopakovat	k5eAaPmF	zopakovat
a	a	k8xC	a
atletičtí	atletický	k2eAgMnPc1d1	atletický
experti	expert	k1gMnPc1	expert
pochybují	pochybovat	k5eAaImIp3nP	pochybovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
někomu	někdo	k3yInSc3	někdo
kdy	kdy	k6eAd1	kdy
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
znám	znát	k5eAaImIp1nS	znát
svým	svůj	k3xOyFgInSc7	svůj
upracovaným	upracovaný	k2eAgInSc7d1	upracovaný
stylem	styl	k1gInSc7	styl
běhu	běh	k1gInSc2	běh
<g/>
,	,	kIx,	,
doprovázeným	doprovázený	k2eAgInSc7d1	doprovázený
křečovitými	křečovitý	k2eAgInPc7d1	křečovitý
grimasami	grimasa	k1gFnPc7	grimasa
<g/>
,	,	kIx,	,
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
byl	být	k5eAaImAgInS	být
přezdíván	přezdívat	k5eAaImNgInS	přezdívat
česká	český	k2eAgFnSc1d1	Česká
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
a	a	k8xC	a
Satupekka	Satupekka	k1gFnSc1	Satupekka
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
do	do	k7c2	do
chudé	chudý	k2eAgFnSc2d1	chudá
rodiny	rodina	k1gFnSc2	rodina
jako	jako	k8xS	jako
sedmé	sedmý	k4xOgNnSc4	sedmý
dítě	dítě	k1gNnSc4	dítě
z	z	k7c2	z
osmi	osm	k4xCc2	osm
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
šestnácti	šestnáct	k4xCc6	šestnáct
letech	let	k1gInPc6	let
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
Baťově	Baťův	k2eAgFnSc6d1	Baťova
obuvnické	obuvnický	k2eAgFnSc6d1	obuvnická
továrně	továrna	k1gFnSc6	továrna
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
ukázal	ukázat	k5eAaPmAgMnS	ukázat
závodní	závodní	k2eAgMnSc1d1	závodní
trenér	trenér	k1gMnSc1	trenér
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
mimochodem	mimochodem	k9	mimochodem
velmi	velmi	k6eAd1	velmi
přísný	přísný	k2eAgInSc1d1	přísný
<g/>
,	,	kIx,	,
na	na	k7c4	na
čtyři	čtyři	k4xCgMnPc4	čtyři
chlapce	chlapec	k1gMnPc4	chlapec
včetně	včetně	k7c2	včetně
mě	já	k3xPp1nSc2	já
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
půjdeme	jít	k5eAaImIp1nP	jít
na	na	k7c4	na
běžecké	běžecký	k2eAgInPc4d1	běžecký
závody	závod	k1gInPc4	závod
<g/>
.	.	kIx.	.
</s>
<s>
Protestoval	protestovat	k5eAaBmAgMnS	protestovat
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
moc	moc	k6eAd1	moc
slabý	slabý	k2eAgMnSc1d1	slabý
a	a	k8xC	a
nemám	mít	k5eNaImIp1nS	mít
na	na	k7c4	na
běhání	běhání	k1gNnSc4	běhání
kondičku	kondička	k1gFnSc4	kondička
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
trenér	trenér	k1gMnSc1	trenér
mě	já	k3xPp1nSc4	já
poslal	poslat	k5eAaPmAgMnS	poslat
na	na	k7c4	na
prohlídku	prohlídka	k1gFnSc4	prohlídka
a	a	k8xC	a
doktor	doktor	k1gMnSc1	doktor
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
naprosto	naprosto	k6eAd1	naprosto
zdravý	zdravý	k2eAgMnSc1d1	zdravý
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
jsem	být	k5eAaImIp1nS	být
musel	muset	k5eAaImAgMnS	muset
závodit	závodit	k5eAaImF	závodit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
rozběhl	rozběhnout	k5eAaPmAgMnS	rozběhnout
<g/>
,	,	kIx,	,
cítil	cítit	k5eAaImAgMnS	cítit
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
chci	chtít	k5eAaImIp1nS	chtít
vyhrát	vyhrát	k5eAaPmF	vyhrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skončil	skončit	k5eAaPmAgMnS	skončit
jsem	být	k5eAaImIp1nS	být
až	až	k9	až
druhý	druhý	k4xOgMnSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
to	ten	k3xDgNnSc1	ten
celé	celý	k2eAgNnSc1d1	celé
začalo	začít	k5eAaPmAgNnS	začít
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
vzpomínal	vzpomínat	k5eAaImAgMnS	vzpomínat
později	pozdě	k6eAd2	pozdě
Zátopek	Zátopek	k1gInSc4	Zátopek
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
druhý	druhý	k4xOgMnSc1	druhý
při	při	k7c6	při
běhu	běh	k1gInSc6	běh
Zlínem	Zlín	k1gInSc7	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
toho	ten	k3xDgInSc2	ten
okamžiku	okamžik	k1gInSc2	okamžik
se	se	k3xPyFc4	se
o	o	k7c4	o
běh	běh	k1gInSc4	běh
začal	začít	k5eAaPmAgMnS	začít
vážně	vážně	k6eAd1	vážně
zajímat	zajímat	k5eAaImF	zajímat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
začal	začít	k5eAaPmAgInS	začít
trénovat	trénovat	k5eAaImF	trénovat
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
tehdy	tehdy	k6eAd1	tehdy
elitních	elitní	k2eAgMnPc2d1	elitní
českých	český	k2eAgMnPc2d1	český
běžců	běžec	k1gMnPc2	běžec
<g/>
,	,	kIx,	,
jako	jako	k9	jako
byli	být	k5eAaImAgMnP	být
Tomáš	Tomáš	k1gMnSc1	Tomáš
Šalé	Šalá	k1gFnSc2	Šalá
či	či	k8xC	či
Jan	Jan	k1gMnSc1	Jan
Haluza	Haluza	k1gMnSc1	Haluza
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Jan	Jan	k1gMnSc1	Jan
Haluza	Haluza	k1gMnSc1	Haluza
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
jeho	jeho	k3xOp3gFnSc2	jeho
prvním	první	k4xOgInSc6	první
(	(	kIx(	(
<g/>
neoficiálním	oficiální	k2eNgInSc6d1	neoficiální
<g/>
)	)	kIx)	)
trenérem	trenér	k1gMnSc7	trenér
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
také	také	k9	také
řadu	řada	k1gFnSc4	řada
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
za	za	k7c4	za
pouhé	pouhý	k2eAgInPc4d1	pouhý
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
pokořil	pokořit	k5eAaPmAgMnS	pokořit
český	český	k2eAgInSc4d1	český
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
3000	[number]	k4	3000
a	a	k8xC	a
5000	[number]	k4	5000
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
do	do	k7c2	do
československého	československý	k2eAgInSc2d1	československý
reprezentačního	reprezentační	k2eAgInSc2d1	reprezentační
týmu	tým	k1gInSc2	tým
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pětikilometrové	pětikilometrový	k2eAgFnSc6d1	pětikilometrová
trati	trať	k1gFnSc6	trať
skončil	skončit	k5eAaPmAgMnS	skončit
pátý	pátý	k4xOgMnSc1	pátý
<g/>
,	,	kIx,	,
když	když	k8xS	když
překonal	překonat	k5eAaPmAgMnS	překonat
výkonem	výkon	k1gInSc7	výkon
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
25,8	[number]	k4	25,8
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
rekord	rekord	k1gInSc4	rekord
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
50,2	[number]	k4	50,2
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
na	na	k7c4	na
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
atletické	atletický	k2eAgNnSc4d1	atletické
závodiště	závodiště	k1gNnSc4	závodiště
na	na	k7c6	na
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
běh	běh	k1gInSc4	běh
na	na	k7c4	na
10	[number]	k4	10
km	km	kA	km
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
tehdy	tehdy	k6eAd1	tehdy
jeho	jeho	k3xOp3gInSc1	jeho
druhý	druhý	k4xOgInSc1	druhý
závod	závod	k1gInSc1	závod
na	na	k7c4	na
takovou	takový	k3xDgFnSc4	takový
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
5	[number]	k4	5
km	km	kA	km
skončil	skončit	k5eAaPmAgInS	skončit
druhý	druhý	k4xOgInSc1	druhý
za	za	k7c7	za
Gastonem	Gaston	k1gInSc7	Gaston
Reiffem	Reiff	k1gInSc7	Reiff
z	z	k7c2	z
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
hned	hned	k6eAd1	hned
dvakrát	dvakrát	k6eAd1	dvakrát
zlomit	zlomit	k5eAaPmF	zlomit
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
10	[number]	k4	10
km	km	kA	km
a	a	k8xC	a
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
dalších	další	k2eAgFnPc6d1	další
sezónách	sezóna	k1gFnPc6	sezóna
si	se	k3xPyFc3	se
třikrát	třikrát	k6eAd1	třikrát
vylepšil	vylepšit	k5eAaPmAgMnS	vylepšit
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Držel	držet	k5eAaImAgInS	držet
také	také	k9	také
světové	světový	k2eAgInPc4d1	světový
rekordy	rekord	k1gInPc4	rekord
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
5	[number]	k4	5
km	km	kA	km
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
20	[number]	k4	20
km	km	kA	km
(	(	kIx(	(
<g/>
dvakrát	dvakrát	k6eAd1	dvakrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
25	[number]	k4	25
km	km	kA	km
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
a	a	k8xC	a
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
30	[number]	k4	30
km	km	kA	km
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
hodinovém	hodinový	k2eAgInSc6d1	hodinový
běhu	běh	k1gInSc6	běh
(	(	kIx(	(
<g/>
dvakrát	dvakrát	k6eAd1	dvakrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
1950	[number]	k4	1950
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
běh	běh	k1gInSc1	běh
na	na	k7c4	na
5	[number]	k4	5
a	a	k8xC	a
10	[number]	k4	10
km	km	kA	km
a	a	k8xC	a
na	na	k7c6	na
dalším	další	k2eAgNnSc6d1	další
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
první	první	k4xOgMnSc1	první
na	na	k7c6	na
desetikilometrové	desetikilometrový	k2eAgFnSc6d1	desetikilometrová
trati	trať	k1gFnSc6	trať
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
legendy	legenda	k1gFnPc4	legenda
ale	ale	k8xC	ale
mezitím	mezitím	k6eAd1	mezitím
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1952	[number]	k4	1952
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
5	[number]	k4	5
km	km	kA	km
<g/>
,	,	kIx,	,
10	[number]	k4	10
km	km	kA	km
a	a	k8xC	a
v	v	k7c6	v
maratonu	maraton	k1gInSc6	maraton
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
třech	tři	k4xCgFnPc6	tři
těchto	tento	k3xDgFnPc6	tento
disciplínách	disciplína	k1gFnPc6	disciplína
navíc	navíc	k6eAd1	navíc
překonal	překonat	k5eAaPmAgMnS	překonat
olympijské	olympijský	k2eAgInPc4d1	olympijský
rekordy	rekord	k1gInPc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Desetikilometrový	desetikilometrový	k2eAgInSc1d1	desetikilometrový
závod	závod	k1gInSc1	závod
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
před	před	k7c7	před
Alainem	Alain	k1gMnSc7	Alain
Mimounem	Mimoun	k1gMnSc7	Mimoun
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
následně	následně	k6eAd1	následně
běh	běh	k1gInSc1	běh
na	na	k7c4	na
5	[number]	k4	5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
po	po	k7c6	po
úporném	úporný	k2eAgInSc6d1	úporný
boji	boj	k1gInSc6	boj
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
kole	kolo	k1gNnSc6	kolo
(	(	kIx(	(
<g/>
57,5	[number]	k4	57,5
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
ze	z	k7c2	z
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc4	třetí
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
získal	získat	k5eAaPmAgMnS	získat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
také	také	k6eAd1	také
maratonu	maraton	k1gInSc2	maraton
<g/>
.	.	kIx.	.
</s>
<s>
Běžel	běžet	k5eAaImAgMnS	běžet
ho	on	k3xPp3gNnSc4	on
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
životě	život	k1gInSc6	život
a	a	k8xC	a
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
strategie	strategie	k1gFnSc1	strategie
pro	pro	k7c4	pro
maraton	maraton	k1gInSc4	maraton
byla	být	k5eAaImAgFnS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
:	:	kIx,	:
držet	držet	k5eAaImF	držet
se	se	k3xPyFc4	se
Jima	Jimum	k1gNnSc2	Jimum
Peterse	Peterse	k1gFnSc2	Peterse
<g/>
,	,	kIx,	,
britského	britský	k2eAgMnSc2d1	britský
držitele	držitel	k1gMnSc2	držitel
světového	světový	k2eAgInSc2d1	světový
rekordu	rekord	k1gInSc2	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úmorných	úmorný	k2eAgNnPc6d1	úmorné
prvních	první	k4xOgNnPc6	první
patnácti	patnáct	k4xCc6	patnáct
kilometrech	kilometr	k1gInPc6	kilometr
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k9	už
Peters	Peters	k1gInSc4	Peters
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
přecenil	přecenit	k5eAaPmAgMnS	přecenit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc4	on
Zátopek	Zátopek	k1gInSc4	Zátopek
zeptal	zeptat	k5eAaPmAgMnS	zeptat
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
si	se	k3xPyFc3	se
zatím	zatím	k6eAd1	zatím
o	o	k7c6	o
závodu	závod	k1gInSc6	závod
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Překvapený	překvapený	k2eAgMnSc1d1	překvapený
Angličan	Angličan	k1gMnSc1	Angličan
řekl	říct	k5eAaPmAgMnS	říct
Zátopkovi	Zátopek	k1gMnSc3	Zátopek
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
ho	on	k3xPp3gNnSc4	on
zmást	zmást	k5eAaPmF	zmást
<g/>
,	,	kIx,	,
že	že	k8xS	že
tempo	tempo	k1gNnSc1	tempo
je	být	k5eAaImIp3nS	být
dost	dost	k6eAd1	dost
pomalé	pomalý	k2eAgNnSc1d1	pomalé
<g/>
.	.	kIx.	.
</s>
<s>
Čech	Čech	k1gMnSc1	Čech
tedy	tedy	k9	tedy
prostě	prostě	k9	prostě
zrychlil	zrychlit	k5eAaPmAgMnS	zrychlit
<g/>
,	,	kIx,	,
Peters	Peters	k1gInSc1	Peters
závod	závod	k1gInSc4	závod
nedokončil	dokončit	k5eNaPmAgInS	dokončit
a	a	k8xC	a
Zátopek	Zátopek	k1gInSc1	Zátopek
nastolil	nastolit	k5eAaPmAgInS	nastolit
nový	nový	k2eAgInSc4d1	nový
olympijský	olympijský	k2eAgInSc4d1	olympijský
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
z	z	k7c2	z
maratonu	maraton	k1gInSc2	maraton
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
obhájit	obhájit	k5eAaPmF	obhájit
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
na	na	k7c6	na
tréninku	trénink	k1gInSc6	trénink
si	se	k3xPyFc3	se
ale	ale	k9	ale
poranil	poranit	k5eAaPmAgMnS	poranit
tříslo	tříslo	k1gNnSc4	tříslo
a	a	k8xC	a
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
na	na	k7c4	na
šest	šest	k4xCc4	šest
týdnů	týden	k1gInPc2	týden
hospitalizován	hospitalizován	k2eAgInSc1d1	hospitalizován
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
nemocnice	nemocnice	k1gFnSc2	nemocnice
už	už	k6eAd1	už
zase	zase	k9	zase
trénoval	trénovat	k5eAaImAgMnS	trénovat
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
formu	forma	k1gFnSc4	forma
už	už	k6eAd1	už
ale	ale	k8xC	ale
zpátky	zpátky	k6eAd1	zpátky
nikdy	nikdy	k6eAd1	nikdy
úplně	úplně	k6eAd1	úplně
nezískal	získat	k5eNaPmAgInS	získat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
šestý	šestý	k4xOgMnSc1	šestý
za	za	k7c7	za
svým	svůj	k3xOyFgMnSc7	svůj
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
rivalem	rival	k1gMnSc7	rival
a	a	k8xC	a
přítelem	přítel	k1gMnSc7	přítel
Alainem	Alain	k1gMnSc7	Alain
Mimounem	Mimoun	k1gMnSc7	Mimoun
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
sportovní	sportovní	k2eAgFnSc4d1	sportovní
kariéru	kariéra	k1gFnSc4	kariéra
ukončil	ukončit	k5eAaPmAgMnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
běžecký	běžecký	k2eAgInSc1d1	běžecký
styl	styl	k1gInSc1	styl
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
a	a	k8xC	a
hodně	hodně	k6eAd1	hodně
se	se	k3xPyFc4	se
lišil	lišit	k5eAaImAgMnS	lišit
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c4	za
účinný	účinný	k2eAgInSc4d1	účinný
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Otáčel	otáčet	k5eAaImAgMnS	otáčet
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
obličej	obličej	k1gInSc1	obličej
měl	mít	k5eAaImAgInS	mít
pokřivený	pokřivený	k2eAgInSc1d1	pokřivený
námahou	námaha	k1gFnSc7	námaha
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
vděčí	vděčit	k5eAaImIp3nS	vděčit
za	za	k7c4	za
přezdívku	přezdívka	k1gFnSc4	přezdívka
Emil	Emil	k1gMnSc1	Emil
Hrozný	hrozný	k2eAgInSc1d1	hrozný
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
lidé	člověk	k1gMnPc1	člověk
ptali	ptat	k5eAaImAgMnP	ptat
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
umučený	umučený	k2eAgInSc4d1	umučený
výraz	výraz	k1gInSc4	výraz
ve	v	k7c6	v
tváři	tvář	k1gFnSc6	tvář
<g/>
,	,	kIx,	,
prý	prý	k9	prý
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
víte	vědět	k5eAaImIp2nP	vědět
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
gymnastika	gymnastika	k1gFnSc1	gymnastika
nebo	nebo	k8xC	nebo
krasobruslení	krasobruslení	k1gNnSc1	krasobruslení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Trénoval	trénovat	k5eAaImAgMnS	trénovat
za	za	k7c2	za
každého	každý	k3xTgNnSc2	každý
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
sněžilo	sněžit	k5eAaImAgNnS	sněžit
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
sobě	se	k3xPyFc3	se
měl	mít	k5eAaImAgInS	mít
často	často	k6eAd1	často
těžké	těžký	k2eAgFnPc4d1	těžká
vojenské	vojenský	k2eAgFnPc4d1	vojenská
kanady	kanada	k1gFnPc4	kanada
namísto	namísto	k7c2	namísto
speciálních	speciální	k2eAgFnPc2d1	speciální
běžeckých	běžecký	k2eAgFnPc2d1	běžecká
bot	bota	k1gFnPc2	bota
<g/>
,	,	kIx,	,
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
závodě	závod	k1gInSc6	závod
tak	tak	k6eAd1	tak
získá	získat	k5eAaPmIp3nS	získat
pocit	pocit	k1gInSc4	pocit
lehkých	lehký	k2eAgFnPc2d1	lehká
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
i	i	k9	i
speciální	speciální	k2eAgFnSc4d1	speciální
tréninkovou	tréninkový	k2eAgFnSc4d1	tréninková
pomůcku	pomůcka	k1gFnSc4	pomůcka
<g/>
.	.	kIx.	.
</s>
<s>
Běžel	běžet	k5eAaImAgMnS	běžet
se	se	k3xPyFc4	se
svojí	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
<g/>
.	.	kIx.	.
</s>
<s>
Vždycky	vždycky	k6eAd1	vždycky
rád	rád	k6eAd1	rád
poradil	poradit	k5eAaPmAgMnS	poradit
jiným	jiný	k2eAgMnSc7d1	jiný
běžcům	běžec	k1gMnPc3	běžec
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
například	například	k6eAd1	například
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
běhu	běh	k1gInSc6	běh
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
třeba	třeba	k6eAd1	třeba
být	být	k5eAaImF	být
uvolněný	uvolněný	k2eAgInSc4d1	uvolněný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
dobré	dobrý	k2eAgNnSc1d1	dobré
se	se	k3xPyFc4	se
koncem	koncem	k7c2	koncem
palce	palec	k1gInSc2	palec
lehce	lehko	k6eAd1	lehko
dotknout	dotknout	k5eAaPmF	dotknout
ukazováčku	ukazováček	k1gInSc3	ukazováček
nebo	nebo	k8xC	nebo
prostředníčku	prostředníček	k1gInSc3	prostředníček
<g/>
.	.	kIx.	.
</s>
<s>
Tenhle	tenhle	k3xDgInSc1	tenhle
nepatrný	nepatrný	k2eAgInSc1d1	nepatrný
dotyk	dotyk	k1gInSc1	dotyk
stačí	stačit	k5eAaBmIp3nS	stačit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
paže	paže	k1gFnSc1	paže
a	a	k8xC	a
ramena	rameno	k1gNnSc2	rameno
zůstaly	zůstat	k5eAaPmAgInP	zůstat
uvolněné	uvolněný	k2eAgInPc1d1	uvolněný
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	on	k3xPp3gMnSc4	on
tempařské	tempařský	k2eAgFnPc1d1	tempařský
schopnosti	schopnost	k1gFnPc1	schopnost
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
přezdívalo	přezdívat	k5eAaImAgNnS	přezdívat
Česká	český	k2eAgFnSc1d1	Česká
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgFnSc4d1	sportovní
kariéru	kariéra	k1gFnSc4	kariéra
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
spojil	spojit	k5eAaPmAgInS	spojit
se	s	k7c7	s
službou	služba	k1gFnSc7	služba
vojáka	voják	k1gMnSc2	voják
z	z	k7c2	z
povolání	povolání	k1gNnSc2	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
rozsudku	rozsudek	k1gInSc2	rozsudek
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
Zátopkovo	Zátopkův	k2eAgNnSc1d1	Zátopkovo
prohlášení	prohlášení	k1gNnSc1	prohlášení
v	v	k7c6	v
Rudém	rudý	k2eAgNnSc6d1	Rudé
právu	právo	k1gNnSc6	právo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
odsouzeným	odsouzený	k1gMnPc3	odsouzený
opakovaně	opakovaně	k6eAd1	opakovaně
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gNnSc1	jejich
počínání	počínání	k1gNnSc1	počínání
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
hanebné	hanebný	k2eAgNnSc4d1	hanebné
<g/>
"	"	kIx"	"
a	a	k8xC	a
rozsudek	rozsudek	k1gInSc4	rozsudek
schválil	schválit	k5eAaPmAgInS	schválit
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
toť	toť	k?	toť
příkaz	příkaz	k1gInSc1	příkaz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
poctivé	poctivý	k2eAgFnSc2d1	poctivá
práce	práce	k1gFnSc2	práce
všech	všecek	k3xTgMnPc2	všecek
našich	naši	k1gMnPc2	naši
dělníků	dělník	k1gMnPc2	dělník
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pokojný	pokojný	k2eAgInSc1d1	pokojný
život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
zachován	zachovat	k5eAaPmNgInS	zachovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
byl	být	k5eAaImAgInS	být
tajným	tajný	k2eAgMnSc7d1	tajný
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
Vojenské	vojenský	k2eAgFnSc2d1	vojenská
kontrarozvědky	kontrarozvědka	k1gFnSc2	kontrarozvědka
s	s	k7c7	s
krycím	krycí	k2eAgNnSc7d1	krycí
jménem	jméno	k1gNnSc7	jméno
Macek	Macek	k1gMnSc1	Macek
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
sportovnímu	sportovní	k2eAgNnSc3d1	sportovní
vytížení	vytížení	k1gNnSc3	vytížení
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
příliš	příliš	k6eAd1	příliš
neangažoval	angažovat	k5eNaBmAgMnS	angažovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
předsednictvu	předsednictvo	k1gNnSc6	předsednictvo
Československého	československý	k2eAgInSc2d1	československý
výboru	výbor	k1gInSc2	výbor
obránců	obránce	k1gMnPc2	obránce
míru	mír	k1gInSc2	mír
se	s	k7c7	s
schůzí	schůze	k1gFnSc7	schůze
tohoto	tento	k3xDgInSc2	tento
orgánu	orgán	k1gInSc2	orgán
účastnil	účastnit	k5eAaImAgInS	účastnit
jen	jen	k9	jen
poskrovnu	poskrovnu	k6eAd1	poskrovnu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
podporoval	podporovat	k5eAaImAgInS	podporovat
demokratické	demokratický	k2eAgNnSc4d1	demokratické
křídlo	křídlo	k1gNnSc4	křídlo
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
za	za	k7c2	za
okupace	okupace	k1gFnSc2	okupace
vojsky	vojsky	k6eAd1	vojsky
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1968	[number]	k4	1968
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
protestů	protest	k1gInPc2	protest
a	a	k8xC	a
po	po	k7c6	po
Pražském	pražský	k2eAgInSc6d1	pražský
jaru	jar	k1gInSc6	jar
byl	být	k5eAaImAgMnS	být
odvolán	odvolán	k2eAgMnSc1d1	odvolán
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
důležitých	důležitý	k2eAgFnPc2d1	důležitá
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
žalobě	žaloba	k1gFnSc6	žaloba
čtyř	čtyři	k4xCgFnPc2	čtyři
dalších	další	k2eAgFnPc2d1	další
osobností	osobnost	k1gFnPc2	osobnost
(	(	kIx(	(
<g/>
Luboš	Luboš	k1gMnSc1	Luboš
Holeček	Holeček	k1gMnSc1	Holeček
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Kohout	Kohout	k1gMnSc1	Kohout
<g/>
,	,	kIx,	,
Luděk	Luděk	k1gMnSc1	Luděk
Pachman	Pachman	k1gMnSc1	Pachman
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Škutina	Škutin	k1gMnSc2	Škutin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
podali	podat	k5eAaPmAgMnP	podat
kvůli	kvůli	k7c3	kvůli
křivému	křivý	k2eAgNnSc3d1	křivé
nařčení	nařčení	k1gNnSc3	nařčení
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
upálením	upálení	k1gNnSc7	upálení
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
na	na	k7c4	na
komunistického	komunistický	k2eAgMnSc4d1	komunistický
poslance	poslanec	k1gMnSc4	poslanec
Viléma	Vilém	k1gMnSc4	Vilém
Nového	Nový	k1gMnSc4	Nový
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
prý	prý	k9	prý
Palacha	Palacha	k1gFnSc1	Palacha
k	k	k7c3	k
činu	čin	k1gInSc3	čin
naváděli	navádět	k5eAaImAgMnP	navádět
<g/>
,	,	kIx,	,
oni	onen	k3xDgMnPc1	onen
ale	ale	k9	ale
Palacha	Palacha	k1gFnSc1	Palacha
vůbec	vůbec	k9	vůbec
neznali	znát	k5eNaImAgMnP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Zátopek	Zátopek	k1gInSc1	Zátopek
při	při	k7c6	při
projednání	projednání	k1gNnSc6	projednání
žaloby	žaloba	k1gFnSc2	žaloba
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1970	[number]	k4	1970
před	před	k7c7	před
Městským	městský	k2eAgInSc7d1	městský
soudem	soud	k1gInSc7	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
ale	ale	k8xC	ale
žalobu	žaloba	k1gFnSc4	žaloba
odvolal	odvolat	k5eAaPmAgMnS	odvolat
a	a	k8xC	a
Novému	Nový	k1gMnSc3	Nový
se	se	k3xPyFc4	se
omluvil	omluvit	k5eAaPmAgMnS	omluvit
<g/>
.	.	kIx.	.
</s>
<s>
Žaloba	žaloba	k1gFnSc1	žaloba
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
čtyř	čtyři	k4xCgFnPc2	čtyři
byla	být	k5eAaImAgFnS	být
zamítnuta	zamítnout	k5eAaPmNgFnS	zamítnout
se	s	k7c7	s
zdůvodněním	zdůvodnění	k1gNnSc7	zdůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
žalující	žalující	k2eAgFnSc4d1	žalující
nemají	mít	k5eNaImIp3nP	mít
právo	právo	k1gNnSc4	právo
hájit	hájit	k5eAaImF	hájit
svou	svůj	k3xOyFgFnSc4	svůj
čest	čest	k1gFnSc4	čest
před	před	k7c7	před
socialistickým	socialistický	k2eAgInSc7d1	socialistický
soudem	soud	k1gInSc7	soud
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jako	jako	k9	jako
známí	známý	k2eAgMnPc1d1	známý
antisocialisté	antisocialista	k1gMnPc1	antisocialista
a	a	k8xC	a
pravicoví	pravicový	k2eAgMnPc1d1	pravicový
oportunisté	oportunista	k1gMnPc1	oportunista
tuto	tento	k3xDgFnSc4	tento
čest	čest	k1gFnSc4	čest
stejně	stejně	k6eAd1	stejně
již	již	k6eAd1	již
ztratili	ztratit	k5eAaPmAgMnP	ztratit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nuceném	nucený	k2eAgInSc6d1	nucený
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
armády	armáda	k1gFnSc2	armáda
jej	on	k3xPp3gMnSc4	on
nikde	nikde	k6eAd1	nikde
nesměli	smět	k5eNaImAgMnP	smět
zaměstnat	zaměstnat	k5eAaPmF	zaměstnat
<g/>
,	,	kIx,	,
místo	místo	k6eAd1	místo
našel	najít	k5eAaPmAgMnS	najít
až	až	k6eAd1	až
u	u	k7c2	u
podniku	podnik	k1gInSc2	podnik
Stavební	stavební	k2eAgFnSc2d1	stavební
geologie	geologie	k1gFnSc2	geologie
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
jezdil	jezdit	k5eAaImAgMnS	jezdit
a	a	k8xC	a
hloubil	hloubit	k5eAaImAgMnS	hloubit
studny	studna	k1gFnPc4	studna
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kolegy	kolega	k1gMnPc7	kolega
přespával	přespávat	k5eAaImAgMnS	přespávat
v	v	k7c6	v
maringotce	maringotka	k1gFnSc6	maringotka
<g/>
,	,	kIx,	,
domů	domů	k6eAd1	domů
se	se	k3xPyFc4	se
vracel	vracet	k5eAaImAgMnS	vracet
na	na	k7c4	na
víkendy	víkend	k1gInPc4	víkend
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
týden	týden	k1gInSc4	týden
nebo	nebo	k8xC	nebo
i	i	k9	i
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
Španělský	španělský	k2eAgMnSc1d1	španělský
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
chtěl	chtít	k5eAaImAgMnS	chtít
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
čtyřnásobný	čtyřnásobný	k2eAgMnSc1d1	čtyřnásobný
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
žije	žít	k5eAaImIp3nS	žít
a	a	k8xC	a
kterého	který	k3yRgNnSc2	který
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
Dana	Dana	k1gFnSc1	Dana
Zátopková	Zátopková	k1gFnSc1	Zátopková
zavezla	zavézt	k5eAaPmAgFnS	zavézt
<g/>
,	,	kIx,	,
spatřil	spatřit	k5eAaPmAgMnS	spatřit
Zátopka	Zátopek	k1gMnSc4	Zátopek
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zablácený	zablácený	k2eAgMnSc1d1	zablácený
pracuje	pracovat	k5eAaImIp3nS	pracovat
ve	v	k7c6	v
špíně	špína	k1gFnSc6	špína
a	a	k8xC	a
bahně	bahno	k1gNnSc6	bahno
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
podle	podle	k7c2	podle
Dany	Dana	k1gFnSc2	Dana
Zátopkové	Zátopkový	k2eAgFnSc2d1	Zátopková
se	se	k3xPyFc4	se
cestou	cesta	k1gFnSc7	cesta
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
rozplakal	rozplakat	k5eAaPmAgInS	rozplakat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Takhle	takhle	k6eAd1	takhle
vy	vy	k3xPp2nPc1	vy
tady	tady	k6eAd1	tady
opečováváte	opečovávat	k5eAaImIp2nP	opečovávat
svého	svůj	k3xOyFgMnSc4	svůj
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
potupně	potupně	k6eAd1	potupně
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
své	své	k1gNnSc4	své
proreformní	proreformní	k2eAgFnSc2d1	proreformní
orientace	orientace	k1gFnSc2	orientace
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zastával	zastávat	k5eAaImAgMnS	zastávat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
1968-1969	[number]	k4	1968-1969
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
signatáře	signatář	k1gMnSc4	signatář
Anticharty	anticharta	k1gFnSc2	anticharta
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1984	[number]	k4	1984
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sovětští	sovětský	k2eAgMnPc1d1	sovětský
sportovci	sportovec	k1gMnPc1	sportovec
se	se	k3xPyFc4	se
nezúčastní	zúčastnit	k5eNaPmIp3nS	zúčastnit
Letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
šest	šest	k4xCc4	šest
dnů	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
k	k	k7c3	k
bojkotu	bojkot	k1gInSc3	bojkot
Olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
i	i	k9	i
Československo	Československo	k1gNnSc4	Československo
<g/>
,	,	kIx,	,
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
oznámil	oznámit	k5eAaPmAgMnS	oznámit
předseda	předseda	k1gMnSc1	předseda
Československého	československý	k2eAgInSc2d1	československý
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
ČSTV	ČSTV	kA	ČSTV
Antonín	Antonín	k1gMnSc1	Antonín
Himl	himl	k0	himl
<g/>
.	.	kIx.	.
</s>
<s>
Zátopek	Zátopek	k1gInSc1	Zátopek
v	v	k7c6	v
prohlášení	prohlášení	k1gNnSc6	prohlášení
pro	pro	k7c4	pro
Rudé	rudý	k2eAgNnSc4d1	Rudé
právo	právo	k1gNnSc4	právo
tehdy	tehdy	k6eAd1	tehdy
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
přivítal	přivítat	k5eAaPmAgMnS	přivítat
a	a	k8xC	a
připojil	připojit	k5eAaPmAgMnS	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
obavám	obava	k1gFnPc3	obava
o	o	k7c4	o
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
Zátopka	Zátopek	k1gMnSc4	Zátopek
rehabilitoval	rehabilitovat	k5eAaBmAgMnS	rehabilitovat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Zátopek	Zátopek	k1gInSc1	Zátopek
zemřel	zemřít	k5eAaPmAgMnS	zemřít
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
nemoci	nemoc	k1gFnSc6	nemoc
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
78	[number]	k4	78
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
posmrtně	posmrtně	k6eAd1	posmrtně
udělena	udělit	k5eAaPmNgFnS	udělit
medaile	medaile	k1gFnSc1	medaile
Pierra	Pierr	k1gInSc2	Pierr
de	de	k?	de
Coubertina	Coubertin	k1gMnSc2	Coubertin
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
akademii	akademie	k1gFnSc4	akademie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zátopkova	Zátopkův	k2eAgFnSc1d1	Zátopkova
manželka	manželka	k1gFnSc1	manželka
Dana	Dana	k1gFnSc1	Dana
(	(	kIx(	(
<g/>
narozená	narozený	k2eAgFnSc1d1	narozená
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
jako	jako	k8xC	jako
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
také	také	k6eAd1	také
výbornou	výborný	k2eAgFnSc7d1	výborná
atletkou	atletka	k1gFnSc7	atletka
<g/>
,	,	kIx,	,
věnovala	věnovat	k5eAaPmAgFnS	věnovat
se	se	k3xPyFc4	se
hodu	hod	k1gInSc2	hod
oštěpem	oštěp	k1gInSc7	oštěp
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
na	na	k7c4	na
LOH	LOH	kA	LOH
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
několik	několik	k4yIc4	několik
okamžiků	okamžik	k1gInPc2	okamžik
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
vítězství	vítězství	k1gNnSc6	vítězství
na	na	k7c4	na
5	[number]	k4	5
<g/>
km	km	kA	km
trati	trať	k1gFnSc2	trať
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
stříbro	stříbro	k1gNnSc1	stříbro
na	na	k7c4	na
LOH	LOH	kA	LOH
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Emilem	Emil	k1gMnSc7	Emil
a	a	k8xC	a
Danou	Dana	k1gFnSc7	Dana
byl	být	k5eAaImAgMnS	být
hravý	hravý	k2eAgInSc4d1	hravý
vztah	vztah	k1gInSc4	vztah
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Zátopek	Zátopek	k1gInSc1	Zátopek
například	například	k6eAd1	například
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
ukrojit	ukrojit	k5eAaPmF	ukrojit
si	se	k3xPyFc3	se
trochu	trocha	k1gFnSc4	trocha
z	z	k7c2	z
Danina	Danin	k2eAgNnSc2d1	Danino
vítězství	vítězství	k1gNnSc2	vítězství
a	a	k8xC	a
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc4	jeho
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
5	[number]	k4	5
km	km	kA	km
jeho	jeho	k3xOp3gFnSc4	jeho
ženu	žena	k1gFnSc4	žena
inspirovalo	inspirovat	k5eAaBmAgNnS	inspirovat
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
rozzlobeně	rozzlobeně	k6eAd1	rozzlobeně
reagovala	reagovat	k5eAaBmAgFnS	reagovat
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vážně	vážně	k6eAd1	vážně
<g/>
?	?	kIx.	?
</s>
<s>
Tak	tak	k9	tak
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
jen	jen	k6eAd1	jen
běž	běžet	k5eAaImRp2nS	běžet
a	a	k8xC	a
zkus	zkusit	k5eAaPmRp2nS	zkusit
inspirovat	inspirovat	k5eAaBmF	inspirovat
nějakou	nějaký	k3yIgFnSc4	nějaký
jinou	jiný	k2eAgFnSc4d1	jiná
a	a	k8xC	a
uvidíme	uvidět	k5eAaPmIp1nP	uvidět
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
taky	taky	k6eAd1	taky
hodí	hodit	k5eAaPmIp3nS	hodit
oštěp	oštěp	k1gInSc4	oštěp
padesát	padesát	k4xCc4	padesát
metrů	metr	k1gInPc2	metr
daleko	daleko	k6eAd1	daleko
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Vítězství	vítězství	k1gNnSc1	vítězství
je	být	k5eAaImIp3nS	být
skvělá	skvělý	k2eAgFnSc1d1	skvělá
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přátelství	přátelství	k1gNnSc1	přátelství
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
lepší	dobrý	k2eAgMnSc1d2	lepší
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Jestli	jestli	k8xS	jestli
chcete	chtít	k5eAaImIp2nP	chtít
něco	něco	k3yInSc4	něco
vyhrát	vyhrát	k5eAaPmF	vyhrát
<g/>
,	,	kIx,	,
běžte	běžet	k5eAaImRp2nP	běžet
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Jestli	jestli	k8xS	jestli
chcete	chtít	k5eAaImIp2nP	chtít
něco	něco	k3yInSc4	něco
zažít	zažít	k5eAaPmF	zažít
<g/>
,	,	kIx,	,
běžte	běžet	k5eAaImRp2nP	běžet
maraton	maraton	k1gInSc4	maraton
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Nebylo	být	k5eNaImAgNnS	být
mi	já	k3xPp1nSc3	já
dáno	dát	k5eAaPmNgNnS	dát
běžet	běžet	k5eAaImF	běžet
a	a	k8xC	a
usmívat	usmívat	k5eAaImF	usmívat
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hranice	hranice	k1gFnSc1	hranice
bolesti	bolest	k1gFnSc2	bolest
a	a	k8xC	a
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dělí	dělit	k5eAaImIp3nS	dělit
chlapce	chlapec	k1gMnPc4	chlapec
od	od	k7c2	od
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Chvíli	chvíle	k1gFnSc4	chvíle
po	po	k7c4	po
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc4	ten
ale	ale	k8xC	ale
nejhezčí	hezký	k2eAgNnSc1d3	nejhezčí
vyčerpání	vyčerpání	k1gNnSc1	vyčerpání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jsem	být	k5eAaImIp1nS	být
kdy	kdy	k6eAd1	kdy
cítil	cítit	k5eAaImAgMnS	cítit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
intervalovém	intervalový	k2eAgInSc6d1	intervalový
tréninku	trénink	k1gInSc6	trénink
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Všichni	všechen	k3xTgMnPc1	všechen
mi	já	k3xPp1nSc3	já
říkali	říkat	k5eAaImAgMnP	říkat
'	'	kIx"	'
<g/>
Emile	Emil	k1gMnSc5	Emil
<g/>
,	,	kIx,	,
ty	ty	k3xPp2nSc1	ty
jsi	být	k5eAaImIp2nS	být
blázen	blázen	k1gMnSc1	blázen
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
'	'	kIx"	'
Ale	ale	k9	ale
když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
poprvé	poprvé	k6eAd1	poprvé
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
říkali	říkat	k5eAaImAgMnP	říkat
'	'	kIx"	'
<g/>
Emile	Emil	k1gMnSc5	Emil
<g/>
,	,	kIx,	,
ty	ty	k3xPp2nSc1	ty
jsi	být	k5eAaImIp2nS	být
génius	génius	k1gMnSc1	génius
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
Když	když	k8xS	když
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
ptali	ptat	k5eAaImAgMnP	ptat
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
zmučený	zmučený	k2eAgInSc4d1	zmučený
výraz	výraz	k1gInSc4	výraz
během	během	k7c2	během
závodu	závod	k1gInSc2	závod
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
víte	vědět	k5eAaImIp2nP	vědět
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
gymnastika	gymnastika	k1gFnSc1	gymnastika
nebo	nebo	k8xC	nebo
krasobruslení	krasobruslení	k1gNnSc1	krasobruslení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Londýn	Londýn	k1gInSc1	Londýn
1948	[number]	k4	1948
5	[number]	k4	5
000	[number]	k4	000
m	m	kA	m
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
10	[number]	k4	10
000	[number]	k4	000
m	m	kA	m
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Helsinky	Helsinky	k1gFnPc4	Helsinky
1952	[number]	k4	1952
video	video	k1gNnSc4	video
5	[number]	k4	5
000	[number]	k4	000
m	m	kA	m
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
10	[number]	k4	10
000	[number]	k4	000
m	m	kA	m
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
)	)	kIx)	)
maratón	maratón	k1gInSc4	maratón
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Melbourne	Melbourne	k1gNnSc1	Melbourne
1956	[number]	k4	1956
maratón	maratón	k1gInSc4	maratón
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Oslo	Oslo	k1gNnSc1	Oslo
1946	[number]	k4	1946
5	[number]	k4	5
000	[number]	k4	000
m	m	kA	m
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Brusel	Brusel	k1gInSc1	Brusel
1950	[number]	k4	1950
5	[number]	k4	5
000	[number]	k4	000
m	m	kA	m
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
10	[number]	k4	10
000	[number]	k4	000
m	m	kA	m
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Bern	Bern	k1gInSc1	Bern
1954	[number]	k4	1954
5	[number]	k4	5
000	[number]	k4	000
m	m	kA	m
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
10	[number]	k4	10
000	[number]	k4	000
m	m	kA	m
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
5	[number]	k4	5
000	[number]	k4	000
m	m	kA	m
-	-	kIx~	-
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
10	[number]	k4	10
000	[number]	k4	000
m	m	kA	m
-	-	kIx~	-
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
kros	kros	k1gInSc1	kros
-	-	kIx~	-
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Jeho	jeho	k3xOp3gNnSc7	jeho
jménem	jméno	k1gNnSc7	jméno
je	být	k5eAaImIp3nS	být
přezdíváno	přezdíván	k2eAgNnSc1d1	přezdíváno
elektrické	elektrický	k2eAgFnSc6d1	elektrická
lokomotivě	lokomotiva	k1gFnSc6	lokomotiva
řady	řada	k1gFnSc2	řada
380	[number]	k4	380
vyráběné	vyráběný	k2eAgNnSc1d1	vyráběné
Škoda	škoda	k1gFnSc1	škoda
Transportation	Transportation	k1gInSc1	Transportation
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Emil	Emil	k1gMnSc1	Emil
Zátopek	Zátopka	k1gFnPc2	Zátopka
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
opery	opera	k1gFnSc2	opera
britské	britský	k2eAgFnSc2d1	britská
skladatelky	skladatelka	k1gFnSc2	skladatelka
Emily	Emil	k1gMnPc4	Emil
Howardové	Howardový	k2eAgFnSc2d1	Howardový
Zátopek	Zátopek	k1gInSc4	Zátopek
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
