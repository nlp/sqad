<p>
<s>
Bongo	bongo	k1gNnSc1	bongo
lesní	lesní	k2eAgNnSc1d1	lesní
<g/>
,	,	kIx,	,
též	též	k9	též
antilopa	antilopa	k1gFnSc1	antilopa
bongo	bongo	k1gNnSc1	bongo
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
bongo	bongo	k1gNnSc1	bongo
(	(	kIx(	(
<g/>
Tragelaphus	Tragelaphus	k1gMnSc1	Tragelaphus
eurycerus	eurycerus	k1gMnSc1	eurycerus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
a	a	k8xC	a
nejtěžší	těžký	k2eAgFnSc1d3	nejtěžší
lesní	lesní	k2eAgFnSc1d1	lesní
antilopa	antilopa	k1gFnSc1	antilopa
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
sytě	sytě	k6eAd1	sytě
kaštanově	kaštanově	k6eAd1	kaštanově
hnědou	hnědý	k2eAgFnSc7d1	hnědá
srstí	srst	k1gFnSc7	srst
s	s	k7c7	s
bílými	bílý	k2eAgInPc7d1	bílý
svislými	svislý	k2eAgInPc7d1	svislý
pruhy	pruh	k1gInPc7	pruh
a	a	k8xC	a
tmavou	tmavý	k2eAgFnSc7d1	tmavá
maskou	maska	k1gFnSc7	maska
je	být	k5eAaImIp3nS	být
právem	právem	k6eAd1	právem
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejpestřeji	pestro	k6eAd3	pestro
zbarvenou	zbarvený	k2eAgFnSc4d1	zbarvená
a	a	k8xC	a
nejkrásnější	krásný	k2eAgFnSc4d3	nejkrásnější
africkou	africký	k2eAgFnSc4d1	africká
antilopu	antilopa	k1gFnSc4	antilopa
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
této	tento	k3xDgFnSc2	tento
antilopy	antilopa	k1gFnSc2	antilopa
mají	mít	k5eAaImIp3nP	mít
rohy	roh	k1gInPc4	roh
<g/>
,	,	kIx,	,
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
objevení	objevení	k1gNnSc6	objevení
byl	být	k5eAaImAgInS	být
bongo	bongo	k1gNnSc4	bongo
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Taurotragus	Taurotragus	k1gInSc4	Taurotragus
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
antilopou	antilopa	k1gFnSc7	antilopa
losí	losí	k2eAgFnSc7d1	losí
a	a	k8xC	a
antilopou	antilopa	k1gFnSc7	antilopa
Derbyho	Derby	k1gMnSc2	Derby
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
sdílí	sdílet	k5eAaImIp3nS	sdílet
tento	tento	k3xDgInSc1	tento
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
toto	tento	k3xDgNnSc1	tento
zařazení	zařazení	k1gNnSc1	zařazení
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
nevyhovující	vyhovující	k2eNgNnSc1d1	nevyhovující
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
antilopu	antilopa	k1gFnSc4	antilopa
bongo	bongo	k1gNnSc1	bongo
byl	být	k5eAaImAgInS	být
zřízen	zřízen	k2eAgInSc1d1	zřízen
monotypický	monotypický	k2eAgInSc1d1	monotypický
rod	rod	k1gInSc1	rod
Boocercus	Boocercus	k1gInSc1	Boocercus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
uznávána	uznáván	k2eAgFnSc1d1	uznávána
jeho	jeho	k3xOp3gFnSc1	jeho
pozice	pozice	k1gFnSc1	pozice
v	v	k7c6	v
rodu	rod	k1gInSc6	rod
Tragelaphus	Tragelaphus	k1gInSc1	Tragelaphus
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
nyaly	nyal	k1gMnPc4	nyal
<g/>
,	,	kIx,	,
kudu	kudu	k1gMnPc4	kudu
a	a	k8xC	a
sitatunga	sitatung	k1gMnSc4	sitatung
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Synonyma	synonymum	k1gNnSc2	synonymum
===	===	k?	===
</s>
</p>
<p>
<s>
Tragelaphus	Tragelaphus	k1gInSc1	Tragelaphus
euryceros	eurycerosa	k1gFnPc2	eurycerosa
</s>
</p>
<p>
<s>
Boocercus	Boocercus	k1gMnSc1	Boocercus
eurycerus	eurycerus	k1gMnSc1	eurycerus
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
240-400	[number]	k4	240-400
kg	kg	kA	kg
</s>
</p>
<p>
<s>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
170-250	[number]	k4	170-250
cm	cm	kA	cm
</s>
</p>
<p>
<s>
délka	délka	k1gFnSc1	délka
ocasu	ocas	k1gInSc2	ocas
<g/>
:	:	kIx,	:
45-65	[number]	k4	45-65
cm	cm	kA	cm
</s>
</p>
<p>
<s>
výška	výška	k1gFnSc1	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
<g/>
:	:	kIx,	:
110-130	[number]	k4	110-130
cmBongo	cmBongo	k6eAd1	cmBongo
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
a	a	k8xC	a
nejmohutnější	mohutný	k2eAgInSc4d3	nejmohutnější
druh	druh	k1gInSc4	druh
lesních	lesní	k2eAgFnPc2d1	lesní
antilop	antilopa	k1gFnPc2	antilopa
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
samic	samice	k1gFnPc2	samice
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
210	[number]	k4	210
a	a	k8xC	a
235	[number]	k4	235
kg	kg	kA	kg
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgFnPc1d2	veliký
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
vážit	vážit	k5eAaImF	vážit
až	až	k9	až
400	[number]	k4	400
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Stavbou	stavba	k1gFnSc7	stavba
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
bongo	bongo	k1gNnSc4	bongo
přizpůsobený	přizpůsobený	k2eAgInSc1d1	přizpůsobený
pohybu	pohyb	k1gInSc3	pohyb
v	v	k7c6	v
pralese	prales	k1gInSc6	prales
-	-	kIx~	-
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
tělu	tělo	k1gNnSc3	tělo
má	mít	k5eAaImIp3nS	mít
kratší	krátký	k2eAgFnSc1d2	kratší
končetiny	končetina	k1gFnPc1	končetina
a	a	k8xC	a
poněkud	poněkud	k6eAd1	poněkud
kapří	kapří	k2eAgInSc4d1	kapří
hřbet	hřbet	k1gInSc4	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc1	pohlaví
mají	mít	k5eAaImIp3nP	mít
spirálovitě	spirálovitě	k6eAd1	spirálovitě
zatočené	zatočený	k2eAgInPc4d1	zatočený
rohy	roh	k1gInPc4	roh
lyrovitého	lyrovitý	k2eAgInSc2d1	lyrovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
délky	délka	k1gFnPc4	délka
asi	asi	k9	asi
75	[number]	k4	75
cm	cm	kA	cm
<g/>
,	,	kIx,	,
u	u	k7c2	u
býků	býk	k1gMnPc2	býk
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
až	až	k6eAd1	až
metru	metr	k1gInSc2	metr
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Rohovina	rohovina	k1gFnSc1	rohovina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	on	k3xPp3gInPc4	on
kryje	krýt	k5eAaImIp3nS	krýt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
černohnědá	černohnědý	k2eAgFnSc1d1	černohnědá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Srst	srst	k1gFnSc1	srst
samic	samice	k1gFnPc2	samice
a	a	k8xC	a
mláďat	mládě	k1gNnPc2	mládě
je	být	k5eAaImIp3nS	být
sytě	sytě	k6eAd1	sytě
kaštanově	kaštanově	k6eAd1	kaštanově
hnědá	hnědat	k5eAaImIp3nS	hnědat
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
věkem	věk	k1gInSc7	věk
tmavnou	tmavnout	k5eAaImIp3nP	tmavnout
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
až	až	k9	až
mahagonově	mahagonově	k6eAd1	mahagonově
černí	černit	k5eAaImIp3nS	černit
<g/>
.	.	kIx.	.
</s>
<s>
Končetiny	končetina	k1gFnPc1	končetina
jsou	být	k5eAaImIp3nP	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
tmavěji	tmavě	k6eAd2	tmavě
zbarvené	zbarvený	k2eAgNnSc1d1	zbarvené
<g/>
.	.	kIx.	.
</s>
<s>
Tmavě	tmavě	k6eAd1	tmavě
hnědá	hnědý	k2eAgFnSc1d1	hnědá
srst	srst	k1gFnSc1	srst
také	také	k9	také
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
masku	maska	k1gFnSc4	maska
na	na	k7c6	na
obličejové	obličejový	k2eAgFnSc6d1	obličejová
části	část	k1gFnSc6	část
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
bílá	bílý	k2eAgFnSc1d1	bílá
srst	srst	k1gFnSc1	srst
kryje	krýt	k5eAaImIp3nS	krýt
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
stany	stan	k1gInPc4	stan
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
"	"	kIx"	"
<g/>
nánosník	nánosník	k1gInSc1	nánosník
<g/>
"	"	kIx"	"
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
oválné	oválný	k2eAgFnPc1d1	oválná
skvrny	skvrna	k1gFnPc1	skvrna
na	na	k7c6	na
lících	líc	k1gFnPc6	líc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prsou	prsa	k1gNnPc6	prsa
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
půlměsíc	půlměsíc	k1gInSc4	půlměsíc
a	a	k8xC	a
nevyhýbá	vyhýbat	k5eNaImIp3nS	vyhýbat
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
končetinám	končetina	k1gFnPc3	končetina
<g/>
.	.	kIx.	.
10-15	[number]	k4	10-15
bílých	bílý	k2eAgInPc2d1	bílý
pruhů	pruh	k1gInPc2	pruh
zdobí	zdobit	k5eAaImIp3nP	zdobit
boky	boka	k1gFnPc4	boka
zvířete	zvíře	k1gNnSc2	zvíře
od	od	k7c2	od
kohoutku	kohoutek	k1gInSc2	kohoutek
až	až	k9	až
po	po	k7c4	po
stehna	stehno	k1gNnPc4	stehno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
krku	krk	k1gInSc6	krk
a	a	k8xC	a
nohou	noha	k1gFnPc6	noha
jsou	být	k5eAaImIp3nP	být
tmavě	tmavě	k6eAd1	tmavě
hnědé	hnědý	k2eAgFnPc1d1	hnědá
skvrny	skvrna	k1gFnPc1	skvrna
různé	různý	k2eAgFnSc2d1	různá
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kopýtky	kopýtko	k1gNnPc7	kopýtko
má	mít	k5eAaImIp3nS	mít
černobílé	černobílý	k2eAgFnPc4d1	černobílá
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc1	pohlaví
mají	mít	k5eAaImIp3nP	mít
krátkou	krátký	k2eAgFnSc4d1	krátká
černobílou	černobílý	k2eAgFnSc4d1	černobílá
hřívu	hříva	k1gFnSc4	hříva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnPc1	rozšíření
a	a	k8xC	a
stanoviště	stanoviště	k1gNnPc1	stanoviště
==	==	k?	==
</s>
</p>
<p>
<s>
Antilopa	antilopa	k1gFnSc1	antilopa
bongo	bongo	k1gNnSc4	bongo
obývá	obývat	k5eAaImIp3nS	obývat
nížinné	nížinný	k2eAgInPc4d1	nížinný
deštné	deštný	k2eAgInPc4d1	deštný
pralesy	prales	k1gInPc4	prales
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Súdánu	Súdán	k1gInSc6	Súdán
a	a	k8xC	a
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Angole	Angola	k1gFnSc6	Angola
<g/>
,	,	kIx,	,
Beninu	Benino	k1gNnSc6	Benino
<g/>
,	,	kIx,	,
Burkině	Burkina	k1gFnSc3	Burkina
Faso	Faso	k6eAd1	Faso
<g/>
,	,	kIx,	,
Kamerunu	Kamerun	k1gInSc2	Kamerun
<g/>
,	,	kIx,	,
Středoafrické	středoafrický	k2eAgFnSc6d1	Středoafrická
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
na	na	k7c6	na
Pobřeží	pobřeží	k1gNnSc6	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rovníkové	rovníkový	k2eAgFnSc6d1	Rovníková
Guineji	Guinea	k1gFnSc6	Guinea
<g/>
,	,	kIx,	,
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
,	,	kIx,	,
Gabunu	Gabun	k1gInSc6	Gabun
<g/>
,	,	kIx,	,
Ghaně	Ghana	k1gFnSc6	Ghana
<g/>
,	,	kIx,	,
Guineji	Guinea	k1gFnSc6	Guinea
<g/>
,	,	kIx,	,
Guineji-Bissau	Guineji-Bissaus	k1gInSc6	Guineji-Bissaus
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
Libérii	Libérie	k1gFnSc6	Libérie
<g/>
,	,	kIx,	,
Mali	Mali	k1gNnSc6	Mali
<g/>
,	,	kIx,	,
Nigeru	Niger	k1gInSc6	Niger
a	a	k8xC	a
v	v	k7c6	v
Sierra	Sierra	k1gFnSc1	Sierra
Leone	Leo	k1gMnSc5	Leo
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
populace	populace	k1gFnPc1	populace
žijí	žít	k5eAaImIp3nP	žít
také	také	k9	také
v	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
horských	horský	k2eAgInPc6d1	horský
tropických	tropický	k2eAgInPc6d1	tropický
lesích	les	k1gInPc6	les
a	a	k8xC	a
bambusových	bambusový	k2eAgInPc6d1	bambusový
porostech	porost	k1gInPc6	porost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bongo	bongo	k1gNnSc1	bongo
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
neprostupný	prostupný	k2eNgInSc1d1	neprostupný
pralesní	pralesní	k2eAgInSc1d1	pralesní
podrost	podrost	k1gInSc1	podrost
<g/>
,	,	kIx,	,
bambusové	bambusový	k2eAgInPc1d1	bambusový
lesy	les	k1gInPc1	les
a	a	k8xC	a
křoviny	křovina	k1gFnPc1	křovina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nachází	nacházet	k5eAaImIp3nS	nacházet
úkryt	úkryt	k1gInSc4	úkryt
i	i	k8xC	i
dostatek	dostatek	k1gInSc4	dostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
životu	život	k1gInSc3	život
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
4000	[number]	k4	4000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Biologie	biologie	k1gFnSc2	biologie
==	==	k?	==
</s>
</p>
<p>
<s>
Bongo	bongo	k1gNnSc1	bongo
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
plaché	plachý	k2eAgNnSc1d1	plaché
zvíře	zvíře	k1gNnSc1	zvíře
s	s	k7c7	s
převážně	převážně	k6eAd1	převážně
noční	noční	k2eAgFnSc7d1	noční
aktivitou	aktivita	k1gFnSc7	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediná	jediný	k2eAgFnSc1d1	jediná
lesní	lesní	k2eAgFnSc1d1	lesní
antilopa	antilopa	k1gFnSc1	antilopa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
pase	pást	k5eAaImIp3nS	pást
ve	v	k7c6	v
stádech	stádo	k1gNnPc6	stádo
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
tvoří	tvořit	k5eAaImIp3nP	tvořit
menší	malý	k2eAgNnPc1d2	menší
stáda	stádo	k1gNnPc1	stádo
o	o	k7c4	o
5	[number]	k4	5
nebo	nebo	k8xC	nebo
6	[number]	k4	6
kusech	kus	k1gInPc6	kus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
větší	veliký	k2eAgFnPc4d2	veliký
skupiny	skupina	k1gFnPc4	skupina
až	až	k9	až
o	o	k7c6	o
padesáti	padesát	k4xCc6	padesát
antilopách	antilopa	k1gFnPc6	antilopa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jediném	jediný	k2eAgNnSc6d1	jediné
stádu	stádo	k1gNnSc6	stádo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
více	hodně	k6eAd2	hodně
býků	býk	k1gMnPc2	býk
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
nejsou	být	k5eNaImIp3nP	být
příliš	příliš	k6eAd1	příliš
teritoriální	teritoriální	k2eAgFnPc1d1	teritoriální
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
býci	býk	k1gMnPc1	býk
mohou	moct	k5eAaImIp3nP	moct
žít	žít	k5eAaImF	žít
samotářsky	samotářsky	k6eAd1	samotářsky
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
velmi	velmi	k6eAd1	velmi
skrytě	skrytě	k6eAd1	skrytě
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
ukrývají	ukrývat	k5eAaImIp3nP	ukrývat
v	v	k7c6	v
podrostu	podrost	k1gInSc6	podrost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
mladé	mladý	k2eAgInPc4d1	mladý
listy	list	k1gInPc4	list
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
keřů	keř	k1gInPc2	keř
<g/>
,	,	kIx,	,
větvičky	větvička	k1gFnPc4	větvička
<g/>
,	,	kIx,	,
výhonky	výhonek	k1gInPc4	výhonek
nebo	nebo	k8xC	nebo
i	i	k9	i
květy	květ	k1gInPc4	květ
a	a	k8xC	a
plody	plod	k1gInPc4	plod
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
pasou	pást	k5eAaImIp3nP	pást
na	na	k7c6	na
trávě	tráva	k1gFnSc6	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
přírodní	přírodní	k2eAgInPc4d1	přírodní
minerální	minerální	k2eAgInPc4d1	minerální
lizy	liz	k1gInPc4	liz
<g/>
,	,	kIx,	,
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
půdu	půda	k1gFnSc4	půda
bohatou	bohatý	k2eAgFnSc4d1	bohatá
na	na	k7c4	na
minerální	minerální	k2eAgFnPc4d1	minerální
látky	látka	k1gFnPc4	látka
nebo	nebo	k8xC	nebo
požírají	požírat	k5eAaImIp3nP	požírat
spálené	spálený	k2eAgNnSc4d1	spálené
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Výhonky	výhonek	k1gInPc1	výhonek
s	s	k7c7	s
listy	list	k1gInPc7	list
strhávají	strhávat	k5eAaImIp3nP	strhávat
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
k	k	k7c3	k
lámání	lámání	k1gNnSc3	lámání
vyšších	vysoký	k2eAgFnPc2d2	vyšší
větví	větev	k1gFnPc2	větev
pak	pak	k6eAd1	pak
používají	používat	k5eAaImIp3nP	používat
své	svůj	k3xOyFgInPc4	svůj
rohy	roh	k1gInPc4	roh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
polyestrická	polyestrický	k2eAgNnPc4d1	polyestrický
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
krávy	kráva	k1gFnPc1	kráva
přicházejí	přicházet	k5eAaImIp3nP	přicházet
do	do	k7c2	do
říje	říje	k1gFnSc2	říje
každých	každý	k3xTgNnPc2	každý
21	[number]	k4	21
nebo	nebo	k8xC	nebo
22	[number]	k4	22
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
trvá	trvat	k5eAaImIp3nS	trvat
282	[number]	k4	282
až	až	k9	až
285	[number]	k4	285
dní	den	k1gInPc2	den
(	(	kIx(	(
<g/>
9	[number]	k4	9
měsíců	měsíc	k1gInPc2	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
narodí	narodit	k5eAaPmIp3nS	narodit
jedno	jeden	k4xCgNnSc1	jeden
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
dvě	dva	k4xCgNnPc4	dva
telata	tele	k1gNnPc4	tele
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
je	on	k3xPp3gInPc4	on
odkládá	odkládat	k5eAaImIp3nS	odkládat
v	v	k7c6	v
podrostu	podrost	k1gInSc6	podrost
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
je	on	k3xPp3gInPc4	on
přichází	přicházet	k5eAaImIp3nS	přicházet
kojit	kojit	k5eAaImF	kojit
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
leží	ležet	k5eAaImIp3nS	ležet
asi	asi	k9	asi
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
následuje	následovat	k5eAaImIp3nS	následovat
matku	matka	k1gFnSc4	matka
a	a	k8xC	a
připojí	připojit	k5eAaPmIp3nS	připojit
se	se	k3xPyFc4	se
ke	k	k7c3	k
stádu	stádo	k1gNnSc3	stádo
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
3	[number]	k4	3
měsících	měsíc	k1gInPc6	měsíc
začnou	začít	k5eAaPmIp3nP	začít
teleti	tele	k1gNnSc3	tele
růst	růst	k1gInSc4	růst
rohy	roh	k1gInPc1	roh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odstaveno	odstaven	k2eAgNnSc1d1	odstaveno
asi	asi	k9	asi
v	v	k7c6	v
6	[number]	k4	6
měsících	měsíc	k1gInPc6	měsíc
<g/>
,	,	kIx,	,
bongové	bong	k1gMnPc1	bong
pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
ve	v	k7c6	v
20	[number]	k4	20
měsících	měsíc	k1gInPc6	měsíc
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
dožít	dožít	k5eAaPmF	dožít
až	až	k9	až
19	[number]	k4	19
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gMnSc7	jejich
největším	veliký	k2eAgMnSc7d3	veliký
nepřítelem	nepřítel	k1gMnSc7	nepřítel
levhart	levhart	k1gMnSc1	levhart
<g/>
,	,	kIx,	,
hroznýši	hroznýš	k1gMnPc1	hroznýš
a	a	k8xC	a
hyeny	hyena	k1gFnPc1	hyena
<g/>
.	.	kIx.	.
</s>
<s>
Bongové	Bong	k1gMnPc1	Bong
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
lekaví	lekavý	k2eAgMnPc1d1	lekavý
a	a	k8xC	a
při	při	k7c6	při
sebemenším	sebemenší	k2eAgNnSc6d1	sebemenší
vyrušení	vyrušení	k1gNnSc6	vyrušení
se	se	k3xPyFc4	se
dávají	dávat	k5eAaImIp3nP	dávat
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běhu	běh	k1gInSc6	běh
se	se	k3xPyFc4	se
nahrbí	nahrbit	k5eAaBmIp3nS	nahrbit
a	a	k8xC	a
položí	položit	k5eAaPmIp3nS	položit
si	se	k3xPyFc3	se
rohy	roh	k1gInPc4	roh
na	na	k7c4	na
hřbet	hřbet	k1gInSc4	hřbet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
při	při	k7c6	při
běhu	běh	k1gInSc6	běh
nezapletly	zaplést	k5eNaPmAgFnP	zaplést
do	do	k7c2	do
lián	liána	k1gFnPc2	liána
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruhy	poddruh	k1gInPc4	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
Popisují	popisovat	k5eAaImIp3nP	popisovat
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc1	dva
poddruhy	poddruh	k1gInPc1	poddruh
antilopy	antilopa	k1gFnSc2	antilopa
bongo	bongo	k1gNnSc1	bongo
<g/>
:	:	kIx,	:
bongo	bongo	k1gNnSc1	bongo
nížinný	nížinný	k2eAgMnSc1d1	nížinný
<g/>
,	,	kIx,	,
T.	T.	kA	T.
e.	e.	k?	e.
eurycerus	eurycerus	k1gInSc1	eurycerus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obývá	obývat	k5eAaImIp3nS	obývat
většinu	většina	k1gFnSc4	většina
současného	současný	k2eAgInSc2d1	současný
areálu	areál	k1gInSc2	areál
bonga	bongo	k1gNnSc2	bongo
v	v	k7c6	v
nížinných	nížinný	k2eAgInPc6d1	nížinný
deštných	deštný	k2eAgInPc6d1	deštný
pralesích	prales	k1gInPc6	prales
<g/>
,	,	kIx,	,
a	a	k8xC	a
bongo	bongo	k1gNnSc1	bongo
horský	horský	k2eAgMnSc1d1	horský
(	(	kIx(	(
<g/>
T.	T.	kA	T.
eurycerus	eurycerus	k1gInSc1	eurycerus
isaaci	isaace	k1gFnSc3	isaace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
ohrožený	ohrožený	k2eAgInSc1d1	ohrožený
vyhubením	vyhubení	k1gNnSc7	vyhubení
a	a	k8xC	a
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
Ugandy	Uganda	k1gFnSc2	Uganda
už	už	k6eAd1	už
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyhynul	vyhynout	k5eAaPmAgMnS	vyhynout
<g/>
.	.	kIx.	.
</s>
<s>
Nížinný	nížinný	k2eAgInSc1d1	nížinný
bongo	bongo	k1gNnSc4	bongo
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
jeho	jeho	k3xOp3gMnSc1	jeho
horský	horský	k2eAgMnSc1d1	horský
příbuzný	příbuzný	k1gMnSc1	příbuzný
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
výrazněji	výrazně	k6eAd2	výrazně
zbarvený	zbarvený	k2eAgInSc1d1	zbarvený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
kolik	kolik	k4yIc4	kolik
antilop	antilopa	k1gFnPc2	antilopa
bongo	bongo	k1gNnSc4	bongo
v	v	k7c6	v
neprostupných	prostupný	k2eNgInPc6d1	neprostupný
pralesích	prales	k1gInPc6	prales
vlastně	vlastně	k9	vlastně
žije	žít	k5eAaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
předpokládané	předpokládaný	k2eAgFnSc6d1	předpokládaná
populační	populační	k2eAgFnSc6d1	populační
hustotě	hustota	k1gFnSc6	hustota
0,25	[number]	k4	0,25
kusu	kus	k1gInSc2	kus
na	na	k7c4	na
km	km	kA	km
<g/>
2	[number]	k4	2
na	na	k7c6	na
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
bongové	bong	k1gMnPc1	bong
běžně	běžně	k6eAd1	běžně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
0,02	[number]	k4	0,02
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
částech	část	k1gFnPc6	část
areálu	areál	k1gInSc2	areál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
celkovou	celkový	k2eAgFnSc4d1	celková
rozlohu	rozloha	k1gFnSc4	rozloha
327	[number]	k4	327
000	[number]	k4	000
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
celková	celkový	k2eAgFnSc1d1	celková
populace	populace	k1gFnSc1	populace
mohla	moct	k5eAaImAgFnS	moct
činit	činit	k5eAaImF	činit
asi	asi	k9	asi
28	[number]	k4	28
000	[number]	k4	000
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jen	jen	k9	jen
asi	asi	k9	asi
na	na	k7c4	na
60	[number]	k4	60
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
bongo	bongo	k1gNnSc1	bongo
chráněný	chráněný	k2eAgMnSc1d1	chráněný
<g/>
.	.	kIx.	.
</s>
<s>
Nížinný	nížinný	k2eAgInSc1d1	nížinný
bongo	bongo	k1gNnSc4	bongo
není	být	k5eNaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
horský	horský	k2eAgInSc4d1	horský
bongo	bongo	k1gNnSc4	bongo
byl	být	k5eAaImAgInS	být
téměř	téměř	k6eAd1	téměř
vyhuben	vyhubit	k5eAaPmNgInS	vyhubit
nadměrným	nadměrný	k2eAgInSc7d1	nadměrný
lovem	lov	k1gInSc7	lov
se	s	k7c7	s
psy	pes	k1gMnPc7	pes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
byli	být	k5eAaImAgMnP	být
bongové	bong	k1gMnPc1	bong
téměř	téměř	k6eAd1	téměř
vyhlazeni	vyhladit	k5eAaPmNgMnP	vyhladit
epidemií	epidemie	k1gFnSc7	epidemie
moru	mor	k1gInSc2	mor
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgNnSc3	který
jsou	být	k5eAaImIp3nP	být
zvláště	zvláště	k6eAd1	zvláště
vnímaví	vnímavý	k2eAgMnPc1d1	vnímavý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
je	on	k3xPp3gInPc4	on
ulovit	ulovit	k5eAaPmF	ulovit
do	do	k7c2	do
pastí	past	k1gFnPc2	past
nebo	nebo	k8xC	nebo
pomocí	pomoc	k1gFnPc2	pomoc
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
antilopy	antilopa	k1gFnSc2	antilopa
ale	ale	k8xC	ale
dlouho	dlouho	k6eAd1	dlouho
chránily	chránit	k5eAaImAgFnP	chránit
pověry	pověra	k1gFnPc1	pověra
<g/>
:	:	kIx,	:
místní	místní	k2eAgMnPc1d1	místní
lidé	člověk	k1gMnPc1	člověk
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dotknou	dotknout	k5eAaPmIp3nP	dotknout
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
bonga	bongo	k1gNnPc1	bongo
<g/>
,	,	kIx,	,
onemocní	onemocnět	k5eAaPmIp3nP	onemocnět
epilepsií	epilepsie	k1gFnSc7	epilepsie
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
ale	ale	k9	ale
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
tabu	tabu	k1gNnSc1	tabu
již	již	k6eAd1	již
neplatí	platit	k5eNaImIp3nS	platit
a	a	k8xC	a
domorodci	domorodec	k1gMnPc1	domorodec
antilopy	antilopa	k1gFnSc2	antilopa
loví	lovit	k5eAaImIp3nP	lovit
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
kůži	kůže	k1gFnSc4	kůže
i	i	k8xC	i
rohy	roh	k1gInPc4	roh
<g/>
.	.	kIx.	.
</s>
<s>
Bongo	bongo	k1gNnSc1	bongo
je	být	k5eAaImIp3nS	být
ohrožený	ohrožený	k2eAgInSc1d1	ohrožený
také	také	k9	také
kácením	kácení	k1gNnSc7	kácení
pralesů	prales	k1gInPc2	prales
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
==	==	k?	==
</s>
</p>
<p>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
chovaná	chovaný	k2eAgNnPc1d1	chované
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
jsou	být	k5eAaImIp3nP	být
potomky	potomek	k1gMnPc7	potomek
antilop	antilopa	k1gFnPc2	antilopa
odchycených	odchycený	k2eAgFnPc2d1	odchycená
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Aberdare	Aberdar	k1gMnSc5	Aberdar
v	v	k7c6	v
Keni	Keňa	k1gFnSc2	Keňa
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
proto	proto	k8xC	proto
horští	horský	k2eAgMnPc1d1	horský
bongové	bong	k1gMnPc1	bong
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Antilopa	antilopa	k1gFnSc1	antilopa
bongo	bongo	k1gNnSc4	bongo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
chována	chovat	k5eAaImNgFnS	chovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Dvůr	Dvůr	k1gInSc1	Dvůr
Králové	Králová	k1gFnSc2	Králová
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
PrahaV	PrahaV	k1gFnSc2	PrahaV
minulosti	minulost	k1gFnSc2	minulost
patřily	patřit	k5eAaImAgInP	patřit
k	k	k7c3	k
chovatelům	chovatel	k1gMnPc3	chovatel
tohoto	tento	k3xDgInSc2	tento
vzácného	vzácný	k2eAgInSc2d1	vzácný
druhu	druh	k1gInSc2	druh
také	také	k9	také
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Liberec	Liberec	k1gInSc1	Liberec
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
OlomoucNa	OlomoucN	k1gInSc2	OlomoucN
Slovensku	Slovensko	k1gNnSc3	Slovensko
chová	chovat	k5eAaImIp3nS	chovat
bonga	bongo	k1gNnSc2	bongo
Zoo	zoo	k1gNnSc2	zoo
Bojnice	Bojnice	k1gFnPc1	Bojnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
chován	chovat	k5eAaImNgInS	chovat
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
pěti	pět	k4xCc6	pět
desítkách	desítka	k1gFnPc6	desítka
veřejných	veřejný	k2eAgNnPc2d1	veřejné
chovatelských	chovatelský	k2eAgNnPc2d1	chovatelské
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
pak	pak	k6eAd1	pak
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Evropské	evropský	k2eAgFnSc2d1	Evropská
asociace	asociace	k1gFnSc2	asociace
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
a	a	k8xC	a
akvárií	akvárium	k1gNnPc2	akvárium
(	(	kIx(	(
<g/>
EAZA	EAZA	kA	EAZA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
chováno	chovat	k5eAaImNgNnS	chovat
178	[number]	k4	178
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
===	===	k?	===
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
chovu	chov	k1gInSc2	chov
započala	započnout	k5eAaPmAgFnS	započnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgInS	být
dovezen	dovézt	k5eAaPmNgInS	dovézt
pár	pár	k4xCyI	pár
<g/>
:	:	kIx,	:
samec	samec	k1gMnSc1	samec
Wajir	Wajir	k1gMnSc1	Wajir
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
Kindu	Kind	k1gInSc2	Kind
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
mládě	mládě	k1gNnSc1	mládě
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
odchováno	odchovat	k5eAaPmNgNnS	odchovat
47	[number]	k4	47
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
3	[number]	k4	3
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
chováno	chovat	k5eAaImNgNnS	chovat
9	[number]	k4	9
jedinců	jedinec	k1gMnPc2	jedinec
(	(	kIx(	(
<g/>
3	[number]	k4	3
samci	samec	k1gInSc6	samec
a	a	k8xC	a
6	[number]	k4	6
samic	samice	k1gFnPc2	samice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
nejvíce	nejvíce	k6eAd1	nejvíce
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
slovenských	slovenský	k2eAgFnPc2d1	slovenská
zoo	zoo	k1gFnPc2	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
čtyř	čtyři	k4xCgNnPc2	čtyři
jedinců	jedinec	k1gMnPc2	jedinec
do	do	k7c2	do
jiných	jiný	k2eAgNnPc2d1	jiné
chovatelských	chovatelský	k2eAgNnPc2d1	chovatelské
zařízení	zařízení	k1gNnPc2	zařízení
chováno	chovat	k5eAaImNgNnS	chovat
pět	pět	k4xCc1	pět
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
<g/>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
ve	v	k7c6	v
výběhu	výběh	k1gInSc6	výběh
vedle	vedle	k7c2	vedle
pavilonu	pavilon	k1gInSc2	pavilon
hrochů	hroch	k1gMnPc2	hroch
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
zoo	zoo	k1gFnSc2	zoo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
podobě	podoba	k1gFnSc6	podoba
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bongo	bongo	k1gNnSc4	bongo
lesní	lesní	k2eAgMnSc1d1	lesní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Tragelaphus	Tragelaphus	k1gMnSc1	Tragelaphus
eurycerus	eurycerus	k1gMnSc1	eurycerus
ve	v	k7c6	v
WikidruzíchZOO	WikidruzíchZOO	k1gFnSc6	WikidruzíchZOO
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Lexikon	lexikon	k1gNnSc1	lexikon
zvířat	zvíře	k1gNnPc2	zvíře
-	-	kIx~	-
Bongo	bongo	k1gNnSc1	bongo
</s>
</p>
<p>
<s>
Bongo	bongo	k1gNnSc1	bongo
(	(	kIx(	(
<g/>
Tragelaphus	Tragelaphus	k1gInSc1	Tragelaphus
eurycerus	eurycerus	k1gInSc1	eurycerus
Ogilby	Ogilba	k1gFnSc2	Ogilba
<g/>
,	,	kIx,	,
1837	[number]	k4	1837
<g/>
)	)	kIx)	)
<g/>
|	|	kIx~	|
<g/>
Wild	Wild	k1gMnSc1	Wild
Africa	Africa	k1gMnSc1	Africa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
zvířat	zvíře	k1gNnPc2	zvíře
</s>
</p>
<p>
<s>
An	An	k?	An
Ultimate	Ultimat	k1gInSc5	Ultimat
Ungulate	Ungulat	k1gInSc5	Ungulat
fact	fact	k1gMnSc1	fact
sheet	sheet	k1gMnSc1	sheet
-	-	kIx~	-
Tragelaphus	Tragelaphus	k1gMnSc1	Tragelaphus
eurycerus	eurycerus	k1gMnSc1	eurycerus
<g/>
,	,	kIx,	,
Bongo	bongo	k1gNnSc1	bongo
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Brensike	Brensike	k1gFnSc1	Brensike
<g/>
,	,	kIx,	,
J.	J.	kA	J.
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Tragelaphus	Tragelaphus	k1gMnSc1	Tragelaphus
eurycerus	eurycerus	k1gMnSc1	eurycerus
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
On-line	Onin	k1gInSc5	On-lin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Animal	animal	k1gMnSc1	animal
Diversity	Diversit	k1gInPc4	Diversit
Web	web	k1gInSc1	web
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
African	African	k1gMnSc1	African
Wildlife	Wildlif	k1gInSc5	Wildlif
Foundation	Foundation	k1gInSc1	Foundation
<g/>
:	:	kIx,	:
Bongo	bongo	k1gNnSc1	bongo
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
IUCN	IUCN	kA	IUCN
Red	Red	k1gFnSc1	Red
List	list	k1gInSc1	list
of	of	k?	of
Threatened	Threatened	k1gInSc1	Threatened
Species	species	k1gFnSc1	species
<g/>
:	:	kIx,	:
Tragelaphus	Tragelaphus	k1gMnSc1	Tragelaphus
eurycerus	eurycerus	k1gMnSc1	eurycerus
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
