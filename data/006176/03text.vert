<s>
Páni	pan	k1gMnPc1	pan
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
von	von	k1gInSc1	von
Pernstein	Pernstein	k2eAgInSc1d1	Pernstein
<g/>
)	)	kIx)	)
či	či	k8xC	či
Pernštejnové	Pernštejn	k1gMnPc1	Pernštejn
byli	být	k5eAaImAgMnP	být
výrazný	výrazný	k2eAgInSc4d1	výrazný
šlechtický	šlechtický	k2eAgInSc4d1	šlechtický
rod	rod	k1gInSc4	rod
původně	původně	k6eAd1	původně
z	z	k7c2	z
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgInSc2d3	veliký
rozmachu	rozmach	k1gInSc2	rozmach
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
patřili	patřit	k5eAaImAgMnP	patřit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Rožmberky	Rožmberk	k1gInPc4	Rožmberk
k	k	k7c3	k
nejmocnějším	mocný	k2eAgInPc3d3	nejmocnější
rodům	rod	k1gInPc3	rod
České	český	k2eAgFnSc2d1	Česká
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
Rožmberkové	Rožmberkové	k?	Rožmberkové
a	a	k8xC	a
právě	právě	k9	právě
Pernštejnové	Pernštejn	k1gMnPc1	Pernštejn
mohli	moct	k5eAaImAgMnP	moct
mít	mít	k5eAaImF	mít
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
své	své	k1gNnSc4	své
paláce	palác	k1gInSc2	palác
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc1	ten
pernštejnský	pernštejnský	k2eAgInSc1d1	pernštejnský
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nazývá	nazývat	k5eAaImIp3nS	nazývat
Lobkovický	lobkovický	k2eAgInSc1d1	lobkovický
palác	palác	k1gInSc1	palác
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozšiřováním	rozšiřování	k1gNnSc7	rozšiřování
pozemkového	pozemkový	k2eAgNnSc2d1	pozemkové
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
a	a	k8xC	a
rozumným	rozumný	k2eAgNnSc7d1	rozumné
hospodařením	hospodaření	k1gNnSc7	hospodaření
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
využití	využití	k1gNnSc2	využití
tzv.	tzv.	kA	tzv.
režijního	režijní	k2eAgNnSc2d1	režijní
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rod	rod	k1gInSc1	rod
nakonec	nakonec	k6eAd1	nakonec
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
nejbohatších	bohatý	k2eAgMnPc2d3	nejbohatší
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Rodu	rod	k1gInSc2	rod
náležela	náležet	k5eAaImAgNnP	náležet
rozsáhlá	rozsáhlý	k2eAgNnPc1d1	rozsáhlé
panství	panství	k1gNnPc1	panství
jak	jak	k6eAd1	jak
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
(	(	kIx(	(
<g/>
Pernštejn	Pernštejn	k1gInSc1	Pernštejn
<g/>
,	,	kIx,	,
Zubštejn	Zubštejn	k1gInSc1	Zubštejn
<g/>
,	,	kIx,	,
Aueršperk	Aueršperk	k1gInSc1	Aueršperk
<g/>
,	,	kIx,	,
Pyšolec	Pyšolec	k1gInSc1	Pyšolec
<g/>
,	,	kIx,	,
Louka	louka	k1gFnSc1	louka
<g/>
,	,	kIx,	,
Mitrov	Mitrov	k1gInSc1	Mitrov
<g/>
,	,	kIx,	,
Jimramov	Jimramov	k1gInSc1	Jimramov
<g/>
,	,	kIx,	,
Víckov	Víckov	k1gInSc1	Víckov
<g/>
,	,	kIx,	,
Lísek	líska	k1gFnPc2	líska
<g/>
,	,	kIx,	,
Dalečín	Dalečína	k1gFnPc2	Dalečína
<g/>
,	,	kIx,	,
Rysov	Rysov	k1gInSc1	Rysov
<g/>
,	,	kIx,	,
Jakubov	Jakubov	k1gInSc1	Jakubov
<g/>
,	,	kIx,	,
Helfštejn	Helfštejn	k1gInSc1	Helfštejn
<g/>
,	,	kIx,	,
Plumlov	Plumlov	k1gInSc1	Plumlov
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
,	,	kIx,	,
Tovačov	Tovačov	k1gInSc1	Tovačov
<g/>
,	,	kIx,	,
Lipník	Lipník	k1gInSc1	Lipník
nad	nad	k7c7	nad
Bečvou	Bečva	k1gFnSc7	Bečva
<g/>
,	,	kIx,	,
Přerov	Přerov	k1gInSc1	Přerov
<g/>
,	,	kIx,	,
Hranice	hranice	k1gFnSc1	hranice
<g/>
,	,	kIx,	,
Drahotuše	Drahotuše	k1gFnSc1	Drahotuše
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
tak	tak	k9	tak
zejména	zejména	k9	zejména
od	od	k7c2	od
přelomu	přelom	k1gInSc2	přelom
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k9	i
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
Kunětická	kunětický	k2eAgFnSc1d1	Kunětická
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Potštejn	Potštejn	k1gInSc1	Potštejn
<g/>
,	,	kIx,	,
Rychnov	Rychnov	k1gInSc1	Rychnov
<g />
.	.	kIx.	.
</s>
<s>
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
,	,	kIx,	,
Litice	Litice	k1gFnSc1	Litice
<g/>
,	,	kIx,	,
Brandýs	Brandýs	k1gInSc1	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Častolovice	Častolovice	k1gFnSc1	Častolovice
<g/>
,	,	kIx,	,
Lanšperk	Lanšperk	k1gInSc1	Lanšperk
<g/>
,	,	kIx,	,
Rychmburk	Rychmburk	k1gInSc1	Rychmburk
<g/>
,	,	kIx,	,
Lanškroun	Lanškroun	k1gInSc1	Lanškroun
<g/>
,	,	kIx,	,
Holice	holice	k1gFnSc1	holice
<g/>
,	,	kIx,	,
Dašice	Dašice	k1gFnSc1	Dašice
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
Ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
Čechách	Čechy	k1gFnPc6	Čechy
významně	významně	k6eAd1	významně
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
tamní	tamní	k2eAgNnSc4d1	tamní
hospodářství	hospodářství	k1gNnSc4	hospodářství
i	i	k8xC	i
utváření	utváření	k1gNnSc4	utváření
krajiny	krajina	k1gFnSc2	krajina
založením	založení	k1gNnSc7	založení
soustavy	soustava	k1gFnSc2	soustava
rybníků	rybník	k1gInPc2	rybník
a	a	k8xC	a
zavodňovacích	zavodňovací	k2eAgFnPc2d1	zavodňovací
struh	strouha	k1gFnPc2	strouha
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
mnohé	mnohý	k2eAgFnPc1d1	mnohá
přetrvaly	přetrvat	k5eAaPmAgFnP	přetrvat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Pernštejnové	Pernštejn	k1gMnPc1	Pernštejn
se	se	k3xPyFc4	se
vyznačovali	vyznačovat	k5eAaImAgMnP	vyznačovat
dlouhověkostí	dlouhověkost	k1gFnSc7	dlouhověkost
<g/>
,	,	kIx,	,
s	s	k7c7	s
průměrným	průměrný	k2eAgInSc7d1	průměrný
věkem	věk	k1gInSc7	věk
dožití	dožití	k1gNnSc2	dožití
asi	asi	k9	asi
70	[number]	k4	70
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
rodu	rod	k1gInSc2	rod
měli	mít	k5eAaImAgMnP	mít
potomky	potomek	k1gMnPc4	potomek
i	i	k8xC	i
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
šedesáti	šedesát	k4xCc6	šedesát
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
Pernštejnové	Pernštejnové	k2eAgMnSc1d1	Pernštejnové
nakonec	nakonec	k6eAd1	nakonec
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
jiné	jiný	k2eAgInPc1d1	jiný
starobylé	starobylý	k2eAgInPc1d1	starobylý
české	český	k2eAgInPc1d1	český
rody	rod	k1gInPc1	rod
<g/>
,	,	kIx,	,
vymřeli	vymřít	k5eAaPmAgMnP	vymřít
v	v	k7c6	v
neklidném	klidný	k2eNgInSc6d1	neklidný
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
po	po	k7c6	po
meči	meč	k1gInSc6	meč
roku	rok	k1gInSc2	rok
1631	[number]	k4	1631
(	(	kIx(	(
<g/>
rok	rok	k1gInSc4	rok
po	po	k7c6	po
Smiřických	smiřický	k2eAgFnPc6d1	Smiřická
<g/>
,	,	kIx,	,
dvacet	dvacet	k4xCc1	dvacet
let	léto	k1gNnPc2	léto
po	po	k7c6	po
svých	svůj	k3xOyFgFnPc6	svůj
velkých	velký	k2eAgFnPc6d1	velká
soupeřích	soupeř	k1gMnPc6	soupeř
Rožmbercích	Rožmberk	k1gInPc6	Rožmberk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
přeslici	přeslice	k1gFnSc6	přeslice
roku	rok	k1gInSc2	rok
1646	[number]	k4	1646
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc1	jejich
dědici	dědic	k1gMnPc1	dědic
–	–	k?	–
včetně	včetně	k7c2	včetně
erbu	erb	k1gInSc2	erb
–	–	k?	–
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Lobkovicové	Lobkovicový	k2eAgFnPc4d1	Lobkovicová
<g/>
.	.	kIx.	.
</s>
<s>
Užití	užití	k1gNnSc1	užití
predikátu	predikát	k1gInSc2	predikát
"	"	kIx"	"
<g/>
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1285	[number]	k4	1285
a	a	k8xC	a
poté	poté	k6eAd1	poté
roku	rok	k1gInSc2	rok
1292	[number]	k4	1292
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
uvažován	uvažován	k2eAgInSc1d1	uvažován
jako	jako	k8xS	jako
doba	doba	k1gFnSc1	doba
přesídlení	přesídlení	k1gNnSc2	přesídlení
rodu	rod	k1gInSc2	rod
na	na	k7c4	na
rodové	rodový	k2eAgNnSc4d1	rodové
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Pernštejn	Pernštejn	k1gInSc1	Pernštejn
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
spojováno	spojovat	k5eAaImNgNnS	spojovat
s	s	k7c7	s
německým	německý	k2eAgInSc7d1	německý
výrazem	výraz	k1gInSc7	výraz
Bärenstein	Bärenstein	k1gMnSc1	Bärenstein
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
medvědí	medvědí	k2eAgInSc1d1	medvědí
kámen	kámen	k1gInSc1	kámen
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
podhradím	podhradí	k1gNnSc7	podhradí
totiž	totiž	k9	totiž
protéká	protékat	k5eAaImIp3nS	protékat
říčka	říčka	k1gFnSc1	říčka
Nedvědice	Nedvědice	k1gFnSc1	Nedvědice
(	(	kIx(	(
<g/>
Medvědice	medvědice	k1gFnSc1	medvědice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poněmčené	poněmčený	k2eAgInPc1d1	poněmčený
názvy	název	k1gInPc1	název
byly	být	k5eAaImAgInP	být
pro	pro	k7c4	pro
šlechtu	šlechta	k1gFnSc4	šlechta
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Rodovým	rodový	k2eAgNnSc7d1	rodové
heslem	heslo	k1gNnSc7	heslo
Pernštejnů	Pernštejn	k1gInPc2	Pernštejn
bylo	být	k5eAaImAgNnS	být
Kdo	kdo	k3yInSc1	kdo
vytrvá	vytrvat	k5eAaPmIp3nS	vytrvat
<g/>
,	,	kIx,	,
vítězí	vítězit	k5eAaImIp3nS	vítězit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
erb	erb	k1gInSc4	erb
přijali	přijmout	k5eAaPmAgMnP	přijmout
černou	černý	k2eAgFnSc4d1	černá
hlavu	hlava	k1gFnSc4	hlava
zubra	zubr	k1gMnSc2	zubr
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
houžví	houžev	k1gFnSc7	houžev
v	v	k7c6	v
nozdrách	nozdra	k1gFnPc6	nozdra
<g/>
,	,	kIx,	,
pole	pole	k1gNnSc1	pole
štítu	štít	k1gInSc2	štít
bylo	být	k5eAaImAgNnS	být
nejprve	nejprve	k6eAd1	nejprve
stříbrné	stříbrný	k2eAgNnSc1d1	stříbrné
<g/>
,	,	kIx,	,
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zobrazovalo	zobrazovat	k5eAaImAgNnS	zobrazovat
jako	jako	k9	jako
zlaté	zlatý	k2eAgNnSc1d1	Zlaté
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
původ	původ	k1gInSc1	původ
erbu	erb	k1gInSc2	erb
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
(	(	kIx(	(
<g/>
erbovní	erbovní	k2eAgFnSc1d1	erbovní
pověst	pověst	k1gFnSc1	pověst
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byli	být	k5eAaImAgMnP	být
Pernštejnové	Pernštejnová	k1gFnPc4	Pernštejnová
v	v	k7c6	v
době	doba	k1gFnSc6	doba
renesance	renesance	k1gFnSc2	renesance
takřka	takřka	k6eAd1	takřka
nejbohatší	bohatý	k2eAgInSc1d3	nejbohatší
rod	rod	k1gInSc1	rod
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejich	jejich	k3xOp3gInPc6	jejich
počátcích	počátek	k1gInPc6	počátek
mnoho	mnoho	k6eAd1	mnoho
informací	informace	k1gFnPc2	informace
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
nazývali	nazývat	k5eAaImAgMnP	nazývat
páni	pan	k1gMnPc1	pan
z	z	k7c2	z
Medlova	Medlův	k2eAgNnSc2d1	Medlův
<g/>
.	.	kIx.	.
</s>
<s>
Prapředkem	prapředek	k1gMnSc7	prapředek
pernštejnského	pernštejnský	k2eAgInSc2d1	pernštejnský
rodu	rod	k1gInSc2	rod
byl	být	k5eAaImAgMnS	být
syn	syn	k1gMnSc1	syn
Gotharda	Gothard	k1gMnSc2	Gothard
z	z	k7c2	z
Medlova	Medlův	k2eAgInSc2d1	Medlův
Štěpán	Štěpán	k1gMnSc1	Štěpán
(	(	kIx(	(
<g/>
†	†	k?	†
<g/>
1235	[number]	k4	1235
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
purkrabí	purkrabí	k1gMnSc1	purkrabí
hradu	hrad	k1gInSc2	hrad
Děvičky	Děvička	k1gFnSc2	Děvička
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
správce	správce	k1gMnSc1	správce
hradu	hrad	k1gInSc2	hrad
Veveří	veveří	k2eAgFnSc1d1	veveří
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
píšící	píšící	k2eAgMnSc1d1	píšící
i	i	k8xC	i
po	po	k7c6	po
hradě	hrad	k1gInSc6	hrad
Pernštejně	Pernštejn	k1gInSc6	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
písemné	písemný	k2eAgFnSc2d1	písemná
historie	historie	k1gFnSc2	historie
rodu	rod	k1gInSc2	rod
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
rok	rok	k1gInSc1	rok
1208	[number]	k4	1208
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
pochází	pocházet	k5eAaImIp3nS	pocházet
listina	listina	k1gFnSc1	listina
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gInSc7	její
předmětem	předmět	k1gInSc7	předmět
byla	být	k5eAaImAgFnS	být
výměna	výměna	k1gFnSc1	výměna
pozemků	pozemek	k1gInPc2	pozemek
mezi	mezi	k7c7	mezi
Štěpánem	Štěpán	k1gMnSc7	Štěpán
a	a	k8xC	a
olomouckým	olomoucký	k2eAgMnSc7d1	olomoucký
biskupem	biskup	k1gMnSc7	biskup
Robertem	Robert	k1gMnSc7	Robert
Angličanem	Angličan	k1gMnSc7	Angličan
<g/>
.	.	kIx.	.
</s>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
nechal	nechat	k5eAaPmAgMnS	nechat
za	za	k7c2	za
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
vystavět	vystavět	k5eAaPmF	vystavět
rodinný	rodinný	k2eAgInSc4d1	rodinný
klášter	klášter	k1gInSc4	klášter
při	při	k7c6	při
kostele	kostel	k1gInSc6	kostel
Povýšení	povýšení	k1gNnSc2	povýšení
sv.	sv.	kA	sv.
Kříže	kříž	k1gInSc2	kříž
v	v	k7c6	v
Doubravníku	Doubravník	k1gInSc6	Doubravník
jako	jako	k8xC	jako
místo	místo	k7c2	místo
rodových	rodový	k2eAgInPc2d1	rodový
pohřbů	pohřeb	k1gInPc2	pohřeb
<g/>
,	,	kIx,	,
či	či	k8xC	či
jako	jako	k9	jako
útočiště	útočiště	k1gNnSc4	útočiště
pro	pro	k7c4	pro
neprovdané	provdaný	k2eNgFnPc4d1	neprovdaná
Pernštejnky	Pernštejnky	k1gFnPc4	Pernštejnky
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
prapotomek	prapotomek	k1gMnSc1	prapotomek
Štěpán	Štěpán	k1gMnSc1	Štěpán
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
a	a	k8xC	a
Medlova	Medlův	k2eAgFnSc1d1	Medlova
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgMnSc7	první
doloženým	doložený	k2eAgMnSc7d1	doložený
vlastníkem	vlastník	k1gMnSc7	vlastník
hradu	hrad	k1gInSc2	hrad
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
Štěpánův	Štěpánův	k2eAgMnSc1d1	Štěpánův
bratranec	bratranec	k1gMnSc1	bratranec
Filip	Filip	k1gMnSc1	Filip
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
rozrodu	rozrod	k1gInSc2	rozrod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1292	[number]	k4	1292
<g/>
–	–	k?	–
<g/>
93	[number]	k4	93
a	a	k8xC	a
1308	[number]	k4	1308
uváděn	uvádět	k5eAaImNgMnS	uvádět
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
zemského	zemský	k2eAgMnSc2d1	zemský
komorníka	komorník	k1gMnSc2	komorník
a	a	k8xC	a
také	také	k9	také
purkrabího	purkrabí	k1gMnSc4	purkrabí
městského	městský	k2eAgInSc2d1	městský
hradu	hrad	k1gInSc2	hrad
v	v	k7c6	v
Hodoníně	Hodonín	k1gInSc6	Hodonín
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Štěpánových	Štěpánových	k2eAgFnSc6d1	Štěpánových
či	či	k8xC	či
Filipových	Filipových	k2eAgFnSc6d1	Filipových
potomcích	potomek	k1gMnPc6	potomek
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tříšti	tříšť	k1gFnSc3	tříšť
a	a	k8xC	a
neúplnosti	neúplnost	k1gFnSc3	neúplnost
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
mnoho	mnoho	k6eAd1	mnoho
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Nejasné	jasný	k2eNgFnPc1d1	nejasná
se	se	k3xPyFc4	se
zdají	zdát	k5eAaPmIp3nP	zdát
být	být	k5eAaImF	být
i	i	k9	i
kořeny	kořen	k1gInPc1	kořen
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
o	o	k7c6	o
možném	možný	k2eAgInSc6d1	možný
uherském	uherský	k2eAgInSc6d1	uherský
či	či	k8xC	či
německém	německý	k2eAgInSc6d1	německý
původu	původ	k1gInSc6	původ
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
sami	sám	k3xTgMnPc1	sám
Pernštejnové	Pernštejn	k1gMnPc1	Pernštejn
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
hrdě	hrdě	k6eAd1	hrdě
hlásili	hlásit	k5eAaImAgMnP	hlásit
k	k	k7c3	k
moravským	moravský	k2eAgInPc3d1	moravský
předkům	předek	k1gInPc3	předek
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
není	být	k5eNaImIp3nS	být
o	o	k7c6	o
Pernštejnech	Pernštejn	k1gInPc6	Pernštejn
mnoho	mnoho	k6eAd1	mnoho
pramenů	pramen	k1gInPc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
rozrůstal	rozrůstat	k5eAaImAgMnS	rozrůstat
a	a	k8xC	a
kromě	kromě	k7c2	kromě
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Svratky	Svratka	k1gFnSc2	Svratka
další	další	k2eAgInPc1d1	další
hrady	hrad	k1gInPc1	hrad
<g/>
:	:	kIx,	:
Aueršperk	Aueršperk	k1gInSc1	Aueršperk
<g/>
,	,	kIx,	,
Pyšolec	Pyšolec	k1gInSc1	Pyšolec
a	a	k8xC	a
Zubštejn	Zubštejn	k1gInSc1	Zubštejn
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
rodová	rodový	k2eAgFnSc1d1	rodová
větev	větev	k1gFnSc1	větev
se	se	k3xPyFc4	se
usadila	usadit	k5eAaPmAgFnS	usadit
v	v	k7c6	v
Jakubově	Jakubův	k2eAgNnSc6d1	Jakubovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
v	v	k7c6	v
listinách	listina	k1gFnPc6	listina
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
příslušníků	příslušník	k1gMnPc2	příslušník
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jejich	jejich	k3xOp3gInPc4	jejich
příbuzenské	příbuzenský	k2eAgInPc4d1	příbuzenský
vztahy	vztah	k1gInPc4	vztah
jsou	být	k5eAaImIp3nP	být
mnohdy	mnohdy	k6eAd1	mnohdy
nejasné	jasný	k2eNgInPc1d1	nejasný
a	a	k8xC	a
rodokmeny	rodokmen	k1gInPc1	rodokmen
sestavené	sestavený	k2eAgInPc1d1	sestavený
genealogy	genealog	k1gMnPc4	genealog
bývají	bývat	k5eAaImIp3nP	bývat
odlišné	odlišný	k2eAgFnPc1d1	odlišná
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
prelátům	prelát	k1gMnPc3	prelát
1	[number]	k4	1
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
patřil	patřit	k5eAaImAgMnS	patřit
například	například	k6eAd1	například
Bedřich	Bedřich	k1gMnSc1	Bedřich
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
rižský	rižský	k2eAgMnSc1d1	rižský
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
Pernštejnové	Pernštejnová	k1gFnPc1	Pernštejnová
stahují	stahovat	k5eAaImIp3nP	stahovat
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
,	,	kIx,	,
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
části	část	k1gFnSc3	část
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
chudnoucí	chudnoucí	k2eAgFnSc4d1	chudnoucí
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
ne	ne	k9	ne
nadlouho	nadlouho	k6eAd1	nadlouho
<g/>
,	,	kIx,	,
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
sporů	spor	k1gInPc2	spor
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
(	(	kIx(	(
<g/>
přelom	přelom	k1gInSc1	přelom
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
nastává	nastávat	k5eAaImIp3nS	nastávat
doba	doba	k1gFnSc1	doba
pro	pro	k7c4	pro
obratné	obratný	k2eAgNnSc4d1	obratné
<g/>
,	,	kIx,	,
až	až	k9	až
bezpáteřní	bezpáteřní	k2eAgFnSc2d1	bezpáteřní
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
takovým	takový	k3xDgMnPc3	takový
zřejmě	zřejmě	k6eAd1	zřejmě
byl	být	k5eAaImAgMnS	být
Vilém	Vilém	k1gMnSc1	Vilém
I.	I.	kA	I.
(	(	kIx(	(
<g/>
†	†	k?	†
<g/>
1427	[number]	k4	1427
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
střídavou	střídavý	k2eAgFnSc7d1	střídavá
podporou	podpora	k1gFnSc7	podpora
Prokopa	Prokop	k1gMnSc2	Prokop
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
markraběte	markrabě	k1gMnSc2	markrabě
Jošta	Jošt	k1gMnSc2	Jošt
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
budoval	budovat	k5eAaImAgMnS	budovat
politickou	politický	k2eAgFnSc4d1	politická
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
členem	člen	k1gMnSc7	člen
užší	úzký	k2eAgFnSc2d2	užší
markraběcí	markraběcí	k2eAgFnSc2d1	markraběcí
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
tedy	tedy	k9	tedy
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
důvěrným	důvěrný	k2eAgNnPc3d1	důvěrné
jednáním	jednání	k1gNnPc3	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Jošt	Jošt	k1gMnSc1	Jošt
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
zajetí	zajetí	k1gNnPc2	zajetí
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
stal	stát	k5eAaPmAgInS	stát
správcem	správce	k1gMnSc7	správce
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
pevné	pevný	k2eAgNnSc4d1	pevné
zázemí	zázemí	k1gNnSc4	zázemí
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
byl	být	k5eAaImAgMnS	být
tedy	tedy	k9	tedy
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1398	[number]	k4	1398
<g/>
/	/	kIx~	/
<g/>
99	[number]	k4	99
pověřen	pověřit	k5eAaPmNgMnS	pověřit
úřadem	úřad	k1gInSc7	úřad
purkrabího	purkrabí	k1gMnSc2	purkrabí
ve	v	k7c6	v
Znojmě	Znojmo	k1gNnSc6	Znojmo
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
i	i	k9	i
moravského	moravský	k2eAgMnSc4d1	moravský
zemského	zemský	k2eAgMnSc4d1	zemský
hejtmana	hejtman	k1gMnSc4	hejtman
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
šlechtický	šlechtický	k2eAgInSc4d1	šlechtický
post	post	k1gInSc4	post
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1408	[number]	k4	1408
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
zastává	zastávat	k5eAaImIp3nS	zastávat
funkci	funkce	k1gFnSc4	funkce
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
komorníka	komorník	k1gMnSc2	komorník
soudu	soud	k1gInSc2	soud
zemského	zemský	k2eAgInSc2d1	zemský
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Léta	léto	k1gNnSc2	léto
1417	[number]	k4	1417
<g/>
–	–	k?	–
<g/>
1420	[number]	k4	1420
jsou	být	k5eAaImIp3nP	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
působením	působení	k1gNnSc7	působení
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
z	z	k7c2	z
Viléma	Vilém	k1gMnSc2	Vilém
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
komorník	komorník	k1gMnSc1	komorník
zemského	zemský	k2eAgInSc2d1	zemský
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Viléma	Viléma	k1gFnSc1	Viléma
již	již	k6eAd1	již
nemohla	moct	k5eNaImAgFnS	moct
ohrozit	ohrozit	k5eAaPmF	ohrozit
ani	ani	k8xC	ani
Joštova	Joštův	k2eAgFnSc1d1	Joštova
smrt	smrt	k1gFnSc1	smrt
(	(	kIx(	(
<g/>
1411	[number]	k4	1411
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
si	se	k3xPyFc3	se
předem	předem	k6eAd1	předem
získal	získat	k5eAaPmAgMnS	získat
pevné	pevný	k2eAgFnPc4d1	pevná
pozice	pozice	k1gFnPc4	pozice
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
až	až	k9	až
polovina	polovina	k1gFnSc1	polovina
jeho	jeho	k3xOp3gInPc2	jeho
příjmů	příjem	k1gInPc2	příjem
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
nelegálních	legální	k2eNgInPc2d1	nelegální
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
však	však	k9	však
nebyl	být	k5eNaImAgMnS	být
prvním	první	k4xOgNnSc7	první
ani	ani	k8xC	ani
posledním	poslední	k2eAgMnSc7d1	poslední
šlechticem	šlechtic	k1gMnSc7	šlechtic
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
kraji	kraj	k1gInSc6	kraj
nechával	nechávat	k5eAaImAgMnS	nechávat
volnou	volný	k2eAgFnSc4d1	volná
ruku	ruka	k1gFnSc4	ruka
loupeživým	loupeživý	k2eAgFnPc3d1	loupeživá
bandám	banda	k1gFnPc3	banda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1414	[number]	k4	1414
i	i	k8xC	i
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
osobně	osobně	k6eAd1	osobně
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
nepřátelství	nepřátelství	k1gNnSc4	nepřátelství
městu	město	k1gNnSc3	město
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
koncem	koncem	k7c2	koncem
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
musel	muset	k5eAaImAgMnS	muset
řešit	řešit	k5eAaImF	řešit
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
husitství	husitství	k1gNnSc2	husitství
se	se	k3xPyFc4	se
Pernštejnové	Pernštejn	k1gMnPc1	Pernštejn
postavili	postavit	k5eAaPmAgMnP	postavit
jednoznačně	jednoznačně	k6eAd1	jednoznačně
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
kališníků	kališník	k1gMnPc2	kališník
<g/>
,	,	kIx,	,
důkazem	důkaz	k1gInSc7	důkaz
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
Vilémova	Vilémův	k2eAgFnSc1d1	Vilémova
pečeť	pečeť	k1gFnSc1	pečeť
(	(	kIx(	(
<g/>
hned	hned	k6eAd1	hned
druhá	druhý	k4xOgFnSc1	druhý
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
)	)	kIx)	)
na	na	k7c6	na
protestním	protestní	k2eAgInSc6d1	protestní
listu	list	k1gInSc6	list
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
adresovanému	adresovaný	k2eAgInSc3d1	adresovaný
kostnickému	kostnický	k2eAgInSc3d1	kostnický
koncilu	koncil	k1gInSc3	koncil
(	(	kIx(	(
<g/>
1415	[number]	k4	1415
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protestující	protestující	k2eAgFnSc1d1	protestující
proti	proti	k7c3	proti
Husovu	Husův	k2eAgNnSc3d1	Husovo
upálení	upálení	k1gNnSc3	upálení
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Vítkově	Vítkov	k1gInSc6	Vítkov
(	(	kIx(	(
<g/>
1420	[number]	k4	1420
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Vilém	Vilém	k1gMnSc1	Vilém
I.	I.	kA	I.
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
podruhé	podruhé	k6eAd1	podruhé
zvolen	zvolit	k5eAaPmNgInS	zvolit
moravským	moravský	k2eAgMnSc7d1	moravský
hejtmanem	hejtman	k1gMnSc7	hejtman
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
však	však	k9	však
Zikmundem	Zikmund	k1gMnSc7	Zikmund
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
pevně	pevně	k6eAd1	pevně
rozhodnut	rozhodnout	k5eAaPmNgMnS	rozhodnout
získat	získat	k5eAaPmF	získat
Moravu	Morava	k1gFnSc4	Morava
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1421	[number]	k4	1421
slíbil	slíbit	k5eAaPmAgMnS	slíbit
Vilém	Vilém	k1gMnSc1	Vilém
Zikmundovi	Zikmund	k1gMnSc3	Zikmund
poslušnost	poslušnost	k1gFnSc4	poslušnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzápětí	vzápětí	k6eAd1	vzápětí
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnPc1	jeho
potomci	potomek	k1gMnPc1	potomek
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
hlásili	hlásit	k5eAaImAgMnP	hlásit
ke	k	k7c3	k
kališníkům	kališník	k1gMnPc3	kališník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1424	[number]	k4	1424
neúspěšně	úspěšně	k6eNd1	úspěšně
obléhalo	obléhat	k5eAaImAgNnS	obléhat
hrad	hrad	k1gInSc4	hrad
Pernštejn	Pernštejn	k1gInSc1	Pernštejn
vojsko	vojsko	k1gNnSc1	vojsko
Zikmundova	Zikmundův	k2eAgMnSc2d1	Zikmundův
zetě	zeť	k1gMnSc2	zeť
Albrechta	Albrecht	k1gMnSc2	Albrecht
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
i	i	k9	i
4	[number]	k4	4
<g/>
.	.	kIx.	.
křížové	křížový	k2eAgFnSc6d1	křížová
výpravě	výprava	k1gFnSc6	výprava
proti	proti	k7c3	proti
kacířským	kacířský	k2eAgMnPc3d1	kacířský
Čechům	Čech	k1gMnPc3	Čech
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
i	i	k9	i
na	na	k7c6	na
pověstných	pověstný	k2eAgInPc6d1	pověstný
husitských	husitský	k2eAgInPc6d1	husitský
rejsech	rejs	k1gInPc6	rejs
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
spanilých	spanilý	k2eAgFnPc6d1	spanilá
jízdách	jízda	k1gFnPc6	jízda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
nejvýraznějším	výrazný	k2eAgMnSc7d3	nejvýraznější
z	z	k7c2	z
Pernštejnů	Pernštejn	k1gInPc2	Pernštejn
je	být	k5eAaImIp3nS	být
označován	označován	k2eAgMnSc1d1	označován
druhorozený	druhorozený	k2eAgMnSc1d1	druhorozený
syn	syn	k1gMnSc1	syn
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
–	–	k?	–
Vilém	Vilém	k1gMnSc1	Vilém
(	(	kIx(	(
<g/>
1438	[number]	k4	1438
<g/>
–	–	k?	–
<g/>
1521	[number]	k4	1521
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
mnoha	mnoho	k4c2	mnoho
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
dobré	dobrý	k2eAgFnSc6d1	dobrá
fyzické	fyzický	k2eAgFnSc6d1	fyzická
kondici	kondice	k1gFnSc6	kondice
a	a	k8xC	a
zdatnosti	zdatnost	k1gFnSc6	zdatnost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
čtrnáctiletý	čtrnáctiletý	k2eAgMnSc1d1	čtrnáctiletý
byl	být	k5eAaImAgMnS	být
společníkem	společník	k1gMnSc7	společník
Ladislava	Ladislav	k1gMnSc2	Ladislav
Pohrobka	pohrobek	k1gMnSc2	pohrobek
(	(	kIx(	(
<g/>
†	†	k?	†
<g/>
1457	[number]	k4	1457
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nebýt	být	k5eNaImF	být
Ladislavovy	Ladislavův	k2eAgFnSc2d1	Ladislavova
náhlé	náhlý	k2eAgFnSc2d1	náhlá
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
rýsovala	rýsovat	k5eAaImAgFnS	rýsovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
po	po	k7c6	po
králově	králův	k2eAgInSc6d1	králův
boku	bok	k1gInSc6	bok
slibná	slibný	k2eAgFnSc1d1	slibná
politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
okruhu	okruh	k1gInSc2	okruh
Jiříka	Jiřík	k1gMnSc2	Jiřík
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
starším	starý	k2eAgMnSc7d2	starší
bratrem	bratr	k1gMnSc7	bratr
Zikmundem	Zikmund	k1gMnSc7	Zikmund
<g/>
,	,	kIx,	,
za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
služby	služba	k1gFnPc4	služba
byli	být	k5eAaImAgMnP	být
odměněni	odměněn	k2eAgMnPc1d1	odměněn
např.	např.	kA	např.
majetky	majetek	k1gInPc4	majetek
kláštera	klášter	k1gInSc2	klášter
Tišnov	Tišnov	k1gInSc1	Tišnov
či	či	k8xC	či
Oslavany	Oslavany	k1gInPc1	Oslavany
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
králově	králův	k2eAgFnSc3d1	králova
straně	strana	k1gFnSc3	strana
ovšem	ovšem	k9	ovšem
stála	stát	k5eAaImAgFnS	stát
Zelenohorská	zelenohorský	k2eAgFnSc1d1	Zelenohorská
jednota	jednota	k1gFnSc1	jednota
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
(	(	kIx(	(
<g/>
1471	[number]	k4	1471
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Vilém	Vilém	k1gMnSc1	Vilém
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
zapeklité	zapeklitý	k2eAgFnSc2d1	zapeklitá
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Bratr	bratr	k1gMnSc1	bratr
Zikmund	Zikmund	k1gMnSc1	Zikmund
byl	být	k5eAaImAgMnS	být
unesen	unést	k5eAaPmNgMnS	unést
a	a	k8xC	a
vězněn	vězněn	k2eAgMnSc1d1	vězněn
přívrženci	přívrženec	k1gMnPc1	přívrženec
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
<g/>
)	)	kIx)	)
<g/>
krále	král	k1gMnSc2	král
Matyáše	Matyáš	k1gMnSc2	Matyáš
Korvína	Korvín	k1gMnSc2	Korvín
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
svobodu	svoboda	k1gFnSc4	svoboda
požadoval	požadovat	k5eAaImAgMnS	požadovat
vojenské	vojenský	k2eAgFnPc4d1	vojenská
služby	služba	k1gFnPc4	služba
Pernštejnů	Pernštejn	k1gInPc2	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
dlouho	dlouho	k6eAd1	dlouho
neváhal	váhat	k5eNaImAgMnS	váhat
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
se	se	k3xPyFc4	se
na	na	k7c4	na
Matyášovu	Matyášův	k2eAgFnSc4d1	Matyášova
stranu	strana	k1gFnSc4	strana
a	a	k8xC	a
z	z	k7c2	z
kališníka	kališník	k1gMnSc2	kališník
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
katolík	katolík	k1gMnSc1	katolík
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
činu	čin	k1gInSc6	čin
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
Vilémova	Vilémův	k2eAgFnSc1d1	Vilémova
tolerantnost	tolerantnost	k1gFnSc1	tolerantnost
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
vlažnost	vlažnost	k1gFnSc4	vlažnost
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Historiky	historik	k1gMnPc4	historik
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
pokrokového	pokrokový	k2eAgNnSc2d1	pokrokové
politika	politikum	k1gNnSc2	politikum
<g/>
.	.	kIx.	.
</s>
<s>
Vyznavače	vyznavač	k1gMnSc4	vyznavač
odlišného	odlišný	k2eAgInSc2d1	odlišný
náboženského	náboženský	k2eAgInSc2d1	náboženský
proudu	proud	k1gInSc2	proud
nepokládal	pokládat	k5eNaImAgMnS	pokládat
za	za	k7c4	za
kacíře	kacíř	k1gMnSc4	kacíř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
za	za	k7c4	za
"	"	kIx"	"
<g/>
jiné	jiný	k2eAgInPc4d1	jiný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
podporoval	podporovat	k5eAaImAgInS	podporovat
myšlenku	myšlenka	k1gFnSc4	myšlenka
nevnucování	nevnucování	k1gNnSc2	nevnucování
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
vlastního	vlastní	k2eAgNnSc2d1	vlastní
svědomí	svědomí	k1gNnSc2	svědomí
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
názory	názor	k1gInPc1	názor
jsou	být	k5eAaImIp3nP	být
shrnuty	shrnut	k2eAgMnPc4d1	shrnut
ve	v	k7c6	v
Vilémově	Vilémův	k2eAgNnSc6d1	Vilémovo
rčení	rčení	k1gNnSc6	rčení
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
S	s	k7c7	s
Římany	Říman	k1gMnPc7	Říman
(	(	kIx(	(
<g/>
=	=	kIx~	=
katolíky	katolík	k1gMnPc4	katolík
<g/>
)	)	kIx)	)
věřím	věřit	k5eAaImIp1nS	věřit
<g/>
,	,	kIx,	,
s	s	k7c7	s
Čechy	Čech	k1gMnPc7	Čech
(	(	kIx(	(
<g/>
=	=	kIx~	=
utrakvisty	utrakvista	k1gMnSc2	utrakvista
<g/>
)	)	kIx)	)
držím	držet	k5eAaImIp1nS	držet
<g/>
,	,	kIx,	,
s	s	k7c7	s
Bratřími	bratr	k1gMnPc7	bratr
(	(	kIx(	(
<g/>
=	=	kIx~	=
Jednotou	jednota	k1gFnSc7	jednota
bratrskou	bratrský	k2eAgFnSc7d1	bratrská
<g/>
)	)	kIx)	)
umírám	umírat	k5eAaImIp1nS	umírat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
S	s	k7c7	s
Vilémem	Vilém	k1gMnSc7	Vilém
je	být	k5eAaImIp3nS	být
však	však	k9	však
spojena	spojit	k5eAaPmNgFnS	spojit
ještě	ještě	k9	ještě
jedna	jeden	k4xCgFnSc1	jeden
velmi	velmi	k6eAd1	velmi
výnosná	výnosný	k2eAgFnSc1d1	výnosná
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
činnost	činnost	k1gFnSc1	činnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
době	doba	k1gFnSc6	doba
začala	začít	k5eAaPmAgFnS	začít
rychle	rychle	k6eAd1	rychle
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
<g/>
:	:	kIx,	:
rybníkářství	rybníkářství	k1gNnPc4	rybníkářství
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
budování	budování	k1gNnSc3	budování
kanálů	kanál	k1gInPc2	kanál
bylo	být	k5eAaImAgNnS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
jeho	jeho	k3xOp3gNnSc1	jeho
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
velkých	velký	k2eAgInPc2d1	velký
územních	územní	k2eAgInPc2d1	územní
celků	celek	k1gInPc2	celek
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Pardubic	Pardubice	k1gInPc2	Pardubice
rybníkářům	rybníkář	k1gMnPc3	rybníkář
dařilo	dařit	k5eAaImAgNnS	dařit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současníků	současník	k1gMnPc2	současník
byl	být	k5eAaImAgMnS	být
Vilém	Vilém	k1gMnSc1	Vilém
člověkem	člověk	k1gMnSc7	člověk
skromným	skromný	k2eAgInSc7d1	skromný
a	a	k8xC	a
pokorným	pokorný	k2eAgInSc7d1	pokorný
<g/>
,	,	kIx,	,
s	s	k7c7	s
neobyčejnými	obyčejný	k2eNgFnPc7d1	neobyčejná
diplomatickými	diplomatický	k2eAgFnPc7d1	diplomatická
schopnostmi	schopnost	k1gFnPc7	schopnost
a	a	k8xC	a
vyvinutým	vyvinutý	k2eAgNnSc7d1	vyvinuté
ekonomickým	ekonomický	k2eAgNnSc7d1	ekonomické
myšlením	myšlení	k1gNnSc7	myšlení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
odráželo	odrážet	k5eAaImAgNnS	odrážet
v	v	k7c6	v
úspěšných	úspěšný	k2eAgFnPc6d1	úspěšná
investicích	investice	k1gFnPc6	investice
<g/>
.	.	kIx.	.
</s>
<s>
Nepotrpěl	potrpět	k5eNaPmAgMnS	potrpět
si	se	k3xPyFc3	se
prý	prý	k9	prý
na	na	k7c4	na
přepych	přepych	k1gInSc4	přepych
<g/>
,	,	kIx,	,
považoval	považovat	k5eAaImAgMnS	považovat
jej	on	k3xPp3gMnSc4	on
za	za	k7c4	za
zbytečnou	zbytečný	k2eAgFnSc4d1	zbytečná
ztrátu	ztráta	k1gFnSc4	ztráta
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Johankou	Johanka	k1gFnSc7	Johanka
z	z	k7c2	z
Liblic	Liblice	k1gFnPc2	Liblice
(	(	kIx(	(
<g/>
†	†	k?	†
<g/>
1515	[number]	k4	1515
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
3	[number]	k4	3
dětí	dítě	k1gFnPc2	dítě
<g/>
:	:	kIx,	:
Bohunky	Bohunka	k1gFnSc2	Bohunka
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc2	Jan
a	a	k8xC	a
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
Pardubický	pardubický	k2eAgInSc1d1	pardubický
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
nechal	nechat	k5eAaPmAgMnS	nechat
zbudovat	zbudovat	k5eAaPmF	zbudovat
přestavbou	přestavba	k1gFnSc7	přestavba
původního	původní	k2eAgInSc2d1	původní
gotického	gotický	k2eAgInSc2d1	gotický
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
nové	nový	k2eAgNnSc4d1	nové
sídlo	sídlo	k1gNnSc4	sídlo
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
,	,	kIx,	,
koupil	koupit	k5eAaPmAgMnS	koupit
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
<g/>
,	,	kIx,	,
blížil	blížit	k5eAaImAgInS	blížit
se	se	k3xPyFc4	se
50	[number]	k4	50
letům	let	k1gInPc3	let
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
zde	zde	k6eAd1	zde
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
krále	král	k1gMnSc2	král
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
zastával	zastávat	k5eAaImAgMnS	zastávat
úřad	úřad	k1gInSc4	úřad
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
hofmistra	hofmistr	k1gMnSc2	hofmistr
českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1523	[number]	k4	1523
však	však	k9	však
Ludvík	Ludvík	k1gMnSc1	Ludvík
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
(	(	kIx(	(
<g/>
†	†	k?	†
<g/>
1526	[number]	k4	1526
<g/>
)	)	kIx)	)
sesadil	sesadit	k5eAaPmAgMnS	sesadit
zemské	zemský	k2eAgMnPc4d1	zemský
úředníky	úředník	k1gMnPc4	úředník
a	a	k8xC	a
spravoval	spravovat	k5eAaImAgMnS	spravovat
zemi	zem	k1gFnSc4	zem
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
králově	králův	k2eAgFnSc3d1	králova
zvůli	zvůle	k1gFnSc3	zvůle
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Vynucenou	vynucený	k2eAgFnSc4d1	vynucená
rezignaci	rezignace	k1gFnSc4	rezignace
bral	brát	k5eAaImAgMnS	brát
jako	jako	k9	jako
urážku	urážka	k1gFnSc4	urážka
a	a	k8xC	a
úřad	úřad	k1gInSc4	úřad
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
nabízen	nabízen	k2eAgMnSc1d1	nabízen
<g/>
.	.	kIx.	.
</s>
<s>
Přijal	přijmout	k5eAaPmAgMnS	přijmout
jej	on	k3xPp3gMnSc4	on
až	až	k9	až
od	od	k7c2	od
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
O	o	k7c6	o
Vojtěchovi	Vojtěch	k1gMnSc6	Vojtěch
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
i	i	k9	i
jako	jako	k9	jako
o	o	k7c6	o
možném	možný	k2eAgMnSc6d1	možný
kandidátovi	kandidát	k1gMnSc6	kandidát
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Jagellonského	jagellonský	k2eAgInSc2d1	jagellonský
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Moháče	Moháč	k1gInSc2	Moháč
<g/>
.	.	kIx.	.
</s>
<s>
Naděje	naděje	k1gFnSc1	naděje
ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
minimální	minimální	k2eAgFnPc1d1	minimální
<g/>
.	.	kIx.	.
</s>
<s>
Náhlá	náhlý	k2eAgFnSc1d1	náhlá
Vojtěchova	Vojtěchův	k2eAgFnSc1d1	Vojtěchova
smrt	smrt	k1gFnSc1	smrt
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1534	[number]	k4	1534
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
předcházela	předcházet	k5eAaImAgFnS	předcházet
krátká	krátký	k2eAgFnSc1d1	krátká
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
záhadná	záhadný	k2eAgFnSc1d1	záhadná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejasné	jasný	k2eNgNnSc1d1	nejasné
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
takovým	takový	k3xDgInSc7	takový
spěchem	spěch	k1gInSc7	spěch
pohřben	pohřben	k2eAgMnSc1d1	pohřben
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
smrtí	smrt	k1gFnSc7	smrt
se	se	k3xPyFc4	se
připomíná	připomínat	k5eAaImIp3nS	připomínat
jedna	jeden	k4xCgFnSc1	jeden
událost	událost	k1gFnSc1	událost
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1534	[number]	k4	1534
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
jako	jako	k8xS	jako
předseda	předseda	k1gMnSc1	předseda
komorního	komorní	k2eAgInSc2d1	komorní
soudu	soud	k1gInSc2	soud
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
umoření	umoření	k1gNnSc2	umoření
hladem	hlad	k1gInSc7	hlad
v	v	k7c6	v
Mihulce	mihulka	k1gFnSc6	mihulka
<g/>
,	,	kIx,	,
věži	věž	k1gFnSc6	věž
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
sadistickou	sadistický	k2eAgFnSc4d1	sadistická
vražedkyni	vražedkyně	k1gFnSc4	vražedkyně
Kateřinu	Kateřina	k1gFnSc4	Kateřina
Bechyňovou	Bechyňová	k1gFnSc4	Bechyňová
z	z	k7c2	z
Lažan	Lažana	k1gFnPc2	Lažana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
postupně	postupně	k6eAd1	postupně
zavraždila	zavraždit	k5eAaPmAgFnS	zavraždit
čtrnáct	čtrnáct	k4xCc4	čtrnáct
svých	svůj	k3xOyFgFnPc2	svůj
služebnic	služebnice	k1gFnPc2	služebnice
<g/>
.	.	kIx.	.
</s>
<s>
Vražedkyně	vražedkyně	k1gFnSc1	vražedkyně
jej	on	k3xPp3gMnSc4	on
po	po	k7c6	po
vynesení	vynesení	k1gNnSc6	vynesení
rozsudku	rozsudek	k1gInSc2	rozsudek
proklela	proklít	k5eAaPmAgFnS	proklít
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
ochuravěl	ochuravět	k5eAaPmAgMnS	ochuravět
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rodového	rodový	k2eAgNnSc2d1	rodové
ujednání	ujednání	k1gNnSc2	ujednání
připadly	připadnout	k5eAaPmAgInP	připadnout
Vojtěchovy	Vojtěchův	k2eAgInPc1d1	Vojtěchův
statky	statek	k1gInPc1	statek
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
novoměstské	novoměstský	k2eAgNnSc1d1	Novoměstské
panství	panství	k1gNnSc1	panství
jeho	jeho	k3xOp3gFnSc2	jeho
bratru	bratru	k9	bratru
Janovi	Jan	k1gMnSc3	Jan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
Bohatý	bohatý	k2eAgMnSc1d1	bohatý
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
původně	původně	k6eAd1	původně
spravoval	spravovat	k5eAaImAgMnS	spravovat
moravské	moravský	k2eAgInPc4d1	moravský
statky	statek	k1gInPc4	statek
a	a	k8xC	a
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
hlavního	hlavní	k2eAgMnSc2d1	hlavní
dědice	dědic	k1gMnSc2	dědic
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
ještě	ještě	k9	ještě
české	český	k2eAgInPc1d1	český
<g/>
,	,	kIx,	,
vládl	vládnout	k5eAaImAgMnS	vládnout
nakonec	nakonec	k6eAd1	nakonec
celému	celý	k2eAgInSc3d1	celý
pernštejnskému	pernštejnský	k2eAgInSc3d1	pernštejnský
majetku	majetek	k1gInSc3	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
vedl	vést	k5eAaImAgInS	vést
moravskou	moravský	k2eAgFnSc4d1	Moravská
zemskou	zemský	k2eAgFnSc4d1	zemská
hotovost	hotovost	k1gFnSc4	hotovost
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
králi	král	k1gMnSc3	král
Ludvíku	Ludvík	k1gMnSc3	Ludvík
Jagellonskému	jagellonský	k2eAgMnSc3d1	jagellonský
proti	proti	k7c3	proti
Turkům	Turek	k1gMnPc3	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
panství	panství	k1gNnPc4	panství
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
postavit	postavit	k5eAaPmF	postavit
velmi	velmi	k6eAd1	velmi
početné	početný	k2eAgNnSc1d1	početné
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
opoziční	opoziční	k2eAgFnSc6d1	opoziční
činnosti	činnost	k1gFnSc6	činnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
namířena	namířen	k2eAgFnSc1d1	namířena
proti	proti	k7c3	proti
centralizačnímu	centralizační	k2eAgNnSc3d1	centralizační
úsilí	úsilí	k1gNnSc3	úsilí
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
V	v	k7c6	v
době	doba	k1gFnSc6	doba
protihabsburského	protihabsburský	k2eAgNnSc2d1	protihabsburské
povstání	povstání	k1gNnSc2	povstání
roku	rok	k1gInSc2	rok
1547	[number]	k4	1547
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
defenzorem	defenzor	k1gMnSc7	defenzor
novoutrakvistů	novoutrakvista	k1gMnPc2	novoutrakvista
a	a	k8xC	a
konzistoře	konzistoř	k1gFnSc2	konzistoř
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
držel	držet	k5eAaImAgMnS	držet
v	v	k7c6	v
zástavě	zástava	k1gFnSc6	zástava
kladské	kladský	k2eAgNnSc4d1	Kladské
hrabství	hrabství	k1gNnSc4	hrabství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgInS	dát
do	do	k7c2	do
zástavy	zástava	k1gFnSc2	zástava
roku	rok	k1gInSc2	rok
1537	[number]	k4	1537
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězné	vítězný	k2eAgFnSc6d1	vítězná
bitvě	bitva	k1gFnSc6	bitva
katolíků	katolík	k1gMnPc2	katolík
proti	proti	k7c3	proti
luteránům	luterán	k1gMnPc3	luterán
u	u	k7c2	u
Mühlberku	Mühlberk	k1gInSc2	Mühlberk
roku	rok	k1gInSc2	rok
1547	[number]	k4	1547
si	se	k3xPyFc3	se
pospíšil	pospíšit	k5eAaPmAgMnS	pospíšit
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	s	k7c7	s
králi	král	k1gMnPc7	král
vzdali	vzdát	k5eAaPmAgMnP	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
prozíravě	prozíravě	k6eAd1	prozíravě
vysokou	vysoký	k2eAgFnSc4d1	vysoká
šlechtu	šlechta	k1gFnSc4	šlechta
omilostnil	omilostnit	k5eAaPmAgMnS	omilostnit
<g/>
.	.	kIx.	.
</s>
<s>
Janovi	Jan	k1gMnSc3	Jan
žádné	žádný	k3yNgNnSc1	žádný
velké	velký	k2eAgNnSc1d1	velké
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
nehrozilo	hrozit	k5eNaImAgNnS	hrozit
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
před	před	k7c7	před
konfiskací	konfiskace	k1gFnSc7	konfiskace
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
jednal	jednat	k5eAaImAgMnS	jednat
s	s	k7c7	s
Wolfem	Wolf	k1gMnSc7	Wolf
Krajířem	Krajíř	k1gMnSc7	Krajíř
z	z	k7c2	z
Krajku	krajek	k1gInSc2	krajek
o	o	k7c6	o
prodeji	prodej	k1gInSc6	prodej
panství	panství	k1gNnSc1	panství
Stubenberkům	Stubenberka	k1gMnPc3	Stubenberka
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
byl	být	k5eAaImAgMnS	být
ještě	ještě	k9	ještě
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
nucen	nutit	k5eAaImNgMnS	nutit
narychlo	narychlo	k6eAd1	narychlo
prodávat	prodávat	k5eAaImF	prodávat
některá	některý	k3yIgNnPc4	některý
panství	panství	k1gNnPc4	panství
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
musel	muset	k5eAaImAgMnS	muset
splácet	splácet	k5eAaImF	splácet
své	svůj	k3xOyFgInPc4	svůj
dluhy	dluh	k1gInPc4	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
již	již	k9	již
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1548	[number]	k4	1548
a	a	k8xC	a
zanechal	zanechat	k5eAaPmAgMnS	zanechat
po	po	k7c6	po
sobě	se	k3xPyFc3	se
3	[number]	k4	3
syny	syn	k1gMnPc4	syn
<g/>
:	:	kIx,	:
Vratislava	Vratislav	k1gMnSc4	Vratislav
<g/>
,	,	kIx,	,
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
a	a	k8xC	a
Vojtěcha	Vojtěch	k1gMnSc4	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
popisován	popisován	k2eAgMnSc1d1	popisován
jako	jako	k8xS	jako
hýřilec	hýřilec	k1gMnSc1	hýřilec
a	a	k8xC	a
bankrotář	bankrotář	k1gMnSc1	bankrotář
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1560	[number]	k4	1560
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
syn	syn	k1gMnSc1	syn
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
se	se	k3xPyFc4	se
majetkově	majetkově	k6eAd1	majetkově
i	i	k9	i
ideologicky	ideologicky	k6eAd1	ideologicky
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
rodu	rod	k1gInSc2	rod
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
samostatné	samostatný	k2eAgFnSc2d1	samostatná
protestantské	protestantský	k2eAgFnSc2d1	protestantská
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
Jednoty	jednota	k1gFnSc2	jednota
bratrské	bratrský	k2eAgFnSc2d1	bratrská
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
je	být	k5eAaImIp3nS	být
také	také	k9	také
znám	znám	k2eAgMnSc1d1	znám
svou	svůj	k3xOyFgFnSc7	svůj
literární	literární	k2eAgFnSc7d1	literární
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1561	[number]	k4	1561
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
nejúspěšnější	úspěšný	k2eAgMnSc1d3	nejúspěšnější
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
bratrů	bratr	k1gMnPc2	bratr
byl	být	k5eAaImAgMnS	být
Vratislav	Vratislav	k1gMnSc1	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
zvaný	zvaný	k2eAgInSc4d1	zvaný
Nádherný	nádherný	k2eAgInSc4d1	nádherný
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
nesporně	sporně	k6eNd1	sporně
značné	značný	k2eAgNnSc4d1	značné
politické	politický	k2eAgNnSc4d1	politické
nadání	nadání	k1gNnSc4	nadání
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
už	už	k6eAd1	už
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
zastával	zastávat	k5eAaImAgMnS	zastávat
vysoké	vysoká	k1gFnPc4	vysoká
politické	politický	k2eAgFnSc2d1	politická
funkce	funkce	k1gFnSc2	funkce
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
pověřován	pověřovat	k5eAaImNgInS	pověřovat
důležitými	důležitý	k2eAgInPc7d1	důležitý
úkoly	úkol	k1gInPc7	úkol
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
krále	král	k1gMnSc2	král
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Vratislav	Vratislav	k1gMnSc1	Vratislav
Maxmiliána	Maxmilián	k1gMnSc4	Maxmilián
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
již	již	k6eAd1	již
jako	jako	k8xS	jako
osmnáctiletý	osmnáctiletý	k2eAgMnSc1d1	osmnáctiletý
mladík	mladík	k1gMnSc1	mladík
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
roku	rok	k1gInSc2	rok
1548	[number]	k4	1548
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1552	[number]	k4	1552
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
vyslán	vyslat	k5eAaPmNgInS	vyslat
na	na	k7c4	na
diplomatickou	diplomatický	k2eAgFnSc4d1	diplomatická
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
politické	politický	k2eAgFnPc4d1	politická
zásluhy	zásluha	k1gFnPc4	zásluha
roku	rok	k1gInSc2	rok
1556	[number]	k4	1556
odměněn	odměněn	k2eAgInSc1d1	odměněn
řádem	řád	k1gInSc7	řád
Zlatého	zlatý	k1gInSc2	zlatý
rouna	rouno	k1gNnSc2	rouno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1556	[number]	k4	1556
se	se	k3xPyFc4	se
Vratislav	Vratislav	k1gMnSc1	Vratislav
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
donou	dona	k1gFnSc7	dona
Marií	Maria	k1gFnPc2	Maria
Manrique	Manriqu	k1gMnSc2	Manriqu
de	de	k?	de
Lara	Larus	k1gMnSc2	Larus
<g/>
,	,	kIx,	,
Španělkou	Španělka	k1gFnSc7	Španělka
ze	z	k7c2	z
starého	starý	k2eAgInSc2d1	starý
rodu	rod	k1gInSc2	rod
Hurtadů	Hurtad	k1gInPc2	Hurtad
de	de	k?	de
Mendoza	Mendoz	k1gMnSc2	Mendoz
<g/>
,	,	kIx,	,
členkou	členka	k1gFnSc7	členka
doprovodu	doprovod	k1gInSc2	doprovod
královny	královna	k1gFnSc2	královna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
Maxmiliánovy	Maxmiliánův	k2eAgFnSc2d1	Maxmiliánova
manželky	manželka	k1gFnSc2	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgMnS	být
jediným	jediné	k1gNnSc7	jediné
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
na	na	k7c6	na
Maxmiliánově	Maxmiliánův	k2eAgInSc6d1	Maxmiliánův
dvoře	dvůr	k1gInSc6	dvůr
spojil	spojit	k5eAaPmAgMnS	spojit
svůj	svůj	k3xOyFgInSc4	svůj
rod	rod	k1gInSc4	rod
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
,	,	kIx,	,
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
tak	tak	k6eAd1	tak
učinil	učinit	k5eAaImAgMnS	učinit
i	i	k9	i
Adam	Adam	k1gMnSc1	Adam
z	z	k7c2	z
Ditrichštejna	Ditrichštejn	k1gInSc2	Ditrichštejn
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
dali	dát	k5eAaPmAgMnP	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
politické	politický	k2eAgFnSc3d1	politická
frakci	frakce	k1gFnSc3	frakce
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
španělskému	španělský	k2eAgInSc3d1	španělský
kroužku	kroužek	k1gInSc3	kroužek
<g/>
"	"	kIx"	"
na	na	k7c6	na
císařově	císařův	k2eAgInSc6d1	císařův
dvoře	dvůr	k1gInSc6	dvůr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
pochopitelně	pochopitelně	k6eAd1	pochopitelně
i	i	k9	i
za	za	k7c2	za
jeho	jeho	k3xOp3gMnSc2	jeho
následovníka	následovník	k1gMnSc2	následovník
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Oblékali	oblékat	k5eAaImAgMnP	oblékat
se	se	k3xPyFc4	se
po	po	k7c6	po
španělském	španělský	k2eAgInSc6d1	španělský
vzoru	vzor	k1gInSc6	vzor
(	(	kIx(	(
<g/>
krátké	krátký	k2eAgFnPc1d1	krátká
baňaté	baňatý	k2eAgFnPc1d1	baňatá
kalhoty	kalhoty	k1gFnPc1	kalhoty
"	"	kIx"	"
<g/>
bubny	buben	k1gInPc1	buben
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
skládaný	skládaný	k2eAgInSc1d1	skládaný
límec	límec	k1gInSc1	límec
"	"	kIx"	"
<g/>
gorguera	gorguera	k1gFnSc1	gorguera
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
boty	bota	k1gFnPc1	bota
"	"	kIx"	"
<g/>
kordovánky	kordovánky	k1gFnPc1	kordovánky
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
plášťový	plášťový	k2eAgInSc1d1	plášťový
přehoz	přehoz	k1gInSc1	přehoz
"	"	kIx"	"
<g/>
capa	capa	k1gFnSc1	capa
<g/>
"	"	kIx"	"
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
uváděli	uvádět	k5eAaImAgMnP	uvádět
do	do	k7c2	do
českého	český	k2eAgNnSc2d1	české
prostředí	prostředí	k1gNnSc2	prostředí
španělskou	španělský	k2eAgFnSc4d1	španělská
katolickou	katolický	k2eAgFnSc4d1	katolická
kulturu	kultura	k1gFnSc4	kultura
manýrismu	manýrismus	k1gInSc2	manýrismus
a	a	k8xC	a
raného	raný	k2eAgNnSc2d1	rané
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Vratislav	Vratislav	k1gMnSc1	Vratislav
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
měli	mít	k5eAaImAgMnP	mít
celkem	celkem	k6eAd1	celkem
20	[number]	k4	20
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k6eAd1	jen
7	[number]	k4	7
se	se	k3xPyFc4	se
dožilo	dožít	k5eAaPmAgNnS	dožít
dospělého	dospělý	k1gMnSc4	dospělý
věku	věk	k1gInSc2	věk
<g/>
:	:	kIx,	:
Johana	Johana	k1gFnSc1	Johana
<g/>
,	,	kIx,	,
Alběta	Alběta	k1gFnSc1	Alběta
Isabela	Isabela	k1gFnSc1	Isabela
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Františka	Františka	k1gFnSc1	Františka
<g/>
,	,	kIx,	,
Polyxena	Polyxena	k1gFnSc1	Polyxena
<g/>
,	,	kIx,	,
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
a	a	k8xC	a
Bibiana	Bibiana	k1gFnSc1	Bibiana
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznější	výrazný	k2eAgMnPc1d3	nejvýraznější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
Polyxena	Polyxena	k1gFnSc1	Polyxena
(	(	kIx(	(
<g/>
†	†	k?	†
<g/>
1642	[number]	k4	1642
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
nadaná	nadaný	k2eAgFnSc1d1	nadaná
schopná	schopný	k2eAgFnSc1d1	schopná
politička	politička	k1gFnSc1	politička
a	a	k8xC	a
hospodářka	hospodářka	k1gFnSc1	hospodářka
<g/>
,	,	kIx,	,
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
žena	žena	k1gFnSc1	žena
nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
celých	celý	k2eAgFnPc6d1	celá
českých	český	k2eAgFnPc6d1	Česká
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
Viléma	Vilém	k1gMnSc4	Vilém
z	z	k7c2	z
Rožmberka	Rožmberk	k1gInSc2	Rožmberk
<g/>
,	,	kIx,	,
manželství	manželství	k1gNnSc1	manželství
však	však	k9	však
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
bezdětné	bezdětný	k2eAgNnSc1d1	bezdětné
(	(	kIx(	(
<g/>
i	i	k9	i
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
vymření	vymření	k1gNnSc2	vymření
Rožmberků	Rožmberk	k1gInPc2	Rožmberk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Vilémově	Vilémův	k2eAgFnSc6d1	Vilémova
smrti	smrt	k1gFnSc6	smrt
(	(	kIx(	(
<g/>
1592	[number]	k4	1592
<g/>
)	)	kIx)	)
zůstala	zůstat	k5eAaPmAgFnS	zůstat
nějakou	nějaký	k3yIgFnSc4	nějaký
dobou	doba	k1gFnSc7	doba
vdovou	vdova	k1gFnSc7	vdova
<g/>
;	;	kIx,	;
žila	žít	k5eAaImAgFnS	žít
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Roudnici	Roudnice	k1gFnSc6	Roudnice
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
povedlo	povést	k5eAaPmAgNnS	povést
získat	získat	k5eAaPmF	získat
od	od	k7c2	od
nemocného	mocný	k2eNgMnSc2d1	nemocný
manžela	manžel	k1gMnSc2	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
získala	získat	k5eAaPmAgFnS	získat
do	do	k7c2	do
poručenství	poručenství	k1gNnSc2	poručenství
sirotky	sirotka	k1gFnSc2	sirotka
po	po	k7c6	po
bratru	bratr	k1gMnSc6	bratr
Janovi	Jan	k1gMnSc6	Jan
<g/>
,	,	kIx,	,
využila	využít	k5eAaPmAgFnS	využít
jejich	jejich	k3xOp3gInPc2	jejich
statků	statek	k1gInPc2	statek
k	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
své	svůj	k3xOyFgFnSc2	svůj
finanční	finanční	k2eAgFnSc2d1	finanční
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1603	[number]	k4	1603
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Popela	Popela	k1gMnSc1	Popela
z	z	k7c2	z
Lobkovic	Lobkovice	k1gInPc2	Lobkovice
<g/>
,	,	kIx,	,
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
kancléře	kancléř	k1gMnSc2	kancléř
království	království	k1gNnSc2	království
a	a	k8xC	a
Koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
oddaného	oddaný	k2eAgMnSc2d1	oddaný
katolíka	katolík	k1gMnSc2	katolík
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Spolu	spolu	k6eAd1	spolu
měli	mít	k5eAaImAgMnP	mít
jednoho	jeden	k4xCgMnSc4	jeden
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc4	Václav
Eusebia	Eusebius	k1gMnSc4	Eusebius
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Polyxena	Polyxena	k1gFnSc1	Polyxena
se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
hlavami	hlava	k1gFnPc7	hlava
místní	místní	k2eAgFnSc2d1	místní
španělské	španělský	k2eAgFnSc2d1	španělská
<g/>
/	/	kIx~	/
<g/>
habsburské	habsburský	k2eAgFnSc2d1	habsburská
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
početně	početně	k6eAd1	početně
sice	sice	k8xC	sice
malé	malý	k2eAgFnPc1d1	malá
(	(	kIx(	(
<g/>
katolíci	katolík	k1gMnPc1	katolík
tvořili	tvořit	k5eAaImAgMnP	tvořit
asi	asi	k9	asi
desetinu	desetina	k1gFnSc4	desetina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Koruny	koruna	k1gFnSc2	koruna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
semknuté	semknutý	k2eAgFnPc1d1	semknutá
a	a	k8xC	a
mocné	mocný	k2eAgFnPc1d1	mocná
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
měli	mít	k5eAaImAgMnP	mít
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
intrikách	intrika	k1gFnPc6	intrika
vedoucích	vedoucí	k2eAgFnPc6d1	vedoucí
nakonec	nakonec	k6eAd1	nakonec
k	k	k7c3	k
výbuchu	výbuch	k1gInSc3	výbuch
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
porážce	porážka	k1gFnSc3	porážka
České	český	k2eAgFnSc2d1	Česká
konfederace	konfederace	k1gFnSc2	konfederace
a	a	k8xC	a
následné	následný	k2eAgFnSc6d1	následná
rekatolizaci	rekatolizace	k1gFnSc6	rekatolizace
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Polyxena	Polyxena	k1gFnSc1	Polyxena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
i	i	k9	i
v	v	k7c6	v
manželství	manželství	k1gNnSc6	manželství
s	s	k7c7	s
Lobkovicem	Lobkovic	k1gMnSc7	Lobkovic
své	svůj	k3xOyFgInPc4	svůj
statky	statek	k1gInPc4	statek
nadále	nadále	k6eAd1	nadále
spravovala	spravovat	k5eAaImAgFnS	spravovat
sama	sám	k3xTgMnSc4	sám
<g/>
,	,	kIx,	,
využila	využít	k5eAaPmAgFnS	využít
pobělohorských	pobělohorský	k2eAgFnPc2d1	pobělohorská
konfiskací	konfiskace	k1gFnPc2	konfiskace
a	a	k8xC	a
záměrně	záměrně	k6eAd1	záměrně
vyvolaného	vyvolaný	k2eAgInSc2d1	vyvolaný
finančního	finanční	k2eAgInSc2d1	finanční
krachu	krach	k1gInSc2	krach
(	(	kIx(	(
<g/>
kalády	kaláda	k1gFnSc2	kaláda
<g/>
)	)	kIx)	)
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
rozmnožení	rozmnožení	k1gNnSc3	rozmnožení
svých	svůj	k3xOyFgInPc2	svůj
majetků	majetek	k1gInPc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
aktivně	aktivně	k6eAd1	aktivně
prosazovala	prosazovat	k5eAaImAgFnS	prosazovat
na	na	k7c6	na
svých	svůj	k3xOyFgNnPc6	svůj
panstvích	panství	k1gNnPc6	panství
nucené	nucený	k2eAgNnSc1d1	nucené
pokatoličtění	pokatoličtění	k1gNnSc1	pokatoličtění
<g/>
,	,	kIx,	,
potlačila	potlačit	k5eAaPmAgFnS	potlačit
povstání	povstání	k1gNnSc4	povstání
poddaných	poddaný	k1gMnPc2	poddaný
a	a	k8xC	a
povolala	povolat	k5eAaPmAgFnS	povolat
proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
jezuity	jezuita	k1gMnSc2	jezuita
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgFnPc7d1	významná
památkami	památka	k1gFnPc7	památka
na	na	k7c4	na
Polyxenu	Polyxena	k1gFnSc4	Polyxena
jsou	být	k5eAaImIp3nP	být
mj.	mj.	kA	mj.
kapucínský	kapucínský	k2eAgInSc4d1	kapucínský
klášter	klášter	k1gInSc4	klášter
v	v	k7c6	v
Roudnici	Roudnice	k1gFnSc6	Roudnice
a	a	k8xC	a
světoznámá	světoznámý	k2eAgFnSc1d1	světoznámá
soška	soška	k1gFnSc1	soška
Pražského	pražský	k2eAgNnSc2d1	Pražské
Jezulátka	Jezulátko	k1gNnSc2	Jezulátko
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
(	(	kIx(	(
<g/>
1587	[number]	k4	1587
<g/>
)	)	kIx)	)
s	s	k7c7	s
příbuznou	příbuzná	k1gFnSc7	příbuzná
<g/>
,	,	kIx,	,
Annou	Anna	k1gFnSc7	Anna
Marií	Maria	k1gFnPc2	Maria
Manrique	Manriqu	k1gInSc2	Manriqu
mladší	mladý	k2eAgFnSc1d2	mladší
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
se	se	k3xPyFc4	se
však	však	k9	však
potýkal	potýkat	k5eAaImAgMnS	potýkat
s	s	k7c7	s
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
dluhům	dluh	k1gInPc3	dluh
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
prodat	prodat	k5eAaPmF	prodat
svá	svůj	k3xOyFgNnPc4	svůj
panství	panství	k1gNnPc4	panství
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
mu	on	k3xPp3gMnSc3	on
zůstal	zůstat	k5eAaPmAgInS	zůstat
jen	jen	k9	jen
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
,	,	kIx,	,
Plumlov	Plumlov	k1gInSc1	Plumlov
a	a	k8xC	a
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
.	.	kIx.	.
</s>
<s>
Dal	dát	k5eAaPmAgMnS	dát
se	se	k3xPyFc4	se
na	na	k7c4	na
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stalo	stát	k5eAaPmAgNnS	stát
osudným	osudný	k2eAgMnSc7d1	osudný
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
až	až	k9	až
na	na	k7c4	na
post	post	k1gInSc4	post
císařského	císařský	k2eAgMnSc2d1	císařský
generála	generál	k1gMnSc2	generál
–	–	k?	–
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1597	[number]	k4	1597
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
pevnosti	pevnost	k1gFnSc2	pevnost
Rábu	Ráb	k1gInSc2	Ráb
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
dělová	dělový	k2eAgFnSc1d1	dělová
koule	koule	k1gFnSc1	koule
z	z	k7c2	z
tureckého	turecký	k2eAgNnSc2d1	turecké
děla	dělo	k1gNnSc2	dělo
utrhla	utrhnout	k5eAaPmAgFnS	utrhnout
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
levou	levý	k2eAgFnSc4d1	levá
ruku	ruka	k1gFnSc4	ruka
i	i	k9	i
s	s	k7c7	s
ramenem	rameno	k1gNnSc7	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
děti	dítě	k1gFnPc1	dítě
už	už	k6eAd1	už
žily	žít	k5eAaImAgFnP	žít
mimo	mimo	k7c4	mimo
Čechy	Čech	k1gMnPc4	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Johana	Johana	k1gFnSc1	Johana
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
ženou	žena	k1gFnSc7	žena
španělského	španělský	k2eAgMnSc2d1	španělský
šlechtice	šlechtic	k1gMnSc2	šlechtic
Fernanda	Fernando	k1gNnSc2	Fernando
de	de	k?	de
Aragón	Aragón	k1gMnSc1	Aragón
y	y	k?	y
de	de	k?	de
Borja	Borjus	k1gMnSc4	Borjus
<g/>
,	,	kIx,	,
pocházejího	pocházejí	k1gMnSc4	pocházejí
z	z	k7c2	z
levobočné	levobočný	k2eAgFnSc2d1	levobočný
větve	větev	k1gFnSc2	větev
aragonských	aragonský	k2eAgMnPc2d1	aragonský
králů	král	k1gMnPc2	král
z	z	k7c2	z
trastamarské	trastamarský	k2eAgFnSc2d1	trastamarský
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
Isabela	Isabela	k1gFnSc1	Isabela
(	(	kIx(	(
<g/>
†	†	k?	†
<g/>
1610	[number]	k4	1610
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1578	[number]	k4	1578
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
švábského	švábský	k2eAgMnSc4d1	švábský
šlechtice	šlechtic	k1gMnSc4	šlechtic
Albrechta	Albrecht	k1gMnSc2	Albrecht
I.	I.	kA	I.
z	z	k7c2	z
Fürstenberka	Fürstenberka	k1gFnSc1	Fürstenberka
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
svého	svůj	k3xOyFgMnSc4	svůj
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
syna	syn	k1gMnSc4	syn
Kryštofa	Kryštof	k1gMnSc2	Kryštof
II	II	kA	II
<g/>
.	.	kIx.	.
je	on	k3xPp3gFnPc4	on
pramáti	pramáti	k1gFnPc4	pramáti
dodnes	dodnes	k6eAd1	dodnes
žijící	žijící	k2eAgFnPc1d1	žijící
větve	větev	k1gFnPc1	větev
Fürstenberků	Fürstenberka	k1gMnPc2	Fürstenberka
<g/>
.	.	kIx.	.
</s>
<s>
Bibiana	Bibiana	k1gFnSc1	Bibiana
(	(	kIx(	(
<g/>
†	†	k?	†
<g/>
1616	[number]	k4	1616
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
vzala	vzít	k5eAaPmAgFnS	vzít
lombardského	lombardský	k2eAgMnSc4d1	lombardský
šlechtice	šlechtic	k1gMnSc4	šlechtic
Francesca	Francescus	k1gMnSc4	Francescus
Gonzagu	Gonzag	k1gInSc2	Gonzag
di	di	k?	di
Castiglione	Castiglion	k1gInSc5	Castiglion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
zanechal	zanechat	k5eAaPmAgMnS	zanechat
Jan	Jan	k1gMnSc1	Jan
jednoho	jeden	k4xCgMnSc4	jeden
synka	synek	k1gMnSc4	synek
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
dcery	dcera	k1gFnPc4	dcera
<g/>
,	,	kIx,	,
Annu	Anna	k1gFnSc4	Anna
<g/>
,	,	kIx,	,
Frebonii	Frebonie	k1gFnSc4	Frebonie
Eufebii	Eufebie	k1gFnSc4	Eufebie
a	a	k8xC	a
novorozenou	novorozený	k2eAgFnSc4d1	novorozená
Evu	Eva	k1gFnSc4	Eva
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
dětském	dětský	k2eAgInSc6d1	dětský
věku	věk	k1gInSc6	věk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Vratislav	Vratislav	k1gMnSc1	Vratislav
Eusebius	Eusebius	k1gMnSc1	Eusebius
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
posledním	poslední	k2eAgMnSc7d1	poslední
potomkem	potomek	k1gMnSc7	potomek
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
neoženil	oženit	k5eNaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
otci	otec	k1gMnSc6	otec
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
kariéru	kariéra	k1gFnSc4	kariéra
vojáka	voják	k1gMnSc2	voják
<g/>
,	,	kIx,	,
sloužil	sloužit	k5eAaImAgMnS	sloužit
pod	pod	k7c7	pod
Valdštejnem	Valdštejn	k1gMnSc7	Valdštejn
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
zabit	zabit	k2eAgInSc1d1	zabit
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
ze	z	k7c2	z
šarvátek	šarvátka	k1gFnPc2	šarvátka
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Švédům	Švéd	k1gMnPc3	Švéd
u	u	k7c2	u
braniborského	braniborský	k2eAgNnSc2d1	braniborské
města	město	k1gNnSc2	město
Tangermünde	Tangermünd	k1gMnSc5	Tangermünd
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
smrtí	smrt	k1gFnSc7	smrt
vymřel	vymřít	k5eAaPmAgInS	vymřít
rod	rod	k1gInSc1	rod
Pernštejnů	Pernštejn	k1gInPc2	Pernštejn
roku	rok	k1gInSc2	rok
1631	[number]	k4	1631
po	po	k7c6	po
meči	meč	k1gInSc6	meč
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Janovy	Janův	k2eAgFnSc2d1	Janova
dcery	dcera	k1gFnSc2	dcera
Frebonie	Frebonie	k1gFnSc2	Frebonie
roku	rok	k1gInSc2	rok
1646	[number]	k4	1646
přechází	přecházet	k5eAaImIp3nS	přecházet
pernštejnský	pernštejnský	k2eAgInSc4d1	pernštejnský
majetek	majetek	k1gInSc4	majetek
na	na	k7c4	na
potomky	potomek	k1gMnPc4	potomek
Polyxeny	Polyxena	k1gFnSc2	Polyxena
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
na	na	k7c4	na
knížata	kníže	k1gNnPc4	kníže
z	z	k7c2	z
Lobkovic	Lobkovice	k1gInPc2	Lobkovice
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
bohatstvím	bohatství	k1gNnSc7	bohatství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
takto	takto	k6eAd1	takto
získali	získat	k5eAaPmAgMnP	získat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
Lobkovicové	Lobkovicový	k2eAgNnSc1d1	Lobkovicové
následně	následně	k6eAd1	následně
vyšvihnout	vyšvihnout	k5eAaPmF	vyšvihnout
mezi	mezi	k7c4	mezi
významnou	významný	k2eAgFnSc4d1	významná
pobělohorskou	pobělohorský	k2eAgFnSc4d1	pobělohorská
šlechtu	šlechta	k1gFnSc4	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Kdysi	kdysi	k6eAd1	kdysi
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
moravském	moravský	k2eAgInSc6d1	moravský
hvozdu	hvozd	k1gInSc6	hvozd
uhlíř	uhlíř	k1gMnSc1	uhlíř
Věňava	Věňava	k1gFnSc1	Věňava
(	(	kIx(	(
<g/>
Vaněk	Vaněk	k1gMnSc1	Vaněk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
se	se	k3xPyFc4	se
vracel	vracet	k5eAaImAgInS	vracet
večer	večer	k1gInSc1	večer
hladový	hladový	k2eAgInSc1d1	hladový
z	z	k7c2	z
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
dorazil	dorazit	k5eAaPmAgMnS	dorazit
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
obydlí	obydlí	k1gNnSc3	obydlí
<g/>
,	,	kIx,	,
spatřil	spatřit	k5eAaPmAgMnS	spatřit
vylomené	vylomený	k2eAgFnPc4d1	vylomená
dveře	dveře	k1gFnPc4	dveře
a	a	k8xC	a
uvnitř	uvnitř	k6eAd1	uvnitř
jen	jen	k9	jen
drobky	drobek	k1gInPc1	drobek
chleba	chléb	k1gInSc2	chléb
na	na	k7c6	na
stole	stol	k1gInSc6	stol
a	a	k8xC	a
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
opakovalo	opakovat	k5eAaImAgNnS	opakovat
i	i	k9	i
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
Věňava	Věňava	k1gFnSc1	Věňava
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
ráno	ráno	k6eAd1	ráno
do	do	k7c2	do
lesa	les	k1gInSc2	les
nepůjde	jít	k5eNaImIp3nS	jít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skryje	skrýt	k5eAaPmIp3nS	skrýt
se	se	k3xPyFc4	se
za	za	k7c7	za
chalupou	chalupa	k1gFnSc7	chalupa
a	a	k8xC	a
na	na	k7c4	na
zloděje	zloděj	k1gMnSc4	zloděj
si	se	k3xPyFc3	se
počíhá	počíhat	k5eAaPmIp3nS	počíhat
<g/>
.	.	kIx.	.
</s>
<s>
Netrvalo	trvat	k5eNaImAgNnS	trvat
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
zaslechl	zaslechnout	k5eAaPmAgMnS	zaslechnout
dupání	dupání	k1gNnSc4	dupání
a	a	k8xC	a
funění	funění	k1gNnSc4	funění
–	–	k?	–
k	k	k7c3	k
chalupě	chalupa	k1gFnSc3	chalupa
přišel	přijít	k5eAaPmAgInS	přijít
z	z	k7c2	z
lesa	les	k1gInSc2	les
zubr	zubr	k1gMnSc1	zubr
<g/>
!	!	kIx.	!
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
uhlíř	uhlíř	k1gMnSc1	uhlíř
nadál	nadát	k5eAaBmAgMnS	nadát
<g/>
,	,	kIx,	,
zubr	zubr	k1gMnSc1	zubr
rohy	roh	k1gInPc4	roh
vylomil	vylomit	k5eAaPmAgMnS	vylomit
dveře	dveře	k1gFnPc4	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
si	se	k3xPyFc3	se
zubr	zubr	k1gMnSc1	zubr
pochutnával	pochutnávat	k5eAaImAgMnS	pochutnávat
v	v	k7c6	v
chýši	chýš	k1gFnSc6	chýš
na	na	k7c6	na
uhlířově	uhlířův	k2eAgInSc6d1	uhlířův
chlebu	chléb	k1gInSc6	chléb
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
přiblížil	přiblížit	k5eAaPmAgInS	přiblížit
potichu	potichu	k6eAd1	potichu
k	k	k7c3	k
chalupě	chalupa	k1gFnSc3	chalupa
a	a	k8xC	a
zastoupil	zastoupit	k5eAaPmAgMnS	zastoupit
zubrovi	zubr	k1gMnSc3	zubr
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chalupě	chalupa	k1gFnSc6	chalupa
bylo	být	k5eAaImAgNnS	být
málo	málo	k1gNnSc1	málo
místa	místo	k1gNnSc2	místo
a	a	k8xC	a
zubr	zubr	k1gMnSc1	zubr
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nemohl	moct	k5eNaImAgMnS	moct
bránit	bránit	k5eAaImF	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
Věnava	Věnava	k1gFnSc1	Věnava
využil	využít	k5eAaPmAgMnS	využít
<g/>
,	,	kIx,	,
lapil	lapit	k5eAaPmAgMnS	lapit
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
vytrhl	vytrhnout	k5eAaPmAgMnS	vytrhnout
ze	z	k7c2	z
dveří	dveře	k1gFnPc2	dveře
houžev	houžev	k1gFnSc1	houžev
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
mu	on	k3xPp3gMnSc3	on
ji	on	k3xPp3gFnSc4	on
protáhl	protáhnout	k5eAaPmAgInS	protáhnout
nozdrami	nozdra	k1gFnPc7	nozdra
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
ho	on	k3xPp3gMnSc4	on
napadlo	napadnout	k5eAaPmAgNnS	napadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohutné	mohutný	k2eAgNnSc1d1	mohutné
zvíře	zvíře	k1gNnSc1	zvíře
dovede	dovést	k5eAaPmIp3nS	dovést
ke	k	k7c3	k
králi	král	k1gMnSc3	král
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
hrad	hrad	k1gInSc1	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
před	před	k7c7	před
králem	král	k1gMnSc7	král
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
z	z	k7c2	z
opasku	opasek	k1gInSc2	opasek
sekeru	sekera	k1gFnSc4	sekera
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
ranou	rána	k1gFnSc7	rána
uťal	utít	k5eAaPmAgMnS	utít
zubrovi	zubr	k1gMnSc3	zubr
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
panovník	panovník	k1gMnSc1	panovník
udatným	udatný	k2eAgInSc7d1	udatný
činem	čin	k1gInSc7	čin
tak	tak	k9	tak
ohromen	ohromen	k2eAgMnSc1d1	ohromen
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
splnit	splnit	k5eAaPmF	splnit
přání	přání	k1gNnSc4	přání
<g/>
.	.	kIx.	.
</s>
<s>
Věňava	Věňava	k1gFnSc1	Věňava
si	se	k3xPyFc3	se
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
silácký	silácký	k2eAgInSc4d1	silácký
kousek	kousek	k1gInSc4	kousek
vyžádal	vyžádat	k5eAaPmAgInS	vyžádat
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
kus	kus	k1gInSc4	kus
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
se	se	k3xPyFc4	se
z	z	k7c2	z
poddaného	poddaný	k1gMnSc2	poddaný
uhlíře	uhlíř	k1gMnSc2	uhlíř
stal	stát	k5eAaPmAgMnS	stát
svobodný	svobodný	k2eAgMnSc1d1	svobodný
pán	pán	k1gMnSc1	pán
a	a	k8xC	a
král	král	k1gMnSc1	král
mu	on	k3xPp3gMnSc3	on
udělil	udělit	k5eAaPmAgMnS	udělit
i	i	k9	i
právo	právo	k1gNnSc4	právo
užívat	užívat	k5eAaImF	užívat
v	v	k7c6	v
erbu	erb	k1gInSc6	erb
zubří	zubří	k2eAgFnSc4d1	zubří
hlavu	hlava	k1gFnSc4	hlava
s	s	k7c7	s
houžví	houžev	k1gFnSc7	houžev
v	v	k7c6	v
nozdrách	nozdra	k1gFnPc6	nozdra
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nejstarší	starý	k2eAgFnSc2d3	nejstarší
historie	historie	k1gFnSc2	historie
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
pernštejnské	pernštejnský	k2eAgFnSc6d1	pernštejnská
Bílé	bílý	k2eAgFnSc6d1	bílá
paní	paní	k1gFnSc6	paní
<g/>
.	.	kIx.	.
</s>
<s>
Pernštejnka	Pernštejnka	k6eAd1	Pernštejnka
Adléta	Adléta	k1gFnSc1	Adléta
byla	být	k5eAaImAgFnS	být
vyslána	vyslat	k5eAaPmNgFnS	vyslat
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
Veveří	veveří	k2eAgInSc4d1	veveří
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
najmout	najmout	k5eAaPmF	najmout
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
blíže	blíž	k1gFnSc2	blíž
neurčených	určený	k2eNgFnPc2d1	neurčená
okolností	okolnost	k1gFnPc2	okolnost
na	na	k7c6	na
hrádku	hrádek	k1gInSc6	hrádek
otěhotněla	otěhotnět	k5eAaPmAgFnS	otěhotnět
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
takovéto	takovýto	k3xDgNnSc1	takovýto
těhotenství	těhotenství	k1gNnSc1	těhotenství
bylo	být	k5eAaImAgNnS	být
vskutku	vskutku	k9	vskutku
velmi	velmi	k6eAd1	velmi
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
<g/>
,	,	kIx,	,
snažila	snažit	k5eAaImAgFnS	snažit
se	se	k3xPyFc4	se
vše	všechen	k3xTgNnSc4	všechen
zapřít	zapřít	k5eAaPmF	zapřít
a	a	k8xC	a
křivě	křivě	k6eAd1	křivě
se	se	k3xPyFc4	se
zapřisáhla	zapřisáhnout	k5eAaPmAgFnS	zapřisáhnout
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pane	Pan	k1gMnSc5	Pan
Bože	bůh	k1gMnSc5	bůh
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
břichatá	břichatý	k2eAgFnSc1d1	břichatá
<g/>
,	,	kIx,	,
bodejť	bodejť	k9	bodejť
bych	by	kYmCp1nS	by
se	se	k3xPyFc4	se
hned	hned	k6eAd1	hned
propadla	propadnout	k5eAaPmAgFnS	propadnout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
také	také	k9	také
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
lze	lze	k6eAd1	lze
Adlétu	Adléta	k1gFnSc4	Adléta
potkat	potkat	k5eAaPmF	potkat
jako	jako	k8xC	jako
hradní	hradní	k2eAgNnSc1d1	hradní
strašidlo	strašidlo	k1gNnSc1	strašidlo
nikoli	nikoli	k9	nikoli
na	na	k7c4	na
Veveří	veveří	k2eAgNnSc4d1	veveří
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
Pernštejně	Pernštejn	k1gInSc6	Pernštejn
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
bílá	bílý	k2eAgFnSc1d1	bílá
paní	paní	k1gFnSc1	paní
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
působit	působit	k5eAaImF	působit
na	na	k7c6	na
domovském	domovský	k2eAgInSc6d1	domovský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yRnSc1	kdo
ji	on	k3xPp3gFnSc4	on
potká	potkat	k5eAaPmIp3nS	potkat
bíle	bíle	k6eAd1	bíle
oděnou	oděný	k2eAgFnSc4d1	oděná
<g/>
,	,	kIx,	,
tomu	ten	k3xDgNnSc3	ten
přinese	přinést	k5eAaPmIp3nS	přinést
štěstí	štěstí	k1gNnSc1	štěstí
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
narození	narození	k1gNnSc2	narození
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
šat	šat	k1gInSc1	šat
černý	černý	k2eAgInSc1d1	černý
<g/>
,	,	kIx,	,
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Spojili	spojit	k5eAaPmAgMnP	spojit
se	se	k3xPyFc4	se
s	s	k7c7	s
Rožmberky	Rožmberk	k1gMnPc7	Rožmberk
<g/>
,	,	kIx,	,
Fürstenberky	Fürstenberk	k1gInPc7	Fürstenberk
<g/>
,	,	kIx,	,
Vartemberky	Vartemberk	k1gInPc7	Vartemberk
<g/>
,	,	kIx,	,
pány	pan	k1gMnPc7	pan
z	z	k7c2	z
Lipé	Lipé	k1gNnSc2	Lipé
<g/>
,	,	kIx,	,
pány	pan	k1gMnPc4	pan
z	z	k7c2	z
Házmburka	Házmburek	k1gMnSc2	Házmburek
<g/>
,	,	kIx,	,
Kostky	kostka	k1gFnSc2	kostka
z	z	k7c2	z
Postupic	Postupice	k1gFnPc2	Postupice
<g/>
,	,	kIx,	,
Vamberky	Vamberk	k1gInPc1	Vamberk
či	či	k8xC	či
Lobkovici	Lobkovici	k?	Lobkovici
<g/>
.	.	kIx.	.
</s>
