<p>
<s>
Přivrácená	přivrácený	k2eAgFnSc1d1	přivrácená
strana	strana	k1gFnSc1	strana
Měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
měsíční	měsíční	k2eAgFnSc1d1	měsíční
hemisféra	hemisféra	k1gFnSc1	hemisféra
(	(	kIx(	(
<g/>
polokoule	polokoule	k1gFnSc1	polokoule
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
trvale	trvale	k6eAd1	trvale
obrácena	obrátit	k5eAaPmNgFnS	obrátit
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Opačná	opačný	k2eAgFnSc1d1	opačná
polokoule	polokoule	k1gFnSc1	polokoule
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
odvrácená	odvrácený	k2eAgFnSc1d1	odvrácená
strana	strana	k1gFnSc1	strana
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
obrací	obracet	k5eAaImIp3nS	obracet
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
stále	stále	k6eAd1	stále
stejnou	stejný	k2eAgFnSc4d1	stejná
polokouli	polokoule	k1gFnSc4	polokoule
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
doba	doba	k1gFnSc1	doba
rotace	rotace	k1gFnSc2	rotace
Měsíce	měsíc	k1gInSc2	měsíc
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xS	jako
doba	doba	k1gFnSc1	doba
oběhu	oběh	k1gInSc2	oběh
Měsíce	měsíc	k1gInSc2	měsíc
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vázaná	vázaný	k2eAgFnSc1d1	vázaná
rotace	rotace	k1gFnSc1	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
osvětlení	osvětlení	k1gNnSc3	osvětlení
Sluncem	slunce	k1gNnSc7	slunce
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
velikost	velikost	k1gFnSc1	velikost
viditelné	viditelný	k2eAgFnSc2d1	viditelná
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
tzv.	tzv.	kA	tzv.
ke	k	k7c3	k
střídání	střídání	k1gNnSc3	střídání
měsíčních	měsíční	k2eAgFnPc2d1	měsíční
fází	fáze	k1gFnPc2	fáze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
Měsíce	měsíc	k1gInSc2	měsíc
eliptická	eliptický	k2eAgFnSc1d1	eliptická
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
libracím	librace	k1gFnPc3	librace
můžeme	moct	k5eAaImIp1nP	moct
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
pozorovat	pozorovat	k5eAaImF	pozorovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovinu	polovina	k1gFnSc4	polovina
jeho	jeho	k3xOp3gInSc2	jeho
povrchu	povrch	k1gInSc2	povrch
–	–	k?	–
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
cca	cca	kA	cca
59	[number]	k4	59
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
názvy	název	k1gInPc4	název
útvarů	útvar	k1gInPc2	útvar
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
latinské	latinský	k2eAgInPc1d1	latinský
názvy	název	k1gInPc1	název
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
existují	existovat	k5eAaImIp3nP	existovat
jejich	jejich	k3xOp3gInPc1	jejich
ekvivalenty	ekvivalent	k1gInPc1	ekvivalent
v	v	k7c6	v
národních	národní	k2eAgInPc6d1	národní
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Přivrácená	přivrácený	k2eAgFnSc1d1	přivrácená
strana	strana	k1gFnSc1	strana
Měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
pokryta	pokryt	k2eAgFnSc1d1	pokryta
světlými	světlý	k2eAgFnPc7d1	světlá
a	a	k8xC	a
tmavými	tmavý	k2eAgFnPc7d1	tmavá
oblastmi	oblast	k1gFnPc7	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
astronomové	astronom	k1gMnPc1	astronom
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tmavé	tmavý	k2eAgFnPc1d1	tmavá
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
pokryty	pokryt	k2eAgFnPc1d1	pokryta
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
tak	tak	k6eAd1	tak
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Světlé	světlý	k2eAgFnPc1d1	světlá
části	část	k1gFnPc1	část
měsíčního	měsíční	k2eAgInSc2d1	měsíční
povrchu	povrch	k1gInSc2	povrch
pak	pak	k6eAd1	pak
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
pevninami	pevnina	k1gFnPc7	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Měsíce	měsíc	k1gInSc2	měsíc
nenachází	nacházet	k5eNaImIp3nS	nacházet
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
oblastí	oblast	k1gFnPc2	oblast
(	(	kIx(	(
<g/>
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
mare	marat	k5eAaPmIp3nS	marat
<g/>
)	)	kIx)	)
jim	on	k3xPp3gMnPc3	on
zůstal	zůstat	k5eAaPmAgMnS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Měsíční	měsíční	k2eAgNnSc4d1	měsíční
moře	moře	k1gNnSc4	moře
jsou	být	k5eAaImIp3nP	být
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
impaktní	impaktní	k2eAgFnPc4d1	impaktní
pánve	pánev	k1gFnPc4	pánev
vyplněné	vyplněný	k2eAgFnPc4d1	vyplněná
měsíčními	měsíční	k2eAgInPc7d1	měsíční
bazalty	bazalt	k1gInPc7	bazalt
(	(	kIx(	(
<g/>
čediči	čedič	k1gInPc7	čedič
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
části	část	k1gFnSc6	část
Měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nachází	nacházet	k5eAaImIp3nS	nacházet
18	[number]	k4	18
měsíčních	měsíční	k2eAgNnPc2d1	měsíční
moří	moře	k1gNnPc2	moře
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
oceán	oceán	k1gInSc1	oceán
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
oceanus	oceanus	k1gInSc1	oceanus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
pozemských	pozemský	k2eAgNnPc2d1	pozemské
moří	moře	k1gNnPc2	moře
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
měsíční	měsíční	k2eAgNnSc4d1	měsíční
moře	moře	k1gNnSc4	moře
zálivy	záliv	k1gInPc1	záliv
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
sinus	sinus	k1gInSc1	sinus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ještě	ještě	k9	ještě
dvě	dva	k4xCgNnPc1	dva
jezera	jezero	k1gNnPc1	jezero
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
lacus	lacus	k1gInSc1	lacus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vůči	vůči	k7c3	vůči
odvrácené	odvrácený	k2eAgFnSc3d1	odvrácená
straně	strana	k1gFnSc3	strana
Měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
přivrácená	přivrácený	k2eAgFnSc1d1	přivrácená
polokoule	polokoule	k1gFnSc1	polokoule
bohatší	bohatý	k2eAgFnSc1d2	bohatší
na	na	k7c6	na
měsíční	měsíční	k2eAgFnSc6d1	měsíční
moře	mora	k1gFnSc6	mora
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
je	být	k5eAaImIp3nS	být
31,2	[number]	k4	31,2
%	%	kIx~	%
na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
polokouli	polokoule	k1gFnSc6	polokoule
ku	k	k7c3	k
2,6	[number]	k4	2,6
%	%	kIx~	%
na	na	k7c4	na
odvrácené	odvrácený	k2eAgInPc4d1	odvrácený
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
vyvýšených	vyvýšený	k2eAgFnPc2d1	vyvýšená
částí	část	k1gFnPc2	část
jsou	být	k5eAaImIp3nP	být
nejvýznamnější	významný	k2eAgNnPc4d3	nejvýznamnější
měsíční	měsíční	k2eAgNnPc4d1	měsíční
pohoří	pohoří	k1gNnSc4	pohoří
–	–	k?	–
Montes	Montes	k1gInSc1	Montes
Apenninus	Apenninus	k1gInSc1	Apenninus
(	(	kIx(	(
<g/>
Apeniny	Apeniny	k1gFnPc1	Apeniny
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Montes	Montes	k1gMnSc1	Montes
Caucasus	Caucasus	k1gMnSc1	Caucasus
(	(	kIx(	(
<g/>
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Montes	Montes	k1gMnSc1	Montes
Carpatus	Carpatus	k1gMnSc1	Carpatus
(	(	kIx(	(
<g/>
Karpaty	Karpaty	k1gInPc1	Karpaty
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Montes	Montes	k1gMnSc1	Montes
Jura	Jura	k1gMnSc1	Jura
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
nachází	nacházet	k5eAaImIp3nS	nacházet
údolí	údolí	k1gNnPc4	údolí
a	a	k8xC	a
vrcholy	vrchol	k1gInPc4	vrchol
<g/>
,	,	kIx,	,
objevil	objevit	k5eAaPmAgMnS	objevit
již	již	k6eAd1	již
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gMnSc6	Galile
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
měsíční	měsíční	k2eAgInSc1d1	měsíční
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
hladký	hladký	k2eAgInSc1d1	hladký
<g/>
.	.	kIx.	.
</s>
<s>
Měsíční	měsíční	k2eAgInSc1d1	měsíční
pohoří	pohořet	k5eAaPmIp3nS	pohořet
však	však	k9	však
nevznikla	vzniknout	k5eNaPmAgFnS	vzniknout
horotvornou	horotvorný	k2eAgFnSc7d1	horotvorná
činností	činnost	k1gFnSc7	činnost
jako	jako	k8xS	jako
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
okrajů	okraj	k1gInPc2	okraj
impaktních	impaktní	k2eAgFnPc2d1	impaktní
pánví	pánev	k1gFnPc2	pánev
nebo	nebo	k8xC	nebo
sesuvů	sesuv	k1gInPc2	sesuv
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
vzniku	vznik	k1gInSc6	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
měsíční	měsíční	k2eAgNnSc1d1	měsíční
pohoří	pohoří	k1gNnSc1	pohoří
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
měsíčních	měsíční	k2eAgFnPc2d1	měsíční
moří	moře	k1gNnPc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
výrazných	výrazný	k2eAgInPc2d1	výrazný
prvků	prvek	k1gInPc2	prvek
přivrácené	přivrácený	k2eAgFnSc2d1	přivrácená
strany	strana	k1gFnSc2	strana
Měsíce	měsíc	k1gInSc2	měsíc
jsou	být	k5eAaImIp3nP	být
krátery	kráter	k1gInPc1	kráter
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc7	jejich
paprsky	paprsek	k1gInPc7	paprsek
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
kráter	kráter	k1gInSc4	kráter
Tycho	Tyc	k1gMnSc4	Tyc
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
a	a	k8xC	a
kráter	kráter	k1gInSc4	kráter
Koperník	Koperník	k1gMnSc1	Koperník
poblíž	poblíž	k6eAd1	poblíž
středu	středa	k1gFnSc4	středa
měsíčního	měsíční	k2eAgInSc2d1	měsíční
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Unikátním	unikátní	k2eAgInSc7d1	unikátní
útvarem	útvar	k1gInSc7	útvar
přivrácené	přivrácený	k2eAgFnSc2d1	přivrácená
polokoule	polokoule	k1gFnSc2	polokoule
je	být	k5eAaImIp3nS	být
světlý	světlý	k2eAgInSc4d1	světlý
objekt	objekt	k1gInSc4	objekt
Reiner	Reinra	k1gFnPc2	Reinra
Gamma	Gamma	k1gNnSc1	Gamma
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Oceánu	oceán	k1gInSc2	oceán
bouří	bouř	k1gFnPc2	bouř
(	(	kIx(	(
<g/>
Oceanus	Oceanus	k1gInSc1	Oceanus
Procellarum	Procellarum	k1gInSc1	Procellarum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
RÜKL	RÜKL	kA	RÜKL
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85277	[number]	k4	85277
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
moří	moře	k1gNnPc2	moře
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
útvarech	útvar	k1gInPc6	útvar
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Měsíce	měsíc	k1gInSc2	měsíc
s	s	k7c7	s
radami	rada	k1gFnPc7	rada
k	k	k7c3	k
pozorování	pozorování	k1gNnSc3	pozorování
jeho	on	k3xPp3gInSc2	on
povrchu	povrch	k1gInSc2	povrch
</s>
</p>
<p>
<s>
Česká	český	k2eAgNnPc1d1	české
jména	jméno	k1gNnPc1	jméno
kráterů	kráter	k1gInPc2	kráter
na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
straně	strana	k1gFnSc6	strana
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Měsíční	měsíční	k2eAgInSc1d1	měsíční
atlas	atlas	k1gInSc1	atlas
</s>
</p>
<p>
<s>
Mapy	mapa	k1gFnPc1	mapa
Měsíce	měsíc	k1gInSc2	měsíc
</s>
</p>
<p>
<s>
Atlas	Atlas	k1gInSc1	Atlas
přivrácené	přivrácený	k2eAgFnSc2d1	přivrácená
strany	strana	k1gFnSc2	strana
Měsíce	měsíc	k1gInSc2	měsíc
volně	volně	k6eAd1	volně
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
</s>
</p>
<p>
<s>
geologický	geologický	k2eAgInSc1d1	geologický
atlas	atlas	k1gInSc1	atlas
Měsíce	měsíc	k1gInSc2	měsíc
</s>
</p>
