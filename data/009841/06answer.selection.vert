<s>
Měsíc	měsíc	k1gInSc1	měsíc
obrací	obracet	k5eAaImIp3nS	obracet
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
stále	stále	k6eAd1	stále
stejnou	stejný	k2eAgFnSc4d1	stejná
polokouli	polokoule	k1gFnSc4	polokoule
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
doba	doba	k1gFnSc1	doba
rotace	rotace	k1gFnSc2	rotace
Měsíce	měsíc	k1gInSc2	měsíc
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xS	jako
doba	doba	k1gFnSc1	doba
oběhu	oběh	k1gInSc2	oběh
Měsíce	měsíc	k1gInSc2	měsíc
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
