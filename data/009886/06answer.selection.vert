<s>
Záporná	záporný	k2eAgFnSc1d1	záporná
elektroda	elektroda	k1gFnSc1	elektroda
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
speciální	speciální	k2eAgFnSc7d1	speciální
kovovou	kovový	k2eAgFnSc7d1	kovová
slitinou	slitina	k1gFnSc7	slitina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
směs	směs	k1gFnSc1	směs
hydridů	hydrid	k1gInPc2	hydrid
neurčitého	určitý	k2eNgNnSc2d1	neurčité
složení	složení	k1gNnSc2	složení
<g/>
.	.	kIx.	.
</s>
