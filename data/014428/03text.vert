<s>
České	český	k2eAgFnPc1d1
souhlásky	souhláska	k1gFnPc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
výslovnost	výslovnost	k1gFnSc4
zapsanou	zapsaný	k2eAgFnSc7d1
znaky	znak	k1gInPc7
mezinárodní	mezinárodní	k2eAgFnSc2d1
fonetické	fonetický	k2eAgFnSc2d1
abecedy	abeceda	k1gFnSc2
IPA	IPA	kA
<g/>
.	.	kIx.
<g/>
Zobrazení	zobrazení	k1gNnSc4
některých	některý	k3yIgInPc2
znaků	znak	k1gInPc2
nemusí	muset	k5eNaImIp3nP
být	být	k5eAaImF
v	v	k7c6
některých	některý	k3yIgInPc6
prohlížečích	prohlížeč	k1gInPc6
korektní	korektní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
lingvistický	lingvistický	k2eAgInSc1d1
článek	článek	k1gInSc1
popisuje	popisovat	k5eAaImIp3nS
české	český	k2eAgFnPc4d1
souhlásky	souhláska	k1gFnPc4
z	z	k7c2
fonetického	fonetický	k2eAgNnSc2d1
a	a	k8xC
fonologického	fonologický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Čeština	čeština	k1gFnSc1
má	mít	k5eAaImIp3nS
celkem	celkem	k6eAd1
27	#num#	k4
souhláskových	souhláskový	k2eAgInPc2d1
fonémů	foném	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1
se	se	k3xPyFc4
tradičně	tradičně	k6eAd1
rozdělují	rozdělovat	k5eAaImIp3nP
podle	podle	k7c2
způsobu	způsob	k1gInSc2
a	a	k8xC
místa	místo	k1gNnSc2
tvoření	tvoření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
tzv.	tzv.	kA
pravých	pravý	k2eAgFnPc2d1
souhlásek	souhláska	k1gFnPc2
navíc	navíc	k6eAd1
existují	existovat	k5eAaImIp3nP
dvojice	dvojice	k1gFnPc1
lišící	lišící	k2eAgFnPc1d1
se	se	k3xPyFc4
svojí	svůj	k3xOyFgFnSc7
znělostí	znělost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Artikulace	artikulace	k1gFnSc1
</s>
<s>
Způsob	způsob	k1gInSc1
artikulace	artikulace	k1gFnSc2
</s>
<s>
Souhlásky	souhláska	k1gFnPc1
jsou	být	k5eAaImIp3nP
poměrně	poměrně	k6eAd1
heterogenní	heterogenní	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
zvuků	zvuk	k1gInPc2
<g/>
,	,	kIx,
existuje	existovat	k5eAaImIp3nS
mnoho	mnoho	k4c4
způsobů	způsob	k1gInPc2
jejich	jejich	k3xOp3gNnSc4
tvoření	tvoření	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společná	společný	k2eAgFnSc1d1
vlastnost	vlastnost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
odlišuje	odlišovat	k5eAaImIp3nS
souhlásky	souhláska	k1gFnPc4
od	od	k7c2
samohlásek	samohláska	k1gFnPc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnSc1
relativní	relativní	k2eAgFnSc1d1
zavřenost	zavřenost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
jejich	jejich	k3xOp3gFnSc6
artikulaci	artikulace	k1gFnSc6
jsou	být	k5eAaImIp3nP
ústa	ústa	k1gNnPc1
otevřena	otevřít	k5eAaPmNgNnP
méně	málo	k6eAd2
než	než	k8xS
u	u	k7c2
samohlásek	samohláska	k1gFnPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
vznikají	vznikat	k5eAaImIp3nP
překážky	překážka	k1gFnPc4
a	a	k8xC
turbulence	turbulence	k1gFnPc4
při	při	k7c6
proudění	proudění	k1gNnSc6
vzduchu	vzduch	k1gInSc2
–	–	k?
příčina	příčina	k1gFnSc1
vzniku	vznik	k1gInSc2
šumu	šum	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Všechny	všechen	k3xTgFnPc1
české	český	k2eAgFnPc1d1
souhlásky	souhláska	k1gFnPc1
jsou	být	k5eAaImIp3nP
tvořeny	tvořit	k5eAaImNgFnP
proudem	proud	k1gInSc7
vzduchu	vzduch	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
uváděn	uvádět	k5eAaImNgInS
do	do	k7c2
pohybu	pohyb	k1gInSc2
vytlačováním	vytlačování	k1gNnSc7
z	z	k7c2
plic	plíce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řadíme	řadit	k5eAaImIp1nP
je	on	k3xPp3gFnPc4
tedy	tedy	k9
mezi	mezi	k7c4
pulmonické	pulmonický	k2eAgFnPc4d1
egresivní	egresivní	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s>
Okluzivy	Okluziva	k1gFnPc1
</s>
<s>
Závěrové	závěrový	k2eAgFnPc1d1
souhlásky	souhláska	k1gFnPc1
(	(	kIx(
<g/>
okluzivy	okluziva	k1gFnPc1
<g/>
)	)	kIx)
vznikají	vznikat	k5eAaImIp3nP
přechodným	přechodný	k2eAgNnSc7d1
vytvořením	vytvoření	k1gNnSc7
závěru	závěr	k1gInSc2
(	(	kIx(
<g/>
okluze	okluze	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
brání	bránit	k5eAaImIp3nS
proudění	proudění	k1gNnSc1
vzduchu	vzduch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvolněním	uvolnění	k1gNnSc7
závěru	závěr	k1gInSc2
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
prudkému	prudký	k2eAgNnSc3d1
uvolnění	uvolnění	k1gNnSc3
přetlaku	přetlak	k1gInSc2
vzduchu	vzduch	k1gInSc2
a	a	k8xC
vzniká	vznikat	k5eAaImIp3nS
typický	typický	k2eAgInSc4d1
šum	šum	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
poslechového	poslechový	k2eAgNnSc2d1
(	(	kIx(
<g/>
akustického	akustický	k2eAgNnSc2d1
<g/>
)	)	kIx)
hlediska	hledisko	k1gNnSc2
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
ražené	ražený	k2eAgFnPc4d1
souhlásky	souhláska	k1gFnPc4
(	(	kIx(
<g/>
explozivy	exploziva	k1gFnPc4
<g/>
,	,	kIx,
často	často	k6eAd1
jen	jen	k9
plozivy	ploziva	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hláska	hláska	k1gFnSc1
</s>
<s>
IPA	IPA	kA
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Alofon	Alofon	k1gInSc1
</s>
<s>
základní	základní	k2eAgInSc1d1
</s>
<s>
znělostní	znělostní	k2eAgFnSc1d1
</s>
<s>
/	/	kIx~
<g/>
p	p	k?
<g/>
/	/	kIx~
</s>
<s>
/	/	kIx~
<g/>
p	p	k?
<g/>
/	/	kIx~
</s>
<s>
neznělá	znělý	k2eNgFnSc1d1
bilabiální	bilabiální	k2eAgFnSc1d1
ploziva	ploziva	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
p	p	k?
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
b	b	k?
<g/>
]	]	kIx)
</s>
<s>
/	/	kIx~
<g/>
b	b	k?
<g/>
/	/	kIx~
</s>
<s>
/	/	kIx~
<g/>
b	b	k?
<g/>
/	/	kIx~
</s>
<s>
znělá	znělý	k2eAgFnSc1d1
bilabiální	bilabiální	k2eAgFnSc1d1
ploziva	ploziva	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
b	b	k?
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
p	p	k?
<g/>
]	]	kIx)
</s>
<s>
/	/	kIx~
<g/>
t	t	k?
<g/>
/	/	kIx~
</s>
<s>
/	/	kIx~
<g/>
t	t	k?
<g/>
/	/	kIx~
</s>
<s>
neznělá	znělý	k2eNgFnSc1d1
alveolární	alveolární	k2eAgFnSc1d1
ploziva	ploziva	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
t	t	k?
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
d	d	k?
<g/>
]	]	kIx)
</s>
<s>
/	/	kIx~
<g/>
d	d	k?
<g/>
/	/	kIx~
</s>
<s>
/	/	kIx~
<g/>
d	d	k?
<g/>
/	/	kIx~
</s>
<s>
znělá	znělý	k2eAgFnSc1d1
alveolární	alveolární	k2eAgFnSc1d1
ploziva	ploziva	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
d	d	k?
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
t	t	k?
<g/>
]	]	kIx)
</s>
<s>
/	/	kIx~
<g/>
ť	ť	k?
<g/>
/	/	kIx~
</s>
<s>
/	/	kIx~
<g/>
c	c	k0
<g/>
/	/	kIx~
</s>
<s>
neznělá	znělý	k2eNgFnSc1d1
palatální	palatální	k2eAgFnSc1d1
ploziva	ploziva	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
c	c	k0
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
ɟ	ɟ	k?
<g/>
]	]	kIx)
</s>
<s>
/	/	kIx~
<g/>
ď	ď	k?
<g/>
/	/	kIx~
</s>
<s>
/	/	kIx~
<g/>
ɟ	ɟ	k?
<g/>
/	/	kIx~
</s>
<s>
znělá	znělý	k2eAgFnSc1d1
palatální	palatální	k2eAgFnSc1d1
ploziva	ploziva	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
ɟ	ɟ	k?
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
c	c	k0
<g/>
]	]	kIx)
</s>
<s>
/	/	kIx~
<g/>
k	k	k7c3
<g/>
/	/	kIx~
</s>
<s>
/	/	kIx~
<g/>
k	k	k7c3
<g/>
/	/	kIx~
</s>
<s>
neznělá	znělý	k2eNgFnSc1d1
velární	velární	k2eAgFnSc1d1
ploziva	ploziva	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
k	k	k7c3
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
g	g	kA
<g/>
]	]	kIx)
</s>
<s>
/	/	kIx~
<g/>
g	g	kA
<g/>
/	/	kIx~
1	#num#	k4
</s>
<s>
/	/	kIx~
<g/>
g	g	kA
<g/>
/	/	kIx~
</s>
<s>
znělá	znělý	k2eAgFnSc1d1
velární	velární	k2eAgFnSc1d1
ploziva	ploziva	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
g	g	kA
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
k	k	k7c3
<g/>
]	]	kIx)
</s>
<s>
neznělá	znělý	k2eNgFnSc1d1
glotální	glotální	k2eAgFnSc1d1
ploziva	ploziva	k1gFnSc1
(	(	kIx(
<g/>
ráz	ráz	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
[	[	kIx(
<g/>
ʔ	ʔ	k?
<g/>
]	]	kIx)
2	#num#	k4
</s>
<s>
1	#num#	k4
Jako	jako	k8xS,k8xC
samostatný	samostatný	k2eAgInSc1d1
foném	foném	k1gInSc1
se	s	k7c7
/	/	kIx~
<g/>
g	g	kA
<g/>
/	/	kIx~
vyskytuje	vyskytovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
přejatých	přejatý	k2eAgNnPc6d1
slovech	slovo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
domácích	domácí	k2eAgNnPc6d1
slovech	slovo	k1gNnPc6
je	být	k5eAaImIp3nS
vyslovované	vyslovovaný	k2eAgNnSc1d1
[	[	kIx(
<g/>
g	g	kA
<g/>
]	]	kIx)
znělou	znělý	k2eAgFnSc7d1
realizací	realizace	k1gFnSc7
/	/	kIx~
<g/>
k	k	k7c3
<g/>
/	/	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
praslovanské	praslovanský	k2eAgInPc1d1
/	/	kIx~
<g/>
g	g	kA
<g/>
/	/	kIx~
se	se	k3xPyFc4
v	v	k7c6
češtině	čeština	k1gFnSc6
během	během	k7c2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
změnilo	změnit	k5eAaPmAgNnS
v	v	k7c6
/	/	kIx~
<g/>
h	h	k?
<g/>
/	/	kIx~
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
Ráz	ráz	k1gInSc1
se	se	k3xPyFc4
nepovažuje	považovat	k5eNaImIp3nS
za	za	k7c4
samostatný	samostatný	k2eAgInSc4d1
foném	foném	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Použití	použití	k1gNnSc1
v	v	k7c6
češtině	čeština	k1gFnSc6
je	být	k5eAaImIp3nS
fakultativní	fakultativní	k2eAgNnSc1d1
(	(	kIx(
<g/>
nepovinné	povinný	k2eNgNnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výslovnost	výslovnost	k1gFnSc1
s	s	k7c7
rázem	ráz	k1gInSc7
nebo	nebo	k8xC
bez	bez	k7c2
rázu	ráz	k1gInSc2
nemění	měnit	k5eNaImIp3nS
význam	význam	k1gInSc1
slov	slovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ráz	ráz	k1gInSc1
nemá	mít	k5eNaImIp3nS
distinktivní	distinktivní	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Dříve	dříve	k6eAd2
formulovaná	formulovaný	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
spisovné	spisovný	k2eAgFnSc2d1
výslovnosti	výslovnost	k1gFnSc2
vyžadovala	vyžadovat	k5eAaImAgFnS
použití	použití	k1gNnSc4
rázu	ráz	k1gInSc2
vždy	vždy	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
slabika	slabika	k1gFnSc1
začínala	začínat	k5eAaImAgFnS
samohláskou	samohláska	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ráz	ráz	k1gInSc1
tak	tak	k6eAd1
vlastně	vlastně	k9
nahrazuje	nahrazovat	k5eAaImIp3nS
chybějící	chybějící	k2eAgFnSc4d1
slabičnou	slabičný	k2eAgFnSc4d1
preturu	pretura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
však	však	k9
ráz	ráz	k1gInSc4
ve	v	k7c6
výslovnosti	výslovnost	k1gFnSc6
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
méně	málo	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používání	používání	k1gNnSc1
rázu	ráz	k1gInSc2
je	být	k5eAaImIp3nS
ovlivněno	ovlivnit	k5eAaPmNgNnS
regionálně	regionálně	k6eAd1
<g/>
,	,	kIx,
obecně	obecně	k6eAd1
však	však	k9
lze	lze	k6eAd1
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
moravské	moravský	k2eAgFnSc6d1
výslovnosti	výslovnost	k1gFnSc6
se	se	k3xPyFc4
ráz	ráz	k1gInSc1
používá	používat	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
méně	málo	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
češtině	čeština	k1gFnSc6
plní	plnit	k5eAaImIp3nS
ráz	ráz	k1gInSc4
dvě	dva	k4xCgFnPc4
funkce	funkce	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
Vyznačení	vyznačení	k1gNnSc1
předělu	předěl	k1gInSc2
mezi	mezi	k7c7
slovy	slovo	k1gNnPc7
nebo	nebo	k8xC
uvniř	uvniř	k1gInSc4
složenin	složenina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vkládá	vkládat	k5eAaImIp3nS
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
mezi	mezi	k7c4
dvě	dva	k4xCgFnPc4
samohlásky	samohláska	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
spolu	spolu	k6eAd1
netvoří	tvořit	k5eNaImIp3nP
dvojhlásku	dvojhláska	k1gFnSc4
<g/>
,	,	kIx,
např.	např.	kA
používat	používat	k5eAaImF
[	[	kIx(
<g/>
po	po	k7c6
<g/>
.	.	kIx.
<g/>
ʔ	ʔ	k1gFnSc6
<g/>
:	:	kIx,
<g/>
vat	vat	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
táta	táta	k1gMnSc1
a	a	k8xC
máma	máma	k1gFnSc1
[	[	kIx(
<g/>
ta	ten	k3xDgFnSc1
<g/>
:	:	kIx,
<g/>
ta	ten	k3xDgFnSc1
ʔ	ʔ	k1gFnSc1
ma	ma	k?
<g/>
:	:	kIx,
<g/>
ma	ma	k?
<g/>
]	]	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
;	;	kIx,
u	u	k7c2
slov	slovo	k1gNnPc2
začínajících	začínající	k2eAgMnPc2d1
samohláskou	samohláska	k1gFnSc7
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
odlišení	odlišení	k1gNnSc3
předložky	předložka	k1gFnSc2
<g/>
,	,	kIx,
např.	např.	kA
z	z	k7c2
okna	okno	k1gNnSc2
[	[	kIx(
<g/>
s	s	k7c7
ʔ	ʔ	k6eAd1
<g/>
]	]	kIx)
<g/>
;	;	kIx,
vkládá	vkládat	k5eAaImIp3nS
se	se	k3xPyFc4
před	před	k7c4
samohlásku	samohláska	k1gFnSc4
u	u	k7c2
druhé	druhý	k4xOgFnSc2
části	část	k1gFnSc2
složeniny	složenina	k1gFnSc2
<g/>
,	,	kIx,
např.	např.	kA
trojúhelník	trojúhelník	k1gInSc4
[	[	kIx(
<g/>
troj	trojit	k5eAaImRp2nS
<g/>
.	.	kIx.
<g/>
ʔ	ʔ	k6eAd1
<g/>
:	:	kIx,
<g/>
ɦ	ɦ	k6eAd1
<g/>
:	:	kIx,
<g/>
k	k	k7c3
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
funkci	funkce	k1gFnSc6
se	se	k3xPyFc4
ráz	ráz	k1gInSc1
používá	používat	k5eAaImIp3nS
převážně	převážně	k6eAd1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
je	být	k5eAaImIp3nS
obvyklá	obvyklý	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
bez	bez	k7c2
rázu	ráz	k1gInSc2
<g/>
,	,	kIx,
např.	např.	kA
[	[	kIx(
<g/>
troju	troju	k5eAaPmIp1nS
<g/>
:	:	kIx,
<g/>
ɦ	ɦ	k6eAd1
<g/>
:	:	kIx,
<g/>
k	k	k7c3
<g/>
]	]	kIx)
<g/>
,	,	kIx,
[	[	kIx(
<g/>
zokna	zokna	k6eAd1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
varianty	varianta	k1gFnPc1
výslovnosti	výslovnost	k1gFnSc2
jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgInPc1d1
za	za	k7c4
správné	správný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Zdůraznění	zdůraznění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Použitím	použití	k1gNnSc7
rázu	ráz	k1gInSc2
lze	lze	k6eAd1
zvýraznit	zvýraznit	k5eAaPmF
důraz	důraz	k1gInSc4
na	na	k7c4
určité	určitý	k2eAgNnSc4d1
slovo	slovo	k1gNnSc4
<g/>
,	,	kIx,
výslovnost	výslovnost	k1gFnSc1
bez	bez	k7c2
rázu	ráz	k1gInSc2
je	být	k5eAaImIp3nS
spíše	spíše	k9
emočně	emočně	k6eAd1
neutrální	neutrální	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
spisovné	spisovný	k2eAgFnSc6d1
výslovnosti	výslovnost	k1gFnSc6
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
neklade	klást	k5eNaImIp3nS
ráz	ráz	k1gInSc4
ve	v	k7c6
slovech	slovo	k1gNnPc6
cizího	cizí	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
setkání	setkání	k1gNnSc1
dvou	dva	k4xCgFnPc2
samohlásek	samohláska	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
ve	v	k7c6
slově	slovo	k1gNnSc6
koala	koala	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Nazály	nazála	k1gFnPc1
</s>
<s>
Nazály	nazála	k1gFnPc1
<g/>
,	,	kIx,
nosovky	nosovka	k1gFnPc1
<g/>
,	,	kIx,
vznikají	vznikat	k5eAaImIp3nP
vytvořením	vytvoření	k1gNnSc7
okluze	okluze	k1gFnSc2
(	(	kIx(
<g/>
závěru	závěr	k1gInSc2
<g/>
)	)	kIx)
v	v	k7c6
dutině	dutina	k1gFnSc6
ústní	ústní	k2eAgFnSc1d1
a	a	k8xC
uvolněním	uvolnění	k1gNnSc7
cesty	cesta	k1gFnSc2
vzduchu	vzduch	k1gInSc2
nosní	nosní	k2eAgFnSc7d1
dutinou	dutina	k1gFnSc7
–	–	k?
u	u	k7c2
ostatních	ostatní	k2eAgFnPc2d1
souhlásek	souhláska	k1gFnPc2
je	být	k5eAaImIp3nS
průchod	průchod	k1gInSc1
do	do	k7c2
nosní	nosní	k2eAgFnSc2d1
dutiny	dutina	k1gFnSc2
uzavřen	uzavřít	k5eAaPmNgInS
měkkým	měkký	k2eAgNnSc7d1
patrem	patro	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyznačují	vyznačovat	k5eAaImIp3nP
se	s	k7c7
šumem	šum	k1gInSc7
i	i	k8xC
tónem	tón	k1gInSc7
(	(	kIx(
<g/>
sonoritou	sonorita	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
formován	formovat	k5eAaImNgInS
rezonancí	rezonance	k1gFnSc7
v	v	k7c6
ústní	ústní	k2eAgFnSc6d1
dutině	dutina	k1gFnSc6
(	(	kIx(
<g/>
závislé	závislý	k2eAgFnSc2d1
na	na	k7c6
postavení	postavení	k1gNnSc6
artikulátorů	artikulátor	k1gInPc2
a	a	k8xC
místě	místo	k1gNnSc6
vytvoření	vytvoření	k1gNnSc2
okluze	okluze	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
souhlásky	souhláska	k1gFnPc4
znělé	znělý	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
artikulace	artikulace	k1gFnSc2
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
uvolnění	uvolnění	k1gNnSc3
okluze	okluze	k1gFnSc2
v	v	k7c6
ústní	ústní	k2eAgFnSc6d1
dutině	dutina	k1gFnSc6
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
též	též	k9
někdy	někdy	k6eAd1
přiřazují	přiřazovat	k5eAaImIp3nP
mezi	mezi	k7c7
explozivy	exploziv	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Hláska	hláska	k1gFnSc1
</s>
<s>
IPA	IPA	kA
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Alofon	Alofon	k1gInSc1
</s>
<s>
základní	základní	k2eAgInSc1d1
</s>
<s>
artikulační	artikulační	k2eAgFnSc1d1
</s>
<s>
slabikotvorný	slabikotvorný	k2eAgInSc1d1
</s>
<s>
/	/	kIx~
<g/>
m	m	kA
<g/>
/	/	kIx~
</s>
<s>
/	/	kIx~
<g/>
m	m	kA
<g/>
/	/	kIx~
</s>
<s>
bilabiální	bilabiální	k2eAgFnSc1d1
nazála	nazála	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
m	m	kA
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
ɱ	ɱ	k?
<g/>
]	]	kIx)
1	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
m	m	kA
<g/>
̩	̩	k?
<g/>
]	]	kIx)
2	#num#	k4
</s>
<s>
/	/	kIx~
<g/>
n	n	k0
<g/>
/	/	kIx~
</s>
<s>
/	/	kIx~
<g/>
n	n	k0
<g/>
/	/	kIx~
</s>
<s>
alveolární	alveolární	k2eAgFnSc1d1
nazála	nazála	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
n	n	k0
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
ŋ	ŋ	k?
<g/>
]	]	kIx)
3	#num#	k4
</s>
<s>
/	/	kIx~
<g/>
ň	ň	k?
<g/>
/	/	kIx~
</s>
<s>
/	/	kIx~
<g/>
ɲ	ɲ	k?
<g/>
/	/	kIx~
</s>
<s>
palatální	palatální	k2eAgFnSc1d1
nazála	nazála	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
ɲ	ɲ	k?
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
Tato	tento	k3xDgFnSc1
realizace	realizace	k1gFnSc1
/	/	kIx~
<g/>
m	m	kA
<g/>
/	/	kIx~
se	se	k3xPyFc4
zpravidla	zpravidla	k6eAd1
objevuje	objevovat	k5eAaImIp3nS
před	před	k7c7
labiodentálními	labiodentální	k2eAgFnPc7d1
(	(	kIx(
<g/>
retozubnými	retozubný	k2eAgFnPc7d1
<g/>
)	)	kIx)
hláskami	hláska	k1gFnPc7
(	(	kIx(
<g/>
/	/	kIx~
<g/>
f	f	k?
<g/>
,	,	kIx,
v	v	k7c6
<g/>
/	/	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
např.	např.	kA
ve	v	k7c6
slově	slovo	k1gNnSc6
tramvaj	tramvaj	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
přizpůsobení	přizpůsobení	k1gNnSc4
místa	místo	k1gNnSc2
tvoření	tvoření	k1gNnSc2
následující	následující	k2eAgFnSc6d1
souhlásce	souhláska	k1gFnSc6
<g/>
,	,	kIx,
/	/	kIx~
<g/>
m	m	kA
<g/>
/	/	kIx~
se	se	k3xPyFc4
jinak	jinak	k6eAd1
vyslovuje	vyslovovat	k5eAaImIp3nS
jako	jako	k9
bilabiální	bilabiální	k2eAgNnSc1d1
(	(	kIx(
<g/>
obouretné	obouretný	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
Zřídka	zřídka	k6eAd1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
/	/	kIx~
<g/>
m	m	kA
<g/>
/	/	kIx~
objevit	objevit	k5eAaPmF
ve	v	k7c6
funkci	funkce	k1gFnSc6
slabičného	slabičný	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
<g/>
,	,	kIx,
např.	např.	kA
ve	v	k7c6
slovech	slovo	k1gNnPc6
sedm	sedm	k4xCc4
<g/>
,	,	kIx,
osm	osm	k4xCc4
<g/>
,	,	kIx,
Rožmberk	Rožmberk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
běžné	běžný	k2eAgFnSc6d1
výslovnosti	výslovnost	k1gFnSc6
se	se	k3xPyFc4
však	však	k9
před	před	k7c7
/	/	kIx~
<g/>
m	m	kA
<g/>
/	/	kIx~
vkládá	vkládat	k5eAaImIp3nS
[	[	kIx(
<g/>
ʊ	ʊ	k?
<g/>
]	]	kIx)
(	(	kIx(
<g/>
[	[	kIx(
<g/>
sɛ	sɛ	k1gMnSc1
<g/>
]	]	kIx)
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
výslovnost	výslovnost	k1gFnSc1
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
korektní	korektní	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
Tato	tento	k3xDgFnSc1
realizace	realizace	k1gFnSc1
/	/	kIx~
<g/>
n	n	k0
<g/>
/	/	kIx~
se	se	k3xPyFc4
zpravidla	zpravidla	k6eAd1
objevuje	objevovat	k5eAaImIp3nS
před	před	k7c7
velárními	velární	k2eAgFnPc7d1
(	(	kIx(
<g/>
zadopatrovými	zadopatrův	k2eAgFnPc7d1
<g/>
)	)	kIx)
hláskami	hláska	k1gFnPc7
(	(	kIx(
<g/>
/	/	kIx~
<g/>
k	k	k7c3
<g/>
,	,	kIx,
g	g	kA
<g/>
,	,	kIx,
x	x	k?
<g/>
/	/	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
např.	např.	kA
ve	v	k7c6
slově	slovo	k1gNnSc6
banka	banka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
přizpůsobení	přizpůsobení	k1gNnSc4
místa	místo	k1gNnSc2
tvoření	tvoření	k1gNnSc2
následující	následující	k2eAgFnSc6d1
souhlásce	souhláska	k1gFnSc6
<g/>
,	,	kIx,
/	/	kIx~
<g/>
n	n	k0
<g/>
/	/	kIx~
se	se	k3xPyFc4
jinak	jinak	k6eAd1
vyslovuje	vyslovovat	k5eAaImIp3nS
jako	jako	k8xS,k8xC
alveolární	alveolární	k2eAgFnPc1d1
(	(	kIx(
<g/>
předodásňové	předodásňová	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Konstriktivy	konstriktiva	k1gFnPc1
</s>
<s>
Úžinové	úžinový	k2eAgFnSc2d1
souhlásky	souhláska	k1gFnSc2
(	(	kIx(
<g/>
konstriktivy	konstriktiva	k1gFnSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
tvoří	tvořit	k5eAaImIp3nS
těsným	těsný	k2eAgNnSc7d1
přiblížením	přiblížení	k1gNnSc7
dvou	dva	k4xCgInPc2
artikulátorů	artikulátor	k1gInPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
vzniká	vznikat	k5eAaImIp3nS
úžina	úžina	k1gFnSc1
(	(	kIx(
<g/>
konstrikce	konstrikce	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
silný	silný	k2eAgInSc4d1
šum	šum	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
poslechového	poslechový	k2eAgInSc2d1
dojmu	dojem	k1gInSc2
se	se	k3xPyFc4
též	též	k9
nazývají	nazývat	k5eAaImIp3nP
třené	třený	k2eAgFnPc1d1
souhlásky	souhláska	k1gFnPc1
(	(	kIx(
<g/>
frikativy	frikativa	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hláska	hláska	k1gFnSc1
</s>
<s>
IPA	IPA	kA
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Alofon	Alofon	k1gInSc1
</s>
<s>
základní	základní	k2eAgInSc1d1
</s>
<s>
znělostní	znělostní	k2eAgFnSc1d1
</s>
<s>
/	/	kIx~
<g/>
f	f	k?
<g/>
/	/	kIx~
1	#num#	k4
</s>
<s>
/	/	kIx~
<g/>
f	f	k?
<g/>
/	/	kIx~
</s>
<s>
neznělá	znělý	k2eNgFnSc1d1
labiodentální	labiodentální	k2eAgFnSc1d1
frikativa	frikativa	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
f	f	k?
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
v	v	k7c6
<g/>
]	]	kIx)
</s>
<s>
/	/	kIx~
<g/>
v	v	k7c6
<g/>
/	/	kIx~
2	#num#	k4
</s>
<s>
/	/	kIx~
<g/>
v	v	k7c6
<g/>
/	/	kIx~
</s>
<s>
znělá	znělý	k2eAgFnSc1d1
labiodentální	labiodentální	k2eAgFnSc1d1
frikativa	frikativa	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
v	v	k7c6
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
f	f	k?
<g/>
]	]	kIx)
</s>
<s>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
/	/	kIx~
</s>
<s>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
/	/	kIx~
</s>
<s>
neznělá	znělý	k2eNgFnSc1d1
alveolární	alveolární	k2eAgFnSc1d1
frikativa	frikativa	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
s	s	k7c7
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
z	z	k7c2
<g/>
]	]	kIx)
</s>
<s>
/	/	kIx~
<g/>
z	z	k7c2
<g/>
/	/	kIx~
</s>
<s>
/	/	kIx~
<g/>
z	z	k7c2
<g/>
/	/	kIx~
</s>
<s>
znělá	znělý	k2eAgFnSc1d1
alveolární	alveolární	k2eAgFnSc1d1
frikativa	frikativa	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
z	z	k7c2
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
s	s	k7c7
<g/>
]	]	kIx)
</s>
<s>
/	/	kIx~
<g/>
š	š	k?
<g/>
/	/	kIx~
</s>
<s>
/	/	kIx~
<g/>
ʃ	ʃ	k?
<g/>
/	/	kIx~
</s>
<s>
neznělá	znělý	k2eNgFnSc1d1
postalveolární	postalveolární	k2eAgFnSc1d1
frikativa	frikativa	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
ʃ	ʃ	k?
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
ʒ	ʒ	k?
<g/>
]	]	kIx)
</s>
<s>
/	/	kIx~
<g/>
ž	ž	k?
<g/>
/	/	kIx~
</s>
<s>
/	/	kIx~
<g/>
ʒ	ʒ	k?
<g/>
/	/	kIx~
</s>
<s>
znělá	znělý	k2eAgFnSc1d1
postalveolární	postalveolární	k2eAgFnSc1d1
frikativa	frikativa	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
ʒ	ʒ	k?
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
ʃ	ʃ	k?
<g/>
]	]	kIx)
</s>
<s>
/	/	kIx~
<g/>
x	x	k?
<g/>
/	/	kIx~
</s>
<s>
/	/	kIx~
<g/>
x	x	k?
<g/>
/	/	kIx~
3	#num#	k4
</s>
<s>
neznělá	znělý	k2eNgFnSc1d1
velární	velární	k2eAgFnSc1d1
frikativa	frikativa	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
x	x	k?
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
ɦ	ɦ	k?
<g/>
/	/	kIx~
<g/>
ɣ	ɣ	k?
<g/>
]	]	kIx)
4	#num#	k4
</s>
<s>
/	/	kIx~
<g/>
h	h	k?
<g/>
/	/	kIx~
5	#num#	k4
</s>
<s>
/	/	kIx~
<g/>
ɦ	ɦ	k?
<g/>
/	/	kIx~
</s>
<s>
znělá	znělý	k2eAgFnSc1d1
glotální	glotální	k2eAgFnSc1d1
frikativa	frikativa	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
ɦ	ɦ	k?
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
x	x	k?
<g/>
]	]	kIx)
3	#num#	k4
</s>
<s>
1	#num#	k4
Jako	jako	k8xS,k8xC
samostatný	samostatný	k2eAgInSc1d1
foném	foném	k1gInSc1
se	s	k7c7
/	/	kIx~
<g/>
f	f	k?
<g/>
/	/	kIx~
v	v	k7c6
domácích	domácí	k2eAgNnPc6d1
slovech	slovo	k1gNnPc6
vyskytuje	vyskytovat	k5eAaImIp3nS
jen	jen	k9
výjimečně	výjimečně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
je	být	k5eAaImIp3nS
hláska	hláska	k1gFnSc1
[	[	kIx(
<g/>
f	f	k?
<g/>
]	]	kIx)
neznělou	znělý	k2eNgFnSc7d1
realizací	realizace	k1gFnSc7
/	/	kIx~
<g/>
v	v	k7c4
<g/>
/	/	kIx~
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
Hláska	hláska	k1gFnSc1
/	/	kIx~
<g/>
v	v	k7c6
<g/>
/	/	kIx~
podléhá	podléhat	k5eAaImIp3nS
regresivní	regresivní	k2eAgFnSc4d1
asimilaci	asimilace	k1gFnSc4
znělosti	znělost	k1gFnSc2
<g/>
,	,	kIx,
např.	např.	kA
vzpomínka	vzpomínka	k1gFnSc1
[	[	kIx(
<g/>
fspomiː	fspomiː	k1gFnSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
sama	sám	k3xTgFnSc1
ji	on	k3xPp3gFnSc4
však	však	k9
nepůsobí	působit	k5eNaImIp3nP
<g/>
,	,	kIx,
např.	např.	kA
svět	svět	k1gInSc1
[	[	kIx(
<g/>
svjɛ	svjɛ	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
3V	3V	k4
písmu	písmo	k1gNnSc6
se	se	k3xPyFc4
zaznamenává	zaznamenávat	k5eAaImIp3nS
spřežkou	spřežka	k1gFnSc7
<ch>
.	.	kIx.
</s>
<s>
4	#num#	k4
Neznělé	znělý	k2eNgFnPc4d1
zadopatrové	zadopatrová	k1gFnPc4
/	/	kIx~
<g/>
x	x	k?
<g/>
/	/	kIx~
tvoří	tvořit	k5eAaImIp3nS
pár	pár	k4xCyI
se	s	k7c7
znělým	znělý	k2eAgInSc7d1
hlasivkovým	hlasivkový	k2eAgInSc7d1
/	/	kIx~
<g/>
h	h	k?
<g/>
/	/	kIx~
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
se	se	k3xPyFc4
obě	dva	k4xCgFnPc1
souhlásky	souhláska	k1gFnPc1
liší	lišit	k5eAaImIp3nP
místem	místo	k1gNnSc7
tvoření	tvoření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mluvčími	mluvčí	k1gMnPc7
jsou	být	k5eAaImIp3nP
však	však	k9
vnímány	vnímat	k5eAaImNgFnP
obě	dva	k4xCgFnPc1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
zadní	zadní	k2eAgFnSc2d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znělou	znělý	k2eAgFnSc7d1
realizací	realizace	k1gFnSc7
/	/	kIx~
<g/>
x	x	k?
<g/>
/	/	kIx~
(	(	kIx(
<g/>
např.	např.	kA
ve	v	k7c6
spojení	spojení	k1gNnSc6
abych	aby	kYmCp1nS
byl	být	k5eAaImAgInS
<g/>
)	)	kIx)
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
buď	buď	k8xC
glotální	glotální	k2eAgNnSc4d1
(	(	kIx(
<g/>
hlasivkové	hlasivkový	k2eAgNnSc4d1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
ɦ	ɦ	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
velární	velární	k2eAgFnPc1d1
(	(	kIx(
<g/>
zadopatrové	zadopatrová	k1gFnPc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
ɣ	ɣ	k?
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
volné	volný	k2eAgInPc4d1
alofony	alofon	k1gInPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
využití	využití	k1gNnSc1
je	být	k5eAaImIp3nS
podmíněno	podmínit	k5eAaPmNgNnS
regionálními	regionální	k2eAgFnPc7d1
zvyklostmi	zvyklost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
5	#num#	k4
Znělé	znělý	k2eAgFnPc4d1
/	/	kIx~
<g/>
h	h	k?
<g/>
/	/	kIx~
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
specifické	specifický	k2eAgInPc4d1
pro	pro	k7c4
češtinu	čeština	k1gFnSc4
a	a	k8xC
některé	některý	k3yIgInPc1
slovanské	slovanský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
(	(	kIx(
<g/>
např.	např.	kA
slovenština	slovenština	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
většině	většina	k1gFnSc6
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
mají	mít	k5eAaImIp3nP
/	/	kIx~
<g/>
h	h	k?
<g/>
/	/	kIx~
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
hláska	hláska	k1gFnSc1
neznělá	znělý	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
podléhat	podléhat	k5eAaImF
progresivní	progresivní	k2eAgFnSc4d1
asimilaci	asimilace	k1gFnSc4
znělosti	znělost	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
ve	v	k7c6
skupině	skupina	k1gFnSc6
/	/	kIx~
<g/>
sh	sh	k?
<g/>
/	/	kIx~
<g/>
,	,	kIx,
např.	např.	kA
shořet	shořet	k5eAaPmF
[	[	kIx(
<g/>
sxor	sxor	k1gInSc1
<g/>
̩	̩	k?
<g/>
ɛ	ɛ	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
výslovnost	výslovnost	k1gFnSc1
je	být	k5eAaImIp3nS
typická	typický	k2eAgFnSc1d1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Moravě	Morava	k1gFnSc6
je	být	k5eAaImIp3nS
běžnější	běžný	k2eAgFnSc1d2
regresivní	regresivní	k2eAgFnSc1d1
asimilace	asimilace	k1gFnSc1
<g/>
,	,	kIx,
[	[	kIx(
<g/>
zɦ	zɦ	k1gInSc1
<g/>
̩	̩	k?
<g/>
ɛ	ɛ	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Semiokluzivy	semiokluziva	k1gFnPc1
</s>
<s>
Poloražené	poloražený	k2eAgFnPc1d1
souhlásky	souhláska	k1gFnPc1
(	(	kIx(
<g/>
semiokluzivy	semiokluziva	k1gFnPc1
<g/>
)	)	kIx)
vznikají	vznikat	k5eAaImIp3nP
prvotní	prvotní	k2eAgFnSc4d1
krátkou	krátká	k1gFnSc4
okluzí	okluze	k1gFnPc2
(	(	kIx(
<g/>
závěrem	závěr	k1gInSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
vzápětí	vzápětí	k6eAd1
uvolněna	uvolnit	k5eAaPmNgFnS
a	a	k8xC
následována	následovat	k5eAaImNgFnS
šumem	šum	k1gInSc7
vzniklým	vzniklý	k2eAgInSc7d1
konstrikcí	konstrikce	k1gFnSc7
(	(	kIx(
<g/>
úžinou	úžina	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podstatě	podstata	k1gFnSc6
jde	jít	k5eAaImIp3nS
o	o	k7c4
rychlý	rychlý	k2eAgInSc4d1
sled	sled	k1gInSc4
okluzivy	okluziva	k1gFnSc2
a	a	k8xC
konstriktivy	konstriktiva	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
artikulovány	artikulovat	k5eAaImNgInP
současně	současně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
výslovnosti	výslovnost	k1gFnSc6
lze	lze	k6eAd1
zřetelně	zřetelně	k6eAd1
rozlišit	rozlišit	k5eAaPmF
současnou	současný	k2eAgFnSc4d1
artikulaci	artikulace	k1gFnSc4
u	u	k7c2
semiokluzivy	semiokluziva	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
/	/	kIx~
<g/>
c	c	k0
<g/>
/	/	kIx~
[	[	kIx(
<g/>
t	t	k?
<g/>
͡	͡	k?
<g/>
s	s	k7c7
<g/>
]	]	kIx)
<g/>
)	)	kIx)
od	od	k7c2
dvou	dva	k4xCgFnPc2
samostatně	samostatně	k6eAd1
vyslovených	vyslovený	k2eAgFnPc2d1
souhlásek	souhláska	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
[	[	kIx(
<g/>
t	t	k?
<g/>
]	]	kIx)
a	a	k8xC
[	[	kIx(
<g/>
s	s	k7c7
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
poslechového	poslechový	k2eAgNnSc2d1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
souhlásky	souhláska	k1gFnPc4
poloražené	poloražený	k2eAgFnPc4d1
(	(	kIx(
<g/>
afrikáty	afrikáta	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hláska	hláska	k1gFnSc1
</s>
<s>
IPA	IPA	kA
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Alofon	Alofon	k1gInSc1
</s>
<s>
základní	základní	k2eAgInSc1d1
</s>
<s>
znělostní	znělostní	k2eAgFnSc1d1
</s>
<s>
/	/	kIx~
<g/>
c	c	k0
<g/>
/	/	kIx~
</s>
<s>
/	/	kIx~
<g/>
t	t	k?
<g/>
͡	͡	k?
<g/>
s	s	k7c7
<g/>
/	/	kIx~
2	#num#	k4
</s>
<s>
neznělá	znělý	k2eNgFnSc1d1
alveolární	alveolární	k2eAgFnSc1d1
afrikáta	afrikáta	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
t	t	k?
<g/>
͡	͡	k?
<g/>
s	s	k7c7
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
d	d	k?
<g/>
͡	͡	k?
<g/>
z	z	k7c2
<g/>
]	]	kIx)
</s>
<s>
/	/	kIx~
<g/>
ʒ	ʒ	k?
<g/>
/	/	kIx~
1	#num#	k4
</s>
<s>
/	/	kIx~
<g/>
d	d	k?
<g/>
͡	͡	k?
<g/>
z	z	k7c2
<g/>
/	/	kIx~
2	#num#	k4
</s>
<s>
znělá	znělý	k2eAgFnSc1d1
alveolární	alveolární	k2eAgFnSc1d1
afrikáta	afrikáta	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
d	d	k?
<g/>
͡	͡	k?
<g/>
z	z	k7c2
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
t	t	k?
<g/>
͡	͡	k?
<g/>
s	s	k7c7
<g/>
]	]	kIx)
</s>
<s>
/	/	kIx~
<g/>
č	č	k0
<g/>
/	/	kIx~
</s>
<s>
/	/	kIx~
<g/>
t	t	k?
<g/>
͡	͡	k?
<g/>
ʃ	ʃ	k?
<g/>
/	/	kIx~
2	#num#	k4
</s>
<s>
neznělá	znělý	k2eNgFnSc1d1
postalveolární	postalveolární	k2eAgFnSc1d1
afrikáta	afrikáta	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
t	t	k?
<g/>
͡	͡	k?
<g/>
ʃ	ʃ	k?
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
d	d	k?
<g/>
͡	͡	k?
<g/>
ʒ	ʒ	k?
<g/>
]	]	kIx)
</s>
<s>
/	/	kIx~
<g/>
ǯ	ǯ	k?
<g/>
/	/	kIx~
1	#num#	k4
</s>
<s>
/	/	kIx~
<g/>
d	d	k?
<g/>
͡	͡	k?
<g/>
ʒ	ʒ	k?
/	/	kIx~
2	#num#	k4
</s>
<s>
znělá	znělý	k2eAgFnSc1d1
postalveolární	postalveolární	k2eAgFnSc1d1
afrikáta	afrikáta	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
d	d	k?
<g/>
͡	͡	k?
<g/>
ʒ	ʒ	k?
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
t	t	k?
<g/>
͡	͡	k?
<g/>
ʃ	ʃ	k?
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
Fonémy	foném	k1gInPc4
/	/	kIx~
<g/>
ʒ	ʒ	k?
<g/>
/	/	kIx~
a	a	k8xC
/	/	kIx~
<g/>
ǯ	ǯ	k?
<g/>
/	/	kIx~
se	se	k3xPyFc4
v	v	k7c6
písmu	písmo	k1gNnSc6
zaznamenávají	zaznamenávat	k5eAaImIp3nP
spřežkami	spřežka	k1gFnPc7
<dz>
a	a	k8xC
<dž>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
samostatné	samostatný	k2eAgInPc1d1
fonémy	foném	k1gInPc1
se	se	k3xPyFc4
tyto	tento	k3xDgInPc1
hlásky	hlásek	k1gInPc1
vyskytují	vyskytovat	k5eAaImIp3nP
převážně	převážně	k6eAd1
ve	v	k7c6
slovech	slovo	k1gNnPc6
cizího	cizí	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
slovech	slovo	k1gNnPc6
domácích	domácí	k2eAgMnPc2d1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
znělé	znělý	k2eAgFnPc4d1
realizace	realizace	k1gFnPc4
fonémů	foném	k1gInPc2
/	/	kIx~
<g/>
c	c	k0
<g/>
/	/	kIx~
a	a	k8xC
/	/	kIx~
<g/>
č	č	k0
<g/>
/	/	kIx~
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
Pro	pro	k7c4
zápis	zápis	k1gInSc4
semiokluziv	semiokluziva	k1gFnPc2
se	se	k3xPyFc4
v	v	k7c6
IPA	IPA	kA
někdy	někdy	k6eAd1
používají	používat	k5eAaImIp3nP
neoficiální	oficiální	k2eNgFnPc1d1,k2eAgFnPc1d1
ligatury	ligatura	k1gFnPc1
<g/>
:	:	kIx,
ʦ	ʦ	k?
<g/>
,	,	kIx,
ʣ	ʣ	k?
<g/>
,	,	kIx,
ʧ	ʧ	k?
<g/>
,	,	kIx,
ʤ	ʤ	k?
Současná	současný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
IPA	IPA	kA
podporuje	podporovat	k5eAaImIp3nS
používání	používání	k1gNnSc2
dvou	dva	k4xCgInPc2
oddělených	oddělený	k2eAgInPc2d1
znaků	znak	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
spojitá	spojitý	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
zdůrazněna	zdůraznit	k5eAaPmNgFnS
použitím	použití	k1gNnSc7
vázacího	vázací	k2eAgInSc2d1
znaku	znak	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
vědecká	vědecký	k2eAgFnSc1d1
transkripce	transkripce	k1gFnSc1
dává	dávat	k5eAaImIp3nS
přednost	přednost	k1gFnSc4
jedinému	jediný	k2eAgInSc3d1
znaku	znak	k1gInSc3
pro	pro	k7c4
zdůraznění	zdůraznění	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
jediný	jediný	k2eAgInSc4d1
foném	foném	k1gInSc4
a	a	k8xC
jednotnou	jednotný	k2eAgFnSc4d1
artikulaci	artikulace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Vibranty	vibranta	k1gFnPc1
</s>
<s>
Vibranty	vibranta	k1gFnPc1
<g/>
,	,	kIx,
kmitavé	kmitavý	k2eAgFnPc1d1
souhlásky	souhláska	k1gFnPc1
<g/>
,	,	kIx,
vznikají	vznikat	k5eAaImIp3nP
aerodynamicky	aerodynamicky	k6eAd1
podmíněným	podmíněný	k2eAgInSc7d1
<g/>
,	,	kIx,
opakovaným	opakovaný	k2eAgInSc7d1
dotykem	dotyk	k1gInSc7
artikulátorů	artikulátor	k1gInPc2
(	(	kIx(
<g/>
kmitáním	kmitání	k1gNnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hláska	hláska	k1gFnSc1
</s>
<s>
IPA	IPA	kA
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Alofon	Alofon	k1gInSc1
</s>
<s>
základní	základní	k2eAgInSc1d1
</s>
<s>
znělostní	znělostní	k2eAgFnSc1d1
</s>
<s>
slabikotvorný	slabikotvorný	k2eAgInSc1d1
</s>
<s>
/	/	kIx~
<g/>
r	r	kA
<g/>
/	/	kIx~
</s>
<s>
/	/	kIx~
<g/>
r	r	kA
<g/>
/	/	kIx~
</s>
<s>
alveolární	alveolární	k2eAgFnSc1d1
vibranta	vibranta	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
r	r	kA
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
r	r	kA
<g/>
̩	̩	k?
<g/>
]	]	kIx)
1	#num#	k4
</s>
<s>
/	/	kIx~
<g/>
ř	ř	k?
<g/>
/	/	kIx~
2	#num#	k4
</s>
<s>
/	/	kIx~
<g/>
r	r	kA
<g/>
̝	̝	k?
<g/>
/	/	kIx~
</s>
<s>
zvýšená	zvýšený	k2eAgFnSc1d1
alveolární	alveolární	k2eAgFnSc1d1
vibranta	vibranta	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
r	r	kA
<g/>
̝	̝	k?
<g/>
]	]	kIx)
3	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
r	r	kA
<g/>
̝	̝	k?
<g/>
°	°	k?
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
V	v	k7c6
češtině	čeština	k1gFnSc6
se	se	k3xPyFc4
často	často	k6eAd1
objevuje	objevovat	k5eAaImIp3nS
/	/	kIx~
<g/>
r	r	kA
<g/>
/	/	kIx~
místo	místo	k1gNnSc4
samohlásky	samohláska	k1gFnSc2
v	v	k7c6
pozici	pozice	k1gFnSc6
slabičného	slabičný	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
(	(	kIx(
<g/>
např.	např.	kA
krk	krk	k1gInSc1
<g/>
,	,	kIx,
kapr	kapr	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
Hláska	hláska	k1gFnSc1
/	/	kIx~
<g/>
ř	ř	k?
<g/>
/	/	kIx~
je	být	k5eAaImIp3nS
specifická	specifický	k2eAgFnSc1d1
pro	pro	k7c4
češtinu	čeština	k1gFnSc4
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
jiných	jiný	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
nevyskytuje	vyskytovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
dvojí	dvojí	k4xRgFnSc4
realizaci	realizace	k1gFnSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
znělá	znělý	k2eAgFnSc1d1
varianta	varianta	k1gFnSc1
je	být	k5eAaImIp3nS
základní	základní	k2eAgFnSc1d1
<g/>
,	,	kIx,
vlivem	vliv	k1gInSc7
souhláskového	souhláskový	k2eAgNnSc2d1
okolí	okolí	k1gNnSc2
a	a	k8xC
na	na	k7c6
konci	konec	k1gInSc6
slova	slovo	k1gNnSc2
se	se	k3xPyFc4
však	však	k9
vyslovuje	vyslovovat	k5eAaImIp3nS
nezněle	zněle	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podléhá	podléhat	k5eAaImIp3nS
jak	jak	k6eAd1
regresivní	regresivní	k2eAgFnSc1d1
(	(	kIx(
<g/>
např.	např.	kA
přetvářka	přetvářka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
progresivní	progresivní	k2eAgFnSc4d1
asimilaci	asimilace	k1gFnSc4
znělosti	znělost	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
při	pře	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS
se	se	k3xPyFc4
vyšší	vysoký	k2eAgFnSc7d2
mírou	míra	k1gFnSc7
kontaktu	kontakt	k1gInSc2
jazyka	jazyk	k1gInSc2
s	s	k7c7
tvrdým	tvrdý	k2eAgNnSc7d1
patrem	patro	k1gNnSc7
než	než	k8xS
/	/	kIx~
<g/>
r	r	kA
<g/>
/	/	kIx~
<g/>
,	,	kIx,
nabývá	nabývat	k5eAaImIp3nS
tak	tak	k6eAd1
více	hodně	k6eAd2
charakteru	charakter	k1gInSc2
konstriktivy	konstriktiva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc4d2
míru	míra	k1gFnSc4
šumu	šum	k1gInSc2
a	a	k8xC
ztrácí	ztrácet	k5eAaImIp3nS
vlastnosti	vlastnost	k1gFnPc4
sonory	sonora	k1gFnSc2
–	–	k?
nemůže	moct	k5eNaImIp3nS
tedy	tedy	k9
stát	stát	k5eAaImF,k5eAaPmF
v	v	k7c6
pozici	pozice	k1gFnSc6
jádra	jádro	k1gNnSc2
slabiky	slabika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
/	/	kIx~
<g/>
r	r	kA
<g/>
/	/	kIx~
se	se	k3xPyFc4
vyznačuje	vyznačovat	k5eAaImIp3nS
1	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
výraznými	výrazný	k2eAgInPc7d1
kmity	kmit	k1gInPc7
<g/>
,	,	kIx,
/	/	kIx~
<g/>
ř	ř	k?
<g/>
/	/	kIx~
má	mít	k5eAaImIp3nS
3	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
menších	malý	k2eAgInPc2d2
a	a	k8xC
rychlejších	rychlý	k2eAgInPc2d2
kmitů	kmit	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rty	ret	k1gInPc7
jsou	být	k5eAaImIp3nP
při	při	k7c6
artikulaci	artikulace	k1gFnSc6
/	/	kIx~
<g/>
ř	ř	k?
<g/>
/	/	kIx~
mírně	mírně	k6eAd1
zaokrouhleny	zaokrouhlen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
Pro	pro	k7c4
zápis	zápis	k1gInSc4
znělého	znělý	k2eAgNnSc2d1
/	/	kIx~
<g/>
ř	ř	k?
<g/>
/	/	kIx~
se	se	k3xPyFc4
v	v	k7c6
IPA	IPA	kA
někdy	někdy	k6eAd1
používá	používat	k5eAaImIp3nS
neoficiální	neoficiální	k2eAgInSc1d1,k2eNgInSc1d1
znak	znak	k1gInSc1
ɼ	ɼ	k?
Tento	tento	k3xDgInSc4
znak	znak	k1gInSc4
byl	být	k5eAaImAgMnS
z	z	k7c2
oficiální	oficiální	k2eAgFnSc2d1
tabulky	tabulka	k1gFnSc2
odstraněn	odstranit	k5eAaPmNgMnS
a	a	k8xC
nahrazen	nahradit	k5eAaPmNgInS
znakem	znak	k1gInSc7
r	r	kA
s	s	k7c7
diakritickým	diakritický	k2eAgNnSc7d1
znaménkem	znaménko	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
značí	značit	k5eAaImIp3nS
„	„	k?
<g/>
zvýšenou	zvýšený	k2eAgFnSc4d1
alveolární	alveolární	k2eAgFnSc4d1
vibrantu	vibranta	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Aproximanty	Aproximanta	k1gFnPc1
</s>
<s>
Aproximanty	Aproximanta	k1gFnPc4
vznikají	vznikat	k5eAaImIp3nP
přiblížením	přiblížení	k1gNnSc7
(	(	kIx(
<g/>
aproximací	aproximace	k1gFnPc2
<g/>
)	)	kIx)
artikulátorů	artikulátor	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
není	být	k5eNaImIp3nS
tak	tak	k6eAd1
těsné	těsný	k2eAgNnSc4d1
jako	jako	k9
u	u	k7c2
konstriktiv	konstriktiva	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
těsnější	těsný	k2eAgInSc1d2
než	než	k8xS
u	u	k7c2
samohlásek	samohláska	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemají	mít	k5eNaImIp3nP
výrazný	výrazný	k2eAgInSc4d1
šum	šum	k1gInSc4
ani	ani	k8xC
tón	tón	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aproximanty	Aproximanta	k1gFnPc1
jsou	být	k5eAaImIp3nP
nejvíce	nejvíce	k6eAd1,k6eAd3
otevřené	otevřený	k2eAgFnPc1d1
souhlásky	souhláska	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nP
tedy	tedy	k9
jakýsi	jakýsi	k3yIgInSc4
plynulý	plynulý	k2eAgInSc4d1
přechod	přechod	k1gInSc4
k	k	k7c3
samohláskám	samohláska	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Hláska	hláska	k1gFnSc1
</s>
<s>
IPA	IPA	kA
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Alofon	Alofon	k1gInSc1
</s>
<s>
základní	základní	k2eAgInSc1d1
</s>
<s>
slabikotvorný	slabikotvorný	k2eAgInSc1d1
</s>
<s>
poziční	poziční	k2eAgFnSc1d1
</s>
<s>
/	/	kIx~
<g/>
l	l	kA
<g/>
/	/	kIx~
1	#num#	k4
</s>
<s>
/	/	kIx~
<g/>
l	l	kA
<g/>
/	/	kIx~
</s>
<s>
alveolární	alveolární	k2eAgFnSc1d1
laterální	laterální	k2eAgFnSc1d1
aproximanta	aproximanta	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
l	l	kA
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
l	l	kA
<g/>
̩	̩	k?
<g/>
]	]	kIx)
2	#num#	k4
</s>
<s>
/	/	kIx~
<g/>
j	j	k?
<g/>
/	/	kIx~
</s>
<s>
/	/	kIx~
<g/>
j	j	k?
<g/>
/	/	kIx~
</s>
<s>
palatální	palatální	k2eAgFnSc1d1
aproximanta	aproximanta	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
j	j	k?
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
ɪ	ɪ	k?
<g/>
̯	̯	k?
<g/>
]	]	kIx)
3	#num#	k4
</s>
<s>
bilabiální	bilabiální	k2eAgFnSc1d1
aproximanta	aproximanta	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
ʊ	ʊ	k?
<g/>
̯	̯	k?
<g/>
]	]	kIx)
4	#num#	k4
</s>
<s>
1	#num#	k4
/	/	kIx~
<g/>
l	l	kA
<g/>
/	/	kIx~
je	být	k5eAaImIp3nS
jediná	jediný	k2eAgFnSc1d1
boková	bokový	k2eAgFnSc1d1
(	(	kIx(
<g/>
laterální	laterální	k2eAgFnSc1d1
<g/>
)	)	kIx)
hláska	hláska	k1gFnSc1
v	v	k7c6
češtině	čeština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jazyk	jazyk	k1gInSc1
se	se	k3xPyFc4
při	při	k7c6
její	její	k3xOp3gFnSc6
výslovnosti	výslovnost	k1gFnSc6
dotýká	dotýkat	k5eAaImIp3nS
dásňového	dásňový	k2eAgInSc2d1
oblouku	oblouk	k1gInSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
vzduch	vzduch	k1gInSc1
proudí	proudit	k5eAaImIp3nS,k5eAaPmIp3nS
okolo	okolo	k7c2
boků	bok	k1gInPc2
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
všech	všecek	k3xTgFnPc2
ostatních	ostatní	k2eAgFnPc2d1
souhlásek	souhláska	k1gFnPc2
proudí	proudit	k5eAaPmIp3nS,k5eAaImIp3nS
vzduch	vzduch	k1gInSc1
před	před	k7c4
střed	střed	k1gInSc4
jazyka	jazyk	k1gInSc2
(	(	kIx(
<g/>
středové	středový	k2eAgFnPc1d1
souhlásky	souhláska	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
V	v	k7c6
češtině	čeština	k1gFnSc6
se	se	k3xPyFc4
často	často	k6eAd1
objevuje	objevovat	k5eAaImIp3nS
/	/	kIx~
<g/>
l	l	kA
<g/>
/	/	kIx~
místo	místo	k1gNnSc4
samohlásky	samohláska	k1gFnSc2
v	v	k7c6
pozici	pozice	k1gFnSc6
slabičného	slabičný	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
(	(	kIx(
<g/>
např.	např.	kA
vlk	vlk	k1gMnSc1
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
Artikulačně	artikulačně	k6eAd1
je	být	k5eAaImIp3nS
/	/	kIx~
<g/>
j	j	k?
<g/>
/	/	kIx~
polovokál	polovokál	k1gInSc1
blízký	blízký	k2eAgInSc1d1
samohlásce	samohláska	k1gFnSc3
/	/	kIx~
<g/>
i	i	k9
<g/>
/	/	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
fonetického	fonetický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
považovat	považovat	k5eAaImF
tautosylabické	tautosylabický	k2eAgNnSc4d1
(	(	kIx(
<g/>
stejnoslabičné	stejnoslabičný	k2eAgNnSc4d1
<g/>
)	)	kIx)
spojení	spojení	k1gNnSc4
samohlásky	samohláska	k1gFnSc2
a	a	k8xC
/	/	kIx~
<g/>
j	j	k?
<g/>
/	/	kIx~
za	za	k7c4
dvojhlásku	dvojhláska	k1gFnSc4
(	(	kIx(
<g/>
např.	např.	kA
[	[	kIx(
<g/>
ɛ	ɛ	k?
<g/>
̯	̯	k?
<g/>
]	]	kIx)
<g/>
/	/	kIx~
<g/>
[	[	kIx(
<g/>
ɛ	ɛ	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
z	z	k7c2
hlediska	hledisko	k1gNnSc2
fonologické	fonologický	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
však	však	k9
ve	v	k7c6
spisovné	spisovný	k2eAgFnSc6d1
češtině	čeština	k1gFnSc6
jde	jít	k5eAaImIp3nS
spojení	spojení	k1gNnSc4
dvou	dva	k4xCgInPc2
fonémů	foném	k1gInPc2
(	(	kIx(
<g/>
/	/	kIx~
<g/>
e	e	k0
<g/>
/	/	kIx~
+	+	kIx~
/	/	kIx~
<g/>
j	j	k?
<g/>
/	/	kIx~
<g/>
)	)	kIx)
–	–	k?
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
obecné	obecný	k2eAgFnSc2d1
češtiny	čeština	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
dvojhláska	dvojhláska	k1gFnSc1
/	/	kIx~
<g/>
ej	ej	k0
<g/>
/	/	kIx~
jako	jako	k8xC,k8xS
foném	foném	k1gInSc1
existuje	existovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
výslovnosti	výslovnost	k1gFnSc6
přejatých	přejatý	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
se	se	k3xPyFc4
vkládá	vkládat	k5eAaImIp3nS
[	[	kIx(
<g/>
j	j	k?
<g/>
]	]	kIx)
mezi	mezi	k7c4
samohlásky	samohláska	k1gFnPc4
ve	v	k7c6
skupinách	skupina	k1gFnPc6
/	/	kIx~
<g/>
ia	ia	k0
<g/>
,	,	kIx,
ie	ie	k?
<g/>
,	,	kIx,
ii	ii	k?
<g/>
,	,	kIx,
io	io	k?
<g/>
,	,	kIx,
iu	iu	k?
<g/>
/	/	kIx~
<g/>
,	,	kIx,
např.	např.	kA
Marie	Marie	k1gFnSc1
[	[	kIx(
<g/>
marɪ	marɪ	k?
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
poziční	poziční	k2eAgFnSc4d1
variantu	varianta	k1gFnSc4
samohlásky	samohláska	k1gFnSc2
/	/	kIx~
<g/>
u	u	k7c2
<g/>
/	/	kIx~
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
v	v	k7c6
češtině	čeština	k1gFnSc6
vyskytuje	vyskytovat	k5eAaImIp3nS
výhradně	výhradně	k6eAd1
jako	jako	k9
druhý	druhý	k4xOgInSc1
člen	člen	k1gInSc1
dvojhlásek	dvojhláska	k1gFnPc2
/	/	kIx~
<g/>
ou	ou	k0
<g/>
,	,	kIx,
au	au	k0
<g/>
,	,	kIx,
eu	eu	k?
<g/>
/	/	kIx~
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
netvoří	tvořit	k5eNaImIp3nS
slabičné	slabičný	k2eAgNnSc4d1
jádro	jádro	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Místo	místo	k7c2
tvoření	tvoření	k1gNnSc2
</s>
<s>
Schéma	schéma	k1gNnSc1
artikulačního	artikulační	k2eAgNnSc2d1
ústrojí	ústrojí	k1gNnSc2
s	s	k7c7
vyznačením	vyznačení	k1gNnSc7
jeho	jeho	k3xOp3gFnPc2
částí	část	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
A	a	k9
–	–	k?
ústní	ústní	k2eAgFnSc1d1
dutina	dutina	k1gFnSc1
(	(	kIx(
<g/>
cavum	cavum	k1gNnSc1
oris	orisa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
B	B	kA
–	–	k?
nosní	nosní	k2eAgFnSc1d1
dutina	dutina	k1gFnSc1
(	(	kIx(
<g/>
cavum	cavum	k1gInSc1
nasi	nas	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
C	C	kA
–	–	k?
hltan	hltan	k1gInSc1
(	(	kIx(
<g/>
pharynx	pharynx	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
–	–	k?
jazyk	jazyk	k1gInSc1
(	(	kIx(
<g/>
lingua	lingua	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2	#num#	k4
–	–	k?
rty	ret	k1gInPc4
(	(	kIx(
<g/>
labia	labia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
3	#num#	k4
–	–	k?
zuby	zub	k1gInPc4
(	(	kIx(
<g/>
dentes	dentes	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
4	#num#	k4
–	–	k?
dásňový	dásňový	k2eAgInSc4d1
oblouk	oblouk	k1gInSc4
se	s	k7c7
sklípky	sklípek	k1gInPc7
(	(	kIx(
<g/>
alveoli	alveoli	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgNnPc6
jsou	být	k5eAaImIp3nP
usazeny	usazen	k2eAgInPc1d1
zuby	zub	k1gInPc1
<g/>
,	,	kIx,
5	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
tvrdé	tvrdý	k2eAgNnSc1d1
patro	patro	k1gNnSc1
(	(	kIx(
<g/>
palatum	palatum	k1gNnSc1
durum	durum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
–	–	k?
měkké	měkký	k2eAgNnSc1d1
patro	patro	k1gNnSc1
(	(	kIx(
<g/>
palatum	palatum	k1gNnSc1
velum	velum	k1gNnSc1
<g/>
)	)	kIx)
7	#num#	k4
–	–	k?
čípek	čípek	k1gInSc1
(	(	kIx(
<g/>
uvula	uvula	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
8	#num#	k4
–	–	k?
hrtanová	hrtanový	k2eAgFnSc1d1
záklopka	záklopka	k1gFnSc1
(	(	kIx(
<g/>
epiglottis	epiglottis	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
9	#num#	k4
–	–	k?
hlasivková	hlasivkový	k2eAgFnSc1d1
štěrbina	štěrbina	k1gFnSc1
(	(	kIx(
<g/>
glottis	glottis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Překážky	překážka	k1gFnPc1
proudění	proudění	k1gNnSc2
vzduchu	vzduch	k1gInSc2
<g/>
,	,	kIx,
způsobující	způsobující	k2eAgFnSc6d1
tvorbu	tvorba	k1gFnSc4
souhláskového	souhláskový	k2eAgInSc2d1
šumu	šum	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
vytvářet	vytvářet	k5eAaImF
na	na	k7c6
různých	různý	k2eAgNnPc6d1
místech	místo	k1gNnPc6
artikulačního	artikulační	k2eAgNnSc2d1
ústrojí	ústrojí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
rovněž	rovněž	k9
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
při	při	k7c6
klasifikaci	klasifikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc4d1
rozdělení	rozdělení	k1gNnSc1
souhlásek	souhláska	k1gFnPc2
používá	používat	k5eAaImIp3nS
názvosloví	názvosloví	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
není	být	k5eNaImIp3nS
zcela	zcela	k6eAd1
systematické	systematický	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
částečně	částečně	k6eAd1
pojmenovává	pojmenovávat	k5eAaImIp3nS
oba	dva	k4xCgInPc4
artikulátory	artikulátor	k1gInPc4
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
jen	jen	k9
jeden	jeden	k4xCgInSc1
z	z	k7c2
artikulátorů	artikulátor	k1gInPc2
zapojených	zapojený	k2eAgInPc2d1
do	do	k7c2
tvoření	tvoření	k1gNnSc2
hlásek	hláska	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
toto	tento	k3xDgNnSc1
rozdělení	rozdělení	k1gNnSc1
je	být	k5eAaImIp3nS
vžité	vžitý	k2eAgNnSc1d1
a	a	k8xC
všeobecně	všeobecně	k6eAd1
užívané	užívaný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
České	český	k2eAgFnPc1d1
souhlásky	souhláska	k1gFnPc1
se	se	k3xPyFc4
z	z	k7c2
tohoto	tento	k3xDgNnSc2
hlediska	hledisko	k1gNnSc2
dělí	dělit	k5eAaImIp3nS
do	do	k7c2
7	#num#	k4
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	nejvíce	k6eAd1,k6eAd3
souhlásek	souhláska	k1gFnPc2
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
tzv.	tzv.	kA
prealveolární	prealveolární	k2eAgFnPc1d1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
tvoří	tvořit	k5eAaImIp3nP
přiblížením	přiblížení	k1gNnSc7
nebo	nebo	k8xC
kontaktem	kontakt	k1gInSc7
špičky	špička	k1gFnSc2
jazyka	jazyk	k1gInSc2
s	s	k7c7
přední	přední	k2eAgFnSc7d1
částí	část	k1gFnSc7
dásňového	dásňový	k2eAgInSc2d1
oblouku	oblouk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Místo	místo	k7c2
artikulace	artikulace	k1gFnSc2
</s>
<s>
Zúčastněné	zúčastněný	k2eAgInPc1d1
artikulátory	artikulátor	k1gInPc1
</s>
<s>
Souhlásky	souhláska	k1gFnPc1
</s>
<s>
obouretné	obouretný	k2eAgInPc4d1
(	(	kIx(
<g/>
bilabiální	bilabiální	k2eAgInPc4d1
<g/>
)	)	kIx)
</s>
<s>
oba	dva	k4xCgInPc4
rty	ret	k1gInPc4
</s>
<s>
m	m	kA
<g/>
,	,	kIx,
p	p	k?
<g/>
,	,	kIx,
b	b	k?
</s>
<s>
retozubné	retozubný	k2eAgInPc4d1
(	(	kIx(
<g/>
labiodentální	labiodentální	k2eAgInPc4d1
<g/>
)	)	kIx)
</s>
<s>
dolní	dolní	k2eAgInSc1d1
ret	ret	k1gInSc1
a	a	k8xC
horní	horní	k2eAgInPc1d1
zuby	zub	k1gInPc1
</s>
<s>
f	f	k?
<g/>
,	,	kIx,
v	v	k7c6
<g/>
;	;	kIx,
nefonemické	fonemický	k2eNgFnSc6d1
<g/>
:	:	kIx,
ɱ	ɱ	k?
</s>
<s>
předodásňové	předodásňová	k1gFnPc1
((	((	k?
<g/>
pre	pre	k?
<g/>
)	)	kIx)
<g/>
alveolární	alveolární	k2eAgFnPc1d1
<g/>
)	)	kIx)
</s>
<s>
přední	přední	k2eAgFnSc1d1
část	část	k1gFnSc1
horních	horní	k2eAgInPc2d1
dásňových	dásňový	k2eAgInPc2d1
výstupků	výstupek	k1gInPc2
a	a	k8xC
jazyk	jazyk	k1gInSc1
</s>
<s>
n	n	k0
<g/>
,	,	kIx,
t	t	k?
<g/>
,	,	kIx,
d	d	k?
<g/>
,	,	kIx,
c	c	k0
<g/>
,	,	kIx,
ʒ	ʒ	k?
<g/>
,	,	kIx,
s	s	k7c7
<g/>
,	,	kIx,
z	z	k7c2
<g/>
,	,	kIx,
r	r	kA
<g/>
,	,	kIx,
ř	ř	k?
<g/>
,	,	kIx,
l	l	kA
</s>
<s>
zadodásňové	zadodásň	k1gMnPc1
(	(	kIx(
<g/>
postalveolární	postalveolární	k2eAgMnPc1d1
<g/>
)	)	kIx)
</s>
<s>
zadní	zadní	k2eAgFnSc1d1
část	část	k1gFnSc1
horních	horní	k2eAgInPc2d1
dásňových	dásňový	k2eAgInPc2d1
výstupků	výstupek	k1gInPc2
a	a	k8xC
jazyk	jazyk	k1gInSc1
</s>
<s>
č	č	k0
<g/>
,	,	kIx,
ǯ	ǯ	k?
<g/>
,	,	kIx,
š	š	k?
<g/>
,	,	kIx,
ž	ž	k?
</s>
<s>
předopatrové	předopatr	k1gMnPc1
(	(	kIx(
<g/>
palatální	palatální	k2eAgMnPc1d1
<g/>
)	)	kIx)
</s>
<s>
tvrdé	tvrdý	k2eAgNnSc1d1
patro	patro	k1gNnSc1
a	a	k8xC
jazyk	jazyk	k1gInSc1
</s>
<s>
ň	ň	k?
<g/>
,	,	kIx,
ť	ť	k?
<g/>
,	,	kIx,
ď	ď	k?
<g/>
,	,	kIx,
j	j	k?
</s>
<s>
zadopatrové	zadopatr	k1gMnPc1
(	(	kIx(
<g/>
velární	velární	k2eAgMnPc1d1
<g/>
)	)	kIx)
</s>
<s>
měkké	měkký	k2eAgNnSc1d1
patro	patro	k1gNnSc1
a	a	k8xC
jazyk	jazyk	k1gInSc1
</s>
<s>
k	k	k7c3
<g/>
,	,	kIx,
g	g	kA
<g/>
,	,	kIx,
x	x	k?
<g/>
;	;	kIx,
nefonemické	fonemický	k2eNgFnSc2d1
<g/>
:	:	kIx,
ŋ	ŋ	k?
</s>
<s>
hlasivkové	hlasivkový	k2eAgInPc4d1
(	(	kIx(
<g/>
glotální	glotální	k2eAgInPc4d1
<g/>
)	)	kIx)
</s>
<s>
hlasivky	hlasivka	k1gFnPc4
</s>
<s>
h	h	k?
<g/>
;	;	kIx,
nefonemické	fonemický	k2eNgFnSc2d1
<g/>
:	:	kIx,
ʔ	ʔ	k?
(	(	kIx(
<g/>
ráz	ráz	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Sekundární	sekundární	k2eAgFnSc1d1
artikulace	artikulace	k1gFnSc1
</s>
<s>
Kromě	kromě	k7c2
základního	základní	k2eAgNnSc2d1
místa	místo	k1gNnSc2
tvoření	tvoření	k1gNnSc2
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
uplatňovat	uplatňovat	k5eAaImF
i	i	k9
činnost	činnost	k1gFnSc4
dalších	další	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Nejdůležitější	důležitý	k2eAgNnSc1d3
je	být	k5eAaImIp3nS
v	v	k7c6
tomto	tento	k3xDgInSc6
ohledu	ohled	k1gInSc6
činnost	činnost	k1gFnSc4
hlasivek	hlasivka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
pravých	pravý	k2eAgFnPc2d1
souhlásek	souhláska	k1gFnPc2
je	být	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc1
aktivní	aktivní	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
(	(	kIx(
<g/>
fonace	fonace	k1gFnSc1
<g/>
)	)	kIx)
rozlišujícím	rozlišující	k2eAgInSc7d1
rysem	rys	k1gInSc7
–	–	k?
tyto	tento	k3xDgFnPc4
souhlásky	souhláska	k1gFnPc4
tvoří	tvořit	k5eAaImIp3nS
dvojice	dvojice	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
jeden	jeden	k4xCgMnSc1
z	z	k7c2
členů	člen	k1gMnPc2
je	být	k5eAaImIp3nS
znělý	znělý	k2eAgInSc1d1
(	(	kIx(
<g/>
hlasivky	hlasivka	k1gFnPc1
kmitají	kmitat	k5eAaImIp3nP
<g/>
)	)	kIx)
a	a	k8xC
druhý	druhý	k4xOgInSc1
neznělý	znělý	k2eNgInSc1d1
(	(	kIx(
<g/>
hlasivky	hlasivka	k1gFnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
klidu	klid	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
jedinečných	jedinečný	k2eAgFnPc2d1
souhlásek	souhláska	k1gFnPc2
je	být	k5eAaImIp3nS
znělá	znělý	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
základní	základní	k2eAgFnSc1d1
<g/>
,	,	kIx,
neznělá	znělý	k2eNgFnSc1d1
realizace	realizace	k1gFnSc1
u	u	k7c2
nich	on	k3xPp3gMnPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
jen	jen	k6eAd1
náhodná	náhodný	k2eAgFnSc1d1
a	a	k8xC
nerozlišuje	rozlišovat	k5eNaImIp3nS
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znělost	znělost	k1gFnSc1
nemá	mít	k5eNaImIp3nS
u	u	k7c2
jedinečných	jedinečný	k2eAgFnPc2d1
souhlásek	souhláska	k1gFnPc2
fonologickou	fonologický	k2eAgFnSc4d1
platnost	platnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1
znělých	znělý	k2eAgFnPc2d1
souhlásek	souhláska	k1gFnPc2
je	být	k5eAaImIp3nS
doprovázena	doprovázet	k5eAaImNgFnS
vyšším	vysoký	k2eAgNnSc7d2
napětím	napětí	k1gNnSc7
svalů	sval	k1gInPc2
artikulačního	artikulační	k2eAgNnSc2d1
ústrojí	ústrojí	k1gNnSc2
–	–	k?
znělé	znělý	k2eAgFnSc2d1
souhlásky	souhláska	k1gFnSc2
jsou	být	k5eAaImIp3nP
napjaté	napjatý	k2eAgNnSc1d1
(	(	kIx(
<g/>
fortisové	fortisový	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
neznělé	znělý	k2eNgFnSc3d1
nenapjaté	napjatý	k2eNgFnSc3d1
(	(	kIx(
<g/>
lenisové	lenisový	k2eAgFnSc3d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
rozdíl	rozdíl	k1gInSc4
si	se	k3xPyFc3
nejlépe	dobře	k6eAd3
uvědomíme	uvědomit	k5eAaPmIp1nP
při	při	k7c6
šeptání	šeptání	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
neuplatňuje	uplatňovat	k5eNaImIp3nS
činnost	činnost	k1gFnSc1
hlasivek	hlasivka	k1gFnPc2
<g/>
,	,	kIx,
přesto	přesto	k8xC
však	však	k9
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
sluchem	sluch	k1gInSc7
rozlišit	rozlišit	k5eAaPmF
znělé	znělý	k2eAgFnPc4d1
a	a	k8xC
neznělé	znělý	k2eNgFnPc4d1
souhlásky	souhláska	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rys	rys	k1gInSc1
napjatosti	napjatost	k1gFnSc2
není	být	k5eNaImIp3nS
pro	pro	k7c4
češtinu	čeština	k1gFnSc4
fonologicky	fonologicky	k6eAd1
relevantní	relevantní	k2eAgMnSc1d1
<g/>
,	,	kIx,
za	za	k7c4
základní	základní	k2eAgInSc4d1
znak	znak	k1gInSc4
se	se	k3xPyFc4
bere	brát	k5eAaImIp3nS
znělost	znělost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
některých	některý	k3yIgFnPc2
souhlásek	souhláska	k1gFnPc2
/	/	kIx~
<g/>
š	š	k?
<g/>
,	,	kIx,
ž	ž	k?
<g/>
,	,	kIx,
č	č	k0
<g/>
,	,	kIx,
ǯ	ǯ	k?
<g/>
,	,	kIx,
ř	ř	k?
<g/>
/	/	kIx~
je	být	k5eAaImIp3nS
výsledný	výsledný	k2eAgInSc1d1
zvuk	zvuk	k1gInSc1
dotvářen	dotvářen	k2eAgInSc1d1
mírným	mírný	k2eAgNnSc7d1
zaokrouhlením	zaokrouhlení	k1gNnSc7
rtů	ret	k1gInPc2
(	(	kIx(
<g/>
labializací	labializace	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
rys	rys	k1gInSc1
však	však	k9
není	být	k5eNaImIp3nS
podstatný	podstatný	k2eAgInSc1d1
pro	pro	k7c4
rozlišení	rozlišení	k1gNnSc4
významu	význam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInPc1d1
sekundární	sekundární	k2eAgInPc1d1
způsoby	způsob	k1gInPc1
artikulace	artikulace	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
palatalizace	palatalizace	k1gFnSc2
<g/>
,	,	kIx,
velarizace	velarizace	k1gFnSc2
atd.	atd.	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
ve	v	k7c6
spisovné	spisovný	k2eAgFnSc6d1
výslovnosti	výslovnost	k1gFnSc6
neuplatňují	uplatňovat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Akustika	akustika	k1gFnSc1
</s>
<s>
Z	z	k7c2
akustického	akustický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
jsou	být	k5eAaImIp3nP
souhlásky	souhláska	k1gFnPc1
dosti	dosti	k6eAd1
heterogenní	heterogenní	k2eAgFnSc2d1
skupinou	skupina	k1gFnSc7
zvuků	zvuk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnou	společný	k2eAgFnSc7d1
vlastností	vlastnost	k1gFnSc7
je	být	k5eAaImIp3nS
přítomnost	přítomnost	k1gFnSc1
šumu	šum	k1gInSc3
<g/>
,	,	kIx,
tj.	tj.	kA
nepravidelných	pravidelný	k2eNgInPc2d1
kmitů	kmit	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
vlastnost	vlastnost	k1gFnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
konsonantnost	konsonantnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgInSc1d1
rozdíl	rozdíl	k1gInSc1
od	od	k7c2
samohlásek	samohláska	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pravé	pravý	k2eAgFnPc1d1
a	a	k8xC
jedinečné	jedinečný	k2eAgFnPc1d1
souhlásky	souhláska	k1gFnPc1
<g/>
,	,	kIx,
znělost	znělost	k1gFnSc1
</s>
<s>
Jako	jako	k8xC,k8xS
pravé	pravý	k2eAgFnPc4d1
souhlásky	souhláska	k1gFnPc4
se	se	k3xPyFc4
označují	označovat	k5eAaImIp3nP
nevokální	vokální	k2eNgFnPc1d1
souhlásky	souhláska	k1gFnPc1
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
mírou	míra	k1gFnSc7
šumu	šum	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
češtině	čeština	k1gFnSc6
vytvářejí	vytvářet	k5eAaImIp3nP
korelační	korelační	k2eAgInPc1d1
páry	pár	k1gInPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc7
členy	člen	k1gInPc7
se	se	k3xPyFc4
od	od	k7c2
sebe	se	k3xPyFc2
liší	lišit	k5eAaImIp3nP
znělostí	znělost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
této	tento	k3xDgFnSc2
skupiny	skupina	k1gFnSc2
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
okluzivy	okluzivo	k1gNnPc7
(	(	kIx(
<g/>
explozivy	exploziv	k1gInPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
semiokluzivy	semiokluziva	k1gFnPc4
(	(	kIx(
<g/>
afrikáty	afrikáta	k1gFnPc4
<g/>
)	)	kIx)
a	a	k8xC
konstriktivy	konstriktiva	k1gFnPc1
(	(	kIx(
<g/>
frikativy	frikativa	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tj.	tj.	kA
/	/	kIx~
<g/>
p	p	k?
<g/>
,	,	kIx,
b	b	k?
<g/>
,	,	kIx,
t	t	k?
<g/>
,	,	kIx,
d	d	k?
<g/>
,	,	kIx,
k	k	k7c3
<g/>
,	,	kIx,
g	g	kA
<g/>
,	,	kIx,
f	f	k?
<g/>
,	,	kIx,
v	v	k7c6
<g/>
,	,	kIx,
s	s	k7c7
<g/>
,	,	kIx,
z	z	k0
<g/>
,	,	kIx,
š	š	k?
<g/>
,	,	kIx,
ž	ž	k?
<g/>
,	,	kIx,
x	x	k?
<g/>
,	,	kIx,
h	h	k?
<g/>
,	,	kIx,
ř	ř	k?
<g/>
,	,	kIx,
c	c	k0
<g/>
,	,	kIx,
ʒ	ʒ	k?
<g/>
,	,	kIx,
č	č	k0
<g/>
,	,	kIx,
ǯ	ǯ	k?
<g/>
/	/	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
sem	sem	k6eAd1
i	i	k9
ráz	ráz	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
však	však	k9
nemá	mít	k5eNaImIp3nS
znělý	znělý	k2eAgInSc1d1
protějšek	protějšek	k1gInSc1
(	(	kIx(
<g/>
není	být	k5eNaImIp3nS
realizovatelný	realizovatelný	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znělé	znělý	k2eAgFnSc2d1
pravé	pravý	k2eAgFnSc2d1
souhlásky	souhláska	k1gFnSc2
se	se	k3xPyFc4
vyznačují	vyznačovat	k5eAaImIp3nP
přítomností	přítomnost	k1gFnSc7
tónu	tón	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
však	však	k9
jiné	jiný	k2eAgFnPc4d1
kvality	kvalita	k1gFnPc4
než	než	k8xS
u	u	k7c2
samohlásek	samohláska	k1gFnPc2
a	a	k8xC
jedinečných	jedinečný	k2eAgFnPc2d1
souhlásek	souhláska	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xC,k8xS
jedinečné	jedinečný	k2eAgFnPc1d1
souhlásky	souhláska	k1gFnPc1
se	se	k3xPyFc4
označují	označovat	k5eAaImIp3nP
souhlásky	souhláska	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
vždy	vždy	k6eAd1
znělé	znělý	k2eAgInPc1d1
(	(	kIx(
<g/>
netvoří	tvořit	k5eNaImIp3nP
znělostní	znělostní	k2eAgInPc1d1
páry	pár	k1gInPc1
<g/>
)	)	kIx)
a	a	k8xC
vyznačují	vyznačovat	k5eAaImIp3nP
se	se	k3xPyFc4
vokálností	vokálnost	k1gFnSc7
–	–	k?
přítomností	přítomnost	k1gFnSc7
vyšších	vysoký	k2eAgFnPc2d2
frekvencí	frekvence	k1gFnSc7
<g/>
,	,	kIx,
tzv.	tzv.	kA
formantů	formans	k1gInPc2
(	(	kIx(
<g/>
F	F	kA
<g/>
1	#num#	k4
<g/>
,	,	kIx,
F2	F2	k1gFnSc1
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
spektru	spektrum	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
češtině	čeština	k1gFnSc6
jde	jít	k5eAaImIp3nS
o	o	k7c4
/	/	kIx~
<g/>
m	m	kA
<g/>
,	,	kIx,
n	n	k0
<g/>
,	,	kIx,
ň	ň	k?
<g/>
,	,	kIx,
r	r	kA
<g/>
,	,	kIx,
l	l	kA
<g/>
/	/	kIx~
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
alofony	alofon	k1gInPc1
[	[	kIx(
<g/>
ɱ	ɱ	k?
<g/>
,	,	kIx,
ŋ	ŋ	k?
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nazývají	nazývat	k5eAaImIp3nP
se	se	k3xPyFc4
též	též	k9
sonory	sonora	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
jedinečným	jedinečný	k2eAgFnPc3d1
souhláskám	souhláska	k1gFnPc3
patří	patřit	k5eAaImIp3nS
i	i	k9
klouzavé	klouzavý	k2eAgInPc1d1
hlásky	hlásek	k1gInPc1
/	/	kIx~
<g/>
j	j	k?
<g/>
,	,	kIx,
ʊ	ʊ	k?
<g/>
̯	̯	k?
<g/>
/	/	kIx~
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
svým	svůj	k3xOyFgInSc7
charakterem	charakter	k1gInSc7
polovokály	polovokál	k1gInPc7
(	(	kIx(
<g/>
blízké	blízký	k2eAgFnSc2d1
samohláskám	samohláska	k1gFnPc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemají	mít	k5eNaImIp3nP
příznak	příznak	k1gInSc4
konsonantnosti	konsonantnost	k1gFnSc2
ani	ani	k8xC
vokálnosti	vokálnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
jsou	být	k5eAaImIp3nP
vždy	vždy	k6eAd1
znělé	znělý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Znělost	znělost	k1gFnSc1
je	být	k5eAaImIp3nS
dána	dát	k5eAaPmNgFnS
aktivní	aktivní	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
hlasivek	hlasivka	k1gFnPc2
(	(	kIx(
<g/>
fonací	fonace	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
tvoří	tvořit	k5eAaImIp3nP
základní	základní	k2eAgFnSc4d1
frekvenci	frekvence	k1gFnSc4
F	F	kA
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znělé	znělý	k2eAgFnPc4d1
souhlásky	souhláska	k1gFnPc4
se	se	k3xPyFc4
vyznačují	vyznačovat	k5eAaImIp3nP
přítomností	přítomnost	k1gFnSc7
tónu	tón	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vlastnost	vlastnost	k1gFnSc1
</s>
<s>
Samohláska	samohláska	k1gFnSc1
</s>
<s>
Souhláska	souhláska	k1gFnSc1
</s>
<s>
klouzavá	klouzavý	k2eAgFnSc1d1
</s>
<s>
jedinečná	jedinečný	k2eAgFnSc1d1
</s>
<s>
praváznělá	praváznělý	k2eAgFnSc1d1
</s>
<s>
praváneznělá	praváneznělý	k2eAgFnSc1d1
</s>
<s>
znělost	znělost	k1gFnSc1
(	(	kIx(
<g/>
F	F	kA
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
++++	++++	k?
<g/>
-	-	kIx~
</s>
<s>
vokálnost	vokálnost	k1gFnSc1
(	(	kIx(
<g/>
F	F	kA
<g/>
1	#num#	k4
<g/>
,	,	kIx,
F2	F2	k1gFnSc1
…	…	k?
<g/>
)	)	kIx)
<g/>
+	+	kIx~
<g/>
-	-	kIx~
<g/>
+	+	kIx~
<g/>
--	--	k?
</s>
<s>
konsonantnost	konsonantnost	k1gFnSc1
(	(	kIx(
<g/>
šum	šum	k1gInSc1
<g/>
)	)	kIx)
<g/>
--	--	k?
<g/>
+++	+++	k?
</s>
<s>
Zástupci	zástupce	k1gMnPc1
v	v	k7c6
češtině	čeština	k1gFnSc6
</s>
<s>
a	a	k8xC
<g/>
,	,	kIx,
e	e	k0
<g/>
,	,	kIx,
i	i	k9
<g/>
,	,	kIx,
o	o	k0
<g/>
,	,	kIx,
uá	uá	k?
<g/>
,	,	kIx,
é	é	k0
<g/>
,	,	kIx,
í	í	k0
<g/>
,	,	kIx,
ó	ó	k0
<g/>
,	,	kIx,
ú	ú	k0
</s>
<s>
j	j	k?
<g/>
,	,	kIx,
ʊ	ʊ	k?
<g/>
̯	̯	k?
</s>
<s>
m	m	kA
<g/>
,	,	kIx,
n	n	k0
<g/>
,	,	kIx,
ň	ň	k?
<g/>
,	,	kIx,
r	r	kA
<g/>
,	,	kIx,
lɱ	lɱ	k?
<g/>
,	,	kIx,
ŋ	ŋ	k?
</s>
<s>
b	b	k?
<g/>
,	,	kIx,
d	d	k?
<g/>
,	,	kIx,
g	g	kA
<g/>
,	,	kIx,
v	v	k7c6
<g/>
,	,	kIx,
zž	zž	k?
<g/>
,	,	kIx,
h	h	k?
<g/>
,	,	kIx,
ř	ř	k?
<g/>
,	,	kIx,
ʒ	ʒ	k?
<g/>
,	,	kIx,
ǯ	ǯ	k?
</s>
<s>
p	p	k?
<g/>
,	,	kIx,
t	t	k?
<g/>
,	,	kIx,
k	k	k7c3
<g/>
,	,	kIx,
f	f	k?
<g/>
,	,	kIx,
sš	sš	k?
<g/>
,	,	kIx,
x	x	k?
<g/>
,	,	kIx,
ř	ř	k?
<g/>
,	,	kIx,
c	c	k0
<g/>
,	,	kIx,
čʔ	čʔ	k?
</s>
<s>
Pro	pro	k7c4
srovnání	srovnání	k1gNnSc4
uvádí	uvádět	k5eAaImIp3nS
tabulka	tabulka	k1gFnSc1
i	i	k8xC
samohlásky	samohláska	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Kompaktnost	kompaktnost	k1gFnSc1
–	–	k?
nekompaktnost	nekompaktnost	k1gFnSc1
</s>
<s>
Kompaktní	kompaktní	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
hlásky	hlásek	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
mají	mít	k5eAaImIp3nP
zvukovou	zvukový	k2eAgFnSc4d1
energii	energie	k1gFnSc4
soustředěnou	soustředěný	k2eAgFnSc4d1
uprostřed	uprostřed	k7c2
spektra	spektrum	k1gNnSc2
<g/>
,	,	kIx,
nekompaktní	kompaktní	k2eNgFnPc1d1
mají	mít	k5eAaImIp3nP
toto	tento	k3xDgNnSc4
maximum	maximum	k1gNnSc4
posunuto	posunut	k2eAgNnSc4d1
k	k	k7c3
hornímu	horní	k2eAgMnSc3d1
nebo	nebo	k8xC
dolnímu	dolní	k2eAgInSc3d1
okraji	okraj	k1gInSc3
spektra	spektrum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kompaktní	kompaktní	k2eAgFnPc4d1
souhlásky	souhláska	k1gFnPc4
se	se	k3xPyFc4
vyslovují	vyslovovat	k5eAaImIp3nP
v	v	k7c6
zadní	zadní	k2eAgFnSc6d1
části	část	k1gFnSc6
ústní	ústní	k2eAgFnSc2d1
dutiny	dutina	k1gFnSc2
(	(	kIx(
<g/>
od	od	k7c2
postalveolár	postalveolár	k1gInSc4
až	až	k9
po	po	k7c4
glotály	glotál	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgMnPc1d1
jsou	být	k5eAaImIp3nP
nekompaktní	kompaktní	k2eNgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Drsnost	drsnost	k1gFnSc1
–	–	k?
matnost	matnost	k1gFnSc1
</s>
<s>
Drsné	drsný	k2eAgFnPc1d1
souhlásky	souhláska	k1gFnPc1
se	se	k3xPyFc4
vyznačují	vyznačovat	k5eAaImIp3nP
výraznými	výrazný	k2eAgFnPc7d1
nepravidelnostmi	nepravidelnost	k1gFnPc7
složek	složka	k1gFnPc2
zvuku	zvuk	k1gInSc2
<g/>
,	,	kIx,
velkým	velký	k2eAgInSc7d1
třecím	třecí	k2eAgInSc7d1
šumem	šum	k1gInSc7
způsobeným	způsobený	k2eAgInSc7d1
turbulencemi	turbulence	k1gFnPc7
vzduchu	vzduch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drsné	drsný	k2eAgFnPc4d1
jsou	být	k5eAaImIp3nP
frikativy	frikativa	k1gFnPc1
a	a	k8xC
afrikáty	afrikáta	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgFnPc4d1
souhlásky	souhláska	k1gFnPc4
jsou	být	k5eAaImIp3nP
matné	matný	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyznačují	vyznačovat	k5eAaImIp3nP
se	s	k7c7
sourodým	sourodý	k2eAgNnSc7d1
zvukovým	zvukový	k2eAgNnSc7d1
spektrem	spektrum	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Drsnost	drsnost	k1gFnSc1
a	a	k8xC
matnost	matnost	k1gFnSc1
tvoří	tvořit	k5eAaImIp3nS
fonologickou	fonologický	k2eAgFnSc4d1
opozici	opozice	k1gFnSc4
mezi	mezi	k7c7
explozivami	exploziva	k1gFnPc7
a	a	k8xC
afrikátami	afrikáta	k1gFnPc7
<g/>
:	:	kIx,
/	/	kIx~
<g/>
t	t	k?
<g/>
/	/	kIx~
<g/>
~	~	kIx~
<g/>
/	/	kIx~
<g/>
c	c	k0
<g/>
/	/	kIx~
<g/>
,	,	kIx,
/	/	kIx~
<g/>
d	d	k?
<g/>
/	/	kIx~
<g/>
~	~	kIx~
<g/>
/	/	kIx~
<g/>
ʒ	ʒ	k?
<g/>
/	/	kIx~
<g/>
,	,	kIx,
/	/	kIx~
<g/>
ť	ť	k?
<g/>
/	/	kIx~
<g/>
~	~	kIx~
<g/>
/	/	kIx~
<g/>
č	č	k0
<g/>
/	/	kIx~
<g/>
,	,	kIx,
/	/	kIx~
<g/>
ď	ď	k?
<g/>
/	/	kIx~
<g/>
~	~	kIx~
<g/>
/	/	kIx~
<g/>
ǯ	ǯ	k?
<g/>
/	/	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
ostatních	ostatní	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
souhlásek	souhláska	k1gFnPc2
není	být	k5eNaImIp3nS
tento	tento	k3xDgInSc1
rys	rys	k1gInSc1
relevantní	relevantní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Kontinuálnost	Kontinuálnost	k1gFnSc1
–	–	k?
nekontinuálnost	nekontinuálnost	k1gFnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
vlastnost	vlastnost	k1gFnSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
časového	časový	k2eAgNnSc2d1
trvání	trvání	k1gNnSc2
artikulace	artikulace	k1gFnSc2
a	a	k8xC
výsledného	výsledný	k2eAgInSc2d1
zvuku	zvuk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontinuální	kontinuální	k2eAgFnPc4d1
(	(	kIx(
<g/>
nepřerušené	přerušený	k2eNgFnPc4d1
<g/>
)	)	kIx)
souhlásky	souhláska	k1gFnPc4
lze	lze	k6eAd1
vyslovovat	vyslovovat	k5eAaImF
relativně	relativně	k6eAd1
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nekontinuální	kontinuální	k2eNgMnSc1d1
mají	mít	k5eAaImIp3nP
během	během	k7c2
své	svůj	k3xOyFgFnSc2
artikulace	artikulace	k1gFnSc2
úplný	úplný	k2eAgInSc4d1
závěr	závěr	k1gInSc4
(	(	kIx(
<g/>
okluzi	okluze	k1gFnSc4
<g/>
)	)	kIx)
s	s	k7c7
následnou	následný	k2eAgFnSc7d1
explozí	exploze	k1gFnSc7
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
o	o	k7c4
explozivy	exploziv	k1gInPc4
(	(	kIx(
<g/>
okluzivy	okluziva	k1gFnPc4
<g/>
)	)	kIx)
orální	orální	k2eAgFnPc4d1
i	i	k8xC
nazální	nazální	k2eAgFnPc4d1
a	a	k8xC
afrikáty	afrikáta	k1gFnPc4
(	(	kIx(
<g/>
semiokluzivy	semiokluziva	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Gravisovost	Gravisovost	k1gFnSc1
–	–	k?
akutovost	akutovost	k1gFnSc1
</s>
<s>
Gravisové	Gravisový	k2eAgFnPc4d1
(	(	kIx(
<g/>
tupé	tupý	k2eAgFnPc4d1
<g/>
)	)	kIx)
souhlásky	souhláska	k1gFnPc4
zahrnují	zahrnovat	k5eAaImIp3nP
nízké	nízký	k2eAgFnPc4d1
frekvence	frekvence	k1gFnPc4
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
spektru	spektrum	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	s	k7c7
přední	přední	k2eAgFnSc7d1
(	(	kIx(
<g/>
bilabiály	bilabiála	k1gFnPc4
a	a	k8xC
labiodentály	labiodentála	k1gFnPc4
<g/>
)	)	kIx)
a	a	k8xC
zadní	zadní	k2eAgFnPc1d1
(	(	kIx(
<g/>
veláry	velára	k1gFnPc1
<g/>
,	,	kIx,
glotály	glotál	k1gInPc1
<g/>
)	)	kIx)
souhlásky	souhláska	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Akutové	akutový	k2eAgFnPc4d1
(	(	kIx(
<g/>
ostré	ostrý	k2eAgFnPc4d1
<g/>
)	)	kIx)
souhlásky	souhláska	k1gFnPc4
jsou	být	k5eAaImIp3nP
všechny	všechen	k3xTgMnPc4
ostatní	ostatní	k2eAgMnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
spektru	spektrum	k1gNnSc6
převážně	převážně	k6eAd1
vysoké	vysoký	k2eAgFnSc2d1
frekvence	frekvence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Délka	délka	k1gFnSc1
</s>
<s>
Artikulace	artikulace	k1gFnSc1
každé	každý	k3xTgFnSc2
souhlásky	souhláska	k1gFnSc2
se	se	k3xPyFc4
vyznačuje	vyznačovat	k5eAaImIp3nS
objektivně	objektivně	k6eAd1
změřitelnou	změřitelný	k2eAgFnSc7d1
délkou	délka	k1gFnSc7
trvání	trvání	k1gNnSc4
(	(	kIx(
<g/>
kvantitou	kvantita	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
ovlivněna	ovlivnit	k5eAaPmNgFnS
mnoha	mnoho	k4c7
faktory	faktor	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1
délka	délka	k1gFnSc1
představuje	představovat	k5eAaImIp3nS
mezní	mezní	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
dodržení	dodržení	k1gNnSc4
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
pro	pro	k7c4
rozlišení	rozlišení	k1gNnSc4
dané	daný	k2eAgFnSc2d1
hlásky	hláska	k1gFnSc2
posluchačem	posluchač	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrné	průměrný	k2eAgFnPc1d1
hodnoty	hodnota	k1gFnPc1
však	však	k9
bývají	bývat	k5eAaImIp3nP
delší	dlouhý	k2eAgInPc1d2
<g/>
.	.	kIx.
</s>
<s>
Nejkratší	krátký	k2eAgNnPc1d3
trvání	trvání	k1gNnPc1
v	v	k7c6
češtině	čeština	k1gFnSc6
mají	mít	k5eAaImIp3nP
souhlásky	souhláska	k1gFnPc1
[	[	kIx(
<g/>
r	r	kA
<g/>
]	]	kIx)
a	a	k8xC
[	[	kIx(
<g/>
l	l	kA
<g/>
]	]	kIx)
<g/>
,	,	kIx,
cca	cca	kA
100	#num#	k4
ms.	ms.	k?
Obecně	obecně	k6eAd1
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
konstriktivy	konstriktiva	k1gFnPc1
(	(	kIx(
<g/>
např.	např.	kA
[	[	kIx(
<g/>
s	s	k7c7
<g/>
]	]	kIx)
cca	cca	kA
260	#num#	k4
ms	ms	k?
<g/>
)	)	kIx)
trvají	trvat	k5eAaImIp3nP
déle	dlouho	k6eAd2
než	než	k8xS
okluzivy	okluziva	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
[	[	kIx(
<g/>
c	c	k0
<g/>
]	]	kIx)
(	(	kIx(
<g/>
ť	ť	k?
<g/>
)	)	kIx)
cca	cca	kA
160	#num#	k4
ms	ms	k?
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
vlastní	vlastnit	k5eAaImIp3nS
ploze	ploze	k6eAd1
cca	cca	kA
10	#num#	k4
ms	ms	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neznělé	znělý	k2eNgFnPc4d1
souhlásky	souhláska	k1gFnPc4
trvají	trvat	k5eAaImIp3nP
déle	dlouho	k6eAd2
než	než	k8xS
znělé	znělý	k2eAgNnSc1d1
<g/>
,	,	kIx,
k	k	k7c3
jejichž	jejichž	k3xOyRp3gFnSc3
artikulaci	artikulace	k1gFnSc3
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
vynaložit	vynaložit	k5eAaPmF
více	hodně	k6eAd2
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Záleží	záležet	k5eAaImIp3nS
též	též	k9
na	na	k7c4
pozici	pozice	k1gFnSc4
souhlásky	souhláska	k1gFnSc2
ve	v	k7c6
slově	slovo	k1gNnSc6
<g/>
,	,	kIx,
např.	např.	kA
[	[	kIx(
<g/>
ʃ	ʃ	k?
<g/>
]	]	kIx)
(	(	kIx(
<g/>
š	š	k?
<g/>
)	)	kIx)
před	před	k7c7
samohláskou	samohláska	k1gFnSc7
trvá	trvat	k5eAaImIp3nS
asi	asi	k9
180	#num#	k4
ms	ms	k?
<g/>
,	,	kIx,
mezi	mezi	k7c7
samohláskami	samohláska	k1gFnPc7
235	#num#	k4
ms	ms	k?
a	a	k8xC
na	na	k7c6
konci	konec	k1gInSc6
slova	slovo	k1gNnSc2
asi	asi	k9
290	#num#	k4
ms.	ms.	k?
</s>
<s>
K	k	k7c3
prodloužení	prodloužení	k1gNnSc3
souhlásek	souhláska	k1gFnPc2
dochází	docházet	k5eAaImIp3nS
např.	např.	kA
při	při	k7c6
emocionálně	emocionálně	k6eAd1
laděných	laděný	k2eAgInPc6d1
projevech	projev	k1gInPc6
<g/>
,	,	kIx,
při	při	k7c6
zdůraznění	zdůraznění	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
citoslovcích	citoslovce	k1gNnPc6
apod.	apod.	kA
Jako	jako	k8xC,k8xS
dlouhá	dlouhý	k2eAgFnSc1d1
souhláska	souhláska	k1gFnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
ve	v	k7c6
spojité	spojitý	k2eAgFnSc3d1
výslovnosti	výslovnost	k1gFnSc3
jevit	jevit	k5eAaImF
zdvojená	zdvojený	k2eAgFnSc1d1
souhláska	souhláska	k1gFnSc1
na	na	k7c4
hranici	hranice	k1gFnSc4
morfémů	morfém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prodloužené	prodloužená	k1gFnSc2
trvání	trvání	k1gNnSc2
se	se	k3xPyFc4
někdy	někdy	k6eAd1
v	v	k7c6
písmu	písmo	k1gNnSc6
označuje	označovat	k5eAaImIp3nS
opakování	opakování	k1gNnSc1
grafému	grafém	k1gInSc2
(	(	kIx(
<g/>
písmena	písmeno	k1gNnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
např.	např.	kA
vrr	vrr	k?
<g/>
,	,	kIx,
bzz	bzz	k?
<g/>
,	,	kIx,
pssst	pssst	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
samohlásek	samohláska	k1gFnPc2
není	být	k5eNaImIp3nS
v	v	k7c6
češtině	čeština	k1gFnSc6
délka	délka	k1gFnSc1
souhlásek	souhláska	k1gFnPc2
fonologicky	fonologicky	k6eAd1
relevantním	relevantní	k2eAgInSc7d1
rysem	rys	k1gInSc7
<g/>
,	,	kIx,
tj.	tj.	kA
nemá	mít	k5eNaImIp3nS
schopnost	schopnost	k1gFnSc4
rozlišit	rozlišit	k5eAaPmF
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Alofony	Alofon	k1gInPc1
</s>
<s>
Alofonem	Alofon	k1gInSc7
se	se	k3xPyFc4
rozumí	rozumět	k5eAaImIp3nS
různá	různý	k2eAgFnSc1d1
realizace	realizace	k1gFnSc1
téže	tenže	k3xDgFnSc2,k3xTgFnSc2
hlásky	hláska	k1gFnSc2
–	–	k?
fonému	foném	k1gInSc2
–	–	k?
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
změně	změna	k1gFnSc3
významu	význam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
různá	různý	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
je	být	k5eAaImIp3nS
buď	buď	k8xC
podmíněna	podmínit	k5eAaPmNgFnS
pozicí	pozice	k1gFnSc7
hlásky	hláska	k1gFnSc2
ve	v	k7c6
slově	slovo	k1gNnSc6
a	a	k8xC
jejím	její	k3xOp3gNnSc7
okolím	okolí	k1gNnSc7
(	(	kIx(
<g/>
poziční	poziční	k2eAgFnSc2d1
varianty	varianta	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgFnPc7d1
mluvčími	mluvčí	k1gFnPc7
<g/>
,	,	kIx,
nářečími	nářečí	k1gNnPc7
(	(	kIx(
<g/>
volné	volný	k2eAgInPc1d1
varianty	variant	k1gInPc1
<g/>
)	)	kIx)
apod.	apod.	kA
</s>
<s>
Spisovná	spisovný	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
češtiny	čeština	k1gFnSc2
připouští	připouštět	k5eAaImIp3nS
u	u	k7c2
souhlásek	souhláska	k1gFnPc2
varianty	varianta	k1gFnSc2
podmíněné	podmíněný	k2eAgFnSc2d1
jejich	jejich	k3xOp3gNnSc4
okolím	okolí	k1gNnSc7
<g/>
,	,	kIx,
volné	volný	k2eAgFnPc1d1
varianty	varianta	k1gFnPc1
(	(	kIx(
<g/>
např.	např.	kA
různá	různý	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
/	/	kIx~
<g/>
l	l	kA
<g/>
/	/	kIx~
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
hodnoceny	hodnotit	k5eAaImNgFnP
jako	jako	k9
nářeční	nářeční	k2eAgFnPc1d1
nebo	nebo	k8xC
jako	jako	k8xS,k8xC
vada	vada	k1gFnSc1
řeči	řeč	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
jiné	jiný	k2eAgFnSc2d1
realizace	realizace	k1gFnSc2
/	/	kIx~
<g/>
r	r	kA
<g/>
/	/	kIx~
než	než	k8xS
jako	jako	k9
alveolární	alveolární	k2eAgFnSc1d1
vibranta	vibranta	k1gFnSc1
<g/>
,	,	kIx,
tzv.	tzv.	kA
ráčkování	ráčkování	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Asimilace	asimilace	k1gFnSc1
a	a	k8xC
neutralizace	neutralizace	k1gFnSc1
znělosti	znělost	k1gFnSc2
</s>
<s>
Pravidelnou	pravidelný	k2eAgFnSc7d1
změnou	změna	k1gFnSc7
<g/>
,	,	kIx,
charakteristickou	charakteristický	k2eAgFnSc7d1
pro	pro	k7c4
českou	český	k2eAgFnSc4d1
výslovnost	výslovnost	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
změna	změna	k1gFnSc1
znělosti	znělost	k1gFnSc2
pravých	pravý	k2eAgFnPc2d1
souhlásek	souhláska	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dochází	docházet	k5eAaImIp3nS
k	k	k7c3
ní	on	k3xPp3gFnSc3
v	v	k7c6
těchto	tento	k3xDgInPc6
případech	případ	k1gInPc6
<g/>
:	:	kIx,
</s>
<s>
asimilace	asimilace	k1gFnSc1
(	(	kIx(
<g/>
spodoba	spodoba	k1gFnSc1
<g/>
)	)	kIx)
znělosti	znělost	k1gFnPc1
ve	v	k7c6
skupinách	skupina	k1gFnPc6
pravých	pravý	k2eAgFnPc2d1
souhlásek	souhláska	k1gFnPc2
–	–	k?
tyto	tento	k3xDgFnPc1
skupiny	skupina	k1gFnPc1
se	se	k3xPyFc4
celé	celá	k1gFnPc1
vyslovují	vyslovovat	k5eAaImIp3nP
buď	buď	k8xC
zněle	zněle	k6eAd1
<g/>
,	,	kIx,
nebo	nebo	k8xC
nezněle	zněle	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpravidla	zpravidla	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
tzv.	tzv.	kA
regresivní	regresivní	k2eAgFnSc6d1
asimilaci	asimilace	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
znělost	znělost	k1gFnSc1
skupiny	skupina	k1gFnSc2
řídí	řídit	k5eAaImIp3nS
podle	podle	k7c2
poslední	poslední	k2eAgFnSc2d1
souhlásky	souhláska	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
roztok	roztok	k1gInSc1
[	[	kIx(
<g/>
rostok	rostok	k1gInSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odchylně	odchylně	k6eAd1
se	se	k3xPyFc4
chovají	chovat	k5eAaImIp3nP
fonémy	foném	k1gInPc1
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
ř	ř	k?
<g/>
,	,	kIx,
v	v	k7c6
<g/>
/	/	kIx~
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
výše	výše	k1gFnSc2,k1gFnSc2wB
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
neutralizace	neutralizace	k1gFnSc1
znělosti	znělost	k1gFnSc2
na	na	k7c6
konci	konec	k1gInSc6
slov	slovo	k1gNnPc2
–	–	k?
znělé	znělý	k2eAgFnSc2d1
souhlásky	souhláska	k1gFnSc2
se	se	k3xPyFc4
na	na	k7c6
konci	konec	k1gInSc6
slov	slovo	k1gNnPc2
vyslovují	vyslovovat	k5eAaImIp3nP
jako	jako	k9
neznělé	znělý	k2eNgInPc1d1
(	(	kIx(
<g/>
např.	např.	kA
led	led	k1gInSc1
[	[	kIx(
<g/>
lɛ	lɛ	k5eAaPmF
<g/>
]	]	kIx)
-	-	kIx~
ledu	led	k1gInSc2
[	[	kIx(
<g/>
lɛ	lɛ	k6eAd1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
spojité	spojitý	k2eAgFnSc6d1
výslovnosti	výslovnost	k1gFnSc6
však	však	k9
může	moct	k5eAaImIp3nS
docházet	docházet	k5eAaImF
k	k	k7c3
asimilaci	asimilace	k1gFnSc3
znělosti	znělost	k1gFnSc2
i	i	k8xC
na	na	k7c6
hranicích	hranice	k1gFnPc6
slov	slovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Asimilaci	asimilace	k1gFnSc4
znělosti	znělost	k1gFnSc2
nepodléhají	podléhat	k5eNaImIp3nP
jedinečné	jedinečný	k2eAgFnPc1d1
souhlásky	souhláska	k1gFnPc1
(	(	kIx(
<g/>
sonory	sonora	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
neboť	neboť	k8xC
nemají	mít	k5eNaImIp3nP
neznělé	znělý	k2eNgInPc4d1
protějšky	protějšek	k1gInPc4
<g/>
,	,	kIx,
ani	ani	k8xC
tuto	tento	k3xDgFnSc4
asimilaci	asimilace	k1gFnSc4
standardně	standardně	k6eAd1
nezpůsobují	způsobovat	k5eNaImIp3nP
(	(	kIx(
<g/>
v	v	k7c6
moravských	moravský	k2eAgNnPc6d1
nářečích	nářečí	k1gNnPc6
však	však	k9
k	k	k7c3
této	tento	k3xDgFnSc3
asimilaci	asimilace	k1gFnSc3
obvykle	obvykle	k6eAd1
dochází	docházet	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Znělé	znělý	k2eAgFnPc1d1
a	a	k8xC
neznělé	znělý	k2eNgFnPc1d1
pravé	pravý	k2eAgFnPc1d1
souhlásky	souhláska	k1gFnPc1
tvoří	tvořit	k5eAaImIp3nP
páry	pár	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
jejichž	jejichž	k3xOyRp3gInSc6
rámci	rámec	k1gInSc6
se	se	k3xPyFc4
spodoba	spodoba	k1gFnSc1
znělosti	znělost	k1gFnSc2
uplatňuje	uplatňovat	k5eAaImIp3nS
<g/>
:	:	kIx,
<g/>
.	.	kIx.
</s>
<s>
Znělostní	znělostní	k2eAgInPc1d1
páry	pár	k1gInPc1
v	v	k7c6
češtině	čeština	k1gFnSc6
</s>
<s>
Neznělé	znělý	k2eNgFnPc1d1
realizace	realizace	k1gFnPc1
</s>
<s>
[	[	kIx(
<g/>
p	p	k?
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
t	t	k?
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
c	c	k0
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
k	k	k7c3
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
f	f	k?
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
s	s	k7c7
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
ʃ	ʃ	k?
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
x	x	k?
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
t	t	k?
<g/>
͡	͡	k?
<g/>
s	s	k7c7
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
t	t	k?
<g/>
͡	͡	k?
<g/>
ʃ	ʃ	k?
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
r	r	kA
<g/>
̝	̝	k?
<g/>
]	]	kIx)
</s>
<s>
Znělé	znělý	k2eAgFnPc1d1
realizace	realizace	k1gFnPc1
</s>
<s>
[	[	kIx(
<g/>
b	b	k?
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
d	d	k?
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
ɟ	ɟ	k?
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
ɡ	ɡ	k?
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
v	v	k7c6
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
z	z	k7c2
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
ʒ	ʒ	k?
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
ɦ	ɦ	k?
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
d	d	k?
<g/>
͡	͡	k?
<g/>
z	z	k7c2
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
d	d	k?
<g/>
͡	͡	k?
<g/>
ʒ	ʒ	k?
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
r	r	kA
<g/>
̝	̝	k?
<g/>
°	°	k?
<g/>
]	]	kIx)
</s>
<s>
Dvojice	dvojice	k1gFnSc1
fonémů	foném	k1gInPc2
</s>
<s>
/	/	kIx~
<g/>
p	p	k?
<g/>
/	/	kIx~
<g/>
~	~	kIx~
<g/>
/	/	kIx~
<g/>
b	b	k?
<g/>
//	//	k?
<g/>
t	t	k?
<g/>
/	/	kIx~
<g/>
~	~	kIx~
<g/>
/	/	kIx~
<g/>
d	d	k?
<g/>
//	//	k?
<g/>
ť	ť	k?
<g/>
/	/	kIx~
<g/>
~	~	kIx~
<g/>
/	/	kIx~
<g/>
ď	ď	k?
<g/>
//	//	k?
<g/>
k	k	k7c3
<g/>
/	/	kIx~
<g/>
~	~	kIx~
<g/>
/	/	kIx~
<g/>
g	g	kA
<g/>
//	//	k?
<g/>
f	f	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
~	~	kIx~
<g/>
/	/	kIx~
<g/>
v	v	k7c4
<g/>
//	//	k?
<g/>
s	s	k7c7
<g/>
/	/	kIx~
<g/>
~	~	kIx~
<g/>
/	/	kIx~
<g/>
z	z	k7c2
<g/>
//	//	k?
<g/>
š	š	k?
<g/>
/	/	kIx~
<g/>
~	~	kIx~
<g/>
/	/	kIx~
<g/>
ž	ž	k?
<g/>
//	//	k?
<g/>
x	x	k?
<g/>
/	/	kIx~
<g/>
~	~	kIx~
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
//	//	k?
<g/>
c	c	k0
<g/>
/	/	kIx~
<g/>
~	~	kIx~
<g/>
/	/	kIx~
<g/>
ʒ	ʒ	k?
<g/>
//	//	k?
<g/>
č	č	k0
<g/>
/	/	kIx~
<g/>
~	~	kIx~
<g/>
/	/	kIx~
<g/>
ǯ	ǯ	k?
<g/>
//	//	k?
<g/>
ř	ř	k?
<g/>
/	/	kIx~
*	*	kIx~
</s>
<s>
*	*	kIx~
K	k	k7c3
asimilaci	asimilace	k1gFnSc3
znělosti	znělost	k1gFnSc2
dochází	docházet	k5eAaImIp3nS
v	v	k7c6
rámci	rámec	k1gInSc6
jediného	jediný	k2eAgInSc2d1
fonému	foném	k1gInSc2
/	/	kIx~
<g/>
ř	ř	k?
<g/>
/	/	kIx~
<g/>
.	.	kIx.
</s>
<s>
Asimilace	asimilace	k1gFnSc1
místa	místo	k1gNnSc2
tvoření	tvoření	k1gNnSc2
</s>
<s>
Pro	pro	k7c4
usnadnění	usnadnění	k1gNnSc4
výslovnosti	výslovnost	k1gFnSc2
může	moct	k5eAaImIp3nS
docházet	docházet	k5eAaImF
k	k	k7c3
posunu	posun	k1gInSc3
místa	místo	k1gNnSc2
tvoření	tvoření	k1gNnSc4
ve	v	k7c6
skupinách	skupina	k1gFnPc6
souhlásek	souhláska	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spisovná	spisovný	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
toto	tento	k3xDgNnSc4
zjednodušení	zjednodušení	k1gNnSc4
připouští	připouštět	k5eAaImIp3nS
u	u	k7c2
nosovek	nosovka	k1gFnPc2
(	(	kIx(
<g/>
nazál	nazála	k1gFnPc2
<g/>
)	)	kIx)
v	v	k7c6
těchto	tento	k3xDgInPc6
případech	případ	k1gInPc6
<g/>
:	:	kIx,
</s>
<s>
Foném	foném	k1gInSc1
/	/	kIx~
<g/>
m	m	kA
<g/>
/	/	kIx~
<g/>
,	,	kIx,
běžně	běžně	k6eAd1
realizovaný	realizovaný	k2eAgInSc1d1
jako	jako	k8xS,k8xC
obouretná	obouretný	k2eAgFnSc1d1
(	(	kIx(
<g/>
bilabiální	bilabiální	k2eAgFnSc1d1
<g/>
)	)	kIx)
hláska	hláska	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
před	před	k7c7
retozubnými	retozubný	k2eAgFnPc7d1
(	(	kIx(
<g/>
labiodentálními	labiodentální	k2eAgFnPc7d1
<g/>
)	)	kIx)
souhláskami	souhláska	k1gFnPc7
/	/	kIx~
<g/>
f	f	k?
<g/>
,	,	kIx,
v	v	k7c6
<g/>
/	/	kIx~
může	moct	k5eAaImIp3nS
vyslovovat	vyslovovat	k5eAaImF
jako	jako	k9
retozubné	retozubný	k2eAgInPc4d1
[	[	kIx(
<g/>
ɱ	ɱ	k?
<g/>
]	]	kIx)
(	(	kIx(
<g/>
např.	např.	kA
ve	v	k7c6
slově	slovo	k1gNnSc6
tramvaj	tramvaj	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
češtině	čeština	k1gFnSc6
se	se	k3xPyFc4
však	však	k9
toto	tento	k3xDgNnSc1
spojení	spojení	k1gNnSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
zřídka	zřídka	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Foném	foném	k1gInSc1
/	/	kIx~
<g/>
n	n	k0
<g/>
/	/	kIx~
<g/>
,	,	kIx,
běžně	běžně	k6eAd1
realizovaný	realizovaný	k2eAgInSc1d1
jako	jako	k8xC,k8xS
předodásňová	předodásňový	k2eAgFnSc1d1
(	(	kIx(
<g/>
alveolární	alveolární	k2eAgFnSc1d1
<g/>
)	)	kIx)
hláska	hláska	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
před	před	k7c7
zadopatrovými	zadopatrův	k2eAgFnPc7d1
(	(	kIx(
<g/>
velárními	velární	k2eAgFnPc7d1
<g/>
)	)	kIx)
souhláskami	souhláska	k1gFnPc7
/	/	kIx~
<g/>
k	k	k7c3
<g/>
,	,	kIx,
g	g	kA
<g/>
,	,	kIx,
x	x	k?
<g/>
/	/	kIx~
může	moct	k5eAaImIp3nS
vyslovovat	vyslovovat	k5eAaImF
jako	jako	k9
zadopatrové	zadopatrový	k2eAgInPc4d1
[	[	kIx(
<g/>
ŋ	ŋ	k?
<g/>
]	]	kIx)
(	(	kIx(
<g/>
např.	např.	kA
ve	v	k7c6
slově	slovo	k1gNnSc6
banka	banka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Splývání	splývání	k1gNnSc1
souhlásek	souhláska	k1gFnPc2
</s>
<s>
Pro	pro	k7c4
tvoření	tvoření	k1gNnSc4
slov	slovo	k1gNnPc2
se	se	k3xPyFc4
na	na	k7c6
hranici	hranice	k1gFnSc6
morfémů	morfém	k1gInPc2
mohou	moct	k5eAaImIp3nP
setkat	setkat	k5eAaPmF
dva	dva	k4xCgInPc1
stejné	stejný	k2eAgInPc1d1
fonémy	foném	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mnoha	mnoho	k4c6
případech	případ	k1gInPc6
<g/>
,	,	kIx,
zejména	zejména	k9
u	u	k7c2
přípon	přípona	k1gFnPc2
<g/>
,	,	kIx,
výslovnost	výslovnost	k1gFnSc1
obou	dva	k4xCgInPc2
fonémů	foném	k1gInPc2
splývá	splývat	k5eAaImIp3nS
<g/>
,	,	kIx,
např.	např.	kA
cenný	cenný	k2eAgInSc1d1
[	[	kIx(
<g/>
t	t	k?
<g/>
͡	͡	k?
<g/>
sɛ	sɛ	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
měkký	měkký	k2eAgInSc1d1
[	[	kIx(
<g/>
mɲ	mɲ	k?
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
vyznačení	vyznačení	k1gNnSc4
předělu	předěl	k1gInSc2
<g/>
,	,	kIx,
zejména	zejména	k9
u	u	k7c2
předpon	předpona	k1gFnPc2
a	a	k8xC
ve	v	k7c6
složeninách	složenina	k1gFnPc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
obvyklá	obvyklý	k2eAgFnSc1d1
zdvojená	zdvojený	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
(	(	kIx(
<g/>
geminace	geminace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tj.	tj.	kA
oba	dva	k4xCgInPc1
fonémy	foném	k1gInPc1
se	se	k3xPyFc4
vysloví	vyslovit	k5eAaPmIp3nP
odděleně	odděleně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdvojená	zdvojený	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
je	být	k5eAaImIp3nS
nutná	nutný	k2eAgFnSc1d1
zejména	zejména	k9
pro	pro	k7c4
rozlišení	rozlišení	k1gNnSc4
různých	různý	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
<g/>
:	:	kIx,
nejjasnější	jasný	k2eAgFnSc1d3
[	[	kIx(
<g/>
nɛ	nɛ	k?
<g/>
]	]	kIx)
x	x	k?
nejasnější	jasný	k2eNgFnPc4d2
[	[	kIx(
<g/>
nɛ	nɛ	k?
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdvojená	zdvojený	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
v	v	k7c6
případech	případ	k1gInPc6
jako	jako	k8xS,k8xC
[	[	kIx(
<g/>
t	t	k?
<g/>
͡	͡	k?
<g/>
sɛ	sɛ	k?
<g/>
]	]	kIx)
nebo	nebo	k8xC
[	[	kIx(
<g/>
mɲ	mɲ	k?
<g/>
]	]	kIx)
však	však	k9
obvykle	obvykle	k6eAd1
působí	působit	k5eAaImIp3nS
strojeně	strojeně	k6eAd1
a	a	k8xC
je	být	k5eAaImIp3nS
vnímána	vnímat	k5eAaImNgFnS
jako	jako	k9
hyperkorektní	hyperkorektní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Rovněž	rovněž	k9
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
při	při	k7c6
slovotvorbě	slovotvorba	k1gFnSc6
setkat	setkat	k5eAaPmF
ploziva	ploziv	k1gMnSc4
(	(	kIx(
<g/>
/	/	kIx~
<g/>
d	d	k?
<g/>
,	,	kIx,
t	t	k?
<g/>
,	,	kIx,
ɟ	ɟ	k?
<g/>
,	,	kIx,
c	c	k0
<g/>
/	/	kIx~
<g/>
)	)	kIx)
s	s	k7c7
frikativou	frikativa	k1gFnSc7
(	(	kIx(
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
,	,	kIx,
z	z	k0
<g/>
,	,	kIx,
ʃ	ʃ	k?
<g/>
,	,	kIx,
ʒ	ʒ	k?
<g/>
/	/	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
běžném	běžný	k2eAgInSc6d1
hovoru	hovor	k1gInSc6
obvykle	obvykle	k6eAd1
akusticky	akusticky	k6eAd1
splývají	splývat	k5eAaImIp3nP
a	a	k8xC
vytvářejí	vytvářet	k5eAaImIp3nP
afrikáty	afrikáta	k1gFnPc4
(	(	kIx(
<g/>
[	[	kIx(
<g/>
t	t	k?
<g/>
͡	͡	k?
<g/>
s	s	k7c7
<g/>
,	,	kIx,
d	d	k?
<g/>
͡	͡	k?
<g/>
z	z	k7c2
<g/>
,	,	kIx,
t	t	k?
<g/>
͡	͡	k?
<g/>
ʃ	ʃ	k?
<g/>
,	,	kIx,
d	d	k?
<g/>
͡	͡	k?
<g/>
ʒ	ʒ	k?
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
:	:	kIx,
dětský	dětský	k2eAgInSc1d1
[	[	kIx(
<g/>
ɟ	ɟ	k1gInSc1
<g/>
͡	͡	k?
<g/>
skiː	skiː	k?
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
pečlivé	pečlivý	k2eAgFnSc6d1
výslovnosti	výslovnost	k1gFnSc6
se	se	k3xPyFc4
však	však	k9
oba	dva	k4xCgInPc1
fonémy	foném	k1gInPc1
vysloví	vyslovit	k5eAaPmIp3nP
odděleně	odděleně	k6eAd1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
ɟ	ɟ	k1gInSc1
<g/>
.	.	kIx.
<g/>
skiː	skiː	k?
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Asimilace	asimilace	k1gFnSc1
souhlásek	souhláska	k1gFnPc2
na	na	k7c6
hranici	hranice	k1gFnSc6
slov	slovo	k1gNnPc2
(	(	kIx(
<g/>
např.	např.	kA
pojď	jít	k5eAaImRp2nS
sem	sem	k6eAd1
[	[	kIx(
<g/>
pot	pot	k1gInSc1
<g/>
͡	͡	k?
<g/>
sɛ	sɛ	k1gInSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ve	v	k7c6
standardní	standardní	k2eAgFnSc6d1
výslovnosti	výslovnost	k1gFnSc6
nevhodná	vhodný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlásky	hlásek	k1gInPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
vyslovovat	vyslovovat	k5eAaImF
vždy	vždy	k6eAd1
zřetelně	zřetelně	k6eAd1
a	a	k8xC
odděleně	odděleně	k6eAd1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
pojc	pojc	k6eAd1
sɛ	sɛ	k6eAd1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Souhlásky	souhláska	k1gFnPc1
v	v	k7c6
písmu	písmo	k1gNnSc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Český	český	k2eAgInSc1d1
pravopis	pravopis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Čeština	čeština	k1gFnSc1
používá	používat	k5eAaImIp3nS
hláskové	hláskový	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
<g/>
,	,	kIx,
latinku	latinka	k1gFnSc4
<g/>
,	,	kIx,
rozšířené	rozšířený	k2eAgInPc4d1
o	o	k7c4
znaky	znak	k1gInPc4
s	s	k7c7
diakritikou	diakritika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
tzv.	tzv.	kA
měkkých	měkký	k2eAgFnPc2d1
souhlásek	souhláska	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
latině	latina	k1gFnSc6
cizí	cizí	k2eAgFnSc2d1
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
háček	háček	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
umísťuje	umísťovat	k5eAaImIp3nS
nad	nad	k7c4
základní	základní	k2eAgInSc4d1
znak	znak	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
písmen	písmeno	k1gNnPc2
<	<	kIx(
<g/>
ť	ť	k?
<g/>
,	,	kIx,
ď	ď	k?
<g/>
>	>	kIx)
má	mít	k5eAaImIp3nS
háček	háček	k1gInSc4
v	v	k7c6
některých	některý	k3yIgInPc6
fontech	font	k1gInPc6
odlišnou	odlišný	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znaky	znak	k1gInPc4
s	s	k7c7
diakritikou	diakritika	k1gFnSc7
jsou	být	k5eAaImIp3nP
tyto	tento	k3xDgFnPc4
<g/>
:	:	kIx,
</s>
<s>
č	č	k0
<g/>
,	,	kIx,
ď	ď	k?
<g/>
,	,	kIx,
ň	ň	k?
<g/>
,	,	kIx,
ř	ř	k?
<g/>
,	,	kIx,
š	š	k?
<g/>
,	,	kIx,
ť	ť	k?
<g/>
,	,	kIx,
ž	ž	k?
</s>
<s>
Č	Č	kA
<g/>
,	,	kIx,
Ď	Ď	kA
<g/>
,	,	kIx,
Ň	Ň	kA
<g/>
,	,	kIx,
Ř	Ř	kA
<g/>
,	,	kIx,
Š	Š	kA
<g/>
,	,	kIx,
Ť	Ť	kA
<g/>
,	,	kIx,
Ž	Ž	kA
</s>
<s>
Diakritický	diakritický	k2eAgInSc1d1
pravopis	pravopis	k1gInSc1
umožňuje	umožňovat	k5eAaImIp3nS
zachovávat	zachovávat	k5eAaImF
zásadu	zásada	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
jednotlivému	jednotlivý	k2eAgInSc3d1
fonému	foném	k1gInSc3
(	(	kIx(
<g/>
hlásce	hláska	k1gFnSc6
<g/>
)	)	kIx)
v	v	k7c6
češtině	čeština	k1gFnSc6
zpravidla	zpravidla	k6eAd1
odpovídá	odpovídat	k5eAaImIp3nS
jediný	jediný	k2eAgInSc1d1
grafém	grafém	k1gInSc1
(	(	kIx(
<g/>
písmeno	písmeno	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diakritický	diakritický	k2eAgInSc1d1
pravopis	pravopis	k1gInSc1
byl	být	k5eAaImAgInS
pro	pro	k7c4
češtinu	čeština	k1gFnSc4
navržen	navrhnout	k5eAaPmNgInS
na	na	k7c6
začátku	začátek	k1gInSc6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
v	v	k7c6
latinsky	latinsky	k6eAd1
psaném	psaný	k2eAgInSc6d1
díle	díl	k1gInSc6
De	De	k?
orthographia	orthographius	k1gMnSc2
Bohemica	Bohemicus	k1gMnSc2
(	(	kIx(
<g/>
1406	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
autorství	autorství	k1gNnSc1
se	se	k3xPyFc4
připisuje	připisovat	k5eAaImIp3nS
Janu	Jan	k1gMnSc3
Husovi	Hus	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
se	se	k3xPyFc4
však	však	k9
místo	místo	k7c2
háčku	háček	k1gInSc2
psala	psát	k5eAaImAgFnS
tečka	tečka	k1gFnSc1
(	(	kIx(
<g/>
punctus	punctus	k1gMnSc1
rotundus	rotundus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
diakritiky	diakritika	k1gFnSc2
používá	používat	k5eAaImIp3nS
čeština	čeština	k1gFnSc1
také	také	k6eAd1
spřežky	spřežka	k1gFnSc2
<ch>
(	(	kIx(
<g/>
považována	považován	k2eAgFnSc1d1
za	za	k7c4
samostatné	samostatný	k2eAgNnSc4d1
písmeno	písmeno	k1gNnSc4
<g/>
,	,	kIx,
stojící	stojící	k2eAgFnSc4d1
v	v	k7c6
abecedě	abeceda	k1gFnSc6
za	za	k7c4
<h>
)	)	kIx)
<g/>
,	,	kIx,
<dz>
a	a	k8xC
<dž>
(	(	kIx(
<g/>
obě	dva	k4xCgFnPc1
téměř	téměř	k6eAd1
výhradně	výhradně	k6eAd1
ve	v	k7c6
slovech	slovo	k1gNnPc6
cizího	cizí	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1
princip	princip	k1gInSc1
českého	český	k2eAgInSc2d1
pravopisu	pravopis	k1gInSc2
je	být	k5eAaImIp3nS
fonologický	fonologický	k2eAgInSc1d1
(	(	kIx(
<g/>
nikoliv	nikoliv	k9
fonetický	fonetický	k2eAgMnSc1d1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
v	v	k7c6
některých	některý	k3yIgFnPc6
učebnicích	učebnice	k1gFnPc6
pro	pro	k7c4
zjednodušení	zjednodušení	k1gNnSc4
uvádí	uvádět	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grafický	grafický	k2eAgInSc1d1
zápis	zápis	k1gInSc1
plně	plně	k6eAd1
neodpovídá	odpovídat	k5eNaImIp3nS
skutečné	skutečný	k2eAgFnPc4d1
výslovnosti	výslovnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivá	jednotlivý	k2eAgNnPc4d1
písmena	písmeno	k1gNnPc4
(	(	kIx(
<g/>
grafémy	grafém	k1gInPc1
<g/>
)	)	kIx)
a	a	k8xC
spřežky	spřežka	k1gFnSc2
neodpovídají	odpovídat	k5eNaImIp3nP
jednotlivým	jednotlivý	k2eAgInPc3d1
zvukům	zvuk	k1gInPc3
<g/>
,	,	kIx,
ale	ale	k8xC
fonémům	foném	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
vystihnout	vystihnout	k5eAaPmF
určité	určitý	k2eAgFnPc4d1
morfologické	morfologický	k2eAgFnPc4d1
a	a	k8xC
etymologické	etymologický	k2eAgFnPc4d1
zákonitosti	zákonitost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typicky	typicky	k6eAd1
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
projevuje	projevovat	k5eAaImIp3nS
například	například	k6eAd1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
pravopis	pravopis	k1gInSc1
nepostihuje	postihovat	k5eNaImIp3nS
spodobu	spodoba	k1gFnSc4
znělosti	znělost	k1gFnSc2
ve	v	k7c6
skupinách	skupina	k1gFnPc6
souhlásek	souhláska	k1gFnPc2
a	a	k8xC
ztrátu	ztráta	k1gFnSc4
znělosti	znělost	k1gFnSc2
na	na	k7c6
konci	konec	k1gInSc6
slov	slovo	k1gNnPc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
výše	výše	k1gFnSc2,k1gFnSc2wB
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
historických	historický	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
se	se	k3xPyFc4
souhlásky	souhláska	k1gFnSc2
/	/	kIx~
<g/>
ď	ď	k?
<g/>
,	,	kIx,
ť	ť	k?
<g/>
,	,	kIx,
ň	ň	k?
<g/>
/	/	kIx~
před	před	k7c7
/	/	kIx~
<g/>
e	e	k0
<g/>
,	,	kIx,
i	i	k9
<g/>
,	,	kIx,
í	í	k0
<g/>
/	/	kIx~
zapisují	zapisovat	k5eAaImIp3nP
jako	jako	k8xC,k8xS
<	<	kIx(
<g/>
d	d	k?
<g/>
,	,	kIx,
t	t	k?
<g/>
,	,	kIx,
n	n	k0
<g/>
>	>	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
že	že	k8xS
tyto	tento	k3xDgFnPc4
skupiny	skupina	k1gFnPc4
se	se	k3xPyFc4
ve	v	k7c6
slovech	slovo	k1gNnPc6
domácího	domácí	k2eAgInSc2d1
původu	původ	k1gInSc2
píší	psát	k5eAaImIp3nP
<	<	kIx(
<g/>
dě	dě	k?
<g/>
,	,	kIx,
tě	ty	k3xPp2nSc4
<g/>
,	,	kIx,
ně	on	k3xPp3gInPc4
<g/>
;	;	kIx,
di	di	k?
<g/>
,	,	kIx,
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
ni	on	k3xPp3gFnSc4
<g/>
;	;	kIx,
dí	dít	k5eAaBmIp3nS
<g/>
,	,	kIx,
tí	tí	k?
<g/>
,	,	kIx,
ní	on	k3xPp3gFnSc3
<g/>
>	>	kIx)
<g/>
.	.	kIx.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1
přehled	přehled	k1gInSc1
</s>
<s>
Následující	následující	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
uvádí	uvádět	k5eAaImIp3nS
výčet	výčet	k1gInSc4
jednotlivých	jednotlivý	k2eAgFnPc2d1
českých	český	k2eAgFnPc2d1
souhlásek	souhláska	k1gFnPc2
<g/>
,	,	kIx,
zapsaných	zapsaný	k2eAgInPc2d1
ve	v	k7c6
znacích	znak	k1gInPc6
IPA	IPA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tučně	tučně	k6eAd1
jsou	být	k5eAaImIp3nP
označeny	označen	k2eAgFnPc4d1
základní	základní	k2eAgFnPc4d1
realizace	realizace	k1gFnPc4
(	(	kIx(
<g/>
alofony	alofon	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
netučné	tučný	k2eNgFnPc1d1
jsou	být	k5eAaImIp3nP
základní	základní	k2eAgFnPc1d1
realizace	realizace	k1gFnPc1
periferních	periferní	k2eAgInPc2d1
fonémů	foném	k1gInPc2
(	(	kIx(
<g/>
tj.	tj.	kA
takových	takový	k3xDgInPc6
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
jen	jen	k9
v	v	k7c6
cizích	cizí	k2eAgNnPc6d1
slovech	slovo	k1gNnPc6
a	a	k8xC
citoslovcích	citoslovce	k1gNnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
závorkách	závorka	k1gFnPc6
jsou	být	k5eAaImIp3nP
poziční	poziční	k2eAgFnPc1d1
varianty	varianta	k1gFnPc1
jiných	jiný	k2eAgFnPc2d1
hlásek	hláska	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
symboly	symbol	k1gInPc4
ve	v	k7c6
dvojicích	dvojice	k1gFnPc6
<g/>
,	,	kIx,
označuje	označovat	k5eAaImIp3nS
levý	levý	k2eAgInSc1d1
symbol	symbol	k1gInSc1
neznělou	znělý	k2eNgFnSc4d1
a	a	k8xC
pravý	pravý	k2eAgInSc4d1
znělou	znělý	k2eAgFnSc4d1
souhlásku	souhláska	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Zobrazit	zobrazit	k5eAaPmF
tabulku	tabulka	k1gFnSc4
jako	jako	k8xS,k8xC
obrázek	obrázek	k1gInSc4
</s>
<s>
Místo	místo	k7c2
tvoření	tvoření	k1gNnSc2
→	→	k?
</s>
<s>
Bila‐	Bila‐	k1gInPc1
</s>
<s>
Labio‐	Labio‐	k1gFnPc1
</s>
<s>
Alveoláry	alveolára	k1gFnPc1
</s>
<s>
Post‐	Post‐	k1gInPc1
</s>
<s>
Pala‐	Pala‐	k1gFnPc1
</s>
<s>
Veláry	velára	k1gFnPc1
</s>
<s>
Glo‐	Glo‐	k1gFnPc1
</s>
<s>
Způsob	způsob	k1gInSc1
tvoření	tvoření	k1gNnSc2
</s>
<s>
Sluchový	sluchový	k2eAgInSc1d1
dojem	dojem	k1gInSc1
</s>
<s>
Akustická	akustický	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1
</s>
<s>
Nazální	nazální	k2eAgFnPc1d1
okluzivy	okluziva	k1gFnPc1
</s>
<s>
Nazální	nazální	k2eAgFnPc1d1
explozivy	exploziva	k1gFnPc1
</s>
<s>
Nekontinuální	kontinuální	k2eNgFnSc1d1
</s>
<s>
Matné	matný	k2eAgNnSc1d1
</s>
<s>
m	m	kA
</s>
<s>
(	(	kIx(
<g/>
ɱ	ɱ	k?
<g/>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
ɲ	ɲ	k?
</s>
<s>
(	(	kIx(
<g/>
ŋ	ŋ	k?
<g/>
)	)	kIx)
</s>
<s>
Orální	orální	k2eAgFnPc1d1
okluzivy	okluziva	k1gFnPc1
</s>
<s>
Orální	orální	k2eAgFnPc1d1
explozivy	exploziva	k1gFnPc1
</s>
<s>
p	p	k?
b	b	k?
</s>
<s>
t	t	k?
d	d	k?
</s>
<s>
c	c	k0
ɟ	ɟ	k?
</s>
<s>
k	k	k7c3
g	g	kA
</s>
<s>
(	(	kIx(
<g/>
ʔ	ʔ	k?
<g/>
)	)	kIx)
</s>
<s>
Semiokluzivy	semiokluziva	k1gFnPc1
</s>
<s>
Afrikáty	afrikáta	k1gFnPc1
</s>
<s>
Drsné	drsný	k2eAgNnSc1d1
</s>
<s>
t	t	k?
<g/>
͡	͡	k?
<g/>
s	s	k7c7
d	d	k?
<g/>
͡	͡	k?
<g/>
z	z	k7c2
</s>
<s>
t	t	k?
<g/>
͡	͡	k?
<g/>
ʃ	ʃ	k?
d	d	k?
<g/>
͡	͡	k?
<g/>
ʒ	ʒ	k?
</s>
<s>
Konstriktivy	konstriktiva	k1gFnPc1
</s>
<s>
Frikativy	frikativa	k1gFnPc1
</s>
<s>
Kontinuální	kontinuální	k2eAgFnSc1d1
</s>
<s>
f	f	k?
v	v	k7c6
</s>
<s>
s	s	k7c7
z	z	k7c2
</s>
<s>
ʃ	ʃ	k?
ʒ	ʒ	k?
</s>
<s>
x	x	k?
(	(	kIx(
<g/>
ɣ	ɣ	k?
<g/>
)	)	kIx)
</s>
<s>
ɦ	ɦ	k?
</s>
<s>
(	(	kIx(
<g/>
r	r	kA
<g/>
̝	̝	k?
<g/>
°	°	k?
<g/>
)	)	kIx)
r	r	kA
<g/>
̝	̝	k?
</s>
<s>
Vibranty	vibranta	k1gFnPc1
</s>
<s>
Matné	matný	k2eAgNnSc1d1
</s>
<s>
r	r	kA
</s>
<s>
Aproximanty	Aproximanta	k1gFnPc1
</s>
<s>
(	(	kIx(
<g/>
ʊ	ʊ	k?
<g/>
̯	̯	k?
<g/>
)	)	kIx)
</s>
<s>
j	j	k?
</s>
<s>
Laterální	laterální	k2eAgFnSc1d1
</s>
<s>
l	l	kA
</s>
<s>
Hlasnost	hlasnost	k1gFnSc1
(	(	kIx(
<g/>
fonace	fonace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Znělost	znělost	k1gFnSc1
(	(	kIx(
<g/>
sonorita	sonorita	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
+	+	kIx~
<g/>
–	–	k?
+	+	kIx~
<g/>
–	–	k?
+	+	kIx~
<g/>
–	–	k?
+	+	kIx~
<g/>
–	–	k?
+	+	kIx~
<g/>
–	–	k?
+	+	kIx~
<g/>
–	–	k?
+	+	kIx~
</s>
<s>
Artikulační	artikulační	k2eAgInSc1d1
orgán	orgán	k1gInSc1
</s>
<s>
labiální	labiální	k2eAgInPc4d1
(	(	kIx(
<g/>
rty	ret	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
lingvální	lingvální	k2eAgInSc1d1
(	(	kIx(
<g/>
jazyk	jazyk	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
glotální	glotální	k2eAgFnPc4d1
<g/>
(	(	kIx(
<g/>
hlasivky	hlasivka	k1gFnPc4
<g/>
)	)	kIx)
</s>
<s>
Akustická	akustický	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
</s>
<s>
gravisové	gravisový	k2eAgNnSc1d1
</s>
<s>
akutové	akutový	k2eAgNnSc1d1
</s>
<s>
gravisové	gravisový	k2eAgNnSc1d1
</s>
<s>
nekompaktní	kompaktní	k2eNgFnSc1d1
</s>
<s>
kompaktní	kompaktní	k2eAgInSc4d1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ČERMÁK	Čermák	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jazyk	jazyk	k1gInSc1
a	a	k8xC
jazykověda	jazykověda	k1gFnSc1
:	:	kIx,
přehled	přehled	k1gInSc1
a	a	k8xC
slovníky	slovník	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
246	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
154	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DUBĚDA	DUBĚDA	kA
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jazyky	jazyk	k1gInPc7
a	a	k8xC
jejich	jejich	k3xOp3gInPc4
zvuky	zvuk	k1gInPc4
<g/>
:	:	kIx,
Univerzálie	univerzálie	k1gFnPc4
a	a	k8xC
typologie	typologie	k1gFnPc4
ve	v	k7c6
fonetice	fonetika	k1gFnSc6
a	a	k8xC
fonologii	fonologie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
246	#num#	k4
<g/>
-	-	kIx~
<g/>
1073	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KARLÍK	Karlík	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
NEKULA	Nekula	k1gMnSc1
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
<g/>
;	;	kIx,
PLESKALOVÁ	PLESKALOVÁ	kA
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedický	encyklopedický	k2eAgInSc4d1
slovník	slovník	k1gInSc4
češtiny	čeština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
484	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
KARLÍK	Karlík	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
NEKULA	Nekula	k1gMnSc1
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
<g/>
;	;	kIx,
RUSÍNOVÁ	Rusínová	k1gFnSc1
<g/>
,	,	kIx,
Zdenka	Zdenka	k1gFnSc1
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příruční	příruční	k2eAgFnSc1d1
mluvnice	mluvnice	k1gFnSc1
češtiny	čeština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
134	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KRČMOVÁ	krčmový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úvod	úvod	k1gInSc1
do	do	k7c2
fonetiky	fonetika	k1gFnSc2
a	a	k8xC
fonologie	fonologie	k1gFnSc2
pro	pro	k7c4
bohemisty	bohemista	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrava	Ostrava	k1gFnSc1
<g/>
:	:	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
OU	ou	k0
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7368	#num#	k4
<g/>
-	-	kIx~
<g/>
213	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PALKOVÁ	PALKOVÁ	kA
<g/>
,	,	kIx,
Zdena	Zdena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fonetika	fonetika	k1gFnSc1
a	a	k8xC
fonologie	fonologie	k1gFnSc1
češtiny	čeština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7066	#num#	k4
<g/>
-	-	kIx~
<g/>
843	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ŠIŠKA	Šiška	k1gMnSc1
<g/>
,	,	kIx,
Zbyněk	Zbyněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fonetika	fonetika	k1gFnSc1
a	a	k8xC
fonologie	fonologie	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Univerzita	univerzita	k1gFnSc1
Palackého	Palacký	k1gMnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
244	#num#	k4
<g/>
-	-	kIx~
<g/>
1044	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Ortoepie	ortoepie	k1gFnSc1
češtiny	čeština	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Systém	systém	k1gInSc1
českých	český	k2eAgFnPc2d1
hlásek	hláska	k1gFnPc2
(	(	kIx(
<g/>
souhlásky	souhláska	k1gFnPc1
a	a	k8xC
samohlásky	samohláska	k1gFnPc1
<g/>
)	)	kIx)
–	–	k?
přednáška	přednáška	k1gFnSc1
pro	pro	k7c4
studenty	student	k1gMnPc4
PdF	PdF	k1gMnPc2
UP	UP	kA
(	(	kIx(
<g/>
PPT	PPT	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Čeština	čeština	k1gFnSc1
Bohemistika	bohemistika	k1gFnSc1
</s>
<s>
Kancelář	kancelář	k1gFnSc1
Slovníku	slovník	k1gInSc2
jazyka	jazyk	k1gInSc2
českého	český	k2eAgInSc2d1
České	český	k2eAgMnPc4d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
</s>
<s>
Ústav	ústav	k1gInSc1
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
český	český	k2eAgInSc4d1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Ústav	ústav	k1gInSc1
Českého	český	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
korpusu	korpus	k1gInSc2
Naše	náš	k3xOp1gFnSc1
řeč	řeč	k1gFnSc1
</s>
<s>
Český	český	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
korpus	korpus	k1gInSc1
</s>
<s>
Chrám	chrám	k1gInSc1
i	i	k8xC
tvrz	tvrz	k1gFnSc1
</s>
<s>
Čeština	čeština	k1gFnSc1
poklepem	poklep	k1gInSc7
a	a	k8xC
poslechem	poslech	k1gInSc7
Variety	varieta	k1gFnSc2
češtiny	čeština	k1gFnSc2
</s>
<s>
historický	historický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
češtiny	čeština	k1gFnSc2
</s>
<s>
spisovná	spisovný	k2eAgFnSc1d1
čeština	čeština	k1gFnSc1
</s>
<s>
hovorová	hovorový	k2eAgFnSc1d1
čeština	čeština	k1gFnSc1
</s>
<s>
obecná	obecný	k2eAgFnSc1d1
čeština	čeština	k1gFnSc1
</s>
<s>
nářečí	nářečí	k1gNnSc1
češtiny	čeština	k1gFnSc2
</s>
<s>
česká	český	k2eAgFnSc1d1
</s>
<s>
středomoravská	středomoravský	k2eAgFnSc1d1
</s>
<s>
východomoravská	východomoravský	k2eAgFnSc1d1
</s>
<s>
lašská	lašský	k2eAgFnSc1d1
</s>
<s>
moravština	moravština	k1gFnSc1
</s>
<s>
jazyk	jazyk	k1gInSc1
československý	československý	k2eAgInSc1d1
</s>
<s>
anglicismy	anglicismus	k1gInPc1
</s>
<s>
germanismy	germanismus	k1gInPc1
</s>
<s>
ruská	ruský	k2eAgFnSc1d1
čeština	čeština	k1gFnSc1
Nástin	nástin	k1gInSc1
české	český	k2eAgFnSc2d1
dialektologie	dialektologie	k1gFnSc2
</s>
<s>
Český	český	k2eAgInSc1d1
jazykový	jazykový	k2eAgInSc1d1
atlas	atlas	k1gInSc1
Lexikografie	lexikografie	k1gFnSc2
</s>
<s>
Příruční	příruční	k2eAgInSc1d1
slovník	slovník	k1gInSc1
jazyka	jazyk	k1gInSc2
českého	český	k2eAgInSc2d1
</s>
<s>
Slovník	slovník	k1gInSc1
spisovného	spisovný	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
českého	český	k2eAgInSc2d1
</s>
<s>
Slovník	slovník	k1gInSc1
spisovné	spisovný	k2eAgFnSc2d1
češtiny	čeština	k1gFnSc2
</s>
<s>
Akademický	akademický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
cizích	cizí	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
</s>
<s>
Slovník	slovník	k1gInSc1
nespisovné	spisovný	k2eNgFnSc2d1
češtiny	čeština	k1gFnSc2
</s>
<s>
Etymologický	etymologický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
jazyka	jazyk	k1gInSc2
českého	český	k2eAgInSc2d1
</s>
<s>
Český	český	k2eAgInSc1d1
etymologický	etymologický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
</s>
<s>
Nová	nový	k2eAgNnPc1d1
slova	slovo	k1gNnPc1
v	v	k7c6
češtině	čeština	k1gFnSc6
–	–	k?
slovník	slovník	k1gInSc4
neologismů	neologismus	k1gInPc2
</s>
<s>
Čeština	čeština	k1gFnSc1
2.0	2.0	k4
česká	český	k2eAgFnSc1d1
exonyma	exonyma	k1gFnSc1
</s>
<s>
spor	spor	k1gInSc1
o	o	k7c4
užití	užití	k1gNnSc4
slova	slovo	k1gNnSc2
Česko	Česko	k1gNnSc1
</s>
<s>
délka	délka	k1gFnSc1
slov	slovo	k1gNnPc2
Gramatika	gramatik	k1gMnSc2
</s>
<s>
česká	český	k2eAgFnSc1d1
gramatika	gramatika	k1gFnSc1
</s>
<s>
podstatná	podstatný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
</s>
<s>
přídavná	přídavný	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
</s>
<s>
zájmena	zájmeno	k1gNnPc1
</s>
<s>
číslovky	číslovka	k1gFnPc1
</s>
<s>
slovesa	sloveso	k1gNnSc2
</s>
<s>
předložky	předložka	k1gFnPc1
</s>
<s>
skloňování	skloňování	k1gNnSc1
</s>
<s>
slovosled	slovosled	k1gInSc1
</s>
<s>
předponyGramatika	předponyGramatika	k1gFnSc1
česká	český	k2eAgFnSc1d1
v	v	k7c6
dvojí	dvojí	k4xRgFnSc6
stránce	stránka	k1gFnSc6
</s>
<s>
Barokní	barokní	k2eAgFnSc1d1
mluvnice	mluvnice	k1gFnSc1
češtiny	čeština	k1gFnSc2
</s>
<s>
Čechořečnost	Čechořečnost	k1gFnSc1
</s>
<s>
Gebauerova	Gebauerův	k2eAgFnSc1d1
mluvnice	mluvnice	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
mluvnice	mluvnice	k1gFnSc1
</s>
<s>
Mluvnice	mluvnice	k1gFnSc1
češtiny	čeština	k1gFnSc2
</s>
<s>
Příruční	příruční	k2eAgFnSc1d1
mluvnice	mluvnice	k1gFnSc1
češtiny	čeština	k1gFnSc2
</s>
<s>
Čeština	čeština	k1gFnSc1
–	–	k?
řeč	řeč	k1gFnSc1
a	a	k8xC
jazyk	jazyk	k1gInSc1
</s>
<s>
Akademická	akademický	k2eAgFnSc1d1
gramatika	gramatika	k1gFnSc1
spisovné	spisovný	k2eAgFnSc2d1
češtiny	čeština	k1gFnSc2
Fonologie	fonologie	k1gFnSc2
</s>
<s>
fonologie	fonologie	k1gFnSc1
češtiny	čeština	k1gFnSc2
</s>
<s>
samohlásky	samohláska	k1gFnPc1
</s>
<s>
souhlásky	souhláska	k1gFnPc1
</s>
<s>
česká	český	k2eAgFnSc1d1
fonetická	fonetický	k2eAgFnSc1d1
transkripce	transkripce	k1gFnSc1
Pravopis	pravopis	k1gInSc1
</s>
<s>
český	český	k2eAgInSc1d1
pravopis	pravopis	k1gInSc1
</s>
<s>
česká	český	k2eAgFnSc1d1
abeceda	abeceda	k1gFnSc1
</s>
<s>
vyjmenovaná	vyjmenovaný	k2eAgNnPc1d1
slova	slovo	k1gNnPc1
</s>
<s>
spor	spor	k1gInSc1
jotistů	jotista	k1gMnPc2
s	s	k7c7
ypsilonisty	ypsilonista	k1gMnPc7
</s>
<s>
Pomlčková	pomlčkový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Orthographia	Orthographia	k1gFnSc1
Bohemica	Bohemic	k1gInSc2
</s>
<s>
Pravidla	pravidlo	k1gNnPc1
českého	český	k2eAgInSc2d1
pravopisu	pravopis	k1gInSc2
Příručky	příručka	k1gFnSc2
</s>
<s>
Internetová	internetový	k2eAgFnSc1d1
jazyková	jazykový	k2eAgFnSc1d1
příručka	příručka	k1gFnSc1
</s>
<s>
Akademická	akademický	k2eAgFnSc1d1
příručka	příručka	k1gFnSc1
českého	český	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
Internetové	internetový	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
</s>
<s>
Vokabulář	vokabulář	k1gInSc1
webový	webový	k2eAgInSc1d1
(	(	kIx(
<g/>
staročeský	staročeský	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Český	český	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
korpus	korpus	k1gInSc1
</s>
<s>
DEBDict	DEBDict	k1gMnSc1
</s>
<s>
Neologismy	neologismus	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Díla	dílo	k1gNnSc2
dostupná	dostupný	k2eAgFnSc1d1
online	onlin	k1gInSc5
<g/>
:	:	kIx,
</s>
<s>
Český	český	k2eAgInSc1d1
jazykový	jazykový	k2eAgInSc1d1
atlas	atlas	k1gInSc1
</s>
<s>
Příruční	příruční	k2eAgInSc1d1
slovník	slovník	k1gInSc1
jazyka	jazyk	k1gInSc2
českého	český	k2eAgInSc2d1
</s>
<s>
Slovník	slovník	k1gInSc1
spisovného	spisovný	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
českého	český	k2eAgInSc2d1
</s>
<s>
Akademická	akademický	k2eAgFnSc1d1
mluvnice	mluvnice	k1gFnSc1
češtiny	čeština	k1gFnSc2
</s>
<s>
Internetová	internetový	k2eAgFnSc1d1
jazyková	jazykový	k2eAgFnSc1d1
příručka	příručka	k1gFnSc1
</s>
<s>
Čeština	čeština	k1gFnSc1
2.0	2.0	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Jazyk	jazyk	k1gInSc1
|	|	kIx~
Český	český	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
</s>
