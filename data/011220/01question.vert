<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
skladba	skladba	k1gFnSc1	skladba
heavymetalové	heavymetalový	k2eAgFnSc2d1	heavymetalová
skupiny	skupina	k1gFnSc2	skupina
Metallica	Metallica	k1gFnSc1	Metallica
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
jako	jako	k8xC	jako
soundtrack	soundtrack	k1gInSc1	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
Mission	Mission	k1gInSc4	Mission
Impossible	Impossible	k1gMnSc2	Impossible
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
?	?	kIx.	?
</s>
