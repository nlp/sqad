<p>
<s>
"	"	kIx"	"
<g/>
I	i	k9	i
Disappear	Disappear	k1gInSc1	Disappear
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
skladba	skladba	k1gFnSc1	skladba
heavymetalové	heavymetalový	k2eAgFnSc2d1	heavymetalová
skupiny	skupina	k1gFnSc2	skupina
Metallica	Metallic	k1gInSc2	Metallic
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
jako	jako	k8xS	jako
soundtrack	soundtrack	k1gInSc1	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
Mission	Mission	k1gInSc4	Mission
Impossible	Impossible	k1gMnSc2	Impossible
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
se	se	k3xPyFc4	se
držela	držet	k5eAaImAgFnS	držet
7	[number]	k4	7
týdnů	týden	k1gInPc2	týden
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
Mainstream	Mainstream	k1gInSc4	Mainstream
Rock	rock	k1gInSc1	rock
Tracks	Tracks	k1gInSc1	Tracks
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skladba	skladba	k1gFnSc1	skladba
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
skladeb	skladba	k1gFnPc2	skladba
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgInSc1d2	kratší
a	a	k8xC	a
opakuje	opakovat	k5eAaImIp3nS	opakovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
pátou	pátý	k4xOgFnSc4	pátý
minutu	minuta	k1gFnSc4	minuta
a	a	k8xC	a
zvuk	zvuk	k1gInSc4	zvuk
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
čerstvý	čerstvý	k2eAgInSc1d1	čerstvý
<g/>
,	,	kIx,	,
rockový	rockový	k2eAgInSc1d1	rockový
a	a	k8xC	a
moderní	moderní	k2eAgMnSc1d1	moderní
<g/>
,	,	kIx,	,
kytarové	kytarový	k2eAgNnSc1d1	kytarové
sólo	sólo	k1gNnSc1	sólo
není	být	k5eNaImIp3nS	být
nijak	nijak	k6eAd1	nijak
složité	složitý	k2eAgNnSc1d1	složité
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
krátké	krátký	k2eAgNnSc1d1	krátké
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
byla	být	k5eAaImAgFnS	být
pevnou	pevný	k2eAgFnSc7d1	pevná
součástí	součást	k1gFnSc7	součást
Summer	Summra	k1gFnPc2	Summra
Sanitarium	Sanitarium	k1gNnSc1	Sanitarium
Tour	Tour	k1gMnSc1	Tour
2000	[number]	k4	2000
a	a	k8xC	a
občasně	občasně	k6eAd1	občasně
byla	být	k5eAaImAgFnS	být
hrána	hrát	k5eAaImNgFnS	hrát
na	na	k7c4	na
Madly	Madla	k1gFnPc4	Madla
in	in	k?	in
Anger	Anger	k1gMnSc1	Anger
with	with	k1gMnSc1	with
the	the	k?	the
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
přidáno	přidat	k5eAaPmNgNnS	přidat
také	také	k9	také
intro	intro	k1gNnSc1	intro
převzaté	převzatý	k2eAgNnSc1d1	převzaté
z	z	k7c2	z
refrénu	refrén	k1gInSc2	refrén
písně	píseň	k1gFnSc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
povšimnutí	povšimnutí	k1gNnSc4	povšimnutí
<g/>
,	,	kIx,	,
že	že	k8xS	že
text	text	k1gInSc1	text
písně	píseň	k1gFnSc2	píseň
magicky	magicky	k6eAd1	magicky
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
odchodem	odchod	k1gInSc7	odchod
baskytaristy	baskytarista	k1gMnSc2	baskytarista
Jasona	Jason	k1gMnSc2	Jason
Newsteda	Newsted	k1gMnSc2	Newsted
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
odešel	odejít	k5eAaPmAgMnS	odejít
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
o	o	k7c4	o
tři	tři	k4xCgFnPc4	tři
čtvrtě	čtvrt	k1gFnPc4	čtvrt
roku	rok	k1gInSc2	rok
později	pozdě	k6eAd2	pozdě
po	po	k7c6	po
nahrání	nahrání	k1gNnSc6	nahrání
skladby	skladba	k1gFnSc2	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Videoklip	videoklip	k1gInSc4	videoklip
==	==	k?	==
</s>
</p>
<p>
<s>
Videoklip	videoklip	k1gInSc1	videoklip
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
písni	píseň	k1gFnSc3	píseň
režíroval	režírovat	k5eAaImAgMnS	režírovat
osvědčený	osvědčený	k2eAgMnSc1d1	osvědčený
Wayne	Wayn	k1gInSc5	Wayn
Isham	Isham	k1gInSc4	Isham
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
video	video	k1gNnSc1	video
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
členy	člen	k1gMnPc4	člen
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
rolích	role	k1gFnPc6	role
jiných	jiný	k2eAgInPc2d1	jiný
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Hetfield	Hetfield	k1gMnSc1	Hetfield
uniká	unikat	k5eAaImIp3nS	unikat
ve	v	k7c6	v
svém	své	k1gNnSc6	své
1967	[number]	k4	1967
Chevroletu	chevrolet	k1gInSc2	chevrolet
Camaro	Camara	k1gFnSc5	Camara
strmými	strmý	k2eAgInPc7d1	strmý
ulicemi	ulice	k1gFnPc7	ulice
San	San	k1gMnSc1	San
Francisca	Francisca	k1gMnSc1	Francisca
postupující	postupující	k2eAgFnSc3d1	postupující
katastrofě	katastrofa	k1gFnSc3	katastrofa
tak	tak	k9	tak
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Bulittův	Bulittův	k2eAgInSc1d1	Bulittův
případ	případ	k1gInSc1	případ
<g/>
,	,	kIx,	,
Kirk	Kirk	k1gMnSc1	Kirk
Hammett	Hammett	k1gMnSc1	Hammett
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
poušti	poušť	k1gFnSc6	poušť
pronásledován	pronásledován	k2eAgInSc4d1	pronásledován
letadlem	letadlo	k1gNnSc7	letadlo
z	z	k7c2	z
filmu	film	k1gInSc2	film
Na	na	k7c4	na
sever	sever	k1gInSc4	sever
severozápadní	severozápadní	k2eAgFnSc7d1	severozápadní
linkou	linka	k1gFnSc7	linka
<g/>
,	,	kIx,	,
Jason	Jason	k1gMnSc1	Jason
Newsted	Newsted	k1gMnSc1	Newsted
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
projít	projít	k5eAaPmF	projít
davem	dav	k1gInSc7	dav
a	a	k8xC	a
čelí	čelit	k5eAaImIp3nS	čelit
tisícům	tisíc	k4xCgInPc3	tisíc
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
Brazil	Brazil	k1gMnPc2	Brazil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Lars	Lars	k1gInSc4	Lars
Ulrich	Ulrich	k1gMnSc1	Ulrich
sebevražedně	sebevražedně	k6eAd1	sebevražedně
vyskočí	vyskočit	k5eAaPmIp3nS	vyskočit
z	z	k7c2	z
budovy	budova	k1gFnSc2	budova
(	(	kIx(	(
<g/>
Smrtonosná	smrtonosný	k2eAgFnSc1d1	Smrtonosná
past	past	k1gFnSc1	past
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Auto	auto	k1gNnSc1	auto
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
řídil	řídit	k5eAaImAgMnS	řídit
James	James	k1gMnSc1	James
ve	v	k7c6	v
videu	video	k1gNnSc6	video
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
darováno	darovat	k5eAaPmNgNnS	darovat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
jej	on	k3xPp3gMnSc4	on
James	James	k1gMnSc1	James
vydražil	vydražit	k5eAaPmAgMnS	vydražit
na	na	k7c4	na
eBay	eBaa	k1gFnPc4	eBaa
a	a	k8xC	a
výtěžek	výtěžek	k1gInSc4	výtěžek
věnoval	věnovat	k5eAaImAgMnS	věnovat
na	na	k7c4	na
charitativní	charitativní	k2eAgInPc4d1	charitativní
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kauza	kauza	k1gFnSc1	kauza
Napster	Napster	k1gMnSc1	Napster
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2000	[number]	k4	2000
objevil	objevit	k5eAaPmAgInS	objevit
Lars	Lars	k1gInSc1	Lars
Ulrich	Ulrich	k1gMnSc1	Ulrich
na	na	k7c6	na
P2P	P2P	k1gFnSc6	P2P
sítí	síť	k1gFnPc2	síť
pro	pro	k7c4	pro
sdílení	sdílení	k1gNnSc4	sdílení
mp	mp	k?	mp
<g/>
3	[number]	k4	3
souborů	soubor	k1gInPc2	soubor
Napster	Napstra	k1gFnPc2	Napstra
demo	demo	k2eAgFnPc2d1	demo
této	tento	k3xDgFnSc2	tento
skladby	skladba	k1gFnSc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
soudní	soudní	k2eAgNnSc4d1	soudní
řešení	řešení	k1gNnSc4	řešení
tohoto	tento	k3xDgInSc2	tento
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
odstrašujícím	odstrašující	k2eAgInSc7d1	odstrašující
případem	případ	k1gInSc7	případ
pro	pro	k7c4	pro
nelegální	legální	k2eNgNnSc4d1	nelegální
stahování	stahování	k1gNnSc4	stahování
a	a	k8xC	a
šíření	šíření	k1gNnSc4	šíření
hudby	hudba	k1gFnSc2	hudba
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Metallica	Metallica	k6eAd1	Metallica
konflikt	konflikt	k1gInSc1	konflikt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přerostl	přerůst	k5eAaPmAgInS	přerůst
v	v	k7c4	v
gigantický	gigantický	k2eAgInSc4d1	gigantický
soudní	soudní	k2eAgInSc4d1	soudní
proces	proces	k1gInSc4	proces
sice	sice	k8xC	sice
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
skončil	skončit	k5eAaPmAgMnS	skončit
mimosoudní	mimosoudní	k2eAgFnSc7d1	mimosoudní
dohodou	dohoda	k1gFnSc7	dohoda
mezi	mezi	k7c7	mezi
Metallicou	Metallica	k1gFnSc7	Metallica
a	a	k8xC	a
Napsterem	Napster	k1gInSc7	Napster
a	a	k8xC	a
Napster	Napster	k1gInSc1	Napster
byl	být	k5eAaImAgInS	být
převeden	převést	k5eAaPmNgInS	převést
na	na	k7c4	na
placenou	placený	k2eAgFnSc4d1	placená
službu	služba	k1gFnSc4	služba
<g/>
.	.	kIx.	.
</s>
</p>
