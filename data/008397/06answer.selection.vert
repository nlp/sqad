<s>
Poetismus	poetismus	k1gInSc1	poetismus
je	být	k5eAaImIp3nS	být
původem	původ	k1gInSc7	původ
literární	literární	k2eAgInSc4d1	literární
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
nerozšířil	rozšířit	k5eNaPmAgMnS	rozšířit
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
výlučně	výlučně	k6eAd1	výlučně
český	český	k2eAgInSc4d1	český
avantgardní	avantgardní	k2eAgInSc4d1	avantgardní
směr	směr	k1gInSc4	směr
<g/>
.	.	kIx.	.
</s>
