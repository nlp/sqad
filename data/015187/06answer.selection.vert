<s desamb="1">
V	v	k7c6
počtu	počet	k1gInSc6
dvou	dva	k4xCgInPc2
kusů	kus	k1gInPc2
ji	on	k3xPp3gFnSc4
vyrobila	vyrobit	k5eAaPmAgFnS
plzeňská	plzeňský	k2eAgFnSc1d1
firma	firma	k1gFnSc1
Škoda	škoda	k1gFnSc1
v	v	k7c6
letech	léto	k1gNnPc6
1958	#num#	k4
a	a	k8xC
1959	#num#	k4
pod	pod	k7c7
továrním	tovární	k2eAgNnSc7d1
označením	označení	k1gNnSc7
Škoda	škoda	k1gFnSc1
23	#num#	k4
<g/>
E.	E.	kA
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
prototypové	prototypový	k2eAgInPc4d1
stroje	stroj	k1gInPc4
<g/>
,	,	kIx,
na	na	k7c4
něž	jenž	k3xRgMnPc4
poté	poté	k6eAd1
navázala	navázat	k5eAaPmAgFnS
výroba	výroba	k1gFnSc1
dalších	další	k2eAgInPc2d1
typů	typ	k1gInPc2
lokomotiv	lokomotiva	k1gFnPc2
<g/>
.	.	kIx.
</s>