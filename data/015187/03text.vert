<s>
Lokomotiva	lokomotiva	k1gFnSc1
180	#num#	k4
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
substituovaný	substituovaný	k2eAgInSc1d1
infobox	infobox	k1gInSc1
<g/>
.	.	kIx.
<g/>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
převedete	převést	k5eAaPmIp2nP
na	na	k7c4
standardní	standardní	k2eAgFnSc4d1
šablonu	šablona	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
lokomotiva	lokomotiva	k1gFnSc1
řady	řada	k1gFnSc2
180	#num#	k4
(	(	kIx(
<g/>
dle	dle	k7c2
označení	označení	k1gNnSc2
UIC	UIC	kA
<g/>
)	)	kIx)
</s>
<s>
Staré	staré	k1gNnSc1
označení	označení	k1gNnSc2
ČSDE	ČSDE	kA
699.0	699.0	k4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
E	E	kA
669.0	669.0	k4
</s>
<s>
Tovární	tovární	k2eAgNnSc1d1
označení	označení	k1gNnSc1
<g/>
23	#num#	k4
<g/>
E	E	kA
</s>
<s>
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Výrobce	výrobce	k1gMnSc1
Škoda	Škoda	k1gMnSc1
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
výroby	výroba	k1gFnSc2
1958	#num#	k4
<g/>
,	,	kIx,
1959	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
vyrobených	vyrobený	k2eAgInPc2d1
kusů	kus	k1gInPc2
<g/>
2	#num#	k4
</s>
<s>
Provozovatel	provozovatel	k1gMnSc1
(	(	kIx(
<g/>
ČSD	ČSD	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ČD	ČD	kA
</s>
<s>
Období	období	k1gNnSc1
provozu	provoz	k1gInSc2
1958	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
</s>
<s>
Trvalý	trvalý	k2eAgInSc1d1
výkon	výkon	k1gInSc1
3	#num#	k4
048	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
180.001	180.001	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
610	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
180.002	180.002	k4
<g/>
)	)	kIx)
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
tažná	tažný	k2eAgFnSc1d1
síla	síla	k1gFnSc1
345	#num#	k4
kN	kN	k?
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
90	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
a	a	k8xC
rozměry	rozměr	k1gInPc1
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
ve	v	k7c6
službě	služba	k1gFnSc6
119	#num#	k4
t	t	k?
</s>
<s>
Délka	délka	k1gFnSc1
přes	přes	k7c4
nárazníky	nárazník	k1gInPc4
18	#num#	k4
800	#num#	k4
mm	mm	kA
</s>
<s>
Minimální	minimální	k2eAgInSc1d1
poloměrprojížděných	poloměrprojížděný	k2eAgInPc2d1
oblouků	oblouk	k1gInPc2
<g/>
125	#num#	k4
m	m	kA
</s>
<s>
Rozchod	rozchod	k1gInSc4
kolejí	kolej	k1gFnPc2
<g/>
1	#num#	k4
435	#num#	k4
mm	mm	kA
</s>
<s>
Parametry	parametr	k1gInPc1
pohonu	pohon	k1gInSc2
</s>
<s>
Uspořádání	uspořádání	k1gNnSc1
pojezduCo	pojezduCo	k1gMnSc1
<g/>
´	´	k?
Co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
<g/>
´	´	k?
</s>
<s>
Regulace	regulace	k1gFnSc1
pohonu	pohon	k1gInSc2
odporová	odporový	k2eAgFnSc1d1
</s>
<s>
Napájecí	napájecí	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
<g/>
3	#num#	k4
kV	kV	k?
</s>
<s>
Seznam	seznam	k1gInSc1
českých	český	k2eAgFnPc2d1
a	a	k8xC
slovenských	slovenský	k2eAgFnPc2d1
lokomotiv	lokomotiva	k1gFnPc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
české	český	k2eAgFnSc6d1
lokomotivě	lokomotiva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
německé	německý	k2eAgFnSc6d1
lokomotivě	lokomotiva	k1gFnSc6
řady	řada	k1gFnSc2
180	#num#	k4
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc4
Lokomotiva	lokomotiva	k1gFnSc1
372	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
elektrické	elektrický	k2eAgFnSc6d1
lokomotivě	lokomotiva	k1gFnSc6
ČSD	ČSD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
parní	parní	k2eAgFnSc6d1
lokomotivě	lokomotiva	k1gFnSc6
řady	řada	k1gFnSc2
180	#num#	k4
kkStB	kkStB	k?
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
kkStB	kkStB	k?
řada	řada	k1gFnSc1
180	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Lokomotiva	lokomotiva	k1gFnSc1
řady	řada	k1gFnSc2
180	#num#	k4
je	být	k5eAaImIp3nS
elektrická	elektrický	k2eAgFnSc1d1
lokomotiva	lokomotiva	k1gFnSc1
na	na	k7c4
stejnosměrný	stejnosměrný	k2eAgInSc4d1
proud	proud	k1gInSc4
určená	určený	k2eAgFnSc1d1
především	především	k9
pro	pro	k7c4
nákladní	nákladní	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
počtu	počet	k1gInSc6
dvou	dva	k4xCgInPc2
kusů	kus	k1gInPc2
ji	on	k3xPp3gFnSc4
vyrobila	vyrobit	k5eAaPmAgFnS
plzeňská	plzeňský	k2eAgFnSc1d1
firma	firma	k1gFnSc1
Škoda	škoda	k1gFnSc1
v	v	k7c6
letech	léto	k1gNnPc6
1958	#num#	k4
a	a	k8xC
1959	#num#	k4
pod	pod	k7c7
továrním	tovární	k2eAgNnSc7d1
označením	označení	k1gNnSc7
Škoda	škoda	k1gFnSc1
23	#num#	k4
<g/>
E.	E.	kA
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
prototypové	prototypový	k2eAgInPc4d1
stroje	stroj	k1gInPc4
<g/>
,	,	kIx,
na	na	k7c4
něž	jenž	k3xRgMnPc4
poté	poté	k6eAd1
navázala	navázat	k5eAaPmAgFnS
výroba	výroba	k1gFnSc1
dalších	další	k2eAgInPc2d1
typů	typ	k1gInPc2
lokomotiv	lokomotiva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
původní	původní	k2eAgFnSc1d1
teze	teze	k1gFnSc1
univerzální	univerzální	k2eAgFnSc2d1
lokomotivy	lokomotiva	k1gFnSc2
pro	pro	k7c4
osobní	osobní	k2eAgFnSc4d1
i	i	k8xC
nákladní	nákladní	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
u	u	k7c2
řady	řada	k1gFnSc2
140	#num#	k4
selhala	selhat	k5eAaPmAgFnS
kvůli	kvůli	k7c3
nedostatečné	dostatečný	k2eNgFnSc3d1
tažné	tažný	k2eAgFnSc3d1
síle	síla	k1gFnSc3
a	a	k8xC
výkonu	výkon	k1gInSc2
<g/>
,	,	kIx,
objednaly	objednat	k5eAaPmAgFnP
si	se	k3xPyFc3
ČSD	ČSD	kA
nový	nový	k2eAgInSc4d1
typ	typ	k1gInSc4
lokomotivy	lokomotiva	k1gFnSc2
s	s	k7c7
vyšším	vysoký	k2eAgInSc7d2
výkonem	výkon	k1gInSc7
a	a	k8xC
šesti	šest	k4xCc7
nápravami	náprava	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
by	by	kYmCp3nP
lépe	dobře	k6eAd2
vyhovovaly	vyhovovat	k5eAaImAgFnP
náročným	náročný	k2eAgFnPc3d1
podmínkám	podmínka	k1gFnPc3
na	na	k7c6
trati	trať	k1gFnSc6
Praha	Praha	k1gFnSc1
–	–	k?
Košice	Košice	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc4d1
provedení	provedení	k1gNnSc1
lokomotiv	lokomotiva	k1gFnPc2
bylo	být	k5eAaImAgNnS
se	s	k7c7
šesti	šest	k4xCc7
elektromotory	elektromotor	k1gInPc7
s	s	k7c7
výkonem	výkon	k1gInSc7
508	#num#	k4
kW	kW	kA
a	a	k8xC
převodovým	převodový	k2eAgInSc7d1
poměrem	poměr	k1gInSc7
pro	pro	k7c4
rychlost	rychlost	k1gFnSc4
120	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Přestože	přestože	k8xS
lokomotivy	lokomotiva	k1gFnPc1
předvedly	předvést	k5eAaPmAgFnP
velmi	velmi	k6eAd1
dobré	dobrý	k2eAgInPc1d1
výkony	výkon	k1gInPc1
a	a	k8xC
v	v	k7c6
dopravě	doprava	k1gFnSc6
těžkých	těžký	k2eAgInPc2d1
rychlíků	rychlík	k1gMnPc2
předčily	předčít	k5eAaPmAgInP,k5eAaBmAgInP
slabší	slabý	k2eAgFnSc4d2
řadu	řada	k1gFnSc4
140	#num#	k4
<g/>
,	,	kIx,
ukázalo	ukázat	k5eAaPmAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gInSc1
výkon	výkon	k1gInSc1
a	a	k8xC
tažná	tažný	k2eAgFnSc1d1
síla	síla	k1gFnSc1
způsobuje	způsobovat	k5eAaImIp3nS
hodně	hodně	k6eAd1
problémů	problém	k1gInPc2
s	s	k7c7
přetrháváním	přetrhávání	k1gNnSc7
šroubovek	šroubovka	k1gFnPc2
(	(	kIx(
<g/>
součást	součást	k1gFnSc1
spojovacího	spojovací	k2eAgNnSc2d1
ústrojí	ústrojí	k1gNnSc2
mezi	mezi	k7c7
vagony	vagon	k1gInPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
byly	být	k5eAaImAgFnP
u	u	k7c2
jedné	jeden	k4xCgFnSc2
lokomotivy	lokomotiva	k1gFnPc1
dosazeny	dosazen	k2eAgInPc4d1
nové	nový	k2eAgInPc4d1
motory	motor	k1gInPc4
s	s	k7c7
nižším	nízký	k2eAgInSc7d2
výkonem	výkon	k1gInSc7
435	#num#	k4
kW	kW	kA
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
později	pozdě	k6eAd2
dosazeny	dosadit	k5eAaPmNgInP
na	na	k7c4
lokomotivy	lokomotiva	k1gFnPc4
řady	řada	k1gFnSc2
181	#num#	k4
a	a	k8xC
na	na	k7c6
tomto	tento	k3xDgInSc6
stroji	stroj	k1gInSc6
byly	být	k5eAaImAgInP
pro	pro	k7c4
tento	tento	k3xDgInSc4
účel	účel	k1gInSc4
vyzkoušeny	vyzkoušen	k2eAgFnPc1d1
(	(	kIx(
<g/>
celkový	celkový	k2eAgInSc1d1
výkon	výkon	k1gInSc1
lokomotivy	lokomotiva	k1gFnSc2
poklesl	poklesnout	k5eAaPmAgInS
na	na	k7c4
2	#num#	k4
610	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhé	druhý	k4xOgNnSc1
lokomotivě	lokomotiva	k1gFnSc3
byl	být	k5eAaImAgInS
rovnou	rovnou	k6eAd1
dosazen	dosazen	k2eAgInSc1d1
nový	nový	k2eAgInSc1d1
typ	typ	k1gInSc1
podvozku	podvozek	k1gInSc2
a	a	k8xC
výkon	výkon	k1gInSc1
zůstal	zůstat	k5eAaPmAgInS
na	na	k7c4
3	#num#	k4
048	#num#	k4
kW	kW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
byla	být	k5eAaImAgFnS
také	také	k6eAd1
oběma	dva	k4xCgMnPc7
strojům	stroj	k1gInPc3
snížena	snížit	k5eAaPmNgFnS
maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
na	na	k7c4
90	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
že	že	k9
šlo	jít	k5eAaImAgNnS
o	o	k7c4
prototypové	prototypový	k2eAgInPc4d1
stroje	stroj	k1gInPc4
<g/>
,	,	kIx,
dále	daleko	k6eAd2
výroba	výroba	k1gFnSc1
nepokračovala	pokračovat	k5eNaImAgFnS
a	a	k8xC
zdokonalená	zdokonalený	k2eAgFnSc1d1
verze	verze	k1gFnSc1
byla	být	k5eAaImAgFnS
rovnou	rovnou	k6eAd1
vyrobena	vyroben	k2eAgFnSc1d1
pod	pod	k7c7
novou	nový	k2eAgFnSc7d1
řadou	řada	k1gFnSc7
E	E	kA
669.1	669.1	k4
(	(	kIx(
<g/>
181	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
dvou	dva	k4xCgInPc2
prototypových	prototypový	k2eAgInPc2d1
strojů	stroj	k1gInPc2
pro	pro	k7c4
ČSD	ČSD	kA
byly	být	k5eAaImAgInP
vyrobeny	vyrobit	k5eAaPmNgInP
dva	dva	k4xCgInPc1
další	další	k2eAgInPc1d1
téměř	téměř	k6eAd1
shodné	shodný	k2eAgInPc1d1
pro	pro	k7c4
Sovětský	sovětský	k2eAgInSc4d1
svaz	svaz	k1gInSc4
(	(	kIx(
<g/>
řada	řada	k1gFnSc1
ČS	čs	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
tovární	tovární	k2eAgInSc1d1
typ	typ	k1gInSc1
Škoda	škoda	k1gFnSc1
25	#num#	k4
<g/>
E	E	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
ně	on	k3xPp3gMnPc4
poté	poté	k6eAd1
navázala	navázat	k5eAaPmAgFnS
sériová	sériový	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
</s>
<s>
Lokomotiva	lokomotiva	k1gFnSc1
má	mít	k5eAaImIp3nS
ocelovou	ocelový	k2eAgFnSc4d1
lokomotivní	lokomotivní	k2eAgFnSc4d1
skříň	skříň	k1gFnSc4
po	po	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
s	s	k7c7
žaluziemi	žaluzie	k1gFnPc7
kvůli	kvůli	k7c3
nasávání	nasávání	k1gNnSc3
chladicího	chladicí	k2eAgInSc2d1
vzduchu	vzduch	k1gInSc2
(	(	kIx(
<g/>
první	první	k4xOgNnSc4
použití	použití	k1gNnSc4
tohoto	tento	k3xDgInSc2
systému	systém	k1gInSc2
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohání	pohánět	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
šest	šest	k4xCc1
elektromotorů	elektromotor	k1gInPc2
<g/>
,	,	kIx,
pro	pro	k7c4
každou	každý	k3xTgFnSc4
nápravu	náprava	k1gFnSc4
jeden	jeden	k4xCgInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
obou	dva	k4xCgInPc6
koncích	konec	k1gInPc6
lokomotivní	lokomotivní	k2eAgFnSc2d1
skříně	skříň	k1gFnSc2
jsou	být	k5eAaImIp3nP
stanoviště	stanoviště	k1gNnSc4
strojvedoucího	strojvedoucí	k1gMnSc2
se	s	k7c7
čtyřdílnými	čtyřdílný	k2eAgNnPc7d1
čelními	čelní	k2eAgNnPc7d1
okny	okno	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Regulace	regulace	k1gFnSc1
výkonu	výkon	k1gInSc2
je	být	k5eAaImIp3nS
klasická	klasický	k2eAgFnSc1d1
odporová	odporový	k2eAgFnSc1d1
(	(	kIx(
<g/>
principiálně	principiálně	k6eAd1
shodná	shodný	k2eAgFnSc1d1
s	s	k7c7
řadou	řada	k1gFnSc7
140	#num#	k4
<g/>
)	)	kIx)
–	–	k?
strojvedoucí	strojvedoucí	k1gMnSc1
vyřazováním	vyřazování	k1gNnSc7
rozjezdových	rozjezdový	k2eAgInPc2d1
odporů	odpor	k1gInPc2
volí	volit	k5eAaImIp3nS
jednotlivé	jednotlivý	k2eAgInPc4d1
jízdní	jízdní	k2eAgInPc4d1
stupně	stupeň	k1gInPc4
a	a	k8xC
může	moct	k5eAaImIp3nS
tak	tak	k6eAd1
regulovat	regulovat	k5eAaImF
výkon	výkon	k1gInSc4
lokomotivy	lokomotiva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzdová	brzdový	k2eAgFnSc1d1
výstroj	výstroj	k1gFnSc1
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
přímočinné	přímočinný	k2eAgFnSc2d1
a	a	k8xC
samočinné	samočinný	k2eAgFnSc2d1
tlakové	tlakový	k2eAgFnSc2d1
brzdy	brzda	k1gFnSc2
a	a	k8xC
zajišťovací	zajišťovací	k2eAgFnSc2d1
ruční	ruční	k2eAgFnSc2d1
brzdy	brzda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Provoz	provoz	k1gInSc1
</s>
<s>
Druhý	druhý	k4xOgInSc1
prototyp	prototyp	k1gInSc1
v	v	k7c6
chomutovském	chomutovský	k2eAgInSc6d1
depozitáři	depozitář	k1gInSc6
</s>
<s>
Stanoviště	stanoviště	k1gNnSc1
muzejní	muzejní	k2eAgFnSc2d1
lokomotivy	lokomotiva	k1gFnSc2
180.001	180.001	k4
(	(	kIx(
<g/>
E	E	kA
669.001	669.001	k4
<g/>
)	)	kIx)
</s>
<s>
Lokomotivy	lokomotiva	k1gFnPc1
byly	být	k5eAaImAgFnP
od	od	k7c2
výrobce	výrobce	k1gMnSc2
předány	předat	k5eAaPmNgFnP
do	do	k7c2
depa	depo	k1gNnSc2
Praha	Praha	k1gFnSc1
hlavní	hlavní	k2eAgNnSc4d1
nádraží	nádraží	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
podstoupily	podstoupit	k5eAaPmAgFnP
první	první	k4xOgFnPc1
zkoušky	zkouška	k1gFnPc1
v	v	k7c6
rychlíkové	rychlíkový	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
rekonstrukci	rekonstrukce	k1gFnSc6
a	a	k8xC
snížení	snížení	k1gNnSc6
rychlosti	rychlost	k1gFnSc2
byly	být	k5eAaImAgFnP
předány	předat	k5eAaPmNgFnP
do	do	k7c2
České	český	k2eAgFnSc2d1
Třebové	Třebová	k1gFnSc2
s	s	k7c7
určením	určení	k1gNnSc7
pro	pro	k7c4
těžkou	těžký	k2eAgFnSc4d1
nákladní	nákladní	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
již	již	k6eAd1
naplno	naplno	k6eAd1
osvědčily	osvědčit	k5eAaPmAgFnP
a	a	k8xC
po	po	k7c6
boku	bok	k1gInSc6
novějších	nový	k2eAgInPc2d2
strojů	stroj	k1gInPc2
sloužily	sloužit	k5eAaImAgFnP
bez	bez	k7c2
problémů	problém	k1gInPc2
po	po	k7c4
mnoho	mnoho	k4c4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
prototyp	prototyp	k1gInSc1
byl	být	k5eAaImAgInS
koncem	koncem	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
odstaven	odstavit	k5eAaPmNgInS
a	a	k8xC
navržen	navrhnout	k5eAaPmNgInS
na	na	k7c4
zrušení	zrušení	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
plánovanými	plánovaný	k2eAgFnPc7d1
oslavami	oslava	k1gFnPc7
150	#num#	k4
let	léto	k1gNnPc2
železnice	železnice	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
Třebové	Třebová	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
prošel	projít	k5eAaPmAgInS
hlavní	hlavní	k2eAgFnSc7d1
opravou	oprava	k1gFnSc7
s	s	k7c7
uvedením	uvedení	k1gNnSc7
do	do	k7c2
původní	původní	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
stroj	stroj	k1gInSc1
stále	stále	k6eAd1
sloužil	sloužit	k5eAaImAgInS
v	v	k7c6
běžné	běžný	k2eAgFnSc6d1
nákladní	nákladní	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
(	(	kIx(
<g/>
především	především	k9
na	na	k7c6
tratích	trať	k1gFnPc6
do	do	k7c2
Pardubic	Pardubice	k1gInPc2
<g/>
,	,	kIx,
Hradce	Hradec	k1gInSc2
Králové	Králová	k1gFnSc2
a	a	k8xC
Nymburka	Nymburk	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
stejně	stejně	k6eAd1
jako	jako	k9
„	„	k?
<g/>
jednička	jednička	k1gFnSc1
<g/>
“	“	k?
obdržel	obdržet	k5eAaPmAgMnS
historický	historický	k2eAgInSc4d1
nátěr	nátěr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
byl	být	k5eAaImAgInS
příležitostně	příležitostně	k6eAd1
provozován	provozovat	k5eAaImNgInS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
a	a	k8xC
poté	poté	k6eAd1
zařazen	zařazen	k2eAgInSc1d1
do	do	k7c2
muzejních	muzejní	k2eAgFnPc2d1
sbírek	sbírka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
již	již	k6eAd1
není	být	k5eNaImIp3nS
provozní	provozní	k2eAgMnSc1d1
a	a	k8xC
je	být	k5eAaImIp3nS
uchováván	uchovávat	k5eAaImNgInS
v	v	k7c6
depozitáři	depozitář	k1gInSc6
NTM	NTM	kA
v	v	k7c6
Chomutově	Chomutov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starší	starý	k2eAgInSc1d2
stroj	stroj	k1gInSc1
zůstává	zůstávat	k5eAaImIp3nS
v	v	k7c6
provozním	provozní	k2eAgInSc6d1
stavu	stav	k1gInSc6
v	v	k7c6
České	český	k2eAgFnSc6d1
Třebové	Třebová	k1gFnSc6
pro	pro	k7c4
občasné	občasný	k2eAgFnPc4d1
historické	historický	k2eAgFnPc4d1
jízdy	jízda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Význam	význam	k1gInSc1
</s>
<s>
Jako	jako	k9
první	první	k4xOgFnPc1
šestinápravové	šestinápravový	k2eAgFnPc1d1
elektrické	elektrický	k2eAgFnPc1d1
lokomotivy	lokomotiva	k1gFnPc1
ze	z	k7c2
Škody	škoda	k1gFnSc2
se	se	k3xPyFc4
staly	stát	k5eAaPmAgInP
oba	dva	k4xCgInPc1
stroje	stroj	k1gInPc1
řady	řada	k1gFnSc2
180	#num#	k4
základním	základní	k2eAgInSc7d1
kamenem	kámen	k1gInSc7
vývoje	vývoj	k1gInSc2
těžkých	těžký	k2eAgFnPc2d1
nákladních	nákladní	k2eAgFnPc2d1
lokomotiv	lokomotiva	k1gFnPc2
minimálně	minimálně	k6eAd1
na	na	k7c4
dvě	dva	k4xCgNnPc4
desetiletí	desetiletí	k1gNnPc4
dopředu	dopředu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohá	mnohé	k1gNnPc4
konstrukční	konstrukční	k2eAgNnPc4d1
řešení	řešení	k1gNnPc4
na	na	k7c6
nich	on	k3xPp3gFnPc6
poprvé	poprvé	k6eAd1
vyzkoušená	vyzkoušený	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
s	s	k7c7
úspěchem	úspěch	k1gInSc7
využita	využit	k2eAgFnSc1d1
v	v	k7c6
dalších	další	k2eAgInPc6d1
typech	typ	k1gInPc6
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
některé	některý	k3yIgInPc1
principy	princip	k1gInPc1
(	(	kIx(
<g/>
např.	např.	kA
vícedílná	vícedílný	k2eAgNnPc4d1
čelní	čelní	k2eAgNnPc4d1
okna	okno	k1gNnPc4
<g/>
,	,	kIx,
koncepce	koncepce	k1gFnPc4
chlazení	chlazení	k1gNnSc2
<g/>
)	)	kIx)
již	již	k6eAd1
znovu	znovu	k6eAd1
uplatněny	uplatněn	k2eAgFnPc1d1
nebyly	být	k5eNaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	se	k3xPyFc4
po	po	k7c6
provedených	provedený	k2eAgFnPc6d1
úpravách	úprava	k1gFnPc6
staly	stát	k5eAaPmAgFnP
spolehlivými	spolehlivý	k2eAgInPc7d1
a	a	k8xC
užitečnými	užitečný	k2eAgInPc7d1
pro	pro	k7c4
nákladní	nákladní	k2eAgInPc4d1
vlaky	vlak	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
nich	on	k3xPp3gMnPc2
novější	nový	k2eAgFnSc2d2
řady	řada	k1gFnSc2
181	#num#	k4
<g/>
,	,	kIx,
182	#num#	k4
a	a	k8xC
183	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historické	historický	k2eAgFnPc1d1
lokomotivy	lokomotiva	k1gFnPc1
</s>
<s>
180.001	180.001	k4
(	(	kIx(
<g/>
České	český	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
<g/>
,	,	kIx,
depo	depo	k1gNnSc4
Česká	český	k2eAgFnSc1d1
Třebová	Třebová	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
180.002	180.002	k4
(	(	kIx(
<g/>
Národní	národní	k2eAgNnSc1d1
technické	technický	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
depozitář	depozitář	k1gInSc1
Chomutov	Chomutov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
KOLEKTIV	kolektivum	k1gNnPc2
<g/>
,	,	kIx,
autorů	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ŽM	ŽM	kA
-	-	kIx~
Atlas	Atlas	k1gInSc1
vozidel	vozidlo	k1gNnPc2
1	#num#	k4
<g/>
.	.	kIx.
<g/>
díl	díl	k1gInSc1
–	–	k?
Elektrické	elektrický	k2eAgFnSc2d1
lokomotivy	lokomotiva	k1gFnSc2
ČD	ČD	kA
a	a	k8xC
ŽSR	ŽSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
M-Presse	M-Presse	k1gFnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
96	#num#	k4
s.	s.	k?
</s>
<s>
BITTNER	Bittner	k1gMnSc1
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
<g/>
;	;	kIx,
KŘENEK	Křenek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
;	;	kIx,
SKÁLA	Skála	k1gMnSc1
<g/>
,	,	kIx,
Bohumil	Bohumil	k1gMnSc1
<g/>
;	;	kIx,
ŠRÁMEK	Šrámek	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malý	malý	k2eAgInSc4d1
atlas	atlas	k1gInSc4
lokomotiv	lokomotiva	k1gFnPc2
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Gradis	Gradis	k1gFnSc1
Bohemia	bohemia	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
392	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86925	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Lokomotiva	lokomotiva	k1gFnSc1
180	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Lokomotiva	lokomotiva	k1gFnSc1
řady	řada	k1gFnSc2
180	#num#	k4
v	v	k7c6
atlasu	atlas	k1gInSc6
lokomotiv	lokomotiva	k1gFnPc2
Archivováno	archivovat	k5eAaBmNgNnS
31	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
Podrobnější	podrobný	k2eAgInSc1d2
popis	popis	k1gInSc1
lokomotivy	lokomotiva	k1gFnSc2
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgFnPc7d1
informacemi	informace	k1gFnPc7
o	o	k7c6
provozu	provoz	k1gInSc6
a	a	k8xC
dobovými	dobový	k2eAgFnPc7d1
fotografiemi	fotografia	k1gFnPc7
Archivováno	archivován	k2eAgNnSc4d1
31	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Hnací	hnací	k2eAgNnPc1d1
vozidla	vozidlo	k1gNnPc1
registrovaná	registrovaný	k2eAgNnPc1d1
v	v	k7c6
Česku	Česko	k1gNnSc6
El.	El.	k1gFnSc2
lokomotivy	lokomotiva	k1gFnSc2
stejnosměrné	stejnosměrný	k2eAgFnSc2d1
</s>
<s>
110	#num#	k4
•	•	k?
111	#num#	k4
•	•	k?
113	#num#	k4
•	•	k?
121	#num#	k4
•	•	k?
122	#num#	k4
•	•	k?
123	#num#	k4
•	•	k?
124	#num#	k4
•	•	k?
127	#num#	k4
•	•	k?
130	#num#	k4
•	•	k?
140	#num#	k4
•	•	k?
141	#num#	k4
•	•	k?
150	#num#	k4
•	•	k?
151	#num#	k4
•	•	k?
162	#num#	k4
•	•	k?
163	#num#	k4
•	•	k?
181	#num#	k4
•	•	k?
182	#num#	k4
•	•	k?
184	#num#	k4
♦	♦	k?
Neprovozované	provozovaný	k2eNgFnSc2d1
řady	řada	k1gFnSc2
<g/>
:	:	kIx,
100	#num#	k4
•	•	k?
106	#num#	k4
•	•	k?
112	#num#	k4
•	•	k?
114	#num#	k4
•	•	k?
169	#num#	k4
•	•	k?
180	#num#	k4
El.	El.	k1gFnSc6
lokomotivy	lokomotiva	k1gFnSc2
akumulátorové	akumulátorový	k2eAgFnSc2d1
</s>
<s>
199	#num#	k4
•	•	k?
704.2	704.2	k4
♦	♦	k?
Neprovozované	provozovaný	k2eNgFnSc2d1
řady	řada	k1gFnSc2
<g/>
:	:	kIx,
103	#num#	k4
El.	El.	k1gFnPc4
lokomotivy	lokomotiva	k1gFnSc2
střídavé	střídavý	k2eAgFnSc2d1
</s>
<s>
210	#num#	k4
•	•	k?
218	#num#	k4
•	•	k?
230	#num#	k4
•	•	k?
240	#num#	k4
•	•	k?
242	#num#	k4
•	•	k?
263	#num#	k4
♦	♦	k?
Neprovozované	provozovaný	k2eNgFnSc2d1
řady	řada	k1gFnSc2
<g/>
:	:	kIx,
209	#num#	k4
El.	El.	k1gFnSc2
lokomotivy	lokomotiva	k1gFnSc2
vícesystémové	vícesystémová	k1gFnSc2
</s>
<s>
340	#num#	k4
•	•	k?
362	#num#	k4
•	•	k?
363	#num#	k4
•	•	k?
363.5	363.5	k4
•	•	k?
365	#num#	k4
•	•	k?
371	#num#	k4
•	•	k?
372	#num#	k4
•	•	k?
380	#num#	k4
•	•	k?
383	#num#	k4
•	•	k?
386	#num#	k4
•	•	k?
388	#num#	k4
El.	El.	k1gFnSc6
jednotky	jednotka	k1gFnSc2
stejnosměrné	stejnosměrný	k2eAgFnSc2d1
</s>
<s>
440	#num#	k4
•	•	k?
460	#num#	k4
•	•	k?
471	#num#	k4
•	•	k?
480	#num#	k4
♦	♦	k?
Neprovozované	provozovaný	k2eNgFnSc2d1
řady	řada	k1gFnSc2
<g/>
:	:	kIx,
451	#num#	k4
•	•	k?
452	#num#	k4
•	•	k?
470	#num#	k4
El.	El.	k1gFnSc6
jednotky	jednotka	k1gFnSc2
střídavé	střídavý	k2eAgFnSc2d1
</s>
<s>
560	#num#	k4
El.	El.	k1gFnSc1
jednotky	jednotka	k1gFnSc2
vícesystémové	vícesystémová	k1gFnSc2
</s>
<s>
640	#num#	k4
•	•	k?
650	#num#	k4
•	•	k?
660	#num#	k4
•	•	k?
665	#num#	k4
•	•	k?
680	#num#	k4
Motorové	motorový	k2eAgFnPc1d1
lokomotivy	lokomotiva	k1gFnPc1
</s>
<s>
700	#num#	k4
•	•	k?
701	#num#	k4
•	•	k?
702	#num#	k4
•	•	k?
702.9	702.9	k4
•	•	k?
703	#num#	k4
•	•	k?
703.5	703.5	k4
•	•	k?
703.7	703.7	k4
•	•	k?
704	#num#	k4
•	•	k?
704.5	704.5	k4
•	•	k?
705.9	705.9	k4
•	•	k?
706.4	706.4	k4
•	•	k?
706.42	706.42	k4
•	•	k?
706.45	706.45	k4
•	•	k?
706.5	706.5	k4
•	•	k?
706.9	706.9	k4
•	•	k?
708	#num#	k4
•	•	k?
709	#num#	k4
(	(	kIx(
<g/>
709.4	709.4	k4
•	•	k?
709.5	709.5	k4
•	•	k?
709.6	709.6	k4
•	•	k?
709.7	709.7	k4
<g/>
)	)	kIx)
•	•	k?
710	#num#	k4
•	•	k?
711	#num#	k4
•	•	k?
711.5	711.5	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
711.7	711.7	k4
•	•	k?
712	#num#	k4
•	•	k?
714	#num#	k4
•	•	k?
716	#num#	k4
•	•	k?
717	#num#	k4
•	•	k?
719	#num#	k4
•	•	k?
720	#num#	k4
•	•	k?
721	#num#	k4
•	•	k?
724.6	724.6	k4
•	•	k?
724.7	724.7	k4
•	•	k?
724.8	724.8	k4
•	•	k?
725	#num#	k4
•	•	k?
726	#num#	k4
•	•	k?
729	#num#	k4
(	(	kIx(
<g/>
729.5	729.5	k4
•	•	k?
729.6	729.6	k4
<g/>
)	)	kIx)
•	•	k?
730	#num#	k4
•	•	k?
731	#num#	k4
•	•	k?
740	#num#	k4
•	•	k?
740.3	740.3	k4
•	•	k?
741.5	741.5	k4
•	•	k?
741.7	741.7	k4
•	•	k?
742	#num#	k4
•	•	k?
742.7	742.7	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
743	#num#	k4
•	•	k?
744	#num#	k4
•	•	k?
744.1	744.1	k4
•	•	k?
744.5	744.5	k4
•	•	k?
744.7	744.7	k4
•	•	k?
745	#num#	k4
•	•	k?
746	#num#	k4
•	•	k?
748.45	748.45	k4
•	•	k?
748.47	748.47	k4
•	•	k?
748.5	748.5	k4
•	•	k?
749	#num#	k4
•	•	k?
750	#num#	k4
•	•	k?
750.7	750.7	k4
•	•	k?
751	#num#	k4
•	•	k?
752.6	752.6	k4
•	•	k?
753	#num#	k4
•	•	k?
753.6	753.6	k4
•	•	k?
753.7	753.7	k4
•	•	k?
754	#num#	k4
•	•	k?
755	#num#	k4
•	•	k?
761	#num#	k4
•	•	k?
770	#num#	k4
(	(	kIx(
<g/>
770.0	770.0	k4
•	•	k?
770.5	770.5	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
770.6	770.6	k4
<g/>
)	)	kIx)
•	•	k?
771	#num#	k4
(	(	kIx(
<g/>
771.0	771.0	k4
•	•	k?
771.5	771.5	k4
•	•	k?
771.7	771.7	k4
<g/>
)	)	kIx)
•	•	k?
774.7	774.7	k4
•	•	k?
783	#num#	k4
•	•	k?
794	#num#	k4
•	•	k?
796	#num#	k4
•	•	k?
797.5	797.5	k4
•	•	k?
797.7	797.7	k4
•	•	k?
797.8	797.8	k4
•	•	k?
798.5	798.5	k4
•	•	k?
799	#num#	k4
♦	♦	k?
Neprovozované	provozovaný	k2eNgFnSc2d1
řady	řada	k1gFnSc2
<g/>
:	:	kIx,
715	#num#	k4
•	•	k?
718	#num#	k4
•	•	k?
735	#num#	k4
•	•	k?
752	#num#	k4
•	•	k?
759	#num#	k4
•	•	k?
781	#num#	k4
Motorové	motorový	k2eAgInPc1d1
vozy	vůz	k1gInPc1
a	a	k8xC
jednotky	jednotka	k1gFnPc1
</s>
<s>
805.9	805.9	k4
•	•	k?
809	#num#	k4
•	•	k?
810	#num#	k4
•	•	k?
811	#num#	k4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
•	•	k?
813.1	813.1	k4
•	•	k?
813.2	813.2	k4
•	•	k?
814	#num#	k4
•	•	k?
816	#num#	k4
•	•	k?
817	#num#	k4
•	•	k?
832	#num#	k4
•	•	k?
840	#num#	k4
•	•	k?
841	#num#	k4
•	•	k?
842	#num#	k4
•	•	k?
843	#num#	k4
•	•	k?
844	#num#	k4
•	•	k?
845	#num#	k4
•	•	k?
846	#num#	k4
•	•	k?
854	#num#	k4
•	•	k?
892	#num#	k4
(	(	kIx(
<g/>
MVTV	MVTV	kA
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
893	#num#	k4
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
MVTV	MVTV	kA
3	#num#	k4
<g/>
)	)	kIx)
♦	♦	k?
Neprovozované	provozovaný	k2eNgFnSc2d1
řady	řada	k1gFnSc2
<g/>
:	:	kIx,
801	#num#	k4
•	•	k?
802	#num#	k4
•	•	k?
811	#num#	k4
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
812	#num#	k4
•	•	k?
815	#num#	k4
•	•	k?
820	#num#	k4
•	•	k?
830	#num#	k4
•	•	k?
831	#num#	k4
•	•	k?
835	#num#	k4
•	•	k?
850	#num#	k4
•	•	k?
851	#num#	k4
•	•	k?
852	#num#	k4
•	•	k?
853	#num#	k4
•	•	k?
860	#num#	k4
•	•	k?
890	#num#	k4
•	•	k?
891	#num#	k4
•	•	k?
894	#num#	k4
Parní	parní	k2eAgFnPc1d1
lokomotivy	lokomotiva	k1gFnPc1
</s>
<s>
Neprovozované	provozovaný	k2eNgFnPc1d1
řady	řada	k1gFnPc1
<g/>
:	:	kIx,
998	#num#	k4
</s>
<s>
Hnací	hnací	k2eAgNnPc1d1
vozidla	vozidlo	k1gNnPc1
provozovaná	provozovaný	k2eAgNnPc1d1
v	v	k7c6
Československu	Československo	k1gNnSc6
El.	El.	k1gFnSc2
lokomotivy	lokomotiva	k1gFnSc2
stejnosměrné	stejnosměrný	k2eAgFnSc2d1
</s>
<s>
E	E	kA
200.0	200.0	k4
•	•	k?
E	E	kA
222.0	222.0	k4
•	•	k?
E	E	kA
225.0	225.0	k4
•	•	k?
E	E	kA
406.0	406.0	k4
•	•	k?
E	E	kA
413.2	413.2	k4
•	•	k?
E	E	kA
416.0	416.0	k4
•	•	k?
E	E	kA
422.0	422.0	k4
•	•	k?
E	E	kA
423.0	423.0	k4
•	•	k?
E	E	kA
424.0	424.0	k4
•	•	k?
E	E	kA
424.1	424.1	k4
•	•	k?
E	E	kA
426.0	426.0	k4
•	•	k?
E	E	kA
436.0	436.0	k4
•	•	k?
E	E	kA
457.0	457.0	k4
•	•	k?
E	E	kA
458.0	458.0	k4
•	•	k?
E	E	kA
458.1	458.1	k4
•	•	k?
E	E	kA
465.0	465.0	k4
•	•	k?
E	E	kA
466.0	466.0	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
E	E	kA
466.1	466.1	k4
•	•	k?
E	E	kA
467.0	467.0	k4
•	•	k?
E	E	kA
469.0	469.0	k4
•	•	k?
E	E	kA
469.1	469.1	k4
•	•	k?
E	E	kA
469.2	469.2	k4
•	•	k?
E	E	kA
469.3	469.3	k4
•	•	k?
E	E	kA
469.303	469.303	k4
<g/>
0	#num#	k4
•	•	k?
E	E	kA
469.5	469.5	k4
•	•	k?
E	E	kA
479.0	479.0	k4
•	•	k?
E	E	kA
479.1	479.1	k4
•	•	k?
E	E	kA
499.0	499.0	k4
•	•	k?
E	E	kA
499.1	499.1	k4
•	•	k?
E	E	kA
499.2	499.2	k4
•	•	k?
E	E	kA
499.3	499.3	k4
•	•	k?
E	E	kA
499.5	499.5	k4
•	•	k?
E	E	kA
659.0	659.0	k4
•	•	k?
E	E	kA
666.0	666.0	k4
•	•	k?
E	E	kA
669.0	669.0	k4
•	•	k?
E	E	kA
669.1	669.1	k4
•	•	k?
E	E	kA
669.2	669.2	k4
•	•	k?
E	E	kA
669.3	669.3	k4
•	•	k?
E	E	kA
699.0	699.0	k4
♦	♦	k?
Bez	bez	k7c2
Kryšpínova	Kryšpínův	k2eAgNnSc2d1
označení	označení	k1gNnSc2
<g/>
:	:	kIx,
162	#num#	k4
El.	El.	k1gFnPc4
lokomotivy	lokomotiva	k1gFnSc2
střídavé	střídavý	k2eAgFnSc2d1
</s>
<s>
S	s	k7c7
458.0	458.0	k4
•	•	k?
S	s	k7c7
479.0	479.0	k4
(	(	kIx(
<g/>
E	E	kA
479.0	479.0	k4
<g/>
)	)	kIx)
•	•	k?
S	s	k7c7
479.1	479.1	k4
(	(	kIx(
<g/>
E	E	kA
479.1	479.1	k4
<g/>
)	)	kIx)
•	•	k?
S	s	k7c7
489.0	489.0	k4
•	•	k?
S	s	k7c7
499.0	499.0	k4
•	•	k?
S	s	k7c7
499.02	499.02	k4
•	•	k?
S	s	k7c7
499.1	499.1	k4
•	•	k?
S	s	k7c7
499.2	499.2	k4
•	•	k?
S	s	k7c7
699.0	699.0	k4
•	•	k?
S	s	k7c7
699.1	699.1	k4
♦	♦	k?
Bez	bez	k7c2
Kryšpínova	Kryšpínův	k2eAgNnSc2d1
označení	označení	k1gNnSc2
<g/>
:	:	kIx,
209	#num#	k4
El.	El.	k1gFnSc2
lokomotivy	lokomotiva	k1gFnSc2
vícesystémové	vícesystémová	k1gFnSc2
</s>
<s>
ES	ES	kA
499.0	499.0	k4
•	•	k?
ES	ES	kA
499.1	499.1	k4
•	•	k?
ES	ES	kA
499.2	499.2	k4
♦	♦	k?
Bez	bez	k7c2
Kryšpínova	Kryšpínův	k2eAgNnSc2d1
označení	označení	k1gNnSc2
<g/>
:	:	kIx,
362	#num#	k4
El.	El.	k1gFnPc4
lokomotivy	lokomotiva	k1gFnSc2
akumulátorové	akumulátorový	k2eAgFnSc2d1
</s>
<s>
E	E	kA
203.0	203.0	k4
•	•	k?
E	E	kA
407.0	407.0	k4
•	•	k?
E	E	kA
417.0	417.0	k4
El.	El.	k1gFnSc2
vozy	vůz	k1gInPc1
a	a	k8xC
jednotky	jednotka	k1gFnPc1
stejnosměrné	stejnosměrný	k2eAgFnPc1d1
</s>
<s>
M	M	kA
200.0	200.0	k4
•	•	k?
M	M	kA
200.1	200.1	k4
•	•	k?
M	M	kA
201.0	201.0	k4
•	•	k?
EM	Ema	k1gFnPc2
400.0	400.0	k4
(	(	kIx(
<g/>
M	M	kA
400.0	400.0	k4
<g/>
)	)	kIx)
•	•	k?
EM	Ema	k1gFnPc2
410.0	410.0	k4
(	(	kIx(
<g/>
M	M	kA
410.0	410.0	k4
<g/>
,	,	kIx,
E	E	kA
410.0	410.0	k4
<g/>
)	)	kIx)
•	•	k?
EM	Ema	k1gFnPc2
411.0	411.0	k4
•	•	k?
EM	Ema	k1gFnPc2
475.0	475.0	k4
•	•	k?
EM	Ema	k1gFnPc2
475.1	475.1	k4
•	•	k?
EM	Ema	k1gFnPc2
475.2	475.2	k4
•	•	k?
EM	Ema	k1gFnPc2
488.0	488.0	k4
♦	♦	k?
Bez	bez	k7c2
Kryšpínova	Kryšpínův	k2eAgNnSc2d1
označení	označení	k1gNnSc2
<g/>
:	:	kIx,
470	#num#	k4
Úzkorozchodné	úzkorozchodný	k2eAgFnSc2d1
</s>
<s>
E	E	kA
10.0	10.0	k4
•	•	k?
E	E	kA
200.0	200.0	k4
•	•	k?
M	M	kA
24.0	24.0	k4
•	•	k?
EMU	Ema	k1gFnSc4
25.0	25.0	k4
(	(	kIx(
<g/>
M	M	kA
25.0	25.0	k4
<g/>
)	)	kIx)
•	•	k?
EMU	Ema	k1gFnSc4
26.0	26.0	k4
•	•	k?
EMU	Ema	k1gFnSc4
28.0	28.0	k4
•	•	k?
EMU	Ema	k1gFnSc4
29.0	29.0	k4
•	•	k?
EMU	Ema	k1gFnSc4
46.0	46.0	k4
•	•	k?
EMU	Ema	k1gFnSc4
46.1	46.1	k4
•	•	k?
EMU	Ema	k1gFnSc4
48.0	48.0	k4
•	•	k?
EMU	Ema	k1gFnSc4
49.0	49.0	k4
•	•	k?
EMU	Ema	k1gFnSc4
89.0	89.0	k4
</s>
<s>
El.	El.	k?
jednotky	jednotka	k1gFnSc2
střídavé	střídavý	k2eAgFnSc2d1
</s>
<s>
SM	SM	kA
488.0	488.0	k4
Motorové	motorový	k2eAgFnSc2d1
lokomotivy	lokomotiva	k1gFnSc2
</s>
<s>
T	T	kA
200.0	200.0	k4
•	•	k?
T	T	kA
200.2	200.2	k4
•	•	k?
T	T	kA
200.3	200.3	k4
•	•	k?
T	T	kA
201.9	201.9	k4
•	•	k?
T	T	kA
203.05	203.05	k4
•	•	k?
T	T	kA
211.0	211.0	k4
•	•	k?
T	T	kA
211.1	211.1	k4
•	•	k?
T	T	kA
212.0	212.0	k4
•	•	k?
T	T	kA
212.1	212.1	k4
•	•	k?
T	T	kA
220.0	220.0	k4
•	•	k?
T	T	kA
234.0	234.0	k4
•	•	k?
T	T	kA
237.0	237.0	k4
•	•	k?
T	T	kA
238.0	238.0	k4
•	•	k?
T	T	kA
304.0	304.0	k4
•	•	k?
T	T	kA
306.0	306.0	k4
•	•	k?
T	T	kA
324.0	324.0	k4
•	•	k?
T	T	kA
333.0	333.0	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
T	T	kA
334.0	334.0	k4
•	•	k?
T	T	kA
334.004	334.004	k4
•	•	k?
T	T	kA
335.0	335.0	k4
•	•	k?
T	T	kA
419.0	419.0	k4
•	•	k?
T	T	kA
419.1	419.1	k4
•	•	k?
T	T	kA
426.0	426.0	k4
•	•	k?
T	T	kA
434.0	434.0	k4
•	•	k?
T	T	kA
435.0	435.0	k4
•	•	k?
T	T	kA
436.0	436.0	k4
•	•	k?
T	T	kA
436.15	436.15	k4
•	•	k?
T	T	kA
444.0	444.0	k4
•	•	k?
T	T	kA
444.1	444.1	k4
•	•	k?
T	T	kA
448.0	448.0	k4
•	•	k?
T	T	kA
449.0	449.0	k4
•	•	k?
T	T	kA
450.0	450.0	k4
(	(	kIx(
<g/>
M	M	kA
450.0	450.0	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
T	T	kA
455.0	455.0	k4
•	•	k?
T	T	kA
457.0	457.0	k4
•	•	k?
T	T	kA
457.1	457.1	k4
•	•	k?
T	T	kA
458.0	458.0	k4
•	•	k?
T	T	kA
458.1	458.1	k4
•	•	k?
T	T	kA
458.5	458.5	k4
•	•	k?
T	T	kA
465.0	465.0	k4
•	•	k?
T	T	kA
466.0	466.0	k4
•	•	k?
T	T	kA
466.1	466.1	k4
•	•	k?
T	T	kA
466.2	466.2	k4
•	•	k?
T	T	kA
466.3	466.3	k4
•	•	k?
T	T	kA
469.0	469.0	k4
•	•	k?
T	T	kA
475.0	475.0	k4
•	•	k?
T	T	kA
475.15	475.15	k4
•	•	k?
T	T	kA
476.0	476.0	k4
•	•	k?
T	T	kA
476.05	476.05	k4
•	•	k?
T	T	kA
<g />
.	.	kIx.
</s>
<s hack="1">
476.1	476.1	k4
(	(	kIx(
<g/>
T	T	kA
476.0	476.0	k4
<g/>
)	)	kIx)
•	•	k?
T	T	kA
478.0	478.0	k4
•	•	k?
T	T	kA
478.1	478.1	k4
•	•	k?
T	T	kA
478.2	478.2	k4
<g/>
I	i	k8xC
•	•	k?
T	T	kA
478.2	478.2	k4
<g/>
II	II	kA
•	•	k?
T	T	kA
478.3	478.3	k4
•	•	k?
T	T	kA
478.4	478.4	k4
•	•	k?
T	T	kA
478.45	478.45	k4
(	(	kIx(
<g/>
T	T	kA
478.2	478.2	k4
<g/>
,	,	kIx,
T	T	kA
478.4	478.4	k4
<g/>
)	)	kIx)
•	•	k?
T	T	kA
499.0	499.0	k4
•	•	k?
T	T	kA
669.0	669.0	k4
•	•	k?
T	T	kA
669.1	669.1	k4
•	•	k?
T	T	kA
669.2	669.2	k4
•	•	k?
T	T	kA
669.50	669.50	k4
•	•	k?
T	T	kA
669.51	669.51	k4
•	•	k?
T	T	kA
678.0	678.0	k4
•	•	k?
T	T	kA
679.0	679.0	k4
•	•	k?
T	T	kA
679.1	679.1	k4
•	•	k?
T	T	kA
679.2	679.2	k4
•	•	k?
T	T	kA
679.5	679.5	k4
♦	♦	k?
Bez	bez	k7c2
Kryšpínova	Kryšpínův	k2eAgNnSc2d1
označení	označení	k1gNnSc2
<g/>
:	:	kIx,
750	#num#	k4
Úzkorozchodné	úzkorozchodný	k2eAgFnSc2d1
</s>
<s>
TU	tu	k6eAd1
29.0	29.0	k4
•	•	k?
TU	tu	k9
29.1	29.1	k4
•	•	k?
TU	tu	k9
29.2	29.2	k4
•	•	k?
T	T	kA
36.0	36.0	k4
•	•	k?
TU	tu	k9
46.0	46.0	k4
•	•	k?
TU	tu	k9
47.0	47.0	k4
</s>
<s>
Hybridní	hybridní	k2eAgFnPc1d1
a	a	k8xC
jiné	jiný	k2eAgFnPc1d1
lokomotivy	lokomotiva	k1gFnPc1
</s>
<s>
TA	ten	k3xDgFnSc1
436.05	436.05	k4
(	(	kIx(
<g/>
ET	ET	kA
459.0	459.0	k4
<g/>
)	)	kIx)
•	•	k?
TL	TL	kA
659.0	659.0	k4
Motorové	motorový	k2eAgInPc1d1
vozy	vůz	k1gInPc1
a	a	k8xC
jednotky	jednotka	k1gFnPc1
</s>
<s>
M	M	kA
120.0	120.0	k4
•	•	k?
M	M	kA
120.1	120.1	k4
•	•	k?
M	M	kA
120.2	120.2	k4
•	•	k?
M	M	kA
120.3	120.3	k4
•	•	k?
M	M	kA
120.4	120.4	k4
•	•	k?
M	M	kA
120.5	120.5	k4
•	•	k?
M	M	kA
122.0	122.0	k4
•	•	k?
M	M	kA
130.0	130.0	k4
•	•	k?
M	M	kA
130.1	130.1	k4
•	•	k?
M	M	kA
130.2	130.2	k4
•	•	k?
M	M	kA
130.3	130.3	k4
•	•	k?
M	M	kA
130.4	130.4	k4
•	•	k?
M	M	kA
130.7	130.7	k4
•	•	k?
M	M	kA
131.0	131.0	k4
•	•	k?
M	M	kA
131.1	131.1	k4
•	•	k?
M	M	kA
131.2	131.2	k4
•	•	k?
M	M	kA
131.3	131.3	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
M	M	kA
131.5	131.5	k4
•	•	k?
M	M	kA
131.52	131.52	k4
•	•	k?
M	M	kA
131.6	131.6	k4
•	•	k?
M	M	kA
132.0	132.0	k4
•	•	k?
M	M	kA
133.0	133.0	k4
•	•	k?
M	M	kA
134.0	134.0	k4
•	•	k?
M	M	kA
140.0	140.0	k4
•	•	k?
M	M	kA
140.1	140.1	k4
•	•	k?
M	M	kA
140.2	140.2	k4
•	•	k?
M	M	kA
140.3	140.3	k4
•	•	k?
M	M	kA
140.4	140.4	k4
•	•	k?
M	M	kA
140.9	140.9	k4
(	(	kIx(
<g/>
M	M	kA
140.4	140.4	k4
<g/>
)	)	kIx)
•	•	k?
M	M	kA
144.0	144.0	k4
•	•	k?
M	M	kA
150.0	150.0	k4
•	•	k?
M	M	kA
150.1	150.1	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
M	M	kA
152.0	152.0	k4
(	(	kIx(
<g/>
M	M	kA
151.0	151.0	k4
<g/>
)	)	kIx)
•	•	k?
M	M	kA
152.5	152.5	k4
•	•	k?
M	M	kA
153.0	153.0	k4
•	•	k?
M	M	kA
153.5	153.5	k4
•	•	k?
M	M	kA
210.0	210.0	k4
•	•	k?
M	M	kA
220.1	220.1	k4
•	•	k?
M	M	kA
220.2	220.2	k4
•	•	k?
M	M	kA
220.3	220.3	k4
•	•	k?
M	M	kA
221.0	221.0	k4
•	•	k?
M	M	kA
221.1	221.1	k4
•	•	k?
M	M	kA
221.2	221.2	k4
•	•	k?
M	M	kA
221.3	221.3	k4
•	•	k?
M	M	kA
222.0	222.0	k4
•	•	k?
M	M	kA
230.0	230.0	k4
•	•	k?
M	M	kA
<g />
.	.	kIx.
</s>
<s hack="1">
230.1	230.1	k4
•	•	k?
M	M	kA
231.0	231.0	k4
•	•	k?
M	M	kA
231.1	231.1	k4
•	•	k?
M	M	kA
232.0	232.0	k4
•	•	k?
M	M	kA
232.1	232.1	k4
•	•	k?
M	M	kA
232.2	232.2	k4
•	•	k?
M	M	kA
232.9	232.9	k4
•	•	k?
M	M	kA
234.0	234.0	k4
•	•	k?
M	M	kA
240.0	240.0	k4
(	(	kIx(
<g/>
M	M	kA
230.5	230.5	k4
<g/>
)	)	kIx)
•	•	k?
M	M	kA
242.0	242.0	k4
•	•	k?
M	M	kA
244.0	244.0	k4
•	•	k?
M	M	kA
250.0	250.0	k4
•	•	k?
M	M	kA
251.0	251.0	k4
•	•	k?
M	M	kA
251.1	251.1	k4
•	•	k?
M	M	kA
251.2	251.2	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
M	M	kA
260.0	260.0	k4
•	•	k?
M	M	kA
261.0	261.0	k4
•	•	k?
M	M	kA
262.0	262.0	k4
•	•	k?
M	M	kA
262.1	262.1	k4
•	•	k?
M	M	kA
263.0	263.0	k4
•	•	k?
M	M	kA
264.0	264.0	k4
•	•	k?
M	M	kA
273.0	273.0	k4
•	•	k?
M	M	kA
274.0	274.0	k4
•	•	k?
M	M	kA
275.0	275.0	k4
•	•	k?
M	M	kA
275.1	275.1	k4
•	•	k?
M	M	kA
283.0	283.0	k4
•	•	k?
M	M	kA
283.1	283.1	k4
•	•	k?
M	M	kA
284.0	284.0	k4
•	•	k?
M	M	kA
284.1	284.1	k4
•	•	k?
M	M	kA
286.0	286.0	k4
(	(	kIx(
<g/>
M	M	kA
283.1	283.1	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
M	M	kA
286.1	286.1	k4
•	•	k?
M	M	kA
290.0	290.0	k4
•	•	k?
M	M	kA
295.0	295.0	k4
(	(	kIx(
<g/>
M	M	kA
495.0	495.0	k4
<g/>
)	)	kIx)
•	•	k?
M	M	kA
295.1	295.1	k4
(	(	kIx(
<g/>
M	M	kA
495.1	495.1	k4
<g/>
)	)	kIx)
•	•	k?
M	M	kA
295.2	295.2	k4
(	(	kIx(
<g/>
M	M	kA
495.2	495.2	k4
<g/>
)	)	kIx)
•	•	k?
M	M	kA
296.0	296.0	k4
•	•	k?
M	M	kA
296.1	296.1	k4
•	•	k?
M	M	kA
296.2	296.2	k4
•	•	k?
M	M	kA
297.0	297.0	k4
•	•	k?
M	M	kA
298.0	298.0	k4
(	(	kIx(
<g/>
M	M	kA
498.0	498.0	k4
<g/>
)	)	kIx)
•	•	k?
M	M	kA
475.0	475.0	k4
(	(	kIx(
<g/>
M	M	kA
474.0	474.0	k4
<g/>
)	)	kIx)
•	•	k?
M	M	kA
485.0	485.0	k4
•	•	k?
M	M	kA
493.0	493.0	k4
•	•	k?
M	M	kA
494.0	494.0	k4
♦	♦	k?
Bez	bez	k7c2
Kryšpínova	Kryšpínův	k2eAgNnSc2d1
označení	označení	k1gNnSc2
<g/>
:	:	kIx,
842	#num#	k4
Úzkorozchodné	úzkorozchodný	k2eAgFnSc2d1
</s>
<s>
M	M	kA
10.0	10.0	k4
•	•	k?
M	M	kA
11.0	11.0	k4
•	•	k?
M	M	kA
21.0	21.0	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Železnice	železnice	k1gFnSc1
</s>
