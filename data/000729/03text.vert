<s>
Saimaa	Saimaa	k1gFnSc1	Saimaa
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
Saimen	Saimen	k2eAgInSc1d1	Saimen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mělké	mělký	k2eAgNnSc1d1	mělké
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
hloubka	hloubka	k1gFnSc1	hloubka
do	do	k7c2	do
82	[number]	k4	82
m	m	kA	m
<g/>
)	)	kIx)	)
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Finské	finský	k2eAgFnSc2d1	finská
jezerní	jezerní	k2eAgFnSc2d1	jezerní
kotliny	kotlina	k1gFnSc2	kotlina
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
76	[number]	k4	76
m	m	kA	m
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
soustavy	soustava	k1gFnSc2	soustava
spojených	spojený	k2eAgNnPc2d1	spojené
jezer	jezero	k1gNnPc2	jezero
je	být	k5eAaImIp3nS	být
4377	[number]	k4	4377
km2	km2	k4	km2
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ho	on	k3xPp3gMnSc4	on
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
po	po	k7c6	po
ruských	ruský	k2eAgNnPc6d1	ruské
jezerech	jezero	k1gNnPc6	jezero
Ladožském	ladožský	k2eAgMnSc6d1	ladožský
<g/>
,	,	kIx,	,
Oněžském	oněžský	k2eAgMnSc6d1	oněžský
a	a	k8xC	a
švédském	švédský	k2eAgNnSc6d1	švédské
jezeru	jezero	k1gNnSc6	jezero
Vänern	Vänerna	k1gFnPc2	Vänerna
<g/>
.	.	kIx.	.
</s>
<s>
Kotliny	kotlina	k1gFnPc1	kotlina
jezer	jezero	k1gNnPc2	jezero
jsou	být	k5eAaImIp3nP	být
ledovcového	ledovcový	k2eAgInSc2d1	ledovcový
původu	původ	k1gInSc2	původ
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
a	a	k8xC	a
uhlazené	uhlazený	k2eAgNnSc1d1	uhlazené
krycím	krycí	k2eAgInSc7d1	krycí
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
vlastního	vlastní	k2eAgNnSc2d1	vlastní
jezera	jezero	k1gNnSc2	jezero
Saimaa	Saima	k1gInSc2	Saima
(	(	kIx(	(
<g/>
1377	[number]	k4	1377
km2	km2	k4	km2
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
přirozeně	přirozeně	k6eAd1	přirozeně
napojených	napojený	k2eAgNnPc2d1	napojené
jezer	jezero	k1gNnPc2	jezero
Pihlajavesi	Pihlajavese	k1gFnSc6	Pihlajavese
(	(	kIx(	(
<g/>
713	[number]	k4	713
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Haukivesi	Haukivese	k1gFnSc4	Haukivese
(	(	kIx(	(
<g/>
620	[number]	k4	620
km2	km2	k4	km2
<g/>
,	,	kIx,	,
hloubka	hloubka	k1gFnSc1	hloubka
58	[number]	k4	58
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Orivesi	Orivese	k1gFnSc4	Orivese
(	(	kIx(	(
<g/>
601	[number]	k4	601
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Puruvesi	Puruvese	k1gFnSc4	Puruvese
(	(	kIx(	(
<g/>
421	[number]	k4	421
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pyhäselkä	Pyhäselkä	k1gMnSc1	Pyhäselkä
(	(	kIx(	(
<g/>
361	[number]	k4	361
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Enonvesi	Enonvese	k1gFnSc4	Enonvese
(	(	kIx(	(
<g/>
196	[number]	k4	196
km2	km2	k4	km2
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
menších	malý	k2eAgMnPc2d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
členění	členění	k1gNnSc1	členění
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
zdrojích	zdroj	k1gInPc6	zdroj
lišit	lišit	k5eAaImF	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
členité	členitý	k2eAgNnSc1d1	členité
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
skalnaté	skalnatý	k2eAgInPc1d1	skalnatý
většinou	většinou	k6eAd1	většinou
pokryté	pokrytý	k2eAgInPc1d1	pokrytý
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Jezerní	jezerní	k2eAgFnSc1d1	jezerní
soustava	soustava	k1gFnSc1	soustava
má	mít	k5eAaImIp3nS	mít
velice	velice	k6eAd1	velice
nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
roztříštěna	roztříštit	k5eAaPmNgFnS	roztříštit
četnými	četný	k2eAgInPc7d1	četný
ostrovy	ostrov	k1gInPc7	ostrov
a	a	k8xC	a
ostrůvky	ostrůvek	k1gInPc7	ostrůvek
<g/>
.	.	kIx.	.
</s>
<s>
Břeh	břeh	k1gInSc1	břeh
měří	měřit	k5eAaImIp3nS	měřit
celkem	celkem	k6eAd1	celkem
15	[number]	k4	15
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
asi	asi	k9	asi
12	[number]	k4	12
938	[number]	k4	938
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
4131	[number]	k4	4131
na	na	k7c6	na
vlastním	vlastní	k2eAgNnSc6d1	vlastní
jezeře	jezero	k1gNnSc6	jezero
Saimaa	Saimaa	k1gMnSc1	Saimaa
<g/>
,	,	kIx,	,
3819	[number]	k4	3819
na	na	k7c6	na
Pihlajavesi	Pihlajavese	k1gFnSc6	Pihlajavese
<g/>
,	,	kIx,	,
2158	[number]	k4	2158
na	na	k7c6	na
Haukivesi	Haukivese	k1gFnSc6	Haukivese
<g/>
,	,	kIx,	,
1495	[number]	k4	1495
na	na	k7c6	na
Orivesi	Orivese	k1gFnSc6	Orivese
<g/>
,	,	kIx,	,
720	[number]	k4	720
na	na	k7c4	na
Puruvesi	Puruvese	k1gFnSc4	Puruvese
a	a	k8xC	a
615	[number]	k4	615
na	na	k7c6	na
Pyhäselkä	Pyhäselkä	k1gFnSc6	Pyhäselkä
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
ostrovem	ostrov	k1gInSc7	ostrov
je	být	k5eAaImIp3nS	být
Sääminginsalo	Sääminginsalo	k1gFnSc1	Sääminginsalo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
části	část	k1gFnSc3	část
Pihlajavesi	Pihlajavese	k1gFnSc4	Pihlajavese
<g/>
,	,	kIx,	,
Haukivesi	Haukivese	k1gFnSc4	Haukivese
a	a	k8xC	a
Puruvesi	Puruvese	k1gFnSc4	Puruvese
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
1	[number]	k4	1
069	[number]	k4	069
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
po	po	k7c6	po
ostrově	ostrov	k1gInSc6	ostrov
Soisalo	Soisalo	k1gFnSc2	Soisalo
na	na	k7c6	na
jezerní	jezerní	k2eAgFnSc6d1	jezerní
soustavě	soustava	k1gFnSc6	soustava
Iso-Kalla	Iso-Kallo	k1gNnSc2	Iso-Kallo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
Äitsaari	Äitsaar	k1gFnSc2	Äitsaar
(	(	kIx(	(
<g/>
74	[number]	k4	74
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moinsalmensaari	Moinsalmensaari	k1gNnSc1	Moinsalmensaari
(	(	kIx(	(
<g/>
53	[number]	k4	53
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Oravisalo	Oravisalo	k1gMnSc1	Oravisalo
(	(	kIx(	(
<g/>
49	[number]	k4	49
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kirkkosaari	Kirkkosaari	k1gNnSc1	Kirkkosaari
(	(	kIx(	(
<g/>
47	[number]	k4	47
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kuivainen	Kuivainen	k1gInSc1	Kuivainen
(	(	kIx(	(
<g/>
33	[number]	k4	33
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Varpasalo	Varpasat	k5eAaPmAgNnS	Varpasat
(	(	kIx(	(
<g/>
27	[number]	k4	27
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Salosaari	Salosaari	k1gNnSc1	Salosaari
(	(	kIx(	(
<g/>
26	[number]	k4	26
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kyläniemi	Kylänie	k1gFnPc7	Kylänie
(	(	kIx(	(
<g/>
23	[number]	k4	23
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Niinikkosaari	Niinikkosaari	k1gNnSc1	Niinikkosaari
(	(	kIx(	(
<g/>
18	[number]	k4	18
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mitinsaari	Mitinsaari	k1gNnSc1	Mitinsaari
(	(	kIx(	(
<g/>
18	[number]	k4	18
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pellosalo	Pellosalo	k1gMnSc1	Pellosalo
(	(	kIx(	(
<g/>
16	[number]	k4	16
km2	km2	k4	km2
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Härskiänsaari	Härskiänsaari	k1gNnSc1	Härskiänsaari
(	(	kIx(	(
<g/>
15	[number]	k4	15
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Otasalo	Otasalo	k1gMnSc1	Otasalo
(	(	kIx(	(
<g/>
15	[number]	k4	15
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kongonsaari	Kongonsaari	k1gNnSc1	Kongonsaari
(	(	kIx(	(
<g/>
13	[number]	k4	13
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ahvionsaari	Ahvionsaari	k1gNnSc1	Ahvionsaari
(	(	kIx(	(
<g/>
13	[number]	k4	13
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Saukonsaari	Saukonsaari	k1gNnSc1	Saukonsaari
(	(	kIx(	(
<g/>
11	[number]	k4	11
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tuohisaari	Tuohisaare	k1gFnSc4	Tuohisaare
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
11	[number]	k4	11
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Liiansaari	Liiansaari	k1gNnSc1	Liiansaari
(	(	kIx(	(
<g/>
11	[number]	k4	11
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Suuri	Suuri	k1gNnSc1	Suuri
Jänkäsalo	Jänkäsalo	k1gFnPc2	Jänkäsalo
(	(	kIx(	(
<g/>
11	[number]	k4	11
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hälvä	Hälvä	k1gMnSc1	Hälvä
(	(	kIx(	(
<g/>
11	[number]	k4	11
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vaahersalo	Vaahersalo	k1gMnSc1	Vaahersalo
(	(	kIx(	(
<g/>
10	[number]	k4	10
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Laukansaari	Laukansaari	k1gNnSc1	Laukansaari
(	(	kIx(	(
<g/>
9	[number]	k4	9
km2	km2	k4	km2
<g/>
)	)	kIx)	)
a	a	k8xC	a
Laukansaari	Laukansaare	k1gFnSc4	Laukansaare
(	(	kIx(	(
<g/>
11	[number]	k4	11
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
představuje	představovat	k5eAaImIp3nS	představovat
důležitou	důležitý	k2eAgFnSc4d1	důležitá
křižovatku	křižovatka	k1gFnSc4	křižovatka
vodních	vodní	k2eAgFnPc2d1	vodní
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
části	část	k1gFnSc2	část
Orivesi	Orivese	k1gFnSc4	Orivese
přitéká	přitékat	k5eAaImIp3nS	přitékat
řeka	řeka	k1gFnSc1	řeka
Viinijoki	Viinijok	k1gFnSc2	Viinijok
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
Viinijärvi	Viinijärev	k1gFnSc6	Viinijärev
a	a	k8xC	a
do	do	k7c2	do
části	část	k1gFnSc2	část
Pyhäselkä	Pyhäselkä	k1gMnSc2	Pyhäselkä
řeka	řek	k1gMnSc2	řek
Pielisjoki	Pielisjoki	k1gNnSc1	Pielisjoki
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
Pielinen	Pielinna	k1gFnPc2	Pielinna
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
povodí	povodí	k1gNnSc2	povodí
je	být	k5eAaImIp3nS	být
69,5	[number]	k4	69,5
tisíce	tisíc	k4xCgInPc4	tisíc
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Vytéká	vytékat	k5eAaImIp3nS	vytékat
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
široká	široký	k2eAgFnSc1d1	široká
řeka	řeka	k1gFnSc1	řeka
Vuoksi	Vuokse	k1gFnSc4	Vuokse
tekoucí	tekoucí	k2eAgFnSc4d1	tekoucí
do	do	k7c2	do
Ladožského	ladožský	k2eAgNnSc2d1	Ladožské
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
umělých	umělý	k2eAgInPc2d1	umělý
kanálů	kanál	k1gInPc2	kanál
(	(	kIx(	(
<g/>
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
je	být	k5eAaImIp3nS	být
průplav	průplav	k1gInSc1	průplav
Saimaa	Saima	k1gInSc2	Saima
(	(	kIx(	(
<g/>
Saimaan	Saimaan	k1gInSc1	Saimaan
kanava	kanava	k1gFnSc1	kanava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jezero	jezero	k1gNnSc4	jezero
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
Vyborským	Vyborský	k2eAgInSc7d1	Vyborský
zálivem	záliv	k1gInSc7	záliv
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
Finského	finský	k2eAgInSc2d1	finský
zálivu	záliv	k1gInSc2	záliv
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
)	)	kIx)	)
a	a	k8xC	a
řadou	řada	k1gFnSc7	řada
dalších	další	k2eAgNnPc2d1	další
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hladina	hladina	k1gFnSc1	hladina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
nejnižší	nízký	k2eAgMnSc1d3	nejnižší
brzo	brzo	k6eAd1	brzo
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
pokryté	pokrytý	k2eAgNnSc1d1	pokryté
ledem	led	k1gInSc7	led
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Členité	členitý	k2eAgFnPc1d1	členitá
mělké	mělký	k2eAgFnPc1d1	mělká
vody	voda	k1gFnPc1	voda
jsou	být	k5eAaImIp3nP	být
rájem	ráj	k1gInSc7	ráj
pro	pro	k7c4	pro
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgNnSc4d1	vodní
ptactvo	ptactvo	k1gNnSc4	ptactvo
a	a	k8xC	a
(	(	kIx(	(
<g/>
pochopitelně	pochopitelně	k6eAd1	pochopitelně
<g/>
)	)	kIx)	)
také	také	k9	také
pro	pro	k7c4	pro
rybáře	rybář	k1gMnSc4	rybář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
jezerního	jezerní	k2eAgInSc2d1	jezerní
systému	systém	k1gInSc2	systém
byl	být	k5eAaImAgInS	být
vytyčen	vytyčen	k2eAgInSc1d1	vytyčen
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Linnansaari	Linnansaar	k1gMnSc3	Linnansaar
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
má	mít	k5eAaImIp3nS	mít
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgNnSc1d1	jiné
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
zdejšího	zdejší	k2eAgInSc2d1	zdejší
sladkovodního	sladkovodní	k2eAgInSc2d1	sladkovodní
poddruhu	poddruh	k1gInSc2	poddruh
tuleně	tuleň	k1gMnSc2	tuleň
kroužkového	kroužkový	k2eAgMnSc2d1	kroužkový
(	(	kIx(	(
<g/>
Phoca	Phocus	k1gMnSc2	Phocus
hispida	hispid	k1gMnSc2	hispid
ssp	ssp	k?	ssp
<g/>
.	.	kIx.	.
saimensis	saimensis	k1gFnSc1	saimensis
-	-	kIx~	-
tuleň	tuleň	k1gMnSc1	tuleň
kroužkový	kroužkový	k2eAgMnSc1d1	kroužkový
saimaaský	saimaaský	k1gMnSc1	saimaaský
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
jezera	jezero	k1gNnSc2	jezero
leží	ležet	k5eAaImIp3nS	ležet
celá	celý	k2eAgFnSc1d1	celá
řad	řada	k1gFnPc2	řada
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
jsou	být	k5eAaImIp3nP	být
Lappeenranta	Lappeenrant	k1gMnSc4	Lappeenrant
<g/>
,	,	kIx,	,
Imatra	Imatr	k1gMnSc4	Imatr
<g/>
,	,	kIx,	,
Joensuu	Joensua	k1gMnSc4	Joensua
<g/>
,	,	kIx,	,
Savonlinna	Savonlinn	k1gMnSc2	Savonlinn
a	a	k8xC	a
Mikkeli	Mikkel	k1gInPc7	Mikkel
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Saimaa	Saima	k1gInSc2	Saima
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
SAIMAA	SAIMAA	kA	SAIMAA
-	-	kIx~	-
Finland	Finland	k1gInSc1	Finland
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
largest	largest	k1gInSc1	largest
lake	lake	k1gNnSc3	lake
</s>
