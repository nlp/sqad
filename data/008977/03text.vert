<p>
<s>
Kašubština	kašubština	k1gFnSc1	kašubština
je	být	k5eAaImIp3nS	být
západoslovanský	západoslovanský	k2eAgInSc4d1	západoslovanský
jazyk	jazyk	k1gInSc4	jazyk
podobný	podobný	k2eAgInSc4d1	podobný
polštině	polština	k1gFnSc3	polština
a	a	k8xC	a
vymřelé	vymřelý	k2eAgFnSc3d1	vymřelá
polabštině	polabština	k1gFnSc3	polabština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
jí	on	k3xPp3gFnSc3	on
Kašubové	Kašub	k1gMnPc1	Kašub
kolem	kolem	k7c2	kolem
Gdaňsku	Gdaňsk	k1gInSc2	Gdaňsk
(	(	kIx(	(
<g/>
kašubsky	kašubsky	k6eAd1	kašubsky
Gduńsk	Gduńsk	k1gInSc1	Gduńsk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Kašubsku	Kašubsek	k1gInSc6	Kašubsek
(	(	kIx(	(
<g/>
kašubsky	kašubsky	k6eAd1	kašubsky
Kaszëbë	Kaszëbë	k1gMnSc1	Kaszëbë
či	či	k8xC	či
Kaszëbskô	Kaszëbskô	k1gMnSc1	Kaszëbskô
<g/>
)	)	kIx)	)
cca	cca	kA	cca
108	[number]	k4	108
tisíc	tisíc	k4xCgInSc1	tisíc
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
složitý	složitý	k2eAgInSc1d1	složitý
systém	systém	k1gInSc1	systém
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
sykavky	sykavka	k1gFnSc2	sykavka
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
Z	Z	kA	Z
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
DZ	DZ	kA	DZ
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
jednodušší	jednoduchý	k2eAgMnSc1d2	jednodušší
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
i	i	k9	i
Ř	Ř	kA	Ř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
polštiny	polština	k1gFnSc2	polština
se	se	k3xPyFc4	se
výslovnost	výslovnost	k1gFnSc1	výslovnost
této	tento	k3xDgFnSc2	tento
hlásky	hláska	k1gFnSc2	hláska
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
kašubštině	kašubština	k1gFnSc6	kašubština
přidělěn	přidělěn	k2eAgInSc4d1	přidělěn
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
kód	kód	k1gInSc4	kód
CSB	CSB	kA	CSB
podle	podle	k7c2	podle
normy	norma	k1gFnSc2	norma
ISO	ISO	kA	ISO
639	[number]	k4	639
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1402	[number]	k4	1402
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
kašubské	kašubský	k2eAgFnPc1d1	kašubská
glosy	glosa	k1gFnPc1	glosa
v	v	k7c6	v
Dutkach	Dutka	k1gFnPc6	Dutka
ziemskych	ziemskycha	k1gFnPc2	ziemskycha
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
tištěnou	tištěný	k2eAgFnSc7d1	tištěná
kašubskou	kašubský	k2eAgFnSc7d1	kašubská
knihou	kniha	k1gFnSc7	kniha
jsou	být	k5eAaImIp3nP	být
Duchovní	duchovní	k2eAgFnPc1d1	duchovní
písně	píseň	k1gFnPc1	píseň
Martina	Martin	k1gMnSc2	Martin
Luthera	Luther	k1gMnSc2	Luther
(	(	kIx(	(
<g/>
Duchowne	Duchown	k1gInSc5	Duchown
piesnie	piesnie	k1gFnSc2	piesnie
D.	D.	kA	D.
Marcina	Marcin	k1gMnSc2	Marcin
Luthera	Luther	k1gMnSc2	Luther
y	y	k?	y
ynszich	ynszich	k1gMnSc1	ynszich
naboznich	naboznich	k1gMnSc1	naboznich
męzow	męzow	k?	męzow
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1586	[number]	k4	1586
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1643	[number]	k4	1643
vyšel	vyjít	k5eAaPmAgInS	vyjít
kašubský	kašubský	k2eAgInSc1d1	kašubský
překlad	překlad	k1gInSc1	překlad
Malého	Malého	k2eAgInSc2d1	Malého
Katechismu	katechismus	k1gInSc2	katechismus
Michałem	Michał	k1gMnSc7	Michał
Mostnikem	Mostnik	k1gMnSc7	Mostnik
(	(	kIx(	(
<g/>
Mały	Mała	k1gMnSc2	Mała
Catechism	Catechism	k1gMnSc1	Catechism
D.	D.	kA	D.
Marciná	Marciná	k1gFnSc1	Marciná
Lutherá	Lutherý	k2eAgFnSc1d1	Lutherý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kašubské	kašubský	k2eAgInPc1d1	kašubský
výrazy	výraz	k1gInPc1	výraz
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
pracích	práce	k1gFnPc6	práce
německého	německý	k2eAgMnSc2d1	německý
sorabisty	sorabista	k1gMnSc2	sorabista
Karla	Karel	k1gMnSc2	Karel
Gottloba	Gottloba	k1gFnSc1	Gottloba
von	von	k1gInSc1	von
Antona	Anton	k1gMnSc2	Anton
(	(	kIx(	(
<g/>
1751	[number]	k4	1751
<g/>
-	-	kIx~	-
<g/>
1818	[number]	k4	1818
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
kašubské	kašubský	k2eAgFnSc2d1	kašubská
literatury	literatura	k1gFnSc2	literatura
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
lékař	lékař	k1gMnSc1	lékař
Florian	Florian	k1gMnSc1	Florian
Ceynowa	Ceynowa	k1gMnSc1	Ceynowa
(	(	kIx(	(
<g/>
1817	[number]	k4	1817
<g/>
-	-	kIx~	-
<g/>
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kašubové	Kašub	k1gMnPc1	Kašub
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
Poláci	Polák	k1gMnPc1	Polák
žijící	žijící	k2eAgMnPc1d1	žijící
na	na	k7c4	na
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
<g/>
)	)	kIx)	)
německém	německý	k2eAgNnSc6d1	německé
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Sepsal	sepsat	k5eAaPmAgMnS	sepsat
mj.	mj.	kA	mj.
první	první	k4xOgFnSc4	první
kašubskou	kašubský	k2eAgFnSc4d1	kašubská
mluvnici	mluvnice	k1gFnSc4	mluvnice
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
a	a	k8xC	a
vydával	vydávat	k5eAaImAgInS	vydávat
i	i	k9	i
kašubský	kašubský	k2eAgInSc1d1	kašubský
časopis	časopis	k1gInSc1	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
básníkem	básník	k1gMnSc7	básník
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
Hieronim	Hieronim	k1gInSc1	Hieronim
(	(	kIx(	(
<g/>
Jarosz	Jarosz	k1gInSc1	Jarosz
<g/>
)	)	kIx)	)
Derdowski	Derdowski	k1gNnSc1	Derdowski
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
eposu	epos	k1gInSc2	epos
O	o	k7c6	o
panu	pan	k1gMnSc6	pan
Čorlinském	Čorlinský	k2eAgNnSc6d1	Čorlinský
(	(	kIx(	(
<g/>
O	o	k7c6	o
Panu	Pan	k1gMnSc6	Pan
Czorlińścim	Czorlińścim	k1gInSc4	Czorlińścim
co	co	k3yRnSc4	co
do	do	k7c2	do
Pucka	pucka	k1gFnSc1	pucka
po	po	k7c6	po
sece	sece	k1gFnSc6	sece
jachoł	jachoł	k?	jachoł
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
<g/>
.	.	kIx.	.
</s>
<s>
Bernard	Bernard	k1gMnSc1	Bernard
Sychta	Sychta	k1gMnSc1	Sychta
byl	být	k5eAaImAgMnS	být
kašubský	kašubský	k2eAgMnSc1d1	kašubský
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
a	a	k8xC	a
etnograf	etnograf	k1gMnSc1	etnograf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
také	také	k9	také
kašubská	kašubský	k2eAgFnSc1d1	kašubská
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kašubštině	kašubština	k1gFnSc6	kašubština
vycházejí	vycházet	k5eAaImIp3nP	vycházet
noviny	novina	k1gFnPc1	novina
<g/>
.	.	kIx.	.
</s>
<s>
Kašubistika	Kašubistika	k1gFnSc1	Kašubistika
je	být	k5eAaImIp3nS	být
filologický	filologický	k2eAgInSc4d1	filologický
obor	obor	k1gInSc4	obor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
literaturou	literatura	k1gFnSc7	literatura
a	a	k8xC	a
jazykem	jazyk	k1gInSc7	jazyk
Kašubů	Kašub	k1gMnPc2	Kašub
<g/>
.	.	kIx.	.
</s>
<s>
Kašubština	kašubština	k1gFnSc1	kašubština
je	být	k5eAaImIp3nS	být
také	také	k9	také
zavedena	zaveden	k2eAgFnSc1d1	zavedena
v	v	k7c6	v
několika	několik	k4yIc6	několik
kostelech	kostel	k1gInPc6	kostel
jako	jako	k8xS	jako
liturgický	liturgický	k2eAgInSc4d1	liturgický
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slovník	slovník	k1gInSc1	slovník
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Abeceda	abeceda	k1gFnSc1	abeceda
==	==	k?	==
</s>
</p>
<p>
<s>
Kašubská	kašubský	k2eAgFnSc1d1	kašubská
abeceda	abeceda	k1gFnSc1	abeceda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
následující	následující	k2eAgInPc4d1	následující
znaky	znak	k1gInPc4	znak
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Čtení	čtení	k1gNnSc3	čtení
v	v	k7c6	v
kašubštině	kašubština	k1gFnSc6	kašubština
==	==	k?	==
</s>
</p>
<p>
<s>
Výslovnost	výslovnost	k1gFnSc1	výslovnost
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
znaků	znak	k1gInPc2	znak
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ã	ã	k?	ã
–	–	k?	–
nosové	nosový	k2eAgFnSc2d1	nosová
a.	a.	k?	a.
</s>
</p>
<p>
<s>
é	é	k0	é
–	–	k?	–
přibližně	přibližně	k6eAd1	přibližně
"	"	kIx"	"
<g/>
yj	yj	k?	yj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ypsilonem	ypsilon	k1gInSc7	ypsilon
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
chápe	chápat	k5eAaImIp3nS	chápat
polské	polský	k2eAgFnPc4d1	polská
y.	y.	k?	y.
</s>
</p>
<p>
<s>
ë	ë	k?	ë
–	–	k?	–
ə	ə	k?	ə
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
odborníky	odborník	k1gMnPc4	odborník
šva	šva	k1gNnSc7	šva
<g/>
,	,	kIx,	,
prostě	prostě	k9	prostě
neurčitý	určitý	k2eNgInSc1d1	neurčitý
temný	temný	k2eAgInSc1d1	temný
zvuk	zvuk	k1gInSc1	zvuk
jako	jako	k8xC	jako
v	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
thE	thE	k?	thE
house	house	k1gNnSc1	house
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ò	ò	k?	ò
–	–	k?	–
ue	ue	k?	ue
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
po	po	k7c6	po
anglicku	anglick	k1gInSc6	anglick
we	we	k?	we
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ô	ô	k?	ô
–	–	k?	–
podle	podle	k7c2	podle
dialektu	dialekt	k1gInSc2	dialekt
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
O	O	kA	O
nebo	nebo	k8xC	nebo
se	s	k7c7	s
zabarvením	zabarvení	k1gNnSc7	zabarvení
do	do	k7c2	do
e.	e.	k?	e.
</s>
</p>
<p>
<s>
ù	ù	k?	ù
–	–	k?	–
wu	wu	k?	wu
(	(	kIx(	(
<g/>
w	w	k?	w
opět	opět	k6eAd1	opět
chápáno	chápat	k5eAaImNgNnS	chápat
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Ukázka	ukázka	k1gFnSc1	ukázka
modlitby	modlitba	k1gFnSc2	modlitba
Otče	otec	k1gMnSc5	otec
náš	náš	k3xOp1gInSc4	náš
v	v	k7c6	v
kašubštině	kašubština	k1gFnSc6	kašubština
(	(	kIx(	(
<g/>
Otče	otec	k1gMnSc5	otec
náš	náš	k3xOp1gMnSc1	náš
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Andrea	Andrea	k1gFnSc1	Andrea
Spretková	Spretková	k1gFnSc1	Spretková
<g/>
:	:	kIx,	:
Charakteristika	charakteristika	k1gFnSc1	charakteristika
Kašubštiny	kašubština	k1gFnSc2	kašubština
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Kartuz	Kartuza	k1gFnPc2	Kartuza
<g/>
,	,	kIx,	,
magisterská	magisterský	k2eAgFnSc1d1	magisterská
diplomová	diplomový	k2eAgFnSc1d1	Diplomová
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
2008	[number]	k4	2008
on-line	onin	k1gInSc5	on-lin
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Kašubové	Kašub	k1gMnPc1	Kašub
</s>
</p>
<p>
<s>
Kašubsko	Kašubsko	k1gNnSc1	Kašubsko
(	(	kIx(	(
<g/>
Kaszëbë	Kaszëbë	k1gFnSc1	Kaszëbë
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kašubština	kašubština	k1gFnSc1	kašubština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Kašubština	kašubština	k1gFnSc1	kašubština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kašubština	kašubština	k1gFnSc1	kašubština
ve	v	k7c6	v
WikislovníkuHajný	WikislovníkuHajný	k2eAgMnSc1d1	WikislovníkuHajný
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Doležal	Doležal	k1gMnSc1	Doležal
J.	J.	kA	J.
Kašubové	Kašub	k1gMnPc1	Kašub
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
(	(	kIx(	(
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
Hospodářská	hospodářský	k2eAgNnPc1d1	hospodářské
a	a	k8xC	a
kulturní	kulturní	k2eAgNnPc1d1	kulturní
studia	studio	k1gNnPc1	studio
<g/>
,	,	kIx,	,
Provozně	provozně	k6eAd1	provozně
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
fakulta	fakulta	k1gFnSc1	fakulta
ČZU	ČZU	kA	ČZU
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kašubský	kašubský	k2eAgInSc1d1	kašubský
konzulát	konzulát	k1gInSc1	konzulát
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
sítě	síť	k1gFnSc2	síť
Kašubská	kašubský	k2eAgFnSc1d1	kašubská
liga	liga	k1gFnSc1	liga
</s>
</p>
<p>
<s>
Kaszëbskô	Kaszëbskô	k?	Kaszëbskô
czëtnica	czëtnica	k6eAd1	czëtnica
–	–	k?	–
Kašubská	kašubský	k2eAgFnSc1d1	kašubská
čítanka	čítanka	k1gFnSc1	čítanka
(	(	kIx(	(
<g/>
kašubsky	kašubsky	k6eAd1	kašubsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cassubia	Cassubia	k1gFnSc1	Cassubia
Slavica	slavicum	k1gNnSc2	slavicum
–	–	k?	–
Kašubská	kašubský	k2eAgNnPc1d1	kašubské
studia	studio	k1gNnPc1	studio
(	(	kIx(	(
<g/>
kašubsky	kašubsky	k6eAd1	kašubsky
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
