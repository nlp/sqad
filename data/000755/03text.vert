<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
řečený	řečený	k2eAgMnSc1d1	řečený
Lev	Lev	k1gMnSc1	Lev
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Louis	Louis	k1gMnSc1	Louis
VIII	VIII	kA	VIII
le	le	k?	le
Lion	Lion	k1gMnSc1	Lion
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1187	[number]	k4	1187
Paříž	Paříž	k1gFnSc4	Paříž
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1226	[number]	k4	1226
Montpensier	Montpensira	k1gFnPc2	Montpensira
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Kapetovců	Kapetovec	k1gInPc2	Kapetovec
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Augusta	Augusta	k1gMnSc1	Augusta
<g/>
.	.	kIx.	.
</s>
<s>
Vládl	vládnout	k5eAaImAgMnS	vládnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1223	[number]	k4	1223
<g/>
-	-	kIx~	-
<g/>
1226	[number]	k4	1226
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
ovládnout	ovládnout	k5eAaPmF	ovládnout
původně	původně	k6eAd1	původně
anglické	anglický	k2eAgNnSc1d1	anglické
Anjou	Anja	k1gMnSc7	Anja
<g/>
,	,	kIx,	,
Poitou	Poita	k1gMnSc7	Poita
<g/>
,	,	kIx,	,
Limousin	Limousin	k1gMnSc1	Limousin
<g/>
,	,	kIx,	,
Périgord	Périgord	k1gMnSc1	Périgord
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1224	[number]	k4	1224
Saintonge	Saintonge	k1gNnPc2	Saintonge
<g/>
.	.	kIx.	.
</s>
<s>
Ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
nedělitelné	dělitelný	k2eNgNnSc4d1	nedělitelné
královské	královský	k2eAgNnSc4d1	královské
léno	léno	k1gNnSc4	léno
bez	bez	k7c2	bez
nároku	nárok	k1gInSc2	nárok
na	na	k7c4	na
dědění	dědění	k1gNnSc4	dědění
ve	v	k7c6	v
vedlejších	vedlejší	k2eAgFnPc6d1	vedlejší
liniích	linie	k1gFnPc6	linie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1213	[number]	k4	1213
a	a	k8xC	a
1215	[number]	k4	1215
<g/>
-	-	kIx~	-
<g/>
1217	[number]	k4	1217
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
pokusil	pokusit	k5eAaPmAgMnS	pokusit
za	za	k7c4	za
podpory	podpora	k1gFnPc4	podpora
rozhořčené	rozhořčený	k2eAgFnSc2d1	rozhořčená
anglické	anglický	k2eAgFnSc2d1	anglická
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
skotského	skotský	k1gInSc2	skotský
krále	král	k1gMnSc2	král
Alexandra	Alexandr	k1gMnSc2	Alexandr
II	II	kA	II
<g/>
.	.	kIx.	.
kandidovat	kandidovat	k5eAaImF	kandidovat
na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
trůn	trůn	k1gInSc4	trůn
Jana	Jan	k1gMnSc2	Jan
Bezzemka	bezzemek	k1gMnSc2	bezzemek
<g/>
.	.	kIx.	.
</s>
<s>
Ambiciózní	ambiciózní	k2eAgInSc1d1	ambiciózní
plán	plán	k1gInSc1	plán
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
díky	díky	k7c3	díky
protistraně	protistrana	k1gFnSc3	protistrana
vedené	vedený	k2eAgFnSc2d1	vedená
Vilémem	Vilém	k1gMnSc7	Vilém
le	le	k?	le
Maréchal	Maréchal	k1gMnSc1	Maréchal
nevyšel	vyjít	k5eNaPmAgMnS	vyjít
a	a	k8xC	a
po	po	k7c6	po
nuceném	nucený	k2eAgInSc6d1	nucený
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
anglické	anglický	k2eAgFnSc2d1	anglická
půdy	půda	k1gFnSc2	půda
musel	muset	k5eAaImAgInS	muset
na	na	k7c4	na
otcovo	otcův	k2eAgNnSc4d1	otcovo
přání	přání	k1gNnSc4	přání
vyrazit	vyrazit	k5eAaPmF	vyrazit
na	na	k7c4	na
jih	jih	k1gInSc4	jih
Francie	Francie	k1gFnSc2	Francie
získat	získat	k5eAaPmF	získat
slib	slib	k1gInSc4	slib
věrnosti	věrnost	k1gFnSc2	věrnost
od	od	k7c2	od
Simona	Simon	k1gMnSc2	Simon
z	z	k7c2	z
Montfortu	Montfort	k1gInSc2	Montfort
<g/>
.	.	kIx.	.
</s>
<s>
Hrabě	Hrabě	k1gMnSc1	Hrabě
z	z	k7c2	z
Montfortu	Montfort	k1gInSc2	Montfort
však	však	k8xC	však
roku	rok	k1gInSc2	rok
1218	[number]	k4	1218
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
Toulouse	Toulouse	k1gInSc2	Toulouse
padl	padnout	k5eAaPmAgMnS	padnout
a	a	k8xC	a
Ludvík	Ludvík	k1gMnSc1	Ludvík
musel	muset	k5eAaImAgMnS	muset
na	na	k7c4	na
jih	jih	k1gInSc4	jih
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přání	přání	k1gNnSc4	přání
papeže	papež	k1gMnSc2	papež
Honoria	Honorium	k1gNnSc2	Honorium
III	III	kA	III
<g/>
.	.	kIx.	.
vyhladit	vyhladit	k5eAaPmF	vyhladit
katary	katar	k1gInPc4	katar
bylo	být	k5eAaImAgNnS	být
podbarveno	podbarvit	k5eAaPmNgNnS	podbarvit
slíbenými	slíbený	k2eAgInPc7d1	slíbený
penězi	peníze	k1gInPc7	peníze
pro	pro	k7c4	pro
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
cca	cca	kA	cca
45	[number]	k4	45
dní	den	k1gInPc2	den
obléhal	obléhat	k5eAaImAgInS	obléhat
Toulouse	Toulouse	k1gInSc1	Toulouse
a	a	k8xC	a
poté	poté	k6eAd1	poté
obléhání	obléhání	k1gNnSc4	obléhání
vzdal	vzdát	k5eAaPmAgMnS	vzdát
a	a	k8xC	a
odtáhl	odtáhnout	k5eAaPmAgMnS	odtáhnout
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
znamenalo	znamenat	k5eAaImAgNnS	znamenat
oživení	oživení	k1gNnSc4	oživení
albigenského	albigenský	k2eAgNnSc2d1	albigenský
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1223	[number]	k4	1223
převzal	převzít	k5eAaPmAgMnS	převzít
vládu	vláda	k1gFnSc4	vláda
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
korunován	korunovat	k5eAaBmNgInS	korunovat
v	v	k7c6	v
Remeši	Remeš	k1gFnSc6	Remeš
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1226	[number]	k4	1226
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
na	na	k7c4	na
papežovo	papežův	k2eAgNnSc4d1	papežovo
naléhání	naléhání	k1gNnSc4	naléhání
znovu	znovu	k6eAd1	znovu
vypravil	vypravit	k5eAaPmAgMnS	vypravit
proti	proti	k7c3	proti
katarům	katar	k1gMnPc3	katar
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dobýt	dobýt	k5eAaPmF	dobýt
jihofrancouzské	jihofrancouzský	k2eAgNnSc1d1	jihofrancouzské
město	město	k1gNnSc1	město
Avignon	Avignon	k1gInSc1	Avignon
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
obléhání	obléhání	k1gNnSc1	obléhání
zapřičinilo	zapřičinit	k5eAaImAgNnS	zapřičinit
epidemii	epidemie	k1gFnSc4	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vzdala	vzdát	k5eAaPmAgFnS	vzdát
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
města	město	k1gNnSc2	město
Nîmes	Nîmesa	k1gFnPc2	Nîmesa
<g/>
,	,	kIx,	,
Beaucaire	Beaucair	k1gMnSc5	Beaucair
<g/>
,	,	kIx,	,
Narbonne	Narbonn	k1gMnSc5	Narbonn
<g/>
,	,	kIx,	,
Carcassonne	Carcassonn	k1gMnSc5	Carcassonn
<g/>
,	,	kIx,	,
Montpellier	Montpellira	k1gFnPc2	Montpellira
<g/>
,	,	kIx,	,
Pamiers	Pamiersa	k1gFnPc2	Pamiersa
a	a	k8xC	a
Castres	Castresa	k1gFnPc2	Castresa
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
úspěšného	úspěšný	k2eAgNnSc2d1	úspěšné
tažení	tažení	k1gNnSc2	tažení
král	král	k1gMnSc1	král
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
úplavici	úplavice	k1gFnSc4	úplavice
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
rodovém	rodový	k2eAgNnSc6d1	rodové
pohřebisti	pohřebistit	k5eAaPmRp2nS	pohřebistit
v	v	k7c6	v
Saint-Denis	Saint-Denis	k1gFnPc6	Saint-Denis
<g/>
.	.	kIx.	.
</s>
