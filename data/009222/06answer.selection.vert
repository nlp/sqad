<s>
Pojmem	pojem	k1gInSc7	pojem
klasický	klasický	k2eAgInSc1d1	klasický
Hollywood	Hollywood	k1gInSc1	Hollywood
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
období	období	k1gNnSc1	období
americké	americký	k2eAgFnSc2d1	americká
kinematografie	kinematografie	k1gFnSc2	kinematografie
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
do	do	k7c2	do
pozdních	pozdní	k2eAgNnPc2d1	pozdní
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
