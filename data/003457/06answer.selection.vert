<s>
William	William	k6eAd1	William
Clark	Clark	k1gInSc1	Clark
Styron	Styron	k1gInSc1	Styron
<g/>
,	,	kIx,	,
Jr	Jr	k1gFnSc1	Jr
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
stajrn	stajrn	k1gInSc1	stajrn
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1925	[number]	k4	1925
Newport	Newport	k1gInSc1	Newport
News	News	k1gInSc4	News
<g/>
,	,	kIx,	,
Virginie	Virginie	k1gFnSc1	Virginie
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2006	[number]	k4	2006
Martha	Martha	k1gMnSc1	Martha
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Vineyard	Vineyard	k1gInSc1	Vineyard
<g/>
,	,	kIx,	,
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
tzv.	tzv.	kA	tzv.
jižanské	jižanský	k2eAgFnSc2d1	jižanská
prózy	próza	k1gFnSc2	próza
<g/>
.	.	kIx.	.
</s>
