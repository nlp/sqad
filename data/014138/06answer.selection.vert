<s desamb="1">
Mnohá	mnohé	k1gNnPc1
děl	dělo	k1gNnPc2
autorů	autor	k1gMnPc2
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
přeložil	přeložit	k5eAaPmAgInS
a	a	k8xC
vydal	vydat	k5eAaPmAgInS
Liberální	liberální	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
nejstarší	starý	k2eAgMnSc1d3
český	český	k2eAgMnSc1d1
think	think	k1gMnSc1
tank	tank	k1gInSc4
zabývající	zabývající	k2eAgInSc4d1
se	s	k7c7
šířením	šíření	k1gNnSc7
liberálních	liberální	k2eAgFnPc2d1
myšlenek	myšlenka	k1gFnPc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>