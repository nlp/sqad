<s>
Kamila	Kamila	k1gFnSc1
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
epizodu	epizoda	k1gFnSc4
ze	z	k7c2
seriálu	seriál	k1gInSc2
Červený	Červený	k1gMnSc1
trpaslík	trpaslík	k1gMnSc1
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
Kamila	Kamila	k1gFnSc1
(	(	kIx(
<g/>
Červený	Červený	k1gMnSc1
trpaslík	trpaslík	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kamila	Kamila	k1gFnSc1
</s>
<s>
ženské	ženský	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
Svátek	svátek	k1gInSc4
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
Původ	původ	k1gInSc1
</s>
<s>
latinský	latinský	k2eAgInSc1d1
Četnost	četnost	k1gFnSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
18941	#num#	k4
Pořadí	pořadí	k1gNnPc1
podle	podle	k7c2
četnosti	četnost	k1gFnSc2
</s>
<s>
119	#num#	k4
Podle	podle	k7c2
údajů	údaj	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
</s>
<s>
2016	#num#	k4
Četnost	četnost	k1gFnSc1
jmen	jméno	k1gNnPc2
a	a	k8xC
příjmení	příjmení	k1gNnSc2
graficky	graficky	k6eAd1
na	na	k7c6
mapě	mapa	k1gFnSc6
<g/>
,	,	kIx,
četnost	četnost	k1gFnSc1
dle	dle	k7c2
ročníků	ročník	k1gInPc2
v	v	k7c6
grafu	graf	k1gInSc6
<g/>
,	,	kIx,
původ	původ	k1gInSc4
a	a	k8xC
osobnostiTento	osobnostiTento	k1gNnSc4
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Camilla	Camilla	k6eAd1
</s>
<s>
ženské	ženský	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
Svátek	svátek	k1gInSc4
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
Původ	původ	k1gInSc1
</s>
<s>
latinský	latinský	k2eAgInSc1d1
Četnost	četnost	k1gFnSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
23	#num#	k4
Pořadí	pořadí	k1gNnPc1
podle	podle	k7c2
četnosti	četnost	k1gFnSc2
</s>
<s>
1459	#num#	k4
Podle	podle	k7c2
údajů	údaj	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
</s>
<s>
2007	#num#	k4
Četnost	četnost	k1gFnSc1
jmen	jméno	k1gNnPc2
a	a	k8xC
příjmení	příjmení	k1gNnSc2
graficky	graficky	k6eAd1
na	na	k7c6
mapě	mapa	k1gFnSc6
<g/>
,	,	kIx,
četnost	četnost	k1gFnSc1
dle	dle	k7c2
ročníků	ročník	k1gInPc2
v	v	k7c6
grafu	graf	k1gInSc6
<g/>
,	,	kIx,
původ	původ	k1gInSc4
a	a	k8xC
osobnostiTento	osobnostiTento	k1gNnSc4
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Kamila	Kamila	k1gFnSc1
je	být	k5eAaImIp3nS
ženské	ženský	k2eAgNnSc4d1
křestní	křestní	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
latinského	latinský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
přechýlené	přechýlený	k2eAgNnSc1d1
z	z	k7c2
mužského	mužský	k2eAgNnSc2d1
Kamil	Kamil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svátek	svátek	k1gInSc4
v	v	k7c6
ČR	ČR	kA
slaví	slavit	k5eAaImIp3nS
podle	podle	k7c2
občanského	občanský	k2eAgInSc2d1
kalendáře	kalendář	k1gInSc2
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jméno	jméno	k1gNnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
latinského	latinský	k2eAgNnSc2d1
camilla	camilla	k1gFnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
význam	význam	k1gInSc1
je	být	k5eAaImIp3nS
„	kIx"	kIx"
<g/>
osoba	osoba	k1gFnSc1
vznešeného	vznešený	k2eAgInSc2d1
původu	původ	k1gInSc2
vhodná	vhodný	k2eAgFnSc1d1
pro	pro	k7c4
kněžskou	kněžský	k2eAgFnSc4d1
službu	služba	k1gFnSc4
<g/>
“	“	kIx"
<g/>
.	.	kIx.
</s>
<s>
Statistické	statistický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Následující	následující	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
uvádí	uvádět	k5eAaImIp3nS
četnost	četnost	k1gFnSc4
jména	jméno	k1gNnSc2
v	v	k7c6
ČR	ČR	kA
a	a	k8xC
pořadí	pořadí	k1gNnSc2
mezi	mezi	k7c7
ženskými	ženský	k2eAgNnPc7d1
jmény	jméno	k1gNnPc7
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
dvou	dva	k4xCgInPc2
roků	rok	k1gInPc2
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
jsou	být	k5eAaImIp3nP
dostupné	dostupný	k2eAgInPc1d1
údaje	údaj	k1gInPc1
MV	MV	kA
ČR	ČR	kA
–	–	k?
lze	lze	k6eAd1
z	z	k7c2
ní	on	k3xPp3gFnSc2
tedy	tedy	k9
vysledovat	vysledovat	k5eAaPmF,k5eAaImF
trend	trend	k1gInSc4
v	v	k7c6
užívání	užívání	k1gNnSc6
tohoto	tento	k3xDgNnSc2
jména	jméno	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Četnost	četnost	k1gFnSc1
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
1999	#num#	k4
</s>
<s>
16905	#num#	k4
</s>
<s>
74	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2002	#num#	k4
</s>
<s>
17163	#num#	k4
</s>
<s>
74	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Porovnání	porovnání	k1gNnSc1
</s>
<s>
o	o	k7c4
258	#num#	k4
více	hodně	k6eAd2
</s>
<s>
žádná	žádný	k3yNgFnSc1
změna	změna	k1gFnSc1
</s>
<s>
2016	#num#	k4
</s>
<s>
18941	#num#	k4
</s>
<s>
119	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Porovnání	porovnání	k1gNnSc1
</s>
<s>
o	o	k7c4
1778	#num#	k4
více	hodně	k6eAd2
</s>
<s>
o	o	k7c4
45	#num#	k4
horší	zlý	k2eAgMnSc1d2
</s>
<s>
Změna	změna	k1gFnSc1
procentního	procentní	k2eAgNnSc2d1
zastoupení	zastoupení	k1gNnSc2
tohoto	tento	k3xDgNnSc2
jména	jméno	k1gNnSc2
mezi	mezi	k7c7
žijícími	žijící	k2eAgFnPc7d1
ženami	žena	k1gFnPc7
v	v	k7c6
ČR	ČR	kA
(	(	kIx(
<g/>
tj.	tj.	kA
procentní	procentní	k2eAgFnSc1d1
změna	změna	k1gFnSc1
se	se	k3xPyFc4
započítáním	započítání	k1gNnSc7
celkového	celkový	k2eAgInSc2d1
úbytku	úbytek	k1gInSc2
žen	žena	k1gFnPc2
v	v	k7c6
ČR	ČR	kA
za	za	k7c4
sledované	sledovaný	k2eAgInPc4d1
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
+2,3	+2,3	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
byla	být	k5eAaImAgFnS
Kamila	Kamila	k1gFnSc1
43	#num#	k4
<g/>
.	.	kIx.
nejčetnějším	četný	k2eAgNnSc7d3
jménem	jméno	k1gNnSc7
mezi	mezi	k7c7
novorozenci	novorozenec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Zdrobněliny	zdrobnělina	k1gFnPc1
</s>
<s>
Kamilka	Kamilka	k1gFnSc1
<g/>
,	,	kIx,
Kamča	Kamča	k?
<g/>
,	,	kIx,
Kami	Kami	k1gNnSc1
<g/>
,	,	kIx,
Kamka	Kamka	k1gFnSc1
<g/>
,	,	kIx,
Kamuška	Kamuška	k1gFnSc1
</s>
<s>
Známé	známý	k2eAgFnPc1d1
nositelky	nositelka	k1gFnPc1
jména	jméno	k1gNnSc2
</s>
<s>
Camila	Camit	k5eAaImAgFnS,k5eAaBmAgFnS,k5eAaPmAgFnS
Cabello	Cabello	k1gNnSc4
–	–	k?
Anglická	anglický	k2eAgFnSc1d1
zpěvačka	zpěvačka	k1gFnSc1
</s>
<s>
Camilla	Camilla	k1gMnSc1
Parker-Bowles	Parker-Bowles	k1gMnSc1
–	–	k?
hraběnka	hraběnka	k1gFnSc1
z	z	k7c2
Cornwallu	Cornwall	k1gInSc2
</s>
<s>
Kamila	Kamila	k1gFnSc1
Berndorffová	Berndorffový	k2eAgFnSc1d1
–	–	k?
současná	současný	k2eAgFnSc1d1
česká	český	k2eAgFnSc1d1
reportážní	reportážní	k2eAgFnSc1d1
a	a	k8xC
dokumentární	dokumentární	k2eAgFnSc1d1
fotografka	fotografka	k1gFnSc1
</s>
<s>
Kamila	Kamila	k1gFnSc1
Bezpalcová	Bezpalcový	k2eAgFnSc1d1
–	–	k?
česká	český	k2eAgFnSc1d1
modelka	modelka	k1gFnSc1
<g/>
,	,	kIx,
fotomodelka	fotomodelka	k1gFnSc1
<g/>
,	,	kIx,
hosteska	hosteska	k1gFnSc1
<g/>
,	,	kIx,
komparzistka	komparzistka	k1gFnSc1
<g/>
,	,	kIx,
personalistka	personalistka	k1gFnSc1
a	a	k8xC
finalistka	finalistka	k1gFnSc1
České	český	k2eAgFnSc2d1
Miss	miss	k1gFnSc2
2014	#num#	k4
</s>
<s>
Kamila	Kamila	k1gFnSc1
Doležalová	Doležalová	k1gFnSc1
–	–	k?
česká	český	k2eAgFnSc1d1
a	a	k8xC
československá	československý	k2eAgFnSc1d1
politička	politička	k1gFnSc1
</s>
<s>
Kamila	Kamila	k1gFnSc1
Gasiuk-Pihowicz	Gasiuk-Pihowicza	k1gFnPc2
–	–	k?
polská	polský	k2eAgFnSc1d1
právnička	právnička	k1gFnSc1
a	a	k8xC
politička	politička	k1gFnSc1
</s>
<s>
Kamila	Kamila	k1gFnSc1
Hájková	Hájková	k1gFnSc1
–	–	k?
česká	český	k2eAgFnSc1d1
krasobruslařka	krasobruslařka	k1gFnSc1
</s>
<s>
Kamila	Kamila	k1gFnSc1
Chudziková	Chudzikový	k2eAgFnSc1d1
–	–	k?
polská	polský	k2eAgFnSc1d1
atletka	atletka	k1gFnSc1
</s>
<s>
Kamila	Kamila	k1gFnSc1
Klugarová	Klugarový	k2eAgFnSc1d1
–	–	k?
česká	český	k2eAgFnSc1d1
varhanice	varhanice	k1gFnSc1
</s>
<s>
Kamila	Kamila	k1gFnSc1
Lićwinková	Lićwinkový	k2eAgFnSc1d1
–	–	k?
polská	polský	k2eAgFnSc1d1
atletka	atletka	k1gFnSc1
</s>
<s>
Kamila	Kamila	k1gFnSc1
Kikinčuková	Kikinčukový	k2eAgFnSc1d1
–	–	k?
česká	český	k2eAgFnSc1d1
divadelní	divadelní	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
</s>
<s>
Kamila	Kamila	k1gFnSc1
Magálová	Magálový	k2eAgFnSc1d1
–	–	k?
slovenská	slovenský	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
</s>
<s>
Kamila	Kamila	k1gFnSc1
Moučková	moučkový	k2eAgFnSc1d1
–	–	k?
česká	český	k2eAgFnSc1d1
televizní	televizní	k2eAgFnSc1d1
hlasatelka	hlasatelka	k1gFnSc1
(	(	kIx(
<g/>
rok	rok	k1gInSc1
1968	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
moderátorka	moderátorka	k1gFnSc1
</s>
<s>
Kamila	Kamila	k1gFnSc1
Neumannová	Neumannová	k1gFnSc1
–	–	k?
česká	český	k2eAgFnSc1d1
nakladatelka	nakladatelka	k1gFnSc1
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
choť	choť	k1gMnSc1
S.	S.	kA
K.	K.	kA
Neumanna	Neumann	k1gMnSc2
<g/>
,	,	kIx,
matka	matka	k1gFnSc1
herce	herec	k1gMnSc2
Stanislava	Stanislav	k1gMnSc2
Neumanna	Neumann	k1gMnSc2
a	a	k8xC
překladatelky	překladatelka	k1gFnSc2
Kamily	Kamila	k1gFnSc2
Značkovské-Neumannové	Značkovské-Neumannová	k1gFnSc2
</s>
<s>
Kamila	Kamila	k1gFnSc1
Nývltová	Nývltová	k1gFnSc1
–	–	k?
česká	český	k2eAgFnSc1d1
zpěvačka	zpěvačka	k1gFnSc1
</s>
<s>
Kamila	Kamila	k1gFnSc1
Pešková	Pešková	k1gFnSc1
–	–	k?
česká	český	k2eAgFnSc1d1
a	a	k8xC
československá	československý	k2eAgFnSc1d1
lékařka	lékařka	k1gFnSc1
</s>
<s>
Kamila	Kamila	k1gFnSc1
Rajdlová	Rajdlový	k2eAgFnSc1d1
–	–	k?
česká	český	k2eAgFnSc1d1
běžkyně	běžkyně	k1gFnSc1
na	na	k7c6
lyžích	lyže	k1gFnPc6
</s>
<s>
Kamila	Kamila	k1gFnSc1
Skolimowska	Skolimowsk	k1gInSc2
–	–	k?
polská	polský	k2eAgFnSc1d1
atletka	atletka	k1gFnSc1
</s>
<s>
Kamila	Kamila	k1gFnSc1
Sojková	Sojková	k1gFnSc1
–	–	k?
česká	český	k2eAgFnSc1d1
spisovatelka	spisovatelka	k1gFnSc1
</s>
<s>
Kamila	Kamila	k1gFnSc1
Stösslová	Stösslový	k2eAgFnSc1d1
–	–	k?
přítelkyně	přítelkyně	k1gFnSc1
a	a	k8xC
pozdní	pozdní	k2eAgFnSc1d1
láska	láska	k1gFnSc1
Leoše	Leoš	k1gMnSc2
Janáčka	Janáček	k1gMnSc2
</s>
<s>
Kamila	Kamila	k1gFnSc1
Špráchalová	Špráchalový	k2eAgFnSc1d1
–	–	k?
česká	český	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
</s>
<s>
Kamila	Kamila	k1gFnSc1
Thompson	Thompsona	k1gFnPc2
–	–	k?
anglická	anglický	k2eAgFnSc1d1
zpěvačka	zpěvačka	k1gFnSc1
</s>
<s>
Kamila	Kamila	k1gFnSc1
Značkovská-Neumannová	Značkovská-Neumannový	k2eAgFnSc1d1
–	–	k?
česká	český	k2eAgFnSc1d1
překladatelka	překladatelka	k1gFnSc1
</s>
<s>
Kamila	Kamila	k1gFnSc1
Ženatá	ženatý	k2eAgFnSc1d1
–	–	k?
česká	český	k2eAgFnSc1d1
výtvarná	výtvarný	k2eAgFnSc1d1
umělkyně	umělkyně	k1gFnSc1
</s>
