<s>
Zla	zlo	k1gNnPc1	zlo
Kolata	Kolat	k2eAgNnPc1d1	Kolat
(	(	kIx(	(
<g/>
Kolac	Kolac	k1gFnSc4	Kolac
<g/>
,	,	kIx,	,
albánsky	albánsky	k6eAd1	albánsky
Kollata	Kolle	k1gNnPc1	Kolle
ë	ë	k?	ë
Keqe	Keq	k1gMnSc2	Keq
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
2534	[number]	k4	2534
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zároveň	zároveň	k6eAd1	zároveň
16	[number]	k4	16
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
v	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
hory	hora	k1gFnSc2	hora
znamená	znamenat	k5eAaImIp3nS	znamenat
Špatná	špatný	k2eAgFnSc1d1	špatná
Kolata	Kolata	k1gFnSc1	Kolata
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
vrcholem	vrchol	k1gInSc7	vrchol
masivu	masiv	k1gInSc2	masiv
Bjeliče	Bjelič	k1gInSc2	Bjelič
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Dobra	dobro	k1gNnSc2	dobro
Kolata	Kolata	k1gFnSc1	Kolata
(	(	kIx(	(
<g/>
albánsky	albánsky	k6eAd1	albánsky
Kollata	Kolle	k1gNnPc1	Kolle
e	e	k0	e
mirë	mirë	k?	mirë
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
Kolata	Kolata	k1gFnSc1	Kolata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
2528	[number]	k4	2528
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
druhou	druhý	k4xOgFnSc7	druhý
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
a	a	k8xC	a
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
něco	něco	k3yInSc4	něco
málo	málo	k1gNnSc4	málo
přes	přes	k7c4	přes
100	[number]	k4	100
m	m	kA	m
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
oddělena	oddělit	k5eAaPmNgFnS	oddělit
sedlem	sedlo	k1gNnSc7	sedlo
2416	[number]	k4	2416
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Význam	význam	k1gInSc1	význam
slova	slovo	k1gNnSc2	slovo
Kolata	Kolat	k1gMnSc2	Kolat
je	být	k5eAaImIp3nS	být
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Kolac	Kolac	k1gFnSc1	Kolac
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
další	další	k2eAgNnSc4d1	další
jméno	jméno	k1gNnSc4	jméno
pro	pro	k7c4	pro
Kolatu	Kolata	k1gFnSc4	Kolata
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
černohorštině	černohorština	k1gFnSc6	černohorština
<g/>
/	/	kIx~	/
<g/>
srbštině	srbština	k1gFnSc6	srbština
hromadu	hromadu	k6eAd1	hromadu
<g/>
.	.	kIx.	.
</s>
<s>
Zla	zlo	k1gNnPc1	zlo
Kolata	Kolat	k2eAgNnPc1d1	Kolat
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Černé	Černé	k2eAgFnSc2d1	Černé
Hory	hora	k1gFnSc2	hora
a	a	k8xC	a
Albánie	Albánie	k1gFnSc2	Albánie
<g/>
,	,	kIx,	,
10	[number]	k4	10
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
černohorského	černohorský	k2eAgNnSc2d1	černohorské
městečka	městečko	k1gNnSc2	městečko
Gusinje	Gusinj	k1gFnSc2	Gusinj
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Dinárských	dinárský	k2eAgFnPc6d1	Dinárská
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Prokletije	Prokletije	k1gFnSc2	Prokletije
<g/>
,	,	kIx,	,
v	v	k7c6	v
horském	horský	k2eAgInSc6d1	horský
celku	celek	k1gInSc6	celek
Bělič	bělič	k1gMnSc1	bělič
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
severní	severní	k2eAgFnSc7d1	severní
rozsochou	rozsocha	k1gFnSc7	rozsocha
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
vrcholu	vrchol	k1gInSc2	vrchol
Bjeliče	Bjelič	k1gInSc2	Bjelič
<g/>
,	,	kIx,	,
Maja	Maja	k1gFnSc1	Maja
Kolats	Kolatsa	k1gFnPc2	Kolatsa
(	(	kIx(	(
<g/>
albánsky	albánsky	k6eAd1	albánsky
Rodi	Rodi	k1gNnSc1	Rodi
e	e	k0	e
Kollatës	Kollatës	k1gInSc4	Kollatës
nebo	nebo	k8xC	nebo
také	také	k9	také
Maja	Maja	k1gFnSc1	Maja
e	e	k0	e
Kollatë	Kollatë	k1gMnSc5	Kollatë
<g/>
,	,	kIx,	,
2556	[number]	k4	2556
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
albánském	albánský	k2eAgNnSc6d1	albánské
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
hory	hora	k1gFnSc2	hora
Zla	zlo	k1gNnSc2	zlo
Kolata	Kolat	k2eAgFnSc1d1	Kolat
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
malé	malý	k2eAgNnSc4d1	malé
sedlo	sedlo	k1gNnSc4	sedlo
leží	ležet	k5eAaImIp3nP	ležet
Dobra	dobro	k1gNnPc1	dobro
Kolata	Kolat	k1gMnSc2	Kolat
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
2	[number]	k4	2
528	[number]	k4	528
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Zla	zlo	k1gNnSc2	zlo
Kolata	Kolata	k1gFnSc1	Kolata
leží	ležet	k5eAaImIp3nS	ležet
necelých	celý	k2eNgInPc2d1	necelý
deset	deset	k4xCc4	deset
kilometrů	kilometr	k1gInPc2	kilometr
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Maja	Maja	k1gFnSc1	Maja
e	e	k0	e
Jezercës	Jezercësa	k1gFnPc2	Jezercësa
a	a	k8xC	a
necelých	celý	k2eNgInPc2d1	necelý
dvacet	dvacet	k4xCc4	dvacet
kilometrů	kilometr	k1gInPc2	kilometr
západně	západně	k6eAd1	západně
od	od	k7c2	od
Djeravica	Djeravic	k1gInSc2	Djeravic
<g/>
,	,	kIx,	,
dvou	dva	k4xCgFnPc6	dva
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hor	hora	k1gFnPc2	hora
Prokletije	Prokletije	k1gFnSc2	Prokletije
<g/>
.	.	kIx.	.
</s>
<s>
Zla	zlo	k1gNnPc4	zlo
Kolata	Kolat	k2eAgFnSc1d1	Kolat
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
mohutného	mohutný	k2eAgInSc2d1	mohutný
skalního	skalní	k2eAgInSc2d1	skalní
zubu	zub	k1gInSc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
ostatně	ostatně	k6eAd1	ostatně
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
celý	celý	k2eAgInSc1d1	celý
masiv	masiv	k1gInSc1	masiv
Bjeliče	Bjelič	k1gInSc2	Bjelič
<g/>
,	,	kIx,	,
budován	budován	k2eAgInSc1d1	budován
z	z	k7c2	z
vápence	vápenec	k1gInSc2	vápenec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
horskému	horský	k2eAgInSc3d1	horský
celku	celek	k1gInSc3	celek
Bjelič	Bjelič	k1gInSc4	Bjelič
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
dostat	dostat	k5eAaPmF	dostat
z	z	k7c2	z
černohorského	černohorský	k2eAgNnSc2d1	černohorské
městečka	městečko	k1gNnSc2	městečko
Gusinje	Gusinj	k1gFnSc2	Gusinj
na	na	k7c4	na
jih	jih	k1gInSc4	jih
údolím	údolí	k1gNnSc7	údolí
Ropojana	Ropojan	k1gMnSc2	Ropojan
do	do	k7c2	do
vesnice	vesnice	k1gFnSc2	vesnice
Vusanje	Vusanj	k1gFnSc2	Vusanj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Vusanje	Vusanj	k1gInSc2	Vusanj
vede	vést	k5eAaImIp3nS	vést
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Zla	zlo	k1gNnSc2	zlo
Kolata	Kolat	k2eAgFnSc1d1	Kolat
značená	značený	k2eAgFnSc1d1	značená
turistická	turistický	k2eAgFnSc1d1	turistická
stezka	stezka	k1gFnSc1	stezka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
předložilo	předložit	k5eAaPmAgNnS	předložit
černohorské	černohorský	k2eAgNnSc1d1	černohorské
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
a	a	k8xC	a
ochrany	ochrana	k1gFnSc2	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
návrh	návrh	k1gInSc4	návrh
Zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
národních	národní	k2eAgInPc6d1	národní
parcích	park	k1gInPc6	park
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
Černohorské	Černohorské	k2eAgFnSc1d1	Černohorské
Prokletije	Prokletije	k1gFnSc4	Prokletije
stát	stát	k5eAaImF	stát
pátým	pátý	k4xOgInSc7	pátý
národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
připravovaného	připravovaný	k2eAgInSc2d1	připravovaný
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k8xC	i
vrcholy	vrchol	k1gInPc7	vrchol
Zla	zlo	k1gNnSc2	zlo
a	a	k8xC	a
Dobra	dobro	k1gNnSc2	dobro
Kolata	Kolata	k1gFnSc1	Kolata
<g/>
.	.	kIx.	.
údolí	údolí	k1gNnSc1	údolí
Ropojana	Ropojana	k1gFnSc1	Ropojana
-	-	kIx~	-
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
ledovcové	ledovcový	k2eAgNnSc1d1	ledovcové
údolí	údolí	k1gNnSc1	údolí
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
protéká	protékat	k5eAaImIp3nS	protékat
říčka	říčka	k1gFnSc1	říčka
Vruja	Vruj	k2eAgFnSc1d1	Vruj
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
zdrojnic	zdrojnice	k1gFnPc2	zdrojnice
Limu	Lima	k1gFnSc4	Lima
<g/>
,	,	kIx,	,
v	v	k7c6	v
dolině	dolina	k1gFnSc6	dolina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kaňon	kaňon	k1gInSc1	kaňon
říčky	říčka	k1gFnSc2	říčka
Skakavica	Skakavic	k1gInSc2	Skakavic
<g/>
;	;	kIx,	;
většina	většina	k1gFnSc1	většina
údolí	údolí	k1gNnSc2	údolí
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Černé	Černé	k2eAgFnSc6d1	Černé
Hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
ale	ale	k8xC	ale
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
až	až	k9	až
do	do	k7c2	do
Albánie	Albánie	k1gFnSc2	Albánie
Vusanje	Vusanj	k1gFnSc2	Vusanj
-	-	kIx~	-
malá	malý	k2eAgFnSc1d1	malá
vesnice	vesnice	k1gFnSc1	vesnice
v	v	k7c6	v
dolině	dolina	k1gFnSc6	dolina
Ropojana	Ropojan	k1gMnSc2	Ropojan
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
mešita	mešita	k1gFnSc1	mešita
Dobra	dobro	k1gNnSc2	dobro
Kolata	Kolata	k1gFnSc1	Kolata
(	(	kIx(	(
<g/>
2528	[number]	k4	2528
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rosni	Roseň	k1gFnSc3	Roseň
vrh	vrh	k1gInSc1	vrh
(	(	kIx(	(
<g/>
Maja	Maja	k1gFnSc1	Maja
e	e	k0	e
Rosit	rosit	k5eAaImF	rosit
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2524	[number]	k4	2524
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Maja	Maja	k1gFnSc1	Maja
e	e	k0	e
Desljes	Desljes	k1gMnSc1	Desljes
(	(	kIx(	(
<g/>
2454	[number]	k4	2454
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
-	-	kIx~	-
další	další	k2eAgInPc4d1	další
vrcholy	vrchol	k1gInPc4	vrchol
masivu	masiv	k1gInSc2	masiv
Bjelič	Bjelič	k1gMnSc1	Bjelič
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zla	zlo	k1gNnSc2	zlo
Kolata	Kolat	k1gMnSc2	Kolat
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Zla	zlo	k1gNnSc2	zlo
Kolata	Kolat	k1gMnSc2	Kolat
na	na	k7c4	na
HoryEvropy	HoryEvrop	k1gInPc4	HoryEvrop
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Tragická	tragický	k2eAgFnSc1d1	tragická
událost	událost	k1gFnSc1	událost
při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
na	na	k7c4	na
Zlou	zlý	k2eAgFnSc4d1	zlá
Kolatu	Kolata	k1gFnSc4	Kolata
na	na	k7c4	na
iDnes	iDnes	k1gInSc4	iDnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
