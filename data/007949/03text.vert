<s>
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
(	(	kIx(	(
<g/>
Rusky	Ruska	k1gFnPc1	Ruska
<g/>
:	:	kIx,	:
Е	Е	k?	Е
О	О	k?	О
<g/>
,	,	kIx,	,
BGN	BGN	kA	BGN
<g/>
/	/	kIx~	/
<g/>
PCGN	PCGN	kA	PCGN
<g/>
:	:	kIx,	:
Yevgeniy	Yevgenia	k1gMnSc2	Yevgenia
Onegin	Onegin	k1gMnSc1	Onegin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
román	román	k1gInSc4	román
ve	v	k7c6	v
verších	verš	k1gInPc6	verš
napsaný	napsaný	k2eAgInSc4d1	napsaný
romantickým	romantický	k2eAgMnSc7d1	romantický
básníkem	básník	k1gMnSc7	básník
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Sergejevičem	Sergejevič	k1gMnSc7	Sergejevič
Puškinem	Puškin	k1gMnSc7	Puškin
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
klasických	klasický	k2eAgFnPc2d1	klasická
knih	kniha	k1gFnPc2	kniha
ruské	ruský	k2eAgFnSc2d1	ruská
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
hrdina	hrdina	k1gMnSc1	hrdina
sloužil	sloužit	k5eAaImAgMnS	sloužit
za	za	k7c4	za
vzor	vzor	k1gInSc4	vzor
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgMnPc2d1	jiný
ruských	ruský	k2eAgMnPc2d1	ruský
literárních	literární	k2eAgMnPc2d1	literární
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
jako	jako	k8xC	jako
seriál	seriál	k1gInSc1	seriál
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1823	[number]	k4	1823
až	až	k9	až
1831	[number]	k4	1831
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
souborná	souborný	k2eAgFnSc1d1	souborná
edice	edice	k1gFnSc1	edice
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
<g/>
;	;	kIx,	;
současná	současný	k2eAgFnSc1d1	současná
kanonická	kanonický	k2eAgFnSc1d1	kanonická
edice	edice	k1gFnSc1	edice
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
<g/>
.	.	kIx.	.
</s>
<s>
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
<g/>
:	:	kIx,	:
mladý	mladý	k2eAgMnSc1d1	mladý
chytrý	chytrý	k2eAgMnSc1d1	chytrý
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
cynik	cynik	k1gMnSc1	cynik
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
znuděn	znudit	k5eAaPmNgInS	znudit
městským	městský	k2eAgInSc7d1	městský
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
zbytečný	zbytečný	k2eAgMnSc1d1	zbytečný
člověk	člověk	k1gMnSc1	člověk
Vladimír	Vladimír	k1gMnSc1	Vladimír
Lenskij	Lenskij	k1gMnSc1	Lenskij
<g/>
:	:	kIx,	:
mladý	mladý	k2eAgMnSc1d1	mladý
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
přítel	přítel	k1gMnSc1	přítel
Oněgina	Oněgina	k1gMnSc1	Oněgina
<g/>
,	,	kIx,	,
žárlivý	žárlivý	k2eAgMnSc1d1	žárlivý
<g/>
,	,	kIx,	,
miluje	milovat	k5eAaImIp3nS	milovat
Olgu	Olga	k1gFnSc4	Olga
Olga	Olga	k1gFnSc1	Olga
Larinová	Larinová	k1gFnSc1	Larinová
<g/>
:	:	kIx,	:
sestra	sestra	k1gFnSc1	sestra
Taťány	Taťána	k1gFnSc2	Taťána
<g/>
,	,	kIx,	,
společenská	společenský	k2eAgFnSc1d1	společenská
<g/>
,	,	kIx,	,
veselá	veselý	k2eAgFnSc1d1	veselá
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
miluje	milovat	k5eAaImIp3nS	milovat
Lenského	Lenského	k2eAgFnSc1d1	Lenského
Taťána	Taťána	k1gFnSc1	Taťána
Larinová	Larinový	k2eAgFnSc1d1	Larinový
<g/>
:	:	kIx,	:
starší	starý	k2eAgFnSc1d2	starší
sestra	sestra	k1gFnSc1	sestra
Olgy	Olga	k1gFnSc2	Olga
<g/>
,	,	kIx,	,
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
<g/>
,	,	kIx,	,
samotářská	samotářský	k2eAgFnSc1d1	samotářská
<g/>
,	,	kIx,	,
miluje	milovat	k5eAaImIp3nS	milovat
Oněgina	Oněgin	k2eAgFnSc1d1	Oněgin
Titulní	titulní	k2eAgFnSc1d1	titulní
postava	postava	k1gFnSc1	postava
románu	román	k1gInSc2	román
<g/>
,	,	kIx,	,
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
–	–	k?	–
mladý	mladý	k2eAgMnSc1d1	mladý
floutek	floutek	k1gMnSc1	floutek
z	z	k7c2	z
vyšší	vysoký	k2eAgFnSc2d2	vyšší
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tráví	trávit	k5eAaImIp3nS	trávit
své	svůj	k3xOyFgInPc4	svůj
petrohradské	petrohradský	k2eAgInPc4d1	petrohradský
dny	den	k1gInPc4	den
dílem	dílem	k6eAd1	dílem
v	v	k7c6	v
nečinnosti	nečinnost	k1gFnSc6	nečinnost
<g/>
,	,	kIx,	,
dílem	dílem	k6eAd1	dílem
návštěvami	návštěva	k1gFnPc7	návštěva
různých	různý	k2eAgInPc2d1	různý
plesů	ples	k1gInPc2	ples
a	a	k8xC	a
večírků	večírek	k1gInPc2	večírek
plných	plný	k2eAgFnPc2d1	plná
konvencí	konvence	k1gFnPc2	konvence
a	a	k8xC	a
přetvářky	přetvářka	k1gFnSc2	přetvářka
<g/>
,	,	kIx,	,
opouští	opouštět	k5eAaImIp3nS	opouštět
rušný	rušný	k2eAgInSc1d1	rušný
společenský	společenský	k2eAgInSc1d1	společenský
život	život	k1gInSc1	život
velkoměsta	velkoměsto	k1gNnSc2	velkoměsto
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ho	on	k3xPp3gInSc4	on
již	již	k9	již
stejně	stejně	k6eAd1	stejně
nudil	nudit	k5eAaImAgMnS	nudit
<g/>
,	,	kIx,	,
a	a	k8xC	a
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
se	se	k3xPyFc4	se
na	na	k7c4	na
venkovské	venkovský	k2eAgNnSc4d1	venkovské
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
zdědil	zdědit	k5eAaPmAgMnS	zdědit
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
strýci	strýc	k1gMnSc6	strýc
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
melancholie	melancholie	k1gFnSc1	melancholie
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
chandra	chandra	k1gFnSc1	chandra
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
ho	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
neopouští	opouštět	k5eNaImIp3nS	opouštět
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
společností	společnost	k1gFnSc7	společnost
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gNnSc3	on
básník	básník	k1gMnSc1	básník
Lenský	Lenský	k1gMnSc1	Lenský
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ho	on	k3xPp3gNnSc4	on
seznámí	seznámit	k5eAaPmIp3nS	seznámit
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
Larinů	Larin	k1gInPc2	Larin
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
mu	on	k3xPp3gMnSc3	on
totiž	totiž	k9	totiž
představit	představit	k5eAaPmF	představit
svou	svůj	k3xOyFgFnSc4	svůj
milou	milý	k2eAgFnSc4d1	Milá
nastávající	nastávající	k1gFnSc4	nastávající
<g/>
,	,	kIx,	,
Olgu	Olga	k1gFnSc4	Olga
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
potká	potkat	k5eAaPmIp3nS	potkat
Oněgin	Oněgin	k1gMnSc1	Oněgin
Taťánu	Taťána	k1gFnSc4	Taťána
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
osudově	osudově	k6eAd1	osudově
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Taťána	Taťána	k1gFnSc1	Taťána
mu	on	k3xPp3gMnSc3	on
napíše	napsat	k5eAaBmIp3nS	napsat
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Oněgin	Oněgin	k1gInSc1	Oněgin
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
potkají	potkat	k5eAaPmIp3nP	potkat
a	a	k8xC	a
Oněgin	Oněgina	k1gFnPc2	Oněgina
Taťáně	Taťána	k1gFnSc3	Taťána
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
časem	časem	k6eAd1	časem
by	by	kYmCp3nS	by
jeho	jeho	k3xOp3gFnSc1	jeho
láska	láska	k1gFnSc1	láska
opadla	opadnout	k5eAaPmAgFnS	opadnout
a	a	k8xC	a
manželství	manželství	k1gNnSc1	manželství
by	by	kYmCp3nS	by
nebylo	být	k5eNaImAgNnS	být
šťastné	šťastný	k2eAgNnSc1d1	šťastné
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
Lenským	Lenský	k1gMnSc7	Lenský
pozván	pozván	k2eAgMnSc1d1	pozván
k	k	k7c3	k
Larinovým	Larinová	k1gFnPc3	Larinová
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
Taťánina	Taťánin	k2eAgInSc2d1	Taťánin
svátku	svátek	k1gInSc2	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
večírku	večírek	k1gInSc2	večírek
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
otrávený	otrávený	k2eAgMnSc1d1	otrávený
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
svůj	svůj	k3xOyFgInSc4	svůj
vzdor	vzdor	k1gInSc4	vzdor
nešťastným	šťastný	k2eNgInSc7d1	nešťastný
způsobem	způsob	k1gInSc7	způsob
–	–	k?	–
celý	celý	k2eAgInSc1d1	celý
večer	večer	k1gInSc1	večer
tančí	tančit	k5eAaImIp3nS	tančit
s	s	k7c7	s
Olgou	Olga	k1gFnSc7	Olga
<g/>
.	.	kIx.	.
</s>
<s>
Rozhněvaný	rozhněvaný	k2eAgMnSc1d1	rozhněvaný
Lenský	Lenský	k1gMnSc1	Lenský
vyzve	vyzvat	k5eAaPmIp3nS	vyzvat
Oněgina	Oněgina	k1gMnSc1	Oněgina
na	na	k7c4	na
souboj	souboj	k1gInSc4	souboj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
je	být	k5eAaImIp3nS	být
Lenský	Lenský	k1gMnSc1	Lenský
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
nešťastné	šťastný	k2eNgFnSc6d1	nešťastná
smrti	smrt	k1gFnSc6	smrt
bývalého	bývalý	k2eAgMnSc2d1	bývalý
přítele	přítel	k1gMnSc2	přítel
Oněgin	Oněgin	k1gMnSc1	Oněgin
uniká	unikat	k5eAaImIp3nS	unikat
<g/>
,	,	kIx,	,
putuje	putovat	k5eAaImIp3nS	putovat
po	po	k7c6	po
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
zamíří	zamířit	k5eAaPmIp3nS	zamířit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
(	(	kIx(	(
<g/>
po	po	k7c6	po
6	[number]	k4	6
letech	let	k1gInPc6	let
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
potká	potkat	k5eAaPmIp3nS	potkat
Taťánu	Taťána	k1gFnSc4	Taťána
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
již	již	k9	již
manželku	manželka	k1gFnSc4	manželka
významného	významný	k2eAgMnSc2d1	významný
šlechtice	šlechtic	k1gMnSc2	šlechtic
(	(	kIx(	(
<g/>
Oněginova	Oněginův	k2eAgMnSc2d1	Oněginův
bratrance	bratranec	k1gMnSc2	bratranec
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
vůdčích	vůdčí	k2eAgFnPc2d1	vůdčí
osobností	osobnost	k1gFnPc2	osobnost
společenské	společenský	k2eAgFnSc2d1	společenská
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
chladná	chladný	k2eAgFnSc1d1	chladná
zklamaná	zklamaný	k2eAgFnSc1d1	zklamaná
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
dvoří	dvořit	k5eAaImIp3nS	dvořit
Oněgin	Oněgin	k1gInSc1	Oněgin
jí	on	k3xPp3gFnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Napíše	napsat	k5eAaBmIp3nS	napsat
jí	on	k3xPp3gFnSc3	on
několik	několik	k4yIc1	několik
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
bez	bez	k7c2	bez
odpovědi	odpověď	k1gFnSc2	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Taťána	Taťána	k1gFnSc1	Taťána
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
jej	on	k3xPp3gMnSc4	on
dosud	dosud	k6eAd1	dosud
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
,	,	kIx,	,
odmítá	odmítat	k5eAaImIp3nS	odmítat
jeho	jeho	k3xOp3gInPc4	jeho
city	cit	k1gInPc4	cit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vdaná	vdaný	k2eAgFnSc1d1	vdaná
za	za	k7c2	za
jiného	jiný	k2eAgMnSc2d1	jiný
a	a	k8xC	a
tomu	ten	k3xDgNnSc3	ten
chce	chtít	k5eAaImIp3nS	chtít
zůstat	zůstat	k5eAaPmF	zůstat
věrná	věrný	k2eAgFnSc1d1	věrná
<g/>
.	.	kIx.	.
</s>
<s>
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
je	být	k5eAaImIp3nS	být
vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
Puškinovo	Puškinův	k2eAgNnSc4d1	Puškinovo
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
použita	použít	k5eAaPmNgFnS	použít
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
oněginská	oněginský	k2eAgFnSc1d1	oněginský
sloka	sloka	k1gFnSc1	sloka
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgFnSc1d1	skládající
ze	z	k7c2	z
14	[number]	k4	14
veršů	verš	k1gInPc2	verš
<g/>
.	.	kIx.	.
</s>
<s>
Oněgin	Oněgin	k1gInSc1	Oněgin
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
ukázkou	ukázka	k1gFnSc7	ukázka
zbytečného	zbytečný	k2eAgMnSc2d1	zbytečný
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
cynika	cynik	k1gMnSc2	cynik
s	s	k7c7	s
pokřiveným	pokřivený	k2eAgInSc7d1	pokřivený
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yIgInSc2	který
je	být	k5eAaImIp3nS	být
odtržen	odtržen	k2eAgMnSc1d1	odtržen
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
protikladem	protiklad	k1gInSc7	protiklad
je	být	k5eAaImIp3nS	být
citově	citově	k6eAd1	citově
založená	založený	k2eAgFnSc1d1	založená
a	a	k8xC	a
naivní	naivní	k2eAgFnSc1d1	naivní
Taťána	Taťána	k1gFnSc1	Taťána
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
sama	sám	k3xTgFnSc1	sám
nestane	stanout	k5eNaPmIp3nS	stanout
chladnou	chladný	k2eAgFnSc4d1	chladná
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
předtím	předtím	k6eAd1	předtím
stal	stát	k5eAaPmAgMnS	stát
i	i	k9	i
Oněgin	Oněgin	k1gMnSc1	Oněgin
<g/>
.	.	kIx.	.
</s>
<s>
Taťánin	Taťánin	k2eAgInSc1d1	Taťánin
dopis	dopis	k1gInSc1	dopis
Oněginovi	Oněgin	k1gMnSc3	Oněgin
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Hora	Hora	k1gMnSc1	Hora
<g/>
)	)	kIx)	)
Podle	podle	k7c2	podle
románu	román	k1gInSc2	román
složil	složit	k5eAaPmAgMnS	složit
ruský	ruský	k2eAgMnSc1d1	ruský
skladatel	skladatel	k1gMnSc1	skladatel
Petr	Petr	k1gMnSc1	Petr
Iljič	Iljič	k1gMnSc1	Iljič
Čajkovskij	Čajkovskij	k1gMnSc1	Čajkovskij
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
operu	opera	k1gFnSc4	opera
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
<g/>
.	.	kIx.	.
</s>
<s>
Obsáhlou	obsáhlý	k2eAgFnSc4d1	obsáhlá
scénickou	scénický	k2eAgFnSc4d1	scénická
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
dramatizaci	dramatizace	k1gFnSc3	dramatizace
románu	román	k1gInSc2	román
napsal	napsat	k5eAaPmAgMnS	napsat
Sergej	Sergej	k1gMnSc1	Sergej
Prokofjev	Prokofjev	k1gMnSc1	Prokofjev
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Anfas	Anfas	k1gMnSc1	Anfas
Divadlo	divadlo	k1gNnSc1	divadlo
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
Petra	Petr	k1gMnSc2	Petr
Bezruče	Bezruč	k1gFnSc2	Bezruč
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
<g />
.	.	kIx.	.
</s>
<s>
ABC	ABC	kA	ABC
<g/>
)	)	kIx)	)
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
Е	Е	k?	Е
О	О	k?	О
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Vasilij	Vasilij	k1gMnSc1	Vasilij
Gončarov	Gončarov	k1gInSc1	Gončarov
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
Е	Е	k?	Е
<g />
.	.	kIx.	.
</s>
<s>
О	О	k?	О
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Roman	Roman	k1gMnSc1	Roman
Tichomirov	Tichomirov	k1gInSc1	Tichomirov
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
Evgeniy	Evgenia	k1gFnPc1	Evgenia
Onegin	Onegin	k1gInSc1	Onegin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Kirk	Kirk	k1gMnSc1	Kirk
Browning	browning	k1gInSc1	browning
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgNnSc4d1	televizní
zpracování	zpracování	k1gNnSc4	zpracování
Čajkovského	Čajkovský	k2eAgInSc2d1	Čajkovský
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
originále	originál	k1gInSc6	originál
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
Eugene	Eugen	k1gInSc5	Eugen
Onegin	Onegin	k2eAgMnSc1d1	Onegin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Petr	Petr	k1gMnSc1	Petr
Weigl	Weigl	k1gMnSc1	Weigl
<g/>
,	,	kIx,	,
filmové	filmový	k2eAgNnSc1d1	filmové
zpracování	zpracování	k1gNnSc1	zpracování
Čajkovského	Čajkovský	k2eAgInSc2d1	Čajkovský
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
originále	originál	k1gInSc6	originál
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
Yevgeny	Yevgen	k1gInPc4	Yevgen
<g />
.	.	kIx.	.
</s>
<s>
Onyegin	Onyegin	k1gInSc1	Onyegin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Humphrey	Humphrea	k1gFnSc2	Humphrea
Burton	Burton	k1gInSc4	Burton
televizní	televizní	k2eAgNnSc4d1	televizní
zpracování	zpracování	k1gNnSc4	zpracování
Čajkovského	Čajkovský	k2eAgInSc2d1	Čajkovský
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
originále	originál	k1gInSc6	originál
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
Onegin	Onegin	k1gInSc1	Onegin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Martha	Martha	k1gFnSc1	Martha
Fiennesová	Fiennesový	k2eAgFnSc1d1	Fiennesová
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gInSc1	Oněgin
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
Eugè	Eugè	k1gMnSc5	Eugè
Onéguine	Onéguin	k1gMnSc5	Onéguin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Don	Don	k1gMnSc1	Don
Kent	Kent	k1gMnSc1	Kent
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgNnSc1d1	televizní
zpracování	zpracování	k1gNnSc1	zpracování
Čajkovského	Čajkovský	k2eAgInSc2d1	Čajkovský
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
originále	originál	k1gInSc6	originál
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
Eugen	Eugen	k2eAgInSc1d1	Eugen
Onegin	Onegin	k1gInSc1	Onegin
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Brian	Brian	k1gMnSc1	Brian
Large	Large	k1gInSc1	Large
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgNnSc4d1	televizní
zpracování	zpracování	k1gNnSc4	zpracování
Čajkovského	Čajkovský	k2eAgInSc2d1	Čajkovský
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
originále	originál	k1gInSc6	originál
Eugen	Eugen	k2eAgMnSc1d1	Eugen
Oněgin	Oněgin	k1gMnSc1	Oněgin
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Vetterle	Vetterle	k1gFnSc1	Vetterle
<g/>
,	,	kIx,	,
Písek	Písek	k1gInSc1	Písek
1860	[number]	k4	1860
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Václav	Václav	k1gMnSc1	Václav
Čeněk	Čeněk	k1gMnSc1	Čeněk
Bendl-Stránický	Bendl-Stránický	k2eAgMnSc1d1	Bendl-Stránický
<g/>
,	,	kIx,	,
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Václav	Václav	k1gMnSc1	Václav
Alois	Alois	k1gMnSc1	Alois
Jung	Jung	k1gMnSc1	Jung
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
a	a	k8xC	a
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Eugen	Eugen	k2eAgInSc1d1	Eugen
Oněgin	Oněgin	k1gInSc1	Oněgin
<g/>
,	,	kIx,	,
E.	E.	kA	E.
K.	K.	kA	K.
Rosendorf	Rosendorf	k1gMnSc1	Rosendorf
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Václav	Václav	k1gMnSc1	Václav
Čeněk	Čeněk	k1gMnSc1	Čeněk
Bendl-Stránický	Bendl-Stránický	k2eAgMnSc1d1	Bendl-Stránický
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Jevgenij	Jevgenít	k5eAaPmRp2nS	Jevgenít
Oněgin	Oněgin	k1gMnSc1	Oněgin
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Topič	topič	k1gMnSc1	topič
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jindřich	Jindřich	k1gMnSc1	Jindřich
Najman	Najman	k1gMnSc1	Najman
<g/>
,	,	kIx,	,
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
<g/>
,	,	kIx,	,
Sfinx	sfinx	k1gFnSc1	sfinx
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Václav	Václav	k1gMnSc1	Václav
Alois	Alois	k1gMnSc1	Alois
Jung	Jung	k1gMnSc1	Jung
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc4d1	poslední
desátý	desátý	k4xOgInSc4	desátý
zpěv	zpěv	k1gInSc4	zpěv
přeložil	přeložit	k5eAaPmAgMnS	přeložit
po	po	k7c4	po
prvé	prvý	k4xOgFnPc4	prvý
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
Bohumil	Bohumil	k1gMnSc1	Bohumil
Mathesius	Mathesius	k1gMnSc1	Mathesius
<g/>
,	,	kIx,	,
Eugen	Eugen	k2eAgMnSc1d1	Eugen
Oněgin	Oněgin	k1gMnSc1	Oněgin
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Hora	Hora	k1gMnSc1	Hora
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1940	[number]	k4	1940
a	a	k8xC	a
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
Výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
II	II	kA	II
<g/>
.	.	kIx.	.
-	-	kIx~	-
Eugen	Eugen	k2eAgMnSc1d1	Eugen
Oněgin	Oněgin	k1gMnSc1	Oněgin
<g/>
,	,	kIx,	,
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Hora	Hora	k1gMnSc1	Hora
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc4d1	poslední
desátý	desátý	k4xOgInSc4	desátý
zpěv	zpěv	k1gInSc4	zpěv
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohumil	Bohumil	k1gMnSc1	Bohumil
Mathesius	Mathesius	k1gMnSc1	Mathesius
<g/>
,	,	kIx,	,
Eugen	Eugen	k2eAgMnSc1d1	Eugen
<g />
.	.	kIx.	.
</s>
<s>
Oněgin	Oněgin	k1gMnSc1	Oněgin
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Hora	Hora	k1gMnSc1	Hora
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc4d1	poslední
desátý	desátý	k4xOgInSc4	desátý
zpěv	zpěv	k1gInSc4	zpěv
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Bohumil	Bohumil	k1gMnSc1	Bohumil
Mathesius	Mathesius	k1gMnSc1	Mathesius
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
a	a	k8xC	a
1962	[number]	k4	1962
a	a	k8xC	a
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
<g/>
,	,	kIx,	,
Svět	svět	k1gInSc1	svět
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Olga	Olga	k1gFnSc1	Olga
Mašková	Mašková	k1gFnSc1	Mašková
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Lidové	lidový	k2eAgNnSc1d1	lidové
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1987	[number]	k4	1987
a	a	k8xC	a
Levné	levný	k2eAgFnPc1d1	levná
knihy	kniha	k1gFnPc1	kniha
KMa	KMa	k1gFnSc1	KMa
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Eugen	Eugen	k2eAgInSc1d1	Eugen
Oněgin	Oněgin	k1gInSc1	Oněgin
<g/>
,	,	kIx,	,
Lidové	lidový	k2eAgNnSc1d1	lidové
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Hora	Hora	k1gMnSc1	Hora
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
<g/>
,	,	kIx,	,
Romeo	Romeo	k1gMnSc1	Romeo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Milan	Milan	k1gMnSc1	Milan
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
MACURA	Macura	k1gMnSc1	Macura
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc4	slovník
světových	světový	k2eAgNnPc2d1	světové
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
2	[number]	k4	2
<g/>
/	/	kIx~	/
M-	M-	k1gMnPc2	M-
<g/>
Ž.	Ž.	kA	Ž.
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
960	[number]	k4	960
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
459	[number]	k4	459
<g/>
.	.	kIx.	.
</s>
<s>
PUŠKIN	PUŠKIN	kA	PUŠKIN
<g/>
,	,	kIx,	,
Aleksandr	Aleksandr	k1gInSc1	Aleksandr
Sergejevič	Sergejevič	k1gInSc1	Sergejevič
<g/>
.	.	kIx.	.
</s>
<s>
Evgen	Evgen	k2eAgInSc1d1	Evgen
Oněgin	Oněgin	k1gInSc1	Oněgin
:	:	kIx,	:
román	román	k1gInSc1	román
ve	v	k7c6	v
verších	verš	k1gInPc6	verš
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Čeněk	Čeněk	k1gMnSc1	Čeněk
Bendl-Stránický	Bendl-Stránický	k2eAgMnSc1d1	Bendl-Stránický
<g/>
;	;	kIx,	;
ilustrace	ilustrace	k1gFnSc1	ilustrace
Josef	Josef	k1gMnSc1	Josef
Wenig	Wenig	k1gMnSc1	Wenig
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
E.K.	E.K.	k1gMnSc1	E.K.
Rosendorf	Rosendorf	k1gMnSc1	Rosendorf
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
149	[number]	k4	149
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k2eAgMnSc1d1	Oněgin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Rozbor	rozbor	k1gInSc1	rozbor
románu	román	k1gInSc2	román
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
-	-	kIx~	-
prof.	prof.	kA	prof.
Vladimír	Vladimír	k1gMnSc1	Vladimír
Svatoň	Svatoň	k1gMnSc1	Svatoň
(	(	kIx(	(
<g/>
YouTube	YouTub	k1gInSc5	YouTub
<g/>
)	)	kIx)	)
</s>
