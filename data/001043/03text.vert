<s>
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
(	(	kIx(	(
[	[	kIx(	[
<g/>
roʊ	roʊ	k?	roʊ
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
State	status	k1gInSc5	status
of	of	k?	of
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
and	and	k?	and
Providence	providence	k1gFnSc2	providence
Plantations	Plantationsa	k1gFnPc2	Plantationsa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
v	v	k7c4	v
oblasti	oblast	k1gFnPc4	oblast
Nové	Nové	k2eAgFnSc2d1	Nové
Anglie	Anglie	k1gFnSc2	Anglie
severovýchodního	severovýchodní	k2eAgInSc2d1	severovýchodní
regionu	region	k1gInSc2	region
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
hraničí	hraničit	k5eAaImIp3nP	hraničit
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Connecticutem	Connecticut	k1gInSc7	Connecticut
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Massachusetts	Massachusetts	k1gNnSc7	Massachusetts
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgNnSc1d1	jižní
ohraničení	ohraničení	k1gNnSc1	ohraničení
státu	stát	k1gInSc2	stát
tvoří	tvořit	k5eAaImIp3nS	tvořit
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
3140	[number]	k4	3140
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc4	Island
nejmenším	malý	k2eAgInSc7d3	nejmenší
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
1,1	[number]	k4	1,1
milionu	milion	k4xCgInSc2	milion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
43	[number]	k4	43
<g/>
.	.	kIx.	.
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
hustoty	hustota	k1gFnSc2	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
388	[number]	k4	388
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
však	však	k9	však
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
a	a	k8xC	a
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Providence	providence	k1gFnSc2	providence
se	s	k7c7	s
180	[number]	k4	180
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
největšími	veliký	k2eAgMnPc7d3	veliký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Warwick	Warwick	k1gInSc4	Warwick
s	s	k7c7	s
80	[number]	k4	80
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
Cranston	Cranston	k1gInSc1	Cranston
(	(	kIx(	(
<g/>
80	[number]	k4	80
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pawtucket	Pawtucket	k1gMnSc1	Pawtucket
(	(	kIx(	(
<g/>
70	[number]	k4	70
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rhode	Rhodos	k1gInSc5	Rhodos
Islandu	Island	k1gInSc2	Island
patří	patřit	k5eAaImIp3nS	patřit
64	[number]	k4	64
km	km	kA	km
pobřeží	pobřeží	k1gNnSc1	pobřeží
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
Narragansett	Narragansett	k1gInSc1	Narragansett
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
mimo	mimo	k6eAd1	mimo
jiných	jiný	k2eAgInPc6d1	jiný
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
největší	veliký	k2eAgInSc4d3	veliký
ostrov	ostrov	k1gInSc4	ostrov
státu	stát	k1gInSc2	stát
Aquidneck	Aquidneck	k1gInSc1	Aquidneck
Island	Island	k1gInSc1	Island
<g/>
;	;	kIx,	;
Asi	asi	k9	asi
20	[number]	k4	20
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
leží	ležet	k5eAaImIp3nP	ležet
ostrov	ostrov	k1gInSc4	ostrov
Block	Block	k1gInSc1	Block
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Jerimoth	Jerimoth	k1gMnSc1	Jerimoth
Hill	Hill	k1gMnSc1	Hill
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
247	[number]	k4	247
m.	m.	k?	m.
Největšími	veliký	k2eAgInPc7d3	veliký
toky	tok	k1gInPc7	tok
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
Pawcatuck	Pawcatucko	k1gNnPc2	Pawcatucko
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
se	s	k7c7	s
státem	stát	k1gInSc7	stát
Connecticut	Connecticut	k1gMnSc1	Connecticut
<g/>
,	,	kIx,	,
a	a	k8xC	a
přílivová	přílivový	k2eAgFnSc1d1	přílivová
řeka	řeka	k1gFnSc1	řeka
Providence	providence	k1gFnSc2	providence
<g/>
.	.	kIx.	.
</s>
<s>
Anglickou	anglický	k2eAgFnSc4d1	anglická
kolonii	kolonie	k1gFnSc4	kolonie
Providence	providence	k1gFnSc2	providence
Plantations	Plantations	k1gInSc1	Plantations
<g/>
,	,	kIx,	,
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
Providence	providence	k1gFnSc2	providence
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1636	[number]	k4	1636
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
kazatel	kazatel	k1gMnSc1	kazatel
Roger	Rogra	k1gFnPc2	Rogra
Williams	Williams	k1gInSc1	Williams
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
vypovězen	vypovědět	k5eAaPmNgInS	vypovědět
ze	z	k7c2	z
sousední	sousední	k2eAgFnSc2d1	sousední
massachusettské	massachusettský	k2eAgFnSc2d1	Massachusettská
kolonie	kolonie	k1gFnSc2	kolonie
kvůli	kvůli	k7c3	kvůli
náboženským	náboženský	k2eAgInPc3d1	náboženský
sporům	spor	k1gInPc3	spor
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
nové	nový	k2eAgFnSc2d1	nová
kolonie	kolonie	k1gFnSc2	kolonie
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
označovaný	označovaný	k2eAgInSc4d1	označovaný
jako	jako	k8xC	jako
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
obecně	obecně	k6eAd1	obecně
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
Aquidneck	Aquidneck	k1gMnSc1	Aquidneck
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
<s>
Provincie	provincie	k1gFnSc1	provincie
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
třinácti	třináct	k4xCc2	třináct
zakládajících	zakládající	k2eAgInPc2d1	zakládající
států	stát	k1gInPc2	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
jako	jako	k9	jako
třináctý	třináctý	k4xOgMnSc1	třináctý
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
stát	stát	k5eAaImF	stát
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
ratifikoval	ratifikovat	k5eAaBmAgInS	ratifikovat
Ústavu	ústava	k1gFnSc4	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
došlo	dojít	k5eAaPmAgNnS	dojít
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1790	[number]	k4	1790
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc4	Island
sídlí	sídlet	k5eAaImIp3nS	sídlet
Brownova	Brownův	k2eAgFnSc1d1	Brownova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
1	[number]	k4	1
052	[number]	k4	052
567	[number]	k4	567
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
81,4	[number]	k4	81,4
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
5,7	[number]	k4	5,7
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,6	[number]	k4	0,6
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
2,9	[number]	k4	2,9
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
6,0	[number]	k4	6,0
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
3,3	[number]	k4	3,3
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
<g />
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgInSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
12,4	[number]	k4	12,4
<g/>
%	%	kIx~	%
populace	populace	k1gFnPc1	populace
<g/>
.	.	kIx.	.
křesťané	křesťan	k1gMnPc1	křesťan
-	-	kIx~	-
87,5	[number]	k4	87,5
<g/>
%	%	kIx~	%
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
-	-	kIx~	-
63.6	[number]	k4	63.6
<g/>
%	%	kIx~	%
protestanté	protestant	k1gMnPc1	protestant
-	-	kIx~	-
21,6	[number]	k4	21,6
<g/>
%	%	kIx~	%
episkopální	episkopální	k2eAgFnSc2d1	episkopální
církve	církev	k1gFnSc2	církev
-	-	kIx~	-
8,1	[number]	k4	8,1
<g/>
%	%	kIx~	%
baptisté	baptista	k1gMnPc1	baptista
-	-	kIx~	-
6,3	[number]	k4	6,3
<g/>
%	%	kIx~	%
Evangelikálové	Evangelikálový	k2eAgFnSc2d1	Evangelikálový
-	-	kIx~	-
4	[number]	k4	4
<g />
.	.	kIx.	.
</s>
<s>
<g/>
%	%	kIx~	%
jiní	jiný	k2eAgMnPc1d1	jiný
-	-	kIx~	-
3,2	[number]	k4	3,2
<g/>
%	%	kIx~	%
jiní	jiný	k2eAgMnPc1d1	jiný
křesťané	křesťan	k1gMnPc1	křesťan
-	-	kIx~	-
2,3	[number]	k4	2,3
<g/>
%	%	kIx~	%
židé	žid	k1gMnPc1	žid
-	-	kIx~	-
1,4	[number]	k4	1,4
<g/>
%	%	kIx~	%
muslimové	muslim	k1gMnPc1	muslim
-	-	kIx~	-
1,2	[number]	k4	1,2
<g/>
%	%	kIx~	%
jiná	jiný	k2eAgNnPc1d1	jiné
náboženství	náboženství	k1gNnPc1	náboženství
-	-	kIx~	-
1,9	[number]	k4	1,9
<g/>
%	%	kIx~	%
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
-	-	kIx~	-
6	[number]	k4	6
<g/>
%	%	kIx~	%
Založen	založit	k5eAaPmNgInS	založit
jako	jako	k8xC	jako
britská	britský	k2eAgFnSc1d1	britská
kolonie	kolonie	k1gFnSc1	kolonie
Rogerem	Roger	k1gMnSc7	Roger
Williamsem	Williams	k1gMnSc7	Williams
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
původně	původně	k6eAd1	původně
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
duchovní	duchovní	k1gMnSc1	duchovní
v	v	k7c6	v
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
spory	spor	k1gInPc4	spor
s	s	k7c7	s
tehdejšími	tehdejší	k2eAgMnPc7d1	tehdejší
duchovními	duchovní	k1gMnPc7	duchovní
a	a	k8xC	a
politiky	politik	k1gMnPc7	politik
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
odtržení	odtržení	k1gNnSc4	odtržení
církve	církev	k1gFnSc2	církev
od	od	k7c2	od
státu	stát	k1gInSc2	stát
a	a	k8xC	a
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
očistu	očista	k1gFnSc4	očista
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
hrozilo	hrozit	k5eAaImAgNnS	hrozit
vypovězení	vypovězení	k1gNnSc4	vypovězení
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Indiánů	Indián	k1gMnPc2	Indián
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
divočiny	divočina	k1gFnSc2	divočina
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1636	[number]	k4	1636
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
koupil	koupit	k5eAaPmAgMnS	koupit
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
založil	založit	k5eAaPmAgMnS	založit
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
britskou	britský	k2eAgFnSc4d1	britská
kolonii	kolonie	k1gFnSc4	kolonie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
spoluzakládající	spoluzakládající	k2eAgInSc1d1	spoluzakládající
stát	stát	k1gInSc1	stát
USA	USA	kA	USA
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
<s>
Williams	Williams	k6eAd1	Williams
založil	založit	k5eAaPmAgMnS	založit
také	také	k9	také
Providence	providence	k1gFnPc4	providence
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
stálou	stálý	k2eAgFnSc4d1	stálá
osadu	osada	k1gFnSc4	osada
Rhode	Rhodos	k1gInSc5	Rhodos
Islandu	Island	k1gInSc2	Island
a	a	k8xC	a
nynější	nynější	k2eAgNnSc4d1	nynější
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc1	první
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
uzákoněna	uzákoněn	k2eAgFnSc1d1	uzákoněna
náboženská	náboženský	k2eAgFnSc1d1	náboženská
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
státu	stát	k1gInSc2	stát
proto	proto	k8xC	proto
proudili	proudit	k5eAaPmAgMnP	proudit
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zastávali	zastávat	k5eAaImAgMnP	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
stát	stát	k1gInSc1	stát
nemá	mít	k5eNaImIp3nS	mít
právo	právo	k1gNnSc4	právo
vměšovat	vměšovat	k5eAaImF	vměšovat
se	se	k3xPyFc4	se
do	do	k7c2	do
jejich	jejich	k3xOp3gFnPc2	jejich
náboženských	náboženský	k2eAgFnPc2d1	náboženská
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
