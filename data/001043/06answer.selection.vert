<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
3140	[number]	k4	3140
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc4	Island
nejmenším	malý	k2eAgInSc7d3	nejmenší
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
1,1	[number]	k4	1,1
milionu	milion	k4xCgInSc2	milion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
43	[number]	k4	43
<g/>
.	.	kIx.	.
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
hustoty	hustota	k1gFnSc2	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
388	[number]	k4	388
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
však	však	k9	však
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
