<s>
Keira	Keira	k1gFnSc1	Keira
Christina	Christin	k1gMnSc2	Christin
Knightley	Knightlea	k1gMnSc2	Knightlea
(	(	kIx(	(
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
anglická	anglický	k2eAgFnSc1d1	anglická
filmová	filmový	k2eAgFnSc1d1	filmová
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
zařazena	zařadit	k5eAaPmNgFnS	zařadit
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
nejlépe	dobře	k6eAd3	dobře
placených	placený	k2eAgFnPc2d1	placená
hereček	herečka	k1gFnPc2	herečka
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
časopis	časopis	k1gInSc1	časopis
Forbes	forbes	k1gInSc4	forbes
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
nejlépe	dobře	k6eAd3	dobře
placenou	placený	k2eAgFnSc4d1	placená
herečku	herečka	k1gFnSc4	herečka
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Hereckou	herecký	k2eAgFnSc4d1	herecká
kariéru	kariéra	k1gFnSc4	kariéra
zahájila	zahájit	k5eAaPmAgFnS	zahájit
již	již	k6eAd1	již
jako	jako	k8xS	jako
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
uznání	uznání	k1gNnSc2	uznání
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
hrála	hrát	k5eAaImAgFnS	hrát
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Blafuj	Blafuj	k1gFnSc1	Blafuj
jako	jako	k8xC	jako
Beckham	Beckham	k1gInSc1	Beckham
<g/>
,	,	kIx,	,
Piráti	pirát	k1gMnPc1	pirát
z	z	k7c2	z
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
:	:	kIx,	:
Prokletí	prokletí	k1gNnSc4	prokletí
Černé	Černé	k2eAgFnSc2d1	Černé
perly	perla	k1gFnSc2	perla
a	a	k8xC	a
Láska	láska	k1gFnSc1	láska
nebeská	nebeský	k2eAgFnSc1d1	nebeská
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
několika	několik	k4yIc6	několik
významných	významný	k2eAgInPc6d1	významný
hollywoodských	hollywoodský	k2eAgInPc6d1	hollywoodský
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
i	i	k8xC	i
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
roli	role	k1gFnSc4	role
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
Bennetové	Bennetový	k2eAgFnSc2d1	Bennetová
v	v	k7c6	v
adaptaci	adaptace	k1gFnSc6	adaptace
románu	román	k1gInSc2	román
Jane	Jan	k1gMnSc5	Jan
Austenové	Austenová	k1gFnPc1	Austenová
Pýcha	pýcha	k1gFnSc1	pýcha
a	a	k8xC	a
předsudek	předsudek	k1gInSc1	předsudek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
za	za	k7c4	za
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Pokání	pokání	k1gNnSc2	pokání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
v	v	k7c6	v
akčním	akční	k2eAgInSc6d1	akční
filmu	film	k1gInSc6	film
Jack	Jack	k1gMnSc1	Jack
Ryan	Ryan	k1gMnSc1	Ryan
<g/>
:	:	kIx,	:
V	v	k7c6	v
utajení	utajení	k1gNnSc6	utajení
<g/>
,	,	kIx,	,
v	v	k7c6	v
muzikálovém	muzikálový	k2eAgInSc6d1	muzikálový
filmu	film	k1gInSc6	film
Love	lov	k1gInSc5	lov
Song	song	k1gInSc4	song
a	a	k8xC	a
romantické	romantický	k2eAgFnSc6d1	romantická
komedii	komedie	k1gFnSc6	komedie
Laggies	Laggies	k1gInSc4	Laggies
a	a	k8xC	a
za	za	k7c4	za
roli	role	k1gFnSc4	role
v	v	k7c6	v
historickém	historický	k2eAgInSc6d1	historický
thrilleru	thriller	k1gInSc6	thriller
Kód	kód	k1gInSc1	kód
Enigmy	enigma	k1gFnSc2	enigma
získala	získat	k5eAaPmAgFnS	získat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Zlatý	zlatý	k2eAgInSc4d1	zlatý
Glóbus	glóbus	k1gInSc4	glóbus
<g/>
,	,	kIx,	,
SAG	SAG	kA	SAG
Award	Award	k1gInSc1	Award
<g/>
,	,	kIx,	,
BAFTA	BAFTA	kA	BAFTA
Award	Award	k1gInSc1	Award
a	a	k8xC	a
Oscara	Oscara	k1gFnSc1	Oscara
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
herečka	herečka	k1gFnSc1	herečka
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dcerou	dcera	k1gFnSc7	dcera
Sharman	Sharman	k1gMnSc1	Sharman
MacDonaldové	Macdonaldová	k1gFnSc2	Macdonaldová
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnSc2d1	známá
dramatičky	dramatička	k1gFnSc2	dramatička
<g/>
,	,	kIx,	,
a	a	k8xC	a
Willa	Willa	k1gFnSc1	Willa
Knightleyho	Knightley	k1gMnSc2	Knightley
<g/>
,	,	kIx,	,
divadelního	divadelní	k2eAgMnSc2d1	divadelní
a	a	k8xC	a
televizního	televizní	k2eAgMnSc2d1	televizní
herce	herec	k1gMnSc2	herec
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
anglického	anglický	k2eAgMnSc4d1	anglický
otce	otec	k1gMnSc4	otec
a	a	k8xC	a
skotskou	skotský	k2eAgFnSc4d1	skotská
matku	matka	k1gFnSc4	matka
s	s	k7c7	s
polovinou	polovina	k1gFnSc7	polovina
velšských	velšský	k2eAgInPc2d1	velšský
předků	předek	k1gInPc2	předek
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
staršího	starý	k2eAgMnSc4d2	starší
bratra	bratr	k1gMnSc4	bratr
<g/>
,	,	kIx,	,
Caleba	Caleb	k1gMnSc4	Caleb
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
Richmondu	Richmond	k1gInSc6	Richmond
<g/>
,	,	kIx,	,
navštěvujíc	navštěvovat	k5eAaImSgNnS	navštěvovat
Stanley	Stanley	k1gInPc7	Stanley
Junior	junior	k1gMnSc1	junior
School	School	k1gInSc4	School
<g/>
,	,	kIx,	,
Teddington	Teddington	k1gInSc4	Teddington
School	Schoola	k1gFnPc2	Schoola
a	a	k8xC	a
Esher	Eshra	k1gFnPc2	Eshra
College	Colleg	k1gFnSc2	Colleg
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dyslektička	dyslektička	k1gFnSc1	dyslektička
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
byla	být	k5eAaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
tedy	tedy	k9	tedy
oprávněna	oprávněn	k2eAgFnSc1d1	oprávněna
získat	získat	k5eAaPmF	získat
talentového	talentový	k2eAgMnSc4d1	talentový
agenta	agent	k1gMnSc4	agent
a	a	k8xC	a
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
herecké	herecký	k2eAgFnSc6d1	herecká
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
snila	snít	k5eAaImAgFnS	snít
o	o	k7c6	o
herectví	herectví	k1gNnSc6	herectví
již	již	k6eAd1	již
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
místních	místní	k2eAgFnPc6d1	místní
amatérských	amatérský	k2eAgFnPc6d1	amatérská
inscenacích	inscenace	k1gFnPc6	inscenace
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
Julii	Julie	k1gFnSc6	Julie
(	(	kIx(	(
<g/>
napsala	napsat	k5eAaPmAgFnS	napsat
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
<g/>
)	)	kIx)	)
a	a	k8xC	a
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
(	(	kIx(	(
<g/>
napsal	napsat	k5eAaPmAgMnS	napsat
ji	on	k3xPp3gFnSc4	on
její	její	k3xOp3gMnSc1	její
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
Ian	Ian	k1gMnSc5	Ian
McShane	McShan	k1gMnSc5	McShan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
v	v	k7c6	v
6	[number]	k4	6
letech	léto	k1gNnPc6	léto
získala	získat	k5eAaPmAgFnS	získat
agenta	agent	k1gMnSc4	agent
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
televizních	televizní	k2eAgFnPc6d1	televizní
reklamách	reklama	k1gFnPc6	reklama
a	a	k8xC	a
menších	malý	k2eAgFnPc6d2	menší
televizních	televizní	k2eAgFnPc6d1	televizní
rolích	role	k1gFnPc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
první	první	k4xOgFnSc1	první
role	role	k1gFnSc1	role
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
filmu	film	k1gInSc6	film
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
Royal	Royal	k1gInSc1	Royal
Celebration	Celebration	k1gInSc4	Celebration
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Nevinné	vinný	k2eNgFnSc2d1	nevinná
lži	lež	k1gFnSc2	lež
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Návrat	návrat	k1gInSc1	návrat
domů	dům	k1gInPc2	dům
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc5	The
Treasure	Treasur	k1gMnSc5	Treasur
Seekers	Seekersa	k1gFnPc2	Seekersa
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
roli	role	k1gFnSc4	role
Rose	Ros	k1gMnSc2	Ros
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
Oliver	Oliver	k1gMnSc1	Oliver
Twist	twist	k1gInSc1	twist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
byla	být	k5eAaImAgFnS	být
obsazena	obsadit	k5eAaPmNgFnS	obsadit
do	do	k7c2	do
role	role	k1gFnSc2	role
Sabé	Sabá	k1gFnSc2	Sabá
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Star	Star	kA	Star
Wars	Warsa	k1gFnPc2	Warsa
<g/>
:	:	kIx,	:
Epizoda	epizoda	k1gFnSc1	epizoda
I	i	k9	i
-	-	kIx~	-
Skrytá	skrytý	k2eAgFnSc1d1	skrytá
hrozba	hrozba	k1gFnSc1	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
cderu	cdera	k1gFnSc4	cdera
Robina	robin	k2eAgNnSc2d1	Robino
Hooda	Hoodo	k1gNnSc2	Hoodo
ve	v	k7c6	v
Disneyovském	disneyovský	k2eAgInSc6d1	disneyovský
filmu	film	k1gInSc6	film
Princezna	princezna	k1gFnSc1	princezna
zlodějů	zloděj	k1gMnPc2	zloděj
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Pure	Pur	k1gFnSc2	Pur
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
hrála	hrát	k5eAaImAgFnS	hrát
roli	role	k1gFnSc4	role
těhotné	těhotný	k2eAgFnSc2d1	těhotná
puberťačky	puberťačka	k1gFnSc2	puberťačka
<g/>
,	,	kIx,	,
závislé	závislý	k2eAgFnSc2d1	závislá
na	na	k7c4	na
heroinu	heroina	k1gFnSc4	heroina
<g/>
.	.	kIx.	.
</s>
<s>
Průlomová	průlomový	k2eAgFnSc1d1	průlomová
role	role	k1gFnSc1	role
nastala	nastat	k5eAaPmAgFnS	nastat
s	s	k7c7	s
filmem	film	k1gInSc7	film
Blafuj	Blafuj	k1gFnSc2	Blafuj
jako	jako	k8xC	jako
Beckham	Beckham	k1gInSc1	Beckham
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slavil	slavit	k5eAaImAgInS	slavit
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2007	[number]	k4	2007
po	po	k7c4	po
vydání	vydání	k1gNnSc4	vydání
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
po	po	k7c6	po
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2003	[number]	k4	2003
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
filmu	film	k1gInSc6	film
Blafuj	Blafuj	k1gFnSc2	Blafuj
jako	jako	k8xC	jako
Beckham	Beckham	k1gInSc4	Beckham
získala	získat	k5eAaPmAgFnS	získat
roli	role	k1gFnSc3	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Piráti	pirát	k1gMnPc1	pirát
z	z	k7c2	z
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
:	:	kIx,	:
Prokletí	prokletí	k1gNnSc4	prokletí
Černé	Černé	k2eAgFnSc2d1	Černé
perly	perla	k1gFnSc2	perla
<g/>
,	,	kIx,	,
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Orlanda	Orlando	k1gNnSc2	Orlando
Blooma	Bloom	k1gMnSc2	Bloom
a	a	k8xC	a
Johnnyho	Johnny	k1gMnSc2	Johnny
Deppa	Depp	k1gMnSc2	Depp
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2003	[number]	k4	2003
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
pozitivní	pozitivní	k2eAgFnPc4d1	pozitivní
reakce	reakce	k1gFnPc4	reakce
kritiků	kritik	k1gMnPc2	kritik
a	a	k8xC	a
skvělé	skvělý	k2eAgFnSc2d1	skvělá
tržby	tržba	k1gFnSc2	tržba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2003	[number]	k4	2003
měl	mít	k5eAaImAgMnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
romantické	romantický	k2eAgFnSc2d1	romantická
film	film	k1gInSc1	film
Láska	láska	k1gFnSc1	láska
nebeská	nebeský	k2eAgFnSc1d1	nebeská
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
idolem	idol	k1gInSc7	idol
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
Emmou	Emma	k1gFnSc7	Emma
Thompson	Thompson	k1gMnSc1	Thompson
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
další	další	k2eAgInSc1d1	další
film	film	k1gInSc1	film
Král	Král	k1gMnSc1	Král
Artuš	Artuš	k1gMnSc1	Artuš
získal	získat	k5eAaPmAgMnS	získat
negativní	negativní	k2eAgFnSc4d1	negativní
kritiku	kritika	k1gFnSc4	kritika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
The	The	k1gMnSc1	The
Jacket	Jacket	k1gMnSc1	Jacket
<g/>
,	,	kIx,	,
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Adriena	Adriena	k1gFnSc1	Adriena
Brodyho	Brody	k1gMnSc2	Brody
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
měl	mít	k5eAaImAgMnS	mít
premiéru	premiér	k1gMnSc3	premiér
film	film	k1gInSc4	film
Pýcha	pýcha	k1gFnSc1	pýcha
a	a	k8xC	a
předsudek	předsudek	k1gInSc1	předsudek
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
roli	role	k1gFnSc4	role
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
získala	získat	k5eAaPmAgFnS	získat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Zlatý	zlatý	k2eAgInSc4d1	zlatý
Glóbus	glóbus	k1gInSc4	glóbus
a	a	k8xC	a
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2006	[number]	k4	2006
si	se	k3xPyFc3	se
zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Piráti	pirát	k1gMnPc1	pirát
z	z	k7c2	z
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
:	:	kIx,	:
Truhla	truhla	k1gFnSc1	truhla
mrtvého	mrtvý	k2eAgMnSc2d1	mrtvý
muže	muž	k1gMnSc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
adaptaci	adaptace	k1gFnSc6	adaptace
novely	novela	k1gFnSc2	novela
autora	autor	k1gMnSc2	autor
Alessandra	Alessandra	k1gFnSc1	Alessandra
Baricca	Baricca	k1gFnSc1	Baricca
Hedvábná	hedvábný	k2eAgFnSc1d1	hedvábná
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Pokání	pokání	k1gNnSc2	pokání
a	a	k8xC	a
v	v	k7c6	v
pokračování	pokračování	k1gNnSc6	pokračování
filmu	film	k1gInSc2	film
Piráti	pirát	k1gMnPc1	pirát
z	z	k7c2	z
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
:	:	kIx,	:
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Pokání	pokání	k1gNnSc2	pokání
získala	získat	k5eAaPmAgFnS	získat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Zlatý	zlatý	k2eAgInSc4d1	zlatý
Glóbus	glóbus	k1gInSc4	glóbus
a	a	k8xC	a
BAFTA	BAFTA	kA	BAFTA
Award	Award	k1gInSc1	Award
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Sienny	Sienna	k1gFnSc2	Sienna
Miller	Miller	k1gMnSc1	Miller
<g/>
,	,	kIx,	,
Cillian	Cillian	k1gMnSc1	Cillian
Murphy	Murpha	k1gFnSc2	Murpha
a	a	k8xC	a
Matthewa	Matthewum	k1gNnSc2	Matthewum
Rhyse	Rhyse	k1gFnSc2	Rhyse
ve	v	k7c6	v
válečném	válečný	k2eAgNnSc6d1	válečné
dramatu	drama	k1gNnSc6	drama
Na	na	k7c6	na
hraně	hrana	k1gFnSc6	hrana
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
natočila	natočit	k5eAaBmAgFnS	natočit
film	film	k1gInSc4	film
Vévodkyně	vévodkyně	k1gFnSc2	vévodkyně
<g/>
,	,	kIx,	,
založeném	založený	k2eAgInSc6d1	založený
na	na	k7c6	na
biografii	biografie	k1gFnSc6	biografie
Georgiana	Georgiana	k1gFnSc1	Georgiana
<g/>
,	,	kIx,	,
Duchess	Duchess	k1gInSc1	Duchess
of	of	k?	of
Devonshire	Devonshir	k1gInSc5	Devonshir
od	od	k7c2	od
Amandy	Amanda	k1gFnSc2	Amanda
Foreman	Foreman	k1gMnSc1	Foreman
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
pozitivní	pozitivní	k2eAgFnSc4d1	pozitivní
kritiku	kritika	k1gFnSc4	kritika
a	a	k8xC	a
Keira	Keira	k1gFnSc1	Keira
byla	být	k5eAaImAgFnS	být
za	za	k7c4	za
roli	role	k1gFnSc4	role
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
BIFA	BIFA	kA	BIFA
Award	Award	k1gInSc1	Award
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2009	[number]	k4	2009
debutovala	debutovat	k5eAaBmAgFnS	debutovat
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
<g/>
..	..	k?	..
Ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
roli	role	k1gFnSc4	role
Jennifer	Jennifra	k1gFnPc2	Jennifra
(	(	kIx(	(
<g/>
Celimene	Celimen	k1gInSc5	Celimen
<g/>
)	)	kIx)	)
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
Crimpově	Crimpův	k2eAgFnSc6d1	Crimpův
adaptaci	adaptace	k1gFnSc6	adaptace
Moliè	Moliè	k1gMnSc2	Moliè
Misantropa	misantrop	k1gMnSc2	misantrop
v	v	k7c4	v
Comedy	Comed	k1gMnPc4	Comed
Theatre	Theatr	k1gInSc5	Theatr
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
West	West	k1gInSc1	West
Endu	Endus	k1gInSc6	Endus
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
výkon	výkon	k1gInSc4	výkon
byla	být	k5eAaImAgFnS	být
nominovaná	nominovaný	k2eAgFnSc1d1	nominovaná
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Laurence	Laurence	k1gFnSc2	Laurence
Oliver	Oliver	k1gMnSc1	Oliver
Award	Award	k1gMnSc1	Award
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
romantickém	romantický	k2eAgNnSc6d1	romantické
dramatu	drama	k1gNnSc6	drama
Last	Last	k2eAgInSc4d1	Last
Night	Night	k1gInSc4	Night
<g/>
,	,	kIx,	,
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Evy	Eva	k1gFnSc2	Eva
Mendes	Mendesa	k1gFnPc2	Mendesa
a	a	k8xC	a
Sama	sám	k3xTgFnSc1	sám
Worthingtona	Worthingtona	k1gFnSc1	Worthingtona
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
samý	samý	k3xTgInSc1	samý
rok	rok	k1gInSc1	rok
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
v	v	k7c6	v
adaptaci	adaptace	k1gFnSc6	adaptace
novely	novela	k1gFnSc2	novela
Neopouštěj	opouštět	k5eNaImRp2nS	opouštět
mě	já	k3xPp1nSc2	já
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Colinem	Colin	k1gMnSc7	Colin
Farrellem	Farrell	k1gMnSc7	Farrell
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Londýnský	londýnský	k2eAgMnSc1d1	londýnský
gangster	gangster	k1gMnSc1	gangster
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
na	na	k7c6	na
Filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
ve	v	k7c6	v
Venice	Venika	k1gFnSc6	Venika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Steva	Steve	k1gMnSc2	Steve
Carella	Carell	k1gMnSc2	Carell
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Hledám	hledat	k5eAaImIp1nS	hledat
přítele	přítel	k1gMnSc4	přítel
pro	pro	k7c4	pro
konec	konec	k1gInSc4	konec
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Anna	Anna	k1gFnSc1	Anna
Karenina	Karenina	k1gFnSc1	Karenina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Scarlett	Scarlett	k2eAgInSc4d1	Scarlett
Johansson	Johansson	k1gInSc4	Johansson
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Love	lov	k1gInSc5	lov
Song	song	k1gInSc1	song
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Laggies	Laggies	k1gInSc1	Laggies
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
na	na	k7c6	na
Filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
Sundance	Sundance	k1gFnSc2	Sundance
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
film	film	k1gInSc1	film
Jack	Jack	k1gMnSc1	Jack
Ryan	Ryan	k1gMnSc1	Ryan
<g/>
:	:	kIx,	:
V	v	k7c6	v
utajení	utajení	k1gNnSc6	utajení
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Benedictem	Benedict	k1gMnSc7	Benedict
Cumberbatchem	Cumberbatch	k1gMnSc7	Cumberbatch
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
zahrála	zahrát	k5eAaPmAgFnS	zahrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Kód	kód	k1gInSc1	kód
Enigmy	enigma	k1gFnSc2	enigma
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
roli	role	k1gFnSc4	role
získala	získat	k5eAaPmAgFnS	získat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
herečka	herečka	k1gFnSc1	herečka
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
svůj	svůj	k3xOyFgInSc4	svůj
Broadwayský	broadwayský	k2eAgInSc4d1	broadwayský
debut	debut	k1gInSc4	debut
zažije	zažít	k5eAaPmIp3nS	zažít
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
rolí	role	k1gFnSc7	role
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
adaptaci	adaptace	k1gFnSc6	adaptace
Thérè	Thérè	k1gMnSc1	Thérè
Raquin	Raquin	k1gMnSc1	Raquin
od	od	k7c2	od
Helen	Helena	k1gFnPc2	Helena
Edmunson	Edmunsona	k1gFnPc2	Edmunsona
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
je	být	k5eAaImIp3nS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
na	na	k7c4	na
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c4	za
hudebníka	hudebník	k1gMnSc4	hudebník
z	z	k7c2	z
britské	britský	k2eAgFnSc2d1	britská
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc2	The
Klaxons	Klaxonsa	k1gFnPc2	Klaxonsa
Jamese	Jamese	k1gFnSc1	Jamese
Rightona	Rightona	k1gFnSc1	Rightona
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
Keira	Keira	k1gFnSc1	Keira
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
čeká	čekat	k5eAaImIp3nS	čekat
první	první	k4xOgNnSc4	první
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
porodila	porodit	k5eAaPmAgFnS	porodit
dceru	dcera	k1gFnSc4	dcera
jménem	jméno	k1gNnSc7	jméno
Edie	Edi	k1gInSc2	Edi
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
popřela	popřít	k5eAaPmAgFnS	popřít
zvěsti	zvěst	k1gFnPc4	zvěst
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
anorektička	anorektička	k1gFnSc1	anorektička
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
Daily	Daila	k1gMnSc2	Daila
Mail	mail	k1gInSc4	mail
kvůli	kvůli	k7c3	kvůli
zprávám	zpráva	k1gFnPc3	zpráva
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
poruše	porucha	k1gFnSc6	porucha
příjmu	příjem	k1gInSc2	příjem
potravy	potrava	k1gFnSc2	potrava
zažalovala	zažalovat	k5eAaPmAgFnS	zažalovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2006	[number]	k4	2006
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
workoholičkou	workoholička	k1gFnSc7	workoholička
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Posledních	poslední	k2eAgNnPc2d1	poslední
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
mi	já	k3xPp1nSc3	já
splývá	splývat	k5eAaImIp3nS	splývat
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Nemohu	moct	k5eNaImIp1nS	moct
vám	vy	k3xPp2nPc3	vy
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
loňském	loňský	k2eAgInSc6d1	loňský
roce	rok	k1gInSc6	rok
a	a	k8xC	a
co	co	k3yInSc1	co
rok	rok	k1gInSc4	rok
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
uvedla	uvést	k5eAaPmAgFnS	uvést
a	a	k8xC	a
dodala	dodat	k5eAaPmAgFnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
přepracovaná	přepracovaný	k2eAgFnSc1d1	přepracovaná
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kurzu	kurz	k1gInSc6	kurz
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
nenávidět	nenávidět	k5eAaImF	nenávidět
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
.	.	kIx.	.
</s>
