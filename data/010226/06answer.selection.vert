<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
uváděný	uváděný	k2eAgInSc1d1	uváděný
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
,	,	kIx,	,
v	v	k7c6	v
Měčíně	Měčína	k1gFnSc6	Měčína
je	být	k5eAaImIp3nS	být
gotická	gotický	k2eAgFnSc1d1	gotická
stavba	stavba	k1gFnSc1	stavba
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
z	z	k7c2	z
lomového	lomový	k2eAgInSc2d1	lomový
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
