<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
uváděný	uváděný	k2eAgInSc1d1	uváděný
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
,	,	kIx,	,
v	v	k7c6	v
Měčíně	Měčína	k1gFnSc6	Měčína
je	být	k5eAaImIp3nS	být
gotická	gotický	k2eAgFnSc1d1	gotická
stavba	stavba	k1gFnSc1	stavba
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
z	z	k7c2	z
lomového	lomový	k2eAgInSc2d1	lomový
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
a	a	k8xC	a
kým	kdo	k3yQnSc7	kdo
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Mikuláše	mikuláš	k1gInSc2	mikuláš
postaven	postavit	k5eAaPmNgInS	postavit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nedochovaly	dochovat	k5eNaPmAgFnP	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
chrámu	chrám	k1gInSc6	chrám
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1352	[number]	k4	1352
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
uváděn	uváděn	k2eAgInSc1d1	uváděn
mezi	mezi	k7c7	mezi
katolickými	katolický	k2eAgFnPc7d1	katolická
farami	fara	k1gFnPc7	fara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1776	[number]	k4	1776
–	–	k?	–
1777	[number]	k4	1777
byl	být	k5eAaImAgInS	být
barokně	barokně	k6eAd1	barokně
upraven	upravit	k5eAaPmNgInS	upravit
plzeňským	plzeňský	k2eAgMnSc7d1	plzeňský
stavitelem	stavitel	k1gMnSc7	stavitel
Antonínem	Antonín	k1gMnSc7	Antonín
Barthem	Barth	k1gInSc7	Barth
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Části	část	k1gFnSc6	část
kostela	kostel	k1gInSc2	kostel
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Loď	loď	k1gFnSc4	loď
===	===	k?	===
</s>
</p>
<p>
<s>
Chrámová	chrámový	k2eAgFnSc1d1	chrámová
loď	loď	k1gFnSc1	loď
měla	mít	k5eAaImAgFnS	mít
původně	původně	k6eAd1	původně
obdélníkový	obdélníkový	k2eAgInSc4d1	obdélníkový
tvar	tvar	k1gInSc4	tvar
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
11,70	[number]	k4	11,70
m	m	kA	m
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
<g/>
)	)	kIx)	)
a	a	k8xC	a
8,50	[number]	k4	8,50
m	m	kA	m
(	(	kIx(	(
<g/>
šířka	šířka	k1gFnSc1	šířka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přestavbě	přestavba	k1gFnSc6	přestavba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1776	[number]	k4	1776
byla	být	k5eAaImAgFnS	být
přepažena	přepažit	k5eAaPmNgFnS	přepažit
na	na	k7c4	na
předsíň	předsíň	k1gFnSc4	předsíň
s	s	k7c7	s
hudební	hudební	k2eAgFnSc7d1	hudební
emporou	empora	k1gFnSc7	empora
a	a	k8xC	a
na	na	k7c4	na
čtvercovou	čtvercový	k2eAgFnSc4d1	čtvercová
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
kouty	kout	k1gInPc1	kout
byly	být	k5eAaImAgInP	být
vyplněny	vyplnit	k5eAaPmNgInP	vyplnit
přizdívkou	přizdívka	k1gFnSc7	přizdívka
s	s	k7c7	s
podvojnými	podvojný	k2eAgInPc7d1	podvojný
pilastry	pilastr	k1gInPc7	pilastr
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
o	o	k7c4	o
ně	on	k3xPp3gMnPc4	on
se	se	k3xPyFc4	se
opírají	opírat	k5eAaImIp3nP	opírat
cípy	cíp	k1gInPc1	cíp
plackové	plackový	k2eAgFnSc2d1	Placková
klenby	klenba	k1gFnSc2	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
hudební	hudební	k2eAgFnSc1d1	hudební
empora	empora	k1gFnSc1	empora
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
lodi	loď	k1gFnSc2	loď
jsou	být	k5eAaImIp3nP	být
umístěna	umístěn	k2eAgNnPc1d1	umístěno
velká	velký	k2eAgNnPc1d1	velké
barokně	barokně	k6eAd1	barokně
vykrajovaná	vykrajovaný	k2eAgNnPc1d1	vykrajované
okna	okno	k1gNnPc1	okno
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
opatřena	opatřit	k5eAaPmNgFnS	opatřit
barevnými	barevný	k2eAgFnPc7d1	barevná
skleněnými	skleněný	k2eAgFnPc7d1	skleněná
výplněmi	výplň	k1gFnPc7	výplň
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
opravy	oprava	k1gFnSc2	oprava
chrámu	chrám	k1gInSc2	chrám
v	v	k7c6	v
letech	let	k1gInPc6	let
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1904	[number]	k4	1904
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
pořízeny	pořídit	k5eAaPmNgInP	pořídit
na	na	k7c4	na
náklady	náklad	k1gInPc4	náklad
patrona	patron	k1gMnSc4	patron
rytíře	rytíř	k1gMnSc4	rytíř
Karla	Karel	k1gMnSc4	Karel
z	z	k7c2	z
Weselých	Weselý	k2eAgInPc2d1	Weselý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
gotický	gotický	k2eAgInSc4d1	gotický
původ	původ	k1gInSc4	původ
lodi	loď	k1gFnSc2	loď
upomíná	upomínat	k5eAaImIp3nS	upomínat
portál	portál	k1gInSc1	portál
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
gotické	gotický	k2eAgNnSc1d1	gotické
okno	okno	k1gNnSc1	okno
ukončené	ukončený	k2eAgNnSc1d1	ukončené
lomeným	lomený	k2eAgInSc7d1	lomený
obloukem	oblouk	k1gInSc7	oblouk
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
tyto	tento	k3xDgInPc1	tento
články	článek	k1gInPc1	článek
byly	být	k5eAaImAgInP	být
obnoveny	obnovit	k5eAaPmNgInP	obnovit
za	za	k7c4	za
opravy	oprava	k1gFnPc4	oprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Velká	velký	k2eAgFnSc1d1	velká
věž	věž	k1gFnSc1	věž
===	===	k?	===
</s>
</p>
<p>
<s>
Hranolová	hranolový	k2eAgFnSc1d1	hranolová
věž	věž	k1gFnSc1	věž
s	s	k7c7	s
obyčejným	obyčejný	k2eAgInSc7d1	obyčejný
vchodem	vchod	k1gInSc7	vchod
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
průčelí	průčelí	k1gNnSc6	průčelí
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnPc1	její
barokní	barokní	k2eAgNnPc1d1	barokní
okna	okno	k1gNnPc1	okno
jsou	být	k5eAaImIp3nP	být
opatřena	opatřit	k5eAaPmNgFnS	opatřit
barevnými	barevný	k2eAgFnPc7d1	barevná
výplněmi	výplň	k1gFnPc7	výplň
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
je	být	k5eAaImIp3nS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
zvonovitou	zvonovitý	k2eAgFnSc7d1	zvonovitá
bání	báně	k1gFnSc7	báně
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
je	být	k5eAaImIp3nS	být
připojen	připojit	k5eAaPmNgInS	připojit
schodištní	schodištní	k2eAgInSc1d1	schodištní
přístavek	přístavek	k1gInSc1	přístavek
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
věže	věž	k1gFnSc2	věž
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
tři	tři	k4xCgInPc1	tři
zvony	zvon	k1gInPc1	zvon
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
pořízeny	pořídit	k5eAaPmNgInP	pořídit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
zvony	zvon	k1gInPc1	zvon
byly	být	k5eAaImAgInP	být
předtím	předtím	k6eAd1	předtím
zrekvírovány	zrekvírován	k2eAgInPc1d1	zrekvírován
pro	pro	k7c4	pro
válečné	válečný	k2eAgInPc4d1	válečný
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Triumfální	triumfální	k2eAgInSc1d1	triumfální
oblouk	oblouk	k1gInSc1	oblouk
===	===	k?	===
</s>
</p>
<p>
<s>
Pomocí	pomocí	k7c2	pomocí
triumfálního	triumfální	k2eAgInSc2d1	triumfální
oblouku	oblouk	k1gInSc2	oblouk
je	být	k5eAaImIp3nS	být
oddělena	oddělen	k2eAgFnSc1d1	oddělena
loď	loď	k1gFnSc1	loď
od	od	k7c2	od
presbytáře	presbytář	k1gInSc2	presbytář
<g/>
.	.	kIx.	.
</s>
<s>
Oblouk	oblouk	k1gInSc1	oblouk
má	mít	k5eAaImIp3nS	mít
polokruhový	polokruhový	k2eAgInSc4d1	polokruhový
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
rohy	roh	k1gInPc4	roh
jsou	být	k5eAaImIp3nP	být
obloženy	obložen	k2eAgInPc4d1	obložen
pilastry	pilastr	k1gInPc4	pilastr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Presbytář	presbytář	k1gInSc4	presbytář
===	===	k?	===
</s>
</p>
<p>
<s>
Presbytář	presbytář	k1gInSc1	presbytář
s	s	k7c7	s
opěráky	opěrák	k1gInPc7	opěrák
je	být	k5eAaImIp3nS	být
užší	úzký	k2eAgNnSc1d2	užší
než	než	k8xS	než
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
od	od	k7c2	od
které	který	k3yRgFnSc2	který
je	být	k5eAaImIp3nS	být
oddělen	oddělit	k5eAaPmNgInS	oddělit
triumfálním	triumfální	k2eAgInSc7d1	triumfální
obloukem	oblouk	k1gInSc7	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zaklenut	zaklenut	k2eAgMnSc1d1	zaklenut
jedním	jeden	k4xCgInSc7	jeden
polem	pole	k1gNnSc7	pole
křížové	křížový	k2eAgFnSc2d1	křížová
klenby	klenba	k1gFnSc2	klenba
<g/>
,	,	kIx,	,
hřbety	hřbet	k1gInPc4	hřbet
klenbových	klenbový	k2eAgFnPc2d1	klenbová
kápí	kápě	k1gFnPc2	kápě
jsou	být	k5eAaImIp3nP	být
vodorovné	vodorovný	k2eAgInPc1d1	vodorovný
<g/>
,	,	kIx,	,
klínová	klínový	k2eAgNnPc1d1	klínové
žebra	žebro	k1gNnPc1	žebro
jednoduše	jednoduše	k6eAd1	jednoduše
vykroužená	vykroužený	k2eAgNnPc1d1	vykroužené
<g/>
.	.	kIx.	.
</s>
<s>
Konzoly	konzola	k1gFnPc1	konzola
jsou	být	k5eAaImIp3nP	být
ozdobeny	ozdoben	k2eAgInPc4d1	ozdoben
květovými	květový	k2eAgInPc7d1	květový
a	a	k8xC	a
listovými	listový	k2eAgInPc7d1	listový
tvary	tvar	k1gInPc7	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Presbytář	presbytář	k1gInSc1	presbytář
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
pětiboký	pětiboký	k2eAgInSc1d1	pětiboký
chórový	chórový	k2eAgInSc1d1	chórový
závěr	závěr	k1gInSc1	závěr
(	(	kIx(	(
<g/>
geometricky	geometricky	k6eAd1	geometricky
sestavený	sestavený	k2eAgMnSc1d1	sestavený
z	z	k7c2	z
pěti	pět	k4xCc2	pět
stran	strana	k1gFnPc2	strana
pravidelného	pravidelný	k2eAgInSc2d1	pravidelný
osmiúhelníku	osmiúhelník	k1gInSc2	osmiúhelník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
opravě	oprava	k1gFnSc6	oprava
v	v	k7c6	v
letech	let	k1gInPc6	let
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1904	[number]	k4	1904
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
presbytáři	presbytář	k1gInSc6	presbytář
objeveny	objevit	k5eAaPmNgFnP	objevit
staré	starý	k2eAgFnPc1d1	stará
fresky	freska	k1gFnPc1	freska
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ovšem	ovšem	k9	ovšem
nebyly	být	k5eNaImAgFnP	být
obnoveny	obnovit	k5eAaPmNgInP	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stěně	stěna	k1gFnSc6	stěna
chórového	chórový	k2eAgInSc2d1	chórový
závěru	závěr	k1gInSc2	závěr
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
1,20	[number]	k4	1,20
m	m	kA	m
široký	široký	k2eAgInSc4d1	široký
výklenek	výklenek	k1gInSc4	výklenek
sklenutý	sklenutý	k2eAgInSc4d1	sklenutý
lomeným	lomený	k2eAgInSc7d1	lomený
obloukem	oblouk	k1gInSc7	oblouk
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
sedile	sedile	k1gNnSc2	sedile
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
farář	farář	k1gMnSc1	farář
během	během	k7c2	během
mše	mše	k1gFnSc2	mše
vsedě	vsedě	k6eAd1	vsedě
odpočíval	odpočívat	k5eAaImAgMnS	odpočívat
ve	v	k7c6	v
chvílích	chvíle	k1gFnPc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nekázal	kázat	k5eNaImAgMnS	kázat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
===	===	k?	===
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
byla	být	k5eAaImAgFnS	být
přistavěna	přistavět	k5eAaPmNgFnS	přistavět
k	k	k7c3	k
jižní	jižní	k2eAgFnSc3d1	jižní
straně	strana	k1gFnSc3	strana
lodi	loď	k1gFnSc2	loď
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1726	[number]	k4	1726
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zaklenuta	zaklenout	k5eAaPmNgFnS	zaklenout
křížovou	křížový	k2eAgFnSc7d1	křížová
klenbou	klenba	k1gFnSc7	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
oltáře	oltář	k1gInSc2	oltář
zasvěceného	zasvěcený	k2eAgInSc2d1	zasvěcený
sv.	sv.	kA	sv.
Janu	Jan	k1gMnSc3	Jan
Nepomuckému	Nepomucký	k1gMnSc3	Nepomucký
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
umístěn	umístit	k5eAaPmNgInS	umístit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
nacházely	nacházet	k5eAaImAgFnP	nacházet
dvě	dva	k4xCgFnPc1	dva
menší	malý	k2eAgFnPc1d2	menší
sochy	socha	k1gFnPc1	socha
andělů	anděl	k1gMnPc2	anděl
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
ukradeny	ukrást	k5eAaPmNgInP	ukrást
společně	společně	k6eAd1	společně
s	s	k7c7	s
několika	několik	k4yIc7	několik
svícny	svícen	k1gInPc7	svícen
a	a	k8xC	a
poháry	pohár	k1gInPc7	pohár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sakristie	sakristie	k1gFnSc2	sakristie
===	===	k?	===
</s>
</p>
<p>
<s>
Sakristie	sakristie	k1gFnSc1	sakristie
a	a	k8xC	a
oratoř	oratoř	k1gFnSc1	oratoř
byly	být	k5eAaImAgFnP	být
přistavěny	přistavět	k5eAaPmNgFnP	přistavět
k	k	k7c3	k
severní	severní	k2eAgFnSc3d1	severní
straně	strana	k1gFnSc3	strana
lodi	loď	k1gFnSc2	loď
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1726	[number]	k4	1726
<g/>
.	.	kIx.	.
</s>
<s>
Oratoř	oratoř	k1gFnSc1	oratoř
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
patře	patro	k1gNnSc6	patro
nad	nad	k7c7	nad
sakristií	sakristie	k1gFnSc7	sakristie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Věžička	věžička	k1gFnSc1	věžička
===	===	k?	===
</s>
</p>
<p>
<s>
Nad	nad	k7c7	nad
presbytářem	presbytář	k1gInSc7	presbytář
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
malá	malý	k2eAgFnSc1d1	malá
věžička	věžička	k1gFnSc1	věžička
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
sanktusník	sanktusník	k1gInSc1	sanktusník
<g/>
)	)	kIx)	)
s	s	k7c7	s
cibulovitou	cibulovitý	k2eAgFnSc7d1	cibulovitá
bání	báně	k1gFnSc7	báně
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
umístěny	umístit	k5eAaPmNgInP	umístit
dva	dva	k4xCgInPc1	dva
zvony	zvon	k1gInPc1	zvon
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
18	[number]	k4	18
kg	kg	kA	kg
a	a	k8xC	a
25	[number]	k4	25
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInSc1d2	menší
zvon	zvon	k1gInSc1	zvon
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
1492	[number]	k4	1492
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgMnSc1d1	vysoký
25	[number]	k4	25
cm	cm	kA	cm
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
30	[number]	k4	30
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
umístěn	umístěn	k2eAgInSc1d1	umístěn
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
ave	ave	k1gNnSc1	ave
maria	marius	k1gMnSc2	marius
gracia	gracius	k1gMnSc2	gracius
plena	plena	k1gFnSc1	plena
dominus	dominus	k1gInSc1	dominus
1492	[number]	k4	1492
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInSc1d2	veliký
zvon	zvon	k1gInSc1	zvon
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
zvonů	zvon	k1gInPc2	zvon
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
zvonilo	zvonit	k5eAaImAgNnS	zvonit
pomocí	pomocí	k7c2	pomocí
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
provazu	provaz	k1gInSc2	provaz
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc4	jeden
jeho	jeho	k3xOp3gInSc4	jeho
konec	konec	k1gInSc1	konec
byl	být	k5eAaImAgMnS	být
přivázán	přivázat	k5eAaPmNgMnS	přivázat
ke	k	k7c3	k
zvonům	zvon	k1gInPc3	zvon
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
vedl	vést	k5eAaImAgInS	vést
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
v	v	k7c6	v
presbytáři	presbytář	k1gInSc6	presbytář
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
mohl	moct	k5eAaImAgMnS	moct
manipulovat	manipulovat	k5eAaImF	manipulovat
farář	farář	k1gMnSc1	farář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zařízení	zařízení	k1gNnPc1	zařízení
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
oltáře	oltář	k1gInPc4	oltář
a	a	k8xC	a
obrazy	obraz	k1gInPc4	obraz
==	==	k?	==
</s>
</p>
<p>
<s>
Oltáře	Oltář	k1gInPc1	Oltář
a	a	k8xC	a
ostatní	ostatní	k2eAgNnPc1d1	ostatní
zařízení	zařízení	k1gNnPc1	zařízení
kostela	kostel	k1gInSc2	kostel
jsou	být	k5eAaImIp3nP	být
barokní	barokní	k2eAgInPc1d1	barokní
a	a	k8xC	a
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
hlavního	hlavní	k2eAgInSc2d1	hlavní
oltáře	oltář	k1gInSc2	oltář
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
obraz	obraz	k1gInSc1	obraz
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
,	,	kIx,	,
patrona	patron	k1gMnSc2	patron
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
na	na	k7c6	na
nástavci	nástavec	k1gInSc6	nástavec
nad	nad	k7c7	nad
ním	on	k3xPp3gInSc7	on
obraz	obraz	k1gInSc1	obraz
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Pomocné	pomocný	k2eAgFnSc2d1	pomocná
<g/>
.	.	kIx.	.
</s>
<s>
Celek	celek	k1gInSc1	celek
je	být	k5eAaImIp3nS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
erbem	erb	k1gInSc7	erb
hrabat	hrabě	k1gNnPc2	hrabě
z	z	k7c2	z
Vrtby	vrtba	k1gFnSc2	vrtba
<g/>
.	.	kIx.	.
</s>
<s>
Vlevo	vlevo	k6eAd1	vlevo
a	a	k8xC	a
vpravo	vpravo	k6eAd1	vpravo
od	od	k7c2	od
obrazu	obraz	k1gInSc2	obraz
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
stojí	stát	k5eAaImIp3nP	stát
sochy	socha	k1gFnPc1	socha
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc4	Jan
Křtitele	křtitel	k1gMnSc4	křtitel
a	a	k8xC	a
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
lodi	loď	k1gFnSc2	loď
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
oltář	oltář	k1gInSc1	oltář
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
Božského	božský	k2eAgNnSc2d1	božské
Srdce	srdce	k1gNnSc2	srdce
Páně	páně	k2eAgNnSc2d1	páně
a	a	k8xC	a
kazatelna	kazatelna	k1gFnSc1	kazatelna
<g/>
,	,	kIx,	,
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
oltář	oltář	k1gInSc4	oltář
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
Nejčistšího	čistý	k2eAgNnSc2d3	nejčistší
Srdce	srdce	k1gNnSc2	srdce
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
postranních	postranní	k2eAgInPc6d1	postranní
oltářích	oltář	k1gInPc6	oltář
se	se	k3xPyFc4	se
také	také	k9	také
nacházejí	nacházet	k5eAaImIp3nP	nacházet
menší	malý	k2eAgFnPc1d2	menší
sochy	socha	k1gFnPc1	socha
andělů	anděl	k1gMnPc2	anděl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
barokní	barokní	k2eAgInSc1d1	barokní
oltář	oltář	k1gInSc1	oltář
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
tohoto	tento	k3xDgMnSc2	tento
světce	světec	k1gMnSc2	světec
a	a	k8xC	a
v	v	k7c6	v
nástavci	nástavec	k1gInSc6	nástavec
obraz	obraz	k1gInSc1	obraz
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
lodi	loď	k1gFnSc2	loď
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
dvě	dva	k4xCgFnPc1	dva
zpovědnice	zpovědnice	k1gFnPc1	zpovědnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klenba	klenba	k1gFnSc1	klenba
lodi	loď	k1gFnSc2	loď
je	být	k5eAaImIp3nS	být
vyzdobena	vyzdobit	k5eAaPmNgFnS	vyzdobit
freskovými	freskový	k2eAgFnPc7d1	fresková
malbami	malba	k1gFnPc7	malba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
od	od	k7c2	od
Karla	Karel	k1gMnSc2	Karel
Théra	Thér	k1gMnSc2	Thér
<g/>
,	,	kIx,	,
malíře	malíř	k1gMnSc2	malíř
z	z	k7c2	z
Votic	Votice	k1gFnPc2	Votice
u	u	k7c2	u
Tábora	Tábor	k1gInSc2	Tábor
<g/>
,	,	kIx,	,
a	a	k8xC	a
akademického	akademický	k2eAgMnSc2d1	akademický
malíře	malíř	k1gMnSc2	malíř
Otmara	Otmar	k1gMnSc2	Otmar
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k6eAd1	uprostřed
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
obraz	obraz	k1gInSc1	obraz
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
těšícího	těšící	k2eAgMnSc2d1	těšící
vězně	vězeň	k1gMnSc2	vězeň
v	v	k7c6	v
žaláři	žalář	k1gInSc6	žalář
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozích	roh	k1gInPc6	roh
kolem	kolem	k7c2	kolem
čtyři	čtyři	k4xCgMnPc4	čtyři
sv.	sv.	kA	sv.
Evangelisté	evangelista	k1gMnPc1	evangelista
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
obrazy	obraz	k1gInPc7	obraz
jsou	být	k5eAaImIp3nP	být
citáty	citát	k1gInPc7	citát
z	z	k7c2	z
písma	písmo	k1gNnSc2	písmo
s	s	k7c7	s
květinovou	květinový	k2eAgFnSc7d1	květinová
výzdobou	výzdoba	k1gFnSc7	výzdoba
a	a	k8xC	a
postavy	postav	k1gInPc7	postav
andělů	anděl	k1gMnPc2	anděl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svícny	svícen	k1gInPc1	svícen
jsou	být	k5eAaImIp3nP	být
barokní	barokní	k2eAgInPc1d1	barokní
a	a	k8xC	a
pocházejí	pocházet	k5eAaImIp3nP	pocházet
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
jich	on	k3xPp3gInPc2	on
byla	být	k5eAaImAgFnS	být
ukradena	ukraden	k2eAgFnSc1d1	ukradena
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
svícny	svícen	k1gInPc7	svícen
novějšími	nový	k2eAgInPc7d2	novější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podlaha	podlaha	k1gFnSc1	podlaha
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
vydlážděna	vydláždit	k5eAaPmNgFnS	vydláždit
šamotovými	šamotový	k2eAgFnPc7d1	šamotová
dlaždicemi	dlaždice	k1gFnPc7	dlaždice
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Bárta	Bárta	k1gMnSc1	Bárta
a	a	k8xC	a
Tichý	tichý	k2eAgMnSc1d1	tichý
z	z	k7c2	z
pražských	pražský	k2eAgMnPc2d1	pražský
Hlubočep	Hlubočep	k1gInSc4	Hlubočep
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
hlavního	hlavní	k2eAgInSc2d1	hlavní
oltáře	oltář	k1gInSc2	oltář
jsou	být	k5eAaImIp3nP	být
použity	použit	k2eAgFnPc1d1	použita
dlaždice	dlaždice	k1gFnPc1	dlaždice
s	s	k7c7	s
ozdobnějším	ozdobný	k2eAgInSc7d2	ozdobnější
vzorkem	vzorek	k1gInSc7	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
lavicemi	lavice	k1gFnPc7	lavice
<g/>
,	,	kIx,	,
v	v	k7c6	v
sakristii	sakristie	k1gFnSc6	sakristie
a	a	k8xC	a
oratoři	oratoř	k1gFnSc6	oratoř
je	být	k5eAaImIp3nS	být
položena	položen	k2eAgFnSc1d1	položena
prkenná	prkenný	k2eAgFnSc1d1	prkenná
podlaha	podlaha	k1gFnSc1	podlaha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Park	park	k1gInSc1	park
kolem	kolem	k7c2	kolem
kostela	kostel	k1gInSc2	kostel
==	==	k?	==
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
kostela	kostel	k1gInSc2	kostel
býval	bývat	k5eAaImAgInS	bývat
dříve	dříve	k6eAd2	dříve
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
přestalo	přestat	k5eAaPmAgNnS	přestat
pohřbívat	pohřbívat	k5eAaImF	pohřbívat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
byl	být	k5eAaImAgInS	být
hřbitov	hřbitov	k1gInSc1	hřbitov
definitivně	definitivně	k6eAd1	definitivně
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
přebudován	přebudovat	k5eAaPmNgInS	přebudovat
na	na	k7c4	na
dnešní	dnešní	k2eAgInSc4d1	dnešní
park	park	k1gInSc4	park
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c7	za
kostelem	kostel	k1gInSc7	kostel
se	se	k3xPyFc4	se
v	v	k7c6	v
parku	park	k1gInSc6	park
nachází	nacházet	k5eAaImIp3nS	nacházet
bývalá	bývalý	k2eAgFnSc1d1	bývalá
márnice	márnice	k1gFnSc1	márnice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
skladiště	skladiště	k1gNnSc1	skladiště
stavebního	stavební	k2eAgInSc2d1	stavební
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
uložena	uložen	k2eAgFnSc1d1	uložena
i	i	k8xC	i
větrná	větrný	k2eAgFnSc1d1	větrná
korouhev	korouhev	k1gFnSc1	korouhev
s	s	k7c7	s
postavami	postava	k1gFnPc7	postava
čerta	čert	k1gMnSc2	čert
a	a	k8xC	a
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1990	[number]	k4	1990
shodil	shodit	k5eAaPmAgMnS	shodit
z	z	k7c2	z
báně	báně	k1gFnSc2	báně
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
věži	věž	k1gFnSc6	věž
vítr	vítr	k1gInSc1	vítr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
