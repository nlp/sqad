<s>
Poprava	poprava	k1gFnSc1	poprava
uvařením	uvaření	k1gNnSc7	uvaření
je	být	k5eAaImIp3nS	být
pomalý	pomalý	k2eAgInSc1d1	pomalý
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
bolestivý	bolestivý	k2eAgInSc1d1	bolestivý
způsob	způsob	k1gInSc1	způsob
vykonání	vykonání	k1gNnSc2	vykonání
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Odsouzený	odsouzený	k1gMnSc1	odsouzený
je	být	k5eAaImIp3nS	být
svlečen	svlečen	k2eAgMnSc1d1	svlečen
a	a	k8xC	a
ponořen	ponořen	k2eAgMnSc1d1	ponořen
do	do	k7c2	do
již	již	k6eAd1	již
vařící	vařící	k2eAgFnSc2d1	vařící
kapaliny	kapalina	k1gFnSc2	kapalina
nebo	nebo	k8xC	nebo
svázán	svázán	k2eAgMnSc1d1	svázán
a	a	k8xC	a
ponořen	ponořen	k2eAgMnSc1d1	ponořen
do	do	k7c2	do
velkého	velký	k2eAgInSc2d1	velký
kotle	kotel	k1gInSc2	kotel
se	s	k7c7	s
studenou	studený	k2eAgFnSc7d1	studená
kapalinou	kapalina	k1gFnSc7	kapalina
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yRgNnSc7	který
popravčí	popravčí	k2eAgInPc4d1	popravčí
zapálí	zapálit	k5eAaPmIp3nS	zapálit
oheň	oheň	k1gInSc4	oheň
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zahřeje	zahřát	k5eAaPmIp3nS	zahřát
kapalinu	kapalina	k1gFnSc4	kapalina
k	k	k7c3	k
varu	var	k1gInSc3	var
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
kapalinu	kapalina	k1gFnSc4	kapalina
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
olej	olej	k1gInSc4	olej
<g/>
,	,	kIx,	,
kyselinu	kyselina	k1gFnSc4	kyselina
<g/>
,	,	kIx,	,
dehet	dehet	k1gInSc4	dehet
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
roztavené	roztavený	k2eAgNnSc1d1	roztavené
olovo	olovo	k1gNnSc1	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
smrti	smrt	k1gFnSc2	smrt
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
popáleniny	popálenina	k1gFnSc2	popálenina
<g/>
.	.	kIx.	.
</s>
<s>
Sice	sice	k8xC	sice
nebylo	být	k5eNaImAgNnS	být
tohoto	tento	k3xDgInSc2	tento
trestu	trest	k1gInSc3	trest
užíváno	užíván	k2eAgNnSc1d1	užíváno
tak	tak	k9	tak
často	často	k6eAd1	často
jako	jako	k9	jako
jiných	jiný	k2eAgFnPc2d1	jiná
metod	metoda	k1gFnPc2	metoda
popravy	poprava	k1gFnSc2	poprava
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
ale	ale	k8xC	ale
uvaření	uvaření	k1gNnSc1	uvaření
zaživa	zaživa	k6eAd1	zaživa
bylo	být	k5eAaImAgNnS	být
široce	široko	k6eAd1	široko
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
dvou	dva	k4xCgInPc2	dva
až	až	k9	až
tří	tři	k4xCgInPc2	tři
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poprava	poprava	k1gFnSc1	poprava
uvařením	uvaření	k1gNnSc7	uvaření
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
používala	používat	k5eAaImAgFnS	používat
už	už	k6eAd1	už
v	v	k7c6	v
antice	antika	k1gFnSc6	antika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
bylo	být	k5eAaImAgNnS	být
vaření	vaření	k1gNnSc1	vaření
v	v	k7c6	v
kotli	kotel	k1gInSc6	kotel
používáno	používán	k2eAgNnSc4d1	používáno
jako	jako	k9	jako
způsob	způsob	k1gInSc4	způsob
mučení	mučení	k1gNnPc2	mučení
a	a	k8xC	a
popravy	poprava	k1gFnPc4	poprava
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
pronásledování	pronásledování	k1gNnSc6	pronásledování
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Vít	Vít	k1gMnSc1	Vít
a	a	k8xC	a
apoštol	apoštol	k1gMnSc1	apoštol
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
byli	být	k5eAaImAgMnP	být
podle	podle	k7c2	podle
legend	legenda	k1gFnPc2	legenda
odsouzeni	odsoudit	k5eAaPmNgMnP	odsoudit
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
způsobu	způsob	k1gInSc3	způsob
popravy	poprava	k1gFnSc2	poprava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vaření	vařený	k2eAgMnPc1d1	vařený
zázračně	zázračně	k6eAd1	zázračně
přežili	přežít	k5eAaPmAgMnP	přežít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c6	o
popravě	poprava	k1gFnSc6	poprava
vařením	vaření	k1gNnSc7	vaření
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
,	,	kIx,	,
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
v	v	k7c6	v
období	období	k1gNnSc6	období
vrcholného	vrcholný	k2eAgInSc2d1	vrcholný
a	a	k8xC	a
pozdního	pozdní	k2eAgInSc2d1	pozdní
středověku	středověk	k1gInSc2	středověk
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
bylo	být	k5eAaImAgNnS	být
trestáno	trestán	k2eAgNnSc1d1	trestáno
padělání	padělání	k1gNnSc1	padělání
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
kacířství	kacířství	k1gNnSc4	kacířství
nebo	nebo	k8xC	nebo
travičství	travičství	k1gNnSc4	travičství
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
na	na	k7c6	na
konci	konec	k1gInSc6	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
trest	trest	k1gInSc1	trest
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
traviče	travič	k1gMnPc4	travič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
a	a	k8xC	a
některých	některý	k3yIgFnPc6	některý
německých	německý	k2eAgFnPc6d1	německá
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
vyhrazena	vyhradit	k5eAaPmNgFnS	vyhradit
pro	pro	k7c4	pro
padělatele	padělatel	k1gMnPc4	padělatel
a	a	k8xC	a
penězokazy	penězokaz	k1gMnPc4	penězokaz
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
pozdního	pozdní	k2eAgInSc2d1	pozdní
středověku	středověk	k1gInSc2	středověk
i	i	k8xC	i
raného	raný	k2eAgInSc2d1	raný
novověku	novověk	k1gInSc2	novověk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
Deventer	Deventra	k1gFnPc2	Deventra
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
lze	lze	k6eAd1	lze
ještě	ještě	k6eAd1	ještě
dnes	dnes	k6eAd1	dnes
vidět	vidět	k5eAaImF	vidět
kotle	kotel	k1gInPc4	kotel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vařili	vařit	k5eAaImAgMnP	vařit
popravení	popravený	k2eAgMnPc1d1	popravený
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
trestalo	trestat	k5eAaImAgNnS	trestat
padělání	padělání	k1gNnSc4	padělání
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
verše	verš	k1gInPc1	verš
básníka	básník	k1gMnSc2	básník
Villona	Villon	k1gMnSc2	Villon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
básní	báseň	k1gFnPc2	báseň
napsal	napsat	k5eAaPmAgInS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
..	..	k?	..
<g/>
tak	tak	k6eAd1	tak
mince	mince	k1gFnSc1	mince
šidíš	šidit	k5eAaImIp2nS	šidit
<g/>
,	,	kIx,	,
než	než	k8xS	než
tě	ty	k3xPp2nSc4	ty
kat	kat	k1gMnSc1	kat
pak	pak	k6eAd1	pak
za	za	k7c4	za
trest	trest	k1gInSc4	trest
hodí	hodit	k5eAaPmIp3nS	hodit
v	v	k7c4	v
olej	olej	k1gInSc4	olej
vřelý	vřelý	k2eAgInSc4d1	vřelý
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
poprava	poprava	k1gFnSc1	poprava
vařením	vaření	k1gNnSc7	vaření
používána	používán	k2eAgNnPc4d1	používáno
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
jen	jen	k9	jen
několik	několik	k4yIc4	několik
svědectví	svědectví	k1gNnSc2	svědectví
<g/>
,	,	kIx,	,
vesměs	vesměs	k6eAd1	vesměs
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Husité	husita	k1gMnPc1	husita
takto	takto	k6eAd1	takto
údajně	údajně	k6eAd1	údajně
někdy	někdy	k6eAd1	někdy
zabíjeli	zabíjet	k5eAaImAgMnP	zabíjet
zajaté	zajatý	k2eAgMnPc4d1	zajatý
katolické	katolický	k2eAgMnPc4d1	katolický
kněze	kněz	k1gMnPc4	kněz
či	či	k8xC	či
mnichy	mnich	k1gMnPc4	mnich
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kněz	kněz	k1gMnSc1	kněz
Ambrož	Ambrož	k1gMnSc1	Ambrož
nechal	nechat	k5eAaPmAgMnS	nechat
roku	rok	k1gInSc2	rok
1425	[number]	k4	1425
během	během	k7c2	během
rejsy	rejsa	k1gFnSc2	rejsa
do	do	k7c2	do
Slezska	Slezsko	k1gNnSc2	Slezsko
uvařit	uvařit	k5eAaPmF	uvařit
faráře	farář	k1gMnPc4	farář
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Radkově	Radkův	k2eAgNnSc6d1	Radkovo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Prachaticích	Prachatice	k1gFnPc6	Prachatice
katem	kat	k1gInSc7	kat
uvařen	uvařit	k5eAaPmNgMnS	uvařit
jakýsi	jakýsi	k3yIgMnSc1	jakýsi
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
padělal	padělat	k5eAaBmAgMnS	padělat
mince	mince	k1gFnSc2	mince
<g/>
.	.	kIx.	.
</s>
<s>
Čínský	čínský	k2eAgInSc1d1	čínský
císařský	císařský	k2eAgInSc1d1	císařský
soudní	soudní	k2eAgInSc1d1	soudní
dvůr	dvůr	k1gInSc1	dvůr
používal	používat	k5eAaImAgInS	používat
uvaření	uvaření	k1gNnSc4	uvaření
zaživa	zaživa	k6eAd1	zaživa
jako	jako	k8xS	jako
formu	forma	k1gFnSc4	forma
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
mučení	mučení	k1gNnSc2	mučení
<g/>
.	.	kIx.	.
</s>
<s>
Mongolský	mongolský	k2eAgMnSc1d1	mongolský
válečník	válečník	k1gMnSc1	válečník
Džamucha	Džamuch	k1gMnSc4	Džamuch
uvařil	uvařit	k5eAaPmAgMnS	uvařit
zaživa	zaživa	k6eAd1	zaživa
některé	některý	k3yIgMnPc4	některý
generály	generál	k1gMnPc4	generál
svého	svůj	k3xOyFgMnSc2	svůj
rivala	rival	k1gMnSc2	rival
Čingischána	Čingischán	k1gMnSc2	Čingischán
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1200	[number]	k4	1200
n.	n.	k?	n.
l.	l.	k?	l.
V	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
lidovém	lidový	k2eAgInSc6d1	lidový
příběhu	příběh	k1gInSc6	příběh
o	o	k7c6	o
nindžovi	nindža	k1gMnSc6	nindža
Išikawovi	Išikawa	k1gMnSc6	Išikawa
Goemonovi	Goemon	k1gMnSc6	Goemon
je	být	k5eAaImIp3nS	být
Goemon	Goemon	k1gInSc1	Goemon
popraven	popravit	k5eAaPmNgInS	popravit
uvařením	uvaření	k1gNnSc7	uvaření
v	v	k7c6	v
oleji	olej	k1gInSc6	olej
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
románu	román	k1gInSc6	román
Šógun	Šóguna	k1gFnPc2	Šóguna
od	od	k7c2	od
Jamese	Jamese	k1gFnSc2	Jamese
Clavella	Clavello	k1gNnSc2	Clavello
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
anglický	anglický	k2eAgMnSc1d1	anglický
námořník	námořník	k1gMnSc1	námořník
popraven	popravit	k5eAaPmNgInS	popravit
uvařením	uvaření	k1gNnSc7	uvaření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
trest	trest	k1gInSc1	trest
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
za	za	k7c4	za
vlády	vláda	k1gFnPc4	vláda
dynastie	dynastie	k1gFnSc2	dynastie
Mughalů	Mughal	k1gInPc2	Mughal
pro	pro	k7c4	pro
zrádce	zrádce	k1gMnPc4	zrádce
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
pramenů	pramen	k1gInPc2	pramen
tak	tak	k6eAd1	tak
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
umučen	umučen	k2eAgMnSc1d1	umučen
Ardžan	Ardžan	k1gMnSc1	Ardžan
Dév	Dév	k1gMnSc1	Dév
<g/>
,	,	kIx,	,
pátý	pátý	k4xOgMnSc1	pátý
guru	guru	k1gMnSc1	guru
Sikhů	sikh	k1gMnPc2	sikh
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
spiknutí	spiknutí	k1gNnSc4	spiknutí
prince	princ	k1gMnSc2	princ
Chusraua	Chusrauus	k1gMnSc2	Chusrauus
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gMnSc3	jeho
otci	otec	k1gMnSc3	otec
císaři	císař	k1gMnSc3	císař
Džahángírovi	Džahángír	k1gMnSc3	Džahángír
<g/>
.	.	kIx.	.
</s>
<s>
Idi	Idi	k?	Idi
Amin	amin	k1gInSc1	amin
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Ugandy	Uganda	k1gFnSc2	Uganda
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
a	a	k8xC	a
vládní	vládní	k2eAgInPc1d1	vládní
úřady	úřad	k1gInPc1	úřad
režimu	režim	k1gInSc2	režim
současného	současný	k2eAgMnSc2d1	současný
prezidenta	prezident	k1gMnSc2	prezident
Isloma	Islom	k1gMnSc2	Islom
Karimova	Karimův	k2eAgMnSc2d1	Karimův
v	v	k7c6	v
Uzbekistánu	Uzbekistán	k1gInSc6	Uzbekistán
byli	být	k5eAaImAgMnP	být
nařčeni	nařknout	k5eAaPmNgMnP	nařknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
uvařili	uvařit	k5eAaPmAgMnP	uvařit
zaživa	zaživa	k6eAd1	zaživa
množství	množství	k1gNnSc4	množství
politických	politický	k2eAgMnPc2d1	politický
disidentů	disident	k1gMnPc2	disident
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Muzafara	Muzafara	k1gFnSc1	Muzafara
Avazova	Avazův	k2eAgFnSc1d1	Avazův
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
klišé	klišé	k1gNnSc1	klišé
slouží	sloužit	k5eAaImIp3nP	sloužit
zobrazení	zobrazení	k1gNnSc1	zobrazení
nebo	nebo	k8xC	nebo
představa	představa	k1gFnSc1	představa
kanibalů	kanibal	k1gMnPc2	kanibal
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vaří	vařit	k5eAaImIp3nP	vařit
lidi	člověk	k1gMnPc4	člověk
zaživa	zaživa	k6eAd1	zaživa
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
kotli	kotel	k1gInSc6	kotel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
románu	román	k1gInSc6	román
Jamese	Jamese	k1gFnSc2	Jamese
Clavella	Clavella	k1gMnSc1	Clavella
Šógun	Šógun	k1gMnSc1	Šógun
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
členů	člen	k1gMnPc2	člen
posádky	posádka	k1gFnSc2	posádka
lodi	loď	k1gFnSc2	loď
Erasmus	Erasmus	k1gInSc1	Erasmus
uvařen	uvařen	k2eAgInSc1d1	uvařen
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
v	v	k7c6	v
kotli	kotel	k1gInSc6	kotel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Boiling	Boiling	k1gInSc1	Boiling
to	ten	k3xDgNnSc1	ten
death	death	k1gInSc1	death
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Uvaření	uvaření	k1gNnSc1	uvaření
zaživa	zaživa	k6eAd1	zaživa
jako	jako	k8xC	jako
forma	forma	k1gFnSc1	forma
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
Pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
:	:	kIx,	:
Mučení	mučení	k1gNnPc4	mučení
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
:	:	kIx,	:
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
</s>
