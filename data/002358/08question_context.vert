<s>
Poprava	poprava	k1gFnSc1	poprava
uvařením	uvaření	k1gNnSc7	uvaření
je	být	k5eAaImIp3nS	být
pomalý	pomalý	k2eAgInSc1d1	pomalý
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
bolestivý	bolestivý	k2eAgInSc1d1	bolestivý
způsob	způsob	k1gInSc1	způsob
vykonání	vykonání	k1gNnSc2	vykonání
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Odsouzený	odsouzený	k1gMnSc1	odsouzený
je	být	k5eAaImIp3nS	být
svlečen	svlečen	k2eAgMnSc1d1	svlečen
a	a	k8xC	a
ponořen	ponořen	k2eAgMnSc1d1	ponořen
do	do	k7c2	do
již	již	k6eAd1	již
vařící	vařící	k2eAgFnSc2d1	vařící
kapaliny	kapalina	k1gFnSc2	kapalina
nebo	nebo	k8xC	nebo
svázán	svázán	k2eAgMnSc1d1	svázán
a	a	k8xC	a
ponořen	ponořen	k2eAgMnSc1d1	ponořen
do	do	k7c2	do
velkého	velký	k2eAgInSc2d1	velký
kotle	kotel	k1gInSc2	kotel
se	s	k7c7	s
studenou	studený	k2eAgFnSc7d1	studená
kapalinou	kapalina	k1gFnSc7	kapalina
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yRgNnSc7	který
popravčí	popravčí	k2eAgInPc4d1	popravčí
zapálí	zapálit	k5eAaPmIp3nS	zapálit
oheň	oheň	k1gInSc4	oheň
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zahřeje	zahřát	k5eAaPmIp3nS	zahřát
kapalinu	kapalina	k1gFnSc4	kapalina
k	k	k7c3	k
varu	var	k1gInSc3	var
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
kapalinu	kapalina	k1gFnSc4	kapalina
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
olej	olej	k1gInSc4	olej
<g/>
,	,	kIx,	,
kyselinu	kyselina	k1gFnSc4	kyselina
<g/>
,	,	kIx,	,
dehet	dehet	k1gInSc4	dehet
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
roztavené	roztavený	k2eAgNnSc1d1	roztavené
olovo	olovo	k1gNnSc1	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
smrti	smrt	k1gFnSc2	smrt
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
popáleniny	popálenina	k1gFnSc2	popálenina
<g/>
.	.	kIx.	.
</s>
<s>
Sice	sice	k8xC	sice
nebylo	být	k5eNaImAgNnS	být
tohoto	tento	k3xDgInSc2	tento
trestu	trest	k1gInSc3	trest
užíváno	užíván	k2eAgNnSc1d1	užíváno
tak	tak	k9	tak
často	často	k6eAd1	často
jako	jako	k9	jako
jiných	jiný	k2eAgFnPc2d1	jiná
metod	metoda	k1gFnPc2	metoda
popravy	poprava	k1gFnSc2	poprava
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
ale	ale	k8xC	ale
uvaření	uvaření	k1gNnSc1	uvaření
zaživa	zaživa	k6eAd1	zaživa
bylo	být	k5eAaImAgNnS	být
široce	široko	k6eAd1	široko
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
dvou	dva	k4xCgInPc2	dva
až	až	k9	až
tří	tři	k4xCgInPc2	tři
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poprava	poprava	k1gFnSc1	poprava
uvařením	uvaření	k1gNnSc7	uvaření
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
používala	používat	k5eAaImAgFnS	používat
už	už	k6eAd1	už
v	v	k7c6	v
antice	antika	k1gFnSc6	antika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
bylo	být	k5eAaImAgNnS	být
vaření	vaření	k1gNnSc1	vaření
v	v	k7c6	v
kotli	kotel	k1gInSc6	kotel
používáno	používán	k2eAgNnSc4d1	používáno
jako	jako	k9	jako
způsob	způsob	k1gInSc4	způsob
mučení	mučení	k1gNnPc2	mučení
a	a	k8xC	a
popravy	poprava	k1gFnPc4	poprava
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
pronásledování	pronásledování	k1gNnSc6	pronásledování
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Vít	Vít	k1gMnSc1	Vít
a	a	k8xC	a
apoštol	apoštol	k1gMnSc1	apoštol
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
byli	být	k5eAaImAgMnP	být
podle	podle	k7c2	podle
legend	legenda	k1gFnPc2	legenda
odsouzeni	odsoudit	k5eAaPmNgMnP	odsoudit
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
způsobu	způsob	k1gInSc3	způsob
popravy	poprava	k1gFnSc2	poprava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vaření	vařený	k2eAgMnPc1d1	vařený
zázračně	zázračně	k6eAd1	zázračně
přežili	přežít	k5eAaPmAgMnP	přežít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c6	o
popravě	poprava	k1gFnSc6	poprava
vařením	vaření	k1gNnSc7	vaření
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
,	,	kIx,	,
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
v	v	k7c6	v
období	období	k1gNnSc6	období
vrcholného	vrcholný	k2eAgInSc2d1	vrcholný
a	a	k8xC	a
pozdního	pozdní	k2eAgInSc2d1	pozdní
středověku	středověk	k1gInSc2	středověk
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
bylo	být	k5eAaImAgNnS	být
trestáno	trestán	k2eAgNnSc1d1	trestáno
padělání	padělání	k1gNnSc1	padělání
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
kacířství	kacířství	k1gNnSc4	kacířství
nebo	nebo	k8xC	nebo
travičství	travičství	k1gNnSc4	travičství
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
na	na	k7c6	na
konci	konec	k1gInSc6	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
trest	trest	k1gInSc1	trest
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
traviče	travič	k1gMnPc4	travič
<g/>
.	.	kIx.	.
</s>
