<s>
Ariana	Ariana	k1gFnSc1
(	(	kIx(
<g/>
odrůda	odrůda	k1gFnSc1
révy	réva	k1gFnSc2
vinné	vinný	k2eAgFnSc2d1
<g/>
)	)	kIx)
</s>
<s>
Ariana	Ariana	k1gFnSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
Ar	ar	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
pozdní	pozdní	k2eAgFnSc1d1
až	až	k8xS
pozdní	pozdní	k2eAgFnSc1d1
moštová	moštový	k2eAgFnSc1d1
odrůda	odrůda	k1gFnSc1
révy	réva	k1gFnSc2
vinné	vinný	k2eAgFnSc2d1
(	(	kIx(
<g/>
Vitis	Vitis	k1gFnSc2
vinifera	vinifer	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
určená	určený	k2eAgFnSc1d1
k	k	k7c3
výrobě	výroba	k1gFnSc3
červených	červený	k2eAgNnPc2d1
vín	víno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odrůda	odrůda	k1gFnSc1
byla	být	k5eAaImAgFnS
vyšlechtěna	vyšlechtit	k5eAaPmNgFnS
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
Perné	perný	k2eAgFnSc6d1
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
křížením	křížení	k1gNnSc7
odrůd	odrůda	k1gFnPc2
(	(	kIx(
<g/>
Ryzlink	ryzlink	k1gInSc4
rýnský	rýnský	k2eAgInSc4d1
x	x	k?
Svatovavřinecké	svatovavřinecké	k1gNnSc4
<g/>
)	)	kIx)
x	x	k?
Zweigeltrebe	Zweigeltreb	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Réva	réva	k1gFnSc1
vinná	vinný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Vitis	Vitis	k1gFnSc1
vinifera	vinifera	k1gFnSc1
<g/>
)	)	kIx)
odrůda	odrůda	k1gFnSc1
Ariana	Ariana	k1gFnSc1
je	být	k5eAaImIp3nS
jednodomá	jednodomý	k2eAgFnSc1d1
dřevitá	dřevitý	k2eAgFnSc1d1
pnoucí	pnoucí	k2eAgFnSc1d1
liána	liána	k1gFnSc1
<g/>
,	,	kIx,
dorůstající	dorůstající	k2eAgFnSc1d1
v	v	k7c6
kultuře	kultura	k1gFnSc6
až	až	k9
několika	několik	k4yIc2
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kmen	kmen	k1gInSc4
tloušťky	tloušťka	k1gFnSc2
až	až	k9
několik	několik	k4yIc1
centimetrů	centimetr	k1gInPc2
je	být	k5eAaImIp3nS
pokryt	pokrýt	k5eAaPmNgInS
světlou	světlý	k2eAgFnSc7d1
borkou	borka	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
loupe	loupat	k5eAaImIp3nS
v	v	k7c6
pruzích	pruh	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Úponky	úponka	k1gFnPc1
révy	réva	k1gFnSc2
umožňují	umožňovat	k5eAaImIp3nP
této	tento	k3xDgFnSc3
rostlině	rostlina	k1gFnSc3
pnout	pnout	k5eAaImF
se	se	k3xPyFc4
po	po	k7c6
pevných	pevný	k2eAgInPc6d1
předmětech	předmět	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Růst	růst	k1gInSc1
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
bujný	bujný	k2eAgInSc1d1
se	s	k7c7
vzpřímenými	vzpřímený	k2eAgInPc7d1
letorosty	letorost	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
List	list	k1gInSc1
odrůdy	odrůda	k1gFnSc2
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
velký	velký	k2eAgInSc1d1
<g/>
,	,	kIx,
tmavozelený	tmavozelený	k2eAgInSc1d1
<g/>
,	,	kIx,
tvar	tvar	k1gInSc1
čepele	čepel	k1gFnSc2
kruhovitý	kruhovitý	k2eAgInSc1d1
<g/>
,	,	kIx,
tří-	tří-	k?
až	až	k6eAd1
pětilaločný	pětilaločný	k2eAgInSc1d1
s	s	k7c7
velmi	velmi	k6eAd1
mělkými	mělký	k2eAgInPc7d1
až	až	k8xS
mělkými	mělký	k2eAgInPc7d1
horními	horní	k2eAgInPc7d1
bočními	boční	k2eAgInPc7d1
výkroji	výkroj	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrchní	vrchní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
čepele	čepel	k1gFnSc2
listu	list	k1gInSc2
je	být	k5eAaImIp3nS
zprohýbaná	zprohýbaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
středně	středně	k6eAd1
puchýřnatá	puchýřnatý	k2eAgFnSc1d1
<g/>
,	,	kIx,
spodní	spodní	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
hustě	hustě	k6eAd1
štětinatá	štětinatý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řapíkový	řapíkový	k2eAgInSc1d1
výkroj	výkroj	k1gInSc1
je	být	k5eAaImIp3nS
lyrovitý	lyrovitý	k2eAgInSc1d1
<g/>
,	,	kIx,
otevřený	otevřený	k2eAgInSc1d1
až	až	k9
lehce	lehko	k6eAd1
překrytý	překrytý	k2eAgInSc1d1
s	s	k7c7
průsvitem	průsvit	k1gInSc7
<g/>
,	,	kIx,
řapík	řapík	k1gInSc1
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
dlouhý	dlouhý	k2eAgInSc1d1
<g/>
,	,	kIx,
narůžovělý	narůžovělý	k2eAgInSc1d1
až	až	k6eAd1
červenorůžový	červenorůžový	k2eAgInSc1d1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
žilnatina	žilnatina	k1gFnSc1
v	v	k7c6
okolí	okolí	k1gNnSc6
řapíku	řapík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Oboupohlavní	oboupohlavní	k2eAgInPc4d1
pětičetné	pětičetný	k2eAgInPc4d1
květy	květ	k1gInPc4
v	v	k7c6
hroznovitých	hroznovitý	k2eAgNnPc6d1
květenstvích	květenství	k1gNnPc6
jsou	být	k5eAaImIp3nP
žlutozelené	žlutozelený	k2eAgInPc1d1
<g/>
,	,	kIx,
samosprašné	samosprašný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plodem	plod	k1gInSc7
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
velká	velký	k2eAgFnSc1d1
<g/>
,	,	kIx,
kulatá	kulatý	k2eAgFnSc1d1
bobule	bobule	k1gFnSc1
se	s	k7c7
středně	středně	k6eAd1
silnou	silný	k2eAgFnSc7d1
slupkou	slupka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barva	barva	k1gFnSc1
bobule	bobule	k1gFnSc1
je	být	k5eAaImIp3nS
modročerná	modročerný	k2eAgFnSc1d1
<g/>
,	,	kIx,
dužnina	dužnina	k1gFnSc1
je	být	k5eAaImIp3nS
bez	bez	k7c2
zbarvení	zbarvení	k1gNnPc2
<g/>
,	,	kIx,
rozplývavá	rozplývavý	k2eAgFnSc1d1
<g/>
,	,	kIx,
plné	plný	k2eAgFnSc3d1
chuti	chuť	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrozen	hrozen	k1gInSc1
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
velký	velký	k2eAgMnSc1d1
(	(	kIx(
<g/>
172	#num#	k4
g	g	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hustý	hustý	k2eAgInSc1d1
<g/>
,	,	kIx,
kuželovitý	kuželovitý	k2eAgInSc1d1
<g/>
,	,	kIx,
rozvětvený	rozvětvený	k2eAgMnSc1d1
se	se	k3xPyFc4
středně	středně	k6eAd1
dlouhou	dlouhý	k2eAgFnSc4d1
až	až	k8xS
delší	dlouhý	k2eAgFnSc4d2
<g/>
,	,	kIx,
narůžovělou	narůžovělý	k2eAgFnSc4d1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
rozvětvenou	rozvětvený	k2eAgFnSc7d1
stopkou	stopka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
a	a	k8xC
rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Ariana	Ariana	k1gFnSc1
je	být	k5eAaImIp3nS
moštová	moštový	k2eAgFnSc1d1
odrůda	odrůda	k1gFnSc1
révy	réva	k1gFnSc2
vinné	vinný	k2eAgFnSc2d1
(	(	kIx(
<g/>
Vitis	Vitis	k1gFnSc2
vinifera	vinifer	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kříženec	kříženec	k1gMnSc1
odrůd	odrůda	k1gFnPc2
(	(	kIx(
<g/>
Ryzlink	ryzlink	k1gInSc4
rýnský	rýnský	k2eAgInSc4d1
x	x	k?
Svatovavřinecké	svatovavřinecké	k1gNnSc4
<g/>
)	)	kIx)
x	x	k?
Zweigeltrebe	Zweigeltreb	k1gInSc5
s	s	k7c7
pracovním	pracovní	k2eAgNnSc7d1
označením	označení	k1gNnSc7
PE-	PE-	k1gFnSc1
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšlechtili	vyšlechtit	k5eAaPmAgMnP
ji	on	k3xPp3gFnSc4
p.	p.	k?
František	František	k1gMnSc1
Zatloukal	Zatloukal	k1gMnSc1
a	a	k8xC
p.	p.	k?
Ing.	ing.	kA
Ludvík	Ludvík	k1gMnSc1
Michlovský	michlovský	k2eAgMnSc1d1
ve	v	k7c6
Šlechtitelské	šlechtitelský	k2eAgFnSc6d1
stanici	stanice	k1gFnSc6
vinařské	vinařský	k2eAgFnSc6d1
v	v	k7c4
Perné	perný	k2eAgFnPc4d1
u	u	k7c2
Mikulova	Mikulov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
ČR	ČR	kA
je	být	k5eAaImIp3nS
odrůda	odrůda	k1gFnSc1
zastupována	zastupován	k2eAgFnSc1d1
firmou	firma	k1gFnSc7
Vinselekt	Vinselekt	k1gInSc4
Michlovský	michlovský	k2eAgInSc4d1
Rakvice	rakvice	k1gFnSc1
a	a	k8xC
je	být	k5eAaImIp3nS
právně	právně	k6eAd1
chráněna	chránit	k5eAaImNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Státní	státní	k2eAgFnSc2d1
odrůdové	odrůdový	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
byla	být	k5eAaImAgFnS
odrůda	odrůda	k1gFnSc1
zapsána	zapsat	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Udržovatelem	udržovatel	k1gMnSc7
odrůdy	odrůda	k1gFnSc2
je	být	k5eAaImIp3nS
p.	p.	k?
Ing.	ing.	kA
Miloš	Miloš	k1gMnSc1
Michlovský	michlovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
CSc.	CSc.	kA
V	v	k7c6
ČR	ČR	kA
zatím	zatím	k6eAd1
odrůda	odrůda	k1gFnSc1
není	být	k5eNaImIp3nS
významněji	významně	k6eAd2
rozšířena	rozšířit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastoupení	zastoupení	k1gNnPc1
ve	v	k7c6
vinicích	vinice	k1gFnPc6
ČR	ČR	kA
bylo	být	k5eAaImAgNnS
roku	rok	k1gInSc2
2010	#num#	k4
menší	malý	k2eAgFnSc1d2
než	než	k8xS
0,01	0,01	k4
%	%	kIx~
<g/>
,	,	kIx,
roku	rok	k1gInSc2
2007	#num#	k4
byla	být	k5eAaImAgFnS
pěstována	pěstovat	k5eAaImNgFnS
na	na	k7c4
3,1	3,1	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
vhodná	vhodný	k2eAgFnSc1d1
k	k	k7c3
pěstování	pěstování	k1gNnSc3
v	v	k7c6
obou	dva	k4xCgFnPc6
vinařských	vinařský	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
ČR	ČR	kA
<g/>
,	,	kIx,
v	v	k7c6
Čechách	Čechy	k1gFnPc6
i	i	k8xC
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Moravě	Morava	k1gFnSc6
je	být	k5eAaImIp3nS
místy	místy	k6eAd1
<g/>
,	,	kIx,
víceméně	víceméně	k9
pokusně	pokusně	k6eAd1
<g/>
,	,	kIx,
pěstována	pěstovat	k5eAaImNgFnS
malopěstiteli	malopěstitel	k1gMnSc3
jako	jako	k8xC,k8xS
stolní	stolní	k2eAgFnSc1d1
odrůda	odrůda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Ariana	Ariana	k1gFnSc1
je	být	k5eAaImIp3nS
ženské	ženský	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
řeckého	řecký	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
varianta	varianta	k1gFnSc1
jména	jméno	k1gNnSc2
Ariadné	Ariadný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklo	vzniknout	k5eAaPmAgNnS
složením	složení	k1gNnSc7
slov	slovo	k1gNnPc2
„	„	k?
<g/>
ari	ari	k?
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
andámo	andámo	k6eAd1
<g/>
“	“	k?
<g/>
,	,	kIx,
ve	v	k7c6
volném	volný	k2eAgInSc6d1
překladu	překlad	k1gInSc6
„	„	k?
<g/>
velmi	velmi	k6eAd1
líbivá	líbivý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ctihodná	ctihodný	k2eAgFnSc1d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Pěstování	pěstování	k1gNnSc1
</s>
<s>
Proti	proti	k7c3
poškození	poškození	k1gNnSc2
zimními	zimní	k2eAgInPc7d1
mrazy	mráz	k1gInPc7
je	být	k5eAaImIp3nS
odolná	odolný	k2eAgNnPc4d1
středně	středně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodí	hodit	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
pro	pro	k7c4
většinu	většina	k1gFnSc4
vedení	vedení	k1gNnSc2
<g/>
,	,	kIx,
snáší	snášet	k5eAaImIp3nS
i	i	k9
krátký	krátký	k2eAgInSc4d1
řez	řez	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
osvědčilo	osvědčit	k5eAaPmAgNnS
se	se	k3xPyFc4
vedení	vedení	k1gNnSc1
s	s	k7c7
řezem	řez	k1gInSc7
na	na	k7c4
dlouhý	dlouhý	k2eAgInSc4d1
vodorovný	vodorovný	k2eAgInSc4d1
tažeň	tažeň	k1gInSc4
<g/>
,	,	kIx,
vhodné	vhodný	k2eAgFnPc4d1
jsou	být	k5eAaImIp3nP
podnože	podnož	k1gFnPc1
SO-	SO-	k1gFnSc1
<g/>
4	#num#	k4
<g/>
,	,	kIx,
Cr	cr	k0
2	#num#	k4
a	a	k8xC
125	#num#	k4
AA	AA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
sa	sa	k?
o	o	k7c4
velmi	velmi	k6eAd1
plodnou	plodný	k2eAgFnSc4d1
odrůdu	odrůda	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vhodné	vhodný	k2eAgNnSc4d1
nižší	nízký	k2eAgNnSc4d2
zatížení	zatížení	k1gNnSc4
oček	očko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plodnost	plodnost	k1gFnSc1
je	být	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc1d2
až	až	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
<g/>
,	,	kIx,
10-15	10-15	k4
t	t	k?
<g/>
/	/	kIx~
<g/>
ha	ha	kA
(	(	kIx(
<g/>
2,5	2,5	k4
až	až	k9
4	#num#	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
keř	keř	k1gInSc1
<g/>
)	)	kIx)
při	při	k7c6
cukernatosti	cukernatost	k1gFnSc6
17-20	17-20	k4
°	°	k?
<g/>
NM	NM	kA
a	a	k8xC
obsahu	obsah	k1gInSc6
kyselin	kyselina	k1gFnPc2
8-10	8-10	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
zkouškách	zkouška	k1gFnPc6
pro	pro	k7c4
registraci	registrace	k1gFnSc4
bylo	být	k5eAaImAgNnS
v	v	k7c6
tříletém	tříletý	k2eAgInSc6d1
průměru	průměr	k1gInSc6
dosaženo	dosáhnout	k5eAaPmNgNnS
výnosu	výnos	k1gInSc2
14,5	14,5	k4
t	t	k?
<g/>
/	/	kIx~
<g/>
ha	ha	kA
při	při	k7c6
průměrné	průměrný	k2eAgFnSc6d1
cukernatosti	cukernatost	k1gFnSc6
19	#num#	k4
<g/>
°	°	k?
NM	NM	kA
<g/>
.	.	kIx.
</s>
<s>
Fenologie	fenologie	k1gFnSc1
</s>
<s>
Doba	doba	k1gFnSc1
rašení	rašení	k1gNnSc2
oček	očko	k1gNnPc2
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
pozdní	pozdní	k2eAgMnSc1d1
(	(	kIx(
<g/>
mezi	mezi	k7c7
odrůdami	odrůda	k1gFnPc7
Veltlínské	veltlínský	k2eAgFnSc2d1
zelené	zelená	k1gFnSc2
a	a	k8xC
Ryzlink	ryzlink	k1gInSc1
rýnský	rýnský	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kvete	kvést	k5eAaImIp3nS
středně	středně	k6eAd1
pozdně	pozdně	k6eAd1
<g/>
,	,	kIx,
zaměká	zaměkat	k5eAaImIp3nS
středně	středně	k6eAd1
pozdně	pozdně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sklizňová	sklizňový	k2eAgFnSc1d1
zralost	zralost	k1gFnSc1
nastává	nastávat	k5eAaImIp3nS
začátkem	začátek	k1gInSc7
až	až	k9
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
října	říjen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Choroby	choroba	k1gFnPc1
a	a	k8xC
škůdci	škůdce	k1gMnPc1
</s>
<s>
Proti	proti	k7c3
napadení	napadení	k1gNnSc6
houbovými	houbový	k2eAgFnPc7d1
chorobami	choroba	k1gFnPc7
je	být	k5eAaImIp3nS
odrůda	odrůda	k1gFnSc1
méně	málo	k6eAd2
až	až	k9
středně	středně	k6eAd1
odolná	odolný	k2eAgFnSc1d1
<g/>
,	,	kIx,
citlivá	citlivý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
zejména	zejména	k9
k	k	k7c3
plísni	plíseň	k1gFnSc3
šedé	šedý	k2eAgFnSc2d1
(	(	kIx(
<g/>
Botrytis	Botrytis	k1gFnSc2
cinerea	cinere	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
a	a	k8xC
půdy	půda	k1gFnPc1
</s>
<s>
Vyžaduje	vyžadovat	k5eAaImIp3nS
svahovité	svahovitý	k2eAgInPc4d1
pozemky	pozemek	k1gInPc4
s	s	k7c7
jižní	jižní	k2eAgFnSc7d1
<g/>
,	,	kIx,
jihovýchodní	jihovýchodní	k2eAgFnSc7d1
či	či	k8xC
jihozápadní	jihozápadní	k2eAgFnSc7d1
expozicí	expozice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
půdu	půda	k1gFnSc4
má	mít	k5eAaImIp3nS
vyšší	vysoký	k2eAgInPc4d2
nároky	nárok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nesnáší	snášet	k5eNaImIp3nP
suché	suchý	k2eAgFnPc1d1
půdy	půda	k1gFnPc1
<g/>
,	,	kIx,
optimální	optimální	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
půdy	půda	k1gFnPc1
hlinité	hlinitý	k2eAgFnPc1d1
až	až	k6eAd1
hlinito-písčité	hlinito-písčitý	k2eAgFnPc1d1
<g/>
,	,	kIx,
dostatečně	dostatečně	k6eAd1
záhřevné	záhřevný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokladem	předpoklad	k1gInSc7
kvality	kvalita	k1gFnSc2
je	být	k5eAaImIp3nS
i	i	k9
dostatečná	dostatečný	k2eAgFnSc1d1
vododržnost	vododržnost	k1gFnSc1
půdy	půda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Víno	víno	k1gNnSc1
</s>
<s>
Technologie	technologie	k1gFnSc1
zpracování	zpracování	k1gNnSc2
hroznů	hrozen	k1gInPc2
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
kvalitě	kvalita	k1gFnSc6
suroviny	surovina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
nedostatečně	dostatečně	k6eNd1
vyzrálé	vyzrálý	k2eAgInPc4d1
hrozny	hrozen	k1gInPc4
se	se	k3xPyFc4
volí	volit	k5eAaImIp3nS
kratší	krátký	k2eAgFnSc1d2
doba	doba	k1gFnSc1
macerace	macerace	k1gFnSc2
(	(	kIx(
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
dní	den	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
umožní	umožnit	k5eAaPmIp3nS
vyrobit	vyrobit	k5eAaPmF
výrazně	výrazně	k6eAd1
aromatické	aromatický	k2eAgInPc1d1
<g/>
,	,	kIx,
chuťově	chuťově	k6eAd1
jemné	jemný	k2eAgNnSc4d1
<g/>
,	,	kIx,
jakostní	jakostní	k2eAgNnSc4d1
víno	víno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přívlastky	přívlastek	k1gInPc7
vyžadují	vyžadovat	k5eAaImIp3nP
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
macerace	macerace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vhodné	vhodný	k2eAgNnSc4d1
je	být	k5eAaImIp3nS
dosažení	dosažení	k1gNnSc4
minimálně	minimálně	k6eAd1
13	#num#	k4
obj	obj	k?
<g/>
.	.	kIx.
%	%	kIx~
alkoholu	alkohol	k1gInSc2
<g/>
,	,	kIx,
víno	víno	k1gNnSc1
je	být	k5eAaImIp3nS
potom	potom	k6eAd1
vhodné	vhodný	k2eAgNnSc1d1
pro	pro	k7c4
zrání	zrání	k1gNnSc4
v	v	k7c6
sudech	sud	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obou	dva	k4xCgFnPc6
kategoriích	kategorie	k1gFnPc6
vín	vína	k1gFnPc2
je	být	k5eAaImIp3nS
nutná	nutný	k2eAgFnSc1d1
jablečno-mléčná	jablečno-mléčný	k2eAgFnSc1d1
fermentace	fermentace	k1gFnSc1
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
zjemní	zjemnit	k5eAaPmIp3nS
chuť	chuť	k1gFnSc4
a	a	k8xC
zvýrazní	zvýraznit	k5eAaPmIp3nS
aroma	aroma	k1gNnSc1
červeného	červený	k2eAgNnSc2d1
ovoce	ovoce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ariana	Ariana	k1gFnSc1
je	být	k5eAaImIp3nS
vhodná	vhodný	k2eAgFnSc1d1
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
velmi	velmi	k6eAd1
zajímavých	zajímavý	k2eAgNnPc2d1
odrůdových	odrůdový	k2eAgNnPc2d1
vín	víno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charakter	charakter	k1gInSc1
vín	víno	k1gNnPc2
připomíná	připomínat	k5eAaImIp3nS
vyzrálá	vyzrálý	k2eAgNnPc4d1
vína	víno	k1gNnPc4
odrůdy	odrůda	k1gFnSc2
Zweigeltrebe	Zweigeltreb	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Víno	víno	k1gNnSc1
je	být	k5eAaImIp3nS
tmavě	tmavě	k6eAd1
rubínové	rubínový	k2eAgFnPc1d1
až	až	k8xS
granátové	granátový	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
<g/>
,	,	kIx,
plné	plný	k2eAgFnPc1d1
<g/>
,	,	kIx,
harmonické	harmonický	k2eAgFnPc1d1
<g/>
,	,	kIx,
sametově	sametově	k6eAd1
hebké	hebký	k2eAgInPc1d1
<g/>
,	,	kIx,
dostatečně	dostatečně	k6eAd1
barevné	barevný	k2eAgFnPc1d1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
dobré	dobrý	k2eAgFnSc2d1
kvality	kvalita	k1gFnSc2
s	s	k7c7
jemným	jemný	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
tříslovin	tříslovina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zráním	zrání	k1gNnSc7
získává	získávat	k5eAaImIp3nS
na	na	k7c6
kvalitě	kvalita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vůni	vůně	k1gFnSc6
a	a	k8xC
chuti	chuť	k1gFnSc6
můžeme	moct	k5eAaImIp1nP
hledat	hledat	k5eAaImF
peckovité	peckovitý	k2eAgNnSc4d1
ovoce	ovoce	k1gNnSc4
<g/>
,	,	kIx,
třešně	třešeň	k1gFnPc4
a	a	k8xC
višně	višeň	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odrůdová	odrůdový	k2eAgFnSc1d1
vína	vína	k1gFnSc1
jsou	být	k5eAaImIp3nP
vhodná	vhodný	k2eAgNnPc1d1
ke	k	k7c3
střednědobé	střednědobý	k2eAgFnSc3d1
archivaci	archivace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Stolování	stolování	k1gNnSc1
</s>
<s>
Mladší	mladý	k2eAgFnSc1d2
odrůdová	odrůdový	k2eAgFnSc1d1
vína	vína	k1gFnSc1
s	s	k7c7
ovocnými	ovocný	k2eAgInPc7d1
tóny	tón	k1gInPc7
jsou	být	k5eAaImIp3nP
vhodná	vhodný	k2eAgFnSc1d1
k	k	k7c3
červeným	červený	k2eAgNnPc3d1
masům	maso	k1gNnPc3
a	a	k8xC
sýrům	sýr	k1gInPc3
s	s	k7c7
bílou	bílý	k2eAgFnSc7d1
plísní	plíseň	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyzrálejší	vyzrálý	k2eAgFnSc1d2
vína	vína	k1gFnSc1
skvěle	skvěle	k6eAd1
doplňují	doplňovat	k5eAaImIp3nP
hovězí	hovězí	k2eAgNnSc4d1
maso	maso	k1gNnSc4
a	a	k8xC
divočinu	divočina	k1gFnSc4
<g/>
,	,	kIx,
i	i	k8xC
v	v	k7c6
pikantnějších	pikantní	k2eAgFnPc6d2
úpravách	úprava	k1gFnPc6
a	a	k8xC
také	také	k9
sýry	sýr	k1gInPc1
s	s	k7c7
modrou	modrý	k2eAgFnSc7d1
plísní	plíseň	k1gFnSc7
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
KOVÁŘ	Kovář	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
VITIS	VITIS	kA
VINIFERA	VINIFERA	kA
L.	L.	kA
–	–	k?
réva	réva	k1gFnSc1
vinná	vinný	k2eAgFnSc1d1
/	/	kIx~
vinič	vinič	k1gMnSc1
hroznorodý	hroznorodý	k2eAgMnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Botany	Botana	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2008-01-22	2008-01-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Vilém	Vilém	k1gMnSc1
Kraus	Kraus	k1gMnSc1
<g/>
,	,	kIx,
Zuzana	Zuzana	k1gFnSc1
Foffová	Foffový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Bohumil	Bohumil	k1gMnSc1
Vurm	Vurm	k1gMnSc1
<g/>
,	,	kIx,
Dáša	Dáša	k1gFnSc1
Krausová	Krausová	k1gFnSc1
:	:	kIx,
Nová	nový	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
českého	český	k2eAgNnSc2d1
a	a	k8xC
moravského	moravský	k2eAgNnSc2d1
vína	víno	k1gNnSc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praga	Praga	k1gFnSc1
Mystica	Mystica	k1gFnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86767	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Multimédia	multimédium	k1gNnPc1
</s>
<s>
Ing.	ing.	kA
Radek	Radek	k1gMnSc1
Sotolář	Sotolář	k1gMnSc1
:	:	kIx,
Multimediální	multimediální	k2eAgInSc1d1
atlas	atlas	k1gInSc1
podnožových	podnožový	k2eAgFnPc2d1
<g/>
,	,	kIx,
moštových	moštový	k2eAgFnPc2d1
a	a	k8xC
stolních	stolní	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
révy	réva	k1gFnSc2
<g/>
,	,	kIx,
Mendelova	Mendelův	k2eAgFnSc1d1
zemědělská	zemědělský	k2eAgFnSc1d1
a	a	k8xC
lesnická	lesnický	k2eAgFnSc1d1
universita	universita	k1gFnSc1
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
zahradnická	zahradnický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
v	v	k7c6
Lednici	Lednice	k1gFnSc6
</s>
<s>
Martin	Martin	k1gMnSc1
Šimek	Šimek	k1gMnSc1
:	:	kIx,
Encyklopédie	Encyklopédie	k1gFnSc1
všemožnejch	všemožnejch	k?
odrůd	odrůda	k1gFnPc2
révy	réva	k1gFnSc2
vinné	vinný	k2eAgFnSc2d1
z	z	k7c2
celýho	celýho	k?
světa	svět	k1gInSc2
s	s	k7c7
přihlédnutím	přihlédnutí	k1gNnSc7
k	k	k7c3
těm	ten	k3xDgMnPc3
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
již	již	k6eAd1
ouplně	ouplně	k6eAd1
vymizely	vymizet	k5eAaPmAgFnP
<g/>
,	,	kIx,
2008-2012	2008-2012	k4
</s>
