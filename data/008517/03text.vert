<p>
<s>
Gutenbergova	Gutenbergův	k2eAgFnSc1d1	Gutenbergova
bible	bible	k1gFnSc1	bible
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
vytištěnou	vytištěný	k2eAgFnSc7d1	vytištěná
knihou	kniha	k1gFnSc7	kniha
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
Mohuči	Mohuč	k1gFnSc6	Mohuč
v	v	k7c6	v
letech	let	k1gInPc6	let
1454	[number]	k4	1454
<g/>
–	–	k?	–
<g/>
1455	[number]	k4	1455
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
vydavateli	vydavatel	k1gMnPc7	vydavatel
byli	být	k5eAaImAgMnP	být
Johann	Johann	k1gMnSc1	Johann
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Fust	Fust	k1gMnSc1	Fust
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Schoeffer	Schoeffer	k1gMnSc1	Schoeffer
<g/>
.	.	kIx.	.
</s>
<s>
Tisk	tisk	k1gInSc1	tisk
bible	bible	k1gFnSc2	bible
začal	začít	k5eAaPmAgInS	začít
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1452	[number]	k4	1452
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgFnSc4	první
knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vytištěna	vytisknout	k5eAaPmNgFnS	vytisknout
pomocí	pomocí	k7c2	pomocí
knihtiskařského	knihtiskařský	k2eAgInSc2d1	knihtiskařský
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
celkem	celkem	k6eAd1	celkem
51	[number]	k4	51
jejích	její	k3xOp3gFnPc2	její
kopií	kopie	k1gFnPc2	kopie
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
12	[number]	k4	12
bylo	být	k5eAaImAgNnS	být
vytištěno	vytisknout	k5eAaPmNgNnS	vytisknout
na	na	k7c4	na
pergamen	pergamen	k1gInSc4	pergamen
a	a	k8xC	a
39	[number]	k4	39
na	na	k7c4	na
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
20	[number]	k4	20
kopií	kopie	k1gFnPc2	kopie
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
ve	v	k7c6	v
výborném	výborný	k2eAgInSc6d1	výborný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
prvotisk	prvotisk	k1gInSc4	prvotisk
<g/>
,	,	kIx,	,
inkunábuli	inkunábule	k1gFnSc4	inkunábule
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
verze	verze	k1gFnPc1	verze
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
42	[number]	k4	42
řádků	řádek	k1gInPc2	řádek
a	a	k8xC	a
jiné	jiné	k1gNnSc4	jiné
mají	mít	k5eAaImIp3nP	mít
36	[number]	k4	36
řádků	řádek	k1gInPc2	řádek
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaPmNgFnS	napsat
latinsky	latinsky	k6eAd1	latinsky
<g/>
.	.	kIx.	.
<g/>
Tzv.	tzv.	kA	tzv.
42	[number]	k4	42
<g/>
řádková	řádkový	k2eAgFnSc1d1	řádková
bible	bible	k1gFnSc1	bible
o	o	k7c6	o
1282	[number]	k4	1282
stranách	strana	k1gFnPc6	strana
byla	být	k5eAaImAgFnS	být
vytištěna	vytištěn	k2eAgFnSc1d1	vytištěna
v	v	k7c6	v
nákladu	náklad	k1gInSc6	náklad
180	[number]	k4	180
exemplářů	exemplář	k1gInPc2	exemplář
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
30	[number]	k4	30
luxusnějších	luxusní	k2eAgInPc2d2	luxusnější
na	na	k7c6	na
pergamenu	pergamen	k1gInSc6	pergamen
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
na	na	k7c6	na
papíru	papír	k1gInSc6	papír
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
první	první	k4xOgNnSc4	první
velké	velký	k2eAgNnSc4d1	velké
dílo	dílo	k1gNnSc4	dílo
knihtisku	knihtisk	k1gInSc2	knihtisk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
technicky	technicky	k6eAd1	technicky
dokonalé	dokonalý	k2eAgNnSc1d1	dokonalé
<g/>
,	,	kIx,	,
s	s	k7c7	s
harmonickou	harmonický	k2eAgFnSc7d1	harmonická
sazbou	sazba	k1gFnSc7	sazba
<g/>
,	,	kIx,	,
zarovnanými	zarovnaný	k2eAgInPc7d1	zarovnaný
pravými	pravý	k2eAgInPc7d1	pravý
okraji	okraj	k1gInPc7	okraj
sloupců	sloupec	k1gInPc2	sloupec
a	a	k8xC	a
stejnoměrně	stejnoměrně	k6eAd1	stejnoměrně
sytou	sytý	k2eAgFnSc7d1	sytá
černí	čerň	k1gFnSc7	čerň
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Britské	britský	k2eAgFnSc6d1	britská
knihovně	knihovna	k1gFnSc6	knihovna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
exemplářích	exemplář	k1gInPc6	exemplář
<g/>
,	,	kIx,	,
jednom	jeden	k4xCgNnSc6	jeden
na	na	k7c6	na
papíru	papír	k1gInSc2	papír
a	a	k8xC	a
jednom	jeden	k4xCgNnSc6	jeden
na	na	k7c6	na
pergamenu	pergamen	k1gInSc6	pergamen
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
kopie	kopie	k1gFnSc1	kopie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
například	například	k6eAd1	například
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
Knihovně	knihovna	k1gFnSc6	knihovna
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
exemplářů	exemplář	k1gInPc2	exemplář
na	na	k7c6	na
pergamenu	pergamen	k1gInSc6	pergamen
součástí	součást	k1gFnPc2	součást
empírové	empírový	k2eAgFnSc2d1	empírová
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Nostickém	nostický	k2eAgInSc6d1	nostický
paláci	palác	k1gInSc6	palác
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dnes	dnes	k6eAd1	dnes
sídlí	sídlet	k5eAaImIp3nS	sídlet
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kultury	kultura	k1gFnSc2	kultura
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
nejasných	jasný	k2eNgFnPc2d1	nejasná
okolností	okolnost	k1gFnPc2	okolnost
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
ztratil	ztratit	k5eAaPmAgMnS	ztratit
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
jej	on	k3xPp3gMnSc4	on
drží	držet	k5eAaImIp3nS	držet
Huntington	Huntington	k1gInSc1	Huntington
Library	Librara	k1gFnSc2	Librara
v	v	k7c6	v
kalifornském	kalifornský	k2eAgNnSc6d1	kalifornské
San	San	k1gMnPc4	San
Marinu	Marina	k1gFnSc4	Marina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
železniční	železniční	k2eAgMnSc1d1	železniční
magnát	magnát	k1gMnSc1	magnát
a	a	k8xC	a
sběratel	sběratel	k1gMnSc1	sběratel
Henry	Henry	k1gMnSc1	Henry
E.	E.	kA	E.
Huntington	Huntington	k1gInSc1	Huntington
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
známým	známý	k2eAgInSc7d1	známý
Gutenbergovým	Gutenbergův	k2eAgInSc7d1	Gutenbergův
tiskem	tisk	k1gInSc7	tisk
chovaným	chovaný	k2eAgInSc7d1	chovaný
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
tištěné	tištěný	k2eAgInPc1d1	tištěný
formuláře	formulář	k1gInPc1	formulář
odpustkových	odpustkový	k2eAgInPc2d1	odpustkový
listů	list	k1gInPc2	list
<g/>
)	)	kIx)	)
jeden	jeden	k4xCgInSc1	jeden
list	list	k1gInSc1	list
z	z	k7c2	z
Gutenbergovy	Gutenbergův	k2eAgFnSc2d1	Gutenbergova
Bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
získala	získat	k5eAaPmAgFnS	získat
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
sign	signum	k1gNnPc2	signum
<g/>
.	.	kIx.	.
39	[number]	k4	39
B	B	kA	B
54	[number]	k4	54
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gutenbergova	Gutenbergův	k2eAgFnSc1d1	Gutenbergova
bible	bible	k1gFnSc1	bible
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Gutenbergova	Gutenbergův	k2eAgFnSc1d1	Gutenbergova
bible	bible	k1gFnSc1	bible
v	v	k7c6	v
Britské	britský	k2eAgFnSc6d1	britská
knihovně	knihovna	k1gFnSc6	knihovna
</s>
</p>
