<s>
Nejnižším	nízký	k2eAgNnSc7d3
místem	místo	k1gNnSc7
je	být	k5eAaImIp3nS
hladina	hladina	k1gFnSc1
řeky	řeka	k1gFnSc2
Seiny	Seina	k1gFnSc2
(	(	kIx(
<g/>
zhruba	zhruba	k6eAd1
25	[number]	k4
m	m	kA
<g/>
,	,	kIx,
kolísá	kolísat	k5eAaImIp3nS
podle	podle	k7c2
stavu	stav	k1gInSc2
vody	voda	k1gFnSc2
<g/>
)	)	kIx)
u	u	k7c2
Pont	Pont	k1gInSc1
aval	aval	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
opouští	opouštět	k5eAaImIp3nS
město	město	k1gNnSc4
a	a	k8xC
nejvyšším	vysoký	k2eAgInSc7d3
přirozeným	přirozený	k2eAgInSc7d1
bodem	bod	k1gInSc7
je	být	k5eAaImIp3nS
vrcholek	vrcholek	k1gInSc1
kopce	kopec	k1gInSc2
Montmartre	Montmartr	k1gInSc5
(	(	kIx(
<g/>
130	[number]	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>