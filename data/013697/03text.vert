<s>
Homotopie	Homotopie	k1gFnSc1
</s>
<s>
Homotopie	Homotopie	k1gFnSc1
je	být	k5eAaImIp3nS
pojem	pojem	k1gInSc4
z	z	k7c2
matematiky	matematika	k1gFnSc2
<g/>
,	,	kIx,
přesněji	přesně	k6eAd2
z	z	k7c2
algebraické	algebraický	k2eAgFnSc2d1
topologie	topologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Motivace	motivace	k1gFnSc1
</s>
<s>
Homotopie	Homotopie	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
postihnout	postihnout	k5eAaPmF
některé	některý	k3yIgFnPc4
topologické	topologický	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
topologických	topologický	k2eAgInPc2d1
prostorů	prostor	k1gInPc2
a	a	k8xC
zachycuje	zachycovat	k5eAaImIp3nS
v	v	k7c6
rámci	rámec	k1gInSc6
matematiky	matematika	k1gFnSc2
představu	představa	k1gFnSc4
spojité	spojitý	k2eAgFnSc2d1
deformace	deformace	k1gFnSc2
prostorů	prostor	k1gInPc2
a	a	k8xC
zobrazení	zobrazení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Definice	definice	k1gFnSc1
</s>
<s>
Nechť	nechť	k9
</s>
<s>
X	X	kA
</s>
<s>
,	,	kIx,
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
X	X	kA
<g/>
,	,	kIx,
<g/>
}	}	kIx)
</s>
<s>
Y	Y	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Y	Y	kA
<g/>
}	}	kIx)
</s>
<s>
jsou	být	k5eAaImIp3nP
topologické	topologický	k2eAgFnPc4d1
prostory	prostora	k1gFnPc4
a	a	k8xC
</s>
<s>
f	f	k?
</s>
<s>
:	:	kIx,
</s>
<s>
X	X	kA
</s>
<s>
→	→	k?
</s>
<s>
Y	Y	kA
</s>
<s>
,	,	kIx,
</s>
<s>
g	g	kA
</s>
<s>
:	:	kIx,
</s>
<s>
X	X	kA
</s>
<s>
→	→	k?
</s>
<s>
Y	Y	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
f	f	k?
<g/>
:	:	kIx,
<g/>
X	X	kA
<g/>
\	\	kIx~
<g/>
to	ten	k3xDgNnSc1
Y	Y	kA
<g/>
,	,	kIx,
<g/>
g	g	kA
<g/>
:	:	kIx,
<g/>
X	X	kA
<g/>
\	\	kIx~
<g/>
to	ten	k3xDgNnSc4
Y	Y	kA
<g/>
}	}	kIx)
</s>
<s>
spojitá	spojitý	k2eAgNnPc1d1
zobrazení	zobrazení	k1gNnPc1
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Homotopie	Homotopie	k1gFnSc1
mezi	mezi	k7c7
</s>
<s>
f	f	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
f	f	k?
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
g	g	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
g	g	kA
<g/>
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
spojité	spojitý	k2eAgNnSc4d1
zobrazení	zobrazení	k1gNnSc4
</s>
<s>
H	H	kA
</s>
<s>
:	:	kIx,
</s>
<s>
[	[	kIx(
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
]	]	kIx)
</s>
<s>
×	×	k?
</s>
<s>
X	X	kA
</s>
<s>
→	→	k?
</s>
<s>
Y	Y	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
H	H	kA
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
0,1	0,1	k4
<g/>
]	]	kIx)
<g/>
\	\	kIx~
<g/>
times	times	k1gInSc1
X	X	kA
<g/>
\	\	kIx~
<g/>
to	ten	k3xDgNnSc4
Y	Y	kA
<g/>
}	}	kIx)
</s>
<s>
takové	takový	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
a	a	k8xC
</s>
<s>
H	H	kA
</s>
<s>
(	(	kIx(
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
x	x	k?
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
H	H	kA
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
,	,	kIx,
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
H	H	kA
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
x	x	k?
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
g	g	kA
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
H	H	kA
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
,	,	kIx,
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
g	g	kA
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
pro	pro	k7c4
každé	každý	k3xTgNnSc4
</s>
<s>
x	x	k?
</s>
<s>
∈	∈	k?
</s>
<s>
X	X	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
\	\	kIx~
<g/>
in	in	k?
X	X	kA
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
kde	kde	k6eAd1
uvažujeme	uvažovat	k5eAaImIp1nP
</s>
<s>
[	[	kIx(
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
]	]	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
[	[	kIx(
<g/>
0,1	0,1	k4
<g/>
]	]	kIx)
<g/>
}	}	kIx)
</s>
<s>
s	s	k7c7
topologií	topologie	k1gFnSc7
danou	daný	k2eAgFnSc7d1
inkluzí	inkluze	k1gFnSc7
</s>
<s>
[	[	kIx(
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
]	]	kIx)
</s>
<s>
⊆	⊆	k?
</s>
<s>
R	R	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
[	[	kIx(
<g/>
0,1	0,1	k4
<g/>
]	]	kIx)
<g/>
\	\	kIx~
<g/>
subseteq	subseteq	k?
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
R	R	kA
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
do	do	k7c2
</s>
<s>
R	R	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
R	R	kA
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
s	s	k7c7
metrickou	metrický	k2eAgFnSc7d1
topologií	topologie	k1gFnSc7
a	a	k8xC
na	na	k7c4
</s>
<s>
[	[	kIx(
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
]	]	kIx)
</s>
<s>
×	×	k?
</s>
<s>
X	X	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
[	[	kIx(
<g/>
0,1	0,1	k4
<g/>
]	]	kIx)
<g/>
\	\	kIx~
<g/>
times	times	k1gInSc1
X	X	kA
<g/>
}	}	kIx)
</s>
<s>
uvažujeme	uvažovat	k5eAaImIp1nP
součinovou	součinový	k2eAgFnSc4d1
topologii	topologie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
existuje	existovat	k5eAaImIp3nS
homotopie	homotopie	k1gFnSc1
mezi	mezi	k7c7
</s>
<s>
f	f	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
f	f	k?
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
g	g	kA
</s>
<s>
,	,	kIx,
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
g	g	kA
<g/>
,	,	kIx,
<g/>
}	}	kIx)
</s>
<s>
řekneme	říct	k5eAaPmIp1nP
<g/>
,	,	kIx,
že	že	k8xS
</s>
<s>
f	f	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
f	f	k?
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
g	g	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
g	g	kA
<g/>
}	}	kIx)
</s>
<s>
jsou	být	k5eAaImIp3nP
homotopická	homotopický	k2eAgNnPc1d1
a	a	k8xC
píšeme	psát	k5eAaImIp1nP
</s>
<s>
f	f	k?
</s>
<s>
≃	≃	k?
</s>
<s>
g	g	kA
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
f	f	k?
<g/>
\	\	kIx~
<g/>
simeq	simeq	k?
g.	g.	k?
<g/>
}	}	kIx)
</s>
<s>
Topologické	topologický	k2eAgFnPc1d1
prostory	prostora	k1gFnPc1
</s>
<s>
X	X	kA
</s>
<s>
,	,	kIx,
</s>
<s>
Y	Y	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
X	X	kA
<g/>
,	,	kIx,
<g/>
Y	Y	kA
<g/>
}	}	kIx)
</s>
<s>
nazveme	nazvat	k5eAaBmIp1nP,k5eAaPmIp1nP
homotopické	homotopický	k2eAgInPc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
existují	existovat	k5eAaImIp3nP
spojitá	spojitý	k2eAgNnPc4d1
zobrazení	zobrazení	k1gNnPc4
</s>
<s>
f	f	k?
</s>
<s>
:	:	kIx,
</s>
<s>
X	X	kA
</s>
<s>
→	→	k?
</s>
<s>
Y	Y	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
f	f	k?
<g/>
:	:	kIx,
<g/>
X	X	kA
<g/>
\	\	kIx~
<g/>
to	ten	k3xDgNnSc4
Y	Y	kA
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
g	g	kA
</s>
<s>
:	:	kIx,
</s>
<s>
Y	Y	kA
</s>
<s>
→	→	k?
</s>
<s>
X	X	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
g	g	kA
<g/>
:	:	kIx,
<g/>
Y	Y	kA
<g/>
\	\	kIx~
<g/>
to	ten	k3xDgNnSc4
X	X	kA
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
že	že	k8xS
</s>
<s>
f	f	k?
</s>
<s>
∘	∘	k?
</s>
<s>
g	g	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
f	f	k?
<g/>
\	\	kIx~
<g/>
circ	circ	k1gInSc1
g	g	kA
<g/>
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
homotopické	homotopický	k2eAgNnSc1d1
</s>
<s>
i	i	k9
</s>
<s>
d	d	k?
</s>
<s>
Y	Y	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
id_	id_	k?
<g/>
{	{	kIx(
<g/>
Y	Y	kA
<g/>
}}	}}	k?
</s>
<s>
a	a	k8xC
</s>
<s>
g	g	kA
</s>
<s>
∘	∘	k?
</s>
<s>
f	f	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
g	g	kA
<g/>
\	\	kIx~
<g/>
circ	circ	k1gInSc1
f	f	k?
<g/>
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
homotopické	homotopický	k2eAgNnSc1d1
</s>
<s>
i	i	k9
</s>
<s>
d	d	k?
</s>
<s>
X	X	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
id_	id_	k?
<g/>
{	{	kIx(
<g/>
X	X	kA
<g/>
}}	}}	k?
</s>
<s>
</s>
<s>
Topologicky	topologicky	k6eAd1
prostor	prostor	k1gInSc1
</s>
<s>
X	X	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
X	X	kA
<g/>
}	}	kIx)
</s>
<s>
nazveme	nazvat	k5eAaBmIp1nP,k5eAaPmIp1nP
kontraktibilní	kontraktibilní	k2eAgMnSc1d1
(	(	kIx(
<g/>
stažitelný	stažitelný	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
homotopický	homotopický	k2eAgInSc1d1
jednoprvkovému	jednoprvkový	k2eAgInSc3d1
topologickému	topologický	k2eAgInSc3d1
prostoru	prostor	k1gInSc3
(	(	kIx(
<g/>
bodu	bod	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Příklady	příklad	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snadno	snadno	k6eAd1
se	se	k3xPyFc4
ověří	ověřit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
každé	každý	k3xTgFnPc1
dvě	dva	k4xCgFnPc1
uzavřené	uzavřený	k2eAgFnPc1d1
křivky	křivka	k1gFnPc1
</s>
<s>
a	a	k8xC
</s>
<s>
,	,	kIx,
</s>
<s>
b	b	k?
</s>
<s>
:	:	kIx,
</s>
<s>
[	[	kIx(
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
]	]	kIx)
</s>
<s>
→	→	k?
</s>
<s>
R	R	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a	a	k8xC
<g/>
,	,	kIx,
<g/>
b	b	k?
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
0,1	0,1	k4
<g/>
]	]	kIx)
<g/>
\	\	kIx~
<g/>
to	ten	k3xDgNnSc1
\	\	kIx~
<g/>
mathbb	mathbb	k1gInSc1
{	{	kIx(
<g/>
R	R	kA
<g/>
}	}	kIx)
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
v	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
R	R	kA
<g/>
}	}	kIx)
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
jsou	být	k5eAaImIp3nP
homotopické	homotopický	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Homotopií	Homotopie	k1gFnPc2
je	být	k5eAaImIp3nS
např.	např.	kA
</s>
<s>
H	H	kA
</s>
<s>
:	:	kIx,
</s>
<s>
[	[	kIx(
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
]	]	kIx)
</s>
<s>
×	×	k?
</s>
<s>
[	[	kIx(
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
]	]	kIx)
</s>
<s>
→	→	k?
</s>
<s>
R	R	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
H	H	kA
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
0,1	0,1	k4
<g/>
]	]	kIx)
<g/>
\	\	kIx~
<g/>
times	times	k1gInSc1
[	[	kIx(
<g/>
0,1	0,1	k4
<g/>
]	]	kIx)
<g/>
\	\	kIx~
<g/>
to	ten	k3xDgNnSc1
\	\	kIx~
<g/>
mathbb	mathbb	k1gInSc1
{	{	kIx(
<g/>
R	R	kA
<g/>
}	}	kIx)
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
dané	daný	k2eAgFnSc3d1
formuli	formule	k1gFnSc3
</s>
<s>
H	H	kA
</s>
<s>
(	(	kIx(
</s>
<s>
s	s	k7c7
</s>
<s>
,	,	kIx,
</s>
<s>
t	t	k?
</s>
<s>
)	)	kIx)
</s>
<s>
:	:	kIx,
<g/>
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
t	t	k?
</s>
<s>
)	)	kIx)
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
s	s	k7c7
</s>
<s>
)	)	kIx)
</s>
<s>
+	+	kIx~
</s>
<s>
t	t	k?
</s>
<s>
g	g	kA
</s>
<s>
(	(	kIx(
</s>
<s>
s	s	k7c7
</s>
<s>
)	)	kIx)
</s>
<s>
,	,	kIx,
</s>
<s>
t	t	k?
</s>
<s>
,	,	kIx,
</s>
<s>
s	s	k7c7
</s>
<s>
∈	∈	k?
</s>
<s>
[	[	kIx(
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
]	]	kIx)
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
H	H	kA
<g/>
(	(	kIx(
<g/>
s	s	k7c7
<g/>
,	,	kIx,
<g/>
t	t	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
<g/>
=	=	kIx~
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
-t	-t	k?
<g/>
)	)	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
+	+	kIx~
<g/>
tg	tg	kA
<g/>
(	(	kIx(
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
t	t	k?
<g/>
,	,	kIx,
<g/>
s	s	k7c7
<g/>
\	\	kIx~
<g/>
in	in	k?
[	[	kIx(
<g/>
0,1	0,1	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžší	těžký	k2eAgInPc1d2
je	být	k5eAaImIp3nS
ověřit	ověřit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
kružnice	kružnice	k1gFnSc1
</s>
<s>
a	a	k8xC
</s>
<s>
(	(	kIx(
</s>
<s>
s	s	k7c7
</s>
<s>
)	)	kIx)
</s>
<s>
:	:	kIx,
<g/>
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
cos	cos	k3yInSc1
</s>
<s>
s	s	k7c7
</s>
<s>
,	,	kIx,
</s>
<s>
sin	sin	kA
</s>
<s>
s	s	k7c7
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a	a	k8xC
<g/>
(	(	kIx(
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
:	:	kIx,
<g/>
=	=	kIx~
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
cos	cos	k3yInSc1
s	s	k7c7
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
sin	sin	kA
s	s	k7c7
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
kružnice	kružnice	k1gFnSc1
</s>
<s>
b	b	k?
</s>
<s>
(	(	kIx(
</s>
<s>
s	s	k7c7
</s>
<s>
)	)	kIx)
</s>
<s>
:	:	kIx,
<g/>
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
cos	cos	k3yInSc1
</s>
<s>
s	s	k7c7
</s>
<s>
,	,	kIx,
</s>
<s>
sin	sin	kA
</s>
<s>
s	s	k7c7
</s>
<s>
)	)	kIx)
</s>
<s>
,	,	kIx,
</s>
<s>
s	s	k7c7
</s>
<s>
∈	∈	k?
</s>
<s>
[	[	kIx(
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
2	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
]	]	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
b	b	k?
<g/>
(	(	kIx(
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
:	:	kIx,
<g/>
=	=	kIx~
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
cos	cos	k3yInSc1
s	s	k7c7
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
sin	sin	kA
s	s	k7c7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
s	s	k7c7
<g/>
\	\	kIx~
<g/>
in	in	k?
[	[	kIx(
<g/>
0,2	0,2	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
]	]	kIx)
<g/>
}	}	kIx)
</s>
<s>
v	v	k7c6
prostoru	prostor	k1gInSc6
</s>
<s>
R	R	kA
</s>
<s>
2	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
{	{	kIx(
</s>
<s>
(	(	kIx(
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
0	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
}	}	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
R	R	kA
<g/>
}	}	kIx)
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
-	-	kIx~
<g/>
\	\	kIx~
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
0,0	0,0	k4
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
}}	}}	k?
</s>
<s>
nejsou	být	k5eNaImIp3nP
homotopické	homotopický	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neformálně	formálně	k6eNd1
lze	lze	k6eAd1
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
první	první	k4xOgFnSc4
kružnici	kružnice	k1gFnSc4
nelze	lze	k6eNd1
zdeformovat	zdeformovat	k5eAaPmF
na	na	k7c4
druhou	druhý	k4xOgFnSc4
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
bychom	by	kYmCp1nP
s	s	k7c7
první	první	k4xOgFnSc6
přešli	přejít	k5eAaPmAgMnP
počátek	počátek	k1gInSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
do	do	k7c2
uvažovaného	uvažovaný	k2eAgInSc2d1
topologického	topologický	k2eAgInSc2d1
prostou	prostý	k2eAgFnSc7d1
nepatří	patřit	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Topologický	topologický	k2eAgInSc4d1
prostor	prostor	k1gInSc4
</s>
<s>
R	R	kA
</s>
<s>
n	n	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
R	R	kA
<g/>
}	}	kIx)
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
</s>
<s>
n	n	k0
</s>
<s>
∈	∈	k?
</s>
<s>
N	N	kA
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n	n	k0
<g/>
\	\	kIx~
<g/>
in	in	k?
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
N	N	kA
<g/>
}	}	kIx)
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
kontraktibilní	kontraktibilní	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
malá	malý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ani	ani	k8xC
velká	velký	k2eAgFnSc1d1
kružnice	kružnice	k1gFnSc1
na	na	k7c6
toru	torus	k1gInSc6
nejsou	být	k5eNaImIp3nP
kontraktibilní	kontraktibilní	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Tvrzení	tvrzení	k1gNnSc1
</s>
<s>
Relace	relace	k1gFnSc2
být	být	k5eAaImF
homotopická	homotopický	k2eAgNnPc4d1
resp.	resp.	kA
být	být	k5eAaImF
homotopické	homotopický	k2eAgFnPc4d1
jsou	být	k5eAaImIp3nP
relacemi	relace	k1gFnPc7
ekvivalence	ekvivalence	k1gFnSc2
na	na	k7c6
množině	množina	k1gFnSc6
všech	všecek	k3xTgNnPc2
spojitých	spojitý	k2eAgNnPc2d1
zobrazení	zobrazení	k1gNnPc2
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
topologickými	topologický	k2eAgInPc7d1
prostory	prostor	k1gInPc7
<g/>
,	,	kIx,
resp.	resp.	kA
na	na	k7c6
množině	množina	k1gFnSc6
všech	všecek	k3xTgInPc2
topologických	topologický	k2eAgInPc2d1
prostorů	prostor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
Pojem	pojem	k1gInSc1
homotopické	homotopický	k2eAgFnSc2d1
grupy	grupa	k1gFnSc2
vznikl	vzniknout	k5eAaPmAgInS
z	z	k7c2
potřeb	potřeba	k1gFnPc2
analýzy	analýza	k1gFnSc2
funkcí	funkce	k1gFnPc2
komplexní	komplexní	k2eAgFnSc2d1
proměnné	proměnná	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
teorie	teorie	k1gFnSc1
integrálů	integrál	k1gInPc2
na	na	k7c6
Riemannových	Riemannův	k2eAgFnPc6d1
plochách	plocha	k1gFnPc6
</s>
<s>
a	a	k8xC
díky	díky	k7c3
snaze	snaha	k1gFnSc3
klasifikovat	klasifikovat	k5eAaImF
jisté	jistý	k2eAgFnPc4d1
třídy	třída	k1gFnPc4
topologických	topologický	k2eAgInPc2d1
prostorů	prostor	k1gInPc2
<g/>
,	,	kIx,
především	především	k9
tzv.	tzv.	kA
hladkých	hladký	k2eAgFnPc2d1
variet	varieta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pojem	pojem	k1gInSc1
homotopie	homotopie	k1gFnSc1
má	mít	k5eAaImIp3nS
rozsáhlá	rozsáhlý	k2eAgNnPc4d1
zobecnění	zobecnění	k1gNnPc4
v	v	k7c6
(	(	kIx(
<g/>
homologické	homologický	k2eAgFnSc6d1
<g/>
)	)	kIx)
algebře	algebra	k1gFnSc6
<g/>
,	,	kIx,
teorii	teorie	k1gFnSc3
deformací	deformace	k1gFnPc2
<g/>
,	,	kIx,
matematické	matematický	k2eAgFnSc6d1
fyzice	fyzika	k1gFnSc6
a	a	k8xC
částech	část	k1gFnPc6
strunové	strunový	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
<g/>
,	,	kIx,
obzvláště	obzvláště	k6eAd1
v	v	k7c6
teorii	teorie	k1gFnSc6
tzv.	tzv.	kA
homologické	homologický	k2eAgFnSc2d1
zrcadlité	zrcadlitý	k2eAgFnSc2d1
symetrie	symetrie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hladké	Hladké	k2eAgFnSc1d1
verze	verze	k1gFnSc1
homotopie	homotopie	k1gFnSc2
v	v	k7c6
kategorii	kategorie	k1gFnSc6
hladkých	hladký	k2eAgFnPc2d1
variet	varieta	k1gFnPc2
se	se	k3xPyFc4
někdy	někdy	k6eAd1
nazývají	nazývat	k5eAaImIp3nP
izotopie	izotopie	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Homologie	homologie	k1gFnSc1
(	(	kIx(
<g/>
matematika	matematika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
HATCHER	HATCHER	kA
<g/>
,	,	kIx,
Allen	Allen	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Algebraic	Algebraic	k1gMnSc1
topology	topolog	k1gMnPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
79540	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4025803-8	4025803-8	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85061803	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85061803	#num#	k4
</s>
