<s>
Homotopie	Homotopie	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
postihnout	postihnout	k5eAaPmF
některé	některý	k3yIgFnPc4
topologické	topologický	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
topologických	topologický	k2eAgInPc2d1
prostorů	prostor	k1gInPc2
a	a	k8xC
zachycuje	zachycovat	k5eAaImIp3nS
v	v	k7c6
rámci	rámec	k1gInSc6
matematiky	matematika	k1gFnSc2
představu	představa	k1gFnSc4
spojité	spojitý	k2eAgFnSc2d1
deformace	deformace	k1gFnSc2
prostorů	prostor	k1gInPc2
a	a	k8xC
zobrazení	zobrazení	k1gNnPc2
<g/>
.	.	kIx.
</s>