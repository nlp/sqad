<p>
<s>
Král	Král	k1gMnSc1	Král
Lávra	Lávra	k1gMnSc1	Lávra
od	od	k7c2	od
spisovatele	spisovatel	k1gMnSc2	spisovatel
Karla	Karel	k1gMnSc2	Karel
Havlíčka	Havlíček	k1gMnSc2	Havlíček
Borovského	Borovský	k1gMnSc2	Borovský
je	být	k5eAaImIp3nS	být
báseň	báseň	k1gFnSc1	báseň
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
irské	irský	k2eAgFnSc2d1	irská
lidové	lidový	k2eAgFnSc2d1	lidová
pověsti	pověst	k1gFnSc2	pověst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Satirická	satirický	k2eAgFnSc1d1	satirická
báseň	báseň	k1gFnSc1	báseň
kratšího	krátký	k2eAgInSc2d2	kratší
rozsahu	rozsah	k1gInSc2	rozsah
s	s	k7c7	s
pohádkovými	pohádkový	k2eAgInPc7d1	pohádkový
motivy	motiv	k1gInPc7	motiv
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
ponaučení	ponaučení	k1gNnSc2	ponaučení
jako	jako	k8xS	jako
v	v	k7c6	v
bajce	bajka	k1gFnSc6	bajka
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
v	v	k7c6	v
době	doba	k1gFnSc6	doba
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
pol.	pol.	k?	pol.
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
inspirováno	inspirován	k2eAgNnSc1d1	inspirováno
řeckou	řecký	k2eAgFnSc4d1	řecká
(	(	kIx(	(
<g/>
Midas	Midas	k1gMnSc1	Midas
<g/>
)	)	kIx)	)
a	a	k8xC	a
irskou	irský	k2eAgFnSc7d1	irská
mytologií	mytologie	k1gFnSc7	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
čtenářů	čtenář	k1gMnPc2	čtenář
bylo	být	k5eAaImAgNnS	být
dílo	dílo	k1gNnSc1	dílo
velmi	velmi	k6eAd1	velmi
ceněno	cenit	k5eAaImNgNnS	cenit
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
cenzurou	cenzura	k1gFnSc7	cenzura
však	však	k9	však
zakazováno	zakazovat	k5eAaImNgNnS	zakazovat
pro	pro	k7c4	pro
jasnou	jasný	k2eAgFnSc4d1	jasná
kritiku	kritika	k1gFnSc4	kritika
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Báseň	báseň	k1gFnSc1	báseň
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
chronologickou	chronologický	k2eAgFnSc4d1	chronologická
kompozici	kompozice	k1gFnSc4	kompozice
<g/>
,	,	kIx,	,
najdeme	najít	k5eAaPmIp1nP	najít
zde	zde	k6eAd1	zde
jazykové	jazykový	k2eAgInPc4d1	jazykový
prostředky	prostředek	k1gInPc4	prostředek
typické	typický	k2eAgInPc4d1	typický
pro	pro	k7c4	pro
pohádku	pohádka	k1gFnSc4	pohádka
(	(	kIx(	(
<g/>
úvod	úvod	k1gInSc1	úvod
–	–	k?	–
Byl	být	k5eAaImAgInS	být
jednou	jednou	k6eAd1	jednou
jeden	jeden	k4xCgMnSc1	jeden
<g/>
...	...	k?	...
<g/>
;	;	kIx,	;
závěr	závěr	k1gInSc1	závěr
–	–	k?	–
ponaučení	ponaučení	k1gNnSc2	ponaučení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
personifikaci	personifikace	k1gFnSc4	personifikace
<g/>
,	,	kIx,	,
metonymii	metonymie	k1gFnSc4	metonymie
<g/>
,	,	kIx,	,
archaismy	archaismus	k1gInPc4	archaismus
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
Karlem	Karel	k1gMnSc7	Karel
Zemanem	Zeman	k1gMnSc7	Zeman
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
==	==	k?	==
</s>
</p>
<p>
<s>
Báseň	báseň	k1gFnSc1	báseň
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
jednom	jeden	k4xCgInSc6	jeden
irském	irský	k2eAgMnSc6d1	irský
králi	král	k1gMnSc6	král
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
velice	velice	k6eAd1	velice
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
a	a	k8xC	a
chytrý	chytrý	k2eAgInSc4d1	chytrý
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
věc	věc	k1gFnSc1	věc
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
byla	být	k5eAaImAgFnS	být
divná	divný	k2eAgFnSc1d1	divná
–	–	k?	–
nechával	nechávat	k5eAaImAgMnS	nechávat
se	se	k3xPyFc4	se
holit	holit	k5eAaImF	holit
a	a	k8xC	a
stříhat	stříhat	k5eAaImF	stříhat
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
do	do	k7c2	do
roka	rok	k1gInSc2	rok
a	a	k8xC	a
pokaždé	pokaždé	k6eAd1	pokaždé
nechal	nechat	k5eAaPmAgMnS	nechat
holiče	holič	k1gMnSc2	holič
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gMnSc4	on
stříhal	stříhat	k5eAaImAgMnS	stříhat
<g/>
,	,	kIx,	,
popravit	popravit	k5eAaPmF	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
si	se	k3xPyFc3	se
tuto	tento	k3xDgFnSc4	tento
zakázku	zakázka	k1gFnSc4	zakázka
vylosoval	vylosovat	k5eAaPmAgInS	vylosovat
mladý	mladý	k2eAgInSc1d1	mladý
Kukulín	Kukulín	k1gInSc1	Kukulín
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
odvedl	odvést	k5eAaPmAgMnS	odvést
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
jako	jako	k8xS	jako
všichni	všechen	k3xTgMnPc1	všechen
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
popraviště	popraviště	k1gNnSc4	popraviště
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
šla	jít	k5eAaImAgFnS	jít
žádat	žádat	k5eAaImF	žádat
krále	král	k1gMnSc4	král
Lávru	Lávra	k1gMnSc4	Lávra
o	o	k7c4	o
milost	milost	k1gFnSc4	milost
<g/>
.	.	kIx.	.
</s>
<s>
Apelovala	apelovat	k5eAaImAgFnS	apelovat
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
dobré	dobrý	k2eAgNnSc4d1	dobré
srdce	srdce	k1gNnSc4	srdce
a	a	k8xC	a
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gMnSc1	její
muž	muž	k1gMnSc1	muž
padl	padnout	k5eAaPmAgMnS	padnout
při	při	k7c6	při
službě	služba	k1gFnSc6	služba
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
dvoře	dvůr	k1gInSc6	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
se	se	k3xPyFc4	se
styděl	stydět	k5eAaImAgMnS	stydět
za	za	k7c4	za
její	její	k3xOp3gNnPc4	její
rozhořčená	rozhořčený	k2eAgNnPc4d1	rozhořčené
slova	slovo	k1gNnPc4	slovo
a	a	k8xC	a
milost	milost	k1gFnSc4	milost
Kukulínovi	Kukulín	k1gMnSc3	Kukulín
udělil	udělit	k5eAaPmAgMnS	udělit
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
nechá	nechat	k5eAaPmIp3nS	nechat
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
při	při	k7c6	při
stříhání	stříhání	k1gNnSc6	stříhání
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
zůstane	zůstat	k5eAaPmIp3nS	zůstat
jeho	jeho	k3xOp3gMnSc7	jeho
holičem	holič	k1gMnSc7	holič
až	až	k9	až
na	na	k7c4	na
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Kukulín	Kukulín	k1gInSc1	Kukulín
s	s	k7c7	s
radostí	radost	k1gFnSc7	radost
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
čase	čas	k1gInSc6	čas
ho	on	k3xPp3gNnSc4	on
začalo	začít	k5eAaPmAgNnS	začít
trápit	trápit	k5eAaImF	trápit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tajemství	tajemství	k1gNnSc1	tajemství
nemůže	moct	k5eNaImIp3nS	moct
nikomu	nikdo	k3yNnSc3	nikdo
sdělit	sdělit	k5eAaPmF	sdělit
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
si	se	k3xPyFc3	se
všimla	všimnout	k5eAaPmAgFnS	všimnout
jeho	jeho	k3xOp3gInSc2	jeho
smutku	smutek	k1gInSc2	smutek
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
jí	jíst	k5eAaImIp3nS	jíst
pověděl	povědět	k5eAaPmAgMnS	povědět
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
trápení	trápení	k1gNnSc6	trápení
<g/>
,	,	kIx,	,
poradila	poradit	k5eAaPmAgFnS	poradit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
jde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
lesa	les	k1gInSc2	les
k	k	k7c3	k
poustevníkovi	poustevník	k1gMnSc3	poustevník
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
pomůže	pomoct	k5eAaPmIp3nS	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Poustevník	poustevník	k1gMnSc1	poustevník
mu	on	k3xPp3gMnSc3	on
poradil	poradit	k5eAaPmAgMnS	poradit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
ho	on	k3xPp3gMnSc4	on
trápí	trápit	k5eAaImIp3nS	trápit
<g/>
,	,	kIx,	,
našeptal	našeptat	k5eAaPmAgMnS	našeptat
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
staré	starý	k2eAgFnSc2d1	stará
vrby	vrba	k1gFnSc2	vrba
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
uleví	ulevit	k5eAaPmIp3nS	ulevit
<g/>
.	.	kIx.	.
</s>
<s>
Kukulín	Kukulín	k1gInSc1	Kukulín
to	ten	k3xDgNnSc4	ten
ještě	ještě	k6eAd1	ještě
ten	ten	k3xDgInSc4	ten
den	den	k1gInSc4	den
udělal	udělat	k5eAaPmAgMnS	udělat
a	a	k8xC	a
skutečně	skutečně	k6eAd1	skutečně
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
ulevilo	ulevit	k5eAaPmAgNnS	ulevit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
čas	čas	k1gInSc4	čas
král	král	k1gMnSc1	král
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
velikou	veliký	k2eAgFnSc4d1	veliká
hostinu	hostina	k1gFnSc4	hostina
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
šli	jít	k5eAaImAgMnP	jít
čeští	český	k2eAgMnPc1d1	český
hudebníci	hudebník	k1gMnPc1	hudebník
<g/>
,	,	kIx,	,
co	co	k9	co
měli	mít	k5eAaImAgMnP	mít
na	na	k7c6	na
hostině	hostina	k1gFnSc6	hostina
hrát	hrát	k5eAaImF	hrát
kolem	kolem	k7c2	kolem
staré	starý	k2eAgFnSc2d1	stará
vrby	vrba	k1gFnSc2	vrba
<g/>
,	,	kIx,	,
basista	basista	k1gMnSc1	basista
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
ztratil	ztratit	k5eAaPmAgMnS	ztratit
kolíček	kolíček	k1gInSc4	kolíček
od	od	k7c2	od
basy	basa	k1gFnSc2	basa
<g/>
.	.	kIx.	.
</s>
<s>
Uřízl	uříznout	k5eAaPmAgMnS	uříznout
tedy	tedy	k9	tedy
větev	větev	k1gFnSc4	větev
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
vrby	vrba	k1gFnSc2	vrba
a	a	k8xC	a
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
si	se	k3xPyFc3	se
nový	nový	k2eAgInSc4d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Netušil	tušit	k5eNaImAgMnS	tušit
však	však	k9	však
<g/>
,	,	kIx,	,
co	co	k8xS	co
tím	ten	k3xDgNnSc7	ten
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
na	na	k7c6	na
hostině	hostina	k1gFnSc6	hostina
začal	začít	k5eAaPmAgInS	začít
jezdit	jezdit	k5eAaImF	jezdit
smyčcem	smyčec	k1gInSc7	smyčec
po	po	k7c6	po
strunách	struna	k1gFnPc6	struna
<g/>
,	,	kIx,	,
basa	basa	k1gFnSc1	basa
sama	sám	k3xTgMnSc4	sám
spustí	spustit	k5eAaPmIp3nS	spustit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Král	Král	k1gMnSc1	Král
Lávra	Lávra	k1gMnSc1	Lávra
má	mít	k5eAaImIp3nS	mít
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
oslí	oslí	k2eAgNnPc4d1	oslí
uši	ucho	k1gNnPc4	ucho
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
je	být	k5eAaImIp3nS	být
ušatec	ušatec	k1gMnSc1	ušatec
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Lávra	Lávra	k1gMnSc1	Lávra
nechal	nechat	k5eAaPmAgMnS	nechat
basistu	basista	k1gMnSc4	basista
vyhodit	vyhodit	k5eAaPmF	vyhodit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
už	už	k9	už
všichni	všechen	k3xTgMnPc1	všechen
věděli	vědět	k5eAaImAgMnP	vědět
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
tajemství	tajemství	k1gNnSc6	tajemství
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgInS	mít
dále	daleko	k6eAd2	daleko
důvod	důvod	k1gInSc1	důvod
skrývat	skrývat	k5eAaImF	skrývat
své	svůj	k3xOyFgFnPc4	svůj
oslí	oslí	k2eAgFnPc4d1	oslí
uši	ucho	k1gNnPc4	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
na	na	k7c4	na
jeho	jeho	k3xOp3gNnPc4	jeho
uši	ucho	k1gNnPc4	ucho
všichni	všechen	k3xTgMnPc1	všechen
zvykli	zvyknout	k5eAaPmAgMnP	zvyknout
–	–	k?	–
zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
oslí	oslí	k2eAgNnPc1d1	oslí
uši	ucho	k1gNnPc1	ucho
ke	k	k7c3	k
koruně	koruna	k1gFnSc3	koruna
hodí	hodit	k5eAaPmIp3nS	hodit
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
byli	být	k5eAaImAgMnP	být
rádi	rád	k2eAgMnPc1d1	rád
za	za	k7c2	za
dobrého	dobrý	k2eAgMnSc2d1	dobrý
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
nevyšlo	vyjít	k5eNaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
Lávrovo	Lávrův	k2eAgNnSc1d1	Lávrův
tajemství	tajemství	k1gNnSc1	tajemství
do	do	k7c2	do
basy	basa	k1gFnSc2	basa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Počeštěné	počeštěný	k2eAgInPc1d1	počeštěný
irské	irský	k2eAgInPc1d1	irský
názvy	název	k1gInPc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
originálním	originální	k2eAgInSc7d1	originální
způsobem	způsob	k1gInSc7	způsob
nakládá	nakládat	k5eAaImIp3nS	nakládat
s	s	k7c7	s
originálními	originální	k2eAgInPc7d1	originální
názvy	název	k1gInPc7	název
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
počešťuje	počešťovat	k5eAaImIp3nS	počešťovat
–	–	k?	–
tak	tak	k6eAd1	tak
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Viklov	Viklov	k1gInSc1	Viklov
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
původně	původně	k6eAd1	původně
Wicklow	Wicklow	k1gFnSc1	Wicklow
ři	ři	k?	ři
hrabství	hrabství	k1gNnSc1	hrabství
Wicklow	Wicklow	k1gMnSc1	Wicklow
a	a	k8xC	a
"	"	kIx"	"
<g/>
Kukulín	Kukulín	k1gInSc1	Kukulín
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc4	jméno
irského	irský	k2eAgMnSc2d1	irský
mýtického	mýtický	k2eAgMnSc2d1	mýtický
hrdiny	hrdina	k1gMnSc2	hrdina
Cúchulainna	Cúchulainn	k1gMnSc2	Cúchulainn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Král	Král	k1gMnSc1	Král
Lear	Lear	k1gMnSc1	Lear
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Král	Král	k1gMnSc1	Král
Lávra	Lávra	k1gMnSc1	Lávra
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Král	Král	k1gMnSc1	Král
Lávra	Lávra	k1gMnSc1	Lávra
recitovaný	recitovaný	k2eAgMnSc1d1	recitovaný
v	v	k7c4	v
mp	mp	k?	mp
<g/>
3	[number]	k4	3
</s>
</p>
<p>
<s>
BOROVSKÝ	Borovský	k1gMnSc1	Borovský
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
.	.	kIx.	.
</s>
<s>
Básnické	básnický	k2eAgInPc4d1	básnický
spisy	spis	k1gInPc4	spis
Karla	Karel	k1gMnSc4	Karel
Havlíčka	Havlíček	k1gMnSc4	Havlíček
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
F.	F.	kA	F.
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
,	,	kIx,	,
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
188	[number]	k4	188
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
-	-	kIx~	-
kapitola	kapitola	k1gFnSc1	kapitola
Král	Král	k1gMnSc1	Král
Lávra	Lávra	k1gMnSc1	Lávra
<g/>
,	,	kIx,	,
s.	s.	k?	s.
63	[number]	k4	63
<g/>
-	-	kIx~	-
<g/>
71	[number]	k4	71
<g/>
.	.	kIx.	.
</s>
</p>
