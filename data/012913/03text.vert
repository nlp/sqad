<p>
<s>
Špaček	Špaček	k1gMnSc1	Špaček
krkavčí	krkavčí	k2eAgMnSc1d1	krkavčí
(	(	kIx(	(
<g/>
Aplonis	Aplonis	k1gInSc1	Aplonis
corvina	corvina	k1gFnSc1	corvina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vyhynulý	vyhynulý	k2eAgMnSc1d1	vyhynulý
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
špačkovití	špačkovitý	k2eAgMnPc1d1	špačkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gMnSc1	druh
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
německý	německý	k2eAgMnSc1d1	německý
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
průzkumník	průzkumník	k1gMnSc1	průzkumník
a	a	k8xC	a
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Heinrich	Heinrich	k1gMnSc1	Heinrich
von	von	k1gInSc4	von
Kittlitz	Kittlitza	k1gFnPc2	Kittlitza
během	během	k7c2	během
svých	svůj	k3xOyFgFnPc2	svůj
cest	cesta	k1gFnPc2	cesta
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Odchytil	odchytit	k5eAaPmAgMnS	odchytit
šest	šest	k4xCc4	šest
exemplářů	exemplář	k1gInPc2	exemplář
těchto	tento	k3xDgMnPc2	tento
ptáků	pták	k1gMnPc2	pták
včetně	včetně	k7c2	včetně
mladých	mladý	k2eAgMnPc2d1	mladý
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gMnSc1	druh
Kittlitz	Kittlitz	k1gMnSc1	Kittlitz
popsal	popsat	k5eAaPmAgMnS	popsat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
Kupfertafeln	Kupfertafeln	k1gMnSc1	Kupfertafeln
zur	zur	k?	zur
Naturgeschichte	Naturgeschicht	k1gInSc5	Naturgeschicht
der	drát	k5eAaImRp2nS	drát
Vögel	Vögel	k1gInSc1	Vögel
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
2	[number]	k4	2
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Lampothornis	Lampothornis	k1gFnSc2	Lampothornis
corvina	corvina	k1gFnSc1	corvina
<g/>
.	.	kIx.	.
</s>
<s>
Ohledně	ohledně	k7c2	ohledně
přesného	přesný	k2eAgNnSc2d1	přesné
data	datum	k1gNnSc2	datum
popsání	popsání	k1gNnSc2	popsání
panovaly	panovat	k5eAaImAgFnP	panovat
zmatky	zmatek	k1gInPc4	zmatek
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
druh	druh	k1gInSc1	druh
popsal	popsat	k5eAaPmAgInS	popsat
roku	rok	k1gInSc2	rok
1832	[number]	k4	1832
nebo	nebo	k8xC	nebo
1833	[number]	k4	1833
<g/>
.	.	kIx.	.
</s>
<s>
Studií	studie	k1gFnPc2	studie
kopií	kopie	k1gFnPc2	kopie
monografie	monografie	k1gFnSc2	monografie
uložených	uložený	k2eAgFnPc2d1	uložená
v	v	k7c6	v
Natural	Natural	k?	Natural
History	Histor	k1gMnPc7	Histor
Museum	museum	k1gNnSc1	museum
at	at	k?	at
Tring	Tringa	k1gFnPc2	Tringa
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
druhý	druhý	k4xOgInSc1	druhý
díl	díl	k1gInSc1	díl
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1833	[number]	k4	1833
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Špaček	Špaček	k1gMnSc1	Špaček
krkavčí	krkavčí	k2eAgMnSc1d1	krkavčí
se	se	k3xPyFc4	se
vyskytoval	vyskytovat	k5eAaImAgMnS	vyskytovat
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Kosrae	Kosra	k1gFnSc2	Kosra
<g/>
,	,	kIx,	,
jednom	jeden	k4xCgMnSc6	jeden
z	z	k7c2	z
ostrovů	ostrov	k1gInPc2	ostrov
Federativních	federativní	k2eAgInPc2d1	federativní
států	stát	k1gInPc2	stát
Mikronésie	Mikronésie	k1gFnSc2	Mikronésie
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
tohoto	tento	k3xDgInSc2	tento
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
100	[number]	k4	100
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Špaček	Špaček	k1gMnSc1	Špaček
obýval	obývat	k5eAaImAgMnS	obývat
zdejší	zdejší	k2eAgInPc4d1	zdejší
horské	horský	k2eAgInPc4d1	horský
lesy	les	k1gInPc4	les
<g/>
,	,	kIx,	,
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
chování	chování	k1gNnSc6	chování
se	se	k3xPyFc4	se
toho	ten	k3xDgMnSc4	ten
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
informace	informace	k1gFnPc4	informace
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
Heinrich	Heinrich	k1gMnSc1	Heinrich
von	von	k1gInSc4	von
Kittlitz	Kittlitza	k1gFnPc2	Kittlitza
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
záznamech	záznam	k1gInPc6	záznam
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Špaček	Špaček	k1gMnSc1	Špaček
krkavčí	krkavčí	k2eAgMnSc1d1	krkavčí
byl	být	k5eAaImAgInS	být
mimořádně	mimořádně	k6eAd1	mimořádně
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
asi	asi	k9	asi
čtvrt	čtvrt	k1gFnSc4	čtvrt
metru	metr	k1gInSc2	metr
vysoký	vysoký	k2eAgInSc1d1	vysoký
druh	druh	k1gInSc1	druh
špačka	špaček	k1gMnSc2	špaček
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
zobákem	zobák	k1gInSc7	zobák
a	a	k8xC	a
ocasem	ocas	k1gInSc7	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peří	k1gNnSc1	peří
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
mělo	mít	k5eAaImAgNnS	mít
černé	černý	k2eAgNnSc1d1	černé
zbarvení	zbarvení	k1gNnSc1	zbarvení
s	s	k7c7	s
odlesky	odlesk	k1gInPc7	odlesk
<g/>
,	,	kIx,	,
černé	černý	k2eAgFnPc1d1	černá
byly	být	k5eAaImAgFnP	být
i	i	k9	i
končetiny	končetina	k1gFnPc1	končetina
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
zbarvený	zbarvený	k2eAgMnSc1d1	zbarvený
byl	být	k5eAaImAgInS	být
i	i	k9	i
zobák	zobák	k1gInSc1	zobák
<g/>
,	,	kIx,	,
podobající	podobající	k2eAgMnSc1d1	podobající
se	se	k3xPyFc4	se
vranímu	vraní	k2eAgInSc3d1	vraní
<g/>
.	.	kIx.	.
</s>
<s>
Duhovka	duhovka	k1gFnSc1	duhovka
měla	mít	k5eAaImAgFnS	mít
zbarvení	zbarvení	k1gNnSc4	zbarvení
rudé	rudý	k2eAgFnSc2d1	rudá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Kosrae	Kosra	k1gFnSc2	Kosra
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
zastávkou	zastávka	k1gFnSc7	zastávka
velrybářů	velrybář	k1gMnPc2	velrybář
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
pochází	pocházet	k5eAaImIp3nS	pocházet
záznam	záznam	k1gInSc4	záznam
o	o	k7c6	o
první	první	k4xOgFnSc6	první
misionářské	misionářský	k2eAgFnSc6d1	misionářská
stanici	stanice	k1gFnSc6	stanice
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
velrybáři	velrybář	k1gMnPc7	velrybář
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
dostaly	dostat	k5eAaPmAgFnP	dostat
i	i	k8xC	i
krysy	krysa	k1gFnPc1	krysa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohly	moct	k5eAaImAgFnP	moct
ptáky	pták	k1gMnPc4	pták
zabíjet	zabíjet	k5eAaImF	zabíjet
<g/>
.	.	kIx.	.
</s>
<s>
Sběratelé	sběratel	k1gMnPc1	sběratel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
dostali	dostat	k5eAaPmAgMnP	dostat
již	již	k6eAd1	již
během	během	k7c2	během
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
tohoto	tento	k3xDgMnSc4	tento
ptáka	pták	k1gMnSc4	pták
bezúspěšně	bezúspěšně	k6eAd1	bezúspěšně
hledali	hledat	k5eAaImAgMnP	hledat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
se	se	k3xPyFc4	se
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
vydal	vydat	k5eAaPmAgMnS	vydat
i	i	k9	i
německý	německý	k2eAgMnSc1d1	německý
etnograf	etnograf	k1gMnSc1	etnograf
a	a	k8xC	a
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Otto	Otto	k1gMnSc1	Otto
Finsch	Finsch	k1gMnSc1	Finsch
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
on	on	k3xPp3gMnSc1	on
zde	zde	k6eAd1	zde
však	však	k9	však
špačka	špaček	k1gMnSc4	špaček
nenalezl	nalézt	k5eNaBmAgMnS	nalézt
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
svaz	svaz	k1gInSc1	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
špačka	špaček	k1gMnSc4	špaček
krkavčího	krkavčí	k2eAgMnSc4d1	krkavčí
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
za	za	k7c4	za
vyhynulý	vyhynulý	k2eAgInSc4d1	vyhynulý
taxon	taxon	k1gInSc4	taxon
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
paleontologa	paleontolog	k1gMnSc2	paleontolog
Juliana	Julian	k1gMnSc4	Julian
Humeho	Hume	k1gMnSc4	Hume
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
vymizení	vymizení	k1gNnSc4	vymizení
mohla	moct	k5eAaImAgFnS	moct
spíše	spíše	k9	spíše
ztráta	ztráta	k1gFnSc1	ztráta
stanoviště	stanoviště	k1gNnSc2	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
druhem	druh	k1gInSc7	druh
vyhynul	vyhynout	k5eAaPmAgInS	vyhynout
během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
i	i	k9	i
chřástal	chřástal	k1gMnSc1	chřástal
mikronéský	mikronéský	k2eAgMnSc1d1	mikronéský
(	(	kIx(	(
<g/>
Porzana	Porzan	k1gMnSc4	Porzan
monasa	monas	k1gMnSc4	monas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Exempláře	exemplář	k1gInPc1	exemplář
těchto	tento	k3xDgMnPc2	tento
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
uchovány	uchovat	k5eAaPmNgInP	uchovat
v	v	k7c6	v
muzeích	muzeum	k1gNnPc6	muzeum
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
a	a	k8xC	a
Leidenu	Leiden	k1gInSc2	Leiden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
FULLER	FULLER	kA	FULLER
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Extinct	Extinct	k2eAgInSc1d1	Extinct
birds	birds	k1gInSc1	birds
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
N.	N.	kA	N.
<g/>
Y.	Y.	kA	Y.
<g/>
:	:	kIx,	:
Facts	Facts	k1gInSc1	Facts
on	on	k3xPp3gMnSc1	on
File	File	k1gFnSc7	File
256	[number]	k4	256
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
816018332	[number]	k4	816018332
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
222	[number]	k4	222
<g/>
−	−	k?	−
<g/>
223	[number]	k4	223
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HUME	HUME	kA	HUME
<g/>
,	,	kIx,	,
Julian	Julian	k1gMnSc1	Julian
P.	P.	kA	P.
<g/>
;	;	kIx,	;
WALTERS	WALTERS	kA	WALTERS
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
.	.	kIx.	.
</s>
<s>
Extinct	Extinct	k2eAgInSc1d1	Extinct
Birds	Birds	k1gInSc1	Birds
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Bloomsbury	Bloomsbur	k1gInPc1	Bloomsbur
Pub	Pub	k1gFnSc2	Pub
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
545	[number]	k4	545
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
9781408157251	[number]	k4	9781408157251
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aplonis	Aplonis	k1gFnSc1	Aplonis
corvina	corvina	k1gFnSc1	corvina
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
iucnredlist	iucnredlist	k1gInSc1	iucnredlist
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HUME	HUME	kA	HUME
<g/>
,	,	kIx,	,
Julian	Julian	k1gMnSc1	Julian
P.	P.	kA	P.
<g/>
;	;	kIx,	;
PATERSON	PATERSON	kA	PATERSON
<g/>
,	,	kIx,	,
Alan	Alan	k1gMnSc1	Alan
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
correct	correct	k2eAgInSc4d1	correct
publication	publication	k1gInSc4	publication
date	datat	k5eAaPmIp3nS	datat
of	of	k?	of
Aplonis	Aplonis	k1gFnSc1	Aplonis
corvina	corvina	k1gFnSc1	corvina
(	(	kIx(	(
<g/>
Kittlitz	Kittlitz	k1gMnSc1	Kittlitz
<g/>
,	,	kIx,	,
1833	[number]	k4	1833
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bulletin	bulletin	k1gInSc1	bulletin
of	of	k?	of
the	the	k?	the
British	British	k1gInSc1	British
Ornithologists	Ornithologists	k1gInSc1	Ornithologists
<g/>
'	'	kIx"	'
Club	club	k1gInSc1	club
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
207	[number]	k4	207
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
špaček	špaček	k1gMnSc1	špaček
krkavčí	krkavčí	k2eAgMnSc1d1	krkavčí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Aplonis	Aplonis	k1gFnSc2	Aplonis
corvina	corvin	k2eAgFnSc1d1	corvin
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
