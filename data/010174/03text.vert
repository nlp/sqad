<p>
<s>
JAXA	JAXA	kA	JAXA
<g/>
,	,	kIx,	,
Japan	japan	k1gInSc4	japan
Aerospace	Aerospace	k1gFnSc2	Aerospace
Exploration	Exploration	k1gInSc4	Exploration
Agency	Agenca	k1gFnSc2	Agenca
(	(	kIx(	(
<g/>
宇	宇	k?	宇
nebo	nebo	k8xC	nebo
ジ	ジ	k?	ジ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
japonská	japonský	k2eAgFnSc1d1	japonská
národní	národní	k2eAgFnSc1d1	národní
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
agentura	agentura	k1gFnSc1	agentura
<g/>
.	.	kIx.	.
</s>
<s>
JAXA	JAXA	kA	JAXA
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2003	[number]	k4	2003
spojením	spojení	k1gNnSc7	spojení
tří	tři	k4xCgFnPc2	tři
dosud	dosud	k6eAd1	dosud
nezávislých	závislý	k2eNgFnPc2d1	nezávislá
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Agentura	agentura	k1gFnSc1	agentura
již	již	k6eAd1	již
úspěšně	úspěšně	k6eAd1	úspěšně
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
umístění	umístění	k1gNnSc4	umístění
družice	družice	k1gFnSc2	družice
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zapojena	zapojen	k2eAgFnSc1d1	zapojena
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgInPc2d1	další
projektů	projekt	k1gInPc2	projekt
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
průzkum	průzkum	k1gInSc1	průzkum
asteroidů	asteroid	k1gInPc2	asteroid
a	a	k8xC	a
případná	případný	k2eAgFnSc1d1	případná
pilotovaná	pilotovaný	k2eAgFnSc1d1	pilotovaná
cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
agentura	agentura	k1gFnSc1	agentura
JAXA	JAXA	kA	JAXA
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2003	[number]	k4	2003
sloučením	sloučení	k1gNnSc7	sloučení
tří	tři	k4xCgFnPc2	tři
nezávislých	závislý	k2eNgFnPc2d1	nezávislá
agentur	agentura	k1gFnPc2	agentura
<g/>
,	,	kIx,	,
ISAS	ISAS	kA	ISAS
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
NAL	NAL	kA	NAL
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
a	a	k8xC	a
NASDA	NASDA	kA	NASDA
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Akari	Akari	k6eAd1	Akari
</s>
</p>
<p>
<s>
IKAROS	Ikaros	k1gMnSc1	Ikaros
–	–	k?	–
první	první	k4xOgFnSc2	první
sluneční	sluneční	k2eAgFnSc2d1	sluneční
plachetnice	plachetnice	k1gFnSc2	plachetnice
</s>
</p>
<p>
<s>
Hajabusa	Hajabus	k1gMnSc4	Hajabus
</s>
</p>
<p>
<s>
Kibó	Kibó	k?	Kibó
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
JAXA	JAXA	kA	JAXA
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
JAXA	JAXA	kA	JAXA
na	na	k7c4	na
Techblog	Techblog	k1gInSc4	Techblog
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Jaxa	Jaxa	k6eAd1	Jaxa
na	na	k7c6	na
webu	web	k1gInSc6	web
Observatory	Observator	k1gInPc1	Observator
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
