<s>
Před	před	k7c7	před
obdobím	období	k1gNnSc7	období
relativního	relativní	k2eAgInSc2d1	relativní
klidu	klid	k1gInSc2	klid
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
sporů	spor	k1gInPc2	spor
podařilo	podařit	k5eAaPmAgNnS	podařit
urovnat	urovnat	k5eAaPmF	urovnat
diplomatickou	diplomatický	k2eAgFnSc7d1	diplomatická
cestou	cesta	k1gFnSc7	cesta
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgInSc4d1	poslední
celoevropský	celoevropský	k2eAgInSc4d1	celoevropský
konflikt	konflikt	k1gInSc4	konflikt
byla	být	k5eAaImAgFnS	být
třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
skončila	skončit	k5eAaPmAgFnS	skončit
roku	rok	k1gInSc2	rok
1648	[number]	k4	1648
<g/>
;	;	kIx,	;
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nicméně	nicméně	k8xC	nicméně
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
méně	málo	k6eAd2	málo
významné	významný	k2eAgInPc4d1	významný
válečné	válečný	k2eAgInPc4d1	válečný
konflikty	konflikt	k1gInPc4	konflikt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byla	být	k5eAaImAgFnS	být
válka	válka	k1gFnSc1	válka
o	o	k7c4	o
španělské	španělský	k2eAgNnSc4d1	španělské
dědictví	dědictví	k1gNnSc4	dědictví
či	či	k8xC	či
sedmiletá	sedmiletý	k2eAgFnSc1d1	sedmiletá
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
kontinentu	kontinent	k1gInSc2	kontinent
zasažena	zasáhnout	k5eAaPmNgFnS	zasáhnout
nejprve	nejprve	k6eAd1	nejprve
tzv.	tzv.	kA	tzv.
revolučními	revoluční	k2eAgFnPc7d1	revoluční
válkami	válka	k1gFnPc7	válka
(	(	kIx(	(
<g/>
války	válka	k1gFnPc1	válka
s	s	k7c7	s
Francouzskou	francouzský	k2eAgFnSc7d1	francouzská
republikou	republika	k1gFnSc7	republika
<g/>
)	)	kIx)	)
a	a	k8xC	a
poté	poté	k6eAd1	poté
válkami	válka	k1gFnPc7	válka
napoleonskými	napoleonský	k2eAgFnPc7d1	napoleonská
<g/>
.	.	kIx.	.
</s>
