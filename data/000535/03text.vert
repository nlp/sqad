<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Číny	Čína	k1gFnSc2	Čína
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
<g/>
:	:	kIx,	:
中	中	k?	中
<g/>
;	;	kIx,	;
pinyin	pinyin	k1gMnSc1	pinyin
<g/>
:	:	kIx,	:
Zhō	Zhō	k1gMnSc1	Zhō
Gò	Gò	k1gMnSc1	Gò
<g/>
;	;	kIx,	;
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
<g/>
:	:	kIx,	:
Čung-kuo	Čunguo	k1gMnSc1	Čung-kuo
Kung-čchan-tang	Kung-čchanang	k1gMnSc1	Kung-čchan-tang
<g/>
;	;	kIx,	;
zkratka	zkratka	k1gFnSc1	zkratka
<g/>
:	:	kIx,	:
中	中	k?	中
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čínská	čínský	k2eAgFnSc1d1	čínská
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
její	její	k3xOp3gFnPc1	její
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
zvítězily	zvítězit	k5eAaPmAgFnP	zvítězit
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
nad	nad	k7c7	nad
Nacionalisty	nacionalista	k1gMnPc7	nacionalista
<g/>
,	,	kIx,	,
zastává	zastávat	k5eAaImIp3nS	zastávat
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
úlohu	úloha	k1gFnSc4	úloha
při	při	k7c6	při
řízení	řízení	k1gNnSc6	řízení
komunistické	komunistický	k2eAgFnSc2d1	komunistická
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
je	být	k5eAaImIp3nS	být
jejím	její	k3xOp3gMnSc7	její
předsedou	předseda	k1gMnSc7	předseda
Si	se	k3xPyFc3	se
Ťin-pching	Ťinching	k1gInSc4	Ťin-pching
<g/>
.	.	kIx.	.
</s>
<s>
Počtem	počet	k1gInSc7	počet
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
podle	podle	k7c2	podle
čínských	čínský	k2eAgNnPc2d1	čínské
státních	státní	k2eAgNnPc2d1	státní
médií	médium	k1gNnPc2	médium
přesahoval	přesahovat	k5eAaImAgInS	přesahovat
80	[number]	k4	80
miliónů	milión	k4xCgInPc2	milión
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Číny	Čína	k1gFnSc2	Čína
největší	veliký	k2eAgFnSc1d3	veliký
politickou	politický	k2eAgFnSc7d1	politická
stranou	strana	k1gFnSc7	strana
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Komunistickou	komunistický	k2eAgFnSc4d1	komunistická
stranu	strana	k1gFnSc4	strana
Číny	Čína	k1gFnSc2	Čína
tvořila	tvořit	k5eAaImAgFnS	tvořit
při	při	k7c6	při
založení	založení	k1gNnSc6	založení
malá	malý	k2eAgFnSc1d1	malá
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
delegáti	delegát	k1gMnPc1	delegát
I.	I.	kA	I.
sjezdu	sjezd	k1gInSc6	sjezd
zastupovali	zastupovat	k5eAaImAgMnP	zastupovat
přes	přes	k7c4	přes
50	[number]	k4	50
členů	člen	k1gInPc2	člen
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Početnost	početnost	k1gFnSc1	početnost
strany	strana	k1gFnSc2	strana
postupně	postupně	k6eAd1	postupně
rostla	růst	k5eAaImAgFnS	růst
<g/>
,	,	kIx,	,
na	na	k7c4	na
tisíc	tisíc	k4xCgInSc4	tisíc
členů	člen	k1gMnPc2	člen
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
necelých	celý	k2eNgInPc2d1	necelý
58	[number]	k4	58
tisíc	tisíc	k4xCgInPc2	tisíc
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
40	[number]	k4	40
tisíc	tisíc	k4xCgInPc2	tisíc
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
a	a	k8xC	a
stoupl	stoupnout	k5eAaPmAgInS	stoupnout
na	na	k7c4	na
1,2	[number]	k4	1,2
miliónu	milión	k4xCgInSc2	milión
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
založení	založení	k1gNnSc2	založení
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
velikost	velikost	k1gFnSc1	velikost
strany	strana	k1gFnSc2	strana
několikanásobně	několikanásobně	k6eAd1	několikanásobně
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
podle	podle	k7c2	podle
čínských	čínský	k2eAgNnPc2d1	čínské
státních	státní	k2eAgNnPc2d1	státní
médií	médium	k1gNnPc2	médium
měla	mít	k5eAaImAgFnS	mít
KS	ks	kA	ks
Číny	Čína	k1gFnSc2	Čína
73	[number]	k4	73
miliónů	milión	k4xCgInPc2	milión
360	[number]	k4	360
tisíc	tisíc	k4xCgInSc4	tisíc
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
jejich	jejich	k3xOp3gNnSc2	jejich
počet	počet	k1gInSc1	počet
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
na	na	k7c4	na
80	[number]	k4	80
269	[number]	k4	269
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
21,017	[number]	k4	21,017
miliónu	milión	k4xCgInSc2	milión
osob	osoba	k1gFnPc2	osoba
požádalo	požádat	k5eAaPmAgNnS	požádat
o	o	k7c4	o
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
3,075	[number]	k4	3,075
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
a	a	k8xC	a
801	[number]	k4	801
tisíc	tisíc	k4xCgInPc2	tisíc
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
nebo	nebo	k8xC	nebo
stranu	strana	k1gFnSc4	strana
opustilo	opustit	k5eAaPmAgNnS	opustit
<g/>
;	;	kIx,	;
ze	z	k7c2	z
členů	člen	k1gMnPc2	člen
strany	strana	k1gFnSc2	strana
bylo	být	k5eAaImAgNnS	být
22,5	[number]	k4	22,5
%	%	kIx~	%
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
6,6	[number]	k4	6,6
%	%	kIx~	%
příslušníků	příslušník	k1gMnPc2	příslušník
národnostních	národnostní	k2eAgFnPc2d1	národnostní
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
mediální	mediální	k2eAgFnSc1d1	mediální
skupina	skupina	k1gFnSc1	skupina
The	The	k1gFnSc2	The
Epoch	epocha	k1gFnPc2	epocha
Times	Times	k1gMnSc1	Times
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2004	[number]	k4	2004
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
sérii	série	k1gFnSc4	série
článků	článek	k1gInPc2	článek
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Devět	devět	k4xCc1	devět
komentářů	komentář	k1gInPc2	komentář
ke	k	k7c3	k
komunistické	komunistický	k2eAgFnSc3d1	komunistická
straně	strana	k1gFnSc3	strana
<g/>
.	.	kIx.	.
</s>
<s>
Dokument	dokument	k1gInSc1	dokument
podává	podávat	k5eAaImIp3nS	podávat
necenzurovanou	cenzurovaný	k2eNgFnSc4d1	necenzurovaná
historii	historie	k1gFnSc4	historie
Čínské	čínský	k2eAgFnSc2d1	čínská
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
povaze	povaha	k1gFnSc6	povaha
<g/>
,	,	kIx,	,
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
postavila	postavit	k5eAaPmAgFnS	postavit
proti	proti	k7c3	proti
tradičním	tradiční	k2eAgFnPc3d1	tradiční
formám	forma	k1gFnPc3	forma
čínské	čínský	k2eAgFnSc2d1	čínská
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
o	o	k7c6	o
vztazích	vztah	k1gInPc6	vztah
mezi	mezi	k7c7	mezi
členy	člen	k1gMnPc7	člen
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
vyjmenovává	vyjmenovávat	k5eAaImIp3nS	vyjmenovávat
zločiny	zločin	k1gInPc4	zločin
<g/>
,	,	kIx,	,
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
dopustila	dopustit	k5eAaPmAgNnP	dopustit
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
po	po	k7c6	po
zveřejnění	zveřejnění	k1gNnSc6	zveřejnění
dokumentu	dokument	k1gInSc2	dokument
se	se	k3xPyFc4	se
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
zvedla	zvednout	k5eAaPmAgFnS	zvednout
vlna	vlna	k1gFnSc1	vlna
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
Číňané	Číňan	k1gMnPc1	Číňan
založili	založit	k5eAaPmAgMnP	založit
Centra	centr	k1gMnSc4	centr
pro	pro	k7c4	pro
vystupování	vystupování	k1gNnSc4	vystupování
z	z	k7c2	z
KS	ks	kA	ks
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
také	také	k9	také
vzniká	vznikat	k5eAaImIp3nS	vznikat
lidové	lidový	k2eAgNnSc1d1	lidové
hnutí	hnutí	k1gNnSc1	hnutí
zvané	zvaný	k2eAgNnSc1d1	zvané
"	"	kIx"	"
<g/>
Tuidang	Tuidang	k1gInSc1	Tuidang
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
zpochybňuje	zpochybňovat	k5eAaImIp3nS	zpochybňovat
její	její	k3xOp3gInSc4	její
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
status	status	k1gInSc4	status
quo	quo	k?	quo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vlivu	vliv	k1gInSc2	vliv
hnutí	hnutí	k1gNnSc2	hnutí
Tuidang	Tuidanga	k1gFnPc2	Tuidanga
začaly	začít	k5eAaPmAgFnP	začít
tisíce	tisíc	k4xCgInPc1	tisíc
Číňanů	Číňan	k1gMnPc2	Číňan
vystupovat	vystupovat	k5eAaImF	vystupovat
z	z	k7c2	z
KS	ks	kA	ks
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
jejích	její	k3xOp3gFnPc2	její
spřízněných	spřízněný	k2eAgFnPc2d1	spřízněná
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
přijal	přijmout	k5eAaPmAgMnS	přijmout
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
španělský	španělský	k2eAgMnSc1d1	španělský
soudce	soudce	k1gMnSc1	soudce
Ismael	Ismael	k1gMnSc1	Ismael
Moreno	Moren	k2eAgNnSc1d1	Moreno
žalobu	žaloba	k1gFnSc4	žaloba
za	za	k7c4	za
genocidu	genocida	k1gFnSc4	genocida
a	a	k8xC	a
mučení	mučení	k1gNnSc4	mučení
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
obviněnými	obviněný	k1gMnPc7	obviněný
figuruje	figurovat	k5eAaImIp3nS	figurovat
pět	pět	k4xCc1	pět
vysokých	vysoká	k1gFnPc2	vysoká
představitelů	představitel	k1gMnPc2	představitel
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
zodpovídat	zodpovídat	k5eAaImF	zodpovídat
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
při	při	k7c6	při
pronásledování	pronásledování	k1gNnSc6	pronásledování
duchovní	duchovní	k2eAgFnSc2d1	duchovní
praxe	praxe	k1gFnSc2	praxe
Falun	Falun	k1gMnSc1	Falun
Gong	gong	k1gInSc1	gong
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
soud	soud	k1gInSc1	soud
uznal	uznat	k5eAaPmAgInS	uznat
kampaň	kampaň	k1gFnSc4	kampaň
proti	proti	k7c3	proti
Falun	Falun	k1gNnSc4	Falun
Gongu	gong	k1gInSc2	gong
za	za	k7c4	za
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
definici	definice	k1gFnSc4	definice
genocidy	genocida	k1gFnSc2	genocida
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
právního	právní	k2eAgNnSc2d1	právní
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
obviněnými	obviněný	k2eAgMnPc7d1	obviněný
je	být	k5eAaImIp3nS	být
i	i	k9	i
bývalý	bývalý	k2eAgMnSc1d1	bývalý
vůdce	vůdce	k1gMnSc1	vůdce
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
Ťiang	Ťiang	k1gMnSc1	Ťiang
Ce-min	Cein	k1gInSc1	Ce-min
(	(	kIx(	(
<g/>
Jiang	Jiang	k1gInSc1	Jiang
Zemin	zemina	k1gFnPc2	zemina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Luo	Luo	k1gMnSc1	Luo
Kan	Kan	k1gMnSc1	Kan
(	(	kIx(	(
<g/>
Luo	Luo	k1gMnSc1	Luo
Gan	Gan	k1gMnSc1	Gan
<g/>
)	)	kIx)	)
-	-	kIx~	-
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ředitel	ředitel	k1gMnSc1	ředitel
Úřadu	úřad	k1gInSc2	úřad
610	[number]	k4	610
<g/>
,	,	kIx,	,
Po	po	k7c6	po
Si-laj	Siaj	k1gMnSc1	Si-laj
(	(	kIx(	(
<g/>
Bo	Bo	k?	Bo
Xilai	Xila	k1gFnSc2	Xila
<g/>
)	)	kIx)	)
-	-	kIx~	-
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
Ťia	Ťia	k1gMnSc1	Ťia
Čching-lin	Čchingina	k1gFnPc2	Čching-lina
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Jia	Jia	k1gFnSc7	Jia
Qinglin	Qinglin	k2eAgInSc4d1	Qinglin
<g/>
)	)	kIx)	)
-	-	kIx~	-
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
nejvýše	nejvýše	k6eAd1	nejvýše
postavený	postavený	k2eAgMnSc1d1	postavený
člen	člen	k1gMnSc1	člen
Strany	strana	k1gFnSc2	strana
a	a	k8xC	a
Wu	Wu	k1gMnSc1	Wu
Kuan-čeng	Kuan-čeng	k1gMnSc1	Kuan-čeng
(	(	kIx(	(
<g/>
Wu	Wu	k1gMnSc1	Wu
Guanzheng	Guanzheng	k1gMnSc1	Guanzheng
<g/>
)	)	kIx)	)
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
interního	interní	k2eAgInSc2d1	interní
disciplinárního	disciplinární	k2eAgInSc2d1	disciplinární
výboru	výbor	k1gInSc2	výbor
Strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
vynesl	vynést	k5eAaPmAgMnS	vynést
Argentinský	argentinský	k2eAgMnSc1d1	argentinský
soudce	soudce	k1gMnSc1	soudce
Octavio	Octavio	k1gMnSc1	Octavio
Araoz	Araoz	k1gMnSc1	Araoz
de	de	k?	de
Lamadrid	Lamadrid	k1gInSc1	Lamadrid
z	z	k7c2	z
Federálního	federální	k2eAgInSc2d1	federální
soudu	soud	k1gInSc2	soud
č.	č.	k?	č.
9	[number]	k4	9
rozsudek	rozsudek	k1gInSc1	rozsudek
v	v	k7c6	v
historickém	historický	k2eAgInSc6d1	historický
procesu	proces	k1gInSc6	proces
se	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
bývalými	bývalý	k2eAgMnPc7d1	bývalý
čelními	čelní	k2eAgMnPc7d1	čelní
čínskými	čínský	k2eAgMnPc7d1	čínský
funkcionáři	funkcionář	k1gMnPc7	funkcionář
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
předseda	předseda	k1gMnSc1	předseda
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Číny	Čína	k1gFnSc2	Čína
Ťiang	Ťiang	k1gMnSc1	Ťiang
Ce-min	Cein	k1gInSc1	Ce-min
(	(	kIx(	(
<g/>
Jiang	Jiang	k1gInSc1	Jiang
Zemin	zemina	k1gFnPc2	zemina
<g/>
)	)	kIx)	)
a	a	k8xC	a
šéf	šéf	k1gMnSc1	šéf
Úřadu	úřad	k1gInSc2	úřad
610	[number]	k4	610
Luo	Luo	k1gMnSc1	Luo
Kan	Kan	k1gMnSc1	Kan
(	(	kIx(	(
<g/>
Luo	Luo	k1gMnSc1	Luo
Gan	Gan	k1gMnSc1	Gan
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
souzeni	souzen	k2eAgMnPc1d1	souzen
za	za	k7c4	za
jejich	jejich	k3xOp3gFnSc4	jejich
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
ve	v	k7c6	v
stále	stále	k6eAd1	stále
probíhajícím	probíhající	k2eAgInSc6d1	probíhající
pronásledování	pronásledování	k1gNnSc2	pronásledování
milionů	milion	k4xCgInPc2	milion
praktikujících	praktikující	k2eAgInPc2d1	praktikující
duchovní	duchovní	k1gMnSc1	duchovní
disciplíny	disciplína	k1gFnSc2	disciplína
Falun	Falun	k1gMnSc1	Falun
Gong	gong	k1gInSc1	gong
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
čínského	čínský	k2eAgInSc2d1	čínský
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
rozsudku	rozsudek	k1gInSc2	rozsudek
je	být	k5eAaImIp3nS	být
i	i	k8xC	i
nařízení	nařízení	k1gNnSc4	nařízení
k	k	k7c3	k
zadržení	zadržení	k1gNnSc3	zadržení
obou	dva	k4xCgMnPc2	dva
funkcionářů	funkcionář	k1gMnPc2	funkcionář
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
obvinění	obviněný	k1gMnPc1	obviněný
vycestují	vycestovat	k5eAaPmIp3nP	vycestovat
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
Argentinou	Argentina	k1gFnSc7	Argentina
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c4	o
vydávání	vydávání	k1gNnSc4	vydávání
zločinců	zločinec	k1gMnPc2	zločinec
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
zadrženi	zadržet	k5eAaPmNgMnP	zadržet
a	a	k8xC	a
převezeni	převézt	k5eAaPmNgMnP	převézt
do	do	k7c2	do
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
postaveni	postavit	k5eAaPmNgMnP	postavit
před	před	k7c4	před
soud	soud	k1gInSc4	soud
a	a	k8xC	a
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
prohlášení	prohlášení	k1gNnSc4	prohlášení
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
obhajobu	obhajoba	k1gFnSc4	obhajoba
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
soudu	soud	k1gInSc2	soud
představuje	představovat	k5eAaImIp3nS	představovat
historický	historický	k2eAgInSc4d1	historický
precedent	precedent	k1gInSc4	precedent
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc1	stav
po	po	k7c6	po
XVII	XVII	kA	XVII
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc6	sjezd
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
Generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
Ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Číny	Čína	k1gFnSc2	Čína
<g/>
:	:	kIx,	:
Si	se	k3xPyFc3	se
Ťin-pching	Ťinching	k1gInSc4	Ťin-pching
Členové	člen	k1gMnPc1	člen
stálého	stálý	k2eAgInSc2d1	stálý
výboru	výbor	k1gInSc2	výbor
politbyra	politbyro	k1gNnSc2	politbyro
ÚV	ÚV	kA	ÚV
KS	ks	kA	ks
Číny	Čína	k1gFnSc2	Čína
<g/>
:	:	kIx,	:
Čou	Čou	k1gMnSc1	Čou
Jung-kchang	Jungchang	k1gMnSc1	Jung-kchang
Che	che	k0	che
Kuo-čchiang	Kuo-čchianga	k1gFnPc2	Kuo-čchianga
Chu	Chu	k1gMnSc1	Chu
Ťin-tchao	Ťinchao	k1gMnSc1	Ťin-tchao
Li	li	k8xS	li
Čchang-čchun	Čchang-čchun	k1gMnSc1	Čchang-čchun
Li	li	k9	li
Kche-čchiang	Kche-čchiang	k1gInSc4	Kche-čchiang
Si	se	k3xPyFc3	se
Ťin-pching	Ťinching	k1gInSc4	Ťin-pching
Ťia	Ťia	k1gFnSc2	Ťia
Čching-lin	Čchingina	k1gFnPc2	Čching-lina
Wen	Wen	k1gMnSc1	Wen
Ťia-pao	Ťiaao	k1gMnSc1	Ťia-pao
Wu	Wu	k1gMnSc1	Wu
Pang-kuo	Panguo	k1gMnSc1	Pang-kuo
<g />
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
politbyra	politbyro	k1gNnSc2	politbyro
ÚV	ÚV	kA	ÚV
KS	ks	kA	ks
Číny	Čína	k1gFnSc2	Čína
<g/>
:	:	kIx,	:
Čang	Čang	k1gMnSc1	Čang
Kao	Kao	k1gMnSc1	Kao
<g/>
-li	i	k?	-li
Čang	Čang	k1gMnSc1	Čang
Te-ťiang	Te-ťiang	k1gMnSc1	Te-ťiang
Čou	Čou	k1gMnSc1	Čou
Jung-kchang	Jungchang	k1gMnSc1	Jung-kchang
Che	che	k0	che
Kuo-čchiang	Kuo-čchiang	k1gInSc4	Kuo-čchiang
Chu	Chu	k1gMnSc7	Chu
Ťin-tchao	Ťinchao	k6eAd1	Ťin-tchao
Chuej	Chuej	k1gInSc4	Chuej
Liang-jü	Liangü	k1gFnPc2	Liang-jü
Jü	Jü	k1gMnSc1	Jü
Čeng-šeng	Čeng-šeng	k1gMnSc1	Čeng-šeng
Kuo	Kuo	k1gMnSc1	Kuo
Po-siung	Poiung	k1gMnSc1	Po-siung
Liou	Lio	k2eAgFnSc4d1	Lio
Čchi	Čche	k1gFnSc4	Čche
Liou	Lious	k1gInSc2	Lious
Jen-tung	Jenung	k1gInSc1	Jen-tung
Liou	Lious	k1gInSc2	Lious
Jün-šan	Jün-šana	k1gFnPc2	Jün-šana
Li	li	k8xS	li
Čchang-čchun	Čchang-čchuna	k1gFnPc2	Čchang-čchuna
Li	li	k8xS	li
Jüan-čchao	Jüan-čchao	k6eAd1	Jüan-čchao
Li	li	k9	li
Kche-čchiang	Kche-čchiang	k1gInSc4	Kche-čchiang
Po	po	k7c4	po
Si-laj	Siaj	k1gInSc4	Si-laj
Si	se	k3xPyFc3	se
Ťin-pching	Ťinching	k1gInSc4	Ťin-pching
Sü	Sü	k1gMnSc7	Sü
Cchaj-chou	Cchajha	k1gMnSc7	Cchaj-cha
Ťia	Ťia	k1gMnSc7	Ťia
Čching-lin	Čchingin	k2eAgMnSc1d1	Čching-lin
Wang	Wang	k1gMnSc1	Wang
Čao-kuo	Čaouo	k1gMnSc1	Čao-kuo
Wang	Wang	k1gMnSc1	Wang
Čchi-šan	Čchi-šan	k1gMnSc1	Čchi-šan
Wang	Wang	k1gMnSc1	Wang
<g />
.	.	kIx.	.
</s>
<s>
Jang	Jang	k1gMnSc1	Jang
Wang	Wang	k1gMnSc1	Wang
Kang	Kang	k1gMnSc1	Kang
Wang	Wang	k1gMnSc1	Wang
Le-čchüan	Le-čchüan	k1gMnSc1	Le-čchüan
Wen	Wen	k1gMnSc1	Wen
Ťia-pao	Ťiaao	k1gMnSc1	Ťia-pao
Wu	Wu	k1gFnSc3	Wu
Pang-kuo	Panguo	k6eAd1	Pang-kuo
Tajemníci	tajemník	k1gMnPc1	tajemník
sekretariátu	sekretariát	k1gInSc2	sekretariát
ÚV	ÚV	kA	ÚV
KS	ks	kA	ks
Číny	Čína	k1gFnSc2	Čína
<g/>
:	:	kIx,	:
Che	che	k0	che
Jung	Jung	k1gMnSc1	Jung
Li	li	k8xS	li
Jüan-čchao	Jüan-čchao	k1gMnSc1	Jüan-čchao
Ling	Ling	k1gMnSc1	Ling
Ťi-chua	Ťihua	k1gMnSc1	Ťi-chua
Liou	Lioa	k1gFnSc4	Lioa
Jün-šan	Jün-šany	k1gInPc2	Jün-šany
Si	se	k3xPyFc3	se
Ťin-pching	Ťinching	k1gInSc1	Ťin-pching
Wang	Wang	k1gMnSc1	Wang
Chu-ning	Chuing	k1gInSc1	Chu-ning
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
vojenská	vojenský	k2eAgFnSc1d1	vojenská
komise	komise	k1gFnSc1	komise
KS	ks	kA	ks
Číny	Čína	k1gFnSc2	Čína
<g/>
:	:	kIx,	:
Předseda	předseda	k1gMnSc1	předseda
<g/>
:	:	kIx,	:
Chu	Chu	k1gMnSc1	Chu
Ťin-tchao	Ťinchao	k6eAd1	Ťin-tchao
Místopředsedové	místopředseda	k1gMnPc1	místopředseda
<g/>
:	:	kIx,	:
Kuo	Kuo	k1gMnSc1	Kuo
Po-siung	Poiung	k1gMnSc1	Po-siung
Sü	Sü	k1gMnSc1	Sü
Cchaj-chou	Cchajhý	k2eAgFnSc4d1	Cchaj-chý
Si	se	k3xPyFc3	se
Ťin-pching	Ťinching	k1gInSc1	Ťin-pching
Členové	člen	k1gMnPc1	člen
<g />
.	.	kIx.	.
</s>
<s>
komise	komise	k1gFnSc1	komise
<g/>
:	:	kIx,	:
Čchang	Čchang	k1gInSc1	Čchang
Wan-čchüan	Wan-čchüana	k1gFnPc2	Wan-čchüana
Čchen	Čchna	k1gFnPc2	Čchna
Ping-te	Ping	k1gInSc5	Ping-t
Li	li	k8xS	li
Ťi-naj	Ťiaj	k1gMnSc1	Ťi-naj
Liang	Liang	k1gMnSc1	Liang
Kuang-lie	Kuangie	k1gFnSc2	Kuang-lie
Liao	Liao	k1gMnSc1	Liao
Si-lung	Siung	k1gMnSc1	Si-lung
Sü	Sü	k1gMnSc1	Sü
Čchi-liang	Čchiiang	k1gMnSc1	Čchi-liang
Ťing	Ťing	k1gMnSc1	Ťing
Č	Č	kA	Č
<g/>
'	'	kIx"	'
<g/>
-jüan	üan	k1gMnSc1	-jüan
Wu	Wu	k1gMnSc1	Wu
Šeng	Šenga	k1gFnPc2	Šenga
<g/>
-li	i	k?	-li
Ústřední	ústřední	k2eAgFnSc2d1	ústřední
disciplinární	disciplinární	k2eAgFnSc2d1	disciplinární
komise	komise	k1gFnSc2	komise
KS	ks	kA	ks
Číny	Čína	k1gFnSc2	Čína
<g/>
:	:	kIx,	:
Tajemník	tajemník	k1gMnSc1	tajemník
komise	komise	k1gFnSc2	komise
<g/>
:	:	kIx,	:
Che	che	k0	che
Kuo-čchiang	Kuo-čchiang	k1gInSc4	Kuo-čchiang
Podtajemníci	podtajemník	k1gMnPc1	podtajemník
<g/>
:	:	kIx,	:
Čang	Čang	k1gMnSc1	Čang
Chuej-sin	Chuejin	k1gMnSc1	Chuej-sin
Čang	Čang	k1gMnSc1	Čang
I	i	k9	i
Che	che	k0	che
Jung	Jung	k1gMnSc1	Jung
Chuang	Chuanga	k1gFnPc2	Chuanga
Šu-sien	Šuien	k1gInSc1	Šu-sien
Kan	Kan	k1gMnSc1	Kan
I-šeng	I-šeng	k1gInSc1	I-šeng
Li	li	k8xS	li
Jü-ping	Jüing	k1gInSc1	Jü-ping
Ma	Ma	k1gMnSc1	Ma
Wen	Wen	k1gMnSc1	Wen
Sun	Sun	kA	Sun
Čung-tchung	Čungchung	k1gMnSc1	Čung-tchung
</s>
