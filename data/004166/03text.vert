<s>
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
1993	[number]	k4	1993
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc1	první
ročník	ročník	k1gInSc1	ročník
výročních	výroční	k2eAgFnPc2d1	výroční
cen	cena	k1gFnPc2	cena
České	český	k2eAgFnSc2d1	Česká
filmové	filmový	k2eAgFnSc2d1	filmová
a	a	k8xC	a
televizní	televizní	k2eAgFnSc2d1	televizní
akademie	akademie	k1gFnSc2	akademie
Český	český	k2eAgMnSc1d1	český
lev	lev	k1gMnSc1	lev
<g/>
.	.	kIx.	.
</s>
<s>
Proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
ještě	ještě	k9	ještě
bez	bez	k7c2	bez
nominací	nominace	k1gFnPc2	nominace
<g/>
.	.	kIx.	.
</s>
<s>
Hlasováním	hlasování	k1gNnSc7	hlasování
poroty	porota	k1gFnSc2	porota
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
kategorii	kategorie	k1gFnSc6	kategorie
vyhodnocena	vyhodnocen	k2eAgFnSc1d1	vyhodnocena
vždy	vždy	k6eAd1	vždy
první	první	k4xOgNnPc4	první
tři	tři	k4xCgNnPc4	tři
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrce	tvůrce	k1gMnSc1	tvůrce
či	či	k8xC	či
film	film	k1gInSc1	film
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
pak	pak	k6eAd1	pak
získal	získat	k5eAaPmAgMnS	získat
Českého	český	k2eAgMnSc4d1	český
lva	lev	k1gMnSc4	lev
<g/>
.	.	kIx.	.
</s>
<s>
Vítězové	vítěz	k1gMnPc1	vítěz
byli	být	k5eAaImAgMnP	být
vyhlášeni	vyhlásit	k5eAaPmNgMnP	vyhlásit
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Večerem	večer	k1gInSc7	večer
provázeli	provázet	k5eAaImAgMnP	provázet
Roman	Roman	k1gMnSc1	Roman
Holý	Holý	k1gMnSc1	Holý
a	a	k8xC	a
Miloš	Miloš	k1gMnSc1	Miloš
Kohout	Kohout	k1gMnSc1	Kohout
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgInSc7d3	nejlepší
filmem	film	k1gInSc7	film
byl	být	k5eAaImAgMnS	být
vyhlášen	vyhlášen	k2eAgInSc4d1	vyhlášen
muzikálový	muzikálový	k2eAgInSc4d1	muzikálový
snímek	snímek	k1gInSc4	snímek
Šakalí	šakalí	k2eAgNnPc4d1	šakalí
léta	léto	k1gNnPc4	léto
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
také	také	k9	také
získal	získat	k5eAaPmAgInS	získat
Lvů	lev	k1gInPc2	lev
nejvíce	nejvíce	k6eAd1	nejvíce
<g/>
.	.	kIx.	.
</s>
<s>
Šakalí	šakalí	k2eAgNnPc1d1	šakalí
léta	léto	k1gNnPc1	léto
Jan	Jan	k1gMnSc1	Jan
Hřebejk	Hřebejk	k1gMnSc1	Hřebejk
(	(	kIx(	(
<g/>
Šakalí	šakalí	k2eAgNnPc4d1	šakalí
léta	léto	k1gNnPc4	léto
<g/>
)	)	kIx)	)
Václav	Václav	k1gMnSc1	Václav
Šašek	Šašek	k1gMnSc1	Šašek
(	(	kIx(	(
<g/>
Helimadoe	Helimadoe	k1gFnSc1	Helimadoe
<g/>
)	)	kIx)	)
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Brabec	Brabec	k1gMnSc1	Brabec
(	(	kIx(	(
<g/>
Krvavý	krvavý	k2eAgInSc1d1	krvavý
román	román	k1gInSc1	román
<g/>
)	)	kIx)	)
Ivan	Ivan	k1gMnSc1	Ivan
Hlas	hlas	k1gInSc1	hlas
(	(	kIx(	(
<g/>
Šakalí	šakalí	k2eAgNnPc1d1	šakalí
léta	léto	k1gNnPc1	léto
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Brožek	Brožek	k1gMnSc1	Brožek
(	(	kIx(	(
<g/>
Krvavý	krvavý	k2eAgInSc1d1	krvavý
román	román	k1gInSc1	román
<g/>
)	)	kIx)	)
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Brabec	Brabec	k1gMnSc1	Brabec
(	(	kIx(	(
<g/>
Krvavý	krvavý	k2eAgInSc1d1	krvavý
román	román	k1gInSc1	román
<g/>
)	)	kIx)	)
Josef	Josef	k1gMnSc1	Josef
Abrhám	Abrha	k1gFnPc3	Abrha
(	(	kIx(	(
<g/>
Šakalí	šakalí	k2eAgNnPc4d1	šakalí
léta	léto	k1gNnPc4	léto
<g/>
)	)	kIx)	)
Jiřina	Jiřina	k1gFnSc1	Jiřina
Bohdalová	Bohdalová	k1gFnSc1	Bohdalová
(	(	kIx(	(
<g/>
Nesmrtelná	smrtelný	k2eNgFnSc1d1	nesmrtelná
teta	teta	k1gFnSc1	teta
<g/>
)	)	kIx)	)
Jurský	jurský	k2eAgInSc1d1	jurský
park	park	k1gInSc1	park
František	František	k1gMnSc1	František
Vláčil	vláčit	k5eAaImAgMnS	vláčit
Blade	Blad	k1gInSc5	Blad
Runner	Runner	k1gInSc4	Runner
Šakalí	šakalí	k2eAgNnPc4d1	šakalí
léta	léto	k1gNnPc4	léto
Kanárská	kanárský	k2eAgFnSc1d1	Kanárská
spojka	spojka	k1gFnSc1	spojka
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
1993	[number]	k4	1993
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
ČFTA	ČFTA	kA	ČFTA
</s>
