<s>
KAJOTbet	KAJOTbet	k1gMnSc1
Hockey	Hockea	k1gFnSc2
Games	Games	k1gInSc1
2012	#num#	k4
</s>
<s>
KAJOTbet	KAJOTbet	k1gMnSc1
Hockey	Hockea	k1gFnSc2
Games	Games	k1gInSc1
2012	#num#	k4
<g/>
Podrobnosti	podrobnost	k1gFnPc4
turnaje	turnaj	k1gInSc2
Pořadatel	pořadatel	k1gMnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Města	město	k1gNnSc2
</s>
<s>
Brno	Brno	k1gNnSc1
Datum	datum	k1gNnSc1
konání	konání	k1gNnSc2
</s>
<s>
26.4	26.4	k4
<g/>
.	.	kIx.
–	–	k?
29.4	29.4	k4
<g/>
.	.	kIx.
2012	#num#	k4
Konečné	Konečné	k2eAgNnPc2d1
pořadí	pořadí	k1gNnPc2
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Finsko	Finsko	k1gNnSc1
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Česko	Česko	k1gNnSc1
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Rusko	Rusko	k1gNnSc1
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
<	<	kIx(
Rok	rok	k1gInSc4
2011	#num#	k4
<g/>
Rok	rok	k1gInSc1
2013	#num#	k4
>	>	kIx)
</s>
<s>
KAJOTbet	KAJOTbet	k1nSc1
Hockey	hockey	k1nSc1
Games	Games	k1nSc1
2012	#num#	k4
byl	být	k5eAaImAgInS
turnaj	turnaj	k1gInSc1
v	v	k7c6
rámci	rámec	k1gInSc6
série	série	k1gFnSc2
Euro	euro	k1gNnSc1
Hockey	Hockea	k1gFnSc2
Tour	Tour	k1gInSc1
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
sehrán	sehrát	k5eAaPmNgInS
od	od	k7c2
26	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vítězem	vítěz	k1gMnSc7
turnaje	turnaj	k1gInSc2
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
Finsko	Finsko	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
ztratilo	ztratit	k5eAaPmAgNnS
jediný	jediný	k2eAgInSc4d1
bod	bod	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
utkání	utkání	k1gNnSc6
s	s	k7c7
Českou	český	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězství	vítězství	k1gNnSc1
nad	nad	k7c7
Ruskem	Rusko	k1gNnSc7
v	v	k7c6
posledním	poslední	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
turnaje	turnaj	k1gInSc2
zajistilo	zajistit	k5eAaPmAgNnS
České	český	k2eAgFnSc3d1
republice	republika	k1gFnSc3
teprve	teprve	k6eAd1
druhý	druhý	k4xOgInSc4
celkový	celkový	k2eAgInSc4d1
triumf	triumf	k1gInSc4
v	v	k7c4
Euro	euro	k1gNnSc4
Hockey	Hockea	k1gFnSc2
Tour	Toura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Zápasy	zápas	k1gInPc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
201220	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
MSK	MSK	kA
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
0	#num#	k4
–	–	k?
2	#num#	k4
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Sportovní	sportovní	k2eAgInSc1d1
komplex	komplex	k1gInSc1
Jubilejnyj	Jubilejnyj	k1gFnSc1
<g/>
,	,	kIx,
PetrohradNávštěvnost	PetrohradNávštěvnost	k1gFnSc1
<g/>
:	:	kIx,
5	#num#	k4
620	#num#	k4
</s>
<s>
Report	report	k1gInSc1
</s>
<s>
Semjon	Semjon	k1gInSc1
Varlamov	Varlamov	k1gInSc1
</s>
<s>
Brankáři	brankář	k1gMnPc1
</s>
<s>
Kari	kari	k1gNnSc1
Lehtonen	Lehtonna	k1gFnPc2
</s>
<s>
Rozhodčí	rozhodčí	k1gMnSc1
<g/>
:	:	kIx,
Lärking	Lärking	k1gInSc1
Johansson	Johansson	k1gMnSc1
</s>
<s>
33	#num#	k4
<g/>
'	'	kIx"
Mikko	Mikko	k1gNnSc4
Mäenpää	Mäenpää	k1gFnSc2
60	#num#	k4
<g/>
'	'	kIx"
Antti	Antti	k1gNnSc1
Pihlström	Pihlströma	k1gFnPc2
</s>
<s>
10	#num#	k4
min	mina	k1gFnPc2
</s>
<s>
Tresty	trest	k1gInPc1
</s>
<s>
12	#num#	k4
min	mina	k1gFnPc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
201218	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
SEČ	SEČ	kA
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
3	#num#	k4
–	–	k?
5	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Kajot	Kajot	k1gInSc1
Arena	Arena	k1gFnSc1
<g/>
,	,	kIx,
BrnoNávštěvnost	BrnoNávštěvnost	k1gFnSc1
<g/>
:	:	kIx,
7	#num#	k4
058	#num#	k4
</s>
<s>
Report	report	k1gInSc1
</s>
<s>
Jhonas	Jhonas	k1gMnSc1
Enroth	Enroth	k1gMnSc1
</s>
<s>
Brankáři	brankář	k1gMnPc1
</s>
<s>
Jakub	Jakub	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
</s>
<s>
Rozhodčí	rozhodčí	k1gMnSc1
<g/>
:	:	kIx,
Anisimov	Anisimov	k1gInSc1
Olenin	Olenina	k1gFnPc2
</s>
<s>
Järnkrok	Järnkrok	k1gInSc1
6	#num#	k4
<g/>
'	'	kIx"
<g/>
Staffan	Staffana	k1gFnPc2
Kronwall	Kronwallum	k1gNnPc2
34	#num#	k4
<g/>
'	'	kIx"
<g/>
Harju	Harju	k1gFnSc1
34	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
20	#num#	k4
<g/>
'	'	kIx"
Ondřej	Ondřej	k1gMnSc1
Němec	Němec	k1gMnSc1
24	#num#	k4
<g/>
'	'	kIx"
Jiří	Jiří	k1gMnSc1
Novotný	Novotný	k1gMnSc1
24	#num#	k4
<g/>
'	'	kIx"
Jiří	Jiří	k1gMnSc1
Tlustý	tlustý	k2eAgMnSc1d1
41	#num#	k4
<g/>
'	'	kIx"
Tomáš	Tomáš	k1gMnSc1
Plekanec	Plekanec	k1gMnSc1
60	#num#	k4
<g/>
'	'	kIx"
Petr	Petr	k1gMnSc1
Průcha	Průcha	k1gMnSc1
</s>
<s>
16	#num#	k4
min	mina	k1gFnPc2
</s>
<s>
Tresty	trest	k1gInPc1
</s>
<s>
18	#num#	k4
min	mina	k1gFnPc2
</s>
<s>
37	#num#	k4
</s>
<s>
Střely	střela	k1gFnPc1
</s>
<s>
35	#num#	k4
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
201214	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
SEČ	SEČ	kA
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
2	#num#	k4
–	–	k?
3	#num#	k4
PP	PP	kA
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
Kajot	Kajot	k1gInSc1
Arena	Arena	k1gFnSc1
<g/>
,	,	kIx,
BrnoNávštěvnost	BrnoNávštěvnost	k1gFnSc1
<g/>
:	:	kIx,
6	#num#	k4
905	#num#	k4
</s>
<s>
Report	report	k1gInSc1
</s>
<s>
Jakub	Jakub	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
</s>
<s>
Brankáři	brankář	k1gMnPc1
</s>
<s>
Petri	Petri	k6eAd1
Vehanen	Vehanen	k2eAgInSc1d1
</s>
<s>
Rozhodčí	rozhodčí	k1gMnSc1
<g/>
:	:	kIx,
Anisimov	Anisimov	k1gInSc1
Olenin	Olenina	k1gFnPc2
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Němec	Němec	k1gMnSc1
14	#num#	k4
<g/>
'	'	kIx"
<g/>
Lukáš	Lukáš	k1gMnSc1
Kašpar	Kašpar	k1gMnSc1
17	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
27	#num#	k4
<g/>
'	'	kIx"
Lennart	Lennarta	k1gFnPc2
Petrell	Petrellum	k1gNnPc2
32	#num#	k4
<g/>
'	'	kIx"
Mikko	Mikko	k1gNnSc4
Mäenpää	Mäenpää	k1gFnSc2
63	#num#	k4
<g/>
'	'	kIx"
Tuomas	Tuomas	k1gInSc4
Kiiskinen	Kiiskinen	k2eAgInSc4d1
</s>
<s>
14	#num#	k4
(	(	kIx(
<g/>
navíc	navíc	k6eAd1
Petr	Petr	k1gMnSc1
Čáslava	Čáslava	k1gFnSc1
5	#num#	k4
<g/>
+	+	kIx~
<g/>
DKU	DKU	kA
<g/>
)	)	kIx)
min	min	kA
</s>
<s>
Tresty	trest	k1gInPc1
</s>
<s>
14	#num#	k4
(	(	kIx(
<g/>
navíc	navíc	k6eAd1
Janne	Jann	k1gInSc5
Pesonen	Pesonno	k1gNnPc2
10	#num#	k4
<g/>
+	+	kIx~
<g/>
5	#num#	k4
<g/>
+	+	kIx~
<g/>
DKU	DKU	kA
<g/>
)	)	kIx)
min	min	kA
</s>
<s>
28	#num#	k4
</s>
<s>
Střely	střela	k1gFnPc1
</s>
<s>
21	#num#	k4
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
201218	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
SEČ	SEČ	kA
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
4	#num#	k4
–	–	k?
2	#num#	k4
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Kajot	Kajot	k1gInSc1
Arena	Arena	k1gFnSc1
<g/>
,	,	kIx,
BrnoNávštěvnost	BrnoNávštěvnost	k1gFnSc1
<g/>
:	:	kIx,
5	#num#	k4
655	#num#	k4
</s>
<s>
Report	report	k1gInSc1
</s>
<s>
Semjon	Semjon	k1gInSc1
Varlamov	Varlamov	k1gInSc1
</s>
<s>
Brankáři	brankář	k1gMnPc1
</s>
<s>
Viktor	Viktor	k1gMnSc1
Fasth	Fasth	k1gMnSc1
</s>
<s>
Rozhodčí	rozhodčí	k1gMnSc1
<g/>
:	:	kIx,
Martin	Martin	k1gMnSc1
Fraňo	Fraňo	k1gMnSc1
Vladimír	Vladimír	k1gMnSc1
Šindler	Šindler	k1gMnSc1
</s>
<s>
Jevgenij	Jevgenít	k5eAaPmRp2nS
Kuzněcov	Kuzněcov	k1gInSc1
35	#num#	k4
<g/>
'	'	kIx"
<g/>
Nikolaj	Nikolaj	k1gMnSc1
Kuljomin	Kuljomin	k1gInSc4
38	#num#	k4
<g/>
'	'	kIx"
<g/>
Nikolaj	Nikolaj	k1gMnSc1
Žerděv	Žerděv	k1gMnSc1
39	#num#	k4
<g/>
'	'	kIx"
<g/>
Nikolaj	Nikolaj	k1gMnSc1
Žerděv	Žerděv	k1gMnSc1
59	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
39	#num#	k4
<g/>
'	'	kIx"
Olausson	Olausson	k1gInSc1
56	#num#	k4
<g/>
'	'	kIx"
Karlsson	Karlsson	k1gInSc1
</s>
<s>
18	#num#	k4
min	mina	k1gFnPc2
</s>
<s>
Tresty	trest	k1gInPc1
</s>
<s>
20	#num#	k4
min	mina	k1gFnPc2
</s>
<s>
28	#num#	k4
</s>
<s>
Střely	střela	k1gFnPc1
</s>
<s>
26	#num#	k4
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
201214	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
SEČ	SEČ	kA
</s>
<s>
Finsko	Finsko	k1gNnSc1
</s>
<s>
4	#num#	k4
–	–	k?
1	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Kajot	Kajot	k1gInSc1
Arena	Arena	k1gFnSc1
<g/>
,	,	kIx,
BrnoNávštěvnost	BrnoNávštěvnost	k1gFnSc1
<g/>
:	:	kIx,
4	#num#	k4
126	#num#	k4
</s>
<s>
Report	report	k1gInSc1
</s>
<s>
Kari	kari	k1gNnSc1
Lehtonen	Lehtonna	k1gFnPc2
</s>
<s>
Brankáři	brankář	k1gMnPc1
</s>
<s>
Jhonas	Jhonas	k1gMnSc1
Enroth	Enroth	k1gMnSc1
</s>
<s>
Rozhodčí	rozhodčí	k1gMnSc1
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Hribík	Hribík	k1gMnSc1
Vladimír	Vladimír	k1gMnSc1
Šindler	Šindler	k1gMnSc1
</s>
<s>
Jussi	Jusse	k1gFnSc3
Jokinen	Jokinen	k1gInSc1
2	#num#	k4
<g/>
'	'	kIx"
<g/>
Ville	Ville	k1gFnSc1
Peltonen	Peltonen	k1gInSc1
29	#num#	k4
<g/>
'	'	kIx"
<g/>
Tuomas	Tuomasa	k1gFnPc2
Kiiskinen	Kiiskinno	k1gNnPc2
50	#num#	k4
<g/>
'	'	kIx"
<g/>
Janne	Jann	k1gInSc5
Pesonen	Pesonno	k1gNnPc2
60	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
45	#num#	k4
<g/>
'	'	kIx"
Patrik	Patrik	k1gMnSc1
Zackrisson	Zackrisson	k1gMnSc1
</s>
<s>
10	#num#	k4
min	mina	k1gFnPc2
</s>
<s>
Tresty	trest	k1gInPc1
</s>
<s>
4	#num#	k4
min	mina	k1gFnPc2
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
201218	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
SEČ	SEČ	kA
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
2	#num#	k4
–	–	k?
1	#num#	k4
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Kajot	Kajot	k1gInSc1
Arena	Arena	k1gFnSc1
<g/>
,	,	kIx,
BrnoNávštěvnost	BrnoNávštěvnost	k1gFnSc1
<g/>
:	:	kIx,
7	#num#	k4
200	#num#	k4
</s>
<s>
Report	report	k1gInSc1
</s>
<s>
Jakub	Jakub	k1gMnSc1
Kovář	Kovář	k1gMnSc1
</s>
<s>
Brankáři	brankář	k1gMnPc1
</s>
<s>
Konstantin	Konstantin	k1gMnSc1
Barulin	Barulin	k2eAgMnSc1d1
</s>
<s>
Rozhodčí	rozhodčí	k1gMnSc1
<g/>
:	:	kIx,
Laaksonen	Laaksonen	k1gInSc1
Leppäalho	Leppäal	k1gMnSc2
</s>
<s>
Jakub	Jakub	k1gMnSc1
Petružálek	Petružálek	k1gMnSc1
38	#num#	k4
<g/>
'	'	kIx"
<g/>
Petr	Petr	k1gMnSc1
Nedvěd	Nedvěd	k1gMnSc1
39	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
24	#num#	k4
<g/>
'	'	kIx"
Jevgenij	Jevgenij	k1gFnPc2
Ketov	Ketovo	k1gNnPc2
</s>
<s>
12	#num#	k4
min	mina	k1gFnPc2
</s>
<s>
Tresty	trest	k1gInPc1
</s>
<s>
16	#num#	k4
min	mina	k1gFnPc2
</s>
<s>
18	#num#	k4
</s>
<s>
Střely	střela	k1gFnPc1
</s>
<s>
30	#num#	k4
</s>
<s>
Tabulka	tabulka	k1gFnSc1
</s>
<s>
Poř	Poř	k?
<g/>
.	.	kIx.
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
VP	VP	kA
</s>
<s>
PP	PP	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
B	B	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finsko	Finsko	k1gNnSc1
<g/>
32100938	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česko	Česko	k1gNnSc1
<g/>
32010977	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko	Rusko	k1gNnSc1
<g/>
31002563	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švédsko	Švédsko	k1gNnSc1
<g/>
300036130	#num#	k4
</s>
<s>
Z	z	k7c2
=	=	kIx~
Odehrané	odehraný	k2eAgInPc1d1
zápasy	zápas	k1gInPc1
<g/>
;	;	kIx,
V	V	kA
=	=	kIx~
Výhry	výhra	k1gFnPc1
<g/>
;	;	kIx,
VP	VP	kA
=	=	kIx~
Výhry	výhra	k1gFnPc1
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
<g/>
/	/	kIx~
<g/>
nájezdech	nájezd	k1gInPc6
<g/>
;	;	kIx,
PP	PP	kA
=	=	kIx~
Prohry	prohra	k1gFnSc2
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
<g/>
/	/	kIx~
<g/>
nájezdech	nájezd	k1gInPc6
<g/>
;	;	kIx,
VG	VG	kA
=	=	kIx~
vstřelené	vstřelený	k2eAgInPc1d1
góly	gól	k1gInPc1
<g/>
;	;	kIx,
OG	OG	kA
=	=	kIx~
obdržené	obdržený	k2eAgInPc1d1
góly	gól	k1gInPc1
<g/>
;	;	kIx,
B	B	kA
=	=	kIx~
Body	bod	k1gInPc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
Euro	euro	k1gNnSc1
Hockey	Hockea	k1gFnSc2
Tour	Toura	k1gFnPc2
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Euro	euro	k1gNnSc1
Hockey	Hockea	k1gFnSc2
Tour	Tour	k1gInSc1
2011-2012	2011-2012	k4
na	na	k7c4
eurohockey	eurohockey	k1gInPc4
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Euro	euro	k1gNnSc1
Hockey	Hockea	k1gFnSc2
Tour	Toura	k1gFnPc2
České	český	k2eAgFnSc2d1
hokejové	hokejový	k2eAgFnSc2d1
hryod	hryod	k1gInSc1
roku	rok	k1gInSc2
1997	#num#	k4
součást	součást	k1gFnSc1
EHT	EHT	kA
</s>
<s>
Pragobanka	Pragobanka	k1gFnSc1
Cup	cup	k1gInSc1
<g/>
:	:	kIx,
1994	#num#	k4
•	•	k?
1995	#num#	k4
•	•	k?
1996	#num#	k4
•	•	k?
1997	#num#	k4
•	•	k?
Česká	český	k2eAgFnSc1d1
Pojišťovna	pojišťovna	k1gFnSc1
Cup	cup	k1gInSc1
<g/>
:	:	kIx,
1998	#num#	k4
•	•	k?
1999	#num#	k4
•	•	k?
2000	#num#	k4
•	•	k?
2001	#num#	k4
•	•	k?
2002	#num#	k4
•	•	k?
2003	#num#	k4
•	•	k?
2004	#num#	k4
•	•	k?
2005	#num#	k4
•	•	k?
2006	#num#	k4
•	•	k?
2007	#num#	k4
•	•	k?
Czech	Czech	k1gMnSc1
Hockey	Hockea	k1gFnSc2
Games	Games	k1gMnSc1
<g/>
:	:	kIx,
2008	#num#	k4
•	•	k?
2009	#num#	k4
(	(	kIx(
<g/>
duben	duben	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
2009	#num#	k4
(	(	kIx(
<g/>
září	září	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
2010	#num#	k4
•	•	k?
2011	#num#	k4
•	•	k?
KAJOTbet	KAJOTbet	k1gMnSc1
Hockey	Hockea	k1gFnSc2
Games	Games	k1gMnSc1
<g/>
:	:	kIx,
2012	#num#	k4
•	•	k?
2013	#num#	k4
(	(	kIx(
<g/>
duben	duben	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
2013	#num#	k4
(	(	kIx(
<g/>
srpen	srpen	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
2014	#num#	k4
•	•	k?
2015	#num#	k4
•	•	k?
2016	#num#	k4
•	•	k?
Czech	Czech	k1gMnSc1
Hockey	Hockea	k1gFnSc2
Games	Games	k1gMnSc1
<g/>
:	:	kIx,
2017	#num#	k4
•	•	k?
2018	#num#	k4
•	•	k?
2019	#num#	k4
•	•	k?
2020	#num#	k4
•	•	k?
2021	#num#	k4
Karjala	Karjala	k1gFnSc1
Cupod	Cupoda	k1gFnPc2
roku	rok	k1gInSc2
1997	#num#	k4
součást	součást	k1gFnSc1
EHT	EHT	kA
</s>
<s>
Sauna	sauna	k1gFnSc1
Cup	cup	k1gInSc1
<g/>
:	:	kIx,
1992	#num#	k4
•	•	k?
Karjala	Karjala	k1gFnSc1
Cup	cup	k1gInSc1
<g/>
:	:	kIx,
1995	#num#	k4
•	•	k?
1996	#num#	k4
•	•	k?
1997	#num#	k4
•	•	k?
1998	#num#	k4
•	•	k?
1999	#num#	k4
•	•	k?
2000	#num#	k4
•	•	k?
2001	#num#	k4
•	•	k?
2002	#num#	k4
(	(	kIx(
<g/>
duben	duben	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
2002	#num#	k4
(	(	kIx(
<g/>
listopad	listopad	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
2003	#num#	k4
•	•	k?
2004	#num#	k4
•	•	k?
2005	#num#	k4
•	•	k?
2006	#num#	k4
•	•	k?
2007	#num#	k4
•	•	k?
2008	#num#	k4
•	•	k?
2009	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2011	#num#	k4
•	•	k?
2012	#num#	k4
•	•	k?
2013	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2015	#num#	k4
•	•	k?
2016	#num#	k4
•	•	k?
2017	#num#	k4
•	•	k?
2018	#num#	k4
•	•	k?
2019	#num#	k4
•	•	k?
2020	#num#	k4
•	•	k?
2021	#num#	k4
Channel	Channela	k1gFnPc2
One	One	k1gFnPc2
Cupod	Cupod	k1gInSc1
roku	rok	k1gInSc2
1997	#num#	k4
součást	součást	k1gFnSc1
EHT	EHT	kA
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
turnaj	turnaj	k1gInSc1
<g/>
:	:	kIx,
1967	#num#	k4
•	•	k?
1968	#num#	k4
•	•	k?
Izvestije	Izvestije	k1gMnSc1
<g/>
:	:	kIx,
1969	#num#	k4
•	•	k?
1970	#num#	k4
•	•	k?
1971	#num#	k4
•	•	k?
1972	#num#	k4
•	•	k?
1973	#num#	k4
•	•	k?
1974	#num#	k4
•	•	k?
1975	#num#	k4
•	•	k?
1976	#num#	k4
•	•	k?
1977	#num#	k4
•	•	k?
1978	#num#	k4
•	•	k?
1979	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1981	#num#	k4
•	•	k?
1982	#num#	k4
•	•	k?
1983	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
1985	#num#	k4
•	•	k?
1986	#num#	k4
•	•	k?
1987	#num#	k4
•	•	k?
1988	#num#	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1989	#num#	k4
•	•	k?
1990	#num#	k4
•	•	k?
1991	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1993	#num#	k4
•	•	k?
1994	#num#	k4
•	•	k?
1995	#num#	k4
•	•	k?
1996	#num#	k4
•	•	k?
Baltika	Baltika	k1gFnSc1
Cup	cup	k1gInSc1
<g/>
:	:	kIx,
1997	#num#	k4
•	•	k?
1998	#num#	k4
•	•	k?
1999	#num#	k4
•	•	k?
2000	#num#	k4
•	•	k?
2001	#num#	k4
•	•	k?
2002	#num#	k4
•	•	k?
2003	#num#	k4
•	•	k?
Rosno	Rosno	k6eAd1
Cup	cup	k1gInSc1
<g/>
:	:	kIx,
2004	#num#	k4
•	•	k?
2005	#num#	k4
•	•	k?
Channel	Channel	k1gInSc1
One	One	k1gMnSc1
Cup	cup	k1gInSc1
<g/>
:	:	kIx,
2006	#num#	k4
•	•	k?
2007	#num#	k4
•	•	k?
2008	#num#	k4
•	•	k?
2009	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2011	#num#	k4
•	•	k?
2012	#num#	k4
•	•	k?
2013	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2015	#num#	k4
•	•	k?
2016	#num#	k4
•	•	k?
2017	#num#	k4
•	•	k?
2018	#num#	k4
•	•	k?
2019	#num#	k4
•	•	k?
2020	#num#	k4
•	•	k?
2021	#num#	k4
Sweden	Swedna	k1gFnPc2
Hockey	Hockea	k1gFnSc2
Gamesod	Gamesod	k1gInSc1
roku	rok	k1gInSc2
1997	#num#	k4
součást	součást	k1gFnSc1
EHT	EHT	kA
</s>
<s>
Švédský	švédský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
<g/>
:	:	kIx,
1980	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
Švédské	švédský	k2eAgFnSc2d1
hokejové	hokejový	k2eAgFnSc2d1
hry	hra	k1gFnSc2
<g/>
:	:	kIx,
1991	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1993	#num#	k4
•	•	k?
1994	#num#	k4
•	•	k?
1995	#num#	k4
•	•	k?
1996	#num#	k4
•	•	k?
1997	#num#	k4
•	•	k?
1998	#num#	k4
•	•	k?
1999	#num#	k4
•	•	k?
2000	#num#	k4
•	•	k?
2001	#num#	k4
(	(	kIx(
<g/>
únor	únor	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
2001	#num#	k4
(	(	kIx(
<g/>
listopad	listopad	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
2002	#num#	k4
•	•	k?
2003	#num#	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
2004	#num#	k4
•	•	k?
2005	#num#	k4
•	•	k?
LG	LG	kA
Hockey	Hockea	k1gMnSc2
Games	Games	k1gMnSc1
<g/>
:	:	kIx,
2006	#num#	k4
•	•	k?
2007	#num#	k4
•	•	k?
2008	#num#	k4
•	•	k?
2009	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2011	#num#	k4
•	•	k?
Oddset	Oddset	k1gMnSc1
Hockey	Hockea	k1gFnSc2
Games	Games	k1gMnSc1
<g/>
:	:	kIx,
2012	#num#	k4
•	•	k?
2013	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2015	#num#	k4
•	•	k?
2016	#num#	k4
•	•	k?
Sweden	Swedno	k1gNnPc2
Hockey	Hockea	k1gMnSc2
Games	Gamesa	k1gFnPc2
<g/>
:	:	kIx,
2017	#num#	k4
•	•	k?
2018	#num#	k4
•	•	k?
2019	#num#	k4
•	•	k?
2020	#num#	k4
•	•	k?
2021	#num#	k4
Euro	euro	k1gNnSc4
Hockey	Hockea	k1gFnSc2
Tour	Toura	k1gFnPc2
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
1999	#num#	k4
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
2003	#num#	k4
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
•	•	k?
2004	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2005	#num#	k4
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
•	•	k?
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
</s>
