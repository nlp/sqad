<s>
Robyn	Robyn	k1gMnSc1
Hitchcock	Hitchcock	k1gMnSc1
</s>
<s>
Robyn	Robyn	k1gMnSc1
Hitchcock	Hitchcock	k1gMnSc1
Robyn	Robyn	k1gMnSc1
Hitchcock	Hitchcock	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Robyn	Robyn	k1gMnSc1
Rowan	Rowan	k1gMnSc1
Hitchcock	Hitchcock	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1953	#num#	k4
(	(	kIx(
<g/>
68	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Anglie	Anglie	k1gFnSc1
Žánry	žánr	k1gInPc4
</s>
<s>
rock	rock	k1gInSc1
<g/>
,	,	kIx,
folk	folk	k1gInSc1
<g/>
,	,	kIx,
pop	pop	k1gMnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
hudebník	hudebník	k1gMnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
harmonika	harmonika	k1gFnSc1
<g/>
,	,	kIx,
zpěv	zpěv	k1gInSc1
Příbuzná	příbuzná	k1gFnSc1
témata	téma	k1gNnPc4
</s>
<s>
The	The	k?
Soft	Soft	k?
Boys	boy	k1gMnPc2
Rodiče	rodič	k1gMnPc1
</s>
<s>
Raymond	Raymond	k1gMnSc1
Hitchcock	Hitchcock	k1gMnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Fleur	Fleur	k1gMnSc1
Hitchcock	Hitchcock	k1gMnSc1
a	a	k8xC
Lal	Lal	k1gMnSc1
Hitchcock	Hitchcock	k1gMnSc1
(	(	kIx(
<g/>
sourozenci	sourozenec	k1gMnPc1
<g/>
)	)	kIx)
<g/>
Ruby	rub	k1gInPc1
Wright	Wrighta	k1gFnPc2
(	(	kIx(
<g/>
neteř	neteř	k1gFnSc1
<g/>
)	)	kIx)
Web	web	k1gInSc1
</s>
<s>
www.robynhitchcock.com	www.robynhitchcock.com	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Robyn	Robyn	k1gMnSc1
Hitchcock	Hitchcock	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1953	#num#	k4
Londýn	Londýn	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
britský	britský	k2eAgMnSc1d1
písničkář	písničkář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začínal	začínat	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
ve	v	k7c6
skupině	skupina	k1gFnSc6
The	The	k1gFnPc2
Soft	Soft	k?
Boys	boy	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jejím	její	k3xOp3gInSc6
rozpadu	rozpad	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
začal	začít	k5eAaPmAgMnS
vydávat	vydávat	k5eAaImF,k5eAaPmF
sólová	sólový	k2eAgNnPc1d1
alba	album	k1gNnPc1
<g/>
;	;	kIx,
první	první	k4xOgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
neslo	nést	k5eAaImAgNnS
název	název	k1gInSc1
Black	Black	k1gMnSc1
Snake	Snake	k1gFnPc2
Diamond	Diamond	k1gMnSc1
Röle	Röl	k1gFnSc2
a	a	k8xC
vyšlo	vyjít	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1985	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
vydával	vydávat	k5eAaImAgInS,k5eAaPmAgInS
alba	album	k1gNnPc1
se	s	k7c7
skupinou	skupina	k1gFnSc7
The	The	k1gFnSc2
Egyptians	Egyptiansa	k1gFnPc2
a	a	k8xC
2006	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
s	s	k7c7
The	The	k1gFnSc7
Venus	Venus	k1gMnSc1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Sólová	sólový	k2eAgFnSc1d1
diskografie	diskografie	k1gFnSc1
</s>
<s>
Black	Black	k1gMnSc1
Snake	Snak	k1gFnSc2
Diamond	Diamond	k1gMnSc1
Röle	Röle	k1gInSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Groovy	Groův	k2eAgFnPc1d1
Decay	Decaa	k1gFnPc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
I	i	k9
Often	Often	k2eAgInSc1d1
Dream	Dream	k1gInSc1
of	of	k?
Trains	Trains	k1gInSc1
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eye	Eye	k?
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Moss	Moss	k1gInSc1
Elixir	Elixir	k1gInSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jewels	Jewels	k1gInSc4
for	forum	k1gNnPc2
Sophia	Sophius	k1gMnSc2
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Luxor	Luxor	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Spooked	Spooked	k1gInSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tromsø	Tromsø	k?
<g/>
,	,	kIx,
Kaptein	Kaptein	k1gMnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Love	lov	k1gInSc5
from	from	k1gMnSc1
London	London	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Man	Man	k1gMnSc1
Upstairs	Upstairs	k1gInSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Robyn	Robyn	k1gMnSc1
Hitchcock	Hitchcock	k1gMnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Robyn	Robyna	k1gFnPc2
Hitchcock	Hitchcocko	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Robyn	Robyn	k1gMnSc1
Hitchcock	Hitchcock	k1gMnSc1
na	na	k7c6
Allmusic	Allmusice	k1gInPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Nositelé	nositel	k1gMnPc1
ceny	cena	k1gFnSc2
Premio	Premio	k6eAd1
Tenco	Tenco	k6eAd1
Pro	pro	k7c4
hudebníka	hudebník	k1gMnSc4
</s>
<s>
Léo	Léo	k?
Ferré	Ferrý	k2eAgFnSc2d1
<g/>
,	,	kIx,
Sergio	Sergio	k1gMnSc1
Endrigo	Endrigo	k1gMnSc1
<g/>
,	,	kIx,
Giorgio	Giorgio	k1gMnSc1
Gaber	gabro	k1gNnPc2
<g/>
,	,	kIx,
Domenico	Domenico	k6eAd1
Modugno	Modugno	k6eAd1
a	a	k8xC
Gino	Gino	k1gMnSc1
Paoli	Paole	k1gFnSc4
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vinicius	Vinicius	k1gInSc1
de	de	k?
Moraes	Moraes	k1gInSc1
<g/>
,	,	kIx,
Fausto	Fausta	k1gMnSc5
Amodei	Amode	k1gMnSc5
<g/>
,	,	kIx,
Umberto	Umberta	k1gFnSc5
Bindi	Bind	k1gMnPc1
<g/>
,	,	kIx,
Fabrizio	Fabrizio	k1gMnSc1
De	De	k?
André	André	k1gMnSc1
<g/>
,	,	kIx,
Francesco	Francesco	k6eAd1
Guccini	Guccin	k1gMnPc1
a	a	k8xC
Enzo	Enzo	k6eAd1
Jannacci	Jannacce	k1gMnPc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Georges	Georges	k1gInSc1
Brassens	Brassens	k1gInSc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jacques	Jacques	k1gMnSc1
Brel	Brel	k1gMnSc1
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leonard	Leonard	k1gInSc1
Cohen	Cohen	k1gInSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lluís	Lluís	k1gInSc1
Llach	Llach	k1gInSc1
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Atahualpa	Atahualpa	k1gFnSc1
Yupanqui	Yupanqu	k1gFnSc2
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Chico	Chico	k1gMnSc1
Buarque	Buarqu	k1gFnSc2
de	de	k?
Hollanda	Hollanda	k1gFnSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Arsen	arsen	k1gInSc1
Dedić	Dedić	k1gFnSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alan	Alan	k1gMnSc1
Stivell	Stivell	k1gMnSc1
<g/>
,	,	kIx,
Paolo	Paola	k1gMnSc5
Conte	Cont	k1gMnSc5
<g/>
,	,	kIx,
Giovanna	Giovanna	k1gFnSc1
Marini	Marin	k2eAgMnPc1d1
a	a	k8xC
Roberto	Roberta	k1gFnSc5
Vecchioni	Vecchioň	k1gFnSc6
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Colette	Colett	k1gInSc5
Magny	Magna	k1gMnSc2
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Silvio	Silvio	k1gMnSc1
Rodríguez	Rodríguez	k1gMnSc1
<g/>
,	,	kIx,
Dave	Dav	k1gInSc5
Van	vana	k1gFnPc2
Ronk	Ronk	k1gInSc4
a	a	k8xC
Bulat	bulat	k5eAaImF
Okudžava	Okudžava	k1gFnSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Tom	Tom	k1gMnSc1
Waits	Waits	k1gInSc1
a	a	k8xC
Joan	Joan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Serrat	Serrat	k1gMnSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Joni	Joni	k1gNnSc1
Mitchell	Mitchell	k1gMnSc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Randy	rand	k1gInPc1
Newman	Newman	k1gMnSc1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Caetano	Caetana	k1gFnSc5
Veloso	Velosa	k1gFnSc5
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Charles	Charles	k1gMnSc1
Trenet	Trenet	k1gMnSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vladimir	Vladimir	k1gMnSc1
Vysockij	Vysockij	k1gMnSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pablo	Pablo	k1gNnSc1
Milanés	Milanés	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sérgio	Sérgio	k1gMnSc1
Godinho	Godin	k1gMnSc2
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Renato	Renata	k1gFnSc5
Carosone	Caroson	k1gInSc5
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jackson	Jackson	k1gMnSc1
Browne	Brown	k1gInSc5
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Elvis	Elvis	k1gMnSc1
Costello	Costello	k1gNnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bruce	Bruce	k1gMnSc1
Cockburn	Cockburn	k1gMnSc1
a	a	k8xC
Zülfü	Zülfü	k1gMnSc1
Livaneli	Livanel	k1gInSc6
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nick	Nick	k1gInSc1
Cave	Cav	k1gFnSc2
a	a	k8xC
Rickie	Rickie	k1gFnSc2
Lee	Lea	k1gFnSc3
Jones	Jones	k1gMnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Laurie	Laurie	k1gFnSc1
Anderson	Anderson	k1gMnSc1
a	a	k8xC
Luis	Luisa	k1gFnPc2
Eduardo	Eduardo	k1gNnSc4
Aute	aut	k1gInSc5
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Donovan	Donovan	k1gMnSc1
a	a	k8xC
Gilberto	Gilberta	k1gFnSc5
Gil	Gil	k1gMnSc6
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Eric	Eric	k1gInSc1
Andersen	Andersen	k1gMnSc1
a	a	k8xC
Patti	Patti	k1gNnSc1
Smith	Smith	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Peter	Peter	k1gMnSc1
Hammill	Hammill	k1gMnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
Cale	Cal	k1gFnSc2
a	a	k8xC
Khaled	Khaled	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Willy	Will	k1gMnPc4
DeVille	DeVille	k1gFnSc2
a	a	k8xC
Bruno	Bruno	k1gMnSc1
Lauzi	Lauze	k1gFnSc4
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jacques	Jacques	k1gMnSc1
Higelin	Higelin	k2eAgMnSc1d1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Milton	Milton	k1gInSc1
Nascimento	Nascimento	k1gNnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Franco	Franco	k6eAd1
Battiato	Battiat	k2eAgNnSc1d1
a	a	k8xC
Angélique	Angélique	k1gNnSc1
Kidjo	Kidjo	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Paul	Paul	k1gMnSc1
Brady	Brada	k1gMnSc2
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Luciano	Luciana	k1gFnSc5
Ligabue	Ligabue	k1gFnSc1
a	a	k8xC
Jaromír	Jaromír	k1gMnSc1
Nohavica	Nohavica	k1gMnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
The	The	k1gFnSc2
Klezmatics	Klezmaticsa	k1gFnPc2
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Robyn	Robyn	k1gMnSc1
Hitchcock	Hitchcock	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
José	Josá	k1gFnSc2
Mário	Mário	k1gMnSc1
Branco	Branco	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
Crosby	Crosba	k1gFnSc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Maria	Maria	k1gFnSc1
Farantouri	Farantouri	k1gNnSc2
<g/>
,	,	kIx,
The	The	k1gMnSc1
Plastic	Plastice	k1gFnPc2
People	People	k1gMnSc1
of	of	k?
the	the	k?
Universe	Universe	k1gFnSc1
a	a	k8xC
John	John	k1gMnSc1
Trudell	Trudell	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Francesco	Francesco	k6eAd1
Guccini	Guccin	k1gMnPc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Otello	Otello	k1gNnSc1
Profazio	Profazio	k1gMnSc1
a	a	k8xC
Stan	stan	k1gInSc1
Ridgway	Ridgwaa	k1gFnSc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vinicio	Vinicio	k1gNnSc1
Capossela	Capossela	k1gFnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zucchero	Zucchero	k1gNnSc4
Fornaciari	Fornaciar	k1gFnSc2
a	a	k8xC
Salvatore	Salvator	k1gMnSc5
Adamo	Adama	k1gFnSc5
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pino	Pino	k1gMnSc1
Donaggio	Donaggio	k1gMnSc1
<g/>
,	,	kIx,
Gianna	Gianna	k1gFnSc1
Nannini	Nannin	k2eAgMnPc1d1
a	a	k8xC
Eric	Eric	k1gFnSc4
Burdon	Burdona	k1gFnPc2
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vasco	Vasco	k6eAd1
Rossi	Rosse	k1gFnSc4
a	a	k8xC
Sting	Sting	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
Pro	pro	k7c4
kulturní	kulturní	k2eAgFnSc4d1
osobnost	osobnost	k1gFnSc4
</s>
<s>
Nanni	Nanen	k2eAgMnPc1d1
Ricordi	Ricord	k1gMnPc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Michele	Michel	k1gInSc2
L.	L.	kA
Straniero	Straniero	k1gNnSc4
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Filippo	Filippa	k1gFnSc5
Crivelli	Crivell	k1gMnSc6
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dario	Daria	k1gFnSc5
Fo	Fo	k1gMnSc6
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Roberto	Roberta	k1gFnSc5
Roversi	Roverse	k1gFnSc6
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Roberto	Roberta	k1gFnSc5
De	De	k?
Simone	Simon	k1gMnSc5
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Giancarlo	Giancarlo	k1gNnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
Cesaroni	Cesaroň	k1gFnSc6
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Giorgio	Giorgio	k1gNnSc4
Calabrese	Calabrese	k1gFnSc2
a	a	k8xC
Ornella	Ornella	k1gMnSc1
Vanoni	Vanon	k1gMnPc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Roberto	Roberta	k1gFnSc5
Murolo	Murola	k1gFnSc5
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sergio	Sergio	k1gMnSc1
Bardotti	Bardotť	k1gFnSc2
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Paolo	Paolo	k1gNnSc4
Poli	pole	k1gFnSc4
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bulat	bulat	k5eAaImF
Okudžava	Okudžava	k1gFnSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Susana	Susana	k1gFnSc1
Rinaldi	Rinald	k1gMnPc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Žanna	Žanen	k2eAgFnSc1d1
Bičevskaja	Bičevskaja	k1gFnSc1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Antonio	Antonio	k1gMnSc1
Carlos	Carlos	k1gMnSc1
Jobim	Jobim	k1gMnSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Milva	Milva	k1gFnSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Virgilio	Virgilio	k1gNnSc1
Savona	Savona	k1gFnSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cesária	Cesárium	k1gNnSc2
Évora	Évor	k1gMnSc2
a	a	k8xC
Cheikha	Cheikh	k1gMnSc2
Rimitti	Rimitť	k1gFnSc2
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Lowell	Lowell	k1gMnSc1
Fulson	Fulson	k1gMnSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Paddy	Padda	k1gMnSc2
Moloney	Molonea	k1gMnSc2
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Roger	Roger	k1gMnSc1
McGuinn	McGuinn	k1gMnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mercedes	mercedes	k1gInSc1
Sosa	Sosa	k1gFnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ute	Ute	k1gMnSc1
Lemper	Lemper	k1gMnSc1
a	a	k8xC
Franco	Franco	k1gMnSc1
Lucà	Lucà	k1gMnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Meri	Meri	k1gNnSc1
Lao	Lao	k1gMnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Arto	Arto	k1gNnSc4
Lindsay	Lindsaa	k1gFnSc2
a	a	k8xC
Enrique	Enriqu	k1gFnSc2
Morente	Morent	k1gInSc5
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jane	Jan	k1gMnSc5
Birkinová	Birkinová	k1gFnSc1
a	a	k8xC
Maria	Maria	k1gFnSc1
Del	Del	k1gFnSc2
Mar	Mar	k1gFnSc1
Bonet	Bonet	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dulce	Dulce	k1gMnSc1
Pontes	Pontes	k1gMnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fernanda	Fernanda	k1gFnSc1
Pivano	Pivana	k1gFnSc5
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gianfranco	Gianfranco	k6eAd1
Reverberi	Reverberi	k1gNnSc1
a	a	k8xC
Achinoam	Achinoam	k1gInSc1
„	„	k?
<g/>
Noa	Noa	k1gFnSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
“	“	k?
Nini	Nin	k1gFnSc2
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Marianne	Mariann	k1gInSc5
Faithfullová	Faithfullová	k1gFnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Joan	Joan	k1gMnSc1
Molas	Molas	k1gMnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Horacio	Horacio	k1gMnSc1
Ferrer	Ferrer	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Amancio	Amancio	k1gMnSc1
Prada	Prada	k1gMnSc1
<g/>
,	,	kIx,
Roberto	Roberta	k1gFnSc5
„	„	k?
<g/>
Freak	Freak	k1gInSc1
<g/>
“	“	k?
Antoni	Anton	k1gMnPc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mauro	Mauro	k1gNnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
Pagani	Pagaň	k1gFnSc6
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alessandro	Alessandra	k1gFnSc5
Portelli	Portell	k1gMnSc6
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Garland	Garland	k1gInSc1
Jeffreys	Jeffreys	k1gInSc1
a	a	k8xC
Cui	Cui	k1gMnSc1
Jian	Jian	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gianni	Gianň	k1gMnSc3
Minà	Minà	k1gMnSc3
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Guido	Guido	k1gNnSc4
De	De	k?
Maria	Maria	k1gFnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sergio	Sergio	k6eAd1
Staino	Staino	k1gNnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Massimo	Massima	k1gFnSc5
Ranieri	Ranier	k1gMnSc3
a	a	k8xC
Camanè	Camanè	k1gMnSc3
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Carlo	Carlo	k1gNnSc1
Petrini	Petrin	k1gMnPc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Franco	Franco	k1gNnSc4
Fabbri	Fabbr	k1gFnSc2
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vincenzo	Vincenza	k1gFnSc5
Mollica	Mollicum	k1gNnPc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
134407156	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5515	#num#	k4
4083	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
91079313	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
2667016	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
91079313	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
