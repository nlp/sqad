<s>
Sumgaitský	Sumgaitský	k2eAgInSc1d1
pogrom	pogrom	k1gInSc1
<g/>
,	,	kIx,
též	též	k9
Sumgaitský	Sumgaitský	k2eAgInSc1d1
masakr	masakr	k1gInSc1
byl	být	k5eAaImAgInS
Ázery	Ázera	k1gFnSc2
vedený	vedený	k2eAgInSc1d1
pogrom	pogrom	k1gInSc1
zacílený	zacílený	k2eAgInSc1d1
na	na	k7c4
arménskou	arménský	k2eAgFnSc4d1
populaci	populace	k1gFnSc4
v	v	k7c6
přímořském	přímořský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Sumqayı	Sumqayı	k1gFnPc2
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
Sumgait	Sumgait	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zhruba	zhruba	k6eAd1
30	#num#	k4
km	km	kA
severně	severně	k6eAd1
od	od	k7c2
Baku	Baku	k1gNnSc2
<g/>
,	,	kIx,
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
tehdejší	tehdejší	k2eAgFnSc2d1
Ázerbájdžánské	ázerbájdžánský	k2eAgFnSc2d1
SSR	SSR	kA
<g/>
,	,	kIx,
koncem	koncem	k7c2
února	únor	k1gInSc2
1988	#num#	k4
<g/>
.	.	kIx.
</s>