<s desamb="1">
Oficiální	oficiální	k2eAgFnSc1d1
bilance	bilance	k1gFnSc1
masakru	masakr	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
seznamu	seznam	k1gInSc2
jmen	jméno	k1gNnPc2
obětí	oběť	k1gFnPc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
32	#num#	k4
lidí	člověk	k1gMnPc2
(	(	kIx(
<g/>
26	#num#	k4
Arménů	Armén	k1gMnPc2
a	a	k8xC
6	#num#	k4
Ázerů	Ázer	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
očití	očitý	k2eAgMnPc1d1
svědci	svědek	k1gMnPc1
hovoří	hovořit	k5eAaImIp3nP
o	o	k7c6
mnohem	mnohem	k6eAd1
větším	veliký	k2eAgInSc6d2
počtu	počet	k1gInSc6
mrtvých	mrtvý	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Mnozí	mnohý	k2eAgMnPc1d1
z	z	k7c2
nich	on	k3xPp3gMnPc2
zastvávají	zastvávat	k5eAaImIp3nP,k5eAaPmIp3nP
tvrzení	tvrzení	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
mrtvých	mrtvý	k1gMnPc2
bylo	být	k5eAaImAgNnS
přinejmenším	přinejmenším	k6eAd1
200	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>