<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Moravský	moravský	k2eAgInSc1d1
kras	kras	k1gInSc1
pokrývá	pokrývat	k5eAaImIp3nS
nejcennější	cenný	k2eAgFnSc2d3
části	část	k1gFnSc2
Moravského	moravský	k2eAgInSc2d1
krasu	kras	k1gInSc2
a	a	k8xC
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
<g/>
.	.	kIx.
</s>