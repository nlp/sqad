<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Moravský	moravský	k2eAgMnSc1d1
kras	kras	k1gInSc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuMoravský	infoboxuMoravský	k2eAgInSc1d1
krasIUCN	krasIUCN	k?
kategorie	kategorie	k1gFnSc1
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Punkevní	punkevní	k2eAgFnSc1d1
jeskyněZákladní	jeskyněZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1956	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
96,82	96,82	k4
km²	km²	k?
Správa	správa	k1gFnSc1
</s>
<s>
Svitavská	svitavský	k2eAgFnSc1d1
29	#num#	k4
<g/>
,	,	kIx,
678	#num#	k4
01	#num#	k4
Blansko	Blansko	k1gNnSc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
20	#num#	k4
<g/>
′	′	k?
<g/>
38,22	38,22	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
42	#num#	k4
<g/>
′	′	k?
<g/>
47,92	47,92	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Moravský	moravský	k2eAgInSc1d1
kras	kras	k1gInSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
</s>
<s>
72	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.moravskykras.ochranaprirody.cz	www.moravskykras.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
Moravském	moravský	k2eAgInSc6d1
krasu	kras	k1gInSc6
jako	jako	k8xS,k8xC
o	o	k7c6
chráněné	chráněný	k2eAgFnSc6d1
krajinné	krajinný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
oblasti	oblast	k1gFnSc6
v	v	k7c6
Jihomoravském	jihomoravský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Moravský	moravský	k2eAgInSc1d1
kras	kras	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Moravský	moravský	k2eAgInSc1d1
kras	kras	k1gInSc1
pokrývá	pokrývat	k5eAaImIp3nS
nejcennější	cenný	k2eAgFnSc2d3
části	část	k1gFnSc2
Moravského	moravský	k2eAgInSc2d1
krasu	kras	k1gInSc2
a	a	k8xC
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celková	celkový	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
je	být	k5eAaImIp3nS
96,82	96,82	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
vápenci	vápenec	k1gInPc7
středního	střední	k2eAgInSc2d1
devonu	devon	k1gInSc2
až	až	k6eAd1
spodního	spodní	k2eAgInSc2d1
karbonu	karbon	k1gInSc2
táhnoucí	táhnoucí	k2eAgFnSc2d1
se	se	k3xPyFc4
v	v	k7c6
severojižním	severojižní	k2eAgInSc6d1
směru	směr	k1gInSc6
mezi	mezi	k7c7
Sloupem	sloup	k1gInSc7
a	a	k8xC
Brnem	Brno	k1gNnSc7
na	na	k7c4
vzdálenost	vzdálenost	k1gFnSc4
25	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tento	tento	k3xDgInSc4
krajinný	krajinný	k2eAgInSc4d1
celek	celek	k1gInSc4
jsou	být	k5eAaImIp3nP
typické	typický	k2eAgFnPc1d1
krasové	krasový	k2eAgFnPc1d1
plošiny	plošina	k1gFnPc1
se	se	k3xPyFc4
závrty	závrt	k1gInPc7
<g/>
,	,	kIx,
hluboké	hluboký	k2eAgInPc1d1
žleby	žleb	k1gInPc1
se	s	k7c7
soutěskami	soutěska	k1gFnPc7
<g/>
,	,	kIx,
ponory	ponor	k1gInPc7
a	a	k8xC
vývěry	vývěr	k1gInPc7
vodních	vodní	k2eAgInPc2d1
toků	tok	k1gInPc2
<g/>
,	,	kIx,
propasti	propast	k1gFnSc2
a	a	k8xC
množství	množství	k1gNnSc2
jeskyní	jeskyně	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
byla	být	k5eAaImAgFnS
chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
vyhlášena	vyhlásit	k5eAaPmNgFnS
znova	znova	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
této	tento	k3xDgFnSc6
příležitosti	příležitost	k1gFnSc6
byla	být	k5eAaImAgFnS
rozloha	rozloha	k1gFnSc1
CHKO	CHKO	kA
rozšířena	rozšířit	k5eAaPmNgFnS
z	z	k7c2
91	#num#	k4
km²	km²	k?
na	na	k7c4
96,82	96,82	k4
km²	km²	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
byl	být	k5eAaImAgInS
na	na	k7c4
seznam	seznam	k1gInSc4
mokřadů	mokřad	k1gInPc2
chráněných	chráněný	k2eAgFnPc2d1
Ramsarskou	Ramsarský	k2eAgFnSc7d1
úmluvou	úmluva	k1gFnSc7
zapsán	zapsán	k2eAgInSc1d1
podzemní	podzemní	k2eAgInSc1d1
tok	tok	k1gInSc1
Punkvy	Punkva	k1gFnSc2
(	(	kIx(
<g/>
1571	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Moravském	moravský	k2eAgInSc6d1
krasu	kras	k1gInSc6
jsou	být	k5eAaImIp3nP
pestře	pestro	k6eAd1
zastoupena	zastoupit	k5eAaPmNgFnS
i	i	k9
vzácná	vzácný	k2eAgFnSc1d1
rostlinná	rostlinný	k2eAgFnSc1d1
a	a	k8xC
živočišná	živočišný	k2eAgNnPc1d1
společenstva	společenstvo	k1gNnPc1
a	a	k8xC
mnohé	mnohý	k2eAgFnPc1d1
jeskyně	jeskyně	k1gFnPc1
jsou	být	k5eAaImIp3nP
důležitými	důležitý	k2eAgFnPc7d1
archeologickými	archeologický	k2eAgFnPc7d1
lokalitami	lokalita	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
bylo	být	k5eAaImAgNnS
na	na	k7c6
území	území	k1gNnSc6
chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
pozorováno	pozorovat	k5eAaImNgNnS
úspěšné	úspěšný	k2eAgNnSc1d1
zahnízdění	zahnízdění	k1gNnSc1
a	a	k8xC
vyvedení	vyvedení	k1gNnSc1
mláďat	mládě	k1gNnPc2
sokola	sokol	k1gMnSc2
stěhovavého	stěhovavý	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
do	do	k7c2
lokality	lokalita	k1gFnSc2
navrátil	navrátit	k5eAaPmAgMnS
po	po	k7c6
50	#num#	k4
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
27	#num#	k4
druhů	druh	k1gInPc2
netopýrů	netopýr	k1gMnPc2
žijících	žijící	k2eAgMnPc2d1
na	na	k7c6
území	území	k1gNnSc6
ČR	ČR	kA
byl	být	k5eAaImAgInS
u	u	k7c2
24	#num#	k4
potvrzen	potvrdit	k5eAaPmNgInS
výskyt	výskyt	k1gInSc1
na	na	k7c4
území	území	k1gNnSc4
chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Moravský	moravský	k2eAgInSc1d1
kras	kras	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
NAŘÍZENÍ	nařízení	k1gNnSc1
VLÁDY	vláda	k1gFnSc2
ze	z	k7c2
dne	den	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
o	o	k7c6
Chráněné	chráněný	k2eAgFnSc6d1
krajinné	krajinný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Moravský	moravský	k2eAgInSc1d1
kras	kras	k1gInSc1
-	-	kIx~
Sbírka	sbírka	k1gFnSc1
zákonů	zákon	k1gInPc2
<g/>
.	.	kIx.
www.sagit.cz	www.sagit.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
CHKO	CHKO	kA
Moravský	moravský	k2eAgInSc1d1
kras	kras	k1gInSc1
se	se	k3xPyFc4
po	po	k7c6
šedesáti	šedesát	k4xCc6
letech	léto	k1gNnPc6
rozšíří	rozšířit	k5eAaPmIp3nS
<g/>
,	,	kIx,
dostane	dostat	k5eAaPmIp3nS
navíc	navíc	k6eAd1
šest	šest	k4xCc4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekolist	Ekolist	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-19	2019-03-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Do	do	k7c2
Moravského	moravský	k2eAgInSc2d1
krasu	kras	k1gInSc2
se	se	k3xPyFc4
po	po	k7c6
50	#num#	k4
letech	léto	k1gNnPc6
vrátil	vrátit	k5eAaPmAgMnS
sokol	sokol	k1gMnSc1
stěhovavý	stěhovavý	k2eAgMnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekolist	Ekolist	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
S.	S.	kA
<g/>
R.	R.	kA
<g/>
O	O	kA
<g/>
,	,	kIx,
VIZUS	VIZUS	kA
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fauna	fauna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AOPK	AOPK	kA
ČR	ČR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Moravský	moravský	k2eAgInSc1d1
kras	kras	k1gInSc1
</s>
<s>
Karsologie	Karsologie	k1gFnSc1
</s>
<s>
Speleologie	speleologie	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Moravský	moravský	k2eAgInSc1d1
kras	kras	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Moravský	moravský	k2eAgInSc1d1
kras	kras	k1gInSc1
(	(	kIx(
<g/>
oficiální	oficiální	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Správa	správa	k1gFnSc1
jeskyní	jeskyně	k1gFnPc2
Moravského	moravský	k2eAgInSc2d1
krasu	kras	k1gInSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
Brně	Brno	k1gNnSc6
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Moravský	moravský	k2eAgInSc1d1
kras	kras	k1gInSc1
Přírodní	přírodní	k2eAgInSc1d1
parky	park	k1gInPc4
</s>
<s>
Baba	baba	k1gFnSc1
•	•	k?
Podkomorské	podkomorský	k2eAgInPc4d1
lesy	les	k1gInPc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Hádecká	hádecký	k2eAgFnSc1d1
planinka	planinka	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Červený	červený	k2eAgInSc1d1
kopec	kopec	k1gInSc1
•	•	k?
Stránská	Stránská	k1gFnSc1
skála	skála	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Babí	babí	k2eAgInPc1d1
doly	dol	k1gInPc1
•	•	k?
Bosonožský	Bosonožský	k2eAgInSc1d1
hájek	hájek	k1gInSc1
•	•	k?
Břenčák	Břenčák	k1gInSc1
•	•	k?
Černovický	Černovický	k2eAgInSc1d1
hájek	hájek	k1gInSc1
•	•	k?
Jelení	jelení	k2eAgInSc1d1
žlíbek	žlíbek	k1gInSc1
•	•	k?
Kamenný	kamenný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Krnovec	Krnovec	k1gInSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
Hornek	Hornek	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Augšperský	Augšperský	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Bílá	bílý	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Holásecká	Holásecká	k1gFnSc1
jezera	jezero	k1gNnSc2
•	•	k?
Junácká	junácký	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Kavky	kavka	k1gFnSc2
•	•	k?
Kůlny	kůlna	k1gFnSc2
•	•	k?
Medlánecká	Medlánecký	k2eAgFnSc1d1
skalka	skalka	k1gFnSc1
•	•	k?
Medlánecké	Medlánecký	k2eAgFnSc6d1
kopce	kopka	k1gFnSc6
•	•	k?
Mniší	mniší	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Na	na	k7c6
Skalách	skála	k1gFnPc6
•	•	k?
Netopýrky	netopýrek	k1gMnPc4
•	•	k?
Obřanská	Obřanský	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Pekárna	pekárna	k1gFnSc1
•	•	k?
Rájecká	Rájecký	k2eAgFnSc1d1
tůň	tůň	k1gFnSc1
•	•	k?
Skalky	skalka	k1gFnSc2
u	u	k7c2
Přehrady	přehrada	k1gFnSc2
•	•	k?
Soběšické	Soběšický	k2eAgInPc1d1
rybníčky	rybníček	k1gInPc1
•	•	k?
Údolí	údolí	k1gNnSc6
Kohoutovického	kohoutovický	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Velká	velký	k2eAgFnSc1d1
Klajdovka	Klajdovka	k1gFnSc1
•	•	k?
Žebětínský	Žebětínský	k2eAgInSc4d1
rybník	rybník	k1gInSc4
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Brno-venkov	Brno-venkov	k1gInSc1
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Moravský	moravský	k2eAgInSc1d1
kras	kras	k1gInSc1
Přírodní	přírodní	k2eAgInSc1d1
parky	park	k1gInPc4
</s>
<s>
Baba	baba	k1gFnSc1
•	•	k?
Bobrava	Bobrava	k1gFnSc1
•	•	k?
Lysicko	Lysicko	k1gNnSc1
•	•	k?
Niva	niva	k1gFnSc1
Jihlavy	Jihlava	k1gFnSc2
•	•	k?
Oslava	oslava	k1gFnSc1
•	•	k?
Podkomorské	podkomorský	k2eAgInPc4d1
lesy	les	k1gInPc4
•	•	k?
Říčky	říčka	k1gFnSc2
•	•	k?
Střední	střední	k2eAgMnPc1d1
Pojihlaví	Pojihlavý	k2eAgMnPc1d1
•	•	k?
Svratecká	svratecký	k2eAgFnSc1d1
hornatina	hornatina	k1gFnSc1
•	•	k?
Údolí	údolí	k1gNnSc1
Bílého	bílý	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Výhon	výhon	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Býčí	býčí	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Hádecká	hádecký	k2eAgFnSc1d1
planinka	planinka	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Jeskyně	jeskyně	k1gFnSc1
Pekárna	pekárna	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Babí	babí	k2eAgInSc1d1
lom	lom	k1gInSc1
•	•	k?
Coufavá	Coufavý	k2eAgFnSc1d1
•	•	k?
Čihadlo	čihadlo	k1gNnSc4
•	•	k?
Dřínová	dřínový	k2eAgFnSc1d1
•	•	k?
Holé	Holé	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
•	•	k?
Hrádky	Hrádok	k1gInPc1
•	•	k?
Jelení	jelení	k2eAgInSc4d1
skok	skok	k1gInSc4
•	•	k?
Malužín	Malužín	k1gInSc1
•	•	k?
Nad	nad	k7c7
horou	hora	k1gFnSc7
•	•	k?
Nad	nad	k7c7
řekami	řeka	k1gFnPc7
•	•	k?
Obůrky-Třeštěnec	Obůrky-Třeštěnec	k1gMnSc1
•	•	k?
Plačkův	Plačkův	k2eAgInSc1d1
les	les	k1gInSc1
a	a	k8xC
říčka	říčka	k1gFnSc1
Šatava	Šatava	k1gFnSc1
•	•	k?
Pod	pod	k7c7
Sýkořskou	Sýkořský	k2eAgFnSc7d1
myslivnou	myslivna	k1gFnSc7
•	•	k?
Slunná	slunný	k2eAgFnSc1d1
•	•	k?
Sokolí	sokolí	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Špice	špice	k1gFnSc2
•	•	k?
Šumický	šumický	k2eAgInSc4d1
rybník	rybník	k1gInSc4
•	•	k?
U	u	k7c2
Brněnky	Brněnka	k1gFnSc2
•	•	k?
Údolí	údolí	k1gNnSc1
Oslavy	oslava	k1gFnSc2
a	a	k8xC
Chvojnice	Chvojnice	k1gFnSc2
•	•	k?
Údolí	údolí	k1gNnSc1
Říčky	říčka	k1gFnSc2
•	•	k?
Velká	velký	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
Hornek	Hornek	k1gInSc1
•	•	k?
Věstonická	věstonický	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
•	•	k?
Zadní	zadní	k2eAgInPc4d1
Hády	hádes	k1gInPc4
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Andělka	Andělka	k1gFnSc1
a	a	k8xC
Čertovka	Čertovka	k1gFnSc1
•	•	k?
Betlém	Betlém	k1gInSc1
•	•	k?
Bezourek	Bezourek	k1gMnSc1
•	•	k?
Biskoupská	Biskoupský	k2eAgFnSc1d1
hadcová	hadcový	k2eAgFnSc1d1
step	step	k1gFnSc1
•	•	k?
Biskoupský	Biskoupský	k2eAgInSc1d1
kopec	kopec	k1gInSc1
•	•	k?
Bouchal	bouchal	k1gMnSc1
•	•	k?
Březina	Březina	k1gMnSc1
•	•	k?
Budkovické	Budkovický	k2eAgInPc1d1
slepence	slepenec	k1gInPc1
•	•	k?
Červené	Červené	k2eAgFnSc2d1
stráně	stráň	k1gFnSc2
•	•	k?
Dobrá	dobrý	k2eAgFnSc1d1
studně	studně	k1gFnSc1
•	•	k?
Dolní	dolní	k2eAgInSc1d1
mušovský	mušovský	k2eAgInSc1d1
luh	luh	k1gInSc1
•	•	k?
Drásovský	Drásovský	k2eAgInSc1d1
kopeček	kopeček	k1gInSc1
(	(	kIx(
<g/>
zrušená	zrušený	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Horka	horko	k1gNnSc2
•	•	k?
Horní	horní	k2eAgFnSc1d1
Židovka	Židovka	k1gFnSc1
•	•	k?
Hrušín	Hrušína	k1gFnPc2
•	•	k?
Hynčicovy	Hynčicův	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Klášterce	Klášterec	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Kněžnice	Kněžnice	k1gFnSc2
•	•	k?
Knížecí	knížecí	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Květnice	Květnice	k1gFnSc2
•	•	k?
Luzichová	Luzichový	k2eAgFnSc1d1
•	•	k?
Malhostovická	Malhostovický	k2eAgFnSc1d1
pecka	pecka	k1gFnSc1
(	(	kIx(
<g/>
zrušená	zrušený	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Malhostovické	Malhostovický	k2eAgInPc1d1
kopečky	kopeček	k1gInPc1
•	•	k?
Míchovec	Míchovec	k1gInSc1
•	•	k?
Na	na	k7c6
hájku	hájek	k1gInSc6
(	(	kIx(
<g/>
zrušená	zrušený	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Na	na	k7c6
kutinách	kutinách	k?
•	•	k?
Na	na	k7c6
lesní	lesní	k2eAgFnSc6d1
horce	horka	k1gFnSc6
•	•	k?
Návrší	návrš	k1gFnPc2
(	(	kIx(
<g/>
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Nosislavská	Nosislavský	k2eAgFnSc1d1
zátočina	zátočina	k1gFnSc1
•	•	k?
Nové	Nová	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
hory	hora	k1gFnPc4
•	•	k?
Padělky	padělek	k1gInPc1
•	•	k?
Patočkova	Patočkův	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Pekárka	Pekárek	k1gMnSc2
•	•	k?
Pilský	Pilský	k2eAgInSc4d1
rybníček	rybníček	k1gInSc4
•	•	k?
Písky	Písek	k1gInPc4
•	•	k?
Pláně	pláň	k1gFnSc2
•	•	k?
Přísnotický	Přísnotický	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Pustý	pustý	k2eAgInSc1d1
mlýn	mlýn	k1gInSc1
•	•	k?
Rojetínský	Rojetínský	k2eAgInSc1d1
hadec	hadec	k1gInSc1
•	•	k?
Rybičková	Rybičková	k1gFnSc1
skála	skála	k1gFnSc1
•	•	k?
Santon	Santon	k1gInSc1
•	•	k?
Střelická	Střelický	k2eAgFnSc1d1
bažinka	bažinka	k1gFnSc1
•	•	k?
Střelický	Střelický	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Svídovec	Svídovec	k1gInSc1
•	•	k?
Sýkoř	Sýkoř	k1gFnSc1
•	•	k?
Synalovské	Synalovský	k2eAgFnSc2d1
kopaniny	kopanina	k1gFnSc2
•	•	k?
Šiberná	Šiberný	k2eAgFnSc1d1
•	•	k?
Trenckova	Trenckův	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
•	•	k?
Troskotovický	Troskotovický	k2eAgInSc1d1
dolní	dolní	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
U	u	k7c2
Staré	Staré	k2eAgFnSc2d1
Vápenice	vápenice	k1gFnSc2
•	•	k?
Údolí	údolí	k1gNnSc1
Chlébského	Chlébský	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
V	v	k7c6
olších	olše	k1gFnPc6
•	•	k?
Velatická	Velatický	k2eAgFnSc1d1
slepencová	slepencový	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Velké	velká	k1gFnSc2
Družďavy	Družďava	k1gFnSc2
•	•	k?
Velký	velký	k2eAgInSc1d1
hájek	hájek	k1gInSc1
•	•	k?
Veselská	Veselská	k1gFnSc1
lada	lado	k1gNnSc2
•	•	k?
Veselský	veselský	k2eAgInSc4d1
chlum	chlum	k1gInSc4
•	•	k?
Vinohrady	Vinohrady	k1gInPc4
•	•	k?
Zámecký	zámecký	k2eAgInSc4d1
les	les	k1gInSc4
v	v	k7c6
Lomnici	Lomnice	k1gFnSc6
•	•	k?
Zhořská	Zhořský	k2eAgFnSc1d1
mokřina	mokřina	k1gFnSc1
•	•	k?
Zlobice	Zlobice	k1gFnSc2
•	•	k?
Žabárník	Žabárník	k1gInSc1
(	(	kIx(
<g/>
zrušená	zrušený	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Žebětín	Žebětín	k1gInSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Blansko	Blansko	k1gNnSc4
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Moravský	moravský	k2eAgInSc1d1
kras	kras	k1gInSc1
Přírodní	přírodní	k2eAgInSc1d1
parky	park	k1gInPc4
</s>
<s>
Halasovo	Halasův	k2eAgNnSc1d1
Kunštátsko	Kunštátsko	k1gNnSc1
•	•	k?
Lysicko	Lysicko	k1gNnSc1
•	•	k?
Rakovecké	Rakovecký	k2eAgNnSc4d1
údolí	údolí	k1gNnSc4
•	•	k?
Řehořkovo	Řehořkův	k2eAgNnSc4d1
Kořenecko	Kořenecko	k1gNnSc4
•	•	k?
Svratecká	svratecký	k2eAgFnSc1d1
hornatina	hornatina	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Býčí	býčí	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Habrůvecká	habrůvecký	k2eAgFnSc1d1
bučina	bučina	k1gFnSc1
•	•	k?
Vývěry	vývěr	k1gInPc1
Punkvy	Punkva	k1gFnSc2
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Rudické	rudický	k2eAgNnSc1d1
propadání	propadání	k1gNnSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Babí	babí	k2eAgInSc1d1
lom	lom	k1gInSc1
•	•	k?
Balcarova	Balcarův	k2eAgFnSc1d1
skála	skála	k1gFnSc1
–	–	k?
Vintoky	Vintok	k1gInPc1
•	•	k?
Bayerova	Bayerův	k2eAgFnSc1d1
•	•	k?
Bílá	bílý	k2eAgFnSc1d1
voda	voda	k1gFnSc1
•	•	k?
Čepičkův	Čepičkův	k2eAgInSc1d1
vrch	vrch	k1gInSc1
a	a	k8xC
údolí	údolí	k1gNnPc1
Hodonínky	Hodonínka	k1gFnSc2
•	•	k?
Durana	Duran	k1gMnSc2
•	•	k?
Kavinský	Kavinský	k2eAgInSc4d1
potok	potok	k1gInSc4
•	•	k?
Louky	louka	k1gFnSc2
pod	pod	k7c7
Kulíškem	kulíšek	k1gMnSc7
•	•	k?
Mokřad	mokřad	k1gInSc1
pod	pod	k7c7
Tipečkem	tipeček	k1gInSc7
•	•	k?
Pavlovské	pavlovský	k2eAgInPc1d1
mokřady	mokřad	k1gInPc1
•	•	k?
Ploník	ploník	k1gInSc1
•	•	k?
Rakovec	Rakovec	k1gInSc4
•	•	k?
Rakovecké	Rakovecký	k2eAgFnSc2d1
stráně	stráň	k1gFnSc2
a	a	k8xC
údolí	údolí	k1gNnSc2
bledulí	bledule	k1gFnPc2
•	•	k?
U	u	k7c2
Nového	Nového	k2eAgInSc2d1
hradu	hrad	k1gInSc2
•	•	k?
U	u	k7c2
Výpustku	výpustek	k1gInSc2
•	•	k?
Vratíkov	Vratíkov	k1gInSc1
Přírodní	přírodní	k2eAgInSc1d1
památky	památka	k1gFnSc2
</s>
<s>
Babolský	Babolský	k2eAgInSc1d1
háj	háj	k1gInSc1
•	•	k?
Bačov	Bačov	k1gInSc1
•	•	k?
Cukl	cuknout	k5eAaPmAgInS
a	a	k8xC
Rozsečské	Rozsečský	k2eAgNnSc1d1
rašeliniště	rašeliniště	k1gNnSc1
•	•	k?
Černá	černat	k5eAaImIp3nS
skála	skála	k1gFnSc1
•	•	k?
Čtvrtky	čtvrtka	k1gFnSc2
za	za	k7c4
Bořím	bořit	k5eAaImIp1nS
•	•	k?
Habrová	habrový	k2eAgFnSc1d1
•	•	k?
Hersica	Hersica	k1gFnSc1
•	•	k?
Horní	horní	k2eAgFnSc1d1
Bělá	bělat	k5eAaImIp3nS
•	•	k?
Kačiny	Kačina	k1gFnSc2
•	•	k?
Krkatá	krkatý	k2eAgFnSc1d1
bába	bába	k1gFnSc1
•	•	k?
Křtinský	Křtinský	k2eAgInSc1d1
lom	lom	k1gInSc1
•	•	k?
Kunštátská	kunštátský	k2eAgFnSc1d1
obora	obora	k1gFnSc1
•	•	k?
Lebeďák	Lebeďák	k1gInSc1
•	•	k?
Lhotské	Lhotské	k2eAgInSc2d1
jalovce	jalovec	k1gInSc2
a	a	k8xC
stěny	stěna	k1gFnSc2
•	•	k?
Loucká	Loucký	k2eAgFnSc1d1
obora	obora	k1gFnSc1
•	•	k?
Lysická	Lysický	k2eAgFnSc1d1
obora	obora	k1gFnSc1
•	•	k?
Nad	nad	k7c7
Berankou	beranka	k1gFnSc7
•	•	k?
Park	park	k1gInSc4
Letovice	Letovice	k1gFnSc2
•	•	k?
Údolí	údolí	k1gNnSc1
Chlébského	Chlébský	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
V	v	k7c6
Jezdinách	Jezdina	k1gFnPc6
•	•	k?
Žižkův	Žižkův	k2eAgInSc4d1
stůl	stůl	k1gInSc4
</s>
