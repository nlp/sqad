<s>
Ostravice	Ostravice	k1gFnSc1	Ostravice
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Ostrawica	Ostrawic	k2eAgFnSc1d1	Ostrawic
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Ostrawitza	Ostrawitza	k1gFnSc1	Ostrawitza
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
okresech	okres	k1gInPc6	okres
Frýdek-Místek	Frýdek-Místka	k1gFnPc2	Frýdek-Místka
a	a	k8xC	a
Ostrava-město	Ostravaěsta	k1gMnSc5	Ostrava-města
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
toku	tok	k1gInSc2	tok
činí	činit	k5eAaImIp3nS	činit
65,1	[number]	k4	65,1
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
měří	měřit	k5eAaImIp3nS	měřit
827,4	[number]	k4	827,4
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
tok	tok	k1gInSc1	tok
tvoří	tvořit	k5eAaImIp3nS	tvořit
zčásti	zčásti	k6eAd1	zčásti
historickou	historický	k2eAgFnSc4d1	historická
zemskou	zemský	k2eAgFnSc4d1	zemská
hranici	hranice	k1gFnSc4	hranice
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
tato	tento	k3xDgFnSc1	tento
hranice	hranice	k1gFnSc1	hranice
podél	podél	k7c2	podél
jejího	její	k3xOp3gInSc2	její
toku	tok	k1gInSc2	tok
mírně	mírně	k6eAd1	mírně
osciluje	oscilovat	k5eAaImIp3nS	oscilovat
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
získala	získat	k5eAaPmAgFnS	získat
název	název	k1gInSc4	název
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
prudkého	prudký	k2eAgInSc2d1	prudký
(	(	kIx(	(
<g/>
ostrého	ostrý	k2eAgInSc2d1	ostrý
<g/>
)	)	kIx)	)
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Řeka	Řek	k1gMnSc2	Řek
vzniká	vznikat	k5eAaImIp3nS	vznikat
soutokem	soutok	k1gInSc7	soutok
Černé	Černé	k2eAgFnSc2d1	Černé
a	a	k8xC	a
Bílé	bílý	k2eAgFnSc2d1	bílá
Ostravice	Ostravice	k1gFnSc2	Ostravice
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Staré	Staré	k2eAgInPc1d1	Staré
Hamry	Hamry	k1gInPc1	Hamry
v	v	k7c6	v
Moravskoslezských	moravskoslezský	k2eAgInPc6d1	moravskoslezský
Beskydech	Beskyd	k1gInPc6	Beskyd
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
Ostravice	Ostravice	k1gFnSc1	Ostravice
pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
svahu	svah	k1gInSc6	svah
Smrkoviny	smrkovina	k1gFnSc2	smrkovina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
západně	západně	k6eAd1	západně
od	od	k7c2	od
osady	osada	k1gFnSc2	osada
Bílý	bílý	k2eAgMnSc1d1	bílý
Kříž	Kříž	k1gMnSc1	Kříž
<g/>
,	,	kIx,	,
Bílá	bílý	k2eAgFnSc1d1	bílá
Ostravice	Ostravice	k1gFnSc1	Ostravice
pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
úbočí	úbočí	k1gNnSc6	úbočí
vrcholku	vrcholek	k1gInSc2	vrcholek
Čarták	Čarták	k1gInSc1	Čarták
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
západně	západně	k6eAd1	západně
od	od	k7c2	od
Bumbálky	bumbálek	k1gMnPc4	bumbálek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
řeky	řeka	k1gFnSc2	řeka
leží	ležet	k5eAaImIp3nS	ležet
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
Šance	šance	k1gFnSc1	šance
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
obce	obec	k1gFnSc2	obec
Ostravice	Ostravice	k1gFnSc2	Ostravice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
chráněný	chráněný	k2eAgInSc1d1	chráněný
přírodní	přírodní	k2eAgInSc1d1	přírodní
útvar	útvar	k1gInSc1	útvar
-	-	kIx~	-
peřeje	peřej	k1gFnPc1	peřej
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
protéká	protékat	k5eAaImIp3nS	protékat
Frýdlantem	Frýdlant	k1gInSc7	Frýdlant
nad	nad	k7c7	nad
Ostravicí	Ostravice	k1gFnSc7	Ostravice
<g/>
,	,	kIx,	,
Paskovem	Paskov	k1gInSc7	Paskov
<g/>
,	,	kIx,	,
Frýdkem-Místkem	Frýdkem-Místek	k1gInSc7	Frýdkem-Místek
a	a	k8xC	a
Ostravou	Ostrava	k1gFnSc7	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
jako	jako	k9	jako
pravostranný	pravostranný	k2eAgInSc1d1	pravostranný
přítok	přítok	k1gInSc1	přítok
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Odry	Odra	k1gFnSc2	Odra
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
soutoku	soutok	k1gInSc2	soutok
Bílé	bílý	k2eAgFnSc2d1	bílá
a	a	k8xC	a
Černé	Černé	k2eAgFnSc2d1	Černé
Ostravice	Ostravice	k1gFnSc2	Ostravice
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
levý	levý	k2eAgMnSc1d1	levý
<g/>
/	/	kIx~	/
<g/>
pravý	pravý	k2eAgMnSc1d1	pravý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
tučně	tučně	k6eAd1	tučně
do	do	k7c2	do
přehrady	přehrada	k1gFnSc2	přehrada
Šance	šance	k1gFnSc1	šance
Červík	červík	k1gMnSc1	červík
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
Velký	velký	k2eAgInSc1d1	velký
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
Jamník	jamník	k1gMnSc1	jamník
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
Poledňana	Poledňan	k1gMnSc2	Poledňan
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
Řečice	Řečice	k1gFnSc1	Řečice
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Ostravice	Ostravice	k1gFnSc1	Ostravice
Mazák	mazák	k1gMnSc1	mazák
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
Bučací	Bučací	k2eAgInSc1d1	Bučací
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
Sepetný	Sepetný	k2eAgMnSc1d1	Sepetný
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
Řasník	Řasník	k1gMnSc1	Řasník
se	s	k7c7	s
Stříbrníkem	stříbrník	k1gMnSc7	stříbrník
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
Čeladenka	Čeladenka	k1gFnSc1	Čeladenka
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
Bílý	bílý	k2eAgInSc1d1	bílý
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
Frýdlantská	frýdlantský	k2eAgFnSc1d1	Frýdlantská
Ondřejnice	Ondřejnice	k1gFnSc1	Ondřejnice
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
Satina	Satina	k1gMnSc1	Satina
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
Sibudov	Sibudov	k1gInSc1	Sibudov
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
Lubenec	Lubenec	k1gMnSc1	Lubenec
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
Bystrý	bystrý	k2eAgInSc1d1	bystrý
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
Baštice	Baštice	k1gFnSc1	Baštice
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
Morávka	Morávek	k1gMnSc2	Morávek
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
Podšajarka	Podšajarka	k1gFnSc1	Podšajarka
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
Ostravická	ostravický	k2eAgFnSc1d1	Ostravická
Datyňka	Datyňka	k1gFnSc1	Datyňka
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
Olešná	Olešný	k2eAgFnSc1d1	Olešná
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
Lučina	lučina	k1gFnSc1	lučina
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
Hlásné	hlásný	k2eAgInPc4d1	hlásný
profily	profil	k1gInPc4	profil
<g/>
:	:	kIx,	:
</s>
