<p>
<s>
Boissiè	Boissiè	k?	Boissiè
je	být	k5eAaImIp3nS	být
nepřestupní	přestupní	k2eNgFnSc1d1	nepřestupní
stanice	stanice	k1gFnSc1	stanice
pařížského	pařížský	k2eAgNnSc2d1	pařížské
metra	metro	k1gNnSc2	metro
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
6	[number]	k4	6
v	v	k7c4	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
Avenue	avenue	k1gFnSc7	avenue
Kléber	Klébra	k1gFnPc2	Klébra
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
s	s	k7c7	s
ulicí	ulice	k1gFnSc7	ulice
Rue	Rue	k1gFnSc7	Rue
Boissiè	Boissiè	k1gFnSc7	Boissiè
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1900	[number]	k4	1900
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
prodloužení	prodloužení	k1gNnSc2	prodloužení
linky	linka	k1gFnSc2	linka
1	[number]	k4	1
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Charles	Charles	k1gMnSc1	Charles
de	de	k?	de
Gaulle	Gaulle	k1gInSc1	Gaulle
–	–	k?	–
Étoile	Étoila	k1gFnSc3	Étoila
↔	↔	k?	↔
Trocadéro	Trocadéro	k1gNnSc4	Trocadéro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1903	[number]	k4	1903
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
linka	linka	k1gFnSc1	linka
2	[number]	k4	2
Sud	suda	k1gFnPc2	suda
(	(	kIx(	(
<g/>
2	[number]	k4	2
Jih	jih	k1gInSc1	jih
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
nazývána	nazýván	k2eAgFnSc1d1	nazývána
Circulaire	Circulair	k1gInSc5	Circulair
Sud	sud	k1gInSc1	sud
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgInSc1d1	jižní
okruh	okruh	k1gInSc1	okruh
<g/>
)	)	kIx)	)
odpojením	odpojení	k1gNnSc7	odpojení
od	od	k7c2	od
linky	linka	k1gFnSc2	linka
1	[number]	k4	1
mezi	mezi	k7c7	mezi
stanicemi	stanice	k1gFnPc7	stanice
Étoile	Étoila	k1gFnSc6	Étoila
a	a	k8xC	a
Passy	Passa	k1gFnSc2	Passa
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1907	[number]	k4	1907
byla	být	k5eAaImAgFnS	být
linka	linka	k1gFnSc1	linka
2	[number]	k4	2
Sud	suda	k1gFnPc2	suda
zrušena	zrušen	k2eAgFnSc1d1	zrušena
a	a	k8xC	a
stanice	stanice	k1gFnSc1	stanice
Boissiè	Boissiè	k1gFnSc1	Boissiè
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
linky	linka	k1gFnSc2	linka
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1942	[number]	k4	1942
byl	být	k5eAaImAgMnS	být
úsek	úsek	k1gInSc4	úsek
Étoile	Étoila	k1gFnSc3	Étoila
↔	↔	k?	↔
Place	plac	k1gInSc6	plac
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italie	Italie	k1gFnSc1	Italie
opět	opět	k6eAd1	opět
odpojen	odpojit	k5eAaPmNgInS	odpojit
od	od	k7c2	od
linky	linka	k1gFnSc2	linka
5	[number]	k4	5
a	a	k8xC	a
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
linkou	linka	k1gFnSc7	linka
6	[number]	k4	6
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tak	tak	k6eAd1	tak
získala	získat	k5eAaPmAgFnS	získat
dnešní	dnešní	k2eAgFnSc4d1	dnešní
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1730	[number]	k4	1730
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
ulice	ulice	k1gFnSc1	ulice
Boissiè	Boissiè	k1gFnSc1	Boissiè
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
ven	ven	k6eAd1	ven
z	z	k7c2	z
města	město	k1gNnSc2	město
a	a	k8xC	a
navazovala	navazovat	k5eAaImAgFnS	navazovat
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Rue	Rue	k1gMnPc2	Rue
de	de	k?	de
la	la	k1gNnPc2	la
Croix-Boissiè	Croix-Boissiè	k1gFnSc7	Croix-Boissiè
umístěnou	umístěný	k2eAgFnSc7d1	umístěná
uvnitř	uvnitř	k7c2	uvnitř
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
jméno	jméno	k1gNnSc1	jméno
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
podle	podle	k7c2	podle
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
</s>
</p>
<p>
<s>
na	na	k7c4	na
Květnou	květný	k2eAgFnSc4d1	Květná
neděli	neděle	k1gFnSc4	neděle
ze	z	k7c2	z
zvyku	zvyk	k1gInSc2	zvyk
zavěšoval	zavěšovat	k5eAaImAgMnS	zavěšovat
zimostráz	zimostráz	k1gInSc4	zimostráz
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
buis	buis	k6eAd1	buis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vstupy	vstup	k1gInPc4	vstup
==	==	k?	==
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc1	jeden
vchod	vchod	k1gInSc1	vchod
na	na	k7c4	na
Avenue	avenue	k1gFnSc4	avenue
Kléber	Klébra	k1gFnPc2	Klébra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Boissiè	Boissiè	k1gFnSc2	Boissiè
(	(	kIx(	(
<g/>
métro	métro	k6eAd1	métro
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Boissiè	Boissiè	k1gFnSc2	Boissiè
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Záznam	záznam	k1gInSc1	záznam
v	v	k7c6	v
evidenci	evidence	k1gFnSc6	evidence
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
</s>
</p>
