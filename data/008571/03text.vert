<p>
<s>
Aletschhorn	Aletschhorn	k1gNnSc1	Aletschhorn
(	(	kIx(	(
<g/>
4192	[number]	k4	4192
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
v	v	k7c6	v
Bernských	bernský	k2eAgFnPc6d1	Bernská
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
území	území	k1gNnSc4	území
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
v	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
Valais	Valais	k1gFnSc2	Valais
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druhý	druhý	k4xOgInSc4	druhý
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
štít	štít	k1gInSc4	štít
Bernských	bernský	k2eAgFnPc2d1	Bernská
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Jungfrau-Aletsch-Bietschhorn	Jungfrau-Aletsch-Bietschhorna	k1gFnPc2	Jungfrau-Aletsch-Bietschhorna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
zapsána	zapsat	k5eAaPmNgFnS	zapsat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Aletschhorn	Aletschhorn	k1gInSc1	Aletschhorn
leží	ležet	k5eAaImIp3nS	ležet
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Eigeru	Eiger	k1gInSc2	Eiger
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
lze	lze	k6eAd1	lze
vystoupit	vystoupit	k5eAaPmF	vystoupit
od	od	k7c2	od
Konkordiahütte	Konkordiahütt	k1gInSc5	Konkordiahütt
(	(	kIx(	(
<g/>
2850	[number]	k4	2850
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hollandiahütte	Hollandiahütt	k1gMnSc5	Hollandiahütt
(	(	kIx(	(
<g/>
3235	[number]	k4	3235
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Oberaletschhütte	Oberaletschhütt	k1gMnSc5	Oberaletschhütt
(	(	kIx(	(
<g/>
2640	[number]	k4	2640
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mittelaletschbiwak	Mittelaletschbiwak	k1gMnSc1	Mittelaletschbiwak
(	(	kIx(	(
<g/>
3013	[number]	k4	3013
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mönchsjochhütte	Mönchsjochhütt	k1gInSc5	Mönchsjochhütt
(	(	kIx(	(
<g/>
3650	[number]	k4	3650
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Horu	hora	k1gFnSc4	hora
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
ledovce	ledovec	k1gInPc1	ledovec
Aletschgletscher	Aletschgletschra	k1gFnPc2	Aletschgletschra
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
Oberaletschgletscher	Oberaletschgletschra	k1gFnPc2	Oberaletschgletschra
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
a	a	k8xC	a
Mittelaletschgletscher	Mittelaletschgletschra	k1gFnPc2	Mittelaletschgletschra
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
jako	jako	k8xC	jako
první	první	k4xOgFnSc7	první
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1859	[number]	k4	1859
Johann	Johann	k1gMnSc1	Johann
Joseph	Joseph	k1gMnSc1	Joseph
Bennen	Bennen	k2eAgMnSc1d1	Bennen
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Bohren	Bohrna	k1gFnPc2	Bohrna
<g/>
,	,	kIx,	,
Victor	Victor	k1gMnSc1	Victor
Tairraz	Tairraz	k1gInSc1	Tairraz
a	a	k8xC	a
Francis	Francis	k1gInSc1	Francis
Fox	fox	k1gInSc1	fox
Tuckett	Tuckett	k2eAgInSc1d1	Tuckett
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Aletschhorn	Aletschhorna	k1gFnPc2	Aletschhorna
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Eiger	Eiger	k1gMnSc1	Eiger
</s>
</p>
<p>
<s>
Jungfrau	Jungfrau	k1gFnSc1	Jungfrau
</s>
</p>
<p>
<s>
Mönch	Mönch	k1gMnSc1	Mönch
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Aletschhorn	Aletschhorn	k1gInSc1	Aletschhorn
na	na	k7c4	na
SummitPost	SummitPost	k1gFnSc4	SummitPost
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
Aletschhorn	Aletschhorn	k1gInSc1	Aletschhorn
-	-	kIx~	-
výstup	výstup	k1gInSc1	výstup
od	od	k7c2	od
chaty	chata	k1gFnSc2	chata
Konkordia	Konkordium	k1gNnSc2	Konkordium
</s>
</p>
