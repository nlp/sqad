<s>
Lilek	lilek	k1gInSc1	lilek
brambor	brambora	k1gFnPc2	brambora
<g/>
,	,	kIx,	,
též	též	k9	též
brambor	brambor	k1gInSc1	brambor
obecný	obecný	k2eAgInSc1d1	obecný
či	či	k8xC	či
brambor	brambor	k1gInSc1	brambor
hlíznatý	hlíznatý	k2eAgInSc1d1	hlíznatý
(	(	kIx(	(
<g/>
Solanum	Solanum	k1gInSc1	Solanum
tuberosum	tuberosum	k1gInSc1	tuberosum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
řeči	řeč	k1gFnSc6	řeč
jen	jen	k6eAd1	jen
brambor	brambora	k1gFnPc2	brambora
nebo	nebo	k8xC	nebo
také	také	k9	také
brambora	brambora	k1gFnSc1	brambora
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
brambory	brambora	k1gFnSc2	brambora
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
víceletá	víceletý	k2eAgFnSc1d1	víceletá
hlíznatá	hlíznatý	k2eAgFnSc1d1	hlíznatá
rostlina	rostlina	k1gFnSc1	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lilkovité	lilkovitý	k2eAgFnPc1d1	lilkovitá
<g/>
,	,	kIx,	,
pěstovaná	pěstovaný	k2eAgFnSc1d1	pěstovaná
jako	jako	k8xS	jako
jednoletá	jednoletý	k2eAgFnSc1d1	jednoletá
plodina	plodina	k1gFnSc1	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Brambory	brambor	k1gInPc1	brambor
jsou	být	k5eAaImIp3nP	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
plodin	plodina	k1gFnPc2	plodina
<g/>
;	;	kIx,	;
větší	veliký	k2eAgInSc4d2	veliký
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
lidskou	lidský	k2eAgFnSc4d1	lidská
výživu	výživa	k1gFnSc4	výživa
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
pšenice	pšenice	k1gFnSc2	pšenice
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnSc2	rýže
a	a	k8xC	a
kukuřice	kukuřice	k1gFnSc1	kukuřice
setá	setý	k2eAgFnSc1d1	setá
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
oblibu	obliba	k1gFnSc4	obliba
vděčí	vděčit	k5eAaImIp3nP	vděčit
nenáročnosti	nenáročnost	k1gFnPc1	nenáročnost
na	na	k7c4	na
přírodní	přírodní	k2eAgFnPc4d1	přírodní
podmínky	podmínka	k1gFnPc4	podmínka
a	a	k8xC	a
především	především	k9	především
pak	pak	k6eAd1	pak
mimořádně	mimořádně	k6eAd1	mimořádně
vysokým	vysoký	k2eAgInPc3d1	vysoký
hektarovým	hektarový	k2eAgInPc3d1	hektarový
výnosům	výnos	k1gInPc3	výnos
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
široká	široký	k2eAgFnSc1d1	široká
adopce	adopce	k1gFnSc1	adopce
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
zemědělství	zemědělství	k1gNnSc6	zemědělství
počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ochránila	ochránit	k5eAaPmAgFnS	ochránit
Evropu	Evropa	k1gFnSc4	Evropa
od	od	k7c2	od
cyklických	cyklický	k2eAgInPc2d1	cyklický
hladomorů	hladomor	k1gInPc2	hladomor
a	a	k8xC	a
"	"	kIx"	"
<g/>
epidemií	epidemie	k1gFnPc2	epidemie
<g/>
"	"	kIx"	"
kurdějí	kurděje	k1gFnPc2	kurděje
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
botanik	botanik	k1gMnSc1	botanik
a	a	k8xC	a
buditel	buditel	k1gMnSc1	buditel
Jan	Jan	k1gMnSc1	Jan
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Presl	Presl	k1gInSc4	Presl
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
Rostlinopise	rostlinopis	k1gInSc6	rostlinopis
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
největší	veliký	k2eAgInSc4d3	veliký
užitek	užitek	k1gInSc4	užitek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
lidstvo	lidstvo	k1gNnSc4	lidstvo
z	z	k7c2	z
objevení	objevení	k1gNnSc2	objevení
Ameriky	Amerika	k1gFnSc2	Amerika
mělo	mít	k5eAaImAgNnS	mít
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
plný	plný	k2eAgInSc4d1	plný
citát	citát	k1gInSc4	citát
v	v	k7c6	v
originále	originál	k1gInSc6	originál
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
a	a	k8xC	a
jistě	jistě	k6eAd1	jistě
nebyl	být	k5eNaImAgInS	být
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
tedy	tedy	k9	tedy
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
jedlých	jedlý	k2eAgFnPc2d1	jedlá
hlíz	hlíza	k1gFnPc2	hlíza
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
rostlina	rostlina	k1gFnSc1	rostlina
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
<g/>
.	.	kIx.	.
</s>
<s>
Solanum	Solanum	k1gNnSc1	Solanum
tuberosum	tuberosum	k1gNnSc1	tuberosum
Linné	Linná	k1gFnSc2	Linná
<g/>
,	,	kIx,	,
1753	[number]	k4	1753
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
tuberosum	tuberosum	k1gInSc1	tuberosum
–	–	k?	–
lilek	lilek	k1gInSc1	lilek
brambor	brambora	k1gFnPc2	brambora
obecný	obecný	k2eAgInSc4d1	obecný
Solanum	Solanum	k1gInSc4	Solanum
tuberosum	tuberosum	k1gNnSc4	tuberosum
Linné	Linná	k1gFnSc2	Linná
<g/>
,	,	kIx,	,
1753	[number]	k4	1753
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
andigenum	andigenum	k1gInSc1	andigenum
(	(	kIx(	(
<g/>
Juzepčuk	Juzepčuk	k1gInSc1	Juzepčuk
&	&	k?	&
Bukasov	Bukasov	k1gInSc1	Bukasov
<g/>
)	)	kIx)	)
J.	J.	kA	J.
G.	G.	kA	G.
Hawkes	Hawkes	k1gMnSc1	Hawkes
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
–	–	k?	–
lilek	lilek	k1gInSc1	lilek
brambor	brambor	k1gInSc1	brambor
andský	andský	k2eAgInSc1d1	andský
<g/>
;	;	kIx,	;
synonyma	synonymum	k1gNnPc1	synonymum
<g/>
:	:	kIx,	:
Solanum	Solanum	k1gInSc1	Solanum
andigenum	andigenum	k1gNnSc1	andigenum
Juzepčuk	Juzepčuk	k1gInSc1	Juzepčuk
&	&	k?	&
<g />
.	.	kIx.	.
</s>
<s>
Bukasov	Bukasov	k1gInSc1	Bukasov
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
Solanum	Solanum	k1gInSc1	Solanum
paramoense	paramoense	k1gFnSc2	paramoense
Bitter	Bittra	k1gFnPc2	Bittra
ex	ex	k6eAd1	ex
Pittier	Pittira	k1gFnPc2	Pittira
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
Solanum	Solanum	k1gInSc1	Solanum
herrerae	herrerae	k1gNnSc2	herrerae
Juzepčuk	Juzepčuka	k1gFnPc2	Juzepčuka
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
Solanum	Solanum	k1gInSc1	Solanum
subandigena	subandigeno	k1gNnSc2	subandigeno
J.	J.	kA	J.
G.	G.	kA	G.
Hawkes	Hawkes	k1gMnSc1	Hawkes
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
Solanum	Solanum	k1gInSc1	Solanum
apurimacense	apurimacense	k1gFnSc2	apurimacense
Vargas	Vargasa	k1gFnPc2	Vargasa
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
Solanum	Solanum	k1gInSc1	Solanum
estradae	estradae	k1gInSc4	estradae
L.	L.	kA	L.
E.	E.	kA	E.
López-Jaramillo	López-Jaramillo	k1gNnSc4	López-Jaramillo
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
Solanum	Solanum	k1gInSc1	Solanum
phureja	phureja	k6eAd1	phureja
Juzepčuk	Juzepčuk	k1gInSc1	Juzepčuk
&	&	k?	&
Bukasov	Bukasov	k1gInSc1	Bukasov
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
subsp	subsp	k1gInSc1	subsp
<g/>
<g />
.	.	kIx.	.
</s>
<s>
estradae	estradae	k1gFnSc1	estradae
(	(	kIx(	(
<g/>
L.	L.	kA	L.
E.	E.	kA	E.
López-Jaramillo	López-Jaramillo	k1gNnSc4	López-Jaramillo
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
J.	J.	kA	J.
G.	G.	kA	G.
Hawkes	Hawkes	k1gMnSc1	Hawkes
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
Brambory	brambora	k1gFnPc1	brambora
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
svoje	svůj	k3xOyFgInPc4	svůj
lidové	lidový	k2eAgInPc4d1	lidový
krajové	krajový	k2eAgInPc4d1	krajový
názvy	název	k1gInPc4	název
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgInPc4	který
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
<g/>
:	:	kIx,	:
zemáky	zemák	k1gInPc1	zemák
zemská	zemský	k2eAgNnPc1d1	zemské
jablka	jablko	k1gNnPc1	jablko
(	(	kIx(	(
<g/>
zemská	zemský	k2eAgFnSc1d1	zemská
japka	japka	k1gFnSc1	japka
<g/>
,	,	kIx,	,
zemní	zemní	k2eAgNnSc1d1	zemní
jablko	jablko	k1gNnSc1	jablko
<g/>
)	)	kIx)	)
erteple	erteple	k1gFnSc1	erteple
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
<g />
.	.	kIx.	.
</s>
<s>
Sudet	Sudety	k1gFnPc2	Sudety
<g/>
)	)	kIx)	)
kobzole	kobzole	k1gFnSc1	kobzole
<g/>
,	,	kIx,	,
kobzol	kobzol	k1gInSc1	kobzol
či	či	k8xC	či
kobzola	kobzola	k1gFnSc1	kobzola
(	(	kIx(	(
<g/>
Lašsko	Lašsko	k1gNnSc1	Lašsko
<g/>
,	,	kIx,	,
Valašsko	Valašsko	k1gNnSc1	Valašsko
<g/>
)	)	kIx)	)
jablóška	jablóška	k1gFnSc1	jablóška
<g/>
,	,	kIx,	,
jablóško	jablóška	k1gFnSc5	jablóška
<g/>
,	,	kIx,	,
jabluško	jabluška	k1gFnSc5	jabluška
zemčátka	zemčátko	k1gNnPc1	zemčátko
(	(	kIx(	(
<g/>
Magdalena	Magdalena	k1gFnSc1	Magdalena
Dobromila	Dobromila	k1gFnSc1	Dobromila
Rettigová	Rettigový	k2eAgFnSc1d1	Rettigová
<g/>
)	)	kIx)	)
krumple	krumple	k6eAd1	krumple
(	(	kIx(	(
<g/>
Drahansko	Drahansko	k1gNnSc1	Drahansko
<g/>
)	)	kIx)	)
grule	grule	k1gFnSc1	grule
bandory	bandor	k1gMnPc4	bandor
arteple	arteple	k6eAd1	arteple
(	(	kIx(	(
<g/>
Kyjovsko	Kyjovsko	k1gNnSc1	Kyjovsko
<g/>
)	)	kIx)	)
zemák	zemák	k1gInSc1	zemák
(	(	kIx(	(
<g/>
Valašsko	Valašsko	k1gNnSc1	Valašsko
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Haná	Haná	k1gFnSc1	Haná
<g/>
)	)	kIx)	)
źymńok	źymńok	k1gInSc1	źymńok
(	(	kIx(	(
<g/>
v	v	k7c6	v
lašských	lašský	k2eAgInPc6d1	lašský
dialektech	dialekt	k1gInPc6	dialekt
<g/>
)	)	kIx)	)
grumbíry	grumbír	k1gInPc1	grumbír
(	(	kIx(	(
<g/>
moravské	moravský	k2eAgNnSc1d1	Moravské
slovácko	slovácko	k1gNnSc1	slovácko
<g/>
)	)	kIx)	)
okroše	okroš	k1gMnSc2	okroš
(	(	kIx(	(
<g/>
úzká	úzký	k2eAgFnSc1d1	úzká
oblast	oblast	k1gFnSc1	oblast
západní	západní	k2eAgFnSc2d1	západní
Moravy	Morava	k1gFnSc2	Morava
<g/>
)	)	kIx)	)
Bylina	bylina	k1gFnSc1	bylina
s	s	k7c7	s
hranatou	hranatý	k2eAgFnSc7d1	hranatá
<g/>
,	,	kIx,	,
bohatě	bohatě	k6eAd1	bohatě
rozvětvenou	rozvětvený	k2eAgFnSc7d1	rozvětvená
lodyhou	lodyha	k1gFnSc7	lodyha
<g/>
,	,	kIx,	,
přímou	přímý	k2eAgFnSc4d1	přímá
nebo	nebo	k8xC	nebo
i	i	k9	i
poléhavou	poléhavý	k2eAgFnSc4d1	poléhavá
<g/>
,	,	kIx,	,
s	s	k7c7	s
krátkými	krátký	k2eAgInPc7d1	krátký
chloupky	chloupek	k1gInPc7	chloupek
<g/>
.	.	kIx.	.
</s>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
výšky	výška	k1gFnSc2	výška
60	[number]	k4	60
až	až	k9	až
100	[number]	k4	100
cm	cm	kA	cm
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
až	až	k9	až
1,5	[number]	k4	1,5
m.	m.	k?	m.
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
lichozpeřené	lichozpeřený	k2eAgInPc1d1	lichozpeřený
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
ochlupené	ochlupený	k2eAgFnPc1d1	ochlupená
<g/>
,	,	kIx,	,
s	s	k7c7	s
drobnými	drobný	k2eAgFnPc7d1	drobná
žlázkami	žlázka	k1gFnPc7	žlázka
<g/>
,	,	kIx,	,
řapíkaté	řapíkatý	k2eAgFnPc1d1	řapíkatá
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgInPc1d1	velký
<g/>
,	,	kIx,	,
30	[number]	k4	30
až	až	k9	až
50	[number]	k4	50
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
bílé	bílý	k2eAgInPc1d1	bílý
<g/>
,	,	kIx,	,
růžové	růžový	k2eAgInPc1d1	růžový
nebo	nebo	k8xC	nebo
fialové	fialový	k2eAgInPc1d1	fialový
se	se	k3xPyFc4	se
sytě	sytě	k6eAd1	sytě
žlutými	žlutý	k2eAgInPc7d1	žlutý
až	až	k8xS	až
oranžovými	oranžový	k2eAgInPc7d1	oranžový
prašníky	prašník	k1gInPc7	prašník
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
zelené	zelený	k2eAgFnPc1d1	zelená
nebo	nebo	k8xC	nebo
žlutozelené	žlutozelený	k2eAgFnPc1d1	žlutozelená
bobule	bobule	k1gFnPc1	bobule
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
2	[number]	k4	2
až	až	k9	až
4	[number]	k4	4
cm	cm	kA	cm
obsahující	obsahující	k2eAgFnPc4d1	obsahující
bílá	bílý	k2eAgNnPc4d1	bílé
semena	semeno	k1gNnPc4	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Podzemní	podzemní	k2eAgFnSc1d1	podzemní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
svazčitými	svazčitý	k2eAgInPc7d1	svazčitý
kořeny	kořen	k1gInPc7	kořen
s	s	k7c7	s
hlízami	hlíza	k1gFnPc7	hlíza
rozmanitých	rozmanitý	k2eAgMnPc2d1	rozmanitý
elipsoidních	elipsoidní	k2eAgMnPc2d1	elipsoidní
až	až	k8xS	až
nepravidelných	pravidelný	k2eNgMnPc2d1	nepravidelný
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
s	s	k7c7	s
okrově	okrově	k6eAd1	okrově
žlutou	žlutý	k2eAgFnSc7d1	žlutá
až	až	k8xS	až
světle	světle	k6eAd1	světle
hnědou	hnědý	k2eAgFnSc4d1	hnědá
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
kultivarů	kultivar	k1gInPc2	kultivar
červenou	červený	k2eAgFnSc4d1	červená
až	až	k8xS	až
červenofialovou	červenofialový	k2eAgFnSc7d1	červenofialová
pokožkou	pokožka	k1gFnSc7	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Lilek	lilek	k1gInSc1	lilek
brambor	brambora	k1gFnPc2	brambora
je	být	k5eAaImIp3nS	být
kulturní	kulturní	k2eAgFnSc1d1	kulturní
rostlina	rostlina	k1gFnSc1	rostlina
s	s	k7c7	s
tetraploidním	tetraploidní	k2eAgInSc7d1	tetraploidní
genomem	genom	k1gInSc7	genom
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
n	n	k0	n
=	=	kIx~	=
48	[number]	k4	48
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brambory	brambora	k1gFnPc1	brambora
tak	tak	k9	tak
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
lokusu	lokus	k1gInSc6	lokus
4	[number]	k4	4
nezávislé	závislý	k2eNgInPc4d1	nezávislý
geny	gen	k1gInPc4	gen
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
kulturních	kulturní	k2eAgFnPc2d1	kulturní
plodin	plodina	k1gFnPc2	plodina
má	mít	k5eAaImIp3nS	mít
brambor	brambor	k1gInSc4	brambor
nejbohatší	bohatý	k2eAgInPc4d3	nejbohatší
genetické	genetický	k2eAgInPc4d1	genetický
zdroje	zdroj	k1gInPc4	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Brambor	brambora	k1gFnPc2	brambora
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgNnPc4	dva
centra	centrum	k1gNnPc4	centrum
biodiverzity	biodiverzita	k1gFnSc2	biodiverzita
<g/>
:	:	kIx,	:
tzv.	tzv.	kA	tzv.
andské	andský	k2eAgNnSc1d1	Andské
centrum	centrum	k1gNnSc1	centrum
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jezera	jezero	k1gNnSc2	jezero
Titicaca	Titicac	k1gInSc2	Titicac
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rostou	růst	k5eAaImIp3nP	růst
kultivary	kultivar	k1gInPc4	kultivar
adaptované	adaptovaný	k2eAgInPc4d1	adaptovaný
na	na	k7c4	na
podmínky	podmínka	k1gFnPc4	podmínka
krátkého	krátký	k2eAgInSc2d1	krátký
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
chilské	chilský	k2eAgNnSc4d1	Chilské
centrum	centrum	k1gNnSc4	centrum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
okolo	okolo	k7c2	okolo
40	[number]	k4	40
<g/>
°	°	k?	°
j.	j.	k?	j.
š.	š.	k?	š.
<g/>
,	,	kIx,	,
s	s	k7c7	s
adaptací	adaptace	k1gFnSc7	adaptace
na	na	k7c4	na
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
chilského	chilský	k2eAgNnSc2d1	Chilské
centra	centrum	k1gNnSc2	centrum
patrně	patrně	k6eAd1	patrně
pochází	pocházet	k5eAaImIp3nP	pocházet
předchůdci	předchůdce	k1gMnPc1	předchůdce
evropských	evropský	k2eAgFnPc2d1	Evropská
kulturních	kulturní	k2eAgFnPc2d1	kulturní
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
mnohé	mnohý	k2eAgFnPc4d1	mnohá
lokální	lokální	k2eAgFnPc4d1	lokální
kulturní	kulturní	k2eAgFnPc4d1	kulturní
a	a	k8xC	a
polokulturní	polokulturní	k2eAgFnPc4d1	polokulturní
odrůdy	odrůda	k1gFnPc4	odrůda
jakož	jakož	k8xC	jakož
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
divokých	divoký	k2eAgMnPc2d1	divoký
příbuzných	příbuzný	k1gMnPc2	příbuzný
s	s	k7c7	s
různým	různý	k2eAgInSc7d1	různý
stupněm	stupeň	k1gInSc7	stupeň
ploidie	ploidie	k1gFnSc2	ploidie
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
hexaploidní	hexaploidní	k2eAgFnSc2d1	hexaploidní
odrůdy	odrůda	k1gFnSc2	odrůda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
divokých	divoký	k2eAgFnPc2d1	divoká
příbuzných	příbuzná	k1gFnPc2	příbuzná
lze	lze	k6eAd1	lze
s	s	k7c7	s
bramborem	brambor	k1gInSc7	brambor
křížit	křížit	k5eAaImF	křížit
a	a	k8xC	a
tak	tak	k6eAd1	tak
získávat	získávat	k5eAaImF	získávat
požadované	požadovaný	k2eAgFnPc4d1	požadovaná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
(	(	kIx(	(
<g/>
ranost	ranost	k1gFnSc1	ranost
<g/>
,	,	kIx,	,
odolnost	odolnost	k1gFnSc1	odolnost
k	k	k7c3	k
chorobám	choroba	k1gFnPc3	choroba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brambory	brambora	k1gFnPc1	brambora
se	se	k3xPyFc4	se
komerčně	komerčně	k6eAd1	komerčně
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
vegetativně	vegetativně	k6eAd1	vegetativně
z	z	k7c2	z
hlíz	hlíza	k1gFnPc2	hlíza
<g/>
,	,	kIx,	,
pravé	pravý	k2eAgNnSc4d1	pravé
semeno	semeno	k1gNnSc4	semeno
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
šlechtitelské	šlechtitelský	k2eAgInPc4d1	šlechtitelský
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mnohých	mnohý	k2eAgFnPc2d1	mnohá
kulturních	kulturní	k2eAgFnPc2d1	kulturní
odrůd	odrůda	k1gFnPc2	odrůda
však	však	k9	však
pylová	pylový	k2eAgFnSc1d1	pylová
sterilita	sterilita	k1gFnSc1	sterilita
představuje	představovat	k5eAaImIp3nS	představovat
závažný	závažný	k2eAgInSc4d1	závažný
problém	problém	k1gInSc4	problém
pro	pro	k7c4	pro
šlechtění	šlechtění	k1gNnSc4	šlechtění
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc7d1	původní
oblastí	oblast	k1gFnSc7	oblast
výskytu	výskyt	k1gInSc2	výskyt
jsou	být	k5eAaImIp3nP	být
podhorské	podhorský	k2eAgFnPc1d1	podhorská
a	a	k8xC	a
horské	horský	k2eAgFnPc1d1	horská
oblasti	oblast	k1gFnPc1	oblast
And	Anda	k1gFnPc2	Anda
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Peru	Peru	k1gNnSc6	Peru
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
jako	jako	k8xS	jako
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
plodina	plodina	k1gFnSc1	plodina
prakticky	prakticky	k6eAd1	prakticky
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
tropů	trop	k1gInPc2	trop
<g/>
,	,	kIx,	,
arktických	arktický	k2eAgFnPc2d1	arktická
a	a	k8xC	a
subarktických	subarktický	k2eAgFnPc2d1	subarktická
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
podle	podle	k7c2	podle
moderních	moderní	k2eAgFnPc2d1	moderní
molekulárních	molekulární	k2eAgFnPc2d1	molekulární
metod	metoda	k1gFnPc2	metoda
usuzujeme	usuzovat	k5eAaImIp1nP	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
brambory	brambora	k1gFnPc1	brambora
byly	být	k5eAaImAgFnP	být
domestikovány	domestikovat	k5eAaBmNgFnP	domestikovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Peru	Peru	k1gNnSc2	Peru
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
4	[number]	k4	4
až	až	k8xS	až
5	[number]	k4	5
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nedařilo	dařit	k5eNaImAgNnS	dařit
kukuřici	kukuřice	k1gFnSc3	kukuřice
byla	být	k5eAaImAgFnS	být
domestikace	domestikace	k1gFnSc1	domestikace
brambor	brambor	k1gInSc4	brambor
podmínkou	podmínka	k1gFnSc7	podmínka
vzniku	vznik	k1gInSc2	vznik
vyspělejší	vyspělý	k2eAgFnSc2d2	vyspělejší
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Inkové	Ink	k1gMnPc1	Ink
nazývali	nazývat	k5eAaImAgMnP	nazývat
tyto	tento	k3xDgFnPc4	tento
odolné	odolný	k2eAgFnPc4d1	odolná
hlízy	hlíza	k1gFnPc4	hlíza
"	"	kIx"	"
<g/>
papa	papa	k1gMnSc1	papa
<g/>
"	"	kIx"	"
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
pojmenování	pojmenování	k1gNnSc1	pojmenování
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
bramborám	brambora	k1gFnPc3	brambora
v	v	k7c6	v
latinskoamerické	latinskoamerický	k2eAgFnSc6d1	latinskoamerická
španělštině	španělština	k1gFnSc6	španělština
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Horské	Horské	k2eAgFnSc3d1	Horské
oblasti	oblast	k1gFnSc3	oblast
Peru	Peru	k1gNnSc2	Peru
<g/>
,	,	kIx,	,
Bolívie	Bolívie	k1gFnSc2	Bolívie
a	a	k8xC	a
Chile	Chile	k1gNnSc2	Chile
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
centrem	centrum	k1gNnSc7	centrum
biodiverzity	biodiverzita	k1gFnSc2	biodiverzita
brambor	brambora	k1gFnPc2	brambora
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
lokálních	lokální	k2eAgFnPc2d1	lokální
odrůd	odrůda	k1gFnPc2	odrůda
a	a	k8xC	a
divokých	divoký	k2eAgMnPc2d1	divoký
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Brambory	brambor	k1gInPc1	brambor
představovaly	představovat	k5eAaImAgInP	představovat
pro	pro	k7c4	pro
inckou	incký	k2eAgFnSc4d1	incká
říši	říše	k1gFnSc4	říše
podobný	podobný	k2eAgInSc4d1	podobný
dar	dar	k1gInSc4	dar
nebes	nebesa	k1gNnPc2	nebesa
jako	jako	k9	jako
pro	pro	k7c4	pro
říši	říše	k1gFnSc4	říše
Aztéků	Azték	k1gMnPc2	Azték
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
.	.	kIx.	.
</s>
<s>
Brambory	brambora	k1gFnPc1	brambora
byly	být	k5eAaImAgFnP	být
buď	buď	k8xC	buď
konzumovány	konzumovat	k5eAaBmNgInP	konzumovat
přímo	přímo	k6eAd1	přímo
či	či	k8xC	či
uchovávány	uchovávat	k5eAaImNgInP	uchovávat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
sušeného	sušený	k2eAgInSc2d1	sušený
prášku	prášek	k1gInSc2	prášek
(	(	kIx(	(
<g/>
chuñ	chuñ	k?	chuñ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Inkové	Ink	k1gMnPc1	Ink
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
rovněž	rovněž	k9	rovněž
připravovali	připravovat	k5eAaImAgMnP	připravovat
alkoholický	alkoholický	k2eAgInSc4d1	alkoholický
nápoj	nápoj	k1gInSc4	nápoj
chacha	chach	k1gMnSc2	chach
podobný	podobný	k2eAgInSc1d1	podobný
pivu	pivo	k1gNnSc3	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
i	i	k9	i
pro	pro	k7c4	pro
medicínské	medicínský	k2eAgInPc4d1	medicínský
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
důležitost	důležitost	k1gFnSc1	důležitost
podtrhuje	podtrhovat	k5eAaImIp3nS	podtrhovat
i	i	k9	i
několik	několik	k4yIc4	několik
bramborových	bramborový	k2eAgNnPc2d1	bramborové
božstev	božstvo	k1gNnPc2	božstvo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
incké	incký	k2eAgFnSc2d1	incká
říše	říš	k1gFnSc2	říš
Španěly	Španěl	k1gMnPc4	Španěl
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
putovaly	putovat	k5eAaImAgFnP	putovat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
kromě	kromě	k7c2	kromě
mnoha	mnoho	k4c2	mnoho
tun	tuna	k1gFnPc2	tuna
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
stříbra	stříbro	k1gNnSc2	stříbro
i	i	k8xC	i
některé	některý	k3yIgFnPc1	některý
exotické	exotický	k2eAgFnPc1d1	exotická
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
i	i	k9	i
brambory	brambora	k1gFnPc4	brambora
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1565	[number]	k4	1565
dostal	dostat	k5eAaPmAgMnS	dostat
první	první	k4xOgFnSc4	první
větší	veliký	k2eAgFnSc4d2	veliký
zásilku	zásilka	k1gFnSc4	zásilka
brambor	brambora	k1gFnPc2	brambora
z	z	k7c2	z
Cuzca	Cuzc	k1gInSc2	Cuzc
jako	jako	k8xC	jako
dar	dar	k1gInSc1	dar
španělský	španělský	k2eAgMnSc1d1	španělský
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
začali	začít	k5eAaPmAgMnP	začít
španělští	španělský	k2eAgMnPc1d1	španělský
námořníci	námořník	k1gMnPc1	námořník
používat	používat	k5eAaImF	používat
brambory	brambor	k1gInPc4	brambor
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgFnSc4d1	hlavní
potravinu	potravina	k1gFnSc4	potravina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jim	on	k3xPp3gMnPc3	on
mimoděk	mimoděk	k6eAd1	mimoděk
pomáhalo	pomáhat	k5eAaImAgNnS	pomáhat
jako	jako	k9	jako
prevence	prevence	k1gFnPc1	prevence
proti	proti	k7c3	proti
kurdějím	kurděje	k1gFnPc3	kurděje
<g/>
.	.	kIx.	.
</s>
<s>
Nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
španělských	španělský	k2eAgMnPc6d1	španělský
dobyvatelích	dobyvatel	k1gMnPc6	dobyvatel
se	se	k3xPyFc4	se
brambory	brambora	k1gFnPc1	brambora
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
slavné	slavný	k2eAgFnSc2d1	slavná
Golden	Goldno	k1gNnPc2	Goldno
Hind	hind	k1gMnSc1	hind
Francise	Francise	k1gFnSc2	Francise
Drakea	Drakea	k1gMnSc1	Drakea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
podobné	podobný	k2eAgFnPc4d1	podobná
přírodní	přírodní	k2eAgFnPc4d1	přírodní
podmínky	podmínka	k1gFnPc4	podmínka
jako	jako	k8xC	jako
horské	horský	k2eAgFnPc4d1	horská
oblasti	oblast	k1gFnPc4	oblast
Peru	Peru	k1gNnSc2	Peru
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
brambory	brambora	k1gFnPc1	brambora
začaly	začít	k5eAaPmAgFnP	začít
běžně	běžně	k6eAd1	běžně
pěstovat	pěstovat	k5eAaImF	pěstovat
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Angličtí	anglický	k2eAgMnPc1d1	anglický
a	a	k8xC	a
irští	irský	k2eAgMnPc1d1	irský
kolonisté	kolonista	k1gMnPc1	kolonista
je	on	k3xPp3gInPc4	on
pak	pak	k6eAd1	pak
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přivezli	přivézt	k5eAaPmAgMnP	přivézt
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Brambory	brambora	k1gFnPc1	brambora
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
Evropě	Evropa	k1gFnSc6	Evropa
zpočátku	zpočátku	k6eAd1	zpočátku
přijímány	přijímat	k5eAaImNgInP	přijímat
se	s	k7c7	s
značnou	značný	k2eAgFnSc7d1	značná
nedůvěrou	nedůvěra	k1gFnSc7	nedůvěra
a	a	k8xC	a
obavami	obava	k1gFnPc7	obava
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
pohanskou	pohanský	k2eAgFnSc4d1	pohanská
a	a	k8xC	a
nekřesťanskou	křesťanský	k2eNgFnSc4d1	nekřesťanská
plodinu	plodina	k1gFnSc4	plodina
<g/>
,	,	kIx,	,
za	za	k7c4	za
plodinu	plodina	k1gFnSc4	plodina
nečistou	čistý	k2eNgFnSc4d1	nečistá
a	a	k8xC	a
zdraví	zdraví	k1gNnSc6	zdraví
ohrožující	ohrožující	k2eAgFnSc4d1	ohrožující
<g/>
.	.	kIx.	.
</s>
<s>
Případně	případně	k6eAd1	případně
byly	být	k5eAaImAgInP	být
používány	používat	k5eAaImNgInP	používat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
okrasná	okrasný	k2eAgFnSc1d1	okrasná
exotická	exotický	k2eAgFnSc1d1	exotická
rostlina	rostlina	k1gFnSc1	rostlina
na	na	k7c6	na
dvorech	dvůr	k1gInPc6	dvůr
velmožů	velmož	k1gMnPc2	velmož
a	a	k8xC	a
v	v	k7c6	v
klášterních	klášterní	k2eAgFnPc6d1	klášterní
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
lékaři	lékař	k1gMnPc1	lékař
je	on	k3xPp3gMnPc4	on
předepisovali	předepisovat	k5eAaImAgMnP	předepisovat
jako	jako	k8xS	jako
zaručený	zaručený	k2eAgInSc1d1	zaručený
lék	lék	k1gInSc1	lék
proti	proti	k7c3	proti
široké	široký	k2eAgFnSc3d1	široká
škále	škála	k1gFnSc3	škála
onemocnění	onemocnění	k1gNnSc2	onemocnění
od	od	k7c2	od
průjmů	průjem	k1gInPc2	průjem
po	po	k7c4	po
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
jako	jako	k9	jako
afrodisiaka	afrodisiakum	k1gNnSc2	afrodisiakum
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nedůvěra	nedůvěra	k1gFnSc1	nedůvěra
trvala	trvat	k5eAaImAgFnS	trvat
téměř	téměř	k6eAd1	téměř
dvě	dva	k4xCgNnPc4	dva
staletí	staletí	k1gNnPc4	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1740	[number]	k4	1740
rozpoznal	rozpoznat	k5eAaPmAgInS	rozpoznat
význam	význam	k1gInSc1	význam
brambor	brambora	k1gFnPc2	brambora
pruský	pruský	k2eAgMnSc1d1	pruský
král	král	k1gMnSc1	král
Bedřich	Bedřich	k1gMnSc1	Bedřich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Veliký	veliký	k2eAgMnSc1d1	veliký
a	a	k8xC	a
nařídil	nařídit	k5eAaPmAgMnS	nařídit
jejich	jejich	k3xOp3gNnSc4	jejich
pěstování	pěstování	k1gNnSc4	pěstování
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Prusku	Prusko	k1gNnSc6	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
přišly	přijít	k5eAaPmAgFnP	přijít
brambory	brambora	k1gFnPc1	brambora
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gFnPc4	jejich
hlízy	hlíza	k1gFnPc4	hlíza
<g/>
)	)	kIx)	)
z	z	k7c2	z
Braniborska	Braniborsko	k1gNnSc2	Braniborsko
(	(	kIx(	(
<g/>
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1778	[number]	k4	1778
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zkomolením	zkomolení	k1gNnSc7	zkomolení
jejich	jejich	k3xOp3gFnSc2	jejich
dnešní	dnešní	k2eAgFnSc2d1	dnešní
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiného	jiný	k2eAgInSc2d1	jiný
názoru	názor	k1gInSc2	názor
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
název	název	k1gInSc1	název
brambory	brambora	k1gFnSc2	brambora
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
bamboly	bambola	k1gFnSc2	bambola
–	–	k?	–
starého	starý	k2eAgNnSc2d1	staré
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
hlízy	hlíza	k1gFnPc4	hlíza
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
v	v	k7c6	v
cizích	cizí	k2eAgNnPc6d1	cizí
nářečích	nářečí	k1gNnPc6	nářečí
označuje	označovat	k5eAaImIp3nS	označovat
většinou	většinou	k6eAd1	většinou
původ	původ	k1gInSc1	původ
(	(	kIx(	(
<g/>
szwaby	szwaba	k1gFnPc1	szwaba
–	–	k?	–
Švábsko	Švábsko	k1gNnSc1	Švábsko
<g/>
,	,	kIx,	,
burgonya	burgonya	k1gFnSc1	burgonya
–	–	k?	–
Burgundsko	Burgundsko	k1gNnSc1	Burgundsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kulatý	kulatý	k2eAgInSc1d1	kulatý
tvar	tvar	k1gInSc1	tvar
(	(	kIx(	(
<g/>
bobál	bobál	k1gInSc1	bobál
<g/>
,	,	kIx,	,
bumbulis	bumbulis	k1gInSc1	bumbulis
<g/>
)	)	kIx)	)
či	či	k8xC	či
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
zemák	zemák	k1gInSc1	zemák
<g/>
,	,	kIx,	,
zemče	zemče	k1gNnSc1	zemče
<g/>
,	,	kIx,	,
zemky	zemek	k1gMnPc4	zemek
<g/>
,	,	kIx,	,
zemské	zemský	k2eAgNnSc1d1	zemské
jablko	jablko	k1gNnSc1	jablko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
bramborech	brambor	k1gInPc6	brambor
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1623	[number]	k4	1623
a	a	k8xC	a
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
jejich	jejich	k3xOp3gNnSc6	jejich
podávání	podávání	k1gNnSc6	podávání
na	na	k7c6	na
stole	stol	k1gInSc6	stol
Viléma	Vilém	k1gMnSc2	Vilém
Slavaty	Slavata	k1gFnSc2	Slavata
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
měřítku	měřítko	k1gNnSc6	měřítko
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
pěstovat	pěstovat	k5eAaImF	pěstovat
a	a	k8xC	a
používat	používat	k5eAaImF	používat
jako	jako	k9	jako
potraviny	potravina	k1gFnPc4	potravina
teprve	teprve	k6eAd1	teprve
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
pruskými	pruský	k2eAgFnPc7d1	pruská
válkami	válka	k1gFnPc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgInPc1d1	běžný
konzumní	konzumní	k2eAgInPc1d1	konzumní
brambory	brambor	k1gInPc1	brambor
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
přibližně	přibližně	k6eAd1	přibližně
24	[number]	k4	24
%	%	kIx~	%
sušiny	sušina	k1gFnSc2	sušina
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
zhruba	zhruba	k6eAd1	zhruba
75	[number]	k4	75
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nS	tvořit
škrob	škrob	k1gMnSc1	škrob
(	(	kIx(	(
<g/>
amylosa	amylosa	k1gFnSc1	amylosa
a	a	k8xC	a
amylopektin	amylopektin	k1gInSc1	amylopektin
cca	cca	kA	cca
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
a	a	k8xC	a
okolo	okolo	k7c2	okolo
2	[number]	k4	2
%	%	kIx~	%
rozpustné	rozpustný	k2eAgInPc1d1	rozpustný
cukry	cukr	k1gInPc1	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Bílkoviny	bílkovina	k1gFnPc1	bílkovina
tvoří	tvořit	k5eAaImIp3nP	tvořit
okolo	okolo	k7c2	okolo
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
%	%	kIx~	%
sušiny	sušina	k1gFnPc4	sušina
a	a	k8xC	a
tuky	tuk	k1gInPc4	tuk
okolo	okolo	k7c2	okolo
0,4	[number]	k4	0,4
%	%	kIx~	%
sušiny	sušina	k1gFnSc2	sušina
<g/>
.	.	kIx.	.
</s>
<s>
Hlízy	hlíza	k1gFnPc1	hlíza
dále	daleko	k6eAd2	daleko
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
významná	významný	k2eAgNnPc4d1	významné
množství	množství	k1gNnPc4	množství
kyseliny	kyselina	k1gFnSc2	kyselina
citrónové	citrónový	k2eAgFnPc4d1	citrónová
<g/>
,	,	kIx,	,
polyfenolů	polyfenol	k1gInPc2	polyfenol
<g/>
,	,	kIx,	,
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
Mg	mg	kA	mg
<g/>
,	,	kIx,	,
Fe	Fe	k1gFnSc1	Fe
<g/>
,	,	kIx,	,
Zn	zn	kA	zn
<g/>
,	,	kIx,	,
Cu	Cu	k1gMnSc1	Cu
<g/>
,	,	kIx,	,
Mn	Mn	k1gMnSc1	Mn
<g/>
,	,	kIx,	,
P	P	kA	P
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
,	,	kIx,	,
Ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
Ca	ca	kA	ca
<g/>
,	,	kIx,	,
K	K	kA	K
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
a	a	k8xC	a
vitamínů	vitamín	k1gInPc2	vitamín
C	C	kA	C
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
B2	B2	k1gFnSc1	B2
a	a	k8xC	a
PP	PP	kA	PP
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
vitamínu	vitamín	k1gInSc2	vitamín
C	C	kA	C
kolísá	kolísat	k5eAaImIp3nS	kolísat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
době	doba	k1gFnSc6	doba
a	a	k8xC	a
způsobu	způsob	k1gInSc6	způsob
uskladnění	uskladnění	k1gNnSc2	uskladnění
brambor	brambora	k1gFnPc2	brambora
a	a	k8xC	a
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
9	[number]	k4	9
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
g.	g.	k?	g.
Právě	právě	k9	právě
obsah	obsah	k1gInSc4	obsah
vitamínu	vitamín	k1gInSc2	vitamín
C	C	kA	C
a	a	k8xC	a
schopnost	schopnost	k1gFnSc1	schopnost
brambor	brambora	k1gFnPc2	brambora
uchovávat	uchovávat	k5eAaImF	uchovávat
tento	tento	k3xDgInSc4	tento
vitamín	vitamín	k1gInSc4	vitamín
činí	činit	k5eAaImIp3nS	činit
z	z	k7c2	z
brambor	brambora	k1gFnPc2	brambora
tzv.	tzv.	kA	tzv.
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
potravinu	potravina	k1gFnSc4	potravina
proti	proti	k7c3	proti
kurdějím	kurděje	k1gFnPc3	kurděje
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výživě	výživa	k1gFnSc6	výživa
brambory	brambora	k1gFnSc2	brambora
plní	plnit	k5eAaImIp3nS	plnit
dále	daleko	k6eAd2	daleko
funkci	funkce	k1gFnSc4	funkce
objemovou	objemový	k2eAgFnSc4d1	objemová
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
dostatečně	dostatečně	k6eAd1	dostatečně
zatěžují	zatěžovat	k5eAaImIp3nP	zatěžovat
trávicí	trávicí	k2eAgFnSc4d1	trávicí
soustavu	soustava	k1gFnSc4	soustava
a	a	k8xC	a
sytící	sytící	k2eAgMnSc1d1	sytící
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
dostatek	dostatek	k1gInSc4	dostatek
energie	energie	k1gFnSc2	energie
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
jsou	být	k5eAaImIp3nP	být
brambory	brambora	k1gFnPc1	brambora
doporučovány	doporučovat	k5eAaImNgFnP	doporučovat
jako	jako	k8xS	jako
dietní	dietní	k2eAgFnSc1d1	dietní
strava	strava	k1gFnSc1	strava
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
sušiny	sušina	k1gFnSc2	sušina
než	než	k8xS	než
obiloviny	obilovina	k1gFnSc2	obilovina
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
i	i	k9	i
menší	malý	k2eAgNnSc1d2	menší
množství	množství	k1gNnSc1	množství
využitelné	využitelný	k2eAgFnSc2d1	využitelná
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nutričních	nutriční	k2eAgInPc2d1	nutriční
parametrů	parametr	k1gInPc2	parametr
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
v	v	k7c6	v
bramborách	brambora	k1gFnPc6	brambora
<g/>
.	.	kIx.	.
</s>
<s>
Brambory	brambora	k1gFnPc1	brambora
také	také	k9	také
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vitamin	vitamin	k1gInSc4	vitamin
K	k	k7c3	k
1	[number]	k4	1
ug	ug	k?	ug
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Brambory	brambora	k1gFnPc1	brambora
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
toxické	toxický	k2eAgFnPc1d1	toxická
glykoalkaloidy	glykoalkaloida	k1gFnPc1	glykoalkaloida
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
solanin	solanin	k1gInSc1	solanin
a	a	k8xC	a
chakonin	chakonin	k1gInSc1	chakonin
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
nad	nad	k7c7	nad
170	[number]	k4	170
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
látky	látka	k1gFnPc1	látka
částečně	částečně	k6eAd1	částečně
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
nadzemních	nadzemní	k2eAgFnPc6d1	nadzemní
částech	část	k1gFnPc6	část
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
květech	květ	k1gInPc6	květ
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
hlízy	hlíza	k1gFnPc4	hlíza
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
koncentrace	koncentrace	k1gFnPc4	koncentrace
alkaloidů	alkaloid	k1gInPc2	alkaloid
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
slupkou	slupka	k1gFnSc7	slupka
a	a	k8xC	a
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
se	se	k3xPyFc4	se
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
brambory	brambora	k1gFnPc1	brambora
na	na	k7c6	na
světle	světlo	k1gNnSc6	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Brambory	brambora	k1gFnPc1	brambora
na	na	k7c6	na
světle	světlo	k1gNnSc6	světlo
rovněž	rovněž	k9	rovněž
zelenají	zelenat	k5eAaImIp3nP	zelenat
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc1	obsah
alkaloidů	alkaloid	k1gInPc2	alkaloid
však	však	k9	však
nemusí	muset	k5eNaImIp3nS	muset
s	s	k7c7	s
barvou	barva	k1gFnSc7	barva
přímo	přímo	k6eAd1	přímo
korelovat	korelovat	k5eAaImF	korelovat
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
obsah	obsah	k1gInSc1	obsah
alkaloidů	alkaloid	k1gInPc2	alkaloid
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
v	v	k7c4	v
okolí	okolí	k1gNnSc4	okolí
oček	očko	k1gNnPc2	očko
(	(	kIx(	(
<g/>
pupeny	pupen	k1gInPc7	pupen
na	na	k7c6	na
hlíze	hlíza	k1gFnSc6	hlíza
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
poranění	poranění	k1gNnSc2	poranění
hlízy	hlíza	k1gFnSc2	hlíza
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
předávkování	předávkování	k1gNnSc6	předávkování
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
i	i	k9	i
ke	k	k7c3	k
smrtelné	smrtelný	k2eAgFnSc3d1	smrtelná
otravě	otrava	k1gFnSc3	otrava
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
otravy	otrava	k1gFnPc1	otrava
bramborami	brambora	k1gFnPc7	brambora
jsou	být	k5eAaImIp3nP	být
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
o	o	k7c4	o
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dítě	dítě	k1gNnSc1	dítě
snědlo	sníst	k5eAaPmAgNnS	sníst
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
plodů	plod	k1gInPc2	plod
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
hlíz	hlíza	k1gFnPc2	hlíza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
nechutnosti	nechutnost	k1gFnSc3	nechutnost
a	a	k8xC	a
nevelkému	velký	k2eNgInSc3d1	nevelký
počtu	počet	k1gInSc3	počet
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
nepravděpodobnou	pravděpodobný	k2eNgFnSc4d1	nepravděpodobná
událost	událost	k1gFnSc4	událost
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
alkaloidů	alkaloid	k1gInPc2	alkaloid
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
sleduje	sledovat	k5eAaImIp3nS	sledovat
během	během	k7c2	během
šlechtění	šlechtění	k1gNnSc2	šlechtění
<g/>
.	.	kIx.	.
</s>
<s>
Šlechtitelé	šlechtitel	k1gMnPc1	šlechtitel
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
nepřekročit	překročit	k5eNaPmF	překročit
koncentraci	koncentrace	k1gFnSc4	koncentrace
solaninu	solanin	k1gInSc2	solanin
0,2	[number]	k4	0,2
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
g.	g.	k?	g.
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
u	u	k7c2	u
moderních	moderní	k2eAgFnPc2d1	moderní
odrůd	odrůda	k1gFnPc2	odrůda
s	s	k7c7	s
koncentrací	koncentrace	k1gFnSc7	koncentrace
solaninu	solanin	k1gInSc2	solanin
pod	pod	k7c4	pod
0,2	[number]	k4	0,2
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
g	g	kA	g
může	moct	k5eAaImIp3nS	moct
po	po	k7c6	po
osvětlení	osvětlení	k1gNnSc6	osvětlení
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
až	až	k6eAd1	až
nad	nad	k7c7	nad
1	[number]	k4	1
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
g	g	kA	g
solaninu	solanin	k1gInSc2	solanin
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
běžně	běžně	k6eAd1	běžně
udávané	udávaný	k2eAgFnSc6d1	udávaná
nebezpečné	bezpečný	k2eNgFnSc6d1	nebezpečná
dávce	dávka	k1gFnSc6	dávka
200	[number]	k4	200
mg	mg	kA	mg
solaninu	solanin	k1gInSc2	solanin
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dospělý	dospělý	k2eAgMnSc1d1	dospělý
člověk	člověk	k1gMnSc1	člověk
může	moct	k5eAaImIp3nS	moct
pozřít	pozřít	k5eAaPmF	pozřít
tuto	tento	k3xDgFnSc4	tento
zdraví	zdravit	k5eAaImIp3nS	zdravit
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
dávku	dávka	k1gFnSc4	dávka
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
větší	veliký	k2eAgFnSc6d2	veliký
zelené	zelený	k2eAgFnSc6d1	zelená
bramboře	brambora	k1gFnSc6	brambora
či	či	k8xC	či
v	v	k7c6	v
cca	cca	kA	cca
1	[number]	k4	1
kg	kg	kA	kg
zdravých	zdravý	k2eAgFnPc2d1	zdravá
brambor	brambora	k1gFnPc2	brambora
<g/>
.	.	kIx.	.
</s>
<s>
Otravy	otrava	k1gFnPc1	otrava
se	se	k3xPyFc4	se
ale	ale	k9	ale
prakticky	prakticky	k6eAd1	prakticky
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Bramborám	brambora	k1gFnPc3	brambora
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
chladnější	chladný	k2eAgNnSc1d2	chladnější
vlhké	vlhký	k2eAgNnSc1d1	vlhké
klima	klima	k1gNnSc1	klima
jaké	jaký	k3yIgFnPc4	jaký
panuje	panovat	k5eAaImIp3nS	panovat
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
případně	případně	k6eAd1	případně
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
teplejších	teplý	k2eAgFnPc2d2	teplejší
klimatických	klimatický	k2eAgFnPc2d1	klimatická
zón	zóna	k1gFnPc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
brambory	brambora	k1gFnPc1	brambora
nesnášejí	snášet	k5eNaImIp3nP	snášet
mrazy	mráz	k1gInPc7	mráz
<g/>
,	,	kIx,	,
při	při	k7c6	při
déletrvajících	déletrvající	k2eAgFnPc6d1	déletrvající
teplotách	teplota	k1gFnPc6	teplota
lehce	lehko	k6eAd1	lehko
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
mrazu	mráz	k1gInSc2	mráz
hlízy	hlíza	k1gFnSc2	hlíza
zmrznou	zmrznout	k5eAaPmIp3nP	zmrznout
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
pro	pro	k7c4	pro
klíčení	klíčení	k1gNnSc4	klíčení
hlíz	hlíza	k1gFnPc2	hlíza
<g/>
.	.	kIx.	.
</s>
<s>
Hlízy	hlíza	k1gFnPc1	hlíza
se	se	k3xPyFc4	se
probouzejí	probouzet	k5eAaImIp3nP	probouzet
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
okolo	okolo	k7c2	okolo
6	[number]	k4	6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
růst	růst	k1gInSc4	růst
jsou	být	k5eAaImIp3nP	být
výhodné	výhodný	k2eAgFnPc1d1	výhodná
denní	denní	k2eAgFnPc1d1	denní
teploty	teplota	k1gFnPc1	teplota
okolo	okolo	k7c2	okolo
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
noční	noční	k2eAgInPc1d1	noční
15	[number]	k4	15
°	°	k?	°
<g/>
C.	C.	kA	C.
Brambory	brambora	k1gFnPc1	brambora
nejsou	být	k5eNaImIp3nP	být
příliš	příliš	k6eAd1	příliš
náročné	náročný	k2eAgInPc1d1	náročný
na	na	k7c4	na
typ	typ	k1gInSc4	typ
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
nevyhovují	vyhovovat	k5eNaImIp3nP	vyhovovat
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
příliš	příliš	k6eAd1	příliš
zamokřené	zamokřený	k2eAgFnPc1d1	zamokřená
půdy	půda	k1gFnPc1	půda
<g/>
,	,	kIx,	,
nehumózní	humózní	k2eNgInPc1d1	humózní
váté	vátý	k2eAgInPc1d1	vátý
písky	písek	k1gInPc1	písek
a	a	k8xC	a
pro	pro	k7c4	pro
strojový	strojový	k2eAgInSc4d1	strojový
sběr	sběr	k1gInSc4	sběr
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
nehodí	hodit	k5eNaPmIp3nS	hodit
silně	silně	k6eAd1	silně
kamenité	kamenitý	k2eAgInPc4d1	kamenitý
pozemky	pozemek	k1gInPc4	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
Statistiky	statistika	k1gFnPc1	statistika
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zemědělství	zemědělství	k1gNnSc2	zemědělství
ČR	ČR	kA	ČR
udávají	udávat	k5eAaImIp3nP	udávat
poněkud	poněkud	k6eAd1	poněkud
vyšší	vysoký	k2eAgFnPc1d2	vyšší
celkové	celkový	k2eAgFnPc1d1	celková
produkce	produkce	k1gFnPc1	produkce
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
zřejmě	zřejmě	k6eAd1	zřejmě
o	o	k7c6	o
započítání	započítání	k1gNnSc6	započítání
produkce	produkce	k1gFnSc2	produkce
sadbových	sadbový	k2eAgFnPc2d1	sadbový
brambor	brambora	k1gFnPc2	brambora
do	do	k7c2	do
celkových	celkový	k2eAgNnPc2d1	celkové
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
hektarových	hektarový	k2eAgInPc2d1	hektarový
výnosů	výnos	k1gInPc2	výnos
přes	přes	k7c4	přes
40	[number]	k4	40
t	t	k?	t
z	z	k7c2	z
hektaru	hektar	k1gInSc2	hektar
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
,	,	kIx,	,
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
výnosy	výnos	k1gInPc7	výnos
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
okolo	okolo	k7c2	okolo
20	[number]	k4	20
tun	tuna	k1gFnPc2	tuna
z	z	k7c2	z
hektaru	hektar	k1gInSc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
FAO	FAO	kA	FAO
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
52	[number]	k4	52
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
použito	použít	k5eAaPmNgNnS	použít
jako	jako	k9	jako
potravina	potravina	k1gFnSc1	potravina
<g/>
,	,	kIx,	,
34	[number]	k4	34
%	%	kIx~	%
jako	jako	k8xC	jako
krmivo	krmivo	k1gNnSc1	krmivo
<g/>
,	,	kIx,	,
11	[number]	k4	11
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nP	tvořit
sadbové	sadbový	k2eAgFnPc1d1	sadbový
brambory	brambora	k1gFnPc1	brambora
a	a	k8xC	a
3	[number]	k4	3
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
škrobu	škrob	k1gInSc2	škrob
a	a	k8xC	a
lihu	líh	k1gInSc2	líh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
EU	EU	kA	EU
se	se	k3xPyFc4	se
brambory	brambor	k1gInPc7	brambor
jako	jako	k8xC	jako
krmivo	krmivo	k1gNnSc1	krmivo
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
nevyužívají	využívat	k5eNaImIp3nP	využívat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
především	především	k6eAd1	především
sójou	sója	k1gFnSc7	sója
a	a	k8xC	a
kukuřicí	kukuřice	k1gFnSc7	kukuřice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
představuje	představovat	k5eAaImIp3nS	představovat
plocha	plocha	k1gFnSc1	plocha
osázená	osázený	k2eAgFnSc1d1	osázená
bramborami	brambora	k1gFnPc7	brambora
asi	asi	k9	asi
1,3	[number]	k4	1,3
%	%	kIx~	%
orné	orný	k2eAgFnSc2d1	orná
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Hlízy	hlíza	k1gFnSc2	hlíza
brambor	brambora	k1gFnPc2	brambora
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
skladovat	skladovat	k5eAaImF	skladovat
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
<g/>
,	,	kIx,	,
v	v	k7c6	v
suchu	sucho	k1gNnSc6	sucho
a	a	k8xC	a
chladu	chlad	k1gInSc6	chlad
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
však	však	k9	však
mrazu	mráz	k1gInSc2	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Ideální	ideální	k2eAgFnPc1d1	ideální
podmínky	podmínka	k1gFnPc1	podmínka
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
4	[number]	k4	4
až	až	k9	až
6	[number]	k4	6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
6	[number]	k4	6
̊	̊	k?	̊
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
fyziologická	fyziologický	k2eAgFnSc1d1	fyziologická
činnost	činnost	k1gFnSc1	činnost
a	a	k8xC	a
relativní	relativní	k2eAgFnPc1d1	relativní
vlhkosti	vlhkost	k1gFnPc1	vlhkost
kolem	kolem	k7c2	kolem
55	[number]	k4	55
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
také	také	k9	také
dobré	dobrý	k2eAgNnSc1d1	dobré
větrání	větrání	k1gNnSc1	větrání
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnPc1d2	vyšší
teploty	teplota	k1gFnPc1	teplota
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
předčasnému	předčasný	k2eAgNnSc3d1	předčasné
klíčení	klíčení	k1gNnSc3	klíčení
brambor	brambora	k1gFnPc2	brambora
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
doprovázeno	doprovázet	k5eAaImNgNnS	doprovázet
zvyšováním	zvyšování	k1gNnSc7	zvyšování
obsahu	obsah	k1gInSc2	obsah
jedovatého	jedovatý	k2eAgInSc2d1	jedovatý
solaninu	solanin	k1gInSc2	solanin
v	v	k7c6	v
hlízách	hlíza	k1gFnPc6	hlíza
<g/>
.	.	kIx.	.
</s>
<s>
Mráz	mráz	k1gInSc1	mráz
ničí	ničit	k5eAaImIp3nS	ničit
hlízy	hlíza	k1gFnPc4	hlíza
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
pak	pak	k6eAd1	pak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
hydrolýze	hydrolýza	k1gFnSc3	hydrolýza
škrobu	škrob	k1gInSc2	škrob
na	na	k7c4	na
nízkomolekulární	nízkomolekulární	k2eAgInPc4d1	nízkomolekulární
oligosacharidy	oligosacharid	k1gInPc4	oligosacharid
<g/>
;	;	kIx,	;
poškozené	poškozený	k2eAgFnPc4d1	poškozená
hlízy	hlíza	k1gFnPc4	hlíza
pak	pak	k6eAd1	pak
snadno	snadno	k6eAd1	snadno
podléhají	podléhat	k5eAaImIp3nP	podléhat
hnilobě	hniloba	k1gFnSc3	hniloba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zabránění	zabránění	k1gNnSc4	zabránění
předčasnému	předčasný	k2eAgNnSc3d1	předčasné
klíčení	klíčení	k1gNnSc3	klíčení
a	a	k8xC	a
současně	současně	k6eAd1	současně
i	i	k9	i
k	k	k7c3	k
ničení	ničení	k1gNnSc3	ničení
spor	spor	k1gInSc1	spor
plísní	plíseň	k1gFnPc2	plíseň
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
užívá	užívat	k5eAaImIp3nS	užívat
radioaktivního	radioaktivní	k2eAgNnSc2d1	radioaktivní
ozařování	ozařování	k1gNnSc2	ozařování
brambor	brambora	k1gFnPc2	brambora
<g/>
.	.	kIx.	.
</s>
<s>
Hlízy	hlíza	k1gFnPc4	hlíza
takto	takto	k6eAd1	takto
ozářené	ozářený	k2eAgFnPc4d1	ozářená
se	se	k3xPyFc4	se
samy	sám	k3xTgFnPc1	sám
nestávají	stávat	k5eNaImIp3nP	stávat
sice	sice	k8xC	sice
radioaktivními	radioaktivní	k2eAgInPc7d1	radioaktivní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
řada	řada	k1gFnSc1	řada
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
neznalosti	neznalost	k1gFnSc2	neznalost
se	se	k3xPyFc4	se
takových	takový	k3xDgFnPc2	takový
potravin	potravina	k1gFnPc2	potravina
bezdůvodně	bezdůvodně	k6eAd1	bezdůvodně
bojí	bát	k5eAaImIp3nP	bát
<g/>
.	.	kIx.	.
</s>
<s>
Hlíza	hlíza	k1gFnSc1	hlíza
prochází	procházet	k5eAaImIp3nS	procházet
třemi	tři	k4xCgFnPc7	tři
fázemi	fáze	k1gFnPc7	fáze
<g/>
:	:	kIx,	:
Období	období	k1gNnSc1	období
po	po	k7c6	po
sklizni	sklizeň	k1gFnSc6	sklizeň
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
období	období	k1gNnSc1	období
vegetačního	vegetační	k2eAgInSc2d1	vegetační
klidu	klid	k1gInSc2	klid
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
a	a	k8xC	a
období	období	k1gNnSc4	období
přechodu	přechod	k1gInSc2	přechod
ke	k	k7c3	k
klíčení	klíčení	k1gNnSc3	klíčení
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
klíčení	klíčení	k1gNnSc3	klíčení
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
retardační	retardační	k2eAgInPc1d1	retardační
přípravky	přípravek	k1gInPc1	přípravek
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
používání	používání	k1gNnSc1	používání
určuje	určovat	k5eAaImIp3nS	určovat
Česká	český	k2eAgFnSc1d1	Česká
rostlinolékařská	rostlinolékařský	k2eAgFnSc1d1	rostlinolékařská
správa	správa	k1gFnSc1	správa
<g/>
.	.	kIx.	.
</s>
<s>
Chlopropham	Chlopropham	k1gInSc1	Chlopropham
<g/>
/	/	kIx~	/
<g/>
propham	propham	k1gInSc1	propham
je	být	k5eAaImIp3nS	být
nejpoužívanější	používaný	k2eAgInSc1d3	nejpoužívanější
výrobek	výrobek	k1gInSc1	výrobek
(	(	kIx(	(
<g/>
obchodní	obchodní	k2eAgInSc1d1	obchodní
název	název	k1gInSc1	název
<g/>
:	:	kIx,	:
Neo	Neo	k1gFnSc1	Neo
Stop	stop	k1gInSc1	stop
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skladování	skladování	k1gNnSc1	skladování
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
v	v	k7c6	v
krechtech	krecht	k1gInPc6	krecht
<g/>
,	,	kIx,	,
sklepích	sklep	k1gInPc6	sklep
nebo	nebo	k8xC	nebo
bramborárnách	bramborárna	k1gFnPc6	bramborárna
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
ložené	ložený	k2eAgInPc4d1	ložený
brambory	brambor	k1gInPc4	brambor
nebo	nebo	k8xC	nebo
na	na	k7c6	na
paletách	paleta	k1gFnPc6	paleta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
produkce	produkce	k1gFnSc2	produkce
brambor	brambora	k1gFnPc2	brambora
v	v	k7c6	v
rozvinutých	rozvinutý	k2eAgFnPc6d1	rozvinutá
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
průmyslově	průmyslově	k6eAd1	průmyslově
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
(	(	kIx(	(
<g/>
udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
75	[number]	k4	75
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
především	především	k6eAd1	především
škrob	škrob	k1gInSc4	škrob
a	a	k8xC	a
ethanol	ethanol	k1gInSc4	ethanol
ať	ať	k8xC	ať
již	již	k6eAd1	již
pro	pro	k7c4	pro
potravinářské	potravinářský	k2eAgNnSc4d1	potravinářské
či	či	k8xC	či
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
použití	použití	k1gNnSc4	použití
<g/>
.	.	kIx.	.
</s>
<s>
Značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
zpracována	zpracovat	k5eAaPmNgFnS	zpracovat
potravinářským	potravinářský	k2eAgInSc7d1	potravinářský
průmyslem	průmysl	k1gInSc7	průmysl
na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
hranolků	hranolek	k1gInPc2	hranolek
<g/>
,	,	kIx,	,
lupínků	lupínek	k1gInPc2	lupínek
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
tvoří	tvořit	k5eAaImIp3nP	tvořit
brambory	brambora	k1gFnPc1	brambora
a	a	k8xC	a
výrobky	výrobek	k1gInPc1	výrobek
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
kalorického	kalorický	k2eAgInSc2d1	kalorický
příjmu	příjem	k1gInSc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Brambory	brambora	k1gFnPc1	brambora
jsou	být	k5eAaImIp3nP	být
napadány	napadat	k5eAaPmNgFnP	napadat
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
chorob	choroba	k1gFnPc2	choroba
a	a	k8xC	a
škůdců	škůdce	k1gMnPc2	škůdce
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
nejznámějším	známý	k2eAgInSc7d3	nejznámější
důkazem	důkaz	k1gInSc7	důkaz
tohoto	tento	k3xDgNnSc2	tento
tvrzení	tvrzení	k1gNnSc2	tvrzení
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tzv.	tzv.	kA	tzv.
Velký	velký	k2eAgInSc1d1	velký
hlad	hlad	k1gInSc1	hlad
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1845	[number]	k4	1845
až	až	k9	až
1849	[number]	k4	1849
plísní	plísnit	k5eAaImIp3nP	plísnit
bramborovou	bramborový	k2eAgFnSc4d1	bramborová
(	(	kIx(	(
<g/>
Phytophtora	Phytophtor	k1gMnSc2	Phytophtor
infestans	infestansa	k1gFnPc2	infestansa
<g/>
)	)	kIx)	)
zničena	zničit	k5eAaPmNgFnS	zničit
prakticky	prakticky	k6eAd1	prakticky
veškerá	veškerý	k3xTgFnSc1	veškerý
úroda	úroda	k1gFnSc1	úroda
brambor	brambora	k1gFnPc2	brambora
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
těžkým	těžký	k2eAgFnPc3d1	těžká
ztrátám	ztráta	k1gFnPc3	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
(	(	kIx(	(
<g/>
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	s	k7c7	s
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
až	až	k9	až
1	[number]	k4	1
a	a	k8xC	a
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
milionu	milion	k4xCgInSc2	milion
<g/>
)	)	kIx)	)
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
k	k	k7c3	k
masové	masový	k2eAgFnSc3d1	masová
emigraci	emigrace	k1gFnSc3	emigrace
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tato	tento	k3xDgFnSc1	tento
katastrofa	katastrofa	k1gFnSc1	katastrofa
významně	významně	k6eAd1	významně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
britské	britský	k2eAgInPc4d1	britský
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
irské	irský	k2eAgFnPc4d1	irská
dějiny	dějiny	k1gFnPc4	dějiny
i	i	k8xC	i
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
její	její	k3xOp3gInPc1	její
důsledky	důsledek	k1gInPc1	důsledek
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
patrné	patrný	k2eAgFnSc2d1	patrná
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Choroby	choroba	k1gFnPc1	choroba
většinou	většina	k1gFnSc7	většina
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
plíseň	plíseň	k1gFnSc4	plíseň
bramborová	bramborový	k2eAgFnSc1d1	bramborová
(	(	kIx(	(
<g/>
Phytophthora	Phytophthora	k1gFnSc1	Phytophthora
infestans	infestansa	k1gFnPc2	infestansa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
černá	černý	k2eAgFnSc1d1	černá
noha	noha	k1gFnSc1	noha
(	(	kIx(	(
<g/>
Erwinia	Erwinium	k1gNnPc1	Erwinium
carotovora	carotovor	k1gMnSc2	carotovor
var.	var.	k?	var.
atroseptica	atrosepticus	k1gMnSc2	atrosepticus
<g/>
)	)	kIx)	)
hnědá	hnědý	k2eAgFnSc1d1	hnědá
skvrnitost	skvrnitost	k1gFnSc1	skvrnitost
listů	list	k1gInPc2	list
(	(	kIx(	(
<g/>
Alternaria	Alternarium	k1gNnSc2	Alternarium
solani	solan	k1gMnPc5	solan
<g/>
)	)	kIx)	)
kořenomorka	kořenomorka	k1gFnSc1	kořenomorka
bramborová	bramborový	k2eAgFnSc1d1	bramborová
(	(	kIx(	(
<g/>
Rhizoctonia	Rhizoctonium	k1gNnPc1	Rhizoctonium
solani	solaň	k1gFnSc6	solaň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
strupovitost	strupovitost	k1gFnSc1	strupovitost
brambor	brambora	k1gFnPc2	brambora
(	(	kIx(	(
<g/>
Streptomyces	Streptomyces	k1gMnSc1	Streptomyces
scabies	scabies	k1gMnSc1	scabies
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
rakovina	rakovina	k1gFnSc1	rakovina
brambor	brambora	k1gFnPc2	brambora
(	(	kIx(	(
<g/>
Synchytrium	Synchytrium	k1gNnSc1	Synchytrium
endobiotikum	endobiotikum	k1gNnSc1	endobiotikum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
karanténní	karanténní	k2eAgFnSc1d1	karanténní
Ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
plísním	plíseň	k1gFnPc3	plíseň
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c4	v
pěstování	pěstování	k1gNnSc4	pěstování
odolných	odolný	k2eAgFnPc2d1	odolná
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
,	,	kIx,	,
aplikaci	aplikace	k1gFnSc6	aplikace
fungicidů	fungicid	k1gInPc2	fungicid
a	a	k8xC	a
správné	správný	k2eAgFnSc6d1	správná
agrotechnice	agrotechnika	k1gFnSc6	agrotechnika
<g/>
.	.	kIx.	.
bakteriální	bakteriální	k2eAgFnSc4d1	bakteriální
kroužkovitost	kroužkovitost	k1gFnSc4	kroužkovitost
bramboru	brambor	k1gInSc2	brambor
(	(	kIx(	(
<g/>
Clavibacter	Clavibacter	k1gMnSc1	Clavibacter
michiganehsis	michiganehsis	k1gFnSc2	michiganehsis
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
sepedonicus	sepedonicus	k1gMnSc1	sepedonicus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
karanténní	karanténní	k2eAgFnSc1d1	karanténní
hnědá	hnědý	k2eAgFnSc1d1	hnědá
plíseň	plíseň	k1gFnSc1	plíseň
brambor	brambora	k1gFnPc2	brambora
fuzáriová	fuzáriový	k2eAgFnSc1d1	fuzáriová
hniloba	hniloba	k1gFnSc1	hniloba
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Fusarium	Fusarium	k1gNnSc1	Fusarium
ssp	ssp	k?	ssp
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
suchá	suchý	k2eAgFnSc1d1	suchá
fomová	fomový	k2eAgFnSc1d1	fomový
hniloba	hniloba	k1gFnSc1	hniloba
(	(	kIx(	(
<g/>
Phoma	Phoma	k1gFnSc1	Phoma
foveata	foveata	k1gFnSc1	foveata
<g/>
)	)	kIx)	)
mokrá	mokrý	k2eAgFnSc1d1	mokrá
bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
hniloba	hniloba	k1gFnSc1	hniloba
(	(	kIx(	(
<g/>
Erwinia	Erwinium	k1gNnSc2	Erwinium
carotovora	carotovora	k1gFnSc1	carotovora
<g/>
)	)	kIx)	)
stolbur	stolbur	k1gInSc1	stolbur
bramboru	brambor	k1gInSc2	brambor
<g/>
,	,	kIx,	,
karanténní	karanténní	k2eAgInSc1d1	karanténní
Y	Y	kA	Y
virus	virus	k1gInSc1	virus
bramboru	brambor	k1gInSc2	brambor
(	(	kIx(	(
<g/>
PVY	PVY	kA	PVY
<g/>
)	)	kIx)	)
virus	virus	k1gInSc1	virus
svinutky	svinutka	k1gFnSc2	svinutka
bramboru	brambor	k1gInSc2	brambor
(	(	kIx(	(
<g/>
PLRV	PLRV	kA	PLRV
<g/>
)	)	kIx)	)
A	a	k8xC	a
virus	virus	k1gInSc1	virus
bramboru	brambor	k1gInSc2	brambor
PVA	PVA	kA	PVA
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
viry	vir	k1gInPc1	vir
(	(	kIx(	(
<g/>
PVM	PVM	kA	PVM
<g/>
,	,	kIx,	,
PVS	PVS	kA	PVS
<g/>
,	,	kIx,	,
PVX	PVX	kA	PVX
<g/>
,	,	kIx,	,
PMTV	PMTV	kA	PMTV
<g/>
,	,	kIx,	,
TRV	TRV	kA	TRV
<g/>
)	)	kIx)	)
viroid	viroid	k1gInSc1	viroid
vřetenovitosti	vřetenovitost	k1gFnSc2	vřetenovitost
(	(	kIx(	(
<g/>
potato	potato	k6eAd1	potato
spindle	spindle	k6eAd1	spindle
tuber	tuber	k1gInSc1	tuber
viroid	viroid	k1gInSc1	viroid
<g/>
,	,	kIx,	,
PSTVd	PSTVd	k1gInSc1	PSTVd
<g/>
)	)	kIx)	)
Některé	některý	k3yIgInPc1	některý
kmeny	kmen	k1gInPc1	kmen
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgInPc2d1	uvedený
virů	vir	k1gInPc2	vir
jsou	být	k5eAaImIp3nP	být
karanténní	karanténní	k2eAgFnPc1d1	karanténní
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
virových	virový	k2eAgFnPc2d1	virová
chorob	choroba	k1gFnPc2	choroba
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
brambor	brambora	k1gFnPc2	brambora
znásoben	znásobit	k5eAaPmNgMnS	znásobit
faktem	fakt	k1gInSc7	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
brambory	brambora	k1gFnPc1	brambora
běžně	běžně	k6eAd1	běžně
množí	množit	k5eAaImIp3nP	množit
vegetativně	vegetativně	k6eAd1	vegetativně
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
klonováním	klonování	k1gNnSc7	klonování
a	a	k8xC	a
neprochází	procházet	k5eNaImIp3nP	procházet
tak	tak	k6eAd1	tak
očistným	očistný	k2eAgNnSc7d1	očistné
stádiem	stádium	k1gNnSc7	stádium
přes	přes	k7c4	přes
semeno	semeno	k1gNnSc4	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
používání	používání	k1gNnSc6	používání
certifikované	certifikovaný	k2eAgFnSc2d1	certifikovaná
sadby	sadba	k1gFnSc2	sadba
<g/>
,	,	kIx,	,
odstranění	odstranění	k1gNnSc1	odstranění
vektorů	vektor	k1gInPc2	vektor
insekticidy	insekticid	k1gInPc1	insekticid
nebo	nebo	k8xC	nebo
fungicidy	fungicid	k1gInPc1	fungicid
<g/>
,	,	kIx,	,
používání	používání	k1gNnSc1	používání
rezistentních	rezistentní	k2eAgFnPc2d1	rezistentní
odrůd	odrůda	k1gFnPc2	odrůda
a	a	k8xC	a
odstraňování	odstraňování	k1gNnSc6	odstraňování
infikovaných	infikovaný	k2eAgFnPc2d1	infikovaná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
škůdce	škůdce	k1gMnPc4	škůdce
patří	patřit	k5eAaImIp3nS	patřit
háďátko	háďátko	k1gNnSc1	háďátko
bramborové	bramborový	k2eAgNnSc1d1	bramborové
(	(	kIx(	(
<g/>
Globodera	Globodera	k1gFnSc1	Globodera
rostochiensis	rostochiensis	k1gFnSc1	rostochiensis
a	a	k8xC	a
G.	G.	kA	G.
pallida	pallida	k1gFnSc1	pallida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
karanténní	karanténní	k2eAgNnSc1d1	karanténní
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
škůdce	škůdce	k1gMnSc1	škůdce
dokáže	dokázat	k5eAaPmIp3nS	dokázat
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
přežívat	přežívat	k5eAaImF	přežívat
poměrně	poměrně	k6eAd1	poměrně
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pozemky	pozemek	k1gInPc4	pozemek
jím	jíst	k5eAaImIp1nS	jíst
zamořené	zamořený	k2eAgNnSc1d1	zamořené
nelze	lze	k6eNd1	lze
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
brambor	brambora	k1gFnPc2	brambora
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
let	léto	k1gNnPc2	léto
i	i	k8xC	i
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Notoricky	notoricky	k6eAd1	notoricky
známým	známý	k2eAgMnSc7d1	známý
škůdcem	škůdce	k1gMnSc7	škůdce
je	být	k5eAaImIp3nS	být
též	též	k9	též
mandelinka	mandelinka	k1gFnSc1	mandelinka
bramborová	bramborový	k2eAgFnSc1d1	bramborová
požírající	požírající	k2eAgFnPc4d1	požírající
listy	lista	k1gFnPc4	lista
<g/>
.	.	kIx.	.
</s>
<s>
Varný	varný	k2eAgInSc1d1	varný
typ	typ	k1gInSc1	typ
A	a	k8xC	a
–	–	k?	–
s	s	k7c7	s
hladkou	hladký	k2eAgFnSc7d1	hladká
slupkou	slupka	k1gFnSc7	slupka
a	a	k8xC	a
lojovitou	lojovitý	k2eAgFnSc7d1	lojovitý
dužinou	dužina	k1gFnSc7	dužina
žluté	žlutý	k2eAgFnPc1d1	žlutá
či	či	k8xC	či
žlutobílé	žlutobílý	k2eAgFnPc1d1	žlutobílá
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
protáhlý	protáhlý	k2eAgInSc4d1	protáhlý
tvar	tvar	k1gInSc4	tvar
(	(	kIx(	(
<g/>
ledvinky	ledvinka	k1gFnPc1	ledvinka
<g/>
,	,	kIx,	,
rohlíčky	rohlíček	k1gInPc1	rohlíček
<g/>
)	)	kIx)	)
a	a	k8xC	a
menší	malý	k2eAgInSc1d2	menší
obsah	obsah	k1gInSc1	obsah
škrobu	škrob	k1gInSc2	škrob
<g/>
.	.	kIx.	.
</s>
<s>
Ideální	ideální	k2eAgNnSc1d1	ideální
pro	pro	k7c4	pro
vaření	vaření	k1gNnSc4	vaření
ve	v	k7c6	v
slupce	slupka	k1gFnSc6	slupka
<g/>
,	,	kIx,	,
na	na	k7c4	na
bramborový	bramborový	k2eAgInSc4d1	bramborový
salát	salát	k1gInSc4	salát
<g/>
,	,	kIx,	,
na	na	k7c4	na
loupačku	loupačka	k1gFnSc4	loupačka
<g/>
.	.	kIx.	.
<g/>
Varný	varný	k2eAgInSc1d1	varný
typ	typ	k1gInSc1	typ
B	B	kA	B
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
brambory	brambor	k1gInPc1	brambor
polopevné	polopevný	k2eAgInPc1d1	polopevný
s	s	k7c7	s
univerzálním	univerzální	k2eAgNnSc7d1	univerzální
použitím	použití	k1gNnSc7	použití
<g/>
.	.	kIx.	.
<g/>
Vhodné	vhodný	k2eAgNnSc1d1	vhodné
do	do	k7c2	do
salátů	salát	k1gInPc2	salát
<g/>
,	,	kIx,	,
gulášů	guláš	k1gInPc2	guláš
<g/>
,	,	kIx,	,
polévek	polévka	k1gFnPc2	polévka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
přílohové	přílohový	k2eAgFnPc1d1	přílohová
a	a	k8xC	a
restované	restovaný	k2eAgFnPc1d1	restovaná
<g/>
.	.	kIx.	.
<g/>
Varný	varný	k2eAgInSc1d1	varný
typ	typ	k1gInSc1	typ
C	C	kA	C
–	–	k?	–
s	s	k7c7	s
drsnější	drsný	k2eAgFnSc7d2	drsnější
slupkou	slupka	k1gFnSc7	slupka
a	a	k8xC	a
bílou	bílý	k2eAgFnSc7d1	bílá
dužinou	dužina	k1gFnSc7	dužina
jsou	být	k5eAaImIp3nP	být
škrobovité	škrobovitý	k2eAgInPc1d1	škrobovitý
<g/>
,	,	kIx,	,
moučnaté	moučnatý	k2eAgInPc1d1	moučnatý
a	a	k8xC	a
po	po	k7c6	po
uvaření	uvaření	k1gNnSc6	uvaření
se	se	k3xPyFc4	se
rozpadají	rozpadat	k5eAaPmIp3nP	rozpadat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
brambory	brambor	k1gInPc1	brambor
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
bramborového	bramborový	k2eAgNnSc2d1	bramborové
těsta	těsto	k1gNnSc2	těsto
<g/>
,	,	kIx,	,
knedlíků	knedlík	k1gInPc2	knedlík
<g/>
,	,	kIx,	,
bramboráků	bramborák	k1gInPc2	bramborák
<g/>
,	,	kIx,	,
bramborových	bramborový	k2eAgFnPc2d1	bramborová
placek	placka	k1gFnPc2	placka
a	a	k8xC	a
kaší	kaše	k1gFnPc2	kaše
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
varnými	varný	k2eAgInPc7d1	varný
typy	typ	k1gInPc7	typ
AB	AB	kA	AB
nebo	nebo	k8xC	nebo
BC	BC	kA	BC
<g/>
;	;	kIx,	;
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
odrůdy	odrůda	k1gFnPc4	odrůda
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
vlastnosti	vlastnost	k1gFnPc1	vlastnost
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
rozmezí	rozmezí	k1gNnSc2	rozmezí
dvou	dva	k4xCgInPc2	dva
varných	varný	k2eAgInPc2d1	varný
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Rané	raný	k2eAgInPc1d1	raný
brambory	brambor	k1gInPc1	brambor
<g/>
:	:	kIx,	:
Brambory	brambor	k1gInPc1	brambor
sklizené	sklizený	k2eAgInPc1d1	sklizený
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Pozdní	pozdní	k2eAgInPc4d1	pozdní
brambory	brambor	k1gInPc4	brambor
<g/>
:	:	kIx,	:
Brambory	brambora	k1gFnPc4	brambora
sklizené	sklizený	k2eAgFnPc4d1	sklizená
po	po	k7c6	po
30	[number]	k4	30
<g/>
.	.	kIx.	.
červnu	červen	k1gInSc3	červen
<g/>
.	.	kIx.	.
</s>
