<s>
Ještěd	Ještěd	k1gInSc1	Ještěd
(	(	kIx(	(
<g/>
1012	[number]	k4	1012
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Jeschken	Jeschken	k2eAgInSc1d1	Jeschken
nebo	nebo	k8xC	nebo
Jeschkenkoppe	Jeschkenkopp	k1gMnSc5	Jeschkenkopp
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Liberce	Liberec	k1gInSc2	Liberec
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
vrchol	vrchol	k1gInSc4	vrchol
Ještědsko-kozákovského	Ještědskoozákovský	k2eAgInSc2d1	Ještědsko-kozákovský
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
prominencí	prominence	k1gFnSc7	prominence
517	[number]	k4	517
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
převýšení	převýšení	k1gNnSc1	převýšení
od	od	k7c2	od
sedla	sedlo	k1gNnSc2	sedlo
v	v	k7c6	v
Jeřmanicích	Jeřmanice	k1gFnPc6	Jeřmanice
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ho	on	k3xPp3gMnSc4	on
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
Jizerských	jizerský	k2eAgFnPc2d1	Jizerská
hor	hora	k1gFnPc2	hora
<g/>
)	)	kIx)	)
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jedenáctou	jedenáctý	k4xOgFnSc4	jedenáctý
nejprominentnější	prominentní	k2eAgFnSc4d3	nejprominentnější
horu	hora	k1gFnSc4	hora
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
známý	známý	k2eAgInSc1d1	známý
televizní	televizní	k2eAgInSc1d1	televizní
vysílač	vysílač	k1gInSc1	vysílač
v	v	k7c6	v
technicistním	technicistní	k2eAgInSc6d1	technicistní
architektonickém	architektonický	k2eAgInSc6d1	architektonický
stylu	styl	k1gInSc6	styl
<g/>
;	;	kIx,	;
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
vysílače	vysílač	k1gInSc2	vysílač
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
hotel	hotel	k1gInSc1	hotel
a	a	k8xC	a
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Ještěd	Ještěd	k1gInSc4	Ještěd
vedou	vést	k5eAaImIp3nP	vést
lanovky	lanovka	k1gFnPc1	lanovka
<g/>
,	,	kIx,	,
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
je	být	k5eAaImIp3nS	být
lyžařské	lyžařský	k2eAgNnSc1d1	lyžařské
středisko	středisko	k1gNnSc1	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
Ještědem	Ještěd	k1gInSc7	Ještěd
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Horní	horní	k2eAgInSc1d1	horní
Hanychov	Hanychov	k1gInSc1	Hanychov
<g/>
,	,	kIx,	,
nejzápadnější	západní	k2eAgFnSc1d3	nejzápadnější
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Liberce	Liberec	k1gInSc2	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Masiv	masiv	k1gInSc1	masiv
Ještědu	Ještěd	k1gInSc2	Ještěd
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
součástí	součást	k1gFnPc2	součást
Přírodního	přírodní	k2eAgInSc2d1	přírodní
parku	park	k1gInSc2	park
Ještěd	Ještěd	k1gInSc1	Ještěd
(	(	kIx(	(
<g/>
93,6	[number]	k4	93,6
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
výstavbou	výstavba	k1gFnSc7	výstavba
vysílače	vysílač	k1gInSc2	vysílač
zde	zde	k6eAd1	zde
stál	stát	k5eAaImAgInS	stát
velký	velký	k2eAgInSc1d1	velký
kříž	kříž	k1gInSc1	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Ještědské	ještědský	k2eAgInPc1d1	ještědský
kříže	kříž	k1gInPc1	kříž
mají	mít	k5eAaImIp3nP	mít
svoji	svůj	k3xOyFgFnSc4	svůj
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
historii	historie	k1gFnSc4	historie
doloženou	doložený	k2eAgFnSc4d1	doložená
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1737	[number]	k4	1737
<g/>
;	;	kIx,	;
prozatím	prozatím	k6eAd1	prozatím
poslední	poslední	k2eAgNnSc1d1	poslední
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tajně	tajně	k6eAd1	tajně
uříznut	uříznut	k2eAgMnSc1d1	uříznut
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
komunistického	komunistický	k2eAgMnSc2d1	komunistický
předsedy	předseda	k1gMnSc2	předseda
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
sehrál	sehrát	k5eAaPmAgInS	sehrát
Ještěd	Ještěd	k1gInSc1	Ještěd
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
srpnové	srpnový	k2eAgFnSc6d1	srpnová
okupaci	okupace	k1gFnSc6	okupace
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
když	když	k8xS	když
odtud	odtud	k6eAd1	odtud
ve	v	k7c6	v
dnech	den	k1gInPc6	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vysílalo	vysílat	k5eAaImAgNnS	vysílat
televizní	televizní	k2eAgNnSc1d1	televizní
Svobodné	svobodný	k2eAgNnSc1d1	svobodné
studio	studio	k1gNnSc1	studio
Sever	sever	k1gInSc1	sever
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
výrazný	výrazný	k2eAgInSc1d1	výrazný
kuželovitý	kuželovitý	k2eAgInSc1d1	kuželovitý
suk	suk	k1gInSc1	suk
budovaný	budovaný	k2eAgInSc1d1	budovaný
ordovickými	ordovický	k2eAgInPc7d1	ordovický
sericitickými	sericitický	k2eAgInPc7d1	sericitický
kvarcity	kvarcit	k1gInPc7	kvarcit
<g/>
,	,	kIx,	,
tvořícími	tvořící	k2eAgInPc7d1	tvořící
mocnou	mocný	k2eAgFnSc4d1	mocná
vložku	vložka	k1gFnSc4	vložka
v	v	k7c6	v
prekambrických	prekambrický	k2eAgInPc6d1	prekambrický
chlorit-muskovitických	chlorituskovitický	k2eAgInPc6d1	chlorit-muskovitický
fylitech	fylit	k1gInPc6	fylit
<g/>
,	,	kIx,	,
vystupujících	vystupující	k2eAgInPc2d1	vystupující
na	na	k7c6	na
západních	západní	k2eAgInPc6d1	západní
svazích	svah	k1gInPc6	svah
<g/>
.	.	kIx.	.
</s>
<s>
Suk	suk	k1gInSc1	suk
se	s	k7c7	s
stupňovitými	stupňovitý	k2eAgInPc7d1	stupňovitý
příkrými	příkrý	k2eAgInPc7d1	příkrý
svahy	svah	k1gInPc7	svah
s	s	k7c7	s
kryoplanačními	kryoplanační	k2eAgFnPc7d1	kryoplanační
terasami	terasa	k1gFnPc7	terasa
krytými	krytý	k2eAgFnPc7d1	krytá
balvanovými	balvanový	k2eAgFnPc7d1	balvanová
haldami	halda	k1gFnPc7	halda
a	a	k8xC	a
kamennými	kamenný	k2eAgInPc7d1	kamenný
moři	moře	k1gNnSc6	moře
(	(	kIx(	(
<g/>
z	z	k7c2	z
rozpadlých	rozpadlý	k2eAgInPc2d1	rozpadlý
mrazových	mrazový	k2eAgInPc2d1	mrazový
srubů	srub	k1gInPc2	srub
a	a	k8xC	a
izolovaných	izolovaný	k2eAgFnPc2d1	izolovaná
skal	skála	k1gFnPc2	skála
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zvedá	zvedat	k5eAaImIp3nS	zvedat
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
největšího	veliký	k2eAgInSc2d3	veliký
zdvihu	zdvih	k1gInSc2	zdvih
hřbetu	hřbet	k1gInSc2	hřbet
nad	nad	k7c7	nad
Libereckou	liberecký	k2eAgFnSc7d1	liberecká
kotlinou	kotlina	k1gFnSc7	kotlina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fylitových	fylitový	k2eAgInPc6d1	fylitový
svazích	svah	k1gInPc6	svah
hřbetu	hřbet	k1gInSc2	hřbet
jsou	být	k5eAaImIp3nP	být
místy	místy	k6eAd1	místy
kvarcitové	kvarcitový	k2eAgFnPc1d1	kvarcitová
skály	skála	k1gFnPc1	skála
(	(	kIx(	(
<g/>
horolezecké	horolezecký	k2eAgInPc1d1	horolezecký
objekty	objekt	k1gInPc1	objekt
<g/>
)	)	kIx)	)
s	s	k7c7	s
hranáčovymi	hranáčovy	k1gFnPc7	hranáčovy
osypy	osyp	k1gInPc4	osyp
a	a	k8xC	a
svahové	svahový	k2eAgFnPc1d1	Svahová
sutě	suť	k1gFnPc1	suť
<g/>
,	,	kIx,	,
kryoplanační	kryoplanační	k2eAgFnPc1d1	kryoplanační
terasy	terasa	k1gFnPc1	terasa
(	(	kIx(	(
<g/>
PP	PP	kA	PP
Terasy	terasa	k1gFnSc2	terasa
Ještědu	Ještěd	k1gInSc2	Ještěd
-	-	kIx~	-
největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
kryoplanačních	kryoplanační	k2eAgFnPc2d1	kryoplanační
teras	terasa	k1gFnPc2	terasa
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
svahu	svah	k1gInSc6	svah
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihozápadních	jihozápadní	k2eAgInPc6d1	jihozápadní
svazích	svah	k1gInPc6	svah
pramení	pramenit	k5eAaImIp3nS	pramenit
Ploučnice	Ploučnice	k1gFnSc1	Ploučnice
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
3,9	[number]	k4	3,9
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc4d1	roční
úhrn	úhrn	k1gInSc4	úhrn
srážek	srážka	k1gFnPc2	srážka
852	[number]	k4	852
mm	mm	kA	mm
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
-	-	kIx~	-
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
se	se	k3xPyFc4	se
zvedá	zvedat	k5eAaImIp3nS	zvedat
nad	nad	k7c7	nad
hranicí	hranice	k1gFnSc7	hranice
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
v	v	k7c6	v
pouhých	pouhý	k2eAgInPc6d1	pouhý
980	[number]	k4	980
metrech	metr	k1gInPc6	metr
(	(	kIx(	(
<g/>
vlivem	vliv	k1gInSc7	vliv
exponované	exponovaný	k2eAgFnSc2d1	exponovaná
geomorfologické	geomorfologický	k2eAgFnSc2d1	geomorfologická
polohy	poloha	k1gFnSc2	poloha
a	a	k8xC	a
silných	silný	k2eAgInPc2d1	silný
větrů	vítr	k1gInPc2	vítr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svahy	svah	k1gInPc1	svah
převážně	převážně	k6eAd1	převážně
zalesněné	zalesněný	k2eAgInPc1d1	zalesněný
-	-	kIx~	-
ve	v	k7c6	v
vrcholových	vrcholový	k2eAgFnPc6d1	vrcholová
partiích	partie	k1gFnPc6	partie
jsou	být	k5eAaImIp3nP	být
rozvolněné	rozvolněný	k2eAgInPc1d1	rozvolněný
klimaticky	klimaticky	k6eAd1	klimaticky
deformované	deformovaný	k2eAgInPc4d1	deformovaný
umělé	umělý	k2eAgInPc4d1	umělý
porosty	porost	k1gInPc4	porost
kleče	kleč	k1gFnSc2	kleč
<g/>
,	,	kIx,	,
smrku	smrk	k1gInSc2	smrk
<g/>
,	,	kIx,	,
břízy	bříza	k1gFnSc2	bříza
<g/>
,	,	kIx,	,
modřínu	modřín	k1gInSc2	modřín
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
přechodné	přechodný	k2eAgFnPc4d1	přechodná
lesokřoviny	lesokřovina	k1gFnPc4	lesokřovina
po	po	k7c6	po
rozvrácených	rozvrácený	k2eAgFnPc6d1	rozvrácená
smrčinách	smrčina	k1gFnPc6	smrčina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lesních	lesní	k2eAgInPc6d1	lesní
porostech	porost	k1gInPc6	porost
převládá	převládat	k5eAaImIp3nS	převládat
smrk	smrk	k1gInSc1	smrk
s	s	k7c7	s
vtroušeným	vtroušený	k2eAgInSc7d1	vtroušený
bukem	buk	k1gInSc7	buk
a	a	k8xC	a
jeřábem	jeřáb	k1gInSc7	jeřáb
<g/>
.	.	kIx.	.
</s>
<s>
Ještěd	Ještěd	k1gInSc1	Ještěd
náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
celku	celek	k1gInSc2	celek
Ještědsko-kozákovský	Ještědskoozákovský	k2eAgInSc4d1	Ještědsko-kozákovský
hřbet	hřbet	k1gInSc4	hřbet
<g/>
,	,	kIx,	,
podcelku	podcelka	k1gFnSc4	podcelka
Ještědský	ještědský	k2eAgMnSc1d1	ještědský
hřbet	hřbet	k1gMnSc1	hřbet
<g/>
,	,	kIx,	,
okrsku	okrsek	k1gInSc2	okrsek
Hlubocký	hlubocký	k2eAgInSc4d1	hlubocký
hřbet	hřbet	k1gInSc4	hřbet
a	a	k8xC	a
podokrsku	podokrska	k1gFnSc4	podokrska
Pasecký	Pasecký	k2eAgInSc4d1	Pasecký
hřbet	hřbet	k1gInSc4	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Ještěd	Ještěd	k1gInSc1	Ještěd
byl	být	k5eAaImAgInS	být
vždy	vždy	k6eAd1	vždy
dominantou	dominanta	k1gFnSc7	dominanta
severních	severní	k2eAgFnPc2d1	severní
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
symbolem	symbol	k1gInSc7	symbol
kraje	kraj	k1gInSc2	kraj
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
kříž	kříž	k1gInSc1	kříž
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
byl	být	k5eAaImAgInS	být
vztyčen	vztyčit	k5eAaPmNgInS	vztyčit
údajně	údajně	k6eAd1	údajně
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1737	[number]	k4	1737
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
kamenný	kamenný	k2eAgMnSc1d1	kamenný
a	a	k8xC	a
vydržel	vydržet	k5eAaPmAgMnS	vydržet
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
povalen	povalen	k2eAgInSc1d1	povalen
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
tradice	tradice	k1gFnSc1	tradice
křížů	kříž	k1gInPc2	kříž
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
už	už	k6eAd1	už
jen	jen	k6eAd1	jen
kříži	kříž	k1gInPc7	kříž
dřevěnými	dřevěný	k2eAgInPc7d1	dřevěný
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
postaven	postavit	k5eAaPmNgInS	postavit
obelisk	obelisk	k1gInSc1	obelisk
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
návštěvy	návštěva	k1gFnSc2	návštěva
Adély	Adéla	k1gFnSc2	Adéla
Rohanové	Rohanová	k1gFnSc2	Rohanová
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
dodnes	dodnes	k6eAd1	dodnes
Rohanský	Rohanský	k2eAgInSc1d1	Rohanský
kámen	kámen	k1gInSc1	kámen
(	(	kIx(	(
<g/>
Rohanstein	Rohanstein	k1gInSc1	Rohanstein
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kamenný	kamenný	k2eAgInSc1d1	kamenný
objekt	objekt	k1gInSc1	objekt
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c4	na
někdejší	někdejší	k2eAgFnSc4d1	někdejší
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Rohanským	Rohanský	k2eAgNnSc7d1	Rohanské
a	a	k8xC	a
Clam-Gallasovským	Clam-Gallasovský	k2eAgNnSc7d1	Clam-Gallasovský
panstvím	panství	k1gNnSc7	panství
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
turistů	turist	k1gMnPc2	turist
o	o	k7c4	o
vrchol	vrchol	k1gInSc4	vrchol
se	se	k3xPyFc4	se
probudil	probudit	k5eAaPmAgInS	probudit
až	až	k9	až
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Turisté	turist	k1gMnPc1	turist
však	však	k9	však
netoužili	toužit	k5eNaImAgMnP	toužit
jen	jen	k9	jen
po	po	k7c6	po
nádherném	nádherný	k2eAgInSc6d1	nádherný
výhledu	výhled	k1gInSc6	výhled
po	po	k7c6	po
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přáli	přát	k5eAaImAgMnP	přát
si	se	k3xPyFc3	se
také	také	k6eAd1	také
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
načerpat	načerpat	k5eAaPmF	načerpat
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
jim	on	k3xPp3gMnPc3	on
vyšli	vyjít	k5eAaPmAgMnP	vyjít
vstříc	vstříc	k6eAd1	vstříc
manželé	manžel	k1gMnPc1	manžel
Florian	Florian	k1gMnSc1	Florian
a	a	k8xC	a
Barbara	Barbara	k1gFnSc1	Barbara
Haslerovi	Haslerův	k2eAgMnPc1d1	Haslerův
z	z	k7c2	z
Horního	horní	k2eAgInSc2d1	horní
Hanychova	Hanychův	k2eAgInSc2d1	Hanychův
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
o	o	k7c6	o
letních	letní	k2eAgFnPc6d1	letní
nedělích	neděle	k1gFnPc6	neděle
roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
zřídili	zřídit	k5eAaPmAgMnP	zřídit
prodej	prodej	k1gFnSc4	prodej
občerstvení	občerstvení	k1gNnSc2	občerstvení
z	z	k7c2	z
přinesených	přinesený	k2eAgFnPc2d1	přinesená
zásob	zásoba	k1gFnPc2	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
nabízel	nabízet	k5eAaImAgInS	nabízet
podobnou	podobný	k2eAgFnSc4d1	podobná
službu	služba	k1gFnSc4	služba
také	také	k9	také
rohanský	rohanský	k2eAgMnSc1d1	rohanský
lesník	lesník	k1gMnSc1	lesník
Hebelt	Hebelt	k1gMnSc1	Hebelt
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
zbudovali	zbudovat	k5eAaPmAgMnP	zbudovat
Haslerovi	Haslerův	k2eAgMnPc1d1	Haslerův
na	na	k7c6	na
Ještědu	Ještěd	k1gInSc6	Ještěd
vlastní	vlastní	k2eAgFnSc4d1	vlastní
chatu	chata	k1gFnSc4	chata
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
vylepšovali	vylepšovat	k5eAaImAgMnP	vylepšovat
a	a	k8xC	a
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
nenabízeli	nabízet	k5eNaImAgMnP	nabízet
jen	jen	k9	jen
občerstvení	občerstvený	k2eAgMnPc1d1	občerstvený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
nocleh	nocleh	k1gInSc1	nocleh
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
chata	chata	k1gFnSc1	chata
dosloužila	dosloužit	k5eAaPmAgFnS	dosloužit
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
však	však	k9	však
záhy	záhy	k6eAd1	záhy
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
chatou	chata	k1gFnSc7	chata
novou	nový	k2eAgFnSc7d1	nová
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
kamennou	kamenný	k2eAgFnSc4d1	kamenná
<g/>
,	,	kIx,	,
finančně	finančně	k6eAd1	finančně
podpořenou	podpořený	k2eAgFnSc4d1	podpořená
i	i	k8xC	i
mnoha	mnoho	k4c2	mnoho
předními	přední	k2eAgFnPc7d1	přední
libereckými	liberecký	k2eAgFnPc7d1	liberecká
občany	občan	k1gMnPc4	občan
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
Rohanově	Rohanův	k2eAgFnSc3d1	Rohanova
chatě	chata	k1gFnSc3	chata
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
přistavěna	přistavět	k5eAaPmNgFnS	přistavět
Německým	německý	k2eAgInSc7d1	německý
horským	horský	k2eAgInSc7d1	horský
spolkem	spolek	k1gInSc7	spolek
terasa	terasa	k1gFnSc1	terasa
pro	pro	k7c4	pro
200	[number]	k4	200
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1876	[number]	k4	1876
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
postavena	postaven	k2eAgFnSc1d1	postavena
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
5	[number]	k4	5
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
věž	věž	k1gFnSc1	věž
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
rozhlednou	rozhledna	k1gFnSc7	rozhledna
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
vydržela	vydržet	k5eAaPmAgFnS	vydržet
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
července	červenec	k1gInSc2	červenec
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
však	však	k9	však
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
rozhledna	rozhledna	k1gFnSc1	rozhledna
nová	nový	k2eAgFnSc1d1	nová
-	-	kIx~	-
vysoká	vysoký	k2eAgFnSc1d1	vysoká
8	[number]	k4	8
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
začalo	začít	k5eAaPmAgNnS	začít
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
honosnější	honosný	k2eAgFnSc6d2	honosnější
<g/>
.	.	kIx.	.
</s>
<s>
Liberecká	liberecký	k2eAgFnSc1d1	liberecká
firma	firma	k1gFnSc1	firma
Gustav	Gustav	k1gMnSc1	Gustav
Sachers	Sachers	k1gInSc1	Sachers
a	a	k8xC	a
synové	syn	k1gMnPc1	syn
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
kamennou	kamenný	k2eAgFnSc4d1	kamenná
stavbu	stavba	k1gFnSc4	stavba
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
středověkého	středověký	k2eAgInSc2d1	středověký
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
horský	horský	k2eAgInSc1d1	horský
spolek	spolek	k1gInSc1	spolek
však	však	k9	však
na	na	k7c4	na
takto	takto	k6eAd1	takto
velkolepou	velkolepý	k2eAgFnSc4d1	velkolepá
stavbu	stavba	k1gFnSc4	stavba
neměl	mít	k5eNaImAgInS	mít
dostatek	dostatek	k1gInSc1	dostatek
prostředků	prostředek	k1gInPc2	prostředek
a	a	k8xC	a
souhlas	souhlas	k1gInSc1	souhlas
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
nedal	dát	k5eNaPmAgMnS	dát
ani	ani	k9	ani
majitel	majitel	k1gMnSc1	majitel
panství	panství	k1gNnSc2	panství
-	-	kIx~	-
hrabě	hrabě	k1gMnSc1	hrabě
Clam-Gallas	Clam-Gallas	k1gMnSc1	Clam-Gallas
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
horský	horský	k2eAgInSc1d1	horský
hotel	hotel	k1gInSc1	hotel
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
libereckého	liberecký	k2eAgMnSc2d1	liberecký
stavitele	stavitel	k1gMnSc2	stavitel
Schäfera	Schäfer	k1gMnSc2	Schäfer
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
27	[number]	k4	27
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1906	[number]	k4	1906
a	a	k8xC	a
již	již	k6eAd1	již
za	za	k7c4	za
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
-	-	kIx~	-
13	[number]	k4	13
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1907	[number]	k4	1907
-	-	kIx~	-
byl	být	k5eAaImAgInS	být
hotel	hotel	k1gInSc1	hotel
slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřít	k5eAaPmNgInS	otevřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
přízemí	přízemí	k1gNnSc6	přízemí
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
společenská	společenský	k2eAgFnSc1d1	společenská
místnost	místnost	k1gFnSc1	místnost
pro	pro	k7c4	pro
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
hostů	host	k1gMnPc2	host
<g/>
,	,	kIx,	,
hotel	hotel	k1gInSc1	hotel
měl	mít	k5eAaImAgInS	mít
23	[number]	k4	23
pokojů	pokoj	k1gInPc2	pokoj
a	a	k8xC	a
noclehárnu	noclehárna	k1gFnSc4	noclehárna
<g/>
,	,	kIx,	,
terasu	terasa	k1gFnSc4	terasa
pro	pro	k7c4	pro
50	[number]	k4	50
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
23	[number]	k4	23
metrů	metr	k1gInPc2	metr
vysokou	vysoký	k2eAgFnSc4d1	vysoká
rozhlednu	rozhledna	k1gFnSc4	rozhledna
<g/>
.	.	kIx.	.
</s>
<s>
Horský	horský	k2eAgInSc1d1	horský
spolek	spolek	k1gInSc1	spolek
zaplatil	zaplatit	k5eAaPmAgInS	zaplatit
za	za	k7c4	za
stavbu	stavba	k1gFnSc4	stavba
161	[number]	k4	161
000	[number]	k4	000
K	K	kA	K
<g/>
,	,	kIx,	,
pozemek	pozemek	k1gInSc4	pozemek
pod	pod	k7c7	pod
hotelem	hotel	k1gInSc7	hotel
však	však	k8xC	však
nevlastnil	vlastnit	k5eNaImAgMnS	vlastnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měl	mít	k5eAaImAgMnS	mít
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
50	[number]	k4	50
let	léto	k1gNnPc2	léto
pronajatý	pronajatý	k2eAgInSc1d1	pronajatý
od	od	k7c2	od
hraběte	hrabě	k1gMnSc2	hrabě
<g/>
.	.	kIx.	.
</s>
<s>
Atraktivita	atraktivita	k1gFnSc1	atraktivita
Ještědu	Ještěd	k1gInSc2	Ještěd
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
,	,	kIx,	,
když	když	k8xS	když
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1909	[number]	k4	1909
<g/>
-	-	kIx~	-
<g/>
1911	[number]	k4	1911
postavena	postaven	k2eAgFnSc1d1	postavena
tříkilometrová	tříkilometrový	k2eAgFnSc1d1	tříkilometrová
sáňkařská	sáňkařský	k2eAgFnSc1d1	sáňkařská
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
konalo	konat	k5eAaImAgNnS	konat
první	první	k4xOgNnSc1	první
mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Dráha	dráha	k1gFnSc1	dráha
byla	být	k5eAaImAgFnS	být
koncem	koncem	k7c2	koncem
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
poškozena	poškozen	k2eAgFnSc1d1	poškozena
polomem	polom	k1gInSc7	polom
<g/>
,	,	kIx,	,
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
jejích	její	k3xOp3gNnPc6	její
místech	místo	k1gNnPc6	místo
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
<g/>
,	,	kIx,	,
kratší	krátký	k2eAgFnSc1d2	kratší
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
torzo	torzo	k1gNnSc1	torzo
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnou	skutečný	k2eAgFnSc7d1	skutečná
ranou	rána	k1gFnSc7	rána
pro	pro	k7c4	pro
Horský	horský	k2eAgInSc4d1	horský
spolek	spolek	k1gInSc4	spolek
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
postavení	postavení	k1gNnSc1	postavení
lanovky	lanovka	k1gFnSc2	lanovka
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
sice	sice	k8xC	sice
znamenalo	znamenat	k5eAaImAgNnS	znamenat
příliv	příliv	k1gInSc4	příliv
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
výnosu	výnos	k1gInSc2	výnos
stal	stát	k5eAaPmAgInS	stát
vrchol	vrchol	k1gInSc1	vrchol
hory	hora	k1gFnSc2	hora
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
majetkem	majetek	k1gInSc7	majetek
provozovatele	provozovatel	k1gMnSc2	provozovatel
lanové	lanový	k2eAgFnSc2d1	lanová
dráhy	dráha	k1gFnSc2	dráha
(	(	kIx(	(
<g/>
ČSD	ČSD	kA	ČSD
<g/>
)	)	kIx)	)
a	a	k8xC	a
tak	tak	k6eAd1	tak
vlastně	vlastně	k9	vlastně
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
hotel	hotel	k1gInSc1	hotel
stal	stát	k5eAaPmAgInS	stát
majetkem	majetek	k1gInSc7	majetek
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přešel	přejít	k5eAaPmAgInS	přejít
pod	pod	k7c4	pod
hotelový	hotelový	k2eAgInSc4d1	hotelový
a	a	k8xC	a
restaurační	restaurační	k2eAgInSc4d1	restaurační
podnik	podnik	k1gInSc4	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
sloužil	sloužit	k5eAaImAgMnS	sloužit
kromě	kromě	k7c2	kromě
turistiky	turistika	k1gFnSc2	turistika
také	také	k9	také
televiznímu	televizní	k2eAgNnSc3d1	televizní
vysílání	vysílání	k1gNnSc3	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Hotel	hotel	k1gInSc1	hotel
i	i	k8xC	i
původní	původní	k2eAgFnSc1d1	původní
Rohanova	Rohanův	k2eAgFnSc1d1	Rohanova
chata	chata	k1gFnSc1	chata
(	(	kIx(	(
<g/>
přejmenovaná	přejmenovaný	k2eAgFnSc1d1	přejmenovaná
na	na	k7c4	na
Chatu	chata	k1gFnSc4	chata
brigádníků	brigádník	k1gMnPc2	brigádník
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
rekonstruovány	rekonstruovat	k5eAaBmNgInP	rekonstruovat
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
libereckého	liberecký	k2eAgMnSc2d1	liberecký
architekta	architekt	k1gMnSc2	architekt
Karla	Karel	k1gMnSc2	Karel
Wintera	Winter	k1gMnSc2	Winter
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
však	však	k8xC	však
při	při	k7c6	při
neopatrném	opatrný	k2eNgNnSc6d1	neopatrné
rozmrazování	rozmrazování	k1gNnSc6	rozmrazování
potrubí	potrubí	k1gNnSc2	potrubí
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
hotelu	hotel	k1gInSc2	hotel
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
celá	celý	k2eAgFnSc1d1	celá
stavba	stavba	k1gFnSc1	stavba
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
osud	osud	k1gInSc1	osud
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
potkal	potkat	k5eAaPmAgInS	potkat
i	i	k9	i
Rohanovu	Rohanův	k2eAgFnSc4d1	Rohanova
chatu	chata	k1gFnSc4	chata
-	-	kIx~	-
jen	jen	k9	jen
důvod	důvod	k1gInSc1	důvod
byl	být	k5eAaImAgInS	být
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
:	:	kIx,	:
rozmrazování	rozmrazování	k1gNnSc1	rozmrazování
oděvů	oděv	k1gInPc2	oděv
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
vrchol	vrchol	k1gInSc1	vrchol
neměl	mít	k5eNaImAgInS	mít
zůstat	zůstat	k5eAaPmF	zůstat
pustý	pustý	k2eAgInSc1d1	pustý
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
navrhována	navrhován	k2eAgFnSc1d1	navrhována
jako	jako	k8xC	jako
hotel	hotel	k1gInSc1	hotel
i	i	k8xC	i
televizní	televizní	k2eAgInSc1d1	televizní
vysílač	vysílač	k1gInSc1	vysílač
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc4	projekt
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
architekt	architekt	k1gMnSc1	architekt
Karel	Karel	k1gMnSc1	Karel
Hubáček	Hubáček	k1gMnSc1	Hubáček
z	z	k7c2	z
ateliéru	ateliér	k1gInSc2	ateliér
SIAL	sial	k1gInSc4	sial
Liberec	Liberec	k1gInSc1	Liberec
a	a	k8xC	a
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
30	[number]	k4	30
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
stavby	stavba	k1gFnSc2	stavba
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
ubytovna	ubytovna	k1gFnSc1	ubytovna
dělníků	dělník	k1gMnPc2	dělník
Pozemních	pozemní	k2eAgFnPc2d1	pozemní
staveb	stavba	k1gFnPc2	stavba
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
Černého	Černého	k2eAgInSc2d1	Černého
vrchu	vrch	k1gInSc2	vrch
horská	horský	k2eAgFnSc1d1	horská
chata	chata	k1gFnSc1	chata
Ještědka	Ještědka	k1gFnSc1	Ještědka
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
hotel	hotel	k1gInSc1	hotel
a	a	k8xC	a
vysílač	vysílač	k1gInSc1	vysílač
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
rotačního	rotační	k2eAgInSc2d1	rotační
hyperboloidu	hyperboloid	k1gInSc2	hyperboloid
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřít	k5eAaPmNgInS	otevřít
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Ještěd	Ještěd	k1gInSc1	Ještěd
je	být	k5eAaImIp3nS	být
také	také	k9	také
významným	významný	k2eAgInSc7d1	významný
telekomunikačním	telekomunikační	k2eAgInSc7d1	telekomunikační
uzlem	uzel	k1gInSc7	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
když	když	k8xS	když
původní	původní	k2eAgFnSc1d1	původní
chata	chata	k1gFnSc1	chata
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
postavit	postavit	k5eAaPmF	postavit
vedle	vedle	k7c2	vedle
horní	horní	k2eAgFnSc2d1	horní
stanice	stanice	k1gFnSc2	stanice
lanovky	lanovka	k1gFnSc2	lanovka
provizorní	provizorní	k2eAgFnSc4d1	provizorní
vysílací	vysílací	k2eAgFnSc4d1	vysílací
stanici	stanice	k1gFnSc4	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
vysílalo	vysílat	k5eAaImAgNnS	vysílat
ve	v	k7c6	v
dnech	den	k1gInPc6	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
Svobodné	svobodný	k2eAgFnSc2d1	svobodná
studio	studio	k1gNnSc4	studio
Sever	sever	k1gInSc1	sever
provozované	provozovaný	k2eAgFnSc2d1	provozovaná
dvěma	dva	k4xCgFnPc7	dva
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
rozhlasovým	rozhlasový	k2eAgMnSc7d1	rozhlasový
redaktorem	redaktor	k1gMnSc7	redaktor
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Hladíkem	Hladík	k1gMnSc7	Hladík
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
používali	používat	k5eAaImAgMnP	používat
vybavení	vybavení	k1gNnSc4	vybavení
Libereckých	liberecký	k2eAgInPc2d1	liberecký
výstavních	výstavní	k2eAgInPc2d1	výstavní
trhů	trh	k1gInPc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Sedmadvacátého	sedmadvacátý	k4xOgInSc2	sedmadvacátý
srpna	srpen	k1gInSc2	srpen
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
živě	živě	k6eAd1	živě
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
herec	herec	k1gMnSc1	herec
Jan	Jan	k1gMnSc1	Jan
Tříska	Tříska	k1gMnSc1	Tříska
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
událost	událost	k1gFnSc4	událost
dnes	dnes	k6eAd1	dnes
připomíná	připomínat	k5eAaImIp3nS	připomínat
pamětní	pamětní	k2eAgFnSc1d1	pamětní
tabulka	tabulka	k1gFnSc1	tabulka
na	na	k7c6	na
posledním	poslední	k2eAgMnSc6d1	poslední
ze	z	k7c2	z
zbytku	zbytek	k1gInSc2	zbytek
základů	základ	k1gInPc2	základ
bývalé	bývalý	k2eAgFnSc2d1	bývalá
vysílací	vysílací	k2eAgFnSc2d1	vysílací
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgInSc1d1	tradiční
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
odstraněný	odstraněný	k2eAgInSc1d1	odstraněný
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
hory	hora	k1gFnSc2	hora
vrátil	vrátit	k5eAaPmAgMnS	vrátit
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Lyžařské	lyžařský	k2eAgNnSc1d1	lyžařské
středisko	středisko	k1gNnSc1	středisko
na	na	k7c6	na
Ještědu	Ještěd	k1gInSc6	Ještěd
ležící	ležící	k2eAgFnSc1d1	ležící
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
540	[number]	k4	540
<g/>
-	-	kIx~	-
<g/>
1000	[number]	k4	1000
m	m	kA	m
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejpřístupnějším	přístupný	k2eAgInSc7d3	nejpřístupnější
takovým	takový	k3xDgInSc7	takový
areálem	areál	k1gInSc7	areál
v	v	k7c6	v
celých	celý	k2eAgFnPc6d1	celá
Čechách	Čechy	k1gFnPc6	Čechy
-	-	kIx~	-
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
areálu	areál	k1gInSc2	areál
totiž	totiž	k9	totiž
jezdí	jezdit	k5eAaImIp3nS	jezdit
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
linka	linka	k1gFnSc1	linka
z	z	k7c2	z
města	město	k1gNnSc2	město
Liberce	Liberec	k1gInSc2	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
do	do	k7c2	do
února	únor	k1gInSc2	únor
2009	[number]	k4	2009
měla	mít	k5eAaImAgFnS	mít
areál	areál	k1gInSc1	areál
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
na	na	k7c4	na
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
pronajatý	pronajatý	k2eAgInSc1d1	pronajatý
společnost	společnost	k1gFnSc1	společnost
Snowhill	Snowhillum	k1gNnPc2	Snowhillum
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
jeho	jeho	k3xOp3gInSc4	jeho
pronájem	pronájem	k1gInSc4	pronájem
platit	platit	k5eAaImF	platit
městu	město	k1gNnSc3	město
8	[number]	k4	8
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
pak	pak	k6eAd1	pak
ještě	ještě	k9	ještě
5,50	[number]	k4	5,50
Kč	Kč	kA	Kč
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
prodané	prodaný	k2eAgFnSc2d1	prodaná
jízdenky	jízdenka	k1gFnSc2	jízdenka
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
dále	daleko	k6eAd2	daleko
měla	mít	k5eAaImAgFnS	mít
do	do	k7c2	do
areálu	areál	k1gInSc2	areál
investovat	investovat	k5eAaBmF	investovat
na	na	k7c4	na
300	[number]	k4	300
miliónů	milión	k4xCgInPc2	milión
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
areál	areál	k1gInSc4	areál
skutečně	skutečně	k6eAd1	skutečně
inovovala	inovovat	k5eAaBmAgFnS	inovovat
a	a	k8xC	a
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
čtyři	čtyři	k4xCgFnPc1	čtyři
lanové	lanový	k2eAgFnPc1d1	lanová
dráhy	dráha	k1gFnPc1	dráha
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
vleky	vlek	k1gInPc4	vlek
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2009	[number]	k4	2009
však	však	k9	však
byly	být	k5eAaImAgFnP	být
podepsány	podepsán	k2eAgFnPc1d1	podepsána
smlouvy	smlouva	k1gFnPc1	smlouva
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
celý	celý	k2eAgInSc4d1	celý
areál	areál	k1gInSc4	areál
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
městské	městský	k2eAgFnSc2d1	městská
společnosti	společnost	k1gFnSc2	společnost
Sportovní	sportovní	k2eAgInSc1d1	sportovní
areál	areál	k1gInSc1	areál
Ještěd	Ještěd	k1gInSc1	Ještěd
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
samotný	samotný	k2eAgInSc1d1	samotný
trpěl	trpět	k5eAaImAgInS	trpět
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
nedostatkem	nedostatek	k1gInSc7	nedostatek
sněhu	sníh	k1gInSc2	sníh
způsobeným	způsobený	k2eAgInSc7d1	způsobený
vyššími	vysoký	k2eAgFnPc7d2	vyšší
teplotami	teplota	k1gFnPc7	teplota
a	a	k8xC	a
menším	malý	k2eAgNnSc7d2	menší
množstvím	množství	k1gNnSc7	množství
sněhu	sníh	k1gInSc2	sníh
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
byl	být	k5eAaImAgInS	být
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
až	až	k9	až
systémem	systém	k1gInSc7	systém
umělého	umělý	k2eAgNnSc2d1	umělé
zasněžování	zasněžování	k1gNnSc2	zasněžování
pokrývajícím	pokrývající	k2eAgInSc7d1	pokrývající
80	[number]	k4	80
%	%	kIx~	%
sjezdových	sjezdový	k2eAgFnPc2d1	sjezdová
tratí	trať	k1gFnPc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
sjezdařských	sjezdařský	k2eAgFnPc2d1	sjezdařská
tratí	trať	k1gFnPc2	trať
<g/>
:	:	kIx,	:
9,2	[number]	k4	9,2
kilometrů	kilometr	k1gInPc2	kilometr
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
<g/>
:	:	kIx,	:
540	[number]	k4	540
až	až	k9	až
1000	[number]	k4	1000
metrů	metr	k1gInPc2	metr
Počet	počet	k1gInSc1	počet
lanovek	lanovka	k1gFnPc2	lanovka
<g/>
:	:	kIx,	:
1	[number]	k4	1
kabinová	kabinový	k2eAgFnSc1d1	kabinová
pro	pro	k7c4	pro
38	[number]	k4	38
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
2	[number]	k4	2
čtyřsedačkové	čtyřsedačkový	k2eAgInPc1d1	čtyřsedačkový
<g/>
,	,	kIx,	,
1	[number]	k4	1
dvousedačková	dvousedačkový	k2eAgFnSc1d1	dvousedačková
Počet	počet	k1gInSc1	počet
vleků	vlek	k1gInPc2	vlek
<g/>
:	:	kIx,	:
5	[number]	k4	5
Mapa	mapa	k1gFnSc1	mapa
areálu	areál	k1gInSc2	areál
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
společnosti	společnost	k1gFnSc2	společnost
Snowhill	Snowhillum	k1gNnPc2	Snowhillum
Od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
Liberec	Liberec	k1gInSc1	Liberec
hostitelem	hostitel	k1gMnSc7	hostitel
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
klasickém	klasický	k2eAgNnSc6d1	klasické
lyžování	lyžování	k1gNnSc6	lyžování
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
šampionát	šampionát	k1gInSc4	šampionát
se	se	k3xPyFc4	se
skládal	skládat	k5eAaImAgMnS	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
:	:	kIx,	:
skoků	skok	k1gInPc2	skok
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
,	,	kIx,	,
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
a	a	k8xC	a
severské	severský	k2eAgFnSc2d1	severská
kombinace	kombinace	k1gFnSc2	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Skokanská	skokanský	k2eAgFnSc1d1	skokanská
část	část	k1gFnSc1	část
šampionátu	šampionát	k1gInSc2	šampionát
se	se	k3xPyFc4	se
odehrávala	odehrávat	k5eAaImAgFnS	odehrávat
právě	právě	k9	právě
v	v	k7c6	v
Ještědském	ještědský	k2eAgInSc6d1	ještědský
areálu	areál	k1gInSc6	areál
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
část	část	k1gFnSc4	část
běžeckou	běžecký	k2eAgFnSc4d1	běžecká
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
areál	areál	k1gInSc1	areál
v	v	k7c6	v
libereckém	liberecký	k2eAgInSc6d1	liberecký
Vesci	Vesec	k1gInSc6	Vesec
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
částí	část	k1gFnSc7	část
areálu	areál	k1gInSc2	areál
je	být	k5eAaImIp3nS	být
také	také	k9	také
dvojice	dvojice	k1gFnSc1	dvojice
můstků	můstek	k1gInPc2	můstek
pro	pro	k7c4	pro
skoky	skok	k1gInPc4	skok
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
:	:	kIx,	:
HS134	HS134	k1gFnSc1	HS134
HS100	HS100	k1gFnSc6	HS100
Oba	dva	k4xCgInPc1	dva
můstky	můstek	k1gInPc1	můstek
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
svahu	svah	k1gInSc6	svah
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
již	již	k6eAd1	již
několikrát	několikrát	k6eAd1	několikrát
upravovány	upravovat	k5eAaImNgFnP	upravovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
dnešní	dnešní	k2eAgInPc4d1	dnešní
i	i	k8xC	i
budoucí	budoucí	k2eAgInPc4d1	budoucí
požadavky	požadavek	k1gInPc4	požadavek
splňují	splňovat	k5eAaImIp3nP	splňovat
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
musí	muset	k5eAaImIp3nS	muset
již	již	k6eAd1	již
jen	jen	k9	jen
dojít	dojít	k5eAaPmF	dojít
výstavbě	výstavba	k1gFnSc3	výstavba
diváckých	divácký	k2eAgFnPc2d1	divácká
tribun	tribuna	k1gFnPc2	tribuna
<g/>
,	,	kIx,	,
chlazení	chlazení	k1gNnSc4	chlazení
nájezdové	nájezdový	k2eAgFnSc2d1	nájezdová
stopy	stopa	k1gFnSc2	stopa
a	a	k8xC	a
položení	položení	k1gNnSc2	položení
umělé	umělý	k2eAgFnSc2d1	umělá
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
je	být	k5eAaImIp3nS	být
bude	být	k5eAaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
využívat	využívat	k5eAaPmF	využívat
celoročně	celoročně	k6eAd1	celoročně
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
němečtí	německý	k2eAgMnPc1d1	německý
turisté	turist	k1gMnPc1	turist
vedli	vést	k5eAaImAgMnP	vést
evidenci	evidence	k1gFnSc4	evidence
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
pěších	pěší	k2eAgInPc2d1	pěší
výstupů	výstup	k1gInPc2	výstup
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
hory	hora	k1gFnSc2	hora
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
zveřejňovali	zveřejňovat	k5eAaImAgMnP	zveřejňovat
jména	jméno	k1gNnPc4	jméno
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
dostaly	dostat	k5eAaPmAgFnP	dostat
nejvícekrát	jvícekrát	k6eNd1	jvícekrát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
Rudolf	Rudolf	k1gMnSc1	Rudolf
Kauschka	Kauschka	k1gMnSc1	Kauschka
s	s	k7c7	s
druhy	druh	k1gInPc7	druh
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
12	[number]	k4	12
<g/>
×	×	k?	×
během	během	k7c2	během
jediného	jediný	k2eAgInSc2d1	jediný
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Držitelkou	držitelka	k1gFnSc7	držitelka
rekordu	rekord	k1gInSc2	rekord
v	v	k7c6	v
největším	veliký	k2eAgInSc6d3	veliký
počtu	počet	k1gInSc6	počet
výstupů	výstup	k1gInPc2	výstup
na	na	k7c4	na
horu	hora	k1gFnSc4	hora
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
Lilly	Lilla	k1gFnSc2	Lilla
Flassaková	Flassakový	k2eAgFnSc1d1	Flassaková
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
celkem	celek	k1gInSc7	celek
709	[number]	k4	709
krát	krát	k6eAd1	krát
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
její	její	k3xOp3gInSc1	její
výkon	výkon	k1gInSc1	výkon
překonal	překonat	k5eAaPmAgInS	překonat
až	až	k9	až
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
třiašedesátiletý	třiašedesátiletý	k2eAgMnSc1d1	třiašedesátiletý
důchodce	důchodce	k1gMnSc1	důchodce
Vladimír	Vladimír	k1gMnSc1	Vladimír
Ježek	Ježek	k1gMnSc1	Ježek
z	z	k7c2	z
Liberce	Liberec	k1gInSc2	Liberec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c4	na
Ještěd	Ještěd	k1gInSc4	Ještěd
po	po	k7c4	po
712	[number]	k4	712
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výstupech	výstup	k1gInPc6	výstup
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
i	i	k9	i
poté	poté	k6eAd1	poté
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
vrcholu	vrchol	k1gInSc2	vrchol
hory	hora	k1gFnPc4	hora
po	po	k7c4	po
1000	[number]	k4	1000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
jiná	jiný	k2eAgFnSc1d1	jiná
paní	paní	k1gFnSc1	paní
-	-	kIx~	-
Frieda	Frieda	k1gMnSc1	Frieda
Mandeliková	Mandelikový	k2eAgFnSc1d1	Mandelikový
-	-	kIx~	-
překonala	překonat	k5eAaPmAgFnS	překonat
hranici	hranice	k1gFnSc4	hranice
5000	[number]	k4	5000
výstupů	výstup	k1gInPc2	výstup
celkem	celkem	k6eAd1	celkem
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
tehdy	tehdy	k6eAd1	tehdy
obdržela	obdržet	k5eAaPmAgFnS	obdržet
stříbrný	stříbrný	k2eAgInSc4d1	stříbrný
odznak	odznak	k1gInSc4	odznak
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
již	již	k9	již
tradicí	tradice	k1gFnSc7	tradice
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
pořádá	pořádat	k5eAaImIp3nS	pořádat
KČT	KČT	kA	KČT
Lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
Liberec	Liberec	k1gInSc1	Liberec
"	"	kIx"	"
<g/>
Novoroční	novoroční	k2eAgInSc1d1	novoroční
výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
Ještěd	Ještěd	k1gInSc4	Ještěd
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
stovky	stovka	k1gFnPc1	stovka
turistů	turist	k1gMnPc2	turist
pěšky	pěšky	k6eAd1	pěšky
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
od	od	k7c2	od
konečné	konečný	k2eAgFnSc2d1	konečná
tramvaje	tramvaj	k1gFnSc2	tramvaj
v	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Hanychově	Hanychův	k2eAgNnSc6d1	Hanychův
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc1	výstup
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
třicátým	třicátý	k4xOgMnSc7	třicátý
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
osamělosti	osamělost	k1gFnSc3	osamělost
hory	hora	k1gFnSc2	hora
se	se	k3xPyFc4	se
z	z	k7c2	z
Ještědu	Ještěd	k1gInSc2	Ještěd
nabízejí	nabízet	k5eAaImIp3nP	nabízet
daleké	daleký	k2eAgInPc1d1	daleký
výhledy	výhled	k1gInPc1	výhled
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
polovinu	polovina	k1gFnSc4	polovina
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
lze	lze	k6eAd1	lze
dohlédnout	dohlédnout	k5eAaPmF	dohlédnout
daleko	daleko	k6eAd1	daleko
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
východněji	východně	k6eAd2	východně
pak	pak	k6eAd1	pak
na	na	k7c4	na
Jizerské	jizerský	k2eAgFnPc4d1	Jizerská
hory	hora	k1gFnPc4	hora
<g/>
,	,	kIx,	,
Krkonoše	Krkonoše	k1gFnPc4	Krkonoše
a	a	k8xC	a
Orlické	orlický	k2eAgFnPc4d1	Orlická
hory	hora	k1gFnPc4	hora
<g/>
.	.	kIx.	.
</s>
<s>
Jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
jsou	být	k5eAaImIp3nP	být
za	za	k7c2	za
pěkného	pěkný	k2eAgNnSc2d1	pěkné
počasí	počasí	k1gNnSc2	počasí
vidět	vidět	k5eAaImF	vidět
Železné	železný	k2eAgFnPc4d1	železná
hory	hora	k1gFnPc4	hora
<g/>
,	,	kIx,	,
jihozápadně	jihozápadně	k6eAd1	jihozápadně
lze	lze	k6eAd1	lze
spatřit	spatřit	k5eAaPmF	spatřit
Polabí	Polabí	k1gNnSc4	Polabí
<g/>
,	,	kIx,	,
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
Brdy	Brdy	k1gInPc4	Brdy
a	a	k8xC	a
Křivoklátskou	křivoklátský	k2eAgFnSc4d1	Křivoklátská
vrchovinu	vrchovina	k1gFnSc4	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Západně	západně	k6eAd1	západně
pak	pak	k6eAd1	pak
České	český	k2eAgNnSc1d1	české
středohoří	středohoří	k1gNnSc1	středohoří
a	a	k8xC	a
při	při	k7c6	při
perfektní	perfektní	k2eAgFnSc6d1	perfektní
viditelnosti	viditelnost	k1gFnSc6	viditelnost
Doupovské	Doupovský	k2eAgFnSc2d1	Doupovská
hory	hora	k1gFnSc2	hora
a	a	k8xC	a
Krušné	krušný	k2eAgFnSc2d1	krušná
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
