<s>
Jako	jako	k9	jako
Freikorps	Freikorps	k1gInSc1	Freikorps
(	(	kIx(	(
<g/>
svobodné	svobodný	k2eAgInPc1d1	svobodný
sbory	sbor	k1gInPc1	sbor
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
označovány	označovat	k5eAaImNgFnP	označovat
dobrovolnické	dobrovolnický	k2eAgFnPc1d1	dobrovolnická
armády	armáda	k1gFnPc1	armáda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nazývaly	nazývat	k5eAaImAgFnP	nazývat
pravicové	pravicový	k2eAgFnPc1d1	pravicová
polovojenské	polovojenský	k2eAgFnPc1d1	polovojenská
organizace	organizace	k1gFnPc1	organizace
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
Freikorps	Freikorps	k1gInSc4	Freikorps
byly	být	k5eAaImAgFnP	být
naverbovány	naverbován	k2eAgFnPc1d1	naverbována
pruským	pruský	k2eAgMnSc7d1	pruský
králem	král	k1gMnSc7	král
Fridrichem	Fridrich	k1gMnSc7	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
během	během	k7c2	během
sedmileté	sedmiletý	k2eAgFnSc2d1	sedmiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
jednotky	jednotka	k1gFnPc1	jednotka
Freikorps	Freikorpsa	k1gFnPc2	Freikorpsa
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
během	během	k7c2	během
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
vedeny	vést	k5eAaImNgInP	vést
například	například	k6eAd1	například
Ludwigem	Ludwig	k1gMnSc7	Ludwig
Adolfem	Adolf	k1gMnSc7	Adolf
Wilhemem	Wilhem	k1gInSc7	Wilhem
von	von	k1gInSc1	von
Lützow	Lützow	k1gFnPc2	Lützow
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgFnPc1d1	běžná
armády	armáda	k1gFnPc1	armáda
vnímaly	vnímat	k5eAaImAgFnP	vnímat
Freikorps	Freikorps	k1gInSc4	Freikorps
jako	jako	k9	jako
nespolehlivé	spolehlivý	k2eNgNnSc1d1	nespolehlivé
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	on	k3xPp3gInPc4	on
využívaly	využívat	k5eAaImAgFnP	využívat
hlavně	hlavně	k6eAd1	hlavně
jako	jako	k8xC	jako
hlídky	hlídka	k1gFnPc1	hlídka
a	a	k8xC	a
při	při	k7c6	při
plnění	plnění	k1gNnSc6	plnění
úkolů	úkol	k1gInPc2	úkol
nižší	nízký	k2eAgFnSc2d2	nižší
důležitosti	důležitost	k1gFnSc2	důležitost
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
slova	slovo	k1gNnSc2	slovo
Freikorps	Freikorps	k1gInSc1	Freikorps
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
byl	být	k5eAaImAgInS	být
termín	termín	k1gInSc1	termín
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
krajně	krajně	k6eAd1	krajně
pravicové	pravicový	k2eAgFnPc4d1	pravicová
polovojenské	polovojenský	k2eAgFnPc4d1	polovojenská
organizace	organizace	k1gFnPc4	organizace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
navrátilci	navrátilec	k1gMnPc1	navrátilec
z	z	k7c2	z
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
Výmarské	výmarský	k2eAgFnSc6d1	Výmarská
republice	republika	k1gFnSc6	republika
existovalo	existovat	k5eAaImAgNnS	existovat
plno	plno	k6eAd1	plno
těchto	tento	k3xDgFnPc2	tento
aktivních	aktivní	k2eAgFnPc2d1	aktivní
polovojenských	polovojenský	k2eAgFnPc2d1	polovojenská
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
německých	německý	k2eAgMnPc2d1	německý
veteránů	veterán	k1gMnPc2	veterán
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
civilního	civilní	k2eAgInSc2d1	civilní
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
do	do	k7c2	do
Freikorps	Freikorpsa	k1gFnPc2	Freikorpsa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nalezli	nalézt	k5eAaBmAgMnP	nalézt
jistotu	jistota	k1gFnSc4	jistota
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
jim	on	k3xPp3gMnPc3	on
dával	dávat	k5eAaImAgInS	dávat
vojenský	vojenský	k2eAgInSc4d1	vojenský
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
se	se	k3xPyFc4	se
přidávali	přidávat	k5eAaImAgMnP	přidávat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
svrhnout	svrhnout	k5eAaPmF	svrhnout
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
vliv	vliv	k1gInSc4	vliv
komunismu	komunismus	k1gInSc2	komunismus
či	či	k8xC	či
aby	aby	kYmCp3nP	aby
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
jistého	jistý	k2eAgInSc2d1	jistý
druhu	druh	k1gInSc2	druh
odvety	odveta	k1gFnSc2	odveta
za	za	k7c4	za
prohranou	prohraný	k2eAgFnSc4d1	prohraná
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
Freikorps	Freikorpsa	k1gFnPc2	Freikorpsa
bojovalo	bojovat	k5eAaImAgNnS	bojovat
po	po	k7c6	po
konci	konec	k1gInSc6	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
i	i	k9	i
na	na	k7c6	na
Baltu	Balt	k1gInSc6	Balt
či	či	k8xC	či
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
nebo	nebo	k8xC	nebo
Prusku	Prusko	k1gNnSc6	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
podporu	podpora	k1gFnSc4	podpora
nalezly	naleznout	k5eAaPmAgFnP	naleznout
u	u	k7c2	u
německého	německý	k2eAgMnSc2d1	německý
ministra	ministr	k1gMnSc2	ministr
obrany	obrana	k1gFnSc2	obrana
Gustava	Gustav	k1gMnSc2	Gustav
Noskeho	Noske	k1gMnSc2	Noske
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
použil	použít	k5eAaPmAgMnS	použít
k	k	k7c3	k
násilnému	násilný	k2eAgNnSc3d1	násilné
rozdrcení	rozdrcení	k1gNnSc3	rozdrcení
Spartakovců	spartakovec	k1gMnPc2	spartakovec
<g/>
.	.	kIx.	.
</s>
<s>
Útok	útok	k1gInSc1	útok
proti	proti	k7c3	proti
tomuto	tento	k3xDgNnSc3	tento
marxistickému	marxistický	k2eAgNnSc3d1	marxistické
hnutí	hnutí	k1gNnSc3	hnutí
se	se	k3xPyFc4	se
neobešel	obešet	k5eNaImAgInS	obešet
bez	bez	k7c2	bez
protiprávních	protiprávní	k2eAgInPc2d1	protiprávní
excesů	exces	k1gInPc2	exces
<g/>
,	,	kIx,	,
když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
zavražděni	zavraždit	k5eAaPmNgMnP	zavraždit
Karl	Karl	k1gMnSc1	Karl
Liebknecht	Liebknecht	k1gMnSc1	Liebknecht
a	a	k8xC	a
Rosa	Rosa	k1gFnSc1	Rosa
Luxemburgová	Luxemburgový	k2eAgFnSc1d1	Luxemburgová
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
také	také	k6eAd1	také
použity	použít	k5eAaPmNgInP	použít
k	k	k7c3	k
rozvrácení	rozvrácení	k1gNnSc3	rozvrácení
Bavorské	bavorský	k2eAgFnSc2d1	bavorská
republiky	republika	k1gFnSc2	republika
rad	rada	k1gFnPc2	rada
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
Freikorps	Freikorpsa	k1gFnPc2	Freikorpsa
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
ukončena	ukončit	k5eAaPmNgFnS	ukončit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gMnPc1	jejich
členové	člen	k1gMnPc1	člen
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1920	[number]	k4	1920
účastnili	účastnit	k5eAaImAgMnP	účastnit
Kappova	Kappův	k2eAgInSc2d1	Kappův
puče	puč	k1gInSc2	puč
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
svrhnout	svrhnout	k5eAaPmF	svrhnout
Výmarskou	výmarský	k2eAgFnSc4d1	Výmarská
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Puč	puč	k1gInSc1	puč
však	však	k9	však
skončil	skončit	k5eAaPmAgInS	skončit
neúspěchem	neúspěch	k1gInSc7	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sudetoněmecký	sudetoněmecký	k2eAgInSc4d1	sudetoněmecký
Freikorps	Freikorps	k1gInSc4	Freikorps
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
krize	krize	k1gFnSc2	krize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
pohraničí	pohraničí	k1gNnSc6	pohraničí
<g/>
,	,	kIx,	,
osídleném	osídlený	k2eAgInSc6d1	osídlený
Sudetskými	sudetský	k2eAgMnPc7d1	sudetský
Němci	Němec	k1gMnPc7	Němec
<g/>
,	,	kIx,	,
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
tajná	tajný	k2eAgFnSc1d1	tajná
teroristická	teroristický	k2eAgFnSc1d1	teroristická
organizace	organizace	k1gFnSc1	organizace
Sudetoněmecký	sudetoněmecký	k2eAgInSc1d1	sudetoněmecký
Freikorps	Freikorps	k1gInSc1	Freikorps
(	(	kIx(	(
<g/>
SdFK	SdFK	k1gFnSc1	SdFK
–	–	k?	–
Sudetendeutsches	Sudetendeutsches	k1gInSc1	Sudetendeutsches
Freikorps	Freikorps	k1gInSc1	Freikorps
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
vyzbrojování	vyzbrojování	k1gNnSc4	vyzbrojování
a	a	k8xC	a
výcviku	výcvik	k1gInSc2	výcvik
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
Wehrmacht	wehrmacht	k1gInSc1	wehrmacht
<g/>
,	,	kIx,	,
SS	SS	kA	SS
i	i	k8xC	i
SA	SA	kA	SA
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
českého	český	k2eAgNnSc2d1	české
pohraničí	pohraničí	k1gNnSc2	pohraničí
pronikali	pronikat	k5eAaImAgMnP	pronikat
němečtí	německý	k2eAgMnPc1d1	německý
důvěrníci	důvěrník	k1gMnPc1	důvěrník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vyhledávali	vyhledávat	k5eAaImAgMnP	vyhledávat
mezi	mezi	k7c7	mezi
sudetskými	sudetský	k2eAgMnPc7d1	sudetský
Němci	Němec	k1gMnPc7	Němec
dobrovolníky	dobrovolník	k1gMnPc7	dobrovolník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pak	pak	k6eAd1	pak
ilegálně	ilegálně	k6eAd1	ilegálně
přecházeli	přecházet	k5eAaImAgMnP	přecházet
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
výcviku	výcvik	k1gInSc2	výcvik
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
především	především	k6eAd1	především
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
na	na	k7c4	na
přepady	přepad	k1gInPc4	přepad
vojenských	vojenský	k2eAgFnPc2d1	vojenská
a	a	k8xC	a
četnických	četnický	k2eAgFnPc2d1	četnická
hlídek	hlídka	k1gFnPc2	hlídka
<g/>
,	,	kIx,	,
přepadávání	přepadávání	k1gNnSc1	přepadávání
celních	celní	k2eAgFnPc2d1	celní
a	a	k8xC	a
četnických	četnický	k2eAgFnPc2d1	četnická
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
pošt	pošta	k1gFnPc2	pošta
<g/>
,	,	kIx,	,
úřadů	úřad	k1gInPc2	úřad
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
záškodnickou	záškodnický	k2eAgFnSc4d1	záškodnická
činnost	činnost	k1gFnSc4	činnost
na	na	k7c6	na
československém	československý	k2eAgNnSc6d1	Československé
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tohoto	tento	k3xDgInSc2	tento
výcviku	výcvik	k1gInSc2	výcvik
byly	být	k5eAaImAgFnP	být
cvičeny	cvičen	k2eAgFnPc1d1	cvičena
i	i	k8xC	i
speciální	speciální	k2eAgFnPc1d1	speciální
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
v	v	k7c6	v
případě	případ	k1gInSc6	případ
války	válka	k1gFnSc2	válka
Německa	Německo	k1gNnSc2	Německo
s	s	k7c7	s
Československem	Československo	k1gNnSc7	Československo
přepadat	přepadat	k5eAaImF	přepadat
objekty	objekt	k1gInPc1	objekt
lehkého	lehký	k2eAgNnSc2d1	lehké
opevnění	opevnění	k1gNnSc2	opevnění
a	a	k8xC	a
ničit	ničit	k5eAaImF	ničit
překážky	překážka	k1gFnPc4	překážka
v	v	k7c6	v
liniích	linie	k1gFnPc6	linie
pohraničního	pohraniční	k2eAgNnSc2d1	pohraniční
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Záškodníci	záškodník	k1gMnPc1	záškodník
z	z	k7c2	z
Freikorpsu	Freikorps	k1gInSc2	Freikorps
byli	být	k5eAaImAgMnP	být
vyzbrojováni	vyzbrojovat	k5eAaImNgMnP	vyzbrojovat
většinou	většina	k1gFnSc7	většina
zbraněmi	zbraň	k1gFnPc7	zbraň
ze	z	k7c2	z
skladů	sklad	k1gInPc2	sklad
rakouské	rakouský	k2eAgFnSc2d1	rakouská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1	příslušník
Freikorpsu	Freikorps	k1gInSc2	Freikorps
skládali	skládat	k5eAaImAgMnP	skládat
slib	slib	k1gInSc4	slib
mlčenlivosti	mlčenlivost	k1gFnSc2	mlčenlivost
a	a	k8xC	a
přísahu	přísaha	k1gFnSc4	přísaha
doživotní	doživotní	k2eAgFnSc2d1	doživotní
věrnosti	věrnost	k1gFnSc2	věrnost
Adolfu	Adolf	k1gMnSc3	Adolf
Hitlerovi	Hitler	k1gMnSc3	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kontě	konto	k1gNnSc6	konto
organizace	organizace	k1gFnSc2	organizace
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
teroristických	teroristický	k2eAgFnPc2d1	teroristická
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgFnPc6	který
členové	člen	k1gMnPc1	člen
Freikorpsu	Freikorps	k1gInSc2	Freikorps
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
2000	[number]	k4	2000
osob	osoba	k1gFnPc2	osoba
členové	člen	k1gMnPc1	člen
Freikorpsu	Freikorpsa	k1gFnSc4	Freikorpsa
násilně	násilně	k6eAd1	násilně
unesli	unést	k5eAaPmAgMnP	unést
z	z	k7c2	z
území	území	k1gNnSc2	území
Československa	Československo	k1gNnSc2	Československo
do	do	k7c2	do
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
pozdější	pozdní	k2eAgMnPc1d2	pozdější
členové	člen	k1gMnPc1	člen
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
vůdci	vůdce	k1gMnPc1	vůdce
nacistů	nacista	k1gMnPc2	nacista
byli	být	k5eAaImAgMnP	být
členy	člen	k1gInPc4	člen
Freikorps	Freikorpsa	k1gFnPc2	Freikorpsa
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Ernsta	Ernst	k1gMnSc2	Ernst
Röhma	Röhm	k1gMnSc2	Röhm
<g/>
,	,	kIx,	,
hlavy	hlava	k1gFnSc2	hlava
SA	SA	kA	SA
<g/>
,	,	kIx,	,
či	či	k8xC	či
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Hösse	Höss	k1gMnSc2	Höss
<g/>
,	,	kIx,	,
velitele	velitel	k1gMnSc2	velitel
vyhlazovacího	vyhlazovací	k2eAgInSc2d1	vyhlazovací
tábora	tábor	k1gInSc2	tábor
Auschwitz	Auschwitza	k1gFnPc2	Auschwitza
-	-	kIx~	-
Birkenau	Birkenaa	k1gFnSc4	Birkenaa
u	u	k7c2	u
polské	polský	k2eAgFnSc2d1	polská
Osvětimi	Osvětim	k1gFnSc2	Osvětim
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Hitlerovy	Hitlerův	k2eAgFnSc2d1	Hitlerova
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
zůstala	zůstat	k5eAaPmAgFnS	zůstat
však	však	k9	však
většina	většina	k1gFnSc1	většina
členů	člen	k1gMnPc2	člen
Freikorps	Freikorpsa	k1gFnPc2	Freikorpsa
stranou	stranou	k6eAd1	stranou
hlavního	hlavní	k2eAgNnSc2d1	hlavní
dění	dění	k1gNnSc2	dění
<g/>
,	,	kIx,	,
mnozí	mnohý	k2eAgMnPc1d1	mnohý
sloužili	sloužit	k5eAaImAgMnP	sloužit
v	v	k7c6	v
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
trestní	trestní	k2eAgInSc1d1	trestní
tribunál	tribunál	k1gInSc1	tribunál
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
označil	označit	k5eAaPmAgInS	označit
Freikorps	Freikorps	k1gInSc1	Freikorps
za	za	k7c4	za
zločineckou	zločinecký	k2eAgFnSc4d1	zločinecká
teroristickou	teroristický	k2eAgFnSc4d1	teroristická
organizaci	organizace	k1gFnSc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
byli	být	k5eAaImAgMnP	být
potrestáni	potrestat	k5eAaPmNgMnP	potrestat
<g/>
,	,	kIx,	,
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
padlo	padnout	k5eAaImAgNnS	padnout
několik	několik	k4yIc4	několik
trestů	trest	k1gInPc2	trest
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
trestů	trest	k1gInPc2	trest
žaláře	žalář	k1gInSc2	žalář
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
poukázali	poukázat	k5eAaPmAgMnP	poukázat
mnozí	mnohý	k2eAgMnPc1d1	mnohý
historici	historik	k1gMnPc1	historik
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yQnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Sudetoněmeckého	sudetoněmecký	k2eAgInSc2d1	sudetoněmecký
Landsmannschaftu	Landsmannschaft	k1gInSc2	Landsmannschaft
existuje	existovat	k5eAaImIp3nS	existovat
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
snaha	snaha	k1gFnSc1	snaha
zločiny	zločin	k1gInPc7	zločin
Freikorpsu	Freikorps	k1gInSc2	Freikorps
zamlčovat	zamlčovat	k5eAaImF	zamlčovat
či	či	k8xC	či
relativizovat	relativizovat	k5eAaImF	relativizovat
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
bývalých	bývalý	k2eAgInPc2d1	bývalý
členů	člen	k1gInPc2	člen
Freikorpsu	Freikorps	k1gInSc2	Freikorps
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
aktivně	aktivně	k6eAd1	aktivně
činných	činný	k2eAgMnPc2d1	činný
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
této	tento	k3xDgFnSc2	tento
organizace	organizace	k1gFnSc2	organizace
až	až	k9	až
do	do	k7c2	do
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Landsmannschaft	Landsmannschaft	k1gMnSc1	Landsmannschaft
se	se	k3xPyFc4	se
od	od	k7c2	od
činnosti	činnost	k1gFnSc2	činnost
Freikorpsu	Freikorps	k1gInSc2	Freikorps
nikdy	nikdy	k6eAd1	nikdy
nedistancoval	distancovat	k5eNaBmAgMnS	distancovat
a	a	k8xC	a
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
jeho	jeho	k3xOp3gMnPc1	jeho
bývalí	bývalý	k2eAgMnPc1d1	bývalý
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
za	za	k7c4	za
jeho	jeho	k3xOp3gInPc4	jeho
zločiny	zločin	k1gInPc4	zločin
nikdy	nikdy	k6eAd1	nikdy
neomluvili	omluvit	k5eNaPmAgMnP	omluvit
<g/>
.	.	kIx.	.
</s>
<s>
Stráž	stráž	k1gFnSc1	stráž
obrany	obrana	k1gFnSc2	obrana
státu	stát	k1gInSc2	stát
Úderná	úderný	k2eAgFnSc1d1	úderná
skupina	skupina	k1gFnSc1	skupina
záolšanských	záolšanský	k2eAgMnPc2d1	záolšanský
povstalců	povstalec	k1gMnPc2	povstalec
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Freikorps	Freikorpsa	k1gFnPc2	Freikorpsa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
