<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Litvy	Litva	k1gFnSc2	Litva
je	být	k5eAaImIp3nS	být
sestavena	sestavit	k5eAaPmNgFnS	sestavit
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
horizontálních	horizontální	k2eAgInPc2d1	horizontální
pruhů	pruh	k1gInPc2	pruh
<g/>
:	:	kIx,	:
žlutého	žlutý	k2eAgMnSc2d1	žlutý
<g/>
,	,	kIx,	,
zeleného	zelený	k2eAgInSc2d1	zelený
a	a	k8xC	a
červeného	červený	k2eAgInSc2d1	červený
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc1d1	poslední
změna	změna	k1gFnSc1	změna
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
však	však	k9	však
změněn	změnit	k5eAaPmNgInS	změnit
pouze	pouze	k6eAd1	pouze
poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
<g/>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
výkladů	výklad	k1gInPc2	výklad
symboliky	symbolika	k1gFnSc2	symbolika
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vychází	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
přírodních	přírodní	k2eAgFnPc2d1	přírodní
a	a	k8xC	a
litevských	litevský	k2eAgFnPc2d1	Litevská
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
slunce	slunce	k1gNnSc2	slunce
ale	ale	k8xC	ale
též	též	k9	též
pšenici	pšenice	k1gFnSc4	pšenice
<g/>
,	,	kIx,	,
litevské	litevský	k2eAgNnSc4d1	litevské
zemědělské	zemědělský	k2eAgNnSc4d1	zemědělské
bohatství	bohatství	k1gNnSc4	bohatství
a	a	k8xC	a
získanou	získaný	k2eAgFnSc4d1	získaná
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
zelená	zelenat	k5eAaImIp3nS	zelenat
louky	louka	k1gFnPc4	louka
<g/>
,	,	kIx,	,
lesy	les	k1gInPc4	les
<g/>
,	,	kIx,	,
rodnou	rodný	k2eAgFnSc4d1	rodná
zem	zem	k1gFnSc4	zem
ale	ale	k8xC	ale
i	i	k9	i
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
pak	pak	k6eAd1	pak
představuje	představovat	k5eAaImIp3nS	představovat
krev	krev	k1gFnSc4	krev
Litevců	Litevec	k1gMnPc2	Litevec
prolitou	prolitý	k2eAgFnSc4d1	prolitá
za	za	k7c4	za
národní	národní	k2eAgFnSc4d1	národní
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
odvahu	odvaha	k1gFnSc4	odvaha
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
cestě	cesta	k1gFnSc6	cesta
nebo	nebo	k8xC	nebo
také	také	k9	také
suverénní	suverénní	k2eAgFnSc1d1	suverénní
moc	moc	k1gFnSc1	moc
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
udatnost	udatnost	k1gFnSc4	udatnost
a	a	k8xC	a
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
vlasti	vlast	k1gFnSc3	vlast
<g/>
.	.	kIx.	.
<g/>
Druhá	druhý	k4xOgFnSc1	druhý
státní	státní	k2eAgFnSc1d1	státní
(	(	kIx(	(
<g/>
historická	historický	k2eAgFnSc1d1	historická
<g/>
)	)	kIx)	)
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
červeným	červený	k2eAgInSc7d1	červený
listem	list	k1gInSc7	list
a	a	k8xC	a
rytířem	rytíř	k1gMnSc7	rytíř
Vytisem	Vytis	k1gInSc7	Vytis
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
vyvěšena	vyvěšen	k2eAgFnSc1d1	vyvěšena
na	na	k7c6	na
třech	tři	k4xCgNnPc6	tři
místech	místo	k1gNnPc6	místo
(	(	kIx(	(
<g/>
Královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
ve	v	k7c6	v
Vilniusu	Vilnius	k1gInSc6	Vilnius
<g/>
,	,	kIx,	,
hrad	hrad	k1gInSc4	hrad
Trakai	Traka	k1gFnSc2	Traka
a	a	k8xC	a
Muzeum	muzeum	k1gNnSc1	muzeum
války	válka	k1gFnSc2	válka
Vytautase	Vytautasa	k1gFnSc6	Vytautasa
Velikého	veliký	k2eAgMnSc4d1	veliký
v	v	k7c4	v
Kaunasu	Kaunasa	k1gFnSc4	Kaunasa
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
vybraných	vybraný	k2eAgNnPc6d1	vybrané
místech	místo	k1gNnPc6	místo
v	v	k7c4	v
určité	určitý	k2eAgInPc4d1	určitý
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
státy	stát	k1gInPc1	stát
vznikaly	vznikat	k5eAaImAgInP	vznikat
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Litvy	Litva	k1gFnSc2	Litva
již	již	k6eAd1	již
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1240	[number]	k4	1240
se	se	k3xPyFc4	se
sjednotily	sjednotit	k5eAaPmAgFnP	sjednotit
do	do	k7c2	do
Litevského	litevský	k2eAgNnSc2d1	litevské
velkoknížectví	velkoknížectví	k1gNnSc2	velkoknížectví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1386	[number]	k4	1386
<g/>
–	–	k?	–
<g/>
1569	[number]	k4	1569
měla	mít	k5eAaImAgFnS	mít
Litva	Litva	k1gFnSc1	Litva
personální	personální	k2eAgFnSc4d1	personální
unii	unie	k1gFnSc4	unie
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Krewská	Krewská	k1gFnSc1	Krewská
unie	unie	k1gFnSc1	unie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
litevských	litevský	k2eAgInPc6d1	litevský
praporech	prapor	k1gInPc6	prapor
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Grunwaldu	Grunwald	k1gInSc2	Grunwald
(	(	kIx(	(
<g/>
1410	[number]	k4	1410
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
litevsko-polsko-ruská	litevskoolskouský	k2eAgNnPc4d1	litevsko-polsko-ruský
vojska	vojsko	k1gNnPc4	vojsko
porazila	porazit	k5eAaPmAgFnS	porazit
Řád	řád	k1gInSc4	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Litevské	litevský	k2eAgInPc1d1	litevský
praporce	praporec	k1gInPc1	praporec
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
červené	červený	k2eAgInPc1d1	červený
<g/>
,	,	kIx,	,
s	s	k7c7	s
vyobrazením	vyobrazení	k1gNnSc7	vyobrazení
legendárního	legendární	k2eAgMnSc2d1	legendární
rytíře	rytíř	k1gMnSc2	rytíř
Vytise	Vytise	k1gFnSc1	Vytise
s	s	k7c7	s
taseným	tasený	k2eAgInSc7d1	tasený
mečem	meč	k1gInSc7	meč
<g/>
,	,	kIx,	,
sedícím	sedící	k2eAgNnSc7d1	sedící
na	na	k7c4	na
koni	kůň	k1gMnPc7	kůň
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
lublinské	lublinský	k2eAgFnSc2d1	Lublinská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
1569	[number]	k4	1569
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Litevské	litevský	k2eAgNnSc1d1	litevské
velkovévodství	velkovévodství	k1gNnSc1	velkovévodství
stalo	stát	k5eAaPmAgNnS	stát
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1795	[number]	k4	1795
de	de	k?	de
facto	facto	k1gNnSc1	facto
provincií	provincie	k1gFnSc7	provincie
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Užívaly	užívat	k5eAaImAgInP	užívat
se	se	k3xPyFc4	se
však	však	k9	však
symboly	symbol	k1gInPc1	symbol
s	s	k7c7	s
rytířem	rytíř	k1gMnSc7	rytíř
Vytisem	Vytis	k1gInSc7	Vytis
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1795	[number]	k4	1795
byla	být	k5eAaImAgFnS	být
Litva	Litva	k1gFnSc1	Litva
připojena	připojen	k2eAgFnSc1d1	připojena
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
jako	jako	k8xS	jako
Litevská	litevský	k2eAgFnSc1d1	Litevská
gubernie	gubernie	k1gFnSc1	gubernie
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c2	za
I.	I.	kA	I.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
v	v	k7c6	v
letech	let	k1gInPc6	let
1915	[number]	k4	1915
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
okupována	okupován	k2eAgFnSc1d1	okupována
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
říjnové	říjnový	k2eAgFnSc6d1	říjnová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
byla	být	k5eAaImAgFnS	být
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1918	[number]	k4	1918
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
Litevská	litevský	k2eAgFnSc1d1	Litevská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vlajkou	vlajka	k1gFnSc7	vlajka
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
původně	původně	k6eAd1	původně
stát	stát	k5eAaImF	stát
historická	historický	k2eAgFnSc1d1	historická
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
rytířem	rytíř	k1gMnSc7	rytíř
Vytisem	Vytis	k1gInSc7	Vytis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
negativně	negativně	k6eAd1	negativně
spojována	spojován	k2eAgFnSc1d1	spojována
s	s	k7c7	s
rudými	rudý	k2eAgFnPc7d1	rudá
revolučními	revoluční	k2eAgFnPc7d1	revoluční
vlajkami	vlajka	k1gFnPc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Odborná	odborný	k2eAgFnSc1d1	odborná
heraldická	heraldický	k2eAgFnSc1d1	heraldická
komise	komise	k1gFnSc1	komise
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
Jonas	Jonas	k1gMnSc1	Jonas
Basanavičius	Basanavičius	k1gMnSc1	Basanavičius
<g/>
,	,	kIx,	,
Antanas	Antanas	k1gMnSc1	Antanas
Žmuidžinavičius	Žmuidžinavičius	k1gMnSc1	Žmuidžinavičius
a	a	k8xC	a
Tadas	Tadas	k1gMnSc1	Tadas
Daugirdas	Daugirdasa	k1gFnPc2	Daugirdasa
měla	mít	k5eAaImAgFnS	mít
proto	proto	k8xC	proto
vybrat	vybrat	k5eAaPmF	vybrat
barvy	barva	k1gFnPc4	barva
vycházející	vycházející	k2eAgFnPc4d1	vycházející
z	z	k7c2	z
tradičních	tradiční	k2eAgInPc2d1	tradiční
litevských	litevský	k2eAgInPc2d1	litevský
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
žluto-zeleno-červená	žlutoeleno-červený	k2eAgFnSc1d1	žluto-zeleno-červený
trikolóra	trikolóra	k1gFnSc1	trikolóra
s	s	k7c7	s
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
a	a	k8xC	a
poměrem	poměr	k1gInSc7	poměr
listu	list	k1gInSc2	list
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgFnPc1	tento
barvy	barva	k1gFnPc1	barva
převládaly	převládat	k5eAaImAgFnP	převládat
na	na	k7c6	na
národních	národní	k2eAgInPc6d1	národní
krojích	kroj	k1gInPc6	kroj
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1918	[number]	k4	1918
návrh	návrh	k1gInSc4	návrh
schválila	schválit	k5eAaPmAgFnS	schválit
Litevská	litevský	k2eAgFnSc1d1	Litevská
Rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
Rada	rada	k1gFnSc1	rada
však	však	k9	však
schválila	schválit	k5eAaPmAgFnS	schválit
jako	jako	k9	jako
státní	státní	k2eAgFnSc1d1	státní
vlaku	vlak	k1gInSc2	vlak
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
historickou	historický	k2eAgFnSc4d1	historická
červenou	červený	k2eAgFnSc4d1	červená
vlajku	vlajka	k1gFnSc4	vlajka
s	s	k7c7	s
rytířem	rytíř	k1gMnSc7	rytíř
Vytisem	Vytis	k1gInSc7	Vytis
na	na	k7c6	na
rubové	rubový	k2eAgFnSc6d1	rubová
straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zdroji	zdroj	k1gInSc6	zdroj
je	být	k5eAaImIp3nS	být
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
dnešní	dnešní	k2eAgFnSc2d1	dnešní
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
obrázek	obrázek	k1gInSc1	obrázek
však	však	k9	však
není	být	k5eNaImIp3nS	být
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
lícové	lícový	k2eAgFnSc6d1	lícová
straně	strana	k1gFnSc6	strana
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
bílé	bílý	k2eAgInPc1d1	bílý
sloupy	sloup	k1gInPc1	sloup
dynastie	dynastie	k1gFnSc2	dynastie
Gediminas	Gediminasa	k1gFnPc2	Gediminasa
<g/>
.	.	kIx.	.
</s>
<s>
Litva	Litva	k1gFnSc1	Litva
tak	tak	k9	tak
měla	mít	k5eAaImAgFnS	mít
oficiálně	oficiálně	k6eAd1	oficiálně
dvě	dva	k4xCgFnPc4	dva
státní	státní	k2eAgFnPc4d1	státní
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Lícová	lícový	k2eAgFnSc1d1	lícová
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
základem	základ	k1gInSc7	základ
vlajky	vlajka	k1gFnSc2	vlajka
Litevské	litevský	k2eAgFnSc2d1	Litevská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1918	[number]	k4	1918
obsadila	obsadit	k5eAaPmAgFnS	obsadit
Litvu	Litva	k1gFnSc4	Litva
sovětská	sovětský	k2eAgFnSc1d1	sovětská
vojska	vojsko	k1gNnPc4	vojsko
a	a	k8xC	a
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1919	[number]	k4	1919
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Litevsko-běloruská	litevskoěloruský	k2eAgFnSc1d1	litevsko-běloruský
sovětská	sovětský	k2eAgFnSc1d1	sovětská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vlajkou	vlajka	k1gFnSc7	vlajka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
rudý	rudý	k2eAgInSc4d1	rudý
revoluční	revoluční	k2eAgInSc4d1	revoluční
list	list	k1gInSc4	list
bez	bez	k7c2	bez
jakýchkoli	jakýkoli	k3yIgInPc2	jakýkoli
symbolů	symbol	k1gInPc2	symbol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
obsadila	obsadit	k5eAaPmAgFnS	obsadit
polská	polský	k2eAgFnSc1d1	polská
armáda	armáda	k1gFnSc1	armáda
Vilnius	Vilnius	k1gMnSc1	Vilnius
a	a	k8xC	a
Střední	střední	k2eAgFnSc1d1	střední
Litva	Litva	k1gFnSc1	Litva
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
opět	opět	k6eAd1	opět
součástí	součást	k1gFnSc7	součást
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
Litvy	Litva	k1gFnSc2	Litva
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
státní	státní	k2eAgInPc4d1	státní
symboly	symbol	k1gInPc4	symbol
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
byly	být	k5eAaImAgFnP	být
obnoveny	obnovit	k5eAaPmNgInP	obnovit
<g/>
.	.	kIx.	.
<g/>
Přestože	přestože	k8xS	přestože
měla	mít	k5eAaImAgFnS	mít
Litva	Litva	k1gFnSc1	Litva
dvě	dva	k4xCgFnPc1	dva
oficiální	oficiální	k2eAgFnPc1d1	oficiální
státní	státní	k2eAgFnPc1d1	státní
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
,	,	kIx,	,
ústavy	ústav	k1gInPc1	ústav
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1922	[number]	k4	1922
a	a	k8xC	a
1928	[number]	k4	1928
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
pouze	pouze	k6eAd1	pouze
vlajku	vlajka	k1gFnSc4	vlajka
s	s	k7c7	s
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xC	jako
národní	národní	k2eAgFnSc1d1	národní
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
rytířem	rytíř	k1gMnSc7	rytíř
pak	pak	k6eAd1	pak
jako	jako	k9	jako
státní	státní	k2eAgFnSc1d1	státní
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
užívána	užívat	k5eAaImNgFnS	užívat
litevským	litevský	k2eAgMnSc7d1	litevský
prezidentem	prezident	k1gMnSc7	prezident
při	při	k7c6	při
zvlášť	zvlášť	k6eAd1	zvlášť
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
a	a	k8xC	a
slavnostních	slavnostní	k2eAgFnPc6d1	slavnostní
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
sovětsko-litevské	sovětskoitevský	k2eAgFnSc2d1	sovětsko-litevský
smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
pomoci	pomoc	k1gFnSc6	pomoc
(	(	kIx(	(
<g/>
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
rozmístěny	rozmístěn	k2eAgFnPc1d1	rozmístěna
posádky	posádka	k1gFnPc1	posádka
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
začala	začít	k5eAaPmAgFnS	začít
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1940	[number]	k4	1940
obsazovat	obsazovat	k5eAaImF	obsazovat
celou	celý	k2eAgFnSc4d1	celá
Litvu	Litva	k1gFnSc4	Litva
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byla	být	k5eAaImAgFnS	být
Lidovým	lidový	k2eAgInSc7d1	lidový
sněmem	sněm	k1gInSc7	sněm
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Litevská	litevský	k2eAgFnSc1d1	Litevská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byla	být	k5eAaImAgFnS	být
začleněna	začleněn	k2eAgFnSc1d1	začleněna
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Litevská	litevský	k2eAgFnSc1d1	Litevská
sovětská	sovětský	k2eAgFnSc1d1	sovětská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1940	[number]	k4	1940
užívala	užívat	k5eAaImAgFnS	užívat
Litva	Litva	k1gFnSc1	Litva
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
vlastní	vlastní	k2eAgFnSc4d1	vlastní
státní	státní	k2eAgFnSc4d1	státní
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
tvořenou	tvořený	k2eAgFnSc4d1	tvořená
červeným	červený	k2eAgInSc7d1	červený
listem	list	k1gInSc7	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
se	s	k7c7	s
žlutým	žlutý	k2eAgInSc7d1	žlutý
nápisem	nápis	k1gInSc7	nápis
LIETUVOS	LIETUVOS	kA	LIETUVOS
TSR	TSR	kA	TSR
(	(	kIx(	(
<g/>
Litevská	litevský	k2eAgFnSc1d1	Litevská
SSR	SSR	kA	SSR
<g/>
)	)	kIx)	)
a	a	k8xC	a
zkříženým	zkřížený	k2eAgInSc7d1	zkřížený
srpem	srp	k1gInSc7	srp
a	a	k8xC	a
kladivem	kladivo	k1gNnSc7	kladivo
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c4	za
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
Litva	Litva	k1gFnSc1	Litva
v	v	k7c6	v
letech	let	k1gInPc6	let
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
okupována	okupován	k2eAgFnSc1d1	okupována
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
patřila	patřit	k5eAaImAgFnS	patřit
pod	pod	k7c4	pod
Říšský	říšský	k2eAgInSc4d1	říšský
komisariát	komisariát	k1gInSc4	komisariát
Ostland	Ostlanda	k1gFnPc2	Ostlanda
<g/>
.	.	kIx.	.
</s>
<s>
Užívaly	užívat	k5eAaImAgFnP	užívat
se	se	k3xPyFc4	se
tehdejší	tehdejší	k2eAgFnPc1d1	tehdejší
německé	německý	k2eAgFnPc1d1	německá
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
.15	.15	k4	.15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1953	[number]	k4	1953
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
nová	nový	k2eAgFnSc1d1	nová
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
Litevské	litevský	k2eAgFnSc2d1	Litevská
SSR	SSR	kA	SSR
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
listem	list	k1gInSc7	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
s	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
červeným	červený	k2eAgInSc7d1	červený
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgInSc7d1	bílý
a	a	k8xC	a
zeleným	zelený	k2eAgInSc7d1	zelený
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
<g/>
,	,	kIx,	,
v	v	k7c6	v
červeném	červený	k2eAgInSc6d1	červený
pruhu	pruh	k1gInSc6	pruh
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
žlutě	žlutě	k6eAd1	žlutě
lemovaná	lemovaný	k2eAgFnSc1d1	lemovaná
pěticípá	pěticípý	k2eAgFnSc1d1	pěticípá
hvězda	hvězda	k1gFnSc1	hvězda
a	a	k8xC	a
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
byly	být	k5eAaImAgFnP	být
zkřížené	zkřížený	k2eAgNnSc4d1	zkřížené
žluté	žlutý	k2eAgNnSc4d1	žluté
kladivo	kladivo	k1gNnSc4	kladivo
se	s	k7c7	s
srpem	srp	k1gInSc7	srp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
při	při	k7c6	při
uvolňování	uvolňování	k1gNnSc6	uvolňování
režimu	režim	k1gInSc2	režim
začaly	začít	k5eAaPmAgFnP	začít
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
akcích	akce	k1gFnPc6	akce
znovu	znovu	k6eAd1	znovu
objevovat	objevovat	k5eAaImF	objevovat
žluto-zeleno-červené	žlutoeleno-červený	k2eAgFnPc4d1	žluto-zeleno-červený
vlajky	vlajka	k1gFnPc4	vlajka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
veřejnosti	veřejnost	k1gFnSc2	veřejnost
přiměl	přimět	k5eAaPmAgInS	přimět
prezidium	prezidium	k1gNnSc4	prezidium
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
Litevské	litevský	k2eAgFnSc2d1	Litevská
SSR	SSR	kA	SSR
vydat	vydat	k5eAaPmF	vydat
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1988	[number]	k4	1988
vydat	vydat	k5eAaPmF	vydat
usnesení	usnesení	k1gNnPc1	usnesení
o	o	k7c6	o
státní	státní	k2eAgFnSc6d1	státní
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc3d1	národní
a	a	k8xC	a
oblastní	oblastní	k2eAgFnSc3d1	oblastní
symbolice	symbolika	k1gFnSc3	symbolika
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
zakázané	zakázaný	k2eAgFnPc1d1	zakázaná
žluto-zeleno-červené	žlutoeleno-červený	k2eAgFnPc1d1	žluto-zeleno-červený
vlajky	vlajka	k1gFnPc1	vlajka
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
užívány	užívat	k5eAaImNgFnP	užívat
během	během	k7c2	během
mládežnických	mládežnický	k2eAgInPc2d1	mládežnický
<g/>
,	,	kIx,	,
sportovních	sportovní	k2eAgFnPc6d1	sportovní
a	a	k8xC	a
společenských	společenský	k2eAgFnPc6d1	společenská
akcích	akce	k1gFnPc6	akce
a	a	k8xC	a
při	při	k7c6	při
lidových	lidový	k2eAgFnPc6d1	lidová
slavnostech	slavnost	k1gFnPc6	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ostatních	ostatní	k2eAgFnPc6d1	ostatní
událostech	událost	k1gFnPc6	událost
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
platila	platit	k5eAaImAgNnP	platit
pravidla	pravidlo	k1gNnPc1	pravidlo
o	o	k7c6	o
užívání	užívání	k1gNnSc6	užívání
vlajky	vlajka	k1gFnSc2	vlajka
Litevské	litevský	k2eAgFnSc2d1	Litevská
SSR	SSR	kA	SSR
<g/>
.11	.11	k4	.11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc1	nezávislost
Litvy	Litva	k1gFnSc2	Litva
na	na	k7c6	na
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
usnesením	usnesení	k1gNnSc7	usnesení
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
rady	rada	k1gFnSc2	rada
Litevské	litevský	k2eAgFnSc2d1	Litevská
republiky	republika	k1gFnSc2	republika
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
platnost	platnost	k1gFnSc1	platnost
historické	historický	k2eAgFnSc2d1	historická
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
v	v	k7c6	v
dočasné	dočasný	k2eAgFnSc6d1	dočasná
ústavě	ústava	k1gFnSc6	ústava
Litevské	litevský	k2eAgFnSc2d1	Litevská
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
I.	I.	kA	I.
<g/>
,	,	kIx,	,
článku	článek	k1gInSc2	článek
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Odstín	odstín	k1gInSc1	odstín
zelené	zelený	k2eAgFnSc2d1	zelená
barvy	barva	k1gFnSc2	barva
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
jako	jako	k8xC	jako
světlejší	světlý	k2eAgInSc1d2	světlejší
než	než	k8xS	než
historické	historický	k2eAgFnPc1d1	historická
vlajky	vlajka	k1gFnPc1	vlajka
a	a	k8xC	a
poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
byl	být	k5eAaImAgInS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
na	na	k7c6	na
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2004	[number]	k4	2004
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
IX-2331	IX-2331	k1gFnPc2	IX-2331
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
změněn	změnit	k5eAaPmNgInS	změnit
poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
vlajky	vlajka	k1gFnSc2	vlajka
na	na	k7c4	na
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhá	druhý	k4xOgFnSc1	druhý
státní	státní	k2eAgFnSc1d1	státní
(	(	kIx(	(
<g/>
historická	historický	k2eAgFnSc1d1	historická
<g/>
)	)	kIx)	)
vlajka	vlajka	k1gFnSc1	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Žluto-zeleno-červená	Žlutoeleno-červený	k2eAgFnSc1d1	Žluto-zeleno-červený
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
užívána	užívat	k5eAaImNgFnS	užívat
jako	jako	k9	jako
národní	národní	k2eAgFnSc1d1	národní
i	i	k8xC	i
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Vexilologické	Vexilologický	k2eAgNnSc4d1	Vexilologický
názvosloví	názvosloví	k1gNnSc4	názvosloví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
změně	změna	k1gFnSc6	změna
poměru	poměr	k1gInSc2	poměr
stran	strana	k1gFnPc2	strana
vlajky	vlajka	k1gFnSc2	vlajka
schválena	schválit	k5eAaPmNgFnS	schválit
i	i	k9	i
vlajka	vlajka	k1gFnSc1	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
s	s	k7c7	s
červeným	červený	k2eAgInSc7d1	červený
listem	list	k1gInSc7	list
a	a	k8xC	a
rytířem	rytíř	k1gMnSc7	rytíř
Vytisem	Vytis	k1gInSc7	Vytis
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
nazývána	nazývat	k5eAaImNgFnS	nazývat
státní	státní	k2eAgFnSc7d1	státní
historickou	historický	k2eAgFnSc7d1	historická
vlajkou	vlajka	k1gFnSc7	vlajka
a	a	k8xC	a
plní	plnit	k5eAaImIp3nS	plnit
funkci	funkce	k1gFnSc4	funkce
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
Česlovas	Česlovas	k1gMnSc1	Česlovas
Juršė	Juršė	k1gMnSc1	Juršė
(	(	kIx(	(
<g/>
místopředseda	místopředseda	k1gMnSc1	místopředseda
Seimasu	Seimas	k1gInSc2	Seimas
<g/>
,	,	kIx,	,
litevského	litevský	k2eAgInSc2d1	litevský
parlamentu	parlament	k1gInSc2	parlament
<g/>
)	)	kIx)	)
a	a	k8xC	a
Edmundas	Edmundas	k1gMnSc1	Edmundas
Rimša	Rimša	k1gMnSc1	Rimša
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
Komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
heraldiku	heraldika	k1gFnSc4	heraldika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
vlajce	vlajka	k1gFnSc6	vlajka
je	být	k5eAaImIp3nS	být
vlajka	vlajka	k1gFnSc1	vlajka
stále	stále	k6eAd1	stále
vyvěšena	vyvěšen	k2eAgFnSc1d1	vyvěšena
na	na	k7c6	na
třech	tři	k4xCgNnPc6	tři
místech	místo	k1gNnPc6	místo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
ve	v	k7c6	v
Vilniusu	Vilnius	k1gInSc6	Vilnius
</s>
</p>
<p>
<s>
Hrad	hrad	k1gInSc1	hrad
Trakai	Traka	k1gFnSc2	Traka
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
války	válka	k1gFnSc2	válka
Vytautase	Vytautasa	k1gFnSc3	Vytautasa
Velikého	veliký	k2eAgMnSc2d1	veliký
v	v	k7c6	v
KaunasuDále	KaunasuDála	k1gFnSc6	KaunasuDála
se	se	k3xPyFc4	se
historická	historický	k2eAgFnSc1d1	historická
vlajka	vlajka	k1gFnSc1	vlajka
vyvěšuje	vyvěšovat	k5eAaImIp3nS	vyvěšovat
na	na	k7c6	na
vybraných	vybraný	k2eAgNnPc6d1	vybrané
místech	místo	k1gNnPc6	místo
a	a	k8xC	a
v	v	k7c4	v
určité	určitý	k2eAgInPc4d1	určitý
dny	den	k1gInPc4	den
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
obnovy	obnova	k1gFnSc2	obnova
Litevského	litevský	k2eAgInSc2d1	litevský
státu	stát	k1gInSc2	stát
na	na	k7c6	na
budově	budova	k1gFnSc6	budova
domu	dům	k1gInSc2	dům
signatářů	signatář	k1gMnPc2	signatář
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Pilies	Piliesa	k1gFnPc2	Piliesa
ve	v	k7c6	v
Vilniusu	Vilnius	k1gInSc6	Vilnius
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
obnovení	obnovení	k1gNnSc2	obnovení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Litvy	Litva	k1gFnSc2	Litva
<g/>
,	,	kIx,	,
u	u	k7c2	u
budovy	budova	k1gFnSc2	budova
Seimasu	Seimas	k1gInSc2	Seimas
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
korunovace	korunovace	k1gFnSc1	korunovace
krále	král	k1gMnSc2	král
Mindaugase	Mindaugas	k1gMnSc5	Mindaugas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
rezidence	rezidence	k1gFnSc2	rezidence
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Grünwaldu	Grünwald	k1gInSc2	Grünwald
<g/>
,	,	kIx,	,
u	u	k7c2	u
litevského	litevský	k2eAgNnSc2d1	litevské
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
obrany	obrana	k1gFnSc2	obrana
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
u	u	k7c2	u
budovy	budova	k1gFnSc2	budova
Seimasu	Seimas	k1gInSc2	Seimas
<g/>
,	,	kIx,	,
na	na	k7c6	na
sídle	sídlo	k1gNnSc6	sídlo
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
</s>
</p>
<p>
<s>
==	==	k?	==
Povinné	povinný	k2eAgNnSc1d1	povinné
vyvěšování	vyvěšování	k1gNnSc1	vyvěšování
==	==	k?	==
</s>
</p>
<p>
<s>
Zákonem	zákon	k1gInSc7	zákon
je	být	k5eAaImIp3nS	být
stanovena	stanoven	k2eAgFnSc1d1	stanovena
povinnost	povinnost	k1gFnSc1	povinnost
vyvěšení	vyvěšení	k1gNnSc1	vyvěšení
státní	státní	k2eAgFnSc2d1	státní
(	(	kIx(	(
<g/>
žluto-zeleno-červené	žlutoeleno-červený	k2eAgFnSc2d1	žluto-zeleno-červený
<g/>
)	)	kIx)	)
vlajky	vlajka	k1gFnSc2	vlajka
od	od	k7c2	od
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
do	do	k7c2	do
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
ve	v	k7c6	v
dnech	den	k1gInPc6	den
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g />
.	.	kIx.	.
</s>
<s>
<g/>
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc4	vlajka
litevských	litevský	k2eAgInPc2d1	litevský
krajů	kraj	k1gInPc2	kraj
==	==	k?	==
</s>
</p>
<p>
<s>
Litva	Litva	k1gFnSc1	Litva
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
10	[number]	k4	10
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
apskritis	apskritis	k1gFnSc1	apskritis
<g/>
,	,	kIx,	,
mn	mn	k?	mn
<g/>
.	.	kIx.	.
č.	č.	k?	č.
apskritys	apskritys	k1gInSc1	apskritys
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
kraje	kraj	k1gInPc1	kraj
užívají	užívat	k5eAaImIp3nP	užívat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
vlajky	vlajka	k1gFnPc4	vlajka
o	o	k7c6	o
jednotném	jednotný	k2eAgInSc6d1	jednotný
poměru	poměr	k1gInSc6	poměr
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Litvy	Litva	k1gFnSc2	Litva
</s>
</p>
<p>
<s>
Litevská	litevský	k2eAgFnSc1d1	Litevská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Litvy	Litva	k1gFnSc2	Litva
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Litevská	litevský	k2eAgFnSc1d1	Litevská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
