<s>
Carl	Carl	k1gMnSc1	Carl
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Scheele	Scheel	k1gInSc2	Scheel
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1742	[number]	k4	1742
Stralsund	Stralsunda	k1gFnPc2	Stralsunda
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1786	[number]	k4	1786
Köping	Köping	k1gInSc1	Köping
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švédský	švédský	k2eAgMnSc1d1	švédský
chemik	chemik	k1gMnSc1	chemik
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
mnoha	mnoho	k4c2	mnoho
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
především	především	k9	především
díky	díky	k7c3	díky
objevu	objev	k1gInSc3	objev
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
wolframu	wolfram	k1gInSc2	wolfram
<g/>
.	.	kIx.	.
</s>
<s>
Scheele	Scheele	k6eAd1	Scheele
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
farmakolog	farmakolog	k1gMnSc1	farmakolog
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
<g/>
,	,	kIx,	,
v	v	k7c6	v
Uppsale	Uppsala	k1gFnSc6	Uppsala
(	(	kIx(	(
<g/>
1770	[number]	k4	1770
<g/>
-	-	kIx~	-
<g/>
1775	[number]	k4	1775
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Köpingu	Köping	k1gInSc6	Köping
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1772	[number]	k4	1772
objevil	objevit	k5eAaPmAgMnS	objevit
kyslík	kyslík	k1gInSc4	kyslík
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
pak	pak	k9	pak
dusík	dusík	k1gInSc1	dusík
(	(	kIx(	(
<g/>
kyslík	kyslík	k1gInSc1	kyslík
objevil	objevit	k5eAaPmAgInS	objevit
roku	rok	k1gInSc2	rok
1774	[number]	k4	1774
ještě	ještě	k9	ještě
Joseph	Joseph	k1gInSc1	Joseph
Priestley	Priestlea	k1gFnSc2	Priestlea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
objevy	objev	k1gInPc4	objev
publikoval	publikovat	k5eAaBmAgMnS	publikovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
jediné	jediný	k2eAgFnSc6d1	jediná
knize	kniha	k1gFnSc6	kniha
<g/>
,	,	kIx,	,
Chemische	Chemische	k1gFnSc1	Chemische
Abhandlung	Abhandlung	k1gMnSc1	Abhandlung
von	von	k1gInSc1	von
der	drát	k5eAaImRp2nS	drát
Luft	Luft	k?	Luft
und	und	k?	und
dem	dem	k?	dem
Feuer	Feuer	k1gInSc1	Feuer
(	(	kIx(	(
<g/>
Chemické	chemický	k2eAgNnSc1d1	chemické
pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c6	o
vzduchu	vzduch	k1gInSc6	vzduch
a	a	k8xC	a
ohni	oheň	k1gInSc6	oheň
<g/>
)	)	kIx)	)
vydané	vydaný	k2eAgNnSc1d1	vydané
roku	rok	k1gInSc2	rok
1777	[number]	k4	1777
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
mj.	mj.	kA	mj.
pojmy	pojem	k1gInPc7	pojem
šíření	šíření	k1gNnSc2	šíření
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
tepelné	tepelný	k2eAgNnSc4d1	tepelné
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
také	také	k9	také
černání	černání	k1gNnSc4	černání
chloridu	chlorid	k1gInSc2	chlorid
stříbrného	stříbrný	k1gInSc2	stříbrný
UV	UV	kA	UV
paprsky	paprsek	k1gInPc1	paprsek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
důležitý	důležitý	k2eAgInSc4d1	důležitý
moment	moment	k1gInSc4	moment
pro	pro	k7c4	pro
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Scheele	Scheele	k6eAd1	Scheele
objevil	objevit	k5eAaPmAgInS	objevit
také	také	k9	také
další	další	k2eAgInPc4d1	další
chemické	chemický	k2eAgInPc4d1	chemický
prvky	prvek	k1gInPc4	prvek
<g/>
:	:	kIx,	:
baryum	baryum	k1gNnSc1	baryum
(	(	kIx(	(
<g/>
1774	[number]	k4	1774
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chlór	chlór	k1gInSc1	chlór
(	(	kIx(	(
<g/>
1774	[number]	k4	1774
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mangan	mangan	k1gInSc1	mangan
(	(	kIx(	(
<g/>
1774	[number]	k4	1774
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
molybden	molybden	k1gInSc1	molybden
(	(	kIx(	(
<g/>
1778	[number]	k4	1778
<g/>
)	)	kIx)	)
a	a	k8xC	a
wolfram	wolfram	k1gInSc1	wolfram
(	(	kIx(	(
<g/>
1781	[number]	k4	1781
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
několik	několik	k4yIc4	několik
chemických	chemický	k2eAgFnPc2d1	chemická
sloučenin	sloučenina	k1gFnPc2	sloučenina
-	-	kIx~	-
kyselinu	kyselina	k1gFnSc4	kyselina
citrónovou	citrónový	k2eAgFnSc4d1	citrónová
<g/>
,	,	kIx,	,
glycerol	glycerol	k1gInSc1	glycerol
<g/>
,	,	kIx,	,
kyanovodík	kyanovodík	k1gInSc1	kyanovodík
<g/>
,	,	kIx,	,
fluorovodík	fluorovodík	k1gInSc1	fluorovodík
a	a	k8xC	a
sirovodík	sirovodík	k1gInSc1	sirovodík
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
objevil	objevit	k5eAaPmAgInS	objevit
proces	proces	k1gInSc1	proces
podobný	podobný	k2eAgInSc1d1	podobný
pasterizaci	pasterizace	k1gFnSc3	pasterizace
<g/>
.	.	kIx.	.
</s>
<s>
Scheele	Scheele	k6eAd1	Scheele
často	často	k6eAd1	často
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
ztížených	ztížený	k2eAgFnPc6d1	ztížená
podmínkách	podmínka	k1gFnPc6	podmínka
a	a	k8xC	a
v	v	k7c6	v
nebezpečných	bezpečný	k2eNgFnPc6d1	nebezpečná
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
zlozvyků	zlozvyk	k1gInPc2	zlozvyk
bylo	být	k5eAaImAgNnS	být
ochutnávat	ochutnávat	k5eAaImF	ochutnávat
chemikálie	chemikálie	k1gFnPc4	chemikálie
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
objevil	objevit	k5eAaPmAgInS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
předčasnou	předčasný	k2eAgFnSc7d1	předčasná
smrtí	smrt	k1gFnSc7	smrt
<g/>
;	;	kIx,	;
posmrtné	posmrtný	k2eAgInPc1d1	posmrtný
symptomy	symptom	k1gInPc1	symptom
byly	být	k5eAaImAgInP	být
podobné	podobný	k2eAgInPc1d1	podobný
otravě	otrava	k1gFnSc3	otrava
rtutí	rtuť	k1gFnPc2	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
minerál	minerál	k1gInSc1	minerál
scheelit	scheelit	k1gInSc1	scheelit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
straně	strana	k1gFnSc6	strana
Měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
kráter	kráter	k1gInSc1	kráter
Scheele	Scheel	k1gInSc2	Scheel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
nese	nést	k5eAaImIp3nS	nést
i	i	k9	i
cena	cena	k1gFnSc1	cena
udělovaná	udělovaný	k2eAgFnSc1d1	udělovaná
švédskou	švédský	k2eAgFnSc7d1	švédská
farmaceutickou	farmaceutický	k2eAgFnSc7d1	farmaceutická
asociací	asociace	k1gFnSc7	asociace
Apotekarsocieteten	Apotekarsocieteten	k2eAgInSc1d1	Apotekarsocieteten
<g/>
.	.	kIx.	.
</s>
