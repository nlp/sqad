populární	populární	k2eAgMnSc1d1	populární
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
významná	významný	k2eAgFnSc1d1	významná
osobnost	osobnost	k1gFnSc1	osobnost
českého	český	k2eAgNnSc2d1	české
skautského	skautský	k2eAgNnSc2d1	skautské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
redaktor	redaktor	k1gMnSc1	redaktor
několika	několik	k4yIc2	několik
dětských	dětský	k2eAgInPc2d1	dětský
časopisů	časopis	k1gInPc2	časopis
a	a	k8xC	a
zážitkový	zážitkový	k2eAgMnSc1d1	zážitkový
vychovatel	vychovatel	k1gMnSc1	vychovatel
