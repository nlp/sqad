<s>
Antonio	Antonio	k1gMnSc1	Antonio
Giacomo	Giacoma	k1gFnSc5	Giacoma
Stradivari	Stradivari	k1gNnSc1	Stradivari
<g/>
,	,	kIx,	,
latinizováno	latinizovat	k5eAaBmNgNnS	latinizovat
jako	jako	k8xS	jako
Antonius	Antonius	k1gMnSc1	Antonius
Stradivarius	Stradivarius	k1gMnSc1	Stradivarius
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1642	[number]	k4	1642
až	až	k9	až
1648	[number]	k4	1648
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1737	[number]	k4	1737
Cremona	Cremona	k1gFnSc1	Cremona
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
mistr	mistr	k1gMnSc1	mistr
houslař	houslař	k1gMnSc1	houslař
<g/>
,	,	kIx,	,
nejslavnější	slavný	k2eAgMnSc1d3	nejslavnější
představitel	představitel	k1gMnSc1	představitel
cremonské	cremonský	k2eAgFnSc2d1	cremonský
houslařské	houslařský	k2eAgFnSc2d1	houslařská
školy	škola	k1gFnSc2	škola
.	.	kIx.	.
</s>
