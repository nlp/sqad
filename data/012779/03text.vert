<p>
<s>
Antonio	Antonio	k1gMnSc1	Antonio
Giacomo	Giacoma	k1gFnSc5	Giacoma
Stradivari	Stradivari	k1gNnSc1	Stradivari
<g/>
,	,	kIx,	,
latinizováno	latinizovat	k5eAaBmNgNnS	latinizovat
jako	jako	k8xS	jako
Antonius	Antonius	k1gMnSc1	Antonius
Stradivarius	Stradivarius	k1gMnSc1	Stradivarius
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1642	[number]	k4	1642
až	až	k9	až
1648	[number]	k4	1648
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1737	[number]	k4	1737
Cremona	Cremona	k1gFnSc1	Cremona
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
mistr	mistr	k1gMnSc1	mistr
houslař	houslař	k1gMnSc1	houslař
<g/>
,	,	kIx,	,
nejslavnější	slavný	k2eAgMnSc1d3	nejslavnější
představitel	představitel	k1gMnSc1	představitel
cremonské	cremonský	k2eAgFnSc2d1	cremonský
houslařské	houslařský	k2eAgFnSc2d1	houslařská
školy	škola	k1gFnSc2	škola
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
datum	datum	k1gNnSc4	datum
jeho	jeho	k3xOp3gNnSc2	jeho
narození	narození	k1gNnSc2	narození
není	být	k5eNaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
<g/>
,	,	kIx,	,
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1642	[number]	k4	1642
až	až	k9	až
1648	[number]	k4	1648
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
rok	rok	k1gInSc1	rok
1644	[number]	k4	1644
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
podle	podle	k7c2	podle
nápisu	nápis	k1gInSc2	nápis
"	"	kIx"	"
<g/>
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
anni	anni	k6eAd1	anni
93	[number]	k4	93
<g/>
"	"	kIx"	"
na	na	k7c6	na
štítku	štítek	k1gInSc6	štítek
v	v	k7c6	v
houslích	housle	k1gFnPc6	housle
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
postavil	postavit	k5eAaPmAgInS	postavit
v	v	k7c6	v
r.	r.	kA	r.
1737	[number]	k4	1737
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
devadesáti	devadesát	k4xCc2	devadesát
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
jeho	on	k3xPp3gNnSc2	on
narození	narození	k1gNnSc2	narození
rovněž	rovněž	k9	rovněž
není	být	k5eNaImIp3nS	být
zdokumentováno	zdokumentován	k2eAgNnSc1d1	zdokumentováno
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
rodiči	rodič	k1gMnPc7	rodič
byli	být	k5eAaImAgMnP	být
Alessandro	Alessandra	k1gFnSc5	Alessandra
Stradivari	Stradivari	k1gNnSc1	Stradivari
a	a	k8xC	a
Anna	Anna	k1gFnSc1	Anna
Moroni	Moron	k1gMnPc1	Moron
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Cremoně	Cremona	k1gFnSc6	Cremona
<g/>
.	.	kIx.	.
</s>
<s>
Stradivariové	Stradivariový	k2eAgNnSc1d1	Stradivariový
patřili	patřit	k5eAaImAgMnP	patřit
mezi	mezi	k7c4	mezi
významné	významný	k2eAgInPc4d1	významný
Cremonské	cremonský	k2eAgInPc4d1	cremonský
rody	rod	k1gInPc4	rod
už	už	k6eAd1	už
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemských	zemský	k2eAgFnPc6d1	zemská
knihách	kniha	k1gFnPc6	kniha
z	z	k7c2	z
r.	r.	kA	r.
1188	[number]	k4	1188
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Antonio	Antonio	k1gMnSc1	Antonio
Stradivari	Stradivar	k1gFnSc2	Stradivar
se	se	k3xPyFc4	se
houslařskému	houslařský	k2eAgNnSc3d1	houslařské
řemeslu	řemeslo	k1gNnSc3	řemeslo
učil	učít	k5eAaPmAgMnS	učít
u	u	k7c2	u
Niccoly	Niccola	k1gFnSc2	Niccola
Amatiho	Amati	k1gMnSc2	Amati
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
vlastní	vlastní	k2eAgFnPc4d1	vlastní
housle	housle	k1gFnPc4	housle
postavil	postavit	k5eAaPmAgInS	postavit
v	v	k7c6	v
r.	r.	kA	r.
1665	[number]	k4	1665
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
sběratele	sběratel	k1gMnSc2	sběratel
hraběte	hrabě	k1gMnSc2	hrabě
Cozia	Cozius	k1gMnSc2	Cozius
di	di	k?	di
Salabue	Salabu	k1gMnSc2	Salabu
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
houslí	housle	k1gFnPc2	housle
vlepil	vlepit	k5eAaPmAgMnS	vlepit
nálepku	nálepka	k1gFnSc4	nálepka
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
Antonius	Antonius	k1gMnSc1	Antonius
Stradiuarius	Stradiuarius	k1gMnSc1	Stradiuarius
Cremonensis	Cremonensis	k1gFnPc2	Cremonensis
Alumnus	alumnus	k1gMnSc1	alumnus
Nicolai	Nicola	k1gFnSc2	Nicola
Amati	Amať	k1gFnSc2	Amať
Faciebat	Faciebat	k1gFnSc2	Faciebat
Anno	Anna	k1gFnSc5	Anna
1665	[number]	k4	1665
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dílně	dílna	k1gFnSc6	dílna
mistra	mistr	k1gMnSc2	mistr
Amatiho	Amati	k1gMnSc2	Amati
pracoval	pracovat	k5eAaImAgMnS	pracovat
Stradivari	Stradivari	k1gNnSc4	Stradivari
až	až	k6eAd1	až
do	do	k7c2	do
r.	r.	kA	r.
1667	[number]	k4	1667
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
vdovou	vdova	k1gFnSc7	vdova
Francescou	Francesca	k1gFnSc7	Francesca
Ferraboschiovou	Ferraboschiový	k2eAgFnSc4d1	Ferraboschiový
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
dětmi	dítě	k1gFnPc7	dítě
a	a	k8xC	a
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
domu	dům	k1gInSc2	dům
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
San	San	k1gMnSc2	San
Luca	Lucus	k1gMnSc2	Lucus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domě	dům	k1gInSc6	dům
bydleli	bydlet	k5eAaImAgMnP	bydlet
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1680	[number]	k4	1680
a	a	k8xC	a
narodilo	narodit	k5eAaPmAgNnS	narodit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
pět	pět	k4xCc4	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Stradivari	Stradivari	k6eAd1	Stradivari
nejdříve	dříve	k6eAd3	dříve
vyráběl	vyrábět	k5eAaImAgMnS	vyrábět
podobné	podobný	k2eAgInPc4d1	podobný
modely	model	k1gInPc4	model
jako	jako	k8xC	jako
Amati	Amať	k1gFnPc4	Amať
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgMnS	začít
experimentovat	experimentovat	k5eAaImF	experimentovat
s	s	k7c7	s
tloušťkou	tloušťka	k1gFnSc7	tloušťka
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
různými	různý	k2eAgInPc7d1	různý
laky	lak	k1gInPc7	lak
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
změnami	změna	k1gFnPc7	změna
v	v	k7c6	v
konstrukci	konstrukce	k1gFnSc6	konstrukce
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
nástroje	nástroj	k1gInPc1	nástroj
si	se	k3xPyFc3	se
získávaly	získávat	k5eAaImAgInP	získávat
stále	stále	k6eAd1	stále
větší	veliký	k2eAgNnSc1d2	veliký
renomé	renomé	k1gNnSc1	renomé
<g/>
.	.	kIx.	.
</s>
<s>
Rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
počet	počet	k1gInSc1	počet
zakázek	zakázka	k1gFnPc2	zakázka
od	od	k7c2	od
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
známých	známý	k2eAgMnPc2d1	známý
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc2	jeho
píle	píle	k1gFnSc2	píle
a	a	k8xC	a
skromnost	skromnost	k1gFnSc4	skromnost
mu	on	k3xPp3gMnSc3	on
přinesly	přinést	k5eAaPmAgFnP	přinést
značné	značný	k2eAgNnSc4d1	značné
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1680	[number]	k4	1680
Stradivari	Stradivar	k1gFnSc2	Stradivar
koupil	koupit	k5eAaPmAgInS	koupit
dům	dům	k1gInSc1	dům
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
No	no	k9	no
<g/>
.	.	kIx.	.
2	[number]	k4	2
Piazza	Piazza	k1gFnSc1	Piazza
Dan	Dana	k1gFnPc2	Dana
Domenico	Domenico	k6eAd1	Domenico
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
No	no	k9	no
<g/>
.	.	kIx.	.
1	[number]	k4	1
Piazza	Piazz	k1gMnSc2	Piazz
Roma	Rom	k1gMnSc2	Rom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
domů	dům	k1gInPc2	dům
rodiny	rodina	k1gFnSc2	rodina
Amati	Amati	k1gNnSc2	Amati
a	a	k8xC	a
Guarneri	Guarneri	k1gNnSc2	Guarneri
<g/>
,	,	kIx,	,
dalších	další	k2eAgMnPc2d1	další
známých	známý	k2eAgMnPc2d1	známý
houslařů	houslař	k1gMnPc2	houslař
v	v	k7c6	v
Cremoně	Cremona	k1gFnSc6	Cremona
<g/>
.	.	kIx.	.
</s>
<s>
Stradivari	Stradivari	k6eAd1	Stradivari
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
používal	používat	k5eAaImAgInS	používat
i	i	k9	i
jako	jako	k9	jako
dílnu	dílna	k1gFnSc4	dílna
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Francesca	Francesc	k2eAgFnSc1d1	Francesca
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
r.	r.	kA	r.
1698	[number]	k4	1698
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	s	k7c7	s
Stradivari	Stradivar	k1gMnPc7	Stradivar
oženil	oženit	k5eAaPmAgMnS	oženit
podruhé	podruhé	k6eAd1	podruhé
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Antonií	Antonie	k1gFnSc7	Antonie
Zambelli	Zambell	k1gMnPc1	Zambell
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgFnSc1d2	mladší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svatby	svatba	k1gFnSc2	svatba
jí	on	k3xPp3gFnSc2	on
bylo	být	k5eAaImAgNnS	být
35	[number]	k4	35
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
r.	r.	kA	r.
1700	[number]	k4	1700
do	do	k7c2	do
r.	r.	kA	r.
1708	[number]	k4	1708
měli	mít	k5eAaImAgMnP	mít
pět	pět	k4xCc4	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
zemřela	zemřít	k5eAaPmAgFnS	zemřít
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1737	[number]	k4	1737
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Antonio	Antonio	k1gMnSc1	Antonio
Stradivari	Stradivar	k1gFnSc2	Stradivar
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
93	[number]	k4	93
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Je	být	k5eAaImIp3nS	být
pohřbený	pohřbený	k2eAgMnSc1d1	pohřbený
v	v	k7c6	v
rodinné	rodinný	k2eAgFnSc6d1	rodinná
hrobce	hrobka	k1gFnSc6	hrobka
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
San	San	k1gMnSc1	San
Domenico	Domenico	k1gMnSc1	Domenico
v	v	k7c6	v
Cremoně	Cremona	k1gFnSc6	Cremona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
postavil	postavit	k5eAaPmAgMnS	postavit
Stradivari	Stradivare	k1gFnSc4	Stradivare
asi	asi	k9	asi
1100	[number]	k4	1100
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
přibližně	přibližně	k6eAd1	přibližně
500	[number]	k4	500
houslí	housle	k1gFnPc2	housle
<g/>
,	,	kIx,	,
20	[number]	k4	20
viol	viola	k1gFnPc2	viola
<g/>
,	,	kIx,	,
50	[number]	k4	50
violoncell	violoncello	k1gNnPc2	violoncello
a	a	k8xC	a
několik	několik	k4yIc1	několik
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
mandolína	mandolína	k1gFnSc1	mandolína
<g/>
,	,	kIx,	,
malá	malý	k2eAgFnSc1d1	malá
harfa	harfa	k1gFnSc1	harfa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gInPc1	jeho
nástroje	nástroj	k1gInPc1	nástroj
nesou	nést	k5eAaImIp3nP	nést
označení	označení	k1gNnSc4	označení
</s>
</p>
<p>
<s>
Antonius	Antonius	k1gMnSc1	Antonius
Stradivarius	Stradivarius	k1gMnSc1	Stradivarius
Cremonensis	Cremonensis	k1gFnPc2	Cremonensis
Faciebat	Faciebat	k1gMnSc1	Faciebat
Anno	Anna	k1gFnSc5	Anna
[	[	kIx(	[
<g/>
rok	rok	k1gInSc4	rok
<g/>
]	]	kIx)	]
<g/>
Svá	svůj	k3xOyFgNnPc4	svůj
nejlepší	dobrý	k2eAgNnPc4d3	nejlepší
díla	dílo	k1gNnPc4	dílo
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1698	[number]	k4	1698
–	–	k?	–
1725	[number]	k4	1725
<g/>
.	.	kIx.	.
</s>
<s>
Nástroje	nástroj	k1gInPc1	nástroj
vyrobené	vyrobený	k2eAgInPc1d1	vyrobený
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1730	[number]	k4	1730
již	již	k6eAd1	již
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nevytvořil	vytvořit	k5eNaPmAgMnS	vytvořit
Antonio	Antonio	k1gMnSc1	Antonio
Stradivari	Stradivar	k1gFnSc2	Stradivar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
Francesco	Francesco	k6eAd1	Francesco
(	(	kIx(	(
<g/>
1671	[number]	k4	1671
–	–	k?	–
1743	[number]	k4	1743
<g/>
)	)	kIx)	)
a	a	k8xC	a
Omobono	Omobona	k1gFnSc5	Omobona
(	(	kIx(	(
<g/>
1679	[number]	k4	1679
–	–	k?	–
1742	[number]	k4	1742
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
signovány	signován	k2eAgFnPc1d1	signována
</s>
</p>
<p>
<s>
Sotto	Sott	k2eAgNnSc1d1	Sotto
la	la	k1gNnSc1	la
Desciplina	Desciplin	k2eAgInSc2d1	Desciplin
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Antonio	Antonio	k1gMnSc1	Antonio
Stradivari	Stradivar	k1gFnSc2	Stradivar
F.	F.	kA	F.
in	in	k?	in
Cremona	Cremona	k1gFnSc1	Cremona
[	[	kIx(	[
<g/>
rok	rok	k1gInSc1	rok
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Pozůstalost	pozůstalost	k1gFnSc4	pozůstalost
==	==	k?	==
</s>
</p>
<p>
<s>
Stradivariho	Stradivarize	k6eAd1	Stradivarize
synové	syn	k1gMnPc1	syn
Francesco	Francesco	k1gMnSc1	Francesco
a	a	k8xC	a
Omobono	Omobona	k1gFnSc5	Omobona
převzali	převzít	k5eAaPmAgMnP	převzít
dílnu	dílna	k1gFnSc4	dílna
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
přežili	přežít	k5eAaPmAgMnP	přežít
jen	jen	k6eAd1	jen
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
smrti	smrt	k1gFnSc6	smrt
dílnu	dílna	k1gFnSc4	dílna
přeměnil	přeměnit	k5eAaPmAgMnS	přeměnit
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
syn	syn	k1gMnSc1	syn
Antonia	Antonio	k1gMnSc2	Antonio
Stradivariho	Stradivari	k1gMnSc2	Stradivari
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
byl	být	k5eAaImAgMnS	být
obchodník	obchodník	k1gMnSc1	obchodník
se	s	k7c7	s
suknem	sukno	k1gNnSc7	sukno
<g/>
,	,	kIx,	,
na	na	k7c4	na
sklad	sklad	k1gInSc4	sklad
sukna	sukno	k1gNnSc2	sukno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1774	[number]	k4	1774
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
pozůstalost	pozůstalost	k1gFnSc4	pozůstalost
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
(	(	kIx(	(
<g/>
nářadí	nářadí	k1gNnPc4	nářadí
<g/>
,	,	kIx,	,
šablony	šablona	k1gFnPc4	šablona
<g/>
,	,	kIx,	,
modely	model	k1gInPc4	model
<g/>
,	,	kIx,	,
formy	forma	k1gFnPc4	forma
<g/>
)	)	kIx)	)
včetně	včetně	k7c2	včetně
řady	řada	k1gFnSc2	řada
nástrojů	nástroj	k1gInPc2	nástroj
městu	město	k1gNnSc3	město
Cremona	Cremon	k1gMnSc2	Cremon
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
však	však	k9	však
nemělo	mít	k5eNaImAgNnS	mít
dostatek	dostatek	k1gInSc4	dostatek
prostředků	prostředek	k1gInPc2	prostředek
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
odkoupení	odkoupení	k1gNnSc4	odkoupení
<g/>
.	.	kIx.	.
</s>
<s>
Majitelem	majitel	k1gMnSc7	majitel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
sběratel	sběratel	k1gMnSc1	sběratel
hrabě	hrabě	k1gMnSc1	hrabě
Cozio	Cozio	k1gMnSc1	Cozio
de	de	k?	de
Salabue	Salabue	k1gInSc1	Salabue
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
sbírku	sbírka	k1gFnSc4	sbírka
známou	známý	k2eAgFnSc4d1	známá
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Collezione	Collezion	k1gInSc5	Collezion
Salabue	Salabue	k1gNnPc7	Salabue
<g/>
"	"	kIx"	"
zdědil	zdědit	k5eAaPmAgMnS	zdědit
markýz	markýz	k1gMnSc1	markýz
Rolande	Roland	k1gInSc5	Roland
della	della	k6eAd1	della
Valle	Vall	k1gMnSc2	Vall
di	di	k?	di
Torino	Torino	k1gNnSc1	Torino
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
dále	daleko	k6eAd2	daleko
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
a	a	k8xC	a
katalogizoval	katalogizovat	k5eAaImAgInS	katalogizovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
sbírku	sbírka	k1gFnSc4	sbírka
čítající	čítající	k2eAgFnSc4d1	čítající
1303	[number]	k4	1303
kusů	kus	k1gInPc2	kus
odkoupil	odkoupit	k5eAaPmAgMnS	odkoupit
známý	známý	k2eAgMnSc1d1	známý
houslař	houslař	k1gMnSc1	houslař
Giuseppe	Giusepp	k1gInSc5	Giusepp
Fiorini	Fiorin	k2eAgMnPc1d1	Fiorin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ji	on	k3xPp3gFnSc4	on
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
věnoval	věnovat	k5eAaImAgMnS	věnovat
muzeu	muzeum	k1gNnSc3	muzeum
v	v	k7c6	v
Cremoně	Cremona	k1gFnSc6	Cremona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Stradivariho	Stradivarize	k6eAd1	Stradivarize
tvorbu	tvorba	k1gFnSc4	tvorba
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
období	období	k1gNnPc2	období
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
r.	r.	kA	r.
1665	[number]	k4	1665
až	až	k6eAd1	až
1686	[number]	k4	1686
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
housle	housle	k1gFnPc4	housle
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
svého	svůj	k3xOyFgMnSc4	svůj
učitele	učitel	k1gMnSc4	učitel
Nicola	Nicola	k1gFnSc1	Nicola
Amatiho	Amati	k1gMnSc2	Amati
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgMnPc3	tento
nástrojům	nástroj	k1gInPc3	nástroj
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
amatisé	amatisý	k2eAgNnSc1d1	amatisý
<g/>
"	"	kIx"	"
.	.	kIx.	.
</s>
<s>
Zachovalo	zachovat	k5eAaPmAgNnS	zachovat
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
jen	jen	k6eAd1	jen
několik	několik	k4yIc4	několik
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
housle	housle	k1gFnPc4	housle
z	z	k7c2	z
r.	r.	kA	r.
1683	[number]	k4	1683
hrával	hrávat	k5eAaImAgMnS	hrávat
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
kvartetu	kvartet	k1gInSc6	kvartet
Josef	Josef	k1gMnSc1	Josef
Suk	Suk	k1gMnSc1	Suk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1686	[number]	k4	1686
<g/>
–	–	k?	–
<g/>
1700	[number]	k4	1700
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nazývaném	nazývaný	k2eAgInSc6d1	nazývaný
dobou	doba	k1gFnSc7	doba
experimentů	experiment	k1gInPc2	experiment
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgInS	dát
svým	svůj	k3xOyFgInPc3	svůj
nástrojům	nástroj	k1gInPc3	nástroj
nové	nový	k2eAgFnSc2d1	nová
proporce	proporce	k1gFnSc2	proporce
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc1	housle
dostaly	dostat	k5eAaPmAgFnP	dostat
větší	veliký	k2eAgInSc4d2	veliký
rozměr	rozměr	k1gInSc4	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
protáhlý	protáhlý	k2eAgInSc4d1	protáhlý
<g/>
,	,	kIx,	,
štíhlý	štíhlý	k2eAgInSc4d1	štíhlý
tvar	tvar	k1gInSc4	tvar
bývají	bývat	k5eAaImIp3nP	bývat
nazývány	nazýván	k2eAgFnPc1d1	nazývána
"	"	kIx"	"
<g/>
allongé	allongá	k1gFnPc1	allongá
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
podlouhlé	podlouhlý	k2eAgNnSc1d1	podlouhlé
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
používal	používat	k5eAaImAgInS	používat
tmavší	tmavý	k2eAgInSc1d2	tmavší
<g/>
,	,	kIx,	,
bohatější	bohatý	k2eAgInSc1d2	bohatší
lak	lak	k1gInSc1	lak
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
světlejší	světlý	k2eAgFnSc2d2	světlejší
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgFnSc2d1	žlutá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
používané	používaný	k2eAgFnSc2d1	používaná
Amatim	Amatim	k1gInSc4	Amatim
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
housle	housle	k1gFnPc1	housle
mají	mít	k5eAaImIp3nP	mít
jasný	jasný	k2eAgInSc4d1	jasný
zvuk	zvuk	k1gInSc4	zvuk
s	s	k7c7	s
charakteristickým	charakteristický	k2eAgNnSc7d1	charakteristické
spodním	spodní	k2eAgNnSc7d1	spodní
temnějším	temný	k2eAgNnSc7d2	temnější
zabarvením	zabarvení	k1gNnSc7	zabarvení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
housle	housle	k1gFnPc4	housle
z	z	k7c2	z
r.	r.	kA	r.
1687	[number]	k4	1687
hrával	hrávat	k5eAaImAgInS	hrávat
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
Jan	Jan	k1gMnSc1	Jan
Kubelík	Kubelík	k1gMnSc1	Kubelík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vrcholným	vrcholný	k2eAgNnSc7d1	vrcholné
obdobím	období	k1gNnSc7	období
<g/>
,	,	kIx,	,
označovaným	označovaný	k2eAgNnSc7d1	označované
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
zlatý	zlatý	k2eAgInSc4d1	zlatý
věk	věk	k1gInSc4	věk
<g/>
"	"	kIx"	"
Stradivariho	Stradivari	k1gMnSc2	Stradivari
tvorby	tvorba	k1gFnSc2	tvorba
jsou	být	k5eAaImIp3nP	být
léta	léto	k1gNnPc4	léto
1700	[number]	k4	1700
až	až	k8xS	až
1725	[number]	k4	1725
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
tvarově	tvarově	k6eAd1	tvarově
i	i	k9	i
zvukově	zvukově	k6eAd1	zvukově
dokonalé	dokonalý	k2eAgInPc1d1	dokonalý
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
velké	velký	k2eAgFnPc1d1	velká
stradivárky	stradivárky	k1gFnPc1	stradivárky
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
těchto	tento	k3xDgInPc2	tento
nástrojů	nástroj	k1gInPc2	nástroj
je	být	k5eAaImIp3nS	být
zúžený	zúžený	k2eAgInSc1d1	zúžený
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgFnSc1d1	dolní
část	část	k1gFnSc1	část
korpusu	korpus	k1gInSc2	korpus
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
překvapivě	překvapivě	k6eAd1	překvapivě
široká	široký	k2eAgFnSc1d1	široká
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tyto	tento	k3xDgInPc4	tento
shodné	shodný	k2eAgInPc4d1	shodný
atributy	atribut	k1gInPc4	atribut
měl	mít	k5eAaImAgInS	mít
každý	každý	k3xTgInSc1	každý
nástroj	nástroj	k1gInSc1	nástroj
své	svůj	k3xOyFgNnSc4	svůj
specifické	specifický	k2eAgNnSc4d1	specifické
provedení	provedení	k1gNnSc4	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
znalec	znalec	k1gMnSc1	znalec
dřeva	dřevo	k1gNnSc2	dřevo
měl	mít	k5eAaImAgMnS	mít
Stradivari	Stradivare	k1gFnSc4	Stradivare
geniální	geniální	k2eAgFnSc4d1	geniální
schopnost	schopnost	k1gFnSc4	schopnost
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
každý	každý	k3xTgInSc4	každý
materiál	materiál	k1gInSc4	materiál
své	svůj	k3xOyFgFnSc2	svůj
zvukové	zvukový	k2eAgFnSc2d1	zvuková
představě	představa	k1gFnSc3	představa
a	a	k8xC	a
opracovat	opracovat	k5eAaPmF	opracovat
ho	on	k3xPp3gMnSc4	on
přesně	přesně	k6eAd1	přesně
podle	podle	k7c2	podle
rezonančních	rezonanční	k2eAgFnPc2d1	rezonanční
schopností	schopnost	k1gFnPc2	schopnost
a	a	k8xC	a
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Nástroje	nástroj	k1gInPc1	nástroj
se	se	k3xPyFc4	se
lišily	lišit	k5eAaImAgInP	lišit
jak	jak	k6eAd1	jak
v	v	k7c6	v
tloušťce	tloušťka	k1gFnSc6	tloušťka
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
kresbách	kresba	k1gFnPc6	kresba
modelů	model	k1gInPc2	model
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdokonalejší	dokonalý	k2eAgFnPc4d3	nejdokonalejší
patří	patřit	k5eAaImIp3nP	patřit
housle	housle	k1gFnPc1	housle
Betts-strad	Bettstrada	k1gFnPc2	Betts-strada
z	z	k7c2	z
r.	r.	kA	r.
1704	[number]	k4	1704
<g/>
,	,	kIx,	,
Laubovy	Laubův	k2eAgFnPc4d1	Laubova
housle	housle	k1gFnPc4	housle
z	z	k7c2	z
r.	r.	kA	r.
1705	[number]	k4	1705
nebo	nebo	k8xC	nebo
Composelice-strad	Composelicetrad	k1gInSc1	Composelice-strad
z	z	k7c2	z
r.	r.	kA	r.
1710	[number]	k4	1710
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgInPc4	který
hrál	hrát	k5eAaImAgInS	hrát
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
Váša	Váša	k1gMnSc1	Váša
Příhoda	Příhoda	k1gMnSc1	Příhoda
<g/>
,	,	kIx,	,
Sancy-strad	Sancytrad	k1gInSc1	Sancy-strad
(	(	kIx(	(
<g/>
1713	[number]	k4	1713
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Delphin-strad	Delphintrad	k1gInSc1	Delphin-strad
(	(	kIx(	(
<g/>
1714	[number]	k4	1714
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kubelíkovy	Kubelíkův	k2eAgFnPc4d1	Kubelíkova
housle	housle	k1gFnPc4	housle
Emperor	Emperora	k1gFnPc2	Emperora
z	z	k7c2	z
r.	r.	kA	r.
1715	[number]	k4	1715
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
období	období	k1gNnSc6	období
po	po	k7c6	po
r.	r.	kA	r.
1725	[number]	k4	1725
vznikají	vznikat	k5eAaImIp3nP	vznikat
stále	stále	k6eAd1	stále
mistrovské	mistrovský	k2eAgInPc4d1	mistrovský
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
práce	práce	k1gFnPc1	práce
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
jako	jako	k8xS	jako
u	u	k7c2	u
předchozích	předchozí	k2eAgInPc2d1	předchozí
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
vyšší	vysoký	k2eAgFnSc1d2	vyšší
klenba	klenba	k1gFnSc1	klenba
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemají	mít	k5eNaImIp3nP	mít
tak	tak	k6eAd1	tak
jasný	jasný	k2eAgInSc4d1	jasný
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
známé	známá	k1gFnPc1	známá
Stradivariho	Stradivari	k1gMnSc2	Stradivari
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
r.	r.	kA	r.
1737	[number]	k4	1737
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
poetický	poetický	k2eAgInSc4d1	poetický
a	a	k8xC	a
nostalgický	nostalgický	k2eAgInSc4d1	nostalgický
název	název	k1gInSc4	název
Labutí	labutí	k2eAgFnSc1d1	labutí
píseň	píseň	k1gFnSc1	píseň
<g/>
.	.	kIx.	.
<g/>
Stradivariho	Stradivari	k1gMnSc2	Stradivari
nástroje	nástroj	k1gInSc2	nástroj
se	se	k3xPyFc4	se
stáler	stáler	k1gInSc1	stáler
těší	těšit	k5eAaImIp3nS	těšit
neobyčejné	obyčejný	k2eNgFnSc3d1	neobyčejná
popularitě	popularita	k1gFnSc3	popularita
a	a	k8xC	a
prodávají	prodávat	k5eAaImIp3nP	prodávat
se	se	k3xPyFc4	se
za	za	k7c4	za
extrémně	extrémně	k6eAd1	extrémně
vysoké	vysoký	k2eAgFnPc4d1	vysoká
ceny	cena	k1gFnPc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
newyorské	newyorský	k2eAgFnSc6d1	newyorská
aukci	aukce	k1gFnSc6	aukce
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2006	[number]	k4	2006
se	se	k3xPyFc4	se
prodaly	prodat	k5eAaPmAgFnP	prodat
housle	housle	k1gFnPc1	housle
"	"	kIx"	"
<g/>
Hammer	Hammer	k1gInSc1	Hammer
Stradivarius	Stradivarius	k1gInSc1	Stradivarius
<g/>
"	"	kIx"	"
za	za	k7c4	za
rekordní	rekordní	k2eAgFnSc4d1	rekordní
cenu	cena	k1gFnSc4	cena
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
přes	přes	k7c4	přes
70	[number]	k4	70
miliónů	milión	k4xCgInPc2	milión
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
veřejně	veřejně	k6eAd1	veřejně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
sbírka	sbírka	k1gFnSc1	sbírka
Stradivariho	Stradivari	k1gMnSc2	Stradivari
nástrojů	nástroj	k1gInPc2	nástroj
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
španělském	španělský	k2eAgInSc6d1	španělský
královském	královský	k2eAgInSc6d1	královský
paláci	palác	k1gInSc6	palác
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
dvoje	dvoje	k4xRgFnPc1	dvoje
housle	housle	k1gFnPc1	housle
<g/>
,	,	kIx,	,
viola	viola	k1gFnSc1	viola
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
violoncella	violoncello	k1gNnPc4	violoncello
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přesné	přesný	k2eAgInPc1d1	přesný
postupy	postup	k1gInPc1	postup
při	při	k7c6	při
konstrukci	konstrukce	k1gFnSc6	konstrukce
Stradivariho	Stradivari	k1gMnSc4	Stradivari
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
složení	složení	k1gNnSc4	složení
laku	lak	k1gInSc2	lak
<g/>
,	,	kIx,	,
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
dodnes	dodnes	k6eAd1	dodnes
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
provedeno	provést	k5eAaPmNgNnS	provést
nespočet	nespočet	k1gInSc1	nespočet
výzkumů	výzkum	k1gInPc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Housle	housle	k1gFnPc1	housle
Cremonských	cremonský	k2eAgMnPc2d1	cremonský
mistrů	mistr	k1gMnPc2	mistr
(	(	kIx(	(
<g/>
Stradivari	Stradivari	k1gNnSc1	Stradivari
<g/>
,	,	kIx,	,
rodina	rodina	k1gFnSc1	rodina
Amati	Amati	k1gNnSc1	Amati
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
Guarneri	Guarner	k1gFnSc2	Guarner
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
údajně	údajně	k6eAd1	údajně
nepřekonané	překonaný	k2eNgNnSc1d1	nepřekonané
v	v	k7c6	v
kráse	krása	k1gFnSc6	krása
tónu	tón	k1gInSc2	tón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stradivariho	Stradivarize	k6eAd1	Stradivarize
housle	housle	k1gFnPc1	housle
mají	mít	k5eAaImIp3nP	mít
překrásný	překrásný	k2eAgInSc4d1	překrásný
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
frekvenčním	frekvenční	k2eAgNnSc6d1	frekvenční
pásmu	pásmo	k1gNnSc6	pásmo
2000	[number]	k4	2000
–	–	k?	–
4000	[number]	k4	4000
Hz	Hz	kA	Hz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
lidské	lidský	k2eAgNnSc1d1	lidské
ucho	ucho	k1gNnSc1	ucho
nejcitlivější	citlivý	k2eAgNnSc1d3	nejcitlivější
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
teorií	teorie	k1gFnPc2	teorie
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ke	k	k7c3	k
krásné	krásný	k2eAgFnSc3d1	krásná
barvě	barva	k1gFnSc3	barva
tónu	tón	k1gInSc2	tón
přispělo	přispět	k5eAaPmAgNnS	přispět
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
studené	studený	k2eAgNnSc1d1	studené
klima	klima	k1gNnSc1	klima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
16	[number]	k4	16
<g/>
.	.	kIx.	.
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
panovala	panovat	k5eAaImAgFnS	panovat
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentě	kontinent	k1gInSc6	kontinent
tzv.	tzv.	kA	tzv.
malá	malý	k2eAgFnSc1d1	malá
doba	doba	k1gFnSc1	doba
ledová	ledový	k2eAgFnSc1d1	ledová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
přímý	přímý	k2eAgInSc4d1	přímý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
dřeva	dřevo	k1gNnSc2	dřevo
–	–	k?	–
nižší	nízký	k2eAgFnPc1d2	nižší
teploty	teplota	k1gFnPc1	teplota
zapříčiňovaly	zapříčiňovat	k5eAaImAgFnP	zapříčiňovat
pomalejší	pomalý	k2eAgInSc4d2	pomalejší
růst	růst	k1gInSc4	růst
stromů	strom	k1gInPc2	strom
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
užší	úzký	k2eAgInPc4d2	užší
letokruhy	letokruh	k1gInPc4	letokruh
<g/>
)	)	kIx)	)
a	a	k8xC	a
slabší	slabý	k2eAgInPc4d2	slabší
buněčné	buněčný	k2eAgInPc4d1	buněčný
stěny	stěn	k1gInPc4	stěn
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
neobyčejného	obyčejný	k2eNgInSc2d1	neobyčejný
zvuku	zvuk	k1gInSc2	zvuk
Stradivariho	Stradivari	k1gMnSc2	Stradivari
houslí	houslit	k5eAaImIp3nS	houslit
ale	ale	k8xC	ale
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
použitém	použitý	k2eAgNnSc6d1	Použité
dřevě	dřevo	k1gNnSc6	dřevo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
stejné	stejné	k1gNnSc1	stejné
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
i	i	k9	i
ostatním	ostatní	k2eAgMnPc3d1	ostatní
evropským	evropský	k2eAgMnPc3d1	evropský
nástrojařům	nástrojař	k1gMnPc3	nástrojař
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgFnPc2d1	jiná
teorií	teorie	k1gFnPc2	teorie
k	k	k7c3	k
výjimečné	výjimečný	k2eAgFnSc3d1	výjimečná
kvalitě	kvalita	k1gFnSc3	kvalita
tónu	tón	k1gInSc2	tón
přispívají	přispívat	k5eAaImIp3nP	přispívat
malé	malý	k2eAgFnPc4d1	malá
asymetrické	asymetrický	k2eAgFnPc4d1	asymetrická
odchylky	odchylka	k1gFnPc4	odchylka
použitého	použitý	k2eAgNnSc2d1	Použité
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
ale	ale	k9	ale
také	také	k9	také
jít	jít	k5eAaImF	jít
čistě	čistě	k6eAd1	čistě
o	o	k7c4	o
subjektivní	subjektivní	k2eAgInSc4d1	subjektivní
vliv	vliv	k1gInSc4	vliv
značky	značka	k1gFnSc2	značka
(	(	kIx(	(
<g/>
mýtus	mýtus	k1gInSc1	mýtus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
posluchačů	posluchač	k1gMnPc2	posluchač
jejich	jejich	k3xOp3gFnSc4	jejich
zvuk	zvuk	k1gInSc1	zvuk
nepreferuje	preferovat	k5eNaImIp3nS	preferovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc1	přehled
Stradivariho	Stradivari	k1gMnSc2	Stradivari
nástrojů	nástroj	k1gInPc2	nástroj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Housle	housle	k1gFnPc4	housle
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Violy	Viola	k1gFnSc2	Viola
===	===	k?	===
</s>
</p>
<p>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
13	[number]	k4	13
Stradivariho	Stradivari	k1gMnSc4	Stradivari
viol	viola	k1gFnPc2	viola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Violoncella	violoncello	k1gNnSc2	violoncello
===	===	k?	===
</s>
</p>
<p>
<s>
Antonio	Antonio	k1gMnSc1	Antonio
Stradivari	Stradivar	k1gFnSc2	Stradivar
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
mezi	mezi	k7c7	mezi
70	[number]	k4	70
a	a	k8xC	a
80	[number]	k4	80
violoncelly	violoncello	k1gNnPc7	violoncello
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kytary	kytara	k1gFnSc2	kytara
===	===	k?	===
</s>
</p>
<p>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
jen	jen	k9	jen
dvě	dva	k4xCgFnPc1	dva
Stradivariho	Stradivari	k1gMnSc2	Stradivari
kytary	kytara	k1gFnSc2	kytara
<g/>
,	,	kIx,	,
z	z	k7c2	z
ostatních	ostatní	k2eAgInPc2d1	ostatní
zbyly	zbýt	k5eAaPmAgInP	zbýt
jen	jen	k9	jen
fragmenty	fragment	k1gInPc1	fragment
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Antonio	Antonio	k1gMnSc1	Antonio
Stradivari	Stradivar	k1gFnPc4	Stradivar
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Antonio	Antonio	k1gMnSc1	Antonio
Stradivari	Stradivar	k1gFnSc2	Stradivar
/	/	kIx~	/
Stradivarius	Stradivarius	k1gInSc1	Stradivarius
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
<g/>
http://eod.vkol.cz/606454/606454.pdf	[url]	k1gInSc1	http://eod.vkol.cz/606454/606454.pdf
</s>
</p>
