<s>
Niedercunnersdorf	Niedercunnersdorf	k1gMnSc1
</s>
<s>
Niedercunnersdorf	Niedercunnersdorf	k1gMnSc1
KostelPoloha	KostelPoloh	k1gMnSc2
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
51	#num#	k4
<g/>
°	°	k?
<g/>
2	#num#	k4
<g/>
′	′	k?
<g/>
59	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
39	#num#	k4
<g/>
′	′	k?
<g/>
39	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
348	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
SEČ	SEČ	kA
<g/>
/	/	kIx~
<g/>
SELČ	SELČ	kA
Stát	stát	k1gInSc1
</s>
<s>
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc4
Spolková	spolkový	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Sasko	Sasko	k1gNnSc4
Zemský	zemský	k2eAgInSc1d1
okres	okres	k1gInSc1
</s>
<s>
Zhořelec	Zhořelec	k1gInSc1
Obec	obec	k1gFnSc1
</s>
<s>
Kottmar	Kottmar	k1gMnSc1
</s>
<s>
Niedercunnersdorf	Niedercunnersdorf	k1gMnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
14,2	14,2	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
1	#num#	k4
008	#num#	k4
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
71,1	71,1	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
místní	místní	k2eAgFnSc1d1
část	část	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.niedercunnersdorf.eu	www.niedercunnersdorf.eu	k6eAd1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
035875	#num#	k4
PSČ	PSČ	kA
</s>
<s>
02708	#num#	k4
Označení	označení	k1gNnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
GR	GR	kA
<g/>
,	,	kIx,
LÖB	LÖB	kA
<g/>
,	,	kIx,
NOL	NOL	kA
<g/>
,	,	kIx,
NY	NY	kA
<g/>
,	,	kIx,
WSW	WSW	kA
<g/>
,	,	kIx,
ZI	ZI	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Niedercunnersdorf	Niedercunnersdorf	k1gInSc1
je	být	k5eAaImIp3nS
bývalá	bývalý	k2eAgFnSc1d1
obec	obec	k1gFnSc1
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
Sasku	Sasko	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
Horní	horní	k2eAgFnSc6d1
Lužici	Lužice	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
zemském	zemský	k2eAgInSc6d1
okrese	okres	k1gInSc6
Zhořelec	Zhořelec	k1gInSc1
<g/>
,	,	kIx,
poblíž	poblíž	k6eAd1
hranic	hranice	k1gFnPc2
s	s	k7c7
Českou	český	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
1	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
obci	obec	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1221	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Gemeinde	Gemeind	k1gInSc5
Kottmar	Kottmar	k1gInSc1
-	-	kIx~
Zahlen	Zahlen	k2eAgInSc1d1
und	und	k?
Fakten	Faktno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Niedercunnersdorf	Niedercunnersdorf	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
16037252-5	16037252-5	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
151167754	#num#	k4
</s>
