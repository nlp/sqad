<s>
Software	software	k1gInSc1	software
(	(	kIx(	(
<g/>
též	též	k9	též
programové	programový	k2eAgNnSc4d1	programové
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
sada	sada	k1gFnSc1	sada
všech	všecek	k3xTgInPc2	všecek
počítačových	počítačový	k2eAgInPc2d1	počítačový
programů	program	k1gInPc2	program
používaných	používaný	k2eAgInPc2d1	používaný
v	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
provádějí	provádět	k5eAaImIp3nP	provádět
nějakou	nějaký	k3yIgFnSc4	nějaký
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Software	software	k1gInSc1	software
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
systémový	systémový	k2eAgInSc4d1	systémový
software	software	k1gInSc4	software
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
chod	chod	k1gInSc4	chod
samotného	samotný	k2eAgInSc2d1	samotný
počítače	počítač	k1gInSc2	počítač
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
styk	styk	k1gInSc4	styk
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
a	a	k8xC	a
na	na	k7c4	na
aplikační	aplikační	k2eAgInSc4d1	aplikační
software	software	k1gInSc4	software
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgMnSc7	který
buď	buď	k8xC	buď
pracuje	pracovat	k5eAaImIp3nS	pracovat
uživatel	uživatel	k1gMnSc1	uživatel
počítače	počítač	k1gInSc2	počítač
nebo	nebo	k8xC	nebo
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
řízení	řízení	k1gNnSc4	řízení
nějakého	nějaký	k3yIgInSc2	nějaký
stroje	stroj	k1gInSc2	stroj
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
embedded	embedded	k1gInSc1	embedded
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Software	software	k1gInSc1	software
je	být	k5eAaImIp3nS	být
protiklad	protiklad	k1gInSc4	protiklad
k	k	k7c3	k
hardwaru	hardware	k1gInSc3	hardware
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
všechny	všechen	k3xTgFnPc4	všechen
fyzické	fyzický	k2eAgFnPc4d1	fyzická
součásti	součást	k1gFnPc4	součást
počítače	počítač	k1gInSc2	počítač
(	(	kIx(	(
<g/>
elektronické	elektronický	k2eAgInPc1d1	elektronický
obvody	obvod	k1gInPc1	obvod
<g/>
,	,	kIx,	,
skříň	skříň	k1gFnSc1	skříň
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
software	software	k1gInSc1	software
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
autorské	autorský	k2eAgNnSc4d1	autorské
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
koncoví	koncový	k2eAgMnPc1d1	koncový
uživatelé	uživatel	k1gMnPc1	uživatel
ho	on	k3xPp3gInSc4	on
využívají	využívat	k5eAaPmIp3nP	využívat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
licencí	licence	k1gFnPc2	licence
od	od	k7c2	od
jejich	jejich	k3xOp3gMnPc2	jejich
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
teorie	teorie	k1gFnSc1	teorie
softwaru	software	k1gInSc2	software
byla	být	k5eAaImAgFnS	být
navržena	navržen	k2eAgFnSc1d1	navržena
Alanem	alan	k1gInSc7	alan
Turingem	Turing	k1gInSc7	Turing
v	v	k7c6	v
eseji	esej	k1gFnSc6	esej
Computable	Computable	k1gMnSc1	Computable
Numbers	Numbersa	k1gFnPc2	Numbersa
with	with	k1gMnSc1	with
an	an	k?	an
Application	Application	k1gInSc1	Application
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Entscheidungsproblem	Entscheidungsprobl	k1gInSc7	Entscheidungsprobl
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
software	software	k1gInSc1	software
<g/>
"	"	kIx"	"
v	v	k7c6	v
textu	text	k1gInSc6	text
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
John	John	k1gMnSc1	John
W.	W.	kA	W.
Tukey	Tukea	k1gFnSc2	Tukea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hovorové	hovorový	k2eAgFnSc6d1	hovorová
řeči	řeč	k1gFnSc6	řeč
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
aplikační	aplikační	k2eAgInSc4d1	aplikační
software	software	k1gInSc4	software
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
a	a	k8xC	a
softwarovém	softwarový	k2eAgNnSc6d1	softwarové
inženýrství	inženýrství	k1gNnSc6	inženýrství
pojem	pojem	k1gInSc1	pojem
software	software	k1gInSc1	software
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
všechny	všechen	k3xTgFnPc4	všechen
informace	informace	k1gFnPc4	informace
zpracované	zpracovaný	k2eAgFnPc4d1	zpracovaná
počítačovým	počítačový	k2eAgInSc7d1	počítačový
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
programy	program	k1gInPc4	program
a	a	k8xC	a
data	datum	k1gNnPc4	datum
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
obory	obor	k1gInPc7	obor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
softwarem	software	k1gInSc7	software
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
informatika	informatika	k1gFnSc1	informatika
a	a	k8xC	a
softwarové	softwarový	k2eAgNnSc1d1	softwarové
inženýrství	inženýrství	k1gNnSc1	inženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
počítačového	počítačový	k2eAgInSc2d1	počítačový
softwaru	software	k1gInSc2	software
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
psát	psát	k5eAaImF	psát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
první	první	k4xOgFnSc1	první
programová	programový	k2eAgFnSc1d1	programová
chyba	chyba	k1gFnSc1	chyba
softwaru	software	k1gInSc2	software
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
a	a	k8xC	a
více	hodně	k6eAd2	hodně
programů	program	k1gInPc2	program
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
sféry	sféra	k1gFnSc2	sféra
firmwaru	firmware	k1gInSc2	firmware
a	a	k8xC	a
hardware	hardware	k1gInSc1	hardware
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgMnSc1d2	menší
<g/>
,	,	kIx,	,
levnější	levný	k2eAgMnSc1d2	levnější
a	a	k8xC	a
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
Mooreův	Mooreův	k2eAgInSc4d1	Mooreův
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k8xC	tak
se	se	k3xPyFc4	se
části	část	k1gFnSc3	část
počítačů	počítač	k1gMnPc2	počítač
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
dříve	dříve	k6eAd2	dříve
považované	považovaný	k2eAgFnPc1d1	považovaná
za	za	k7c4	za
software	software	k1gInSc4	software
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
hardwaru	hardware	k1gInSc3	hardware
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
dnešních	dnešní	k2eAgFnPc2d1	dnešní
hardwarových	hardwarový	k2eAgFnPc2d1	hardwarová
společností	společnost	k1gFnPc2	společnost
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
více	hodně	k6eAd2	hodně
softwarových	softwarový	k2eAgMnPc2d1	softwarový
programátorů	programátor	k1gMnPc2	programátor
než	než	k8xS	než
hardwarových	hardwarový	k2eAgMnPc2d1	hardwarový
návrhářů	návrhář	k1gMnPc2	návrhář
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
co	co	k9	co
softwarové	softwarový	k2eAgInPc1d1	softwarový
nástroje	nástroj	k1gInPc1	nástroj
zautomatizovaly	zautomatizovat	k5eAaPmAgFnP	zautomatizovat
mnoho	mnoho	k6eAd1	mnoho
úkolů	úkol	k1gInPc2	úkol
souvisejících	související	k2eAgInPc2d1	související
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
plošných	plošný	k2eAgInPc2d1	plošný
spojů	spoj	k1gInPc2	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
automobilovém	automobilový	k2eAgInSc6d1	automobilový
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
základy	základ	k1gInPc4	základ
softwarového	softwarový	k2eAgInSc2d1	softwarový
průmyslu	průmysl	k1gInSc2	průmysl
položilo	položit	k5eAaPmAgNnS	položit
několik	několik	k4yIc1	několik
vizionářů	vizionář	k1gMnPc2	vizionář
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
garáži	garáž	k1gFnSc6	garáž
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
své	svůj	k3xOyFgInPc4	svůj
prototypy	prototyp	k1gInPc4	prototyp
<g/>
.	.	kIx.	.
</s>
<s>
Steve	Steve	k1gMnSc1	Steve
Jobs	Jobs	k1gInSc1	Jobs
a	a	k8xC	a
Bill	Bill	k1gMnSc1	Bill
Gates	Gates	k1gMnSc1	Gates
byli	být	k5eAaImAgMnP	být
Henry	henry	k1gInSc7	henry
Ford	ford	k1gInSc1	ford
a	a	k8xC	a
Louis	Louis	k1gMnSc1	Louis
Chevrolet	chevrolet	k1gInSc4	chevrolet
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
profitovali	profitovat	k5eAaBmAgMnP	profitovat
z	z	k7c2	z
nápadů	nápad	k1gInPc2	nápad
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
všeobecně	všeobecně	k6eAd1	všeobecně
známé	známý	k2eAgInPc1d1	známý
v	v	k7c6	v
době	doba	k1gFnSc6	doba
ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
začali	začít	k5eAaPmAgMnP	začít
podnikat	podnikat	k5eAaImF	podnikat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
klíčový	klíčový	k2eAgInSc4d1	klíčový
moment	moment	k1gInSc4	moment
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
softwaru	software	k1gInSc2	software
se	se	k3xPyFc4	se
všeobecně	všeobecně	k6eAd1	všeobecně
považuje	považovat	k5eAaImIp3nS	považovat
publikace	publikace	k1gFnSc1	publikace
specifikací	specifikace	k1gFnPc2	specifikace
k	k	k7c3	k
IBM	IBM	kA	IBM
PC	PC	kA	PC
od	od	k7c2	od
zaměstnance	zaměstnanec	k1gMnSc2	zaměstnanec
IBM	IBM	kA	IBM
Philipa	Philip	k1gMnSc2	Philip
Dona	Don	k1gMnSc2	Don
Estridge	Estridg	k1gMnSc2	Estridg
z	z	k7c2	z
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
typ	typ	k1gInSc4	typ
crowdsourcingu	crowdsourcing	k1gInSc2	crowdsourcing
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
software	software	k1gInSc1	software
svázán	svázat	k5eAaPmNgInS	svázat
s	s	k7c7	s
hardwarem	hardware	k1gInSc7	hardware
od	od	k7c2	od
OEM	OEM	kA	OEM
výrobců	výrobce	k1gMnPc2	výrobce
jako	jako	k8xS	jako
Data	datum	k1gNnSc2	datum
General	General	k1gFnSc2	General
<g/>
,	,	kIx,	,
Digital	Digital	kA	Digital
Equipment	Equipment	k1gInSc1	Equipment
Corporation	Corporation	k1gInSc1	Corporation
a	a	k8xC	a
IBM	IBM	kA	IBM
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
zákazník	zákazník	k1gMnSc1	zákazník
koupil	koupit	k5eAaPmAgMnS	koupit
minipočítač	minipočítač	k1gInSc4	minipočítač
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nejmenší	malý	k2eAgInSc4d3	nejmenší
počítač	počítač	k1gInSc4	počítač
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
počítač	počítač	k1gInSc1	počítač
neobsahoval	obsahovat	k5eNaImAgInS	obsahovat
žádný	žádný	k3yNgInSc4	žádný
předinstalovaný	předinstalovaný	k2eAgInSc4d1	předinstalovaný
software	software	k1gInSc4	software
<g/>
.	.	kIx.	.
</s>
<s>
Software	software	k1gInSc1	software
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
nainstalován	nainstalován	k2eAgInSc1d1	nainstalován
inženýry	inženýr	k1gMnPc4	inženýr
zaměstnanými	zaměstnaný	k2eAgFnPc7d1	zaměstnaná
u	u	k7c2	u
OEM	OEM	kA	OEM
<g/>
.	.	kIx.	.
</s>
<s>
Hardwarové	hardwarový	k2eAgFnPc1d1	hardwarová
společnosti	společnost	k1gFnPc1	společnost
nejen	nejen	k6eAd1	nejen
že	že	k8xS	že
svazovaly	svazovat	k5eAaImAgFnP	svazovat
svůj	svůj	k3xOyFgInSc4	svůj
software	software	k1gInSc4	software
s	s	k7c7	s
hardwarem	hardware	k1gInSc7	hardware
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
kladly	klást	k5eAaImAgInP	klást
požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
umístění	umístění	k1gNnSc4	umístění
hardwaru	hardware	k1gInSc2	hardware
do	do	k7c2	do
klimatizované	klimatizovaný	k2eAgFnSc2d1	klimatizovaná
počítačové	počítačový	k2eAgFnSc2d1	počítačová
místnosti	místnost	k1gFnSc2	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
společností	společnost	k1gFnPc2	společnost
vedla	vést	k5eAaImAgFnS	vést
v	v	k7c6	v
účetnictví	účetnictví	k1gNnSc6	účetnictví
svůj	svůj	k3xOyFgInSc1	svůj
software	software	k1gInSc1	software
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
0	[number]	k4	0
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
software	software	k1gInSc1	software
nemohl	moct	k5eNaImAgInS	moct
být	být	k5eAaImF	být
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k9	jako
majetek	majetek	k1gInSc1	majetek
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
podobné	podobný	k2eAgNnSc1d1	podobné
jako	jako	k8xS	jako
financování	financování	k1gNnSc1	financování
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
společnost	společnost	k1gFnSc1	společnost
Data	datum	k1gNnSc2	datum
General	General	k1gFnSc2	General
představila	představit	k5eAaPmAgFnS	představit
počítač	počítač	k1gInSc4	počítač
Data	datum	k1gNnSc2	datum
General	General	k1gFnSc2	General
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
Digidyne	Digidyn	k1gInSc5	Digidyn
chtěla	chtít	k5eAaImAgFnS	chtít
použít	použít	k5eAaPmF	použít
jeho	jeho	k3xOp3gInSc4	jeho
RDOSový	RDOSový	k2eAgInSc4d1	RDOSový
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
vlastním	vlastní	k2eAgInSc6d1	vlastní
hardwarovém	hardwarový	k2eAgInSc6d1	hardwarový
klonu	klon	k1gInSc6	klon
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnSc2	datum
General	General	k1gFnSc2	General
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
licencovat	licencovat	k5eAaBmF	licencovat
jejich	jejich	k3xOp3gInSc4	jejich
software	software	k1gInSc4	software
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
ale	ale	k9	ale
těžko	těžko	k6eAd1	těžko
proveditelné	proveditelný	k2eAgNnSc1d1	proveditelné
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
účetnictví	účetnictví	k1gNnSc6	účetnictví
veden	vést	k5eAaImNgMnS	vést
jako	jako	k8xC	jako
volné	volný	k2eAgNnSc4d1	volné
aktivum	aktivum	k1gNnSc4	aktivum
<g/>
)	)	kIx)	)
a	a	k8xC	a
domáhala	domáhat	k5eAaImAgFnS	domáhat
se	se	k3xPyFc4	se
svých	svůj	k3xOyFgNnPc2	svůj
práv	právo	k1gNnPc2	právo
na	na	k7c6	na
svázání	svázání	k1gNnSc6	svázání
softwaru	software	k1gInSc2	software
s	s	k7c7	s
hardwarem	hardware	k1gInSc7	hardware
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
precedens	precedens	k1gNnSc4	precedens
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Digidyne	Digidyn	k1gInSc5	Digidyn
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
General	General	k1gFnSc2	General
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
uznal	uznat	k5eAaPmAgInS	uznat
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
nižší	nízký	k2eAgFnSc2d2	nižší
instance	instance	k1gFnSc2	instance
a	a	k8xC	a
Data	datum	k1gNnSc2	datum
General	General	k1gFnSc2	General
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
donucena	donucen	k2eAgFnSc1d1	donucena
licencovat	licencovat	k5eAaBmF	licencovat
software	software	k1gInSc4	software
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
bylo	být	k5eAaImAgNnS	být
usneseno	usnést	k5eAaPmNgNnS	usnést
<g/>
,	,	kIx,	,
že	že	k8xS	že
omezování	omezování	k1gNnSc1	omezování
licence	licence	k1gFnSc2	licence
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
DG	dg	kA	dg
hardware	hardware	k1gInSc4	hardware
bylo	být	k5eAaImAgNnS	být
nelegální	legální	k2eNgNnSc1d1	nelegální
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
IBM	IBM	kA	IBM
veřejně	veřejně	k6eAd1	veřejně
publikovala	publikovat	k5eAaBmAgFnS	publikovat
API	API	kA	API
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
DOS	DOS	kA	DOS
a	a	k8xC	a
zrodil	zrodit	k5eAaPmAgMnS	zrodit
se	se	k3xPyFc4	se
Microsoft	Microsoft	kA	Microsoft
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgFnSc1d1	finanční
ztráta	ztráta	k1gFnSc1	ztráta
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
z	z	k7c2	z
poplatků	poplatek	k1gInPc2	poplatek
právníkům	právník	k1gMnPc3	právník
nakonec	nakonec	k6eAd1	nakonec
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Data	datum	k1gNnSc2	datum
General	General	k1gFnSc2	General
byla	být	k5eAaImAgFnS	být
převzata	převzít	k5eAaPmNgFnS	převzít
společností	společnost	k1gFnSc7	společnost
EMC	EMC	kA	EMC
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
nárůst	nárůst	k1gInSc4	nárůst
ceny	cena	k1gFnSc2	cena
softwaru	software	k1gInSc2	software
a	a	k8xC	a
také	také	k9	také
nakupování	nakupování	k1gNnSc1	nakupování
softwarových	softwarový	k2eAgInPc2d1	softwarový
patentů	patent	k1gInPc2	patent
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
společnosti	společnost	k1gFnSc2	společnost
IBM	IBM	kA	IBM
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vnímáno	vnímat	k5eAaImNgNnS	vnímat
téměř	téměř	k6eAd1	téměř
jako	jako	k8xC	jako
revolta	revolta	k1gFnSc1	revolta
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
málo	málo	k4c1	málo
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
než	než	k8xS	než
IBM	IBM	kA	IBM
bude	být	k5eAaImBp3nS	být
ze	z	k7c2	z
softwaru	software	k1gInSc2	software
profitovat	profitovat	k5eAaBmF	profitovat
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
a	a	k8xC	a
Apple	Apple	kA	Apple
tudíž	tudíž	k8xC	tudíž
mohli	moct	k5eAaImAgMnP	moct
využít	využít	k5eAaPmF	využít
situace	situace	k1gFnPc4	situace
a	a	k8xC	a
začít	začít	k5eAaPmF	začít
vydělávat	vydělávat	k5eAaImF	vydělávat
na	na	k7c6	na
softwarových	softwarový	k2eAgInPc6d1	softwarový
produktech	produkt	k1gInPc6	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Těžko	těžko	k6eAd1	těžko
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
dnes	dnes	k6eAd1	dnes
představit	představit	k5eAaPmF	představit
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
názorů	názor	k1gInPc2	názor
byl	být	k5eAaImAgInS	být
software	software	k1gInSc1	software
bez	bez	k7c2	bez
hardwarového	hardwarový	k2eAgNnSc2d1	hardwarové
zařízení	zařízení	k1gNnSc2	zařízení
bezcenný	bezcenný	k2eAgInSc4d1	bezcenný
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
úspěšných	úspěšný	k2eAgFnPc2d1	úspěšná
firem	firma	k1gFnPc2	firma
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
prodává	prodávat	k5eAaImIp3nS	prodávat
pouze	pouze	k6eAd1	pouze
softwarové	softwarový	k2eAgInPc4d1	softwarový
produkty	produkt	k1gInPc4	produkt
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
stále	stále	k6eAd1	stále
existují	existovat	k5eAaImIp3nP	existovat
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
licencemi	licence	k1gFnPc7	licence
softwaru	software	k1gInSc2	software
kvůli	kvůli	k7c3	kvůli
složitosti	složitost	k1gFnSc3	složitost
návrhů	návrh	k1gInPc2	návrh
a	a	k8xC	a
špatné	špatný	k2eAgFnSc6d1	špatná
dokumentaci	dokumentace	k1gFnSc6	dokumentace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
patentových	patentový	k2eAgMnPc2d1	patentový
trollů	troll	k1gMnPc2	troll
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
volně	volně	k6eAd1	volně
šiřitelnými	šiřitelný	k2eAgFnPc7d1	šiřitelná
softwarovými	softwarový	k2eAgFnPc7d1	softwarová
specifikacemi	specifikace	k1gFnPc7	specifikace
a	a	k8xC	a
možností	možnost	k1gFnSc7	možnost
licencování	licencování	k1gNnSc2	licencování
softwaru	software	k1gInSc2	software
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nové	nový	k2eAgFnPc1d1	nová
příležitosti	příležitost	k1gFnPc1	příležitost
pro	pro	k7c4	pro
softwarové	softwarový	k2eAgInPc4d1	softwarový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
de	de	k?	de
facto	facta	k1gFnSc5	facta
standardem	standard	k1gInSc7	standard
–	–	k?	–
jako	jako	k9	jako
například	například	k6eAd1	například
DOS	DOS	kA	DOS
pro	pro	k7c4	pro
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
nebo	nebo	k8xC	nebo
také	také	k9	také
různé	různý	k2eAgInPc4d1	různý
proprietární	proprietární	k2eAgInPc4d1	proprietární
programy	program	k1gInPc4	program
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
textu	text	k1gInSc2	text
a	a	k8xC	a
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
proprietární	proprietární	k2eAgFnPc1d1	proprietární
metody	metoda	k1gFnPc1	metoda
vývoje	vývoj	k1gInSc2	vývoj
staly	stát	k5eAaPmAgFnP	stát
standardní	standardní	k2eAgFnSc7d1	standardní
metodologií	metodologie	k1gFnSc7	metodologie
vývoje	vývoj	k1gInSc2	vývoj
softwaru	software	k1gInSc2	software
<g/>
.	.	kIx.	.
</s>
<s>
Definici	definice	k1gFnSc4	definice
ani	ani	k8xC	ani
rozdělení	rozdělení	k1gNnSc4	rozdělení
software	software	k1gInSc4	software
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zcela	zcela	k6eAd1	zcela
přesně	přesně	k6eAd1	přesně
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
existuje	existovat	k5eAaImIp3nS	existovat
velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k4c4	mnoho
pohledů	pohled	k1gInPc2	pohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
provedena	provést	k5eAaPmNgFnS	provést
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
diskutovány	diskutován	k2eAgInPc4d1	diskutován
některé	některý	k3yIgInPc4	některý
aspekty	aspekt	k1gInPc4	aspekt
<g/>
.	.	kIx.	.
</s>
<s>
Software	software	k1gInSc1	software
můžeme	moct	k5eAaImIp1nP	moct
definovat	definovat	k5eAaBmF	definovat
i	i	k9	i
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
není	být	k5eNaImIp3nS	být
hardware	hardware	k1gInSc1	hardware
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
vše	všechen	k3xTgNnSc4	všechen
kromě	kromě	k7c2	kromě
fyzických	fyzický	k2eAgFnPc2d1	fyzická
součástí	součást	k1gFnPc2	součást
počítače	počítač	k1gInSc2	počítač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
zahrnujeme	zahrnovat	k5eAaImIp1nP	zahrnovat
mezi	mezi	k7c4	mezi
software	software	k1gInSc4	software
i	i	k9	i
data	datum	k1gNnPc1	datum
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
typicky	typicky	k6eAd1	typicky
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vykonat	vykonat	k5eAaPmF	vykonat
procesorem	procesor	k1gInSc7	procesor
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
strojové	strojový	k2eAgFnPc4d1	strojová
instrukce	instrukce	k1gFnPc4	instrukce
pro	pro	k7c4	pro
procesor	procesor	k1gInSc4	procesor
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
data	datum	k1gNnPc1	datum
popisují	popisovat	k5eAaImIp3nP	popisovat
obrázek	obrázek	k1gInSc4	obrázek
<g/>
,	,	kIx,	,
textový	textový	k2eAgInSc4d1	textový
dokument	dokument	k1gInSc4	dokument
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
software	software	k1gInSc1	software
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
někdy	někdy	k6eAd1	někdy
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
jen	jen	k9	jen
na	na	k7c4	na
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
vztahovat	vztahovat	k5eAaImF	vztahovat
i	i	k9	i
na	na	k7c4	na
data	datum	k1gNnPc4	datum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
lze	lze	k6eAd1	lze
však	však	k9	však
na	na	k7c4	na
data	datum	k1gNnPc4	datum
pohlížet	pohlížet	k5eAaImF	pohlížet
i	i	k8xC	i
jako	jako	k9	jako
na	na	k7c4	na
program	program	k1gInSc4	program
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
programy	program	k1gInPc4	program
zkomprimované	zkomprimovaný	k2eAgInPc4d1	zkomprimovaný
do	do	k7c2	do
ZIP	zip	k1gInSc1	zip
souboru	soubor	k1gInSc2	soubor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
výkonným	výkonný	k2eAgInSc7d1	výkonný
software	software	k1gInSc1	software
(	(	kIx(	(
<g/>
programem	program	k1gInSc7	program
<g/>
)	)	kIx)	)
a	a	k8xC	a
daty	datum	k1gNnPc7	datum
je	být	k5eAaImIp3nS	být
nejasná	jasný	k2eNgFnSc1d1	nejasná
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
HTML	HTML	kA	HTML
souborů	soubor	k1gInPc2	soubor
obsahujících	obsahující	k2eAgInPc2d1	obsahující
webové	webový	k2eAgNnSc4d1	webové
stránky	stránka	k1gFnPc4	stránka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
úryvek	úryvek	k1gInSc1	úryvek
programu	program	k1gInSc2	program
v	v	k7c6	v
JavaScriptu	JavaScript	k1gInSc6	JavaScript
nebo	nebo	k8xC	nebo
jazyce	jazyk	k1gInSc6	jazyk
PHP	PHP	kA	PHP
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
i	i	k9	i
další	další	k2eAgInPc4d1	další
příklady	příklad	k1gInPc4	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Software	software	k1gInSc1	software
může	moct	k5eAaImIp3nS	moct
provádět	provádět	k5eAaImF	provádět
i	i	k9	i
nezamýšlenou	zamýšlený	k2eNgFnSc4d1	nezamýšlená
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
hovoříme	hovořit	k5eAaImIp1nP	hovořit
buď	buď	k8xC	buď
o	o	k7c6	o
programátorské	programátorský	k2eAgFnSc6d1	programátorská
chybě	chyba	k1gFnSc6	chyba
nebo	nebo	k8xC	nebo
o	o	k7c6	o
počítačových	počítačový	k2eAgInPc6d1	počítačový
virech	vir	k1gInPc6	vir
<g/>
,	,	kIx,	,
malware	malwar	k1gMnSc5	malwar
<g/>
,	,	kIx,	,
spyware	spywar	k1gMnSc5	spywar
<g/>
,	,	kIx,	,
trojských	trojský	k2eAgMnPc6d1	trojský
koních	kůň	k1gMnPc6	kůň
a	a	k8xC	a
podobném	podobný	k2eAgInSc6d1	podobný
nežádoucím	žádoucí	k2eNgInSc7d1	nežádoucí
software	software	k1gInSc4	software
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
existence	existence	k1gFnSc2	existence
nežádoucího	žádoucí	k2eNgInSc2d1	nežádoucí
software	software	k1gInSc4	software
jsou	být	k5eAaImIp3nP	být
zlé	zlý	k2eAgInPc1d1	zlý
nebo	nebo	k8xC	nebo
nečestné	čestný	k2eNgInPc4d1	nečestný
úmysly	úmysl	k1gInPc4	úmysl
jejích	její	k3xOp3gMnPc2	její
tvůrců	tvůrce	k1gMnPc2	tvůrce
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zneužívají	zneužívat	k5eAaImIp3nP	zneužívat
chyb	chyba	k1gFnPc2	chyba
ostatních	ostatní	k2eAgInPc2d1	ostatní
software	software	k1gInSc4	software
(	(	kIx(	(
<g/>
webový	webový	k2eAgMnSc1d1	webový
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
<g/>
,	,	kIx,	,
e-mailový	eailový	k2eAgMnSc1d1	e-mailový
klient	klient	k1gMnSc1	klient
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
operačním	operační	k2eAgInSc6d1	operační
systému	systém	k1gInSc6	systém
a	a	k8xC	a
jinde	jinde	k6eAd1	jinde
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
neznalosti	neznalost	k1gFnSc2	neznalost
obsluhy	obsluha	k1gFnSc2	obsluha
počítače	počítač	k1gInSc2	počítač
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
sociální	sociální	k2eAgNnSc4d1	sociální
inženýrství	inženýrství	k1gNnSc4	inženýrství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Běžný	běžný	k2eAgMnSc1d1	běžný
uživatel	uživatel	k1gMnSc1	uživatel
počítače	počítač	k1gInSc2	počítač
obvykle	obvykle	k6eAd1	obvykle
nemá	mít	k5eNaImIp3nS	mít
dostatečné	dostatečný	k2eAgFnPc4d1	dostatečná
technické	technický	k2eAgFnPc4d1	technická
znalosti	znalost	k1gFnPc4	znalost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
takový	takový	k3xDgInSc1	takový
software	software	k1gInSc1	software
rozeznal	rozeznat	k5eAaPmAgInS	rozeznat
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
zabránil	zabránit	k5eAaPmAgInS	zabránit
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
existují	existovat	k5eAaImIp3nP	existovat
antivirové	antivirový	k2eAgInPc4d1	antivirový
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
antispyware	antispywar	k1gMnSc5	antispywar
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
aplikačního	aplikační	k2eAgMnSc2d1	aplikační
a	a	k8xC	a
systémového	systémový	k2eAgMnSc2d1	systémový
software	software	k1gInSc1	software
a	a	k8xC	a
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
se	se	k3xPyFc4	se
činnost	činnost	k1gFnSc1	činnost
nežádoucího	žádoucí	k2eNgInSc2d1	nežádoucí
software	software	k1gInSc4	software
eliminovat	eliminovat	k5eAaBmF	eliminovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
funkce	funkce	k1gFnSc2	funkce
můžeme	moct	k5eAaImIp1nP	moct
software	software	k1gInSc4	software
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
systémový	systémový	k2eAgInSc1d1	systémový
software	software	k1gInSc1	software
–	–	k?	–
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
efektivní	efektivní	k2eAgNnSc4d1	efektivní
používání	používání	k1gNnSc4	používání
počítače	počítač	k1gInSc2	počítač
firmware	firmware	k1gInSc1	firmware
–	–	k?	–
software	software	k1gInSc1	software
obsažené	obsažený	k2eAgInPc1d1	obsažený
v	v	k7c4	v
hardware	hardware	k1gInSc4	hardware
(	(	kIx(	(
<g/>
BIOS	BIOS	kA	BIOS
<g/>
,	,	kIx,	,
firmware	firmware	k1gInSc1	firmware
vstupně-výstupních	vstupněýstupní	k2eAgNnPc2d1	vstupně-výstupní
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
–	–	k?	–
spravuje	spravovat	k5eAaImIp3nS	spravovat
počítač	počítač	k1gInSc1	počítač
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
prostředí	prostředí	k1gNnSc1	prostředí
pro	pro	k7c4	pro
programy	program	k1gInPc4	program
jádro	jádro	k1gNnSc4	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
včetně	včetně	k7c2	včetně
ovladačů	ovladač	k1gInPc2	ovladač
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
)	)	kIx)	)
pomocné	pomocný	k2eAgInPc1d1	pomocný
systémové	systémový	k2eAgInPc1d1	systémový
nástroje	nástroj	k1gInPc1	nástroj
–	–	k?	–
pro	pro	k7c4	pro
správu	správa	k1gFnSc4	správa
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
formátování	formátování	k1gNnSc4	formátování
disků	disk	k1gInPc2	disk
<g/>
,	,	kIx,	,
nastavení	nastavení	k1gNnSc4	nastavení
oprávnění	oprávnění	k1gNnSc1	oprávnění
<g/>
,	,	kIx,	,
utility	utilita	k1gFnPc1	utilita
<g/>
,	,	kIx,	,
démoni	démon	k1gMnPc1	démon
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
aplikační	aplikační	k2eAgInSc1d1	aplikační
software	software	k1gInSc1	software
–	–	k?	–
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uživateli	uživatel	k1gMnSc3	uživatel
vykonávat	vykonávat	k5eAaImF	vykonávat
nějakou	nějaký	k3yIgFnSc4	nějaký
užitečnou	užitečný	k2eAgFnSc4d1	užitečná
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
kancelářské	kancelářský	k2eAgInPc1d1	kancelářský
balíky	balík	k1gInPc1	balík
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
textový	textový	k2eAgInSc1d1	textový
editor	editor	k1gInSc1	editor
<g/>
,	,	kIx,	,
tabulkový	tabulkový	k2eAgInSc1d1	tabulkový
procesor	procesor	k1gInSc1	procesor
<g/>
,	,	kIx,	,
prezentační	prezentační	k2eAgInSc1d1	prezentační
program	program	k1gInSc1	program
<g/>
...	...	k?	...
grafické	grafický	k2eAgInPc1d1	grafický
programy	program	k1gInPc1	program
<g/>
:	:	kIx,	:
vektorový	vektorový	k2eAgInSc1d1	vektorový
grafický	grafický	k2eAgInSc1d1	grafický
editor	editor	k1gInSc1	editor
<g/>
,	,	kIx,	,
bitmapový	bitmapový	k2eAgInSc1d1	bitmapový
grafický	grafický	k2eAgInSc1d1	grafický
editor	editor	k1gInSc1	editor
<g/>
...	...	k?	...
vývojové	vývojový	k2eAgInPc1d1	vývojový
nástroje	nástroj	k1gInPc1	nástroj
<g/>
:	:	kIx,	:
vývojové	vývojový	k2eAgNnSc1d1	vývojové
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
,	,	kIx,	,
překladač	překladač	k1gInSc1	překladač
<g/>
...	...	k?	...
zábavní	zábavní	k2eAgInSc1d1	zábavní
software	software	k1gInSc1	software
<g/>
:	:	kIx,	:
počítačové	počítačový	k2eAgFnSc2d1	počítačová
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
přehrávače	přehrávač	k1gInSc2	přehrávač
digitálního	digitální	k2eAgInSc2d1	digitální
zvuku	zvuk	k1gInSc2	zvuk
a	a	k8xC	a
videa	video	k1gNnSc2	video
apod.	apod.	kA	apod.
Podle	podle	k7c2	podle
finanční	finanční	k2eAgFnSc2d1	finanční
dostupnosti	dostupnost	k1gFnSc2	dostupnost
můžeme	moct	k5eAaImIp1nP	moct
software	software	k1gInSc4	software
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
freeware	freeware	k1gInSc4	freeware
shareware	shareware	k1gInSc1	shareware
komerční	komerční	k2eAgInSc1d1	komerční
software	software	k1gInSc1	software
Retail	Retail	k1gInSc4	Retail
verze	verze	k1gFnSc2	verze
softwaru	software	k1gInSc2	software
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
krabicová	krabicový	k2eAgFnSc1d1	krabicová
verze	verze	k1gFnSc1	verze
softwaru	software	k1gInSc2	software
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nainstalována	nainstalovat	k5eAaPmNgFnS	nainstalovat
na	na	k7c4	na
libovolný	libovolný	k2eAgInSc4d1	libovolný
počítač	počítač	k1gInSc4	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tedy	tedy	k9	tedy
současně	současně	k6eAd1	současně
nainstalována	nainstalován	k2eAgFnSc1d1	nainstalována
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
počítači	počítač	k1gInSc6	počítač
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
licencí	licence	k1gFnPc2	licence
na	na	k7c6	na
více	hodně	k6eAd2	hodně
počítačích	počítač	k1gInPc6	počítač
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
možno	možno	k6eAd1	možno
ji	on	k3xPp3gFnSc4	on
libovolně	libovolně	k6eAd1	libovolně
přenášet	přenášet	k5eAaImF	přenášet
z	z	k7c2	z
počítače	počítač	k1gInSc2	počítač
na	na	k7c4	na
počítač	počítač	k1gInSc4	počítač
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Lze	lze	k6eAd1	lze
vymyslet	vymyslet	k5eAaPmF	vymyslet
i	i	k9	i
různá	různý	k2eAgNnPc1d1	různé
další	další	k2eAgNnPc1d1	další
rozdělení	rozdělení	k1gNnPc1	rozdělení
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
účelu	účel	k1gInSc2	účel
<g/>
,	,	kIx,	,
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
,	,	kIx,	,
funkčnosti	funkčnost	k1gFnSc2	funkčnost
–	–	k?	–
například	například	k6eAd1	například
oddělit	oddělit	k5eAaPmF	oddělit
softwarové	softwarový	k2eAgFnPc4d1	softwarová
knihovny	knihovna	k1gFnPc4	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
kategorie	kategorie	k1gFnPc1	kategorie
jako	jako	k9	jako
například	například	k6eAd1	například
antispyware	antispywar	k1gMnSc5	antispywar
<g/>
,	,	kIx,	,
malware	malwar	k1gMnSc5	malwar
(	(	kIx(	(
<g/>
rogueware	roguewar	k1gMnSc5	roguewar
<g/>
,	,	kIx,	,
scareware	scarewar	k1gMnSc5	scarewar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ransomware	ransomwar	k1gMnSc5	ransomwar
<g/>
,	,	kIx,	,
spyware	spywar	k1gMnSc5	spywar
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
skupiny	skupina	k1gFnPc1	skupina
jako	jako	k8xC	jako
adware	adwar	k1gInSc5	adwar
<g/>
.	.	kIx.	.
</s>
<s>
Nechtěný	chtěný	k2eNgInSc1d1	nechtěný
předinstalovaný	předinstalovaný	k2eAgInSc1d1	předinstalovaný
software	software	k1gInSc1	software
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
crapware	crapwar	k1gInSc5	crapwar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Computer	computer	k1gInSc1	computer
software	software	k1gInSc1	software
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Aplikační	aplikační	k2eAgInSc1d1	aplikační
software	software	k1gInSc1	software
Výukový	výukový	k2eAgInSc4d1	výukový
program	program	k1gInSc4	program
Hardware	hardware	k1gInSc1	hardware
Historie	historie	k1gFnSc1	historie
počítačů	počítač	k1gMnPc2	počítač
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
software	software	k1gInSc1	software
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
software	software	k1gInSc1	software
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
