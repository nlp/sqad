<p>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
klávesistou	klávesista	k1gMnSc7	klávesista
a	a	k8xC	a
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Michaelem	Michael	k1gMnSc7	Michael
Kocábem	Kocáb	k1gMnSc7	Kocáb
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
orientovala	orientovat	k5eAaBmAgFnS	orientovat
na	na	k7c4	na
jazz	jazz	k1gInSc4	jazz
rock	rock	k1gInSc4	rock
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
populární	populární	k2eAgMnSc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
přistoupila	přistoupit	k5eAaPmAgFnS	přistoupit
k	k	k7c3	k
celkové	celkový	k2eAgFnSc3d1	celková
změně	změna	k1gFnSc3	změna
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
působila	působit	k5eAaImAgFnS	působit
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
českých	český	k2eAgFnPc2d1	Česká
novovlných	novovlný	k2eAgFnPc2d1	novovlný
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
1986	[number]	k4	1986
byl	být	k5eAaImAgInS	být
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
režimem	režim	k1gInSc7	režim
zakázán	zakázán	k2eAgInSc1d1	zakázán
<g/>
,	,	kIx,	,
ke	k	k7c3	k
koncertování	koncertování	k1gNnSc3	koncertování
a	a	k8xC	a
vydávání	vydávání	k1gNnSc3	vydávání
alb	album	k1gNnPc2	album
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
vrátit	vrátit	k5eAaPmF	vrátit
až	až	k9	až
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
skupina	skupina	k1gFnSc1	skupina
spíše	spíše	k9	spíše
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
,	,	kIx,	,
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
koncertů	koncert	k1gInPc2	koncert
znovu	znovu	k6eAd1	znovu
pořádala	pořádat	k5eAaImAgFnS	pořádat
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
neshodám	neshoda	k1gFnPc3	neshoda
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
<g/>
,	,	kIx,	,
Michaelem	Michael	k1gMnSc7	Michael
Kocábem	Kocáb	k1gMnSc7	Kocáb
a	a	k8xC	a
kytaristou	kytarista	k1gMnSc7	kytarista
Michalem	Michal	k1gMnSc7	Michal
Pavlíčkem	Pavlíček	k1gMnSc7	Pavlíček
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2006	[number]	k4	2006
založil	založit	k5eAaPmAgMnS	založit
Kocáb	Kocáb	k1gMnSc1	Kocáb
novou	nový	k2eAgFnSc4d1	nová
skupinu	skupina	k1gFnSc4	skupina
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Pražský	pražský	k2eAgInSc4d1	pražský
výběr	výběr	k1gInSc4	výběr
II	II	kA	II
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
koncertovala	koncertovat	k5eAaImAgFnS	koncertovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
oznámen	oznámen	k2eAgInSc1d1	oznámen
reunion	reunion	k1gInSc1	reunion
Pražského	pražský	k2eAgInSc2d1	pražský
výběru	výběr	k1gInSc2	výběr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
asi	asi	k9	asi
nejoblíbenějším	oblíbený	k2eAgMnSc6d3	nejoblíbenější
<g/>
,	,	kIx,	,
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
instrumentálních	instrumentální	k2eAgFnPc2d1	instrumentální
schopností	schopnost	k1gFnPc2	schopnost
jednoznačně	jednoznačně	k6eAd1	jednoznačně
nejvybavenějším	vybavený	k2eAgMnPc3d3	nejvybavenější
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
nejdiskutovanějším	diskutovaný	k2eAgInSc7d3	nejdiskutovanější
souborem	soubor	k1gInSc7	soubor
české	český	k2eAgFnSc2d1	Česká
nové	nový	k2eAgFnSc2d1	nová
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Citujme	citovat	k5eAaBmRp1nP	citovat
knihu	kniha	k1gFnSc4	kniha
"	"	kIx"	"
<g/>
Excentrici	excentrik	k1gMnPc1	excentrik
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Odvaz	Odvaz	k1gInSc4	Odvaz
<g/>
,	,	kIx,	,
sranda	sranda	k1gFnSc1	sranda
<g/>
,	,	kIx,	,
recese	recese	k1gFnSc2	recese
<g/>
,	,	kIx,	,
crazy	craza	k1gFnSc2	craza
humor	humor	k1gInSc4	humor
<g/>
,	,	kIx,	,
ironie	ironie	k1gFnSc2	ironie
a	a	k8xC	a
nadsázka	nadsázka	k1gFnSc1	nadsázka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
patrony	patrona	k1gFnPc1	patrona
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
začala	začít	k5eAaPmAgFnS	začít
česká	český	k2eAgFnSc1d1	Česká
nová	nový	k2eAgFnSc1d1	nová
vlna	vlna	k1gFnSc1	vlna
nabíjet	nabíjet	k5eAaImF	nabíjet
svůj	svůj	k3xOyFgInSc4	svůj
výrazový	výrazový	k2eAgInSc4d1	výrazový
materiál	materiál	k1gInSc4	materiál
jako	jako	k8xS	jako
mladá	mladý	k2eAgFnSc1d1	mladá
hudba	hudba	k1gFnSc1	hudba
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
desetiletí	desetiletí	k1gNnSc4	desetiletí
(	(	kIx(	(
<g/>
míněna	míněn	k2eAgFnSc1d1	míněna
80	[number]	k4	80
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
-	-	kIx~	-
poz	poz	k?	poz
<g/>
.	.	kIx.	.
aut	auto	k1gNnPc2	auto
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
tvůrčí	tvůrčí	k2eAgInPc1d1	tvůrčí
principy	princip	k1gInPc1	princip
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
vtrhl	vtrhnout	k5eAaPmAgInS	vtrhnout
na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
obnovený	obnovený	k2eAgInSc4d1	obnovený
Pražský	pražský	k2eAgInSc4d1	pražský
výběr	výběr	k1gInSc4	výběr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gInPc4	on
vyhrotil	vyhrotit	k5eAaPmAgInS	vyhrotit
do	do	k7c2	do
excentrické	excentrický	k2eAgFnSc2d1	excentrická
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gInPc4	on
vysvářel	vysvářet	k5eAaImAgMnS	vysvářet
a	a	k8xC	a
ofrézoval	ofrézovat	k5eAaPmAgMnS	ofrézovat
do	do	k7c2	do
sevřených	sevřený	k2eAgInPc2d1	sevřený
jednolitých	jednolitý	k2eAgInPc2d1	jednolitý
písničkových	písničkový	k2eAgInPc2d1	písničkový
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
původní	původní	k2eAgFnSc7d1	původní
rockovou	rockový	k2eAgFnSc7d1	rocková
písničkou	písnička	k1gFnSc7	písnička
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nezapře	zapřít	k5eNaPmIp3nS	zapřít
českou	český	k2eAgFnSc4d1	Česká
hudebnost	hudebnost	k1gFnSc4	hudebnost
a	a	k8xC	a
mentalitu	mentalita	k1gFnSc4	mentalita
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc4	schopnost
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
běh	běh	k1gInSc4	běh
života	život	k1gInSc2	život
smíchem	smích	k1gInSc7	smích
a	a	k8xC	a
komentovat	komentovat	k5eAaBmF	komentovat
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
tradici	tradice	k1gFnSc6	tradice
českého	český	k2eAgInSc2d1	český
humoru	humor	k1gInSc2	humor
a	a	k8xC	a
dryáčnického	dryáčnický	k2eAgNnSc2d1	dryáčnické
kramářského	kramářský	k2eAgNnSc2d1	kramářské
divadelnictví	divadelnictví	k1gNnSc2	divadelnictví
<g/>
.	.	kIx.	.
</s>
<s>
Profesionální	profesionální	k2eAgNnSc1d1	profesionální
provedení	provedení	k1gNnSc1	provedení
bylo	být	k5eAaImAgNnS	být
navíc	navíc	k6eAd1	navíc
předpokladem	předpoklad	k1gInSc7	předpoklad
dokonalého	dokonalý	k2eAgInSc2d1	dokonalý
a	a	k8xC	a
bezchybného	bezchybný	k2eAgInSc2d1	bezchybný
výkonu	výkon	k1gInSc2	výkon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
dával	dávat	k5eAaImAgMnS	dávat
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
koncertu	koncert	k1gInSc2	koncert
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
něco	něco	k3yInSc1	něco
takového	takový	k3xDgNnSc2	takový
se	se	k3xPyFc4	se
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
na	na	k7c6	na
veřejných	veřejný	k2eAgNnPc6d1	veřejné
pódiích	pódium	k1gNnPc6	pódium
ještě	ještě	k6eAd1	ještě
neobjevilo	objevit	k5eNaPmAgNnS	objevit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
M.	M.	kA	M.
Kocába	kocába	k1gFnSc1	kocába
<g/>
)	)	kIx)	)
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k9	jako
odnož	odnož	k1gInSc1	odnož
Pražského	pražský	k2eAgMnSc2d1	pražský
Big	Big	k1gMnSc2	Big
Bandu	band	k1gInSc2	band
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
začátku	začátek	k1gInSc2	začátek
hrál	hrát	k5eAaImAgInS	hrát
čistý	čistý	k2eAgInSc1d1	čistý
jazz	jazz	k1gInSc1	jazz
rock	rock	k1gInSc4	rock
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jeho	jeho	k3xOp3gFnPc1	jeho
snahy	snaha	k1gFnPc1	snaha
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
"	"	kIx"	"
<g/>
Žízeň	žízeň	k1gFnSc1	žízeň
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zmrazení	zmrazení	k1gNnSc3	zmrazení
aktivit	aktivita	k1gFnPc2	aktivita
a	a	k8xC	a
přerušení	přerušení	k1gNnSc2	přerušení
činnosti	činnost	k1gFnSc2	činnost
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
objevil	objevit	k5eAaPmAgMnS	objevit
již	již	k6eAd1	již
proslulý	proslulý	k2eAgMnSc1d1	proslulý
kytarista	kytarista	k1gMnSc1	kytarista
Michal	Michal	k1gMnSc1	Michal
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
(	(	kIx(	(
<g/>
ex-Bohemia	ex-Bohemia	k1gFnSc1	ex-Bohemia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1980	[number]	k4	1980
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
byl	být	k5eAaImAgMnS	být
podívat	podívat	k5eAaImF	podívat
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
na	na	k7c4	na
festival	festival	k1gInSc4	festival
v	v	k7c6	v
Readingu	Reading	k1gInSc6	Reading
a	a	k8xC	a
tam	tam	k6eAd1	tam
mě	já	k3xPp1nSc4	já
ten	ten	k3xDgMnSc1	ten
rock	rock	k1gInSc4	rock
svou	svůj	k3xOyFgFnSc7	svůj
přímočarostí	přímočarost	k1gFnSc7	přímočarost
a	a	k8xC	a
nasazením	nasazení	k1gNnSc7	nasazení
znova	znova	k6eAd1	znova
oslovil	oslovit	k5eAaPmAgInS	oslovit
<g/>
.	.	kIx.	.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
a	a	k8xC	a
sháněl	shánět	k5eAaImAgMnS	shánět
někoho	někdo	k3yInSc4	někdo
s	s	k7c7	s
kým	kdo	k3yRnSc7	kdo
bych	by	kYmCp1nS	by
si	se	k3xPyFc3	se
rock	rock	k1gInSc1	rock
zase	zase	k9	zase
zahrál	zahrát	k5eAaPmAgInS	zahrát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Jistotu	jistota	k1gFnSc4	jistota
měl	mít	k5eAaImAgInS	mít
zatím	zatím	k6eAd1	zatím
jedinou	jediný	k2eAgFnSc4d1	jediná
–	–	k?	–
svého	svůj	k3xOyFgMnSc4	svůj
kamaráda	kamarád	k1gMnSc4	kamarád
a	a	k8xC	a
bubeníka	bubeník	k1gMnSc4	bubeník
z	z	k7c2	z
Expanze	expanze	k1gFnSc2	expanze
Jiřího	Jiří	k1gMnSc2	Jiří
"	"	kIx"	"
<g/>
Chomáče	chomáč	k1gInPc1	chomáč
<g/>
"	"	kIx"	"
Hrubeše	Hrubeš	k1gMnSc2	Hrubeš
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
akci	akce	k1gFnSc6	akce
v	v	k7c6	v
Redutě	reduta	k1gFnSc6	reduta
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
oslovil	oslovit	k5eAaPmAgMnS	oslovit
Michala	Michal	k1gMnSc4	Michal
Kocába	Kocáb	k1gMnSc4	Kocáb
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
ovšem	ovšem	k9	ovšem
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
chystaný	chystaný	k2eAgInSc1d1	chystaný
projekt	projekt	k1gInSc1	projekt
bude	být	k5eAaImBp3nS	být
opět	opět	k6eAd1	opět
jazz	jazz	k1gInSc1	jazz
rockový	rockový	k2eAgInSc1d1	rockový
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
mezitím	mezitím	k6eAd1	mezitím
obstaral	obstarat	k5eAaPmAgMnS	obstarat
nahrávky	nahrávka	k1gFnPc4	nahrávka
nových	nový	k2eAgFnPc2d1	nová
mladých	mladý	k2eAgFnPc2d1	mladá
kapel	kapela	k1gFnPc2	kapela
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pustí	pustit	k5eAaPmIp3nS	pustit
do	do	k7c2	do
rockového	rockový	k2eAgInSc2d1	rockový
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
nová	nový	k2eAgFnSc1d1	nová
vlna	vlna	k1gFnSc1	vlna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
protože	protože	k8xS	protože
Pavlíčkova	Pavlíčkův	k2eAgFnSc1d1	Pavlíčkova
Expanze	expanze	k1gFnSc1	expanze
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Kocábův	Kocábův	k2eAgInSc1d1	Kocábův
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
ve	v	k7c6	v
stádiu	stádium	k1gNnSc6	stádium
zimního	zimní	k2eAgInSc2d1	zimní
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
požíval	požívat	k5eAaImAgMnS	požívat
statusu	status	k1gInSc6	status
stále	stále	k6eAd1	stále
existující	existující	k2eAgFnSc2d1	existující
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
o	o	k7c6	o
názvu	název	k1gInSc6	název
souboru	soubor	k1gInSc2	soubor
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
členy	člen	k1gMnPc7	člen
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Kocábův	Kocábův	k2eAgMnSc1d1	Kocábův
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
spoluhráč	spoluhráč	k1gMnSc1	spoluhráč
<g/>
,	,	kIx,	,
i	i	k9	i
vlastně	vlastně	k9	vlastně
ještě	ještě	k9	ještě
z	z	k7c2	z
první	první	k4xOgFnSc2	první
sestavy	sestava	k1gFnSc2	sestava
Pražského	pražský	k2eAgInSc2d1	pražský
výběru	výběr	k1gInSc2	výběr
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
Žízeň	žízeň	k1gFnSc1	žízeň
<g/>
)	)	kIx)	)
Ondřej	Ondřej	k1gMnSc1	Ondřej
Soukup	Soukup	k1gMnSc1	Soukup
(	(	kIx(	(
<g/>
bg	bg	k?	bg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
bongista	bongista	k1gMnSc1	bongista
Jiří	Jiří	k1gMnSc1	Jiří
Tomek	Tomek	k1gMnSc1	Tomek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nastalo	nastat	k5eAaPmAgNnS	nastat
období	období	k1gNnSc1	období
kloubení	kloubení	k1gNnSc2	kloubení
dvou	dva	k4xCgInPc2	dva
skladatelských	skladatelský	k2eAgInPc2d1	skladatelský
rukopisů	rukopis	k1gInPc2	rukopis
–	–	k?	–
Pavlíčkova	Pavlíčkův	k2eAgInSc2d1	Pavlíčkův
rockového	rockový	k2eAgInSc2d1	rockový
a	a	k8xC	a
Kocábova	Kocábův	k2eAgInSc2d1	Kocábův
jazz-rockového	jazzockový	k2eAgInSc2d1	jazz-rockový
–	–	k?	–
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
repertoár	repertoár	k1gInSc1	repertoár
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
(	(	kIx(	(
<g/>
Kocáb	Kocáb	k1gMnSc1	Kocáb
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
seriál	seriál	k1gInSc4	seriál
Bigbít	bigbít	k1gInSc1	bigbít
přiznal	přiznat	k5eAaPmAgInS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
ani	ani	k9	ani
nepamatuje	pamatovat	k5eNaImIp3nS	pamatovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
písničky	písnička	k1gFnPc4	písnička
psali	psát	k5eAaImAgMnP	psát
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
dotvrzení	dotvrzení	k1gNnSc3	dotvrzení
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
přihlašuje	přihlašovat	k5eAaImIp3nS	přihlašovat
k	k	k7c3	k
novému	nový	k2eAgInSc3d1	nový
rockovému	rockový	k2eAgInSc3d1	rockový
proudu	proud	k1gInSc3	proud
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
obohatit	obohatit	k5eAaPmF	obohatit
sound	sound	k1gInSc4	sound
i	i	k8xC	i
těmi	ten	k3xDgMnPc7	ten
"	"	kIx"	"
<g/>
novovlnnými	novovlnný	k2eAgMnPc7d1	novovlnný
<g/>
"	"	kIx"	"
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k8xS	takže
se	se	k3xPyFc4	se
hudebníci	hudebník	k1gMnPc1	hudebník
začali	začít	k5eAaPmAgMnP	začít
oblékat	oblékat	k5eAaImF	oblékat
do	do	k7c2	do
bizarních	bizarní	k2eAgInPc2d1	bizarní
ne-rockových	ockův	k2eNgInPc2d1	-rockův
šatů	šat	k1gInPc2	šat
a	a	k8xC	a
oděvů	oděv	k1gInPc2	oděv
<g/>
,	,	kIx,	,
používali	používat	k5eAaImAgMnP	používat
masky	maska	k1gFnPc4	maska
<g/>
,	,	kIx,	,
paruky	paruka	k1gFnPc4	paruka
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
podivné	podivný	k2eAgFnPc4d1	podivná
serepetičky	serepetička	k1gFnPc4	serepetička
(	(	kIx(	(
<g/>
neobvyklé	obvyklý	k2eNgInPc4d1	neobvyklý
tvary	tvar	k1gInPc4	tvar
brýlí	brýle	k1gFnPc2	brýle
<g/>
,	,	kIx,	,
netradiční	tradiční	k2eNgFnPc4d1	netradiční
úpravy	úprava	k1gFnPc4	úprava
vlasů	vlas	k1gInPc2	vlas
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
M.	M.	kA	M.
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nikdy	nikdy	k6eAd1	nikdy
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
neříkali	říkat	k5eNaImAgMnP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
vytvoříme	vytvořit	k5eAaPmIp1nP	vytvořit
záměrně	záměrně	k6eAd1	záměrně
nějakou	nějaký	k3yIgFnSc4	nějaký
vnější	vnější	k2eAgFnSc4d1	vnější
image	image	k1gFnSc4	image
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
naprosto	naprosto	k6eAd1	naprosto
přirozený	přirozený	k2eAgInSc1d1	přirozený
<g/>
,	,	kIx,	,
každej	každej	k?	každej
se	se	k3xPyFc4	se
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
sám	sám	k3xTgMnSc1	sám
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
nemělo	mít	k5eNaImAgNnS	mít
to	ten	k3xDgNnSc1	ten
žádnou	žádný	k3yNgFnSc4	žádný
choreografii	choreografie	k1gFnSc4	choreografie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Ad	ad	k7c4	ad
hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
skupina	skupina	k1gFnSc1	skupina
přišla	přijít	k5eAaPmAgFnS	přijít
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
v	v	k7c6	v
Encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
jazzu	jazz	k1gInSc2	jazz
a	a	k8xC	a
moderní	moderní	k2eAgFnSc2d1	moderní
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
zkráceno	zkrácen	k2eAgNnSc1d1	zkráceno
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
s	s	k7c7	s
agresivním	agresivní	k2eAgInSc7d1	agresivní
rockem	rock	k1gInSc7	rock
<g/>
,	,	kIx,	,
vypůjčila	vypůjčit	k5eAaPmAgFnS	vypůjčit
si	se	k3xPyFc3	se
některé	některý	k3yIgFnPc4	některý
vokální	vokální	k2eAgFnSc1d1	vokální
inspirace	inspirace	k1gFnSc1	inspirace
z	z	k7c2	z
příbuzného	příbuzný	k1gMnSc2	příbuzný
punk	punk	k1gInSc4	punk
rocku	rock	k1gInSc2	rock
(	(	kIx(	(
<g/>
výraz	výraz	k1gInSc1	výraz
<g/>
,	,	kIx,	,
popření	popření	k1gNnSc1	popření
́	́	k?	́
<g/>
nezbytné	zbytný	k2eNgFnSc2d1	zbytný
<g/>
́	́	k?	́
melodiky	melodika	k1gFnPc4	melodika
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
,	,	kIx,	,
recitace	recitace	k1gFnSc1	recitace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
oproti	oproti	k7c3	oproti
punkovému	punkový	k2eAgNnSc3d1	punkové
apriorně	apriorně	k6eAd1	apriorně
zdůrazňovanému	zdůrazňovaný	k2eAgInSc3d1	zdůrazňovaný
primitivismu	primitivismus	k1gInSc3	primitivismus
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
složitou	složitý	k2eAgFnSc4d1	složitá
hudební	hudební	k2eAgFnSc4d1	hudební
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
využívá	využívat	k5eAaImIp3nS	využívat
široké	široký	k2eAgFnPc4d1	široká
palety	paleta	k1gFnPc4	paleta
stylů	styl	k1gInPc2	styl
a	a	k8xC	a
univerzálních	univerzální	k2eAgFnPc2d1	univerzální
tvůrčích	tvůrčí	k2eAgFnPc2d1	tvůrčí
schopností	schopnost	k1gFnPc2	schopnost
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Jazzová	jazzový	k2eAgFnSc1d1	jazzová
minulost	minulost	k1gFnSc1	minulost
většiny	většina	k1gFnSc2	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vylučovala	vylučovat	k5eAaImAgFnS	vylučovat
příklon	příklon	k1gInSc4	příklon
ke	k	k7c3	k
střednímu	střední	k2eAgInSc3d1	střední
proudu	proud	k1gInSc3	proud
pop	pop	k1gMnSc1	pop
music	music	k1gMnSc1	music
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
též	též	k9	též
negovala	negovat	k5eAaImAgFnS	negovat
komplikované	komplikovaný	k2eAgFnPc4d1	komplikovaná
formy	forma	k1gFnPc4	forma
rocku	rock	k1gInSc2	rock
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
např.	např.	kA	např.
art	art	k?	art
rock	rock	k1gInSc1	rock
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nepřeberné	přeberný	k2eNgNnSc4d1	nepřeberné
množství	množství	k1gNnSc4	množství
melodických	melodický	k2eAgInPc2d1	melodický
<g/>
,	,	kIx,	,
harmonických	harmonický	k2eAgInPc2d1	harmonický
i	i	k8xC	i
aranžérských	aranžérský	k2eAgInPc2d1	aranžérský
nápadů	nápad	k1gInPc2	nápad
skupina	skupina	k1gFnSc1	skupina
rámovala	rámovat	k5eAaImAgFnS	rámovat
nezbytným	nezbytný	k2eAgInSc7d1	nezbytný
čtyřdobým	čtyřdobý	k2eAgInSc7d1	čtyřdobý
taktem	takt	k1gInSc7	takt
<g/>
:	:	kIx,	:
neúprosně	úprosně	k6eNd1	úprosně
pulsující	pulsující	k2eAgFnSc1d1	pulsující
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
rytmika	rytmika	k1gFnSc1	rytmika
s	s	k7c7	s
nevšedními	všední	k2eNgFnPc7d1	nevšední
nosnými	nosný	k2eAgFnPc7d1	nosná
basovými	basový	k2eAgFnPc7d1	basová
figurami	figura	k1gFnPc7	figura
žene	hnát	k5eAaImIp3nS	hnát
před	před	k7c7	před
sebou	se	k3xPyFc7	se
Kocábovy	Kocábův	k2eAgInPc4d1	Kocábův
kratičké	kratičký	k2eAgInPc4d1	kratičký
syntezátorové	syntezátorový	k2eAgInPc4d1	syntezátorový
motivy	motiv	k1gInPc4	motiv
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
<g/>
,	,	kIx,	,
a	a	k8xC	a
Pavlíčkovy	Pavlíčkův	k2eAgInPc1d1	Pavlíčkův
znamenité	znamenitý	k2eAgInPc1d1	znamenitý
kytarové	kytarový	k2eAgInPc1d1	kytarový
riffy	riff	k1gInPc1	riff
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
doprovodného	doprovodný	k2eAgInSc2d1	doprovodný
orchestru	orchestr	k1gInSc2	orchestr
Karla	Karel	k1gMnSc4	Karel
Gotta	Gott	k1gMnSc4	Gott
baskytarista	baskytarista	k1gMnSc1	baskytarista
Ondřej	Ondřej	k1gMnSc1	Ondřej
Soukup	Soukup	k1gMnSc1	Soukup
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
to	ten	k3xDgNnSc4	ten
komentovala	komentovat	k5eAaBmAgFnS	komentovat
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
S.	S.	kA	S.
<g/>
O.S.	O.S.	k1gFnSc6	O.S.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
prachy	prach	k1gInPc4	prach
<g/>
,	,	kIx,	,
jenom	jenom	k9	jenom
prachy	prach	k1gInPc1	prach
nečekají	čekat	k5eNaImIp3nP	čekat
<g/>
,	,	kIx,	,
jen	jen	k9	jen
po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
sáhneš	sáhnout	k5eAaPmIp2nS	sáhnout
a	a	k8xC	a
už	už	k6eAd1	už
tě	ty	k3xPp2nSc4	ty
mají	mít	k5eAaImIp3nP	mít
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
písni	píseň	k1gFnSc6	píseň
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
objevuje	objevovat	k5eAaImIp3nS	objevovat
samotný	samotný	k2eAgMnSc1d1	samotný
Soukup	Soukup	k1gMnSc1	Soukup
a	a	k8xC	a
sice	sice	k8xC	sice
fingovaným	fingovaný	k2eAgNnSc7d1	fingované
zvednutím	zvednutí	k1gNnSc7	zvednutí
telefonu	telefon	k1gInSc2	telefon
(	(	kIx(	(
<g/>
,,	,,	k?	,,
<g/>
Haló	haló	k0	haló
<g/>
,	,	kIx,	,
Soukup	Soukup	k1gMnSc1	Soukup
<g/>
...	...	k?	...
Haló	haló	k0	haló
<g/>
..	..	k?	..
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
zdařilou	zdařilý	k2eAgFnSc7d1	zdařilá
náhradou	náhrada	k1gFnSc7	náhrada
za	za	k7c2	za
Soukupa	Soukup	k1gMnSc2	Soukup
se	se	k3xPyFc4	se
však	však	k9	však
stal	stát	k5eAaPmAgMnS	stát
Vilém	Vilém	k1gMnSc1	Vilém
Čok	Čok	k1gMnSc1	Čok
<g/>
,	,	kIx,	,
baskytarista	baskytarista	k1gMnSc1	baskytarista
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Zikkuratu	zikkurat	k1gInSc2	zikkurat
<g/>
.	.	kIx.	.
</s>
<s>
Čok	Čok	k?	Čok
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
Pražského	pražský	k2eAgInSc2d1	pražský
výběru	výběr	k1gInSc2	výběr
nechtěl	chtít	k5eNaImAgInS	chtít
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
moc	moc	k6eAd1	moc
jsem	být	k5eAaImIp1nS	být
je	on	k3xPp3gMnPc4	on
neznal	neznat	k5eAaImAgMnS	neznat
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
je	on	k3xPp3gNnPc4	on
někde	někde	k6eAd1	někde
slyšel	slyšet	k5eAaImAgMnS	slyšet
<g/>
,	,	kIx,	,
moc	moc	k6eAd1	moc
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc1	ten
nelíbilo	líbit	k5eNaImAgNnS	líbit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
na	na	k7c4	na
nabídku	nabídka	k1gFnSc4	nabídka
kývl	kývnout	k5eAaPmAgInS	kývnout
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
měly	mít	k5eAaImAgInP	mít
neshody	neshoda	k1gFnPc4	neshoda
v	v	k7c6	v
Zikkuratu	zikkurat	k1gInSc6	zikkurat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jelikož	jelikož	k8xS	jelikož
záhy	záhy	k6eAd1	záhy
odpadl	odpadnout	k5eAaPmAgInS	odpadnout
"	"	kIx"	"
<g/>
whisky	whisky	k1gFnSc1	whisky
man	man	k1gMnSc1	man
<g/>
"	"	kIx"	"
Jiří	Jiří	k1gMnSc1	Jiří
Tomek	Tomek	k1gMnSc1	Tomek
(	(	kIx(	(
<g/>
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
se	se	k3xPyFc4	se
objevoval	objevovat	k5eAaImAgInS	objevovat
jako	jako	k9	jako
host-tanečník	hostanečník	k1gInSc1	host-tanečník
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
skladbě	skladba	k1gFnSc6	skladba
<g/>
)	)	kIx)	)
ustálila	ustálit	k5eAaPmAgFnS	ustálit
se	se	k3xPyFc4	se
sestava	sestava	k1gFnSc1	sestava
Pražského	pražský	k2eAgInSc2d1	pražský
výběru	výběr	k1gInSc2	výběr
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
nejslavnější	slavný	k2eAgFnSc6d3	nejslavnější
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jak	jak	k6eAd1	jak
byla	být	k5eAaImAgFnS	být
vídána	vídat	k5eAaImNgFnS	vídat
na	na	k7c6	na
českých	český	k2eAgNnPc6d1	české
pódiích	pódium	k1gNnPc6	pódium
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1981	[number]	k4	1981
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
Michal	Michal	k1gMnSc1	Michal
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
(	(	kIx(	(
<g/>
lg	lg	k?	lg
<g/>
,	,	kIx,	,
voc	voc	k?	voc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Kocáb	Kocáb	k1gMnSc1	Kocáb
(	(	kIx(	(
<g/>
voc	voc	k?	voc
<g/>
,	,	kIx,	,
ks	ks	kA	ks
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Hrubeš	Hrubeš	k1gMnSc1	Hrubeš
(	(	kIx(	(
<g/>
ds	ds	k?	ds
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vilém	Vilém	k1gMnSc1	Vilém
Čok	Čok	k1gMnSc1	Čok
(	(	kIx(	(
<g/>
bg	bg	k?	bg
<g/>
,	,	kIx,	,
voc	voc	k?	voc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ad	ad	k7c4	ad
texty	text	k1gInPc4	text
<g/>
:	:	kIx,	:
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
nechtěně	chtěně	k6eNd1	chtěně
proslavila	proslavit	k5eAaPmAgFnS	proslavit
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
svahilštinou	svahilština	k1gFnSc7	svahilština
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gNnSc4	její
vlastní	vlastní	k2eAgNnSc4d1	vlastní
pojmenování	pojmenování	k1gNnSc4	pojmenování
pro	pro	k7c4	pro
vcelku	vcelku	k6eAd1	vcelku
běžnou	běžný	k2eAgFnSc4d1	běžná
praxi	praxe	k1gFnSc4	praxe
–	–	k?	–
zvukomalebné	zvukomalebný	k2eAgInPc4d1	zvukomalebný
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
rádobyanglická	rádobyanglický	k2eAgNnPc1d1	rádobyanglický
slova	slovo	k1gNnPc1	slovo
nedávala	dávat	k5eNaImAgNnP	dávat
žádný	žádný	k3yNgInSc4	žádný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
zpívat	zpívat	k5eAaImF	zpívat
telefonní	telefonní	k2eAgInSc4d1	telefonní
seznam	seznam	k1gInSc4	seznam
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Coby	Coby	k?	Coby
dobrá	dobrý	k2eAgFnSc1d1	dobrá
pomůcka	pomůcka	k1gFnSc1	pomůcka
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
objevila	objevit	k5eAaPmAgFnS	objevit
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
beatový	beatový	k2eAgMnSc1d1	beatový
skladatel	skladatel	k1gMnSc1	skladatel
použil	použít	k5eAaPmAgMnS	použít
simulovanou	simulovaný	k2eAgFnSc4d1	simulovaná
angličtinu	angličtina	k1gFnSc4	angličtina
jako	jako	k8xC	jako
podklad	podklad	k1gInSc4	podklad
a	a	k8xC	a
vodítko	vodítko	k1gNnSc4	vodítko
pro	pro	k7c4	pro
české	český	k2eAgNnSc4d1	české
otextování	otextování	k1gNnSc4	otextování
písničky	písnička	k1gFnSc2	písnička
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
to	ten	k3xDgNnSc1	ten
dotáhl	dotáhnout	k5eAaPmAgInS	dotáhnout
ještě	ještě	k6eAd1	ještě
dál	daleko	k6eAd2	daleko
–	–	k?	–
prostě	prostě	k6eAd1	prostě
se	s	k7c7	s
svahilštinou	svahilština	k1gFnSc7	svahilština
obrážel	obrážet	k5eAaImAgMnS	obrážet
všechny	všechen	k3xTgInPc4	všechen
koncerty	koncert	k1gInPc4	koncert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Nebylo	být	k5eNaImAgNnS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
chtěli	chtít	k5eAaImAgMnP	chtít
zpívat	zpívat	k5eAaImF	zpívat
́	́	k?	́
<g/>
svahilštinou	svahilština	k1gFnSc7	svahilština
<g/>
́	́	k?	́
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
Kocáb	Kocáb	k1gMnSc1	Kocáb
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Prostě	prostě	k6eAd1	prostě
jsme	být	k5eAaImIp1nP	být
nestačili	stačit	k5eNaBmAgMnP	stačit
udělat	udělat	k5eAaPmF	udělat
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Nestihli	stihnout	k5eNaPmAgMnP	stihnout
jsme	být	k5eAaImIp1nP	být
to	ten	k3xDgNnSc4	ten
<g/>
.	.	kIx.	.
</s>
<s>
Psát	psát	k5eAaImF	psát
jsme	být	k5eAaImIp1nP	být
je	on	k3xPp3gMnPc4	on
neuměli	umět	k5eNaImAgMnP	umět
a	a	k8xC	a
dlouho	dlouho	k6eAd1	dlouho
jsme	být	k5eAaImIp1nP	být
hledali	hledat	k5eAaImAgMnP	hledat
někoho	někdo	k3yInSc4	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
by	by	kYmCp3nS	by
nám	my	k3xPp1nPc3	my
ty	ten	k3xDgFnPc4	ten
písničky	písnička	k1gFnPc4	písnička
otextoval	otextovat	k5eAaPmAgMnS	otextovat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
trefil	trefit	k5eAaPmAgMnS	trefit
Fr.	Fr.	k1gMnSc1	Fr.
Ringo	Ringo	k1gMnSc1	Ringo
Čech	Čech	k1gMnSc1	Čech
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
dobrý	dobrý	k2eAgInSc4d1	dobrý
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
takový	takový	k3xDgInSc1	takový
připitomělý	připitomělý	k2eAgInSc1d1	připitomělý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždycky	vždycky	k6eAd1	vždycky
nesly	nést	k5eAaImAgFnP	nést
nějaký	nějaký	k3yIgInSc4	nějaký
poselství	poselství	k1gNnSc4	poselství
<g/>
,	,	kIx,	,
těžko	těžko	k6eAd1	těžko
pochopitelný	pochopitelný	k2eAgInSc1d1	pochopitelný
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
komouše	komouš	k1gMnPc4	komouš
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nelíbilo	líbit	k5eNaImAgNnS	líbit
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Zubatá	zubatá	k1gFnSc1	zubatá
<g/>
"	"	kIx"	"
–	–	k?	–
koho	kdo	k3yRnSc2	kdo
prej	prej	k?	prej
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc4	ten
týká	týkat	k5eAaImIp3nS	týkat
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
všech	všecek	k3xTgInPc2	všecek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Juraj	Juraj	k1gMnSc1	Juraj
Herz	Herz	k1gMnSc1	Herz
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
film	film	k1gInSc1	film
"	"	kIx"	"
<g/>
Straka	Straka	k1gMnSc1	Straka
v	v	k7c6	v
hrsti	hrst	k1gFnSc6	hrst
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
okamžitě	okamžitě	k6eAd1	okamžitě
skončil	skončit	k5eAaPmAgMnS	skončit
v	v	k7c6	v
trezoru	trezor	k1gInSc6	trezor
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Po	po	k7c6	po
tomhle	tenhle	k3xDgInSc6	tenhle
filmu	film	k1gInSc6	film
si	se	k3xPyFc3	se
nás	my	k3xPp1nPc2	my
začali	začít	k5eAaPmAgMnP	začít
fízlové	fízlové	k?	fízlové
všímat	všímat	k5eAaImF	všímat
naplno	naplno	k6eAd1	naplno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komparsu	kompars	k1gInSc6	kompars
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
podepsali	podepsat	k5eAaPmAgMnP	podepsat
Chartu	charta	k1gFnSc4	charta
77	[number]	k4	77
a	a	k8xC	a
StB	StB	k1gFnSc1	StB
začala	začít	k5eAaPmAgFnS	začít
ten	ten	k3xDgInSc4	ten
film	film	k1gInSc4	film
mapovat	mapovat	k5eAaImF	mapovat
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
podílel	podílet	k5eAaImAgInS	podílet
a	a	k8xC	a
o	o	k7c4	o
co	co	k3yInSc4	co
tu	tu	k6eAd1	tu
vlastně	vlastně	k9	vlastně
jde	jít	k5eAaImIp3nS	jít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nejpopulárnější	populární	k2eAgFnSc1d3	nejpopulárnější
českou	český	k2eAgFnSc7d1	Česká
rockovou	rockový	k2eAgFnSc7d1	rocková
kapelou	kapela	k1gFnSc7	kapela
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
něco	něco	k3yInSc4	něco
nejpopulárnější	populární	k2eAgFnSc1d3	nejpopulárnější
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc4	ten
bolševik	bolševik	k1gMnSc1	bolševik
posvětil	posvětit	k5eAaPmAgMnS	posvětit
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
se	se	k3xPyFc4	se
ptát	ptát	k5eAaImF	ptát
–	–	k?	–
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
populární	populární	k2eAgInSc1d1	populární
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
jde	jít	k5eAaImIp3nS	jít
proti	proti	k7c3	proti
mně	já	k3xPp1nSc3	já
<g/>
?	?	kIx.	?
</s>
<s>
A	a	k9	a
hned	hned	k6eAd1	hned
si	se	k3xPyFc3	se
odpověděl	odpovědět	k5eAaPmAgInS	odpovědět
–	–	k?	–
ano	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
natočené	natočený	k2eAgNnSc1d1	natočené
album	album	k1gNnSc1	album
"	"	kIx"	"
<g/>
Straka	Straka	k1gMnSc1	Straka
v	v	k7c6	v
hrsti	hrst	k1gFnSc6	hrst
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zdařilou	zdařilý	k2eAgFnSc4d1	zdařilá
kolekci	kolekce	k1gFnSc4	kolekce
vlastních	vlastní	k2eAgFnPc2d1	vlastní
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
si	se	k3xPyFc3	se
rockový	rockový	k2eAgInSc1d1	rockový
lid	lid	k1gInSc1	lid
zpíval	zpívat	k5eAaImAgInS	zpívat
už	už	k6eAd1	už
během	během	k7c2	během
neustále	neustále	k6eAd1	neustále
vyprodaných	vyprodaný	k2eAgInPc2d1	vyprodaný
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
samozřejmě	samozřejmě	k6eAd1	samozřejmě
v	v	k7c6	v
onom	onen	k3xDgInSc6	onen
roce	rok	k1gInSc6	rok
nevyšla	vyjít	k5eNaPmAgFnS	vyjít
(	(	kIx(	(
<g/>
vyšla	vyjít	k5eAaPmAgFnS	vyjít
až	až	k9	až
o	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
šířena	šířit	k5eAaImNgFnS	šířit
nahráváním	nahrávání	k1gNnSc7	nahrávání
z	z	k7c2	z
pásku	pásek	k1gInSc2	pásek
na	na	k7c4	na
pásek	pásek	k1gInSc4	pásek
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
republice	republika	k1gFnSc6	republika
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
černý	černý	k2eAgInSc1d1	černý
mor	mor	k1gInSc1	mor
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
to	ten	k3xDgNnSc1	ten
už	už	k6eAd1	už
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
Pražským	pražský	k2eAgInSc7d1	pražský
výběrem	výběr	k1gInSc7	výběr
vznášel	vznášet	k5eAaImAgInS	vznášet
Damoklův	Damoklův	k2eAgInSc1d1	Damoklův
meč	meč	k1gInSc1	meč
zákazu	zákaz	k1gInSc2	zákaz
<g/>
,	,	kIx,	,
jen	jen	k6eAd1	jen
se	se	k3xPyFc4	se
čekalo	čekat	k5eAaImAgNnS	čekat
na	na	k7c4	na
nějakou	nějaký	k3yIgFnSc4	nějaký
záminku	záminka	k1gFnSc4	záminka
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
koncerty	koncert	k1gInPc1	koncert
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgFnPc6	který
skupina	skupina	k1gFnSc1	skupina
"	"	kIx"	"
<g/>
prodávala	prodávat	k5eAaImAgFnS	prodávat
neoficiální	oficiální	k2eNgFnSc1d1	neoficiální
plakáty	plakát	k1gInPc4	plakát
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
nezákonně	zákonně	k6eNd1	zákonně
finančně	finančně	k6eAd1	finančně
obohacovala	obohacovat	k5eAaImAgFnS	obohacovat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
uvedla	uvést	k5eAaPmAgFnS	uvést
načerno	načerno	k6eAd1	načerno
jako	jako	k8xC	jako
předskokana	předskokan	k1gMnSc4	předskokan
kapelu	kapela	k1gFnSc4	kapela
Trifidi	Trifid	k1gMnPc5	Trifid
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
a	a	k8xC	a
hrála	hrát	k5eAaImAgFnS	hrát
místo	místo	k7c2	místo
tří	tři	k4xCgInPc2	tři
koncertů	koncert	k1gInPc2	koncert
čtyři	čtyři	k4xCgMnPc4	čtyři
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
"	"	kIx"	"
<g/>
nebylo	být	k5eNaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
ve	v	k7c6	v
smlouvě	smlouva	k1gFnSc6	smlouva
s	s	k7c7	s
PKS	PKS	kA	PKS
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
argumenty	argument	k1gInPc1	argument
nejprve	nejprve	k6eAd1	nejprve
zazněly	zaznít	k5eAaPmAgInP	zaznít
v	v	k7c6	v
krajském	krajský	k2eAgInSc6d1	krajský
bolševickém	bolševický	k2eAgInSc6d1	bolševický
plátku	plátek	k1gInSc6	plátek
Pochodeň	pochodeň	k1gFnSc4	pochodeň
<g/>
,	,	kIx,	,
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
i	i	k9	i
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
v	v	k7c6	v
Tribuně	tribuna	k1gFnSc6	tribuna
a	a	k8xC	a
Rudém	rudý	k2eAgNnSc6d1	Rudé
Právu	právo	k1gNnSc6	právo
a	a	k8xC	a
zahájily	zahájit	k5eAaPmAgInP	zahájit
tak	tak	k9	tak
brojení	brojení	k1gNnSc1	brojení
úřadů	úřad	k1gInPc2	úřad
proti	proti	k7c3	proti
české	český	k2eAgFnSc3d1	Česká
nové	nový	k2eAgFnSc3d1	nová
vlně	vlna	k1gFnSc3	vlna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
1983	[number]	k4	1983
byl	být	k5eAaImAgInS	být
závěrečným	závěrečný	k2eAgInSc7d1	závěrečný
rokem	rok	k1gInSc7	rok
skupiny	skupina	k1gFnSc2	skupina
Pražský	pražský	k2eAgInSc4d1	pražský
výběr	výběr	k1gInSc4	výběr
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
o	o	k7c6	o
souboru	soubor	k1gInSc6	soubor
natočil	natočit	k5eAaBmAgMnS	natočit
dokument	dokument	k1gInSc4	dokument
"	"	kIx"	"
<g/>
Nesmírná	smírný	k2eNgFnSc1d1	nesmírná
únava	únava	k1gFnSc1	únava
materiálu	materiál	k1gInSc2	materiál
<g/>
"	"	kIx"	"
režisér	režisér	k1gMnSc1	režisér
Michal	Michal	k1gMnSc1	Michal
Herz	Herz	k1gMnSc1	Herz
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
bubeník	bubeník	k1gMnSc1	bubeník
Jiří	Jiří	k1gMnSc1	Jiří
Hrubeš	Hrubeš	k1gMnSc1	Hrubeš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
České	český	k2eAgInPc1d1	český
poměry	poměr	k1gInPc1	poměr
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
svérázné	svérázný	k2eAgFnPc1d1	svérázná
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
píše	psát	k5eAaImIp3nS	psát
se	se	k3xPyFc4	se
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Excentrici	excentrik	k1gMnPc1	excentrik
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
V	v	k7c6	v
očích	oko	k1gNnPc6	oko
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
ovlivněných	ovlivněný	k2eAgInPc2d1	ovlivněný
nepřiměřeným	přiměřený	k2eNgNnSc7d1	nepřiměřené
skandalizováním	skandalizování	k1gNnSc7	skandalizování
koncertu	koncert	k1gInSc2	koncert
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
pakulturní	pakulturní	k2eAgInSc1d1	pakulturní
soubor	soubor	k1gInSc4	soubor
šířící	šířící	k2eAgInSc4d1	šířící
brak	brak	k1gInSc4	brak
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
podle	podle	k7c2	podle
názorů	názor	k1gInPc2	názor
některých	některý	k3yIgMnPc2	některý
zarytých	zarytý	k2eAgMnPc2d1	zarytý
novovlňáků	novovlňák	k1gMnPc2	novovlňák
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
podvodníky	podvodník	k1gMnPc4	podvodník
a	a	k8xC	a
státem	stát	k1gInSc7	stát
placené	placený	k2eAgFnPc4d1	placená
rockery	rocker	k1gMnPc7	rocker
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jenom	jenom	k9	jenom
využili	využít	k5eAaPmAgMnP	využít
módní	módní	k2eAgMnPc1d1	módní
vlny	vlna	k1gFnSc2	vlna
k	k	k7c3	k
vlastním	vlastní	k2eAgFnPc3d1	vlastní
egoistickým	egoistický	k2eAgFnPc3d1	egoistická
kalkulacím	kalkulace	k1gFnPc3	kalkulace
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgMnSc1d1	hudební
publicista	publicista	k1gMnSc1	publicista
Ondřej	Ondřej	k1gMnSc1	Ondřej
Konrád	Konrád	k1gMnSc1	Konrád
dnes	dnes	k6eAd1	dnes
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
byl	být	k5eAaImAgInS	být
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
záležitost	záležitost	k1gFnSc1	záležitost
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
dívat	dívat	k5eAaImF	dívat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
pohledů	pohled	k1gInPc2	pohled
<g/>
:	:	kIx,	:
za	za	k7c4	za
prvé	prvý	k4xOgFnPc4	prvý
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
trochu	trochu	k6eAd1	trochu
vypočítavost	vypočítavost	k1gFnSc1	vypočítavost
od	od	k7c2	od
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
muzikantsky	muzikantsky	k6eAd1	muzikantsky
hodně	hodně	k6eAd1	hodně
zdatní	zdatný	k2eAgMnPc1d1	zdatný
a	a	k8xC	a
protřelí	protřelý	k2eAgMnPc1d1	protřelý
a	a	k8xC	a
využili	využít	k5eAaPmAgMnP	využít
nějakého	nějaký	k3yIgMnSc4	nějaký
výbuchu	výbuch	k1gInSc6	výbuch
té	ten	k3xDgFnSc2	ten
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
se	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
jejích	její	k3xOp3gMnPc2	její
prostředků	prostředek	k1gInPc2	prostředek
a	a	k8xC	a
svými	svůj	k3xOyFgFnPc7	svůj
znalostmi	znalost	k1gFnPc7	znalost
a	a	k8xC	a
schopnostmi	schopnost	k1gFnPc7	schopnost
to	ten	k3xDgNnSc4	ten
udělali	udělat	k5eAaPmAgMnP	udělat
mnohem	mnohem	k6eAd1	mnohem
dokonaleji	dokonale	k6eAd2	dokonale
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
nepovažoval	považovat	k5eNaImAgInS	považovat
bych	by	kYmCp1nS	by
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
za	za	k7c4	za
podvodníky	podvodník	k1gMnPc4	podvodník
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
rozhodně	rozhodně	k6eAd1	rozhodně
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Mám	mít	k5eAaImIp1nS	mít
spíš	spíš	k9	spíš
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
spontánně	spontánně	k6eAd1	spontánně
<g/>
.	.	kIx.	.
</s>
<s>
Nemyslím	myslet	k5eNaImIp1nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
jim	on	k3xPp3gMnPc3	on
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
nějaký	nějaký	k3yIgInSc4	nějaký
laciný	laciný	k2eAgInSc4d1	laciný
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ovšem	ovšem	k9	ovšem
měli	mít	k5eAaImAgMnP	mít
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
obrovský	obrovský	k2eAgInSc1d1	obrovský
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
se	se	k3xPyFc4	se
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
vynuceného	vynucený	k2eAgInSc2d1	vynucený
zákazu	zákaz	k1gInSc2	zákaz
dala	dát	k5eAaPmAgFnS	dát
dohromady	dohromady	k6eAd1	dohromady
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Výběr	výběr	k1gInSc1	výběr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čeští	český	k2eAgMnPc1d1	český
fanoušci	fanoušek	k1gMnPc1	fanoušek
je	on	k3xPp3gMnPc4	on
během	během	k7c2	během
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
mohli	moct	k5eAaImAgMnP	moct
vidět	vidět	k5eAaImF	vidět
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
32	[number]	k4	32
převážně	převážně	k6eAd1	převážně
vyprodaných	vyprodaný	k2eAgInPc6d1	vyprodaný
koncertech	koncert	k1gInPc6	koncert
a	a	k8xC	a
6	[number]	k4	6
festivalech	festival	k1gInPc6	festival
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2012	[number]	k4	2012
ohlásila	ohlásit	k5eAaPmAgFnS	ohlásit
skupina	skupina	k1gFnSc1	skupina
reunion	reunion	k1gInSc4	reunion
a	a	k8xC	a
odehrála	odehrát	k5eAaPmAgFnS	odehrát
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
pět	pět	k4xCc4	pět
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turné	turné	k1gNnSc6	turné
vyjela	vyjet	k5eAaPmAgFnS	vyjet
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
bubeníky	bubeník	k1gMnPc7	bubeník
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
Kocáb	Kocáb	k1gMnSc1	Kocáb
–	–	k?	–
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
–	–	k?	–
Čok	Čok	k1gMnSc1	Čok
–	–	k?	–
Hrubeš	Hrubeš	k1gMnSc1	Hrubeš
–	–	k?	–
Kryšpín	Kryšpín	k1gMnSc1	Kryšpín
<g/>
.	.	kIx.	.
</s>
<s>
Koncertní	koncertní	k2eAgNnSc1d1	koncertní
turné	turné	k1gNnSc1	turné
shlédlo	shlédnout	k5eAaPmAgNnS	shlédnout
v	v	k7c6	v
součtu	součet	k1gInSc6	součet
100	[number]	k4	100
000	[number]	k4	000
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
karlovarského	karlovarský	k2eAgInSc2d1	karlovarský
koncertu	koncert	k1gInSc2	koncert
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
natočila	natočit	k5eAaBmAgFnS	natočit
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
záznam	záznam	k1gInSc1	záznam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byl	být	k5eAaImAgMnS	být
Michal	Michal	k1gMnSc1	Michal
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
oceněn	oceněn	k2eAgMnSc1d1	oceněn
cenami	cena	k1gFnPc7	cena
Anděl	Anděla	k1gFnPc2	Anděla
do	do	k7c2	do
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
skupina	skupina	k1gFnSc1	skupina
odehrála	odehrát	k5eAaPmAgFnS	odehrát
podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
slov	slovo	k1gNnPc2	slovo
největší	veliký	k2eAgInSc1d3	veliký
koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
<g/>
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
O2	O2	k1gFnSc6	O2
aréně	aréna	k1gFnSc6	aréna
za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
Českého	český	k2eAgInSc2d1	český
národního	národní	k2eAgInSc2d1	národní
symfonického	symfonický	k2eAgInSc2d1	symfonický
orchestru	orchestr	k1gInSc2	orchestr
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
hosté	host	k1gMnPc1	host
přišli	přijít	k5eAaPmAgMnP	přijít
Ondřej	Ondřej	k1gMnSc1	Ondřej
Soukup	Soukup	k1gMnSc1	Soukup
<g/>
,	,	kIx,	,
<g/>
Iva	Iva	k1gFnSc1	Iva
Pazderková	Pazderková	k1gFnSc1	Pazderková
i	i	k8xC	i
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Dyk	Dyk	k?	Dyk
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
koncert	koncert	k1gInSc4	koncert
uváděla	uvádět	k5eAaImAgFnS	uvádět
skupina	skupina	k1gFnSc1	skupina
jako	jako	k8xC	jako
poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
poté	poté	k6eAd1	poté
ještě	ještě	k9	ještě
párkrát	párkrát	k6eAd1	párkrát
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2018	[number]	k4	2018
skupina	skupina	k1gFnSc1	skupina
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
jako	jako	k9	jako
předkapela	předkapela	k1gFnSc1	předkapela
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnPc2	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
devátou	devátý	k4xOgFnSc7	devátý
hodinou	hodina	k1gFnSc7	hodina
večerní	večerní	k2eAgFnSc7d1	večerní
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
v	v	k7c6	v
Letňanech	Letňan	k1gMnPc6	Letňan
<g/>
.	.	kIx.	.
</s>
<s>
Kocáb	Kocáb	k1gMnSc1	Kocáb
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
velká	velký	k2eAgFnSc1d1	velká
čest	čest	k1gFnSc1	čest
a	a	k8xC	a
také	také	k9	také
vyvrcholení	vyvrcholení	k1gNnSc1	vyvrcholení
naší	náš	k3xOp1gFnSc2	náš
dráhy	dráha	k1gFnSc2	dráha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Kvůli	kvůli	k7c3	kvůli
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gInSc4	Stones
jsem	být	k5eAaImIp1nS	být
začal	začít	k5eAaPmAgMnS	začít
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
"	"	kIx"	"
dodal	dodat	k5eAaPmAgMnS	dodat
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
rozehřála	rozehřát	k5eAaPmAgFnS	rozehřát
publikum	publikum	k1gNnSc4	publikum
skladbou	skladba	k1gFnSc7	skladba
Hrabě	Hrabě	k1gMnSc1	Hrabě
X	X	kA	X
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
vystoupení	vystoupení	k1gNnSc4	vystoupení
zakončila	zakončit	k5eAaPmAgFnS	zakončit
skladbou	skladba	k1gFnSc7	skladba
Snaživec	snaživec	k1gMnSc1	snaživec
<g/>
,	,	kIx,	,
hostem	host	k1gMnSc7	host
skupiny	skupina	k1gFnSc2	skupina
byla	být	k5eAaImAgFnS	být
Iva	Iva	k1gFnSc1	Iva
Pazderková	Pazderková	k1gFnSc1	Pazderková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
-	-	kIx~	-
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Kocáb	Kocáb	k1gMnSc1	Kocáb
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
Čok	Čok	k1gMnSc1	Čok
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Hrubeš	Hrubeš	k1gMnSc1	Hrubeš
</s>
</p>
<p>
<s>
==	==	k?	==
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
II	II	kA	II
(	(	kIx(	(
<g/>
od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Kocáb	Kocáb	k1gMnSc1	Kocáb
</s>
</p>
<p>
<s>
Klaudius	Klaudius	k1gMnSc1	Klaudius
Kryšpín	Kryšpín	k1gMnSc1	Kryšpín
</s>
</p>
<p>
<s>
Glenn	Glenn	k1gMnSc1	Glenn
Proudfoot	Proudfoot	k1gMnSc1	Proudfoot
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Scheufler	Scheufler	k1gMnSc1	Scheufler
</s>
</p>
<p>
<s>
Zlata	Zlata	k1gFnSc1	Zlata
Kinská	Kinská	k1gFnSc1	Kinská
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
výběrŽízeň	výběrŽízeň	k1gFnSc4	výběrŽízeň
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Výběr	výběr	k1gInSc1	výběr
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Běr	Běr	k?	Běr
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
Pražský	pražský	k2eAgInSc4d1	pražský
výběr	výběr	k1gInSc4	výběr
IIVymlácený	IIVymlácený	k2eAgMnSc1d1	IIVymlácený
rockový	rockový	k2eAgMnSc1d1	rockový
palice	palice	k1gFnPc4	palice
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Koncertní	koncertní	k2eAgNnPc4d1	koncertní
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
Adieu	adieu	k0	adieu
C.A.	C.A.	k1gMnSc6	C.A.
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Beatová	beatový	k2eAgFnSc1d1	beatová
síň	síň	k1gFnSc1	síň
slávy	sláva	k1gFnSc2	sláva
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
;	;	kIx,	;
kompilace	kompilace	k1gFnSc2	kompilace
+	+	kIx~	+
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Kompilace	kompilace	k1gFnSc2	kompilace
===	===	k?	===
</s>
</p>
<p>
<s>
Komplet	komplet	k1gInSc1	komplet
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tango	tango	k1gNnSc1	tango
Ropotámo	Ropotáma	k1gFnSc5	Ropotáma
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Habaděj	habaděj	k6eAd1	habaděj
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Beatová	beatový	k2eAgFnSc1d1	beatová
síň	síň	k1gFnSc1	síň
slávy	sláva	k1gFnSc2	sláva
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
;	;	kIx,	;
kompilace	kompilace	k1gFnSc2	kompilace
+	+	kIx~	+
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Singly	singl	k1gInPc4	singl
===	===	k?	===
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Vyznání	vyznání	k1gNnSc3	vyznání
<g/>
"	"	kIx"	"
<g/>
/	/	kIx~	/
<g/>
"	"	kIx"	"
<g/>
Ondřej	Ondřej	k1gMnSc1	Ondřej
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Komu	kdo	k3yQnSc3	kdo
se	se	k3xPyFc4	se
nelení	lenit	k5eNaImIp3nS	lenit
–	–	k?	–
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
ženění	ženěný	k2eAgMnPc1d1	ženěný
<g/>
"	"	kIx"	"
<g/>
/	/	kIx~	/
<g/>
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
se	se	k3xPyFc4	se
mám	mít	k5eAaImIp1nS	mít
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Olda	Olda	k1gMnSc1	Olda
přítel	přítel	k1gMnSc1	přítel
můj	můj	k3xOp1gMnSc1	můj
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgFnSc1d1	další
tvorba	tvorba	k1gFnSc1	tvorba
===	===	k?	===
</s>
</p>
<p>
<s>
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Pražákum	Pražákum	k?	Pražákum
<g/>
,	,	kIx,	,
těm	ten	k3xDgMnPc3	ten
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
hej	hej	k6eAd1	hej
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
SP	SP	kA	SP
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
F.	F.	kA	F.
R.	R.	kA	R.
Čecha	Čech	k1gMnSc2	Čech
nahrál	nahrát	k5eAaBmAgInS	nahrát
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Elektrovox	Elektrovox	k1gInSc1	Elektrovox
</s>
</p>
<p>
<s>
Hledám	hledat	k5eAaImIp1nS	hledat
dům	dům	k1gInSc1	dům
holubí	holubí	k2eAgInSc1d1	holubí
(	(	kIx(	(
<g/>
EP	EP	kA	EP
soundtrack	soundtrack	k1gInSc4	soundtrack
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
–	–	k?	–
název	název	k1gInSc1	název
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
není	být	k5eNaImIp3nS	být
zmíněn	zmíněn	k2eAgInSc1d1	zmíněn
<g/>
,	,	kIx,	,
uvedeni	uveden	k2eAgMnPc1d1	uveden
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
samostatně	samostatně	k6eAd1	samostatně
Michael	Michael	k1gMnSc1	Michael
Kocáb	Kocáb	k1gMnSc1	Kocáb
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
a	a	k8xC	a
Vilém	Vilém	k1gMnSc1	Vilém
Čok	Čok	k1gMnSc1	Čok
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
text	text	k1gInSc1	text
uvolněný	uvolněný	k2eAgInSc1d1	uvolněný
pod	pod	k7c7	pod
licencí	licence	k1gFnSc7	licence
GFDL	GFDL	kA	GFDL
ze	z	k7c2	z
stránky	stránka	k1gFnSc2	stránka
na	na	k7c6	na
webu	web	k1gInSc6	web
Kulturák	kulturák	k1gInSc1	kulturák
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pražský	pražský	k2eAgInSc4d1	pražský
výběr	výběr	k1gInSc4	výběr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Pražský	pražský	k2eAgInSc1d1	pražský
výběr	výběr	k1gInSc1	výběr
</s>
</p>
