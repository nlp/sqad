<s>
Český	český	k2eAgInSc1d1	český
statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
(	(	kIx(	(
<g/>
ČSÚ	ČSÚ	kA	ČSÚ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ústřední	ústřední	k2eAgInSc1d1	ústřední
orgán	orgán	k1gInSc1	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zřízen	zřízen	k2eAgInSc1d1	zřízen
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
1969	[number]	k4	1969
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c4	o
zřízení	zřízení	k1gNnSc4	zřízení
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
ústředních	ústřední	k2eAgInPc2d1	ústřední
orgánů	orgán	k1gInPc2	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Působnost	působnost	k1gFnSc1	působnost
ČSÚ	ČSÚ	kA	ČSÚ
je	být	k5eAaImIp3nS	být
vymezena	vymezit	k5eAaPmNgFnS	vymezit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
89	[number]	k4	89
<g/>
/	/	kIx~	/
<g/>
1995	[number]	k4	1995
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
státní	státní	k2eAgFnSc6d1	státní
statistické	statistický	k2eAgFnSc6d1	statistická
službě	služba	k1gFnSc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
orgán	orgán	k1gInSc1	orgán
státní	státní	k2eAgFnSc2d1	státní
statistické	statistický	k2eAgFnSc2d1	statistická
služby	služba	k1gFnSc2	služba
také	také	k9	také
koordinuje	koordinovat	k5eAaBmIp3nS	koordinovat
sběr	sběr	k1gInSc4	sběr
a	a	k8xC	a
zpracování	zpracování	k1gNnSc4	zpracování
statistických	statistický	k2eAgInPc2d1	statistický
údajů	údaj	k1gInPc2	údaj
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
provádějí	provádět	k5eAaImIp3nP	provádět
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
ministerstva	ministerstvo	k1gNnPc4	ministerstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
postavení	postavení	k1gNnSc2	postavení
ČSÚ	ČSÚ	kA	ČSÚ
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
hlavní	hlavní	k2eAgInPc4d1	hlavní
úkoly	úkol	k1gInPc4	úkol
<g/>
,	,	kIx,	,
organizaci	organizace	k1gFnSc4	organizace
<g/>
,	,	kIx,	,
hospodaření	hospodaření	k1gNnSc4	hospodaření
a	a	k8xC	a
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
,	,	kIx,	,
k	k	k7c3	k
ostatním	ostatní	k2eAgInPc3d1	ostatní
orgánům	orgán	k1gInPc3	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
územní	územní	k2eAgFnSc2d1	územní
samosprávy	samospráva	k1gFnSc2	samospráva
upravuje	upravovat	k5eAaImIp3nS	upravovat
Statut	statut	k1gInSc1	statut
Českého	český	k2eAgInSc2d1	český
statistického	statistický	k2eAgInSc2d1	statistický
úřadu	úřad	k1gInSc2	úřad
schválený	schválený	k2eAgInSc1d1	schválený
usnesením	usnesení	k1gNnSc7	usnesení
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
číslo	číslo	k1gNnSc1	číslo
1160	[number]	k4	1160
ze	z	k7c2	z
dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ČSÚ	ČSÚ	kA	ČSÚ
hospodaří	hospodařit	k5eAaImIp3nS	hospodařit
jako	jako	k9	jako
rozpočtová	rozpočtový	k2eAgFnSc1d1	rozpočtová
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
ústředí	ústředí	k1gNnSc1	ústředí
úřadu	úřad	k1gInSc2	úřad
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
krajích	kraj	k1gInPc6	kraj
působí	působit	k5eAaImIp3nS	působit
regionální	regionální	k2eAgNnSc4d1	regionální
pracoviště	pracoviště	k1gNnSc4	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
získávání	získávání	k1gNnSc4	získávání
a	a	k8xC	a
zpracování	zpracování	k1gNnSc4	zpracování
údajů	údaj	k1gInPc2	údaj
pro	pro	k7c4	pro
statistické	statistický	k2eAgInPc4d1	statistický
účely	účel	k1gInPc4	účel
a	a	k8xC	a
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
statistické	statistický	k2eAgFnPc4d1	statistická
informace	informace	k1gFnPc4	informace
státním	státní	k2eAgInPc3d1	státní
orgánům	orgán	k1gInPc3	orgán
<g/>
,	,	kIx,	,
orgánům	orgán	k1gInPc3	orgán
územní	územní	k2eAgFnSc2d1	územní
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
,	,	kIx,	,
veřejnosti	veřejnost	k1gFnSc2	veřejnost
a	a	k8xC	a
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
srovnatelnost	srovnatelnost	k1gFnSc4	srovnatelnost
statistických	statistický	k2eAgFnPc2d1	statistická
informací	informace	k1gFnPc2	informace
ve	v	k7c6	v
vnitrostátním	vnitrostátní	k2eAgNnSc6d1	vnitrostátní
i	i	k8xC	i
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
stanoví	stanovit	k5eAaPmIp3nP	stanovit
metodiku	metodika	k1gFnSc4	metodika
statistických	statistický	k2eAgNnPc2d1	statistické
zjišťování	zjišťování	k1gNnPc2	zjišťování
včetně	včetně	k7c2	včetně
programu	program	k1gInSc2	program
statistických	statistický	k2eAgNnPc2d1	statistické
zjišťování	zjišťování	k1gNnPc2	zjišťování
<g/>
.	.	kIx.	.
</s>
<s>
Uvedený	uvedený	k2eAgInSc1d1	uvedený
program	program	k1gInSc1	program
přesně	přesně	k6eAd1	přesně
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
působnost	působnost	k1gFnSc4	působnost
ČSÚ	ČSÚ	kA	ČSÚ
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
rezortů	rezort	k1gInPc2	rezort
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gFnSc6	jejichž
náplni	náplň	k1gFnSc6	náplň
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
statistické	statistický	k2eAgNnSc1d1	statistické
zjišťování	zjišťování	k1gNnSc1	zjišťování
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
státního	státní	k2eAgInSc2d1	státní
statistického	statistický	k2eAgInSc2d1	statistický
systému	systém	k1gInSc2	systém
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
nejen	nejen	k6eAd1	nejen
výsledky	výsledek	k1gInPc1	výsledek
statistických	statistický	k2eAgNnPc2d1	statistické
zjišťování	zjišťování	k1gNnPc2	zjišťování
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k9	i
administrativní	administrativní	k2eAgInPc4d1	administrativní
údaje	údaj	k1gInPc4	údaj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
statistických	statistický	k2eAgFnPc2d1	statistická
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
statistického	statistický	k2eAgInSc2d1	statistický
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
statistický	statistický	k2eAgInSc1d1	statistický
metainformační	metainformační	k2eAgInSc1d1	metainformační
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
díky	díky	k7c3	díky
unikátnímu	unikátní	k2eAgInSc3d1	unikátní
popisu	popis	k1gInSc3	popis
ukazatelů	ukazatel	k1gInPc2	ukazatel
přesně	přesně	k6eAd1	přesně
definuje	definovat	k5eAaBmIp3nS	definovat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
ukazatele	ukazatel	k1gInPc4	ukazatel
používané	používaný	k2eAgInPc4d1	používaný
a	a	k8xC	a
využívané	využívaný	k2eAgInPc4d1	využívaný
ve	v	k7c6	v
statistice	statistika	k1gFnSc6	statistika
<g/>
.	.	kIx.	.
</s>
<s>
Metainformace	Metainformace	k1gFnPc1	Metainformace
jsou	být	k5eAaImIp3nP	být
standardním	standardní	k2eAgInSc7d1	standardní
prvkem	prvek	k1gInSc7	prvek
všech	všecek	k3xTgInPc2	všecek
údajů	údaj	k1gInPc2	údaj
zveřejněných	zveřejněný	k2eAgInPc2d1	zveřejněný
ve	v	k7c6	v
veřejné	veřejný	k2eAgFnSc6d1	veřejná
databázi	databáze	k1gFnSc6	databáze
ČSÚ	ČSÚ	kA	ČSÚ
<g/>
.	.	kIx.	.
</s>
<s>
ČSÚ	ČSÚ	kA	ČSÚ
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
zpracování	zpracování	k1gNnSc4	zpracování
i	i	k8xC	i
zveřejňování	zveřejňování	k1gNnSc4	zveřejňování
údajů	údaj	k1gInPc2	údaj
<g/>
,	,	kIx,	,
sestavuje	sestavovat	k5eAaImIp3nS	sestavovat
souhrnné	souhrnný	k2eAgFnPc4d1	souhrnná
statistické	statistický	k2eAgFnPc4d1	statistická
charakteristiky	charakteristika	k1gFnPc4	charakteristika
vývoje	vývoj	k1gInSc2	vývoj
národního	národní	k2eAgNnSc2d1	národní
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
analýzy	analýza	k1gFnPc4	analýza
<g/>
,	,	kIx,	,
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
projekce	projekce	k1gFnSc1	projekce
demografického	demografický	k2eAgInSc2d1	demografický
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
provádí	provádět	k5eAaImIp3nS	provádět
konjunkturální	konjunkturální	k2eAgInPc4d1	konjunkturální
průzkumy	průzkum	k1gInPc4	průzkum
<g/>
.	.	kIx.	.
</s>
<s>
Sleduje	sledovat	k5eAaImIp3nS	sledovat
dodržování	dodržování	k1gNnSc1	dodržování
povinností	povinnost	k1gFnPc2	povinnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
státní	státní	k2eAgFnSc2d1	státní
statistické	statistický	k2eAgFnSc2d1	statistická
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
a	a	k8xC	a
spravuje	spravovat	k5eAaImIp3nS	spravovat
statistické	statistický	k2eAgFnPc4d1	statistická
klasifikace	klasifikace	k1gFnPc4	klasifikace
<g/>
,	,	kIx,	,
statistické	statistický	k2eAgInPc4d1	statistický
číselníky	číselník	k1gInPc4	číselník
a	a	k8xC	a
statistické	statistický	k2eAgInPc4d1	statistický
registry	registr	k1gInPc4	registr
a	a	k8xC	a
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
informace	informace	k1gFnSc2	informace
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
statistické	statistický	k2eAgFnPc4d1	statistická
informace	informace	k1gFnPc4	informace
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
organizacemi	organizace	k1gFnPc7	organizace
<g/>
,	,	kIx,	,
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
statistiky	statistika	k1gFnSc2	statistika
Evropských	evropský	k2eAgNnPc2d1	Evropské
společenství	společenství	k1gNnPc2	společenství
<g/>
.	.	kIx.	.
</s>
<s>
ČSÚ	ČSÚ	kA	ČSÚ
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
státní	státní	k2eAgFnSc4d1	státní
správu	správa	k1gFnSc4	správa
na	na	k7c6	na
úseku	úsek	k1gInSc6	úsek
zpracování	zpracování	k1gNnSc2	zpracování
výsledků	výsledek	k1gInPc2	výsledek
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
územních	územní	k2eAgInPc2d1	územní
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
konaných	konaný	k2eAgInPc2d1	konaný
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
zpracování	zpracování	k1gNnSc6	zpracování
výsledků	výsledek	k1gInPc2	výsledek
celostátního	celostátní	k2eAgNnSc2d1	celostátní
referenda	referendum	k1gNnSc2	referendum
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
stanoveném	stanovený	k2eAgInSc6d1	stanovený
zvláštními	zvláštní	k2eAgInPc7d1	zvláštní
právními	právní	k2eAgInPc7d1	právní
předpisy	předpis	k1gInPc7	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
voleb	volba	k1gFnPc2	volba
jsou	být	k5eAaImIp3nP	být
on-line	onin	k1gInSc5	on-lin
zveřejňovány	zveřejňovat	k5eAaImNgInP	zveřejňovat
na	na	k7c4	na
www.volby.cz	www.volby.cz	k1gInSc4	www.volby.cz
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
10	[number]	k4	10
let	léto	k1gNnPc2	léto
provádí	provádět	k5eAaImIp3nS	provádět
Sčítání	sčítání	k1gNnSc1	sčítání
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
domů	dům	k1gInPc2	dům
a	a	k8xC	a
bytů	byt	k1gInPc2	byt
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
výsledky	výsledek	k1gInPc1	výsledek
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
nejbohatším	bohatý	k2eAgInSc7d3	nejbohatší
zdrojem	zdroj	k1gInSc7	zdroj
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
obyvatelstvu	obyvatelstvo	k1gNnSc6	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
sčítání	sčítání	k1gNnSc1	sčítání
bylo	být	k5eAaImAgNnS	být
uskutečněno	uskutečnit	k5eAaPmNgNnS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
dále	daleko	k6eAd2	daleko
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
statistické	statistický	k2eAgFnPc4d1	statistická
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
porovnání	porovnání	k1gNnSc4	porovnání
úrovně	úroveň	k1gFnSc2	úroveň
sociálního	sociální	k2eAgInSc2d1	sociální
<g/>
,	,	kIx,	,
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
<g/>
,	,	kIx,	,
demografického	demografický	k2eAgInSc2d1	demografický
a	a	k8xC	a
ekologického	ekologický	k2eAgInSc2d1	ekologický
vývoje	vývoj	k1gInSc2	vývoj
státu	stát	k1gInSc2	stát
se	s	k7c7	s
zahraničím	zahraničí	k1gNnSc7	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Získané	získaný	k2eAgFnPc4d1	získaná
informace	informace	k1gFnPc4	informace
i	i	k8xC	i
výsledky	výsledek	k1gInPc4	výsledek
porovnání	porovnání	k1gNnSc2	porovnání
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
státním	státní	k2eAgMnPc3d1	státní
orgánům	orgán	k1gMnPc3	orgán
i	i	k8xC	i
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Veškerá	veškerý	k3xTgNnPc1	veškerý
data	datum	k1gNnPc1	datum
a	a	k8xC	a
informace	informace	k1gFnPc1	informace
jsou	být	k5eAaImIp3nP	být
zdarma	zdarma	k6eAd1	zdarma
dostupné	dostupný	k2eAgFnPc1d1	dostupná
na	na	k7c4	na
www.czso.cz	www.czso.cz	k1gInSc4	www.czso.cz
<g/>
.	.	kIx.	.
</s>
<s>
Vydává	vydávat	k5eAaPmIp3nS	vydávat
Statistickou	statistický	k2eAgFnSc4d1	statistická
ročenku	ročenka	k1gFnSc4	ročenka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc4d1	další
odborné	odborný	k2eAgFnPc4d1	odborná
publikace	publikace	k1gFnPc4	publikace
a	a	k8xC	a
časopisy	časopis	k1gInPc4	časopis
a	a	k8xC	a
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
vývoj	vývoj	k1gInSc4	vývoj
nových	nový	k2eAgFnPc2d1	nová
statistických	statistický	k2eAgFnPc2d1	statistická
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgInPc1d3	nejnovější
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
ekonomickém	ekonomický	k2eAgInSc6d1	ekonomický
i	i	k8xC	i
sociálním	sociální	k2eAgInSc6d1	sociální
vývoji	vývoj	k1gInSc6	vývoj
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
zveřejňují	zveřejňovat	k5eAaImIp3nP	zveřejňovat
formou	forma	k1gFnSc7	forma
tzv.	tzv.	kA	tzv.
Rychlých	Rychlých	k2eAgFnSc7d1	Rychlých
informací	informace	k1gFnSc7	informace
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
stránkách	stránka	k1gFnPc6	stránka
zveřejňuje	zveřejňovat	k5eAaImIp3nS	zveřejňovat
analýzy	analýza	k1gFnPc4	analýza
<g/>
,	,	kIx,	,
on-line	onin	k1gInSc5	on-lin
publikace	publikace	k1gFnPc1	publikace
<g/>
,	,	kIx,	,
datové	datový	k2eAgFnPc1d1	datová
sady	sada	k1gFnPc1	sada
<g/>
,	,	kIx,	,
časové	časový	k2eAgFnPc1d1	časová
řady	řada	k1gFnPc1	řada
<g/>
,	,	kIx,	,
tiskové	tiskový	k2eAgFnPc1d1	tisková
zprávy	zpráva	k1gFnPc1	zpráva
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
informace	informace	k1gFnPc1	informace
<g/>
.	.	kIx.	.
</s>
<s>
ČSÚ	ČSÚ	kA	ČSÚ
je	být	k5eAaImIp3nS	být
nezávislý	závislý	k2eNgInSc1d1	nezávislý
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
a	a	k8xC	a
politických	politický	k2eAgFnPc6d1	politická
stranách	strana	k1gFnPc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Nestrannost	nestrannost	k1gFnSc1	nestrannost
úřadu	úřad	k1gInSc2	úřad
je	být	k5eAaImIp3nS	být
garantována	garantovat	k5eAaBmNgFnS	garantovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ČSÚ	ČSÚ	kA	ČSÚ
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
relevantními	relevantní	k2eAgInPc7d1	relevantní
zákony	zákon	k1gInPc7	zákon
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
právními	právní	k2eAgInPc7d1	právní
předpisy	předpis	k1gInPc7	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ČSÚ	ČSÚ	kA	ČSÚ
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
odborná	odborný	k2eAgNnPc4d1	odborné
hlediska	hledisko	k1gNnPc4	hledisko
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
vědecké	vědecký	k2eAgFnPc4d1	vědecká
metody	metoda	k1gFnPc4	metoda
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zjišťování	zjišťování	k1gNnSc6	zjišťování
i	i	k8xC	i
při	při	k7c6	při
zveřejňování	zveřejňování	k1gNnSc6	zveřejňování
statistických	statistický	k2eAgFnPc2d1	statistická
informací	informace	k1gFnPc2	informace
vždy	vždy	k6eAd1	vždy
postupuje	postupovat	k5eAaImIp3nS	postupovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
nebyla	být	k5eNaImAgFnS	být
narušena	narušen	k2eAgFnSc1d1	narušena
objektivita	objektivita	k1gFnSc1	objektivita
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
ČSÚ	ČSÚ	kA	ČSÚ
respektuje	respektovat	k5eAaImIp3nS	respektovat
rovný	rovný	k2eAgInSc4d1	rovný
přístup	přístup	k1gInSc4	přístup
všech	všecek	k3xTgMnPc2	všecek
uživatelů	uživatel	k1gMnPc2	uživatel
ke	k	k7c3	k
statistickým	statistický	k2eAgFnPc3d1	statistická
informacím	informace	k1gFnPc3	informace
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
statistické	statistický	k2eAgFnPc1d1	statistická
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
sociálního	sociální	k2eAgInSc2d1	sociální
<g/>
,	,	kIx,	,
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
i	i	k8xC	i
demografického	demografický	k2eAgInSc2d1	demografický
vývoje	vývoj	k1gInSc2	vývoj
se	se	k3xPyFc4	se
zveřejňují	zveřejňovat	k5eAaImIp3nP	zveřejňovat
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Rychlých	Rychlých	k2eAgFnPc6d1	Rychlých
informacích	informace	k1gFnPc6	informace
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
inflace	inflace	k1gFnSc2	inflace
<g/>
,	,	kIx,	,
průměrné	průměrný	k2eAgFnSc2d1	průměrná
mzdy	mzda	k1gFnSc2	mzda
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kalendář	kalendář	k1gInSc1	kalendář
s	s	k7c7	s
termíny	termín	k1gInPc7	termín
zveřejnění	zveřejnění	k1gNnSc2	zveřejnění
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
Rychlých	Rychlých	k2eAgFnPc2d1	Rychlých
informací	informace	k1gFnPc2	informace
na	na	k7c4	na
daný	daný	k2eAgInSc4d1	daný
rok	rok	k1gInSc4	rok
je	být	k5eAaImIp3nS	být
publikován	publikovat	k5eAaBmNgInS	publikovat
vždy	vždy	k6eAd1	vždy
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
předcházejícího	předcházející	k2eAgInSc2d1	předcházející
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc4d1	ostatní
statistické	statistický	k2eAgFnPc4d1	statistická
informace	informace	k1gFnPc4	informace
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
publikací	publikace	k1gFnPc2	publikace
<g/>
,	,	kIx,	,
analýz	analýza	k1gFnPc2	analýza
a	a	k8xC	a
časových	časový	k2eAgFnPc2d1	časová
řad	řada	k1gFnPc2	řada
jsou	být	k5eAaImIp3nP	být
zveřejňovány	zveřejňovat	k5eAaImNgInP	zveřejňovat
podle	podle	k7c2	podle
termínů	termín	k1gInPc2	termín
uvedených	uvedený	k2eAgInPc2d1	uvedený
v	v	k7c6	v
Katalogu	katalog	k1gInSc6	katalog
produktů	produkt	k1gInPc2	produkt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
veřejnosti	veřejnost	k1gFnPc4	veřejnost
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
ČSÚ	ČSÚ	kA	ČSÚ
vždy	vždy	k6eAd1	vždy
koncem	koncem	k7c2	koncem
listopadu	listopad	k1gInSc2	listopad
s	s	k7c7	s
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
produktech	produkt	k1gInPc6	produkt
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
následující	následující	k2eAgInSc4d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
uživatelé	uživatel	k1gMnPc1	uživatel
tak	tak	k6eAd1	tak
mají	mít	k5eAaImIp3nP	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
novým	nový	k2eAgFnPc3d1	nová
informacím	informace	k1gFnPc3	informace
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
rozsah	rozsah	k1gInSc1	rozsah
zveřejněných	zveřejněný	k2eAgFnPc2d1	zveřejněná
informací	informace	k1gFnPc2	informace
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
uživatele	uživatel	k1gMnPc4	uživatel
totožný	totožný	k2eAgInSc1d1	totožný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
ČSÚ	ČSÚ	kA	ČSÚ
stojí	stát	k5eAaImIp3nS	stát
předseda	předseda	k1gMnSc1	předseda
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
vlády	vláda	k1gFnSc2	vláda
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
ČSÚ	ČSÚ	kA	ČSÚ
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
své	svůj	k3xOyFgMnPc4	svůj
zástupce	zástupce	k1gMnPc4	zástupce
-	-	kIx~	-
místopředsedy	místopředseda	k1gMnPc7	místopředseda
<g/>
.	.	kIx.	.
</s>
<s>
ČSÚ	ČSÚ	kA	ČSÚ
zřizuje	zřizovat	k5eAaImIp3nS	zřizovat
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
poradní	poradní	k2eAgInSc4d1	poradní
orgán	orgán	k1gInSc4	orgán
Českou	český	k2eAgFnSc4d1	Česká
statistickou	statistický	k2eAgFnSc4d1	statistická
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
je	být	k5eAaImIp3nS	být
předseda	předseda	k1gMnSc1	předseda
ČSÚ	ČSÚ	kA	ČSÚ
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
poradním	poradní	k2eAgInPc3d1	poradní
orgánům	orgán	k1gInPc3	orgán
předsedy	předseda	k1gMnSc2	předseda
ČSÚ	ČSÚ	kA	ČSÚ
náleží	náležet	k5eAaImIp3nP	náležet
Porada	porada	k1gFnSc1	porada
vedení	vedení	k1gNnSc1	vedení
a	a	k8xC	a
Kolegium	kolegium	k1gNnSc1	kolegium
<g/>
.	.	kIx.	.
</s>
<s>
Porada	porada	k1gFnSc1	porada
vedení	vedení	k1gNnSc2	vedení
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
jak	jak	k6eAd1	jak
zásadními	zásadní	k2eAgFnPc7d1	zásadní
otázkami	otázka	k1gFnPc7	otázka
činnosti	činnost	k1gFnSc2	činnost
ČSÚ	ČSÚ	kA	ČSÚ
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
otázkami	otázka	k1gFnPc7	otázka
operativního	operativní	k2eAgNnSc2d1	operativní
řízení	řízení	k1gNnSc2	řízení
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
zajišťováním	zajišťování	k1gNnSc7	zajišťování
a	a	k8xC	a
plněním	plnění	k1gNnSc7	plnění
jeho	jeho	k3xOp3gInPc2	jeho
aktuálních	aktuální	k2eAgInPc2d1	aktuální
úkolů	úkol	k1gInPc2	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Kolegium	kolegium	k1gNnSc1	kolegium
projednává	projednávat	k5eAaImIp3nS	projednávat
především	především	k9	především
koncepční	koncepční	k2eAgInPc4d1	koncepční
materiály	materiál	k1gInPc4	materiál
ze	z	k7c2	z
sféry	sféra	k1gFnSc2	sféra
státní	státní	k2eAgFnSc2d1	státní
statistické	statistický	k2eAgFnPc4d1	statistická
služby	služba	k1gFnPc4	služba
vyplývající	vyplývající	k2eAgFnPc4d1	vyplývající
z	z	k7c2	z
působnosti	působnost	k1gFnSc2	působnost
Českého	český	k2eAgInSc2d1	český
statistického	statistický	k2eAgInSc2d1	statistický
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
každoroční	každoroční	k2eAgFnSc1d1	každoroční
náplň	náplň	k1gFnSc1	náplň
programu	program	k1gInSc2	program
statistických	statistický	k2eAgNnPc2d1	statistické
zjišťování	zjišťování	k1gNnPc2	zjišťování
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
úkoly	úkol	k1gInPc4	úkol
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yIgInPc6	který
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
předseda	předseda	k1gMnSc1	předseda
ČSÚ	ČSÚ	kA	ČSÚ
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
je	být	k5eAaImIp3nS	být
předsedkyní	předsedkyně	k1gFnSc7	předsedkyně
Českého	český	k2eAgInSc2d1	český
statistického	statistický	k2eAgInSc2d1	statistický
úřadu	úřad	k1gInSc2	úřad
prof.	prof.	kA	prof.
Ing.	ing.	kA	ing.
Iva	Iva	k1gFnSc1	Iva
Ritschelová	Ritschelová	k1gFnSc1	Ritschelová
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2014	[number]	k4	2014
se	se	k3xPyFc4	se
místopředsedkyní	místopředsedkyně	k1gFnSc7	místopředsedkyně
stala	stát	k5eAaPmAgFnS	stát
Ing.	ing.	kA	ing.
Eva	Eva	k1gFnSc1	Eva
Bartoňová	Bartoňová	k1gFnSc1	Bartoňová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
nahradila	nahradit	k5eAaPmAgFnS	nahradit
dosavadního	dosavadní	k2eAgMnSc4d1	dosavadní
místopředsedu	místopředseda	k1gMnSc4	místopředseda
Ing.	ing.	kA	ing.
Stanislava	Stanislav	k1gMnSc4	Stanislav
Drápala	Drápal	k1gMnSc4	Drápal
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
35	[number]	k4	35
letech	léto	k1gNnPc6	léto
práce	práce	k1gFnSc2	práce
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2014	[number]	k4	2014
se	se	k3xPyFc4	se
místopředsedou	místopředseda	k1gMnSc7	místopředseda
stal	stát	k5eAaPmAgInS	stát
Ing.	ing.	kA	ing.
Marek	Marek	k1gMnSc1	Marek
Rojíček	Rojíček	k1gMnSc1	Rojíček
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
ČSÚ	ČSÚ	kA	ČSÚ
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
vrchní	vrchní	k2eAgMnSc1d1	vrchní
ředitel	ředitel	k1gMnSc1	ředitel
sekce	sekce	k1gFnSc2	sekce
makroekonomických	makroekonomický	k2eAgFnPc2d1	makroekonomická
statistik	statistika	k1gFnPc2	statistika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2016	[number]	k4	2016
se	se	k3xPyFc4	se
místopředsedou	místopředseda	k1gMnSc7	místopředseda
ČSÚ	ČSÚ	kA	ČSÚ
stal	stát	k5eAaPmAgMnS	stát
Ing.	ing.	kA	ing.
Roman	Roman	k1gMnSc1	Roman
Bechtold	Bechtold	k1gMnSc1	Bechtold
<g/>
,	,	kIx,	,
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Ing.	ing.	kA	ing.
Evu	Eva	k1gFnSc4	Eva
Bartoňovou	Bartoňová	k1gFnSc4	Bartoňová
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
počátek	počátek	k1gInSc1	počátek
organizované	organizovaný	k2eAgFnSc2d1	organizovaná
statistiky	statistika	k1gFnSc2	statistika
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
datován	datovat	k5eAaImNgInS	datovat
již	již	k6eAd1	již
rokem	rok	k1gInSc7	rok
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
fungovala	fungovat	k5eAaImAgFnS	fungovat
statistická	statistický	k2eAgFnSc1d1	statistická
kancelář	kancelář	k1gFnSc1	kancelář
jako	jako	k8xC	jako
samostatné	samostatný	k2eAgNnSc1d1	samostatné
pracoviště	pracoviště	k1gNnSc1	pracoviště
financované	financovaný	k2eAgNnSc1d1	financované
z	z	k7c2	z
podpory	podpora	k1gFnSc2	podpora
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
začala	začít	k5eAaPmAgFnS	začít
systematicky	systematicky	k6eAd1	systematicky
zveřejňovat	zveřejňovat	k5eAaImF	zveřejňovat
každoroční	každoroční	k2eAgFnSc2d1	každoroční
statistické	statistický	k2eAgFnSc2d1	statistická
zprávy	zpráva	k1gFnSc2	zpráva
tiskem	tisk	k1gInSc7	tisk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgInP	být
podávány	podáván	k2eAgInPc1d1	podáván
požadavky	požadavek	k1gInPc1	požadavek
přeměnit	přeměnit	k5eAaPmF	přeměnit
statistickou	statistický	k2eAgFnSc4d1	statistická
kancelář	kancelář	k1gFnSc4	kancelář
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
úřad	úřad	k1gInSc4	úřad
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vycházelo	vycházet	k5eAaImAgNnS	vycházet
z	z	k7c2	z
potřeby	potřeba	k1gFnSc2	potřeba
statistických	statistický	k2eAgInPc2d1	statistický
údajů	údaj	k1gInPc2	údaj
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
orgány	orgán	k1gInPc4	orgán
zemské	zemský	k2eAgFnSc2d1	zemská
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1897	[number]	k4	1897
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Zemský	zemský	k2eAgInSc1d1	zemský
statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
Království	království	k1gNnPc4	království
české	český	k2eAgFnPc1d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
byla	být	k5eAaImAgFnS	být
Statistická	statistický	k2eAgFnSc1d1	statistická
komise	komise	k1gFnSc1	komise
jako	jako	k8xS	jako
poradní	poradní	k2eAgInSc1d1	poradní
orgán	orgán	k1gInSc1	orgán
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
také	také	k9	také
Statistická	statistický	k2eAgFnSc1d1	statistická
kancelář	kancelář	k1gFnSc1	kancelář
zastupující	zastupující	k2eAgFnSc4d1	zastupující
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
složku	složka	k1gFnSc4	složka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
opět	opět	k6eAd1	opět
prováděla	provádět	k5eAaImAgFnS	provádět
sběr	sběr	k1gInSc4	sběr
údajů	údaj	k1gInPc2	údaj
a	a	k8xC	a
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
statistická	statistický	k2eAgNnPc4d1	statistické
data	datum	k1gNnPc4	datum
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc4	počátek
vzniku	vznik	k1gInSc2	vznik
organizované	organizovaný	k2eAgFnSc2d1	organizovaná
statistické	statistický	k2eAgFnSc2d1	statistická
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
schválilo	schválit	k5eAaPmAgNnS	schválit
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
statistické	statistický	k2eAgFnSc6d1	statistická
službě	služba	k1gFnSc6	služba
(	(	kIx(	(
<g/>
č.	č.	k?	č.
49	[number]	k4	49
<g/>
/	/	kIx~	/
<g/>
1919	[number]	k4	1919
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
z.	z.	k?	z.
n.	n.	k?	n.
"	"	kIx"	"
<g/>
o	o	k7c6	o
organizaci	organizace	k1gFnSc6	organizace
statistické	statistický	k2eAgFnSc2d1	statistická
služby	služba	k1gFnSc2	služba
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
vymezoval	vymezovat	k5eAaImAgInS	vymezovat
nově	nově	k6eAd1	nově
ustavený	ustavený	k2eAgInSc1d1	ustavený
statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
jako	jako	k8xS	jako
státní	státní	k2eAgFnSc4d1	státní
instituci	instituce	k1gFnSc4	instituce
se	s	k7c7	s
svými	svůj	k3xOyFgNnPc7	svůj
právy	právo	k1gNnPc7	právo
a	a	k8xC	a
povinnostmi	povinnost	k1gFnPc7	povinnost
<g/>
.	.	kIx.	.
</s>
