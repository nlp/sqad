<s>
Blansko	Blansko	k1gNnSc1	Blansko
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Blanz	Blanz	k1gInSc1	Blanz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Svitavy	Svitava	k1gFnSc2	Svitava
<g/>
,	,	kIx,	,
19	[number]	k4	19
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
má	mít	k5eAaImIp3nS	mít
nadmořskou	nadmořský	k2eAgFnSc4d1	nadmořská
výšku	výška	k1gFnSc4	výška
276	[number]	k4	276
m	m	kA	m
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
ve	v	k7c6	v
svahu	svah	k1gInSc6	svah
nad	nad	k7c7	nad
levým	levý	k2eAgInSc7d1	levý
břehem	břeh	k1gInSc7	břeh
Svitavy	Svitava	k1gFnSc2	Svitava
<g/>
.	.	kIx.	.
</s>
<s>
Blansko	Blansko	k1gNnSc1	Blansko
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
rozlohu	rozloha	k1gFnSc4	rozloha
18,29	[number]	k4	18,29
km2	km2	k4	km2
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
téměř	téměř	k6eAd1	téměř
21	[number]	k4	21
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
zdejší	zdejší	k2eAgFnSc6d1	zdejší
osadě	osada	k1gFnSc6	osada
(	(	kIx(	(
<g/>
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Starém	staré	k1gNnSc6	staré
Blansku	Blansko	k1gNnSc6	Blansko
<g/>
)	)	kIx)	)
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Svitavy	Svitava	k1gFnSc2	Svitava
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
Letopise	letopis	k1gInSc6	letopis
tzv.	tzv.	kA	tzv.
Kanovníka	kanovník	k1gMnSc2	kanovník
vyšehradského	vyšehradský	k2eAgMnSc2d1	vyšehradský
a	a	k8xC	a
váže	vázat	k5eAaImIp3nS	vázat
se	se	k3xPyFc4	se
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1136	[number]	k4	1136
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
probíhal	probíhat	k5eAaImAgInS	probíhat
ostrý	ostrý	k2eAgInSc1d1	ostrý
majetkový	majetkový	k2eAgInSc1d1	majetkový
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
hlavně	hlavně	k6eAd1	hlavně
politický	politický	k2eAgInSc4d1	politický
<g/>
)	)	kIx)	)
spor	spor	k1gInSc4	spor
o	o	k7c4	o
právo	právo	k1gNnSc4	právo
vystavět	vystavět	k5eAaPmF	vystavět
v	v	k7c6	v
Blansku	Blansko	k1gNnSc6	Blansko
kostel	kostel	k1gInSc4	kostel
mezi	mezi	k7c7	mezi
olomouckým	olomoucký	k2eAgMnSc7d1	olomoucký
biskupem	biskup	k1gMnSc7	biskup
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Zdíkem	Zdík	k1gMnSc7	Zdík
a	a	k8xC	a
brněnským	brněnský	k2eAgMnSc7d1	brněnský
údělným	údělný	k2eAgMnSc7d1	údělný
knížetem	kníže	k1gMnSc7	kníže
Vratislavem	Vratislav	k1gMnSc7	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Blansko	Blansko	k1gNnSc1	Blansko
se	se	k3xPyFc4	se
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
osadami	osada	k1gFnPc7	osada
stalo	stát	k5eAaPmAgNnS	stát
majetkem	majetek	k1gInSc7	majetek
olomouckého	olomoucký	k2eAgNnSc2d1	olomoucké
biskupství	biskupství	k1gNnSc2	biskupství
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
držitelé	držitel	k1gMnPc1	držitel
dostávali	dostávat	k5eAaImAgMnP	dostávat
toto	tento	k3xDgNnSc4	tento
zboží	zboží	k1gNnSc4	zboží
jako	jako	k8xS	jako
léno	léno	k1gNnSc4	léno
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gInSc7	centr
tohoto	tento	k3xDgNnSc2	tento
panství	panství	k1gNnSc2	panství
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
hrad	hrad	k1gInSc1	hrad
Blansek	Blansko	k1gNnPc2	Blansko
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
východně	východně	k6eAd1	východně
od	od	k7c2	od
Blanska	Blansko	k1gNnSc2	Blansko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1277	[number]	k4	1277
založil	založit	k5eAaPmAgMnS	založit
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
biskup	biskup	k1gMnSc1	biskup
Bruno	Bruno	k1gMnSc1	Bruno
ze	z	k7c2	z
Schauenburku	Schauenburk	k1gInSc2	Schauenburk
na	na	k7c6	na
protější	protější	k2eAgFnSc6d1	protější
straně	strana	k1gFnSc6	strana
řeky	řeka	k1gFnSc2	řeka
novou	nový	k2eAgFnSc4d1	nová
osadu	osada	k1gFnSc4	osada
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
Nové	Nové	k2eAgNnSc1d1	Nové
Blansko	Blansko	k1gNnSc1	Blansko
stala	stát	k5eAaPmAgFnS	stát
jádrem	jádro	k1gNnSc7	jádro
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
držení	držení	k1gNnSc6	držení
obou	dva	k4xCgFnPc2	dva
vesnic	vesnice	k1gFnPc2	vesnice
se	se	k3xPyFc4	se
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
řada	řada	k1gFnSc1	řada
lenních	lenní	k2eAgMnPc2d1	lenní
majitelů	majitel	k1gMnPc2	majitel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
k	k	k7c3	k
nejznámějším	známý	k2eAgInPc3d3	nejznámější
patřil	patřit	k5eAaImAgInS	patřit
rod	rod	k1gInSc1	rod
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
a	a	k8xC	a
Černohorských	Černohorská	k1gFnPc2	Černohorská
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
měl	mít	k5eAaImAgMnS	mít
blanenské	blanenský	k2eAgNnSc1d1	blanenské
panství	panství	k1gNnSc1	panství
rod	rod	k1gInSc4	rod
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Doubravky	Doubravka	k1gFnSc2	Doubravka
a	a	k8xC	a
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
,	,	kIx,	,
za	za	k7c2	za
nichž	jenž	k3xRgInPc2	jenž
byl	být	k5eAaImAgInS	být
zanedbaný	zanedbaný	k2eAgInSc1d1	zanedbaný
statek	statek	k1gInSc1	statek
zveleben	zveleben	k2eAgInSc1d1	zveleben
a	a	k8xC	a
opraven	opraven	k2eAgInSc1d1	opraven
a	a	k8xC	a
osady	osada	k1gFnPc1	osada
Staré	Staré	k2eAgFnPc1d1	Staré
a	a	k8xC	a
Nové	Nové	k2eAgNnSc1d1	Nové
Blansko	Blansko	k1gNnSc1	Blansko
sloučeny	sloučen	k2eAgFnPc4d1	sloučena
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Skála	Skála	k1gMnSc1	Skála
z	z	k7c2	z
Doubravky	Doubravka	k1gFnSc2	Doubravka
odkoupil	odkoupit	k5eAaPmAgMnS	odkoupit
od	od	k7c2	od
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Černohorského	Černohorský	k1gMnSc2	Černohorský
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
blanenský	blanenský	k2eAgInSc4d1	blanenský
manský	manský	k2eAgInSc4d1	manský
dvůr	dvůr	k1gInSc4	dvůr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
ostatním	ostatní	k2eAgNnSc7d1	ostatní
zbožím	zboží	k1gNnSc7	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1580	[number]	k4	1580
povýšil	povýšit	k5eAaPmAgMnS	povýšit
Matyáš	Matyáš	k1gMnSc1	Matyáš
Žalkovský	Žalkovský	k2eAgMnSc1d1	Žalkovský
ze	z	k7c2	z
Žalkovic	Žalkovice	k1gFnPc2	Žalkovice
Blansko	Blansko	k1gNnSc4	Blansko
na	na	k7c4	na
městečko	městečko	k1gNnSc4	městečko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1631-1694	[number]	k4	1631-1694
držel	držet	k5eAaImAgMnS	držet
Blansko	Blansko	k1gNnSc4	Blansko
rod	rod	k1gInSc1	rod
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Rožmitálu	Rožmitál	k1gInSc2	Rožmitál
<g/>
.	.	kIx.	.
</s>
<s>
Slezský	slezský	k2eAgInSc1d1	slezský
rod	rod	k1gInSc1	rod
hrabat	hrabě	k1gNnPc2	hrabě
Gellhornů	Gellhorn	k1gInPc2	Gellhorn
zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1698	[number]	k4	1698
založil	založit	k5eAaPmAgMnS	založit
první	první	k4xOgFnPc4	první
železárny	železárna	k1gFnPc4	železárna
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významnému	významný	k2eAgInSc3d1	významný
rozvoji	rozvoj	k1gInSc3	rozvoj
Blanska	Blansko	k1gNnSc2	Blansko
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
právě	právě	k9	právě
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
rozmachem	rozmach	k1gInSc7	rozmach
zdejších	zdejší	k2eAgFnPc2d1	zdejší
železáren	železárna	k1gFnPc2	železárna
a	a	k8xC	a
strojíren	strojírna	k1gFnPc2	strojírna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
zbudoval	zbudovat	k5eAaPmAgMnS	zbudovat
Hugo	Hugo	k1gMnSc1	Hugo
František	František	k1gMnSc1	František
Salm	Salm	k1gMnSc1	Salm
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
rozmachu	rozmach	k1gInSc3	rozmach
města	město	k1gNnSc2	město
přispěla	přispět	k5eAaPmAgFnS	přispět
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Brno	Brno	k1gNnSc4	Brno
-	-	kIx~	-
Česká	český	k2eAgFnSc1d1	Česká
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bylo	být	k5eAaImAgNnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřen	k2eAgFnSc1d1	otevřena
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1849	[number]	k4	1849
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
roboty	robota	k1gFnSc2	robota
a	a	k8xC	a
šlechtických	šlechtický	k2eAgNnPc2d1	šlechtické
panství	panství	k1gNnPc2	panství
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Blansko	Blansko	k1gNnSc1	Blansko
centrem	centr	k1gMnSc7	centr
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
soudních	soudní	k2eAgInPc2d1	soudní
okresů	okres	k1gInPc2	okres
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
spadaly	spadat	k5eAaImAgInP	spadat
pod	pod	k7c4	pod
hejtmanství	hejtmanství	k1gNnSc4	hejtmanství
v	v	k7c6	v
Boskovicích	Boskovice	k1gInPc6	Boskovice
<g/>
.	.	kIx.	.
</s>
<s>
Blansko	Blansko	k1gNnSc1	Blansko
z	z	k7c2	z
městyse	městys	k1gInSc2	městys
na	na	k7c4	na
město	město	k1gNnSc4	město
povýšil	povýšit	k5eAaPmAgMnS	povýšit
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
sepsán	sepsat	k5eAaPmNgInS	sepsat
pamětní	pamětní	k2eAgInSc1d1	pamětní
list	list	k1gInSc1	list
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
poměry	poměr	k1gInPc4	poměr
v	v	k7c6	v
Blansku	Blansko	k1gNnSc6	Blansko
<g/>
,	,	kIx,	,
stav	stav	k1gInSc4	stav
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
spolků	spolek	k1gInPc2	spolek
apod.	apod.	kA	apod.
Blansko	Blansko	k1gNnSc1	Blansko
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
417	[number]	k4	417
domů	dům	k1gInPc2	dům
s	s	k7c7	s
3350	[number]	k4	3350
obyvateli	obyvatel	k1gMnPc7	obyvatel
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
rozvinutým	rozvinutý	k2eAgNnSc7d1	rozvinuté
průmyslovým	průmyslový	k2eAgNnSc7d1	průmyslové
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
továrnou	továrna	k1gFnSc7	továrna
byly	být	k5eAaImAgInP	být
Salmovy	Salmův	k2eAgInPc1d1	Salmův
železářské	železářský	k2eAgInPc1d1	železářský
závody	závod	k1gInPc1	závod
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
široký	široký	k2eAgInSc4d1	široký
sortiment	sortiment	k1gInSc4	sortiment
nejen	nejen	k6eAd1	nejen
železářské	železářský	k2eAgFnSc2d1	železářská
produkce	produkce	k1gFnSc2	produkce
a	a	k8xC	a
které	který	k3yIgFnPc4	který
zaměstnávaly	zaměstnávat	k5eAaImAgFnP	zaměstnávat
2000	[number]	k4	2000
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
závodem	závod	k1gInSc7	závod
byla	být	k5eAaImAgFnS	být
továrna	továrna	k1gFnSc1	továrna
a	a	k8xC	a
slévárna	slévárna	k1gFnSc1	slévárna
firmy	firma	k1gFnSc2	firma
Ježek	Ježek	k1gMnSc1	Ježek
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zaměstnávala	zaměstnávat	k5eAaImAgFnS	zaměstnávat
400	[number]	k4	400
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
továrna	továrna	k1gFnSc1	továrna
firmy	firma	k1gFnSc2	firma
Carl	Carl	k1gMnSc1	Carl
Mayers	Mayers	k1gInSc1	Mayers
Söhne	Söhn	k1gInSc5	Söhn
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
výrobou	výroba	k1gFnSc7	výroba
hliněných	hliněný	k2eAgNnPc2d1	hliněné
kamen	kamna	k1gNnPc2	kamna
a	a	k8xC	a
šamotového	šamotový	k2eAgNnSc2d1	šamotové
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
zaměstnávající	zaměstnávající	k2eAgFnSc2d1	zaměstnávající
120	[number]	k4	120
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
menšími	malý	k2eAgFnPc7d2	menší
firmami	firma	k1gFnPc7	firma
byly	být	k5eAaImAgFnP	být
továrna	továrna	k1gFnSc1	továrna
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
strojů	stroj	k1gInPc2	stroj
Františka	František	k1gMnSc2	František
Šaumana	Šauman	k1gMnSc2	Šauman
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
strojů	stroj	k1gInPc2	stroj
Družstvo	družstvo	k1gNnSc1	družstvo
železářů	železář	k1gMnPc2	železář
<g/>
,	,	kIx,	,
strojírna	strojírna	k1gFnSc1	strojírna
bratří	bratr	k1gMnPc2	bratr
Nejezchlebů	Nejezchleb	k1gMnPc2	Nejezchleb
<g/>
,	,	kIx,	,
cihelna	cihelna	k1gFnSc1	cihelna
Martina	Martin	k1gMnSc2	Martin
Kaly	Kala	k1gMnSc2	Kala
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
Blansku	Blansko	k1gNnSc6	Blansko
inženýr	inženýr	k1gMnSc1	inženýr
Erich	Erich	k1gMnSc1	Erich
Roučka	Roučka	k1gMnSc1	Roučka
továrnu	továrna	k1gFnSc4	továrna
na	na	k7c6	na
výrobu	výrob	k1gInSc6	výrob
elektrických	elektrický	k2eAgInPc2d1	elektrický
měřících	měřící	k2eAgInPc2d1	měřící
přístrojů	přístroj	k1gInPc2	přístroj
a	a	k8xC	a
regulačních	regulační	k2eAgFnPc2d1	regulační
soustav	soustava	k1gFnPc2	soustava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
o	o	k7c4	o
23	[number]	k4	23
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
prodal	prodat	k5eAaPmAgInS	prodat
Robertu	Robert	k1gMnSc3	Robert
Sochorovi	Sochor	k1gMnSc3	Sochor
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
továrna	továrna	k1gFnSc1	továrna
znárodněna	znárodněn	k2eAgFnSc1d1	znárodněna
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
Metra	metro	k1gNnSc2	metro
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ČKD	ČKD	kA	ČKD
Blansko	Blansko	k1gNnSc1	Blansko
a	a	k8xC	a
Adastem	Adast	k1gInSc7	Adast
Blansko	Blansko	k1gNnSc1	Blansko
tvořila	tvořit	k5eAaImAgFnS	tvořit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
jádro	jádro	k1gNnSc4	jádro
blanenského	blanenský	k2eAgInSc2d1	blanenský
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Blansko	Blansko	k1gNnSc1	Blansko
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
si	se	k3xPyFc3	se
Blansko	Blansko	k1gNnSc1	Blansko
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
svůj	svůj	k3xOyFgInSc4	svůj
převážně	převážně	k6eAd1	převážně
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jeho	jeho	k3xOp3gInSc4	jeho
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
zaměstnanost	zaměstnanost	k1gFnSc4	zaměstnanost
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
znatelně	znatelně	k6eAd1	znatelně
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Blanska	Blansko	k1gNnSc2	Blansko
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
CHKO	CHKO	kA	CHKO
Moravský	moravský	k2eAgInSc1d1	moravský
kras	kras	k1gInSc1	kras
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
správním	správní	k2eAgNnSc6d1	správní
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
řadu	řada	k1gFnSc4	řada
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zajímavostem	zajímavost	k1gFnPc3	zajímavost
patří	patřit	k5eAaImIp3nP	patřit
vyhledávaný	vyhledávaný	k2eAgInSc1d1	vyhledávaný
zatopený	zatopený	k2eAgInSc1d1	zatopený
lom	lom	k1gInSc1	lom
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
vlakového	vlakový	k2eAgNnSc2d1	vlakové
nádraží	nádraží	k1gNnSc2	nádraží
přímo	přímo	k6eAd1	přímo
vedle	vedle	k7c2	vedle
železniční	železniční	k2eAgFnSc2d1	železniční
tratě	trať	k1gFnSc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
mimořádně	mimořádně	k6eAd1	mimořádně
kvalitní	kvalitní	k2eAgFnSc4d1	kvalitní
průzračně	průzračně	k6eAd1	průzračně
čistou	čistý	k2eAgFnSc4d1	čistá
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Blansku	Blansko	k1gNnSc6	Blansko
<g/>
.	.	kIx.	.
</s>
<s>
Blansko	Blansko	k1gNnSc1	Blansko
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
Brno-Česká	Brno-Český	k2eAgFnSc1d1	Brno-Česká
Třebová	Třebová	k1gFnSc1	Třebová
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
východiskem	východisko	k1gNnSc7	východisko
turistických	turistický	k2eAgFnPc2d1	turistická
cest	cesta	k1gFnPc2	cesta
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Moravského	moravský	k2eAgInSc2d1	moravský
krasu	kras	k1gInSc2	kras
je	být	k5eAaImIp3nS	být
nazýváno	nazývat	k5eAaImNgNnS	nazývat
bránou	brána	k1gFnSc7	brána
Moravského	moravský	k2eAgInSc2d1	moravský
krasu	kras	k1gInSc2	kras
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
slovní	slovní	k2eAgNnSc4d1	slovní
spojení	spojení	k1gNnSc4	spojení
si	se	k3xPyFc3	se
Blansko	Blansko	k1gNnSc1	Blansko
nechalo	nechat	k5eAaPmAgNnS	nechat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zaregistrovat	zaregistrovat	k5eAaPmF	zaregistrovat
jako	jako	k8xC	jako
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
známku	známka	k1gFnSc4	známka
<g/>
.	.	kIx.	.
</s>
<s>
Východně	východně	k6eAd1	východně
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
Blansek	Blansko	k1gNnPc2	Blansko
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Blanska	Blansko	k1gNnSc2	Blansko
leží	ležet	k5eAaImIp3nS	ležet
zřícenina	zřícenina	k1gFnSc1	zřícenina
Čertova	čertův	k2eAgInSc2d1	čertův
hrádku	hrádek	k1gInSc2	hrádek
a	a	k8xC	a
Nový	nový	k2eAgInSc1d1	nový
hrad	hrad	k1gInSc1	hrad
<g/>
.	.	kIx.	.
náměstí	náměstí	k1gNnSc1	náměstí
Republiky	republika	k1gFnSc2	republika
-	-	kIx~	-
náměstí	náměstí	k1gNnSc1	náměstí
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
;	;	kIx,	;
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
leží	ležet	k5eAaImIp3nS	ležet
budova	budova	k1gFnSc1	budova
Komerční	komerční	k2eAgFnSc2d1	komerční
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
Městského	městský	k2eAgInSc2d1	městský
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
hotel	hotel	k1gInSc1	hotel
Dukla	Dukla	k1gFnSc1	Dukla
(	(	kIx(	(
<g/>
zbourán	zbourat	k5eAaPmNgInS	zbourat
r.	r.	kA	r.
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
vybudován	vybudován	k2eAgInSc1d1	vybudován
vodotrysk	vodotrysk	k1gInSc1	vodotrysk
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
lavičky	lavička	k1gFnSc2	lavička
a	a	k8xC	a
také	také	k9	také
parkovací	parkovací	k2eAgNnPc4d1	parkovací
místa	místo	k1gNnPc4	místo
<g/>
;	;	kIx,	;
v	v	k7c6	v
období	období	k1gNnSc6	období
prázdnin	prázdniny	k1gFnPc2	prázdniny
tu	tu	k6eAd1	tu
promítá	promítat	k5eAaImIp3nS	promítat
letní	letní	k2eAgNnSc4d1	letní
kino	kino	k1gNnSc4	kino
náměstí	náměstí	k1gNnSc2	náměstí
Svobody	Svoboda	k1gMnSc2	Svoboda
-	-	kIx~	-
náměstí	náměstí	k1gNnSc1	náměstí
před	před	k7c7	před
budovou	budova	k1gFnSc7	budova
blanenské	blanenský	k2eAgFnSc2d1	blanenská
radnice	radnice	k1gFnSc2	radnice
<g/>
;	;	kIx,	;
uprostřed	uprostřed	k7c2	uprostřed
náměstí	náměstí	k1gNnSc2	náměstí
je	být	k5eAaImIp3nS	být
parčík	parčík	k1gInSc1	parčík
s	s	k7c7	s
lavičkami	lavička	k1gFnPc7	lavička
<g/>
,	,	kIx,	,
fontánkou	fontánka	k1gFnSc7	fontánka
s	s	k7c7	s
labutěmi	labuť	k1gFnPc7	labuť
<g/>
,	,	kIx,	,
bronzovou	bronzový	k2eAgFnSc7d1	bronzová
sochou	socha	k1gFnSc7	socha
hříbat	hříbě	k1gNnPc2	hříbě
(	(	kIx(	(
<g/>
připomínka	připomínka	k1gFnSc1	připomínka
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tu	tu	k6eAd1	tu
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
stávaly	stávat	k5eAaImAgFnP	stávat
<g/>
)	)	kIx)	)
a	a	k8xC	a
památníkem	památník	k1gInSc7	památník
padlých	padlý	k1gMnPc2	padlý
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
Wankelovo	Wankelův	k2eAgNnSc4d1	Wankelovo
náměstí	náměstí	k1gNnSc4	náměstí
-	-	kIx~	-
podlouhlé	podlouhlý	k2eAgNnSc4d1	podlouhlé
náměstí	náměstí	k1gNnSc4	náměstí
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
Blanska	Blansko	k1gNnSc2	Blansko
s	s	k7c7	s
výzdobou	výzdoba	k1gFnSc7	výzdoba
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
litinovými	litinový	k2eAgInPc7d1	litinový
sochami	socha	k1gFnPc7	socha
náměstí	náměstí	k1gNnSc6	náměstí
Míru	mír	k1gInSc2	mír
-	-	kIx~	-
křižovatka	křižovatka	k1gFnSc1	křižovatka
ulic	ulice	k1gFnPc2	ulice
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
Stařeckého	stařecký	k2eAgInSc2d1	stařecký
<g/>
,	,	kIx,	,
Čapkova	Čapkův	k2eAgInSc2d1	Čapkův
a	a	k8xC	a
Bartošova	Bartošův	k2eAgFnSc1d1	Bartošova
Rožmitálova	Rožmitálův	k2eAgFnSc1d1	Rožmitálova
ulice	ulice	k1gFnSc1	ulice
-	-	kIx~	-
nově	nově	k6eAd1	nově
opravená	opravený	k2eAgFnSc1d1	opravená
<g />
.	.	kIx.	.
</s>
<s>
ulice	ulice	k1gFnSc1	ulice
(	(	kIx(	(
<g/>
pěší	pěší	k2eAgFnSc1d1	pěší
zóna	zóna	k1gFnSc1	zóna
<g/>
)	)	kIx)	)
s	s	k7c7	s
cihlově	cihlově	k6eAd1	cihlově
červenou	červený	k2eAgFnSc7d1	červená
dlažbou	dlažba	k1gFnSc7	dlažba
<g/>
,	,	kIx,	,
lavičkami	lavička	k1gFnPc7	lavička
park	park	k1gInSc1	park
u	u	k7c2	u
Zborováka	Zborovák	k1gMnSc2	Zborovák
-	-	kIx~	-
park	park	k1gInSc1	park
u	u	k7c2	u
vlakové	vlakový	k2eAgFnSc2d1	vlaková
zastávky	zastávka	k1gFnSc2	zastávka
s	s	k7c7	s
památníkem	památník	k1gInSc7	památník
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Zborova	Zborov	k1gInSc2	Zborov
blanenský	blanenský	k2eAgInSc4d1	blanenský
zámek	zámek	k1gInSc4	zámek
se	s	k7c7	s
zámeckým	zámecký	k2eAgInSc7d1	zámecký
parkem	park	k1gInSc7	park
-	-	kIx~	-
v	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
umístěno	umístěn	k2eAgNnSc4d1	umístěno
Muzeum	muzeum	k1gNnSc4	muzeum
Blansko	Blansko	k1gNnSc1	Blansko
s	s	k7c7	s
několika	několik	k4yIc2	několik
stálými	stálý	k2eAgFnPc7d1	stálá
expozicemi	expozice	k1gFnPc7	expozice
<g/>
:	:	kIx,	:
historie	historie	k1gFnPc1	historie
železářství	železářství	k1gNnPc2	železářství
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
blanenská	blanenský	k2eAgFnSc1d1	blanenská
umělecká	umělecký	k2eAgFnSc1d1	umělecká
litina	litina	k1gFnSc1	litina
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
Moravský	moravský	k2eAgInSc1d1	moravský
kras	kras	k1gInSc1	kras
<g/>
,	,	kIx,	,
historické	historický	k2eAgInPc1d1	historický
interiéry	interiér	k1gInPc1	interiér
<g/>
;	;	kIx,	;
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
zámku	zámek	k1gInSc6	zámek
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
pobýval	pobývat	k5eAaImAgMnS	pobývat
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
archeolog	archeolog	k1gMnSc1	archeolog
Jindřich	Jindřich	k1gMnSc1	Jindřich
Wankel	Wankel	k1gMnSc1	Wankel
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
von	von	k1gInSc4	von
Saar	Saara	k1gFnPc2	Saara
nebo	nebo	k8xC	nebo
Karolina	Karolinum	k1gNnSc2	Karolinum
Meineke	Meineke	k1gNnSc2	Meineke
ADAST	ADAST	kA	ADAST
-	-	kIx~	-
správní	správní	k2eAgFnSc1d1	správní
budova	budova	k1gFnSc1	budova
firmy	firma	k1gFnSc2	firma
ADAST	ADAST	kA	ADAST
Blansko	Blansko	k1gNnSc1	Blansko
a.	a.	k?	a.
s.	s.	k?	s.
<g />
.	.	kIx.	.
</s>
<s>
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
ve	v	k7c6	v
windsorském	windsorský	k2eAgInSc6d1	windsorský
stylu	styl	k1gInSc6	styl
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
zámku	zámek	k1gInSc2	zámek
Miramare	Miramar	k1gMnSc5	Miramar
<g/>
;	;	kIx,	;
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
budově	budova	k1gFnSc6	budova
vinárna	vinárna	k1gFnSc1	vinárna
a	a	k8xC	a
restaurace	restaurace	k1gFnSc1	restaurace
barokní	barokní	k2eAgFnSc1d1	barokní
kostel	kostel	k1gInSc4	kostel
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc2	Martin
(	(	kIx(	(
<g/>
postaven	postavit	k5eAaPmNgInS	postavit
1672	[number]	k4	1672
<g/>
-	-	kIx~	-
<g/>
1691	[number]	k4	1691
<g/>
)	)	kIx)	)
-	-	kIx~	-
původně	původně	k6eAd1	původně
zde	zde	k6eAd1	zde
stál	stát	k5eAaImAgInS	stát
románský	románský	k2eAgInSc1d1	románský
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
gotický	gotický	k2eAgInSc1d1	gotický
<g/>
)	)	kIx)	)
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
pol.	pol.	k?	pol.
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
olomouckým	olomoucký	k2eAgMnSc7d1	olomoucký
biskupem	biskup	k1gMnSc7	biskup
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Zdíkem	Zdík	k1gMnSc7	Zdík
<g/>
;	;	kIx,	;
starobylý	starobylý	k2eAgInSc1d1	starobylý
zvon	zvon	k1gInSc1	zvon
ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
;	;	kIx,	;
Kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgNnSc7d1	významné
zastavením	zastavení	k1gNnSc7	zastavení
na	na	k7c6	na
evropské	evropský	k2eAgFnSc6d1	Evropská
stezce	stezka	k1gFnSc6	stezka
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc2	Martin
"	"	kIx"	"
<g/>
Via	via	k7c4	via
Sancti	Sancti	k1gNnSc4	Sancti
Martini	martini	k1gNnSc2	martini
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dokládá	dokládat	k5eAaImIp3nS	dokládat
informační	informační	k2eAgFnPc4d1	informační
tabule	tabule	k1gFnPc4	tabule
na	na	k7c4	na
zdi	zeď	k1gFnPc4	zeď
farní	farní	k2eAgFnSc2d1	farní
zahrady	zahrada	k1gFnSc2	zahrada
a	a	k8xC	a
také	také	k9	také
symbolická	symbolický	k2eAgFnSc1d1	symbolická
bronzová	bronzový	k2eAgFnSc1d1	bronzová
šlépěj	šlépěj	k1gFnSc1	šlépěj
sv.	sv.	kA	sv.
Martina	Martina	k1gFnSc1	Martina
umístěná	umístěný	k2eAgFnSc1d1	umístěná
v	v	k7c6	v
lodi	loď	k1gFnSc6	loď
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Klamova	Klamův	k2eAgFnSc1d1	Klamova
huť	huť	k1gFnSc1	huť
(	(	kIx(	(
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
-	-	kIx~	-
poslední	poslední	k2eAgFnSc1d1	poslední
zachovaná	zachovaný	k2eAgFnSc1d1	zachovaná
huť	huť	k1gFnSc1	huť
železáren	železárna	k1gFnPc2	železárna
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
muzeum	muzeum	k1gNnSc1	muzeum
umělecké	umělecký	k2eAgFnSc2d1	umělecká
litiny	litina	k1gFnSc2	litina
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
opět	opět	k6eAd1	opět
výrobní	výrobní	k2eAgInPc4d1	výrobní
prostory	prostor	k1gInPc4	prostor
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
kostelík	kostelík	k1gInSc4	kostelík
svaté	svatý	k2eAgFnSc2d1	svatá
Paraskivy	Paraskiva	k1gFnSc2	Paraskiva
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
<g/>
,	,	kIx,	,
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
Blanska	Blansko	k1gNnSc2	Blansko
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
radnice	radnice	k1gFnSc1	radnice
-	-	kIx~	-
postavena	postaven	k2eAgFnSc1d1	postavena
r.	r.	kA	r.
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
současnou	současný	k2eAgFnSc4d1	současná
podobu	podoba	k1gFnSc4	podoba
nabyla	nabýt	k5eAaPmAgFnS	nabýt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
dostavěna	dostavěn	k2eAgFnSc1d1	dostavěna
věž	věž	k1gFnSc1	věž
s	s	k7c7	s
hodinami	hodina	k1gFnPc7	hodina
<g/>
;	;	kIx,	;
sídlo	sídlo	k1gNnSc4	sídlo
úřadu	úřad	k1gInSc2	úřad
starosty	starosta	k1gMnSc2	starosta
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
složek	složka	k1gFnPc2	složka
Městského	městský	k2eAgInSc2d1	městský
úřadu	úřad	k1gInSc2	úřad
Kino	kino	k1gNnSc1	kino
Blansko	Blansko	k1gNnSc1	Blansko
-	-	kIx~	-
postaveno	postavit	k5eAaPmNgNnS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
jako	jako	k8xC	jako
Bio	Bio	k?	Bio
Invalidů	invalid	k1gInPc2	invalid
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
bylo	být	k5eAaImAgNnS	být
kompletně	kompletně	k6eAd1	kompletně
<g />
.	.	kIx.	.
</s>
<s>
digitalizováno	digitalizován	k2eAgNnSc1d1	digitalizováno
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
dle	dle	k7c2	dle
návrhu	návrh	k1gInSc2	návrh
věhlasného	věhlasný	k2eAgMnSc2d1	věhlasný
architekta	architekt	k1gMnSc2	architekt
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Fuchse	Fuchs	k1gMnSc2	Fuchs
postaven	postaven	k2eAgInSc4d1	postaven
areál	areál	k1gInSc4	areál
vodárny	vodárna	k1gFnSc2	vodárna
hotel	hotel	k1gInSc1	hotel
Skalní	skalní	k2eAgInSc1d1	skalní
mlýn	mlýn	k1gInSc1	mlýn
-	-	kIx~	-
nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
centrum	centrum	k1gNnSc1	centrum
Moravského	moravský	k2eAgInSc2d1	moravský
krasu	kras	k1gInSc2	kras
památník	památník	k1gInSc1	památník
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
parčíku	parčík	k1gInSc6	parčík
u	u	k7c2	u
křižovatky	křižovatka	k1gFnSc2	křižovatka
ulic	ulice	k1gFnPc2	ulice
Žižkovy	Žižkův	k2eAgFnSc2d1	Žižkova
a	a	k8xC	a
Havlíčkovy	Havlíčkův	k2eAgFnSc2d1	Havlíčkova
památník	památník	k1gInSc4	památník
padlým	padlý	k1gMnPc3	padlý
v	v	k7c4	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
parku	park	k1gInSc6	park
na	na	k7c6	na
Náměstí	náměstí	k1gNnSc6	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
památník	památník	k1gInSc1	památník
hrdinům	hrdina	k1gMnPc3	hrdina
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Zborova	Zborov	k1gInSc2	Zborov
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
odhalen	odhalit	k5eAaPmNgInS	odhalit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
a	a	k8xC	a
přečkal	přečkat	k5eAaPmAgMnS	přečkat
bez	bez	k7c2	bez
úhony	úhona	k1gFnSc2	úhona
dobu	doba	k1gFnSc4	doba
komunistické	komunistický	k2eAgFnSc2d1	komunistická
totality	totalita	k1gFnSc2	totalita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
socha	socha	k1gFnSc1	socha
opravena	opravit	k5eAaPmNgFnS	opravit
a	a	k8xC	a
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
původní	původní	k2eAgNnSc4d1	původní
místo	místo	k1gNnSc4	místo
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
i	i	k9	i
ztracený	ztracený	k2eAgInSc4d1	ztracený
bodák	bodák	k1gInSc4	bodák
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
parčíku	parčík	k1gInSc6	parčík
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Čecha	Čech	k1gMnSc2	Čech
u	u	k7c2	u
zastávky	zastávka	k1gFnSc2	zastávka
ČD	ČD	kA	ČD
Blansko-město	Blanskoěsta	k1gMnSc5	Blansko-města
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc4	pomník
zborovského	zborovský	k2eAgMnSc2d1	zborovský
bojovníka	bojovník	k1gMnSc2	bojovník
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
jediný	jediný	k2eAgInSc1d1	jediný
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
odlit	odlít	k5eAaPmNgInS	odlít
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
podniku	podnik	k1gInSc6	podnik
ČKD	ČKD	kA	ČKD
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
stejný	stejný	k2eAgInSc4d1	stejný
pomník	pomník	k1gInSc4	pomník
určený	určený	k2eAgInSc4d1	určený
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
odhalen	odhalit	k5eAaPmNgInS	odhalit
před	před	k7c7	před
kostelem	kostel	k1gInSc7	kostel
Sv.	sv.	kA	sv.
Anežky	Anežka	k1gFnSc2	Anežka
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1937	[number]	k4	1937
generálem	generál	k1gMnSc7	generál
Stanislavem	Stanislav	k1gMnSc7	Stanislav
Čečkem	Čeček	k1gMnSc7	Čeček
<g/>
,	,	kIx,	,
a	a	k8xC	a
třetí	třetí	k4xOgMnSc1	třetí
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
instalován	instalovat	k5eAaBmNgInS	instalovat
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Haliči	Halič	k1gFnSc6	Halič
<g/>
.	.	kIx.	.
</s>
<s>
Pražskou	pražský	k2eAgFnSc4d1	Pražská
sochu	socha	k1gFnSc4	socha
však	však	k9	však
zničili	zničit	k5eAaPmAgMnP	zničit
nacisté	nacista	k1gMnPc1	nacista
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
osud	osud	k1gInSc1	osud
třetího	třetí	k4xOgInSc2	třetí
pomníku	pomník	k1gInSc2	pomník
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
sochy	socha	k1gFnSc2	socha
je	být	k5eAaImIp3nS	být
ruský	ruský	k2eAgMnSc1d1	ruský
legionář	legionář	k1gMnSc1	legionář
Karel	Karel	k1gMnSc1	Karel
Babka	Babka	k1gMnSc1	Babka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
obnovu	obnova	k1gFnSc4	obnova
pražského	pražský	k2eAgInSc2d1	pražský
pomníku	pomník	k1gInSc2	pomník
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
veřejná	veřejný	k2eAgFnSc1d1	veřejná
sbírka	sbírka	k1gFnSc1	sbírka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
demokratické	demokratický	k2eAgNnSc4d1	demokratické
hnutí	hnutí	k1gNnSc4	hnutí
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
na	na	k7c4	na
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
sochu	socha	k1gFnSc4	socha
kromě	kromě	k7c2	kromě
příspěvků	příspěvek	k1gInPc2	příspěvek
svých	svůj	k3xOyFgMnPc2	svůj
členů	člen	k1gMnPc2	člen
ještě	ještě	k6eAd1	ještě
částku	částka	k1gFnSc4	částka
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
realizace	realizace	k1gFnSc1	realizace
na	na	k7c6	na
původním	původní	k2eAgNnSc6d1	původní
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
4	[number]	k4	4
-	-	kIx~	-
Spořilově	spořilův	k2eAgFnSc3d1	Spořilova
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
neuvěřitelnou	uvěřitelný	k2eNgFnSc7d1	neuvěřitelná
řadou	řada	k1gFnSc7	řada
obtíží	obtíž	k1gFnPc2	obtíž
<g/>
.	.	kIx.	.
památník	památník	k1gInSc1	památník
osvobození	osvobození	k1gNnSc2	osvobození
-	-	kIx~	-
"	"	kIx"	"
<g/>
Rudoarmějec	rudoarmějec	k1gMnSc1	rudoarmějec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
parčíku	parčík	k1gInSc6	parčík
u	u	k7c2	u
křižovatky	křižovatka	k1gFnSc2	křižovatka
ulic	ulice	k1gFnPc2	ulice
Sadové	sadový	k2eAgFnPc4d1	Sadová
<g/>
,	,	kIx,	,
Seifertovy	Seifertův	k2eAgFnPc4d1	Seifertova
a	a	k8xC	a
Hybešovy	Hybešův	k2eAgFnPc4d1	Hybešova
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Alegorie	alegorie	k1gFnSc1	alegorie
válečnictví	válečnictví	k1gNnSc2	válečnictví
<g/>
"	"	kIx"	"
-	-	kIx~	-
dvoutunová	dvoutunový	k2eAgFnSc1d1	dvoutunová
litinová	litinový	k2eAgFnSc1d1	litinová
socha	socha	k1gFnSc1	socha
(	(	kIx(	(
<g/>
Ares	Ares	k1gMnSc1	Ares
odpočívající	odpočívající	k2eAgMnSc1d1	odpočívající
na	na	k7c6	na
válečných	válečný	k2eAgInPc6d1	válečný
trofejích	trofej	k1gInPc6	trofej
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
odlita	odlít	k5eAaPmNgFnS	odlít
pro	pro	k7c4	pro
Trident	Trident	k1gMnSc1	Trident
(	(	kIx(	(
<g/>
Trento	Trento	k1gNnSc1	Trento
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
pohnutým	pohnutý	k2eAgFnPc3d1	pohnutá
událostem	událost	k1gFnPc3	událost
konce	konec	k1gInSc2	konec
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
už	už	k6eAd1	už
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
určení	určení	k1gNnSc2	určení
nedorazila	dorazit	k5eNaPmAgFnS	dorazit
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
parčíku	parčík	k1gInSc6	parčík
u	u	k7c2	u
budovy	budova	k1gFnSc2	budova
ČKD	ČKD	kA	ČKD
<g/>
.	.	kIx.	.
náhrobní	náhrobní	k2eAgInSc4d1	náhrobní
památník	památník	k1gInSc4	památník
první	první	k4xOgFnSc2	první
ženy	žena	k1gFnSc2	žena
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
Viléma	Vilém	k1gMnSc2	Vilém
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Karoliny	Karolinum	k1gNnPc7	Karolinum
Meineke	Meineke	k1gInSc1	Meineke
<g/>
,	,	kIx,	,
obklopený	obklopený	k2eAgInSc1d1	obklopený
nově	nově	k6eAd1	nově
zbudovaným	zbudovaný	k2eAgNnSc7d1	zbudované
rozáriem	rozárium	k1gNnSc7	rozárium
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přímo	přímo	k6eAd1	přímo
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Martina	Martina	k1gFnSc1	Martina
Městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
Blansko	Blansko	k1gNnSc4	Blansko
-	-	kIx~	-
okresní	okresní	k2eAgFnSc1d1	okresní
knihovna	knihovna	k1gFnSc1	knihovna
Galerie	galerie	k1gFnSc2	galerie
města	město	k1gNnSc2	město
Blanska	Blansko	k1gNnSc2	Blansko
-	-	kIx~	-
expozice	expozice	k1gFnSc2	expozice
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
Městský	městský	k2eAgInSc4d1	městský
klub	klub	k1gInSc4	klub
"	"	kIx"	"
<g/>
Dělnický	dělnický	k2eAgInSc4d1	dělnický
dům	dům	k1gInSc4	dům
<g/>
"	"	kIx"	"
-	-	kIx~	-
klubové	klubový	k2eAgNnSc1d1	klubové
zařízení	zařízení	k1gNnSc1	zařízení
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
sálem	sál	k1gInSc7	sál
a	a	k8xC	a
jevištěm	jeviště	k1gNnSc7	jeviště
<g/>
,	,	kIx,	,
malými	malý	k2eAgInPc7d1	malý
sály	sál	k1gInPc7	sál
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
klubovnami	klubovna	k1gFnPc7	klubovna
<g/>
;	;	kIx,	;
součástí	součást	k1gFnSc7	součást
klubu	klub	k1gInSc2	klub
je	být	k5eAaImIp3nS	být
restaurace	restaurace	k1gFnSc1	restaurace
s	s	k7c7	s
letní	letní	k2eAgFnSc7d1	letní
zahrádkou	zahrádka	k1gFnSc7	zahrádka
Poliklinika	poliklinika	k1gFnSc1	poliklinika
a	a	k8xC	a
Nemocnice	nemocnice	k1gFnSc2	nemocnice
Blansko	Blansko	k1gNnSc1	Blansko
Hotel	hotel	k1gInSc1	hotel
Macocha	Macocha	k1gFnSc1	Macocha
Domov	domov	k1gInSc4	domov
důchodců	důchodce	k1gMnPc2	důchodce
Obchodní	obchodní	k2eAgFnSc2d1	obchodní
akademie	akademie	k1gFnSc2	akademie
a	a	k8xC	a
Střední	střední	k2eAgFnSc1d1	střední
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
škola	škola	k1gFnSc1	škola
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
škola	škola	k1gFnSc1	škola
Strojírenský	strojírenský	k2eAgInSc1d1	strojírenský
komplex	komplex	k1gInSc1	komplex
ČKD	ČKD	kA	ČKD
Blansko	Blansko	k1gNnSc4	Blansko
Ježkův	Ježkův	k2eAgInSc1d1	Ježkův
vlečkový	vlečkový	k2eAgInSc1d1	vlečkový
most	most	k1gInSc1	most
-	-	kIx~	-
pocházející	pocházející	k2eAgFnSc2d1	pocházející
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
veden	vést	k5eAaImNgInS	vést
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
.	.	kIx.	.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Blansko	Blansko	k1gNnSc1	Blansko
Obchodní	obchodní	k2eAgFnSc2d1	obchodní
akademie	akademie	k1gFnSc2	akademie
a	a	k8xC	a
Střední	střední	k2eAgFnSc1d1	střední
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
škola	škola	k1gFnSc1	škola
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
technická	technický	k2eAgFnSc1d1	technická
a	a	k8xC	a
gastronomická	gastronomický	k2eAgFnSc1d1	gastronomická
Blansko	Blansko	k1gNnSc4	Blansko
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
gastronomická	gastronomický	k2eAgFnSc1d1	gastronomická
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
ZŠ	ZŠ	kA	ZŠ
Salmova	Salmův	k2eAgFnSc1d1	Salmova
ZŠ	ZŠ	kA	ZŠ
Dvorská	Dvorská	k1gFnSc1	Dvorská
ZŠ	ZŠ	kA	ZŠ
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigua	Garriguus	k1gMnSc2	Garriguus
Masaryka	Masaryk	k1gMnSc2	Masaryk
ZŠ	ZŠ	kA	ZŠ
Erbenova	Erbenův	k2eAgFnSc1d1	Erbenova
ZŠ	ZŠ	kA	ZŠ
Nad	nad	k7c7	nad
Čertovkou	Čertovka	k1gFnSc7	Čertovka
ZUŠ	ZUŠ	kA	ZUŠ
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
,	,	kIx,	,
Zámek	zámek	k1gInSc1	zámek
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
8	[number]	k4	8
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
a	a	k8xC	a
12	[number]	k4	12
částí	část	k1gFnPc2	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Katastrální	katastrální	k2eAgNnPc1d1	katastrální
území	území	k1gNnPc1	území
a	a	k8xC	a
Části	část	k1gFnPc1	část
města	město	k1gNnSc2	město
na	na	k7c6	na
nich	on	k3xPp3gNnPc6	on
ležící	ležící	k2eAgMnPc1d1	ležící
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Blansko	Blansko	k1gNnSc1	Blansko
-	-	kIx~	-
Blansko	Blansko	k1gNnSc4	Blansko
Dolní	dolní	k2eAgFnSc1d1	dolní
Lhota	Lhota	k1gFnSc1	Lhota
-	-	kIx~	-
Dolní	dolní	k2eAgFnSc1d1	dolní
Lhota	Lhota	k1gFnSc1	Lhota
Horní	horní	k2eAgFnSc1d1	horní
Lhota	Lhota	k1gFnSc1	Lhota
u	u	k7c2	u
Blanska	Blansko	k1gNnSc2	Blansko
-	-	kIx~	-
Horní	horní	k2eAgFnSc1d1	horní
Lhota	Lhota	k1gFnSc1	Lhota
Hořice	Hořice	k1gFnPc1	Hořice
u	u	k7c2	u
Blanska	Blansko	k1gNnSc2	Blansko
-	-	kIx~	-
Hořice	Hořice	k1gFnPc1	Hořice
Klepačov	Klepačov	k1gInSc1	Klepačov
-	-	kIx~	-
Klepačov	Klepačov	k1gInSc1	Klepačov
Lažánky	Lažánka	k1gFnSc2	Lažánka
u	u	k7c2	u
Blanska	Blansko	k1gNnSc2	Blansko
-	-	kIx~	-
Lažánky	Lažánka	k1gFnSc2	Lažánka
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
Blansko	Blansko	k1gNnSc1	Blansko
Olešná	Olešný	k2eAgFnSc1d1	Olešná
u	u	k7c2	u
Blanska	Blansko	k1gNnSc2	Blansko
-	-	kIx~	-
Olešná	Olešný	k2eAgFnSc1d1	Olešná
Těchov	Těchov	k1gInSc1	Těchov
-	-	kIx~	-
Těchov	Těchov	k1gInSc1	Těchov
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Obůrka	obůrka	k1gFnSc1	obůrka
<g/>
,	,	kIx,	,
Skalní	skalní	k2eAgInSc1d1	skalní
mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
Žižlavice	Žižlavice	k1gFnSc1	Žižlavice
a	a	k8xC	a
Češkovice	Češkovice	k1gFnSc1	Češkovice
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
neoficiálně	neoficiálně	k6eAd1	neoficiálně
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
městské	městský	k2eAgFnPc4d1	městská
čtvrti	čtvrt	k1gFnPc4	čtvrt
<g/>
:	:	kIx,	:
Zborovce	Zborovec	k1gMnSc2	Zborovec
Sever	sever	k1gInSc1	sever
Staré	Staré	k2eAgFnSc2d1	Staré
bytovky	bytovka	k1gFnSc2	bytovka
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
<g/>
)	)	kIx)	)
Písečná	písečný	k2eAgNnPc4d1	písečné
Podlesí	podlesí	k1gNnPc4	podlesí
Blansko-město	Blanskoěsta	k1gMnSc5	Blansko-města
Staré	Staré	k2eAgFnPc1d1	Staré
Blansko	Blansko	k1gNnSc4	Blansko
Vojánky	Vojánka	k1gFnSc2	Vojánka
Arnoštovo	Arnoštův	k2eAgNnSc4d1	Arnoštovo
údolí	údolí	k1gNnPc4	údolí
Související	související	k2eAgFnPc4d1	související
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Okres	okres	k1gInSc4	okres
Blansko	Blansko	k1gNnSc1	Blansko
a	a	k8xC	a
Obvod	obvod	k1gInSc1	obvod
obce	obec	k1gFnSc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
.	.	kIx.	.
</s>
<s>
Blansko	Blansko	k1gNnSc1	Blansko
bylo	být	k5eAaImAgNnS	být
dříve	dříve	k6eAd2	dříve
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
obcí	obec	k1gFnSc7	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
a	a	k8xC	a
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
Blansko	Blansko	k1gNnSc1	Blansko
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
116	[number]	k4	116
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
ORP	ORP	kA	ORP
z	z	k7c2	z
43	[number]	k4	43
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
byla	být	k5eAaImAgFnS	být
dána	dát	k5eAaPmNgFnS	dát
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Brno	Brno	k1gNnSc4	Brno
-	-	kIx~	-
Česká	český	k2eAgFnSc1d1	Česká
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
železniční	železniční	k2eAgFnSc2d1	železniční
tratě	trať	k1gFnSc2	trať
do	do	k7c2	do
Vyškova	Vyškov	k1gInSc2	Vyškov
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
realizováno	realizován	k2eAgNnSc1d1	realizováno
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
nebyly	být	k5eNaImAgFnP	být
počátkem	počátek	k1gInSc7	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
realizovány	realizován	k2eAgInPc1d1	realizován
návrhy	návrh	k1gInPc1	návrh
na	na	k7c6	na
zbudování	zbudování	k1gNnSc6	zbudování
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
do	do	k7c2	do
Moravského	moravský	k2eAgInSc2d1	moravský
krasu	kras	k1gInSc2	kras
<g/>
,	,	kIx,	,
Líšně	Líšeň	k1gFnSc2	Líšeň
<g/>
,	,	kIx,	,
Prostějova	Prostějov	k1gInSc2	Prostějov
či	či	k8xC	či
Tišnova	Tišnov	k1gInSc2	Tišnov
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
rozvoji	rozvoj	k1gInSc3	rozvoj
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
nedošlo	dojít	k5eNaPmAgNnS	dojít
a	a	k8xC	a
Blanensko	Blanensko	k1gNnSc1	Blanensko
se	se	k3xPyFc4	se
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
začalo	začít	k5eAaPmAgNnS	začít
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c4	na
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
realizovali	realizovat	k5eAaBmAgMnP	realizovat
soukromí	soukromý	k2eAgMnPc1d1	soukromý
dopravci	dopravce	k1gMnPc1	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
státní	státní	k2eAgInSc1d1	státní
podnik	podnik	k1gInSc1	podnik
ČSAD	ČSAD	kA	ČSAD
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
dopravním	dopravní	k2eAgInSc7d1	dopravní
závodem	závod	k1gInSc7	závod
národního	národní	k2eAgInSc2d1	národní
podniku	podnik	k1gInSc2	podnik
ČSAD	ČSAD	kA	ČSAD
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
na	na	k7c4	na
trh	trh	k1gInSc4	trh
další	další	k2eAgMnPc1d1	další
dopravci	dopravce	k1gMnPc1	dopravce
<g/>
,	,	kIx,	,
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
byla	být	k5eAaImAgFnS	být
dopravní	dopravní	k2eAgFnSc1d1	dopravní
firma	firma	k1gFnSc1	firma
ČSAD	ČSAD	kA	ČSAD
Blansko	Blansko	k1gNnSc1	Blansko
privatizována	privatizovat	k5eAaImNgFnS	privatizovat
a	a	k8xC	a
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
ČAD	Čad	k1gInSc4	Čad
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
.	.	kIx.	.
</s>
<s>
Současnou	současný	k2eAgFnSc4d1	současná
městskou	městský	k2eAgFnSc4d1	městská
hromadnou	hromadný	k2eAgFnSc4d1	hromadná
dopravu	doprava	k1gFnSc4	doprava
ve	v	k7c6	v
městě	město	k1gNnSc6	město
i	i	k9	i
do	do	k7c2	do
okolních	okolní	k2eAgNnPc2d1	okolní
měst	město	k1gNnPc2	město
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
Integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
dopravního	dopravní	k2eAgInSc2d1	dopravní
systému	systém	k1gInSc2	systém
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Blansko	Blansko	k1gNnSc1	Blansko
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
označováno	označovat	k5eAaImNgNnS	označovat
také	také	k9	také
za	za	k7c4	za
"	"	kIx"	"
<g/>
město	město	k1gNnSc4	město
sportu	sport	k1gInSc2	sport
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
zde	zde	k6eAd1	zde
působí	působit	k5eAaImIp3nS	působit
řada	řada	k1gFnSc1	řada
sportovních	sportovní	k2eAgInPc2d1	sportovní
klubů	klub	k1gInPc2	klub
a	a	k8xC	a
oddílů	oddíl	k1gInPc2	oddíl
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
vzešla	vzejít	k5eAaPmAgFnS	vzejít
mnohá	mnohý	k2eAgNnPc4d1	mnohé
významná	významný	k2eAgNnPc4d1	významné
jména	jméno	k1gNnPc4	jméno
českého	český	k2eAgInSc2d1	český
vrcholového	vrcholový	k2eAgInSc2d1	vrcholový
sportu	sport	k1gInSc2	sport
(	(	kIx(	(
<g/>
především	především	k9	především
blanenský	blanenský	k2eAgMnSc1d1	blanenský
rodák	rodák	k1gMnSc1	rodák
a	a	k8xC	a
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
v	v	k7c6	v
hodu	hod	k1gInSc6	hod
diskem	disk	k1gInSc7	disk
Ludvík	Ludvík	k1gMnSc1	Ludvík
Daněk	Daněk	k1gMnSc1	Daněk
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
široká	široký	k2eAgFnSc1d1	široká
škála	škála	k1gFnSc1	škála
moderních	moderní	k2eAgNnPc2d1	moderní
sportovišť	sportoviště	k1gNnPc2	sportoviště
(	(	kIx(	(
<g/>
kryté	krytý	k2eAgFnPc1d1	krytá
lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
aquapark	aquapark	k1gInSc1	aquapark
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
rekonstruovaná	rekonstruovaný	k2eAgNnPc4d1	rekonstruované
hřiště	hřiště	k1gNnPc4	hřiště
a	a	k8xC	a
tělocvičny	tělocvična	k1gFnPc4	tělocvična
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
významných	významný	k2eAgFnPc2d1	významná
sportovních	sportovní	k2eAgFnPc2d1	sportovní
a	a	k8xC	a
kulturních	kulturní	k2eAgFnPc2d1	kulturní
akcí	akce	k1gFnPc2	akce
nadregionálního	nadregionální	k2eAgInSc2d1	nadregionální
významu	význam	k1gInSc2	význam
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
každoročně	každoročně	k6eAd1	každoročně
konají	konat	k5eAaImIp3nP	konat
např.	např.	kA	např.
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
Půlmaraton	půlmaraton	k1gInSc4	půlmaraton
Moravským	moravský	k2eAgInSc7d1	moravský
krasem	kras	k1gInSc7	kras
<g/>
,	,	kIx,	,
běh	běh	k1gInSc1	běh
You	You	k1gFnPc2	You
Dream	Dream	k1gInSc1	Dream
We	We	k1gMnSc1	We
Run	run	k1gInSc1	run
<g/>
,	,	kIx,	,
BAMBIfest	BAMBIfest	k1gFnSc1	BAMBIfest
<g/>
(	(	kIx(	(
bambiriáda	bambiriáda	k1gFnSc1	bambiriáda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Blanenská	blanenský	k2eAgFnSc1d1	blanenská
desítka	desítka	k1gFnSc1	desítka
<g/>
,	,	kIx,	,
Blanenské	blanenský	k2eAgNnSc1d1	blanenské
vítání	vítání	k1gNnSc1	vítání
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc4	Martin
a	a	k8xC	a
podzimní	podzimní	k2eAgInSc4d1	podzimní
RAJBAS	RAJBAS	kA	RAJBAS
festival	festival	k1gInSc1	festival
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
setkání	setkání	k1gNnSc2	setkání
a	a	k8xC	a
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
pořádaný	pořádaný	k2eAgInSc4d1	pořádaný
Sdružením	sdružení	k1gNnSc7	sdružení
Horizont	horizont	k1gInSc4	horizont
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
.	.	kIx.	.
</s>
<s>
Blanenský	blanenský	k2eAgInSc1d1	blanenský
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
FK	FK	kA	FK
Blansko	Blansko	k1gNnSc4	Blansko
hrál	hrát	k5eAaImAgInS	hrát
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
A	a	k9	a
třídu	třída	k1gFnSc4	třída
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
každoročnímu	každoroční	k2eAgInSc3d1	každoroční
postupu	postup	k1gInSc3	postup
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
hraje	hrát	k5eAaImIp3nS	hrát
MSFL	MSFL	kA	MSFL
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
následovaly	následovat	k5eAaImAgInP	následovat
dva	dva	k4xCgInPc1	dva
sestupy	sestup	k1gInPc1	sestup
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
pak	pak	k6eAd1	pak
postup	postup	k1gInSc4	postup
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
tak	tak	k9	tak
klub	klub	k1gInSc1	klub
hraje	hrát	k5eAaImIp3nS	hrát
Moravskoslezskou	moravskoslezský	k2eAgFnSc4d1	Moravskoslezská
divizi	divize	k1gFnSc4	divize
D.	D.	kA	D.
Hokejový	hokejový	k2eAgInSc4d1	hokejový
klub	klub	k1gInSc4	klub
HC	HC	kA	HC
Blansko	Blansko	k1gNnSc1	Blansko
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
2	[number]	k4	2
<g/>
.	.	kIx.	.
národní	národní	k2eAgFnSc4d1	národní
hokejovou	hokejový	k2eAgFnSc4d1	hokejová
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
je	být	k5eAaImIp3nS	být
dvojnásobná	dvojnásobný	k2eAgFnSc1d1	dvojnásobná
účast	účast	k1gFnSc1	účast
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
NL	NL	kA	NL
v	v	k7c6	v
sezonách	sezona	k1gFnPc6	sezona
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
a	a	k8xC	a
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
sezonou	sezona	k1gFnSc7	sezona
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
prodal	prodat	k5eAaPmAgInS	prodat
druholigovou	druholigový	k2eAgFnSc4d1	druholigová
licenci	licence	k1gFnSc4	licence
z	z	k7c2	z
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
důvodů	důvod	k1gInPc2	důvod
týmu	tým	k1gInSc2	tým
Vsetín	Vsetín	k1gInSc1	Vsetín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
se	se	k3xPyFc4	se
blanenský	blanenský	k2eAgInSc1d1	blanenský
tým	tým	k1gInSc1	tým
sloučil	sloučit	k5eAaPmAgInS	sloučit
s	s	k7c7	s
Technikou	technika	k1gFnSc7	technika
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
VSK	VSK	kA	VSK
Technika	technikum	k1gNnSc2	technikum
Blansko	Blansko	k1gNnSc4	Blansko
v	v	k7c6	v
Blansku	Blansko	k1gNnSc6	Blansko
hrál	hrát	k5eAaImAgMnS	hrát
třetí	třetí	k4xOgFnSc1	třetí
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
však	však	k9	však
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
ukončena	ukončit	k5eAaPmNgFnS	ukončit
a	a	k8xC	a
hokejový	hokejový	k2eAgInSc4d1	hokejový
klub	klub	k1gInSc4	klub
v	v	k7c6	v
Blansku	Blansko	k1gNnSc6	Blansko
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
sezóny	sezóna	k1gFnSc2	sezóna
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
však	však	k9	však
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
nový	nový	k2eAgInSc4d1	nový
tým	tým	k1gInSc4	tým
DYNAMITERS	DYNAMITERS	kA	DYNAMITERS
Blansko	Blansko	k1gNnSc1	Blansko
HK	HK	kA	HK
<g/>
,	,	kIx,	,
složený	složený	k2eAgInSc1d1	složený
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
odchovanců	odchovanec	k1gMnPc2	odchovanec
původního	původní	k2eAgInSc2d1	původní
hokejového	hokejový	k2eAgInSc2d1	hokejový
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
zde	zde	k6eAd1	zde
také	také	k9	také
velmi	velmi	k6eAd1	velmi
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
judistický	judistický	k2eAgInSc1d1	judistický
oddíl	oddíl	k1gInSc1	oddíl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dokázal	dokázat	k5eAaPmAgInS	dokázat
vychovat	vychovat	k5eAaPmF	vychovat
několik	několik	k4yIc4	několik
mistrů	mistr	k1gMnPc2	mistr
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
úspěšně	úspěšně	k6eAd1	úspěšně
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
i	i	k9	i
baseballový	baseballový	k2eAgInSc1d1	baseballový
oddíl	oddíl	k1gInSc1	oddíl
TJ	tj	kA	tj
Olympia	Olympia	k1gFnSc1	Olympia
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
výborně	výborně	k6eAd1	výborně
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
mládeží	mládež	k1gFnSc7	mládež
a	a	k8xC	a
kromě	kromě	k7c2	kromě
několika	několik	k4yIc2	několik
klubových	klubový	k2eAgNnPc2d1	klubové
medailových	medailový	k2eAgNnPc2d1	medailové
umístění	umístění	k1gNnSc2	umístění
na	na	k7c6	na
MČR	MČR	kA	MČR
<g/>
,	,	kIx,	,
vychoval	vychovat	k5eAaPmAgInS	vychovat
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
nejen	nejen	k6eAd1	nejen
mládežnických	mládežnický	k2eAgFnPc2d1	mládežnická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
seniorských	seniorský	k2eAgMnPc2d1	seniorský
reprezentantů	reprezentant	k1gMnPc2	reprezentant
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Mužský	mužský	k2eAgInSc1d1	mužský
tým	tým	k1gInSc1	tým
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
do	do	k7c2	do
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
nepřetržitým	přetržitý	k2eNgMnSc7d1	nepřetržitý
účastníkem	účastník	k1gMnSc7	účastník
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
soutěže	soutěž	k1gFnSc2	soutěž
v	v	k7c6	v
baseballu	baseball	k1gInSc6	baseball
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
České	český	k2eAgFnSc2d1	Česká
baseballové	baseballový	k2eAgFnSc2d1	baseballová
extraligy	extraliga	k1gFnSc2	extraliga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sestupu	sestup	k1gInSc6	sestup
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
je	být	k5eAaImIp3nS	být
účastníkem	účastník	k1gMnSc7	účastník
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
baseballové	baseballový	k2eAgFnSc2d1	baseballová
ligy	liga	k1gFnSc2	liga
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
a	a	k8xC	a
2010	[number]	k4	2010
bronzová	bronzový	k2eAgNnPc1d1	bronzové
umístění	umístění	k1gNnSc4	umístění
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
Olympii	Olympia	k1gFnSc3	Olympia
soutěž	soutěž	k1gFnSc1	soutěž
podařila	podařit	k5eAaPmAgFnS	podařit
vyhrát	vyhrát	k5eAaPmF	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Baráži	baráž	k1gFnSc6	baráž
o	o	k7c4	o
extraligu	extraliga	k1gFnSc4	extraliga
porazila	porazit	k5eAaPmAgFnS	porazit
tým	tým	k1gInSc1	tým
Skokani	skokan	k1gMnPc1	skokan
Olomouc	Olomouc	k1gFnSc4	Olomouc
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
a	a	k8xC	a
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
české	český	k2eAgFnSc2d1	Česká
baseballové	baseballový	k2eAgFnSc2d1	baseballová
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Blanenský	blanenský	k2eAgInSc1d1	blanenský
baseballový	baseballový	k2eAgInSc1d1	baseballový
areál	areál	k1gInSc1	areál
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
kadetů	kadet	k1gMnPc2	kadet
(	(	kIx(	(
<g/>
prvenství	prvenství	k1gNnSc1	prvenství
získalo	získat	k5eAaPmAgNnS	získat
družstvo	družstvo	k1gNnSc4	družstvo
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
odehrála	odehrát	k5eAaPmAgFnS	odehrát
část	část	k1gFnSc1	část
Mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejhezčím	hezký	k2eAgFnPc3d3	nejhezčí
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
už	už	k6eAd1	už
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
řídí	řídit	k5eAaImIp3nS	řídit
město	město	k1gNnSc4	město
široká	široký	k2eAgFnSc1d1	široká
koalice	koalice	k1gFnSc1	koalice
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
a	a	k8xC	a
Sdružení	sdružení	k1gNnSc2	sdružení
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ustavujícím	ustavující	k2eAgNnSc6d1	ustavující
zasedání	zasedání	k1gNnSc6	zasedání
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
starostou	starosta	k1gMnSc7	starosta
opětovně	opětovně	k6eAd1	opětovně
zvolen	zvolit	k5eAaPmNgMnS	zvolit
starostou	starosta	k1gMnSc7	starosta
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
starosta	starosta	k1gMnSc1	starosta
Ivo	Ivo	k1gMnSc1	Ivo
Polák	Polák	k1gMnSc1	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Uvolněným	uvolněný	k2eAgMnSc7d1	uvolněný
místostarostou	místostarosta	k1gMnSc7	místostarosta
zůstal	zůstat	k5eAaPmAgMnS	zůstat
Jiří	Jiří	k1gMnSc1	Jiří
Crha	Crha	k1gMnSc1	Crha
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
a	a	k8xC	a
neuvolněným	uvolněný	k2eNgInSc7d1	neuvolněný
pak	pak	k6eAd1	pak
Jaromír	Jaromír	k1gMnSc1	Jaromír
Roučka	Roučka	k1gMnSc1	Roučka
z	z	k7c2	z
Volby	volba	k1gFnSc2	volba
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Ježek	Ježek	k1gMnSc1	Ježek
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
-	-	kIx~	-
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
prosperujícího	prosperující	k2eAgInSc2d1	prosperující
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
podniku	podnik	k1gInSc2	podnik
v	v	k7c6	v
Blansku	Blansko	k1gNnSc6	Blansko
<g/>
,	,	kIx,	,
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
starosta	starosta	k1gMnSc1	starosta
Blanska	Blansko	k1gNnSc2	Blansko
<g/>
,	,	kIx,	,
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
povýšení	povýšení	k1gNnSc6	povýšení
na	na	k7c4	na
město	město	k1gNnSc4	město
v	v	k7c6	v
r.	r.	kA	r.
1905	[number]	k4	1905
Karel	Karel	k1gMnSc1	Karel
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Maška	Mašek	k1gMnSc2	Mašek
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
-	-	kIx~	-
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
archeolog	archeolog	k1gMnSc1	archeolog
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
<g />
.	.	kIx.	.
</s>
<s>
světoznámých	světoznámý	k2eAgNnPc2d1	světoznámé
archeologických	archeologický	k2eAgNnPc2d1	Archeologické
nalezišť	naleziště	k1gNnPc2	naleziště
Předmostí	předmostí	k1gNnSc2	předmostí
u	u	k7c2	u
Přerova	Přerov	k1gInSc2	Přerov
a	a	k8xC	a
jeskyně	jeskyně	k1gFnSc1	jeskyně
Šipka	šipka	k1gFnSc1	šipka
u	u	k7c2	u
Štramberka	Štramberk	k1gInSc2	Štramberk
Hugo	Hugo	k1gMnSc1	Hugo
Václav	Václav	k1gMnSc1	Václav
Sáňka	Sáňka	k1gMnSc1	Sáňka
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
-	-	kIx~	-
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
speleolog	speleolog	k1gMnSc1	speleolog
<g/>
,	,	kIx,	,
archeolog	archeolog	k1gMnSc1	archeolog
<g/>
,	,	kIx,	,
blanenský	blanenský	k2eAgMnSc1d1	blanenský
kronikář	kronikář	k1gMnSc1	kronikář
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Bakeš	Bakeš	k1gMnSc1	Bakeš
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
-	-	kIx~	-
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
chirurg	chirurg	k1gMnSc1	chirurg
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
<g />
.	.	kIx.	.
</s>
<s>
ústavu	ústava	k1gFnSc4	ústava
pro	pro	k7c4	pro
onkologické	onkologický	k2eAgMnPc4d1	onkologický
pacienty	pacient	k1gMnPc4	pacient
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
Josef	Josef	k1gMnSc1	Josef
Pilnáček	Pilnáček	k1gMnSc1	Pilnáček
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
-	-	kIx~	-
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
moravský	moravský	k2eAgMnSc1d1	moravský
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
genealog	genealog	k1gMnSc1	genealog
Rudolf	Rudolf	k1gMnSc1	Rudolf
Barák	Barák	k1gMnSc1	Barák
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
-	-	kIx~	-
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
funkcionář	funkcionář	k1gMnSc1	funkcionář
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953	[number]	k4	1953
<g/>
-	-	kIx~	-
<g/>
1961	[number]	k4	1961
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Ludvík	Ludvík	k1gMnSc1	Ludvík
Daněk	Daněk	k1gMnSc1	Daněk
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1937	[number]	k4	1937
-	-	kIx~	-
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
v	v	k7c6	v
hodu	hod	k1gInSc6	hod
diskem	disk	k1gInSc7	disk
Roman	Roman	k1gMnSc1	Roman
Meluzín	Meluzín	k1gMnSc1	Meluzín
(	(	kIx(	(
<g/>
*	*	kIx~	*
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Kromě	kromě	k7c2	kromě
rodáků	rodák	k1gMnPc2	rodák
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
městem	město	k1gNnSc7	město
také	také	k9	také
spojeny	spojit	k5eAaPmNgFnP	spojit
další	další	k2eAgFnPc1d1	další
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tu	tu	k6eAd1	tu
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
pobývaly	pobývat	k5eAaImAgFnP	pobývat
a	a	k8xC	a
působily	působit	k5eAaImAgFnP	působit
<g/>
/	/	kIx~	/
<g/>
působí	působit	k5eAaImIp3nS	působit
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Reichenbach	Reichenbach	k1gMnSc1	Reichenbach
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1788	[number]	k4	1788
-	-	kIx~	-
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
metalurg	metalurg	k1gMnSc1	metalurg
a	a	k8xC	a
průmyslník	průmyslník	k1gMnSc1	průmyslník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
v	v	k7c6	v
Blansku	Blansko	k1gNnSc6	Blansko
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
parafín	parafín	k1gInSc1	parafín
Ludwig	Ludwig	k1gMnSc1	Ludwig
Georg	Georg	k1gMnSc1	Georg
Treviranus	Treviranus	k1gMnSc1	Treviranus
(	(	kIx(	(
<g/>
1790	[number]	k4	1790
-	-	kIx~	-
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
konstruktér	konstruktér	k1gMnSc1	konstruktér
prvních	první	k4xOgInPc2	první
průmyslově	průmyslově	k6eAd1	průmyslově
využitelných	využitelný	k2eAgInPc2d1	využitelný
parostrojů	parostroj	k1gInPc2	parostroj
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
vůbec	vůbec	k9	vůbec
Jindřich	Jindřich	k1gMnSc1	Jindřich
Wankel	Wankel	k1gMnSc1	Wankel
(	(	kIx(	(
<g/>
1821	[number]	k4	1821
-	-	kIx~	-
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
archeolog	archeolog	k1gMnSc1	archeolog
a	a	k8xC	a
speleolog	speleolog	k1gMnSc1	speleolog
Karel	Karel	k1gMnSc1	Karel
Absolon	Absolon	k1gMnSc1	Absolon
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
-	-	kIx~	-
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
badatel	badatel	k1gMnSc1	badatel
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
krasu	kras	k1gInSc6	kras
Erich	Erich	k1gMnSc1	Erich
Roučka	Roučka	k1gMnSc1	Roučka
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
-	-	kIx~	-
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
továrny	továrna	k1gFnSc2	továrna
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
elektrických	elektrický	k2eAgInPc2d1	elektrický
měřicích	měřicí	k2eAgInPc2d1	měřicí
přístrojů	přístroj	k1gInPc2	přístroj
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Metra	metro	k1gNnSc2	metro
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
)	)	kIx)	)
Miloslav	Miloslav	k1gMnSc1	Miloslav
Kala	Kala	k1gMnSc1	Kala
(	(	kIx(	(
<g/>
*	*	kIx~	*
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Jaromír	Jaromír	k1gMnSc1	Jaromír
Blažek	Blažek	k1gMnSc1	Blažek
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
brankář	brankář	k1gMnSc1	brankář
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
reprezentant	reprezentant	k1gMnSc1	reprezentant
ČR	ČR	kA	ČR
Yvetta	Yvetta	k1gFnSc1	Yvetta
Hlaváčová	Hlaváčová	k1gFnSc1	Hlaváčová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
reprezentantka	reprezentantka	k1gFnSc1	reprezentantka
ČR	ČR	kA	ČR
v	v	k7c6	v
dálkovém	dálkový	k2eAgNnSc6d1	dálkové
plavání	plavání	k1gNnSc6	plavání
a	a	k8xC	a
příležitostná	příležitostný	k2eAgFnSc1d1	příležitostná
modelka	modelka	k1gFnSc1	modelka
U	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc2	Martin
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
Karoliny	Karolinum	k1gNnPc7	Karolinum
Meineke	Meinek	k1gFnSc2	Meinek
<g/>
,	,	kIx,	,
první	první	k4xOgFnPc1	první
manželky	manželka	k1gFnPc1	manželka
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
Viléma	Vilém	k1gMnSc2	Vilém
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
poslední	poslední	k2eAgFnSc1d1	poslední
léta	léto	k1gNnSc2	léto
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
trávila	trávit	k5eAaImAgFnS	trávit
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Blansku	Blansko	k1gNnSc6	Blansko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Bouloc	Bouloc	k1gFnSc1	Bouloc
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Komárno	Komárno	k1gNnSc1	Komárno
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Legnica	Legnic	k1gInSc2	Legnic
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Mürzzuschlag	Mürzzuschlaga	k1gFnPc2	Mürzzuschlaga
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
Scandiano	Scandiana	k1gFnSc5	Scandiana
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Vacquiers	Vacquiers	k1gInSc1	Vacquiers
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Villeneuve	Villeneuev	k1gFnSc2	Villeneuev
lè	lè	k?	lè
Bouloc	Bouloc	k1gInSc1	Bouloc
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
</s>
