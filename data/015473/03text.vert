<s>
Trnava	Trnava	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
slovenském	slovenský	k2eAgNnSc6d1
městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Trnava	Trnava	k1gFnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Trnava	Trnava	k1gFnSc1
Historické	historický	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
z	z	k7c2
letadla	letadlo	k1gNnSc2
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
40	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
35	#num#	k4
<g/>
′	′	k?
<g/>
10	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
144	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Trnavský	trnavský	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
Trnava	Trnava	k1gFnSc1
Tradiční	tradiční	k2eAgFnSc1d1
region	region	k1gInSc4
</s>
<s>
Dolní	dolní	k2eAgNnSc1d1
Pováží	povážet	k5eAaImIp3nS,k5eAaPmIp3nS
Administrativní	administrativní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
</s>
<s>
6	#num#	k4
místních	místní	k2eAgMnPc2d1
částí	část	k1gFnPc2
</s>
<s>
Trnava	Trnava	k1gFnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
71,5	71,5	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
65	#num#	k4
382	#num#	k4
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
913,9	913,9	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
Krajské	krajský	k2eAgNnSc1d1
město	město	k1gNnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Peter	Peter	k1gMnSc1
Bročka	Broček	k1gMnSc2
Vznik	vznik	k1gInSc1
</s>
<s>
1211	#num#	k4
(	(	kIx(
<g/>
první	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.trnava.sk/sk	www.trnava.sk/sk	k1gInSc1
E-mail	e-mail	k1gInSc1
</s>
<s>
msutt@trnava.sk	msutt@trnava.sk	k1gInSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Mestský	Mestský	k2eAgInSc1d1
úrad	úrada	k1gFnPc2
TrnavaTrhová	TrnavaTrhová	k1gFnSc1
3917	#num#	k4
01	#num#	k4
Trnava	Trnava	k1gFnSc1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
033	#num#	k4
PSČ	PSČ	kA
</s>
<s>
917	#num#	k4
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
917	#num#	k4
0	#num#	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
917	#num#	k4
05	#num#	k4
a	a	k8xC
917	#num#	k4
08	#num#	k4
Označení	označení	k1gNnPc2
vozidel	vozidlo	k1gNnPc2
</s>
<s>
TT	TT	kA
NUTS	NUTS	kA
</s>
<s>
506745	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Trnava	Trnava	k1gFnSc1
<g/>
,	,	kIx,
chrám	chrám	k1gInSc1
sv.	sv.	kA
Mikuláše	mikuláš	k1gInPc1
od	od	k7c2
severu	sever	k1gInSc2
</s>
<s>
Stará	starý	k2eAgFnSc1d1
univerzitní	univerzitní	k2eAgFnSc1d1
kolej	kolej	k1gFnSc1
a	a	k8xC
současná	současný	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
</s>
<s>
Trnava	Trnava	k1gFnSc1
<g/>
,	,	kIx,
Nová	nový	k2eAgFnSc1d1
synagoga	synagoga	k1gFnSc1
</s>
<s>
Trnava	Trnava	k1gFnSc1
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
Nagyszombat	Nagyszombat	k1gFnSc1
<g/>
,	,	kIx,
německy	německy	k6eAd1
Tyrnau	Tyrnaa	k1gFnSc4
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Tyrnavia	Tyrnavia	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
krajské	krajský	k2eAgNnSc1d1
a	a	k8xC
okresní	okresní	k2eAgNnSc1d1
město	město	k1gNnSc1
na	na	k7c6
západním	západní	k2eAgNnSc6d1
Slovensku	Slovensko	k1gNnSc6
<g/>
,	,	kIx,
44	#num#	k4
km	km	kA
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
Bratislavy	Bratislava	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
centru	centrum	k1gNnSc6
Trnavské	trnavský	k2eAgFnSc2d1
pahorkatiny	pahorkatina	k1gFnSc2
na	na	k7c6
řece	řeka	k1gFnSc6
Trnávce	Trnávka	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
výšce	výška	k1gFnSc6
146	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Trnava	Trnava	k1gFnSc1
je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc4d1
historické	historický	k2eAgNnSc4d1
a	a	k8xC
kulturní	kulturní	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
sídlem	sídlo	k1gNnSc7
dvou	dva	k4xCgFnPc2
univerzit	univerzita	k1gFnPc2
a	a	k8xC
arcibiskupství	arcibiskupství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
65	#num#	k4
713	#num#	k4
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
bylo	být	k5eAaImAgNnS
tak	tak	k9
sedmým	sedmý	k4xOgInSc7
největším	veliký	k2eAgInSc7d3
slovenským	slovenský	k2eAgInSc7d1
městem	město	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Dějiny	dějiny	k1gFnPc4
Trnavy	Trnava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Osídlení	osídlení	k1gNnSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
doloženo	doložen	k2eAgNnSc1d1
od	od	k7c2
doby	doba	k1gFnSc2
bronzové	bronzový	k2eAgNnSc1d1
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1211	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trnava	Trnava	k1gFnSc1
byla	být	k5eAaImAgFnS
prvním	první	k4xOgNnSc7
městem	město	k1gNnSc7
na	na	k7c6
území	území	k1gNnSc6
dnešního	dnešní	k2eAgNnSc2d1
Slovenska	Slovensko	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
obdrželo	obdržet	k5eAaPmAgNnS
výsady	výsada	k1gFnPc4
svobodného	svobodný	k2eAgNnSc2d1
královského	královský	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Udělil	udělit	k5eAaPmAgMnS
je	on	k3xPp3gNnSc4
uherský	uherský	k2eAgMnSc1d1
král	král	k1gMnSc1
Béla	Béla	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
v	v	k7c6
roce	rok	k1gInSc6
1238	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
bylo	být	k5eAaImAgNnS
cílem	cíl	k1gInSc7
německé	německý	k2eAgFnSc2d1
kolonizace	kolonizace	k1gFnSc2
a	a	k8xC
dostalo	dostat	k5eAaPmAgNnS
rozsáhlé	rozsáhlý	k2eAgNnSc1d1
městské	městský	k2eAgNnSc1d1
opevnění	opevnění	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
obepínalo	obepínat	k5eAaImAgNnS
plochu	plocha	k1gFnSc4
60	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1418	#num#	k4
<g/>
–	–	k?
<g/>
1425	#num#	k4
je	on	k3xPp3gMnPc4
obsadili	obsadit	k5eAaPmAgMnP
husité	husita	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
města	město	k1gNnSc2
nastal	nastat	k5eAaPmAgInS
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Moháče	Moháč	k1gInSc2
roku	rok	k1gInSc2
1526	#num#	k4
a	a	k8xC
tureckém	turecký	k2eAgNnSc6d1
obsazení	obsazení	k1gNnSc6
Ostřihomi	Ostřiho	k1gFnPc7
v	v	k7c6
roce	rok	k1gInSc6
1543	#num#	k4
se	se	k3xPyFc4
do	do	k7c2
města	město	k1gNnSc2
uchýlil	uchýlit	k5eAaPmAgMnS
ostřihomský	ostřihomský	k2eAgMnSc1d1
arcibiskup	arcibiskup	k1gMnSc1
s	s	k7c7
kapitulou	kapitula	k1gFnSc7
a	a	k8xC
sídlil	sídlit	k5eAaImAgInS
zde	zde	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1820	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trnava	Trnava	k1gFnSc1
tak	tak	k6eAd1
byla	být	k5eAaImAgFnS
po	po	k7c4
200	#num#	k4
let	léto	k1gNnPc2
církevním	církevní	k2eAgMnSc7d1
i	i	k9
kulturním	kulturní	k2eAgInSc7d1
středem	střed	k1gInSc7
Uher	Uhry	k1gFnPc2
i	i	k8xC
protireformace	protireformace	k1gFnSc2
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1561	#num#	k4
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
výstavba	výstavba	k1gFnSc1
jezuitské	jezuitský	k2eAgFnSc2d1
koleje	kolej	k1gFnSc2
<g/>
,	,	kIx,
sídla	sídlo	k1gNnSc2
původní	původní	k2eAgFnSc2d1
Trnavské	trnavský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
sousedství	sousedství	k1gNnSc6
současné	současný	k2eAgFnSc2d1
katedrály	katedrála	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
město	město	k1gNnSc1
silně	silně	k6eAd1
utrpělo	utrpět	k5eAaPmAgNnS
při	při	k7c6
šlechtických	šlechtický	k2eAgNnPc6d1
povstáních	povstání	k1gNnPc6
proti	proti	k7c3
Habsburkům	Habsburk	k1gMnPc3
(	(	kIx(
<g/>
Gabriel	Gabriel	k1gMnSc1
Betlen	Betlen	k2eAgMnSc1d1
<g/>
,	,	kIx,
Imrich	Imrich	k1gMnSc1
Thököly	Thököla	k1gFnSc2
<g/>
,	,	kIx,
František	František	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rákóczi	Rákócze	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1635	#num#	k4
zde	zde	k6eAd1
založil	založit	k5eAaPmAgMnS
arcibiskup	arcibiskup	k1gMnSc1
Péter	Péter	k1gMnSc1
Pázmány	Pázmán	k2eAgFnPc4d1
univerzitu	univerzita	k1gFnSc4
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1777	#num#	k4
přeloženou	přeložený	k2eAgFnSc4d1
do	do	k7c2
Budapešti	Budapešť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
sice	sice	k8xC
ztratilo	ztratit	k5eAaPmAgNnS
na	na	k7c6
významu	význam	k1gInSc6
<g/>
,	,	kIx,
zůstalo	zůstat	k5eAaPmAgNnS
však	však	k9
střediskem	středisko	k1gNnSc7
slovenské	slovenský	k2eAgFnSc2d1
vzdělanosti	vzdělanost	k1gFnSc2
a	a	k8xC
při	při	k7c6
první	první	k4xOgFnSc6
kodifikaci	kodifikace	k1gFnSc6
slovenštiny	slovenština	k1gFnSc2
vzal	vzít	k5eAaPmAgMnS
Anton	Anton	k1gMnSc1
Bernolák	Bernolák	k1gMnSc1
za	za	k7c4
základ	základ	k1gInSc4
trnavské	trnavský	k2eAgNnSc4d1
nářečí	nářečí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1831	#num#	k4
si	se	k3xPyFc3
měšťané	měšťan	k1gMnPc1
postavili	postavit	k5eAaPmAgMnP
první	první	k4xOgNnSc4
divadlo	divadlo	k1gNnSc4
a	a	k8xC
roku	rok	k1gInSc2
1846	#num#	k4
spojila	spojit	k5eAaPmAgFnS
Trnavu	Trnava	k1gFnSc4
s	s	k7c7
Bratislavou	Bratislava	k1gFnSc7
koněspřežná	koněspřežný	k2eAgFnSc1d1
železnice	železnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
rakousko-uherském	rakousko-uherský	k2eAgNnSc6d1
vyrovnání	vyrovnání	k1gNnSc6
roku	rok	k1gInSc2
1867	#num#	k4
zavládla	zavládnout	k5eAaPmAgFnS
silná	silný	k2eAgFnSc1d1
maďarizace	maďarizace	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
zákazu	zákaz	k1gInSc6
Matice	matice	k1gFnSc2
slovenské	slovenský	k2eAgFnSc2d1
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
Trnavě	Trnava	k1gFnSc6
Spolok	Spolok	k1gInSc1
sv.	sv.	kA
Vojtecha	Vojtech	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
dále	daleko	k6eAd2
vydával	vydávat	k5eAaImAgMnS,k5eAaPmAgMnS
slovenské	slovenský	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Trnava	Trnava	k1gFnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1978	#num#	k4
opět	opět	k6eAd1
sídlem	sídlo	k1gNnSc7
arcibiskupa	arcibiskup	k1gMnSc2
sufragánní	sufragánní	k2eAgFnSc2d1
trnavské	trnavský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
Trnavské	trnavský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
Univerzity	univerzita	k1gFnSc2
sv.	sv.	kA
Cyrila	Cyril	k1gMnSc4
a	a	k8xC
Metoděje	Metoděj	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
je	být	k5eAaImIp3nS
sídlem	sídlo	k1gNnSc7
Trnavského	trnavský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
a	a	k8xC
roku	rok	k1gInSc2
2003	#num#	k4
byla	být	k5eAaImAgFnS
východně	východně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
založena	založen	k2eAgFnSc1d1
automobilka	automobilka	k1gFnSc1
Peugeot-Citroën	Peugeot-Citroëna	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vyrábí	vyrábět	k5eAaImIp3nS
téměř	téměř	k6eAd1
300	#num#	k4
tisíc	tisíc	k4xCgInPc2
malých	malý	k2eAgInPc2d1
vozů	vůz	k1gInPc2
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severozápadně	severozápadně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
leží	ležet	k5eAaImIp3nS
také	také	k9
jaderná	jaderný	k2eAgFnSc1d1
elektrárna	elektrárna	k1gFnSc1
Jaslovské	Jaslovský	k2eAgFnPc1d1
Bohunice	Bohunice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Bazilika	bazilika	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Mikuláše	Mikuláš	k1gMnSc2
<g/>
,	,	kIx,
trojlodní	trojlodní	k2eAgFnSc1d1
gotická	gotický	k2eAgFnSc1d1
basilika	basilika	k1gFnSc1
se	s	k7c7
dvěma	dva	k4xCgFnPc7
věžemi	věž	k1gFnPc7
v	v	k7c4
průčelí	průčelí	k1gNnSc4
z	z	k7c2
let	léto	k1gNnPc2
1380	#num#	k4
<g/>
–	–	k?
<g/>
1421	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1543	#num#	k4
<g/>
–	–	k?
<g/>
1820	#num#	k4
uherská	uherský	k2eAgFnSc1d1
a	a	k8xC
v	v	k7c6
letech	let	k1gInPc6
1918	#num#	k4
<g/>
–	–	k?
<g/>
1977	#num#	k4
slovenská	slovenský	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Katedrála	katedrála	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
<g/>
,	,	kIx,
barokní	barokní	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
se	s	k7c7
dvěma	dva	k4xCgFnPc7
věžemi	věž	k1gFnPc7
z	z	k7c2
let	léto	k1gNnPc2
1629	#num#	k4
<g/>
–	–	k?
<g/>
1637	#num#	k4
navrhli	navrhnout	k5eAaPmAgMnP
italští	italský	k2eAgMnPc1d1
architekti	architekt	k1gMnPc1
Antonio	Antonio	k1gMnSc1
a	a	k8xC
Pietro	Pietro	k1gNnSc4
Spazzi	Spazze	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sálová	sálový	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
s	s	k7c7
bočními	boční	k2eAgFnPc7d1
kaplemi	kaple	k1gFnPc7
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1978	#num#	k4
katedrálou	katedrála	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Jezuitská	jezuitský	k2eAgFnSc1d1
kolej	kolej	k1gFnSc1
<g/>
,	,	kIx,
barokní	barokní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
<g/>
,	,	kIx,
sídlo	sídlo	k1gNnSc1
historické	historický	k2eAgFnSc2d1
i	i	k8xC
současné	současný	k2eAgFnSc2d1
Trnavské	trnavský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Arcibiskupský	arcibiskupský	k2eAgInSc1d1
palác	palác	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1562	#num#	k4
</s>
<s>
Městská	městský	k2eAgFnSc1d1
věž	věž	k1gFnSc1
<g/>
,	,	kIx,
renesanční	renesanční	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1574	#num#	k4
<g/>
,	,	kIx,
vysoká	vysoký	k2eAgFnSc1d1
57	#num#	k4
metrů	metr	k1gInPc2
</s>
<s>
Sloup	sloup	k1gInSc1
svaté	svatý	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
na	na	k7c6
Trojičném	trojičný	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
z	z	k7c2
roku	rok	k1gInSc2
1696	#num#	k4
</s>
<s>
Kostel	kostel	k1gInSc1
svaté	svatý	k2eAgFnSc2d1
Heleny	Helena	k1gFnSc2
ze	z	k7c2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
svaté	svatý	k2eAgFnSc2d1
Anny	Anna	k1gFnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Jakuba	Jakub	k1gMnSc2
ze	z	k7c2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
z	z	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Barokní	barokní	k2eAgInSc1d1
paulínský	paulínský	k2eAgInSc1d1
kostel	kostel	k1gInSc1
sv.	sv.	kA
Josefa	Josef	k1gMnSc2
</s>
<s>
Synagoga	synagoga	k1gFnSc1
status	status	k1gInSc1
quo	quo	k?
ante	antat	k5eAaPmIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1891	#num#	k4
</s>
<s>
Městské	městský	k2eAgNnSc1d1
opevnění	opevnění	k1gNnSc1
ze	z	k7c2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
s	s	k7c7
Bernolákovou	Bernolákův	k2eAgFnSc7d1
branou	brána	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
původních	původní	k2eAgInPc2d1
3	#num#	k4
km	km	kA
se	se	k3xPyFc4
dochovalo	dochovat	k5eAaPmAgNnS
2	#num#	k4
km	km	kA
opevnění	opevnění	k1gNnSc1
s	s	k7c7
věžemi	věž	k1gFnPc7
postavené	postavený	k2eAgFnPc1d1
z	z	k7c2
cihel	cihla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Divadlo	divadlo	k1gNnSc1
Jána	Ján	k1gMnSc2
Palárika	Palárik	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1831	#num#	k4
na	na	k7c6
Trojičném	trojičný	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
</s>
<s>
Evangelický	evangelický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1924	#num#	k4
</s>
<s>
Cholerová	cholerový	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
zvaná	zvaný	k2eAgFnSc1d1
také	také	k9
Kaple	kaple	k1gFnSc1
sv.	sv.	kA
Kříže	kříž	k1gInPc1
z	z	k7c2
roku	rok	k1gInSc2
1835	#num#	k4
</s>
<s>
Vodojem	vodojem	k1gInSc1
Trnava	Trnava	k1gFnSc1
z	z	k7c2
let	léto	k1gNnPc2
1942	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
</s>
<s>
Pomník	pomník	k1gInSc1
buditele	buditel	k1gMnSc2
Antona	Anton	k1gMnSc2
Bernoláka	Bernolák	k1gMnSc2
na	na	k7c4
Radlinského	Radlinský	k2eAgMnSc4d1
ulici	ulice	k1gFnSc6
v	v	k7c6
Bernolákových	Bernolákův	k2eAgInPc6d1
sadech	sad	k1gInPc6
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1
hřbitov	hřbitov	k1gInSc1
z	z	k7c2
První	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Pomník	pomník	k1gInSc1
Mikuláše	Mikuláš	k1gMnSc2
Schneidra	Schneidr	k1gMnSc2
Trnavského	trnavský	k2eAgMnSc2d1
</s>
<s>
Trnavské	trnavský	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
</s>
<s>
Městské	městský	k2eAgFnSc3d1
části	část	k1gFnSc3
</s>
<s>
Městské	městský	k2eAgFnSc3d1
části	část	k1gFnSc3
(	(	kIx(
<g/>
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Modranka	Modranka	k1gFnSc1
<g/>
,	,	kIx,
Trnava-juh	Trnava-juh	k1gInSc1
<g/>
,	,	kIx,
Trnava-sever	Trnava-sever	k1gInSc1
<g/>
,	,	kIx,
Trnava-stred	Trnava-stred	k1gInSc1
<g/>
,	,	kIx,
Trnava-východ	Trnava-východ	k1gInSc1
<g/>
,	,	kIx,
Trnava-západ	Trnava-západ	k1gInSc1
</s>
<s>
Sídliště	sídliště	k1gNnSc1
<g/>
:	:	kIx,
Družba	družba	k1gFnSc1
I-III	I-III	k1gFnSc2
<g/>
,	,	kIx,
Linčianska	Linčiansko	k1gNnSc2
<g/>
,	,	kIx,
Ľ.	Ľ.	k1gFnSc2
Podjavorinskej	Podjavorinskej	k1gFnSc2
<g/>
,	,	kIx,
Prednádražie	Prednádražie	k1gFnSc2
I	I	kA
<g/>
,	,	kIx,
<g/>
II	II	kA
<g/>
,	,	kIx,
Vodáreň	Vodáreň	k1gFnSc1
I	I	kA
<g/>
,	,	kIx,
<g/>
II	II	kA
<g/>
,	,	kIx,
Zátvor	zátvor	k1gInSc1
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1
části	část	k1gFnPc1
<g/>
:	:	kIx,
Hlboká	Hlboký	k2eAgFnSc1d1
<g/>
,	,	kIx,
Kopánka	Kopánka	k1gFnSc1
<g/>
,	,	kIx,
Nové	Nové	k2eAgNnSc1d1
Mesto	Mesto	k1gNnSc1
<g/>
,	,	kIx,
Staré	Staré	k2eAgNnSc1d1
mesto	mesto	k1gNnSc1
<g/>
,	,	kIx,
Tulipán	tulipán	k1gInSc1
</s>
<s>
Obce	obec	k1gFnPc1
<g/>
:	:	kIx,
Dolina	dolina	k1gFnSc1
<g/>
,	,	kIx,
Dvor	Dvor	k1gInSc1
9	#num#	k4
<g/>
.	.	kIx.
mája	máj	k1gInSc2
<g/>
,	,	kIx,
Farský	farský	k2eAgInSc4d1
mlyn	mlyn	k1gInSc4
<g/>
,	,	kIx,
Kočišské	Kočišský	k2eAgNnSc4d1
<g/>
,	,	kIx,
Medziháje	Medziháj	k1gInPc4
<g/>
,	,	kIx,
Oravné	Oravný	k2eAgInPc4d1
<g/>
,	,	kIx,
Pažitný	Pažitný	k2eAgInSc4d1
mlyn	mlyn	k1gInSc4
<g/>
,	,	kIx,
Peklo	peklo	k1gNnSc4
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Trnava	Trnava	k1gFnSc1
leží	ležet	k5eAaImIp3nS
na	na	k7c6
hlavní	hlavní	k2eAgFnSc6d1
železniční	železniční	k2eAgFnSc6d1
trati	trať	k1gFnSc6
Bratislava	Bratislava	k1gFnSc1
–	–	k?
Žilina	Žilina	k1gFnSc1
–	–	k?
Košice	Košice	k1gInPc1
a	a	k8xC
končí	končit	k5eAaImIp3nS
zde	zde	k6eAd1
lokální	lokální	k2eAgFnPc4d1
trati	trať	k1gFnPc4
do	do	k7c2
Kút	Kút	k1gFnPc2
a	a	k8xC
Seredi	Sered	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městskou	městský	k2eAgFnSc7d1
částí	část	k1gFnSc7
Modranka	Modranka	k1gFnSc1
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
města	město	k1gNnSc2
prochází	procházet	k5eAaImIp3nS
dálnice	dálnice	k1gFnSc1
D1	D1	k1gFnSc1
z	z	k7c2
Bratislavy	Bratislava	k1gFnSc2
do	do	k7c2
Piešťan	Piešťany	k1gInPc2
<g/>
,	,	kIx,
Trenčína	Trenčín	k1gInSc2
a	a	k8xC
Považské	Považský	k2eAgFnSc2d1
Bystrice	Bystrica	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Městská	městský	k2eAgFnSc1d1
autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
má	mít	k5eAaImIp3nS
13	#num#	k4
linek	linka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejbližšími	blízký	k2eAgFnPc7d3
letišti	letiště	k1gNnSc6
jsou	být	k5eAaImIp3nP
Bratislava	Bratislava	k1gFnSc1
a	a	k8xC
Piešťany	Piešťany	k1gInPc1
<g/>
.	.	kIx.
10	#num#	k4
km	km	kA
severozápadně	severozápadně	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
neveřejné	veřejný	k2eNgNnSc4d1
letiště	letiště	k1gNnSc4
Boleráz	boleráz	k1gInSc1
u	u	k7c2
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
se	s	k7c7
96,91	96,91	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
přihlásilo	přihlásit	k5eAaPmAgNnS
k	k	k7c3
slovenské	slovenský	k2eAgFnSc3d1
národnosti	národnost	k1gFnSc3
<g/>
,	,	kIx,
0,79	0,79	k4
%	%	kIx~
k	k	k7c3
české	český	k2eAgFnSc3d1
<g/>
,	,	kIx,
0,27	0,27	k4
%	%	kIx~
k	k	k7c3
romské	romský	k2eAgFnSc3d1
<g/>
,	,	kIx,
0,21	0,21	k4
%	%	kIx~
k	k	k7c3
maďarské	maďarský	k2eAgFnSc3d1
<g/>
,	,	kIx,
0,05	0,05	k4
%	%	kIx~
k	k	k7c3
německé	německý	k2eAgFnSc3d1
a	a	k8xC
0,03	0,03	k4
%	%	kIx~
k	k	k7c3
ukrajinské	ukrajinský	k2eAgFnSc2d1
<g/>
.	.	kIx.
71,85	71,85	k4
%	%	kIx~
bylo	být	k5eAaImAgNnS
římských	římský	k2eAgMnPc2d1
katolíků	katolík	k1gMnPc2
<g/>
,	,	kIx,
2,93	2,93	k4
%	%	kIx~
protestantů	protestant	k1gMnPc2
<g/>
,	,	kIx,
0,2	0,2	k4
%	%	kIx~
řeckokatolíků	řeckokatolík	k1gMnPc2
a	a	k8xC
0,11	0,11	k4
%	%	kIx~
pravoslavných	pravoslavný	k2eAgInPc2d1
<g/>
.	.	kIx.
18,37	18,37	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
bylo	být	k5eAaImAgNnS
bez	bez	k7c2
vyznání	vyznání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
I.	I.	kA
Veliký	veliký	k2eAgMnSc1d1
(	(	kIx(
<g/>
1326	#num#	k4
<g/>
–	–	k?
<g/>
1382	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
uherský	uherský	k2eAgMnSc1d1
a	a	k8xC
polský	polský	k2eAgMnSc1d1
král	král	k1gMnSc1
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1
Oláh	Oláh	k1gMnSc1
(	(	kIx(
<g/>
1493	#num#	k4
<g/>
–	–	k?
<g/>
1568	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
uherský	uherský	k2eAgMnSc1d1
humanista	humanista	k1gMnSc1
</s>
<s>
Johannes	Johannes	k1gMnSc1
Sambucus	Sambucus	k1gMnSc1
(	(	kIx(
<g/>
1531	#num#	k4
<g/>
–	–	k?
<g/>
1584	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
básník	básník	k1gMnSc1
<g/>
,	,	kIx,
polyhistor	polyhistor	k1gMnSc1
<g/>
,	,	kIx,
lékař	lékař	k1gMnSc1
</s>
<s>
Péter	Péter	k1gInSc4
Pázmány	Pázmán	k2eAgFnPc1d1
(	(	kIx(
<g/>
1570	#num#	k4
<g/>
–	–	k?
<g/>
1637	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
maďarský	maďarský	k2eAgMnSc1d1
filosof	filosof	k1gMnSc1
<g/>
,	,	kIx,
teolog	teolog	k1gMnSc1
a	a	k8xC
kardinál	kardinál	k1gMnSc1
</s>
<s>
Jozef	Jozef	k1gMnSc1
Ignác	Ignác	k1gMnSc1
Bajza	Bajza	k1gMnSc1
(	(	kIx(
<g/>
1755	#num#	k4
<g/>
–	–	k?
<g/>
1836	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
katolický	katolický	k2eAgMnSc1d1
farář	farář	k1gMnSc1
a	a	k8xC
spisovatel	spisovatel	k1gMnSc1
</s>
<s>
Anton	Anton	k1gMnSc1
Bernolák	Bernolák	k1gMnSc1
(	(	kIx(
<g/>
1762	#num#	k4
<g/>
–	–	k?
<g/>
1813	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
římskokatolický	římskokatolický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
a	a	k8xC
jazykovědec	jazykovědec	k1gMnSc1
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
kodifikátor	kodifikátor	k1gMnSc1
spisovní	spisovní	k2eAgFnSc2d1
slovenštiny	slovenština	k1gFnSc2
</s>
<s>
Šimon	Šimon	k1gMnSc1
Sidon	Sidon	k1gMnSc1
(	(	kIx(
<g/>
1815	#num#	k4
<g/>
–	–	k?
<g/>
1891	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
trnavský	trnavský	k2eAgMnSc1d1
rabín	rabín	k1gMnSc1
</s>
<s>
Svetozár	Svetozár	k1gMnSc1
Hurban-Vajanský	Hurban-Vajanský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1847	#num#	k4
<g/>
–	–	k?
<g/>
1916	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
</s>
<s>
Ferko	Ferko	k1gNnSc1
Urbánek	Urbánek	k1gMnSc1
(	(	kIx(
<g/>
1859	#num#	k4
<g/>
–	–	k?
<g/>
1934	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
dramatik	dramatik	k1gMnSc1
<g/>
,	,	kIx,
novinář	novinář	k1gMnSc1
<g/>
,	,	kIx,
úředník	úředník	k1gMnSc1
a	a	k8xC
podnikatel	podnikatel	k1gMnSc1
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1
Schneider-Trnavský	Schneider-Trnavský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1881	#num#	k4
<g/>
–	–	k?
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
slovenský	slovenský	k2eAgMnSc1d1
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
dirigent	dirigent	k1gMnSc1
a	a	k8xC
hudební	hudební	k2eAgMnSc1d1
pedagog	pedagog	k1gMnSc1
</s>
<s>
Zoltán	Zoltán	k1gMnSc1
Kodály	Kodála	k1gFnSc2
(	(	kIx(
<g/>
1882	#num#	k4
<g/>
–	–	k?
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
maďarský	maďarský	k2eAgMnSc1d1
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
a	a	k8xC
pedagog	pedagog	k1gMnSc1
</s>
<s>
Martin	Martin	k1gMnSc1
Gregor	Gregor	k1gMnSc1
(	(	kIx(
<g/>
1907	#num#	k4
<g/>
–	–	k?
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
herec	herec	k1gMnSc1
</s>
<s>
Anton	Anton	k1gMnSc1
Malatinský	Malatinský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
slovenský	slovenský	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
a	a	k8xC
trenér	trenér	k1gMnSc1
</s>
<s>
Gerta	Gert	k1gMnSc4
Vrbová	Vrbová	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1926	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
neuroložka	neuroložka	k1gFnSc1
<g/>
,	,	kIx,
profesorka	profesorka	k1gFnSc1
<g/>
,	,	kIx,
spisovatelka	spisovatelka	k1gFnSc1
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Válek	Válek	k1gMnSc1
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
básník	básník	k1gMnSc1
<g/>
,	,	kIx,
publicista	publicista	k1gMnSc1
<g/>
,	,	kIx,
překladatel	překladatel	k1gMnSc1
<g/>
,	,	kIx,
kulturní	kulturní	k2eAgMnSc1d1
organizátor	organizátor	k1gMnSc1
a	a	k8xC
politik	politik	k1gMnSc1
</s>
<s>
Imrich	Imrich	k1gMnSc1
Stacho	Stacha	k1gFnSc5
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
slovenský	slovenský	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
</s>
<s>
Matúš	Matúš	k1gMnSc1
Kučera	Kučera	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1932	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
historik	historik	k1gMnSc1
a	a	k8xC
profesor	profesor	k1gMnSc1
</s>
<s>
Jozef	Jozef	k1gMnSc1
Adamovič	Adamovič	k1gMnSc1
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
herec	herec	k1gMnSc1
</s>
<s>
Juraj	Juraj	k1gMnSc1
Beneš	Beneš	k1gMnSc1
(	(	kIx(
<g/>
1940	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
a	a	k8xC
klavírista	klavírista	k1gMnSc1
</s>
<s>
Eva	Eva	k1gFnSc1
Kostolányiová	Kostolányiový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1942	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zpěvačka	zpěvačka	k1gFnSc1
</s>
<s>
Karol	Karol	k1gInSc1
Čálik	Čálik	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
herec	herec	k1gMnSc1
a	a	k8xC
zpěvák	zpěvák	k1gMnSc1
(	(	kIx(
<g/>
tenor	tenor	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Soňa	Soňa	k1gFnSc1
Valentová	Valentová	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1946	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
divadelní	divadelní	k2eAgFnSc1d1
a	a	k8xC
filmová	filmový	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
</s>
<s>
Viliam	Viliam	k1gMnSc1
Sivek	sivka	k1gFnPc2
<g/>
,	,	kIx,
(	(	kIx(
<g/>
*	*	kIx~
1946	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
český	český	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
</s>
<s>
Karol	Karol	k1gInSc1
Ježík	ježík	k1gMnSc1
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
publicista	publicista	k1gMnSc1
</s>
<s>
Jozef	Jozef	k1gMnSc1
Heriban	Heriban	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1953	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
scenárista	scenárista	k1gMnSc1
</s>
<s>
Tibor	Tibor	k1gMnSc1
Huszár	Huszár	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1953	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
fotograf	fotograf	k1gMnSc1
a	a	k8xC
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
pedagog	pedagog	k1gMnSc1
</s>
<s>
Blažej	Blažej	k1gMnSc1
Baláž	Baláž	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1958	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
malíř	malíř	k1gMnSc1
a	a	k8xC
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
pedagog	pedagog	k1gMnSc1
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Zvolenský	zvolenský	k2eAgMnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
1958	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
arcibiskup	arcibiskup	k1gMnSc1
bratislavský	bratislavský	k2eAgMnSc1d1
a	a	k8xC
metropolita	metropolita	k1gMnSc1
západoslovenský	západoslovenský	k2eAgMnSc1d1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
Konference	konference	k1gFnSc2
biskupů	biskup	k1gMnPc2
Slovenska	Slovensko	k1gNnSc2
</s>
<s>
Róbert	Róbert	k1gMnSc1
Bezák	Bezák	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1960	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
římskokatolický	římskokatolický	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
</s>
<s>
Michaela	Michaela	k1gFnSc1
Čobejová	Čobejový	k2eAgFnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
herečka	herečka	k1gFnSc1
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Karhan	karhan	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
1976	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
fotbalový	fotbalový	k2eAgMnSc1d1
reprezentant	reprezentant	k1gMnSc1
</s>
<s>
Libor	Libor	k1gMnSc1
Charfreitag	Charfreitag	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kladivář	kladivář	k1gMnSc1
</s>
<s>
Taťána	Taťána	k1gFnSc1
Kuchařová	Kuchařová	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1987	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
česká	český	k2eAgFnSc1d1
modelka	modelka	k1gFnSc1
<g/>
,	,	kIx,
Miss	miss	k1gFnSc1
World	World	k1gInSc1
2006	#num#	k4
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
Jedlička	Jedlička	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1985	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
slovenský	slovenský	k2eAgMnSc1d1
rapper	rapper	k1gMnSc1
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Balakovo	Balakův	k2eAgNnSc4d1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc4
</s>
<s>
Břeclav	Břeclav	k1gFnSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
</s>
<s>
Chomutov	Chomutov	k1gInSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
</s>
<s>
Casale	Casale	k6eAd1
Monferrato	Monferrat	k2eAgNnSc1d1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
</s>
<s>
Novo	nova	k1gFnSc5
mesto	mesta	k1gFnSc5
<g/>
,	,	kIx,
Slovinsko	Slovinsko	k1gNnSc4
</s>
<s>
Sangerhausen	Sangerhausen	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
Scranton	Scranton	k1gInSc1
<g/>
,	,	kIx,
Pensylvánie	Pensylvánie	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
Szombathely	Szombathel	k1gInPc1
<g/>
,	,	kIx,
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Varaždín	Varaždín	k1gInSc1
<g/>
,	,	kIx,
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Zabrze	Zabrze	k1gFnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Mikuláše	Mikuláš	k1gMnSc2
</s>
<s>
Katedrála	katedrála	k1gFnSc1
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
</s>
<s>
Katedrála	katedrála	k1gFnSc1
<g/>
,	,	kIx,
vnitřek	vnitřek	k1gInSc1
</s>
<s>
Radnice	radnice	k1gFnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Josefa	Josef	k1gMnSc2
</s>
<s>
Kostelík	kostelík	k1gInSc1
sv.	sv.	kA
Heleny	Helena	k1gFnSc2
</s>
<s>
Evangelický	evangelický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
SR	SR	kA
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
.	.	kIx.
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KOLEKTIV	kolektivum	k1gNnPc2
AUTORŮ	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc1
Czechoslovakia	Czechoslovakia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
STN	STN	kA
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
303	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Trnava	Trnava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
25	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
775	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Arcidiecéze	arcidiecéze	k1gFnSc1
trnavská	trnavský	k2eAgFnSc1d1
</s>
<s>
Arcidiecéze	arcidiecéze	k1gFnSc1
bratislavsko-trnavská	bratislavsko-trnavský	k2eAgFnSc1d1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Trnava	Trnava	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Trnava	Trnava	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Neoficiální	neoficiální	k2eAgFnPc4d1,k2eNgFnPc4d1
stránky	stránka	k1gFnPc4
města	město	k1gNnSc2
</s>
<s>
Jízdní	jízdní	k2eAgInPc4d1
řády	řád	k1gInPc4
a	a	k8xC
jiné	jiný	k2eAgFnPc4d1
informace	informace	k1gFnPc4
o	o	k7c6
MHD	MHD	kA
v	v	k7c6
Trnavě	Trnava	k1gFnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Města	město	k1gNnSc2
a	a	k8xC
obce	obec	k1gFnSc2
okresu	okres	k1gInSc2
Trnava	Trnava	k1gFnSc1
</s>
<s>
Biely	Biel	k1gInPc1
Kostol	Kostola	k1gFnPc2
•	•	k?
Bíňovce	Bíňovec	k1gInSc2
•	•	k?
Bohdanovce	Bohdanovka	k1gFnSc6
nad	nad	k7c7
Trnavou	Trnava	k1gFnSc7
•	•	k?
Boleráz	boleráz	k1gInSc4
•	•	k?
Borová	borový	k2eAgFnSc1d1
•	•	k?
Brestovany	Brestovan	k1gMnPc4
•	•	k?
Bučany	Bučana	k1gFnSc2
•	•	k?
Buková	bukový	k2eAgFnSc1d1
•	•	k?
Cífer	Cífero	k1gNnPc2
•	•	k?
Dechtice	Dechtice	k1gFnSc2
•	•	k?
Dlhá	Dlhá	k1gFnSc1
•	•	k?
Dobrá	dobrý	k2eAgFnSc1d1
Voda	voda	k1gFnSc1
•	•	k?
Dolná	Dolná	k1gFnSc1
Krupá	Krupý	k2eAgFnSc1d1
•	•	k?
Dolné	Dolná	k1gFnSc3
Dubové	Dubové	k2eAgInPc2d1
•	•	k?
Dolné	Dolný	k2eAgFnSc2d1
Lovčice	Lovčice	k1gFnSc2
•	•	k?
Dolné	Dolný	k2eAgFnSc2d1
Orešany	Orešana	k1gFnSc2
•	•	k?
Horná	Horná	k1gFnSc1
Krupá	Krupý	k2eAgFnSc1d1
•	•	k?
Horné	horný	k1gMnPc4
Dubové	Dubová	k1gFnSc2
•	•	k?
Horné	horný	k1gMnPc4
Orešany	Orešana	k1gFnSc2
•	•	k?
Hrnčiarovce	Hrnčiarovka	k1gFnSc6
nad	nad	k7c7
<g />
.	.	kIx.
</s>
<s hack="1">
Parnou	parný	k2eAgFnSc4d1
•	•	k?
Jaslovské	Jaslovský	k2eAgFnPc4d1
Bohunice	Bohunice	k1gFnPc4
•	•	k?
Kátlovce	Kátlovka	k1gFnSc6
•	•	k?
Košolná	Košolný	k2eAgFnSc1d1
•	•	k?
Križovany	Križovan	k1gMnPc4
nad	nad	k7c4
Dudváhom	Dudváhom	k1gInSc4
•	•	k?
Lošonec	Lošonec	k1gInSc1
•	•	k?
Majcichov	Majcichov	k1gInSc1
•	•	k?
Malženice	Malženice	k1gFnSc2
•	•	k?
Naháč	naháč	k1gMnSc1
•	•	k?
Opoj	opojit	k5eAaPmRp2nS
•	•	k?
Pavlice	Pavlika	k1gFnSc3
•	•	k?
Radošovce	Radošovec	k1gInSc2
•	•	k?
Ružindol	Ružindol	k1gInSc1
•	•	k?
Slovenská	slovenský	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Smolenice	Smolenica	k1gFnSc2
•	•	k?
Suchá	Suchá	k1gFnSc1
nad	nad	k7c4
Parnou	parný	k2eAgFnSc4d1
•	•	k?
Šelpice	Šelpice	k1gFnSc1
•	•	k?
Špačince	Špačinec	k1gInPc1
•	•	k?
Šúrovce	Šúrovec	k1gInSc2
•	•	k?
Trnava	Trnava	k1gFnSc1
•	•	k?
Trstín	Trstín	k1gInSc1
•	•	k?
Vlčkovce	Vlčkovec	k1gInSc2
•	•	k?
Voderady	Voderada	k1gFnSc2
•	•	k?
Zavar	Zavar	k1gMnSc1
•	•	k?
Zeleneč	Zeleneč	k1gMnSc1
•	•	k?
Zvončín	Zvončín	k1gMnSc1
</s>
<s>
Památkové	památkový	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
Městské	městský	k2eAgFnSc2d1
památkové	památkový	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Banská	banský	k2eAgFnSc1d1
Štiavnica	Štiavnica	k1gFnSc1
•	•	k?
Bardejov	Bardejov	k1gInSc1
•	•	k?
Bratislava	Bratislava	k1gFnSc1
•	•	k?
Kežmarok	Kežmarok	k1gInSc4
•	•	k?
Košice	Košice	k1gInPc4
•	•	k?
Kremnica	Kremnica	k1gFnSc1
•	•	k?
Levoča	Levoča	k1gFnSc1
•	•	k?
Nitra	Nitra	k1gFnSc1
•	•	k?
Podolínec	Podolínec	k1gInSc1
•	•	k?
Prešov	Prešov	k1gInSc1
•	•	k?
Spišská	spišský	k2eAgFnSc1d1
Kapitula	kapitula	k1gFnSc1
•	•	k?
Spišská	spišský	k2eAgFnSc1d1
Sobota	sobota	k1gFnSc1
•	•	k?
Svätý	Svätý	k2eAgInSc1d1
Jur	jura	k1gFnPc2
•	•	k?
Trenčín	Trenčín	k1gInSc1
•	•	k?
Trnava	Trnava	k1gFnSc1
•	•	k?
Štiavnické	Štiavnický	k2eAgFnSc3d1
Bane	Bane	k1gFnSc3
•	•	k?
Žilina	Žilina	k1gFnSc1
Památkové	památkový	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
lidové	lidový	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
</s>
<s>
Brhlovce	Brhlovka	k1gFnSc3
•	•	k?
Čičmany	Čičman	k1gInPc1
•	•	k?
Osturňa	Osturň	k1gInSc2
•	•	k?
Plavecký	plavecký	k2eAgMnSc1d1
Peter	Peter	k1gMnSc1
•	•	k?
Podbiel	Podbiel	k1gMnSc1
•	•	k?
Sebechleby	Sebechleba	k1gFnSc2
•	•	k?
Veľké	Veľká	k1gFnSc2
Leváre	Levár	k1gInSc5
•	•	k?
Vlkolínec	Vlkolínec	k1gInSc1
•	•	k?
Špania	Španium	k1gNnSc2
Dolina	dolina	k1gFnSc1
•	•	k?
Ždiar	Ždiar	k1gInSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
–	–	k?
(	(	kIx(
<g/>
SK	Sk	kA
<g/>
)	)	kIx)
Kraje	kraj	k1gInPc1
a	a	k8xC
jejich	jejich	k3xOp3gNnPc1
správní	správní	k2eAgNnPc1d1
centra	centrum	k1gNnPc1
</s>
<s>
Banskobystrický	banskobystrický	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Bratislavský	bratislavský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Bratislava	Bratislava	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Košický	košický	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Košice	Košice	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Nitranský	nitranský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Nitra	Nitra	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prešovský	prešovský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Prešov	Prešov	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Trenčínský	trenčínský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Trenčín	Trenčín	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Trnavský	trnavský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Trnava	Trnava	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Žilinský	žilinský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Žilina	Žilina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Města	město	k1gNnPc1
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
podle	podle	k7c2
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnSc1
200	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
•	•	k?
Košice	Košice	k1gInPc1
50	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Prešov	Prešov	k1gInSc1
•	•	k?
Žilina	Žilina	k1gFnSc1
•	•	k?
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
•	•	k?
Nitra	Nitra	k1gFnSc1
•	•	k?
Trnava	Trnava	k1gFnSc1
•	•	k?
Trenčín	Trenčín	k1gInSc1
•	•	k?
Martin	Martin	k1gMnSc1
•	•	k?
Poprad	Poprad	k1gInSc1
30	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Prievidza	Prievidza	k1gFnSc1
•	•	k?
Zvolen	Zvolen	k1gInSc1
•	•	k?
Považská	Považský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
•	•	k?
Michalovce	Michalovce	k1gInPc1
•	•	k?
Nové	Nové	k2eAgInPc1d1
Zámky	zámek	k1gInPc1
•	•	k?
Spišská	spišský	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Komárno	Komárno	k1gNnSc1
•	•	k?
Humenné	Humenné	k1gNnSc1
•	•	k?
Levice	levice	k1gFnSc1
•	•	k?
Bardejov	Bardejov	k1gInSc4
•	•	k?
Liptovský	liptovský	k2eAgMnSc1d1
Mikuláš	Mikuláš	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
131124	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4078490-3	4078490-3	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2001113300	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
152635979	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2001113300	#num#	k4
</s>
