<s>
Lhoce	Lhoce	k1gFnSc1	Lhoce
(	(	kIx(	(
<g/>
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
oficiálně	oficiálně	k6eAd1	oficiálně
Lhozê	Lhozê	k1gFnSc4	Lhozê
<g/>
,	,	kIx,	,
tibetsky	tibetsky	k6eAd1	tibetsky
lho	lho	k?	lho
rtse	rtse	k1gInSc1	rtse
<g/>
;	;	kIx,	;
čínsky	čínsky	k6eAd1	čínsky
洛	洛	k?	洛
<g/>
;	;	kIx,	;
pinyinm	pinyinm	k1gMnSc1	pinyinm
Luò	Luò	k1gMnSc1	Luò
Fē	Fē	k1gMnSc1	Fē
<g/>
,	,	kIx,	,
též	též	k9	též
Lhotse	Lhotse	k1gFnSc1	Lhotse
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Himálaji	Himálaj	k1gFnSc6	Himálaj
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Nepálu	Nepál	k1gInSc2	Nepál
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
(	(	kIx(	(
<g/>
Tibetu	Tibet	k1gInSc2	Tibet
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
Mount	Mount	k1gInSc1	Mount
Everestem	Everest	k1gInSc7	Everest
<g/>
.	.	kIx.	.
</s>
