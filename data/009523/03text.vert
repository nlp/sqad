<p>
<s>
Lhoce	Lhoce	k1gFnSc1	Lhoce
(	(	kIx(	(
<g/>
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
oficiálně	oficiálně	k6eAd1	oficiálně
Lhozê	Lhozê	k1gFnSc4	Lhozê
<g/>
,	,	kIx,	,
tibetsky	tibetsky	k6eAd1	tibetsky
lho	lho	k?	lho
rtse	rtse	k1gInSc1	rtse
<g/>
;	;	kIx,	;
čínsky	čínsky	k6eAd1	čínsky
洛	洛	k?	洛
<g/>
;	;	kIx,	;
pinyinm	pinyinm	k1gMnSc1	pinyinm
Luò	Luò	k1gMnSc1	Luò
Fē	Fē	k1gMnSc1	Fē
<g/>
,	,	kIx,	,
též	též	k9	též
Lhotse	Lhotse	k1gFnSc1	Lhotse
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Himálaji	Himálaj	k1gFnSc6	Himálaj
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Nepálu	Nepál	k1gInSc2	Nepál
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
(	(	kIx(	(
<g/>
Tibetu	Tibet	k1gInSc2	Tibet
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
Mount	Mount	k1gInSc1	Mount
Everestem	Everest	k1gInSc7	Everest
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavní	hlavní	k2eAgInSc1d1	hlavní
vrchol	vrchol	k1gInSc1	vrchol
má	mít	k5eAaImIp3nS	mít
8516	[number]	k4	8516
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
Lhoce	Lhoce	k1gFnSc1	Lhoce
Middle	Middle	k1gFnSc1	Middle
8414	[number]	k4	8414
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
Lhoce	Lhoec	k1gInSc2	Lhoec
Šar	Šar	k1gFnSc2	Šar
8383	[number]	k4	8383
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Lhoce	Lhoce	k1gFnSc1	Lhoce
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
blízkosti	blízkost	k1gFnSc3	blízkost
k	k	k7c3	k
Mount	Mounta	k1gFnPc2	Mounta
Everestu	Everest	k1gInSc2	Everest
a	a	k8xC	a
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
horolezci	horolezec	k1gMnPc1	horolezec
stoupající	stoupající	k2eAgFnSc7d1	stoupající
tradiční	tradiční	k2eAgFnSc7d1	tradiční
cestou	cesta	k1gFnSc7	cesta
stráví	strávit	k5eAaPmIp3nS	strávit
nějaký	nějaký	k3yIgInSc4	nějaký
ten	ten	k3xDgInSc4	ten
čas	čas	k1gInSc4	čas
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
stěně	stěna	k1gFnSc6	stěna
Lhoce	Lhoec	k1gInSc2	Lhoec
<g/>
.	.	kIx.	.
</s>
<s>
Hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
dramatická	dramatický	k2eAgFnSc1d1	dramatická
především	především	k9	především
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
spadá	spadat	k5eAaPmIp3nS	spadat
svou	svůj	k3xOyFgFnSc4	svůj
obří	obří	k2eAgFnSc4d1	obří
3,2	[number]	k4	3,2
<g/>
kilometrovou	kilometrový	k2eAgFnSc7d1	kilometrová
stěnou	stěna	k1gFnSc7	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejstrmější	strmý	k2eAgFnSc4d3	nejstrmější
stěnu	stěna	k1gFnSc4	stěna
takovéto	takovýto	k3xDgFnSc2	takovýto
velikosti	velikost	k1gFnSc2	velikost
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
stěna	stěna	k1gFnSc1	stěna
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
svědkem	svědek	k1gMnSc7	svědek
mnoha	mnoho	k4c2	mnoho
marných	marný	k2eAgInPc2d1	marný
pokusů	pokus	k1gInPc2	pokus
o	o	k7c6	o
zlezení	zlezení	k1gNnSc6	zlezení
a	a	k8xC	a
několika	několik	k4yIc2	několik
tragických	tragický	k2eAgFnPc2d1	tragická
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Stěnou	stěna	k1gFnSc7	stěna
prostoupilo	prostoupit	k5eAaPmAgNnS	prostoupit
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
lezců	lezec	k1gMnPc2	lezec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Tomo	Tomo	k1gMnSc1	Tomo
Česen	česno	k1gNnPc2	česno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výstupy	výstup	k1gInPc4	výstup
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
výstup	výstup	k1gInSc4	výstup
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
útok	útok	k1gInSc1	útok
na	na	k7c6	na
Lhoce	Lhoka	k1gFnSc6	Lhoka
byl	být	k5eAaImAgInS	být
veden	vést	k5eAaImNgInS	vést
r.	r.	kA	r.
1955	[number]	k4	1955
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
himálajskou	himálajský	k2eAgFnSc7d1	himálajská
expedicí	expedice	k1gFnSc7	expedice
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Normanem	Norman	k1gMnSc7	Norman
Dyhrenfurthem	Dyhrenfurth	k1gInSc7	Dyhrenfurth
<g/>
.	.	kIx.	.
</s>
<s>
Účastnili	účastnit	k5eAaImAgMnP	účastnit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
také	také	k9	také
dva	dva	k4xCgMnPc1	dva
Rakušané	Rakušan	k1gMnPc1	Rakušan
(	(	kIx(	(
<g/>
kartograf	kartograf	k1gMnSc1	kartograf
Erwin	Erwin	k1gMnSc1	Erwin
Schneider	Schneider	k1gMnSc1	Schneider
a	a	k8xC	a
Ernst	Ernst	k1gMnSc1	Ernst
Senn	Senn	k1gMnSc1	Senn
<g/>
)	)	kIx)	)
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
Švýcaři	Švýcar	k1gMnPc1	Švýcar
(	(	kIx(	(
<g/>
Bruno	Bruno	k1gMnSc1	Bruno
Spirig	Spirig	k1gMnSc1	Spirig
a	a	k8xC	a
Artur	Artur	k1gMnSc1	Artur
Spöhel	Spöhel	k1gMnSc1	Spöhel
<g/>
)	)	kIx)	)
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
také	také	k9	také
zároveň	zároveň	k6eAd1	zároveň
první	první	k4xOgInSc4	první
expedicí	expedice	k1gFnPc2	expedice
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Everestu	Everest	k1gInSc2	Everest
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
Američané	Američan	k1gMnPc1	Američan
jako	jako	k8xC	jako
členové	člen	k1gMnPc1	člen
výpravy	výprava	k1gFnSc2	výprava
(	(	kIx(	(
<g/>
Fred	Fred	k1gMnSc1	Fred
Beckey	Beckea	k1gMnSc2	Beckea
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
Bell	bell	k1gInSc4	bell
<g/>
,	,	kIx,	,
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
McGowan	McGowan	k1gMnSc1	McGowan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nepálský	nepálský	k2eAgMnSc1d1	nepálský
styčný	styčný	k2eAgMnSc1d1	styčný
důstojník	důstojník	k1gMnSc1	důstojník
byl	být	k5eAaImAgMnS	být
Gaya	Gaya	k?	Gaya
Nanda	Nanda	k1gFnSc1	Nanda
Vaidya	Vaidya	k1gFnSc1	Vaidya
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
doprovázeni	doprovázen	k2eAgMnPc1d1	doprovázen
asi	asi	k9	asi
200	[number]	k4	200
místními	místní	k2eAgInPc7d1	místní
údolními	údolní	k2eAgInPc7d1	údolní
šerpskými	šerpský	k2eAgInPc7d1	šerpský
nosiči	nosič	k1gInPc7	nosič
a	a	k8xC	a
několika	několik	k4yIc7	několik
výškovými	výškový	k2eAgInPc7d1	výškový
nosiči	nosič	k1gInPc7	nosič
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
nebezpečný	bezpečný	k2eNgInSc4d1	nebezpečný
přístup	přístup	k1gInSc4	přístup
pod	pod	k7c4	pod
jižní	jižní	k2eAgFnSc4d1	jižní
stěnu	stěna	k1gFnSc4	stěna
Lhoce	Lhoce	k1gMnSc2	Lhoce
Šar	Šar	k1gMnSc2	Šar
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
neskutečnou	skutečný	k2eNgFnSc4d1	neskutečná
strmost	strmost	k1gFnSc4	strmost
změnili	změnit	k5eAaPmAgMnP	změnit
taktiku	taktika	k1gFnSc4	taktika
a	a	k8xC	a
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
se	se	k3xPyFc4	se
během	během	k7c2	během
září	září	k1gNnSc2	září
a	a	k8xC	a
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
stěny	stěna	k1gFnSc2	stěna
Lhoce	Lhoce	k1gFnSc2	Lhoce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
výšky	výška	k1gFnSc2	výška
8100	[number]	k4	8100
m.	m.	k?	m.
Expedice	expedice	k1gFnSc1	expedice
však	však	k9	však
byla	být	k5eAaImAgFnS	být
zastavena	zastavit	k5eAaPmNgFnS	zastavit
silným	silný	k2eAgInSc7d1	silný
větrem	vítr	k1gInSc7	vítr
a	a	k8xC	a
extrémně	extrémně	k6eAd1	extrémně
nízkými	nízký	k2eAgFnPc7d1	nízká
teplotami	teplota	k1gFnPc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Schneiderovým	Schneiderův	k2eAgNnSc7d1	Schneiderovo
vedením	vedení	k1gNnSc7	vedení
dokončili	dokončit	k5eAaPmAgMnP	dokončit
první	první	k4xOgFnSc4	první
mapu	mapa	k1gFnSc4	mapa
oblasti	oblast	k1gFnSc2	oblast
Everestu	Everest	k1gInSc2	Everest
v	v	k7c6	v
měřítku	měřítko	k1gNnSc6	měřítko
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
také	také	k9	také
stihla	stihnout	k5eAaPmAgFnS	stihnout
natočit	natočit	k5eAaBmF	natočit
několik	několik	k4yIc4	několik
krátkometrážních	krátkometrážní	k2eAgInPc2d1	krátkometrážní
filmů	film	k1gInPc2	film
o	o	k7c6	o
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
místní	místní	k2eAgFnSc6d1	místní
kultuře	kultura	k1gFnSc6	kultura
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
povedlo	povést	k5eAaPmAgNnS	povést
několik	několik	k4yIc4	několik
prvovýstupů	prvovýstup	k1gInPc2	prvovýstup
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
regionu	region	k1gInSc2	region
Khumbu	Khumb	k1gInSc2	Khumb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Prvovýstup	prvovýstup	k1gInSc4	prvovýstup
===	===	k?	===
</s>
</p>
<p>
<s>
Prvovýstup	prvovýstup	k1gInSc1	prvovýstup
se	se	k3xPyFc4	se
zdařil	zdařit	k5eAaPmAgInS	zdařit
následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
švýcarské	švýcarský	k2eAgFnSc6d1	švýcarská
expedici	expedice	k1gFnSc6	expedice
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vedl	vést	k5eAaImAgMnS	vést
Albert	Albert	k1gMnSc1	Albert
Eggler	Eggler	k1gMnSc1	Eggler
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1956	[number]	k4	1956
Luchsigner	Luchsignra	k1gFnPc2	Luchsignra
a	a	k8xC	a
Reiss	Reissa	k1gFnPc2	Reissa
<g/>
.	.	kIx.	.
</s>
<s>
Husarským	husarský	k2eAgInSc7d1	husarský
kouskem	kousek	k1gInSc7	kousek
této	tento	k3xDgFnSc2	tento
expedice	expedice	k1gFnSc2	expedice
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
přesunu	přesun	k1gInSc6	přesun
tábora	tábor	k1gInSc2	tábor
na	na	k7c4	na
Jižní	jižní	k2eAgNnSc4d1	jižní
sedlo	sedlo	k1gNnSc4	sedlo
(	(	kIx(	(
<g/>
a	a	k8xC	a
vybudování	vybudování	k1gNnSc4	vybudování
posledního	poslední	k2eAgInSc2d1	poslední
tábora	tábor	k1gInSc2	tábor
na	na	k7c6	na
hřebenu	hřeben	k1gInSc6	hřeben
<g/>
)	)	kIx)	)
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
dvojice	dvojice	k1gFnSc1	dvojice
Marmet	Marmet	k1gInSc1	Marmet
a	a	k8xC	a
Schmied	Schmied	k1gInSc1	Schmied
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Everestu	Everest	k1gInSc2	Everest
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
den	den	k1gInSc1	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Čhomolunngmy	Čhomolunngma	k1gFnSc2	Čhomolunngma
Gunten	Gunten	k2eAgInSc4d1	Gunten
a	a	k8xC	a
Reist	Reist	k1gInSc4	Reist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
dalších	další	k2eAgInPc2d1	další
výstupů	výstup	k1gInPc2	výstup
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
243	[number]	k4	243
horolezců	horolezec	k1gMnPc2	horolezec
Lhoce	Lhoce	k1gFnSc2	Lhoce
zdolalo	zdolat	k5eAaPmAgNnS	zdolat
a	a	k8xC	a
11	[number]	k4	11
zde	zde	k6eAd1	zde
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkem	celkem	k6eAd1	celkem
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
Lhoce	Lhoce	k1gFnSc2	Lhoce
jen	jen	k9	jen
2	[number]	k4	2
cesty	cesta	k1gFnSc2	cesta
(	(	kIx(	(
<g/>
3	[number]	k4	3
v	v	k7c6	v
případě	případ	k1gInSc6	případ
započítání	započítání	k1gNnSc2	započítání
sporného	sporný	k2eAgInSc2d1	sporný
výstupu	výstup	k1gInSc2	výstup
Tomo	Tomo	k6eAd1	Tomo
Česena	Česen	k2eAgFnSc1d1	Česen
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1	[number]	k4	1
cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Lhoce	Lhoec	k1gInPc4	Lhoec
Middle	Middle	k1gMnPc2	Middle
a	a	k8xC	a
2	[number]	k4	2
cesty	cesta	k1gFnPc4	cesta
na	na	k7c4	na
Lhoce	Lhoce	k1gMnPc4	Lhoce
Šar	Šar	k1gFnSc2	Šar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1955	[number]	k4	1955
–	–	k?	–
první	první	k4xOgInSc4	první
oficiální	oficiální	k2eAgInSc4d1	oficiální
pokus	pokus	k1gInSc4	pokus
expedicí	expedice	k1gFnPc2	expedice
International	International	k1gMnSc1	International
Himalayan	Himalayan	k1gMnSc1	Himalayan
Expedition	Expedition	k1gInSc4	Expedition
v	v	k7c6	v
Z	Z	kA	Z
stěně	stěna	k1gFnSc6	stěna
sleduje	sledovat	k5eAaImIp3nS	sledovat
klasickou	klasický	k2eAgFnSc4d1	klasická
jižní	jižní	k2eAgFnSc4d1	jižní
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
Everest	Everest	k1gInSc4	Everest
do	do	k7c2	do
7800	[number]	k4	7800
<g/>
m.	m.	k?	m.
</s>
</p>
<p>
<s>
1956	[number]	k4	1956
–	–	k?	–
první	první	k4xOgFnSc6	první
výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
hlavní	hlavní	k2eAgInSc4d1	hlavní
vrchol	vrchol	k1gInSc4	vrchol
se	se	k3xPyFc4	se
povedl	povést	k5eAaPmAgInS	povést
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
švýcarské	švýcarský	k2eAgFnSc6d1	švýcarská
expedici	expedice	k1gFnSc6	expedice
-	-	kIx~	-
Ernst	Ernst	k1gMnSc1	Ernst
Reiss	Reissa	k1gFnPc2	Reissa
a	a	k8xC	a
Fritz	Fritza	k1gFnPc2	Fritza
Luchsinger	Luchsingra	k1gFnPc2	Luchsingra
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
podnikla	podniknout	k5eAaPmAgFnS	podniknout
současně	současně	k6eAd1	současně
druhý	druhý	k4xOgInSc4	druhý
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
Mount	Mount	k1gInSc4	Mount
Everest	Everest	k1gInSc1	Everest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1965	[number]	k4	1965
–	–	k?	–
první	první	k4xOgFnSc6	první
pokus	pokus	k1gInSc1	pokus
na	na	k7c4	na
Lhoce	Lhoce	k1gMnSc4	Lhoce
Šar	Šar	k1gMnSc4	Šar
japonskou	japonský	k2eAgFnSc7d1	japonská
expedicí	expedice	k1gFnSc7	expedice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
8100	[number]	k4	8100
m	m	kA	m
v	v	k7c6	v
JV	JV	kA	JV
stěně	stěna	k1gFnSc6	stěna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
–	–	k?	–
první	první	k4xOgInSc4	první
výstup	výstup	k1gInSc4	výstup
na	na	k7c6	na
Lhoce	Lhoka	k1gFnSc6	Lhoka
Šar	Šar	k1gFnSc2	Šar
provedli	provést	k5eAaPmAgMnP	provést
dva	dva	k4xCgMnPc1	dva
Rakušané	Rakušan	k1gMnPc1	Rakušan
Zepp	Zepp	k1gMnSc1	Zepp
Mayerl	Mayerl	k1gMnSc1	Mayerl
a	a	k8xC	a
Rolf	Rolf	k1gMnSc1	Rolf
Walter	Walter	k1gMnSc1	Walter
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Stoupali	stoupat	k5eAaImAgMnP	stoupat
JV	JV	kA	JV
úbočím	úbočí	k1gNnSc7	úbočí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
-	-	kIx~	-
první	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
výstup	výstup	k1gInSc4	výstup
J	J	kA	J
stěnou	stěna	k1gFnSc7	stěna
hory	hora	k1gFnSc2	hora
podnikají	podnikat	k5eAaImIp3nP	podnikat
Japonci	Japonec	k1gMnPc1	Japonec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
-	-	kIx~	-
Italská	italský	k2eAgFnSc1d1	italská
expedice	expedice	k1gFnSc1	expedice
(	(	kIx(	(
<g/>
s	s	k7c7	s
účastí	účast	k1gFnSc7	účast
R.	R.	kA	R.
<g/>
Messnera	Messner	k1gMnSc2	Messner
<g/>
)	)	kIx)	)
zkouší	zkoušet	k5eAaImIp3nS	zkoušet
cestu	cesta	k1gFnSc4	cesta
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
části	část	k1gFnSc6	část
J	J	kA	J
stěny	stěna	k1gFnPc1	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
7400	[number]	k4	7400
<g/>
m.	m.	k?	m.
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
-	-	kIx~	-
Poláci	Polák	k1gMnPc1	Polák
zkoušejí	zkoušet	k5eAaImIp3nP	zkoušet
první	první	k4xOgInSc4	první
zimní	zimní	k2eAgInSc4d1	zimní
výstup	výstup	k1gInSc4	výstup
na	na	k7c6	na
Lhoce	Lhoka	k1gFnSc6	Lhoka
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
jednoho	jeden	k4xCgMnSc2	jeden
člena	člen	k1gMnSc2	člen
výpravy	výprava	k1gFnSc2	výprava
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
8200	[number]	k4	8200
<g/>
m.	m.	k?	m.
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
-	-	kIx~	-
druhý	druhý	k4xOgInSc1	druhý
výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
hlavní	hlavní	k2eAgInSc4d1	hlavní
vrchol	vrchol	k1gInSc4	vrchol
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
(	(	kIx(	(
<g/>
Z	Z	kA	Z
stěnou	stěna	k1gFnSc7	stěna
<g/>
)	)	kIx)	)
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
německá	německý	k2eAgFnSc1d1	německá
výprava	výprava	k1gFnSc1	výprava
bez	bez	k7c2	bez
kyslíkových	kyslíkový	k2eAgInPc2d1	kyslíkový
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
sólo	sólo	k2eAgMnSc1d1	sólo
Michael	Michael	k1gMnSc1	Michael
Dacher	Dachra	k1gFnPc2	Dachra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
–	–	k?	–
druhý	druhý	k4xOgInSc4	druhý
výstup	výstup	k1gInSc4	výstup
na	na	k7c6	na
Lhoce	Lhoka	k1gFnSc6	Lhoka
Šar	Šar	k1gFnSc6	Šar
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
Švýcaři	Švýcar	k1gMnPc1	Švýcar
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
-	-	kIx~	-
Jugoslávská	jugoslávský	k2eAgFnSc1d1	jugoslávská
expedice	expedice	k1gFnSc1	expedice
vedená	vedený	k2eAgFnSc1d1	vedená
Alešem	Aleš	k1gMnSc7	Aleš
Kunaverem	Kunaver	k1gMnSc7	Kunaver
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
J	J	kA	J
stěně	stěna	k1gFnSc6	stěna
Lhoce	Lhoce	k1gFnSc1	Lhoce
8100	[number]	k4	8100
<g/>
m.	m.	k?	m.
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
třetí	třetí	k4xOgInSc4	třetí
výstup	výstup	k1gInSc4	výstup
na	na	k7c6	na
Lhoce	Lhoka	k1gFnSc6	Lhoka
Šar	Šar	k1gFnSc2	Šar
provedený	provedený	k2eAgInSc4d1	provedený
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Čechoslováky	Čechoslovák	k1gMnPc4	Čechoslovák
prvovýstupem	prvovýstup	k1gInSc7	prvovýstup
J	J	kA	J
stěnou	stěna	k1gFnSc7	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Zoltán	Zoltán	k1gMnSc1	Zoltán
Demján	Demján	k1gInSc4	Demján
sólo	sólo	k2eAgInSc4d1	sólo
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
den	den	k1gInSc4	den
Josef	Josef	k1gMnSc1	Josef
Rakoncaj	Rakoncaj	k1gMnSc1	Rakoncaj
<g/>
,	,	kIx,	,
Jarýk	Jarýk	k1gMnSc1	Jarýk
Stejskal	Stejskal	k1gMnSc1	Stejskal
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Božík	Božík	k1gMnSc1	Božík
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
výstup	výstup	k1gInSc1	výstup
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
zopakován	zopakovat	k5eAaPmNgInS	zopakovat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejtěžších	těžký	k2eAgFnPc2d3	nejtěžší
cest	cesta	k1gFnPc2	cesta
v	v	k7c6	v
Himálaji	Himálaj	k1gFnSc6	Himálaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
-	-	kIx~	-
Krzysztof	Krzysztof	k1gInSc1	Krzysztof
Wielicki	Wielick	k1gFnSc2	Wielick
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
na	na	k7c6	na
Lhoce	Lhoka	k1gFnSc6	Lhoka
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
–	–	k?	–
Jerzy	Jerza	k1gFnSc2	Jerza
Kukuczka	Kukuczka	k1gFnSc1	Kukuczka
zemřel	zemřít	k5eAaPmAgInS	zemřít
při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
jižní	jižní	k2eAgNnSc1d1	jižní
stěnou	stěna	k1gFnSc7	stěna
Lhoce	Lhoce	k1gFnSc2	Lhoce
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
přetrhlo	přetrhnout	k5eAaPmAgNnS	přetrhnout
lano	lano	k1gNnSc4	lano
v	v	k7c6	v
8200	[number]	k4	8200
<g/>
m.	m.	k?	m.
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
třetí	třetí	k4xOgFnSc4	třetí
polskou	polský	k2eAgFnSc4d1	polská
výpravu	výprava	k1gFnSc4	výprava
v	v	k7c6	v
J	J	kA	J
stěně	stěna	k1gFnSc6	stěna
Lhoce	Lhoce	k1gMnSc4	Lhoce
(	(	kIx(	(
<g/>
předtím	předtím	k6eAd1	předtím
1985	[number]	k4	1985
a	a	k8xC	a
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
neúspěšné	úspěšný	k2eNgFnPc1d1	neúspěšná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
-	-	kIx~	-
Slovinec	Slovinec	k1gMnSc1	Slovinec
Tomo	Tomo	k6eAd1	Tomo
Česen	česno	k1gNnPc2	česno
ohlašuje	ohlašovat	k5eAaImIp3nS	ohlašovat
sólovýstup	sólovýstup	k1gInSc4	sólovýstup
J	J	kA	J
stěnou	stěna	k1gFnSc7	stěna
Lhoce	Lhoce	k1gFnSc2	Lhoce
Jugoslávskou	jugoslávský	k2eAgFnSc7d1	jugoslávská
cestou	cesta	k1gFnSc7	cesta
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
výstup	výstup	k1gInSc1	výstup
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
zpochybňován	zpochybňován	k2eAgInSc1d1	zpochybňován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
-	-	kIx~	-
Rusové	Rus	k1gMnPc1	Rus
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
Česenovi	Česen	k1gMnSc6	Česen
otevírají	otevírat	k5eAaImIp3nP	otevírat
cestu	cesta	k1gFnSc4	cesta
pilířem	pilíř	k1gInSc7	pilíř
v	v	k7c6	v
J	J	kA	J
stěně	stěna	k1gFnSc6	stěna
Lhoce	Lhoce	k1gFnPc1	Lhoce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
Chantal	Chantal	k1gMnSc1	Chantal
Mauduit	Mauduit	k1gMnSc1	Mauduit
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
vrcholu	vrchol	k1gInSc3	vrchol
Lhoce	Lhoec	k1gInSc2	Lhoec
(	(	kIx(	(
<g/>
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
první	první	k4xOgInSc4	první
výstup	výstup	k1gInSc4	výstup
na	na	k7c6	na
Lhoce	Lhoka	k1gFnSc6	Lhoka
Middle	Middle	k1gFnPc2	Middle
se	se	k3xPyFc4	se
zdařil	zdařit	k5eAaPmAgInS	zdařit
Rusům	Rus	k1gMnPc3	Rus
(	(	kIx(	(
<g/>
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
Vinogradsky	Vinogradsky	k1gMnSc1	Vinogradsky
<g/>
,	,	kIx,	,
Sergej	Sergej	k1gMnSc1	Sergej
Timofejev	Timofejev	k1gMnSc1	Timofejev
<g/>
,	,	kIx,	,
Alexej	Alexej	k1gMnSc1	Alexej
Bolotov	Bolotov	k1gInSc1	Bolotov
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Kuzněcov	Kuzněcov	k1gInSc1	Kuzněcov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Lhoce	Lhoko	k6eAd1	Lhoko
Middle	Middle	k1gFnSc1	Middle
byla	být	k5eAaImAgFnS	být
posledním	poslední	k2eAgInSc7d1	poslední
pojmenovaným	pojmenovaný	k2eAgInSc7d1	pojmenovaný
osmitisícovým	osmitisícový	k2eAgInSc7d1	osmitisícový
vrcholem	vrchol	k1gInSc7	vrchol
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
člověk	člověk	k1gMnSc1	člověk
nezlezl	zlézt	k5eNaPmAgMnS	zlézt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
–	–	k?	–
Japonská	japonský	k2eAgFnSc1d1	japonská
expedice	expedice	k1gFnSc1	expedice
zlézá	zlézat	k5eAaImIp3nS	zlézat
jižní	jižní	k2eAgFnSc1d1	jižní
stěnu	stěna	k1gFnSc4	stěna
Lhoce	Lhoce	k1gFnSc2	Lhoce
Jugoslávskou	jugoslávský	k2eAgFnSc7d1	jugoslávská
cestou	cesta	k1gFnSc7	cesta
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Horolezci	horolezec	k1gMnPc1	horolezec
výstup	výstup	k1gInSc4	výstup
přerušili	přerušit	k5eAaPmAgMnP	přerušit
pouhých	pouhý	k2eAgInPc2d1	pouhý
41	[number]	k4	41
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
vrcholem	vrchol	k1gInSc7	vrchol
na	na	k7c6	na
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
vrcholovém	vrcholový	k2eAgInSc6d1	vrcholový
hřebeni	hřeben	k1gInSc6	hřeben
pro	pro	k7c4	pro
nepřízeň	nepřízeň	k1gFnSc4	nepřízeň
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výstupy	výstup	k1gInPc4	výstup
českých	český	k2eAgMnPc2d1	český
horolezců	horolezec	k1gMnPc2	horolezec
===	===	k?	===
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
–	–	k?	–
Ivan	Ivan	k1gMnSc1	Ivan
Exnar	Exnar	k1gMnSc1	Exnar
klasickou	klasický	k2eAgFnSc4d1	klasická
cestou	cesta	k1gFnSc7	cesta
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
Jarýk	Jarýk	k1gMnSc1	Jarýk
Stejskal	Stejskal	k1gMnSc1	Stejskal
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Rakoncaj	Rakoncaj	k1gMnSc1	Rakoncaj
-	-	kIx~	-
prvovýstup	prvovýstup	k1gInSc1	prvovýstup
J	J	kA	J
stěnou	stěna	k1gFnSc7	stěna
na	na	k7c6	na
Lhoce	Lhoka	k1gFnSc6	Lhoka
Šar	Šar	k1gFnSc2	Šar
(	(	kIx(	(
<g/>
s	s	k7c7	s
Peterem	Peter	k1gMnSc7	Peter
Božíkem	Božík	k1gMnSc7	Božík
-	-	kIx~	-
SK	Sk	kA	Sk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Šimůnek	Šimůnek	k1gMnSc1	Šimůnek
<g/>
,	,	kIx,	,
Soňa	Soňa	k1gFnSc1	Soňa
Boštíková	Boštíková	k1gFnSc1	Boštíková
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hrubý	Hrubý	k1gMnSc1	Hrubý
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Morávek	Morávek	k1gMnSc1	Morávek
klasickou	klasický	k2eAgFnSc4d1	klasická
cestou	cesta	k1gFnSc7	cesta
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
–	–	k?	–
Martin	Martin	k1gMnSc1	Martin
Minařík	Minařík	k1gMnSc1	Minařík
-	-	kIx~	-
sólovýstup	sólovýstup	k1gInSc1	sólovýstup
klasickou	klasický	k2eAgFnSc7d1	klasická
cestou	cesta	k1gFnSc7	cesta
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
–	–	k?	–
Radek	Radek	k1gMnSc1	Radek
Jaroš	Jaroš	k1gMnSc1	Jaroš
-	-	kIx~	-
sólo	sólo	k2eAgInSc1d1	sólo
výstup	výstup	k1gInSc1	výstup
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
–	–	k?	–
Milan	Milan	k1gMnSc1	Milan
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
Radovan	Radovan	k1gMnSc1	Radovan
Marek	Marek	k1gMnSc1	Marek
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Sedláček	Sedláček	k1gMnSc1	Sedláček
zahynul	zahynout	k5eAaPmAgMnS	zahynout
na	na	k7c6	na
sestupu	sestup	k1gInSc6	sestup
</s>
</p>
<p>
<s>
===	===	k?	===
Možnosti	možnost	k1gFnPc4	možnost
dalších	další	k2eAgInPc2d1	další
výstupů	výstup	k1gInPc2	výstup
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
obtížné	obtížný	k2eAgFnSc6d1	obtížná
J	J	kA	J
stěně	stěna	k1gFnSc6	stěna
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
nedokončené	dokončený	k2eNgFnPc1d1	nedokončená
cesty	cesta	k1gFnPc1	cesta
italské	italský	k2eAgFnSc2d1	italská
expedice	expedice	k1gFnSc2	expedice
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
vlevo	vlevo	k6eAd1	vlevo
k	k	k7c3	k
JZ	JZ	kA	JZ
hřebeni	hřeben	k1gInSc3	hřeben
<g/>
,	,	kIx,	,
jugoslávská	jugoslávský	k2eAgFnSc1d1	jugoslávská
cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
neúspěchu	neúspěch	k1gInSc2	neúspěch
Tomo	Tomo	k1gMnSc1	Tomo
Česena	Česeno	k1gNnSc2	Česeno
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
středem	střed	k1gInSc7	střed
a	a	k8xC	a
polská	polský	k2eAgFnSc1d1	polská
cesta	cesta	k1gFnSc1	cesta
v	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
části	část	k1gFnSc6	část
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
výstup	výstup	k1gInSc4	výstup
pravou	pravý	k2eAgFnSc7d1	pravá
částí	část	k1gFnSc7	část
J	J	kA	J
stěny	stěna	k1gFnPc1	stěna
Lhoce	Lhoce	k1gMnSc1	Lhoce
Šar	Šar	k1gMnSc1	Šar
do	do	k7c2	do
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
trasy	trasa	k1gFnSc2	trasa
JV	JV	kA	JV
úbočím	úbočí	k1gNnSc7	úbočí
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Z	Z	kA	Z
stěně	stěna	k1gFnSc6	stěna
hory	hora	k1gFnSc2	hora
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc1	možnost
výstupu	výstup	k1gInSc2	výstup
pravou	pravá	k1gFnSc4	pravá
částí	část	k1gFnPc2	část
k	k	k7c3	k
JZ	JZ	kA	JZ
hřebeni	hřeben	k1gInSc6	hřeben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
severu	sever	k1gInSc2	sever
(	(	kIx(	(
<g/>
z	z	k7c2	z
Tibetu	Tibet	k1gInSc2	Tibet
<g/>
)	)	kIx)	)
nevedou	vést	k5eNaImIp3nP	vést
dosud	dosud	k6eAd1	dosud
žádné	žádný	k3yNgFnPc4	žádný
cesty	cesta	k1gFnPc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
varianta	varianta	k1gFnSc1	varianta
britsko-australské	britskoustralský	k2eAgFnSc2d1	britsko-australská
cesty	cesta	k1gFnSc2	cesta
V	v	k7c6	v
stěnou	stěna	k1gFnSc7	stěna
Mount	Mounta	k1gFnPc2	Mounta
Everestu	Everest	k1gInSc2	Everest
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
s	s	k7c7	s
navázáním	navázání	k1gNnSc7	navázání
na	na	k7c4	na
SZ	SZ	kA	SZ
hřeben	hřeben	k1gInSc4	hřeben
nebo	nebo	k8xC	nebo
na	na	k7c4	na
klasickou	klasický	k2eAgFnSc4d1	klasická
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
výzvou	výzva	k1gFnSc7	výzva
stále	stále	k6eAd1	stále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
přechod	přechod	k1gInSc1	přechod
všech	všecek	k3xTgInPc2	všecek
vrcholů	vrchol	k1gInPc2	vrchol
Lhoce	Lhoce	k1gFnSc2	Lhoce
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
o	o	k7c4	o
přechod	přechod	k1gInSc4	přechod
pokusil	pokusit	k5eAaPmAgMnS	pokusit
Francouz	Francouz	k1gMnSc1	Francouz
Nicholas	Nicholas	k1gMnSc1	Nicholas
Jaeger	Jaeger	k1gMnSc1	Jaeger
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zmizel	zmizet	k5eAaPmAgMnS	zmizet
beze	beze	k7c2	beze
stopy	stopa	k1gFnSc2	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Erhard	Erhard	k1gMnSc1	Erhard
Loretan	Loretan	k1gInSc4	Loretan
vzdal	vzdát	k5eAaPmAgMnS	vzdát
myšlenky	myšlenka	k1gFnPc4	myšlenka
na	na	k7c4	na
přechod	přechod	k1gInSc4	přechod
hory	hora	k1gFnSc2	hora
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
hlavního	hlavní	k2eAgInSc2d1	hlavní
vrcholu	vrchol	k1gInSc2	vrchol
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Neuspěli	uspět	k5eNaPmAgMnP	uspět
ani	ani	k8xC	ani
Rusové	Rus	k1gMnPc1	Rus
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hor	hora	k1gFnPc2	hora
světa	svět	k1gInSc2	svět
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lhoce	Lhoce	k1gFnSc2	Lhoce
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Lhoce	Lhoce	k1gFnSc2	Lhoce
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Minařík	Minařík	k1gMnSc1	Minařík
</s>
</p>
<p>
<s>
Expedice	expedice	k1gFnSc1	expedice
Lhotse	Lhotse	k1gFnSc2	Lhotse
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
Radka	Radek	k1gMnSc2	Radek
Jaroše	Jaroš	k1gMnSc2	Jaroš
</s>
</p>
