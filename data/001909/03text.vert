<s>
Kambrium	kambrium	k1gNnSc1	kambrium
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
perioda	perioda	k1gFnSc1	perioda
prvohor	prvohory	k1gFnPc2	prvohory
(	(	kIx(	(
<g/>
paleozoika	paleozoikum	k1gNnSc2	paleozoikum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
počátek	počátek	k1gInSc1	počátek
je	být	k5eAaImIp3nS	být
kladen	kladen	k2eAgInSc1d1	kladen
před	před	k7c7	před
cca	cca	kA	cca
542	[number]	k4	542
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
končí	končit	k5eAaImIp3nS	končit
cca	cca	kA	cca
před	před	k7c4	před
488	[number]	k4	488
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začátkem	začátkem	k7c2	začátkem
ordoviku	ordovik	k1gInSc2	ordovik
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgNnSc7d3	nejstarší
obdobím	období	k1gNnSc7	období
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnPc6	jehož
horninách	hornina	k1gFnPc6	hornina
byly	být	k5eAaImAgInP	být
nalezeny	nalezen	k2eAgInPc1d1	nalezen
četné	četný	k2eAgInPc1d1	četný
fosilizované	fosilizovaný	k2eAgInPc1d1	fosilizovaný
mnohobuněčné	mnohobuněčný	k2eAgInPc1d1	mnohobuněčný
organismy	organismus	k1gInPc1	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgInS	použít
anglický	anglický	k2eAgMnSc1d1	anglický
geolog	geolog	k1gMnSc1	geolog
A.	A.	kA	A.
Sedgwick	Sedgwick	k1gMnSc1	Sedgwick
roku	rok	k1gInSc2	rok
1833	[number]	k4	1833
dle	dle	k7c2	dle
latinského	latinský	k2eAgNnSc2d1	latinské
označení	označení	k1gNnSc2	označení
severního	severní	k2eAgInSc2d1	severní
Walesu	Wales	k1gInSc2	Wales
(	(	kIx(	(
<g/>
Cambria	Cambrium	k1gNnSc2	Cambrium
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
spodní	spodní	k2eAgFnSc4d1	spodní
část	část	k1gFnSc4	část
vrstev	vrstva	k1gFnPc2	vrstva
ležících	ležící	k2eAgFnPc2d1	ležící
diskordantně	diskordantně	k6eAd1	diskordantně
na	na	k7c6	na
proterozoiku	proterozoikum	k1gNnSc6	proterozoikum
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
rozsah	rozsah	k1gInSc4	rozsah
vymezený	vymezený	k2eAgInSc4d1	vymezený
Sedgwickem	Sedgwick	k1gInSc7	Sedgwick
však	však	k8xC	však
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
i	i	k9	i
vrstvy	vrstva	k1gFnPc1	vrstva
později	pozdě	k6eAd2	pozdě
zařazené	zařazený	k2eAgInPc4d1	zařazený
do	do	k7c2	do
ordoviku	ordovik	k1gInSc2	ordovik
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
bylo	být	k5eAaImAgNnS	být
kambrium	kambrium	k1gNnSc1	kambrium
vymezeno	vymezit	k5eAaPmNgNnS	vymezit
na	na	k7c6	na
období	období	k1gNnSc6	období
před	před	k7c4	před
570-500	[number]	k4	570-500
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
hranice	hranice	k1gFnSc1	hranice
je	být	k5eAaImIp3nS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
nástupem	nástup	k1gInSc7	nástup
primitivních	primitivní	k2eAgInPc2d1	primitivní
útesotvorných	útesotvorný	k2eAgInPc2d1	útesotvorný
organismů	organismus	k1gInPc2	organismus
(	(	kIx(	(
<g/>
Archaeocyatha	Archaeocyatha	k1gMnSc1	Archaeocyatha
<g/>
)	)	kIx)	)
a	a	k8xC	a
raných	raný	k2eAgMnPc2d1	raný
členovců	členovec	k1gMnPc2	členovec
(	(	kIx(	(
<g/>
trilobiti	trilobit	k1gMnPc1	trilobit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
hranici	hranice	k1gFnSc4	hranice
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
zvýšení	zvýšení	k1gNnSc1	zvýšení
obsahu	obsah	k1gInSc2	obsah
izotopu	izotop	k1gInSc2	izotop
C-	C-	k1gFnSc2	C-
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
dle	dle	k7c2	dle
některých	některý	k3yIgMnPc2	některý
autorů	autor	k1gMnPc2	autor
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
následcích	následek	k1gInPc6	následek
dopadu	dopad	k1gInSc2	dopad
mimozemského	mimozemský	k2eAgNnSc2d1	mimozemské
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgNnSc2	tento
stáří	stáří	k1gNnSc2	stáří
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
horizont	horizont	k1gInSc1	horizont
sopečného	sopečný	k2eAgInSc2d1	sopečný
popela	popel	k1gInSc2	popel
v	v	k7c6	v
Ománu	Omán	k1gInSc6	Omán
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInPc4d1	obsahující
zirkony	zirkon	k1gInPc4	zirkon
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
radiometrické	radiometrický	k2eAgNnSc1d1	radiometrické
stáří	stáří	k1gNnSc1	stáří
je	být	k5eAaImIp3nS	být
542	[number]	k4	542
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgNnPc1d1	podobné
data	datum	k1gNnPc1	datum
poskytly	poskytnout	k5eAaPmAgInP	poskytnout
anomálie	anomálie	k1gFnPc4	anomálie
C-13	C-13	k1gFnPc2	C-13
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
a	a	k8xC	a
v	v	k7c6	v
Namibii	Namibie	k1gFnSc6	Namibie
<g/>
.	.	kIx.	.
</s>
<s>
Svrchní	svrchní	k2eAgFnSc1d1	svrchní
hranice	hranice	k1gFnSc1	hranice
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
nástupem	nástup	k1gInSc7	nástup
trilobitů	trilobit	k1gMnPc2	trilobit
rodu	rod	k1gInSc2	rod
Jujuyaspis	Jujuyaspis	k1gFnPc2	Jujuyaspis
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
vůdčí	vůdčí	k2eAgFnSc4d1	vůdčí
fosílii	fosílie	k1gFnSc4	fosílie
spodního	spodní	k2eAgInSc2d1	spodní
ordoviku	ordovik	k1gInSc2	ordovik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
stanovila	stanovit	k5eAaPmAgFnS	stanovit
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
subkomise	subkomise	k1gFnSc1	subkomise
pro	pro	k7c4	pro
stratigrafii	stratigrafie	k1gFnSc4	stratigrafie
rozpětí	rozpětí	k1gNnSc2	rozpětí
kambria	kambrium	k1gNnSc2	kambrium
na	na	k7c4	na
545-490	[number]	k4	545-490
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
kambrium	kambrium	k1gNnSc1	kambrium
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
3	[number]	k4	3
oddíly	oddíl	k1gInPc4	oddíl
<g/>
:	:	kIx,	:
spodní	spodní	k2eAgFnSc1d1	spodní
(	(	kIx(	(
<g/>
časově	časově	k6eAd1	časově
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc4d1	střední
a	a	k8xC	a
svrchní	svrchní	k2eAgFnSc4d1	svrchní
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
specifickým	specifický	k2eAgInSc7d1	specifický
faunistickým	faunistický	k2eAgInSc7d1	faunistický
obsahem	obsah	k1gInSc7	obsah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
Waucoban	Waucoban	k1gMnSc1	Waucoban
<g/>
,	,	kIx,	,
Albertian	Albertian	k1gMnSc1	Albertian
a	a	k8xC	a
Furongian	Furongian	k1gMnSc1	Furongian
(	(	kIx(	(
<g/>
Croixian	Croixian	k1gMnSc1	Croixian
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
biostratigrafické	biostratigrafický	k2eAgFnPc1d1	biostratigrafický
jednotky	jednotka	k1gFnPc1	jednotka
společné	společný	k2eAgFnPc1d1	společná
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
kambria	kambrium	k1gNnSc2	kambrium
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
spodní	spodní	k2eAgNnSc4d1	spodní
kambrium	kambrium	k1gNnSc4	kambrium
Cordubien	Cordubina	k1gFnPc2	Cordubina
<g/>
,	,	kIx,	,
Ovétien	Ovétina	k1gFnPc2	Ovétina
<g/>
,	,	kIx,	,
Mariannien	Mariannina	k1gFnPc2	Mariannina
a	a	k8xC	a
Bilbillien	Bilbillina	k1gFnPc2	Bilbillina
<g/>
,	,	kIx,	,
definované	definovaný	k2eAgInPc1d1	definovaný
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
střední	střední	k2eAgInSc4d1	střední
Léonien	Léonien	k1gInSc4	Léonien
<g/>
,	,	kIx,	,
Caesaraugustien	Caesaraugustien	k2eAgInSc4d1	Caesaraugustien
a	a	k8xC	a
Languedocien	Languedocien	k2eAgInSc4d1	Languedocien
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hlavních	hlavní	k2eAgInPc2d1	hlavní
rodů	rod	k1gInPc2	rod
trilobitů	trilobit	k1gMnPc2	trilobit
se	se	k3xPyFc4	se
v	v	k7c6	v
baltsko-atlantské	baltskotlantský	k2eAgFnSc6d1	baltsko-atlantský
paleozoogeografické	paleozoogeografický	k2eAgFnSc6d1	paleozoogeografický
provincii	provincie	k1gFnSc6	provincie
zastarale	zastarale	k6eAd1	zastarale
označuje	označovat	k5eAaImIp3nS	označovat
spodní	spodní	k2eAgNnSc4d1	spodní
kambrium	kambrium	k1gNnSc4	kambrium
jako	jako	k8xS	jako
olenellové	olenellová	k1gFnPc4	olenellová
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
rodu	rod	k1gInSc2	rod
Olenellus	Olenellus	k1gInSc1	Olenellus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnPc1d1	střední
jako	jako	k8xC	jako
paradoxidové	paradoxidový	k2eAgFnPc1d1	paradoxidový
(	(	kIx(	(
<g/>
rod	rod	k1gInSc1	rod
Paradoxides	Paradoxides	k1gInSc1	Paradoxides
<g/>
)	)	kIx)	)
a	a	k8xC	a
svrchní	svrchní	k2eAgInSc1d1	svrchní
jako	jako	k8xS	jako
olenové	olenové	k2eAgInSc1d1	olenové
(	(	kIx(	(
<g/>
rod	rod	k1gInSc1	rod
Olenus	Olenus	k1gInSc1	Olenus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
oddíl	oddíl	k1gInSc1	oddíl
kambria	kambrium	k1gNnSc2	kambrium
je	být	k5eAaImIp3nS	být
regionálně	regionálně	k6eAd1	regionálně
dále	daleko	k6eAd2	daleko
členěn	členit	k5eAaImNgInS	členit
na	na	k7c4	na
faunistické	faunistický	k2eAgInPc4d1	faunistický
stupně	stupeň	k1gInPc4	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
komisí	komise	k1gFnSc7	komise
pro	pro	k7c4	pro
stratigrafii	stratigrafie	k1gFnSc4	stratigrafie
byl	být	k5eAaImAgInS	být
však	však	k9	však
dosud	dosud	k6eAd1	dosud
uznán	uznat	k5eAaPmNgInS	uznat
pouze	pouze	k6eAd1	pouze
svrchnokambrický	svrchnokambrický	k2eAgInSc1d1	svrchnokambrický
Paibian	Paibian	k1gInSc1	Paibian
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
subkomise	subkomise	k1gFnSc1	subkomise
pro	pro	k7c4	pro
kambrickou	kambrický	k2eAgFnSc4d1	kambrická
stratigrafii	stratigrafie	k1gFnSc4	stratigrafie
rozčlenila	rozčlenit	k5eAaPmAgFnS	rozčlenit
kambrium	kambrium	k1gNnSc4	kambrium
na	na	k7c4	na
4	[number]	k4	4
série	série	k1gFnSc2	série
a	a	k8xC	a
10	[number]	k4	10
etáží	etáž	k1gFnPc2	etáž
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
spodnímu	spodní	k2eAgNnSc3d1	spodní
kambriu	kambrium	k1gNnSc3	kambrium
<g/>
,	,	kIx,	,
etáž	etáž	k1gFnSc1	etáž
1	[number]	k4	1
začíná	začínat	k5eAaImIp3nS	začínat
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
Trichophycus	Trichophycus	k1gMnSc1	Trichophycus
pedum	pedum	k1gNnSc1	pedum
<g/>
,	,	kIx,	,
etáž	etáž	k1gFnSc1	etáž
2	[number]	k4	2
objevením	objevení	k1gNnSc7	objevení
archeocyátů	archeocyát	k1gMnPc2	archeocyát
<g/>
,	,	kIx,	,
3	[number]	k4	3
etáž	etáž	k1gFnSc1	etáž
s	s	k7c7	s
trilobity	trilobit	k1gMnPc7	trilobit
Fallotaspididae	Fallotaspididae	k1gNnSc2	Fallotaspididae
a	a	k8xC	a
4	[number]	k4	4
etáž	etáž	k1gFnSc1	etáž
s	s	k7c7	s
trilobity	trilobit	k1gMnPc7	trilobit
Olenellus	Olenellus	k1gInSc4	Olenellus
v	v	k7c4	v
oblasti	oblast	k1gFnPc4	oblast
Laurentie	Laurentie	k1gFnSc2	Laurentie
a	a	k8xC	a
Redlichiidae	Redlichiida	k1gFnSc2	Redlichiida
v	v	k7c6	v
Gondwaně	Gondwana	k1gFnSc6	Gondwana
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
3	[number]	k4	3
koresponduje	korespondovat	k5eAaImIp3nS	korespondovat
se	s	k7c7	s
středním	střední	k2eAgNnSc7d1	střední
kambriem	kambrium	k1gNnSc7	kambrium
<g/>
,	,	kIx,	,
etáž	etáž	k1gFnSc1	etáž
5	[number]	k4	5
začíná	začínat	k5eAaImIp3nS	začínat
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
trilobitů	trilobit	k1gMnPc2	trilobit
Oryctocephalus	Oryctocephalus	k1gInSc4	Oryctocephalus
<g/>
,	,	kIx,	,
etáž	etáž	k1gFnSc1	etáž
6	[number]	k4	6
(	(	kIx(	(
<g/>
Drumien	Drumien	k2eAgInSc4d1	Drumien
<g/>
)	)	kIx)	)
Ptychagnostus	Ptychagnostus	k1gInSc4	Ptychagnostus
a	a	k8xC	a
7	[number]	k4	7
etáž	etáž	k1gFnSc1	etáž
Lejopyge	Lejopyge	k1gFnSc1	Lejopyge
laevigata	laevigata	k1gFnSc1	laevigata
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
série	série	k1gFnSc1	série
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Furongian	Furongian	k1gInSc1	Furongian
<g/>
,	,	kIx,	,
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
svrchnímu	svrchní	k2eAgNnSc3d1	svrchní
kambriu	kambrium	k1gNnSc3	kambrium
<g/>
,	,	kIx,	,
etáž	etáž	k1gFnSc1	etáž
8	[number]	k4	8
(	(	kIx(	(
<g/>
Paibian	Paibian	k1gInSc1	Paibian
<g/>
)	)	kIx)	)
s	s	k7c7	s
objevením	objevení	k1gNnSc7	objevení
trilobitů	trilobit	k1gMnPc2	trilobit
Glyptagnostus	Glyptagnostus	k1gMnSc1	Glyptagnostus
reticulatus	reticulatus	k1gMnSc1	reticulatus
<g/>
,	,	kIx,	,
9	[number]	k4	9
etáž	etáž	k1gFnSc1	etáž
s	s	k7c7	s
Agnostotes	Agnostotes	k1gMnSc1	Agnostotes
orientalista	orientalista	k1gMnSc1	orientalista
<g/>
,	,	kIx,	,
10	[number]	k4	10
etáž	etáž	k1gFnSc4	etáž
pak	pak	k6eAd1	pak
s	s	k7c7	s
trilobity	trilobit	k1gMnPc7	trilobit
Lotagnostus	Lotagnostus	k1gMnSc1	Lotagnostus
americanus	americanus	k1gMnSc1	americanus
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
převzato	převzít	k5eAaPmNgNnS	převzít
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
originálu	originál	k1gInSc2	originál
<g/>
)	)	kIx)	)
Rozmístění	rozmístění	k1gNnSc1	rozmístění
kontinentů	kontinent	k1gInPc2	kontinent
v	v	k7c6	v
kambriu	kambrium	k1gNnSc6	kambrium
bylo	být	k5eAaImAgNnS	být
značně	značně	k6eAd1	značně
odlišné	odlišný	k2eAgNnSc1d1	odlišné
od	od	k7c2	od
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Stabilní	stabilní	k2eAgInPc1d1	stabilní
pevninské	pevninský	k2eAgInPc1d1	pevninský
štíty	štít	k1gInPc1	štít
-	-	kIx~	-
jihoamerický	jihoamerický	k2eAgInSc1d1	jihoamerický
<g/>
,	,	kIx,	,
africký	africký	k2eAgInSc1d1	africký
<g/>
,	,	kIx,	,
arabský	arabský	k2eAgInSc1d1	arabský
<g/>
,	,	kIx,	,
indický	indický	k2eAgMnSc1d1	indický
<g/>
,	,	kIx,	,
antarktický	antarktický	k2eAgInSc1d1	antarktický
a	a	k8xC	a
australský	australský	k2eAgInSc1d1	australský
tvořily	tvořit	k5eAaImAgFnP	tvořit
jediný	jediný	k2eAgInSc4d1	jediný
superkontinent	superkontinent	k1gInSc4	superkontinent
-	-	kIx~	-
Gondwanu	Gondwana	k1gFnSc4	Gondwana
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgInSc4d1	ležící
nepochybně	pochybně	k6eNd1	pochybně
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
bloky	blok	k1gInPc1	blok
<g/>
,	,	kIx,	,
kanadský	kanadský	k2eAgMnSc1d1	kanadský
(	(	kIx(	(
<g/>
Laurentia	Laurentia	k1gFnSc1	Laurentia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
baltický	baltický	k2eAgMnSc1d1	baltický
(	(	kIx(	(
<g/>
Baltica	Baltica	k1gMnSc1	Baltica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sibiřský	sibiřský	k2eAgInSc1d1	sibiřský
(	(	kIx(	(
<g/>
Angara	Angara	k1gFnSc1	Angara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čínský	čínský	k2eAgInSc4d1	čínský
a	a	k8xC	a
kazašský	kazašský	k2eAgInSc4d1	kazašský
byly	být	k5eAaImAgInP	být
vzájemně	vzájemně	k6eAd1	vzájemně
odděleny	oddělit	k5eAaPmNgInP	oddělit
oceány	oceán	k1gInPc7	oceán
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
částečně	částečně	k6eAd1	částečně
vynořeny	vynořit	k5eAaPmNgFnP	vynořit
popř.	popř.	kA	popř.
pokryty	pokryt	k2eAgInPc1d1	pokryt
mělkými	mělký	k2eAgFnPc7d1	mělká
epikontinentálními	epikontinentální	k2eAgNnPc7d1	epikontinentální
moři	moře	k1gNnPc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
nálezů	nález	k1gInPc2	nález
fauny	fauna	k1gFnSc2	fauna
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
bloky	blok	k1gInPc1	blok
nacházely	nacházet	k5eAaImAgInP	nacházet
v	v	k7c6	v
teplé	teplý	k2eAgFnSc6d1	teplá
klimatické	klimatický	k2eAgFnSc6d1	klimatická
zóně	zóna	k1gFnSc6	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
spodní	spodní	k2eAgNnSc4d1	spodní
a	a	k8xC	a
střední	střední	k2eAgNnSc4d1	střední
kambrium	kambrium	k1gNnSc4	kambrium
je	být	k5eAaImIp3nS	být
příznačná	příznačný	k2eAgFnSc1d1	příznačná
mořská	mořský	k2eAgFnSc1d1	mořská
transgrese	transgrese	k1gFnSc1	transgrese
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgFnSc1d1	dosahující
vrcholu	vrchol	k1gInSc2	vrchol
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
karbonu	karbon	k1gInSc6	karbon
<g/>
.	.	kIx.	.
</s>
<s>
Plynulý	plynulý	k2eAgInSc1d1	plynulý
přechod	přechod	k1gInSc1	přechod
sedimentace	sedimentace	k1gFnSc2	sedimentace
od	od	k7c2	od
proterozoika	proterozoikum	k1gNnSc2	proterozoikum
byl	být	k5eAaImAgInS	být
zjištěn	zjistit	k5eAaPmNgInS	zjistit
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
sibiřském	sibiřský	k2eAgInSc6d1	sibiřský
štítu	štít	k1gInSc6	štít
<g/>
,	,	kIx,	,
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
a	a	k8xC	a
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgNnSc1d1	vrchní
kambrium	kambrium	k1gNnSc1	kambrium
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
regresí	regrese	k1gFnSc7	regrese
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
častými	častý	k2eAgInPc7d1	častý
stratigrafickými	stratigrafický	k2eAgInPc7d1	stratigrafický
hiáty	hiát	k1gInPc7	hiát
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
má	mít	k5eAaImIp3nS	mít
transgresivní	transgresivní	k2eAgInSc4d1	transgresivní
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Horniny	hornina	k1gFnPc1	hornina
spodního	spodní	k2eAgNnSc2d1	spodní
kambria	kambrium	k1gNnSc2	kambrium
představují	představovat	k5eAaImIp3nP	představovat
často	často	k6eAd1	často
mělkovodní	mělkovodní	k2eAgFnSc1d1	mělkovodní
klastika	klastika	k1gFnSc1	klastika
(	(	kIx(	(
<g/>
slepence	slepenec	k1gInPc1	slepenec
<g/>
,	,	kIx,	,
pískovce	pískovec	k1gInPc1	pískovec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrstevnaté	vrstevnatý	k2eAgInPc1d1	vrstevnatý
biochemické	biochemický	k2eAgInPc1d1	biochemický
archeocyátové	archeocyátový	k2eAgInPc1d1	archeocyátový
vápence	vápenec	k1gInPc1	vápenec
(	(	kIx(	(
<g/>
indikující	indikující	k2eAgNnSc1d1	indikující
teplé	teplý	k2eAgNnSc1d1	teplé
klima	klima	k1gNnSc1	klima
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známá	k1gFnSc1	známá
jsou	být	k5eAaImIp3nP	být
ložiska	ložisko	k1gNnPc1	ložisko
evaporitů	evaporit	k1gInPc2	evaporit
(	(	kIx(	(
<g/>
Sibiř	Sibiř	k1gFnSc1	Sibiř
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Sev	Sev	k1gFnSc1	Sev
<g/>
.	.	kIx.	.
</s>
<s>
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
Ve	v	k7c6	v
spodním	spodní	k2eAgNnSc6d1	spodní
kambriu	kambrium	k1gNnSc6	kambrium
doznívá	doznívat	k5eAaImIp3nS	doznívat
assyntské	assyntský	k2eAgNnSc1d1	assyntský
(	(	kIx(	(
<g/>
kadomské	kadomský	k2eAgNnSc4d1	kadomské
<g/>
)	)	kIx)	)
vrásnění	vrásnění	k1gNnSc4	vrásnění
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svrchním	svrchní	k2eAgNnSc6d1	svrchní
kambriu	kambrium	k1gNnSc6	kambrium
se	se	k3xPyFc4	se
tektonická	tektonický	k2eAgFnSc1d1	tektonická
aktivita	aktivita	k1gFnSc1	aktivita
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
fáze	fáze	k1gFnSc1	fáze
sardinská	sardinský	k2eAgFnSc1d1	sardinská
(	(	kIx(	(
<g/>
trvá	trvat	k5eAaImIp3nS	trvat
do	do	k7c2	do
spodního	spodní	k2eAgInSc2d1	spodní
ordoviku	ordovik	k1gInSc2	ordovik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
orogenetická	orogenetický	k2eAgFnSc1d1	orogenetický
aktivita	aktivita	k1gFnSc1	aktivita
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhalo	probíhat	k5eAaImAgNnS	probíhat
salairské	salairský	k2eAgNnSc4d1	salairský
vrásnění	vrásnění	k1gNnSc4	vrásnění
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
etapách	etapa	k1gFnPc6	etapa
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
tektonické	tektonický	k2eAgInPc1d1	tektonický
pohyby	pohyb	k1gInPc1	pohyb
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
lokální	lokální	k2eAgInSc4d1	lokální
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
doložený	doložený	k2eAgInSc1d1	doložený
stratigrafickými	stratigrafický	k2eAgInPc7d1	stratigrafický
hiáty	hiát	k1gInPc7	hiát
(	(	kIx(	(
<g/>
sandomierská	sandomierský	k2eAgFnSc1d1	sandomierský
fáze	fáze	k1gFnSc1	fáze
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vrstvách	vrstva	k1gFnPc6	vrstva
spodního	spodní	k2eAgNnSc2d1	spodní
kambria	kambrium	k1gNnSc2	kambrium
se	se	k3xPyFc4	se
náhle	náhle	k6eAd1	náhle
objevuje	objevovat	k5eAaImIp3nS	objevovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Bouřlivý	bouřlivý	k2eAgInSc1d1	bouřlivý
nástup	nástup	k1gInSc1	nástup
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
nazýván	nazývat	k5eAaImNgInS	nazývat
evoluční	evoluční	k2eAgFnSc7d1	evoluční
explozí	exploze	k1gFnSc7	exploze
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
kambria	kambrium	k1gNnSc2	kambrium
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
objevují	objevovat	k5eAaImIp3nP	objevovat
všechny	všechen	k3xTgInPc4	všechen
hlavní	hlavní	k2eAgInPc4d1	hlavní
živočišné	živočišný	k2eAgInPc4d1	živočišný
kmeny	kmen	k1gInPc4	kmen
<g/>
:	:	kIx,	:
Porifera	Porifero	k1gNnSc2	Porifero
(	(	kIx(	(
<g/>
mořské	mořský	k2eAgFnPc1d1	mořská
houby	houba	k1gFnPc1	houba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vermes	Vermes	k1gInSc1	Vermes
(	(	kIx(	(
<g/>
červi	červ	k1gMnPc1	červ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Coelenterata	Coelenterata	k1gFnSc1	Coelenterata
<g/>
,	,	kIx,	,
Bryozoa	Bryozoa	k1gFnSc1	Bryozoa
(	(	kIx(	(
<g/>
Mechovky	mechovka	k1gFnPc1	mechovka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brachiopoda	Brachiopoda	k1gMnSc1	Brachiopoda
<g/>
,	,	kIx,	,
Mollusca	Mollusca	k1gMnSc1	Mollusca
<g/>
,	,	kIx,	,
Arthropoda	Arthropoda	k1gMnSc1	Arthropoda
<g/>
,	,	kIx,	,
Echinodermata	Echinoderma	k1gNnPc4	Echinoderma
a	a	k8xC	a
v	v	k7c6	v
pozdním	pozdní	k2eAgNnSc6d1	pozdní
kambriu	kambrium	k1gNnSc6	kambrium
i	i	k9	i
Vertebrata	Vertebrat	k2eAgFnSc1d1	Vertebrat
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
teorií	teorie	k1gFnPc2	teorie
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
hladiny	hladina	k1gFnSc2	hladina
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
rychlejší	rychlý	k2eAgInSc4d2	rychlejší
životní	životní	k2eAgInSc4d1	životní
pochody	pochod	k1gInPc4	pochod
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
organismy	organismus	k1gInPc7	organismus
získávaly	získávat	k5eAaImAgFnP	získávat
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
budování	budování	k1gNnSc4	budování
pevných	pevný	k2eAgFnPc2d1	pevná
schránek	schránka	k1gFnPc2	schránka
a	a	k8xC	a
koster	kostra	k1gFnPc2	kostra
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
teorie	teorie	k1gFnSc1	teorie
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
vývoji	vývoj	k1gInSc3	vývoj
došlo	dojít	k5eAaPmAgNnS	dojít
vlivem	vliv	k1gInSc7	vliv
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
chemismu	chemismus	k1gInSc6	chemismus
oceánských	oceánský	k2eAgFnPc2d1	oceánská
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
skupinou	skupina	k1gFnSc7	skupina
kambria	kambrium	k1gNnSc2	kambrium
byli	být	k5eAaImAgMnP	být
trilobiti	trilobit	k1gMnPc1	trilobit
(	(	kIx(	(
<g/>
asi	asi	k9	asi
60	[number]	k4	60
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
vymezovány	vymezován	k2eAgFnPc1d1	vymezována
faunistické	faunistický	k2eAgFnPc1d1	faunistická
provincie	provincie	k1gFnPc1	provincie
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
podrobné	podrobný	k2eAgNnSc4d1	podrobné
zonální	zonální	k2eAgNnSc4d1	zonální
členění	členění	k1gNnSc4	členění
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spodním	spodní	k2eAgNnSc6d1	spodní
kambriu	kambrium	k1gNnSc6	kambrium
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
především	především	k9	především
rody	rod	k1gInPc1	rod
Olenellus	Olenellus	k1gInSc1	Olenellus
<g/>
,	,	kIx,	,
Fallotaspis	Fallotaspis	k1gInSc1	Fallotaspis
<g/>
,	,	kIx,	,
Holmia	holmium	k1gNnPc1	holmium
<g/>
,	,	kIx,	,
Protolenus	Protolenus	k1gInSc1	Protolenus
<g/>
,	,	kIx,	,
Strenuella	Strenuella	k1gFnSc1	Strenuella
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středním	střední	k1gMnSc6	střední
Paradoxides	Paradoxides	k1gInSc4	Paradoxides
<g/>
,	,	kIx,	,
Ptychoparia	Ptychoparium	k1gNnPc4	Ptychoparium
<g/>
,	,	kIx,	,
Conocoryphe	Conocoryph	k1gFnPc4	Conocoryph
<g/>
,	,	kIx,	,
Sao	Sao	k1gMnSc1	Sao
<g/>
,	,	kIx,	,
Agraulos	Agraulos	k1gMnSc1	Agraulos
<g/>
,	,	kIx,	,
Ellipsocephalus	Ellipsocephalus	k1gMnSc1	Ellipsocephalus
<g/>
.	.	kIx.	.
</s>
<s>
Drobní	drobný	k2eAgMnPc1d1	drobný
agnostidní	agnostidní	k2eAgMnPc1d1	agnostidní
trilobité	trilobita	k1gMnPc1	trilobita
s	s	k7c7	s
redukovaným	redukovaný	k2eAgInSc7d1	redukovaný
počtem	počet	k1gInSc7	počet
hrudních	hrudní	k2eAgInPc2d1	hrudní
článků	článek	k1gInPc2	článek
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
zonální	zonální	k2eAgNnSc1d1	zonální
členění	členění	k1gNnSc1	členění
středního	střední	k2eAgNnSc2d1	střední
a	a	k8xC	a
svrchního	svrchní	k2eAgNnSc2d1	svrchní
kambria	kambrium	k1gNnSc2	kambrium
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
rozšíření	rozšíření	k1gNnSc2	rozšíření
trilobitů	trilobit	k1gMnPc2	trilobit
se	se	k3xPyFc4	se
vyčleňují	vyčleňovat	k5eAaImIp3nP	vyčleňovat
následující	následující	k2eAgFnPc1d1	následující
biostratigrafické	biostratigrafický	k2eAgFnPc1d1	biostratigrafický
provincie	provincie	k1gFnPc1	provincie
<g/>
:	:	kIx,	:
atlantická	atlantický	k2eAgFnSc1d1	Atlantická
(	(	kIx(	(
<g/>
akadobaltická	akadobaltický	k2eAgFnSc1d1	akadobaltický
<g/>
)	)	kIx)	)
s	s	k7c7	s
dílčí	dílčí	k2eAgFnSc7d1	dílčí
severoevropskou	severoevropský	k2eAgFnSc7d1	severoevropská
a	a	k8xC	a
mediteránní	mediteránní	k2eAgFnSc7d1	mediteránní
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnSc7d1	zahrnující
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc4d1	severní
Afriku	Afrika	k1gFnSc4	Afrika
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
pacifická	pacifický	k2eAgFnSc1d1	Pacifická
(	(	kIx(	(
<g/>
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
sever	sever	k1gInSc1	sever
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
)	)	kIx)	)
sibiřská	sibiřský	k2eAgFnSc1d1	sibiřská
čínská	čínský	k2eAgFnSc1d1	čínská
(	(	kIx(	(
<g/>
jihoasijsko-australská	jihoasijskoustralský	k2eAgFnSc1d1	jihoasijsko-australský
<g/>
)	)	kIx)	)
Významnou	významný	k2eAgFnSc7d1	významná
skupinou	skupina	k1gFnSc7	skupina
výlučně	výlučně	k6eAd1	výlučně
spodního	spodní	k2eAgNnSc2d1	spodní
a	a	k8xC	a
středního	střední	k2eAgNnSc2d1	střední
kambria	kambrium	k1gNnSc2	kambrium
jsou	být	k5eAaImIp3nP	být
Archaeocyatha	Archaeocyatha	k1gMnSc1	Archaeocyatha
<g/>
.	.	kIx.	.
</s>
<s>
Vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
pevné	pevný	k2eAgFnPc4d1	pevná
vápnité	vápnitý	k2eAgFnPc4d1	vápnitá
schránky	schránka	k1gFnPc4	schránka
s	s	k7c7	s
kornoutovitou	kornoutovitý	k2eAgFnSc7d1	kornoutovitý
kostrou	kostra	k1gFnSc7	kostra
<g/>
,	,	kIx,	,
pevně	pevně	k6eAd1	pevně
přisedlé	přisedlý	k2eAgInPc4d1	přisedlý
k	k	k7c3	k
podkladu	podklad	k1gInSc3	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Místy	místy	k6eAd1	místy
tvořili	tvořit	k5eAaImAgMnP	tvořit
vápencové	vápencový	k2eAgFnPc4d1	vápencová
masy	masa	k1gFnPc4	masa
svědčící	svědčící	k2eAgFnPc4d1	svědčící
o	o	k7c6	o
mělkém	mělký	k2eAgNnSc6d1	mělké
a	a	k8xC	a
teplém	teplý	k2eAgNnSc6d1	teplé
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
rozvoj	rozvoj	k1gInSc1	rozvoj
ramenonožců	ramenonožec	k1gMnPc2	ramenonožec
(	(	kIx(	(
<g/>
Brachiopoda	Brachiopoda	k1gMnSc1	Brachiopoda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvořících	tvořící	k2eAgFnPc2d1	tvořící
asi	asi	k9	asi
30	[number]	k4	30
<g/>
%	%	kIx~	%
kambrijské	kambrijský	k2eAgFnSc2d1	kambrijská
fauny	fauna	k1gFnSc2	fauna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
bezzámkových	bezzámkův	k2eAgFnPc2d1	bezzámkův
forem	forma	k1gFnPc2	forma
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
Inarticulata	Inarticule	k1gNnPc4	Inarticule
s	s	k7c7	s
chitinovými	chitinový	k2eAgFnPc7d1	chitinová
schránkami	schránka	k1gFnPc7	schránka
(	(	kIx(	(
<g/>
Lingulella	Lingulella	k1gMnSc1	Lingulella
<g/>
,	,	kIx,	,
Obolella	Obolella	k1gMnSc1	Obolella
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
kambriu	kambrium	k1gNnSc6	kambrium
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
druhy	druh	k1gInPc1	druh
s	s	k7c7	s
dobře	dobře	k6eAd1	dobře
vyvinutým	vyvinutý	k2eAgInSc7d1	vyvinutý
zámkem	zámek	k1gInSc7	zámek
a	a	k8xC	a
vápnitými	vápnitý	k2eAgFnPc7d1	vápnitá
schránkami	schránka	k1gFnPc7	schránka
(	(	kIx(	(
<g/>
Bohemiella	Bohemiello	k1gNnPc4	Bohemiello
z	z	k7c2	z
českého	český	k2eAgNnSc2d1	české
kambria	kambrium	k1gNnSc2	kambrium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Echinodermata	Echinoderma	k1gNnPc1	Echinoderma
(	(	kIx(	(
<g/>
ostnokožci	ostnokožec	k1gMnPc1	ostnokožec
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
zastoupeni	zastoupit	k5eAaPmNgMnP	zastoupit
přisedlými	přisedlý	k2eAgFnPc7d1	přisedlá
primitivními	primitivní	k2eAgFnPc7d1	primitivní
lilijicemi	lilijice	k1gFnPc7	lilijice
(	(	kIx(	(
<g/>
Eocrinoidea	Eocrinoide	k2eAgFnSc1d1	Eocrinoide
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plochými	plochý	k2eAgInPc7d1	plochý
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
ležícími	ležící	k2eAgInPc7d1	ležící
či	či	k8xC	či
přisedlými	přisedlý	k2eAgInPc7d1	přisedlý
druhy	druh	k1gInPc7	druh
Edrioasteroidea	Edrioasteroide	k1gInSc2	Edrioasteroide
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Stromatocystites	Stromatocystites	k1gInSc1	Stromatocystites
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokládanými	pokládaný	k2eAgInPc7d1	pokládaný
za	za	k7c4	za
předchůdce	předchůdce	k1gMnPc4	předchůdce
hvězdic	hvězdice	k1gFnPc2	hvězdice
a	a	k8xC	a
ježovek	ježovka	k1gFnPc2	ježovka
<g/>
.	.	kIx.	.
</s>
<s>
Vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
měkkýši	měkkýš	k1gMnPc1	měkkýš
(	(	kIx(	(
<g/>
Mollusca	Mollusca	k1gMnSc1	Mollusca
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
diferencované	diferencovaný	k2eAgNnSc1d1	diferencované
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
hlavních	hlavní	k2eAgFnPc2d1	hlavní
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Nálezy	nález	k1gInPc1	nález
primitivních	primitivní	k2eAgMnPc2d1	primitivní
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
z	z	k7c2	z
Dálného	dálný	k2eAgInSc2d1	dálný
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnPc1d3	nejstarší
známí	známý	k2eAgMnPc1d1	známý
mlži	mlž	k1gMnPc1	mlž
jsou	být	k5eAaImIp3nP	být
rodu	rod	k1gInSc3	rod
Fordilla	Fordilla	k1gFnSc1	Fordilla
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Významnější	významný	k2eAgMnSc1d2	významnější
jsou	být	k5eAaImIp3nP	být
zástupci	zástupce	k1gMnPc1	zástupce
vyhynulé	vyhynulý	k2eAgFnSc2d1	vyhynulá
třídy	třída	k1gFnSc2	třída
Hyolithida	Hyolithida	k1gFnSc1	Hyolithida
(	(	kIx(	(
<g/>
hyolité	hyolitý	k2eAgNnSc1d1	hyolitý
<g/>
)	)	kIx)	)
s	s	k7c7	s
vápenatými	vápenatý	k2eAgFnPc7d1	vápenatá
kuželovitými	kuželovitý	k2eAgFnPc7d1	kuželovitá
schránkami	schránka	k1gFnPc7	schránka
<g/>
,	,	kIx,	,
uzavřenými	uzavřený	k2eAgNnPc7d1	uzavřené
víčky	víčko	k1gNnPc7	víčko
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgFnPc7d1	významná
paleontologickými	paleontologický	k2eAgFnPc7d1	paleontologická
lokalitami	lokalita	k1gFnPc7	lokalita
jsou	být	k5eAaImIp3nP	být
střednokambrijské	střednokambrijský	k2eAgFnSc2d1	střednokambrijský
burgesské	burgesský	k2eAgFnSc2d1	burgesský
břidlice	břidlice	k1gFnSc2	břidlice
v	v	k7c6	v
Britské	britský	k2eAgFnSc6d1	britská
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
(	(	kIx(	(
<g/>
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
s	s	k7c7	s
nálezy	nález	k1gInPc7	nález
velmi	velmi	k6eAd1	velmi
zachovalých	zachovalý	k2eAgFnPc2d1	zachovalá
zkamenělin	zkamenělina	k1gFnPc2	zkamenělina
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
měkkých	měkký	k2eAgFnPc2d1	měkká
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
např.	např.	kA	např.
údajný	údajný	k2eAgMnSc1d1	údajný
strunatec	strunatec	k1gMnSc1	strunatec
Pikaia	Pikaium	k1gNnSc2	Pikaium
<g/>
)	)	kIx)	)
a	a	k8xC	a
spodnokambrijské	spodnokambrijský	k2eAgFnPc1d1	spodnokambrijský
břidlice	břidlice	k1gFnPc1	břidlice
provincie	provincie	k1gFnSc2	provincie
Jün-nan	Jünana	k1gFnPc2	Jün-nana
(	(	kIx(	(
<g/>
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
zachované	zachovaný	k2eAgInPc1d1	zachovaný
zbytky	zbytek	k1gInPc1	zbytek
rostlin	rostlina	k1gFnPc2	rostlina
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
kambriu	kambrium	k1gNnSc6	kambrium
velmi	velmi	k6eAd1	velmi
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
mořské	mořský	k2eAgFnPc1d1	mořská
vápnité	vápnitý	k2eAgFnPc1d1	vápnitá
řasy	řasa	k1gFnPc1	řasa
a	a	k8xC	a
sinice	sinice	k1gFnPc1	sinice
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
nálezy	nález	k1gInPc1	nález
suchozemských	suchozemský	k2eAgFnPc2d1	suchozemská
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tzv.	tzv.	kA	tzv.
biomolekulárních	biomolekulární	k2eAgFnPc2d1	biomolekulární
hodin	hodina	k1gFnPc2	hodina
je	být	k5eAaImIp3nS	být
přítomnost	přítomnost	k1gFnSc1	přítomnost
primitivních	primitivní	k2eAgMnPc2d1	primitivní
zástupců	zástupce	k1gMnPc2	zástupce
na	na	k7c6	na
vlhkých	vlhký	k2eAgNnPc6d1	vlhké
místech	místo	k1gNnPc6	místo
souše	souš	k1gFnSc2	souš
již	již	k6eAd1	již
předpokládána	předpokládán	k2eAgFnSc1d1	předpokládána
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kambrium	kambrium	k1gNnSc1	kambrium
je	být	k5eAaImIp3nS	být
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
jak	jak	k8xS	jak
v	v	k7c6	v
mobilních	mobilní	k2eAgFnPc6d1	mobilní
geosynklinálních	geosynklinální	k2eAgFnPc6d1	geosynklinální
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
obklopujících	obklopující	k2eAgFnPc6d1	obklopující
sibiřský	sibiřský	k2eAgInSc4d1	sibiřský
a	a	k8xC	a
čínský	čínský	k2eAgInSc4d1	čínský
štít	štít	k1gInSc4	štít
(	(	kIx(	(
<g/>
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Altaj	Altaj	k1gInSc1	Altaj
<g/>
,	,	kIx,	,
Salair	Salair	k1gInSc1	Salair
<g/>
,	,	kIx,	,
Kašmír	Kašmír	k1gInSc1	Kašmír
<g/>
,	,	kIx,	,
Thajsko	Thajsko	k1gNnSc1	Thajsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
zaplaveném	zaplavený	k2eAgInSc6d1	zaplavený
mělkým	mělký	k2eAgNnSc7d1	mělké
epikontinentálním	epikontinentální	k2eAgNnSc7d1	epikontinentální
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Stratigrafický	Stratigrafický	k2eAgInSc1d1	Stratigrafický
význam	význam	k1gInSc1	význam
mají	mít	k5eAaImIp3nP	mít
sedimenty	sediment	k1gInPc1	sediment
na	na	k7c6	na
sibiřském	sibiřský	k2eAgInSc6d1	sibiřský
štítu	štít	k1gInSc6	štít
<g/>
,	,	kIx,	,
konkordantně	konkordantně	k6eAd1	konkordantně
uložené	uložený	k2eAgInPc1d1	uložený
na	na	k7c6	na
svrchním	svrchní	k2eAgNnSc6d1	svrchní
proterozoiku	proterozoikum	k1gNnSc6	proterozoikum
<g/>
.	.	kIx.	.
</s>
<s>
Nejspodnější	spodní	k2eAgFnSc1d3	nejspodnější
část	část	k1gFnSc1	část
kambrických	kambrický	k2eAgInPc2d1	kambrický
sedimentů	sediment	k1gInPc2	sediment
s	s	k7c7	s
četnými	četný	k2eAgInPc7d1	četný
nálezy	nález	k1gInPc7	nález
archeocyátů	archeocyát	k1gInPc2	archeocyát
a	a	k8xC	a
primitivních	primitivní	k2eAgMnPc2d1	primitivní
měkkýšů	měkkýš	k1gMnPc2	měkkýš
patří	patřit	k5eAaImIp3nS	patřit
stupni	stupeň	k1gInPc7	stupeň
tommot	tommota	k1gFnPc2	tommota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nadložních	nadložní	k2eAgFnPc6d1	nadložní
vrstvách	vrstva	k1gFnPc6	vrstva
se	se	k3xPyFc4	se
již	již	k6eAd1	již
objevují	objevovat	k5eAaImIp3nP	objevovat
první	první	k4xOgMnPc1	první
trilobiti	trilobit	k1gMnPc1	trilobit
(	(	kIx(	(
<g/>
Profallotaspis	Profallotaspis	k1gInSc1	Profallotaspis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
čínském	čínský	k2eAgInSc6d1	čínský
štítu	štít	k1gInSc6	štít
převládají	převládat	k5eAaImIp3nP	převládat
sedimenty	sediment	k1gInPc1	sediment
s	s	k7c7	s
faunou	fauna	k1gFnSc7	fauna
jihoasijsko-australské	jihoasijskoustralský	k2eAgFnSc2d1	jihoasijsko-australský
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
východním	východní	k2eAgInSc6d1	východní
okraji	okraj	k1gInSc6	okraj
patří	patřit	k5eAaImIp3nS	patřit
kambriu	kambrium	k1gNnSc3	kambrium
sedimenty	sediment	k1gInPc1	sediment
tasmánské	tasmánský	k2eAgFnSc2d1	tasmánská
mobilní	mobilní	k2eAgFnSc2d1	mobilní
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
uloženiny	uloženina	k1gFnPc1	uloženina
adelaidské	adelaidský	k2eAgFnPc4d1	adelaidský
mobilní	mobilní	k2eAgFnPc4d1	mobilní
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
leží	ležet	k5eAaImIp3nP	ležet
konkordantně	konkordantně	k6eAd1	konkordantně
na	na	k7c6	na
svrchním	svrchní	k2eAgNnSc6d1	svrchní
proterozoiku	proterozoikum	k1gNnSc6	proterozoikum
se	s	k7c7	s
známou	známý	k2eAgFnSc7d1	známá
ediakarskou	ediakarský	k2eAgFnSc7d1	ediakarská
faunou	fauna	k1gFnSc7	fauna
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgNnSc1d1	spodní
kambrium	kambrium	k1gNnSc1	kambrium
tvoří	tvořit	k5eAaImIp3nS	tvořit
vápence	vápenec	k1gInPc4	vápenec
s	s	k7c7	s
archeocyátovými	archeocyátův	k2eAgInPc7d1	archeocyátův
útesy	útes	k1gInPc7	útes
až	až	k9	až
200	[number]	k4	200
km	km	kA	km
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nadloží	nadloží	k1gNnSc6	nadloží
převládají	převládat	k5eAaImIp3nP	převládat
břidlice	břidlice	k1gFnPc4	břidlice
s	s	k7c7	s
hojnými	hojný	k2eAgMnPc7d1	hojný
trilobity	trilobit	k1gMnPc7	trilobit
<g/>
.	.	kIx.	.
</s>
<s>
Mocné	mocný	k2eAgInPc1d1	mocný
příkrovy	příkrov	k1gInPc1	příkrov
bazaltů	bazalt	k1gInPc2	bazalt
jsou	být	k5eAaImIp3nP	být
výsledkem	výsledek	k1gInSc7	výsledek
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
činnosti	činnost	k1gFnSc2	činnost
na	na	k7c6	na
konci	konec	k1gInSc6	konec
kambria	kambrium	k1gNnSc2	kambrium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
byly	být	k5eAaImAgInP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
archeocyátové	archeocyátový	k2eAgInPc1d1	archeocyátový
vápence	vápenec	k1gInPc1	vápenec
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Rossova	Rossův	k2eAgNnSc2d1	Rossovo
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
ve	v	k7c6	v
vrtech	vrt	k1gInPc6	vrt
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Weddellova	Weddellův	k2eAgNnSc2d1	Weddellovo
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Mobilní	mobilní	k2eAgFnPc1d1	mobilní
oblasti	oblast	k1gFnPc1	oblast
lemovaly	lemovat	k5eAaImAgFnP	lemovat
kanadský	kanadský	k2eAgInSc4d1	kanadský
štít	štít	k1gInSc4	štít
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
kordilérská	kordilérský	k2eAgFnSc1d1	kordilérský
(	(	kIx(	(
<g/>
Skalnaté	skalnatý	k2eAgFnPc1d1	skalnatá
hory	hora	k1gFnPc1	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
appalačská	appalačský	k2eAgFnSc1d1	appalačský
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc4	součást
Protoatlantiku	Protoatlantika	k1gFnSc4	Protoatlantika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
franklinská	franklinský	k2eAgFnSc1d1	franklinský
<g/>
.	.	kIx.	.
</s>
<s>
Sedimenty	sediment	k1gInPc1	sediment
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
mocnosti	mocnost	k1gFnPc1	mocnost
i	i	k9	i
přes	přes	k7c4	přes
5000	[number]	k4	5000
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
pískovce	pískovec	k1gInPc1	pískovec
<g/>
,	,	kIx,	,
vápence	vápenec	k1gInSc2	vápenec
a	a	k8xC	a
místy	místo	k1gNnPc7	místo
vulkanity	vulkanita	k1gFnSc2	vulkanita
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
Protoatlantiku	Protoatlantikum	k1gNnSc3	Protoatlantikum
byla	být	k5eAaImAgFnS	být
kadomská	kadomský	k2eAgFnSc1d1	kadomská
mobilní	mobilní	k2eAgFnSc1d1	mobilní
oblast	oblast	k1gFnSc1	oblast
v	v	k7c6	v
sz.	sz.	k?	sz.
Evropě	Evropa	k1gFnSc6	Evropa
sahající	sahající	k2eAgFnPc1d1	sahající
přes	přes	k7c4	přes
britské	britský	k2eAgInPc4d1	britský
ostrovy	ostrov	k1gInPc4	ostrov
do	do	k7c2	do
Norska	Norsko	k1gNnSc2	Norsko
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
sz.	sz.	k?	sz.
do	do	k7c2	do
východního	východní	k2eAgNnSc2d1	východní
Grónska	Grónsko	k1gNnSc2	Grónsko
a	a	k8xC	a
na	na	k7c4	na
Špicberky	Špicberky	k1gFnPc4	Špicberky
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgNnSc1d1	spodní
kambrium	kambrium	k1gNnSc1	kambrium
má	mít	k5eAaImIp3nS	mít
transgresivní	transgresivní	k2eAgInSc4d1	transgresivní
charakter	charakter	k1gInSc4	charakter
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
klastik	klastika	k1gFnPc2	klastika
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgNnSc1d1	střední
kambrium	kambrium	k1gNnSc1	kambrium
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
peliticko-karbonátické	pelitickoarbonátický	k2eAgInPc1d1	peliticko-karbonátický
sedimenty	sediment	k1gInPc1	sediment
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svrchním	svrchní	k2eAgNnSc6d1	svrchní
kambriu	kambrium	k1gNnSc6	kambrium
znovu	znovu	k6eAd1	znovu
písčité	písčitý	k2eAgFnSc2d1	písčitá
uloženiny	uloženina	k1gFnSc2	uloženina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
sedimenty	sediment	k1gInPc1	sediment
kambria	kambrium	k1gNnSc2	kambrium
mocnosti	mocnost	k1gFnSc2	mocnost
až	až	k9	až
5	[number]	k4	5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
armorickém	armorický	k2eAgInSc6d1	armorický
masívu	masív	k1gInSc6	masív
(	(	kIx(	(
<g/>
sz.	sz.	k?	sz.
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
sedimentace	sedimentace	k1gFnSc1	sedimentace
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
kyselým	kyselý	k2eAgInSc7d1	kyselý
vulkanismem	vulkanismus	k1gInSc7	vulkanismus
(	(	kIx(	(
<g/>
ryolity	ryolita	k1gFnSc2	ryolita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kambrium	kambrium	k1gNnSc1	kambrium
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
z	z	k7c2	z
jižního	jižní	k2eAgNnSc2d1	jižní
a	a	k8xC	a
středního	střední	k2eAgNnSc2d1	střední
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
,	,	kIx,	,
ostrova	ostrov	k1gInSc2	ostrov
Bornholm	Bornholma	k1gFnPc2	Bornholma
a	a	k8xC	a
pobaltských	pobaltský	k2eAgFnPc2d1	pobaltská
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
sedimenty	sediment	k1gInPc1	sediment
mělkého	mělký	k2eAgNnSc2d1	mělké
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
písčité	písčitý	k2eAgNnSc4d1	písčité
(	(	kIx(	(
<g/>
spodní	spodní	k2eAgNnSc4d1	spodní
kambrium	kambrium	k1gNnSc4	kambrium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
je	být	k5eAaImIp3nS	být
leningradský	leningradský	k2eAgInSc1d1	leningradský
modrý	modrý	k2eAgInSc1d1	modrý
jíl	jíl	k1gInSc1	jíl
-	-	kIx~	-
vrstva	vrstva	k1gFnSc1	vrstva
asi	asi	k9	asi
80	[number]	k4	80
m	m	kA	m
mocná	mocný	k2eAgFnSc1d1	mocná
s	s	k7c7	s
hyolity	hyolit	k1gInPc7	hyolit
<g/>
,	,	kIx,	,
spórami	spóra	k1gFnPc7	spóra
a	a	k8xC	a
gastropody	gastropod	k1gInPc7	gastropod
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
kambriu	kambrium	k1gNnSc6	kambrium
převládají	převládat	k5eAaImIp3nP	převládat
břidlice	břidlice	k1gFnPc1	břidlice
bohaté	bohatý	k2eAgFnSc2d1	bohatá
pyritem	pyrit	k1gInSc7	pyrit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Osla	Oslo	k1gNnSc2	Oslo
lze	lze	k6eAd1	lze
sledovat	sledovat	k5eAaImF	sledovat
vývoj	vývoj	k1gInSc4	vývoj
trilobitových	trilobitový	k2eAgFnPc2d1	trilobitová
faun	fauna	k1gFnPc2	fauna
<g/>
,	,	kIx,	,
umožňujících	umožňující	k2eAgInPc2d1	umožňující
podrobné	podrobný	k2eAgNnSc4d1	podrobné
zonální	zonální	k2eAgNnSc4d1	zonální
členění	členění	k1gNnSc4	členění
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kambriu	kambrium	k1gNnSc3	kambrium
patří	patřit	k5eAaImIp3nP	patřit
sedimenty	sediment	k1gInPc1	sediment
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
(	(	kIx(	(
<g/>
Iberská	iberský	k2eAgFnSc1d1	iberská
meseta	meseta	k1gFnSc1	meseta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc4	Francie
(	(	kIx(	(
<g/>
Montage	Montagus	k1gMnSc5	Montagus
Noire	Noir	k1gMnSc5	Noir
<g/>
,	,	kIx,	,
Pyreneje	Pyreneje	k1gFnPc1	Pyreneje
<g/>
,	,	kIx,	,
Normandie	Normandie	k1gFnPc1	Normandie
<g/>
,	,	kIx,	,
Ardeny	Ardeny	k1gFnPc1	Ardeny
<g/>
,	,	kIx,	,
oblasti	oblast	k1gFnPc1	oblast
kolem	kolem	k7c2	kolem
řeky	řeka	k1gFnSc2	řeka
Loiry	Loira	k1gFnSc2	Loira
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sardinii	Sardinie	k1gFnSc6	Sardinie
<g/>
,	,	kIx,	,
Maroku	Maroko	k1gNnSc6	Maroko
(	(	kIx(	(
<g/>
Antiatlas	Antiatlas	k1gMnSc1	Antiatlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spodním	spodní	k2eAgNnSc6d1	spodní
kambriu	kambrium	k1gNnSc6	kambrium
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
klastika	klastika	k1gFnSc1	klastika
(	(	kIx(	(
<g/>
křemence	křemenec	k1gInPc1	křemenec
<g/>
,	,	kIx,	,
arkózy	arkóza	k1gFnPc1	arkóza
<g/>
)	)	kIx)	)
s	s	k7c7	s
karbonáty	karbonát	k1gInPc7	karbonát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
kambriu	kambrium	k1gNnSc6	kambrium
převládají	převládat	k5eAaImIp3nP	převládat
břidlice	břidlice	k1gFnPc1	břidlice
s	s	k7c7	s
trilobitovou	trilobitový	k2eAgFnSc7d1	trilobitová
faunou	fauna	k1gFnSc7	fauna
blízkou	blízký	k2eAgFnSc7d1	blízká
českému	český	k2eAgNnSc3d1	české
středními	střední	k2eAgInPc7d1	střední
kambriu	kambrium	k1gNnSc3	kambrium
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svrchním	svrchní	k2eAgNnSc6d1	svrchní
kambriu	kambrium	k1gNnSc6	kambrium
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
podíl	podíl	k1gInSc4	podíl
písčitých	písčitý	k2eAgInPc2d1	písčitý
(	(	kIx(	(
<g/>
flyšoidních	flyšoidní	k2eAgInPc2d1	flyšoidní
<g/>
)	)	kIx)	)
sedimentů	sediment	k1gInPc2	sediment
s	s	k7c7	s
faunou	fauna	k1gFnSc7	fauna
blízkou	blízký	k2eAgFnSc7d1	blízká
pacifické	pacifický	k2eAgFnSc6d1	Pacifická
provincii	provincie	k1gFnSc6	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
Prototethysu	Prototethys	k1gInSc3	Prototethys
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c6	na
oblasti	oblast	k1gFnSc6	oblast
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
na	na	k7c4	na
východ	východ	k1gInSc4	východ
až	až	k9	až
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Sedimenty	sediment	k1gInPc1	sediment
kambria	kambrium	k1gNnSc2	kambrium
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Barrandienu	barrandien	k1gInSc2	barrandien
leží	ležet	k5eAaImIp3nS	ležet
diskordantně	diskordantně	k6eAd1	diskordantně
na	na	k7c6	na
kadomsky	kadomsky	k6eAd1	kadomsky
zvrásněném	zvrásněný	k2eAgNnSc6d1	zvrásněné
proterozoiku	proterozoikum	k1gNnSc6	proterozoikum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c6	v
tektonicky	tektonicky	k6eAd1	tektonicky
predisponované	predisponovaný	k2eAgFnSc6d1	predisponovaná
kadomské	kadomský	k2eAgFnSc6d1	kadomská
depresi	deprese	k1gFnSc6	deprese
v	v	k7c6	v
brdské	brdský	k2eAgFnSc6d1	Brdská
(	(	kIx(	(
<g/>
příbramsko-jinecké	příbramskoinecký	k2eAgFnSc6d1	příbramsko-jinecký
<g/>
)	)	kIx)	)
a	a	k8xC	a
skryjsko-týřovické	skryjskoýřovický	k2eAgFnPc4d1	skryjsko-týřovický
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brdském	brdský	k2eAgNnSc6d1	Brdské
kambriu	kambrium	k1gNnSc6	kambrium
se	se	k3xPyFc4	se
ve	v	k7c6	v
spodním	spodní	k2eAgNnSc6d1	spodní
kambriu	kambrium	k1gNnSc6	kambrium
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
kontinentální	kontinentální	k2eAgInPc1d1	kontinentální
sedimenty	sediment	k1gInPc1	sediment
mocné	mocný	k2eAgInPc1d1	mocný
až	až	k9	až
3000	[number]	k4	3000
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
aluviální	aluviální	k2eAgInPc4d1	aluviální
kužely	kužel	k1gInPc4	kužel
<g/>
,	,	kIx,	,
nivy	niva	k1gFnPc1	niva
<g/>
,	,	kIx,	,
řečiště	řečiště	k1gNnPc1	řečiště
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
kambriu	kambrium	k1gNnSc6	kambrium
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
mořské	mořský	k2eAgFnSc3d1	mořská
transgresi	transgrese	k1gFnSc3	transgrese
(	(	kIx(	(
<g/>
sedimenty	sediment	k1gInPc4	sediment
až	až	k9	až
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
mocné	mocný	k2eAgFnPc1d1	mocná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
ve	v	k7c6	v
svrchním	svrchní	k2eAgNnSc6d1	svrchní
kambriu	kambrium	k1gNnSc6	kambrium
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
regresní	regresní	k2eAgInPc1d1	regresní
sedimenty	sediment	k1gInPc1	sediment
a	a	k8xC	a
četné	četný	k2eAgInPc1d1	četný
vulkanity	vulkanit	k1gInPc1	vulkanit
<g/>
.	.	kIx.	.
</s>
<s>
Průřez	průřez	k1gInSc1	průřez
sedimenty	sediment	k1gInPc1	sediment
<g/>
:	:	kIx,	:
spodní	spodní	k2eAgNnSc4d1	spodní
kambrium	kambrium	k1gNnSc4	kambrium
žitecko-hlubošské	žiteckolubošský	k2eAgNnSc1d1	žitecko-hlubošský
souvrství	souvrství	k1gNnSc1	souvrství
(	(	kIx(	(
<g/>
žitecké	žitecký	k2eAgInPc1d1	žitecký
slepence	slepenec	k1gInPc1	slepenec
<g/>
,	,	kIx,	,
hlubošské	hlubošský	k2eAgInPc1d1	hlubošský
slepence	slepenec	k1gInPc1	slepenec
<g/>
)	)	kIx)	)
sádecké	sádecký	k2eAgNnSc1d1	sádecké
souvrství	souvrství	k1gNnSc1	souvrství
(	(	kIx(	(
<g/>
pískovce	pískovec	k1gInPc1	pískovec
<g/>
,	,	kIx,	,
droby	droba	k1gFnPc1	droba
<g/>
)	)	kIx)	)
holšinsko-hořické	holšinskoořický	k2eAgNnSc1d1	holšinsko-hořický
souvrství	souvrství	k1gNnSc1	souvrství
(	(	kIx(	(
<g/>
holšinské	holšinský	k2eAgInPc1d1	holšinský
slepence	slepenec	k1gInPc1	slepenec
<g/>
,	,	kIx,	,
hořické	hořický	k2eAgInPc1d1	hořický
pískovce	pískovec	k1gInPc1	pískovec
<g/>
,	,	kIx,	,
pasecké	pasecký	k2eAgFnPc1d1	pasecký
břidlice	břidlice	k1gFnPc1	břidlice
se	s	k7c7	s
zbytky	zbytek	k1gInPc7	zbytek
členovce	členovec	k1gMnSc2	členovec
Kodymirus	Kodymirus	k1gInSc1	Kodymirus
vagans	vagans	k6eAd1	vagans
<g/>
,	,	kIx,	,
považované	považovaný	k2eAgNnSc1d1	považované
<g />
.	.	kIx.	.
</s>
<s>
za	za	k7c4	za
nejstarší	starý	k2eAgFnSc4d3	nejstarší
českou	český	k2eAgFnSc4d1	Česká
živočišnou	živočišný	k2eAgFnSc4d1	živočišná
fosílii	fosílie	k1gFnSc4	fosílie
<g/>
)	)	kIx)	)
kloučecko-čenkovské	kloučecko-čenkovský	k2eAgNnSc1d1	kloučecko-čenkovský
souvrství	souvrství	k1gNnSc1	souvrství
(	(	kIx(	(
<g/>
kloučecké	kloučecký	k2eAgInPc1d1	kloučecký
slepence	slepenec	k1gInPc1	slepenec
<g/>
,	,	kIx,	,
čenkovské	čenkovský	k2eAgInPc1d1	čenkovský
pískovce	pískovec	k1gInPc1	pískovec
<g/>
)	)	kIx)	)
chumavsko-baštinské	chumavskoaštinský	k2eAgNnSc1d1	chumavsko-baštinský
souvrství	souvrství	k1gNnSc1	souvrství
(	(	kIx(	(
<g/>
slepence	slepenec	k1gInPc1	slepenec
<g/>
,	,	kIx,	,
pískovce	pískovec	k1gInPc1	pískovec
<g/>
)	)	kIx)	)
střední	střední	k2eAgNnSc4d1	střední
kambrium	kambrium	k1gNnSc4	kambrium
jinecké	jinecký	k2eAgNnSc1d1	jinecké
souvrství	souvrství	k1gNnSc1	souvrství
(	(	kIx(	(
<g/>
břidlice	břidlice	k1gFnPc1	břidlice
<g/>
,	,	kIx,	,
pískovce	pískovec	k1gInPc1	pískovec
<g/>
,	,	kIx,	,
slepence	slepenec	k1gInPc1	slepenec
<g/>
,	,	kIx,	,
faunisticky	faunisticky	k6eAd1	faunisticky
bohaté	bohatý	k2eAgInPc1d1	bohatý
mořské	mořský	k2eAgInPc1d1	mořský
sedimenty	sediment	k1gInPc1	sediment
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
ohrazenické	ohrazenický	k2eAgNnSc1d1	ohrazenický
souvrství	souvrství	k1gNnSc1	souvrství
(	(	kIx(	(
<g/>
slepence	slepenec	k1gInPc1	slepenec
<g/>
)	)	kIx)	)
svrchní	svrchní	k2eAgNnSc4d1	svrchní
kambrium	kambrium	k1gNnSc4	kambrium
pavlovské	pavlovský	k2eAgNnSc1d1	pavlovské
souvrství	souvrství	k1gNnSc1	souvrství
(	(	kIx(	(
<g/>
pavlovské	pavlovský	k2eAgInPc1d1	pavlovský
slepence	slepenec	k1gInPc1	slepenec
<g/>
,	,	kIx,	,
strašické	strašický	k2eAgInPc1d1	strašický
vulkanity	vulkanit	k1gInPc1	vulkanit
-	-	kIx~	-
andezity	andezit	k1gInPc1	andezit
<g/>
,	,	kIx,	,
ryolity	ryolit	k1gInPc1	ryolit
<g/>
,	,	kIx,	,
bazalty	bazalt	k1gInPc1	bazalt
<g/>
)	)	kIx)	)
střední	střední	k2eAgNnSc1d1	střední
kambrium	kambrium	k1gNnSc1	kambrium
milečské	milečský	k2eAgInPc1d1	milečský
slepence	slepenec	k1gInPc1	slepenec
týřovické	týřovický	k2eAgInPc1d1	týřovický
slepence	slepenec	k1gInPc1	slepenec
skryjské	skryjský	k2eAgFnSc2d1	skryjský
paradoxidové	paradoxidový	k2eAgFnSc2d1	paradoxidový
břidlice	břidlice	k1gFnSc2	břidlice
s	s	k7c7	s
hojnou	hojný	k2eAgFnSc7d1	hojná
trilobitovou	trilobitový	k2eAgFnSc7d1	trilobitová
faunou	fauna	k1gFnSc7	fauna
vosnické	vosnický	k2eAgInPc1d1	vosnický
slepence	slepenec	k1gInPc1	slepenec
střední-svrchní	střednívrchnět	k5eAaPmIp3nP	střední-svrchnět
kambrium	kambrium	k1gNnSc4	kambrium
-	-	kIx~	-
vulkanity	vulkanita	k1gFnSc2	vulkanita
křivoklátsko-rokycanského	křivoklátskookycanský	k2eAgInSc2d1	křivoklátsko-rokycanský
komplexu	komplex	k1gInSc2	komplex
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
andezity	andezit	k1gInPc1	andezit
<g/>
,	,	kIx,	,
dacity	dacit	k1gInPc1	dacit
<g/>
,	,	kIx,	,
ryolity	ryolit	k1gInPc1	ryolit
<g/>
)	)	kIx)	)
až	až	k9	až
1000	[number]	k4	1000
metrů	metr	k1gInPc2	metr
mocné	mocný	k2eAgFnSc2d1	mocná
Kambriu	kambrium	k1gNnSc6	kambrium
patří	patřit	k5eAaImIp3nP	patřit
senické	senický	k2eAgFnPc1d1	senická
vrstvy	vrstva	k1gFnPc1	vrstva
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
oblast	oblast	k1gFnSc1	oblast
kolem	kolem	k7c2	kolem
Heřmanova	Heřmanův	k2eAgInSc2d1	Heřmanův
Městce	Městec	k1gInSc2	Městec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
droby	drob	k1gInPc1	drob
a	a	k8xC	a
pískovce	pískovec	k1gInPc1	pískovec
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
do	do	k7c2	do
podloží	podloží	k1gNnSc2	podloží
české	český	k2eAgFnSc2d1	Česká
křídové	křídový	k2eAgFnSc2d1	křídová
tabule	tabule	k1gFnSc2	tabule
V	v	k7c6	v
Ašském	ašský	k2eAgInSc6d1	ašský
výběžku	výběžek	k1gInSc6	výběžek
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupen	k2eAgFnPc1d1	zastoupena
horniny	hornina	k1gFnPc1	hornina
arzberské	arzberský	k2eAgFnSc2d1	arzberský
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
problematického	problematický	k2eAgNnSc2d1	problematické
stáří	stáří	k1gNnSc2	stáří
(	(	kIx(	(
<g/>
svrchní	svrchní	k2eAgNnSc1d1	svrchní
proterozoikum	proterozoikum	k1gNnSc1	proterozoikum
-	-	kIx~	-
spodní	spodní	k2eAgNnSc1d1	spodní
kambrium	kambrium	k1gNnSc1	kambrium
<g/>
)	)	kIx)	)
<g/>
s	s	k7c7	s
fylity	fylit	k1gInPc7	fylit
a	a	k8xC	a
svory	svor	k1gInPc7	svor
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
nálezů	nález	k1gInPc2	nález
fosílií	fosílie	k1gFnPc2	fosílie
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kambriu	kambrium	k1gNnSc3	kambrium
jsou	být	k5eAaImIp3nP	být
řazeny	řazen	k2eAgInPc1d1	řazen
bez	bez	k7c2	bez
paleontologického	paleontologický	k2eAgNnSc2d1	paleontologické
doložení	doložení	k1gNnSc2	doložení
svory	svora	k1gFnSc2	svora
klínovecké	klínovecký	k2eAgFnSc2d1	Klínovecká
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedický	encyklopedický	k2eAgInSc1d1	encyklopedický
slovník	slovník	k1gInSc1	slovník
geologických	geologický	k2eAgFnPc2d1	geologická
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
A-	A-	k1gFnPc2	A-
<g/>
M.	M.	kA	M.
ACADEMIA	academia	k1gFnSc1	academia
Praha	Praha	k1gFnSc1	Praha
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Mísař	Mísař	k1gMnSc1	Mísař
Z.	Z.	kA	Z.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Geologie	geologie	k1gFnSc2	geologie
ČSSR	ČSSR	kA	ČSSR
I	I	kA	I
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
masív	masív	k1gInSc1	masív
<g/>
.	.	kIx.	.
</s>
<s>
SPN	SPN	kA	SPN
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Mišík	Mišík	k1gMnSc1	Mišík
M.	M.	kA	M.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Stratigrafická	Stratigrafický	k2eAgFnSc1d1	Stratigrafická
a	a	k8xC	a
historická	historický	k2eAgFnSc1d1	historická
geológia	geológia	k1gFnSc1	geológia
<g/>
.	.	kIx.	.
</s>
<s>
SPN	SPN	kA	SPN
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Špinar	Špinar	k1gMnSc1	Špinar
Z.V.	Z.V.	k1gMnSc1	Z.V.
<g/>
,	,	kIx,	,
Burian	Burian	k1gMnSc1	Burian
Z.	Z.	kA	Z.
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
pravěku	pravěk	k1gInSc6	pravěk
<g/>
.	.	kIx.	.
</s>
<s>
AVENTINUM	AVENTINUM	kA	AVENTINUM
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Kambrická	kambrický	k2eAgFnSc1d1	kambrická
exploze	exploze	k1gFnSc1	exploze
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kambrium	kambrium	k1gNnSc4	kambrium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
