<s>
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
má	mít	k5eAaImIp3nS	mít
mírné	mírný	k2eAgNnSc1d1	mírné
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
střídají	střídat	k5eAaImIp3nP	střídat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
čtyři	čtyři	k4xCgNnPc4	čtyři
roční	roční	k2eAgNnPc4d1	roční
období	období	k1gNnPc4	období
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
rozloženy	rozložit	k5eAaPmNgFnP	rozložit
srážky	srážka	k1gFnPc1	srážka
<g/>
.	.	kIx.	.
</s>
