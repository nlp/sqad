<p>
<s>
Let	let	k1gInSc1	let
US	US	kA	US
Airways	Airways	k1gInSc1	Airways
č.	č.	k?	č.
1549	[number]	k4	1549
byl	být	k5eAaImAgInS	být
každodenní	každodenní	k2eAgInSc1d1	každodenní
komerční	komerční	k2eAgInSc1d1	komerční
let	let	k1gInSc1	let
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
LaGuardia	LaGuardium	k1gNnSc2	LaGuardium
Airport	Airport	k1gInSc1	Airport
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
–	–	k?	–
Charlotte	Charlott	k1gInSc5	Charlott
–	–	k?	–
Seattle	Seattle	k1gFnSc2	Seattle
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
asi	asi	k9	asi
šest	šest	k4xCc1	šest
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
startu	start	k1gInSc6	start
nouzově	nouzově	k6eAd1	nouzově
přistál	přistát	k5eAaPmAgMnS	přistát
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Hudson	Hudsona	k1gFnPc2	Hudsona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc4	průběh
nehody	nehoda	k1gFnSc2	nehoda
==	==	k?	==
</s>
</p>
<p>
<s>
Airbus	airbus	k1gInSc1	airbus
A320	A320	k1gFnSc2	A320
se	s	k7c7	s
150	[number]	k4	150
pasažéry	pasažér	k1gMnPc7	pasažér
a	a	k8xC	a
5	[number]	k4	5
členy	člen	k1gMnPc7	člen
posádky	posádka	k1gFnSc2	posádka
se	se	k3xPyFc4	se
zvedl	zvednout	k5eAaPmAgMnS	zvednout
z	z	k7c2	z
dráhy	dráha	k1gFnSc2	dráha
04	[number]	k4	04
letiště	letiště	k1gNnSc2	letiště
La	la	k1gNnSc2	la
Guardia	Guardia	k?	Guardia
v	v	k7c6	v
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
<g/>
:	:	kIx,	:
<g/>
34	[number]	k4	34
<g/>
.	.	kIx.	.
</s>
<s>
Minutu	minuta	k1gFnSc4	minuta
a	a	k8xC	a
půl	půl	k1xP	půl
po	po	k7c6	po
startu	start	k1gInSc6	start
<g/>
,	,	kIx,	,
v	v	k7c6	v
čase	čas	k1gInSc6	čas
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
27	[number]	k4	27
<g/>
:	:	kIx,	:
<g/>
11	[number]	k4	11
se	se	k3xPyFc4	se
letoun	letoun	k1gInSc1	letoun
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
hejna	hejno	k1gNnSc2	hejno
kanadských	kanadský	k2eAgFnPc2d1	kanadská
hus	husa	k1gFnPc2	husa
(	(	kIx(	(
<g/>
Bernešek	berneška	k1gFnPc2	berneška
velkých	velký	k2eAgFnPc2d1	velká
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nasátí	nasátí	k1gNnSc2	nasátí
jejich	jejich	k3xOp3gNnPc2	jejich
těl	tělo	k1gNnPc2	tělo
značně	značně	k6eAd1	značně
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
oběma	dva	k4xCgMnPc7	dva
motorům	motor	k1gInPc3	motor
tah	tah	k1gInSc1	tah
<g/>
.	.	kIx.	.
</s>
<s>
Otáčky	otáčka	k1gFnPc1	otáčka
N1	N1	k1gFnSc2	N1
prvního	první	k4xOgInSc2	první
motoru	motor	k1gInSc2	motor
klesly	klesnout	k5eAaPmAgFnP	klesnout
z	z	k7c2	z
82	[number]	k4	82
<g/>
%	%	kIx~	%
na	na	k7c4	na
36	[number]	k4	36
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
u	u	k7c2	u
druhého	druhý	k4xOgInSc2	druhý
motoru	motor	k1gInSc2	motor
až	až	k9	až
na	na	k7c4	na
16	[number]	k4	16
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
teplota	teplota	k1gFnSc1	teplota
výstupních	výstupní	k2eAgInPc2d1	výstupní
plynů	plyn	k1gInPc2	plyn
motorů	motor	k1gInPc2	motor
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
blížit	blížit	k5eAaImF	blížit
k	k	k7c3	k
horní	horní	k2eAgFnSc3d1	horní
hraniční	hraniční	k2eAgFnSc3d1	hraniční
hodnotě	hodnota	k1gFnSc3	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
Chesley	Cheslea	k1gFnSc2	Cheslea
Sullenberger	Sullenberger	k1gMnSc1	Sullenberger
zapnul	zapnout	k5eAaPmAgMnS	zapnout
pomocnou	pomocný	k2eAgFnSc4d1	pomocná
energetickou	energetický	k2eAgFnSc4d1	energetická
jednotku	jednotka	k1gFnSc4	jednotka
APU	APU	kA	APU
<g/>
,	,	kIx,	,
převzal	převzít	k5eAaPmAgInS	převzít
řízení	řízení	k1gNnSc4	řízení
letadla	letadlo	k1gNnSc2	letadlo
a	a	k8xC	a
požádal	požádat	k5eAaPmAgMnS	požádat
prvního	první	k4xOgMnSc4	první
důstojníka	důstojník	k1gMnSc4	důstojník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
našel	najít	k5eAaPmAgMnS	najít
v	v	k7c6	v
příručce	příručka	k1gFnSc6	příručka
(	(	kIx(	(
<g/>
QRH	QRH	kA	QRH
–	–	k?	–
quick	quick	k1gMnSc1	quick
reference	reference	k1gFnSc2	reference
handbook	handbook	k1gInSc1	handbook
<g/>
)	)	kIx)	)
postup	postup	k1gInSc1	postup
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
případ	případ	k1gInSc4	případ
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
poněkud	poněkud	k6eAd1	poněkud
nešťastně	šťastně	k6eNd1	šťastně
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
postup	postup	k1gInSc4	postup
pro	pro	k7c4	pro
výpadek	výpadek	k1gInSc4	výpadek
obou	dva	k4xCgInPc2	dva
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
provedl	provést	k5eAaPmAgMnS	provést
jejich	jejich	k3xOp3gInSc4	jejich
restart	restart	k1gInSc4	restart
<g/>
.	.	kIx.	.
</s>
<s>
Motor	motor	k1gInSc1	motor
č.	č.	k?	č.
2	[number]	k4	2
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
nepodařilo	podařit	k5eNaPmAgNnS	podařit
nahodit	nahodit	k5eAaPmF	nahodit
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
motor	motor	k1gInSc1	motor
č.	č.	k?	č.
1	[number]	k4	1
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
rozběhl	rozběhnout	k5eAaPmAgMnS	rozběhnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
otáčky	otáčka	k1gFnPc1	otáčka
již	již	k6eAd1	již
nedosáhly	dosáhnout	k5eNaPmAgFnP	dosáhnout
předchozích	předchozí	k2eAgFnPc2d1	předchozí
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
ovšem	ovšem	k9	ovšem
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dalece	dalece	k?	dalece
to	ten	k3xDgNnSc1	ten
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
průběh	průběh	k1gInSc4	průběh
dalších	další	k2eAgFnPc2d1	další
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
kapitán	kapitán	k1gMnSc1	kapitán
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
stav	stav	k1gInSc4	stav
nouze	nouze	k1gFnSc2	nouze
a	a	k8xC	a
provedl	provést	k5eAaPmAgInS	provést
levou	levý	k2eAgFnSc4d1	levá
zatáčku	zatáčka	k1gFnSc4	zatáčka
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
bránící	bránící	k2eAgInSc1d1	bránící
bezpečnému	bezpečný	k2eAgInSc3d1	bezpečný
návratu	návrat	k1gInSc3	návrat
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
LaGuardia	LaGuardium	k1gNnSc2	LaGuardium
i	i	k8xC	i
přistání	přistání	k1gNnSc2	přistání
na	na	k7c6	na
bližším	blízký	k2eAgNnSc6d2	bližší
letišti	letiště	k1gNnSc6	letiště
Teterboro	Teterbora	k1gFnSc5	Teterbora
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
nouzové	nouzový	k2eAgNnSc4d1	nouzové
přistání	přistání	k1gNnSc4	přistání
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Hudson	Hudsona	k1gFnPc2	Hudsona
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
také	také	k9	také
v	v	k7c6	v
rychlosti	rychlost	k1gFnSc6	rychlost
okolo	okolo	k7c2	okolo
230	[number]	k4	230
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
(	(	kIx(	(
<g/>
125	[number]	k4	125
KIAS	KIAS	kA	KIAS
<g/>
)	)	kIx)	)
v	v	k7c6	v
čase	čas	k1gInSc6	čas
15	[number]	k4	15
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
:	:	kIx,	:
<g/>
44	[number]	k4	44
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
:	:	kIx,	:
<g/>
44	[number]	k4	44
UTC	UTC	kA	UTC
<g/>
)	)	kIx)	)
úspěšně	úspěšně	k6eAd1	úspěšně
provedl	provést	k5eAaPmAgMnS	provést
<g/>
.	.	kIx.	.
<g/>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
extrémně	extrémně	k6eAd1	extrémně
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
a	a	k8xC	a
náročný	náročný	k2eAgInSc1d1	náročný
manévr	manévr	k1gInSc1	manévr
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
A320	A320	k1gFnSc1	A320
má	mít	k5eAaImIp3nS	mít
motory	motor	k1gInPc4	motor
umístěné	umístěný	k2eAgInPc4d1	umístěný
v	v	k7c6	v
gondolách	gondola	k1gFnPc6	gondola
pod	pod	k7c7	pod
křídly	křídlo	k1gNnPc7	křídlo
a	a	k8xC	a
při	při	k7c6	při
dosednutí	dosednutí	k1gNnSc6	dosednutí
na	na	k7c4	na
hladinu	hladina	k1gFnSc4	hladina
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	k9	aby
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
narazily	narazit	k5eAaPmAgFnP	narazit
na	na	k7c4	na
hladinu	hladina	k1gFnSc4	hladina
současně	současně	k6eAd1	současně
a	a	k8xC	a
pod	pod	k7c7	pod
správným	správný	k2eAgInSc7d1	správný
úhlem	úhel	k1gInSc7	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
pilota	pilot	k1gMnSc2	pilot
poněkud	poněkud	k6eAd1	poněkud
usnadnila	usnadnit	k5eAaPmAgFnS	usnadnit
klidná	klidný	k2eAgFnSc1d1	klidná
hladina	hladina	k1gFnSc1	hladina
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
příznivé	příznivý	k2eAgFnSc2d1	příznivá
meteorologické	meteorologický	k2eAgFnSc2d1	meteorologická
podmínky	podmínka	k1gFnSc2	podmínka
a	a	k8xC	a
snad	snad	k9	snad
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
letoun	letoun	k1gInSc1	letoun
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
poměrně	poměrně	k6eAd1	poměrně
propracovaným	propracovaný	k2eAgInSc7d1	propracovaný
systémem	systém	k1gInSc7	systém
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pilot	pilot	k1gMnSc1	pilot
mohl	moct	k5eAaImAgMnS	moct
část	část	k1gFnSc4	část
práce	práce	k1gFnSc2	práce
přenechat	přenechat	k5eAaPmF	přenechat
palubním	palubní	k2eAgMnPc3d1	palubní
počítačům	počítač	k1gMnPc3	počítač
a	a	k8xC	a
soustředit	soustředit	k5eAaPmF	soustředit
se	se	k3xPyFc4	se
jen	jen	k9	jen
na	na	k7c4	na
základní	základní	k2eAgInPc4d1	základní
parametry	parametr	k1gInPc4	parametr
přistání	přistání	k1gNnSc2	přistání
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k9	tak
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
naprosto	naprosto	k6eAd1	naprosto
ojedinělý	ojedinělý	k2eAgInSc4d1	ojedinělý
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
při	při	k7c6	při
přistání	přistání	k1gNnSc6	přistání
letadla	letadlo	k1gNnSc2	letadlo
podobné	podobný	k2eAgFnPc1d1	podobná
konstrukce	konstrukce	k1gFnPc1	konstrukce
na	na	k7c4	na
vodní	vodní	k2eAgFnSc4d1	vodní
hladinu	hladina	k1gFnSc4	hladina
nedošlo	dojít	k5eNaPmAgNnS	dojít
ke	k	k7c3	k
ztrátám	ztráta	k1gFnPc3	ztráta
na	na	k7c6	na
lidských	lidský	k2eAgInPc6d1	lidský
životech	život	k1gInPc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
letoun	letoun	k1gInSc1	letoun
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
nárazu	náraz	k1gInSc6	náraz
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
ztratil	ztratit	k5eAaPmAgInS	ztratit
jeden	jeden	k4xCgInSc1	jeden
motor	motor	k1gInSc1	motor
a	a	k8xC	a
konstrukce	konstrukce	k1gFnSc1	konstrukce
draku	drak	k1gInSc2	drak
se	se	k3xPyFc4	se
natolik	natolik	k6eAd1	natolik
zdeformovala	zdeformovat	k5eAaPmAgFnS	zdeformovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
odepsán	odepsat	k5eAaPmNgInS	odepsat
a	a	k8xC	a
pojišťovací	pojišťovací	k2eAgFnSc1d1	pojišťovací
společnost	společnost	k1gFnSc1	společnost
ho	on	k3xPp3gInSc4	on
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
v	v	k7c6	v
internetové	internetový	k2eAgFnSc6d1	internetová
aukci	aukce	k1gFnSc6	aukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
cestující	cestující	k1gMnPc1	cestující
i	i	k8xC	i
členové	člen	k1gMnPc1	člen
posádky	posádka	k1gFnSc2	posádka
přistání	přistání	k1gNnSc4	přistání
přežili	přežít	k5eAaPmAgMnP	přežít
<g/>
,	,	kIx,	,
vyskytlo	vyskytnout	k5eAaPmAgNnS	vyskytnout
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
život	život	k1gInSc4	život
neohrožujících	ohrožující	k2eNgNnPc2d1	neohrožující
zranění	zranění	k1gNnPc2	zranění
a	a	k8xC	a
podchlazení	podchlazení	k1gNnPc2	podchlazení
(	(	kIx(	(
<g/>
teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
byla	být	k5eAaImAgFnS	být
v	v	k7c4	v
daný	daný	k2eAgInSc4d1	daný
okamžik	okamžik	k1gInSc4	okamžik
–	–	k?	–
<g/>
7	[number]	k4	7
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zkušený	zkušený	k2eAgMnSc1d1	zkušený
osmapadesátiletý	osmapadesátiletý	k2eAgMnSc1d1	osmapadesátiletý
kapitán	kapitán	k1gMnSc1	kapitán
Sullenberger	Sullenberger	k1gMnSc1	Sullenberger
byl	být	k5eAaImAgMnS	být
oceněn	ocenit	k5eAaPmNgMnS	ocenit
za	za	k7c4	za
perfektní	perfektní	k2eAgInSc4d1	perfektní
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
vyžadující	vyžadující	k2eAgFnSc4d1	vyžadující
značnou	značný	k2eAgFnSc4d1	značná
odvahu	odvaha	k1gFnSc4	odvaha
<g/>
,	,	kIx,	,
chladnokrevnost	chladnokrevnost	k1gFnSc4	chladnokrevnost
a	a	k8xC	a
dokonalé	dokonalý	k2eAgNnSc4d1	dokonalé
ovládání	ovládání	k1gNnSc4	ovládání
letounu	letoun	k1gInSc2	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
mu	on	k3xPp3gMnSc3	on
pogratulovali	pogratulovat	k5eAaPmAgMnP	pogratulovat
úřadující	úřadující	k2eAgMnSc1d1	úřadující
prezident	prezident	k1gMnSc1	prezident
George	Georg	k1gMnSc2	Georg
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
i	i	k8xC	i
zvolený	zvolený	k2eAgMnSc1d1	zvolený
prezident	prezident	k1gMnSc1	prezident
Barack	Barack	k1gMnSc1	Barack
Obama	Obama	k?	Obama
(	(	kIx(	(
<g/>
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
úřadu	úřad	k1gInSc2	úřad
ujal	ujmout	k5eAaPmAgInS	ujmout
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
byl	být	k5eAaImAgInS	být
Sullenbergerův	Sullenbergerův	k2eAgInSc1d1	Sullenbergerův
husarský	husarský	k2eAgInSc1d1	husarský
kousek	kousek	k1gInSc1	kousek
oslavován	oslavován	k2eAgInSc1d1	oslavován
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Hudsonský	Hudsonský	k2eAgInSc1d1	Hudsonský
zázrak	zázrak	k1gInSc1	zázrak
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zfilmovaná	zfilmovaný	k2eAgNnPc1d1	zfilmované
ztvárnění	ztvárnění	k1gNnPc1	ztvárnění
události	událost	k1gFnSc2	událost
==	==	k?	==
</s>
</p>
<p>
<s>
Havárie	havárie	k1gFnSc1	havárie
se	s	k7c7	s
šťastným	šťastný	k2eAgInSc7d1	šťastný
koncem	konec	k1gInSc7	konec
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
předmětem	předmět	k1gInSc7	předmět
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
dílů	díl	k1gInPc2	díl
dokumentární	dokumentární	k2eAgFnSc2d1	dokumentární
série	série	k1gFnSc2	série
Letecké	letecký	k2eAgFnSc2d1	letecká
katastrofy	katastrofa	k1gFnSc2	katastrofa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
film	film	k1gInSc1	film
Sully	Sulla	k1gFnSc2	Sulla
<g/>
:	:	kIx,	:
Zázrak	zázrak	k1gInSc1	zázrak
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Hudson	Hudsona	k1gFnPc2	Hudsona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Let	let	k1gInSc1	let
US	US	kA	US
Airways	Airways	k1gInSc4	Airways
1549	[number]	k4	1549
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Záběry	záběr	k1gInPc1	záběr
nouzového	nouzový	k2eAgNnSc2d1	nouzové
přistání	přistání	k1gNnSc2	přistání
a	a	k8xC	a
záchranných	záchranný	k2eAgFnPc2d1	záchranná
akcí	akce	k1gFnPc2	akce
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Černá	černý	k2eAgFnSc1d1	černá
skříňka	skříňka	k1gFnSc1	skříňka
<g/>
:	:	kIx,	:
Airbusu	airbus	k1gInSc2	airbus
po	po	k7c6	po
střetu	střet	k1gInSc6	střet
s	s	k7c7	s
ptáky	pták	k1gMnPc7	pták
vypadly	vypadnout	k5eAaPmAgInP	vypadnout
oba	dva	k4xCgInPc1	dva
motory	motor	k1gInPc1	motor
naráz	naráz	k6eAd1	naráz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Anatomy	anatom	k1gMnPc4	anatom
of	of	k?	of
a	a	k8xC	a
miracle	miracl	k1gMnSc2	miracl
<g/>
:	:	kIx,	:
How	How	k1gMnSc2	How
Captain	Captain	k1gMnSc1	Captain
Chesley	Cheslea	k1gMnSc2	Cheslea
Sullenberger	Sullenberger	k1gMnSc1	Sullenberger
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
skill	skill	k1gMnSc1	skill
saved	saved	k1gMnSc1	saved
155	[number]	k4	155
lives	lives	k1gInSc1	lives
-	-	kIx~	-
článek	článek	k1gInSc1	článek
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Daily	Daila	k1gFnSc2	Daila
Mail	mail	k1gInSc1	mail
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInPc1d1	oficiální
dokumenty	dokument	k1gInPc1	dokument
NTSB	NTSB	kA	NTSB
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
animace	animace	k1gFnSc1	animace
nehody	nehoda	k1gFnSc2	nehoda
</s>
</p>
