<p>
<s>
Sešit	sešit	k1gInSc1	sešit
<g/>
,	,	kIx,	,
zdrobněle	zdrobněle	k6eAd1	zdrobněle
sešítek	sešítka	k1gFnPc2	sešítka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
předmět	předmět	k1gInSc1	předmět
denní	denní	k2eAgFnSc2d1	denní
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
listy	lista	k1gFnPc1	lista
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
spojené	spojený	k2eAgInPc1d1	spojený
v	v	k7c6	v
ohybu	ohyb	k1gInSc6	ohyb
svorkami	svorka	k1gFnPc7	svorka
nebo	nebo	k8xC	nebo
sešité	sešitý	k2eAgFnPc1d1	sešitá
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
sešit	sešit	k1gInSc1	sešit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stránky	stránka	k1gFnPc1	stránka
sešitu	sešit	k1gInSc2	sešit
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
čisté	čistý	k2eAgInPc1d1	čistý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mít	mít	k5eAaImF	mít
předtištěnou	předtištěný	k2eAgFnSc4d1	předtištěná
předlohu	předloha	k1gFnSc4	předloha
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
linky	linka	k1gFnPc4	linka
nebo	nebo	k8xC	nebo
čtvercovou	čtvercový	k2eAgFnSc4d1	čtvercová
síť	síť	k1gFnSc4	síť
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
čtverečkované	čtverečkovaný	k2eAgInPc1d1	čtverečkovaný
sešity	sešit	k1gInPc1	sešit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Linkované	linkovaný	k2eAgInPc1d1	linkovaný
sešity	sešit	k1gInPc1	sešit
mívají	mívat	k5eAaImIp3nP	mívat
červené	červený	k2eAgInPc4d1	červený
motivy	motiv	k1gInPc4	motiv
<g/>
,	,	kIx,	,
čtverečkované	čtverečkovaný	k2eAgFnPc4d1	čtverečkovaná
zelené	zelený	k2eAgFnPc4d1	zelená
a	a	k8xC	a
čisté	čistý	k2eAgNnSc1d1	čisté
modré	modré	k1gNnSc1	modré
<g/>
.	.	kIx.	.
</s>
<s>
Sešity	sešit	k1gInPc1	sešit
mají	mít	k5eAaImIp3nP	mít
nejširší	široký	k2eAgNnSc4d3	nejširší
použití	použití	k1gNnSc4	použití
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žákům	žák	k1gMnPc3	žák
a	a	k8xC	a
studentům	student	k1gMnPc3	student
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
diktáty	diktát	k1gInPc4	diktát
<g/>
,	,	kIx,	,
zápisky	zápiska	k1gFnPc4	zápiska
učiva	učivo	k1gNnSc2	učivo
a	a	k8xC	a
domácí	domácí	k2eAgInPc4d1	domácí
úkoly	úkol	k1gInPc4	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
ale	ale	k9	ale
i	i	k9	i
v	v	k7c6	v
administrativě	administrativa	k1gFnSc6	administrativa
<g/>
,	,	kIx,	,
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Aspekt	aspekt	k1gInSc1	aspekt
recyklace	recyklace	k1gFnSc2	recyklace
==	==	k?	==
</s>
</p>
<p>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
jak	jak	k6eAd1	jak
z	z	k7c2	z
nového	nový	k2eAgNnSc2d1	nové
<g/>
,	,	kIx,	,
tak	tak	k9	tak
z	z	k7c2	z
recyklovaného	recyklovaný	k2eAgInSc2d1	recyklovaný
(	(	kIx(	(
<g/>
downcyklovaného	downcyklovaný	k2eAgInSc2d1	downcyklovaný
<g/>
)	)	kIx)	)
papíru	papír	k1gInSc2	papír
–	–	k?	–
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
měkčí	měkký	k2eAgInSc4d2	měkčí
a	a	k8xC	a
poddajnější	poddajný	k2eAgInSc4d2	poddajnější
papír	papír	k1gInSc4	papír
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
jeho	jeho	k3xOp3gFnSc2	jeho
menší	malý	k2eAgFnSc2d2	menší
čistoty	čistota	k1gFnSc2	čistota
<g/>
.	.	kIx.	.
</s>
<s>
Ekologický	ekologický	k2eAgInSc1d1	ekologický
aspekt	aspekt	k1gInSc1	aspekt
šetření	šetření	k1gNnSc2	šetření
přírody	příroda	k1gFnSc2	příroda
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
značný	značný	k2eAgInSc1d1	značný
–	–	k?	–
177	[number]	k4	177
sešitů	sešit	k1gInPc2	sešit
ze	z	k7c2	z
starého	starý	k2eAgInSc2d1	starý
papíru	papír	k1gInSc2	papír
ušetří	ušetřit	k5eAaPmIp3nS	ušetřit
jeden	jeden	k4xCgMnSc1	jeden
vzrostlý	vzrostlý	k2eAgInSc1d1	vzrostlý
strom	strom	k1gInSc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
poslední	poslední	k2eAgFnSc7d1	poslední
dobou	doba	k1gFnSc7	doba
recyklované	recyklovaný	k2eAgInPc1d1	recyklovaný
sešity	sešit	k1gInPc1	sešit
ustupují	ustupovat	k5eAaImIp3nP	ustupovat
(	(	kIx(	(
<g/>
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
neexistovaly	existovat	k5eNaImAgFnP	existovat
jiné	jiná	k1gFnPc1	jiná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Kód	kód	k1gInSc4	kód
vyráběných	vyráběný	k2eAgInPc2d1	vyráběný
sešitů	sešit	k1gInPc2	sešit
==	==	k?	==
</s>
</p>
<p>
<s>
Sešity	sešit	k1gInPc1	sešit
vyrobené	vyrobený	k2eAgInPc1d1	vyrobený
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jsou	být	k5eAaImIp3nP	být
značeny	značit	k5eAaImNgFnP	značit
kódem	kód	k1gInSc7	kód
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
číslic	číslice	k1gFnPc2	číslice
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgMnSc2	který
lze	lze	k6eAd1	lze
vyčíst	vyčíst	k5eAaPmF	vyčíst
jejich	jejich	k3xOp3gFnSc4	jejich
velikost	velikost	k1gFnSc4	velikost
(	(	kIx(	(
<g/>
formát	formát	k1gInSc4	formát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
listů	list	k1gInPc2	list
a	a	k8xC	a
tištěnou	tištěný	k2eAgFnSc4d1	tištěná
předlohu	předloha	k1gFnSc4	předloha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
číslice	číslice	k1gFnSc1	číslice
značí	značit	k5eAaImIp3nS	značit
formát	formát	k1gInSc4	formát
a	a	k8xC	a
nabývá	nabývat	k5eAaImIp3nS	nabývat
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
4	[number]	k4	4
-	-	kIx~	-
pro	pro	k7c4	pro
formát	formát	k1gInSc4	formát
A	a	k9	a
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
5	[number]	k4	5
-	-	kIx~	-
pro	pro	k7c4	pro
formát	formát	k1gInSc4	formát
A	a	k9	a
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
6	[number]	k4	6
-	-	kIx~	-
pro	pro	k7c4	pro
formát	formát	k1gInSc4	formát
A	a	k9	a
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
Druhá	druhý	k4xOgFnSc1	druhý
číslice	číslice	k1gFnSc1	číslice
označuje	označovat	k5eAaImIp3nS	označovat
počet	počet	k1gInSc1	počet
listů	list	k1gInPc2	list
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
-	-	kIx~	-
pro	pro	k7c4	pro
10	[number]	k4	10
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
2	[number]	k4	2
-	-	kIx~	-
pro	pro	k7c4	pro
20	[number]	k4	20
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
4	[number]	k4	4
-	-	kIx~	-
pro	pro	k7c4	pro
40	[number]	k4	40
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
6	[number]	k4	6
-	-	kIx~	-
pro	pro	k7c4	pro
60	[number]	k4	60
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
8	[number]	k4	8
-	-	kIx~	-
pro	pro	k7c4	pro
80	[number]	k4	80
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
<g/>
Třetí	třetí	k4xOgFnSc1	třetí
číslice	číslice	k1gFnSc1	číslice
značí	značit	k5eAaImIp3nS	značit
předtištěnou	předtištěný	k2eAgFnSc4d1	předtištěná
předlohu	předloha	k1gFnSc4	předloha
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
0	[number]	k4	0
-	-	kIx~	-
pro	pro	k7c4	pro
sešit	sešit	k1gInSc4	sešit
nelinkovaný	linkovaný	k2eNgInSc4d1	nelinkovaný
-	-	kIx~	-
čistý	čistý	k2eAgInSc4d1	čistý
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1	[number]	k4	1
-	-	kIx~	-
pro	pro	k7c4	pro
sešit	sešit	k1gInSc4	sešit
linkovaný	linkovaný	k2eAgInSc4d1	linkovaný
-	-	kIx~	-
linky	linka	k1gFnSc2	linka
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
20	[number]	k4	20
mm	mm	kA	mm
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
2	[number]	k4	2
-	-	kIx~	-
pro	pro	k7c4	pro
sešit	sešit	k1gInSc4	sešit
linkovaný	linkovaný	k2eAgInSc4d1	linkovaný
-	-	kIx~	-
linky	linka	k1gFnSc2	linka
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
16	[number]	k4	16
mm	mm	kA	mm
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
3	[number]	k4	3
-	-	kIx~	-
pro	pro	k7c4	pro
sešit	sešit	k1gInSc4	sešit
linkovaný	linkovaný	k2eAgInSc4d1	linkovaný
-	-	kIx~	-
linky	linka	k1gFnSc2	linka
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
12	[number]	k4	12
mm	mm	kA	mm
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
4	[number]	k4	4
-	-	kIx~	-
pro	pro	k7c4	pro
sešit	sešit	k1gInSc4	sešit
linkovaný	linkovaný	k2eAgInSc4d1	linkovaný
-	-	kIx~	-
linky	linka	k1gFnSc2	linka
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
8	[number]	k4	8
mm	mm	kA	mm
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
5	[number]	k4	5
-	-	kIx~	-
pro	pro	k7c4	pro
sešit	sešit	k1gInSc4	sešit
čtverečkovaný	čtverečkovaný	k2eAgInSc4d1	čtverečkovaný
-	-	kIx~	-
čtverečky	čtvereček	k1gInPc1	čtvereček
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
5	[number]	k4	5
x	x	k?	x
5	[number]	k4	5
mm	mm	kA	mm
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
10	[number]	k4	10
-	-	kIx~	-
pro	pro	k7c4	pro
sešit	sešit	k1gInSc4	sešit
čtverečkovaný	čtverečkovaný	k2eAgInSc4d1	čtverečkovaný
-	-	kIx~	-
čtverečky	čtvereček	k1gInPc1	čtvereček
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
10	[number]	k4	10
x	x	k?	x
10	[number]	k4	10
mm	mm	kA	mm
<g/>
,	,	kIx,	,
<g/>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
tlusté	tlustý	k2eAgInPc1d1	tlustý
sešity	sešit	k1gInPc1	sešit
o	o	k7c4	o
100	[number]	k4	100
a	a	k8xC	a
více	hodně	k6eAd2	hodně
stranách	strana	k1gFnPc6	strana
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
lepenou	lepený	k2eAgFnSc7d1	lepená
vazbou	vazba	k1gFnSc7	vazba
a	a	k8xC	a
opatřené	opatřený	k2eAgInPc1d1	opatřený
tuhými	tuhý	k2eAgFnPc7d1	tuhá
deskami	deska	k1gFnPc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgInPc1	takovýto
sešity	sešit	k1gInPc1	sešit
bývají	bývat	k5eAaImIp3nP	bývat
označovány	označován	k2eAgInPc1d1	označován
jakožto	jakožto	k8xS	jakožto
zapisovací	zapisovací	k2eAgFnSc1d1	zapisovací
kniha	kniha	k1gFnSc1	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Sešity	sešit	k1gInPc1	sešit
se	s	k7c7	s
speciálním	speciální	k2eAgInSc7d1	speciální
slabým	slabý	k2eAgInSc7d1	slabý
a	a	k8xC	a
průsvitným	průsvitný	k2eAgInSc7d1	průsvitný
papírem	papír	k1gInSc7	papír
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
průpisníky	průpisník	k1gInPc1	průpisník
<g/>
.	.	kIx.	.
</s>
<s>
Sešity	sešit	k1gInPc1	sešit
bez	bez	k7c2	bez
předtištěné	předtištěný	k2eAgFnSc2d1	předtištěná
předlohy	předloha	k1gFnSc2	předloha
<g/>
,	,	kIx,	,
z	z	k7c2	z
kvalitnějšího	kvalitní	k2eAgInSc2d2	kvalitnější
papíru	papír	k1gInSc2	papír
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
o	o	k7c6	o
formátu	formát	k1gInSc6	formát
A4	A4	k1gFnSc2	A4
a	a	k8xC	a
větších	veliký	k2eAgFnPc2d2	veliký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
sloužit	sloužit	k5eAaImF	sloužit
pro	pro	k7c4	pro
kreslení	kreslení	k1gNnSc4	kreslení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
skicáky	skicák	k1gInPc1	skicák
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
mají	mít	k5eAaImIp3nP	mít
ale	ale	k9	ale
většinou	většinou	k6eAd1	většinou
lepenou	lepený	k2eAgFnSc4d1	lepená
vazbu	vazba	k1gFnSc4	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Předtištěná	předtištěný	k2eAgFnSc1d1	předtištěná
předloha	předloha	k1gFnSc1	předloha
se	se	k3xPyFc4	se
nemusí	muset	k5eNaImIp3nS	muset
omezovat	omezovat	k5eAaImF	omezovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
linky	linka	k1gFnPc4	linka
a	a	k8xC	a
čtverečky	čtvereček	k1gInPc4	čtvereček
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
běžně	běžně	k6eAd1	běžně
koupit	koupit	k5eAaPmF	koupit
i	i	k9	i
sešity	sešit	k1gInPc1	sešit
s	s	k7c7	s
předtištěnými	předtištěný	k2eAgInPc7d1	předtištěný
vzory	vzor	k1gInPc7	vzor
pro	pro	k7c4	pro
různou	různý	k2eAgFnSc4d1	různá
administrační	administrační	k2eAgFnSc4d1	administrační
agendu	agenda	k1gFnSc4	agenda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
různé	různý	k2eAgFnPc4d1	různá
evidence	evidence	k1gFnPc4	evidence
<g/>
,	,	kIx,	,
výkazy	výkaz	k1gInPc4	výkaz
<g/>
,	,	kIx,	,
návštěvní	návštěvní	k2eAgInPc1d1	návštěvní
záznamy	záznam	k1gInPc1	záznam
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
sešity	sešit	k1gInPc1	sešit
s	s	k7c7	s
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
dny	den	k1gInPc7	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
–	–	k?	–
jejich	jejich	k3xOp3gFnPc4	jejich
funkce	funkce	k1gFnPc4	funkce
pak	pak	k6eAd1	pak
částečně	částečně	k6eAd1	částečně
splývá	splývat	k5eAaImIp3nS	splývat
s	s	k7c7	s
funkcí	funkce	k1gFnSc7	funkce
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobné	podobný	k2eAgInPc1d1	podobný
výrazy	výraz	k1gInPc1	výraz
k	k	k7c3	k
výrazu	výraz	k1gInSc3	výraz
sešit	sešit	k1gInSc1	sešit
jsou	být	k5eAaImIp3nP	být
poznámkový	poznámkový	k2eAgInSc1d1	poznámkový
blok	blok	k1gInSc1	blok
nebo	nebo	k8xC	nebo
notes	notes	k1gInSc1	notes
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zdrobnělé	zdrobnělý	k2eAgFnSc6d1	zdrobnělá
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
notýsek	notýsek	k1gInSc1	notýsek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jiný	jiný	k2eAgInSc1d1	jiný
význam	význam	k1gInSc1	význam
slova	slovo	k1gNnSc2	slovo
==	==	k?	==
</s>
</p>
<p>
<s>
Levné	levný	k2eAgFnPc1d1	levná
knihy	kniha	k1gFnPc1	kniha
a	a	k8xC	a
brožury	brožura	k1gFnPc1	brožura
vyráběné	vyráběný	k2eAgInPc1d1	vyráběný
podobnou	podobný	k2eAgFnSc4d1	podobná
technologii	technologie	k1gFnSc4	technologie
jakožto	jakožto	k8xS	jakožto
klasické	klasický	k2eAgInPc4d1	klasický
sešity	sešit	k1gInPc4	sešit
sesvorkováním	sesvorkování	k1gNnSc7	sesvorkování
bývají	bývat	k5eAaImIp3nP	bývat
někdy	někdy	k6eAd1	někdy
označovány	označovat	k5eAaImNgInP	označovat
jakožto	jakožto	k8xS	jakožto
tzv.	tzv.	kA	tzv.
sešitové	sešitový	k2eAgNnSc1d1	sešitové
vydání	vydání	k1gNnSc1	vydání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
sešit	sešit	k1gInSc1	sešit
se	se	k3xPyFc4	se
též	též	k9	též
označuje	označovat	k5eAaImIp3nS	označovat
soubor	soubor	k1gInSc4	soubor
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
některého	některý	k3yIgNnSc2	některý
z	z	k7c2	z
tabulkových	tabulkový	k2eAgInPc2d1	tabulkový
procesorů	procesor	k1gInPc2	procesor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Microsoft	Microsoft	kA	Microsoft
Excel	Excel	kA	Excel
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Calc	Calc	k1gFnSc1	Calc
z	z	k7c2	z
OpenOffice	OpenOffice	k1gFnSc2	OpenOffice
či	či	k8xC	či
LibreOffice	LibreOffice	k1gFnSc2	LibreOffice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sešit	sešit	k1gInSc1	sešit
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
sešit	sešit	k1gInSc1	sešit
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
