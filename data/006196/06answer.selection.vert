<s>
Na	na	k7c6	na
summitu	summit	k1gInSc6	summit
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
historicky	historicky	k6eAd1	historicky
prvním	první	k4xOgMnSc7	první
stálým	stálý	k2eAgMnSc7d1	stálý
předsedou	předseda	k1gMnSc7	předseda
Evropské	evropský	k2eAgFnSc2d1	Evropská
rady	rada	k1gFnSc2	rada
zvolen	zvolen	k2eAgMnSc1d1	zvolen
belgický	belgický	k2eAgMnSc1d1	belgický
premiér	premiér	k1gMnSc1	premiér
Herman	Herman	k1gMnSc1	Herman
Van	van	k1gInSc4	van
Rompuy	Rompua	k1gFnSc2	Rompua
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgInSc2	svůj
úřadu	úřad	k1gInSc2	úřad
ujal	ujmout	k5eAaPmAgInS	ujmout
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
