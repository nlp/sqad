<s>
Šitbořice	Šitbořice	k1gFnSc1
</s>
<s>
Šitbořice	Šitbořice	k1gFnPc4
Celkový	celkový	k2eAgInSc1d1
pohled	pohled	k1gInSc1
</s>
<s>
znakvlajka	znakvlajka	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
obec	obec	k1gFnSc1
LAU	LAU	kA
2	#num#	k4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CZ0644	CZ0644	k4
584932	#num#	k4
Pověřená	pověřený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
a	a	k8xC
obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
</s>
<s>
Hustopeče	Hustopeč	k1gFnPc1
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Břeclav	Břeclav	k1gFnSc1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
644	#num#	k4
<g/>
)	)	kIx)
Kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
NUTS	NUTS	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jihomoravský	jihomoravský	k2eAgMnSc1d1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
64	#num#	k4
<g/>
)	)	kIx)
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Morava	Morava	k1gFnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
0	#num#	k4
<g/>
′	′	k?
<g/>
52	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
46	#num#	k4
<g/>
′	′	k?
<g/>
47	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
2	#num#	k4
083	#num#	k4
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
12,25	12,25	k4
km²	km²	k?
Katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
</s>
<s>
Šitbořice	Šitbořice	k1gFnSc1
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
264	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
691	#num#	k4
76	#num#	k4
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
1	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Osvobození	osvobození	k1gNnSc1
9269176	#num#	k4
Šitbořice	Šitbořice	k1gFnSc2
starosta@sitborice.cz	starosta@sitborice.cza	k1gFnPc2
Starosta	Starosta	k1gMnSc1
</s>
<s>
Antonín	Antonín	k1gMnSc1
Lengál	Lengál	k1gMnSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.sitborice.cz	www.sitborice.cz	k1gInSc1
</s>
<s>
Šitbořice	Šitbořice	k1gFnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
obce	obec	k1gFnPc1
</s>
<s>
584932	#num#	k4
Kód	kód	k1gInSc1
části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
162680	#num#	k4
Geodata	Geodata	k1gFnSc1
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obec	obec	k1gFnSc1
Šitbořice	Šitbořice	k1gFnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Schüttboritz	Schüttboritz	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
okrese	okres	k1gInSc6
Břeclav	Břeclav	k1gFnSc1
v	v	k7c6
Jihomoravském	jihomoravský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
25	#num#	k4
km	km	kA
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
přibližně	přibližně	k6eAd1
2	#num#	k4
100	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
se	se	k3xPyFc4
vyvíjel	vyvíjet	k5eAaImAgInS
od	od	k7c2
varianty	varianta	k1gFnSc2
Sdeboric	Sdeborice	k1gFnPc2
(	(	kIx(
<g/>
1255	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Schutwaricz	Schutwaricz	k1gMnSc1
(	(	kIx(
<g/>
1317	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sutborzicz	Sutborzicz	k1gMnSc1
(	(	kIx(
<g/>
1368	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jessdeborsicz	Jessdeborsicz	k1gMnSc1
(	(	kIx(
<g/>
1373	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Šitbořicích	Šitbořice	k1gFnPc6
(	(	kIx(
<g/>
1412	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Šidbořicích	Šidbořice	k1gFnPc6
(	(	kIx(
<g/>
1437	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
Ssytboržicz	Ssytboržicza	k1gFnPc2
(	(	kIx(
<g/>
1480	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Schittboržitz	Schittboržitz	k1gMnSc1
(	(	kIx(
<g/>
1718	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Schüttboržitz	Schüttboržitz	k1gMnSc1
(	(	kIx(
<g/>
1720	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Schitboržitz	Schitboržitz	k1gMnSc1
(	(	kIx(
<g/>
1751	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Schuettboržitz	Schuettboržitz	k1gInSc1
a	a	k8xC
Ssittbořice	Ssittbořice	k1gFnSc1
(	(	kIx(
<g/>
1846	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Schüttborzitz	Schüttborzitz	k1gInSc1
a	a	k8xC
Šitbořice	Šitbořice	k1gFnSc1
(	(	kIx(
<g/>
1872	#num#	k4
<g/>
)	)	kIx)
až	až	k6eAd1
k	k	k7c3
podobě	podoba	k1gFnSc3
Šitbořice	Šitbořice	k1gFnSc2
(	(	kIx(
<g/>
1924	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgFnSc1d1
místní	místní	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
znělo	znět	k5eAaImAgNnS
Ješutbořice	Ješutbořice	k1gFnPc1
a	a	k8xC
vzniklo	vzniknout	k5eAaPmAgNnS
z	z	k7c2
osobního	osobní	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
Ješutbor	Ješutbor	k1gInSc1
<g/>
,	,	kIx,
pozdější	pozdní	k2eAgInSc1d2
název	název	k1gInSc1
Šitbořice	Šitbořice	k1gFnSc2
vznikl	vzniknout	k5eAaPmAgInS
dekompozicí	dekompozice	k1gFnSc7
původního	původní	k2eAgInSc2d1
Ješutbořice	Ješutbořice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojmenování	pojmenování	k1gNnSc1
je	být	k5eAaImIp3nS
rodu	rod	k1gInSc2
ženského	ženský	k2eAgInSc2d1
<g/>
,	,	kIx,
čísla	číslo	k1gNnSc2
pomnožného	pomnožný	k2eAgNnSc2d1
a	a	k8xC
genitiv	genitiv	k1gInSc1
zní	znět	k5eAaImIp3nS
Šitbořic	Šitbořice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Podle	podle	k7c2
vykopávek	vykopávka	k1gFnPc2
v	v	k7c6
trati	trať	k1gFnSc6
Padělky	padělek	k1gInPc4
byl	být	k5eAaImAgInS
prostor	prostor	k1gInSc1
dnešních	dnešní	k2eAgFnPc2d1
Šitbořic	Šitbořice	k1gFnPc2
osídlen	osídlen	k2eAgInSc1d1
již	již	k6eAd1
v	v	k7c6
neolitu	neolit	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
trati	trať	k1gFnSc6
Staré	Staré	k2eAgFnSc2d1
hory	hora	k1gFnSc2
bylo	být	k5eAaImAgNnS
objeveno	objevit	k5eAaPmNgNnS
pohřebiště	pohřebiště	k1gNnSc4
z	z	k7c2
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
vsi	ves	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1255	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nad	nad	k7c7
farou	fara	k1gFnSc7
držel	držet	k5eAaImAgInS
patronát	patronát	k1gInSc1
klášter	klášter	k1gInSc1
v	v	k7c6
Rajhradu	Rajhrad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
se	se	k3xPyFc4
nazývala	nazývat	k5eAaImAgFnS
Ješutbořice	Ješutbořice	k1gFnSc1
podle	podle	k7c2
legendárního	legendární	k2eAgMnSc2d1
zakladatele	zakladatel	k1gMnSc2
vladyky	vladyka	k1gMnSc2
Ješutbora	Ješutbor	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vznikla	vzniknout	k5eAaPmAgFnS
na	na	k7c6
vrchu	vrch	k1gInSc6
Hradisko	hradisko	k1gNnSc1
tvrz	tvrz	k1gFnSc1
<g/>
,	,	kIx,
vlastněná	vlastněný	k2eAgFnSc1d1
pány	pan	k1gMnPc7
z	z	k7c2
Deblína	Deblín	k1gInSc2
a	a	k8xC
Lomnice	Lomnice	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1368	#num#	k4
pány	pan	k1gMnPc7
z	z	k7c2
Dambořic	Dambořice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zanikla	zaniknout	k5eAaPmAgFnS
zřejmě	zřejmě	k6eAd1
kolem	kolem	k7c2
roku	rok	k1gInSc2
1460	#num#	k4
za	za	k7c2
česko-uherských	česko-uherský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
patřila	patřit	k5eAaImAgFnS
obec	obec	k1gFnSc1
klášteru	klášter	k1gInSc3
sv.	sv.	kA
Anny	Anna	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1824	#num#	k4
byla	být	k5eAaImAgFnS
připojena	připojit	k5eAaPmNgFnS
k	k	k7c3
panství	panství	k1gNnSc6
Klobouky	Klobouky	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1898	#num#	k4
funguje	fungovat	k5eAaImIp3nS
hasičský	hasičský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obec	obec	k1gFnSc1
byla	být	k5eAaImAgFnS
velmi	velmi	k6eAd1
poničena	poničit	k5eAaPmNgFnS
v	v	k7c6
dubnu	duben	k1gInSc6
1945	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zde	zde	k6eAd1
Wehrmacht	wehrmacht	k1gInSc1
sváděl	svádět	k5eAaImAgInS
urputné	urputný	k2eAgInPc4d1
ústupové	ústupový	k2eAgInPc4d1
boje	boj	k1gInPc4
(	(	kIx(
<g/>
Bratislavsko-brněnská	bratislavsko-brněnský	k2eAgFnSc1d1
operace	operace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
znaku	znak	k1gInSc6
obce	obec	k1gFnSc2
je	být	k5eAaImIp3nS
žlutý	žlutý	k2eAgInSc4d1
džbán	džbán	k1gInSc4
zvaný	zvaný	k2eAgInSc1d1
plucar	plucar	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
místní	místní	k2eAgFnSc4d1
léčivou	léčivý	k2eAgFnSc4d1
vodu	voda	k1gFnSc4
<g/>
,	,	kIx,
černé	černý	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
z	z	k7c2
erbu	erb	k1gInSc2
pánů	pan	k1gMnPc2
z	z	k7c2
Lomnice	Lomnice	k1gFnSc2
a	a	k8xC
vinný	vinný	k2eAgInSc1d1
hrozen	hrozen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šitbořice	Šitbořice	k1gFnSc1
byly	být	k5eAaImAgFnP
zvoleny	zvolit	k5eAaPmNgFnP
Vesnicí	vesnice	k1gFnSc7
roku	rok	k1gInSc2
okresu	okres	k1gInSc2
Břeclav	Břeclav	k1gFnSc1
1998	#num#	k4
a	a	k8xC
2001	#num#	k4
<g/>
,	,	kIx,
dostaly	dostat	k5eAaPmAgFnP
bílou	bílý	k2eAgFnSc4d1
stuhu	stuha	k1gFnSc4
za	za	k7c4
práci	práce	k1gFnSc4
s	s	k7c7
mládeží	mládež	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Podle	podle	k7c2
sčítání	sčítání	k1gNnPc2
1930	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
v	v	k7c6
389	#num#	k4
domech	dům	k1gInPc6
1851	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
se	se	k3xPyFc4
1847	#num#	k4
hlásilo	hlásit	k5eAaImAgNnS
k	k	k7c3
československé	československý	k2eAgFnSc3d1
národnosti	národnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žilo	žít	k5eAaImAgNnS
zde	zde	k6eAd1
1837	#num#	k4
římských	římský	k2eAgMnPc2d1
katolíků	katolík	k1gMnPc2
<g/>
,	,	kIx,
5	#num#	k4
evangelíků	evangelík	k1gMnPc2
a	a	k8xC
3	#num#	k4
židé	žid	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Struktura	struktura	k1gFnSc1
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
za	za	k7c4
celou	celý	k2eAgFnSc4d1
obec	obec	k1gFnSc4
i	i	k9
za	za	k7c4
jeho	jeho	k3xOp3gFnPc4
jednotlivé	jednotlivý	k2eAgFnPc4d1
části	část	k1gFnPc4
uvádí	uvádět	k5eAaImIp3nS
tabulka	tabulka	k1gFnSc1
níže	níže	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yIgFnSc6,k3yQgFnSc6
se	se	k3xPyFc4
zobrazuje	zobrazovat	k5eAaImIp3nS
i	i	k9
příslušnost	příslušnost	k1gFnSc4
jednotlivých	jednotlivý	k2eAgFnPc2d1
částí	část	k1gFnPc2
k	k	k7c3
obci	obec	k1gFnSc3
či	či	k8xC
následné	následný	k2eAgNnSc4d1
odtržení	odtržení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Místní	místní	k2eAgFnPc1d1
části	část	k1gFnPc1
<g/>
18961880189019001910192119301950196119701980199120012013	#num#	k4
</s>
<s>
2018	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
část	část	k1gFnSc1
Šitbořice	Šitbořice	k1gFnSc2
<g/>
1162134113791458159816271851183320572044200919271	#num#	k4
9531	#num#	k4
940	#num#	k4
</s>
<s>
2	#num#	k4
037	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
domů	dům	k1gInPc2
</s>
<s>
část	část	k1gFnSc1
Šitbořice	Šitbořice	k1gFnSc2
<g/>
248272284304315320389404437461466550559	#num#	k4
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Socha	socha	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1759	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Gotický	gotický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
sv.	sv.	kA
Mikuláše	mikuláš	k1gInPc1
z	z	k7c2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byl	být	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1909	#num#	k4
zbořen	zbořit	k5eAaPmNgInS
z	z	k7c2
důvodů	důvod	k1gInPc2
nevyhovující	vyhovující	k2eNgFnSc2d1
statiky	statika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
stejném	stejný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
byl	být	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1910	#num#	k4
až	až	k9
1911	#num#	k4
vystavěn	vystavěn	k2eAgInSc4d1
nový	nový	k2eAgInSc4d1
kostel	kostel	k1gInSc4
v	v	k7c6
historizujícím	historizující	k2eAgInSc6d1
slohu	sloh	k1gInSc6
inspirovaném	inspirovaný	k2eAgInSc6d1
architekturou	architektura	k1gFnSc7
románskou	románský	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysvěcení	vysvěcení	k1gNnSc3
nového	nový	k2eAgInSc2d1
kostela	kostel	k1gInSc2
proběhlo	proběhnout	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1913	#num#	k4
v	v	k7c4
den	den	k1gInSc4
slavnosti	slavnost	k1gFnSc2
svatého	svatý	k2eAgMnSc2d1
Václava	Václav	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
opět	opět	k6eAd1
byl	být	k5eAaImAgInS
zasvěcen	zasvětit	k5eAaPmNgInS
svatému	svatý	k1gMnSc3
Mikuláši	Mikuláš	k1gMnSc3
</s>
<s>
sirný	sirný	k2eAgInSc4d1
pramen	pramen	k1gInSc4
Štengar	Štengara	k1gFnPc2
a	a	k8xC
místní	místní	k2eAgFnPc1d1
lázně	lázeň	k1gFnPc1
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
</s>
<s>
Metoděj	Metoděj	k1gMnSc1
Janíček	Janíček	k1gMnSc1
(	(	kIx(
<g/>
1862	#num#	k4
<g/>
–	–	k?
<g/>
1940	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
učitel	učitel	k1gMnSc1
a	a	k8xC
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Manoušek	Manoušek	k1gMnSc1
starší	starší	k1gMnSc1
(	(	kIx(
<g/>
1881	#num#	k4
<g/>
–	–	k?
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zvonař	zvonař	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
odlil	odlít	k5eAaPmAgInS
zvony	zvon	k1gInPc4
pro	pro	k7c4
šitbořický	šitbořický	k2eAgInSc4d1
kostel	kostel	k1gInSc4
svatého	svatý	k2eAgMnSc2d1
Mikuláše	Mikuláš	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Konečný	Konečný	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1889	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
člen	člen	k1gMnSc1
Československých	československý	k2eAgFnPc2d1
legií	legie	k1gFnPc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
a	a	k8xC
6	#num#	k4
<g/>
.	.	kIx.
střeleckého	střelecký	k2eAgInSc2d1
pluku	pluk	k1gInSc2
"	"	kIx"
<g/>
Hanáckého	hanácký	k2eAgMnSc2d1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vilém	Vilém	k1gMnSc1
Viktorin	Viktorin	k1gInSc1
(	(	kIx(
<g/>
1892	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
katolický	katolický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
</s>
<s>
Vilém	Vilém	k1gMnSc1
Tocháček	Tocháček	k1gMnSc1
(	(	kIx(
<g/>
1893	#num#	k4
<g/>
–	–	k?
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mlynář	mlynář	k1gMnSc1
<g/>
,	,	kIx,
oběť	oběť	k1gFnSc1
nacismu	nacismus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Antonín	Antonín	k1gMnSc1
Juvenál	Juvenála	k1gFnPc2
Valíček	Valíček	k1gMnSc1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
katolický	katolický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
<g/>
,	,	kIx,
kapucín	kapucín	k1gMnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Stejskal	Stejskal	k1gMnSc1
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
katolický	katolický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
</s>
<s>
Antonín	Antonín	k1gMnSc1
Konečný	Konečný	k1gMnSc1
(	(	kIx(
<g/>
1923	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
odborník	odborník	k1gMnSc1
na	na	k7c6
vinařství	vinařství	k1gNnSc6
a	a	k8xC
vinohradnictví	vinohradnictví	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Syn	syn	k1gMnSc1
legionáře	legionář	k1gMnSc4
Josefa	Josef	k1gMnSc4
Konečného	Konečný	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
Vozdecký	Vozdecký	k2eAgMnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
politik	politik	k1gMnSc1
a	a	k8xC
podnikatel	podnikatel	k1gMnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Nečas	Nečas	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1946	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
řezbář	řezbář	k1gMnSc1
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jiřina	Jiřina	k1gFnSc1
Kulíšková	Kulíšková	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1947	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
výtvarnice	výtvarnice	k1gFnSc1
<g/>
,	,	kIx,
autorka	autorka	k1gFnSc1
tapiserií	tapiserie	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
Zelinka	Zelinka	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1948	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
výtvarník	výtvarník	k1gMnSc1
<g/>
,	,	kIx,
řezbář	řezbář	k1gMnSc1
<g/>
,	,	kIx,
vinař	vinař	k1gMnSc1
a	a	k8xC
heraldik	heraldik	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Světlana	Světlana	k1gFnSc1
Kulíšková	Kulíškový	k2eAgFnSc1d1
–	–	k?
Ruggierová	Ruggierová	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
výtvarnice	výtvarnice	k1gFnSc1
<g/>
,	,	kIx,
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
technice	technika	k1gFnSc3
Art	Art	k1gMnPc2
protis	protis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dcera	dcera	k1gFnSc1
výtvarnice	výtvarnice	k1gFnSc1
Jiřiny	Jiřina	k1gFnSc2
Kulíškové	kulíšek	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Navrátil	Navrátil	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
fotbalista	fotbalista	k1gMnSc1
</s>
<s>
Marek	Marek	k1gMnSc1
Topolář	Topolář	k1gMnSc1
<g/>
,	,	kIx,
fotograf	fotograf	k1gMnSc1
a	a	k8xC
reprezentant	reprezentant	k1gMnSc1
ČR	ČR	kA
v	v	k7c6
kolové	kolová	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Držitel	držitel	k1gMnSc1
bronzu	bronz	k1gInSc2
z	z	k7c2
Mistrovství	mistrovství	k1gNnSc2
Evropy	Evropa	k1gFnSc2
juniorů	junior	k1gMnPc2
2012	#num#	k4
v	v	k7c6
Gentu	Gento	k1gNnSc6
s	s	k7c7
Filipem	Filip	k1gMnSc7
Zahálkou	Zahálka	k1gMnSc7
ze	z	k7c2
Svitávky	Svitávka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Mikuláše	Mikuláš	k1gMnSc2
</s>
<s>
Socha	socha	k1gFnSc1
sv.	sv.	kA
<g/>
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
</s>
<s>
Hasičská	hasičský	k2eAgFnSc1d1
zbrojnice	zbrojnice	k1gFnSc1
</s>
<s>
Divácká	divácký	k2eAgFnSc1d1
ulice	ulice	k1gFnSc1
</s>
<s>
Život	život	k1gInSc1
v	v	k7c6
obci	obec	k1gFnSc6
</s>
<s>
Šitbořice	Šitbořice	k1gFnPc1
jsou	být	k5eAaImIp3nP
významnou	významný	k2eAgFnSc7d1
vinařskou	vinařský	k2eAgFnSc7d1
obcí	obec	k1gFnSc7
<g/>
,	,	kIx,
vinice	vinice	k1gFnPc1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
tratích	trať	k1gFnPc6
Stará	starý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
,	,	kIx,
Rozdíly	rozdíl	k1gInPc1
<g/>
,	,	kIx,
Torhety	Torhet	k1gInPc1
<g/>
,	,	kIx,
Odměry	odměr	k1gInPc1
<g/>
,	,	kIx,
Ostudy	ostuda	k1gFnPc1
(	(	kIx(
<g/>
název	název	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
od	od	k7c2
úzké	úzký	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
vůz	vůz	k1gInSc1
často	často	k6eAd1
vyjel	vyjet	k5eAaPmAgInS
do	do	k7c2
pole	pole	k1gNnSc2
a	a	k8xC
udělal	udělat	k5eAaPmAgMnS
tam	tam	k6eAd1
ostudu	ostuda	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastějšími	častý	k2eAgFnPc7d3
odrůdami	odrůda	k1gFnPc7
jsou	být	k5eAaImIp3nP
sauvignon	sauvignon	k1gNnSc1
<g/>
,	,	kIx,
veltlínské	veltlínský	k2eAgFnPc1d1
zelené	zelený	k2eAgFnPc1d1
a	a	k8xC
neuburské	uburský	k2eNgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vinné	vinný	k2eAgInPc4d1
sklepy	sklep	k1gInPc4
návštěvník	návštěvník	k1gMnSc1
najde	najít	k5eAaPmIp3nS
v	v	k7c6
lokalitách	lokalita	k1gFnPc6
Hradisko	hradisko	k1gNnSc1
<g/>
,	,	kIx,
Žleby	žleb	k1gInPc1
<g/>
,	,	kIx,
Karpaty	Karpaty	k1gInPc1
<g/>
,	,	kIx,
Na	na	k7c6
pětince	pětinka	k1gFnSc6
a	a	k8xC
Ve	v	k7c6
Svatojánku	Svatojánek	k1gInSc6
<g/>
,	,	kIx,
turistickým	turistický	k2eAgNnPc3d1
zájezdům	zájezd	k1gInPc3
je	být	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
družstevní	družstevní	k2eAgInSc1d1
Sklep	sklep	k1gInSc1
Maryša	Maryša	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šitbořickým	Šitbořický	k2eAgMnSc7d1
rodákem	rodák	k1gMnSc7
je	být	k5eAaImIp3nS
vinařský	vinařský	k2eAgMnSc1d1
odborník	odborník	k1gMnSc1
Antonín	Antonín	k1gMnSc1
Konečný	Konečný	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
knihy	kniha	k1gFnSc2
Vinařem	vinař	k1gMnSc7
v	v	k7c6
Africe	Afrika	k1gFnSc6
i	i	k9
leckde	leckde	k6eAd1
jinde	jinde	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
je	být	k5eAaImIp3nS
hlavním	hlavní	k2eAgMnSc7d1
zaměstnavatelem	zaměstnavatel	k1gMnSc7
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Zemax	Zemax	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
transformací	transformace	k1gFnSc7
místního	místní	k2eAgInSc2d1
JZD	JZD	kA
<g/>
,	,	kIx,
mnoho	mnoho	k4c1
šitbořických	šitbořický	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
dojíždí	dojíždět	k5eAaImIp3nS
díky	díky	k7c3
poměrně	poměrně	k6eAd1
dobrému	dobrý	k2eAgNnSc3d1
spojení	spojení	k1gNnSc3
za	za	k7c7
prací	práce	k1gFnSc7
do	do	k7c2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šitbořice	Šitbořice	k1gFnPc1
mají	mít	k5eAaImIp3nP
tradičně	tradičně	k6eAd1
nízkou	nízký	k2eAgFnSc4d1
míru	míra	k1gFnSc4
nezaměstnanosti	nezaměstnanost	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
okresu	okres	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
cihelna	cihelna	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
dodává	dodávat	k5eAaImIp3nS
velmi	velmi	k6eAd1
kvalitní	kvalitní	k2eAgFnPc4d1
klasické	klasický	k2eAgFnPc4d1
cihly	cihla	k1gFnPc4
na	na	k7c4
spárované	spárovaný	k2eAgNnSc4d1
zdivo	zdivo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
typicky	typicky	k6eAd1
nažloutlou	nažloutlý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obci	obec	k1gFnSc6
sídlí	sídlet	k5eAaImIp3nS
také	také	k9
První	první	k4xOgInSc1
chodský	chodský	k2eAgInSc1d1
pivovárek	pivovárek	k1gInSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vaří	vařit	k5eAaImIp3nS
pivo	pivo	k1gNnSc4
značky	značka	k1gFnSc2
Kulak	kulak	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
obci	obec	k1gFnSc6
vychází	vycházet	k5eAaImIp3nS
zpravodaj	zpravodaj	k1gMnSc1
Štengaráček	Štengaráček	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Oddíl	oddíl	k1gInSc1
kolové	kolová	k1gFnSc2
Sokol	Sokol	k1gMnSc1
Šitbořice	Šitbořice	k1gFnSc1
každoročně	každoročně	k6eAd1
pořádá	pořádat	k5eAaImIp3nS
Turnaj	turnaj	k1gInSc1
mistrů	mistr	k1gMnPc2
světa	svět	k1gInSc2
v	v	k7c6
kolové	kolová	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
katastru	katastr	k1gInSc6
obce	obec	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
sirný	sirný	k2eAgInSc1d1
pramen	pramen	k1gInSc1
Štengar	Štengar	k1gInSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
název	název	k1gInSc1
se	se	k3xPyFc4
odvozuje	odvozovat	k5eAaImIp3nS
z	z	k7c2
německého	německý	k2eAgInSc2d1
výrazu	výraz	k1gInSc2
stinken	stinkna	k1gFnPc2
—	—	k?
páchnout	páchnout	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Voda	voda	k1gFnSc1
z	z	k7c2
pramene	pramen	k1gInSc2
léčí	léčit	k5eAaImIp3nP
revmatismus	revmatismus	k1gInSc4
a	a	k8xC
kožní	kožní	k2eAgFnPc4d1
choroby	choroba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
cestě	cesta	k1gFnSc6
na	na	k7c4
Těšany	Těšan	k1gInPc4
na	na	k7c6
místě	místo	k1gNnSc6
zvaném	zvaný	k2eAgNnSc6d1
U	u	k7c2
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
je	být	k5eAaImIp3nS
studánka	studánka	k1gFnSc1
<g/>
,	,	kIx,
u	u	k7c2
které	který	k3yQgFnSc2,k3yIgFnSc2,k3yRgFnSc2
podle	podle	k7c2
pověsti	pověst	k1gFnSc2
straší	strašit	k5eAaImIp3nS
duch	duch	k1gMnSc1
utopeného	utopený	k2eAgMnSc2d1
sedláka	sedlák	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2021	.2021	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
HOSÁK	HOSÁK	kA
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
;	;	kIx,
ŠRÁMEK	Šrámek	k1gMnSc1
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místní	místní	k2eAgMnPc1d1
jména	jméno	k1gNnSc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
M-	M-	k1gMnSc2
<g/>
Ž.	Ž.	kA
Svazek	svazek	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
550	#num#	k4
<g/>
-	-	kIx~
<g/>
551	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Statistický	statistický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
v	v	k7c6
Republice	republika	k1gFnSc6
československé	československý	k2eAgFnSc6d1
1930	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	země	k1gFnSc1
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
1935	#num#	k4
<g/>
.	.	kIx.
212	#num#	k4
s.	s.	k?
S.	S.	kA
50	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Šitbořice	Šitbořice	k1gFnSc1
-	-	kIx~
obec	obec	k1gFnSc1
<g/>
/	/	kIx~
<g/>
město	město	k1gNnSc1
(	(	kIx(
<g/>
okr	okr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Břeclav	Břeclav	k1gFnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2011-03-26	2011-03-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Statistický	statistický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
900	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
250	#num#	k4
<g/>
-	-	kIx~
<g/>
2394	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
Šitbořice	Šitbořice	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
489	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
1869	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
630	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
250	#num#	k4
<g/>
-	-	kIx~
<g/>
1310	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Okres	okres	k1gInSc1
Břeclav	Břeclav	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
640	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Antonín	Antonín	k1gMnSc1
Konečný	Konečný	k1gMnSc1
-	-	kIx~
Oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
obce	obec	k1gFnSc2
Šitbořice	Šitbořice	k1gFnSc1
<g/>
.	.	kIx.
www.sitborice.cz	www.sitborice.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vilém	Vilém	k1gMnSc1
Tocháček	Tocháček	k1gMnSc1
-	-	kIx~
Oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
obce	obec	k1gFnSc2
Šitbořice	Šitbořice	k1gFnSc1
<g/>
.	.	kIx.
www.sitborice.cz	www.sitborice.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Antonín	Antonín	k1gMnSc1
Konečný	Konečný	k1gMnSc1
životopis	životopis	k1gInSc1
|	|	kIx~
Databáze	databáze	k1gFnSc1
knih	kniha	k1gFnPc2
<g/>
.	.	kIx.
www.databazeknih.cz	www.databazeknih.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Václav	Václav	k1gMnSc1
Nečas	Nečas	k1gMnSc1
-	-	kIx~
Oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
obce	obec	k1gFnSc2
Šitbořice	Šitbořice	k1gFnSc1
<g/>
.	.	kIx.
www.sitborice.cz	www.sitborice.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Jiřina	Jiřina	k1gFnSc1
a	a	k8xC
Světlana	Světlana	k1gFnSc1
Kulíšková	Kulíšková	k1gFnSc1
-	-	kIx~
Ruggierová	Ruggierová	k1gFnSc1
-	-	kIx~
Oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
obce	obec	k1gFnSc2
Šitbořice	Šitbořice	k1gFnSc1
<g/>
.	.	kIx.
www.sitborice.cz	www.sitborice.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Jiřina	Jiřina	k1gFnSc1
Kulíšková	Kulíšková	k1gFnSc1
|	|	kIx~
Alfons	Alfons	k1gMnSc1
-	-	kIx~
Výtvarná	výtvarný	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
Hustopeče	Hustopeč	k1gFnSc2
<g/>
.	.	kIx.
www.alfonsart.cz	www.alfonsart.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
O	o	k7c6
mně	já	k3xPp1nSc6
|	|	kIx~
Josef	Josef	k1gMnSc1
Zelinka	Zelinka	k1gMnSc1
-	-	kIx~
řezbář	řezbář	k1gMnSc1
Hanáckého	hanácký	k2eAgNnSc2d1
Slovácka	Slovácko	k1gNnSc2
<g/>
.	.	kIx.
www.rezbaraucitel.cz	www.rezbaraucitel.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Světlana	Světlana	k1gFnSc1
Ruggierová	Ruggierová	k1gFnSc1
Kulíšková	Kulíšková	k1gFnSc1
|	|	kIx~
Porta	porta	k1gFnSc1
culturae	cultura	k1gFnSc2
<g/>
.	.	kIx.
www.portaculturae.eu	www.portaculturae.eu	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DANEŠ	Daneš	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolista	Kolista	k1gMnSc1
Marek	Marek	k1gMnSc1
Topolář	Topolář	k1gMnSc1
zajistil	zajistit	k5eAaPmAgMnS
Šitbořicím	Šitbořice	k1gFnPc3
první	první	k4xOgFnSc6
evropskou	evropský	k2eAgFnSc7d1
mediali	mediat	k5eAaImAgMnP,k5eAaPmAgMnP,k5eAaBmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Břeclavský	břeclavský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
zpravodaj	zpravodaj	k1gMnSc1
Štengaráček	Štengaráček	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šitbořice	Šitbořice	k1gFnSc1
<g/>
:	:	kIx,
obec	obec	k1gFnSc1
Šitbořice	Šitbořice	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Na	na	k7c4
turnaj	turnaj	k1gInSc4
šampionů	šampion	k1gMnPc2
v	v	k7c6
kolové	kolová	k1gFnSc6
do	do	k7c2
Šitbořic	Šitbořice	k1gFnPc2
míří	mířit	k5eAaImIp3nS
pět	pět	k4xCc1
mistrů	mistr	k1gMnPc2
světa	svět	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šitbořice	Šitbořice	k1gFnSc1
<g/>
:	:	kIx,
obec	obec	k1gFnSc1
Šitbořice	Šitbořice	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Zelinka	Zelinka	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Šitbořice	Šitbořice	k1gFnSc1
1255	#num#	k4
<g/>
—	—	k?
<g/>
2005	#num#	k4
</s>
<s>
Jan	Jan	k1gMnSc1
Grombíř	Grombíř	k1gMnSc1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
Grombíř	Grombíř	k1gMnSc1
<g/>
:	:	kIx,
Průvodce	průvodce	k1gMnSc1
sklepními	sklepní	k2eAgFnPc7d1
uličkami	ulička	k1gFnPc7
jižní	jižní	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
Nadace	nadace	k1gFnSc1
Partnerství	partnerství	k1gNnSc2
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-239-9422-3	978-80-239-9422-3	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Šitbořice	Šitbořice	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Šitbořice	Šitbořice	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Šitbořice	Šitbořice	k1gFnSc1
v	v	k7c6
Registru	registrum	k1gNnSc6
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
obce	obec	k1gFnSc2
Šitbořice	Šitbořice	k1gFnSc2
</s>
<s>
Stránky	stránka	k1gFnPc1
farnosti	farnost	k1gFnSc2
Šitbořice	Šitbořice	k1gFnSc2
</s>
<s>
Společenský	společenský	k2eAgInSc1d1
dům	dům	k1gInSc1
Orlovna	orlovna	k1gFnSc1
se	s	k7c7
sirnými	sirný	k2eAgFnPc7d1
lázněmi	lázeň	k1gFnPc7
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Města	město	k1gNnSc2
<g/>
,	,	kIx,
městyse	městys	k1gInSc2
a	a	k8xC
obce	obec	k1gFnSc2
okresu	okres	k1gInSc2
Břeclav	Břeclav	k1gFnSc1
</s>
<s>
Bavory	Bavory	k1gInPc1
•	•	k?
Boleradice	Boleradice	k1gFnSc2
•	•	k?
Borkovany	Borkovany	k?
•	•	k?
Bořetice	Bořetika	k1gFnSc3
•	•	k?
Brod	Brod	k1gInSc1
nad	nad	k7c7
Dyjí	Dyje	k1gFnSc7
•	•	k?
Brumovice	Brumovice	k1gFnSc2
•	•	k?
Břeclav	Břeclav	k1gFnSc1
•	•	k?
Březí	březí	k1gNnSc1
•	•	k?
Bulhary	Bulhar	k1gMnPc7
•	•	k?
Diváky	Diváky	k1gInPc1
•	•	k?
Dobré	dobrý	k2eAgFnSc2d1
Pole	pole	k1gFnSc2
•	•	k?
Dolní	dolní	k2eAgFnPc1d1
Dunajovice	Dunajovice	k1gFnPc1
•	•	k?
Dolní	dolní	k2eAgFnPc1d1
Věstonice	Věstonice	k1gFnPc1
•	•	k?
Drnholec	Drnholec	k1gInSc1
•	•	k?
Hlohovec	Hlohovec	k1gInSc4
•	•	k?
Horní	horní	k2eAgFnSc2d1
Bojanovice	Bojanovice	k1gFnSc2
•	•	k?
Horní	horní	k2eAgFnPc1d1
Věstonice	Věstonice	k1gFnPc1
•	•	k?
Hrušky	hruška	k1gFnSc2
•	•	k?
Hustopeče	Hustopeč	k1gFnSc2
•	•	k?
Jevišovka	Jevišovka	k1gFnSc1
•	•	k?
Kašnice	Kašnice	k1gFnSc2
•	•	k?
Klentnice	Klentnice	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Klobouky	Klobouky	k1gInPc7
u	u	k7c2
Brna	Brno	k1gNnSc2
•	•	k?
Kobylí	kobylí	k2eAgFnSc1d1
•	•	k?
Kostice	kostice	k1gFnSc1
•	•	k?
Krumvíř	Krumvíř	k1gInSc1
•	•	k?
Křepice	Křepice	k1gFnSc2
•	•	k?
Kurdějov	Kurdějov	k1gInSc1
•	•	k?
Ladná	ladný	k2eAgFnSc1d1
•	•	k?
Lanžhot	Lanžhot	k1gInSc1
•	•	k?
Lednice	Lednice	k1gFnSc2
•	•	k?
Mikulov	Mikulov	k1gInSc1
•	•	k?
Milovice	Milovice	k1gFnPc4
•	•	k?
Moravská	moravský	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Moravský	moravský	k2eAgInSc1d1
Žižkov	Žižkov	k1gInSc1
•	•	k?
Morkůvky	Morkůvka	k1gFnSc2
•	•	k?
Němčičky	Němčička	k1gFnSc2
•	•	k?
Nikolčice	Nikolčice	k1gFnSc2
•	•	k?
Novosedly	Novosedlo	k1gNnPc7
•	•	k?
Nový	nový	k2eAgInSc1d1
Přerov	Přerov	k1gInSc1
•	•	k?
Pavlov	Pavlov	k1gInSc1
•	•	k?
Perná	perný	k2eAgFnSc1d1
•	•	k?
Podivín	podivín	k1gMnSc1
•	•	k?
Popice	Popice	k1gFnSc2
•	•	k?
Pouzdřany	Pouzdřan	k1gMnPc4
•	•	k?
Přítluky	Přítluk	k1gInPc4
•	•	k?
Rakvice	rakvice	k1gFnSc2
•	•	k?
Sedlec	Sedlec	k1gInSc1
•	•	k?
Starovice	Starovice	k1gFnSc2
•	•	k?
Starovičky	Starovička	k1gFnSc2
•	•	k?
Strachotín	Strachotín	k1gMnSc1
•	•	k?
Šakvice	Šakvice	k1gFnSc2
•	•	k?
Šitbořice	Šitbořice	k1gFnSc2
•	•	k?
Tvrdonice	Tvrdonice	k1gFnSc2
•	•	k?
Týnec	Týnec	k1gInSc1
•	•	k?
Uherčice	Uherčice	k1gFnSc2
•	•	k?
Valtice	Valtice	k1gFnPc4
•	•	k?
Velké	velký	k2eAgInPc1d1
Bílovice	Bílovice	k1gInPc1
•	•	k?
Velké	velký	k2eAgInPc1d1
Hostěrádky	Hostěrádek	k1gInPc1
•	•	k?
Velké	velký	k2eAgFnSc2d1
Němčice	Němčice	k1gFnSc2
•	•	k?
Velké	velký	k2eAgFnPc1d1
Pavlovice	Pavlovice	k1gFnPc1
•	•	k?
Vrbice	vrbice	k1gFnSc1
•	•	k?
Zaječí	zaječet	k5eAaPmIp3nS
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
město	město	k1gNnSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
.	.	kIx.
</s>
