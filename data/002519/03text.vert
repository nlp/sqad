<s>
Irwin	Irwin	k1gMnSc1	Irwin
Allen	Allen	k1gMnSc1	Allen
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1926	[number]	k4	1926
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
vůdčích	vůdčí	k2eAgFnPc2d1	vůdčí
osobností	osobnost	k1gFnPc2	osobnost
beatnické	beatnický	k2eAgFnSc2d1	beatnická
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
ruská	ruský	k2eAgFnSc1d1	ruská
Židovka	Židovka	k1gFnSc1	Židovka
levicového	levicový	k2eAgNnSc2d1	levicové
smýšlení	smýšlení	k1gNnSc2	smýšlení
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
trpěla	trpět	k5eAaImAgFnS	trpět
duševní	duševní	k2eAgFnSc7d1	duševní
poruchou	porucha	k1gFnSc7	porucha
(	(	kIx(	(
<g/>
paranoia	paranoia	k1gFnSc1	paranoia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
A.	A.	kA	A.
Ginsberga	Ginsberg	k1gMnSc4	Ginsberg
výrazně	výrazně	k6eAd1	výrazně
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c4	na
Columbia	Columbia	k1gFnSc1	Columbia
College	College	k1gNnPc1	College
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
získal	získat	k5eAaPmAgInS	získat
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
titul	titul	k1gInSc1	titul
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studiích	studie	k1gFnPc6	studie
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
<g/>
,	,	kIx,	,
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
s	s	k7c7	s
celou	cela	k1gFnSc7	cela
pozdější	pozdní	k2eAgInSc1d2	pozdější
Beat	beat	k1gInSc1	beat
generation	generation	k1gInSc1	generation
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
opakovaným	opakovaný	k2eAgFnPc3d1	opakovaná
krádežím	krádež	k1gFnPc3	krádež
a	a	k8xC	a
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
byl	být	k5eAaImAgInS	být
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyloučení	vyloučení	k1gNnSc6	vyloučení
prošel	projít	k5eAaPmAgMnS	projít
řadou	řada	k1gFnSc7	řada
povolání	povolání	k1gNnSc2	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c6	po
Severní	severní	k2eAgFnSc6d1	severní
i	i	k8xC	i
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k8xC	i
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vyhoštěn	vyhostit	k5eAaPmNgInS	vyhostit
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
vládní	vládní	k2eAgInPc4d1	vládní
úřady	úřad	k1gInPc4	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vedl	vést	k5eAaImAgMnS	vést
kampaně	kampaň	k1gFnSc2	kampaň
proti	proti	k7c3	proti
zneužívání	zneužívání	k1gNnSc3	zneužívání
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
byl	být	k5eAaImAgMnS	být
vyhoštěn	vyhostit	k5eAaPmNgMnS	vyhostit
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
-	-	kIx~	-
studenti	student	k1gMnPc1	student
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
zvolili	zvolit	k5eAaPmAgMnP	zvolit
králem	král	k1gMnSc7	král
Majálesu	majáles	k1gInSc2	majáles
a	a	k8xC	a
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
provokace	provokace	k1gFnSc2	provokace
Státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
zadržen	zadržet	k5eAaPmNgMnS	zadržet
a	a	k8xC	a
deportován	deportovat	k5eAaBmNgMnS	deportovat
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
československý	československý	k2eAgInSc4d1	československý
tisk	tisk	k1gInSc4	tisk
jej	on	k3xPp3gInSc4	on
obvinil	obvinit	k5eAaPmAgInS	obvinit
z	z	k7c2	z
kažení	kažení	k1gNnSc2	kažení
československé	československý	k2eAgFnSc2d1	Československá
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
poradil	poradit	k5eAaPmAgMnS	poradit
studentům	student	k1gMnPc3	student
Berkeley	Berkelea	k1gFnSc2	Berkelea
University	universita	k1gFnSc2	universita
před	před	k7c7	před
jejich	jejich	k3xOp3gFnSc7	jejich
protiválečnou	protiválečný	k2eAgFnSc7d1	protiválečná
demonstrací	demonstrace	k1gFnSc7	demonstrace
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ozdobili	ozdobit	k5eAaPmAgMnP	ozdobit
první	první	k4xOgFnPc4	první
řady	řada	k1gFnPc4	řada
květinami	květina	k1gFnPc7	květina
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
demonstrace	demonstrace	k1gFnSc1	demonstrace
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
hnutí	hnutí	k1gNnSc2	hnutí
hippies	hippiesa	k1gFnPc2	hippiesa
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
myšlenky	myšlenka	k1gFnSc2	myšlenka
flower	flower	k1gMnSc1	flower
power	power	k1gMnSc1	power
<g/>
.	.	kIx.	.
</s>
<s>
Zajímal	zajímat	k5eAaImAgMnS	zajímat
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
o	o	k7c4	o
americkou	americký	k2eAgFnSc4d1	americká
<g/>
,	,	kIx,	,
tak	tak	k9	tak
o	o	k7c4	o
evropskou	evropský	k2eAgFnSc4d1	Evropská
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
z	z	k7c2	z
asijské	asijský	k2eAgFnSc2d1	asijská
ho	on	k3xPp3gMnSc4	on
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
především	především	k9	především
indicko-japonský	indickoaponský	k2eAgInSc1d1	indicko-japonský
zen-buddhismus	zenuddhismus	k1gInSc1	zen-buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Výrazem	výraz	k1gInSc7	výraz
jeho	jeho	k3xOp3gFnSc2	jeho
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
sblížení	sblížení	k1gNnSc4	sblížení
západního	západní	k2eAgNnSc2d1	západní
a	a	k8xC	a
východního	východní	k2eAgNnSc2d1	východní
filozofického	filozofický	k2eAgNnSc2d1	filozofické
myšlení	myšlení	k1gNnSc2	myšlení
bylo	být	k5eAaImAgNnS	být
založení	založení	k1gNnSc4	založení
Kerouacovy	Kerouacův	k2eAgFnSc2d1	Kerouacova
školy	škola	k1gFnSc2	škola
osvobozené	osvobozený	k2eAgFnSc2d1	osvobozená
poetiky	poetika	k1gFnSc2	poetika
(	(	kIx(	(
<g/>
Jack	Jack	k1gInSc1	Jack
Kerouac	Kerouac	k1gInSc1	Kerouac
School	School	k1gInSc1	School
of	of	k?	of
Disembodied	Disembodied	k1gInSc1	Disembodied
Poetics	Poetics	k1gInSc1	Poetics
<g/>
)	)	kIx)	)
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
Náropa	Nárop	k1gMnSc2	Nárop
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Boulder	Bouldra	k1gFnPc2	Bouldra
v	v	k7c6	v
Coloradu	Colorado	k1gNnSc6	Colorado
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
a	a	k8xC	a
příležitostně	příležitostně	k6eAd1	příležitostně
zde	zde	k6eAd1	zde
přednášel	přednášet	k5eAaImAgMnS	přednášet
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
dalších	další	k2eAgMnPc2d1	další
beatnických	beatnický	k2eAgMnPc2d1	beatnický
autorů	autor	k1gMnPc2	autor
byl	být	k5eAaImAgInS	být
Ginsbergovi	Ginsberg	k1gMnSc3	Ginsberg
inspirací	inspirace	k1gFnPc2	inspirace
Walt	Walt	k1gMnSc1	Walt
Whitman	Whitman	k1gMnSc1	Whitman
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
humanismus	humanismus	k1gInSc4	humanismus
a	a	k8xC	a
demokratický	demokratický	k2eAgInSc4d1	demokratický
optimismus	optimismus	k1gInSc4	optimismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Carlos	Carlos	k1gMnSc1	Carlos
Williams	Williams	k1gInSc1	Williams
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
neotřelou	otřelý	k2eNgFnSc4d1	neotřelá
dikci	dikce	k1gFnSc4	dikce
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
surrealismus	surrealismus	k1gInSc4	surrealismus
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
experimentoval	experimentovat	k5eAaImAgInS	experimentovat
s	s	k7c7	s
halucinogenními	halucinogenní	k2eAgFnPc7d1	halucinogenní
látkami	látka	k1gFnPc7	látka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
i	i	k9	i
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
a	a	k8xC	a
smýšlení	smýšlení	k1gNnSc6	smýšlení
<g/>
.	.	kIx.	.
</s>
<s>
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
například	například	k6eAd1	například
zastával	zastávat	k5eAaImAgMnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
podání	podání	k1gNnSc1	podání
LSD	LSD	kA	LSD
Chruščovovi	Chruščova	k1gMnSc6	Chruščova
a	a	k8xC	a
Kennedymu	Kennedym	k1gInSc3	Kennedym
by	by	kYmCp3nP	by
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
světovému	světový	k2eAgInSc3d1	světový
míru	mír	k1gInSc3	mír
<g/>
.	.	kIx.	.
</s>
<s>
Allen	Allen	k1gMnSc1	Allen
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
zemřel	zemřít	k5eAaPmAgMnS	zemřít
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1997	[number]	k4	1997
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
70	[number]	k4	70
let	léto	k1gNnPc2	léto
na	na	k7c4	na
selhání	selhání	k1gNnSc4	selhání
jater	játra	k1gNnPc2	játra
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
hepatitidy	hepatitida	k1gFnSc2	hepatitida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
se	s	k7c7	s
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1955	[number]	k4	1955
Allen	Allen	k1gMnSc1	Allen
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
oficiálního	oficiální	k2eAgInSc2d1	oficiální
zrodu	zrod	k1gInSc2	zrod
Beat	beat	k1gInSc1	beat
generation	generation	k1gInSc1	generation
<g/>
,	,	kIx,	,
za	za	k7c2	za
nějž	jenž	k3xRgNnSc2	jenž
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
čtení	čtení	k1gNnSc1	čtení
"	"	kIx"	"
<g/>
šesti	šest	k4xCc6	šest
andělů	anděl	k1gMnPc2	anděl
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
jevišti	jeviště	k1gNnSc6	jeviště
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Ginsberga	Ginsberg	k1gMnSc2	Ginsberg
předčítali	předčítat	k5eAaImAgMnP	předčítat
svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
Kenneth	Kenneth	k1gMnSc1	Kenneth
Rexroth	Rexroth	k1gMnSc1	Rexroth
<g/>
,	,	kIx,	,
Gary	Gara	k1gFnPc1	Gara
Snyder	Snyder	k1gMnSc1	Snyder
<g/>
,	,	kIx,	,
Philip	Philip	k1gMnSc1	Philip
Whalen	Whalen	k1gInSc1	Whalen
<g/>
,	,	kIx,	,
Philip	Philip	k1gInSc1	Philip
Lamantia	Lamantia	k1gFnSc1	Lamantia
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
McClure	McClur	k1gMnSc5	McClur
<g/>
.	.	kIx.	.
</s>
<s>
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
četl	číst	k5eAaImAgMnS	číst
svou	svůj	k3xOyFgFnSc4	svůj
báseň	báseň	k1gFnSc4	báseň
Kvílení	kvílení	k1gNnSc2	kvílení
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
předtím	předtím	k6eAd1	předtím
a	a	k8xC	a
jíž	jenž	k3xRgFnSc3	jenž
doslova	doslova	k6eAd1	doslova
ohromil	ohromit	k5eAaPmAgMnS	ohromit
naslouchající	naslouchající	k2eAgNnSc4d1	naslouchající
publikum	publikum	k1gNnSc4	publikum
-	-	kIx~	-
silou	síla	k1gFnSc7	síla
a	a	k8xC	a
také	také	k9	také
výraznou	výrazný	k2eAgFnSc7d1	výrazná
živou	živý	k2eAgFnSc7d1	živá
recitací	recitace	k1gFnSc7	recitace
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
předčítání	předčítání	k1gNnSc1	předčítání
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
stalo	stát	k5eAaPmAgNnS	stát
legendárním	legendární	k2eAgMnSc7d1	legendární
a	a	k8xC	a
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
byl	být	k5eAaImAgMnS	být
opakovaně	opakovaně	k6eAd1	opakovaně
zván	zván	k2eAgInSc4d1	zván
na	na	k7c4	na
podobné	podobný	k2eAgFnPc4d1	podobná
akce	akce	k1gFnPc4	akce
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
San	San	k1gFnSc2	San
Francisca	Francisc	k1gInSc2	Francisc
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
báseň	báseň	k1gFnSc1	báseň
obrazem	obraz	k1gInSc7	obraz
zoufalství	zoufalství	k1gNnSc4	zoufalství
a	a	k8xC	a
strádání	strádání	k1gNnSc4	strádání
jedné	jeden	k4xCgFnSc2	jeden
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
autorovo	autorův	k2eAgNnSc4d1	autorovo
čtení	čtení	k1gNnSc4	čtení
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
dělalo	dělat	k5eAaImAgNnS	dělat
explozi	exploze	k1gFnSc4	exploze
<g/>
,	,	kIx,	,
závan	závan	k1gInSc4	závan
svobody	svoboda	k1gFnSc2	svoboda
v	v	k7c6	v
zatuchlém	zatuchlý	k2eAgNnSc6d1	zatuchlé
muzeu	muzeum	k1gNnSc6	muzeum
společenských	společenský	k2eAgFnPc2d1	společenská
konvencí	konvence	k1gFnPc2	konvence
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
popularitě	popularita	k1gFnSc3	popularita
Kvílení	kvílení	k1gNnSc2	kvílení
přispělo	přispět	k5eAaPmAgNnS	přispět
soudní	soudní	k2eAgNnSc4d1	soudní
řízení	řízení	k1gNnSc4	řízení
o	o	k7c4	o
šíření	šíření	k1gNnSc4	šíření
nemravné	mravný	k2eNgFnSc2d1	nemravná
literatury	literatura	k1gFnSc2	literatura
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Beat	beat	k1gInSc1	beat
generation	generation	k1gInSc1	generation
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jakýmsi	jakýsi	k3yIgMnSc7	jakýsi
hlasatelem	hlasatel	k1gMnSc7	hlasatel
transcendentních	transcendentní	k2eAgInPc2d1	transcendentní
ideálů	ideál	k1gInPc2	ideál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
pozval	pozvat	k5eAaPmAgMnS	pozvat
ruskou	ruský	k2eAgFnSc4d1	ruská
delegaci	delegace	k1gFnSc4	delegace
při	při	k7c6	při
OSN	OSN	kA	OSN
na	na	k7c6	na
předčítání	předčítání	k1gNnSc6	předčítání
poezie	poezie	k1gFnSc2	poezie
do	do	k7c2	do
Judson	Judsona	k1gFnPc2	Judsona
Church	Churcha	k1gFnPc2	Churcha
na	na	k7c4	na
Washington	Washington	k1gInSc4	Washington
Square	square	k1gInSc4	square
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgNnSc6	jenž
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Peterem	Peter	k1gMnSc7	Peter
Orlovským	orlovský	k2eAgMnSc7d1	orlovský
<g/>
.	.	kIx.	.
</s>
<s>
Orlovsky	orlovsky	k6eAd1	orlovsky
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zdůraznil	zdůraznit	k5eAaPmAgInS	zdůraznit
smysl	smysl	k1gInSc4	smysl
osobního	osobní	k2eAgNnSc2d1	osobní
odhalení	odhalení	k1gNnSc2	odhalení
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
četl	číst	k5eAaImAgMnS	číst
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
svlékl	svléknout	k5eAaPmAgMnS	svléknout
do	do	k7c2	do
spodků	spodek	k1gInPc2	spodek
a	a	k8xC	a
zástupci	zástupce	k1gMnPc7	zástupce
ruské	ruský	k2eAgFnSc2d1	ruská
delegace	delegace	k1gFnSc2	delegace
znejistěli	znejistět	k5eAaPmAgMnP	znejistět
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
Ginsberg	Ginsberg	k1gInSc1	Ginsberg
a	a	k8xC	a
když	když	k8xS	když
vášnivě	vášnivě	k6eAd1	vášnivě
četl	číst	k5eAaImAgMnS	číst
pasáž	pasáž	k1gFnSc4	pasáž
z	z	k7c2	z
"	"	kIx"	"
<g/>
Kvílení	kvílení	k1gNnSc2	kvílení
<g/>
"	"	kIx"	"
o	o	k7c6	o
Molochovi	Moloch	k1gMnSc6	Moloch
<g/>
,	,	kIx,	,
ruští	ruský	k2eAgMnPc1d1	ruský
soudruzi	soudruh	k1gMnPc1	soudruh
vyklidili	vyklidit	k5eAaPmAgMnP	vyklidit
místnost	místnost	k1gFnSc4	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
symbolická	symbolický	k2eAgFnSc1d1	symbolická
lekce	lekce	k1gFnSc1	lekce
politické	politický	k2eAgFnSc2d1	politická
moci	moc	k1gFnSc2	moc
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Allen	Allen	k1gMnSc1	Allen
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
byl	být	k5eAaImAgMnS	být
homosexuál	homosexuál	k1gMnSc1	homosexuál
a	a	k8xC	a
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sexuální	sexuální	k2eAgFnSc7d1	sexuální
orientací	orientace	k1gFnSc7	orientace
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
těžce	těžce	k6eAd1	těžce
vyrovnával	vyrovnávat	k5eAaImAgMnS	vyrovnávat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
sexuálním	sexuální	k2eAgInPc3d1	sexuální
zmatkům	zmatek	k1gInPc3	zmatek
přispěla	přispět	k5eAaPmAgFnS	přispět
nemoc	nemoc	k1gFnSc1	nemoc
Ginsbergovy	Ginsbergův	k2eAgFnSc2d1	Ginsbergova
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Ginsberg	Ginsberg	k1gInSc1	Ginsberg
byl	být	k5eAaImAgInS	být
soužen	soužit	k5eAaImNgInS	soužit
palčivou	palčivý	k2eAgFnSc7d1	palčivá
touhou	touha	k1gFnSc7	touha
po	po	k7c6	po
fyzickém	fyzický	k2eAgInSc6d1	fyzický
kontaktu	kontakt	k1gInSc6	kontakt
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
mohl	moct	k5eAaImAgInS	moct
utišit	utišit	k5eAaPmF	utišit
pouze	pouze	k6eAd1	pouze
masturbací	masturbace	k1gFnSc7	masturbace
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studií	studio	k1gNnPc2	studio
na	na	k7c6	na
Kolumbijské	kolumbijský	k2eAgFnSc6d1	kolumbijská
univerzitě	univerzita	k1gFnSc6	univerzita
mu	on	k3xPp3gNnSc3	on
přátelství	přátelství	k1gNnSc3	přátelství
s	s	k7c7	s
Jackem	Jacek	k1gMnSc7	Jacek
Kerouacem	Kerouace	k1gMnSc7	Kerouace
-	-	kIx~	-
jehož	jehož	k3xOyRp3gFnSc7	jehož
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c2	za
něžného	něžný	k2eAgNnSc2d1	něžné
a	a	k8xC	a
nekonečně	konečně	k6eNd1	konečně
citlivého	citlivý	k2eAgMnSc2d1	citlivý
člověka	člověk	k1gMnSc2	člověk
-	-	kIx~	-
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
spolehlivou	spolehlivý	k2eAgFnSc4d1	spolehlivá
formu	forma	k1gFnSc4	forma
vztahu	vztah	k1gInSc2	vztah
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tak	tak	k6eAd1	tak
zoufale	zoufale	k6eAd1	zoufale
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Kerouacova	Kerouacův	k2eAgFnSc1d1	Kerouacova
tolerance	tolerance	k1gFnSc1	tolerance
a	a	k8xC	a
obrovská	obrovský	k2eAgFnSc1d1	obrovská
empatie	empatie	k1gFnSc1	empatie
dokázala	dokázat	k5eAaPmAgFnS	dokázat
rozptýlit	rozptýlit	k5eAaPmF	rozptýlit
Ginsbergův	Ginsbergův	k2eAgInSc4d1	Ginsbergův
pocit	pocit	k1gInSc4	pocit
viny	vina	k1gFnSc2	vina
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k6eAd1	William
Seward	Seward	k1gInSc1	Seward
Burroughs	Burroughsa	k1gFnPc2	Burroughsa
-	-	kIx~	-
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
viděl	vidět	k5eAaImAgMnS	vidět
náhradního	náhradní	k2eAgMnSc4d1	náhradní
otce	otec	k1gMnSc4	otec
-	-	kIx~	-
mu	on	k3xPp3gMnSc3	on
zase	zase	k9	zase
pomohl	pomoct	k5eAaPmAgInS	pomoct
uvědomit	uvědomit	k5eAaPmF	uvědomit
si	se	k3xPyFc3	se
důvody	důvod	k1gInPc4	důvod
svého	svůj	k3xOyFgInSc2	svůj
strachu	strach	k1gInSc2	strach
a	a	k8xC	a
zábran	zábrana	k1gFnPc2	zábrana
<g/>
.	.	kIx.	.
</s>
<s>
Radil	radit	k5eAaImAgMnS	radit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
smířil	smířit	k5eAaPmAgMnS	smířit
sám	sám	k3xTgMnSc1	sám
se	s	k7c7	s
sebou	se	k3xPyFc7	se
a	a	k8xC	a
jednal	jednat	k5eAaImAgMnS	jednat
dle	dle	k7c2	dle
svých	svůj	k3xOyFgFnPc2	svůj
niterných	niterný	k2eAgFnPc2d1	niterná
potřeb	potřeba	k1gFnPc2	potřeba
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
společenský	společenský	k2eAgInSc4d1	společenský
diktát	diktát	k1gInSc4	diktát
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Nutno	nutno	k6eAd1	nutno
poznamenat	poznamenat	k5eAaPmF	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
homosexualita	homosexualita	k1gFnSc1	homosexualita
brána	brána	k1gFnSc1	brána
jako	jako	k8xS	jako
perverze	perverze	k1gFnSc1	perverze
hraničící	hraničící	k2eAgFnSc1d1	hraničící
s	s	k7c7	s
kriminalitou	kriminalita	k1gFnSc7	kriminalita
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Pro	pro	k7c4	pro
Ginsberga	Ginsberg	k1gMnSc4	Ginsberg
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
obnovení	obnovení	k1gNnSc4	obnovení
nutné	nutný	k2eAgFnSc2d1	nutná
sebeúcty	sebeúcta	k1gFnSc2	sebeúcta
a	a	k8xC	a
zbavilo	zbavit	k5eAaPmAgNnS	zbavit
jej	on	k3xPp3gMnSc4	on
to	ten	k3xDgNnSc1	ten
vtíravého	vtíravý	k2eAgInSc2d1	vtíravý
strachu	strach	k1gInSc2	strach
z	z	k7c2	z
vlastních	vlastní	k2eAgFnPc2d1	vlastní
představ	představa	k1gFnPc2	představa
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
jej	on	k3xPp3gMnSc4	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
Neal	Neal	k1gMnSc1	Neal
Cassady	Cassada	k1gFnSc2	Cassada
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
celoživotním	celoživotní	k2eAgMnSc7d1	celoživotní
partnerem	partner	k1gMnSc7	partner
Peter	Peter	k1gMnSc1	Peter
Orlovsky	orlovsky	k6eAd1	orlovsky
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
díky	díky	k7c3	díky
sanfranciskému	sanfranciský	k2eAgMnSc3d1	sanfranciský
malíři	malíř	k1gMnSc3	malíř
Robertu	Robert	k1gMnSc3	Robert
LaVigne	LaVign	k1gInSc5	LaVign
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vzal	vzít	k5eAaPmAgMnS	vzít
Ginsberga	Ginsberg	k1gMnSc4	Ginsberg
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
bytu	byt	k1gInSc2	byt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
po	po	k7c6	po
celonoční	celonoční	k2eAgFnSc6d1	celonoční
konverzaci	konverzace	k1gFnSc6	konverzace
ve	v	k7c6	v
Fosterově	Fosterův	k2eAgFnSc6d1	Fosterova
kavárně	kavárna	k1gFnSc6	kavárna
ukázal	ukázat	k5eAaPmAgInS	ukázat
svou	svůj	k3xOyFgFnSc4	svůj
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Ginsberg	Ginsberg	k1gInSc1	Ginsberg
spatřil	spatřit	k5eAaPmAgInS	spatřit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
rozměrný	rozměrný	k2eAgInSc1d1	rozměrný
portrét	portrét	k1gInSc1	portrét
nahého	nahý	k2eAgMnSc2d1	nahý
chlapce	chlapec	k1gMnSc2	chlapec
<g/>
.	.	kIx.	.
</s>
<s>
Objekt	objekt	k1gInSc1	objekt
LaVigneova	LaVigneův	k2eAgInSc2d1	LaVigneův
obrazu	obraz	k1gInSc2	obraz
-	-	kIx~	-
Peter	Peter	k1gMnSc1	Peter
Orlovsky	orlovsky	k6eAd1	orlovsky
-	-	kIx~	-
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
pokoje	pokoj	k1gInSc2	pokoj
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
když	když	k8xS	když
jej	on	k3xPp3gInSc4	on
Ginsberg	Ginsberg	k1gInSc4	Ginsberg
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
<g/>
.	.	kIx.	.
</s>
<s>
Orlovsky	orlovsky	k6eAd1	orlovsky
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgMnSc1d2	mladší
a	a	k8xC	a
Allen	Allen	k1gMnSc1	Allen
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
cítil	cítit	k5eAaImAgMnS	cítit
upřímnost	upřímnost	k1gFnSc4	upřímnost
a	a	k8xC	a
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
odezvu	odezva	k1gFnSc4	odezva
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
přijali	přijmout	k5eAaPmAgMnP	přijmout
absolutní	absolutní	k2eAgInSc4d1	absolutní
vzájemný	vzájemný	k2eAgInSc4d1	vzájemný
závazek	závazek	k1gInSc4	závazek
<g/>
.	.	kIx.	.
</s>
<s>
Orlovsky	orlovsky	k6eAd1	orlovsky
naplnil	naplnit	k5eAaPmAgInS	naplnit
Ginsbergovu	Ginsbergův	k2eAgFnSc4d1	Ginsbergova
nezměrnou	změrný	k2eNgFnSc4d1	nezměrná
citovou	citový	k2eAgFnSc4d1	citová
potřebu	potřeba	k1gFnSc4	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Ginsbergova	Ginsbergův	k2eAgFnSc1d1	Ginsbergova
tvorba	tvorba	k1gFnSc1	tvorba
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
hlavních	hlavní	k2eAgInPc2d1	hlavní
tematických	tematický	k2eAgInPc2d1	tematický
okruhů	okruh	k1gInPc2	okruh
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
intimní	intimní	k2eAgFnSc1d1	intimní
poezie	poezie	k1gFnSc1	poezie
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
kritika	kritika	k1gFnSc1	kritika
soudobé	soudobý	k2eAgFnSc2d1	soudobá
americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Ginsbergovy	Ginsbergův	k2eAgFnSc2d1	Ginsbergova
poezie	poezie	k1gFnSc2	poezie
je	být	k5eAaImIp3nS	být
rozmanitá	rozmanitý	k2eAgFnSc1d1	rozmanitá
<g/>
,	,	kIx,	,
od	od	k7c2	od
extatické	extatický	k2eAgFnSc2d1	extatická
euforie	euforie	k1gFnSc2	euforie
až	až	k9	až
po	po	k7c4	po
čiré	čirý	k2eAgNnSc4d1	čiré
zoufalství	zoufalství	k1gNnSc4	zoufalství
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
verše	verš	k1gInPc1	verš
nesou	nést	k5eAaImIp3nP	nést
různorodou	různorodý	k2eAgFnSc4d1	různorodá
náladu	nálada	k1gFnSc4	nálada
<g/>
,	,	kIx,	,
chvíli	chvíle	k1gFnSc4	chvíle
jsou	být	k5eAaImIp3nP	být
paranoidní	paranoidní	k2eAgNnPc1d1	paranoidní
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
důvěrné	důvěrný	k2eAgFnPc1d1	důvěrná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
plné	plný	k2eAgInPc4d1	plný
citu	cit	k1gInSc3	cit
zdůrazňujícího	zdůrazňující	k2eAgInSc2d1	zdůrazňující
nadřazenost	nadřazenost	k1gFnSc4	nadřazenost
ducha	duch	k1gMnSc2	duch
nad	nad	k7c7	nad
materialismem	materialismus	k1gInSc7	materialismus
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
verš	verš	k1gInSc1	verš
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
Ginsbergovi	Ginsberg	k1gMnSc3	Ginsberg
prostor	prostor	k1gInSc4	prostor
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
procesu	proces	k1gInSc2	proces
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
vizuálních	vizuální	k2eAgFnPc6d1	vizuální
představách	představa	k1gFnPc6	představa
<g/>
,	,	kIx,	,
slovech	slovo	k1gNnPc6	slovo
<g/>
,	,	kIx,	,
neustále	neustále	k6eAd1	neustále
odbíhajících	odbíhající	k2eAgFnPc6d1	odbíhající
asociacích	asociace	k1gFnPc6	asociace
a	a	k8xC	a
větvení	větvení	k1gNnSc6	větvení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Johnu	John	k1gMnSc3	John
Hollanderovi	Hollander	k1gMnSc3	Hollander
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chci	chtít	k5eAaImIp1nS	chtít
napsat	napsat	k5eAaBmF	napsat
divokou	divoký	k2eAgFnSc4d1	divoká
stránku	stránka	k1gFnSc4	stránka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
divokou	divoký	k2eAgFnSc4d1	divoká
a	a	k8xC	a
čistou	čistý	k2eAgFnSc4d1	čistá
(	(	kIx(	(
<g/>
opravdu	opravdu	k6eAd1	opravdu
čistou	čistý	k2eAgFnSc4d1	čistá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
mysl	mysl	k1gFnSc1	mysl
-	-	kIx~	-
žádné	žádný	k3yNgNnSc4	žádný
cpaní	cpaní	k1gNnSc4	cpaní
myšlenek	myšlenka	k1gFnPc2	myšlenka
do	do	k7c2	do
svěrací	svěrací	k2eAgFnSc2d1	svěrací
kazajky	kazajka	k1gFnSc2	kazajka
-	-	kIx~	-
něco	něco	k3yInSc1	něco
jako	jako	k8xS	jako
hledání	hledání	k1gNnSc1	hledání
rytmu	rytmus	k1gInSc2	rytmus
myšlenek	myšlenka	k1gFnPc2	myšlenka
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc2	jejich
přirozeného	přirozený	k2eAgInSc2d1	přirozený
výskytu	výskyt	k1gInSc2	výskyt
a	a	k8xC	a
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
a	a	k8xC	a
symbolických	symbolický	k2eAgNnPc2d1	symbolické
paradigmat	paradigma	k1gNnPc2	paradigma
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Básnické	básnický	k2eAgFnPc1d1	básnická
sbírky	sbírka	k1gFnPc1	sbírka
a	a	k8xC	a
básně	báseň	k1gFnPc1	báseň
Howl	Howl	k1gInSc1	Howl
and	and	k?	and
Other	Other	k1gInSc1	Other
Poems	Poems	k1gInSc1	Poems
(	(	kIx(	(
<g/>
Kvílení	kvílení	k1gNnSc1	kvílení
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
básně	báseň	k1gFnPc1	báseň
<g/>
)	)	kIx)	)
-	-	kIx~	-
1956	[number]	k4	1956
<g/>
:	:	kIx,	:
básnická	básnický	k2eAgFnSc1d1	básnická
sbírka	sbírka	k1gFnSc1	sbírka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
žalovaná	žalovaný	k2eAgFnSc1d1	žalovaná
pro	pro	k7c4	pro
údajnou	údajný	k2eAgFnSc4d1	údajná
obscénnost	obscénnost	k1gFnSc4	obscénnost
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
a	a	k8xC	a
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
básní	báseň	k1gFnSc7	báseň
je	být	k5eAaImIp3nS	být
Kvílení	kvílení	k1gNnSc1	kvílení
pro	pro	k7c4	pro
Carla	Carl	k1gMnSc4	Carl
Solomona	Solomon	k1gMnSc4	Solomon
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Se	s	k7c7	s
Solomonem	Solomon	k1gMnSc7	Solomon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přiblížil	přiblížit	k5eAaPmAgMnS	přiblížit
Ginsbergovi	Ginsberg	k1gMnSc3	Ginsberg
surrealismus	surrealismus	k1gInSc4	surrealismus
a	a	k8xC	a
dadaismus	dadaismus	k1gInSc4	dadaismus
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
seznámil	seznámit	k5eAaPmAgMnS	seznámit
ve	v	k7c6	v
Státní	státní	k2eAgFnSc6d1	státní
psychiatrické	psychiatrický	k2eAgFnSc6d1	psychiatrická
léčebně	léčebna	k1gFnSc6	léčebna
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Báseň	báseň	k1gFnSc1	báseň
je	být	k5eAaImIp3nS	být
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
předním	přední	k2eAgMnPc3d1	přední
představitelům	představitel	k1gMnPc3	představitel
Beat	beat	k1gInSc4	beat
generation	generation	k1gInSc1	generation
-	-	kIx~	-
J.	J.	kA	J.
Kerouacovi	Kerouacův	k2eAgMnPc1d1	Kerouacův
<g/>
,	,	kIx,	,
W.	W.	kA	W.
S.	S.	kA	S.
Burroughsovi	Burroughs	k1gMnSc6	Burroughs
a	a	k8xC	a
Nealu	Neal	k1gInSc3	Neal
Cassadymu	Cassadym	k1gInSc2	Cassadym
-	-	kIx~	-
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
formu	forma	k1gFnSc4	forma
rapsodického	rapsodický	k2eAgInSc2d1	rapsodický
nářku	nářek	k1gInSc2	nářek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
litanická	litanický	k2eAgFnSc1d1	litanická
báseň	báseň	k1gFnSc1	báseň
je	být	k5eAaImIp3nS	být
výpovědí	výpověď	k1gFnSc7	výpověď
o	o	k7c4	o
"	"	kIx"	"
<g/>
generaci	generace	k1gFnSc4	generace
zničené	zničený	k2eAgFnSc2d1	zničená
šílenstvím	šílenství	k1gNnPc3	šílenství
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
drog	droga	k1gFnPc2	droga
<g/>
,	,	kIx,	,
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
orientální	orientální	k2eAgFnSc2d1	orientální
filosofie	filosofie	k1gFnSc2	filosofie
i	i	k8xC	i
hudby	hudba	k1gFnSc2	hudba
snažila	snažit	k5eAaImAgFnS	snažit
uniknout	uniknout	k5eAaPmF	uniknout
z	z	k7c2	z
amerického	americký	k2eAgNnSc2d1	americké
pekla	peklo	k1gNnSc2	peklo
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
manifestem	manifest	k1gInSc7	manifest
celé	celá	k1gFnSc2	celá
beat	beat	k1gInSc1	beat
generation	generation	k1gInSc1	generation
<g/>
.	.	kIx.	.
</s>
<s>
Kaddish	Kaddish	k1gInSc1	Kaddish
and	and	k?	and
Other	Other	k1gInSc1	Other
Poems	Poems	k1gInSc1	Poems
(	(	kIx(	(
<g/>
Kadiš	Kadiš	k1gInSc1	Kadiš
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
básně	báseň	k1gFnPc1	báseň
<g/>
)	)	kIx)	)
-	-	kIx~	-
1961	[number]	k4	1961
<g/>
:	:	kIx,	:
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
elegie	elegie	k1gFnSc1	elegie
"	"	kIx"	"
<g/>
Kadiš	Kadiš	k1gInSc1	Kadiš
za	za	k7c7	za
Naomi	Nao	k1gFnPc7	Nao
Ginsbergovou	Ginsbergův	k2eAgFnSc7d1	Ginsbergova
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kadiš	Kadiš	k1gInSc1	Kadiš
je	být	k5eAaImIp3nS	být
židovská	židovský	k2eAgFnSc1d1	židovská
modlitba	modlitba	k1gFnSc1	modlitba
za	za	k7c4	za
mrtvého	mrtvý	k1gMnSc4	mrtvý
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
básníkovu	básníkův	k2eAgFnSc4d1	básníkova
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
téma	téma	k1gNnSc1	téma
je	být	k5eAaImIp3nS	být
rozvinuto	rozvinout	k5eAaPmNgNnS	rozvinout
do	do	k7c2	do
komplexní	komplexní	k2eAgFnSc2d1	komplexní
výpovědi	výpověď	k1gFnSc2	výpověď
o	o	k7c6	o
židovském	židovský	k2eAgInSc6d1	židovský
národě	národ	k1gInSc6	národ
a	a	k8xC	a
vyjádření	vyjádření	k1gNnSc6	vyjádření
autorových	autorův	k2eAgInPc2d1	autorův
názorů	názor	k1gInPc2	názor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
psána	psát	k5eAaImNgFnS	psát
volným	volný	k2eAgInSc7d1	volný
veršem	verš	k1gInSc7	verš
<g/>
.	.	kIx.	.
</s>
<s>
Empty	Empt	k1gInPc1	Empt
Mirror	Mirrora	k1gFnPc2	Mirrora
<g/>
:	:	kIx,	:
Early	earl	k1gMnPc4	earl
Poems	Poemsa	k1gFnPc2	Poemsa
(	(	kIx(	(
<g/>
Prázdné	prázdný	k2eAgNnSc1d1	prázdné
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
<g/>
)	)	kIx)	)
-	-	kIx~	-
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Reality	realita	k1gFnPc1	realita
Sandwiches	Sandwichesa	k1gFnPc2	Sandwichesa
(	(	kIx(	(
<g/>
Sendviče	sendvič	k1gInPc1	sendvič
reality	realita	k1gFnSc2	realita
<g/>
)	)	kIx)	)
-	-	kIx~	-
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
King	King	k1gMnSc1	King
of	of	k?	of
a	a	k8xC	a
May	May	k1gMnSc1	May
Day	Day	k1gMnSc1	Day
parade	parást	k5eAaPmIp3nS	parást
(	(	kIx(	(
<g/>
Král	Král	k1gMnSc1	Král
majálesu	majáles	k1gInSc2	majáles
<g/>
)	)	kIx)	)
-	-	kIx~	-
1965	[number]	k4	1965
<g/>
:	:	kIx,	:
báseň	báseň	k1gFnSc1	báseň
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
napsal	napsat	k5eAaBmAgInS	napsat
v	v	k7c6	v
letadle	letadlo	k1gNnSc6	letadlo
po	po	k7c6	po
vyhoštění	vyhoštění	k1gNnSc6	vyhoštění
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
je	být	k5eAaImIp3nS	být
výrazem	výraz	k1gInSc7	výraz
odporu	odpor	k1gInSc2	odpor
k	k	k7c3	k
totalitnímu	totalitní	k2eAgInSc3d1	totalitní
komunistickému	komunistický	k2eAgInSc3d1	komunistický
režimu	režim	k1gInSc3	režim
a	a	k8xC	a
sympatií	sympatie	k1gFnPc2	sympatie
s	s	k7c7	s
českými	český	k2eAgMnPc7d1	český
studenty	student	k1gMnPc7	student
<g/>
.	.	kIx.	.
</s>
<s>
Planet	planeta	k1gFnPc2	planeta
News	Newsa	k1gFnPc2	Newsa
-	-	kIx~	-
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Fall	Fall	k1gMnSc1	Fall
of	of	k?	of
America	America	k1gMnSc1	America
<g/>
:	:	kIx,	:
Poems	Poems	k1gInSc1	Poems
of	of	k?	of
These	these	k1gFnSc1	these
States	States	k1gMnSc1	States
-	-	kIx~	-
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Iron	iron	k1gInSc1	iron
Horse	Horse	k1gFnSc2	Horse
-	-	kIx~	-
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Mind	Mind	k1gInSc1	Mind
Breaths	Breaths	k1gInSc1	Breaths
-	-	kIx~	-
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Collected	Collected	k1gInSc1	Collected
poems	poems	k6eAd1	poems
1947-1980	[number]	k4	1947-1980
-	-	kIx~	-
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
White	White	k5eAaPmIp2nP	White
Shroud	Shroud	k1gInSc4	Shroud
<g/>
:	:	kIx,	:
Poems	Poems	k1gInSc1	Poems
1980-1985	[number]	k4	1980-1985
-	-	kIx~	-
[	[	kIx(	[
<g/>
1986	[number]	k4	1986
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Česky	česky	k6eAd1	česky
Kvílení	kvílení	k1gNnSc1	kvílení
-	-	kIx~	-
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Vylízanej	Vylízanej	k?	Vylízanej
mozek	mozek	k1gInSc1	mozek
-	-	kIx~	-
Birthbrain	Birthbrain	k1gInSc1	Birthbrain
<g/>
:	:	kIx,	:
básně	báseň	k1gFnPc1	báseň
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1965	[number]	k4	1965
<g/>
-	-	kIx~	-
<g/>
'	'	kIx"	'
<g/>
90	[number]	k4	90
-	-	kIx~	-
Vokno	Vokno	k1gNnSc1	Vokno
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
1991	[number]	k4	1991
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
marihuanový	marihuanový	k2eAgInSc1d1	marihuanový
švindl	švindl	k?	švindl
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Great	Great	k1gInSc1	Great
Marijuana	Marijuana	k1gFnSc1	Marijuana
Hoax	Hoax	k1gInSc1	Hoax
-	-	kIx~	-
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Slovy	slovo	k1gNnPc7	slovo
a	a	k8xC	a
dechem	dech	k1gInSc7	dech
-	-	kIx~	-
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Karma	karma	k1gFnSc1	karma
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
-	-	kIx~	-
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
poezie	poezie	k1gFnSc2	poezie
<g/>
;	;	kIx,	;
textovou	textový	k2eAgFnSc4d1	textová
část	část	k1gFnSc4	část
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
editorův	editorův	k2eAgInSc1d1	editorův
esej	esej	k1gInSc1	esej
<g/>
,	,	kIx,	,
bohatý	bohatý	k2eAgInSc1d1	bohatý
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
materiál	materiál	k1gInSc1	materiál
včetně	včetně	k7c2	včetně
ohlasů	ohlas	k1gInPc2	ohlas
na	na	k7c4	na
Ginsbergovo	Ginsbergův	k2eAgNnSc4d1	Ginsbergovo
dílo	dílo	k1gNnSc4	dílo
a	a	k8xC	a
rozhovor	rozhovor	k1gInSc4	rozhovor
s	s	k7c7	s
autorem	autor	k1gMnSc7	autor
i	i	k8xC	i
řada	řada	k1gFnSc1	řada
obrazových	obrazový	k2eAgInPc2d1	obrazový
dokumentů	dokument	k1gInPc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Neposílejte	posílat	k5eNaImRp2nP	posílat
mi	já	k3xPp1nSc3	já
už	už	k6eAd1	už
žádné	žádný	k3yNgInPc1	žádný
dopisy	dopis	k1gInPc1	dopis
-	-	kIx~	-
Maťa	Mať	k1gInSc2	Mať
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Nejobsáhlejší	obsáhlý	k2eAgInSc1d3	nejobsáhlejší
český	český	k2eAgInSc1d1	český
výbor	výbor	k1gInSc1	výbor
podrobně	podrobně	k6eAd1	podrobně
mapuje	mapovat	k5eAaImIp3nS	mapovat
Ginsbergovu	Ginsbergův	k2eAgFnSc4d1	Ginsbergova
tvorbu	tvorba	k1gFnSc4	tvorba
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1947	[number]	k4	1947
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Knihu	kniha	k1gFnSc4	kniha
doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
autorovy	autorův	k2eAgFnPc1d1	autorova
ilustrace	ilustrace	k1gFnPc1	ilustrace
<g/>
.	.	kIx.	.
</s>
<s>
Kvílení	kvílení	k1gNnSc1	kvílení
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Howl	Howl	k1gInSc1	Howl
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
film	film	k1gInSc1	film
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc1	scénář
a	a	k8xC	a
režie	režie	k1gFnSc1	režie
Rob	roba	k1gFnPc2	roba
Epstein	Epsteina	k1gFnPc2	Epsteina
<g/>
,	,	kIx,	,
Jeffrey	Jeffrea	k1gFnSc2	Jeffrea
Friedman	Friedman	k1gMnSc1	Friedman
<g/>
.	.	kIx.	.
90	[number]	k4	90
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
životopisný	životopisný	k2eAgInSc1d1	životopisný
<g/>
.	.	kIx.	.
</s>
<s>
Zbav	zbavit	k5eAaPmRp2nS	zbavit
se	se	k3xPyFc4	se
svých	svůj	k3xOyFgMnPc2	svůj
miláčků	miláček	k1gMnPc2	miláček
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Kill	Kill	k1gInSc1	Kill
Your	Your	k1gMnSc1	Your
Darlings	Darlings	k1gInSc1	Darlings
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
film	film	k1gInSc1	film
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc1	scénář
a	a	k8xC	a
režie	režie	k1gFnSc1	režie
John	John	k1gMnSc1	John
Krokidas	Krokidas	k1gMnSc1	Krokidas
<g/>
.	.	kIx.	.
104	[number]	k4	104
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Životopisný	životopisný	k2eAgInSc1d1	životopisný
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
<g/>
.	.	kIx.	.
</s>
