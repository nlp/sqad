<s>
Excalibur	Excalibura	k1gFnPc2	Excalibura
(	(	kIx(	(
<g/>
čti	číst	k5eAaImRp2nS	číst
ekskalibr	ekskalibr	k1gInSc1	ekskalibr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
legendární	legendární	k2eAgInSc4d1	legendární
meč	meč	k1gInSc4	meč
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
používal	používat	k5eAaImAgMnS	používat
král	král	k1gMnSc1	král
Artuš	Artuš	k1gMnSc1	Artuš
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
obdařen	obdařit	k5eAaPmNgInS	obdařit
magickými	magický	k2eAgFnPc7d1	magická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
či	či	k8xC	či
spojován	spojovat	k5eAaImNgInS	spojovat
se	s	k7c7	s
suverenitou	suverenita	k1gFnSc7	suverenita
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
bývá	bývat	k5eAaImIp3nS	bývat
zaměňován	zaměňován	k2eAgInSc1d1	zaměňován
s	s	k7c7	s
"	"	kIx"	"
<g/>
Mečem	meč	k1gInSc7	meč
v	v	k7c6	v
kameni	kámen	k1gInSc6	kámen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
tyto	tento	k3xDgInPc1	tento
meče	meč	k1gInPc1	meč
nezávisle	závisle	k6eNd1	závisle
(	(	kIx(	(
<g/>
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
výjimek	výjimka	k1gFnPc2	výjimka
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
filmů	film	k1gInPc2	film
na	na	k7c4	na
Artušovské	artušovský	k2eAgNnSc4d1	artušovský
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
Excalibur	Excalibur	k1gMnSc1	Excalibur
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
verzí	verze	k1gFnPc2	verze
pověsti	pověst	k1gFnSc2	pověst
dostane	dostat	k5eAaPmIp3nS	dostat
Artuš	Artuš	k1gMnSc1	Artuš
Excalibur	Excalibur	k1gMnSc1	Excalibur
od	od	k7c2	od
Paní	paní	k1gFnSc2	paní
Jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
nad	nad	k7c4	nad
hladinu	hladina	k1gFnSc4	hladina
vynoří	vynořit	k5eAaPmIp3nS	vynořit
její	její	k3xOp3gFnSc1	její
ruka	ruka	k1gFnSc1	ruka
držící	držící	k2eAgFnSc1d1	držící
meč	meč	k1gInSc1	meč
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Artuš	Artuš	k1gMnSc1	Artuš
měl	mít	k5eAaImAgMnS	mít
mít	mít	k5eAaImF	mít
meč	meč	k1gInSc4	meč
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
až	až	k6eAd1	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
poslední	poslední	k2eAgFnSc2d1	poslední
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byl	být	k5eAaImAgMnS	být
vzat	vzít	k5eAaPmNgMnS	vzít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
meč	meč	k1gInSc1	meč
i	i	k9	i
vrácen	vrácen	k2eAgMnSc1d1	vrácen
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
odhození	odhození	k1gNnSc6	odhození
do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
vynoří	vynořit	k5eAaPmIp3nS	vynořit
ruka	ruka	k1gFnSc1	ruka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gMnSc4	on
zachytí	zachytit	k5eAaPmIp3nS	zachytit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
byl	být	k5eAaImAgInS	být
ukován	ukovat	k5eAaPmNgInS	ukovat
jako	jako	k8xS	jako
dar	dar	k1gInSc1	dar
pro	pro	k7c4	pro
Julia	Julius	k1gMnSc4	Julius
Caesara	Caesar	k1gMnSc4	Caesar
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
symbolem	symbol	k1gInSc7	symbol
královské	královský	k2eAgFnSc3d1	královská
moci	moc	k1gFnSc3	moc
na	na	k7c6	na
britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
film	film	k1gInSc1	film
Poslední	poslední	k2eAgFnSc1d1	poslední
legie	legie	k1gFnSc1	legie
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
tohoto	tento	k3xDgInSc2	tento
meče	meč	k1gInSc2	meč
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
velice	velice	k6eAd1	velice
sporná	sporný	k2eAgFnSc1d1	sporná
-	-	kIx~	-
všechny	všechen	k3xTgInPc4	všechen
podklady	podklad	k1gInPc4	podklad
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
máme	mít	k5eAaImIp1nP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
příběhy	příběh	k1gInPc1	příběh
nebo	nebo	k8xC	nebo
legendy	legenda	k1gFnPc1	legenda
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
vědecky	vědecky	k6eAd1	vědecky
či	či	k8xC	či
historicky	historicky	k6eAd1	historicky
podložená	podložený	k2eAgNnPc1d1	podložené
fakta	faktum	k1gNnPc1	faktum
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
tedy	tedy	k9	tedy
jen	jen	k9	jen
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
excalibur	excalibur	k1gMnSc1	excalibur
doopravdy	doopravdy	k6eAd1	doopravdy
existoval	existovat	k5eAaImAgMnS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
spojována	spojován	k2eAgFnSc1d1	spojována
s	s	k7c7	s
kouzelníkem	kouzelník	k1gMnSc7	kouzelník
Merlinem	Merlin	k1gInSc7	Merlin
<g/>
.	.	kIx.	.
</s>
