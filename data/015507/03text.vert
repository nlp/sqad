<s>
Ford	ford	k1gInSc1
Motor	motor	k1gInSc1
Company	Compana	k1gFnSc2
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Ford	ford	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c4
výrobci	výrobce	k1gMnPc1
automobilů	automobil	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Ford	ford	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ford	ford	k1gInSc1
Motor	motor	k1gInSc1
Company	Compana	k1gFnSc2
Logo	logo	k1gNnSc4
Základní	základní	k2eAgInPc4d1
údaje	údaj	k1gInPc4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
1903	#num#	k4
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Henry	henry	k1gInSc4
Ford	ford	k1gInSc1
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Dearborn	Dearborn	k1gInSc1
<g/>
,	,	kIx,
Michigan	Michigan	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Dearborn	Dearborn	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
(	(	kIx(
<g/>
Ford	ford	k1gInSc1
World	World	k1gMnSc1
Headquarters	Headquarters	k1gInSc1
<g/>
)	)	kIx)
Klíčoví	klíčový	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
</s>
<s>
William	William	k1gInSc1
C.	C.	kA
Ford	ford	k1gInSc1
<g/>
,	,	kIx,
Jr	Jr	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alan	alan	k1gInSc1
R.	R.	kA
Mullaly	Mullaly	k1gFnSc1
Charakteristika	charakteristika	k1gFnSc1
firmy	firma	k1gFnSc2
Oblast	oblast	k1gFnSc1
činnosti	činnost	k1gFnSc2
</s>
<s>
Strojírenský	strojírenský	k2eAgMnSc1d1
Produkty	produkt	k1gInPc1
</s>
<s>
Automobily	automobil	k1gInPc1
<g/>
,	,	kIx,
automobilové	automobilový	k2eAgFnPc1d1
části	část	k1gFnPc1
Tržní	tržní	k2eAgFnSc2d1
kapitalizace	kapitalizace	k1gFnSc2
</s>
<s>
61	#num#	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
(	(	kIx(
<g/>
I	I	kA
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Obrat	obrat	k1gInSc1
</s>
<s>
118,3	118,3	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Výsledek	výsledek	k1gInSc1
hospodaření	hospodaření	k1gNnSc2
</s>
<s>
2,7	2,7	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Celková	celkový	k2eAgNnPc1d1
aktiva	aktivum	k1gNnPc1
</s>
<s>
194,9	194,9	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Vlastní	vlastní	k2eAgInSc1d1
kapitál	kapitál	k1gInSc1
</s>
<s>
-6,5	-6,5	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Zaměstnanci	zaměstnanec	k1gMnPc1
</s>
<s>
159	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Majitelé	majitel	k1gMnPc1
</s>
<s>
Ford	ford	k1gInSc1
Credit	Credit	k1gFnPc2
Ford	ford	k1gInSc1
division	division	k1gInSc4
Lincoln	Lincoln	k1gMnSc1
Mercury	Mercura	k1gFnSc2
Dceřiné	dceřiný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
</s>
<s>
Otosan	Otosan	k1gInSc1
(	(	kIx(
<g/>
41	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
Ford	ford	k1gInSc1
Air	Air	k1gFnSc2
Transport	transporta	k1gFnPc2
ServiceFord	ServiceForda	k1gFnPc2
Global	globat	k5eAaImAgInS
TechnologiesFord	TechnologiesFord	k1gInSc1
Motor	motor	k1gInSc1
CompanyMercury	CompanyMercura	k1gFnSc2
Ocenění	ocenění	k1gNnSc2
</s>
<s>
Silver	Silver	k1gInSc1
Anvil	Anvila	k1gFnPc2
Awards	Awardsa	k1gFnPc2
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
<g/>
Silver	Silvero	k1gNnPc2
Anvil	Anvila	k1gFnPc2
Awards	Awardsa	k1gFnPc2
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
<g/>
Silver	Silvero	k1gNnPc2
Anvil	Anvila	k1gFnPc2
Awards	Awardsa	k1gFnPc2
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
Identifikátory	identifikátor	k1gInPc1
Oficiální	oficiální	k2eAgInPc1d1
web	web	k1gInSc4
</s>
<s>
http://www.ford.com	http://www.ford.com	k1gInSc1
http://www.ford.cz	http://www.ford.cz	k1gInSc1
ISIN	ISIN	kA
</s>
<s>
US3453708600	US3453708600	k4
LEI	lei	k1gInSc2
</s>
<s>
20S05OYHG0MQM4VUIC57	20S05OYHG0MQM4VUIC57	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ford	ford	k1gInSc1
Motor	motor	k1gInSc1
Company	company	k1gFnSc1
(	(	kIx(
<g/>
často	často	k6eAd1
nazývaná	nazývaný	k2eAgFnSc1d1
zkráceně	zkráceně	k6eAd1
Ford	ford	k1gInSc1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
také	také	k9
FoMoCo	FoMoCo	k1gNnSc1
<g/>
,	,	kIx,
Ford	ford	k1gInSc1
Motor	motor	k1gInSc1
nebo	nebo	k8xC
FMC	FMC	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americká	americký	k2eAgFnSc1d1
nadnárodní	nadnárodní	k2eAgFnSc1d1
korporace	korporace	k1gFnSc1
<g/>
,	,	kIx,
vyrábějící	vyrábějící	k2eAgInPc1d1
automobily	automobil	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
výrobce	výrobce	k1gMnPc4
automobilů	automobil	k1gInPc2
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
Henrym	Henry	k1gMnSc7
a	a	k8xC
jeho	jeho	k3xOp3gMnSc7
synem	syn	k1gMnSc7
Edselem	Edsel	k1gMnSc7
Fordem	ford	k1gInSc7
v	v	k7c6
Dearbornu	Dearborno	k1gNnSc6
<g/>
,	,	kIx,
na	na	k7c6
předměstí	předměstí	k1gNnSc6
Detroitu	Detroit	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
státě	stát	k1gInSc6
Michigan	Michigan	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
(	(	kIx(
<g/>
kde	kde	k6eAd1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
vedení	vedení	k1gNnSc2
společnosti	společnost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1903	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
počátku	počátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byl	být	k5eAaImAgInS
Ford	ford	k1gInSc1
společně	společně	k6eAd1
s	s	k7c7
firmami	firma	k1gFnPc7
General	General	k1gFnSc1
Motors	Motors	k1gInSc1
a	a	k8xC
Chrysler	Chrysler	k1gInSc1
členem	člen	k1gInSc7
Detroitské	Detroitský	k2eAgFnSc2d1
„	„	k?
<g/>
Velké	velká	k1gFnSc2
trojky	trojka	k1gFnSc2
<g/>
“	“	k?
výrobců	výrobce	k1gMnPc2
automobilů	automobil	k1gInPc2
společnosti	společnost	k1gFnSc3
<g/>
,	,	kIx,
které	který	k3yIgFnSc3,k3yRgFnSc3,k3yQgFnSc3
dominovaly	dominovat	k5eAaImAgInP
americkému	americký	k2eAgInSc3d1
automobilovému	automobilový	k2eAgInSc3d1
trhu	trh	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozvoj	rozvoj	k1gInSc1
sériové	sériový	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
</s>
<s>
Ford	ford	k1gInSc1
zavedl	zavést	k5eAaPmAgInS
metody	metoda	k1gFnPc4
masové	masový	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
vozidel	vozidlo	k1gNnPc2
a	a	k8xC
hromadného	hromadný	k2eAgInSc2d1
managementu	management	k1gInSc2
průmyslové	průmyslový	k2eAgFnSc2d1
pracovní	pracovní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ford	ford	k1gInSc1
uskutečnil	uskutečnit	k5eAaPmAgInS
nápady	nápad	k1gInPc4
Eli	Eli	k1gMnSc4
Whitneyho	Whitney	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
vyvinul	vyvinout	k5eAaPmAgInS
jednu	jeden	k4xCgFnSc4
z	z	k7c2
prvních	první	k4xOgFnPc2
montážních	montážní	k2eAgFnPc2d1
linek	linka	k1gFnPc2
s	s	k7c7
použitím	použití	k1gNnSc7
univerzálních	univerzální	k2eAgFnPc2d1
součástek	součástka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
uvést	uvést	k5eAaPmF
do	do	k7c2
provozu	provoz	k1gInSc2
velmi	velmi	k6eAd1
levnou	levný	k2eAgFnSc4d1
<g/>
,	,	kIx,
opakovatelnou	opakovatelný	k2eAgFnSc4d1
a	a	k8xC
spolehlivou	spolehlivý	k2eAgFnSc4d1
výrobu	výroba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Použití	použití	k1gNnSc1
pásové	pásový	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
k	k	k7c3
pohybu	pohyb	k1gInSc3
vozů	vůz	k1gInPc2
k	k	k7c3
dělníkům	dělník	k1gMnPc3
bylo	být	k5eAaImAgNnS
v	v	k7c6
tomto	tento	k3xDgNnSc6
průmyslovém	průmyslový	k2eAgNnSc6d1
odvětví	odvětví	k1gNnSc6
unikátní	unikátní	k2eAgFnPc1d1
a	a	k8xC
rychle	rychle	k6eAd1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
oblíbeným	oblíbený	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
se	se	k3xPyFc4
totiž	totiž	k9
stala	stát	k5eAaPmAgFnS
práce	práce	k1gFnSc1
jednoduchou	jednoduchý	k2eAgFnSc4d1
a	a	k8xC
opakující	opakující	k2eAgFnSc4d1
se	se	k3xPyFc4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
najmout	najmout	k5eAaPmF
nezkušené	zkušený	k2eNgMnPc4d1
pracovníky	pracovník	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
mohli	moct	k5eAaImAgMnP
rychle	rychle	k6eAd1
naučit	naučit	k5eAaPmF
jednoduché	jednoduchý	k2eAgInPc4d1
úkoly	úkol	k1gInPc4
(	(	kIx(
<g/>
ačkoli	ačkoli	k8xS
to	ten	k3xDgNnSc1
odstranilo	odstranit	k5eAaPmAgNnS
téměř	téměř	k6eAd1
veškeré	veškerý	k3xTgNnSc4
uspokojení	uspokojení	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
mohl	moct	k5eAaImAgMnS
mít	mít	k5eAaImF
dělník	dělník	k1gMnSc1
vykonávající	vykonávající	k2eAgFnSc2d1
různorodé	různorodý	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
umožnilo	umožnit	k5eAaPmAgNnS
to	ten	k3xDgNnSc1
výrobu	výroba	k1gFnSc4
prvního	první	k4xOgNnSc2
světového	světový	k2eAgNnSc2d1
auta	auto	k1gNnSc2
Ford	ford	k1gInSc1
model	model	k1gInSc4
T	T	kA
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
jako	jako	k8xC,k8xS
první	první	k4xOgMnSc1
cenově	cenově	k6eAd1
dostupný	dostupný	k2eAgInSc1d1
automobil	automobil	k1gInSc1
vytvořil	vytvořit	k5eAaPmAgInS
masovou	masový	k2eAgFnSc4d1
motorizaci	motorizace	k1gFnSc4
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Ford	ford	k1gInSc1
model	model	k1gInSc1
T.	T.	kA
</s>
<s>
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
Česká	český	k2eAgFnSc1d1
pobočka	pobočka	k1gFnSc1
Ford	ford	k1gInSc1
Motor	motor	k1gInSc1
Company	Compana	k1gFnSc2
(	(	kIx(
<g/>
Ford	ford	k1gInSc1
Motor	motor	k1gInSc1
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
r.	r.	kA
o.	o.	k?
<g/>
)	)	kIx)
zajišťuje	zajišťovat	k5eAaImIp3nS
dovoz	dovoz	k1gInSc4
a	a	k8xC
distribuci	distribuce	k1gFnSc4
osobních	osobní	k2eAgInPc2d1
a	a	k8xC
užitkových	užitkový	k2eAgInPc2d1
automobilů	automobil	k1gInPc2
značky	značka	k1gFnSc2
Ford	ford	k1gInSc1
do	do	k7c2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc1
centrála	centrála	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
Praze	Praha	k1gFnSc6
ve	v	k7c6
čtvrti	čtvrt	k1gFnSc6
Karlín	Karlín	k1gInSc1
v	v	k7c6
ulici	ulice	k1gFnSc6
Karolinská	karolinský	k2eAgFnSc1d1
654	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Ford	ford	k1gInSc1
v	v	k7c6
rámci	rámec	k1gInSc6
úspor	úspora	k1gFnPc2
(	(	kIx(
<g/>
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
General	General	k1gFnSc1
Motors	Motors	k1gInSc1
<g/>
)	)	kIx)
převedl	převést	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
některé	některý	k3yIgFnPc4
ze	z	k7c2
svých	svůj	k3xOyFgFnPc2
kanceláří	kancelář	k1gFnPc2
do	do	k7c2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
do	do	k7c2
Olomouce	Olomouc	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
čeští	český	k2eAgMnPc1d1
pracovníci	pracovník	k1gMnPc1
vykonávají	vykonávat	k5eAaImIp3nP
administrativní	administrativní	k2eAgInSc4d1
servis	servis	k1gInSc4
(	(	kIx(
<g/>
claims	claims	k6eAd1
<g/>
,	,	kIx,
supply	suppnout	k5eAaPmAgFnP
chain	chain	k2eAgInSc4d1
management	management	k1gInSc4
<g/>
)	)	kIx)
pro	pro	k7c4
americkou	americký	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
Ford	ford	k1gInSc1
Motor	motor	k1gInSc1
Company	Compana	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gMnPc4
autorizované	autorizovaný	k2eAgMnPc4d1
prodejce	prodejce	k1gMnPc4
vozů	vůz	k1gInPc2
Ford	ford	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavostí	zajímavost	k1gFnPc2
také	také	k9
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
má	mít	k5eAaImIp3nS
mnoho	mnoho	k4c1
dodavatelů	dodavatel	k1gMnPc2
Fordu	ford	k1gInSc6
své	svůj	k3xOyFgFnSc2
továrny	továrna	k1gFnSc2
<g/>
,	,	kIx,
např.	např.	kA
firmy	firma	k1gFnPc1
Nemak	Nemak	k1gMnSc1
Czech	Czech	k1gMnSc1
Republic	Republice	k1gFnPc2
a	a	k8xC
Starcam	Starcam	k1gInSc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
jsou	být	k5eAaImIp3nP
výrobci	výrobce	k1gMnPc7
hliníkových	hliníkový	k2eAgFnPc2d1
hlav	hlava	k1gFnPc2
motorů	motor	k1gInPc2
pro	pro	k7c4
automobily	automobil	k1gInPc4
<g/>
,	,	kIx,
dále	daleko	k6eAd2
RAI	RAI	kA
Most	most	k1gInSc1
<g/>
,	,	kIx,
výrobce	výrobce	k1gMnSc1
interiérových	interiérový	k2eAgInPc2d1
prvků	prvek	k1gInPc2
z	z	k7c2
polyretanu	polyretan	k1gInSc2
pro	pro	k7c4
automobilový	automobilový	k2eAgInSc4d1
průmysl	průmysl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osram	Osram	k1gInSc1
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
vyrábí	vyrábět	k5eAaImIp3nS
a	a	k8xC
dodává	dodávat	k5eAaImIp3nS
jako	jako	k9
prvnímu	první	k4xOgNnSc3
autu	aut	k1gInSc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
novému	nový	k2eAgMnSc3d1
Mustangu	mustang	k1gMnSc3
2010	#num#	k4
<g/>
,	,	kIx,
variabilní	variabilní	k2eAgNnSc4d1
diodové	diodový	k2eAgNnSc4d1
osvětlení	osvětlení	k1gNnSc4
<g/>
;	;	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
systém	systém	k1gInSc4
Osram	Osram	k1gInSc1
Joule	joule	k1gInSc1
Led	led	k1gInSc1
a	a	k8xC
Topled	Topled	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Výrobce	výrobce	k1gMnSc1
pneumatik	pneumatika	k1gFnPc2
Barum	barum	k1gInSc1
Continental	Continental	k1gMnSc1
vyrábí	vyrábět	k5eAaImIp3nS
pro	pro	k7c4
modely	model	k1gInPc4
automobilů	automobil	k1gInPc2
Ford	ford	k1gInSc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
třeba	třeba	k9
pneumatiky	pneumatika	k1gFnPc1
ContiSportContact	ContiSportContact	k1gInSc1
3	#num#	k4
pro	pro	k7c4
Ford	ford	k1gInSc4
Focus	Focus	k1gInSc1
RS	RS	kA
<g/>
,	,	kIx,
v	v	k7c6
Otrokovicích	Otrokovice	k1gFnPc6
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prodeje	prodej	k1gInPc1
vozů	vůz	k1gInPc2
v	v	k7c6
ČR	ČR	kA
</s>
<s>
Ford	ford	k1gInSc1
je	být	k5eAaImIp3nS
nejúspěšnějším	úspěšný	k2eAgMnSc7d3
automobilovým	automobilový	k2eAgMnSc7d1
importérem	importér	k1gMnSc7
do	do	k7c2
ČR	ČR	kA
za	za	k7c4
roky	rok	k1gInPc4
2007	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
a	a	k8xC
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
zmíněného	zmíněný	k2eAgInSc2d1
úspěchů	úspěch	k1gInPc2
znamená	znamenat	k5eAaImIp3nS
18	#num#	k4
516	#num#	k4
nově	nově	k6eAd1
zaregistrovaných	zaregistrovaný	k2eAgInPc2d1
Fordů	ford	k1gInPc2
za	za	k7c4
rok	rok	k1gInSc4
2009	#num#	k4
také	také	k6eAd1
absolutní	absolutní	k2eAgInSc4d1
rekord	rekord	k1gInSc4
mezi	mezi	k7c7
všemi	všecek	k3xTgMnPc7
dovozci	dovozce	k1gMnPc7
v	v	k7c6
historii	historie	k1gFnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nejprodávanějším	prodávaný	k2eAgInSc7d3
Fordem	ford	k1gInSc7
v	v	k7c6
ČR	ČR	kA
je	být	k5eAaImIp3nS
Ford	ford	k1gInSc1
Fusion	Fusion	k1gInSc1
<g/>
,	,	kIx,
následovaný	následovaný	k2eAgInSc1d1
Fordem	ford	k1gInSc7
Focus	Focus	k1gInSc1
a	a	k8xC
užitkovým	užitkový	k2eAgInSc7d1
Fordem	ford	k1gInSc7
Transit	transit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejném	stejný	k2eAgNnSc6d1
pořadí	pořadí	k1gNnSc6
tyto	tento	k3xDgInPc4
modely	model	k1gInPc4
také	také	k9
okupují	okupovat	k5eAaBmIp3nP
stupně	stupeň	k1gInPc1
vítězů	vítěz	k1gMnPc2
nejvíce	nejvíce	k6eAd1,k6eAd3
prodávaných	prodávaný	k2eAgInPc2d1
dovážených	dovážený	k2eAgInPc2d1
vozů	vůz	k1gInPc2
v	v	k7c6
ČR	ČR	kA
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
2006	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
2009	#num#	k4
</s>
<s>
Podíl	podíl	k1gInSc1
na	na	k7c6
českém	český	k2eAgInSc6d1
trhu	trh	k1gInSc6
</s>
<s>
6,3	6,3	k4
%	%	kIx~
</s>
<s>
6,5	6,5	k4
%	%	kIx~
</s>
<s>
7,5	7,5	k4
%	%	kIx~
</s>
<s>
10,2	10,2	k4
%	%	kIx~
</s>
<s>
Osobní	osobní	k2eAgInPc4d1
vozy	vůz	k1gInPc4
</s>
<s>
5	#num#	k4
414	#num#	k4
</s>
<s>
8	#num#	k4
474	#num#	k4
</s>
<s>
14	#num#	k4
658	#num#	k4
</s>
<s>
16	#num#	k4
054	#num#	k4
</s>
<s>
Užitkové	užitkový	k2eAgInPc1d1
vozy	vůz	k1gInPc1
</s>
<s>
5	#num#	k4
575	#num#	k4
</s>
<s>
6	#num#	k4
184	#num#	k4
</s>
<s>
2	#num#	k4
722	#num#	k4
</s>
<s>
2	#num#	k4
462	#num#	k4
</s>
<s>
Celkem	celkem	k6eAd1
prodáno	prodat	k5eAaPmNgNnS
v	v	k7c6
ČR	ČR	kA
</s>
<s>
10	#num#	k4
989	#num#	k4
</s>
<s>
14	#num#	k4
658	#num#	k4
</s>
<s>
17	#num#	k4
380	#num#	k4
</s>
<s>
18	#num#	k4
516	#num#	k4
</s>
<s>
Životní	životní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojována	ozdrojován	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Ford	ford	k1gInSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
první	první	k4xOgFnPc4
automobilky	automobilka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
nabízí	nabízet	k5eAaImIp3nS
hybridní	hybridní	k2eAgInPc4d1
a	a	k8xC
elektrické	elektrický	k2eAgInPc4d1
vozy	vůz	k1gInPc4
<g/>
,	,	kIx,
vozy	vůz	k1gInPc4
s	s	k7c7
motory	motor	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
poháněny	pohánět	k5eAaImNgInP
palivem	palivo	k1gNnSc7
E85	E85	k1gFnSc2
(	(	kIx(
<g/>
bioetanol	bioetanol	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
dalšími	další	k2eAgNnPc7d1
alternativními	alternativní	k2eAgNnPc7d1
palivy	palivo	k1gNnPc7
jako	jako	k8xS,k8xC
LPG	LPG	kA
a	a	k8xC
CNG	CNG	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
modelů	model	k1gInPc2
patří	patřit	k5eAaImIp3nS
na	na	k7c4
přední	přední	k2eAgNnPc4d1
místa	místo	k1gNnPc4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
třídě	třída	k1gFnSc6
v	v	k7c6
oblasti	oblast	k1gFnSc6
nízké	nízký	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
emisí	emise	k1gFnPc2
CO	co	k8xS
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ford	ford	k1gInSc1
je	být	k5eAaImIp3nS
prvním	první	k4xOgMnSc7
výrobcem	výrobce	k1gMnSc7
automobilu	automobil	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
získal	získat	k5eAaPmAgInS
certifikaci	certifikace	k1gFnSc4
podle	podle	k7c2
normy	norma	k1gFnSc2
ISO	ISO	kA
14001	#num#	k4
na	na	k7c4
ochranu	ochrana	k1gFnSc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
pro	pro	k7c4
všechny	všechen	k3xTgInPc4
své	svůj	k3xOyFgInPc4
výrobní	výrobní	k2eAgInPc4d1
závody	závod	k1gInPc4
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
na	na	k7c6
špičce	špička	k1gFnSc6
ve	v	k7c6
vývoji	vývoj	k1gInSc6
vozidel	vozidlo	k1gNnPc2
poháněných	poháněný	k2eAgFnPc2d1
vodíkem	vodík	k1gInSc7
a	a	k8xC
vodíkových	vodíkový	k2eAgInPc2d1
palivových	palivový	k2eAgInPc2d1
článků	článek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Obecný	obecný	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
společnosti	společnost	k1gFnSc2
</s>
<s>
zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
Ford	ford	k1gInSc1
Motor	motor	k1gInSc1
Company	Compana	k1gFnSc2
2002	#num#	k4
</s>
<s>
1896	#num#	k4
Henry	henry	k1gInSc2
Ford	ford	k1gInSc1
montuje	montovat	k5eAaImIp3nS
svoje	svůj	k3xOyFgNnSc4
první	první	k4xOgNnSc4
vozidlo	vozidlo	k1gNnSc4
–	–	k?
Quadricycle	Quadricycle	k1gInSc1
–	–	k?
na	na	k7c6
kostře	kostra	k1gFnSc6
kočáru	kočár	k1gInSc2
se	s	k7c7
čtyřmi	čtyři	k4xCgNnPc7
koly	kolo	k1gNnPc7
z	z	k7c2
bicyklu	bicykl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1901	#num#	k4
Henry	henry	k1gInSc2
Ford	ford	k1gInSc1
vyhrává	vyhrávat	k5eAaImIp3nS
závody	závod	k1gInPc4
vysokoprofilových	vysokoprofilův	k2eAgInPc2d1
vozů	vůz	k1gInPc2
v	v	k7c4
Grosse	Gross	k1gMnPc4
Pointe	Pointe	k?
<g/>
,	,	kIx,
Michigan	Michigan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
1903	#num#	k4
Založena	založen	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Ford	ford	k1gInSc1
Motor	motor	k1gInSc1
Company	Compana	k1gFnSc2
s	s	k7c7
11	#num#	k4
původními	původní	k2eAgMnPc7d1
investory	investor	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
představen	představit	k5eAaPmNgInS
Ford	ford	k1gInSc1
model	model	k1gInSc1
A	A	kA
“	“	k?
<g/>
Fordmobile	Fordmobila	k1gFnSc6
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s>
1908	#num#	k4
Je	být	k5eAaImIp3nS
představen	představit	k5eAaPmNgInS
Ford	ford	k1gInSc1
model	model	k1gInSc1
T.	T.	kA
Do	do	k7c2
roku	rok	k1gInSc2
1927	#num#	k4
se	se	k3xPyFc4
vyrobí	vyrobit	k5eAaPmIp3nS
15	#num#	k4
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
1911	#num#	k4
Ford	ford	k1gInSc1
otevírá	otevírat	k5eAaImIp3nS
první	první	k4xOgFnSc4
továrnu	továrna	k1gFnSc4
mimo	mimo	k7c4
Severní	severní	k2eAgFnSc4d1
Ameriku	Amerika	k1gFnSc4
–	–	k?
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
Manchesteru	Manchester	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
1913	#num#	k4
Pohybující	pohybující	k2eAgFnSc1d1
se	se	k3xPyFc4
montážní	montážní	k2eAgFnSc1d1
linka	linka	k1gFnSc1
je	být	k5eAaImIp3nS
představena	představit	k5eAaPmNgFnS
v	v	k7c6
montážním	montážní	k2eAgInSc6d1
závodě	závod	k1gInSc6
Highland	Highland	k1gInSc1
Park	park	k1gInSc1
<g/>
,	,	kIx,
připravujíce	připravovat	k5eAaImSgFnP
model	model	k1gInSc1
T	T	kA
8	#num#	k4
<g/>
x	x	k?
rychleji	rychle	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
1914	#num#	k4
Ford	ford	k1gInSc1
zavádí	zavádět	k5eAaImIp3nS
minimální	minimální	k2eAgFnSc4d1
denní	denní	k2eAgFnSc4d1
mzdu	mzda	k1gFnSc4
pět	pět	k4xCc4
dolarů	dolar	k1gInPc2
(	(	kIx(
<g/>
dvojitá	dvojitý	k2eAgFnSc1d1
než	než	k8xS
u	u	k7c2
jiných	jiný	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Model	model	k1gInSc1
T	T	kA
parkuje	parkovat	k5eAaImIp3nS
před	před	k7c7
Geelongskou	Geelongský	k2eAgFnSc7d1
knihovnou	knihovna	k1gFnSc7
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1915	#num#	k4
</s>
<s>
1918	#num#	k4
Započata	započat	k2eAgFnSc1d1
konstrukce	konstrukce	k1gFnSc1
komplexu	komplex	k1gInSc2
montážního	montážní	k2eAgInSc2d1
závodu	závod	k1gInSc2
River	River	k1gMnSc1
Rouge	rouge	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
1919	#num#	k4
Edsel	Edsel	k1gInSc1
Ford	ford	k1gInSc4
nastupuje	nastupovat	k5eAaImIp3nS
místo	místo	k1gNnSc4
Henryho	Henry	k1gMnSc2
jako	jako	k8xS,k8xC
prezident	prezident	k1gMnSc1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1922	#num#	k4
Ford	ford	k1gInSc1
kupuje	kupovat	k5eAaImIp3nS
Lincoln	Lincoln	k1gMnSc1
Motor	motor	k1gInSc4
Company	Compana	k1gFnSc2
za	za	k7c4
osm	osm	k4xCc4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
1925	#num#	k4
Ford	ford	k1gInSc1
uvádí	uvádět	k5eAaImIp3nS
na	na	k7c4
trh	trh	k1gInSc4
letadlo	letadlo	k1gNnSc4
Ford	ford	k1gInSc1
Tri-Motor	Tri-Motor	k1gInSc4
pro	pro	k7c4
letecké	letecký	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
1928	#num#	k4
Ford	ford	k1gInSc1
model	model	k1gInSc4
A	a	k9
</s>
<s>
1927	#num#	k4
Ford	ford	k1gInSc1
uvádí	uvádět	k5eAaImIp3nS
druhou	druhý	k4xOgFnSc4
generaci	generace	k1gFnSc4
modelu	model	k1gInSc2
A	a	k8xC
z	z	k7c2
komplexu	komplex	k1gInSc2
River	Rivra	k1gFnPc2
Rouge	rouge	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1932	#num#	k4
Ford	ford	k1gInSc1
uvádí	uvádět	k5eAaImIp3nS
osmiválcový	osmiválcový	k2eAgInSc1d1
motor	motor	k1gInSc1
z	z	k7c2
jednoho	jeden	k4xCgInSc2
odlitku	odlitek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1936	#num#	k4
Je	být	k5eAaImIp3nS
představen	představen	k2eAgMnSc1d1
Lincoln	Lincoln	k1gMnSc1
Zephyr	Zephyr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
1938	#num#	k4
Je	být	k5eAaImIp3nS
ustanovena	ustanoven	k2eAgFnSc1d1
divize	divize	k1gFnSc1
Mercury	Mercura	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vyplnila	vyplnit	k5eAaPmAgFnS
mezeru	mezera	k1gFnSc4
mezi	mezi	k7c7
úspornými	úsporný	k2eAgInPc7d1
Fordy	ford	k1gInPc7
a	a	k8xC
luxusními	luxusní	k2eAgMnPc7d1
Lincolny	Lincoln	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
1941	#num#	k4
Je	být	k5eAaImIp3nS
představen	představen	k2eAgMnSc1d1
Lincoln	Lincoln	k1gMnSc1
Continental	Continental	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ford	ford	k1gInSc1
začíná	začínat	k5eAaImIp3nS
vyrábět	vyrábět	k5eAaImF
džípy	džíp	k1gInPc4
pro	pro	k7c4
armádu	armáda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
pracovní	pracovní	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
s	s	k7c7
UAW-CIO	UAW-CIO	k1gFnSc7
pokrývá	pokrývat	k5eAaImIp3nS
severoamerické	severoamerický	k2eAgMnPc4d1
zaměstnance	zaměstnanec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
1942	#num#	k4
Výroba	výroba	k1gFnSc1
civilních	civilní	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
zastavena	zastaven	k2eAgFnSc1d1
<g/>
,	,	kIx,
odklonění	odklonění	k1gNnSc1
zaměření	zaměření	k1gNnSc2
továren	továrna	k1gFnPc2
na	na	k7c4
výrobu	výroba	k1gFnSc4
bombardérů	bombardér	k1gInPc2
B-24	B-24	k1gMnSc1
Liberator	Liberator	k1gMnSc1
<g/>
,	,	kIx,
tanků	tank	k1gInPc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
produktů	produkt	k1gInPc2
pro	pro	k7c4
válečné	válečný	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
1943	#num#	k4
Umírá	umírat	k5eAaImIp3nS
Edsel	Edsel	k1gMnSc1
Ford	ford	k1gInSc1
<g/>
,	,	kIx,
Henry	henry	k1gInSc1
Ford	ford	k1gInSc1
pokračuje	pokračovat	k5eAaImIp3nS
v	v	k7c6
prezidentství	prezidentství	k1gNnSc6
během	během	k7c2
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1945	#num#	k4
Henry	henry	k1gInSc2
Ford	ford	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
prezidentem	prezident	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
1946	#num#	k4
The	The	k1gMnSc1
Whiz	Whiz	k1gMnSc1
Kids	Kidsa	k1gFnPc2
–	–	k?
dřívější	dřívější	k2eAgMnPc1d1
důstojníci	důstojník	k1gMnPc1
leteckých	letecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
armády	armáda	k1gFnSc2
USA	USA	kA
jsou	být	k5eAaImIp3nP
najati	najat	k2eAgMnPc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
revitalizovali	revitalizovat	k5eAaImAgMnP
společnost	společnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
1948	#num#	k4
Představeno	představen	k2eAgNnSc1d1
nákladní	nákladní	k2eAgNnSc1d1
auto	auto	k1gNnSc1
Ford	ford	k1gInSc1
F-Series	F-Series	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
1949	#num#	k4
Ford	ford	k1gInSc1
předvádí	předvádět	k5eAaImIp3nS
moderní	moderní	k2eAgMnSc1d1
poválečné	poválečný	k2eAgInPc4d1
automobily	automobil	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvedení	uvedení	k1gNnSc1
kombíku	kombík	k1gInSc2
„	„	k?
<g/>
Woody	Wooda	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
1954	#num#	k4
Je	být	k5eAaImIp3nS
představen	představit	k5eAaPmNgInS
Ford	ford	k1gInSc1
Thunderbird	Thunderbird	k1gInSc1
jako	jako	k8xC,k8xS
osobní	osobní	k2eAgInSc1d1
luxusní	luxusní	k2eAgInSc1d1
automobil	automobil	k1gInSc1
s	s	k7c7
8	#num#	k4
<g/>
válcovým	válcový	k2eAgInSc7d1
motorem	motor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ford	ford	k1gInSc1
začíná	začínat	k5eAaImIp3nS
s	s	k7c7
crash	crash	k1gInSc4
testy	test	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
1956	#num#	k4
Je	být	k5eAaImIp3nS
představen	představen	k2eAgMnSc1d1
Lincoln	Lincoln	k1gMnSc1
Continental	Continental	k1gMnSc1
Mark	Mark	k1gMnSc1
II	II	kA
za	za	k7c4
10000	#num#	k4
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ford	ford	k1gInSc1
obchoduje	obchodovat	k5eAaImIp3nS
na	na	k7c6
burze	burza	k1gFnSc6
s	s	k7c7
kmenovými	kmenový	k2eAgFnPc7d1
akciemi	akcie	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
1959	#num#	k4
Je	být	k5eAaImIp3nS
ustanovena	ustanoven	k2eAgFnSc1d1
Ford	ford	k1gInSc1
Motor	motor	k1gInSc1
Credit	Credit	k1gFnSc2
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
poskytovala	poskytovat	k5eAaImAgFnS
automobilové	automobilový	k2eAgNnSc4d1
financování	financování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
1960	#num#	k4
Představeny	představen	k2eAgFnPc4d1
Ford	ford	k1gInSc4
Galaxy	Galax	k1gInPc4
a	a	k8xC
Ford	ford	k1gInSc1
Falcon	Falcona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lee	Lea	k1gFnSc6
Iacocca	Iacocc	k2eAgFnSc1d1
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
viceprezidentem	viceprezident	k1gMnSc7
společnosti	společnost	k1gFnSc2
a	a	k8xC
generálním	generální	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
divize	divize	k1gFnSc2
Ford	ford	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
1964	#num#	k4
Představeny	představen	k2eAgFnPc4d1
Ford	ford	k1gInSc1
Mustang	mustang	k1gMnSc1
a	a	k8xC
Ford	ford	k1gInSc1
GT	GT	kA
<g/>
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1967	#num#	k4
Je	být	k5eAaImIp3nS
založen	založen	k2eAgInSc1d1
Ford	ford	k1gInSc1
of	of	k?
Europe	Europ	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
1968	#num#	k4
Na	na	k7c4
trh	trh	k1gInSc4
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgInSc1d1
Ford	ford	k1gInSc1
Escort	Escort	k1gInSc1
<g/>
,	,	kIx,
s	s	k7c7
celkově	celkově	k6eAd1
dvaceti	dvacet	k4xCc7
miliony	milion	k4xCgInPc7
prodaných	prodaný	k2eAgMnPc2d1
vozů	vůz	k1gInPc2
nejúspěšnější	úspěšný	k2eAgInSc4d3
model	model	k1gInSc4
automobilky	automobilka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1970	#num#	k4
Ford	ford	k1gInSc1
uzavírá	uzavírat	k5eAaImIp3nS,k5eAaPmIp3nS
obchody	obchod	k1gInPc4
v	v	k7c6
asijsko-pacifické	asijsko-pacifický	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1976	#num#	k4
Představeny	představen	k2eAgInPc4d1
samonavíjecí	samonavíjecí	k2eAgInPc4d1
bezpečnostní	bezpečnostní	k2eAgInPc4d1
pásy	pás	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
1976	#num#	k4
V	v	k7c6
Argentině	Argentina	k1gFnSc6
se	se	k3xPyFc4
ujímá	ujímat	k5eAaImIp3nS
moci	moct	k5eAaImF
vojenská	vojenský	k2eAgFnSc1d1
junta	junta	k1gFnSc1
J.	J.	kA
R.	R.	kA
Videly	Videl	k1gInPc1
<g/>
;	;	kIx,
automobilka	automobilka	k1gFnSc1
se	se	k3xPyFc4
aktivně	aktivně	k6eAd1
podílí	podílet	k5eAaImIp3nS
na	na	k7c4
násilné	násilný	k2eAgNnSc4d1
represi	represe	k1gFnSc3
vůči	vůči	k7c3
odborům	odbor	k1gInPc3
a	a	k8xC
opozici	opozice	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1979	#num#	k4
Ford	ford	k1gInSc1
získává	získávat	k5eAaImIp3nS
25	#num#	k4
<g/>
%	%	kIx~
podíl	podíl	k1gInSc4
v	v	k7c6
Mazdě	Mazda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1981	#num#	k4
Ford	ford	k1gInSc1
Escort	Escort	k1gInSc4
je	být	k5eAaImIp3nS
představen	představit	k5eAaPmNgInS
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
1984	#num#	k4
Na	na	k7c4
trh	trh	k1gInSc4
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgInSc1d1
Ford	ford	k1gInSc1
Sierra	Sierra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
1985	#num#	k4
Ford	ford	k1gInSc1
Taurus	Taurus	k1gInSc4
představen	představit	k5eAaPmNgInS
s	s	k7c7
revolučním	revoluční	k2eAgInSc7d1
stylem	styl	k1gInSc7
„	„	k?
<g/>
aero	aero	k1gNnSc1
designu	design	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
uvedení	uvedení	k1gNnSc1
na	na	k7c4
trh	trh	k1gInSc4
modelu	model	k1gInSc2
Ford	ford	k1gInSc1
Scorpio	Scorpio	k6eAd1
</s>
<s>
1987	#num#	k4
Ford	ford	k1gInSc1
získává	získávat	k5eAaImIp3nS
Aston	Aston	k1gMnSc1
Martin	Martin	k1gMnSc1
<g/>
,	,	kIx,
Lagondu	Lagonda	k1gFnSc4
a	a	k8xC
Hertz	hertz	k1gInSc4
Rent-a-Car	Rent-a-Cara	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
1989	#num#	k4
Ford	ford	k1gInSc1
získává	získávat	k5eAaImIp3nS
Jaguar	Jaguar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
1990	#num#	k4
Představena	představen	k2eAgFnSc1d1
Mazda	Mazda	k1gFnSc1
MX-5	MX-5	k1gFnSc1
Miata	Miata	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
1991	#num#	k4
Představen	představen	k2eAgInSc1d1
Ford	ford	k1gInSc1
Explorer	Explorero	k1gNnPc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
z	z	k7c2
venkovského	venkovský	k2eAgInSc2d1
<g/>
/	/	kIx~
<g/>
rekreačního	rekreační	k2eAgInSc2d1
džípu	džíp	k1gInSc2
stalo	stát	k5eAaPmAgNnS
oblíbené	oblíbený	k2eAgNnSc4d1
rodinné	rodinný	k2eAgNnSc4d1
auto	auto	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
1993	#num#	k4
Ford	ford	k1gInSc1
dává	dávat	k5eAaImIp3nS
do	do	k7c2
standardní	standardní	k2eAgFnSc2d1
výbavy	výbava	k1gFnSc2
dva	dva	k4xCgInPc4
airbagy	airbag	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
1996	#num#	k4
Ford	ford	k1gInSc1
certifikuje	certifikovat	k5eAaImIp3nS
všech	všecek	k3xTgInPc2
svých	svůj	k3xOyFgFnPc2
26	#num#	k4
továren	továrna	k1gFnPc2
ekologickým	ekologický	k2eAgInSc7d1
standardem	standard	k1gInSc7
ISO	ISO	kA
14001	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1998	#num#	k4
Představen	představen	k2eAgInSc1d1
Ford	ford	k1gInSc1
Focus	Focus	k1gInSc1
</s>
<s>
1999	#num#	k4
Ford	ford	k1gInSc1
kupuje	kupovat	k5eAaImIp3nS
Volvo	Volvo	k1gNnSc1
(	(	kIx(
<g/>
automobilovou	automobilový	k2eAgFnSc4d1
divizi	divize	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bill	Bill	k1gMnSc1
Ford	ford	k1gInSc1
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
předsedou	předseda	k1gMnSc7
představenstva	představenstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
2000	#num#	k4
Ford	ford	k1gInSc1
kupuje	kupovat	k5eAaImIp3nS
Land	Land	k1gMnSc1
Rover	rover	k1gMnSc1
od	od	k7c2
BMW	BMW	kA
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
Retro	retro	k1gNnSc7
stylový	stylový	k2eAgInSc1d1
Ford	ford	k1gInSc1
Thunderbird	Thunderbird	k1gInSc1
je	být	k5eAaImIp3nS
představen	představit	k5eAaPmNgInS
v	v	k7c6
nové	nový	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
100	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc3
společnosti	společnost	k1gFnSc2
Ford	ford	k1gInSc1
Motor	motor	k1gInSc1
Company	Compana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
Nové	Nové	k2eAgInPc1d1
modely	model	k1gInPc1
S-MAX	S-MAX	k1gMnPc2
<g/>
,	,	kIx,
Focus	Focus	k1gInSc4
CC	CC	kA
<g/>
,	,	kIx,
Transit	transit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
Nové	Nová	k1gFnSc6
Mondeo	mondeo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
Ford	ford	k1gInSc1
prodává	prodávat	k5eAaImIp3nS
Land	Land	k1gMnSc1
Rover	rover	k1gMnSc1
a	a	k8xC
Jaguar	Jaguar	k1gMnSc1
indické	indický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
Tata	tata	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
přichází	přicházet	k5eAaImIp3nS
do	do	k7c2
výroby	výroba	k1gFnSc2
nová	nový	k2eAgFnSc1d1
Fiesta	fiesta	k1gFnSc1
a	a	k8xC
Ka	Ka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
roku	rok	k1gInSc2
se	se	k3xPyFc4
Ford	ford	k1gInSc1
se	se	k3xPyFc4
zbavuje	zbavovat	k5eAaImIp3nS
20	#num#	k4
<g/>
%	%	kIx~
akcií	akcie	k1gFnPc2
automobilky	automobilka	k1gFnSc2
Mazda	Mazda	k1gFnSc1
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
$	$	kIx~
<g/>
540	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuální	aktuální	k2eAgInSc1d1
stav	stav	k1gInSc1
13	#num#	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
2009	#num#	k4
Přichází	přicházet	k5eAaImIp3nS
nový	nový	k2eAgInSc4d1
Focus	Focus	k1gInSc4
RS	RS	kA
a	a	k8xC
Transit	transit	k1gInSc1
Connect	Connect	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
Ford	ford	k1gInSc1
obměňuje	obměňovat	k5eAaImIp3nS
celou	celý	k2eAgFnSc4d1
paletu	paleta	k1gFnSc4
velkých	velký	k2eAgInPc2d1
osobních	osobní	k2eAgInPc2d1
vozů	vůz	k1gInPc2
<g/>
:	:	kIx,
faceliftované	faceliftovaný	k2eAgFnPc1d1
verze	verze	k1gFnPc1
modelů	model	k1gInPc2
S-MAX	S-MAX	k1gMnPc2
<g/>
,	,	kIx,
Galaxy	Galax	k1gInPc1
a	a	k8xC
Mondeo	mondeo	k1gNnSc1
<g/>
,	,	kIx,
nová	nový	k2eAgFnSc1d1
generace	generace	k1gFnSc1
Fordu	ford	k1gInSc2
C-MAX	C-MAX	k1gFnSc2
(	(	kIx(
<g/>
ve	v	k7c6
dvou	dva	k4xCgFnPc6
karosářských	karosářský	k2eAgFnPc6d1
variantách	varianta	k1gFnPc6
<g/>
:	:	kIx,
Compact	Compacta	k1gFnPc2
a	a	k8xC
Grand	grand	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
Ukončení	ukončení	k1gNnSc1
činnosti	činnost	k1gFnSc2
automobilky	automobilka	k1gFnSc2
Mercury	Mercura	k1gFnSc2
z	z	k7c2
důvodu	důvod	k1gInSc2
ekonomické	ekonomický	k2eAgFnSc2d1
krize	krize	k1gFnSc2
a	a	k8xC
trvale	trvale	k6eAd1
klesajícího	klesající	k2eAgInSc2d1
zájmu	zájem	k1gInSc2
o	o	k7c4
vozy	vůz	k1gInPc4
této	tento	k3xDgFnSc2
značky	značka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
Začátek	začátek	k1gInSc1
výroby	výroba	k1gFnSc2
typu	typ	k1gInSc2
B-MAX	B-MAX	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Ford	ford	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
www.nyse.com	www.nyse.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
31	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
28	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2006	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
Ford	ford	k1gInSc1
Motor	motor	k1gInSc1
Company	Compana	k1gFnSc2
za	za	k7c4
rok	rok	k1gInSc4
2009	#num#	k4
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.ford.co.uk	www.ford.co.uk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.dtocz.cz/casopis.php?id_article=276%5B%5D	http://www.dtocz.cz/casopis.php?id_article=276%5B%5D	k4
<g/>
↑	↑	k?
S	s	k7c7
diodami	dioda	k1gFnPc7
LED	LED	kA
společnosti	společnost	k1gFnSc2
OSRAM	OSRAM	kA
září	zářit	k5eAaImIp3nS
Ford	ford	k1gInSc1
Mustang	mustang	k1gMnSc1
2010	#num#	k4
uvnitř	uvnitř	k6eAd1
i	i	k9
zvenčí	zvenčí	k6eAd1
<g/>
,	,	kIx,
Osram	Osram	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2009	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ford	ford	k1gInSc1
Focus	Focus	k1gInSc1
RS	RS	kA
na	na	k7c6
pneumatikách	pneumatika	k1gFnPc6
Continental	Continental	k1gMnSc1
Archivováno	archivován	k2eAgNnSc4d1
1	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Auto	auto	k1gNnSc1
<g/>
.	.	kIx.
<g/>
auto-news	auto-news	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ford	ford	k1gInSc1
je	být	k5eAaImIp3nS
nejúspěšnější	úspěšný	k2eAgFnSc1d3
dovážená	dovážený	k2eAgFnSc1d1
značka	značka	k1gFnSc1
v	v	k7c6
ČR	ČR	kA
Archivováno	archivovat	k5eAaBmNgNnS
20	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Autoweb	Autowba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
11	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2008	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
When	When	k1gInSc1
Ford	ford	k1gInSc1
bulit	bulit	k5eAaImF
a	a	k8xC
torture	tortur	k1gMnSc5
chambre	chambr	k1gInSc5
Jacobin	Jacobina	k1gFnPc2
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Henry	henry	k1gInSc1
Ford	ford	k1gInSc1
</s>
<s>
Sériová	sériový	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
<g/>
#	#	kIx~
<g/>
Historie	historie	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ford	ford	k1gInSc1
Motor	motor	k1gInSc1
Company	Compana	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
České	český	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
Ford	ford	k1gInSc1
Motor	motor	k1gInSc1
Company	Compana	k1gFnSc2
</s>
<s>
Oficiální	oficiální	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
Ford	ford	k1gInSc1
Motor	motor	k1gInSc1
Company	Compana	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
</s>
<s>
•	•	k?
kz	kz	k?
•	•	k?
d	d	k?
•	•	k?
e	e	k0
≺	≺	k?
předchozí	předchozí	k2eAgFnSc1d1
–	–	k?
Automobily	automobil	k1gInPc4
značky	značka	k1gFnSc2
Ford	ford	k1gInSc1
1980	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
(	(	kIx(
<g/>
evropský	evropský	k2eAgInSc1d1
trh	trh	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Třída	třída	k1gFnSc1
</s>
<s>
1980	#num#	k4
</s>
<s>
1990200020102020	#num#	k4
</s>
<s>
01234567890123456789012345678901234567890	#num#	k4
</s>
<s>
Mini	mini	k2eAgFnSc1d1
</s>
<s>
Ka	Ka	k?
I	i	k9
</s>
<s>
Ka	Ka	k?
II	II	kA
</s>
<s>
Ka	Ka	k?
III	III	kA
</s>
<s>
Malé	Malé	k2eAgInPc1d1
</s>
<s>
Fiesta	fiesta	k1gFnSc1
I	i	k9
</s>
<s>
Fiesta	fiesta	k1gFnSc1
II	II	kA
</s>
<s>
Fiesta	fiesta	k1gFnSc1
III	III	kA
</s>
<s>
Fiesta	fiesta	k1gFnSc1
IV	Iva	k1gFnPc2
</s>
<s>
Fiesta	fiesta	k1gFnSc1
V	v	k7c6
</s>
<s>
Fiesta	fiesta	k1gFnSc1
VI	VI	kA
</s>
<s>
Fiesta	fiesta	k1gFnSc1
VII	VII	kA
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2
střední	střední	k2eAgFnSc1d1
</s>
<s>
Escort	Escort	k1gInSc1
III	III	kA
</s>
<s>
Escort	Escort	k1gInSc1
IV	Iva	k1gFnPc2
</s>
<s>
Escort	Escort	k1gInSc1
V	v	k7c6
</s>
<s>
Escort	Escort	k1gInSc1
VI	VI	kA
</s>
<s>
Escort	Escort	k1gInSc1
VII	VII	kA
</s>
<s>
Orion	orion	k1gInSc1
I	i	k9
</s>
<s>
Orion	orion	k1gInSc1
II	II	kA
</s>
<s>
Orion	orion	k1gInSc1
III	III	kA
</s>
<s>
Orion	orion	k1gInSc1
IV	Iva	k1gFnPc2
</s>
<s>
Focus	Focus	k1gMnSc1
I	i	k9
</s>
<s>
Focus	Focus	k1gMnSc1
II	II	kA
</s>
<s>
Focus	Focus	k1gMnSc1
III	III	kA
</s>
<s>
Focus	Focus	k1gMnSc1
IV	IV	kA
</s>
<s>
Střední	střední	k2eAgFnSc1d1
</s>
<s>
Cortina	Cortina	k1gFnSc1
V	v	k7c6
</s>
<s>
Sierra	Sierra	k1gFnSc1
I	i	k9
</s>
<s>
Sierra	Sierra	k1gFnSc1
II	II	kA
</s>
<s>
Sierra	Sierra	k1gFnSc1
III	III	kA
</s>
<s>
Mondeo	mondeo	k1gNnSc1
I	i	k9
</s>
<s>
Mondeo	mondeo	k1gNnSc1
II	II	kA
</s>
<s>
Mondeo	mondeo	k1gNnSc1
III	III	kA
</s>
<s>
Mondeo	mondeo	k1gNnSc1
IV	Iva	k1gFnPc2
</s>
<s>
Mondeo	mondeo	k1gNnSc1
V	v	k7c6
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2
střední	střední	k2eAgFnSc1d1
</s>
<s>
Granada	Granada	k1gFnSc1
II	II	kA
</s>
<s>
Scorpio	Scorpio	k6eAd1
I	i	k9
/	/	kIx~
Granada	Granada	k1gFnSc1
III	III	kA
</s>
<s>
Scorpio	Scorpio	k1gMnSc1
II	II	kA
</s>
<s>
Sportovní	sportovní	k2eAgFnPc1d1
</s>
<s>
Capri	Capri	k6eAd1
III	III	kA
</s>
<s>
Probe	Probat	k5eAaPmIp3nS
I	i	k9
</s>
<s>
Probe	Probat	k5eAaPmIp3nS
II	II	kA
</s>
<s>
Cougar	Cougar	k1gMnSc1
</s>
<s>
Mustang	mustang	k1gMnSc1
VI	VI	kA
</s>
<s>
Puma	puma	k1gFnSc1
</s>
<s>
StreetKa	StreetKa	k6eAd1
</s>
<s>
GT	GT	kA
</s>
<s>
GT	GT	kA
</s>
<s>
Kompaktní	kompaktní	k2eAgFnSc1d1
SUV	SUV	kA
</s>
<s>
EcoSport	EcoSport	k1gInSc1
</s>
<s>
Crossover	Crossover	k1gMnSc1
/	/	kIx~
SUV	SUV	kA
</s>
<s>
Puma	puma	k1gFnSc1
</s>
<s>
Kuga	Kuga	k6eAd1
I	i	k9
</s>
<s>
Kuga	Kuga	k1gFnSc1
II	II	kA
</s>
<s>
Kuga	Kuga	k1gFnSc1
III	III	kA
</s>
<s>
Velká	velká	k1gFnSc1
SUV	SUV	kA
</s>
<s>
Edge	Edge	k6eAd1
</s>
<s>
Explorer	Explorer	k1gMnSc1
</s>
<s>
Mini	mini	k2eAgFnSc1d1
MPV	MPV	kA
</s>
<s>
Fusion	Fusion	k1gInSc1
</s>
<s>
B-Max	B-Max	k1gInSc1
</s>
<s>
Střední	střední	k2eAgFnSc1d1
MPV	MPV	kA
</s>
<s>
C-MAX	C-MAX	k?
I	i	k9
</s>
<s>
C-MAX	C-MAX	k?
II	II	kA
</s>
<s>
Velké	velký	k2eAgInPc1d1
MPV	MPV	kA
</s>
<s>
S-MAX	S-MAX	k?
I	i	k9
</s>
<s>
S-MAX	S-MAX	k?
II	II	kA
</s>
<s>
Galaxy	Galax	k1gInPc1
I	i	k9
</s>
<s>
Galaxy	Galax	k1gInPc1
II	II	kA
</s>
<s>
Galaxy	Galax	k1gInPc1
III	III	kA
</s>
<s>
Galaxy	Galax	k1gInPc1
IV	Iva	k1gFnPc2
</s>
<s>
Terénní	terénní	k2eAgFnSc1d1
/	/	kIx~
SUV	SUV	kA
</s>
<s>
Maverick	Maverick	k6eAd1
I	i	k9
</s>
<s>
Maverick	Maverick	k1gMnSc1
II	II	kA
</s>
<s>
Ford	ford	k1gInSc1
Motor	motor	k1gInSc1
Company	Compana	k1gFnSc2
(	(	kIx(
<g/>
globálně	globálně	k6eAd1
<g/>
)	)	kIx)
Platformy	platforma	k1gFnPc1
•	•	k?
Motory	motor	k1gInPc1
•	•	k?
Automobily	automobil	k1gInPc1
•	•	k?
Kategorie	kategorie	k1gFnSc2
</s>
<s>
Současné	současný	k2eAgInPc1d1
a	a	k8xC
budoucí	budoucí	k2eAgInPc4d1
modely	model	k1gInPc4
Osobní	osobní	k2eAgInPc4d1
auta	auto	k1gNnPc1
</s>
<s>
Activa	Activa	k1gFnSc1
•	•	k?
Crown	Crown	k1gNnSc1
Victoria	Victorium	k1gNnSc2
/	/	kIx~
Police	police	k1gFnSc2
Interceptor	Interceptor	k1gInSc1
•	•	k?
Fairlane	Fairlan	k1gMnSc5
•	•	k?
Falcon	Falcon	k1gMnSc1
/	/	kIx~
FPV	FPV	kA
models	models	k1gInSc1
•	•	k?
Fiesta	fiesta	k1gFnSc1
/	/	kIx~
Ikon	ikon	k1gInSc1
/	/	kIx~
Fusion	Fusion	k1gInSc1
(	(	kIx(
<g/>
Evropa	Evropa	k1gFnSc1
<g/>
)	)	kIx)
/	/	kIx~
EcoSport	EcoSport	k1gInSc1
•	•	k?
Focus	Focus	k1gInSc1
/	/	kIx~
C-MAX	C-MAX	k1gFnSc1
/	/	kIx~
Kuga	Kuga	k1gFnSc1
•	•	k?
Fusion	Fusion	k1gInSc1
(	(	kIx(
<g/>
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Galaxy	Galax	k1gInPc1
•	•	k?
Ka	Ka	k1gFnPc2
/	/	kIx~
Sportka	sportka	k1gFnSc1
/	/	kIx~
Streetka	Streetka	k1gFnSc1
•	•	k?
Laser	laser	k1gInSc1
/	/	kIx~
Lynx	Lynx	k1gInSc1
/	/	kIx~
Tierra	Tierra	k1gFnSc1
•	•	k?
Mondeo	mondeo	k1gNnSc1
/	/	kIx~
Metrostar	Metrostar	k1gMnSc1
•	•	k?
Mustang	mustang	k1gMnSc1
•	•	k?
S-MAX	S-MAX	k1gMnSc1
•	•	k?
Taurus	Taurus	k1gMnSc1
Užitkové	užitkový	k2eAgFnSc2d1
<g/>
,	,	kIx,
SUV	SUV	kA
</s>
<s>
Cargo	Cargo	k1gMnSc1
•	•	k?
Courier	Courier	k1gMnSc1
/	/	kIx~
Bantam	bantam	k1gInSc1
•	•	k?
E-Series	E-Series	k1gInSc1
/	/	kIx~
Chateau	Chateaus	k1gInSc2
Wagon	Wagon	k1gMnSc1
•	•	k?
Econovan	Econovan	k1gMnSc1
•	•	k?
Edge	Edge	k1gInSc1
•	•	k?
Escape	Escap	k1gInSc5
/	/	kIx~
Hybrid	hybrid	k1gInSc1
/	/	kIx~
Maverick	Maverick	k1gInSc1
•	•	k?
Expedition	Expedition	k1gInSc1
•	•	k?
Explorer	Explorer	k1gInSc1
•	•	k?
Everest	Everest	k1gInSc1
<g/>
/	/	kIx~
<g/>
Endeavour	Endeavour	k1gInSc1
•	•	k?
F-Series	F-Series	k1gInSc1
•	•	k?
Flex	Flex	k1gInSc1
•	•	k?
Pronto	Pronto	k1gNnSc1
•	•	k?
Ranger	Ranger	k1gMnSc1
/	/	kIx~
Courier	Courier	k1gMnSc1
•	•	k?
Territory	Territor	k1gInPc1
•	•	k?
Tourneo	Tourneo	k6eAd1
•	•	k?
Tourneo	Tourneo	k1gMnSc1
Connect	Connect	k1gMnSc1
•	•	k?
Transit	transit	k1gInSc1
•	•	k?
Transit	transit	k1gInSc1
Connect	Connect	k1gInSc1
</s>
<s>
Historická	historický	k2eAgNnPc1d1
a	a	k8xC
starší	starý	k2eAgNnPc1d2
auta	auto	k1gNnPc1
1900	#num#	k4
</s>
<s>
Model	model	k1gInSc1
A	A	kA
<g/>
/	/	kIx~
<g/>
AC	AC	kA
•	•	k?
Model	model	k1gInSc1
B	B	kA
•	•	k?
Model	model	k1gInSc1
C	C	kA
•	•	k?
Model	model	k1gInSc1
F	F	kA
•	•	k?
Model	modla	k1gFnPc2
K	k	k7c3
•	•	k?
Model	modla	k1gFnPc2
N	N	kA
•	•	k?
Model	model	k1gInSc1
R	R	kA
•	•	k?
Model	model	k1gInSc1
S	s	k7c7
•	•	k?
Model	model	k1gInSc1
T	T	kA
1910	#num#	k4
<g/>
–	–	k?
<g/>
1920	#num#	k4
</s>
<s>
Model	model	k1gInSc1
TT	TT	kA
•	•	k?
Model	model	k1gInSc1
A	a	k8xC
•	•	k?
Model	model	k1gInSc1
AA	AA	kA
1930	#num#	k4
<g/>
–	–	k?
<g/>
1940	#num#	k4
</s>
<s>
Model	model	k1gInSc1
B	B	kA
•	•	k?
model	model	k1gInSc1
Y	Y	kA
•	•	k?
Model	model	k1gInSc1
C	C	kA
•	•	k?
Model	model	k1gInSc1
CX	CX	kA
•	•	k?
Junior	junior	k1gMnSc1
Popular	Popular	k1gMnSc1
•	•	k?
1937	#num#	k4
Ford	ford	k1gInSc1
•	•	k?
Junior	junior	k1gMnSc1
De	De	k?
Luxe	Lux	k1gMnSc2
•	•	k?
Köln	Köln	k1gInSc1
•	•	k?
Rheinland	Rheinland	k1gInSc1
•	•	k?
Eifel	Eifel	k1gInSc1
•	•	k?
Model	model	k1gInSc1
7Y	7Y	k4
•	•	k?
Model	model	k1gInSc1
7W	7W	k4
•	•	k?
Anglia	Anglia	k1gFnSc1
•	•	k?
Prefect	Prefect	k1gInSc1
•	•	k?
Country	country	k2eAgInSc1d1
Squire	Squir	k1gInSc5
•	•	k?
Meteor	meteor	k1gInSc1
•	•	k?
Pilot	pilot	k1gInSc1
•	•	k?
Vedette	Vedett	k1gInSc5
1950	#num#	k4
</s>
<s>
C-Series	C-Series	k1gInSc1
Trucks	Trucks	k1gInSc1
•	•	k?
Consul	Consul	k1gInSc1
•	•	k?
Country	country	k2eAgInSc1d1
Sedan	sedan	k1gInSc1
•	•	k?
Del	Del	k1gFnSc2
Rio	Rio	k1gMnSc1
•	•	k?
Edsel	Edsel	k1gMnSc1
(	(	kIx(
<g/>
Brand	Brand	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Fairlane	Fairlan	k1gMnSc5
•	•	k?
Galaxie	galaxie	k1gFnSc1
•	•	k?
Mainline	Mainlin	k1gInSc5
•	•	k?
Parklane	Parklan	k1gMnSc5
•	•	k?
Popular	Populara	k1gFnPc2
•	•	k?
Ranchero	Ranchero	k1gNnSc4
•	•	k?
Squire	Squir	k1gInSc5
•	•	k?
Taunus	Taunus	k1gMnSc1
•	•	k?
Thunderbird	Thunderbird	k1gMnSc1
•	•	k?
Versailles	Versailles	k1gFnSc2
•	•	k?
Zephyr	Zephyr	k1gInSc1
1960	#num#	k4
</s>
<s>
Bronco	Bronco	k6eAd1
•	•	k?
Capri	Capr	k1gMnSc3
•	•	k?
Corcel	Corcel	k1gMnSc1
•	•	k?
Corsair	Corsair	k1gMnSc1
•	•	k?
Cortina	Cortina	k1gFnSc1
•	•	k?
Escort	Escort	k1gInSc1
•	•	k?
Falcon	Falcon	k1gInSc1
(	(	kIx(
<g/>
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
GT40	GT40	k1gMnSc1
•	•	k?
H-Series	H-Series	k1gInSc1
Trucks	Trucks	k1gInSc1
•	•	k?
LTD	ltd	kA
(	(	kIx(
<g/>
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
N-Series	N-Series	k1gInSc1
Trucks	Trucksa	k1gFnPc2
•	•	k?
Torino	Torino	k1gNnSc4
•	•	k?
W-Series	W-Series	k1gInSc1
Trucks	Trucks	k1gInSc1
1970	#num#	k4
</s>
<s>
Elite	Elite	k5eAaPmIp2nP
•	•	k?
Fairmont	Fairmont	k1gInSc1
•	•	k?
Granada	Granada	k1gFnSc1
(	(	kIx(
<g/>
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
L-Series	L-Series	k1gInSc1
Trucks	Trucks	k1gInSc1
•	•	k?
Maverick	Maverick	k1gInSc1
(	(	kIx(
<g/>
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Pinto	pinta	k1gFnSc5
1980	#num#	k4
</s>
<s>
Aerostar	Aerostar	k1gInSc1
•	•	k?
Bantam	bantam	k1gInSc1
•	•	k?
Bronco	Bronco	k1gMnSc1
II	II	kA
•	•	k?
Del	Del	k1gMnSc1
Rey	Rea	k1gFnSc2
•	•	k?
Escort	Escort	k1gInSc1
•	•	k?
EXP	exp	kA
•	•	k?
Festiva	Festiva	k1gFnSc1
•	•	k?
Laser	laser	k1gInSc1
•	•	k?
LTD	ltd	kA
Crown	Crown	k1gInSc4
Victoria	Victorium	k1gNnSc2
•	•	k?
Orion	orion	k1gInSc1
•	•	k?
Probe	Prob	k1gInSc5
•	•	k?
RS200	RS200	k1gMnSc3
•	•	k?
Scorpio	Scorpio	k1gMnSc1
•	•	k?
Sierra	Sierra	k1gFnSc1
•	•	k?
Telstar	Telstara	k1gFnPc2
•	•	k?
Tempo	tempo	k1gNnSc4
•	•	k?
Verona	Verona	k1gFnSc1
1990	#num#	k4
</s>
<s>
Aspire	Aspir	k1gMnSc5
•	•	k?
Contour	Contour	k1gMnSc1
•	•	k?
Cougar	Cougar	k1gMnSc1
•	•	k?
Maverick	Maverick	k1gMnSc1
•	•	k?
Puma	puma	k1gFnSc1
•	•	k?
Windstar	Windstar	k1gMnSc1
•	•	k?
ZX2	ZX2	k1gMnSc1
2000	#num#	k4
</s>
<s>
Excursion	Excursion	k1gInSc1
•	•	k?
Freestar	Freestar	k1gInSc1
•	•	k?
GT	GT	kA
•	•	k?
Five	Five	k1gInSc1
Hundred	Hundred	k1gInSc1
•	•	k?
Freestyle	Freestyl	k1gInSc5
<g/>
/	/	kIx~
<g/>
Taurus	Taurus	k1gInSc4
X	X	kA
</s>
<s>
Globálně	globálně	k6eAd1
Korporace	korporace	k1gFnPc1
<g/>
,	,	kIx,
<g/>
Dceřiné	dceřiný	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
<g/>
,	,	kIx,
<g/>
Joint	Joint	k1gInSc1
venture	ventur	k1gMnSc5
(	(	kIx(
<g/>
spolupráce	spolupráce	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Ford	ford	k1gInSc1
Motor	motor	k1gInSc1
Company	Compana	k1gFnSc2
•	•	k?
Lincoln	Lincoln	k1gMnSc1
Motor	motor	k1gInSc4
Company	Compana	k1gFnSc2
•	•	k?
Mercury	Mercura	k1gFnSc2
•	•	k?
Ford	ford	k1gInSc1
of	of	k?
Canada	Canada	k1gFnSc1
•	•	k?
Ford	ford	k1gInSc1
of	of	k?
Britain	Britain	k1gInSc1
•	•	k?
Ford	ford	k1gInSc1
of	of	k?
Argentina	Argentina	k1gFnSc1
•	•	k?
Ford	ford	k1gInSc1
Germany	German	k1gInPc1
•	•	k?
Ford	ford	k1gInSc1
of	of	k?
Australia	Australia	k1gFnSc1
•	•	k?
Ford	ford	k1gInSc1
of	of	k?
Brazil	Brazil	k1gFnSc2
•	•	k?
Ford	ford	k1gInSc1
Europe	Europ	k1gInSc5
•	•	k?
Ford	ford	k1gInSc1
France	Franc	k1gMnSc2
•	•	k?
Ford	ford	k1gInSc1
India	indium	k1gNnSc2
•	•	k?
Ford	ford	k1gInSc1
Motor	motor	k1gInSc1
Company	Compana	k1gFnSc2
Philippines	Philippines	k1gMnSc1
•	•	k?
Arabian	Arabian	k1gInSc1
Motors	Motors	k1gInSc1
Group	Group	k1gInSc1
•	•	k?
AutoAlliance	AutoAllianec	k1gMnSc2
International	International	k1gMnSc2
•	•	k?
AutoAlliance	AutoAlliance	k1gFnSc2
Thailand	Thailanda	k1gFnPc2
•	•	k?
Chang	Chang	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
Ford	ford	k1gInSc1
•	•	k?
Jiangling	Jiangling	k1gInSc1
Motors	Motors	k1gInSc1
•	•	k?
Volvo	Volvo	k1gNnSc1
Cars	Cars	k1gInSc1
•	•	k?
Ford-Otosan	Ford-Otosan	k1gInSc1
•	•	k?
Mazda	Mazda	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010711100	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0720	#num#	k4
9454	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80085248	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
125431219	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80085248	#num#	k4
</s>
