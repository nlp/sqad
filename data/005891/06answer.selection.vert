<s>
Hnědý	hnědý	k2eAgMnSc1d1	hnědý
trpaslík	trpaslík	k1gMnSc1	trpaslík
je	být	k5eAaImIp3nS	být
subhvězdný	subhvězdný	k2eAgInSc4d1	subhvězdný
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nevyzařuje	vyzařovat	k5eNaImIp3nS	vyzařovat
světlo	světlo	k1gNnSc4	světlo
a	a	k8xC	a
energii	energie	k1gFnSc4	energie
díky	díky	k7c3	díky
termonukleárním	termonukleární	k2eAgFnPc3d1	termonukleární
reakcím	reakce	k1gFnPc3	reakce
jako	jako	k8xC	jako
hvězdy	hvězda	k1gFnPc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
vodivý	vodivý	k2eAgInSc4d1	vodivý
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
jádro	jádro	k1gNnSc4	jádro
<g/>
.	.	kIx.	.
</s>
