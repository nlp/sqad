<s>
Hnědý	hnědý	k2eAgMnSc1d1	hnědý
trpaslík	trpaslík	k1gMnSc1	trpaslík
je	být	k5eAaImIp3nS	být
subhvězdný	subhvězdný	k2eAgInSc4d1	subhvězdný
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nevyzařuje	vyzařovat	k5eNaImIp3nS	vyzařovat
světlo	světlo	k1gNnSc4	světlo
a	a	k8xC	a
energii	energie	k1gFnSc4	energie
díky	díky	k7c3	díky
termonukleárním	termonukleární	k2eAgFnPc3d1	termonukleární
reakcím	reakce	k1gFnPc3	reakce
jako	jako	k8xC	jako
hvězdy	hvězda	k1gFnPc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
vodivý	vodivý	k2eAgInSc4d1	vodivý
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
jádro	jádro	k1gNnSc4	jádro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
sice	sice	k8xC	sice
může	moct	k5eAaImIp3nS	moct
dočasně	dočasně	k6eAd1	dočasně
probíhat	probíhat	k5eAaImF	probíhat
syntéza	syntéza	k1gFnSc1	syntéza
helia	helium	k1gNnSc2	helium
z	z	k7c2	z
deuteria	deuterium	k1gNnSc2	deuterium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
nestačí	stačit	k5eNaBmIp3nS	stačit
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
syntéze	syntéza	k1gFnSc3	syntéza
těžších	těžký	k2eAgInPc2d2	těžší
prvků	prvek	k1gInPc2	prvek
z	z	k7c2	z
(	(	kIx(	(
<g/>
běžného	běžný	k2eAgInSc2d1	běžný
<g/>
)	)	kIx)	)
lehkého	lehký	k2eAgInSc2d1	lehký
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
rádiové	rádiový	k2eAgNnSc1d1	rádiové
a	a	k8xC	a
infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
viditelné	viditelný	k2eAgNnSc4d1	viditelné
světlo	světlo	k1gNnSc4	světlo
o	o	k7c6	o
velmi	velmi	k6eAd1	velmi
dlouhé	dlouhý	k2eAgFnSc3d1	dlouhá
vlnové	vlnový	k2eAgFnSc3d1	vlnová
délce	délka	k1gFnSc3	délka
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
červené	červený	k2eAgNnSc1d1	červené
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
hnědý	hnědý	k2eAgMnSc1d1	hnědý
trpaslík	trpaslík	k1gMnSc1	trpaslík
zavedla	zavést	k5eAaPmAgFnS	zavést
Jill	Jill	k1gInSc4	Jill
Tarterová	Tarterová	k1gFnSc1	Tarterová
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odlišila	odlišit	k5eAaPmAgFnS	odlišit
tyto	tento	k3xDgInPc4	tento
subhvězdné	subhvězdný	k2eAgInPc4d1	subhvězdný
objekty	objekt	k1gInPc4	objekt
od	od	k7c2	od
červených	červený	k2eAgMnPc2d1	červený
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
skutečné	skutečný	k2eAgInPc1d1	skutečný
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
málo	málo	k6eAd1	málo
hmotné	hmotný	k2eAgFnPc4d1	hmotná
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
až	až	k9	až
100	[number]	k4	100
miliard	miliarda	k4xCgFnPc2	miliarda
hnědých	hnědý	k2eAgInPc2d1	hnědý
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Hnědý	hnědý	k2eAgMnSc1d1	hnědý
trpaslík	trpaslík	k1gMnSc1	trpaslík
je	být	k5eAaImIp3nS	být
vesmírné	vesmírný	k2eAgNnSc4d1	vesmírné
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
z	z	k7c2	z
protohvězdy	protohvězda	k1gFnSc2	protohvězda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nemá	mít	k5eNaImIp3nS	mít
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
mohly	moct	k5eAaImAgFnP	moct
probíhat	probíhat	k5eAaImF	probíhat
veškeré	veškerý	k3xTgFnPc4	veškerý
termonukleární	termonukleární	k2eAgFnPc4d1	termonukleární
reakce	reakce	k1gFnPc4	reakce
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
slučování	slučování	k1gNnSc2	slučování
deuteria	deuterium	k1gNnSc2	deuterium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
přechod	přechod	k1gInSc4	přechod
mezi	mezi	k7c7	mezi
planetou	planeta	k1gFnSc7	planeta
a	a	k8xC	a
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
objekt	objekt	k1gInSc4	objekt
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
13	[number]	k4	13
až	až	k9	až
80	[number]	k4	80
hmotností	hmotnost	k1gFnPc2	hmotnost
planety	planeta	k1gFnSc2	planeta
Jupiter	Jupiter	k1gMnSc1	Jupiter
(	(	kIx(	(
<g/>
MJ	mj	kA	mj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vyšší	vysoký	k2eAgFnSc6d2	vyšší
hmotnosti	hmotnost	k1gFnSc6	hmotnost
než	než	k8xS	než
80	[number]	k4	80
MJ	mj	kA	mj
(	(	kIx(	(
<g/>
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
0,08	[number]	k4	0,08
hmotnosti	hmotnost	k1gFnSc2	hmotnost
našeho	náš	k3xOp1gNnSc2	náš
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
těleso	těleso	k1gNnSc1	těleso
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
jádře	jádro	k1gNnSc6	jádro
teploty	teplota	k1gFnSc2	teplota
potřebné	potřebný	k2eAgFnSc2d1	potřebná
k	k	k7c3	k
spalování	spalování	k1gNnSc3	spalování
lehkého	lehký	k2eAgInSc2d1	lehký
vodíku	vodík	k1gInSc2	vodík
<g/>
:	:	kIx,	:
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
při	při	k7c6	při
nižší	nízký	k2eAgFnSc6d2	nižší
hmotnosti	hmotnost	k1gFnSc6	hmotnost
než	než	k8xS	než
13	[number]	k4	13
MJ	mj	kA	mj
nedojde	dojít	k5eNaPmIp3nS	dojít
ani	ani	k9	ani
k	k	k7c3	k
deuteriovým	deuteriový	k2eAgFnPc3d1	deuteriová
reakcím	reakce	k1gFnPc3	reakce
a	a	k8xC	a
z	z	k7c2	z
objektu	objekt	k1gInSc2	objekt
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stane	stanout	k5eAaPmIp3nS	stanout
jen	jen	k9	jen
obří	obří	k2eAgFnSc1d1	obří
planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
hnědých	hnědý	k2eAgMnPc2d1	hnědý
trpaslíků	trpaslík	k1gMnPc2	trpaslík
byla	být	k5eAaImAgFnS	být
prokázána	prokázán	k2eAgFnSc1d1	prokázána
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byly	být	k5eAaImAgFnP	být
jen	jen	k9	jen
hypotetickými	hypotetický	k2eAgInPc7d1	hypotetický
objekty	objekt	k1gInPc7	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jich	on	k3xPp3gMnPc2	on
známe	znát	k5eAaImIp1nP	znát
mnoho	mnoho	k6eAd1	mnoho
<g/>
,	,	kIx,	,
velkým	velký	k2eAgNnSc7d1	velké
nalezištěm	naleziště	k1gNnSc7	naleziště
je	být	k5eAaImIp3nS	být
hvězdokupa	hvězdokupa	k1gFnSc1	hvězdokupa
Plejády	Plejáda	k1gFnSc2	Plejáda
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
hvězd	hvězda	k1gFnPc2	hvězda
starých	starý	k2eAgFnPc2d1	stará
jen	jen	k8xS	jen
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Termonukleární	termonukleární	k2eAgFnSc1d1	termonukleární
fúze	fúze	k1gFnSc1	fúze
hvězdám	hvězda	k1gFnPc3	hvězda
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vyzáří	vyzářit	k5eAaPmIp3nS	vyzářit
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
energeticky	energeticky	k6eAd1	energeticky
vyvážené	vyvážený	k2eAgFnPc1d1	vyvážená
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílem	rozdíl	k1gInSc7	rozdíl
mezi	mezi	k7c7	mezi
plně	plně	k6eAd1	plně
funkční	funkční	k2eAgFnSc7d1	funkční
hvězdou	hvězda	k1gFnSc7	hvězda
a	a	k8xC	a
hnědým	hnědý	k2eAgMnSc7d1	hnědý
trpaslíkem	trpaslík	k1gMnSc7	trpaslík
jsou	být	k5eAaImIp3nP	být
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
<g/>
:	:	kIx,	:
K	k	k7c3	k
zažehnutí	zažehnutí	k1gNnSc3	zažehnutí
termonukleárních	termonukleární	k2eAgFnPc2d1	termonukleární
reakcí	reakce	k1gFnPc2	reakce
je	být	k5eAaImIp3nS	být
třeba	třeba	k9	třeba
teplota	teplota	k1gFnSc1	teplota
alespoň	alespoň	k9	alespoň
8	[number]	k4	8
<g/>
×	×	k?	×
<g/>
106	[number]	k4	106
K	K	kA	K
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
přeměňovat	přeměňovat	k5eAaImF	přeměňovat
i	i	k9	i
lehký	lehký	k2eAgInSc4d1	lehký
vodík	vodík	k1gInSc4	vodík
na	na	k7c4	na
helium	helium	k1gNnSc4	helium
<g/>
.	.	kIx.	.
</s>
<s>
Hnědý	hnědý	k2eAgMnSc1d1	hnědý
trpaslík	trpaslík	k1gMnSc1	trpaslík
ale	ale	k8xC	ale
této	tento	k3xDgFnSc2	tento
teploty	teplota	k1gFnSc2	teplota
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
<g/>
.	.	kIx.	.
</s>
<s>
Sice	sice	k8xC	sice
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
některé	některý	k3yIgFnPc1	některý
termojaderné	termojaderný	k2eAgFnPc1d1	termojaderná
syntézy	syntéza	k1gFnPc1	syntéza
probíhají	probíhat	k5eAaImIp3nP	probíhat
<g/>
,	,	kIx,	,
nedochází	docházet	k5eNaImIp3nS	docházet
však	však	k9	však
ke	k	k7c3	k
slučování	slučování	k1gNnSc3	slučování
lehkého	lehký	k2eAgInSc2d1	lehký
vodíku	vodík	k1gInSc2	vodík
1	[number]	k4	1
<g/>
H.	H.	kA	H.
Deuteriová	Deuteriový	k2eAgFnSc1d1	Deuteriový
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
druhý	druhý	k4xOgInSc1	druhý
krok	krok	k1gInSc1	krok
z	z	k7c2	z
proton-protonového	protonrotonový	k2eAgInSc2d1	proton-protonový
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
k	k	k7c3	k
energetickému	energetický	k2eAgNnSc3d1	energetické
vyrovnání	vyrovnání	k1gNnSc3	vyrovnání
hnědého	hnědý	k2eAgMnSc2d1	hnědý
trpaslíka	trpaslík	k1gMnSc2	trpaslík
nestačí	stačit	k5eNaBmIp3nS	stačit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
těžkého	těžký	k2eAgInSc2d1	těžký
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
deuteria	deuterium	k1gNnSc2	deuterium
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nedostatek	nedostatek	k1gInSc1	nedostatek
<g/>
:	:	kIx,	:
Hnědý	hnědý	k2eAgMnSc1d1	hnědý
trpaslík	trpaslík	k1gMnSc1	trpaslík
má	mít	k5eAaImIp3nS	mít
ještě	ještě	k9	ještě
příliš	příliš	k6eAd1	příliš
nízkou	nízký	k2eAgFnSc4d1	nízká
hmotnost	hmotnost	k1gFnSc4	hmotnost
a	a	k8xC	a
nevytvoří	vytvořit	k5eNaPmIp3nS	vytvořit
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
tlak	tlak	k1gInSc1	tlak
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
vlastního	vlastní	k2eAgNnSc2d1	vlastní
deuteria	deuterium	k1gNnSc2	deuterium
z	z	k7c2	z
lehkého	lehký	k2eAgInSc2d1	lehký
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
po	po	k7c6	po
vyčerpání	vyčerpání	k1gNnSc6	vyčerpání
jeho	jeho	k3xOp3gFnPc2	jeho
zásob	zásoba	k1gFnPc2	zásoba
chladne	chladnout	k5eAaImIp3nS	chladnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
hnědých	hnědý	k2eAgMnPc2d1	hnědý
trpaslíků	trpaslík	k1gMnPc2	trpaslík
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
spaluje	spalovat	k5eAaImIp3nS	spalovat
(	(	kIx(	(
<g/>
rozuměj	rozumět	k5eAaImRp2nS	rozumět
slučuje	slučovat	k5eAaImIp3nS	slučovat
<g/>
)	)	kIx)	)
deuterium	deuterium	k1gNnSc1	deuterium
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
lehkého	lehký	k2eAgNnSc2d1	lehké
hélia	hélium	k1gNnSc2	hélium
<g/>
:	:	kIx,	:
D	D	kA	D
+	+	kIx~	+
pH	ph	kA	ph
→	→	k?	→
3	[number]	k4	3
<g/>
He	he	k0	he
+	+	kIx~	+
γ	γ	k?	γ
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jinou	jiný	k2eAgFnSc7d1	jiná
notací	notace	k1gFnSc7	notace
to	ten	k3xDgNnSc1	ten
samé	samý	k3xTgFnSc3	samý
<g/>
:	:	kIx,	:
2H	[number]	k4	2H
+	+	kIx~	+
1H	[number]	k4	1H
→	→	k?	→
3	[number]	k4	3
<g/>
He	he	k0	he
+	+	kIx~	+
γ	γ	k?	γ
)	)	kIx)	)
Deuteriové	Deuteriový	k2eAgFnSc2d1	Deuteriový
reakce	reakce	k1gFnSc2	reakce
<g />
.	.	kIx.	.
</s>
<s>
k	k	k7c3	k
produkci	produkce	k1gFnSc3	produkce
energie	energie	k1gFnSc2	energie
hnědého	hnědý	k2eAgMnSc2d1	hnědý
trpaslíka	trpaslík	k1gMnSc2	trpaslík
přispívají	přispívat	k5eAaImIp3nP	přispívat
jen	jen	k9	jen
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
vývoje	vývoj	k1gInSc2	vývoj
hnědého	hnědý	k2eAgMnSc2d1	hnědý
trpaslíka	trpaslík	k1gMnSc2	trpaslík
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
vzniku	vznik	k1gInSc6	vznik
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
tento	tento	k3xDgInSc4	tento
mohl	moct	k5eAaImAgInS	moct
do	do	k7c2	do
vínku	vínek	k1gInSc2	vínek
dostat	dostat	k5eAaPmF	dostat
i	i	k9	i
lithium	lithium	k1gNnSc4	lithium
<g/>
:	:	kIx,	:
Pro	pro	k7c4	pro
spalování	spalování	k1gNnSc4	spalování
lithia	lithium	k1gNnSc2	lithium
na	na	k7c4	na
hélium	hélium	k1gNnSc4	hélium
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
beryllium	beryllium	k1gNnSc4	beryllium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
dokonce	dokonce	k9	dokonce
ještě	ještě	k9	ještě
méně	málo	k6eAd2	málo
vstupní	vstupní	k2eAgFnSc2d1	vstupní
energie	energie	k1gFnSc2	energie
než	než	k8xS	než
pro	pro	k7c4	pro
fúzi	fúze	k1gFnSc4	fúze
deuteria	deuterium	k1gNnSc2	deuterium
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
lithium	lithium	k1gNnSc1	lithium
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
na	na	k7c4	na
hélium	hélium	k1gNnSc4	hélium
podstatně	podstatně	k6eAd1	podstatně
rychleji	rychle	k6eAd2	rychle
a	a	k8xC	a
bouřlivěji	bouřlivě	k6eAd2	bouřlivě
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
vyčerpání	vyčerpání	k1gNnSc6	vyčerpání
dochází	docházet	k5eAaImIp3nS	docházet
už	už	k6eAd1	už
jen	jen	k9	jen
k	k	k7c3	k
deuteriovým	deuteriový	k2eAgFnPc3d1	deuteriová
reakcím	reakce	k1gFnPc3	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
přítomnost	přítomnost	k1gFnSc1	přítomnost
či	či	k8xC	či
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
lithia	lithium	k1gNnSc2	lithium
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
předmětem	předmět	k1gInSc7	předmět
testu	test	k1gInSc2	test
na	na	k7c4	na
hnědého	hnědý	k2eAgMnSc4d1	hnědý
trpaslíka	trpaslík	k1gMnSc4	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
test	test	k1gInSc1	test
je	být	k5eAaImIp3nS	být
však	však	k9	však
nepřesný	přesný	k2eNgMnSc1d1	nepřesný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ho	on	k3xPp3gMnSc4	on
ještě	ještě	k9	ještě
nemusel	muset	k5eNaImAgMnS	muset
spálit	spálit	k5eAaPmF	spálit
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
může	moct	k5eAaImIp3nS	moct
lithium	lithium	k1gNnSc4	lithium
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
i	i	k9	i
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
plně	plně	k6eAd1	plně
funkčních	funkční	k2eAgFnPc2d1	funkční
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
takto	takto	k6eAd1	takto
získaná	získaný	k2eAgFnSc1d1	získaná
znamená	znamenat	k5eAaImIp3nS	znamenat
pro	pro	k7c4	pro
hnědého	hnědý	k2eAgMnSc4d1	hnědý
trpaslíka	trpaslík	k1gMnSc4	trpaslík
příspěvek	příspěvek	k1gInSc4	příspěvek
pro	pro	k7c4	pro
zářivou	zářivý	k2eAgFnSc4d1	zářivá
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
např.	např.	kA	např.
u	u	k7c2	u
objektů	objekt	k1gInPc2	objekt
o	o	k7c4	o
15	[number]	k4	15
<g/>
násobku	násobek	k1gInSc2	násobek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Jupitera	Jupiter	k1gMnSc2	Jupiter
(	(	kIx(	(
<g/>
15	[number]	k4	15
MJ	mj	kA	mj
<g/>
)	)	kIx)	)
vystačí	vystačit	k5eAaBmIp3nS	vystačit
asi	asi	k9	asi
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
pro	pro	k7c4	pro
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
13	[number]	k4	13
MJ	mj	kA	mj
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
energetický	energetický	k2eAgInSc1d1	energetický
příspěvek	příspěvek	k1gInSc1	příspěvek
zanedbatelný	zanedbatelný	k2eAgInSc1d1	zanedbatelný
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
hnědým	hnědý	k2eAgMnSc7d1	hnědý
trpaslíkem	trpaslík	k1gMnSc7	trpaslík
a	a	k8xC	a
obří	obří	k2eAgFnSc7d1	obří
planetou	planeta	k1gFnSc7	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
ovšem	ovšem	k9	ovšem
o	o	k7c4	o
přesnou	přesný	k2eAgFnSc4d1	přesná
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
13	[number]	k4	13
MJ	mj	kA	mj
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
jen	jen	k9	jen
momentálně	momentálně	k6eAd1	momentálně
všeobecně	všeobecně	k6eAd1	všeobecně
přijímané	přijímaný	k2eAgNnSc4d1	přijímané
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Deuteriové	Deuteriový	k2eAgFnPc1d1	Deuteriový
reakce	reakce	k1gFnPc1	reakce
mohou	moct	k5eAaImIp3nP	moct
přinášet	přinášet	k5eAaImF	přinášet
dokonce	dokonce	k9	dokonce
i	i	k9	i
natolik	natolik	k6eAd1	natolik
málo	málo	k4c1	málo
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
že	že	k8xS	že
trpaslík	trpaslík	k1gMnSc1	trpaslík
začne	začít	k5eAaPmIp3nS	začít
vychládat	vychládat	k5eAaImF	vychládat
ještě	ještě	k9	ještě
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
úplným	úplný	k2eAgNnSc7d1	úplné
vyčerpáním	vyčerpání	k1gNnSc7	vyčerpání
<g/>
,	,	kIx,	,
a	a	k8xC	a
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
jen	jen	k9	jen
slabě	slabě	k6eAd1	slabě
žhnout	žhnout	k5eAaImF	žhnout
<g/>
.	.	kIx.	.
</s>
<s>
Dosažení	dosažení	k1gNnSc1	dosažení
stavu	stav	k1gInSc2	stav
planety	planeta	k1gFnSc2	planeta
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
prohlásit	prohlásit	k5eAaPmF	prohlásit
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
1000	[number]	k4	1000
K.	K.	kA	K.
Vývoj	vývoj	k1gInSc1	vývoj
těchto	tento	k3xDgInPc2	tento
objektů	objekt	k1gInPc2	objekt
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
jeho	on	k3xPp3gInSc4	on
počáteční	počáteční	k2eAgInSc4d1	počáteční
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
hnědých	hnědý	k2eAgMnPc2d1	hnědý
trpaslíků	trpaslík	k1gMnPc2	trpaslík
menší	malý	k2eAgFnSc2d2	menší
než	než	k8xS	než
0,08	[number]	k4	0,08
hmotností	hmotnost	k1gFnSc7	hmotnost
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
proto	proto	k8xC	proto
nemůže	moct	k5eNaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
zapálení	zapálení	k1gNnSc3	zapálení
vodíkových	vodíkový	k2eAgFnPc2d1	vodíková
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
pokryly	pokrýt	k5eAaPmAgFnP	pokrýt
energetické	energetický	k2eAgFnPc4d1	energetická
ztráty	ztráta	k1gFnPc4	ztráta
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Objekt	objekt	k1gInSc1	objekt
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
gravitačně	gravitačně	k6eAd1	gravitačně
hroutí	hroutit	k5eAaImIp3nS	hroutit
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
elektronová	elektronový	k2eAgFnSc1d1	elektronová
degenerace	degenerace	k1gFnSc1	degenerace
postoupí	postoupit	k5eAaPmIp3nS	postoupit
do	do	k7c2	do
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Svrchní	svrchní	k2eAgFnPc1d1	svrchní
vrstvy	vrstva	k1gFnPc1	vrstva
<g/>
,	,	kIx,	,
kterých	který	k3yIgMnPc2	který
se	se	k3xPyFc4	se
elektronová	elektronový	k2eAgFnSc1d1	elektronová
degenerace	degenerace	k1gFnSc1	degenerace
ještě	ještě	k6eAd1	ještě
nedotkla	dotknout	k5eNaPmAgFnS	dotknout
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
gravitačně	gravitačně	k6eAd1	gravitačně
kontrahují	kontrahovat	k5eAaBmIp3nP	kontrahovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
poloměru	poloměr	k1gInSc6	poloměr
jsou	být	k5eAaImIp3nP	být
nepatrné	nepatrný	k2eAgFnPc1d1	nepatrná
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
degenerace	degenerace	k1gFnPc1	degenerace
postoupí	postoupit	k5eAaPmIp3nP	postoupit
i	i	k9	i
do	do	k7c2	do
svrchních	svrchní	k2eAgFnPc2d1	svrchní
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
kontrakce	kontrakce	k1gFnSc1	kontrakce
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ještě	ještě	k9	ještě
objekt	objekt	k1gInSc1	objekt
září	září	k1gNnSc2	září
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
své	svůj	k3xOyFgFnSc2	svůj
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
i	i	k8xC	i
povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
stane	stanout	k5eAaPmIp3nS	stanout
tmavý	tmavý	k2eAgInSc1d1	tmavý
<g/>
,	,	kIx,	,
nezářící	zářící	k2eNgInSc1d1	zářící
objekt	objekt	k1gInSc1	objekt
–	–	k?	–
černý	černý	k1gMnSc1	černý
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
.	.	kIx.	.
</s>
