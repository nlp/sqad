<p>
<s>
Sv.	sv.	kA	sv.
Sixtus	Sixtus	k1gMnSc1	Sixtus
I.	I.	kA	I.
byl	být	k5eAaImAgInS	být
7	[number]	k4	7
<g/>
.	.	kIx.	.
papežem	papež	k1gMnSc7	papež
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pontifikát	pontifikát	k1gInSc1	pontifikát
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
let	léto	k1gNnPc2	léto
115	[number]	k4	115
<g/>
/	/	kIx~	/
<g/>
116	[number]	k4	116
<g/>
–	–	k?	–
<g/>
125	[number]	k4	125
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Sixtus	Sixtus	k1gMnSc1	Sixtus
byl	být	k5eAaImAgMnS	být
sedmým	sedmý	k4xOgMnSc7	sedmý
papežem	papež	k1gMnSc7	papež
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejstarších	starý	k2eAgInPc6d3	nejstarší
dokumentech	dokument	k1gInPc6	dokument
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
psáno	psát	k5eAaImNgNnS	psát
jako	jako	k8xS	jako
Xystus	Xystus	k1gInSc1	Xystus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
znamený	znamený	k2eAgInSc4d1	znamený
"	"	kIx"	"
<g/>
oholený	oholený	k2eAgInSc4d1	oholený
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Liber	libra	k1gFnPc2	libra
Pontificalis	Pontificalis	k1gFnPc2	Pontificalis
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Pastor	pastor	k1gMnSc1	pastor
<g/>
.	.	kIx.	.
</s>
<s>
Papežem	Papež	k1gMnSc7	Papež
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
vlády	vláda	k1gFnPc4	vláda
císaře	císař	k1gMnSc2	císař
Hadriána	Hadrián	k1gMnSc2	Hadrián
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
tradici	tradice	k1gFnSc6	tradice
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
připisují	připisovat	k5eAaImIp3nP	připisovat
tato	tento	k3xDgNnPc4	tento
nařízení	nařízení	k1gNnPc4	nařízení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
nikdo	nikdo	k3yNnSc1	nikdo
<g/>
,	,	kIx,	,
vyjma	vyjma	k7c2	vyjma
posvěcené	posvěcený	k2eAgFnSc2d1	posvěcená
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
dotýkat	dotýkat	k5eAaImF	dotýkat
bohoslužebných	bohoslužebný	k2eAgFnPc2d1	bohoslužebná
nádob	nádoba	k1gFnPc2	nádoba
a	a	k8xC	a
korporale	korporale	k1gNnSc1	korporale
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
z	z	k7c2	z
pravého	pravý	k2eAgNnSc2d1	pravé
lněného	lněný	k2eAgNnSc2d1	lněné
plátna	plátno	k1gNnSc2	plátno
</s>
</p>
<p>
<s>
biskupové	biskup	k1gMnPc1	biskup
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
povoláni	povolat	k5eAaPmNgMnP	povolat
ke	k	k7c3	k
Svatému	svatý	k2eAgInSc3d1	svatý
stolci	stolec	k1gInSc3	stolec
se	se	k3xPyFc4	se
nesmějí	smát	k5eNaImIp3nP	smát
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
původní	původní	k2eAgFnSc2d1	původní
diecéze	diecéze	k1gFnSc2	diecéze
bez	bez	k7c2	bez
výslovného	výslovný	k2eAgNnSc2d1	výslovné
svolení	svolení	k1gNnSc2	svolení
apoštolským	apoštolský	k2eAgInSc7d1	apoštolský
listem	list	k1gInSc7	list
</s>
</p>
<p>
<s>
část	část	k1gFnSc1	část
mše	mše	k1gFnSc2	mše
Sanctus	Sanctus	k1gInSc1	Sanctus
recitují	recitovat	k5eAaImIp3nP	recitovat
kněží	kněz	k1gMnPc1	kněz
společně	společně	k6eAd1	společně
s	s	k7c7	s
věřícímiTraduje	věřícímiTradovat	k5eAaBmIp3nS	věřícímiTradovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
mučedníkem	mučedník	k1gMnSc7	mučedník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
tvrzení	tvrzení	k1gNnSc3	tvrzení
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgInPc4	žádný
důkazy	důkaz	k1gInPc4	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
vedle	vedle	k7c2	vedle
hrobky	hrobka	k1gFnSc2	hrobka
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
údajně	údajně	k6eAd1	údajně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1132	[number]	k4	1132
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
Alatri	Alatr	k1gFnSc2	Alatr
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jiné	jiný	k2eAgInPc1d1	jiný
prameny	pramen	k1gInPc1	pramen
(	(	kIx(	(
<g/>
O.	O.	kA	O.
Jozzi	Jozze	k1gFnSc6	Jozze
:	:	kIx,	:
"	"	kIx"	"
<g/>
Il	Il	k1gFnSc1	Il
corpo	corpa	k1gFnSc5	corpa
di	di	k?	di
S.	S.	kA	S.
Sisto	Sisto	k1gNnSc1	Sisto
I.	I.	kA	I.
<g/>
,	,	kIx,	,
papa	papa	k1gMnSc1	papa
e	e	k0	e
martire	martir	k1gInSc5	martir
rivendicato	rivendicato	k6eAd1	rivendicato
alla	all	k2eAgFnSc1d1	alla
basilica	basilic	k2eAgFnSc1d1	basilica
Vaticana	Vaticana	k1gFnSc1	Vaticana
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Rome	Rom	k1gMnSc5	Rom
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
v	v	k7c6	v
Bazilice	bazilika	k1gFnSc6	bazilika
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
památka	památka	k1gFnSc1	památka
se	se	k3xPyFc4	se
připomíná	připomínat	k5eAaImIp3nS	připomínat
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
GELMI	GELMI	kA	GELMI
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Papežové	Papež	k1gMnPc1	Papež
:	:	kIx,	:
Od	od	k7c2	od
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
po	po	k7c4	po
Jana	Jan	k1gMnSc4	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
328	[number]	k4	328
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
457	[number]	k4	457
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MAXWELL-STUART	MAXWELL-STUART	k?	MAXWELL-STUART
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
G.	G.	kA	G.
Papežové	Papež	k1gMnPc1	Papež
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
Pavlu	Pavel	k1gMnSc3	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
servis	servis	k1gInSc1	servis
<g/>
)	)	kIx)	)
240	[number]	k4	240
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902300	[number]	k4	902300
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RENDINA	RENDINA	kA	RENDINA
<g/>
,	,	kIx,	,
Claudio	Claudio	k6eAd1	Claudio
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
papežů	papež	k1gMnPc2	papež
:	:	kIx,	:
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
:	:	kIx,	:
životopisy	životopis	k1gInPc4	životopis
265	[number]	k4	265
římských	římský	k2eAgMnPc2d1	římský
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
714	[number]	k4	714
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
574	[number]	k4	574
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VONDRUŠKA	Vondruška	k1gMnSc1	Vondruška
<g/>
,	,	kIx,	,
Isidor	Isidor	k1gMnSc1	Isidor
<g/>
.	.	kIx.	.
</s>
<s>
Životopisy	životopis	k1gInPc1	životopis
svatých	svatá	k1gFnPc2	svatá
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
dějin	dějiny	k1gFnPc2	dějiny
církevních	církevní	k2eAgFnPc2d1	církevní
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kuncíř	Kuncíř	k1gMnSc1	Kuncíř
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sixtus	Sixtus	k1gMnSc1	Sixtus
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
)	)	kIx)	)
</s>
</p>
