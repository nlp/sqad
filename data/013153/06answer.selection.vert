<s>
Vydra	vydra	k1gFnSc1	vydra
mořská	mořský	k2eAgFnSc1d1	mořská
(	(	kIx(	(
<g/>
Enhydra	Enhydra	k1gFnSc1	Enhydra
lutris	lutris	k1gFnSc2	lutris
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
kalan	kalan	k1gInSc1	kalan
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
druh	druh	k1gInSc1	druh
vydry	vydra	k1gFnSc2	vydra
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rosomákem	rosomák	k1gMnSc7	rosomák
největší	veliký	k2eAgFnSc1d3	veliký
šelma	šelma	k1gFnSc1	šelma
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lasicovitých	lasicovitý	k2eAgMnPc2d1	lasicovitý
<g/>
.	.	kIx.	.
</s>
