<p>
<s>
Vydra	vydra	k1gFnSc1	vydra
mořská	mořský	k2eAgFnSc1d1	mořská
(	(	kIx(	(
<g/>
Enhydra	Enhydra	k1gFnSc1	Enhydra
lutris	lutris	k1gFnSc2	lutris
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
kalan	kalan	k1gInSc1	kalan
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
druh	druh	k1gInSc1	druh
vydry	vydra	k1gFnSc2	vydra
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rosomákem	rosomák	k1gMnSc7	rosomák
největší	veliký	k2eAgFnSc1d3	veliký
šelma	šelma	k1gFnSc1	šelma
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lasicovitých	lasicovitý	k2eAgMnPc2d1	lasicovitý
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
vyder	vydra	k1gFnPc2	vydra
nejvíce	nejvíce	k6eAd1	nejvíce
přizpůsobena	přizpůsobit	k5eAaPmNgFnS	přizpůsobit
vodnímu	vodní	k2eAgInSc3d1	vodní
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
nejhustší	hustý	k2eAgFnSc4d3	nejhustší
srst	srst	k1gFnSc4	srst
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
recentních	recentní	k2eAgMnPc2d1	recentní
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
mořské	mořský	k2eAgNnSc1d1	mořské
pobřeží	pobřeží	k1gNnSc1	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
od	od	k7c2	od
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
asijské	asijský	k2eAgNnSc4d1	asijské
pobřeží	pobřeží	k1gNnSc4	pobřeží
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Aljašku	Aljaška	k1gFnSc4	Aljaška
až	až	k9	až
po	po	k7c6	po
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
vyhubena	vyhubit	k5eAaPmNgFnS	vyhubit
lovci	lovec	k1gMnPc1	lovec
kožešin	kožešina	k1gFnPc2	kožešina
<g/>
.	.	kIx.	.
</s>
<s>
Chráněna	chráněn	k2eAgFnSc1d1	chráněna
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
teprve	teprve	k6eAd1	teprve
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	s	k7c7	s
převážně	převážně	k6eAd1	převážně
mořskými	mořský	k2eAgFnPc7d1	mořská
ježovkami	ježovka	k1gFnPc7	ježovka
<g/>
,	,	kIx,	,
ušněmi	ušeň	k1gFnPc7	ušeň
<g/>
,	,	kIx,	,
mlži	mlž	k1gMnPc7	mlž
<g/>
,	,	kIx,	,
hlavonožci	hlavonožec	k1gMnPc7	hlavonožec
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
živočichy	živočich	k1gMnPc7	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k6eAd1	jen
mořských	mořský	k2eAgFnPc2d1	mořská
ježovek	ježovka	k1gFnPc2	ježovka
zkonzumuje	zkonzumovat	k5eAaPmIp3nS	zkonzumovat
denně	denně	k6eAd1	denně
několik	několik	k4yIc4	několik
kilogramů	kilogram	k1gInPc2	kilogram
(	(	kIx(	(
<g/>
až	až	k9	až
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
své	svůj	k3xOyFgFnSc2	svůj
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vydra	vydra	k1gFnSc1	vydra
mořská	mořský	k2eAgFnSc1d1	mořská
je	být	k5eAaImIp3nS	být
hravá	hravý	k2eAgFnSc1d1	hravá
a	a	k8xC	a
zvědavá	zvědavý	k2eAgFnSc1d1	zvědavá
a	a	k8xC	a
tyto	tento	k3xDgFnPc4	tento
její	její	k3xOp3gFnPc4	její
vlastnosti	vlastnost	k1gFnPc4	vlastnost
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nádhernou	nádherný	k2eAgFnSc7d1	nádherná
kožešinou	kožešina	k1gFnSc7	kožešina
zapříčinily	zapříčinit	k5eAaPmAgFnP	zapříčinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
téměř	téměř	k6eAd1	téměř
vyhubil	vyhubit	k5eAaPmAgInS	vyhubit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Vydra	vydra	k1gFnSc1	vydra
mořská	mořský	k2eAgFnSc1d1	mořská
má	mít	k5eAaImIp3nS	mít
zavalité	zavalitý	k2eAgNnSc4d1	zavalité
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ocasu	ocas	k1gInSc2	ocas
měří	měřit	k5eAaImIp3nS	měřit
100	[number]	k4	100
-	-	kIx~	-
145	[number]	k4	145
cm	cm	kA	cm
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
asi	asi	k9	asi
30	[number]	k4	30
cm	cm	kA	cm
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
mírně	mírně	k6eAd1	mírně
zploštělý	zploštělý	k2eAgInSc4d1	zploštělý
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
15-45	[number]	k4	15-45
kg	kg	kA	kg
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgFnPc1d2	veliký
než	než	k8xS	než
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
srsti	srst	k1gFnSc2	srst
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
tmavě	tmavě	k6eAd1	tmavě
hnědé	hnědý	k2eAgFnPc1d1	hnědá
až	až	k8xS	až
šedohnědé	šedohnědý	k2eAgFnPc1d1	šedohnědá
<g/>
,	,	kIx,	,
na	na	k7c6	na
obličeji	obličej	k1gInSc6	obličej
světlejší	světlý	k2eAgInSc1d2	světlejší
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgNnPc1d2	starší
zvířata	zvíře	k1gNnPc1	zvíře
mají	mít	k5eAaImIp3nP	mít
obličej	obličej	k1gInSc4	obličej
žlutavý	žlutavý	k2eAgInSc4d1	žlutavý
až	až	k6eAd1	až
bílý	bílý	k2eAgInSc4d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
vydry	vydra	k1gFnSc2	vydra
mořské	mořský	k2eAgFnSc2d1	mořská
má	mít	k5eAaImIp3nS	mít
mírně	mírně	k6eAd1	mírně
klabonosý	klabonosý	k2eAgInSc1d1	klabonosý
profil	profil	k1gInSc1	profil
<g/>
,	,	kIx,	,
nápadné	nápadný	k2eAgInPc1d1	nápadný
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
vibirsy	vibirs	k1gInPc4	vibirs
(	(	kIx(	(
<g/>
hmatové	hmatový	k2eAgInPc4d1	hmatový
vousy	vous	k1gInPc4	vous
<g/>
)	)	kIx)	)
a	a	k8xC	a
výrazné	výrazný	k2eAgFnPc4d1	výrazná
hnědé	hnědý	k2eAgFnPc4d1	hnědá
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
boltce	boltec	k1gInPc1	boltec
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgInPc1d1	malý
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
ukryté	ukrytý	k2eAgFnPc1d1	ukrytá
v	v	k7c6	v
srsti	srst	k1gFnSc6	srst
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
uzavíratelné	uzavíratelný	k2eAgNnSc1d1	uzavíratelné
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
prsty	prst	k1gInPc7	prst
má	mít	k5eAaImIp3nS	mít
vydra	vydra	k1gFnSc1	vydra
mořská	mořský	k2eAgFnSc1d1	mořská
vyvinuté	vyvinutý	k2eAgFnPc4d1	vyvinutá
plovací	plovací	k2eAgFnPc4d1	plovací
blány	blána	k1gFnPc4	blána
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zvláště	zvláště	k6eAd1	zvláště
patrné	patrný	k2eAgFnPc1d1	patrná
na	na	k7c6	na
zadních	zadní	k2eAgFnPc6d1	zadní
končetinách	končetina	k1gFnPc6	končetina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Rodí	rodit	k5eAaImIp3nS	rodit
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc1	jeden
mládě	mládě	k1gNnSc1	mládě
<g/>
,	,	kIx,	,
ojediněle	ojediněle	k6eAd1	ojediněle
dvojčata	dvojče	k1gNnPc4	dvojče
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgNnSc1d1	Malé
mládě	mládě	k1gNnSc1	mládě
se	se	k3xPyFc4	se
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gInSc4	on
matka	matka	k1gFnSc1	matka
nosí	nosit	k5eAaImIp3nS	nosit
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
vydra	vydra	k1gFnSc1	vydra
mořská	mořský	k2eAgFnSc1d1	mořská
může	moct	k5eAaImIp3nS	moct
dožít	dožít	k5eAaPmF	dožít
až	až	k9	až
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
ale	ale	k8xC	ale
zřídka	zřídka	k6eAd1	zřídka
žijí	žít	k5eAaImIp3nP	žít
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
úhynu	úhyn	k1gInSc2	úhyn
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
značné	značný	k2eAgNnSc4d1	značné
opotřebení	opotřebení	k1gNnSc4	opotřebení
chrupu	chrup	k1gInSc2	chrup
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
následek	následek	k1gInSc4	následek
konzumace	konzumace	k1gFnSc2	konzumace
živočichů	živočich	k1gMnPc2	živočich
s	s	k7c7	s
tvrdými	tvrdý	k2eAgFnPc7d1	tvrdá
skořápkami	skořápka	k1gFnPc7	skořápka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vydry	vydra	k1gFnPc1	vydra
mají	mít	k5eAaImIp3nP	mít
mnoho	mnoho	k6eAd1	mnoho
přirozených	přirozený	k2eAgMnPc2d1	přirozený
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Vydra	vydra	k1gFnSc1	vydra
říční	říční	k2eAgFnSc2d1	říční
</s>
</p>
<p>
<s>
Vydra	vydra	k1gFnSc1	vydra
severoamerická	severoamerický	k2eAgFnSc1d1	severoamerická
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vydra	vydra	k1gFnSc1	vydra
mořská	mořský	k2eAgFnSc1d1	mořská
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
vydra	vydra	k1gFnSc1	vydra
mořská	mořský	k2eAgFnSc1d1	mořská
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
