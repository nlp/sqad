<s>
Intimní	intimní	k2eAgNnSc1d1	intimní
osvětlení	osvětlení	k1gNnSc1	osvětlení
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc4d1	český
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
natočený	natočený	k2eAgInSc4d1	natočený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
režisérem	režisér	k1gMnSc7	režisér
Ivanem	Ivan	k1gMnSc7	Ivan
Passerem	Passer	k1gMnSc7	Passer
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gInSc1	jeho
dlouhometrážní	dlouhometrážní	k2eAgInSc1d1	dlouhometrážní
debut	debut	k1gInSc1	debut
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
natáčen	natáčet	k5eAaImNgInS	natáčet
v	v	k7c6	v
reálných	reálný	k2eAgInPc6d1	reálný
interiérech	interiér	k1gInPc6	interiér
s	s	k7c7	s
hrajícími	hrající	k2eAgMnPc7d1	hrající
neherci	neherec	k1gMnPc7	neherec
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
přineslo	přinést	k5eAaPmAgNnS	přinést
velkou	velký	k2eAgFnSc4d1	velká
míru	míra	k1gFnSc4	míra
autentičnosti	autentičnost	k1gFnSc2	autentičnost
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
význačným	význačný	k2eAgInSc7d1	význačný
filmem	film	k1gInSc7	film
české	český	k2eAgFnSc2d1	Česká
nové	nový	k2eAgFnSc2d1	nová
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
přijat	přijmout	k5eAaPmNgMnS	přijmout
pozitivně	pozitivně	k6eAd1	pozitivně
domácí	domácí	k2eAgMnSc1d1	domácí
i	i	k9	i
zahraniční	zahraniční	k2eAgFnSc7d1	zahraniční
odbornou	odborný	k2eAgFnSc7d1	odborná
kritikou	kritika	k1gFnSc7	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
běžného	běžný	k2eAgInSc2d1	běžný
života	život	k1gInSc2	život
obyčejné	obyčejný	k2eAgFnSc2d1	obyčejná
rodiny	rodina	k1gFnSc2	rodina
na	na	k7c6	na
vesnici	vesnice	k1gFnSc6	vesnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
pospolu	pospolu	k6eAd1	pospolu
několik	několik	k4yIc4	několik
generací	generace	k1gFnPc2	generace
<g/>
,	,	kIx,	,
staví	stavit	k5eAaPmIp3nS	stavit
se	se	k3xPyFc4	se
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
amatérský	amatérský	k2eAgInSc1d1	amatérský
muzicíruje	muzicírovat	k5eAaImIp3nS	muzicírovat
a	a	k8xC	a
vedou	vést	k5eAaImIp3nP	vést
hovory	hovor	k1gInPc4	hovor
o	o	k7c6	o
všem	všecek	k3xTgNnSc6	všecek
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yQgFnPc2	který
tuto	tento	k3xDgFnSc4	tento
rodinu	rodina	k1gFnSc4	rodina
navštíví	navštívit	k5eAaPmIp3nS	navštívit
starý	starý	k2eAgMnSc1d1	starý
přítel	přítel	k1gMnSc1	přítel
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
návštěva	návštěva	k1gFnSc1	návštěva
dává	dávat	k5eAaImIp3nS	dávat
možnost	možnost	k1gFnSc4	možnost
k	k	k7c3	k
bilancování	bilancování	k1gNnSc3	bilancování
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Intimní	intimní	k2eAgNnSc1d1	intimní
osvětlení	osvětlení	k1gNnSc1	osvětlení
<g/>
:	:	kIx,	:
komika	komika	k1gFnSc1	komika
a	a	k8xC	a
lyrika	lyrika	k1gFnSc1	lyrika
banality	banalita	k1gFnSc2	banalita
–	–	k?	–
text	text	k1gInSc1	text
Jiřího	Jiří	k1gMnSc2	Jiří
Voráče	Voráč	k1gMnSc2	Voráč
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
PDF	PDF	kA	PDF
</s>
