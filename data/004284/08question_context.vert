<s>
Ikarie	Ikarie	k1gFnSc1	Ikarie
XB	XB	kA	XB
1	[number]	k4	1
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
a	a	k8xC	a
jediný	jediný	k2eAgInSc4d1	jediný
snímek	snímek	k1gInSc4	snímek
československé	československý	k2eAgFnSc2d1	Československá
kinematografie	kinematografie	k1gFnSc2	kinematografie
v	v	k7c6	v
žánru	žánr	k1gInSc6	žánr
klasické	klasický	k2eAgFnPc1d1	klasická
(	(	kIx(	(
<g/>
hard	hard	k1gInSc1	hard
<g/>
)	)	kIx)	)
science	science	k1gFnSc1	science
fiction	fiction	k1gInSc1	fiction
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
Jindřich	Jindřich	k1gMnSc1	Jindřich
Polák	Polák	k1gMnSc1	Polák
natočil	natočit	k5eAaBmAgMnS	natočit
snímek	snímek	k1gInSc4	snímek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
K	k	k7c3	k
Mrakům	mrak	k1gInPc3	mrak
Magellanovým	Magellanův	k2eAgInPc3d1	Magellanův
polského	polský	k2eAgMnSc2d1	polský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Stanisława	Stanisławus	k1gMnSc2	Stanisławus
Lema	Lemus	k1gMnSc2	Lemus
<g/>
.	.	kIx.	.
</s>
