<s>
Quo	Quo	k?	Quo
vadis	vadis	k1gInSc1	vadis
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
polského	polský	k2eAgMnSc2d1	polský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Henryka	Henryka	k1gFnSc1	Henryka
Sienkiewicze	Sienkiewicze	k1gFnSc1	Sienkiewicze
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
římském	římský	k2eAgNnSc6d1	římské
impériu	impérium	k1gNnSc6	impérium
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Nerona	Nero	k1gMnSc2	Nero
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
milostného	milostný	k2eAgInSc2d1	milostný
příběhu	příběh	k1gInSc2	příběh
římského	římský	k2eAgMnSc2d1	římský
patricije	patricij	k1gMnSc2	patricij
Vinicia	Vinicius	k1gMnSc2	Vinicius
a	a	k8xC	a
křesťanky	křesťanka	k1gFnSc2	křesťanka
Lygie	Lygie	k1gFnSc2	Lygie
popisuje	popisovat	k5eAaImIp3nS	popisovat
počátky	počátek	k1gInPc4	počátek
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
krutosti	krutost	k1gFnSc2	krutost
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
Neronovu	Neronův	k2eAgFnSc4d1	Neronova
znuděnou	znuděný	k2eAgFnSc4d1	znuděná
zvrhlost	zvrhlost	k1gFnSc4	zvrhlost
<g/>
,	,	kIx,	,
požár	požár	k1gInSc4	požár
Říma	Řím	k1gInSc2	Řím
atd.	atd.	kA	atd.
Významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
také	také	k9	také
Petronius	Petronius	k1gMnSc1	Petronius
<g/>
,	,	kIx,	,
Viniciův	Viniciův	k2eAgMnSc1d1	Viniciův
strýc	strýc	k1gMnSc1	strýc
a	a	k8xC	a
Neronův	Neronův	k2eAgMnSc1d1	Neronův
arbiter	arbiter	k1gMnSc1	arbiter
elegantiarum	elegantiarum	k1gInSc1	elegantiarum
neboli	neboli	k8xC	neboli
soudce	soudce	k1gMnSc1	soudce
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
vkusu	vkus	k1gInSc2	vkus
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
legendy	legenda	k1gFnSc2	legenda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
vylíčena	vylíčit	k5eAaPmNgFnS	vylíčit
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Petr	Petr	k1gMnSc1	Petr
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Neronova	Neronův	k2eAgNnSc2d1	Neronovo
pronásledování	pronásledování	k1gNnSc2	pronásledování
křesťanů	křesťan	k1gMnPc2	křesťan
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
svými	svůj	k3xOyFgMnPc7	svůj
stoupenci	stoupenec	k1gMnPc7	stoupenec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
odešel	odejít	k5eAaPmAgInS	odejít
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
do	do	k7c2	do
bezpečí	bezpečí	k1gNnSc2	bezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
potkává	potkávat	k5eAaImIp3nS	potkávat
Ježíše	Ježíš	k1gMnSc4	Ježíš
Krista	Kristus	k1gMnSc4	Kristus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
kráčí	kráčet	k5eAaImIp3nS	kráčet
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
překvapeně	překvapeně	k6eAd1	překvapeně
ptá	ptat	k5eAaImIp3nS	ptat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Quo	Quo	k1gFnSc1	Quo
vadis	vadis	k1gFnSc1	vadis
<g/>
,	,	kIx,	,
Domine	Domin	k1gMnSc5	Domin
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Kam	kam	k6eAd1	kam
kráčíš	kráčet	k5eAaImIp2nS	kráčet
<g/>
,	,	kIx,	,
Pane	Pan	k1gMnSc5	Pan
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Když	když	k8xS	když
ty	ty	k3xPp2nSc1	ty
opouštíš	opouštět	k5eAaImIp2nS	opouštět
můj	můj	k3xOp1gInSc4	můj
lid	lid	k1gInSc4	lid
<g/>
,	,	kIx,	,
jdu	jít	k5eAaImIp1nS	jít
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mě	já	k3xPp1nSc4	já
ukřižovali	ukřižovat	k5eAaPmAgMnP	ukřižovat
podruhé	podruhé	k6eAd1	podruhé
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
mu	on	k3xPp3gMnSc3	on
Ježíš	ježit	k5eAaImIp2nS	ježit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Petr	Petr	k1gMnSc1	Petr
obrací	obracet	k5eAaImIp3nS	obracet
nazpět	nazpět	k6eAd1	nazpět
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
umírá	umírat	k5eAaImIp3nS	umírat
na	na	k7c6	na
kříži	kříž	k1gInSc6	kříž
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnSc1	jeho
Pán	pán	k1gMnSc1	pán
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pokory	pokora	k1gFnSc2	pokora
však	však	k9	však
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
Ježíše	Ježíš	k1gMnPc4	Ježíš
nenapodoboval	napodobovat	k5eNaImAgInS	napodobovat
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
Sv.	sv.	kA	sv.
Petr	Petr	k1gMnSc1	Petr
vyprosí	vyprosit	k5eAaPmIp3nS	vyprosit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jej	on	k3xPp3gMnSc4	on
ukřižovali	ukřižovat	k5eAaPmAgMnP	ukřižovat
jinak	jinak	k6eAd1	jinak
–	–	k?	–
hlavou	hlava	k1gFnSc7	hlava
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Vinicius	Vinicius	k1gMnSc1	Vinicius
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
Lygie	Lygie	k1gFnSc2	Lygie
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
krále	král	k1gMnSc4	král
Lygů	Lyg	k1gInPc2	Lyg
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
od	od	k7c2	od
mala	mal	k1gInSc2	mal
jako	jako	k8xS	jako
rukojmí	rukojmí	k1gNnPc2	rukojmí
v	v	k7c6	v
domě	dům	k1gInSc6	dům
vznešené	vznešený	k2eAgFnSc2d1	vznešená
římské	římský	k2eAgFnSc2d1	římská
rodiny	rodina	k1gFnSc2	rodina
Aulů	Aul	k1gMnPc2	Aul
<g/>
.	.	kIx.	.
</s>
<s>
Zvyklý	zvyklý	k2eAgMnSc1d1	zvyklý
<g/>
,	,	kIx,	,
že	že	k8xS	že
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
<g/>
,	,	kIx,	,
nese	nést	k5eAaImIp3nS	nést
velmi	velmi	k6eAd1	velmi
těžko	těžko	k6eAd1	těžko
Lygiinu	Lygiin	k2eAgFnSc4d1	Lygiin
nedostupnost	nedostupnost	k1gFnSc4	nedostupnost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
o	o	k7c6	o
všem	všecek	k3xTgNnSc6	všecek
poví	povědět	k5eAaPmIp3nS	povědět
svému	svůj	k3xOyFgMnSc3	svůj
strýci	strýc	k1gMnSc3	strýc
Petroniovi	Petronius	k1gMnSc3	Petronius
<g/>
,	,	kIx,	,
oblíbenci	oblíbenec	k1gMnSc3	oblíbenec
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
dostanou	dostat	k5eAaPmIp3nP	dostat
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
Lygii	Lygie	k1gFnSc4	Lygie
z	z	k7c2	z
domu	dům	k1gInSc2	dům
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
Petronius	Petronius	k1gInSc1	Petronius
přemluví	přemluvit	k5eAaPmIp3nS	přemluvit
císaře	císař	k1gMnSc4	císař
Nerona	Nero	k1gMnSc4	Nero
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nechal	nechat	k5eAaPmAgMnS	nechat
dívku	dívka	k1gFnSc4	dívka
převézt	převézt	k5eAaPmF	převézt
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
má	mít	k5eAaImIp3nS	mít
Vinicius	Vinicius	k1gInSc4	Vinicius
dvořit	dvořit	k5eAaImF	dvořit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
večeři	večeře	k1gFnSc6	večeře
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
však	však	k9	však
opilý	opilý	k2eAgInSc4d1	opilý
Vinicius	Vinicius	k1gInSc4	Vinicius
Lygii	Lygie	k1gFnSc4	Lygie
prozradí	prozradit	k5eAaPmIp3nP	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zítra	zítra	k6eAd1	zítra
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
přijedou	přijet	k5eAaPmIp3nP	přijet
jeho	jeho	k3xOp3gMnPc1	jeho
otroci	otrok	k1gMnPc1	otrok
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
Lygii	Lygie	k1gFnSc4	Lygie
odvezli	odvézt	k5eAaPmAgMnP	odvézt
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
ale	ale	k9	ale
nechce	chtít	k5eNaImIp3nS	chtít
dělat	dělat	k5eAaImF	dělat
milenku	milenka	k1gFnSc4	milenka
Viniciovi	Viniciův	k2eAgMnPc1d1	Viniciův
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
raději	rád	k6eAd2	rád
večer	večer	k6eAd1	večer
uteče	utéct	k5eAaPmIp3nS	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Vinicius	Vinicius	k1gInSc1	Vinicius
běsní	běsnit	k5eAaImIp3nS	běsnit
a	a	k8xC	a
nechává	nechávat	k5eAaImIp3nS	nechávat
prohledat	prohledat	k5eAaPmF	prohledat
Řím	Řím	k1gInSc1	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Lygie	Lygie	k1gFnSc1	Lygie
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
ukryje	ukrýt	k5eAaPmIp3nS	ukrýt
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
křesťané	křesťan	k1gMnPc1	křesťan
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
ona	onen	k3xDgFnSc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ještě	ještě	k9	ještě
bylo	být	k5eAaImAgNnS	být
křesťanství	křesťanství	k1gNnSc1	křesťanství
římskými	římský	k2eAgInPc7d1	římský
úřady	úřad	k1gInPc7	úřad
pronásledováno	pronásledován	k2eAgNnSc1d1	pronásledováno
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
vyznavači	vyznavač	k1gMnPc1	vyznavač
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
ukrývat	ukrývat	k5eAaImF	ukrývat
v	v	k7c6	v
katakombách	katakomby	k1gFnPc6	katakomby
<g/>
.	.	kIx.	.
</s>
<s>
Petronius	Petronius	k1gInSc1	Petronius
chce	chtít	k5eAaImIp3nS	chtít
Viniciovi	Vinicius	k1gMnSc3	Vinicius
pomoct	pomoct	k5eAaPmF	pomoct
a	a	k8xC	a
najme	najmout	k5eAaPmIp3nS	najmout
Chilóna	Chilón	k1gMnSc4	Chilón
<g/>
,	,	kIx,	,
člověka	člověk	k1gMnSc4	člověk
z	z	k7c2	z
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
našel	najít	k5eAaPmAgMnS	najít
Lygii	Lygie	k1gFnSc4	Lygie
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Chilón	Chilón	k1gMnSc1	Chilón
Lygii	Lygie	k1gFnSc4	Lygie
najde	najít	k5eAaPmIp3nS	najít
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
ji	on	k3xPp3gFnSc4	on
s	s	k7c7	s
pomocí	pomocí	k7c2	pomocí
jednoho	jeden	k4xCgMnSc2	jeden
gladiátora	gladiátor	k1gMnSc2	gladiátor
unést	unést	k5eAaPmF	unést
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Lygii	Lygie	k1gFnSc4	Lygie
přijde	přijít	k5eAaPmIp3nS	přijít
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
silný	silný	k2eAgMnSc1d1	silný
barbar	barbar	k1gMnSc1	barbar
Ursus	Ursus	k1gMnSc1	Ursus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
gladiátora	gladiátor	k1gMnSc4	gladiátor
zabije	zabít	k5eAaPmIp3nS	zabít
a	a	k8xC	a
Vinicia	Vinicia	k1gFnSc1	Vinicia
zraní	zranit	k5eAaPmIp3nS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Vinicius	Vinicius	k1gMnSc1	Vinicius
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lygie	Lygie	k1gFnSc1	Lygie
je	být	k5eAaImIp3nS	být
křesťanka	křesťanka	k1gFnSc1	křesťanka
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
znovu	znovu	k6eAd1	znovu
ztratí	ztratit	k5eAaPmIp3nS	ztratit
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
se	se	k3xPyFc4	se
dozvědět	dozvědět	k5eAaPmF	dozvědět
o	o	k7c4	o
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
Lygie	Lygie	k1gFnSc1	Lygie
nemiluje	milovat	k5eNaImIp3nS	milovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
zaslíbená	zaslíbená	k1gFnSc1	zaslíbená
Ježíši	Ježíš	k1gMnSc3	Ježíš
Kristu	Krista	k1gFnSc4	Krista
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
o	o	k7c4	o
křesťanské	křesťanský	k2eAgNnSc4d1	křesťanské
učení	učení	k1gNnSc4	učení
zajímat	zajímat	k5eAaImF	zajímat
a	a	k8xC	a
dokáže	dokázat	k5eAaPmIp3nS	dokázat
Lygii	Lygie	k1gFnSc4	Lygie
chápat	chápat	k5eAaImF	chápat
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
nechá	nechat	k5eAaPmIp3nS	nechat
znuděný	znuděný	k2eAgMnSc1d1	znuděný
císař	císař	k1gMnSc1	císař
Nero	Nero	k1gMnSc1	Nero
zapálit	zapálit	k5eAaPmF	zapálit
Řím	Řím	k1gInSc4	Řím
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dostal	dostat	k5eAaPmAgMnS	dostat
inspiraci	inspirace	k1gFnSc4	inspirace
pro	pro	k7c4	pro
dokonalou	dokonalý	k2eAgFnSc4d1	dokonalá
báseň	báseň	k1gFnSc4	báseň
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
chce	chtít	k5eAaImIp3nS	chtít
opěvovat	opěvovat	k5eAaImF	opěvovat
hořící	hořící	k2eAgFnSc4d1	hořící
Tróju	Trója	k1gFnSc4	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
Vinicius	Vinicius	k1gInSc1	Vinicius
<g/>
,	,	kIx,	,
spěchá	spěchat	k5eAaImIp3nS	spěchat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hledá	hledat	k5eAaImIp3nS	hledat
Lygii	Lygie	k1gFnSc4	Lygie
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezpečí	bezpečí	k1gNnSc6	bezpečí
a	a	k8xC	a
Vinicius	Vinicius	k1gInSc1	Vinicius
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
pokřtít	pokřtít	k5eAaPmF	pokřtít
<g/>
.	.	kIx.	.
</s>
<s>
Nero	Nero	k1gMnSc1	Nero
se	se	k3xPyFc4	se
zalekne	zaleknout	k5eAaPmIp3nS	zaleknout
nespokojenosti	nespokojenost	k1gFnSc2	nespokojenost
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
hledá	hledat	k5eAaImIp3nS	hledat
<g/>
,	,	kIx,	,
na	na	k7c4	na
koho	kdo	k3yRnSc4	kdo
by	by	kYmCp3nP	by
svedl	svést	k5eAaPmAgInS	svést
vinu	vina	k1gFnSc4	vina
za	za	k7c4	za
požár	požár	k1gInSc4	požár
<g/>
.	.	kIx.	.
</s>
<s>
Petroniův	Petroniův	k2eAgMnSc1d1	Petroniův
úhlavní	úhlavní	k2eAgMnSc1d1	úhlavní
nepřítel	nepřítel	k1gMnSc1	nepřítel
Tigellinus	Tigellinus	k1gMnSc1	Tigellinus
vnukne	vnuknout	k5eAaPmIp3nS	vnuknout
císaři	císař	k1gMnSc3	císař
nápad	nápad	k1gInSc1	nápad
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc4	ten
byli	být	k5eAaImAgMnP	být
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Petronius	Petronius	k1gInSc1	Petronius
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
obratnou	obratný	k2eAgFnSc7d1	obratná
řečí	řeč	k1gFnSc7	řeč
pokusí	pokusit	k5eAaPmIp3nS	pokusit
císaře	císař	k1gMnSc4	císař
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc4	ten
nedělal	dělat	k5eNaImAgMnS	dělat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prohraje	prohrát	k5eAaPmIp3nS	prohrát
a	a	k8xC	a
pochopí	pochopit	k5eAaPmIp3nS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
zatýkání	zatýkání	k1gNnSc1	zatýkání
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
prozrazuje	prozrazovat	k5eAaImIp3nS	prozrazovat
udavač	udavač	k1gMnSc1	udavač
Chilón	Chilón	k1gMnSc1	Chilón
<g/>
.	.	kIx.	.
</s>
<s>
Lygie	Lygie	k1gFnSc1	Lygie
je	být	k5eAaImIp3nS	být
také	také	k9	také
zatčena	zatčen	k2eAgFnSc1d1	zatčena
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ji	on	k3xPp3gFnSc4	on
nesnáší	snášet	k5eNaImIp3nS	snášet
Neronova	Neronův	k2eAgFnSc1d1	Neronova
žárlivá	žárlivý	k2eAgFnSc1d1	žárlivá
manželka	manželka	k1gFnSc1	manželka
Poppaea	Poppaea	k1gFnSc1	Poppaea
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
včetně	včetně	k7c2	včetně
dětí	dítě	k1gFnPc2	dítě
jsou	být	k5eAaImIp3nP	být
za	za	k7c2	za
žhářství	žhářství	k1gNnSc2	žhářství
odsouzeni	odsouzet	k5eAaImNgMnP	odsouzet
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
upálením	upálení	k1gNnPc3	upálení
<g/>
.	.	kIx.	.
</s>
<s>
Chilón	Chilón	k1gMnSc1	Chilón
se	se	k3xPyFc4	se
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
hořící	hořící	k2eAgFnPc4d1	hořící
oběti	oběť	k1gFnPc4	oběť
na	na	k7c6	na
kříži	kříž	k1gInSc6	kříž
obrátí	obrátit	k5eAaPmIp3nP	obrátit
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
,	,	kIx,	,
urazí	urazit	k5eAaPmIp3nS	urazit
císaře	císař	k1gMnSc4	císař
Nerona	Nero	k1gMnSc4	Nero
a	a	k8xC	a
prozradí	prozradit	k5eAaPmIp3nS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
nechal	nechat	k5eAaPmAgInS	nechat
zapálit	zapálit	k5eAaPmF	zapálit
sám	sám	k3xTgMnSc1	sám
Nero	Nero	k1gMnSc1	Nero
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
křestané	křestaný	k2eAgFnSc3d1	křestaný
včetně	včetně	k7c2	včetně
Ursa	Ursum	k1gNnSc2	Ursum
a	a	k8xC	a
Lygie	Lygie	k1gFnSc2	Lygie
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
popraveni	popraven	k2eAgMnPc1d1	popraven
v	v	k7c6	v
aréně	aréna	k1gFnSc6	aréna
<g/>
.	.	kIx.	.
</s>
<s>
Ursus	Ursus	k1gInSc1	Ursus
přitom	přitom	k6eAd1	přitom
vysvobodí	vysvobodit	k5eAaPmIp3nS	vysvobodit
Lygii	Lygie	k1gFnSc4	Lygie
uvázanou	uvázaný	k2eAgFnSc4d1	uvázaná
na	na	k7c4	na
rohy	roh	k1gInPc4	roh
pratura	pratur	k1gMnSc2	pratur
<g/>
,	,	kIx,	,
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
miláčkem	miláček	k1gMnSc7	miláček
davu	dav	k1gInSc2	dav
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
je	být	k5eAaImIp3nS	být
donucen	donutit	k5eAaPmNgMnS	donutit
dát	dát	k5eAaPmF	dát
jim	on	k3xPp3gMnPc3	on
milost	milost	k1gFnSc4	milost
<g/>
.	.	kIx.	.
</s>
<s>
Lygie	Lygie	k1gFnSc1	Lygie
a	a	k8xC	a
Vinicius	Vinicius	k1gInSc1	Vinicius
odjíždějí	odjíždět	k5eAaImIp3nP	odjíždět
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Petronius	Petronius	k1gMnSc1	Petronius
je	být	k5eAaImIp3nS	být
donucen	donutit	k5eAaPmNgMnS	donutit
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
venkovského	venkovský	k2eAgNnSc2d1	venkovské
sídla	sídlo	k1gNnSc2	sídlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
počkat	počkat	k5eAaPmF	počkat
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
rozkazy	rozkaz	k1gInPc4	rozkaz
zatímco	zatímco	k8xS	zatímco
Nero	Nero	k1gMnSc1	Nero
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Petronius	Petronius	k1gMnSc1	Petronius
pochopí	pochopit	k5eAaPmIp3nS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
rozsudek	rozsudek	k1gInSc4	rozsudek
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
uspořádá	uspořádat	k5eAaPmIp3nS	uspořádat
hostinu	hostina	k1gFnSc4	hostina
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
nepokrytě	pokrytě	k6eNd1	pokrytě
všichni	všechen	k3xTgMnPc1	všechen
baví	bavit	k5eAaImIp3nP	bavit
<g/>
,	,	kIx,	,
přečte	přečíst	k5eAaPmIp3nS	přečíst
jim	on	k3xPp3gMnPc3	on
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
poslal	poslat	k5eAaPmAgMnS	poslat
Neronovi	Nero	k1gMnSc3	Nero
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
píše	psát	k5eAaImIp3nS	psát
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
si	se	k3xPyFc3	se
všichni	všechen	k3xTgMnPc1	všechen
o	o	k7c6	o
Neronovi	Nero	k1gMnSc6	Nero
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
spáchá	spáchat	k5eAaPmIp3nS	spáchat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
milenkou	milenka	k1gFnSc7	milenka
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
povstání	povstání	k1gNnSc4	povstání
spojené	spojený	k2eAgNnSc4d1	spojené
se	s	k7c7	s
státním	státní	k2eAgInSc7d1	státní
převratem	převrat	k1gInSc7	převrat
<g/>
,	,	kIx,	,
Nero	Nero	k1gMnSc1	Nero
je	být	k5eAaImIp3nS	být
sesazen	sesadit	k5eAaPmNgMnS	sesadit
z	z	k7c2	z
trůnu	trůn	k1gInSc2	trůn
a	a	k8xC	a
hrozí	hrozit	k5eAaImIp3nS	hrozit
mu	on	k3xPp3gNnSc3	on
zatčení	zatčení	k1gNnSc3	zatčení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
útěku	útěk	k1gInSc6	útěk
před	před	k7c7	před
povstalci	povstalec	k1gMnPc7	povstalec
nakonec	nakonec	k6eAd1	nakonec
sám	sám	k3xTgMnSc1	sám
spáchá	spáchat	k5eAaPmIp3nS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
několikrát	několikrát	k6eAd1	několikrát
zfilmován	zfilmovat	k5eAaPmNgInS	zfilmovat
<g/>
:	:	kIx,	:
Quo	Quo	k1gMnSc1	Quo
Vadis	Vadis	k1gFnSc2	Vadis
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Enrico	Enrico	k6eAd1	Enrico
Guazzoni	Guazzon	k1gMnPc1	Guazzon
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
Quo	Quo	k1gFnSc2	Quo
Vadis	Vadis	k1gFnSc2	Vadis
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Gabriellino	Gabriellin	k2eAgNnSc1d1	Gabriellin
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Annunzio	Annunzio	k1gMnSc1	Annunzio
a	a	k8xC	a
Georg	Georg	k1gMnSc1	Georg
Jacoby	Jacoba	k1gFnSc2	Jacoba
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
Quo	Quo	k1gFnSc2	Quo
Vadis	Vadis	k1gFnSc2	Vadis
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Mervyn	Mervyn	k1gNnSc1	Mervyn
LeRoy	LeRoa	k1gMnSc2	LeRoa
Quo	Quo	k1gFnSc1	Quo
Vadis	Vadis	k1gFnSc1	Vadis
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Franco	Franco	k6eAd1	Franco
Rossi	Rosse	k1gFnSc4	Rosse
Quo	Quo	k1gFnSc2	Quo
Vadis	Vadis	k1gFnSc2	Vadis
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jerzy	Jerza	k1gFnSc2	Jerza
Kawalerowicz	Kawalerowicza	k1gFnPc2	Kawalerowicza
Quo	Quo	k1gFnSc7	Quo
vadis	vadis	k1gFnPc2	vadis
<g/>
,	,	kIx,	,
vydala	vydat	k5eAaPmAgFnS	vydat
Audiotéka	Audiotéka	k1gFnSc1	Audiotéka
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
v	v	k7c4	v
edici	edice	k1gFnSc4	edice
Mistři	mistr	k1gMnPc1	mistr
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Edice	edice	k1gFnSc1	edice
Mistři	mistr	k1gMnPc1	mistr
slova	slovo	k1gNnSc2	slovo
je	být	k5eAaImIp3nS	být
plná	plný	k2eAgFnSc1d1	plná
literárních	literární	k2eAgInPc2d1	literární
pokladů	poklad	k1gInPc2	poklad
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gInPc1	náš
nejlepší	dobrý	k2eAgInPc1d3	nejlepší
herecké	herecký	k2eAgInPc1d1	herecký
hlasy	hlas	k1gInPc1	hlas
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
nám	my	k3xPp1nPc3	my
tak	tak	k6eAd1	tak
blízké	blízký	k2eAgFnPc1d1	blízká
<g/>
,	,	kIx,	,
namlouvají	namlouvat	k5eAaImIp3nP	namlouvat
své	svůj	k3xOyFgFnPc4	svůj
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
knihy	kniha	k1gFnPc4	kniha
a	a	k8xC	a
významná	významný	k2eAgNnPc4d1	významné
literární	literární	k2eAgNnPc4d1	literární
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
je	být	k5eAaImIp3nS	být
oslovila	oslovit	k5eAaPmAgFnS	oslovit
<g/>
,	,	kIx,	,
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
a	a	k8xC	a
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
třeba	třeba	k6eAd1	třeba
i	i	k9	i
rádi	rád	k2eAgMnPc1d1	rád
vracejí	vracet	k5eAaImIp3nP	vracet
<g/>
.	.	kIx.	.
</s>
