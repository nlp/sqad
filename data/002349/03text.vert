<s>
Flegmóna	flegmóna	k1gFnSc1	flegmóna
(	(	kIx(	(
<g/>
phlegmona	phlegmona	k1gFnSc1	phlegmona
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
neohraničený	ohraničený	k2eNgInSc4d1	neohraničený
bakteriální	bakteriální	k2eAgInSc4d1	bakteriální
zánět	zánět	k1gInSc4	zánět
šířící	šířící	k2eAgInSc4d1	šířící
se	se	k3xPyFc4	se
měkkými	měkký	k2eAgFnPc7d1	měkká
tkáněmi	tkáň	k1gFnPc7	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
hodin	hodina	k1gFnPc2	hodina
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
rozšířit	rozšířit	k5eAaPmF	rozšířit
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
končetiny	končetina	k1gFnSc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Končetina	končetina	k1gFnSc1	končetina
oteče	otéct	k5eAaPmIp3nS	otéct
<g/>
,	,	kIx,	,
zrudne	zrudnout	k5eAaPmIp3nS	zrudnout
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
znatelně	znatelně	k6eAd1	znatelně
teplejší	teplý	k2eAgInSc1d2	teplejší
než	než	k8xS	než
zbylé	zbylý	k2eAgFnPc1d1	zbylá
končetiny	končetina	k1gFnPc1	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
horečka	horečka	k1gFnSc1	horečka
<g/>
.	.	kIx.	.
</s>
<s>
Flegmóna	flegmóna	k1gFnSc1	flegmóna
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
léčbu	léčba	k1gFnSc4	léčba
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
i	i	k9	i
chirurgické	chirurgický	k2eAgNnSc4d1	chirurgické
ošetření	ošetření	k1gNnSc4	ošetření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
léčba	léčba	k1gFnSc1	léčba
zanedbá	zanedbat	k5eAaPmIp3nS	zanedbat
či	či	k8xC	či
přijde	přijít	k5eAaPmIp3nS	přijít
pozdě	pozdě	k6eAd1	pozdě
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
postižený	postižený	k2eAgMnSc1d1	postižený
o	o	k7c4	o
končetinu	končetina	k1gFnSc4	končetina
přijít	přijít	k5eAaPmF	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Absces	absces	k1gInSc4	absces
Infekce	infekce	k1gFnSc2	infekce
Panaricium	Panaricium	k1gNnSc4	Panaricium
</s>
