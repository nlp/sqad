<p>
<s>
Polština	polština	k1gFnSc1	polština
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
język	język	k6eAd1	język
polski	polski	k6eAd1	polski
<g/>
,	,	kIx,	,
polszczyzna	polszczyzna	k1gFnSc1	polszczyzna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přirozený	přirozený	k2eAgInSc4d1	přirozený
jazyk	jazyk	k1gInSc4	jazyk
patřící	patřící	k2eAgInSc4d1	patřící
mezi	mezi	k7c4	mezi
západoslovanské	západoslovanský	k2eAgInPc4d1	západoslovanský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
čeština	čeština	k1gFnSc1	čeština
<g/>
,	,	kIx,	,
slovenština	slovenština	k1gFnSc1	slovenština
<g/>
,	,	kIx,	,
kašubština	kašubština	k1gFnSc1	kašubština
a	a	k8xC	a
lužická	lužický	k2eAgFnSc1d1	Lužická
srbština	srbština	k1gFnSc1	srbština
<g/>
.	.	kIx.	.
</s>
<s>
Náleží	náležet	k5eAaImIp3nS	náležet
tedy	tedy	k9	tedy
mezi	mezi	k7c4	mezi
slovanské	slovanský	k2eAgInPc4d1	slovanský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
jazyků	jazyk	k1gInPc2	jazyk
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgFnP	být
čeština	čeština	k1gFnSc1	čeština
a	a	k8xC	a
polština	polština	k1gFnSc1	polština
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
stále	stále	k6eAd1	stále
jeden	jeden	k4xCgInSc1	jeden
jazyk	jazyk	k1gInSc1	jazyk
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
rozcházet	rozcházet	k5eAaImF	rozcházet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k9	ještě
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
si	se	k3xPyFc3	se
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
Poláci	Polák	k1gMnPc1	Polák
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
rozuměli	rozumět	k5eAaImAgMnP	rozumět
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Řada	řada	k1gFnSc1	řada
církevních	církevní	k2eAgNnPc2d1	církevní
slov	slovo	k1gNnPc2	slovo
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
češtině	čeština	k1gFnSc3	čeština
i	i	k8xC	i
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
Poláci	Polák	k1gMnPc1	Polák
přejali	přejmout	k5eAaPmAgMnP	přejmout
křesťanství	křesťanství	k1gNnSc4	křesťanství
od	od	k7c2	od
Čechů	Čech	k1gMnPc2	Čech
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnSc2	staletí
polština	polština	k1gFnSc1	polština
přejala	přejmout	k5eAaPmAgFnS	přejmout
mnoho	mnoho	k4c4	mnoho
slov	slovo	k1gNnPc2	slovo
cizího	cizí	k2eAgInSc2d1	cizí
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
,	,	kIx,	,
němčiny	němčina	k1gFnSc2	němčina
<g/>
,	,	kIx,	,
češtiny	čeština	k1gFnSc2	čeština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
ale	ale	k8xC	ale
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
výraznějším	výrazný	k2eAgInPc3d2	výraznější
puristickým	puristický	k2eAgInPc3d1	puristický
zásahům	zásah	k1gInPc3	zásah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
[	[	kIx(	[
<g/>
kým	kdo	k3yInSc7	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
že	že	k8xS	že
polský	polský	k2eAgInSc1d1	polský
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
mateřským	mateřský	k2eAgInSc7d1	mateřský
jazykem	jazyk	k1gInSc7	jazyk
asi	asi	k9	asi
44	[number]	k4	44
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Polsky	polsky	k6eAd1	polsky
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
územích	území	k1gNnPc6	území
sousedních	sousední	k2eAgMnPc2d1	sousední
států	stát	k1gInPc2	stát
s	s	k7c7	s
polskou	polský	k2eAgFnSc7d1	polská
menšinou	menšina	k1gFnSc7	menšina
–	–	k?	–
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
Těšínsko	Těšínsko	k1gNnSc1	Těšínsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
mnoha	mnoho	k4c2	mnoho
vystěhovaleckých	vystěhovalecký	k2eAgFnPc2d1	vystěhovalecká
vln	vlna	k1gFnPc2	vlna
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
letech	léto	k1gNnPc6	léto
existují	existovat	k5eAaImIp3nP	existovat
polsky	polsky	k6eAd1	polsky
hovořící	hovořící	k2eAgFnPc1d1	hovořící
menšiny	menšina	k1gFnPc1	menšina
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
členství	členství	k1gNnSc3	členství
Polska	Polsko	k1gNnSc2	Polsko
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
unijních	unijní	k2eAgInPc2d1	unijní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polština	polština	k1gFnSc1	polština
je	být	k5eAaImIp3nS	být
blízká	blízký	k2eAgFnSc1d1	blízká
češtině	čeština	k1gFnSc3	čeština
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
obě	dva	k4xCgFnPc1	dva
lužické	lužický	k2eAgFnPc1d1	Lužická
srbštiny	srbština	k1gFnPc1	srbština
nebo	nebo	k8xC	nebo
kašubština	kašubština	k1gFnSc1	kašubština
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
méně	málo	k6eAd2	málo
než	než	k8xS	než
slovenština	slovenština	k1gFnSc1	slovenština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejblíže	blízce	k6eAd3	blízce
polštině	polština	k1gFnSc3	polština
je	být	k5eAaImIp3nS	být
však	však	k9	však
nářečí	nářečí	k1gNnSc1	nářečí
"	"	kIx"	"
<g/>
po	po	k7c6	po
naszymu	naszym	k1gInSc6	naszym
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
staropolštiny	staropolština	k1gFnSc2	staropolština
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
živý	živý	k2eAgInSc4d1	živý
jazyk	jazyk	k1gInSc4	jazyk
používaný	používaný	k2eAgInSc4d1	používaný
na	na	k7c6	na
východě	východ	k1gInSc6	východ
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
v	v	k7c6	v
regionu	region	k1gInSc6	region
Těšínské	Těšínské	k2eAgNnSc4d1	Těšínské
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Neznalými	znalý	k2eNgMnPc7d1	neznalý
turisty	turist	k1gMnPc7	turist
Těšínska	Těšínsko	k1gNnSc2	Těšínsko
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc4	tento
nářečí	nářečí	k1gNnSc4	nářečí
pro	pro	k7c4	pro
nápadnou	nápadný	k2eAgFnSc4d1	nápadná
podobu	podoba	k1gFnSc4	podoba
polštině	polština	k1gFnSc3	polština
často	často	k6eAd1	často
mylně	mylně	k6eAd1	mylně
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
samotnou	samotný	k2eAgFnSc4d1	samotná
polštinu	polština	k1gFnSc4	polština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přízvuk	přízvuk	k1gInSc4	přízvuk
==	==	k?	==
</s>
</p>
<p>
<s>
Přízvuk	přízvuk	k1gInSc1	přízvuk
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
italštině	italština	k1gFnSc6	italština
<g/>
)	)	kIx)	)
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
předposlední	předposlední	k2eAgFnSc6d1	předposlední
slabice	slabika	k1gFnSc6	slabika
<g/>
,	,	kIx,	,
penultimě	penultima	k1gFnSc3	penultima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pečlivé	pečlivý	k2eAgFnSc6d1	pečlivá
výslovnosti	výslovnost	k1gFnSc6	výslovnost
přitom	přitom	k6eAd1	přitom
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
mírnému	mírný	k2eAgNnSc3d1	mírné
prodloužení	prodloužení	k1gNnSc3	prodloužení
samohlásky	samohláska	k1gFnSc2	samohláska
na	na	k7c4	na
polodlouhou	polodlouhý	k2eAgFnSc4d1	polodlouhá
(	(	kIx(	(
<g/>
ne	ne	k9	ne
na	na	k7c4	na
českou	český	k2eAgFnSc4d1	Česká
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kwadratowy	kwadratowa	k1gFnPc1	kwadratowa
–	–	k?	–
kfadrato	kfadrato	k6eAd1	kfadrato
<g/>
'	'	kIx"	'
<g/>
vy	vy	k3xPp2nPc1	vy
(	(	kIx(	(
<g/>
čtvercový	čtvercový	k2eAgInSc4d1	čtvercový
<g/>
,	,	kIx,	,
čtverečný	čtverečný	k2eAgInSc4d1	čtverečný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přízvuk	přízvuk	k1gInSc1	přízvuk
se	se	k3xPyFc4	se
nijak	nijak	k6eAd1	nijak
nevyznačuje	vyznačovat	k5eNaImIp3nS	vyznačovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlásky	hláska	k1gFnPc1	hláska
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
znaky	znak	k1gInPc1	znak
==	==	k?	==
</s>
</p>
<p>
<s>
Pravopis	pravopis	k1gInSc1	pravopis
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
český	český	k2eAgMnSc1d1	český
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
plně	plně	k6eAd1	plně
fonetický	fonetický	k2eAgInSc1d1	fonetický
s	s	k7c7	s
několika	několik	k4yIc7	několik
výjimkami	výjimka	k1gFnPc7	výjimka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Abeceda	abeceda	k1gFnSc1	abeceda
===	===	k?	===
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
uvedená	uvedený	k2eAgFnSc1d1	uvedená
abeceda	abeceda	k1gFnSc1	abeceda
je	být	k5eAaImIp3nS	být
srovnána	srovnat	k5eAaPmNgFnS	srovnat
podle	podle	k7c2	podle
standardního	standardní	k2eAgNnSc2d1	standardní
abecedního	abecední	k2eAgNnSc2d1	abecední
řazení	řazení	k1gNnSc2	řazení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polská	polský	k2eAgFnSc1d1	polská
abeceda	abeceda	k1gFnSc1	abeceda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
základní	základní	k2eAgFnSc4d1	základní
latinskou	latinský	k2eAgFnSc4d1	Latinská
abecedu	abeceda	k1gFnSc4	abeceda
<g/>
,	,	kIx,	,
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
navíc	navíc	k6eAd1	navíc
ještě	ještě	k9	ještě
následující	následující	k2eAgInPc1d1	následující
grafémy	grafém	k1gInPc1	grafém
s	s	k7c7	s
4	[number]	k4	4
odlišujícími	odlišující	k2eAgFnPc7d1	odlišující
značkami	značka	k1gFnPc7	značka
<g/>
,	,	kIx,	,
s	s	k7c7	s
diakritikou	diakritika	k1gFnSc7	diakritika
<g/>
,	,	kIx,	,
vyjma	vyjma	k7c2	vyjma
ó	ó	k0	ó
<g/>
,	,	kIx,	,
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
od	od	k7c2	od
české	český	k2eAgFnSc2d1	Česká
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
s	s	k7c7	s
ocáskem	ocásek	k1gInSc7	ocásek
(	(	kIx(	(
<g/>
ogonek	ogonek	k1gInSc1	ogonek
<g/>
)	)	kIx)	)
ą	ą	k?	ą
<g/>
,	,	kIx,	,
ę	ę	k?	ę
</s>
</p>
<p>
<s>
s	s	k7c7	s
přeškrtnutím	přeškrtnutí	k1gNnSc7	přeškrtnutí
ł	ł	k?	ł
</s>
</p>
<p>
<s>
s	s	k7c7	s
čárkou	čárka	k1gFnSc7	čárka
(	(	kIx(	(
<g/>
kreska	kreska	k1gFnSc1	kreska
<g/>
)	)	kIx)	)
ć	ć	k?	ć
<g/>
,	,	kIx,	,
ś	ś	k?	ś
<g/>
,	,	kIx,	,
ź	ź	k?	ź
<g/>
,	,	kIx,	,
ń	ń	k?	ń
a	a	k8xC	a
ó	ó	k0	ó
</s>
</p>
<p>
<s>
s	s	k7c7	s
tečkou	tečka	k1gFnSc7	tečka
(	(	kIx(	(
<g/>
kropka	kropka	k1gFnSc1	kropka
<g/>
)	)	kIx)	)
żNaopak	żNaopak	k1gInSc1	żNaopak
nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
v	v	k7c6	v
<g/>
,	,	kIx,	,
q	q	k?	q
a	a	k8xC	a
x.	x.	k?	x.
V	v	k7c6	v
psané	psaný	k2eAgFnSc6d1	psaná
polštině	polština	k1gFnSc6	polština
se	se	k3xPyFc4	se
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
nových	nový	k2eAgNnPc2d1	nové
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
tato	tento	k3xDgNnPc1	tento
písmena	písmeno	k1gNnPc4	písmeno
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
w	w	k?	w
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kw	kw	kA	kw
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
ks	ks	kA	ks
<g/>
"	"	kIx"	"
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
neliší	lišit	k5eNaImIp3nP	lišit
od	od	k7c2	od
českého	český	k2eAgMnSc2d1	český
"	"	kIx"	"
<g/>
v	v	k7c6	v
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
q	q	k?	q
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
x	x	k?	x
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
slovnících	slovník	k1gInPc6	slovník
se	se	k3xPyFc4	se
případně	případně	k6eAd1	případně
řadí	řadit	k5eAaImIp3nS	řadit
podle	podle	k7c2	podle
obvyklého	obvyklý	k2eAgNnSc2d1	obvyklé
latinkového	latinkový	k2eAgNnSc2d1	latinkový
umístění	umístění	k1gNnSc2	umístění
<g/>
:	:	kIx,	:
...	...	k?	...
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
v	v	k7c6	v
<g/>
,	,	kIx,	,
w	w	k?	w
<g/>
,	,	kIx,	,
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
,	,	kIx,	,
...	...	k?	...
</s>
</p>
<p>
<s>
Nestandardně	standardně	k6eNd1	standardně
užívá	užívat	k5eAaImIp3nS	užívat
i	i	k9	i
grafická	grafický	k2eAgFnSc1d1	grafická
varianta	varianta	k1gFnSc1	varianta
vodorovně	vodorovně	k6eAd1	vodorovně
škrtnuté	škrtnutý	k2eAgInPc4d1	škrtnutý
z	z	k7c2	z
místo	místo	k1gNnSc4	místo
"	"	kIx"	"
<g/>
ż	ż	k?	ż
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Hláskování	hláskování	k1gNnSc2	hláskování
====	====	k?	====
</s>
</p>
<p>
<s>
Hláskuje	hláskovat	k5eAaImIp3nS	hláskovat
se	se	k3xPyFc4	se
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
jako	jako	k8xC	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedinými	jediný	k2eAgInPc7d1	jediný
rozdíly	rozdíl	k1gInPc7	rozdíl
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
<g/>
,	,	kIx,	,
w	w	k?	w
<g/>
,	,	kIx,	,
h	h	k?	h
<g/>
,	,	kIx,	,
ch	ch	k0	ch
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ch	Ch	kA	Ch
se	se	k3xPyFc4	se
hláskuje	hláskovat	k5eAaImIp3nS	hláskovat
jako	jako	k9	jako
[	[	kIx(	[
<g/>
ce-cha	ceha	k1gMnSc1	ce-cha
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
samotné	samotný	k2eAgInPc1d1	samotný
h	h	k?	h
jako	jako	k8xS	jako
[	[	kIx(	[
<g/>
cha	cha	k0	cha
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
v	v	k7c4	v
se	se	k3xPyFc4	se
hláskuje	hláskovat	k5eAaImIp3nS	hláskovat
[	[	kIx(	[
<g/>
fau	fau	k?	fau
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
němčiny	němčina	k1gFnSc2	němčina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
w	w	k?	w
jako	jako	k9	jako
[	[	kIx(	[
<g/>
vu	vu	k?	vu
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
češtině	čeština	k1gFnSc3	čeština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
dvojitém	dvojitý	k2eAgInSc6d1	dvojitý
vé	vé	k?	vé
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
"	"	kIx"	"
<g/>
vv	vv	k?	vv
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spřežky	spřežka	k1gFnSc2	spřežka
===	===	k?	===
</s>
</p>
<p>
<s>
Řada	řada	k1gFnSc1	řada
hlásek	hláska	k1gFnPc2	hláska
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
spřežkami	spřežka	k1gFnPc7	spřežka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
jednoduchými	jednoduchý	k2eAgNnPc7d1	jednoduché
<g/>
:	:	kIx,	:
ch	ch	k0	ch
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
"	"	kIx"	"
<g/>
h	h	k?	h
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cz	cz	k?	cz
<g/>
,	,	kIx,	,
sz	sz	k?	sz
<g/>
,	,	kIx,	,
rz	rz	k?	rz
<g/>
,	,	kIx,	,
dz	dz	k?	dz
<g/>
,	,	kIx,	,
dż	dż	k?	dż
a	a	k8xC	a
</s>
</p>
<p>
<s>
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
změkčených	změkčený	k2eAgFnPc2d1	změkčená
souhlásek	souhláska	k1gFnPc2	souhláska
před	před	k7c7	před
samohláskou	samohláska	k1gFnSc7	samohláska
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
)	)	kIx)	)
přípisem	přípis	k1gInSc7	přípis
"	"	kIx"	"
<g/>
i	i	k9	i
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
"	"	kIx"	"
<g/>
xi-	xi-	k?	xi-
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
<g/>
Žádná	žádný	k3yNgFnSc1	žádný
spřežka	spřežka	k1gFnSc1	spřežka
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
"	"	kIx"	"
<g/>
ch	ch	k0	ch
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
abecedním	abecední	k2eAgNnSc6d1	abecední
řazení	řazení	k1gNnSc6	řazení
status	status	k1gInSc4	status
samostatného	samostatný	k2eAgNnSc2d1	samostatné
písmene	písmeno	k1gNnSc2	písmeno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
dílčích	dílčí	k2eAgInPc2d1	dílčí
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
v	v	k7c6	v
polském	polský	k2eAgInSc6d1	polský
slovníku	slovník	k1gInSc6	slovník
"	"	kIx"	"
<g/>
chleb	chlebit	k5eAaImRp2nS	chlebit
<g/>
"	"	kIx"	"
za	za	k7c7	za
"	"	kIx"	"
<g/>
cesarz	cesarz	k1gMnSc1	cesarz
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
před	před	k7c7	před
"	"	kIx"	"
<g/>
cicho	cic	k1gMnSc2	cic
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polském	polský	k2eAgInSc6d1	polský
pravopisu	pravopis	k1gInSc6	pravopis
jsou	být	k5eAaImIp3nP	být
zachovány	zachovat	k5eAaPmNgInP	zachovat
některé	některý	k3yIgInPc1	některý
jevy	jev	k1gInPc1	jev
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
čeština	čeština	k1gFnSc1	čeština
opustila	opustit	k5eAaPmAgFnS	opustit
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
právě	právě	k9	právě
spřežky	spřežka	k1gFnPc1	spřežka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
čeština	čeština	k1gFnSc1	čeština
v	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
nahradila	nahradit	k5eAaPmAgFnS	nahradit
písmeny	písmeno	k1gNnPc7	písmeno
s	s	k7c7	s
diakritikou	diakritika	k1gFnSc7	diakritika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Samohlásky	samohláska	k1gFnSc2	samohláska
===	===	k?	===
</s>
</p>
<p>
<s>
Samohlásky	samohláska	k1gFnPc1	samohláska
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
stejné	stejný	k2eAgFnPc1d1	stejná
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
)	)	kIx)	)
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
i	i	k8xC	i
<g/>
/	/	kIx~	/
<g/>
y.	y.	k?	y.
</s>
</p>
<p>
<s>
Samohlásky	samohláska	k1gFnPc1	samohláska
i	i	k8xC	i
<g/>
/	/	kIx~	/
<g/>
y	y	k?	y
se	se	k3xPyFc4	se
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
zásadně	zásadně	k6eAd1	zásadně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
např.	např.	kA	např.
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
podle	podle	k7c2	podle
výslovnosti	výslovnost	k1gFnSc2	výslovnost
rozlišit	rozlišit	k5eAaPmF	rozlišit
być	być	k?	być
(	(	kIx(	(
<g/>
být	být	k5eAaImF	být
<g/>
)	)	kIx)	)
a	a	k8xC	a
bić	bić	k?	bić
(	(	kIx(	(
<g/>
bít	bít	k5eAaImF	bít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polštině	polština	k1gFnSc6	polština
neexistují	existovat	k5eNaImIp3nP	existovat
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
samohlásky	samohláska	k1gFnPc1	samohláska
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
samohlásky	samohláska	k1gFnPc1	samohláska
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
délku	délka	k1gFnSc4	délka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Písmeno	písmeno	k1gNnSc1	písmeno
ó	ó	k0	ó
====	====	k?	====
</s>
</p>
<p>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
ó	ó	k0	ó
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
jako	jako	k9	jako
samohláska	samohláska	k1gFnSc1	samohláska
u	u	k7c2	u
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
žádný	žádný	k3yNgInSc4	žádný
rozdíl	rozdíl	k1gInSc4	rozdíl
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
ó	ó	k0	ó
a	a	k8xC	a
u.	u.	k?	u.
Čárka	čárka	k1gFnSc1	čárka
neoznačuje	označovat	k5eNaImIp3nS	označovat
délku	délka	k1gFnSc4	délka
samohlásky	samohláska	k1gFnSc2	samohláska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k9	jako
přehláska	přehláska	k1gFnSc1	přehláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
ó	ó	k0	ó
často	často	k6eAd1	často
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
českému	český	k2eAgMnSc3d1	český
ů	ů	k?	ů
a	a	k8xC	a
při	při	k7c6	při
ohýbání	ohýbání	k1gNnSc6	ohýbání
slov	slovo	k1gNnPc2	slovo
skutečně	skutečně	k6eAd1	skutečně
střídá	střídat	k5eAaImIp3nS	střídat
s	s	k7c7	s
o	o	k7c4	o
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
:	:	kIx,	:
wóz	wóz	k?	wóz
[	[	kIx(	[
<g/>
vuz	vuz	k?	vuz
<g/>
]	]	kIx)	]
/	/	kIx~	/
do	do	k7c2	do
wozu	wozus	k1gInSc2	wozus
[	[	kIx(	[
<g/>
do	do	k7c2	do
vozu	vůz	k1gInSc2	vůz
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nezvykle	zvykle	k6eNd1	zvykle
a	a	k8xC	a
neintuitivně	intuitivně	k6eNd1	intuitivně
však	však	k9	však
ó	ó	k0	ó
může	moct	k5eAaImIp3nS	moct
znít	znít	k5eAaImF	znít
zejména	zejména	k9	zejména
při	při	k7c6	při
zdánlivé	zdánlivý	k2eAgFnSc6d1	zdánlivá
shodě	shoda	k1gFnSc6	shoda
českých	český	k2eAgInPc2d1	český
výrazů	výraz	k1gInPc2	výraz
nebo	nebo	k8xC	nebo
názvů	název	k1gInPc2	název
s	s	k7c7	s
polskými	polský	k2eAgMnPc7d1	polský
<g/>
.	.	kIx.	.
</s>
<s>
Ku	k	k7c3	k
příkladu	příklad	k1gInSc3	příklad
názvy	název	k1gInPc7	název
obcí	obec	k1gFnPc2	obec
končící	končící	k2eAgFnPc1d1	končící
"	"	kIx"	"
<g/>
-ów	-ów	k?	-ów
<g/>
"	"	kIx"	"
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
tvarech	tvar	k1gInPc6	tvar
mají	mít	k5eAaImIp3nP	mít
základ	základ	k1gInSc4	základ
na	na	k7c4	na
"	"	kIx"	"
<g/>
-ow	w	k?	-ow
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Krakov	Krakov	k1gInSc1	Krakov
je	být	k5eAaImIp3nS	být
polsky	polsky	k6eAd1	polsky
Kraków	Kraków	k1gFnSc1	Kraków
[	[	kIx(	[
<g/>
krakuf	krakuf	k1gInSc1	krakuf
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
např.	např.	kA	např.
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
pádu	pád	k1gInSc2	pád
je	být	k5eAaImIp3nS	být
hlásková	hláskový	k2eAgFnSc1d1	hlásková
podoba	podoba	k1gFnSc1	podoba
shodná	shodný	k2eAgFnSc1d1	shodná
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
-	-	kIx~	-
Krakowie	Krakowie	k1gFnSc1	Krakowie
[	[	kIx(	[
<g/>
Krakově	krakův	k2eAgFnSc3d1	Krakova
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Nosové	nosový	k2eAgFnPc1d1	nosová
samohlásky	samohláska	k1gFnPc1	samohláska
ę	ę	k?	ę
a	a	k8xC	a
ą	ą	k?	ą
====	====	k?	====
</s>
</p>
<p>
<s>
Nosovost	nosovost	k1gFnSc1	nosovost
samohlásky	samohláska	k1gFnSc2	samohláska
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
nosové	nosový	k2eAgNnSc1d1	nosové
zakončení	zakončení	k1gNnSc1	zakončení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
spodním	spodní	k2eAgInSc7d1	spodní
ocáskem	ocásek	k1gInSc7	ocásek
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
a	a	k8xC	a
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
znakových	znakový	k2eAgFnPc6d1	znaková
sadách	sada	k1gFnPc6	sada
"	"	kIx"	"
<g/>
ogonek	ogonek	k1gInSc4	ogonek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
k	k	k7c3	k
základnímu	základní	k2eAgNnSc3d1	základní
písmenu	písmeno	k1gNnSc3	písmeno
"	"	kIx"	"
<g/>
e	e	k0	e
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
a	a	k8xC	a
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
hlásky	hláska	k1gFnSc2	hláska
"	"	kIx"	"
<g/>
ą	ą	k?	ą
<g/>
"	"	kIx"	"
foneticky	foneticky	k6eAd1	foneticky
(	(	kIx(	(
<g/>
výslovností	výslovnost	k1gFnSc7	výslovnost
i	i	k8xC	i
původem	původ	k1gInSc7	původ
<g/>
)	)	kIx)	)
jde	jít	k5eAaImIp3nS	jít
ovšem	ovšem	k9	ovšem
o	o	k7c4	o
hlásku	hláska	k1gFnSc4	hláska
"	"	kIx"	"
<g/>
o	o	k7c4	o
<g/>
"	"	kIx"	"
s	s	k7c7	s
nosovkou	nosovka	k1gFnSc7	nosovka
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
o	o	k7c4	o
hlásku	hláska	k1gFnSc4	hláska
"	"	kIx"	"
<g/>
a	a	k8xC	a
<g/>
"	"	kIx"	"
s	s	k7c7	s
nosovým	nosový	k2eAgInSc7d1	nosový
koncem	konec	k1gInSc7	konec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samohláska	samohláska	k1gFnSc1	samohláska
psaná	psaný	k2eAgFnSc1d1	psaná
ę	ę	k?	ę
</s>
</p>
<p>
<s>
Samohlásku	samohláska	k1gFnSc4	samohláska
ę	ę	k?	ę
vyslovujeme	vyslovovat	k5eAaImIp1nP	vyslovovat
</s>
</p>
<p>
<s>
před	před	k7c7	před
s	s	k7c7	s
<g/>
,	,	kIx,	,
z	z	k0	z
<g/>
,	,	kIx,	,
sz	sz	k?	sz
a	a	k8xC	a
ż	ż	k?	ż
nosově	nosově	k6eAd1	nosově
[	[	kIx(	[
<g/>
e	e	k0	e
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
[	[	kIx(	[
<g/>
eu	eu	k?	eu
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
klęska	klęsek	k1gMnSc2	klęsek
<g/>
,	,	kIx,	,
węży	węża	k1gMnSc2	węża
</s>
</p>
<p>
<s>
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
a	a	k8xC	a
před	před	k7c7	před
l	l	kA	l
nebo	nebo	k8xC	nebo
ł	ł	k?	ł
jako	jako	k8xS	jako
[	[	kIx(	[
<g/>
e	e	k0	e
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
např.	např.	kA	např.
widzę	widzę	k?	widzę
tę	tę	k?	tę
ulicę	ulicę	k?	ulicę
<g/>
,	,	kIx,	,
wzięła	wzięłus	k1gMnSc4	wzięłus
<g/>
,	,	kIx,	,
zdjęli	zdjęle	k1gFnSc4	zdjęle
</s>
</p>
<p>
<s>
před	před	k7c7	před
b	b	k?	b
nebo	nebo	k8xC	nebo
p	p	k?	p
jako	jako	k8xS	jako
slabičné	slabičný	k2eAgFnPc1d1	slabičná
[	[	kIx(	[
<g/>
em	em	k?	em
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zęby	zęba	k1gFnPc1	zęba
<g/>
,	,	kIx,	,
dęby	dęby	k1gInPc1	dęby
<g/>
,	,	kIx,	,
następny	następen	k2eAgInPc1d1	następen
</s>
</p>
<p>
<s>
před	před	k7c7	před
k	k	k7c3	k
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
,	,	kIx,	,
dz	dz	k?	dz
a	a	k8xC	a
cz	cz	k?	cz
jako	jako	k8xS	jako
slabičné	slabičný	k2eAgFnPc1d1	slabičná
[	[	kIx(	[
<g/>
en	en	k?	en
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ręka	ręk	k1gInSc2	ręk
<g/>
,	,	kIx,	,
piętro	piętro	k6eAd1	piętro
</s>
</p>
<p>
<s>
před	před	k7c7	před
ć	ć	k?	ć
a	a	k8xC	a
dź	dź	k?	dź
jako	jako	k8xC	jako
slabičné	slabičný	k2eAgFnPc1d1	slabičná
[	[	kIx(	[
<g/>
eň	eň	k?	eň
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
se	s	k7c7	s
změkčením	změkčení	k1gNnSc7	změkčení
z	z	k7c2	z
následující	následující	k2eAgFnSc2d1	následující
měkké	měkký	k2eAgFnSc2d1	měkká
souhlásky	souhláska	k1gFnSc2	souhláska
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pięć	pięć	k?	pięć
<g/>
,	,	kIx,	,
będzie	będzie	k1gFnSc1	będzie
<g/>
,	,	kIx,	,
sędziaV	sędziaV	k?	sędziaV
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
tendence	tendence	k1gFnPc1	tendence
vyslovovat	vyslovovat	k5eAaImF	vyslovovat
hyperspisovně	hyperspisovně	k6eAd1	hyperspisovně
nosově	nosově	k6eAd1	nosově
samohlásku	samohláska	k1gFnSc4	samohláska
"	"	kIx"	"
<g/>
ę	ę	k?	ę
<g/>
"	"	kIx"	"
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
standardu	standard	k1gInSc2	standard
číst	číst	k5eAaImF	číst
nemá	mít	k5eNaImIp3nS	mít
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
się	się	k?	się
vyslovované	vyslovovaný	k2eAgFnSc2d1	vyslovovaná
jako	jako	k8xC	jako
[	[	kIx(	[
<g/>
śeu	śeu	k?	śeu
<g/>
]	]	kIx)	]
namísto	namísto	k7c2	namísto
[	[	kIx(	[
<g/>
śe	śe	k?	śe
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
přijatelné	přijatelný	k2eAgNnSc1d1	přijatelné
jen	jen	k9	jen
v	v	k7c6	v
jevištním	jevištní	k2eAgInSc6d1	jevištní
přednesu	přednes	k1gInSc6	přednes
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
u	u	k7c2	u
výslovnosti	výslovnost	k1gFnSc2	výslovnost
"	"	kIx"	"
<g/>
ł	ł	k?	ł
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samohláska	samohláska	k1gFnSc1	samohláska
psaná	psaný	k2eAgFnSc1d1	psaná
ą	ą	k?	ą
</s>
</p>
<p>
<s>
Samohlásku	samohláska	k1gFnSc4	samohláska
ą	ą	k?	ą
vyslovujeme	vyslovovat	k5eAaImIp1nP	vyslovovat
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
obdobných	obdobný	k2eAgFnPc2d1	obdobná
jako	jako	k9	jako
pro	pro	k7c4	pro
ę	ę	k?	ę
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
před	před	k7c7	před
ż	ż	k?	ż
<g/>
,	,	kIx,	,
sz	sz	k?	sz
a	a	k8xC	a
s	s	k7c7	s
nosově	nosově	k6eAd1	nosově
[	[	kIx(	[
<g/>
oņ	oņ	k?	oņ
<g/>
]	]	kIx)	]
nebo	nebo	k8xC	nebo
delší	dlouhý	k2eAgNnSc4d2	delší
[	[	kIx(	[
<g/>
ó	ó	k0	ó
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
wciąż	wciąż	k?	wciąż
<g/>
,	,	kIx,	,
wąska	wąska	k6eAd1	wąska
</s>
</p>
<p>
<s>
před	před	k7c7	před
l	l	kA	l
a	a	k8xC	a
ł	ł	k?	ł
jako	jako	k8xS	jako
[	[	kIx(	[
<g/>
o	o	k7c4	o
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
např.	např.	kA	např.
wziął	wziął	k?	wziął
<g/>
,	,	kIx,	,
zdjął	zdjął	k?	zdjął
</s>
</p>
<p>
<s>
před	před	k7c7	před
b	b	k?	b
a	a	k8xC	a
p	p	k?	p
jako	jako	k8xS	jako
slabičné	slabičný	k2eAgFnPc1d1	slabičná
[	[	kIx(	[
<g/>
om	om	k?	om
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
např.	např.	kA	např.
rąbać	rąbać	k?	rąbać
<g/>
,	,	kIx,	,
ząb	ząb	k?	ząb
<g/>
,	,	kIx,	,
stąpać	stąpać	k?	stąpać
</s>
</p>
<p>
<s>
před	před	k7c7	před
k	k	k7c3	k
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
,	,	kIx,	,
dz	dz	k?	dz
a	a	k8xC	a
cz	cz	k?	cz
jako	jako	k8xC	jako
slabičné	slabičný	k2eAgFnPc1d1	slabičná
[	[	kIx(	[
<g/>
on	on	k3xPp3gMnSc1	on
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pociąg	pociąg	k1gInSc1	pociąg
<g/>
,	,	kIx,	,
gorący	gorący	k1gInPc1	gorący
</s>
</p>
<p>
<s>
před	před	k7c7	před
ć	ć	k?	ć
nebo	nebo	k8xC	nebo
dź	dź	k?	dź
jako	jako	k8xS	jako
slabičné	slabičný	k2eAgFnPc1d1	slabičná
[	[	kIx(	[
<g/>
oň	oň	k?	oň
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
změkčení	změkčení	k1gNnSc1	změkčení
zpětně	zpětně	k6eAd1	zpětně
přebíráno	přebírat	k5eAaImNgNnS	přebírat
z	z	k7c2	z
následující	následující	k2eAgFnSc2d1	následující
měkké	měkký	k2eAgFnSc2d1	měkká
souhlásky	souhláska	k1gFnSc2	souhláska
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zacząć	zacząć	k?	zacząć
</s>
</p>
<p>
<s>
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
odlišné	odlišný	k2eAgNnSc4d1	odlišné
pravidlo	pravidlo	k1gNnSc4	pravidlo
oproti	oproti	k7c3	oproti
"	"	kIx"	"
<g/>
ę	ę	k?	ę
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
kde	kde	k6eAd1	kde
standard	standard	k1gInSc1	standard
připouští	připouštět	k5eAaImIp3nS	připouštět
[	[	kIx(	[
<g/>
ou	ou	k0	ou
<g/>
]	]	kIx)	]
anebo	anebo	k8xC	anebo
nosové	nosový	k2eAgNnSc1d1	nosové
[	[	kIx(	[
<g/>
oņ	oņ	k?	oņ
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
z	z	k7c2	z
mamą	mamą	k?	mamą
[	[	kIx(	[
<g/>
z	z	k7c2	z
mamou	mama	k1gFnSc7	mama
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
idą	idą	k?	idą
tą	tą	k?	tą
ulicą	ulicą	k?	ulicą
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hovorové	hovorový	k2eAgFnSc6d1	hovorová
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nP	různit
podle	podle	k7c2	podle
dialektu	dialekt	k1gInSc2	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
krajová	krajový	k2eAgFnSc1d1	krajová
je	být	k5eAaImIp3nS	být
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
om	om	k?	om
<g/>
]	]	kIx)	]
pro	pro	k7c4	pro
přímořská	přímořský	k2eAgNnPc4d1	přímořské
vojvodství	vojvodství	k1gNnPc4	vojvodství
<g/>
;	;	kIx,	;
starší	starý	k2eAgMnPc1d2	starší
lidé	člověk	k1gMnPc1	člověk
na	na	k7c6	na
Podlesí	Podlesí	k1gNnSc6	Podlesí
zase	zase	k9	zase
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
Moraváci	Moravák	k1gMnPc1	Moravák
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
samotné	samotný	k2eAgNnSc1d1	samotné
[	[	kIx(	[
<g/>
o	o	k0	o
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Souhlásky	souhláska	k1gFnSc2	souhláska
===	===	k?	===
</s>
</p>
<p>
<s>
Hlásky	hlásek	k1gInPc1	hlásek
h	h	k?	h
<g/>
/	/	kIx~	/
<g/>
ch	ch	k0	ch
se	se	k3xPyFc4	se
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
i	i	k9	i
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
školáci	školák	k1gMnPc1	školák
i	i	k8xC	i
dospělí	dospělí	k1gMnPc1	dospělí
často	často	k6eAd1	často
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
napsat	napsat	k5eAaBmF	napsat
<g/>
.	.	kIx.	.
</s>
<s>
Krajově	krajově	k6eAd1	krajově
lze	lze	k6eAd1	lze
zachytit	zachytit	k5eAaPmF	zachytit
výslovnost	výslovnost	k1gFnSc4	výslovnost
h	h	k?	h
zněle	zněle	k6eAd1	zněle
<g/>
,	,	kIx,	,
ch	ch	k0	ch
nezněle	zněle	k6eNd1	zněle
<g/>
.	.	kIx.	.
<g/>
Hlásky	hláska	k1gFnSc2	hláska
r	r	kA	r
a	a	k8xC	a
l	l	kA	l
nejsou	být	k5eNaImIp3nP	být
slabikotvorné	slabikotvorný	k2eAgFnPc1d1	slabikotvorný
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
třeba	třeba	k6eAd1	třeba
</s>
</p>
<p>
<s>
místo	místo	k7c2	místo
českého	český	k2eAgMnSc2d1	český
ví-tr	vír	k1gInSc4	ví-tr
Poláci	Polák	k1gMnPc1	Polák
říkají	říkat	k5eAaImIp3nP	říkat
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
slabice	slabika	k1gFnSc6	slabika
wiatr	wiatr	k1gInSc1	wiatr
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
dodávají	dodávat	k5eAaImIp3nP	dodávat
samohlásky	samohláska	k1gFnPc1	samohláska
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
polském	polský	k2eAgNnSc6d1	polské
podání	podání	k1gNnSc6	podání
zní	znět	k5eAaImIp3nS	znět
řeka	řeka	k1gFnSc1	řeka
Vl-ta-va	Vlaa	k1gFnSc1	Vl-ta-va
jako	jako	k8xS	jako
Weł-ta-wa	Wełaa	k1gFnSc1	Weł-ta-wa
<g/>
.	.	kIx.	.
<g/>
Polské	polský	k2eAgFnSc3d1	polská
ł	ł	k?	ł
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
souhláskové	souhláskový	k2eAgNnSc1d1	souhláskové
<g/>
,	,	kIx,	,
neslabičné	slabičný	k2eNgInPc1d1	slabičný
"	"	kIx"	"
<g/>
u	u	k7c2	u
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
anglické	anglický	k2eAgNnSc1d1	anglické
w	w	k?	w
<g/>
;	;	kIx,	;
jen	jen	k9	jen
archaicky	archaicky	k6eAd1	archaicky
<g/>
,	,	kIx,	,
v	v	k7c6	v
jevištní	jevištní	k2eAgFnSc6d1	jevištní
řeči	řeč	k1gFnSc6	řeč
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
historických	historický	k2eAgInPc6d1	historický
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
temné	temný	k2eAgInPc1d1	temný
l	l	kA	l
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
ławka	ławka	k1gFnSc1	ławka
–	–	k?	–
uafka	uafka	k1gFnSc1	uafka
(	(	kIx(	(
<g/>
lavice	lavice	k1gFnSc1	lavice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
łąka	łąk	k2eAgFnSc1d1	łąk
–	–	k?	–
uonka	uonka	k1gFnSc1	uonka
(	(	kIx(	(
<g/>
louka	louka	k1gFnSc1	louka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
l	l	kA	l
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
češtině	čeština	k1gFnSc3	čeština
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
malinko	malinko	k6eAd1	malinko
měkčeji	měkko	k6eAd2	měkko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výrazně	výrazně	k6eAd1	výrazně
měkčeji	měkko	k6eAd2	měkko
před	před	k7c4	před
i	i	k9	i
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
slovenské	slovenský	k2eAgFnSc2d1	slovenská
ľ.	ľ.	k?	ľ.
Omyl	omýt	k5eAaPmAgMnS	omýt
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
písmenku	písmenko	k1gNnSc6	písmenko
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
úplně	úplně	k6eAd1	úplně
jiné	jiný	k2eAgNnSc4d1	jiné
slovo	slovo	k1gNnSc4	slovo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
laska	laska	k1gMnSc1	laska
[	[	kIx(	[
<g/>
laska	laska	k1gMnSc1	laska
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
hůl	hůl	k1gFnSc1	hůl
<g/>
,	,	kIx,	,
prut	prut	k1gInSc1	prut
nebo	nebo	k8xC	nebo
lískovka	lískovka	k1gFnSc1	lískovka
<g/>
,	,	kIx,	,
přeneseně	přeneseně	k6eAd1	přeneseně
i	i	k9	i
šiška	šiška	k1gFnSc1	šiška
salámu	salám	k1gInSc2	salám
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
łaska	łaska	k1gFnSc1	łaska
[	[	kIx(	[
<g/>
uaska	uaska	k1gFnSc1	uaska
<g/>
]	]	kIx)	]
-	-	kIx~	-
milost	milost	k1gFnSc1	milost
(	(	kIx(	(
<g/>
královská	královský	k2eAgFnSc1d1	královská
<g/>
,	,	kIx,	,
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
laskavost	laskavost	k1gFnSc4	laskavost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Spodoba	spodoba	k1gFnSc1	spodoba
====	====	k?	====
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
spodobě	spodoba	k1gFnSc3	spodoba
(	(	kIx(	(
<g/>
přizpůsobení	přizpůsobení	k1gNnSc2	přizpůsobení
<g/>
)	)	kIx)	)
znělosti	znělost	k1gFnSc2	znělost
podle	podle	k7c2	podle
následující	následující	k2eAgFnSc2d1	následující
hlásky	hláska	k1gFnSc2	hláska
<g/>
,	,	kIx,	,
např.	např.	kA	např.
wprost	wprost	k1gFnSc1	wprost
[	[	kIx(	[
<g/>
fprost	fprost	k1gInSc1	fprost
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
spodoba	spodoba	k1gFnSc1	spodoba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
podle	podle	k7c2	podle
předcházející	předcházející	k2eAgFnSc2d1	předcházející
hlásky	hláska	k1gFnSc2	hláska
<g/>
:	:	kIx,	:
trzy	trza	k1gFnSc2	trza
čteno	číst	k5eAaImNgNnS	číst
tšy	tšy	k?	tšy
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
obdobně	obdobně	k6eAd1	obdobně
neznělé	znělý	k2eNgFnPc4d1	neznělá
ř	ř	k?	ř
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
znělému	znělý	k2eAgMnSc3d1	znělý
rz	rz	k?	rz
[	[	kIx(	[
<g/>
ż	ż	k?	ż
<g/>
]	]	kIx)	]
v	v	k7c6	v
drzewo	drzewo	k6eAd1	drzewo
(	(	kIx(	(
<g/>
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
obdobně	obdobně	k6eAd1	obdobně
znělé	znělý	k2eAgInPc1d1	znělý
ř	ř	k?	ř
v	v	k7c4	v
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
twarz	twarz	k1gMnSc1	twarz
<g/>
,	,	kIx,	,
twój	twój	k1gMnSc1	twój
<g/>
,	,	kIx,	,
kwiecień	kwiecień	k?	kwiecień
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
[	[	kIx(	[
<g/>
tfaš	tfaš	k1gInSc1	tfaš
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
tfuj	tfuj	k0	tfuj
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
kfjećeň	kfjećeň	k1gFnSc1	kfjećeň
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Změkčování	změkčování	k1gNnPc1	změkčování
souhlásek	souhláska	k1gFnPc2	souhláska
čárkou	čárka	k1gFnSc7	čárka
a	a	k8xC	a
pomocí	pomoc	k1gFnSc7	pomoc
"	"	kIx"	"
<g/>
i	i	k9	i
<g/>
"	"	kIx"	"
====	====	k?	====
</s>
</p>
<p>
<s>
K	k	k7c3	k
dokonalé	dokonalý	k2eAgFnSc3d1	dokonalá
výslovnosti	výslovnost	k1gFnSc3	výslovnost
pak	pak	k6eAd1	pak
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
měkká	měkký	k2eAgFnSc1d1	měkká
výslovnost	výslovnost	k1gFnSc1	výslovnost
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
měkkost	měkkost	k1gFnSc1	měkkost
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
není	být	k5eNaImIp3nS	být
a	a	k8xC	a
po	po	k7c6	po
kterých	který	k3yQgMnPc6	který
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
následuje	následovat	k5eAaImIp3nS	následovat
i	i	k9	i
<g/>
,	,	kIx,	,
a	a	k8xC	a
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
si	se	k3xPyFc3	se
změkčení	změkčený	k2eAgMnPc1d1	změkčený
Češi	Čech	k1gMnPc1	Čech
často	často	k6eAd1	často
ani	ani	k9	ani
nevšimnou	všimnout	k5eNaPmIp3nP	všimnout
<g/>
.	.	kIx.	.
</s>
<s>
Měkké	měkký	k2eAgNnSc1d1	měkké
"	"	kIx"	"
<g/>
i	i	k9	i
<g/>
"	"	kIx"	"
po	po	k7c6	po
souhlásce	souhláska	k1gFnSc6	souhláska
má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
význam	význam	k1gInSc4	význam
změkčení	změkčení	k1gNnSc2	změkčení
předchozí	předchozí	k2eAgFnSc2d1	předchozí
souhlásky	souhláska	k1gFnSc2	souhláska
<g/>
,	,	kIx,	,
po	po	k7c4	po
které	který	k3yQgFnPc4	který
následuje	následovat	k5eAaImIp3nS	následovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
významově	významově	k6eAd1	významově
změkčující	změkčující	k2eAgNnSc1d1	změkčující
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
diakritické	diakritický	k2eAgNnSc1d1	diakritické
znaménko	znaménko	k1gNnSc1	znaménko
čárka	čárka	k1gFnSc1	čárka
nad	nad	k7c7	nad
souhláskou	souhláska	k1gFnSc7	souhláska
<g/>
,	,	kIx,	,
ne	ne	k9	ne
jako	jako	k9	jako
další	další	k2eAgFnSc1d1	další
slabika	slabika	k1gFnSc1	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
české	český	k2eAgMnPc4d1	český
čtenáře	čtenář	k1gMnPc4	čtenář
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
obtížné	obtížný	k2eAgNnSc1d1	obtížné
napodobit	napodobit	k5eAaPmF	napodobit
přesné	přesný	k2eAgNnSc4d1	přesné
znění	znění	k1gNnSc4	znění
změkčených	změkčený	k2eAgFnPc2d1	změkčená
hlásek	hláska	k1gFnPc2	hláska
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
p	p	k?	p
<g/>
,	,	kIx,	,
m	m	kA	m
<g/>
,	,	kIx,	,
k	k	k7c3	k
:	:	kIx,	:
kierowca	kierowca	k1gMnSc1	kierowca
<g/>
,	,	kIx,	,
giełda	giełda	k1gMnSc1	giełda
<g/>
,	,	kIx,	,
mi	já	k3xPp1nSc3	já
(	(	kIx(	(
<g/>
vysloveného	vyslovený	k2eAgNnSc2d1	vyslovené
měkce	měkko	k6eAd1	měkko
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
my	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
se	s	k7c7	s
stejnými	stejný	k2eAgInPc7d1	stejný
významy	význam	k1gInPc7	význam
jako	jako	k8xC	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lipiec	lipiec	k1gInSc1	lipiec
se	s	k7c7	s
změkčenými	změkčený	k2eAgFnPc7d1	změkčená
k	k	k7c3	k
<g/>
́	́	k?	́
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
́	́	k?	́
<g/>
,	,	kIx,	,
m	m	kA	m
<g/>
́	́	k?	́
<g/>
,	,	kIx,	,
l	l	kA	l
<g/>
́	́	k?	́
a	a	k8xC	a
p	p	k?	p
<g/>
́	́	k?	́
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
měkká	měkký	k2eAgFnSc1d1	měkká
výslovnost	výslovnost	k1gFnSc1	výslovnost
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
i	i	k9	i
po	po	k7c6	po
změkčujícím	změkčující	k2eAgInSc6d1	změkčující
k	k	k7c3	k
<g/>
,	,	kIx,	,
g	g	kA	g
následovaným	následovaný	k2eAgNnSc7d1	následované
"	"	kIx"	"
<g/>
e	e	k0	e
<g/>
"	"	kIx"	"
např.	např.	kA	např.
ve	v	k7c6	v
jménech	jméno	k1gNnPc6	jméno
s	s	k7c7	s
takovým	takový	k3xDgInSc7	takový
starým	starý	k2eAgInSc7d1	starý
pravopisem	pravopis	k1gInSc7	pravopis
(	(	kIx(	(
<g/>
Geremek	Geremek	k1gInSc1	Geremek
<g/>
,	,	kIx,	,
čteno	čten	k2eAgNnSc1d1	čteno
G	G	kA	G
<g/>
́	́	k?	́
<g/>
je-	je-	k?	je-
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
Gierek	Gierky	k1gFnPc2	Gierky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
po	po	k7c6	po
k	k	k7c3	k
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
,	,	kIx,	,
f	f	k?	f
píše	psát	k5eAaImIp3nS	psát
měkké	měkký	k2eAgNnSc1d1	měkké
i	i	k9	i
a	a	k8xC	a
ne	ne	k9	ne
y	y	k?	y
jako	jako	k8xC	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
např.	např.	kA	např.
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
lekcje	lekcje	k1gFnSc1	lekcje
fizyki	fizyk	k1gFnSc2	fizyk
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
polské	polský	k2eAgFnSc2d1	polská
gramatiky	gramatika	k1gFnSc2	gramatika
a	a	k8xC	a
pravopisu	pravopis	k1gInSc2	pravopis
správně	správně	k6eAd1	správně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skupiny	skupina	k1gFnPc1	skupina
samohlásek	samohláska	k1gFnPc2	samohláska
ia	ia	k0	ia
<g/>
,	,	kIx,	,
ie	ie	k?	ie
<g/>
,	,	kIx,	,
io	io	k?	io
<g/>
,	,	kIx,	,
iu	iu	k?	iu
(	(	kIx(	(
<g/>
ió	ió	k?	ió
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k9	i
pokud	pokud	k8xS	pokud
následují	následovat	k5eAaImIp3nP	následovat
za	za	k7c7	za
souhláskami	souhláska	k1gFnPc7	souhláska
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
obojetnými	obojetný	k2eAgInPc7d1	obojetný
a	a	k8xC	a
k	k	k7c3	k
a	a	k8xC	a
g	g	kA	g
<g/>
)	)	kIx)	)
kromě	kromě	k7c2	kromě
s	s	k7c7	s
<g/>
,	,	kIx,	,
z	z	k7c2	z
<g/>
,	,	kIx,	,
c	c	k0	c
a	a	k8xC	a
dz	dz	k?	dz
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
náhradně	náhradně	k6eAd1	náhradně
vyslovit	vyslovit	k5eAaPmF	vyslovit
jako	jako	k8xC	jako
ja	ja	k?	ja
<g/>
,	,	kIx,	,
jo	jo	k9	jo
<g/>
,	,	kIx,	,
ju	ju	k0	ju
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nikoliv	nikoliv	k9	nikoliv
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
dvouslabičně	dvouslabičně	k6eAd1	dvouslabičně
ija	ija	k0	ija
<g/>
,	,	kIx,	,
ije	ije	k?	ije
<g/>
,	,	kIx,	,
ijo	ijo	k?	ijo
<g/>
,	,	kIx,	,
iju	iju	k?	iju
<g/>
,	,	kIx,	,
iji	iji	k?	iji
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
piach	piach	k1gMnSc1	piach
[	[	kIx(	[
<g/>
p	p	k?	p
<g/>
́	́	k?	́
<g/>
jach	jach	k1gInSc1	jach
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
wioska	wioska	k1gFnSc1	wioska
<g/>
,	,	kIx,	,
pióro	pióro	k1gNnSc1	pióro
[	[	kIx(	[
<g/>
p	p	k?	p
<g/>
́	́	k?	́
<g/>
juro	jura	k1gFnSc5	jura
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
pero	pero	k1gNnSc1	pero
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kiur	kiur	k1gMnSc1	kiur
<g/>
,	,	kIx,	,
w	w	k?	w
oranżerii	oranżerie	k1gFnSc4	oranżerie
[	[	kIx(	[
<g/>
v	v	k7c6	v
oranžer-ji	oranžer	k1gInSc6	oranžer-j
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
mie	mie	k?	mie
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
vždy	vždy	k6eAd1	vždy
mje	mje	k?	mje
<g/>
;	;	kIx,	;
tedy	tedy	k9	tedy
jako	jako	k9	jako
moravské	moravský	k2eAgFnSc6d1	Moravská
"	"	kIx"	"
<g/>
mě	já	k3xPp1nSc2	já
<g/>
"	"	kIx"	"
a	a	k8xC	a
ne	ne	k9	ne
"	"	kIx"	"
<g/>
mně	já	k3xPp1nSc3	já
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgNnSc1d1	osobní
zájmeno	zájmeno	k1gNnSc1	zájmeno
"	"	kIx"	"
<g/>
mně	já	k3xPp1nSc6	já
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
stejně	stejně	k6eAd1	stejně
i	i	k9	i
polsky	polsky	k6eAd1	polsky
a	a	k8xC	a
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
"	"	kIx"	"
<g/>
mnie	mnie	k6eAd1	mnie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polština	polština	k1gFnSc1	polština
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
češtiny	čeština	k1gFnSc2	čeština
nezměkčuje	změkčovat	k5eNaImIp3nS	změkčovat
v	v	k7c6	v
pravopise	pravopis	k1gInSc6	pravopis
souhlásku	souhláska	k1gFnSc4	souhláska
t	t	k?	t
na	na	k7c4	na
"	"	kIx"	"
<g/>
ť	ť	k?	ť
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
"	"	kIx"	"
<g/>
ć	ć	k?	ć
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
před	před	k7c7	před
samohláskou	samohláska	k1gFnSc7	samohláska
psané	psaný	k2eAgFnSc2d1	psaná
"	"	kIx"	"
<g/>
ci	ci	k0	ci
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
zpravidla	zpravidla	k6eAd1	zpravidla
obdobné	obdobný	k2eAgNnSc4d1	obdobné
umístění	umístění	k1gNnSc4	umístění
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
příbuzných	příbuzná	k1gFnPc2	příbuzná
českým	český	k2eAgInSc7d1	český
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
českého	český	k2eAgInSc2d1	český
ť.	ť.	k?	ť.
</s>
</p>
<p>
<s>
Polské	polský	k2eAgFnPc1d1	polská
ń	ń	k?	ń
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
české	český	k2eAgFnSc2d1	Česká
ň	ň	k?	ň
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
samohláskami	samohláska	k1gFnPc7	samohláska
je	být	k5eAaImIp3nS	být
také	také	k9	také
vždy	vždy	k6eAd1	vždy
psané	psaný	k2eAgFnSc2d1	psaná
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
ni	on	k3xPp3gFnSc4	on
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Toruń	Toruń	k1gFnPc1	Toruń
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
Torunia	Torunium	k1gNnSc2	Torunium
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
před	před	k7c7	před
měkkým	měkký	k2eAgInSc7d1	měkký
i	i	k8xC	i
se	se	k3xPyFc4	se
změkčující	změkčující	k2eAgFnSc1d1	změkčující
diakritické	diakritický	k2eAgNnSc4d1	diakritické
znaménko	znaménko	k1gNnSc4	znaménko
nepíše	psát	k5eNaImIp3nS	psát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polštině	polština	k1gFnSc6	polština
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
toto	tento	k3xDgNnSc1	tento
pravidlo	pravidlo	k1gNnSc1	pravidlo
o	o	k7c6	o
"	"	kIx"	"
<g/>
ušetřeném	ušetřený	k2eAgNnSc6d1	ušetřené
<g/>
"	"	kIx"	"
znaménku	znaménko	k1gNnSc6	znaménko
před	před	k7c4	před
"	"	kIx"	"
<g/>
-i-	-	k?	-i-
<g/>
"	"	kIx"	"
týká	týkat	k5eAaImIp3nS	týkat
všech	všecek	k3xTgNnPc2	všecek
písmen	písmeno	k1gNnPc2	písmeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Sykavky	sykavka	k1gFnSc2	sykavka
a	a	k8xC	a
měkčení	měkčení	k1gNnSc2	měkčení
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
polštině	polština	k1gFnSc6	polština
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgInPc1	tři
řady	řad	k1gInPc1	řad
sykavek	sykavka	k1gFnPc2	sykavka
-	-	kIx~	-
prosté	prostý	k2eAgFnSc2d1	prostá
<g/>
,	,	kIx,	,
měkké	měkký	k2eAgFnSc2d1	měkká
a	a	k8xC	a
tvrdé	tvrdý	k2eAgFnSc2d1	tvrdá
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
kromě	kromě	k7c2	kromě
prostých	prostý	k2eAgFnPc2d1	prostá
sykavek	sykavka	k1gFnPc2	sykavka
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
dvě	dva	k4xCgFnPc1	dva
řady	řada	k1gFnPc1	řada
zadodásňových	zadodásňový	k2eAgFnPc2d1	zadodásňový
sykavek	sykavka	k1gFnPc2	sykavka
–	–	k?	–
měkké	měkký	k2eAgFnSc2d1	měkká
ś	ś	k?	ś
<g/>
,	,	kIx,	,
ź	ź	k?	ź
<g/>
,	,	kIx,	,
ć	ć	k?	ć
a	a	k8xC	a
dź	dź	k?	dź
<g/>
,	,	kIx,	,
vyslovované	vyslovovaný	k2eAgFnPc1d1	vyslovovaná
se	s	k7c7	s
hřbetem	hřbet	k1gInSc7	hřbet
jazyka	jazyk	k1gInSc2	jazyk
posunutým	posunutý	k2eAgInSc7d1	posunutý
k	k	k7c3	k
tvrdému	tvrdý	k2eAgNnSc3d1	tvrdé
patru	patro	k1gNnSc3	patro
(	(	kIx(	(
<g/>
palatalizované	palatalizovaný	k2eAgInPc4d1	palatalizovaný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
sz	sz	k?	sz
<g/>
,	,	kIx,	,
ż	ż	k?	ż
<g/>
,	,	kIx,	,
cz	cz	k?	cz
<g/>
,	,	kIx,	,
a	a	k8xC	a
dż	dż	k?	dż
<g/>
,	,	kIx,	,
vyslovované	vyslovovaný	k2eAgFnSc2d1	vyslovovaná
se	se	k3xPyFc4	se
špičkou	špička	k1gFnSc7	špička
jazyka	jazyk	k1gInSc2	jazyk
obrácenou	obrácený	k2eAgFnSc7d1	obrácená
k	k	k7c3	k
tvrdému	tvrdý	k2eAgNnSc3d1	tvrdé
patru	patro	k1gNnSc3	patro
(	(	kIx(	(
<g/>
retroflexní	retroflexní	k1gNnSc3	retroflexní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
sypialnia	sypialnium	k1gNnPc1	sypialnium
(	(	kIx(	(
<g/>
ložnice	ložnice	k1gFnSc1	ložnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
śruba	śruba	k1gFnSc1	śruba
(	(	kIx(	(
<g/>
šroub	šroub	k1gInSc1	šroub
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
siwy	siwy	k1gInPc1	siwy
[	[	kIx(	[
<g/>
šívy	šíva	k1gFnPc1	šíva
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
šedý	šedý	k2eAgInSc1d1	šedý
<g/>
,	,	kIx,	,
sivý	sivý	k2eAgInSc1d1	sivý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
szycie	szycie	k1gFnSc1	szycie
[	[	kIx(	[
<g/>
šyće	šyće	k1gFnSc1	šyće
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
šití	šití	k1gNnSc4	šití
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Prosté	prostý	k2eAgFnSc2d1	prostá
sykavky	sykavka	k1gFnSc2	sykavka
====	====	k?	====
</s>
</p>
<p>
<s>
Prosté	prostý	k2eAgFnPc1d1	prostá
sykavky	sykavka	k1gFnPc1	sykavka
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
jsou	být	k5eAaImIp3nP	být
c	c	k0	c
<g/>
,	,	kIx,	,
dz	dz	k?	dz
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
,	,	kIx,	,
z	z	k7c2	z
a	a	k8xC	a
českým	český	k2eAgMnSc7d1	český
mluvčím	mluvčí	k1gMnSc7	mluvčí
nečiní	činit	k5eNaImIp3nS	činit
žádné	žádný	k3yNgFnPc4	žádný
obtíže	obtíž	k1gFnPc4	obtíž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvojznak	dvojznak	k1gInSc1	dvojznak
dz	dz	k?	dz
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
dzwon	dzwon	k1gNnSc1	dzwon
(	(	kIx(	(
<g/>
zvon	zvon	k1gInSc1	zvon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
widzę	widzę	k?	widzę
(	(	kIx(	(
<g/>
vidím	vidět	k5eAaImIp1nS	vidět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
neuznává	uznávat	k5eNaImIp3nS	uznávat
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
hlásku	hláska	k1gFnSc4	hláska
čeština	čeština	k1gFnSc1	čeština
oficiálně	oficiálně	k6eAd1	oficiálně
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Souběh	souběh	k1gInSc1	souběh
"	"	kIx"	"
<g/>
-dz-	z-	k?	-dz-
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
číst	číst	k5eAaImF	číst
odděleně	odděleně	k6eAd1	odděleně
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
od-zvonit	odvonit	k1gInSc1	od-zvonit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hovorově	hovorově	k6eAd1	hovorově
se	se	k3xPyFc4	se
sloučí	sloučit	k5eAaPmIp3nP	sloučit
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
hlásky	hláska	k1gFnSc2	hláska
"	"	kIx"	"
<g/>
dz	dz	k?	dz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Měkké	měkký	k2eAgFnSc2d1	měkká
sykavky	sykavka	k1gFnSc2	sykavka
====	====	k?	====
</s>
</p>
<p>
<s>
Polština	polština	k1gFnSc1	polština
má	mít	k5eAaImIp3nS	mít
dále	daleko	k6eAd2	daleko
měkké	měkký	k2eAgFnPc4d1	měkká
sykavky	sykavka	k1gFnPc4	sykavka
ć	ć	k?	ć
<g/>
,	,	kIx,	,
dź	dź	k?	dź
<g/>
,	,	kIx,	,
ś	ś	k?	ś
<g/>
,	,	kIx,	,
ź	ź	k?	ź
<g/>
,	,	kIx,	,
takto	takto	k6eAd1	takto
s	s	k7c7	s
čárkou	čárka	k1gFnSc7	čárka
psané	psaný	k2eAgFnSc6d1	psaná
před	před	k7c7	před
souhláskami	souhláska	k1gFnPc7	souhláska
(	(	kIx(	(
<g/>
śruba	śrub	k1gMnSc2	śrub
<g/>
,	,	kIx,	,
leśny	leśna	k1gFnSc2	leśna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
samohláskami	samohláska	k1gFnPc7	samohláska
se	se	k3xPyFc4	se
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
jako	jako	k8xS	jako
spřežka	spřežka	k1gFnSc1	spřežka
"	"	kIx"	"
<g/>
xi	xi	k?	xi
<g/>
"	"	kIx"	"
s	s	k7c7	s
nevysloveným	vyslovený	k2eNgNnSc7d1	nevyslovené
i	i	k9	i
-	-	kIx~	-
např.	např.	kA	např.
ciasto	ciasto	k6eAd1	ciasto
(	(	kIx(	(
<g/>
těsto	těsto	k1gNnSc1	těsto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
siebie	siebie	k1gFnSc1	siebie
(	(	kIx(	(
<g/>
sebe	sebe	k3xPyFc4	sebe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dziś	dziś	k?	dziś
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poziom	poziom	k1gInSc1	poziom
(	(	kIx(	(
<g/>
úroveň	úroveň	k1gFnSc1	úroveň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dziura	dziura	k1gFnSc1	dziura
(	(	kIx(	(
<g/>
díra	díra	k1gFnSc1	díra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
měkká	měkký	k2eAgFnSc1d1	měkká
řada	řada	k1gFnSc1	řada
sykavek	sykavka	k1gFnPc2	sykavka
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
českým	český	k2eAgFnPc3d1	Česká
hláskám	hláska	k1gFnPc3	hláska
ť	ť	k?	ť
<g/>
,	,	kIx,	,
ď	ď	k?	ď
<g/>
,	,	kIx,	,
s	s	k7c7	s
a	a	k8xC	a
z.	z.	k?	z.
Měkké	měkký	k2eAgFnSc2d1	měkká
sykavky	sykavka	k1gFnSc2	sykavka
se	se	k3xPyFc4	se
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
pomocí	pomocí	k7c2	pomocí
zadní	zadní	k2eAgFnSc2d1	zadní
části	část	k1gFnSc2	část
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
patlavě	patlavě	k6eAd1	patlavě
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Čecha	Čech	k1gMnSc4	Čech
nesnadno	snadno	k6eNd1	snadno
<g/>
,	,	kIx,	,
výslovností	výslovnost	k1gFnSc7	výslovnost
měkčí	měkký	k2eAgFnSc7d2	měkčí
a	a	k8xC	a
s	s	k7c7	s
lehkým	lehký	k2eAgNnSc7d1	lehké
třením	tření	k1gNnSc7	tření
a	a	k8xC	a
rty	ret	k1gInPc7	ret
nejlépe	dobře	k6eAd3	dobře
navzájem	navzájem	k6eAd1	navzájem
blízko	blízko	k6eAd1	blízko
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
ty	ten	k3xDgFnPc1	ten
tvrdé	tvrdá	k1gFnPc1	tvrdá
(	(	kIx(	(
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
dż	dż	k?	dż
<g/>
,	,	kIx,	,
sz	sz	k?	sz
<g/>
,	,	kIx,	,
ż	ż	k?	ż
<g/>
)	)	kIx)	)
přední	přední	k2eAgFnSc7d1	přední
částí	část	k1gFnSc7	část
jazyka	jazyk	k1gInSc2	jazyk
zpravidla	zpravidla	k6eAd1	zpravidla
s	s	k7c7	s
okrouhlými	okrouhlý	k2eAgInPc7d1	okrouhlý
rty	ret	k1gInPc7	ret
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Franciszek	Franciszek	k1gInSc1	Franciszek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ci	ci	k0	ci
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k8xC	jako
ještě	ještě	k6eAd1	ještě
měkčí	měkký	k2eAgInSc1d2	měkčí
"	"	kIx"	"
<g/>
-či-	-či-	k?	-či-
<g/>
"	"	kIx"	"
než	než	k8xS	než
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
blízko	blízko	k6eAd1	blízko
"	"	kIx"	"
<g/>
ti	ten	k3xDgMnPc1	ten
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomtéž	týž	k3xTgNnSc6	týž
slově	slovo	k1gNnSc6	slovo
"	"	kIx"	"
<g/>
-sze-	ze-	k?	-sze-
<g/>
"	"	kIx"	"
naopak	naopak	k6eAd1	naopak
správně	správně	k6eAd1	správně
zní	znět	k5eAaImIp3nS	znět
tvrději	tvrdě	k6eAd2	tvrdě
než	než	k8xS	než
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
přímo	přímo	k6eAd1	přímo
o	o	k7c6	o
hlásku	hlásek	k1gInSc6	hlásek
i	i	k8xC	i
v	v	k7c6	v
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
zi	zi	k?	zi
<g/>
,	,	kIx,	,
ci	ci	k0	ci
<g/>
,	,	kIx,	,
dzi	dzi	k?	dzi
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
před	před	k7c4	před
"	"	kIx"	"
<g/>
i	i	k8xC	i
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
pravopisně	pravopisně	k6eAd1	pravopisně
nezdvojí	zdvojit	k5eNaPmIp3nP	zdvojit
a	a	k8xC	a
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc4	tento
skupiny	skupina	k1gFnPc4	skupina
jako	jako	k9	jako
śi	śi	k?	śi
<g/>
,	,	kIx,	,
źi	źi	k?	źi
<g/>
,	,	kIx,	,
ći	ći	k?	ći
<g/>
,	,	kIx,	,
dźi	dźi	k?	dźi
<g/>
,	,	kIx,	,
např.	např.	kA	např.
siny	sinus	k1gInPc1	sinus
<g/>
,	,	kIx,	,
zima	zima	k1gFnSc1	zima
(	(	kIx(	(
<g/>
zima	zima	k1gFnSc1	zima
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cichy	cich	k1gInPc1	cich
(	(	kIx(	(
<g/>
tichý	tichý	k2eAgInSc1d1	tichý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dziwny	dziwn	k1gInPc1	dziwn
(	(	kIx(	(
<g/>
divný	divný	k2eAgInSc1d1	divný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přidáním	přidání	k1gNnSc7	přidání
i	i	k9	i
při	při	k7c6	při
změkčení	změkčení	k1gNnSc6	změkčení
dvojznaku	dvojznak	k1gInSc2	dvojznak
dz	dz	k?	dz
před	před	k7c7	před
samohláskou	samohláska	k1gFnSc7	samohláska
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
"	"	kIx"	"
<g/>
i	i	k9	i
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
získáme	získat	k5eAaPmIp1nP	získat
jeho	jeho	k3xOp3gFnSc4	jeho
variantu	varianta	k1gFnSc4	varianta
dzi	dzi	k?	dzi
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgInSc4d1	jediný
polský	polský	k2eAgInSc4d1	polský
trojznak	trojznak	k?	trojznak
<g/>
,	,	kIx,	,
trigraf	trigraf	k1gInSc4	trigraf
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
dziennik	dziennik	k1gInSc4	dziennik
[	[	kIx(	[
<g/>
dźennik	dźennik	k1gInSc4	dźennik
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
blízko	blízko	k6eAd1	blízko
českému	český	k2eAgMnSc3d1	český
[	[	kIx(	[
<g/>
ď	ď	k?	ď
<g/>
]	]	kIx)	]
[	[	kIx(	[
<g/>
ďennik	ďennik	k1gInSc4	ďennik
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
deník	deník	k1gInSc4	deník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dziura	dziur	k1gMnSc4	dziur
(	(	kIx(	(
<g/>
díra	díra	k1gFnSc1	díra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Tvrdé	Tvrdé	k2eAgFnSc2d1	Tvrdé
sykavky	sykavka	k1gFnSc2	sykavka
====	====	k?	====
</s>
</p>
<p>
<s>
A	a	k9	a
konečně	konečně	k6eAd1	konečně
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
i	i	k9	i
třetí	třetí	k4xOgFnSc1	třetí
řada	řada	k1gFnSc1	řada
tvrdých	tvrdý	k2eAgFnPc2d1	tvrdá
sykavek	sykavka	k1gFnPc2	sykavka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
dż	dż	k?	dż
<g/>
,	,	kIx,	,
sz	sz	k?	sz
<g/>
,	,	kIx,	,
ż	ż	k?	ż
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
můžeme	moct	k5eAaImIp1nP	moct
přihrnout	přihrnout	k5eAaPmF	přihrnout
i	i	k9	i
rz	rz	k?	rz
s	s	k7c7	s
výslovností	výslovnost	k1gFnSc7	výslovnost
totožnou	totožný	k2eAgFnSc7d1	totožná
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ż	ż	k?	ż
nebo	nebo	k8xC	nebo
sz	sz	k?	sz
,	,	kIx,	,
<g/>
funkčně	funkčně	k6eAd1	funkčně
odpovídající	odpovídající	k2eAgMnSc1d1	odpovídající
českým	český	k2eAgFnPc3d1	Česká
</s>
</p>
<p>
<s>
č	č	k0	č
<g/>
,	,	kIx,	,
dž	dž	k?	dž
<g/>
,	,	kIx,	,
š	š	k?	š
<g/>
,	,	kIx,	,
ž	ž	k?	ž
a	a	k8xC	a
"	"	kIx"	"
<g/>
ř	ř	k?	ř
<g/>
"	"	kIx"	"
<g/>
Česká	český	k2eAgFnSc1d1	Česká
č	č	k0	č
<g/>
,	,	kIx,	,
dž	dž	k?	dž
<g/>
,	,	kIx,	,
š	š	k?	š
<g/>
,	,	kIx,	,
ž	ž	k?	ž
jsou	být	k5eAaImIp3nP	být
co	co	k9	co
do	do	k7c2	do
tvrdosti	tvrdost	k1gFnSc2	tvrdost
mezi	mezi	k7c7	mezi
polskými	polský	k2eAgFnPc7d1	polská
měkkými	měkký	k2eAgFnPc7d1	měkká
a	a	k8xC	a
tvrdými	tvrdý	k2eAgFnPc7d1	tvrdá
hláskami	hláska	k1gFnPc7	hláska
blíže	blíž	k1gFnSc2	blíž
tvrdým	tvrdý	k2eAgInPc3d1	tvrdý
a	a	k8xC	a
také	také	k9	také
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
výslovnosti	výslovnost	k1gFnSc6	výslovnost
polských	polský	k2eAgFnPc2d1	polská
hlásek	hláska	k1gFnPc2	hláska
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
polohami	poloha	k1gFnPc7	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
také	také	k9	také
česká	český	k2eAgFnSc1d1	Česká
výslovnost	výslovnost	k1gFnSc1	výslovnost
občas	občas	k6eAd1	občas
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
pro	pro	k7c4	pro
ozvláštnění	ozvláštnění	k1gNnSc4	ozvláštnění
nápisů	nápis	k1gInPc2	nápis
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
tečkovaného	tečkovaný	k2eAgInSc2d1	tečkovaný
ż	ż	k?	ż
píše	psát	k5eAaImIp3nS	psát
vodorovně	vodorovně	k6eAd1	vodorovně
přeškrtnuté	přeškrtnutý	k2eAgNnSc4d1	přeškrtnuté
z.	z.	k?	z.
</s>
</p>
<p>
<s>
Polské	polský	k2eAgFnPc1d1	polská
rz	rz	k?	rz
se	se	k3xPyFc4	se
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
neliší	lišit	k5eNaImIp3nS	lišit
od	od	k7c2	od
ż	ż	k?	ż
<g/>
,	,	kIx,	,
funkčně	funkčně	k6eAd1	funkčně
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
jako	jako	k9	jako
české	český	k2eAgNnSc4d1	české
ř.	ř.	k?	ř.
Proto	proto	k8xC	proto
také	také	k9	také
Poláci	Polák	k1gMnPc1	Polák
mají	mít	k5eAaImIp3nP	mít
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
rz	rz	k?	rz
a	a	k8xC	a
kde	kde	k6eAd1	kde
ż	ż	k?	ż
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
zvuku	zvuk	k1gInSc2	zvuk
to	ten	k3xDgNnSc4	ten
nepoznají	poznat	k5eNaPmIp3nP	poznat
a	a	k8xC	a
musí	muset	k5eAaImIp3nP	muset
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc2	jeho
použití	použití	k1gNnSc2	použití
učit	učit	k5eAaImF	učit
nazpaměť	nazpaměť	k6eAd1	nazpaměť
-	-	kIx~	-
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xC	jako
Češi	Čech	k1gMnPc1	Čech
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
učit	učit	k5eAaImF	učit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
píše	psát	k5eAaImIp3nS	psát
jaké	jaký	k3yIgNnSc4	jaký
"	"	kIx"	"
<g/>
i	i	k9	i
<g/>
/	/	kIx~	/
<g/>
y	y	k?	y
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polštině	polština	k1gFnSc6	polština
se	se	k3xPyFc4	se
po	po	k7c6	po
rz	rz	k?	rz
i	i	k8xC	i
ż	ż	k?	ż
vždy	vždy	k6eAd1	vždy
píše	psát	k5eAaImIp3nS	psát
y	y	k?	y
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
tvrdé	tvrdý	k2eAgFnPc4d1	tvrdá
souhlásky	souhláska	k1gFnPc4	souhláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příklady	příklad	k1gInPc1	příklad
různých	různý	k2eAgFnPc2d1	různá
sykavek	sykavka	k1gFnPc2	sykavka
<g/>
:	:	kIx,	:
morze	morze	k1gFnSc1	morze
(	(	kIx(	(
<g/>
moře	moře	k1gNnSc1	moře
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
zcela	zcela	k6eAd1	zcela
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
może	może	k6eAd1	może
(	(	kIx(	(
<g/>
může	moct	k5eAaImIp3nS	moct
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
se	se	k3xPyFc4	se
však	však	k9	však
pozná	poznat	k5eAaPmIp3nS	poznat
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
tvaru	tvar	k1gInSc6	tvar
slova	slovo	k1gNnSc2	slovo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
pádě	pád	k1gInSc6	pád
"	"	kIx"	"
<g/>
nad	nad	k7c7	nad
morzem	morz	k1gInSc7	morz
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
u	u	k7c2	u
moře	moře	k1gNnSc2	moře
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
osobě	osoba	k1gFnSc6	osoba
"	"	kIx"	"
<g/>
mogę	mogę	k?	mogę
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
mohu	moct	k5eAaImIp1nS	moct
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohýbání	ohýbání	k1gNnSc1	ohýbání
slov	slovo	k1gNnPc2	slovo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Skloňování	skloňování	k1gNnSc2	skloňování
===	===	k?	===
</s>
</p>
<p>
<s>
Polština	polština	k1gFnSc1	polština
má	mít	k5eAaImIp3nS	mít
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
čeština	čeština	k1gFnSc1	čeština
7	[number]	k4	7
pádů	pád	k1gInPc2	pád
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
przypadek	przypadek	k1gInSc1	przypadek
<g/>
,	,	kIx,	,
pl.	pl.	k?	pl.
przypadki	przypadk	k1gFnSc2	przypadk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pádové	pádový	k2eAgFnPc1d1	pádová
otázky	otázka	k1gFnPc1	otázka
jsou	být	k5eAaImIp3nP	být
stejné	stejný	k2eAgFnPc1d1	stejná
jako	jako	k8xC	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
pádů	pád	k1gInPc2	pád
je	být	k5eAaImIp3nS	být
také	také	k9	také
stejné	stejný	k2eAgNnSc1d1	stejné
<g/>
,	,	kIx,	,
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
,	,	kIx,	,
vokativ	vokativ	k1gInSc1	vokativ
a	a	k8xC	a
instrumentál	instrumentál	k1gInSc1	instrumentál
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
češtinou	čeština	k1gFnSc7	čeština
opačné	opačný	k2eAgFnSc2d1	opačná
pozice	pozice	k1gFnSc2	pozice
<g/>
:	:	kIx,	:
vokativ	vokativ	k1gInSc1	vokativ
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
5	[number]	k4	5
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
7	[number]	k4	7
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
a	a	k8xC	a
instrumentál	instrumentál	k1gInSc1	instrumentál
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
7	[number]	k4	7
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
5	[number]	k4	5
<g/>
.	.	kIx.	.
pádem	pád	k1gInSc7	pád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
přehledu	přehled	k1gInSc6	přehled
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
jejich	jejich	k3xOp3gInPc1	jejich
názvy	název	k1gInPc1	název
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
opravdu	opravdu	k6eAd1	opravdu
užívají	užívat	k5eAaImIp3nP	užívat
-	-	kIx~	-
oproti	oproti	k7c3	oproti
češtině	čeština	k1gFnSc3	čeština
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
častěji	často	k6eAd2	často
mluvíme	mluvit	k5eAaImIp1nP	mluvit
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gNnSc2	jejich
pořadí	pořadí	k1gNnSc2	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
jsou	být	k5eAaImIp3nP	být
pádové	pádový	k2eAgFnPc1d1	pádová
otázky	otázka	k1gFnPc1	otázka
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
prakticky	prakticky	k6eAd1	prakticky
shodné	shodný	k2eAgFnSc2d1	shodná
s	s	k7c7	s
češtinou	čeština	k1gFnSc7	čeština
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
mianownik	mianownik	k1gInSc1	mianownik
(	(	kIx(	(
<g/>
kto	kto	k?	kto
<g/>
?	?	kIx.	?
</s>
<s>
co	co	k3yRnSc1	co
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
dopełniacz	dopełniacz	k1gMnSc1	dopełniacz
(	(	kIx(	(
<g/>
kogo	kogo	k1gMnSc1	kogo
<g/>
?	?	kIx.	?
</s>
<s>
czego	czego	k6eAd1	czego
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
celownik	celownik	k1gInSc1	celownik
(	(	kIx(	(
<g/>
komu	kdo	k3yQnSc3	kdo
<g/>
?	?	kIx.	?
</s>
<s>
czemu	czemat	k5eAaPmIp1nS	czemat
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
biernik	biernik	k1gMnSc1	biernik
(	(	kIx(	(
<g/>
kogo	kogo	k1gMnSc1	kogo
<g/>
?	?	kIx.	?
</s>
<s>
co	co	k3yInSc1	co
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
narzędnik	narzędnik	k1gInSc1	narzędnik
((	((	k?	((
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
kim	kim	k?	kim
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
czym	czyma	k1gFnPc2	czyma
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
miejscownik	miejscownik	k1gInSc1	miejscownik
(	(	kIx(	(
<g/>
o	o	k7c6	o
kim	kim	k?	kim
<g/>
?	?	kIx.	?
</s>
<s>
o	o	k7c4	o
czym	czym	k1gInSc4	czym
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
wołaczStejně	wołaczStejně	k6eAd1	wołaczStejně
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgNnPc1	dva
gramatická	gramatický	k2eAgNnPc1d1	gramatické
čísla	číslo	k1gNnPc1	číslo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
jednotné	jednotný	k2eAgNnSc1d1	jednotné
(	(	kIx(	(
<g/>
liczba	liczba	k1gFnSc1	liczba
pojedyncza	pojedyncza	k1gFnSc1	pojedyncza
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
<g/>
:	:	kIx,	:
l.	l.	k?	l.
p.	p.	k?	p.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
množné	množný	k2eAgNnSc1d1	množné
(	(	kIx(	(
<g/>
liczba	liczba	k1gFnSc1	liczba
mnoga	mnoga	k1gFnSc1	mnoga
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
<g/>
:	:	kIx,	:
l.	l.	k?	l.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
Pravidla	pravidlo	k1gNnSc2	pravidlo
skloňování	skloňování	k1gNnSc2	skloňování
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
obdobná	obdobný	k2eAgNnPc1d1	obdobné
českému	český	k2eAgMnSc3d1	český
a	a	k8xC	a
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
téměř	téměř	k6eAd1	téměř
neztěžují	ztěžovat	k5eNaImIp3nP	ztěžovat
českému	český	k2eAgMnSc3d1	český
porozumění	porozumění	k1gNnSc2	porozumění
polské	polský	k2eAgFnSc6d1	polská
promluvě	promluva	k1gFnSc6	promluva
nebo	nebo	k8xC	nebo
textu	text	k1gInSc6	text
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jejich	jejich	k3xOp3gInSc1	jejich
význam	význam	k1gInSc1	význam
a	a	k8xC	a
užití	užití	k1gNnSc1	užití
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
totožný	totožný	k2eAgInSc1d1	totožný
<g/>
.	.	kIx.	.
</s>
<s>
Zachoval	Zachoval	k1gMnSc1	Zachoval
se	se	k3xPyFc4	se
ale	ale	k9	ale
genitiv	genitiv	k1gInSc1	genitiv
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
<g/>
,	,	kIx,	,
dopełniacz	dopełniacz	k1gInSc1	dopełniacz
<g/>
)	)	kIx)	)
v	v	k7c6	v
negativní	negativní	k2eAgFnSc6d1	negativní
existenciální	existenciální	k2eAgFnSc6d1	existenciální
větě	věta	k1gFnSc6	věta
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
záporový	záporový	k2eAgInSc1d1	záporový
genitiv	genitiv	k1gInSc1	genitiv
<g/>
;	;	kIx,	;
obdobně	obdobně	k6eAd1	obdobně
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
zaniklý	zaniklý	k2eAgMnSc1d1	zaniklý
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Ten	ten	k3xDgInSc1	ten
tu	tu	k6eAd1	tu
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
polsky	polsky	k6eAd1	polsky
"	"	kIx"	"
<g/>
Tego	Tego	k6eAd1	Tego
tu	tu	k6eAd1	tu
nie	nie	k?	nie
ma	ma	k?	ma
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Vyjadřování	vyjadřování	k1gNnPc4	vyjadřování
zdvořilosti	zdvořilost	k1gFnSc2	zdvořilost
==	==	k?	==
</s>
</p>
<p>
<s>
Odstupňování	odstupňování	k1gNnSc1	odstupňování
společenského	společenský	k2eAgNnSc2d1	společenské
vyjadřování	vyjadřování	k1gNnSc2	vyjadřování
respektu	respekt	k1gInSc2	respekt
je	být	k5eAaImIp3nS	být
i	i	k9	i
gramaticky	gramaticky	k6eAd1	gramaticky
bohatší	bohatý	k2eAgMnPc1d2	bohatší
<g/>
,	,	kIx,	,
než	než	k8xS	než
české	český	k2eAgFnPc1d1	Česká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
standardní	standardní	k2eAgNnSc4d1	standardní
přátelské	přátelský	k2eAgNnSc4d1	přátelské
tykání	tykání	k1gNnSc4	tykání
<g/>
,	,	kIx,	,
stejné	stejný	k2eAgNnSc4d1	stejné
jako	jako	k8xS	jako
české	český	k2eAgNnSc4d1	české
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zdvořilé	zdvořilý	k2eAgNnSc1d1	zdvořilé
oslovení	oslovení	k1gNnSc1	oslovení
a	a	k8xC	a
časování	časování	k1gNnSc1	časování
v	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
osobě	osoba	k1gFnSc3	osoba
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
českého	český	k2eAgNnSc2d1	české
vykání	vykání	k1gNnSc2	vykání
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
formou	forma	k1gFnSc7	forma
"	"	kIx"	"
<g/>
onikání	onikání	k1gNnSc4	onikání
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
oslovování	oslovování	k1gNnSc4	oslovování
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
osobě	osoba	k1gFnSc6	osoba
slovem	slovem	k6eAd1	slovem
pan	pan	k1gMnSc1	pan
(	(	kIx(	(
<g/>
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
panowie	panowie	k1gFnSc2	panowie
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pani	pani	k6eAd1	pani
<g/>
,	,	kIx,	,
vůči	vůči	k7c3	vůči
smíšené	smíšený	k2eAgFnSc3d1	smíšená
společnosti	společnost	k1gFnSc3	společnost
państwo	państwo	k6eAd1	państwo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
doslovném	doslovný	k2eAgInSc6d1	doslovný
překladu	překlad	k1gInSc6	překlad
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
polské	polský	k2eAgFnSc2d1	polská
otázky	otázka	k1gFnSc2	otázka
"	"	kIx"	"
<g/>
Czy	Czy	k1gMnSc2	Czy
mógłby	mógłba	k1gFnSc2	mógłba
mi	já	k3xPp1nSc3	já
pan	pan	k1gMnSc1	pan
powiedzieć	powiedzieć	k?	powiedzieć
<g/>
,	,	kIx,	,
gdzie	gdzie	k1gFnSc1	gdzie
jest	být	k5eAaImIp3nS	být
pana	pan	k1gMnSc4	pan
(	(	kIx(	(
<g/>
pański	pański	k6eAd1	pański
<g/>
)	)	kIx)	)
samochód	samochód	k6eAd1	samochód
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
<g/>
:	:	kIx,	:
Mohl	moct	k5eAaImAgInS	moct
by	by	kYmCp3nS	by
mi	já	k3xPp1nSc3	já
pán	pán	k1gMnSc1	pán
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pána	pán	k1gMnSc4	pán
(	(	kIx(	(
<g/>
příp	příp	kA	příp
<g/>
.	.	kIx.	.
panské	panské	k1gNnSc1	panské
<g/>
,	,	kIx,	,
vaše	váš	k3xOp2gNnSc1	váš
<g/>
)	)	kIx)	)
auto	auto	k1gNnSc1	auto
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
ve	v	k7c6	v
správném	správný	k2eAgInSc6d1	správný
překladu	překlad	k1gInSc6	překlad
<g/>
:	:	kIx,	:
Mohl	moct	k5eAaImAgMnS	moct
byste	by	kYmCp2nP	by
mi	já	k3xPp1nSc3	já
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vaše	váš	k3xOp2gNnSc4	váš
auto	auto	k1gNnSc4	auto
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
také	také	k9	také
přesná	přesný	k2eAgFnSc1d1	přesná
obdoba	obdoba	k1gFnSc1	obdoba
českého	český	k2eAgInSc2d1	český
(	(	kIx(	(
<g/>
a	a	k8xC	a
ruského	ruský	k2eAgMnSc2d1	ruský
nebo	nebo	k8xC	nebo
francouzského	francouzský	k2eAgMnSc2d1	francouzský
<g/>
)	)	kIx)	)
vykání	vykání	k1gNnSc2	vykání
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
osobě	osoba	k1gFnSc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
používali	používat	k5eAaImAgMnP	používat
polští	polský	k2eAgMnPc1d1	polský
komunisté	komunista	k1gMnPc1	komunista
v	v	k7c6	v
PSDS	PSDS	kA	PSDS
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
oslovením	oslovení	k1gNnSc7	oslovení
towarzyszu	towarzysz	k1gInSc2	towarzysz
<g/>
,	,	kIx,	,
towarzyszko	towarzyszka	k1gFnSc5	towarzyszka
<g/>
,	,	kIx,	,
towarzysze	towarzysze	k6eAd1	towarzysze
(	(	kIx(	(
<g/>
soudruhu	soudruh	k1gMnSc5	soudruh
<g/>
,	,	kIx,	,
soudružko	soudružka	k1gFnSc5	soudružka
<g/>
,	,	kIx,	,
soudruzi	soudruh	k1gMnPc5	soudruh
<g/>
)	)	kIx)	)
a	a	k8xC	a
za	za	k7c2	za
jejich	jejich	k3xOp3gFnSc2	jejich
vlády	vláda	k1gFnSc2	vláda
i	i	k8xC	i
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgMnPc2d1	ostatní
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
variantou	varianta	k1gFnSc7	varianta
je	být	k5eAaImIp3nS	být
smíšené	smíšený	k2eAgNnSc1d1	smíšené
tykání	tykání	k1gNnSc1	tykání
(	(	kIx(	(
<g/>
s	s	k7c7	s
2	[number]	k4	2
<g/>
.	.	kIx.	.
osobou	osoba	k1gFnSc7	osoba
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
oslovením	oslovení	k1gNnSc7	oslovení
"	"	kIx"	"
<g/>
pan	pan	k1gMnSc1	pan
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
pani	pani	k6eAd1	pani
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
částečně	částečně	k6eAd1	částečně
nezdvořilé	zdvořilý	k2eNgNnSc1d1	nezdvořilé
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
znamení	znamení	k1gNnSc1	znamení
jisté	jistý	k2eAgFnSc2d1	jistá
důvěrnosti	důvěrnost	k1gFnSc2	důvěrnost
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
tak	tak	k9	tak
oslovovat	oslovovat	k5eAaImF	oslovovat
známí	známý	k2eAgMnPc1d1	známý
sousedé	soused	k1gMnPc1	soused
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
řidiči	řidič	k1gMnPc1	řidič
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
chtějí	chtít	k5eAaImIp3nP	chtít
něco	něco	k6eAd1	něco
"	"	kIx"	"
<g/>
vyříkat	vyříkat	k5eAaPmF	vyříkat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
měl	mít	k5eAaImAgMnS	mít
dát	dát	k5eAaPmF	dát
komu	kdo	k3yRnSc3	kdo
přednost	přednost	k1gFnSc4	přednost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
česky	česky	k6eAd1	česky
přešlo	přejít	k5eAaPmAgNnS	přejít
na	na	k7c6	na
tykání	tykání	k1gNnSc6	tykání
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
s	s	k7c7	s
významem	význam	k1gInSc7	význam
<g/>
:	:	kIx,	:
Copak	copak	k3yQnSc1	copak
pán	pán	k1gMnSc1	pán
nevidíš	vidět	k5eNaImIp2nS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedu	jet	k5eAaImIp1nS	jet
z	z	k7c2	z
pravé	pravý	k2eAgFnSc2d1	pravá
strany	strana	k1gFnSc2	strana
<g/>
?!	?!	k?	?!
Musíš	muset	k5eAaImIp2nS	muset
mi	já	k3xPp1nSc3	já
pán	pán	k1gMnSc1	pán
dát	dát	k5eAaPmF	dát
přednost	přednost	k1gFnSc4	přednost
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
==	==	k?	==
Vzorový	vzorový	k2eAgInSc1d1	vzorový
text	text	k1gInSc1	text
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Pösingerová	Pösingerová	k1gFnSc1	Pösingerová
a	a	k8xC	a
Anna	Anna	k1gFnSc1	Anna
Seretny	Seretna	k1gFnSc2	Seretna
<g/>
:	:	kIx,	:
Czy	Czy	k1gMnSc1	Czy
Czechów	Czechów	k1gMnSc1	Czechów
jest	být	k5eAaImIp3nS	být
trzech	trzech	k1gInSc4	trzech
<g/>
?	?	kIx.	?
</s>
<s>
Učebnice	učebnice	k1gFnSc1	učebnice
polského	polský	k2eAgInSc2d1	polský
jazyka	jazyk	k1gInSc2	jazyk
pro	pro	k7c4	pro
Čechy	Čech	k1gMnPc4	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Uniwersytet	Uniwersytet	k1gInSc1	Uniwersytet
Jagielloński	Jagiellońsk	k1gFnSc2	Jagiellońsk
<g/>
,	,	kIx,	,
Kraków	Kraków	k1gFnSc1	Kraków
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
273	[number]	k4	273
pp	pp	k?	pp
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
83-233-0601-X	[number]	k4	83-233-0601-X
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Oliva	Oliva	k1gMnSc1	Oliva
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Kulošová	Kulošová	k1gFnSc1	Kulošová
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Zdenko	Zdenka	k1gFnSc5	Zdenka
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
:	:	kIx,	:
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
–	–	k?	–
<g/>
český	český	k2eAgMnSc1d1	český
<g/>
,	,	kIx,	,
česko	česko	k1gNnSc1	česko
<g/>
–	–	k?	–
<g/>
polský	polský	k2eAgInSc1d1	polský
kapesní	kapesní	k2eAgInSc1d1	kapesní
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
ed	ed	k?	ed
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
943	[number]	k4	943
pp	pp	k?	pp
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-04-23486-0	[number]	k4	80-04-23486-0
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
slezština	slezština	k1gFnSc1	slezština
</s>
</p>
<p>
<s>
kašubština	kašubština	k1gFnSc1	kašubština
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
polština	polština	k1gFnSc1	polština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
polština	polština	k1gFnSc1	polština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
Polština	polština	k1gFnSc1	polština
ve	v	k7c4	v
WikikniháchSienkiewicz	WikikniháchSienkiewicz	k1gInSc4	WikikniháchSienkiewicz
polsky	polsky	k6eAd1	polsky
a	a	k8xC	a
česky	česky	k6eAd1	česky
-	-	kIx~	-
popisy	popis	k1gInPc1	popis
přírody	příroda	k1gFnSc2	příroda
-	-	kIx~	-
zrcadlové	zrcadlový	k2eAgNnSc1d1	zrcadlové
vydání	vydání	k1gNnSc1	vydání
(	(	kIx(	(
<g/>
Portable	portable	k1gInSc1	portable
Document	Document	k1gMnSc1	Document
Format	Format	k1gInSc1	Format
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Polsko-český	polsko-český	k2eAgInSc1d1	polsko-český
slovník	slovník	k1gInSc1	slovník
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
Česko-polský	českoolský	k2eAgInSc1d1	česko-polský
slovník	slovník	k1gInSc1	slovník
administrativních	administrativní	k2eAgInPc2d1	administrativní
pojmů	pojem	k1gInPc2	pojem
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
Krkonošský	krkonošský	k2eAgInSc1d1	krkonošský
česko-polský	českoolský	k2eAgInSc1d1	česko-polský
slovník	slovník	k1gInSc1	slovník
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
Portable	portable	k1gInSc1	portable
Document	Document	k1gMnSc1	Document
Format	Format	k1gInSc1	Format
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Česko-polský	českoolský	k2eAgInSc1d1	česko-polský
sborník	sborník	k1gInSc1	sborník
administrativních	administrativní	k2eAgInPc2d1	administrativní
dokumentů	dokument	k1gInPc2	dokument
</s>
</p>
<p>
<s>
Česko-polský	českoolský	k2eAgInSc1d1	česko-polský
tematický	tematický	k2eAgInSc1d1	tematický
slovník	slovník	k1gInSc1	slovník
pro	pro	k7c4	pro
žáky	žák	k1gMnPc4	žák
a	a	k8xC	a
studenty	student	k1gMnPc4	student
(	(	kIx(	(
<g/>
Portable	portable	k1gInSc1	portable
Document	Document	k1gMnSc1	Document
Format	Format	k1gInSc1	Format
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Digitalizovaná	digitalizovaný	k2eAgFnSc1d1	digitalizovaná
edice	edice	k1gFnSc1	edice
učebnic	učebnice	k1gFnPc2	učebnice
"	"	kIx"	"
<g/>
Snadno	snadno	k6eAd1	snadno
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
<g/>
"	"	kIx"	"
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
včetně	včetně	k7c2	včetně
učebnice	učebnice	k1gFnSc2	učebnice
polštiny	polština	k1gFnSc2	polština
</s>
</p>
<p>
<s>
Česko-polský	českoolský	k2eAgInSc1d1	česko-polský
slovník	slovník	k1gInSc1	slovník
zrádných	zrádný	k2eAgNnPc2d1	zrádné
slov	slovo	k1gNnPc2	slovo
</s>
</p>
