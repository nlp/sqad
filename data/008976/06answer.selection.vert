<s>
Polština	polština	k1gFnSc1	polština
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
język	język	k6eAd1	język
polski	polski	k6eAd1	polski
<g/>
,	,	kIx,	,
polszczyzna	polszczyzna	k1gFnSc1	polszczyzna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přirozený	přirozený	k2eAgInSc4d1	přirozený
jazyk	jazyk	k1gInSc4	jazyk
patřící	patřící	k2eAgInSc4d1	patřící
mezi	mezi	k7c4	mezi
západoslovanské	západoslovanský	k2eAgInPc4d1	západoslovanský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
čeština	čeština	k1gFnSc1	čeština
<g/>
,	,	kIx,	,
slovenština	slovenština	k1gFnSc1	slovenština
<g/>
,	,	kIx,	,
kašubština	kašubština	k1gFnSc1	kašubština
a	a	k8xC	a
lužická	lužický	k2eAgFnSc1d1	Lužická
srbština	srbština	k1gFnSc1	srbština
<g/>
.	.	kIx.	.
</s>
