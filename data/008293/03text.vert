<p>
<s>
Gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
chorál	chorál	k1gInSc1	chorál
je	být	k5eAaImIp3nS	být
jednohlasý	jednohlasý	k2eAgInSc1d1	jednohlasý
latinský	latinský	k2eAgInSc1d1	latinský
zpěv	zpěv	k1gInSc1	zpěv
bez	bez	k7c2	bez
doprovodu	doprovod	k1gInSc2	doprovod
nástrojů	nástroj	k1gInPc2	nástroj
užívaný	užívaný	k2eAgInSc4d1	užívaný
při	při	k7c6	při
obřadech	obřad	k1gInPc6	obřad
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
podle	podle	k7c2	podle
papeže	papež	k1gMnSc2	papež
Řehoře	Řehoř	k1gMnSc2	Řehoř
Velikého	veliký	k2eAgMnSc2d1	veliký
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
540	[number]	k4	540
<g/>
-	-	kIx~	-
<g/>
604	[number]	k4	604
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejstarší	starý	k2eAgFnSc4d3	nejstarší
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
až	až	k6eAd1	až
dodnes	dodnes	k6eAd1	dodnes
uchovala	uchovat	k5eAaPmAgFnS	uchovat
beze	beze	k7c2	beze
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Původ	původ	k1gInSc1	původ
===	===	k?	===
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
označení	označení	k1gNnSc4	označení
gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
dostal	dostat	k5eAaPmAgInS	dostat
podle	podle	k7c2	podle
papeže	papež	k1gMnSc4	papež
Řehoře	Řehoř	k1gMnSc2	Řehoř
I.	I.	kA	I.
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Gregorius	Gregorius	k1gInSc1	Gregorius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nechal	nechat	k5eAaPmAgMnS	nechat
zpěvy	zpěv	k1gInPc4	zpěv
sesbírat	sesbírat	k5eAaPmF	sesbírat
a	a	k8xC	a
setřídit	setřídit	k5eAaPmF	setřídit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
přínos	přínos	k1gInSc1	přínos
však	však	k9	však
tkví	tkvět	k5eAaImIp3nS	tkvět
především	především	k9	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
liturgie	liturgie	k1gFnSc2	liturgie
<g/>
.	.	kIx.	.
</s>
<s>
Gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
chorál	chorál	k1gInSc1	chorál
byl	být	k5eAaImAgInS	být
ustanoven	ustanovit	k5eAaPmNgInS	ustanovit
teprve	teprve	k6eAd1	teprve
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
za	za	k7c2	za
pontifikátu	pontifikát	k1gInSc2	pontifikát
76	[number]	k4	76
<g/>
.	.	kIx.	.
papeže	papež	k1gMnSc4	papež
Vitaliana	Vitalian	k1gMnSc4	Vitalian
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Gregorianský	Gregorianský	k2eAgInSc1d1	Gregorianský
chorál	chorál	k1gInSc1	chorál
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
základních	základní	k2eAgFnPc6d1	základní
formách	forma	k1gFnPc6	forma
<g/>
:	:	kIx,	:
žalmy	žalm	k1gInPc4	žalm
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
kladen	kladen	k2eAgInSc4d1	kladen
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
vyjádření	vyjádření	k1gNnSc4	vyjádření
textu	text	k1gInSc2	text
a	a	k8xC	a
každé	každý	k3xTgFnSc3	každý
slabice	slabika	k1gFnSc3	slabika
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jedna	jeden	k4xCgFnSc1	jeden
nota	nota	k1gFnSc1	nota
(	(	kIx(	(
<g/>
syllabický	syllabický	k2eAgInSc1d1	syllabický
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zpěvů	zpěv	k1gInPc2	zpěv
aleluja	aleluja	k1gNnSc2	aleluja
(	(	kIx(	(
<g/>
Alleluja-Jubilus	Alleluja-Jubilus	k1gInSc1	Alleluja-Jubilus
<g/>
)	)	kIx)	)
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
slabiku	slabika	k1gFnSc4	slabika
více	hodně	k6eAd2	hodně
tónů	tón	k1gInPc2	tón
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	on	k3xPp3gFnPc4	on
melodie	melodie	k1gFnPc4	melodie
mnohem	mnohem	k6eAd1	mnohem
rozšířenější	rozšířený	k2eAgMnSc1d2	rozšířenější
(	(	kIx(	(
<g/>
melismatický	melismatický	k2eAgInSc1d1	melismatický
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mešní	mešní	k2eAgInPc1d1	mešní
zpěvy	zpěv	k1gInPc1	zpěv
jsou	být	k5eAaImIp3nP	být
propracované	propracovaný	k2eAgFnPc4d1	propracovaná
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Chorál	chorál	k1gInSc4	chorál
zpívají	zpívat	k5eAaImIp3nP	zpívat
sólisté	sólista	k1gMnPc1	sólista
nebo	nebo	k8xC	nebo
chór	chór	k1gInSc1	chór
nebo	nebo	k8xC	nebo
střídavě	střídavě	k6eAd1	střídavě
obě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
(	(	kIx(	(
<g/>
antifonálně	antifonálně	k6eAd1	antifonálně
<g/>
,	,	kIx,	,
či	či	k8xC	či
responsoriálně	responsoriálně	k6eAd1	responsoriálně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
zpěv	zpěv	k1gInSc4	zpěv
žalmů	žalm	k1gInPc2	žalm
v	v	k7c6	v
denní	denní	k2eAgFnSc6d1	denní
modlitbě	modlitba	k1gFnSc6	modlitba
církve	církev	k1gFnSc2	církev
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
celkem	celkem	k6eAd1	celkem
8	[number]	k4	8
modů	modus	k1gInPc2	modus
(	(	kIx(	(
<g/>
nápěvů	nápěv	k1gInPc2	nápěv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
mají	mít	k5eAaImIp3nP	mít
ještě	ještě	k9	ještě
jednu	jeden	k4xCgFnSc4	jeden
či	či	k9wB	či
více	hodně	k6eAd2	hodně
variací	variace	k1gFnPc2	variace
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
liší	lišit	k5eAaImIp3nP	lišit
způsobem	způsob	k1gInSc7	způsob
intonace	intonace	k1gFnSc2	intonace
závěrečných	závěrečný	k2eAgFnPc2d1	závěrečná
částí	část	k1gFnPc2	část
veršů	verš	k1gInPc2	verš
žalmů	žalm	k1gInPc2	žalm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nápěvy	nápěv	k1gInPc1	nápěv
pro	pro	k7c4	pro
denní	denní	k2eAgFnSc4d1	denní
modlitbu	modlitba	k1gFnSc4	modlitba
církve	církev	k1gFnSc2	církev
jsou	být	k5eAaImIp3nP	být
shromážděny	shromáždit	k5eAaPmNgFnP	shromáždit
v	v	k7c6	v
antifonáři	antifonář	k1gInSc6	antifonář
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
mj.	mj.	kA	mj.
v	v	k7c6	v
usuálu	usuál	k1gInSc6	usuál
<g/>
.	.	kIx.	.
</s>
<s>
Gregoriánské	gregoriánský	k2eAgInPc1d1	gregoriánský
zpěvy	zpěv	k1gInPc1	zpěv
<g/>
,	,	kIx,	,
užívané	užívaný	k2eAgInPc1d1	užívaný
při	při	k7c6	při
mši	mše	k1gFnSc6	mše
svaté	svatá	k1gFnSc2	svatá
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
shromážděny	shromáždit	k5eAaPmNgFnP	shromáždit
v	v	k7c6	v
graduálu	graduál	k1gInSc6	graduál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
gregoriánsky	gregoriánsky	k6eAd1	gregoriánsky
zpívanou	zpívaný	k2eAgFnSc7d1	zpívaná
modlitbou	modlitba	k1gFnSc7	modlitba
breviáře	breviář	k1gInSc2	breviář
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
zejména	zejména	k9	zejména
v	v	k7c6	v
řeholních	řeholní	k2eAgFnPc6d1	řeholní
komunitách	komunita	k1gFnPc6	komunita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
jako	jako	k9	jako
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
poslání	poslání	k1gNnPc2	poslání
slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
konání	konání	k1gNnSc1	konání
bohoslužeb	bohoslužba	k1gFnPc2	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
klášterech	klášter	k1gInPc6	klášter
mnišských	mnišský	k2eAgInPc2d1	mnišský
řádů	řád	k1gInPc2	řád
(	(	kIx(	(
<g/>
benediktini	benediktin	k1gMnPc1	benediktin
<g/>
,	,	kIx,	,
cisterciáci	cisterciák	k1gMnPc1	cisterciák
<g/>
,	,	kIx,	,
trapisté	trapist	k1gMnPc1	trapist
<g/>
,	,	kIx,	,
kartuziáni	kartuzián	k1gMnPc1	kartuzián
<g/>
)	)	kIx)	)
či	či	k8xC	či
klášterech	klášter	k1gInPc6	klášter
řeholních	řeholní	k2eAgMnPc2d1	řeholní
kanovníků	kanovník	k1gMnPc2	kanovník
(	(	kIx(	(
<g/>
premonstráti	premonstrát	k1gMnPc1	premonstrát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Renesance	renesance	k1gFnSc2	renesance
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
renesanční	renesanční	k2eAgFnSc6d1	renesanční
hudbě	hudba	k1gFnSc6	hudba
se	se	k3xPyFc4	se
vžil	vžít	k5eAaPmAgInS	vžít
systém	systém	k1gInSc1	systém
kombinování	kombinování	k1gNnSc2	kombinování
mše	mše	k1gFnSc2	mše
s	s	k7c7	s
gregoriánským	gregoriánský	k2eAgInSc7d1	gregoriánský
chorálem	chorál	k1gInSc7	chorál
<g/>
,	,	kIx,	,
např.	např.	kA	např.
mezi	mezi	k7c4	mezi
5	[number]	k4	5
částí	část	k1gFnPc2	část
mše	mše	k1gFnSc2	mše
se	se	k3xPyFc4	se
vkládaly	vkládat	k5eAaImAgFnP	vkládat
části	část	k1gFnPc1	část
melismatického	melismatický	k2eAgInSc2d1	melismatický
zpěvu	zpěv	k1gInSc2	zpěv
(	(	kIx(	(
<g/>
aleluja	aleluja	k1gNnSc2	aleluja
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Missa	Miss	k1gMnSc2	Miss
Pange	Pange	k1gFnSc1	Pange
Lingua	Lingua	k1gFnSc1	Lingua
(	(	kIx(	(
<g/>
Josquin	Josquin	k2eAgInSc1d1	Josquin
Desprez	Desprez	k1gInSc1	Desprez
1450	[number]	k4	1450
<g/>
–	–	k?	–
<g/>
1521	[number]	k4	1521
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
vrcholné	vrcholný	k2eAgFnSc2d1	vrcholná
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Interpreti	interpret	k1gMnPc5	interpret
==	==	k?	==
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Eben	eben	k1gInSc1	eben
–	–	k?	–
umělecký	umělecký	k2eAgMnSc1d1	umělecký
vedoucí	vedoucí	k1gMnSc1	vedoucí
souboru	soubor	k1gInSc2	soubor
Schola	Schola	k1gFnSc1	Schola
gregoriana	gregoriana	k1gFnSc1	gregoriana
pragensis	pragensis	k1gFnSc1	pragensis
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Gerbrich	Gerbrich	k1gMnSc1	Gerbrich
–	–	k?	–
umělecký	umělecký	k2eAgMnSc1d1	umělecký
vedoucí	vedoucí	k1gMnSc1	vedoucí
souboru	soubor	k1gInSc2	soubor
Svatomichalská	Svatomichalský	k2eAgFnSc1d1	Svatomichalská
gregoriánská	gregoriánský	k2eAgFnSc1d1	gregoriánská
schola	schola	k1gFnSc1	schola
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Hodina	hodina	k1gFnSc1	hodina
–	–	k?	–
umělecký	umělecký	k2eAgMnSc1d1	umělecký
vedoucí	vedoucí	k1gMnSc1	vedoucí
souboru	soubor	k1gInSc2	soubor
Pražská	pražský	k2eAgFnSc1d1	Pražská
katedrální	katedrální	k2eAgFnSc1d1	katedrální
schola	schola	k1gFnSc1	schola
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Venhoda	Venhoda	k1gMnSc1	Venhoda
–	–	k?	–
umělecký	umělecký	k2eAgMnSc1d1	umělecký
vedoucí	vedoucí	k1gMnSc1	vedoucí
chlapeckého	chlapecký	k2eAgInSc2d1	chlapecký
sboru	sbor	k1gInSc2	sbor
Schola	Schola	k1gFnSc1	Schola
cantorum	cantorum	k1gNnSc4	cantorum
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
v	v	k7c6	v
Břevnově	Břevnov	k1gInSc6	Břevnov
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Řehoř	Řehoř	k1gMnSc1	Řehoř
I.	I.	kA	I.
Veliký	veliký	k2eAgInSc5d1	veliký
</s>
</p>
<p>
<s>
Vitalianus	Vitalianus	k1gMnSc1	Vitalianus
</s>
</p>
<p>
<s>
Gregorian	Gregorian	k1gInSc1	Gregorian
(	(	kIx(	(
<g/>
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
chorál	chorál	k1gInSc4	chorál
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Výukový	výukový	k2eAgInSc1d1	výukový
kurs	kurs	k1gInSc1	kurs
Dějiny	dějiny	k1gFnPc1	dějiny
evropské	evropský	k2eAgFnSc2d1	Evropská
hudby	hudba	k1gFnSc2	hudba
<g/>
/	/	kIx~	/
<g/>
Středověk	středověk	k1gInSc1	středověk
<g/>
#	#	kIx~	#
<g/>
Gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
chorál	chorál	k1gInSc1	chorál
ve	v	k7c6	v
Wikiverzitě	Wikiverzita	k1gFnSc6	Wikiverzita
</s>
</p>
<p>
<s>
Canticum	Canticum	k1gNnSc1	Canticum
Novum	novum	k1gNnSc4	novum
o	o	k7c6	o
gregoriánském	gregoriánský	k2eAgInSc6d1	gregoriánský
chorálu	chorál	k1gInSc6	chorál
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Odkazy	odkaz	k1gInPc1	odkaz
na	na	k7c4	na
stránky	stránka	k1gFnPc4	stránka
s	s	k7c7	s
tematikou	tematika	k1gFnSc7	tematika
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
chorálu	chorál	k1gInSc2	chorál
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
s	s	k7c7	s
notacemi	notace	k1gFnPc7	notace
pro	pro	k7c4	pro
modlitbu	modlitba	k1gFnSc4	modlitba
breviáře	breviář	k1gInSc2	breviář
</s>
</p>
