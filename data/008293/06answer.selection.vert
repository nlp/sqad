<s>
Gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
chorál	chorál	k1gInSc1	chorál
je	být	k5eAaImIp3nS	být
jednohlasý	jednohlasý	k2eAgInSc1d1	jednohlasý
latinský	latinský	k2eAgInSc1d1	latinský
zpěv	zpěv	k1gInSc1	zpěv
bez	bez	k7c2	bez
doprovodu	doprovod	k1gInSc2	doprovod
nástrojů	nástroj	k1gInPc2	nástroj
užívaný	užívaný	k2eAgInSc4d1	užívaný
při	při	k7c6	při
obřadech	obřad	k1gInPc6	obřad
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
podle	podle	k7c2	podle
papeže	papež	k1gMnSc2	papež
Řehoře	Řehoř	k1gMnSc2	Řehoř
Velikého	veliký	k2eAgMnSc2d1	veliký
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
540	[number]	k4	540
<g/>
-	-	kIx~	-
<g/>
604	[number]	k4	604
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
