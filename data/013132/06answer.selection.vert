<s>
Doktor	doktor	k1gMnSc1	doktor
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
rerum	rerum	k1gInSc1	rerum
naturalium	naturalium	k1gNnSc1	naturalium
doctor	doctor	k1gInSc1	doctor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
RNDr.	RNDr.	kA	RNDr.
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
udělovaný	udělovaný	k2eAgInSc4d1	udělovaný
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
