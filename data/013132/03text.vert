<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
rerum	rerum	k1gInSc1	rerum
naturalium	naturalium	k1gNnSc1	naturalium
doctor	doctor	k1gInSc1	doctor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
RNDr.	RNDr.	kA	RNDr.
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
udělovaný	udělovaný	k2eAgInSc4d1	udělovaný
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Udělován	udělován	k2eAgMnSc1d1	udělován
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
přírodovědeckých	přírodovědecký	k2eAgFnPc6d1	Přírodovědecká
fakultách	fakulta	k1gFnPc6	fakulta
některé	některý	k3yIgFnSc2	některý
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
slovenských	slovenský	k2eAgFnPc2d1	slovenská
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
obdržení	obdržení	k1gNnSc3	obdržení
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
získaný	získaný	k2eAgInSc4d1	získaný
titul	titul	k1gInSc4	titul
magistr	magistr	k1gMnSc1	magistr
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
a	a	k8xC	a
složení	složení	k1gNnPc4	složení
rigorózní	rigorózní	k2eAgFnPc4d1	rigorózní
zkoušky	zkouška	k1gFnPc4	zkouška
ze	z	k7c2	z
zvoleného	zvolený	k2eAgInSc2d1	zvolený
vědního	vědní	k2eAgInSc2d1	vědní
oboru	obor	k1gInSc2	obor
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
obhájení	obhájení	k1gNnSc1	obhájení
rigorózní	rigorózní	k2eAgFnSc2d1	rigorózní
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
fakultativní	fakultativní	k2eAgFnSc4d1	fakultativní
zkoušku	zkouška	k1gFnSc4	zkouška
zpravidla	zpravidla	k6eAd1	zpravidla
spojenou	spojený	k2eAgFnSc4d1	spojená
s	s	k7c7	s
poplatky	poplatek	k1gInPc7	poplatek
<g/>
.	.	kIx.	.
<g/>
Titul	titul	k1gInSc1	titul
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
RNDr.	RNDr.	kA	RNDr.
však	však	k9	však
není	být	k5eNaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
doktorského	doktorský	k2eAgNnSc2d1	doktorské
studia	studio	k1gNnSc2	studio
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
–	–	k?	–
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
doctor	doctor	k1gInSc1	doctor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
stále	stále	k6eAd1	stále
o	o	k7c4	o
magisterský	magisterský	k2eAgInSc4d1	magisterský
stupeň	stupeň	k1gInSc4	stupeň
vysokoškolské	vysokoškolský	k2eAgFnSc2d1	vysokoškolská
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
magistr	magistr	k1gMnSc1	magistr
<g/>
,	,	kIx,	,
7	[number]	k4	7
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
případné	případný	k2eAgNnSc4d1	případné
doktorské	doktorský	k2eAgNnSc4d1	doktorské
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
velký	velký	k2eAgInSc1d1	velký
doktorát	doktorát	k1gInSc1	doktorát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dosahován	dosahovat	k5eAaImNgInS	dosahovat
dalším	další	k2eAgInSc7d1	další
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
čtyřletým	čtyřletý	k2eAgNnSc7d1	čtyřleté
studiem	studio	k1gNnSc7	studio
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
Pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
předpis	předpis	k1gInSc1	předpis
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
příslušný	příslušný	k2eAgInSc1d1	příslušný
rigorózní	rigorózní	k2eAgInSc1d1	rigorózní
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
nositel	nositel	k1gMnSc1	nositel
titulu	titul	k1gInSc2	titul
magistr	magistr	k1gMnSc1	magistr
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
případně	případně	k6eAd1	případně
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
stejná	stejný	k2eAgFnSc1d1	stejná
předložená	předložený	k2eAgFnSc1d1	předložená
magisterská	magisterský	k2eAgFnSc1d1	magisterská
(	(	kIx(	(
<g/>
diplomová	diplomový	k2eAgFnSc1d1	Diplomová
<g/>
)	)	kIx)	)
práce	práce	k1gFnSc1	práce
rovněž	rovněž	k9	rovněž
uznána	uznat	k5eAaPmNgFnS	uznat
i	i	k9	i
jako	jako	k9	jako
rigorózní	rigorózní	k2eAgFnPc4d1	rigorózní
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Rigorózum	rigorózum	k1gNnSc1	rigorózum
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
finančně	finančně	k6eAd1	finančně
podmíněno	podmínit	k5eAaPmNgNnS	podmínit
–	–	k?	–
poplatky	poplatek	k1gInPc7	poplatek
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
spojené	spojený	k2eAgInPc1d1	spojený
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
příjmem	příjem	k1gInSc7	příjem
dané	daný	k2eAgFnSc2d1	daná
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
absolvent	absolvent	k1gMnSc1	absolvent
příslušného	příslušný	k2eAgInSc2d1	příslušný
doktorského	doktorský	k2eAgInSc2d1	doktorský
studijního	studijní	k2eAgInSc2d1	studijní
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
příslušné	příslušný	k2eAgInPc1d1	příslušný
vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
předpisy	předpis	k1gInPc1	předpis
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
případně	případně	k6eAd1	případně
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
uznání	uznání	k1gNnSc4	uznání
své	svůj	k3xOyFgFnSc2	svůj
disertační	disertační	k2eAgFnSc2d1	disertační
práce	práce	k1gFnSc2	práce
jako	jako	k8xS	jako
práce	práce	k1gFnSc2	práce
rigorózní	rigorózní	k2eAgFnSc2d1	rigorózní
<g/>
,	,	kIx,	,
podrobnosti	podrobnost	k1gFnPc4	podrobnost
většinou	většina	k1gFnSc7	většina
upravuje	upravovat	k5eAaImIp3nS	upravovat
pokyn	pokyn	k1gInSc4	pokyn
rektora	rektor	k1gMnSc2	rektor
<g/>
,	,	kIx,	,
pokyn	pokyn	k1gInSc4	pokyn
děkana	děkan	k1gMnSc2	děkan
atp.	atp.	kA	atp.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
===	===	k?	===
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
titulu	titul	k1gInSc2	titul
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
oddělením	oddělení	k1gNnSc7	oddělení
přírodovědeckých	přírodovědecký	k2eAgFnPc2d1	Přírodovědecká
fakult	fakulta	k1gFnPc2	fakulta
od	od	k7c2	od
fakult	fakulta	k1gFnPc2	fakulta
filozofických	filozofický	k2eAgMnPc2d1	filozofický
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
absolventům	absolvent	k1gMnPc3	absolvent
přírodovědných	přírodovědný	k2eAgFnPc2d1	přírodovědná
studií	studie	k1gFnPc2	studie
na	na	k7c6	na
filozofických	filozofický	k2eAgFnPc6d1	filozofická
fakultách	fakulta	k1gFnPc6	fakulta
přiznáván	přiznáván	k2eAgInSc1d1	přiznáván
po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
složení	složení	k1gNnSc6	složení
přísných	přísný	k2eAgFnPc2d1	přísná
zkoušek	zkouška	k1gFnPc2	zkouška
titul	titul	k1gInSc4	titul
doktora	doktor	k1gMnSc4	doktor
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yRnSc1	kdo
chtěl	chtít	k5eAaImAgMnS	chtít
poté	poté	k6eAd1	poté
získat	získat	k5eAaPmF	získat
doktorát	doktorát	k1gInSc4	doktorát
věd	věda	k1gFnPc2	věda
přírodních	přírodní	k2eAgFnPc2d1	přírodní
(	(	kIx(	(
<g/>
používal	používat	k5eAaImAgInS	používat
se	se	k3xPyFc4	se
neoficiální	neoficiální	k2eAgInSc1d1	neoficiální
titul	titul	k1gInSc1	titul
RNC	RNC	kA	RNC
<g/>
.	.	kIx.	.
–	–	k?	–
rerum	rerum	k1gInSc1	rerum
naturalium	naturalium	k1gNnSc1	naturalium
candidatus	candidatus	k1gInSc1	candidatus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgInS	muset
obhájit	obhájit	k5eAaPmF	obhájit
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
rozpravu	rozprava	k1gFnSc4	rozprava
(	(	kIx(	(
<g/>
disertaci	disertace	k1gFnSc4	disertace
<g/>
)	)	kIx)	)
z	z	k7c2	z
přírodovědného	přírodovědný	k2eAgInSc2d1	přírodovědný
oboru	obor	k1gInSc2	obor
a	a	k8xC	a
vykonat	vykonat	k5eAaPmF	vykonat
dvě	dva	k4xCgFnPc1	dva
přísné	přísný	k2eAgFnPc1d1	přísná
zkoušky	zkouška	k1gFnPc1	zkouška
(	(	kIx(	(
<g/>
rigorosa	rigorosum	k1gNnPc1	rigorosum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
disertace	disertace	k1gFnSc2	disertace
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
z	z	k7c2	z
filosofie	filosofie	k1gFnSc1	filosofie
biologických	biologický	k2eAgFnPc2d1	biologická
nebo	nebo	k8xC	nebo
exaktních	exaktní	k2eAgFnPc2d1	exaktní
věd	věda	k1gFnPc2	věda
či	či	k8xC	či
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
přírodovědného	přírodovědný	k2eAgInSc2d1	přírodovědný
oboru	obor	k1gInSc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
ověřeno	ověřen	k2eAgNnSc1d1	ověřeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
kandidát	kandidát	k1gMnSc1	kandidát
doktorství	doktorství	k1gNnSc2	doktorství
způsobilý	způsobilý	k2eAgInSc1d1	způsobilý
vědecky	vědecky	k6eAd1	vědecky
bádat	bádat	k5eAaImF	bádat
<g/>
.	.	kIx.	.
</s>
<s>
Hodnocení	hodnocení	k1gNnSc1	hodnocení
bylo	být	k5eAaImAgNnS	být
obstál	obstát	k5eAaPmAgMnS	obstát
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
<g/>
,	,	kIx,	,
obstál	obstát	k5eAaPmAgMnS	obstát
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
neobstál	obstát	k5eNaPmAgInS	obstát
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zkoušky	zkouška	k1gFnPc1	zkouška
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
opakovány	opakovat	k5eAaImNgFnP	opakovat
nejvýše	nejvýše	k6eAd1	nejvýše
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
kandidát	kandidát	k1gMnSc1	kandidát
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
promován	promovat	k5eAaBmNgMnS	promovat
doktorem	doktor	k1gMnSc7	doktor
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
reformě	reforma	k1gFnSc6	reforma
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
studia	studio	k1gNnSc2	studio
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
už	už	k6eAd1	už
nebyly	být	k5eNaImAgInP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
akademické	akademický	k2eAgInPc4d1	akademický
tituly	titul	k1gInPc4	titul
udělovány	udělován	k2eAgInPc4d1	udělován
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
tzv.	tzv.	kA	tzv.
profesní	profesní	k2eAgNnSc1d1	profesní
označení	označení	k1gNnSc1	označení
<g/>
)	)	kIx)	)
a	a	k8xC	a
návrat	návrat	k1gInSc4	návrat
titulu	titul	k1gInSc2	titul
doktora	doktor	k1gMnSc2	doktor
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
tak	tak	k6eAd1	tak
znamenal	znamenat	k5eAaImAgInS	znamenat
až	až	k9	až
další	další	k2eAgInSc1d1	další
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
doktor	doktor	k1gMnSc1	doktor
přírodovědy	přírodověda	k1gFnSc2	přírodověda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
už	už	k6eAd1	už
ale	ale	k8xC	ale
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
akademicko-vědecké	akademickoědecký	k2eAgNnSc4d1	akademicko-vědecký
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
již	již	k6eAd1	již
byly	být	k5eAaImAgInP	být
zavedeny	zavést	k5eAaPmNgInP	zavést
po	po	k7c6	po
sovětském	sovětský	k2eAgInSc6d1	sovětský
vzoru	vzor	k1gInSc6	vzor
tzv.	tzv.	kA	tzv.
vědecké	vědecký	k2eAgFnSc2d1	vědecká
hodnosti	hodnost	k1gFnSc2	hodnost
(	(	kIx(	(
<g/>
CSc.	CSc.	kA	CSc.
a	a	k8xC	a
DrSc	DrSc	kA	DrSc
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
RNDr.	RNDr.	kA	RNDr.
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
doktor	doktor	k1gMnSc1	doktor
přírododních	přírododní	k2eAgFnPc2d1	přírododní
věd	věda	k1gFnPc2	věda
<g/>
)	)	kIx)	)
nicméně	nicméně	k8xC	nicméně
opět	opět	k6eAd1	opět
nebyl	být	k5eNaImAgInS	být
automaticky	automaticky	k6eAd1	automaticky
udělován	udělovat	k5eAaImNgInS	udělovat
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
přírodovědného	přírodovědný	k2eAgNnSc2d1	Přírodovědné
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
udělen	udělit	k5eAaPmNgMnS	udělit
pouze	pouze	k6eAd1	pouze
pokud	pokud	k8xS	pokud
absolvent	absolvent	k1gMnSc1	absolvent
opět	opět	k6eAd1	opět
obhájil	obhájit	k5eAaPmAgMnS	obhájit
písemnou	písemný	k2eAgFnSc4d1	písemná
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
složil	složit	k5eAaPmAgMnS	složit
rigorózní	rigorózní	k2eAgFnSc4d1	rigorózní
zkoušku	zkouška	k1gFnSc4	zkouška
ze	z	k7c2	z
zvoleného	zvolený	k2eAgInSc2d1	zvolený
přírodovědného	přírodovědný	k2eAgInSc2d1	přírodovědný
oboru	obor	k1gInSc2	obor
a	a	k8xC	a
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
širšího	široký	k2eAgInSc2d2	širší
vědního	vědní	k2eAgInSc2d1	vědní
základu	základ	k1gInSc2	základ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
novým	nový	k2eAgInSc7d1	nový
vysokoškolským	vysokoškolský	k2eAgInSc7d1	vysokoškolský
zákonem	zákon	k1gInSc7	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
akademický	akademický	k2eAgInSc1d1	akademický
titul	titul	k1gInSc1	titul
RNDr.	RNDr.	kA	RNDr.
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgInPc1d1	ostatní
fakultativní	fakultativní	k2eAgInPc1d1	fakultativní
malé	malý	k2eAgInPc1d1	malý
doktoráty	doktorát	k1gInPc1	doktorát
<g/>
,	,	kIx,	,
zrušen	zrušen	k2eAgInSc1d1	zrušen
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
udělován	udělovat	k5eAaImNgInS	udělovat
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
školy	škola	k1gFnSc2	škola
udělován	udělován	k2eAgInSc4d1	udělován
titul	titul	k1gInSc4	titul
magistra	magistra	k1gFnSc1	magistra
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
doktorátu	doktorát	k1gInSc2	doktorát
obtížnější	obtížný	k2eAgFnSc1d2	obtížnější
<g/>
.	.	kIx.	.
</s>
<s>
Boloňský	boloňský	k2eAgInSc1d1	boloňský
proces	proces	k1gInSc1	proces
pak	pak	k6eAd1	pak
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
evropské	evropský	k2eAgNnSc4d1	Evropské
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
stavem	stav	k1gInSc7	stav
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
přijetí	přijetí	k1gNnSc2	přijetí
nového	nový	k2eAgInSc2d1	nový
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
zákona	zákon	k1gInSc2	zákon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
opět	opět	k6eAd1	opět
udělován	udělován	k2eAgInSc1d1	udělován
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nyní	nyní	k6eAd1	nyní
po	po	k7c6	po
dodatečné	dodatečný	k2eAgFnSc6d1	dodatečná
a	a	k8xC	a
zpoplatněné	zpoplatněný	k2eAgFnSc6d1	zpoplatněná
rigorózní	rigorózní	k2eAgFnSc6d1	rigorózní
zkoušce	zkouška	k1gFnSc6	zkouška
–	–	k?	–
jeho	jeho	k3xOp3gNnSc4	jeho
udělení	udělení	k1gNnSc4	udělení
tak	tak	k6eAd1	tak
nepředchází	předcházet	k5eNaImIp3nS	předcházet
žádné	žádný	k3yNgNnSc4	žádný
další	další	k2eAgNnSc4d1	další
formální	formální	k2eAgNnSc4d1	formální
studium	studium	k1gNnSc4	studium
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
jak	jak	k6eAd1	jak
RNDr.	RNDr.	kA	RNDr.
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
Mgr.	Mgr.	kA	Mgr.
(	(	kIx(	(
<g/>
magistr	magistr	k1gMnSc1	magistr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
oba	dva	k4xCgMnPc1	dva
označují	označovat	k5eAaImIp3nP	označovat
de	de	k?	de
facto	facto	k1gNnSc4	facto
stejnou	stejný	k2eAgFnSc4d1	stejná
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
(	(	kIx(	(
<g/>
magisterskou	magisterský	k2eAgFnSc4d1	magisterská
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
7	[number]	k4	7
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
degree	degree	k1gFnSc1	degree
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
předmětem	předmět	k1gInSc7	předmět
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
<g/>
Vyšší	vysoký	k2eAgFnSc4d2	vyšší
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
(	(	kIx(	(
<g/>
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
doctor	doctor	k1gInSc1	doctor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gNnSc7	degree
<g/>
)	)	kIx)	)
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
primárně	primárně	k6eAd1	primárně
pro	pro	k7c4	pro
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
činnost	činnost	k1gFnSc4	činnost
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
možno	možno	k6eAd1	možno
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
dalším	další	k1gNnSc7	další
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
letým	letý	k2eAgNnSc7d1	leté
studiem	studio	k1gNnSc7	studio
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
–	–	k?	–
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1990-1998	[number]	k4	1990-1998
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
postgraduální	postgraduální	k2eAgNnSc1d1	postgraduální
studium	studium	k1gNnSc1	studium
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
–	–	k?	–
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
předchozím	předchozí	k2eAgNnSc6d1	předchozí
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
aspiranturu	aspirantura	k1gFnSc4	aspirantura
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kandidát	kandidát	k1gMnSc1	kandidát
věd	věda	k1gFnPc2	věda
–	–	k?	–
CSc.	CSc.	kA	CSc.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
