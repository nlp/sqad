<s>
Ebola	Ebola	k1gFnSc1	Ebola
(	(	kIx(	(
<g/>
též	též	k9	též
krvácivá	krvácivý	k2eAgFnSc1d1	krvácivá
horečka	horečka	k1gFnSc1	horečka
ebola	ebola	k1gFnSc1	ebola
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
virové	virový	k2eAgNnSc1d1	virové
onemocnění	onemocnění	k1gNnSc1	onemocnění
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
krvácivých	krvácivý	k2eAgFnPc2d1	krvácivá
(	(	kIx(	(
<g/>
hemoragických	hemoragický	k2eAgFnPc2d1	hemoragická
<g/>
)	)	kIx)	)
horeček	horečka	k1gFnPc2	horečka
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
napadá	napadat	k5eAaPmIp3nS	napadat
lidi	člověk	k1gMnPc4	člověk
a	a	k8xC	a
některé	některý	k3yIgMnPc4	některý
další	další	k2eAgMnPc4d1	další
primáty	primát	k1gMnPc4	primát
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejnebezpečnějších	bezpečný	k2eNgFnPc2d3	nejnebezpečnější
nákaz	nákaza	k1gFnPc2	nákaza
<g/>
,	,	kIx,	,
s	s	k7c7	s
jakou	jaký	k3yIgFnSc7	jaký
se	se	k3xPyFc4	se
kdy	kdy	k6eAd1	kdy
lidstvo	lidstvo	k1gNnSc1	lidstvo
setkalo	setkat	k5eAaPmAgNnS	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
filovirus	filovirus	k1gMnSc1	filovirus
ebola	ebola	k1gMnSc1	ebola
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
největší	veliký	k2eAgFnSc1d3	veliký
epidemie	epidemie	k1gFnSc1	epidemie
této	tento	k3xDgFnSc2	tento
nemoci	nemoc	k1gFnSc2	nemoc
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
úmrtí	úmrtí	k1gNnSc2	úmrtí
propukla	propuknout	k5eAaPmAgFnS	propuknout
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2013	[number]	k4	2013
a	a	k8xC	a
2014	[number]	k4	2014
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Symptomy	symptom	k1gInPc1	symptom
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
začínají	začínat	k5eAaImIp3nP	začínat
projevovat	projevovat	k5eAaImF	projevovat
dva	dva	k4xCgInPc4	dva
až	až	k9	až
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
nákaze	nákaz	k1gInSc6	nákaz
virem	vir	k1gInSc7	vir
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
horečkou	horečka	k1gFnSc7	horečka
<g/>
,	,	kIx,	,
bolestí	bolest	k1gFnSc7	bolest
v	v	k7c6	v
krku	krk	k1gInSc6	krk
<g/>
,	,	kIx,	,
bolestmi	bolest	k1gFnPc7	bolest
svalů	sval	k1gInPc2	sval
a	a	k8xC	a
bolestí	bolest	k1gFnPc2	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
typicky	typicky	k6eAd1	typicky
následuje	následovat	k5eAaImIp3nS	následovat
nevolnost	nevolnost	k1gFnSc1	nevolnost
<g/>
,	,	kIx,	,
zvracení	zvracení	k1gNnSc1	zvracení
a	a	k8xC	a
průjem	průjem	k1gInSc1	průjem
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	s	k7c7	s
zhoršeným	zhoršený	k2eAgNnSc7d1	zhoršené
fungováním	fungování	k1gNnSc7	fungování
jater	játra	k1gNnPc2	játra
a	a	k8xC	a
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
začínají	začínat	k5eAaImIp3nP	začínat
mít	mít	k5eAaImF	mít
někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
krvácením	krvácení	k1gNnSc7	krvácení
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
ebola	ebolo	k1gNnSc2	ebolo
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
různé	různý	k2eAgInPc1d1	různý
kmeny	kmen	k1gInPc1	kmen
viru	vir	k1gInSc2	vir
ebola	ebola	k6eAd1	ebola
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
epidemie	epidemie	k1gFnPc1	epidemie
s	s	k7c7	s
mírou	míra	k1gFnSc7	míra
letality	letalita	k1gFnSc2	letalita
dosahující	dosahující	k2eAgFnSc2d1	dosahující
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Místem	místo	k1gNnSc7	místo
těchto	tento	k3xDgFnPc2	tento
epidemií	epidemie	k1gFnPc2	epidemie
byly	být	k5eAaImAgFnP	být
především	především	k9	především
africké	africký	k2eAgFnPc1d1	africká
země	zem	k1gFnPc1	zem
jako	jako	k8xS	jako
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
Republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
Gabon	Gabon	k1gNnSc1	Gabon
<g/>
,	,	kIx,	,
Uganda	Uganda	k1gFnSc1	Uganda
a	a	k8xC	a
Súdán	Súdán	k1gInSc1	Súdán
<g/>
,	,	kIx,	,
během	během	k7c2	během
žádné	žádný	k3yNgFnSc2	žádný
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
epidemií	epidemie	k1gFnPc2	epidemie
nebylo	být	k5eNaImAgNnS	být
nakaženo	nakazit	k5eAaPmNgNnS	nakazit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
500	[number]	k4	500
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
epidemie	epidemie	k1gFnSc1	epidemie
v	v	k7c6	v
západoafrické	západoafrický	k2eAgFnSc6d1	západoafrická
Guineji	Guinea	k1gFnSc6	Guinea
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
i	i	k9	i
do	do	k7c2	do
sousední	sousední	k2eAgFnSc2d1	sousední
Libérie	Libérie	k1gFnSc2	Libérie
a	a	k8xC	a
Sierry	Sierra	k1gFnSc2	Sierra
Leone	Leo	k1gMnSc5	Leo
i	i	k9	i
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
Nigérie	Nigérie	k1gFnSc2	Nigérie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2014	[number]	k4	2014
počet	počet	k1gInSc4	počet
nakažených	nakažený	k2eAgMnPc2d1	nakažený
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
7	[number]	k4	7
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
počet	počet	k1gInSc1	počet
mrtvých	mrtvý	k1gMnPc2	mrtvý
3	[number]	k4	3
500	[number]	k4	500
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
5	[number]	k4	5
kmenů	kmen	k1gInPc2	kmen
viru	vir	k1gInSc2	vir
ebola	ebola	k6eAd1	ebola
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
(	(	kIx(	(
<g/>
ebola-Zair	ebola-Zaira	k1gFnPc2	ebola-Zaira
<g/>
,	,	kIx,	,
-Súdán	-Súdán	k2eAgInSc1d1	-Súdán
a	a	k8xC	a
-Bundibugyo	-Bundibugyo	k6eAd1	-Bundibugyo
<g/>
)	)	kIx)	)
napadají	napadat	k5eAaBmIp3nP	napadat
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
kmen	kmen	k1gInSc1	kmen
(	(	kIx(	(
<g/>
ebola-Pobřeží	ebola-Pobřeží	k1gNnSc1	ebola-Pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Taï	Taï	k1gMnSc1	Taï
Forest	Forest	k1gMnSc1	Forest
ebolavirus	ebolavirus	k1gMnSc1	ebolavirus
<g/>
)	)	kIx)	)
dosud	dosud	k6eAd1	dosud
způsobil	způsobit	k5eAaPmAgMnS	způsobit
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc4	jeden
zaznamenané	zaznamenaný	k2eAgNnSc4d1	zaznamenané
lidské	lidský	k2eAgNnSc4d1	lidské
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
když	když	k8xS	když
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
choroby	choroba	k1gFnSc2	choroba
z	z	k7c2	z
pitvaného	pitvaný	k2eAgMnSc2d1	pitvaný
šimpanze	šimpanz	k1gMnSc2	šimpanz
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
smrtelné	smrtelný	k2eAgNnSc1d1	smrtelné
<g/>
.	.	kIx.	.
</s>
<s>
Pátý	pátý	k4xOgInSc1	pátý
kmen	kmen	k1gInSc1	kmen
(	(	kIx(	(
<g/>
ebola-Reston	ebola-Reston	k1gInSc1	ebola-Reston
<g/>
)	)	kIx)	)
napadá	napadat	k5eAaBmIp3nS	napadat
opice	opice	k1gFnPc4	opice
a	a	k8xC	a
prasata	prase	k1gNnPc4	prase
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
ebola	ebola	k6eAd1	ebola
bylo	být	k5eAaImAgNnS	být
převzato	převzít	k5eAaPmNgNnS	převzít
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
oblastí	oblast	k1gFnSc7	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
propukla	propuknout	k5eAaPmAgFnS	propuknout
první	první	k4xOgFnPc4	první
epidemie	epidemie	k1gFnPc4	epidemie
této	tento	k3xDgFnSc2	tento
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
Ebola	Ebolo	k1gNnSc2	Ebolo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
rodiny	rodina	k1gFnSc2	rodina
filovirů	filovir	k1gInPc2	filovir
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
řadí	řadit	k5eAaImIp3nS	řadit
i	i	k9	i
Virus	virus	k1gInSc1	virus
Marburg	Marburg	k1gInSc1	Marburg
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
Ebola	Ebolo	k1gNnSc2	Ebolo
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
blízkosti	blízkost	k1gFnSc6	blízkost
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
zaznamenán	zaznamenán	k2eAgInSc1d1	zaznamenán
výskyt	výskyt	k1gInSc1	výskyt
tohoto	tento	k3xDgInSc2	tento
viru	vir	k1gInSc2	vir
doktorem	doktor	k1gMnSc7	doktor
Ngoy	Ngoy	k1gInPc7	Ngoy
Musholaou	Musholaá	k1gFnSc4	Musholaá
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Yambuku	Yambuk	k1gInSc2	Yambuk
(	(	kIx(	(
<g/>
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
kmen	kmen	k1gInSc1	kmen
Ebola-Zair	Ebola-Zair	k1gInSc1	Ebola-Zair
<g/>
,	,	kIx,	,
Ebola-Z	Ebola-Z	k1gFnSc1	Ebola-Z
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgInSc1d1	další
výskyt	výskyt	k1gInSc1	výskyt
byl	být	k5eAaImAgInS	být
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Nzara	Nzar	k1gInSc2	Nzar
<g/>
,	,	kIx,	,
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
Súdánu	Súdán	k1gInSc6	Súdán
(	(	kIx(	(
<g/>
kmen	kmen	k1gInSc1	kmen
Ebola-Súdán	Ebola-Súdán	k2eAgInSc1d1	Ebola-Súdán
<g/>
,	,	kIx,	,
Ebola-S	Ebola-S	k1gMnSc1	Ebola-S
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
602	[number]	k4	602
identifikovaných	identifikovaný	k2eAgInPc2d1	identifikovaný
případů	případ	k1gInPc2	případ
bylo	být	k5eAaImAgNnS	být
397	[number]	k4	397
smrtelných	smrtelný	k2eAgFnPc2d1	smrtelná
<g/>
.	.	kIx.	.
</s>
<s>
Kmen	kmen	k1gInSc4	kmen
viru	vir	k1gInSc2	vir
Sudan	sudan	k1gInSc1	sudan
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
menší	malý	k2eAgFnSc4d2	menší
míru	míra	k1gFnSc4	míra
smrtnosti	smrtnost	k1gFnSc2	smrtnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kolem	kolem	k7c2	kolem
50	[number]	k4	50
%	%	kIx~	%
<g/>
;	;	kIx,	;
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
90	[number]	k4	90
%	%	kIx~	%
kmenu	kmen	k1gInSc2	kmen
Ebola	Ebolo	k1gNnSc2	Ebolo
Zaire	Zair	k1gInSc5	Zair
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
identifikován	identifikovat	k5eAaBmNgInS	identifikovat
další	další	k2eAgInSc1d1	další
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc1	třetí
kmen	kmen	k1gInSc1	kmen
viru	vir	k1gInSc2	vir
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Reston	Reston	k1gInSc4	Reston
<g/>
,	,	kIx,	,
Virginia	Virginium	k1gNnPc4	Virginium
mezi	mezi	k7c7	mezi
opicemi	opice	k1gFnPc7	opice
importovanými	importovaný	k2eAgFnPc7d1	importovaná
z	z	k7c2	z
Filipín	Filipíny	k1gFnPc2	Filipíny
<g/>
,	,	kIx,	,
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
byl	být	k5eAaImAgMnS	být
Ebola	Ebola	k1gMnSc1	Ebola
<g/>
–	–	k?	–
<g/>
Reston	Reston	k1gInSc1	Reston
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
kmenu	kmen	k1gInSc2	kmen
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dokáže	dokázat	k5eAaPmIp3nS	dokázat
šířit	šířit	k5eAaImF	šířit
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
4	[number]	k4	4
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
vykazovali	vykazovat	k5eAaImAgMnP	vykazovat
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
testy	test	k1gInPc4	test
na	na	k7c4	na
virus	virus	k1gInSc4	virus
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
neonemocněl	onemocnět	k5eNaPmAgInS	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
kmen	kmen	k1gInSc1	kmen
nezpůsobuje	způsobovat	k5eNaImIp3nS	způsobovat
žádné	žádný	k3yNgInPc4	žádný
fatální	fatální	k2eAgInPc4d1	fatální
následky	následek	k1gInPc4	následek
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
epidemie	epidemie	k1gFnSc1	epidemie
tohoto	tento	k3xDgInSc2	tento
viru	vir	k1gInSc2	vir
byla	být	k5eAaImAgNnP	být
zaznamenána	zaznamenat	k5eAaPmNgNnP	zaznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
mezi	mezi	k7c7	mezi
prasaty	prase	k1gNnPc7	prase
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
a	a	k8xC	a
následně	následně	k6eAd1	následně
u	u	k7c2	u
6	[number]	k4	6
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
jatek	jatka	k1gFnPc2	jatka
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
nakažených	nakažený	k2eAgMnPc2d1	nakažený
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
však	však	k9	však
neonemocněl	onemocnět	k5eNaPmAgMnS	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Ebola	Ebola	k1gFnSc1	Ebola
znova	znova	k6eAd1	znova
propukla	propuknout	k5eAaPmAgFnS	propuknout
v	v	k7c6	v
Demokratické	demokratický	k2eAgFnSc6d1	demokratická
republice	republika	k1gFnSc6	republika
Kongo	Kongo	k1gNnSc4	Kongo
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
a	a	k8xC	a
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Gabonu	Gabon	k1gInSc6	Gabon
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
a	a	k8xC	a
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ugandě	Uganda	k1gFnSc6	Uganda
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
a	a	k8xC	a
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
a	a	k8xC	a
v	v	k7c6	v
Republice	republika	k1gFnSc6	republika
Kongo	Kongo	k1gNnSc1	Kongo
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
a	a	k8xC	a
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
podtyp	podtyp	k1gInSc1	podtyp
byl	být	k5eAaImAgMnS	být
izolován	izolovat	k5eAaBmNgMnS	izolovat
z	z	k7c2	z
vědce	vědec	k1gMnSc2	vědec
v	v	k7c6	v
Pobřeží	pobřeží	k1gNnSc6	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
EBO	EBO	kA	EBO
<g/>
–	–	k?	–
<g/>
CI	ci	k0	ci
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
propukla	propuknout	k5eAaPmAgFnS	propuknout
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
epidemie	epidemie	k1gFnSc1	epidemie
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazení	zobrazení	k1gNnSc4	zobrazení
viru	vir	k1gInSc2	vir
Ebola	Ebolo	k1gNnSc2	Ebolo
elektronovým	elektronový	k2eAgInSc7d1	elektronový
mikroskopem	mikroskop	k1gInSc7	mikroskop
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
vláknovitý	vláknovitý	k2eAgInSc4d1	vláknovitý
charakter	charakter	k1gInSc4	charakter
viru	vir	k1gInSc2	vir
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
typický	typický	k2eAgInSc1d1	typický
pro	pro	k7c4	pro
filoviry	filovira	k1gFnPc4	filovira
<g/>
.	.	kIx.	.
</s>
<s>
Virová	virový	k2eAgNnPc1d1	virové
vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
tvarována	tvarován	k2eAgFnSc1d1	tvarována
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
podob	podoba	k1gFnPc2	podoba
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
podoby	podoba	k1gFnSc2	podoba
"	"	kIx"	"
<g/>
U	u	k7c2	u
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
6	[number]	k4	6
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
svinuté	svinutý	k2eAgFnSc2d1	svinutá
podoby	podoba	k1gFnSc2	podoba
nebo	nebo	k8xC	nebo
i	i	k9	i
tzv.	tzv.	kA	tzv.
rozvětvené	rozvětvený	k2eAgFnSc2d1	rozvětvená
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnPc1	pozorování
a	a	k8xC	a
měření	měření	k1gNnPc1	měření
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlákna	vlákno	k1gNnPc1	vlákno
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
80	[number]	k4	80
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
vlákna	vlákno	k1gNnSc2	vlákno
je	být	k5eAaImIp3nS	být
však	však	k9	však
velice	velice	k6eAd1	velice
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
vlákna	vlákna	k1gFnSc1	vlákna
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
až	až	k9	až
14	[number]	k4	14
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
obrázek	obrázek	k1gInSc1	obrázek
viru	vir	k1gInSc2	vir
Ebola	Ebolo	k1gNnSc2	Ebolo
byl	být	k5eAaImAgInS	být
získán	získat	k5eAaPmNgInS	získat
pomocí	pomocí	k7c2	pomocí
elektronového	elektronový	k2eAgInSc2d1	elektronový
mikroskopu	mikroskop	k1gInSc2	mikroskop
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
doktorem	doktor	k1gMnSc7	doktor
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
Murphym	Murphym	k1gInSc1	Murphym
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
výzkumném	výzkumný	k2eAgNnSc6d1	výzkumné
středisku	středisko	k1gNnSc6	středisko
CDC	CDC	kA	CDC
(	(	kIx(	(
<g/>
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
chorob	choroba	k1gFnPc2	choroba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ebola	Ebola	k1gFnSc1	Ebola
<g/>
–	–	k?	–
<g/>
Zaire	Zair	k1gInSc5	Zair
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc1	první
objevený	objevený	k2eAgInSc1d1	objevený
a	a	k8xC	a
nejnebezpečnější	bezpečný	k2eNgInSc1d3	nejnebezpečnější
kmen	kmen	k1gInSc1	kmen
eboly	ebola	k1gFnSc2	ebola
se	s	k7c7	s
smrtností	smrtnost	k1gFnSc7	smrtnost
až	až	k9	až
90	[number]	k4	90
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
největší	veliký	k2eAgInSc4d3	veliký
počet	počet	k1gInSc4	počet
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Yambuku	Yambuk	k1gInSc2	Yambuk
(	(	kIx(	(
<g/>
Zaire	Zair	k1gInSc5	Zair
–	–	k?	–
nyní	nyní	k6eAd1	nyní
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc4	Kongo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pacient	pacient	k1gMnSc1	pacient
Mabelo	Mabela	k1gFnSc5	Mabela
Lokela	Lokela	k1gFnSc1	Lokela
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
vesnické	vesnický	k2eAgFnSc2d1	vesnická
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
kontrolu	kontrola	k1gFnSc4	kontrola
do	do	k7c2	do
místní	místní	k2eAgFnSc2d1	místní
misionářské	misionářský	k2eAgFnSc2d1	misionářská
nemocnice	nemocnice	k1gFnSc2	nemocnice
s	s	k7c7	s
horečkou	horečka	k1gFnSc7	horečka
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
sester	sestra	k1gFnPc2	sestra
pracující	pracující	k2eAgMnSc1d1	pracující
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
nemocnici	nemocnice	k1gFnSc6	nemocnice
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
malárii	malárie	k1gFnSc4	malárie
<g/>
,	,	kIx,	,
a	a	k8xC	a
dala	dát	k5eAaPmAgFnS	dát
mu	on	k3xPp3gMnSc3	on
injekci	injekce	k1gFnSc6	injekce
s	s	k7c7	s
chininem	chinin	k1gInSc7	chinin
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Lokela	Lokel	k1gMnSc2	Lokel
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
nemocnice	nemocnice	k1gFnSc2	nemocnice
zpět	zpět	k6eAd1	zpět
domů	dům	k1gInPc2	dům
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
rodné	rodný	k2eAgFnSc2d1	rodná
vesnice	vesnice	k1gFnSc2	vesnice
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
uspořádaly	uspořádat	k5eAaPmAgInP	uspořádat
tradiční	tradiční	k2eAgInSc4d1	tradiční
africký	africký	k2eAgInSc4d1	africký
pohřeb	pohřeb	k1gInSc4	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přípravě	příprava	k1gFnSc6	příprava
na	na	k7c4	na
pohřeb	pohřeb	k1gInSc4	pohřeb
odstranily	odstranit	k5eAaPmAgFnP	odstranit
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
těla	tělo	k1gNnSc2	tělo
veškerou	veškerý	k3xTgFnSc4	veškerý
krev	krev	k1gFnSc4	krev
a	a	k8xC	a
exkrementy	exkrement	k1gInPc4	exkrement
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
holýma	holý	k2eAgFnPc7d1	holá
rukama	ruka	k1gFnPc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
sestry	sestra	k1gFnPc1	sestra
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
měly	mít	k5eAaImAgInP	mít
epidemii	epidemie	k1gFnSc4	epidemie
Eboly	Ebol	k1gInPc1	Ebol
tzv.	tzv.	kA	tzv.
přímo	přímo	k6eAd1	přímo
pod	pod	k7c7	pod
rukama	ruka	k1gFnPc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Injekční	injekční	k2eAgFnSc1d1	injekční
stříkačka	stříkačka	k1gFnSc1	stříkačka
použitá	použitý	k2eAgFnSc1d1	použitá
pro	pro	k7c4	pro
chininovou	chininový	k2eAgFnSc4d1	chininová
injekci	injekce	k1gFnSc4	injekce
pro	pro	k7c4	pro
Lokelu	Lokela	k1gFnSc4	Lokela
byla	být	k5eAaImAgFnS	být
nedostatečně	dostatečně	k6eNd1	dostatečně
sterilizována	sterilizován	k2eAgFnSc1d1	sterilizována
<g/>
,	,	kIx,	,
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
byly	být	k5eAaImAgFnP	být
injekční	injekční	k2eAgFnPc1d1	injekční
stříkačky	stříkačka	k1gFnPc1	stříkačka
často	často	k6eAd1	často
znovupoužívány	znovupoužívat	k5eAaImNgFnP	znovupoužívat
bez	bez	k7c2	bez
sterilizace	sterilizace	k1gFnSc2	sterilizace
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
se	se	k3xPyFc4	se
virus	virus	k1gInSc1	virus
Ebola	Ebolo	k1gNnSc2	Ebolo
šířit	šířit	k5eAaImF	šířit
z	z	k7c2	z
pacienta	pacient	k1gMnSc2	pacient
na	na	k7c4	na
pacienta	pacient	k1gMnSc4	pacient
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
epidemie	epidemie	k1gFnSc2	epidemie
viru	vir	k1gInSc2	vir
Ebola	Ebolo	k1gNnSc2	Ebolo
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
neznámé	známý	k2eNgFnSc2d1	neznámá
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
šířila	šířit	k5eAaImAgFnS	šířit
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
povolán	povolat	k5eAaPmNgMnS	povolat
doktor	doktor	k1gMnSc1	doktor
Ngoi	Ngo	k1gFnSc2	Ngo
Mushola	Mushola	k1gFnSc1	Mushola
<g/>
,	,	kIx,	,
oblastní	oblastní	k2eAgMnSc1d1	oblastní
ředitel	ředitel	k1gMnSc1	ředitel
pro	pro	k7c4	pro
zdravotnictví	zdravotnictví	k1gNnSc4	zdravotnictví
<g/>
.	.	kIx.	.
</s>
<s>
Vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
správně	správně	k6eAd1	správně
sterilizovat	sterilizovat	k5eAaImF	sterilizovat
injekční	injekční	k2eAgFnPc1d1	injekční
stříkačky	stříkačka	k1gFnPc1	stříkačka
a	a	k8xC	a
jak	jak	k8xC	jak
správně	správně	k6eAd1	správně
čistit	čistit	k5eAaImF	čistit
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k6eAd1	též
je	být	k5eAaImIp3nS	být
instruoval	instruovat	k5eAaBmAgMnS	instruovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
rodinám	rodina	k1gFnPc3	rodina
pacientů	pacient	k1gMnPc2	pacient
vysvětlily	vysvětlit	k5eAaPmAgInP	vysvětlit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mrtvoly	mrtvola	k1gFnPc1	mrtvola
nesmí	smět	k5eNaImIp3nP	smět
být	být	k5eAaImF	být
pohřbívány	pohřbívat	k5eAaImNgInP	pohřbívat
přímo	přímo	k6eAd1	přímo
uvnitř	uvnitř	k7c2	uvnitř
domu	dům	k1gInSc2	dům
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
přímé	přímý	k2eAgFnSc6d1	přímá
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
to	ten	k3xDgNnSc1	ten
místní	místní	k2eAgFnPc1d1	místní
tradice	tradice	k1gFnPc1	tradice
nařizovaly	nařizovat	k5eAaImAgFnP	nařizovat
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k9	též
informoval	informovat	k5eAaBmAgMnS	informovat
úřady	úřad	k1gInPc7	úřad
v	v	k7c6	v
Kinshase	Kinshasa	k1gFnSc6	Kinshasa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
byli	být	k5eAaImAgMnP	být
posláni	poslat	k5eAaPmNgMnP	poslat
mikrobiologové	mikrobiolog	k1gMnPc1	mikrobiolog
a	a	k8xC	a
epidemiologové	epidemiolog	k1gMnPc1	epidemiolog
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
provedli	provést	k5eAaPmAgMnP	provést
pitvy	pitva	k1gFnPc4	pitva
zemřelých	zemřelý	k2eAgMnPc2d1	zemřelý
pacientů	pacient	k1gMnPc2	pacient
a	a	k8xC	a
shromažďovali	shromažďovat	k5eAaImAgMnP	shromažďovat
vzorky	vzorek	k1gInPc4	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
oblasti	oblast	k1gFnSc6	oblast
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
karanténa	karanténa	k1gFnSc1	karanténa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k8xC	jako
správné	správný	k2eAgNnSc4d1	správné
řešení	řešení	k1gNnSc4	řešení
a	a	k8xC	a
šíření	šíření	k1gNnSc4	šíření
viru	vir	k1gInSc2	vir
Ebola	Ebol	k1gMnSc2	Ebol
se	se	k3xPyFc4	se
tak	tak	k9	tak
dostalo	dostat	k5eAaPmAgNnS	dostat
pod	pod	k7c4	pod
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
karanténa	karanténa	k1gFnSc1	karanténa
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
znamenala	znamenat	k5eAaImAgFnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
izolována	izolovat	k5eAaBmNgFnS	izolovat
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
všichni	všechen	k3xTgMnPc1	všechen
infikovaní	infikovaný	k2eAgMnPc1d1	infikovaný
pacienti	pacient	k1gMnPc1	pacient
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Doktor	doktor	k1gMnSc1	doktor
Ngwete	Ngwe	k1gNnSc2	Ngwe
Kikhela	Kikhela	k1gFnSc1	Kikhela
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
,	,	kIx,	,
též	též	k9	též
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
chorob	choroba	k1gFnPc2	choroba
(	(	kIx(	(
<g/>
CDC	CDC	kA	CDC
<g/>
)	)	kIx)	)
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
CDC	CDC	kA	CDC
informovalo	informovat	k5eAaBmAgNnS	informovat
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
komunitu	komunita	k1gFnSc4	komunita
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
epidemii	epidemie	k1gFnSc6	epidemie
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc6	jejich
možných	možný	k2eAgInPc6d1	možný
následcích	následek	k1gInPc6	následek
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
viru	vir	k1gInSc2	vir
<g/>
:	:	kIx,	:
rok	rok	k1gInSc1	rok
1976	[number]	k4	1976
<g/>
–	–	k?	–
<g/>
318	[number]	k4	318
nemocných	nemocný	k1gMnPc2	nemocný
<g/>
/	/	kIx~	/
<g/>
208	[number]	k4	208
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
–	–	k?	–
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
–	–	k?	–
49	[number]	k4	49
<g/>
/	/	kIx~	/
<g/>
29	[number]	k4	29
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
–	–	k?	–
315	[number]	k4	315
<g/>
/	/	kIx~	/
<g/>
256	[number]	k4	256
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
–	–	k?	–
93	[number]	k4	93
<g/>
/	/	kIx~	/
<g/>
68	[number]	k4	68
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
–	–	k?	–
122	[number]	k4	122
<g/>
/	/	kIx~	/
<g/>
97	[number]	k4	97
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
–	–	k?	–
142	[number]	k4	142
<g/>
/	/	kIx~	/
<g/>
128	[number]	k4	128
<g/>
.	.	kIx.	.
</s>
<s>
Smrtnost	smrtnost	k1gFnSc1	smrtnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
%	%	kIx~	%
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
podkmenech	podkmen	k1gInPc6	podkmen
viru	vir	k1gInSc2	vir
<g/>
.	.	kIx.	.
</s>
<s>
Přirozený	přirozený	k2eAgInSc1d1	přirozený
rezervoár	rezervoár	k1gInSc1	rezervoár
je	být	k5eAaImIp3nS	být
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
,	,	kIx,	,
podezřívány	podezříván	k2eAgFnPc1d1	podezřívána
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgFnPc1	některý
antilopy	antilopa	k1gFnPc1	antilopa
či	či	k8xC	či
netopýři	netopýr	k1gMnPc1	netopýr
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
propuknutí	propuknutí	k1gNnSc6	propuknutí
nákazy	nákaza	k1gFnSc2	nákaza
v	v	k7c6	v
Zairu	Zair	k1gInSc6	Zair
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
podobná	podobný	k2eAgFnSc1d1	podobná
epidemie	epidemie	k1gFnSc1	epidemie
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Nzara	Nzaro	k1gNnSc2	Nzaro
a	a	k8xC	a
Maridi	Marid	k1gMnPc1	Marid
v	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
případ	případ	k1gInSc1	případ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vyskytl	vyskytnout	k5eAaPmAgInS	vyskytnout
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Nzara	Nzar	k1gInSc2	Nzar
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgMnS	týkat
pracovníka	pracovník	k1gMnSc4	pracovník
místního	místní	k2eAgInSc2d1	místní
závodu	závod	k1gInSc2	závod
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
továrna	továrna	k1gFnSc1	továrna
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
místo	místo	k1gNnSc4	místo
výskytu	výskyt	k1gInSc2	výskyt
a	a	k8xC	a
ložisko	ložisko	k1gNnSc4	ložisko
viru	vir	k1gInSc2	vir
Ebola	Ebolo	k1gNnSc2	Ebolo
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
bylo	být	k5eAaImAgNnS	být
otestováno	otestovat	k5eAaPmNgNnS	otestovat
mnoho	mnoho	k4c1	mnoho
živočichů	živočich	k1gMnPc2	živočich
zde	zde	k6eAd1	zde
žijících	žijící	k2eAgMnPc2d1	žijící
<g/>
,	,	kIx,	,
od	od	k7c2	od
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
,	,	kIx,	,
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
krys	krysa	k1gFnPc2	krysa
až	až	k8xS	až
po	po	k7c4	po
netopýry	netopýr	k1gMnPc4	netopýr
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc1	žádný
z	z	k7c2	z
testu	test	k1gInSc2	test
nic	nic	k3yNnSc1	nic
neprokázal	prokázat	k5eNaPmAgMnS	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
zásobárna	zásobárna	k1gFnSc1	zásobárna
a	a	k8xC	a
ložisko	ložisko	k1gNnSc1	ložisko
viru	vir	k1gInSc2	vir
Ebola-Súdán	Ebola-Súdán	k2eAgMnSc1d1	Ebola-Súdán
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
neznámé	známý	k2eNgNnSc1d1	neznámé
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
případem	případ	k1gInSc7	případ
byla	být	k5eAaImAgFnS	být
smrt	smrt	k1gFnSc1	smrt
vlastníka	vlastník	k1gMnSc2	vlastník
nočního	noční	k2eAgInSc2d1	noční
klubu	klub	k1gInSc2	klub
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Nzara	Nzar	k1gInSc2	Nzar
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgInS	moct
dovolit	dovolit	k5eAaPmF	dovolit
lepší	dobrý	k2eAgFnSc4d2	lepší
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
péči	péče	k1gFnSc4	péče
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Maridi	Marid	k1gMnPc1	Marid
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
sestry	sestra	k1gFnPc1	sestra
nedostatečně	dostatečně	k6eNd1	dostatečně
sterilizovaly	sterilizovat	k5eAaImAgFnP	sterilizovat
injekční	injekční	k2eAgFnPc1d1	injekční
stříkačky	stříkačka	k1gFnPc1	stříkačka
<g/>
,	,	kIx,	,
a	a	k8xC	a
nemocnice	nemocnice	k1gFnPc1	nemocnice
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ta	ten	k3xDgFnSc1	ten
v	v	k7c6	v
Yambuku	Yambuk	k1gInSc6	Yambuk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
líhní	líheň	k1gFnSc7	líheň
pro	pro	k7c4	pro
nové	nový	k2eAgInPc4d1	nový
případy	případ	k1gInPc4	případ
onemocnění	onemocnění	k1gNnSc2	onemocnění
virem	vir	k1gInSc7	vir
Ebola	Ebol	k1gMnSc2	Ebol
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
propuknula	propuknout	k5eAaPmAgFnS	propuknout
Ebola-Sudan	Ebola-Sudan	k1gInSc4	Ebola-Sudan
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
nakazilo	nakazit	k5eAaPmAgNnS	nakazit
jen	jen	k9	jen
v	v	k7c6	v
okrsku	okrsek	k1gInSc6	okrsek
Yambio	Yambio	k6eAd1	Yambio
20	[number]	k4	20
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
5	[number]	k4	5
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
CDC	CDC	kA	CDC
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
již	již	k6eAd1	již
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
Ebola	Ebola	k1gMnSc1	Ebola
<g/>
–	–	k?	–
<g/>
Sudan	sudan	k1gInSc1	sudan
<g/>
.	.	kIx.	.
</s>
<s>
Kmen	kmen	k1gInSc1	kmen
ebola-Súdán	ebola-Súdán	k2eAgInSc1d1	ebola-Súdán
je	být	k5eAaImIp3nS	být
vzácnější	vzácný	k2eAgInSc1d2	vzácnější
a	a	k8xC	a
méně	málo	k6eAd2	málo
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
třikrát	třikrát	k6eAd1	třikrát
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
–	–	k?	–
285	[number]	k4	285
nakažených	nakažený	k2eAgMnPc2d1	nakažený
<g/>
/	/	kIx~	/
<g/>
151	[number]	k4	151
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
–	–	k?	–
34	[number]	k4	34
<g/>
/	/	kIx~	/
<g/>
23	[number]	k4	23
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
425	[number]	k4	425
<g/>
/	/	kIx~	/
<g/>
226	[number]	k4	226
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
onemocnělo	onemocnět	k5eAaPmAgNnS	onemocnět
744	[number]	k4	744
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
400	[number]	k4	400
<g/>
.	.	kIx.	.
</s>
<s>
Smrtnost	smrtnost	k1gFnSc1	smrtnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
60	[number]	k4	60
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
makakové	makakové	k?	makakové
zanesli	zanést	k5eAaPmAgMnP	zanést
nový	nový	k2eAgInSc4d1	nový
kmen	kmen	k1gInSc4	kmen
viru	vir	k1gInSc2	vir
Ebola	Ebolo	k1gNnSc2	Ebolo
do	do	k7c2	do
karanténního	karanténní	k2eAgNnSc2d1	karanténní
zařízení	zařízení	k1gNnSc2	zařízení
pro	pro	k7c4	pro
opice	opice	k1gFnPc4	opice
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Reston	Reston	k1gInSc1	Reston
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
čtyř	čtyři	k4xCgMnPc2	čtyři
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
tohoto	tento	k3xDgNnSc2	tento
zařízení	zařízení	k1gNnSc2	zařízení
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
protilátky	protilátka	k1gFnPc1	protilátka
z	z	k7c2	z
nakažení	nakažení	k1gNnSc2	nakažení
virem	vir	k1gInSc7	vir
Ebola	Ebol	k1gMnSc2	Ebol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
neonemocněl	onemocnět	k5eNaPmAgMnS	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Ebola	Ebola	k1gFnSc1	Ebola
<g/>
–	–	k?	–
<g/>
Reston	Reston	k1gInSc1	Reston
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pravidelně	pravidelně	k6eAd1	pravidelně
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
jako	jako	k8xC	jako
nákaza	nákaza	k1gFnSc1	nákaza
opic	opice	k1gFnPc2	opice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádný	žádný	k3yNgMnSc1	žádný
člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
nenakazil	nakazit	k5eNaPmAgInS	nakazit
<g/>
,	,	kIx,	,
přesný	přesný	k2eAgInSc4d1	přesný
důvod	důvod	k1gInSc4	důvod
lidské	lidský	k2eAgFnSc2d1	lidská
imunity	imunita	k1gFnSc2	imunita
je	být	k5eAaImIp3nS	být
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
přírodní	přírodní	k2eAgInSc4d1	přírodní
rezervoár	rezervoár	k1gInSc4	rezervoár
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
makak	makak	k1gMnSc1	makak
a	a	k8xC	a
kočkodan	kočkodan	k1gMnSc1	kočkodan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
virus	virus	k1gInSc1	virus
Ebola	Ebol	k1gMnSc2	Ebol
<g/>
–	–	k?	–
<g/>
Reston	Reston	k1gInSc1	Reston
nalezen	nalezen	k2eAgMnSc1d1	nalezen
v	v	k7c6	v
uhynulých	uhynulý	k2eAgNnPc6d1	uhynulé
filipínských	filipínský	k2eAgNnPc6d1	filipínské
prasatech	prase	k1gNnPc6	prase
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
pracovníci	pracovník	k1gMnPc1	pracovník
chovu	chov	k1gInSc2	chov
proti	proti	k7c3	proti
viru	vir	k1gInSc3	vir
protilátky	protilátka	k1gFnSc2	protilátka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
vědec	vědec	k1gMnSc1	vědec
pracující	pracující	k1gMnSc1	pracující
s	s	k7c7	s
virem	vir	k1gInSc7	vir
Ebola	Ebola	k1gMnSc1	Ebola
nakazil	nakazit	k5eAaPmAgMnS	nakazit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
prováděl	provádět	k5eAaImAgMnS	provádět
pitvu	pitva	k1gFnSc4	pitva
na	na	k7c6	na
divokém	divoký	k2eAgMnSc6d1	divoký
šimpanzovi	šimpanz	k1gMnSc6	šimpanz
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
mělo	mít	k5eAaImAgNnS	mít
mírnou	mírný	k2eAgFnSc4d1	mírná
formu	forma	k1gFnSc4	forma
a	a	k8xC	a
vědec	vědec	k1gMnSc1	vědec
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
uzdravil	uzdravit	k5eAaPmAgMnS	uzdravit
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nedostatku	nedostatek	k1gInSc3	nedostatek
dat	datum	k1gNnPc2	datum
nejsou	být	k5eNaImIp3nP	být
o	o	k7c6	o
kmenu	kmen	k1gInSc6	kmen
známy	znám	k2eAgFnPc1d1	známa
bližší	blízký	k2eAgFnPc4d2	bližší
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
v	v	k7c6	v
Ugandě	Uganda	k1gFnSc6	Uganda
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
Bundibugyo	Bundibugyo	k1gMnSc1	Bundibugyo
<g/>
)	)	kIx)	)
objevil	objevit	k5eAaPmAgInS	objevit
nový	nový	k2eAgInSc1d1	nový
typ	typ	k1gInSc1	typ
viru	vir	k1gInSc2	vir
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
způsobil	způsobit	k5eAaPmAgInS	způsobit
přes	přes	k7c4	přes
sto	sto	k4xCgNnSc4	sto
případů	případ	k1gInPc2	případ
nemoci	nemoc	k1gFnSc2	nemoc
a	a	k8xC	a
necelých	celý	k2eNgNnPc2d1	necelé
40	[number]	k4	40
úmrtí	úmrtí	k1gNnPc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
Demokratické	demokratický	k2eAgFnSc6d1	demokratická
republice	republika	k1gFnSc6	republika
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
viru	vir	k1gInSc2	vir
Ebola	Ebolo	k1gNnSc2	Ebolo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
2	[number]	k4	2
dny	den	k1gInPc7	den
–	–	k?	–
3	[number]	k4	3
týdny	týden	k1gInPc4	týden
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
dnů	den	k1gInPc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Symptomy	symptom	k1gInPc1	symptom
se	se	k3xPyFc4	se
též	též	k9	též
liší	lišit	k5eAaImIp3nS	lišit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
počátek	počátek	k1gInSc4	počátek
nákazy	nákaza	k1gFnSc2	nákaza
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
náhlý	náhlý	k2eAgInSc1d1	náhlý
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
vysokými	vysoký	k2eAgFnPc7d1	vysoká
horečkami	horečka	k1gFnPc7	horečka
<g/>
,	,	kIx,	,
skleslostí	skleslost	k1gFnSc7	skleslost
<g/>
,	,	kIx,	,
svalovými	svalový	k2eAgFnPc7d1	svalová
bolestmi	bolest	k1gFnPc7	bolest
<g/>
,	,	kIx,	,
bolestmi	bolest	k1gFnPc7	bolest
v	v	k7c6	v
kloubech	kloub	k1gInPc6	kloub
<g/>
,	,	kIx,	,
bolestmi	bolest	k1gFnPc7	bolest
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
břicha	břich	k1gInSc2	břich
a	a	k8xC	a
bolestmi	bolest	k1gFnPc7	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Častým	častý	k2eAgInSc7d1	častý
symptomem	symptom	k1gInSc7	symptom
je	být	k5eAaImIp3nS	být
zvracení	zvracení	k1gNnSc1	zvracení
<g/>
,	,	kIx,	,
průjem	průjem	k1gInSc1	průjem
<g/>
,	,	kIx,	,
poškození	poškození	k1gNnSc1	poškození
ústní	ústní	k2eAgFnSc2d1	ústní
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
zánět	zánět	k1gInSc4	zánět
spojivek	spojivka	k1gFnPc2	spojivka
<g/>
,	,	kIx,	,
krvácení	krvácení	k1gNnSc4	krvácení
jak	jak	k8xS	jak
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
vnější	vnější	k2eAgFnSc4d1	vnější
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
nejprve	nejprve	k6eAd1	nejprve
skrz	skrz	k7c4	skrz
trávicí	trávicí	k2eAgNnSc4d1	trávicí
ústrojí	ústrojí	k1gNnSc4	ústrojí
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
zaměňováno	zaměňovat	k5eAaImNgNnS	zaměňovat
s	s	k7c7	s
příznaky	příznak	k1gInPc7	příznak
úplavice	úplavice	k1gFnSc2	úplavice
či	či	k8xC	či
tyfu	tyf	k1gInSc2	tyf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
tělesných	tělesný	k2eAgInPc2d1	tělesný
otvorů	otvor	k1gInPc2	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
těžkému	těžký	k2eAgNnSc3d1	těžké
poškození	poškození	k1gNnSc3	poškození
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
jater	játra	k1gNnPc2	játra
(	(	kIx(	(
<g/>
lékaři	lékař	k1gMnPc1	lékař
provádějící	provádějící	k2eAgFnSc4d1	provádějící
pitvu	pitva	k1gFnSc4	pitva
zemřelých	zemřelá	k1gFnPc2	zemřelá
popisovali	popisovat	k5eAaImAgMnP	popisovat
stav	stav	k1gInSc4	stav
jejich	jejich	k3xOp3gNnPc2	jejich
jater	játra	k1gNnPc2	játra
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
jako	jako	k9	jako
namixovaná	namixovaný	k2eAgFnSc1d1	namixovaná
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
či	či	k8xC	či
"	"	kIx"	"
<g/>
krvavá	krvavý	k2eAgFnSc1d1	krvavá
kaše	kaše	k1gFnSc1	kaše
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
přeživších	přeživší	k2eAgMnPc2d1	přeživší
pacientů	pacient	k1gMnPc2	pacient
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
kmenu	kmen	k1gInSc6	kmen
viru	vir	k1gInSc2	vir
a	a	k8xC	a
na	na	k7c6	na
fyzické	fyzický	k2eAgFnSc6d1	fyzická
kondici	kondice	k1gFnSc6	kondice
pacienta	pacient	k1gMnSc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
nebo	nebo	k8xC	nebo
postupné	postupný	k2eAgNnSc1d1	postupné
uzdravování	uzdravování	k1gNnSc1	uzdravování
nastává	nastávat	k5eAaImIp3nS	nastávat
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
viru	vir	k1gInSc2	vir
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
při	při	k7c6	při
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
krví	krev	k1gFnSc7	krev
nebo	nebo	k8xC	nebo
tělními	tělní	k2eAgFnPc7d1	tělní
tekutinami	tekutina	k1gFnPc7	tekutina
nakaženého	nakažený	k2eAgNnSc2d1	nakažené
zvířete	zvíře	k1gNnSc2	zvíře
(	(	kIx(	(
<g/>
běžně	běžně	k6eAd1	běžně
opice	opice	k1gFnSc1	opice
nebo	nebo	k8xC	nebo
kaloni	kaloň	k1gMnPc1	kaloň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šíření	šíření	k1gNnSc1	šíření
vzduchem	vzduch	k1gInSc7	vzduch
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
přirozeném	přirozený	k2eAgNnSc6d1	přirozené
prostředí	prostředí	k1gNnSc6	prostředí
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kaloni	kaloň	k1gMnPc1	kaloň
virus	virus	k1gInSc1	virus
přenášejí	přenášet	k5eAaImIp3nP	přenášet
a	a	k8xC	a
šíří	šířit	k5eAaImIp3nP	šířit
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
je	on	k3xPp3gNnSc4	on
samotné	samotný	k2eAgNnSc4d1	samotné
postihoval	postihovat	k5eAaImAgMnS	postihovat
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
nákaze	nákaza	k1gFnSc3	nákaza
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
onemocnění	onemocnění	k1gNnPc2	onemocnění
šířit	šířit	k5eAaImF	šířit
i	i	k9	i
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nákazu	nákaza	k1gFnSc4	nákaza
přežijí	přežít	k5eAaPmIp3nP	přežít
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
onemocnění	onemocnění	k1gNnSc4	onemocnění
šířit	šířit	k5eAaImF	šířit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
spermatu	sperma	k1gNnSc2	sperma
téměř	téměř	k6eAd1	téměř
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
účelem	účel	k1gInSc7	účel
stanovení	stanovení	k1gNnSc1	stanovení
diagnózy	diagnóza	k1gFnSc2	diagnóza
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nejdříve	dříve	k6eAd3	dříve
vyloučí	vyloučit	k5eAaPmIp3nP	vyloučit
ostatní	ostatní	k2eAgNnPc1d1	ostatní
onemocnění	onemocnění	k1gNnPc1	onemocnění
s	s	k7c7	s
podobnými	podobný	k2eAgInPc7d1	podobný
symptomy	symptom	k1gInPc7	symptom
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
malárie	malárie	k1gFnSc1	malárie
<g/>
,	,	kIx,	,
cholera	cholera	k1gFnSc1	cholera
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
virové	virový	k2eAgFnPc4d1	virová
krvácivé	krvácivý	k2eAgFnPc4d1	krvácivá
horečky	horečka	k1gFnPc4	horečka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
účelem	účel	k1gInSc7	účel
potvrzení	potvrzení	k1gNnSc2	potvrzení
diagnózy	diagnóza	k1gFnSc2	diagnóza
jsou	být	k5eAaImIp3nP	být
krevní	krevní	k2eAgInPc1d1	krevní
vzorky	vzorek	k1gInPc1	vzorek
testovány	testovat	k5eAaImNgInP	testovat
na	na	k7c4	na
virové	virový	k2eAgFnPc4d1	virová
protilátky	protilátka	k1gFnPc4	protilátka
<g/>
,	,	kIx,	,
virovou	virový	k2eAgFnSc4d1	virová
RNA	RNA	kA	RNA
nebo	nebo	k8xC	nebo
virus	virus	k1gInSc1	virus
samotný	samotný	k2eAgInSc1d1	samotný
<g/>
.	.	kIx.	.
</s>
<s>
Pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
virem	vir	k1gInSc7	vir
Ebola	Ebolo	k1gNnSc2	Ebolo
se	se	k3xPyFc4	se
smí	smět	k5eAaImIp3nS	smět
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
samotné	samotný	k2eAgNnSc4d1	samotné
místo	místo	k1gNnSc4	místo
epidemie	epidemie	k1gFnSc2	epidemie
<g/>
)	)	kIx)	)
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
laboratořích	laboratoř	k1gFnPc6	laboratoř
se	s	k7c7	s
4	[number]	k4	4
<g/>
.	.	kIx.	.
úrovní	úroveň	k1gFnSc7	úroveň
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
pracovišť	pracoviště	k1gNnPc2	pracoviště
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
jím	on	k3xPp3gNnSc7	on
Centrum	centrum	k1gNnSc4	centrum
biologické	biologický	k2eAgFnSc2d1	biologická
ochrany	ochrana	k1gFnSc2	ochrana
u	u	k7c2	u
Těchonína	Těchonín	k1gInSc2	Těchonín
v	v	k7c6	v
Orlických	orlický	k2eAgFnPc6d1	Orlická
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Pacienti	pacient	k1gMnPc1	pacient
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
důkladně	důkladně	k6eAd1	důkladně
izolováni	izolován	k2eAgMnPc1d1	izolován
<g/>
,	,	kIx,	,
personál	personál	k1gInSc1	personál
používá	používat	k5eAaImIp3nS	používat
ochranný	ochranný	k2eAgInSc1d1	ochranný
oděv	oděv	k1gInSc1	oděv
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
onemocnění	onemocnění	k1gNnSc3	onemocnění
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
specifická	specifický	k2eAgFnSc1d1	specifická
léčba	léčba	k1gFnSc1	léčba
<g/>
;	;	kIx,	;
mezi	mezi	k7c4	mezi
snahy	snaha	k1gFnPc4	snaha
pomoci	pomoct	k5eAaPmF	pomoct
nakaženým	nakažený	k2eAgFnPc3d1	nakažená
osobám	osoba	k1gFnPc3	osoba
patří	patřit	k5eAaImIp3nS	patřit
buď	buď	k8xC	buď
orální	orální	k2eAgFnSc1d1	orální
rehydratační	rehydratační	k2eAgFnSc1d1	rehydratační
léčba	léčba	k1gFnSc1	léčba
(	(	kIx(	(
<g/>
podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
mírně	mírně	k6eAd1	mírně
sladká	sladký	k2eAgFnSc1d1	sladká
a	a	k8xC	a
slaná	slaný	k2eAgFnSc1d1	slaná
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nitrožilně	nitrožilně	k6eAd1	nitrožilně
podávané	podávaný	k2eAgFnSc2d1	podávaná
tekutiny	tekutina	k1gFnSc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
onemocněním	onemocnění	k1gNnSc7	onemocnění
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
vysoká	vysoký	k2eAgFnSc1d1	vysoká
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
<g/>
:	:	kIx,	:
často	často	k6eAd1	často
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
50	[number]	k4	50
%	%	kIx~	%
až	až	k9	až
90	[number]	k4	90
%	%	kIx~	%
osob	osoba	k1gFnPc2	osoba
nakažených	nakažený	k2eAgMnPc2d1	nakažený
virem	vir	k1gInSc7	vir
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
prakticky	prakticky	k6eAd1	prakticky
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
interferonem	interferon	k1gInSc7	interferon
<g/>
.	.	kIx.	.
</s>
<s>
Účinnost	účinnost	k1gFnSc1	účinnost
všech	všecek	k3xTgFnPc2	všecek
známých	známý	k2eAgFnPc2d1	známá
virostatik	virostatika	k1gFnPc2	virostatika
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
nulová	nulový	k2eAgFnSc1d1	nulová
nebo	nebo	k8xC	nebo
velice	velice	k6eAd1	velice
slabá	slabý	k2eAgFnSc1d1	slabá
<g/>
.	.	kIx.	.
</s>
<s>
Sérum	sérum	k1gNnSc1	sérum
získané	získaný	k2eAgNnSc1d1	získané
z	z	k7c2	z
přeživších	přeživší	k2eAgFnPc2d1	přeživší
nemocných	nemocná	k1gFnPc2	nemocná
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
omezený	omezený	k2eAgInSc1d1	omezený
účinek	účinek	k1gInSc1	účinek
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
uchovávání	uchovávání	k1gNnSc1	uchovávání
jsou	být	k5eAaImIp3nP	být
neúnosně	únosně	k6eNd1	únosně
drahé	drahý	k2eAgInPc1d1	drahý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgNnPc1	tři
léčiva	léčivo	k1gNnPc1	léčivo
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
epidemii	epidemie	k1gFnSc3	epidemie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
americký	americký	k2eAgInSc1d1	americký
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
léčiv	léčivo	k1gNnPc2	léčivo
povolil	povolit	k5eAaPmAgMnS	povolit
použití	použití	k1gNnSc4	použití
dvou	dva	k4xCgInPc2	dva
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
léků	lék	k1gInPc2	lék
u	u	k7c2	u
nemocných	nemocný	k1gMnPc2	nemocný
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
pak	pak	k6eAd1	pak
léčiv	léčivo	k1gNnPc2	léčivo
ZMapp	ZMapp	k1gInSc1	ZMapp
(	(	kIx(	(
<g/>
nedosáhlo	dosáhnout	k5eNaPmAgNnS	dosáhnout
ani	ani	k9	ani
první	první	k4xOgFnPc1	první
fáze	fáze	k1gFnPc1	fáze
klinických	klinický	k2eAgInPc2d1	klinický
testů	test	k1gInPc2	test
<g/>
)	)	kIx)	)
a	a	k8xC	a
TKM-Ebola	TKM-Ebola	k1gFnSc1	TKM-Ebola
(	(	kIx(	(
<g/>
staženo	stažen	k2eAgNnSc1d1	staženo
z	z	k7c2	z
první	první	k4xOgFnSc2	první
fáze	fáze	k1gFnSc2	fáze
klinických	klinický	k2eAgInPc2d1	klinický
testů	test	k1gInPc2	test
kvůli	kvůli	k7c3	kvůli
vedlejším	vedlejší	k2eAgInPc3d1	vedlejší
účinkům	účinek	k1gInPc3	účinek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
vakcín	vakcína	k1gFnPc2	vakcína
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
ve	v	k7c6	v
stádiu	stádium	k1gNnSc6	stádium
pokusů	pokus	k1gInPc2	pokus
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
testy	testa	k1gFnPc1	testa
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
na	na	k7c6	na
lidech	člověk	k1gMnPc6	člověk
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
Výzkumného	výzkumný	k2eAgNnSc2d1	výzkumné
vakcinačního	vakcinační	k2eAgNnSc2d1	vakcinační
Centra	centrum	k1gNnSc2	centrum
(	(	kIx(	(
<g/>
VRC	VRC	kA	VRC
<g/>
)	)	kIx)	)
při	při	k7c6	při
Národním	národní	k2eAgInSc6d1	národní
institutu	institut	k1gInSc6	institut
Alergií	alergie	k1gFnPc2	alergie
a	a	k8xC	a
Infekčních	infekční	k2eAgNnPc2d1	infekční
onemocnění	onemocnění	k1gNnPc2	onemocnění
(	(	kIx(	(
<g/>
NIAID	NIAID	kA	NIAID
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
Národního	národní	k2eAgInSc2d1	národní
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
institutu	institut	k1gInSc2	institut
(	(	kIx(	(
<g/>
NIH	NIH	kA	NIH
<g/>
)	)	kIx)	)
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
vakcínu	vakcína	k1gFnSc4	vakcína
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
podávána	podávat	k5eAaImNgFnS	podávat
dobrovolníkovi	dobrovolník	k1gMnSc3	dobrovolník
na	na	k7c6	na
Klinickém	klinický	k2eAgNnSc6d1	klinické
centru	centrum	k1gNnSc6	centrum
při	při	k7c6	při
NIH	NIH	kA	NIH
v	v	k7c6	v
Bethesdě	Bethesda	k1gFnSc6	Bethesda
v	v	k7c6	v
Marylandu	Maryland	k1gInSc6	Maryland
<g/>
.	.	kIx.	.
</s>
<s>
Vakcína	vakcína	k1gFnSc1	vakcína
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádný	žádný	k3yNgInSc4	žádný
infekční	infekční	k2eAgInSc4d1	infekční
materiál	materiál	k1gInSc4	materiál
z	z	k7c2	z
viru	vir	k1gInSc2	vir
Ebola	Ebolo	k1gNnSc2	Ebolo
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
testy	test	k1gInPc1	test
na	na	k7c6	na
opicích	opice	k1gFnPc6	opice
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
pozitivní	pozitivní	k2eAgInPc1d1	pozitivní
výsledky	výsledek	k1gInPc1	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Testy	test	k1gInPc1	test
na	na	k7c6	na
člověku	člověk	k1gMnSc6	člověk
stále	stále	k6eAd1	stále
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgNnSc4d1	oficiální
stanovisko	stanovisko	k1gNnSc4	stanovisko
však	však	k9	však
dosud	dosud	k6eAd1	dosud
nebylo	být	k5eNaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
vakcín	vakcína	k1gFnPc2	vakcína
preventivně	preventivně	k6eAd1	preventivně
použita	použít	k5eAaPmNgFnS	použít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
vědkyň	vědkyně	k1gFnPc2	vědkyně
píchla	píchnout	k5eAaPmAgFnS	píchnout
do	do	k7c2	do
prstu	prst	k1gInSc2	prst
injekční	injekční	k2eAgFnSc7d1	injekční
jehlou	jehla	k1gFnSc7	jehla
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
předtím	předtím	k6eAd1	předtím
virem	vir	k1gInSc7	vir
ebola	ebolo	k1gNnSc2	ebolo
infikovala	infikovat	k5eAaBmAgFnS	infikovat
laboratorní	laboratorní	k2eAgFnPc4d1	laboratorní
myši	myš	k1gFnPc4	myš
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
nepropukla	propuknout	k5eNaPmAgFnS	propuknout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
však	však	k9	však
byla	být	k5eAaImAgFnS	být
nakažena	nakazit	k5eAaPmNgFnS	nakazit
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
objevitele	objevitel	k1gMnSc4	objevitel
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
vakcín	vakcína	k1gFnPc2	vakcína
<g/>
,	,	kIx,	,
Petera	Peter	k1gMnSc2	Peter
Walshe	Walsh	k1gMnSc2	Walsh
<g/>
,	,	kIx,	,
brání	bránit	k5eAaImIp3nS	bránit
rychlému	rychlý	k2eAgInSc3d1	rychlý
vývoji	vývoj	k1gInSc3	vývoj
vakcín	vakcína	k1gFnPc2	vakcína
nedostatek	nedostatek	k1gInSc4	nedostatek
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemoc	nemoc	k1gFnSc1	nemoc
postihuje	postihovat	k5eAaImIp3nS	postihovat
zejména	zejména	k9	zejména
chudé	chudý	k2eAgMnPc4d1	chudý
Afričany	Afričan	k1gMnPc4	Afričan
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
pro	pro	k7c4	pro
farmaceutické	farmaceutický	k2eAgFnPc4d1	farmaceutická
společnosti	společnost	k1gFnPc4	společnost
atraktivními	atraktivní	k2eAgMnPc7d1	atraktivní
zákazníky	zákazník	k1gMnPc7	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
Prevence	prevence	k1gFnSc1	prevence
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
omezení	omezení	k1gNnSc4	omezení
šíření	šíření	k1gNnSc2	šíření
onemocnění	onemocnění	k1gNnSc2	onemocnění
z	z	k7c2	z
nakažených	nakažený	k2eAgFnPc2d1	nakažená
opic	opice	k1gFnPc2	opice
a	a	k8xC	a
prasat	prase	k1gNnPc2	prase
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
kontrolováním	kontrolování	k1gNnSc7	kontrolování
těchto	tento	k3xDgNnPc2	tento
zvířat	zvíře	k1gNnPc2	zvíře
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
nákazy	nákaza	k1gFnSc2	nákaza
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
zabitím	zabití	k1gNnSc7	zabití
a	a	k8xC	a
řádnou	řádný	k2eAgFnSc7d1	řádná
likvidací	likvidace	k1gFnSc7	likvidace
těl	tělo	k1gNnPc2	tělo
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potvrzení	potvrzení	k1gNnSc2	potvrzení
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Nápomocné	nápomocný	k2eAgNnSc1d1	nápomocné
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
správné	správný	k2eAgNnSc1d1	správné
vaření	vaření	k1gNnSc1	vaření
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
nošení	nošení	k1gNnSc2	nošení
ochranného	ochranný	k2eAgInSc2d1	ochranný
oděvu	oděv	k1gInSc2	oděv
při	při	k7c6	při
manipulaci	manipulace	k1gFnSc6	manipulace
s	s	k7c7	s
masem	maso	k1gNnSc7	maso
<g/>
.	.	kIx.	.
</s>
<s>
Nošení	nošení	k1gNnSc1	nošení
ochranného	ochranný	k2eAgInSc2d1	ochranný
oděvu	oděv	k1gInSc2	oděv
a	a	k8xC	a
mytí	mytí	k1gNnSc2	mytí
rukou	ruka	k1gFnPc2	ruka
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
nakažené	nakažený	k2eAgFnSc2d1	nakažená
osoby	osoba	k1gFnSc2	osoba
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
vzorky	vzorek	k1gInPc7	vzorek
tělesných	tělesný	k2eAgFnPc2d1	tělesná
tekutin	tekutina	k1gFnPc2	tekutina
a	a	k8xC	a
tkání	tkáň	k1gFnPc2	tkáň
nakažených	nakažený	k2eAgFnPc2d1	nakažená
osob	osoba	k1gFnPc2	osoba
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
zacházet	zacházet	k5eAaImF	zacházet
se	s	k7c7	s
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
opatrností	opatrnost	k1gFnSc7	opatrnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nanejvýš	nanejvýš	k6eAd1	nanejvýš
obtížné	obtížný	k2eAgNnSc1d1	obtížné
předvídat	předvídat	k5eAaImF	předvídat
propuknutí	propuknutí	k1gNnSc3	propuknutí
epidemií	epidemie	k1gFnPc2	epidemie
eboly	ebola	k1gFnSc2	ebola
<g/>
.	.	kIx.	.
</s>
<s>
Rezervoárem	rezervoár	k1gInSc7	rezervoár
viru	vir	k1gInSc2	vir
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
rozličné	rozličný	k2eAgFnPc1d1	rozličná
divoce	divoce	k6eAd1	divoce
žijící	žijící	k2eAgFnPc1d1	žijící
africké	africký	k2eAgFnPc1d1	africká
opice	opice	k1gFnPc1	opice
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumy	výzkum	k1gInPc1	výzkum
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
epidemií	epidemie	k1gFnPc2	epidemie
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
předcházelo	předcházet	k5eAaImAgNnS	předcházet
vypuknutí	vypuknutí	k1gNnSc3	vypuknutí
epidemie	epidemie	k1gFnSc2	epidemie
mezi	mezi	k7c7	mezi
opicemi	opice	k1gFnPc7	opice
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vymírání	vymírání	k1gNnSc1	vymírání
přirozených	přirozený	k2eAgMnPc2d1	přirozený
nositelů	nositel	k1gMnPc2	nositel
onemocnění	onemocnění	k1gNnSc2	onemocnění
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
divočině	divočina	k1gFnSc6	divočina
zavčas	zavčas	k6eAd1	zavčas
zjistit	zjistit	k5eAaPmF	zjistit
a	a	k8xC	a
připravit	připravit	k5eAaPmF	připravit
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c4	na
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
riziko	riziko	k1gNnSc4	riziko
přenosu	přenos	k1gInSc2	přenos
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
a	a	k8xC	a
vypuknutí	vypuknutí	k1gNnSc4	vypuknutí
epidemie	epidemie	k1gFnSc2	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Bezpříznakovým	bezpříznakový	k2eAgInSc7d1	bezpříznakový
přenašečem	přenašeč	k1gInSc7	přenašeč
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
někteří	některý	k3yIgMnPc1	některý
netopýři	netopýr	k1gMnPc1	netopýr
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
domácí	domácí	k2eAgNnPc4d1	domácí
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
onemocnět	onemocnět	k5eAaPmF	onemocnět
ebolou	ebolá	k1gFnSc4	ebolá
mohou	moct	k5eAaImIp3nP	moct
i	i	k9	i
prasata	prase	k1gNnPc1	prase
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
bezpříznakovými	bezpříznakový	k2eAgInPc7d1	bezpříznakový
přenašeči	přenašeč	k1gInPc7	přenašeč
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
stát	stát	k5eAaImF	stát
psi	pes	k1gMnPc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
vážné	vážný	k2eAgFnPc1d1	vážná
obavy	obava	k1gFnPc1	obava
ze	z	k7c2	z
zanesení	zanesení	k1gNnSc2	zanesení
nemoci	nemoc	k1gFnSc2	nemoc
do	do	k7c2	do
civilizovaného	civilizovaný	k2eAgInSc2d1	civilizovaný
světa	svět	k1gInSc2	svět
turisty	turista	k1gMnSc2	turista
či	či	k8xC	či
s	s	k7c7	s
domácími	domácí	k2eAgMnPc7d1	domácí
mazlíčky	mazlíček	k1gMnPc7	mazlíček
<g/>
.	.	kIx.	.
</s>
<s>
Známo	znám	k2eAgNnSc1d1	známo
také	také	k9	také
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc4	jaký
je	být	k5eAaImIp3nS	být
vývoj	vývoj	k1gInSc4	vývoj
viru	vir	k1gInSc2	vir
Ebola	Ebolo	k1gNnSc2	Ebolo
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
informace	informace	k1gFnSc1	informace
o	o	k7c6	o
člověku	člověk	k1gMnSc6	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
infikován	infikovat	k5eAaBmNgInS	infikovat
virem	vir	k1gInSc7	vir
Ebola	Ebola	k1gMnSc1	Ebola
dvakrát	dvakrát	k6eAd1	dvakrát
a	a	k8xC	a
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
či	či	k8xC	či
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
prvním	první	k4xOgNnSc6	první
nakažení	nakažení	k1gNnSc6	nakažení
virem	vir	k1gInSc7	vir
Ebola	Ebol	k1gMnSc2	Ebol
je	být	k5eAaImIp3nS	být
daný	daný	k2eAgInSc4d1	daný
jedinec	jedinec	k1gMnSc1	jedinec
imunní	imunní	k2eAgFnSc1d1	imunní
vůči	vůči	k7c3	vůči
všem	všecek	k3xTgFnPc3	všecek
mutacím	mutace	k1gFnPc3	mutace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
avšak	avšak	k8xC	avšak
známý	známý	k2eAgInSc1d1	známý
případ	případ	k1gInSc1	případ
britské	britský	k2eAgFnSc2d1	britská
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
sestry	sestra	k1gFnSc2	sestra
<g/>
,	,	kIx,	,
Pauline	Paulin	k1gInSc5	Paulin
Cafferkeyové	Cafferkeyové	k2eAgNnPc6d1	Cafferkeyové
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yRgFnSc2	který
virus	virus	k1gInSc4	virus
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
těle	tělo	k1gNnSc6	tělo
opakovaně	opakovaně	k6eAd1	opakovaně
přežívá	přežívat	k5eAaImIp3nS	přežívat
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
velké	velký	k2eAgFnPc1d1	velká
obavy	obava	k1gFnPc1	obava
z	z	k7c2	z
využití	využití	k1gNnSc2	využití
eboly	ebola	k1gMnSc2	ebola
teroristy	terorista	k1gMnSc2	terorista
nebo	nebo	k8xC	nebo
některými	některý	k3yIgInPc7	některý
nebezpečnými	bezpečný	k2eNgInPc7d1	nebezpečný
režimy	režim	k1gInPc7	režim
jakožto	jakožto	k8xS	jakožto
biologické	biologický	k2eAgFnPc1d1	biologická
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
využití	využití	k1gNnSc1	využití
viru	vir	k1gInSc2	vir
Ebola	Ebolo	k1gNnSc2	Ebolo
jako	jako	k8xC	jako
biologické	biologický	k2eAgFnSc2d1	biologická
zbraně	zbraň	k1gFnSc2	zbraň
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
.	.	kIx.	.
</s>
<s>
Přeměna	přeměna	k1gFnSc1	přeměna
viru	vir	k1gInSc2	vir
Ebola	Ebolo	k1gNnSc2	Ebolo
do	do	k7c2	do
biologické	biologický	k2eAgFnSc2d1	biologická
zbraně	zbraň	k1gFnSc2	zbraň
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
náročná	náročný	k2eAgFnSc1d1	náročná
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
své	svůj	k3xOyFgNnSc4	svůj
omezení	omezení	k1gNnSc4	omezení
<g/>
;	;	kIx,	;
Ebola	Ebola	k1gFnSc1	Ebola
by	by	kYmCp3nS	by
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
přenášena	přenášen	k2eAgFnSc1d1	přenášena
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
efektivní	efektivní	k2eAgFnSc1d1	efektivní
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Ebola	Ebola	k1gFnSc1	Ebola
může	moct	k5eAaImIp3nS	moct
šířit	šířit	k5eAaImF	šířit
vzduchem	vzduch	k1gInSc7	vzduch
v	v	k7c6	v
kapičkách	kapička	k1gFnPc6	kapička
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
případ	případ	k1gInSc1	případ
Reston	Reston	k1gInSc1	Reston
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
nejnebezpečnějších	bezpečný	k2eNgInPc2d3	nejnebezpečnější
kmenů	kmen	k1gInPc2	kmen
tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
známa	znám	k2eAgFnSc1d1	známa
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
další	další	k2eAgMnPc1d1	další
vědci	vědec	k1gMnPc1	vědec
ale	ale	k8xC	ale
optimismus	optimismus	k1gInSc1	optimismus
svých	svůj	k3xOyFgMnPc2	svůj
kolegů	kolega	k1gMnPc2	kolega
nesdílí	sdílet	k5eNaImIp3nP	sdílet
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
obavy	obava	k1gFnPc1	obava
ještě	ještě	k9	ještě
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c4	na
Západ	západ	k1gInSc4	západ
dostaly	dostat	k5eAaPmAgFnP	dostat
podrobnosti	podrobnost	k1gFnPc1	podrobnost
o	o	k7c6	o
ruských	ruský	k2eAgInPc6d1	ruský
biologických	biologický	k2eAgInPc6d1	biologický
zbrojních	zbrojní	k2eAgInPc6d1	zbrojní
programech	program	k1gInPc6	program
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
doktora	doktor	k1gMnSc2	doktor
Kanatžana	Kanatžan	k1gMnSc2	Kanatžan
Alibekova	Alibekův	k2eAgInSc2d1	Alibekův
vynaložil	vynaložit	k5eAaPmAgInS	vynaložit
SSSR	SSSR	kA	SSSR
(	(	kIx(	(
<g/>
a	a	k8xC	a
následně	následně	k6eAd1	následně
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
obrovské	obrovský	k2eAgInPc1d1	obrovský
prostředky	prostředek	k1gInPc1	prostředek
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
eboly	ebol	k1gInPc4	ebol
a	a	k8xC	a
pokusy	pokus	k1gInPc4	pokus
"	"	kIx"	"
<g/>
zkřížit	zkřížit	k5eAaPmF	zkřížit
ji	on	k3xPp3gFnSc4	on
<g/>
"	"	kIx"	"
s	s	k7c7	s
virem	vir	k1gInSc7	vir
neštovic	neštovice	k1gFnPc2	neštovice
a	a	k8xC	a
virem	vir	k1gInSc7	vir
machupo	machupa	k1gFnSc5	machupa
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Richard	Richarda	k1gFnPc2	Richarda
Preston	Preston	k1gInSc1	Preston
<g />
.	.	kIx.	.
</s>
<s>
napsal	napsat	k5eAaPmAgInS	napsat
thriller	thriller	k1gInSc1	thriller
The	The	k1gFnSc2	The
Hot	hot	k0	hot
Zone	Zone	k1gNnPc6	Zone
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jako	jako	k8xC	jako
Zákeřná	zákeřný	k2eAgFnSc1d1	zákeřná
Ebola	Ebola	k1gFnSc1	Ebola
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
skutečných	skutečný	k2eAgFnPc2d1	skutečná
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
propuknutí	propuknutí	k1gNnSc3	propuknutí
epidemie	epidemie	k1gFnSc2	epidemie
filoviru	filovir	k1gInSc2	filovir
Marburg	Marburg	k1gInSc1	Marburg
<g/>
,	,	kIx,	,
snahu	snaha	k1gFnSc4	snaha
vypátrat	vypátrat	k5eAaPmF	vypátrat
jeho	on	k3xPp3gInSc4	on
zdroj	zdroj	k1gInSc4	zdroj
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
Kitum	Kitum	k1gInSc1	Kitum
na	na	k7c4	na
Mount	Mount	k1gInSc4	Mount
Elgon	Elgon	k1gNnSc1	Elgon
a	a	k8xC	a
objevení	objevení	k1gNnSc1	objevení
viru	vir	k1gInSc2	vir
Ebola	Ebolo	k1gNnSc2	Ebolo
v	v	k7c6	v
Restonu	Reston	k1gInSc6	Reston
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k1gInSc1	William
T.	T.	kA	T.
Close	Clos	k1gMnSc2	Clos
–	–	k?	–
Ebola	Ebola	k1gMnSc1	Ebola
Robert	Robert	k1gMnSc1	Robert
Liparulo	Liparula	k1gFnSc5	Liparula
–	–	k?	–
Virus	virus	k1gInSc1	virus
Robin	robin	k2eAgMnSc1d1	robin
Cook	Cook	k1gMnSc1	Cook
–	–	k?	–
Nákaza	nákaza	k1gFnSc1	nákaza
Tom	Tom	k1gMnSc1	Tom
Clancy	Clanca	k1gFnSc2	Clanca
–	–	k?	–
Z	z	k7c2	z
rozkazu	rozkaz	k1gInSc2	rozkaz
prezidenta	prezident	k1gMnSc2	prezident
(	(	kIx(	(
<g/>
Executive	Executiv	k1gInSc5	Executiv
Orders	Ordersa	k1gFnPc2	Ordersa
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Průlom	průlom	k1gInSc4	průlom
v	v	k7c6	v
léčbě	léčba	k1gFnSc6	léčba
viru	vir	k1gInSc2	vir
Ebola	Ebola	k1gFnSc1	Ebola
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
Idnes	Idnes	k1gInSc1	Idnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Breakthrough	Breakthrough	k1gMnSc1	Breakthrough
in	in	k?	in
Ebola	Ebola	k1gMnSc1	Ebola
Vaccine	Vaccin	k1gMnSc5	Vaccin
<g/>
.	.	kIx.	.
</s>
<s>
BBC	BBC	kA	BBC
News	News	k1gInSc1	News
World	World	k1gInSc1	World
Edition	Edition	k1gInSc1	Edition
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Busharizi	Busharih	k1gMnPc1	Busharih
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
<g/>
.	.	kIx.	.
</s>
<s>
Ebola	Ebola	k1gFnSc1	Ebola
Hits	Hits	k1gInSc1	Hits
Uganda	Uganda	k1gFnSc1	Uganda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Tourism	Touris	k1gNnSc7	Touris
Revival	revival	k1gInSc1	revival
Effort	Effort	k1gInSc1	Effort
<g/>
.	.	kIx.	.
</s>
<s>
Planet	planeta	k1gFnPc2	planeta
Ark	Ark	k1gFnPc2	Ark
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Centers	Centers	k6eAd1	Centers
for	forum	k1gNnPc2	forum
Disease	Diseasa	k1gFnSc3	Diseasa
Control	Control	k1gInSc1	Control
and	and	k?	and
Prevention	Prevention	k1gInSc1	Prevention
and	and	k?	and
World	World	k1gInSc1	World
Health	Health	k1gMnSc1	Health
Organization	Organization	k1gInSc1	Organization
<g/>
.	.	kIx.	.
</s>
<s>
Infection	Infection	k1gInSc1	Infection
for	forum	k1gNnPc2	forum
Health	Healtha	k1gFnPc2	Healtha
Control	Controla	k1gFnPc2	Controla
of	of	k?	of
Viral	Viral	k1gMnSc1	Viral
Hemorrhagic	Hemorrhagic	k1gMnSc1	Hemorrhagic
Fevers	Feversa	k1gFnPc2	Feversa
in	in	k?	in
the	the	k?	the
African	African	k1gMnSc1	African
Health	Health	k1gMnSc1	Health
Care	car	k1gMnSc5	car
Setting	Setting	k1gInSc4	Setting
<g/>
.	.	kIx.	.
</s>
<s>
Atlanta	Atlanta	k1gFnSc1	Atlanta
<g/>
,	,	kIx,	,
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
Disease	Diseasa	k1gFnSc3	Diseasa
Control	Control	k1gInSc1	Control
and	and	k?	and
Prevention	Prevention	k1gInSc1	Prevention
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Draper	Draper	k1gMnSc1	Draper
<g/>
,	,	kIx,	,
Allison	Allison	k1gMnSc1	Allison
Stark	Stark	k1gInSc1	Stark
<g/>
.	.	kIx.	.
</s>
<s>
Ebola	Ebola	k6eAd1	Ebola
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
The	The	k1gFnSc7	The
Rosen	rosen	k2eAgInSc4d1	rosen
Publishing	Publishing	k1gInSc4	Publishing
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Ebola	Ebola	k1gMnSc1	Ebola
Hemorrhagic	Hemorrhagic	k1gMnSc1	Hemorrhagic
Fever	Fever	k1gMnSc1	Fever
<g/>
.	.	kIx.	.
</s>
<s>
Centers	Centers	k6eAd1	Centers
for	forum	k1gNnPc2	forum
Disease	Diseas	k1gInSc6	Diseas
Control	Control	k1gInSc1	Control
Special	Special	k1gInSc1	Special
Pathogens	Pathogensa	k1gFnPc2	Pathogensa
Branch	Brancha	k1gFnPc2	Brancha
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Ebola	Ebola	k1gMnSc1	Ebola
Hemorrhagic	Hemorrhagice	k1gInPc2	Hemorrhagice
Fever	Fever	k1gInSc1	Fever
Table	tablo	k1gNnSc6	tablo
Showing	Showing	k1gInSc1	Showing
Known	Known	k1gMnSc1	Known
Cases	Cases	k1gMnSc1	Cases
and	and	k?	and
Outbreaks	Outbreaks	k1gInSc1	Outbreaks
<g/>
,	,	kIx,	,
in	in	k?	in
Chronological	Chronological	k1gMnSc1	Chronological
Order	Order	k1gMnSc1	Order
<g/>
.	.	kIx.	.
</s>
<s>
Centers	Centers	k6eAd1	Centers
for	forum	k1gNnPc2	forum
Disease	Diseasa	k1gFnSc3	Diseasa
Control	Control	k1gInSc1	Control
and	and	k?	and
Prevention	Prevention	k1gInSc1	Prevention
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Fact	Fact	k1gMnSc1	Fact
Sheet	Sheet	k1gMnSc1	Sheet
Number	Number	k1gInSc4	Number
103	[number]	k4	103
Ebola	Ebola	k1gMnSc1	Ebola
Hemorrhagic	Hemorrhagic	k1gMnSc1	Hemorrhagic
Fever	Fever	k1gMnSc1	Fever
<g/>
.	.	kIx.	.
</s>
<s>
World	World	k6eAd1	World
Health	Health	k1gInSc1	Health
Organization	Organization	k1gInSc1	Organization
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Filoviruses	Filoviruses	k1gMnSc1	Filoviruses
<g/>
.	.	kIx.	.
</s>
<s>
Centers	Centers	k6eAd1	Centers
for	forum	k1gNnPc2	forum
Disease	Diseas	k1gInSc6	Diseas
Control	Control	k1gInSc1	Control
Special	Special	k1gInSc1	Special
Pathogens	Pathogensa	k1gFnPc2	Pathogensa
Branch	Brancha	k1gFnPc2	Brancha
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Horowitz	Horowitz	k1gMnSc1	Horowitz
<g/>
,	,	kIx,	,
Leonard	Leonard	k1gMnSc1	Leonard
G.	G.	kA	G.
Emerging	Emerging	k1gInSc1	Emerging
Viruses	Viruses	k1gInSc1	Viruses
<g/>
:	:	kIx,	:
AIDS	AIDS	kA	AIDS
&	&	k?	&
Ebola	Ebola	k1gMnSc1	Ebola
–	–	k?	–
Nature	Natur	k1gMnSc5	Natur
<g/>
,	,	kIx,	,
Accident	Accident	k1gMnSc1	Accident
<g/>
,	,	kIx,	,
or	or	k?	or
Intentional	Intentional	k1gFnSc1	Intentional
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Rockport	Rockport	k1gInSc1	Rockport
<g/>
,	,	kIx,	,
MA	MA	kA	MA
<g/>
:	:	kIx,	:
Tetrahedron	Tetrahedron	k1gMnSc1	Tetrahedron
<g/>
,	,	kIx,	,
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Preston	Preston	k1gInSc1	Preston
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Hot	hot	k0	hot
Zone	Zone	k1gNnPc3	Zone
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Anchor	Anchor	k1gInSc1	Anchor
Books	Booksa	k1gFnPc2	Booksa
Doubleday	Doubledaa	k1gFnSc2	Doubledaa
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Russell	Russell	k1gMnSc1	Russell
<g/>
,	,	kIx,	,
Brett	Brett	k1gMnSc1	Brett
<g/>
.	.	kIx.	.
</s>
<s>
What	What	k1gMnSc1	What
are	ar	k1gInSc5	ar
the	the	k?	the
Chances	Chancesa	k1gFnPc2	Chancesa
<g/>
?	?	kIx.	?
</s>
<s>
Ebola	Ebola	k1gMnSc1	Ebola
FAQ	FAQ	kA	FAQ
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
TED	Ted	k1gMnSc1	Ted
Case	Cas	k1gInSc2	Cas
Study	stud	k1gInPc1	stud
<g/>
:	:	kIx,	:
Ebola	Ebola	k1gFnSc1	Ebola
and	and	k?	and
Trade	Trad	k1gInSc5	Trad
<g/>
.	.	kIx.	.
</s>
<s>
Trade	Trade	k6eAd1	Trade
and	and	k?	and
EnvironmentDatabases	EnvironmentDatabases	k1gInSc1	EnvironmentDatabases
<g/>
.	.	kIx.	.
květen	květen	k1gInSc1	květen
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Oplinger	Oplinger	k1gInSc1	Oplinger
<g/>
,	,	kIx,	,
Anne	Anne	k1gInSc1	Anne
A.	A.	kA	A.
NIAID	NIAID	kA	NIAID
Ebola	Ebola	k1gMnSc1	Ebola
Vaccine	Vaccin	k1gInSc5	Vaccin
Enters	Enters	k1gInSc1	Enters
Human	Human	k1gMnSc1	Human
Trial	trial	k1gInSc1	trial
<g/>
.	.	kIx.	.
</s>
<s>
NIAID	NIAID	kA	NIAID
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
National	National	k1gMnSc1	National
Institute	institut	k1gInSc5	institut
of	of	k?	of
Allergy	Allerg	k1gInPc1	Allerg
and	and	k?	and
Infectious	Infectious	k1gInSc1	Infectious
Diseases	Diseases	k1gInSc1	Diseases
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Mangold	mangold	k1gInSc1	mangold
<g/>
,	,	kIx,	,
Tom	Tom	k1gMnSc1	Tom
<g/>
;	;	kIx,	;
Goldberg	Goldberg	k1gMnSc1	Goldberg
<g/>
,	,	kIx,	,
Jeff	Jeff	k1gMnSc1	Jeff
<g/>
:	:	kIx,	:
A	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
(	(	kIx(	(
<g/>
pravda	pravda	k1gFnSc1	pravda
o	o	k7c6	o
biologických	biologický	k2eAgFnPc6d1	biologická
válkách	válka	k1gFnPc6	válka
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ebola	Ebolo	k1gNnSc2	Ebolo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ebola	ebola	k6eAd1	ebola
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
http://www.itg.be/ebola/ebola-06.htm	[url]	k6eAd1	http://www.itg.be/ebola/ebola-06.htm
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc4d1	různý
články	článek	k1gInPc4	článek
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
eboly	ebot	k5eAaBmAgInP	ebot
<g/>
,	,	kIx,	,
vynikající	vynikající	k2eAgInSc1d1	vynikající
zdroj	zdroj	k1gInSc1	zdroj
informací	informace	k1gFnPc2	informace
<g/>
)	)	kIx)	)
Článek	článek	k1gInSc1	článek
Virus	virus	k1gInSc1	virus
Ebola	Ebol	k1gMnSc2	Ebol
Jiřího	Jiří	k1gMnSc2	Jiří
Svrška	Svršek	k1gMnSc2	Svršek
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Natura	Natura	k1gFnSc1	Natura
http://www.journals.uchicago.edu/JID/journal/contents/v179nS1.html	[url]	k1gInSc1	http://www.journals.uchicago.edu/JID/journal/contents/v179nS1.html
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
http://www.scripps.edu/newsandviews/e_20020114/ebola1.html	[url]	k1gInSc1	http://www.scripps.edu/newsandviews/e_20020114/ebola1.html
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
http://www.ncbi.nlm.nih.gov/ICTVdb/ICTVdB/01.025.htm	[url]	k6eAd1	http://www.ncbi.nlm.nih.gov/ICTVdb/ICTVdB/01.025.htm
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
http://fzu.xf.cz/ebov/index.php	[url]	k1gInSc1	http://fzu.xf.cz/ebov/index.php
(	(	kIx(	(
<g/>
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
česká	český	k2eAgFnSc1d1	Česká
na	na	k7c4	na
konec	konec	k1gInSc4	konec
<g/>
)	)	kIx)	)
</s>
