<p>
<s>
Malgašský	Malgašský	k2eAgInSc4d1	Malgašský
ariary	ariar	k1gInPc4	ariar
(	(	kIx(	(
<g/>
malgašsky	malgašsky	k6eAd1	malgašsky
ariary	ariar	k1gInPc1	ariar
malagasy	malagasa	k1gFnSc2	malagasa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zákonným	zákonný	k2eAgNnSc7d1	zákonné
platidlem	platidlo	k1gNnSc7	platidlo
ostrovního	ostrovní	k2eAgInSc2d1	ostrovní
státu	stát	k1gInSc2	stát
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
ISO	ISO	kA	ISO
4217	[number]	k4	4217
kód	kód	k1gInSc4	kód
je	on	k3xPp3gMnPc4	on
MGA.	MGA.	k1gMnPc4	MGA.
Je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
měn	měna	k1gFnPc2	měna
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nepoužívají	používat	k5eNaImIp3nP	používat
desítkovou	desítkový	k2eAgFnSc4d1	desítková
soustavu	soustava	k1gFnSc4	soustava
(	(	kIx(	(
<g/>
další	další	k2eAgFnSc1d1	další
je	být	k5eAaImIp3nS	být
mauritánská	mauritánský	k2eAgFnSc1d1	mauritánská
ukíjá	ukíjá	k1gFnSc1	ukíjá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
ariary	ariar	k1gInPc1	ariar
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
5	[number]	k4	5
iraimbilanja	iraimbilanj	k1gInSc2	iraimbilanj
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
ariary	ariara	k1gFnSc2	ariara
a	a	k8xC	a
iraimbilanja	iraimbilanja	k6eAd1	iraimbilanja
mají	mít	k5eAaImIp3nP	mít
původ	původ	k1gInSc4	původ
už	už	k9	už
v	v	k7c6	v
předkoloniálním	předkoloniální	k2eAgNnSc6d1	předkoloniální
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
1	[number]	k4	1
euro	euro	k1gNnSc1	euro
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
rovnalo	rovnat	k5eAaImAgNnS	rovnat
přibližně	přibližně	k6eAd1	přibližně
2500	[number]	k4	2500
ariary	ariar	k1gInPc4	ariar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
zavedl	zavést	k5eAaPmAgInS	zavést
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
vlastní	vlastní	k2eAgFnSc4d1	vlastní
národní	národní	k2eAgFnSc4d1	národní
měnu	měna	k1gFnSc4	měna
-	-	kIx~	-
malgašský	malgašský	k2eAgInSc1d1	malgašský
frank	frank	k1gInSc1	frank
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2003	[number]	k4	2003
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
uvedeny	uveden	k2eAgFnPc4d1	uvedena
nové	nový	k2eAgFnPc4d1	nová
bankovky	bankovka	k1gFnPc4	bankovka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
napsanou	napsaný	k2eAgFnSc4d1	napsaná
nominální	nominální	k2eAgFnSc4d1	nominální
hodnotu	hodnota	k1gFnSc4	hodnota
v	v	k7c4	v
ariary	ariara	k1gFnPc4	ariara
velkým	velký	k2eAgNnSc7d1	velké
písmem	písmo	k1gNnSc7	písmo
<g/>
,	,	kIx,	,
ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
hodnota	hodnota	k1gFnSc1	hodnota
ve	v	k7c6	v
francích	frank	k1gInPc6	frank
byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaPmNgFnS	napsat
malým	malý	k2eAgNnSc7d1	malé
písmem	písmo	k1gNnSc7	písmo
pod	pod	k7c7	pod
hodnotou	hodnota	k1gFnSc7	hodnota
v	v	k7c4	v
ariary	ariara	k1gFnPc4	ariara
<g/>
.	.	kIx.	.
</s>
<s>
Směnný	směnný	k2eAgInSc1d1	směnný
kurz	kurz	k1gInSc1	kurz
mezi	mezi	k7c4	mezi
ariarym	ariarym	k1gInSc4	ariarym
a	a	k8xC	a
frankem	frank	k1gInSc7	frank
byl	být	k5eAaImAgMnS	být
1	[number]	k4	1
ariary	ariar	k1gInPc4	ariar
=	=	kIx~	=
5	[number]	k4	5
franků	frank	k1gInPc2	frank
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
iraimbilanja	iraimbilanja	k1gMnSc1	iraimbilanja
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
rovná	rovnat	k5eAaImIp3nS	rovnat
jednomu	jeden	k4xCgNnSc3	jeden
franku	frank	k1gInSc3	frank
<g/>
.	.	kIx.	.
</s>
<s>
1	1	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
se	se	k3xPyFc4	se
ariary	ariara	k1gFnSc2	ariara
stal	stát	k5eAaPmAgInS	stát
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
platidlem	platidlo	k1gNnSc7	platidlo
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
oběhu	oběh	k1gInSc3	oběh
mince	mince	k1gFnSc2	mince
a	a	k8xC	a
bankovky	bankovka	k1gFnPc1	bankovka
vydané	vydaný	k2eAgFnPc1d1	vydaná
jak	jak	k8xC	jak
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Madagaskarská	madagaskarský	k2eAgFnSc1d1	madagaskarská
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
stahuje	stahovat	k5eAaImIp3nS	stahovat
oběživo	oběživo	k1gNnSc4	oběživo
staršího	starý	k2eAgNnSc2d2	starší
data	datum	k1gNnSc2	datum
z	z	k7c2	z
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mince	mince	k1gFnPc4	mince
a	a	k8xC	a
bankovky	bankovka	k1gFnPc4	bankovka
==	==	k?	==
</s>
</p>
<p>
<s>
Současné	současný	k2eAgFnPc1d1	současná
mince	mince	k1gFnPc1	mince
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
mají	mít	k5eAaImIp3nP	mít
hodnoty	hodnota	k1gFnPc1	hodnota
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
iraimbilanja	iraimbilanj	k1gInSc2	iraimbilanj
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
20	[number]	k4	20
a	a	k8xC	a
50	[number]	k4	50
ariary	ariar	k1gInPc7	ariar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bankovky	bankovka	k1gFnPc1	bankovka
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
mají	mít	k5eAaImIp3nP	mít
hodnoty	hodnota	k1gFnSc2	hodnota
100	[number]	k4	100
<g/>
,	,	kIx,	,
200	[number]	k4	200
<g/>
,	,	kIx,	,
500	[number]	k4	500
<g/>
,	,	kIx,	,
1	[number]	k4	1
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
2	[number]	k4	2
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
5	[number]	k4	5
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
10	[number]	k4	10
000	[number]	k4	000
ariary	ariar	k1gInPc7	ariar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Malgašský	Malgašský	k2eAgInSc4d1	Malgašský
ariary	ariar	k1gInPc4	ariar
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
bankovek	bankovka	k1gFnPc2	bankovka
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
místní	místní	k2eAgFnSc2d1	místní
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
</s>
</p>
<p>
<s>
Vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
mincí	mince	k1gFnPc2	mince
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
místní	místní	k2eAgFnSc2d1	místní
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
</s>
</p>
<p>
<s>
Historický	historický	k2eAgInSc1d1	historický
přehled	přehled	k1gInSc1	přehled
měn	měna	k1gFnPc2	měna
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
místní	místní	k2eAgFnSc2d1	místní
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
</s>
</p>
<p>
<s>
Historický	historický	k2eAgInSc1d1	historický
přehled	přehled	k1gInSc1	přehled
měn	měna	k1gFnPc2	měna
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
www.globalfinancialdata.com	www.globalfinancialdata.com	k1gInSc1	www.globalfinancialdata.com
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c4	o
ariary	ariara	k1gFnPc4	ariara
na	na	k7c4	na
www.spiritus-temporis.com	www.spiritusemporis.com	k1gInSc4	www.spiritus-temporis.com
</s>
</p>
