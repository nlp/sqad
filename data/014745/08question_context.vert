<s>
Libra	libra	k1gFnSc1
šterlinků	šterlink	k1gInPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
též	též	k9
britská	britský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
pound	pound	k1gInSc1
sterling	sterling	k1gInSc1
<g/>
,	,	kIx,
British	British	k1gInSc1
pound	pound	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zákonné	zákonný	k2eAgNnSc4d1
platidlo	platidlo	k1gNnSc4
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>