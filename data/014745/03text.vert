<s>
Libra	libra	k1gFnSc1
šterlinků	šterlink	k1gInPc2
</s>
<s>
libra	libra	k1gFnSc1
šterlinkůpound	šterlinkůpound	k1gMnSc1
sterling	sterling	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Země	země	k1gFnSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
královstvíMan	královstvíMan	k1gInSc4
ManGuernsey	ManGuernsey	k1gInPc4
GuernseyJersey	GuernseyJerse	k2eAgInPc4d1
JerseyGibraltar	JerseyGibraltar	k1gInSc4
GibraltarFalklandy	GibraltarFalklanda	k1gFnSc2
FalklandySvatá	FalklandySvatá	k1gFnSc1
Helena	Helena	k1gFnSc1
<g/>
,	,	kIx,
Ascension	Ascension	k1gInSc1
a	a	k8xC
Tristan	Tristan	k1gInSc1
da	da	k?
Cunha	Cunha	k1gFnSc1
Svatá	svatý	k2eAgFnSc1d1
Helena	Helena	k1gFnSc1
<g/>
,	,	kIx,
Ascension	Ascension	k1gInSc1
a	a	k8xC
Tristan	Tristan	k1gInSc1
da	da	k?
CunhaBritské	CunhaBritský	k2eAgNnSc1d1
indickooceánské	indickooceánský	k2eAgNnSc1d1
území	území	k1gNnSc1
Britské	britský	k2eAgNnSc1d1
indickooceánské	indickooceánský	k2eAgNnSc1d1
území	území	k1gNnSc1
(	(	kIx(
<g/>
de	de	k?
iure	iure	k1gInSc1
<g/>
)	)	kIx)
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
GBP	GBP	kA
Symbol	symbol	k1gInSc1
</s>
<s>
£	£	k?
Dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
penny	penny	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
)	)	kIx)
Mince	mince	k1gFnSc1
</s>
<s>
1	#num#	k4
p	p	k?
<g/>
,	,	kIx,
2	#num#	k4
p	p	k?
<g/>
,	,	kIx,
5	#num#	k4
p	p	k?
<g/>
,	,	kIx,
10	#num#	k4
p	p	k?
<g/>
,	,	kIx,
20	#num#	k4
p	p	k?
<g/>
,	,	kIx,
50	#num#	k4
p	p	k?
<g/>
,	,	kIx,
1	#num#	k4
£	£	k?
<g/>
,	,	kIx,
2	#num#	k4
£	£	k?
Bankovky	bankovka	k1gFnPc1
</s>
<s>
běžně	běžně	k6eAd1
5	#num#	k4
£	£	k?
<g/>
,	,	kIx,
10	#num#	k4
£	£	k?
<g/>
,	,	kIx,
20	#num#	k4
£	£	k?
<g/>
,	,	kIx,
50	#num#	k4
£	£	k?
<g/>
zřídka	zřídka	k6eAd1
1	#num#	k4
£	£	k?
<g/>
,	,	kIx,
100	#num#	k4
£	£	k?
</s>
<s>
Libra	libra	k1gFnSc1
šterlinků	šterlink	k1gInPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
též	též	k9
britská	britský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
pound	pound	k1gInSc1
sterling	sterling	k1gInSc1
<g/>
,	,	kIx,
British	British	k1gInSc1
pound	pound	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zákonné	zákonný	k2eAgNnSc4d1
platidlo	platidlo	k1gNnSc4
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiální	oficiální	k2eAgFnSc1d1
ISO	ISO	kA
4217	#num#	k4
kód	kód	k1gInSc4
měny	měna	k1gFnSc2
je	být	k5eAaImIp3nS
GBP	GBP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Značka	značka	k1gFnSc1
pro	pro	k7c4
libru	libra	k1gFnSc4
je	být	k5eAaImIp3nS
£	£	k?
a	a	k8xC
v	v	k7c6
angličtině	angličtina	k1gFnSc6
se	se	k3xPyFc4
dává	dávat	k5eAaImIp3nS
před	před	k7c4
sumu	suma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
libra	libra	k1gFnSc1
označovala	označovat	k5eAaImAgFnS
jen	jen	k9
jednotku	jednotka	k1gFnSc4
hmotnosti	hmotnost	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
libra	libra	k1gFnSc1
=	=	kIx~
453	#num#	k4
gramů	gram	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
až	až	k9
později	pozdě	k6eAd2
se	se	k3xPyFc4
libra	libra	k1gFnSc1
šterlinků	šterlink	k1gInPc2
začala	začít	k5eAaPmAgFnS
používat	používat	k5eAaImF
i	i	k9
jako	jako	k9
měnová	měnový	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
libra	libra	k1gFnSc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
100	#num#	k4
pencí	pence	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
libru	libra	k1gFnSc4
šterlinků	šterlink	k1gInPc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
poměru	poměr	k1gInSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
pevně	pevně	k6eAd1
navázány	navázán	k2eAgFnPc4d1
tři	tři	k4xCgFnPc4
měny	měna	k1gFnPc4
britských	britský	k2eAgNnPc2d1
zámořských	zámořský	k2eAgNnPc2d1
území	území	k1gNnPc2
–	–	k?
Gibraltaru	Gibraltar	k1gInSc6
<g/>
,	,	kIx,
Falkland	Falkland	k1gInSc1
a	a	k8xC
Svaté	svatý	k2eAgFnSc2d1
Heleny	Helena	k1gFnSc2
<g/>
,	,	kIx,
Ascensionu	Ascension	k1gInSc2
a	a	k8xC
Tristanu	Tristan	k1gInSc2
da	da	k?
Cunha	Cunha	k1gMnSc1
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bankovky	bankovka	k1gFnPc4
libry	libra	k1gFnSc2
šterlinků	šterlink	k1gInPc2
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
běžně	běžně	k6eAd1
akceptované	akceptovaný	k2eAgInPc1d1
společně	společně	k6eAd1
s	s	k7c7
místními	místní	k2eAgFnPc7d1
bankovkami	bankovka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vývoj	vývoj	k1gInSc1
kurzu	kurz	k1gInSc2
GBP	GBP	kA
-	-	kIx~
od	od	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
vůči	vůči	k7c3
české	český	k2eAgFnSc3d1
koruně	koruna	k1gFnSc3
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
roku	rok	k1gInSc2
1971	#num#	k4
se	se	k3xPyFc4
jedna	jeden	k4xCgFnSc1
libra	libra	k1gFnSc1
dělila	dělit	k5eAaImAgFnS
na	na	k7c4
20	#num#	k4
šilinků	šilink	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
tento	tento	k3xDgInSc1
šilink	šilink	k1gInSc1
dále	daleko	k6eAd2
sestával	sestávat	k5eAaImAgInS
z	z	k7c2
12	#num#	k4
pencí	pence	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
pence	pence	k1gFnSc1
byla	být	k5eAaImAgFnS
tedy	tedy	k9
1	#num#	k4
<g/>
/	/	kIx~
<g/>
240	#num#	k4
staré	starý	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
libry	libra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Šterlink	šterlink	k1gInSc1
byl	být	k5eAaImAgInS
název	název	k1gInSc4
stříbrné	stříbrný	k2eAgFnSc2d1
mince	mince	k1gFnSc2
–	–	k?
stříbrný	stříbrný	k2eAgInSc4d1
penny	penny	k1gInSc4
(	(	kIx(
<g/>
=	=	kIx~
<g/>
pence	pence	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc7,k3yQgFnSc7,k3yIgFnSc7
se	se	k3xPyFc4
v	v	k7c4
Anglii	Anglie	k1gFnSc4
platilo	platit	k5eAaImAgNnS
v	v	k7c6
době	doba	k1gFnSc6
vlády	vláda	k1gFnSc2
Normanů	Norman	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
inflaci	inflace	k1gFnSc3
vystřídala	vystřídat	k5eAaPmAgFnS
časem	časem	k6eAd1
libra	libra	k1gFnSc1
penny	penny	k1gFnSc2
jako	jako	k8xS,k8xC
oficiální	oficiální	k2eAgFnSc4d1
měnu	měna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotný	samotný	k2eAgInSc1d1
název	název	k1gInSc1
„	„	k?
<g/>
libra	libra	k1gFnSc1
<g/>
“	“	k?
je	být	k5eAaImIp3nS
používán	používat	k5eAaImNgInS
i	i	k9
jako	jako	k8xC,k8xS
váhová	váhový	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvojí	dvojit	k5eAaImIp3nS
význam	význam	k1gInSc4
tohoto	tento	k3xDgNnSc2
slova	slovo	k1gNnSc2
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
libra	libra	k1gFnSc1
označovala	označovat	k5eAaImAgFnS
pouze	pouze	k6eAd1
jednotku	jednotka	k1gFnSc4
hmotnosti	hmotnost	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
libra	libra	k1gFnSc1
=	=	kIx~
453	#num#	k4
gramů	gram	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
dřívější	dřívější	k2eAgFnSc2d1
praxi	praxe	k1gFnSc4
vážení	vážení	k1gNnSc2
stříbra	stříbro	k1gNnSc2
při	při	k7c6
obchodování	obchodování	k1gNnSc6
se	se	k3xPyFc4
pak	pak	k6eAd1
význam	význam	k1gInSc1
postupně	postupně	k6eAd1
přenesl	přenést	k5eAaPmAgInS
i	i	k9
na	na	k7c4
peníze	peníz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Libra	libra	k1gFnSc1
tedy	tedy	k8xC
znamenala	znamenat	k5eAaImAgFnS
určité	určitý	k2eAgNnSc4d1
množství	množství	k1gNnSc4
stříbra	stříbro	k1gNnSc2
vážící	vážící	k2eAgFnSc4d1
jednu	jeden	k4xCgFnSc4
libru	libra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc4
libra	libra	k1gFnSc1
šterlinků	šterlink	k1gInPc2
je	být	k5eAaImIp3nS
zkracován	zkracovat	k5eAaImNgInS
na	na	k7c4
libra	libra	k1gFnSc1
nebo	nebo	k8xC
někdy	někdy	k6eAd1
na	na	k7c4
šterlink	šterlink	k1gInSc4
<g/>
,	,	kIx,
respektive	respektive	k9
pound	pound	k1gInSc1
nebo	nebo	k8xC
slangově	slangově	k6eAd1
quid	quid	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pence	pence	k1gFnSc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
dělily	dělit	k5eAaImAgInP
na	na	k7c4
čtvrtiny	čtvrtina	k1gFnPc4
neboli	neboli	k8xC
farthingy	farthing	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
objevovaly	objevovat	k5eAaImAgFnP
mince	mince	k1gFnPc1
nejrůznějších	různý	k2eAgFnPc2d3
dalších	další	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
–	–	k?
dva	dva	k4xCgInPc4
šilinky	šilink	k1gInPc4
tvořily	tvořit	k5eAaImAgInP
jeden	jeden	k4xCgInSc4
florin	florin	k1gInSc4
<g/>
,	,	kIx,
dva	dva	k4xCgInPc4
šilinky	šilink	k1gInPc4
a	a	k8xC
6	#num#	k4
pencí	pence	k1gFnPc2
byly	být	k5eAaImAgFnP
půlkoruna	půlkoruna	k1gFnSc1
a	a	k8xC
celá	celý	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
měla	mít	k5eAaImAgFnS
hodnotu	hodnota	k1gFnSc4
pěti	pět	k4xCc2
šilinků	šilink	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1663	#num#	k4
až	až	k8xS
1816	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Británii	Británie	k1gFnSc6
razila	razit	k5eAaImAgFnS
guinea	guinea	k1gFnSc1
–	–	k?
zlatá	zlatý	k2eAgFnSc1d1
mince	mince	k1gFnSc1
s	s	k7c7
nominální	nominální	k2eAgFnSc7d1
hodnotou	hodnota	k1gFnSc7
jedné	jeden	k4xCgFnSc2
libry	libra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1817	#num#	k4
ji	on	k3xPp3gFnSc4
nahradil	nahradit	k5eAaPmAgInS
sovereign	sovereign	k1gInSc1
–	–	k?
též	též	k6eAd1
zlatá	zlatý	k2eAgFnSc1d1
mince	mince	k1gFnSc1
se	s	k7c7
stejnou	stejný	k2eAgFnSc7d1
nominální	nominální	k2eAgFnSc7d1
hodnotou	hodnota	k1gFnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
i	i	k9
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
stále	stále	k6eAd1
razí	razit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1971	#num#	k4
byla	být	k5eAaImAgFnS
provedena	proveden	k2eAgFnSc1d1
decimalizace	decimalizace	k1gFnSc1
(	(	kIx(
<g/>
začala	začít	k5eAaPmAgFnS
se	se	k3xPyFc4
používat	používat	k5eAaImF
desítková	desítkový	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
jednu	jeden	k4xCgFnSc4
libru	libra	k1gFnSc4
od	od	k7c2
tohoto	tento	k3xDgNnSc2
data	datum	k1gNnSc2
tvoří	tvořit	k5eAaImIp3nS
100	#num#	k4
pencí	pence	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
vstoupilo	vstoupit	k5eAaPmAgNnS
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
měnového	měnový	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
měl	mít	k5eAaImAgInS
za	za	k7c4
cíl	cíl	k1gInSc4
udržovat	udržovat	k5eAaImF
stabilní	stabilní	k2eAgInPc4d1
kurzy	kurz	k1gInPc4
mezi	mezi	k7c7
měnami	měna	k1gFnPc7
svých	svůj	k3xOyFgMnPc2
účastníků	účastník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
ovšem	ovšem	k9
začala	začít	k5eAaPmAgFnS
libra	libra	k1gFnSc1
prudce	prudko	k6eAd1
oslabovat	oslabovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bank	bank	k1gInSc1
of	of	k?
England	England	k1gInSc1
utratila	utratit	k5eAaPmAgFnS
devizy	deviza	k1gFnPc4
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
27	#num#	k4
miliard	miliarda	k4xCgFnPc2
liber	libra	k1gFnPc2
v	v	k7c6
marné	marný	k2eAgFnSc6d1
snaze	snaha	k1gFnSc6
udržet	udržet	k5eAaPmF
libru	libra	k1gFnSc4
v	v	k7c6
daném	daný	k2eAgNnSc6d1
pásmu	pásmo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
britská	britský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
snahy	snaha	k1gFnSc2
o	o	k7c4
udržení	udržení	k1gNnSc4
kurzu	kurz	k1gInSc2
v	v	k7c6
stanovém	stanový	k2eAgNnSc6d1
pásmu	pásmo	k1gNnSc6
vzdala	vzdát	k5eAaPmAgFnS
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
září	září	k1gNnSc6
1992	#num#	k4
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
z	z	k7c2
Evropského	evropský	k2eAgInSc2d1
měnového	měnový	k2eAgInSc2d1
systému	systém	k1gInSc2
vystoupilo	vystoupit	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Britské	britský	k2eAgFnPc1d1
daňové	daňový	k2eAgFnPc1d1
poplatníky	poplatník	k1gMnPc4
tato	tento	k3xDgFnSc1
epizoda	epizoda	k1gFnSc1
stála	stát	k5eAaImAgFnS
3,3	3,3	k4
miliard	miliarda	k4xCgFnPc2
liber	libra	k1gFnPc2
<g/>
,	,	kIx,
tedy	tedy	k9
asi	asi	k9
půl	půl	k1xP
procenta	procento	k1gNnSc2
tehdejšího	tehdejší	k2eAgInSc2d1
britského	britský	k2eAgInSc2d1
hrubého	hrubý	k2eAgInSc2d1
domácího	domácí	k2eAgInSc2d1
produktu	produkt	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bankovky	bankovka	k1gFnPc1
</s>
<s>
Ve	v	k7c6
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
a	a	k8xC
v	v	k7c6
jeho	jeho	k3xOp3gNnPc2
korunních	korunní	k2eAgNnPc2d1
závislých	závislý	k2eAgNnPc2d1
území	území	k1gNnPc2
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
bankovky	bankovka	k1gFnPc4
řada	řada	k1gFnSc1
institucí	instituce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
Bank	banka	k1gFnPc2
of	of	k?
England	Englanda	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
plní	plnit	k5eAaImIp3nS
funkci	funkce	k1gFnSc4
centrální	centrální	k2eAgFnSc2d1
banky	banka	k1gFnSc2
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
právo	právo	k1gNnSc4
vydávat	vydávat	k5eAaImF,k5eAaPmF
bankovky	bankovka	k1gFnPc4
i	i	k9
3	#num#	k4
skotské	skotský	k2eAgFnSc2d1
banky	banka	k1gFnSc2
(	(	kIx(
<g/>
Bank	bank	k1gInSc1
of	of	k?
Scotland	Scotland	k1gInSc1
<g/>
,	,	kIx,
Royal	Royal	k1gInSc1
Bank	bank	k1gInSc1
of	of	k?
Scotland	Scotland	k1gInSc1
a	a	k8xC
Clydesdale	Clydesdala	k1gFnSc3
Bank	banka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
4	#num#	k4
severoirské	severoirský	k2eAgFnPc1d1
banky	banka	k1gFnPc1
(	(	kIx(
<g/>
Bank	bank	k1gInSc1
of	of	k?
Ireland	Ireland	k1gInSc1
<g/>
,	,	kIx,
First	First	k1gInSc1
Trust	trust	k1gInSc1
Bank	bank	k1gInSc1
<g/>
,	,	kIx,
Danske	Dansk	k1gInPc1
Bank	bank	k1gInSc1
a	a	k8xC
Ulster	Ulster	k1gInSc1
Bank	banka	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
místní	místní	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
ostrovů	ostrov	k1gInPc2
Man	Man	k1gMnSc1
<g/>
,	,	kIx,
Guernsey	Guernse	k1gMnPc4
a	a	k8xC
Jersey	Jerse	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Nejčastější	častý	k2eAgFnPc1d3
bankovky	bankovka	k1gFnPc1
v	v	k7c6
oběhu	oběh	k1gInSc6
mají	mít	k5eAaImIp3nP
hodnoty	hodnota	k1gFnPc4
£	£	k?
<g/>
5	#num#	k4
<g/>
,	,	kIx,
£	£	k?
<g/>
10	#num#	k4
<g/>
,	,	kIx,
£	£	k?
<g/>
20	#num#	k4
a	a	k8xC
£	£	k?
<g/>
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skotské	skotský	k2eAgInPc1d1
a	a	k8xC
severoirské	severoirský	k2eAgInPc1d1
banky	bank	k1gInPc1
vydávají	vydávat	k5eAaPmIp3nP,k5eAaImIp3nP
navíc	navíc	k6eAd1
bankovku	bankovka	k1gFnSc4
v	v	k7c6
nominální	nominální	k2eAgFnSc6d1
hodnotě	hodnota	k1gFnSc6
a	a	k8xC
£	£	k?
<g/>
100	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Bankovky	bankovka	k1gFnSc2
£	£	k?
<g/>
1	#num#	k4
existují	existovat	k5eAaImIp3nP
i	i	k9
na	na	k7c6
ostrovech	ostrov	k1gInPc6
Man	mana	k1gFnPc2
<g/>
,	,	kIx,
Guersey	Guersea	k1gFnSc2
a	a	k8xC
Jersey	Jersea	k1gFnSc2
(	(	kIx(
<g/>
a	a	k8xC
bankou	banka	k1gFnSc7
Royal	Royal	k1gMnSc1
Bank	bank	k1gInSc1
of	of	k?
Scotland	Scotland	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Severoirské	severoirský	k2eAgFnSc2d1
a	a	k8xC
skotské	skotský	k2eAgFnSc2d1
bankovky	bankovka	k1gFnSc2
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
kryty	krýt	k5eAaImNgFnP
ekvivalentním	ekvivalentní	k2eAgNnSc7d1
množstvím	množství	k1gNnSc7
bankovek	bankovka	k1gFnPc2
vydávaných	vydávaný	k2eAgFnPc2d1
Bank	banka	k1gFnPc2
of	of	k?
England	Englanda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
existují	existovat	k5eAaImIp3nP
bankovky	bankovka	k1gFnPc4
o	o	k7c6
nominálních	nominální	k2eAgFnPc6d1
hodnotách	hodnota	k1gFnPc6
£	£	k?
<g/>
1	#num#	k4
000	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
Giant	Giant	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
£	£	k?
<g/>
100	#num#	k4
000	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
Titan	titan	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
vydala	vydat	k5eAaPmAgFnS
Bank	bank	k1gInSc4
of	of	k?
England	Englanda	k1gFnPc2
a	a	k8xC
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
uloženy	uložit	k5eAaPmNgFnP
v	v	k7c6
jejím	její	k3xOp3gInSc6
depozitu	depozit	k1gInSc6
a	a	k8xC
nikdy	nikdy	k6eAd1
nebyly	být	k5eNaImAgInP
uvedeny	uvést	k5eAaPmNgInP
do	do	k7c2
oběhu	oběh	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Zámořské	zámořský	k2eAgNnSc4d1
území	území	k1gNnSc4
Svatá	svatý	k2eAgFnSc1d1
Helena	Helena	k1gFnSc1
<g/>
,	,	kIx,
Ascension	Ascension	k1gInSc1
a	a	k8xC
Tristan	Tristan	k1gInSc1
da	da	k?
Cunha	Cunha	k1gFnSc1
sestává	sestávat	k5eAaImIp3nS
ze	z	k7c2
tří	tři	k4xCgFnPc2
oddělených	oddělený	k2eAgFnPc2d1
součástí	součást	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
svatohelenská	svatohelenský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
na	na	k7c6
ostrovech	ostrov	k1gInPc6
Svatá	svatý	k2eAgFnSc1d1
Helena	Helena	k1gFnSc1
a	a	k8xC
Ascension	Ascension	k1gInSc1
<g/>
,	,	kIx,
britská	britský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
šterlinků	šterlink	k1gInPc2
je	být	k5eAaImIp3nS
používána	používat	k5eAaImNgFnS
na	na	k7c6
ostrově	ostrov	k1gInSc6
Tristan	Tristan	k1gInSc1
da	da	k?
Cunha	Cunh	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnPc6
dependencích	dependence	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.kurzy.cz/cnb/ekonomika/devizove-kurzy-ke-konci-mesice-249/gbp/	http://www.kurzy.cz/cnb/ekonomika/devizove-kurzy-ke-konci-mesice-249/gbp/	k4
GBP	GBP	kA
-	-	kIx~
Devizové	devizový	k2eAgInPc1d1
kurzy	kurz	k1gInPc1
ke	k	k7c3
konci	konec	k1gInSc3
měsíce	měsíc	k1gInSc2
-	-	kIx~
ekonomika	ekonomika	k1gFnSc1
ČNB	ČNB	kA
<g/>
,	,	kIx,
kurzy	kurz	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Co	co	k9
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
šterlink	šterlink	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
-	-	kIx~
Ontola	Ontola	k1gFnSc1
<g/>
.	.	kIx.
www.ontola.com	www.ontola.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Co	co	k9
(	(	kIx(
<g/>
možná	možná	k9
<g/>
)	)	kIx)
ještě	ještě	k6eAd1
nevíte	vědět	k5eNaImIp2nP
o	o	k7c6
britské	britský	k2eAgFnSc6d1
libře	libra	k1gFnSc6
|	|	kIx~
ZUNO	zuna	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
ZUNO	zuna	k1gFnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
https://www.mesec.cz/clanky/historicke-milniky-vyvoje-britske-meny/	https://www.mesec.cz/clanky/historicke-milniky-vyvoje-britske-meny/	k?
Historické	historický	k2eAgInPc1d1
milníky	milník	k1gInPc1
vývoje	vývoj	k1gInSc2
britské	britský	k2eAgFnSc2d1
měny	měna	k1gFnSc2
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
HÖFERT	HÖFERT	kA
<g/>
,	,	kIx,
Andreas	Andreas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
20	#num#	k4
<g/>
th	th	k?
anniversary	anniversara	k1gFnSc2
of	of	k?
the	the	k?
EMS	EMS	kA
crisis	crisis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
UBS	UBS	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
31	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Skotské	skotský	k2eAgFnSc2d1
a	a	k8xC
severoirské	severoirský	k2eAgFnSc2d1
bankovky	bankovka	k1gFnSc2
Current	Current	k1gMnSc1
Banknotes	Banknotes	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
Association	Association	k1gInSc4
of	of	k?
Commercial	Commercial	k1gInSc1
Banknote	Banknot	k1gInSc5
Issuers	Issuers	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vyobrazení	vyobrazení	k1gNnSc2
bankovek	bankovka	k1gFnPc2
jerseyské	jerseyský	k2eAgFnSc2d1
libry	libra	k1gFnSc2
<g/>
:	:	kIx,
On	on	k3xPp3gInSc1
Demand	Demand	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
States	States	k1gMnSc1
of	of	k?
Jersey	Jersea	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vyobrazení	vyobrazení	k1gNnSc2
bankovek	bankovka	k1gFnPc2
manské	manský	k2eAgFnSc2d1
libry	libra	k1gFnSc2
<g/>
:	:	kIx,
Know	Know	k1gFnSc1
your	your	k1gMnSc1
notes	notes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Isle	Isl	k1gInSc2
of	of	k?
Man	Man	k1gMnSc1
Government	Government	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vyobrazení	vyobrazení	k1gNnSc2
bankovek	bankovka	k1gFnPc2
guernseyské	guernseyský	k2eAgFnSc2d1
libry	libra	k1gFnSc2
<g/>
:	:	kIx,
Bank	bank	k1gInSc1
Notes	notes	k1gInSc1
issued	issued	k1gInSc4
by	by	kYmCp3nS
The	The	k1gMnSc1
States	States	k1gMnSc1
of	of	k?
Guernsey	Guernsea	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Philatelic	Philatelice	k1gFnPc2
Bureau	Bureaus	k1gInSc2
-	-	kIx~
Guernsey	Guernsey	k1gInPc4
Post	post	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Scottish	Scottish	k1gInSc1
and	and	k?
Northern	Northern	k1gInSc1
Ireland	Ireland	k1gInSc1
banknotes	banknotes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bank	bank	k1gInSc1
of	of	k?
England	England	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
A	a	k9
history	histor	k1gMnPc4
of	of	k?
the	the	k?
British	British	k1gInSc1
banknote	banknot	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Telegraph	Telegraph	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
měn	měna	k1gFnPc2
Evropy	Evropa	k1gFnSc2
</s>
<s>
Jerseyská	Jerseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
</s>
<s>
Guernseyská	Guernseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
</s>
<s>
Manská	manský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
</s>
<s>
Gibraltarská	gibraltarský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
</s>
<s>
Svatohelenská	Svatohelenský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
</s>
<s>
Falklandská	Falklandský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
libra	libra	k1gFnSc1
šterlinků	šterlink	k1gInPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Libra	libra	k1gFnSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Bankovky	bankovka	k1gFnSc2
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Bankovky	bankovka	k1gFnPc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
Skotska	Skotsko	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Bankovky	bankovka	k1gFnPc1
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
Severního	severní	k2eAgNnSc2d1
Irska	Irsko	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Měny	měna	k1gFnSc2
Evropy	Evropa	k1gFnSc2
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Bulharský	bulharský	k2eAgInSc1d1
lev	lev	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Dánská	dánský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Chorvatská	chorvatský	k2eAgFnSc1d1
kuna	kuna	k1gFnSc1
•	•	k?
Euro	euro	k1gNnSc1
•	•	k?
Maďarský	maďarský	k2eAgInSc1d1
forint	forint	k1gInSc1
•	•	k?
Polský	polský	k2eAgInSc1d1
zlotý	zlotý	k1gInSc1
•	•	k?
Rumunský	rumunský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Švédská	švédský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
Východní	východní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Běloruský	běloruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Gruzínský	gruzínský	k2eAgInSc1d1
lari	lari	k1gInSc1
•	•	k?
Moldavský	moldavský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Ruský	ruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Ukrajinská	ukrajinský	k2eAgFnSc1d1
hřivna	hřivna	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
Podněsterský	podněsterský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
<g/>
)	)	kIx)
Jižní	jižní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Albánský	albánský	k2eAgInSc1d1
lek	lek	k1gInSc1
•	•	k?
Bosenskohercegovská	bosenskohercegovský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Makedonský	makedonský	k2eAgInSc1d1
denár	denár	k1gInSc1
•	•	k?
Srbský	srbský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
Západní	západní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Britská	britský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
šterlinků	šterlink	k1gInPc2
•	•	k?
Faerská	Faerský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Gibraltarská	gibraltarský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Guernseyská	Guernseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Islandská	islandský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Jerseyská	Jerseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Manská	manský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Norská	norský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Švýcarský	švýcarský	k2eAgInSc4d1
frank	frank	k1gInSc4
</s>
<s>
Globální	globální	k2eAgFnSc2d1
rezervní	rezervní	k2eAgFnSc2d1
měny	měna	k1gFnSc2
</s>
<s>
Americký	americký	k2eAgInSc1d1
dolar	dolar	k1gInSc1
(	(	kIx(
<g/>
USD	USD	kA
<g/>
)	)	kIx)
•	•	k?
Euro	euro	k1gNnSc1
(	(	kIx(
<g/>
EUR	euro	k1gNnPc2
<g/>
)	)	kIx)
•	•	k?
Čínský	čínský	k2eAgInSc4d1
jüan	jüan	k1gInSc4
(	(	kIx(
<g/>
CNY	CNY	kA
<g/>
)	)	kIx)
•	•	k?
Britská	britský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
(	(	kIx(
<g/>
GBP	GBP	kA
<g/>
)	)	kIx)
•	•	k?
Japonský	japonský	k2eAgMnSc1d1
jen	jen	k9
(	(	kIx(
<g/>
JPY	JPY	kA
<g/>
)	)	kIx)
•	•	k?
Švýcarský	švýcarský	k2eAgInSc4d1
frank	frank	k1gInSc4
(	(	kIx(
<g/>
CHF	CHF	kA
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4174107-9	4174107-9	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
