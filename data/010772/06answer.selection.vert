<s>
Mezi	mezi	k7c4	mezi
legendy	legenda	k1gFnPc4	legenda
ale	ale	k8xC	ale
mezitím	mezitím	k6eAd1	mezitím
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1952	[number]	k4	1952
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
5	[number]	k4	5
km	km	kA	km
<g/>
,	,	kIx,	,
10	[number]	k4	10
km	km	kA	km
a	a	k8xC	a
v	v	k7c6	v
maratonu	maraton	k1gInSc6	maraton
<g/>
.	.	kIx.	.
</s>
