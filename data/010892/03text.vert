<p>
<s>
Vicki	Vicki	k6eAd1	Vicki
Baumová	Baumový	k2eAgFnSc1d1	Baumový
(	(	kIx(	(
<g/>
nepřechýleně	přechýleně	k6eNd1	přechýleně
Vicki	Vicki	k1gNnSc1	Vicki
Baum	Bauma	k1gFnPc2	Bauma
<g/>
;	;	kIx,	;
24	[number]	k4	24
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
Hollywood	Hollywood	k1gInSc1	Hollywood
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
původem	původ	k1gInSc7	původ
rakouská	rakouský	k2eAgFnSc1d1	rakouská
harfenistka	harfenistka	k1gFnSc1	harfenistka
<g/>
,	,	kIx,	,
scenáristka	scenáristka	k1gFnSc1	scenáristka
<g/>
,	,	kIx,	,
novinářka	novinářka	k1gFnSc1	novinářka
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
řadící	řadící	k2eAgFnSc1d1	řadící
se	se	k3xPyFc4	se
k	k	k7c3	k
literárnímu	literární	k2eAgNnSc3d1	literární
hnutí	hnutí	k1gNnSc3	hnutí
Nová	nový	k2eAgFnSc1d1	nová
věcnost	věcnost	k1gFnSc1	věcnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
jako	jako	k8xS	jako
Hedwig	Hedwig	k1gMnSc1	Hedwig
Baumová	Baumová	k1gFnSc1	Baumová
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
jako	jako	k8xC	jako
jedináček	jedináček	k1gMnSc1	jedináček
v	v	k7c6	v
dobře	dobře	k6eAd1	dobře
situované	situovaný	k2eAgFnSc6d1	situovaná
židovské	židovský	k2eAgFnSc6d1	židovská
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
otec	otec	k1gMnSc1	otec
Hermann	Hermann	k1gMnSc1	Hermann
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
avšak	avšak	k8xC	avšak
míti	mít	k5eAaImF	mít
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
jmenovati	jmenovat	k5eAaImF	jmenovat
Viktor	Viktor	k1gMnSc1	Viktor
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
z	z	k7c2	z
Hedwigy	Hedwiga	k1gFnSc2	Hedwiga
stala	stát	k5eAaPmAgFnS	stát
Vicki	Vicke	k1gFnSc4	Vicke
<g/>
.	.	kIx.	.
</s>
<s>
Nevyrůstala	vyrůstat	k5eNaImAgFnS	vyrůstat
ale	ale	k8xC	ale
vůbec	vůbec	k9	vůbec
v	v	k7c6	v
idylickém	idylický	k2eAgNnSc6d1	idylické
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
povoláním	povolání	k1gNnSc7	povolání
bankovní	bankovní	k2eAgMnSc1d1	bankovní
úředník	úředník	k1gMnSc1	úředník
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
hrubián	hrubián	k1gMnSc1	hrubián
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
zakazoval	zakazovat	k5eAaImAgMnS	zakazovat
dokonce	dokonce	k9	dokonce
čísti	číst	k5eAaImF	číst
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
Mathilde	Mathild	k1gInSc5	Mathild
zase	zase	k9	zase
trpěla	trpět	k5eAaImAgFnS	trpět
psychickými	psychický	k2eAgInPc7d1	psychický
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
jediné	jediný	k2eAgNnSc4d1	jediné
útočiště	útočiště	k1gNnSc4	útočiště
nalézala	nalézat	k5eAaImAgFnS	nalézat
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
dědečka	dědeček	k1gMnSc2	dědeček
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
hudební	hudební	k2eAgNnSc4d1	hudební
nadání	nadání	k1gNnSc4	nadání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
harfu	harfa	k1gFnSc4	harfa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvním	první	k4xOgInSc6	první
rozvodu	rozvod	k1gInSc6	rozvod
<g/>
,	,	kIx,	,
následném	následný	k2eAgInSc6d1	následný
novém	nový	k2eAgInSc6d1	nový
vztahu	vztah	k1gInSc6	vztah
a	a	k8xC	a
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
na	na	k7c4	na
svět	svět	k1gInSc4	svět
dvou	dva	k4xCgMnPc2	dva
synů	syn	k1gMnPc2	syn
se	se	k3xPyFc4	se
ale	ale	k9	ale
přiklonila	přiklonit	k5eAaPmAgFnS	přiklonit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1926	[number]	k4	1926
<g/>
–	–	k?	–
<g/>
1931	[number]	k4	1931
byla	být	k5eAaImAgFnS	být
zaměstnána	zaměstnat	k5eAaPmNgFnS	zaměstnat
v	v	k7c6	v
berlínském	berlínský	k2eAgNnSc6d1	berlínské
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Ullstein-Verlag	Ullstein-Verlaga	k1gFnPc2	Ullstein-Verlaga
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
redakčně	redakčně	k6eAd1	redakčně
starala	starat	k5eAaImAgFnS	starat
o	o	k7c4	o
časopis	časopis	k1gInSc4	časopis
Die	Die	k1gMnSc2	Die
Dame	Dam	k1gMnSc2	Dam
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
první	první	k4xOgInSc1	první
román	román	k1gInSc1	román
Menschen	Menschen	k1gInSc1	Menschen
im	im	k?	im
Hotel	hotel	k1gInSc1	hotel
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
období	období	k1gNnSc6	období
nebývalý	bývalý	k2eNgInSc4d1	bývalý
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Dočkal	dočkat	k5eAaPmAgMnS	dočkat
se	se	k3xPyFc4	se
nejenom	nejenom	k6eAd1	nejenom
nejednoho	nejeden	k4xCyIgNnSc2	nejeden
divadelního	divadelní	k2eAgNnSc2d1	divadelní
uvedení	uvedení	k1gNnSc2	uvedení
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k9	i
toho	ten	k3xDgInSc2	ten
filmového	filmový	k2eAgInSc2d1	filmový
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
Grand	grand	k1gMnSc1	grand
Hotel	hotel	k1gInSc1	hotel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
např.	např.	kA	např.
i	i	k8xC	i
herečka	herečka	k1gFnSc1	herečka
Greta	Gret	k1gInSc2	Gret
Garbo	Garba	k1gFnSc5	Garba
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
podílela	podílet	k5eAaImAgFnS	podílet
jako	jako	k9	jako
scenáristka	scenáristka	k1gFnSc1	scenáristka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literární	literární	k2eAgInSc1d1	literární
prototyp	prototyp	k1gInSc1	prototyp
tzv.	tzv.	kA	tzv.
Nové	Nové	k2eAgFnSc2d1	Nové
ženy	žena	k1gFnSc2	žena
===	===	k?	===
</s>
</p>
<p>
<s>
Spadá	spadat	k5eAaPmIp3nS	spadat
typově	typově	k6eAd1	typově
do	do	k7c2	do
výmarského	výmarský	k2eAgInSc2d1	výmarský
emancipačního	emancipační	k2eAgInSc2d1	emancipační
ideálu	ideál	k1gInSc2	ideál
<g/>
,	,	kIx,	,
moderního	moderní	k2eAgInSc2d1	moderní
kultu	kult	k1gInSc2	kult
tzv.	tzv.	kA	tzv.
Nové	Nová	k1gFnSc2	Nová
ženy	žena	k1gFnSc2	žena
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Neue	Neue	k1gNnSc1	Neue
Frau	Fraus	k1gInSc2	Fraus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
již	již	k6eAd1	již
dokáže	dokázat	k5eAaPmIp3nS	dokázat
navzdory	navzdory	k7c3	navzdory
všem	všecek	k3xTgFnPc3	všecek
předchozím	předchozí	k2eAgFnPc3d1	předchozí
nesnázím	nesnáz	k1gFnPc3	nesnáz
prosaditi	prosadit	k5eAaPmF	prosadit
a	a	k8xC	a
realizovati	realizovat	k5eAaBmF	realizovat
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ryze	ryze	k6eAd1	ryze
mužské	mužský	k2eAgFnSc2d1	mužská
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
jež	jenž	k3xRgFnSc1	jenž
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
mužského	mužský	k2eAgNnSc2d1	mužské
nazírání	nazírání	k1gNnSc2	nazírání
na	na	k7c4	na
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
klasickou	klasický	k2eAgFnSc7d1	klasická
představitelkou	představitelka	k1gFnSc7	představitelka
výmarské	výmarský	k2eAgFnSc2d1	Výmarská
dívčí	dívčí	k2eAgFnSc2d1	dívčí
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
tvorbě	tvorba	k1gFnSc6	tvorba
rezonují	rezonovat	k5eAaImIp3nP	rezonovat
specificky	specificky	k6eAd1	specificky
ženská	ženský	k2eAgNnPc1d1	ženské
témata	téma	k1gNnPc1	téma
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
studium	studium	k1gNnSc1	studium
<g/>
,	,	kIx,	,
potrat	potrat	k1gInSc1	potrat
<g/>
,	,	kIx,	,
mateřství	mateřství	k1gNnSc1	mateřství
<g/>
,	,	kIx,	,
či	či	k8xC	či
manželství	manželství	k1gNnSc1	manželství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
necílí	cílit	k5eNaImIp3nS	cílit
primárně	primárně	k6eAd1	primárně
pouze	pouze	k6eAd1	pouze
a	a	k8xC	a
jenom	jenom	k9	jenom
na	na	k7c4	na
ženské	ženský	k2eAgNnSc4d1	ženské
publikum	publikum	k1gNnSc4	publikum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1906	[number]	k4	1906
<g/>
–	–	k?	–
<g/>
1910	[number]	k4	1910
byla	být	k5eAaImAgFnS	být
provdána	provdán	k2eAgFnSc1d1	provdána
za	za	k7c2	za
rakouského	rakouský	k2eAgMnSc2d1	rakouský
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
novináře	novinář	k1gMnSc2	novinář
Maxe	Max	k1gMnSc2	Max
Presla	Presla	k1gMnSc2	Presla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
se	se	k3xPyFc4	se
vdala	vdát	k5eAaPmAgFnS	vdát
podruhé	podruhé	k6eAd1	podruhé
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
dirigenta	dirigent	k1gMnSc4	dirigent
Richarda	Richard	k1gMnSc4	Richard
Lerta	Lert	k1gMnSc4	Lert
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
také	také	k9	také
porodila	porodit	k5eAaPmAgFnS	porodit
posléze	posléze	k6eAd1	posléze
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
;	;	kIx,	;
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
již	již	k6eAd1	již
začíná	začínat	k5eAaImIp3nS	začínat
ale	ale	k8xC	ale
více	hodně	k6eAd2	hodně
věnovati	věnovat	k5eAaPmF	věnovat
samotnému	samotný	k2eAgNnSc3d1	samotné
psaní	psaní	k1gNnSc3	psaní
nežli	nežli	k8xS	nežli
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pobývala	pobývat	k5eAaImAgFnS	pobývat
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
,	,	kIx,	,
národní	národní	k2eAgMnPc1d1	národní
socialisté	socialist	k1gMnPc1	socialist
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
pálili	pálit	k5eAaImAgMnP	pálit
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
její	její	k3xOp3gFnSc2	její
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
získala	získat	k5eAaPmAgFnS	získat
americké	americký	k2eAgNnSc4d1	americké
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
psala	psát	k5eAaImAgFnS	psát
již	již	k6eAd1	již
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
jí	on	k3xPp3gFnSc3	on
byla	být	k5eAaImAgFnS	být
diagnostikována	diagnostikovat	k5eAaBmNgFnS	diagnostikovat
leukemie	leukemie	k1gFnSc1	leukemie
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yRgInPc4	který
zpočátku	zpočátku	k6eAd1	zpočátku
nikomu	nikdo	k3yNnSc3	nikdo
nic	nic	k3yNnSc1	nic
neřekla	říct	k5eNaPmAgFnS	říct
<g/>
,	,	kIx,	,
onemocnění	onemocnění	k1gNnPc2	onemocnění
i	i	k8xC	i
před	před	k7c7	před
svojí	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
tajila	tajit	k5eAaImAgFnS	tajit
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1960	[number]	k4	1960
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
České	český	k2eAgInPc1d1	český
překlady	překlad	k1gInPc1	překlad
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
==	==	k?	==
</s>
</p>
<p>
<s>
Zlaté	zlatý	k2eAgInPc1d1	zlatý
střevíčky	střevíček	k1gInPc1	střevíček
<g/>
:	:	kIx,	:
román	román	k1gInSc1	román
primabaleríny	primabalerína	k1gFnSc2	primabalerína
(	(	kIx(	(
<g/>
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Die	Die	k1gFnSc7	Die
goldenen	goldenen	k2eAgInSc4d1	goldenen
Schuhe	Schuhe	k1gInSc4	Schuhe
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
339	[number]	k4	339
S.	S.	kA	S.
Překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Klára	Klára	k1gFnSc1	Klára
Vachulová	Vachulový	k2eAgFnSc1d1	Vachulová
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
výprodej	výprodej	k1gInSc1	výprodej
(	(	kIx(	(
<g/>
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Der	drát	k5eAaImRp2nS	drát
grosse	grosse	k1gFnSc2	grosse
Ausverkauf	Ausverkauf	k1gInSc1	Ausverkauf
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Julius	Julius	k1gMnSc1	Julius
Albert	Albert	k1gMnSc1	Albert
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
229	[number]	k4	229
S.	S.	kA	S.
Překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Pavel	Pavel	k1gMnSc1	Pavel
Janda	Janda	k1gMnSc1	Janda
</s>
</p>
<p>
<s>
Kariéra	kariéra	k1gFnSc1	kariéra
Dory	Dora	k1gFnSc2	Dora
Hartové	Hartová	k1gFnSc2	Hartová
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Die	Die	k1gMnSc5	Die
Karriere	Karrier	k1gMnSc5	Karrier
der	drát	k5eAaImRp2nS	drát
Doris	Doris	k1gFnPc3	Doris
Hart	Hart	k1gInSc4	Hart
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Jos	Jos	k1gFnPc6	Jos
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
334	[number]	k4	334
S.	S.	kA	S.
</s>
</p>
<p>
<s>
Lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
(	(	kIx(	(
<g/>
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Menschen	Menschen	k1gInSc1	Menschen
im	im	k?	im
Hotel	hotel	k1gInSc1	hotel
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Sfinx	sfinx	k1gInSc1	sfinx
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Janda	Janda	k1gMnSc1	Janda
<g/>
.	.	kIx.	.
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
275	[number]	k4	275
S.	S.	kA	S.
Překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Kovárna	kovárna	k1gFnSc1	kovárna
</s>
</p>
<p>
<s>
Studující	studující	k2eAgFnSc1d1	studující
chemie	chemie	k1gFnSc1	chemie
Helena	Helena	k1gFnSc1	Helena
Willfüerová	Willfüerová	k1gFnSc1	Willfüerová
<g/>
:	:	kIx,	:
román	román	k1gInSc1	román
mladé	mladý	k2eAgFnSc2d1	mladá
dívky	dívka	k1gFnSc2	dívka
naší	náš	k3xOp1gFnSc2	náš
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Stud	stud	k1gInSc1	stud
<g/>
.	.	kIx.	.
chem.	chem.	k?	chem.
Helene	Helen	k1gInSc5	Helen
Willfüer	Willfüer	k1gMnSc1	Willfüer
<g/>
:	:	kIx,	:
Roman	Roman	k1gMnSc1	Roman
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Plamja	Plamj	k1gInSc2	Plamj
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
254	[number]	k4	254
S.	S.	kA	S.
Překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
Hynek	Hynek	k1gMnSc1	Hynek
Bruner	Bruner	k1gMnSc1	Bruner
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vicki	Vick	k1gFnSc2	Vick
Baumová	Baumová	k1gFnSc1	Baumová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc1	heslo
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
Databazeknih	Databazekniha	k1gFnPc2	Databazekniha
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
