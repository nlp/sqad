<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
se	se	k3xPyFc4	se
vdala	vdát	k5eAaPmAgFnS	vdát
podruhé	podruhé	k6eAd1	podruhé
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
dirigenta	dirigent	k1gMnSc4	dirigent
Richarda	Richard	k1gMnSc4	Richard
Lerta	Lert	k1gMnSc4	Lert
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
také	také	k9	také
porodila	porodit	k5eAaPmAgFnS	porodit
posléze	posléze	k6eAd1	posléze
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
;	;	kIx,	;
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
již	již	k6eAd1	již
začíná	začínat	k5eAaImIp3nS	začínat
ale	ale	k8xC	ale
více	hodně	k6eAd2	hodně
věnovati	věnovat	k5eAaImF	věnovat
samotnému	samotný	k2eAgNnSc3d1	samotné
psaní	psaní	k1gNnSc3	psaní
nežli	nežli	k8xS	nežli
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
