<s>
Eliška	Eliška	k1gFnSc1
Křenková	Křenková	k1gFnSc1
</s>
<s>
MgA.	MgA.	k?
Eliška	Eliška	k1gFnSc1
KřenkováRodné	KřenkováRodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Eliška	Eliška	k1gFnSc1
Křenková	Křenková	k1gFnSc1
Narození	narození	k1gNnPc5
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1990	#num#	k4
(	(	kIx(
<g/>
31	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
PrahaČeskoslovensko	PrahaČeskoslovensko	k1gNnSc1
Československo	Československo	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
AMU	AMU	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
Aktivní	aktivní	k2eAgInPc4d1
roky	rok	k1gInPc4
</s>
<s>
2005	#num#	k4
<g/>
–	–	k?
Český	český	k2eAgMnSc1d1
lev	lev	k1gMnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
herečka	herečka	k1gFnSc1
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
2018	#num#	k4
–	–	k?
Všechno	všechen	k3xTgNnSc1
bude	být	k5eAaImBp3nS
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Eliška	Eliška	k1gFnSc1
Křenková	Křenková	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
31	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1990	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
výkon	výkon	k1gInSc4
v	v	k7c6
Omerzuově	Omerzuův	k2eAgInSc6d1
filmu	film	k1gInSc6
Všechno	všechen	k3xTgNnSc1
bude	být	k5eAaImBp3nS
obdržela	obdržet	k5eAaPmAgFnS
roku	rok	k1gInSc2
2018	#num#	k4
Českého	český	k2eAgInSc2d1
lva	lev	k1gInSc2
pro	pro	k7c4
nejlepší	dobrý	k2eAgFnSc4d3
herečku	herečka	k1gFnSc4
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
absolvovala	absolvovat	k5eAaPmAgFnS
na	na	k7c6
Divadelní	divadelní	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
Akademie	akademie	k1gFnSc2
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
magisterský	magisterský	k2eAgInSc1d1
obor	obor	k1gInSc1
herectví	herectví	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c6
katedře	katedra	k1gFnSc6
alternativního	alternativní	k2eAgNnSc2d1
a	a	k8xC
loutkového	loutkový	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
před	před	k7c7
kamerou	kamera	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
v	v	k7c6
seriálu	seriál	k1gInSc6
Ulice	ulice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
hrála	hrát	k5eAaImAgFnS
menší	malý	k2eAgFnSc1d2
role	role	k1gFnSc1
v	v	k7c6
několika	několik	k4yIc6
filmech	film	k1gInPc6
jako	jako	k8xC,k8xS
Rafťáci	Rafťák	k1gMnPc1
<g/>
,	,	kIx,
Škola	škola	k1gFnSc1
ve	v	k7c6
mlejně	mlejna	k1gFnSc6
a	a	k8xC
ve	v	k7c6
studentské	studentský	k2eAgFnSc6d1
krátkometrážní	krátkometrážní	k2eAgFnSc6d1
komedii	komedie	k1gFnSc6
Vojtěcha	Vojtěch	k1gMnSc2
Kotka	Kotek	k1gMnSc2
Ctrl	Ctrl	kA
Emotion	Emotion	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ztvárnila	ztvárnit	k5eAaPmAgFnS
také	také	k9
postavy	postava	k1gFnPc4
ve	v	k7c6
filmu	film	k1gInSc6
Muži	muž	k1gMnPc1
v	v	k7c6
naději	naděje	k1gFnSc6
a	a	k8xC
seriálu	seriál	k1gInSc6
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
Vyprávěj	vyprávět	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s>
Filmografie	filmografie	k1gFnSc1
</s>
<s>
Filmy	film	k1gInPc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Role	role	k1gFnSc1
</s>
<s>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
2006	#num#	k4
</s>
<s>
Rafťáci	Rafťák	k1gMnPc1
</s>
<s>
Vendula	Vendula	k1gFnSc1
</s>
<s>
2009	#num#	k4
</s>
<s>
Čerpací	čerpací	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
</s>
<s>
zákaznice	zákaznice	k1gFnSc1
</s>
<s>
studentský	studentský	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Ctrl	Ctrl	kA
Emotion	Emotion	k1gInSc1
</s>
<s>
2011	#num#	k4
</s>
<s>
Muži	muž	k1gMnPc1
v	v	k7c6
naději	naděje	k1gFnSc6
</s>
<s>
Bára	Bára	k1gFnSc1
</s>
<s>
2012	#num#	k4
</s>
<s>
Probudím	probudit	k5eAaPmIp1nS
se	se	k3xPyFc4
včera	včera	k6eAd1
</s>
<s>
18	#num#	k4
<g/>
letá	letý	k2eAgFnSc1d1
Zuzana	Zuzana	k1gFnSc1
</s>
<s>
2014	#num#	k4
</s>
<s>
Andělé	anděl	k1gMnPc1
všedního	všední	k2eAgInSc2d1
dne	den	k1gInSc2
</s>
<s>
anděl	anděl	k1gMnSc1
Ilmuth	Ilmuth	k1gMnSc1
</s>
<s>
Parádně	parádně	k6eAd1
pokecal	pokecat	k5eAaPmAgMnS
</s>
<s>
Všiváci	všivák	k1gMnPc1
</s>
<s>
Jana	Jana	k1gFnSc1
</s>
<s>
2015	#num#	k4
</s>
<s>
Rodinný	rodinný	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Kristýna	Kristýna	k1gFnSc1
</s>
<s>
Padesátka	padesátka	k1gFnSc1
</s>
<s>
Ilona	Ilona	k1gFnSc1
</s>
<s>
2017	#num#	k4
</s>
<s>
Anna	Anna	k1gFnSc1
</s>
<s>
krátkometrážní	krátkometrážní	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
Křižáček	křižáček	k1gMnSc1
</s>
<s>
2018	#num#	k4
</s>
<s>
Všechno	všechen	k3xTgNnSc1
bude	být	k5eAaImBp3nS
</s>
<s>
Bára	Bára	k1gFnSc1
</s>
<s>
Český	český	k2eAgMnSc1d1
lev	lev	k1gMnSc1
–	–	k?
nejlepší	dobrý	k2eAgFnSc1d3
herečka	herečka	k1gFnSc1
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
</s>
<s>
2019	#num#	k4
</s>
<s>
Tiché	Tiché	k2eAgInPc1d1
doteky	dotek	k1gInPc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mia	Mia	k?
</s>
<s>
Jiří	Jiří	k1gMnSc1
pes	pes	k1gMnSc1
uprchlík	uprchlík	k1gMnSc1
</s>
<s>
krátkometrážní	krátkometrážní	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
2020	#num#	k4
</s>
<s>
Princezna	princezna	k1gFnSc1
zakletá	zakletý	k2eAgFnSc1d1
v	v	k7c6
čase	čas	k1gInSc6
</s>
<s>
Amélie	Amélie	k1gFnSc1
</s>
<s>
Televize	televize	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Role	role	k1gFnSc1
</s>
<s>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
2005	#num#	k4
</s>
<s>
Ulice	ulice	k1gFnSc1
</s>
<s>
Eliška	Eliška	k1gFnSc1
Pávková	Pávková	k1gFnSc1
</s>
<s>
2007	#num#	k4
</s>
<s>
Škola	škola	k1gFnSc1
ve	v	k7c6
mlejně	mlejna	k1gFnSc6
</s>
<s>
žákyně	žákyně	k1gFnSc1
Měchulová	Měchulový	k2eAgFnSc1d1
</s>
<s>
TV	TV	kA
film	film	k1gInSc1
</s>
<s>
2010	#num#	k4
</s>
<s>
Běžci	běžec	k1gMnPc1
</s>
<s>
Pětka	pětka	k1gFnSc1
</s>
<s>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
</s>
<s>
Borgia	Borgia	k1gFnSc1
</s>
<s>
Sancia	Sancia	k1gFnSc1
Squillace	Squillace	k1gFnSc2
</s>
<s>
4	#num#	k4
díly	dílo	k1gNnPc7
</s>
<s>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
</s>
<s>
Vyprávěj	vyprávět	k5eAaImRp2nS
</s>
<s>
Lucie	Lucie	k1gFnSc1
Francová	Francová	k1gFnSc1
</s>
<s>
23	#num#	k4
dílů	díl	k1gInPc2
</s>
<s>
2013	#num#	k4
</s>
<s>
Škoda	škoda	k1gFnSc1
lásky	láska	k1gFnSc2
</s>
<s>
1	#num#	k4
díl	díl	k1gInSc1
(	(	kIx(
<g/>
Přirozený	přirozený	k2eAgInSc1d1
talent	talent	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2014	#num#	k4
</s>
<s>
Neviditelní	viditelný	k2eNgMnPc1d1
</s>
<s>
2015	#num#	k4
</s>
<s>
Vinaři	vinař	k1gMnPc1
</s>
<s>
Klára	Klára	k1gFnSc1
Hamplová	Hamplová	k1gFnSc1
</s>
<s>
16	#num#	k4
dílů	díl	k1gInPc2
</s>
<s>
Vraždy	vražda	k1gFnPc1
v	v	k7c6
kruhu	kruh	k1gInSc6
</s>
<s>
Erika	Erika	k1gFnSc1
Hašková	Hašková	k1gFnSc1
</s>
<s>
1	#num#	k4
díl	díl	k1gInSc1
(	(	kIx(
<g/>
Na	na	k7c6
nejvyšší	vysoký	k2eAgFnSc6d3
úrovni	úroveň	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
2016	#num#	k4
</s>
<s>
Pustina	pustina	k1gFnSc1
</s>
<s>
Klára	Klára	k1gFnSc1
Sikorová	Sikorová	k1gFnSc1
</s>
<s>
8	#num#	k4
dílů	díl	k1gInPc2
</s>
<s>
Já	já	k3xPp1nSc1
<g/>
,	,	kIx,
Mattoni	Matton	k1gMnPc1
</s>
<s>
pokojská	pokojská	k1gFnSc1
Luci	Luci	k?
</s>
<s>
1	#num#	k4
díl	díl	k1gInSc1
(	(	kIx(
<g/>
Láska	láska	k1gFnSc1
a	a	k8xC
intriky	intrika	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
2018	#num#	k4
</s>
<s>
Specialisté	specialista	k1gMnPc1
</s>
<s>
prap.	prap.	k?
Monika	Monika	k1gFnSc1
Švarcová	Švarcová	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
od	od	k7c2
2	#num#	k4
<g/>
.	.	kIx.
řady	řada	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Přijela	přijet	k5eAaPmAgFnS
pouť	pouť	k1gFnSc1
</s>
<s>
Mia	Mia	k?
</s>
<s>
minisérie	minisérie	k1gFnSc1
</s>
<s>
2019	#num#	k4
</s>
<s>
Všechno	všechen	k3xTgNnSc1
je	být	k5eAaImIp3nS
jinak	jinak	k6eAd1
</s>
<s>
Lištice	Lištice	k1gFnSc1
</s>
<s>
6	#num#	k4
dílů	díl	k1gInPc2
</s>
<s>
Sever	sever	k1gInSc1
</s>
<s>
Lucie	Lucie	k1gFnSc1
Svobodová	Svobodová	k1gFnSc1
</s>
<s>
televizní	televizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
</s>
<s>
Jak	jak	k6eAd1
si	se	k3xPyFc3
nepodělat	podělat	k5eNaPmF
život	život	k1gInSc4
</s>
<s>
Lenka	Lenka	k1gFnSc1
</s>
<s>
díl	díl	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Non-stop	Non-stop	k1gInSc1
lahůdky	lahůdka	k1gFnSc2
<g/>
“	“	k?
</s>
<s>
2021	#num#	k4
</s>
<s>
Boží	boží	k2eAgInPc1d1
mlýny	mlýn	k1gInPc1
</s>
<s>
Markéta	Markéta	k1gFnSc1
Stránská	Stránská	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Krásna	krásno	k1gNnSc2
Eliška	Eliška	k1gFnSc1
Křenková	Křenková	k1gFnSc1
zo	zo	k?
seriálu	seriál	k1gInSc2
Vyprávěj	vyprávět	k5eAaImRp2nS
<g/>
:	:	kIx,
Štúdium	Štúdium	k1gNnSc1
bol	bola	k1gFnPc2
horor	horor	k1gInSc1
<g/>
,	,	kIx,
Pravda	pravda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
<g/>
↑	↑	k?
http://www.damu.cz/katedry-a-kabinety/katedra-alternativniho-a-loutkoveho-divadla/studenti/studenti-oboru-herectvi-ald-1/1-rocnik-herectvi-mgr-1/eliska-krenkova/view%5B%5D	http://www.damu.cz/katedry-a-kabinety/katedra-alternativniho-a-loutkoveho-divadla/studenti/studenti-oboru-herectvi-ald-1/1-rocnik-herectvi-mgr-1/eliska-krenkova/view%5B%5D	k4
<g/>
↑	↑	k?
Eliska	Eliska	k1gFnSc1
Krenková	Krenková	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IMDb	IMDb	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Eliška	Eliška	k1gFnSc1
Křenková	Křenková	k1gFnSc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Eliška	Eliška	k1gFnSc1
Křenková	Křenková	k1gFnSc1
ve	v	k7c6
Filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Český	český	k2eAgInSc1d1
lev	lev	k1gInSc1
za	za	k7c4
nejlepší	dobrý	k2eAgInSc4d3
ženský	ženský	k2eAgInSc4d1
herecký	herecký	k2eAgInSc4d1
výkon	výkon	k1gInSc4
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
</s>
<s>
Jana	Jana	k1gFnSc1
Preissová	Preissová	k1gFnSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Tereza	Tereza	k1gFnSc1
Brodská	Brodská	k1gFnSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Veronika	Veronika	k1gFnSc1
Žilková	Žilková	k1gFnSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Klára	Klára	k1gFnSc1
Issová	Issová	k1gFnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Agnieszka	Agnieszka	k1gFnSc1
Siteková	Siteková	k1gFnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Anna	Anna	k1gFnSc1
Geislerová	Geislerová	k1gFnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Eva	Eva	k1gFnSc1
Holubová	Holubová	k1gFnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Zuzana	Zuzana	k1gFnSc1
Kronerová	Kronerová	k1gFnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jana	Jana	k1gFnSc1
Hubinská	Hubinský	k2eAgFnSc1d1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Vilma	Vilma	k1gFnSc1
Cibulková	Cibulková	k1gFnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Klára	Klára	k1gFnSc1
Melíšková	Melíšková	k1gFnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Anna	Anna	k1gFnSc1
Geislerová	Geislerová	k1gFnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jana	Jana	k1gFnSc1
Brejchová	Brejchová	k1gFnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Zuzana	Zuzana	k1gFnSc1
Bydžovská	Bydžovská	k1gFnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Lenka	Lenka	k1gFnSc1
Termerová	Termerová	k1gFnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Daniela	Daniela	k1gFnSc1
Kolářová	Kolářová	k1gFnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Eliška	Eliška	k1gFnSc1
Balzerová	Balzerová	k1gFnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Taťjana	Taťjana	k1gFnSc1
Medvecká	Medvecký	k2eAgFnSc1d1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Klára	Klára	k1gFnSc1
Melíšková	Melíšková	k1gFnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jaroslava	Jaroslava	k1gFnSc1
Pokorná	Pokorná	k1gFnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Lenka	Lenka	k1gFnSc1
Krobotová	Krobotový	k2eAgFnSc1d1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Lucie	Lucie	k1gFnSc1
Žáčková	Žáčková	k1gFnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Klára	Klára	k1gFnSc1
Melíšková	Melíšková	k1gFnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Petra	Petra	k1gFnSc1
Špalková	špalkový	k2eAgFnSc1d1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Eliška	Eliška	k1gFnSc1
Křenková	Křenková	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Klára	Klára	k1gFnSc1
Melíšková	Melíšková	k1gFnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Petra	Petra	k1gFnSc1
Špalková	špalkový	k2eAgFnSc1d1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
172608	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1076340784	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
1709	#num#	k4
9043	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
305104321	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
</s>
