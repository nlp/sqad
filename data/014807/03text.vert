<s>
Kościuszkovo	Kościuszkův	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
</s>
<s>
Tadeusz	Tadeusz	k1gMnSc1
Kościuszko	Kościuszka	k1gFnSc5
skládá	skládat	k5eAaImIp3nS
24	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1794	#num#	k4
přísahu	přísaha	k1gFnSc4
na	na	k7c6
Hlavním	hlavní	k2eAgNnSc6d1
krakovském	krakovský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
</s>
<s>
Kościuszkovo	Kościuszkův	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
Insurekcja	Insurekcj	k2eAgFnSc1d1
kościuszkowska	kościuszkowska	k1gFnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
vzpoura	vzpoura	k1gFnSc1
proti	proti	k7c3
snaze	snaha	k1gFnSc3
Pruska	Prusko	k1gNnSc2
a	a	k8xC
Ruska	Rusko	k1gNnSc2
zcela	zcela	k6eAd1
rozložit	rozložit	k5eAaPmF
a	a	k8xC
následně	následně	k6eAd1
anektovat	anektovat	k5eAaBmF
zbytek	zbytek	k1gInSc4
Republiky	republika	k1gFnSc2
obou	dva	k4xCgInPc2
národů	národ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpor	odpor	k1gInSc1
vedl	vést	k5eAaImAgMnS
generál	generál	k1gMnSc1
Tadeusz	Tadeusz	k1gMnSc1
Kościuszko	Kościuszka	k1gFnSc5
<g/>
,	,	kIx,
po	po	k7c6
němž	jenž	k3xRgInSc6
je	být	k5eAaImIp3nS
povstání	povstání	k1gNnSc1
pojmenováno	pojmenovat	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
němu	on	k3xPp3gNnSc3
v	v	k7c6
roce	rok	k1gInSc6
1794	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
i	i	k9
když	když	k8xS
zaznamenalo	zaznamenat	k5eAaPmAgNnS
zpočátku	zpočátku	k6eAd1
jisté	jistý	k2eAgInPc4d1
úspěchy	úspěch	k1gInPc4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
následně	následně	k6eAd1
tvrdě	tvrdě	k6eAd1
potlačeno	potlačit	k5eAaPmNgNnS
ruskými	ruský	k2eAgInPc7d1
a	a	k8xC
pruskými	pruský	k2eAgNnPc7d1
vojsky	vojsko	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
bylo	být	k5eAaImAgNnS
třetí	třetí	k4xOgNnSc1
a	a	k8xC
definitivní	definitivní	k2eAgNnSc4d1
rozdělení	rozdělení	k1gNnSc4
Polska	Polsko	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
na	na	k7c4
123	#num#	k4
let	léto	k1gNnPc2
ukončilo	ukončit	k5eAaPmAgNnS
samostatnou	samostatný	k2eAgFnSc4d1
existenci	existence	k1gFnSc4
tohoto	tento	k3xDgInSc2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Příčiny	příčina	k1gFnPc4
</s>
<s>
Úpadek	úpadek	k1gInSc1
Republiky	republika	k1gFnSc2
obou	dva	k4xCgInPc2
národů	národ	k1gInPc2
</s>
<s>
Polští	polský	k2eAgMnPc1d1
magnáti	magnát	k1gMnPc1
na	na	k7c6
kresbě	kresba	k1gFnSc6
Jana	Jan	k1gMnSc2
Matejky	Matejka	k1gFnSc2
</s>
<s>
Dlouhodobým	dlouhodobý	k2eAgInSc7d1
problémem	problém	k1gInSc7
Republiky	republika	k1gFnSc2
obou	dva	k4xCgInPc2
národů	národ	k1gInPc2
byla	být	k5eAaImAgFnS
rostoucí	rostoucí	k2eAgFnSc1d1
moc	moc	k1gFnSc1
šlechty	šlechta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhruba	zhruba	k6eAd1
od	od	k7c2
začátku	začátek	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
kontrolovali	kontrolovat	k5eAaImAgMnP
šlechtičtí	šlechtický	k2eAgMnPc1d1
magnáti	magnát	k1gMnPc1
politický	politický	k2eAgMnSc1d1
i	i	k8xC
ekonomický	ekonomický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zneužívali	zneužívat	k5eAaImAgMnP
liberum	liberum	k1gNnSc4
veto	veto	k1gNnSc1
<g/>
,	,	kIx,
paralyzovali	paralyzovat	k5eAaBmAgMnP
sejm	sejm	k1gInSc4
a	a	k8xC
bránili	bránit	k5eAaImAgMnP
jakýmkoli	jakýkoli	k3yIgFnPc3
reformám	reforma	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
by	by	kYmCp3nP
tyto	tento	k3xDgFnPc4
jejich	jejich	k3xOp3gFnSc2
"	"	kIx"
<g/>
zlaté	zlatý	k2eAgFnSc2d1
svobody	svoboda	k1gFnSc2
<g/>
"	"	kIx"
omezily	omezit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravomoci	pravomoc	k1gFnSc3
panovníka	panovník	k1gMnSc2
byly	být	k5eAaImAgFnP
silně	silně	k6eAd1
zredukované	zredukovaný	k2eAgFnPc1d1
a	a	k8xC
pokud	pokud	k8xS
i	i	k9
sám	sám	k3xTgMnSc1
panovník	panovník	k1gMnSc1
byl	být	k5eAaImAgMnS
slabou	slabý	k2eAgFnSc7d1
osobností	osobnost	k1gFnSc7
<g/>
,	,	kIx,
šlechtě	šlechta	k1gFnSc3
to	ten	k3xDgNnSc1
vyhovovalo	vyhovovat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
nahrávalo	nahrávat	k5eAaImAgNnS
sousedním	sousední	k2eAgFnPc3d1
mocnostem	mocnost	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prakticky	prakticky	k6eAd1
od	od	k7c2
doby	doba	k1gFnSc2
severní	severní	k2eAgFnSc2d1
války	válka	k1gFnSc2
byl	být	k5eAaImAgInS
vliv	vliv	k1gInSc1
Pruska	Prusko	k1gNnSc2
<g/>
,	,	kIx,
Ruska	Rusko	k1gNnSc2
a	a	k8xC
do	do	k7c2
jisté	jistý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
i	i	k8xC
Rakouska	Rakousko	k1gNnSc2
zásadním	zásadní	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
polské	polský	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Především	především	k6eAd1
Rusko	Rusko	k1gNnSc1
hledělo	hledět	k5eAaImAgNnS
na	na	k7c4
úpadek	úpadek	k1gInSc4
Rzeczpospolite	Rzeczpospolit	k1gInSc5
se	se	k3xPyFc4
zalíbením	zalíbení	k1gNnSc7
a	a	k8xC
podporovalo	podporovat	k5eAaImAgNnS
místní	místní	k2eAgNnSc1d1
šlechtické	šlechtický	k2eAgInPc1d1
rody	rod	k1gInPc1
v	v	k7c6
jejich	jejich	k3xOp3gInSc6
boji	boj	k1gInSc6
za	za	k7c4
udržení	udržení	k1gNnSc4
svých	svůj	k3xOyFgInPc2
"	"	kIx"
<g/>
práv	právo	k1gNnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polsko-litevská	polsko-litevský	k2eAgFnSc1d1
unijní	unijní	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
byla	být	k5eAaImAgFnS
velmi	velmi	k6eAd1
slabá	slabý	k2eAgFnSc1d1
(	(	kIx(
<g/>
okolo	okolo	k7c2
20	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
nemohla	moct	k5eNaImAgFnS
se	se	k3xPyFc4
postavit	postavit	k5eAaPmF
na	na	k7c4
odpor	odpor	k1gInSc4
vojskům	vojsko	k1gNnPc3
Ruska	Rusko	k1gNnSc2
(	(	kIx(
<g/>
okolo	okolo	k7c2
300	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pruska	Prusko	k1gNnSc2
a	a	k8xC
Rakouska	Rakousko	k1gNnSc2
(	(	kIx(
<g/>
každá	každý	k3xTgFnSc1
okolo	okolo	k7c2
200	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
jakémkoliv	jakýkoliv	k3yIgInSc6
větším	veliký	k2eAgInSc6d2
konfliktu	konflikt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Pokusy	pokus	k1gInPc1
o	o	k7c4
reformu	reforma	k1gFnSc4
tohoto	tento	k3xDgInSc2
zhoubného	zhoubný	k2eAgInSc2d1
systému	systém	k1gInSc2
se	se	k3xPyFc4
sice	sice	k8xC
objevovaly	objevovat	k5eAaImAgFnP
<g/>
,	,	kIx,
ale	ale	k8xC
narážely	narážet	k5eAaImAgFnP,k5eAaPmAgFnP
na	na	k7c4
odpor	odpor	k1gInSc4
jak	jak	k6eAd1
uvnitř	uvnitř	k7c2
unie	unie	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
u	u	k7c2
sousedních	sousední	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1768	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
tzv.	tzv.	kA
Barská	Barská	k1gFnSc1
konfederace	konfederace	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
hlavním	hlavní	k2eAgInSc6d1
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
vymanit	vymanit	k5eAaPmF
stát	stát	k1gInSc4
z	z	k7c2
vlivu	vliv	k1gInSc2
Ruska	Rusko	k1gNnSc2
a	a	k8xC
posílit	posílit	k5eAaPmF
pozici	pozice	k1gFnSc4
katolictví	katolictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konfederace	konfederace	k1gFnSc1
však	však	k9
měla	mít	k5eAaImAgFnS
spoustu	spousta	k1gFnSc4
odpůrců	odpůrce	k1gMnPc2
jak	jak	k8xS,k8xC
doma	doma	k6eAd1
tak	tak	k9
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
bylo	být	k5eAaImAgNnS
vypuknutí	vypuknutí	k1gNnSc1
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
do	do	k7c2
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
na	na	k7c6
straně	strana	k1gFnSc6
protivníků	protivník	k1gMnPc2
Barské	Barský	k2eAgFnSc2d1
konfederace	konfederace	k1gFnSc2
postavilo	postavit	k5eAaPmAgNnS
i	i	k9
Rusko	Rusko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
se	se	k3xPyFc4
reformisté	reformista	k1gMnPc1
poměrně	poměrně	k6eAd1
dlouho	dlouho	k6eAd1
drželi	držet	k5eAaImAgMnP
<g/>
,	,	kIx,
snažili	snažit	k5eAaImAgMnP
se	se	k3xPyFc4
horlivou	horlivý	k2eAgFnSc7d1
diplomacií	diplomacie	k1gFnSc7
získat	získat	k5eAaPmF
spojence	spojenec	k1gMnPc4
v	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
Sasku	Sasko	k1gNnSc6
a	a	k8xC
Rakousku	Rakousko	k1gNnSc6
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
podlehli	podlehnout	k5eAaPmAgMnP
převaze	převaha	k1gFnSc3
ruských	ruský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
vedených	vedený	k2eAgInPc2d1
generálem	generál	k1gMnSc7
Suvorovem	Suvorov	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyústěním	vyústění	k1gNnSc7
tohoto	tento	k3xDgInSc2
vývoje	vývoj	k1gInSc2
bylo	být	k5eAaImAgNnS
první	první	k4xOgNnSc1
dělení	dělení	k1gNnSc1
Polska	Polsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1772	#num#	k4
si	se	k3xPyFc3
Prusko	Prusko	k1gNnSc4
<g/>
,	,	kIx,
Rakousko	Rakousko	k1gNnSc4
a	a	k8xC
Rusko	Rusko	k1gNnSc4
přivlastnily	přivlastnit	k5eAaPmAgFnP
rozsáhlé	rozsáhlý	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
polsko-litevského	polsko-litevský	k2eAgNnSc2d1
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Argumentem	argument	k1gInSc7
k	k	k7c3
tomuto	tento	k3xDgInSc3
kroku	krok	k1gInSc3
byla	být	k5eAaImAgFnS
údajná	údajný	k2eAgFnSc1d1
snaha	snaha	k1gFnSc1
zabránit	zabránit	k5eAaPmF
totálnímu	totální	k2eAgInSc3d1
rozkladu	rozklad	k1gInSc3
státu	stát	k1gInSc2
<g/>
,	,	kIx,
vnitřní	vnitřní	k2eAgFnSc3d1
a	a	k8xC
vnější	vnější	k2eAgFnSc3d1
nestabilitě	nestabilita	k1gFnSc3
<g/>
,	,	kIx,
útlaku	útlak	k1gInSc6
nekatolíků	nekatolík	k1gMnPc2
a	a	k8xC
kvazihistorické	kvazihistorický	k2eAgInPc4d1
nároky	nárok	k1gInPc4
na	na	k7c4
zabraná	zabraný	k2eAgNnPc4d1
území	území	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Snahy	snaha	k1gFnPc1
o	o	k7c4
reformu	reforma	k1gFnSc4
</s>
<s>
„	„	k?
<g/>
Velký	velký	k2eAgInSc1d1
sejm	sejm	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
či	či	k8xC
Čtyřletý	čtyřletý	k2eAgInSc1d1
sejm	sejm	k1gInSc1
<g/>
)	)	kIx)
přijímá	přijímat	k5eAaImIp3nS
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1791	#num#	k4
ústavu	ústav	k1gInSc2
</s>
<s>
Velká	velký	k2eAgFnSc1d1
příležitost	příležitost	k1gFnSc1
reformovat	reformovat	k5eAaBmF
stát	stát	k5eAaImF,k5eAaPmF
přišla	přijít	k5eAaPmAgFnS
během	během	k7c2
tzv.	tzv.	kA
Velkého	velký	k2eAgNnSc2d1
(	(	kIx(
<g/>
neboli	neboli	k8xC
Čtyřletého	čtyřletý	k2eAgInSc2d1
<g/>
)	)	kIx)
sejmu	sejm	k1gInSc2
v	v	k7c6
letech	léto	k1gNnPc6
1788	#num#	k4
<g/>
-	-	kIx~
<g/>
1792	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sousedé	soused	k1gMnPc1
Polska	Polsko	k1gNnSc2
vedli	vést	k5eAaImAgMnP
různé	různý	k2eAgInPc4d1
války	válek	k1gInPc4
(	(	kIx(
<g/>
Rusko-turecká	rusko-turecký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
1787	#num#	k4
<g/>
-	-	kIx~
<g/>
1792	#num#	k4
<g/>
,	,	kIx,
Rakousko-turecká	rakousko-turecký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
1787	#num#	k4
<g/>
-	-	kIx~
<g/>
1791	#num#	k4
<g/>
,	,	kIx,
Rusko-švédská	rusko-švédský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
1788	#num#	k4
<g/>
-	-	kIx~
<g/>
1790	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
byli	být	k5eAaImAgMnP
tudíž	tudíž	k8xC
zaneprázdněni	zaneprázdněn	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unie	unie	k1gFnSc1
uzavřela	uzavřít	k5eAaPmAgFnS
roku	rok	k1gInSc2
1790	#num#	k4
alianci	aliance	k1gFnSc4
s	s	k7c7
Pruskem	Prusko	k1gNnSc7
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
vypadala	vypadat	k5eAaPmAgNnP,k5eAaImAgNnP
<g/>
,	,	kIx,
že	že	k8xS
zabezpečí	zabezpečit	k5eAaPmIp3nS
Polsko	Polsko	k1gNnSc1
před	před	k7c7
dalším	další	k2eAgNnSc7d1
ruským	ruský	k2eAgNnSc7d1
vměšováním	vměšování	k1gNnSc7
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1791	#num#	k4
vydal	vydat	k5eAaPmAgInS
sejm	sejm	k1gInSc1
ústavu	ústav	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vzbuzovala	vzbuzovat	k5eAaImAgFnS
velké	velký	k2eAgFnPc4d1
naděje	naděje	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
omezila	omezit	k5eAaPmAgFnS
moc	moc	k6eAd1
magnátů	magnát	k1gMnPc2
-	-	kIx~
mj.	mj.	kA
rušila	rušit	k5eAaImAgFnS
liberum	liberum	k1gInSc4
veto	veto	k1gNnSc4
<g/>
,	,	kIx,
garantovala	garantovat	k5eAaBmAgNnP
určitá	určitý	k2eAgNnPc1d1
politická	politický	k2eAgNnPc1d1
práva	právo	k1gNnPc1
měšťanstvu	měšťanstvo	k1gNnSc6
a	a	k8xC
mírnila	mírnit	k5eAaImAgFnS
nevolnictví	nevolnictví	k1gNnSc4
(	(	kIx(
<g/>
poskytla	poskytnout	k5eAaPmAgFnS
rolníkům	rolník	k1gMnPc3
ochranu	ochrana	k1gFnSc4
státu	stát	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Když	když	k8xS
se	se	k3xPyFc4
ruské	ruský	k2eAgInPc1d1
vládnoucí	vládnoucí	k2eAgInPc1d1
kruhy	kruh	k1gInPc1
dozvěděly	dozvědět	k5eAaPmAgInP
o	o	k7c6
nové	nový	k2eAgFnSc6d1
ústavě	ústava	k1gFnSc6
<g/>
,	,	kIx,
rozzuřily	rozzuřit	k5eAaPmAgFnP
se	se	k3xPyFc4
<g/>
.	.	kIx.
„	„	k?
<g/>
Z	z	k7c2
Varšavy	Varšava	k1gFnSc2
přišla	přijít	k5eAaPmAgFnS
nejhorší	zlý	k2eAgFnSc1d3
možná	možný	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
:	:	kIx,
polský	polský	k2eAgMnSc1d1
král	král	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
téměř	téměř	k6eAd1
nezávislým	závislý	k2eNgInSc7d1
<g/>
.	.	kIx.
<g/>
“	“	k?
reagoval	reagovat	k5eAaBmAgMnS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
předních	přední	k2eAgMnPc2d1
úředníků	úředník	k1gMnPc2
ministerstva	ministerstvo	k1gNnSc2
zahraničí	zahraničí	k1gNnSc2
Alexandr	Alexandr	k1gMnSc1
Bezborodko	Bezborodka	k1gFnSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Prusko	Prusko	k1gNnSc1
ústava	ústava	k1gFnSc1
pobouřila	pobouřit	k5eAaPmAgFnS
natolik	natolik	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
sdělilo	sdělit	k5eAaPmAgNnS
polským	polský	k2eAgInPc3d1
diplomatům	diplomat	k1gInPc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
již	již	k6eAd1
necítí	cítit	k5eNaImIp3nS
vázáno	vázat	k5eAaImNgNnS
dohodou	dohoda	k1gFnSc7
z	z	k7c2
roku	rok	k1gInSc2
1790	#num#	k4
<g/>
,	,	kIx,
neboť	neboť	k8xC
polský	polský	k2eAgInSc1d1
stát	stát	k1gInSc1
se	se	k3xPyFc4
údajně	údajně	k6eAd1
tímto	tento	k3xDgNnSc7
zcela	zcela	k6eAd1
změnil	změnit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
sousedů	soused	k1gMnPc2
si	se	k3xPyFc3
nepřál	přát	k5eNaImAgInS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ústava	ústava	k1gFnSc1
nastartovala	nastartovat	k5eAaPmAgFnS
Polsko	Polsko	k1gNnSc4
k	k	k7c3
obrodě	obroda	k1gFnSc3
a	a	k8xC
posílení	posílení	k1gNnSc3
<g/>
,	,	kIx,
obávali	obávat	k5eAaImAgMnP
se	s	k7c7
mj.	mj.	kA
i	i	k8xC
případných	případný	k2eAgInPc2d1
budoucích	budoucí	k2eAgInPc2d1
nároků	nárok	k1gInPc2
Polska	Polsko	k1gNnSc2
na	na	k7c6
vrácení	vrácení	k1gNnSc6
jimi	on	k3xPp3gMnPc7
uchvácených	uchvácený	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Druhé	druhý	k4xOgNnSc4
dělení	dělení	k1gNnSc4
Polska	Polsko	k1gNnSc2
</s>
<s>
Rusko-polská	rusko-polský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
1792	#num#	k4
</s>
<s>
Kníže	kníže	k1gMnSc1
Józef	Józef	k1gMnSc1
Poniatowski	Poniatowsk	k1gMnPc1
<g/>
,	,	kIx,
polský	polský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
a	a	k8xC
synovec	synovec	k1gMnSc1
krále	král	k1gMnSc2
Stanislawa	Stanislawum	k1gNnSc2
Augusta	August	k1gMnSc2
Poniatowskeho	Poniatowske	k1gMnSc2
</s>
<s>
Ústava	ústava	k1gFnSc1
vzbudila	vzbudit	k5eAaPmAgFnS
logicky	logicky	k6eAd1
nevoli	nevole	k1gFnSc4
i	i	k9
u	u	k7c2
části	část	k1gFnSc2
Poláků	Polák	k1gMnPc2
-	-	kIx~
zvláště	zvláště	k6eAd1
u	u	k7c2
vysoké	vysoký	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnozí	mnohý	k2eAgMnPc1d1
magnáti	magnát	k1gMnPc1
<g/>
,	,	kIx,
především	především	k9
ti	ten	k3xDgMnPc1
napojení	napojený	k2eAgMnPc1d1
na	na	k7c4
Rusko	Rusko	k1gNnSc4
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
proti	proti	k7c3
od	od	k7c2
začátku	začátek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
jejich	jejich	k3xOp3gInSc2
odporu	odpor	k1gInSc2
bylo	být	k5eAaImAgNnS
zformování	zformování	k1gNnSc1
tzv.	tzv.	kA
Targovické	Targovický	k2eAgFnSc2d1
konfederace	konfederace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
prohlášení	prohlášení	k1gNnSc6
připraveném	připravený	k2eAgInSc6d1
pod	pod	k7c7
pečlivým	pečlivý	k2eAgInSc7d1
ruským	ruský	k2eAgInSc7d1
dohledem	dohled	k1gInSc7
v	v	k7c6
Petrohradu	Petrohrad	k1gInSc3
v	v	k7c6
lednu	leden	k1gInSc6
1792	#num#	k4
kritizovala	kritizovat	k5eAaImAgFnS
ústavu	ústav	k1gInSc2
jako	jako	k8xS,k8xC
nakaženou	nakažený	k2eAgFnSc4d1
„	„	k?
<g/>
demokracií	demokracie	k1gFnPc2
<g/>
,	,	kIx,
následující	následující	k2eAgInSc1d1
fatální	fatální	k2eAgInSc4d1
příklad	příklad	k1gInSc4
z	z	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
,	,	kIx,
omezující	omezující	k2eAgNnPc4d1
základní	základní	k2eAgNnPc4d1
práva	právo	k1gNnPc4
a	a	k8xC
svobody	svoboda	k1gFnPc4
šlechty	šlechta	k1gFnSc2
a	a	k8xC
zavádějící	zavádějící	k2eAgMnSc1d1
do	do	k7c2
revoluce	revoluce	k1gFnSc2
a	a	k8xC
převratu	převrat	k1gInSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
Členové	člen	k1gMnPc1
konfederace	konfederace	k1gFnSc2
se	se	k3xPyFc4
ve	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
svém	svůj	k3xOyFgNnSc6
prohlášení	prohlášení	k1gNnSc6
obrátili	obrátit	k5eAaPmAgMnP
na	na	k7c4
ruskou	ruský	k2eAgFnSc4d1
carevnu	carevna	k1gFnSc4
Kateřinu	Kateřina	k1gFnSc4
II	II	kA
<g/>
.	.	kIx.
jako	jako	k8xS,k8xC
na	na	k7c4
„	„	k?
<g/>
spravedlivou	spravedlivý	k2eAgFnSc4d1
panovnici	panovnice	k1gFnSc4
<g/>
,	,	kIx,
našeho	náš	k3xOp1gMnSc4
spojence	spojenec	k1gMnSc4
a	a	k8xC
přítele	přítel	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
respektuje	respektovat	k5eAaImIp3nS
potřeby	potřeba	k1gFnPc4
našeho	náš	k3xOp1gInSc2
národa	národ	k1gInSc2
na	na	k7c4
blahobyt	blahobyt	k1gInSc4
a	a	k8xC
vždy	vždy	k6eAd1
nabízí	nabízet	k5eAaImIp3nS
pomocnou	pomocný	k2eAgFnSc4d1
ruku	ruka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Tou	ten	k3xDgFnSc7
pomocnou	pomocný	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
byla	být	k5eAaImAgFnS
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
vojenská	vojenský	k2eAgFnSc1d1
intervence	intervence	k1gFnSc1
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1792	#num#	k4
vyhlásilo	vyhlásit	k5eAaPmAgNnS
Rusko	Rusko	k1gNnSc1
Polsku	Polsko	k1gNnSc6
válku	válek	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
ten	ten	k3xDgInSc4
stejný	stejný	k2eAgInSc4d1
den	den	k1gInSc4
vstoupily	vstoupit	k5eAaPmAgFnP
ruské	ruský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
do	do	k7c2
Rzecypospolite	Rzecypospolit	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polské	polský	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
vedli	vést	k5eAaImAgMnP
generálové	generál	k1gMnPc1
Józef	Józef	k1gMnSc1
Antoni	Anton	k1gMnPc1
Poniatowski	Poniatowski	k1gNnPc2
a	a	k8xC
Tadeusz	Tadeusza	k1gFnPc2
Kościuszko	Kościuszka	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proběhlo	proběhnout	k5eAaPmAgNnS
sice	sice	k8xC
několik	několik	k4yIc1
menších	malý	k2eAgInPc2d2
střetů	střet	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
k	k	k7c3
žádné	žádný	k3yNgFnSc3
větší	veliký	k2eAgFnSc3d2
bitvě	bitva	k1gFnSc3
nedošlo	dojít	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
v	v	k7c6
červenci	červenec	k1gInSc6
1792	#num#	k4
podpisem	podpis	k1gInSc7
kapitulace	kapitulace	k1gFnSc2
polským	polský	k2eAgMnSc7d1
králem	král	k1gMnSc7
Stanisławem	Stanisław	k1gMnSc7
Augustem	August	k1gMnSc7
Poniatowskim	Poniatowskim	k1gInSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
doufal	doufat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podaří	podařit	k5eAaPmIp3nS
vyjednat	vyjednat	k5eAaPmF
kompromis	kompromis	k1gInSc4
diplomatickou	diplomatický	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Královy	Králův	k2eAgFnPc1d1
naděje	naděje	k1gFnPc1
však	však	k9
byly	být	k5eAaImAgFnP
brzy	brzy	k6eAd1
zmařeny	zmařen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
podzim	podzim	k1gInSc4
1793	#num#	k4
zasedl	zasednout	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
mimořádný	mimořádný	k2eAgInSc1d1
parlament	parlament	k1gInSc1
zvaný	zvaný	k2eAgInSc1d1
Sejm	Sejm	k1gInSc1
z	z	k7c2
Grodna	Grodno	k1gNnSc2
(	(	kIx(
<g/>
Sejm	Sejm	k1gInSc1
grodzieński	grodzieńsk	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
probíhal	probíhat	k5eAaImAgInS
plně	plně	k6eAd1
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
ruských	ruský	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
anuloval	anulovat	k5eAaBmAgMnS
ústavu	ústava	k1gFnSc4
z	z	k7c2
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1791	#num#	k4
a	a	k8xC
odsouhlasil	odsouhlasit	k5eAaPmAgInS
další	další	k2eAgNnSc4d1
dělení	dělení	k1gNnSc4
polského	polský	k2eAgNnSc2d1
území	území	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
již	již	k6eAd1
mezi	mezi	k7c7
sebou	se	k3xPyFc7
dojednali	dojednat	k5eAaPmAgMnP
zástupci	zástupce	k1gMnPc1
Ruska	Rusko	k1gNnSc2
a	a	k8xC
Pruska	Prusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusku	Rusko	k1gNnSc6
připadlo	připadnout	k5eAaPmAgNnS
250	#num#	k4
000	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
Prusku	Prusko	k1gNnSc6
58	#num#	k4
000	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
to	ten	k3xDgNnSc1
vítězným	vítězný	k2eAgFnPc3d1
mocnostem	mocnost	k1gFnPc3
nestačilo	stačit	k5eNaBmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezávislost	nezávislost	k1gFnSc1
zbylého	zbylý	k2eAgNnSc2d1
území	území	k1gNnSc2
byla	být	k5eAaImAgFnS
jen	jen	k9
iluzorní	iluzorní	k2eAgFnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
na	na	k7c6
něm	on	k3xPp3gInSc6
usídlily	usídlit	k5eAaPmAgFnP
jednotky	jednotka	k1gFnPc1
ruské	ruský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
Targovické	Targovický	k2eAgFnSc2d1
konfederace	konfederace	k1gFnSc2
byli	být	k5eAaImAgMnP
zcela	zcela	k6eAd1
oprávněně	oprávněně	k6eAd1
považováni	považován	k2eAgMnPc1d1
většinou	většinou	k6eAd1
Poláků	polák	k1gInPc2
za	za	k7c4
zrádce	zrádce	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Příprava	příprava	k1gFnSc1
povstání	povstání	k1gNnSc2
</s>
<s>
Polský	polský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
a	a	k8xC
vlastenec	vlastenec	k1gMnSc1
Tadeusz	Tadeusz	k1gMnSc1
Kościuszko	Kościuszka	k1gFnSc5
</s>
<s>
Hugo	Hugo	k1gMnSc1
Kołłątaj	Kołłątaj	k1gMnSc1
–	–	k?
významný	významný	k2eAgMnSc1d1
polský	polský	k2eAgMnSc1d1
myslitel	myslitel	k1gMnSc1
a	a	k8xC
osvícenec	osvícenec	k1gMnSc1
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
architektů	architekt	k1gMnPc2
povstání	povstání	k1gNnSc2
</s>
<s>
Polští	polský	k2eAgMnPc1d1
armádní	armádní	k2eAgMnPc1d1
důstojníci	důstojník	k1gMnPc1
považovali	považovat	k5eAaImAgMnP
kapitulaci	kapitulace	k1gFnSc4
Polska	Polsko	k1gNnSc2
ve	v	k7c6
válce	válka	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1792	#num#	k4
jako	jako	k8xC,k8xS
předčasnou	předčasný	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tadeusz	Tadeusz	k1gInSc1
Kościuszko	Kościuszka	k1gFnSc5
<g/>
,	,	kIx,
kníže	kníže	k1gMnSc1
Józef	Józef	k1gMnSc1
Antoni	Anton	k1gMnPc1
Poniatowski	Poniatowski	k1gNnPc2
a	a	k8xC
mnozí	mnohý	k2eAgMnPc1d1
další	další	k2eAgMnPc1d1
kritizovali	kritizovat	k5eAaImAgMnP
královo	králův	k2eAgNnSc4d1
rozhodnutí	rozhodnutí	k1gNnSc4
a	a	k8xC
na	na	k7c4
protest	protest	k1gInSc4
rezignovali	rezignovat	k5eAaBmAgMnP
na	na	k7c4
své	svůj	k3xOyFgMnPc4
funkce	funkce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
vojáků	voják	k1gMnPc2
polské	polský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
byl	být	k5eAaImAgInS
nejprve	nejprve	k6eAd1
snížen	snížit	k5eAaPmNgInS
na	na	k7c4
36	#num#	k4
000	#num#	k4
a	a	k8xC
posléze	posléze	k6eAd1
bylo	být	k5eAaImAgNnS
Rusy	Rus	k1gMnPc4
povoleno	povolen	k2eAgNnSc1d1
jen	jen	k6eAd1
15	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vzbudilo	vzbudit	k5eAaPmAgNnS
odpor	odpor	k1gInSc4
uvnitř	uvnitř	k7c2
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nespokojenost	nespokojenost	k1gFnSc1
se	se	k3xPyFc4
šířila	šířit	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kościuszko	Kościuszka	k1gFnSc5
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
Polsku	Polsko	k1gNnSc6
nesmírně	smírně	k6eNd1
populární	populární	k2eAgMnSc1d1
<g/>
,	,	kIx,
neztratil	ztratit	k5eNaPmAgMnS
ve	v	k7c6
válce	válka	k1gFnSc6
ani	ani	k8xC
jednu	jeden	k4xCgFnSc4
bitvu	bitva	k1gFnSc4
<g/>
,	,	kIx,
tudíž	tudíž	k8xC
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
kapitulace	kapitulace	k1gFnSc1
znamenala	znamenat	k5eAaImAgFnS
velkou	velký	k2eAgFnSc4d1
ránu	rána	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
opustit	opustit	k5eAaPmF
Rzeczpospolitou	Rzeczpospolita	k1gFnSc7
a	a	k8xC
v	v	k7c6
prosinci	prosinec	k1gInSc6
1792	#num#	k4
se	se	k3xPyFc4
přesunul	přesunout	k5eAaPmAgMnS
do	do	k7c2
Lipska	Lipsko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
formovala	formovat	k5eAaImAgFnS
komunita	komunita	k1gFnSc1
složená	složený	k2eAgFnSc1d1
z	z	k7c2
polských	polský	k2eAgMnPc2d1
emigrantů	emigrant	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
byli	být	k5eAaImAgMnP
Ignacy	Ignac	k2eAgFnPc4d1
Potocki	Potock	k1gFnPc4
<g/>
,	,	kIx,
Hugo	Hugo	k1gMnSc1
Kołłątaj	Kołłątaj	k1gMnSc1
a	a	k8xC
Ignacy	Ignac	k2eAgFnPc4d1
Działyński	Działyńsk	k1gFnPc4
<g/>
,	,	kIx,
začali	začít	k5eAaPmAgMnP
plánovat	plánovat	k5eAaImF
povstání	povstání	k1gNnSc4
proti	proti	k7c3
ruské	ruský	k2eAgFnSc3d1
nadvládě	nadvláda	k1gFnSc3
nad	nad	k7c7
Polskem	Polsko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zimě	zima	k1gFnSc6
1793	#num#	k4
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgInS
do	do	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zkusil	zkusit	k5eAaPmAgMnS
zajistit	zajistit	k5eAaPmF
spojenectví	spojenectví	k1gNnSc4
<g/>
,	,	kIx,
či	či	k8xC
alespoň	alespoň	k9
podporu	podpora	k1gFnSc4
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představitelé	představitel	k1gMnPc1
revolucí	revoluce	k1gFnPc2
zmítané	zmítaný	k2eAgFnSc2d1
Francie	Francie	k1gFnSc2
sice	sice	k8xC
s	s	k7c7
Poláky	Polák	k1gMnPc7
sympatizovali	sympatizovat	k5eAaImAgMnP
<g/>
,	,	kIx,
ale	ale	k8xC
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
tak	tak	k9
vše	všechen	k3xTgNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
pomoc	pomoc	k1gFnSc4
čekat	čekat	k5eAaImF
nemohli	moct	k5eNaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
srpnu	srpen	k1gInSc6
1793	#num#	k4
se	se	k3xPyFc4
Kościuszko	Kościuszka	k1gFnSc5
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Lipska	Lipsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhé	druhý	k4xOgNnSc4
dělení	dělení	k1gNnSc4
Polska	Polsko	k1gNnSc2
právě	právě	k6eAd1
probíhalo	probíhat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kościuszko	Kościuszka	k1gFnSc5
byl	být	k5eAaImAgInS
realista	realista	k1gMnSc1
<g/>
,	,	kIx,
věděl	vědět	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
povstání	povstání	k1gNnSc1
proti	proti	k7c3
třem	tři	k4xCgMnPc3
mocným	mocný	k2eAgMnPc3d1
sousedům	soused	k1gMnPc3
má	mít	k5eAaImIp3nS
malou	malý	k2eAgFnSc4d1
šanci	šance	k1gFnSc4
na	na	k7c4
úspěch	úspěch	k1gInSc4
<g/>
,	,	kIx,
přesto	přesto	k8xC
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
ujmout	ujmout	k5eAaPmF
se	se	k3xPyFc4
tohoto	tento	k3xDgInSc2
nelehkého	lehký	k2eNgInSc2d1
úkolu	úkol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
1793	#num#	k4
překročil	překročit	k5eAaPmAgMnS
polské	polský	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
<g/>
,	,	kIx,
cestoval	cestovat	k5eAaImAgMnS
<g/>
,	,	kIx,
mapoval	mapovat	k5eAaImAgMnS
situaci	situace	k1gFnSc4
a	a	k8xC
kontaktoval	kontaktovat	k5eAaImAgMnS
nespokojené	spokojený	k2eNgMnPc4d1
důstojníky	důstojník	k1gMnPc4
a	a	k8xC
politiky	politik	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
v	v	k7c6
plánu	plán	k1gInSc6
začátek	začátek	k1gInSc1
povstání	povstání	k1gNnSc2
zatím	zatím	k6eAd1
oddálit	oddálit	k5eAaPmF
<g/>
,	,	kIx,
vydal	vydat	k5eAaPmAgMnS
instrukci	instrukce	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
kolovala	kolovat	k5eAaImAgFnS
po	po	k7c6
Polsku	Polsko	k1gNnSc6
a	a	k8xC
sám	sám	k3xTgMnSc1
zatím	zatím	k6eAd1
odjel	odjet	k5eAaPmAgMnS
do	do	k7c2
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
tajně	tajně	k6eAd1
vyjednával	vyjednávat	k5eAaImAgMnS
mj.	mj.	kA
i	i	k9
s	s	k7c7
představiteli	představitel	k1gMnPc7
Vatikánu	Vatikán	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Nicméně	nicméně	k8xC
situace	situace	k1gFnSc1
v	v	k7c6
Polsku	Polsko	k1gNnSc6
se	se	k3xPyFc4
rapidně	rapidně	k6eAd1
zhoršovala	zhoršovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rus	k1gMnPc1
nařídili	nařídit	k5eAaPmAgMnP
zredukovat	zredukovat	k5eAaPmF
polskou	polský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
a	a	k8xC
propuštěné	propuštěný	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
zařadit	zařadit	k5eAaPmF
do	do	k7c2
svých	svůj	k3xOyFgFnPc2
řad	řada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carští	carský	k2eAgMnPc1d1
agenti	agent	k1gMnPc1
objevili	objevit	k5eAaPmAgMnP
odbojové	odbojový	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
ve	v	k7c6
Varšavě	Varšava	k1gFnSc6
<g/>
,	,	kIx,
začalo	začít	k5eAaPmAgNnS
zatýkání	zatýkání	k1gNnSc1
politiků	politik	k1gMnPc2
a	a	k8xC
velitelů	velitel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
1794	#num#	k4
se	se	k3xPyFc4
Kościuszko	Kościuszka	k1gFnSc5
narychlo	narychlo	k6eAd1
vydal	vydat	k5eAaPmAgMnS
do	do	k7c2
Krakova	Krakov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
potřeba	potřeba	k6eAd1
začít	začít	k5eAaPmF
jednat	jednat	k5eAaImF
<g/>
,	,	kIx,
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
nedokončené	dokončený	k2eNgFnPc4d1
přípravy	příprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
</s>
<s>
Kosynierzy	Kosynierza	k1gFnPc1
před	před	k7c7
bitvou	bitva	k1gFnSc7
u	u	k7c2
Racławic	Racławice	k1gInPc2
</s>
<s>
Mobilizace	mobilizace	k1gFnSc1
polského	polský	k2eAgInSc2d1
národa	národ	k1gInSc2
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1794	#num#	k4
odmítl	odmítnout	k5eAaPmAgMnS
generál	generál	k1gMnSc1
Antoni	Antoň	k1gFnSc3
Madaliński	Madalińsk	k1gMnPc1
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
velkopolské	velkopolský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
jezdecké	jezdecký	k2eAgFnSc2d1
brigády	brigáda	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
500	#num#	k4
mužů	muž	k1gMnPc2
<g/>
)	)	kIx)
rozkaz	rozkaz	k1gInSc4
demobilizovat	demobilizovat	k5eAaBmF
a	a	k8xC
začal	začít	k5eAaPmAgMnS
přesouvat	přesouvat	k5eAaImF
své	svůj	k3xOyFgFnPc4
jednotky	jednotka	k1gFnPc4
z	z	k7c2
Ostrołęka	Ostrołęek	k1gInSc2
do	do	k7c2
Krakova	Krakov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
čin	čin	k1gInSc4
zažehl	zažehnout	k5eAaPmAgMnS
nepokoje	nepokoj	k1gInSc2
a	a	k8xC
vzpoury	vzpoura	k1gFnSc2
proti	proti	k7c3
ruským	ruský	k2eAgFnPc3d1
silám	síla	k1gFnPc3
po	po	k7c6
celé	celý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruské	ruský	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
umístěné	umístěný	k2eAgFnPc4d1
v	v	k7c6
Krakově	Krakov	k1gInSc6
byly	být	k5eAaImAgFnP
vyslány	vyslán	k2eAgFnPc1d1
čelit	čelit	k5eAaImF
Madalinskimu	Madalinskima	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
ponechalo	ponechat	k5eAaPmAgNnS
město	město	k1gNnSc1
bez	bez	k7c2
dozoru	dozor	k1gInSc2
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1794	#num#	k4
vyhlásil	vyhlásit	k5eAaPmAgInS
Kościuszko	Kościuszka	k1gFnSc5
na	na	k7c6
Hlavním	hlavní	k2eAgNnSc6d1
krakovském	krakovský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
všeobecné	všeobecný	k2eAgNnSc4d1
povstání	povstání	k1gNnSc4
<g/>
,	,	kIx,
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
staví	stavit	k5eAaImIp3nS,k5eAaBmIp3nS,k5eAaPmIp3nS
do	do	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
čela	čelo	k1gNnSc2
a	a	k8xC
složil	složit	k5eAaPmAgMnS
přísahu	přísaha	k1gFnSc4
<g/>
:	:	kIx,
„	„	k?
<g/>
Já	já	k3xPp1nSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Tadeusz	Tadeusz	k1gInSc1
Kościuszko	Kościuszka	k1gFnSc5
přísahám	přísahat	k5eAaImIp1nS
před	před	k7c7
Bohem	bůh	k1gMnSc7
celému	celý	k2eAgInSc3d1
polskému	polský	k2eAgInSc3d1
národu	národ	k1gInSc3
<g/>
...	...	k?
<g/>
,	,	kIx,
že	že	k8xS
nebudu	být	k5eNaImBp1nS
nikoho	nikdo	k3yNnSc2
utiskovat	utiskovat	k5eAaImF
<g/>
,	,	kIx,
budu	být	k5eAaImBp1nS
bránit	bránit	k5eAaImF
celistvost	celistvost	k1gFnSc4
polských	polský	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
<g/>
,	,	kIx,
získám	získat	k5eAaPmIp1nS
zpět	zpět	k6eAd1
národní	národní	k2eAgFnSc4d1
nezávislost	nezávislost	k1gFnSc4
a	a	k8xC
posílím	posílit	k5eAaPmIp1nS
všeobecné	všeobecný	k2eAgFnSc2d1
svobody	svoboda	k1gFnSc2
<g/>
...	...	k?
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgInS
vyhlášen	vyhlásit	k5eAaPmNgInS
Akt	akt	k1gInSc1
povstání	povstání	k1gNnSc2
občanů	občan	k1gMnPc2
a	a	k8xC
obyvatel	obyvatel	k1gMnPc2
krakovského	krakovský	k2eAgNnSc2d1
vojvodství	vojvodství	k1gNnSc2
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgInSc3
se	se	k3xPyFc4
začali	začít	k5eAaPmAgMnP
připojovat	připojovat	k5eAaImF
obyvatelé	obyvatel	k1gMnPc1
dalších	další	k2eAgNnPc2d1
polských	polský	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Text	text	k1gInSc1
byl	být	k5eAaImAgInS
pravděpodobně	pravděpodobně	k6eAd1
formulován	formulovat	k5eAaImNgInS
ještě	ještě	k9
v	v	k7c6
saské	saský	k2eAgFnSc6d1
emigraci	emigrace	k1gFnSc6
setrvávajícím	setrvávající	k2eAgInSc7d1
Hugo	Hugo	k1gMnSc1
Kołłatajem	Kołłataj	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
povstání	povstání	k1gNnSc2
se	se	k3xPyFc4
od	od	k7c2
května	květen	k1gInSc2
stala	stát	k5eAaPmAgFnS
Nejvyšší	vysoký	k2eAgFnSc1d3
národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
sjednocovala	sjednocovat	k5eAaImAgNnP
původní	původní	k2eAgNnPc1d1
místní	místní	k2eAgNnPc1d1
odbojová	odbojový	k2eAgNnPc1d1
centra	centrum	k1gNnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Aby	aby	kYmCp3nP
maximálně	maximálně	k6eAd1
posílili	posílit	k5eAaPmAgMnP
slabou	slabý	k2eAgFnSc4d1
polskou	polský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
,	,	kIx,
vydali	vydat	k5eAaPmAgMnP
povstalci	povstalec	k1gMnPc1
zákon	zákon	k1gInSc4
o	o	k7c4
mobilizaci	mobilizace	k1gFnSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
požadoval	požadovat	k5eAaImAgInS
<g/>
,	,	kIx,
aby	aby	k9
každých	každý	k3xTgInPc2
5	#num#	k4
domů	dům	k1gInPc2
v	v	k7c6
Malopolsku	Malopolsko	k1gNnSc6
poskytlo	poskytnout	k5eAaPmAgNnS
nejméně	málo	k6eAd3
jednoho	jeden	k4xCgMnSc4
muže	muž	k1gMnSc4
<g/>
/	/	kIx~
<g/>
vojáka	voják	k1gMnSc4
vyzbrojeného	vyzbrojený	k2eAgMnSc4d1
palnou	palný	k2eAgFnSc7d1
zbraní	zbraň	k1gFnPc2
<g/>
,	,	kIx,
píkou	píka	k1gFnSc7
nebo	nebo	k8xC
sekerou	sekera	k1gFnSc7
a	a	k8xC
každých	každý	k3xTgInPc2
50	#num#	k4
domů	dům	k1gInPc2
jednoho	jeden	k4xCgMnSc2
jezdce	jezdec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teoreticky	teoreticky	k6eAd1
tak	tak	k6eAd1
mohli	moct	k5eAaImAgMnP
shromáždit	shromáždit	k5eAaPmF
asi	asi	k9
100	#num#	k4
000	#num#	k4
pěšáků	pěšák	k1gMnPc2
a	a	k8xC
10	#num#	k4
000	#num#	k4
jezdců	jezdec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
této	tento	k3xDgFnSc2
pravidelné	pravidelný	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
bylo	být	k5eAaImAgNnS
vyhlášeno	vyhlásit	k5eAaPmNgNnS
zřízení	zřízení	k1gNnSc1
milice	milice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
ní	on	k3xPp3gFnSc6
měli	mít	k5eAaImAgMnP
jít	jít	k5eAaImF
muži	muž	k1gMnPc1
ve	v	k7c6
věku	věk	k1gInSc6
18-28	18-28	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
nedostali	dostat	k5eNaPmAgMnP
k	k	k7c3
pravidelné	pravidelný	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
důvodu	důvod	k1gInSc2
nedostatku	nedostatek	k1gInSc2
kvalitní	kvalitní	k2eAgFnSc2d1
moderní	moderní	k2eAgFnSc2d1
výzbroje	výzbroj	k1gFnSc2
vznikly	vzniknout	k5eAaPmAgInP
oddíly	oddíl	k1gInPc1
vyzbrojené	vyzbrojený	k2eAgInPc1d1
kosami	kosa	k1gFnPc7
nazývané	nazývaný	k2eAgFnSc2d1
kosynierzy	kosynierza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
byla	být	k5eAaImAgFnS
zavedena	zaveden	k2eAgFnSc1d1
daň	daň	k1gFnSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
si	se	k3xPyFc3
velení	velení	k1gNnSc4
povstání	povstání	k1gNnSc2
zajistilo	zajistit	k5eAaPmAgNnS
alespoň	alespoň	k9
nějaký	nějaký	k3yIgInSc4
počáteční	počáteční	k2eAgInSc4d1
pravidelný	pravidelný	k2eAgInSc4d1
příjem	příjem	k1gInSc4
<g/>
,	,	kIx,
tiskly	tisknout	k5eAaImAgFnP
se	se	k3xPyFc4
papírové	papírový	k2eAgFnPc1d1
bankovky	bankovka	k1gFnPc1
a	a	k8xC
mnozí	mnohý	k2eAgMnPc1d1
bohatší	bohatý	k2eAgMnPc1d2
lidé	člověk	k1gMnPc1
dávali	dávat	k5eAaImAgMnP
do	do	k7c2
povstalecké	povstalecký	k2eAgFnSc2d1
pokladny	pokladna	k1gFnSc2
peníze	peníz	k1gInPc1
a	a	k8xC
šperky	šperk	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Povstalcům	povstalec	k1gMnPc3
bylo	být	k5eAaImAgNnS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
nemůžou	nemůžou	k?
uspět	uspět	k5eAaPmF
proti	proti	k7c3
třem	tři	k4xCgFnPc3
okolním	okolní	k2eAgFnPc3d1
mocnostem	mocnost	k1gFnPc3
naráz	naráz	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
již	již	k6eAd1
před	před	k7c7
začátkem	začátek	k1gInSc7
povstání	povstání	k1gNnSc2
snažili	snažit	k5eAaImAgMnP
veškerými	veškerý	k3xTgFnPc7
možnými	možný	k2eAgFnPc7d1
diplomatickými	diplomatický	k2eAgFnPc7d1
cestami	cesta	k1gFnPc7
zajistit	zajistit	k5eAaPmF
neutralitu	neutralita	k1gFnSc4
Rakouska	Rakousko	k1gNnSc2
i	i	k8xC
Pruska	Prusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Polské	polský	k2eAgInPc1d1
úspěchy	úspěch	k1gInPc1
</s>
<s>
Jakmile	jakmile	k8xS
se	se	k3xPyFc4
o	o	k7c6
nepokojích	nepokoj	k1gInPc6
dozvěděla	dozvědět	k5eAaPmAgFnS
carevna	carevna	k1gFnSc1
Kateřina	Kateřina	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
nařídila	nařídit	k5eAaPmAgFnS
sboru	sbor	k1gInSc3
generálmajora	generálmajor	k1gMnSc2
Fjodora	Fjodor	k1gMnSc2
Děnisova	Děnisův	k2eAgFnSc1d1
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
Krakov	Krakov	k1gInSc4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1794	#num#	k4
se	se	k3xPyFc4
polská	polský	k2eAgFnSc1d1
(	(	kIx(
<g/>
cca	cca	kA
5	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
ruská	ruský	k2eAgFnSc1d1
(	(	kIx(
<g/>
cca	cca	kA
3	#num#	k4
000-4	000-4	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
vojska	vojsko	k1gNnSc2
střetla	střetnout	k5eAaPmAgFnS
u	u	k7c2
vesnice	vesnice	k1gFnSc2
Racławice	Racławice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
prudkému	prudký	k2eAgInSc3d1
boji	boj	k1gInSc3
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
polští	polský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
<g/>
,	,	kIx,
ač	ač	k8xS
hůře	zle	k6eAd2
vyzbrojení	vyzbrojený	k2eAgMnPc1d1
<g/>
,	,	kIx,
zvítězili	zvítězit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rus	k1gMnPc1
ztratili	ztratit	k5eAaPmAgMnP
asi	asi	k9
800-1000	800-1000	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
Poláci	Polák	k1gMnPc1
100	#num#	k4
<g/>
-	-	kIx~
<g/>
250	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězové	vítěz	k1gMnPc1
ukořistili	ukořistit	k5eAaPmAgMnP
i	i	k9
několik	několik	k4yIc4
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
strategický	strategický	k2eAgInSc4d1
význam	význam	k1gInSc4
vítězství	vítězství	k1gNnSc2
nebyl	být	k5eNaImAgMnS
velký	velký	k2eAgMnSc1d1
<g/>
,	,	kIx,
poražené	poražený	k2eAgFnPc1d1
ruské	ruský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
byly	být	k5eAaImAgFnP
dále	daleko	k6eAd2
schopny	schopen	k2eAgFnPc1d1
operačního	operační	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
<g/>
,	,	kIx,
ohlas	ohlas	k1gInSc1
bitvy	bitva	k1gFnSc2
byl	být	k5eAaImAgInS
zásadní	zásadní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpráva	zpráva	k1gFnSc1
se	se	k3xPyFc4
rychle	rychle	k6eAd1
roznesla	roznést	k5eAaPmAgFnS
a	a	k8xC
mnoho	mnoho	k4c4
Poláků	Polák	k1gMnPc2
se	se	k3xPyFc4
následně	následně	k6eAd1
připojilo	připojit	k5eAaPmAgNnS
k	k	k7c3
řadám	řada	k1gFnPc3
povstalců	povstalec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polské	polský	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
shromážděné	shromážděný	k2eAgFnPc4d1
v	v	k7c6
oblasti	oblast	k1gFnSc6
Volyně	Volyně	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
měly	mít	k5eAaImAgInP
být	být	k5eAaImF
poslány	poslat	k5eAaPmNgInP
do	do	k7c2
Ruska	Rusko	k1gNnSc2
a	a	k8xC
začleněny	začlenit	k5eAaPmNgFnP
do	do	k7c2
tamní	tamní	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
připojily	připojit	k5eAaPmAgInP
ke	k	k7c3
Kościuszkovým	Kościuszkový	k2eAgFnPc3d1
silám	síla	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Povstání	povstání	k1gNnSc1
ve	v	k7c6
Varšavě	Varšava	k1gFnSc6
</s>
<s>
Zrádci	zrádce	k1gMnPc1
z	z	k7c2
Targovické	Targovický	k2eAgFnSc2d1
konfederace	konfederace	k1gFnSc2
byli	být	k5eAaImAgMnP
veřejně	veřejně	k6eAd1
věšeni	věšen	k2eAgMnPc1d1
nejen	nejen	k6eAd1
fyzicky	fyzicky	k6eAd1
ale	ale	k8xC
i	i	k9
symbolicky	symbolicky	k6eAd1
–	–	k?
když	když	k8xS
se	se	k3xPyFc4
je	on	k3xPp3gInPc4
nepodařilo	podařit	k5eNaPmAgNnS
dopadnout	dopadnout	k5eAaPmF
<g/>
,	,	kIx,
„	„	k?
<g/>
pověsili	pověsit	k5eAaPmAgMnP
<g/>
“	“	k?
jejich	jejich	k3xOp3gInPc4
portréty	portrét	k1gInPc4
</s>
<s>
Tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
se	se	k3xPyFc4
ruské	ruský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
umístěné	umístěný	k2eAgFnPc1d1
ve	v	k7c6
Varšavě	Varšava	k1gFnSc6
pokusily	pokusit	k5eAaPmAgFnP
odzbrojit	odzbrojit	k5eAaPmF
polskou	polský	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
posádku	posádka	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vyústilo	vyústit	k5eAaPmAgNnS
v	v	k7c6
povstání	povstání	k1gNnSc6
Poláků	polák	k1gInPc2
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Jana	Jan	k1gMnSc2
Kilińskiho	Kiliński	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povstalcům	povstalec	k1gMnPc3
nahrála	nahrát	k5eAaPmAgFnS,k5eAaBmAgFnS
neschopnost	neschopnost	k1gFnSc1
ruského	ruský	k2eAgMnSc2d1
velitele	velitel	k1gMnSc2
Josifa	Josif	k1gMnSc2
Igelströma	Igelström	k1gMnSc2
a	a	k8xC
fakt	fakt	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
mnoho	mnoho	k4c1
ruských	ruský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
šlo	jít	k5eAaImAgNnS
neozbrojeno	ozbrojit	k5eNaPmNgNnS
do	do	k7c2
kostela	kostel	k1gInSc2
na	na	k7c4
svaté	svatý	k2eAgNnSc4d1
přijímání	přijímání	k1gNnSc4
v	v	k7c6
rámci	rámec	k1gInSc6
oslav	oslava	k1gFnPc2
Svatého	svatý	k2eAgInSc2d1
týdne	týden	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akce	akce	k1gFnSc1
začala	začít	k5eAaPmAgFnS
ve	v	k7c4
čtvrtek	čtvrtek	k1gInSc4
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1794	#num#	k4
a	a	k8xC
kromě	kromě	k7c2
vojáků	voják	k1gMnPc2
se	se	k3xPyFc4
do	do	k7c2
povstání	povstání	k1gNnSc2
zapojilo	zapojit	k5eAaPmAgNnS
množství	množství	k1gNnSc1
civilních	civilní	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
prudkým	prudký	k2eAgInPc3d1
pouličním	pouliční	k2eAgInPc3d1
bojům	boj	k1gInPc3
<g/>
,	,	kIx,
při	při	k7c6
nichž	jenž	k3xRgInPc6
ruské	ruský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
ztratily	ztratit	k5eAaPmAgFnP
2	#num#	k4
000-4	000-4	k4
000	#num#	k4
mužů	muž	k1gMnPc2
a	a	k8xC
po	po	k7c6
dvou	dva	k4xCgInPc6
dnech	den	k1gInPc6
se	se	k3xPyFc4
zbytek	zbytek	k1gInSc1
z	z	k7c2
původně	původně	k6eAd1
osmitisícové	osmitisícový	k2eAgFnSc2d1
okupační	okupační	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
musel	muset	k5eAaImAgMnS
stáhnout	stáhnout	k5eAaPmF
z	z	k7c2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
podobnému	podobný	k2eAgNnSc3d1
povstání	povstání	k1gNnSc3
došlo	dojít	k5eAaPmAgNnS
i	i	k9
v	v	k7c6
dalších	další	k2eAgNnPc6d1
polských	polský	k2eAgNnPc6d1
městech	město	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masakr	masakr	k1gInSc1
neozbrojených	ozbrojený	k2eNgMnPc2d1
ruských	ruský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
jdoucích	jdoucí	k2eAgInPc2d1
slavit	slavit	k5eAaImF
velikonoční	velikonoční	k2eAgInSc4d1
svátek	svátek	k1gInSc4
považovali	považovat	k5eAaImAgMnP
Rusové	Rus	k1gMnPc1
za	za	k7c4
zločin	zločin	k1gInSc4
proti	proti	k7c3
lidskosti	lidskost	k1gFnSc3
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
krutě	krutě	k6eAd1
ztrestat	ztrestat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Povstání	povstání	k1gNnSc1
na	na	k7c6
Litvě	Litva	k1gFnSc6
</s>
<s>
Jeden	jeden	k4xCgMnSc1
z	z	k7c2
radikálních	radikální	k2eAgMnPc2d1
vůdců	vůdce	k1gMnPc2
povstání	povstání	k1gNnSc2
ve	v	k7c6
Vilně	vilně	k6eAd1
a	a	k8xC
posléze	posléze	k6eAd1
ve	v	k7c6
Varšavě	Varšava	k1gFnSc6
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
Jasiński	Jasińsk	k1gFnSc2
</s>
<s>
Povstání	povstání	k1gNnSc1
na	na	k7c6
Litvě	Litva	k1gFnSc6
začalo	začít	k5eAaPmAgNnS
ve	v	k7c6
Žmudi	Žmud	k1gMnPc5
<g/>
,	,	kIx,
kde	kde	k6eAd1
nebyly	být	k5eNaImAgFnP
přítomné	přítomný	k2eAgFnPc1d1
žádné	žádný	k3yNgFnPc1
ruské	ruský	k2eAgFnPc1d1
posádky	posádka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
čela	čelo	k1gNnSc2
se	se	k3xPyFc4
postavili	postavit	k5eAaPmAgMnP
Antoni	Anton	k1gMnPc1
Chlewiński	Chlewiński	k1gNnSc2
a	a	k8xC
Jakub	Jakub	k1gMnSc1
Jasiński	Jasińsk	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
noci	noc	k1gFnSc6
z	z	k7c2
23	#num#	k4
<g/>
.	.	kIx.
na	na	k7c4
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1794	#num#	k4
pak	pak	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
převratu	převrat	k1gInSc3
i	i	k9
ve	v	k7c6
Vilně	vilně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruská	ruský	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
byla	být	k5eAaImAgFnS
pobita	pobit	k2eAgFnSc1d1
<g/>
,	,	kIx,
zajata	zajat	k2eAgFnSc1d1
nebo	nebo	k8xC
donucena	donucen	k2eAgFnSc1d1
opustit	opustit	k5eAaPmF
město	město	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
vyhlášen	vyhlásit	k5eAaPmNgInS
Akt	akt	k1gInSc1
povstání	povstání	k1gNnSc2
litevského	litevský	k2eAgInSc2d1
národa	národ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc1d1
podíl	podíl	k1gInSc1
na	na	k7c6
akcích	akce	k1gFnPc6
mělo	mít	k5eAaImAgNnS
litevské	litevský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
měšťanstvo	měšťanstvo	k1gNnSc1
a	a	k8xC
městský	městský	k2eAgInSc1d1
plebs	plebs	k1gInSc1
<g/>
,	,	kIx,
jen	jen	k9
z	z	k7c2
malé	malý	k2eAgFnSc2d1
části	část	k1gFnSc2
šlechta	šlechta	k1gFnSc1
a	a	k8xC
rolníci	rolník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povstání	povstání	k1gNnSc1
zpočátku	zpočátku	k6eAd1
probíhalo	probíhat	k5eAaImAgNnS
mimo	mimo	k7c4
kontrolu	kontrola	k1gFnSc4
Kościuszka	Kościuszka	k1gFnSc1
a	a	k8xC
vedení	vedení	k1gNnSc1
polské	polský	k2eAgFnSc2d1
odnože	odnož	k1gFnSc2
vzpoury	vzpoura	k1gFnSc2
a	a	k8xC
bylo	být	k5eAaImAgNnS
velmi	velmi	k6eAd1
radikální	radikální	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otevřeně	otevřeně	k6eAd1
se	se	k3xPyFc4
hlásilo	hlásit	k5eAaImAgNnS
k	k	k7c3
jakobinismu	jakobinismus	k1gInSc3
a	a	k8xC
již	již	k6eAd1
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
zde	zde	k6eAd1
byl	být	k5eAaImAgMnS
popraven	popraven	k2eAgMnSc1d1
první	první	k4xOgMnSc1
zrádce	zrádce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
byl	být	k5eAaImAgMnS
ale	ale	k9
Jasiński	Jasińsk	k1gFnSc2
donucen	donutit	k5eAaPmNgMnS
opustit	opustit	k5eAaPmF
Vilno	Vilno	k6eAd1
a	a	k8xC
přemístit	přemístit	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
Varšavy	Varšava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radikalita	radikalita	k1gFnSc1
hnutí	hnutí	k1gNnSc2
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
oslabena	oslabit	k5eAaPmNgFnS
a	a	k8xC
ústředí	ústředí	k1gNnSc1
tak	tak	k6eAd1
získalo	získat	k5eAaPmAgNnS
větší	veliký	k2eAgFnSc4d2
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
litevskými	litevský	k2eAgFnPc7d1
událostmi	událost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
i	i	k9
kvůli	kvůli	k7c3
tomu	ten	k3xDgInSc3
se	se	k3xPyFc4
ve	v	k7c6
Varšavě	Varšava	k1gFnSc6
posílily	posílit	k5eAaPmAgFnP
levicové	levicový	k2eAgInPc4d1
aspekty	aspekt	k1gInPc4
povstání	povstání	k1gNnSc2
a	a	k8xC
zostřily	zostřit	k5eAaPmAgInP
se	se	k3xPyFc4
vnitropovstalecké	vnitropovstalecký	k2eAgInPc1d1
spory	spor	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
<g/>
Uniwersał	Uniwersał	k1gFnSc1
Połaniecki	Połanieck	k1gFnSc2
<g/>
“	“	k?
</s>
<s>
Mapa	mapa	k1gFnSc1
povstání	povstání	k1gNnSc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
vydal	vydat	k5eAaPmAgMnS
Kościuszko	Kościuszka	k1gFnSc5
zákon	zákon	k1gInSc1
známý	známý	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
Uniwersał	Uniwersał	k1gFnSc1
Połaniecki	Połanieck	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
Połaniecká	Połaniecký	k2eAgFnSc1d1
proklamace	proklamace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
omezil	omezit	k5eAaPmAgMnS
nevolnictví	nevolnictví	k1gNnSc2
<g/>
,	,	kIx,
s	s	k7c7
příslibem	příslib	k1gInSc7
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
úplného	úplný	k2eAgNnSc2d1
zrušení	zrušení	k1gNnSc2
<g/>
,	,	kIx,
garantoval	garantovat	k5eAaBmAgInS
občanská	občanský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
všem	všecek	k3xTgMnPc3
rolníkům	rolník	k1gMnPc3
a	a	k8xC
zaručil	zaručit	k5eAaPmAgMnS
jim	on	k3xPp3gMnPc3
státní	státní	k2eAgFnPc4d1
ochranu	ochrana	k1gFnSc4
před	před	k7c7
zvůlí	zvůle	k1gFnSc7
ze	z	k7c2
strany	strana	k1gFnSc2
šlechty	šlechta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rolníkům	rolník	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
vstoupí	vstoupit	k5eAaPmIp3nP
do	do	k7c2
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
garantoval	garantovat	k5eAaBmAgInS
zrušení	zrušení	k1gNnSc4
nevolnictví	nevolnictví	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Ačkoli	ačkoli	k8xS
zákon	zákon	k1gInSc1
nenabyl	nabýt	k5eNaPmAgInS
faktické	faktický	k2eAgFnPc4d1
účinnosti	účinnost	k1gFnPc4
a	a	k8xC
byl	být	k5eAaImAgInS
navíc	navíc	k6eAd1
bojkotován	bojkotován	k2eAgMnSc1d1
částí	část	k1gFnSc7
šlechty	šlechta	k1gFnSc2
<g/>
,	,	kIx,
nalákal	nalákat	k5eAaPmAgInS
mnoho	mnoho	k4c4
rolníků	rolník	k1gMnPc2
do	do	k7c2
armády	armáda	k1gFnSc2
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
jakýmsi	jakýsi	k3yIgInSc7
symbolem	symbol	k1gInSc7
povstání	povstání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
poprvé	poprvé	k6eAd1
v	v	k7c6
dějinách	dějiny	k1gFnPc6
Polska	Polsko	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
rolníci	rolník	k1gMnPc1
měli	mít	k5eAaImAgMnP
oficiálně	oficiálně	k6eAd1
stát	stát	k5eAaImF,k5eAaPmF
součástí	součást	k1gFnSc7
národa	národ	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
dříve	dříve	k6eAd2
vyhrazeno	vyhradit	k5eAaPmNgNnS
téměř	téměř	k6eAd1
výhradně	výhradně	k6eAd1
šlechtě	šlechta	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Rusko	Rusko	k1gNnSc1
a	a	k8xC
Prusko	Prusko	k1gNnSc1
útočí	útočit	k5eAaImIp3nS
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Szczekociny	Szczekocin	k2eAgFnSc2d1
na	na	k7c6
dobovém	dobový	k2eAgInSc6d1
náčrtku	náčrtek	k1gInSc6
</s>
<s>
Navzdory	navzdory	k7c3
příslibům	příslib	k1gInPc3
reforem	reforma	k1gFnPc2
<g/>
,	,	kIx,
nabíráním	nabírání	k1gNnSc7
rolníků	rolník	k1gMnPc2
do	do	k7c2
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
nadšení	nadšení	k1gNnSc3
a	a	k8xC
prvotním	prvotní	k2eAgInPc3d1
úspěchům	úspěch	k1gInPc3
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
strategická	strategický	k2eAgFnSc1d1
situace	situace	k1gFnSc1
Polska	Polsko	k1gNnSc2
špatná	špatný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kościuszkovi	Kościuszkův	k2eAgMnPc5d1
se	se	k3xPyFc4
sice	sice	k8xC
podařilo	podařit	k5eAaPmAgNnS
shromáždit	shromáždit	k5eAaPmF
během	během	k7c2
poměrně	poměrně	k6eAd1
krátké	krátký	k2eAgFnSc2d1
doby	doba	k1gFnSc2
více	hodně	k6eAd2
než	než	k8xS
15	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
do	do	k7c2
své	svůj	k3xOyFgFnSc2
hlavní	hlavní	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
převaha	převaha	k1gFnSc1
protivníků	protivník	k1gMnPc2
byla	být	k5eAaImAgFnS
drtivá	drtivý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kościuszko	Kościuszka	k1gFnSc5
doufal	doufat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Prusko	Prusko	k1gNnSc1
zůstane	zůstat	k5eAaPmIp3nS
neutrální	neutrální	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
nestalo	stát	k5eNaPmAgNnS
se	se	k3xPyFc4
tak	tak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polský	polský	k2eAgInSc1d1
boj	boj	k1gInSc1
za	za	k7c4
svobodu	svoboda	k1gFnSc4
národa	národ	k1gInSc2
<g/>
,	,	kIx,
garance	garance	k1gFnPc1
práv	právo	k1gNnPc2
rolníkům	rolník	k1gMnPc3
a	a	k8xC
snaha	snaha	k1gFnSc1
o	o	k7c6
demokratizaci	demokratizace	k1gFnSc6
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
vše	všechen	k3xTgNnSc1
dráždilo	dráždit	k5eAaImAgNnS
Prusy	Prus	k1gMnPc4
stejně	stejně	k6eAd1
jako	jako	k9
Rusy	Rus	k1gMnPc4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
překročilo	překročit	k5eAaPmAgNnS
hranice	hranice	k1gFnPc4
17	#num#	k4
500	#num#	k4
pruských	pruský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
vedených	vedený	k2eAgFnPc2d1
generálem	generál	k1gMnSc7
Francisem	Francis	k1gInSc7
Favratem	Favrat	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
do	do	k7c2
čela	čelo	k1gNnSc2
armády	armáda	k1gFnSc2
postavil	postavit	k5eAaPmAgMnS
samotný	samotný	k2eAgMnSc1d1
král	král	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Vilém	Vilém	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pruské	pruský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
se	se	k3xPyFc4
připojilo	připojit	k5eAaPmAgNnS
k	k	k7c3
9	#num#	k4
000	#num#	k4
ruským	ruský	k2eAgMnSc7d1
vojákům	voják	k1gMnPc3
operujícím	operující	k2eAgNnSc6d1
v	v	k7c6
severním	severní	k2eAgNnSc6d1
Polsku	Polsko	k1gNnSc6
<g/>
,	,	kIx,
jimž	jenž	k3xRgMnPc3
veleli	velet	k5eAaImAgMnP
Fjodor	Fjodor	k1gMnSc1
Děnisov	Děnisov	k1gInSc1
a	a	k8xC
Ivan	Ivan	k1gMnSc1
Fersen	Fersna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kościuszko	Kościuszka	k1gFnSc5
se	s	k7c7
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
pokusil	pokusit	k5eAaPmAgMnS
tuto	tento	k3xDgFnSc4
armádu	armáda	k1gFnSc4
zastavit	zastavit	k5eAaPmF
u	u	k7c2
Szczekociny	Szczekocin	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tentokrát	tentokrát	k6eAd1
se	se	k3xPyFc4
však	však	k9
projevila	projevit	k5eAaPmAgFnS
převaha	převaha	k1gFnSc1
pravidelné	pravidelný	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
vyzbrojené	vyzbrojený	k2eAgFnSc2d1
množstvím	množství	k1gNnSc7
kanónů	kanón	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruské	ruský	k2eAgInPc1d1
a	a	k8xC
pruské	pruský	k2eAgNnSc4d1
dělostřelectvo	dělostřelectvo	k1gNnSc4
způsobilo	způsobit	k5eAaPmAgNnS
polské	polský	k2eAgFnSc3d1
pěchotě	pěchota	k1gFnSc3
těžké	těžký	k2eAgFnSc2d1
ztráty	ztráta	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
i	i	k9
když	když	k8xS
se	se	k3xPyFc4
Poláci	Polák	k1gMnPc1
bili	bít	k5eAaImAgMnP
statečně	statečně	k6eAd1
<g/>
,	,	kIx,
pravidelné	pravidelný	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
jezdectva	jezdectvo	k1gNnSc2
a	a	k8xC
pěchoty	pěchota	k1gFnSc2
vítězství	vítězství	k1gNnSc2
dokonaly	dokonat	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
Poláci	Polák	k1gMnPc1
ztratili	ztratit	k5eAaPmAgMnP
asi	asi	k9
1	#num#	k4
250	#num#	k4
mrtvých	mrtvý	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
několika	několik	k4yIc2
generálů	generál	k1gMnPc2
a	a	k8xC
hrdiny	hrdina	k1gMnPc7
od	od	k7c2
Raclawic	Raclawice	k1gFnPc2
Bartosze	Bartosze	k1gFnSc2
Głowackiho	Głowacki	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zraněných	zraněný	k2eAgMnPc2d1
bylo	být	k5eAaImAgNnS
přes	přes	k7c4
700	#num#	k4
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
i	i	k9
samotný	samotný	k2eAgInSc1d1
Kościuszko	Kościuszka	k1gFnSc5
<g/>
.	.	kIx.
500	#num#	k4
mužů	muž	k1gMnPc2
bylo	být	k5eAaImAgNnS
zajato	zajmout	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rusové	k2eAgMnPc2d1
a	a	k8xC
Prusové	Prus	k1gMnPc1
přišli	přijít	k5eAaPmAgMnP
asi	asi	k9
o	o	k7c4
1	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
zabitých	zabitý	k2eAgFnPc2d1
a	a	k8xC
o	o	k7c4
něco	něco	k3yInSc4
menší	malý	k2eAgInSc1d2
počet	počet	k1gInSc1
zraněných	zraněný	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc4
dny	den	k1gInPc4
nato	nato	k6eAd1
byla	být	k5eAaImAgFnS
malá	malý	k2eAgFnSc1d1
polská	polský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
vedl	vést	k5eAaImAgMnS
generál	generál	k1gMnSc1
Józef	Józef	k1gMnSc1
Zajączek	Zajączka	k1gFnPc2
<g/>
,	,	kIx,
zdecimována	zdecimován	k2eAgFnSc1d1
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Chelmu	Chelm	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
6	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
ztratili	ztratit	k5eAaPmAgMnP
Poláci	Polák	k1gMnPc1
asi	asi	k9
1	#num#	k4
500	#num#	k4
<g/>
,	,	kIx,
ruské	ruský	k2eAgFnPc4d1
síly	síla	k1gFnPc4
jen	jen	k9
zhruba	zhruba	k6eAd1
200	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbylé	zbylý	k2eAgInPc4d1
polské	polský	k2eAgInPc4d1
útvary	útvar	k1gInPc4
se	se	k3xPyFc4
většinou	většinou	k6eAd1
stáhly	stáhnout	k5eAaPmAgInP
do	do	k7c2
Varšavy	Varšava	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
začali	začít	k5eAaPmAgMnP
opevňovat	opevňovat	k5eAaImF
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
obsadili	obsadit	k5eAaPmAgMnP
Prusové	Prus	k1gMnPc1
Krakov	Krakov	k1gInSc4
a	a	k8xC
v	v	k7c6
červenci	červenec	k1gInSc6
obklíčily	obklíčit	k5eAaPmAgFnP
ruské	ruský	k2eAgFnPc1d1
a	a	k8xC
pruské	pruský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
Varšavu	Varšava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
v	v	k7c6
srpnu	srpen	k1gInSc6
vypukly	vypuknout	k5eAaPmAgFnP
nepokoje	nepokoj	k1gInPc4
ve	v	k7c4
Velkopolsku	Velkopolska	k1gFnSc4
a	a	k8xC
obě	dva	k4xCgFnPc4
armády	armáda	k1gFnPc4
obléhání	obléhání	k1gNnPc2
přerušily	přerušit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s>
Zásah	zásah	k1gInSc1
Rakouska	Rakousko	k1gNnSc2
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
se	s	k7c7
znepokojením	znepokojení	k1gNnSc7
sledovalo	sledovat	k5eAaImAgNnS
situaci	situace	k1gFnSc4
v	v	k7c6
Polsku	Polsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tentokrát	tentokrát	k6eAd1
nehodlalo	hodlat	k5eNaImAgNnS
zůstat	zůstat	k5eAaPmF
stranou	stranou	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
při	při	k7c6
druhém	druhý	k4xOgNnSc6
dělení	dělení	k1gNnSc6
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1794	#num#	k4
vstoupil	vstoupit	k5eAaPmAgInS
rakouský	rakouský	k2eAgInSc1d1
sbor	sbor	k1gInSc1
do	do	k7c2
vojvodství	vojvodství	k1gNnSc2
krakovského	krakovský	k2eAgNnSc2d1
<g/>
,	,	kIx,
sandoměřského	sandoměřský	k2eAgMnSc2d1
a	a	k8xC
lublinského	lublinský	k2eAgInSc2d1
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
obsadil	obsadit	k5eAaPmAgMnS
Lublin	Lublin	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Dobývání	dobývání	k1gNnSc1
Polska	Polsko	k1gNnSc2
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
Ivan	Ivan	k1gMnSc1
Fersen	Fersna	k1gFnPc2
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
hlavních	hlavní	k2eAgMnPc2d1
potlačovatelů	potlačovatel	k1gMnPc2
povstání	povstání	k1gNnSc2
(	(	kIx(
<g/>
portrét	portrét	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1795	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
létě	léto	k1gNnSc6
1794	#num#	k4
potlačila	potlačit	k5eAaPmAgFnS
ruská	ruský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
povstání	povstání	k1gNnSc2
v	v	k7c6
Litvě	Litva	k1gFnSc6
-	-	kIx~
Vilnius	Vilnius	k1gInSc1
kapituloval	kapitulovat	k5eAaBmAgInS
12	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
povstání	povstání	k1gNnSc3
ve	v	k7c6
Velkopolsku	Velkopolsko	k1gNnSc6
dosáhlo	dosáhnout	k5eAaPmAgNnS
jistých	jistý	k2eAgInPc2d1
úspěchů	úspěch	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polské	polský	k2eAgInPc1d1
sbory	sbor	k1gInPc1
vedené	vedený	k2eAgInPc1d1
Janem	Jan	k1gMnSc7
Henrykem	Henryk	k1gInSc7
Dąbrowskim	Dąbrowskimo	k1gNnPc2
pronikly	proniknout	k5eAaPmAgFnP
k	k	k7c3
Pruskem	Prusko	k1gNnSc7
obsazené	obsazený	k2eAgFnSc2d1
Bydhošti	Bydhošti	k1gFnSc2
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
ji	on	k3xPp3gFnSc4
obsadily	obsadit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dąbrowského	Dąbrowského	k2eAgInPc1d1
útvary	útvar	k1gInPc1
postupovaly	postupovat	k5eAaImAgInP
velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
<g/>
,	,	kIx,
vyhnuly	vyhnout	k5eAaPmAgFnP
se	se	k3xPyFc4
obklíčení	obklíčení	k1gNnSc2
pomalejší	pomalý	k2eAgFnSc7d2
pruskou	pruský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
a	a	k8xC
donutily	donutit	k5eAaPmAgFnP
Prusy	Prus	k1gMnPc4
opustit	opustit	k5eAaPmF
centrální	centrální	k2eAgNnSc1d1
Polsko	Polsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trvalejších	trvalý	k2eAgMnPc2d2
zisků	zisk	k1gInPc2
však	však	k9
Poláci	Polák	k1gMnPc1
nedosáhli	dosáhnout	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Rusové	Rus	k1gMnPc1
mezitím	mezitím	k6eAd1
nasadili	nasadit	k5eAaPmAgMnP
další	další	k2eAgInSc4d1
sbor	sbor	k1gInSc4
<g/>
,	,	kIx,
kterému	který	k3yRgMnSc3,k3yQgMnSc3,k3yIgMnSc3
velel	velet	k5eAaImAgInS
osvědčený	osvědčený	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
Vasiljevič	Vasiljevič	k1gMnSc1
Suvorov	Suvorovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poláci	Polák	k1gMnPc1
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc4
sice	sice	k8xC
pokoušeli	pokoušet	k5eAaImAgMnP
zastavit	zastavit	k5eAaPmF
při	při	k7c6
postupu	postup	k1gInSc6
do	do	k7c2
centrálního	centrální	k2eAgNnSc2d1
Polska	Polsko	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
utrpěli	utrpět	k5eAaPmAgMnP
v	v	k7c6
září	září	k1gNnSc6
1794	#num#	k4
několik	několik	k4yIc1
porážek	porážka	k1gFnPc2
(	(	kIx(
<g/>
bitva	bitva	k1gFnSc1
u	u	k7c2
Krupčic	Krupčice	k1gFnPc2
a	a	k8xC
bitva	bitva	k1gFnSc1
u	u	k7c2
Terespolu	Terespol	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Suvorov	Suvorov	k1gInSc1
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c6
postupu	postup	k1gInSc6
na	na	k7c4
Varšavu	Varšava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
v	v	k7c6
úmyslu	úmysl	k1gInSc6
spojit	spojit	k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
jednotkami	jednotka	k1gFnPc7
Fjodora	Fjodora	k1gFnSc1
Děnisova	Děnisův	k2eAgFnSc1d1
a	a	k8xC
Ivana	Ivana	k1gFnSc1
Fersena	Fersen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kościuszko	Kościuszka	k1gFnSc5
věděl	vědět	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
se	se	k3xPyFc4
ruské	ruský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
spojí	spojit	k5eAaPmIp3nS
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
prakticky	prakticky	k6eAd1
nemožné	možný	k2eNgNnSc1d1
je	být	k5eAaImIp3nS
porazit	porazit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
proto	proto	k8xC
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
nejrychleji	rychle	k6eAd3
napadnout	napadnout	k5eAaPmF
a	a	k8xC
zničit	zničit	k5eAaPmF
Děnisova	Děnisův	k2eAgFnSc1d1
a	a	k8xC
Ferzena	Ferzen	k2eAgFnSc1d1
a	a	k8xC
následně	následně	k6eAd1
se	se	k3xPyFc4
pokusit	pokusit	k5eAaPmF
zastavit	zastavit	k5eAaPmF
Suvorova	Suvorův	k2eAgFnSc1d1
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
se	se	k3xPyFc4
polská	polský	k2eAgFnSc1d1
a	a	k8xC
ruská	ruský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
střetla	střetnout	k5eAaPmAgNnP
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Maciejowic	Maciejowice	k1gInPc2
(	(	kIx(
<g/>
česky	česky	k6eAd1
Matějovice	Matějovice	k1gFnSc1
<g/>
,	,	kIx,
vesnice	vesnice	k1gFnSc1
asi	asi	k9
80	#num#	k4
km	km	kA
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
Varšavy	Varšava	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zajetí	zajetí	k1gNnSc1
Kościuszka	Kościuszka	k1gFnSc1
</s>
<s>
Kozáci	Kozák	k1gMnPc1
útočí	útočit	k5eAaImIp3nP
na	na	k7c6
Tadeusze	Tadeusza	k1gFnSc6
Kościuszka	Kościuszka	k1gFnSc1
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Maciejowic	Maciejowice	k1gInPc2
(	(	kIx(
<g/>
Matějovic	Matějovice	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Tadeusz	Tadeusz	k1gMnSc1
Kościuszko	Kościuszka	k1gFnSc5
měl	mít	k5eAaImAgInS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
jen	jen	k9
6	#num#	k4
300	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
přesto	přesto	k8xC
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
čelit	čelit	k5eAaImF
ruskému	ruský	k2eAgInSc3d1
sboru	sbor	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
téměř	téměř	k6eAd1
dvakrát	dvakrát	k6eAd1
silnější	silný	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doufal	doufat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
ještě	ještě	k9
posílí	posílit	k5eAaPmIp3nS
útvar	útvar	k1gInSc1
o	o	k7c4
4	#num#	k4
000	#num#	k4
vojácích	voják	k1gMnPc6
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
vedl	vést	k5eAaImAgMnS
Adam	Adam	k1gMnSc1
Poniński	Ponińsk	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
vinou	vina	k1gFnSc7
zpoždění	zpoždění	k1gNnSc2
poslů	posel	k1gMnPc2
(	(	kIx(
<g/>
nebo	nebo	k8xC
zrady	zrada	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
nestalo	stát	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poláci	Polák	k1gMnPc1
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
vybudovat	vybudovat	k5eAaPmF
obranná	obranný	k2eAgNnPc4d1
postavení	postavení	k1gNnPc4
u	u	k7c2
vesnice	vesnice	k1gFnSc2
Maciejowice	Maciejowice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velitelství	velitelství	k1gNnSc1
si	se	k3xPyFc3
zřídili	zřídit	k5eAaPmAgMnP
v	v	k7c6
zámku	zámek	k1gInSc6
Podzamcze	Podzamcze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rusové	k2eAgMnPc2d1
v	v	k7c6
noci	noc	k1gFnSc6
překročili	překročit	k5eAaPmAgMnP
Vislu	Visla	k1gFnSc4
a	a	k8xC
začali	začít	k5eAaPmAgMnP
svůj	svůj	k3xOyFgInSc4
útok	útok	k1gInSc4
10	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
ráno	ráno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpoutala	rozpoutat	k5eAaPmAgFnS
se	se	k3xPyFc4
krvavá	krvavý	k2eAgFnSc1d1
řež	řež	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
trvala	trvat	k5eAaImAgFnS
několik	několik	k4yIc4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
Poláci	Polák	k1gMnPc1
bojovali	bojovat	k5eAaImAgMnP
neobyčejně	obyčejně	k6eNd1
srdnatě	srdnatě	k6eAd1
<g/>
,	,	kIx,
postupně	postupně	k6eAd1
se	se	k3xPyFc4
projevila	projevit	k5eAaPmAgFnS
ruská	ruský	k2eAgFnSc1d1
převaha	převaha	k1gFnSc1
ve	v	k7c6
všech	všecek	k3xTgInPc6
druzích	druh	k1gInPc6
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polské	polský	k2eAgFnSc6d1
artilérii	artilérie	k1gFnSc6
došla	dojít	k5eAaPmAgFnS
munice	munice	k1gFnSc1
<g/>
,	,	kIx,
jízda	jízda	k1gFnSc1
nevydržela	vydržet	k5eNaPmAgFnS
nápor	nápor	k1gInSc4
kozáků	kozák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruské	ruský	k2eAgNnSc1d1
dělostřelectvo	dělostřelectvo	k1gNnSc1
zmasakrovalo	zmasakrovat	k5eAaPmAgNnS
kartáčovou	kartáčový	k2eAgFnSc7d1
palbou	palba	k1gFnSc7
i	i	k8xC
poslední	poslední	k2eAgInSc1d1
zoufalý	zoufalý	k2eAgInSc1d1
útok	útok	k1gInSc1
kosynierow	kosynierow	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jízda	jízda	k1gFnSc1
v	v	k7c6
panice	panika	k1gFnSc6
opustila	opustit	k5eAaPmAgFnS
bojiště	bojiště	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
pěchota	pěchota	k1gFnSc1
rozdělená	rozdělený	k2eAgFnSc1d1
na	na	k7c4
skupiny	skupina	k1gFnPc4
se	se	k3xPyFc4
bránila	bránit	k5eAaImAgFnS
až	až	k9
do	do	k7c2
odpoledne	odpoledne	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kościuszko	Kościuszka	k1gFnSc5
<g/>
,	,	kIx,
pod	pod	k7c7
nímž	jenž	k3xRgNnSc7
padlo	padnout	k5eAaPmAgNnS,k5eAaImAgNnS
několik	několik	k4yIc1
koní	kůň	k1gMnPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
zraněn	zranit	k5eAaPmNgMnS
ranou	rána	k1gFnSc7
šavlí	šavle	k1gFnPc2
do	do	k7c2
hlavy	hlava	k1gFnSc2
<g/>
,	,	kIx,
probodnut	probodnut	k2eAgMnSc1d1
kopím	kopit	k5eAaImIp1nS
a	a	k8xC
nakonec	nakonec	k6eAd1
zajat	zajat	k2eAgInSc4d1
kozáky	kozák	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údajně	údajně	k6eAd1
zvolal	zvolat	k5eAaPmAgInS
„	„	k?
<g/>
Finis	finis	k1gInSc1
Poloniae	Poloniae	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Konec	konec	k1gInSc1
Polska	Polsko	k1gNnSc2
<g/>
;	;	kIx,
pravděpodobně	pravděpodobně	k6eAd1
jde	jít	k5eAaImIp3nS
jen	jen	k9
o	o	k7c4
legendu	legenda	k1gFnSc4
<g/>
)	)	kIx)
a	a	k8xC
pokusil	pokusit	k5eAaPmAgMnS
se	se	k3xPyFc4
zastřelit	zastřelit	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
pistole	pistole	k1gFnSc1
nevystřelila	vystřelit	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
po	po	k7c6
bitvě	bitva	k1gFnSc6
dorazil	dorazit	k5eAaPmAgMnS
Poniński	Ponińske	k1gFnSc4
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
vojáky	voják	k1gMnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
když	když	k8xS
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
po	po	k7c6
všem	všecek	k3xTgNnSc6
<g/>
,	,	kIx,
stáhl	stáhnout	k5eAaPmAgMnS
se	se	k3xPyFc4
k	k	k7c3
Varšavě	Varšava	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
ztratili	ztratit	k5eAaPmAgMnP
Poláci	Polák	k1gMnPc1
4	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
Rusové	Rusové	k2eAgInSc4d1
2	#num#	k4
300	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kościuszkovi	Kościuszkův	k2eAgMnPc5d1
nedával	dávat	k5eNaImAgMnS
ruský	ruský	k2eAgMnSc1d1
polní	polní	k2eAgMnSc1d1
felčar	felčar	k1gMnSc1
příliš	příliš	k6eAd1
nadějí	naděje	k1gFnSc7
na	na	k7c4
přežití	přežití	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
polský	polský	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
se	se	k3xPyFc4
uzdravoval	uzdravovat	k5eAaImAgMnS
nezvykle	zvykle	k6eNd1
dobře	dobře	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
proto	proto	k8xC
eskortován	eskortovat	k5eAaBmNgInS
do	do	k7c2
Petrohradu	Petrohrad	k1gInSc2
a	a	k8xC
uvězněn	uvězněn	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Boj	boj	k1gInSc1
o	o	k7c4
Varšavu	Varšava	k1gFnSc4
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Druhá	druhý	k4xOgFnSc1
bitva	bitva	k1gFnSc1
u	u	k7c2
Varšavy	Varšava	k1gFnSc2
roku	rok	k1gInSc2
1794	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Obrana	obrana	k1gFnSc1
Pragi	Prag	k1gFnSc2
</s>
<s>
Novým	nový	k2eAgMnSc7d1
velitelem	velitel	k1gMnSc7
vojenských	vojenský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
Tomasz	Tomasz	k1gInSc1
Wawrzecki	Wawrzeck	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
politickou	politický	k2eAgFnSc4d1
moc	moc	k1gFnSc4
měl	mít	k5eAaImAgMnS
generál	generál	k1gMnSc1
Józef	Józef	k1gMnSc1
Zajączek	Zajączka	k1gFnPc2
(	(	kIx(
<g/>
1752	#num#	k4
<g/>
-	-	kIx~
<g/>
1826	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
ve	v	k7c6
velmi	velmi	k6eAd1
svízelné	svízelný	k2eAgFnSc6d1
situaci	situace	k1gFnSc6
<g/>
,	,	kIx,
probíhaly	probíhat	k5eAaImAgFnP
i	i	k9
v	v	k7c6
hnutí	hnutí	k1gNnSc6
odporu	odpor	k1gInSc2
vnitřní	vnitřní	k2eAgInPc4d1
boje	boj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavními	hlavní	k2eAgMnPc7d1
soupeři	soupeř	k1gMnPc7
byli	být	k5eAaImAgMnP
polští	polský	k2eAgMnPc1d1
jakobíni	jakobín	k1gMnPc1
<g/>
,	,	kIx,
umírněná	umírněný	k2eAgFnSc1d1
pravice	pravice	k1gFnSc1
a	a	k8xC
monarchistická	monarchistický	k2eAgFnSc1d1
šlechta	šlechta	k1gFnSc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
králem	král	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
hrál	hrát	k5eAaImAgMnS
doposud	doposud	k6eAd1
v	v	k7c6
událostech	událost	k1gFnPc6
povstání	povstání	k1gNnSc2
jen	jen	k9
malou	malý	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
těmto	tento	k3xDgInPc3
vnitřním	vnitřní	k2eAgInPc3d1
rozporům	rozpor	k1gInPc3
docházelo	docházet	k5eAaImAgNnS
prakticky	prakticky	k6eAd1
od	od	k7c2
začátku	začátek	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
teď	teď	k6eAd1
měly	mít	k5eAaImAgInP
obzvláště	obzvláště	k6eAd1
destruktivní	destruktivní	k2eAgInPc1d1
účinky	účinek	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
příčin	příčina	k1gFnPc2
<g/>
,	,	kIx,
proč	proč	k6eAd1
obránci	obránce	k1gMnPc1
nedokončili	dokončit	k5eNaPmAgMnP
opevňování	opevňování	k1gNnSc4
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sbor	sbor	k1gInSc1
generála	generál	k1gMnSc2
Henryka	Henryek	k1gMnSc2
Dąbrowského	Dąbrowský	k1gMnSc2
mezitím	mezitím	k6eAd1
opustil	opustit	k5eAaPmAgMnS
Velkopolsko	Velkopolsko	k1gNnSc4
a	a	k8xC
stáhnul	stáhnout	k5eAaPmAgMnS
se	se	k3xPyFc4
do	do	k7c2
Brochówa	Brochów	k1gInSc2
nad	nad	k7c7
Bzurou	Bzura	k1gFnSc7
<g/>
,	,	kIx,
asi	asi	k9
60	#num#	k4
km	km	kA
západně	západně	k6eAd1
od	od	k7c2
Varšavy	Varšava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
neposílit	posílit	k5eNaPmF
městskou	městský	k2eAgFnSc4d1
posádku	posádka	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
polská	polský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
nebyla	být	k5eNaImAgFnS
umístěná	umístěný	k2eAgFnSc1d1
jen	jen	k9
na	na	k7c6
jednom	jeden	k4xCgInSc6
místě	místo	k1gNnSc6
a	a	k8xC
nutila	nutit	k5eAaImAgFnS
Prusy	Prus	k1gMnPc4
a	a	k8xC
Rusy	Rus	k1gMnPc4
operovat	operovat	k5eAaImF
odděleně	odděleně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
však	však	k9
ukázalo	ukázat	k5eAaPmAgNnS
jako	jako	k9
neprozíravé	prozíravý	k2eNgNnSc1d1
a	a	k8xC
v	v	k7c6
podstatě	podstata	k1gFnSc6
zbytečné	zbytečný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruská	ruský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
byla	být	k5eAaImAgFnS
dostatečně	dostatečně	k6eAd1
silná	silný	k2eAgFnSc1d1
a	a	k8xC
mohla	moct	k5eAaImAgFnS
se	se	k3xPyFc4
soustředit	soustředit	k5eAaPmF
na	na	k7c4
rozhodný	rozhodný	k2eAgInSc4d1
útok	útok	k1gInSc4
na	na	k7c4
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Ruské	ruský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
dosáhly	dosáhnout	k5eAaPmAgFnP
3	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
předměstí	předměstí	k1gNnSc2
a	a	k8xC
okamžitě	okamžitě	k6eAd1
zahájily	zahájit	k5eAaPmAgInP
dělostřeleckou	dělostřelecký	k2eAgFnSc4d1
palbu	palba	k1gFnSc4
na	na	k7c4
obránce	obránce	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polští	polský	k2eAgMnPc1d1
velitelé	velitel	k1gMnPc1
předpokládali	předpokládat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
začátek	začátek	k1gInSc4
dlouhého	dlouhý	k2eAgNnSc2d1
obléhání	obléhání	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
Suvorov	Suvorov	k1gInSc1
zvolil	zvolit	k5eAaPmAgInS
jinou	jiný	k2eAgFnSc4d1
taktiku	taktika	k1gFnSc4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
brzy	brzy	k6eAd1
ráno	ráno	k6eAd1
proklouzli	proklouznout	k5eAaPmAgMnP
ruští	ruský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
v	v	k7c6
tichosti	tichost	k1gFnSc6
co	co	k9
nejblíže	blízce	k6eAd3
k	k	k7c3
polským	polský	k2eAgFnPc3d1
pozicím	pozice	k1gFnPc3
a	a	k8xC
zaútočili	zaútočit	k5eAaPmAgMnP
-	-	kIx~
začala	začít	k5eAaPmAgFnS
bitva	bitva	k1gFnSc1
o	o	k7c4
Varšavu	Varšava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útok	útok	k1gInSc1
směřoval	směřovat	k5eAaImAgInS
na	na	k7c4
čtvrť	čtvrť	k1gFnSc4
Praga	Prag	k1gMnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
ještě	ještě	k9
donedávna	donedávna	k6eAd1
samostatné	samostatný	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poláci	Polák	k1gMnPc1
disponovali	disponovat	k5eAaBmAgMnP
asi	asi	k9
13	#num#	k4
až	až	k9
14	#num#	k4
000	#num#	k4
vojáky	voják	k1gMnPc7
a	a	k8xC
několika	několik	k4yIc7
tisíci	tisíc	k4xCgInPc7
dobrovolníky	dobrovolník	k1gMnPc7
a	a	k8xC
milicí	milice	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Některé	některý	k3yIgInPc1
zdroje	zdroj	k1gInPc1
uvádějí	uvádět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
šlo	jít	k5eAaImAgNnS
celkem	celkem	k6eAd1
až	až	k9
o	o	k7c4
30	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Rusové	Rusové	k2eAgInSc1d1
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
útočili	útočit	k5eAaImAgMnP
s	s	k7c7
dobře	dobře	k6eAd1
vybavenou	vybavený	k2eAgFnSc7d1
pravidelnou	pravidelný	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
o	o	k7c6
síle	síla	k1gFnSc6
23	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
vynikajícím	vynikající	k2eAgMnSc7d1
velitelem	velitel	k1gMnSc7
Suvorovem	Suvorov	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prudká	Prudká	k1gFnSc1
bitva	bitva	k1gFnSc1
trvala	trvat	k5eAaImAgFnS
jen	jen	k9
čtyři	čtyři	k4xCgFnPc1
hodiny	hodina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rusové	k2eAgFnSc1d1
zcela	zcela	k6eAd1
ovládli	ovládnout	k5eAaPmAgMnP
bojiště	bojiště	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
vojáků	voják	k1gMnPc2
spolu	spolu	k6eAd1
s	s	k7c7
lehce	lehko	k6eAd1
zraněným	zraněný	k2eAgInSc7d1
Józefem	Józef	k1gInSc7
Zajackem	Zajack	k1gInSc7
se	se	k3xPyFc4
stáhla	stáhnout	k5eAaPmAgFnS
do	do	k7c2
Varšavy	Varšava	k1gFnSc2
na	na	k7c4
levý	levý	k2eAgInSc4d1
břeh	břeh	k1gInSc4
Visly	Visla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poláci	Polák	k1gMnPc1
ztratili	ztratit	k5eAaPmAgMnP
asi	asi	k9
6	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
včetně	včetně	k7c2
Jakuba	Jakub	k1gMnSc2
Jasińského	Jasińský	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Rzeź	Rzeź	k?
Pragi	Pragi	k1gNnSc1
</s>
<s>
Rzeź	Rzeź	k?
Pragi	Pragi	k1gNnSc1
na	na	k7c6
dřevorytu	dřevoryt	k1gInSc6
z	z	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Jakmile	jakmile	k8xS
skončila	skončit	k5eAaPmAgFnS
bitva	bitva	k1gFnSc1
o	o	k7c6
Pragu	Prag	k1gInSc6
<g/>
,	,	kIx,
začali	začít	k5eAaPmAgMnP
se	se	k3xPyFc4
ruští	ruský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
částečně	částečně	k6eAd1
mstít	mstít	k5eAaImF
na	na	k7c6
civilním	civilní	k2eAgNnSc6d1
obyvatelstvu	obyvatelstvo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
události	událost	k1gFnPc1
vešly	vejít	k5eAaPmAgFnP
ve	v	k7c4
známost	známost	k1gFnSc4
jako	jako	k8xC,k8xS
„	„	k?
<g/>
Rzeź	Rzeź	k1gFnSc1
Pragi	Prag	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Suvorov	Suvorov	k1gInSc1
zřejmě	zřejmě	k6eAd1
vydal	vydat	k5eAaPmAgInS
nařízení	nařízení	k1gNnSc4
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
chovat	chovat	k5eAaImF
k	k	k7c3
zajatcům	zajatec	k1gMnPc3
a	a	k8xC
zároveň	zároveň	k6eAd1
aby	aby	kYmCp3nP
byli	být	k5eAaImAgMnP
ušetřeni	ušetřen	k2eAgMnPc1d1
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
povstání	povstání	k1gNnSc2
proti	proti	k7c3
koruně	koruna	k1gFnSc3
neúčastnili	účastnit	k5eNaImAgMnP
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
aby	aby	kYmCp3nP
byly	být	k5eAaImAgFnP
ušetřeny	ušetřen	k2eAgFnPc1d1
ženy	žena	k1gFnPc1
a	a	k8xC
děti	dítě	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rus	k1gMnPc1
porušili	porušit	k5eAaPmAgMnP
Suvorovův	Suvorovův	k2eAgInSc4d1
rozkaz	rozkaz	k1gInSc4
a	a	k8xC
začali	začít	k5eAaPmAgMnP
řež	řež	k1gFnSc4
<g/>
,	,	kIx,
dále	daleko	k6eAd2
ospravedlňovali	ospravedlňovat	k5eAaImAgMnP
tuto	tento	k3xDgFnSc4
akci	akce	k1gFnSc4
jako	jako	k8xC,k8xS
pomstu	pomsta	k1gFnSc4
za	za	k7c4
masakr	masakr	k1gInSc4
neozbrojených	ozbrojený	k2eNgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
šli	jít	k5eAaImAgMnP
o	o	k7c6
Velikonocích	Velikonoce	k1gFnPc6
v	v	k7c6
dubnu	duben	k1gInSc6
1794	#num#	k4
do	do	k7c2
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
drancování	drancování	k1gNnSc2
se	se	k3xPyFc4
mstili	mstít	k5eAaImAgMnP
nejen	nejen	k6eAd1
vojáci	voják	k1gMnPc1
carské	carský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
ale	ale	k8xC
i	i	k9
záporožští	záporožský	k2eAgMnPc1d1
kozáci	kozák	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Suvorov	Suvorov	k1gInSc1
nechal	nechat	k5eAaPmAgInS
mezitím	mezitím	k6eAd1
strhnout	strhnout	k5eAaPmF
most	most	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
spojoval	spojovat	k5eAaImAgMnS
Pragu	Praga	k1gFnSc4
a	a	k8xC
levobřežní	levobřežní	k2eAgFnSc4d1
Varšavu	Varšava	k1gFnSc4
<g/>
.	.	kIx.
aby	aby	kYmCp3nS
ušetřil	ušetřit	k5eAaPmAgMnS
zbylé	zbylý	k2eAgFnPc4d1
části	část	k1gFnPc4
Varšavy	Varšava	k1gFnSc2
před	před	k7c7
nelítostným	lítostný	k2eNgNnSc7d1
řáděním	řádění	k1gNnSc7
svých	svůj	k3xOyFgMnPc2
vojáků	voják	k1gMnPc2
a	a	k8xC
aby	aby	kYmCp3nS
se	se	k3xPyFc4
plenění	plenění	k1gNnSc1
nerozšířilo	rozšířit	k5eNaPmAgNnS
na	na	k7c4
většinu	většina	k1gFnSc4
města	město	k1gNnSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
požár	požár	k1gInSc1
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
v	v	k7c6
dřevěné	dřevěný	k2eAgFnSc6d1
Praze	Praha	k1gFnSc6
rychle	rychle	k6eAd1
šířit	šířit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
den	den	k1gInSc1
a	a	k8xC
noc	noc	k1gFnSc1
trvajícího	trvající	k2eAgNnSc2d1
řádění	řádění	k1gNnSc2
soldatesky	soldatesky	k6eAd1
bylo	být	k5eAaImAgNnS
mnoho	mnoho	k4c1
tisíc	tisíc	k4xCgInPc2
mrtvých	mrtvý	k1gMnPc2
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
se	se	k3xPyFc4
udává	udávat	k5eAaImIp3nS
7	#num#	k4
000	#num#	k4
až	až	k9
20	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
civilistů	civilista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
většina	většina	k1gFnSc1
zajatých	zajatý	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
i	i	k8xC
ozbrojených	ozbrojený	k2eAgMnPc2d1
civilistů	civilista	k1gMnPc2
bylo	být	k5eAaImAgNnS
po	po	k7c6
bitvě	bitva	k1gFnSc6
propuštěno	propuštěn	k2eAgNnSc1d1
do	do	k7c2
svých	svůj	k3xOyFgMnPc2
domovů	domov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
Suvorov	Suvorov	k1gInSc4
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
celá	celý	k2eAgFnSc1d1
Praga	Praga	k1gFnSc1
byla	být	k5eAaImAgFnS
posetá	posetý	k2eAgFnSc1d1
mrtvými	mrtvý	k2eAgNnPc7d1
těly	tělo	k1gNnPc7
<g/>
,	,	kIx,
krev	krev	k1gFnSc1
tekla	téct	k5eAaImAgFnS
proudem	proud	k1gInSc7
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konec	konec	k1gInSc1
bojů	boj	k1gInPc2
</s>
<s>
Tomasz	Tomasz	k1gMnSc1
Wawrzecki	Wawrzeck	k1gFnSc2
–	–	k?
nejvyšší	vysoký	k2eAgMnSc1d3
velitel	velitel	k1gMnSc1
povstaleckých	povstalecký	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
po	po	k7c6
zajetí	zajetí	k1gNnSc6
Kościuszka	Kościuszka	k1gFnSc1
</s>
<s>
Zbylé	zbylý	k2eAgFnPc1d1
polské	polský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
byly	být	k5eAaImAgFnP
demoralizované	demoralizovaný	k2eAgFnPc1d1
a	a	k8xC
do	do	k7c2
8	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
buď	buď	k8xC
opustily	opustit	k5eAaPmAgFnP
Varšavu	Varšava	k1gFnSc4
nebo	nebo	k8xC
se	se	k3xPyFc4
vzdaly	vzdát	k5eAaPmAgFnP
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
vstoupila	vstoupit	k5eAaPmAgFnS
ruská	ruský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
do	do	k7c2
levobřežní	levobřežní	k2eAgFnSc2d1
Varšavy	Varšava	k1gFnSc2
<g/>
,	,	kIx,
den	den	k1gInSc4
předtím	předtím	k6eAd1
obsadily	obsadit	k5eAaPmAgFnP
rakouské	rakouský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
Radom	Radom	k1gInSc1
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc6
kapitulovaly	kapitulovat	k5eAaBmAgFnP
před	před	k7c7
generálem	generál	k1gMnSc7
Fjodorem	Fjodor	k1gMnSc7
Děnisovem	Děnisov	k1gInSc7
zbytky	zbytek	k1gInPc1
polských	polský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
vedené	vedený	k2eAgFnPc1d1
Tomaszem	Tomasz	k1gMnSc7
Wawrzeckim	Wawrzeckim	k1gMnSc1
u	u	k7c2
Radoszyc	Radoszyc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povstání	povstání	k1gNnSc1
skončilo	skončit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Důsledky	důsledek	k1gInPc1
</s>
<s>
Mapa	mapa	k1gFnSc1
dělení	dělení	k1gNnSc2
Polska	Polsko	k1gNnSc2
</s>
<s>
Třetí	třetí	k4xOgNnSc4
dělení	dělení	k1gNnSc4
Polska	Polsko	k1gNnSc2
</s>
<s>
Bezprostředním	bezprostřední	k2eAgInSc7d1
důsledkem	důsledek	k1gInSc7
prohraného	prohraný	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
bylo	být	k5eAaImAgNnS
třetí	třetí	k4xOgFnSc4
a	a	k8xC
na	na	k7c4
123	#num#	k4
let	léto	k1gNnPc2
definitivní	definitivní	k2eAgNnSc4d1
rozdělení	rozdělení	k1gNnSc4
Polska	Polsko	k1gNnSc2
(	(	kIx(
<g/>
pomineme	pominout	k5eAaPmIp1nP
<g/>
-li	-li	k?
krátkou	krátký	k2eAgFnSc4d1
existenci	existence	k1gFnSc4
"	"	kIx"
<g/>
kvazisamostatného	kvazisamostatný	k2eAgNnSc2d1
<g/>
"	"	kIx"
Varšavského	varšavský	k2eAgNnSc2d1
knížectví	knížectví	k1gNnSc2
za	za	k7c2
napoleonských	napoleonský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězné	vítězný	k2eAgFnPc1d1
mocnosti	mocnost	k1gFnPc1
se	se	k3xPyFc4
rozhodly	rozhodnout	k5eAaPmAgFnP
<g/>
,	,	kIx,
že	že	k8xS
vymažou	vymazat	k5eAaPmIp3nP
toto	tento	k3xDgNnSc1
„	„	k?
<g/>
hnízdo	hnízdo	k1gNnSc1
nepokojů	nepokoj	k1gInPc2
<g/>
“	“	k?
z	z	k7c2
mapy	mapa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
1795	#num#	k4
se	se	k3xPyFc4
sešli	sejít	k5eAaPmAgMnP
diplomaté	diplomat	k1gMnPc1
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
Pruska	Prusko	k1gNnSc2
a	a	k8xC
Rakouska	Rakousko	k1gNnSc2
a	a	k8xC
dojednali	dojednat	k5eAaPmAgMnP
základní	základní	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
rozdělení	rozdělení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formální	formální	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
byla	být	k5eAaImAgFnS
podepsána	podepsat	k5eAaPmNgFnS
až	až	k6eAd1
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1797	#num#	k4
v	v	k7c6
Petrohradě	Petrohrad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko	Rusko	k1gNnSc1
opět	opět	k6eAd1
získalo	získat	k5eAaPmAgNnS
největší	veliký	k2eAgNnSc4d3
území	území	k1gNnSc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
si	se	k3xPyFc3
byly	být	k5eAaImAgFnP
rozdělené	rozdělený	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
téměř	téměř	k6eAd1
rovny	roven	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tentokrát	tentokrát	k6eAd1
ani	ani	k8xC
nebylo	být	k5eNaImAgNnS
třeba	třeba	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
si	se	k3xPyFc3
mocnosti	mocnost	k1gFnPc1
vynutily	vynutit	k5eAaPmAgFnP
souhlas	souhlas	k1gInSc4
s	s	k7c7
dělením	dělení	k1gNnSc7
na	na	k7c6
nějaké	nějaký	k3yIgFnSc6
loutkové	loutkový	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
<g/>
,	,	kIx,
jakákoliv	jakýkoliv	k3yIgFnSc1
polská	polský	k2eAgFnSc1d1
státnost	státnost	k1gFnSc1
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
politická	politický	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
již	již	k6eAd1
nebyla	být	k5eNaImAgFnS
více	hodně	k6eAd2
uznávána	uznáván	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polský	polský	k2eAgMnSc1d1
král	král	k1gMnSc1
Stanislaw	Stanislaw	k1gFnSc2
Poniatowski	Poniatowski	k1gNnPc2
byl	být	k5eAaImAgInS
donucen	donucen	k2eAgInSc1d1
abdikovat	abdikovat	k5eAaBmF
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
faktickým	faktický	k2eAgMnSc7d1
zajatcem	zajatec	k1gMnSc7
Kateřiny	Kateřina	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgInPc1
důsledky	důsledek	k1gInPc1
povstání	povstání	k1gNnSc2
a	a	k8xC
vliv	vliv	k1gInSc1
na	na	k7c4
další	další	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
</s>
<s>
Povstání	povstání	k1gNnSc1
představovalo	představovat	k5eAaImAgNnS
jeden	jeden	k4xCgInSc4
ze	z	k7c2
zásadních	zásadní	k2eAgInPc2d1
mezníků	mezník	k1gInPc2
ve	v	k7c6
vývoji	vývoj	k1gInSc6
k	k	k7c3
modernímu	moderní	k2eAgNnSc3d1
polskému	polský	k2eAgNnSc3d1
a	a	k8xC
částečně	částečně	k6eAd1
i	i	k8xC
litevskému	litevský	k2eAgInSc3d1
národu	národ	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
byl	být	k5eAaImAgInS
pojem	pojem	k1gInSc1
národ	národ	k1gInSc1
spojován	spojován	k2eAgInSc1d1
prakticky	prakticky	k6eAd1
výhradně	výhradně	k6eAd1
se	s	k7c7
šlechtou	šlechta	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
díky	díky	k7c3
povstání	povstání	k1gNnSc3
začali	začít	k5eAaPmAgMnP
být	být	k5eAaImF
za	za	k7c4
součást	součást	k1gFnSc4
národa	národ	k1gInSc2
považováni	považován	k2eAgMnPc1d1
i	i	k9
měšťané	měšťan	k1gMnPc1
<g/>
,	,	kIx,
městský	městský	k2eAgInSc1d1
plebs	plebs	k1gInSc1
a	a	k8xC
zčásti	zčásti	k6eAd1
i	i	k9
venkované	venkovan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
národ	národ	k1gInSc1
byl	být	k5eAaImAgInS
nadále	nadále	k6eAd1
vnímán	vnímat	k5eAaImNgInS
především	především	k6eAd1
státně	státně	k6eAd1
a	a	k8xC
nikoliv	nikoliv	k9
etnicky	etnicky	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mnoho	mnoho	k4c1
Poláků	Polák	k1gMnPc2
aktivních	aktivní	k2eAgMnPc2d1
za	za	k7c2
povstání	povstání	k1gNnSc2
odešlo	odejít	k5eAaPmAgNnS
do	do	k7c2
emigrace	emigrace	k1gFnSc2
a	a	k8xC
tvořilo	tvořit	k5eAaImAgNnS
páteř	páteř	k1gFnSc4
polského	polský	k2eAgInSc2d1
exilu	exil	k1gInSc2
na	na	k7c6
začátku	začátek	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
Kościuszko	Kościuszka	k1gFnSc5
byl	být	k5eAaImAgInS
propuštěn	propustit	k5eAaPmNgMnS
z	z	k7c2
vězení	vězení	k1gNnSc2
nově	nově	k6eAd1
nastoupivším	nastoupivší	k2eAgMnSc7d1
carem	car	k1gMnSc7
Pavlem	Pavel	k1gMnSc7
I.	I.	kA
v	v	k7c6
roce	rok	k1gInSc6
1796	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
složil	složit	k5eAaPmAgMnS
slib	slib	k1gInSc4
věrnosti	věrnost	k1gFnSc2
(	(	kIx(
<g/>
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
ale	ale	k8xC
nedodržel	dodržet	k5eNaPmAgMnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
dožil	dožít	k5eAaPmAgMnS
v	v	k7c6
emigraci	emigrace	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
<g/>
Uniwersał	Uniwersał	k1gFnSc1
Połaniecki	Połanieck	k1gFnSc2
<g/>
“	“	k?
měl	mít	k5eAaImAgMnS
zásadní	zásadní	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
rozvoj	rozvoj	k1gInSc4
polského	polský	k2eAgNnSc2d1
levicového	levicový	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
a	a	k8xC
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Navázání	navázání	k1gNnSc1
značných	značný	k2eAgFnPc2d1
pruských	pruský	k2eAgFnPc2d1
sil	síla	k1gFnPc2
v	v	k7c6
Polsku	Polsko	k1gNnSc6
pomohlo	pomoct	k5eAaPmAgNnS
Francouzům	Francouz	k1gMnPc3
odrazit	odrazit	k5eAaPmF
intervenční	intervenční	k2eAgFnPc4d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ekonomický	ekonomický	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
polských	polský	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
především	především	k6eAd1
těch	ten	k3xDgNnPc2
okupovaných	okupovaný	k2eAgNnPc2d1
Rusy	Rus	k1gMnPc4
byl	být	k5eAaImAgMnS
značně	značně	k6eAd1
zpomalen	zpomalit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
stávajících	stávající	k2eAgFnPc2d1
manufaktur	manufaktura	k1gFnPc2
byla	být	k5eAaImAgFnS
zavřena	zavřít	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
bank	banka	k1gFnPc2
zkrachovalo	zkrachovat	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouholeté	dlouholetý	k2eAgFnPc1d1
obchodní	obchodní	k2eAgFnPc1d1
vazby	vazba	k1gFnPc1
byly	být	k5eAaImAgFnP
zpřetrhány	zpřetrhat	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s>
Školský	školský	k2eAgInSc1d1
systém	systém	k1gInSc1
byl	být	k5eAaImAgInS
zrušen	zrušit	k5eAaPmNgInS
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
germanizaci	germanizace	k1gFnSc3
a	a	k8xC
rusifikaci	rusifikace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
to	ten	k3xDgNnSc1
umožnilo	umožnit	k5eAaPmAgNnS
rychlejší	rychlý	k2eAgNnSc1d2
šíření	šíření	k1gNnSc1
myšlenek	myšlenka	k1gFnPc2
osvícenství	osvícenství	k1gNnSc2
<g/>
,	,	kIx,
především	především	k9
z	z	k7c2
vyspělého	vyspělý	k2eAgInSc2d1
německého	německý	k2eAgInSc2d1
kulturního	kulturní	k2eAgInSc2d1
okruhu	okruh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
ruských	ruský	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
bylo	být	k5eAaImAgNnS
znovu	znovu	k6eAd1
zavedeno	zavést	k5eAaPmNgNnS
nevolnictví	nevolnictví	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rolníci	rolník	k1gMnPc1
byli	být	k5eAaImAgMnP
často	často	k6eAd1
bičováni	bičován	k2eAgMnPc1d1
jen	jen	k9
při	při	k7c6
zmínce	zmínka	k1gFnSc6
jména	jméno	k1gNnSc2
Kościuszko	Kościuszka	k1gFnSc5
<g/>
,	,	kIx,
či	či	k9wB
při	při	k7c6
jakýchkoliv	jakýkoliv	k3yIgFnPc6
stížnostech	stížnost	k1gFnPc6
na	na	k7c6
své	svůj	k3xOyFgFnSc6
povinnosti	povinnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Rusové	Rus	k1gMnPc1
zavedli	zavést	k5eAaPmAgMnP
rozsáhlé	rozsáhlý	k2eAgInPc4d1
odvody	odvod	k1gInPc4
do	do	k7c2
své	svůj	k3xOyFgFnSc2
armády	armáda	k1gFnSc2
především	především	k9
z	z	k7c2
okupovaných	okupovaný	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Šlechta	šlechta	k1gFnSc1
zapojená	zapojený	k2eAgFnSc1d1
do	do	k7c2
povstání	povstání	k1gNnSc2
přišla	přijít	k5eAaPmAgFnS
o	o	k7c4
svůj	svůj	k3xOyFgInSc4
majetek	majetek	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
připadl	připadnout	k5eAaPmAgMnS
státu	stát	k1gInSc3
nebo	nebo	k8xC
cizím	cizí	k2eAgMnPc3d1
šlechtickým	šlechtický	k2eAgMnPc3d1
rodům	rod	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
pravoslavné	pravoslavný	k2eAgMnPc4d1
poddané	poddaný	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
žili	žít	k5eAaImAgMnP
především	především	k6eAd1
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
<g/>
,	,	kIx,
mělo	mít	k5eAaImAgNnS
dělení	dělení	k1gNnSc1
jeden	jeden	k4xCgMnSc1
pozitivní	pozitivní	k2eAgInSc1d1
efekt	efekt	k1gInSc1
-	-	kIx~
přestali	přestat	k5eAaPmAgMnP
být	být	k5eAaImF
šikanováni	šikanovat	k5eAaImNgMnP
svými	svůj	k3xOyFgMnPc7
katolickými	katolický	k2eAgMnPc7d1
pány	pan	k1gMnPc7
kvůli	kvůli	k7c3
náboženství	náboženství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Kościuszko	Kościuszka	k1gFnSc5
uprising	uprising	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
,	,	kIx,
Battle	Battle	k1gFnSc1
of	of	k?
Praga	Praga	k1gFnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
,	,	kIx,
Insurekcja	Insurekcj	k2eAgNnPc4d1
kościuszkowska	kościuszkowsko	k1gNnPc4
na	na	k7c6
polské	polský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Bitwa	Bitwa	k1gFnSc1
pod	pod	k7c7
Maciejowicami	Maciejowica	k1gFnPc7
na	na	k7c6
polské	polský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
ŘEZNÍK	řezník	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stručná	stručný	k2eAgFnSc1d1
historie	historie	k1gFnSc1
států	stát	k1gInPc2
<g/>
:	:	kIx,
Polsko	Polsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
123	#num#	k4
<g/>
-	-	kIx~
<g/>
124	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Anglický	anglický	k2eAgInSc1d1
překlad	překlad	k1gInSc1
ústavy	ústava	k1gFnSc2
na	na	k7c6
Wikisource	Wikisourka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BAUER	Bauer	k1gMnSc1
<g/>
,	,	kIx,
Krzysztof	Krzysztof	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uchwalenie	Uchwalenie	k1gFnSc1
i	i	k8xC
obrona	obrona	k1gFnSc1
Konstytucji	Konstytucje	k1gFnSc4
3	#num#	k4
Maja	Maja	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Wydawnictwa	Wydawnictwa	k1gFnSc1
Szkolne	Szkoln	k1gInSc5
i	i	k9
Pedagogiczne	Pedagogiczne	k1gMnPc7
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
167	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
TRENCSÉNYI	TRENCSÉNYI	kA
<g/>
,	,	kIx,
Balázs	Balázs	k1gInSc1
<g/>
;	;	kIx,
KOPEČEK	Kopeček	k1gMnSc1
<g/>
,	,	kIx,
<g/>
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Late	lat	k1gInSc5
Enlightenment	Enlightenment	k1gInSc1
<g/>
:	:	kIx,
Emergence	emergence	k1gFnSc1
of	of	k?
modern	modern	k1gInSc1
´	´	k?
<g/>
national	nationat	k5eAaPmAgInS,k5eAaImAgInS
idea	idea	k1gFnSc1
<g/>
´	´	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Central	Central	k1gFnSc1
European	Europeany	k1gInPc2
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
284	#num#	k4
<g/>
-	-	kIx~
<g/>
285	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
STOROZYNSKY	STOROZYNSKY	kA
<g/>
,	,	kIx,
Alex	Alex	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Peasant	Peasanta	k1gFnPc2
Prince	princ	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
St.	st.	kA
Martin	Martin	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
171	#num#	k4
<g/>
-	-	kIx~
<g/>
172	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
STOROZYNSKY	STOROZYNSKY	kA
<g/>
,	,	kIx,
Alex	Alex	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Peasant	Peasanta	k1gFnPc2
Prince	princ	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
St.	st.	kA
Martin	Martin	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
177	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
STOROZYNSKY	STOROZYNSKY	kA
<g/>
,	,	kIx,
Alex	Alex	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Peasant	Peasanta	k1gFnPc2
Prince	princ	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
St.	st.	kA
Martin	Martin	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
182	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŘEZNÍK	řezník	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
naši	náš	k3xOp1gFnSc4
a	a	k8xC
vaši	váš	k3xOp2gFnSc4
svobodu	svoboda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
102	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
STOROZYNSKY	STOROZYNSKY	kA
<g/>
,	,	kIx,
Alex	Alex	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Peasant	Peasanta	k1gFnPc2
Prince	princ	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
St.	st.	kA
Martin	Martin	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
184	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŘEZNÍK	řezník	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
naši	náš	k3xOp1gFnSc4
a	a	k8xC
vaši	váš	k3xOp2gFnSc4
svobodu	svoboda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
114	#num#	k4
<g/>
-	-	kIx~
<g/>
119	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
STOROZYNSKY	STOROZYNSKY	kA
<g/>
,	,	kIx,
Alex	Alex	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Peasant	Peasanta	k1gFnPc2
Prince	princ	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
St.	st.	kA
Martin	Martin	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
190	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
STOROZYNSKY	STOROZYNSKY	kA
<g/>
,	,	kIx,
Alex	Alex	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Peasant	Peasanta	k1gFnPc2
Prince	princ	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
St.	st.	kA
Martin	Martin	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
205	#num#	k4
<g/>
-	-	kIx~
<g/>
213	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
ŘEZNÍK	řezník	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
naši	náš	k3xOp1gFnSc4
a	a	k8xC
vaši	váš	k3xOp2gFnSc4
svobodu	svoboda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
137	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
DUFFY	DUFFY	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Russia	Russia	k1gFnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Military	Militar	k1gInPc7
Way	Way	k1gFnSc2
to	ten	k3xDgNnSc1
the	the	k?
West	West	k1gInSc1
<g/>
:	:	kIx,
origins	origins	k1gInSc1
and	and	k?
nature	natur	k1gMnSc5
of	of	k?
russian	russian	k1gMnSc1
military	militara	k1gFnSc2
power	power	k1gInSc1
1700	#num#	k4
<g/>
-	-	kIx~
<g/>
1800	#num#	k4
<g/>
..	..	k?
London	London	k1gMnSc1
<g/>
:	:	kIx,
Routledge	Routledge	k1gInSc1
and	and	k?
Kegan	Kegan	k1gInSc1
Paul	Paul	k1gMnSc1
Ltd	ltd	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
113892475	#num#	k4
<g/>
X.	X.	kA
OCLC	OCLC	kA
975416227	#num#	k4
S.	S.	kA
196	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
znít	znít	k5eAaImF
„	„	k?
<g/>
Pražská	pražský	k2eAgFnSc1d1
řež	řež	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
ale	ale	k8xC
tím	ten	k3xDgNnSc7
by	by	kYmCp3nS
vzbuzoval	vzbuzovat	k5eAaImAgMnS
přílišné	přílišný	k2eAgFnPc4d1
konotace	konotace	k1gFnPc4
s	s	k7c7
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lib	Liba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
ru	ru	k?
<g/>
/	/	kIx~
<g/>
К	К	k?
<g/>
:	:	kIx,
Д	Д	k?
Д	Д	k?
В	В	k?
<g/>
.	.	kIx.
В	В	k?
с	с	k?
в	в	k?
С	С	k?
<g/>
.	.	kIx.
az	az	k?
<g/>
.	.	kIx.
<g/>
lib	líbit	k5eAaImRp2nS
<g/>
.	.	kIx.
<g/>
ru	ru	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ф	Ф	k?
Б	Б	k?
<g/>
.	.	kIx.
В	В	k?
<g/>
.	.	kIx.
elcocheingles	elcocheingles	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ŠVANKMAJER	ŠVANKMAJER	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kateřina	Kateřina	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lesk	lesk	k1gInSc4
a	a	k8xC
bída	bída	k1gFnSc1
impéria	impérium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
NLN	NLN	kA
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
184	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŘEZNÍK	řezník	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
naši	náš	k3xOp1gFnSc4
a	a	k8xC
vaši	váš	k3xOp2gFnSc4
svobodu	svoboda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
141	#num#	k4
<g/>
-	-	kIx~
<g/>
147	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
STOROZYNSKY	STOROZYNSKY	kA
<g/>
,	,	kIx,
Alex	Alex	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Peasant	Peasanta	k1gFnPc2
Prince	princ	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
St.	st.	kA
Martin	Martin	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
214	#num#	k4
<g/>
-	-	kIx~
<g/>
278	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
FRIEDL	FRIEDL	kA
J.	J.	kA
<g/>
;	;	kIx,
JUREK	Jurek	k1gMnSc1
<g/>
,	,	kIx,
T.	T.	kA
<g/>
;	;	kIx,
ŘEZNÍK	řezník	k1gMnSc1
M.	M.	kA
<g/>
;	;	kIx,
WIHODA	WIHODA	kA
<g/>
,	,	kIx,
M.	M.	kA
Dějiny	dějiny	k1gFnPc1
Polska	Polsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
NLN	NLN	kA
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
692	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
306	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HERBST	HERBST	kA
<g/>
,	,	kIx,
Stanislaw	Stanislaw	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tadeusz	Tadeusz	k1gInSc1
Kościuszko	Kościuszka	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Polski	Polsk	k1gFnPc1
Slownik	Slownik	k1gInSc4
Biograficzny	Biograficzna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wroclaw	Wroclaw	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
RAWSKI	RAWSKI	kA
<g/>
,	,	kIx,
Tadeusz	Tadeusz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojevůdcovské	vojevůdcovský	k2eAgNnSc1d1
umění	umění	k1gNnSc1
Tadeáše	Tadeáš	k1gMnSc2
Kościuszka	Kościuszka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Naše	náš	k3xOp1gNnSc1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ŘEZNÍK	řezník	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stručná	stručný	k2eAgFnSc1d1
historie	historie	k1gFnSc1
států	stát	k1gInPc2
<g/>
:	:	kIx,
Polsko	Polsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ŘEZNÍK	řezník	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
naši	náš	k3xOp1gFnSc4
a	a	k8xC
vaši	váš	k3xOp2gFnSc4
svobodu	svoboda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Století	století	k1gNnSc1
polských	polský	k2eAgNnPc2d1
povstání	povstání	k1gNnPc2
(	(	kIx(
<g/>
1794	#num#	k4
<g/>
-	-	kIx~
<g/>
1864	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
STOROZYNSKI	STOROZYNSKI	kA
<g/>
,	,	kIx,
Alex	Alex	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Peasant	Peasanta	k1gFnPc2
Prince	princ	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thaddeus	Thaddeus	k1gInSc1
Kosciuszko	Kosciuszka	k1gFnSc5
and	and	k?
the	the	k?
Age	Age	k1gFnSc2
of	of	k?
Revolution	Revolution	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
St.	st.	kA
Martin	Martin	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
ŠVANKMAJER	ŠVANKMAJER	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kateřina	Kateřina	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lesk	lesk	k1gInSc4
a	a	k8xC
bída	bída	k1gFnSc1
impéria	impérium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
NLN	NLN	kA
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KOSMAN	kosman	k1gMnSc1
<g/>
,	,	kIx,
Marceli	Marcel	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Polska	Polsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kościuszkovo	Kościuszkův	k2eAgNnSc1d1
povstání	povstání	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
81	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Insurekcja	Insurekcja	k1gFnSc1
Kościuszkowska	Kościuszkowska	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
III	III	kA
rozbiór	rozbiór	k1gInSc1
Polski	Polski	k1gNnSc1
(	(	kIx(
<g/>
video	video	k1gNnSc1
<g/>
,	,	kIx,
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Polsko	Polsko	k1gNnSc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
