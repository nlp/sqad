<s>
Nesta	Nesta	k1gMnSc1
Carter	Carter	k1gMnSc1
</s>
<s>
Nesta	Nesta	k1gMnSc1
Carter	Carter	k1gMnSc1
Nesta	Nesta	k1gMnSc1
Carter	Carter	k1gMnSc1
(	(	kIx(
<g/>
uprostřed	uprostřed	k6eAd1
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
Osobní	osobní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Narození	narození	k1gNnSc2
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1985	#num#	k4
(	(	kIx(
<g/>
35	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Stát	stát	k1gInSc1
</s>
<s>
Jamajka	Jamajka	k1gFnSc1
Jamajka	Jamajka	k1gFnSc1
Výška	výška	k1gFnSc1
</s>
<s>
178	#num#	k4
cm	cm	kA
Váha	váha	k1gFnSc1
</s>
<s>
70	#num#	k4
kg	kg	kA
Kariéra	kariéra	k1gFnSc1
Disciplína	disciplína	k1gFnSc1
</s>
<s>
100	#num#	k4
m	m	kA
<g/>
,	,	kIx,
200	#num#	k4
m	m	kA
Účasti	účast	k1gFnSc2
na	na	k7c4
LOH	LOH	kA
</s>
<s>
2008	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
Účasti	účast	k1gFnSc2
na	na	k7c4
MS	MS	kA
</s>
<s>
2007	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Atletika	atletika	k1gFnSc1
na	na	k7c4
LOH	LOH	kA
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
LOH	LOH	kA
2008	#num#	k4
</s>
<s>
štafeta	štafeta	k1gFnSc1
4	#num#	k4
×	×	k?
100	#num#	k4
m	m	kA
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
LOH	LOH	kA
2012	#num#	k4
</s>
<s>
štafeta	štafeta	k1gFnSc1
4	#num#	k4
×	×	k?
100	#num#	k4
m	m	kA
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
MS	MS	kA
2007	#num#	k4
</s>
<s>
štafeta	štafeta	k1gFnSc1
4	#num#	k4
×	×	k?
100	#num#	k4
m	m	kA
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MS	MS	kA
2011	#num#	k4
</s>
<s>
štafeta	štafeta	k1gFnSc1
4	#num#	k4
×	×	k?
100	#num#	k4
m	m	kA
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
MS	MS	kA
2013	#num#	k4
</s>
<s>
běh	běh	k1gInSc1
na	na	k7c4
100	#num#	k4
m	m	kA
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MS	MS	kA
2013	#num#	k4
</s>
<s>
štafeta	štafeta	k1gFnSc1
4	#num#	k4
×	×	k?
100	#num#	k4
m	m	kA
</s>
<s>
Halové	halový	k2eAgFnPc4d1
MS	MS	kA
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
HMS	HMS	kA
2012	#num#	k4
</s>
<s>
běh	běh	k1gInSc1
na	na	k7c4
60	#num#	k4
m	m	kA
</s>
<s>
Nesta	Nesta	k1gMnSc1
Carter	Carter	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1985	#num#	k4
<g/>
,	,	kIx,
Banana	Banana	k1gFnSc1
Ground	Ground	k1gInSc1
<g/>
,	,	kIx,
Manchester	Manchester	k1gInSc1
Parish	Parish	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jamajský	jamajský	k2eAgMnSc1d1
sportovec	sportovec	k1gMnSc1
<g/>
,	,	kIx,
atlet	atlet	k1gMnSc1
<g/>
,	,	kIx,
sprinter	sprinter	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
běhu	běh	k1gInSc2
na	na	k7c6
100	#num#	k4
a	a	k8xC
200	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Kariéra	kariéra	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
skončil	skončit	k5eAaPmAgInS
na	na	k7c6
MS	MS	kA
v	v	k7c6
atletice	atletika	k1gFnSc6
v	v	k7c6
japonské	japonský	k2eAgFnSc6d1
Ósace	Ósaka	k1gFnSc6
v	v	k7c6
běhu	běh	k1gInSc6
na	na	k7c4
100	#num#	k4
metrů	metr	k1gInPc2
v	v	k7c6
semifinále	semifinále	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
téhož	týž	k3xTgInSc2
šampionátu	šampionát	k1gInSc2
si	se	k3xPyFc3
odvezl	odvézt	k5eAaPmAgMnS
stříbrnou	stříbrný	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
vybojoval	vybojovat	k5eAaPmAgInS
ve	v	k7c6
štafetě	štafeta	k1gFnSc6
na	na	k7c4
4	#num#	k4
×	×	k?
100	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
doběhl	doběhnout	k5eAaPmAgMnS
druhý	druhý	k4xOgMnSc1
na	na	k7c6
světovém	světový	k2eAgNnSc6d1
atletickém	atletický	k2eAgNnSc6d1
finále	finále	k1gNnSc6
ve	v	k7c6
Stuttgartu	Stuttgart	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
se	se	k3xPyFc4
probojoval	probojovat	k5eAaPmAgMnS
na	na	k7c6
halovém	halový	k2eAgNnSc6d1
MS	MS	kA
v	v	k7c6
katarském	katarský	k2eAgInSc6d1
Dauhá	Dauhý	k2eAgFnSc1d1
do	do	k7c2
finále	finále	k1gNnSc2
běhu	běh	k1gInSc2
na	na	k7c4
60	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
obsadil	obsadit	k5eAaPmAgMnS
časem	čas	k1gInSc7
6,72	6,72	k4
s	s	k7c7
sedmé	sedmý	k4xOgNnSc4
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2010	#num#	k4
si	se	k3xPyFc3
vylepšil	vylepšit	k5eAaPmAgInS
na	na	k7c6
mítinku	mítink	k1gInSc6
v	v	k7c6
italském	italský	k2eAgNnSc6d1
Rieti	Rieti	k1gNnSc6
na	na	k7c6
stometrové	stometrový	k2eAgFnSc6d1
trati	trať	k1gFnSc6
hodnotu	hodnota	k1gFnSc4
osobního	osobní	k2eAgInSc2d1
rekordu	rekord	k1gInSc2
na	na	k7c4
9,78	9,78	k4
sekundy	sekunda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
časem	čas	k1gInSc7
vyrovnal	vyrovnat	k5eAaPmAgMnS,k5eAaBmAgMnS
nejlepší	dobrý	k2eAgInSc4d3
výkon	výkon	k1gInSc4
sezóny	sezóna	k1gFnSc2
Američana	Američan	k1gMnSc2
Tysona	Tyson	k1gMnSc2
Gaye	gay	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
tento	tento	k3xDgInSc1
čas	čas	k1gInSc1
zaběhl	zaběhnout	k5eAaPmAgInS
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
na	na	k7c6
Diamantové	diamantový	k2eAgFnSc6d1
lize	liga	k1gFnSc6
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
LOH	LOH	kA
2008	#num#	k4
</s>
<s>
Na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2008	#num#	k4
v	v	k7c6
Pekingu	Peking	k1gInSc6
byl	být	k5eAaImAgInS
ve	v	k7c6
finále	finála	k1gFnSc6
členem	člen	k1gInSc7
štafety	štafeta	k1gFnSc2
na	na	k7c4
4	#num#	k4
×	×	k?
100	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
zaběhla	zaběhnout	k5eAaPmAgFnS
původní	původní	k2eAgInSc4d1
světový	světový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc4
hodnota	hodnota	k1gFnSc1
byla	být	k5eAaImAgFnS
37,10	37,10	k4
s.	s.	k?
Carter	Carter	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
závod	závod	k1gInSc4
rozbíhal	rozbíhat	k5eAaImAgMnS
předával	předávat	k5eAaImAgMnS
štafetový	štafetový	k2eAgInSc4d1
kolík	kolík	k1gInSc4
Michaelu	Michael	k1gMnSc3
Fraterovi	Frater	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgInSc1
úsek	úsek	k1gInSc1
běžel	běžet	k5eAaImAgInS
Usain	Usain	k2eAgInSc4d1
Bolt	Bolt	k1gInSc4
a	a	k8xC
finišmanem	finišman	k1gMnSc7
byl	být	k5eAaImAgMnS
Asafa	Asaf	k1gMnSc4
Powell	Powell	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
LOH	LOH	kA
2012	#num#	k4
</s>
<s>
Na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2012	#num#	k4
v	v	k7c6
Londýně	Londýn	k1gInSc6
byl	být	k5eAaImAgInS
ve	v	k7c6
finále	finála	k1gFnSc6
členem	člen	k1gInSc7
štafety	štafeta	k1gFnSc2
na	na	k7c4
4	#num#	k4
×	×	k?
100	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zaběhla	zaběhnout	k5eAaPmAgFnS
nový	nový	k2eAgInSc4d1
světový	světový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc4
hodnota	hodnota	k1gFnSc1
je	být	k5eAaImIp3nS
36,84	36,84	k4
s.	s.	k?
Carter	Carter	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
závod	závod	k1gInSc4
rozbíhal	rozbíhat	k5eAaImAgMnS
předával	předávat	k5eAaImAgMnS
štafetový	štafetový	k2eAgInSc4d1
kolík	kolík	k1gInSc4
Michaelu	Michael	k1gMnSc3
Fraterovi	Frater	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgInSc4
úsek	úsek	k1gInSc4
běžel	běžet	k5eAaImAgMnS
Yohan	Yohan	k1gMnSc1
Blake	Blak	k1gFnSc2
a	a	k8xC
finišmanem	finišman	k1gMnSc7
byl	být	k5eAaImAgInS
Usain	Usain	k2eAgInSc1d1
Bolt	Bolt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Osobní	osobní	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
</s>
<s>
100	#num#	k4
m	m	kA
–	–	k?
9,78	9,78	k4
s.	s.	k?
(	(	kIx(
<g/>
Rieti	Rieti	k1gNnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
200	#num#	k4
m	m	kA
–	–	k?
20,31	20,31	k4
s.	s.	k?
(	(	kIx(
<g/>
Kingston	Kingston	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
×	×	k?
100	#num#	k4
m	m	kA
–	–	k?
36,84	36,84	k4
s	s	k7c7
(	(	kIx(
<g/>
SR	SR	kA
<g/>
,	,	kIx,
LOH	LOH	kA
2012	#num#	k4
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Profil	profil	k1gInSc1
na	na	k7c6
sports-reference	sports-referenka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
com	com	k?
Profil	profil	k1gInSc1
na	na	k7c6
sports-reference	sports-referenka	k1gFnSc6
<g/>
.	.	kIx.
www.sports-reference.com	www.sports-reference.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Rudisha	Rudisha	k1gMnSc1
po	po	k7c6
týdnu	týden	k1gInSc6
opět	opět	k6eAd1
zaběhl	zaběhnout	k5eAaPmAgMnS
světový	světový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
na	na	k7c4
800	#num#	k4
metrů	metr	k1gInPc2
<g/>
↑	↑	k?
Běh	běh	k1gInSc1
na	na	k7c4
100	#num#	k4
m	m	kA
–	–	k?
muži	muž	k1gMnPc1
toplists	toplists	k6eAd1
2010	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Nesta	Nest	k1gInSc2
Carter	Cartra	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Nesta	Nesta	k1gFnSc1
Carter	Cartra	k1gFnPc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
IAAF	IAAF	kA
</s>
<s>
Nesta	Nesta	k1gMnSc1
Carter	Carter	k1gMnSc1
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Profil	profil	k1gInSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
tilastopaja	tilastopaj	k1gInSc2
<g/>
.	.	kIx.
<g/>
net	net	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Olympijští	olympijský	k2eAgMnPc1d1
vítězové	vítěz	k1gMnPc1
v	v	k7c6
běhu	běh	k1gInSc6
štafeta	štafeta	k1gFnSc1
na	na	k7c4
4	#num#	k4
x	x	k?
100	#num#	k4
m	m	kA
</s>
<s>
1912	#num#	k4
–	–	k?
19601964	#num#	k4
–	–	k?
20002004	#num#	k4
–	–	k?
</s>
<s>
1912	#num#	k4
<g/>
:	:	kIx,
GBR	GBR	kA
–	–	k?
Jacobs	Jacobs	k1gInSc1
•	•	k?
Macintosh	Macintosh	kA
•	•	k?
d	d	k?
<g/>
'	'	kIx"
<g/>
Arcy	Arca	k1gMnSc2
•	•	k?
Applegarth	Applegarth	k1gMnSc1
</s>
<s>
1920	#num#	k4
<g/>
:	:	kIx,
USA	USA	kA
–	–	k?
Paddock	Paddock	k1gMnSc1
•	•	k?
Scholz	Scholz	k1gMnSc1
•	•	k?
Murchison	Murchison	k1gMnSc1
•	•	k?
Kirksey	Kirksea	k1gFnSc2
</s>
<s>
1924	#num#	k4
<g/>
:	:	kIx,
USA	USA	kA
–	–	k?
Murchison	Murchison	k1gInSc1
•	•	k?
Clarke	Clark	k1gMnSc2
•	•	k?
Hussey	Hussea	k1gMnSc2
•	•	k?
LeConey	LeConea	k1gMnSc2
</s>
<s>
1928	#num#	k4
<g/>
:	:	kIx,
USA	USA	kA
–	–	k?
Wykoff	Wykoff	k1gMnSc1
•	•	k?
Quinn	Quinn	k1gMnSc1
•	•	k?
Borah	Borah	k1gMnSc1
•	•	k?
Russell	Russell	k1gMnSc1
</s>
<s>
1932	#num#	k4
<g/>
:	:	kIx,
USA	USA	kA
–	–	k?
Kiesel	Kiesel	k1gMnSc1
•	•	k?
Dyer	Dyer	k1gMnSc1
•	•	k?
Toppino	Toppin	k2eAgNnSc1d1
•	•	k?
Wykoff	Wykoff	k1gInSc4
</s>
<s>
1936	#num#	k4
<g/>
:	:	kIx,
USA	USA	kA
–	–	k?
Owens	Owens	k1gInSc1
•	•	k?
Metcalfe	Metcalf	k1gInSc5
•	•	k?
Draper	Draper	k1gMnSc1
•	•	k?
Wykoff	Wykoff	k1gMnSc1
</s>
<s>
1948	#num#	k4
<g/>
:	:	kIx,
USA	USA	kA
–	–	k?
Ewell	Ewell	k1gMnSc1
•	•	k?
Wright	Wright	k1gMnSc1
•	•	k?
Dillard	Dillard	k1gMnSc1
•	•	k?
Patton	Patton	k1gInSc1
</s>
<s>
1952	#num#	k4
<g/>
:	:	kIx,
USA	USA	kA
–	–	k?
Smith	Smith	k1gMnSc1
•	•	k?
Dillard	Dillard	k1gMnSc1
•	•	k?
Remigino	Remigino	k1gNnSc4
•	•	k?
Stanfield	Stanfield	k1gInSc1
</s>
<s>
1956	#num#	k4
<g/>
:	:	kIx,
USA	USA	kA
–	–	k?
Baker	Baker	k1gMnSc1
•	•	k?
King	King	k1gMnSc1
•	•	k?
Morrow	Morrow	k1gMnSc1
•	•	k?
Murchison	Murchison	k1gMnSc1
</s>
<s>
1960	#num#	k4
<g/>
:	:	kIx,
EUA	EUA	kA
–	–	k?
Cullmann	Cullmann	k1gInSc1
•	•	k?
Hary	Hara	k1gFnSc2
•	•	k?
Mahlendorf	Mahlendorf	k1gMnSc1
•	•	k?
Lauer	Lauer	k1gMnSc1
</s>
<s>
1964	#num#	k4
<g/>
:	:	kIx,
USA	USA	kA
–	–	k?
Drayton	Drayton	k1gInSc1
•	•	k?
Ashworth	Ashworth	k1gInSc1
•	•	k?
Stebbins	Stebbins	k1gInSc1
•	•	k?
Hayes	Hayes	k1gInSc1
</s>
<s>
1968	#num#	k4
<g/>
:	:	kIx,
USA	USA	kA
–	–	k?
Greene	Green	k1gInSc5
•	•	k?
Pender	Pender	k1gMnSc1
•	•	k?
Smith	Smith	k1gMnSc1
•	•	k?
Hines	Hines	k1gMnSc1
</s>
<s>
1972	#num#	k4
<g/>
:	:	kIx,
USA	USA	kA
–	–	k?
Black	Black	k1gMnSc1
•	•	k?
Taylor	Taylor	k1gMnSc1
•	•	k?
Tinker	Tinker	k1gMnSc1
•	•	k?
Hart	Hart	k1gMnSc1
</s>
<s>
1976	#num#	k4
<g/>
:	:	kIx,
USA	USA	kA
–	–	k?
Glance	Glance	k?
•	•	k?
Jones	Jones	k1gMnSc1
•	•	k?
Hampton	Hampton	k1gInSc1
•	•	k?
Riddick	Riddick	k1gInSc1
</s>
<s>
1980	#num#	k4
<g/>
:	:	kIx,
URS	URS	kA
–	–	k?
Muravjov	Muravjov	k1gInSc1
•	•	k?
Sidorov	Sidorov	k1gInSc1
•	•	k?
Aksinin	Aksinin	k2eAgInSc1d1
•	•	k?
Prokofjev	Prokofjev	k1gFnSc7
</s>
<s>
1984	#num#	k4
<g/>
:	:	kIx,
USA	USA	kA
–	–	k?
Graddy	Gradda	k1gFnSc2
•	•	k?
Brown	Brown	k1gMnSc1
•	•	k?
Smith	Smith	k1gMnSc1
•	•	k?
Lewis	Lewis	k1gInSc1
</s>
<s>
1988	#num#	k4
<g/>
:	:	kIx,
URS	URS	kA
–	–	k?
Bryzgin	Bryzgin	k1gInSc1
•	•	k?
Krylov	Krylov	k1gInSc1
•	•	k?
Muravjov	Muravjov	k1gInSc1
•	•	k?
Savin	Savin	k1gInSc1
</s>
<s>
1992	#num#	k4
<g/>
:	:	kIx,
USA	USA	kA
–	–	k?
Marsh	Marsh	k1gMnSc1
•	•	k?
Burrell	Burrell	k1gMnSc1
•	•	k?
Mitchell	Mitchell	k1gMnSc1
•	•	k?
Lewis	Lewis	k1gInSc1
•	•	k?
Jett	Jett	k1gInSc1
(	(	kIx(
<g/>
rozběh	rozběh	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1996	#num#	k4
<g/>
:	:	kIx,
CAN	CAN	kA
–	–	k?
Esmie	Esmie	k1gFnSc2
•	•	k?
Gilbert	Gilbert	k1gMnSc1
•	•	k?
Surin	Surin	k1gMnSc1
•	•	k?
Bailey	Bailea	k1gFnSc2
•	•	k?
Chambers	Chambers	k1gInSc1
(	(	kIx(
<g/>
rozběh	rozběh	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2000	#num#	k4
<g/>
:	:	kIx,
USA	USA	kA
–	–	k?
Drummond	Drummond	k1gInSc1
•	•	k?
Williams	Williams	k1gInSc1
•	•	k?
Lewis	Lewis	k1gInSc1
•	•	k?
Greene	Green	k1gInSc5
•	•	k?
Montgomery	Montgomera	k1gFnPc1
(	(	kIx(
<g/>
roz	roz	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
•	•	k?
Brokenburr	Brokenburr	k1gInSc1
(	(	kIx(
<g/>
roz	roz	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
2004	#num#	k4
<g/>
:	:	kIx,
GBR	GBR	kA
–	–	k?
Gardener	Gardener	k1gMnSc1
•	•	k?
Campbell	Campbell	k1gMnSc1
•	•	k?
Devonish	Devonish	k1gMnSc1
•	•	k?
Francis	Francis	k1gInSc1
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
JAM	jáma	k1gFnPc2
–	–	k?
Carter	Carter	k1gMnSc1
•	•	k?
Frater	Frater	k1gMnSc1
•	•	k?
Bolt	Bolt	k1gMnSc1
•	•	k?
Powell	Powell	k1gMnSc1
</s>
<s>
2012	#num#	k4
<g/>
:	:	kIx,
JAM	jam	k1gInSc1
–	–	k?
Carter	Carter	k1gInSc1
•	•	k?
Frater	Frater	k1gInSc1
•	•	k?
Blake	Blake	k1gInSc1
•	•	k?
Bolt	Bolt	k1gInSc1
</s>
<s>
2016	#num#	k4
<g/>
:	:	kIx,
JAM	jáma	k1gFnPc2
–	–	k?
Powell	Powell	k1gMnSc1
•	•	k?
Blake	Blake	k1gInSc1
•	•	k?
Ashmeade	Ashmead	k1gInSc5
•	•	k?
Bolt	Bolta	k1gFnPc2
</s>
<s>
Mistři	mistr	k1gMnPc1
světa	svět	k1gInSc2
v	v	k7c6
běhu	běh	k1gInSc6
štafeta	štafeta	k1gFnSc1
4	#num#	k4
x	x	k?
100	#num#	k4
m	m	kA
</s>
<s>
1983	#num#	k4
–	–	k?
19992001	#num#	k4
–	–	k?
20162015	#num#	k4
–	–	k?
</s>
<s>
1983	#num#	k4
<g/>
:	:	kIx,
King	King	k1gMnSc1
•	•	k?
Gault	Gault	k1gMnSc1
•	•	k?
Smith	Smith	k1gMnSc1
•	•	k?
Lewis	Lewis	k1gInSc1
</s>
<s>
1987	#num#	k4
<g/>
:	:	kIx,
McRae	McRa	k1gInSc2
•	•	k?
McNeill	McNeill	k1gMnSc1
•	•	k?
Glance	Glance	k?
•	•	k?
Lewis	Lewis	k1gInSc1
</s>
<s>
1991	#num#	k4
<g/>
:	:	kIx,
Cason	Cason	k1gMnSc1
•	•	k?
Burrell	Burrell	k1gMnSc1
•	•	k?
Mitchell	Mitchell	k1gMnSc1
•	•	k?
Lewis	Lewis	k1gInSc1
</s>
<s>
1993	#num#	k4
<g/>
:	:	kIx,
Drummond	Drummond	k1gMnSc1
•	•	k?
Cason	Cason	k1gMnSc1
•	•	k?
Mitchell	Mitchell	k1gMnSc1
•	•	k?
Burrell	Burrell	k1gMnSc1
</s>
<s>
1995	#num#	k4
<g/>
:	:	kIx,
Esmie	Esmie	k1gFnSc2
•	•	k?
Gilbert	Gilbert	k1gMnSc1
•	•	k?
Surin	Surin	k1gMnSc1
•	•	k?
Bailey	Bailea	k1gFnSc2
</s>
<s>
1997	#num#	k4
<g/>
:	:	kIx,
Esmie	Esmie	k1gFnSc2
•	•	k?
Gilbert	Gilbert	k1gMnSc1
•	•	k?
Surin	Surin	k1gMnSc1
•	•	k?
Bailey	Bailea	k1gFnSc2
</s>
<s>
1999	#num#	k4
<g/>
:	:	kIx,
Drummond	Drummond	k1gInSc1
•	•	k?
Montgomery	Montgomera	k1gFnSc2
•	•	k?
Lewis	Lewis	k1gFnSc2
•	•	k?
Greene	Green	k1gInSc5
</s>
<s>
2001	#num#	k4
<g/>
:	:	kIx,
Nagel	Nagel	k1gInSc1
•	•	k?
Du	Du	k?
Plessis	Plessis	k1gFnSc2
•	•	k?
Newton	Newton	k1gMnSc1
•	•	k?
Quinn	Quinn	k1gMnSc1
</s>
<s>
2003	#num#	k4
<g/>
:	:	kIx,
Capel	Capel	k1gInSc1
•	•	k?
Williams	Williams	k1gInSc1
•	•	k?
Patton	Patton	k1gInSc1
•	•	k?
Johnson	Johnson	k1gMnSc1
</s>
<s>
2005	#num#	k4
<g/>
:	:	kIx,
Ducouré	Ducourý	k2eAgFnSc2d1
•	•	k?
Pognon	Pognon	k1gMnSc1
•	•	k?
de	de	k?
Lépine	Lépin	k1gInSc5
•	•	k?
Dovy	Dov	k2eAgFnPc4d1
</s>
<s>
2007	#num#	k4
<g/>
:	:	kIx,
Patton	Patton	k1gInSc1
•	•	k?
Spearmon	Spearmon	k1gMnSc1
•	•	k?
Gay	gay	k1gMnSc1
•	•	k?
Dixon	Dixon	k1gMnSc1
</s>
<s>
2009	#num#	k4
<g/>
:	:	kIx,
Mullings	Mullingsa	k1gFnPc2
•	•	k?
Frater	Frater	k1gMnSc1
•	•	k?
Bolt	Bolt	k1gMnSc1
•	•	k?
Powell	Powell	k1gMnSc1
</s>
<s>
2011	#num#	k4
<g/>
:	:	kIx,
Carter	Carter	k1gInSc1
•	•	k?
Frater	Frater	k1gInSc1
•	•	k?
Blake	Blake	k1gInSc1
•	•	k?
Bolt	Bolt	k1gInSc1
</s>
<s>
2013	#num#	k4
<g/>
:	:	kIx,
Carter	Carter	k1gInSc1
•	•	k?
Bailey-Cole	Bailey-Cole	k1gFnSc2
•	•	k?
Ashmeade	Ashmead	k1gInSc5
•	•	k?
Bolt	Bolta	k1gFnPc2
</s>
<s>
2015	#num#	k4
<g/>
:	:	kIx,
Carter	Carter	k1gMnSc1
•	•	k?
Powell	Powell	k1gMnSc1
•	•	k?
Ashmeade	Ashmead	k1gInSc5
•	•	k?
Bolt	Bolt	k2eAgInSc1d1
</s>
<s>
2017	#num#	k4
<g/>
:	:	kIx,
Ujah	Ujah	k1gMnSc1
•	•	k?
Gemili	Gemili	k1gMnSc1
•	•	k?
Talbot	Talbot	k1gMnSc1
•	•	k?
Mitchell-Blake	Mitchell-Blake	k1gInSc1
</s>
<s>
2019	#num#	k4
<g/>
:	:	kIx,
Coleman	Coleman	k1gMnSc1
•	•	k?
Gatlin	Gatlin	k1gInSc1
•	•	k?
Rodgers	Rodgers	k1gInSc1
•	•	k?
Lyles	Lyles	k1gInSc1
•	•	k?
Gillespie	Gillespie	k1gFnSc2
<g/>
*	*	kIx~
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
