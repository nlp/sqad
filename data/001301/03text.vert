<s>
Bismut	bismut	k1gInSc1	bismut
(	(	kIx(	(
<g/>
vizmut	vizmut	k1gInSc1	vizmut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Bi	Bi	k1gFnSc1	Bi
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Bismuthum	Bismuthum	k1gInSc1	Bismuthum
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
těžké	těžký	k2eAgInPc4d1	těžký
kovy	kov	k1gInPc4	kov
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
lidstvu	lidstvo	k1gNnSc3	lidstvo
již	již	k9	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
různých	různý	k2eAgFnPc2d1	různá
slitin	slitina	k1gFnPc2	slitina
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
barviv	barvivo	k1gNnPc2	barvivo
a	a	k8xC	a
keramických	keramický	k2eAgInPc2d1	keramický
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
těžký	těžký	k2eAgInSc1d1	těžký
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
se	s	k7c7	s
slabým	slabý	k2eAgInSc7d1	slabý
růžovým	růžový	k2eAgInSc7d1	růžový
leskem	lesk	k1gInSc7	lesk
<g/>
,	,	kIx,	,
křehký	křehký	k2eAgMnSc1d1	křehký
a	a	k8xC	a
hrubě	hrubě	k6eAd1	hrubě
krystalický	krystalický	k2eAgMnSc1d1	krystalický
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plynném	plynný	k2eAgInSc6d1	plynný
stavu	stav	k1gInSc6	stav
tvoří	tvořit	k5eAaImIp3nS	tvořit
bismut	bismut	k1gInSc1	bismut
molekuly	molekula	k1gFnSc2	molekula
Bi	Bi	k1gFnSc2	Bi
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgInPc2d1	ostatní
těžkých	těžký	k2eAgInPc2d1	těžký
kovů	kov	k1gInPc2	kov
nejsou	být	k5eNaImIp3nP	být
jeho	jeho	k3xOp3gNnSc1	jeho
sloučeniny	sloučenina	k1gFnPc1	sloučenina
toxické	toxický	k2eAgFnPc1d1	toxická
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
v	v	k7c6	v
mocenství	mocenství	k1gNnSc4	mocenství
Bi	Bi	k1gFnSc2	Bi
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
a	a	k8xC	a
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
jako	jako	k8xC	jako
Bi	Bi	k1gFnSc1	Bi
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Bismut	bismut	k1gInSc1	bismut
se	se	k3xPyFc4	se
nerozpouští	rozpouštět	k5eNaImIp3nS	rozpouštět
v	v	k7c6	v
neoxidujících	oxidující	k2eNgFnPc6d1	oxidující
kyselinách	kyselina	k1gFnPc6	kyselina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ušlechtilý	ušlechtilý	k2eAgInSc1d1	ušlechtilý
prvek	prvek	k1gInSc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
však	však	k9	však
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
především	především	k9	především
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc6d1	chlorovodíková
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
i	i	k8xC	i
malých	malý	k2eAgFnPc2d1	malá
množství	množství	k1gNnSc4	množství
oxidačních	oxidační	k2eAgNnPc2d1	oxidační
činidel	činidlo	k1gNnPc2	činidlo
(	(	kIx(	(
<g/>
HNO	HNO	kA	HNO
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bismut	bismut	k1gInSc1	bismut
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
za	za	k7c2	za
laboratorní	laboratorní	k2eAgFnSc2d1	laboratorní
teploty	teplota	k1gFnSc2	teplota
stálý	stálý	k2eAgInSc1d1	stálý
<g/>
,	,	kIx,	,
v	v	k7c6	v
červeném	červený	k2eAgInSc6d1	červený
žáru	žár	k1gInSc6	žár
shoří	shořet	k5eAaPmIp3nS	shořet
namodralým	namodralý	k2eAgInSc7d1	namodralý
plamenem	plamen	k1gInSc7	plamen
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
bismutitý	bismutitý	k2eAgInSc4d1	bismutitý
Bi	Bi	k1gFnSc7	Bi
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
za	za	k7c2	za
žáru	žár	k1gInSc2	žár
se	se	k3xPyFc4	se
bismut	bismut	k1gInSc1	bismut
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Bismut	bismut	k1gInSc1	bismut
tvoří	tvořit	k5eAaImIp3nS	tvořit
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
kovů	kov	k1gInPc2	kov
slitiny	slitina	k1gFnSc2	slitina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
nízké	nízký	k2eAgFnPc4d1	nízká
teploty	teplota	k1gFnPc4	teplota
tání	tání	k1gNnSc2	tání
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
bismut	bismut	k1gInSc1	bismut
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgFnSc4d3	veliký
diamagnetickou	diamagnetický	k2eAgFnSc4d1	diamagnetická
konstantu	konstanta	k1gFnSc4	konstanta
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
známých	známý	k2eAgInPc2d1	známý
kovových	kovový	k2eAgInPc2d1	kovový
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
vysoký	vysoký	k2eAgInSc4d1	vysoký
elektrický	elektrický	k2eAgInSc4d1	elektrický
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
hodnotu	hodnota	k1gFnSc4	hodnota
Hallovy	Hallův	k2eAgFnSc2d1	Hallova
konstanty	konstanta	k1gFnSc2	konstanta
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
rtutí	rtuť	k1gFnSc7	rtuť
má	mít	k5eAaImIp3nS	mít
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
vodivost	vodivost	k1gFnSc4	vodivost
ze	z	k7c2	z
známých	známý	k2eAgInPc2d1	známý
kovových	kovový	k2eAgInPc2d1	kovový
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
byl	být	k5eAaImAgInS	být
bismut	bismut	k1gInSc1	bismut
209	[number]	k4	209
<g/>
Bi	Bi	k1gFnPc2	Bi
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
stabilní	stabilní	k2eAgInSc4d1	stabilní
<g/>
,	,	kIx,	,
neměnný	neměnný	k2eAgInSc4d1	neměnný
izotop	izotop	k1gInSc4	izotop
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
to	ten	k3xDgNnSc1	ten
odporovalo	odporovat	k5eAaImAgNnS	odporovat
teoretickým	teoretický	k2eAgInPc3d1	teoretický
výpočtům	výpočet	k1gInPc3	výpočet
stability	stabilita	k1gFnSc2	stabilita
atomových	atomový	k2eAgNnPc2d1	atomové
jader	jádro	k1gNnPc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
francouzskými	francouzský	k2eAgMnPc7d1	francouzský
atomovými	atomový	k2eAgMnPc7d1	atomový
fyziky	fyzik	k1gMnPc7	fyzik
ve	v	k7c6	v
Výzkumném	výzkumný	k2eAgInSc6d1	výzkumný
institutu	institut	k1gInSc6	institut
astrofyziky	astrofyzika	k1gFnSc2	astrofyzika
v	v	k7c4	v
Orsay	Orsa	k1gMnPc4	Orsa
podán	podán	k2eAgInSc4d1	podán
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
bismut	bismut	k1gInSc1	bismut
podléhá	podléhat	k5eAaImIp3nS	podléhat
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
přeměně	přeměna	k1gFnSc3	přeměna
<g/>
.	.	kIx.	.
</s>
<s>
Bismut	bismut	k1gInSc1	bismut
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nestabilním	stabilní	k2eNgInSc7d1	nestabilní
prvkem	prvek	k1gInSc7	prvek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
podléhá	podléhat	k5eAaImIp3nS	podléhat
alfa	alfa	k1gNnSc4	alfa
rozpadu	rozpad	k1gInSc2	rozpad
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
1019	[number]	k4	1019
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
činí	činit	k5eAaImIp3nS	činit
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejpomaleji	pomale	k6eAd3	pomale
se	se	k3xPyFc4	se
přeměňujících	přeměňující	k2eAgInPc2d1	přeměňující
přirozených	přirozený	k2eAgInPc2d1	přirozený
radioizotopů	radioizotop	k1gInPc2	radioizotop
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
bismutu	bismut	k1gInSc6	bismut
jako	jako	k8xS	jako
o	o	k7c6	o
kovu	kov	k1gInSc6	kov
podobném	podobný	k2eAgInSc6d1	podobný
cínu	cín	k1gInSc6	cín
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
Baselius	Baselius	k1gMnSc1	Baselius
Valentinus	Valentinus	k1gMnSc1	Valentinus
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
určili	určit	k5eAaPmAgMnP	určit
Johann	Johann	k1gMnSc1	Johann
Heinrich	Heinrich	k1gMnSc1	Heinrich
Pott	Pott	k1gMnSc1	Pott
a	a	k8xC	a
Torbern	Torbern	k1gMnSc1	Torbern
Olof	Olof	k1gMnSc1	Olof
Bergman	Bergman	k1gMnSc1	Bergman
bismut	bismut	k1gInSc1	bismut
jako	jako	k8xS	jako
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
prvek	prvek	k1gInSc1	prvek
kovového	kovový	k2eAgInSc2d1	kovový
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Bismut	bismut	k1gInSc1	bismut
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
velmi	velmi	k6eAd1	velmi
vzácným	vzácný	k2eAgInSc7d1	vzácný
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
činí	činit	k5eAaImIp3nS	činit
pouze	pouze	k6eAd1	pouze
0,2	[number]	k4	0,2
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
parts	partsa	k1gFnPc2	partsa
per	pero	k1gNnPc2	pero
milion	milion	k4xCgInSc1	milion
<g/>
;	;	kIx,	;
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
činí	činit	k5eAaImIp3nS	činit
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
pouze	pouze	k6eAd1	pouze
0,017	[number]	k4	0,017
mikrogramu	mikrogram	k1gInSc2	mikrogram
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
bismutu	bismut	k1gInSc2	bismut
přibližně	přibližně	k6eAd1	přibližně
300	[number]	k4	300
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Bismut	bismut	k1gInSc1	bismut
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ryzí	ryzí	k2eAgFnSc1d1	ryzí
i	i	k9	i
ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
minerály	minerál	k1gInPc7	minerál
bismutu	bismut	k1gInSc2	bismut
jsou	být	k5eAaImIp3nP	být
bismutinit	bismutinit	k5eAaPmF	bismutinit
(	(	kIx(	(
<g/>
psáno	psát	k5eAaImNgNnS	psát
též	též	k9	též
bismutin	bismutina	k1gFnPc2	bismutina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
sulfid	sulfid	k1gInSc4	sulfid
bismutitý	bismutitý	k2eAgInSc4d1	bismutitý
Bi	Bi	k1gFnSc7	Bi
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
bismit	bismit	k1gInSc1	bismit
jako	jako	k8xC	jako
oxid	oxid	k1gInSc1	oxid
Bi	Bi	k1gFnSc2	Bi
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
a	a	k8xC	a
zásaditý	zásaditý	k2eAgInSc1d1	zásaditý
uhličitan	uhličitan	k1gInSc1	uhličitan
bismutit	bismutit	k5eAaPmF	bismutit
[	[	kIx(	[
<g/>
(	(	kIx(	(
<g/>
BiO	BiO	k?	BiO
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
CO	co	k8xS	co
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
příměs	příměs	k1gFnSc1	příměs
v	v	k7c6	v
rudách	ruda	k1gFnPc6	ruda
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
rudy	ruda	k1gFnPc1	ruda
bismutu	bismut	k1gInSc2	bismut
nalézají	nalézat	k5eAaImIp3nP	nalézat
v	v	k7c6	v
Českosaském	českosaský	k2eAgNnSc6d1	Českosaské
Rudohoří	Rudohoří	k1gNnSc6	Rudohoří
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
bismutu	bismut	k1gInSc2	bismut
těží	těžet	k5eAaImIp3nS	těžet
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
a	a	k8xC	a
Bolívie	Bolívie	k1gFnSc1	Bolívie
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
bolivijská	bolivijský	k2eAgNnPc1d1	bolivijské
ložiska	ložisko	k1gNnPc1	ložisko
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
jako	jako	k9	jako
primární	primární	k2eAgFnPc1d1	primární
rudy	ruda	k1gFnPc1	ruda
minerály	minerál	k1gInPc4	minerál
bismutu	bismut	k1gInSc2	bismut
<g/>
,	,	kIx,	,
v	v	k7c6	v
ostatních	ostatní	k2eAgNnPc6d1	ostatní
nalezištích	naleziště	k1gNnPc6	naleziště
tvoří	tvořit	k5eAaImIp3nS	tvořit
bismut	bismut	k1gInSc1	bismut
pouze	pouze	k6eAd1	pouze
příměsi	příměs	k1gFnSc2	příměs
v	v	k7c6	v
rudách	ruda	k1gFnPc6	ruda
jiných	jiný	k2eAgInPc2d1	jiný
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Bismut	bismut	k1gInSc1	bismut
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
starších	starý	k2eAgFnPc6d2	starší
dobách	doba	k1gFnPc6	doba
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
rud	ruda	k1gFnPc2	ruda
získáván	získávat	k5eAaImNgInS	získávat
vycezováním	vycezování	k1gNnSc7	vycezování
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
bismut	bismut	k1gInSc1	bismut
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
rud	ruda	k1gFnPc2	ruda
získává	získávat	k5eAaImIp3nS	získávat
buď	buď	k8xC	buď
redukčním	redukční	k2eAgInSc7d1	redukční
pochodem	pochod	k1gInSc7	pochod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bismit	bismit	k1gInSc1	bismit
reaguje	reagovat	k5eAaBmIp3nS	reagovat
v	v	k7c6	v
peci	pec	k1gFnSc6	pec
s	s	k7c7	s
uhlíkem	uhlík	k1gInSc7	uhlík
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
srážecím	srážecí	k2eAgInSc7d1	srážecí
pochodem	pochod	k1gInSc7	pochod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
taví	tavit	k5eAaImIp3nS	tavit
bismutinit	bismutinit	k5eAaPmF	bismutinit
se	s	k7c7	s
železem	železo	k1gNnSc7	železo
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
lze	lze	k6eAd1	lze
pražením	pražení	k1gNnSc7	pražení
převést	převést	k5eAaPmF	převést
sulfid	sulfid	k1gInSc4	sulfid
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
a	a	k8xC	a
provést	provést	k5eAaPmF	provést
redukční	redukční	k2eAgInSc4d1	redukční
pochod	pochod	k1gInSc4	pochod
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
těchto	tento	k3xDgInPc6	tento
pochodech	pochod	k1gInPc6	pochod
je	být	k5eAaImIp3nS	být
bismut	bismut	k1gInSc1	bismut
ještě	ještě	k6eAd1	ještě
značně	značně	k6eAd1	značně
znečištěn	znečistit	k5eAaPmNgMnS	znečistit
příměsemi	příměse	k1gFnPc7	příměse
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
rudě	ruda	k1gFnSc6	ruda
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
surový	surový	k2eAgInSc1d1	surový
bismut	bismut	k1gInSc1	bismut
musí	muset	k5eAaImIp3nS	muset
ještě	ještě	k6eAd1	ještě
rafinovat	rafinovat	k5eAaImF	rafinovat
<g/>
.	.	kIx.	.
</s>
<s>
Bi	Bi	k?	Bi
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
+	+	kIx~	+
3	[number]	k4	3
C	C	kA	C
→	→	k?	→
2	[number]	k4	2
Bi	Bi	k1gFnPc2	Bi
+	+	kIx~	+
3	[number]	k4	3
CO	co	k8xS	co
Redukční	redukční	k2eAgInSc1d1	redukční
pochod	pochod	k1gInSc1	pochod
Bi	Bi	k1gFnSc1	Bi
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
3	[number]	k4	3
+	+	kIx~	+
3	[number]	k4	3
Fe	Fe	k1gFnSc2	Fe
→	→	k?	→
2	[number]	k4	2
Bi	Bi	k1gFnPc2	Bi
+	+	kIx~	+
3	[number]	k4	3
FeS	fes	k1gNnPc2	fes
Srážecí	srážecí	k2eAgInSc1d1	srážecí
pochod	pochod	k1gInSc1	pochod
2	[number]	k4	2
Bi	Bi	k1gFnSc2	Bi
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
3	[number]	k4	3
+	+	kIx~	+
9	[number]	k4	9
O2	O2	k1gFnSc2	O2
→	→	k?	→
2	[number]	k4	2
Bi	Bi	k1gFnSc1	Bi
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
+	+	kIx~	+
6	[number]	k4	6
SO2	SO2	k1gFnSc7	SO2
Pražení	pražení	k1gNnSc2	pražení
sulfidu	sulfid	k1gInSc2	sulfid
Pro	pro	k7c4	pro
získání	získání	k1gNnPc4	získání
velmi	velmi	k6eAd1	velmi
čistého	čistý	k2eAgInSc2d1	čistý
bismutu	bismut	k1gInSc2	bismut
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
elektrolýzu	elektrolýza	k1gFnSc4	elektrolýza
tavenin	tavenina	k1gFnPc2	tavenina
jeho	jeho	k3xOp3gFnPc2	jeho
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
uplatnění	uplatnění	k1gNnSc1	uplatnění
nalézá	nalézat	k5eAaImIp3nS	nalézat
bismut	bismut	k1gInSc4	bismut
jako	jako	k8xC	jako
legovací	legovací	k2eAgInSc4d1	legovací
prvek	prvek	k1gInSc4	prvek
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
slitinách	slitina	k1gFnPc6	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
slitiny	slitina	k1gFnPc1	slitina
bismutu	bismut	k1gInSc2	bismut
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
nízký	nízký	k2eAgInSc4d1	nízký
bod	bod	k1gInSc4	bod
tání	tání	k1gNnSc2	tání
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
varu	var	k1gInSc2	var
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
konstrukci	konstrukce	k1gFnSc6	konstrukce
automatických	automatický	k2eAgInPc2d1	automatický
hasicích	hasicí	k2eAgInPc2d1	hasicí
systémů	systém	k1gInPc2	systém
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
sprinklerů	sprinkler	k1gInPc2	sprinkler
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
montovány	montovat	k5eAaImNgFnP	montovat
do	do	k7c2	do
výškových	výškový	k2eAgFnPc2d1	výšková
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
automaticky	automaticky	k6eAd1	automaticky
začnou	začít	k5eAaPmIp3nP	začít
rozprašovat	rozprašovat	k5eAaImF	rozprašovat
vodu	voda	k1gFnSc4	voda
při	při	k7c6	při
náhlém	náhlý	k2eAgInSc6d1	náhlý
nárůstu	nárůst	k1gInSc6	nárůst
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
nízké	nízký	k2eAgFnSc3d1	nízká
toxicitě	toxicita	k1gFnSc3	toxicita
se	se	k3xPyFc4	se
bismut	bismut	k1gInSc1	bismut
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
náhrada	náhrada	k1gFnSc1	náhrada
olova	olovo	k1gNnSc2	olovo
v	v	k7c6	v
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
aplikacích	aplikace	k1gFnPc6	aplikace
-	-	kIx~	-
především	především	k6eAd1	především
jako	jako	k8xS	jako
složka	složka	k1gFnSc1	složka
pájek	pájka	k1gFnPc2	pájka
pro	pro	k7c4	pro
instalatérské	instalatérský	k2eAgFnPc4d1	instalatérská
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
střeliva	střelivo	k1gNnSc2	střelivo
a	a	k8xC	a
broků	brok	k1gInPc2	brok
<g/>
.	.	kIx.	.
</s>
<s>
Přídavek	přídavek	k1gInSc1	přídavek
bismutu	bismut	k1gInSc2	bismut
do	do	k7c2	do
slitin	slitina	k1gFnPc2	slitina
obvykle	obvykle	k6eAd1	obvykle
snižuje	snižovat	k5eAaImIp3nS	snižovat
tvrdost	tvrdost	k1gFnSc1	tvrdost
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
jeho	jeho	k3xOp3gFnSc4	jeho
kujnost	kujnost	k1gFnSc4	kujnost
<g/>
.	.	kIx.	.
</s>
<s>
Slitina	slitina	k1gFnSc1	slitina
železa	železo	k1gNnSc2	železo
s	s	k7c7	s
bismutem	bismut	k1gInSc7	bismut
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xC	jako
kujná	kujný	k2eAgFnSc1d1	kujná
litina	litina	k1gFnSc1	litina
a	a	k8xC	a
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
běžné	běžný	k2eAgFnSc2d1	běžná
litiny	litina	k1gFnSc2	litina
snadno	snadno	k6eAd1	snadno
tvářet	tvářet	k5eAaImF	tvářet
kováním	kování	k1gNnSc7	kování
i	i	k8xC	i
válcováním	válcování	k1gNnSc7	válcování
<g/>
.	.	kIx.	.
</s>
<s>
Slitiny	slitina	k1gFnPc1	slitina
bismutu	bismut	k1gInSc2	bismut
obvykle	obvykle	k6eAd1	obvykle
při	při	k7c6	při
chladnutí	chladnutí	k1gNnSc6	chladnutí
mírně	mírně	k6eAd1	mírně
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
svůj	svůj	k3xOyFgInSc4	svůj
objem	objem	k1gInSc4	objem
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
proto	proto	k8xC	proto
k	k	k7c3	k
lití	lití	k1gNnSc3	lití
do	do	k7c2	do
forem	forma	k1gFnPc2	forma
pro	pro	k7c4	pro
vytváření	vytváření	k1gNnSc4	vytváření
přesných	přesný	k2eAgFnPc2d1	přesná
replik	replika	k1gFnPc2	replika
a	a	k8xC	a
kopií	kopie	k1gFnPc2	kopie
různých	různý	k2eAgInPc2d1	různý
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Bismut	bismut	k1gInSc1	bismut
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
do	do	k7c2	do
slitin	slitina	k1gFnPc2	slitina
ložiskových	ložiskový	k2eAgInPc2d1	ložiskový
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
známým	známý	k2eAgFnPc3d1	známá
nízkotajícím	nízkotající	k2eAgFnPc3d1	nízkotající
slitinám	slitina	k1gFnPc3	slitina
bismutu	bismut	k1gInSc2	bismut
patří	patřit	k5eAaImIp3nP	patřit
Woodův	Woodův	k2eAgInSc4d1	Woodův
kov	kov	k1gInSc4	kov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
směsí	směs	k1gFnSc7	směs
55	[number]	k4	55
%	%	kIx~	%
bismutu	bismut	k1gInSc2	bismut
<g/>
,	,	kIx,	,
25	[number]	k4	25
%	%	kIx~	%
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
15	[number]	k4	15
%	%	kIx~	%
cínu	cín	k1gInSc2	cín
a	a	k8xC	a
5	[number]	k4	5
%	%	kIx~	%
kadmia	kadmium	k1gNnSc2	kadmium
a	a	k8xC	a
taje	tát	k5eAaImIp3nS	tát
okolo	okolo	k7c2	okolo
70	[number]	k4	70
°	°	k?	°
<g/>
C.	C.	kA	C.
Lipowitzova	Lipowitzův	k2eAgFnSc1d1	Lipowitzův
slitina	slitina	k1gFnSc1	slitina
je	být	k5eAaImIp3nS	být
směsí	směs	k1gFnSc7	směs
50	[number]	k4	50
%	%	kIx~	%
bismutu	bismut	k1gInSc2	bismut
<g/>
,	,	kIx,	,
25	[number]	k4	25
%	%	kIx~	%
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
15	[number]	k4	15
%	%	kIx~	%
cínu	cín	k1gInSc2	cín
a	a	k8xC	a
10	[number]	k4	10
%	%	kIx~	%
kadmia	kadmium	k1gNnSc2	kadmium
a	a	k8xC	a
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc2	tání
má	mít	k5eAaImIp3nS	mít
okolo	okolo	k7c2	okolo
60	[number]	k4	60
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejníže	nízce	k6eAd3	nízce
tající	tající	k2eAgFnSc1d1	tající
slitina	slitina	k1gFnSc1	slitina
směsi	směs	k1gFnSc2	směs
těchto	tento	k3xDgInPc2	tento
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Roseův	Roseův	k2eAgInSc1d1	Roseův
kov	kov	k1gInSc1	kov
je	být	k5eAaImIp3nS	být
směsí	směs	k1gFnSc7	směs
50	[number]	k4	50
%	%	kIx~	%
bismutu	bismut	k1gInSc2	bismut
<g/>
,	,	kIx,	,
25	[number]	k4	25
%	%	kIx~	%
olova	olovo	k1gNnSc2	olovo
a	a	k8xC	a
25	[number]	k4	25
%	%	kIx~	%
cínu	cín	k1gInSc2	cín
a	a	k8xC	a
taje	tát	k5eAaImIp3nS	tát
při	při	k7c6	při
94	[number]	k4	94
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
je	být	k5eAaImIp3nS	být
bismut	bismut	k1gInSc1	bismut
součástí	součást	k1gFnSc7	součást
katalyzátorů	katalyzátor	k1gInPc2	katalyzátor
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
akrylátů	akrylát	k1gInPc2	akrylát
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
keramiky	keramika	k1gFnSc2	keramika
slouží	sloužit	k5eAaImIp3nS	sloužit
bismut	bismut	k1gInSc1	bismut
jako	jako	k8xC	jako
náhrada	náhrada	k1gFnSc1	náhrada
olova	olovo	k1gNnSc2	olovo
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
glazur	glazura	k1gFnPc2	glazura
<g/>
,	,	kIx,	,
barviv	barvivo	k1gNnPc2	barvivo
pro	pro	k7c4	pro
keramické	keramický	k2eAgInPc4d1	keramický
materiály	materiál	k1gInPc4	materiál
a	a	k8xC	a
výrobě	výroba	k1gFnSc3	výroba
optických	optický	k2eAgNnPc2d1	optické
vláken	vlákno	k1gNnPc2	vlákno
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
indexem	index	k1gInSc7	index
lomu	lom	k1gInSc2	lom
<g/>
.	.	kIx.	.
</s>
<s>
Bismut	bismut	k1gInSc1	bismut
je	být	k5eAaImIp3nS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
kosmetických	kosmetický	k2eAgInPc2d1	kosmetický
a	a	k8xC	a
lékařských	lékařský	k2eAgInPc2d1	lékařský
přípravků	přípravek	k1gInPc2	přípravek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
zásaditá	zásaditý	k2eAgFnSc1d1	zásaditá
sůl	sůl	k1gFnSc1	sůl
kyseliny	kyselina	k1gFnSc2	kyselina
gallové	gallový	k2eAgFnSc2d1	gallový
Bi	Bi	k1gFnSc2	Bi
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
·	·	k?	·
<g/>
OCO	OCO	kA	OCO
<g/>
·	·	k?	·
<g/>
C	C	kA	C
<g/>
6	[number]	k4	6
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
na	na	k7c4	na
zásyp	zásyp	k1gInSc4	zásyp
ran	rána	k1gFnPc2	rána
-	-	kIx~	-
dermatol	dermatol	k1gInSc1	dermatol
<g/>
;	;	kIx,	;
sloučeniny	sloučenina	k1gFnPc1	sloučenina
bismutu	bismut	k1gInSc2	bismut
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
jako	jako	k9	jako
léky	lék	k1gInPc4	lék
proti	proti	k7c3	proti
syfilidě	syfilida	k1gFnSc3	syfilida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
různých	různý	k2eAgFnPc2d1	různá
sloučenin	sloučenina	k1gFnPc2	sloučenina
bismutu	bismut	k1gInSc2	bismut
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
kosmetice	kosmetika	k1gFnSc6	kosmetika
a	a	k8xC	a
medicíně	medicína	k1gFnSc6	medicína
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
různých	různý	k2eAgInPc2d1	různý
desinfekčních	desinfekční	k2eAgInPc2d1	desinfekční
prostředků	prostředek	k1gInPc2	prostředek
a	a	k8xC	a
léků	lék	k1gInPc2	lék
používaných	používaný	k2eAgInPc2d1	používaný
při	při	k7c6	při
léčení	léčení	k1gNnSc6	léčení
žaludečních	žaludeční	k2eAgFnPc2d1	žaludeční
a	a	k8xC	a
střevních	střevní	k2eAgFnPc2d1	střevní
chorob	choroba	k1gFnPc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Slitina	slitina	k1gFnSc1	slitina
s	s	k7c7	s
manganem	mangan	k1gInSc7	mangan
s	s	k7c7	s
názvem	název	k1gInSc7	název
bismanol	bismanol	k1gInSc1	bismanol
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
velmi	velmi	k6eAd1	velmi
silných	silný	k2eAgInPc2d1	silný
permanentních	permanentní	k2eAgInPc2d1	permanentní
magnetů	magnet	k1gInPc2	magnet
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
bismut	bismut	k1gInSc1	bismut
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
především	především	k9	především
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Bi	Bi	k1gFnSc2	Bi
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gFnPc2	jeho
solí	sůl	k1gFnPc2	sůl
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
hydrolyzují	hydrolyzovat	k5eAaBmIp3nP	hydrolyzovat
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc4	oxid
bismutitý	bismutitý	k2eAgInSc4d1	bismutitý
Bi	Bi	k1gFnSc7	Bi
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
normální	normální	k2eAgFnPc4d1	normální
teploty	teplota	k1gFnPc4	teplota
žlutý	žlutý	k2eAgMnSc1d1	žlutý
a	a	k8xC	a
za	za	k7c4	za
horka	horko	k1gNnPc4	horko
červenohnědý	červenohnědý	k2eAgInSc4d1	červenohnědý
prášek	prášek	k1gInSc4	prášek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kyselinotvorný	kyselinotvorný	k2eAgInSc1d1	kyselinotvorný
oxid	oxid	k1gInSc1	oxid
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
modifikacích	modifikace	k1gFnPc6	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
oxidací	oxidace	k1gFnSc7	oxidace
kovu	kov	k1gInSc2	kov
nebo	nebo	k8xC	nebo
rozkladem	rozklad	k1gInSc7	rozklad
dusičnanu	dusičnan	k1gInSc2	dusičnan
či	či	k8xC	či
uhličitanu	uhličitan	k1gInSc2	uhličitan
bismutitého	bismutitý	k2eAgInSc2d1	bismutitý
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
bismutitý	bismutitý	k2eAgInSc4d1	bismutitý
BiCl	BiCl	k1gInSc4	BiCl
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
sněhobílá	sněhobílý	k2eAgFnSc1d1	sněhobílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
vlhkém	vlhký	k2eAgInSc6d1	vlhký
vzduchu	vzduch	k1gInSc6	vzduch
rozplývá	rozplývat	k5eAaImIp3nS	rozplývat
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
bismutitý	bismutitý	k2eAgInSc1d1	bismutitý
totiž	totiž	k9	totiž
není	být	k5eNaImIp3nS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
a	a	k8xC	a
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxichloridu	oxichlorid	k1gInSc2	oxichlorid
bismutitého	bismutitý	k2eAgInSc2d1	bismutitý
BiOCl	BiOCl	k1gInSc1	BiOCl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
jako	jako	k9	jako
netoxické	toxický	k2eNgNnSc1d1	netoxické
barvivo	barvivo	k1gNnSc1	barvivo
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k8xS	jako
perlová	perlový	k2eAgFnSc1d1	Perlová
běloba	běloba	k1gFnSc1	běloba
<g/>
.	.	kIx.	.
</s>
<s>
Bromid	bromid	k1gInSc1	bromid
bismutitý	bismutitý	k2eAgInSc4d1	bismutitý
BiBr	BiBr	k1gInSc4	BiBr
<g/>
3	[number]	k4	3
a	a	k8xC	a
jodid	jodid	k1gInSc1	jodid
bismuitý	bismuitý	k2eAgInSc1d1	bismuitý
BiI	BiI	k?	BiI
<g/>
3	[number]	k4	3
jsou	být	k5eAaImIp3nP	být
žluté	žlutý	k2eAgInPc1d1	žlutý
prášky	prášek	k1gInPc1	prášek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
podobají	podobat	k5eAaImIp3nP	podobat
chloridu	chlorid	k1gInSc2	chlorid
bismutitému	bismutitý	k2eAgNnSc3d1	bismutitý
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc1	dusičnan
bismutitý	bismutitý	k2eAgInSc1d1	bismutitý
Bi	Bi	k1gMnSc7	Bi
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
oxidusičnan	oxidusičnan	k1gInSc4	oxidusičnan
bismutitý	bismutitý	k2eAgInSc4d1	bismutitý
BiO	BiO	k?	BiO
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
barvivo	barvivo	k1gNnSc1	barvivo
s	s	k7c7	s
názvem	název	k1gInSc7	název
španělská	španělský	k2eAgFnSc1d1	španělská
běloba	běloba	k1gFnSc1	běloba
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
zásaditý	zásaditý	k2eAgInSc1d1	zásaditý
dusičnan	dusičnan	k1gInSc1	dusičnan
bismutitý	bismutitý	k2eAgInSc1d1	bismutitý
má	mít	k5eAaImIp3nS	mít
využití	využití	k1gNnSc2	využití
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc1	dusičnan
bismutitý	bismutitý	k2eAgMnSc1d1	bismutitý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
kovového	kovový	k2eAgInSc2d1	kovový
bismutu	bismut	k1gInSc2	bismut
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
dusičné	dusičný	k2eAgFnSc6d1	dusičná
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc4	síran
bizmutitý	bizmutitý	k2eAgInSc4d1	bizmutitý
Bi	Bi	k1gFnSc7	Bi
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
<g/>
,	,	kIx,	,
hygroskopická	hygroskopický	k2eAgFnSc1d1	hygroskopická
<g/>
,	,	kIx,	,
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
kovu	kov	k1gInSc2	kov
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
nebo	nebo	k8xC	nebo
sulfidu	sulfid	k1gInSc2	sulfid
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc6d1	sírová
<g/>
.	.	kIx.	.
</s>
<s>
Uhličitan	uhličitan	k1gInSc4	uhličitan
bismutitý	bismutitý	k2eAgInSc4d1	bismutitý
Bi	Bi	k1gFnSc7	Bi
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
CO	co	k6eAd1	co
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
reakcí	reakce	k1gFnSc7	reakce
rozpustné	rozpustný	k2eAgFnSc2d1	rozpustná
bismuité	bismuitý	k2eAgFnSc2d1	bismuitý
soli	sůl	k1gFnSc2	sůl
s	s	k7c7	s
rozpustným	rozpustný	k2eAgInSc7d1	rozpustný
uhličitanem	uhličitan	k1gInSc7	uhličitan
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc4	sulfid
bismutitý	bismutitý	k2eAgInSc4d1	bismutitý
Bi	Bi	k1gFnSc7	Bi
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
tmavěhnědá	tmavěhnědý	k2eAgFnSc1d1	tmavěhnědá
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
srážením	srážení	k1gNnSc7	srážení
se	s	k7c7	s
sirovodíkem	sirovodík	k1gInSc7	sirovodík
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
šedá	šedat	k5eAaImIp3nS	šedat
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
tavením	tavení	k1gNnSc7	tavení
síry	síra	k1gFnSc2	síra
s	s	k7c7	s
bismutem	bismut	k1gInSc7	bismut
<g/>
)	)	kIx)	)
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Tmavěhnědý	Tmavěhnědý	k2eAgInSc1d1	Tmavěhnědý
sulfid	sulfid	k1gInSc1	sulfid
pozvolna	pozvolna	k6eAd1	pozvolna
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
šedou	šedý	k2eAgFnSc4d1	šedá
modifikaci	modifikace	k1gFnSc4	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
minerál	minerál	k1gInSc4	minerál
bismutinit	bismutinit	k5eAaPmF	bismutinit
<g/>
.	.	kIx.	.
</s>
<s>
Bismutovodík	Bismutovodík	k1gInSc1	Bismutovodík
BiH	BiH	k1gFnSc2	BiH
<g/>
3	[number]	k4	3
neboli	neboli	k8xC	neboli
bismutan	bismutan	k1gInSc1	bismutan
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgNnPc1d1	bezbarvé
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
rozkládající	rozkládající	k2eAgFnSc1d1	rozkládající
plynná	plynný	k2eAgFnSc1d1	plynná
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
nepatrném	nepatrný	k2eAgInSc6d1	nepatrný
výtěžku	výtěžek	k1gInSc6	výtěžek
<g/>
)	)	kIx)	)
při	při	k7c6	při
rozkladu	rozklad	k1gInSc6	rozklad
práškové	práškový	k2eAgFnSc2d1	prášková
slitiny	slitina	k1gFnSc2	slitina
bismutu	bismut	k1gInSc2	bismut
s	s	k7c7	s
hořčíkem	hořčík	k1gInSc7	hořčík
kyselinou	kyselina	k1gFnSc7	kyselina
chlorovodíkovou	chlorovodíkový	k2eAgFnSc7d1	chlorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Bismut	bismut	k1gInSc1	bismut
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bismut	bismut	k1gInSc1	bismut
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
bismut	bismut	k1gInSc1	bismut
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
