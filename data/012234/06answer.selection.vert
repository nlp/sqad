<s>
Ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
UV	UV	kA	UV
<g/>
,	,	kIx,	,
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
ultraviolet	ultraviolet	k1gInSc1	ultraviolet
<g/>
)	)	kIx)	)
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
s	s	k7c7	s
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
kratší	krátký	k2eAgFnSc7d2	kratší
než	než	k8xS	než
má	mít	k5eAaImIp3nS	mít
viditelné	viditelný	k2eAgNnSc1d1	viditelné
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
delší	dlouhý	k2eAgMnSc1d2	delší
než	než	k8xS	než
má	mít	k5eAaImIp3nS	mít
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
