<p>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2017	[number]	k4	2017
Tesla	Tesla	k1gFnSc1	Tesla
Motors	Motorsa	k1gFnPc2	Motorsa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
automobilka	automobilka	k1gFnSc1	automobilka
a	a	k8xC	a
energetická	energetický	k2eAgFnSc1d1	energetická
společnost	společnost	k1gFnSc1	společnost
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
Palo	Pala	k1gMnSc5	Pala
Alto	Alto	k1gNnSc1	Alto
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Silicon	Silicon	kA	Silicon
Valley	Vallea	k1gMnSc2	Vallea
<g/>
.	.	kIx.	.
</s>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
<g/>
,	,	kIx,	,
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
a	a	k8xC	a
prodává	prodávat	k5eAaImIp3nS	prodávat
elektromobily	elektromobil	k1gInPc4	elektromobil
<g/>
,	,	kIx,	,
domácí	domácí	k2eAgNnSc1d1	domácí
úložiště	úložiště	k1gNnSc1	úložiště
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
Powerwall	Powerwalla	k1gFnPc2	Powerwalla
<g/>
,	,	kIx,	,
komerční	komerční	k2eAgNnPc4d1	komerční
úložiště	úložiště	k1gNnPc4	úložiště
Powerpack	Powerpacka	k1gFnPc2	Powerpacka
a	a	k8xC	a
solární	solární	k2eAgFnPc1d1	solární
střešní	střešní	k2eAgFnPc1d1	střešní
tašky	taška	k1gFnPc1	taška
<g/>
.	.	kIx.	.
</s>
<s>
Firmu	firma	k1gFnSc4	firma
založili	založit	k5eAaPmAgMnP	založit
Martin	Martin	k1gMnSc1	Martin
Eberhard	Eberhard	k1gMnSc1	Eberhard
a	a	k8xC	a
Marc	Marc	k1gFnSc1	Marc
Tarpenning	Tarpenning	k1gInSc1	Tarpenning
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
a	a	k8xC	a
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
ji	on	k3xPp3gFnSc4	on
po	po	k7c6	po
elektroinženýrovi	elektroinženýr	k1gMnSc6	elektroinženýr
a	a	k8xC	a
fyzikovi	fyzik	k1gMnSc6	fyzik
Nikolovi	Nikola	k1gMnSc6	Nikola
Teslovi	Tesla	k1gMnSc6	Tesla
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
k	k	k7c3	k
firmě	firma	k1gFnSc3	firma
připojili	připojit	k5eAaPmAgMnP	připojit
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
<g/>
,	,	kIx,	,
JB	JB	kA	JB
Straubel	Straubel	k1gInSc1	Straubel
a	a	k8xC	a
Ian	Ian	k1gMnSc1	Ian
Wright	Wright	k1gMnSc1	Wright
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Automobilka	automobilka	k1gFnSc1	automobilka
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
veřejného	veřejný	k2eAgNnSc2d1	veřejné
povědomí	povědomí	k1gNnSc2	povědomí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začala	začít	k5eAaPmAgFnS	začít
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
svého	svůj	k3xOyFgInSc2	svůj
prvního	první	k4xOgInSc2	první
sportovního	sportovní	k2eAgInSc2d1	sportovní
elektromobilu	elektromobil	k1gInSc2	elektromobil
Tesla	Tesla	k1gFnSc1	Tesla
Roadster	roadster	k1gInSc1	roadster
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
tvoří	tvořit	k5eAaImIp3nP	tvořit
nabídku	nabídka	k1gFnSc4	nabídka
firmy	firma	k1gFnSc2	firma
luxusní	luxusní	k2eAgInSc1d1	luxusní
sedan	sedan	k1gInSc1	sedan
Model	model	k1gInSc1	model
S	s	k7c7	s
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgNnSc1d1	elektrické
SUV	SUV	kA	SUV
Model	modla	k1gFnPc2	modla
X	X	kA	X
a	a	k8xC	a
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
levnější	levný	k2eAgInSc1d2	levnější
Model	model	k1gInSc1	model
3	[number]	k4	3
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
dorazí	dorazit	k5eAaPmIp3nS	dorazit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prvního	první	k4xOgInSc2	první
prodeje	prodej	k1gInSc2	prodej
modelu	model	k1gInSc2	model
Roadster	roadster	k1gInSc1	roadster
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
do	do	k7c2	do
září	září	k1gNnSc2	září
2016	[number]	k4	2016
prodala	prodat	k5eAaPmAgFnS	prodat
Tesla	Tesla	k1gFnSc1	Tesla
Motors	Motorsa	k1gFnPc2	Motorsa
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
160	[number]	k4	160
000	[number]	k4	000
elektrických	elektrický	k2eAgInPc2d1	elektrický
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
Tesla	Tesla	k1gFnSc1	Tesla
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
cca	cca	kA	cca
25	[number]	k4	25
elektromobilů	elektromobil	k1gInPc2	elektromobil
týdně	týdně	k6eAd1	týdně
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
přibližně	přibližně	k6eAd1	přibližně
2000	[number]	k4	2000
vozů	vůz	k1gInPc2	vůz
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
cenově	cenově	k6eAd1	cenově
dostupnějšího	dostupný	k2eAgInSc2d2	dostupnější
Modelu	model	k1gInSc2	model
3	[number]	k4	3
chce	chtít	k5eAaImIp3nS	chtít
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
dále	daleko	k6eAd2	daleko
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
produkci	produkce	k1gFnSc4	produkce
a	a	k8xC	a
překonat	překonat	k5eAaPmF	překonat
hranici	hranice	k1gFnSc4	hranice
500	[number]	k4	500
000	[number]	k4	000
prodaných	prodaný	k2eAgInPc2d1	prodaný
elektromobilů	elektromobil	k1gInPc2	elektromobil
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
3	[number]	k4	3
má	mít	k5eAaImIp3nS	mít
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
500	[number]	k4	500
000	[number]	k4	000
zaregistrovaných	zaregistrovaný	k2eAgFnPc2d1	zaregistrovaná
objednávek	objednávka	k1gFnPc2	objednávka
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
Modelu	model	k1gInSc2	model
3	[number]	k4	3
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c4	na
35	[number]	k4	35
000	[number]	k4	000
amerických	americký	k2eAgInPc6d1	americký
dolarech	dolar	k1gInPc6	dolar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
zhruba	zhruba	k6eAd1	zhruba
polovinu	polovina	k1gFnSc4	polovina
současných	současný	k2eAgFnPc2d1	současná
cen	cena	k1gFnPc2	cena
Modelu	model	k1gInSc2	model
S	s	k7c7	s
a	a	k8xC	a
X.	X.	kA	X.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2003	[number]	k4	2003
Martinem	Martin	k1gMnSc7	Martin
Eberhardem	Eberhard	k1gMnSc7	Eberhard
a	a	k8xC	a
Marcem	Marce	k1gMnSc7	Marce
Tarpenningem	Tarpenning	k1gInSc7	Tarpenning
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
k	k	k7c3	k
firmě	firma	k1gFnSc3	firma
připojili	připojit	k5eAaPmAgMnP	připojit
další	další	k2eAgMnPc1d1	další
investoři	investor	k1gMnPc1	investor
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
do	do	k7c2	do
firmy	firma	k1gFnSc2	firma
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
financování	financování	k1gNnSc2	financování
investoval	investovat	k5eAaBmAgInS	investovat
7,5	[number]	k4	7,5
milionů	milion	k4xCgInPc2	milion
USD	USD	kA	USD
z	z	k7c2	z
vlastních	vlastní	k2eAgInPc2d1	vlastní
zdrojů	zdroj	k1gInPc2	zdroj
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
kolech	kolo	k1gNnPc6	kolo
financování	financování	k1gNnSc2	financování
pak	pak	k8xC	pak
další	další	k2eAgFnPc1d1	další
desítky	desítka	k1gFnPc1	desítka
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
je	být	k5eAaImIp3nS	být
Tesla	Tesla	k1gFnSc1	Tesla
veřejně	veřejně	k6eAd1	veřejně
obchodovatelnou	obchodovatelný	k2eAgFnSc7d1	obchodovatelná
společností	společnost	k1gFnSc7	společnost
na	na	k7c6	na
technologické	technologický	k2eAgFnSc6d1	technologická
burze	burza	k1gFnSc6	burza
NASDAQ	NASDAQ	kA	NASDAQ
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
Tesly	Tesla	k1gFnSc2	Tesla
na	na	k7c4	na
burzu	burza	k1gFnSc4	burza
byl	být	k5eAaImAgInS	být
senzací	senzace	k1gFnSc7	senzace
a	a	k8xC	a
cena	cena	k1gFnSc1	cena
akcií	akcie	k1gFnPc2	akcie
lámala	lámat	k5eAaImAgFnS	lámat
rekordy	rekord	k1gInPc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
akcie	akcie	k1gFnPc1	akcie
Tesly	Tesla	k1gFnSc2	Tesla
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
nadhodnocené	nadhodnocený	k2eAgInPc4d1	nadhodnocený
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
připouští	připouštět	k5eAaImIp3nS	připouštět
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
Musk	Musk	k1gMnSc1	Musk
<g/>
.	.	kIx.	.
</s>
<s>
Důvěru	důvěra	k1gFnSc4	důvěra
investorů	investor	k1gMnPc2	investor
však	však	k9	však
získala	získat	k5eAaPmAgFnS	získat
firma	firma	k1gFnSc1	firma
už	už	k6eAd1	už
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
na	na	k7c4	na
burzu	burza	k1gFnSc4	burza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
společnost	společnost	k1gFnSc1	společnost
Daimler	Daimler	k1gMnSc1	Daimler
AG	AG	kA	AG
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
za	za	k7c4	za
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
USD	USD	kA	USD
10	[number]	k4	10
<g/>
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
Tesly	Tesla	k1gFnSc2	Tesla
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vzápětí	vzápětí	k6eAd1	vzápětí
prodala	prodat	k5eAaPmAgFnS	prodat
40	[number]	k4	40
<g/>
%	%	kIx~	%
svého	své	k1gNnSc2	své
podílu	podíl	k1gInSc2	podíl
Aabar	Aabara	k1gFnPc2	Aabara
Investments	Investmentsa	k1gFnPc2	Investmentsa
z	z	k7c2	z
Abu	Abu	k1gFnSc2	Abu
Dhabí	Dhabí	k1gNnSc2	Dhabí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
Tesla	Tesla	k1gFnSc1	Tesla
přesvědčila	přesvědčit	k5eAaPmAgFnS	přesvědčit
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
důvěryhodnosti	důvěryhodnost	k1gFnSc6	důvěryhodnost
i	i	k9	i
americkou	americký	k2eAgFnSc4d1	americká
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jí	on	k3xPp3gFnSc3	on
půjčila	půjčit	k5eAaPmAgFnS	půjčit
465	[number]	k4	465
milionů	milion	k4xCgInPc2	milion
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
půjčku	půjčka	k1gFnSc4	půjčka
splatila	splatit	k5eAaPmAgFnS	splatit
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
výši	výše	k1gFnSc6	výše
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
<g/>
Prvotní	prvotní	k2eAgInSc1d1	prvotní
prodej	prodej	k1gInSc1	prodej
akcií	akcie	k1gFnPc2	akcie
firmě	firma	k1gFnSc6	firma
vynesl	vynést	k5eAaPmAgInS	vynést
226	[number]	k4	226
milionů	milion	k4xCgInPc2	milion
USD	USD	kA	USD
a	a	k8xC	a
významnou	významný	k2eAgFnSc7d1	významná
měrou	míra	k1gFnSc7wR	míra
tak	tak	k6eAd1	tak
přispěl	přispět	k5eAaPmAgMnS	přispět
ke	k	k7c3	k
splacení	splacení	k1gNnSc3	splacení
půjčky	půjčka	k1gFnSc2	půjčka
americké	americký	k2eAgFnSc3d1	americká
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
akcie	akcie	k1gFnSc2	akcie
firmy	firma	k1gFnSc2	firma
ztratily	ztratit	k5eAaPmAgFnP	ztratit
20	[number]	k4	20
<g/>
%	%	kIx~	%
své	svůj	k3xOyFgFnSc2	svůj
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
dáno	dát	k5eAaPmNgNnS	dát
především	především	k6eAd1	především
medializací	medializace	k1gFnSc7	medializace
požárů	požár	k1gInPc2	požár
tří	tři	k4xCgInPc2	tři
Modelů	model	k1gInPc2	model
S.	S.	kA	S.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
hurikánem	hurikán	k1gInSc7	hurikán
Irma	Irma	k1gFnSc1	Irma
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
firma	firma	k1gFnSc1	firma
softwareově	softwareův	k2eAgFnSc6d1	softwareův
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
vypnula	vypnout	k5eAaPmAgFnS	vypnout
omezení	omezení	k1gNnSc4	omezení
kapacity	kapacita	k1gFnSc2	kapacita
baterie	baterie	k1gFnSc2	baterie
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
úmyslně	úmyslně	k6eAd1	úmyslně
vestavěno	vestavět	k5eAaPmNgNnS	vestavět
u	u	k7c2	u
nižších	nízký	k2eAgInPc2d2	nižší
modelů	model	k1gInPc2	model
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Modely	model	k1gInPc1	model
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Tesla	Tesla	k1gFnSc1	Tesla
Roadster	roadster	k1gInSc1	roadster
===	===	k?	===
</s>
</p>
<p>
<s>
Tesla	Tesla	k1gMnSc1	Tesla
Roadster	roadster	k1gInSc4	roadster
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgInSc7	první
modelem	model	k1gInSc7	model
automobilky	automobilka	k1gFnSc2	automobilka
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jí	jíst	k5eAaImIp3nS	jíst
zajistil	zajistit	k5eAaPmAgMnS	zajistit
publicitu	publicita	k1gFnSc4	publicita
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
využíval	využívat	k5eAaImAgInS	využívat
lithium-iontové	lithiumontový	k2eAgInPc4d1	lithium-iontový
akumulátory	akumulátor	k1gInPc4	akumulátor
a	a	k8xC	a
automobil	automobil	k1gInSc4	automobil
měl	mít	k5eAaImAgInS	mít
dojezd	dojezd	k1gInSc1	dojezd
téměř	téměř	k6eAd1	téměř
400	[number]	k4	400
km	km	kA	km
na	na	k7c4	na
plné	plný	k2eAgNnSc4d1	plné
nabití	nabití	k1gNnSc4	nabití
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
prototyp	prototyp	k1gInSc1	prototyp
vozu	vůz	k1gInSc2	vůz
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
várka	várka	k1gFnSc1	várka
čítající	čítající	k2eAgFnSc1d1	čítající
100	[number]	k4	100
kusů	kus	k1gInPc2	kus
se	se	k3xPyFc4	se
vyprodala	vyprodat	k5eAaPmAgFnS	vyprodat
během	během	k7c2	během
tří	tři	k4xCgInPc2	tři
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Masová	masový	k2eAgFnSc1d1	masová
produkce	produkce	k1gFnSc1	produkce
pak	pak	k6eAd1	pak
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2008	[number]	k4	2008
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
automobilu	automobil	k1gInSc2	automobil
probíhala	probíhat	k5eAaImAgFnS	probíhat
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
očekáván	očekáván	k2eAgInSc1d1	očekáván
příchod	příchod	k1gInSc1	příchod
druhé	druhý	k4xOgFnSc2	druhý
generace	generace	k1gFnSc2	generace
tohoto	tento	k3xDgInSc2	tento
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
do	do	k7c2	do
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
firma	firma	k1gFnSc1	firma
dodala	dodat	k5eAaPmAgFnS	dodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2550	[number]	k4	2550
Roadsterů	roadster	k1gInPc2	roadster
svým	svůj	k3xOyFgMnPc3	svůj
zákazníkům	zákazník	k1gMnPc3	zákazník
v	v	k7c6	v
31	[number]	k4	31
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
<g/>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
dojezd	dojezd	k1gInSc1	dojezd
automobilu	automobil	k1gInSc2	automobil
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
Tesly	Tesla	k1gFnSc2	Tesla
245	[number]	k4	245
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
394	[number]	k4	394
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
ujel	ujet	k5eAaPmAgInS	ujet
Roadster	roadster	k1gInSc1	roadster
řízený	řízený	k2eAgInSc1d1	řízený
Simonem	Simon	k1gMnSc7	Simon
Hackettem	Hackett	k1gMnSc7	Hackett
celý	celý	k2eAgInSc4d1	celý
313	[number]	k4	313
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
504	[number]	k4	504
km	km	kA	km
<g/>
)	)	kIx)	)
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
úsek	úsek	k1gInSc4	úsek
australského	australský	k2eAgInSc2d1	australský
každoročního	každoroční	k2eAgInSc2d1	každoroční
závodu	závod	k1gInSc2	závod
ekologických	ekologický	k2eAgNnPc2d1	ekologické
aut	auto	k1gNnPc2	auto
Global	globat	k5eAaImAgInS	globat
Green	Green	k2eAgInSc1d1	Green
Challenge	Challenge	k1gInSc1	Challenge
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
nabití	nabití	k1gNnSc4	nabití
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
rychlostí	rychlost	k1gFnSc7	rychlost
40	[number]	k4	40
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Tesla	Tesla	k1gFnSc1	Tesla
Roadster	roadster	k1gInSc1	roadster
zvládl	zvládnout	k5eAaPmAgInS	zvládnout
zrychlení	zrychlení	k1gNnSc4	zrychlení
z	z	k7c2	z
0	[number]	k4	0
na	na	k7c4	na
97	[number]	k4	97
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
za	za	k7c4	za
3,9	[number]	k4	3,9
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
činí	činit	k5eAaImIp3nS	činit
201	[number]	k4	201
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Základní	základní	k2eAgFnSc1d1	základní
cena	cena	k1gFnSc1	cena
auta	auto	k1gNnSc2	auto
byla	být	k5eAaImAgFnS	být
109	[number]	k4	109
000	[number]	k4	000
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Roadster	roadster	k1gInSc1	roadster
Sport	sport	k1gInSc1	sport
pak	pak	k6eAd1	pak
začínal	začínat	k5eAaImAgInS	začínat
na	na	k7c4	na
128	[number]	k4	128
500	[number]	k4	500
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sportovnější	sportovní	k2eAgFnSc1d2	sportovnější
verze	verze	k1gFnSc1	verze
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
zrychlit	zrychlit	k5eAaPmF	zrychlit
z	z	k7c2	z
0	[number]	k4	0
na	na	k7c4	na
97	[number]	k4	97
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
za	za	k7c4	za
3,7	[number]	k4	3,7
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2018	[number]	k4	2018
se	se	k3xPyFc4	se
Tesla	Tesla	k1gFnSc1	Tesla
Roadster	roadster	k1gInSc1	roadster
stal	stát	k5eAaPmAgInS	stát
prvním	první	k4xOgInSc7	první
pozemským	pozemský	k2eAgInSc7d1	pozemský
osobním	osobní	k2eAgInSc7d1	osobní
automobilem	automobil	k1gInSc7	automobil
vyneseným	vynesený	k2eAgInSc7d1	vynesený
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
při	při	k7c6	při
úspěšném	úspěšný	k2eAgInSc6d1	úspěšný
testu	test	k1gInSc6	test
nové	nový	k2eAgFnSc2d1	nová
rakety	raketa	k1gFnSc2	raketa
Falcon	Falcona	k1gFnPc2	Falcona
Heavy	Heava	k1gFnSc2	Heava
společnosti	společnost	k1gFnSc2	společnost
SpaceX	SpaceX	k1gFnSc2	SpaceX
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tesla	Tesla	k1gFnSc1	Tesla
Model	model	k1gInSc1	model
S	s	k7c7	s
===	===	k?	===
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
nového	nový	k2eAgInSc2d1	nový
sedanu	sedan	k1gInSc2	sedan
automobilky	automobilka	k1gFnSc2	automobilka
byl	být	k5eAaImAgInS	být
oznámen	oznámit	k5eAaPmNgInS	oznámit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2008	[number]	k4	2008
a	a	k8xC	a
nový	nový	k2eAgInSc1d1	nový
Model	model	k1gInSc1	model
S	s	k7c7	s
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
prodávat	prodávat	k5eAaImF	prodávat
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
ukončení	ukončení	k1gNnSc6	ukončení
produkce	produkce	k1gFnSc2	produkce
Roadsteru	roadster	k1gInSc2	roadster
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
automobil	automobil	k1gInSc1	automobil
nabízel	nabízet	k5eAaImAgInS	nabízet
s	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
různými	různý	k2eAgFnPc7d1	různá
kapacitami	kapacita	k1gFnPc7	kapacita
baterie	baterie	k1gFnSc2	baterie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
malý	malý	k2eAgInSc4d1	malý
zájem	zájem	k1gInSc4	zájem
byla	být	k5eAaImAgFnS	být
verze	verze	k1gFnSc1	verze
se	s	k7c7	s
40	[number]	k4	40
kWh	kwh	kA	kwh
baterií	baterie	k1gFnPc2	baterie
vyřazena	vyřazen	k2eAgFnSc1d1	vyřazena
z	z	k7c2	z
nabídky	nabídka	k1gFnSc2	nabídka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
tak	tak	k6eAd1	tak
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
dvě	dva	k4xCgFnPc1	dva
verze	verze	k1gFnPc1	verze
s	s	k7c7	s
60	[number]	k4	60
a	a	k8xC	a
85	[number]	k4	85
kWh	kwh	kA	kwh
bateriemi	baterie	k1gFnPc7	baterie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Model	model	k1gInSc1	model
S	s	k7c7	s
nabízí	nabízet	k5eAaImIp3nS	nabízet
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
typy	typ	k1gInPc7	typ
baterií	baterie	k1gFnPc2	baterie
o	o	k7c6	o
kapacitách	kapacita	k1gFnPc6	kapacita
70	[number]	k4	70
a	a	k8xC	a
100	[number]	k4	100
kWh	kwh	kA	kwh
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
výkonnostních	výkonnostní	k2eAgFnPc6d1	výkonnostní
variantách	varianta	k1gFnPc6	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholným	vrcholný	k2eAgNnSc7d1	vrcholné
provedením	provedení	k1gNnSc7	provedení
je	být	k5eAaImIp3nS	být
verze	verze	k1gFnSc1	verze
P100D	P100D	k1gFnSc1	P100D
se	s	k7c7	s
100	[number]	k4	100
kWh	kwh	kA	kwh
baterií	baterie	k1gFnPc2	baterie
<g/>
,	,	kIx,	,
s	s	k7c7	s
dojezdem	dojezd	k1gInSc7	dojezd
až	až	k9	až
613	[number]	k4	613
km	km	kA	km
<g/>
,	,	kIx,	,
zrychlením	zrychlení	k1gNnSc7	zrychlení
z	z	k7c2	z
0	[number]	k4	0
na	na	k7c4	na
100	[number]	k4	100
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
za	za	k7c4	za
2,7	[number]	k4	2,7
sekundy	sekunda	k1gFnSc2	sekunda
a	a	k8xC	a
maximální	maximální	k2eAgFnSc7d1	maximální
rychlostí	rychlost	k1gFnSc7	rychlost
250	[number]	k4	250
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2016	[number]	k4	2016
dostal	dostat	k5eAaPmAgInS	dostat
Model	model	k1gInSc1	model
S	s	k7c7	s
nový	nový	k2eAgInSc4d1	nový
design	design	k1gInSc4	design
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
s	s	k7c7	s
designem	design	k1gInSc7	design
Modelu	model	k1gInSc2	model
X.	X.	kA	X.
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
změnou	změna	k1gFnSc7	změna
bylo	být	k5eAaImAgNnS	být
odstranění	odstranění	k1gNnSc1	odstranění
falešné	falešný	k2eAgFnSc2d1	falešná
masky	maska	k1gFnSc2	maska
chladiče	chladič	k1gInSc2	chladič
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
původní	původní	k2eAgInSc1d1	původní
Model	model	k1gInSc1	model
S	s	k7c7	s
disponoval	disponovat	k5eAaBmAgInS	disponovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Model	model	k1gInSc4	model
S	s	k7c7	s
je	být	k5eAaImIp3nS	být
vyráběn	vyráběn	k2eAgInSc1d1	vyráběn
ve	v	k7c6	v
Fremontu	Fremont	k1gInSc6	Fremont
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
evropský	evropský	k2eAgInSc4d1	evropský
trh	trh	k1gInSc4	trh
je	být	k5eAaImIp3nS	být
automobil	automobil	k1gInSc1	automobil
montován	montován	k2eAgInSc1d1	montován
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
distribučním	distribuční	k2eAgNnSc6d1	distribuční
centru	centrum	k1gNnSc6	centrum
automobilky	automobilka	k1gFnSc2	automobilka
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Tilburgu	Tilburg	k1gInSc6	Tilburg
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Tilburg	Tilburg	k1gInSc1	Tilburg
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
distribuci	distribuce	k1gFnSc4	distribuce
aut	aut	k1gInSc1	aut
vybrán	vybrán	k2eAgInSc1d1	vybrán
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
polohu	poloha	k1gFnSc4	poloha
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
přístavu	přístav	k1gInSc2	přístav
v	v	k7c6	v
Rotterdamu	Rotterdam	k1gInSc6	Rotterdam
<g/>
,	,	kIx,	,
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
něhož	jenž	k3xRgNnSc2	jenž
se	se	k3xPyFc4	se
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
dostávají	dostávat	k5eAaImIp3nP	dostávat
komponenty	komponent	k1gInPc4	komponent
pro	pro	k7c4	pro
montáž	montáž	k1gFnSc4	montáž
finálních	finální	k2eAgInPc2d1	finální
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Auta	auto	k1gNnPc1	auto
jsou	být	k5eAaImIp3nP	být
vyrobena	vyroben	k2eAgNnPc1d1	vyrobeno
a	a	k8xC	a
otestována	otestován	k2eAgNnPc1d1	otestováno
ve	v	k7c4	v
Fremontu	Fremonta	k1gFnSc4	Fremonta
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
baterie	baterie	k1gFnPc1	baterie
<g/>
,	,	kIx,	,
elektromotor	elektromotor	k1gInSc1	elektromotor
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
části	část	k1gFnPc1	část
rozebrány	rozebrán	k2eAgFnPc1d1	rozebrána
a	a	k8xC	a
odeslány	odeslán	k2eAgFnPc1d1	odeslána
do	do	k7c2	do
Tilburgu	Tilburg	k1gInSc2	Tilburg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
znovu	znovu	k6eAd1	znovu
smontovány	smontován	k2eAgFnPc1d1	smontována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tesla	Tesla	k1gMnSc1	Tesla
Model	model	k1gInSc4	model
S	s	k7c7	s
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
elektromobil	elektromobil	k1gInSc4	elektromobil
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
majitelé	majitel	k1gMnPc1	majitel
společně	společně	k6eAd1	společně
překročili	překročit	k5eAaPmAgMnP	překročit
hranici	hranice	k1gFnSc4	hranice
miliardy	miliarda	k4xCgFnSc2	miliarda
najetých	najetý	k2eAgFnPc2d1	najetá
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
1,6	[number]	k4	1,6
miliardy	miliarda	k4xCgFnSc2	miliarda
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
bez	bez	k7c2	bez
emisí	emise	k1gFnPc2	emise
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tesla	Tesla	k1gFnSc1	Tesla
Model	model	k1gInSc1	model
X	X	kA	X
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2012	[number]	k4	2012
Tesla	Tesla	k1gFnSc1	Tesla
oznámila	oznámit	k5eAaPmAgFnS	oznámit
vývoj	vývoj	k1gInSc4	vývoj
Modelu	model	k1gInSc2	model
X	X	kA	X
<g/>
,	,	kIx,	,
elektrického	elektrický	k2eAgInSc2d1	elektrický
SUV	SUV	kA	SUV
pro	pro	k7c4	pro
7	[number]	k4	7
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
s	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
úložnými	úložný	k2eAgInPc7d1	úložný
prostory	prostor	k1gInPc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
Model	model	k1gInSc1	model
X	X	kA	X
byl	být	k5eAaImAgInS	být
prezentován	prezentovat	k5eAaBmNgInS	prezentovat
a	a	k8xC	a
předán	předat	k5eAaPmNgInS	předat
prvním	první	k4xOgMnSc6	první
majitelům	majitel	k1gMnPc3	majitel
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tisíc	tisíc	k4xCgInSc1	tisíc
lidí	člověk	k1gMnPc2	člověk
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
odhalení	odhalení	k1gNnSc1	odhalení
automobilu	automobil	k1gInSc2	automobil
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgNnSc6	který
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
auto	auto	k1gNnSc1	auto
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2013	[number]	k4	2013
automobilka	automobilka	k1gFnSc1	automobilka
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
produkce	produkce	k1gFnSc1	produkce
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
na	na	k7c4	na
konec	konec	k1gInSc4	konec
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
ziskovost	ziskovost	k1gFnSc4	ziskovost
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
a	a	k8xC	a
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
dosažení	dosažení	k1gNnSc4	dosažení
20	[number]	k4	20
000	[number]	k4	000
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
Modelů	model	k1gInPc2	model
S	s	k7c7	s
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
začala	začít	k5eAaPmAgFnS	začít
Tesla	Tesla	k1gFnSc1	Tesla
přijímat	přijímat	k5eAaImF	přijímat
objednávky	objednávka	k1gFnPc4	objednávka
na	na	k7c4	na
první	první	k4xOgInPc4	první
automobily	automobil	k1gInPc4	automobil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2013	[number]	k4	2013
společnost	společnost	k1gFnSc1	společnost
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
bude	být	k5eAaImBp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
dodat	dodat	k5eAaPmF	dodat
prvních	první	k4xOgInPc2	první
pár	pár	k4xCyI	pár
kusů	kus	k1gInPc2	kus
Modelu	model	k1gInSc2	model
X	X	kA	X
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
masová	masový	k2eAgFnSc1d1	masová
výroba	výroba	k1gFnSc1	výroba
začne	začít	k5eAaPmIp3nS	začít
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
odložení	odložení	k1gNnSc4	odložení
prvních	první	k4xOgFnPc2	první
dodávek	dodávka	k1gFnPc2	dodávka
až	až	k6eAd1	až
na	na	k7c6	na
září	září	k1gNnSc6	září
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
automobilka	automobilka	k1gFnSc1	automobilka
registrovala	registrovat	k5eAaBmAgFnS	registrovat
již	již	k6eAd1	již
30	[number]	k4	30
000	[number]	k4	000
objednávek	objednávka	k1gFnPc2	objednávka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2016	[number]	k4	2016
se	se	k3xPyFc4	se
Model	model	k1gInSc1	model
X	X	kA	X
stal	stát	k5eAaPmAgInS	stát
nejprodávanějším	prodávaný	k2eAgInSc7d3	nejprodávanější
elektromobilem	elektromobil	k1gInSc7	elektromobil
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tesla	Tesla	k1gFnSc1	Tesla
Model	model	k1gInSc1	model
3	[number]	k4	3
===	===	k?	===
</s>
</p>
<p>
<s>
Model	model	k1gInSc1	model
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgInSc7	druhý
luxusním	luxusní	k2eAgInSc7d1	luxusní
sedanem	sedan	k1gInSc7	sedan
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Odhalen	odhalen	k2eAgMnSc1d1	odhalen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2016	[number]	k4	2016
a	a	k8xC	a
první	první	k4xOgFnPc4	první
dodávky	dodávka	k1gFnPc4	dodávka
jsou	být	k5eAaImIp3nP	být
plánovány	plánovat	k5eAaImNgFnP	plánovat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
3	[number]	k4	3
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
stát	stát	k5eAaPmF	stát
dostupnějším	dostupný	k2eAgInSc7d2	dostupnější
elektromobilem	elektromobil	k1gInSc7	elektromobil
než	než	k8xS	než
ostatní	ostatní	k2eAgInPc4d1	ostatní
dva	dva	k4xCgInPc4	dva
modely	model	k1gInPc4	model
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
cena	cena	k1gFnSc1	cena
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c4	na
35	[number]	k4	35
000	[number]	k4	000
USD	USD	kA	USD
před	před	k7c7	před
dalšími	další	k2eAgFnPc7d1	další
slevami	sleva	k1gFnPc7	sleva
a	a	k8xC	a
dotacemi	dotace	k1gFnPc7	dotace
<g/>
.	.	kIx.	.
</s>
<s>
Automobil	automobil	k1gInSc1	automobil
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hitem	hit	k1gInSc7	hit
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
než	než	k8xS	než
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prezentace	prezentace	k1gFnSc2	prezentace
automobilu	automobil	k1gInSc2	automobil
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ještě	ještě	k9	ještě
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
představením	představení	k1gNnSc7	představení
firma	firma	k1gFnSc1	firma
registrovala	registrovat	k5eAaBmAgFnS	registrovat
přes	přes	k7c4	přes
100	[number]	k4	100
000	[number]	k4	000
rezervací	rezervace	k1gFnPc2	rezervace
a	a	k8xC	a
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
Tesla	Tesla	k1gFnSc1	Tesla
hlásila	hlásit	k5eAaImAgFnS	hlásit
již	již	k6eAd1	již
400	[number]	k4	400
000	[number]	k4	000
rezervací	rezervace	k1gFnPc2	rezervace
na	na	k7c4	na
Model	model	k1gInSc4	model
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
Model	model	k1gInSc1	model
3	[number]	k4	3
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
strategie	strategie	k1gFnSc2	strategie
společnosti	společnost	k1gFnSc2	společnost
začít	začít	k5eAaPmF	začít
nejprve	nejprve	k6eAd1	nejprve
vyrábět	vyrábět	k5eAaImF	vyrábět
drahá	drahý	k2eAgNnPc4d1	drahé
luxusní	luxusní	k2eAgNnPc4d1	luxusní
auta	auto	k1gNnPc4	auto
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
rozšířit	rozšířit	k5eAaPmF	rozšířit
nabídku	nabídka	k1gFnSc4	nabídka
i	i	k9	i
o	o	k7c4	o
dostupnější	dostupný	k2eAgInPc4d2	dostupnější
modely	model	k1gInPc4	model
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
vývoj	vývoj	k1gInSc1	vývoj
bude	být	k5eAaImBp3nS	být
zaplacen	zaplatit	k5eAaPmNgInS	zaplatit
z	z	k7c2	z
prodejů	prodej	k1gInPc2	prodej
dražších	drahý	k2eAgInPc2d2	dražší
modelů	model	k1gInPc2	model
<g/>
.	.	kIx.	.
</s>
<s>
Automobil	automobil	k1gInSc1	automobil
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
designu	design	k1gInSc2	design
Modelu	model	k1gInSc2	model
S	s	k7c7	s
<g/>
,	,	kIx,	,
jen	jen	k8xS	jen
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
zhruba	zhruba	k6eAd1	zhruba
pětinu	pětina	k1gFnSc4	pětina
menší	malý	k2eAgFnSc4d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
ceně	cena	k1gFnSc6	cena
bude	být	k5eAaImBp3nS	být
zahrnut	zahrnout	k5eAaPmNgInS	zahrnout
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
síti	síť	k1gFnSc3	síť
nabíjecích	nabíjecí	k2eAgFnPc2d1	nabíjecí
stanic	stanice	k1gFnPc2	stanice
Supercharger	Supercharger	k1gInSc1	Supercharger
a	a	k8xC	a
kostra	kostra	k1gFnSc1	kostra
nebude	být	k5eNaImBp3nS	být
z	z	k7c2	z
hliníku	hliník	k1gInSc2	hliník
jako	jako	k8xS	jako
u	u	k7c2	u
Modelu	model	k1gInSc2	model
S	s	k7c7	s
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc4	model
3	[number]	k4	3
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
urazit	urazit	k5eAaPmF	urazit
minimálně	minimálně	k6eAd1	minimálně
320	[number]	k4	320
km	km	kA	km
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
nabití	nabití	k1gNnSc4	nabití
<g/>
.	.	kIx.	.
</s>
<s>
Zrychlení	zrychlený	k2eAgMnPc1d1	zrychlený
z	z	k7c2	z
0	[number]	k4	0
na	na	k7c4	na
100	[number]	k4	100
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
pak	pak	k6eAd1	pak
auto	auto	k1gNnSc1	auto
zvládne	zvládnout	k5eAaPmIp3nS	zvládnout
pod	pod	k7c4	pod
6	[number]	k4	6
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tesla	Tesla	k1gFnSc1	Tesla
Semi	Sem	k1gFnSc2	Sem
===	===	k?	===
</s>
</p>
<p>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
Semi	Semi	k1gNnSc2	Semi
je	být	k5eAaImIp3nS	být
prototyp	prototyp	k1gInSc1	prototyp
tahače	tahač	k1gInSc2	tahač
třídy	třída	k1gFnSc2	třída
8	[number]	k4	8
s	s	k7c7	s
elektrickým	elektrický	k2eAgInSc7d1	elektrický
pohonem	pohon	k1gInSc7	pohon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
odhalen	odhalit	k5eAaPmNgInS	odhalit
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2017	[number]	k4	2017
firmou	firma	k1gFnSc7	firma
Tesla	Tesla	k1gFnSc1	Tesla
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
SpaceX	SpaceX	k1gFnSc2	SpaceX
v	v	k7c6	v
kalifornském	kalifornský	k2eAgNnSc6d1	kalifornské
Hawthorne	Hawthorn	k1gInSc5	Hawthorn
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
výroba	výroba	k1gFnSc1	výroba
je	být	k5eAaImIp3nS	být
naplánovaná	naplánovaný	k2eAgFnSc1d1	naplánovaná
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technologie	technologie	k1gFnSc2	technologie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Baterie	baterie	k1gFnPc1	baterie
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgMnPc2d1	jiný
výrobců	výrobce	k1gMnPc2	výrobce
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
Tesla	Tesla	k1gFnSc1	Tesla
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
autech	aut	k1gInPc6	aut
nepoužívá	používat	k5eNaImIp3nS	používat
jednoúčelové	jednoúčelový	k2eAgNnSc1d1	jednoúčelové
<g/>
,	,	kIx,	,
velké	velký	k2eAgInPc1d1	velký
bateriové	bateriový	k2eAgInPc1d1	bateriový
články	článek	k1gInPc1	článek
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
nich	on	k3xPp3gNnPc2	on
auta	auto	k1gNnSc2	auto
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
tisíce	tisíc	k4xCgInPc1	tisíc
malých	malý	k1gMnPc2	malý
lithium-iontových	lithiumontův	k2eAgInPc2d1	lithium-iontův
článků	článek	k1gInPc2	článek
podobných	podobný	k2eAgInPc2d1	podobný
těm	ten	k3xDgInPc3	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
v	v	k7c6	v
noteboocích	notebook	k1gInPc6	notebook
a	a	k8xC	a
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
spotřební	spotřební	k2eAgFnSc6d1	spotřební
elektronice	elektronika	k1gFnSc6	elektronika
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
autobaterie	autobaterie	k1gFnPc1	autobaterie
jsou	být	k5eAaImIp3nP	být
lehčí	lehký	k2eAgNnSc4d2	lehčí
a	a	k8xC	a
jednodušší	jednoduchý	k2eAgNnSc4d2	jednodušší
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
některé	některý	k3yIgInPc4	některý
bezpečnostní	bezpečnostní	k2eAgInPc4d1	bezpečnostní
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Tesly	Tesla	k1gFnSc2	Tesla
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
prvky	prvek	k1gInPc4	prvek
zbytečné	zbytečný	k2eAgNnSc1d1	zbytečné
díky	díky	k7c3	díky
pokročilému	pokročilý	k2eAgInSc3d1	pokročilý
rozvodu	rozvod	k1gInSc3	rozvod
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
protipožární	protipožární	k2eAgFnSc6d1	protipožární
chemikálii	chemikálie	k1gFnSc6	chemikálie
obsažené	obsažený	k2eAgFnSc6d1	obsažená
v	v	k7c6	v
baterii	baterie	k1gFnSc6	baterie
<g/>
.	.	kIx.	.
</s>
<s>
Dodavatelem	dodavatel	k1gMnSc7	dodavatel
baterií	baterie	k1gFnPc2	baterie
pro	pro	k7c4	pro
Teslu	Tesla	k1gMnSc4	Tesla
je	být	k5eAaImIp3nS	být
Panasonic	Panasonic	kA	Panasonic
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
vznikající	vznikající	k2eAgFnSc6d1	vznikající
továrně	továrna	k1gFnSc6	továrna
na	na	k7c4	na
baterie	baterie	k1gFnPc4	baterie
zvané	zvaný	k2eAgFnPc4d1	zvaná
Gigafactory	Gigafactor	k1gInPc4	Gigafactor
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
automobil	automobil	k1gInSc1	automobil
<g/>
,	,	kIx,	,
Tesla	Tesla	k1gFnSc1	Tesla
Roadster	roadster	k1gInSc1	roadster
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
baterie	baterie	k1gFnPc4	baterie
umístěné	umístěný	k2eAgFnPc1d1	umístěná
vzadu	vzadu	k6eAd1	vzadu
za	za	k7c7	za
sedadly	sedadlo	k1gNnPc7	sedadlo
<g/>
,	,	kIx,	,
Model	model	k1gInSc4	model
S	s	k7c7	s
<g/>
,	,	kIx,	,
Model	model	k1gInSc1	model
X	X	kA	X
i	i	k8xC	i
vznikající	vznikající	k2eAgInSc4d1	vznikající
Model	model	k1gInSc4	model
3	[number]	k4	3
mají	mít	k5eAaImIp3nP	mít
baterie	baterie	k1gFnPc1	baterie
uložené	uložený	k2eAgFnPc1d1	uložená
v	v	k7c6	v
podlaze	podlaha	k1gFnSc6	podlaha
pod	pod	k7c7	pod
kabinou	kabina	k1gFnSc7	kabina
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
řešení	řešení	k1gNnSc1	řešení
šetří	šetřit	k5eAaImIp3nS	šetřit
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
i	i	k8xC	i
zavazadlový	zavazadlový	k2eAgInSc4d1	zavazadlový
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
společně	společně	k6eAd1	společně
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
podvozkem	podvozek	k1gInSc7	podvozek
Modelu	model	k1gInSc2	model
S	s	k7c7	s
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
poškození	poškození	k1gNnSc2	poškození
baterie	baterie	k1gFnSc2	baterie
po	po	k7c6	po
nárazu	náraz	k1gInSc6	náraz
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
baterie	baterie	k1gFnSc2	baterie
Model	model	k1gInSc4	model
S	s	k7c7	s
používá	používat	k5eAaImIp3nS	používat
6	[number]	k4	6
mm	mm	kA	mm
silný	silný	k2eAgInSc4d1	silný
štít	štít	k1gInSc4	štít
z	z	k7c2	z
hliníkové	hliníkový	k2eAgFnSc2d1	hliníková
slitiny	slitina	k1gFnSc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Umístění	umístění	k1gNnSc1	umístění
baterie	baterie	k1gFnSc2	baterie
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
její	její	k3xOp3gFnSc4	její
rychlou	rychlý	k2eAgFnSc4d1	rychlá
výměnu	výměna	k1gFnSc4	výměna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
okolo	okolo	k7c2	okolo
90	[number]	k4	90
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
poslední	poslední	k2eAgFnSc1d1	poslední
stanice	stanice	k1gFnSc1	stanice
Tesly	Tesla	k1gFnSc2	Tesla
na	na	k7c4	na
výměnu	výměna	k1gFnSc4	výměna
baterií	baterie	k1gFnPc2	baterie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Harris	Harris	k1gFnSc6	Harris
Ranch	Rancha	k1gFnPc2	Rancha
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nízkému	nízký	k2eAgInSc3d1	nízký
zájmu	zájem	k1gInSc3	zájem
zákazníků	zákazník	k1gMnPc2	zákazník
se	se	k3xPyFc4	se
vyměňování	vyměňování	k1gNnSc1	vyměňování
baterií	baterie	k1gFnPc2	baterie
nebude	být	k5eNaImBp3nS	být
dále	daleko	k6eAd2	daleko
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Autopilot	autopilot	k1gMnSc1	autopilot
===	===	k?	===
</s>
</p>
<p>
<s>
Tesla	Tesla	k1gMnSc1	Tesla
autopilot	autopilot	k1gMnSc1	autopilot
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
poloautonomní	poloautonomní	k2eAgMnSc1d1	poloautonomní
asistent	asistent	k1gMnSc1	asistent
řízení	řízení	k1gNnSc2	řízení
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
vozech	vůz	k1gInPc6	vůz
Tesla	Tesla	k1gMnSc1	Tesla
<g/>
.	.	kIx.	.
</s>
<s>
Automobily	automobil	k1gInPc1	automobil
jsou	být	k5eAaImIp3nP	být
vybaveny	vybavit	k5eAaPmNgInP	vybavit
kamerami	kamera	k1gFnPc7	kamera
nad	nad	k7c7	nad
čelním	čelní	k2eAgNnSc7d1	čelní
sklem	sklo	k1gNnSc7	sklo
a	a	k8xC	a
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
vozu	vůz	k1gInSc2	vůz
<g/>
,	,	kIx,	,
radarem	radar	k1gInSc7	radar
<g/>
,	,	kIx,	,
a	a	k8xC	a
akustickými	akustický	k2eAgInPc7d1	akustický
senzory	senzor	k1gInPc7	senzor
nad	nad	k7c7	nad
předními	přední	k2eAgInPc7d1	přední
i	i	k8xC	i
zadními	zadní	k2eAgInPc7d1	zadní
nárazníky	nárazník	k1gInPc7	nárazník
<g/>
.	.	kIx.	.
</s>
<s>
Tohle	tenhle	k3xDgNnSc1	tenhle
vybavení	vybavení	k1gNnSc1	vybavení
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
automobilům	automobil	k1gInPc3	automobil
rozeznat	rozeznat	k5eAaPmF	rozeznat
dopravní	dopravní	k2eAgNnSc4d1	dopravní
značení	značení	k1gNnSc4	značení
<g/>
,	,	kIx,	,
jízdní	jízdní	k2eAgInPc4d1	jízdní
pruhy	pruh	k1gInPc4	pruh
<g/>
,	,	kIx,	,
překážky	překážka	k1gFnPc4	překážka
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
a	a	k8xC	a
ostatní	ostatní	k2eAgNnPc1d1	ostatní
vozidla	vozidlo	k1gNnPc1	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
první	první	k4xOgFnSc3	první
smrtelné	smrtelný	k2eAgFnSc3d1	smrtelná
nehodě	nehoda	k1gFnSc3	nehoda
při	při	k7c6	při
zapnuté	zapnutý	k2eAgFnSc6d1	zapnutá
funkci	funkce	k1gFnSc6	funkce
autopilota	autopilot	k1gMnSc2	autopilot
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
miliónech	milión	k4xCgInPc6	milión
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Automobilka	automobilka	k1gFnSc1	automobilka
byla	být	k5eAaImAgFnS	být
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokročilého	pokročilý	k2eAgMnSc2d1	pokročilý
asistenta	asistent	k1gMnSc2	asistent
řízení	řízení	k1gNnSc2	řízení
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k8xS	jako
autopilota	autopilot	k1gMnSc2	autopilot
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
některé	některý	k3yIgMnPc4	některý
lidi	člověk	k1gMnPc4	člověk
vést	vést	k5eAaImF	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
aktivaci	aktivace	k1gFnSc6	aktivace
přestanou	přestat	k5eAaPmIp3nP	přestat
věnovat	věnovat	k5eAaImF	věnovat
řízení	řízení	k1gNnSc4	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
však	však	k9	však
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
autopilot	autopilot	k1gMnSc1	autopilot
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
doplňkem	doplněk	k1gInSc7	doplněk
a	a	k8xC	a
řidič	řidič	k1gMnSc1	řidič
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
během	během	k7c2	během
jízdy	jízda	k1gFnSc2	jízda
věnovat	věnovat	k5eAaPmF	věnovat
řízení	řízení	k1gNnSc4	řízení
a	a	k8xC	a
udržovat	udržovat	k5eAaImF	udržovat
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
funkce	funkce	k1gFnPc1	funkce
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
omezeny	omezit	k5eAaPmNgFnP	omezit
především	především	k9	především
na	na	k7c4	na
jízdu	jízda	k1gFnSc4	jízda
po	po	k7c6	po
dálnici	dálnice	k1gFnSc6	dálnice
a	a	k8xC	a
na	na	k7c4	na
jízdu	jízda	k1gFnSc4	jízda
po	po	k7c6	po
okresních	okresní	k2eAgFnPc6d1	okresní
silnicích	silnice	k1gFnPc6	silnice
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
chvílích	chvíle	k1gFnPc6	chvíle
musí	muset	k5eAaImIp3nS	muset
řízení	řízení	k1gNnSc1	řízení
převzít	převzít	k5eAaPmF	převzít
řidič	řidič	k1gMnSc1	řidič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
všechny	všechen	k3xTgInPc1	všechen
nové	nový	k2eAgInPc1d1	nový
automobily	automobil	k1gInPc1	automobil
Tesla	Tesla	k1gFnSc1	Tesla
vybaveny	vybavit	k5eAaPmNgInP	vybavit
hardwarem	hardware	k1gInSc7	hardware
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
úplné	úplný	k2eAgNnSc4d1	úplné
autonomní	autonomní	k2eAgNnSc4d1	autonomní
řízení	řízení	k1gNnSc4	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Prozatím	prozatím	k6eAd1	prozatím
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
dostupný	dostupný	k2eAgMnSc1d1	dostupný
jen	jen	k9	jen
asistent	asistent	k1gMnSc1	asistent
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jsou	být	k5eAaImIp3nP	být
sbírána	sbírán	k2eAgNnPc1d1	sbíráno
data	datum	k1gNnPc1	datum
o	o	k7c6	o
jízdách	jízda	k1gFnPc6	jízda
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zlepšit	zlepšit	k5eAaPmF	zlepšit
funkci	funkce	k1gFnSc4	funkce
na	na	k7c4	na
takovou	takový	k3xDgFnSc4	takový
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
softwarově	softwarově	k6eAd1	softwarově
zpřístupněna	zpřístupněn	k2eAgFnSc1d1	zpřístupněna
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
vozech	vůz	k1gInPc6	vůz
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
bude	být	k5eAaImBp3nS	být
odladěná	odladěný	k2eAgFnSc1d1	odladěná
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
to	ten	k3xDgNnSc1	ten
umožní	umožnit	k5eAaPmIp3nS	umožnit
legislativa	legislativa	k1gFnSc1	legislativa
<g/>
.	.	kIx.	.
</s>
<s>
Automobilka	automobilka	k1gFnSc1	automobilka
očekává	očekávat	k5eAaImIp3nS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpřístupní	zpřístupnit	k5eAaPmIp3nS	zpřístupnit
plně	plně	k6eAd1	plně
autonomní	autonomní	k2eAgNnSc1d1	autonomní
řízení	řízení	k1gNnSc1	řízení
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Baterie	baterie	k1gFnPc1	baterie
pro	pro	k7c4	pro
domácnost	domácnost	k1gFnSc4	domácnost
===	===	k?	===
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
Tesla	Tesla	k1gFnSc1	Tesla
Motors	Motors	k1gInSc1	Motors
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
uvedla	uvést	k5eAaPmAgFnS	uvést
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2015	[number]	k4	2015
na	na	k7c4	na
trh	trh	k1gInSc4	trh
baterie	baterie	k1gFnSc1	baterie
pro	pro	k7c4	pro
domácnost	domácnost	k1gFnSc4	domácnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
Tesla	Tesla	k1gMnSc1	Tesla
Powerwall	Powerwall	k1gMnSc1	Powerwall
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
produkt	produkt	k1gInSc4	produkt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
výrazně	výrazně	k6eAd1	výrazně
překračuje	překračovat	k5eAaImIp3nS	překračovat
hranice	hranice	k1gFnSc1	hranice
automobilového	automobilový	k2eAgInSc2d1	automobilový
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
přesto	přesto	k8xC	přesto
s	s	k7c7	s
automobilkou	automobilka	k1gFnSc7	automobilka
velice	velice	k6eAd1	velice
souvisí	souviset	k5eAaImIp3nS	souviset
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
se	se	k3xPyFc4	se
Tesla	Tesla	k1gFnSc1	Tesla
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
výrazně	výrazně	k6eAd1	výrazně
odchýlila	odchýlit	k5eAaPmAgFnS	odchýlit
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
zaměření	zaměření	k1gNnSc2	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Baterie	baterie	k1gFnPc1	baterie
pro	pro	k7c4	pro
domácnost	domácnost	k1gFnSc4	domácnost
měly	mít	k5eAaImAgFnP	mít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
uvedení	uvedení	k1gNnSc2	uvedení
na	na	k7c4	na
trh	trh	k1gInSc4	trh
kapacitu	kapacita	k1gFnSc4	kapacita
7	[number]	k4	7
kWh	kwh	kA	kwh
nebo	nebo	k8xC	nebo
10	[number]	k4	10
kWh	kwh	kA	kwh
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
Tesla	Tesla	k1gFnSc1	Tesla
nabízí	nabízet	k5eAaImIp3nS	nabízet
vylepšený	vylepšený	k2eAgInSc4d1	vylepšený
model	model	k1gInSc4	model
o	o	k7c6	o
kapacitě	kapacita	k1gFnSc6	kapacita
13,5	[number]	k4	13,5
kWh	kwh	kA	kwh
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
domácí	domácí	k2eAgFnPc1d1	domácí
baterie	baterie	k1gFnPc1	baterie
jsou	být	k5eAaImIp3nP	být
vyráběny	vyrábět	k5eAaImNgFnP	vyrábět
společně	společně	k6eAd1	společně
s	s	k7c7	s
akumulátory	akumulátor	k1gInPc7	akumulátor
pro	pro	k7c4	pro
automobily	automobil	k1gInPc4	automobil
v	v	k7c6	v
průmyslovém	průmyslový	k2eAgInSc6d1	průmyslový
objektu	objekt	k1gInSc6	objekt
v	v	k7c6	v
Nevadě	Nevada	k1gFnSc6	Nevada
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
baterií	baterie	k1gFnSc7	baterie
Tesla	Tesla	k1gFnSc1	Tesla
Powerwall	Powerwall	k1gMnSc1	Powerwall
byla	být	k5eAaImAgFnS	být
odhalena	odhalit	k5eAaPmNgFnS	odhalit
i	i	k9	i
baterie	baterie	k1gFnSc1	baterie
Tesla	Tesla	k1gFnSc1	Tesla
Powerpack	Powerpack	k1gInSc4	Powerpack
sloužící	sloužící	k1gFnSc2	sloužící
k	k	k7c3	k
napájení	napájení	k1gNnSc3	napájení
velkých	velký	k2eAgInPc2d1	velký
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
pro	pro	k7c4	pro
energetické	energetický	k2eAgFnPc4d1	energetická
společnosti	společnost	k1gFnPc4	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Solární	solární	k2eAgFnPc1d1	solární
střešní	střešní	k2eAgFnPc1d1	střešní
tašky	taška	k1gFnPc1	taška
===	===	k?	===
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
Tesla	Tesla	k1gFnSc1	Tesla
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
uvedla	uvést	k5eAaPmAgFnS	uvést
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2017	[number]	k4	2017
na	na	k7c4	na
trh	trh	k1gInSc4	trh
solární	solární	k2eAgFnSc2d1	solární
střešní	střešní	k2eAgFnSc2d1	střešní
tašky	taška	k1gFnSc2	taška
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc4	první
dvě	dva	k4xCgNnPc4	dva
provedení	provedení	k1gNnPc2	provedení
doplní	doplnit	k5eAaPmIp3nS	doplnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
další	další	k1gNnSc1	další
dvě	dva	k4xCgNnPc1	dva
<g/>
.	.	kIx.	.
</s>
<s>
Cenově	cenově	k6eAd1	cenově
i	i	k9	i
vizuálně	vizuálně	k6eAd1	vizuálně
konkurují	konkurovat	k5eAaImIp3nP	konkurovat
konvenčním	konvenční	k2eAgFnPc3d1	konvenční
krytinám	krytina	k1gFnPc3	krytina
<g/>
,	,	kIx,	,
generují	generovat	k5eAaImIp3nP	generovat
navíc	navíc	k6eAd1	navíc
elektřinu	elektřina	k1gFnSc4	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Tašky	taška	k1gFnPc1	taška
na	na	k7c6	na
vhodně	vhodně	k6eAd1	vhodně
nasvícených	nasvícený	k2eAgNnPc6d1	nasvícené
místech	místo	k1gNnPc6	místo
střechy	střecha	k1gFnSc2	střecha
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
solární	solární	k2eAgInSc4d1	solární
panel	panel	k1gInSc4	panel
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnSc4d1	ostatní
elektřinu	elektřina	k1gFnSc4	elektřina
negenerují	generovat	k5eNaImIp3nP	generovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
úrovně	úroveň	k1gFnSc2	úroveň
ulice	ulice	k1gFnSc2	ulice
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
úhlovému	úhlový	k2eAgInSc3d1	úhlový
filtru	filtr	k1gInSc3	filtr
nelze	lze	k6eNd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
tak	tak	k6eAd1	tak
moderním	moderní	k2eAgInSc7d1	moderní
a	a	k8xC	a
čistým	čistý	k2eAgInSc7d1	čistý
dojmem	dojem	k1gInSc7	dojem
<g/>
.	.	kIx.	.
</s>
<s>
Nasedají	nasedat	k5eAaImIp3nP	nasedat
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
zámky	zámek	k1gInPc4	zámek
s	s	k7c7	s
konektorem	konektor	k1gInSc7	konektor
pro	pro	k7c4	pro
rychlou	rychlý	k2eAgFnSc4d1	rychlá
instalaci	instalace	k1gFnSc4	instalace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Tesla	Tesla	k1gFnSc1	Tesla
pokrývačům	pokrývač	k1gMnPc3	pokrývač
trvá	trvat	k5eAaImIp3nS	trvat
5-7	[number]	k4	5-7
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
střechy	střecha	k1gFnSc2	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
demonstrovala	demonstrovat	k5eAaBmAgFnS	demonstrovat
extrémní	extrémní	k2eAgFnSc4d1	extrémní
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
kroupám	kroupa	k1gFnPc3	kroupa
a	a	k8xC	a
jiným	jiný	k2eAgInPc3d1	jiný
projektilům	projektil	k1gInPc3	projektil
<g/>
.	.	kIx.	.
</s>
<s>
Tašku	taška	k1gFnSc4	taška
kryje	krýt	k5eAaImIp3nS	krýt
vrstva	vrstva	k1gFnSc1	vrstva
tvrzeného	tvrzený	k2eAgNnSc2d1	tvrzené
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
časově	časově	k6eAd1	časově
neomezenou	omezený	k2eNgFnSc4d1	neomezená
záruku	záruka	k1gFnSc4	záruka
na	na	k7c4	na
funkčnost	funkčnost	k1gFnSc4	funkčnost
krytiny	krytina	k1gFnSc2	krytina
a	a	k8xC	a
30	[number]	k4	30
let	léto	k1gNnPc2	léto
záruku	záruka	k1gFnSc4	záruka
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
schopnost	schopnost	k1gFnSc4	schopnost
generovat	generovat	k5eAaImF	generovat
elektřinu	elektřina	k1gFnSc4	elektřina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
se	se	k3xPyFc4	se
tak	tak	k9	tak
dále	daleko	k6eAd2	daleko
profiluje	profilovat	k5eAaImIp3nS	profilovat
jako	jako	k9	jako
obecně	obecně	k6eAd1	obecně
energetická	energetický	k2eAgFnSc1d1	energetická
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yQgFnSc4	který
jsou	být	k5eAaImIp3nP	být
automobily	automobil	k1gInPc1	automobil
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
kategorie	kategorie	k1gFnSc1	kategorie
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rychlonabíjecí	Rychlonabíjecí	k2eAgFnSc2d1	Rychlonabíjecí
stanice	stanice	k1gFnSc2	stanice
Supercharger	Supercharger	k1gMnSc1	Supercharger
===	===	k?	===
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nP	aby
Tesla	Tesla	k1gFnSc1	Tesla
umožnila	umožnit	k5eAaPmAgFnS	umožnit
rychlé	rychlý	k2eAgNnSc4d1	rychlé
dobíjení	dobíjení	k1gNnSc4	dobíjení
svých	svůj	k3xOyFgInPc2	svůj
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
budovat	budovat	k5eAaImF	budovat
síť	síť	k1gFnSc4	síť
rychlonabíjecích	rychlonabíjecí	k2eAgFnPc2d1	rychlonabíjecí
stanic	stanice	k1gFnPc2	stanice
zvaných	zvaný	k2eAgFnPc2d1	zvaná
Supercharger	Superchargra	k1gFnPc2	Superchargra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2016	[number]	k4	2016
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
celosvětově	celosvětově	k6eAd1	celosvětově
719	[number]	k4	719
stanic	stanice	k1gFnPc2	stanice
s	s	k7c7	s
4428	[number]	k4	4428
nabíjecími	nabíjecí	k2eAgNnPc7d1	nabíjecí
místy	místo	k1gNnPc7	místo
<g/>
.	.	kIx.	.
</s>
<s>
Rychlonabíjecí	Rychlonabíjecí	k2eAgFnSc1d1	Rychlonabíjecí
stanice	stanice	k1gFnSc1	stanice
zvládne	zvládnout	k5eAaPmIp3nS	zvládnout
nabít	nabít	k5eAaPmF	nabít
90	[number]	k4	90
kWh	kwh	kA	kwh
Model	model	k1gInSc1	model
S	s	k7c7	s
během	běh	k1gInSc7	běh
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
na	na	k7c4	na
dalších	další	k2eAgFnPc2d1	další
270	[number]	k4	270
km	km	kA	km
jízdy	jízda	k1gFnSc2	jízda
a	a	k8xC	a
plné	plný	k2eAgNnSc4d1	plné
dobití	dobití	k1gNnSc4	dobití
automobilu	automobil	k1gInSc2	automobil
trvá	trvat	k5eAaImIp3nS	trvat
75	[number]	k4	75
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
dosud	dosud	k6eAd1	dosud
vyrobené	vyrobený	k2eAgInPc1d1	vyrobený
automobily	automobil	k1gInPc1	automobil
Tesla	Tesla	k1gFnSc1	Tesla
mají	mít	k5eAaImIp3nP	mít
rychlonabíjení	rychlonabíjení	k1gNnPc1	rychlonabíjení
u	u	k7c2	u
Superchargeru	Supercharger	k1gInSc2	Supercharger
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Auta	auto	k1gNnPc1	auto
objednaná	objednaný	k2eAgNnPc1d1	objednané
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
dostanou	dostat	k5eAaPmIp3nP	dostat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
zdarma	zdarma	k6eAd1	zdarma
400	[number]	k4	400
kWh	kwh	kA	kwh
kredit	kredit	k1gInSc1	kredit
pro	pro	k7c4	pro
dobíjení	dobíjení	k1gNnSc4	dobíjení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
zhruba	zhruba	k6eAd1	zhruba
energii	energie	k1gFnSc4	energie
na	na	k7c6	na
ujetí	ujetí	k1gNnSc6	ujetí
1600	[number]	k4	1600
km	km	kA	km
(	(	kIx(	(
<g/>
1000	[number]	k4	1000
mil	míle	k1gFnPc2	míle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
další	další	k2eAgNnSc4d1	další
nabíjení	nabíjení	k1gNnSc4	nabíjení
nad	nad	k7c4	nad
rámec	rámec	k1gInSc4	rámec
poskytnutého	poskytnutý	k2eAgInSc2d1	poskytnutý
kreditu	kredit	k1gInSc2	kredit
bude	být	k5eAaImBp3nS	být
účtován	účtován	k2eAgInSc1d1	účtován
dosud	dosud	k6eAd1	dosud
neupřesněný	upřesněný	k2eNgInSc1d1	neupřesněný
poplatek	poplatek	k1gInSc1	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Auta	auto	k1gNnPc1	auto
objednaná	objednaný	k2eAgNnPc1d1	objednané
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
stále	stále	k6eAd1	stále
užívání	užívání	k1gNnSc3	užívání
firemních	firemní	k2eAgFnPc2d1	firemní
rychlonabíjecích	rychlonabíjecí	k2eAgFnPc2d1	rychlonabíjecí
stanic	stanice	k1gFnPc2	stanice
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
otevřen	otevřít	k5eAaPmNgInS	otevřít
první	první	k4xOgInSc1	první
Supercharger	Supercharger	k1gInSc1	Supercharger
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
u	u	k7c2	u
dálnice	dálnice	k1gFnSc2	dálnice
D1	D1	k1gFnSc2	D1
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Humpolce	Humpolec	k1gInSc2	Humpolec
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
rychlonabíjecí	rychlonabíjecí	k2eAgFnPc1d1	rychlonabíjecí
stanice	stanice	k1gFnPc1	stanice
Supercharger	Superchargra	k1gFnPc2	Superchargra
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
u	u	k7c2	u
Olomouce	Olomouc	k1gFnSc2	Olomouc
a	a	k8xC	a
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Vestec	Vestec	k1gInSc4	Vestec
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
v	v	k7c6	v
ČR	ČR	kA	ČR
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
22	[number]	k4	22
stojanů	stojan	k1gInPc2	stojan
Tesla	Tesla	k1gFnSc1	Tesla
Supercharger	Supercharger	k1gInSc1	Supercharger
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sdílení	sdílení	k1gNnPc1	sdílení
technologií	technologie	k1gFnPc2	technologie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2014	[number]	k4	2014
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tesla	Tesla	k1gFnSc1	Tesla
v	v	k7c6	v
dobré	dobrý	k2eAgFnSc6d1	dobrá
víře	víra	k1gFnSc6	víra
umožní	umožnit	k5eAaPmIp3nP	umožnit
komukoliv	kdokoliv	k3yInSc3	kdokoliv
užívat	užívat	k5eAaImF	užívat
její	její	k3xOp3gInPc1	její
technologické	technologický	k2eAgInPc1d1	technologický
patenty	patent	k1gInPc1	patent
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
především	především	k6eAd1	především
motivovat	motivovat	k5eAaBmF	motivovat
talentované	talentovaný	k2eAgMnPc4d1	talentovaný
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
a	a	k8xC	a
urychlit	urychlit	k5eAaPmF	urychlit
pokrok	pokrok	k1gInSc4	pokrok
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
s	s	k7c7	s
elektromobily	elektromobil	k1gInPc7	elektromobil
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Realita	realita	k1gFnSc1	realita
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
produkce	produkce	k1gFnSc1	produkce
elektromobilů	elektromobil	k1gInPc2	elektromobil
u	u	k7c2	u
hlavních	hlavní	k2eAgMnPc2d1	hlavní
výrobců	výrobce	k1gMnPc2	výrobce
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
až	až	k9	až
neexistující	existující	k2eNgFnSc2d1	neexistující
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
méně	málo	k6eAd2	málo
než	než	k8xS	než
1	[number]	k4	1
<g/>
%	%	kIx~	%
jejich	jejich	k3xOp3gInPc2	jejich
celkových	celkový	k2eAgInPc2d1	celkový
prodejů	prodej	k1gInPc2	prodej
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
dodal	dodat	k5eAaPmAgMnS	dodat
Musk	Musk	k1gMnSc1	Musk
<g/>
.	.	kIx.	.
</s>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
zůstane	zůstat	k5eAaPmIp3nS	zůstat
stále	stále	k6eAd1	stále
držitelem	držitel	k1gMnSc7	držitel
jiného	jiný	k2eAgNnSc2d1	jiné
duševního	duševní	k2eAgNnSc2d1	duševní
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
ochranné	ochranný	k2eAgFnPc4d1	ochranná
známky	známka	k1gFnPc4	známka
a	a	k8xC	a
obchodní	obchodní	k2eAgNnSc4d1	obchodní
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zabrání	zabránit	k5eAaPmIp3nS	zabránit
přímému	přímý	k2eAgNnSc3d1	přímé
kopírování	kopírování	k1gNnSc3	kopírování
automobilů	automobil	k1gInPc2	automobil
značky	značka	k1gFnSc2	značka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výrobní	výrobní	k2eAgNnPc1d1	výrobní
zařízení	zařízení	k1gNnPc1	zařízení
a	a	k8xC	a
prodejny	prodejna	k1gFnPc1	prodejna
==	==	k?	==
</s>
</p>
<p>
<s>
Centrála	centrála	k1gFnSc1	centrála
firmy	firma	k1gFnSc2	firma
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Palo	Pala	k1gMnSc5	Pala
Alto	Alto	k1gNnSc1	Alto
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
měla	mít	k5eAaImAgFnS	mít
Tesla	Tesla	k1gFnSc1	Tesla
pobočky	pobočka	k1gFnSc2	pobočka
ve	v	k7c6	v
21	[number]	k4	21
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
USA	USA	kA	USA
===	===	k?	===
</s>
</p>
<p>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
San	San	k1gFnSc6	San
Carlos	Carlos	k1gMnSc1	Carlos
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
společnost	společnost	k1gFnSc1	společnost
otevřela	otevřít	k5eAaPmAgFnS	otevřít
své	svůj	k3xOyFgFnPc4	svůj
kanceláře	kancelář	k1gFnPc4	kancelář
v	v	k7c4	v
Rochester	Rochester	k1gInSc4	Rochester
Hills	Hillsa	k1gFnPc2	Hillsa
v	v	k7c6	v
Michiganu	Michigan	k1gInSc6	Michigan
<g/>
.	.	kIx.	.
</s>
<s>
Kanceláře	kancelář	k1gFnPc1	kancelář
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
ztrátovosti	ztrátovost	k1gFnSc2	ztrátovost
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
do	do	k7c2	do
menší	malý	k2eAgFnSc2d2	menší
kanceláře	kancelář	k1gFnSc2	kancelář
v	v	k7c4	v
Auburn	Auburn	k1gNnSc4	Auburn
Hills	Hillsa	k1gFnPc2	Hillsa
v	v	k7c6	v
Michiganu	Michigan	k1gInSc6	Michigan
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
většina	většina	k1gFnSc1	většina
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
zpočátku	zpočátku	k6eAd1	zpočátku
otevřela	otevřít	k5eAaPmAgFnS	otevřít
tyto	tento	k3xDgFnPc4	tento
prodejny	prodejna	k1gFnPc4	prodejna
<g/>
:	:	kIx,	:
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
v	v	k7c4	v
Menlo	Menlo	k1gNnSc4	Menlo
Park	park	k1gInSc1	park
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2008	[number]	k4	2008
a	a	k8xC	a
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
Tesla	Tesla	k1gFnSc1	Tesla
přesunula	přesunout	k5eAaPmAgFnS	přesunout
svou	svůj	k3xOyFgFnSc4	svůj
centrálu	centrála	k1gFnSc4	centrála
do	do	k7c2	do
nového	nový	k2eAgNnSc2d1	nové
vývojového	vývojový	k2eAgNnSc2d1	vývojové
střediska	středisko	k1gNnSc2	středisko
v	v	k7c6	v
Palo	Pala	k1gMnSc5	Pala
Alto	Alto	k1gNnSc1	Alto
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Automobily	automobil	k1gInPc1	automobil
společnosti	společnost	k1gFnSc2	společnost
jsou	být	k5eAaImIp3nP	být
vyráběny	vyrábět	k5eAaImNgFnP	vyrábět
ve	v	k7c6	v
Fremontu	Fremont	k1gInSc6	Fremont
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Gigafactory	Gigafactor	k1gInPc4	Gigafactor
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
Nevadě	Nevada	k1gFnSc6	Nevada
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
také	také	k9	také
obří	obří	k2eAgFnSc1d1	obří
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
baterie	baterie	k1gFnPc4	baterie
pro	pro	k7c4	pro
elektromobily	elektromobil	k1gInPc4	elektromobil
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Gigafactory	Gigafactor	k1gInPc4	Gigafactor
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
začnou	začít	k5eAaPmIp3nP	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
lithium-iontové	lithiumontový	k2eAgInPc4d1	lithium-iontový
akumulátory	akumulátor	k1gInPc4	akumulátor
pro	pro	k7c4	pro
automobil	automobil	k1gInSc4	automobil
Tesla	Tesla	k1gFnSc1	Tesla
Model	model	k1gInSc1	model
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
tvrzení	tvrzení	k1gNnSc2	tvrzení
firmy	firma	k1gFnSc2	firma
bude	být	k5eAaImBp3nS	být
továrna	továrna	k1gFnSc1	továrna
zaměstnávat	zaměstnávat	k5eAaImF	zaměstnávat
okolo	okolo	k7c2	okolo
6500	[number]	k4	6500
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
masová	masový	k2eAgFnSc1d1	masová
produkce	produkce	k1gFnSc1	produkce
baterií	baterie	k1gFnPc2	baterie
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Panasonic	Panasonic	kA	Panasonic
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgFnS	mít
snížit	snížit	k5eAaPmF	snížit
jejich	jejich	k3xOp3gFnSc4	jejich
cenu	cena	k1gFnSc4	cena
asi	asi	k9	asi
o	o	k7c4	o
30	[number]	k4	30
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
a	a	k8xC	a
dosáhnutí	dosáhnutý	k2eAgMnPc1d1	dosáhnutý
maximální	maximální	k2eAgMnSc1d1	maximální
výroby	výroba	k1gFnSc2	výroba
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
továrna	továrna	k1gFnSc1	továrna
produkovat	produkovat	k5eAaImF	produkovat
takové	takový	k3xDgNnSc4	takový
množství	množství	k1gNnSc4	množství
baterií	baterie	k1gFnPc2	baterie
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jejich	jejich	k3xOp3gFnSc3	jejich
celosvětové	celosvětový	k2eAgFnSc3d1	celosvětová
produkci	produkce	k1gFnSc3	produkce
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
Tesla	Tesla	k1gMnSc1	Tesla
Powerwall	Powerwall	k1gMnSc1	Powerwall
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dokončení	dokončení	k1gNnSc4	dokončení
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
budovu	budova	k1gFnSc4	budova
na	na	k7c6	na
světě	svět	k1gInSc6	svět
dle	dle	k7c2	dle
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kanada	Kanada	k1gFnSc1	Kanada
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
prodejna	prodejna	k1gFnSc1	prodejna
Tesly	Tesla	k1gFnSc2	Tesla
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2012	[number]	k4	2012
v	v	k7c6	v
obchodním	obchodní	k2eAgNnSc6d1	obchodní
centru	centrum	k1gNnSc6	centrum
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgFnSc4	první
prodejnu	prodejna	k1gFnSc4	prodejna
nové	nový	k2eAgFnSc2d1	nová
generace	generace	k1gFnSc2	generace
s	s	k7c7	s
interaktivními	interaktivní	k2eAgInPc7d1	interaktivní
displeji	displej	k1gInPc7	displej
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zákazníkům	zákazník	k1gMnPc3	zákazník
umožnily	umožnit	k5eAaPmAgFnP	umožnit
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
výbavu	výbava	k1gFnSc4	výbava
automobilu	automobil	k1gInSc2	automobil
a	a	k8xC	a
zobrazit	zobrazit	k5eAaPmF	zobrazit
výslednou	výsledný	k2eAgFnSc4d1	výsledná
konfiguraci	konfigurace	k1gFnSc4	konfigurace
na	na	k7c4	na
85	[number]	k4	85
palcové	palcový	k2eAgFnSc2d1	palcová
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
8	[number]	k4	8
prodejen	prodejna	k1gFnPc2	prodejna
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
<g/>
,	,	kIx,	,
Quebec	Quebec	k1gMnSc1	Quebec
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
Calgary	Calgary	k1gNnSc6	Calgary
<g/>
,	,	kIx,	,
Torontu	Toronto	k1gNnSc6	Toronto
a	a	k8xC	a
Vancouveru	Vancouver	k1gInSc6	Vancouver
</s>
</p>
<p>
<s>
===	===	k?	===
Evropa	Evropa	k1gFnSc1	Evropa
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
první	první	k4xOgFnSc1	první
evropská	evropský	k2eAgFnSc1d1	Evropská
prodejna	prodejna	k1gFnSc1	prodejna
automobilky	automobilka	k1gFnSc2	automobilka
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
následovala	následovat	k5eAaImAgFnS	následovat
prodejna	prodejna	k1gFnSc1	prodejna
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
v	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
centrála	centrála	k1gFnSc1	centrála
firmy	firma	k1gFnSc2	firma
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Tilburgu	Tilburg	k1gInSc6	Tilburg
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
evropské	evropský	k2eAgNnSc1d1	Evropské
distribuční	distribuční	k2eAgNnSc1d1	distribuční
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
montují	montovat	k5eAaImIp3nP	montovat
automobily	automobil	k1gInPc1	automobil
pro	pro	k7c4	pro
evropský	evropský	k2eAgInSc4d1	evropský
trh	trh	k1gInSc4	trh
z	z	k7c2	z
dovezených	dovezený	k2eAgInPc2d1	dovezený
dílů	díl	k1gInPc2	díl
z	z	k7c2	z
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Asie	Asie	k1gFnSc2	Asie
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
asijská	asijský	k2eAgFnSc1d1	asijská
prodejna	prodejna	k1gFnSc1	prodejna
Tesly	Tesla	k1gFnSc2	Tesla
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
pak	pak	k9	pak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
první	první	k4xOgFnSc1	první
prodejna	prodejna	k1gFnSc1	prodejna
v	v	k7c4	v
Hong	Hong	k1gInSc4	Hong
Kongu	Kongo	k1gNnSc3	Kongo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
do	do	k7c2	do
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
existovala	existovat	k5eAaImAgFnS	existovat
prodejna	prodejna	k1gFnSc1	prodejna
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
Singapuru	Singapur	k1gInSc6	Singapur
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
provoz	provoz	k1gInSc1	provoz
byl	být	k5eAaImAgInS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedostatku	nedostatek	k1gInSc2	nedostatek
tamních	tamní	k2eAgFnPc2d1	tamní
daňových	daňový	k2eAgFnPc2d1	daňová
úlev	úleva	k1gFnPc2	úleva
pro	pro	k7c4	pro
elektromobily	elektromobila	k1gFnPc4	elektromobila
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
první	první	k4xOgFnSc1	první
pobočka	pobočka	k1gFnSc1	pobočka
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Austrálie	Austrálie	k1gFnSc2	Austrálie
===	===	k?	===
</s>
</p>
<p>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
otevřela	otevřít	k5eAaPmAgFnS	otevřít
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
australskou	australský	k2eAgFnSc4d1	australská
pobočku	pobočka	k1gFnSc4	pobočka
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
4	[number]	k4	4
prodejny	prodejna	k1gFnSc2	prodejna
<g/>
,	,	kIx,	,
2	[number]	k4	2
v	v	k7c4	v
Sydney	Sydney	k1gNnSc4	Sydney
a	a	k8xC	a
2	[number]	k4	2
v	v	k7c6	v
Melbourne	Melbourne	k1gNnSc6	Melbourne
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
</s>
</p>
<p>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
Model	modla	k1gFnPc2	modla
S	s	k7c7	s
</s>
</p>
<p>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
Model	model	k1gInSc1	model
X	X	kA	X
</s>
</p>
<p>
<s>
Gigafactory	Gigafactor	k1gInPc1	Gigafactor
1	[number]	k4	1
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tesla	Tesla	k1gFnSc1	Tesla
Motors	Motors	k1gInSc4	Motors
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Web	web	k1gInSc1	web
Tesla	Tesla	k1gFnSc1	Tesla
</s>
</p>
