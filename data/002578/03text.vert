<s>
Federico	Federico	k1gMnSc1	Federico
García	García	k1gMnSc1	García
Lorca	Lorca	k1gMnSc1	Lorca
[	[	kIx(	[
<g/>
Garsia	Garsia	k1gFnSc1	Garsia
Lorka	Lorka	k1gFnSc1	Lorka
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1898	[number]	k4	1898
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
španělský	španělský	k2eAgMnSc1d1	španělský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
.	.	kIx.	.
</s>
<s>
Zastřelen	zastřelen	k2eAgInSc1d1	zastřelen
frankisty	frankista	k1gMnPc7	frankista
v	v	k7c6	v
Granadě	Granada	k1gFnSc6	Granada
(	(	kIx(	(
<g/>
Víznaru	Víznar	k1gInSc6	Víznar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Granadě	Granada	k1gFnSc6	Granada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
,	,	kIx,	,
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
práva	právo	k1gNnPc1	právo
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
studiích	studio	k1gNnPc6	studio
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ohniskem	ohnisko	k1gNnSc7	ohnisko
umělecké	umělecký	k2eAgFnSc2d1	umělecká
avantgardy	avantgarda	k1gFnSc2	avantgarda
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
byl	být	k5eAaImAgMnS	být
homosexuál	homosexuál	k1gMnSc1	homosexuál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
1930	[number]	k4	1930
studoval	studovat	k5eAaImAgMnS	studovat
v	v	k7c6	v
USA	USA	kA	USA
na	na	k7c6	na
Kolumbijské	kolumbijský	k2eAgFnSc6d1	kolumbijská
universitě	universita	k1gFnSc6	universita
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
strávil	strávit	k5eAaPmAgMnS	strávit
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
1934	[number]	k4	1934
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Granady	Granada	k1gFnSc2	Granada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
r.	r.	kA	r.
1936	[number]	k4	1936
popraven	popravit	k5eAaPmNgInS	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
především	především	k9	především
andaluským	andaluský	k2eAgInSc7d1	andaluský
folklórem	folklór	k1gInSc7	folklór
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
patrný	patrný	k2eAgMnSc1d1	patrný
i	i	k8xC	i
vliv	vliv	k1gInSc1	vliv
surrealismu	surrealismus	k1gInSc2	surrealismus
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc3	jeho
kombinace	kombinace	k1gFnSc1	kombinace
avantgardních	avantgardní	k2eAgInPc2d1	avantgardní
prvků	prvek	k1gInPc2	prvek
s	s	k7c7	s
lidovou	lidový	k2eAgFnSc7d1	lidová
slovesností	slovesnost	k1gFnSc7	slovesnost
jižního	jižní	k2eAgNnSc2d1	jižní
Španělska	Španělsko	k1gNnSc2	Španělsko
-	-	kIx~	-
Andalusií	Andalusie	k1gFnPc2	Andalusie
je	být	k5eAaImIp3nS	být
ojedinělá	ojedinělý	k2eAgFnSc1d1	ojedinělá
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
inspirativní	inspirativní	k2eAgMnSc1d1	inspirativní
byl	být	k5eAaImAgInS	být
i	i	k9	i
jeho	on	k3xPp3gInSc4	on
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
Salvadoru	Salvador	k1gMnSc3	Salvador
Dalímu	Dalí	k1gMnSc3	Dalí
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgMnSc2	jenž
byl	být	k5eAaImAgInS	být
zamilován	zamilován	k2eAgInSc1d1	zamilován
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
jejich	jejich	k3xOp3gFnSc1	jejich
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
korespondence	korespondence	k1gFnSc1	korespondence
<g/>
.	.	kIx.	.
</s>
<s>
Zda	zda	k8xS	zda
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
cit	cit	k1gInSc1	cit
opětován	opětován	k2eAgInSc1d1	opětován
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
Salvador	Salvador	k1gMnSc1	Salvador
Dalí	Dalí	k1gMnSc1	Dalí
se	se	k3xPyFc4	se
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
tématu	téma	k1gNnSc3	téma
odmítal	odmítat	k5eAaImAgInS	odmítat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
a	a	k8xC	a
Lorcu	Lorca	k1gFnSc4	Lorca
označoval	označovat	k5eAaImAgInS	označovat
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
za	za	k7c4	za
přítele	přítel	k1gMnSc4	přítel
z	z	k7c2	z
mládí	mládí	k1gNnSc2	mládí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svého	svůj	k3xOyFgMnSc2	svůj
významného	významný	k2eAgMnSc2d1	významný
životopisce	životopisec	k1gMnSc2	životopisec
anglického	anglický	k2eAgMnSc2d1	anglický
historika	historik	k1gMnSc2	historik
Iana	Ianus	k1gMnSc4	Ianus
Gibsona	Gibson	k1gMnSc4	Gibson
byl	být	k5eAaImAgMnS	být
Lorca	Lorca	k1gMnSc1	Lorca
homoeroticky	homoeroticky	k6eAd1	homoeroticky
orientován	orientovat	k5eAaBmNgMnS	orientovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
básnické	básnický	k2eAgNnSc4d1	básnické
období	období	k1gNnSc4	období
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
období	období	k1gNnPc4	období
<g/>
:	:	kIx,	:
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
období	období	k1gNnSc6	období
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Lorcově	Lorcův	k2eAgFnSc6d1	Lorcova
tvorbě	tvorba	k1gFnSc6	tvorba
patrné	patrný	k2eAgNnSc1d1	patrné
působení	působení	k1gNnSc1	působení
rodného	rodný	k2eAgInSc2d1	rodný
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ho	on	k3xPp3gMnSc4	on
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
zejména	zejména	k9	zejména
svými	svůj	k3xOyFgNnPc7	svůj
nářečími	nářečí	k1gNnPc7	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
je	být	k5eAaImIp3nS	být
i	i	k9	i
značný	značný	k2eAgInSc1d1	značný
vliv	vliv	k1gInSc1	vliv
Juana	Juan	k1gMnSc2	Juan
Ramóna	Ramóna	k1gFnSc1	Ramóna
Jiméneze	Jiméneze	k1gFnSc1	Jiméneze
a	a	k8xC	a
Jorge	Jorge	k1gFnSc1	Jorge
Giulléna	Giulléno	k1gNnSc2	Giulléno
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
poesía	poesía	k1gMnSc1	poesía
pura	pura	k?	pura
<g/>
"	"	kIx"	"
-	-	kIx~	-
čistou	čistý	k2eAgFnSc7d1	čistá
poezií	poezie	k1gFnSc7	poezie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
též	též	k9	též
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
Ramónem	Ramón	k1gMnSc7	Ramón
Menéndezem	Menéndez	k1gInSc7	Menéndez
Pidalem	Pidal	k1gMnSc7	Pidal
<g/>
,	,	kIx,	,
převzal	převzít	k5eAaPmAgMnS	převzít
formu	forma	k1gFnSc4	forma
"	"	kIx"	"
<g/>
romance	romance	k1gFnSc1	romance
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Kniha	kniha	k1gFnSc1	kniha
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
ze	z	k7c2	z
stejné	stejný	k2eAgFnSc2d1	stejná
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
i	i	k9	i
Poema	poema	k1gFnSc1	poema
del	del	k?	del
cante	cant	k1gMnSc5	cant
jondo	jonda	k1gMnSc5	jonda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
převládá	převládat	k5eAaImIp3nS	převládat
metafora	metafora	k1gFnSc1	metafora
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
vliv	vliv	k1gInSc1	vliv
surrealistů	surrealista	k1gMnPc2	surrealista
<g/>
.	.	kIx.	.
</s>
<s>
Lorcův	Lorcův	k2eAgInSc1d1	Lorcův
příjezd	příjezd	k1gInSc1	příjezd
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
změnil	změnit	k5eAaPmAgMnS	změnit
jeho	on	k3xPp3gInSc4	on
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Přijal	přijmout	k5eAaPmAgMnS	přijmout
šokující	šokující	k2eAgFnSc4d1	šokující
formu	forma	k1gFnSc4	forma
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
silné	silný	k2eAgInPc4d1	silný
surrealistické	surrealistický	k2eAgInPc4d1	surrealistický
náměty	námět	k1gInPc4	námět
a	a	k8xC	a
obrazy	obraz	k1gInPc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
španělský	španělský	k2eAgInSc4d1	španělský
surrealismus	surrealismus	k1gInSc4	surrealismus
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
racionální	racionální	k2eAgFnSc1d1	racionální
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nepřijal	přijmout	k5eNaPmAgMnS	přijmout
automatické	automatický	k2eAgNnSc4d1	automatické
psaní	psaní	k1gNnSc4	psaní
(	(	kIx(	(
<g/>
André	André	k1gMnSc1	André
Breton	Breton	k1gMnSc1	Breton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
stěžejním	stěžejní	k2eAgNnSc7d1	stěžejní
dílem	dílo	k1gNnSc7	dílo
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
Poeta	poeta	k1gMnSc1	poeta
en	en	k?	en
Nueva	Nueva	k1gFnSc1	Nueva
York	York	k1gInSc1	York
-	-	kIx~	-
Básník	básník	k1gMnSc1	básník
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
především	především	k9	především
o	o	k7c6	o
tématu	téma	k1gNnSc6	téma
ztraceného	ztracený	k2eAgNnSc2d1	ztracené
lidství	lidství	k1gNnSc2	lidství
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
znovu	znovu	k6eAd1	znovu
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
civilizace	civilizace	k1gFnSc1	civilizace
ho	on	k3xPp3gMnSc4	on
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
<g/>
.	.	kIx.	.
</s>
<s>
Různí	různý	k2eAgMnPc1d1	různý
hudebníci	hudebník	k1gMnPc1	hudebník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
zhudebnili	zhudebnit	k5eAaPmAgMnP	zhudebnit
několik	několik	k4yIc1	několik
jeho	jeho	k3xOp3gFnPc2	jeho
básní	báseň	k1gFnPc2	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávky	nahrávka	k1gFnPc1	nahrávka
vyšly	vyjít	k5eAaPmAgFnP	vyjít
na	na	k7c6	na
albu	album	k1gNnSc6	album
Federico	Federico	k6eAd1	Federico
García	Garcí	k2eAgFnSc1d1	García
Lorca	Lorca	k1gFnSc1	Lorca
<g/>
:	:	kIx,	:
De	De	k?	De
Granada	Granada	k1gFnSc1	Granada
a	a	k8xC	a
La	la	k1gNnSc1	la
Luna	luna	k1gFnSc1	luna
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
hudebníky	hudebník	k1gMnPc4	hudebník
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
John	John	k1gMnSc1	John
Cale	Cal	k1gFnSc2	Cal
či	či	k8xC	či
Neneh	Neneha	k1gFnPc2	Neneha
Cherry	Cherra	k1gFnSc2	Cherra
<g/>
.	.	kIx.	.
</s>
<s>
Cikánská	cikánský	k2eAgFnSc1d1	cikánská
romance	romance	k1gFnSc1	romance
-	-	kIx~	-
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejslavnější	slavný	k2eAgMnSc1d3	nejslavnější
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Básník	básník	k1gMnSc1	básník
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
Kniha	kniha	k1gFnSc1	kniha
básní	básnit	k5eAaImIp3nS	básnit
Písně	píseň	k1gFnSc2	píseň
První	první	k4xOgFnPc4	první
písně	píseň	k1gFnPc4	píseň
Kniha	kniha	k1gFnSc1	kniha
písní	píseň	k1gFnSc7	píseň
Pláňka	pláňka	k1gFnSc1	pláňka
(	(	kIx(	(
<g/>
Yerma	Yerma	k1gFnSc1	Yerma
<g/>
)	)	kIx)	)
Dům	dům	k1gInSc1	dům
Bernardy	Bernard	k1gMnPc4	Bernard
Albové	albový	k2eAgInPc1d1	albový
Uhrančivý	uhrančivý	k2eAgMnSc1d1	uhrančivý
motýl	motýl	k1gMnSc1	motýl
Láska	láska	k1gFnSc1	láska
dona	dona	k1gFnSc1	dona
Perlimplína	Perlimplína	k1gFnSc1	Perlimplína
a	a	k8xC	a
vášnivost	vášnivost	k1gFnSc1	vášnivost
Belisina	Belisin	k2eAgMnSc2d1	Belisin
Mariana	Marian	k1gMnSc2	Marian
Pineda	Pined	k1gMnSc2	Pined
Čarokrásná	čarokrásný	k2eAgFnSc1d1	čarokrásná
ševcová	ševcová	k1gFnSc1	ševcová
Krvavá	krvavý	k2eAgFnSc1d1	krvavá
svatba	svatba	k1gFnSc1	svatba
Pimprlata	pimprle	k1gNnPc4	pimprle
Neprovdaná	provdaný	k2eNgFnSc1d1	neprovdaná
doňa	doňa	k1gFnSc1	doňa
Rosita	Rosit	k2eAgFnSc1d1	Rosita
aneb	aneb	k?	aneb
Květomluva	květomluva	k1gFnSc1	květomluva
Fantastická	fantastický	k2eAgFnSc1d1	fantastická
ševcová	ševcová	k1gFnSc1	ševcová
Publikum	publikum	k1gNnSc4	publikum
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
1979	[number]	k4	1979
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
-	-	kIx~	-
Federico	Federico	k6eAd1	Federico
García	Garcí	k2eAgFnSc1d1	García
Lorca	Lorca	k1gFnSc1	Lorca
<g/>
:	:	kIx,	:
Neprovdaná	provdaný	k2eNgFnSc1d1	neprovdaná
paní	paní	k1gFnSc1	paní
Rosita	Rosita	k1gFnSc1	Rosita
neboli	neboli	k8xC	neboli
Mluva	mluva	k1gFnSc1	mluva
květů	květ	k1gInPc2	květ
<g/>
.	.	kIx.	.
</s>
<s>
Přeložil	přeložit	k5eAaPmAgInS	přeložit
Lumír	Lumír	k1gInSc1	Lumír
Čivrný	Čivrný	k2eAgInSc1d1	Čivrný
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
Jiří	Jiří	k1gMnSc1	Jiří
Váchal	Váchal	k1gMnSc1	Váchal
<g/>
.	.	kIx.	.
</s>
<s>
Rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
úprava	úprava	k1gFnSc1	úprava
a	a	k8xC	a
režie	režie	k1gFnSc1	režie
Alena	Alena	k1gFnSc1	Alena
Adamcová	Adamcová	k1gFnSc1	Adamcová
<g/>
.	.	kIx.	.
</s>
<s>
Dramaturgie	dramaturgie	k1gFnSc1	dramaturgie
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Strejčková	Strejčková	k1gFnSc1	Strejčková
<g/>
.	.	kIx.	.
</s>
<s>
Účinkují	účinkovat	k5eAaImIp3nP	účinkovat
<g/>
:	:	kIx,	:
Růžena	Růžena	k1gFnSc1	Růžena
Merunková	Merunková	k1gFnSc1	Merunková
<g/>
,	,	kIx,	,
Jiřina	Jiřina	k1gFnSc1	Jiřina
Švorcová	švorcový	k2eAgFnSc1d1	Švorcová
<g/>
,	,	kIx,	,
Milada	Milada	k1gFnSc1	Milada
Davidová	Davidová	k1gFnSc1	Davidová
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Šmeral	Šmeral	k1gMnSc1	Šmeral
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Schánilec	Schánilec	k1gMnSc1	Schánilec
<g/>
,	,	kIx,	,
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Navrátil	Navrátil	k1gMnSc1	Navrátil
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
Prokoš	Prokoš	k1gMnSc1	Prokoš
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Hartl	Hartl	k1gMnSc1	Hartl
<g/>
,	,	kIx,	,
Ljuba	Ljuba	k1gFnSc1	Ljuba
Hušková	Hušková	k1gFnSc1	Hušková
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Trunečková	Trunečková	k1gFnSc1	Trunečková
<g/>
,	,	kIx,	,
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
Fialková	fialkový	k2eAgFnSc1d1	Fialková
<g/>
,	,	kIx,	,
Miriam	Miriam	k1gFnSc1	Miriam
Kantorková	Kantorková	k1gFnSc1	Kantorková
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Blanka	Blanka	k1gFnSc1	Blanka
Vikusová	Vikusový	k2eAgFnSc1d1	Vikusová
<g/>
,	,	kIx,	,
Karolina	Karolinum	k1gNnPc1	Karolinum
Slunéčková	Slunéčková	k1gFnSc1	Slunéčková
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Klenová	klenový	k2eAgFnSc1d1	Klenová
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Boušková	Boušková	k1gFnSc1	Boušková
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Boušková	Boušková	k1gFnSc1	Boušková
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Robin	robin	k2eAgMnSc1d1	robin
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Lebeda	Lebeda	k1gMnSc1	Lebeda
a	a	k8xC	a
Radislav	Radislav	k1gMnSc1	Radislav
Nikodém	Nikodém	k1gMnSc1	Nikodém
<g/>
.	.	kIx.	.
1981	[number]	k4	1981
Krvavá	krvavý	k2eAgFnSc1d1	krvavá
svatba	svatba	k1gFnSc1	svatba
(	(	kIx(	(
<g/>
Bodas	Bodas	k1gInSc1	Bodas
de	de	k?	de
sangre	sangr	k1gMnSc5	sangr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Carlos	Carlos	k1gMnSc1	Carlos
Saura	Saura	k1gMnSc1	Saura
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
<g/>
:	:	kIx,	:
Antonio	Antonio	k1gMnSc1	Antonio
Gades	Gades	k1gMnSc1	Gades
<g/>
,	,	kIx,	,
Cristina	Cristina	k1gFnSc1	Cristina
Hoyos	Hoyos	k1gInSc1	Hoyos
<g/>
.	.	kIx.	.
</s>
