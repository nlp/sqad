<s>
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
ا	ا	k?	ا
<g/>
;	;	kIx,	;
Alžírská	alžírský	k2eAgFnSc1d1	alžírská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
a	a	k8xC	a
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
2	[number]	k4	2
381	[number]	k4	381
741	[number]	k4	741
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
zemí	zem	k1gFnSc7	zem
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
desátou	desátá	k1gFnSc4	desátá
největší	veliký	k2eAgFnSc4d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
okolo	okolo	k7c2	okolo
37	[number]	k4	37
900	[number]	k4	900
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
prezidentskou	prezidentský	k2eAgFnSc7d1	prezidentská
republikou	republika	k1gFnSc7	republika
s	s	k7c7	s
48	[number]	k4	48
provinciemi	provincie	k1gFnPc7	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
je	být	k5eAaImIp3nS	být
prezidentem	prezident	k1gMnSc7	prezident
Abdelazíz	Abdelazíz	k1gMnSc1	Abdelazíz
Buteflika	Buteflik	k1gMnSc4	Buteflik
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
a	a	k8xC	a
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Alžír	Alžír	k1gInSc1	Alžír
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
existovala	existovat	k5eAaImAgFnS	existovat
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
obchodní	obchodní	k2eAgFnSc2d1	obchodní
kolonie	kolonie	k1gFnSc2	kolonie
Féničanů	Féničan	k1gMnPc2	Féničan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
přišli	přijít	k5eAaPmAgMnP	přijít
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
předci	předek	k1gMnPc1	předek
Berberů	Berber	k1gMnPc2	Berber
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zde	zde	k6eAd1	zde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
římská	římský	k2eAgFnSc1d1	římská
provincie	provincie	k1gFnSc1	provincie
Mauretania	Mauretanium	k1gNnSc2	Mauretanium
Caesarensis	Caesarensis	k1gFnSc2	Caesarensis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
46	[number]	k4	46
až	až	k9	až
429	[number]	k4	429
ovládali	ovládat	k5eAaImAgMnP	ovládat
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
Vandalové	Vandal	k1gMnPc1	Vandal
a	a	k8xC	a
Byzantici	Byzantik	k1gMnPc1	Byzantik
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1529	[number]	k4	1529
bylo	být	k5eAaImAgNnS	být
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
připojeno	připojit	k5eAaPmNgNnS	připojit
k	k	k7c3	k
Osmanské	osmanský	k2eAgFnSc3d1	Osmanská
říši	říš	k1gFnSc3	říš
Chairem	Chairma	k1gFnPc2	Chairma
ad-Dinem	ad-Din	k1gInSc7	ad-Din
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
bratrem	bratr	k1gMnSc7	bratr
Arujem	Aruj	k1gMnSc7	Aruj
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
udělal	udělat	k5eAaPmAgMnS	udělat
z	z	k7c2	z
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
základnu	základna	k1gFnSc4	základna
tzv.	tzv.	kA	tzv.
barbarských	barbarský	k2eAgMnPc2d1	barbarský
korzárů	korzár	k1gMnPc2	korzár
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
16	[number]	k4	16
<g/>
.	.	kIx.	.
až	až	k9	až
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
severoafrickými	severoafrický	k2eAgFnPc7d1	severoafrická
piráty	pirát	k1gMnPc4	pirát
odvlečeno	odvlečen	k2eAgNnSc4d1	odvlečen
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
až	až	k6eAd1	až
1	[number]	k4	1
milion	milion	k4xCgInSc1	milion
Evropanů	Evropan	k1gMnPc2	Evropan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
provedla	provést	k5eAaPmAgFnS	provést
Francie	Francie	k1gFnSc1	Francie
do	do	k7c2	do
země	zem	k1gFnSc2	zem
invazi	invaze	k1gFnSc4	invaze
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
se	se	k3xPyFc4	se
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
dostalo	dostat	k5eAaPmAgNnS	dostat
pod	pod	k7c4	pod
její	její	k3xOp3gFnSc4	její
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Desítky	desítka	k1gFnPc1	desítka
tisíc	tisíc	k4xCgInSc1	tisíc
kolonistů	kolonista	k1gMnPc2	kolonista
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
Malty	Malta	k1gFnPc1	Malta
přijely	přijet	k5eAaPmAgFnP	přijet
do	do	k7c2	do
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
hospodařily	hospodařit	k5eAaImAgFnP	hospodařit
na	na	k7c6	na
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
planinách	planina	k1gFnPc6	planina
a	a	k8xC	a
zabraly	zabrat	k5eAaPmAgFnP	zabrat
nejdražší	drahý	k2eAgInSc4d3	nejdražší
pozemky	pozemek	k1gInPc4	pozemek
ve	v	k7c6	v
městech	město	k1gNnPc6	město
vyvlastněné	vyvlastněný	k2eAgInPc1d1	vyvlastněný
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
evropského	evropský	k2eAgInSc2d1	evropský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
původní	původní	k2eAgMnPc1d1	původní
alžírští	alžírský	k2eAgMnPc1d1	alžírský
Židé	Žid	k1gMnPc1	Žid
byli	být	k5eAaImAgMnP	být
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
právoplatnými	právoplatný	k2eAgMnPc7d1	právoplatný
francouzskými	francouzský	k2eAgMnPc7d1	francouzský
občany	občan	k1gMnPc7	občan
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
alžírských	alžírský	k2eAgMnPc2d1	alžírský
muslimů	muslim	k1gMnPc2	muslim
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zůstali	zůstat	k5eAaPmAgMnP	zůstat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
mimo	mimo	k7c4	mimo
francouzské	francouzský	k2eAgNnSc4d1	francouzské
právo	právo	k1gNnSc4	právo
nemajíce	mít	k5eNaImSgFnP	mít
občanství	občanství	k1gNnSc4	občanství
ani	ani	k8xC	ani
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přivedl	přivést	k5eAaPmAgMnS	přivést
první	první	k4xOgFnPc4	první
snahy	snaha	k1gFnPc4	snaha
o	o	k7c4	o
změnu	změna	k1gFnSc4	změna
poměru	poměr	k1gInSc2	poměr
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Důležitými	důležitý	k2eAgNnPc7d1	důležité
daty	datum	k1gNnPc7	datum
byl	být	k5eAaImAgInS	být
rok	rok	k1gInSc4	rok
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
vypracován	vypracovat	k5eAaPmNgInS	vypracovat
"	"	kIx"	"
<g/>
Manifest	manifest	k1gInSc1	manifest
alžírského	alžírský	k2eAgInSc2d1	alžírský
lidu	lid	k1gInSc2	lid
<g/>
"	"	kIx"	"
a	a	k8xC	a
rok	rok	k1gInSc4	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Hnutí	hnutí	k1gNnSc1	hnutí
za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
demokratických	demokratický	k2eAgFnPc2d1	demokratická
svobod	svoboda	k1gFnPc2	svoboda
(	(	kIx(	(
<g/>
MTLD	MTLD	kA	MTLD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
se	se	k3xPyFc4	se
část	část	k1gFnSc1	část
MTLD	MTLD	kA	MTLD
transformovala	transformovat	k5eAaBmAgFnS	transformovat
do	do	k7c2	do
FNO	FNO	kA	FNO
(	(	kIx(	(
<g/>
Fronta	fronta	k1gFnSc1	fronta
národního	národní	k2eAgNnSc2d1	národní
osvobození	osvobození	k1gNnSc2	osvobození
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
AFRISOU	AFRISOU	kA	AFRISOU
<g/>
,	,	kIx,	,
Bajer	Bajer	k1gMnSc1	Bajer
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
zahájila	zahájit	k5eAaPmAgFnS	zahájit
Národní	národní	k2eAgFnSc1d1	národní
osvobozenecká	osvobozenecký	k2eAgFnSc1d1	osvobozenecká
fronta	fronta	k1gFnSc1	fronta
(	(	kIx(	(
<g/>
NOF	NOF	kA	NOF
<g/>
)	)	kIx)	)
guerillovou	guerillový	k2eAgFnSc4d1	guerillová
Alžírskou	alžírský	k2eAgFnSc4d1	alžírská
válku	válka	k1gFnSc4	válka
o	o	k7c4	o
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skoro	skoro	k6eAd1	skoro
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
městech	město	k1gNnPc6	město
i	i	k8xC	i
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
vyhnat	vyhnat	k5eAaPmF	vyhnat
Francouze	Francouz	k1gMnSc4	Francouz
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
evropského	evropský	k2eAgInSc2d1	evropský
původu	původ	k1gInSc2	původ
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
profrancouzskými	profrancouzský	k2eAgMnPc7d1	profrancouzský
muslimy	muslim	k1gMnPc7	muslim
během	během	k7c2	během
několika	několik	k4yIc2	několik
měsíců	měsíc	k1gInPc2	měsíc
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
odletěla	odletět	k5eAaPmAgFnS	odletět
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
osvobozenecké	osvobozenecký	k2eAgFnSc2d1	osvobozenecká
fronty	fronta	k1gFnSc2	fronta
Ahmed	Ahmed	k1gInSc1	Ahmed
Ben	Ben	k1gInSc1	Ben
Bella	Bella	k1gFnSc1	Bella
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
svržen	svrhnout	k5eAaPmNgMnS	svrhnout
svým	svůj	k3xOyFgMnSc7	svůj
bývalým	bývalý	k2eAgMnSc7d1	bývalý
spojencem	spojenec	k1gMnSc7	spojenec
a	a	k8xC	a
ministrem	ministr	k1gMnSc7	ministr
obrany	obrana	k1gFnSc2	obrana
Houarim	Houarim	k1gMnSc1	Houarim
Boumédiè	Boumédiè	k1gMnSc1	Boumédiè
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
po	po	k7c4	po
25	[number]	k4	25
let	léto	k1gNnPc2	léto
udržela	udržet	k5eAaPmAgFnS	udržet
relativní	relativní	k2eAgFnSc1d1	relativní
stabilita	stabilita	k1gFnSc1	stabilita
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
socialistické	socialistický	k2eAgFnSc2d1	socialistická
strany	strana	k1gFnSc2	strana
Bumediena	Bumedien	k1gMnSc2	Bumedien
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
následovníků	následovník	k1gMnPc2	následovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
neuspokojivé	uspokojivý	k2eNgFnSc2d1	neuspokojivá
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
situace	situace	k1gFnSc2	situace
(	(	kIx(	(
<g/>
pomalé	pomalý	k2eAgNnSc1d1	pomalé
tempo	tempo	k1gNnSc1	tempo
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
reforem	reforma	k1gFnPc2	reforma
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
státnímu	státní	k2eAgInSc3d1	státní
převratu	převrat	k1gInSc3	převrat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
následující	následující	k2eAgNnPc1d1	následující
léta	léto	k1gNnPc1	léto
probíhala	probíhat	k5eAaImAgNnP	probíhat
ekonomicky	ekonomicky	k6eAd1	ekonomicky
velmi	velmi	k6eAd1	velmi
neuspokojivě	uspokojivě	k6eNd1	uspokojivě
<g/>
,	,	kIx,	,
důsledkem	důsledek	k1gInSc7	důsledek
byly	být	k5eAaImAgFnP	být
časté	častý	k2eAgFnPc1d1	častá
personální	personální	k2eAgFnPc1d1	personální
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
nejvyšších	vysoký	k2eAgFnPc6d3	nejvyšší
pozicích	pozice	k1gFnPc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
byl	být	k5eAaImAgInS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
silný	silný	k2eAgInSc4d1	silný
centralistický	centralistický	k2eAgInSc4d1	centralistický
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
nastává	nastávat	k5eAaImIp3nS	nastávat
období	období	k1gNnSc4	období
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
proměn	proměna	k1gFnPc2	proměna
v	v	k7c6	v
odvětvích	odvětví	k1gNnPc6	odvětví
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
též	též	k9	též
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1988	[number]	k4	1988
Alžírsko	Alžírsko	k1gNnSc4	Alžírsko
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
vlny	vlna	k1gFnPc1	vlna
stávek	stávka	k1gFnPc2	stávka
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
propad	propad	k1gInSc4	propad
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
střetech	střet	k1gInPc6	střet
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
několik	několik	k4yIc1	několik
stovek	stovka	k1gFnPc2	stovka
stávkujících	stávkující	k1gMnPc2	stávkující
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Chadli	Chadli	k1gMnSc1	Chadli
Bendjedid	Bendjedid	k1gInSc4	Bendjedid
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
nepovolil	povolit	k5eNaPmAgMnS	povolit
zrušení	zrušení	k1gNnSc4	zrušení
politického	politický	k2eAgInSc2d1	politický
monopolu	monopol	k1gInSc2	monopol
NOF	NOF	kA	NOF
<g/>
,	,	kIx,	,
připustil	připustit	k5eAaPmAgMnS	připustit
existenci	existence	k1gFnSc4	existence
různých	různý	k2eAgInPc2d1	různý
názorových	názorový	k2eAgInPc2d1	názorový
proudů	proud	k1gInPc2	proud
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgInS	navrhnout
novou	nový	k2eAgFnSc4d1	nová
ústavu	ústava	k1gFnSc4	ústava
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
byl	být	k5eAaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
Národnímu	národní	k2eAgNnSc3d1	národní
shromáždění	shromáždění	k1gNnSc3	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
referenda	referendum	k1gNnSc2	referendum
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
ústavě	ústava	k1gFnSc6	ústava
-	-	kIx~	-
92	[number]	k4	92
<g/>
%	%	kIx~	%
pro	pro	k7c4	pro
-	-	kIx~	-
byl	být	k5eAaImAgInS	být
však	však	k9	však
přijat	přijmout	k5eAaPmNgInS	přijmout
podezřívavě	podezřívavě	k6eAd1	podezřívavě
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
nařčen	nařčen	k2eAgMnSc1d1	nařčen
z	z	k7c2	z
profrancouzské	profrancouzský	k2eAgFnSc2d1	profrancouzská
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
Chadli	Chadli	k1gMnSc1	Chadli
Bendjedid	Bendjedida	k1gFnPc2	Bendjedida
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
znovu	znovu	k6eAd1	znovu
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
umožnila	umožnit	k5eAaPmAgFnS	umožnit
nová	nový	k2eAgFnSc1d1	nová
ústava	ústava	k1gFnSc1	ústava
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
i	i	k9	i
opozičním	opoziční	k2eAgFnPc3d1	opoziční
stranám	strana	k1gFnPc3	strana
<g/>
.	.	kIx.	.
</s>
<s>
Zavedení	zavedení	k1gNnSc1	zavedení
pluralismu	pluralismus	k1gInSc2	pluralismus
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
a	a	k8xC	a
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
postupném	postupný	k2eAgNnSc6d1	postupné
rušení	rušení	k1gNnSc6	rušení
apartheidu	apartheid	k1gInSc2	apartheid
v	v	k7c4	v
JAR	jar	k1gFnSc4	jar
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
události	událost	k1gFnPc1	událost
spadající	spadající	k2eAgFnPc1d1	spadající
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
před	před	k7c7	před
pádem	pád	k1gInSc7	pád
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
<g/>
,	,	kIx,	,
předzvěstí	předzvěst	k1gFnSc7	předzvěst
demokratické	demokratický	k2eAgFnSc2d1	demokratická
změny	změna	k1gFnSc2	změna
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1991	[number]	k4	1991
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Islámská	islámský	k2eAgFnSc1d1	islámská
fronta	fronta	k1gFnSc1	fronta
spásy	spása	k1gFnSc2	spása
(	(	kIx(	(
<g/>
IFS	IFS	kA	IFS
<g/>
)	)	kIx)	)
fundamentalistických	fundamentalistický	k2eAgMnPc2d1	fundamentalistický
muslimů	muslim	k1gMnPc2	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
vojenský	vojenský	k2eAgInSc1d1	vojenský
převrat	převrat	k1gInSc1	převrat
<g/>
,	,	kIx,	,
moc	moc	k6eAd1	moc
převzala	převzít	k5eAaPmAgFnS	převzít
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
státní	státní	k2eAgFnSc1d1	státní
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1992	[number]	k4	1992
rozpustila	rozpustit	k5eAaPmAgFnS	rozpustit
parlament	parlament	k1gInSc4	parlament
a	a	k8xC	a
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
IFS	IFS	kA	IFS
byla	být	k5eAaImAgFnS	být
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
neukončila	ukončit	k5eNaPmAgFnS	ukončit
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
vyzvala	vyzvat	k5eAaPmAgFnS	vyzvat
všechny	všechen	k3xTgMnPc4	všechen
cizince	cizinec	k1gMnPc4	cizinec
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ji	on	k3xPp3gFnSc4	on
opustili	opustit	k5eAaPmAgMnP	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uplynuti	uplynout	k5eAaPmNgMnP	uplynout
této	tento	k3xDgFnSc2	tento
lhůty	lhůta	k1gFnSc2	lhůta
začaly	začít	k5eAaPmAgInP	začít
masakry	masakr	k1gInPc1	masakr
namířené	namířený	k2eAgInPc1d1	namířený
nejen	nejen	k6eAd1	nejen
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
Liamine	Liamin	k1gInSc5	Liamin
Zéroual	Zéroual	k1gInSc1	Zéroual
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
podporován	podporovat	k5eAaImNgInS	podporovat
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
usmiřovacích	usmiřovací	k2eAgFnPc6d1	usmiřovací
konferencích	konference	k1gFnPc6	konference
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996	[number]	k4	1996
a	a	k8xC	a
1997	[number]	k4	1997
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
přimět	přimět	k5eAaPmF	přimět
umírněné	umírněný	k2eAgMnPc4d1	umírněný
islamisty	islamista	k1gMnPc4	islamista
k	k	k7c3	k
dialogu	dialog	k1gInSc2	dialog
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1996	[number]	k4	1996
schválilo	schválit	k5eAaPmAgNnS	schválit
85,6	[number]	k4	85,6
%	%	kIx~	%
Alžířanů	Alžířan	k1gMnPc2	Alžířan
ústavní	ústavní	k2eAgFnSc4d1	ústavní
reformu	reforma	k1gFnSc4	reforma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
náboženské	náboženský	k2eAgFnPc4d1	náboženská
strany	strana	k1gFnPc4	strana
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
formování	formování	k1gNnSc1	formování
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
posiluje	posilovat	k5eAaImIp3nS	posilovat
úlohu	úloha	k1gFnSc4	úloha
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
mezi	mezi	k7c7	mezi
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
islamistickými	islamistický	k2eAgMnPc7d1	islamistický
povstalci	povstalec	k1gMnPc7	povstalec
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
přes	přes	k7c4	přes
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zvolení	zvolení	k1gNnSc6	zvolení
sekulárního	sekulární	k2eAgMnSc2d1	sekulární
prezidenta	prezident	k1gMnSc2	prezident
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
islamisté	islamista	k1gMnPc1	islamista
začali	začít	k5eAaPmAgMnP	začít
mstít	mstít	k5eAaImF	mstít
masakrováním	masakrování	k1gNnSc7	masakrování
nezúčastněných	zúčastněný	k2eNgMnPc2d1	nezúčastněný
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2008	[number]	k4	2008
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
sebevražednému	sebevražedný	k2eAgInSc3d1	sebevražedný
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
policejní	policejní	k2eAgFnSc4d1	policejní
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgNnSc6	který
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
43	[number]	k4	43
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
probíhaly	probíhat	k5eAaImAgFnP	probíhat
nebo	nebo	k8xC	nebo
probíhají	probíhat	k5eAaImIp3nP	probíhat
protesty	protest	k1gInPc1	protest
<g/>
,	,	kIx,	,
nepokoje	nepokoj	k1gInPc1	nepokoj
<g/>
,	,	kIx,	,
povstání	povstání	k1gNnPc1	povstání
a	a	k8xC	a
revoluce	revoluce	k1gFnPc1	revoluce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nazývány	nazývat	k5eAaImNgFnP	nazývat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
arabské	arabský	k2eAgNnSc1d1	arabské
jaro	jaro	k1gNnSc1	jaro
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
těchto	tento	k3xDgFnPc2	tento
událostí	událost	k1gFnPc2	událost
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
zrušen	zrušit	k5eAaPmNgInS	zrušit
téměř	téměř	k6eAd1	téměř
20	[number]	k4	20
let	léto	k1gNnPc2	léto
trvající	trvající	k2eAgInSc1d1	trvající
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
<g/>
.	.	kIx.	.
</s>
<s>
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Afriky	Afrika	k1gFnSc2	Afrika
je	být	k5eAaImIp3nS	být
rozlohou	rozloha	k1gFnSc7	rozloha
největší	veliký	k2eAgInSc1d3	veliký
stát	stát	k1gInSc1	stát
celého	celý	k2eAgInSc2d1	celý
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
je	být	k5eAaImIp3nS	být
998	[number]	k4	998
km	km	kA	km
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
kopcovité	kopcovitý	k2eAgInPc4d1	kopcovitý
až	až	k8xS	až
hornaté	hornatý	k2eAgNnSc4d1	hornaté
pobřeží	pobřeží	k1gNnSc4	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
leží	ležet	k5eAaImIp3nS	ležet
většina	většina	k1gFnSc1	většina
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
včetně	včetně	k7c2	včetně
hlavního	hlavní	k2eAgInSc2d1	hlavní
Alžíru	Alžír	k1gInSc2	Alžír
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Tuniskem	Tunisko	k1gNnSc7	Tunisko
(	(	kIx(	(
<g/>
965	[number]	k4	965
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Libyí	Libye	k1gFnSc7	Libye
(	(	kIx(	(
<g/>
982	[number]	k4	982
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Nigerem	Niger	k1gInSc7	Niger
(	(	kIx(	(
<g/>
956	[number]	k4	956
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
s	s	k7c7	s
Mali	Mali	k1gNnSc7	Mali
(	(	kIx(	(
<g/>
1376	[number]	k4	1376
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
s	s	k7c7	s
Mauritánií	Mauritánie	k1gFnSc7	Mauritánie
(	(	kIx(	(
<g/>
463	[number]	k4	463
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	s	k7c7	s
Západní	západní	k2eAgFnSc7d1	západní
Saharou	Sahara	k1gFnSc7	Sahara
(	(	kIx(	(
<g/>
42	[number]	k4	42
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
Marokem	Maroko	k1gNnSc7	Maroko
(	(	kIx(	(
<g/>
1559	[number]	k4	1559
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
rozlohy	rozloha	k1gFnSc2	rozloha
země	zem	k1gFnSc2	zem
zabírá	zabírat	k5eAaImIp3nS	zabírat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
poušť	poušť	k1gFnSc1	poušť
Sahara	Sahara	k1gFnSc1	Sahara
s	s	k7c7	s
pohořími	pohoří	k1gNnPc7	pohoří
Ahaggar	Ahaggar	k1gMnSc1	Ahaggar
a	a	k8xC	a
Tassili	Tassili	k1gMnSc1	Tassili
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ahaggaru	Ahaggar	k1gInSc6	Ahaggar
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
země	zem	k1gFnSc2	zem
Tahat	tahat	k5eAaImF	tahat
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
2	[number]	k4	2
908	[number]	k4	908
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
dominují	dominovat	k5eAaImIp3nP	dominovat
dva	dva	k4xCgMnPc1	dva
paralelně	paralelně	k6eAd1	paralelně
od	od	k7c2	od
východu	východ	k1gInSc2	východ
na	na	k7c4	na
západ	západ	k1gInSc4	západ
probíhající	probíhající	k2eAgInPc4d1	probíhající
horské	horský	k2eAgInPc4d1	horský
hřebeny	hřeben	k1gInPc4	hřeben
Atlasu	Atlas	k1gInSc2	Atlas
<g/>
,	,	kIx,	,
oddělující	oddělující	k2eAgNnSc1d1	oddělující
pobřeží	pobřeží	k1gNnSc1	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
od	od	k7c2	od
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Velkým	velký	k2eAgInSc7d1	velký
Atlasem	Atlas	k1gInSc7	Atlas
a	a	k8xC	a
Malým	malý	k2eAgInSc7d1	malý
Atlasem	Atlas	k1gInSc7	Atlas
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
700	[number]	k4	700
<g/>
-	-	kIx~	-
<g/>
1200	[number]	k4	1200
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
plošina	plošina	k1gFnSc1	plošina
šotů	šot	k1gInPc2	šot
se	s	k7c7	s
slanými	slaný	k2eAgNnPc7d1	slané
jezery	jezero	k1gNnPc7	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
80	[number]	k4	80
%	%	kIx~	%
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
se	se	k3xPyFc4	se
nenachází	nacházet	k5eNaImIp3nS	nacházet
žádná	žádný	k3yNgFnSc1	žádný
vegetace	vegetace	k1gFnSc1	vegetace
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
oblast	oblast	k1gFnSc1	oblast
Kabylie	Kabylie	k1gFnSc2	Kabylie
východně	východně	k6eAd1	východně
od	od	k7c2	od
Alžíru	Alžír	k1gInSc2	Alžír
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
souvislým	souvislý	k2eAgInSc7d1	souvislý
lesním	lesní	k2eAgInSc7d1	lesní
porostem	porost	k1gInSc7	porost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pobřežní	pobřežní	k2eAgFnSc6d1	pobřežní
oblasti	oblast	k1gFnSc6	oblast
vládne	vládnout	k5eAaImIp3nS	vládnout
středomořské	středomořský	k2eAgNnSc4d1	středomořské
podnebí	podnebí	k1gNnSc4	podnebí
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
Atlasu	Atlas	k1gInSc2	Atlas
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
klima	klima	k1gNnSc1	klima
s	s	k7c7	s
horkými	horký	k2eAgNnPc7d1	horké
léty	léto	k1gNnPc7	léto
a	a	k8xC	a
studenými	studený	k2eAgFnPc7d1	studená
zimami	zima	k1gFnPc7	zima
<g/>
.	.	kIx.	.
</s>
<s>
Suché	Suché	k2eAgNnSc1d1	Suché
a	a	k8xC	a
horké	horký	k2eAgNnSc1d1	horké
pouštní	pouštní	k2eAgNnSc1d1	pouštní
počasí	počasí	k1gNnSc1	počasí
s	s	k7c7	s
vysokými	vysoký	k2eAgInPc7d1	vysoký
teplotními	teplotní	k2eAgInPc7d1	teplotní
rozdíly	rozdíl	k1gInPc7	rozdíl
mezi	mezi	k7c7	mezi
dnem	den	k1gInSc7	den
a	a	k8xC	a
nocí	noc	k1gFnSc7	noc
vládne	vládnout	k5eAaImIp3nS	vládnout
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nejníže	nízce	k6eAd3	nízce
položené	položený	k2eAgNnSc1d1	položené
je	být	k5eAaImIp3nS	být
Chott	Chott	k2eAgInSc4d1	Chott
Merlhir	Merlhir	k1gInSc4	Merlhir
(	(	kIx(	(
<g/>
-	-	kIx~	-
40	[number]	k4	40
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
Největší	veliký	k2eAgFnSc7d3	veliký
řekou	řeka	k1gFnSc7	řeka
je	být	k5eAaImIp3nS	být
Šelif	Šelif	k1gInSc1	Šelif
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
volený	volený	k2eAgInSc1d1	volený
na	na	k7c4	na
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
změny	změna	k1gFnSc2	změna
ústavy	ústava	k1gFnSc2	ústava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
mohl	moct	k5eAaImAgMnS	moct
vykonávat	vykonávat	k5eAaImF	vykonávat
maximálně	maximálně	k6eAd1	maximálně
dva	dva	k4xCgInPc4	dva
mandáty	mandát	k1gInPc4	mandát
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
mandátů	mandát	k1gInPc2	mandát
neomezený	omezený	k2eNgInSc1d1	neomezený
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
předsedu	předseda	k1gMnSc4	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sestavuje	sestavovat	k5eAaImIp3nS	sestavovat
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Alžírský	alžírský	k2eAgInSc1d1	alžírský
parlament	parlament	k1gInSc1	parlament
je	být	k5eAaImIp3nS	být
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1	dolní
komora	komora	k1gFnSc1	komora
(	(	kIx(	(
<g/>
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
380	[number]	k4	380
poslanců	poslanec	k1gMnPc2	poslanec
volených	volená	k1gFnPc2	volená
na	na	k7c4	na
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
komora	komora	k1gFnSc1	komora
(	(	kIx(	(
<g/>
Rada	rada	k1gFnSc1	rada
národů	národ	k1gInPc2	národ
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
144	[number]	k4	144
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
96	[number]	k4	96
je	být	k5eAaImIp3nS	být
voleno	volit	k5eAaImNgNnS	volit
a	a	k8xC	a
48	[number]	k4	48
jmenováno	jmenován	k2eAgNnSc1d1	jmenováno
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
žádná	žádný	k3yNgFnSc1	žádný
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
rozdílech	rozdíl	k1gInPc6	rozdíl
v	v	k7c6	v
náboženství	náboženství	k1gNnSc6	náboženství
<g/>
,	,	kIx,	,
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
rasy	rasa	k1gFnSc2	rasa
<g/>
,	,	kIx,	,
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
povolání	povolání	k1gNnSc2	povolání
nebo	nebo	k8xC	nebo
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
parlamentní	parlamentní	k2eAgFnPc1d1	parlamentní
volby	volba	k1gFnPc1	volba
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
a	a	k8xC	a
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
Fronta	fronta	k1gFnSc1	fronta
národního	národní	k2eAgNnSc2d1	národní
osvobození	osvobození	k1gNnSc2	osvobození
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
221	[number]	k4	221
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Abderrahmane	Abderrahman	k1gMnSc5	Abderrahman
Farè	Farè	k1gMnSc1	Farè
7.4	[number]	k4	7.4
<g/>
.1962	.1962	k4	.1962
<g/>
-	-	kIx~	-
<g/>
25.9	[number]	k4	25.9
<g/>
.1962	.1962	k4	.1962
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
exekutivy	exekutiva	k1gFnSc2	exekutiva
-	-	kIx~	-
FLN	FLN	kA	FLN
Ferhat	Ferhat	k1gMnSc1	Ferhat
Said	Said	k1gMnSc1	Said
Ahmed	Ahmed	k1gMnSc1	Ahmed
Abbas	Abbas	k1gMnSc1	Abbas
25.9	[number]	k4	25.9
<g/>
.1962	.1962	k4	.1962
<g/>
-	-	kIx~	-
<g/>
27.9	[number]	k4	27.9
<g/>
.1962	.1962	k4	.1962
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
Ústavodárného	ústavodárný	k2eAgNnSc2d1	Ústavodárné
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
-	-	kIx~	-
FLN	FLN	kA	FLN
Ahmed	Ahmed	k1gInSc1	Ahmed
Ben	Ben	k1gInSc1	Ben
Bella	Bella	k1gFnSc1	Bella
27.9	[number]	k4	27.9
<g/>
.1962	.1962	k4	.1962
<g/>
-	-	kIx~	-
<g/>
20.9	[number]	k4	20.9
<g/>
.1963	.1963	k4	.1963
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
-	-	kIx~	-
FLN	FLN	kA	FLN
Ahmed	Ahmed	k1gInSc1	Ahmed
Ben	Ben	k1gInSc1	Ben
Bella	Bella	k1gFnSc1	Bella
20.9	[number]	k4	20.9
<g/>
.1963	.1963	k4	.1963
<g/>
-	-	kIx~	-
<g/>
19.6	[number]	k4	19.6
<g/>
.1965	.1965	k4	.1965
-	-	kIx~	-
prezident	prezident	k1gMnSc1	prezident
-	-	kIx~	-
FLN	FLN	kA	FLN
Houari	Houare	k1gFnSc4	Houare
Boumédiè	Boumédiè	k1gFnSc1	Boumédiè
19.6	[number]	k4	19.6
<g/>
.1965	.1965	k4	.1965
<g/>
-	-	kIx~	-
<g/>
11.12	[number]	k4	11.12
<g/>
.1976	.1976	k4	.1976
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
Revoluční	revoluční	k2eAgFnSc2d1	revoluční
rady	rada	k1gFnSc2	rada
-	-	kIx~	-
voj.	voj.	k?	voj.
<g/>
/	/	kIx~	/
<g/>
FLN	FLN	kA	FLN
Houari	Houare	k1gFnSc4	Houare
Boumédiè	Boumédiè	k1gFnSc1	Boumédiè
11.12	[number]	k4	11.12
<g/>
.1976	.1976	k4	.1976
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
27.12	[number]	k4	27.12
<g/>
.1978	.1978	k4	.1978
-	-	kIx~	-
prezident	prezident	k1gMnSc1	prezident
-	-	kIx~	-
voj.	voj.	k?	voj.
<g/>
/	/	kIx~	/
<g/>
FLN	FLN	kA	FLN
Rabah	Rabah	k1gInSc1	Rabah
Bitat	Bitat	k1gInSc1	Bitat
27.12	[number]	k4	27.12
<g/>
.1978	.1978	k4	.1978
<g/>
-	-	kIx~	-
<g/>
9.2	[number]	k4	9.2
<g/>
.1979	.1979	k4	.1979
-	-	kIx~	-
prozatímní	prozatímní	k2eAgMnSc1d1	prozatímní
prezident	prezident	k1gMnSc1	prezident
-	-	kIx~	-
FLN	FLN	kA	FLN
Chadli	Chadli	k1gFnSc1	Chadli
Bendjedid	Bendjedid	k1gInSc1	Bendjedid
9.2	[number]	k4	9.2
<g/>
.1979	.1979	k4	.1979
<g/>
-	-	kIx~	-
<g/>
11.1	[number]	k4	11.1
<g/>
.1992	.1992	k4	.1992
-	-	kIx~	-
prezident	prezident	k1gMnSc1	prezident
-	-	kIx~	-
FLN	FLN	kA	FLN
Abdelmalek	Abdelmalek	k1gInSc1	Abdelmalek
Benhabyles	Benhabyles	k1gInSc1	Benhabyles
11.1	[number]	k4	11.1
<g/>
.1992	.1992	k4	.1992
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
14.1	[number]	k4	14.1
<g/>
.1992	.1992	k4	.1992
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
Ústavodárné	ústavodárný	k2eAgFnSc2d1	ústavodárná
rady	rada	k1gFnSc2	rada
-	-	kIx~	-
FLN	FLN	kA	FLN
Mohamed	Mohamed	k1gMnSc1	Mohamed
Boudiaf	Boudiaf	k1gMnSc1	Boudiaf
14.1	[number]	k4	14.1
<g/>
.1992	.1992	k4	.1992
-	-	kIx~	-
31.1	[number]	k4	31.1
<g/>
.1994	.1994	k4	.1994
-	-	kIx~	-
(	(	kIx(	(
<g/>
PRS	prs	k1gInSc1	prs
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
16.1	[number]	k4	16.1
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
29.6	[number]	k4	29.6
<g/>
.1992	.1992	k4	.1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tedjini	Tedjin	k2eAgMnPc1d1	Tedjin
Haddam	Haddam	k1gInSc1	Haddam
(	(	kIx(	(
<g/>
bezpartijní	bezpartijní	k1gMnSc1	bezpartijní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ali	Ali	k1gMnSc1	Ali
Haroun	Haroun	k1gMnSc1	Haroun
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
FLN	FLN	kA	FLN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ali	Ali	k1gMnSc1	Ali
Kafi	Kafi	k?	Kafi
(	(	kIx(	(
<g/>
FLN	FLN	kA	FLN
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
od	od	k7c2	od
2.7	[number]	k4	2.7
<g/>
.1992	.1992	k4	.1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Redha	Redha	k1gMnSc1	Redha
Malek	Malek	k1gMnSc1	Malek
(	(	kIx(	(
<g/>
bezpartijní	bezpartijní	k1gMnSc1	bezpartijní
<g/>
,	,	kIx,	,
od	od	k7c2	od
2.7	[number]	k4	2.7
<g/>
.1992	.1992	k4	.1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Khaled	Khaled	k1gMnSc1	Khaled
Nezzar	Nezzar	k1gMnSc1	Nezzar
(	(	kIx(	(
<g/>
voj.	voj.	k?	voj.
<g/>
)	)	kIx)	)
-	-	kIx~	-
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
státní	státní	k2eAgFnSc1d1	státní
rada	rada	k1gFnSc1	rada
Liamine	Liamin	k1gInSc5	Liamin
<g />
.	.	kIx.	.
</s>
<s>
Zéroual	Zéroual	k1gInSc1	Zéroual
31.1	[number]	k4	31.1
<g/>
.1994	.1994	k4	.1994
<g/>
-	-	kIx~	-
<g/>
27.11	[number]	k4	27.11
<g/>
.1995	.1995	k4	.1995
-	-	kIx~	-
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
-	-	kIx~	-
bezpartijní	bezpartijní	k1gMnSc1	bezpartijní
<g/>
/	/	kIx~	/
<g/>
RND	RND	kA	RND
Liamine	Liamin	k1gInSc5	Liamin
Zéroual	Zéroual	k1gMnSc1	Zéroual
27.11	[number]	k4	27.11
<g/>
.1995	.1995	k4	.1995
<g/>
-	-	kIx~	-
<g/>
27.4	[number]	k4	27.4
<g/>
.1999	.1999	k4	.1999
-	-	kIx~	-
prezident	prezident	k1gMnSc1	prezident
-	-	kIx~	-
bezpartijní	bezpartijní	k1gMnSc1	bezpartijní
<g/>
/	/	kIx~	/
<g/>
RND	RND	kA	RND
Abdelazíz	Abdelazíz	k1gInSc1	Abdelazíz
Buteflika	Buteflik	k1gMnSc2	Buteflik
od	od	k7c2	od
27.4	[number]	k4	27.4
<g/>
.1999	.1999	k4	.1999
-	-	kIx~	-
prezident	prezident	k1gMnSc1	prezident
-	-	kIx~	-
bezpartijní	bezpartijní	k2eAgNnSc1d1	bezpartijní
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
má	mít	k5eAaImIp3nS	mít
48	[number]	k4	48
provincií	provincie	k1gFnPc2	provincie
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
vilájetů	vilájet	k1gMnPc2	vilájet
<g/>
,	,	kIx,	,
553	[number]	k4	553
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
daï	daï	k?	daï
<g/>
)	)	kIx)	)
a	a	k8xC	a
1	[number]	k4	1
541	[number]	k4	541
obcí	obec	k1gFnPc2	obec
(	(	kIx(	(
<g/>
baladiyah	baladiyah	k1gInSc1	baladiyah
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgMnPc1d1	původní
administrativní	administrativní	k2eAgMnPc1d1	administrativní
rozdělení	rozdělený	k2eAgMnPc1d1	rozdělený
Alžířané	Alžířan	k1gMnPc1	Alžířan
převzali	převzít	k5eAaPmAgMnP	převzít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
po	po	k7c6	po
francouzské	francouzský	k2eAgFnSc6d1	francouzská
správě	správa	k1gFnSc6	správa
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
15	[number]	k4	15
původních	původní	k2eAgFnPc2d1	původní
departementy	departement	k1gInPc4	departement
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
vilájety	vilájet	k1gMnPc4	vilájet
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
na	na	k7c4	na
31	[number]	k4	31
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
17	[number]	k4	17
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
vilájet	vilájet	k5eAaPmF	vilájet
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
<g/>
,	,	kIx,	,
obec	obec	k1gFnSc1	obec
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
sídle	sídlo	k1gNnSc6	sídlo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
i	i	k9	i
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tak	tak	k6eAd1	tak
získalo	získat	k5eAaPmAgNnS	získat
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
i	i	k8xC	i
název	název	k1gInSc1	název
po	po	k7c6	po
Alžíru	Alžír	k1gInSc6	Alžír
<g/>
.	.	kIx.	.
</s>
<s>
Rozlohou	rozloha	k1gFnSc7	rozloha
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc1d3	veliký
vilájet	vilájet	k5eAaImF	vilájet
Tamanrasset	Tamanrasset	k1gInSc4	Tamanrasset
(	(	kIx(	(
<g/>
556	[number]	k4	556
200	[number]	k4	200
km2	km2	k4	km2
<g/>
)	)	kIx)	)
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
a	a	k8xC	a
nejvíce	hodně	k6eAd3	hodně
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
vilájetu	vilájet	k1gInSc6	vilájet
Alžír	Alžír	k1gInSc1	Alžír
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
2	[number]	k4	2
947	[number]	k4	947
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
<g/>
.	.	kIx.	.
</s>
<s>
Páteří	páteř	k1gFnSc7	páteř
alžírského	alžírský	k2eAgNnSc2d1	alžírské
hospodářství	hospodářství	k1gNnSc2	hospodářství
je	být	k5eAaImIp3nS	být
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
fosfáty	fosfát	k1gInPc1	fosfát
<g/>
,	,	kIx,	,
polymetalické	polymetalický	k2eAgFnPc1d1	polymetalický
rudy	ruda	k1gFnPc1	ruda
a	a	k8xC	a
rtuť	rtuť	k1gFnSc1	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
důležitými	důležitý	k2eAgInPc7d1	důležitý
odvětvími	odvětví	k1gNnPc7	odvětví
jsou	být	k5eAaImIp3nP	být
zpracování	zpracování	k1gNnSc4	zpracování
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
a	a	k8xC	a
textilní	textilní	k2eAgInSc1d1	textilní
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ale	ale	k8xC	ale
tvoří	tvořit	k5eAaImIp3nP	tvořit
jen	jen	k9	jen
13	[number]	k4	13
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Alžírskou	alžírský	k2eAgFnSc7d1	alžírská
měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
alžírský	alžírský	k2eAgInSc4d1	alžírský
dinár	dinár	k1gInSc4	dinár
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
ruského	ruský	k2eAgMnSc2d1	ruský
prezidenta	prezident	k1gMnSc2	prezident
Vladimira	Vladimir	k1gInSc2	Vladimir
Putina	putin	k2eAgInSc2d1	putin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Ruska	Rusko	k1gNnSc2	Rusko
odpuštěno	odpustit	k5eAaPmNgNnS	odpustit
4,74	[number]	k4	4,74
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
nákup	nákup	k1gInSc4	nákup
bojových	bojový	k2eAgNnPc2d1	bojové
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
obranných	obranný	k2eAgInPc2d1	obranný
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
zbraní	zbraň	k1gFnPc2	zbraň
v	v	k7c6	v
ceně	cena	k1gFnSc6	cena
7,5	[number]	k4	7,5
miliard	miliarda	k4xCgFnPc2	miliarda
doladů	dolad	k1gInPc2	dolad
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Rosoboronexport	Rosoboronexport	k1gInSc1	Rosoboronexport
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Alžíru	Alžír	k1gInSc6	Alžír
dáno	dát	k5eAaPmNgNnS	dát
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
druhé	druhý	k4xOgNnSc1	druhý
metro	metro	k1gNnSc1	metro
na	na	k7c6	na
africkém	africký	k2eAgInSc6d1	africký
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c4	na
37	[number]	k4	37
900	[number]	k4	900
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
měla	mít	k5eAaImAgFnS	mít
populace	populace	k1gFnSc1	populace
přibližně	přibližně	k6eAd1	přibližně
čtyři	čtyři	k4xCgInPc4	čtyři
milióny	milión	k4xCgInPc4	milión
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
90	[number]	k4	90
%	%	kIx~	%
Alžířanů	Alžířan	k1gMnPc2	Alžířan
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
pobřežní	pobřežní	k2eAgFnSc6d1	pobřežní
oblasti	oblast	k1gFnSc6	oblast
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
a	a	k8xC	a
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
přístav	přístav	k1gInSc1	přístav
Alžír	Alžír	k1gInSc1	Alžír
(	(	kIx(	(
<g/>
3	[number]	k4	3
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
významná	významný	k2eAgNnPc4d1	významné
města	město	k1gNnPc4	město
a	a	k8xC	a
přístavy	přístav	k1gInPc4	přístav
Oran	Orana	k1gFnPc2	Orana
(	(	kIx(	(
<g/>
necelých	celý	k2eNgNnPc2d1	necelé
1,2	[number]	k4	1,2
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Constantine	Constantin	k1gMnSc5	Constantin
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
943	[number]	k4	943
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sétif	Sétif	k1gMnSc1	Sétif
(	(	kIx(	(
<g/>
609	[number]	k4	609
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgFnSc1d1	kulturní
oblast	oblast	k1gFnSc1	oblast
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
Alžír	Alžír	k1gInSc1	Alžír
a	a	k8xC	a
Constantine	Constantin	k1gInSc5	Constantin
je	být	k5eAaImIp3nS	být
nazývána	nazývat	k5eAaImNgFnS	nazývat
Kabylie	Kabylie	k1gFnSc1	Kabylie
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
oblastí	oblast	k1gFnSc7	oblast
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
leží	ležet	k5eAaImIp3nS	ležet
město	město	k1gNnSc1	město
Tamanrasset	Tamanrasseta	k1gFnPc2	Tamanrasseta
s	s	k7c7	s
92	[number]	k4	92
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
kočovným	kočovný	k2eAgInSc7d1	kočovný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
28,1	[number]	k4	28,1
<g/>
%	%	kIx~	%
Alžířanů	Alžířan	k1gMnPc2	Alžířan
má	mít	k5eAaImIp3nS	mít
méně	málo	k6eAd2	málo
než	než	k8xS	než
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnější	početní	k2eAgFnSc4d3	nejpočetnější
skupinu	skupina	k1gFnSc4	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
tvoří	tvořit	k5eAaImIp3nP	tvořit
alžířští	alžířský	k2eAgMnPc1d1	alžířský
Arabové	Arab	k1gMnPc1	Arab
(	(	kIx(	(
<g/>
59	[number]	k4	59
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Berbeři	Berber	k1gMnPc1	Berber
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Beduíni	Beduín	k1gMnPc1	Beduín
(	(	kIx(	(
<g/>
beduinští	beduinský	k2eAgMnPc1d1	beduinský
Arabové	Arab	k1gMnPc1	Arab
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Berberské	berberský	k2eAgInPc1d1	berberský
kmeny	kmen	k1gInPc1	kmen
tvoří	tvořit	k5eAaImIp3nP	tvořit
na	na	k7c6	na
alžírském	alžírský	k2eAgNnSc6d1	alžírské
území	území	k1gNnSc6	území
kmenová	kmenový	k2eAgNnPc1d1	kmenové
sdružení	sdružení	k1gNnPc1	sdružení
Kabylů	Kabyl	k1gInPc2	Kabyl
<g/>
,	,	kIx,	,
Mozabitů	Mozabita	k1gMnPc2	Mozabita
a	a	k8xC	a
Tuaregů	Tuareg	k1gMnPc2	Tuareg
<g/>
.	.	kIx.	.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
Alžířanů	Alžířan	k1gMnPc2	Alžířan
jsou	být	k5eAaImIp3nP	být
Arabové	Arab	k1gMnPc1	Arab
nebo	nebo	k8xC	nebo
původní	původní	k2eAgMnPc1d1	původní
Berbeři	Berber	k1gMnPc1	Berber
<g/>
.	.	kIx.	.
</s>
<s>
Druzí	druhý	k4xOgMnPc1	druhý
jmenovaní	jmenovaný	k1gMnPc1	jmenovaný
tvoří	tvořit	k5eAaImIp3nP	tvořit
odhadem	odhad	k1gInSc7	odhad
celou	celá	k1gFnSc4	celá
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
oblast	oblast	k1gFnSc4	oblast
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Arabové	Arab	k1gMnPc1	Arab
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
majoritním	majoritní	k2eAgNnSc7d1	majoritní
etnikem	etnikum	k1gNnSc7	etnikum
<g/>
.	.	kIx.	.
</s>
<s>
Berbeři	Berber	k1gMnPc1	Berber
se	se	k3xPyFc4	se
usídlili	usídlit	k5eAaPmAgMnP	usídlit
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
vzdáleni	vzdálen	k2eAgMnPc1d1	vzdálen
arabskému	arabský	k2eAgInSc3d1	arabský
vlivu	vliv	k1gInSc2	vliv
si	se	k3xPyFc3	se
dodnes	dodnes	k6eAd1	dodnes
uchovali	uchovat	k5eAaPmAgMnP	uchovat
silné	silný	k2eAgFnPc4d1	silná
kulturní	kulturní	k2eAgFnPc4d1	kulturní
hodnoty	hodnota	k1gFnPc4	hodnota
i	i	k8xC	i
vlastní	vlastní	k2eAgInSc4d1	vlastní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Evropané	Evropan	k1gMnPc1	Evropan
usazení	usazený	k2eAgMnPc1d1	usazený
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
tzv.	tzv.	kA	tzv.
pied	pied	k1gMnSc1	pied
noir	noir	k1gMnSc1	noir
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
Francouzského	francouzský	k2eAgNnSc2d1	francouzské
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
dnes	dnes	k6eAd1	dnes
sotva	sotva	k6eAd1	sotva
jedno	jeden	k4xCgNnSc1	jeden
procento	procento	k1gNnSc1	procento
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
země	zem	k1gFnSc2	zem
žije	žít	k5eAaImIp3nS	žít
90	[number]	k4	90
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
165	[number]	k4	165
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
uprchlých	uprchlý	k2eAgMnPc2d1	uprchlý
ze	z	k7c2	z
Západní	západní	k2eAgFnSc2d1	západní
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
přes	přes	k7c4	přes
4	[number]	k4	4
000	[number]	k4	000
palestinských	palestinský	k2eAgMnPc2d1	palestinský
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
integrováni	integrován	k2eAgMnPc1d1	integrován
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
i	i	k9	i
35	[number]	k4	35
000	[number]	k4	000
čínských	čínský	k2eAgMnPc2d1	čínský
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
Alžířanů	Alžířan	k1gMnPc2	Alžířan
mimo	mimo	k7c4	mimo
svou	svůj	k3xOyFgFnSc4	svůj
zemi	zem	k1gFnSc4	zem
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1,7	[number]	k4	1,7
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálními	oficiální	k2eAgMnPc7d1	oficiální
jazyky	jazyk	k1gMnPc7	jazyk
jsou	být	k5eAaImIp3nP	být
arabština	arabština	k1gFnSc1	arabština
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ovládá	ovládat	k5eAaImIp3nS	ovládat
asi	asi	k9	asi
72	[number]	k4	72
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
berberština	berberština	k1gFnSc1	berberština
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mluví	mluvit	k5eAaImIp3nS	mluvit
27	[number]	k4	27
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Hovorová	hovorový	k2eAgFnSc1d1	hovorová
arabština	arabština	k1gFnSc1	arabština
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
ovlivněna	ovlivněn	k2eAgFnSc1d1	ovlivněna
francouzštinou	francouzština	k1gFnSc7	francouzština
a	a	k8xC	a
berberštinou	berberština	k1gFnSc7	berberština
<g/>
.	.	kIx.	.
</s>
<s>
Berberština	berberština	k1gFnSc1	berberština
byla	být	k5eAaImAgFnS	být
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
novelou	novela	k1gFnSc7	novela
ústavy	ústava	k1gFnSc2	ústava
uznána	uznat	k5eAaPmNgFnS	uznat
jako	jako	k8xS	jako
národní	národní	k2eAgInSc4d1	národní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stala	stát	k5eAaPmAgFnS	stát
oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Francouzština	francouzština	k1gFnSc1	francouzština
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
oficiální	oficiální	k2eAgInSc4d1	oficiální
status	status	k1gInSc4	status
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
frankofonní	frankofonní	k2eAgFnSc7d1	frankofonní
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jak	jak	k6eAd1	jak
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
a	a	k8xC	a
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
správě	správa	k1gFnSc6	správa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
umělo	umět	k5eAaImAgNnS	umět
33	[number]	k4	33
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
číst	číst	k5eAaImF	číst
a	a	k8xC	a
psát	psát	k5eAaImF	psát
francouzsky	francouzsky	k6eAd1	francouzsky
a	a	k8xC	a
60	[number]	k4	60
%	%	kIx~	%
jich	on	k3xPp3gFnPc2	on
francouzštinu	francouzština	k1gFnSc4	francouzština
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
podstatně	podstatně	k6eAd1	podstatně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
získalo	získat	k5eAaPmAgNnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Výuka	výuka	k1gFnSc1	výuka
francouzštiny	francouzština	k1gFnSc2	francouzština
je	být	k5eAaImIp3nS	být
podporována	podporován	k2eAgFnSc1d1	podporována
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
ve	v	k7c6	v
výuce	výuka	k1gFnSc6	výuka
už	už	k9	už
na	na	k7c6	na
základních	základní	k2eAgFnPc6d1	základní
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
40	[number]	k4	40
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
je	být	k5eAaImIp3nS	být
negramotných	gramotný	k2eNgInPc2d1	negramotný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zde	zde	k6eAd1	zde
děti	dítě	k1gFnPc1	dítě
pracují	pracovat	k5eAaImIp3nP	pracovat
už	už	k6eAd1	už
od	od	k7c2	od
útlého	útlý	k2eAgInSc2d1	útlý
věku	věk	k1gInSc2	věk
a	a	k8xC	a
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
chodí	chodit	k5eAaImIp3nS	chodit
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
nebo	nebo	k8xC	nebo
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
sunnitskému	sunnitský	k2eAgInSc3d1	sunnitský
islámu	islám	k1gInSc3	islám
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
99	[number]	k4	99
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
státním	státní	k2eAgNnSc7d1	státní
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
150	[number]	k4	150
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
ve	v	k7c6	v
vilájetu	vilájeto	k1gNnSc6	vilájeto
Ghardája	Ghardáj	k1gInSc2	Ghardáj
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
islámské	islámský	k2eAgFnSc3d1	islámská
větvi	větev	k1gFnSc3	větev
ibádíja	ibádíj	k1gInSc2	ibádíj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Alžíru	Alžír	k1gInSc6	Alžír
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
mešita	mešita	k1gFnSc1	mešita
s	s	k7c7	s
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
školou	škola	k1gFnSc7	škola
<g/>
,	,	kIx,	,
knihovnou	knihovna	k1gFnSc7	knihovna
<g/>
,	,	kIx,	,
muzeem	muzeum	k1gNnSc7	muzeum
a	a	k8xC	a
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
minaretem	minaret	k1gInSc7	minaret
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
265	[number]	k4	265
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
arcidiecézi	arcidiecéze	k1gFnSc4	arcidiecéze
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
hlásilo	hlásit	k5eAaImAgNnS	hlásit
až	až	k9	až
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
věřících	věřící	k1gMnPc2	věřící
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
země	země	k1gFnSc1	země
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
odešla	odejít	k5eAaPmAgFnS	odejít
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
asi	asi	k9	asi
83	[number]	k4	83
000	[number]	k4	000
římskokatolíků	římskokatolík	k1gInPc2	římskokatolík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1944	[number]	k4	1944
<g/>
-	-	kIx~	-
<g/>
1956	[number]	k4	1956
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Alžíru	Alžír	k1gInSc6	Alžír
postavena	postaven	k2eAgFnSc1d1	postavena
katedrála	katedrála	k1gFnSc1	katedrála
Sacré-Coeur	Sacré-Coeura	k1gFnPc2	Sacré-Coeura
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Alger	Alger	k1gMnSc1	Alger
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
i	i	k9	i
početná	početný	k2eAgFnSc1d1	početná
komunita	komunita	k1gFnSc1	komunita
protestantů	protestant	k1gMnPc2	protestant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
asi	asi	k9	asi
140	[number]	k4	140
000	[number]	k4	000
židů	žid	k1gMnPc2	žid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
získali	získat	k5eAaPmAgMnP	získat
francouzské	francouzský	k2eAgNnSc4d1	francouzské
občanství	občanství	k1gNnSc4	občanství
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
jich	on	k3xPp3gInPc2	on
odešla	odejít	k5eAaPmAgFnS	odejít
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
10	[number]	k4	10
%	%	kIx~	%
pak	pak	k6eAd1	pak
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
vláda	vláda	k1gFnSc1	vláda
neeviduje	evidovat	k5eNaImIp3nS	evidovat
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
příslušnost	příslušnost	k1gFnSc4	příslušnost
kvůli	kvůli	k7c3	kvůli
zabránění	zabránění	k1gNnSc3	zabránění
náboženského	náboženský	k2eAgNnSc2d1	náboženské
napětí	napětí	k1gNnSc2	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Počty	počet	k1gInPc1	počet
věřících	věřící	k1gMnPc2	věřící
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
odhadovány	odhadovat	k5eAaImNgFnP	odhadovat
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
ústava	ústava	k1gFnSc1	ústava
zakotvila	zakotvit	k5eAaPmAgFnS	zakotvit
islám	islám	k1gInSc4	islám
jako	jako	k8xC	jako
státní	státní	k2eAgNnSc4d1	státní
náboženství	náboženství	k1gNnSc4	náboženství
a	a	k8xC	a
arabskou	arabský	k2eAgFnSc4d1	arabská
a	a	k8xC	a
berberskou	berberský	k2eAgFnSc4d1	berberská
národnost	národnost	k1gFnSc4	národnost
jako	jako	k8xS	jako
základ	základ	k1gInSc4	základ
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Výrazem	výraz	k1gInSc7	výraz
venkovských	venkovský	k2eAgFnPc2d1	venkovská
tradic	tradice	k1gFnPc2	tradice
jsou	být	k5eAaImIp3nP	být
každoroční	každoroční	k2eAgInSc4d1	každoroční
tzv.	tzv.	kA	tzv.
datlové	datlový	k2eAgFnSc2d1	datlová
slavnosti	slavnost	k1gFnSc2	slavnost
a	a	k8xC	a
tanci	tanec	k1gInPc7	tanec
a	a	k8xC	a
závody	závod	k1gInPc7	závod
velbloudů	velbloud	k1gMnPc2	velbloud
<g/>
.	.	kIx.	.
</s>
<s>
Literární	literární	k2eAgNnPc1d1	literární
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
vydávána	vydáván	k2eAgNnPc1d1	vydáváno
jak	jak	k6eAd1	jak
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
a	a	k8xC	a
arabštině	arabština	k1gFnSc6	arabština
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
berberských	berberský	k2eAgInPc6d1	berberský
dialektech	dialekt	k1gInPc6	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Kráter	kráter	k1gInSc1	kráter
Tin	Tina	k1gFnPc2	Tina
Bider	Bidra	k1gFnPc2	Bidra
</s>
