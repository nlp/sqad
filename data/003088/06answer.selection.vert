<s>
Roku	rok	k1gInSc2	rok
1529	[number]	k4	1529
bylo	být	k5eAaImAgNnS	být
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
připojeno	připojit	k5eAaPmNgNnS	připojit
k	k	k7c3	k
Osmanské	osmanský	k2eAgFnSc3d1	Osmanská
říši	říš	k1gFnSc3	říš
Chairem	Chairma	k1gFnPc2	Chairma
ad-Dinem	ad-Din	k1gInSc7	ad-Din
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
bratrem	bratr	k1gMnSc7	bratr
Arujem	Aruj	k1gMnSc7	Aruj
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
udělal	udělat	k5eAaPmAgMnS	udělat
z	z	k7c2	z
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
základnu	základna	k1gFnSc4	základna
tzv.	tzv.	kA	tzv.
barbarských	barbarský	k2eAgMnPc2d1	barbarský
korzárů	korzár	k1gMnPc2	korzár
<g/>
.	.	kIx.	.
</s>
