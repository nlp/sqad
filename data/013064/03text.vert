<p>
<s>
Chrpa	chrpa	k1gFnSc1	chrpa
chlumní	chlumní	k2eAgFnSc1d1	chlumní
(	(	kIx(	(
<g/>
Centaurea	Centaurea	k1gMnSc1	Centaurea
triumfettii	triumfettie	k1gFnSc4	triumfettie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bylina	bylina	k1gFnSc1	bylina
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
fialově	fialově	k6eAd1	fialově
modrým	modrý	k2eAgInSc7d1	modrý
květem	květ	k1gInSc7	květ
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
téměř	téměř	k6eAd1	téměř
700	[number]	k4	700
druhů	druh	k1gInPc2	druh
rodu	rod	k1gInSc2	rod
chrpa	chrpa	k1gFnSc1	chrpa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Rostlina	rostlina	k1gFnSc1	rostlina
poměrně	poměrně	k6eAd1	poměrně
teplomilná	teplomilný	k2eAgFnSc1d1	teplomilná
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
české	český	k2eAgNnSc4d1	české
území	území	k1gNnSc4	území
prochází	procházet	k5eAaImIp3nS	procházet
severní	severní	k2eAgFnSc1d1	severní
hranice	hranice	k1gFnSc1	hranice
jejího	její	k3xOp3gNnSc2	její
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
severozápadě	severozápad	k1gInSc6	severozápad
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
častá	častý	k2eAgFnSc1d1	častá
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
jen	jen	k9	jen
ostrůvkovitě	ostrůvkovitě	k6eAd1	ostrůvkovitě
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
ve	v	k7c6	v
stepích	step	k1gFnPc6	step
<g/>
,	,	kIx,	,
lesostepích	lesostep	k1gFnPc6	lesostep
<g/>
,	,	kIx,	,
skalnatých	skalnatý	k2eAgFnPc6d1	skalnatá
stráních	stráň	k1gFnPc6	stráň
<g/>
,	,	kIx,	,
v	v	k7c6	v
kamenitých	kamenitý	k2eAgInPc6d1	kamenitý
okrajích	okraj	k1gInPc6	okraj
lesů	les	k1gInPc2	les
a	a	k8xC	a
suchých	suchý	k2eAgFnPc2d1	suchá
luk	louka	k1gFnPc2	louka
<g/>
.	.	kIx.	.
</s>
<s>
Vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
slunná	slunný	k2eAgNnPc4d1	slunné
teplá	teplý	k2eAgNnPc4d1	teplé
stanoviště	stanoviště	k1gNnPc4	stanoviště
a	a	k8xC	a
suchou	suchý	k2eAgFnSc4d1	suchá
<g/>
,	,	kIx,	,
humózní	humózní	k2eAgFnSc4d1	humózní
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
propustnou	propustný	k2eAgFnSc4d1	propustná
půdu	půda	k1gFnSc4	půda
s	s	k7c7	s
vápencovým	vápencový	k2eAgNnSc7d1	vápencové
podložím	podloží	k1gNnSc7	podloží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
roste	růst	k5eAaImIp3nS	růst
lokálně	lokálně	k6eAd1	lokálně
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
převážně	převážně	k6eAd1	převážně
jen	jen	k9	jen
v	v	k7c6	v
teplejších	teplý	k2eAgFnPc6d2	teplejší
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
a	a	k8xC	a
severozápadních	severozápadní	k2eAgFnPc6d1	severozápadní
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
od	od	k7c2	od
Mostecké	mostecký	k2eAgFnSc2d1	Mostecká
a	a	k8xC	a
Sokolovské	sokolovský	k2eAgFnSc2d1	Sokolovská
pánve	pánev	k1gFnSc2	pánev
přes	přes	k7c4	přes
Doupovské	Doupovský	k2eAgFnPc4d1	Doupovská
hory	hora	k1gFnPc4	hora
<g/>
,	,	kIx,	,
České	český	k2eAgNnSc4d1	české
středohoří	středohoří	k1gNnSc4	středohoří
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
kras	kras	k1gInSc1	kras
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgNnSc1d1	dolní
Povltaví	Povltaví	k1gNnSc1	Povltaví
až	až	k9	až
po	po	k7c4	po
střední	střední	k2eAgNnSc4d1	střední
Polabí	Polabí	k1gNnSc4	Polabí
<g/>
)	)	kIx)	)
a	a	k8xC	a
pak	pak	k6eAd1	pak
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
(	(	kIx(	(
<g/>
od	od	k7c2	od
podhůří	podhůří	k1gNnSc2	podhůří
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
až	až	k9	až
po	po	k7c4	po
Drahanskou	Drahanský	k2eAgFnSc4d1	Drahanská
vrchovinu	vrchovina	k1gFnSc4	vrchovina
a	a	k8xC	a
Bílé	bílý	k2eAgInPc1d1	bílý
Karpaty	Karpaty	k1gInPc1	Karpaty
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzácně	vzácně	k6eAd1	vzácně
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Berounky	Berounka	k1gFnSc2	Berounka
až	až	k9	až
po	po	k7c4	po
Křivoklátsko	Křivoklátsko	k1gNnSc4	Křivoklátsko
a	a	k8xC	a
Plzeňskou	plzeňský	k2eAgFnSc4d1	Plzeňská
pahorkatinu	pahorkatina	k1gFnSc4	pahorkatina
a	a	k8xC	a
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Vltavy	Vltava	k1gFnSc2	Vltava
do	do	k7c2	do
středního	střední	k2eAgNnSc2d1	střední
Povltaví	Povltaví	k1gNnSc2	Povltaví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
rostlina	rostlina	k1gFnSc1	rostlina
se	s	k7c7	s
zelenou	zelená	k1gFnSc7	zelená
<g/>
,	,	kIx,	,
slabě	slabě	k6eAd1	slabě
plstnatou	plstnatý	k2eAgFnSc7d1	plstnatá
lodyhou	lodyha	k1gFnSc7	lodyha
přímou	přímý	k2eAgFnSc7d1	přímá
nebo	nebo	k8xC	nebo
vystoupavou	vystoupavý	k2eAgFnSc7d1	vystoupavá
<g/>
,	,	kIx,	,
jednoduchou	jednoduchý	k2eAgFnSc7d1	jednoduchá
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nahoře	nahoře	k6eAd1	nahoře
spoře	sporo	k6eAd1	sporo
větvenou	větvený	k2eAgFnSc4d1	větvená
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
z	z	k7c2	z
plazivého	plazivý	k2eAgInSc2d1	plazivý
oddenku	oddenek	k1gInSc2	oddenek
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
10	[number]	k4	10
až	až	k9	až
40	[number]	k4	40
<g/>
,	,	kIx,	,
ojediněle	ojediněle	k6eAd1	ojediněle
až	až	k9	až
80	[number]	k4	80
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Tvarově	tvarově	k6eAd1	tvarově
značně	značně	k6eAd1	značně
proměnlivé	proměnlivý	k2eAgInPc1d1	proměnlivý
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
měkké	měkký	k2eAgInPc1d1	měkký
a	a	k8xC	a
šedě	šedě	k6eAd1	šedě
plstnaté	plstnatý	k2eAgNnSc1d1	plstnaté
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
jen	jen	k9	jen
na	na	k7c6	na
rubu	rub	k1gInSc6	rub
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
bývají	bývat	k5eAaImIp3nP	bývat
celokrajné	celokrajný	k2eAgInPc1d1	celokrajný
a	a	k8xC	a
nedělené	dělený	k2eNgInPc1d1	nedělený
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
peřenolaločnaté	peřenolaločnatý	k2eAgFnPc1d1	peřenolaločnatý
až	až	k8xS	až
peřenodílné	peřenodílný	k2eAgFnPc1d1	peřenodílný
<g/>
.	.	kIx.	.
</s>
<s>
Přízemní	přízemní	k2eAgInPc4d1	přízemní
listy	list	k1gInPc4	list
mají	mít	k5eAaImIp3nP	mít
krátké	krátký	k2eAgInPc4d1	krátký
řapíky	řapík	k1gInPc4	řapík
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
růžici	růžice	k1gFnSc4	růžice
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
podlouhle	podlouhle	k6eAd1	podlouhle
vejčitý	vejčitý	k2eAgMnSc1d1	vejčitý
až	až	k8xS	až
kopinatý	kopinatý	k2eAgMnSc1d1	kopinatý
<g/>
,	,	kIx,	,
horní	horní	k2eAgFnPc1d1	horní
jsou	být	k5eAaImIp3nP	být
přisedlé	přisedlý	k2eAgFnPc1d1	přisedlá
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
mírně	mírně	k6eAd1	mírně
objímavé	objímavý	k2eAgFnPc1d1	objímavá
a	a	k8xC	a
podlouhle	podlouhle	k6eAd1	podlouhle
kopinaté	kopinatý	k2eAgNnSc1d1	kopinaté
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Květní	květní	k2eAgInPc1d1	květní
úbory	úbor	k1gInPc1	úbor
(	(	kIx(	(
<g/>
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
cm	cm	kA	cm
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
)	)	kIx)	)
s	s	k7c7	s
krátkými	krátký	k2eAgFnPc7d1	krátká
stopkami	stopka	k1gFnPc7	stopka
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
lodyh	lodyha	k1gFnPc2	lodyha
většinou	většinou	k6eAd1	většinou
jednotlivě	jednotlivě	k6eAd1	jednotlivě
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
obaleny	obalen	k2eAgInPc4d1	obalen
listy	list	k1gInPc4	list
<g/>
.	.	kIx.	.
</s>
<s>
Zákrov	zákrov	k1gInSc1	zákrov
je	být	k5eAaImIp3nS	být
vejčitý	vejčitý	k2eAgInSc1d1	vejčitý
až	až	k9	až
široce	široko	k6eAd1	široko
vejčitý	vejčitý	k2eAgMnSc1d1	vejčitý
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
listeny	listen	k1gInPc1	listen
s	s	k7c7	s
trojbokými	trojboký	k2eAgInPc7d1	trojboký
přívěsky	přívěsek	k1gInPc7	přívěsek
bílé	bílý	k2eAgFnSc2d1	bílá
nebo	nebo	k8xC	nebo
hnědé	hnědý	k2eAgFnSc2d1	hnědá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
mívají	mívat	k5eAaImIp3nP	mívat
listeny	listen	k1gInPc1	listen
třásnité	třásnitý	k2eAgInPc1d1	třásnitý
sbíhavé	sbíhavý	k2eAgInPc1d1	sbíhavý
zuby	zub	k1gInPc1	zub
bílé	bílý	k2eAgInPc1d1	bílý
nebo	nebo	k8xC	nebo
hnědavé	hnědavý	k2eAgInPc1d1	hnědavý
<g/>
,	,	kIx,	,
u	u	k7c2	u
báze	báze	k1gFnSc2	báze
jsou	být	k5eAaImIp3nP	být
červené	červený	k2eAgFnPc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Listeny	listen	k1gInPc1	listen
mají	mít	k5eAaImIp3nP	mít
brvy	brva	k1gFnPc1	brva
delší	dlouhý	k2eAgFnSc1d2	delší
než	než	k8xS	než
šířka	šířka	k1gFnSc1	šířka
lemu	lem	k1gInSc2	lem
s	s	k7c7	s
přívěsky	přívěsek	k1gInPc7	přívěsek
<g/>
.	.	kIx.	.
</s>
<s>
Prostřední	prostřednět	k5eAaImIp3nP	prostřednět
trubkovité	trubkovitý	k2eAgInPc4d1	trubkovitý
oboupohlavné	oboupohlavný	k2eAgInPc4d1	oboupohlavný
květy	květ	k1gInPc4	květ
jsou	být	k5eAaImIp3nP	být
červenavé	červenavý	k2eAgInPc1d1	červenavý
nebo	nebo	k8xC	nebo
fialové	fialový	k2eAgInPc1d1	fialový
<g/>
.	.	kIx.	.
</s>
<s>
Okrajové	okrajový	k2eAgNnSc1d1	okrajové
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
sterilní	sterilní	k2eAgInPc1d1	sterilní
<g/>
,	,	kIx,	,
paprskovité	paprskovitý	k2eAgInPc1d1	paprskovitý
květy	květ	k1gInPc1	květ
bývají	bývat	k5eAaImIp3nP	bývat
modré	modrý	k2eAgFnPc1d1	modrá
<g/>
,	,	kIx,	,
fialové	fialový	k2eAgFnPc1d1	fialová
nebo	nebo	k8xC	nebo
řídce	řídce	k6eAd1	řídce
i	i	k9	i
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
.	.	kIx.	.
</s>
<s>
Rostlina	rostlina	k1gFnSc1	rostlina
kvete	kvést	k5eAaImIp3nS	kvést
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
jsou	být	k5eAaImIp3nP	být
nažky	nažka	k1gFnPc1	nažka
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
asi	asi	k9	asi
4	[number]	k4	4
mm	mm	kA	mm
s	s	k7c7	s
dvouřadým	dvouřadý	k2eAgInSc7d1	dvouřadý
bílým	bílý	k2eAgInSc7d1	bílý
chmýrem	chmýr	k1gInSc7	chmýr
kratším	krátký	k2eAgInSc7d2	kratší
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
samy	sám	k3xTgMnPc4	sám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Chrpa	chrpa	k1gFnSc1	chrpa
chlumní	chlumní	k2eAgFnSc1d1	chlumní
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
variabilní	variabilní	k2eAgInSc1d1	variabilní
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
je	být	k5eAaImIp3nS	být
Evropě	Evropa	k1gFnSc6	Evropa
rozpoznáno	rozpoznat	k5eAaPmNgNnS	rozpoznat
těchto	tento	k3xDgInPc2	tento
13	[number]	k4	13
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Centaurea	Centaurea	k1gMnSc1	Centaurea
triumfetti	triumfetť	k1gFnSc2	triumfetť
All	All	k1gMnSc1	All
<g/>
.	.	kIx.	.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
triumfetti	triumfett	k5eAaBmF	triumfett
</s>
</p>
<p>
<s>
Centaurea	Centaurea	k1gMnSc1	Centaurea
triumfetti	triumfetť	k1gFnSc2	triumfetť
All	All	k1gMnSc1	All
<g/>
.	.	kIx.	.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
adscendens	adscendens	k1gInSc1	adscendens
(	(	kIx(	(
<g/>
Bartl	Bartl	k1gMnSc1	Bartl
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Dostál	Dostál	k1gMnSc1	Dostál
</s>
</p>
<p>
<s>
Centaurea	Centaurea	k1gMnSc1	Centaurea
triumfetti	triumfetť	k1gFnSc2	triumfetť
All	All	k1gMnSc1	All
<g/>
.	.	kIx.	.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
aligera	aligera	k1gFnSc1	aligera
(	(	kIx(	(
<g/>
Gugler	Gugler	k1gMnSc1	Gugler
<g/>
)	)	kIx)	)
Dostál	Dostál	k1gMnSc1	Dostál
</s>
</p>
<p>
<s>
Centaurea	Centaurea	k1gMnSc1	Centaurea
triumfetti	triumfetť	k1gFnSc2	triumfetť
All	All	k1gMnSc1	All
<g/>
.	.	kIx.	.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
angelescuii	angelescuie	k1gFnSc4	angelescuie
(	(	kIx(	(
<g/>
Grint	Grint	k1gInSc4	Grint
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Dostál	Dostál	k1gMnSc1	Dostál
</s>
</p>
<p>
<s>
Centaurea	Centaurea	k1gMnSc1	Centaurea
triumfetti	triumfetť	k1gFnSc2	triumfetť
All	All	k1gMnSc1	All
<g/>
.	.	kIx.	.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
cana	cana	k1gMnSc1	cana
(	(	kIx(	(
<g/>
Sibth	Sibth	k1gMnSc1	Sibth
<g/>
.	.	kIx.	.
et	et	k?	et
Sm	Sm	k1gFnSc1	Sm
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Dostál	Dostál	k1gMnSc1	Dostál
</s>
</p>
<p>
<s>
Centaurea	Centaurea	k1gMnSc1	Centaurea
triumfetti	triumfetť	k1gFnSc2	triumfetť
All	All	k1gMnSc1	All
<g/>
.	.	kIx.	.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
dominii	dominion	k1gNnPc7	dominion
Dostál	Dostál	k1gMnSc1	Dostál
</s>
</p>
<p>
<s>
Centaurea	Centaurea	k1gMnSc1	Centaurea
triumfetti	triumfetť	k1gFnSc2	triumfetť
All	All	k1gMnSc1	All
<g/>
.	.	kIx.	.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
lingulata	lingule	k1gNnPc1	lingule
(	(	kIx(	(
<g/>
Lag	Lag	k1gFnSc1	Lag
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Dostál	Dostál	k1gMnSc1	Dostál
</s>
</p>
<p>
<s>
Centaurea	Centaurea	k1gMnSc1	Centaurea
triumfetti	triumfetť	k1gFnSc2	triumfetť
All	All	k1gMnSc1	All
<g/>
.	.	kIx.	.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
lugdunensis	lugdunensis	k1gInSc1	lugdunensis
(	(	kIx(	(
<g/>
Jord	Jord	k1gInSc1	Jord
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Dostál	Dostál	k1gMnSc1	Dostál
</s>
</p>
<p>
<s>
Centaurea	Centaurea	k1gMnSc1	Centaurea
triumfetti	triumfetť	k1gFnSc2	triumfetť
All	All	k1gMnSc1	All
<g/>
.	.	kIx.	.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
novakii	novakie	k1gFnSc4	novakie
(	(	kIx(	(
<g/>
Dostál	Dostál	k1gMnSc1	Dostál
<g/>
)	)	kIx)	)
Dostál	Dostál	k1gMnSc1	Dostál
</s>
</p>
<p>
<s>
Centaurea	Centaurea	k1gMnSc1	Centaurea
triumfetti	triumfetť	k1gFnSc2	triumfetť
All	All	k1gMnSc1	All
<g/>
.	.	kIx.	.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
pirinensis	pirinensis	k1gInSc1	pirinensis
(	(	kIx(	(
<g/>
Degen	Degen	k1gInSc1	Degen
<g/>
,	,	kIx,	,
Urum	Urum	k1gInSc1	Urum
<g/>
.	.	kIx.	.
et	et	k?	et
J.	J.	kA	J.
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
)	)	kIx)	)
Dostál	Dostál	k1gMnSc1	Dostál
</s>
</p>
<p>
<s>
Centaurea	Centaurea	k1gMnSc1	Centaurea
triumfetti	triumfetť	k1gFnSc2	triumfetť
All	All	k1gMnSc1	All
<g/>
.	.	kIx.	.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
semidecurrens	semidecurrens	k1gInSc1	semidecurrens
(	(	kIx(	(
<g/>
Jord	Jord	k1gInSc1	Jord
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Dostál	Dostál	k1gMnSc1	Dostál
</s>
</p>
<p>
<s>
Centaurea	Centaurea	k1gMnSc1	Centaurea
triumfetti	triumfetť	k1gFnSc2	triumfetť
All	All	k1gMnSc1	All
<g/>
.	.	kIx.	.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
stricta	stricta	k1gMnSc1	stricta
(	(	kIx(	(
<g/>
Waldst	Waldst	k1gMnSc1	Waldst
<g/>
.	.	kIx.	.
et	et	k?	et
Kit	kit	k1gInSc1	kit
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Dostál	Dostál	k1gMnSc1	Dostál
</s>
</p>
<p>
<s>
Centaurea	Centaurea	k1gMnSc1	Centaurea
triumfetti	triumfetť	k1gFnSc2	triumfetť
All	All	k1gMnSc1	All
<g/>
.	.	kIx.	.
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
tanaitica	tanaitica	k1gMnSc1	tanaitica
(	(	kIx(	(
<g/>
Klokov	Klokov	k1gInSc1	Klokov
<g/>
)	)	kIx)	)
DostálPodle	DostálPodle	k1gFnSc1	DostálPodle
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgInSc1d1	jediný
poddruh	poddruh	k1gInSc1	poddruh
</s>
</p>
<p>
<s>
Chrpa	chrpa	k1gFnSc1	chrpa
chlumní	chlumní	k2eAgFnSc1d1	chlumní
širolistá	širolistý	k2eAgFnSc1d1	širolistá
(	(	kIx(	(
<g/>
Centaurea	Centaure	k2eAgFnSc1d1	Centaurea
triumfettii	triumfettie	k1gFnSc4	triumfettie
All	All	k1gFnSc2	All
<g/>
.	.	kIx.	.
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
axillaris	axillaris	k1gFnSc1	axillaris
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Willd	Willd	k1gInSc1	Willd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Dostál	Dostál	k1gMnSc1	Dostál
<g/>
.	.	kIx.	.
<g/>
Ne	ne	k9	ne
všemi	všecek	k3xTgMnPc7	všecek
odborníky	odborník	k1gMnPc7	odborník
jsou	být	k5eAaImIp3nP	být
zjištěné	zjištěný	k2eAgInPc1d1	zjištěný
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgMnPc7	tento
podruhy	podruh	k1gMnPc7	podruh
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
tak	tak	k6eAd1	tak
podstatné	podstatný	k2eAgNnSc4d1	podstatné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
stačily	stačit	k5eAaBmAgInP	stačit
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
taxonů	taxon	k1gInPc2	taxon
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
mnohdy	mnohdy	k6eAd1	mnohdy
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
pouhé	pouhý	k2eAgNnSc4d1	pouhé
synonyma	synonymum	k1gNnPc4	synonymum
druhu	druh	k1gInSc2	druh
Centaurea	Centaure	k2eAgNnPc1d1	Centaure
triumfetti	triumfetti	k1gNnPc1	triumfetti
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Chrpa	chrpa	k1gFnSc1	chrpa
chlumní	chlumní	k2eAgNnSc1d1	chlumní
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
zřetelné	zřetelný	k2eAgNnSc1d1	zřetelné
snižování	snižování	k1gNnSc1	snižování
počtu	počet	k1gInSc2	počet
kvetoucích	kvetoucí	k2eAgFnPc2d1	kvetoucí
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
úbytku	úbytek	k1gInSc3	úbytek
stanovišť	stanoviště	k1gNnPc2	stanoviště
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
vyhláškou	vyhláška	k1gFnSc7	vyhláška
MŽP	MŽP	kA	MŽP
ČR	ČR	kA	ČR
č.	č.	k?	č.
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
vyhl	vyhla	k1gFnPc2	vyhla
<g/>
.	.	kIx.	.
č.	č.	k?	č.
175	[number]	k4	175
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
a	a	k8xC	a
"	"	kIx"	"
<g/>
Černým	Černý	k1gMnPc3	Černý
a	a	k8xC	a
červeným	červená	k1gFnPc3	červená
seznam	seznam	k1gInSc1	seznam
cévnatých	cévnatý	k2eAgFnPc2d1	cévnatá
rostlin	rostlina	k1gFnPc2	rostlina
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
"	"	kIx"	"
za	za	k7c4	za
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
chrpa	chrpa	k1gFnSc1	chrpa
chlumní	chlumní	k2eAgFnSc1d1	chlumní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
