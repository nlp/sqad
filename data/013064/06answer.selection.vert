<s>
Chrpa	chrpa	k1gFnSc1	chrpa
chlumní	chlumní	k2eAgFnSc1d1	chlumní
(	(	kIx(	(
<g/>
Centaurea	Centaurea	k1gMnSc1	Centaurea
triumfettii	triumfettie	k1gFnSc4	triumfettie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bylina	bylina	k1gFnSc1	bylina
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
fialově	fialově	k6eAd1	fialově
modrým	modrý	k2eAgInSc7d1	modrý
květem	květ	k1gInSc7	květ
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
téměř	téměř	k6eAd1	téměř
700	[number]	k4	700
druhů	druh	k1gInPc2	druh
rodu	rod	k1gInSc2	rod
chrpa	chrpa	k1gFnSc1	chrpa
<g/>
.	.	kIx.	.
</s>
