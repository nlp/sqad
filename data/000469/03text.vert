<s>
Lima	Lima	k1gFnSc1	Lima
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Lima	Lima	k1gFnSc1	Lima
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Peru	Peru	k1gNnSc1	Peru
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
administrativní	administrativní	k2eAgNnSc4d1	administrativní
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
a	a	k8xC	a
kulturní	kulturní	k2eAgNnSc4d1	kulturní
centrum	centrum	k1gNnSc4	centrum
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
zdaleka	zdaleka	k6eAd1	zdaleka
největším	veliký	k2eAgNnSc7d3	veliký
peruánským	peruánský	k2eAgNnSc7d1	peruánské
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Lima	Lima	k1gFnSc1	Lima
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
přímořské	přímořský	k2eAgFnSc6d1	přímořská
rovině	rovina	k1gFnSc6	rovina
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
úpatí	úpatí	k1gNnSc6	úpatí
And	Anda	k1gFnPc2	Anda
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
12	[number]	k4	12
km	km	kA	km
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
průměrně	průměrně	k6eAd1	průměrně
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
185	[number]	k4	185
m	m	kA	m
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
11	[number]	k4	11
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
238	[number]	k4	238
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
23	[number]	k4	23
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
okolo	okolo	k7c2	okolo
15	[number]	k4	15
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
spadne	spadnout	k5eAaPmIp3nS	spadnout
průměrně	průměrně	k6eAd1	průměrně
3	[number]	k4	3
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
5	[number]	k4	5
mm	mm	kA	mm
<g/>
,	,	kIx,	,
celoročně	celoročně	k6eAd1	celoročně
pouze	pouze	k6eAd1	pouze
20	[number]	k4	20
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Lima	Lima	k1gFnSc1	Lima
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejsušších	suchý	k2eAgNnPc2d3	nejsušší
hlavních	hlavní	k2eAgNnPc2d1	hlavní
měst	město	k1gNnPc2	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgFnSc1d1	zdejší
délka	délka	k1gFnSc1	délka
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
činí	činit	k5eAaImIp3nS	činit
pouze	pouze	k6eAd1	pouze
1284	[number]	k4	1284
hodin	hodina	k1gFnPc2	hodina
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
28,6	[number]	k4	28,6
hodin	hodina	k1gFnPc2	hodina
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
179,1	[number]	k4	179,1
hodin	hodina	k1gFnPc2	hodina
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
výjimečně	výjimečně	k6eAd1	výjimečně
nízké	nízký	k2eAgFnPc1d1	nízká
hodnoty	hodnota	k1gFnPc1	hodnota
<g/>
,	,	kIx,	,
s	s	k7c7	s
přihlédnutím	přihlédnutí	k1gNnSc7	přihlédnutí
k	k	k7c3	k
zeměpisné	zeměpisný	k2eAgFnSc3d1	zeměpisná
poloze	poloha	k1gFnSc3	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Limy	Lima	k1gFnSc2	Lima
<g/>
.	.	kIx.	.
</s>
<s>
Lima	Lima	k1gFnSc1	Lima
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1535	[number]	k4	1535
španělským	španělský	k2eAgMnSc7d1	španělský
dobyvatelem	dobyvatel	k1gMnSc7	dobyvatel
Franciscem	Francisce	k1gMnSc7	Francisce
Pizzarem	Pizzar	k1gMnSc7	Pizzar
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
název	název	k1gInSc1	název
města	město	k1gNnSc2	město
zněl	znět	k5eAaImAgInS	znět
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
los	los	k1gInSc1	los
Reyes	Reyes	k1gInSc1	Reyes
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Město	město	k1gNnSc1	město
králů	král	k1gMnPc2	král
<g/>
)	)	kIx)	)
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
sídlem	sídlo	k1gNnSc7	sídlo
peruánských	peruánský	k2eAgMnPc2d1	peruánský
místokrálů	místokrál	k1gMnPc2	místokrál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1551	[number]	k4	1551
zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
nejstarší	starý	k2eAgFnSc2d3	nejstarší
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1563	[number]	k4	1563
ji	on	k3xPp3gFnSc4	on
následovalo	následovat	k5eAaImAgNnS	následovat
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
Peru	Peru	k1gNnSc1	Peru
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Lima	Lima	k1gFnSc1	Lima
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
se	se	k3xPyFc4	se
Lima	Lima	k1gFnSc1	Lima
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1852	[number]	k4	1852
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
totiž	totiž	k9	totiž
postavena	postaven	k2eAgFnSc1d1	postavena
nejstarší	starý	k2eAgFnSc1d3	nejstarší
železnice	železnice	k1gFnSc1	železnice
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
spojovala	spojovat	k5eAaImAgFnS	spojovat
Limu	Lima	k1gFnSc4	Lima
a	a	k8xC	a
přístav	přístav	k1gInSc4	přístav
Callao	Callao	k6eAd1	Callao
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
bylo	být	k5eAaImAgNnS	být
historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
mnoho	mnoho	k4c4	mnoho
budov	budova	k1gFnPc2	budova
vzniknuvších	vzniknuvší	k2eAgFnPc2d1	vzniknuvší
v	v	k7c6	v
době	doba	k1gFnSc6	doba
španělské	španělský	k2eAgFnSc2d1	španělská
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
moderní	moderní	k2eAgFnSc1d1	moderní
výstavba	výstavba	k1gFnSc1	výstavba
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Náměstí	náměstí	k1gNnSc1	náměstí
Plaza	plaz	k1gMnSc2	plaz
de	de	k?	de
Armas	Armas	k1gInSc1	Armas
je	být	k5eAaImIp3nS	být
pomníkem	pomník	k1gInSc7	pomník
koloniálního	koloniální	k2eAgNnSc2d1	koloniální
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
paláce	palác	k1gInSc2	palác
jej	on	k3xPp3gMnSc4	on
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
patří	patřit	k5eAaImIp3nS	patřit
městská	městský	k2eAgFnSc1d1	městská
radnice	radnice	k1gFnSc1	radnice
Cabildo	Cabildo	k1gNnSc4	Cabildo
<g/>
,	,	kIx,	,
Arcibiskupský	arcibiskupský	k2eAgInSc4d1	arcibiskupský
palác	palác	k1gInSc4	palác
s	s	k7c7	s
vyřezávanými	vyřezávaný	k2eAgInPc7d1	vyřezávaný
dřevěnými	dřevěný	k2eAgInPc7d1	dřevěný
balkony	balkon	k1gInPc7	balkon
<g/>
,	,	kIx,	,
katedrála	katedrála	k1gFnSc1	katedrála
(	(	kIx(	(
<g/>
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
znovuvybudování	znovuvybudování	k1gNnSc6	znovuvybudování
po	po	k7c6	po
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1746	[number]	k4	1746
se	se	k3xPyFc4	se
podíl	podíl	k1gInSc1	podíl
i	i	k9	i
český	český	k2eAgMnSc1d1	český
jezuita	jezuita	k1gMnSc1	jezuita
Jan	Jan	k1gMnSc1	Jan
Rohra	Rohra	k1gMnSc1	Rohra
<g/>
)	)	kIx)	)
a	a	k8xC	a
palác	palác	k1gInSc4	palác
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgInSc1d1	obchodní
střed	střed	k1gInSc1	střed
Limy	Lima	k1gFnSc2	Lima
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
Plaza	plaz	k1gMnSc2	plaz
Dos	Dos	k1gMnSc2	Dos
de	de	k?	de
Mayo	Mayo	k1gMnSc1	Mayo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
tvar	tvar	k1gInSc1	tvar
je	být	k5eAaImIp3nS	být
kruhovitý	kruhovitý	k2eAgInSc1d1	kruhovitý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtích	čtvrt	k1gFnPc6	čtvrt
Santiago	Santiago	k1gNnSc1	Santiago
de	de	k?	de
Surco	Surco	k1gNnSc1	Surco
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
<g/>
,	,	kIx,	,
Miraflores	Miraflores	k1gInSc1	Miraflores
a	a	k8xC	a
San	San	k1gFnSc1	San
Isidro	Isidra	k1gFnSc5	Isidra
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
vily	vila	k1gFnPc1	vila
peruánské	peruánský	k2eAgFnSc2d1	peruánská
smetánky	smetánka	k1gFnSc2	smetánka
a	a	k8xC	a
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
<g/>
.	.	kIx.	.
</s>
<s>
Náměstí	náměstí	k1gNnSc1	náměstí
Plaza	plaz	k1gMnSc2	plaz
San	San	k1gMnSc2	San
Martín	Martín	k1gMnSc1	Martín
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
zakladatele	zakladatel	k1gMnSc2	zakladatel
republiky	republika	k1gFnSc2	republika
Peru	Peru	k1gNnSc1	Peru
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
období	období	k1gNnSc2	období
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Limě	Lima	k1gFnSc6	Lima
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
parní	parní	k2eAgFnSc2d1	parní
železnice	železnice	k1gFnSc2	železnice
na	na	k7c6	na
jihoamerickém	jihoamerický	k2eAgInSc6d1	jihoamerický
kontinentě	kontinent	k1gInSc6	kontinent
-	-	kIx~	-
trať	trať	k1gFnSc1	trať
Lima	Lima	k1gFnSc1	Lima
-	-	kIx~	-
Callao	Callao	k1gMnSc1	Callao
<g/>
.	.	kIx.	.
</s>
<s>
Koněspřežná	koněspřežný	k2eAgFnSc1d1	koněspřežná
tramvaj	tramvaj	k1gFnSc1	tramvaj
započala	započnout	k5eAaPmAgFnS	započnout
provoz	provoz	k1gInSc4	provoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
24	[number]	k4	24
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1878	[number]	k4	1878
-	-	kIx~	-
provozovatelem	provozovatel	k1gMnSc7	provozovatel
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
Empresa	Empresa	k1gFnSc1	Empresa
de	de	k?	de
Tramways	Tramways	k1gInSc1	Tramways
de	de	k?	de
Lima	Lima	k1gFnSc1	Lima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
měla	mít	k5eAaImAgFnS	mít
síť	síť	k1gFnSc1	síť
3	[number]	k4	3
linky	linka	k1gFnPc4	linka
<g/>
,	,	kIx,	,
66	[number]	k4	66
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
40	[number]	k4	40
vozů	vůz	k1gInPc2	vůz
a	a	k8xC	a
400	[number]	k4	400
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Elektrické	elektrický	k2eAgFnPc1d1	elektrická
tramvaje	tramvaj	k1gFnPc1	tramvaj
společnosti	společnost	k1gFnSc2	společnost
Tranvía	Tranvía	k1gMnSc1	Tranvía
Eléctrico	Eléctrico	k1gMnSc1	Eléctrico
de	de	k?	de
Lima	Lima	k1gFnSc1	Lima
y	y	k?	y
Chorrillos	Chorrillos	k1gInSc1	Chorrillos
započaly	započnout	k5eAaPmAgFnP	započnout
svůj	svůj	k3xOyFgInSc4	svůj
provoz	provoz	k1gInSc4	provoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
společností	společnost	k1gFnSc7	společnost
Compañ	Compañ	k1gFnPc2	Compañ
de	de	k?	de
Automóviles	Automóviles	k1gMnSc1	Automóviles
de	de	k?	de
Lima	Lima	k1gFnSc1	Lima
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
nový	nový	k2eAgInSc1d1	nový
dopravní	dopravní	k2eAgInSc1d1	dopravní
prostředek	prostředek	k1gInSc1	prostředek
-	-	kIx~	-
6	[number]	k4	6
autobusů	autobus	k1gInPc2	autobus
s	s	k7c7	s
elektrickým	elektrický	k2eAgInSc7d1	elektrický
pohonem	pohon	k1gInSc7	pohon
napájeným	napájený	k2eAgInSc7d1	napájený
z	z	k7c2	z
baterií	baterie	k1gFnPc2	baterie
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
motory	motor	k1gInPc1	motor
byly	být	k5eAaImAgInP	být
příliš	příliš	k6eAd1	příliš
slabé	slabý	k2eAgInPc1d1	slabý
a	a	k8xC	a
baterie	baterie	k1gFnPc4	baterie
často	často	k6eAd1	často
hořely	hořet	k5eAaImAgInP	hořet
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
celý	celý	k2eAgInSc1d1	celý
experiment	experiment	k1gInSc1	experiment
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
měsících	měsíc	k1gInPc6	měsíc
provozu	provoz	k1gInSc2	provoz
skončil	skončit	k5eAaPmAgInS	skončit
a	a	k8xC	a
autobusy	autobus	k1gInPc1	autobus
byly	být	k5eAaImAgInP	být
přestavěny	přestavět	k5eAaPmNgInP	přestavět
na	na	k7c4	na
benzínový	benzínový	k2eAgInSc4d1	benzínový
pohon	pohon	k1gInSc4	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
také	také	k9	také
probíhala	probíhat	k5eAaImAgFnS	probíhat
elektrizace	elektrizace	k1gFnSc1	elektrizace
tratí	trať	k1gFnPc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
sítě	síť	k1gFnSc2	síť
nastal	nastat	k5eAaPmAgInS	nastat
zlom	zlom	k1gInSc1	zlom
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1928	[number]	k4	1928
-	-	kIx~	-
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
tratí	trať	k1gFnPc2	trať
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
limská	limský	k2eAgFnSc1d1	Limská
dopravní	dopravní	k2eAgFnSc1d1	dopravní
společnost	společnost	k1gFnSc1	společnost
Lima	limo	k1gNnSc2	limo
Light	Lighta	k1gFnPc2	Lighta
<g/>
,	,	kIx,	,
Power	Power	k1gInSc1	Power
&	&	k?	&
Tramways	Tramways	k1gInSc4	Tramways
Company	Compana	k1gFnSc2	Compana
objednala	objednat	k5eAaPmAgFnS	objednat
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
6	[number]	k4	6
trolejbusů	trolejbus	k1gInPc2	trolejbus
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
nasazeny	nasadit	k5eAaPmNgFnP	nasadit
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
bývalé	bývalý	k2eAgFnSc2d1	bývalá
okružní	okružní	k2eAgFnSc2d1	okružní
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
linky	linka	k1gFnSc2	linka
č.	č.	k?	č.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
provoz	provoz	k1gInSc1	provoz
však	však	k9	však
trval	trvat	k5eAaImAgInS	trvat
pouhé	pouhý	k2eAgInPc4d1	pouhý
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byly	být	k5eAaImAgInP	být
trolejbusy	trolejbus	k1gInPc1	trolejbus
přestavěny	přestavět	k5eAaPmNgInP	přestavět
na	na	k7c4	na
tramvaje	tramvaj	k1gFnPc4	tramvaj
a	a	k8xC	a
ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
udržely	udržet	k5eAaPmAgFnP	udržet
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
až	až	k6eAd1	až
do	do	k7c2	do
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jediný	jediný	k2eAgInSc4d1	jediný
známý	známý	k2eAgInSc4d1	známý
případ	případ	k1gInSc4	případ
přestavby	přestavba	k1gFnSc2	přestavba
trolejbusu	trolejbus	k1gInSc2	trolejbus
na	na	k7c4	na
tramvaj	tramvaj	k1gFnSc4	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
nastal	nastat	k5eAaPmAgInS	nastat
útlum	útlum	k1gInSc1	útlum
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
jen	jen	k9	jen
24	[number]	k4	24
vozů	vůz	k1gInPc2	vůz
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
150	[number]	k4	150
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
byl	být	k5eAaImAgInS	být
provoz	provoz	k1gInSc1	provoz
tramvají	tramvaj	k1gFnPc2	tramvaj
ukončen	ukončen	k2eAgMnSc1d1	ukončen
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dvě	dva	k4xCgNnPc4	dva
desetiletí	desetiletí	k1gNnPc4	desetiletí
později	pozdě	k6eAd2	pozdě
započala	započnout	k5eAaPmAgFnS	započnout
výstavba	výstavba	k1gFnSc1	výstavba
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Soukup	Soukup	k1gMnSc1	Soukup
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
salesiánský	salesiánský	k2eAgMnSc1d1	salesiánský
misionář	misionář	k1gMnSc1	misionář
<g/>
,	,	kIx,	,
botanik	botanik	k1gMnSc1	botanik
<g/>
;	;	kIx,	;
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Lima	limo	k1gNnSc2	limo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lima	limo	k1gNnSc2	limo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
