<p>
<s>
Santorini	Santorin	k1gMnPc1	Santorin
<g/>
,	,	kIx,	,
Santorin	santorin	k1gInSc1	santorin
nebo	nebo	k8xC	nebo
Théra	Théra	k1gFnSc1	Théra
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Σ	Σ	k?	Σ
nebo	nebo	k8xC	nebo
Θ	Θ	k?	Θ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
souostroví	souostroví	k1gNnSc1	souostroví
a	a	k8xC	a
obec	obec	k1gFnSc1	obec
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
Egejského	egejský	k2eAgNnSc2d1	Egejské
a	a	k8xC	a
Krétského	krétský	k2eAgNnSc2d1	krétské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
souostroví	souostroví	k1gNnSc1	souostroví
Kyklady	Kyklady	k1gFnPc1	Kyklady
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnPc3d1	hlavní
a	a	k8xC	a
zdaleka	zdaleka	k6eAd1	zdaleka
největším	veliký	k2eAgInSc7d3	veliký
ostrovem	ostrov	k1gInSc7	ostrov
<g/>
,	,	kIx,	,
obepínajícím	obepínající	k2eAgMnSc7d1	obepínající
obloukovitě	obloukovitě	k6eAd1	obloukovitě
celý	celý	k2eAgInSc4d1	celý
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
východ	východ	k1gInSc4	východ
a	a	k8xC	a
jih	jih	k1gInSc4	jih
souostroví	souostroví	k1gNnSc2	souostroví
je	být	k5eAaImIp3nS	být
Théra	Théra	k1gFnSc1	Théra
<g/>
,	,	kIx,	,
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
jeho	jeho	k3xOp3gInSc1	jeho
protějšek	protějšek	k1gInSc1	protějšek
tvoří	tvořit	k5eAaImIp3nS	tvořit
ostrov	ostrov	k1gInSc4	ostrov
Thirasia	Thirasia	k1gFnSc1	Thirasia
a	a	k8xC	a
obvod	obvod	k1gInSc1	obvod
na	na	k7c6	na
západě	západ	k1gInSc6	západ
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
ostrůvek	ostrůvek	k1gInSc1	ostrůvek
Aspronisi	Aspronise	k1gFnSc4	Aspronise
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k6eAd1	uprostřed
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
leží	ležet	k5eAaImIp3nP	ležet
dvojice	dvojice	k1gFnPc1	dvojice
ostrovů	ostrov	k1gInPc2	ostrov
Nea	Nea	k1gFnSc1	Nea
Kaimeni	Kaimen	k2eAgMnPc1d1	Kaimen
a	a	k8xC	a
Palea	palea	k1gFnSc1	palea
Kaimeni	Kaimen	k2eAgMnPc1d1	Kaimen
<g/>
.	.	kIx.	.
</s>
<s>
Souostroví	souostroví	k1gNnSc1	souostroví
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
110	[number]	k4	110
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Kréty	Kréta	k1gFnSc2	Kréta
a	a	k8xC	a
220	[number]	k4	220
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Athén	Athéna	k1gFnPc2	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
zároveň	zároveň	k6eAd1	zároveň
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
obec	obec	k1gFnSc4	obec
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
92,5	[number]	k4	92,5
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
Profitis	Profitis	k1gFnSc1	Profitis
Ilias	Ilias	k1gFnSc1	Ilias
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Théra	Théro	k1gNnSc2	Théro
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
567	[number]	k4	567
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
obecní	obecní	k2eAgFnSc6d1	obecní
jednotce	jednotka	k1gFnSc6	jednotka
15550	[number]	k4	15550
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
souostroví	souostroví	k1gNnSc1	souostroví
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednu	jeden	k4xCgFnSc4	jeden
obec	obec	k1gFnSc4	obec
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
obecní	obecní	k2eAgFnPc4d1	obecní
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
komunit	komunita	k1gFnPc2	komunita
a	a	k8xC	a
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
měst	město	k1gNnPc2	město
a	a	k8xC	a
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
komunit	komunita	k1gFnPc2	komunita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
Théra	Théra	k1gFnSc1	Théra
(	(	kIx(	(
<g/>
15550	[number]	k4	15550
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obecní	obecní	k2eAgFnSc1d1	obecní
jednotka	jednotka	k1gFnSc1	jednotka
Théra	Théra	k1gFnSc1	Théra
(	(	kIx(	(
<g/>
14005	[number]	k4	14005
<g/>
)	)	kIx)	)
–	–	k?	–
Akrotiri	Akrotiri	k1gNnSc4	Akrotiri
(	(	kIx(	(
<g/>
489	[number]	k4	489
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Emborios	Emborios	k1gMnSc1	Emborios
(	(	kIx(	(
<g/>
3085	[number]	k4	3085
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Episkopi	Episkopi	k1gNnSc1	Episkopi
Gonias	Goniasa	k1gFnPc2	Goniasa
(	(	kIx(	(
<g/>
1462	[number]	k4	1462
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Exo	Exo	k1gMnSc1	Exo
Gonias	Gonias	k1gMnSc1	Gonias
(	(	kIx(	(
<g/>
395	[number]	k4	395
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fira	Fira	k1gMnSc1	Fira
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Imerovigli	Imerovigl	k1gMnPc1	Imerovigl
(	(	kIx(	(
<g/>
535	[number]	k4	535
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Katerados	Katerados	k1gMnSc1	Katerados
(	(	kIx(	(
<g/>
1293	[number]	k4	1293
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Megalochori	Megalochori	k1gNnSc1	Megalochori
(	(	kIx(	(
<g/>
594	[number]	k4	594
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mesaria	Mesarium	k1gNnPc1	Mesarium
(	(	kIx(	(
<g/>
2092	[number]	k4	2092
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pirgos	Pirgos	k1gInSc4	Pirgos
Kallisti	Kallist	k1gMnPc1	Kallist
(	(	kIx(	(
<g/>
912	[number]	k4	912
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vothonas	Vothonas	k1gMnSc1	Vothonas
(	(	kIx(	(
<g/>
756	[number]	k4	756
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vouvoulos	Vouvoulos	k1gMnSc1	Vouvoulos
(	(	kIx(	(
<g/>
535	[number]	k4	535
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Obecní	obecní	k2eAgFnSc1d1	obecní
jednotka	jednotka	k1gFnSc1	jednotka
Oia	Oia	k1gFnSc2	Oia
(	(	kIx(	(
<g/>
1545	[number]	k4	1545
<g/>
)	)	kIx)	)
–	–	k?	–
Oia	Oia	k1gFnSc1	Oia
(	(	kIx(	(
<g/>
1226	[number]	k4	1226
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Thirasia	Thirasia	k1gFnSc1	Thirasia
(	(	kIx(	(
<g/>
319	[number]	k4	319
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Města	město	k1gNnSc2	město
a	a	k8xC	a
turistická	turistický	k2eAgNnPc1d1	turistické
letoviska	letovisko	k1gNnPc1	letovisko
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Západní	západní	k2eAgNnSc4d1	západní
pobřeží	pobřeží	k1gNnSc4	pobřeží
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
ostrova	ostrov	k1gInSc2	ostrov
Santorini	Santorin	k1gMnPc1	Santorin
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jeho	jeho	k3xOp3gNnSc7	jeho
správním	správní	k2eAgNnSc7d1	správní
centrem	centrum	k1gNnSc7	centrum
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
Fira	Firum	k1gNnSc2	Firum
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
asi	asi	k9	asi
260	[number]	k4	260
m.	m.	k?	m.
Dole	dole	k6eAd1	dole
pod	pod	k7c7	pod
městem	město	k1gNnSc7	město
leží	ležet	k5eAaImIp3nS	ležet
přístav	přístav	k1gInSc4	přístav
Skala	Skala	k1gMnSc1	Skala
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
sopečného	sopečný	k2eAgInSc2d1	sopečný
kráteru	kráter	k1gInSc2	kráter
najdeme	najít	k5eAaPmIp1nP	najít
kromě	kromě	k7c2	kromě
Firy	Fira	k1gFnSc2	Fira
hned	hned	k6eAd1	hned
několik	několik	k4yIc4	několik
známých	známý	k2eAgNnPc2d1	známé
turistických	turistický	k2eAgNnPc2d1	turistické
letovisek	letovisko	k1gNnPc2	letovisko
-	-	kIx~	-
město	město	k1gNnSc1	město
Oia	Oia	k1gFnSc2	Oia
proslavené	proslavený	k2eAgFnSc2d1	proslavená
svými	svůj	k3xOyFgInPc7	svůj
nádhernými	nádherný	k2eAgInPc7d1	nádherný
západy	západ	k1gInPc7	západ
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
Imerovigli	Imerovigle	k1gFnSc4	Imerovigle
s	s	k7c7	s
pozůstatky	pozůstatek	k1gInPc7	pozůstatek
středověkého	středověký	k2eAgInSc2d1	středověký
hradu	hrad	k1gInSc2	hrad
Skaros	Skarosa	k1gFnPc2	Skarosa
nebo	nebo	k8xC	nebo
například	například	k6eAd1	například
Akrotiri	Akrotir	k1gMnPc1	Akrotir
s	s	k7c7	s
archeologickým	archeologický	k2eAgNnSc7d1	Archeologické
nalezištěm	naleziště	k1gNnSc7	naleziště
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
památkám	památka	k1gFnPc3	památka
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Východní	východní	k2eAgNnSc4d1	východní
pobřeží	pobřeží	k1gNnSc4	pobřeží
===	===	k?	===
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
západní	západní	k2eAgNnSc1d1	západní
pobřeží	pobřeží	k1gNnSc1	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
těší	těšit	k5eAaImIp3nS	těšit
návštěvníky	návštěvník	k1gMnPc4	návštěvník
krásným	krásný	k2eAgInSc7d1	krásný
výhledem	výhled	k1gInSc7	výhled
do	do	k7c2	do
dalekého	daleký	k2eAgNnSc2d1	daleké
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Santorini	Santorin	k1gMnPc1	Santorin
se	se	k3xPyFc4	se
rozprostírají	rozprostírat	k5eAaImIp3nP	rozprostírat
jedinečné	jedinečný	k2eAgFnPc4d1	jedinečná
pláže	pláž	k1gFnPc4	pláž
pokryté	pokrytý	k2eAgFnPc4d1	pokrytá
sopečným	sopečný	k2eAgInSc7d1	sopečný
pískem	písek	k1gInSc7	písek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgNnPc3d3	nejznámější
městům	město	k1gNnPc3	město
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
patří	patřit	k5eAaImIp3nP	patřit
především	především	k9	především
přímořská	přímořský	k2eAgNnPc1d1	přímořské
střediska	středisko	k1gNnPc1	středisko
Kamari	Kamar	k1gFnSc2	Kamar
a	a	k8xC	a
Perissa	Perissa	k1gFnSc1	Perissa
včetně	včetně	k7c2	včetně
blízkého	blízký	k2eAgNnSc2d1	blízké
archeologického	archeologický	k2eAgNnSc2d1	Archeologické
naleziště	naleziště	k1gNnSc2	naleziště
starověkého	starověký	k2eAgNnSc2d1	starověké
města	město	k1gNnSc2	město
Théra	Thér	k1gInSc2	Thér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
ostrova	ostrov	k1gInSc2	ostrov
===	===	k?	===
</s>
</p>
<p>
<s>
Vnitrozemí	vnitrozemí	k1gNnSc1	vnitrozemí
ostrova	ostrov	k1gInSc2	ostrov
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
hlavně	hlavně	k9	hlavně
malé	malý	k2eAgNnSc1d1	malé
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
tradiční	tradiční	k2eAgFnSc2d1	tradiční
řecké	řecký	k2eAgFnSc2d1	řecká
vesničky	vesnička	k1gFnSc2	vesnička
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
turistický	turistický	k2eAgInSc1d1	turistický
ruch	ruch	k1gInSc1	ruch
ještě	ještě	k9	ještě
moc	moc	k6eAd1	moc
nezasáhl	zasáhnout	k5eNaPmAgMnS	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
například	například	k6eAd1	například
vesničky	vesnička	k1gFnPc4	vesnička
Emporio	Emporio	k6eAd1	Emporio
<g/>
,	,	kIx,	,
Mesa	Mes	k2eAgFnSc1d1	Mesa
Gonia	Gonia	k1gFnSc1	Gonia
či	či	k8xC	či
Karterados	Karterados	k1gMnSc1	Karterados
s	s	k7c7	s
několika	několik	k4yIc7	několik
krásnými	krásný	k2eAgInPc7d1	krásný
kostely	kostel	k1gInPc7	kostel
a	a	k8xC	a
kapličkami	kaplička	k1gFnPc7	kaplička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
ostrova	ostrov	k1gInSc2	ostrov
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
jen	jen	k9	jen
obvodové	obvodový	k2eAgFnPc1d1	obvodová
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
obklopující	obklopující	k2eAgFnPc1d1	obklopující
až	až	k9	až
400	[number]	k4	400
m	m	kA	m
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
lagunu	laguna	k1gFnSc4	laguna
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
oválný	oválný	k2eAgInSc1d1	oválný
tvar	tvar	k1gInSc1	tvar
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
zhruba	zhruba	k6eAd1	zhruba
17	[number]	k4	17
km	km	kA	km
od	od	k7c2	od
severozápadu	severozápad	k1gInSc2	severozápad
k	k	k7c3	k
jihovýchodu	jihovýchod	k1gInSc3	jihovýchod
a	a	k8xC	a
šířce	šířka	k1gFnSc6	šířka
13	[number]	k4	13
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
z	z	k7c2	z
vnější	vnější	k2eAgFnSc2d1	vnější
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
povrch	povrch	k6eAd1wR	povrch
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
zvedá	zvedat	k5eAaImIp3nS	zvedat
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
dovnitř	dovnitř	k6eAd1	dovnitř
laguny	laguna	k1gFnPc1	laguna
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
200	[number]	k4	200
až	až	k9	až
300	[number]	k4	300
m	m	kA	m
vysoké	vysoký	k2eAgFnSc2d1	vysoká
skalní	skalní	k2eAgFnSc2d1	skalní
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostrovy	ostrov	k1gInPc1	ostrov
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Název	název	k1gInSc1	název
===	===	k?	===
</s>
</p>
<p>
<s>
Současný	současný	k2eAgInSc1d1	současný
název	název	k1gInSc1	název
Santorini	Santorin	k1gMnPc1	Santorin
dostalo	dostat	k5eAaPmAgNnS	dostat
souostroví	souostroví	k1gNnSc3	souostroví
až	až	k6eAd1	až
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
latinského	latinský	k2eAgNnSc2d1	latinské
císařství	císařství	k1gNnSc2	císařství
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
podle	podle	k7c2	podle
svaté	svatý	k2eAgFnSc2d1	svatá
Ireny	Irena	k1gFnSc2	Irena
(	(	kIx(	(
<g/>
Sancta	Sancta	k1gMnSc1	Sancta
Irene	Iren	k1gInSc5	Iren
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgInPc1d1	dřívější
názvy	název	k1gInPc1	název
souostroví	souostroví	k1gNnSc2	souostroví
zněly	znět	k5eAaImAgInP	znět
Kallístē	Kallístē	k1gFnSc2	Kallístē
(	(	kIx(	(
<g/>
Κ	Κ	k?	Κ
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ta	ten	k3xDgFnSc1	ten
nejkrásnější	krásný	k2eAgFnSc1d3	nejkrásnější
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Strongýlē	Strongýlē	k1gFnSc1	Strongýlē
(	(	kIx(	(
<g/>
Σ	Σ	k?	Σ
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kruhová	kruhový	k2eAgFnSc1d1	kruhová
<g/>
"	"	kIx"	"
resp.	resp.	kA	resp.
"	"	kIx"	"
<g/>
kulatá	kulatý	k2eAgFnSc1d1	kulatá
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Thē	Thē	k1gFnSc1	Thē
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vazba	vazba	k1gFnSc1	vazba
na	na	k7c4	na
České	český	k2eAgFnPc4d1	Česká
země	zem	k1gFnPc4	zem
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1477	[number]	k4	1477
byl	být	k5eAaImAgInS	být
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
na	na	k7c4	na
titulárního	titulární	k2eAgMnSc4d1	titulární
biskupa	biskup	k1gMnSc4	biskup
baziliky	bazilika	k1gFnSc2	bazilika
sv.	sv.	kA	sv.
Ireny	Irena	k1gFnSc2	Irena
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Santorini	Santorin	k1gMnPc1	Santorin
Augustin	Augustin	k1gMnSc1	Augustin
Luciani	Lucian	k1gMnPc1	Lucian
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1482	[number]	k4	1482
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
1493	[number]	k4	1493
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
utrakvistům	utrakvista	k1gMnPc3	utrakvista
(	(	kIx(	(
<g/>
české	český	k2eAgFnSc3d1	Česká
a	a	k8xC	a
moravské	moravský	k2eAgFnSc3d1	Moravská
církvi	církev	k1gFnSc3	církev
podobojí	podobojí	k2eAgInSc1d1	podobojí
<g/>
)	)	kIx)	)
světil	světit	k5eAaImAgInS	světit
jáhny	jáhen	k1gMnPc4	jáhen
a	a	k8xC	a
kněze	kněz	k1gMnPc4	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgMnSc1d1	boží
<g/>
)	)	kIx)	)
před	před	k7c7	před
Týnem	Týn	k1gInSc7	Týn
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sopečný	sopečný	k2eAgInSc1d1	sopečný
výbuch	výbuch	k1gInSc1	výbuch
===	===	k?	===
</s>
</p>
<p>
<s>
Nynější	nynější	k2eAgNnSc1d1	nynější
souostroví	souostroví	k1gNnSc1	souostroví
je	být	k5eAaImIp3nS	být
zbytkem	zbytek	k1gInSc7	zbytek
velkého	velký	k2eAgInSc2d1	velký
sopečného	sopečný	k2eAgInSc2d1	sopečný
ostrova	ostrov	k1gInSc2	ostrov
(	(	kIx(	(
<g/>
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
Thē	Thē	k1gFnSc2	Thē
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
střed	střed	k1gInSc1	střed
byl	být	k5eAaImAgInS	být
rozmetán	rozmetat	k5eAaPmNgInS	rozmetat
mohutným	mohutný	k2eAgInSc7d1	mohutný
výbuchem	výbuch	k1gInSc7	výbuch
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
sopečné	sopečný	k2eAgFnSc3d1	sopečná
erupci	erupce	k1gFnSc3	erupce
došlo	dojít	k5eAaPmAgNnS	dojít
někdy	někdy	k6eAd1	někdy
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
(	(	kIx(	(
<g/>
Druhá	druhý	k4xOgFnSc1	druhý
přechodná	přechodný	k2eAgFnSc1d1	přechodná
doba	doba	k1gFnSc1	doba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejsilnějších	silný	k2eAgInPc2d3	nejsilnější
sopečných	sopečný	k2eAgInPc2d1	sopečný
výbuchů	výbuch	k1gInPc2	výbuch
známých	známý	k2eAgInPc2d1	známý
člověku	člověk	k1gMnSc6	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
svou	svůj	k3xOyFgFnSc7	svůj
silou	síla	k1gFnSc7	síla
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
převýšil	převýšit	k5eAaPmAgInS	převýšit
slavný	slavný	k2eAgInSc1d1	slavný
výbuch	výbuch	k1gInSc1	výbuch
sopky	sopka	k1gFnSc2	sopka
Krakatoa	Krakato	k1gInSc2	Krakato
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
odhadů	odhad	k1gInPc2	odhad
až	až	k9	až
100	[number]	k4	100
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jím	jíst	k5eAaImIp1nS	jíst
vyvolaným	vyvolaný	k2eAgFnPc3d1	vyvolaná
klimatickým	klimatický	k2eAgFnPc3d1	klimatická
změnám	změna	k1gFnPc3	změna
a	a	k8xC	a
vlnám	vlna	k1gFnPc3	vlna
tsunami	tsunami	k1gNnSc7	tsunami
je	být	k5eAaImIp3nS	být
připisován	připisován	k2eAgInSc1d1	připisován
pád	pád	k1gInSc1	pád
mínojské	mínojský	k2eAgFnSc2d1	mínojská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
se	se	k3xPyFc4	se
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
z	z	k7c2	z
biblických	biblický	k2eAgFnPc2d1	biblická
deseti	deset	k4xCc2	deset
egyptských	egyptský	k2eAgFnPc2d1	egyptská
ran	rána	k1gFnPc2	rána
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
erupci	erupce	k1gFnSc6	erupce
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
následovala	následovat	k5eAaImAgFnS	následovat
Nová	nový	k2eAgFnSc1d1	nová
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
zakladatel	zakladatel	k1gMnSc1	zakladatel
Ahmose	Ahmosa	k1gFnSc3	Ahmosa
I.	I.	kA	I.
je	být	k5eAaImIp3nS	být
identifikován	identifikovat	k5eAaBmNgInS	identifikovat
jako	jako	k9	jako
faraon	faraon	k1gInSc1	faraon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
Exodus	Exodus	k1gInSc1	Exodus
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
katastrofa	katastrofa	k1gFnSc1	katastrofa
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
zdroj	zdroj	k1gInSc4	zdroj
pověsti	pověst	k1gFnSc2	pověst
o	o	k7c6	o
Atlantidě	Atlantida	k1gFnSc6	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
výbuchu	výbuch	k1gInSc2	výbuch
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
kolem	kolem	k7c2	kolem
2	[number]	k4	2
400	[number]	k4	400
megatun	megatuna	k1gFnPc2	megatuna
TNT	TNT	kA	TNT
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
hirošimská	hirošimský	k2eAgFnSc1d1	hirošimská
bomba	bomba	k1gFnSc1	bomba
explodovala	explodovat	k5eAaBmAgFnS	explodovat
se	s	k7c7	s
silou	síla	k1gFnSc7	síla
15	[number]	k4	15
kilotun	kilotuna	k1gFnPc2	kilotuna
TNT	TNT	kA	TNT
<g/>
,	,	kIx,	,
Théra	Théra	k1gFnSc1	Théra
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vyrovnala	vyrovnat	k5eAaPmAgFnS	vyrovnat
svojí	svůj	k3xOyFgFnSc7	svůj
silou	síla	k1gFnSc7	síla
160	[number]	k4	160
000	[number]	k4	000
hirošimským	hirošimský	k2eAgFnPc3d1	hirošimská
bombám	bomba	k1gFnPc3	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgMnSc1d3	nejsilnější
lidstvem	lidstvo	k1gNnSc7	lidstvo
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
bomba	bomba	k1gFnSc1	bomba
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
car-bomba	caromba	k1gFnSc1	car-bomba
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
i	i	k9	i
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
síle	síla	k1gFnSc6	síla
výbuchu	výbuch	k1gInSc2	výbuch
50	[number]	k4	50
megatun	megatuna	k1gFnPc2	megatuna
TNT	TNT	kA	TNT
stále	stále	k6eAd1	stále
48	[number]	k4	48
<g/>
krát	krát	k6eAd1	krát
slabší	slabý	k2eAgInPc1d2	slabší
než	než	k8xS	než
výbuch	výbuch	k1gInSc1	výbuch
ostrova	ostrov	k1gInSc2	ostrov
Théra	Théro	k1gNnSc2	Théro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Náboženství	náboženství	k1gNnSc2	náboženství
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
minorita	minorita	k1gFnSc1	minorita
<g/>
.	.	kIx.	.
</s>
<s>
Diecéze	diecéze	k1gFnSc2	diecéze
Santorini	Santorin	k2eAgMnPc1d1	Santorin
(	(	kIx(	(
<g/>
Théra	Théra	k1gMnSc1	Théra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sufragánní	sufragánní	k2eAgFnSc1d1	sufragánní
(	(	kIx(	(
<g/>
podřízená	podřízený	k2eAgFnSc1d1	podřízená
<g/>
)	)	kIx)	)
arcidiecézi	arcidiecéze	k1gFnSc4	arcidiecéze
Naxos	Naxos	k1gInSc4	Naxos
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1204	[number]	k4	1204
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
farnosti	farnost	k1gFnSc6	farnost
1	[number]	k4	1
řeholního	řeholní	k2eAgMnSc2d1	řeholní
kněze	kněz	k1gMnSc2	kněz
<g/>
,	,	kIx,	,
1	[number]	k4	1
stálého	stálý	k2eAgMnSc4d1	stálý
jáhna	jáhen	k1gMnSc4	jáhen
<g/>
,	,	kIx,	,
1	[number]	k4	1
řeholníka	řeholník	k1gMnSc4	řeholník
a	a	k8xC	a
16	[number]	k4	16
řeholnic	řeholnice	k1gFnPc2	řeholnice
a	a	k8xC	a
500	[number]	k4	500
věřících	věřící	k1gFnPc2	věřící
(	(	kIx(	(
<g/>
tvořili	tvořit	k5eAaImAgMnP	tvořit
tak	tak	k6eAd1	tak
4	[number]	k4	4
%	%	kIx~	%
z	z	k7c2	z
12	[number]	k4	12
360	[number]	k4	360
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Santorin	santorin	k1gInSc1	santorin
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Santorini	Santorin	k2eAgMnPc1d1	Santorin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Santorini	Santorin	k1gMnPc1	Santorin
–	–	k?	–
kaldera	kaldera	k1gFnSc1	kaldera
(	(	kIx(	(
<g/>
webcam	webcam	k1gInSc1	webcam
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc4	ostrov
Santorini	Santorin	k1gMnPc1	Santorin
-	-	kIx~	-
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Kalimera	Kalimera	k1gFnSc1	Kalimera
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podrobné	podrobný	k2eAgInPc1d1	podrobný
popisy	popis	k1gInPc1	popis
památek	památka	k1gFnPc2	památka
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Théra	Théro	k1gNnSc2	Théro
<g/>
,	,	kIx,	,
alias	alias	k9	alias
Santorini	Santorin	k1gMnPc1	Santorin
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
