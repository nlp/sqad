<s>
Nynější	nynější	k2eAgNnSc1d1	nynější
souostroví	souostroví	k1gNnSc1	souostroví
je	být	k5eAaImIp3nS	být
zbytkem	zbytek	k1gInSc7	zbytek
velkého	velký	k2eAgInSc2d1	velký
sopečného	sopečný	k2eAgInSc2d1	sopečný
ostrova	ostrov	k1gInSc2	ostrov
(	(	kIx(	(
<g/>
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
Thē	Thē	k1gFnSc2	Thē
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
střed	střed	k1gInSc1	střed
byl	být	k5eAaImAgInS	být
rozmetán	rozmetat	k5eAaPmNgInS	rozmetat
mohutným	mohutný	k2eAgInSc7d1	mohutný
výbuchem	výbuch	k1gInSc7	výbuch
<g/>
.	.	kIx.	.
</s>
