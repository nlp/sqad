<s>
Boberská	Boberský	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuPřírodní	infoboxuPřírodní	k2eAgFnSc3d1
památkaBoberská	památkaBoberský	k2eAgFnSc1d1
stráň	stráň	k1gFnSc4
Letecký	letecký	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
stráňZákladní	stráňZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1981	#num#	k4
Vyhlásil	vyhlásit	k5eAaPmAgMnS
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
kultury	kultura	k1gFnSc2
ČSR	ČSR	kA
Nadm	Nadm	k1gMnSc1
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
660	#num#	k4
-	-	kIx~
850	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
18,86	18,86	k4
ha	ha	kA
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Trutnov	Trutnov	k1gInSc1
Umístění	umístění	k1gNnSc1
</s>
<s>
Žacléř	Žacléř	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
39	#num#	k4
<g/>
′	′	k?
<g/>
9,49	9,49	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
<g/>
24,56	24,56	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Boberská	Boberský	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc1
</s>
<s>
285	#num#	k4
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Boberská	Boberský	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
byla	být	k5eAaImAgFnS
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
ev.	ev.	k?
č.	č.	k?
285	#num#	k4
na	na	k7c6
severovýchodních	severovýchodní	k2eAgInPc6d1
svazích	svah	k1gInPc6
Žacléřského	žacléřský	k2eAgInSc2d1
hřbetu	hřbet	k1gInSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
poblíž	poblíž	k7c2
obce	obec	k1gFnSc2
Žacléř	Žacléř	k1gInSc1
v	v	k7c6
okrese	okres	k1gInSc6
Trutnov	Trutnov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
spravovala	spravovat	k5eAaImAgFnS
Správa	správa	k1gFnSc1
KRNAP	KRNAP	kA
<g/>
.	.	kIx.
</s>
<s>
Důvodem	důvod	k1gInSc7
ochrany	ochrana	k1gFnSc2
byla	být	k5eAaImAgFnS
typická	typický	k2eAgFnSc1d1
submontánní	submontánní	k2eAgFnSc1d1
květnatá	květnatý	k2eAgFnSc1d1
bučina	bučina	k1gFnSc1
a	a	k8xC
suťový	suťový	k2eAgInSc1d1
porost	porost	k1gInSc1
v	v	k7c6
Krkonoších	Krkonoše	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památka	památka	k1gFnSc1
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
dubnu	duben	k1gInSc6
2008	#num#	k4
kvůli	kvůli	k7c3
začlenění	začlenění	k1gNnSc3
do	do	k7c2
NP	NP	kA
Krkonoše	Krkonoše	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Pohled	pohled	k1gInSc1
do	do	k7c2
svahu	svah	k1gInSc2
</s>
<s>
Krmelec	krmelec	k1gInSc1
</s>
<s>
Záryp	Záryp	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Trutnov	Trutnov	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
RYBÁŘ	Rybář	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přírodou	příroda	k1gFnSc7
od	od	k7c2
Krkonoš	Krkonoše	k1gFnPc2
po	po	k7c4
Vysočinu	vysočina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
:	:	kIx,
Kruh	kruh	k1gInSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
392	#num#	k4
s.	s.	k?
46	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
89	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Boberská	Boberský	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
