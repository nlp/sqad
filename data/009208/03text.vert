<p>
<s>
Válečkové	válečkový	k2eAgInPc1d1	válečkový
dopravníky	dopravník	k1gInPc1	dopravník
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
dopravě	doprava	k1gFnSc3	doprava
a	a	k8xC	a
manipulaci	manipulace	k1gFnSc4	manipulace
s	s	k7c7	s
kusovými	kusový	k2eAgInPc7d1	kusový
předměty	předmět	k1gInPc7	předmět
(	(	kIx(	(
<g/>
kartonové	kartonový	k2eAgFnSc2d1	kartonová
krabice	krabice	k1gFnSc2	krabice
<g/>
,	,	kIx,	,
plastové	plastový	k2eAgFnSc2d1	plastová
přepravky	přepravka	k1gFnSc2	přepravka
-	-	kIx~	-
KLT	KLT	kA	KLT
boxy	box	k1gInPc1	box
<g/>
,	,	kIx,	,
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
palety	paleta	k1gFnPc1	paleta
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Válečky	válečka	k1gFnPc1	válečka
jsou	být	k5eAaImIp3nP	být
usazeny	usadit	k5eAaPmNgFnP	usadit
v	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
rámu	rám	k1gInSc6	rám
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
souvislou	souvislý	k2eAgFnSc4d1	souvislá
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
konstrukčním	konstrukční	k2eAgInSc7d1	konstrukční
prvkem	prvek	k1gInSc7	prvek
dopravníku	dopravník	k1gInSc2	dopravník
je	být	k5eAaImIp3nS	být
transportní	transportní	k2eAgInSc4d1	transportní
váleček	váleček	k1gInSc4	váleček
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
rozměry	rozměr	k1gInPc1	rozměr
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
<g/>
,	,	kIx,	,
průměr	průměr	k1gInSc1	průměr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nosnost	nosnost	k1gFnSc4	nosnost
a	a	k8xC	a
materiálové	materiálový	k2eAgNnSc4d1	materiálové
provedení	provedení	k1gNnSc4	provedení
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
závislé	závislý	k2eAgFnPc1d1	závislá
na	na	k7c6	na
charakteru	charakter	k1gInSc6	charakter
dopravovaného	dopravovaný	k2eAgInSc2d1	dopravovaný
předmětu	předmět	k1gInSc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Válečkové	válečkový	k2eAgInPc1d1	válečkový
dopravníky	dopravník	k1gInPc1	dopravník
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nepoháněné	poháněný	k2eNgFnPc1d1	nepoháněná
a	a	k8xC	a
poháněné	poháněný	k2eAgFnPc1d1	poháněná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
nepoháněných	poháněný	k2eNgInPc2d1	poháněný
dopravníků	dopravník	k1gInPc2	dopravník
se	se	k3xPyFc4	se
zboží	zboží	k1gNnSc1	zboží
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
díky	díky	k7c3	díky
gravitační	gravitační	k2eAgFnSc3d1	gravitační
síle	síla	k1gFnSc3	síla
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
manuálním	manuální	k2eAgInSc7d1	manuální
posunem	posun	k1gInSc7	posun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poháněné	poháněný	k2eAgInPc1d1	poháněný
dopravníky	dopravník	k1gInPc1	dopravník
jsou	být	k5eAaImIp3nP	být
opatřeny	opatřen	k2eAgInPc1d1	opatřen
pohonem	pohon	k1gInSc7	pohon
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
použití	použití	k1gNnSc2	použití
dopravník	dopravník	k1gInSc1	dopravník
jsou	být	k5eAaImIp3nP	být
požívány	požíván	k2eAgInPc4d1	požíván
motory	motor	k1gInPc4	motor
s	s	k7c7	s
převodovkou	převodovka	k1gFnSc7	převodovka
nebo	nebo	k8xC	nebo
motor	motor	k1gInSc1	motor
zabudovaný	zabudovaný	k2eAgInSc1d1	zabudovaný
ve	v	k7c6	v
válečku	váleček	k1gInSc6	váleček
-	-	kIx~	-
rollerdrive	rollerdrivat	k5eAaPmIp3nS	rollerdrivat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
ozubených	ozubený	k2eAgInPc2d1	ozubený
řemenů	řemen	k1gInPc2	řemen
<g/>
,	,	kIx,	,
kruhových	kruhový	k2eAgInPc2d1	kruhový
řemínků	řemínek	k1gInPc2	řemínek
<g/>
,	,	kIx,	,
válečkových	válečkový	k2eAgInPc2d1	válečkový
řetězů	řetěz	k1gInPc2	řetěz
(	(	kIx(	(
<g/>
řetěz	řetěz	k1gInSc1	řetěz
z	z	k7c2	z
válečku	váleček	k1gInSc2	váleček
na	na	k7c4	na
váleček	váleček	k1gInSc4	váleček
nebo	nebo	k8xC	nebo
tečný	tečný	k2eAgInSc4d1	tečný
řetěz	řetěz	k1gInSc4	řetěz
<g/>
)	)	kIx)	)
či	či	k8xC	či
tečným	tečný	k2eAgInSc7d1	tečný
řemenem	řemen	k1gInSc7	řemen
pohání	pohánět	k5eAaImIp3nP	pohánět
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
válečky	válečka	k1gFnPc4	válečka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sílu	síla	k1gFnSc4	síla
dále	daleko	k6eAd2	daleko
přenášejí	přenášet	k5eAaImIp3nP	přenášet
na	na	k7c4	na
dopravovaný	dopravovaný	k2eAgInSc4d1	dopravovaný
předmět	předmět	k1gInSc4	předmět
a	a	k8xC	a
uvádějí	uvádět	k5eAaImIp3nP	uvádět
jej	on	k3xPp3gNnSc4	on
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Válečkový	válečkový	k2eAgInSc4d1	válečkový
dopravník	dopravník	k1gInSc4	dopravník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
