<p>
<s>
Alojzov	Alojzov	k1gInSc1	Alojzov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Aloisdorf	Aloisdorf	k1gInSc1	Aloisdorf
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
asi	asi	k9	asi
8	[number]	k4	8
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Prostějova	Prostějov	k1gInSc2	Prostějov
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
243	[number]	k4	243
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
184	[number]	k4	184
popisných	popisný	k2eAgNnPc2d1	popisné
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
333	[number]	k4	333
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1783	[number]	k4	1783
<g/>
.	.	kIx.	.
</s>
<s>
Silnice	silnice	k1gFnPc1	silnice
se	se	k3xPyFc4	se
v	v	k7c6	v
Alojzově	Alojzův	k2eAgNnSc6d1	Alojzův
stavěla	stavět	k5eAaImAgFnS	stavět
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Struktura	struktura	k1gFnSc1	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
obec	obec	k1gFnSc4	obec
i	i	k9	i
za	za	k7c4	za
jeho	jeho	k3xOp3gFnPc4	jeho
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
uvádí	uvádět	k5eAaImIp3nS	uvádět
tabulka	tabulka	k1gFnSc1	tabulka
níže	níže	k1gFnSc1	níže
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
i	i	k9	i
příslušnost	příslušnost	k1gFnSc4	příslušnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
k	k	k7c3	k
obci	obec	k1gFnSc3	obec
či	či	k8xC	či
následné	následný	k2eAgNnSc4d1	následné
odtržení	odtržení	k1gNnSc4	odtržení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Spálený	spálený	k2eAgInSc1d1	spálený
kopec	kopec	k1gInSc1	kopec
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Alojzov	Alojzovo	k1gNnPc2	Alojzovo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Alojzov	Alojzov	k1gInSc1	Alojzov
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
