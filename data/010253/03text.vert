<p>
<s>
Havran	Havran	k1gMnSc1	Havran
(	(	kIx(	(
<g/>
v	v	k7c4	v
orig	orig	k1gInSc4	orig
<g/>
.	.	kIx.	.
The	The	k?	The
Raven	Raven	k1gInSc1	Raven
<g/>
,	,	kIx,	,
v	v	k7c6	v
přesném	přesný	k2eAgInSc6d1	přesný
překladu	překlad	k1gInSc6	překlad
Krkavec	krkavec	k1gMnSc1	krkavec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
básní	báseň	k1gFnPc2	báseň
významného	významný	k2eAgMnSc2d1	významný
amerického	americký	k2eAgMnSc2d1	americký
básníka	básník	k1gMnSc2	básník
Edgara	Edgar	k1gMnSc2	Edgar
Allana	Allan	k1gMnSc2	Allan
Poea	Poeus	k1gMnSc2	Poeus
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1845	[number]	k4	1845
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Evening	Evening	k1gInSc1	Evening
Mirror	Mirror	k1gInSc1	Mirror
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
==	==	k?	==
</s>
</p>
<p>
<s>
Lyrickým	lyrický	k2eAgInSc7d1	lyrický
subjektem	subjekt	k1gInSc7	subjekt
(	(	kIx(	(
<g/>
vypravěčem	vypravěč	k1gMnSc7	vypravěč
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
osamělý	osamělý	k2eAgMnSc1d1	osamělý
muž	muž	k1gMnSc1	muž
trýzněný	trýzněný	k2eAgMnSc1d1	trýzněný
horečkou	horečka	k1gFnSc7	horečka
a	a	k8xC	a
především	především	k6eAd1	především
mučivými	mučivý	k2eAgFnPc7d1	mučivá
vzpomínkami	vzpomínka	k1gFnPc7	vzpomínka
na	na	k7c4	na
zemřelou	zemřelá	k1gFnSc4	zemřelá
dívku	dívka	k1gFnSc4	dívka
Lenoru	Lenora	k1gFnSc4	Lenora
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
marně	marně	k6eAd1	marně
hledá	hledat	k5eAaImIp3nS	hledat
cestu	cesta	k1gFnSc4	cesta
čtením	čtení	k1gNnSc7	čtení
v	v	k7c6	v
okultních	okultní	k2eAgInPc6d1	okultní
svazcích	svazek	k1gInPc6	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Lyrický	lyrický	k2eAgInSc1d1	lyrický
subjekt	subjekt	k1gInSc1	subjekt
vystraší	vystrašit	k5eAaPmIp3nS	vystrašit
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
šramot	šramot	k1gInSc4	šramot
<g/>
.	.	kIx.	.
</s>
<s>
Ukáže	ukázat	k5eAaPmIp3nS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
původcem	původce	k1gMnSc7	původce
podivných	podivný	k2eAgInPc2d1	podivný
zvuků	zvuk	k1gInPc2	zvuk
je	být	k5eAaImIp3nS	být
krkavec	krkavec	k1gMnSc1	krkavec
(	(	kIx(	(
<g/>
v	v	k7c6	v
tradičních	tradiční	k2eAgFnPc6d1	tradiční
českých	český	k2eAgFnPc6d1	Česká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgMnPc2d1	jiný
cizojazyčných	cizojazyčný	k2eAgMnPc2d1	cizojazyčný
<g/>
,	,	kIx,	,
překladech	překlad	k1gInPc6	překlad
básně	báseň	k1gFnPc4	báseň
označovaný	označovaný	k2eAgMnSc1d1	označovaný
jako	jako	k8xC	jako
havran	havran	k1gMnSc1	havran
<g/>
,	,	kIx,	,
zoologicky	zoologicky	k6eAd1	zoologicky
sice	sice	k8xC	sice
nesprávně	správně	k6eNd1	správně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
tradiční	tradiční	k2eAgFnSc7d1	tradiční
literární	literární	k2eAgFnSc7d1	literární
symbolikou	symbolika	k1gFnSc7	symbolika
české	český	k2eAgFnSc2d1	Česká
poezie	poezie	k1gFnSc2	poezie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vzápětí	vzápětí	k6eAd1	vzápětí
vletí	vletět	k5eAaPmIp3nS	vletět
do	do	k7c2	do
jeho	on	k3xPp3gInSc2	on
pokoje	pokoj	k1gInSc2	pokoj
a	a	k8xC	a
znepokojuje	znepokojovat	k5eAaImIp3nS	znepokojovat
muže	muž	k1gMnPc4	muž
svou	svůj	k3xOyFgFnSc7	svůj
záhadností	záhadnost	k1gFnSc7	záhadnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
otázky	otázka	k1gFnPc4	otázka
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
míněné	míněný	k2eAgInPc1d1	míněný
žertem	žert	k1gInSc7	žert
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jedním	jeden	k4xCgNnSc7	jeden
slovem	slovo	k1gNnSc7	slovo
–	–	k?	–
nevermore	nevermor	k1gInSc5	nevermor
(	(	kIx(	(
<g/>
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
překládáno	překládat	k5eAaImNgNnS	překládat
většinou	většina	k1gFnSc7	většina
jako	jako	k8xS	jako
víckrát	víckrát	k6eAd1	víckrát
ne	ne	k9	ne
nebo	nebo	k8xC	nebo
nikdy	nikdy	k6eAd1	nikdy
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
ponecháváno	ponechávat	k5eAaImNgNnS	ponechávat
v	v	k7c6	v
originále	originál	k1gInSc6	originál
<g/>
.	.	kIx.	.
</s>
<s>
Zásadně	zásadně	k6eAd1	zásadně
odlišných	odlišný	k2eAgNnPc2d1	odlišné
překladatelských	překladatelský	k2eAgNnPc2d1	překladatelské
řešení	řešení	k1gNnPc2	řešení
tohoto	tento	k3xDgInSc2	tento
refrénu	refrén	k1gInSc2	refrén
není	být	k5eNaImIp3nS	být
mnoho	mnoho	k6eAd1	mnoho
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mužův	mužův	k2eAgInSc1d1	mužův
neklid	neklid	k1gInSc1	neklid
se	se	k3xPyFc4	se
stupňuje	stupňovat	k5eAaImIp3nS	stupňovat
v	v	k7c4	v
paniku	panika	k1gFnSc4	panika
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
v	v	k7c4	v
hrůzu	hrůza	k1gFnSc4	hrůza
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
krkavce	krkavec	k1gMnSc2	krkavec
vyhnat	vyhnat	k5eAaPmF	vyhnat
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
ale	ale	k9	ale
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
lyrický	lyrický	k2eAgInSc1d1	lyrický
subjekt	subjekt	k1gInSc1	subjekt
metaforicky	metaforicky	k6eAd1	metaforicky
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
krkavec	krkavec	k1gMnSc1	krkavec
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
neodletí	odletět	k5eNaPmIp3nS	odletět
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Překlady	překlad	k1gInPc4	překlad
==	==	k?	==
</s>
</p>
<p>
<s>
Havran	Havran	k1gMnSc1	Havran
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeložen	přeložit	k5eAaPmNgInS	přeložit
mnohokrát	mnohokrát	k6eAd1	mnohokrát
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
Vratislavem	Vratislav	k1gMnSc7	Vratislav
Šemberou	Šembera	k1gMnSc7	Šembera
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
následoval	následovat	k5eAaImAgInS	následovat
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
nejúspěšnější	úspěšný	k2eAgInSc4d3	nejúspěšnější
překlad	překlad	k1gInSc4	překlad
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Vrchlického	Vrchlický	k1gMnSc2	Vrchlický
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
a	a	k8xC	a
napodruhé	napodruhé	k6eAd1	napodruhé
1890	[number]	k4	1890
i	i	k8xC	i
v	v	k7c6	v
parodii	parodie	k1gFnSc6	parodie
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
klasické	klasický	k2eAgInPc1d1	klasický
překlady	překlad	k1gInPc1	překlad
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
Augustin	Augustin	k1gMnSc1	Augustin
Eugen	Eugen	k2eAgInSc4d1	Eugen
Mužík	mužík	k1gInSc4	mužík
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Dostál-Lutinov	Dostál-Lutinov	k1gInSc4	Dostál-Lutinov
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
nejpoužívanějším	používaný	k2eAgInSc7d3	nejpoužívanější
překladem	překlad	k1gInSc7	překlad
ten	ten	k3xDgMnSc1	ten
od	od	k7c2	od
Vítězslava	Vítězslav	k1gMnSc2	Vítězslav
Nezvala	Nezval	k1gMnSc2	Nezval
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Překladů	překlad	k1gInPc2	překlad
básně	báseň	k1gFnSc2	báseň
ovšem	ovšem	k9	ovšem
existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
od	od	k7c2	od
O.	O.	kA	O.
F.	F.	kA	F.
Bablera	Babler	k1gMnSc2	Babler
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Jiřího	Jiří	k1gMnSc2	Jiří
Taufera	Taufero	k1gNnSc2	Taufero
1938	[number]	k4	1938
a	a	k8xC	a
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
času	čas	k1gInSc2	čas
přišly	přijít	k5eAaPmAgFnP	přijít
další	další	k2eAgFnPc1d1	další
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
století	století	k1gNnSc2	století
<g/>
:	:	kIx,	:
Miroslav	Miroslav	k1gMnSc1	Miroslav
Macek	Macek	k1gMnSc1	Macek
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Pokorný	Pokorný	k1gMnSc1	Pokorný
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Najser	Najser	k1gMnSc1	Najser
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
Filip	Filip	k1gMnSc1	Filip
Krajník	krajník	k1gMnSc1	krajník
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
přidali	přidat	k5eAaPmAgMnP	přidat
Marek	Marek	k1gMnSc1	Marek
Řezanka	řezanka	k1gFnSc1	řezanka
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Jacko	Jacko	k1gNnSc4	Jacko
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Z.	Z.	kA	Z.
J.	J.	kA	J.
Pinkava	Pinkava	k?	Pinkava
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
Barbora	Barbora	k1gFnSc1	Barbora
Pastyříková	Pastyříková	k1gFnSc1	Pastyříková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
cizích	cizí	k2eAgInPc2d1	cizí
překladů	překlad	k1gInPc2	překlad
platí	platit	k5eAaImIp3nS	platit
za	za	k7c4	za
nejpozoruhodnější	pozoruhodný	k2eAgInSc4d3	nejpozoruhodnější
francouzský	francouzský	k2eAgInSc4d1	francouzský
překlad	překlad	k1gInSc4	překlad
Charlese	Charles	k1gMnSc2	Charles
Baudelaira	Baudelair	k1gMnSc2	Baudelair
<g/>
,	,	kIx,	,
pořízený	pořízený	k2eAgInSc4d1	pořízený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Báseň	báseň	k1gFnSc1	báseň
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
problémů	problém	k1gInPc2	problém
translatologie	translatologie	k1gFnSc2	translatologie
a	a	k8xC	a
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
další	další	k2eAgInPc4d1	další
pokusy	pokus	k1gInPc4	pokus
<g/>
,	,	kIx,	,
dostupné	dostupný	k2eAgInPc4d1	dostupný
na	na	k7c6	na
webu	web	k1gInSc6	web
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Martin	Martin	k1gMnSc1	Martin
Kozák	Kozák	k1gMnSc1	Kozák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Adaptace	adaptace	k1gFnSc1	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
Poe	Poe	k?	Poe
napsal	napsat	k5eAaBmAgInS	napsat
tuto	tento	k3xDgFnSc4	tento
báseň	báseň	k1gFnSc4	báseň
jako	jako	k8xC	jako
ukázku	ukázka	k1gFnSc4	ukázka
své	svůj	k3xOyFgFnSc2	svůj
tvůrčí	tvůrčí	k2eAgFnSc2d1	tvůrčí
metody	metoda	k1gFnSc2	metoda
(	(	kIx(	(
<g/>
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgMnSc6	tento
duchu	duch	k1gMnSc6	duch
ji	on	k3xPp3gFnSc4	on
okomentoval	okomentovat	k5eAaPmAgMnS	okomentovat
esejem	esej	k1gInSc7	esej
Filozofie	filozofie	k1gFnSc2	filozofie
básnické	básnický	k2eAgFnSc2d1	básnická
skladby	skladba	k1gFnSc2	skladba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
básní	báseň	k1gFnSc7	báseň
se	se	k3xPyFc4	se
rázem	rázem	k6eAd1	rázem
proslavil	proslavit	k5eAaPmAgMnS	proslavit
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
kratších	krátký	k2eAgFnPc2d2	kratší
básní	báseň	k1gFnPc2	báseň
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
108	[number]	k4	108
veršů	verš	k1gInPc2	verš
<g/>
)	)	kIx)	)
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
děl	dělo	k1gNnPc2	dělo
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Báseň	báseň	k1gFnSc1	báseň
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
známá	známý	k2eAgFnSc1d1	známá
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
anglofonních	anglofonní	k2eAgFnPc6d1	anglofonní
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
dočkala	dočkat	k5eAaPmAgFnS	dočkat
se	se	k3xPyFc4	se
mnoha	mnoho	k4c2	mnoho
odkazů	odkaz	k1gInPc2	odkaz
<g/>
,	,	kIx,	,
ohlasů	ohlas	k1gInPc2	ohlas
i	i	k8xC	i
parodií	parodie	k1gFnPc2	parodie
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
dílech	dílo	k1gNnPc6	dílo
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
v	v	k7c6	v
mírně	mírně	k6eAd1	mírně
zkrácené	zkrácený	k2eAgFnSc6d1	zkrácená
verzi	verze	k1gFnSc6	verze
"	"	kIx"	"
<g/>
zfilmována	zfilmován	k2eAgFnSc1d1	zfilmována
<g/>
"	"	kIx"	"
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
značně	značně	k6eAd1	značně
rozvinuta	rozvinout	k5eAaPmNgFnS	rozvinout
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
<g/>
,	,	kIx,	,
komediální	komediální	k2eAgFnSc6d1	komediální
adaptaci	adaptace	k1gFnSc6	adaptace
Havran	Havran	k1gMnSc1	Havran
Rogera	Rogera	k1gFnSc1	Rogera
Cormana	Corman	k1gMnSc2	Corman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
francouzské	francouzský	k2eAgMnPc4d1	francouzský
prokleté	prokletý	k2eAgMnPc4d1	prokletý
básníky	básník	k1gMnPc4	básník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
MACURA	Macura	k1gMnSc1	Macura
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc4	slovník
světových	světový	k2eAgNnPc2d1	světové
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
2	[number]	k4	2
<g/>
/	/	kIx~	/
M-	M-	k1gMnPc2	M-
<g/>
Ž.	Ž.	kA	Ž.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
960	[number]	k4	960
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
459	[number]	k4	459
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Havran	Havran	k1gMnSc1	Havran
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Havran	Havran	k1gMnSc1	Havran
ve	v	k7c4	v
WikizdrojíchEdgar	WikizdrojíchEdgar	k1gInSc4	WikizdrojíchEdgar
Allan	Allan	k1gMnSc1	Allan
Poe	Poe	k1gMnSc1	Poe
<g/>
:	:	kIx,	:
Havran	Havran	k1gMnSc1	Havran
<g/>
,	,	kIx,	,
Filozofie	filozofie	k1gFnSc1	filozofie
básnické	básnický	k2eAgFnSc2d1	básnická
skladby	skladba	k1gFnSc2	skladba
</s>
</p>
