<p>
<s>
Jih	jih	k1gInSc1	jih
<g/>
,	,	kIx,	,
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
češtině	čeština	k1gFnSc6	čeština
též	též	k9	též
poledne	poledne	k1gNnSc4	poledne
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
světových	světový	k2eAgFnPc2d1	světová
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Zkratkou	zkratka	k1gFnSc7	zkratka
je	být	k5eAaImIp3nS	být
písmeno	písmeno	k1gNnSc1	písmeno
"	"	kIx"	"
<g/>
J	J	kA	J
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
S	s	k7c7	s
<g/>
"	"	kIx"	"
z	z	k7c2	z
německého	německý	k2eAgNnSc2d1	německé
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
Süd	Süd	k1gFnSc1	Süd
<g/>
"	"	kIx"	"
či	či	k8xC	či
anglického	anglický	k2eAgMnSc2d1	anglický
"	"	kIx"	"
<g/>
South	South	k1gInSc1	South
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Definice	definice	k1gFnSc2	definice
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zeměpisný	zeměpisný	k2eAgInSc1d1	zeměpisný
jižní	jižní	k2eAgInSc1d1	jižní
pól	pól	k1gInSc1	pól
===	===	k?	===
</s>
</p>
<p>
<s>
Skutečný	skutečný	k2eAgInSc1d1	skutečný
jih	jih	k1gInSc1	jih
je	být	k5eAaImIp3nS	být
směr	směr	k1gInSc4	směr
k	k	k7c3	k
jižnímu	jižní	k2eAgInSc3d1	jižní
pólu	pól	k1gInSc3	pól
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgNnSc2d1	jiné
rotujícího	rotující	k2eAgNnSc2d1	rotující
tělesa	těleso	k1gNnSc2	těleso
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
pokud	pokud	k8xS	pokud
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
otáčení	otáčení	k1gNnSc2	otáčení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Magnetický	magnetický	k2eAgInSc1d1	magnetický
jižní	jižní	k2eAgInSc1d1	jižní
pól	pól	k1gInSc1	pól
===	===	k?	===
</s>
</p>
<p>
<s>
Střelka	střelka	k1gFnSc1	střelka
kompasu	kompas	k1gInSc2	kompas
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
směr	směr	k1gInSc1	směr
k	k	k7c3	k
jižnímu	jižní	k2eAgInSc3d1	jižní
magnetickému	magnetický	k2eAgInSc3d1	magnetický
pólu	pól	k1gInSc3	pól
<g/>
.	.	kIx.	.
</s>
<s>
Odchylka	odchylka	k1gFnSc1	odchylka
mezi	mezi	k7c7	mezi
zeměpisným	zeměpisný	k2eAgInSc7d1	zeměpisný
a	a	k8xC	a
magnetickým	magnetický	k2eAgInSc7d1	magnetický
severem	sever	k1gInSc7	sever
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
magnetická	magnetický	k2eAgFnSc1d1	magnetická
deklinace	deklinace	k1gFnSc1	deklinace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
jihu	jih	k1gInSc2	jih
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
lze	lze	k6eAd1	lze
jih	jih	k1gInSc4	jih
přibližně	přibližně	k6eAd1	přibližně
určit	určit	k5eAaPmF	určit
snadno	snadno	k6eAd1	snadno
podle	podle	k7c2	podle
hodin	hodina	k1gFnPc2	hodina
–	–	k?	–
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
směr	směr	k1gInSc1	směr
Slunce	slunce	k1gNnSc2	slunce
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
v	v	k7c6	v
zenitu	zenit	k1gInSc6	zenit
<g/>
;	;	kIx,	;
odtud	odtud	k6eAd1	odtud
i	i	k9	i
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
polední	polední	k2eAgFnSc1d1	polední
strana	strana	k1gFnSc1	strana
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
má	mít	k5eAaImIp3nS	mít
jih	jih	k1gInSc4	jih
tradičně	tradičně	k6eAd1	tradičně
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
význam	význam	k1gInSc4	význam
<g/>
;	;	kIx,	;
např.	např.	kA	např.
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
domy	dům	k1gInPc4	dům
<g/>
,	,	kIx,	,
města	město	k1gNnPc4	město
i	i	k8xC	i
trůn	trůn	k1gInSc4	trůn
císaře	císař	k1gMnSc2	císař
orientovány	orientovat	k5eAaBmNgFnP	orientovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
rovník	rovník	k1gInSc1	rovník
</s>
</p>
<p>
<s>
poledník	poledník	k1gMnSc1	poledník
</s>
</p>
<p>
<s>
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
</s>
</p>
<p>
<s>
sever	sever	k1gInSc1	sever
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
jih	jih	k1gInSc1	jih
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Jih	jih	k1gInSc1	jih
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
jih	jih	k1gInSc1	jih
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
