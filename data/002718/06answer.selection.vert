<s>
Sněžka	Sněžka	k1gFnSc1	Sněžka
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Śnieżka	Śnieżka	k1gFnSc1	Śnieżka
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Schneekoppe	Schneekopp	k1gMnSc5	Schneekopp
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
1603,30	[number]	k4	1603,30
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
uváděný	uváděný	k2eAgInSc1d1	uváděný
údaj	údaj	k1gInSc1	údaj
1602	[number]	k4	1602
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
je	být	k5eAaImIp3nS	být
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
trigonometrického	trigonometrický	k2eAgInSc2d1	trigonometrický
bodu	bod	k1gInSc2	bod
<g/>
)	)	kIx)	)
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Hraničního	hraniční	k2eAgInSc2d1	hraniční
(	(	kIx(	(
<g/>
Slezského	slezský	k2eAgInSc2d1	slezský
<g/>
)	)	kIx)	)
hřebenu	hřeben	k1gInSc2	hřeben
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
,	,	kIx,	,
Sudet	Sudety	k1gFnPc2	Sudety
<g/>
,	,	kIx,	,
Čech	Čechy	k1gFnPc2	Čechy
i	i	k8xC	i
celého	celý	k2eAgNnSc2d1	celé
Česka	Česko	k1gNnSc2	Česko
a	a	k8xC	a
celého	celý	k2eAgNnSc2d1	celé
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
