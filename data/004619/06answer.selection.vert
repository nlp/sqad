<s>
Repertoár	repertoár	k1gInSc1	repertoár
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc4	slovo
pocházející	pocházející	k2eAgNnSc4d1	pocházející
z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
soubor	soubor	k1gInSc1	soubor
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
nebo	nebo	k8xC	nebo
hudebních	hudební	k2eAgFnPc2d1	hudební
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
programu	program	k1gInSc6	program
umělec	umělec	k1gMnSc1	umělec
či	či	k8xC	či
umělecký	umělecký	k2eAgInSc1d1	umělecký
soubor	soubor	k1gInSc1	soubor
<g/>
.	.	kIx.	.
</s>
