<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
soubor	soubor	k1gInSc1	soubor
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
nebo	nebo	k8xC	nebo
hudebních	hudební	k2eAgFnPc2d1	hudební
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
programu	program	k1gInSc6	program
umělec	umělec	k1gMnSc1	umělec
či	či	k8xC	či
umělecký	umělecký	k2eAgInSc1d1	umělecký
soubor	soubor	k1gInSc1	soubor
<g/>
?	?	kIx.	?
</s>
