<s>
Repertoár	repertoár	k1gInSc1	repertoár
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc4	slovo
pocházející	pocházející	k2eAgNnSc4d1	pocházející
z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
soubor	soubor	k1gInSc1	soubor
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
nebo	nebo	k8xC	nebo
hudebních	hudební	k2eAgFnPc2d1	hudební
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
programu	program	k1gInSc6	program
umělec	umělec	k1gMnSc1	umělec
či	či	k8xC	či
umělecký	umělecký	k2eAgInSc1d1	umělecký
soubor	soubor	k1gInSc1	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Přípravou	příprava	k1gFnSc7	příprava
repertoáru	repertoár	k1gInSc2	repertoár
se	se	k3xPyFc4	se
u	u	k7c2	u
velkých	velký	k2eAgInPc2d1	velký
uměleckých	umělecký	k2eAgInPc2d1	umělecký
souborů	soubor	k1gInPc2	soubor
a	a	k8xC	a
těles	těleso	k1gNnPc2	těleso
zabývá	zabývat	k5eAaImIp3nS	zabývat
umělecký	umělecký	k2eAgInSc4d1	umělecký
obor	obor	k1gInSc4	obor
dramaturgie	dramaturgie	k1gFnSc2	dramaturgie
<g/>
,	,	kIx,	,
pracovníci	pracovník	k1gMnPc1	pracovník
působící	působící	k2eAgMnPc1d1	působící
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
uměleckém	umělecký	k2eAgInSc6d1	umělecký
oboru	obor	k1gInSc6	obor
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
dramaturgové	dramaturg	k1gMnPc1	dramaturg
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
repertoár	repertoár	k1gInSc1	repertoár
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
