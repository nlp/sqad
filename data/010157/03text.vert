<p>
<s>
Žofka	Žofka	k1gFnSc1	Žofka
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
československý	československý	k2eAgInSc1d1	československý
animovaný	animovaný	k2eAgInSc1d1	animovaný
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
vysílaný	vysílaný	k2eAgInSc1d1	vysílaný
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Večerníčku	večerníček	k1gInSc2	večerníček
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
scénáře	scénář	k1gInSc2	scénář
byli	být	k5eAaImAgMnP	být
Miloš	Miloš	k1gMnSc1	Miloš
Macourek	Macourek	k1gMnSc1	Macourek
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Doubrava	Doubrava	k1gMnSc1	Doubrava
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc4	seriál
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
výtvarník	výtvarník	k1gMnSc1	výtvarník
Adolf	Adolf	k1gMnSc1	Adolf
Born	Born	k1gMnSc1	Born
<g/>
,	,	kIx,	,
za	za	k7c7	za
kamerou	kamera	k1gFnSc7	kamera
stála	stát	k5eAaImAgFnS	stát
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Zimová	Zimová	k1gFnSc1	Zimová
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
seriálu	seriál	k1gInSc3	seriál
složil	složit	k5eAaPmAgMnS	složit
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Havlík	Havlík	k1gMnSc1	Havlík
<g/>
.	.	kIx.	.
</s>
<s>
Režii	režie	k1gFnSc3	režie
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
Miloš	Miloš	k1gMnSc1	Miloš
Macourek	Macourek	k1gMnSc1	Macourek
<g/>
.	.	kIx.	.
</s>
<s>
Pohádky	pohádka	k1gFnSc2	pohádka
svým	svůj	k3xOyFgInSc7	svůj
hlasem	hlas	k1gInSc7	hlas
obdařil	obdařit	k5eAaPmAgMnS	obdařit
Petr	Petr	k1gMnSc1	Petr
Nárožný	Nárožný	k2eAgMnSc1d1	Nárožný
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
13	[number]	k4	13
dílů	díl	k1gInPc2	díl
po	po	k7c6	po
8	[number]	k4	8
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příběhy	příběh	k1gInPc1	příběh
nezbedné	nezbedný	k2eAgFnSc2d1	nezbedná
opičí	opičí	k2eAgFnSc2d1	opičí
slečny	slečna	k1gFnSc2	slečna
Žofky	Žofka	k1gFnSc2	Žofka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
dílů	díl	k1gInPc2	díl
==	==	k?	==
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
Žofka	Žofka	k1gFnSc1	Žofka
pořádala	pořádat	k5eAaImAgFnS	pořádat
maškarní	maškarní	k2eAgInSc4d1	maškarní
bál	bál	k1gInSc4	bál
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
Žofka	Žofka	k1gFnSc1	Žofka
pořádala	pořádat	k5eAaImAgFnS	pořádat
sportovní	sportovní	k2eAgInSc4d1	sportovní
den	den	k1gInSc4	den
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
Žofka	Žofka	k1gFnSc1	Žofka
pořádala	pořádat	k5eAaImAgFnS	pořádat
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
představení	představení	k1gNnSc4	představení
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
Žofka	Žofka	k1gFnSc1	Žofka
postarala	postarat	k5eAaPmAgFnS	postarat
o	o	k7c4	o
mravní	mravní	k2eAgFnSc4d1	mravní
převýchovu	převýchova	k1gFnSc4	převýchova
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
Žofka	Žofka	k1gFnSc1	Žofka
zachránila	zachránit	k5eAaPmAgFnS	zachránit
pana	pan	k1gMnSc4	pan
Levharta	levhart	k1gMnSc4	levhart
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
Žofka	Žofka	k1gFnSc1	Žofka
umožnila	umožnit	k5eAaPmAgFnS	umožnit
panu	pan	k1gMnSc3	pan
Mrožovi	mrož	k1gMnSc3	mrož
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
sever	sever	k1gInSc4	sever
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
Žofka	Žofka	k1gFnSc1	Žofka
zavedla	zavést	k5eAaPmAgFnS	zavést
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
ochranné	ochranný	k2eAgNnSc4d1	ochranné
zabarvení	zabarvení	k1gNnSc4	zabarvení
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
Žofka	Žofka	k1gFnSc1	Žofka
překazila	překazit	k5eAaPmAgFnS	překazit
školení	školení	k1gNnSc4	školení
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
Žofka	Žofka	k1gFnSc1	Žofka
odhalila	odhalit	k5eAaPmAgFnS	odhalit
zloděje	zloděj	k1gMnPc4	zloděj
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
Žofka	Žofka	k1gFnSc1	Žofka
postarala	postarat	k5eAaPmAgFnS	postarat
o	o	k7c4	o
svatbu	svatba	k1gFnSc4	svatba
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
Žofka	Žofka	k1gFnSc1	Žofka
překazila	překazit	k5eAaPmAgFnS	překazit
chuligánům	chuligán	k1gMnPc3	chuligán
spády	spád	k1gInPc4	spád
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
Žofka	Žofka	k1gFnSc1	Žofka
stala	stát	k5eAaPmAgFnS	stát
ředitelkou	ředitelka	k1gFnSc7	ředitelka
ZOO	zoo	k1gNnSc2	zoo
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
Žofka	Žofka	k1gFnSc1	Žofka
dopustila	dopustit	k5eAaPmAgFnS	dopustit
osudové	osudový	k2eAgFnPc4d1	osudová
chyby	chyba	k1gFnPc4	chyba
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Žofka	Žofka	k1gFnSc1	Žofka
ředitelkou	ředitelka	k1gFnSc7	ředitelka
ZOO	zoo	k1gFnSc7	zoo
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Žofka	Žofka	k1gFnSc1	Žofka
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Žofka	Žofka	k1gFnSc1	Žofka
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
