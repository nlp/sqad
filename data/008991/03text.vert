<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
čistka	čistka	k1gFnSc1	čistka
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Б	Б	k?	Б
ч	ч	k?	ч
<g/>
,	,	kIx,	,
Bal	bal	k1gInSc1	bal
<g/>
'	'	kIx"	'
<g/>
šaja	šaj	k2eAgFnSc1d1	šaj
čistka	čistka	k1gFnSc1	čistka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
označovaná	označovaný	k2eAgFnSc1d1	označovaná
i	i	k9	i
jako	jako	k8xS	jako
Velký	velký	k2eAgInSc1d1	velký
teror	teror	k1gInSc1	teror
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc2	označení
několika	několik	k4yIc2	několik
souvisejících	související	k2eAgFnPc2d1	související
kampaní	kampaň	k1gFnPc2	kampaň
politické	politický	k2eAgFnSc2d1	politická
represe	represe	k1gFnSc2	represe
a	a	k8xC	a
perzekuce	perzekuce	k1gFnSc2	perzekuce
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
Josif	Josif	k1gMnSc1	Josif
Stalin	Stalin	k1gMnSc1	Stalin
spustil	spustit	k5eAaPmAgMnS	spustit
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
odstranit	odstranit	k5eAaPmF	odstranit
zbývající	zbývající	k2eAgFnSc4d1	zbývající
opozici	opozice	k1gFnSc4	opozice
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
neomezenou	omezený	k2eNgFnSc4d1	neomezená
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Čistka	čistka	k1gFnSc1	čistka
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
většiny	většina	k1gFnSc2	většina
vrstev	vrstva	k1gFnPc2	vrstva
sovětské	sovětský	k2eAgFnSc2d1	sovětská
společnosti	společnost	k1gFnSc2	společnost
<g/>
:	:	kIx,	:
samotné	samotný	k2eAgFnSc2d1	samotná
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
úřednictva	úřednictvo	k1gNnSc2	úřednictvo
<g/>
,	,	kIx,	,
důstojnického	důstojnický	k2eAgInSc2d1	důstojnický
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
,	,	kIx,	,
rolnictva	rolnictvo	k1gNnSc2	rolnictvo
(	(	kIx(	(
<g/>
takzvaných	takzvaný	k2eAgMnPc2d1	takzvaný
"	"	kIx"	"
<g/>
kulaků	kulak	k1gMnPc2	kulak
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
etnických	etnický	k2eAgFnPc2d1	etnická
minorit	minorita	k1gFnPc2	minorita
a	a	k8xC	a
náboženských	náboženský	k2eAgFnPc2d1	náboženská
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Záminkou	záminka	k1gFnSc7	záminka
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
rozpoutání	rozpoutání	k1gNnSc3	rozpoutání
byla	být	k5eAaImAgFnS	být
vražda	vražda	k1gFnSc1	vražda
Sergeje	Sergej	k1gMnSc2	Sergej
Kirova	Kirov	k1gInSc2	Kirov
<g/>
,	,	kIx,	,
hlavy	hlava	k1gFnSc2	hlava
leningradské	leningradský	k2eAgFnSc2d1	Leningradská
stranické	stranický	k2eAgFnSc2d1	stranická
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Nejviditelnějším	viditelný	k2eAgInSc7d3	nejviditelnější
projevem	projev	k1gInSc7	projev
čistky	čistka	k1gFnSc2	čistka
byly	být	k5eAaImAgInP	být
takzvané	takzvaný	k2eAgInPc1d1	takzvaný
moskevské	moskevský	k2eAgInPc1d1	moskevský
procesy	proces	k1gInPc1	proces
<g/>
,	,	kIx,	,
veřejná	veřejný	k2eAgFnSc1d1	veřejná
<g/>
,	,	kIx,	,
předem	předem	k6eAd1	předem
secvičená	secvičený	k2eAgFnSc1d1	secvičená
a	a	k8xC	a
rozhodnutá	rozhodnutý	k2eAgFnSc1d1	rozhodnutá
soudní	soudní	k2eAgNnSc4d1	soudní
přelíčení	přelíčení	k1gNnSc4	přelíčení
proti	proti	k7c3	proti
vedoucím	vedoucí	k2eAgMnPc3d1	vedoucí
funkcionářům	funkcionář	k1gMnPc3	funkcionář
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
končící	končící	k2eAgInPc1d1	končící
drakonickými	drakonický	k2eAgInPc7d1	drakonický
rozsudky	rozsudek	k1gInPc7	rozsudek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc4	průběh
==	==	k?	==
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
čistka	čistka	k1gFnSc1	čistka
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
pod	pod	k7c7	pod
šéfem	šéf	k1gMnSc7	šéf
NKVD	NKVD	kA	NKVD
Genrichem	Genrich	k1gMnSc7	Genrich
Jagodou	Jagoda	k1gMnSc7	Jagoda
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
vrcholu	vrchol	k1gInSc6	vrchol
od	od	k7c2	od
září	září	k1gNnSc2	září
1936	[number]	k4	1936
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
1938	[number]	k4	1938
ji	on	k3xPp3gFnSc4	on
řídil	řídit	k5eAaImAgMnS	řídit
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Ježov	Ježovo	k1gNnPc2	Ježovo
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
perioda	perioda	k1gFnSc1	perioda
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
někdy	někdy	k6eAd1	někdy
nazývána	nazýván	k2eAgFnSc1d1	nazývána
"	"	kIx"	"
<g/>
ježovština	ježovština	k1gFnSc1	ježovština
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
Jagoda	Jagoda	k1gFnSc1	Jagoda
i	i	k8xC	i
Ježov	Ježov	k1gInSc1	Ježov
ovšem	ovšem	k9	ovšem
stalinským	stalinský	k2eAgFnPc3d1	stalinská
čistkám	čistka	k1gFnPc3	čistka
zanedlouho	zanedlouho	k6eAd1	zanedlouho
padli	padnout	k5eAaImAgMnP	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
sami	sám	k3xTgMnPc1	sám
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
byl	být	k5eAaImAgInS	být
popraven	popraven	k2eAgInSc1d1	popraven
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
o	o	k7c4	o
necelé	celý	k2eNgInPc4d1	necelý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc4	dva
dva	dva	k4xCgInPc4	dva
ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
vykonávali	vykonávat	k5eAaImAgMnP	vykonávat
přání	přání	k1gNnSc4	přání
a	a	k8xC	a
vůli	vůle	k1gFnSc4	vůle
Stalina	Stalin	k1gMnSc2	Stalin
<g/>
.	.	kIx.	.
</s>
<s>
Ježova	Ježův	k2eAgFnSc1d1	Ježova
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
šéfa	šéf	k1gMnSc2	šéf
NKVD	NKVD	kA	NKVD
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Stalinův	Stalinův	k2eAgMnSc1d1	Stalinův
chráněnec	chráněnec	k1gMnSc1	chráněnec
Lavrentij	Lavrentij	k1gMnSc1	Lavrentij
Berija	Berija	k1gMnSc1	Berija
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oběti	oběť	k1gFnPc1	oběť
čistek	čistka	k1gFnPc2	čistka
byly	být	k5eAaImAgFnP	být
často	často	k6eAd1	často
popravovány	popravován	k2eAgFnPc1d1	popravován
či	či	k8xC	či
vězněny	vězněn	k2eAgFnPc1d1	vězněna
bez	bez	k7c2	bez
řádného	řádný	k2eAgInSc2d1	řádný
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jej	on	k3xPp3gMnSc4	on
předepisoval	předepisovat	k5eAaImAgMnS	předepisovat
i	i	k9	i
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
sovětský	sovětský	k2eAgInSc4d1	sovětský
právní	právní	k2eAgInSc4d1	právní
řád	řád	k1gInSc4	řád
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
takzvaných	takzvaný	k2eAgFnPc2d1	takzvaná
"	"	kIx"	"
<g/>
trojek	trojka	k1gFnPc2	trojka
<g/>
"	"	kIx"	"
složených	složený	k2eAgNnPc2d1	složené
z	z	k7c2	z
důstojníků	důstojník	k1gMnPc2	důstojník
tajné	tajný	k2eAgFnSc2d1	tajná
policie	policie	k1gFnSc2	policie
NKVD	NKVD	kA	NKVD
<g/>
,	,	kIx,	,
tajemníka	tajemník	k1gMnSc2	tajemník
krajského	krajský	k2eAgInSc2d1	krajský
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
krajského	krajský	k2eAgMnSc2d1	krajský
státního	státní	k2eAgMnSc2d1	státní
zástupce	zástupce	k1gMnSc2	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
k	k	k7c3	k
soudům	soud	k1gInPc3	soud
docházelo	docházet	k5eAaImAgNnS	docházet
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
zinscenované	zinscenovaný	k2eAgFnPc4d1	zinscenovaná
a	a	k8xC	a
předem	předem	k6eAd1	předem
rozhodnuté	rozhodnutý	k2eAgInPc1d1	rozhodnutý
politické	politický	k2eAgInPc1d1	politický
procesy	proces	k1gInPc1	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgNnPc6	jenž
bylo	být	k5eAaImAgNnS	být
doznání	doznání	k1gNnSc1	doznání
vynuceno	vynucen	k2eAgNnSc1d1	vynuceno
pod	pod	k7c7	pod
nátlakem	nátlak	k1gInSc7	nátlak
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
mučením	mučení	k1gNnSc7	mučení
obžalovaných	obžalovaný	k1gMnPc2	obžalovaný
<g/>
.	.	kIx.	.
</s>
<s>
Oběti	oběť	k1gFnPc1	oběť
byly	být	k5eAaImAgFnP	být
obviňovány	obviňovat	k5eAaImNgFnP	obviňovat
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
smyšlených	smyšlený	k2eAgNnPc2d1	smyšlené
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
politických	politický	k2eAgMnPc2d1	politický
<g/>
,	,	kIx,	,
zločinů	zločin	k1gInPc2	zločin
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
sabotáže	sabotáž	k1gFnSc2	sabotáž
<g/>
,	,	kIx,	,
špionáže	špionáž	k1gFnSc2	špionáž
nebo	nebo	k8xC	nebo
protisovětské	protisovětský	k2eAgFnSc2d1	protisovětská
agitace	agitace	k1gFnSc2	agitace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
odtajněných	odtajněný	k2eAgInPc2d1	odtajněný
sovětských	sovětský	k2eAgInPc2d1	sovětský
archivů	archiv	k1gInPc2	archiv
NKVD	NKVD	kA	NKVD
během	během	k7c2	během
let	léto	k1gNnPc2	léto
1937	[number]	k4	1937
a	a	k8xC	a
1938	[number]	k4	1938
zatkla	zatknout	k5eAaPmAgFnS	zatknout
1	[number]	k4	1
548	[number]	k4	548
367	[number]	k4	367
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
bylo	být	k5eAaImAgNnS	být
pak	pak	k6eAd1	pak
681	[number]	k4	681
692	[number]	k4	692
zastřeleno	zastřelit	k5eAaPmNgNnS	zastřelit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
znamená	znamenat	k5eAaImIp3nS	znamenat
asi	asi	k9	asi
1000	[number]	k4	1000
poprav	poprava	k1gFnPc2	poprava
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
ježovštiny	ježovština	k1gFnSc2	ježovština
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
950	[number]	k4	950
tisíc	tisíc	k4xCgInSc4	tisíc
až	až	k8xS	až
1,2	[number]	k4	1,2
milionu	milion	k4xCgInSc2	milion
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
neúplné	úplný	k2eNgFnSc3d1	neúplná
dokumentaci	dokumentace	k1gFnSc3	dokumentace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
podstatně	podstatně	k6eAd1	podstatně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
zatčenými	zatčený	k1gMnPc7	zatčený
byli	být	k5eAaImAgMnP	být
spisovatelé	spisovatel	k1gMnPc1	spisovatel
Isaak	Isaak	k1gMnSc1	Isaak
Babel	Babel	k1gMnSc1	Babel
<g/>
,	,	kIx,	,
Varlam	Varlam	k1gInSc1	Varlam
Šalamov	Šalamov	k1gInSc1	Šalamov
<g/>
,	,	kIx,	,
Boris	Boris	k1gMnSc1	Boris
Pilňak	Pilňak	k1gMnSc1	Pilňak
a	a	k8xC	a
Osip	Osip	k1gMnSc1	Osip
Mandelštam	Mandelštam	k1gInSc1	Mandelštam
<g/>
,	,	kIx,	,
letecký	letecký	k2eAgMnSc1d1	letecký
konstruktér	konstruktér	k1gMnSc1	konstruktér
Andrej	Andrej	k1gMnSc1	Andrej
Tupolev	Tupolev	k1gMnSc1	Tupolev
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc2	tvůrce
sovětského	sovětský	k2eAgInSc2d1	sovětský
raketového	raketový	k2eAgInSc2d1	raketový
programu	program	k1gInSc2	program
Sergej	Sergej	k1gMnSc1	Sergej
Koroljov	Koroljovo	k1gNnPc2	Koroljovo
<g/>
,	,	kIx,	,
esperantista	esperantista	k1gMnSc1	esperantista
Vladimir	Vladimir	k1gMnSc1	Vladimir
Varankin	Varankin	k1gMnSc1	Varankin
<g/>
,	,	kIx,	,
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
Branislav	Branislav	k1gMnSc1	Branislav
Taraškievič	Taraškievič	k1gMnSc1	Taraškievič
<g/>
,	,	kIx,	,
diplomaté	diplomat	k1gMnPc1	diplomat
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Krestinskij	Krestinskij	k1gMnSc1	Krestinskij
a	a	k8xC	a
Grigorij	Grigorij	k1gMnSc1	Grigorij
Sokolnikov	Sokolnikov	k1gInSc1	Sokolnikov
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
letec	letec	k1gMnSc1	letec
Jan	Jan	k1gMnSc1	Jan
Březina	Březina	k1gMnSc1	Březina
<g/>
,	,	kIx,	,
bolševičtí	bolševický	k2eAgMnPc1d1	bolševický
revolucionáři	revolucionář	k1gMnPc1	revolucionář
Vladimir	Vladimir	k1gMnSc1	Vladimir
Antonov-Ovsejenko	Antonov-Ovsejenka	k1gFnSc5	Antonov-Ovsejenka
a	a	k8xC	a
Béla	Béla	k1gMnSc1	Béla
Kun	kuna	k1gFnPc2	kuna
<g/>
,	,	kIx,	,
bělogvardějský	bělogvardějský	k2eAgMnSc1d1	bělogvardějský
velitel	velitel	k1gMnSc1	velitel
Anatolij	Anatolij	k1gMnSc1	Anatolij
Pepeljajev	Pepeljajev	k1gMnSc1	Pepeljajev
nebo	nebo	k8xC	nebo
maršál	maršál	k1gMnSc1	maršál
Konstantin	Konstantin	k1gMnSc1	Konstantin
Rokossovskij	Rokossovskij	k1gMnSc1	Rokossovskij
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
byl	být	k5eAaImAgMnS	být
unesen	unést	k5eAaPmNgMnS	unést
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
popraven	popraven	k2eAgMnSc1d1	popraven
bývalý	bývalý	k2eAgMnSc1d1	bývalý
bělogvardějský	bělogvardějský	k2eAgMnSc1d1	bělogvardějský
generál	generál	k1gMnSc1	generál
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
Miller	Miller	k1gMnSc1	Miller
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
události	událost	k1gFnPc1	událost
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
periodizaci	periodizace	k1gFnSc3	periodizace
období	období	k1gNnSc2	období
Velké	velký	k2eAgFnSc2d1	velká
čistky	čistka	k1gFnSc2	čistka
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používají	používat	k5eAaImIp3nP	používat
tyto	tento	k3xDgFnPc4	tento
události	událost	k1gFnPc4	událost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
moskevský	moskevský	k2eAgInSc1d1	moskevský
proces	proces	k1gInSc1	proces
(	(	kIx(	(
<g/>
srpen	srpen	k1gInSc1	srpen
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
ze	z	k7c2	z
série	série	k1gFnSc2	série
tří	tři	k4xCgFnPc2	tři
velkých	velká	k1gFnPc2	velká
"	"	kIx"	"
<g/>
divadelních	divadelní	k2eAgNnPc2d1	divadelní
<g/>
"	"	kIx"	"
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
proti	proti	k7c3	proti
16	[number]	k4	16
členům	člen	k1gInPc3	člen
údajného	údajný	k2eAgNnSc2d1	údajné
"	"	kIx"	"
<g/>
trockisticko-zinovjevovského	trockistickoinovjevovský	k2eAgNnSc2d1	trockisticko-zinovjevovský
teroristického	teroristický	k2eAgNnSc2d1	teroristické
centra	centrum	k1gNnSc2	centrum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
všichni	všechen	k3xTgMnPc1	všechen
poté	poté	k6eAd1	poté
popraveni	popraven	k2eAgMnPc1d1	popraven
<g/>
;	;	kIx,	;
hlavními	hlavní	k2eAgMnPc7d1	hlavní
obžalovanými	obžalovaný	k1gMnPc7	obžalovaný
byli	být	k5eAaImAgMnP	být
prominentní	prominentní	k2eAgMnPc1d1	prominentní
komunističtí	komunistický	k2eAgMnPc1d1	komunistický
vůdci	vůdce	k1gMnPc1	vůdce
Grigorij	Grigorij	k1gMnSc1	Grigorij
Zinovjev	Zinovjev	k1gMnSc1	Zinovjev
a	a	k8xC	a
Lev	Lev	k1gMnSc1	Lev
Kameněv	Kameněv	k1gMnSc1	Kameněv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhý	druhý	k4xOgInSc1	druhý
moskevský	moskevský	k2eAgInSc1d1	moskevský
proces	proces	k1gInSc1	proces
nad	nad	k7c7	nad
17	[number]	k4	17
předními	přední	k2eAgFnPc7d1	přední
funkcionáři	funkcionář	k1gMnPc1	funkcionář
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jehož	jehož	k3xOyRp3gMnPc7	jehož
obžalovanými	obžalovaný	k1gMnPc7	obžalovaný
byl	být	k5eAaImAgMnS	být
například	například	k6eAd1	například
Karl	Karl	k1gMnSc1	Karl
Radek	Radek	k1gMnSc1	Radek
<g/>
.	.	kIx.	.
13	[number]	k4	13
obžalovaných	obžalovaný	k1gMnPc2	obžalovaný
bylo	být	k5eAaImAgNnS	být
zastřeleno	zastřelit	k5eAaPmNgNnS	zastřelit
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
brzy	brzy	k6eAd1	brzy
pomřel	pomřít	k5eAaPmAgInS	pomřít
v	v	k7c6	v
pracovních	pracovní	k2eAgInPc6d1	pracovní
táborech	tábor	k1gInPc6	tábor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
paragraf	paragraf	k1gInSc1	paragraf
58-14	[number]	k4	58-14
trestního	trestní	k2eAgInSc2d1	trestní
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
"	"	kIx"	"
<g/>
kontrarevoluční	kontrarevoluční	k2eAgFnSc6d1	kontrarevoluční
sabotáži	sabotáž	k1gFnSc6	sabotáž
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
umožňoval	umožňovat	k5eAaImAgMnS	umožňovat
takovou	takový	k3xDgFnSc4	takový
sabotáž	sabotáž	k1gFnSc4	sabotáž
trestat	trestat	k5eAaImF	trestat
zastřelením	zastřelení	k1gNnSc7	zastřelení
a	a	k8xC	a
konfiskací	konfiskace	k1gFnSc7	konfiskace
majetku	majetek	k1gInSc2	majetek
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tajný	tajný	k2eAgInSc4d1	tajný
proces	proces	k1gInSc4	proces
proti	proti	k7c3	proti
vysokým	vysoký	k2eAgMnPc3d1	vysoký
důstojníkům	důstojník	k1gMnPc3	důstojník
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osm	osm	k4xCc1	osm
obžalovaných	obžalovaný	k1gMnPc2	obžalovaný
včetně	včetně	k7c2	včetně
maršála	maršál	k1gMnSc2	maršál
Tuchačevského	Tuchačevský	k2eAgMnSc2d1	Tuchačevský
bylo	být	k5eAaImAgNnS	být
popraveno	popraven	k2eAgNnSc1d1	popraveno
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
unikl	uniknout	k5eAaPmAgMnS	uniknout
sebevraždou	sebevražda	k1gFnSc7	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
procesu	proces	k1gInSc6	proces
následovala	následovat	k5eAaImAgFnS	následovat
vlna	vlna	k1gFnSc1	vlna
masivních	masivní	k2eAgFnPc2d1	masivní
čistek	čistka	k1gFnPc2	čistka
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
důstojnickém	důstojnický	k2eAgInSc6d1	důstojnický
sboru	sbor	k1gInSc6	sbor
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zavedení	zavedení	k1gNnSc1	zavedení
"	"	kIx"	"
<g/>
trojek	trojka	k1gFnPc2	trojka
<g/>
"	"	kIx"	"
NKVD	NKVD	kA	NKVD
pro	pro	k7c4	pro
urychlení	urychlení	k1gNnSc4	urychlení
"	"	kIx"	"
<g/>
revoluční	revoluční	k2eAgFnSc2d1	revoluční
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ježovův	Ježovův	k2eAgInSc4d1	Ježovův
rozkaz	rozkaz	k1gInSc4	rozkaz
00447	[number]	k4	00447
platný	platný	k2eAgInSc4d1	platný
k	k	k7c3	k
30	[number]	k4	30
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc3	červenec
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgInSc1	třetí
moskevský	moskevský	k2eAgInSc1d1	moskevský
proces	proces	k1gInSc1	proces
nad	nad	k7c7	nad
21	[number]	k4	21
zčásti	zčásti	k6eAd1	zčásti
vysoce	vysoce	k6eAd1	vysoce
postavenými	postavený	k2eAgFnPc7d1	postavená
stranickými	stranický	k2eAgFnPc7d1	stranická
vůdci	vůdce	k1gMnPc1	vůdce
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
bývalým	bývalý	k2eAgMnSc7d1	bývalý
předsedou	předseda	k1gMnSc7	předseda
Kominterny	Kominterna	k1gFnSc2	Kominterna
Nikolajem	Nikolaj	k1gMnSc7	Nikolaj
Bucharinem	Bucharin	k1gInSc7	Bucharin
<g/>
,	,	kIx,	,
bývalým	bývalý	k2eAgMnSc7d1	bývalý
premiérem	premiér	k1gMnSc7	premiér
Alexejem	Alexej	k1gMnSc7	Alexej
Rykovem	Rykovo	k1gNnSc7	Rykovo
a	a	k8xC	a
nedávnou	dávný	k2eNgFnSc7d1	nedávná
hlavou	hlava	k1gFnSc7	hlava
tajné	tajný	k2eAgFnSc2d1	tajná
policie	policie	k1gFnSc2	policie
NKVD	NKVD	kA	NKVD
Genrichem	Genrich	k1gMnSc7	Genrich
Jagodou	Jagoda	k1gMnSc7	Jagoda
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
ústící	ústící	k2eAgMnSc1d1	ústící
v	v	k7c4	v
popravu	poprava	k1gFnSc4	poprava
všech	všecek	k3xTgMnPc2	všecek
hlavních	hlavní	k2eAgMnPc2d1	hlavní
obžalovaných	obžalovaný	k1gMnPc2	obžalovaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dekret	dekret	k1gInSc1	dekret
Sovnarkomu	sovnarkom	k1gInSc2	sovnarkom
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgFnSc2d2	pozdější
Rady	rada	k1gFnSc2	rada
ministrů	ministr	k1gMnPc2	ministr
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
strany	strana	k1gFnSc2	strana
rušící	rušící	k2eAgNnPc4d1	rušící
mimořádná	mimořádný	k2eAgNnPc4d1	mimořádné
bezpečnostní	bezpečnostní	k2eAgNnPc4d1	bezpečnostní
opatření	opatření	k1gNnPc4	opatření
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
ukončil	ukončit	k5eAaPmAgInS	ukončit
období	období	k1gNnSc4	období
čistky	čistka	k1gFnSc2	čistka
<g/>
.	.	kIx.	.
</s>
<s>
Neznamenal	znamenat	k5eNaImAgMnS	znamenat
skončení	skončení	k1gNnSc4	skončení
stalinských	stalinský	k2eAgFnPc2d1	stalinská
represí	represe	k1gFnPc2	represe
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
útlak	útlak	k1gInSc4	útlak
již	již	k6eAd1	již
poté	poté	k6eAd1	poté
obvykle	obvykle	k6eAd1	obvykle
nebyl	být	k5eNaImAgInS	být
tak	tak	k6eAd1	tak
intenzivní	intenzivní	k2eAgInSc1d1	intenzivní
a	a	k8xC	a
krvavý	krvavý	k2eAgInSc1d1	krvavý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Rehabilitation	Rehabilitation	k1gInSc1	Rehabilitation
<g/>
:	:	kIx,	:
As	as	k9	as
It	It	k1gMnSc1	It
Happened	Happened	k1gMnSc1	Happened
<g/>
.	.	kIx.	.
</s>
<s>
Documents	Documents	k1gInSc1	Documents
of	of	k?	of
the	the	k?	the
CPSU	CPSU	kA	CPSU
CC	CC	kA	CC
Presidium	presidium	k1gNnSc4	presidium
and	and	k?	and
Other	Other	k1gInSc1	Other
Materials	Materials	k1gInSc1	Materials
<g/>
.	.	kIx.	.
</s>
<s>
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
February	Februar	k1gInPc1	Februar
1956	[number]	k4	1956
<g/>
-Early	-Earla	k1gFnSc2	-Earla
1980	[number]	k4	1980
<g/>
s.	s.	k?	s.
Moscow	Moscow	k1gMnSc1	Moscow
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Compiled	Compiled	k1gInSc1	Compiled
by	by	kYmCp3nS	by
A.	A.	kA	A.
Artizov	Artizov	k1gInSc4	Artizov
<g/>
,	,	kIx,	,
Yu	Yu	k1gFnSc4	Yu
<g/>
.	.	kIx.	.
</s>
<s>
Sigachev	Sigachev	k1gFnSc1	Sigachev
<g/>
,	,	kIx,	,
I.	I.	kA	I.
Shevchuk	Shevchuk	k1gInSc1	Shevchuk
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Khlopov	Khlopov	k1gInSc1	Khlopov
under	under	k1gInSc1	under
editorship	editorship	k1gInSc1	editorship
of	of	k?	of
acad	acad	k1gInSc1	acad
<g/>
.	.	kIx.	.
</s>
<s>
A.	A.	kA	A.
N.	N.	kA	N.
Yakovlev	Yakovlev	k1gMnSc1	Yakovlev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eternal	Eternat	k5eAaImAgMnS	Eternat
Memory	Memora	k1gFnPc4	Memora
<g/>
:	:	kIx,	:
Voices	Voices	k1gMnSc1	Voices
From	From	k1gMnSc1	From
the	the	k?	the
Great	Great	k2eAgMnSc1d1	Great
Terror	Terror	k1gMnSc1	Terror
<g/>
.	.	kIx.	.
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
16	[number]	k4	16
mm	mm	kA	mm
feature	featur	k1gMnSc5	featur
film	film	k1gInSc1	film
directed	directed	k1gInSc4	directed
by	by	kYmCp3nS	by
Pultz	Pultz	k1gMnSc1	Pultz
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Narrated	Narrated	k1gInSc1	Narrated
by	by	kYmCp3nS	by
Meryl	Meryl	k1gInSc1	Meryl
Streep	Streep	k1gInSc4	Streep
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Conquest	Conquest	k1gMnSc1	Conquest
<g/>
:	:	kIx,	:
The	The	k1gFnSc6	The
Great	Great	k2eAgMnSc1d1	Great
Terror	Terror	k1gMnSc1	Terror
<g/>
:	:	kIx,	:
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Purge	Purge	k1gInSc1	Purge
of	of	k?	of
the	the	k?	the
Thirties	Thirties	k1gInSc1	Thirties
<g/>
.	.	kIx.	.
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Conquest	Conquest	k1gMnSc1	Conquest
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Great	Great	k2eAgInSc4d1	Great
Terror	Terror	k1gInSc4	Terror
<g/>
:	:	kIx,	:
A	a	k9	a
Reassessment	Reassessment	k1gInSc1	Reassessment
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
May	May	k1gMnSc1	May
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
hardcover	hardcover	k1gInSc1	hardcover
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
505580	[number]	k4	505580
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
trade	trade	k6eAd1	trade
paperback	paperback	k1gInSc1	paperback
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
September	September	k1gInSc1	September
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-19-507132-8	[number]	k4	0-19-507132-8
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Gellately	Gellatela	k1gFnSc2	Gellatela
<g/>
,	,	kIx,	,
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
,	,	kIx,	,
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
and	and	k?	and
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Age	Age	k1gMnSc1	Age
of	of	k?	of
Social	Social	k1gMnSc1	Social
Catastrophe	Catastroph	k1gFnSc2	Catastroph
<g/>
.	.	kIx.	.
</s>
<s>
Knopf	Knopf	k1gMnSc1	Knopf
<g/>
,	,	kIx,	,
August	August	k1gMnSc1	August
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
720	[number]	k4	720
pages	pagesa	k1gFnPc2	pagesa
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
1400040051	[number]	k4	1400040051
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Arch	arch	k1gInSc1	arch
Getty	Getta	k1gMnSc2	Getta
and	and	k?	and
Oleg	Oleg	k1gMnSc1	Oleg
V.	V.	kA	V.
Naumov	Naumov	k1gInSc1	Naumov
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Road	Road	k1gMnSc1	Road
to	ten	k3xDgNnSc4	ten
Terror	Terror	k1gMnSc1	Terror
<g/>
:	:	kIx,	:
Stalin	Stalin	k1gMnSc1	Stalin
and	and	k?	and
the	the	k?	the
Self-Destruction	Self-Destruction	k1gInSc1	Self-Destruction
of	of	k?	of
the	the	k?	the
Bolsheviks	Bolsheviks	k1gInSc1	Bolsheviks
<g/>
,	,	kIx,	,
Yale	Yale	k1gInSc1	Yale
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Arch	arch	k1gInSc1	arch
Getty	Getta	k1gMnSc2	Getta
and	and	k?	and
Roberta	Robert	k1gMnSc2	Robert
T.	T.	kA	T.
Manning	Manning	k1gInSc1	Manning
<g/>
,	,	kIx,	,
Stalinist	Stalinist	k1gMnSc1	Stalinist
Terror	Terror	k1gMnSc1	Terror
<g/>
:	:	kIx,	:
New	New	k1gMnSc1	New
Perspectives	Perspectives	k1gMnSc1	Perspectives
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nicolas	Nicolas	k1gMnSc1	Nicolas
Werth	Werth	k1gMnSc1	Werth
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Bartosek	Bartoska	k1gFnPc2	Bartoska
<g/>
,	,	kIx,	,
Jean-Louis	Jean-Louis	k1gFnPc2	Jean-Louis
Panne	Pann	k1gInSc5	Pann
<g/>
,	,	kIx,	,
Jean-Louis	Jean-Louis	k1gFnPc3	Jean-Louis
Margolin	Margolina	k1gFnPc2	Margolina
<g/>
,	,	kIx,	,
Andrzej	Andrzej	k1gMnSc5	Andrzej
Paczkowski	Paczkowsk	k1gMnSc5	Paczkowsk
<g/>
,	,	kIx,	,
Stephane	Stephan	k1gMnSc5	Stephan
Courtois	Courtois	k1gFnSc5	Courtois
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Black	Black	k1gMnSc1	Black
Book	Book	k1gMnSc1	Book
of	of	k?	of
Communism	Communism	k1gMnSc1	Communism
<g/>
:	:	kIx,	:
Crimes	Crimes	k1gMnSc1	Crimes
<g/>
,	,	kIx,	,
Terror	Terror	k1gMnSc1	Terror
<g/>
,	,	kIx,	,
Repression	Repression	k1gInSc1	Repression
<g/>
,	,	kIx,	,
Harvard	Harvard	k1gInSc1	Harvard
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
hardcover	hardcover	k1gInSc1	hardcover
<g/>
,	,	kIx,	,
858	[number]	k4	858
pages	pagesa	k1gFnPc2	pagesa
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
674	[number]	k4	674
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7608	[number]	k4	7608
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Chapter	Chapter	k1gInSc1	Chapter
10	[number]	k4	10
<g/>
:	:	kIx,	:
The	The	k1gFnSc6	The
Great	Great	k2eAgMnSc1d1	Great
Terror	Terror	k1gMnSc1	Terror
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Earl	earl	k1gMnSc1	earl
Haynes	Haynes	k1gMnSc1	Haynes
and	and	k?	and
Harvey	Harvea	k1gFnSc2	Harvea
Klehr	Klehr	k1gMnSc1	Klehr
<g/>
,	,	kIx,	,
In	In	k1gMnSc1	In
Denial	Denial	k1gMnSc1	Denial
<g/>
:	:	kIx,	:
Historians	Historians	k1gInSc1	Historians
<g/>
,	,	kIx,	,
Communism	Communism	k1gInSc1	Communism
<g/>
,	,	kIx,	,
and	and	k?	and
Espionage	Espionage	k1gInSc1	Espionage
<g/>
,	,	kIx,	,
Encounter	Encounter	k1gInSc1	Encounter
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
September	Septembra	k1gFnPc2	Septembra
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
hardcover	hardcover	k1gInSc1	hardcover
<g/>
,	,	kIx,	,
312	[number]	k4	312
pages	pagesa	k1gFnPc2	pagesa
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
1-893554-72-4	[number]	k4	1-893554-72-4
</s>
</p>
<p>
<s>
Barry	Barr	k1gInPc1	Barr
McLoughlin	McLoughlina	k1gFnPc2	McLoughlina
and	and	k?	and
Kevin	Kevin	k1gMnSc1	Kevin
McDermott	McDermott	k1gMnSc1	McDermott
<g/>
,	,	kIx,	,
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Terror	Terrora	k1gFnPc2	Terrora
<g/>
:	:	kIx,	:
High	High	k1gInSc1	High
Politics	Politics	k1gInSc1	Politics
and	and	k?	and
Mass	Mass	k1gInSc1	Mass
Repression	Repression	k1gInSc1	Repression
in	in	k?	in
the	the	k?	the
Soviet	Soviet	k1gInSc1	Soviet
Union	union	k1gInSc1	union
<g/>
,	,	kIx,	,
Palgrave	Palgrav	k1gInSc5	Palgrav
Macmillan	Macmillan	k1gInSc1	Macmillan
<g/>
,	,	kIx,	,
December	December	k1gInSc1	December
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
hardcover	hardcover	k1gInSc1	hardcover
<g/>
,	,	kIx,	,
280	[number]	k4	280
pages	pagesa	k1gFnPc2	pagesa
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
1-4039-0119-8	[number]	k4	1-4039-0119-8
</s>
</p>
<p>
<s>
Arthur	Arthur	k1gMnSc1	Arthur
Koestler	Koestler	k1gMnSc1	Koestler
<g/>
,	,	kIx,	,
Darkness	Darkness	k1gInSc1	Darkness
at	at	k?	at
Noon	Noon	k1gInSc1	Noon
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-553-26595-4	[number]	k4	0-553-26595-4
</s>
</p>
<p>
<s>
Rehabilitation	Rehabilitation	k1gInSc1	Rehabilitation
<g/>
:	:	kIx,	:
Political	Political	k1gFnSc1	Political
Processes	Processes	k1gInSc1	Processes
of	of	k?	of
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
<g/>
th	th	k?	th
years	years	k1gInSc1	years
<g/>
,	,	kIx,	,
in	in	k?	in
Russian	Russian	k1gInSc1	Russian
(	(	kIx(	(
<g/>
Р	Р	k?	Р
<g/>
.	.	kIx.	.
П	П	k?	П
п	п	k?	п
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
<g/>
-х	-х	k?	-х
г	г	k?	г
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
editor	editor	k1gInSc1	editor
<g/>
:	:	kIx,	:
Academician	Academician	k1gInSc1	Academician
A.	A.	kA	A.
<g/>
N.	N.	kA	N.
<g/>
Yakovlev	Yakovlev	k1gMnSc1	Yakovlev
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
ISBN	ISBN	kA	ISBN
5-250-01429-1	[number]	k4	5-250-01429-1
</s>
</p>
<p>
<s>
Aleksandr	Aleksandr	k1gInSc1	Aleksandr
I.	I.	kA	I.
Solzhenitsyn	Solzhenitsyn	k1gInSc1	Solzhenitsyn
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Gulag	gulag	k1gInSc1	gulag
Archipelago	Archipelago	k6eAd1	Archipelago
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
HarperCollins	HarperCollins	k1gInSc1	HarperCollins
<g/>
,	,	kIx,	,
February	Februar	k1gInPc1	Februar
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
paperback	paperback	k1gInSc1	paperback
<g/>
,	,	kIx,	,
512	[number]	k4	512
pages	pagesa	k1gFnPc2	pagesa
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-06-000776-1	[number]	k4	0-06-000776-1
</s>
</p>
<p>
<s>
Eugene	Eugen	k1gMnSc5	Eugen
Lyons	Lyons	k1gInSc1	Lyons
<g/>
,	,	kIx,	,
Assignment	Assignment	k1gInSc1	Assignment
in	in	k?	in
Utopia	Utopia	k1gFnSc1	Utopia
<g/>
,	,	kIx,	,
Harcourt	Harcourt	k1gInSc1	Harcourt
Brace	braka	k1gFnSc6	braka
and	and	k?	and
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vadim	Vadim	k?	Vadim
Rogovin	Rogovina	k1gFnPc2	Rogovina
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Two	Two	k1gMnSc1	Two
lectures	lectures	k1gMnSc1	lectures
<g/>
:	:	kIx,	:
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Great	Great	k1gInSc1	Great
Terror	Terror	k1gInSc1	Terror
<g/>
:	:	kIx,	:
Origins	Origins	k1gInSc1	Origins
and	and	k?	and
Consequences	Consequences	k1gInSc1	Consequences
Leon	Leona	k1gFnPc2	Leona
Trotsky	Trotsky	k1gFnSc2	Trotsky
and	and	k?	and
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Fate	Fat	k1gFnSc2	Fat
of	of	k?	of
Marxism	Marxism	k1gMnSc1	Marxism
in	in	k?	in
the	the	k?	the
USSR	USSR	kA	USSR
<g/>
"	"	kIx"	"
Mehring	Mehring	k1gInSc1	Mehring
books	books	k6eAd1	books
<g/>
,1996	,1996	k4	,1996
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
929087	[number]	k4	929087
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vadim	Vadim	k?	Vadim
Rogovin	Rogovina	k1gFnPc2	Rogovina
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
1937	[number]	k4	1937
<g/>
:	:	kIx,	:
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Year	Year	k1gInSc1	Year
of	of	k?	of
Terror	Terror	k1gInSc1	Terror
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mehring	Mehring	k1gInSc1	Mehring
books	booksa	k1gFnPc2	booksa
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
929087	[number]	k4	929087
<g/>
-	-	kIx~	-
<g/>
77	[number]	k4	77
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Velká	velký	k2eAgFnSc1d1	velká
čistka	čistka	k1gFnSc1	čistka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Documenting	Documenting	k1gInSc1	Documenting
the	the	k?	the
Death	Death	k1gMnSc1	Death
Toll	Toll	k1gMnSc1	Toll
<g/>
:	:	kIx,	:
Research	Research	k1gMnSc1	Research
into	into	k1gMnSc1	into
the	the	k?	the
Mass	Mass	k1gInSc1	Mass
Murder	Murder	k1gInSc1	Murder
of	of	k?	of
Foreigners	Foreigners	k1gInSc1	Foreigners
in	in	k?	in
Moscow	Moscow	k1gFnSc2	Moscow
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
<g/>
–	–	k?	–
<g/>
38	[number]	k4	38
<g/>
"	"	kIx"	"
By	by	k9	by
Barry	Barr	k1gInPc4	Barr
McLoughlin	McLoughlina	k1gFnPc2	McLoughlina
<g/>
,	,	kIx,	,
American	Americana	k1gFnPc2	Americana
Historical	Historical	k1gFnPc2	Historical
Association	Association	k1gInSc1	Association
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
American	American	k1gInSc1	American
Communists	Communists	k1gInSc1	Communists
and	and	k?	and
Radicals	Radicals	k1gInSc1	Radicals
Executed	Executed	k1gInSc1	Executed
by	by	kYmCp3nS	by
Soviet	Soviet	k1gInSc4	Soviet
Political	Political	k1gFnSc2	Political
Police	police	k1gFnSc2	police
and	and	k?	and
Buried	Buried	k1gMnSc1	Buried
at	at	k?	at
Sandarmokh	Sandarmokh	k1gMnSc1	Sandarmokh
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Appendix	Appendix	k1gInSc1	Appendix
to	ten	k3xDgNnSc4	ten
John	John	k1gMnSc1	John
Earl	earl	k1gMnSc1	earl
Haynes	Haynes	k1gMnSc1	Haynes
and	and	k?	and
Harvey	Harvea	k1gFnSc2	Harvea
Klehr	Klehra	k1gFnPc2	Klehra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
In	In	k1gFnSc7	In
Denial	Denial	k1gMnSc1	Denial
<g/>
:	:	kIx,	:
Historians	Historians	k1gInSc1	Historians
<g/>
,	,	kIx,	,
Communism	Communism	k1gInSc1	Communism
and	and	k?	and
Espionage	Espionage	k1gInSc1	Espionage
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Former	Former	k1gInSc1	Former
Killing	Killing	k1gInSc1	Killing
Ground	Grounda	k1gFnPc2	Grounda
Becomes	Becomesa	k1gFnPc2	Becomesa
Shrine	Shrin	k1gInSc5	Shrin
to	ten	k3xDgNnSc4	ten
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Victims	Victims	k1gInSc1	Victims
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
<g/>
,	,	kIx,	,
June	jun	k1gMnSc5	jun
8	[number]	k4	8
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
http://is.muni.cz/th/333029/fss_b/velky_teror.pdf	[url]	k1gMnSc1	http://is.muni.cz/th/333029/fss_b/velky_teror.pdf
</s>
</p>
