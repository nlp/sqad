<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Dolinek	dolinka	k1gFnPc2	dolinka
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1983	[number]	k4	1983
Vyškov	Vyškov	k1gInSc1	Vyškov
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
první	první	k4xOgFnPc4	první
řady	řada	k1gFnPc4	řada
soutěže	soutěž	k1gFnSc2	soutěž
Česko	Česko	k1gNnSc1	Česko
hledá	hledat	k5eAaImIp3nS	hledat
Superstar	superstar	k1gFnSc4	superstar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obsadil	obsadit	k5eAaPmAgMnS	obsadit
sedmé	sedmý	k4xOgNnSc4	sedmý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Zpívat	zpívat	k5eAaImF	zpívat
začal	začít	k5eAaPmAgInS	začít
již	již	k6eAd1	již
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Janovicích	Janovice	k1gFnPc6	Janovice
nad	nad	k7c7	nad
Úhlavou	Úhlava	k1gFnSc7	Úhlava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k9	jako
dítě	dítě	k1gNnSc1	dítě
důstojníka	důstojník	k1gMnSc2	důstojník
armády	armáda	k1gFnSc2	armáda
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
zpíval	zpívat	k5eAaImAgMnS	zpívat
ve	v	k7c6	v
sboru	sbor	k1gInSc6	sbor
Víceletého	víceletý	k2eAgNnSc2d1	víceleté
gymnázia	gymnázium	k1gNnSc2	gymnázium
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Vrchlického	Vrchlický	k1gMnSc2	Vrchlický
v	v	k7c6	v
Klatovech	Klatovy	k1gInPc6	Klatovy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtrnácti	čtrnáct	k4xCc6	čtrnáct
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
soutěže	soutěž	k1gFnSc2	soutěž
Karlovarský	karlovarský	k2eAgMnSc1d1	karlovarský
skřivánek	skřivánek	k1gMnSc1	skřivánek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
celorepublikového	celorepublikový	k2eAgNnSc2d1	celorepublikové
kola	kolo	k1gNnSc2	kolo
a	a	k8xC	a
obsadil	obsadit	k5eAaPmAgMnS	obsadit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
předních	přední	k2eAgFnPc2d1	přední
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
studiích	studie	k1gFnPc6	studie
na	na	k7c6	na
STŠ	STŠ	kA	STŠ
MO	MO	kA	MO
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
Třebové	Třebová	k1gFnSc6	Třebová
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaPmF	věnovat
i	i	k9	i
ochotnickému	ochotnický	k2eAgNnSc3d1	ochotnické
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mu	on	k3xPp3gMnSc3	on
vydrželo	vydržet	k5eAaPmAgNnS	vydržet
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
studií	studie	k1gFnPc2	studie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
veřejnosti	veřejnost	k1gFnSc2	veřejnost
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
1	[number]	k4	1
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc6	ročník
soutěže	soutěž	k1gFnSc2	soutěž
Česko	Česko	k1gNnSc1	Česko
hledá	hledat	k5eAaImIp3nS	hledat
Superstar	superstar	k1gFnSc4	superstar
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Probojoval	probojovat	k5eAaPmAgInS	probojovat
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
finálové	finálový	k2eAgFnSc2d1	finálová
desítky	desítka	k1gFnSc2	desítka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
jako	jako	k9	jako
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
a	a	k8xC	a
obsadil	obsadit	k5eAaPmAgMnS	obsadit
tak	tak	k9	tak
celkové	celkový	k2eAgNnSc4d1	celkové
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
vydal	vydat	k5eAaPmAgMnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
Standa	Standa	k1gMnSc1	Standa
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yRgNnSc4	který
dostal	dostat	k5eAaPmAgInS	dostat
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
desku	deska	k1gFnSc4	deska
za	za	k7c4	za
10	[number]	k4	10
000	[number]	k4	000
prodaných	prodaný	k2eAgInPc2d1	prodaný
nosičů	nosič	k1gInPc2	nosič
<g/>
.	.	kIx.	.
</s>
<s>
Hudbou	hudba	k1gFnSc7	hudba
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
několik	několik	k4yIc4	několik
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc2	který
připravoval	připravovat	k5eAaImAgMnS	připravovat
i	i	k9	i
své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgFnSc2	druhý
CD	CD	kA	CD
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nespatřilo	spatřit	k5eNaPmAgNnS	spatřit
světlo	světlo	k1gNnSc1	světlo
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
známy	znám	k2eAgFnPc1d1	známa
pouze	pouze	k6eAd1	pouze
písně	píseň	k1gFnPc4	píseň
Powerless	Powerlessa	k1gFnPc2	Powerlessa
<g/>
,	,	kIx,	,
Out	Out	k1gFnPc2	Out
a	a	k8xC	a
Latino	latina	k1gFnSc5	latina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
zpěvu	zpěv	k1gInSc2	zpěv
profesionálně	profesionálně	k6eAd1	profesionálně
nevěnuje	věnovat	k5eNaImIp3nS	věnovat
a	a	k8xC	a
po	po	k7c6	po
práci	práce	k1gFnSc6	práce
redaktora	redaktor	k1gMnSc2	redaktor
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
Spy	Spy	k1gFnSc2	Spy
<g/>
,	,	kIx,	,
Story	story	k1gFnSc2	story
a	a	k8xC	a
provozního	provozní	k2eAgInSc2d1	provozní
v	v	k7c6	v
restauraci	restaurace	k1gFnSc6	restaurace
a	a	k8xC	a
hotelu	hotel	k1gInSc6	hotel
<g/>
,	,	kIx,	,
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
bankovnictví	bankovnictví	k1gNnSc2	bankovnictví
a	a	k8xC	a
pojišťovnictví	pojišťovnictví	k1gNnSc2	pojišťovnictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc4	chvíle
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česko	Česko	k1gNnSc1	Česko
hledá	hledat	k5eAaImIp3nS	hledat
Superstar	superstar	k1gFnSc4	superstar
==	==	k?	==
</s>
</p>
<p>
<s>
Dolinek	dolinka	k1gFnPc2	dolinka
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
první	první	k4xOgFnPc4	první
řady	řada	k1gFnPc4	řada
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
deset	deset	k4xCc4	deset
díky	díky	k7c3	díky
"	"	kIx"	"
<g/>
Divoké	divoký	k2eAgFnSc3d1	divoká
kartě	karta	k1gFnSc3	karta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mu	on	k3xPp3gMnSc3	on
udělila	udělit	k5eAaPmAgFnS	udělit
porota	porota	k1gFnSc1	porota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOP	topit	k5eAaImRp2nS	topit
40	[number]	k4	40
<g/>
:	:	kIx,	:
Killing	Killing	k1gInSc1	Killing
Me	Me	k1gMnSc1	Me
Softly	Softly	k1gMnSc1	Softly
(	(	kIx(	(
<g/>
Fugees	Fugees	k1gMnSc1	Fugees
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TOP	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
<g/>
:	:	kIx,	:
Strong	Strong	k1gMnSc1	Strong
Enough	Enough	k1gMnSc1	Enough
(	(	kIx(	(
<g/>
Cher	Cher	k1gMnSc1	Cher
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TOP	topit	k5eAaImRp2nS	topit
9	[number]	k4	9
<g/>
:	:	kIx,	:
Tam	tam	k6eAd1	tam
u	u	k7c2	u
nebeských	nebeský	k2eAgFnPc2d1	nebeská
bran	brána	k1gFnPc2	brána
(	(	kIx(	(
<g/>
Michal	Michal	k1gMnSc1	Michal
Tučný	tučný	k2eAgMnSc1d1	tučný
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TOP	topit	k5eAaImRp2nS	topit
8	[number]	k4	8
<g/>
:	:	kIx,	:
Proklínám	proklínat	k5eAaImIp1nS	proklínat
(	(	kIx(	(
<g/>
Petr	Petr	k1gMnSc1	Petr
Muk	muka	k1gFnPc2	muka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TOP	topit	k5eAaImRp2nS	topit
7	[number]	k4	7
<g/>
:	:	kIx,	:
Trezor	trezor	k1gInSc1	trezor
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Standa	Standa	k1gFnSc1	Standa
001	[number]	k4	001
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Stanislav	Stanislav	k1gMnSc1	Stanislav
Dolinek	dolinka	k1gFnPc2	dolinka
</s>
</p>
