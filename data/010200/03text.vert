<p>
<s>
Restrictions	Restrictions	k6eAd1	Restrictions
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgNnSc1	třetí
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
americké	americký	k2eAgFnSc2d1	americká
hardrockové	hardrockový	k2eAgFnSc2d1	hardrocková
skupiny	skupina	k1gFnSc2	skupina
Cactus	Cactus	k1gMnSc1	Cactus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
nahrávání	nahrávání	k1gNnSc1	nahrávání
probíhalo	probíhat	k5eAaImAgNnS	probíhat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
Electric	Electrice	k1gFnPc2	Electrice
Lady	lady	k1gFnSc1	lady
Studios	Studios	k?	Studios
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1971	[number]	k4	1971
u	u	k7c2	u
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Atco	Atco	k6eAd1	Atco
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
sedmi	sedm	k4xCc2	sedm
původních	původní	k2eAgFnPc2d1	původní
skladeb	skladba	k1gFnPc2	skladba
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
jednu	jeden	k4xCgFnSc4	jeden
coververzi	coververze	k1gFnSc4	coververze
<g/>
;	;	kIx,	;
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Evil	Evil	k1gInSc4	Evil
<g/>
"	"	kIx"	"
od	od	k7c2	od
Howlin	Howlina	k1gFnPc2	Howlina
<g/>
'	'	kIx"	'
Wolfa	Wolf	k1gMnSc2	Wolf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
CactusRusty	CactusRust	k1gInPc1	CactusRust
Day	Day	k1gFnSc2	Day
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
harmonika	harmonika	k1gFnSc1	harmonika
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnPc1	perkuse
</s>
</p>
<p>
<s>
Jim	on	k3xPp3gMnPc3	on
McCarty	McCart	k1gMnPc4	McCart
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Tim	Tim	k?	Tim
Bogert	Bogert	k1gInSc1	Bogert
−	−	k?	−
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Carmine	Carmin	k1gInSc5	Carmin
Appice	Appic	k1gMnPc4	Appic
−	−	k?	−
bicí	bicí	k2eAgInSc1d1	bicí
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnSc1	perkuse
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěvOstatníRon	zpěvOstatníRon	k1gInSc1	zpěvOstatníRon
Leejack	Leejack	k1gInSc1	Leejack
–	–	k?	–
slide	slide	k1gInSc1	slide
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Albhy	Albh	k1gInPc1	Albh
Galuten	Galuten	k2eAgInSc1d1	Galuten
–	–	k?	–
klavír	klavír	k1gInSc1	klavír
</s>
</p>
<p>
<s>
Duane	Duanout	k5eAaImIp3nS	Duanout
Hitchings	Hitchings	k1gInSc4	Hitchings
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
