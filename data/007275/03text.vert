<s>
Závrať	závrať	k1gFnSc1	závrať
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
také	také	k9	také
motolice	motolice	k1gFnSc1	motolice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pocit	pocit	k1gInSc4	pocit
nevolnosti	nevolnost	k1gFnSc2	nevolnost
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
postiženému	postižený	k1gMnSc3	postižený
točí	točit	k5eAaImIp3nS	točit
hlava	hlava	k1gFnSc1	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pocity	pocit	k1gInPc4	pocit
lehké	lehký	k2eAgFnSc2d1	lehká
nejistoty	nejistota	k1gFnSc2	nejistota
<g/>
,	,	kIx,	,
vrávorání	vrávorání	k1gNnSc2	vrávorání
<g/>
,	,	kIx,	,
pocit	pocit	k1gInSc4	pocit
nevolnosti	nevolnost	k1gFnSc2	nevolnost
nebo	nebo	k8xC	nebo
opilosti	opilost	k1gFnSc2	opilost
až	až	k9	až
po	po	k7c4	po
pocity	pocit	k1gInPc4	pocit
točení	točení	k1gNnSc2	točení
vlastního	vlastní	k2eAgNnSc2d1	vlastní
těla	tělo	k1gNnSc2	tělo
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
okolí	okolí	k1gNnSc3	okolí
nebo	nebo	k8xC	nebo
okolních	okolní	k2eAgInPc2d1	okolní
předmětů	předmět	k1gInPc2	předmět
okolo	okolo	k7c2	okolo
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
</s>
<s>
Pravou	pravý	k2eAgFnSc7d1	pravá
závratí	závrať	k1gFnSc7	závrať
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
vertigem	vertig	k1gMnSc7	vertig
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
iluze	iluze	k1gFnSc1	iluze
pohybu	pohyb	k1gInSc2	pohyb
vlastního	vlastní	k2eAgNnSc2d1	vlastní
těla	tělo	k1gNnSc2	tělo
nebo	nebo	k8xC	nebo
předmětů	předmět	k1gInPc2	předmět
kolem	kolem	k6eAd1	kolem
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
podobnému	podobný	k2eAgInSc3d1	podobný
jevu	jev	k1gInSc3	jev
dochází	docházet	k5eAaImIp3nS	docházet
například	například	k6eAd1	například
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
točíme	točit	k5eAaImIp1nP	točit
na	na	k7c6	na
kolotoči	kolotoč	k1gInSc6	kolotoč
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
však	však	k9	však
závrať	závrať	k1gFnSc1	závrať
vzniká	vznikat	k5eAaImIp3nS	vznikat
bez	bez	k7c2	bez
viditelného	viditelný	k2eAgInSc2d1	viditelný
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc4	ten
být	být	k5eAaImF	být
příznakem	příznak	k1gInSc7	příznak
nějakého	nějaký	k3yIgNnSc2	nějaký
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Opravdová	opravdový	k2eAgFnSc1d1	opravdová
závrať	závrať	k1gFnSc1	závrať
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
doprovázena	doprovázen	k2eAgFnSc1d1	doprovázena
psychickými	psychický	k2eAgFnPc7d1	psychická
a	a	k8xC	a
neurovegetativními	urovegetativní	k2eNgFnPc7d1	neurovegetativní
poruchami	porucha	k1gFnPc7	porucha
jako	jako	k8xS	jako
např.	např.	kA	např.
<g/>
:	:	kIx,	:
nevolností	nevolnost	k1gFnSc7	nevolnost
zvracením	zvracení	k1gNnSc7	zvracení
zblednutím	zblednutí	k1gNnPc3	zblednutí
pocením	pocení	k1gNnPc3	pocení
pocitem	pocit	k1gInSc7	pocit
nejistoty	nejistota	k1gFnSc2	nejistota
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Lze	lze	k6eAd1	lze
to	ten	k3xDgNnSc4	ten
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
těsným	těsný	k2eAgNnSc7d1	těsné
spojením	spojení	k1gNnSc7	spojení
mezi	mezi	k7c7	mezi
vestibulárním	vestibulární	k2eAgInSc7d1	vestibulární
aparátem	aparát	k1gInSc7	aparát
a	a	k8xC	a
vegetativní	vegetativní	k2eAgFnSc7d1	vegetativní
nervovou	nervový	k2eAgFnSc7d1	nervová
soustavou	soustava	k1gFnSc7	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Jakýkoli	jakýkoli	k3yIgInSc1	jakýkoli
zásah	zásah	k1gInSc1	zásah
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vestibulárního	vestibulární	k2eAgInSc2d1	vestibulární
aparátu	aparát	k1gInSc2	aparát
okamžitě	okamžitě	k6eAd1	okamžitě
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
současné	současný	k2eAgFnPc4d1	současná
vegetativní	vegetativní	k2eAgFnPc4d1	vegetativní
poruchy	porucha	k1gFnPc4	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
pocit	pocit	k1gInSc4	pocit
strachu	strach	k1gInSc2	strach
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
prakticky	prakticky	k6eAd1	prakticky
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
záchvat	záchvat	k1gInSc4	záchvat
závratě	závrať	k1gFnSc2	závrať
<g/>
,	,	kIx,	,
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
závrať	závrať	k1gFnSc4	závrať
nepředstavuje	představovat	k5eNaImIp3nS	představovat
stav	stav	k1gInSc1	stav
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
pro	pro	k7c4	pro
život	život	k1gInSc4	život
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
důležité	důležitý	k2eAgNnSc1d1	důležité
včas	včas	k6eAd1	včas
a	a	k8xC	a
správně	správně	k6eAd1	správně
určit	určit	k5eAaPmF	určit
diagnózu	diagnóza	k1gFnSc4	diagnóza
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
závrať	závrať	k1gFnSc4	závrať
<g/>
.	.	kIx.	.
</s>
<s>
Kejáková	Kejákový	k2eAgFnSc1d1	Kejákový
motolice	motolice	k1gFnSc1	motolice
</s>
