<s>
Letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Hry	hra	k1gFnPc1	hra
XXX	XXX	kA	XXX
<g/>
.	.	kIx.	.
olympiády	olympiáda	k1gFnSc2	olympiáda
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Games	Games	k1gInSc1	Games
of	of	k?	of
the	the	k?	the
XXX	XXX	kA	XXX
Olympiad	Olympiad	k1gInSc1	Olympiad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
zahájení	zahájení	k1gNnSc1	zahájení
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
ukončení	ukončení	k1gNnSc1	ukončení
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
<g/>
;	;	kIx,	;
samotná	samotný	k2eAgFnSc1d1	samotná
sportovní	sportovní	k2eAgNnSc4d1	sportovní
klání	klání	k1gNnSc4	klání
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
již	již	k6eAd1	již
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
rozběhla	rozběhnout	k5eAaPmAgFnS	rozběhnout
hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
her	hra	k1gFnPc2	hra
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
sportů	sport	k1gInPc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hrách	hra	k1gFnPc6	hra
bylo	být	k5eAaImAgNnS	být
rozdáno	rozdat	k5eAaPmNgNnS	rozdat
302	[number]	k4	302
sad	sada	k1gFnPc2	sada
medailí	medaile	k1gFnPc2	medaile
ve	v	k7c6	v
26	[number]	k4	26
sportech	sport	k1gInPc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
prvním	první	k4xOgNnSc7	první
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
v	v	k7c6	v
novodobé	novodobý	k2eAgFnSc6d1	novodobá
historii	historie	k1gFnSc6	historie
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
třikrát	třikrát	k6eAd1	třikrát
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1908	[number]	k4	1908
a	a	k8xC	a
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
již	již	k6eAd1	již
tradičně	tradičně	k6eAd1	tradičně
konaly	konat	k5eAaImAgFnP	konat
i	i	k9	i
paralympijské	paralympijský	k2eAgFnPc1d1	paralympijská
hry	hra	k1gFnPc1	hra
v	v	k7c6	v
termínu	termín	k1gInSc6	termín
od	od	k7c2	od
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
do	do	k7c2	do
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Kandidaturu	kandidatura	k1gFnSc4	kandidatura
na	na	k7c4	na
pořadatelství	pořadatelství	k1gNnSc4	pořadatelství
her	hra	k1gFnPc2	hra
nabídlo	nabídnout	k5eAaPmAgNnS	nabídnout
celkem	celkem	k6eAd1	celkem
devět	devět	k4xCc1	devět
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podala	podat	k5eAaPmAgFnS	podat
přihlášku	přihláška	k1gFnSc4	přihláška
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
města	město	k1gNnPc1	město
prošla	projít	k5eAaPmAgFnS	projít
výběrovým	výběrový	k2eAgNnSc7d1	výběrové
řízením	řízení	k1gNnSc7	řízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vyplněných	vyplněný	k2eAgInPc2d1	vyplněný
dotazníků	dotazník	k1gInPc2	dotazník
ohodnotilo	ohodnotit	k5eAaPmAgNnS	ohodnotit
v	v	k7c6	v
celkem	celkem	k6eAd1	celkem
jedenácti	jedenáct	k4xCc6	jedenáct
kritériích	kritérion	k1gNnPc6	kritérion
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
finance	finance	k1gFnPc1	finance
<g/>
,	,	kIx,	,
ubytování	ubytování	k1gNnSc1	ubytování
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
stanovilo	stanovit	k5eAaPmAgNnS	stanovit
skóre	skóre	k1gNnSc1	skóre
vyjadřující	vyjadřující	k2eAgNnSc1d1	vyjadřující
schopnost	schopnost	k1gFnSc4	schopnost
pořádat	pořádat	k5eAaImF	pořádat
olympiádu	olympiáda	k1gFnSc4	olympiáda
<g/>
:	:	kIx,	:
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
-	-	kIx~	-
skóre	skóre	k1gNnSc1	skóre
8,5	[number]	k4	8,5
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
-	-	kIx~	-
skóre	skóre	k1gNnSc4	skóre
8,3	[number]	k4	8,3
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
-	-	kIx~	-
skóre	skóre	k1gNnSc4	skóre
7,6	[number]	k4	7,6
New	New	k1gMnPc2	New
York	York	k1gInSc4	York
City	City	k1gFnPc2	City
<g/>
,	,	kIx,	,
USA	USA	kA	USA
-	-	kIx~	-
skóre	skóre	k1gNnSc4	skóre
7,5	[number]	k4	7,5
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
-	-	kIx~	-
skóre	skóre	k1gNnSc4	skóre
6,5	[number]	k4	6,5
Lipsko	Lipsko	k1gNnSc1	Lipsko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
-	-	kIx~	-
skóre	skóre	k1gNnSc4	skóre
6,0	[number]	k4	6,0
Rio	Rio	k1gFnSc2	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
-	-	kIx~	-
skóre	skóre	k1gNnSc1	skóre
5,1	[number]	k4	5,1
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
-	-	kIx~	-
skóre	skóre	k1gNnSc4	skóre
4,8	[number]	k4	4,8
Havana	havana	k1gNnPc2	havana
<g/>
,	,	kIx,	,
Kuba	kuba	k1gNnSc1	kuba
-	-	kIx~	-
skóre	skóre	k1gNnSc4	skóre
3,6	[number]	k4	3,6
Pět	pět	k4xCc1	pět
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
adeptů	adept	k1gMnPc2	adept
bylo	být	k5eAaImAgNnS	být
vybráno	vybrat	k5eAaPmNgNnS	vybrat
do	do	k7c2	do
finálové	finálový	k2eAgFnSc2d1	finálová
volby	volba	k1gFnSc2	volba
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
nakonec	nakonec	k6eAd1	nakonec
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
vítězně	vítězně	k6eAd1	vítězně
město	město	k1gNnSc1	město
Londýn	Londýn	k1gInSc1	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
zahájení	zahájení	k1gNnSc1	zahájení
her	hra	k1gFnPc2	hra
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
na	na	k7c4	na
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
od	od	k7c2	od
21	[number]	k4	21
hodin	hodina	k1gFnPc2	hodina
britského	britský	k2eAgInSc2d1	britský
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
22.00	[number]	k4	22.00
hodin	hodina	k1gFnPc2	hodina
letního	letní	k2eAgInSc2d1	letní
středoevropského	středoevropský	k2eAgInSc2d1	středoevropský
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
režisérem	režisér	k1gMnSc7	režisér
uměleckého	umělecký	k2eAgInSc2d1	umělecký
programu	program	k1gInSc2	program
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
zahájení	zahájení	k1gNnSc2	zahájení
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
významný	významný	k2eAgMnSc1d1	významný
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
Danny	Danna	k1gFnSc2	Danna
Boyle	Boyle	k1gFnSc2	Boyle
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahájení	zahájení	k1gNnSc6	zahájení
byla	být	k5eAaImAgFnS	být
prezentována	prezentovat	k5eAaBmNgFnS	prezentovat
britská	britský	k2eAgFnSc1d1	britská
historie	historie	k1gFnSc1	historie
a	a	k8xC	a
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
podle	podle	k7c2	podle
očekávání	očekávání	k1gNnSc2	očekávání
zahájila	zahájit	k5eAaPmAgFnS	zahájit
hlava	hlava	k1gFnSc1	hlava
hostitelského	hostitelský	k2eAgInSc2d1	hostitelský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
britská	britský	k2eAgFnSc1d1	britská
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
v	v	k7c6	v
předtočeném	předtočený	k2eAgInSc6d1	předtočený
klipu	klip	k1gInSc6	klip
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
do	do	k7c2	do
Buckinghamského	buckinghamský	k2eAgInSc2d1	buckinghamský
paláce	palác	k1gInSc2	palác
přijede	přijet	k5eAaPmIp3nS	přijet
Daniel	Daniel	k1gMnSc1	Daniel
Craig	Craig	k1gMnSc1	Craig
coby	coby	k?	coby
představitel	představitel	k1gMnSc1	představitel
Jamese	Jamese	k1gFnSc2	Jamese
Bonda	Bonda	k1gMnSc1	Bonda
a	a	k8xC	a
pokyne	pokynout	k5eAaPmIp3nS	pokynout
královně	královna	k1gFnSc3	královna
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přiletěla	přiletět	k5eAaPmAgFnS	přiletět
ke	k	k7c3	k
stadionu	stadion	k1gInSc2	stadion
helikoptérou	helikoptéra	k1gFnSc7	helikoptéra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hrách	hra	k1gFnPc6	hra
XXX	XXX	kA	XXX
<g/>
.	.	kIx.	.
olympiády	olympiáda	k1gFnSc2	olympiáda
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
soutěže	soutěž	k1gFnPc1	soutěž
v	v	k7c6	v
30	[number]	k4	30
sportovních	sportovní	k2eAgNnPc6d1	sportovní
odvětvích	odvětví	k1gNnPc6	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
programu	program	k1gInSc2	program
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
baseball	baseball	k1gInSc4	baseball
a	a	k8xC	a
softball	softball	k1gInSc4	softball
<g/>
.	.	kIx.	.
</s>
<s>
Aquatics	Aquatics	k6eAd1	Aquatics
Centre	centr	k1gInSc5	centr
(	(	kIx(	(
<g/>
plavání	plavání	k1gNnSc1	plavání
<g/>
,	,	kIx,	,
synchronizované	synchronizovaný	k2eAgNnSc1d1	synchronizované
plavání	plavání	k1gNnSc1	plavání
<g/>
,	,	kIx,	,
skoky	skok	k1gInPc1	skok
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgInSc1d1	moderní
pětiboj	pětiboj	k1gInSc1	pětiboj
<g/>
/	/	kIx~	/
<g/>
plavání	plavání	k1gNnSc1	plavání
<g/>
)	)	kIx)	)
Basketball	Basketball	k1gInSc1	Basketball
Arena	Aren	k1gInSc2	Aren
(	(	kIx(	(
<g/>
košíková	košíková	k1gFnSc1	košíková
<g/>
)	)	kIx)	)
BMX	BMX	kA	BMX
Circuit	Circuit	k1gMnSc1	Circuit
(	(	kIx(	(
<g/>
BMX	BMX	kA	BMX
cyklistika	cyklistika	k1gFnSc1	cyklistika
<g/>
)	)	kIx)	)
Eton	Eton	k1gMnSc1	Eton
Manor	Manor	k1gMnSc1	Manor
(	(	kIx(	(
<g/>
paralympijská	paralympijský	k2eAgFnSc1d1	paralympijská
soutěž	soutěž	k1gFnSc1	soutěž
v	v	k7c6	v
tenise	tenis	k1gInSc6	tenis
vozíčkářů	vozíčkář	k1gMnPc2	vozíčkář
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Handball	Handball	k1gInSc1	Handball
Arena	Aren	k1gInSc2	Aren
-	-	kIx~	-
Copper	Copper	k1gInSc1	Copper
Box	box	k1gInSc1	box
(	(	kIx(	(
<g/>
házená	házená	k1gFnSc1	házená
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgInSc1d1	moderní
pětiboj	pětiboj	k1gInSc1	pětiboj
<g/>
/	/	kIx~	/
<g/>
šerm	šerm	k1gInSc1	šerm
<g/>
)	)	kIx)	)
Hockey	Hockea	k1gFnPc1	Hockea
Centre	centr	k1gInSc5	centr
(	(	kIx(	(
<g/>
pozemní	pozemní	k2eAgInSc4d1	pozemní
hokej	hokej	k1gInSc4	hokej
<g/>
)	)	kIx)	)
Olympic	Olympice	k1gInPc2	Olympice
Stadium	stadium	k1gNnSc1	stadium
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgInSc1d1	hlavní
olympijský	olympijský	k2eAgInSc1d1	olympijský
stadion	stadion	k1gInSc1	stadion
<g/>
,	,	kIx,	,
lehká	lehký	k2eAgFnSc1d1	lehká
atletika	atletika	k1gFnSc1	atletika
<g/>
,	,	kIx,	,
slavností	slavnost	k1gFnSc7	slavnost
zahájení	zahájení	k1gNnSc2	zahájení
a	a	k8xC	a
zakončení	zakončení	k1gNnSc2	zakončení
LOH	LOH	kA	LOH
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Velodrom	velodrom	k1gInSc1	velodrom
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
dráhová	dráhový	k2eAgFnSc1d1	dráhová
cyklistika	cyklistika	k1gFnSc1	cyklistika
<g/>
)	)	kIx)	)
Water	Water	k1gInSc1	Water
Polo	polo	k6eAd1	polo
Arena	Areen	k2eAgFnSc1d1	Arena
(	(	kIx(	(
<g/>
vodní	vodní	k2eAgNnSc1d1	vodní
pólo	pólo	k1gNnSc1	pólo
<g/>
)	)	kIx)	)
All	All	k1gFnPc2	All
England	Englando	k1gNnPc2	Englando
Lawn	Lawn	k1gMnSc1	Lawn
Tennis	Tennis	k1gFnSc2	Tennis
and	and	k?	and
Croquet	Croquet	k1gInSc1	Croquet
Club	club	k1gInSc1	club
(	(	kIx(	(
<g/>
tenis	tenis	k1gInSc1	tenis
<g/>
)	)	kIx)	)
Earls	Earls	k1gInSc1	Earls
Court	Court	k1gInSc1	Court
(	(	kIx(	(
<g/>
volejbal	volejbal	k1gInSc1	volejbal
<g/>
)	)	kIx)	)
ExCeL	Excel	kA	Excel
(	(	kIx(	(
<g/>
box	box	k1gInSc1	box
<g/>
,	,	kIx,	,
šerm	šerm	k1gInSc1	šerm
<g/>
,	,	kIx,	,
judo	judo	k1gNnSc1	judo
<g/>
,	,	kIx,	,
stolní	stolní	k2eAgInSc1d1	stolní
tenis	tenis	k1gInSc1	tenis
<g/>
,	,	kIx,	,
taekwondo	taekwondo	k1gNnSc1	taekwondo
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
vzpírání	vzpírání	k1gNnPc1	vzpírání
<g/>
,	,	kIx,	,
zápas	zápas	k1gInSc1	zápas
<g/>
)	)	kIx)	)
Greenwich	Greenwich	k1gInSc1	Greenwich
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
jezdectví	jezdectví	k1gNnSc1	jezdectví
a	a	k8xC	a
moderní	moderní	k2eAgInSc1d1	moderní
pětiboj	pětiboj	k1gInSc1	pětiboj
<g/>
/	/	kIx~	/
<g/>
jezdectví	jezdectví	k1gNnSc1	jezdectví
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgInSc1d1	moderní
pětiboj	pětiboj	k1gInSc1	pětiboj
<g/>
/	/	kIx~	/
<g/>
běh	běh	k1gInSc1	běh
a	a	k8xC	a
střelba	střelba	k1gFnSc1	střelba
<g/>
)	)	kIx)	)
Hampton	Hampton	k1gInSc1	Hampton
Court	Court	k1gInSc1	Court
Palace	Palace	k1gFnSc1	Palace
(	(	kIx(	(
<g/>
silniční	silniční	k2eAgFnSc1d1	silniční
cyklistika	cyklistika	k1gFnSc1	cyklistika
<g/>
/	/	kIx~	/
<g/>
časovka	časovka	k1gFnSc1	časovka
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
)	)	kIx)	)
Horse	Horse	k1gFnSc1	Horse
Guards	Guardsa	k1gFnPc2	Guardsa
Parade	Parad	k1gInSc5	Parad
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
plážový	plážový	k2eAgInSc1d1	plážový
volejbal	volejbal	k1gInSc1	volejbal
<g/>
)	)	kIx)	)
Hyde	Hyde	k1gInSc1	Hyde
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
olympijský	olympijský	k2eAgInSc1d1	olympijský
triatlon	triatlon	k1gInSc1	triatlon
<g/>
,	,	kIx,	,
dálkové	dálkový	k2eAgNnSc1d1	dálkové
plavání	plavání	k1gNnSc1	plavání
<g/>
)	)	kIx)	)
Lord	lord	k1gMnSc1	lord
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Cricket	Cricket	k1gMnSc1	Cricket
Ground	Ground	k1gMnSc1	Ground
(	(	kIx(	(
<g/>
lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
<g/>
)	)	kIx)	)
North	North	k1gInSc1	North
Greenwich	Greenwich	k1gInSc1	Greenwich
Arena	Arena	k1gFnSc1	Arena
(	(	kIx(	(
<g/>
sportovní	sportovní	k2eAgFnSc1d1	sportovní
gymnastika	gymnastika	k1gFnSc1	gymnastika
a	a	k8xC	a
basketbal	basketbal	k1gInSc1	basketbal
<g/>
)	)	kIx)	)
Royal	Royal	k1gInSc1	Royal
Artillery	Artiller	k1gInPc4	Artiller
Barracks	Barracks	k1gInSc1	Barracks
(	(	kIx(	(
<g/>
sportovní	sportovní	k2eAgFnSc1d1	sportovní
střelba	střelba	k1gFnSc1	střelba
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Mall	Mall	k1gMnSc1	Mall
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
silniční	silniční	k2eAgFnSc1d1	silniční
cyklistika	cyklistika	k1gFnSc1	cyklistika
<g/>
/	/	kIx~	/
<g/>
hromadné	hromadný	k2eAgInPc1d1	hromadný
závody	závod	k1gInPc1	závod
<g/>
,	,	kIx,	,
lehká	lehký	k2eAgFnSc1d1	lehká
atletika	atletika	k1gFnSc1	atletika
-	-	kIx~	-
závody	závod	k1gInPc1	závod
v	v	k7c6	v
chůzi	chůze	k1gFnSc6	chůze
a	a	k8xC	a
maratonské	maratonský	k2eAgInPc1d1	maratonský
běhy	běh	k1gInPc1	běh
<g/>
)	)	kIx)	)
Wembley	Wembley	k1gInPc1	Wembley
Arena	Arena	k1gFnSc1	Arena
(	(	kIx(	(
<g/>
badminton	badminton	k1gInSc1	badminton
a	a	k8xC	a
moderní	moderní	k2eAgFnSc1d1	moderní
gymnastika	gymnastika	k1gFnSc1	gymnastika
<g/>
)	)	kIx)	)
Wembley	Wembley	k1gInPc1	Wembley
Stadion	stadion	k1gNnSc1	stadion
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
Coventry	Coventr	k1gInPc1	Coventr
Stadion	stadion	k1gNnSc1	stadion
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
-	-	kIx~	-
Coventry	Coventr	k1gInPc1	Coventr
Eton	Eton	k1gNnSc4	Eton
Dorney	Dornea	k1gFnSc2	Dornea
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
veslování	veslování	k1gNnPc1	veslování
a	a	k8xC	a
rychlostní	rychlostní	k2eAgFnSc1d1	rychlostní
kanoistika	kanoistika	k1gFnSc1	kanoistika
<g/>
)	)	kIx)	)
-	-	kIx~	-
Eton	Eton	k1gMnSc1	Eton
Hadleigh	Hadleigh	k1gMnSc1	Hadleigh
Farm	Farm	k1gMnSc1	Farm
(	(	kIx(	(
<g/>
cyklistika	cyklistika	k1gFnSc1	cyklistika
na	na	k7c6	na
horských	horský	k2eAgNnPc6d1	horské
kolech	kolo	k1gNnPc6	kolo
<g/>
)	)	kIx)	)
-	-	kIx~	-
Jižní	jižní	k2eAgInSc1d1	jižní
Essex	Essex	k1gInSc1	Essex
<g/>
,	,	kIx,	,
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
hradu	hrad	k1gInSc2	hrad
Hadleigh	Hadleigha	k1gFnPc2	Hadleigha
Hampden	Hampdna	k1gFnPc2	Hampdna
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
-	-	kIx~	-
Glasgow	Glasgow	k1gNnSc4	Glasgow
Lee	Lea	k1gFnSc3	Lea
Valley	Vallea	k1gFnSc2	Vallea
White	Whit	k1gInSc5	Whit
Water	Water	k1gInSc1	Water
Centre	centr	k1gInSc5	centr
-	-	kIx~	-
(	(	kIx(	(
<g/>
slalom	slalom	k1gInSc1	slalom
na	na	k7c6	na
divoké	divoký	k2eAgFnSc6d1	divoká
vodě	voda	k1gFnSc6	voda
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Millennium	millennium	k1gNnSc1	millennium
Stadium	stadium	k1gNnSc1	stadium
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
-	-	kIx~	-
Cardiff	Cardiff	k1gInSc1	Cardiff
Old	Olda	k1gFnPc2	Olda
Trafford	Trafford	k1gInSc1	Trafford
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
-	-	kIx~	-
Manchester	Manchester	k1gInSc1	Manchester
St	St	kA	St
James	James	k1gInSc1	James
<g/>
'	'	kIx"	'
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
-	-	kIx~	-
Newcastle	Newcastle	k1gMnSc1	Newcastle
Weymouth	Weymouth	k1gMnSc1	Weymouth
a	a	k8xC	a
Portland	Portland	k1gInSc1	Portland
(	(	kIx(	(
<g/>
jachting	jachting	k1gInSc1	jachting
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc1d2	podrobnější
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Medailové	medailový	k2eAgNnSc4d1	medailové
pořadí	pořadí	k1gNnSc4	pořadí
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
olympionici	olympionik	k1gMnPc1	olympionik
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
získali	získat	k5eAaPmAgMnP	získat
celkem	celkem	k6eAd1	celkem
11	[number]	k4	11
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
medaile	medaile	k1gFnSc2	medaile
vybojovali	vybojovat	k5eAaPmAgMnP	vybojovat
Miroslava	Miroslav	k1gMnSc2	Miroslav
Knapková	Knapková	k1gFnSc1	Knapková
(	(	kIx(	(
<g/>
skif	skif	k1gInSc1	skif
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Barbora	Barbora	k1gFnSc1	Barbora
Špotáková	Špotáková	k1gFnSc1	Špotáková
(	(	kIx(	(
<g/>
oštěp	oštěp	k1gInSc1	oštěp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
moderní	moderní	k2eAgInSc1d1	moderní
pětiboj	pětiboj	k1gInSc1	pětiboj
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kulhavý	kulhavý	k2eAgMnSc1d1	kulhavý
(	(	kIx(	(
<g/>
horská	horský	k2eAgNnPc4d1	horské
kola	kolo	k1gNnPc4	kolo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
se	se	k3xPyFc4	se
po	po	k7c6	po
60	[number]	k4	60
letech	let	k1gInPc6	let
nedostalo	dostat	k5eNaPmAgNnS	dostat
mezi	mezi	k7c4	mezi
trio	trio	k1gNnSc4	trio
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
medailí	medaile	k1gFnPc2	medaile
zastává	zastávat	k5eAaImIp3nS	zastávat
s	s	k7c7	s
82	[number]	k4	82
kusy	kus	k1gInPc7	kus
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
na	na	k7c4	na
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
5	[number]	k4	5
zlatých	zlatý	k2eAgFnPc2d1	zlatá
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Tenisové	tenisový	k2eAgFnPc1d1	tenisová
soutěže	soutěž	k1gFnPc1	soutěž
se	se	k3xPyFc4	se
odehrávaly	odehrávat	k5eAaImAgFnP	odehrávat
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
na	na	k7c6	na
dvorcích	dvorec	k1gInPc6	dvorec
All	All	k1gFnSc1	All
England	England	k1gInSc1	England
Lawn	Lawn	k1gMnSc1	Lawn
Tennis	Tennis	k1gInSc1	Tennis
and	and	k?	and
Croquet	Croquet	k1gInSc1	Croquet
Clubu	club	k1gInSc2	club
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
článek	článek	k1gInSc1	článek
tenis	tenis	k1gInSc4	tenis
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Pořadatelská	pořadatelský	k2eAgFnSc1d1	pořadatelská
země	země	k1gFnSc1	země
měla	mít	k5eAaImAgFnS	mít
právo	právo	k1gNnSc4	právo
bez	bez	k7c2	bez
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
postavit	postavit	k5eAaPmF	postavit
svůj	svůj	k3xOyFgInSc4	svůj
národní	národní	k2eAgInSc4d1	národní
tým	tým	k1gInSc4	tým
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
kolektivních	kolektivní	k2eAgInPc6d1	kolektivní
sportech	sport	k1gInPc6	sport
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
programu	program	k1gInSc6	program
příslušných	příslušný	k2eAgFnPc2d1	příslušná
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
historickému	historický	k2eAgInSc3d1	historický
zlomu	zlom	k1gInSc3	zlom
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
po	po	k7c6	po
dlouhých	dlouhý	k2eAgNnPc6d1	dlouhé
52	[number]	k4	52
letech	léto	k1gNnPc6	léto
z	z	k7c2	z
prestižních	prestižní	k2eAgInPc2d1	prestižní
důvodů	důvod	k1gInPc2	důvod
postavilo	postavit	k5eAaPmAgNnS	postavit
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
svůj	svůj	k3xOyFgInSc4	svůj
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
tým	tým	k1gInSc4	tým
jednotně	jednotně	k6eAd1	jednotně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
za	za	k7c4	za
celé	celý	k2eAgNnSc4d1	celé
Spojené	spojený	k2eAgNnSc4d1	spojené
království	království	k1gNnSc4	království
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
historické	historický	k2eAgNnSc4d1	historické
rozdělení	rozdělení	k1gNnSc4	rozdělení
jejich	jejich	k3xOp3gInPc2	jejich
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
svazů	svaz	k1gInPc2	svaz
mezi	mezi	k7c4	mezi
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc1	Skotsko
<g/>
,	,	kIx,	,
Wales	Wales	k1gInSc1	Wales
a	a	k8xC	a
Severní	severní	k2eAgNnSc1d1	severní
Irsko	Irsko	k1gNnSc1	Irsko
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
platné	platný	k2eAgFnPc4d1	platná
olympijské	olympijský	k2eAgFnPc4d1	olympijská
regule	regule	k1gFnPc4	regule
takovéto	takovýto	k3xDgNnSc4	takovýto
rozdělení	rozdělení	k1gNnSc4	rozdělení
jedné	jeden	k4xCgFnSc2	jeden
země	zem	k1gFnSc2	zem
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
části	část	k1gFnPc4	část
prakticky	prakticky	k6eAd1	prakticky
vůbec	vůbec	k9	vůbec
neumožňovaly	umožňovat	k5eNaImAgFnP	umožňovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společná	společný	k2eAgFnSc1d1	společná
reprezentace	reprezentace	k1gFnSc1	reprezentace
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
hrála	hrát	k5eAaImAgFnS	hrát
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
před	před	k7c7	před
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
zahájením	zahájení	k1gNnSc7	zahájení
her	hra	k1gFnPc2	hra
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
diplomatickému	diplomatický	k2eAgInSc3d1	diplomatický
incidentu	incident	k1gInSc3	incident
<g/>
,	,	kIx,	,
když	když	k8xS	když
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
zápasu	zápas	k1gInSc2	zápas
žen	žena	k1gFnPc2	žena
KLDR	KLDR	kA	KLDR
<g/>
-	-	kIx~	-
<g/>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
pořadatelé	pořadatel	k1gMnPc1	pořadatel
omylem	omyl	k1gInSc7	omyl
na	na	k7c4	na
světelnou	světelný	k2eAgFnSc4d1	světelná
tabuli	tabule	k1gFnSc4	tabule
umístili	umístit	k5eAaPmAgMnP	umístit
vlajku	vlajka	k1gFnSc4	vlajka
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
KLDR	KLDR	kA	KLDR
<g/>
.	.	kIx.	.
</s>
<s>
Hráčky	hráčka	k1gFnPc1	hráčka
KLDR	KLDR	kA	KLDR
odmítly	odmítnout	k5eAaPmAgFnP	odmítnout
k	k	k7c3	k
utkání	utkání	k1gNnSc3	utkání
nastoupit	nastoupit	k5eAaPmF	nastoupit
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
změnily	změnit	k5eAaPmAgInP	změnit
až	až	k9	až
po	po	k7c6	po
omluvě	omluva	k1gFnSc6	omluva
pořadatelů	pořadatel	k1gMnPc2	pořadatel
<g/>
.	.	kIx.	.
</s>
<s>
Olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
i	i	k9	i
Zara	Zara	k1gFnSc1	Zara
Phillips	Phillipsa	k1gFnPc2	Phillipsa
<g/>
,	,	kIx,	,
vnučka	vnučka	k1gFnSc1	vnučka
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
pořadatelské	pořadatelský	k2eAgFnSc2d1	pořadatelská
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
britské	britský	k2eAgFnSc2d1	britská
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
jezdecké	jezdecký	k2eAgFnSc6d1	jezdecká
všestrannosti	všestrannost	k1gFnSc6	všestrannost
družstev	družstvo	k1gNnPc2	družstvo
získala	získat	k5eAaPmAgFnS	získat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šermu	šerm	k1gInSc6	šerm
kordem	kord	k1gInSc7	kord
žen	žena	k1gFnPc2	žena
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
kontroverzní	kontroverzní	k2eAgFnSc3d1	kontroverzní
situaci	situace	k1gFnSc3	situace
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
porazila	porazit	k5eAaPmAgFnS	porazit
Němka	Němka	k1gFnSc1	Němka
Britta	Britta	k1gFnSc1	Britta
Heidemannová	Heidemannový	k2eAgFnSc1d1	Heidemannový
Korejku	Korejka	k1gFnSc4	Korejka
Sin	sino	k1gNnPc2	sino
A	a	k8xC	a
Lam	lama	k1gFnPc2	lama
až	až	k6eAd1	až
v	v	k7c6	v
prodloužení	prodloužení	k1gNnSc6	prodloužení
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
sekundě	sekunda	k1gFnSc6	sekunda
časového	časový	k2eAgInSc2d1	časový
limitu	limit	k1gInSc2	limit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jihokorejské	jihokorejský	k2eAgFnSc2d1	jihokorejská
výpravy	výprava	k1gFnSc2	výprava
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
vítězný	vítězný	k2eAgInSc4d1	vítězný
zásah	zásah	k1gInSc4	zásah
až	až	k9	až
po	po	k7c6	po
časovém	časový	k2eAgInSc6d1	časový
limitu	limit	k1gInSc6	limit
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
nakonec	nakonec	k6eAd1	nakonec
přisoudili	přisoudit	k5eAaPmAgMnP	přisoudit
vítězství	vítězství	k1gNnSc4	vítězství
Němce	Němec	k1gMnSc2	Němec
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
hodinové	hodinový	k2eAgFnSc6d1	hodinová
diskuzi	diskuze	k1gFnSc6	diskuze
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
seděla	sedět	k5eAaImAgFnS	sedět
korejská	korejský	k2eAgFnSc1d1	Korejská
kordistka	kordistka	k1gFnSc1	kordistka
v	v	k7c6	v
pláči	pláč	k1gInSc6	pláč
a	a	k8xC	a
slzách	slza	k1gFnPc6	slza
na	na	k7c6	na
planši	planš	k1gInSc6	planš
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
odchodem	odchod	k1gInSc7	odchod
by	by	kYmCp3nS	by
uznala	uznat	k5eAaPmAgFnS	uznat
svoji	svůj	k3xOyFgFnSc4	svůj
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
badmintonu	badminton	k1gInSc6	badminton
žen	žena	k1gFnPc2	žena
bylo	být	k5eAaImAgNnS	být
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
osm	osm	k4xCc1	osm
hráček	hráčka	k1gFnPc2	hráčka
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
v	v	k7c6	v
zápasech	zápas	k1gInPc6	zápas
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
hrály	hrát	k5eAaImAgInP	hrát
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
prohrály	prohrát	k5eAaPmAgFnP	prohrát
<g/>
,	,	kIx,	,
a	a	k8xC	a
měly	mít	k5eAaImAgFnP	mít
lepší	dobrý	k2eAgFnPc1d2	lepší
soupeřky	soupeřka	k1gFnPc1	soupeřka
ve	v	k7c6	v
vyřazovacích	vyřazovací	k2eAgInPc6d1	vyřazovací
zápasech	zápas	k1gInPc6	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Badmintonová	badmintonový	k2eAgFnSc1d1	badmintonová
federace	federace	k1gFnSc1	federace
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
porušování	porušování	k1gNnPc4	porušování
základních	základní	k2eAgNnPc2d1	základní
badmintonových	badmintonový	k2eAgNnPc2d1	badmintonové
pravidel	pravidlo	k1gNnPc2	pravidlo
vyloučila	vyloučit	k5eAaPmAgFnS	vyloučit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
znevažování	znevažování	k1gNnSc3	znevažování
sportu	sport	k1gInSc2	sport
a	a	k8xC	a
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgInPc1d1	finanční
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
olympiádu	olympiáda	k1gFnSc4	olympiáda
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
9,3	[number]	k4	9,3
miliardy	miliarda	k4xCgFnSc2	miliarda
liber	libra	k1gFnPc2	libra
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
tři	tři	k4xCgFnPc4	tři
sta	sto	k4xCgNnPc4	sto
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
jistoty	jistota	k1gFnSc2	jistota
návratnosti	návratnost	k1gFnSc2	návratnost
investice	investice	k1gFnSc1	investice
(	(	kIx(	(
<g/>
očekávaná	očekávaný	k2eAgFnSc1d1	očekávaná
návratnost	návratnost	k1gFnSc1	návratnost
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
britské	britský	k2eAgFnSc2d1	britská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
13	[number]	k4	13
mld.	mld.	k?	mld.
liber	libra	k1gFnPc2	libra
<g/>
,	,	kIx,	,
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
421,1	[number]	k4	421,1
mld.	mld.	k?	mld.
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
80	[number]	k4	80
miliónů	milión	k4xCgInPc2	milión
liber	libra	k1gFnPc2	libra
<g/>
,	,	kIx,	,
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
2,6	[number]	k4	2,6
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
českých	český	k2eAgMnPc2d1	český
<g/>
,	,	kIx,	,
stál	stát	k5eAaImAgInS	stát
rozpočet	rozpočet	k1gInSc1	rozpočet
zahajovacích	zahajovací	k2eAgInPc2d1	zahajovací
a	a	k8xC	a
ukončovacích	ukončovací	k2eAgInPc2d1	ukončovací
ceremoniálů	ceremoniál	k1gInPc2	ceremoniál
<g/>
..	..	k?	..
Brokový	brokový	k2eAgMnSc1d1	brokový
střelec	střelec	k1gMnSc1	střelec
Násir	Násira	k1gFnPc2	Násira
Al	ala	k1gFnPc2	ala
Attíja	Attíjus	k1gMnSc2	Attíjus
z	z	k7c2	z
Kataru	katar	k1gInSc2	katar
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
i	i	k9	i
automobilovým	automobilový	k2eAgMnSc7d1	automobilový
závodníkem	závodník	k1gMnSc7	závodník
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
vítězem	vítěz	k1gMnSc7	vítěz
Rallye	rallye	k1gFnSc2	rallye
Dakar	Dakar	k1gInSc1	Dakar
(	(	kIx(	(
<g/>
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
získal	získat	k5eAaPmAgMnS	získat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
ve	v	k7c6	v
střelbě	střelba	k1gFnSc6	střelba
na	na	k7c4	na
skeet	skeet	k1gInSc4	skeet
<g/>
.	.	kIx.	.
</s>
<s>
KOVÁŘ	Kovář	k1gMnSc1	Kovář
<g/>
,	,	kIx,	,
MARTIN	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
a	a	k8xC	a
hry	hra	k1gFnPc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
londýnských	londýnský	k2eAgFnPc2d1	londýnská
olympiád	olympiáda	k1gFnPc2	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
/	/	kIx~	/
Togga	Togga	k1gFnSc1	Togga
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7308-396-0	[number]	k4	978-80-7308-396-0
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
2012	[number]	k4	2012
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Olympijské	olympijský	k2eAgNnSc1d1	Olympijské
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
britské	britský	k2eAgFnSc2d1	britská
veřejnoprávní	veřejnoprávní	k2eAgFnSc2d1	veřejnoprávní
televize	televize	k1gFnSc2	televize
BBC	BBC	kA	BBC
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Olympijské	olympijský	k2eAgNnSc1d1	Olympijské
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
britské	britský	k2eAgFnSc2d1	britská
veřejnoprávní	veřejnoprávní	k2eAgFnSc2d1	veřejnoprávní
televize	televize	k1gFnSc2	televize
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Olympijské	olympijský	k2eAgNnSc1d1	Olympijské
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
české	český	k2eAgFnSc2d1	Česká
veřejnoprávní	veřejnoprávní	k2eAgFnSc2d1	veřejnoprávní
televize	televize	k1gFnSc2	televize
ČT4	ČT4	k1gFnSc2	ČT4
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
z	z	k7c2	z
XXX	XXX	kA	XXX
<g/>
.	.	kIx.	.
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Olympijské	olympijský	k2eAgNnSc1d1	Olympijské
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
britské	britský	k2eAgFnSc2d1	britská
veřejnoprávní	veřejnoprávní	k2eAgFnSc2d1	veřejnoprávní
televize	televize	k1gFnSc2	televize
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
2012	[number]	k4	2012
</s>
