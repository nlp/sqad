<s>
Madeira	Madeira	k1gFnSc1	Madeira
je	být	k5eAaImIp3nS	být
portugalské	portugalský	k2eAgNnSc4d1	portugalské
souostroví	souostroví	k1gNnSc4	souostroví
v	v	k7c6	v
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceánu	oceán	k1gInSc6	oceán
(	(	kIx(	(
<g/>
580	[number]	k4	580
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
Maroka	Maroko	k1gNnSc2	Maroko
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
980	[number]	k4	980
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Lisabonu	Lisabon	k1gInSc2	Lisabon
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
)	)	kIx)	)
se	s	k7c7	s
statusem	status	k1gInSc7	status
autonomní	autonomní	k2eAgFnSc2d1	autonomní
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Madeiře	Madeira	k1gFnSc6	Madeira
žije	žít	k5eAaImIp3nS	žít
271400	[number]	k4	271400
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Funchal	Funchal	k1gFnSc4	Funchal
<g/>
.	.	kIx.	.
</s>
<s>
Jedinými	jediný	k2eAgInPc7d1	jediný
obydlenými	obydlený	k2eAgInPc7d1	obydlený
ostrovy	ostrov	k1gInPc7	ostrov
jsou	být	k5eAaImIp3nP	být
Madeira	Madeira	k1gFnSc1	Madeira
a	a	k8xC	a
Porto	porto	k1gNnSc1	porto
Santo	Sant	k2eAgNnSc1d1	Santo
<g/>
.	.	kIx.	.
</s>
<s>
Madeira	Madeira	k1gFnSc1	Madeira
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
celoročně	celoročně	k6eAd1	celoročně
vyhledávané	vyhledávaný	k2eAgFnPc4d1	vyhledávaná
turistické	turistický	k2eAgFnPc4d1	turistická
lokality	lokalita	k1gFnPc4	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Světově	světově	k6eAd1	světově
proslulé	proslulý	k2eAgFnPc1d1	proslulá
jsou	být	k5eAaImIp3nP	být
novoroční	novoroční	k2eAgFnPc1d1	novoroční
oslavy	oslava	k1gFnPc1	oslava
s	s	k7c7	s
velkolepým	velkolepý	k2eAgInSc7d1	velkolepý
ohňostrojem	ohňostroj	k1gInSc7	ohňostroj
<g/>
,	,	kIx,	,
dojímavé	dojímavý	k2eAgFnPc1d1	dojímavá
scenérie	scenérie	k1gFnPc1	scenérie
<g/>
,	,	kIx,	,
madeirské	madeirský	k2eAgNnSc1d1	Madeirské
víno	víno	k1gNnSc1	víno
a	a	k8xC	a
nádherná	nádherný	k2eAgFnSc1d1	nádherná
flóra	flóra	k1gFnSc1	flóra
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
také	také	k9	také
sopka	sopka	k1gFnSc1	sopka
Pico	Pico	k6eAd1	Pico
Ruivo	Ruivo	k1gNnSc1	Ruivo
<g/>
,	,	kIx,	,
vysoká	vysoká	k1gFnSc1	vysoká
1862	[number]	k4	1862
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
souostroví	souostroví	k1gNnSc2	souostroví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Souostroví	souostroví	k1gNnSc1	souostroví
Madeira	Madeira	k1gFnSc1	Madeira
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
již	již	k6eAd1	již
starým	starý	k2eAgMnPc3d1	starý
Římanům	Říman	k1gMnPc3	Říman
<g/>
,	,	kIx,	,
náhodou	náhodou	k6eAd1	náhodou
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
znovuobjeveno	znovuobjeven	k2eAgNnSc1d1	znovuobjeven
a	a	k8xC	a
osídleno	osídlen	k2eAgNnSc1d1	osídleno
portugalskými	portugalský	k2eAgMnPc7d1	portugalský
námořníky	námořník	k1gMnPc7	námořník
roku	rok	k1gInSc2	rok
1418	[number]	k4	1418
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
souostroví	souostroví	k1gNnSc1	souostroví
Madeira	Madeira	k1gFnSc1	Madeira
je	být	k5eAaImIp3nS	být
sopečného	sopečný	k2eAgInSc2d1	sopečný
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
celá	celý	k2eAgFnSc1d1	celá
oblast	oblast	k1gFnSc1	oblast
Makaronézie	Makaronézie	k1gFnSc1	Makaronézie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
geografického	geografický	k2eAgNnSc2d1	geografické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
situováno	situován	k2eAgNnSc1d1	situováno
na	na	k7c6	na
Africké	africký	k2eAgFnSc6d1	africká
desce	deska	k1gFnSc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
vrcholy	vrchol	k1gInPc4	vrchol
starých	starý	k2eAgFnPc2d1	stará
podmořských	podmořský	k2eAgFnPc2d1	podmořská
sopek	sopka	k1gFnPc2	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Moře	moře	k1gNnSc1	moře
mezi	mezi	k7c7	mezi
ostrovy	ostrov	k1gInPc7	ostrov
madeirského	madeirský	k2eAgNnSc2d1	Madeirské
souostroví	souostroví	k1gNnSc2	souostroví
má	mít	k5eAaImIp3nS	mít
hloubku	hloubka	k1gFnSc4	hloubka
kolem	kolem	k7c2	kolem
4000	[number]	k4	4000
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
5	[number]	k4	5
km	km	kA	km
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hloubka	hloubka	k1gFnSc1	hloubka
moře	moře	k1gNnSc1	moře
3000	[number]	k4	3000
m	m	kA	m
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Souostroví	souostroví	k1gNnSc1	souostroví
je	být	k5eAaImIp3nS	být
omýváno	omývat	k5eAaImNgNnS	omývat
Golfským	golfský	k2eAgInSc7d1	golfský
proudem	proud	k1gInSc7	proud
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
kolem	kolem	k7c2	kolem
ostrova	ostrov	k1gInSc2	ostrov
Madeira	Madeira	k1gFnSc1	Madeira
ani	ani	k8xC	ani
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
neklesá	klesat	k5eNaImIp3nS	klesat
pod	pod	k7c4	pod
18	[number]	k4	18
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
"	"	kIx"	"
<g/>
věčného	věčný	k2eAgNnSc2d1	věčné
jara	jaro	k1gNnSc2	jaro
<g/>
"	"	kIx"	"
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Souostroví	souostroví	k1gNnSc1	souostroví
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
člení	členit	k5eAaImIp3nS	členit
na	na	k7c6	na
4	[number]	k4	4
části	část	k1gFnSc6	část
<g/>
:	:	kIx,	:
Madeira	Madeira	k1gFnSc1	Madeira
(	(	kIx(	(
<g/>
740,7	[number]	k4	740,7
km2	km2	k4	km2
<g/>
)	)	kIx)	)
Porto	porto	k1gNnSc1	porto
Santo	Santo	k1gNnSc1	Santo
(	(	kIx(	(
<g/>
42,5	[number]	k4	42,5
km2	km2	k4	km2
<g/>
)	)	kIx)	)
Ilhas	Ilhas	k1gMnSc1	Ilhas
Desertas	Desertas	k1gMnSc1	Desertas
(	(	kIx(	(
<g/>
14,2	[number]	k4	14,2
km2	km2	k4	km2
<g/>
)	)	kIx)	)
Ilhas	Ilhas	k1gInSc1	Ilhas
Selvagens	Selvagens	k1gInSc1	Selvagens
(	(	kIx(	(
<g/>
3,6	[number]	k4	3,6
km2	km2	k4	km2
<g/>
)	)	kIx)	)
Hlavním	hlavní	k2eAgInSc7d1	hlavní
a	a	k8xC	a
největším	veliký	k2eAgInSc7d3	veliký
ostrovem	ostrov	k1gInSc7	ostrov
je	být	k5eAaImIp3nS	být
Madeira	Madeira	k1gFnSc1	Madeira
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
žijí	žít	k5eAaImIp3nP	žít
téměř	téměř	k6eAd1	téměř
všichni	všechen	k3xTgMnPc1	všechen
obyvatelé	obyvatel	k1gMnPc1	obyvatel
souostroví	souostroví	k1gNnPc2	souostroví
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
obydleném	obydlený	k2eAgInSc6d1	obydlený
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
Porto	porto	k1gNnSc1	porto
Santo	Sant	k2eAgNnSc1d1	Santo
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
necelých	celý	k2eNgInPc2d1	necelý
4500	[number]	k4	4500
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
Desertas	Desertasa	k1gFnPc2	Desertasa
jsou	být	k5eAaImIp3nP	být
jakýmsi	jakýsi	k3yIgNnSc7	jakýsi
pokračováním	pokračování	k1gNnSc7	pokračování
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
ostrova	ostrov	k1gInSc2	ostrov
Madeira	Madeira	k1gFnSc1	Madeira
<g/>
,	,	kIx,	,
od	od	k7c2	od
něhož	jenž	k3xRgInSc2	jenž
je	být	k5eAaImIp3nS	být
dělí	dělit	k5eAaImIp3nP	dělit
20	[number]	k4	20
km	km	kA	km
vodní	vodní	k2eAgFnSc2d1	vodní
plochy	plocha	k1gFnSc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
úžinou	úžina	k1gFnSc7	úžina
proplouvají	proplouvat	k5eAaImIp3nP	proplouvat
lodě	loď	k1gFnPc1	loď
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
také	také	k9	také
tudy	tudy	k6eAd1	tudy
pravidelně	pravidelně	k6eAd1	pravidelně
táhnou	táhnout	k5eAaImIp3nP	táhnout
velryby	velryba	k1gFnPc1	velryba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
základem	základ	k1gInSc7	základ
zdejšího	zdejší	k2eAgInSc2d1	zdejší
lovu	lov	k1gInSc2	lov
a	a	k8xC	a
zpracování	zpracování	k1gNnSc2	zpracování
velryb	velryba	k1gFnPc2	velryba
<g/>
.	.	kIx.	.
</s>
<s>
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
velrybáři	velrybář	k1gMnPc1	velrybář
dnes	dnes	k6eAd1	dnes
pracují	pracovat	k5eAaImIp3nP	pracovat
v	v	k7c6	v
Muzeu	muzeum	k1gNnSc6	muzeum
velrybářství	velrybářství	k1gNnSc2	velrybářství
v	v	k7c6	v
Caniçalu	Caniçal	k1gInSc6	Caniçal
nebo	nebo	k8xC	nebo
v	v	k7c6	v
člunech	člun	k1gInPc6	člun
provázejí	provázet	k5eAaImIp3nP	provázet
turisty	turist	k1gMnPc7	turist
pozorující	pozorující	k2eAgFnSc2d1	pozorující
velryby	velryba	k1gFnSc2	velryba
<g/>
.	.	kIx.	.
</s>
<s>
Souostroví	souostroví	k1gNnSc1	souostroví
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
pozorovatelné	pozorovatelný	k2eAgNnSc1d1	pozorovatelné
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
jižního	jižní	k2eAgNnSc2d1	jižní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Madeiry	Madeira	k1gFnSc2	Madeira
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
přírodní	přírodní	k2eAgFnSc7d1	přírodní
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
,	,	kIx,	,
přísně	přísně	k6eAd1	přísně
hlídanou	hlídaný	k2eAgFnSc7d1	hlídaná
ochranáři	ochranář	k1gMnPc5	ochranář
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
zde	zde	k6eAd1	zde
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejvzácnějším	vzácný	k2eAgMnSc7d3	nejvzácnější
chráněným	chráněný	k2eAgMnSc7d1	chráněný
živočichem	živočich	k1gMnSc7	živočich
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
tuleň	tuleň	k1gMnSc1	tuleň
středomořský	středomořský	k2eAgMnSc1d1	středomořský
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
Selvagens	Selvagensa	k1gFnPc2	Selvagensa
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
přírodní	přírodní	k2eAgFnSc7d1	přírodní
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
lodím	lodit	k5eAaImIp1nS	lodit
plujícím	plující	k2eAgInSc7d1	plující
na	na	k7c4	na
Kanárské	kanárský	k2eAgInPc4d1	kanárský
ostrovy	ostrov	k1gInPc4	ostrov
a	a	k8xC	a
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgFnP	být
častou	častý	k2eAgFnSc7d1	častá
příčinou	příčina	k1gFnSc7	příčina
neshod	neshoda	k1gFnPc2	neshoda
mezi	mezi	k7c7	mezi
Španělskem	Španělsko	k1gNnSc7	Španělsko
a	a	k8xC	a
Portugalskem	Portugalsko	k1gNnSc7	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc4	seznam
11	[number]	k4	11
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
celků	celek	k1gInPc2	celek
na	na	k7c6	na
Madeiře	Madeira	k1gFnSc6	Madeira
<g/>
:	:	kIx,	:
Calheta	Calhet	k2eAgFnSc1d1	Calhet
Câmara	Câmara	k1gFnSc1	Câmara
de	de	k?	de
Lobos	Lobos	k1gMnSc1	Lobos
Funchal	Funchal	k1gMnSc1	Funchal
Machico	Machico	k1gMnSc1	Machico
Ponta	Ponta	k1gMnSc1	Ponta
do	do	k7c2	do
Sol	sol	k1gNnSc2	sol
Porto	porto	k1gNnSc1	porto
Moniz	Moniz	k1gInSc1	Moniz
Porto	porto	k1gNnSc1	porto
Santo	Santo	k1gNnSc4	Santo
Ribeira	Ribeir	k1gMnSc4	Ribeir
Brava	bravo	k1gMnSc2	bravo
Santa	Sant	k1gMnSc2	Sant
Cruz	Cruz	k1gMnSc1	Cruz
Santana	Santan	k1gMnSc2	Santan
Sã	Sã	k1gFnSc2	Sã
Vicente	Vicent	k1gMnSc5	Vicent
</s>
