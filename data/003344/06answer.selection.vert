<s>
Skupina	skupina	k1gFnSc1	skupina
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
devět	devět	k4xCc4	devět
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
a	a	k8xC	a
jako	jako	k9	jako
jediné	jediný	k2eAgFnSc3d1	jediná
kapele	kapela	k1gFnSc3	kapela
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
bodovat	bodovat	k5eAaImF	bodovat
v	v	k7c6	v
albovém	albový	k2eAgNnSc6d1	albové
žebříčku	žebříčko	k1gNnSc6	žebříčko
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
s	s	k7c7	s
pěti	pět	k4xCc7	pět
po	po	k7c6	po
sobě	se	k3xPyFc3	se
následujícími	následující	k2eAgNnPc7d1	následující
alby	album	k1gNnPc7	album
<g/>
.	.	kIx.	.
</s>
