<p>
<s>
Tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
tuberculum	tuberculum	k1gInSc1	tuberculum
–	–	k?	–
hrbolek	hrbolek	k1gInSc1	hrbolek
<g/>
,	,	kIx,	,
nádorek	nádorek	k1gInSc1	nádorek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
TBC	TBC	kA	TBC
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
souchotiny	souchotiny	k1gFnPc4	souchotiny
či	či	k8xC	či
úbytě	úbytě	k1gFnPc4	úbytě
(	(	kIx(	(
<g/>
oubytě	oubytě	k6eAd1	oubytě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
infekční	infekční	k2eAgNnSc4d1	infekční
onemocnění	onemocnění	k1gNnSc4	onemocnění
způsobené	způsobený	k2eAgNnSc4d1	způsobené
bakteriemi	bakterie	k1gFnPc7	bakterie
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
tuberculosis	tuberculosis	k1gFnPc2	tuberculosis
komplex	komplex	k1gInSc4	komplex
s	s	k7c7	s
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
zástupcem	zástupce	k1gMnSc7	zástupce
M.	M.	kA	M.
tuberculosis	tuberculosis	k1gFnPc4	tuberculosis
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
napadá	napadat	k5eAaPmIp3nS	napadat
nejen	nejen	k6eAd1	nejen
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
většinou	většina	k1gFnSc7	většina
napadá	napadat	k5eAaPmIp3nS	napadat
plíce	plíce	k1gFnPc4	plíce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
postihnout	postihnout	k5eAaPmF	postihnout
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
části	část	k1gFnPc4	část
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Šíří	šířit	k5eAaImIp3nS	šířit
se	s	k7c7	s
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
,	,	kIx,	,
když	když	k8xS	když
osoba	osoba	k1gFnSc1	osoba
s	s	k7c7	s
aktivní	aktivní	k2eAgFnSc7d1	aktivní
formou	forma	k1gFnSc7	forma
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
kašle	kašlat	k5eAaImIp3nS	kašlat
<g/>
,	,	kIx,	,
kýchá	kýchat	k5eAaImIp3nS	kýchat
nebo	nebo	k8xC	nebo
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
své	svůj	k3xOyFgFnPc4	svůj
sliny	slina	k1gFnPc4	slina
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
infekcí	infekce	k1gFnPc2	infekce
je	být	k5eAaImIp3nS	být
asymptomatická	asymptomatický	k2eAgFnSc1d1	asymptomatická
<g/>
,	,	kIx,	,
latentní	latentní	k2eAgFnSc1d1	latentní
<g/>
;	;	kIx,	;
zhruba	zhruba	k6eAd1	zhruba
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
deseti	deset	k4xCc2	deset
onemocnění	onemocnění	k1gNnPc2	onemocnění
přejde	přejít	k5eAaPmIp3nS	přejít
v	v	k7c4	v
aktivní	aktivní	k2eAgFnSc4d1	aktivní
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
neléčí	léčit	k5eNaImIp3nS	léčit
<g/>
,	,	kIx,	,
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
úmrtí	úmrtí	k1gNnSc4	úmrtí
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
medicínský	medicínský	k2eAgInSc1d1	medicínský
obor	obor	k1gInSc1	obor
ftizeologie	ftizeologie	k1gFnSc1	ftizeologie
<g/>
.	.	kIx.	.
<g/>
Nejtypičtějším	typický	k2eAgInSc7d3	nejtypičtější
příznakem	příznak	k1gInSc7	příznak
je	být	k5eAaImIp3nS	být
chronický	chronický	k2eAgInSc1d1	chronický
kašel	kašel	k1gInSc1	kašel
s	s	k7c7	s
krvavým	krvavý	k2eAgNnSc7d1	krvavé
sputem	sputum	k1gNnSc7	sputum
–	–	k?	–
vykašlávání	vykašlávání	k1gNnSc1	vykašlávání
nebo	nebo	k8xC	nebo
chrlení	chrlení	k1gNnSc1	chrlení
krve	krev	k1gFnSc2	krev
(	(	kIx(	(
<g/>
hemoptoe	hemoptoe	k1gFnSc1	hemoptoe
nebo	nebo	k8xC	nebo
také	také	k9	také
hemoptýza	hemoptýza	k1gFnSc1	hemoptýza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
horečka	horečka	k1gFnSc1	horečka
(	(	kIx(	(
<g/>
Febris	Febris	k1gInSc1	Febris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
noční	noční	k2eAgNnSc1d1	noční
pocení	pocení	k1gNnSc1	pocení
a	a	k8xC	a
ztráta	ztráta	k1gFnSc1	ztráta
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
dalších	další	k2eAgInPc2d1	další
orgánů	orgán	k1gInPc2	orgán
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
velkou	velký	k2eAgFnSc4d1	velká
škálu	škála	k1gFnSc4	škála
symptomů	symptom	k1gInPc2	symptom
<g/>
.	.	kIx.	.
</s>
<s>
Diagnóza	diagnóza	k1gFnSc1	diagnóza
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
mikrobiologickým	mikrobiologický	k2eAgNnSc7d1	mikrobiologické
vyšetřením	vyšetření	k1gNnSc7	vyšetření
sputa	sputum	k1gNnSc2	sputum
či	či	k8xC	či
odebraných	odebraný	k2eAgInPc2d1	odebraný
vzorků	vzorek	k1gInPc2	vzorek
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
tuberkulinovým	tuberkulinový	k2eAgInSc7d1	tuberkulinový
testem	test	k1gInSc7	test
<g/>
,	,	kIx,	,
pomocí	pomoc	k1gFnSc7	pomoc
radiologie	radiologie	k1gFnSc2	radiologie
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
rentgenem	rentgen	k1gInSc7	rentgen
hrudi	hruď	k1gFnSc2	hruď
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
krevními	krevní	k2eAgInPc7d1	krevní
testy	test	k1gInPc7	test
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
je	být	k5eAaImIp3nS	být
náročná	náročný	k2eAgFnSc1d1	náročná
a	a	k8xC	a
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
užívání	užívání	k1gNnSc1	užívání
různých	různý	k2eAgNnPc2d1	různé
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Vzrůstajícím	vzrůstající	k2eAgInSc7d1	vzrůstající
problémem	problém	k1gInSc7	problém
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
je	být	k5eAaImIp3nS	být
antibiotická	antibiotický	k2eAgFnSc1d1	antibiotická
rezistence	rezistence	k1gFnSc1	rezistence
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Prevence	prevence	k1gFnSc1	prevence
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
screeningu	screening	k1gInSc6	screening
a	a	k8xC	a
očkování	očkování	k1gNnSc6	očkování
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
bacilem	bacil	k1gInSc7	bacil
Calmettovým-Guérinovým	Calmettovým-Guérinův	k2eAgInSc7d1	Calmettovým-Guérinův
(	(	kIx(	(
<g/>
BCG	BCG	kA	BCG
<g/>
)	)	kIx)	)
–	–	k?	–
očkování	očkování	k1gNnSc2	očkování
proti	proti	k7c3	proti
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
kalmetizace	kalmetizace	k1gFnSc1	kalmetizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
třetina	třetina	k1gFnSc1	třetina
světové	světový	k2eAgFnSc2d1	světová
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
infikována	infikovat	k5eAaBmNgFnS	infikovat
bakterií	bakterie	k1gFnSc7	bakterie
M.	M.	kA	M.
tuberculosis	tuberculosis	k1gFnSc7	tuberculosis
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
každou	každý	k3xTgFnSc4	každý
sekundu	sekunda	k1gFnSc4	sekunda
je	být	k5eAaImIp3nS	být
infikován	infikován	k2eAgMnSc1d1	infikován
nový	nový	k2eAgMnSc1d1	nový
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Procento	procento	k1gNnSc1	procento
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stejné	stejný	k2eAgNnSc1d1	stejné
nebo	nebo	k8xC	nebo
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
růstu	růst	k1gInSc2	růst
populace	populace	k1gFnSc2	populace
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
nakažených	nakažený	k2eAgMnPc2d1	nakažený
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
odhadováno	odhadovat	k5eAaImNgNnS	odhadovat
na	na	k7c4	na
13,7	[number]	k4	13,7
milionů	milion	k4xCgInPc2	milion
chronických	chronický	k2eAgInPc2d1	chronický
aktivních	aktivní	k2eAgInPc2d1	aktivní
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
9,3	[number]	k4	9,3
milionů	milion	k4xCgInPc2	milion
nových	nový	k2eAgInPc2d1	nový
případů	případ	k1gInPc2	případ
a	a	k8xC	a
1,8	[number]	k4	1,8
milionů	milion	k4xCgInPc2	milion
úmrtí	úmrtí	k1gNnPc2	úmrtí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
není	být	k5eNaImIp3nS	být
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
–	–	k?	–
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
asijských	asijský	k2eAgFnPc6d1	asijská
a	a	k8xC	a
afrických	africký	k2eAgFnPc6d1	africká
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
nakaženo	nakažen	k2eAgNnSc1d1	nakaženo
až	až	k6eAd1	až
80	[number]	k4	80
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
jen	jen	k9	jen
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
</s>
</p>
<p>
<s>
hlášeno	hlášen	k2eAgNnSc1d1	hlášeno
518	[number]	k4	518
onemocnění	onemocnění	k1gNnSc1	onemocnění
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
4,9	[number]	k4	4,9
případu	případ	k1gInSc2	případ
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
710	[number]	k4	710
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
6,8	[number]	k4	6,8
na	na	k7c4	na
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Terminologie	terminologie	k1gFnSc2	terminologie
==	==	k?	==
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
říkalo	říkat	k5eAaImAgNnS	říkat
souchotiny	souchotiny	k1gFnPc4	souchotiny
<g/>
,	,	kIx,	,
úbytě	úbytě	k1gFnPc4	úbytě
nebo	nebo	k8xC	nebo
ftíza	ftíza	k1gFnSc1	ftíza
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
řecký	řecký	k2eAgInSc1d1	řecký
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
úbytek	úbytek	k1gInSc4	úbytek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidi	člověk	k1gMnPc4	člověk
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
zevnitř	zevnitř	k6eAd1	zevnitř
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
názvy	název	k1gInPc7	název
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
bílý	bílý	k2eAgInSc4d1	bílý
mor	mor	k1gInSc4	mor
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemocní	nemocný	k1gMnPc1	nemocný
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
bledí	bledý	k2eAgMnPc1d1	bledý
<g/>
;	;	kIx,	;
nebo	nebo	k8xC	nebo
královo	králův	k2eAgNnSc4d1	královo
zlo	zlo	k1gNnSc4	zlo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
králův	králův	k2eAgInSc1d1	králův
dotyk	dotyk	k1gInSc1	dotyk
souchotiny	souchotiny	k1gFnPc4	souchotiny
vyléčí	vyléčit	k5eAaPmIp3nS	vyléčit
<g/>
.	.	kIx.	.
</s>
<s>
Krtice	krtice	k1gFnSc1	krtice
je	být	k5eAaImIp3nS	být
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
napadající	napadající	k2eAgInSc1d1	napadající
lymfatický	lymfatický	k2eAgInSc1d1	lymfatický
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
okruží	okruží	k1gNnSc2	okruží
napadá	napadat	k5eAaPmIp3nS	napadat
břicho	břicho	k1gNnSc4	břicho
<g/>
,	,	kIx,	,
lupus	lupus	k1gInSc4	lupus
vulgaris	vulgaris	k1gFnSc2	vulgaris
napadá	napadat	k5eAaBmIp3nS	napadat
kůži	kůže	k1gFnSc4	kůže
a	a	k8xC	a
Pottova	Pottův	k2eAgFnSc1d1	Pottův
nemoc	nemoc	k1gFnSc1	nemoc
je	být	k5eAaImIp3nS	být
zánět	zánět	k1gInSc4	zánět
páteře	páteř	k1gFnSc2	páteř
a	a	k8xC	a
obratlů	obratel	k1gInPc2	obratel
<g/>
.	.	kIx.	.
<g/>
Miliární	miliární	k2eAgFnSc1d1	miliární
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
vzniká	vznikat	k5eAaImIp3nS	vznikat
rozsevem	rozsev	k1gInSc7	rozsev
mykobakterií	mykobakterie	k1gFnPc2	mykobakterie
přes	přes	k7c4	přes
oběhovou	oběhový	k2eAgFnSc4d1	oběhová
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Bakterii	bakterie	k1gFnSc3	bakterie
M.	M.	kA	M.
tuberculosis	tuberculosis	k1gInSc1	tuberculosis
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
říká	říkat	k5eAaImIp3nS	říkat
Kochův	Kochův	k2eAgInSc4d1	Kochův
bacil	bacil	k1gInSc4	bacil
po	po	k7c6	po
jejím	její	k3xOp3gMnSc6	její
objeviteli	objevitel	k1gMnSc6	objevitel
Robertu	Robert	k1gMnSc6	Robert
Kochovi	Koch	k1gMnSc6	Koch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
se	se	k3xPyFc4	se
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
už	už	k6eAd1	už
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
<g/>
.	.	kIx.	.
</s>
<s>
Genetická	genetický	k2eAgFnSc1d1	genetická
analýza	analýza	k1gFnSc1	analýza
indikuje	indikovat	k5eAaBmIp3nS	indikovat
stáří	stáří	k1gNnSc4	stáří
40	[number]	k4	40
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
nález	nález	k1gInSc1	nález
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
tuberculosis	tuberculosis	k1gInSc1	tuberculosis
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
bizona	bizon	k1gMnSc2	bizon
starého	starý	k1gMnSc2	starý
zhruba	zhruba	k6eAd1	zhruba
18	[number]	k4	18
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
u	u	k7c2	u
dobytka	dobytek	k1gInSc2	dobytek
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
přenesla	přenést	k5eAaPmAgFnS	přenést
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
(	(	kIx(	(
<g/>
zoonóza	zoonóza	k1gFnSc1	zoonóza
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
odpojila	odpojit	k5eAaPmAgFnS	odpojit
od	od	k7c2	od
společného	společný	k2eAgMnSc2d1	společný
předka	předek	k1gMnSc2	předek
infikujícího	infikující	k2eAgInSc2d1	infikující
jiné	jiný	k2eAgInPc4d1	jiný
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Spíše	spíše	k9	spíše
půjde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
původní	původní	k2eAgNnSc4d1	původní
onemocnění	onemocnění	k1gNnSc4	onemocnění
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
možná	možná	k9	možná
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
používání	používání	k1gNnSc1	používání
ohně	oheň	k1gInSc2	oheň
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
tuberculosis	tuberculosis	k1gFnSc1	tuberculosis
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejstarší	starý	k2eAgInSc4d3	nejstarší
druh	druh	k1gInSc4	druh
tuberkulózních	tuberkulózní	k2eAgFnPc2d1	tuberkulózní
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
odpojila	odpojit	k5eAaPmAgFnS	odpojit
linie	linie	k1gFnSc1	linie
bakterií	bakterie	k1gFnPc2	bakterie
M.	M.	kA	M.
africanum	africanum	k1gInSc1	africanum
<g/>
,	,	kIx,	,
M.	M.	kA	M.
microti	microt	k1gMnPc1	microt
<g/>
,	,	kIx,	,
M.	M.	kA	M.
pinnipedii	pinnipedie	k1gFnSc4	pinnipedie
<g/>
,	,	kIx,	,
M.	M.	kA	M.
caprae	caprae	k1gInSc1	caprae
a	a	k8xC	a
M.	M.	kA	M.
bovis	bovis	k1gInSc1	bovis
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
bovis	bovis	k1gFnSc1	bovis
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
druhů	druh	k1gInPc2	druh
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejstarší	starý	k2eAgInPc4d3	nejstarší
<g/>
.	.	kIx.	.
<g/>
Kosterní	kosterní	k2eAgInPc4d1	kosterní
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
z	z	k7c2	z
neolitické	neolitický	k2eAgFnSc2d1	neolitická
osady	osada	k1gFnSc2	osada
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
středomoří	středomoří	k1gNnSc6	středomoří
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
období	období	k1gNnSc2	období
7000	[number]	k4	7000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
měli	mít	k5eAaImAgMnP	mít
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
a	a	k8xC	a
známky	známka	k1gFnPc1	známka
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
se	se	k3xPyFc4	se
našly	najít	k5eAaPmAgFnP	najít
i	i	k9	i
v	v	k7c6	v
mumiích	mumie	k1gFnPc6	mumie
z	z	k7c2	z
období	období	k1gNnSc2	období
3000	[number]	k4	3000
<g/>
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2400	[number]	k4	2400
let	let	k1gInSc1	let
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Řecký	řecký	k2eAgInSc1d1	řecký
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
je	být	k5eAaImIp3nS	být
phthisis	phthisis	k1gInSc1	phthisis
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
ftíza	ftíz	k1gMnSc2	ftíz
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
460	[number]	k4	460
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l	l	kA	l
určil	určit	k5eAaPmAgInS	určit
Hippokratés	Hippokratés	k1gInSc4	Hippokratés
ftízu	ftíz	k1gInSc2	ftíz
jako	jako	k8xS	jako
nejrozšířenější	rozšířený	k2eAgNnSc4d3	nejrozšířenější
onemocnění	onemocnění	k1gNnSc4	onemocnění
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
vykašlávání	vykašlávání	k1gNnSc4	vykašlávání
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
horečku	horečka	k1gFnSc4	horečka
a	a	k8xC	a
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
smrtelné	smrtelný	k2eAgNnSc1d1	smrtelné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
pochází	pocházet	k5eAaImIp3nP	pocházet
první	první	k4xOgInPc1	první
důkazy	důkaz	k1gInPc1	důkaz
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
od	od	k7c2	od
kultury	kultura	k1gFnSc2	kultura
Paracas	Paracas	k1gInSc1	Paracas
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
750	[number]	k4	750
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Suzanne	Suzannout	k5eAaPmIp3nS	Suzannout
Austin	Austin	k2eAgInSc4d1	Austin
Alchon	Alchon	k1gInSc4	Alchon
napsala	napsat	k5eAaBmAgFnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
kosterní	kosterní	k2eAgInPc1d1	kosterní
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
z	z	k7c2	z
pravěké	pravěký	k2eAgFnSc2d1	pravěká
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
častá	častý	k2eAgFnSc1d1	častá
<g/>
,	,	kIx,	,
že	že	k8xS	že
'	'	kIx"	'
<g/>
prakticky	prakticky	k6eAd1	prakticky
každý	každý	k3xTgMnSc1	každý
člen	člen	k1gMnSc1	člen
těchto	tento	k3xDgFnPc2	tento
pravěkých	pravěký	k2eAgFnPc2d1	pravěká
společností	společnost	k1gFnPc2	společnost
byl	být	k5eAaImAgInS	být
vystaven	vystaven	k2eAgInSc1d1	vystaven
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
patřila	patřit	k5eAaImAgFnS	patřit
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgFnPc4d3	nejčastější
příčiny	příčina	k1gFnPc4	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1862	[number]	k4	1862
z	z	k7c2	z
527	[number]	k4	527
zemřelých	zemřelý	k1gMnPc2	zemřelý
byla	být	k5eAaImAgFnS	být
příčinou	příčina	k1gFnSc7	příčina
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
u	u	k7c2	u
155	[number]	k4	155
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
sešlost	sešlost	k1gFnSc4	sešlost
věkem	věk	k1gInSc7	věk
<g/>
,	,	kIx,	,
vysílení	vysílení	k1gNnSc4	vysílení
a	a	k8xC	a
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
slabost	slabost	k1gFnSc4	slabost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
druhá	druhý	k4xOgFnSc1	druhý
nejčastější	častý	k2eAgFnSc1d3	nejčastější
příčina	příčina	k1gFnSc1	příčina
<g/>
,	,	kIx,	,
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
období	období	k1gNnSc6	období
jen	jen	k9	jen
85	[number]	k4	85
Pražanů	Pražan	k1gMnPc2	Pražan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výzkum	výzkum	k1gInSc1	výzkum
a	a	k8xC	a
léčba	léčba	k1gFnSc1	léčba
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
se	se	k3xPyFc4	se
léčba	léčba	k1gFnSc1	léčba
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
stravu	strava	k1gFnSc4	strava
nemocných	nemocný	k1gMnPc2	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Plinius	Plinius	k1gMnSc1	Plinius
starší	starší	k1gMnSc1	starší
popsal	popsat	k5eAaPmAgMnS	popsat
několik	několik	k4yIc4	několik
metod	metoda	k1gFnPc2	metoda
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Historia	Historium	k1gNnSc2	Historium
naturalis	naturalis	k1gFnSc2	naturalis
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
vlčí	vlčet	k5eAaImIp3nP	vlčet
játra	játra	k1gNnPc1	játra
ponořená	ponořený	k2eAgNnPc1d1	ponořené
v	v	k7c6	v
řídkém	řídký	k2eAgNnSc6d1	řídké
víně	víno	k1gNnSc6	víno
<g/>
,	,	kIx,	,
sádlo	sádlo	k1gNnSc1	sádlo
prasete	prase	k1gNnSc2	prase
krmeného	krmený	k2eAgNnSc2d1	krmené
trávou	tráva	k1gFnSc7	tráva
nebo	nebo	k8xC	nebo
kůže	kůže	k1gFnSc1	kůže
oslice	oslice	k1gFnSc2	oslice
ponořená	ponořený	k2eAgFnSc1d1	ponořená
ve	v	k7c6	v
vývaru	vývar	k1gInSc6	vývar
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Avicenna	Avicenna	k6eAd1	Avicenna
psal	psát	k5eAaImAgMnS	psát
o	o	k7c6	o
tuberkulóze	tuberkulóza	k1gFnSc6	tuberkulóza
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Kánon	kánon	k1gInSc1	kánon
medicíny	medicína	k1gFnSc2	medicína
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Řecích	Řek	k1gMnPc6	Řek
převzal	převzít	k5eAaPmAgMnS	převzít
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
onemocnění	onemocnění	k1gNnSc1	onemocnění
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
znečištění	znečištění	k1gNnSc4	znečištění
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
teorie	teorie	k1gFnSc1	teorie
miasmat	miasma	k1gNnPc2	miasma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1689	[number]	k4	1689
Richard	Richard	k1gMnSc1	Richard
Morton	Morton	k1gInSc4	Morton
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
plicní	plicní	k2eAgFnSc1d1	plicní
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
nádory	nádor	k1gInPc7	nádor
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
tuberkly	tuberkl	k1gInPc1	tuberkl
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
až	až	k9	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
kvůli	kvůli	k7c3	kvůli
rozmanitosti	rozmanitost	k1gFnSc3	rozmanitost
symptomů	symptom	k1gInPc2	symptom
označena	označit	k5eAaPmNgFnS	označit
za	za	k7c2	za
jedno	jeden	k4xCgNnSc4	jeden
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
"	"	kIx"	"
<g/>
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
"	"	kIx"	"
ji	on	k3xPp3gFnSc4	on
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
Johann	Johann	k1gMnSc1	Johann
Lukas	Lukas	k1gMnSc1	Lukas
Schönlein	Schönlein	k1gMnSc1	Schönlein
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1838	[number]	k4	1838
<g/>
–	–	k?	–
<g/>
1845	[number]	k4	1845
se	se	k3xPyFc4	se
John	John	k1gMnSc1	John
Croghan	Croghan	k1gMnSc1	Croghan
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
Mamutí	mamutí	k2eAgFnSc2d1	mamutí
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
vyléčit	vyléčit	k5eAaPmF	vyléčit
několik	několik	k4yIc4	několik
pacientů	pacient	k1gMnPc2	pacient
stálou	stálý	k2eAgFnSc7d1	stálá
teplotou	teplota	k1gFnSc7	teplota
a	a	k8xC	a
čistotou	čistota	k1gFnSc7	čistota
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
jeskyni	jeskyně	k1gFnSc6	jeskyně
<g/>
;	;	kIx,	;
všichni	všechen	k3xTgMnPc1	všechen
nemocní	nemocný	k1gMnPc1	nemocný
do	do	k7c2	do
roka	rok	k1gInSc2	rok
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
sanatorium	sanatorium	k1gNnSc1	sanatorium
pro	pro	k7c4	pro
pacienty	pacient	k1gMnPc4	pacient
nakažené	nakažený	k2eAgFnSc6d1	nakažená
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
otevřel	otevřít	k5eAaPmAgMnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
Hermann	Hermann	k1gMnSc1	Hermann
Brehmer	Brehmer	k1gMnSc1	Brehmer
v	v	k7c6	v
Görbersdorfu	Görbersdorf	k1gInSc6	Görbersdorf
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc4d1	dnešní
Sokołowsko	Sokołowsko	k1gNnSc4	Sokołowsko
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Bacil	bacit	k5eAaPmAgMnS	bacit
způsobující	způsobující	k2eAgFnSc4d1	způsobující
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
,	,	kIx,	,
Mycobacterium	Mycobacterium	k1gNnSc4	Mycobacterium
tuberculosis	tuberculosis	k1gFnSc2	tuberculosis
<g/>
,	,	kIx,	,
objevil	objevit	k5eAaPmAgMnS	objevit
a	a	k8xC	a
popsal	popsat	k5eAaPmAgMnS	popsat
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1882	[number]	k4	1882
Robert	Robert	k1gMnSc1	Robert
Koch	Koch	k1gMnSc1	Koch
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
objev	objev	k1gInSc4	objev
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
získal	získat	k5eAaPmAgMnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
lékařství	lékařství	k1gNnSc4	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Koch	Koch	k1gMnSc1	Koch
nevěřil	věřit	k5eNaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dobytčí	dobytčí	k2eAgFnSc1d1	dobytčí
a	a	k8xC	a
lidská	lidský	k2eAgFnSc1d1	lidská
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
opozdilo	opozdit	k5eAaPmAgNnS	opozdit
objev	objev	k1gInSc4	objev
<g/>
,	,	kIx,	,
že	že	k8xS	že
zdrojem	zdroj	k1gInSc7	zdroj
infekce	infekce	k1gFnSc2	infekce
je	být	k5eAaImIp3nS	být
mléko	mléko	k1gNnSc1	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zdroj	zdroj	k1gInSc1	zdroj
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
odstraněn	odstranit	k5eAaPmNgInS	odstranit
pasterizací	pasterizace	k1gFnSc7	pasterizace
<g/>
.	.	kIx.	.
</s>
<s>
Koch	Koch	k1gMnSc1	Koch
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lékem	lék	k1gInSc7	lék
proti	proti	k7c3	proti
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
je	být	k5eAaImIp3nS	být
glycerolový	glycerolový	k2eAgInSc1d1	glycerolový
extrakt	extrakt	k1gInSc1	extrakt
bacilu	bacil	k1gInSc2	bacil
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
,	,	kIx,	,
a	a	k8xC	a
nazval	nazvat	k5eAaPmAgInS	nazvat
ho	on	k3xPp3gInSc4	on
"	"	kIx"	"
<g/>
tuberkulin	tuberkulin	k1gInSc4	tuberkulin
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
touto	tento	k3xDgFnSc7	tento
látkou	látka	k1gFnSc7	látka
byla	být	k5eAaImAgFnS	být
neúspěšná	úspěšný	k2eNgNnPc1d1	neúspěšné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
při	při	k7c6	při
diagnóze	diagnóza	k1gFnSc6	diagnóza
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
.	.	kIx.	.
<g/>
Prvního	první	k4xOgMnSc2	první
úspěchu	úspěch	k1gInSc3	úspěch
s	s	k7c7	s
očkováním	očkování	k1gNnSc7	očkování
proti	proti	k7c3	proti
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
Albert	Albert	k1gMnSc1	Albert
Calmette	Calmett	k1gInSc5	Calmett
a	a	k8xC	a
Camille	Camill	k1gMnPc4	Camill
Guérin	Guérin	k1gInSc4	Guérin
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
vynalezli	vynaleznout	k5eAaPmAgMnP	vynaleznout
bacil	bacit	k5eAaPmAgInS	bacit
Calmettův-Guérinův	Calmettův-Guérinův	k2eAgMnSc1d1	Calmettův-Guérinův
(	(	kIx(	(
<g/>
BCG	BCG	kA	BCG
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vakcína	vakcína	k1gFnSc1	vakcína
BCG	BCG	kA	BCG
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
lidech	člověk	k1gMnPc6	člověk
poprvé	poprvé	k6eAd1	poprvé
použita	použít	k5eAaPmNgNnP	použít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
byla	být	k5eAaImAgNnP	být
přijata	přijmout	k5eAaPmNgNnP	přijmout
až	až	k6eAd1	až
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
známa	známo	k1gNnSc2	známo
jako	jako	k8xS	jako
endemické	endemický	k2eAgNnSc4d1	endemické
onemocnění	onemocnění	k1gNnSc4	onemocnění
chudých	chudý	k2eAgMnPc2d1	chudý
venkovanů	venkovan	k1gMnPc2	venkovan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
zapříčinila	zapříčinit	k5eAaPmAgFnS	zapříčinit
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgNnPc2	čtyři
úmrtí	úmrtí	k1gNnPc2	úmrtí
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
příčinou	příčina	k1gFnSc7	příčina
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
šesti	šest	k4xCc3	šest
úmrtí	úmrtí	k1gNnPc2	úmrtí
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
způsobila	způsobit	k5eAaPmAgFnS	způsobit
celkově	celkově	k6eAd1	celkově
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
úmrtí	úmrtí	k1gNnPc2	úmrtí
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nakažlivá	nakažlivý	k2eAgFnSc1d1	nakažlivá
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
za	za	k7c4	za
onemocnění	onemocnění	k1gNnSc4	onemocnění
podléhající	podléhající	k2eAgNnSc1d1	podléhající
hlášení	hlášení	k1gNnSc1	hlášení
<g/>
;	;	kIx,	;
existovaly	existovat	k5eAaImAgFnP	existovat
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
zákaz	zákaz	k1gInSc4	zákaz
plivání	plivání	k1gNnSc2	plivání
na	na	k7c6	na
veřejných	veřejný	k2eAgNnPc6d1	veřejné
místech	místo	k1gNnPc6	místo
a	a	k8xC	a
infikovaní	infikovaný	k2eAgMnPc1d1	infikovaný
chudí	chudý	k2eAgMnPc1d1	chudý
lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
do	do	k7c2	do
sanatorií	sanatorium	k1gNnPc2	sanatorium
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
připomínala	připomínat	k5eAaImAgFnS	připomínat
vězení	vězení	k1gNnSc4	vězení
<g/>
;	;	kIx,	;
sanatoria	sanatorium	k1gNnPc4	sanatorium
pro	pro	k7c4	pro
střední	střední	k2eAgFnPc4d1	střední
a	a	k8xC	a
vyšší	vysoký	k2eAgFnPc4d2	vyšší
třídy	třída	k1gFnPc4	třída
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
nepřetržitou	přetržitý	k2eNgFnSc4d1	nepřetržitá
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
nejlepších	dobrý	k2eAgNnPc6d3	nejlepší
sanatoriích	sanatorium	k1gNnPc6	sanatorium
umíralo	umírat	k5eAaImAgNnS	umírat
50	[number]	k4	50
%	%	kIx~	%
nemocných	nemocný	k2eAgMnPc2d1	nemocný
do	do	k7c2	do
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
(	(	kIx(	(
<g/>
údaj	údaj	k1gInSc1	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
úmrtí	úmrtí	k1gNnSc2	úmrtí
v	v	k7c6	v
následku	následek	k1gInSc6	následek
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
klesla	klesnout	k5eAaPmAgFnS	klesnout
z	z	k7c2	z
500	[number]	k4	500
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
na	na	k7c4	na
50	[number]	k4	50
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Zlepšování	zlepšování	k1gNnSc1	zlepšování
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
zapříčinilo	zapříčinit	k5eAaPmAgNnS	zapříčinit
úbytek	úbytek	k1gInSc4	úbytek
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
zůstala	zůstat	k5eAaPmAgFnS	zůstat
tak	tak	k6eAd1	tak
závažným	závažný	k2eAgNnSc7d1	závažné
onemocněním	onemocnění	k1gNnSc7	onemocnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
založen	založen	k2eAgMnSc1d1	založen
Medical	Medical	k1gMnSc1	Medical
Research	Research	k1gMnSc1	Research
Council	Council	k1gMnSc1	Council
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
počátečním	počáteční	k2eAgNnSc7d1	počáteční
zaměřením	zaměření	k1gNnSc7	zaměření
byl	být	k5eAaImAgInS	být
výzkum	výzkum	k1gInSc1	výzkum
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
.	.	kIx.	.
<g/>
Účinná	účinný	k2eAgFnSc1d1	účinná
léčba	léčba	k1gFnSc1	léčba
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
přišla	přijít	k5eAaPmAgFnS	přijít
až	až	k9	až
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
streptomycinu	streptomycin	k1gInSc2	streptomycin
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1943	[number]	k4	1943
poprvé	poprvé	k6eAd1	poprvé
izoloval	izolovat	k5eAaBmAgMnS	izolovat
Albert	Albert	k1gMnSc1	Albert
Schatz	Schatz	k1gMnSc1	Schatz
<g/>
,	,	kIx,	,
student	student	k1gMnSc1	student
Selmana	Selman	k1gMnSc2	Selman
Abrahama	Abraham	k1gMnSc2	Abraham
Waksmana	Waksman	k1gMnSc2	Waksman
na	na	k7c4	na
Rutgers	Rutgers	k1gInSc4	Rutgers
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Waksman	Waksman	k1gMnSc1	Waksman
poté	poté	k6eAd1	poté
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
za	za	k7c4	za
objev	objev	k1gInSc4	objev
získal	získat	k5eAaPmAgMnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
medicínu	medicína	k1gFnSc4	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byly	být	k5eAaImAgInP	být
jediným	jediný	k2eAgInSc7d1	jediný
způsobem	způsob	k1gInSc7	způsob
léčby	léčba	k1gFnSc2	léčba
sanatoria	sanatorium	k1gNnSc2	sanatorium
a	a	k8xC	a
chirurgické	chirurgický	k2eAgInPc4d1	chirurgický
zákroky	zákrok	k1gInPc4	zákrok
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
bronchoskopie	bronchoskopie	k1gFnSc1	bronchoskopie
nebo	nebo	k8xC	nebo
iatrogenní	iatrogenní	k2eAgInSc1d1	iatrogenní
pneumotorax	pneumotorax	k1gInSc1	pneumotorax
<g/>
.	.	kIx.	.
</s>
<s>
Chirurgické	chirurgický	k2eAgInPc1d1	chirurgický
zákroky	zákrok	k1gInPc1	zákrok
se	se	k3xPyFc4	se
ale	ale	k9	ale
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
neefektivními	efektivní	k2eNgFnPc7d1	neefektivní
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
procesu	proces	k1gInSc2	proces
léčby	léčba	k1gFnSc2	léčba
byly	být	k5eAaImAgFnP	být
znovu	znovu	k6eAd1	znovu
zahrnuty	zahrnout	k5eAaPmNgFnP	zahrnout
až	až	k6eAd1	až
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
multirezistentní	multirezistentní	k2eAgFnSc2d1	multirezistentní
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
její	její	k3xOp3gFnSc6	její
léčbě	léčba	k1gFnSc6	léčba
se	se	k3xPyFc4	se
chirurgicky	chirurgicky	k6eAd1	chirurgicky
odstraňují	odstraňovat	k5eAaImIp3nP	odstraňovat
infikované	infikovaný	k2eAgFnPc4d1	infikovaná
tkáně	tkáň	k1gFnPc4	tkáň
a	a	k8xC	a
snižuje	snižovat	k5eAaImIp3nS	snižovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
počet	počet	k1gInSc1	počet
bakterií	bakterie	k1gFnPc2	bakterie
v	v	k7c6	v
plicích	plíce	k1gFnPc6	plíce
a	a	k8xC	a
zbývající	zbývající	k2eAgFnPc1d1	zbývající
bakterie	bakterie	k1gFnPc1	bakterie
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
vystaveny	vystavit	k5eAaPmNgInP	vystavit
lékům	lék	k1gInPc3	lék
kolujícím	kolující	k2eAgInPc3d1	kolující
v	v	k7c6	v
krevním	krevní	k2eAgInSc6d1	krevní
oběhu	oběh	k1gInSc6	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tím	ten	k3xDgInSc7	ten
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
účinnost	účinnost	k1gFnSc1	účinnost
chemoterapie	chemoterapie	k1gFnSc1	chemoterapie
<g/>
.	.	kIx.	.
<g/>
Naděje	naděje	k1gFnSc1	naděje
na	na	k7c4	na
úplné	úplný	k2eAgNnSc4d1	úplné
vymýcení	vymýcení	k1gNnSc4	vymýcení
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
byla	být	k5eAaImAgFnS	být
napadena	napadnout	k5eAaPmNgFnS	napadnout
příchodem	příchod	k1gInSc7	příchod
rezistentních	rezistentní	k2eAgInPc2d1	rezistentní
typů	typ	k1gInPc2	typ
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
případů	případ	k1gInPc2	případ
snížil	snížit	k5eAaPmAgInS	snížit
ze	z	k7c2	z
117	[number]	k4	117
000	[number]	k4	000
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
5	[number]	k4	5
000	[number]	k4	000
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pak	pak	k6eAd1	pak
zase	zase	k9	zase
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
na	na	k7c4	na
6300	[number]	k4	6300
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
a	a	k8xC	a
7600	[number]	k4	7600
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
<g/>
Návrat	návrat	k1gInSc1	návrat
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c4	v
oznámení	oznámení	k1gNnSc4	oznámení
Světové	světový	k2eAgFnSc2d1	světová
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
o	o	k7c6	o
nouzovém	nouzový	k2eAgInSc6d1	nouzový
stavu	stav	k1gInSc6	stav
ohrožení	ohrožení	k1gNnSc2	ohrožení
tímto	tento	k3xDgNnSc7	tento
onemocněním	onemocnění	k1gNnSc7	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
celosvětově	celosvětově	k6eAd1	celosvětově
přibývá	přibývat	k5eAaImIp3nS	přibývat
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
nových	nový	k2eAgMnPc2d1	nový
případů	případ	k1gInPc2	případ
onemocnění	onemocnění	k1gNnSc2	onemocnění
multirezistentní	multirezistentní	k2eAgFnSc7d1	multirezistentní
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
(	(	kIx(	(
<g/>
MDR-TB	MDR-TB	k1gFnSc7	MDR-TB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
objeven	objevit	k5eAaPmNgInS	objevit
nový	nový	k2eAgInSc1d1	nový
druh	druh	k1gInSc1	druh
bakterie	bakterie	k1gFnSc2	bakterie
řadící	řadící	k2eAgFnSc2d1	řadící
se	se	k3xPyFc4	se
do	do	k7c2	do
Mycobacterium	Mycobacterium	k1gNnSc4	Mycobacterium
tuberculosis	tuberculosis	k1gFnSc7	tuberculosis
komplexu	komplex	k1gInSc2	komplex
<g/>
,	,	kIx,	,
M.	M.	kA	M.
mungi	mung	k1gInPc7	mung
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původce	původce	k1gMnSc1	původce
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
původcem	původce	k1gMnSc7	původce
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
,	,	kIx,	,
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
tuberculosis	tuberculosis	k1gFnSc2	tuberculosis
(	(	kIx(	(
<g/>
MTB	MTB	kA	MTB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
aerobní	aerobní	k2eAgFnSc1d1	aerobní
tyčinkovitá	tyčinkovitý	k2eAgFnSc1d1	tyčinkovitá
bakterie	bakterie	k1gFnSc1	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
obsah	obsah	k1gInSc1	obsah
lipidů	lipid	k1gInPc2	lipid
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
jejích	její	k3xOp3gFnPc2	její
výjimečných	výjimečný	k2eAgFnPc2d1	výjimečná
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
každých	každý	k3xTgFnPc2	každý
16	[number]	k4	16
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
dělí	dělit	k5eAaImIp3nS	dělit
za	za	k7c4	za
méně	málo	k6eAd2	málo
než	než	k8xS	než
hodinu	hodina	k1gFnSc4	hodina
<g/>
,	,	kIx,	,
extrémně	extrémně	k6eAd1	extrémně
pomalé	pomalý	k2eAgNnSc1d1	pomalé
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
MTB	MTB	kA	MTB
má	mít	k5eAaImIp3nS	mít
buněčnou	buněčný	k2eAgFnSc4d1	buněčná
stěnu	stěna	k1gFnSc4	stěna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
dvojici	dvojice	k1gFnSc4	dvojice
vnější	vnější	k2eAgFnSc2d1	vnější
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
plazmatické	plazmatický	k2eAgFnSc2d1	plazmatická
membrány	membrána	k1gFnSc2	membrána
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
klasifikována	klasifikován	k2eAgFnSc1d1	klasifikována
jako	jako	k8xC	jako
grampozitivní	grampozitivní	k2eAgFnSc1d1	grampozitivní
bakterie	bakterie	k1gFnSc1	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Gramova	Gramův	k2eAgNnSc2d1	Gramovo
barvení	barvení	k1gNnSc2	barvení
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
vysokému	vysoký	k2eAgInSc3d1	vysoký
obsahu	obsah	k1gInSc3	obsah
lipidů	lipid	k1gInPc2	lipid
v	v	k7c6	v
buněčné	buněčný	k2eAgFnSc6d1	buněčná
stěně	stěna	k1gFnSc6	stěna
barví	barvit	k5eAaImIp3nS	barvit
buď	buď	k8xC	buď
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
nebo	nebo	k8xC	nebo
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
MTB	MTB	kA	MTB
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
odolat	odolat	k5eAaPmF	odolat
slabé	slabý	k2eAgFnSc3d1	slabá
dezinfekci	dezinfekce	k1gFnSc3	dezinfekce
a	a	k8xC	a
dokáže	dokázat	k5eAaPmIp3nS	dokázat
přežít	přežít	k5eAaPmF	přežít
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
v	v	k7c6	v
suchém	suchý	k2eAgInSc6d1	suchý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
může	moct	k5eAaImIp3nS	moct
růst	růst	k1gInSc4	růst
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
hostitele	hostitel	k1gMnSc2	hostitel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
pěstovat	pěstovat	k5eAaImF	pěstovat
in	in	k?	in
vitro	vitro	k6eAd1	vitro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pomocí	pomocí	k7c2	pomocí
barvících	barvící	k2eAgFnPc2d1	barvící
mikrobiologických	mikrobiologický	k2eAgFnPc2d1	mikrobiologická
metod	metoda	k1gFnPc2	metoda
lze	lze	k6eAd1	lze
M.	M.	kA	M.
tuberculosis	tuberculosis	k1gFnPc2	tuberculosis
detekovat	detekovat	k5eAaImF	detekovat
ve	v	k7c6	v
sputu	sputum	k1gNnSc6	sputum
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
používanou	používaný	k2eAgFnSc7d1	používaná
metodou	metoda	k1gFnSc7	metoda
je	být	k5eAaImIp3nS	být
Ziehl-Neelsenovo	Ziehl-Neelsenův	k2eAgNnSc1d1	Ziehl-Neelsenův
barvení	barvení	k1gNnSc1	barvení
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgNnSc6	který
se	se	k3xPyFc4	se
M.	M.	kA	M.
tuberculosis	tuberculosis	k1gFnSc7	tuberculosis
zbarví	zbarvit	k5eAaPmIp3nS	zbarvit
červeně	červeně	k6eAd1	červeně
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vizualizaci	vizualizace	k1gFnSc3	vizualizace
MTB	MTB	kA	MTB
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
i	i	k9	i
směs	směs	k1gFnSc4	směs
auramin-rhodamin	auraminhodamin	k1gInSc4	auramin-rhodamin
a	a	k8xC	a
fluorescenční	fluorescenční	k2eAgFnSc4d1	fluorescenční
mikroskopii	mikroskopie	k1gFnSc4	mikroskopie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
komplexu	komplex	k1gInSc2	komplex
M.	M.	kA	M.
tuberculosis	tuberculosis	k1gFnSc4	tuberculosis
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
geneticky	geneticky	k6eAd1	geneticky
blízkých	blízký	k2eAgFnPc2d1	blízká
mykobakterií	mykobakterie	k1gFnPc2	mykobakterie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
ještě	ještě	k9	ještě
M.	M.	kA	M.
bovis	bovis	k1gInSc1	bovis
<g/>
,	,	kIx,	,
M.	M.	kA	M.
africanum	africanum	k1gInSc1	africanum
<g/>
,	,	kIx,	,
M.	M.	kA	M.
canetti	canetť	k1gFnSc2	canetť
a	a	k8xC	a
M.	M.	kA	M.
microti	microt	k1gMnPc1	microt
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
africanum	africanum	k1gNnSc1	africanum
není	být	k5eNaImIp3nS	být
hojně	hojně	k6eAd1	hojně
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
Afriky	Afrika	k1gFnSc2	Afrika
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
důležité	důležitý	k2eAgMnPc4d1	důležitý
původce	původce	k1gMnPc4	původce
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
bovis	bovis	k1gFnSc1	bovis
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
běžnou	běžný	k2eAgFnSc7d1	běžná
příčinou	příčina	k1gFnSc7	příčina
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zavedením	zavedení	k1gNnSc7	zavedení
metody	metoda	k1gFnSc2	metoda
pasterizace	pasterizace	k1gFnSc2	pasterizace
mléka	mléko	k1gNnSc2	mléko
se	se	k3xPyFc4	se
infekce	infekce	k1gFnSc1	infekce
tímto	tento	k3xDgInSc7	tento
druhem	druh	k1gInSc7	druh
téměř	téměř	k6eAd1	téměř
eliminovala	eliminovat	k5eAaBmAgFnS	eliminovat
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
canetti	canetti	k1gNnSc1	canetti
je	být	k5eAaImIp3nS	být
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
a	a	k8xC	a
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
několik	několik	k4yIc1	několik
případů	případ	k1gInPc2	případ
výskytu	výskyt	k1gInSc2	výskyt
této	tento	k3xDgFnSc2	tento
bakterie	bakterie	k1gFnSc2	bakterie
u	u	k7c2	u
afrických	africký	k2eAgMnPc2d1	africký
emigrantů	emigrant	k1gMnPc2	emigrant
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
microti	microt	k1gMnPc1	microt
je	on	k3xPp3gFnPc4	on
nejčastěji	často	k6eAd3	často
pozorována	pozorován	k2eAgFnSc1d1	pozorována
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
poškozenou	poškozený	k2eAgFnSc7d1	poškozená
imunitou	imunita	k1gFnSc7	imunita
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
prevalence	prevalence	k1gFnSc1	prevalence
tohoto	tento	k3xDgInSc2	tento
patogenu	patogen	k1gInSc2	patogen
byla	být	k5eAaImAgFnS	být
podceněna	podcenit	k5eAaPmNgFnS	podcenit
<g/>
.	.	kIx.	.
<g/>
Dalšími	další	k2eAgMnPc7d1	další
známými	známý	k2eAgMnPc7d1	známý
zástupci	zástupce	k1gMnPc7	zástupce
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
jsou	být	k5eAaImIp3nP	být
M.	M.	kA	M.
leprae	leprae	k1gFnSc4	leprae
<g/>
,	,	kIx,	,
komplex	komplex	k1gInSc4	komplex
Mycobacterium	Mycobacterium	k1gNnSc4	Mycobacterium
avium	avium	k1gInSc1	avium
a	a	k8xC	a
M.	M.	kA	M.
kansasii	kansasie	k1gFnSc4	kansasie
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
dvě	dva	k4xCgFnPc1	dva
zmíněné	zmíněný	k2eAgFnPc1d1	zmíněná
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
netuberkulózních	tuberkulózní	k2eNgFnPc2d1	netuberkulózní
mykobakterií	mykobakterie	k1gFnPc2	mykobakterie
(	(	kIx(	(
<g/>
NTM	NTM	kA	NTM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
sice	sice	k8xC	sice
nezpůsobují	způsobovat	k5eNaImIp3nP	způsobovat
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
ani	ani	k8xC	ani
lepru	lepra	k1gFnSc4	lepra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
plicní	plicní	k2eAgNnPc4d1	plicní
onemocnění	onemocnění	k1gNnPc4	onemocnění
podobná	podobný	k2eAgFnSc1d1	podobná
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Epidemiologie	epidemiologie	k1gFnSc2	epidemiologie
==	==	k?	==
</s>
</p>
<p>
<s>
Bakterií	bakterie	k1gFnSc7	bakterie
M.	M.	kA	M.
tuberculosis	tuberculosis	k1gFnSc7	tuberculosis
je	být	k5eAaImIp3nS	být
nakažena	nakazit	k5eAaPmNgFnS	nakazit
zhruba	zhruba	k6eAd1	zhruba
třetina	třetina	k1gFnSc1	třetina
světové	světový	k2eAgFnSc2d1	světová
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
každou	každý	k3xTgFnSc4	každý
sekundu	sekunda	k1gFnSc4	sekunda
je	být	k5eAaImIp3nS	být
infikován	infikován	k2eAgMnSc1d1	infikován
nový	nový	k2eAgMnSc1d1	nový
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všechny	všechen	k3xTgFnPc4	všechen
tyto	tento	k3xDgFnPc4	tento
nákazy	nákaza	k1gFnPc4	nákaza
ale	ale	k8xC	ale
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
jsou	být	k5eAaImIp3nP	být
asymptomatické	asymptomatický	k2eAgInPc1d1	asymptomatický
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
zhruba	zhruba	k6eAd1	zhruba
13,7	[number]	k4	13,7
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
aktivní	aktivní	k2eAgFnSc7d1	aktivní
formou	forma	k1gFnSc7	forma
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
9,3	[number]	k4	9,3
milionů	milion	k4xCgInPc2	milion
bylo	být	k5eAaImAgNnS	být
nových	nový	k2eAgInPc2d1	nový
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
1,8	[number]	k4	1,8
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgFnSc1d1	roční
incidence	incidence	k1gFnSc1	incidence
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
mezi	mezi	k7c7	mezi
363	[number]	k4	363
případy	případ	k1gInPc7	případ
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
32	[number]	k4	32
případy	případ	k1gInPc7	případ
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
z	z	k7c2	z
infekčních	infekční	k2eAgNnPc2d1	infekční
onemocnění	onemocnění	k1gNnPc2	onemocnění
nejvíce	hodně	k6eAd3	hodně
úmrtí	úmrtí	k1gNnSc4	úmrtí
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
reproduktivním	reproduktivní	k2eAgInSc6d1	reproduktivní
věku	věk	k1gInSc6	věk
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavním	hlavní	k2eAgMnSc7d1	hlavní
příčin	příčina	k1gFnPc2	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
HIV	HIV	kA	HIV
<g/>
/	/	kIx~	/
<g/>
AIDS	AIDS	kA	AIDS
<g/>
.	.	kIx.	.
<g/>
Zvyšování	zvyšování	k1gNnSc1	zvyšování
počtu	počet	k1gInSc2	počet
nákaz	nákaza	k1gFnPc2	nákaza
virem	vir	k1gInSc7	vir
HIV	HIV	kA	HIV
a	a	k8xC	a
zanedbávání	zanedbávání	k1gNnSc4	zanedbávání
programů	program	k1gInPc2	program
na	na	k7c4	na
kontrolu	kontrola	k1gFnSc4	kontrola
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
vlně	vlna	k1gFnSc3	vlna
tohoto	tento	k3xDgNnSc2	tento
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
nástup	nástup	k1gInSc1	nástup
rezistentních	rezistentní	k2eAgInPc2d1	rezistentní
typů	typ	k1gInPc2	typ
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2000	[number]	k4	2000
a	a	k8xC	a
2004	[number]	k4	2004
bylo	být	k5eAaImAgNnS	být
20	[number]	k4	20
%	%	kIx~	%
případů	případ	k1gInPc2	případ
rezistentních	rezistentní	k2eAgInPc2d1	rezistentní
proti	proti	k7c3	proti
prvořadým	prvořadý	k2eAgNnPc3d1	prvořadé
antibiotikům	antibiotikum	k1gNnPc3	antibiotikum
a	a	k8xC	a
2	[number]	k4	2
%	%	kIx~	%
i	i	k8xC	i
proti	proti	k7c3	proti
druhořadým	druhořadý	k2eAgFnPc3d1	druhořadá
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
výskytu	výskyt	k1gInSc2	výskyt
nových	nový	k2eAgNnPc2d1	nové
onemocnění	onemocnění	k1gNnPc2	onemocnění
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
u	u	k7c2	u
sousedních	sousední	k2eAgInPc2d1	sousední
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
nejspíše	nejspíše	k9	nejspíše
kvůli	kvůli	k7c3	kvůli
rozdílům	rozdíl	k1gInPc3	rozdíl
ve	v	k7c6	v
zdravotních	zdravotní	k2eAgInPc6d1	zdravotní
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
státem	stát	k1gInSc7	stát
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
incidencí	incidence	k1gFnSc7	incidence
Svazijsko	Svazijsko	k1gNnSc1	Svazijsko
s	s	k7c7	s
1200	[number]	k4	1200
případy	případ	k1gInPc7	případ
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
měla	mít	k5eAaImAgFnS	mít
nejvíce	hodně	k6eAd3	hodně
nově	nově	k6eAd1	nově
nahlášených	nahlášený	k2eAgInPc2d1	nahlášený
případů	případ	k1gInPc2	případ
–	–	k?	–
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
méně	málo	k6eAd2	málo
častá	častý	k2eAgFnSc1d1	častá
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
činil	činit	k5eAaImAgInS	činit
národní	národní	k2eAgInSc1d1	národní
průměr	průměr	k1gInSc1	průměr
15	[number]	k4	15
nových	nový	k2eAgInPc2d1	nový
případů	případ	k1gInPc2	případ
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
největší	veliký	k2eAgFnSc1d3	veliký
míra	míra	k1gFnSc1	míra
incidence	incidence	k1gFnSc2	incidence
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
byla	být	k5eAaImAgFnS	být
30	[number]	k4	30
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
000	[number]	k4	000
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
a	a	k8xC	a
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
to	ten	k3xDgNnSc1	ten
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
98	[number]	k4	98
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
48	[number]	k4	48
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
000	[number]	k4	000
a	a	k8xC	a
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
se	se	k3xPyFc4	se
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
venkovských	venkovský	k2eAgFnPc6d1	venkovská
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
činila	činit	k5eAaImAgFnS	činit
incidence	incidence	k1gFnSc1	incidence
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
000	[number]	k4	000
s	s	k7c7	s
celkovým	celkový	k2eAgInSc7d1	celkový
počtem	počet	k1gInSc7	počet
893	[number]	k4	893
nakažených	nakažený	k2eAgMnPc2d1	nakažený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Incidence	incidence	k1gFnSc1	incidence
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
postihuje	postihovat	k5eAaImIp3nS	postihovat
převážně	převážně	k6eAd1	převážně
adolescenty	adolescent	k1gMnPc4	adolescent
a	a	k8xC	a
mladé	mladý	k2eAgMnPc4d1	mladý
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
incidence	incidence	k1gFnSc1	incidence
relativně	relativně	k6eAd1	relativně
rychle	rychle	k6eAd1	rychle
prudce	prudko	k6eAd1	prudko
snížila	snížit	k5eAaPmAgFnS	snížit
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
postihuje	postihovat	k5eAaImIp3nS	postihovat
spíše	spíše	k9	spíše
staré	starý	k2eAgMnPc4d1	starý
lidi	člověk	k1gMnPc4	člověk
nebo	nebo	k8xC	nebo
lidi	člověk	k1gMnPc4	člověk
se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
imunity	imunita	k1gFnSc2	imunita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rizikové	rizikový	k2eAgInPc1d1	rizikový
faktory	faktor	k1gInPc1	faktor
===	===	k?	===
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
známých	známý	k2eAgInPc2d1	známý
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
vnímavost	vnímavost	k1gFnSc4	vnímavost
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
:	:	kIx,	:
celosvětově	celosvětově	k6eAd1	celosvětově
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
HIV	HIV	kA	HIV
<g/>
.	.	kIx.	.
</s>
<s>
Koinfekce	Koinfekce	k1gFnSc1	Koinfekce
virem	vir	k1gInSc7	vir
HIV	HIV	kA	HIV
je	být	k5eAaImIp3nS	být
problémem	problém	k1gInSc7	problém
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nákaza	nákaza	k1gFnSc1	nákaza
tímto	tento	k3xDgInSc7	tento
virem	vir	k1gInSc7	vir
velmi	velmi	k6eAd1	velmi
častá	častý	k2eAgFnSc1d1	častá
<g/>
.	.	kIx.	.
</s>
<s>
Kouření	kouření	k1gNnSc1	kouření
20	[number]	k4	20
a	a	k8xC	a
více	hodně	k6eAd2	hodně
cigaret	cigareta	k1gFnPc2	cigareta
denně	denně	k6eAd1	denně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
nákazy	nákaza	k1gFnSc2	nákaza
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
dvakrát	dvakrát	k6eAd1	dvakrát
až	až	k6eAd1	až
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
velikost	velikost	k1gFnSc4	velikost
rizika	riziko	k1gNnSc2	riziko
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
vliv	vliv	k1gInSc4	vliv
i	i	k9	i
strava	strava	k1gFnSc1	strava
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
mezi	mezi	k7c7	mezi
indickými	indický	k2eAgMnPc7d1	indický
imigranty	imigrant	k1gMnPc7	imigrant
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
mají	mít	k5eAaImIp3nP	mít
vegetariánští	vegetariánský	k2eAgMnPc1d1	vegetariánský
hinduisté	hinduista	k1gMnPc1	hinduista
zhruba	zhruba	k6eAd1	zhruba
8,5	[number]	k4	8,5
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc4d2	veliký
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
než	než	k8xS	než
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
denně	denně	k6eAd1	denně
jedí	jíst	k5eAaImIp3nP	jíst
maso	maso	k1gNnSc4	maso
a	a	k8xC	a
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
důkaz	důkaz	k1gInSc1	důkaz
příčinné	příčinný	k2eAgFnSc2d1	příčinná
souvislosti	souvislost	k1gFnSc2	souvislost
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
toto	tento	k3xDgNnSc4	tento
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
riziko	riziko	k1gNnSc4	riziko
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
nedostatkem	nedostatek	k1gInSc7	nedostatek
stopových	stopová	k1gFnPc2	stopová
prvků	prvek	k1gInPc2	prvek
<g/>
:	:	kIx,	:
nejspíše	nejspíše	k9	nejspíše
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
vitamínu	vitamín	k1gInSc2	vitamín
B12	B12	k1gFnSc2	B12
nebo	nebo	k8xC	nebo
vitamínu	vitamín	k1gInSc2	vitamín
D.	D.	kA	D.
Další	další	k2eAgInPc1d1	další
výzkumy	výzkum	k1gInPc1	výzkum
poskytly	poskytnout	k5eAaPmAgInP	poskytnout
více	hodně	k6eAd2	hodně
důkazů	důkaz	k1gInPc2	důkaz
o	o	k7c6	o
spojitosti	spojitost	k1gFnSc6	spojitost
nedostatku	nedostatek	k1gInSc2	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
D	D	kA	D
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgNnSc7d1	zvýšené
rizikem	riziko	k1gNnSc7	riziko
onemocnění	onemocnění	k1gNnPc2	onemocnění
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
velký	velký	k2eAgInSc1d1	velký
nárůst	nárůst	k1gInSc1	nárůst
rizika	riziko	k1gNnSc2	riziko
rozvoje	rozvoj	k1gInSc2	rozvoj
aktivní	aktivní	k2eAgFnSc2d1	aktivní
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
malnutrice	malnutrika	k1gFnSc3	malnutrika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přelidněním	přelidnění	k1gNnSc7	přelidnění
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
malnutrice	malnutrika	k1gFnSc3	malnutrika
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
příčin	příčina	k1gFnPc2	příčina
silné	silný	k2eAgFnSc2d1	silná
spojitosti	spojitost	k1gFnSc2	spojitost
mezi	mezi	k7c7	mezi
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
a	a	k8xC	a
chudobou	chudoba	k1gFnSc7	chudoba
<g/>
.	.	kIx.	.
<g/>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
silikózou	silikóza	k1gFnSc7	silikóza
mají	mít	k5eAaImIp3nP	mít
zhruba	zhruba	k6eAd1	zhruba
30	[number]	k4	30
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc4d2	veliký
šanci	šance	k1gFnSc4	šance
získat	získat	k5eAaPmF	získat
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnSc1	částice
oxidu	oxid	k1gInSc2	oxid
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
dráždí	dráždit	k5eAaImIp3nS	dráždit
dýchací	dýchací	k2eAgFnSc1d1	dýchací
soustavu	soustava	k1gFnSc4	soustava
a	a	k8xC	a
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
imunogenní	imunogenní	k2eAgFnPc4d1	imunogenní
reakce	reakce	k1gFnPc4	reakce
jako	jako	k8xS	jako
např.	např.	kA	např.
fagocytózu	fagocytóza	k1gFnSc4	fagocytóza
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zase	zase	k9	zase
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
zanesení	zanesení	k1gNnSc4	zanesení
mízních	mízní	k2eAgFnPc2d1	mízní
cév	céva	k1gFnPc2	céva
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
zhoršení	zhoršení	k1gNnSc1	zhoršení
funkce	funkce	k1gFnSc2	funkce
makrofágů	makrofág	k1gInPc2	makrofág
způsobené	způsobený	k2eAgFnPc1d1	způsobená
částicemi	částice	k1gFnPc7	částice
oxidu	oxid	k1gInSc2	oxid
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
rozvoje	rozvoj	k1gInSc2	rozvoj
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
chronickým	chronický	k2eAgNnSc7d1	chronické
selháním	selhání	k1gNnSc7	selhání
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
hemodialýzou	hemodialýza	k1gFnSc7	hemodialýza
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
riziko	riziko	k1gNnSc4	riziko
rozvoje	rozvoj	k1gInSc2	rozvoj
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
.	.	kIx.	.
<g/>
Lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
cukrovkou	cukrovka	k1gFnSc7	cukrovka
mají	mít	k5eAaImIp3nP	mít
dvakrát	dvakrát	k6eAd1	dvakrát
až	až	k9	až
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
větší	veliký	k2eAgNnSc4d2	veliký
riziko	riziko	k1gNnSc4	riziko
rozvoje	rozvoj	k1gInSc2	rozvoj
aktivní	aktivní	k2eAgFnSc2d1	aktivní
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
než	než	k8xS	než
lidé	člověk	k1gMnPc1	člověk
bez	bez	k7c2	bez
tohoto	tento	k3xDgNnSc2	tento
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
a	a	k8xC	a
toto	tento	k3xDgNnSc4	tento
riziko	riziko	k1gNnSc4	riziko
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgMnSc1d2	vyšší
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc4	mellitus
I.	I.	kA	I.
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
klinické	klinický	k2eAgInPc4d1	klinický
stavy	stav	k1gInPc4	stav
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
spojovány	spojovat	k5eAaImNgInP	spojovat
s	s	k7c7	s
aktivní	aktivní	k2eAgFnSc7d1	aktivní
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
gastrektomie	gastrektomie	k1gFnSc1	gastrektomie
<g/>
,	,	kIx,	,
transplantace	transplantace	k1gFnSc1	transplantace
ledvin	ledvina	k1gFnPc2	ledvina
a	a	k8xC	a
srdce	srdce	k1gNnSc1	srdce
<g/>
,	,	kIx,	,
karcinom	karcinom	k1gInSc1	karcinom
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
krku	krk	k1gInSc2	krk
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
nádory	nádor	k1gInPc4	nádor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
karcinom	karcinom	k1gInSc4	karcinom
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
lymfomy	lymfom	k1gInPc1	lymfom
nebo	nebo	k8xC	nebo
leukemie	leukemie	k1gFnPc1	leukemie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Se	s	k7c7	s
zvýšeným	zvýšený	k2eAgNnSc7d1	zvýšené
rizikem	riziko	k1gNnSc7	riziko
rozvoje	rozvoj	k1gInSc2	rozvoj
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
je	být	k5eAaImIp3nS	být
také	také	k9	také
spojena	spojen	k2eAgFnSc1d1	spojena
nízká	nízký	k2eAgFnSc1d1	nízká
váha	váha	k1gFnSc1	váha
<g/>
.	.	kIx.	.
</s>
<s>
Index	index	k1gInSc1	index
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
(	(	kIx(	(
<g/>
BMI	BMI	kA	BMI
<g/>
)	)	kIx)	)
menší	malý	k2eAgFnSc4d2	menší
než	než	k8xS	než
18,5	[number]	k4	18,5
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
2	[number]	k4	2
<g/>
krát	krát	k6eAd1	krát
až	až	k9	až
3	[number]	k4	3
<g/>
krát	krát	k6eAd1	krát
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšení	zvýšení	k1gNnSc1	zvýšení
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
riziko	riziko	k1gNnSc4	riziko
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gMnSc1	mellitus
mají	mít	k5eAaImIp3nP	mít
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
riziko	riziko	k1gNnSc4	riziko
rozvoje	rozvoj	k1gInSc2	rozvoj
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
a	a	k8xC	a
hůře	zle	k6eAd2	zle
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
zhoršeným	zhoršený	k2eAgNnSc7d1	zhoršené
vstřebáváním	vstřebávání	k1gNnSc7	vstřebávání
léků	lék	k1gInPc2	lék
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
diabetici	diabetik	k1gMnPc1	diabetik
měli	mít	k5eAaImAgMnP	mít
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
riziko	riziko	k1gNnSc4	riziko
zisku	zisk	k1gInSc2	zisk
latentního	latentní	k2eAgNnSc2d1	latentní
stádia	stádium	k1gNnSc2	stádium
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
vyšší	vysoký	k2eAgFnSc1d2	vyšší
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
přechodu	přechod	k1gInSc2	přechod
z	z	k7c2	z
latentního	latentní	k2eAgMnSc2d1	latentní
do	do	k7c2	do
aktivního	aktivní	k2eAgNnSc2d1	aktivní
stádia	stádium	k1gNnSc2	stádium
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
faktory	faktor	k1gInPc4	faktor
zvyšující	zvyšující	k2eAgNnSc4d1	zvyšující
riziko	riziko	k1gNnSc4	riziko
rozvoje	rozvoj	k1gInSc2	rozvoj
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
sdílení	sdílení	k1gNnSc1	sdílení
injekcí	injekce	k1gFnSc7	injekce
uživateli	uživatel	k1gMnSc3	uživatel
drog	droga	k1gFnPc2	droga
<g/>
;	;	kIx,	;
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
kortikosteroidní	kortikosteroidní	k2eAgFnSc1d1	kortikosteroidní
<g />
.	.	kIx.	.
</s>
<s>
nebo	nebo	k8xC	nebo
jiná	jiný	k2eAgFnSc1d1	jiná
imunosupresivní	imunosupresivní	k2eAgFnSc1d1	imunosupresivní
terapie	terapie	k1gFnSc1	terapie
<g/>
;	;	kIx,	;
selhání	selhání	k1gNnSc1	selhání
imunity	imunita	k1gFnSc2	imunita
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
AIDS	AIDS	kA	AIDS
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Hodgkinova	Hodgkinův	k2eAgFnSc1d1	Hodgkinova
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
;	;	kIx,	;
střevní	střevní	k2eAgInSc1d1	střevní
bypass	bypass	k1gInSc1	bypass
<g/>
;	;	kIx,	;
chronická	chronický	k2eAgFnSc1d1	chronická
porucha	porucha	k1gFnSc1	porucha
vstřebávání	vstřebávání	k1gNnSc2	vstřebávání
<g/>
;	;	kIx,	;
nedostatek	nedostatek	k1gInSc1	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
D	D	kA	D
<g/>
;	;	kIx,	;
malnutrice	malnutrika	k1gFnSc3	malnutrika
<g/>
;	;	kIx,	;
a	a	k8xC	a
alkoholismus	alkoholismus	k1gInSc1	alkoholismus
<g/>
.	.	kIx.	.
<g/>
Výzkum	výzkum	k1gInSc1	výzkum
dvojčat	dvojče	k1gNnPc2	dvojče
ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
naznačil	naznačit	k5eAaPmAgMnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnímavost	vnímavost	k1gFnSc1	vnímavost
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
je	být	k5eAaImIp3nS	být
dědičná	dědičný	k2eAgFnSc1d1	dědičná
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
dvojčat	dvojče	k1gNnPc2	dvojče
získalo	získat	k5eAaPmAgNnS	získat
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgFnSc6	druhý
mělo	mít	k5eAaImAgNnS	mít
vyšší	vysoký	k2eAgFnSc4d2	vyšší
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
získat	získat	k5eAaPmF	získat
toto	tento	k3xDgNnSc4	tento
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
byla	být	k5eAaImAgNnP	být
dvojčata	dvojče	k1gNnPc1	dvojče
jednovaječná	jednovaječný	k2eAgNnPc1d1	jednovaječné
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zjištění	zjištění	k1gNnSc3	zjištění
byla	být	k5eAaImAgFnS	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
novějšími	nový	k2eAgInPc7d2	novější
výzkumy	výzkum	k1gInPc7	výzkum
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vnímavostí	vnímavost	k1gFnSc7	vnímavost
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
je	být	k5eAaImIp3nS	být
spojen	spojen	k2eAgInSc1d1	spojen
polymorfismus	polymorfismus	k1gInSc1	polymorfismus
genu	gen	k1gInSc2	gen
IL	IL	kA	IL
<g/>
12	[number]	k4	12
<g/>
B.	B.	kA	B.
<g/>
Některé	některý	k3yIgInPc4	některý
léky	lék	k1gInPc4	lék
<g/>
,	,	kIx,	,
např.	např.	kA	např.
léky	lék	k1gInPc4	lék
proti	proti	k7c3	proti
revmatoidní	revmatoidní	k2eAgFnSc3d1	revmatoidní
artritidě	artritida	k1gFnSc3	artritida
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
blokují	blokovat	k5eAaImIp3nP	blokovat
funkci	funkce	k1gFnSc4	funkce
bílkoviny	bílkovina	k1gFnSc2	bílkovina
TNF-α	TNF-α	k1gFnSc2	TNF-α
(	(	kIx(	(
<g/>
cytokinu	cytokin	k1gInSc2	cytokin
vyvolávajícího	vyvolávající	k2eAgInSc2d1	vyvolávající
záněty	zánět	k1gInPc4	zánět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
riziko	riziko	k1gNnSc4	riziko
aktivace	aktivace	k1gFnSc2	aktivace
latentní	latentní	k2eAgFnSc2d1	latentní
infekce	infekce	k1gFnSc2	infekce
kvůli	kvůli	k7c3	kvůli
důležitosti	důležitost	k1gFnSc3	důležitost
tohoto	tento	k3xDgNnSc2	tento
cytokinu	cytokina	k1gFnSc4	cytokina
při	při	k7c6	při
imunitní	imunitní	k2eAgFnSc6d1	imunitní
obraně	obrana	k1gFnSc6	obrana
proti	proti	k7c3	proti
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Patogeneze	patogeneze	k1gFnSc2	patogeneze
==	==	k?	==
</s>
</p>
<p>
<s>
Když	když	k8xS	když
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
aktivní	aktivní	k2eAgFnSc7d1	aktivní
plicní	plicní	k2eAgFnSc7d1	plicní
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
vykašlávají	vykašlávat	k5eAaImIp3nP	vykašlávat
<g/>
,	,	kIx,	,
kýchají	kýchat	k5eAaImIp3nP	kýchat
<g/>
,	,	kIx,	,
mluví	mluvit	k5eAaImIp3nP	mluvit
nebo	nebo	k8xC	nebo
plivají	plivat	k5eAaImIp3nP	plivat
<g/>
,	,	kIx,	,
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
nakažlivé	nakažlivý	k2eAgFnPc4d1	nakažlivá
kapičky	kapička	k1gFnPc4	kapička
aerosolu	aerosol	k1gInSc2	aerosol
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
0,5	[number]	k4	0,5
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
μ	μ	k?	μ
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
kýchnutím	kýchnutí	k1gNnSc7	kýchnutí
mohou	moct	k5eAaImIp3nP	moct
vyloučit	vyloučit	k5eAaPmF	vyloučit
až	až	k9	až
40	[number]	k4	40
000	[number]	k4	000
kapiček	kapička	k1gFnPc2	kapička
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
kapiček	kapička	k1gFnPc2	kapička
může	moct	k5eAaImIp3nS	moct
přenést	přenést	k5eAaPmF	přenést
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
infekční	infekční	k2eAgFnSc1d1	infekční
dávka	dávka	k1gFnSc1	dávka
tohoto	tento	k3xDgNnSc2	tento
onemocnění	onemocnění	k1gNnSc2	onemocnění
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
a	a	k8xC	a
infekci	infekce	k1gFnSc3	infekce
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
i	i	k9	i
vdechnutí	vdechnutí	k1gNnSc4	vdechnutí
méně	málo	k6eAd2	málo
než	než	k8xS	než
deseti	deset	k4xCc2	deset
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
<g/>
Míra	Míra	k1gFnSc1	Míra
infekčnosti	infekčnost	k1gFnSc2	infekčnost
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
dlouhodobým	dlouhodobý	k2eAgInSc7d1	dlouhodobý
<g/>
,	,	kIx,	,
častým	častý	k2eAgInSc7d1	častý
a	a	k8xC	a
intenzivním	intenzivní	k2eAgInSc7d1	intenzivní
kontaktem	kontakt	k1gInSc7	kontakt
s	s	k7c7	s
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
činí	činit	k5eAaImIp3nS	činit
zhruba	zhruba	k6eAd1	zhruba
22	[number]	k4	22
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
s	s	k7c7	s
aktivní	aktivní	k2eAgFnSc7d1	aktivní
a	a	k8xC	a
neléčenou	léčený	k2eNgFnSc7d1	neléčená
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
průměrně	průměrně	k6eAd1	průměrně
nakazí	nakazit	k5eAaPmIp3nS	nakazit
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
lidí	člověk	k1gMnPc2	člověk
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
rizikovými	rizikový	k2eAgFnPc7d1	riziková
skupinami	skupina	k1gFnPc7	skupina
lidí	člověk	k1gMnPc2	člověk
jsou	být	k5eAaImIp3nP	být
uživatelé	uživatel	k1gMnPc1	uživatel
drog	droga	k1gFnPc2	droga
injekčními	injekční	k2eAgFnPc7d1	injekční
stříkačkami	stříkačka	k1gFnPc7	stříkačka
<g/>
,	,	kIx,	,
obyvatelé	obyvatel	k1gMnPc1	obyvatel
hojně	hojně	k6eAd1	hojně
obydlených	obydlený	k2eAgFnPc2d1	obydlená
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
chudé	chudý	k2eAgFnPc4d1	chudá
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
,	,	kIx,	,
vysokorizikové	vysokorizikový	k2eAgFnPc4d1	vysokorizikový
rasové	rasový	k2eAgFnPc4d1	rasová
nebo	nebo	k8xC	nebo
etnické	etnický	k2eAgFnPc4d1	etnická
menšiny	menšina	k1gFnPc4	menšina
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
ztrátou	ztráta	k1gFnSc7	ztráta
imunity	imunita	k1gFnPc1	imunita
<g/>
,	,	kIx,	,
uživatelé	uživatel	k1gMnPc1	uživatel
imunosupresivních	imunosupresivní	k2eAgInPc2d1	imunosupresivní
léků	lék	k1gInPc2	lék
a	a	k8xC	a
zdravotní	zdravotní	k2eAgMnPc1d1	zdravotní
pracovníci	pracovník	k1gMnPc1	pracovník
pracující	pracující	k2eAgMnPc1d1	pracující
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
rizikovými	rizikový	k2eAgFnPc7d1	riziková
skupinami	skupina	k1gFnPc7	skupina
<g/>
.	.	kIx.	.
<g/>
Přenášet	přenášet	k5eAaImF	přenášet
onemocnění	onemocnění	k1gNnSc4	onemocnění
mohou	moct	k5eAaImIp3nP	moct
pouze	pouze	k6eAd1	pouze
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
aktivní	aktivní	k2eAgFnSc7d1	aktivní
–	–	k?	–
nelatentní	latentní	k2eNgInSc1d1	latentní
–	–	k?	–
formou	forma	k1gFnSc7	forma
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
přenosu	přenos	k1gInSc2	přenos
z	z	k7c2	z
osoby	osoba	k1gFnSc2	osoba
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
počtu	počet	k1gInSc6	počet
infikovaných	infikovaný	k2eAgFnPc2d1	infikovaná
kapiček	kapička	k1gFnPc2	kapička
vyloučených	vyloučený	k2eAgFnPc2d1	vyloučená
nakaženým	nakažený	k2eAgMnSc7d1	nakažený
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
efektivitě	efektivita	k1gFnSc3	efektivita
ventilace	ventilace	k1gFnSc2	ventilace
<g/>
,	,	kIx,	,
době	doba	k1gFnSc6	doba
vystavení	vystavení	k1gNnSc2	vystavení
a	a	k8xC	a
virulenci	virulence	k1gFnSc4	virulence
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
varianty	varianta	k1gFnSc2	varianta
M.	M.	kA	M.
tuberculosis	tuberculosis	k1gFnSc1	tuberculosis
<g/>
.	.	kIx.	.
<g/>
Řetězec	řetězec	k1gInSc1	řetězec
přenosu	přenos	k1gInSc2	přenos
lze	lze	k6eAd1	lze
přerušit	přerušit	k5eAaPmF	přerušit
izolací	izolace	k1gFnSc7	izolace
lidí	člověk	k1gMnPc2	člověk
nakažených	nakažený	k2eAgFnPc2d1	nakažená
aktivní	aktivní	k2eAgFnSc7d1	aktivní
formou	forma	k1gFnSc7	forma
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
a	a	k8xC	a
zahájením	zahájení	k1gNnSc7	zahájení
protituberkulózní	protituberkulózní	k2eAgFnSc2d1	protituberkulózní
terapie	terapie	k1gFnSc2	terapie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
týdnech	týden	k1gInPc6	týden
léčení	léčený	k2eAgMnPc1d1	léčený
většinou	většinou	k6eAd1	většinou
lidé	člověk	k1gMnPc1	člověk
nakažení	nakažení	k1gNnSc2	nakažení
nerezistentním	rezistentní	k2eNgInSc7d1	rezistentní
typem	typ	k1gInSc7	typ
aktivní	aktivní	k2eAgFnSc2d1	aktivní
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
přestávají	přestávat	k5eAaImIp3nP	přestávat
být	být	k5eAaImF	být
nakažlivými	nakažlivý	k2eAgFnPc7d1	nakažlivá
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nakažení	nakažení	k1gNnSc6	nakažení
člověka	člověk	k1gMnSc2	člověk
trvá	trvat	k5eAaImIp3nS	trvat
tři	tři	k4xCgInPc4	tři
až	až	k6eAd1	až
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
onemocnění	onemocnění	k1gNnSc1	onemocnění
může	moct	k5eAaImIp3nS	moct
přenést	přenést	k5eAaPmF	přenést
na	na	k7c4	na
další	další	k2eAgMnPc4d1	další
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
<g/>
Zhruba	zhruba	k6eAd1	zhruba
90	[number]	k4	90
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
nakažených	nakažený	k2eAgFnPc2d1	nakažená
bakterií	bakterie	k1gFnPc2	bakterie
Mycobacterium	Mycobacterium	k1gNnSc4	Mycobacterium
tuberculosis	tuberculosis	k1gFnSc1	tuberculosis
má	mít	k5eAaImIp3nS	mít
asymptomatickou	asymptomatický	k2eAgFnSc4d1	asymptomatická
<g/>
,	,	kIx,	,
latentní	latentní	k2eAgFnSc4d1	latentní
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
aktivní	aktivní	k2eAgFnSc2d1	aktivní
formy	forma	k1gFnSc2	forma
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bez	bez	k7c2	bez
léčby	léčba	k1gFnSc2	léčba
je	být	k5eAaImIp3nS	být
smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Infekce	infekce	k1gFnSc1	infekce
začíná	začínat	k5eAaImIp3nS	začínat
<g/>
,	,	kIx,	,
když	když	k8xS	když
mykobakterie	mykobakterie	k1gFnPc1	mykobakterie
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
plicních	plicní	k2eAgInPc2d1	plicní
sklípků	sklípek	k1gInPc2	sklípek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vniknou	vniknout	k5eAaPmIp3nP	vniknout
do	do	k7c2	do
endozomů	endozom	k1gInPc2	endozom
alveolárních	alveolární	k2eAgInPc2d1	alveolární
makrofágů	makrofág	k1gInPc2	makrofág
a	a	k8xC	a
rozmnoží	rozmnožit	k5eAaPmIp3nP	rozmnožit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
místo	místo	k1gNnSc1	místo
infekce	infekce	k1gFnSc2	infekce
v	v	k7c6	v
plicích	plíce	k1gFnPc6	plíce
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Ghonův	Ghonův	k2eAgInSc1d1	Ghonův
komplex	komplex	k1gInSc1	komplex
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
vrchní	vrchní	k2eAgFnSc6d1	vrchní
části	část	k1gFnSc6	část
spodního	spodní	k2eAgInSc2d1	spodní
laloku	lalok	k1gInSc2	lalok
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
vrchního	vrchní	k2eAgInSc2d1	vrchní
laloku	lalok	k1gInSc2	lalok
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
pohltí	pohltit	k5eAaPmIp3nP	pohltit
dendritické	dendritický	k2eAgFnPc1d1	dendritická
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
bakteriím	bakterium	k1gNnPc3	bakterium
znemožní	znemožnit	k5eAaPmIp3nS	znemožnit
replikaci	replikace	k1gFnSc4	replikace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roznesou	roznést	k5eAaPmIp3nP	roznést
je	on	k3xPp3gFnPc4	on
do	do	k7c2	do
mízních	mízní	k2eAgFnPc2d1	mízní
uzlin	uzlina	k1gFnPc2	uzlina
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
bakterie	bakterie	k1gFnPc1	bakterie
roznášeny	roznášen	k2eAgFnPc1d1	roznášena
přes	přes	k7c4	přes
krevní	krevní	k2eAgInSc4d1	krevní
oběh	oběh	k1gInSc4	oběh
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
druhotná	druhotný	k2eAgNnPc1d1	druhotné
poškození	poškození	k1gNnPc1	poškození
v	v	k7c6	v
plicích	plíce	k1gFnPc6	plíce
<g/>
,	,	kIx,	,
okrajových	okrajový	k2eAgFnPc6d1	okrajová
mízních	mízní	k2eAgFnPc6d1	mízní
uzlinách	uzlina	k1gFnPc6	uzlina
<g/>
,	,	kIx,	,
ledvinách	ledvina	k1gFnPc6	ledvina
<g/>
,	,	kIx,	,
mozku	mozek	k1gInSc6	mozek
a	a	k8xC	a
kostech	kost	k1gFnPc6	kost
<g/>
.	.	kIx.	.
</s>
<s>
Tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
může	moct	k5eAaImIp3nS	moct
poškodit	poškodit	k5eAaPmF	poškodit
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
části	část	k1gFnPc4	část
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
poškodí	poškodit	k5eAaPmIp3nS	poškodit
srdce	srdce	k1gNnSc1	srdce
<g/>
,	,	kIx,	,
příčně	příčně	k6eAd1	příčně
pruhované	pruhovaný	k2eAgInPc4d1	pruhovaný
svaly	sval	k1gInPc4	sval
<g/>
,	,	kIx,	,
slinivku	slinivka	k1gFnSc4	slinivka
břišní	břišní	k2eAgFnSc4d1	břišní
nebo	nebo	k8xC	nebo
štítnou	štítný	k2eAgFnSc4d1	štítná
žlázu	žláza	k1gFnSc4	žláza
<g/>
.	.	kIx.	.
<g/>
Tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
je	být	k5eAaImIp3nS	být
zařazována	zařazovat	k5eAaImNgFnS	zařazovat
mezi	mezi	k7c4	mezi
granulomatózní	granulomatózní	k2eAgInPc4d1	granulomatózní
záněty	zánět	k1gInPc4	zánět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
buňky	buňka	k1gFnPc4	buňka
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
granulomy	granulom	k1gInPc1	granulom
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
makrofágy	makrofág	k1gInPc4	makrofág
<g/>
,	,	kIx,	,
T-lymfocyty	Tymfocyt	k1gInPc4	T-lymfocyt
<g/>
,	,	kIx,	,
B-lymfocyty	Bymfocyt	k1gInPc4	B-lymfocyt
a	a	k8xC	a
fibroblasty	fibroblast	k1gInPc4	fibroblast
<g/>
.	.	kIx.	.
</s>
<s>
Granulom	granulom	k1gInSc1	granulom
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
šíření	šíření	k1gNnSc4	šíření
mykobakterií	mykobakterie	k1gFnPc2	mykobakterie
a	a	k8xC	a
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
buňky	buňka	k1gFnPc1	buňka
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
snaží	snažit	k5eAaImIp3nS	snažit
onemocnění	onemocnění	k1gNnSc4	onemocnění
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
v	v	k7c6	v
granulomu	granulom	k1gInSc6	granulom
mohou	moct	k5eAaImIp3nP	moct
přejít	přejít	k5eAaPmF	přejít
do	do	k7c2	do
nečinného	činný	k2eNgNnSc2d1	nečinné
stádia	stádium	k1gNnSc2	stádium
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pak	pak	k6eAd1	pak
vyústí	vyústit	k5eAaPmIp3nS	vyústit
v	v	k7c4	v
latentní	latentní	k2eAgFnSc4d1	latentní
formu	forma	k1gFnSc4	forma
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Tuberkulózní	tuberkulózní	k2eAgInPc1d1	tuberkulózní
granulomy	granulom	k1gInPc1	granulom
také	také	k9	také
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
mrtvé	mrtvý	k2eAgFnPc1d1	mrtvá
buňky	buňka	k1gFnPc1	buňka
(	(	kIx(	(
<g/>
nekróza	nekróza	k1gFnSc1	nekróza
<g/>
)	)	kIx)	)
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
nádoru	nádor	k1gInSc2	nádor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
mrtvé	mrtvý	k2eAgFnPc1d1	mrtvá
buňky	buňka	k1gFnPc1	buňka
vzhledem	vzhledem	k7c3	vzhledem
připomínají	připomínat	k5eAaImIp3nP	připomínat
sýr	sýr	k1gInSc1	sýr
a	a	k8xC	a
této	tento	k3xDgFnSc3	tento
nekróze	nekróza	k1gFnSc3	nekróza
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
říká	říkat	k5eAaImIp3nS	říkat
kaseózní	kaseózní	k2eAgFnSc1d1	kaseózní
nekróza	nekróza	k1gFnSc1	nekróza
<g/>
.	.	kIx.	.
<g/>
Jestliže	jestliže	k8xS	jestliže
bakterie	bakterie	k1gFnPc1	bakterie
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
vniknou	vniknout	k5eAaPmIp3nP	vniknout
do	do	k7c2	do
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
poškozené	poškozený	k2eAgFnSc2d1	poškozená
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
šíří	šířit	k5eAaImIp3nS	šířit
se	se	k3xPyFc4	se
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
zanechávají	zanechávat	k5eAaImIp3nP	zanechávat
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
mnoho	mnoho	k4c4	mnoho
ložisek	ložisko	k1gNnPc2	ložisko
infekce	infekce	k1gFnSc2	infekce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vypadají	vypadat	k5eAaImIp3nP	vypadat
jako	jako	k9	jako
malé	malý	k2eAgInPc1d1	malý
bílé	bílý	k2eAgInPc1d1	bílý
nádory	nádor	k1gInPc1	nádor
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
forma	forma	k1gFnSc1	forma
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
je	být	k5eAaImIp3nS	být
nejčastější	častý	k2eAgNnSc1d3	nejčastější
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
u	u	k7c2	u
starších	starý	k2eAgMnPc2d2	starší
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
miliární	miliární	k2eAgFnSc1d1	miliární
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
bez	bez	k7c2	bez
léčby	léčba	k1gFnSc2	léčba
umírají	umírat	k5eAaImIp3nP	umírat
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
100	[number]	k4	100
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
s	s	k7c7	s
léčbou	léčba	k1gFnSc7	léčba
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
míra	míra	k1gFnSc1	míra
fatality	fatalita	k1gFnSc2	fatalita
snižuje	snižovat	k5eAaImIp3nS	snižovat
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c4	na
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
infekce	infekce	k1gFnSc1	infekce
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
a	a	k8xC	a
zase	zase	k9	zase
ubývá	ubývat	k5eAaImIp3nS	ubývat
<g/>
.	.	kIx.	.
</s>
<s>
Poškození	poškození	k1gNnSc1	poškození
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
nekrózu	nekróza	k1gFnSc4	nekróza
vyvažují	vyvažovat	k5eAaImIp3nP	vyvažovat
hojení	hojený	k2eAgMnPc1d1	hojený
a	a	k8xC	a
fibróza	fibróza	k1gFnSc1	fibróza
<g/>
.	.	kIx.	.
</s>
<s>
Nakažené	nakažený	k2eAgFnPc1d1	nakažená
tkáně	tkáň	k1gFnPc1	tkáň
jsou	být	k5eAaImIp3nP	být
nahrazeny	nahrazen	k2eAgFnPc1d1	nahrazena
jizvami	jizva	k1gFnPc7	jizva
a	a	k8xC	a
dutiny	dutina	k1gFnPc1	dutina
jsou	být	k5eAaImIp3nP	být
zaplněny	zaplněn	k2eAgMnPc4d1	zaplněn
bílým	bílý	k2eAgInSc7d1	bílý
nekrotickým	nekrotický	k2eAgInSc7d1	nekrotický
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
aktivní	aktivní	k2eAgFnSc2d1	aktivní
formy	forma	k1gFnSc2	forma
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
lze	lze	k6eAd1	lze
obsah	obsah	k1gInSc4	obsah
některých	některý	k3yIgFnPc2	některý
těchto	tento	k3xDgFnPc2	tento
dutin	dutina	k1gFnPc2	dutina
vykašlat	vykašlat	k5eAaPmF	vykašlat
<g/>
.	.	kIx.	.
</s>
<s>
Vykašlaný	vykašlaný	k2eAgInSc1d1	vykašlaný
materiál	materiál	k1gInSc1	materiál
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
žijící	žijící	k2eAgFnPc4d1	žijící
bakterie	bakterie	k1gFnPc4	bakterie
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
může	moct	k5eAaImIp3nS	moct
přenášet	přenášet	k5eAaImF	přenášet
infekci	infekce	k1gFnSc4	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
vhodnými	vhodný	k2eAgNnPc7d1	vhodné
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
bakterie	bakterie	k1gFnSc2	bakterie
usmrcuje	usmrcovat	k5eAaImIp3nS	usmrcovat
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hojení	hojení	k1gNnSc1	hojení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
léčbě	léčba	k1gFnSc6	léčba
jsou	být	k5eAaImIp3nP	být
nakažené	nakažený	k2eAgFnPc1d1	nakažená
oblasti	oblast	k1gFnPc1	oblast
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
zjizvenými	zjizvený	k2eAgFnPc7d1	zjizvená
tkáněmi	tkáň	k1gFnPc7	tkáň
<g/>
.	.	kIx.	.
<g/>
Neléčená	léčený	k2eNgFnSc1d1	neléčená
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
lobární	lobární	k2eAgFnSc4d1	lobární
pneumonii	pneumonie	k1gFnSc4	pneumonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
a	a	k8xC	a
symptomy	symptom	k1gInPc1	symptom
==	==	k?	==
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
onemocnění	onemocnění	k1gNnSc4	onemocnění
přejde	přejít	k5eAaPmIp3nS	přejít
do	do	k7c2	do
aktivního	aktivní	k2eAgNnSc2d1	aktivní
stadia	stadion	k1gNnSc2	stadion
<g/>
,	,	kIx,	,
v	v	k7c6	v
75	[number]	k4	75
%	%	kIx~	%
případů	případ	k1gInPc2	případ
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
infekci	infekce	k1gFnSc4	infekce
plic	plíce	k1gFnPc2	plíce
(	(	kIx(	(
<g/>
plicní	plicní	k2eAgFnSc1d1	plicní
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
symptomy	symptom	k1gInPc7	symptom
patří	patřit	k5eAaImIp3nS	patřit
bolest	bolest	k1gFnSc4	bolest
hrudi	hruď	k1gFnSc2	hruď
<g/>
,	,	kIx,	,
vykašlávání	vykašlávání	k1gNnSc2	vykašlávání
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
kašel	kašel	k1gInSc1	kašel
trvající	trvající	k2eAgInSc1d1	trvající
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
<g/>
,	,	kIx,	,
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
třesavka	třesavka	k1gFnSc1	třesavka
<g/>
,	,	kIx,	,
noční	noční	k2eAgFnSc4d1	noční
pocení	pocení	k1gNnSc3	pocení
<g/>
,	,	kIx,	,
ztráta	ztráta	k1gFnSc1	ztráta
chuti	chuť	k1gFnSc2	chuť
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
,	,	kIx,	,
ztráta	ztráta	k1gFnSc1	ztráta
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
pobledlost	pobledlost	k1gFnSc1	pobledlost
a	a	k8xC	a
únava	únava	k1gFnSc1	únava
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c4	v
zbylých	zbylý	k2eAgNnPc2d1	zbylé
25	[number]	k4	25
%	%	kIx~	%
případů	případ	k1gInPc2	případ
se	se	k3xPyFc4	se
infekce	infekce	k1gFnSc1	infekce
přesune	přesunout	k5eAaPmIp3nS	přesunout
z	z	k7c2	z
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
jiné	jiný	k2eAgFnPc4d1	jiná
formy	forma	k1gFnPc4	forma
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
se	se	k3xPyFc4	se
hromadně	hromadně	k6eAd1	hromadně
říká	říkat	k5eAaImIp3nS	říkat
mimoplicní	mimoplicní	k2eAgFnSc1d1	mimoplicní
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
častěji	často	k6eAd2	často
probíhá	probíhat	k5eAaImIp3nS	probíhat
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
potlačenou	potlačený	k2eAgFnSc7d1	potlačená
imunitou	imunita	k1gFnSc7	imunita
a	a	k8xC	a
u	u	k7c2	u
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tuberkulóze	tuberkulóza	k1gFnSc6	tuberkulóza
mízních	mízní	k2eAgFnPc2d1	mízní
uzlin	uzlina	k1gFnPc2	uzlina
(	(	kIx(	(
<g/>
krtice	krtice	k1gFnPc1	krtice
<g/>
)	)	kIx)	)
uzliny	uzlina	k1gFnPc1	uzlina
natékají	natékat	k5eAaImIp3nP	natékat
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
může	moct	k5eAaImIp3nS	moct
začít	začít	k5eAaPmF	začít
vytékat	vytékat	k5eAaImF	vytékat
tekutina	tekutina	k1gFnSc1	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
kloubů	kloub	k1gInPc2	kloub
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
bolest	bolest	k1gFnSc4	bolest
a	a	k8xC	a
oslabení	oslabení	k1gNnSc4	oslabení
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
náchylnější	náchylný	k2eAgFnPc1d2	náchylnější
na	na	k7c4	na
zlomeniny	zlomenina	k1gFnPc4	zlomenina
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
postihovat	postihovat	k5eAaImF	postihovat
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgFnPc4	všechen
kosti	kost	k1gFnPc4	kost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
obratle	obratel	k1gInSc2	obratel
(	(	kIx(	(
<g/>
Pottova	Pottův	k2eAgFnSc1d1	Pottův
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
čelistní	čelistní	k2eAgFnPc1d1	čelistní
kosti	kost	k1gFnPc1	kost
–	–	k?	–
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nejčastěji	často	k6eAd3	často
lícní	lícní	k2eAgFnSc4d1	lícní
kost	kost	k1gFnSc4	kost
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
hematogenně	hematogenně	k6eAd1	hematogenně
(	(	kIx(	(
<g/>
přenos	přenos	k1gInSc1	přenos
krevní	krevní	k2eAgInSc1d1	krevní
cestou	cesta	k1gFnSc7	cesta
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
per	prát	k5eAaImRp2nS	prát
continuitatem	continuitat	k1gInSc7	continuitat
(	(	kIx(	(
<g/>
přestupem	přestup	k1gInSc7	přestup
skrz	skrz	k7c4	skrz
tkáně	tkáň	k1gFnPc4	tkáň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
projevy	projev	k1gInPc1	projev
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
vznikem	vznik	k1gInSc7	vznik
píštělí	píštěl	k1gFnPc2	píštěl
a	a	k8xC	a
vtažených	vtažený	k2eAgFnPc2d1	vtažená
jizev	jizva	k1gFnPc2	jizva
<g/>
.	.	kIx.	.
</s>
<s>
Symptomy	symptom	k1gInPc1	symptom
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
trávicí	trávicí	k2eAgFnSc2d1	trávicí
soustavy	soustava	k1gFnSc2	soustava
jsou	být	k5eAaImIp3nP	být
bolest	bolest	k1gFnSc4	bolest
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
průjem	průjem	k1gInSc1	průjem
a	a	k8xC	a
krvácení	krvácení	k1gNnSc1	krvácení
z	z	k7c2	z
řitního	řitní	k2eAgInSc2d1	řitní
otvoru	otvor	k1gInSc2	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Urogenitální	urogenitální	k2eAgFnSc1d1	urogenitální
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
např.	např.	kA	např.
bolest	bolest	k1gFnSc4	bolest
při	při	k7c6	při
močení	močení	k1gNnSc6	močení
a	a	k8xC	a
krvavou	krvavý	k2eAgFnSc4d1	krvavá
moč	moč	k1gFnSc4	moč
a	a	k8xC	a
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
centrální	centrální	k2eAgFnSc2d1	centrální
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
bolest	bolest	k1gFnSc4	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
zamlžené	zamlžený	k2eAgFnSc2d1	zamlžená
vidění	vidění	k1gNnSc3	vidění
a	a	k8xC	a
záchvaty	záchvat	k1gInPc7	záchvat
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
existuje	existovat	k5eAaImIp3nS	existovat
např.	např.	kA	např.
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
kůže	kůže	k1gFnSc1	kůže
(	(	kIx(	(
<g/>
lupus	lupus	k1gInSc1	lupus
vulgaris	vulgaris	k1gFnSc2	vulgaris
nebo	nebo	k8xC	nebo
tuberculosis	tuberculosis	k1gFnSc2	tuberculosis
verrucosa	verrucosa	k1gFnSc1	verrucosa
cutis	cutis	k1gFnSc1	cutis
<g/>
)	)	kIx)	)
způsobující	způsobující	k2eAgInPc1d1	způsobující
pupínky	pupínek	k1gInPc1	pupínek
<g/>
,	,	kIx,	,
bradavice	bradavice	k1gFnPc1	bradavice
<g/>
,	,	kIx,	,
puchýře	puchýř	k1gInPc1	puchýř
nebo	nebo	k8xC	nebo
vředy	vřed	k1gInPc1	vřed
nebo	nebo	k8xC	nebo
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
osrdečníku	osrdečník	k1gInSc2	osrdečník
způsobující	způsobující	k2eAgFnSc2d1	způsobující
těžkosti	těžkost	k1gFnSc2	těžkost
při	při	k7c6	při
dýchání	dýchání	k1gNnSc6	dýchání
a	a	k8xC	a
bolest	bolest	k1gFnSc4	bolest
hrudi	hruď	k1gFnSc2	hruď
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
nebezpečnou	bezpečný	k2eNgFnSc7d1	nebezpečná
formou	forma	k1gFnSc7	forma
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
je	být	k5eAaImIp3nS	být
miliární	miliární	k2eAgFnSc1d1	miliární
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
napadá	napadat	k5eAaPmIp3nS	napadat
několik	několik	k4yIc4	několik
míst	místo	k1gNnPc2	místo
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Mimoplicní	mimoplicní	k2eAgFnSc1d1	mimoplicní
a	a	k8xC	a
plicní	plicní	k2eAgFnSc1d1	plicní
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
mohou	moct	k5eAaImIp3nP	moct
probíhat	probíhat	k5eAaImF	probíhat
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diagnostika	diagnostika	k1gFnSc1	diagnostika
==	==	k?	==
</s>
</p>
<p>
<s>
Rozhodujícím	rozhodující	k2eAgNnSc7d1	rozhodující
diagnostickým	diagnostický	k2eAgNnSc7d1	diagnostické
kritériem	kritérion	k1gNnSc7	kritérion
je	být	k5eAaImIp3nS	být
průkaz	průkaz	k1gInSc4	průkaz
tuberkulózních	tuberkulózní	k2eAgInPc2d1	tuberkulózní
bacilů	bacil	k1gInPc2	bacil
(	(	kIx(	(
<g/>
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
tuberculosis	tuberculosis	k1gFnPc2	tuberculosis
<g/>
)	)	kIx)	)
v	v	k7c6	v
klinickém	klinický	k2eAgInSc6d1	klinický
vzorku	vzorek	k1gInSc6	vzorek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ve	v	k7c6	v
sputu	sputum	k1gNnSc6	sputum
nebo	nebo	k8xC	nebo
hnisu	hnis	k1gInSc6	hnis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
kultivačně	kultivačně	k6eAd1	kultivačně
nebo	nebo	k8xC	nebo
mikroskopicky	mikroskopicky	k6eAd1	mikroskopicky
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ho	on	k3xPp3gNnSc4	on
z	z	k7c2	z
nějakého	nějaký	k3yIgInSc2	nějaký
důvodu	důvod	k1gInSc2	důvod
nelze	lze	k6eNd1	lze
získat	získat	k5eAaPmF	získat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
diagnóza	diagnóza	k1gFnSc1	diagnóza
možná	možný	k2eAgFnSc1d1	možná
pomocí	pomocí	k7c2	pomocí
zobrazování	zobrazování	k1gNnSc2	zobrazování
(	(	kIx(	(
<g/>
např.	např.	kA	např.
rentgenu	rentgen	k1gInSc2	rentgen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tuberkulinového	tuberkulinový	k2eAgInSc2d1	tuberkulinový
testu	test	k1gInSc2	test
<g/>
,	,	kIx,	,
histopatologie	histopatologie	k1gFnSc2	histopatologie
anebo	anebo	k8xC	anebo
pomocí	pomocí	k7c2	pomocí
Interferon	interferon	k1gInSc1	interferon
Gamma	Gamma	k1gNnSc4	Gamma
Release	Releasa	k1gFnSc3	Releasa
Assay	Assaa	k1gFnSc2	Assaa
(	(	kIx(	(
<g/>
IGRA	IGRA	kA	IGRA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tyto	tento	k3xDgFnPc1	tento
metody	metoda	k1gFnPc1	metoda
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
neprůkazné	průkazný	k2eNgNnSc1d1	neprůkazné
<g/>
.	.	kIx.	.
<g/>
Celková	celkový	k2eAgFnSc1d1	celková
diagnóza	diagnóza	k1gFnSc1	diagnóza
při	při	k7c6	při
podezření	podezření	k1gNnSc6	podezření
z	z	k7c2	z
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
obsahovat	obsahovat	k5eAaImF	obsahovat
anamnézu	anamnéza	k1gFnSc4	anamnéza
<g/>
,	,	kIx,	,
fyzické	fyzický	k2eAgNnSc4d1	fyzické
vyšetření	vyšetření	k1gNnSc4	vyšetření
<g/>
,	,	kIx,	,
rentgen	rentgen	k1gInSc4	rentgen
hrudi	hruď	k1gFnSc2	hruď
<g/>
,	,	kIx,	,
mikrobiologický	mikrobiologický	k2eAgInSc1d1	mikrobiologický
vzorek	vzorek	k1gInSc1	vzorek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sputum	sputum	k1gNnSc4	sputum
<g/>
)	)	kIx)	)
a	a	k8xC	a
buněčnou	buněčný	k2eAgFnSc4d1	buněčná
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
i	i	k9	i
tuberkulinový	tuberkulinový	k2eAgInSc4d1	tuberkulinový
test	test	k1gInSc4	test
a	a	k8xC	a
sérologický	sérologický	k2eAgInSc4d1	sérologický
test	test	k1gInSc4	test
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
diagnózy	diagnóza	k1gFnSc2	diagnóza
je	být	k5eAaImIp3nS	být
kultivace	kultivace	k1gFnSc1	kultivace
tohoto	tento	k3xDgInSc2	tento
pomalurostoucího	pomalurostoucí	k2eAgInSc2d1	pomalurostoucí
organismu	organismus	k1gInSc2	organismus
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
kultivační	kultivační	k2eAgNnSc1d1	kultivační
médium	médium	k1gNnSc1	médium
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
krevní	krevní	k2eAgInSc4d1	krevní
agar	agar	k1gInSc4	agar
nebo	nebo	k8xC	nebo
Lowensteinovo-Jensenovo	Lowensteinovo-Jensenův	k2eAgNnSc4d1	Lowensteinovo-Jensenův
médium	médium	k1gNnSc4	médium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
bývá	bývat	k5eAaImIp3nS	bývat
latentní	latentní	k2eAgFnSc1d1	latentní
forma	forma	k1gFnSc1	forma
infekce	infekce	k1gFnSc2	infekce
diagnostikována	diagnostikovat	k5eAaBmNgFnS	diagnostikovat
u	u	k7c2	u
neočkovaných	očkovaný	k2eNgMnPc2d1	neočkovaný
lidí	člověk	k1gMnPc2	člověk
tuberkulinovým	tuberkulinový	k2eAgInSc7d1	tuberkulinový
kožním	kožní	k2eAgInSc7d1	kožní
testem	test	k1gInSc7	test
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
vzorek	vzorek	k1gInSc4	vzorek
M.	M.	kA	M.
tuberculosis	tuberculosis	k1gInSc1	tuberculosis
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
už	už	k6eAd1	už
proti	proti	k7c3	proti
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
očkováni	očkován	k2eAgMnPc1d1	očkován
byli	být	k5eAaImAgMnP	být
<g/>
,	,	kIx,	,
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
test	test	k1gInSc4	test
reagují	reagovat	k5eAaBmIp3nP	reagovat
mnohem	mnohem	k6eAd1	mnohem
citlivěji	citlivě	k6eAd2	citlivě
<g/>
.	.	kIx.	.
</s>
<s>
Tuberkulinový	tuberkulinový	k2eAgInSc1d1	tuberkulinový
test	test	k1gInSc1	test
má	mít	k5eAaImIp3nS	mít
tu	ten	k3xDgFnSc4	ten
nevýhodu	nevýhoda	k1gFnSc4	nevýhoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
negativním	negativní	k2eAgInSc7d1	negativní
i	i	k9	i
za	za	k7c4	za
přítomnosti	přítomnost	k1gFnPc4	přítomnost
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
testovaný	testovaný	k2eAgMnSc1d1	testovaný
člověk	člověk	k1gMnSc1	člověk
komorbidní	komorbidní	k2eAgFnSc4d1	komorbidní
sarkoidózu	sarkoidóza	k1gFnSc4	sarkoidóza
<g/>
,	,	kIx,	,
Hodgkinovu	Hodgkinův	k2eAgFnSc4d1	Hodgkinova
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
malnutrici	malnutrice	k1gFnSc4	malnutrice
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
aktivní	aktivní	k2eAgFnSc4d1	aktivní
formu	forma	k1gFnSc4	forma
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgInPc1d2	novější
testy	test	k1gInPc1	test
pomocí	pomocí	k7c2	pomocí
interferonů	interferon	k1gInPc2	interferon
gama	gama	k1gNnSc2	gama
(	(	kIx(	(
<g/>
Interferon	interferon	k1gInSc1	interferon
Gamma	Gammum	k1gNnSc2	Gammum
Release	Releasa	k1gFnSc6	Releasa
Assay	Assay	k1gInPc1	Assay
–	–	k?	–
IGRA	IGRA	kA	IGRA
<g/>
)	)	kIx)	)
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
problémů	problém	k1gInPc2	problém
překonaly	překonat	k5eAaPmAgFnP	překonat
<g/>
.	.	kIx.	.
</s>
<s>
Testy	test	k1gInPc1	test
IGRA	IGRA	kA	IGRA
jsou	být	k5eAaImIp3nP	být
krevní	krevní	k2eAgInPc1d1	krevní
testy	test	k1gInPc1	test
in	in	k?	in
vitro	vitro	k6eAd1	vitro
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
přesnější	přesný	k2eAgInPc1d2	přesnější
než	než	k8xS	než
kožní	kožní	k2eAgInPc1d1	kožní
testy	test	k1gInPc1	test
<g/>
.	.	kIx.	.
</s>
<s>
Dokáží	dokázat	k5eAaPmIp3nP	dokázat
odhalit	odhalit	k5eAaPmF	odhalit
uvolňování	uvolňování	k1gNnSc4	uvolňování
interferonu	interferon	k1gInSc2	interferon
gama	gama	k1gNnSc2	gama
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
mykobakteriální	mykobakteriální	k2eAgFnPc4d1	mykobakteriální
bílkoviny	bílkovina	k1gFnPc4	bílkovina
jako	jako	k8xS	jako
ESAT-	ESAT-	k1gFnSc4	ESAT-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Testy	test	k1gInPc1	test
nejsou	být	k5eNaImIp3nP	být
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
předchozím	předchozí	k2eAgNnSc7d1	předchozí
očkováním	očkování	k1gNnSc7	očkování
ani	ani	k8xC	ani
netuberkulózními	tuberkulózní	k2eNgFnPc7d1	netuberkulózní
mykobakteriemi	mykobakterie	k1gFnPc7	mykobakterie
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vracejí	vracet	k5eAaImIp3nP	vracet
méně	málo	k6eAd2	málo
nesprávných	správný	k2eNgInPc2d1	nesprávný
pozitivních	pozitivní	k2eAgInPc2d1	pozitivní
výsledků	výsledek	k1gInPc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
testy	test	k1gInPc1	test
IGRA	IGRA	kA	IGRA
jsou	být	k5eAaImIp3nP	být
citlivější	citlivý	k2eAgInPc1d2	citlivější
než	než	k8xS	než
kožní	kožní	k2eAgInPc1d1	kožní
testy	test	k1gInPc1	test
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgNnSc1	některý
nově	nově	k6eAd1	nově
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
testy	test	k1gInPc1	test
jsou	být	k5eAaImIp3nP	být
rychlejší	rychlý	k2eAgInPc1d2	rychlejší
a	a	k8xC	a
přesnější	přesný	k2eAgInPc1d2	přesnější
než	než	k8xS	než
výše	vysoce	k6eAd2	vysoce
zmíněné	zmíněný	k2eAgFnPc1d1	zmíněná
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
test	test	k1gInSc4	test
pomocí	pomocí	k7c2	pomocí
polymerázové	polymerázový	k2eAgFnSc2d1	polymerázová
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
zjištění	zjištění	k1gNnSc3	zjištění
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
testů	test	k1gInPc2	test
vrací	vracet	k5eAaImIp3nS	vracet
výsledek	výsledek	k1gInSc1	výsledek
do	do	k7c2	do
100	[number]	k4	100
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
nabízen	nabízet	k5eAaImNgInS	nabízet
116	[number]	k4	116
rozvojovým	rozvojový	k2eAgFnPc3d1	rozvojová
zemím	zem	k1gFnPc3	zem
se	s	k7c7	s
slevou	sleva	k1gFnSc7	sleva
zajištěnou	zajištěný	k2eAgFnSc4d1	zajištěná
WHO	WHO	kA	WHO
a	a	k8xC	a
nadací	nadace	k1gFnSc7	nadace
Billa	Bill	k1gMnSc2	Bill
a	a	k8xC	a
Melindy	Melinda	k1gFnSc2	Melinda
Gatesových	Gatesový	k2eAgMnPc2d1	Gatesový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prevence	prevence	k1gFnSc1	prevence
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
prevenci	prevence	k1gFnSc3	prevence
a	a	k8xC	a
kontrole	kontrola	k1gFnSc3	kontrola
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
se	se	k3xPyFc4	se
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
úrovních	úroveň	k1gFnPc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
identifikaci	identifikace	k1gFnSc6	identifikace
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterými	který	k3yIgFnPc7	který
se	se	k3xPyFc4	se
stýkají	stýkat	k5eAaImIp3nP	stýkat
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
následná	následný	k2eAgFnSc1d1	následná
léčba	léčba	k1gFnSc1	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
úrovní	úroveň	k1gFnSc7	úroveň
je	být	k5eAaImIp3nS	být
očkování	očkování	k1gNnSc1	očkování
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
žádná	žádný	k3yNgFnSc1	žádný
vakcína	vakcína	k1gFnSc1	vakcína
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
spolehlivou	spolehlivý	k2eAgFnSc4d1	spolehlivá
ochranu	ochrana	k1gFnSc4	ochrana
pro	pro	k7c4	pro
dospělé	dospělý	k2eAgFnPc4d1	dospělá
osoby	osoba	k1gFnPc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
ale	ale	k8xC	ale
jako	jako	k9	jako
částečná	částečný	k2eAgFnSc1d1	částečná
ochrana	ochrana	k1gFnSc1	ochrana
může	moct	k5eAaImIp3nS	moct
fungovat	fungovat	k5eAaImF	fungovat
vystavení	vystavení	k1gNnSc4	vystavení
netuberkulózním	tuberkulózní	k2eNgFnPc3d1	netuberkulózní
mykobakteriím	mykobakterie	k1gFnPc3	mykobakterie
<g/>
.	.	kIx.	.
<g/>
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
nouzový	nouzový	k2eAgInSc4d1	nouzový
stav	stav	k1gInSc4	stav
ohrožení	ohrožení	k1gNnSc2	ohrožení
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
a	a	k8xC	a
Stop	stop	k1gInSc1	stop
TBC	TBC	kA	TBC
Partnership	Partnership	k1gInSc1	Partnership
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
Celosvětový	celosvětový	k2eAgInSc4d1	celosvětový
plán	plán	k1gInSc4	plán
pro	pro	k7c4	pro
zastavení	zastavení	k1gNnSc4	zastavení
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
(	(	kIx(	(
<g/>
Global	globat	k5eAaImAgInS	globat
Plan	plan	k1gInSc1	plan
to	ten	k3xDgNnSc1	ten
Stop	stop	k2eAgInSc1d1	stop
Tuberculosis	Tuberculosis	k1gInSc1	Tuberculosis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
zachránit	zachránit	k5eAaPmF	zachránit
14	[number]	k4	14
milionů	milion	k4xCgInPc2	milion
životů	život	k1gInPc2	život
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
2006	[number]	k4	2006
až	až	k9	až
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vakcíny	vakcína	k1gFnSc2	vakcína
===	===	k?	===
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
zemí	zem	k1gFnPc2	zem
využívá	využívat	k5eAaImIp3nS	využívat
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
programu	program	k1gInSc2	program
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
očkování	očkování	k1gNnSc2	očkování
bacilem	bacil	k1gInSc7	bacil
Calmettovým-Guérinovým	Calmettovým-Guérinův	k2eAgInSc7d1	Calmettovým-Guérinův
(	(	kIx(	(
<g/>
BCG	BCG	kA	BCG
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
kojence	kojenec	k1gMnPc4	kojenec
<g/>
.	.	kIx.	.
</s>
<s>
BCG	BCG	kA	BCG
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejčastěji	často	k6eAd3	často
využívaných	využívaný	k2eAgFnPc2d1	využívaná
vakcín	vakcína	k1gFnPc2	vakcína
současnosti	současnost	k1gFnSc2	současnost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
s	s	k7c7	s
národními	národní	k2eAgInPc7d1	národní
programy	program	k1gInPc7	program
vakcinace	vakcinace	k1gFnSc2	vakcinace
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
míra	míra	k1gFnSc1	míra
zastoupení	zastoupení	k1gNnSc2	zastoupení
této	tento	k3xDgFnSc2	tento
vakcíny	vakcína	k1gFnSc2	vakcína
80	[number]	k4	80
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
BCG	BCG	kA	BCG
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
neposkytuje	poskytovat	k5eNaImIp3nS	poskytovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
není	být	k5eNaImIp3nS	být
tak	tak	k9	tak
častým	častý	k2eAgNnSc7d1	časté
onemocněním	onemocnění	k1gNnSc7	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
BCG	BCG	kA	BCG
byl	být	k5eAaImAgInS	být
první	první	k4xOgFnSc7	první
vakcínou	vakcína	k1gFnSc7	vakcína
proti	proti	k7c3	proti
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
v	v	k7c6	v
Pasteurově	Pasteurův	k2eAgInSc6d1	Pasteurův
ústavu	ústav	k1gInSc6	ústav
pracovali	pracovat	k5eAaImAgMnP	pracovat
Albert	Albert	k1gMnSc1	Albert
Calmette	Calmett	k1gInSc5	Calmett
a	a	k8xC	a
Camille	Camille	k1gFnSc1	Camille
Guérin	Guérin	k1gInSc4	Guérin
a	a	k8xC	a
první	první	k4xOgInPc4	první
testy	test	k1gInPc4	test
na	na	k7c6	na
lidech	člověk	k1gMnPc6	člověk
provedli	provést	k5eAaPmAgMnP	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
Úmrtí	úmrtí	k1gNnPc1	úmrtí
způsobená	způsobený	k2eAgNnPc1d1	způsobené
špatnými	špatný	k2eAgInPc7d1	špatný
výrobními	výrobní	k2eAgInPc7d1	výrobní
procesy	proces	k1gInPc7	proces
ale	ale	k8xC	ale
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
veřejný	veřejný	k2eAgInSc4d1	veřejný
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
BCG	BCG	kA	BCG
a	a	k8xC	a
hromadné	hromadný	k2eAgNnSc1d1	hromadné
očkování	očkování	k1gNnSc1	očkování
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
odloženo	odložit	k5eAaPmNgNnS	odložit
až	až	k9	až
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Meta-analýza	Metanalýza	k1gFnSc1	Meta-analýza
několika	několik	k4yIc2	několik
studií	studie	k1gFnPc2	studie
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
účinnost	účinnost	k1gFnSc1	účinnost
vakcín	vakcína	k1gFnPc2	vakcína
BCG	BCG	kA	BCG
proti	proti	k7c3	proti
některým	některý	k3yIgFnPc3	některý
formám	forma	k1gFnPc3	forma
TBC	TBC	kA	TBC
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tuberkulózní	tuberkulózní	k2eAgFnSc1d1	tuberkulózní
meningitida	meningitida	k1gFnSc1	meningitida
a	a	k8xC	a
miliární	miliární	k2eAgFnSc1d1	miliární
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
80	[number]	k4	80
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
účinnost	účinnost	k1gFnSc4	účinnost
BCG	BCG	kA	BCG
vakcín	vakcína	k1gFnPc2	vakcína
proti	proti	k7c3	proti
plicní	plicní	k2eAgFnSc3d1	plicní
formě	forma	k1gFnSc3	forma
TBC	TBC	kA	TBC
u	u	k7c2	u
dospívajících	dospívající	k2eAgMnPc2d1	dospívající
a	a	k8xC	a
dospělých	dospělý	k2eAgMnPc2d1	dospělý
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
0	[number]	k4	0
do	do	k7c2	do
80	[number]	k4	80
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Příčina	příčina	k1gFnSc1	příčina
tak	tak	k6eAd1	tak
velkého	velký	k2eAgInSc2d1	velký
rozptylu	rozptyl	k1gInSc2	rozptyl
hlášených	hlášený	k2eAgFnPc2d1	hlášená
účinností	účinnost	k1gFnPc2	účinnost
ochrany	ochrana	k1gFnSc2	ochrana
před	před	k7c7	před
plicní	plicní	k2eAgFnSc7d1	plicní
formou	forma	k1gFnSc7	forma
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
svoji	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
hrají	hrát	k5eAaImIp3nP	hrát
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
kmeny	kmen	k1gInPc7	kmen
používanými	používaný	k2eAgInPc7d1	používaný
při	při	k7c6	při
očkování	očkování	k1gNnSc6	očkování
<g/>
,	,	kIx,	,
různá	různý	k2eAgFnSc1d1	různá
expozice	expozice	k1gFnSc1	expozice
sledovaných	sledovaný	k2eAgFnPc2d1	sledovaná
populací	populace	k1gFnPc2	populace
mykobaktériím	mykobaktérie	k1gFnPc3	mykobaktérie
v	v	k7c6	v
životním	životní	k2eAgNnSc6d1	životní
prostředí	prostředí	k1gNnSc6	prostředí
a	a	k8xC	a
genetické	genetický	k2eAgInPc4d1	genetický
a	a	k8xC	a
výživové	výživový	k2eAgInPc4d1	výživový
rozdíly	rozdíl	k1gInPc4	rozdíl
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
populací	populace	k1gFnPc2	populace
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
existuje	existovat	k5eAaImIp3nS	existovat
celosvětově	celosvětově	k6eAd1	celosvětově
více	hodně	k6eAd2	hodně
případů	případ	k1gInPc2	případ
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
než	než	k8xS	než
kdykoli	kdykoli	k6eAd1	kdykoli
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
státě	stát	k1gInSc6	stát
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
prevalencí	prevalence	k1gFnSc7	prevalence
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
BCG	BCG	kA	BCG
očkováno	očkovat	k5eAaImNgNnS	očkovat
každé	každý	k3xTgNnSc1	každý
dítě	dítě	k1gNnSc1	dítě
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
BCG	BCG	kA	BCG
má	mít	k5eAaImIp3nS	mít
slabší	slabý	k2eAgInPc4d2	slabší
účinky	účinek	k1gInPc4	účinek
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
prevalence	prevalence	k1gFnSc1	prevalence
mykobakterií	mykobakterie	k1gFnPc2	mykobakterie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
existují	existovat	k5eAaImIp3nP	existovat
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
vývoj	vývoj	k1gInSc4	vývoj
novějších	nový	k2eAgFnPc2d2	novější
vakcín	vakcína	k1gFnPc2	vakcína
na	na	k7c4	na
prevenci	prevence	k1gFnSc4	prevence
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
rekombinantní	rekombinantní	k2eAgFnSc1d1	rekombinantní
vakcína	vakcína	k1gFnSc1	vakcína
proti	proti	k7c3	proti
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
<g/>
,	,	kIx,	,
Mtb	Mtb	k1gFnSc1	Mtb
<g/>
72	[number]	k4	72
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
vešla	vejít	k5eAaPmAgFnS	vejít
v	v	k7c6	v
USA	USA	kA	USA
do	do	k7c2	do
fáze	fáze	k1gFnSc2	fáze
klinického	klinický	k2eAgNnSc2d1	klinické
testování	testování	k1gNnSc2	testování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
DNA	dna	k1gFnSc1	dna
vakcína	vakcína	k1gFnSc1	vakcína
proti	proti	k7c3	proti
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
podávaná	podávaný	k2eAgFnSc1d1	podávaná
s	s	k7c7	s
obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
chemoterapií	chemoterapie	k1gFnSc7	chemoterapie
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
myší	myš	k1gFnPc2	myš
urychlit	urychlit	k5eAaPmF	urychlit
mizení	mizení	k1gNnSc4	mizení
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
ochránit	ochránit	k5eAaPmF	ochránit
proti	proti	k7c3	proti
opětovné	opětovný	k2eAgFnSc3d1	opětovná
nákaze	nákaza	k1gFnSc3	nákaza
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
vakcína	vakcína	k1gFnSc1	vakcína
MVA85A	MVA85A	k1gFnPc2	MVA85A
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
geneticky	geneticky	k6eAd1	geneticky
modifikovaném	modifikovaný	k2eAgInSc6d1	modifikovaný
viru	vir	k1gInSc6	vir
vaccinia	vaccinium	k1gNnSc2	vaccinium
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
fungoval	fungovat	k5eAaImAgMnS	fungovat
jako	jako	k8xS	jako
vakcína	vakcína	k1gFnSc1	vakcína
proti	proti	k7c3	proti
neštovicím	neštovice	k1gFnPc3	neštovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
nových	nový	k2eAgFnPc2d1	nová
vakcín	vakcína	k1gFnPc2	vakcína
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
mnoho	mnoho	k4c1	mnoho
různých	různý	k2eAgFnPc2d1	různá
strategií	strategie	k1gFnPc2	strategie
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
vakcín	vakcína	k1gFnPc2	vakcína
lze	lze	k6eAd1	lze
podávat	podávat	k5eAaImF	podávat
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
jehel	jehla	k1gFnPc2	jehla
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
pro	pro	k7c4	pro
oblasti	oblast	k1gFnPc4	oblast
s	s	k7c7	s
častými	častý	k2eAgInPc7d1	častý
případy	případ	k1gInPc7	případ
AIDS	AIDS	kA	AIDS
<g/>
.	.	kIx.	.
<g/>
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
stimulace	stimulace	k1gFnSc1	stimulace
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
vakcinací	vakcinace	k1gFnPc2	vakcinace
BCG	BCG	kA	BCG
zřejmě	zřejmě	k6eAd1	zřejmě
představuje	představovat	k5eAaImIp3nS	představovat
faktor	faktor	k1gInSc1	faktor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
snižuje	snižovat	k5eAaImIp3nS	snižovat
riziko	riziko	k1gNnSc4	riziko
vzniku	vznik	k1gInSc2	vznik
maligního	maligní	k2eAgInSc2d1	maligní
melanomu	melanom	k1gInSc2	melanom
<g/>
.	.	kIx.	.
</s>
<s>
Pfahlberg	Pfahlberg	k1gMnSc1	Pfahlberg
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
publikovali	publikovat	k5eAaBmAgMnP	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
studii	studie	k1gFnSc4	studie
designovanou	designovaný	k2eAgFnSc4d1	designovaná
jako	jako	k8xC	jako
retrospektivní	retrospektivní	k2eAgFnSc1d1	retrospektivní
analýza	analýza	k1gFnSc1	analýza
případů	případ	k1gInPc2	případ
a	a	k8xC	a
kontrol	kontrola	k1gFnPc2	kontrola
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odds	odds	k6eAd1	odds
ratio	ratio	k6eAd1	ratio
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
očkování	očkování	k1gNnSc6	očkování
0,44	[number]	k4	0,44
<g/>
.	.	kIx.	.
</s>
<s>
BCG	BCG	kA	BCG
kmeny	kmen	k1gInPc1	kmen
se	se	k3xPyFc4	se
testují	testovat	k5eAaImIp3nP	testovat
jako	jako	k9	jako
prostředek	prostředek	k1gInSc4	prostředek
ke	k	k7c3	k
stimulaci	stimulace	k1gFnSc3	stimulace
buněčné	buněčný	k2eAgFnSc2d1	buněčná
odpovědi	odpověď	k1gFnSc2	odpověď
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
při	při	k7c6	při
terapii	terapie	k1gFnSc6	terapie
řady	řada	k1gFnPc1	řada
nádorů	nádor	k1gInPc2	nádor
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
užití	užití	k1gNnSc1	užití
této	tento	k3xDgFnSc2	tento
formy	forma	k1gFnSc2	forma
imunoterapie	imunoterapie	k1gFnSc2	imunoterapie
i	i	k8xC	i
do	do	k7c2	do
klinické	klinický	k2eAgFnSc2d1	klinická
praxe	praxe	k1gFnSc2	praxe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Léčba	léčba	k1gFnSc1	léčba
==	==	k?	==
</s>
</p>
<p>
<s>
Léčba	léčba	k1gFnSc1	léčba
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c4	v
podávání	podávání	k1gNnSc4	podávání
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
(	(	kIx(	(
<g/>
antituberkulotik	antituberkulotikum	k1gNnPc2	antituberkulotikum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
usmrcují	usmrcovat	k5eAaImIp3nP	usmrcovat
bakterie	bakterie	k1gFnPc4	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
neobvyklé	obvyklý	k2eNgFnSc3d1	neobvyklá
struktuře	struktura	k1gFnSc3	struktura
a	a	k8xC	a
chemickému	chemický	k2eAgNnSc3d1	chemické
složení	složení	k1gNnSc3	složení
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
mykobakterií	mykobakterie	k1gFnPc2	mykobakterie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
snižují	snižovat	k5eAaImIp3nP	snižovat
účinnost	účinnost	k1gFnSc4	účinnost
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
léčba	léčba	k1gFnSc1	léčba
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
za	za	k7c7	za
nejúčinnější	účinný	k2eAgFnSc7d3	nejúčinnější
považovány	považován	k2eAgInPc1d1	považován
plně	plně	k6eAd1	plně
kontrolované	kontrolovaný	k2eAgInPc1d1	kontrolovaný
krátkodobé	krátkodobý	k2eAgInPc1d1	krátkodobý
režimy	režim	k1gInPc1	režim
(	(	kIx(	(
<g/>
DOTS	DOTS	kA	DOTS
–	–	k?	–
Directly	Directly	k1gMnSc1	Directly
Observed	Observed	k1gMnSc1	Observed
Treatment	Treatment	k1gMnSc1	Treatment
Short-Course	Short-Course	k1gFnSc1	Short-Course
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
4	[number]	k4	4
kategorie	kategorie	k1gFnSc2	kategorie
léčby	léčba	k1gFnSc2	léčba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
lokalizace	lokalizace	k1gFnSc2	lokalizace
<g/>
,	,	kIx,	,
rozsahu	rozsah	k1gInSc2	rozsah
onemocnění	onemocnění	k1gNnSc2	onemocnění
a	a	k8xC	a
bakteriologického	bakteriologický	k2eAgInSc2d1	bakteriologický
nálezu	nález	k1gInSc2	nález
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
kategorii	kategorie	k1gFnSc4	kategorie
tvoří	tvořit	k5eAaImIp3nP	tvořit
nová	nový	k2eAgNnPc1d1	nové
bakteriologicky	bakteriologicky	k6eAd1	bakteriologicky
ověřená	ověřený	k2eAgNnPc1d1	ověřené
onemocnění	onemocnění	k1gNnPc1	onemocnění
a	a	k8xC	a
klinicky	klinicky	k6eAd1	klinicky
závažné	závažný	k2eAgFnPc4d1	závažná
formy	forma	k1gFnPc4	forma
plicní	plicní	k2eAgFnPc4d1	plicní
a	a	k8xC	a
mimoplicní	mimoplicní	k2eAgFnPc4d1	mimoplicní
tuberkulózy	tuberkulóza	k1gFnPc4	tuberkulóza
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
recidivy	recidiva	k1gFnPc4	recidiva
<g/>
,	,	kIx,	,
léčebné	léčebný	k2eAgInPc4d1	léčebný
neúspěchy	neúspěch	k1gInPc4	neúspěch
a	a	k8xC	a
přerušené	přerušený	k2eAgFnPc4d1	přerušená
léčby	léčba	k1gFnPc4	léčba
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc4	třetí
méně	málo	k6eAd2	málo
závažné	závažný	k2eAgFnPc4d1	závažná
formy	forma	k1gFnPc4	forma
neověřené	ověřený	k2eNgFnSc2d1	neověřená
plicní	plicní	k2eAgFnSc2d1	plicní
a	a	k8xC	a
mimoplicní	mimoplicní	k2eAgFnSc2d1	mimoplicní
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
,	,	kIx,	,
a	a	k8xC	a
čtvrtou	čtvrtá	k1gFnSc4	čtvrtá
tvoří	tvořit	k5eAaImIp3nP	tvořit
ostatní	ostatní	k2eAgInPc1d1	ostatní
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
bakteriologicky	bakteriologicky	k6eAd1	bakteriologicky
pozitivní	pozitivní	k2eAgInPc1d1	pozitivní
i	i	k9	i
po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
dokončení	dokončení	k1gNnSc6	dokončení
léčby	léčba	k1gFnSc2	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
každé	každý	k3xTgFnSc2	každý
kategorie	kategorie	k1gFnSc2	kategorie
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jiný	jiný	k2eAgInSc1d1	jiný
léčebný	léčebný	k2eAgInSc1d1	léčebný
režim	režim	k1gInSc1	režim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvěma	dva	k4xCgFnPc7	dva
nejpoužívanějšími	používaný	k2eAgFnPc7d3	nejpoužívanější
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
jsou	být	k5eAaImIp3nP	být
isoniazid	isoniazid	k1gInSc1	isoniazid
a	a	k8xC	a
rifampicin	rifampicin	k2eAgInSc1d1	rifampicin
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgFnPc2d1	ostatní
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
infekcí	infekce	k1gFnPc2	infekce
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
mnohem	mnohem	k6eAd1	mnohem
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
léčby	léčba	k1gFnSc2	léčba
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
6	[number]	k4	6
a	a	k8xC	a
24	[number]	k4	24
měsíci	měsíc	k1gInPc7	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
pomalé	pomalý	k2eAgNnSc1d1	pomalé
dělení	dělení	k1gNnSc1	dělení
a	a	k8xC	a
vysoká	vysoký	k2eAgFnSc1d1	vysoká
koncentrace	koncentrace	k1gFnSc1	koncentrace
lipidů	lipid	k1gInPc2	lipid
snižujících	snižující	k2eAgInPc2d1	snižující
prostupnost	prostupnost	k1gFnSc4	prostupnost
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
léčby	léčba	k1gFnSc2	léčba
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
stanovit	stanovit	k5eAaPmF	stanovit
citlivost	citlivost	k1gFnSc4	citlivost
mykobakteriálního	mykobakteriální	k2eAgInSc2d1	mykobakteriální
kmene	kmen	k1gInSc2	kmen
k	k	k7c3	k
základním	základní	k2eAgMnPc3d1	základní
antituberkulotikům	antituberkulotik	k1gMnPc3	antituberkulotik
(	(	kIx(	(
<g/>
izoniazid	izoniazid	k1gInSc1	izoniazid
<g/>
,	,	kIx,	,
streptomycin	streptomycin	k1gInSc1	streptomycin
<g/>
,	,	kIx,	,
rifampicin	rifampicin	k2eAgInSc1d1	rifampicin
<g/>
,	,	kIx,	,
etambutol	etambutol	k1gInSc1	etambutol
a	a	k8xC	a
pyrazinamid	pyrazinamid	k1gInSc1	pyrazinamid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Léčebný	léčebný	k2eAgInSc1d1	léčebný
režim	režim	k1gInSc1	režim
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
fáze	fáze	k1gFnPc4	fáze
<g/>
:	:	kIx,	:
iniciační	iniciační	k2eAgFnSc4d1	iniciační
(	(	kIx(	(
<g/>
běžně	běžně	k6eAd1	běžně
2	[number]	k4	2
měsíce	měsíc	k1gInPc4	měsíc
a	a	k8xC	a
za	za	k7c4	za
hospitalizace	hospitalizace	k1gFnPc4	hospitalizace
<g/>
)	)	kIx)	)
a	a	k8xC	a
pokračovací	pokračovací	k2eAgFnSc7d1	pokračovací
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
ambulantní	ambulantní	k2eAgInSc4d1	ambulantní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
následuje	následovat	k5eAaImIp3nS	následovat
bezprostředně	bezprostředně	k6eAd1	bezprostředně
poté	poté	k6eAd1	poté
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
latentní	latentní	k2eAgFnSc2d1	latentní
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
většinou	většina	k1gFnSc7	většina
postačí	postačit	k5eAaPmIp3nS	postačit
používat	používat	k5eAaImF	používat
jeden	jeden	k4xCgInSc1	jeden
typ	typ	k1gInSc1	typ
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aktivní	aktivní	k2eAgFnSc4d1	aktivní
formu	forma	k1gFnSc4	forma
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgNnSc1d2	vhodnější
léčit	léčit	k5eAaImF	léčit
kombinací	kombinace	k1gFnSc7	kombinace
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
u	u	k7c2	u
bakterie	bakterie	k1gFnSc2	bakterie
nevyvinula	vyvinout	k5eNaPmAgFnS	vyvinout
antibiotická	antibiotický	k2eAgFnSc1d1	antibiotická
rezistence	rezistence	k1gFnSc1	rezistence
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
latentní	latentní	k2eAgFnSc7d1	latentní
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
probíhá	probíhat	k5eAaImIp3nS	probíhat
kvůli	kvůli	k7c3	kvůli
prevenci	prevence	k1gFnSc3	prevence
přechodu	přechod	k1gInSc2	přechod
do	do	k7c2	do
aktivního	aktivní	k2eAgNnSc2d1	aktivní
stádia	stádium	k1gNnSc2	stádium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Antibiotická	antibiotický	k2eAgFnSc1d1	antibiotická
rezistence	rezistence	k1gFnSc1	rezistence
===	===	k?	===
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
rezistentní	rezistentní	k2eAgFnSc2d1	rezistentní
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Primární	primární	k2eAgFnSc1d1	primární
rezistence	rezistence	k1gFnSc1	rezistence
vzniká	vznikat	k5eAaImIp3nS	vznikat
nakažením	nakažení	k1gNnSc7	nakažení
rezistentním	rezistentní	k2eAgInSc7d1	rezistentní
typem	typ	k1gInSc7	typ
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Sekundární	sekundární	k2eAgFnSc1d1	sekundární
(	(	kIx(	(
<g/>
získaná	získaný	k2eAgFnSc1d1	získaná
<g/>
)	)	kIx)	)
rezistence	rezistence	k1gFnSc1	rezistence
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
nevhodné	vhodný	k2eNgFnSc6d1	nevhodná
léčbě	léčba	k1gFnSc6	léčba
nerezistentní	rezistentní	k2eNgFnSc2d1	rezistentní
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
požívání	požívání	k1gNnSc6	požívání
nekvalitních	kvalitní	k2eNgInPc2d1	nekvalitní
léků	lék	k1gInPc2	lék
nebo	nebo	k8xC	nebo
porušování	porušování	k1gNnSc2	porušování
předepsaného	předepsaný	k2eAgInSc2d1	předepsaný
režimu	režim	k1gInSc2	režim
dávkování	dávkování	k1gNnSc2	dávkování
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
totiž	totiž	k9	totiž
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
evolučně	evolučně	k6eAd1	evolučně
řízený	řízený	k2eAgInSc4d1	řízený
zisk	zisk	k1gInSc4	zisk
rezistence	rezistence	k1gFnSc2	rezistence
<g/>
.	.	kIx.	.
</s>
<s>
Rezistentní	rezistentní	k2eAgFnSc1d1	rezistentní
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
je	být	k5eAaImIp3nS	být
problémem	problém	k1gInSc7	problém
mnoha	mnoho	k4c2	mnoho
rozvojových	rozvojový	k2eAgFnPc2d1	rozvojová
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
její	její	k3xOp3gFnSc1	její
léčba	léčba	k1gFnSc1	léčba
trvá	trvat	k5eAaImIp3nS	trvat
déle	dlouho	k6eAd2	dlouho
a	a	k8xC	a
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
dražší	drahý	k2eAgInPc4d2	dražší
léky	lék	k1gInPc4	lék
<g/>
.	.	kIx.	.
</s>
<s>
Multirezistentní	Multirezistentní	k2eAgFnSc1d1	Multirezistentní
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
(	(	kIx(	(
<g/>
MDR-TB	MDR-TB	k1gFnSc1	MDR-TB
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rezistentní	rezistentní	k2eAgMnSc1d1	rezistentní
proti	proti	k7c3	proti
dvěma	dva	k4xCgInPc3	dva
nejúčinnějším	účinný	k2eAgInPc3d3	nejúčinnější
prvořadým	prvořadý	k2eAgInPc3d1	prvořadý
lékům	lék	k1gInPc3	lék
<g/>
:	:	kIx,	:
rifampicinu	rifampicin	k2eAgFnSc4d1	rifampicin
a	a	k8xC	a
isoniazidu	isoniazida	k1gFnSc4	isoniazida
<g/>
.	.	kIx.	.
</s>
<s>
Extenzivně	extenzivně	k6eAd1	extenzivně
rezistentní	rezistentní	k2eAgFnSc1d1	rezistentní
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
(	(	kIx(	(
<g/>
XDR-TB	XDR-TB	k1gFnSc1	XDR-TB
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
rezistentní	rezistentní	k2eAgFnSc1d1	rezistentní
proti	proti	k7c3	proti
jakémukoli	jakýkoli	k3yIgInSc3	jakýkoli
fluorochinolonu	fluorochinolon	k1gInSc3	fluorochinolon
a	a	k8xC	a
minimálně	minimálně	k6eAd1	minimálně
jednomu	jeden	k4xCgNnSc3	jeden
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
druhořadých	druhořadý	k2eAgNnPc2d1	druhořadé
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
(	(	kIx(	(
<g/>
kanamicin	kanamicin	k2eAgInSc1d1	kanamicin
<g/>
,	,	kIx,	,
kapreomycin	kapreomycin	k1gInSc1	kapreomycin
<g/>
,	,	kIx,	,
amikacin	amikacin	k1gInSc1	amikacin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
asi	asi	k9	asi
4	[number]	k4	4
%	%	kIx~	%
případů	případ	k1gInPc2	případ
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
je	být	k5eAaImIp3nS	být
multirezistentních	multirezistentní	k2eAgNnPc2d1	multirezistentní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
Východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
takové	takový	k3xDgInPc1	takový
případy	případ	k1gInPc1	případ
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
až	až	k9	až
6	[number]	k4	6
<g/>
×	×	k?	×
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
upadající	upadající	k2eAgFnSc1d1	upadající
kvalita	kvalita	k1gFnSc1	kvalita
léčby	léčba	k1gFnSc2	léčba
(	(	kIx(	(
<g/>
pacienti	pacient	k1gMnPc1	pacient
jsou	být	k5eAaImIp3nP	být
léčeni	léčit	k5eAaImNgMnP	léčit
pouze	pouze	k6eAd1	pouze
jedním	jeden	k4xCgInSc7	jeden
nebo	nebo	k8xC	nebo
dvěma	dva	k4xCgInPc7	dva
léky	lék	k1gInPc7	lék
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
5	[number]	k4	5
%	%	kIx~	%
případů	případ	k1gInPc2	případ
multirezistentní	multirezistentní	k2eAgFnSc2d1	multirezistentní
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
je	být	k5eAaImIp3nS	být
extenzivně	extenzivně	k6eAd1	extenzivně
rezistentní	rezistentní	k2eAgMnSc1d1	rezistentní
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
léčby	léčba	k1gFnSc2	léčba
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prognóza	prognóza	k1gFnSc1	prognóza
==	==	k?	==
</s>
</p>
<p>
<s>
Přechod	přechod	k1gInSc1	přechod
od	od	k7c2	od
nákazy	nákaza	k1gFnSc2	nákaza
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
k	k	k7c3	k
onemocnění	onemocnění	k1gNnSc3	onemocnění
nastává	nastávat	k5eAaImIp3nS	nastávat
<g/>
,	,	kIx,	,
když	když	k8xS	když
mykobakterie	mykobakterie	k1gFnPc1	mykobakterie
překonají	překonat	k5eAaPmIp3nP	překonat
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
a	a	k8xC	a
začnou	začít	k5eAaPmIp3nP	začít
se	se	k3xPyFc4	se
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
primární	primární	k2eAgFnSc6d1	primární
tuberkulóze	tuberkulóza	k1gFnSc6	tuberkulóza
–	–	k?	–
která	který	k3yIgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
%	%	kIx~	%
případů	případ	k1gInPc2	případ
–	–	k?	–
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stává	stávat	k5eAaImIp3nS	stávat
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
nakažení	nakažení	k1gNnSc6	nakažení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
se	se	k3xPyFc4	se
ale	ale	k9	ale
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
latentní	latentní	k2eAgFnSc1d1	latentní
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgInPc4	žádný
symptomy	symptom	k1gInPc4	symptom
<g/>
.	.	kIx.	.
</s>
<s>
Latentní	latentní	k2eAgFnSc1d1	latentní
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
pak	pak	k6eAd1	pak
přejde	přejít	k5eAaPmIp3nS	přejít
v	v	k7c4	v
aktivní	aktivní	k2eAgFnSc4d1	aktivní
formu	forma	k1gFnSc4	forma
ve	v	k7c6	v
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
23	[number]	k4	23
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
často	často	k6eAd1	často
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
po	po	k7c6	po
nákaze	nákaza	k1gFnSc6	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
přechodu	přechod	k1gInSc2	přechod
do	do	k7c2	do
aktivní	aktivní	k2eAgFnSc2d1	aktivní
formy	forma	k1gFnSc2	forma
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
s	s	k7c7	s
imunosupresí	imunosuprese	k1gFnSc7	imunosuprese
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
např.	např.	kA	např.
infekcí	infekce	k1gFnPc2	infekce
HIV	HIV	kA	HIV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
u	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
==	==	k?	==
</s>
</p>
<p>
<s>
Tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
skotu	skot	k1gInSc2	skot
(	(	kIx(	(
<g/>
bovinní	bovinní	k2eAgFnSc4d1	bovinní
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
)	)	kIx)	)
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
Mycobacterium	Mycobacterium	k1gNnSc4	Mycobacterium
bovis	bovis	k1gFnSc2	bovis
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
potlačena	potlačit	k5eAaPmNgNnP	potlačit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
dokončila	dokončit	k5eAaPmAgFnS	dokončit
eradikaci	eradikace	k1gFnSc4	eradikace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
přírodnímu	přírodní	k2eAgInSc3d1	přírodní
rezervoáru	rezervoár	k1gInSc3	rezervoár
(	(	kIx(	(
<g/>
kterým	který	k3yRgNnSc7	který
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
possumové	possumové	k2eAgInSc2d1	possumové
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
,	,	kIx,	,
jezevci	jezevec	k1gMnPc1	jezevec
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
nebo	nebo	k8xC	nebo
bizoni	bizon	k1gMnPc1	bizon
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
eradikace	eradikace	k1gFnSc2	eradikace
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
incidence	incidence	k1gFnSc1	incidence
bovinní	bovinní	k2eAgFnSc2d1	bovinní
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
stále	stále	k6eAd1	stále
roste	růst	k5eAaImIp3nS	růst
i	i	k9	i
přes	přes	k7c4	přes
probíhající	probíhající	k2eAgFnSc4d1	probíhající
intenzivní	intenzivní	k2eAgFnSc4d1	intenzivní
eradikaci	eradikace	k1gFnSc4	eradikace
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
šíření	šíření	k1gNnSc2	šíření
nemoci	nemoc	k1gFnSc2	nemoc
jezevci	jezevec	k1gMnSc3	jezevec
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
možných	možný	k2eAgNnPc2d1	možné
vysvětlení	vysvětlení	k1gNnPc2	vysvětlení
i	i	k8xC	i
infekce	infekce	k1gFnSc2	infekce
skotu	skot	k1gInSc2	skot
motolicí	motolice	k1gFnSc7	motolice
jaterní	jaterní	k2eAgFnSc2d1	jaterní
(	(	kIx(	(
<g/>
Fasciola	Fasciola	k1gFnSc1	Fasciola
hepatica	hepatica	k1gMnSc1	hepatica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Souvislost	souvislost	k1gFnSc1	souvislost
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
infekce	infekce	k1gFnSc2	infekce
motolicí	motolice	k1gFnSc7	motolice
jaterní	jaterní	k2eAgFnSc7d1	jaterní
u	u	k7c2	u
testovaného	testovaný	k2eAgNnSc2d1	testované
zvířete	zvíře	k1gNnSc2	zvíře
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
falešně	falešně	k6eAd1	falešně
negativní	negativní	k2eAgInSc4d1	negativní
výsledek	výsledek	k1gInSc4	výsledek
při	při	k7c6	při
tuberkulinovém	tuberkulinový	k2eAgInSc6d1	tuberkulinový
testu	test	k1gInSc6	test
<g/>
.	.	kIx.	.
</s>
<s>
Motoličnatost	Motoličnatost	k1gFnSc1	Motoličnatost
takto	takto	k6eAd1	takto
může	moct	k5eAaImIp3nS	moct
maskovat	maskovat	k5eAaBmF	maskovat
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
při	při	k7c6	při
diagnostice	diagnostika	k1gFnSc6	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
bovinní	bovinní	k2eAgFnSc1d1	bovinní
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
stále	stále	k6eAd1	stále
běžným	běžný	k2eAgNnSc7d1	běžné
onemocněním	onemocnění	k1gNnSc7	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nakažení	nakažení	k1gNnSc6	nakažení
trvá	trvat	k5eAaImIp3nS	trvat
většinou	většinou	k6eAd1	většinou
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
bovinní	bovinní	k2eAgFnSc1d1	bovinní
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
začne	začít	k5eAaPmIp3nS	začít
projevovat	projevovat	k5eAaImF	projevovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
symptomy	symptom	k1gInPc7	symptom
patří	patřit	k5eAaImIp3nP	patřit
např.	např.	kA	např.
vyhublost	vyhublost	k1gFnSc4	vyhublost
<g/>
,	,	kIx,	,
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
slabost	slabost	k1gFnSc1	slabost
a	a	k8xC	a
nechuť	nechuť	k1gFnSc1	nechuť
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
.	.	kIx.	.
<g/>
Původcem	původce	k1gMnSc7	původce
ptačí	ptačí	k2eAgFnSc2d1	ptačí
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
je	být	k5eAaImIp3nS	být
M.	M.	kA	M.
avium	avium	k1gInSc1	avium
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
avium	avium	k1gInSc1	avium
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
prasat	prase	k1gNnPc2	prase
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
M.	M.	kA	M.
avium	avium	k1gNnSc1	avium
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
hominissuis	hominissuis	k1gInSc1	hominissuis
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
prasečí	prasečí	k2eAgFnSc2d1	prasečí
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
kontaminované	kontaminovaný	k2eAgNnSc4d1	kontaminované
vnější	vnější	k2eAgNnSc4d1	vnější
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC	a
volně	volně	k6eAd1	volně
žijící	žijící	k2eAgMnPc1d1	žijící
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
<g/>
Paratuberkulózu	Paratuberkulóza	k1gFnSc4	Paratuberkulóza
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
M.	M.	kA	M.
avium	avium	k1gInSc1	avium
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
paratuberculosis	paratuberculosis	k1gInSc1	paratuberculosis
<g/>
.	.	kIx.	.
</s>
<s>
Paratuberkulóza	Paratuberkulóza	k1gFnSc1	Paratuberkulóza
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
běžným	běžný	k2eAgNnPc3d1	běžné
onemocněním	onemocnění	k1gNnPc3	onemocnění
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
od	od	k7c2	od
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
objevovat	objevovat	k5eAaImF	objevovat
i	i	k9	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
paratuberkulózy	paratuberkulóza	k1gFnSc2	paratuberkulóza
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
6	[number]	k4	6
měsíci	měsíc	k1gInPc7	měsíc
a	a	k8xC	a
15	[number]	k4	15
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
příznakem	příznak	k1gInSc7	příznak
je	být	k5eAaImIp3nS	být
chronické	chronický	k2eAgNnSc1d1	chronické
hubnutí	hubnutí	k1gNnSc1	hubnutí
<g/>
,	,	kIx,	,
u	u	k7c2	u
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
%	%	kIx~	%
zvířat	zvíře	k1gNnPc2	zvíře
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
průjem	průjem	k1gInSc1	průjem
nazelenalé	nazelenalý	k2eAgFnSc2d1	nazelenalá
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
vodnaté	vodnatý	k2eAgFnSc2d1	vodnatá
konzistence	konzistence	k1gFnSc2	konzistence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Tuberculosis	Tuberculosis	k1gFnSc2	Tuberculosis
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Abreografie	Abreografie	k1gFnSc1	Abreografie
</s>
</p>
<p>
<s>
Kalmetizace	kalmetizace	k1gFnSc1	kalmetizace
</s>
</p>
<p>
<s>
Ptačí	ptačí	k2eAgFnSc1d1	ptačí
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
</s>
</p>
<p>
<s>
Tuberkulin	tuberkulin	k1gInSc1	tuberkulin
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
ve	v	k7c6	v
WikislovníkuČeskyOčkování	WikislovníkuČeskyOčkování	k1gNnSc6	WikislovníkuČeskyOčkování
proti	proti	k7c3	proti
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
na	na	k7c4	na
Vakciny	Vakcin	k1gInPc4	Vakcin
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
</s>
</p>
<p>
<s>
Kubín	Kubín	k1gMnSc1	Kubín
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
–	–	k?	–
prevence	prevence	k1gFnSc1	prevence
a	a	k8xC	a
léčba	léčba	k1gFnSc1	léčba
<g/>
.	.	kIx.	.
na	na	k7c6	na
Zdrav	zdravit	k5eAaImRp2nS	zdravit
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Světu	svět	k1gInSc3	svět
hrozí	hrozit	k5eAaImIp3nS	hrozit
nová	nový	k2eAgFnSc1d1	nová
forma	forma	k1gFnSc1	forma
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
</s>
</p>
<p>
<s>
Potrepčiaková	Potrepčiakový	k2eAgNnPc4d1	Potrepčiakový
S.	S.	kA	S.
<g/>
,	,	kIx,	,
Skřičková	Skřičková	k1gFnSc1	Skřičková
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Practicus	Practicus	k1gInSc1	Practicus
2008	[number]	k4	2008
<g/>
;	;	kIx,	;
4	[number]	k4	4
<g/>
:	:	kIx,	:
24-29	[number]	k4	24-29
</s>
</p>
<p>
<s>
Bártů	Bárta	k1gMnPc2	Bárta
V.	V.	kA	V.
<g/>
:	:	kIx,	:
Tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
–	–	k?	–
infekční	infekční	k2eAgFnSc1d1	infekční
choroba	choroba	k1gFnSc1	choroba
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Med	med	k1gInSc1	med
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Praxi	praxe	k1gFnSc4	praxe
2008	[number]	k4	2008
<g/>
;	;	kIx,	;
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
245-248	[number]	k4	245-248
</s>
</p>
<p>
<s>
Tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
dospělých	dospělí	k1gMnPc2	dospělí
na	na	k7c6	na
webu	web	k1gInSc6	web
České	český	k2eAgFnSc2d1	Česká
pneumologické	pneumologický	k2eAgFnSc2d1	pneumologická
a	a	k8xC	a
ftizeologické	ftizeologický	k2eAgFnSc2d1	ftizeologická
společnosti	společnost	k1gFnSc2	společnost
při	při	k7c6	při
České	český	k2eAgFnSc6d1	Česká
lékařské	lékařský	k2eAgFnSc6d1	lékařská
společnosti	společnost	k1gFnSc6	společnost
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
PurkyněAnglickyStop	PurkyněAnglickyStop	k1gInSc1	PurkyněAnglickyStop
TB	TB	kA	TB
Partnership	Partnership	k1gInSc1	Partnership
</s>
</p>
<p>
<s>
CDC	CDC	kA	CDC
–	–	k?	–
Tuberculosis	Tuberculosis	k1gInSc1	Tuberculosis
</s>
</p>
<p>
<s>
HPA	HPA	kA	HPA
–	–	k?	–
Tuberculosis	Tuberculosis	k1gInSc1	Tuberculosis
</s>
</p>
<p>
<s>
The	The	k?	The
Tuberculosis	Tuberculosis	k1gInSc4	Tuberculosis
Coalition	Coalition	k1gInSc4	Coalition
for	forum	k1gNnPc2	forum
Technical	Technical	k1gMnSc2	Technical
Assistance	Assistanec	k1gMnSc2	Assistanec
</s>
</p>
<p>
<s>
Tuberculosis	Tuberculosis	k1gFnSc1	Tuberculosis
na	na	k7c4	na
MedicineNet	MedicineNet	k1gInSc4	MedicineNet
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Tuberculosis	Tuberculosis	k1gFnSc1	Tuberculosis
2007	[number]	k4	2007
–	–	k?	–
A	a	k8xC	a
Medical	Medical	k1gFnSc1	Medical
Textbook	Textbook	k1gInSc1	Textbook
</s>
</p>
