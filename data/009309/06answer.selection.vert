<s>
Tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
tuberculum	tuberculum	k1gInSc1	tuberculum
–	–	k?	–
hrbolek	hrbolek	k1gInSc1	hrbolek
<g/>
,	,	kIx,	,
nádorek	nádorek	k1gInSc1	nádorek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
TBC	TBC	kA	TBC
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
souchotiny	souchotiny	k1gFnPc4	souchotiny
či	či	k8xC	či
úbytě	úbytě	k1gFnPc4	úbytě
(	(	kIx(	(
<g/>
oubytě	oubytě	k6eAd1	oubytě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
infekční	infekční	k2eAgNnSc4d1	infekční
onemocnění	onemocnění	k1gNnSc4	onemocnění
způsobené	způsobený	k2eAgNnSc4d1	způsobené
bakteriemi	bakterie	k1gFnPc7	bakterie
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
tuberculosis	tuberculosis	k1gFnPc2	tuberculosis
komplex	komplex	k1gInSc4	komplex
s	s	k7c7	s
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
zástupcem	zástupce	k1gMnSc7	zástupce
M.	M.	kA	M.
tuberculosis	tuberculosis	k1gFnPc4	tuberculosis
<g/>
.	.	kIx.	.
</s>
