<s>
Vileď	Viledit	k5eAaPmRp2nS
</s>
<s>
Vileď	Viledit	k5eAaPmRp2nS
Řeka	řeka	k1gFnSc1
VileďZákladní	VileďZákladný	k2eAgMnPc1d1
informace	informace	k1gFnSc1
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
321	#num#	k4
km	km	kA
Plocha	plocha	k1gFnSc1
povodí	povodit	k5eAaPmIp3nS
</s>
<s>
5610	#num#	k4
km²	km²	k?
Průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
</s>
<s>
42,7	42,7	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Ústí	ústí	k1gNnSc2
</s>
<s>
do	do	k7c2
Vyčegdy	Vyčegda	k1gFnSc2
61	#num#	k4
<g/>
°	°	k?
<g/>
20	#num#	k4
<g/>
′	′	k?
<g/>
24	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
47	#num#	k4
<g/>
°	°	k?
<g/>
21	#num#	k4
<g/>
′	′	k?
<g/>
21	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Protéká	protékat	k5eAaImIp3nS
</s>
<s>
Rusko	Rusko	k1gNnSc1
Rusko	Rusko	k1gNnSc1
(	(	kIx(
<g/>
Archangelská	archangelský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Úmoří	úmoří	k1gNnSc1
<g/>
,	,	kIx,
povodí	povodí	k1gNnSc1
</s>
<s>
Severní	severní	k2eAgInSc1d1
ledový	ledový	k2eAgInSc1d1
oceán	oceán	k1gInSc1
<g/>
,	,	kIx,
Bílé	bílý	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc1d1
Dvina	Dvina	k1gFnSc1
<g/>
,	,	kIx,
Vyčegda	Vyčegda	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vileď	Vileď	k1gFnSc1
nebo	nebo	k8xC
Viljať	Viljať	k1gFnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
В	В	k?
nebo	nebo	k8xC
В	В	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
Archangelské	archangelský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
dlouhá	dlouhý	k2eAgFnSc1d1
321	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plocha	plocha	k1gFnSc1
povodí	povodí	k1gNnSc2
měří	měřit	k5eAaImIp3nS
5610	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
toku	tok	k1gInSc2
</s>
<s>
Ústí	ústí	k1gNnSc1
zprava	zprava	k6eAd1
do	do	k7c2
Vyčegdy	Vyčegda	k1gFnSc2
(	(	kIx(
<g/>
povodí	povodí	k1gNnSc2
Severní	severní	k2eAgFnSc2d1
Dviny	Dvina	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Přítoky	přítok	k1gInPc1
</s>
<s>
Největším	veliký	k2eAgInSc7d3
přítokem	přítok	k1gInSc7
je	být	k5eAaImIp3nS
Velká	velký	k2eAgFnSc1d1
Ochta	Ochta	k1gFnSc1
zleva	zleva	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
režim	režim	k1gInSc1
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1
roční	roční	k2eAgInSc1d1
průtok	průtok	k1gInSc1
vody	voda	k1gFnSc2
u	u	k7c2
osady	osada	k1gFnSc2
Inajevskaja	Inajevskaj	k1gInSc2
činí	činit	k5eAaImIp3nS
42,7	42,7	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s.	s.	k?
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Nepravidelná	pravidelný	k2eNgFnSc1d1
vodní	vodní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
je	být	k5eAaImIp3nS
provozována	provozovat	k5eAaImNgFnS
k	k	k7c3
ústí	ústí	k1gNnSc3
přítoku	přítok	k1gInSc2
Naryčug	Naryčuga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgFnP
použity	použit	k2eAgFnPc1d1
informace	informace	k1gFnPc1
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
encyklopedie	encyklopedie	k1gFnSc2
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
В	В	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vileď	Vileď	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
