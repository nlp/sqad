<s>
Severoatlantická	severoatlantický	k2eAgFnSc1d1	Severoatlantická
aliance	aliance	k1gFnSc1	aliance
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
North	North	k1gInSc1	North
Atlantic	Atlantice	k1gFnPc2	Atlantice
Treaty	Treata	k1gFnSc2	Treata
Organization	Organization	k1gInSc1	Organization
–	–	k?	–
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Organisation	Organisation	k1gInSc4	Organisation
du	du	k?	du
Traité	Traitý	k2eAgFnSc2d1	Traitý
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Atlantique	Atlantiqu	k1gMnSc2	Atlantiqu
Nord	Nord	k1gInSc1	Nord
–	–	k?	–
OTAN	OTAN	kA	OTAN
<g/>
;	;	kIx,	;
doslova	doslova	k6eAd1	doslova
Organizace	organizace	k1gFnSc1	organizace
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
euroatlantický	euroatlantický	k2eAgInSc1d1	euroatlantický
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
vojenský	vojenský	k2eAgInSc1d1	vojenský
pakt	pakt	k1gInSc1	pakt
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1949	[number]	k4	1949
podpisem	podpis	k1gInSc7	podpis
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Aliance	aliance	k1gFnSc1	aliance
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
zřízení	zřízení	k1gNnSc4	zřízení
Západoevropské	západoevropský	k2eAgFnSc2d1	západoevropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
dohody	dohoda	k1gFnSc2	dohoda
umožňující	umožňující	k2eAgFnSc2d1	umožňující
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
vstup	vstup	k1gInSc4	vstup
NSR	NSR	kA	NSR
do	do	k7c2	do
NATO	NATO	kA	NATO
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
založení	založení	k1gNnSc2	založení
tzv.	tzv.	kA	tzv.
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
sovětského	sovětský	k2eAgNnSc2d1	sovětské
impéria	impérium	k1gNnSc2	impérium
a	a	k8xC	a
zániku	zánik	k1gInSc2	zánik
NDR	NDR	kA	NDR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
rozpuštěna	rozpustit	k5eAaPmNgFnS	rozpustit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
počátku	počátek	k1gInSc6	počátek
byla	být	k5eAaImAgFnS	být
Severoatlantická	severoatlantický	k2eAgFnSc1d1	Severoatlantická
aliance	aliance	k1gFnSc1	aliance
jen	jen	k9	jen
o	o	k7c4	o
trochu	trocha	k1gFnSc4	trocha
více	hodně	k6eAd2	hodně
než	než	k8xS	než
politické	politický	k2eAgNnSc1d1	politické
sdružení	sdružení	k1gNnSc1	sdružení
<g/>
.	.	kIx.	.
</s>
<s>
Korejská	korejský	k2eAgFnSc1d1	Korejská
válka	válka	k1gFnSc1	válka
ale	ale	k8xC	ale
podnítila	podnítit	k5eAaPmAgFnS	podnítit
členské	členský	k2eAgInPc4d1	členský
státy	stát	k1gInPc4	stát
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
vojenské	vojenský	k2eAgFnSc2d1	vojenská
struktury	struktura	k1gFnSc2	struktura
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
dvou	dva	k4xCgInPc2	dva
amerických	americký	k2eAgMnPc2d1	americký
velitelů	velitel	k1gMnPc2	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Slovy	slovo	k1gNnPc7	slovo
prvního	první	k4xOgMnSc2	první
generálního	generální	k2eAgMnSc2d1	generální
tajemníka	tajemník	k1gMnSc2	tajemník
Hastingse	Hastingse	k1gFnSc2	Hastingse
Ismaye	Ismaye	k1gNnSc1	Ismaye
bylo	být	k5eAaImAgNnS	být
úkolem	úkol	k1gInSc7	úkol
NATO	nato	k6eAd1	nato
"	"	kIx"	"
<g/>
udržet	udržet	k5eAaPmF	udržet
Ameriku	Amerika	k1gFnSc4	Amerika
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
mimo	mimo	k7c4	mimo
západní	západní	k2eAgFnSc4d1	západní
Evropu	Evropa	k1gFnSc4	Evropa
a	a	k8xC	a
Německo	Německo	k1gNnSc4	Německo
při	při	k7c6	při
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
odešla	odejít	k5eAaPmAgFnS	odejít
Francie	Francie	k1gFnSc1	Francie
z	z	k7c2	z
vojenských	vojenský	k2eAgFnPc2d1	vojenská
struktur	struktura	k1gFnPc2	struktura
NATO	NATO	kA	NATO
kvůli	kvůli	k7c3	kvůli
snaze	snaha	k1gFnSc3	snaha
o	o	k7c4	o
udržení	udržení	k1gNnSc4	udržení
si	se	k3xPyFc3	se
vojenské	vojenský	k2eAgFnPc1d1	vojenská
nezávislosti	nezávislost	k1gFnPc1	nezávislost
na	na	k7c6	na
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
sídlo	sídlo	k1gNnSc1	sídlo
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
do	do	k7c2	do
Bruselu	Brusel	k1gInSc2	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
Aliance	aliance	k1gFnSc1	aliance
angažovala	angažovat	k5eAaBmAgFnS	angažovat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
vojenské	vojenský	k2eAgFnPc1d1	vojenská
operace	operace	k1gFnPc1	operace
NATO	NATO	kA	NATO
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1992	[number]	k4	1992
a	a	k8xC	a
1995	[number]	k4	1995
při	při	k7c6	při
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Bosně	Bosna	k1gFnSc6	Bosna
a	a	k8xC	a
Hercegovině	Hercegovina	k1gFnSc6	Hercegovina
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Aliance	aliance	k1gFnSc1	aliance
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
zlepšit	zlepšit	k5eAaPmF	zlepšit
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
východními	východní	k2eAgInPc7d1	východní
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
její	její	k3xOp3gNnSc4	její
rozšíření	rozšíření	k1gNnSc4	rozšíření
několika	několik	k4yIc7	několik
státy	stát	k1gInPc7	stát
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
a	a	k8xC	a
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
5	[number]	k4	5
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
základ	základ	k1gInSc4	základ
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
po	po	k7c6	po
teroristických	teroristický	k2eAgInPc6d1	teroristický
útocích	útok	k1gInPc6	útok
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
útok	útok	k1gInSc1	útok
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
útok	útok	k1gInSc4	útok
proti	proti	k7c3	proti
všem	všecek	k3xTgMnPc3	všecek
19	[number]	k4	19
členským	členský	k2eAgInPc3d1	členský
státům	stát	k1gInPc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
vede	vést	k5eAaImIp3nS	vést
Aliance	aliance	k1gFnSc1	aliance
činnost	činnost	k1gFnSc1	činnost
Mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
podpůrných	podpůrný	k2eAgFnPc2d1	podpůrná
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
výcvik	výcvik	k1gInSc4	výcvik
nové	nový	k2eAgFnSc2d1	nová
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
protipirátských	protipirátský	k2eAgFnPc2d1	protipirátská
operací	operace	k1gFnPc2	operace
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
bezletovou	bezletový	k2eAgFnSc4d1	bezletová
zónu	zóna	k1gFnSc4	zóna
nad	nad	k7c7	nad
Libyí	Libye	k1gFnSc7	Libye
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
rezolucí	rezoluce	k1gFnSc7	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
č.	č.	k?	č.
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
cíl	cíl	k1gInSc1	cíl
akce	akce	k1gFnSc2	akce
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
intervenční	intervenční	k2eAgFnSc6d1	intervenční
síly	síla	k1gFnPc1	síla
dostaly	dostat	k5eAaPmAgFnP	dostat
mandát	mandát	k1gInSc4	mandát
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
(	(	kIx(	(
<g/>
už	už	k9	už
mimo	mimo	k7c4	mimo
mandát	mandát	k1gInSc4	mandát
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
svržení	svržení	k1gNnSc3	svržení
dosavadního	dosavadní	k2eAgInSc2d1	dosavadní
vládnoucího	vládnoucí	k2eAgInSc2d1	vládnoucí
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
uvrhlo	uvrhnout	k5eAaPmAgNnS	uvrhnout
zemi	zem	k1gFnSc4	zem
do	do	k7c2	do
krvavého	krvavý	k2eAgInSc2d1	krvavý
chaosu	chaos	k1gInSc2	chaos
trvajícího	trvající	k2eAgInSc2d1	trvající
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c4	mezi
NATO	NATO	kA	NATO
a	a	k8xC	a
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
podepsána	podepsán	k2eAgFnSc1d1	podepsána
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
Berlín	Berlín	k1gInSc4	Berlín
plus	plus	k1gNnSc2	plus
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
využívat	využívat	k5eAaPmF	využívat
prostředky	prostředek	k1gInPc4	prostředek
i	i	k8xC	i
kapacity	kapacita	k1gFnPc4	kapacita
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
29	[number]	k4	29
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
nejnovějšími	nový	k2eAgInPc7d3	nejnovější
členy	člen	k1gInPc7	člen
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
předtím	předtím	k6eAd1	předtím
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
Albánie	Albánie	k1gFnSc2	Albánie
a	a	k8xC	a
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgInPc1d1	celkový
armádní	armádní	k2eAgInPc1d1	armádní
výdaje	výdaj	k1gInPc1	výdaj
všech	všecek	k3xTgInPc2	všecek
členů	člen	k1gInPc2	člen
NATO	NATO	kA	NATO
tvoří	tvořit	k5eAaImIp3nS	tvořit
přes	přes	k7c4	přes
70	[number]	k4	70
%	%	kIx~	%
celosvětových	celosvětový	k2eAgInPc2d1	celosvětový
armádních	armádní	k2eAgInPc2d1	armádní
výdajů	výdaj	k1gInPc2	výdaj
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgInPc1d1	samotný
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
zodpovídají	zodpovídat	k5eAaPmIp3nP	zodpovídat
za	za	k7c4	za
43	[number]	k4	43
%	%	kIx~	%
celosvětových	celosvětový	k2eAgInPc2d1	celosvětový
výdajů	výdaj	k1gInPc2	výdaj
na	na	k7c4	na
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Itálie	Itálie	k1gFnSc1	Itálie
za	za	k7c4	za
dalších	další	k2eAgNnPc2d1	další
15	[number]	k4	15
%	%	kIx~	%
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
sestřelilo	sestřelit	k5eAaPmAgNnS	sestřelit
turecké	turecký	k2eAgNnSc1d1	turecké
letectvo	letectvo	k1gNnSc4	letectvo
ruský	ruský	k2eAgInSc1d1	ruský
bitevní	bitevní	k2eAgInSc1d1	bitevní
letoun	letoun	k1gInSc1	letoun
Suchoj	Suchoj	k1gInSc1	Suchoj
Su-	Su-	k1gFnSc1	Su-
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
údajně	údajně	k6eAd1	údajně
narušil	narušit	k5eAaPmAgMnS	narušit
turecký	turecký	k2eAgInSc4d1	turecký
vzdušný	vzdušný	k2eAgInSc4d1	vzdušný
prostor	prostor	k1gInSc4	prostor
nad	nad	k7c4	nad
provincii	provincie	k1gFnSc4	provincie
Hatay	Hataa	k1gFnSc2	Hataa
<g/>
,	,	kIx,	,
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
vteřin	vteřina	k1gFnPc2	vteřina
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
první	první	k4xOgNnSc4	první
přímé	přímý	k2eAgNnSc4d1	přímé
střetnutí	střetnutí	k1gNnSc4	střetnutí
mezi	mezi	k7c7	mezi
ozbrojenými	ozbrojený	k2eAgFnPc7d1	ozbrojená
silami	síla	k1gFnPc7	síla
členského	členský	k2eAgInSc2d1	členský
státu	stát	k1gInSc2	stát
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
v	v	k7c6	v
padesátileté	padesátiletý	k2eAgFnSc6d1	padesátiletá
historii	historie	k1gFnSc6	historie
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Severoatlantická	severoatlantický	k2eAgFnSc1d1	Severoatlantická
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
předchůdce	předchůdce	k1gMnSc4	předchůdce
NATO	NATO	kA	NATO
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
Bruselský	bruselský	k2eAgInSc1d1	bruselský
pakt	pakt	k1gInSc1	pakt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1948	[number]	k4	1948
podepsaly	podepsat	k5eAaPmAgFnP	podepsat
Belgie	Belgie	k1gFnPc1	Belgie
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
proti	proti	k7c3	proti
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ale	ale	k8xC	ale
zapotřebí	zapotřebí	k6eAd1	zapotřebí
i	i	k9	i
účast	účast	k1gFnSc4	účast
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
nové	nový	k2eAgFnSc6d1	nová
vojenské	vojenský	k2eAgFnSc6d1	vojenská
alianci	aliance	k1gFnSc6	aliance
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
Severoatlantickou	severoatlantický	k2eAgFnSc4d1	Severoatlantická
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
ten	ten	k3xDgInSc4	ten
samý	samý	k3xTgInSc4	samý
cíl	cíl	k1gInSc4	cíl
<g/>
,	,	kIx,	,
ubránit	ubránit	k5eAaPmF	ubránit
se	se	k3xPyFc4	se
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
Varšavské	varšavský	k2eAgFnSc6d1	Varšavská
smlouvě	smlouva	k1gFnSc6	smlouva
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1949	[number]	k4	1949
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gMnSc1	D.C.
pěti	pět	k4xCc2	pět
státy	stát	k1gInPc1	stát
Bruselského	bruselský	k2eAgInSc2d1	bruselský
paktu	pakt	k1gInSc2	pakt
<g/>
,	,	kIx,	,
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
Kanadou	Kanada	k1gFnSc7	Kanada
<g/>
,	,	kIx,	,
Portugalskem	Portugalsko	k1gNnSc7	Portugalsko
<g/>
,	,	kIx,	,
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
,	,	kIx,	,
Norskem	Norsko	k1gNnSc7	Norsko
<g/>
,	,	kIx,	,
Dánskem	Dánsko	k1gNnSc7	Dánsko
a	a	k8xC	a
Islandem	Island	k1gInSc7	Island
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
podpisu	podpis	k1gInSc2	podpis
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
nazývá	nazývat	k5eAaImIp3nS	nazývat
Washingtonskou	washingtonský	k2eAgFnSc7d1	Washingtonská
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
14	[number]	k4	14
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
patří	patřit	k5eAaImIp3nS	patřit
článek	článek	k1gInSc1	článek
5	[number]	k4	5
<g/>
:	:	kIx,	:
Ve	v	k7c6	v
článku	článek	k1gInSc6	článek
IV	Iva	k1gFnPc2	Iva
Bruselského	bruselský	k2eAgInSc2d1	bruselský
paktu	pakt	k1gInSc2	pakt
se	se	k3xPyFc4	se
výslovně	výslovně	k6eAd1	výslovně
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
reakce	reakce	k1gFnSc1	reakce
států	stát	k1gInPc2	stát
na	na	k7c4	na
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
útok	útok	k1gInSc4	útok
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vojenského	vojenský	k2eAgInSc2d1	vojenský
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
článku	článek	k1gInSc2	článek
5	[number]	k4	5
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vojenský	vojenský	k2eAgInSc1d1	vojenský
útok	útok	k1gInSc1	útok
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
možných	možný	k2eAgFnPc2d1	možná
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
Severoatlantická	severoatlantický	k2eAgFnSc1d1	Severoatlantická
smlouva	smlouva	k1gFnSc1	smlouva
(	(	kIx(	(
<g/>
ve	v	k7c6	v
článku	článek	k1gInSc6	článek
6	[number]	k4	6
<g/>
)	)	kIx)	)
omezuje	omezovat	k5eAaImIp3nS	omezovat
pole	pole	k1gNnSc4	pole
působnosti	působnost	k1gFnSc2	působnost
nad	nad	k7c4	nad
obratník	obratník	k1gInSc4	obratník
Raka	rak	k1gMnSc2	rak
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
NATO	nato	k6eAd1	nato
nezasáhlo	zasáhnout	k5eNaPmAgNnS	zasáhnout
při	při	k7c6	při
válce	válka	k1gFnSc6	válka
o	o	k7c4	o
Falklandy	Falkland	k1gInPc4	Falkland
nebo	nebo	k8xC	nebo
při	při	k7c6	při
indické	indický	k2eAgFnSc6d1	indická
vojenské	vojenský	k2eAgFnSc6d1	vojenská
invazi	invaze	k1gFnSc6	invaze
do	do	k7c2	do
Portugalské	portugalský	k2eAgFnSc2d1	portugalská
Indie	Indie	k1gFnSc2	Indie
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
a	a	k8xC	a
po	po	k7c6	po
anexi	anexe	k1gFnSc6	anexe
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
Indií	Indie	k1gFnPc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Začátek	začátek	k1gInSc1	začátek
Korejské	korejský	k2eAgFnSc2d1	Korejská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
podnítil	podnítit	k5eAaPmAgInS	podnítit
NATO	NATO	kA	NATO
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
vojenských	vojenský	k2eAgFnPc2d1	vojenská
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
v	v	k7c6	v
Lisabonu	Lisabon	k1gInSc6	Lisabon
se	se	k3xPyFc4	se
dohodla	dohodnout	k5eAaPmAgFnS	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
zavede	zavést	k5eAaPmIp3nS	zavést
minimálně	minimálně	k6eAd1	minimálně
50	[number]	k4	50
divizí	divize	k1gFnPc2	divize
do	do	k7c2	do
konce	konec	k1gInSc2	konec
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
počet	počet	k1gInSc1	počet
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
na	na	k7c4	na
96	[number]	k4	96
<g/>
.	.	kIx.	.
</s>
<s>
Příští	příští	k2eAgInSc1d1	příští
rok	rok	k1gInSc1	rok
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
požadavek	požadavek	k1gInSc1	požadavek
snížen	snížit	k5eAaPmNgInS	snížit
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
35	[number]	k4	35
divizí	divize	k1gFnPc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lisabonu	Lisabon	k1gInSc6	Lisabon
byla	být	k5eAaImAgFnS	být
také	také	k9	také
zavedena	zaveden	k2eAgFnSc1d1	zavedena
pozice	pozice	k1gFnSc1	pozice
Generálního	generální	k2eAgMnSc2d1	generální
tajemníka	tajemník	k1gMnSc2	tajemník
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
stal	stát	k5eAaPmAgMnS	stát
baron	baron	k1gMnSc1	baron
Hastings	Hastingsa	k1gFnPc2	Hastingsa
Lionel	Lionel	k1gMnSc1	Lionel
Ismay	Ismaa	k1gFnSc2	Ismaa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1952	[number]	k4	1952
začalo	začít	k5eAaPmAgNnS	začít
první	první	k4xOgNnSc4	první
velké	velký	k2eAgNnSc4d1	velké
vojenské	vojenský	k2eAgNnSc4d1	vojenské
cvičení	cvičení	k1gNnSc4	cvičení
NATO	NATO	kA	NATO
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
:	:	kIx,	:
při	při	k7c6	při
operaci	operace	k1gFnSc6	operace
Mainbrace	Mainbrace	k1gFnSc2	Mainbrace
nacvičovalo	nacvičovat	k5eAaImAgNnS	nacvičovat
obranu	obrana	k1gFnSc4	obrana
Dánska	Dánsko	k1gNnSc2	Dánsko
a	a	k8xC	a
Norska	Norsko	k1gNnSc2	Norsko
přes	přes	k7c4	přes
160	[number]	k4	160
plavidel	plavidlo	k1gNnPc2	plavidlo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
velká	velký	k2eAgNnPc4d1	velké
cvičení	cvičení	k1gNnPc4	cvičení
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
operace	operace	k1gFnSc1	operace
Grand	grand	k1gMnSc1	grand
Slam	slam	k1gInSc1	slam
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
NATO	nato	k6eAd1	nato
poprvé	poprvé	k6eAd1	poprvé
nacvičovalo	nacvičovat	k5eAaImAgNnS	nacvičovat
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
operace	operace	k1gFnPc4	operace
Mariner	Marinra	k1gFnPc2	Marinra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
300	[number]	k4	300
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
1000	[number]	k4	1000
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
operace	operace	k1gFnSc1	operace
Italic	Italice	k1gFnPc2	Italice
Weld	Welda	k1gFnPc2	Welda
probíhající	probíhající	k2eAgFnSc1d1	probíhající
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
operace	operace	k1gFnSc1	operace
Grand	grand	k1gMnSc1	grand
Repulse	repulse	k1gFnSc1	repulse
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
nebo	nebo	k8xC	nebo
operace	operace	k1gFnSc1	operace
Monte	Mont	k1gInSc5	Mont
Carlo	Carlo	k1gNnSc1	Carlo
simulující	simulující	k2eAgFnPc1d1	simulující
podmínky	podmínka	k1gFnPc1	podmínka
s	s	k7c7	s
atomovými	atomový	k2eAgFnPc7d1	atomová
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
se	se	k3xPyFc4	se
členy	člen	k1gInPc7	člen
NATO	nato	k6eAd1	nato
staly	stát	k5eAaPmAgInP	stát
Řecko	Řecko	k1gNnSc4	Řecko
a	a	k8xC	a
Turecko	Turecko	k1gNnSc4	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
započalo	započnout	k5eAaPmAgNnS	započnout
budování	budování	k1gNnSc1	budování
sítě	síť	k1gFnSc2	síť
Gladio	Gladio	k1gNnSc1	Gladio
<g/>
,	,	kIx,	,
evropského	evropský	k2eAgInSc2d1	evropský
protikomunistického	protikomunistický	k2eAgInSc2d1	protikomunistický
odboje	odboj	k1gInSc2	odboj
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
Severoatlantickou	severoatlantický	k2eAgFnSc7d1	Severoatlantická
aliancí	aliance	k1gFnSc7	aliance
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
oznámil	oznámit	k5eAaPmAgInS	oznámit
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
připojit	připojit	k5eAaPmF	připojit
k	k	k7c3	k
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
mír	mír	k1gInSc1	mír
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Členské	členský	k2eAgFnPc1d1	členská
země	zem	k1gFnPc1	zem
se	se	k3xPyFc4	se
ale	ale	k9	ale
bály	bát	k5eAaImAgFnP	bát
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
chce	chtít	k5eAaImIp3nS	chtít
Alianci	aliance	k1gFnSc4	aliance
oslabit	oslabit	k5eAaPmF	oslabit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k8xS	tak
návrh	návrh	k1gInSc4	návrh
zamítly	zamítnout	k5eAaPmAgInP	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Přijetí	přijetí	k1gNnSc1	přijetí
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
do	do	k7c2	do
Aliance	aliance	k1gFnSc2	aliance
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1955	[number]	k4	1955
popsal	popsat	k5eAaPmAgMnS	popsat
Halvard	Halvard	k1gMnSc1	Halvard
Lange	Lang	k1gInSc2	Lang
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
za	za	k7c4	za
"	"	kIx"	"
<g/>
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
zlomový	zlomový	k2eAgInSc4d1	zlomový
bod	bod	k1gInSc4	bod
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
našeho	náš	k3xOp1gInSc2	náš
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Odpovědí	odpověď	k1gFnSc7	odpověď
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
událost	událost	k1gFnSc4	událost
bylo	být	k5eAaImAgNnS	být
vytvoření	vytvoření	k1gNnSc4	vytvoření
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1955	[number]	k4	1955
podepsaly	podepsat	k5eAaPmAgFnP	podepsat
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
Albánie	Albánie	k1gFnSc1	Albánie
a	a	k8xC	a
Německá	německý	k2eAgFnSc1d1	německá
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
NDR	NDR	kA	NDR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
NATO	NATO	kA	NATO
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
několik	několik	k4yIc1	několik
velkých	velký	k2eAgNnPc2d1	velké
vojenských	vojenský	k2eAgNnPc2d1	vojenské
cvičení	cvičení	k1gNnPc2	cvičení
<g/>
.	.	kIx.	.
</s>
<s>
Operací	operace	k1gFnPc2	operace
Counter	Counter	k1gMnSc1	Counter
Punch	Punch	k1gMnSc1	Punch
<g/>
,	,	kIx,	,
Strikeback	Strikeback	k1gMnSc1	Strikeback
a	a	k8xC	a
Deep	Deep	k1gMnSc1	Deep
Water	Watra	k1gFnPc2	Watra
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
250	[number]	k4	250
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
300	[number]	k4	300
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
1500	[number]	k4	1500
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Sjednocenost	sjednocenost	k1gFnSc1	sjednocenost
NATO	NATO	kA	NATO
byla	být	k5eAaImAgFnS	být
narušena	narušit	k5eAaPmNgFnS	narušit
během	během	k7c2	během
vlády	vláda	k1gFnSc2	vláda
francouzského	francouzský	k2eAgMnSc2d1	francouzský
prezidenta	prezident	k1gMnSc2	prezident
Charlese	Charles	k1gMnSc2	Charles
de	de	k?	de
Gaulla	Gaull	k1gMnSc2	Gaull
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
protestoval	protestovat	k5eAaBmAgMnS	protestovat
proti	proti	k7c3	proti
silnému	silný	k2eAgInSc3d1	silný
vlivu	vliv	k1gInSc3	vliv
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
na	na	k7c4	na
Alianci	aliance	k1gFnSc4	aliance
a	a	k8xC	a
blízkým	blízký	k2eAgInPc3d1	blízký
vztahům	vztah	k1gInPc3	vztah
mezi	mezi	k7c7	mezi
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1958	[number]	k4	1958
poslal	poslat	k5eAaPmAgMnS	poslat
prezidentovi	prezident	k1gMnSc3	prezident
Dwightu	Dwight	k1gMnSc3	Dwight
D.	D.	kA	D.
Eisenhowerovi	Eisenhower	k1gMnSc3	Eisenhower
a	a	k8xC	a
britskému	britský	k2eAgMnSc3d1	britský
premiérovi	premiér	k1gMnSc3	premiér
Haroldu	Harold	k1gMnSc3	Harold
Macmillanovi	Macmillan	k1gMnSc3	Macmillan
memorandum	memorandum	k1gNnSc4	memorandum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
žádal	žádat	k5eAaImAgMnS	žádat
vyzdvižení	vyzdvižení	k1gNnSc1	vyzdvižení
Francie	Francie	k1gFnSc2	Francie
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
mají	mít	k5eAaImIp3nP	mít
USA	USA	kA	USA
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc1	rozšíření
působnosti	působnost	k1gFnSc2	působnost
NATO	NATO	kA	NATO
na	na	k7c6	na
oblasti	oblast	k1gFnSc6	oblast
jako	jako	k8xS	jako
Francouzské	francouzský	k2eAgNnSc1d1	francouzské
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Francie	Francie	k1gFnSc1	Francie
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
pomoci	pomoct	k5eAaPmF	pomoct
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
povstalcům	povstalec	k1gMnPc3	povstalec
v	v	k7c6	v
alžírské	alžírský	k2eAgFnSc6d1	alžírská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Odpověď	odpověď	k1gFnSc1	odpověď
na	na	k7c4	na
memorandum	memorandum	k1gNnSc4	memorandum
de	de	k?	de
Gaulla	Gaulla	k1gFnSc1	Gaulla
neuspokojila	uspokojit	k5eNaPmAgFnS	uspokojit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
začal	začít	k5eAaPmAgInS	začít
pro	pro	k7c4	pro
Francii	Francie	k1gFnSc4	Francie
vytvářet	vytvářet	k5eAaImF	vytvářet
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
obrannou	obranný	k2eAgFnSc4d1	obranná
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
také	také	k9	také
Francii	Francie	k1gFnSc4	Francie
poskytnout	poskytnout	k5eAaPmF	poskytnout
možnost	možnost	k1gFnSc4	možnost
uzavřít	uzavřít	k5eAaPmF	uzavřít
mír	mír	k1gInSc4	mír
s	s	k7c7	s
Východním	východní	k2eAgInSc7d1	východní
blokem	blok	k1gInSc7	blok
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
zatažena	zatáhnout	k5eAaPmNgFnS	zatáhnout
do	do	k7c2	do
globální	globální	k2eAgFnSc2d1	globální
války	válka	k1gFnSc2	válka
mezi	mezi	k7c7	mezi
NATO	NATO	kA	NATO
a	a	k8xC	a
státy	stát	k1gInPc1	stát
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1959	[number]	k4	1959
Francie	Francie	k1gFnSc1	Francie
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
své	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgFnPc4	první
jednotky	jednotka	k1gFnPc4	jednotka
z	z	k7c2	z
velení	velení	k1gNnSc2	velení
NATO	NATO	kA	NATO
<g/>
;	;	kIx,	;
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
de	de	k?	de
Gaulle	Gaull	k1gMnSc4	Gaull
zakázal	zakázat	k5eAaPmAgInS	zakázat
umisťovat	umisťovat	k5eAaImF	umisťovat
cizí	cizí	k2eAgFnPc4d1	cizí
jaderné	jaderný	k2eAgFnPc4d1	jaderná
zbraně	zbraň	k1gFnPc4	zbraň
na	na	k7c4	na
francouzské	francouzský	k2eAgNnSc4d1	francouzské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
byly	být	k5eAaImAgInP	být
z	z	k7c2	z
vojenských	vojenský	k2eAgFnPc2d1	vojenská
struktur	struktura	k1gFnPc2	struktura
NATO	nato	k6eAd1	nato
staženy	stažen	k2eAgFnPc1d1	stažena
všechny	všechen	k3xTgFnPc1	všechen
francouzské	francouzský	k2eAgFnPc1d1	francouzská
jednotky	jednotka	k1gFnPc1	jednotka
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
vykázala	vykázat	k5eAaPmAgFnS	vykázat
všechny	všechen	k3xTgFnPc4	všechen
cizí	cizí	k2eAgFnPc4d1	cizí
jednotky	jednotka	k1gFnPc4	jednotka
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
předtím	předtím	k6eAd1	předtím
bylo	být	k5eAaImAgNnS	být
Vrchní	vrchní	k2eAgNnSc1d1	vrchní
velitelství	velitelství	k1gNnSc1	velitelství
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
SHAPE	SHAPE	kA	SHAPE
<g/>
)	)	kIx)	)
umístěno	umístit	k5eAaPmNgNnS	umístit
poblíž	poblíž	k6eAd1	poblíž
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
muselo	muset	k5eAaImAgNnS	muset
se	se	k3xPyFc4	se
přesunout	přesunout	k5eAaPmF	přesunout
do	do	k7c2	do
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
zůstala	zůstat	k5eAaPmAgFnS	zůstat
členem	člen	k1gMnSc7	člen
Aliance	aliance	k1gFnSc2	aliance
a	a	k8xC	a
zavázala	zavázat	k5eAaPmAgFnS	zavázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
útoku	útok	k1gInSc2	útok
komunistů	komunista	k1gMnPc2	komunista
použije	použít	k5eAaPmIp3nS	použít
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
Evropy	Evropa	k1gFnSc2	Evropa
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
francouzskými	francouzský	k2eAgMnPc7d1	francouzský
a	a	k8xC	a
americkými	americký	k2eAgMnPc7d1	americký
státními	státní	k2eAgMnPc7d1	státní
úředníky	úředník	k1gMnPc7	úředník
byly	být	k5eAaImAgFnP	být
uzavřeny	uzavřen	k2eAgFnPc4d1	uzavřena
tajné	tajný	k2eAgFnPc4d1	tajná
dohody	dohoda	k1gFnPc4	dohoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
popisovaly	popisovat	k5eAaImAgFnP	popisovat
návrat	návrat	k1gInSc4	návrat
francouzských	francouzský	k2eAgFnPc2d1	francouzská
jednotek	jednotka	k1gFnPc2	jednotka
do	do	k7c2	do
struktur	struktura	k1gFnPc2	struktura
NATO	NATO	kA	NATO
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
vypukne	vypuknout	k5eAaPmIp3nS	vypuknout
válka	válka	k1gFnSc1	válka
mezí	mez	k1gFnPc2	mez
Východem	východ	k1gInSc7	východ
a	a	k8xC	a
Západem	západ	k1gInSc7	západ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
berlínské	berlínský	k2eAgFnSc6d1	Berlínská
krizi	krize	k1gFnSc6	krize
a	a	k8xC	a
Karibské	karibský	k2eAgFnSc6d1	karibská
krizi	krize	k1gFnSc6	krize
si	se	k3xPyFc3	se
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
uvědomily	uvědomit	k5eAaPmAgFnP	uvědomit
nutnost	nutnost	k1gFnSc4	nutnost
institucionalizovat	institucionalizovat	k5eAaImF	institucionalizovat
proces	proces	k1gInSc4	proces
uvolňování	uvolňování	k1gNnSc2	uvolňování
napětí	napětí	k1gNnSc2	napětí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
NATO	nato	k6eAd1	nato
přijalo	přijmout	k5eAaPmAgNnS	přijmout
strategii	strategie	k1gFnSc4	strategie
vytyčující	vytyčující	k2eAgFnPc4d1	vytyčující
dvě	dva	k4xCgFnPc4	dva
funkce	funkce	k1gFnPc4	funkce
Aliance	aliance	k1gFnSc2	aliance
<g/>
:	:	kIx,	:
udržet	udržet	k5eAaPmF	udržet
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
prosazovat	prosazovat	k5eAaImF	prosazovat
politiku	politika	k1gFnSc4	politika
détente	détente	k2eAgFnSc4d1	détente
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
strategií	strategie	k1gFnSc7	strategie
se	se	k3xPyFc4	se
NATO	nato	k6eAd1	nato
řídilo	řídit	k5eAaImAgNnS	řídit
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
generálního	generální	k2eAgMnSc2d1	generální
tajemníka	tajemník	k1gMnSc2	tajemník
NATO	NATO	kA	NATO
italský	italský	k2eAgMnSc1d1	italský
politik	politik	k1gMnSc1	politik
Manlio	Manlio	k1gMnSc1	Manlio
Brosio	Brosio	k1gMnSc1	Brosio
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
prvních	první	k4xOgFnPc2	první
akcí	akce	k1gFnPc2	akce
bylo	být	k5eAaImAgNnS	být
založení	založení	k1gNnSc1	založení
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
obranné	obranný	k2eAgNnSc4d1	obranné
plánování	plánování	k1gNnSc4	plánování
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
podřízenými	podřízený	k1gMnPc7	podřízený
orgány	orgán	k1gInPc7	orgán
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
určeny	určen	k2eAgInPc1d1	určen
již	již	k6eAd1	již
existující	existující	k2eAgInSc1d1	existující
Vojenský	vojenský	k2eAgInSc1d1	vojenský
výbor	výbor	k1gInSc1	výbor
a	a	k8xC	a
nově	nově	k6eAd1	nově
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
Skupina	skupina	k1gFnSc1	skupina
pro	pro	k7c4	pro
jaderné	jaderný	k2eAgNnSc4d1	jaderné
plánování	plánování	k1gNnSc4	plánování
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
rozhodovací	rozhodovací	k2eAgFnPc1d1	rozhodovací
pravomoci	pravomoc	k1gFnPc1	pravomoc
ve	v	k7c6	v
vojenských	vojenský	k2eAgFnPc6d1	vojenská
záležitostech	záležitost	k1gFnPc6	záležitost
byly	být	k5eAaImAgFnP	být
převedeny	převést	k5eAaPmNgFnP	převést
na	na	k7c4	na
Výbor	výbor	k1gInSc4	výbor
pro	pro	k7c4	pro
obranné	obranný	k2eAgNnSc4d1	obranné
plánování	plánování	k1gNnSc4	plánování
a	a	k8xC	a
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
Francii	Francie	k1gFnSc4	Francie
umožněno	umožnit	k5eAaPmNgNnS	umožnit
setrvat	setrvat	k5eAaPmF	setrvat
v	v	k7c6	v
politické	politický	k2eAgFnSc6d1	politická
struktuře	struktura	k1gFnSc6	struktura
Aliance	aliance	k1gFnSc2	aliance
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
učinily	učinit	k5eAaImAgInP	učinit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
a	a	k8xC	a
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
zásadní	zásadní	k2eAgFnSc2d1	zásadní
změny	změna	k1gFnSc2	změna
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
lépe	dobře	k6eAd2	dobře
prosazovat	prosazovat	k5eAaImF	prosazovat
détente	détente	k2eAgNnSc4d1	détente
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
Nixon	Nixon	k1gMnSc1	Nixon
zavedl	zavést	k5eAaPmAgMnS	zavést
Nixonovu	Nixonův	k2eAgFnSc4d1	Nixonova
doktrínu	doktrína	k1gFnSc4	doktrína
a	a	k8xC	a
v	v	k7c6	v
Západním	západní	k2eAgNnSc6d1	západní
Německu	Německo	k1gNnSc6	Německo
začala	začít	k5eAaPmAgFnS	začít
platit	platit	k5eAaImF	platit
Ostpolitik	Ostpolitik	k1gMnSc1	Ostpolitik
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Hallsteinovu	Hallsteinův	k2eAgFnSc4d1	Hallsteinův
doktrínu	doktrína	k1gFnSc4	doktrína
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
stanovovala	stanovovat	k5eAaImAgFnS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
nebude	být	k5eNaImBp3nS	být
navazovat	navazovat	k5eAaImF	navazovat
ani	ani	k8xC	ani
udržovat	udržovat	k5eAaImF	udržovat
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
žádným	žádný	k3yNgInSc7	žádný
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
naváže	navázat	k5eAaPmIp3nS	navázat
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
NDR	NDR	kA	NDR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
učiněny	učiněn	k2eAgFnPc1d1	učiněna
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
jaderné	jaderný	k2eAgNnSc4d1	jaderné
odzbrojování	odzbrojování	k1gNnSc4	odzbrojování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1970	[number]	k4	1970
vešla	vejít	k5eAaPmAgFnS	vejít
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
Smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
nešíření	nešíření	k1gNnSc6	nešíření
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1972	[number]	k4	1972
Richard	Richard	k1gMnSc1	Richard
Nixon	Nixon	k1gMnSc1	Nixon
a	a	k8xC	a
Leonid	Leonid	k1gInSc1	Leonid
Iljič	Iljič	k1gMnSc1	Iljič
Brežněv	Brežněv	k1gMnSc1	Brežněv
podepsali	podepsat	k5eAaPmAgMnP	podepsat
smlouvy	smlouva	k1gFnPc4	smlouva
SALT	salto	k1gNnPc2	salto
I	i	k8xC	i
a	a	k8xC	a
ABM	ABM	kA	ABM
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
pak	pak	k9	pak
byla	být	k5eAaImAgFnS	být
Brežněvem	Brežněv	k1gInSc7	Brežněv
a	a	k8xC	a
novým	nový	k2eAgMnSc7d1	nový
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
Jimmym	Jimmymum	k1gNnPc2	Jimmymum
Carterem	Carter	k1gInSc7	Carter
podepsána	podepsán	k2eAgFnSc1d1	podepsána
smlouva	smlouva	k1gFnSc1	smlouva
SALT	salto	k1gNnPc2	salto
II	II	kA	II
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
přestala	přestat	k5eAaPmAgFnS	přestat
platit	platit	k5eAaImF	platit
SALT	salto	k1gNnPc2	salto
I.	I.	kA	I.
SALT	salto	k1gNnPc2	salto
II	II	kA	II
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
nevstoupila	vstoupit	k5eNaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
její	její	k3xOp3gFnSc4	její
ratifikaci	ratifikace	k1gFnSc4	ratifikace
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
Senát	senát	k1gInSc4	senát
USA	USA	kA	USA
kvůli	kvůli	k7c3	kvůli
sovětské	sovětský	k2eAgFnSc3d1	sovětská
agresi	agrese	k1gFnSc3	agrese
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
limity	limit	k1gInPc1	limit
ale	ale	k9	ale
oba	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
vcelku	vcelku	k6eAd1	vcelku
dodržovaly	dodržovat	k5eAaImAgInP	dodržovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
začal	začít	k5eAaPmAgInS	začít
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
rozmisťovat	rozmisťovat	k5eAaImF	rozmisťovat
jaderné	jaderný	k2eAgInPc4d1	jaderný
systémy	systém	k1gInPc4	systém
typu	typ	k1gInSc2	typ
SS-	SS-	k1gFnSc2	SS-
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Odpovědí	odpověď	k1gFnSc7	odpověď
NATO	nato	k6eAd1	nato
bylo	být	k5eAaImAgNnS	být
přijetí	přijetí	k1gNnSc1	přijetí
tzv.	tzv.	kA	tzv.
dvoukolejného	dvoukolejný	k2eAgNnSc2d1	dvoukolejné
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Aliance	aliance	k1gFnSc1	aliance
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
rozmístit	rozmístit	k5eAaPmF	rozmístit
téměř	téměř	k6eAd1	téměř
600	[number]	k4	600
odpalovacích	odpalovací	k2eAgNnPc2d1	odpalovací
zařízení	zařízení	k1gNnPc2	zařízení
řízených	řízený	k2eAgFnPc2d1	řízená
střel	střela	k1gFnPc2	střela
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
podporu	podpora	k1gFnSc4	podpora
kontroly	kontrola	k1gFnSc2	kontrola
zbrojení	zbrojení	k1gNnSc2	zbrojení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
novou	nový	k2eAgFnSc4d1	nová
Reaganovu	Reaganův	k2eAgFnSc4d1	Reaganova
doktrínu	doktrína	k1gFnSc4	doktrína
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
určila	určit	k5eAaPmAgFnS	určit
nový	nový	k2eAgInSc4d1	nový
směr	směr	k1gInSc4	směr
politiky	politika	k1gFnSc2	politika
Západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
požadoval	požadovat	k5eAaImAgMnS	požadovat
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
kampaň	kampaň	k1gFnSc4	kampaň
za	za	k7c4	za
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
,	,	kIx,	,
pouhá	pouhý	k2eAgFnSc1d1	pouhá
obrana	obrana	k1gFnSc1	obrana
proti	proti	k7c3	proti
komunismu	komunismus	k1gInSc3	komunismus
mu	on	k3xPp3gMnSc3	on
nepřipadala	připadat	k5eNaImAgFnS	připadat
dostatečná	dostatečný	k2eAgFnSc1d1	dostatečná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
se	s	k7c7	s
členem	člen	k1gInSc7	člen
Aliance	aliance	k1gFnSc2	aliance
stalo	stát	k5eAaPmAgNnS	stát
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začalo	začít	k5eAaPmAgNnS	začít
NATO	nato	k6eAd1	nato
tlačit	tlačit	k5eAaImF	tlačit
na	na	k7c4	na
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
ohledně	ohledně	k7c2	ohledně
dodržování	dodržování	k1gNnSc2	dodržování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgMnSc7	ten
Reagan	Reagan	k1gMnSc1	Reagan
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
označil	označit	k5eAaPmAgMnS	označit
komunistický	komunistický	k2eAgMnSc1d1	komunistický
svět	svět	k1gInSc4	svět
za	za	k7c4	za
"	"	kIx"	"
<g/>
říši	říše	k1gFnSc4	říše
zla	zlo	k1gNnSc2	zlo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
západních	západní	k2eAgNnPc2d1	západní
médií	médium	k1gNnPc2	médium
považovala	považovat	k5eAaImAgFnS	považovat
za	za	k7c2	za
přehnané	přehnaný	k2eAgFnSc2d1	přehnaná
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1983	[number]	k4	1983
sovětská	sovětský	k2eAgFnSc1d1	sovětská
protivzdušná	protivzdušný	k2eAgFnSc1d1	protivzdušná
obrana	obrana	k1gFnSc1	obrana
sestřelila	sestřelit	k5eAaPmAgFnS	sestřelit
bez	bez	k7c2	bez
varování	varování	k1gNnSc2	varování
jihokorejské	jihokorejský	k2eAgNnSc4d1	jihokorejské
civilní	civilní	k2eAgNnSc4d1	civilní
letadlo	letadlo	k1gNnSc4	letadlo
<g/>
;	;	kIx,	;
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
250	[number]	k4	250
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
tragédie	tragédie	k1gFnSc1	tragédie
podpořila	podpořit	k5eAaPmAgFnS	podpořit
Reaganovu	Reaganův	k2eAgFnSc4d1	Reaganova
politiku	politika	k1gFnSc4	politika
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
západní	západní	k2eAgFnSc2d1	západní
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
táhl	táhnout	k5eAaImAgMnS	táhnout
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
dohod	dohoda	k1gFnPc2	dohoda
o	o	k7c6	o
odzbrojování	odzbrojování	k1gNnSc6	odzbrojování
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
schůze	schůze	k1gFnPc1	schůze
Reagana	Reagan	k1gMnSc2	Reagan
a	a	k8xC	a
nového	nový	k2eAgMnSc2d1	nový
sovětského	sovětský	k2eAgMnSc2d1	sovětský
vůdce	vůdce	k1gMnSc2	vůdce
Michaila	Michail	k1gMnSc2	Michail
Gorbačova	Gorbačův	k2eAgFnSc1d1	Gorbačova
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
schůzi	schůze	k1gFnSc6	schůze
v	v	k7c6	v
Reykjavíku	Reykjavík	k1gInSc6	Reykjavík
se	se	k3xPyFc4	se
Gorbačov	Gorbačov	k1gInSc1	Gorbačov
snažil	snažit	k5eAaImAgInS	snažit
Reagana	Reagan	k1gMnSc4	Reagan
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ustoupil	ustoupit	k5eAaPmAgInS	ustoupit
od	od	k7c2	od
jím	jíst	k5eAaImIp1nS	jíst
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
Strategické	strategický	k2eAgFnSc2d1	strategická
obranné	obranný	k2eAgFnSc2d1	obranná
iniciativy	iniciativa	k1gFnSc2	iniciativa
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
on	on	k3xPp3gMnSc1	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
setkání	setkání	k1gNnSc6	setkání
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
pak	pak	k6eAd1	pak
Reagan	Reagan	k1gMnSc1	Reagan
a	a	k8xC	a
Gorbačov	Gorbačov	k1gInSc4	Gorbačov
podepsali	podepsat	k5eAaPmAgMnP	podepsat
Smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
likvidaci	likvidace	k1gFnSc6	likvidace
raket	raketa	k1gFnPc2	raketa
středního	střední	k2eAgInSc2d1	střední
a	a	k8xC	a
krátkého	krátký	k2eAgInSc2d1	krátký
doletu	dolet	k1gInSc2	dolet
(	(	kIx(	(
<g/>
INF	INF	kA	INF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zánik	zánik	k1gInSc1	zánik
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
odstranil	odstranit	k5eAaPmAgMnS	odstranit
de	de	k?	de
facto	facto	k1gNnSc4	facto
hlavního	hlavní	k2eAgMnSc2d1	hlavní
protivníka	protivník	k1gMnSc2	protivník
NATO	NATO	kA	NATO
a	a	k8xC	a
podnítil	podnítit	k5eAaPmAgMnS	podnítit
nové	nový	k2eAgFnPc4d1	nová
debaty	debata	k1gFnPc4	debata
o	o	k7c6	o
účelu	účel	k1gInSc6	účel
a	a	k8xC	a
povaze	povaha	k1gFnSc6	povaha
Aliance	aliance	k1gFnSc1	aliance
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Oslovením	oslovení	k1gNnSc7	oslovení
bývalých	bývalý	k2eAgMnPc2d1	bývalý
nepřátel	nepřítel	k1gMnPc2	nepřítel
a	a	k8xC	a
návrhem	návrh	k1gInSc7	návrh
na	na	k7c6	na
spolupráci	spolupráce	k1gFnSc6	spolupráce
k	k	k7c3	k
udržování	udržování	k1gNnSc3	udržování
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
NATO	nato	k6eAd1	nato
aktivně	aktivně	k6eAd1	aktivně
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
rozdělení	rozdělení	k1gNnSc2	rozdělení
Evropy	Evropa	k1gFnSc2	Evropa
na	na	k7c4	na
Východ	východ	k1gInSc4	východ
a	a	k8xC	a
Západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
<g/>
Tato	tento	k3xDgFnSc1	tento
<g />
.	.	kIx.	.
</s>
<s>
zásadní	zásadní	k2eAgFnSc1d1	zásadní
změna	změna	k1gFnSc1	změna
v	v	k7c6	v
přístupu	přístup	k1gInSc6	přístup
byla	být	k5eAaImAgNnP	být
zakotvena	zakotvit	k5eAaPmNgNnP	zakotvit
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
strategické	strategický	k2eAgFnSc6d1	strategická
koncepci	koncepce	k1gFnSc6	koncepce
NATO	NATO	kA	NATO
z	z	k7c2	z
listopadu	listopad	k1gInSc2	listopad
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přijala	přijmout	k5eAaPmAgFnS	přijmout
širší	široký	k2eAgInSc4d2	širší
bezpečnostní	bezpečnostní	k2eAgInSc4d1	bezpečnostní
rámec	rámec	k1gInSc4	rámec
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
období	období	k1gNnSc2	období
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
politicky	politicky	k6eAd1	politicky
udržovaly	udržovat	k5eAaImAgFnP	udržovat
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
upevňovaly	upevňovat	k5eAaImAgFnP	upevňovat
výjimečnou	výjimečný	k2eAgFnSc4d1	výjimečná
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
díky	dík	k1gInPc1	dík
které	který	k3yRgMnPc4	který
mohly	moct	k5eAaImAgInP	moct
diktovat	diktovat	k5eAaImF	diktovat
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemohly	moct	k5eNaImAgFnP	moct
nebrat	brat	k5eNaPmF	brat
do	do	k7c2	do
úvahy	úvaha	k1gFnSc2	úvaha
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
,	,	kIx,	,
Čínu	Čína	k1gFnSc4	Čína
a	a	k8xC	a
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
.	.	kIx.	.
<g/>
Vojensky	vojensky	k6eAd1	vojensky
byl	být	k5eAaImAgInS	být
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
USA	USA	kA	USA
a	a	k8xC	a
zbytkem	zbytek	k1gInSc7	zbytek
světa	svět	k1gInSc2	svět
velmi	velmi	k6eAd1	velmi
výrazný	výrazný	k2eAgInSc1d1	výrazný
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
si	se	k3xPyFc3	se
však	však	k9	však
nemohli	moct	k5eNaImAgMnP	moct
myslet	myslet	k5eAaImF	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
vyřešit	vyřešit	k5eAaPmF	vyřešit
všechny	všechen	k3xTgInPc4	všechen
problémy	problém	k1gInPc4	problém
světa	svět	k1gInSc2	svět
a	a	k8xC	a
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
pouze	pouze	k6eAd1	pouze
svých	svůj	k3xOyFgMnPc2	svůj
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
desetiletí	desetiletí	k1gNnSc2	desetiletí
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pak	pak	k6eAd1	pak
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
se	se	k3xPyFc4	se
evropské	evropský	k2eAgFnSc2d1	Evropská
<g/>
,	,	kIx,	,
latinskoamerické	latinskoamerický	k2eAgFnSc2d1	latinskoamerická
a	a	k8xC	a
blízkovýchodní	blízkovýchodní	k2eAgFnSc2d1	blízkovýchodní
vojenské	vojenský	k2eAgFnSc2d1	vojenská
politické	politický	k2eAgFnSc2d1	politická
organizace	organizace	k1gFnSc2	organizace
kvůli	kvůli	k7c3	kvůli
dosažení	dosažení	k1gNnSc3	dosažení
svých	svůj	k3xOyFgInPc2	svůj
cílů	cíl	k1gInPc2	cíl
uchýlily	uchýlit	k5eAaPmAgFnP	uchýlit
k	k	k7c3	k
ozbrojenému	ozbrojený	k2eAgInSc3d1	ozbrojený
boji	boj	k1gInSc3	boj
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
rozšíření	rozšíření	k1gNnSc1	rozšíření
NATO	NATO	kA	NATO
po	po	k7c6	po
studené	studený	k2eAgFnSc6d1	studená
válce	válka	k1gFnSc6	válka
přišlo	přijít	k5eAaPmAgNnS	přijít
se	s	k7c7	s
znovusjednocením	znovusjednocení	k1gNnSc7	znovusjednocení
Německa	Německo	k1gNnSc2	Německo
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
bývalá	bývalý	k2eAgFnSc1d1	bývalá
NDR	NDR	kA	NDR
připojila	připojit	k5eAaPmAgFnS	připojit
ke	k	k7c3	k
Spolkové	spolkový	k2eAgFnSc3d1	spolková
republice	republika	k1gFnSc3	republika
Německo	Německo	k1gNnSc4	Německo
a	a	k8xC	a
Alianci	aliance	k1gFnSc4	aliance
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
dohodnuto	dohodnut	k2eAgNnSc1d1	dohodnuto
Smlouvou	smlouva	k1gFnSc7	smlouva
dva	dva	k4xCgInPc4	dva
plus	plus	k6eAd1	plus
čtyři	čtyři	k4xCgInPc4	čtyři
dříve	dříve	k6eAd2	dříve
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	k9	aby
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
bodem	bod	k1gInSc7	bod
smlouvy	smlouva	k1gFnSc2	smlouva
souhlasil	souhlasit	k5eAaImAgInS	souhlasit
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
dohodly	dohodnout	k5eAaPmAgFnP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
NDR	NDR	kA	NDR
nebudou	být	k5eNaImBp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
cizí	cizí	k2eAgFnPc1d1	cizí
vojenské	vojenský	k2eAgFnPc1d1	vojenská
jednotky	jednotka	k1gFnPc1	jednotka
a	a	k8xC	a
jaderné	jaderný	k2eAgFnPc1d1	jaderná
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Stephen	Stephen	k1gInSc1	Stephen
F.	F.	kA	F.
Cohen	Cohen	k1gInSc1	Cohen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
NATO	NATO	kA	NATO
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
zavázalo	zavázat	k5eAaPmAgNnS	zavázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nebude	být	k5eNaImBp3nS	být
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
Roberta	Robert	k1gMnSc2	Robert
Zoellicka	Zoellicko	k1gNnSc2	Zoellicko
<g/>
,	,	kIx,	,
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
zástupce	zástupce	k1gMnSc2	zástupce
ministra	ministr	k1gMnSc2	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
USA	USA	kA	USA
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
smlouvání	smlouvání	k1gNnPc4	smlouvání
o	o	k7c6	o
podmínkách	podmínka	k1gFnPc6	podmínka
Smlouvy	smlouva	k1gFnSc2	smlouva
dva	dva	k4xCgMnPc1	dva
plus	plus	k6eAd1	plus
čtyři	čtyři	k4xCgMnPc1	čtyři
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc1	žádný
takový	takový	k3xDgInSc1	takový
závazek	závazek	k1gInSc1	závazek
neexistoval	existovat	k5eNaImAgInS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
Michail	Michail	k1gMnSc1	Michail
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Gorbačov	Gorbačov	k1gInSc4	Gorbačov
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Američané	Američan	k1gMnPc1	Američan
slíbili	slíbit	k5eAaPmAgMnP	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
NATO	NATO	kA	NATO
se	se	k3xPyFc4	se
nebude	být	k5eNaImBp3nS	být
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
dál	daleko	k6eAd2	daleko
než	než	k8xS	než
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
Německa	Německo	k1gNnSc2	Německo
po	po	k7c6	po
studené	studený	k2eAgFnSc6d1	studená
válce	válka	k1gFnSc6	válka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
NATO	NATO	kA	NATO
po	po	k7c6	po
studené	studený	k2eAgFnSc6d1	studená
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
reorganizace	reorganizace	k1gFnSc1	reorganizace
její	její	k3xOp3gFnSc2	její
vojenské	vojenský	k2eAgFnSc2d1	vojenská
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
některé	některý	k3yIgFnPc4	některý
nové	nový	k2eAgFnPc4d1	nová
složky	složka	k1gFnPc4	složka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Velitelství	velitelství	k1gNnSc1	velitelství
spojeneckého	spojenecký	k2eAgInSc2d1	spojenecký
sboru	sbor	k1gInSc2	sbor
rychlé	rychlý	k2eAgFnSc2d1	rychlá
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
dohody	dohoda	k1gFnPc1	dohoda
o	o	k7c4	o
redukci	redukce	k1gFnSc4	redukce
vojenských	vojenský	k2eAgFnPc2d1	vojenská
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
konvenčních	konvenční	k2eAgFnPc6d1	konvenční
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
silách	síla	k1gFnPc6	síla
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
CFE	CFE	kA	CFE
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
NATO	NATO	kA	NATO
a	a	k8xC	a
státy	stát	k1gInPc1	stát
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
snížení	snížení	k1gNnSc3	snížení
počtu	počet	k1gInSc2	počet
jednotek	jednotka	k1gFnPc2	jednotka
na	na	k7c4	na
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nepřekračují	překračovat	k5eNaImIp3nP	překračovat
určité	určitý	k2eAgFnPc4d1	určitá
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
postbipolárního	postbipolární	k2eAgInSc2d1	postbipolární
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
zánikem	zánik	k1gInSc7	zánik
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
světových	světový	k2eAgFnPc2d1	světová
mocností	mocnost	k1gFnPc2	mocnost
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
a	a	k8xC	a
ukončením	ukončení	k1gNnSc7	ukončení
období	období	k1gNnSc2	období
nazývaného	nazývaný	k2eAgNnSc2d1	nazývané
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
zůstala	zůstat	k5eAaPmAgFnS	zůstat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
supervelmocí	supervelmoc	k1gFnPc2	supervelmoc
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
vyzbrojovaná	vyzbrojovaný	k2eAgFnSc1d1	vyzbrojovaná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
adaptovaný	adaptovaný	k2eAgInSc1d1	adaptovaný
text	text	k1gInSc1	text
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
byla	být	k5eAaImAgFnS	být
odstraněna	odstraněn	k2eAgFnSc1d1	odstraněna
bloková	blokový	k2eAgFnSc1d1	bloková
architektura	architektura	k1gFnSc1	architektura
(	(	kIx(	(
<g/>
NATO	NATO	kA	NATO
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
NATO	NATO	kA	NATO
ale	ale	k8xC	ale
zatím	zatím	k6eAd1	zatím
adaptovaný	adaptovaný	k2eAgInSc4d1	adaptovaný
text	text	k1gInSc4	text
neratifikovaly	ratifikovat	k5eNaBmAgFnP	ratifikovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
čekají	čekat	k5eAaImIp3nP	čekat
<g/>
,	,	kIx,	,
až	až	k8xS	až
Rusko	Rusko	k1gNnSc1	Rusko
stáhne	stáhnout	k5eAaPmIp3nS	stáhnout
své	svůj	k3xOyFgFnPc4	svůj
jednotky	jednotka	k1gFnPc4	jednotka
z	z	k7c2	z
Gruzie	Gruzie	k1gFnSc2	Gruzie
a	a	k8xC	a
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
Francie	Francie	k1gFnSc1	Francie
navrátila	navrátit	k5eAaPmAgFnS	navrátit
do	do	k7c2	do
Vojenského	vojenský	k2eAgInSc2d1	vojenský
výboru	výbor	k1gInSc2	výbor
NATO	NATO	kA	NATO
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
její	její	k3xOp3gFnSc1	její
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
vojenskými	vojenský	k2eAgFnPc7d1	vojenská
strukturami	struktura	k1gFnPc7	struktura
Aliance	aliance	k1gFnSc2	aliance
<g/>
.	.	kIx.	.
</s>
<s>
Politika	politika	k1gFnSc1	politika
Nicolase	Nicolasa	k1gFnSc6	Nicolasa
Sarkozyho	Sarkozy	k1gMnSc2	Sarkozy
nakonec	nakonec	k6eAd1	nakonec
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
návrat	návrat	k1gInSc4	návrat
Francie	Francie	k1gFnSc2	Francie
do	do	k7c2	do
vojenského	vojenský	k2eAgNnSc2d1	vojenské
velení	velení	k1gNnSc2	velení
NATO	NATO	kA	NATO
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
se	s	k7c7	s
členskými	členský	k2eAgFnPc7d1	členská
zeměmi	zem	k1gFnPc7	zem
NATO	nato	k6eAd1	nato
staly	stát	k5eAaPmAgFnP	stát
Česko	Česko	k1gNnSc4	Česko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
a	a	k8xC	a
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
summitu	summit	k1gInSc6	summit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
NATO	NATO	kA	NATO
zrušilo	zrušit	k5eAaPmAgNnS	zrušit
některé	některý	k3yIgFnPc4	některý
staré	starý	k2eAgFnPc4d1	stará
struktury	struktura	k1gFnPc4	struktura
a	a	k8xC	a
založila	založit	k5eAaPmAgFnS	založit
nové	nový	k2eAgInPc4d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
Síly	síl	k1gInPc1	síl
rychlé	rychlý	k2eAgFnSc2d1	rychlá
reakce	reakce	k1gFnSc2	reakce
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
Vrchní	vrchní	k2eAgNnSc1d1	vrchní
velitelství	velitelství	k1gNnSc1	velitelství
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
Atlantiku	Atlantik	k1gInSc6	Atlantik
(	(	kIx(	(
<g/>
SACLANT	SACLANT	kA	SACLANT
<g/>
)	)	kIx)	)
a	a	k8xC	a
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Velitelství	velitelství	k1gNnSc4	velitelství
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
sil	síla	k1gFnPc2	síla
pro	pro	k7c4	pro
transformaci	transformace	k1gFnSc4	transformace
(	(	kIx(	(
<g/>
ACT	ACT	kA	ACT
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
Vrchního	vrchní	k2eAgNnSc2d1	vrchní
velitelství	velitelství	k1gNnSc2	velitelství
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
SHAPE	SHAPE	kA	SHAPE
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Velitelství	velitelství	k1gNnSc1	velitelství
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
sil	síla	k1gFnPc2	síla
pro	pro	k7c4	pro
operace	operace	k1gFnPc4	operace
(	(	kIx(	(
<g/>
ACO	ACO	kA	ACO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
summitu	summit	k1gInSc6	summit
se	se	k3xPyFc4	se
také	také	k9	také
začalo	začít	k5eAaPmAgNnS	začít
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
vstupu	vstup	k1gInSc6	vstup
sedmi	sedm	k4xCc2	sedm
nových	nový	k2eAgInPc2d1	nový
států	stát	k1gInPc2	stát
do	do	k7c2	do
NATO	NATO	kA	NATO
<g/>
:	:	kIx,	:
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
Estonsko	Estonsko	k1gNnSc4	Estonsko
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc4	Lotyšsko
<g/>
,	,	kIx,	,
Litvu	Litva	k1gFnSc4	Litva
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
a	a	k8xC	a
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Aliance	aliance	k1gFnSc2	aliance
tyto	tento	k3xDgInPc1	tento
státy	stát	k1gInPc1	stát
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
summitem	summit	k1gInSc7	summit
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
začala	začít	k5eAaPmAgFnS	začít
mise	mise	k1gFnSc1	mise
Baltic	Baltice	k1gFnPc2	Baltice
Air	Air	k1gFnPc2	Air
Policing	Policing	k1gInSc1	Policing
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
suverenitu	suverenita	k1gFnSc4	suverenita
Baltských	baltský	k2eAgInPc2d1	baltský
států	stát	k1gInPc2	stát
poskytováním	poskytování	k1gNnSc7	poskytování
vojenských	vojenský	k2eAgInPc2d1	vojenský
letounů	letoun	k1gInPc2	letoun
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
ochrany	ochrana	k1gFnSc2	ochrana
před	před	k7c7	před
případným	případný	k2eAgInSc7d1	případný
leteckým	letecký	k2eAgInSc7d1	letecký
útokem	útok	k1gInSc7	útok
<g/>
.	.	kIx.	.
</s>
<s>
Summit	summit	k1gInSc1	summit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
v	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
summitem	summit	k1gInSc7	summit
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
probíhal	probíhat	k5eAaImAgInS	probíhat
na	na	k7c6	na
bývalém	bývalý	k2eAgNnSc6d1	bývalé
území	území	k1gNnSc6	území
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
byla	být	k5eAaImAgFnS	být
otázka	otázka	k1gFnSc1	otázka
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
a	a	k8xC	a
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
další	další	k2eAgNnSc4d1	další
rozšíření	rozšíření	k1gNnSc4	rozšíření
NATO	NATO	kA	NATO
a	a	k8xC	a
Partnerství	partnerství	k1gNnSc2	partnerství
pro	pro	k7c4	pro
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
summitu	summit	k1gInSc6	summit
v	v	k7c6	v
Bukurešti	Bukurešť	k1gFnSc6	Bukurešť
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byly	být	k5eAaImAgInP	být
ke	k	k7c3	k
členství	členství	k1gNnSc3	členství
v	v	k7c6	v
Alianci	aliance	k1gFnSc6	aliance
přizvány	přizván	k2eAgInPc1d1	přizván
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
a	a	k8xC	a
Albánie	Albánie	k1gFnPc1	Albánie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
připojily	připojit	k5eAaPmAgInP	připojit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajině	Ukrajina	k1gFnSc3	Ukrajina
a	a	k8xC	a
Gruzii	Gruzie	k1gFnSc3	Gruzie
bylo	být	k5eAaImAgNnS	být
přislíbeno	přislíben	k2eAgNnSc1d1	přislíbeno
přijetí	přijetí	k1gNnSc1	přijetí
do	do	k7c2	do
Aliance	aliance	k1gFnSc2	aliance
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
aliance	aliance	k1gFnSc2	aliance
od	od	k7c2	od
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
summitu	summit	k1gInSc6	summit
bylo	být	k5eAaImAgNnS	být
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
přezkoumání	přezkoumání	k1gNnSc1	přezkoumání
možnosti	možnost	k1gFnSc2	možnost
ochrany	ochrana	k1gFnSc2	ochrana
území	území	k1gNnSc2	území
Aliance	aliance	k1gFnSc2	aliance
před	před	k7c7	před
raketovými	raketový	k2eAgFnPc7d1	raketová
hrozbami	hrozba	k1gFnPc7	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
později	pozdě	k6eAd2	pozdě
začaly	začít	k5eAaPmAgInP	začít
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
Českem	Česko	k1gNnSc7	Česko
o	o	k7c4	o
vybudování	vybudování	k1gNnSc4	vybudování
protiraketové	protiraketový	k2eAgFnSc2d1	protiraketová
obrany	obrana	k1gFnSc2	obrana
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
základny	základna	k1gFnPc1	základna
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
uvedeny	uvést	k5eAaPmNgInP	uvést
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2008	[number]	k4	2008
Česko	Česko	k1gNnSc1	Česko
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
podepsaly	podepsat	k5eAaPmAgInP	podepsat
předběžnou	předběžný	k2eAgFnSc4d1	předběžná
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c4	o
umístění	umístění	k1gNnSc4	umístění
základny	základna	k1gFnSc2	základna
protiraketové	protiraketový	k2eAgFnSc2d1	protiraketová
obrany	obrana	k1gFnSc2	obrana
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
a	a	k8xC	a
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
byla	být	k5eAaImAgFnS	být
podobná	podobný	k2eAgFnSc1d1	podobná
dohoda	dohoda	k1gFnSc1	dohoda
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
petici	petice	k1gFnSc4	petice
požadující	požadující	k2eAgNnSc1d1	požadující
referendum	referendum	k1gNnSc1	referendum
o	o	k7c6	o
zřízení	zřízení	k1gNnSc6	zřízení
základny	základna	k1gFnSc2	základna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c6	na
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
ruský	ruský	k2eAgMnSc1d1	ruský
prezident	prezident	k1gMnSc1	prezident
Vladimir	Vladimir	k1gMnSc1	Vladimir
Putin	putin	k2eAgMnSc1d1	putin
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
uskutečnění	uskutečnění	k1gNnSc4	uskutečnění
těchto	tento	k3xDgInPc2	tento
plánů	plán	k1gInPc2	plán
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
novým	nový	k2eAgInPc3d1	nový
závodům	závod	k1gInPc3	závod
ve	v	k7c6	v
zbrojení	zbrojení	k1gNnSc6	zbrojení
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
naznačil	naznačit	k5eAaPmAgMnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rusko	Rusko	k1gNnSc1	Rusko
odstoupí	odstoupit	k5eAaPmIp3nS	odstoupit
od	od	k7c2	od
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
konvenčních	konvenční	k2eAgFnPc6d1	konvenční
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
silách	síla	k1gFnPc6	síla
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
CFE	CFE	kA	CFE
<g/>
)	)	kIx)	)
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
všechny	všechen	k3xTgInPc1	všechen
státy	stát	k1gInPc1	stát
NATO	nato	k6eAd1	nato
podepíšou	podepsat	k5eAaPmIp3nP	podepsat
adaptovaný	adaptovaný	k2eAgInSc4d1	adaptovaný
text	text	k1gInSc4	text
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Rusko	Rusko	k1gNnSc1	Rusko
nemá	mít	k5eNaImIp3nS	mít
čeho	co	k3yQnSc2	co
obávat	obávat	k5eAaImF	obávat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
několik	několik	k4yIc4	několik
protiraketových	protiraketový	k2eAgFnPc2d1	protiraketová
střel	střela	k1gFnPc2	střela
by	by	k9	by
ruský	ruský	k2eAgInSc4d1	ruský
jaderný	jaderný	k2eAgInSc4d1	jaderný
arzenál	arzenál	k1gInSc4	arzenál
zastavit	zastavit	k5eAaPmF	zastavit
nedokázalo	dokázat	k5eNaPmAgNnS	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
odstup	odstup	k1gInSc1	odstup
od	od	k7c2	od
smlouvy	smlouva	k1gFnSc2	smlouva
CFE	CFE	kA	CFE
odhlasován	odhlasovat	k5eAaPmNgInS	odhlasovat
ruským	ruský	k2eAgInSc7d1	ruský
parlamentem	parlament	k1gInSc7	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
umístění	umístění	k1gNnSc6	umístění
10	[number]	k4	10
protiraketových	protiraketový	k2eAgFnPc2d1	protiraketová
střel	střela	k1gFnPc2	střela
a	a	k8xC	a
systému	systém	k1gInSc2	systém
MIM-104	MIM-104	k1gMnSc1	MIM-104
Patriot	patriot	k1gMnSc1	patriot
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
jaderným	jaderný	k2eAgFnPc3d1	jaderná
hrozbám	hrozba	k1gFnPc3	hrozba
Polsku	Polska	k1gFnSc4	Polska
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
navíc	navíc	k6eAd1	navíc
Rusko	Rusko	k1gNnSc1	Rusko
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
Norsku	Norsko	k1gNnSc3	Norsko
<g/>
,	,	kIx,	,
že	že	k8xS	že
zruší	zrušit	k5eAaPmIp3nS	zrušit
všechnu	všechen	k3xTgFnSc4	všechen
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Barack	Barack	k1gMnSc1	Barack
Obama	Obama	k?	Obama
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
opouští	opouštět	k5eAaImIp3nS	opouštět
od	od	k7c2	od
plánu	plán	k1gInSc2	plán
na	na	k7c4	na
protiraketové	protiraketový	k2eAgFnPc4d1	protiraketová
střely	střela	k1gFnPc4	střela
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
doletu	dolet	k1gInSc2	dolet
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
bude	být	k5eAaImBp3nS	být
Evropa	Evropa	k1gFnSc1	Evropa
chráněna	chránit	k5eAaImNgFnS	chránit
proti	proti	k7c3	proti
střelám	střela	k1gFnPc3	střela
středního	střední	k2eAgInSc2d1	střední
a	a	k8xC	a
krátkého	krátký	k2eAgInSc2d1	krátký
doletu	dolet	k1gInSc2	dolet
loďmi	loď	k1gFnPc7	loď
využívající	využívající	k2eAgInSc4d1	využívající
systém	systém	k1gInSc4	systém
Aegis	Aegis	k1gFnSc2	Aegis
<g/>
.	.	kIx.	.
</s>
<s>
Oznámení	oznámení	k1gNnSc1	oznámení
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
kritikou	kritika	k1gFnSc7	kritika
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Kongresu	kongres	k1gInSc6	kongres
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
..	..	k?	..
Rusko	Rusko	k1gNnSc1	Rusko
naopak	naopak	k6eAd1	naopak
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
uvítalo	uvítat	k5eAaPmAgNnS	uvítat
a	a	k8xC	a
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
opatření	opatření	k1gNnPc1	opatření
přijatá	přijatý	k2eAgNnPc1d1	přijaté
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
americký	americký	k2eAgInSc4d1	americký
projekt	projekt	k1gInSc4	projekt
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
rozmístění	rozmístění	k1gNnSc4	rozmístění
raket	raketa	k1gFnPc2	raketa
typu	typ	k1gInSc2	typ
9K720	[number]	k4	9K720
Iskander	Iskandra	k1gFnPc2	Iskandra
v	v	k7c6	v
Kaliningradské	kaliningradský	k2eAgFnSc6d1	Kaliningradská
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
zruší	zrušit	k5eAaPmIp3nS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
zvolený	zvolený	k2eAgMnSc1d1	zvolený
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
NATO	NATO	kA	NATO
Anders	Anders	k1gInSc1	Anders
Fogh	Fogh	k1gInSc1	Fogh
Rasmussen	Rasmussen	k1gInSc1	Rasmussen
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
Rusku	Ruska	k1gFnSc4	Ruska
spolupráci	spolupráce	k1gFnSc3	spolupráce
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
konkrétně	konkrétně	k6eAd1	konkrétně
protiraketové	protiraketový	k2eAgFnSc2d1	protiraketová
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Severoatlantickou	severoatlantický	k2eAgFnSc4d1	Severoatlantická
smlouvu	smlouva	k1gFnSc4	smlouva
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1949	[number]	k4	1949
dvanáct	dvanáct	k4xCc4	dvanáct
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
a	a	k8xC	a
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
se	se	k3xPyFc4	se
připojilo	připojit	k5eAaPmAgNnS	připojit
Řecko	Řecko	k1gNnSc1	Řecko
a	a	k8xC	a
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
plné	plný	k2eAgFnSc2d1	plná
suverenity	suverenita	k1gFnSc2	suverenita
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
se	se	k3xPyFc4	se
připojilo	připojit	k5eAaPmAgNnS	připojit
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1999	[number]	k4	1999
předali	předat	k5eAaPmAgMnP	předat
ministři	ministr	k1gMnPc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
své	svůj	k3xOyFgFnSc3	svůj
americké	americký	k2eAgFnSc3d1	americká
kolegyni	kolegyně	k1gFnSc3	kolegyně
Madeleine	Madeleine	k1gFnSc3	Madeleine
Albrightové	Albrightová	k1gFnSc3	Albrightová
příslušné	příslušný	k2eAgFnSc2d1	příslušná
ratifikační	ratifikační	k2eAgFnSc2d1	ratifikační
listiny	listina	k1gFnSc2	listina
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
země	zem	k1gFnPc1	zem
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
také	také	k9	také
staly	stát	k5eAaPmAgInP	stát
členy	člen	k1gInPc1	člen
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
v	v	k7c6	v
největší	veliký	k2eAgFnSc6d3	veliký
vlně	vlna	k1gFnSc6	vlna
rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
NATO	NATO	kA	NATO
připojilo	připojit	k5eAaPmAgNnS	připojit
7	[number]	k4	7
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
Litva	Litva	k1gFnSc1	Litva
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
a	a	k8xC	a
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
přistoupily	přistoupit	k5eAaPmAgFnP	přistoupit
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
a	a	k8xC	a
Albánie	Albánie	k1gFnSc1	Albánie
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
jako	jako	k8xS	jako
první	první	k4xOgFnSc2	první
země	zem	k1gFnSc2	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
Východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1999	[number]	k4	1999
do	do	k7c2	do
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Jan	Jan	k1gMnSc1	Jan
Kavan	Kavan	k1gMnSc1	Kavan
předal	předat	k5eAaPmAgMnS	předat
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Independence	Independence	k1gFnSc2	Independence
v	v	k7c6	v
Missouri	Missouri	k1gFnSc6	Missouri
ratifikační	ratifikační	k2eAgFnSc4d1	ratifikační
listinu	listina	k1gFnSc4	listina
americké	americký	k2eAgFnSc3d1	americká
ministryni	ministryně	k1gFnSc3	ministryně
Madeleine	Madeleine	k1gFnSc3	Madeleine
Albrightové	Albrightová	k1gFnSc3	Albrightová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
summitu	summit	k1gInSc6	summit
v	v	k7c6	v
Bukurešti	Bukurešť	k1gFnSc6	Bukurešť
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
budoucí	budoucí	k2eAgNnSc1d1	budoucí
přizvání	přizvání	k1gNnSc1	přizvání
do	do	k7c2	do
Aliance	aliance	k1gFnSc2	aliance
přislíbeno	přislíbit	k5eAaPmNgNnS	přislíbit
třem	tři	k4xCgFnPc3	tři
státům	stát	k1gInPc3	stát
<g/>
:	:	kIx,	:
Gruzii	Gruzie	k1gFnSc6	Gruzie
<g/>
,	,	kIx,	,
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
Makedonii	Makedonie	k1gFnSc6	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
sporu	spor	k1gInSc3	spor
o	o	k7c4	o
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Makedonie	Makedonie	k1gFnSc1	Makedonie
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
Makedonií	Makedonie	k1gFnSc7	Makedonie
a	a	k8xC	a
Řeckem	Řecko	k1gNnSc7	Řecko
nebyla	být	k5eNaImAgFnS	být
Makedonie	Makedonie	k1gFnSc1	Makedonie
ještě	ještě	k6eAd1	ještě
přijata	přijat	k2eAgFnSc1d1	přijata
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
přijetí	přijetí	k1gNnSc1	přijetí
bylo	být	k5eAaImAgNnS	být
odloženo	odložit	k5eAaPmNgNnS	odložit
na	na	k7c4	na
neurčito	neurčito	k1gNnSc4	neurčito
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
do	do	k7c2	do
vyřešení	vyřešení	k1gNnSc2	vyřešení
sporu	spor	k1gInSc2	spor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vstupu	vstup	k1gInSc2	vstup
Kypru	Kypr	k1gInSc2	Kypr
do	do	k7c2	do
Aliance	aliance	k1gFnSc2	aliance
brání	bránit	k5eAaImIp3nS	bránit
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
parlament	parlament	k1gInSc1	parlament
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
odhlasoval	odhlasovat	k5eAaPmAgInS	odhlasovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
o	o	k7c4	o
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
NATO	NATO	kA	NATO
ucházet	ucházet	k5eAaImF	ucházet
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
potenciálním	potenciální	k2eAgMnSc7d1	potenciální
kandidátem	kandidát	k1gMnSc7	kandidát
je	být	k5eAaImIp3nS	být
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
v	v	k7c6	v
Akčním	akční	k2eAgInSc6d1	akční
plánu	plán	k1gInSc6	plán
členství	členství	k1gNnSc2	členství
(	(	kIx(	(
<g/>
MAP	mapa	k1gFnPc2	mapa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
předstupeň	předstupeň	k1gInSc1	předstupeň
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
do	do	k7c2	do
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
připravit	připravit	k5eAaPmF	připravit
budoucí	budoucí	k2eAgFnPc4d1	budoucí
členské	členský	k2eAgFnPc4d1	členská
země	zem	k1gFnPc4	zem
na	na	k7c4	na
povinnosti	povinnost	k1gFnPc4	povinnost
a	a	k8xC	a
závazky	závazek	k1gInPc4	závazek
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
členství	členství	k1gNnSc1	členství
přináší	přinášet	k5eAaImIp3nS	přinášet
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
NATO	NATO	kA	NATO
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
Vladimir	Vladimir	k1gInSc1	Vladimir
Putin	putin	k2eAgInSc1d1	putin
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Srbsko	Srbsko	k1gNnSc4	Srbsko
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
stát	stát	k1gInSc1	stát
stal	stát	k5eAaPmAgInS	stát
členem	člen	k1gInSc7	člen
Aliance	aliance	k1gFnSc2	aliance
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
zájmům	zájem	k1gInPc3	zájem
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
IFOR	IFOR	kA	IFOR
<g/>
,	,	kIx,	,
SFOR	SFOR	kA	SFOR
a	a	k8xC	a
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Bosně	Bosna	k1gFnSc6	Bosna
a	a	k8xC	a
Hercegovině	Hercegovina	k1gFnSc6	Hercegovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
operace	operace	k1gFnSc2	operace
Deny	Dena	k1gFnSc2	Dena
Flight	Flight	k1gInSc1	Flight
(	(	kIx(	(
<g/>
Odepřený	odepřený	k2eAgInSc1d1	odepřený
let	let	k1gInSc1	let
<g/>
)	)	kIx)	)
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1993	[number]	k4	1993
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1995	[number]	k4	1995
NATO	nato	k6eAd1	nato
zajišťovalo	zajišťovat	k5eAaImAgNnS	zajišťovat
bezletovou	bezletový	k2eAgFnSc4d1	bezletová
zónu	zóna	k1gFnSc4	zóna
nad	nad	k7c7	nad
Bosnou	Bosna	k1gFnSc7	Bosna
a	a	k8xC	a
Hercegovinou	Hercegovina	k1gFnSc7	Hercegovina
během	během	k7c2	během
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Bosně	Bosna	k1gFnSc6	Bosna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
červnem	červen	k1gInSc7	červen
1993	[number]	k4	1993
a	a	k8xC	a
červnem	červen	k1gInSc7	červen
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
říjnem	říjen	k1gInSc7	říjen
<g/>
)	)	kIx)	)
1996	[number]	k4	1996
probíhala	probíhat	k5eAaImAgFnS	probíhat
operace	operace	k1gFnSc1	operace
Sharp	sharp	k1gInSc4	sharp
Guard	Guard	k1gInSc1	Guard
(	(	kIx(	(
<g/>
Bdělá	bdělý	k2eAgFnSc1d1	bdělá
stráž	stráž	k1gFnSc1	stráž
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
námořní	námořní	k2eAgFnSc1d1	námořní
blokáda	blokáda	k1gFnSc1	blokáda
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1994	[number]	k4	1994
sestřelily	sestřelit	k5eAaPmAgInP	sestřelit
americké	americký	k2eAgInPc1d1	americký
letouny	letoun	k1gInPc1	letoun
F-16	F-16	k1gFnSc2	F-16
čtyři	čtyři	k4xCgNnPc1	čtyři
vojenská	vojenský	k2eAgNnPc1d1	vojenské
letadla	letadlo	k1gNnPc1	letadlo
Republiky	republika	k1gFnSc2	republika
srbské	srbský	k2eAgFnSc2d1	Srbská
útočící	útočící	k2eAgFnSc7d1	útočící
proti	proti	k7c3	proti
pozemnímu	pozemní	k2eAgInSc3d1	pozemní
cíli	cíl	k1gInSc3	cíl
<g/>
;	;	kIx,	;
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
vojenský	vojenský	k2eAgInSc1d1	vojenský
zásah	zásah	k1gInSc1	zásah
NATO	NATO	kA	NATO
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
Rozhodná	rozhodný	k2eAgFnSc1d1	rozhodná
síla	síla	k1gFnSc1	síla
(	(	kIx(	(
<g/>
Deliberate	Deliberat	k1gInSc5	Deliberat
Force	force	k1gFnSc1	force
<g/>
)	)	kIx)	)
probíhala	probíhat	k5eAaImAgFnS	probíhat
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1995	[number]	k4	1995
a	a	k8xC	a
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
bombardování	bombardování	k1gNnSc6	bombardování
pozic	pozice	k1gFnPc2	pozice
Vojska	vojsko	k1gNnSc2	vojsko
Republiky	republika	k1gFnSc2	republika
srbské	srbský	k2eAgFnSc2d1	Srbská
ohrožujících	ohrožující	k2eAgInPc2d1	ohrožující
tzv.	tzv.	kA	tzv.
bezpečné	bezpečný	k2eAgFnPc4d1	bezpečná
zóny	zóna	k1gFnPc4	zóna
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
a	a	k8xC	a
Goražde	Goražd	k1gInSc5	Goražd
<g/>
.	.	kIx.	.
</s>
<s>
Zásah	zásah	k1gInSc1	zásah
NATO	NATO	kA	NATO
dopomohl	dopomoct	k5eAaPmAgInS	dopomoct
k	k	k7c3	k
podepsání	podepsání	k1gNnSc3	podepsání
Daytonské	daytonský	k2eAgFnSc2d1	Daytonská
dohody	dohoda	k1gFnSc2	dohoda
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
této	tento	k3xDgFnSc2	tento
dohody	dohoda	k1gFnSc2	dohoda
nasadilo	nasadit	k5eAaPmAgNnS	nasadit
NATO	nato	k6eAd1	nato
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
operace	operace	k1gFnSc2	operace
Joint	Jointa	k1gFnPc2	Jointa
Endeavour	Endeavour	k1gInSc1	Endeavour
(	(	kIx(	(
<g/>
Společné	společný	k2eAgNnSc1d1	společné
úsilí	úsilí	k1gNnSc1	úsilí
<g/>
)	)	kIx)	)
mírové	mírový	k2eAgFnPc1d1	mírová
jednotky	jednotka	k1gFnPc1	jednotka
IFOR	IFOR	kA	IFOR
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
SFOR	SFOR	kA	SFOR
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
působily	působit	k5eAaImAgInP	působit
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
1996	[number]	k4	1996
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
NATO	NATO	kA	NATO
začalo	začít	k5eAaPmAgNnS	začít
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
operacích	operace	k1gFnPc6	operace
udělovat	udělovat	k5eAaImF	udělovat
medaile	medaile	k1gFnPc4	medaile
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Operace	operace	k1gFnSc2	operace
Spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1999	[number]	k4	1999
NATO	nato	k6eAd1	nato
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
jedenáctitýdenní	jedenáctitýdenní	k2eAgFnSc4d1	jedenáctitýdenní
leteckou	letecký	k2eAgFnSc4d1	letecká
operaci	operace	k1gFnSc4	operace
Spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
síla	síla	k1gFnSc1	síla
(	(	kIx(	(
<g/>
Allied	Allied	k1gInSc1	Allied
Force	force	k1gFnSc2	force
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
Svazové	svazový	k2eAgFnSc3d1	svazová
republice	republika	k1gFnSc3	republika
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zastavit	zastavit	k5eAaPmF	zastavit
násilné	násilný	k2eAgNnSc4d1	násilné
jednání	jednání	k1gNnSc4	jednání
vůči	vůči	k7c3	vůči
albánskému	albánský	k2eAgNnSc3d1	albánské
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
Kosova	Kosův	k2eAgNnSc2d1	Kosovo
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
potlačit	potlačit	k5eAaPmF	potlačit
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
silou	síla	k1gFnSc7	síla
ozbrojené	ozbrojený	k2eAgNnSc4d1	ozbrojené
povstání	povstání	k1gNnSc4	povstání
kosovskoalbánských	kosovskoalbánský	k2eAgMnPc2d1	kosovskoalbánský
separatistů	separatista	k1gMnPc2	separatista
<g/>
.	.	kIx.	.
</s>
<s>
NATO	NATO	kA	NATO
požadovalo	požadovat	k5eAaImAgNnS	požadovat
souhlas	souhlas	k1gInSc4	souhlas
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
s	s	k7c7	s
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
intervencí	intervence	k1gFnSc7	intervence
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
poskytnut	poskytnout	k5eAaPmNgInS	poskytnout
kvůli	kvůli	k7c3	kvůli
vetu	veto	k1gNnSc3	veto
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
později	pozdě	k6eAd2	pozdě
podaly	podat	k5eAaPmAgInP	podat
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
ukončení	ukončení	k1gNnSc4	ukončení
intervence	intervence	k1gFnSc2	intervence
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
podpořila	podpořit	k5eAaPmAgFnS	podpořit
ale	ale	k8xC	ale
jen	jen	k9	jen
Namibie	Namibie	k1gFnSc2	Namibie
a	a	k8xC	a
nebyl	být	k5eNaImAgInS	být
tak	tak	k6eAd1	tak
schválen	schválit	k5eAaPmNgInS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
skončil	skončit	k5eAaPmAgInS	skončit
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jugoslávský	jugoslávský	k2eAgMnSc1d1	jugoslávský
vůdce	vůdce	k1gMnSc1	vůdce
Slobodan	Slobodan	k1gMnSc1	Slobodan
Milošević	Milošević	k1gMnSc1	Milošević
vyhověl	vyhovět	k5eAaPmAgInS	vyhovět
požadavkům	požadavek	k1gInPc3	požadavek
NATO	nato	k6eAd1	nato
přijmutím	přijmutí	k1gNnSc7	přijmutí
rezoluce	rezoluce	k1gFnSc2	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
č.	č.	k?	č.
1244	[number]	k4	1244
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1999	[number]	k4	1999
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
Kosova	Kosův	k2eAgInSc2d1	Kosův
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rezoluce	rezoluce	k1gFnSc2	rezoluce
č.	č.	k?	č.
1244	[number]	k4	1244
vojáci	voják	k1gMnPc1	voják
KFOR	KFOR	kA	KFOR
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
dosažení	dosažení	k1gNnSc2	dosažení
trvalého	trvalý	k2eAgInSc2d1	trvalý
míru	mír	k1gInSc2	mír
a	a	k8xC	a
stability	stabilita	k1gFnSc2	stabilita
v	v	k7c4	v
oblasti	oblast	k1gFnPc4	oblast
postižené	postižený	k2eAgFnPc4d1	postižená
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Mise	mise	k1gFnSc1	mise
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
také	také	k9	také
Armáda	armáda	k1gFnSc1	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
srpnem	srpen	k1gInSc7	srpen
a	a	k8xC	a
zářím	září	k1gNnSc7	září
2001	[number]	k4	2001
rovněž	rovněž	k9	rovněž
probíhala	probíhat	k5eAaImAgFnS	probíhat
Aliancí	aliance	k1gFnSc7	aliance
řízená	řízený	k2eAgFnSc1d1	řízená
operace	operace	k1gFnSc1	operace
Essential	Essential	k1gInSc1	Essential
Harvest	Harvest	k1gFnSc1	Harvest
(	(	kIx(	(
<g/>
Nezbytná	zbytný	k2eNgFnSc1d1	zbytný
sklizeň	sklizeň	k1gFnSc1	sklizeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
shromáždit	shromáždit	k5eAaPmF	shromáždit
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
munici	munice	k1gFnSc4	munice
odevzdanou	odevzdaný	k2eAgFnSc4d1	odevzdaná
dobrovolně	dobrovolně	k6eAd1	dobrovolně
albánskými	albánský	k2eAgMnPc7d1	albánský
povstalci	povstalec	k1gMnPc7	povstalec
v	v	k7c6	v
Makedonii	Makedonie	k1gFnSc6	Makedonie
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc1	všechen
další	další	k2eAgInPc1d1	další
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
NATO	NATO	kA	NATO
odporovaly	odporovat	k5eAaImAgInP	odporovat
snahám	snaha	k1gFnPc3	snaha
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
pravidla	pravidlo	k1gNnSc2	pravidlo
souhlasu	souhlas	k1gInSc2	souhlas
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
s	s	k7c7	s
vojenskými	vojenský	k2eAgFnPc7d1	vojenská
operacemi	operace	k1gFnPc7	operace
Aliance	aliance	k1gFnSc2	aliance
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yRgFnPc4	který
patřila	patřit	k5eAaImAgFnS	patřit
např.	např.	kA	např.
operace	operace	k1gFnSc1	operace
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Nutnost	nutnost	k1gFnSc1	nutnost
souhlasu	souhlas	k1gInSc2	souhlas
OSN	OSN	kA	OSN
požadovala	požadovat	k5eAaImAgFnS	požadovat
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
strana	strana	k1gFnSc1	strana
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
podlomilo	podlomit	k5eAaPmAgNnS	podlomit
autoritu	autorita	k1gFnSc4	autorita
Aliance	aliance	k1gFnSc2	aliance
<g/>
,	,	kIx,	,
a	a	k8xC	a
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Čína	Čína	k1gFnSc1	Čína
by	by	kYmCp3nS	by
útok	útok	k1gInSc1	útok
na	na	k7c6	na
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
vetovaly	vetovat	k5eAaBmAgFnP	vetovat
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
dělat	dělat	k5eAaImF	dělat
i	i	k9	i
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nP	by
zmařily	zmařit	k5eAaPmAgFnP	zmařit
všechen	všechen	k3xTgInSc4	všechen
potenciál	potenciál	k1gInSc4	potenciál
a	a	k8xC	a
účel	účel	k1gInSc4	účel
Aliance	aliance	k1gFnSc2	aliance
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
podpůrné	podpůrný	k2eAgFnSc2d1	podpůrná
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
Rozhodná	rozhodný	k2eAgFnSc1d1	rozhodná
podpora	podpora	k1gFnSc1	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Teroristické	teroristický	k2eAgInPc4d1	teroristický
útoky	útok	k1gInPc4	útok
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
přiměly	přimět	k5eAaPmAgInP	přimět
NATO	nato	k6eAd1	nato
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
uplatnit	uplatnit	k5eAaPmF	uplatnit
článek	článek	k1gInSc4	článek
5	[number]	k4	5
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
je	být	k5eAaImIp3nS	být
útok	útok	k1gInSc1	útok
na	na	k7c4	na
jakéhokoli	jakýkoli	k3yIgMnSc4	jakýkoli
člena	člen	k1gMnSc4	člen
Aliance	aliance	k1gFnSc1	aliance
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
útok	útok	k1gInSc4	útok
proti	proti	k7c3	proti
všem	všecek	k3xTgNnPc3	všecek
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2001	[number]	k4	2001
NATO	NATO	kA	NATO
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zmíněné	zmíněný	k2eAgInPc4d1	zmíněný
teroristické	teroristický	k2eAgInPc4d1	teroristický
útoky	útok	k1gInPc4	útok
pod	pod	k7c4	pod
tento	tento	k3xDgInSc4	tento
článek	článek	k1gInSc4	článek
spadají	spadat	k5eAaImIp3nP	spadat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
osm	osm	k4xCc4	osm
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
operací	operace	k1gFnPc2	operace
provedených	provedený	k2eAgFnPc2d1	provedená
NATO	NATO	kA	NATO
patří	patřit	k5eAaImIp3nP	patřit
operace	operace	k1gFnSc1	operace
Eagle	Eagle	k1gInSc4	Eagle
Assist	Assist	k1gInSc1	Assist
(	(	kIx(	(
<g/>
Pomoc	pomoc	k1gFnSc1	pomoc
orla	orel	k1gMnSc2	orel
<g/>
;	;	kIx,	;
hlídání	hlídání	k1gNnSc2	hlídání
amerického	americký	k2eAgInSc2d1	americký
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
prostoru	prostor	k1gInSc2	prostor
<g/>
)	)	kIx)	)
a	a	k8xC	a
operace	operace	k1gFnSc1	operace
Active	Actiev	k1gFnSc2	Actiev
Endeavour	Endeavour	k1gInSc1	Endeavour
(	(	kIx(	(
<g/>
Aktivní	aktivní	k2eAgFnSc1d1	aktivní
snaha	snaha	k1gFnSc1	snaha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
námořní	námořní	k2eAgFnSc2d1	námořní
operace	operace	k1gFnSc2	operace
mající	mající	k2eAgMnPc1d1	mající
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
zabránit	zabránit	k5eAaPmF	zabránit
pohybu	pohyb	k1gInSc3	pohyb
teroristů	terorista	k1gMnPc2	terorista
a	a	k8xC	a
zbraní	zbraň	k1gFnPc2	zbraň
hromadného	hromadný	k2eAgNnSc2d1	hromadné
ničení	ničení	k1gNnSc2	ničení
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
zvýšit	zvýšit	k5eAaPmF	zvýšit
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
námořní	námořní	k2eAgFnSc2d1	námořní
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2003	[number]	k4	2003
NATO	nato	k6eAd1	nato
započalo	započnout	k5eAaPmAgNnS	započnout
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
operaci	operace	k1gFnSc4	operace
mimo	mimo	k7c4	mimo
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
když	když	k8xS	když
převzala	převzít	k5eAaPmAgFnS	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
Mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
bezpečnostními	bezpečnostní	k2eAgFnPc7d1	bezpečnostní
podpůrnými	podpůrný	k2eAgFnPc7d1	podpůrná
sílami	síla	k1gFnPc7	síla
(	(	kIx(	(
<g/>
ISAF	ISAF	kA	ISAF
<g/>
)	)	kIx)	)
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
ISAF	ISAF	kA	ISAF
měly	mít	k5eAaImAgFnP	mít
původně	původně	k6eAd1	původně
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zajistit	zajistit	k5eAaPmF	zajistit
Kábul	Kábul	k1gInSc4	Kábul
a	a	k8xC	a
okolí	okolí	k1gNnSc4	okolí
proti	proti	k7c3	proti
Tálibánu	Tálibán	k2eAgFnSc4d1	Tálibán
a	a	k8xC	a
Al-Káidě	Al-Káida	k1gFnSc3	Al-Káida
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
fungovat	fungovat	k5eAaImF	fungovat
vláda	vláda	k1gFnSc1	vláda
Hámida	Hámida	k1gFnSc1	Hámida
Karzaje	Karzaje	k1gFnSc1	Karzaje
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2003	[number]	k4	2003
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
jednohlasně	jednohlasně	k6eAd1	jednohlasně
schválila	schválit	k5eAaPmAgFnS	schválit
rozšíření	rozšíření	k1gNnSc4	rozšíření
působnosti	působnost	k1gFnSc2	působnost
ISAF	ISAF	kA	ISAF
na	na	k7c6	na
zbytku	zbytek	k1gInSc6	zbytek
území	území	k1gNnSc2	území
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
březnu	březen	k1gInSc3	březen
2012	[number]	k4	2012
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
působilo	působit	k5eAaImAgNnS	působit
téměř	téměř	k6eAd1	téměř
130	[number]	k4	130
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Ukončení	ukončení	k1gNnSc1	ukončení
bojových	bojový	k2eAgFnPc2d1	bojová
operací	operace	k1gFnPc2	operace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
nová	nový	k2eAgFnSc1d1	nová
koaliční	koaliční	k2eAgFnSc1d1	koaliční
operace	operace	k1gFnSc1	operace
s	s	k7c7	s
názvem	název	k1gInSc7	název
Rozhodná	rozhodný	k2eAgFnSc1d1	rozhodná
podpora	podpora	k1gFnSc1	podpora
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
12	[number]	k4	12
500	[number]	k4	500
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
vojáků	voják	k1gMnPc2	voják
provádět	provádět	k5eAaImF	provádět
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
výcvik	výcvik	k1gInSc4	výcvik
<g/>
,	,	kIx,	,
poradenství	poradenství	k1gNnSc4	poradenství
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
afghánských	afghánský	k2eAgFnPc2d1	afghánská
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
NATO	NATO	kA	NATO
započala	započnout	k5eAaPmAgFnS	započnout
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
irácké	irácký	k2eAgFnSc2d1	irácká
vlády	vláda	k1gFnSc2	vláda
výcvikovou	výcvikový	k2eAgFnSc4d1	výcviková
misi	mise	k1gFnSc4	mise
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
(	(	kIx(	(
<g/>
NATO	NATO	kA	NATO
Training	Training	k1gInSc1	Training
Mission	Mission	k1gInSc1	Mission
Iraq	Iraq	k1gFnSc1	Iraq
<g/>
,	,	kIx,	,
NTM-I	NTM-I	k1gFnSc1	NTM-I
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
starala	starat	k5eAaImAgFnS	starat
o	o	k7c4	o
výcvik	výcvik	k1gInSc4	výcvik
nové	nový	k2eAgFnSc2d1	nová
irácké	irácký	k2eAgFnSc2d1	irácká
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
poskytnout	poskytnout	k5eAaPmF	poskytnout
demokraticky	demokraticky	k6eAd1	demokraticky
vedený	vedený	k2eAgInSc1d1	vedený
a	a	k8xC	a
schopný	schopný	k2eAgInSc1d1	schopný
obranný	obranný	k2eAgInSc1d1	obranný
sektor	sektor	k1gInSc1	sektor
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
nejdříve	dříve	k6eAd3	dříve
s	s	k7c7	s
Koalicí	koalice	k1gFnSc7	koalice
mnohonárodních	mnohonárodní	k2eAgFnPc2d1	mnohonárodní
sil	síla	k1gFnPc2	síla
(	(	kIx(	(
<g/>
Multi-National	Multi-National	k1gFnSc1	Multi-National
Force	force	k1gFnSc1	force
Iraq	Iraq	k1gFnSc1	Iraq
<g/>
,	,	kIx,	,
MNF-I	MNF-I	k1gFnSc1	MNF-I
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
zániku	zánik	k1gInSc6	zánik
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
pak	pak	k6eAd1	pak
s	s	k7c7	s
nově	nově	k6eAd1	nově
vzniklou	vzniklý	k2eAgFnSc7d1	vzniklá
složkou	složka	k1gFnSc7	složka
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
Forces	Forces	k1gMnSc1	Forces
–	–	k?	–
Iraq	Iraq	k1gMnSc1	Iraq
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
mise	mise	k1gFnSc1	mise
soustředila	soustředit	k5eAaPmAgFnS	soustředit
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
výcvik	výcvik	k1gInSc4	výcvik
vysoce	vysoce	k6eAd1	vysoce
postavených	postavený	k2eAgMnPc2d1	postavený
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2005	[number]	k4	2005
NATO	NATO	kA	NATO
otevřelo	otevřít	k5eAaPmAgNnS	otevřít
na	na	k7c6	na
kraji	kraj	k1gInSc6	kraj
Bagdádu	Bagdád	k1gInSc2	Bagdád
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
akademii	akademie	k1gFnSc4	akademie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
do	do	k7c2	do
konce	konec	k1gInSc2	konec
února	únor	k1gInSc2	únor
2009	[number]	k4	2009
působili	působit	k5eAaImAgMnP	působit
čtyři	čtyři	k4xCgMnPc4	čtyři
čeští	český	k2eAgMnPc1d1	český
vojáci	voják	k1gMnPc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2007	[number]	k4	2007
se	se	k3xPyFc4	se
operace	operace	k1gFnSc1	operace
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
na	na	k7c4	na
výcvik	výcvik	k1gInSc4	výcvik
federální	federální	k2eAgFnSc2d1	federální
policie	policie	k1gFnSc2	policie
a	a	k8xC	a
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2008	[number]	k4	2008
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
složky	složka	k1gFnPc4	složka
jako	jako	k8xC	jako
např.	např.	kA	např.
loďstvo	loďstvo	k1gNnSc1	loďstvo
a	a	k8xC	a
letectvo	letectvo	k1gNnSc1	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc6	prosinec
2011	[number]	k4	2011
mise	mise	k1gFnSc1	mise
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
,	,	kIx,	,
když	když	k8xS	když
vypršela	vypršet	k5eAaPmAgFnS	vypršet
platnost	platnost	k1gFnSc4	platnost
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
existenci	existence	k1gFnSc4	existence
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
23	[number]	k4	23
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
přispěly	přispět	k5eAaPmAgInP	přispět
celkově	celkově	k6eAd1	celkově
částkou	částka	k1gFnSc7	částka
téměř	téměř	k6eAd1	téměř
250	[number]	k4	250
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pirátství	pirátství	k1gNnSc2	pirátství
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
NATO	nato	k6eAd1	nato
operaci	operace	k1gFnSc4	operace
Ocean	Oceana	k1gFnPc2	Oceana
Shield	Shielda	k1gFnPc2	Shielda
(	(	kIx(	(
<g/>
Oceánský	oceánský	k2eAgInSc1d1	oceánský
štít	štít	k1gInSc1	štít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
ochraně	ochrana	k1gFnSc6	ochrana
námořní	námořní	k2eAgFnSc2d1	námořní
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
Adenském	adenský	k2eAgInSc6d1	adenský
zálivu	záliv	k1gInSc6	záliv
a	a	k8xC	a
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
(	(	kIx(	(
<g/>
okolí	okolí	k1gNnSc6	okolí
Afrického	africký	k2eAgInSc2d1	africký
rohu	roh	k1gInSc2	roh
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
somálským	somálský	k2eAgMnPc3d1	somálský
pirátům	pirát	k1gMnPc3	pirát
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
tak	tak	k9	tak
na	na	k7c4	na
předchozí	předchozí	k2eAgFnPc4d1	předchozí
operace	operace	k1gFnPc4	operace
Allied	Allied	k1gMnSc1	Allied
Provider	Provider	k1gMnSc1	Provider
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc1	říjen
<g/>
–	–	k?	–
<g/>
prosinec	prosinec	k1gInSc1	prosinec
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
a	a	k8xC	a
Allied	Allied	k1gMnSc1	Allied
Protector	Protector	k1gMnSc1	Protector
(	(	kIx(	(
<g/>
březen-srpen	březenrpen	k2eAgMnSc1d1	březen-srpen
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
probíhaly	probíhat	k5eAaImAgFnP	probíhat
také	také	k9	také
v	v	k7c6	v
Adenském	adenský	k2eAgInSc6d1	adenský
zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
operace	operace	k1gFnSc1	operace
Allied	Allied	k1gMnSc1	Allied
Provider	Provider	k1gMnSc1	Provider
bylo	být	k5eAaImAgNnS	být
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
civilních	civilní	k2eAgFnPc2d1	civilní
lodí	loď	k1gFnPc2	loď
Světového	světový	k2eAgInSc2d1	světový
potravinového	potravinový	k2eAgInSc2d1	potravinový
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
dovážely	dovážet	k5eAaImAgFnP	dovážet
potraviny	potravina	k1gFnPc1	potravina
<g/>
,	,	kIx,	,
a	a	k8xC	a
operace	operace	k1gFnSc1	operace
Allied	Allied	k1gInSc4	Allied
Protector	Protector	k1gInSc4	Protector
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
působnost	působnost	k1gFnSc1	působnost
i	i	k9	i
na	na	k7c4	na
komerční	komerční	k2eAgNnPc4d1	komerční
plavidla	plavidlo	k1gNnPc4	plavidlo
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
Ocean	Ocean	k1gMnSc1	Ocean
Shield	Shield	k1gMnSc1	Shield
kromě	kromě	k7c2	kromě
samotné	samotný	k2eAgFnSc2d1	samotná
kontroly	kontrola	k1gFnSc2	kontrola
vod	voda	k1gFnPc2	voda
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
okolním	okolní	k2eAgFnPc3d1	okolní
zemím	zem	k1gFnPc3	zem
výcvik	výcvik	k1gInSc1	výcvik
vlastních	vlastní	k2eAgFnPc2d1	vlastní
protipirátských	protipirátský	k2eAgFnPc2d1	protipirátská
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
intervence	intervence	k1gFnSc1	intervence
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
a	a	k8xC	a
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vyeskalovalo	vyeskalovat	k5eAaImAgNnS	vyeskalovat
násilí	násilí	k1gNnSc1	násilí
mezi	mezi	k7c7	mezi
protestanty	protestant	k1gMnPc7	protestant
a	a	k8xC	a
libyjskou	libyjský	k2eAgFnSc7d1	Libyjská
vládou	vláda	k1gFnSc7	vláda
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Muammarem	Muammar	k1gMnSc7	Muammar
Kaddáfím	Kaddáfí	k1gMnSc7	Kaddáfí
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
schválila	schválit	k5eAaPmAgFnS	schválit
rezoluci	rezoluce	k1gFnSc4	rezoluce
č.	č.	k?	č.
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
požadovala	požadovat	k5eAaImAgFnS	požadovat
příměří	příměří	k1gNnSc4	příměří
a	a	k8xC	a
schválila	schválit	k5eAaPmAgFnS	schválit
vojenský	vojenský	k2eAgInSc4d1	vojenský
zásah	zásah	k1gInSc4	zásah
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
obrany	obrana	k1gFnSc2	obrana
civilního	civilní	k2eAgNnSc2d1	civilní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Koalice	koalice	k1gFnSc1	koalice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
několik	několik	k4yIc1	několik
členů	člen	k1gInPc2	člen
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
zřídila	zřídit	k5eAaPmAgFnS	zřídit
nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
nad	nad	k7c7	nad
Libyí	Libye	k1gFnSc7	Libye
bezletovou	bezletový	k2eAgFnSc7d1	bezletová
zónu	zóna	k1gFnSc4	zóna
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
schválilo	schválit	k5eAaPmAgNnS	schválit
NATO	nato	k6eAd1	nato
embargo	embargo	k1gNnSc1	embargo
na	na	k7c4	na
dovoz	dovoz	k1gInSc4	dovoz
zbraní	zbraň	k1gFnSc7	zbraň
libyjskému	libyjský	k2eAgInSc3d1	libyjský
režimu	režim	k1gInSc3	režim
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
prosazovala	prosazovat	k5eAaImAgFnS	prosazovat
skrze	skrze	k?	skrze
operaci	operace	k1gFnSc3	operace
Unified	Unified	k1gMnSc1	Unified
Protector	Protector	k1gMnSc1	Protector
(	(	kIx(	(
<g/>
Sjednocený	sjednocený	k2eAgMnSc1d1	sjednocený
ochránce	ochránce	k1gMnSc1	ochránce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
"	"	kIx"	"
<g/>
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
<g/>
,	,	kIx,	,
nahlásit	nahlásit	k5eAaPmF	nahlásit
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
zastavit	zastavit	k5eAaPmF	zastavit
lodě	loď	k1gFnPc4	loď
podezřívané	podezřívaný	k2eAgFnPc4d1	podezřívaná
z	z	k7c2	z
nelegálního	legální	k2eNgInSc2d1	nelegální
převozu	převoz	k1gInSc2	převoz
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
NATO	nato	k6eAd1	nato
souhlasilo	souhlasit	k5eAaImAgNnS	souhlasit
s	s	k7c7	s
převzetím	převzetí	k1gNnSc7	převzetí
moci	moc	k1gFnSc2	moc
nad	nad	k7c7	nad
bezletovou	bezletový	k2eAgFnSc7d1	bezletová
zónou	zóna	k1gFnSc7	zóna
od	od	k7c2	od
původní	původní	k2eAgFnSc2d1	původní
koalice	koalice	k1gFnSc2	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
nad	nad	k7c7	nad
pozemními	pozemní	k2eAgFnPc7d1	pozemní
jednotkami	jednotka	k1gFnPc7	jednotka
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
koalice	koalice	k1gFnSc2	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všechny	všechen	k3xTgFnPc1	všechen
členské	členský	k2eAgFnPc1d1	členská
země	zem	k1gFnPc1	zem
se	se	k3xPyFc4	se
však	však	k9	však
bojových	bojový	k2eAgFnPc2d1	bojová
operací	operace	k1gFnPc2	operace
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
jich	on	k3xPp3gFnPc2	on
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
8	[number]	k4	8
z	z	k7c2	z
tehdy	tehdy	k6eAd1	tehdy
celkových	celkový	k2eAgFnPc2d1	celková
28	[number]	k4	28
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
konflikt	konflikt	k1gInSc4	konflikt
mezi	mezi	k7c7	mezi
americkým	americký	k2eAgMnSc7d1	americký
ministrem	ministr	k1gMnSc7	ministr
obrany	obrana	k1gFnSc2	obrana
Robertem	Robert	k1gMnSc7	Robert
Gatesem	Gates	k1gMnSc7	Gates
a	a	k8xC	a
státy	stát	k1gInPc1	stát
jako	jako	k8xS	jako
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Gates	Gates	k1gMnSc1	Gates
po	po	k7c6	po
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
na	na	k7c6	na
operaci	operace	k1gFnSc6	operace
podílely	podílet	k5eAaImAgInP	podílet
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
proslovu	proslov	k1gInSc6	proslov
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
Gates	Gates	k1gMnSc1	Gates
dále	daleko	k6eAd2	daleko
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
nečinné	činný	k2eNgFnPc4d1	nečinná
země	zem	k1gFnPc4	zem
s	s	k7c7	s
poznámkou	poznámka	k1gFnSc7	poznámka
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gNnSc1	jejich
jednání	jednání	k1gNnSc1	jednání
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
způsobit	způsobit	k5eAaPmF	způsobit
rozpad	rozpad	k1gInSc4	rozpad
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
byla	být	k5eAaImAgFnS	být
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
Norsko	Norsko	k1gNnSc1	Norsko
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
postupně	postupně	k6eAd1	postupně
utlumí	utlumit	k5eAaPmIp3nS	utlumit
svou	svůj	k3xOyFgFnSc4	svůj
účast	účast	k1gFnSc4	účast
na	na	k7c4	na
operaci	operace	k1gFnSc4	operace
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
kompletně	kompletně	k6eAd1	kompletně
ukončí	ukončit	k5eAaPmIp3nS	ukončit
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpnu	srpen	k1gInSc6	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Stažení	stažení	k1gNnSc1	stažení
norských	norský	k2eAgInPc2d1	norský
stíhacích	stíhací	k2eAgInPc2d1	stíhací
letounů	letoun	k1gInPc2	letoun
nakonec	nakonec	k6eAd1	nakonec
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Velitel	velitel	k1gMnSc1	velitel
Royal	Royal	k1gMnSc1	Royal
Navy	Navy	k?	Navy
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
britská	britský	k2eAgFnSc1d1	britská
účast	účast	k1gFnSc1	účast
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
škrtům	škrt	k1gInPc3	škrt
v	v	k7c6	v
rozpočtu	rozpočet	k1gInSc6	rozpočet
neudržitelná	udržitelný	k2eNgFnSc1d1	neudržitelná
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnPc1	operace
nakonec	nakonec	k6eAd1	nakonec
probíhaly	probíhat	k5eAaImAgFnP	probíhat
až	až	k9	až
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
;	;	kIx,	;
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byl	být	k5eAaImAgMnS	být
zabit	zabit	k2eAgMnSc1d1	zabit
Muammar	Muammar	k1gMnSc1	Muammar
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
a	a	k8xC	a
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
ukončena	ukončen	k2eAgFnSc1d1	ukončena
intervence	intervence	k1gFnSc1	intervence
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Intervence	intervence	k1gFnSc1	intervence
NATO	NATO	kA	NATO
pomohla	pomoct	k5eAaPmAgFnS	pomoct
svrhnout	svrhnout	k5eAaPmF	svrhnout
a	a	k8xC	a
zavraždit	zavraždit	k5eAaPmF	zavraždit
vůdce	vůdce	k1gMnSc4	vůdce
Libye	Libye	k1gFnSc1	Libye
<g/>
,	,	kIx,	,
diktátora	diktátor	k1gMnSc2	diktátor
Muammara	Muammar	k1gMnSc2	Muammar
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
uvrhla	uvrhnout	k5eAaPmAgFnS	uvrhnout
celou	celý	k2eAgFnSc4d1	celá
zemi	zem	k1gFnSc4	zem
do	do	k7c2	do
krvavého	krvavý	k2eAgInSc2d1	krvavý
chaosu	chaos	k1gInSc2	chaos
<g/>
,	,	kIx,	,
trvajícího	trvající	k2eAgNnSc2d1	trvající
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
NATO	nato	k6eAd1	nato
zřídilo	zřídit	k5eAaPmAgNnS	zřídit
Euroatlantickou	euroatlantický	k2eAgFnSc4d1	euroatlantická
radu	rada	k1gFnSc4	rada
partnerství	partnerství	k1gNnSc2	partnerství
a	a	k8xC	a
Partnerství	partnerství	k1gNnSc2	partnerství
pro	pro	k7c4	pro
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
29	[number]	k4	29
států	stát	k1gInPc2	stát
NATO	NATO	kA	NATO
a	a	k8xC	a
21	[number]	k4	21
"	"	kIx"	"
<g/>
partnerských	partnerský	k2eAgFnPc2d1	partnerská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Partnerství	partnerství	k1gNnSc1	partnerství
pro	pro	k7c4	pro
mír	mír	k1gInSc4	mír
(	(	kIx(	(
<g/>
PfP	PfP	k1gMnPc2	PfP
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
individuální	individuální	k2eAgFnSc6d1	individuální
vojenské	vojenský	k2eAgFnSc6d1	vojenská
spolupráci	spolupráce	k1gFnSc6	spolupráce
každého	každý	k3xTgInSc2	každý
partnerského	partnerský	k2eAgInSc2d1	partnerský
státu	stát	k1gInSc2	stát
s	s	k7c7	s
Aliancí	aliance	k1gFnSc7	aliance
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
členy	člen	k1gMnPc7	člen
patří	patřit	k5eAaImIp3nS	patřit
všechny	všechen	k3xTgInPc1	všechen
současné	současný	k2eAgInPc1d1	současný
a	a	k8xC	a
bývalé	bývalý	k2eAgInPc1d1	bývalý
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
Společenství	společenství	k1gNnSc2	společenství
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
se	se	k3xPyFc4	se
k	k	k7c3	k
Partnerství	partnerství	k1gNnSc3	partnerství
pro	pro	k7c4	pro
mír	mír	k1gInSc4	mír
připojilo	připojit	k5eAaPmAgNnS	připojit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Euroatlantická	euroatlantický	k2eAgFnSc1d1	euroatlantická
rada	rada	k1gFnSc1	rada
partnerství	partnerství	k1gNnSc2	partnerství
(	(	kIx(	(
<g/>
EAPC	EAPC	kA	EAPC
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
ustanovena	ustanovit	k5eAaPmNgFnS	ustanovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
fórum	fórum	k1gNnSc1	fórum
politické	politický	k2eAgFnSc2d1	politická
výměny	výměna	k1gFnSc2	výměna
názorů	názor	k1gInPc2	názor
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgFnPc7	všecek
padesáti	padesát	k4xCc7	padesát
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Středomořský	středomořský	k2eAgInSc1d1	středomořský
dialog	dialog	k1gInSc1	dialog
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
Partnerství	partnerství	k1gNnSc2	partnerství
pro	pro	k7c4	pro
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
soustředí	soustředit	k5eAaPmIp3nS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
severoafrické	severoafrický	k2eAgInPc4d1	severoafrický
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Istanbulská	istanbulský	k2eAgFnSc1d1	Istanbulská
iniciativa	iniciativa	k1gFnSc1	iniciativa
spolupráce	spolupráce	k1gFnSc2	spolupráce
(	(	kIx(	(
<g/>
ICI	ICI	kA	ICI
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
na	na	k7c6	na
summitu	summit	k1gInSc6	summit
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
a	a	k8xC	a
soustředí	soustředit	k5eAaPmIp3nS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
státy	stát	k1gInPc4	stát
širšího	široký	k2eAgInSc2d2	širší
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2002	[number]	k4	2002
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
summitu	summit	k1gInSc6	summit
zahájeny	zahájen	k2eAgInPc4d1	zahájen
Individuální	individuální	k2eAgInPc4d1	individuální
akční	akční	k2eAgInPc4d1	akční
plány	plán	k1gInPc4	plán
partnerství	partnerství	k1gNnSc2	partnerství
(	(	kIx(	(
<g/>
IPAP	IPAP	kA	IPAP
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
zemím	zem	k1gFnPc3	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
politickou	politický	k2eAgFnSc4d1	politická
vůli	vůle	k1gFnSc4	vůle
a	a	k8xC	a
schopnosti	schopnost	k1gFnPc4	schopnost
prohloubit	prohloubit	k5eAaPmF	prohloubit
své	svůj	k3xOyFgInPc4	svůj
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
IPAP	IPAP	kA	IPAP
účastní	účastnit	k5eAaImIp3nP	účastnit
následující	následující	k2eAgInPc1d1	následující
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
Gruzie	Gruzie	k1gFnSc1	Gruzie
Gruzie	Gruzie	k1gFnSc1	Gruzie
(	(	kIx(	(
<g/>
od	od	k7c2	od
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
(	(	kIx(	(
<g/>
od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Arménie	Arménie	k1gFnSc2	Arménie
Arménie	Arménie	k1gFnSc2	Arménie
(	(	kIx(	(
<g/>
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
(	(	kIx(	(
<g/>
od	od	k7c2	od
<g />
.	.	kIx.	.
</s>
<s>
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
(	(	kIx(	(
<g/>
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Srbsko	Srbsko	k1gNnSc1	Srbsko
Srbsko	Srbsko	k1gNnSc1	Srbsko
(	(	kIx(	(
<g/>
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Od	od	k7c2	od
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
začala	začít	k5eAaPmAgFnS	začít
Aliance	aliance	k1gFnSc1	aliance
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
se	s	k7c7	s
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
nemá	mít	k5eNaImIp3nS	mít
uzavřené	uzavřený	k2eAgNnSc1d1	uzavřené
žádné	žádný	k3yNgFnPc4	žádný
z	z	k7c2	z
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgNnPc2d1	uvedené
oficiálních	oficiální	k2eAgNnPc2d1	oficiální
partnerství	partnerství	k1gNnPc2	partnerství
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Argentina	Argentina	k1gFnSc1	Argentina
a	a	k8xC	a
Chile	Chile	k1gNnSc1	Chile
spolupracovaly	spolupracovat	k5eAaImAgInP	spolupracovat
s	s	k7c7	s
NATO	NATO	kA	NATO
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Bosně	Bosna	k1gFnSc6	Bosna
a	a	k8xC	a
Hercegovině	Hercegovina	k1gFnSc6	Hercegovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
Aliance	aliance	k1gFnSc1	aliance
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
obecné	obecný	k2eAgFnPc4d1	obecná
směrnice	směrnice	k1gFnPc4	směrnice
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
ochotné	ochotný	k2eAgInPc1d1	ochotný
s	s	k7c7	s
NATO	NATO	kA	NATO
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
kontaktní	kontaktní	k2eAgFnPc1d1	kontaktní
země	zem	k1gFnPc1	zem
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
další	další	k2eAgMnPc1d1	další
globální	globální	k2eAgMnSc1d1	globální
partneři	partner	k1gMnPc1	partner
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
velitelství	velitelství	k1gNnSc1	velitelství
NATO	NATO	kA	NATO
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Harenu	Haren	k1gInSc6	Haren
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Pracují	pracovat	k5eAaImIp3nP	pracovat
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
delegace	delegace	k1gFnSc1	delegace
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
styční	styčný	k2eAgMnPc1d1	styčný
důstojníci	důstojník	k1gMnPc1	důstojník
nebo	nebo	k8xC	nebo
diplomaté	diplomat	k1gMnPc1	diplomat
partnerských	partnerský	k2eAgFnPc2d1	partnerská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgMnPc1d1	mezinárodní
civilní	civilní	k2eAgMnPc1d1	civilní
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgMnPc1d1	mezinárodní
vojenští	vojenský	k2eAgMnPc1d1	vojenský
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
stálých	stálý	k2eAgMnPc2d1	stálý
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
4000	[number]	k4	4000
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
politickou	politický	k2eAgFnSc4d1	politická
strukturu	struktura	k1gFnSc4	struktura
NATO	NATO	kA	NATO
tvoří	tvořit	k5eAaImIp3nS	tvořit
Severoatlantická	severoatlantický	k2eAgFnSc1d1	Severoatlantická
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
obranné	obranný	k2eAgNnSc4d1	obranné
plánování	plánování	k1gNnSc4	plánování
a	a	k8xC	a
Skupina	skupina	k1gFnSc1	skupina
pro	pro	k7c4	pro
jaderné	jaderný	k2eAgNnSc4d1	jaderné
plánování	plánování	k1gNnSc4	plánování
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgMnPc3	tento
orgánům	orgán	k1gMnPc3	orgán
jsou	být	k5eAaImIp3nP	být
podřízeny	podřízen	k2eAgInPc1d1	podřízen
Hlavní	hlavní	k2eAgInPc1d1	hlavní
výbory	výbor	k1gInPc1	výbor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
řeší	řešit	k5eAaImIp3nP	řešit
specifické	specifický	k2eAgInPc4d1	specifický
úkoly	úkol	k1gInPc4	úkol
v	v	k7c6	v
politické	politický	k2eAgFnSc6d1	politická
<g/>
,	,	kIx,	,
vojenské	vojenský	k2eAgFnSc6d1	vojenská
a	a	k8xC	a
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Severoatlantická	severoatlantický	k2eAgFnSc1d1	Severoatlantická
rada	rada	k1gFnSc1	rada
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
rozhodovací	rozhodovací	k2eAgInSc4d1	rozhodovací
a	a	k8xC	a
konzultační	konzultační	k2eAgInSc4d1	konzultační
orgán	orgán	k1gInSc4	orgán
Aliance	aliance	k1gFnSc2	aliance
<g/>
.	.	kIx.	.
</s>
<s>
Delegace	delegace	k1gFnSc1	delegace
každé	každý	k3xTgFnSc2	každý
z	z	k7c2	z
29	[number]	k4	29
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
jedním	jeden	k4xCgMnSc7	jeden
delegátem	delegát	k1gMnSc7	delegát
(	(	kIx(	(
<g/>
stálým	stálý	k2eAgMnSc7d1	stálý
zástupcem	zástupce	k1gMnSc7	zástupce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
se	se	k3xPyFc4	se
schází	scházet	k5eAaImIp3nS	scházet
alespoň	alespoň	k9	alespoň
jednou	jeden	k4xCgFnSc7	jeden
týdně	týdně	k6eAd1	týdně
<g/>
,	,	kIx,	,
zasedání	zasedání	k1gNnSc1	zasedání
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
ministrů	ministr	k1gMnPc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
předsedů	předseda	k1gMnPc2	předseda
vlád	vláda	k1gFnPc2	vláda
nebo	nebo	k8xC	nebo
ministrů	ministr	k1gMnPc2	ministr
obrany	obrana	k1gFnSc2	obrana
probíhají	probíhat	k5eAaImIp3nP	probíhat
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Radě	rada	k1gFnSc3	rada
předsedá	předsedat	k5eAaImIp3nS	předsedat
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
NATO	NATO	kA	NATO
a	a	k8xC	a
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
jednomyslně	jednomyslně	k6eAd1	jednomyslně
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vojenským	vojenský	k2eAgInSc7d1	vojenský
orgánem	orgán	k1gInSc7	orgán
NATO	NATO	kA	NATO
je	být	k5eAaImIp3nS	být
Vojenský	vojenský	k2eAgInSc1d1	vojenský
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
předkládá	předkládat	k5eAaImIp3nS	předkládat
návrhy	návrh	k1gInPc4	návrh
a	a	k8xC	a
doporučení	doporučení	k1gNnPc4	doporučení
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
radě	rada	k1gFnSc3	rada
<g/>
,	,	kIx,	,
Výboru	výbor	k1gInSc3	výbor
pro	pro	k7c4	pro
obranné	obranný	k2eAgNnSc4d1	obranné
plánování	plánování	k1gNnSc4	plánování
a	a	k8xC	a
Skupině	skupina	k1gFnSc3	skupina
pro	pro	k7c4	pro
jaderné	jaderný	k2eAgNnSc4d1	jaderné
plánování	plánování	k1gNnSc4	plánování
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
Severoatlantické	severoatlantický	k2eAgFnSc6d1	Severoatlantická
radě	rada	k1gFnSc6	rada
je	být	k5eAaImIp3nS	být
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
jedním	jeden	k4xCgInSc7	jeden
(	(	kIx(	(
<g/>
vojenským	vojenský	k2eAgMnSc7d1	vojenský
<g/>
)	)	kIx)	)
zástupcem	zástupce	k1gMnSc7	zástupce
a	a	k8xC	a
výbor	výbor	k1gInSc4	výbor
se	se	k3xPyFc4	se
schází	scházet	k5eAaImIp3nS	scházet
alespoň	alespoň	k9	alespoň
jednou	jeden	k4xCgFnSc7	jeden
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
Vojenského	vojenský	k2eAgInSc2d1	vojenský
výboru	výbor	k1gInSc2	výbor
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
náčelníky	náčelník	k1gInPc7	náčelník
generálních	generální	k2eAgInPc2d1	generální
štábů	štáb	k1gInPc2	štáb
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
na	na	k7c4	na
tříleté	tříletý	k2eAgNnSc4d1	tříleté
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
jím	on	k3xPp3gNnSc7	on
armádní	armádní	k2eAgMnSc1d1	armádní
generál	generál	k1gMnSc1	generál
Petr	Petr	k1gMnSc1	Petr
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Výboru	výbor	k1gInSc3	výbor
podléhají	podléhat	k5eAaImIp3nP	podléhat
dvě	dva	k4xCgNnPc1	dva
vojenská	vojenský	k2eAgNnPc1d1	vojenské
velitelství	velitelství	k1gNnPc1	velitelství
<g/>
:	:	kIx,	:
Velitelství	velitelství	k1gNnSc4	velitelství
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
sil	síla	k1gFnPc2	síla
pro	pro	k7c4	pro
operace	operace	k1gFnPc4	operace
(	(	kIx(	(
<g/>
ACO	ACO	kA	ACO
<g/>
)	)	kIx)	)
a	a	k8xC	a
Velitelství	velitelství	k1gNnSc1	velitelství
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
sil	síla	k1gFnPc2	síla
pro	pro	k7c4	pro
transformaci	transformace	k1gFnSc4	transformace
(	(	kIx(	(
<g/>
ACT	ACT	kA	ACT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ACO	ACO	kA	ACO
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
Vrchním	vrchní	k2eAgNnSc6d1	vrchní
velitelství	velitelství	k1gNnSc6	velitelství
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
SHAPE	SHAPE	kA	SHAPE
<g/>
)	)	kIx)	)
a	a	k8xC	a
velí	velet	k5eAaImIp3nS	velet
mu	on	k3xPp3gMnSc3	on
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
sil	síla	k1gFnPc2	síla
NATO	NATO	kA	NATO
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
SACEUR	SACEUR	kA	SACEUR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
admirál	admirál	k1gMnSc1	admirál
James	James	k1gMnSc1	James
Stavridis	Stavridis	k1gFnSc1	Stavridis
<g/>
.	.	kIx.	.
</s>
<s>
Velitelem	velitel	k1gMnSc7	velitel
ACT	ACT	kA	ACT
(	(	kIx(	(
<g/>
SACT	SACT	kA	SACT
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
generál	generál	k1gMnSc1	generál
Stéphane	Stéphan	k1gMnSc5	Stéphan
Abrial	Abrial	k1gInSc1	Abrial
<g/>
.	.	kIx.	.
</s>
<s>
Parlamentní	parlamentní	k2eAgNnSc1d1	parlamentní
shromáždění	shromáždění	k1gNnSc1	shromáždění
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
poslanců	poslanec	k1gMnPc2	poslanec
parlamentů	parlament	k1gInPc2	parlament
všech	všecek	k3xTgInPc2	všecek
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
a	a	k8xC	a
14	[number]	k4	14
přidružených	přidružený	k2eAgInPc2d1	přidružený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
konzultativní	konzultativní	k2eAgInSc4d1	konzultativní
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Delegáti	delegát	k1gMnPc1	delegát
pracují	pracovat	k5eAaImIp3nP	pracovat
v	v	k7c6	v
pěti	pět	k4xCc6	pět
výborech	výbor	k1gInPc6	výbor
<g/>
:	:	kIx,	:
ekonomickém	ekonomický	k2eAgNnSc6d1	ekonomické
<g/>
,	,	kIx,	,
politickém	politický	k2eAgNnSc6d1	politické
<g/>
,	,	kIx,	,
bezpečnostním	bezpečnostní	k2eAgNnSc6d1	bezpečnostní
a	a	k8xC	a
vědecko-technickém	vědeckoechnický	k2eAgMnSc6d1	vědecko-technický
a	a	k8xC	a
ve	v	k7c6	v
zvláštní	zvláštní	k2eAgFnSc6d1	zvláštní
skupině	skupina	k1gFnSc6	skupina
pro	pro	k7c4	pro
středomořský	středomořský	k2eAgInSc4d1	středomořský
dialog	dialog	k1gInSc4	dialog
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
poslanců	poslanec	k1gMnPc2	poslanec
zemí	zem	k1gFnPc2	zem
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
v	v	k7c6	v
shromáždění	shromáždění	k1gNnSc6	shromáždění
jich	on	k3xPp3gInPc2	on
zasedá	zasedat	k5eAaImIp3nS	zasedat
celkem	celkem	k6eAd1	celkem
přes	přes	k7c4	přes
300	[number]	k4	300
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Rada	Rada	k1gMnSc1	Rada
NATO-Rusko	NATO-Rusko	k1gNnSc1	NATO-Rusko
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Stálou	stálý	k2eAgFnSc4d1	stálá
společnou	společný	k2eAgFnSc4d1	společná
radu	rada	k1gFnSc4	rada
Rusko-NATO	ruskoato	k6eAd1	rusko-nato
(	(	kIx(	(
<g/>
Permanent	permanent	k1gInSc1	permanent
Join	Join	k1gNnSc1	Join
Council	Council	k1gInSc1	Council
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc4	Rusko
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
NATO	NATO	kA	NATO
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
v	v	k7c6	v
ní	on	k3xPp3gFnSc2	on
jednají	jednat	k5eAaImIp3nP	jednat
jako	jako	k9	jako
rovnocenní	rovnocenný	k2eAgMnPc1d1	rovnocenný
partneři	partner	k1gMnPc1	partner
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
konzultaci	konzultace	k1gFnSc3	konzultace
o	o	k7c6	o
bezpečnostních	bezpečnostní	k2eAgFnPc6d1	bezpečnostní
a	a	k8xC	a
vojenských	vojenský	k2eAgFnPc6d1	vojenská
otázkách	otázka	k1gFnPc6	otázka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
ruský	ruský	k2eAgMnSc1d1	ruský
prezident	prezident	k1gMnSc1	prezident
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
Medveděv	Medveděv	k1gMnSc1	Medveděv
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
NATO	NATO	kA	NATO
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
porušilo	porušit	k5eAaPmAgNnS	porušit
sliby	slib	k1gInPc4	slib
dané	daný	k2eAgFnSc2d1	daná
západními	západní	k2eAgMnPc7d1	západní
politiky	politik	k1gMnPc7	politik
po	po	k7c6	po
sjednocení	sjednocení	k1gNnSc6	sjednocení
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
NATO	nato	k6eAd1	nato
přerušilo	přerušit	k5eAaPmAgNnS	přerušit
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
veškerou	veškerý	k3xTgFnSc4	veškerý
spolupráci	spolupráce	k1gFnSc4	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
příměří	příměří	k1gNnSc2	příměří
v	v	k7c6	v
Minsku	Minsk	k1gInSc6	Minsk
mezi	mezi	k7c7	mezi
proruskými	proruský	k2eAgMnPc7d1	proruský
separatisty	separatista	k1gMnPc7	separatista
v	v	k7c6	v
Donbasu	Donbas	k1gInSc6	Donbas
a	a	k8xC	a
ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
němečtí	německý	k2eAgMnPc1d1	německý
představitelé	představitel	k1gMnPc1	představitel
z	z	k7c2	z
okruhu	okruh	k1gInSc2	okruh
kancléřky	kancléřka	k1gFnSc2	kancléřka
Merkelové	Merkelová	k1gFnSc2	Merkelová
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
vrchního	vrchní	k2eAgMnSc4d1	vrchní
velitele	velitel	k1gMnSc4	velitel
sil	síla	k1gFnPc2	síla
NATO	NATO	kA	NATO
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
amerického	americký	k2eAgMnSc2d1	americký
generála	generál	k1gMnSc2	generál
Philipa	Philip	k1gMnSc2	Philip
Breedlova	Breedlův	k2eAgMnSc2d1	Breedlův
za	za	k7c4	za
"	"	kIx"	"
<g/>
nepravdivá	pravdivý	k2eNgNnPc4d1	nepravdivé
tvrzení	tvrzení	k1gNnPc4	tvrzení
a	a	k8xC	a
přehnané	přehnaný	k2eAgInPc4d1	přehnaný
soudy	soud	k1gInPc4	soud
<g/>
"	"	kIx"	"
o	o	k7c6	o
přítomnosti	přítomnost	k1gFnSc6	přítomnost
ruských	ruský	k2eAgNnPc2d1	ruské
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zbytečně	zbytečně	k6eAd1	zbytečně
vyhrocují	vyhrocovat	k5eAaImIp3nP	vyhrocovat
ukrajinskou	ukrajinský	k2eAgFnSc4d1	ukrajinská
krizi	krize	k1gFnSc4	krize
a	a	k8xC	a
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
důvěryhodnost	důvěryhodnost	k1gFnSc4	důvěryhodnost
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
německého	německý	k2eAgInSc2d1	německý
magazínu	magazín	k1gInSc2	magazín
Der	drát	k5eAaImRp2nS	drát
Spiegel	Spiegel	k1gMnSc1	Spiegel
generál	generál	k1gMnSc1	generál
Breedlove	Breedlov	k1gInSc5	Breedlov
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
měsících	měsíc	k1gInPc6	měsíc
opakovaně	opakovaně	k6eAd1	opakovaně
veřejně	veřejně	k6eAd1	veřejně
varoval	varovat	k5eAaImAgMnS	varovat
před	před	k7c7	před
hrozící	hrozící	k2eAgFnSc7d1	hrozící
ruskou	ruský	k2eAgFnSc7d1	ruská
invazí	invaze	k1gFnSc7	invaze
nebo	nebo	k8xC	nebo
před	před	k7c7	před
přítomností	přítomnost	k1gFnSc7	přítomnost
vysokého	vysoký	k2eAgInSc2d1	vysoký
počtu	počet	k1gInSc2	počet
ruských	ruský	k2eAgMnPc2d1	ruský
vojáků	voják	k1gMnPc2	voják
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokaždé	pokaždé	k6eAd1	pokaždé
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
německé	německý	k2eAgFnSc2d1	německá
rozvědky	rozvědka	k1gFnSc2	rozvědka
jeho	jeho	k3xOp3gFnSc2	jeho
informace	informace	k1gFnSc2	informace
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
jako	jako	k9	jako
přehnané	přehnaný	k2eAgFnPc1d1	přehnaná
nebo	nebo	k8xC	nebo
nepravdivé	pravdivý	k2eNgFnPc1d1	nepravdivá
<g/>
.	.	kIx.	.
</s>
<s>
Znepokojení	znepokojení	k1gNnSc4	znepokojení
nad	nad	k7c4	nad
výroky	výrok	k1gInPc4	výrok
generála	generál	k1gMnSc2	generál
Breedlova	Breedlův	k2eAgFnSc1d1	Breedlův
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
velvyslanci	velvyslanec	k1gMnPc1	velvyslanec
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
při	při	k7c6	při
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vojenské	vojenský	k2eAgFnSc2d1	vojenská
intervence	intervence	k1gFnSc2	intervence
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
svržení	svržení	k1gNnSc3	svržení
režimu	režim	k1gInSc2	režim
Muammara	Muammar	k1gMnSc2	Muammar
Kaddáfího	Kaddáfí	k1gMnSc2	Kaddáfí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Libye	Libye	k1gFnSc1	Libye
propadla	propadnout	k5eAaPmAgFnS	propadnout
do	do	k7c2	do
chaosu	chaos	k1gInSc2	chaos
a	a	k8xC	a
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
politici	politik	k1gMnPc1	politik
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
slovenský	slovenský	k2eAgMnSc1d1	slovenský
premiér	premiér	k1gMnSc1	premiér
Robert	Robert	k1gMnSc1	Robert
Fico	Fico	k1gMnSc1	Fico
<g/>
,	,	kIx,	,
jihoafrický	jihoafrický	k2eAgMnSc1d1	jihoafrický
prezident	prezident	k1gMnSc1	prezident
Jacob	Jacoba	k1gFnPc2	Jacoba
Zuma	Zuma	k1gMnSc1	Zuma
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
senátor	senátor	k1gMnSc1	senátor
Rand	rand	k1gInSc4	rand
Paul	Paul	k1gMnSc1	Paul
nebo	nebo	k8xC	nebo
britský	britský	k2eAgMnSc1d1	britský
politik	politik	k1gMnSc1	politik
Nigel	Nigel	k1gMnSc1	Nigel
Farage	Farage	k1gFnSc1	Farage
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
destabilizaci	destabilizace	k1gFnSc3	destabilizace
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
a	a	k8xC	a
k	k	k7c3	k
následné	následný	k2eAgFnSc3d1	následná
migrační	migrační	k2eAgFnSc3d1	migrační
vlně	vlna	k1gFnSc3	vlna
přispěly	přispět	k5eAaPmAgFnP	přispět
vojenské	vojenský	k2eAgFnPc1d1	vojenská
intervence	intervence	k1gFnPc1	intervence
NATO	NATO	kA	NATO
a	a	k8xC	a
západních	západní	k2eAgFnPc2d1	západní
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
a	a	k8xC	a
Libyi	Libye	k1gFnSc6	Libye
i	i	k9	i
podpora	podpora	k1gFnSc1	podpora
povstalců	povstalec	k1gMnPc2	povstalec
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
NATO	NATO	kA	NATO
Jensem	Jens	k1gMnSc7	Jens
Stoltenbergem	Stoltenberg	k1gMnSc7	Stoltenberg
v	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
místopředseda	místopředseda	k1gMnSc1	místopředseda
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
NATO	NATO	kA	NATO
za	za	k7c4	za
nezájem	nezájem	k1gInSc4	nezájem
o	o	k7c4	o
ochranu	ochrana	k1gFnSc4	ochrana
evropských	evropský	k2eAgFnPc2d1	Evropská
hranic	hranice	k1gFnPc2	hranice
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
NATO	NATO	kA	NATO
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
účelově	účelově	k6eAd1	účelově
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
somálským	somálský	k2eAgMnPc3d1	somálský
pirátům	pirát	k1gMnPc3	pirát
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tam	tam	k6eAd1	tam
byl	být	k5eAaImAgInS	být
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
zájem	zájem	k1gInSc1	zájem
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
NATO	NATO	kA	NATO
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
Bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
rady	rada	k1gFnSc2	rada
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
NATO	NATO	kA	NATO
uprchlíci	uprchlík	k1gMnPc1	uprchlík
nezajímají	zajímat	k5eNaImIp3nP	zajímat
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
jejich	jejich	k3xOp3gFnSc7	jejich
vstupní	vstupní	k2eAgFnSc7d1	vstupní
branou	brána	k1gFnSc7	brána
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
je	být	k5eAaImIp3nS	být
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
člen	člen	k1gInSc1	člen
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
a	a	k8xC	a
pašeráci	pašerák	k1gMnPc1	pašerák
na	na	k7c6	na
území	území	k1gNnSc6	území
<g />
.	.	kIx.	.
</s>
<s>
Turecka	Turecko	k1gNnPc1	Turecko
<g/>
,	,	kIx,	,
člena	člen	k1gMnSc4	člen
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
operují	operovat	k5eAaImIp3nP	operovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Německý	německý	k2eAgMnSc1d1	německý
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Angely	Angela	k1gFnSc2	Angela
Merkelové	Merkelová	k1gFnSc2	Merkelová
Frank-Walter	Frank-Walter	k1gMnSc1	Frank-Walter
Steinmeier	Steinmeier	k1gMnSc1	Steinmeier
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2016	[number]	k4	2016
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
NATO	NATO	kA	NATO
za	za	k7c4	za
eskalaci	eskalace	k1gFnSc4	eskalace
napětí	napětí	k1gNnSc2	napětí
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
označil	označit	k5eAaPmAgInS	označit
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
vojenská	vojenský	k2eAgNnPc4d1	vojenské
cvičení	cvičení	k1gNnPc4	cvičení
NATO	nato	k6eAd1	nato
u	u	k7c2	u
hranic	hranice	k1gFnPc2	hranice
Ruska	Rusko	k1gNnSc2	Rusko
za	za	k7c4	za
"	"	kIx"	"
<g/>
kontraproduktivní	kontraproduktivní	k2eAgInPc4d1	kontraproduktivní
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Steinmeier	Steinmeier	k1gMnSc1	Steinmeier
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
bychom	by	kYmCp1nP	by
nyní	nyní	k6eAd1	nyní
neměli	mít	k5eNaImAgMnP	mít
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dál	daleko	k6eAd2	daleko
vyhrocovat	vyhrocovat	k5eAaImF	vyhrocovat
situaci	situace	k1gFnSc3	situace
chrastěním	chrastění	k1gNnSc7	chrastění
zbraní	zbraň	k1gFnSc7	zbraň
a	a	k8xC	a
válečným	válečný	k2eAgNnSc7d1	válečné
štvaním	štvaní	k1gNnSc7	štvaní
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Průzkum	průzkum	k1gInSc1	průzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2016	[number]	k4	2016
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
68	[number]	k4	68
%	%	kIx~	%
dotázaných	dotázaný	k2eAgMnPc2d1	dotázaný
Rusů	Rus	k1gMnPc2	Rus
považuje	považovat	k5eAaImIp3nS	považovat
rozmístění	rozmístění	k1gNnSc1	rozmístění
vojsk	vojsko	k1gNnPc2	vojsko
NATO	NATO	kA	NATO
u	u	k7c2	u
ruských	ruský	k2eAgFnPc2d1	ruská
hranic	hranice	k1gFnPc2	hranice
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
Pobaltí	Pobaltí	k1gNnSc4	Pobaltí
za	za	k7c4	za
hrozbu	hrozba	k1gFnSc4	hrozba
pro	pro	k7c4	pro
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
NATO	nato	k6eAd1	nato
bylo	být	k5eAaImAgNnS	být
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mlčí	mlčet	k5eAaImIp3nS	mlčet
k	k	k7c3	k
porušování	porušování	k1gNnSc3	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
druhé	druhý	k4xOgNnSc1	druhý
největší	veliký	k2eAgNnSc1d3	veliký
vojsko	vojsko	k1gNnSc1	vojsko
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
NATO	NATO	kA	NATO
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
států	stát	k1gInPc2	stát
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
politiky	politika	k1gFnSc2	politika
sdílení	sdílení	k1gNnSc2	sdílení
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
politických	politický	k2eAgFnPc2d1	politická
čistek	čistka	k1gFnPc2	čistka
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
proti	proti	k7c3	proti
skutečným	skutečný	k2eAgInPc3d1	skutečný
nebo	nebo	k8xC	nebo
domnělým	domnělý	k2eAgMnPc3d1	domnělý
oponentům	oponent	k1gMnPc3	oponent
režimu	režim	k1gInSc2	režim
po	po	k7c6	po
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
vojenský	vojenský	k2eAgInSc4d1	vojenský
převrat	převrat	k1gInSc4	převrat
ze	z	k7c2	z
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2016	[number]	k4	2016
požádalo	požádat	k5eAaPmAgNnS	požádat
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
vysoce	vysoce	k6eAd1	vysoce
postavených	postavený	k2eAgMnPc2d1	postavený
tureckých	turecký	k2eAgMnPc2d1	turecký
důstojníků	důstojník	k1gMnPc2	důstojník
působících	působící	k2eAgMnPc2d1	působící
ve	v	k7c6	v
strukturách	struktura	k1gFnPc6	struktura
NATO	nato	k6eAd1	nato
o	o	k7c4	o
azyl	azyl	k1gInSc4	azyl
v	v	k7c6	v
členských	členský	k2eAgFnPc6d1	členská
zemích	zem	k1gFnPc6	zem
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Generálním	generální	k2eAgInSc7d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
NATO	NATO	kA	NATO
Stoltenberg	Stoltenberg	k1gMnSc1	Stoltenberg
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
reagoval	reagovat	k5eAaBmAgMnS	reagovat
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Turecko	Turecko	k1gNnSc1	Turecko
má	mít	k5eAaImIp3nS	mít
právo	práv	k2eAgNnSc1d1	právo
stíhat	stíhat	k5eAaImF	stíhat
organizátory	organizátor	k1gMnPc4	organizátor
<g/>
"	"	kIx"	"
vojenského	vojenský	k2eAgInSc2d1	vojenský
převratu	převrat	k1gInSc2	převrat
a	a	k8xC	a
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Turecko	Turecko	k1gNnSc1	Turecko
je	být	k5eAaImIp3nS	být
klíčovým	klíčový	k2eAgMnSc7d1	klíčový
spojencem	spojenec	k1gMnSc7	spojenec
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
azyl	azyl	k1gInSc4	azyl
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
požádal	požádat	k5eAaPmAgMnS	požádat
i	i	k9	i
turecký	turecký	k2eAgMnSc1d1	turecký
admirál	admirál	k1gMnSc1	admirál
Mustafa	Mustaf	k1gMnSc4	Mustaf
Ugurlu	Ugurla	k1gMnSc4	Ugurla
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c6	v
misi	mise	k1gFnSc6	mise
NATO	NATO	kA	NATO
v	v	k7c6	v
Norfolku	Norfolek	k1gInSc6	Norfolek
ve	v	k7c6	v
Virgínii	Virgínie	k1gFnSc6	Virgínie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
také	také	k9	také
generálmajor	generálmajor	k1gMnSc1	generálmajor
Cahit	Cahit	k1gMnSc1	Cahit
Bakir	Bakir	k1gMnSc1	Bakir
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
velel	velet	k5eAaImAgInS	velet
tureckým	turecký	k2eAgNnSc7d1	turecké
silám	síla	k1gFnPc3	síla
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
jednotkách	jednotka	k1gFnPc6	jednotka
NATO	NATO	kA	NATO
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
Turecký	turecký	k2eAgInSc1d1	turecký
provládní	provládní	k2eAgInSc1d1	provládní
deník	deník	k1gInSc1	deník
Yeni	Yen	k1gFnSc2	Yen
Şafak	Şafak	k1gMnSc1	Şafak
obvinil	obvinit	k5eAaPmAgMnS	obvinit
amerického	americký	k2eAgMnSc4d1	americký
generála	generál	k1gMnSc4	generál
Johna	John	k1gMnSc4	John
Campbella	Campbell	k1gMnSc4	Campbell
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
velel	velet	k5eAaImAgInS	velet
jednotkám	jednotka	k1gFnPc3	jednotka
NATO	nato	k6eAd1	nato
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
plánování	plánování	k1gNnSc4	plánování
vojenského	vojenský	k2eAgInSc2d1	vojenský
převratu	převrat	k1gInSc2	převrat
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
komentátorů	komentátor	k1gMnPc2	komentátor
NATO	NATO	kA	NATO
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
rozšiřování	rozšiřování	k1gNnSc6	rozšiřování
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
snížilo	snížit	k5eAaPmAgNnS	snížit
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
standardy	standard	k1gInPc4	standard
a	a	k8xC	a
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
i	i	k9	i
se	s	k7c7	s
zkorumpovanými	zkorumpovaný	k2eAgInPc7d1	zkorumpovaný
a	a	k8xC	a
autokratickými	autokratický	k2eAgInPc7d1	autokratický
režimy	režim	k1gInPc7	režim
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Mila	Milus	k1gMnSc2	Milus
Djukanoviče	Djukanovič	k1gMnSc2	Djukanovič
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
k	k	k7c3	k
NATO	NATO	kA	NATO
přistoupila	přistoupit	k5eAaPmAgFnS	přistoupit
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
členskými	členský	k2eAgFnPc7d1	členská
zeměmi	zem	k1gFnPc7	zem
NATO	NATO	kA	NATO
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
interních	interní	k2eAgInPc2d1	interní
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
spor	spor	k1gInSc1	spor
o	o	k7c4	o
Gibraltar	Gibraltar	k1gInSc4	Gibraltar
mezi	mezi	k7c7	mezi
Španělskem	Španělsko	k1gNnSc7	Španělsko
a	a	k8xC	a
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
<g/>
,	,	kIx,	,
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
mezi	mezi	k7c7	mezi
Řeckem	Řecko	k1gNnSc7	Řecko
a	a	k8xC	a
Tureckem	Turecko	k1gNnSc7	Turecko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
vystupňovalo	vystupňovat	k5eAaPmAgNnS	vystupňovat
po	po	k7c6	po
turecké	turecký	k2eAgFnSc6d1	turecká
invazi	invaze	k1gFnSc6	invaze
na	na	k7c4	na
Kypr	Kypr	k1gInSc4	Kypr
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
turecká	turecký	k2eAgFnSc1d1	turecká
armáda	armáda	k1gFnSc1	armáda
nezákonně	zákonně	k6eNd1	zákonně
okupuje	okupovat	k5eAaBmIp3nS	okupovat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
spor	spor	k1gInSc1	spor
mezi	mezi	k7c7	mezi
některými	některý	k3yIgFnPc7	některý
členskými	členský	k2eAgFnPc7d1	členská
zeměmi	zem	k1gFnPc7	zem
NATO	NATO	kA	NATO
a	a	k8xC	a
Tureckem	Turecko	k1gNnSc7	Turecko
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
americké	americký	k2eAgFnSc2d1	americká
podpory	podpora	k1gFnSc2	podpora
Kurdů	Kurd	k1gMnPc2	Kurd
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
turecké	turecký	k2eAgFnSc2d1	turecká
podpory	podpora	k1gFnSc2	podpora
syrské	syrský	k2eAgFnSc2d1	Syrská
teroristické	teroristický	k2eAgFnPc4d1	teroristická
organizace	organizace	k1gFnSc2	organizace
An-Nusrá	An-Nusrý	k2eAgFnSc1d1	An-Nusrý
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
napojená	napojený	k2eAgFnSc1d1	napojená
na	na	k7c4	na
Al-Káidu	Al-Káida	k1gFnSc4	Al-Káida
<g/>
.	.	kIx.	.
</s>
