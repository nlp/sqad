<p>
<s>
FAT	fatum	k1gNnPc2	fatum
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
anglického	anglický	k2eAgInSc2d1	anglický
názvu	název	k1gInSc2	název
File	Fil	k1gFnSc2	Fil
Allocation	Allocation	k1gInSc1	Allocation
Table	tablo	k1gNnSc6	tablo
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tabulku	tabulka	k1gFnSc4	tabulka
obsahující	obsahující	k2eAgFnSc2d1	obsahující
informace	informace	k1gFnSc2	informace
o	o	k7c6	o
obsazení	obsazení	k1gNnSc6	obsazení
disku	disk	k1gInSc2	disk
v	v	k7c6	v
souborovém	souborový	k2eAgInSc6d1	souborový
systému	systém	k1gInSc6	systém
vytvořeném	vytvořený	k2eAgInSc6d1	vytvořený
pro	pro	k7c4	pro
DOS	DOS	kA	DOS
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
označuje	označovat	k5eAaImIp3nS	označovat
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
nalezení	nalezení	k1gNnSc4	nalezení
souboru	soubor	k1gInSc2	soubor
(	(	kIx(	(
<g/>
allocation	allocation	k1gInSc1	allocation
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zapsaný	zapsaný	k2eAgInSc1d1	zapsaný
na	na	k7c6	na
disku	disk	k1gInSc6	disk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
FAT	fatum	k1gNnPc2	fatum
je	být	k5eAaImIp3nS	být
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
podporován	podporovat	k5eAaImNgInS	podporovat
prakticky	prakticky	k6eAd1	prakticky
všemi	všecek	k3xTgInPc7	všecek
operačními	operační	k2eAgInPc7d1	operační
systémy	systém	k1gInPc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Určitě	určitě	k6eAd1	určitě
ho	on	k3xPp3gMnSc4	on
podporují	podporovat	k5eAaImIp3nP	podporovat
MS-DOS	MS-DOS	k1gFnSc7	MS-DOS
<g/>
,	,	kIx,	,
FreeDOS	FreeDOS	k1gFnSc7	FreeDOS
<g/>
,	,	kIx,	,
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Linux	Linux	kA	Linux
<g/>
,	,	kIx,	,
FreeBSD	FreeBSD	k1gFnSc2	FreeBSD
a	a	k8xC	a
BeOS	BeOS	k1gFnSc2	BeOS
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
jednoduchosti	jednoduchost	k1gFnSc3	jednoduchost
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc3	rozšíření
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
používán	používat	k5eAaImNgInS	používat
na	na	k7c6	na
výměnných	výměnný	k2eAgInPc6d1	výměnný
médiích	médium	k1gNnPc6	médium
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
disketa	disketa	k1gFnSc1	disketa
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
verze	verze	k1gFnSc1	verze
FAT	fatum	k1gNnPc2	fatum
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USB	USB	kA	USB
flash	flash	k1gInSc1	flash
disk	disk	k1gInSc1	disk
nebo	nebo	k8xC	nebo
IOMEGA	IOMEGA	kA	IOMEGA
ZIP	zip	k1gInSc1	zip
disk	disk	k1gInSc1	disk
(	(	kIx(	(
<g/>
pozor	pozor	k1gInSc1	pozor
<g/>
,	,	kIx,	,
na	na	k7c6	na
CD	CD	kA	CD
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
FAT	fatum	k1gNnPc2	fatum
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
a	a	k8xC	a
naprogramován	naprogramován	k2eAgInSc1d1	naprogramován
Marcem	Marce	k1gMnSc7	Marce
McDonaldem	McDonald	k1gMnSc7	McDonald
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
na	na	k7c6	na
základě	základ	k1gInSc6	základ
série	série	k1gFnSc2	série
diskuzí	diskuze	k1gFnPc2	diskuze
s	s	k7c7	s
Billem	Bill	k1gMnSc7	Bill
Gatesem	Gates	k1gMnSc7	Gates
jako	jako	k8xC	jako
8	[number]	k4	8
<g/>
bitový	bitový	k2eAgInSc4d1	bitový
pro	pro	k7c4	pro
předchůdce	předchůdce	k1gMnSc2	předchůdce
Standalone	Standalon	k1gInSc5	Standalon
Disk	disk	k1gInSc1	disk
BASIC-	BASIC-	k1gFnSc1	BASIC-
<g/>
80	[number]	k4	80
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1980	[number]	k4	1980
Tim	Tim	k?	Tim
Paterson	Paterson	k1gMnSc1	Paterson
začlenil	začlenit	k5eAaPmAgMnS	začlenit
FAT	fatum	k1gNnPc2	fatum
do	do	k7c2	do
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
86-DOS	[number]	k4	86-DOS
(	(	kIx(	(
<g/>
QDOS	QDOS	kA	QDOS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
záznam	záznam	k1gInSc1	záznam
ve	v	k7c6	v
FAT	fatum	k1gNnPc2	fatum
na	na	k7c4	na
12	[number]	k4	12
bitů	bit	k1gInPc2	bit
a	a	k8xC	a
prodloužil	prodloužit	k5eAaPmAgInS	prodloužit
názvy	název	k1gInPc4	název
souborů	soubor	k1gInPc2	soubor
z	z	k7c2	z
6	[number]	k4	6
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
na	na	k7c4	na
8	[number]	k4	8
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
formát	formát	k1gInSc1	formát
FAT	fatum	k1gNnPc2	fatum
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
adresy	adresa	k1gFnPc4	adresa
clusterů	cluster	k1gInPc2	cluster
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
12	[number]	k4	12
bitů	bit	k1gInPc2	bit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
omezen	omezit	k5eAaPmNgInS	omezit
počet	počet	k1gInSc1	počet
clusterů	cluster	k1gInPc2	cluster
na	na	k7c4	na
4096	[number]	k4	4096
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
omezovalo	omezovat	k5eAaImAgNnS	omezovat
celkovou	celkový	k2eAgFnSc4d1	celková
kapacitu	kapacita	k1gFnSc4	kapacita
na	na	k7c4	na
16	[number]	k4	16
MB	MB	kA	MB
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
se	se	k3xPyFc4	se
86-DOS	[number]	k4	86-DOS
přerodil	přerodit	k5eAaPmAgMnS	přerodit
na	na	k7c6	na
MS-DOS	MS-DOS	k1gFnSc6	MS-DOS
a	a	k8xC	a
IBM	IBM	kA	IBM
PC	PC	kA	PC
DOS	DOS	kA	DOS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
FAT16	FAT16	k1gMnPc5	FAT16
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
Microsoft	Microsoft	kA	Microsoft
vydal	vydat	k5eAaPmAgInS	vydat
další	další	k2eAgFnPc4d1	další
verzi	verze	k1gFnSc4	verze
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
podadresářů	podadresář	k1gInPc2	podadresář
a	a	k8xC	a
používající	používající	k2eAgInPc1d1	používající
16	[number]	k4	16
bitů	bit	k1gInPc2	bit
na	na	k7c4	na
adresaci	adresace	k1gFnSc4	adresace
clusteru	cluster	k1gInSc2	cluster
<g/>
.	.	kIx.	.
</s>
<s>
Cluster	cluster	k1gInSc1	cluster
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
tvořen	tvořit	k5eAaImNgInS	tvořit
jedním	jeden	k4xCgInSc7	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
sektory	sektor	k1gInPc1	sektor
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
mocniny	mocnina	k1gFnPc1	mocnina
dvou	dva	k4xCgFnPc2	dva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
ovšem	ovšem	k9	ovšem
64	[number]	k4	64
(	(	kIx(	(
<g/>
velikost	velikost	k1gFnSc1	velikost
32	[number]	k4	32
KB	kb	kA	kb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jednosektorovém	jednosektorový	k2eAgInSc6d1	jednosektorový
clusteru	cluster	k1gInSc6	cluster
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
maximální	maximální	k2eAgFnSc1d1	maximální
možná	možný	k2eAgFnSc1d1	možná
velikost	velikost	k1gFnSc1	velikost
logického	logický	k2eAgInSc2d1	logický
disku	disk	k1gInSc2	disk
32	[number]	k4	32
MB	MB	kA	MB
<g/>
,	,	kIx,	,
při	při	k7c6	při
maximálním	maximální	k2eAgNnSc6d1	maximální
2	[number]	k4	2
GB	GB	kA	GB
(	(	kIx(	(
<g/>
Dodatečné	dodatečný	k2eAgNnSc4d1	dodatečné
rozšíření	rozšíření	k1gNnSc4	rozšíření
u	u	k7c2	u
Windows	Windows	kA	Windows
NT	NT	kA	NT
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
XP	XP	kA	XP
a	a	k8xC	a
EDR-DOSu	EDR-DOSa	k1gFnSc4	EDR-DOSa
<g/>
:	:	kIx,	:
64	[number]	k4	64
KB	kb	kA	kb
velikost	velikost	k1gFnSc1	velikost
clusteru	cluster	k1gInSc2	cluster
<g/>
,	,	kIx,	,
logického	logický	k2eAgInSc2d1	logický
disku	disk	k1gInSc2	disk
až	až	k9	až
4	[number]	k4	4
GB	GB	kA	GB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nazývá	nazývat	k5eAaImIp3nS	nazývat
FAT16	FAT16	k1gFnSc4	FAT16
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
verzi	verze	k1gFnSc4	verze
s	s	k7c7	s
12	[number]	k4	12
bity	bit	k1gInPc7	bit
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
doplněnou	doplněný	k2eAgFnSc7d1	doplněná
podporou	podpora	k1gFnSc7	podpora
podadresářů	podadresář	k1gInPc2	podadresář
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
FAT	fatum	k1gNnPc2	fatum
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
VFAT	VFAT	kA	VFAT
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
s	s	k7c7	s
Windows	Windows	kA	Windows
95	[number]	k4	95
další	další	k2eAgFnSc2d1	další
rozšíření	rozšíření	k1gNnSc4	rozšíření
pro	pro	k7c4	pro
FAT12	FAT12	k1gFnSc4	FAT12
a	a	k8xC	a
FAT	fatum	k1gNnPc2	fatum
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgFnSc6d1	nazývaná
VFAT	VFAT	kA	VFAT
<g/>
.	.	kIx.	.
</s>
<s>
Novinkou	novinka	k1gFnSc7	novinka
byla	být	k5eAaImAgFnS	být
podpora	podpora	k1gFnSc1	podpora
dlouhých	dlouhý	k2eAgNnPc2d1	dlouhé
jmen	jméno	k1gNnPc2	jméno
souborů	soubor	k1gInPc2	soubor
(	(	kIx(	(
<g/>
delších	dlouhý	k2eAgInPc2d2	delší
než	než	k8xS	než
původních	původní	k2eAgInPc2d1	původní
8	[number]	k4	8
znaků	znak	k1gInPc2	znak
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
3	[number]	k4	3
znaky	znak	k1gInPc7	znak
přípony	přípona	k1gFnSc2	přípona
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Požadovaného	požadovaný	k2eAgInSc2d1	požadovaný
efektu	efekt	k1gInSc2	efekt
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
soubor	soubor	k1gInSc1	soubor
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
názvem	název	k1gInSc7	název
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
adresářových	adresářový	k2eAgFnPc2d1	adresářová
položek	položka	k1gFnPc2	položka
(	(	kIx(	(
<g/>
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
souborů	soubor	k1gInPc2	soubor
v	v	k7c6	v
adresáři	adresář	k1gInSc6	adresář
<g/>
)	)	kIx)	)
více	hodně	k6eAd2	hodně
záznamů	záznam	k1gInPc2	záznam
umístěných	umístěný	k2eAgInPc2d1	umístěný
bezprostředně	bezprostředně	k6eAd1	bezprostředně
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
až	až	k9	až
poslední	poslední	k2eAgInSc1d1	poslední
záznam	záznam	k1gInSc1	záznam
je	být	k5eAaImIp3nS	být
standardním	standardní	k2eAgInSc7d1	standardní
záznamem	záznam	k1gInSc7	záznam
typu	typ	k1gInSc2	typ
FAT	fatum	k1gNnPc2	fatum
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgInPc1d1	předchozí
záznamy	záznam	k1gInPc1	záznam
mají	mít	k5eAaImIp3nP	mít
příznaky	příznak	k1gInPc4	příznak
souboru	soubor	k1gInSc2	soubor
nastavené	nastavený	k2eAgNnSc1d1	nastavené
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
staršími	starý	k2eAgInPc7d2	starší
operačními	operační	k2eAgInPc7d1	operační
systémy	systém	k1gInPc7	systém
ignorovány	ignorovat	k5eAaImNgInP	ignorovat
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
tyto	tento	k3xDgInPc4	tento
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
<g/>
)	)	kIx)	)
pouze	pouze	k6eAd1	pouze
krátký	krátký	k2eAgInSc4d1	krátký
název	název	k1gInSc4	název
souboru	soubor	k1gInSc2	soubor
8	[number]	k4	8
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
uložený	uložený	k2eAgInSc1d1	uložený
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
záznamu	záznam	k1gInSc6	záznam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
FAT32	FAT32	k1gMnPc5	FAT32
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
vyšla	vyjít	k5eAaPmAgFnS	vyjít
verze	verze	k1gFnSc1	verze
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
FAT	fatum	k1gNnPc2	fatum
<g/>
32	[number]	k4	32
<g/>
.	.	kIx.	.
</s>
<s>
Přináší	přinášet	k5eAaImIp3nS	přinášet
32	[number]	k4	32
<g/>
bitové	bitový	k2eAgFnSc2d1	bitová
adresy	adresa	k1gFnSc2	adresa
clusterů	cluster	k1gInPc2	cluster
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
číslo	číslo	k1gNnSc1	číslo
alokační	alokační	k2eAgFnSc2d1	alokační
jednotky	jednotka	k1gFnSc2	jednotka
využívá	využívat	k5eAaImIp3nS	využívat
28	[number]	k4	28
bitů	bit	k1gInPc2	bit
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
limit	limit	k1gInSc1	limit
velikosti	velikost	k1gFnSc2	velikost
diskového	diskový	k2eAgInSc2d1	diskový
oddílu	oddíl	k1gInSc2	oddíl
na	na	k7c4	na
8	[number]	k4	8
TiB	tiba	k1gFnPc2	tiba
pro	pro	k7c4	pro
32	[number]	k4	32
kiB	kiB	k?	kiB
cluster	cluster	k1gInSc1	cluster
(	(	kIx(	(
<g/>
228	[number]	k4	228
<g/>
×	×	k?	×
<g/>
32	[number]	k4	32
kiB	kiB	k?	kiB
<g/>
)	)	kIx)	)
a	a	k8xC	a
velikost	velikost	k1gFnSc4	velikost
souborů	soubor	k1gInPc2	soubor
na	na	k7c4	na
4	[number]	k4	4
GB	GB	kA	GB
(	(	kIx(	(
<g/>
přesně	přesně	k6eAd1	přesně
232	[number]	k4	232
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
bajtů	bajt	k1gInPc2	bajt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
není	být	k5eNaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
ukládání	ukládání	k1gNnSc4	ukládání
velkých	velký	k2eAgInPc2d1	velký
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
obrazy	obraz	k1gInPc1	obraz
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
velké	velký	k2eAgInPc1d1	velký
soubory	soubor	k1gInPc1	soubor
s	s	k7c7	s
videem	video	k1gNnSc7	video
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Limitem	limit	k1gInSc7	limit
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
i	i	k9	i
další	další	k2eAgNnPc1d1	další
omezení	omezení	k1gNnPc1	omezení
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
vzít	vzít	k5eAaPmF	vzít
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
DOS	DOS	kA	DOS
využívá	využívat	k5eAaPmIp3nS	využívat
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
disku	disk	k1gInSc2	disk
BIOS	BIOS	kA	BIOS
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jejich	jejich	k3xOp3gInSc7	jejich
limitem	limit	k1gInSc7	limit
je	být	k5eAaImIp3nS	být
adresace	adresace	k1gFnSc1	adresace
CHS	CHS	kA	CHS
s	s	k7c7	s
hranicí	hranice	k1gFnSc7	hranice
32	[number]	k4	32
GB	GB	kA	GB
(	(	kIx(	(
<g/>
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
se	se	k3xPyFc4	se
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
DOSu	DOSus	k1gInSc2	DOSus
nebo	nebo	k8xC	nebo
při	při	k7c6	při
startu	start	k1gInSc6	start
Windows	Windows	kA	Windows
95	[number]	k4	95
a	a	k8xC	a
98	[number]	k4	98
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Omezení	omezení	k1gNnSc1	omezení
LBA	LBA	kA	LBA
adresace	adresace	k1gFnSc2	adresace
je	být	k5eAaImIp3nS	být
až	až	k9	až
do	do	k7c2	do
specifikace	specifikace	k1gFnSc2	specifikace
ATA	ATA	kA	ATA
66	[number]	k4	66
omezeno	omezit	k5eAaPmNgNnS	omezit
hranicí	hranice	k1gFnSc7	hranice
128	[number]	k4	128
GB	GB	kA	GB
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
možností	možnost	k1gFnPc2	možnost
komunikace	komunikace	k1gFnSc1	komunikace
přes	přes	k7c4	přes
starší	starý	k2eAgFnSc4d2	starší
verzi	verze	k1gFnSc4	verze
IDE	IDE	kA	IDE
rozhraní	rozhraní	k1gNnSc1	rozhraní
s	s	k7c7	s
PATA	pata	k1gFnSc1	pata
kabelem	kabel	k1gInSc7	kabel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vadný	vadný	k2eAgInSc1d1	vadný
nástroj	nástroj	k1gInSc1	nástroj
scandisk	scandisk	k1gInSc1	scandisk
ve	v	k7c6	v
Windows	Windows	kA	Windows
95	[number]	k4	95
a	a	k8xC	a
98	[number]	k4	98
nepracuje	pracovat	k5eNaImIp3nS	pracovat
s	s	k7c7	s
oddíly	oddíl	k1gInPc7	oddíl
většími	veliký	k2eAgInPc7d2	veliký
než	než	k8xS	než
128	[number]	k4	128
GB	GB	kA	GB
a	a	k8xC	a
tyto	tento	k3xDgFnPc1	tento
verze	verze	k1gFnPc1	verze
Windows	Windows	kA	Windows
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
též	též	k9	též
vadný	vadný	k2eAgInSc4d1	vadný
nástroj	nástroj	k1gInSc4	nástroj
fdisk	fdisk	k1gInSc1	fdisk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nepracuje	pracovat	k5eNaImIp3nS	pracovat
správně	správně	k6eAd1	správně
s	s	k7c7	s
oddíly	oddíl	k1gInPc7	oddíl
většími	veliký	k2eAgInPc7d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
64	[number]	k4	64
GB	GB	kA	GB
<g/>
.	.	kIx.	.
</s>
<s>
Opravený	opravený	k2eAgInSc1d1	opravený
nástroj	nástroj	k1gInSc1	nástroj
fdisk	fdisk	k1gInSc1	fdisk
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
vytvářet	vytvářet	k5eAaImF	vytvářet
oddíly	oddíl	k1gInPc4	oddíl
větší	veliký	k2eAgInPc4d2	veliký
než	než	k8xS	než
512	[number]	k4	512
GB	GB	kA	GB
<g/>
.	.	kIx.	.
<g/>
Nativní	nativní	k2eAgFnSc1d1	nativní
podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
32	[number]	k4	32
<g/>
bitové	bitový	k2eAgInPc4d1	bitový
systémy	systém	k1gInPc4	systém
je	být	k5eAaImIp3nS	být
obsažena	obsáhnout	k5eAaPmNgFnS	obsáhnout
ve	v	k7c6	v
Windows	Windows	kA	Windows
2000	[number]	k4	2000
a	a	k8xC	a
novějších	nový	k2eAgInPc2d2	novější
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Windows	Windows	kA	Windows
XP	XP	kA	XP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tak	tak	k9	tak
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
číst	číst	k5eAaImF	číst
a	a	k8xC	a
zapisovat	zapisovat	k5eAaImF	zapisovat
na	na	k7c4	na
libovolně	libovolně	k6eAd1	libovolně
velký	velký	k2eAgInSc4d1	velký
oddíl	oddíl	k1gInSc4	oddíl
s	s	k7c7	s
FAT	fatum	k1gNnPc2	fatum
<g/>
32	[number]	k4	32
<g/>
.	.	kIx.	.
</s>
<s>
Záměrně	záměrně	k6eAd1	záměrně
však	však	k9	však
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
naformátovat	naformátovat	k5eAaPmF	naformátovat
pouze	pouze	k6eAd1	pouze
oddíl	oddíl	k1gInSc4	oddíl
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
velikostí	velikost	k1gFnSc7	velikost
32	[number]	k4	32
GB	GB	kA	GB
(	(	kIx(	(
<g/>
stejný	stejný	k2eAgInSc1d1	stejný
limit	limit	k1gInSc1	limit
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
také	také	k9	také
instalační	instalační	k2eAgInSc1d1	instalační
program	program	k1gInSc1	program
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Microsoft	Microsoft	kA	Microsoft
považuje	považovat	k5eAaImIp3nS	považovat
tak	tak	k6eAd1	tak
velké	velký	k2eAgInPc1d1	velký
souborové	souborový	k2eAgInPc1d1	souborový
systémy	systém	k1gInPc1	systém
FAT	fatum	k1gNnPc2	fatum
za	za	k7c2	za
pomalé	pomalý	k2eAgFnSc2d1	pomalá
a	a	k8xC	a
neefektivní	efektivní	k2eNgFnSc2d1	neefektivní
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc1d2	veliký
existující	existující	k2eAgInPc1d1	existující
oddíly	oddíl	k1gInPc1	oddíl
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
bez	bez	k7c2	bez
problému	problém	k1gInSc2	problém
naformátovány	naformátován	k2eAgInPc4d1	naformátován
jinými	jiný	k2eAgInPc7d1	jiný
programy	program	k1gInPc7	program
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
FAT	fatum	k1gNnPc2	fatum
<g/>
+	+	kIx~	+
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
nezávislými	závislý	k2eNgMnPc7d1	nezávislý
programátory	programátor	k1gMnPc7	programátor
publikováno	publikovat	k5eAaBmNgNnS	publikovat
kompatibilní	kompatibilní	k2eAgNnSc1d1	kompatibilní
rozšíření	rozšíření	k1gNnSc1	rozšíření
FAT	fatum	k1gNnPc2	fatum
<g/>
+	+	kIx~	+
(	(	kIx(	(
<g/>
FATplus	FATplus	k1gInSc1	FATplus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvyšující	zvyšující	k2eAgFnSc1d1	zvyšující
maximální	maximální	k2eAgFnSc1d1	maximální
velikost	velikost	k1gFnSc1	velikost
souboru	soubor	k1gInSc2	soubor
ze	z	k7c2	z
4	[number]	k4	4
GB	GB	kA	GB
na	na	k7c4	na
256	[number]	k4	256
GB	GB	kA	GB
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
implementováno	implementován	k2eAgNnSc1d1	implementováno
v	v	k7c6	v
systému	systém	k1gInSc6	systém
Enhanced-DR-DOS	Enhanced-DR-DOS	k1gFnSc2	Enhanced-DR-DOS
<g/>
,	,	kIx,	,
plánuje	plánovat	k5eAaImIp3nS	plánovat
se	se	k3xPyFc4	se
i	i	k9	i
pro	pro	k7c4	pro
FreeDOS	FreeDOS	k1gFnSc4	FreeDOS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
exFAT	exFAT	k?	exFAT
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
Microsoft	Microsoft	kA	Microsoft
uvedl	uvést	k5eAaPmAgMnS	uvést
patentovanou	patentovaný	k2eAgFnSc4d1	patentovaná
exFAT	exFAT	k?	exFAT
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
jednodušší	jednoduchý	k2eAgInSc1d2	jednodušší
než	než	k8xS	než
NTFS	NTFS	kA	NTFS
a	a	k8xC	a
podobný	podobný	k2eAgInSc4d1	podobný
FAT	fatum	k1gNnPc2	fatum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgMnS	být
zcela	zcela	k6eAd1	zcela
kompatibilní	kompatibilní	k2eAgMnSc1d1	kompatibilní
<g/>
.	.	kIx.	.
</s>
<s>
Podpora	podpora	k1gFnSc1	podpora
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
s	s	k7c7	s
Windows	Windows	kA	Windows
7	[number]	k4	7
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
exFAT	exFAT	k?	exFAT
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
u	u	k7c2	u
SDXC	SDXC	kA	SDXC
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Struktura	struktura	k1gFnSc1	struktura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Boot	Boot	k2eAgInSc1d1	Boot
sektor	sektor	k1gInSc1	sektor
(	(	kIx(	(
<g/>
VBR	VBR	kA	VBR
<g/>
,	,	kIx,	,
spouštěcí	spouštěcí	k2eAgInPc1d1	spouštěcí
záznam	záznam	k1gInSc4	záznam
svazku	svazek	k1gInSc2	svazek
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
sektor	sektor	k1gInSc1	sektor
logické	logický	k2eAgFnSc2d1	logická
oblasti	oblast	k1gFnSc2	oblast
disku	disk	k1gInSc2	disk
(	(	kIx(	(
<g/>
označované	označovaný	k2eAgFnPc1d1	označovaná
také	také	k6eAd1	také
jako	jako	k9	jako
diskový	diskový	k2eAgInSc1d1	diskový
oddíl	oddíl	k1gInSc1	oddíl
<g/>
,	,	kIx,	,
logický	logický	k2eAgInSc1d1	logický
oddíl	oddíl	k1gInSc1	oddíl
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
nebo	nebo	k8xC	nebo
partition	partition	k1gInSc1	partition
<g/>
)	)	kIx)	)
obsahující	obsahující	k2eAgInSc1d1	obsahující
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
FAT	fatum	k1gNnPc2	fatum
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
blok	blok	k1gInSc1	blok
parametrů	parametr	k1gInPc2	parametr
disku	disk	k1gInSc2	disk
a	a	k8xC	a
spouštěcí	spouštěcí	k2eAgInSc4d1	spouštěcí
kód	kód	k1gInSc4	kód
svazku	svazek	k1gInSc2	svazek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Drobné	drobný	k2eAgNnSc1d1	drobné
upozornění	upozornění	k1gNnSc1	upozornění
<g/>
:	:	kIx,	:
Boot	Boot	k2eAgInSc1d1	Boot
sektor	sektor	k1gInSc1	sektor
není	být	k5eNaImIp3nS	být
totéž	týž	k3xTgNnSc1	týž
co	co	k3yInSc1	co
Master	master	k1gMnSc1	master
boot	boot	k1gMnSc1	boot
record	record	k1gMnSc1	record
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgInSc1d1	hlavní
spouštěcí	spouštěcí	k2eAgInSc1d1	spouštěcí
záznam	záznam	k1gInSc1	záznam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
úplně	úplně	k6eAd1	úplně
prvním	první	k4xOgInSc7	první
sektorem	sektor	k1gInSc7	sektor
na	na	k7c6	na
fyzickém	fyzický	k2eAgInSc6d1	fyzický
disku	disk	k1gInSc6	disk
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tabulku	tabulka	k1gFnSc4	tabulka
rozdělení	rozdělení	k1gNnSc2	rozdělení
disku	disk	k1gInSc2	disk
(	(	kIx(	(
<g/>
Partition	Partition	k1gInSc1	Partition
table	tablo	k1gNnSc6	tablo
<g/>
)	)	kIx)	)
a	a	k8xC	a
hlavní	hlavní	k2eAgInSc4d1	hlavní
spouštěcí	spouštěcí	k2eAgInSc4d1	spouštěcí
kód	kód	k1gInSc4	kód
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
boot	boot	k2eAgInSc1d1	boot
sektor	sektor	k1gInSc1	sektor
i	i	k9	i
master	master	k1gMnSc1	master
boot	boot	k1gMnSc1	boot
record	record	k6eAd1	record
jsou	být	k5eAaImIp3nP	být
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
místem	místo	k1gNnSc7	místo
počítačových	počítačový	k2eAgInPc2d1	počítačový
virů	vir	k1gInPc2	vir
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
kód	kód	k1gInSc1	kód
uložený	uložený	k2eAgInSc1d1	uložený
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
sektorech	sektor	k1gInPc6	sektor
bývá	bývat	k5eAaImIp3nS	bývat
vykonán	vykonat	k5eAaPmNgInS	vykonat
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
samotný	samotný	k2eAgInSc1d1	samotný
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Blok	blok	k1gInSc1	blok
parametrů	parametr	k1gInPc2	parametr
disku	disk	k1gInSc2	disk
====	====	k?	====
</s>
</p>
<p>
<s>
Blok	blok	k1gInSc1	blok
parametrů	parametr	k1gInPc2	parametr
disku	disk	k1gInSc2	disk
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
svazku	svazek	k1gInSc6	svazek
jako	jako	k9	jako
např.	např.	kA	např.
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
sektorů	sektor	k1gInPc2	sektor
na	na	k7c4	na
cluster	cluster	k1gInSc4	cluster
<g/>
,	,	kIx,	,
počet	počet	k1gInSc4	počet
rezervovaných	rezervovaný	k2eAgInPc2d1	rezervovaný
sektorů	sektor	k1gInPc2	sektor
před	před	k7c4	před
první	první	k4xOgInSc4	první
FAT	fatum	k1gNnPc2	fatum
<g/>
,	,	kIx,	,
počet	počet	k1gInSc4	počet
FAT	fatum	k1gNnPc2	fatum
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
sektorů	sektor	k1gInPc2	sektor
kořenového	kořenový	k2eAgInSc2d1	kořenový
adresáře	adresář	k1gInSc2	adresář
<g/>
,	,	kIx,	,
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
sektorů	sektor	k1gInPc2	sektor
na	na	k7c6	na
disku	disk	k1gInSc6	disk
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
sektorů	sektor	k1gInPc2	sektor
v	v	k7c6	v
jedné	jeden	k4xCgFnSc2	jeden
FAT	fatum	k1gNnPc2	fatum
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
svazku	svazek	k1gInSc2	svazek
(	(	kIx(	(
<g/>
volume	volum	k1gInSc5	volum
label	lablo	k1gNnPc2	lablo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Alokační	alokační	k2eAgFnSc1d1	alokační
tabulka	tabulka	k1gFnSc1	tabulka
souborů	soubor	k1gInPc2	soubor
(	(	kIx(	(
<g/>
FAT	fatum	k1gNnPc2	fatum
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
tabulka	tabulka	k1gFnSc1	tabulka
popisuje	popisovat	k5eAaImIp3nS	popisovat
přiřazení	přiřazení	k1gNnSc4	přiřazení
každého	každý	k3xTgInSc2	každý
clusteru	cluster	k1gInSc2	cluster
v	v	k7c6	v
oddílu	oddíl	k1gInSc6	oddíl
(	(	kIx(	(
<g/>
1	[number]	k4	1
záznam	záznam	k1gInSc1	záznam
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
1	[number]	k4	1
clusteru	cluster	k1gInSc2	cluster
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
existují	existovat	k5eAaImIp3nP	existovat
2	[number]	k4	2
kopie	kopie	k1gFnPc1	kopie
(	(	kIx(	(
<g/>
obě	dva	k4xCgNnPc4	dva
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgFnP	uložit
bezprostředně	bezprostředně	k6eAd1	bezprostředně
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
)	)	kIx)	)
–	–	k?	–
ta	ten	k3xDgNnPc1	ten
druhá	druhý	k4xOgFnSc1	druhý
je	být	k5eAaImIp3nS	být
použita	použít	k5eAaPmNgFnS	použít
v	v	k7c6	v
momentě	moment	k1gInSc6	moment
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
první	první	k4xOgFnSc7	první
stane	stanout	k5eAaPmIp3nS	stanout
nečitelnou	čitelný	k2eNgFnSc7d1	nečitelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přiřazení	přiřazení	k1gNnSc1	přiřazení
clusteru	cluster	k1gInSc2	cluster
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
různých	různý	k2eAgFnPc2d1	různá
specifických	specifický	k2eAgFnPc2d1	specifická
hodnot	hodnota	k1gFnPc2	hodnota
jako	jako	k8xS	jako
např.	např.	kA	např.
volný	volný	k2eAgMnSc1d1	volný
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
x	x	k?	x
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vadný	vadný	k2eAgMnSc1d1	vadný
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
xFFFE	xFFFE	k?	xFFFE
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cluster	cluster	k1gInSc1	cluster
indikující	indikující	k2eAgInSc4d1	indikující
konec	konec	k1gInSc4	konec
souboru	soubor	k1gInSc2	soubor
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
xFFFF	xFFFF	k?	xFFFF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
číslo	číslo	k1gNnSc1	číslo
následujícího	následující	k2eAgInSc2d1	následující
clusteru	cluster	k1gInSc2	cluster
souboru	soubor	k1gInSc2	soubor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kořenový	kořenový	k2eAgInSc1d1	kořenový
adresář	adresář	k1gInSc1	adresář
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
verzi	verze	k1gFnSc6	verze
obsahoval	obsahovat	k5eAaImAgMnS	obsahovat
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
databázi	databáze	k1gFnSc4	databáze
obsahující	obsahující	k2eAgFnPc1d1	obsahující
veškeré	veškerý	k3xTgFnPc1	veškerý
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
všech	všecek	k3xTgInPc6	všecek
souborech	soubor	k1gInPc6	soubor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
operačnímu	operační	k2eAgInSc3d1	operační
systému	systém	k1gInSc3	systém
<g/>
,	,	kIx,	,
v	v	k7c6	v
příslušném	příslušný	k2eAgInSc6d1	příslušný
oddílu	oddíl	k1gInSc6	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zavedením	zavedení	k1gNnSc7	zavedení
podadresářů	podadresář	k1gInPc2	podadresář
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
dalších	další	k2eAgInPc2d1	další
adresářů	adresář	k1gInPc2	adresář
kromě	kromě	k7c2	kromě
tohoto	tento	k3xDgNnSc2	tento
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
kořenem	kořen	k1gInSc7	kořen
stromové	stromový	k2eAgFnSc2d1	stromová
hierarchie	hierarchie	k1gFnSc2	hierarchie
adresářů	adresář	k1gInPc2	adresář
<g/>
:	:	kIx,	:
záznam	záznam	k1gInSc1	záznam
o	o	k7c6	o
podadresáři	podadresář	k1gInSc6	podadresář
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
uložený	uložený	k2eAgInSc4d1	uložený
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádné	žádný	k3yNgFnPc4	žádný
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
souborech	soubor	k1gInPc6	soubor
uložených	uložený	k2eAgFnPc2d1	uložená
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
podadresáři	podadresář	k1gInSc6	podadresář
(	(	kIx(	(
<g/>
ani	ani	k8xC	ani
součet	součet	k1gInSc1	součet
velikostí	velikost	k1gFnPc2	velikost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
podadresáři	podadresář	k1gInSc6	podadresář
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
tedy	tedy	k9	tedy
chtěli	chtít	k5eAaImAgMnP	chtít
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
databází	databáze	k1gFnSc7	databáze
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
bychom	by	kYmCp1nP	by
přečíst	přečíst	k5eAaPmF	přečíst
všechny	všechen	k3xTgInPc4	všechen
adresáře	adresář	k1gInPc4	adresář
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
běžnou	běžný	k2eAgFnSc4d1	běžná
práci	práce	k1gFnSc4	práce
se	s	k7c7	s
svazkem	svazek	k1gInSc7	svazek
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
FAT12	FAT12	k1gMnSc2	FAT12
a	a	k8xC	a
FAT16	FAT16	k1gMnSc2	FAT16
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
velikost	velikost	k1gFnSc1	velikost
stanovena	stanovit	k5eAaPmNgFnS	stanovit
napevno	napevno	k6eAd1	napevno
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
souborového	souborový	k2eAgInSc2d1	souborový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
od	od	k7c2	od
verze	verze	k1gFnSc2	verze
FAT32	FAT32	k1gFnSc2	FAT32
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
uložen	uložit	k5eAaPmNgMnS	uložit
kdekoliv	kdekoliv	k6eAd1	kdekoliv
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
velikost	velikost	k1gFnSc1	velikost
může	moct	k5eAaImIp3nS	moct
libovolně	libovolně	k6eAd1	libovolně
narůstat	narůstat	k5eAaImF	narůstat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Problémy	problém	k1gInPc4	problém
==	==	k?	==
</s>
</p>
<p>
<s>
Fragmentace	fragmentace	k1gFnSc1	fragmentace
</s>
</p>
<p>
<s>
Ztracené	ztracený	k2eAgInPc1d1	ztracený
clustery	cluster	k1gInPc1	cluster
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
FAT	fatum	k1gNnPc2	fatum
tabulce	tabulka	k1gFnSc6	tabulka
clustery	cluster	k1gInPc1	cluster
označené	označený	k2eAgInPc1d1	označený
jako	jako	k8xC	jako
používané	používaný	k2eAgInPc1d1	používaný
avšak	avšak	k8xC	avšak
k	k	k7c3	k
žádnému	žádný	k3yNgInSc3	žádný
souboru	soubor	k1gInSc3	soubor
nejsou	být	k5eNaImIp3nP	být
přiřazeny	přiřazen	k2eAgInPc1d1	přiřazen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Překřížené	překřížený	k2eAgInPc1d1	překřížený
soubory	soubor	k1gInPc1	soubor
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
ve	v	k7c6	v
FAT	fatum	k1gNnPc2	fatum
tabulce	tabulka	k1gFnSc6	tabulka
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
2	[number]	k4	2
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
souborů	soubor	k1gInPc2	soubor
vyhrazeny	vyhrazen	k2eAgInPc1d1	vyhrazen
clustery	cluster	k1gInPc1	cluster
se	se	k3xPyFc4	se
stejným	stejný	k2eAgNnSc7d1	stejné
číslem	číslo	k1gNnSc7	číslo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Poškozená	poškozený	k2eAgFnSc1d1	poškozená
FAT	fatum	k1gNnPc2	fatum
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
souboru	soubor	k1gInSc3	soubor
přiřazen	přiřazen	k2eAgInSc1d1	přiřazen
blok	blok	k1gInSc1	blok
několika	několik	k4yIc2	několik
clusterů	cluster	k1gInPc2	cluster
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ukazatel	ukazatel	k1gInSc1	ukazatel
v	v	k7c6	v
některém	některý	k3yIgInSc6	některý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
clusterů	cluster	k1gInPc2	cluster
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
za	za	k7c4	za
konec	konec	k1gInSc4	konec
disku	disk	k1gInSc2	disk
nebo	nebo	k8xC	nebo
oddílu	oddíl	k1gInSc2	oddíl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
ExFAT	ExFAT	k?	ExFAT
</s>
</p>
<p>
<s>
Master	master	k1gMnSc1	master
boot	boot	k1gMnSc1	boot
record	record	k1gMnSc1	record
</s>
</p>
<p>
<s>
Diskový	diskový	k2eAgInSc1d1	diskový
oddíl	oddíl	k1gInSc1	oddíl
</s>
</p>
<p>
<s>
Souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
</s>
</p>
<p>
<s>
DOS	DOS	kA	DOS
,	,	kIx,	,
FreeDOS	FreeDOS	k1gFnSc1	FreeDOS
</s>
</p>
<p>
<s>
Cluster	cluster	k1gInSc1	cluster
</s>
</p>
<p>
<s>
HPFS	HPFS	kA	HPFS
<g/>
,	,	kIx,	,
NTFS	NTFS	kA	NTFS
–	–	k?	–
modernější	moderní	k2eAgInPc4d2	modernější
souborové	souborový	k2eAgInPc4d1	souborový
systémy	systém	k1gInPc4	systém
(	(	kIx(	(
<g/>
používané	používaný	k2eAgFnPc4d1	používaná
v	v	k7c6	v
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
resp.	resp.	kA	resp.
Windows	Windows	kA	Windows
NT	NT	kA	NT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
vývoji	vývoj	k1gInSc6	vývoj
se	se	k3xPyFc4	se
také	také	k6eAd1	také
podílel	podílet	k5eAaImAgMnS	podílet
Microsoft	Microsoft	kA	Microsoft
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
FAT32	FAT32	k4	FAT32
specifikace	specifikace	k1gFnSc1	specifikace
od	od	k7c2	od
Microsoftu	Microsoft	k1gInSc2	Microsoft
</s>
</p>
<p>
<s>
FATplus	FATplus	k1gMnSc1	FATplus
</s>
</p>
