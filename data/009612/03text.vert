<p>
<s>
Perl	perl	k1gInSc1	perl
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
purl	purl	k1gInSc1	purl
<g/>
]	]	kIx)	]
či	či	k8xC	či
[	[	kIx(	[
<g/>
pɜ	pɜ	k?	pɜ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
interpretovaný	interpretovaný	k2eAgInSc1d1	interpretovaný
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
Larry	Larra	k1gFnSc2	Larra
Wallem	Wallo	k1gNnSc7	Wallo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
internetu	internet	k1gInSc2	internet
se	se	k3xPyFc4	se
Perl	perl	k1gInSc1	perl
stal	stát	k5eAaPmAgInS	stát
velmi	velmi	k6eAd1	velmi
populárním	populární	k2eAgInSc7d1	populární
nástrojem	nástroj	k1gInSc7	nástroj
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
CGI	CGI	kA	CGI
skriptů	skript	k1gInPc2	skript
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Perl	perl	k1gInSc1	perl
zahájil	zahájit	k5eAaPmAgInS	zahájit
svou	svůj	k3xOyFgFnSc4	svůj
éru	éra	k1gFnSc4	éra
jako	jako	k8xC	jako
skriptovací	skriptovací	k2eAgInSc1d1	skriptovací
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
náhrada	náhrada	k1gFnSc1	náhrada
jazyka	jazyk	k1gInSc2	jazyk
AWK	AWK	kA	AWK
a	a	k8xC	a
interpretru	interpretr	k1gInSc2	interpretr
sh	sh	k?	sh
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgNnSc2d3	veliký
rozšíření	rozšíření	k1gNnSc2	rozšíření
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
4	[number]	k4	4
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Verze	verze	k1gFnSc1	verze
5	[number]	k4	5
přinesla	přinést	k5eAaPmAgFnS	přinést
četná	četný	k2eAgNnPc4d1	četné
vylepšení	vylepšení	k1gNnSc4	vylepšení
<g/>
,	,	kIx,	,
především	především	k9	především
výkonné	výkonný	k2eAgFnPc4d1	výkonná
datové	datový	k2eAgFnPc4d1	datová
struktury	struktura	k1gFnPc4	struktura
a	a	k8xC	a
možnost	možnost	k1gFnSc4	možnost
objektového	objektový	k2eAgNnSc2d1	objektové
programování	programování	k1gNnSc2	programování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
získal	získat	k5eAaPmAgMnS	získat
Perl	perl	k1gInSc4	perl
oblibu	obliba	k1gFnSc4	obliba
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
v	v	k7c6	v
bioinformatice	bioinformatika	k1gFnSc6	bioinformatika
<g/>
.	.	kIx.	.
</s>
<s>
PC	PC	kA	PC
Magazine	Magazin	k1gInSc5	Magazin
zařadil	zařadit	k5eAaPmAgInS	zařadit
Perl	perl	k1gInSc4	perl
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
mezi	mezi	k7c4	mezi
finalisty	finalista	k1gMnPc4	finalista
soutěže	soutěž	k1gFnSc2	soutěž
Technical	Technical	k1gFnSc2	Technical
Excellence	Excellence	k1gFnSc2	Excellence
Award	Awarda	k1gFnPc2	Awarda
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Development	Development	k1gMnSc1	Development
Tool	Tool	k1gMnSc1	Tool
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc4	slovo
Perl	perl	k1gInSc4	perl
najdete	najít	k5eAaPmIp2nP	najít
v	v	k7c4	v
Oxford	Oxford	k1gInSc4	Oxford
English	English	k1gInSc4	English
Dictionary	Dictionara	k1gFnSc2	Dictionara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Larry	Larra	k1gFnPc1	Larra
Wall	Walla	k1gFnPc2	Walla
se	se	k3xPyFc4	se
při	při	k7c6	při
konstrukci	konstrukce	k1gFnSc6	konstrukce
jazyka	jazyk	k1gInSc2	jazyk
řídil	řídit	k5eAaImAgInS	řídit
heslem	heslo	k1gNnSc7	heslo
"	"	kIx"	"
<g/>
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
udělat	udělat	k5eAaPmF	udělat
více	hodně	k6eAd2	hodně
způsoby	způsob	k1gInPc1	způsob
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
there	ther	k1gInSc5	ther
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
more	mor	k1gInSc5	mor
than	thana	k1gFnPc2	thana
one	one	k?	one
way	way	k?	way
to	ten	k3xDgNnSc4	ten
do	do	k7c2	do
it	it	k?	it
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
psát	psát	k5eAaImF	psát
krátké	krátký	k2eAgInPc4d1	krátký
programy	program	k1gInPc4	program
jednoduše	jednoduše	k6eAd1	jednoduše
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
nebrání	bránit	k5eNaImIp3nS	bránit
v	v	k7c6	v
psaní	psaní	k1gNnSc6	psaní
těch	ten	k3xDgFnPc2	ten
složitých	složitý	k2eAgFnPc2d1	složitá
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
způsobů	způsob	k1gInPc2	způsob
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
obvykle	obvykle	k6eAd1	obvykle
velmi	velmi	k6eAd1	velmi
stručný	stručný	k2eAgInSc4d1	stručný
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Perl	perl	k1gInSc1	perl
získal	získat	k5eAaPmAgInS	získat
nezaslouženou	zasloužený	k2eNgFnSc4d1	nezasloužená
pověst	pověst	k1gFnSc4	pověst
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
nesrozumitelný	srozumitelný	k2eNgInSc1d1	nesrozumitelný
a	a	k8xC	a
neudržovatelný	udržovatelný	k2eNgInSc1d1	neudržovatelný
kód	kód	k1gInSc1	kód
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kritika	kritika	k1gFnSc1	kritika
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
oprávněná	oprávněný	k2eAgFnSc1d1	oprávněná
<g/>
,	,	kIx,	,
Perl	perl	k1gInSc1	perl
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
malých	malý	k2eAgInPc2d1	malý
i	i	k8xC	i
velkých	velký	k2eAgInPc2d1	velký
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Schopnosti	schopnost	k1gFnPc1	schopnost
a	a	k8xC	a
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
u	u	k7c2	u
velkých	velký	k2eAgInPc2d1	velký
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
i	i	k9	i
v	v	k7c6	v
krátkých	krátký	k2eAgInPc6d1	krátký
skriptech	skript	k1gInPc6	skript
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dobrý	dobrý	k2eAgInSc1d1	dobrý
programátor	programátor	k1gInSc1	programátor
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
Larryho	Larry	k1gMnSc2	Larry
Walla	Wall	k1gMnSc2	Wall
být	být	k5eAaImF	být
líný	líný	k2eAgMnSc1d1	líný
<g/>
,	,	kIx,	,
netrpělivý	trpělivý	k2eNgInSc1d1	netrpělivý
a	a	k8xC	a
sebevědomý	sebevědomý	k2eAgMnSc1d1	sebevědomý
(	(	kIx(	(
<g/>
Laziness	Laziness	k1gInSc1	Laziness
<g/>
,	,	kIx,	,
Impatience	Impatience	k1gFnSc1	Impatience
and	and	k?	and
Hubris	Hubris	k1gInSc1	Hubris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výhody	výhod	k1gInPc1	výhod
Perlu	perl	k1gInSc2	perl
==	==	k?	==
</s>
</p>
<p>
<s>
výhody	výhoda	k1gFnPc1	výhoda
"	"	kIx"	"
<g/>
interpretovaného	interpretovaný	k2eAgInSc2d1	interpretovaný
<g/>
"	"	kIx"	"
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
rychlý	rychlý	k2eAgInSc1d1	rychlý
vývoj	vývoj	k1gInSc1	vývoj
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
kompilace	kompilace	k1gFnSc2	kompilace
a	a	k8xC	a
linkování	linkování	k1gNnSc2	linkování
–	–	k?	–
program	program	k1gInSc1	program
je	být	k5eAaImIp3nS	být
zkompilován	zkompilovat	k5eAaPmNgInS	zkompilovat
po	po	k7c6	po
každém	každý	k3xTgInSc6	každý
spuštění	spuštění	k1gNnSc6	spuštění
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
kdykoli	kdykoli	k6eAd1	kdykoli
možné	možný	k2eAgNnSc1d1	možné
"	"	kIx"	"
<g/>
přikompilovat	přikompilovat	k5eAaPmF	přikompilovat
<g/>
"	"	kIx"	"
další	další	k2eAgInSc4d1	další
kód	kód	k1gInSc4	kód
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
i	i	k9	i
provést	provést	k5eAaPmF	provést
část	část	k1gFnSc4	část
kódu	kód	k1gInSc2	kód
před	před	k7c7	před
kompilací	kompilace	k1gFnSc7	kompilace
zbytku	zbytek	k1gInSc2	zbytek
a	a	k8xC	a
nastavit	nastavit	k5eAaPmF	nastavit
tak	tak	k9	tak
konstanty	konstanta	k1gFnPc1	konstanta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
následně	následně	k6eAd1	následně
může	moct	k5eAaImIp3nS	moct
využít	využít	k5eAaPmF	využít
optimalizační	optimalizační	k2eAgFnSc4d1	optimalizační
část	část	k1gFnSc4	část
kompilace	kompilace	k1gFnSc2	kompilace
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
například	například	k6eAd1	například
vynechat	vynechat	k5eAaPmF	vynechat
kód	kód	k1gInSc4	kód
pro	pro	k7c4	pro
ladicí	ladicí	k2eAgInPc4d1	ladicí
výpisy	výpis	k1gInPc4	výpis
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
program	program	k1gInSc1	program
spuštěn	spustit	k5eAaPmNgInS	spustit
s	s	k7c7	s
určitým	určitý	k2eAgInSc7d1	určitý
parametrem	parametr	k1gInSc7	parametr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
přes	přes	k7c4	přes
18	[number]	k4	18
000	[number]	k4	000
volně	volně	k6eAd1	volně
dostupných	dostupný	k2eAgInPc2d1	dostupný
modulů	modul	k1gInPc2	modul
třetích	třetí	k4xOgFnPc2	třetí
stran	strana	k1gFnPc2	strana
v	v	k7c4	v
Comprehensive	Comprehensiev	k1gFnPc4	Comprehensiev
Perl	perl	k1gInSc4	perl
Archive	archiv	k1gInSc5	archiv
Network	network	k1gInSc1	network
CPAN	CPAN	kA	CPAN
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
<g/>
,	,	kIx,	,
kategorizace	kategorizace	k1gFnSc1	kategorizace
<g/>
,	,	kIx,	,
dokumentace	dokumentace	k1gFnSc1	dokumentace
<g/>
,	,	kIx,	,
testování	testování	k1gNnSc1	testování
a	a	k8xC	a
instalace	instalace	k1gFnPc1	instalace
modulů	modul	k1gInPc2	modul
jsou	být	k5eAaImIp3nP	být
standardizovány	standardizován	k2eAgFnPc1d1	standardizována
<g/>
.	.	kIx.	.
</s>
<s>
Moduly	modul	k1gInPc1	modul
zpřístupňují	zpřístupňovat	k5eAaImIp3nP	zpřístupňovat
prakticky	prakticky	k6eAd1	prakticky
veškerá	veškerý	k3xTgNnPc4	veškerý
dostupná	dostupný	k2eAgNnPc4d1	dostupné
rozhraní	rozhraní	k1gNnPc4	rozhraní
a	a	k8xC	a
knihovny	knihovna	k1gFnPc4	knihovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
efektivita	efektivita	k1gFnSc1	efektivita
programování	programování	k1gNnSc2	programování
–	–	k?	–
v	v	k7c6	v
programech	program	k1gInPc6	program
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nejsou	být	k5eNaImIp3nP	být
přímo	přímo	k6eAd1	přímo
závislé	závislý	k2eAgFnPc1d1	závislá
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
běhu	běh	k1gInSc2	běh
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
výhodnější	výhodný	k2eAgInSc1d2	výhodnější
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
méně	málo	k6eAd2	málo
řádek	řádek	k1gInSc4	řádek
kódu	kód	k1gInSc2	kód
</s>
</p>
<p>
<s>
automatická	automatický	k2eAgFnSc1d1	automatická
práce	práce	k1gFnSc1	práce
s	s	k7c7	s
pamětí	paměť	k1gFnSc7	paměť
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
explicitně	explicitně	k6eAd1	explicitně
alokovat	alokovat	k5eAaImF	alokovat
a	a	k8xC	a
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
paměť	paměť	k1gFnSc4	paměť
<g/>
)	)	kIx)	)
a	a	k8xC	a
pokročilé	pokročilý	k2eAgInPc1d1	pokročilý
datové	datový	k2eAgInPc1d1	datový
typy	typ	k1gInPc1	typ
např.	např.	kA	např.
asociativní	asociativní	k2eAgNnSc1d1	asociativní
pole	pole	k1gNnSc1	pole
neboli	neboli	k8xC	neboli
hash	hash	k1gInSc1	hash
(	(	kIx(	(
<g/>
lineární	lineární	k2eAgInPc1d1	lineární
seznamy	seznam	k1gInPc1	seznam
a	a	k8xC	a
binární	binární	k2eAgInPc1d1	binární
stromy	strom	k1gInPc1	strom
již	již	k6eAd1	již
nejsou	být	k5eNaImIp3nP	být
třeba	třeba	k6eAd1	třeba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Perl	perl	k1gInSc1	perl
je	být	k5eAaImIp3nS	být
svobodný	svobodný	k2eAgInSc1d1	svobodný
software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
licencován	licencován	k2eAgInSc1d1	licencován
pod	pod	k7c7	pod
Artistic	Artistice	k1gFnPc2	Artistice
License	License	k1gFnPc4	License
nebo	nebo	k8xC	nebo
GNU	gnu	k1gNnSc4	gnu
General	General	k1gFnSc2	General
Public	publicum	k1gNnPc2	publicum
License	Licens	k1gInSc5	Licens
</s>
</p>
<p>
<s>
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
kompilovanými	kompilovaný	k2eAgInPc7d1	kompilovaný
jazyky	jazyk	k1gInPc7	jazyk
lepší	dobrý	k2eAgFnSc2d2	lepší
možnosti	možnost	k1gFnSc2	možnost
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
při	při	k7c6	při
běhu	běh	k1gInSc6	běh
kódu	kód	k1gInSc2	kód
</s>
</p>
<p>
<s>
snadné	snadný	k2eAgNnSc1d1	snadné
spojování	spojování	k1gNnSc1	spojování
již	již	k6eAd1	již
hotových	hotový	k2eAgFnPc2d1	hotová
komponent	komponenta	k1gFnPc2	komponenta
(	(	kIx(	(
<g/>
modulů	modul	k1gInPc2	modul
<g/>
)	)	kIx)	)
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
u	u	k7c2	u
Delphi	Delphi	k1gNnSc2	Delphi
<g/>
,	,	kIx,	,
Visual	Visual	k1gMnSc1	Visual
Basicu	Basicus	k1gInSc2	Basicus
<g/>
,	,	kIx,	,
PowerBuilderu	PowerBuilder	k1gInSc2	PowerBuilder
<g/>
,	,	kIx,	,
Tcl	Tcl	k1gFnSc2	Tcl
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
<g/>
,	,	kIx,	,
Emacs	Emacs	k1gInSc1	Emacs
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
širokého	široký	k2eAgNnSc2d1	široké
použití	použití	k1gNnSc2	použití
(	(	kIx(	(
<g/>
Linux	linux	k1gInSc1	linux
<g/>
,	,	kIx,	,
Unix	Unix	k1gInSc1	Unix
<g/>
,	,	kIx,	,
MS	MS	kA	MS
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
VMS	VMS	kA	VMS
<g/>
,	,	kIx,	,
Palm	Palm	k1gInSc1	Palm
OS	OS	kA	OS
<g/>
,	,	kIx,	,
Apache	Apache	k1gNnSc2	Apache
<g/>
,	,	kIx,	,
ASP	ASP	kA	ASP
<g/>
,	,	kIx,	,
CGI	CGI	kA	CGI
<g/>
,	,	kIx,	,
ISAPI	ISAPI	kA	ISAPI
<g/>
,	,	kIx,	,
OLE	OLE	kA	OLE
<g/>
,	,	kIx,	,
WSH	WSH	kA	WSH
<g/>
,	,	kIx,	,
WSC	WSC	kA	WSC
<g/>
,	,	kIx,	,
wxPerl	wxPerl	k1gMnSc1	wxPerl
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
převzetí	převzetí	k1gNnSc1	převzetí
osvědčených	osvědčený	k2eAgFnPc2d1	osvědčená
konstrukcí	konstrukce	k1gFnPc2	konstrukce
z	z	k7c2	z
ostatních	ostatní	k2eAgInPc2d1	ostatní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
C	C	kA	C
<g/>
,	,	kIx,	,
AWK	AWK	kA	AWK
<g/>
,	,	kIx,	,
sed	sed	k1gInSc1	sed
<g/>
,	,	kIx,	,
sh	sh	k?	sh
<g/>
,	,	kIx,	,
BASIC	Basic	kA	Basic
<g/>
,	,	kIx,	,
funkcionální	funkcionální	k2eAgInPc4d1	funkcionální
jazyky	jazyk	k1gInPc4	jazyk
(	(	kIx(	(
<g/>
uzávěry	uzávěra	k1gFnPc4	uzávěra
<g/>
,	,	kIx,	,
funkce	funkce	k1gFnPc4	funkce
jako	jako	k8xS	jako
parametry	parametr	k1gInPc4	parametr
nebo	nebo	k8xC	nebo
návratové	návratový	k2eAgFnPc4d1	návratová
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
</s>
</p>
<p>
<s>
reference	reference	k1gFnSc1	reference
na	na	k7c4	na
statické	statický	k2eAgFnPc4d1	statická
<g/>
,	,	kIx,	,
dynamické	dynamický	k2eAgFnPc4d1	dynamická
i	i	k8xC	i
anonymní	anonymní	k2eAgFnPc4d1	anonymní
datové	datový	k2eAgFnPc4d1	datová
struktury	struktura	k1gFnPc4	struktura
</s>
</p>
<p>
<s>
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
procedurální	procedurální	k2eAgMnSc1d1	procedurální
<g/>
,	,	kIx,	,
funkcionální	funkcionální	k2eAgMnSc1d1	funkcionální
i	i	k9	i
objektově	objektově	k6eAd1	objektově
orientované	orientovaný	k2eAgNnSc4d1	orientované
programování	programování	k1gNnSc4	programování
</s>
</p>
<p>
<s>
snadná	snadný	k2eAgFnSc1d1	snadná
práce	práce	k1gFnSc1	práce
s	s	k7c7	s
textem	text	k1gInSc7	text
a	a	k8xC	a
značkovacími	značkovací	k2eAgInPc7d1	značkovací
jazyky	jazyk	k1gInPc7	jazyk
(	(	kIx(	(
<g/>
XML	XML	kA	XML
<g/>
,	,	kIx,	,
HTML	HTML	kA	HTML
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
regulární	regulární	k2eAgInPc4d1	regulární
výrazy	výraz	k1gInPc4	výraz
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
rychlost	rychlost	k1gFnSc4	rychlost
</s>
</p>
<p>
<s>
Perl	perl	k1gInSc1	perl
podporuje	podporovat	k5eAaImIp3nS	podporovat
znakovou	znakový	k2eAgFnSc4d1	znaková
sadu	sada	k1gFnSc4	sada
Unicode	Unicod	k1gInSc5	Unicod
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgMnS	být
<g/>
)	)	kIx)	)
Y2K	Y2K	k1gMnSc7	Y2K
kompatibilní	kompatibilní	k2eAgInSc4d1	kompatibilní
</s>
</p>
<p>
<s>
eval	eval	k1gInSc1	eval
(	(	kIx(	(
<g/>
možnost	možnost	k1gFnSc1	možnost
využít	využít	k5eAaPmF	využít
funkce	funkce	k1gFnPc4	funkce
překladače	překladač	k1gInSc2	překladač
za	za	k7c2	za
běhu	běh	k1gInSc2	běh
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
můžeme	moct	k5eAaImIp1nP	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
dynamický	dynamický	k2eAgInSc4d1	dynamický
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
kód	kód	k1gInSc4	kód
optimalizovat	optimalizovat	k5eAaBmF	optimalizovat
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
za	za	k7c2	za
běhu	běh	k1gInSc2	běh
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
dynamické	dynamický	k2eAgNnSc4d1	dynamické
volání	volání	k1gNnSc4	volání
procedur	procedura	k1gFnPc2	procedura
(	(	kIx(	(
<g/>
název	název	k1gInSc4	název
známe	znát	k5eAaImIp1nP	znát
až	až	k9	až
za	za	k7c2	za
běhu	běh	k1gInSc2	běh
programu	program	k1gInSc2	program
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
stabilita	stabilita	k1gFnSc1	stabilita
–	–	k?	–
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
vyvíjený	vyvíjený	k2eAgInSc1d1	vyvíjený
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
</s>
</p>
<p>
<s>
možnost	možnost	k1gFnSc1	možnost
použití	použití	k1gNnSc2	použití
inline	inlinout	k5eAaPmIp3nS	inlinout
C	C	kA	C
<g/>
/	/	kIx~	/
<g/>
C	C	kA	C
<g/>
++	++	k?	++
v	v	k7c6	v
Perlu	perl	k1gInSc6	perl
nebo	nebo	k8xC	nebo
rozhraní	rozhraní	k1gNnSc6	rozhraní
Perlu	perl	k1gInSc2	perl
pro	pro	k7c4	pro
externí	externí	k2eAgNnSc4d1	externí
C	C	kA	C
<g/>
/	/	kIx~	/
<g/>
C	C	kA	C
<g/>
++	++	k?	++
knihovny	knihovna	k1gFnSc2	knihovna
skrz	skrz	k7c4	skrz
XS	XS	kA	XS
nebo	nebo	k8xC	nebo
SWIG	SWIG	kA	SWIG
</s>
</p>
<p>
<s>
uzávěry	uzávěr	k1gInPc1	uzávěr
(	(	kIx(	(
<g/>
podprogramy	podprogram	k1gInPc1	podprogram
si	se	k3xPyFc3	se
pamatují	pamatovat	k5eAaImIp3nP	pamatovat
proměnné	proměnná	k1gFnPc4	proměnná
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
běhu	běh	k1gInSc3	běh
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
návěstí	návěstí	k1gNnSc1	návěstí
(	(	kIx(	(
<g/>
řízení	řízení	k1gNnSc1	řízení
cyklů	cyklus	k1gInPc2	cyklus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
automatické	automatický	k2eAgNnSc1d1	automatické
zavádění	zavádění	k1gNnSc1	zavádění
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
jméno	jméno	k1gNnSc1	jméno
funkce	funkce	k1gFnSc2	funkce
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
Perl	perl	k1gInSc1	perl
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
spuštění	spuštění	k1gNnSc4	spuštění
definovaného	definovaný	k2eAgInSc2d1	definovaný
kódu	kód	k1gInSc2	kód
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
mod	mod	k?	mod
perl	perl	k1gInSc1	perl
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
web	web	k1gInSc4	web
serveru	server	k1gInSc2	server
Apache	Apach	k1gFnSc2	Apach
vložení	vložení	k1gNnSc2	vložení
Perlu	perla	k1gFnSc4	perla
s	s	k7c7	s
výhodami	výhoda	k1gFnPc7	výhoda
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
vložení	vložení	k1gNnSc6	vložení
PHP	PHP	kA	PHP
</s>
</p>
<p>
<s>
interpret	interpret	k1gMnSc1	interpret
Perlu	perl	k1gInSc2	perl
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vložen	vložen	k2eAgInSc4d1	vložen
(	(	kIx(	(
<g/>
embedded	embedded	k1gInSc4	embedded
<g/>
)	)	kIx)	)
do	do	k7c2	do
ostatních	ostatní	k2eAgInPc2d1	ostatní
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
include	include	k6eAd1	include
<Perl.h>
</s>
</p>
<p>
<s>
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
dokumentace	dokumentace	k1gFnSc1	dokumentace
a	a	k8xC	a
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
komunita	komunita	k1gFnSc1	komunita
kolem	kolem	k7c2	kolem
Perlu	perl	k1gInSc2	perl
<g/>
,	,	kIx,	,
konference	konference	k1gFnSc2	konference
<g/>
,	,	kIx,	,
news	ws	k6eNd1	ws
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
==	==	k?	==
Nevýhody	nevýhoda	k1gFnSc2	nevýhoda
Perlu	perl	k1gInSc2	perl
==	==	k?	==
</s>
</p>
<p>
<s>
nedisciplinovaný	disciplinovaný	k2eNgMnSc1d1	nedisciplinovaný
programátor	programátor	k1gMnSc1	programátor
může	moct	k5eAaImIp3nS	moct
extrémně	extrémně	k6eAd1	extrémně
snadno	snadno	k6eAd1	snadno
vytvářet	vytvářet	k5eAaImF	vytvářet
nesrozumitelný	srozumitelný	k2eNgInSc4d1	nesrozumitelný
kód	kód	k1gInSc4	kód
(	(	kIx(	(
<g/>
jazyk	jazyk	k1gInSc1	jazyk
velice	velice	k6eAd1	velice
benevolentní	benevolentní	k2eAgInSc1d1	benevolentní
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
při	při	k7c6	při
některých	některý	k3yIgFnPc6	některý
aplikacích	aplikace	k1gFnPc6	aplikace
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
projevit	projevit	k5eAaPmF	projevit
neefektivnost	neefektivnost	k1gFnSc1	neefektivnost
dynamicky	dynamicky	k6eAd1	dynamicky
typovaného	typovaný	k2eAgInSc2d1	typovaný
jazyka	jazyk	k1gInSc2	jazyk
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
staticky	staticky	k6eAd1	staticky
typovanými	typovaný	k2eAgInPc7d1	typovaný
jazyky	jazyk	k1gInPc7	jazyk
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
spotřeba	spotřeba	k1gFnSc1	spotřeba
paměti	paměť	k1gFnSc2	paměť
</s>
</p>
<p>
<s>
kruhové	kruhový	k2eAgInPc1d1	kruhový
odkazy	odkaz	k1gInPc1	odkaz
a	a	k8xC	a
problematika	problematika	k1gFnSc1	problematika
jejich	jejich	k3xOp3gFnSc2	jejich
destrukce	destrukce	k1gFnSc2	destrukce
(	(	kIx(	(
<g/>
použití	použití	k1gNnSc4	použití
reference	reference	k1gFnSc2	reference
counting	counting	k1gInSc4	counting
garbage	garbag	k1gFnSc2	garbag
collectoru	collector	k1gInSc2	collector
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
použití	použití	k1gNnSc1	použití
destruktorů	destruktor	k1gInPc2	destruktor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
přeruší	přerušit	k5eAaPmIp3nS	přerušit
kruh	kruh	k1gInSc1	kruh
nebo	nebo	k8xC	nebo
používání	používání	k1gNnSc1	používání
weak	weaka	k1gFnPc2	weaka
[	[	kIx(	[
<g/>
wiː	wiː	k?	wiː
<g/>
]	]	kIx)	]
referencí	reference	k1gFnPc2	reference
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nezvyšují	zvyšovat	k5eNaImIp3nP	zvyšovat
hodnotu	hodnota	k1gFnSc4	hodnota
čítače	čítač	k1gInSc2	čítač
referencí	reference	k1gFnPc2	reference
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
mnozí	mnohý	k2eAgMnPc1d1	mnohý
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jazyk	jazyk	k1gInSc4	jazyk
nevhodný	vhodný	k2eNgInSc4d1	nevhodný
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
programování	programování	k1gNnSc2	programování
<g/>
,	,	kIx,	,
ostatním	ostatní	k2eAgMnPc3d1	ostatní
use	usus	k1gInSc5	usus
strict	strictum	k1gNnPc2	strictum
<g/>
;	;	kIx,	;
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
ve	v	k7c6	v
výuce	výuka	k1gFnSc6	výuka
hrozí	hrozit	k5eAaImIp3nS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
student	student	k1gMnSc1	student
zvykne	zvyknout	k5eAaPmIp3nS	zvyknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
o	o	k7c4	o
spoustu	spousta	k1gFnSc4	spousta
věcí	věc	k1gFnPc2	věc
nemusí	muset	k5eNaImIp3nS	muset
starat	starat	k5eAaImF	starat
a	a	k8xC	a
přechod	přechod	k1gInSc1	přechod
na	na	k7c4	na
nižší	nízký	k2eAgInSc4d2	nižší
jazyk	jazyk	k1gInSc4	jazyk
pak	pak	k6eAd1	pak
bolí	bolet	k5eAaImIp3nP	bolet
</s>
</p>
<p>
<s>
syntaxe	syntaxe	k1gFnSc1	syntaxe
perlu	perl	k1gInSc2	perl
5	[number]	k4	5
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
složitá	složitý	k2eAgFnSc1d1	složitá
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistuje	existovat	k5eNaImIp3nS	existovat
samostatný	samostatný	k2eAgInSc4d1	samostatný
parser	parser	k1gInSc4	parser
ani	ani	k8xC	ani
formální	formální	k2eAgFnPc4d1	formální
definice	definice	k1gFnPc4	definice
syntaxe	syntax	k1gFnSc2	syntax
jazyka	jazyk	k1gInSc2	jazyk
<g/>
;	;	kIx,	;
jediný	jediný	k2eAgMnSc1d1	jediný
parser	parser	k1gMnSc1	parser
perlu	perl	k1gInSc2	perl
řady	řada	k1gFnSc2	řada
5	[number]	k4	5
je	být	k5eAaImIp3nS	být
samotný	samotný	k2eAgMnSc1d1	samotný
interpret	interpret	k1gMnSc1	interpret
perlu	perl	k1gInSc2	perl
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
programátorské	programátorský	k2eAgInPc1d1	programátorský
editory	editor	k1gInPc1	editor
Vim	Vim	k?	Vim
a	a	k8xC	a
Emacs	Emacs	k1gInSc1	Emacs
neumí	umět	k5eNaImIp3nS	umět
pro	pro	k7c4	pro
perl	perl	k1gInSc4	perl
korektně	korektně	k6eAd1	korektně
zvýrazňovat	zvýrazňovat	k5eAaImF	zvýrazňovat
syntaxi	syntaxe	k1gFnSc4	syntaxe
</s>
</p>
<p>
<s>
jazyk	jazyk	k1gInSc1	jazyk
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
funkce	funkce	k1gFnPc4	funkce
typu	typ	k1gInSc2	typ
typemap	typemap	k1gInSc1	typemap
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
provést	provést	k5eAaPmF	provést
kód	kód	k1gInSc4	kód
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
funkce	funkce	k1gFnSc1	funkce
eval	evala	k1gFnPc2	evala
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Budoucí	budoucí	k2eAgInSc1d1	budoucí
vývoj	vývoj	k1gInSc1	vývoj
(	(	kIx(	(
<g/>
Perl	perl	k1gInSc1	perl
6	[number]	k4	6
a	a	k8xC	a
VM	VM	kA	VM
Parrot	Parrot	k1gInSc1	Parrot
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
byl	být	k5eAaImAgInS	být
odstartován	odstartován	k2eAgInSc1d1	odstartován
projekt	projekt	k1gInSc1	projekt
vývoje	vývoj	k1gInSc2	vývoj
jazyka	jazyk	k1gInSc2	jazyk
Perl	perl	k1gInSc1	perl
6	[number]	k4	6
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
započata	započat	k2eAgFnSc1d1	započata
práce	práce	k1gFnSc1	práce
na	na	k7c6	na
návrhu	návrh	k1gInSc6	návrh
virtuálního	virtuální	k2eAgInSc2d1	virtuální
stroje	stroj	k1gInSc2	stroj
(	(	kIx(	(
<g/>
VM	VM	kA	VM
–	–	k?	–
Virtual	Virtual	k1gInSc1	Virtual
Machine	Machin	k1gInSc5	Machin
<g/>
)	)	kIx)	)
s	s	k7c7	s
názvem	název	k1gInSc7	název
Parrot	Parrota	k1gFnPc2	Parrota
<g/>
.	.	kIx.	.
</s>
<s>
Parrot	Parrot	k1gInSc1	Parrot
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
efektivní	efektivní	k2eAgInSc4d1	efektivní
běh	běh	k1gInSc4	běh
dynamických	dynamický	k2eAgInPc2d1	dynamický
programovacích	programovací	k2eAgInPc2d1	programovací
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
primárně	primárně	k6eAd1	primárně
pro	pro	k7c4	pro
Perl	perl	k1gInSc4	perl
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
nové	nový	k2eAgFnSc2d1	nová
verze	verze	k1gFnSc2	verze
jazyka	jazyk	k1gInSc2	jazyk
Perl	perl	k1gInSc1	perl
tak	tak	k6eAd1	tak
bude	být	k5eAaImBp3nS	být
kompletně	kompletně	k6eAd1	kompletně
přepsáno	přepsán	k2eAgNnSc1d1	přepsáno
jako	jako	k8xC	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
např.	např.	kA	např.
u	u	k7c2	u
projektu	projekt	k1gInSc2	projekt
Mozilla	Mozillo	k1gNnSc2	Mozillo
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
syntaxe	syntaxe	k1gFnSc1	syntaxe
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
dosti	dosti	k6eAd1	dosti
zásadně	zásadně	k6eAd1	zásadně
změní	změnit	k5eAaPmIp3nS	změnit
a	a	k8xC	a
úplná	úplný	k2eAgFnSc1d1	úplná
zpětná	zpětný	k2eAgFnSc1d1	zpětná
kompatibilita	kompatibilita	k1gFnSc1	kompatibilita
bude	být	k5eAaImBp3nS	být
zaručena	zaručit	k5eAaPmNgFnS	zaručit
jinými	jiný	k2eAgInPc7d1	jiný
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ukázka	ukázka	k1gFnSc1	ukázka
kódu	kód	k1gInSc2	kód
==	==	k?	==
</s>
</p>
<p>
<s>
Tradiční	tradiční	k2eAgInSc1d1	tradiční
program	program	k1gInSc1	program
"	"	kIx"	"
<g/>
Hello	Hello	k1gNnSc1	Hello
world	worlda	k1gFnPc2	worlda
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
verze	verze	k1gFnSc1	verze
tradičního	tradiční	k2eAgInSc2d1	tradiční
programu	program	k1gInSc2	program
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
výstup	výstup	k1gInSc4	výstup
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Hello	Hello	k1gNnSc1	Hello
<g/>
,	,	kIx,	,
world	world	k1gInSc1	world
<g/>
!	!	kIx.	!
</s>
<s>
The	The	k?	The
magic	magic	k1gMnSc1	magic
number	number	k1gMnSc1	number
is	is	k?	is
234542354	[number]	k4	234542354
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hello	Hello	k1gNnSc1	Hello
<g/>
,	,	kIx,	,
world	world	k1gInSc1	world
<g/>
!	!	kIx.	!
</s>
<s>
The	The	k?	The
magic	magic	k1gMnSc1	magic
number	number	k1gMnSc1	number
is	is	k?	is
-1	-1	k4	-1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
<g/>
:	:	kIx,	:
print	print	k1gInSc1	print
<g/>
;	;	kIx,	;
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
znamená	znamenat	k5eAaImIp3nS	znamenat
print	print	k1gInSc1	print
$	$	kIx~	$
<g/>
_	_	kIx~	_
<g/>
;	;	kIx,	;
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
$	$	kIx~	$
<g/>
_	_	kIx~	_
je	být	k5eAaImIp3nS	být
výchozí	výchozí	k2eAgFnSc1d1	výchozí
proměnná	proměnná	k1gFnSc1	proměnná
<g/>
.	.	kIx.	.
s	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
\	\	kIx~	\
<g/>
d	d	k?	d
<g/>
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
$	$	kIx~	$
<g/>
_	_	kIx~	_
=	=	kIx~	=
<g/>
~	~	kIx~	~
s	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
\	\	kIx~	\
<g/>
d	d	k?	d
<g/>
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
;	;	kIx,	;
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
regulární	regulární	k2eAgInSc1d1	regulární
výraz	výraz	k1gInSc1	výraz
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
nahradí	nahradit	k5eAaPmIp3nS	nahradit
první	první	k4xOgFnSc4	první
nalezenou	nalezený	k2eAgFnSc4d1	nalezená
sekvenci	sekvence	k1gFnSc4	sekvence
číslic	číslice	k1gFnPc2	číslice
(	(	kIx(	(
<g/>
celé	celý	k2eAgNnSc1d1	celé
číslo	číslo	k1gNnSc1	číslo
<g/>
)	)	kIx)	)
číslem	číslo	k1gNnSc7	číslo
−	−	k?	−
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Perl	perl	k1gInSc1	perl
je	být	k5eAaImIp3nS	být
dynamický	dynamický	k2eAgInSc1d1	dynamický
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
používat	používat	k5eAaImF	používat
kompilátor	kompilátor	k1gInSc4	kompilátor
v	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
programu	program	k1gInSc6	program
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
je	být	k5eAaImIp3nS	být
příklad	příklad	k1gInSc1	příklad
kalkulačky	kalkulačka	k1gFnSc2	kalkulačka
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
řádku	řádek	k1gInSc6	řádek
kódu	kód	k1gInSc2	kód
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lze	lze	k6eAd1	lze
pak	pak	k6eAd1	pak
zadat	zadat	k5eAaPmF	zadat
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Složitější	složitý	k2eAgFnSc7d2	složitější
ukázkou	ukázka	k1gFnSc7	ukázka
je	být	k5eAaImIp3nS	být
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vypíše	vypsat	k5eAaPmIp3nS	vypsat
Fibonacciho	Fibonacci	k1gMnSc4	Fibonacci
posloupnost	posloupnost	k1gFnSc1	posloupnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Výstupem	výstup	k1gInSc7	výstup
tohoto	tento	k3xDgInSc2	tento
programu	program	k1gInSc2	program
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
1	[number]	k4	1
2	[number]	k4	2
3	[number]	k4	3
5	[number]	k4	5
8	[number]	k4	8
13	[number]	k4	13
21	[number]	k4	21
34	[number]	k4	34
55	[number]	k4	55
89	[number]	k4	89
</s>
</p>
<p>
<s>
Perl	perl	k1gInSc1	perl
do	do	k7c2	do
verze	verze	k1gFnSc2	verze
5.20	[number]	k4	5.20
(	(	kIx(	(
<g/>
květen	květen	k1gInSc4	květen
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
neumožňoval	umožňovat	k5eNaImAgInS	umožňovat
tradiční	tradiční	k2eAgInSc1d1	tradiční
způsob	způsob	k1gInSc1	způsob
předání	předání	k1gNnSc2	předání
parametrů	parametr	k1gInPc2	parametr
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Hodnoty	hodnota	k1gFnPc1	hodnota
předávané	předávaný	k2eAgFnPc1d1	předávaná
při	při	k7c6	při
volání	volání	k1gNnSc6	volání
funkce	funkce	k1gFnSc2	funkce
jsou	být	k5eAaImIp3nP	být
uloženy	uložen	k2eAgInPc1d1	uložen
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
@	@	kIx~	@
<g/>
_	_	kIx~	_
a	a	k8xC	a
programátor	programátor	k1gMnSc1	programátor
si	se	k3xPyFc3	se
musel	muset	k5eAaImAgMnS	muset
sám	sám	k3xTgMnSc1	sám
zkontrolovat	zkontrolovat	k5eAaPmF	zkontrolovat
kolik	kolik	k4yRc4	kolik
a	a	k8xC	a
jakých	jaký	k3yRgInPc2	jaký
parametrů	parametr	k1gInPc2	parametr
funkce	funkce	k1gFnSc2	funkce
dostala	dostat	k5eAaPmAgFnS	dostat
a	a	k8xC	a
uložit	uložit	k5eAaPmF	uložit
si	se	k3xPyFc3	se
je	on	k3xPp3gFnPc4	on
do	do	k7c2	do
lokálních	lokální	k2eAgFnPc2d1	lokální
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
existují	existovat	k5eAaImIp3nP	existovat
knihovny	knihovna	k1gFnPc1	knihovna
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
pomocí	pomoc	k1gFnSc7	pomoc
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nadeklarovat	nadeklarovat	k5eAaPmF	nadeklarovat
validaci	validace	k1gFnSc4	validace
<g/>
,	,	kIx,	,
výchozí	výchozí	k2eAgFnPc4d1	výchozí
hodnoty	hodnota	k1gFnPc4	hodnota
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Práce	práce	k1gFnSc1	práce
s	s	k7c7	s
proměnnými	proměnná	k1gFnPc7	proměnná
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Perlu	perl	k1gInSc6	perl
poněkud	poněkud	k6eAd1	poněkud
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
kompilovaných	kompilovaný	k2eAgInPc2d1	kompilovaný
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
/	/	kIx~	/
<g/>
C	C	kA	C
<g/>
++	++	k?	++
<g/>
,	,	kIx,	,
Pascal	pascal	k1gInSc1	pascal
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Krása	krása	k1gFnSc1	krása
Perlu	perl	k1gInSc2	perl
==	==	k?	==
</s>
</p>
<p>
<s>
Larry	Larr	k1gMnPc4	Larr
Wall	Wallum	k1gNnPc2	Wallum
považuje	považovat	k5eAaImIp3nS	považovat
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
za	za	k7c4	za
umělce	umělec	k1gMnPc4	umělec
a	a	k8xC	a
Perl	perl	k1gInSc4	perl
za	za	k7c4	za
umělecké	umělecký	k2eAgNnSc4d1	umělecké
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následující	následující	k2eAgInSc1d1	následující
program	program	k1gInSc1	program
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
malá	malý	k2eAgFnSc1d1	malá
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
,	,	kIx,	,
čeho	co	k3yInSc2	co
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
schopen	schopen	k2eAgInSc1d1	schopen
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
vypíše	vypsat	k5eAaPmIp3nS	vypsat
poměrně	poměrně	k6eAd1	poměrně
pomalu	pomalu	k6eAd1	pomalu
slova	slovo	k1gNnSc2	slovo
<g/>
:	:	kIx,	:
Just	just	k6eAd1	just
another	anothra	k1gFnPc2	anothra
Perl	perl	k1gInSc1	perl
/	/	kIx~	/
Unix	Unix	k1gInSc1	Unix
hacker	hacker	k1gMnSc1	hacker
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tím	ten	k3xDgInSc7	ten
účelem	účel	k1gInSc7	účel
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
32	[number]	k4	32
paralelních	paralelní	k2eAgInPc2d1	paralelní
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
složitě	složitě	k6eAd1	složitě
synchronizuje	synchronizovat	k5eAaBmIp3nS	synchronizovat
pomocí	pomocí	k7c2	pomocí
rour	roura	k1gFnPc2	roura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
samozřejmě	samozřejmě	k6eAd1	samozřejmě
není	být	k5eNaImIp3nS	být
doporučený	doporučený	k2eAgInSc4d1	doporučený
ani	ani	k8xC	ani
obvyklý	obvyklý	k2eAgInSc4d1	obvyklý
způsob	způsob	k1gInSc4	způsob
zápisu	zápis	k1gInSc2	zápis
programů	program	k1gInPc2	program
v	v	k7c6	v
Perlu	perl	k1gInSc6	perl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
ukázka	ukázka	k1gFnSc1	ukázka
schválně	schválně	k6eAd1	schválně
neoptimálního	optimální	k2eNgInSc2d1	optimální
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
WALL	WALL	kA	WALL
<g/>
,	,	kIx,	,
Larry	Larra	k1gFnSc2	Larra
<g/>
;	;	kIx,	;
SCHWARTZ	SCHWARTZ	kA	SCHWARTZ
<g/>
,	,	kIx,	,
Randal	Randal	k1gMnSc1	Randal
L.	L.	kA	L.
<g/>
;	;	kIx,	;
CHRISTIANSEN	CHRISTIANSEN	kA	CHRISTIANSEN
<g/>
,	,	kIx,	,
Tom	Tom	k1gMnSc1	Tom
<g/>
.	.	kIx.	.
</s>
<s>
Programování	programování	k1gNnSc1	programování
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Perl	perl	k1gInSc1	perl
(	(	kIx(	(
<g/>
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Programming	Programming	k1gInSc1	Programming
Perl	perl	k1gInSc1	perl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Martin	Martin	k1gMnSc1	Martin
Blažík	Blažík	k1gMnSc1	Blažík
<g/>
;	;	kIx,	;
Editor	editor	k1gInSc1	editor
ing.	ing.	kA	ing.
Martina	Martina	k1gFnSc1	Martina
Mojzesová	Mojzesový	k2eAgFnSc1d1	Mojzesový
<g/>
;	;	kIx,	;
Odborná	odborný	k2eAgFnSc1d1	odborná
korektura	korektura	k1gFnSc1	korektura
ing.	ing.	kA	ing.
Bohumil	Bohumil	k1gMnSc1	Bohumil
Michal	Michal	k1gMnSc1	Michal
<g/>
;	;	kIx,	;
Jazyková	jazykový	k2eAgFnSc1d1	jazyková
korektura	korektura	k1gFnSc1	korektura
Josef	Josef	k1gMnSc1	Josef
Novák	Novák	k1gMnSc1	Novák
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Computer	computer	k1gInSc1	computer
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
683	[number]	k4	683
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85896	[number]	k4	85896
<g/>
-	-	kIx~	-
<g/>
95	[number]	k4	95
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
o	o	k7c6	o
autorech	autor	k1gMnPc6	autor
<g/>
.	.	kIx.	.
</s>
<s>
SRINIVASAN	SRINIVASAN	kA	SRINIVASAN
<g/>
,	,	kIx,	,
Sriram	Sriram	k1gInSc1	Sriram
<g/>
.	.	kIx.	.
</s>
<s>
Programování	programování	k1gNnSc1	programování
v	v	k7c6	v
Perlu	perl	k1gInSc6	perl
pro	pro	k7c4	pro
pokročilé	pokročilý	k2eAgInPc4d1	pokročilý
<g/>
:	:	kIx,	:
základy	základ	k1gInPc1	základ
a	a	k8xC	a
techniky	technika	k1gFnPc1	technika
pro	pro	k7c4	pro
vývojáře	vývojář	k1gMnSc4	vývojář
(	(	kIx(	(
<g/>
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Advanced	Advanced	k1gInSc1	Advanced
Perl	perl	k1gInSc1	perl
programing	programing	k1gInSc1	programing
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Martin	Martin	k1gMnSc1	Martin
Blažík	Blažík	k1gMnSc1	Blažík
<g/>
;	;	kIx,	;
Editor	editor	k1gInSc1	editor
Ivo	Ivo	k1gMnSc1	Ivo
Magera	Magera	k1gFnSc1	Magera
<g/>
;	;	kIx,	;
Jazyková	jazykový	k2eAgFnSc1d1	jazyková
korektura	korektura	k1gFnSc1	korektura
Libor	Libor	k1gMnSc1	Libor
Vyhnálek	Vyhnálek	k1gMnSc1	Vyhnálek
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Computer	computer	k1gInSc1	computer
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
412	[number]	k4	412
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7226	[number]	k4	7226
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
79	[number]	k4	79
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
LEMAY	LEMAY	kA	LEMAY
<g/>
,	,	kIx,	,
Laura	Laura	k1gFnSc1	Laura
<g/>
.	.	kIx.	.
</s>
<s>
Naučte	naučit	k5eAaPmRp2nP	naučit
se	se	k3xPyFc4	se
Perl	perl	k1gInSc1	perl
za	za	k7c4	za
21	[number]	k4	21
dní	den	k1gInPc2	den
(	(	kIx(	(
<g/>
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Teach	Teach	k1gInSc1	Teach
yourself	yourself	k1gInSc1	yourself
Perl	perl	k1gInSc4	perl
in	in	k?	in
21	[number]	k4	21
days	days	k1gInSc1	days
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Bogdan	Bogdany	k1gInPc2	Bogdany
Kiszka	Kiszka	k1gFnSc1	Kiszka
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Černý	Černý	k1gMnSc1	Černý
<g/>
;	;	kIx,	;
Jazyková	jazykový	k2eAgFnSc1d1	jazyková
korektura	korektura	k1gFnSc1	korektura
Eva	Eva	k1gFnSc1	Eva
Bublová	Bublová	k1gFnSc1	Bublová
<g/>
;	;	kIx,	;
Odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
redaktor	redaktor	k1gMnSc1	redaktor
Ivo	Ivo	k1gMnSc1	Ivo
Magera	Magera	k1gFnSc1	Magera
<g/>
;	;	kIx,	;
Technický	technický	k2eAgMnSc1d1	technický
redaktor	redaktor	k1gMnSc1	redaktor
Jiří	Jiří	k1gMnSc1	Jiří
Matoušek	Matoušek	k1gMnSc1	Matoušek
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Computer	computer	k1gInSc1	computer
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
546	[number]	k4	546
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
jistě	jistě	k6eAd1	jistě
<g/>
.	.	kIx.	.
</s>
<s>
Internet	Internet	k1gInSc1	Internet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7226	[number]	k4	7226
<g/>
-	-	kIx~	-
<g/>
616	[number]	k4	616
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Přeloženo	přeložit	k5eAaPmNgNnS	přeložit
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
KYSELA	Kysela	k1gMnSc1	Kysela
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Perl	perl	k1gInSc1	perl
<g/>
:	:	kIx,	:
kompletní	kompletní	k2eAgMnSc1d1	kompletní
kapesní	kapesní	k2eAgMnSc1d1	kapesní
průvodce	průvodce	k1gMnSc1	průvodce
programátora	programátor	k1gMnSc2	programátor
<g/>
.	.	kIx.	.
</s>
<s>
Odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
redaktor	redaktor	k1gMnSc1	redaktor
Miroslav	Miroslav	k1gMnSc1	Miroslav
Lochman	Lochman	k1gMnSc1	Lochman
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
134	[number]	k4	134
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
1170	[number]	k4	1170
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
DAŘENA	dařen	k2eAgFnSc1d1	dařen
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Myslíme	myslet	k5eAaImIp1nP	myslet
v	v	k7c6	v
jazyku	jazyk	k1gInSc6	jazyk
PERL	perl	k1gInSc4	perl
<g/>
.	.	kIx.	.
</s>
<s>
Odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
redaktor	redaktor	k1gMnSc1	redaktor
Miroslav	Miroslav	k1gMnSc1	Miroslav
Lochman	Lochman	k1gMnSc1	Lochman
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
700	[number]	k4	700
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Knihovna	knihovna	k1gFnSc1	knihovna
programátora	programátor	k1gMnSc2	programátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
1147	[number]	k4	1147
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
bibliografii	bibliografie	k1gFnSc4	bibliografie
<g/>
,	,	kIx,	,
bibliografické	bibliografický	k2eAgInPc4d1	bibliografický
odkazy	odkaz	k1gInPc4	odkaz
a	a	k8xC	a
rejstřík	rejstřík	k1gInSc1	rejstřík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Perl	perl	k1gInSc1	perl
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Výukový	výukový	k2eAgInSc1d1	výukový
kurs	kurs	k1gInSc1	kurs
Perl	perl	k1gInSc1	perl
ve	v	k7c6	v
Wikiverzitě	Wikiverzita	k1gFnSc6	Wikiverzita
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CPAN	CPAN	kA	CPAN
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
–	–	k?	–
bohatá	bohatý	k2eAgFnSc1d1	bohatá
kolekce	kolekce	k1gFnSc1	kolekce
software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
modulů	modul	k1gInPc2	modul
<g/>
,	,	kIx,	,
skriptů	skript	k1gInPc2	skript
a	a	k8xC	a
dokumentace	dokumentace	k1gFnSc2	dokumentace
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
metacpan	metacpan	k1gInSc1	metacpan
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
–	–	k?	–
bohatá	bohatý	k2eAgFnSc1d1	bohatá
kolekce	kolekce	k1gFnSc1	kolekce
software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
modulů	modul	k1gInPc2	modul
<g/>
,	,	kIx,	,
skriptů	skript	k1gInPc2	skript
a	a	k8xC	a
dokumentace	dokumentace	k1gFnSc2	dokumentace
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
české	český	k2eAgFnPc4d1	Česká
stránky	stránka	k1gFnPc4	stránka
o	o	k7c4	o
Perl	perl	k1gInSc4	perl
<g/>
6	[number]	k4	6
<g/>
Články	článek	k1gInPc1	článek
a	a	k8xC	a
učebnice	učebnice	k1gFnPc1	učebnice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
seriál	seriál	k1gInSc1	seriál
Perličky	perlička	k1gFnSc2	perlička
na	na	k7c4	na
Root	Root	k1gInSc4	Root
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
seriál	seriál	k1gInSc1	seriál
Regulární	regulární	k2eAgInPc1d1	regulární
výrazy	výraz	k1gInPc1	výraz
na	na	k7c4	na
Root	Root	k1gInSc4	Root
<g/>
.	.	kIx.	.
<g/>
czSouvisející	czSouvisející	k2eAgInPc4d1	czSouvisející
projekty	projekt	k1gInPc4	projekt
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
gtk	gtk	k?	gtk
<g/>
2	[number]	k4	2
<g/>
-perl	erl	k1gInSc1	-perl
–	–	k?	–
psaní	psaní	k1gNnSc1	psaní
GUI	GUI	kA	GUI
aplikací	aplikace	k1gFnPc2	aplikace
v	v	k7c6	v
Perlu	perl	k1gInSc6	perl
nad	nad	k7c7	nad
GTK2	GTK2	k1gFnSc7	GTK2
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
wxPerl	wxPerl	k1gInSc1	wxPerl
–	–	k?	–
psaní	psaní	k1gNnSc1	psaní
GUI	GUI	kA	GUI
aplikací	aplikace	k1gFnPc2	aplikace
v	v	k7c6	v
Perlu	perl	k1gInSc6	perl
pomocí	pomocí	k7c2	pomocí
wxWidgets	wxWidgetsa	k1gFnPc2	wxWidgetsa
</s>
</p>
