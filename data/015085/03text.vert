<s>
Bedřich	Bedřich	k1gMnSc1
Dittel	Dittel	k1gMnSc1
</s>
<s>
Bedřich	Bedřich	k1gMnSc1
Dittel	Dittel	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1695	#num#	k4
nebo	nebo	k8xC
8	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1696	#num#	k4
<g/>
Kadaň	Kadaň	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
katolický	katolický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
katolická	katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Bedřich	Bedřich	k1gMnSc1
Dittel	Dittel	k1gMnSc1
<g/>
,	,	kIx,
variantně	variantně	k6eAd1
Friedrich	Friedrich	k1gMnSc1
Dittel	Dittel	k1gMnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
františkán	františkán	k1gMnSc1
působící	působící	k2eAgFnSc2d1
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
a	a	k8xC
v	v	k7c6
české	český	k2eAgFnSc6d1
řádové	řádový	k2eAgFnSc6d1
provincii	provincie	k1gFnSc6
sv.	sv.	kA
Václava	Václav	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
více	hodně	k6eAd2
konventech	konvent	k1gInPc6
byl	být	k5eAaImAgInS
představeným	představený	k2eAgMnSc7d1
(	(	kIx(
<g/>
kvardiánem	kvardián	k1gMnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1673	#num#	k4
<g/>
–	–	k?
<g/>
1674	#num#	k4
řídil	řídit	k5eAaImAgInS
klášter	klášter	k1gInSc1
ve	v	k7c6
Slaném	Slaný	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
května	květen	k1gInSc2
1675	#num#	k4
do	do	k7c2
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
byl	být	k5eAaImAgInS
kvardiánem	kvardián	k1gMnSc7
v	v	k7c6
Jindřichově	Jindřichův	k2eAgInSc6d1
Hradci	Hradec	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Řádová	řádový	k2eAgFnSc1d1
kapitula	kapitula	k1gFnSc1
v	v	k7c6
květnu	květen	k1gInSc6
1684	#num#	k4
jej	on	k3xPp3gMnSc4
zvolila	zvolit	k5eAaPmAgFnS
členem	člen	k1gInSc7
provinčního	provinční	k2eAgNnSc2d1
definitoria	definitorium	k1gNnSc2
(	(	kIx(
<g/>
definitorem	definitor	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
posléze	posléze	k6eAd1
byl	být	k5eAaImAgInS
též	též	k9
definitorem	definitor	k1gInSc7
doživotním	doživotní	k2eAgInSc7d1
(	(	kIx(
<g/>
habituálním	habituální	k2eAgMnPc3d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Dále	daleko	k6eAd2
působil	působit	k5eAaImAgMnS
jako	jako	k9
hudebník	hudebník	k1gMnSc1
ve	v	k7c6
františkánských	františkánský	k2eAgInPc6d1
chrámech	chrám	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1681	#num#	k4
si	se	k3xPyFc3
opsal	opsat	k5eAaPmAgMnS
zpěvník	zpěvník	k1gInSc4
používaný	používaný	k2eAgInSc4d1
pro	pro	k7c4
doprovod	doprovod	k1gInSc4
mší	mše	k1gFnPc2
nebo	nebo	k8xC
obsahující	obsahující	k2eAgInPc1d1
zpěvy	zpěv	k1gInPc1
žalmů	žalm	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Svazek	svazek	k1gInSc1
zřejmě	zřejmě	k6eAd1
nevytvořil	vytvořit	k5eNaPmAgInS
k	k	k7c3
potřebě	potřeba	k1gFnSc6
nějakého	nějaký	k3yIgInSc2
františkánského	františkánský	k2eAgInSc2d1
kostela	kostel	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
k	k	k7c3
vlastnímu	vlastní	k2eAgNnSc3d1
užívání	užívání	k1gNnSc3
<g/>
,	,	kIx,
neboť	neboť	k8xC
si	se	k3xPyFc3
jej	on	k3xPp3gNnSc4
držel	držet	k5eAaImAgMnS
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Františkán	františkán	k1gMnSc1
Bedřich	Bedřich	k1gMnSc1
Dittel	Dittel	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
Kadani	Kadaň	k1gFnSc6
8	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1695	#num#	k4
nebo	nebo	k8xC
1696	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BENEŠ	Beneš	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Regalát	Regalát	k1gInSc1
OFM	OFM	kA
<g/>
;	;	kIx,
PŘIKRYL	Přikryl	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Františkánský	františkánský	k2eAgInSc1d1
klášter	klášter	k1gInSc1
ve	v	k7c6
Slaném	Slaný	k1gInSc6
:	:	kIx,
1655	#num#	k4
<g/>
-	-	kIx~
<g/>
1950	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slaný	Slaný	k1gInSc1
<g/>
:	:	kIx,
Vlastivědné	vlastivědný	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
239	#num#	k4
<g/>
-	-	kIx~
<g/>
6123	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
51	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
„	„	k?
<g/>
Často	často	k6eAd1
kvardián	kvardián	k1gMnSc1
<g/>
“	“	k?
uvádí	uvádět	k5eAaImIp3nS
u	u	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
jména	jméno	k1gNnSc2
nekrologium	nekrologium	k1gNnSc1
české	český	k2eAgFnSc2d1
františkánské	františkánský	k2eAgFnSc2d1
provincie	provincie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liber	libra	k1gFnPc2
Memorabilium	Memorabilium	k1gNnSc1
conventus	conventus	k1gMnSc1
Novodomensis	Novodomensis	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
144	#num#	k4
-	-	kIx~
strojopisný	strojopisný	k2eAgInSc1d1
přepis	přepis	k1gInSc1
-	-	kIx~
Jihočeská	jihočeský	k2eAgFnSc1d1
vědecká	vědecký	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
<g/>
,	,	kIx,
Zlatá	zlatý	k2eAgFnSc1d1
Koruna	koruna	k1gFnSc1
<g/>
,	,	kIx,
sign	signum	k1gNnPc2
<g/>
.	.	kIx.
1	#num#	k4
JH	JH	kA
54	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
WRBCZANSKY	WRBCZANSKY	kA
<g/>
,	,	kIx,
Severin	Severin	k1gMnSc1
OFM	OFM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nucleus	Nucleus	k1gMnSc1
minoriticus	minoriticus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1746	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Liber	libra	k1gFnPc2
Memorabilium	Memorabilium	k1gNnSc1
conventus	conventus	k1gMnSc1
Novodomensis	Novodomensis	k1gInSc1
(	(	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
147	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nekrologium	nekrologium	k1gNnSc1
české	český	k2eAgFnSc2d1
františkánské	františkánský	k2eAgFnSc2d1
provincie	provincie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
„	„	k?
<g/>
Hic	Hic	k?
liber	libra	k1gFnPc2
missas	missasa	k1gFnPc2
graduale	graduale	k1gNnSc2
antiphonarium	antiphonarium	k1gNnSc1
descripsit	descripsit	k1gInSc1
Fridericus	Fridericus	k1gMnSc1
Dittel	Dittel	k1gMnSc1
<g/>
“	“	k?
je	být	k5eAaImIp3nS
uveden	uvést	k5eAaPmNgInS
tento	tento	k3xDgInSc1
rukopis	rukopis	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1681	#num#	k4
<g/>
,	,	kIx,
do	do	k7c2
roku	rok	k1gInSc2
1950	#num#	k4
uložený	uložený	k2eAgInSc1d1
v	v	k7c6
klášterní	klášterní	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
v	v	k7c6
Kadani	Kadaň	k1gFnSc6
<g/>
,	,	kIx,
po	po	k7c6
restitucích	restituce	k1gFnPc6
klášterních	klášterní	k2eAgFnPc2d1
knihoven	knihovna	k1gFnPc2
v	v	k7c6
bibliotéce	bibliotéka	k1gFnSc6
kláštera	klášter	k1gInSc2
u	u	k7c2
P.	P.	kA
Marie	Maria	k1gFnSc2
Sněžné	sněžný	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Rok	rok	k1gInSc1
1696	#num#	k4
uvádí	uvádět	k5eAaImIp3nS
nekrologium	nekrologium	k1gNnSc4
české	český	k2eAgFnSc2d1
františkánské	františkánský	k2eAgFnSc2d1
provincie	provincie	k1gFnSc2
<g/>
,	,	kIx,
elektronický	elektronický	k2eAgInSc4d1
přepis	přepis	k1gInSc4
z	z	k7c2
více	hodně	k6eAd2
pramenů	pramen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úmrtí	úmrtí	k1gNnPc1
v	v	k7c6
roce	rok	k1gInSc6
1695	#num#	k4
viz	vidět	k5eAaImRp2nS
BENEŠ	Beneš	k1gMnSc1
-	-	kIx~
PŘIKRYL	Přikryl	k1gMnSc1
<g/>
,	,	kIx,
Františkánský	františkánský	k2eAgInSc1d1
klášter	klášter	k1gInSc1
ve	v	k7c6
Slaném	Slaný	k1gInSc6
(	(	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
51	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ola	ola	k?
<g/>
2015886948	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
207144647692615012444	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
</s>
