<s>
Fullereny	fulleren	k1gInPc1
jsou	být	k5eAaImIp3nP
molekuly	molekula	k1gFnPc1
<g/>
,	,	kIx,
tvořené	tvořený	k2eAgInPc1d1
atomy	atom	k1gInPc1
uhlíku	uhlík	k1gInSc2
uspořádanými	uspořádaný	k2eAgInPc7d1
do	do	k7c2
vrstvy	vrstva	k1gFnSc2
z	z	k7c2
pěti-	pěti-	k?
a	a	k8xC
šestiúhelníků	šestiúhelník	k1gInPc2
s	s	k7c7
atomy	atom	k1gInPc7
ve	v	k7c6
vrcholech	vrchol	k1gInPc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
prostorově	prostorově	k6eAd1
svinuta	svinut	k2eAgFnSc1d1
do	do	k7c2
uzavřeného	uzavřený	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
(	(	kIx(
<g/>
nejčastěji	často	k6eAd3
do	do	k7c2
tvaru	tvar	k1gInSc2
koule	koule	k1gFnSc2
nebo	nebo	k8xC
elipsoidu	elipsoid	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>