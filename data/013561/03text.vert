<s>
Fullereny	fulleren	k1gInPc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Fulleren	fulleren	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
francouzské	francouzský	k2eAgFnSc6d1
obci	obec	k1gFnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Fulleren	fulleren	k1gInSc1
(	(	kIx(
<g/>
Haut-Rhin	Haut-Rhin	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Fulleren	fulleren	k1gInSc1
C540	C540	k1gFnSc2
</s>
<s>
Fullereny	fulleren	k1gInPc1
jsou	být	k5eAaImIp3nP
molekuly	molekula	k1gFnPc1
<g/>
,	,	kIx,
tvořené	tvořený	k2eAgInPc1d1
atomy	atom	k1gInPc1
uhlíku	uhlík	k1gInSc2
uspořádanými	uspořádaný	k2eAgInPc7d1
do	do	k7c2
vrstvy	vrstva	k1gFnSc2
z	z	k7c2
pěti-	pěti-	k?
a	a	k8xC
šestiúhelníků	šestiúhelník	k1gInPc2
s	s	k7c7
atomy	atom	k1gInPc7
ve	v	k7c6
vrcholech	vrchol	k1gInPc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
prostorově	prostorově	k6eAd1
svinuta	svinut	k2eAgFnSc1d1
do	do	k7c2
uzavřeného	uzavřený	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
(	(	kIx(
<g/>
nejčastěji	často	k6eAd3
do	do	k7c2
tvaru	tvar	k1gInSc2
koule	koule	k1gFnSc2
nebo	nebo	k8xC
elipsoidu	elipsoid	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
této	tento	k3xDgFnSc3
struktuře	struktura	k1gFnSc3
jsou	být	k5eAaImIp3nP
mimořádně	mimořádně	k6eAd1
odolné	odolný	k2eAgFnSc2d1
vůči	vůči	k7c3
vnějším	vnější	k2eAgInPc3d1
fyzikálním	fyzikální	k2eAgInPc3d1
vlivům	vliv	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dutině	dutina	k1gFnSc6
molekuly	molekula	k1gFnSc2
fullerenu	fulleren	k1gInSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
uzavřený	uzavřený	k2eAgInSc4d1
jiný	jiný	k2eAgInSc4d1
atom	atom	k1gInSc4
<g/>
,	,	kIx,
několik	několik	k4yIc4
atomů	atom	k1gInPc2
či	či	k8xC
malá	malý	k2eAgFnSc1d1
molekula	molekula	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Zatím	zatím	k6eAd1
nejstabilnější	stabilní	k2eAgInSc1d3
známý	známý	k2eAgInSc1d1
fulleren	fulleren	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
60	#num#	k4
atomů	atom	k1gInPc2
uhlíku	uhlík	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fullereny	fulleren	k1gInPc1
se	se	k3xPyFc4
uměle	uměle	k6eAd1
připravují	připravovat	k5eAaImIp3nP
nejčastěji	často	k6eAd3
v	v	k7c6
elektrickém	elektrický	k2eAgInSc6d1
oblouku	oblouk	k1gInSc6
s	s	k7c7
uhlíkatými	uhlíkatý	k2eAgFnPc7d1
elektrodami	elektroda	k1gFnPc7
nebo	nebo	k8xC
pyrolýzou	pyrolýza	k1gFnSc7
organických	organický	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
v	v	k7c6
kontrolovaném	kontrolovaný	k2eAgInSc6d1
plameni	plamen	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Fullereny	fulleren	k1gInPc1
byly	být	k5eAaImAgInP
nazvány	nazvat	k5eAaPmNgInP,k5eAaBmNgInP
po	po	k7c6
americkém	americký	k2eAgInSc6d1
architektovi	architekt	k1gMnSc3
Buckminsteru	Buckminster	k1gMnSc3
Fullerovi	Fuller	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
projektoval	projektovat	k5eAaBmAgInS
geodetické	geodetický	k2eAgFnPc4d1
kopule	kopule	k1gFnPc4
podobného	podobný	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
objev	objev	k1gInSc4
a	a	k8xC
studium	studium	k1gNnSc4
vlastností	vlastnost	k1gFnPc2
fullerenů	fulleren	k1gInPc2
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
udělena	udělen	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Nobelova	Nobelův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
za	za	k7c4
chemii	chemie	k1gFnSc4
Robertu	Roberta	k1gFnSc4
F.	F.	kA
Curlovi	Curlův	k2eAgMnPc1d1
a	a	k8xC
Richardu	Richarda	k1gFnSc4
E.	E.	kA
Smalleymu	Smalleym	k1gInSc2
a	a	k8xC
Haroldu	Harold	k1gInSc2
W.	W.	kA
Krotoovi	Krotoa	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
výzkum	výzkum	k1gInSc4
vlastností	vlastnost	k1gFnPc2
a	a	k8xC
metod	metoda	k1gFnPc2
přípravy	příprava	k1gFnSc2
fullerenů	fulleren	k1gInPc2
velmi	velmi	k6eAd1
intenzivně	intenzivně	k6eAd1
studován	studovat	k5eAaImNgInS
na	na	k7c6
řadě	řada	k1gFnSc6
vědeckých	vědecký	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
(	(	kIx(
<g/>
v	v	k7c6
Česku	Česko	k1gNnSc6
od	od	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
zkoumal	zkoumat	k5eAaImAgInS
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
průkopník	průkopník	k1gMnSc1
jejich	jejich	k3xOp3gFnPc2
nanotechnologických	nanotechnologický	k2eAgFnPc2d1
užití	užití	k1gNnPc2
Zd	Zd	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slanina	slanina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Objev	objev	k1gInSc1
a	a	k8xC
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
předpověděl	předpovědět	k5eAaPmAgMnS
P.	P.	kA
R.	R.	kA
Buseck	Busecka	k1gFnPc2
<g/>
,	,	kIx,
že	že	k8xS
fullereny	fulleren	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
nalezeny	naleznout	k5eAaPmNgInP,k5eAaBmNgInP
ve	v	k7c6
fulguritech	fulgurit	k1gInPc6
neboli	neboli	k8xC
sklech	sklo	k1gNnPc6
protavených	protavený	k2eAgFnPc2d1
úderem	úder	k1gInSc7
blesku	blesk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
tento	tento	k3xDgInSc4
předpoklad	předpoklad	k1gInSc4
potvrdil	potvrdit	k5eAaPmAgMnS
T.	T.	kA
K.	K.	kA
Dally	Dalla	k1gMnSc2
při	při	k7c6
výzkumu	výzkum	k1gInSc6
fulguritu	fulgurit	k1gInSc2
ze	z	k7c2
Sheep	Sheep	k1gInSc1
Mountain	Mountain	k1gInSc1
v	v	k7c6
Coloradu	Colorado	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Systematicky	systematicky	k6eAd1
byly	být	k5eAaImAgFnP
prozkoumány	prozkoumat	k5eAaPmNgInP
fullereny	fulleren	k1gInPc1
až	až	k8xS
do	do	k7c2
molekuly	molekula	k1gFnSc2
obsahující	obsahující	k2eAgInSc4d1
96	#num#	k4
atomů	atom	k1gInPc2
uhlíku	uhlík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organická	organický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
se	se	k3xPyFc4
nejvíce	hodně	k6eAd3,k6eAd1
rozvíjí	rozvíjet	k5eAaImIp3nS
kolem	kolem	k7c2
molekul	molekula	k1gFnPc2
s	s	k7c7
60	#num#	k4
a	a	k8xC
70	#num#	k4
atomy	atom	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
metallofullereny	metallofulleren	k1gInPc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
fullereny	fulleren	k1gInPc1
které	který	k3yQgMnPc4,k3yIgMnPc4,k3yRgMnPc4
mají	mít	k5eAaImIp3nP
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
dutině	dutina	k1gFnSc6
umístěn	umístit	k5eAaPmNgInS
atom	atom	k1gInSc1
či	či	k8xC
i	i	k9
několik	několik	k4yIc1
atomů	atom	k1gInPc2
kovu	kov	k1gInSc2
(	(	kIx(
<g/>
La	la	k1gNnSc1
<g/>
,	,	kIx,
Ca	ca	kA
<g/>
,	,	kIx,
Li	li	k8xS
<g/>
;	;	kIx,
ale	ale	k8xC
i	i	k8xC
He	he	k0
<g/>
,	,	kIx,
N	N	kA
-	-	kIx~
tzv.	tzv.	kA
endohedrální	endohedrální	k2eAgInPc1d1
fullereny	fulleren	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	se	k3xPyFc4
jako	jako	k9
základ	základ	k1gInSc1
používají	používat	k5eAaImIp3nP
i	i	k9
vyšší	vysoký	k2eAgFnPc1d2
molekuly	molekula	k1gFnPc1
jako	jako	k9
třeba	třeba	k6eAd1
C80	C80	k1gFnSc1
či	či	k8xC
C	C	kA
<g/>
82	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
fullerenů	fulleren	k1gInPc2
se	se	k3xPyFc4
odvozují	odvozovat	k5eAaImIp3nP
i	i	k9
uhlíkaté	uhlíkatý	k2eAgFnPc1d1
nanotrubičky	nanotrubička	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgFnPc1
na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
respektují	respektovat	k5eAaImIp3nP
hlavní	hlavní	k2eAgInSc4d1
topologický	topologický	k2eAgInSc4d1
rys	rys	k1gInSc4
fullerenů	fulleren	k1gInPc2
-	-	kIx~
výstavba	výstavba	k1gFnSc1
z	z	k7c2
proměnlivého	proměnlivý	k2eAgInSc2d1
počtu	počet	k1gInSc2
šestiúhelníků	šestiúhelník	k1gMnPc2
a	a	k8xC
dvanácti	dvanáct	k4xCc2
pětiúhelníků	pětiúhelník	k1gInPc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInSc7
typickým	typický	k2eAgInSc7d1
tvarem	tvar	k1gInSc7
je	být	k5eAaImIp3nS
ale	ale	k9
protažený	protažený	k2eAgInSc1d1
válec	válec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastní	vlastní	k2eAgInPc1d1
fullereny	fulleren	k1gInPc1
přitom	přitom	k6eAd1
bývají	bývat	k5eAaImIp3nP
tvarem	tvar	k1gInSc7
poměrně	poměrně	k6eAd1
blízké	blízký	k2eAgFnSc6d1
kouli	koule	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Buckminsterfullerene	Buckminsterfulleren	k1gMnSc5
(	(	kIx(
<g/>
C	C	kA
<g/>
60	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nejnověji	nově	k6eAd3
se	se	k3xPyFc4
ukazuje	ukazovat	k5eAaImIp3nS
možnost	možnost	k1gFnSc1
připravit	připravit	k5eAaPmF
i	i	k9
fullereny	fulleren	k1gInPc4
menší	malý	k2eAgInPc4d2
než	než	k8xS
C	C	kA
<g/>
60	#num#	k4
<g/>
,	,	kIx,
např.	např.	kA
C	C	kA
<g/>
36	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
molekuly	molekula	k1gFnPc1
tvaru	tvar	k1gInSc2
fullerenů	fulleren	k1gInPc2
složené	složený	k2eAgFnSc2d1
i	i	k8xC
z	z	k7c2
jiných	jiný	k2eAgInPc2d1
prvků	prvek	k1gInPc2
než	než	k8xS
z	z	k7c2
uhlíku	uhlík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tzv.	tzv.	kA
BN-fullereny	BN-fullerena	k1gFnSc2
například	například	k6eAd1
obsahují	obsahovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
bór	bór	k1gInSc4
a	a	k8xC
dusík	dusík	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Pevné	pevný	k2eAgFnPc1d1
krystalické	krystalický	k2eAgFnPc1d1
formy	forma	k1gFnPc1
fullerenů	fulleren	k1gInPc2
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
fullerity	fullerit	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stlačením	stlačení	k1gNnSc7
fulleritu	fullerit	k1gInSc2
za	za	k7c4
vysoké	vysoký	k2eAgFnPc4d1
teploty	teplota	k1gFnPc4
lze	lze	k6eAd1
připravit	připravit	k5eAaPmF
mikrokrystalický	mikrokrystalický	k2eAgInSc4d1
diamant	diamant	k1gInSc4
zajímavých	zajímavý	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
vnitřní	vnitřní	k2eAgFnSc2d1
dutiny	dutina	k1gFnSc2
fullerenů	fulleren	k1gInPc2
lze	lze	k6eAd1
vložit	vložit	k5eAaPmF
(	(	kIx(
<g/>
enkapsulovat	enkapsulovat	k5eAaPmF,k5eAaImF,k5eAaBmF
<g/>
)	)	kIx)
některé	některý	k3yIgFnPc4
malé	malý	k2eAgFnPc4d1
molekuly	molekula	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
např.	např.	kA
vodík	vodík	k1gInSc1
<g/>
,	,	kIx,
dusík	dusík	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Sc	Sc	k?
<g/>
3	#num#	k4
<g/>
N	N	kA
či	či	k8xC
vodu	voda	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Takto	takto	k6eAd1
vložené	vložený	k2eAgFnPc1d1
molekuly	molekula	k1gFnPc1
pak	pak	k6eAd1
ovlivňují	ovlivňovat	k5eAaImIp3nP
výsledné	výsledný	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
fullerenů	fulleren	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Typy	typ	k1gInPc1
fullerenů	fulleren	k1gInPc2
</s>
<s>
buckyball	buckyball	k1gInSc1
klastry	klastr	k1gInPc1
<g/>
:	:	kIx,
nejmenší	malý	k2eAgFnPc1d3
z	z	k7c2
nich	on	k3xPp3gFnPc2
je	být	k5eAaImIp3nS
C	C	kA
<g/>
20	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
nejčastější	častý	k2eAgInSc1d3
typ	typ	k1gInSc1
je	být	k5eAaImIp3nS
C60	C60	k1gFnSc4
</s>
<s>
polymerní	polymerní	k2eAgFnSc1d1
<g/>
:	:	kIx,
řetěze	řetěz	k1gInSc2
dvou	dva	k4xCgNnPc2
a	a	k8xC
třídimenzionálních	třídimenzionální	k2eAgInPc2d1
polymerů	polymer	k1gInPc2
utvářených	utvářený	k2eAgInPc2d1
pod	pod	k7c7
vysokým	vysoký	k2eAgInSc7d1
tlakem	tlak	k1gInSc7
a	a	k8xC
teplotou	teplota	k1gFnSc7
<g/>
;	;	kIx,
</s>
<s>
nano	nano	k1gNnSc1
<g/>
"	"	kIx"
<g/>
onions	onions	k1gInSc1
<g/>
"	"	kIx"
<g/>
:	:	kIx,
vícevrstvé	vícevrstvý	k2eAgInPc1d1
sférické	sférický	k2eAgInPc1d1
fullereny	fulleren	k1gInPc1
(	(	kIx(
<g/>
využití	využití	k1gNnSc1
jako	jako	k8xS,k8xC
mazivo	mazivo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
buckypaper	buckypaper	k1gInSc1
<g/>
:	:	kIx,
experimentální	experimentální	k2eAgInPc1d1
fullereno-kompozitní	fullereno-kompozitní	k2eAgInPc1d1
materiály	materiál	k1gInPc1
</s>
<s>
Strukturu	struktura	k1gFnSc4
blízkou	blízký	k2eAgFnSc4d1
fullerenům	fulleren	k1gInPc3
(	(	kIx(
<g/>
tedy	tedy	k9
tvořené	tvořený	k2eAgFnPc1d1
vrstvou	vrstva	k1gFnSc7
nebo	nebo	k8xC
několika	několik	k4yIc7
málo	málo	k6eAd1
vrstvami	vrstva	k1gFnPc7
atomů	atom	k1gInPc2
uhlíku	uhlík	k1gInSc2
uspořádaných	uspořádaný	k2eAgFnPc2d1
do	do	k7c2
pětiúhelníků	pětiúhelník	k1gInPc2
a	a	k8xC
šestiúhelníků	šestiúhelník	k1gInPc2
<g/>
)	)	kIx)
mají	mít	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
Grafen	Grafen	k1gInSc1
(	(	kIx(
<g/>
nesvinutá	svinutý	k2eNgFnSc1d1
vrstva	vrstva	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Uhlíkové	Uhlíkové	k2eAgFnPc1d1
nanotrubičky	nanotrubička	k1gFnPc1
(	(	kIx(
<g/>
vrstva	vrstva	k1gFnSc1
svinuta	svinut	k2eAgFnSc1d1
do	do	k7c2
válcového	válcový	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Příprava	příprava	k1gFnSc1
fullerenů	fulleren	k1gInPc2
</s>
<s>
Jsou	být	k5eAaImIp3nP
známy	znám	k2eAgInPc1d1
čtyři	čtyři	k4xCgInPc1
způsoby	způsob	k1gInPc1
přípravy	příprava	k1gFnSc2
pyrolýzou	pyrolýza	k1gFnSc7
<g/>
:	:	kIx,
</s>
<s>
Rozhodující	rozhodující	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
přípravy	příprava	k1gFnSc2
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
vypařování	vypařování	k1gNnSc1
grafitu	grafit	k1gInSc2
v	v	k7c6
elektrickém	elektrický	k2eAgInSc6d1
oblouku	oblouk	k1gInSc6
v	v	k7c6
atmosféře	atmosféra	k1gFnSc6
inertního	inertní	k2eAgInSc2d1
plynu	plyn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Metoda	metoda	k1gFnSc1
přípravy	příprava	k1gFnSc2
fullerenu	fulleren	k1gInSc2
v	v	k7c6
plamenech	plamen	k1gInPc6
různých	různý	k2eAgFnPc2d1
organických	organický	k2eAgFnPc2d1
látek	látka	k1gFnPc2
se	se	k3xPyFc4
zatím	zatím	k6eAd1
příliš	příliš	k6eAd1
nevžila	vžít	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Třetí	třetí	k4xOgFnSc1
metoda	metoda	k1gFnSc1
pracuje	pracovat	k5eAaImIp3nS
se	s	k7c7
slunečním	sluneční	k2eAgNnSc7d1
zářením	záření	k1gNnSc7
koncentrovaným	koncentrovaný	k2eAgNnSc7d1
pomocí	pomoc	k1gFnSc7
zrcadla	zrcadlo	k1gNnSc2
do	do	k7c2
ohniska	ohnisko	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
je	být	k5eAaImIp3nS
umístěn	umístěn	k2eAgInSc1d1
grafit	grafit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
pyrolýzy	pyrolýza	k1gFnPc4
organických	organický	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
laserem	laser	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Postupný	postupný	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
průmyslové	průmyslový	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
významně	významně	k6eAd1
snížil	snížit	k5eAaPmAgInS
ceny	cena	k1gFnPc4
fullerenů	fulleren	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časově	časově	k6eAd1
nejnáročnější	náročný	k2eAgFnSc2d3
fází	fáze	k1gFnSc7
celého	celý	k2eAgInSc2d1
procesu	proces	k1gInSc2
je	být	k5eAaImIp3nS
separace	separace	k1gFnSc1
jednotlivých	jednotlivý	k2eAgInPc2d1
fullerenů	fulleren	k1gInPc2
pomocí	pomocí	k7c2
kapalinové	kapalinový	k2eAgFnSc2d1
chromatografie	chromatografie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
fullerenů	fulleren	k1gInPc2
</s>
<s>
Největší	veliký	k2eAgInPc1d3
prostředky	prostředek	k1gInPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
současnosti	současnost	k1gFnSc6
vynakládány	vynakládán	k2eAgFnPc1d1
na	na	k7c4
výzkum	výzkum	k1gInSc4
fullerenů	fulleren	k1gInPc2
jako	jako	k8xC,k8xS
nových	nový	k2eAgInPc2d1
perspektivních	perspektivní	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
pro	pro	k7c4
techniku	technika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejdůležitější	důležitý	k2eAgFnPc4d3
vlastnosti	vlastnost	k1gFnPc4
patří	patřit	k5eAaImIp3nS
jejich	jejich	k3xOp3gNnSc1
supravodivost	supravodivost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukázalo	ukázat	k5eAaPmAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
vytvářet	vytvářet	k5eAaImF
sloučeniny	sloučenina	k1gFnPc1
C60	C60	k1gFnPc2
s	s	k7c7
alkalickými	alkalický	k2eAgInPc7d1
kovy	kov	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
jsou	být	k5eAaImIp3nP
supravodivé	supravodivý	k2eAgFnPc1d1
při	při	k7c6
teplotách	teplota	k1gFnPc6
18	#num#	k4
K	k	k7c3
i	i	k8xC
vyšších	vysoký	k2eAgFnPc6d2
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgInSc7d1
perspektivním	perspektivní	k2eAgInSc7d1
oborem	obor	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
předpokládat	předpokládat	k5eAaImF
jejich	jejich	k3xOp3gNnSc4
využití	využití	k1gNnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
lékařství	lékařství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
by	by	kYmCp3nS
je	on	k3xPp3gMnPc4
šlo	jít	k5eAaImAgNnS
využít	využít	k5eAaPmF
jako	jako	k8xC,k8xS
léčiva	léčivo	k1gNnPc4
na	na	k7c4
AIDS	AIDS	kA
<g/>
.	.	kIx.
</s>
<s>
Již	již	k6eAd1
od	od	k7c2
počátku	počátek	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jsou	být	k5eAaImIp3nP
fullereny	fulleren	k1gInPc1
využívány	využívat	k5eAaPmNgInP,k5eAaImNgInP
jako	jako	k8xS,k8xC
mikrokomponenty	mikrokomponenta	k1gFnPc1
v	v	k7c6
nanoinženýrství	nanoinženýrství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
</s>
<s>
PETRÁSEK	Petrásek	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svět	svět	k1gInSc1
vesmíru	vesmír	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005-11-24	2005-11-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Diamanty	diamant	k1gInPc4
už	už	k6eAd1
nejsou	být	k5eNaImIp3nP
nejtvrdší	tvrdý	k2eAgFnPc1d3
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
</s>
<s>
KRATSCHMER	KRATSCHMER	kA
<g/>
,	,	kIx,
W.	W.	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
new	new	k?
form	form	k1gInSc1
of	of	k?
Carbon	Carbon	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
354	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
347354	#num#	k4
<g/>
a	a	k8xC
<g/>
0	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Slanina	Slanina	k1gMnSc1
<g/>
,	,	kIx,
Z.	Z.	kA
<g/>
,	,	kIx,
První	první	k4xOgFnSc1
Nobelova	Nobelův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
pro	pro	k7c4
fullereny	fulleren	k1gInPc4
<g/>
:	:	kIx,
http://chemicke-listy.cz/Bulletin/bulletin281/970101.html	http://chemicke-listy.cz/Bulletin/bulletin281/970101.html	k1gMnSc1
<g/>
↑	↑	k?
Až	až	k9
do	do	k7c2
(	(	kIx(
<g/>
druhého	druhý	k4xOgInSc2
<g/>
)	)	kIx)
administrativního	administrativní	k2eAgInSc2d1
zákazu	zákaz	k1gInSc2
<g/>
,	,	kIx,
blíže	blízce	k6eAd2
<g/>
:	:	kIx,
http://www.scienceworld.cz/ostatni/dopis-ctenare-dusene-fullereny-po-cesku-4182/?switch_theme=mobile	http://www.scienceworld.cz/ostatni/dopis-ctenare-dusene-fullereny-po-cesku-4182/?switch_theme=mobile	k6eAd1
</s>
<s>
http://www.scienceworld.cz/neziva-priroda/dopis-ctenare-o-pocatcich-molekulove-elektroniky-v-cechach-4226/?switch_theme=mobile	http://www.scienceworld.cz/neziva-priroda/dopis-ctenare-o-pocatcich-molekulove-elektroniky-v-cechach-4226/?switch_theme=mobile	k6eAd1
<g/>
↑	↑	k?
Slanina	Slanina	k1gMnSc1
<g/>
,	,	kIx,
Z.	Z.	kA
<g/>
,	,	kIx,
Obří	obří	k2eAgFnPc4d1
molekuly	molekula	k1gFnPc4
uhlíku	uhlík	k1gInSc2
<g/>
,	,	kIx,
Vesmír	vesmír	k1gInSc1
67	#num#	k4
<g/>
,	,	kIx,
13	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
č.	č.	k?
<g/>
1	#num#	k4
<g/>
↑	↑	k?
Dokonce	dokonce	k9
i	i	k9
dimér	dimér	k1gMnSc1
vody	voda	k1gFnSc2
<g/>
:	:	kIx,
http://www.tandfonline.com/doi/full/10.1080/1536383X.2015.1072515#abstract	http://www.tandfonline.com/doi/full/10.1080/1536383X.2015.1072515#abstract	k1gInSc1
<g/>
↑	↑	k?
Vougioukalakis	Vougioukalakis	k1gInSc1
<g/>
,	,	kIx,
Gc	Gc	k1gFnSc1
<g/>
;	;	kIx,
Roubelakis	Roubelakis	k1gInSc1
<g/>
,	,	kIx,
Mm	mm	kA
<g/>
;	;	kIx,
Orfanopoulos	Orfanopoulos	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
Open-cage	Open-cage	k1gInSc1
fullerenes	fullerenes	k1gInSc1
<g/>
:	:	kIx,
towards	towards	k1gInSc1
the	the	k?
construction	construction	k1gInSc1
of	of	k?
nanosized	nanosized	k1gInSc1
molecular	molecular	k1gMnSc1
containers	containers	k1gInSc1
<g/>
..	..	k?
Chemical	Chemical	k1gFnSc1
Society	societa	k1gFnSc2
Reviews	Reviewsa	k1gFnPc2
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
817	#num#	k4
<g/>
–	–	k?
<g/>
44	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
b	b	k?
<g/>
913766	#num#	k4
<g/>
a.	a.	k?
PMID	PMID	kA
20111794	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hummelen	Hummelen	k2eAgInSc1d1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
C.	C.	kA
<g/>
;	;	kIx,
PRATO	PRATO	kA
<g/>
,	,	kIx,
Maurizio	Maurizio	k1gMnSc1
<g/>
;	;	kIx,
WUDL	WUDL	kA
<g/>
,	,	kIx,
Fred	Fred	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
There	Ther	k1gInSc5
Is	Is	k1gFnSc1
a	a	k8xC
Hole	hole	k1gFnSc1
in	in	k?
My	my	k3xPp1nPc1
Bucky	bucek	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gInSc4
Chemical	Chemical	k1gMnSc2
Society	societa	k1gFnSc2
<g/>
.	.	kIx.
1995	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
7003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ja	ja	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
131	#num#	k4
<g/>
a	a	k8xC
<g/>
0	#num#	k4
<g/>
24	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Roubelakis	Roubelakis	k1gInSc1
<g/>
,	,	kIx,
Mm	mm	kA
<g/>
;	;	kIx,
Vougioukalakis	Vougioukalakis	k1gFnSc1
<g/>
,	,	kIx,
Gc	Gc	k1gFnSc1
<g/>
;	;	kIx,
Orfanopoulos	Orfanopoulos	k1gInSc1
<g/>
,	,	kIx,
M.	M.	kA
Open-cage	Open-cage	k1gInSc1
fullerene	fulleren	k1gInSc5
derivatives	derivatives	k1gInSc4
having	having	k1gInSc1
11	#num#	k4
<g/>
-	-	kIx~
<g/>
,	,	kIx,
12	#num#	k4
<g/>
-	-	kIx~
<g/>
,	,	kIx,
and	and	k?
13	#num#	k4
<g/>
-membered-ring	-membered-ring	k1gInSc1
orifices	orifices	k1gInSc4
<g/>
:	:	kIx,
chemical	chemicat	k5eAaPmAgInS
transformations	transformations	k1gInSc1
of	of	k?
the	the	k?
organic	organice	k1gFnPc2
addends	addends	k6eAd1
on	on	k3xPp3gMnSc1
the	the	k?
rim	rim	k?
of	of	k?
the	the	k?
orifice	orifice	k1gFnSc1
<g/>
..	..	k?
The	The	k1gFnSc1
Journal	Journal	k1gFnPc2
of	of	k?
organic	organice	k1gFnPc2
chemistry	chemistr	k1gMnPc7
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
6526	#num#	k4
<g/>
–	–	k?
<g/>
33	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22	#num#	k4
<g/>
-	-	kIx~
<g/>
3263	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
jo	jo	k9
<g/>
0	#num#	k4
<g/>
70796	#num#	k4
<g/>
l.	l.	k?
PMID	PMID	kA
17655360	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Příprava	příprava	k1gFnSc1
He	he	k0
<g/>
@	@	kIx~
<g/>
C	C	kA
<g/>
60	#num#	k4
a	a	k8xC
He	he	k0
<g/>
2	#num#	k4
<g/>
@	@	kIx~
<g/>
C	C	kA
<g/>
60	#num#	k4
pomocí	pomocí	k7c2
exploze	exploze	k1gFnSc2
<g/>
.	.	kIx.
www.z-moravec.net	www.z-moravec.net	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
SODOMKA	SODOMKA	kA
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fullereny	fulleren	k1gInPc7
–	–	k?
struktura	struktura	k1gFnSc1
<g/>
,	,	kIx,
vlastnosti	vlastnost	k1gFnPc1
a	a	k8xC
perspektivy	perspektiva	k1gFnPc1
použití	použití	k1gNnSc2
v	v	k7c6
dopravě	doprava	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnSc6d1
vysoké	vysoká	k1gFnSc6
učení	učení	k1gNnSc4
technické	technický	k2eAgNnSc4d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
Fakulta	fakulta	k1gFnSc1
dopravní	dopravní	k2eAgFnSc1d1
<g/>
,	,	kIx,
2002	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Fullereny	fulleren	k1gInPc7
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Kniha	kniha	k1gFnSc1
Fullereny	fulleren	k1gInPc1
a	a	k8xC
nanotrubičky	nanotrubička	k1gFnPc1
ve	v	k7c6
Wikiknihách	Wikikniha	k1gFnPc6
</s>
<s>
Galerie	galerie	k1gFnSc1
Fullereny	fulleren	k1gInPc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Properties	Properties	k1gMnSc1
of	of	k?
C60	C60	k1gMnSc1
fullerene	fulleren	k1gInSc5
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Richard	Richard	k1gMnSc1
Smalley	Smallea	k1gMnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
autobiography	autobiograph	k1gInPc7
at	at	k?
Nobel	Nobel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
se	se	k3xPyFc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Sir	sir	k1gMnSc1
Harry	Harra	k1gFnSc2
Kroto	Krot	k2eAgNnSc1d1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
webpage	webpage	k1gFnSc7
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Simple	Simple	k1gFnSc1
model	model	k1gInSc1
of	of	k?
Fullerene	fulleren	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Rhonditic	Rhonditice	k1gFnPc2
Steel	Stelo	k1gNnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Introduction	Introduction	k1gInSc1
to	ten	k3xDgNnSc1
fullerites	fullerites	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Bucky	bucek	k1gMnPc4
Balls	Balls	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
short	short	k1gInSc1
video	video	k1gNnSc1
explaining	explaining	k1gInSc1
the	the	k?
structure	structur	k1gMnSc5
of	of	k?
C60	C60	k1gFnSc2
by	by	kYmCp3nP
the	the	k?
Vega	Veg	k2eAgFnSc1d1
Science	Science	k1gFnSc1
Trust	trust	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Giant	Giant	k1gMnSc1
Fullerenes	Fullerenes	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
short	short	k1gInSc1
video	video	k1gNnSc1
looking	looking	k1gInSc1
at	at	k?
Giant	Giant	k1gMnSc1
Fullerenes	Fullerenes	k1gMnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Uhlíkové	Uhlíkové	k2eAgFnSc1d1
nanotrubice	nanotrubice	k1gFnSc1
</s>
<s>
Šungit	šungit	k1gInSc1
</s>
<s>
Grafen	Grafen	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4305238-1	4305238-1	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
5907	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
91005662	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
91005662	#num#	k4
</s>
