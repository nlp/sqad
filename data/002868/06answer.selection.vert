<s>
Generál	generál	k1gMnSc1	generál
(	(	kIx(	(
<g/>
armádní	armádní	k2eAgMnSc1d1	armádní
generál	generál	k1gMnSc1	generál
i.	i.	k?	i.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
Heliodor	Heliodor	k1gMnSc1	Heliodor
Prokop	Prokop	k1gMnSc1	Prokop
Píka	píka	k1gFnSc1	píka
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1897	[number]	k4	1897
Štítina	Štítina	k1gFnSc1	Štítina
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1949	[number]	k4	1949
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
legionář	legionář	k1gMnSc1	legionář
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
představitel	představitel	k1gMnSc1	představitel
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
československého	československý	k2eAgInSc2d1	československý
protinacistického	protinacistický	k2eAgInSc2d1	protinacistický
odboje	odboj	k1gInSc2	odboj
a	a	k8xC	a
po	po	k7c6	po
únorovém	únorový	k2eAgInSc6d1	únorový
převratu	převrat	k1gInSc6	převrat
oběť	oběť	k1gFnSc1	oběť
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
