<s>
Jaká	jaký	k3yQgFnSc1	jaký
řeka	řeka	k1gFnSc1	řeka
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
východní	východní	k2eAgFnSc1d1	východní
Afrikou	Afrika	k1gFnSc7	Afrika
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
na	na	k7c4	na
sever	sever	k1gInSc4	sever
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
6	[number]	k4	6
671	[number]	k4	671
km	km	kA	km
<g/>
?	?	kIx.	?
</s>
