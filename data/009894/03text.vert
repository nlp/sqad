<p>
<s>
Irák	Irák	k1gInSc1	Irák
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Irácká	irácký	k2eAgFnSc1d1	irácká
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
ج	ج	k?	ج
ا	ا	k?	ا
<g/>
,	,	kIx,	,
al-Džumhúríja	al-Džumhúríja	k6eAd1	al-Džumhúríja
al-irákíja	alrákíja	k6eAd1	al-irákíja
<g/>
,	,	kIx,	,
kurdsky	kurdsky	k6eAd1	kurdsky
ك	ك	k?	ك
ع	ع	k?	ع
<g/>
,	,	kIx,	,
Kómárí	Kómárí	k2eAgInSc1d1	Kómárí
Érák	Érák	k1gInSc1	Érák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Kuvajtem	Kuvajt	k1gInSc7	Kuvajt
a	a	k8xC	a
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Jordánskem	Jordánsko	k1gNnSc7	Jordánsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
se	s	k7c7	s
Sýrií	Sýrie	k1gFnSc7	Sýrie
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Íránem	Írán	k1gInSc7	Írán
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
Perský	perský	k2eAgInSc1d1	perský
záliv	záliv	k1gInSc1	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
32	[number]	k4	32
847	[number]	k4	847
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
většinu	většina	k1gFnSc4	většina
(	(	kIx(	(
<g/>
75	[number]	k4	75
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
%	%	kIx~	%
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
Arabové	Arab	k1gMnPc1	Arab
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
převážně	převážně	k6eAd1	převážně
Kurdové	Kurd	k1gMnPc1	Kurd
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
97	[number]	k4	97
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
muslimského	muslimský	k2eAgNnSc2d1	muslimské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
většinou	většinou	k6eAd1	většinou
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itského	itský	k1gMnSc2	itský
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
a	a	k8xC	a
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Bagdád	Bagdád	k1gInSc1	Bagdád
<g/>
,	,	kIx,	,
úředními	úřední	k2eAgInPc7d1	úřední
jazyky	jazyk	k1gInPc7	jazyk
jsou	být	k5eAaImIp3nP	být
arabština	arabština	k1gFnSc1	arabština
a	a	k8xC	a
kurdština	kurdština	k1gFnSc1	kurdština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
Irák	Irák	k1gInSc1	Irák
znám	znát	k5eAaImIp1nS	znát
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
domovem	domov	k1gInSc7	domov
několika	několik	k4yIc2	několik
úspěšných	úspěšný	k2eAgFnPc2d1	úspěšná
civilizací	civilizace	k1gFnPc2	civilizace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Akkadské	Akkadský	k2eAgFnSc2d1	Akkadská
<g/>
,	,	kIx,	,
Sumerské	sumerský	k2eAgFnSc2d1	sumerská
<g/>
,	,	kIx,	,
Novoasyrské	Novoasyrský	k2eAgFnSc2d1	Novoasyrská
<g/>
,	,	kIx,	,
Novobabylonské	Novobabylonský	k2eAgFnSc2d1	Novobabylonská
a	a	k8xC	a
Seleukovské	Seleukovský	k2eAgFnSc2d1	Seleukovská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
vládly	vládnout	k5eAaImAgInP	vládnout
jí	on	k3xPp3gFnSc3	on
také	také	k9	také
dynastie	dynastie	k1gFnSc1	dynastie
Arsakovců	Arsakovec	k1gMnPc2	Arsakovec
<g/>
,	,	kIx,	,
Sásánovců	Sásánovec	k1gMnPc2	Sásánovec
a	a	k8xC	a
Abbásovců	Abbásovec	k1gMnPc2	Abbásovec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ovládnutí	ovládnutí	k1gNnSc1	ovládnutí
muslimskými	muslimský	k2eAgMnPc7d1	muslimský
Araby	Arab	k1gMnPc7	Arab
v	v	k7c6	v
půlce	půlka	k1gFnSc6	půlka
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
chalífátu	chalífát	k1gInSc2	chalífát
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
volených	volený	k2eAgMnPc2d1	volený
chalífů	chalífa	k1gMnPc2	chalífa
a	a	k8xC	a
Umajjovců	Umajjovec	k1gMnPc2	Umajjovec
<g/>
,	,	kIx,	,
ílchanátu	ílchanát	k1gInSc2	ílchanát
<g/>
,	,	kIx,	,
říše	říš	k1gFnSc2	říš
Safíovců	Safíovec	k1gMnPc2	Safíovec
a	a	k8xC	a
Afšárovců	Afšárovec	k1gMnPc2	Afšárovec
a	a	k8xC	a
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
moderního	moderní	k2eAgInSc2d1	moderní
Iráku	Irák	k1gInSc2	Irák
určila	určit	k5eAaPmAgFnS	určit
Společnost	společnost	k1gFnSc1	společnost
národů	národ	k1gInPc2	národ
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
Sè	Sè	k1gFnSc7	Sè
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
Iráku	Irák	k1gInSc2	Irák
připadla	připadnout	k5eAaPmAgFnS	připadnout
Spojenému	spojený	k2eAgNnSc3d1	spojené
království	království	k1gNnSc3	království
jako	jako	k8xC	jako
mandátní	mandátní	k2eAgNnPc1d1	mandátní
území	území	k1gNnPc1	území
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Britský	britský	k2eAgInSc4d1	britský
mandát	mandát	k1gInSc4	mandát
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
monarchie	monarchie	k1gFnSc1	monarchie
a	a	k8xC	a
Irácké	irácký	k2eAgNnSc1d1	irácké
království	království	k1gNnSc1	království
získalo	získat	k5eAaPmAgNnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Irácká	irácký	k2eAgFnSc1d1	irácká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1968	[number]	k4	1968
a	a	k8xC	a
2003	[number]	k4	2003
ovládána	ovládat	k5eAaImNgFnS	ovládat
socialistickou	socialistický	k2eAgFnSc7d1	socialistická
stranou	strana	k1gFnSc7	strana
Baas	Baasa	k1gFnPc2	Baasa
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
vůdcem	vůdce	k1gMnSc7	vůdce
a	a	k8xC	a
prezidentem	prezident	k1gMnSc7	prezident
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
stal	stát	k5eAaPmAgMnS	stát
Saddám	Saddám	k1gMnSc1	Saddám
Husajn	Husajn	k1gMnSc1	Husajn
<g/>
.	.	kIx.	.
</s>
<s>
Zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
i	i	k8xC	i
vnitrostátní	vnitrostátní	k2eAgFnSc4d1	vnitrostátní
situaci	situace	k1gFnSc4	situace
výrazně	výrazně	k6eAd1	výrazně
ovlivnily	ovlivnit	k5eAaPmAgInP	ovlivnit
irácko-íránská	irácko-íránský	k2eAgFnSc1d1	irácko-íránská
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
vnikla	vniknout	k5eAaPmAgFnS	vniknout
koalice	koalice	k1gFnSc1	koalice
vedená	vedený	k2eAgFnSc1d1	vedená
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
území	území	k1gNnSc6	území
začala	začít	k5eAaPmAgNnP	začít
okupovat	okupovat	k5eAaBmF	okupovat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
kontrola	kontrola	k1gFnSc1	kontrola
předána	předat	k5eAaPmNgFnS	předat
dočasné	dočasný	k2eAgFnSc3d1	dočasná
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
nová	nový	k2eAgFnSc1d1	nová
ústava	ústava	k1gFnSc1	ústava
a	a	k8xC	a
zvolena	zvolen	k2eAgFnSc1d1	zvolena
nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
američtí	americký	k2eAgMnPc1d1	americký
vojáci	voják	k1gMnPc1	voják
zemi	zem	k1gFnSc4	zem
opustili	opustit	k5eAaPmAgMnP	opustit
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Irák	Irák	k1gInSc1	Irák
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
politika	politika	k1gFnSc1	politika
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
silně	silně	k6eAd1	silně
zkorumpovaná	zkorumpovaný	k2eAgFnSc1d1	zkorumpovaná
a	a	k8xC	a
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
působení	působení	k1gNnSc3	působení
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
skupin	skupina	k1gFnPc2	skupina
neklidná	klidný	k2eNgFnSc1d1	neklidná
<g/>
.	.	kIx.	.
</s>
<s>
Vládu	vláda	k1gFnSc4	vláda
vede	vést	k5eAaImIp3nS	vést
premiér	premiér	k1gMnSc1	premiér
a	a	k8xC	a
zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
jednokomorová	jednokomorový	k2eAgFnSc1d1	jednokomorová
Sněmovna	sněmovna	k1gFnSc1	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
země	zem	k1gFnSc2	zem
existuje	existovat	k5eAaImIp3nS	existovat
de	de	k?	de
facto	facto	k1gNnSc1	facto
autonomní	autonomní	k2eAgFnSc4d1	autonomní
oblast	oblast	k1gFnSc4	oblast
irácký	irácký	k2eAgInSc4d1	irácký
Kurdistán	Kurdistán	k1gInSc4	Kurdistán
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
ekonomicky	ekonomicky	k6eAd1	ekonomicky
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
Bagdádu	Bagdád	k1gInSc6	Bagdád
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hrubého	hrubý	k2eAgInSc2d1	hrubý
domácího	domácí	k2eAgInSc2d1	domácí
produktu	produkt	k1gInSc2	produkt
je	být	k5eAaImIp3nS	být
irácká	irácký	k2eAgFnSc1d1	irácká
ekonomika	ekonomika	k1gFnSc1	ekonomika
62	[number]	k4	62
<g/>
.	.	kIx.	.
největší	veliký	k2eAgInSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Irák	Irák	k1gInSc1	Irák
má	mít	k5eAaImIp3nS	mít
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
zásob	zásoba	k1gFnPc2	zásoba
ropy	ropa	k1gFnSc2	ropa
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
zhruba	zhruba	k6eAd1	zhruba
115	[number]	k4	115
miliard	miliarda	k4xCgFnPc2	miliarda
barelů	barel	k1gInPc2	barel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Arabský	arabský	k2eAgInSc1d1	arabský
název	název	k1gInSc1	název
al-Irakíja	al-Irakíj	k1gInSc2	al-Irakíj
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
možným	možný	k2eAgInSc7d1	možný
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
je	být	k5eAaImIp3nS	být
původ	původ	k1gInSc4	původ
z	z	k7c2	z
názvu	název	k1gInSc2	název
sumerského	sumerský	k2eAgNnSc2d1	sumerské
města	město	k1gNnSc2	město
Uruk	Uruka	k1gFnPc2	Uruka
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Bible	bible	k1gFnSc2	bible
Erech	Erech	k1gInSc1	Erech
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
možným	možný	k2eAgNnSc7d1	možné
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
je	být	k5eAaImIp3nS	být
původ	původ	k1gInSc1	původ
z	z	k7c2	z
arabského	arabský	k2eAgNnSc2d1	arabské
slova	slovo	k1gNnSc2	slovo
'	'	kIx"	'
<g/>
araqa	araqa	k6eAd1	araqa
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
přibližně	přibližně	k6eAd1	přibližně
"	"	kIx"	"
<g/>
dobře	dobře	k6eAd1	dobře
zavlažený	zavlažený	k2eAgInSc1d1	zavlažený
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
úrodný	úrodný	k2eAgMnSc1d1	úrodný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
nazývána	nazýván	k2eAgFnSc1d1	nazývána
Irácká	irácký	k2eAgFnSc1d1	irácká
země	země	k1gFnSc1	země
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
úrodná	úrodný	k2eAgFnSc1d1	úrodná
země	země	k1gFnSc1	země
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
existovaly	existovat	k5eAaImAgInP	existovat
dvě	dva	k4xCgFnPc4	dva
oblasti	oblast	k1gFnPc4	oblast
s	s	k7c7	s
názvem	název	k1gInSc7	název
Irák	Irák	k1gInSc1	Irák
<g/>
:	:	kIx,	:
první	první	k4xOgInSc1	první
byl	být	k5eAaImAgInS	být
ʿ	ʿ	k?	ʿ
ʿ	ʿ	k?	ʿ
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
arabský	arabský	k2eAgInSc1d1	arabský
Irák	Irák	k1gInSc1	Irák
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
oblast	oblast	k1gFnSc1	oblast
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
ʿ	ʿ	k?	ʿ
ʿ	ʿ	k?	ʿ
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
cizí	cizí	k2eAgInSc1d1	cizí
Irák	Irák	k1gInSc1	Irák
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
dnešní	dnešní	k2eAgInSc1d1	dnešní
severozápadní	severozápadní	k2eAgInSc1d1	severozápadní
Írán	Írán	k1gInSc1	Írán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
Arabský	arabský	k2eAgInSc1d1	arabský
Irák	Irák	k1gInSc1	Irák
<g/>
"	"	kIx"	"
nezahrnoval	zahrnovat	k5eNaImAgInS	zahrnovat
oblast	oblast	k1gFnSc4	oblast
dnešního	dnešní	k2eAgInSc2d1	dnešní
severního	severní	k2eAgInSc2d1	severní
Iráku	Irák	k1gInSc2	Irák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
a	a	k8xC	a
arabská	arabský	k2eAgFnSc1d1	arabská
nadvláda	nadvláda	k1gFnSc1	nadvláda
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
důkazy	důkaz	k1gInPc1	důkaz
osídlení	osídlení	k1gNnSc2	osídlení
území	území	k1gNnSc2	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Iráku	Irák	k1gInSc2	Irák
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
asi	asi	k9	asi
150	[number]	k4	150
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
a	a	k8xC	a
nejstarší	starý	k2eAgNnSc4d3	nejstarší
známé	známý	k2eAgNnSc4d1	známé
vesnické	vesnický	k2eAgNnSc4d1	vesnické
osídlení	osídlení	k1gNnSc4	osídlení
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
území	území	k1gNnPc1	území
mezi	mezi	k7c7	mezi
řekami	řeka	k1gFnPc7	řeka
Eufrat	Eufrat	k1gInSc4	Eufrat
a	a	k8xC	a
Tigris	Tigris	k1gInSc4	Tigris
nazývalo	nazývat	k5eAaImAgNnS	nazývat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dostalo	dostat	k5eAaPmAgNnS	dostat
první	první	k4xOgNnSc1	první
sumerské	sumerský	k2eAgNnSc1d1	sumerské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
a	a	k8xC	a
založilo	založit	k5eAaPmAgNnS	založit
zde	zde	k6eAd1	zde
civilizaci	civilizace	k1gFnSc3	civilizace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
civilizací	civilizace	k1gFnPc2	civilizace
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Sumerové	Sumer	k1gMnPc1	Sumer
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vynalezli	vynaleznout	k5eAaPmAgMnP	vynaleznout
klínové	klínový	k2eAgNnSc4d1	klínové
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
ryli	rýt	k5eAaImAgMnP	rýt
do	do	k7c2	do
hliněných	hliněný	k2eAgFnPc2d1	hliněná
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Babylonii	Babylonie	k1gFnSc6	Babylonie
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Asýrii	Asýrie	k1gFnSc6	Asýrie
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
dějiny	dějiny	k1gFnPc1	dějiny
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
vyvíjely	vyvíjet	k5eAaImAgFnP	vyvíjet
odděleně	odděleně	k6eAd1	odděleně
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
mísit	mísit	k5eAaImF	mísit
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgNnSc7d1	typické
sumerským	sumerský	k2eAgNnSc7d1	sumerské
politickým	politický	k2eAgNnSc7d1	politické
zřízením	zřízení	k1gNnSc7	zřízení
byly	být	k5eAaImAgFnP	být
městské	městský	k2eAgFnPc1d1	městská
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
dobách	doba	k1gFnPc6	doba
nahrazovány	nahrazovat	k5eAaImNgInP	nahrazovat
říšemi	říš	k1gFnPc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
velkou	velký	k2eAgFnSc4d1	velká
říši	říše	k1gFnSc4	říše
s	s	k7c7	s
názvem	název	k1gInSc7	název
Akkadská	Akkadský	k2eAgFnSc1d1	Akkadská
říše	říše	k1gFnSc1	říše
založil	založit	k5eAaPmAgInS	založit
Sargon	Sargon	k1gInSc1	Sargon
Akkadský	Akkadský	k2eAgInSc1d1	Akkadský
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
2310	[number]	k4	2310
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
následoval	následovat	k5eAaImAgInS	následovat
stát	stát	k1gInSc1	stát
3	[number]	k4	3
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
urské	urský	k2eAgFnSc2d1	urská
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
pramenů	pramen	k1gInPc2	pramen
z	z	k7c2	z
mezopotámské	mezopotámský	k2eAgFnSc2d1	mezopotámská
doby	doba	k1gFnSc2	doba
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
starobabylonského	starobabylonský	k2eAgNnSc2d1	starobabylonský
období	období	k1gNnSc2	období
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
vlády	vláda	k1gFnSc2	vláda
Chammurapiho	Chammurapi	k1gMnSc2	Chammurapi
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nechal	nechat	k5eAaPmAgMnS	nechat
vydat	vydat	k5eAaPmF	vydat
slavný	slavný	k2eAgMnSc1d1	slavný
Chammurapiho	Chammurapi	k1gMnSc4	Chammurapi
zákoník	zákoník	k1gInSc1	zákoník
<g/>
.	.	kIx.	.
</s>
<s>
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
byla	být	k5eAaImAgFnS	být
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
výhodné	výhodný	k2eAgFnSc3d1	výhodná
lokalitě	lokalita	k1gFnSc3	lokalita
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
netypicky	typicky	k6eNd1	typicky
úrodné	úrodný	k2eAgFnPc1d1	úrodná
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
okolními	okolní	k2eAgNnPc7d1	okolní
územími	území	k1gNnPc7	území
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
napadána	napadat	k5eAaBmNgFnS	napadat
okolními	okolní	k2eAgInPc7d1	okolní
národy	národ	k1gInPc7	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
Babylonie	Babylonie	k1gFnSc1	Babylonie
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
Chetité	Chetita	k1gMnPc1	Chetita
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Kassité	Kassita	k1gMnPc1	Kassita
<g/>
,	,	kIx,	,
Chaldejci	Chaldejec	k1gMnPc1	Chaldejec
a	a	k8xC	a
Peršané	Peršan	k1gMnPc1	Peršan
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Asýrii	Asýrie	k1gFnSc4	Asýrie
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Médové	Méd	k1gMnPc1	Méd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
331	[number]	k4	331
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dobyl	dobýt	k5eAaPmAgMnS	dobýt
Mezopotámii	Mezopotámie	k1gFnSc4	Mezopotámie
Alexandr	Alexandr	k1gMnSc1	Alexandr
Veliký	veliký	k2eAgMnSc1d1	veliký
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Seleukovské	Seleukovský	k2eAgFnSc2d1	Seleukovská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
150	[number]	k4	150
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
území	území	k1gNnSc2	území
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
Parthové	Parth	k1gMnPc1	Parth
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
Parthové	Parthové	k2eAgMnPc3d1	Parthové
Peršanům	Peršan	k1gMnPc3	Peršan
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Sásánovců	Sásánovec	k1gMnPc2	Sásánovec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
Babylonii	Babylonie	k1gFnSc4	Babylonie
připojili	připojit	k5eAaPmAgMnP	připojit
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Mnohá	mnohé	k1gNnPc1	mnohé
z	z	k7c2	z
mezopotámských	mezopotámský	k2eAgNnPc2d1	mezopotámský
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
než	než	k8xS	než
patřil	patřit	k5eAaImAgMnS	patřit
např.	např.	kA	např.
Babylón	Babylón	k1gInSc1	Babylón
<g/>
,	,	kIx,	,
Akkad	Akkad	k1gInSc1	Akkad
<g/>
,	,	kIx,	,
Ur	Ur	k1gInSc1	Ur
<g/>
,	,	kIx,	,
Uruk	Uruk	k1gMnSc1	Uruk
<g/>
,	,	kIx,	,
Lagaš	Lagaš	k1gMnSc1	Lagaš
<g/>
,	,	kIx,	,
Nippur	Nippur	k1gMnSc1	Nippur
<g/>
,	,	kIx,	,
Aššúr	Aššúr	k1gMnSc1	Aššúr
<g/>
,	,	kIx,	,
Ninive	Ninive	k1gNnSc1	Ninive
a	a	k8xC	a
Nimrud	Nimrud	k1gInSc1	Nimrud
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
významnými	významný	k2eAgFnPc7d1	významná
archeologickými	archeologický	k2eAgFnPc7d1	archeologická
lokalitami	lokalita	k1gFnPc7	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
kromě	kromě	k7c2	kromě
Chammurapiho	Chammurapi	k1gMnSc2	Chammurapi
zákoníku	zákoník	k1gInSc2	zákoník
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
např.	např.	kA	např.
Visuté	visutý	k2eAgFnSc2d1	visutá
zahrady	zahrada	k1gFnSc2	zahrada
Semiramidiny	Semiramidin	k2eAgFnSc2d1	Semiramidin
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
divů	div	k1gInPc2	div
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Epos	epos	k1gInSc1	epos
o	o	k7c6	o
Gilgamešovi	Gilgameš	k1gMnSc6	Gilgameš
a	a	k8xC	a
biblická	biblický	k2eAgFnSc1d1	biblická
Babylonská	babylonský	k2eAgFnSc1d1	Babylonská
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
637	[number]	k4	637
se	se	k3xPyFc4	se
Mezopotámie	Mezopotámie	k1gFnPc1	Mezopotámie
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
muslimští	muslimský	k2eAgMnPc1d1	muslimský
Arabové	Arab	k1gMnPc1	Arab
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
Irák	Irák	k1gInSc1	Irák
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tuto	tento	k3xDgFnSc4	tento
zemi	zem	k1gFnSc4	zem
nazvali	nazvat	k5eAaBmAgMnP	nazvat
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
chalífátu	chalífát	k1gInSc2	chalífát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
doby	doba	k1gFnSc2	doba
arabské	arabský	k2eAgFnSc2d1	arabská
nadvlády	nadvláda	k1gFnSc2	nadvláda
byl	být	k5eAaImAgInS	být
Irák	Irák	k1gInSc1	Irák
centrem	centrum	k1gNnSc7	centrum
chalífátu	chalífát	k1gInSc2	chalífát
a	a	k8xC	a
Bagdád	Bagdád	k1gInSc4	Bagdád
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
muslimové	muslim	k1gMnPc1	muslim
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
762	[number]	k4	762
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
nejspíše	nejspíše	k9	nejspíše
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
chalífů	chalífa	k1gMnPc2	chalífa
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
nebyla	být	k5eNaImAgFnS	být
vždy	vždy	k6eAd1	vždy
pevná	pevný	k2eAgFnSc1d1	pevná
<g/>
,	,	kIx,	,
zemi	zem	k1gFnSc4	zem
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vládli	vládnout	k5eAaImAgMnP	vládnout
íránští	íránský	k2eAgMnPc1d1	íránský
Bújovci	Bújovec	k1gMnPc1	Bújovec
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
velké	velký	k2eAgFnSc2d1	velká
Seldžucké	Seldžucký	k2eAgFnSc2d1	Seldžucká
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mongolská	mongolský	k2eAgFnSc1d1	mongolská
nadvláda	nadvláda	k1gFnSc1	nadvláda
a	a	k8xC	a
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1258	[number]	k4	1258
se	se	k3xPyFc4	se
po	po	k7c6	po
obléhání	obléhání	k1gNnSc6	obléhání
a	a	k8xC	a
následném	následný	k2eAgNnSc6d1	následné
zdevastování	zdevastování	k1gNnSc6	zdevastování
Bagdádu	Bagdád	k1gInSc2	Bagdád
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
200	[number]	k4	200
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
jeho	jeho	k3xOp3gMnPc2	jeho
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
Iráku	Irák	k1gInSc2	Irák
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
Mongolové	Mongol	k1gMnPc1	Mongol
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
ílchanátu	ílchanát	k1gInSc2	ílchanát
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jejich	jejich	k3xOp3gFnSc2	jejich
vlády	vláda	k1gFnSc2	vláda
země	zem	k1gFnSc2	zem
ekonomicky	ekonomicky	k6eAd1	ekonomicky
i	i	k9	i
kulturně	kulturně	k6eAd1	kulturně
upadala	upadat	k5eAaPmAgFnS	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
ílchanátu	ílchanát	k1gInSc2	ílchanát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1335	[number]	k4	1335
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
džálajirovského	džálajirovský	k2eAgInSc2d1	džálajirovský
sultanátu	sultanát	k1gInSc2	sultanát
a	a	k8xC	a
o	o	k7c4	o
půl	půl	k1xP	půl
století	století	k1gNnSc2	století
později	pozdě	k6eAd2	pozdě
říše	říš	k1gFnSc2	říš
turkického	turkický	k2eAgMnSc4d1	turkický
vojevůdce	vojevůdce	k1gMnSc4	vojevůdce
Tamerlána	Tamerlán	k2eAgMnSc4d1	Tamerlán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
Irákem	Irák	k1gInSc7	Irák
ujali	ujmout	k5eAaPmAgMnP	ujmout
Turkméni	Turkmén	k1gMnPc1	Turkmén
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Safíovci	Safíovec	k1gMnSc3	Safíovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1514	[number]	k4	1514
porazil	porazit	k5eAaPmAgMnS	porazit
osmanský	osmanský	k2eAgMnSc1d1	osmanský
sultán	sultán	k1gMnSc1	sultán
Selim	Selim	k?	Selim
I.	I.	kA	I.
safíovského	safíovský	k2eAgMnSc4d1	safíovský
vládce	vládce	k1gMnSc4	vládce
Ismá	Ismá	k1gFnSc1	Ismá
<g/>
'	'	kIx"	'
<g/>
íla	íla	k?	íla
I.	I.	kA	I.
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Čaldiránu	Čaldirán	k1gInSc2	Čaldirán
<g/>
,	,	kIx,	,
připojil	připojit	k5eAaPmAgInS	připojit
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
říši	říš	k1gFnSc3	říš
severní	severní	k2eAgInSc1d1	severní
Irák	Irák	k1gInSc1	Irák
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
pak	pak	k6eAd1	pak
zbytek	zbytek	k1gInSc4	zbytek
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Irák	Irák	k1gInSc1	Irák
poté	poté	k6eAd1	poté
fungoval	fungovat	k5eAaImAgInS	fungovat
hlavně	hlavně	k9	hlavně
jako	jako	k9	jako
nárazníkový	nárazníkový	k2eAgInSc1d1	nárazníkový
stát	stát	k1gInSc1	stát
mezi	mezi	k7c7	mezi
zbytkem	zbytek	k1gInSc7	zbytek
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
Íránem	Írán	k1gInSc7	Írán
<g/>
,	,	kIx,	,
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
vláda	vláda	k1gFnSc1	vláda
ale	ale	k8xC	ale
zprvu	zprvu	k6eAd1	zprvu
nebyla	být	k5eNaImAgFnS	být
pevná	pevný	k2eAgFnSc1d1	pevná
a	a	k8xC	a
v	v	k7c6	v
půlce	půlka	k1gFnSc6	půlka
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
dostali	dostat	k5eAaPmAgMnP	dostat
mamlúci	mamlúce	k1gFnSc4	mamlúce
gruzijského	gruzijský	k2eAgInSc2d1	gruzijský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
podporovali	podporovat	k5eAaImAgMnP	podporovat
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
obchod	obchod	k1gInSc4	obchod
a	a	k8xC	a
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
povolili	povolit	k5eAaPmAgMnP	povolit
zřízení	zřízení	k1gNnSc4	zřízení
pobočky	pobočka	k1gFnSc2	pobočka
britské	britský	k2eAgFnSc2d1	britská
Východoindické	východoindický	k2eAgFnSc2d1	Východoindická
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
Basře	Basra	k1gFnSc6	Basra
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zvyšovalo	zvyšovat	k5eAaImAgNnS	zvyšovat
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
klíčové	klíčový	k2eAgNnSc1d1	klíčové
pro	pro	k7c4	pro
události	událost	k1gFnPc4	událost
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
půlky	půlka	k1gFnSc2	půlka
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Osmanům	Osman	k1gMnPc3	Osman
podařilo	podařit	k5eAaPmAgNnS	podařit
svrhnout	svrhnout	k5eAaPmF	svrhnout
mamlúckou	mamlúcký	k2eAgFnSc4d1	mamlúcká
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
přímou	přímý	k2eAgFnSc4d1	přímá
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
Irákem	Irák	k1gInSc7	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začala	začít	k5eAaPmAgFnS	začít
země	zem	k1gFnPc4	zem
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
postupným	postupný	k2eAgNnSc7d1	postupné
zaváděním	zavádění	k1gNnSc7	zavádění
paroplavby	paroplavba	k1gFnSc2	paroplavba
na	na	k7c6	na
Eufratu	Eufrat	k1gInSc6	Eufrat
a	a	k8xC	a
Tigridu	Tigris	k1gInSc2	Tigris
pěstovat	pěstovat	k5eAaImF	pěstovat
a	a	k8xC	a
vyvážet	vyvážet	k5eAaImF	vyvážet
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
více	hodně	k6eAd2	hodně
obilí	obilí	k1gNnSc2	obilí
než	než	k8xS	než
kdy	kdy	k6eAd1	kdy
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
asi	asi	k9	asi
desetinásobně	desetinásobně	k6eAd1	desetinásobně
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
plocha	plocha	k1gFnSc1	plocha
obdělávané	obdělávaný	k2eAgFnSc2d1	obdělávaná
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
míry	míra	k1gFnSc2	míra
přestávali	přestávat	k5eAaImAgMnP	přestávat
žít	žít	k5eAaImF	žít
nomádským	nomádský	k2eAgInSc7d1	nomádský
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
vzestupem	vzestup	k1gInSc7	vzestup
hnutí	hnutí	k1gNnSc2	hnutí
Mladoturků	mladoturek	k1gMnPc2	mladoturek
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
arabských	arabský	k2eAgFnPc6d1	arabská
zemích	zem	k1gFnPc6	zem
pod	pod	k7c7	pod
osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
nadvládou	nadvláda	k1gFnSc7	nadvláda
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
myšlenka	myšlenka	k1gFnSc1	myšlenka
arabského	arabský	k2eAgInSc2d1	arabský
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
arabské	arabský	k2eAgFnPc4d1	arabská
země	zem	k1gFnPc4	zem
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nP	kdyby
se	se	k3xPyFc4	se
od	od	k7c2	od
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
oddělily	oddělit	k5eAaPmAgFnP	oddělit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
myšlenka	myšlenka	k1gFnSc1	myšlenka
se	se	k3xPyFc4	se
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Iráku	Irák	k1gInSc2	Irák
zamlouvala	zamlouvat	k5eAaImAgFnS	zamlouvat
Spojenému	spojený	k2eAgNnSc3d1	spojené
království	království	k1gNnSc3	království
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
stále	stále	k6eAd1	stále
zvyšovalo	zvyšovat	k5eAaImAgNnS	zvyšovat
objem	objem	k1gInSc4	objem
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Britská	britský	k2eAgFnSc1d1	britská
nadvláda	nadvláda	k1gFnSc1	nadvláda
a	a	k8xC	a
Irácké	irácký	k2eAgNnSc1d1	irácké
království	království	k1gNnSc1	království
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
přidala	přidat	k5eAaPmAgFnS	přidat
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Německého	německý	k2eAgNnSc2d1	německé
císařství	císařství	k1gNnSc2	císařství
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
proto	proto	k8xC	proto
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
Mezopotámské	Mezopotámský	k2eAgNnSc1d1	Mezopotámské
tažení	tažení	k1gNnSc1	tažení
kvůli	kvůli	k7c3	kvůli
obraně	obrana	k1gFnSc3	obrana
svých	svůj	k3xOyFgInPc2	svůj
zájmů	zájem	k1gInPc2	zájem
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
Britové	Brit	k1gMnPc1	Brit
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
celého	celý	k2eAgNnSc2d1	celé
území	území	k1gNnSc2	území
a	a	k8xC	a
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
jim	on	k3xPp3gMnPc3	on
oblast	oblast	k1gFnSc1	oblast
připadla	připadnout	k5eAaPmAgFnS	připadnout
jako	jako	k9	jako
mandátní	mandátní	k2eAgNnSc4d1	mandátní
území	území	k1gNnSc4	území
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Britský	britský	k2eAgInSc4d1	britský
mandát	mandát	k1gInSc4	mandát
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
začalo	začít	k5eAaPmAgNnS	začít
povstání	povstání	k1gNnSc1	povstání
proti	proti	k7c3	proti
britské	britský	k2eAgFnSc3d1	britská
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
proto	proto	k6eAd1	proto
nabídlo	nabídnout	k5eAaPmAgNnS	nabídnout
bývalému	bývalý	k2eAgMnSc3d1	bývalý
králi	král	k1gMnSc3	král
Velké	velký	k2eAgFnSc2d1	velká
Sýrie	Sýrie	k1gFnSc2	Sýrie
Fajsalovi	Fajsalův	k2eAgMnPc1d1	Fajsalův
I.	I.	kA	I.
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
králem	král	k1gMnSc7	král
Iráku	Irák	k1gInSc2	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celostátním	celostátní	k2eAgNnSc6d1	celostátní
referendu	referendum	k1gNnSc6	referendum
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
Irák	Irák	k1gInSc1	Irák
tím	ten	k3xDgNnSc7	ten
ale	ale	k9	ale
nedosáhl	dosáhnout	k5eNaPmAgMnS	dosáhnout
úplné	úplný	k2eAgFnPc4d1	úplná
nezávislosti	nezávislost	k1gFnPc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
naléhání	naléhání	k1gNnSc4	naléhání
iráckých	irácký	k2eAgMnPc2d1	irácký
nacionalistů	nacionalista	k1gMnPc2	nacionalista
nakonec	nakonec	k6eAd1	nakonec
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
souhlasilo	souhlasit	k5eAaImAgNnS	souhlasit
s	s	k7c7	s
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Iráckého	irácký	k2eAgNnSc2d1	irácké
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
nadále	nadále	k6eAd1	nadále
si	se	k3xPyFc3	se
ale	ale	k9	ale
Britové	Brit	k1gMnPc1	Brit
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
zachovali	zachovat	k5eAaPmAgMnP	zachovat
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
dostali	dostat	k5eAaPmAgMnP	dostat
panarabisté	panarabista	k1gMnPc1	panarabista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
začali	začít	k5eAaPmAgMnP	začít
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
se	s	k7c7	s
zeměmi	zem	k1gFnPc7	zem
Osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
do	do	k7c2	do
země	zem	k1gFnSc2	zem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
vnikli	vniknout	k5eAaPmAgMnP	vniknout
Britové	Brit	k1gMnPc1	Brit
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
zemi	zem	k1gFnSc4	zem
opět	opět	k6eAd1	opět
okupovat	okupovat	k5eAaBmF	okupovat
<g/>
;	;	kIx,	;
odešli	odejít	k5eAaPmAgMnP	odejít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Irák	Irák	k1gInSc1	Irák
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
Bagdádského	bagdádský	k2eAgInSc2d1	bagdádský
paktu	pakt	k1gInSc2	pakt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vznik	vznik	k1gInSc1	vznik
Irácké	irácký	k2eAgFnSc2d1	irácká
republiky	republika	k1gFnSc2	republika
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
plochy	plocha	k1gFnSc2	plocha
obdělávané	obdělávaný	k2eAgFnSc2d1	obdělávaná
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
navíc	navíc	k6eAd1	navíc
přesouvána	přesouvat	k5eAaImNgFnS	přesouvat
z	z	k7c2	z
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
státu	stát	k1gInSc2	stát
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
soukromého	soukromý	k2eAgNnSc2d1	soukromé
<g/>
.	.	kIx.	.
</s>
<s>
Získávaly	získávat	k5eAaImAgFnP	získávat
ji	on	k3xPp3gFnSc4	on
ale	ale	k9	ale
většinou	většinou	k6eAd1	většinou
bohaté	bohatý	k2eAgFnPc1d1	bohatá
rodiny	rodina	k1gFnPc1	rodina
a	a	k8xC	a
zvyšovaly	zvyšovat	k5eAaImAgFnP	zvyšovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
chudou	chudý	k2eAgFnSc7d1	chudá
a	a	k8xC	a
bohatou	bohatý	k2eAgFnSc7d1	bohatá
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
Irák	Irák	k1gInSc1	Irák
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
velký	velký	k2eAgInSc4d1	velký
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
rozvoj	rozvoj	k1gInSc4	rozvoj
kvůli	kvůli	k7c3	kvůli
prudkému	prudký	k2eAgInSc3d1	prudký
nárůstu	nárůst	k1gInSc3	nárůst
vývozu	vývoz	k1gInSc2	vývoz
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
vojenský	vojenský	k2eAgInSc1d1	vojenský
převrat	převrat	k1gInSc1	převrat
vedený	vedený	k2eAgInSc1d1	vedený
Abdalem	Abdal	k1gMnSc7	Abdal
Karim	Karim	k1gMnSc1	Karim
Kásimem	Kásim	k1gMnSc7	Kásim
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
královského	královský	k2eAgInSc2d1	královský
paláce	palác	k1gInSc2	palác
zastřelen	zastřelen	k2eAgMnSc1d1	zastřelen
král	král	k1gMnSc1	král
Fajsal	Fajsal	k1gFnSc2	Fajsal
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
revoluční	revoluční	k2eAgFnSc1d1	revoluční
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Kásimem	Kásim	k1gMnSc7	Kásim
zrušila	zrušit	k5eAaPmAgFnS	zrušit
monarchii	monarchie	k1gFnSc4	monarchie
<g/>
,	,	kIx,	,
nastolila	nastolit	k5eAaPmAgFnS	nastolit
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
islám	islám	k1gInSc4	islám
za	za	k7c4	za
státní	státní	k2eAgNnSc4d1	státní
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
odešla	odejít	k5eAaPmAgFnS	odejít
z	z	k7c2	z
Bagdádského	bagdádský	k2eAgInSc2d1	bagdádský
paktu	pakt	k1gInSc2	pakt
a	a	k8xC	a
politicky	politicky	k6eAd1	politicky
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
sbližovat	sbližovat	k5eAaImF	sbližovat
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Převrat	převrat	k1gInSc4	převrat
a	a	k8xC	a
první	první	k4xOgInPc4	první
kroky	krok	k1gInPc4	krok
nového	nový	k2eAgInSc2d1	nový
režimu	režim	k1gInSc2	režim
byly	být	k5eAaImAgInP	být
vřele	vřele	k6eAd1	vřele
uvítány	uvítán	k2eAgInPc1d1	uvítán
iráckou	irácký	k2eAgFnSc7d1	irácká
veřejností	veřejnost	k1gFnSc7	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1963	[number]	k4	1963
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
další	další	k2eAgInSc1d1	další
státní	státní	k2eAgInSc1d1	státní
převrat	převrat	k1gInSc1	převrat
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
arabskou	arabský	k2eAgFnSc7d1	arabská
socialistickou	socialistický	k2eAgFnSc7d1	socialistická
stranou	strana	k1gFnSc7	strana
Baas	Baasa	k1gFnPc2	Baasa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
se	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
stal	stát	k5eAaPmAgMnS	stát
Abdul	Abdul	k1gMnSc1	Abdul
Salám	salám	k1gInSc4	salám
Árif	Árif	k1gInSc4	Árif
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nechal	nechat	k5eAaPmAgInS	nechat
několik	několik	k4yIc4	několik
členů	člen	k1gInPc2	člen
Baas	Baasa	k1gFnPc2	Baasa
uvěznit	uvěznit	k5eAaPmF	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
strana	strana	k1gFnSc1	strana
ujala	ujmout	k5eAaPmAgFnS	ujmout
moci	moct	k5eAaImF	moct
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgInP	být
částečně	částečně	k6eAd1	částečně
vyřešeny	vyřešit	k5eAaPmNgInP	vyřešit
dlouhodobé	dlouhodobý	k2eAgInPc1d1	dlouhodobý
spory	spor	k1gInPc1	spor
s	s	k7c7	s
Kurdy	Kurd	k1gMnPc7	Kurd
žijícími	žijící	k2eAgMnPc7d1	žijící
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
když	když	k8xS	když
vláda	vláda	k1gFnSc1	vláda
uveřejnila	uveřejnit	k5eAaPmAgFnS	uveřejnit
manifest	manifest	k1gInSc4	manifest
uznávající	uznávající	k2eAgFnSc4d1	uznávající
legitimitu	legitimita	k1gFnSc4	legitimita
kurdské	kurdský	k2eAgFnSc2d1	kurdská
národnosti	národnost	k1gFnSc2	národnost
a	a	k8xC	a
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
vymezit	vymezit	k5eAaPmF	vymezit
autonomní	autonomní	k2eAgFnSc4d1	autonomní
oblast	oblast	k1gFnSc4	oblast
irácký	irácký	k2eAgInSc4d1	irácký
Kurdistán	Kurdistán	k1gInSc4	Kurdistán
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
ukázal	ukázat	k5eAaPmAgInS	ukázat
jako	jako	k9	jako
falešný	falešný	k2eAgInSc1d1	falešný
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
vypukly	vypuknout	k5eAaPmAgInP	vypuknout
v	v	k7c6	v
Kurdistánu	Kurdistán	k1gInSc6	Kurdistán
otevřené	otevřený	k2eAgInPc4d1	otevřený
boje	boj	k1gInPc4	boj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
trvaly	trvat	k5eAaImAgInP	trvat
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
strany	strana	k1gFnSc2	strana
Baas	Baasa	k1gFnPc2	Baasa
zpočátku	zpočátku	k6eAd1	zpočátku
nenabyla	nabýt	k5eNaPmAgFnS	nabýt
oblibě	obliba	k1gFnSc3	obliba
ani	ani	k9	ani
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
ani	ani	k8xC	ani
mimo	mimo	k7c4	mimo
něj	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgFnSc4d1	domácí
podporu	podpora	k1gFnSc4	podpora
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
znárodněním	znárodnění	k1gNnSc7	znárodnění
Irácké	irácký	k2eAgFnSc2d1	irácká
ropné	ropný	k2eAgFnSc2d1	ropná
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
IPC	IPC	kA	IPC
<g/>
)	)	kIx)	)
a	a	k8xC	a
zlepšení	zlepšení	k1gNnSc4	zlepšení
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
vztahů	vztah	k1gInPc2	vztah
bylo	být	k5eAaImAgNnS	být
zapříčiněno	zapříčinit	k5eAaPmNgNnS	zapříčinit
postupným	postupný	k2eAgNnSc7d1	postupné
odvracením	odvracení	k1gNnSc7	odvracení
se	se	k3xPyFc4	se
od	od	k7c2	od
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
komunismu	komunismus	k1gInSc2	komunismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Husajnova	Husajnův	k2eAgFnSc1d1	Husajnova
vláda	vláda	k1gFnSc1	vláda
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
Irák	Irák	k1gInSc1	Irák
zbohatl	zbohatnout	k5eAaPmAgInS	zbohatnout
kvůli	kvůli	k7c3	kvůli
prudkému	prudký	k2eAgInSc3d1	prudký
nárůstu	nárůst	k1gInSc3	nárůst
příjmů	příjem	k1gInPc2	příjem
z	z	k7c2	z
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
podporovat	podporovat	k5eAaImF	podporovat
finančně	finančně	k6eAd1	finančně
náročné	náročný	k2eAgInPc4d1	náročný
programy	program	k1gInPc4	program
např.	např.	kA	např.
ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
a	a	k8xC	a
bytové	bytový	k2eAgFnSc3d1	bytová
výstavbě	výstavba	k1gFnSc3	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
životní	životní	k2eAgFnSc1d1	životní
úroveň	úroveň	k1gFnSc1	úroveň
a	a	k8xC	a
režim	režim	k1gInSc1	režim
měl	mít	k5eAaImAgInS	mít
širokou	široký	k2eAgFnSc4d1	široká
podporu	podpora	k1gFnSc4	podpora
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
výrazně	výrazně	k6eAd1	výrazně
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
produkce	produkce	k1gFnSc1	produkce
a	a	k8xC	a
s	s	k7c7	s
problémy	problém	k1gInPc7	problém
se	se	k3xPyFc4	se
potýkal	potýkat	k5eAaImAgInS	potýkat
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
služby	služba	k1gFnPc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
Saddám	Saddám	k1gMnSc1	Saddám
Husajn	Husajn	k1gMnSc1	Husajn
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
převratu	převrat	k1gInSc6	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
získával	získávat	k5eAaImAgMnS	získávat
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc4d2	veliký
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
Iráku	Irák	k1gInSc2	Irák
a	a	k8xC	a
velitelem	velitel	k1gMnSc7	velitel
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
vládu	vláda	k1gFnSc4	vláda
začal	začít	k5eAaPmAgInS	začít
popravou	poprava	k1gFnSc7	poprava
22	[number]	k4	22
svých	svůj	k3xOyFgMnPc2	svůj
odpůrců	odpůrce	k1gMnPc2	odpůrce
a	a	k8xC	a
likvidováním	likvidování	k1gNnSc7	likvidování
konkurence	konkurence	k1gFnSc2	konkurence
se	se	k3xPyFc4	se
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
celé	celý	k2eAgNnSc1d1	celé
období	období	k1gNnSc1	období
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
svržení	svržení	k1gNnSc6	svržení
starého	starý	k2eAgInSc2d1	starý
režimu	režim	k1gInSc2	režim
v	v	k7c6	v
sousedním	sousední	k2eAgInSc6d1	sousední
Íránu	Írán	k1gInSc6	Írán
a	a	k8xC	a
nástupu	nástup	k1gInSc6	nástup
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itské	itský	k2eAgFnSc2d1	itská
diktatury	diktatura	k1gFnSc2	diktatura
se	se	k3xPyFc4	se
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
špatné	špatný	k2eAgInPc4d1	špatný
vztahy	vztah	k1gInPc4	vztah
Íránu	Írán	k1gInSc2	Írán
s	s	k7c7	s
Irákem	Irák	k1gInSc7	Irák
ještě	ještě	k6eAd1	ještě
zhoršily	zhoršit	k5eAaPmAgFnP	zhoršit
a	a	k8xC	a
Husajn	Husajn	k1gMnSc1	Husajn
zahájil	zahájit	k5eAaPmAgMnS	zahájit
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1980	[number]	k4	1980
irácko-íránskou	irácko-íránský	k2eAgFnSc4d1	irácko-íránská
válku	válka	k1gFnSc4	válka
invazí	invaze	k1gFnPc2	invaze
vojsk	vojsko	k1gNnPc2	vojsko
do	do	k7c2	do
Íránu	Írán	k1gInSc2	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
po	po	k7c6	po
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
uzavřením	uzavření	k1gNnSc7	uzavření
příměří	příměří	k1gNnSc2	příměří
<g/>
.	.	kIx.	.
</s>
<s>
Zemřelo	zemřít	k5eAaPmAgNnS	zemřít
během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
mezi	mezi	k7c7	mezi
500	[number]	k4	500
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
a	a	k8xC	a
Irák	Irák	k1gInSc4	Irák
finančně	finančně	k6eAd1	finančně
zruinovala	zruinovat	k5eAaPmAgFnS	zruinovat
<g/>
.	.	kIx.	.
</s>
<s>
Urychlila	urychlit	k5eAaPmAgFnS	urychlit
expanzi	expanze	k1gFnSc3	expanze
soukromého	soukromý	k2eAgNnSc2d1	soukromé
hospodářství	hospodářství	k1gNnSc2	hospodářství
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
staly	stát	k5eAaPmAgInP	stát
spojencem	spojenec	k1gMnSc7	spojenec
Iráku	Irák	k1gInSc2	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
rok	rok	k1gInSc4	rok
války	válka	k1gFnSc2	válka
začala	začít	k5eAaPmAgFnS	začít
genocida	genocida	k1gFnSc1	genocida
kurdského	kurdský	k2eAgNnSc2d1	kurdské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
irácké	irácký	k2eAgFnSc2d1	irácká
vlády	vláda	k1gFnSc2	vláda
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
al-Anfál	al-Anfála	k1gFnPc2	al-Anfála
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyústila	vyústit	k5eAaPmAgFnS	vyústit
ve	v	k7c4	v
smrt	smrt	k1gFnSc4	smrt
zhruba	zhruba	k6eAd1	zhruba
200	[number]	k4	200
000	[number]	k4	000
Kurdů	Kurd	k1gMnPc2	Kurd
a	a	k8xC	a
úprk	úprk	k1gInSc1	úprk
dalších	další	k2eAgInPc2d1	další
150	[number]	k4	150
000	[number]	k4	000
do	do	k7c2	do
Íránu	Írán	k1gInSc2	Írán
a	a	k8xC	a
Turecka	Turecko	k1gNnSc2	Turecko
během	během	k7c2	během
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
byly	být	k5eAaImAgFnP	být
použity	použít	k5eAaPmNgFnP	použít
i	i	k9	i
chemické	chemický	k2eAgFnPc1d1	chemická
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
Husajn	Husajn	k1gMnSc1	Husajn
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
Kuvajt	Kuvajt	k1gInSc4	Kuvajt
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
ho	on	k3xPp3gNnSc4	on
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
další	další	k2eAgFnSc4d1	další
provincii	provincie	k1gFnSc4	provincie
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
započal	započnout	k5eAaPmAgInS	započnout
válku	válka	k1gFnSc4	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
uvalila	uvalit	k5eAaPmAgFnS	uvalit
na	na	k7c4	na
Irák	Irák	k1gInSc4	Irák
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
embargo	embargo	k1gNnSc1	embargo
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
ujaly	ujmout	k5eAaPmAgInP	ujmout
vedení	vedení	k1gNnSc1	vedení
koalice	koalice	k1gFnSc1	koalice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
vyhnala	vyhnat	k5eAaPmAgFnS	vyhnat
iráckou	irácký	k2eAgFnSc4d1	irácká
armádu	armáda	k1gFnSc4	armáda
z	z	k7c2	z
okupovaného	okupovaný	k2eAgNnSc2d1	okupované
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Porážka	porážka	k1gFnSc1	porážka
podnítila	podnítit	k5eAaPmAgFnS	podnítit
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
ity	ity	k?	ity
a	a	k8xC	a
Kurdy	Kurd	k1gMnPc4	Kurd
k	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
proti	proti	k7c3	proti
režimu	režim	k1gInSc3	režim
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
násilně	násilně	k6eAd1	násilně
potlačeno	potlačen	k2eAgNnSc1d1	potlačeno
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
mrtvých	mrtvý	k1gMnPc2	mrtvý
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
až	až	k9	až
300	[number]	k4	300
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
až	až	k9	až
2	[number]	k4	2
500	[number]	k4	500
000	[number]	k4	000
Kurdů	Kurd	k1gMnPc2	Kurd
a	a	k8xC	a
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itů	itů	k?	itů
bylo	být	k5eAaImAgNnS	být
donuceno	donucen	k2eAgNnSc1d1	donuceno
emigrovat	emigrovat	k5eAaBmF	emigrovat
do	do	k7c2	do
Turecka	Turecko	k1gNnSc2	Turecko
a	a	k8xC	a
Íránu	Írán	k1gInSc2	Írán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1991	[number]	k4	1991
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
kvůli	kvůli	k7c3	kvůli
ochraně	ochrana	k1gFnSc3	ochrana
obyvatel	obyvatel	k1gMnPc2	obyvatel
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
nad	nad	k7c7	nad
iráckým	irácký	k2eAgInSc7d1	irácký
Kurdistánem	Kurdistán	k1gInSc7	Kurdistán
bezletovou	bezletový	k2eAgFnSc4d1	bezletová
zónu	zóna	k1gFnSc4	zóna
a	a	k8xC	a
tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
de	de	k?	de
facto	facto	k1gNnSc4	facto
kurdská	kurdský	k2eAgFnSc1d1	kurdská
autonomní	autonomní	k2eAgFnSc1d1	autonomní
oblast	oblast	k1gFnSc1	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
začal	začít	k5eAaPmAgMnS	začít
účinek	účinek	k1gInSc4	účinek
embarga	embargo	k1gNnSc2	embargo
uvaleného	uvalený	k2eAgInSc2d1	uvalený
OSN	OSN	kA	OSN
slábnout	slábnout	k5eAaImF	slábnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
životní	životní	k2eAgFnSc1d1	životní
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
horšila	horšit	k5eAaImAgFnS	horšit
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
trpělo	trpět	k5eAaImAgNnS	trpět
podvýživou	podvýživa	k1gFnSc7	podvýživa
a	a	k8xC	a
nedostatečnou	dostatečný	k2eNgFnSc7d1	nedostatečná
zdravotní	zdravotní	k2eAgFnSc7d1	zdravotní
péčí	péče	k1gFnSc7	péče
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
zhoršovala	zhoršovat	k5eAaImAgFnS	zhoršovat
úroveň	úroveň	k1gFnSc4	úroveň
vzdělání	vzdělání	k1gNnSc3	vzdělání
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
nepříznivým	příznivý	k2eNgFnPc3d1	nepříznivá
podmínkám	podmínka	k1gFnPc3	podmínka
opustilo	opustit	k5eAaPmAgNnS	opustit
Irák	Irák	k1gInSc4	Irák
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
miliony	milion	k4xCgInPc4	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
Husajnovi	Husajn	k1gMnSc6	Husajn
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
teroristických	teroristický	k2eAgInPc6d1	teroristický
útocích	útok	k1gInPc6	útok
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2001	[number]	k4	2001
označil	označit	k5eAaPmAgInS	označit
George	George	k1gFnSc7	George
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
Irák	Irák	k1gInSc1	Irák
za	za	k7c2	za
součást	součást	k1gFnSc1	součást
"	"	kIx"	"
<g/>
osy	osa	k1gFnPc1	osa
zla	zlo	k1gNnSc2	zlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
provedly	provést	k5eAaPmAgInP	provést
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
spojenci	spojenec	k1gMnPc1	spojenec
invazi	invaze	k1gFnSc4	invaze
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
započala	započnout	k5eAaPmAgFnS	započnout
válku	válka	k1gFnSc4	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
okupovali	okupovat	k5eAaBmAgMnP	okupovat
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2003	[number]	k4	2003
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
rodném	rodný	k2eAgInSc6d1	rodný
Tikrítu	Tikrít	k1gInSc6	Tikrít
nalezen	nalezen	k2eAgMnSc1d1	nalezen
Husajn	Husajn	k1gMnSc1	Husajn
<g/>
.	.	kIx.	.
</s>
<s>
Koaliční	koaliční	k2eAgFnPc1d1	koaliční
síly	síla	k1gFnPc1	síla
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
Prozatímní	prozatímní	k2eAgFnSc4d1	prozatímní
koaliční	koaliční	k2eAgFnSc4d1	koaliční
správu	správa	k1gFnSc4	správa
jako	jako	k8xS	jako
přechodnou	přechodný	k2eAgFnSc4d1	přechodná
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2004	[number]	k4	2004
rozpuštěna	rozpuštěn	k2eAgFnSc1d1	rozpuštěna
a	a	k8xC	a
moci	moc	k1gFnSc3	moc
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
irácká	irácký	k2eAgFnSc1d1	irácká
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
Ajádem	Ajád	k1gMnSc7	Ajád
Alávím	Aláví	k1gNnSc7	Aláví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Iráckého	irácký	k2eAgNnSc2d1	irácké
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
nová	nový	k2eAgFnSc1d1	nová
demokratická	demokratický	k2eAgFnSc1d1	demokratická
ústava	ústava	k1gFnSc1	ústava
a	a	k8xC	a
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Džalál	Džalál	k1gMnSc1	Džalál
Talabání	Talabání	k1gNnSc2	Talabání
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
zvyšovalo	zvyšovat	k5eAaImAgNnS	zvyšovat
násilí	násilí	k1gNnSc1	násilí
mezi	mezi	k7c7	mezi
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itskými	itský	k1gMnPc7	itský
a	a	k8xC	a
sunnitskými	sunnitský	k2eAgMnPc7d1	sunnitský
muslimy	muslim	k1gMnPc7	muslim
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
páchali	páchat	k5eAaImAgMnP	páchat
atentáty	atentát	k1gInPc7	atentát
<g/>
,	,	kIx,	,
a	a	k8xC	a
tyto	tento	k3xDgInPc1	tento
nepokoje	nepokoj	k1gInPc1	nepokoj
vyústily	vyústit	k5eAaPmAgInP	vyústit
v	v	k7c4	v
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
začala	začít	k5eAaPmAgFnS	začít
postupně	postupně	k6eAd1	postupně
uklidňovat	uklidňovat	k5eAaImF	uklidňovat
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nejlepší	dobrý	k2eAgFnSc2d3	nejlepší
úrovně	úroveň	k1gFnSc2	úroveň
od	od	k7c2	od
invaze	invaze	k1gFnSc2	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
přes	přes	k7c4	přes
100	[number]	k4	100
000	[number]	k4	000
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
bojové	bojový	k2eAgFnPc1d1	bojová
jednotky	jednotka	k1gFnPc1	jednotka
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
opustily	opustit	k5eAaPmAgInP	opustit
Irák	Irák	k1gInSc4	Irák
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2010	[number]	k4	2010
a	a	k8xC	a
poslední	poslední	k2eAgInPc1d1	poslední
ze	z	k7c2	z
zbylých	zbylý	k2eAgNnPc2d1	zbylé
asi	asi	k9	asi
50	[number]	k4	50
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
zemi	zem	k1gFnSc6	zem
opustili	opustit	k5eAaPmAgMnP	opustit
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
americké	americký	k2eAgFnSc6d1	americká
intervenci	intervence	k1gFnSc6	intervence
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Irák	Irák	k1gInSc4	Irák
vlna	vlna	k1gFnSc1	vlna
protestů	protest	k1gInPc2	protest
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
arabské	arabský	k2eAgNnSc1d1	arabské
jaro	jaro	k1gNnSc1	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Demonstrací	demonstrace	k1gFnSc7	demonstrace
ve	v	k7c6	v
větších	veliký	k2eAgNnPc6d2	veliký
městech	město	k1gNnPc6	město
se	se	k3xPyFc4	se
účastnily	účastnit	k5eAaImAgInP	účastnit
desetitisíce	desetitisíce	k1gInPc1	desetitisíce
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
požadovali	požadovat	k5eAaImAgMnP	požadovat
snížení	snížení	k1gNnSc4	snížení
korupce	korupce	k1gFnSc2	korupce
<g/>
,	,	kIx,	,
odstoupení	odstoupení	k1gNnSc1	odstoupení
některých	některý	k3yIgMnPc2	některý
vládních	vládní	k2eAgMnPc2d1	vládní
činitelů	činitel	k1gMnPc2	činitel
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgFnSc1d2	lepší
dostupnost	dostupnost	k1gFnSc1	dostupnost
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
nové	nový	k2eAgFnSc2d1	nová
pracovní	pracovní	k2eAgFnSc2d1	pracovní
nabídky	nabídka	k1gFnSc2	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
protestech	protest	k1gInPc6	protest
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
několik	několik	k4yIc1	několik
lidí	člověk	k1gMnPc2	člověk
na	na	k7c4	na
následky	následek	k1gInPc4	následek
střelných	střelný	k2eAgFnPc2d1	střelná
zranění	zranění	k1gNnPc4	zranění
způsobených	způsobený	k2eAgFnPc2d1	způsobená
bezpečnostními	bezpečnostní	k2eAgFnPc7d1	bezpečnostní
složkami	složka	k1gFnPc7	složka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
a	a	k8xC	a
2013	[number]	k4	2013
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
míra	míra	k1gFnSc1	míra
násilí	násilí	k1gNnSc2	násilí
a	a	k8xC	a
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
skupiny	skupina	k1gFnPc1	skupina
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
byli	být	k5eAaImAgMnP	být
burcováni	burcovat	k5eAaImNgMnP	burcovat
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
sunnité	sunnita	k1gMnPc1	sunnita
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
šíité	šíita	k1gMnPc1	šíita
překročili	překročit	k5eAaPmAgMnP	překročit
hranici	hranice	k1gFnSc4	hranice
a	a	k8xC	a
zasáhli	zasáhnout	k5eAaPmAgMnP	zasáhnout
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
protestovali	protestovat	k5eAaBmAgMnP	protestovat
hlavně	hlavně	k9	hlavně
sunnitští	sunnitský	k2eAgMnPc1d1	sunnitský
Arabi	Arab	k1gMnPc1	Arab
proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
snaží	snažit	k5eAaImIp3nS	snažit
odstavit	odstavit	k5eAaPmF	odstavit
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
sunnitské	sunnitský	k2eAgFnPc1d1	sunnitská
militantní	militantní	k2eAgFnPc1d1	militantní
skupiny	skupina	k1gFnPc1	skupina
zintenzivnily	zintenzivnit	k5eAaPmAgFnP	zintenzivnit
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
šíitské	šíitský	k2eAgMnPc4d1	šíitský
obyvatele	obyvatel	k1gMnPc4	obyvatel
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
podkopat	podkopat	k5eAaPmF	podkopat
důvěru	důvěra	k1gFnSc4	důvěra
ve	v	k7c4	v
vládu	vláda	k1gFnSc4	vláda
Núrí	Núrí	k1gFnSc2	Núrí
Málikího	Málikí	k1gMnSc2	Málikí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
sunnitští	sunnitský	k2eAgMnPc1d1	sunnitský
povstalci	povstalec	k1gMnPc1	povstalec
náležící	náležící	k2eAgMnSc1d1	náležící
k	k	k7c3	k
Islámskému	islámský	k2eAgInSc3d1	islámský
státu	stát	k1gInSc3	stát
převzali	převzít	k5eAaPmAgMnP	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
několika	několik	k4yIc7	několik
velkými	velký	k2eAgFnPc7d1	velká
iráckými	irácký	k2eAgFnPc7d1	irácká
městy	město	k1gNnPc7	město
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Tikrítu	Tikrít	k1gInSc2	Tikrít
<g/>
,	,	kIx,	,
Fallúdži	Fallúdž	k1gFnSc3	Fallúdž
a	a	k8xC	a
Mosulu	Mosul	k1gInSc3	Mosul
<g/>
.	.	kIx.	.
<g/>
Islámskému	islámský	k2eAgNnSc3d1	islámské
státu	stát	k1gInSc3	stát
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
využít	využít	k5eAaPmF	využít
chaosu	chaos	k1gInSc3	chaos
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
s	s	k7c7	s
roztříštěnou	roztříštěný	k2eAgFnSc7d1	roztříštěná
vládou	vláda	k1gFnSc7	vláda
i	i	k8xC	i
společností	společnost	k1gFnSc7	společnost
a	a	k8xC	a
vojensky	vojensky	k6eAd1	vojensky
se	se	k3xPyFc4	se
etablovat	etablovat	k5eAaBmF	etablovat
na	na	k7c4	na
značné	značný	k2eAgFnPc4d1	značná
části	část	k1gFnPc4	část
iráckého	irácký	k2eAgMnSc2d1	irácký
i	i	k8xC	i
syrského	syrský	k2eAgNnSc2d1	syrské
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
musela	muset	k5eAaImAgFnS	muset
opustit	opustit	k5eAaPmF	opustit
své	svůj	k3xOyFgInPc4	svůj
domovy	domov	k1gInPc4	domov
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
3	[number]	k4	3
miliony	milion	k4xCgInPc4	milion
vnitřně	vnitřně	k6eAd1	vnitřně
vysídlených	vysídlený	k2eAgFnPc2d1	vysídlená
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
dalších	další	k2eAgInPc2d1	další
8	[number]	k4	8
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
kvůli	kvůli	k7c3	kvůli
konfliktu	konflikt	k1gInSc3	konflikt
akutní	akutní	k2eAgFnSc1d1	akutní
humanitární	humanitární	k2eAgFnSc1d1	humanitární
pomoc	pomoc	k1gFnSc1	pomoc
<g/>
.	.	kIx.	.
<g/>
Mosul	Mosul	k1gInSc1	Mosul
byl	být	k5eAaImAgInS	být
osvobozen	osvobodit	k5eAaPmNgInS	osvobodit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2017	[number]	k4	2017
iráckou	irácký	k2eAgFnSc7d1	irácká
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
kurdskými	kurdský	k2eAgFnPc7d1	kurdská
milicemi	milice	k1gFnPc7	milice
za	za	k7c2	za
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
letecké	letecký	k2eAgFnSc2d1	letecká
a	a	k8xC	a
logistické	logistický	k2eAgFnSc2d1	logistická
podpory	podpora	k1gFnSc2	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
bojích	boj	k1gInPc6	boj
o	o	k7c4	o
Mosul	Mosul	k1gInSc4	Mosul
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
tisíc	tisíc	k4xCgInPc2	tisíc
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Irák	Irák	k1gInSc1	Irák
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Asii	Asie	k1gFnSc6	Asie
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
označované	označovaný	k2eAgFnSc6d1	označovaná
jako	jako	k8xC	jako
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
osmi	osm	k4xCc2	osm
států	stát	k1gInPc2	stát
ležících	ležící	k2eAgInPc2d1	ležící
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
pobřeží	pobřeží	k1gNnSc1	pobřeží
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
délky	délka	k1gFnSc2	délka
58	[number]	k4	58
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
činí	činit	k5eAaImIp3nS	činit
438	[number]	k4	438
317	[number]	k4	317
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
950	[number]	k4	950
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgFnSc2d1	vodní
plochy	plocha	k1gFnSc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Kuvajtem	Kuvajt	k1gInSc7	Kuvajt
a	a	k8xC	a
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Jordánskem	Jordánsko	k1gNnSc7	Jordánsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
se	s	k7c7	s
Sýrií	Sýrie	k1gFnSc7	Sýrie
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Íránem	Írán	k1gInSc7	Írán
<g/>
.	.	kIx.	.
<g/>
Irák	Irák	k1gInSc1	Irák
lze	lze	k6eAd1	lze
topograficky	topograficky	k6eAd1	topograficky
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
pouště	poušť	k1gFnSc2	poušť
typické	typický	k2eAgFnPc1d1	typická
pro	pro	k7c4	pro
irácký	irácký	k2eAgInSc4d1	irácký
<g />
.	.	kIx.	.
</s>
<s>
západ	západ	k1gInSc1	západ
a	a	k8xC	a
jihozápad	jihozápad	k1gInSc1	jihozápad
<g/>
;	;	kIx,	;
vysočiny	vysočina	k1gFnPc1	vysočina
mezi	mezi	k7c7	mezi
řekami	řeka	k1gFnPc7	řeka
Eufrat	Eufrat	k1gInSc4	Eufrat
a	a	k8xC	a
Tigris	Tigris	k1gInSc4	Tigris
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
začínající	začínající	k2eAgFnSc1d1	začínající
asi	asi	k9	asi
120	[number]	k4	120
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Bagdádu	Bagdád	k1gInSc2	Bagdád
<g/>
;	;	kIx,	;
aluviální	aluviální	k2eAgFnSc2d1	aluviální
roviny	rovina	k1gFnSc2	rovina
v	v	k7c6	v
centrálním	centrální	k2eAgInSc6d1	centrální
a	a	k8xC	a
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zabírají	zabírat	k5eAaImIp3nP	zabírat
území	území	k1gNnPc1	území
větší	veliký	k2eAgFnSc2d2	veliký
než	než	k8xS	než
132	[number]	k4	132
000	[number]	k4	000
km2	km2	k4	km2
<g/>
;	;	kIx,	;
a	a	k8xC	a
pohoří	pohoří	k1gNnSc4	pohoří
severovýchodu	severovýchod	k1gInSc2	severovýchod
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
Turecka	Turecko	k1gNnSc2	Turecko
a	a	k8xC	a
Íránu	Írán	k1gInSc2	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
Cheekha	Cheekha	k1gFnSc1	Cheekha
Dar	dar	k1gInSc1	dar
s	s	k7c7	s
3	[number]	k4	3
611	[number]	k4	611
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
nejnižším	nízký	k2eAgMnSc7d3	nejnižší
je	být	k5eAaImIp3nS	být
hladina	hladina	k1gFnSc1	hladina
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
0	[number]	k4	0
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
Největšími	veliký	k2eAgFnPc7d3	veliký
a	a	k8xC	a
nejvýznamnějšími	významný	k2eAgFnPc7d3	nejvýznamnější
řekami	řeka	k1gFnPc7	řeka
Iráku	Irák	k1gInSc2	Irák
jsou	být	k5eAaImIp3nP	být
Eufrat	Eufrat	k1gInSc1	Eufrat
a	a	k8xC	a
Tigris	Tigris	k1gInSc1	Tigris
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2500	[number]	k4	2500
km	km	kA	km
spojují	spojovat	k5eAaImIp3nP	spojovat
asi	asi	k9	asi
150	[number]	k4	150
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
řeky	řeka	k1gFnPc4	řeka
patří	patřit	k5eAaImIp3nP	patřit
Chábúr	Chábúr	k1gMnSc1	Chábúr
<g/>
,	,	kIx,	,
Velký	velký	k2eAgMnSc1d1	velký
Zab	Zab	k1gMnSc1	Zab
<g/>
,	,	kIx,	,
Malý	Malý	k1gMnSc1	Malý
Zab	Zab	k1gMnSc1	Zab
<g/>
,	,	kIx,	,
Dijála	Dijála	k1gFnSc1	Dijála
<g/>
,	,	kIx,	,
Šatt	Šatt	k2eAgInSc1d1	Šatt
al-Arab	al-Arab	k1gInSc1	al-Arab
a	a	k8xC	a
Kerche	Kerche	k1gInSc1	Kerche
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
13	[number]	k4	13
%	%	kIx~	%
půdy	půda	k1gFnSc2	půda
je	být	k5eAaImIp3nS	být
zemědělsky	zemědělsky	k6eAd1	zemědělsky
využitelných	využitelný	k2eAgMnPc2d1	využitelný
<g/>
,	,	kIx,	,
nejúrodnější	úrodný	k2eAgFnSc1d3	nejúrodnější
oblast	oblast	k1gFnSc1	oblast
jsou	být	k5eAaImIp3nP	být
aluviální	aluviální	k2eAgFnPc4d1	aluviální
roviny	rovina	k1gFnPc4	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgNnPc7d3	veliký
jezery	jezero	k1gNnPc7	jezero
jsou	být	k5eAaImIp3nP	být
Tharthar	Tharthara	k1gFnPc2	Tharthara
a	a	k8xC	a
Milh	Milha	k1gFnPc2	Milha
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
vodní	vodní	k2eAgFnSc7d1	vodní
přehradou	přehrada	k1gFnSc7	přehrada
je	být	k5eAaImIp3nS	být
Mosulská	mosulský	k2eAgFnSc1d1	mosulský
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
největší	veliký	k2eAgFnSc7d3	veliký
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
<g/>
Irák	Irák	k1gInSc1	Irák
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
dvou	dva	k4xCgInPc2	dva
ekoregionů	ekoregion	k1gInPc2	ekoregion
<g/>
:	:	kIx,	:
říčního	říční	k2eAgInSc2d1	říční
systému	systém	k1gInSc2	systém
Eufrat-Tigris	Eufrat-Tigris	k1gFnSc2	Eufrat-Tigris
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
Íránsko-anatolského	íránskonatolský	k2eAgInSc2d1	íránsko-anatolský
hotspotu	hotspot	k1gInSc2	hotspot
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgMnPc4d3	nejčastější
živočichy	živočich	k1gMnPc4	živočich
patří	patřit	k5eAaImIp3nP	patřit
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
vlk	vlk	k1gMnSc1	vlk
<g/>
,	,	kIx,	,
liška	liška	k1gFnSc1	liška
<g/>
,	,	kIx,	,
šakal	šakal	k1gMnSc1	šakal
<g/>
,	,	kIx,	,
hyena	hyena	k1gFnSc1	hyena
<g/>
,	,	kIx,	,
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
a	a	k8xC	a
kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
<g/>
.	.	kIx.	.
</s>
<s>
Irák	Irák	k1gInSc1	Irák
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
endemických	endemický	k2eAgInPc2d1	endemický
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgInPc4	který
patří	patřit	k5eAaImIp3nP	patřit
ryby	ryba	k1gFnPc1	ryba
parmoun	parmoun	k1gInSc4	parmoun
Widdowsonův	Widdowsonův	k2eAgInSc4d1	Widdowsonův
(	(	kIx(	(
<g/>
Typhlogarra	Typhlogarra	k1gFnSc1	Typhlogarra
widdowsoni	widdowson	k1gMnPc5	widdowson
<g/>
)	)	kIx)	)
a	a	k8xC	a
Caecocypris	Caecocypris	k1gFnSc3	Caecocypris
basimi	basi	k1gFnPc7	basi
<g/>
,	,	kIx,	,
krysa	krysa	k1gFnSc1	krysa
Bunnova	Bunnův	k2eAgFnSc1d1	Bunnův
(	(	kIx(	(
<g/>
Nesokia	Nesokia	k1gFnSc1	Nesokia
bunnii	bunnium	k1gNnPc7	bunnium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
bezobratlých	bezobratlý	k2eAgFnPc2d1	bezobratlý
a	a	k8xC	a
asi	asi	k9	asi
190	[number]	k4	190
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
trpí	trpět	k5eAaImIp3nS	trpět
znečišťováním	znečišťování	k1gNnSc7	znečišťování
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
erozí	eroze	k1gFnSc7	eroze
<g/>
,	,	kIx,	,
dezertifikací	dezertifikace	k1gFnSc7	dezertifikace
a	a	k8xC	a
snahami	snaha	k1gFnPc7	snaha
vlády	vláda	k1gFnSc2	vláda
o	o	k7c4	o
změnu	změna	k1gFnSc4	změna
toku	tok	k1gInSc2	tok
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgFnPc1	dva
podnebné	podnebný	k2eAgFnPc1d1	podnebná
oblasti	oblast	k1gFnPc1	oblast
<g/>
:	:	kIx,	:
většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc2	území
zabírají	zabírat	k5eAaImIp3nP	zabírat
níže	nízce	k6eAd2	nízce
položené	položený	k2eAgFnPc1d1	položená
oblasti	oblast	k1gFnPc1	oblast
s	s	k7c7	s
aridním	aridní	k2eAgNnSc7d1	aridní
a	a	k8xC	a
teplým	teplý	k2eAgNnSc7d1	teplé
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
pohoří	pohoří	k1gNnSc4	pohoří
s	s	k7c7	s
nižšími	nízký	k2eAgFnPc7d2	nižší
teplotami	teplota	k1gFnPc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouště	poušť	k1gFnSc2	poušť
a	a	k8xC	a
aluviální	aluviální	k2eAgFnSc2d1	aluviální
roviny	rovina	k1gFnSc2	rovina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
střídá	střídat	k5eAaImIp3nS	střídat
léto	léto	k1gNnSc1	léto
a	a	k8xC	a
zima	zima	k1gFnSc1	zima
s	s	k7c7	s
krátkými	krátký	k2eAgNnPc7d1	krátké
přechodnými	přechodný	k2eAgNnPc7d1	přechodné
obdobími	období	k1gNnPc7	období
<g/>
.	.	kIx.	.
</s>
<s>
Léto	léto	k1gNnSc1	léto
trvající	trvající	k2eAgNnSc1d1	trvající
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
teplé	teplý	k2eAgNnSc1d1	teplé
<g/>
,	,	kIx,	,
průměrné	průměrný	k2eAgFnPc1d1	průměrná
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
22	[number]	k4	22
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
38	[number]	k4	38
°	°	k?	°
<g/>
C.	C.	kA	C.
Ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
je	být	k5eAaImIp3nS	být
období	období	k1gNnSc1	období
léta	léto	k1gNnSc2	léto
kratší	krátký	k2eAgFnSc2d2	kratší
–	–	k?	–
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
září	září	k1gNnSc2	září
–	–	k?	–
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
chladnější	chladný	k2eAgFnPc1d2	chladnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
spadne	spadnout	k5eAaPmIp3nS	spadnout
zhruba	zhruba	k6eAd1	zhruba
90	[number]	k4	90
%	%	kIx~	%
celoročních	celoroční	k2eAgFnPc2d1	celoroční
srážek	srážka	k1gFnPc2	srážka
<g/>
;	;	kIx,	;
ročně	ročně	k6eAd1	ročně
spadne	spadnout	k5eAaPmIp3nS	spadnout
mezi	mezi	k7c7	mezi
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
170	[number]	k4	170
mm	mm	kA	mm
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
a	a	k8xC	a
320	[number]	k4	320
<g/>
–	–	k?	–
<g/>
570	[number]	k4	570
mm	mm	kA	mm
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Teploty	teplota	k1gFnPc1	teplota
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
hodnot	hodnota	k1gFnPc2	hodnota
mezi	mezi	k7c7	mezi
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
17	[number]	k4	17
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
==	==	k?	==
Vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
<s>
Irák	Irák	k1gInSc1	Irák
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
Sněmovnou	sněmovna	k1gFnSc7	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
jím	on	k3xPp3gInSc7	on
je	být	k5eAaImIp3nS	být
Fuád	Fuád	k1gInSc1	Fuád
Masúm	Masúma	k1gFnPc2	Masúma
<g/>
.	.	kIx.	.
</s>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
premiér	premiér	k1gMnSc1	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
určován	určovat	k5eAaImNgInS	určovat
prezidentem	prezident	k1gMnSc7	prezident
z	z	k7c2	z
většinové	většinový	k2eAgFnSc2d1	většinová
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
a	a	k8xC	a
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
post	post	k1gInSc1	post
zastává	zastávat	k5eAaImIp3nS	zastávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
Hajdar	Hajdara	k1gFnPc2	Hajdara
Abádí	Abádí	k1gNnSc2	Abádí
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
představují	představovat	k5eAaImIp3nP	představovat
Sněmovna	sněmovna	k1gFnSc1	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
a	a	k8xC	a
Sněmovna	sněmovna	k1gFnSc1	sněmovna
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
reprezentantů	reprezentant	k1gMnPc2	reprezentant
je	být	k5eAaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Iráku	Irák	k1gInSc2	Irák
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
irácké	irácký	k2eAgFnSc2d1	irácká
ústavy	ústava	k1gFnSc2	ústava
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
1	[number]	k4	1
reprezentant	reprezentant	k1gInSc1	reprezentant
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
jich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
325	[number]	k4	325
<g/>
,	,	kIx,	,
volby	volba	k1gFnPc1	volba
probíhají	probíhat	k5eAaImIp3nP	probíhat
každé	každý	k3xTgInPc1	každý
čtyři	čtyři	k4xCgInPc1	čtyři
roky	rok	k1gInPc1	rok
a	a	k8xC	a
poslední	poslední	k2eAgFnPc1d1	poslední
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Nejlépe	dobře	k6eAd3	dobře
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
koalice	koalice	k1gFnSc2	koalice
Irácké	irácký	k2eAgNnSc4d1	irácké
národní	národní	k2eAgNnSc4d1	národní
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sebe	sebe	k3xPyFc4	sebe
samu	sám	k3xTgFnSc4	sám
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k9	jako
sekulární	sekulární	k2eAgFnSc4d1	sekulární
a	a	k8xC	a
nesektářskou	sektářský	k2eNgFnSc4d1	nesektářská
koalici	koalice	k1gFnSc4	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
Málikího	Málikí	k1gMnSc4	Málikí
Koalice	koalice	k1gFnSc2	koalice
právního	právní	k2eAgInSc2d1	právní
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
zvýšit	zvýšit	k5eAaPmF	zvýšit
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
třetí	třetí	k4xOgFnSc6	třetí
pozici	pozice	k1gFnSc6	pozice
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itská	itskat	k5eAaImIp3nS	itskat
Národní	národní	k2eAgFnSc1d1	národní
irácká	irácký	k2eAgFnSc1d1	irácká
aliance	aliance	k1gFnSc1	aliance
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
stranou	strana	k1gFnSc7	strana
bylo	být	k5eAaImAgNnS	být
Sadristické	Sadristický	k2eAgNnSc1d1	Sadristický
hnutí	hnutí	k1gNnSc1	hnutí
(	(	kIx(	(
<g/>
z	z	k7c2	z
Národní	národní	k2eAgFnSc2d1	národní
irácké	irácký	k2eAgFnSc2d1	irácká
aliance	aliance	k1gFnSc2	aliance
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
kurdských	kurdský	k2eAgFnPc2d1	kurdská
stran	strana	k1gFnPc2	strana
Kurdská	kurdský	k2eAgFnSc1d1	kurdská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
Iráku	Irák	k1gInSc2	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
soudní	soudní	k2eAgFnSc7d1	soudní
autoritou	autorita	k1gFnSc7	autorita
je	být	k5eAaImIp3nS	být
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
federální	federální	k2eAgInSc1d1	federální
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
50	[number]	k4	50
000	[number]	k4	000
amerických	americký	k2eAgMnPc2d1	americký
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
poslední	poslední	k2eAgFnSc6d1	poslední
zemi	zem	k1gFnSc6	zem
opustili	opustit	k5eAaPmAgMnP	opustit
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
kvůli	kvůli	k7c3	kvůli
pomoci	pomoc	k1gFnSc3	pomoc
iráckým	irácký	k2eAgMnPc3d1	irácký
protějškům	protějšek	k1gInPc3	protějšek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nová	nový	k2eAgFnSc1d1	nová
demokratická	demokratický	k2eAgFnSc1d1	demokratická
ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
oddělující	oddělující	k2eAgInPc1d1	oddělující
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
<g/>
,	,	kIx,	,
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
a	a	k8xC	a
soudní	soudní	k2eAgFnSc4d1	soudní
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
téměř	téměř	k6eAd1	téměř
80	[number]	k4	80
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1968	[number]	k4	1968
a	a	k8xC	a
2003	[number]	k4	2003
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
strany	strana	k1gFnSc2	strana
Baas	Baas	k1gInSc1	Baas
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
existuje	existovat	k5eAaImIp3nS	existovat
systém	systém	k1gInSc1	systém
více	hodně	k6eAd2	hodně
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Politika	politika	k1gFnSc1	politika
Iráku	Irák	k1gInSc2	Irák
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
zkorumpovaná	zkorumpovaný	k2eAgFnSc1d1	zkorumpovaná
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
index	index	k1gInSc4	index
zhroucených	zhroucený	k2eAgInPc2d1	zhroucený
států	stát	k1gInPc2	stát
vytvářený	vytvářený	k2eAgInSc1d1	vytvářený
časopisem	časopis	k1gInSc7	časopis
Foreign	Foreign	k1gMnSc1	Foreign
Policy	Policy	k1gInPc4	Policy
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
7	[number]	k4	7
<g/>
.	.	kIx.	.
největší	veliký	k2eAgInSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Irák	Irák	k1gInSc1	Irák
se	se	k3xPyFc4	se
také	také	k9	také
pravidelně	pravidelně	k6eAd1	pravidelně
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
na	na	k7c6	na
posledních	poslední	k2eAgNnPc6d1	poslední
místech	místo	k1gNnPc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
podle	podle	k7c2	podle
světového	světový	k2eAgInSc2d1	světový
indexu	index	k1gInSc2	index
míru	mír	k1gInSc2	mír
sestavovaného	sestavovaný	k2eAgInSc2d1	sestavovaný
Institutem	institut	k1gInSc7	institut
pro	pro	k7c4	pro
ekonomii	ekonomie	k1gFnSc4	ekonomie
a	a	k8xC	a
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
měl	mít	k5eAaImAgInS	mít
tento	tento	k3xDgInSc1	tento
index	index	k1gInSc1	index
po	po	k7c6	po
Somálsku	Somálsko	k1gNnSc6	Somálsko
nejmenší	malý	k2eAgInPc1d3	nejmenší
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
přítomnosti	přítomnost	k1gFnSc3	přítomnost
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
vládě	vláda	k1gFnSc6	vláda
bojují	bojovat	k5eAaImIp3nP	bojovat
např.	např.	kA	např.
al-Káida	al-Káida	k1gFnSc1	al-Káida
a	a	k8xC	a
různé	různý	k2eAgFnPc1d1	různá
sunnitské	sunnitský	k2eAgFnPc1d1	sunnitská
a	a	k8xC	a
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itské	itský	k2eAgFnPc1d1	itská
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
páchají	páchat	k5eAaImIp3nP	páchat
veřejné	veřejný	k2eAgInPc1d1	veřejný
sebevražedné	sebevražedný	k2eAgInPc1d1	sebevražedný
atentáty	atentát	k1gInPc1	atentát
<g/>
,	,	kIx,	,
vraždy	vražda	k1gFnPc1	vražda
<g/>
,	,	kIx,	,
únosy	únos	k1gInPc1	únos
a	a	k8xC	a
mučení	mučení	k1gNnSc1	mučení
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
cílem	cíl	k1gInSc7	cíl
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
příslušníci	příslušník	k1gMnPc1	příslušník
etnických	etnický	k2eAgFnPc2d1	etnická
a	a	k8xC	a
náboženských	náboženský	k2eAgFnPc2d1	náboženská
menšin	menšina	k1gFnPc2	menšina
<g/>
,	,	kIx,	,
státní	státní	k2eAgMnPc1d1	státní
úředníci	úředník	k1gMnPc1	úředník
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
soudci	soudce	k1gMnSc3	soudce
atd.	atd.	kA	atd.
Na	na	k7c6	na
politickém	politický	k2eAgNnSc6d1	politické
poli	pole	k1gNnSc6	pole
spolu	spolu	k6eAd1	spolu
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
ité	ité	k?	ité
a	a	k8xC	a
sunnité	sunnita	k1gMnPc1	sunnita
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
<g/>
.	.	kIx.	.
<g/>
Amnesty	Amnest	k1gInPc4	Amnest
International	International	k1gFnPc2	International
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
sunnitů	sunnita	k1gMnPc2	sunnita
podezřelých	podezřelý	k2eAgMnPc2d1	podezřelý
z	z	k7c2	z
financování	financování	k1gNnSc2	financování
sunnitských	sunnitský	k2eAgFnPc2d1	sunnitská
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
skupin	skupina	k1gFnPc2	skupina
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
oběťmi	oběť	k1gFnPc7	oběť
porušování	porušování	k1gNnPc4	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
svévolné	svévolný	k2eAgNnSc1d1	svévolné
zatýkání	zatýkání	k1gNnSc1	zatýkání
<g/>
,	,	kIx,	,
mučení	mučení	k1gNnSc1	mučení
a	a	k8xC	a
znásilňování	znásilňování	k1gNnSc1	znásilňování
(	(	kIx(	(
<g/>
v	v	k7c6	v
tajných	tajný	k2eAgFnPc6d1	tajná
věznicích	věznice	k1gFnPc6	věznice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k8xC	i
odsouzení	odsouzení	k1gNnSc4	odsouzení
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
po	po	k7c6	po
nespravedlivých	spravedlivý	k2eNgInPc6d1	nespravedlivý
soudních	soudní	k2eAgInPc6d1	soudní
procesech	proces	k1gInPc6	proces
<g/>
.	.	kIx.	.
</s>
<s>
Lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
porušovali	porušovat	k5eAaImAgMnP	porušovat
i	i	k8xC	i
američtí	americký	k2eAgMnPc1d1	americký
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
zabili	zabít	k5eAaPmAgMnP	zabít
několik	několik	k4yIc4	několik
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
shromáždění	shromáždění	k1gNnSc2	shromáždění
je	být	k5eAaImIp3nS	být
garantováno	garantovat	k5eAaBmNgNnS	garantovat
ústavou	ústava	k1gFnSc7	ústava
a	a	k8xC	a
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
dodržuje	dodržovat	k5eAaImIp3nS	dodržovat
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
slova	slovo	k1gNnSc2	slovo
je	být	k5eAaImIp3nS	být
také	také	k9	také
zaručené	zaručený	k2eAgNnSc1d1	zaručené
právně	právně	k6eAd1	právně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
narušuje	narušovat	k5eAaImIp3nS	narušovat
ho	on	k3xPp3gNnSc4	on
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
různými	různý	k2eAgFnPc7d1	různá
skupinami	skupina	k1gFnPc7	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
přes	přes	k7c4	přes
150	[number]	k4	150
periodik	periodikum	k1gNnPc2	periodikum
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
fungují	fungovat	k5eAaImIp3nP	fungovat
bez	bez	k7c2	bez
výrazných	výrazný	k2eAgFnPc2d1	výrazná
omezení	omezení	k1gNnPc2	omezení
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
zákon	zákon	k1gInSc4	zákon
zakazující	zakazující	k2eAgNnPc4d1	zakazující
kritizování	kritizování	k1gNnSc4	kritizování
veřejných	veřejný	k2eAgMnPc2d1	veřejný
činitelů	činitel	k1gMnPc2	činitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
<s>
Irák	Irák	k1gInSc1	Irák
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
19	[number]	k4	19
guvernorátů	guvernorát	k1gInPc2	guvernorát
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
muháfaza	muháfaz	k1gMnSc2	muháfaz
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
provincií	provincie	k1gFnSc7	provincie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
nový	nový	k2eAgInSc1d1	nový
guvernorát	guvernorát	k1gInSc1	guvernorát
Halabdža	Halabdž	k1gInSc2	Halabdž
v	v	k7c6	v
Iráckém	irácký	k2eAgInSc6d1	irácký
Kurdistánu	Kurdistán	k1gInSc6	Kurdistán
<g/>
.	.	kIx.	.
</s>
<s>
Guvernoráty	Guvernorát	k1gInPc1	Guvernorát
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
okresy	okres	k1gInPc4	okres
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgNnPc4d3	nejvýznamnější
města	město	k1gNnPc4	město
patří	patřit	k5eAaImIp3nS	patřit
Bagdád	Bagdád	k1gInSc1	Bagdád
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
5	[number]	k4	5
700	[number]	k4	700
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mosul	Mosul	k1gInSc1	Mosul
(	(	kIx(	(
<g/>
1	[number]	k4	1
450	[number]	k4	450
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Arbíl	Arbíl	k1gMnSc1	Arbíl
(	(	kIx(	(
<g/>
1	[number]	k4	1
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Basra	Basra	k1gMnSc1	Basra
(	(	kIx(	(
<g/>
923	[number]	k4	923
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sulajmáníja	Sulajmáníja	k1gMnSc1	Sulajmáníja
(	(	kIx(	(
<g/>
836	[number]	k4	836
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Irácký	irácký	k2eAgInSc1d1	irácký
Kurdistán	Kurdistán	k1gInSc1	Kurdistán
====	====	k?	====
</s>
</p>
<p>
<s>
Irácký	irácký	k2eAgInSc1d1	irácký
Kurdistán	Kurdistán	k1gInSc1	Kurdistán
je	být	k5eAaImIp3nS	být
právně	právně	k6eAd1	právně
vymezená	vymezený	k2eAgFnSc1d1	vymezená
autonomní	autonomní	k2eAgFnSc1d1	autonomní
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
irácké	irácký	k2eAgFnSc2d1	irácká
provincie	provincie	k1gFnSc2	provincie
Arbíl	Arbíl	k1gMnSc1	Arbíl
<g/>
,	,	kIx,	,
Dahúk	Dahúk	k1gMnSc1	Dahúk
<g/>
,	,	kIx,	,
Halabdža	Halabdža	k1gMnSc1	Halabdža
a	a	k8xC	a
Sulajmáníja	Sulajmáníja	k1gMnSc1	Sulajmáníja
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc4	území
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
asi	asi	k9	asi
36	[number]	k4	36
000	[number]	k4	000
km2	km2	k4	km2
a	a	k8xC	a
4	[number]	k4	4
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Iráku	Irák	k1gInSc2	Irák
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
finančně	finančně	k6eAd1	finančně
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
Bagdádu	Bagdád	k1gInSc6	Bagdád
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
rozdílnou	rozdílný	k2eAgFnSc4d1	rozdílná
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnSc4d1	vlastní
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
vojenské	vojenský	k2eAgFnPc4d1	vojenská
jednotky	jednotka	k1gFnPc4	jednotka
(	(	kIx(	(
<g/>
zvané	zvaný	k2eAgFnPc4d1	zvaná
pešmerga	pešmerg	k1gMnSc4	pešmerg
<g/>
)	)	kIx)	)
a	a	k8xC	a
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
s	s	k7c7	s
arabským	arabský	k2eAgInSc7d1	arabský
Irákem	Irák	k1gInSc7	Irák
hlídají	hlídat	k5eAaImIp3nP	hlídat
bezpečnostní	bezpečnostní	k2eAgFnPc1d1	bezpečnostní
složky	složka	k1gFnPc1	složka
a	a	k8xC	a
arabští	arabský	k2eAgMnPc1d1	arabský
Iráčané	Iráčan	k1gMnPc1	Iráčan
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Kurdistánu	Kurdistán	k1gInSc6	Kurdistán
často	často	k6eAd1	často
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
cizince	cizinec	k1gMnPc4	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
byla	být	k5eAaImAgFnS	být
vymezena	vymezit	k5eAaPmNgFnS	vymezit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
de	de	k?	de
facto	facto	k1gNnSc4	facto
autonomní	autonomní	k2eAgFnSc2d1	autonomní
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
její	její	k3xOp3gNnSc4	její
území	území	k1gNnSc4	území
opustily	opustit	k5eAaPmAgFnP	opustit
irácké	irácký	k2eAgFnPc1d1	irácká
vojenské	vojenský	k2eAgFnPc1d1	vojenská
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
také	také	k6eAd1	také
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Organizace	organizace	k1gFnSc1	organizace
nezastoupených	zastoupený	k2eNgInPc2d1	nezastoupený
států	stát	k1gInPc2	stát
a	a	k8xC	a
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
kurdskou	kurdský	k2eAgFnSc7d1	kurdská
koalicí	koalice	k1gFnSc7	koalice
je	být	k5eAaImIp3nS	být
Kurdská	kurdský	k2eAgFnSc1d1	kurdská
aliance	aliance	k1gFnSc1	aliance
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
11	[number]	k4	11
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Kurdské	kurdský	k2eAgFnSc2d1	kurdská
regionální	regionální	k2eAgFnSc2d1	regionální
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
získala	získat	k5eAaPmAgFnS	získat
59	[number]	k4	59
ze	z	k7c2	z
111	[number]	k4	111
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
prezidentem	prezident	k1gMnSc7	prezident
kurdského	kurdský	k2eAgInSc2d1	kurdský
regionu	region	k1gInSc2	region
je	být	k5eAaImIp3nS	být
Masúd	Masúd	k1gInSc1	Masúd
Barzání	Barzání	k1gNnSc2	Barzání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
vztahy	vztah	k1gInPc1	vztah
===	===	k?	===
</s>
</p>
<p>
<s>
Klíčovým	klíčový	k2eAgMnSc7d1	klíčový
spojencem	spojenec	k1gMnSc7	spojenec
Iráku	Irák	k1gInSc2	Irák
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
kvůli	kvůli	k7c3	kvůli
pomoci	pomoc	k1gFnSc3	pomoc
při	při	k7c6	při
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Irák	Irák	k1gInSc1	Irák
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
několika	několik	k4yIc2	několik
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gInPc2	člen
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
Ligy	liga	k1gFnSc2	liga
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
a	a	k8xC	a
Organizace	organizace	k1gFnSc2	organizace
zemí	zem	k1gFnPc2	zem
vyvážejících	vyvážející	k2eAgFnPc2d1	vyvážející
ropu	ropa	k1gFnSc4	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Saddáma	Saddám	k1gMnSc4	Saddám
Husajna	Husajn	k1gMnSc4	Husajn
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
navazovat	navazovat	k5eAaImF	navazovat
a	a	k8xC	a
udržovat	udržovat	k5eAaImF	udržovat
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
hlavně	hlavně	k9	hlavně
se	s	k7c7	s
sousedními	sousední	k2eAgInPc7d1	sousední
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
posílit	posílit	k5eAaPmF	posílit
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
dominantního	dominantní	k2eAgMnSc2d1	dominantní
obchodního	obchodní	k2eAgMnSc2d1	obchodní
partnera	partner	k1gMnSc2	partner
Iráku	Irák	k1gInSc2	Irák
a	a	k8xC	a
investuje	investovat	k5eAaBmIp3nS	investovat
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
převážně	převážně	k6eAd1	převážně
do	do	k7c2	do
energetiky	energetika	k1gFnSc2	energetika
<g/>
.	.	kIx.	.
</s>
<s>
Vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
obchodní	obchodní	k2eAgFnSc1d1	obchodní
výměna	výměna	k1gFnSc1	výměna
s	s	k7c7	s
Íránem	Írán	k1gInSc7	Írán
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
podepsány	podepsán	k2eAgFnPc1d1	podepsána
dohody	dohoda	k1gFnPc1	dohoda
o	o	k7c6	o
ropné	ropný	k2eAgFnSc6d1	ropná
spolupráci	spolupráce	k1gFnSc6	spolupráce
a	a	k8xC	a
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
je	být	k5eAaImIp3nS	být
naplánováno	naplánován	k2eAgNnSc4d1	naplánováno
odstraňování	odstraňování	k1gNnSc4	odstraňování
celních	celní	k2eAgFnPc2d1	celní
a	a	k8xC	a
obchodních	obchodní	k2eAgFnPc2d1	obchodní
překážek	překážka	k1gFnPc2	překážka
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Kuvajtem	Kuvajt	k1gInSc7	Kuvajt
se	se	k3xPyFc4	se
Irák	Irák	k1gInSc1	Irák
snaží	snažit	k5eAaImIp3nS	snažit
vyřešit	vyřešit	k5eAaPmF	vyřešit
otázku	otázka	k1gFnSc4	otázka
válečných	válečný	k2eAgFnPc2d1	válečná
reparací	reparace	k1gFnPc2	reparace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mu	on	k3xPp3gInSc3	on
má	mít	k5eAaImIp3nS	mít
platit	platit	k5eAaImF	platit
<g/>
.	.	kIx.	.
</s>
<s>
Částku	částka	k1gFnSc4	částka
stanovenou	stanovený	k2eAgFnSc4d1	stanovená
jako	jako	k8xS	jako
5	[number]	k4	5
%	%	kIx~	%
příjmů	příjem	k1gInPc2	příjem
z	z	k7c2	z
ropy	ropa	k1gFnSc2	ropa
se	se	k3xPyFc4	se
Irák	Irák	k1gInSc1	Irák
snaží	snažit	k5eAaImIp3nS	snažit
snížit	snížit	k5eAaPmF	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
členem	člen	k1gInSc7	člen
Světové	světový	k2eAgFnSc2d1	světová
obchodní	obchodní	k2eAgFnSc2d1	obchodní
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
WTO	WTO	kA	WTO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c6	o
členství	členství	k1gNnSc6	členství
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
status	status	k1gInSc4	status
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
od	od	k7c2	od
invaze	invaze	k1gFnSc2	invaze
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
Iráku	Irák	k1gInSc6	Irák
finanční	finanční	k2eAgFnSc4d1	finanční
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
asi	asi	k9	asi
1	[number]	k4	1
miliardy	miliarda	k4xCgFnSc2	miliarda
euro	euro	k1gNnSc1	euro
na	na	k7c4	na
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
země	zem	k1gFnSc2	zem
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
,	,	kIx,	,
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
prosazování	prosazování	k1gNnSc2	prosazování
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
dostupnosti	dostupnost	k1gFnSc2	dostupnost
základních	základní	k2eAgFnPc2d1	základní
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2006	[number]	k4	2006
začalo	začít	k5eAaPmAgNnS	začít
vyjednávání	vyjednávání	k1gNnSc1	vyjednávání
mezi	mezi	k7c7	mezi
EU	EU	kA	EU
a	a	k8xC	a
Irákem	Irák	k1gInSc7	Irák
na	na	k7c6	na
podpisu	podpis	k1gInSc6	podpis
partnerské	partnerský	k2eAgFnSc2d1	partnerská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
politické	politický	k2eAgFnSc6d1	politická
a	a	k8xC	a
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
spolupráci	spolupráce	k1gFnSc6	spolupráce
dodala	dodat	k5eAaPmAgFnS	dodat
právní	právní	k2eAgInSc4d1	právní
základ	základ	k1gInSc4	základ
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
udržuje	udržovat	k5eAaImIp3nS	udržovat
Irák	Irák	k1gInSc1	Irák
nadále	nadále	k6eAd1	nadále
úzké	úzký	k2eAgInPc4d1	úzký
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
vliv	vliv	k1gInSc4	vliv
USA	USA	kA	USA
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
amerických	americký	k2eAgMnPc2d1	americký
vojáků	voják	k1gMnPc2	voják
postupně	postupně	k6eAd1	postupně
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
<g/>
Česko	Česko	k1gNnSc1	Česko
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Bagdádu	Bagdád	k1gInSc6	Bagdád
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
a	a	k8xC	a
Irák	Irák	k1gInSc1	Irák
má	mít	k5eAaImIp3nS	mít
velvyslanectví	velvyslanectví	k1gNnSc4	velvyslanectví
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
státy	stát	k1gInPc7	stát
je	být	k5eAaImIp3nS	být
podepsáno	podepsat	k5eAaPmNgNnS	podepsat
několik	několik	k4yIc1	několik
smluv	smlouva	k1gFnPc2	smlouva
<g/>
,	,	kIx,	,
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
jsou	být	k5eAaImIp3nP	být
plánovány	plánován	k2eAgFnPc1d1	plánována
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
zabránění	zabránění	k1gNnSc6	zabránění
dvojímu	dvojí	k4xRgInSc3	dvojí
zdanění	zdanění	k1gNnSc6	zdanění
a	a	k8xC	a
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
ochraně	ochrana	k1gFnSc6	ochrana
a	a	k8xC	a
podpoře	podpora	k1gFnSc6	podpora
investic	investice	k1gFnPc2	investice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
působí	působit	k5eAaImIp3nS	působit
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
česká	český	k2eAgFnSc1d1	Česká
humanitární	humanitární	k2eAgFnSc1d1	humanitární
organizace	organizace	k1gFnSc1	organizace
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
občany	občan	k1gMnPc4	občan
ČR	ČR	kA	ČR
platí	platit	k5eAaImIp3nS	platit
vízová	vízový	k2eAgFnSc1d1	vízová
povinnost	povinnost	k1gFnSc1	povinnost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kurdistánské	kurdistánský	k2eAgFnPc4d1	Kurdistánská
provincie	provincie	k1gFnPc4	provincie
ale	ale	k8xC	ale
existuje	existovat	k5eAaImIp3nS	existovat
jiný	jiný	k2eAgInSc4d1	jiný
režim	režim	k1gInSc4	režim
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
bezvízový	bezvízový	k2eAgInSc1d1	bezvízový
styk	styk	k1gInSc1	styk
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
10	[number]	k4	10
dnů	den	k1gInPc2	den
a	a	k8xC	a
o	o	k7c6	o
prodloužení	prodloužení	k1gNnSc6	prodloužení
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
žádat	žádat	k5eAaImF	žádat
na	na	k7c6	na
místní	místní	k2eAgFnSc6d1	místní
policejní	policejní	k2eAgFnSc6d1	policejní
stanici	stanice	k1gFnSc6	stanice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obrana	obrana	k1gFnSc1	obrana
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
===	===	k?	===
</s>
</p>
<p>
<s>
Irácká	irácký	k2eAgFnSc1d1	irácká
policie	policie	k1gFnSc1	policie
působící	působící	k2eAgFnSc1d1	působící
pod	pod	k7c7	pod
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
sestává	sestávat	k5eAaImIp3nS	sestávat
zhruba	zhruba	k6eAd1	zhruba
ze	z	k7c2	z
400	[number]	k4	400
000	[number]	k4	000
policistů	policista	k1gMnPc2	policista
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
o	o	k7c4	o
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
mimo	mimo	k6eAd1	mimo
Kurdistánu	Kurdistán	k1gInSc2	Kurdistán
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
kurdská	kurdský	k2eAgFnSc1d1	kurdská
regionální	regionální	k2eAgFnSc1d1	regionální
policie	policie	k1gFnSc1	policie
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgInSc1d1	minimální
věk	věk	k1gInSc1	věk
pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
je	být	k5eAaImIp3nS	být
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Irácké	irácký	k2eAgFnPc1d1	irácká
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
letectvo	letectvo	k1gNnSc1	letectvo
a	a	k8xC	a
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Čítají	čítat	k5eAaImIp3nP	čítat
necelých	celý	k2eNgInPc2d1	necelý
280	[number]	k4	280
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
6000	[number]	k4	6000
v	v	k7c6	v
letectvu	letectvo	k1gNnSc6	letectvo
a	a	k8xC	a
2000	[number]	k4	2000
v	v	k7c6	v
námořnictvu	námořnictvo	k1gNnSc6	námořnictvo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
Irák	Irák	k1gInSc1	Irák
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
295	[number]	k4	295
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
2695	[number]	k4	2695
obrněných	obrněný	k2eAgNnPc2d1	obrněné
a	a	k8xC	a
bojových	bojový	k2eAgNnPc2d1	bojové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
1580	[number]	k4	1580
kusů	kus	k1gInPc2	kus
mobilního	mobilní	k2eAgNnSc2d1	mobilní
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
<g/>
,	,	kIx,	,
224	[number]	k4	224
samohybných	samohybný	k2eAgNnPc2d1	samohybné
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
550	[number]	k4	550
protitankových	protitankový	k2eAgFnPc2d1	protitanková
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
1139	[number]	k4	1139
letadel	letadlo	k1gNnPc2	letadlo
a	a	k8xC	a
15	[number]	k4	15
lodí	loď	k1gFnPc2	loď
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
13	[number]	k4	13
hlídkových	hlídkový	k2eAgFnPc2d1	hlídková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výdaje	výdaj	k1gInPc1	výdaj
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2011	[number]	k4	2011
činily	činit	k5eAaImAgFnP	činit
téměř	téměř	k6eAd1	téměř
18	[number]	k4	18
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
provinciální	provinciální	k2eAgFnSc6d1	provinciální
úrovni	úroveň	k1gFnSc6	úroveň
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
častým	častý	k2eAgFnPc3d1	častá
infiltracím	infiltrace	k1gFnPc3	infiltrace
zdejších	zdejší	k2eAgFnPc2d1	zdejší
milic	milice	k1gFnPc2	milice
do	do	k7c2	do
armádních	armádní	k2eAgFnPc2d1	armádní
řad	řada	k1gFnPc2	řada
a	a	k8xC	a
tyto	tento	k3xDgFnPc1	tento
nepříliš	příliš	k6eNd1	příliš
loajální	loajální	k2eAgFnPc1d1	loajální
skupiny	skupina	k1gFnPc1	skupina
představují	představovat	k5eAaImIp3nP	představovat
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
pro	pro	k7c4	pro
irácký	irácký	k2eAgInSc4d1	irácký
stát	stát	k1gInSc4	stát
problém	problém	k1gInSc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
však	však	k9	však
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
situace	situace	k1gFnSc2	situace
–	–	k?	–
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
spolupráci	spolupráce	k1gFnSc3	spolupráce
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
armádou	armáda	k1gFnSc7	armáda
jsou	být	k5eAaImIp3nP	být
případní	případný	k2eAgMnPc1d1	případný
členové	člen	k1gMnPc1	člen
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
skupin	skupina	k1gFnPc2	skupina
či	či	k8xC	či
jiní	jiný	k2eAgMnPc1d1	jiný
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
pochybnou	pochybný	k2eAgFnSc7d1	pochybná
loajalitou	loajalita	k1gFnSc7	loajalita
z	z	k7c2	z
armády	armáda	k1gFnSc2	armáda
propouštěni	propouštěn	k2eAgMnPc1d1	propouštěn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
nadace	nadace	k1gFnSc2	nadace
Bertelsmann	Bertelsmanna	k1gFnPc2	Bertelsmanna
politické	politický	k2eAgFnSc2d1	politická
špičky	špička	k1gFnSc2	špička
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
významní	významný	k2eAgMnPc1d1	významný
činitelé	činitel	k1gMnPc1	činitel
irácké	irácký	k2eAgFnSc2d1	irácká
společnosti	společnost	k1gFnSc2	společnost
opakovaně	opakovaně	k6eAd1	opakovaně
uchylují	uchylovat	k5eAaImIp3nP	uchylovat
k	k	k7c3	k
navazování	navazování	k1gNnSc3	navazování
užších	úzký	k2eAgInPc2d2	užší
kontaktů	kontakt	k1gInPc2	kontakt
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
nad	nad	k7c7	nad
vybranými	vybraný	k2eAgMnPc7d1	vybraný
jedinci	jedinec	k1gMnPc7	jedinec
drží	držet	k5eAaImIp3nS	držet
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
ruku	ruka	k1gFnSc4	ruka
či	či	k8xC	či
jim	on	k3xPp3gMnPc3	on
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
výhodné	výhodný	k2eAgFnPc1d1	výhodná
funkce	funkce	k1gFnPc1	funkce
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
započala	započnout	k5eAaPmAgFnS	započnout
Severoatlantická	severoatlantický	k2eAgFnSc1d1	Severoatlantická
aliance	aliance	k1gFnSc1	aliance
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
irácké	irácký	k2eAgFnSc2d1	irácká
vlády	vláda	k1gFnSc2	vláda
výcvikovou	výcvikový	k2eAgFnSc4d1	výcviková
misi	mise	k1gFnSc4	mise
s	s	k7c7	s
názvem	název	k1gInSc7	název
NATO	NATO	kA	NATO
Training	Training	k1gInSc1	Training
Mission	Mission	k1gInSc1	Mission
Iraq	Iraq	k1gFnSc1	Iraq
(	(	kIx(	(
<g/>
NTM-I	NTM-I	k1gFnSc1	NTM-I
<g/>
)	)	kIx)	)
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
přetvoření	přetvoření	k1gNnSc2	přetvoření
a	a	k8xC	a
profesionalizace	profesionalizace	k1gFnSc2	profesionalizace
irácké	irácký	k2eAgFnSc2d1	irácká
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
této	tento	k3xDgFnSc2	tento
mise	mise	k1gFnSc2	mise
je	být	k5eAaImIp3nS	být
poskytnout	poskytnout	k5eAaPmF	poskytnout
účinný	účinný	k2eAgInSc1d1	účinný
obranný	obranný	k2eAgInSc1d1	obranný
sektor	sektor	k1gInSc1	sektor
s	s	k7c7	s
demokratickým	demokratický	k2eAgNnSc7d1	demokratické
vedením	vedení	k1gNnSc7	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
výcvik	výcvik	k1gInSc4	výcvik
výše	vysoce	k6eAd2	vysoce
postavených	postavený	k2eAgMnPc2d1	postavený
armádních	armádní	k2eAgMnPc2d1	armádní
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
však	však	k9	však
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
na	na	k7c4	na
výcvik	výcvik	k1gInSc4	výcvik
policie	policie	k1gFnSc2	policie
a	a	k8xC	a
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2008	[number]	k4	2008
na	na	k7c4	na
výcvik	výcvik	k1gInSc4	výcvik
dalších	další	k2eAgFnPc2d1	další
složek	složka	k1gFnPc2	složka
jako	jako	k8xC	jako
např.	např.	kA	např.
loďstva	loďstvo	k1gNnSc2	loďstvo
a	a	k8xC	a
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
operace	operace	k1gFnPc4	operace
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
23	[number]	k4	23
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
přispěly	přispět	k5eAaPmAgInP	přispět
celkově	celkově	k6eAd1	celkově
částkou	částka	k1gFnSc7	částka
asi	asi	k9	asi
250	[number]	k4	250
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
loďstva	loďstvo	k1gNnSc2	loďstvo
a	a	k8xC	a
letectva	letectvo	k1gNnSc2	letectvo
je	být	k5eAaImIp3nS	být
irácká	irácký	k2eAgFnSc1d1	irácká
armáda	armáda	k1gFnSc1	armáda
nadále	nadále	k6eAd1	nadále
na	na	k7c6	na
americké	americký	k2eAgFnSc6d1	americká
podpoře	podpora	k1gFnSc6	podpora
závislá	závislý	k2eAgFnSc1d1	závislá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stála	stát	k5eAaImAgFnS	stát
ekonomika	ekonomika	k1gFnSc1	ekonomika
Iráku	Irák	k1gInSc2	Irák
na	na	k7c6	na
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
ale	ale	k9	ale
nastala	nastat	k5eAaPmAgFnS	nastat
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
revoluce	revoluce	k1gFnSc1	revoluce
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
Irák	Irák	k1gInSc1	Irák
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
systém	systém	k1gInSc4	systém
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
ekonomiky	ekonomika	k1gFnSc2	ekonomika
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
druhým	druhý	k4xOgInSc7	druhý
nejbohatším	bohatý	k2eAgInSc7d3	nejbohatší
státem	stát	k1gInSc7	stát
arabského	arabský	k2eAgInSc2d1	arabský
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
po	po	k7c6	po
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
)	)	kIx)	)
a	a	k8xC	a
třetím	třetí	k4xOgMnSc7	třetí
nejbohatším	bohatý	k2eAgMnSc7d3	nejbohatší
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Irácké	irácký	k2eAgNnSc4d1	irácké
hospodářství	hospodářství	k1gNnSc4	hospodářství
poškodila	poškodit	k5eAaPmAgFnS	poškodit
nejdříve	dříve	k6eAd3	dříve
irácko-íránská	irácko-íránský	k2eAgFnSc1d1	irácko-íránská
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
pak	pak	k6eAd1	pak
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
a	a	k8xC	a
embargo	embargo	k1gNnSc1	embargo
uvalené	uvalený	k2eAgFnSc2d1	uvalená
OSN	OSN	kA	OSN
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
ránou	rána	k1gFnSc7	rána
pro	pro	k7c4	pro
hospodářství	hospodářství	k1gNnSc4	hospodářství
byla	být	k5eAaImAgFnS	být
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
začínající	začínající	k2eAgFnSc1d1	začínající
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
<g/>
Irák	Irák	k1gInSc1	Irák
se	se	k3xPyFc4	se
od	od	k7c2	od
ustanovení	ustanovení	k1gNnSc2	ustanovení
nového	nový	k2eAgInSc2d1	nový
režimu	režim	k1gInSc2	režim
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
snaží	snažit	k5eAaImIp3nP	snažit
podporovat	podporovat	k5eAaImF	podporovat
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
,	,	kIx,	,
lákat	lákat	k5eAaImF	lákat
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
investory	investor	k1gMnPc4	investor
a	a	k8xC	a
liberalizovat	liberalizovat	k5eAaImF	liberalizovat
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
ale	ale	k8xC	ale
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
stále	stále	k6eAd1	stále
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
uhlovodíkový	uhlovodíkový	k2eAgInSc4d1	uhlovodíkový
sektor	sektor	k1gInSc4	sektor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
hlavní	hlavní	k2eAgFnSc7d1	hlavní
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
těžba	těžba	k1gFnSc1	těžba
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
místního	místní	k2eAgNnSc2d1	místní
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
stát	stát	k5eAaPmF	stát
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
odvětví	odvětví	k1gNnPc4	odvětví
patří	patřit	k5eAaImIp3nS	patřit
výroba	výroba	k1gFnSc1	výroba
chemikálií	chemikálie	k1gFnPc2	chemikálie
<g/>
,	,	kIx,	,
textilu	textil	k1gInSc2	textil
<g/>
,	,	kIx,	,
stavebních	stavební	k2eAgInPc2d1	stavební
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
<g/>
Irácké	irácký	k2eAgFnPc1d1	irácká
zásoby	zásoba	k1gFnPc1	zásoba
ropy	ropa	k1gFnSc2	ropa
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
páté	pátá	k1gFnSc2	pátá
největší	veliký	k2eAgFnSc2d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Prodej	prodej	k1gInSc1	prodej
ropy	ropa	k1gFnSc2	ropa
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
přes	přes	k7c4	přes
90	[number]	k4	90
%	%	kIx~	%
vládních	vládní	k2eAgInPc2d1	vládní
zisků	zisk	k1gInPc2	zisk
a	a	k8xC	a
ropa	ropa	k1gFnSc1	ropa
tvoří	tvořit	k5eAaImIp3nS	tvořit
přes	přes	k7c4	přes
80	[number]	k4	80
%	%	kIx~	%
vývozních	vývozní	k2eAgFnPc2d1	vývozní
komodit	komodita	k1gFnPc2	komodita
<g/>
;	;	kIx,	;
Irák	Irák	k1gInSc1	Irák
je	být	k5eAaImIp3nS	být
11	[number]	k4	11
<g/>
.	.	kIx.	.
stát	stát	k1gInSc1	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
podle	podle	k7c2	podle
exportu	export	k1gInSc2	export
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
denně	denně	k6eAd1	denně
produkuje	produkovat	k5eAaImIp3nS	produkovat
zhruba	zhruba	k6eAd1	zhruba
2,4	[number]	k4	2,4
milionu	milion	k4xCgInSc2	milion
barelů	barel	k1gInPc2	barel
a	a	k8xC	a
exportuje	exportovat	k5eAaBmIp3nS	exportovat
jich	on	k3xPp3gInPc2	on
1,9	[number]	k4	1,9
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Irák	Irák	k1gInSc1	Irák
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
11	[number]	k4	11
<g/>
.	.	kIx.	.
největší	veliký	k2eAgFnSc2d3	veliký
zásoby	zásoba	k1gFnSc2	zásoba
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ropy	ropa	k1gFnSc2	ropa
vyváží	vyvážet	k5eAaImIp3nP	vyvážet
další	další	k2eAgFnPc1d1	další
suroviny	surovina	k1gFnPc1	surovina
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
8	[number]	k4	8
%	%	kIx~	%
exportu	export	k1gInSc2	export
<g/>
)	)	kIx)	)
a	a	k8xC	a
živočišné	živočišný	k2eAgInPc4d1	živočišný
produkty	produkt	k1gInPc4	produkt
(	(	kIx(	(
<g/>
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
exportu	export	k1gInSc2	export
putuje	putovat	k5eAaImIp3nS	putovat
do	do	k7c2	do
USA	USA	kA	USA
(	(	kIx(	(
<g/>
25,2	[number]	k4	25,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
(	(	kIx(	(
<g/>
15,1	[number]	k4	15,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
(	(	kIx(	(
<g/>
9,9	[number]	k4	9,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
9,2	[number]	k4	9,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Čínské	čínský	k2eAgFnPc1d1	čínská
lidové	lidový	k2eAgFnPc1d1	lidová
republiky	republika	k1gFnPc1	republika
(	(	kIx(	(
<g/>
8,5	[number]	k4	8,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Irák	Irák	k1gInSc1	Irák
nejvíce	nejvíce	k6eAd1	nejvíce
importuje	importovat	k5eAaBmIp3nS	importovat
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
(	(	kIx(	(
<g/>
23,8	[number]	k4	23,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sýrie	Sýrie	k1gFnSc1	Sýrie
(	(	kIx(	(
<g/>
16,6	[number]	k4	16,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čínské	čínský	k2eAgFnPc1d1	čínská
lidové	lidový	k2eAgFnPc1d1	lidová
republiky	republika	k1gFnPc1	republika
(	(	kIx(	(
<g/>
8,5	[number]	k4	8,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
USA	USA	kA	USA
(	(	kIx(	(
<g/>
8,3	[number]	k4	8,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejčastěji	často	k6eAd3	často
dovážené	dovážený	k2eAgNnSc4d1	dovážené
zboží	zboží	k1gNnSc4	zboží
patří	patřit	k5eAaImIp3nS	patřit
potraviny	potravina	k1gFnPc4	potravina
<g/>
,	,	kIx,	,
léky	lék	k1gInPc4	lék
a	a	k8xC	a
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
výrobky	výrobek	k1gInPc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
obchodním	obchodní	k2eAgMnSc7d1	obchodní
partnerem	partner	k1gMnSc7	partner
Iráku	Irák	k1gInSc2	Irák
jsou	být	k5eAaImIp3nP	být
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgNnSc1d1	následované
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
.	.	kIx.	.
</s>
<s>
Vysokému	vysoký	k2eAgInSc3d1	vysoký
zájmu	zájem	k1gInSc3	zájem
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
investorů	investor	k1gMnPc2	investor
překážejí	překážet	k5eAaImIp3nP	překážet
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
nákupem	nákup	k1gInSc7	nákup
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
činil	činit	k5eAaImAgInS	činit
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
<g/>
)	)	kIx)	)
Iráku	Irák	k1gInSc2	Irák
v	v	k7c6	v
paritě	parita	k1gFnSc6	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
113,4	[number]	k4	113,4
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Meziroční	meziroční	k2eAgInSc1d1	meziroční
růst	růst	k1gInSc1	růst
HDP	HDP	kA	HDP
za	za	k7c4	za
roky	rok	k1gInPc4	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
a	a	k8xC	a
2010	[number]	k4	2010
činil	činit	k5eAaImAgInS	činit
9,5	[number]	k4	9,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
4,2	[number]	k4	4,2
%	%	kIx~	%
a	a	k8xC	a
0,8	[number]	k4	0,8
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
v	v	k7c6	v
paritě	parita	k1gFnSc6	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
činilo	činit	k5eAaImAgNnS	činit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zhruba	zhruba	k6eAd1	zhruba
3800	[number]	k4	3800
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
Irák	Irák	k1gInSc1	Irák
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
160	[number]	k4	160
<g/>
.	.	kIx.	.
státem	stát	k1gInSc7	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
%	%	kIx~	%
<g/>
,	,	kIx,	,
z	z	k7c2	z
pracujícího	pracující	k2eAgNnSc2d1	pracující
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
21	[number]	k4	21
%	%	kIx~	%
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
19	[number]	k4	19
%	%	kIx~	%
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
60	[number]	k4	60
%	%	kIx~	%
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
žen	žena	k1gFnPc2	žena
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
práce	práce	k1gFnSc2	práce
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
17	[number]	k4	17
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největším	veliký	k2eAgMnPc3d3	veliký
zaměstnavatelům	zaměstnavatel	k1gMnPc3	zaměstnavatel
patří	patřit	k5eAaImIp3nS	patřit
státní	státní	k2eAgInSc1d1	státní
sektor	sektor	k1gInSc1	sektor
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
kurdistánských	kurdistánský	k2eAgFnPc6d1	Kurdistánská
provinciích	provincie	k1gFnPc6	provincie
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
pracuje	pracovat	k5eAaImIp3nS	pracovat
až	až	k9	až
50	[number]	k4	50
%	%	kIx~	%
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
<g/>
Služby	služba	k1gFnPc1	služba
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
nejnižší	nízký	k2eAgFnSc6d3	nejnižší
úrovni	úroveň	k1gFnSc6	úroveň
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
odvětví	odvětví	k1gNnPc2	odvětví
irácké	irácký	k2eAgFnSc2d1	irácká
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zlepšování	zlepšování	k1gNnSc3	zlepšování
ve	v	k7c6	v
finančních	finanční	k2eAgFnPc6d1	finanční
službách	služba	k1gFnPc6	služba
<g/>
,	,	kIx,	,
pojišťovnictví	pojišťovnictví	k1gNnSc6	pojišťovnictví
<g/>
,	,	kIx,	,
zajišťování	zajišťování	k1gNnSc6	zajišťování
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
telekomunikacích	telekomunikace	k1gFnPc6	telekomunikace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zdravotnictví	zdravotnictví	k1gNnSc1	zdravotnictví
<g/>
,	,	kIx,	,
zásobování	zásobování	k1gNnSc1	zásobování
pitnou	pitný	k2eAgFnSc7d1	pitná
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
zásobování	zásobování	k1gNnSc4	zásobování
elektřinou	elektřina	k1gFnSc7	elektřina
a	a	k8xC	a
školství	školství	k1gNnSc2	školství
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
špatném	špatný	k2eAgInSc6d1	špatný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
péče	péče	k1gFnSc1	péče
a	a	k8xC	a
ošetření	ošetření	k1gNnSc1	ošetření
je	být	k5eAaImIp3nS	být
poskytováno	poskytovat	k5eAaImNgNnS	poskytovat
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
bezpečnostní	bezpečnostní	k2eAgFnSc3d1	bezpečnostní
situaci	situace	k1gFnSc3	situace
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
neuskutečňuje	uskutečňovat	k5eNaImIp3nS	uskutečňovat
téměř	téměř	k6eAd1	téměř
žádný	žádný	k3yNgInSc4	žádný
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgNnSc7d1	významné
odvětvím	odvětví	k1gNnSc7	odvětví
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
větším	veliký	k2eAgInSc7d2	veliký
podílem	podíl	k1gInSc7	podíl
na	na	k7c6	na
HDP	HDP	kA	HDP
je	být	k5eAaImIp3nS	být
stavebnictví	stavebnictví	k1gNnSc1	stavebnictví
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
zaznamenával	zaznamenávat	k5eAaImAgInS	zaznamenávat
stát	stát	k1gInSc1	stát
velkou	velký	k2eAgFnSc4d1	velká
míru	míra	k1gFnSc4	míra
inflace	inflace	k1gFnSc2	inflace
(	(	kIx(	(
<g/>
běžně	běžně	k6eAd1	běžně
přes	přes	k7c4	přes
30	[number]	k4	30
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
index	index	k1gInSc1	index
spotřebitelských	spotřebitelský	k2eAgFnPc2d1	spotřebitelská
cen	cena	k1gFnPc2	cena
již	již	k6eAd1	již
pouze	pouze	k6eAd1	pouze
2,4	[number]	k4	2,4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
irácký	irácký	k2eAgInSc4d1	irácký
dinár	dinár	k1gInSc4	dinár
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
směnný	směnný	k2eAgInSc1d1	směnný
kurz	kurz	k1gInSc1	kurz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
1170	[number]	k4	1170
dinárů	dinár	k1gInPc2	dinár
za	za	k7c4	za
dolar	dolar	k1gInSc4	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zemědělství	zemědělství	k1gNnSc1	zemědělství
===	===	k?	===
</s>
</p>
<p>
<s>
Půda	půda	k1gFnSc1	půda
patří	patřit	k5eAaImIp3nS	patřit
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
soukromým	soukromý	k2eAgMnPc3d1	soukromý
vlastníkům	vlastník	k1gMnPc3	vlastník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
systém	systém	k1gInSc1	systém
zavodňování	zavodňování	k1gNnSc2	zavodňování
a	a	k8xC	a
využívají	využívat	k5eAaPmIp3nP	využívat
se	se	k3xPyFc4	se
zemědělské	zemědělský	k2eAgInPc1d1	zemědělský
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
hnojiva	hnojivo	k1gNnPc1	hnojivo
a	a	k8xC	a
pesticidy	pesticid	k1gInPc1	pesticid
<g/>
,	,	kIx,	,
k	k	k7c3	k
výraznému	výrazný	k2eAgInSc3d1	výrazný
vývoji	vývoj	k1gInSc3	vývoj
v	v	k7c6	v
použití	použití	k1gNnSc6	použití
moderních	moderní	k2eAgFnPc2d1	moderní
technologií	technologie	k1gFnPc2	technologie
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
dochází	docházet	k5eAaImIp3nS	docházet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
zemědělství	zemědělství	k1gNnSc2	zemědělství
na	na	k7c4	na
HDP	HDP	kA	HDP
činí	činit	k5eAaImIp3nS	činit
necelých	celý	k2eNgInPc2d1	necelý
10	[number]	k4	10
%	%	kIx~	%
<g/>
,	,	kIx,	,
na	na	k7c6	na
zaměstnanosti	zaměstnanost	k1gFnSc6	zaměstnanost
pak	pak	k6eAd1	pak
něco	něco	k3yInSc1	něco
málo	málo	k6eAd1	málo
přes	přes	k7c4	přes
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
populace	populace	k1gFnSc2	populace
jsou	být	k5eAaImIp3nP	být
závislé	závislý	k2eAgFnPc1d1	závislá
na	na	k7c6	na
potravinách	potravina	k1gFnPc6	potravina
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
dováží	dovážet	k5eAaImIp3nP	dovážet
téměř	téměř	k6eAd1	téměř
dva	dva	k4xCgInPc4	dva
miliony	milion	k4xCgInPc4	milion
tun	tuna	k1gFnPc2	tuna
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
Irák	Irák	k1gInSc1	Irák
vyprodukoval	vyprodukovat	k5eAaPmAgInS	vyprodukovat
1,7	[number]	k4	1,7
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
pšenice	pšenice	k1gFnSc2	pšenice
a	a	k8xC	a
0,5	[number]	k4	0,5
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
ječmene	ječmen	k1gInSc2	ječmen
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
irácko-íránské	irácko-íránský	k2eAgFnSc2d1	irácko-íránská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
byl	být	k5eAaImAgInS	být
Irák	Irák	k1gInSc1	Irák
největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
datlí	datle	k1gFnPc2	datle
na	na	k7c6	na
světě	svět	k1gInSc6	svět
s	s	k7c7	s
596	[number]	k4	596
920	[number]	k4	920
tunami	tunami	k1gNnPc2	tunami
ročně	ročně	k6eAd1	ročně
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
konfliktům	konflikt	k1gInPc3	konflikt
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
propadl	propadnout	k5eAaPmAgMnS	propadnout
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sedmým	sedmý	k4xOgMnSc7	sedmý
největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
s	s	k7c7	s
507	[number]	k4	507
002	[number]	k4	002
tunami	tunami	k1gNnPc2	tunami
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zemědělství	zemědělství	k1gNnSc2	zemědělství
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c4	o
rehabilitaci	rehabilitace	k1gFnSc4	rehabilitace
tohoto	tento	k3xDgNnSc2	tento
odvětví	odvětví	k1gNnSc2	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýdělečnější	Nejvýdělečný	k2eAgFnSc7d2	Nejvýdělečný
zemědělskou	zemědělský	k2eAgFnSc7d1	zemědělská
plodinou	plodina	k1gFnSc7	plodina
je	být	k5eAaImIp3nS	být
rajče	rajče	k1gNnSc1	rajče
jedlé	jedlý	k2eAgNnSc1d1	jedlé
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
Irák	Irák	k1gInSc1	Irák
vyprodukoval	vyprodukovat	k5eAaPmAgInS	vyprodukovat
913	[number]	k4	913
493	[number]	k4	493
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
setý	setý	k2eAgInSc1d1	setý
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnSc1	rýže
a	a	k8xC	a
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
živočišné	živočišný	k2eAgFnSc2d1	živočišná
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
Irák	Irák	k1gInSc1	Irák
vyprodukoval	vyprodukovat	k5eAaPmAgInS	vyprodukovat
47	[number]	k4	47
493	[number]	k4	493
tun	tuna	k1gFnPc2	tuna
hovězího	hovězí	k2eAgNnSc2d1	hovězí
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
46	[number]	k4	46
984	[number]	k4	984
tun	tuna	k1gFnPc2	tuna
jehněčího	jehněčí	k2eAgNnSc2d1	jehněčí
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
48	[number]	k4	48
312	[number]	k4	312
tun	tuna	k1gFnPc2	tuna
kuřecího	kuřecí	k2eAgNnSc2d1	kuřecí
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
915,6	[number]	k4	915,6
milionů	milion	k4xCgInPc2	milion
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
irácká	irácký	k2eAgFnSc1d1	irácká
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
válkám	válka	k1gFnPc3	válka
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
poničena	poničit	k5eAaPmNgFnS	poničit
a	a	k8xC	a
Irák	Irák	k1gInSc1	Irák
se	se	k3xPyFc4	se
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
rozvoj	rozvoj	k1gInSc4	rozvoj
nesoustřeďoval	soustřeďovat	k5eNaImAgMnS	soustřeďovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
nedostatečné	dostatečný	k2eNgNnSc4d1	nedostatečné
zásobování	zásobování	k1gNnSc4	zásobování
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc4	nedostatek
zdravotních	zdravotní	k2eAgNnPc2d1	zdravotní
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
špatně	špatně	k6eAd1	špatně
fungující	fungující	k2eAgFnSc1d1	fungující
veřejná	veřejný	k2eAgFnSc1d1	veřejná
rozvodná	rozvodný	k2eAgFnSc1d1	rozvodná
síť	síť	k1gFnSc1	síť
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
např.	např.	kA	např.
v	v	k7c6	v
Bagdádu	Bagdád	k1gInSc6	Bagdád
funguje	fungovat	k5eAaImIp3nS	fungovat
průměrně	průměrně	k6eAd1	průměrně
pouze	pouze	k6eAd1	pouze
5	[number]	k4	5
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
rozsah	rozsah	k1gInSc1	rozsah
silnic	silnice	k1gFnPc2	silnice
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
44	[number]	k4	44
900	[number]	k4	900
km	km	kA	km
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
zhruba	zhruba	k6eAd1	zhruba
37	[number]	k4	37
900	[number]	k4	900
km	km	kA	km
je	být	k5eAaImIp3nS	být
zpevněných	zpevněný	k2eAgFnPc2d1	zpevněná
<g/>
;	;	kIx,	;
počet	počet	k1gInSc1	počet
automobilů	automobil	k1gInPc2	automobil
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
<g/>
.	.	kIx.	.
</s>
<s>
Irák	Irák	k1gInSc1	Irák
má	mít	k5eAaImIp3nS	mít
104	[number]	k4	104
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
75	[number]	k4	75
se	s	k7c7	s
zpevněnou	zpevněný	k2eAgFnSc7d1	zpevněná
dráhou	dráha	k1gFnSc7	dráha
<g/>
,	,	kIx,	,
a	a	k8xC	a
21	[number]	k4	21
heliportů	heliport	k1gInPc2	heliport
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
počtu	počet	k1gInSc2	počet
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
letišť	letiště	k1gNnPc2	letiště
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
–	–	k?	–
v	v	k7c6	v
Bagdádu	Bagdád	k1gInSc6	Bagdád
<g/>
,	,	kIx,	,
Basře	Basř	k1gInSc2	Basř
<g/>
,	,	kIx,	,
Arbílu	Arbíl	k1gInSc2	Arbíl
<g/>
,	,	kIx,	,
Sulajmáníje	Sulajmáníj	k1gInSc2	Sulajmáníj
a	a	k8xC	a
Nadžafu	Nadžaf	k1gInSc2	Nadžaf
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
leteckou	letecký	k2eAgFnSc7d1	letecká
společností	společnost	k1gFnSc7	společnost
je	být	k5eAaImIp3nS	být
Iraqi	Iraqi	k1gNnSc1	Iraqi
Airways	Airwaysa	k1gFnPc2	Airwaysa
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
má	mít	k5eAaImIp3nS	mít
dále	daleko	k6eAd2	daleko
2272	[number]	k4	2272
km	km	kA	km
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
často	často	k6eAd1	často
nefunkční	funkční	k2eNgMnSc1d1	nefunkční
<g/>
.	.	kIx.	.
</s>
<s>
Modernizace	modernizace	k1gFnSc1	modernizace
železnic	železnice	k1gFnPc2	železnice
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
naplánována	naplánovat	k5eAaBmNgFnS	naplánovat
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
a	a	k8xC	a
Irák	Irák	k1gInSc1	Irák
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
investuje	investovat	k5eAaBmIp3nS	investovat
<g/>
.	.	kIx.	.
</s>
<s>
Říční	říční	k2eAgFnSc4d1	říční
dopravu	doprava	k1gFnSc4	doprava
využívají	využívat	k5eAaPmIp3nP	využívat
jen	jen	k9	jen
malí	malý	k2eAgMnPc1d1	malý
podnikatelé	podnikatel	k1gMnPc1	podnikatel
a	a	k8xC	a
rybáři	rybář	k1gMnPc1	rybář
<g/>
.	.	kIx.	.
</s>
<s>
Irák	Irák	k1gInSc1	Irák
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
obchodní	obchodní	k2eAgFnSc4d1	obchodní
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zbylé	zbylý	k2eAgFnPc1d1	zbylá
byly	být	k5eAaImAgFnP	být
během	během	k7c2	během
válek	válka	k1gFnPc2	válka
zničeny	zničen	k2eAgFnPc4d1	zničena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
vybudovat	vybudovat	k5eAaPmF	vybudovat
obchodní	obchodní	k2eAgFnSc4d1	obchodní
flotilu	flotila	k1gFnSc4	flotila
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
září	září	k1gNnSc2	září
2011	[number]	k4	2011
oznámil	oznámit	k5eAaPmAgMnS	oznámit
koupi	koupě	k1gFnSc4	koupě
tří	tři	k4xCgFnPc2	tři
nákladních	nákladní	k2eAgFnPc2d1	nákladní
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
telekomunikací	telekomunikace	k1gFnPc2	telekomunikace
se	se	k3xPyFc4	se
Irák	Irák	k1gInSc1	Irák
rychle	rychle	k6eAd1	rychle
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
pevné	pevný	k2eAgFnSc3d1	pevná
lince	linka	k1gFnSc3	linka
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
počet	počet	k1gInSc1	počet
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
téměř	téměř	k6eAd1	téměř
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
mělo	mít	k5eAaImAgNnS	mít
asi	asi	k9	asi
325	[number]	k4	325
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
operuje	operovat	k5eAaImIp3nS	operovat
zhruba	zhruba	k6eAd1	zhruba
80	[number]	k4	80
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
20	[number]	k4	20
televizních	televizní	k2eAgFnPc2d1	televizní
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
dostupné	dostupný	k2eAgNnSc1d1	dostupné
je	být	k5eAaImIp3nS	být
satelitní	satelitní	k2eAgNnSc1d1	satelitní
zahraniční	zahraniční	k2eAgNnSc1d1	zahraniční
vysílání	vysílání	k1gNnSc1	vysílání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
měnového	měnový	k2eAgInSc2d1	měnový
fondu	fond	k1gInSc2	fond
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
32	[number]	k4	32
847	[number]	k4	847
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
CIA	CIA	kA	CIA
30	[number]	k4	30
399	[number]	k4	399
572	[number]	k4	572
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
75	[number]	k4	75
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
Arabové	Arab	k1gMnPc1	Arab
a	a	k8xC	a
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
Kurdové	Kurd	k1gMnPc1	Kurd
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
skupinami	skupina	k1gFnPc7	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
Asyřané	Asyřan	k1gMnPc1	Asyřan
a	a	k8xC	a
iráčtí	irácký	k2eAgMnPc1d1	irácký
Turkméni	Turkmén	k1gMnPc1	Turkmén
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Iráku	Irák	k1gInSc6	Irák
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
žilo	žít	k5eAaImAgNnS	žít
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
000	[number]	k4	000
bažinných	bažinný	k2eAgMnPc2d1	bažinný
Arabů	Arab	k1gMnPc2	Arab
(	(	kIx(	(
<g/>
Madánů	Madán	k1gMnPc2	Madán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
žije	žít	k5eAaImIp3nS	žít
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInSc1	tisíc
Čerkesů	Čerkes	k1gMnPc2	Čerkes
<g/>
,	,	kIx,	,
Arménů	Armén	k1gMnPc2	Armén
a	a	k8xC	a
Čečenců	Čečenec	k1gMnPc2	Čečenec
<g/>
.	.	kIx.	.
</s>
<s>
Úředními	úřední	k2eAgMnPc7d1	úřední
jazyky	jazyk	k1gMnPc7	jazyk
jsou	být	k5eAaImIp3nP	být
arabština	arabština	k1gFnSc1	arabština
a	a	k8xC	a
kurdština	kurdština	k1gFnSc1	kurdština
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
častými	častý	k2eAgMnPc7d1	častý
jazyky	jazyk	k1gMnPc7	jazyk
jsou	být	k5eAaImIp3nP	být
syrština	syrština	k1gFnSc1	syrština
a	a	k8xC	a
turkmenština	turkmenština	k1gFnSc1	turkmenština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
regionech	region	k1gInPc6	region
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
podle	podle	k7c2	podle
irácké	irácký	k2eAgFnSc2d1	irácká
ústavy	ústava	k1gFnSc2	ústava
za	za	k7c4	za
úřední	úřední	k2eAgInPc4d1	úřední
prohlášeny	prohlášen	k2eAgInPc4d1	prohlášen
i	i	k8xC	i
další	další	k2eAgInPc4d1	další
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
38	[number]	k4	38
%	%	kIx~	%
Iráčanů	Iráčan	k1gMnPc2	Iráčan
mladších	mladý	k2eAgMnPc2d2	mladší
15	[number]	k4	15
let	let	k1gInSc4	let
a	a	k8xC	a
mediánový	mediánový	k2eAgInSc4d1	mediánový
věk	věk	k1gInSc4	věk
je	být	k5eAaImIp3nS	být
20,9	[number]	k4	20,9
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInSc1d1	roční
přirozený	přirozený	k2eAgInSc1d1	přirozený
přírůstek	přírůstek	k1gInSc1	přírůstek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
činí	činit	k5eAaImIp3nS	činit
2,4	[number]	k4	2,4
%	%	kIx~	%
a	a	k8xC	a
úhrnná	úhrnný	k2eAgFnSc1d1	úhrnná
plodnost	plodnost	k1gFnSc1	plodnost
je	být	k5eAaImIp3nS	být
3,67	[number]	k4	3,67
dítěte	dítě	k1gNnSc2	dítě
na	na	k7c4	na
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Porodnost	porodnost	k1gFnSc1	porodnost
je	být	k5eAaImIp3nS	být
28,8	[number]	k4	28,8
narozených	narozený	k2eAgMnPc2d1	narozený
na	na	k7c4	na
1000	[number]	k4	1000
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
úmrtnost	úmrtnost	k1gFnSc4	úmrtnost
5,4	[number]	k4	5,4
zemřelých	zemřelý	k1gMnPc2	zemřelý
na	na	k7c4	na
1000	[number]	k4	1000
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
70	[number]	k4	70
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
choroby	choroba	k1gFnPc4	choroba
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
průjem	průjem	k1gInSc1	průjem
<g/>
,	,	kIx,	,
hepatitida	hepatitida	k1gFnSc1	hepatitida
A	a	k9	a
a	a	k8xC	a
břišní	břišní	k2eAgInSc4d1	břišní
tyfus	tyfus	k1gInSc4	tyfus
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
66	[number]	k4	66
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
největší	veliký	k2eAgFnSc7d3	veliký
jsou	být	k5eAaImIp3nP	být
Bagdád	Bagdád	k1gInSc1	Bagdád
<g/>
,	,	kIx,	,
Basra	Basra	k1gFnSc1	Basra
<g/>
,	,	kIx,	,
Mosul	Mosul	k1gInSc1	Mosul
<g/>
,	,	kIx,	,
Arbíl	Arbíl	k1gInSc1	Arbíl
a	a	k8xC	a
Sulajmáníja	Sulajmáníja	k1gFnSc1	Sulajmáníja
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
.	.	kIx.	.
91	[number]	k4	91
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
ve	v	k7c6	v
městech	město	k1gNnPc6	město
a	a	k8xC	a
55	[number]	k4	55
%	%	kIx~	%
na	na	k7c6	na
vesnici	vesnice	k1gFnSc6	vesnice
má	mít	k5eAaImIp3nS	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
pitné	pitný	k2eAgFnSc3d1	pitná
vodě	voda	k1gFnSc3	voda
<g/>
,	,	kIx,	,
73	[number]	k4	73
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
má	mít	k5eAaImIp3nS	mít
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
zdravotnickým	zdravotnický	k2eAgNnPc3d1	zdravotnické
zařízením	zařízení	k1gNnPc3	zařízení
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
ale	ale	k9	ale
často	často	k6eAd1	často
trpí	trpět	k5eAaImIp3nP	trpět
nedostatkem	nedostatek	k1gInSc7	nedostatek
kvalifikovaného	kvalifikovaný	k2eAgInSc2d1	kvalifikovaný
personálu	personál	k1gInSc2	personál
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
dodávkách	dodávka	k1gFnPc6	dodávka
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
od	od	k7c2	od
UNICEFu	UNICEFus	k1gInSc2	UNICEFus
závislých	závislý	k2eAgMnPc2d1	závislý
přes	přes	k7c4	přes
200	[number]	k4	200
000	[number]	k4	000
Iráčanů	Iráčan	k1gMnPc2	Iráčan
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
mají	mít	k5eAaImIp3nP	mít
ženy	žena	k1gFnPc1	žena
stejné	stejný	k2eAgNnSc1d1	stejné
právo	právo	k1gNnSc1	právo
jako	jako	k8xS	jako
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
zákonech	zákon	k1gInPc6	zákon
i	i	k8xC	i
praxi	praxe	k1gFnSc6	praxe
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
diskriminovány	diskriminován	k2eAgFnPc1d1	diskriminována
<g/>
,	,	kIx,	,
např.	např.	kA	např.
nuceny	nucen	k2eAgInPc1d1	nucen
k	k	k7c3	k
dodržování	dodržování	k1gNnSc3	dodržování
přísného	přísný	k2eAgInSc2d1	přísný
stylu	styl	k1gInSc2	styl
oblékání	oblékání	k1gNnSc2	oblékání
nebo	nebo	k8xC	nebo
k	k	k7c3	k
předčasným	předčasný	k2eAgNnPc3d1	předčasné
manželstvím	manželství	k1gNnPc3	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
i	i	k9	i
domácí	domácí	k2eAgNnSc4d1	domácí
násilí	násilí	k1gNnSc4	násilí
na	na	k7c6	na
ženách	žena	k1gFnPc6	žena
<g/>
,	,	kIx,	,
vraždy	vražda	k1gFnPc1	vražda
ze	z	k7c2	z
cti	čest	k1gFnSc2	čest
a	a	k8xC	a
ženská	ženský	k2eAgFnSc1d1	ženská
obřízka	obřízka	k1gFnSc1	obřízka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
kurdském	kurdský	k2eAgInSc6d1	kurdský
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
Iráčanů	Iráčan	k1gMnPc2	Iráčan
(	(	kIx(	(
<g/>
97	[number]	k4	97
%	%	kIx~	%
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
muslimského	muslimský	k2eAgNnSc2d1	muslimské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
65	[number]	k4	65
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nS	tvořit
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
ité	ité	k?	ité
a	a	k8xC	a
32	[number]	k4	32
<g/>
–	–	k?	–
<g/>
37	[number]	k4	37
%	%	kIx~	%
sunnité	sunnita	k1gMnPc1	sunnita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgNnPc1	dva
posvátná	posvátný	k2eAgNnPc1d1	posvátné
města	město	k1gNnPc1	město
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itů	itů	k?	itů
<g/>
,	,	kIx,	,
Nadžaf	Nadžaf	k1gInSc1	Nadžaf
a	a	k8xC	a
Karbalá	Karbalý	k2eAgFnSc1d1	Karbalá
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
Jezídové	Jezídový	k2eAgFnPc1d1	Jezídový
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
Kurdové	Kurd	k1gMnPc1	Kurd
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc4	jejichž
náboženství	náboženství	k1gNnSc1	náboženství
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
prvky	prvek	k1gInPc7	prvek
několika	několik	k4yIc2	několik
jiných	jiný	k2eAgNnPc2d1	jiné
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
křesťanství	křesťanství	k1gNnSc1	křesťanství
a	a	k8xC	a
islám	islám	k1gInSc1	islám
<g/>
.	.	kIx.	.
<g/>
Ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
a	a	k8xC	a
ostatním	ostatní	k2eAgNnSc7d1	ostatní
náboženstvím	náboženství	k1gNnSc7	náboženství
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
3	[number]	k4	3
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
křesťanské	křesťanský	k2eAgFnPc4d1	křesťanská
církve	církev	k1gFnPc4	církev
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
patří	patřit	k5eAaImIp3nS	patřit
Chaldejská	chaldejský	k2eAgFnSc1d1	Chaldejská
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
Syrská	syrský	k2eAgFnSc1d1	Syrská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
a	a	k8xC	a
Asyrská	asyrský	k2eAgFnSc1d1	Asyrská
východní	východní	k2eAgFnSc1d1	východní
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
iráckém	irácký	k2eAgNnSc6d1	irácké
území	území	k1gNnSc6	území
žijí	žít	k5eAaImIp3nP	žít
křesťané	křesťan	k1gMnPc1	křesťan
asi	asi	k9	asi
2000	[number]	k4	2000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
koncentrují	koncentrovat	k5eAaBmIp3nP	koncentrovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Bagdádu	Bagdád	k1gInSc6	Bagdád
<g/>
,	,	kIx,	,
Basře	Basra	k1gFnSc6	Basra
<g/>
,	,	kIx,	,
Kirkúku	Kirkúko	k1gNnSc6	Kirkúko
<g/>
,	,	kIx,	,
Arbílu	Arbílo	k1gNnSc6	Arbílo
a	a	k8xC	a
Mosulu	Mosul	k1gInSc6	Mosul
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
okolo	okolo	k7c2	okolo
800	[number]	k4	800
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
po	po	k7c6	po
sesazení	sesazení	k1gNnSc6	sesazení
Saddáma	Saddám	k1gMnSc4	Saddám
Husajna	Husajn	k1gMnSc4	Husajn
se	se	k3xPyFc4	se
ale	ale	k9	ale
kvůli	kvůli	k7c3	kvůli
nepřátelskému	přátelský	k2eNgNnSc3d1	nepřátelské
chování	chování	k1gNnSc3	chování
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
muslimů	muslim	k1gMnPc2	muslim
asi	asi	k9	asi
polovina	polovina	k1gFnSc1	polovina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
odstěhovala	odstěhovat	k5eAaPmAgFnS	odstěhovat
do	do	k7c2	do
Sýrie	Sýrie	k1gFnSc2	Sýrie
a	a	k8xC	a
menší	malý	k2eAgInSc1d2	menší
počet	počet	k1gInSc1	počet
do	do	k7c2	do
Jordánska	Jordánsko	k1gNnSc2	Jordánsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bagdádu	Bagdád	k1gInSc6	Bagdád
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
žije	žít	k5eAaImIp3nS	žít
zhruba	zhruba	k6eAd1	zhruba
5	[number]	k4	5
000	[number]	k4	000
mandejců	mandejec	k1gMnPc2	mandejec
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
náboženství	náboženství	k1gNnPc1	náboženství
starší	starý	k2eAgFnSc2d2	starší
než	než	k8xS	než
křesťanství	křesťanství	k1gNnSc2	křesťanství
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
čerpá	čerpat	k5eAaImIp3nS	čerpat
z	z	k7c2	z
mezopotámského	mezopotámský	k2eAgNnSc2d1	mezopotámský
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
iráckém	irácký	k2eAgNnSc6d1	irácké
území	území	k1gNnSc6	území
asi	asi	k9	asi
150	[number]	k4	150
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byli	být	k5eAaImAgMnP	být
ale	ale	k8xC	ale
vládou	vláda	k1gFnSc7	vláda
soustavně	soustavně	k6eAd1	soustavně
pronásledováni	pronásledovat	k5eAaImNgMnP	pronásledovat
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
Izraele	Izrael	k1gInSc2	Izrael
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
Irák	Irák	k1gInSc1	Irák
opustila	opustit	k5eAaPmAgFnS	opustit
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
jich	on	k3xPp3gMnPc2	on
tam	tam	k6eAd1	tam
žije	žít	k5eAaImIp3nS	žít
méně	málo	k6eAd2	málo
než	než	k8xS	než
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Uprchlíci	uprchlík	k1gMnPc5	uprchlík
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
druhé	druhý	k4xOgFnSc2	druhý
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
uprchlo	uprchnout	k5eAaPmAgNnS	uprchnout
z	z	k7c2	z
Iráku	Irák	k1gInSc2	Irák
přes	přes	k7c4	přes
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
další	další	k2eAgInSc4d1	další
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
byly	být	k5eAaImAgInP	být
vyhnány	vyhnat	k5eAaPmNgInP	vyhnat
z	z	k7c2	z
domova	domov	k1gInSc2	domov
a	a	k8xC	a
přestěhovaly	přestěhovat	k5eAaPmAgFnP	přestěhovat
se	se	k3xPyFc4	se
na	na	k7c4	na
jiné	jiný	k2eAgNnSc4d1	jiné
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
milion	milion	k4xCgInSc4	milion
iráckých	irácký	k2eAgMnPc2d1	irácký
uprchlíků	uprchlík	k1gMnPc2	uprchlík
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
asi	asi	k9	asi
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
je	on	k3xPp3gMnPc4	on
v	v	k7c6	v
Jordánsku	Jordánsko	k1gNnSc6	Jordánsko
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
<g/>
,	,	kIx,	,
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
arabských	arabský	k2eAgInPc6d1	arabský
státech	stát	k1gInPc6	stát
v	v	k7c6	v
Perském	perský	k2eAgInSc6d1	perský
zálivu	záliv	k1gInSc6	záliv
<g/>
,	,	kIx,	,
Íránu	Írán	k1gInSc6	Írán
a	a	k8xC	a
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
krize	krize	k1gFnSc1	krize
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
uprchlickou	uprchlický	k2eAgFnSc7d1	uprchlická
krizí	krize	k1gFnSc7	krize
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
od	od	k7c2	od
první	první	k4xOgFnSc2	první
arabsko-izraelské	arabskozraelský	k2eAgFnSc2d1	arabsko-izraelská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
OSN	OSN	kA	OSN
Irák	Irák	k1gInSc1	Irák
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
<g/>
Mnoho	mnoho	k4c1	mnoho
dětí	dítě	k1gFnPc2	dítě
uprchlíků	uprchlík	k1gMnPc2	uprchlík
nechodí	chodit	k5eNaImIp3nP	chodit
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nP	bát
odhalení	odhalení	k1gNnSc3	odhalení
jejich	jejich	k3xOp3gMnPc2	jejich
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
legálního	legální	k2eAgInSc2d1	legální
pobytu	pobyt	k1gInSc2	pobyt
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
jich	on	k3xPp3gMnPc2	on
taky	taky	k6eAd1	taky
ilegálně	ilegálně	k6eAd1	ilegálně
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
uživily	uživit	k5eAaPmAgInP	uživit
své	svůj	k3xOyFgFnPc4	svůj
rodiny	rodina	k1gFnPc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
se	s	k7c7	s
problémem	problém	k1gInSc7	problém
stala	stát	k5eAaPmAgFnS	stát
dětská	dětský	k2eAgFnSc1d1	dětská
a	a	k8xC	a
ženská	ženský	k2eAgFnSc1d1	ženská
prostituce	prostituce	k1gFnSc1	prostituce
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
se	se	k3xPyFc4	se
uprchlíci	uprchlík	k1gMnPc1	uprchlík
uchylují	uchylovat	k5eAaImIp3nP	uchylovat
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Stáří	stáří	k1gNnSc1	stáří
irácké	irácký	k2eAgFnSc2d1	irácká
kultury	kultura	k1gFnSc2	kultura
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
starověké	starověký	k2eAgFnSc2d1	starověká
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
území	území	k1gNnSc6	území
žili	žít	k5eAaImAgMnP	žít
Sumerové	Sumer	k1gMnPc1	Sumer
<g/>
,	,	kIx,	,
Akkadové	Akkadový	k2eAgFnPc1d1	Akkadový
<g/>
,	,	kIx,	,
Babylonci	Babylonek	k1gMnPc1	Babylonek
a	a	k8xC	a
Asyřané	Asyřan	k1gMnPc1	Asyřan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
např.	např.	kA	např.
visuté	visutý	k2eAgFnSc2d1	visutá
zahrady	zahrada	k1gFnSc2	zahrada
Semiramidiny	Semiramidin	k2eAgFnPc1d1	Semiramidin
<g/>
,	,	kIx,	,
Ištařina	Ištařina	k1gFnSc1	Ištařina
brána	brána	k1gFnSc1	brána
a	a	k8xC	a
mnohé	mnohý	k2eAgInPc1d1	mnohý
chrámy	chrám	k1gInPc1	chrám
zasvěcené	zasvěcený	k2eAgInPc1d1	zasvěcený
mezopotámským	mezopotámský	k2eAgMnPc3d1	mezopotámský
bohům	bůh	k1gMnPc3	bůh
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
patřily	patřit	k5eAaImAgInP	patřit
i	i	k9	i
zikkuraty	zikkurat	k1gInPc1	zikkurat
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
zikkuratů	zikkurat	k1gInPc2	zikkurat
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ten	ten	k3xDgInSc1	ten
v	v	k7c6	v
Borsippě	Borsippa	k1gFnSc6	Borsippa
nedaleko	nedaleko	k7c2	nedaleko
Bagádu	Bagád	k1gInSc2	Bagád
<g/>
,	,	kIx,	,
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xC	jako
předloha	předloha	k1gFnSc1	předloha
k	k	k7c3	k
biblické	biblický	k2eAgFnSc3d1	biblická
Babylonské	babylonský	k2eAgFnSc3d1	Babylonská
věži	věž	k1gFnSc3	věž
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
půlky	půlka	k1gFnSc2	půlka
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
ujali	ujmout	k5eAaPmAgMnP	ujmout
muslimové	muslim	k1gMnPc1	muslim
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
projevuje	projevovat	k5eAaImIp3nS	projevovat
islámská	islámský	k2eAgFnSc1d1	islámská
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
islámské	islámský	k2eAgFnPc4d1	islámská
stavby	stavba	k1gFnPc4	stavba
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
patří	patřit	k5eAaImIp3nP	patřit
Alího	Alí	k1gMnSc4	Alí
mešita	mešita	k1gFnSc1	mešita
(	(	kIx(	(
<g/>
třetí	třetí	k4xOgFnSc1	třetí
nejposvátnější	posvátný	k2eAgFnSc1d3	nejposvátnější
mešita	mešita	k1gFnSc1	mešita
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itského	itský	k2eAgInSc2d1	itský
islámu	islám	k1gInSc2	islám
<g/>
;	;	kIx,	;
Nadžaf	Nadžaf	k1gInSc1	Nadžaf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
al	ala	k1gFnPc2	ala
Askarího	Askarí	k1gMnSc4	Askarí
mešita	mešita	k1gFnSc1	mešita
(	(	kIx(	(
<g/>
Samarra	Samarra	k1gFnSc1	Samarra
<g/>
)	)	kIx)	)
a	a	k8xC	a
Husajnova	Husajnův	k2eAgFnSc1d1	Husajnova
mešita	mešita	k1gFnSc1	mešita
(	(	kIx(	(
<g/>
Karbalá	Karbalý	k2eAgFnSc1d1	Karbalá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Centrem	centr	k1gInSc7	centr
irácké	irácký	k2eAgFnSc2d1	irácká
kultury	kultura	k1gFnSc2	kultura
je	být	k5eAaImIp3nS	být
Bagdád	Bagdád	k1gInSc1	Bagdád
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
míchají	míchat	k5eAaImIp3nP	míchat
západní	západní	k2eAgInPc1d1	západní
umělecké	umělecký	k2eAgInPc1d1	umělecký
projevy	projev	k1gInPc1	projev
(	(	kIx(	(
<g/>
např.	např.	kA	např.
balet	balet	k1gInSc1	balet
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc1	divadlo
a	a	k8xC	a
moderní	moderní	k2eAgNnSc1d1	moderní
umění	umění	k1gNnSc1	umění
<g/>
)	)	kIx)	)
s	s	k7c7	s
tradičnějším	tradiční	k2eAgNnSc7d2	tradičnější
blízkovýchodním	blízkovýchodní	k2eAgNnSc7d1	blízkovýchodní
uměním	umění	k1gNnSc7	umění
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgFnPc7d1	významná
uměleckými	umělecký	k2eAgFnPc7d1	umělecká
formami	forma	k1gFnPc7	forma
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
jsou	být	k5eAaImIp3nP	být
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
malířství	malířství	k1gNnSc2	malířství
a	a	k8xC	a
sochařství	sochařství	k1gNnSc2	sochařství
<g/>
.	.	kIx.	.
</s>
<s>
Iráčané	Iráčan	k1gMnPc1	Iráčan
k	k	k7c3	k
většině	většina	k1gFnSc3	většina
běžných	běžný	k2eAgFnPc2d1	běžná
záležitostí	záležitost	k1gFnPc2	záležitost
využívají	využívat	k5eAaPmIp3nP	využívat
gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
,	,	kIx,	,
k	k	k7c3	k
náboženským	náboženský	k2eAgInPc3d1	náboženský
svátkům	svátek	k1gInPc3	svátek
pak	pak	k6eAd1	pak
islámský	islámský	k2eAgInSc4d1	islámský
lunární	lunární	k2eAgInSc4d1	lunární
kalendář	kalendář	k1gInSc4	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
muslimský	muslimský	k2eAgInSc1d1	muslimský
svátek	svátek	k1gInSc1	svátek
je	být	k5eAaImIp3nS	být
ramadán	ramadán	k1gInSc4	ramadán
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
Íd	Íd	k1gMnSc1	Íd
al-Fitr	al-Fitr	k1gMnSc1	al-Fitr
<g/>
,	,	kIx,	,
oslava	oslava	k1gFnSc1	oslava
konce	konec	k1gInSc2	konec
ramadánu	ramadán	k1gInSc2	ramadán
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
informací	informace	k1gFnPc2	informace
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
o	o	k7c4	o
zachování	zachování	k1gNnSc4	zachování
tradičních	tradiční	k2eAgInPc2d1	tradiční
druhů	druh	k1gInPc2	druh
povolání	povolání	k1gNnSc2	povolání
jako	jako	k8xS	jako
např.	např.	kA	např.
zpracování	zpracování	k1gNnSc4	zpracování
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
koželužství	koželužství	k1gNnSc2	koželužství
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
iráčtí	irácký	k2eAgMnPc1d1	irácký
umělci	umělec	k1gMnPc1	umělec
a	a	k8xC	a
intelektuálové	intelektuál	k1gMnPc1	intelektuál
snažili	snažit	k5eAaImAgMnP	snažit
vytvořit	vytvořit	k5eAaPmF	vytvořit
novou	nový	k2eAgFnSc4d1	nová
národní	národní	k2eAgFnSc4d1	národní
identitu	identita	k1gFnSc4	identita
skrze	skrze	k?	skrze
odkazy	odkaz	k1gInPc4	odkaz
na	na	k7c4	na
civilizace	civilizace	k1gFnPc4	civilizace
dávné	dávný	k2eAgFnSc2d1	dávná
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
Režim	režim	k1gInSc4	režim
strany	strana	k1gFnSc2	strana
Baas	Baas	k1gInSc1	Baas
toto	tento	k3xDgNnSc4	tento
snažení	snažení	k1gNnSc4	snažení
podporoval	podporovat	k5eAaImAgInS	podporovat
výstavbou	výstavba	k1gFnSc7	výstavba
archeologických	archeologický	k2eAgNnPc2d1	Archeologické
muzeí	muzeum	k1gNnPc2	muzeum
a	a	k8xC	a
pořádáním	pořádání	k1gNnSc7	pořádání
kulturních	kulturní	k2eAgInPc2d1	kulturní
festivalů	festival	k1gInPc2	festival
<g/>
.	.	kIx.	.
</s>
<s>
Saddám	Saddám	k1gMnSc1	Saddám
Husajn	Husajn	k1gMnSc1	Husajn
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
stranu	strana	k1gFnSc4	strana
umění	umění	k1gNnSc2	umění
podporoval	podporovat	k5eAaImAgMnS	podporovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cenzuroval	cenzurovat	k5eAaImAgMnS	cenzurovat
a	a	k8xC	a
zakazoval	zakazovat	k5eAaImAgMnS	zakazovat
ty	ten	k3xDgInPc4	ten
umělecké	umělecký	k2eAgInPc4d1	umělecký
projevy	projev	k1gInPc4	projev
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ho	on	k3xPp3gNnSc4	on
kritizovaly	kritizovat	k5eAaImAgFnP	kritizovat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zakázal	zakázat	k5eAaPmAgMnS	zakázat
některé	některý	k3yIgInPc4	některý
svátky	svátek	k1gInPc4	svátek
kvůli	kvůli	k7c3	kvůli
obavě	obava	k1gFnSc3	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
velká	velký	k2eAgNnPc1d1	velké
setkání	setkání	k1gNnPc1	setkání
lidí	člověk	k1gMnPc2	člověk
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
demonstracím	demonstrace	k1gFnPc3	demonstrace
proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
<g/>
Nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
muzeem	muzeum	k1gNnSc7	muzeum
je	být	k5eAaImIp3nS	být
Irácké	irácký	k2eAgNnSc4d1	irácké
národní	národní	k2eAgNnSc4d1	národní
muzeum	muzeum	k1gNnSc4	muzeum
umístěné	umístěný	k2eAgNnSc4d1	umístěné
v	v	k7c6	v
Bagdádu	Bagdád	k1gInSc6	Bagdád
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
jsou	být	k5eAaImIp3nP	být
zapsána	zapsán	k2eAgFnSc1d1	zapsána
3	[number]	k4	3
irácká	irácký	k2eAgFnSc1d1	irácká
města	město	k1gNnSc2	město
<g/>
:	:	kIx,	:
Aššúr	Aššúr	k1gMnSc1	Aššúr
<g/>
,	,	kIx,	,
Samarra	Samarra	k1gMnSc1	Samarra
a	a	k8xC	a
Hatra	Hatra	k1gMnSc1	Hatra
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
významná	významný	k2eAgNnPc4d1	významné
historická	historický	k2eAgNnPc4d1	historické
města	město	k1gNnPc4	město
sloužící	sloužící	k2eAgFnSc2d1	sloužící
jako	jako	k8xC	jako
archeologické	archeologický	k2eAgFnSc2d1	archeologická
lokality	lokalita	k1gFnSc2	lokalita
patří	patřit	k5eAaImIp3nS	patřit
Ninive	Ninive	k1gNnSc1	Ninive
<g/>
,	,	kIx,	,
Kiš	kiš	k0	kiš
<g/>
,	,	kIx,	,
Akkad	Akkad	k1gInSc4	Akkad
<g/>
,	,	kIx,	,
Lagaš	Lagaš	k1gInSc4	Lagaš
<g/>
,	,	kIx,	,
Eridu	Eris	k1gFnSc4	Eris
<g/>
,	,	kIx,	,
Ur	Ur	k1gInSc1	Ur
<g/>
,	,	kIx,	,
Uruk	Uruk	k1gInSc1	Uruk
<g/>
,	,	kIx,	,
Nimrud	Nimrud	k1gInSc1	Nimrud
<g/>
,	,	kIx,	,
Babylón	Babylón	k1gInSc1	Babylón
a	a	k8xC	a
Nippur	Nippur	k1gMnSc1	Nippur
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mezopotámského	mezopotámský	k2eAgNnSc2d1	mezopotámský
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
také	také	k9	také
různé	různý	k2eAgFnPc1d1	různá
sochy	socha	k1gFnPc1	socha
<g/>
,	,	kIx,	,
malby	malba	k1gFnPc1	malba
a	a	k8xC	a
texty	text	k1gInPc1	text
vyryté	vyrytý	k2eAgInPc1d1	vyrytý
do	do	k7c2	do
hliněných	hliněný	k2eAgFnPc2d1	hliněná
tabulek	tabulka	k1gFnPc2	tabulka
(	(	kIx(	(
<g/>
Epos	epos	k1gInSc4	epos
o	o	k7c6	o
Gilgamešovi	Gilgameš	k1gMnSc6	Gilgameš
<g/>
,	,	kIx,	,
Chammurapiho	Chammurapi	k1gMnSc4	Chammurapi
zákoník	zákoník	k1gMnSc1	zákoník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
kandidátů	kandidát	k1gMnPc2	kandidát
na	na	k7c4	na
zápis	zápis	k1gInSc4	zápis
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
UNESCO	UNESCO	kA	UNESCO
je	být	k5eAaImIp3nS	být
citadela	citadela	k1gFnSc1	citadela
v	v	k7c6	v
Arbílu	Arbílo	k1gNnSc6	Arbílo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
sedmitisíciletou	sedmitisíciletý	k2eAgFnSc7d1	sedmitisíciletý
historií	historie	k1gFnSc7	historie
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgInPc4d3	nejstarší
nepřetržitě	přetržitě	k6eNd1	přetržitě
obydlená	obydlený	k2eAgNnPc4d1	obydlené
městská	městský	k2eAgNnPc4d1	Městské
sídla	sídlo	k1gNnPc4	sídlo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
věda	věda	k1gFnSc1	věda
===	===	k?	===
</s>
</p>
<p>
<s>
Zhruba	zhruba	k6eAd1	zhruba
75	[number]	k4	75
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
gramotných	gramotný	k2eAgMnPc2d1	gramotný
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
doba	doba	k1gFnSc1	doba
strávená	strávený	k2eAgFnSc1d1	strávená
vzděláváním	vzdělávání	k1gNnSc7	vzdělávání
(	(	kIx(	(
<g/>
od	od	k7c2	od
primárního	primární	k2eAgMnSc2d1	primární
do	do	k7c2	do
terciárního	terciární	k2eAgInSc2d1	terciární
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Povinná	povinný	k2eAgFnSc1d1	povinná
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
šesté	šestý	k4xOgFnSc2	šestý
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
žáci	žák	k1gMnPc1	žák
píší	psát	k5eAaImIp3nP	psát
celostátní	celostátní	k2eAgFnPc4d1	celostátní
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
o	o	k7c6	o
postupu	postup	k1gInSc6	postup
do	do	k7c2	do
vyšších	vysoký	k2eAgInPc2d2	vyšší
ročníků	ročník	k1gInPc2	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sedmé	sedmý	k4xOgFnSc2	sedmý
třídy	třída	k1gFnSc2	třída
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
škol	škola	k1gFnPc2	škola
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
chlapecké	chlapecký	k2eAgFnPc4d1	chlapecká
a	a	k8xC	a
dívčí	dívčí	k2eAgFnPc4d1	dívčí
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
neexistovaly	existovat	k5eNaImAgFnP	existovat
soukromé	soukromý	k2eAgFnPc1d1	soukromá
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
jich	on	k3xPp3gMnPc2	on
už	už	k9	už
bylo	být	k5eAaImAgNnS	být
přes	přes	k7c4	přes
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělávací	vzdělávací	k2eAgFnPc1d1	vzdělávací
instituce	instituce	k1gFnPc1	instituce
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
špatné	špatný	k2eAgFnSc6d1	špatná
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
profesorů	profesor	k1gMnPc2	profesor
emigrovalo	emigrovat	k5eAaBmAgNnS	emigrovat
během	během	k7c2	během
vlády	vláda	k1gFnSc2	vláda
Saddáma	Saddám	k1gMnSc4	Saddám
Husajna	Husajn	k1gMnSc4	Husajn
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
stalo	stát	k5eAaPmAgNnS	stát
oběťmi	oběť	k1gFnPc7	oběť
výhrůžek	výhrůžka	k1gFnPc2	výhrůžka
a	a	k8xC	a
fyzického	fyzický	k2eAgNnSc2d1	fyzické
napadání	napadání	k1gNnSc2	napadání
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
vlně	vlna	k1gFnSc3	vlna
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
celkových	celkový	k2eAgFnPc2d1	celková
13	[number]	k4	13
univerzit	univerzita	k1gFnPc2	univerzita
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
40	[number]	k4	40
institucí	instituce	k1gFnPc2	instituce
vysokého	vysoký	k2eAgNnSc2d1	vysoké
vzdělání	vzdělání	k1gNnSc2	vzdělání
byla	být	k5eAaImAgFnS	být
navíc	navíc	k6eAd1	navíc
vypleněna	vyplenit	k5eAaPmNgFnS	vyplenit
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
nepokoji	nepokoj	k1gInPc7	nepokoj
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
a	a	k8xC	a
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
přestavbu	přestavba	k1gFnSc4	přestavba
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
zhruba	zhruba	k6eAd1	zhruba
80	[number]	k4	80
%	%	kIx~	%
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
15	[number]	k4	15
000	[number]	k4	000
budov	budova	k1gFnPc2	budova
nižšího	nízký	k2eAgNnSc2d2	nižší
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
kvůli	kvůli	k7c3	kvůli
nízkým	nízký	k2eAgFnPc3d1	nízká
dotacím	dotace	k1gFnPc3	dotace
za	za	k7c2	za
Husajnovy	Husajnův	k2eAgFnSc2d1	Husajnova
vlády	vláda	k1gFnSc2	vláda
ve	v	k7c6	v
špatném	špatný	k2eAgInSc6d1	špatný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sport	sport	k1gInSc1	sport
===	===	k?	===
</s>
</p>
<p>
<s>
Nejhranějším	hraný	k2eAgInSc7d3	nejhranější
sportem	sport	k1gInSc7	sport
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
je	být	k5eAaImIp3nS	být
fotbal	fotbal	k1gInSc1	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
irácká	irácký	k2eAgFnSc1d1	irácká
reprezentace	reprezentace	k1gFnSc1	reprezentace
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
mistrovství	mistrovství	k1gNnSc4	mistrovství
Asie	Asie	k1gFnSc2	Asie
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
Irák	Irák	k1gInSc1	Irák
poprvé	poprvé	k6eAd1	poprvé
účastnil	účastnit	k5eAaImAgMnS	účastnit
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
účasti	účast	k1gFnSc2	účast
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
si	se	k3xPyFc3	se
odnesl	odnést	k5eAaPmAgMnS	odnést
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
za	za	k7c4	za
vzpírání	vzpírání	k1gNnSc4	vzpírání
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
se	se	k3xPyFc4	se
iráčtí	irácký	k2eAgMnPc1d1	irácký
sportovci	sportovec	k1gMnPc1	sportovec
účastní	účastnit	k5eAaImIp3nS	účastnit
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zatím	zatím	k6eAd1	zatím
se	se	k3xPyFc4	se
neobjevili	objevit	k5eNaPmAgMnP	objevit
na	na	k7c6	na
zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kuchyně	kuchyně	k1gFnSc2	kuchyně
===	===	k?	===
</s>
</p>
<p>
<s>
Irácká	irácký	k2eAgFnSc1d1	irácká
kuchyně	kuchyně	k1gFnSc1	kuchyně
má	mít	k5eAaImIp3nS	mít
tradici	tradice	k1gFnSc4	tradice
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
10	[number]	k4	10
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
Bagdádu	Bagdád	k1gInSc6	Bagdád
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
toto	tento	k3xDgNnSc1	tento
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
centrem	centr	k1gInSc7	centr
islámské	islámský	k2eAgFnSc2d1	islámská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
výrazně	výrazně	k6eAd1	výrazně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
kuchyněmi	kuchyně	k1gFnPc7	kuchyně
sousedních	sousední	k2eAgFnPc2d1	sousední
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
Íránu	Írán	k1gInSc2	Írán
<g/>
,	,	kIx,	,
Turecka	Turecko	k1gNnSc2	Turecko
a	a	k8xC	a
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
všechny	všechen	k3xTgFnPc4	všechen
ostatní	ostatní	k2eAgFnPc4d1	ostatní
oblasti	oblast	k1gFnPc4	oblast
kultury	kultura	k1gFnSc2	kultura
je	být	k5eAaImIp3nS	být
i	i	k9	i
irácká	irácký	k2eAgFnSc1d1	irácká
kuchyně	kuchyně	k1gFnSc1	kuchyně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
islámem	islám	k1gInSc7	islám
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
zákazem	zákaz	k1gInSc7	zákaz
konzumace	konzumace	k1gFnSc2	konzumace
vepřového	vepřový	k2eAgNnSc2d1	vepřové
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
pokrmem	pokrm	k1gInSc7	pokrm
je	být	k5eAaImIp3nS	být
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
kuřecí	kuřecí	k2eAgMnSc1d1	kuřecí
a	a	k8xC	a
jehněčí	jehněčí	k2eAgMnSc1d1	jehněčí
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
provedeních	provedení	k1gNnPc6	provedení
jako	jako	k8xC	jako
kebab	kebab	k1gInSc4	kebab
<g/>
,	,	kIx,	,
khouzí	khouzet	k5eAaImIp3nS	khouzet
(	(	kIx(	(
<g/>
skopové	skopový	k2eAgNnSc1d1	skopové
maso	maso	k1gNnSc1	maso
s	s	k7c7	s
rýží	rýže	k1gFnSc7	rýže
<g/>
)	)	kIx)	)
a	a	k8xC	a
kibbe	kibbat	k5eAaPmIp3nS	kibbat
(	(	kIx(	(
<g/>
pšeničné	pšeničný	k2eAgFnPc1d1	pšeničná
kuličky	kulička	k1gFnPc1	kulička
s	s	k7c7	s
masem	maso	k1gNnSc7	maso
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgFnSc7d3	nejčastější
přílohou	příloha	k1gFnSc7	příloha
k	k	k7c3	k
masu	maso	k1gNnSc3	maso
je	být	k5eAaImIp3nS	být
rýže	rýže	k1gFnSc1	rýže
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
běžná	běžný	k2eAgNnPc4d1	běžné
jídla	jídlo	k1gNnPc4	jídlo
patří	patřit	k5eAaImIp3nS	patřit
dolma	dolma	k1gFnSc1	dolma
(	(	kIx(	(
<g/>
plněná	plněný	k2eAgFnSc1d1	plněná
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jogurt	jogurt	k1gInSc1	jogurt
<g/>
,	,	kIx,	,
datle	datle	k1gFnSc1	datle
nebo	nebo	k8xC	nebo
masguf	masguf	k1gInSc1	masguf
<g/>
,	,	kIx,	,
grilovaná	grilovaný	k2eAgFnSc1d1	grilovaná
rybí	rybí	k2eAgFnSc1d1	rybí
specialita	specialita	k1gFnSc1	specialita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
považovaná	považovaný	k2eAgFnSc1d1	považovaná
za	za	k7c4	za
irácké	irácký	k2eAgNnSc4d1	irácké
národní	národní	k2eAgNnSc4d1	národní
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
koření	kořenit	k5eAaImIp3nS	kořenit
se	se	k3xPyFc4	se
nejčastější	častý	k2eAgFnSc7d3	nejčastější
používá	používat	k5eAaImIp3nS	používat
směs	směs	k1gFnSc1	směs
baharat	baharat	k5eAaPmF	baharat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Česky	Česko	k1gNnPc7	Česko
====	====	k?	====
</s>
</p>
<p>
<s>
GOMBÁR	GOMBÁR	kA	GOMBÁR
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
<g/>
;	;	kIx,	;
PECHA	Pecha	k1gMnSc1	Pecha
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Iráku	Irák	k1gInSc2	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
702	[number]	k4	702
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7422	[number]	k4	7422
<g/>
-	-	kIx~	-
<g/>
249	[number]	k4	249
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MUSIL	Musil	k1gMnSc1	Musil
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Eufratem	Eufrat	k1gInSc7	Eufrat
a	a	k8xC	a
Tigridem	Tigris	k1gInSc7	Tigris
:	:	kIx,	:
Nový	nový	k2eAgInSc1d1	nový
Irák	Irák	k1gInSc1	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
167	[number]	k4	167
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PONÍŽILOVÁ	PONÍŽILOVÁ	kA	PONÍŽILOVÁ
<g/>
,	,	kIx,	,
Martina	Martina	k1gFnSc1	Martina
<g/>
.	.	kIx.	.
</s>
<s>
Irák	Irák	k1gInSc1	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
168	[number]	k4	168
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Stručná	stručný	k2eAgFnSc1d1	stručná
historie	historie	k1gFnSc1	historie
států	stát	k1gInPc2	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
491	[number]	k4	491
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PŘEBINDA	PŘEBINDA	kA	PŘEBINDA
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
soudobému	soudobý	k2eAgInSc3d1	soudobý
Iráku	Irák	k1gInSc3	Irák
:	:	kIx,	:
Núrí	Núrí	k2eAgInSc1d1	Núrí
as-Sacíd	as-Sacíd	k1gInSc1	as-Sacíd
a	a	k8xC	a
dějiny	dějiny	k1gFnPc1	dějiny
irácké	irácký	k2eAgFnSc2d1	irácká
monarchie	monarchie	k1gFnSc2	monarchie
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Montanex	Montanex	k1gInSc1	Montanex
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
155	[number]	k4	155
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7225	[number]	k4	7225
<g/>
-	-	kIx~	-
<g/>
189	[number]	k4	189
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RADĚJ	raděj	k9	raděj
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Irácké	irácký	k2eAgNnSc1d1	irácké
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003-2009	[number]	k4	2003-2009
:	:	kIx,	:
strategie	strategie	k1gFnSc1	strategie
<g/>
,	,	kIx,	,
taktika	taktika	k1gFnSc1	taktika
a	a	k8xC	a
ideologie	ideologie	k1gFnSc1	ideologie
islámských	islámský	k2eAgNnPc2d1	islámské
radikálních	radikální	k2eAgNnPc2d1	radikální
a	a	k8xC	a
nacionalistických	nacionalistický	k2eAgNnPc2d1	nacionalistické
uskupení	uskupení	k1gNnPc2	uskupení
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
210	[number]	k4	210
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7225	[number]	k4	7225
<g/>
-	-	kIx~	-
<g/>
189	[number]	k4	189
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SLUGLETT	SLUGLETT	kA	SLUGLETT
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
;	;	kIx,	;
FAROUK-SLUGLETTOVÁ	FAROUK-SLUGLETTOVÁ	k1gMnSc1	FAROUK-SLUGLETTOVÁ
<g/>
,	,	kIx,	,
Marion	Marion	k1gInSc1	Marion
<g/>
.	.	kIx.	.
</s>
<s>
Irák	Irák	k1gInSc1	Irák
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
:	:	kIx,	:
Od	od	k7c2	od
revoluce	revoluce	k1gFnSc2	revoluce
k	k	k7c3	k
diktatuře	diktatura	k1gFnSc3	diktatura
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
450	[number]	k4	450
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
500	[number]	k4	500
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TAUER	TAUER	kA	TAUER
<g/>
,	,	kIx,	,
Felix	Felix	k1gMnSc1	Felix
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
islámu	islám	k1gInSc2	islám
:	:	kIx,	:
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
kultura	kultura	k1gFnSc1	kultura
:	:	kIx,	:
nástin	nástin	k1gInSc1	nástin
politického	politický	k2eAgInSc2d1	politický
<g/>
,	,	kIx,	,
sociálního	sociální	k2eAgInSc2d1	sociální
<g/>
,	,	kIx,	,
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
a	a	k8xC	a
kulturního	kulturní	k2eAgInSc2d1	kulturní
vývoje	vývoj	k1gInSc2	vývoj
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgMnPc2	jenž
proniklo	proniknout	k5eAaPmAgNnS	proniknout
učení	učení	k1gNnSc1	učení
arabského	arabský	k2eAgMnSc2d1	arabský
proroka	prorok	k1gMnSc2	prorok
<g/>
,	,	kIx,	,
od	od	k7c2	od
jeho	jeho	k3xOp3gNnSc2	jeho
vystoupení	vystoupení	k1gNnSc2	vystoupení
do	do	k7c2	do
konce	konec	k1gInSc2	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
445	[number]	k4	445
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
828	[number]	k4	828
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WEST	WEST	kA	WEST
<g/>
,	,	kIx,	,
Francis	Francis	k1gInSc1	Francis
J.	J.	kA	J.
Nejmocnější	mocný	k2eAgInSc1d3	nejmocnější
kmen	kmen	k1gInSc1	kmen
:	:	kIx,	:
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
politika	politika	k1gFnSc1	politika
a	a	k8xC	a
ukončení	ukončení	k1gNnSc1	ukončení
přítomnosti	přítomnost	k1gFnSc2	přítomnost
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
206	[number]	k4	206
<g/>
-	-	kIx~	-
<g/>
1142	[number]	k4	1142
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Anglicky	Anglicko	k1gNnPc7	Anglicko
====	====	k?	====
</s>
</p>
<p>
<s>
Geology	geolog	k1gMnPc4	geolog
of	of	k?	of
Iraq	Iraq	k1gFnSc1	Iraq
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Saad	Saada	k1gFnPc2	Saada
Z.	Z.	kA	Z.
Jassim	Jassim	k1gInSc1	Jassim
<g/>
,	,	kIx,	,
Jeremy	Jerem	k1gInPc1	Jerem
C.	C.	kA	C.
Goff	Goff	k1gInSc1	Goff
<g/>
.	.	kIx.	.
</s>
<s>
Prague	Prague	k1gNnSc1	Prague
<g/>
;	;	kIx,	;
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Dolin	dolina	k1gFnPc2	dolina
<g/>
;	;	kIx,	;
Moravian	Moravian	k1gMnSc1	Moravian
Museum	museum	k1gNnSc4	museum
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7028	[number]	k4	7028
<g/>
-	-	kIx~	-
<g/>
287	[number]	k4	287
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hard	Hard	k6eAd1	Hard
lessons	lessons	k6eAd1	lessons
:	:	kIx,	:
The	The	k1gFnSc1	The
Iraq	Iraq	k1gFnSc1	Iraq
reconstruction	reconstruction	k1gInSc4	reconstruction
experience	experience	k1gFnSc2	experience
<g/>
.	.	kIx.	.
</s>
<s>
Washington	Washington	k1gInSc1	Washington
<g/>
:	:	kIx,	:
U.	U.	kA	U.
S.	S.	kA	S.
<g/>
Document	Document	k1gInSc1	Document
Printing	Printing	k1gInSc1	Printing
Office	Office	kA	Office
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
80817	[number]	k4	80817
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Iraq	Iraq	k?	Iraq
<g/>
.	.	kIx.	.
</s>
<s>
Petaluma	Petaluma	k1gFnSc1	Petaluma
<g/>
:	:	kIx,	:
World	World	k1gInSc1	World
Trade	Trad	k1gInSc5	Trad
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
60780	[number]	k4	60780
<g/>
-	-	kIx~	-
<g/>
366	[number]	k4	366
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Religion	religion	k1gInSc1	religion
and	and	k?	and
nationalism	nationalism	k1gInSc1	nationalism
in	in	k?	in
Iraq	Iraq	k1gFnSc2	Iraq
:	:	kIx,	:
a	a	k8xC	a
comparative	comparativ	k1gInSc5	comparativ
perspective	perspectiv	k1gInSc5	perspectiv
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
David	David	k1gMnSc1	David
Little	Little	k1gFnSc2	Little
<g/>
,	,	kIx,	,
Donald	Donald	k1gMnSc1	Donald
K.	K.	kA	K.
Swearer	Swearer	k1gMnSc1	Swearer
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
,	,	kIx,	,
Mass	Mass	k1gInSc1	Mass
<g/>
:	:	kIx,	:
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
the	the	k?	the
Study	stud	k1gInPc1	stud
of	of	k?	of
World	World	k1gInSc1	World
Religions	Religionsa	k1gFnPc2	Religionsa
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
945454	[number]	k4	945454
<g/>
-	-	kIx~	-
<g/>
41	[number]	k4	41
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
EIU	EIU	kA	EIU
<g/>
,	,	kIx,	,
the	the	k?	the
Economist	Economist	k1gInSc1	Economist
Intelligence	Intelligence	k1gFnSc2	Intelligence
Unit	Unita	k1gFnPc2	Unita
<g/>
.	.	kIx.	.
</s>
<s>
Country	country	k2eAgInSc1d1	country
report	report	k1gInSc1	report
<g/>
.	.	kIx.	.
</s>
<s>
Iraq	Iraq	k?	Iraq
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Unit	Unit	k1gMnSc1	Unit
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
1986	[number]	k4	1986
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ALGAZE	ALGAZE	kA	ALGAZE
<g/>
,	,	kIx,	,
Guillermo	Guillerma	k1gFnSc5	Guillerma
<g/>
.	.	kIx.	.
</s>
<s>
Ancient	Ancient	k1gMnSc1	Ancient
Mesopotamia	Mesopotamium	k1gNnSc2	Mesopotamium
at	at	k?	at
the	the	k?	the
dawn	dawn	k1gInSc1	dawn
of	of	k?	of
civilization	civilization	k1gInSc1	civilization
<g/>
:	:	kIx,	:
the	the	k?	the
evolution	evolution	k1gInSc1	evolution
of	of	k?	of
an	an	k?	an
urban	urban	k1gInSc1	urban
landscape	landscapat	k5eAaPmIp3nS	landscapat
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
Chicago	Chicago	k1gNnSc1	Chicago
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
226	[number]	k4	226
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1377	[number]	k4	1377
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ALÁVÍ	ALÁVÍ	kA	ALÁVÍ
<g/>
,	,	kIx,	,
Alí	Alí	k1gFnSc1	Alí
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
occupation	occupation	k1gInSc1	occupation
of	of	k?	of
Iraq	Iraq	k1gFnSc1	Iraq
:	:	kIx,	:
winning	winning	k1gInSc1	winning
the	the	k?	the
war	war	k?	war
<g/>
,	,	kIx,	,
losing	losing	k1gInSc1	losing
the	the	k?	the
peace	peace	k1gFnSc2	peace
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Haven	Haven	k1gInSc1	Haven
<g/>
:	:	kIx,	:
Yale	Yale	k1gInSc1	Yale
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
<g/>
-	-	kIx~	-
<g/>
13614	[number]	k4	13614
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BARKER	BARKER	kA	BARKER
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
A.	A.	kA	A.
<g/>
;	;	kIx,	;
HAMILTON	HAMILTON	kA	HAMILTON
<g/>
,	,	kIx,	,
Lee	Lea	k1gFnSc3	Lea
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Iraq	Iraq	k1gFnSc1	Iraq
study	stud	k1gInPc4	stud
group	group	k1gMnSc1	group
report	report	k1gInSc4	report
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Vintage	Vintage	k1gInSc1	Vintage
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
307	[number]	k4	307
<g/>
-	-	kIx~	-
<g/>
38656	[number]	k4	38656
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GOMPERT	GOMPERT	kA	GOMPERT
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
C.	C.	kA	C.
<g/>
;	;	kIx,	;
KELLY	KELLY	kA	KELLY
<g/>
,	,	kIx,	,
Terrence	Terrence	k1gFnPc1	Terrence
K.	K.	kA	K.
<g/>
;	;	kIx,	;
WATKINS	WATKINS	kA	WATKINS
<g/>
,	,	kIx,	,
Jessica	Jessicum	k1gNnSc2	Jessicum
<g/>
.	.	kIx.	.
</s>
<s>
Security	Securita	k1gFnPc1	Securita
in	in	k?	in
Iraq	Iraq	k1gFnSc1	Iraq
:	:	kIx,	:
a	a	k8xC	a
framework	framework	k1gInSc4	framework
for	forum	k1gNnPc2	forum
analyzing	analyzing	k1gInSc4	analyzing
emerging	emerging	k1gInSc1	emerging
threats	threats	k1gInSc1	threats
as	as	k1gInSc4	as
U.	U.	kA	U.
S.	S.	kA	S.
forces	forces	k1gMnSc1	forces
leave	leavat	k5eAaPmIp3nS	leavat
<g/>
.	.	kIx.	.
</s>
<s>
Santa	Santa	k1gMnSc1	Santa
Monica	Monica	k1gMnSc1	Monica
<g/>
:	:	kIx,	:
RAND	rand	k1gInSc1	rand
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8330	[number]	k4	8330
<g/>
-	-	kIx~	-
<g/>
4771	[number]	k4	4771
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHEHAB	CHEHAB	kA	CHEHAB
<g/>
,	,	kIx,	,
Zaki	Zaki	k1gNnSc2	Zaki
<g/>
.	.	kIx.	.
</s>
<s>
Iraq	Iraq	k?	Iraq
ablaze	ablaha	k1gFnSc3	ablaha
:	:	kIx,	:
inside	insid	k1gInSc5	insid
the	the	k?	the
insurgency	insurgencum	k1gNnPc7	insurgencum
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
I.B.	I.B.	k1gFnSc1	I.B.
Tauris	Tauris	k1gFnSc1	Tauris
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
84511	[number]	k4	84511
<g/>
-	-	kIx~	-
<g/>
110	[number]	k4	110
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LUKITZ	LUKITZ	kA	LUKITZ
<g/>
,	,	kIx,	,
Liora	Lioro	k1gNnSc2	Lioro
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
quest	quest	k1gFnSc1	quest
in	in	k?	in
the	the	k?	the
Middle	Middle	k1gMnSc1	Middle
East	East	k1gMnSc1	East
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
;	;	kIx,	;
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
I.B.	I.B.	k1gFnSc1	I.B.
Tauris	Tauris	k1gFnSc1	Tauris
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
85043	[number]	k4	85043
<g/>
-	-	kIx~	-
<g/>
415	[number]	k4	415
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MŪ	MŪ	k?	MŪ
<g/>
,	,	kIx,	,
Muḥ	Muḥ	k1gMnSc1	Muḥ
Jā	Jā	k1gMnSc1	Jā
<g/>
.	.	kIx.	.
</s>
<s>
Reading	Reading	k1gInSc1	Reading
Iraq	Iraq	k1gFnSc2	Iraq
:	:	kIx,	:
culture	cultur	k1gMnSc5	cultur
and	and	k?	and
power	power	k1gMnSc1	power
and	and	k?	and
conflict	conflict	k1gMnSc1	conflict
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
I.B.	I.B.	k1gFnSc1	I.B.
Tauris	Tauris	k1gFnSc1	Tauris
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
84511	[number]	k4	84511
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
70	[number]	k4	70
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SASSOON	SASSOON	kA	SASSOON
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Iraqi	Iraqi	k1gNnSc1	Iraqi
refugees	refugees	k1gMnSc1	refugees
:	:	kIx,	:
the	the	k?	the
new	new	k?	new
crisis	crisis	k1gInSc1	crisis
in	in	k?	in
the	the	k?	the
Middle	Middle	k1gMnSc1	Middle
East	East	k1gMnSc1	East
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
;	;	kIx,	;
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
I.B.	I.B.	k1gFnSc1	I.B.
Tauris	Tauris	k1gFnSc1	Tauris
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
84511	[number]	k4	84511
<g/>
-	-	kIx~	-
<g/>
919	[number]	k4	919
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Irák	Irák	k1gInSc1	Irák
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Irák	Irák	k1gInSc1	Irák
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Irák	Irák	k1gInSc1	Irák
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
SPECIÁL	speciál	k1gInSc1	speciál
<g/>
:	:	kIx,	:
Irák	Irák	k1gInSc1	Irák
–	–	k?	–
speciální	speciální	k2eAgFnSc1d1	speciální
příloha	příloha	k1gFnSc1	příloha
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
Iráku	Irák	k1gInSc2	Irák
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Novinky	novinka	k1gFnSc2	novinka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Iraq	Iraq	k?	Iraq
Directory	Director	k1gInPc1	Director
</s>
</p>
<p>
<s>
Country	country	k2eAgInSc1d1	country
of	of	k?	of
Origin	Origin	k2eAgInSc1d1	Origin
Information	Information	k1gInSc1	Information
Report	report	k1gInSc1	report
–	–	k?	–
Iraq	Iraq	k1gFnSc1	Iraq
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
UK	UK	kA	UK
Border	Border	k1gMnSc1	Border
Agency	Agenca	k1gMnSc2	Agenca
<g/>
,	,	kIx,	,
2011-08-17	[number]	k4	2011-08-17
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Iraq	Iraq	k?	Iraq
–	–	k?	–
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Iraq	Iraq	k?	Iraq
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Iraq	Iraq	k1gMnSc2	Iraq
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
Near	Near	k1gInSc1	Near
Eastern	Eastern	k1gMnSc1	Eastern
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Iraq	Iraq	k1gFnSc1	Iraq
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-05-02	[number]	k4	2011-05-02
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
–	–	k?	–
Iraq	Iraq	k1gFnSc1	Iraq
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-07-12	[number]	k4	2011-07-12
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Library	Librar	k1gInPc1	Librar
of	of	k?	of
Congress	Congress	k1gInSc1	Congress
<g/>
.	.	kIx.	.
</s>
<s>
Country	country	k2eAgFnPc1d1	country
Profile	profil	k1gInSc5	profil
<g/>
:	:	kIx,	:
Iraq	Iraq	k1gFnPc3	Iraq
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2010-07-08	[number]	k4	2010-07-08
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Bagdádu	Bagdád	k1gInSc6	Bagdád
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Irák	Irák	k1gInSc1	Irák
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-05-10	[number]	k4	2011-05-10
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BLAKE	BLAKE	kA	BLAKE
<g/>
,	,	kIx,	,
Gerald	Gerald	k1gMnSc1	Gerald
Henry	Henry	k1gMnSc1	Henry
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Iraq	Iraq	k?	Iraq
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Profil	profil	k1gInSc1	profil
Iráku	Irák	k1gInSc2	Irák
na	na	k7c6	na
BBC	BBC	kA	BBC
News	News	k1gInSc4	News
</s>
</p>
