<s>
Irák	Irák	k1gInSc1	Irák
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
politika	politika	k1gFnSc1	politika
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
silně	silně	k6eAd1	silně
zkorumpovaná	zkorumpovaný	k2eAgFnSc1d1	zkorumpovaná
a	a	k8xC	a
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
působení	působení	k1gNnSc3	působení
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
skupin	skupina	k1gFnPc2	skupina
neklidná	klidný	k2eNgFnSc1d1	neklidná
<g/>
.	.	kIx.	.
</s>
