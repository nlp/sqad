<s>
Za	za	k7c2	za
nejstarší	starý	k2eAgFnSc2d3	nejstarší
živé	živý	k2eAgFnSc2d1	živá
lípy	lípa	k1gFnSc2	lípa
malolisté	malolistý	k2eAgFnSc2d1	malolistá
jsou	být	k5eAaImIp3nP	být
považované	považovaný	k2eAgMnPc4d1	považovaný
Žeberská	Žeberský	k2eAgFnSc1d1	Žeberská
lípa	lípa	k1gFnSc1	lípa
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
700	[number]	k4	700
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lípa	lípa	k1gFnSc1	lípa
v	v	k7c6	v
Lipce	lipka	k1gFnSc6	lipka
(	(	kIx(	(
<g/>
600	[number]	k4	600
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lípa	lípa	k1gFnSc1	lípa
na	na	k7c4	na
Babí	babí	k2eAgInPc4d1	babí
(	(	kIx(	(
<g/>
550	[number]	k4	550
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Maškovická	Maškovický	k2eAgFnSc1d1	Maškovický
lípa	lípa	k1gFnSc1	lípa
(	(	kIx(	(
<g/>
500	[number]	k4	500
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
Horní	horní	k2eAgFnSc1d1	horní
Popovská	popovský	k2eAgFnSc1d1	Popovská
lípa	lípa	k1gFnSc1	lípa
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
400	[number]	k4	400
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
