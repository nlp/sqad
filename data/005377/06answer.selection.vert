<s>
Treska	treska	k1gFnSc1	treska
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Gadus	Gadus	k1gMnSc1	Gadus
morhua	morhua	k1gMnSc1	morhua
<g/>
,	,	kIx,	,
Linnaeus	Linnaeus	k1gMnSc1	Linnaeus
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mořská	mořský	k2eAgFnSc1d1	mořská
ryba	ryba	k1gFnSc1	ryba
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
treskovitých	treskovití	k1gMnPc2	treskovití
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
může	moct	k5eAaImIp3nS	moct
dorůstat	dorůstat	k5eAaImF	dorůstat
velikosti	velikost	k1gFnSc3	velikost
až	až	k9	až
2	[number]	k4	2
m	m	kA	m
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
90	[number]	k4	90
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
