<s>
Treska	treska	k1gFnSc1	treska
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Gadus	Gadus	k1gMnSc1	Gadus
morhua	morhua	k1gMnSc1	morhua
<g/>
,	,	kIx,	,
Linnaeus	Linnaeus	k1gMnSc1	Linnaeus
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mořská	mořský	k2eAgFnSc1d1	mořská
ryba	ryba	k1gFnSc1	ryba
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
treskovitých	treskovití	k1gMnPc2	treskovití
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
dorůstat	dorůstat	k5eAaImF	dorůstat
velikosti	velikost	k1gFnSc3	velikost
až	až	k9	až
2	[number]	k4	2
m	m	kA	m
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
90	[number]	k4	90
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
silnou	silný	k2eAgFnSc4d1	silná
a	a	k8xC	a
mohutnou	mohutný	k2eAgFnSc4d1	mohutná
rybu	ryba	k1gFnSc4	ryba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
hejnech	hejno	k1gNnPc6	hejno
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
nad	nad	k7c7	nad
pevninským	pevninský	k2eAgInSc7d1	pevninský
šelfem	šelf	k1gInSc7	šelf
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
mezi	mezi	k7c7	mezi
30	[number]	k4	30
až	až	k9	až
80	[number]	k4	80
metry	metr	k1gInPc4	metr
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
svoji	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
malých	malý	k2eAgFnPc2d1	malá
ryb	ryba	k1gFnPc2	ryba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sledi	sleď	k1gMnPc1	sleď
<g/>
)	)	kIx)	)
žijících	žijící	k2eAgInPc2d1	žijící
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
hejnech	hejno	k1gNnPc6	hejno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ideálním	ideální	k2eAgInSc6d1	ideální
stavu	stav	k1gInSc6	stav
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
treska	treska	k1gFnSc1	treska
obecná	obecná	k1gFnSc1	obecná
dožít	dožít	k5eAaPmF	dožít
až	až	k9	až
60	[number]	k4	60
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
moderní	moderní	k2eAgInSc1d1	moderní
intenzivní	intenzivní	k2eAgInSc1d1	intenzivní
rybolov	rybolov	k1gInSc1	rybolov
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
tresky	treska	k1gFnPc1	treska
jsou	být	k5eAaImIp3nP	být
uloveny	ulovit	k5eAaPmNgFnP	ulovit
mnohem	mnohem	k6eAd1	mnohem
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
tohoto	tento	k3xDgInSc2	tento
věku	věk	k1gInSc2	věk
či	či	k8xC	či
maximální	maximální	k2eAgFnSc2d1	maximální
možné	možný	k2eAgFnSc2d1	možná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
loveny	loven	k2eAgFnPc1d1	lovena
tresky	treska	k1gFnPc1	treska
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
okolo	okolo	k7c2	okolo
11	[number]	k4	11
kg	kg	kA	kg
a	a	k8xC	a
jedinci	jedinec	k1gMnPc1	jedinec
nad	nad	k7c7	nad
15	[number]	k4	15
kg	kg	kA	kg
jsou	být	k5eAaImIp3nP	být
vzácní	vzácný	k2eAgMnPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
chladné	chladný	k2eAgFnSc2d1	chladná
vody	voda	k1gFnSc2	voda
mírného	mírný	k2eAgNnSc2d1	mírné
pásma	pásmo	k1gNnSc2	pásmo
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
oblastech	oblast	k1gFnPc6	oblast
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
rybolovem	rybolov	k1gInSc7	rybolov
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
silně	silně	k6eAd1	silně
zdecimoval	zdecimovat	k5eAaPmAgInS	zdecimovat
její	její	k3xOp3gFnSc4	její
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
populaci	populace	k1gFnSc4	populace
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
hospodářsky	hospodářsky	k6eAd1	hospodářsky
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
mořskou	mořský	k2eAgFnSc4d1	mořská
rybu	ryba	k1gFnSc4	ryba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
lovena	lovit	k5eAaImNgFnS	lovit
<g/>
.	.	kIx.	.
</s>
<s>
Treska	treska	k1gFnSc1	treska
obecná	obecná	k1gFnSc1	obecná
obývá	obývat	k5eAaImIp3nS	obývat
chladnější	chladný	k2eAgFnSc2d2	chladnější
vody	voda	k1gFnSc2	voda
severního	severní	k2eAgInSc2d1	severní
Atlantického	atlantický	k2eAgInSc2d1	atlantický
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
USA	USA	kA	USA
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
,	,	kIx,	,
Islandu	Island	k1gInSc2	Island
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
Finska	Finsko	k1gNnSc2	Finsko
a	a	k8xC	a
části	část	k1gFnSc2	část
evropského	evropský	k2eAgNnSc2d1	Evropské
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
Baltském	baltský	k2eAgNnSc6d1	Baltské
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
proniká	pronikat	k5eAaImIp3nS	pronikat
z	z	k7c2	z
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
populace	populace	k1gFnPc1	populace
tresky	treska	k1gFnSc2	treska
obecné	obecná	k1gFnSc2	obecná
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
několika	několik	k4yIc6	několik
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
těmito	tento	k3xDgFnPc7	tento
oblastmi	oblast	k1gFnPc7	oblast
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Severní	severní	k2eAgNnSc1d1	severní
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
severovýchodní	severovýchodní	k2eAgInSc1d1	severovýchodní
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
Labradorské	labradorský	k2eAgNnSc1d1	labradorský
moře	moře	k1gNnSc1	moře
a	a	k8xC	a
u	u	k7c2	u
Grand	grand	k1gMnSc1	grand
Banks	Banks	k1gInSc1	Banks
<g/>
.	.	kIx.	.
</s>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
vody	voda	k1gFnPc4	voda
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
přibližně	přibližně	k6eAd1	přibližně
600	[number]	k4	600
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
mořskou	mořský	k2eAgFnSc7d1	mořská
hladinou	hladina	k1gFnSc7	hladina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
150	[number]	k4	150
až	až	k9	až
200	[number]	k4	200
m	m	kA	m
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Preferuje	preferovat	k5eAaImIp3nS	preferovat
teploty	teplota	k1gFnPc4	teplota
vody	voda	k1gFnSc2	voda
mezi	mezi	k7c7	mezi
2	[number]	k4	2
-	-	kIx~	-
15	[number]	k4	15
°	°	k?	°
<g/>
C.	C.	kA	C.
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
od	od	k7c2	od
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
vod	voda	k1gFnPc2	voda
po	po	k7c6	po
oblasti	oblast	k1gFnSc6	oblast
kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
šelfu	šelf	k1gInSc2	šelf
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
tresky	treska	k1gFnSc2	treska
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
jak	jak	k6eAd1	jak
podle	podle	k7c2	podle
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
jejího	její	k3xOp3gInSc2	její
výskytu	výskyt	k1gInSc2	výskyt
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
treska	treska	k1gFnSc1	treska
menší	malý	k2eAgFnSc1d2	menší
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
rostoucí	rostoucí	k2eAgFnSc6d1	rostoucí
severní	severní	k2eAgFnSc6d1	severní
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
šířce	šířka	k1gFnSc6	šířka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
treska	treska	k1gFnSc1	treska
okolo	okolo	k7c2	okolo
2	[number]	k4	2
m	m	kA	m
a	a	k8xC	a
maximální	maximální	k2eAgFnSc2d1	maximální
hmotnosti	hmotnost	k1gFnSc2	hmotnost
90	[number]	k4	90
kg	kg	kA	kg
(	(	kIx(	(
<g/>
jiný	jiný	k2eAgInSc1d1	jiný
zdroj	zdroj	k1gInSc1	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
až	až	k9	až
96	[number]	k4	96
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
délky	délka	k1gFnPc4	délka
okolo	okolo	k7c2	okolo
80	[number]	k4	80
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Treska	treska	k1gFnSc1	treska
má	mít	k5eAaImIp3nS	mít
mohutnou	mohutný	k2eAgFnSc4d1	mohutná
hlavu	hlava	k1gFnSc4	hlava
s	s	k7c7	s
předsunutou	předsunutý	k2eAgFnSc7d1	předsunutá
horní	horní	k2eAgFnSc7d1	horní
čelistí	čelist	k1gFnSc7	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
čelisti	čelist	k1gFnSc6	čelist
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
vousek	vousek	k1gInSc1	vousek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyčnívá	vyčnívat	k5eAaImIp3nS	vyčnívat
<g/>
,	,	kIx,	,
čelist	čelist	k1gFnSc1	čelist
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
drobnými	drobný	k2eAgInPc7d1	drobný
zuby	zub	k1gInPc7	zub
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
jsou	být	k5eAaImIp3nP	být
rozmístěny	rozmístit	k5eAaPmNgFnP	rozmístit
tři	tři	k4xCgFnPc1	tři
hřbetní	hřbetní	k2eAgFnPc1d1	hřbetní
ploutve	ploutev	k1gFnPc1	ploutev
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
řitní	řitní	k2eAgFnPc1d1	řitní
ploutve	ploutev	k1gFnPc1	ploutev
zaobleného	zaoblený	k2eAgInSc2d1	zaoblený
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ocasní	ocasní	k2eAgFnSc1d1	ocasní
ploutev	ploutev	k1gFnSc1	ploutev
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
malými	malý	k2eAgFnPc7d1	malá
podlouhlými	podlouhlý	k2eAgFnPc7d1	podlouhlá
šupinami	šupina	k1gFnPc7	šupina
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
různá	různý	k2eAgFnSc1d1	různá
a	a	k8xC	a
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
jsou	být	k5eAaImIp3nP	být
ryby	ryba	k1gFnPc1	ryba
načervenalé	načervenalý	k2eAgFnPc1d1	načervenalá
či	či	k8xC	či
skvrnitě	skvrnitě	k6eAd1	skvrnitě
hnědé	hnědý	k2eAgFnSc6d1	hnědá
s	s	k7c7	s
dobře	dobře	k6eAd1	dobře
viditelnou	viditelný	k2eAgFnSc7d1	viditelná
bílou	bílý	k2eAgFnSc7d1	bílá
postranní	postranní	k2eAgFnSc7d1	postranní
čárou	čára	k1gFnSc7	čára
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
tresky	treska	k1gFnSc2	treska
za	za	k7c2	za
pohybu	pohyb	k1gInSc2	pohyb
vydávají	vydávat	k5eAaPmIp3nP	vydávat
zvuky	zvuk	k1gInPc1	zvuk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zní	znět	k5eAaImIp3nS	znět
jako	jako	k8xC	jako
chrochtání	chrochtání	k1gNnSc4	chrochtání
<g/>
.	.	kIx.	.
</s>
<s>
Treska	treska	k1gFnSc1	treska
obecná	obecná	k1gFnSc1	obecná
se	se	k3xPyFc4	se
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
života	život	k1gInSc2	život
živí	živit	k5eAaImIp3nP	živit
planktonem	plankton	k1gInSc7	plankton
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zní	znět	k5eAaImIp3nS	znět
stává	stávat	k5eAaImIp3nS	stávat
dravá	dravý	k2eAgFnSc1d1	dravá
ryba	ryba	k1gFnSc1	ryba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pojídá	pojídat	k5eAaImIp3nS	pojídat
mnohoštětinatce	mnohoštětinatec	k1gMnPc4	mnohoštětinatec
<g/>
,	,	kIx,	,
korýše	korýš	k1gMnPc4	korýš
<g/>
,	,	kIx,	,
měkkýše	měkkýš	k1gMnPc4	měkkýš
a	a	k8xC	a
menší	malý	k2eAgMnPc4d2	menší
druhy	druh	k1gMnPc4	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Tření	tření	k1gNnSc1	tření
tresky	treska	k1gFnSc2	treska
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
jarních	jarní	k2eAgInPc6d1	jarní
měsících	měsíc	k1gInPc6	měsíc
od	od	k7c2	od
února	únor	k1gInSc2	únor
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
pouze	pouze	k6eAd1	pouze
mezi	mezi	k7c7	mezi
4	[number]	k4	4
až	až	k9	až
6	[number]	k4	6
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
100	[number]	k4	100
až	až	k9	až
200	[number]	k4	200
m.	m.	k?	m.
Jedinci	jedinec	k1gMnPc1	jedinec
táhnou	táhnout	k5eAaImIp3nP	táhnout
do	do	k7c2	do
míst	místo	k1gNnPc2	místo
tření	tření	k1gNnSc2	tření
až	až	k6eAd1	až
tisíce	tisíc	k4xCgInSc2	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
následně	následně	k6eAd1	následně
samice	samice	k1gFnSc1	samice
vypustí	vypustit	k5eAaPmIp3nS	vypustit
několik	několik	k4yIc4	několik
miliónů	milión	k4xCgInPc2	milión
jiker	jikra	k1gFnPc2	jikra
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
0,5	[number]	k4	0,5
až	až	k9	až
9,5	[number]	k4	9,5
milióny	milión	k4xCgInPc1	milión
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
velikost	velikost	k1gFnSc4	velikost
okolo	okolo	k7c2	okolo
1,5	[number]	k4	1,5
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
oplodněných	oplodněný	k2eAgNnPc2d1	oplodněné
vajíček	vajíčko	k1gNnPc2	vajíčko
se	se	k3xPyFc4	se
za	za	k7c2	za
2	[number]	k4	2
až	až	k9	až
4	[number]	k4	4
týdny	týden	k1gInPc4	týden
vylíhnou	vylíhnout	k5eAaPmIp3nP	vylíhnout
mladé	mladý	k2eAgFnPc4d1	mladá
rybky	rybka	k1gFnPc4	rybka
žijící	žijící	k2eAgFnPc4d1	žijící
u	u	k7c2	u
dna	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
dospělosti	dospělost	k1gFnSc2	dospělost
treska	treska	k1gFnSc1	treska
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
již	již	k6eAd1	již
během	během	k7c2	během
2	[number]	k4	2
až	až	k9	až
4	[number]	k4	4
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Barentsova	Barentsův	k2eAgNnSc2d1	Barentsovo
a	a	k8xC	a
Islandského	islandský	k2eAgNnSc2d1	islandské
moře	moře	k1gNnSc2	moře
ale	ale	k8xC	ale
až	až	k9	až
za	za	k7c4	za
5	[number]	k4	5
až	až	k9	až
7	[number]	k4	7
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tresky	treska	k1gFnPc1	treska
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
několika	několik	k4yIc6	několik
vzájemně	vzájemně	k6eAd1	vzájemně
oddělených	oddělený	k2eAgFnPc6d1	oddělená
populacích	populace	k1gFnPc6	populace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
oblastech	oblast	k1gFnPc6	oblast
oceánu	oceán	k1gInSc2	oceán
v	v	k7c6	v
hloubkách	hloubka	k1gFnPc6	hloubka
okolo	okolo	k7c2	okolo
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Treska	treska	k1gFnSc1	treska
obecná	obecná	k1gFnSc1	obecná
je	být	k5eAaImIp3nS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
rybou	ryba	k1gFnSc7	ryba
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
rybolovu	rybolov	k1gInSc6	rybolov
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
hojně	hojně	k6eAd1	hojně
lovena	lovit	k5eAaImNgFnS	lovit
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gNnPc1	její
hejna	hejno	k1gNnPc1	hejno
jsou	být	k5eAaImIp3nP	být
nevyčerpatelná	vyčerpatelný	k2eNgNnPc1d1	nevyčerpatelné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
její	její	k3xOp3gFnSc2	její
populace	populace	k1gFnSc2	populace
rapidně	rapidně	k6eAd1	rapidně
prořídla	prořídnout	k5eAaPmAgFnS	prořídnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
probíhají	probíhat	k5eAaImIp3nP	probíhat
snahy	snaha	k1gFnPc1	snaha
o	o	k7c6	o
stabilizaci	stabilizace	k1gFnSc6	stabilizace
populací	populace	k1gFnPc2	populace
a	a	k8xC	a
o	o	k7c4	o
trvale	trvale	k6eAd1	trvale
udržitelný	udržitelný	k2eAgInSc4d1	udržitelný
rybolov	rybolov	k1gInSc4	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
oblasti	oblast	k1gFnPc1	oblast
okolo	okolo	k7c2	okolo
Ameriky	Amerika	k1gFnSc2	Amerika
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
i	i	k9	i
nadále	nadále	k6eAd1	nadále
nadměrně	nadměrně	k6eAd1	nadměrně
loveny	loven	k2eAgFnPc1d1	lovena
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
nepříznivý	příznivý	k2eNgInSc4d1	nepříznivý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
obnovu	obnova	k1gFnSc4	obnova
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Trvalý	trvalý	k2eAgInSc1d1	trvalý
tlak	tlak	k1gInSc1	tlak
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
amerického	americký	k2eAgNnSc2d1	americké
pobřeží	pobřeží	k1gNnSc2	pobřeží
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
na	na	k7c6	na
věku	věk	k1gInSc6	věk
a	a	k8xC	a
velikosti	velikost	k1gFnSc6	velikost
ulovených	ulovený	k2eAgFnPc2d1	ulovená
tresek	treska	k1gFnPc2	treska
<g/>
.	.	kIx.	.
</s>
<s>
Panují	panovat	k5eAaImIp3nP	panovat
obavy	obava	k1gFnPc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
negativně	negativně	k6eAd1	negativně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
budoucí	budoucí	k2eAgFnSc2d1	budoucí
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
schopnost	schopnost	k1gFnSc1	schopnost
regenerace	regenerace	k1gFnSc2	regenerace
početních	početní	k2eAgInPc2d1	početní
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
se	se	k3xPyFc4	se
již	již	k6eAd1	již
dokonce	dokonce	k9	dokonce
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
treska	treska	k1gFnSc1	treska
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
vyhynutí	vyhynutí	k1gNnSc2	vyhynutí
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
bílé	bílý	k2eAgNnSc1d1	bílé
maso	maso	k1gNnSc1	maso
je	být	k5eAaImIp3nS	být
připravováno	připravovat	k5eAaImNgNnS	připravovat
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
způsobů	způsob	k1gInPc2	způsob
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
a	a	k8xC	a
oceňované	oceňovaný	k2eAgNnSc1d1	oceňované
jako	jako	k8xS	jako
chutné	chutný	k2eAgNnSc1d1	chutné
<g/>
.	.	kIx.	.
</s>
<s>
Distribuuje	distribuovat	k5eAaBmIp3nS	distribuovat
se	se	k3xPyFc4	se
čerstvá	čerstvý	k2eAgFnSc1d1	čerstvá
<g/>
,	,	kIx,	,
mražená	mražený	k2eAgFnSc1d1	mražená
<g/>
,	,	kIx,	,
uzená	uzený	k2eAgFnSc1d1	uzená
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
filé	filé	k1gNnPc4	filé
či	či	k8xC	či
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
jejich	jejich	k3xOp3gNnPc2	jejich
jater	játra	k1gNnPc2	játra
vyráběn	vyráběn	k2eAgInSc1d1	vyráběn
rybí	rybí	k2eAgInSc4d1	rybí
olej	olej	k1gInSc4	olej
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
části	část	k1gFnPc1	část
těla	tělo	k1gNnSc2	tělo
jako	jako	k8xC	jako
kůže	kůže	k1gFnSc2	kůže
či	či	k8xC	či
vzduchový	vzduchový	k2eAgInSc1d1	vzduchový
měchýř	měchýř	k1gInSc1	měchýř
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
klihu	klih	k1gInSc2	klih
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nutričních	nutriční	k2eAgInPc2d1	nutriční
parametrů	parametr	k1gInPc2	parametr
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
v	v	k7c6	v
mase	maso	k1gNnSc6	maso
tresky	treska	k1gFnSc2	treska
<g/>
.	.	kIx.	.
</s>
