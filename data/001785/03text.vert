<s>
Coursing	Coursing	k1gInSc1	Coursing
je	být	k5eAaImIp3nS	být
kynologický	kynologický	k2eAgInSc1d1	kynologický
sport	sport	k1gInSc1	sport
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
touze	touha	k1gFnSc6	touha
psa	pes	k1gMnSc2	pes
lovit	lovit	k5eAaImF	lovit
<g/>
.	.	kIx.	.
</s>
<s>
Běhá	běhat	k5eAaImIp3nS	běhat
se	se	k3xPyFc4	se
v	v	k7c6	v
přirozeném	přirozený	k2eAgInSc6d1	přirozený
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
svažitém	svažitý	k2eAgInSc6d1	svažitý
terénu	terén	k1gInSc6	terén
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
přírodními	přírodní	k2eAgFnPc7d1	přírodní
nebo	nebo	k8xC	nebo
umělými	umělý	k2eAgFnPc7d1	umělá
překážkami	překážka	k1gFnPc7	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
psa	pes	k1gMnSc2	pes
při	při	k7c6	při
coursingu	coursing	k1gInSc6	coursing
je	být	k5eAaImIp3nS	být
ulovit	ulovit	k5eAaPmF	ulovit
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
tažená	tažený	k2eAgFnSc1d1	tažená
navijákem	naviják	k1gInSc7	naviják
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
návnada	návnada	k1gFnSc1	návnada
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
střapce	střapec	k1gInSc2	střapec
z	z	k7c2	z
igelitových	igelitový	k2eAgInPc2d1	igelitový
pásků	pásek	k1gInPc2	pásek
nebo	nebo	k8xC	nebo
králičí	králičí	k2eAgFnSc2d1	králičí
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tažena	táhnout	k5eAaImNgFnS	táhnout
lankem	lanko	k1gNnSc7	lanko
mezi	mezi	k7c7	mezi
kladkami	kladka	k1gFnPc7	kladka
po	po	k7c6	po
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
simuluje	simulovat	k5eAaImIp3nS	simulovat
skutečný	skutečný	k2eAgInSc4d1	skutečný
běh	běh	k1gInSc4	běh
zajíce	zajíc	k1gMnSc2	zajíc
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
dvoukolový	dvoukolový	k2eAgInSc1d1	dvoukolový
závod	závod	k1gInSc1	závod
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
trať	trať	k1gFnSc1	trať
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
cca	cca	kA	cca
548	[number]	k4	548
-	-	kIx~	-
914	[number]	k4	914
m	m	kA	m
(	(	kIx(	(
<g/>
600	[number]	k4	600
až	až	k9	až
1000	[number]	k4	1000
yardů	yard	k1gInPc2	yard
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
psi	pes	k1gMnPc1	pes
prokázali	prokázat	k5eAaPmAgMnP	prokázat
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
štvavost	štvavost	k1gFnSc4	štvavost
(	(	kIx(	(
<g/>
při	při	k7c6	při
smečce	smečka	k1gFnSc6	smečka
i	i	k8xC	i
spolupráci	spolupráce	k1gFnSc6	spolupráce
<g/>
)	)	kIx)	)
a	a	k8xC	a
inteligenci	inteligence	k1gFnSc4	inteligence
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
návnadě	návnada	k1gFnSc6	návnada
různě	různě	k6eAd1	různě
nadbíhají	nadbíhat	k5eAaImIp3nP	nadbíhat
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgInSc1d1	maximální
počet	počet	k1gInSc1	počet
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
lze	lze	k6eAd1	lze
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
kole	kolo	k1gNnSc6	kolo
získat	získat	k5eAaPmF	získat
je	on	k3xPp3gFnPc4	on
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgInSc1d1	minimální
počet	počet	k1gInSc1	počet
bodů	bod	k1gInPc2	bod
nutný	nutný	k2eAgMnSc1d1	nutný
pro	pro	k7c4	pro
postup	postup	k1gInSc4	postup
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
je	být	k5eAaImIp3nS	být
50	[number]	k4	50
<g/>
.	.	kIx.	.
</s>
<s>
Coursing	Coursing	k1gInSc1	Coursing
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
oblíbenou	oblíbený	k2eAgFnSc4d1	oblíbená
aktivitou	aktivita	k1gFnSc7	aktivita
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
již	již	k6eAd1	již
pouze	pouze	k6eAd1	pouze
doménou	doména	k1gFnSc7	doména
chrtích	chrtí	k2eAgFnPc2d1	chrtí
plemen	plemeno	k1gNnPc2	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ideální	ideální	k2eAgNnSc4d1	ideální
vybití	vybití	k1gNnSc4	vybití
energie	energie	k1gFnSc2	energie
živějších	živý	k2eAgNnPc2d2	živější
plemen	plemeno	k1gNnPc2	plemeno
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
coursingu	coursing	k1gInSc2	coursing
<g/>
:	:	kIx,	:
Lure	Lure	k1gInSc1	Lure
coursing	coursing	k1gInSc1	coursing
-	-	kIx~	-
běh	běh	k1gInSc1	běh
za	za	k7c7	za
umělou	umělý	k2eAgFnSc7d1	umělá
návnadou	návnada	k1gFnSc7	návnada
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
střapcem	střapec	k1gInSc7	střapec
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
jako	jako	k8xS	jako
náhrada	náhrada	k1gFnSc1	náhrada
hare	har	k1gFnSc2	har
coursingu	coursing	k1gInSc2	coursing
<g/>
.	.	kIx.	.
</s>
<s>
Hare	Hare	k1gInSc1	Hare
coursing	coursing	k1gInSc1	coursing
(	(	kIx(	(
<g/>
též	též	k9	též
Open	Open	k1gInSc1	Open
field	fielda	k1gFnPc2	fielda
coursing	coursing	k1gInSc1	coursing
<g/>
,	,	kIx,	,
Live	Live	k1gInSc1	Live
coursing	coursing	k1gInSc1	coursing
<g/>
)	)	kIx)	)
-	-	kIx~	-
běh	běh	k1gInSc1	běh
za	za	k7c7	za
živým	živý	k1gMnSc7	živý
zajícem	zajíc	k1gMnSc7	zajíc
<g/>
,	,	kIx,	,
krysou	krysa	k1gFnSc7	krysa
nebo	nebo	k8xC	nebo
kočkou	kočka	k1gFnSc7	kočka
<g/>
,	,	kIx,	,
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
zakázaný	zakázaný	k2eAgInSc4d1	zakázaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
legální	legální	k2eAgFnSc1d1	legální
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
znám	znám	k2eAgInSc1d1	znám
jako	jako	k9	jako
"	"	kIx"	"
<g/>
zkouška	zkouška	k1gFnSc1	zkouška
ostrosti	ostrost	k1gFnSc2	ostrost
<g/>
"	"	kIx"	"
prováděná	prováděný	k2eAgFnSc1d1	prováděná
u	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
loveckých	lovecký	k2eAgNnPc2d1	lovecké
plemen	plemeno	k1gNnPc2	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Coursing	Coursing	k1gInSc1	Coursing
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
NorthWind	NorthWind	k1gMnSc1	NorthWind
Club	club	k1gInSc4	club
Aktuální	aktuální	k2eAgNnSc4d1	aktuální
dění	dění	k1gNnSc4	dění
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
coursingu	coursing	k1gInSc2	coursing
Český	český	k2eAgInSc1d1	český
coursingový	coursingový	k2eAgInSc1d1	coursingový
klub	klub	k1gInSc1	klub
Moravskoslezský	moravskoslezský	k2eAgInSc1d1	moravskoslezský
coursingový	coursingový	k2eAgInSc1d1	coursingový
klub	klub	k1gInSc1	klub
Coursing	Coursing	k1gInSc1	Coursing
Louny	Louny	k1gInPc1	Louny
o.s.	o.s.	k?	o.s.
</s>
