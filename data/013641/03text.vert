<s>
Maďarský	maďarský	k2eAgInSc1d1
forint	forint	k1gInSc1
</s>
<s>
Maďarský	maďarský	k2eAgInSc1d1
forintMagyar	forintMagyar	k1gInSc1
forint	forint	k1gInSc1
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
<g/>
)	)	kIx)
<g/>
Země	země	k1gFnSc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
Maďarsko	Maďarsko	k1gNnSc1
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
HUF	huf	k0
Inflace	inflace	k1gFnSc1
</s>
<s>
-0,1	-0,1	k4
<g/>
%	%	kIx~
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2015	#num#	k4
odhad	odhad	k1gInSc1
<g/>
)	)	kIx)
Symbol	symbol	k1gInSc1
</s>
<s>
Ft	Ft	k?
Dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
filler	filler	k1gInSc1
Mince	mince	k1gFnSc2
</s>
<s>
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
a	a	k8xC
200	#num#	k4
Ft	Ft	k1gMnPc2
Bankovky	bankovka	k1gFnSc2
</s>
<s>
500	#num#	k4
<g/>
,	,	kIx,
1000	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
5000	#num#	k4
<g/>
,	,	kIx,
10000	#num#	k4
<g/>
,	,	kIx,
20000	#num#	k4
Ft	Ft	k1gFnPc2
</s>
<s>
Maďarský	maďarský	k2eAgInSc1d1
forint	forint	k1gInSc1
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
jen	jen	k9
forint	forint	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
maďarská	maďarský	k2eAgFnSc1d1
měna	měna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc1
ISO	ISO	kA
4217	#num#	k4
kód	kód	k1gInSc1
je	být	k5eAaImIp3nS
HUF	HUF	kA
<g/>
,	,	kIx,
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
se	se	k3xPyFc4
běžně	běžně	k6eAd1
užívá	užívat	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
Ft	Ft	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
forint	forint	k1gInSc1
sestává	sestávat	k5eAaImIp3nS
ze	z	k7c2
sta	sto	k4xCgNnSc2
fillérů	fillér	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kupní	kupní	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
fillérů	fillér	k1gMnPc2
je	být	k5eAaImIp3nS
natolik	natolik	k6eAd1
nízká	nízký	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
nejnižší	nízký	k2eAgFnSc7d3
používanou	používaný	k2eAgFnSc7d1
mincí	mince	k1gFnSc7
je	být	k5eAaImIp3nS
5	#num#	k4
forintů	forint	k1gInPc2
a	a	k8xC
neexistují	existovat	k5eNaImIp3nP
žádné	žádný	k3yNgFnPc4
fillerové	fillerové	k2eAgFnPc4d1
mince	mince	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
forint	forint	k1gInSc4
pochází	pocházet	k5eAaImIp3nS
od	od	k7c2
jména	jméno	k1gNnSc2
italského	italský	k2eAgNnSc2d1
města	město	k1gNnSc2
Florencie	Florencie	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
tam	tam	k6eAd1
razily	razit	k5eAaImAgFnP
mince	mince	k1gFnPc4
s	s	k7c7
názvem	název	k1gInSc7
fiorino	fiorino	k1gNnSc4
d	d	k?
<g/>
'	'	kIx"
<g/>
oro	oro	k?
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
a	a	k8xC
vývoj	vývoj	k1gInSc1
</s>
<s>
Předchůdci	předchůdce	k1gMnPc1
dnešního	dnešní	k2eAgInSc2d1
forintu	forint	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
Rakousko-uherská	rakousko-uherský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
(	(	kIx(
<g/>
1892	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Maďarská	maďarský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
-	-	kIx~
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Maďarské	maďarský	k2eAgNnSc1d1
pengő	pengő	k?
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Do	do	k7c2
oběhu	oběh	k1gInSc2
byl	být	k5eAaImAgInS
forint	forint	k1gInSc1
zaveden	zaveden	k2eAgInSc1d1
dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1946	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
inflace	inflace	k1gFnSc1
znehodnotila	znehodnotit	k5eAaPmAgFnS
předchozí	předchozí	k2eAgFnSc4d1
měnu	měna	k1gFnSc4
s	s	k7c7
názvem	název	k1gInSc7
pengő	pengő	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Forinty	forint	k1gInPc7
zavedla	zavést	k5eAaPmAgFnS
již	již	k9
tehdy	tehdy	k6eAd1
nová	nový	k2eAgFnSc1d1
komunistická	komunistický	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
<g/>
,	,	kIx,
stará	starý	k2eAgFnSc1d1
měna	měna	k1gFnSc1
na	na	k7c4
ně	on	k3xPp3gInPc4
byla	být	k5eAaImAgFnS
převedena	převést	k5eAaPmNgFnS
v	v	k7c6
poměru	poměr	k1gInSc6
1	#num#	k4
forint	forint	k1gInSc1
=	=	kIx~
4	#num#	k4
<g/>
×	×	k?
<g/>
1029	#num#	k4
pengő	pengő	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
60	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
byla	být	k5eAaImAgFnS
hodnota	hodnota	k1gFnSc1
forintu	forint	k1gInSc2
poměrně	poměrně	k6eAd1
stabilní	stabilní	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
a	a	k8xC
po	po	k7c6
pádu	pád	k1gInSc6
socialismu	socialismus	k1gInSc2
se	se	k3xPyFc4
situace	situace	k1gFnSc1
změnila	změnit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inflace	inflace	k1gFnSc1
vzrostla	vzrůst	k5eAaPmAgFnS
až	až	k9
na	na	k7c4
35	#num#	k4
%	%	kIx~
ročně	ročně	k6eAd1
a	a	k8xC
hodnota	hodnota	k1gFnSc1
měny	měna	k1gFnSc2
tak	tak	k6eAd1
začala	začít	k5eAaPmAgFnS
klesat	klesat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
vstoupilo	vstoupit	k5eAaPmAgNnS
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2004	#num#	k4
do	do	k7c2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
ze	z	k7c2
závazků	závazek	k1gInPc2
vyplývajících	vyplývající	k2eAgInPc2d1
z	z	k7c2
přistoupení	přistoupení	k1gNnSc2
k	k	k7c3
EU	EU	kA
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
vstupující	vstupující	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
,	,	kIx,
až	až	k8xS
splní	splnit	k5eAaPmIp3nP
všechny	všechen	k3xTgFnPc4
podmínky	podmínka	k1gFnPc4
<g/>
,	,	kIx,
zavede	zavést	k5eAaPmIp3nS
místo	místo	k7c2
své	svůj	k3xOyFgFnSc2
národní	národní	k2eAgFnSc2d1
měny	měna	k1gFnSc2
společnou	společný	k2eAgFnSc4d1
evropskou	evropský	k2eAgFnSc4d1
měnu	měna	k1gFnSc4
euro	euro	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maďarsko	Maďarsko	k1gNnSc1
hodlalo	hodlat	k5eAaImAgNnS
euro	euro	k1gNnSc4
zavést	zavést	k5eAaPmF
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
deklaroval	deklarovat	k5eAaBmAgMnS
maďarský	maďarský	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
hospodářství	hospodářství	k1gNnSc2
možnost	možnost	k1gFnSc1
přijetí	přijetí	k1gNnSc2
eura	euro	k1gNnSc2
do	do	k7c2
konce	konec	k1gInSc2
desetiletí	desetiletí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
prohlásil	prohlásit	k5eAaPmAgMnS
guvernér	guvernér	k1gMnSc1
maďarské	maďarský	k2eAgFnSc2d1
centrální	centrální	k2eAgFnSc2d1
banky	banka	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
euro	euro	k1gNnSc4
byla	být	k5eAaImAgFnS
chyba	chyba	k1gFnSc1
a	a	k8xC
že	že	k8xS
si	se	k3xPyFc3
Maďarsko	Maďarsko	k1gNnSc4
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
Česko	Česko	k1gNnSc4
<g/>
,	,	kIx,
nestanovilo	stanovit	k5eNaPmAgNnS
konkrétní	konkrétní	k2eAgInSc4d1
termín	termín	k1gInSc4
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
přijetí	přijetí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mince	mince	k1gFnSc1
</s>
<s>
Současné	současný	k2eAgFnPc1d1
mince	mince	k1gFnPc1
mají	mít	k5eAaImIp3nP
nominální	nominální	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
a	a	k8xC
200	#num#	k4
forintů	forint	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
líci	líc	k1gInSc6
je	být	k5eAaImIp3nS
státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
nebo	nebo	k8xC
ukázka	ukázka	k1gFnSc1
flóry	flóra	k1gFnSc2
a	a	k8xC
fauny	fauna	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
rubu	rub	k1gInSc6
je	být	k5eAaImIp3nS
číslovka	číslovka	k1gFnSc1
nominální	nominální	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mince	mince	k1gFnSc1
v	v	k7c6
nominální	nominální	k2eAgFnSc6d1
hodnotě	hodnota	k1gFnSc6
1	#num#	k4
a	a	k8xC
2	#num#	k4
forinty	forint	k1gInPc4
byly	být	k5eAaImAgFnP
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2008	#num#	k4
stažené	stažený	k2eAgNnSc4d1
z	z	k7c2
oběhu	oběh	k1gInSc2
kvůli	kvůli	k7c3
nízké	nízký	k2eAgFnSc3d1
kupní	kupní	k2eAgFnSc3d1
ceně	cena	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
mince	mince	k1gFnSc1
</s>
<s>
Pamětní	pamětní	k2eAgFnPc1d1
mince	mince	k1gFnPc1
jsou	být	k5eAaImIp3nP
od	od	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
raženy	ražen	k2eAgMnPc4d1
z	z	k7c2
obecných	obecný	k2eAgInPc2d1
i	i	k8xC
drahých	drahý	k2eAgInPc2d1
kovů	kov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevily	objevit	k5eAaPmAgInP
se	se	k3xPyFc4
i	i	k9
neobvyklé	obvyklý	k2eNgFnPc4d1
nominální	nominální	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
75	#num#	k4
forintů	forint	k1gInPc2
k	k	k7c3
75	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc1
Magyar	Magyara	k1gFnPc2
Nemzeti	Nemzeti	k1gFnPc2
Bank	bank	k1gInSc1
nebo	nebo	k8xC
750	#num#	k4
forintů	forint	k1gInPc2
k	k	k7c3
MS	MS	kA
ve	v	k7c6
fotbale	fotbal	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
pamětní	pamětní	k2eAgFnSc2d1
mince	mince	k1gFnSc2
mají	mít	k5eAaImIp3nP
i	i	k9
neobvyklý	obvyklý	k2eNgInSc4d1
tvar	tvar	k1gInSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
čtverec	čtverec	k1gInSc1
(	(	kIx(
<g/>
500	#num#	k4
forintů	forint	k1gInPc2
připomínající	připomínající	k2eAgInSc1d1
šachový	šachový	k2eAgInSc1d1
stroj	stroj	k1gInSc1
Farkase	Farkasa	k1gFnSc6
Kempelena	Kempelen	k2eAgNnPc1d1
nebo	nebo	k8xC
Rubikovu	Rubikův	k2eAgFnSc4d1
kostku	kostka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Existují	existovat	k5eAaImIp3nP
také	také	k9
mince	mince	k1gFnPc4
půlené	půlený	k2eAgFnPc4d1
či	či	k8xC
obdélníkové	obdélníkový	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raženy	ražen	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
také	také	k9
klasické	klasický	k2eAgFnPc1d1
mince	mince	k1gFnPc1
věnované	věnovaný	k2eAgFnPc1d1
slavným	slavný	k2eAgMnPc3d1
Maďarům	maďar	k1gInPc3
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
více	hodně	k6eAd2
než	než	k8xS
sto	sto	k4xCgNnSc4
pamětních	pamětní	k2eAgFnPc2d1
mincí	mince	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bankovky	bankovka	k1gFnPc1
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
jsou	být	k5eAaImIp3nP
v	v	k7c6
oběhu	oběh	k1gInSc6
bankovky	bankovka	k1gFnSc2
o	o	k7c6
hodnotách	hodnota	k1gFnPc6
500	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
000	#num#	k4
a	a	k8xC
20	#num#	k4
000	#num#	k4
forintů	forint	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bankovka	bankovka	k1gFnSc1
o	o	k7c6
hodnotě	hodnota	k1gFnSc6
200	#num#	k4
forintů	forint	k1gInPc2
byla	být	k5eAaImAgFnS
19	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2009	#num#	k4
stažena	stáhnout	k5eAaPmNgFnS
z	z	k7c2
oběhu	oběh	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
všech	všecek	k3xTgFnPc6
bankovkách	bankovka	k1gFnPc6
jsou	být	k5eAaImIp3nP
na	na	k7c6
líci	líc	k1gInSc6
portréty	portrét	k1gInPc4
významných	významný	k2eAgInPc2d1
Maďarů	maďar	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c6
rubu	rub	k1gInSc6
je	být	k5eAaImIp3nS
místo	místo	k1gNnSc1
spjaté	spjatý	k2eAgNnSc1d1
s	s	k7c7
vyobrazenou	vyobrazený	k2eAgFnSc7d1
osobností	osobnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
bankovkám	bankovka	k1gFnPc3
patří	patřit	k5eAaImIp3nS
ještě	ještě	k9
speciální	speciální	k2eAgFnSc1d1
edice	edice	k1gFnSc1
2000	#num#	k4
<g/>
forintové	forintový	k2eAgFnPc4d1
bankovky	bankovka	k1gFnPc4
<g/>
,	,	kIx,
vydané	vydaný	k2eAgFnPc4d1
u	u	k7c2
příležitosti	příležitost	k1gFnSc2
milénia	milénium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Staré	Staré	k2eAgInPc1d1
vzory	vzor	k1gInPc1
bankovek	bankovka	k1gFnPc2
nejsou	být	k5eNaImIp3nP
od	od	k7c2
července	červenec	k1gInSc2
2017	#num#	k4
platné	platný	k2eAgInPc1d1
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnSc4
výměnu	výměna	k1gFnSc4
za	za	k7c4
platné	platný	k2eAgFnPc4d1
bankovky	bankovka	k1gFnPc4
provádí	provádět	k5eAaImIp3nP
banky	banka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Současné	současný	k2eAgFnPc1d1
maďarské	maďarský	k2eAgFnPc1d1
bankovky	bankovka	k1gFnPc1
</s>
<s>
ZobrazeníHodnotaBarvaVyobrazeníV	ZobrazeníHodnotaBarvaVyobrazeníV	k?
oběhu	oběh	k1gInSc2
odRozměr	odRozměra	k1gFnPc2
</s>
<s>
LícRub	LícRub	k1gMnSc1
</s>
<s>
200	#num#	k4
forintů	forint	k1gInPc2
</s>
<s>
zelená	zelený	k2eAgFnSc1d1
</s>
<s>
Líc	líc	k1gFnSc1
<g/>
:	:	kIx,
Karel	Karel	k1gMnSc1
I.	I.	kA
Robert	Robert	k1gMnSc1
-	-	kIx~
uherský	uherský	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
,	,	kIx,
vládl	vládnout	k5eAaImAgInS
1308	#num#	k4
-	-	kIx~
1342	#num#	k4
<g/>
Rub	rub	k1gInSc1
<g/>
:	:	kIx,
Diósgyőri	Diósgyőri	k1gNnSc1
vár	vár	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1998	#num#	k4
<g/>
(	(	kIx(
<g/>
stažena	stáhnout	k5eAaPmNgFnS
<g/>
)	)	kIx)
</s>
<s>
154	#num#	k4
×	×	k?
70	#num#	k4
mm	mm	kA
</s>
<s>
500	#num#	k4
forintů	forint	k1gInPc2
</s>
<s>
oranžová	oranžový	k2eAgFnSc1d1
a	a	k8xC
hnědá	hnědý	k2eAgFnSc1d1
</s>
<s>
Líc	líc	k1gFnSc1
<g/>
:	:	kIx,
František	František	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rákóczi	Rákócze	k1gFnSc4
-	-	kIx~
vůdce	vůdce	k1gMnSc2
protihabsburského	protihabsburský	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
na	na	k7c6
začátku	začátek	k1gInSc6
18	#num#	k4
<g/>
.	.	kIx.
stoletíRub	stoletíRub	k1gInSc1
<g/>
:	:	kIx,
Pataki	Pataki	k1gNnSc1
vár	vár	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
1998	#num#	k4
</s>
<s>
500	#num#	k4
forintů	forint	k1gInPc2
</s>
<s>
oranžová	oranžový	k2eAgFnSc1d1
a	a	k8xC
hnědá	hnědý	k2eAgFnSc1d1
</s>
<s>
lícová	lícový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
s	s	k7c7
totožným	totožný	k2eAgInSc7d1
motivem	motiv	k1gInSc7
<g/>
,	,	kIx,
zvláštní	zvláštní	k2eAgFnSc1d1
edice	edice	k1gFnSc1
k	k	k7c3
50	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc2
Maďarského	maďarský	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
</s>
<s>
2006	#num#	k4
</s>
<s>
1.000	1.000	k4
forintů	forint	k1gInPc2
</s>
<s>
modrá	modrý	k2eAgFnSc1d1
a	a	k8xC
žlutá	žlutý	k2eAgFnSc1d1
</s>
<s>
Líc	líc	k1gFnSc1
<g/>
:	:	kIx,
Matyáš	Matyáš	k1gMnSc1
Korvín	Korvín	k1gMnSc1
-	-	kIx~
uherský	uherský	k2eAgMnSc1d1
král	král	k1gMnSc1
<g/>
,	,	kIx,
vládl	vládnout	k5eAaImAgInS
1458	#num#	k4
<g/>
-	-	kIx~
<g/>
1490	#num#	k4
<g/>
Rub	rub	k1gInSc1
<g/>
:	:	kIx,
Fontána	fontána	k1gFnSc1
paláce	palác	k1gInSc2
ve	v	k7c6
Visegrádu	Visegrád	k1gInSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1998	#num#	k4
</s>
<s>
2.000	2.000	k4
forintů	forint	k1gInPc2
</s>
<s>
hnědá	hnědat	k5eAaImIp3nS
</s>
<s>
Líc	líc	k1gFnSc1
<g/>
:	:	kIx,
Gabriel	Gabriel	k1gMnSc1
Betlen	Betlen	k2eAgMnSc1d1
-	-	kIx~
vojenský	vojenský	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
během	během	k7c2
třicetileté	třicetiletý	k2eAgFnSc2d1
válkyRub	válkyRub	k1gMnSc1
<g/>
:	:	kIx,
Princ	princ	k1gMnSc1
mezi	mezi	k7c7
vědci	vědec	k1gMnPc7
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
1998	#num#	k4
</s>
<s>
2.000	2.000	k4
forintů	forint	k1gInPc2
</s>
<s>
hnědá	hnědat	k5eAaImIp3nS
</s>
<s>
Líc	líc	k1gFnSc1
<g/>
:	:	kIx,
Gabriel	Gabriel	k1gMnSc1
Betlen	Betlen	k2eAgMnSc1d1
-	-	kIx~
vojenský	vojenský	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
během	během	k7c2
třicetileté	třicetiletý	k2eAgFnSc2d1
válkyRub	válkyRub	k1gMnSc1
<g/>
:	:	kIx,
Princ	princ	k1gMnSc1
mezi	mezi	k7c7
vědci	vědec	k1gMnPc7
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2016	#num#	k4
</s>
<s>
5.000	5.000	k4
forintů	forint	k1gInPc2
</s>
<s>
fialová	fialový	k2eAgFnSc1d1
a	a	k8xC
hnědá	hnědý	k2eAgFnSc1d1
</s>
<s>
Líc	líc	k1gInSc1
<g/>
:	:	kIx,
István	István	k2eAgMnSc1d1
Széchenyi	Szécheny	k1gMnPc1
-	-	kIx~
maďarský	maďarský	k2eAgMnSc1d1
reformní	reformní	k2eAgMnSc1d1
politik	politik	k1gMnSc1
a	a	k8xC
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
představitel	představitel	k1gMnSc1
revoluce	revoluce	k1gFnSc2
1848-1849	1848-1849	k4
Rub	rub	k1gInSc1
<g/>
:	:	kIx,
Rodný	rodný	k2eAgInSc1d1
dům	dům	k1gInSc1
v	v	k7c4
Nagycenku	Nagycenka	k1gFnSc4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1999	#num#	k4
</s>
<s>
5.000	5.000	k4
forintů	forint	k1gInPc2
</s>
<s>
žlutá	žlutý	k2eAgFnSc1d1
a	a	k8xC
hnědá	hnědý	k2eAgFnSc1d1
</s>
<s>
Líc	líc	k1gInSc1
<g/>
:	:	kIx,
István	István	k2eAgMnSc1d1
Széchenyi	Szécheny	k1gMnPc1
-	-	kIx~
maďarský	maďarský	k2eAgMnSc1d1
reformní	reformní	k2eAgMnSc1d1
politik	politik	k1gMnSc1
a	a	k8xC
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
představitel	představitel	k1gMnSc1
revoluce	revoluce	k1gFnSc2
1848-1849	1848-1849	k4
Rub	rub	k1gInSc1
<g/>
:	:	kIx,
Rodný	rodný	k2eAgInSc1d1
dům	dům	k1gInSc1
v	v	k7c4
Nagycenku	Nagycenka	k1gFnSc4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2016	#num#	k4
</s>
<s>
10.000	10.000	k4
forintů	forint	k1gInPc2
</s>
<s>
červená	červený	k2eAgFnSc1d1
a	a	k8xC
tyrkysová	tyrkysový	k2eAgFnSc1d1
</s>
<s>
Líc	líc	k1gFnSc1
<g/>
:	:	kIx,
Štěpán	Štěpán	k1gMnSc1
I.	I.	kA
-	-	kIx~
zakladatel	zakladatel	k1gMnSc1
Uherského	uherský	k2eAgInSc2d1
královstvíRub	královstvíRub	k1gInSc1
<g/>
:	:	kIx,
Panorama	panorama	k1gNnSc1
Esztergom	Esztergom	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1997	#num#	k4
</s>
<s>
10.000	10.000	k4
forintů	forint	k1gInPc2
</s>
<s>
červená	červený	k2eAgFnSc1d1
a	a	k8xC
tyrkysová	tyrkysový	k2eAgFnSc1d1
</s>
<s>
Líc	líc	k1gFnSc1
<g/>
:	:	kIx,
Štěpán	Štěpán	k1gMnSc1
I.	I.	kA
-	-	kIx~
zakladatel	zakladatel	k1gMnSc1
Uherského	uherský	k2eAgInSc2d1
královstvíRub	královstvíRub	k1gInSc1
<g/>
:	:	kIx,
Panorama	panorama	k1gNnSc1
Esztergom	Esztergom	k1gInSc4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2014	#num#	k4
</s>
<s>
20.000	20.000	k4
forintů	forint	k1gInPc2
</s>
<s>
šedá	šedý	k2eAgNnPc1d1
a	a	k8xC
oranžová	oranžový	k2eAgNnPc1d1
</s>
<s>
Líc	líc	k1gFnSc1
<g/>
:	:	kIx,
Ferenc	Ferenc	k1gMnSc1
Deák	Deák	k1gMnSc1
-	-	kIx~
maďarský	maďarský	k2eAgMnSc1d1
státník	státník	k1gMnSc1
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
stoletíRub	stoletíRub	k1gInSc1
<g/>
:	:	kIx,
Náměstí	náměstí	k1gNnSc1
v	v	k7c6
Budapešti	Budapešť	k1gFnSc6
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2001	#num#	k4
</s>
<s>
20.000	20.000	k4
forintů	forint	k1gInPc2
</s>
<s>
šedá	šedý	k2eAgNnPc1d1
a	a	k8xC
oranžová	oranžový	k2eAgNnPc1d1
</s>
<s>
Líc	líc	k1gFnSc1
<g/>
:	:	kIx,
Ferenc	Ferenc	k1gMnSc1
Deák	Deák	k1gMnSc1
-	-	kIx~
maďarský	maďarský	k2eAgMnSc1d1
státník	státník	k1gMnSc1
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
stoletíRub	stoletíRub	k1gInSc1
<g/>
:	:	kIx,
Náměstí	náměstí	k1gNnSc1
v	v	k7c6
Budapešti	Budapešť	k1gFnSc6
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2015	#num#	k4
</s>
<s>
Ukázky	ukázka	k1gFnPc4
dřívějších	dřívější	k2eAgNnPc2d1
forintových	forintův	k2eAgNnPc2d1
platidel	platidlo	k1gNnPc2
</s>
<s>
Bankovka	bankovka	k1gFnSc1
10	#num#	k4
forintů	forint	k1gInPc2
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bankovka	bankovka	k1gFnSc1
100	#num#	k4
forintů	forint	k1gInPc2
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bankovka	bankovka	k1gFnSc1
50	#num#	k4
forintů	forint	k1gInPc2
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bankovka	bankovka	k1gFnSc1
1000	#num#	k4
forintů	forint	k1gInPc2
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
INFLATION	INFLATION	kA
RATE	RATE	kA
(	(	kIx(
<g/>
CONSUMER	CONSUMER	kA
PRICES	PRICES	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1553	#num#	k4
<g/>
-	-	kIx~
<g/>
8133	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
může	moct	k5eAaImIp3nS
přijmout	přijmout	k5eAaPmF
euro	euro	k1gNnSc4
do	do	k7c2
konce	konec	k1gInSc2
desetiletí	desetiletí	k1gNnSc2
<g/>
,	,	kIx,
předpokládá	předpokládat	k5eAaImIp3nS
ministr	ministr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
</s>
<s>
Euro	euro	k1gNnSc1
bylo	být	k5eAaImAgNnS
chybou	chyba	k1gFnSc7
<g/>
....	....	k?
Hospodářské	hospodářský	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
HAVEL	Havel	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
ODEHNAL	Odehnal	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peníze	peníz	k1gInSc2
do	do	k7c2
kapsy	kapsa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Albatros	albatros	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1408	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Maďarský	maďarský	k2eAgInSc1d1
forint	forint	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
341	#num#	k4
<g/>
-	-	kIx~
<g/>
343	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
english	english	k1gInSc1
<g/>
.	.	kIx.
<g/>
mnb	mnb	k?
<g/>
.	.	kIx.
<g/>
hu	hu	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Maďarské	maďarský	k2eAgInPc1d1
euromince	eurominec	k1gInPc1
</s>
<s>
Maďarská	maďarský	k2eAgFnSc1d1
ekonomika	ekonomika	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
měn	měna	k1gFnPc2
Evropy	Evropa	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
maďarský	maďarský	k2eAgInSc4d1
forint	forint	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
forint	forint	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Magyar	Magyar	k1gInSc1
Nemzeti	Nemzeti	k1gFnSc2
Bank	banka	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Maďarské	maďarský	k2eAgInPc4d1
peníze	peníz	k1gInPc4
(	(	kIx(
<g/>
forinty	forint	k1gInPc4
i	i	k9
pengő	pengő	k?
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Směnné	směnný	k2eAgInPc1d1
kurzy	kurz	k1gInPc1
-	-	kIx~
denní	denní	k2eAgInSc1d1
v	v	k7c6
Maďarské	maďarský	k2eAgFnSc6d1
národní	národní	k2eAgFnSc6d1
bance	banka	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
<g/>
)	)	kIx)
Obrázky	obrázek	k1gInPc1
starých	starý	k2eAgFnPc2d1
bankovek	bankovka	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Další	další	k2eAgInPc1d1
obrázky	obrázek	k1gInPc1
bankovek	bankovka	k1gFnPc2
forintů	forint	k1gInPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Maďarské	maďarský	k2eAgFnSc2d1
mince	mince	k1gFnSc2
(	(	kIx(
<g/>
katalog	katalog	k1gInSc1
a	a	k8xC
galerie	galerie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Měny	měna	k1gFnSc2
Evropy	Evropa	k1gFnSc2
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Bulharský	bulharský	k2eAgInSc1d1
lev	lev	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Dánská	dánský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Chorvatská	chorvatský	k2eAgFnSc1d1
kuna	kuna	k1gFnSc1
•	•	k?
Euro	euro	k1gNnSc1
•	•	k?
Maďarský	maďarský	k2eAgInSc1d1
forint	forint	k1gInSc1
•	•	k?
Polský	polský	k2eAgInSc1d1
zlotý	zlotý	k1gInSc1
•	•	k?
Rumunský	rumunský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Švédská	švédský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
Východní	východní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Běloruský	běloruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Gruzínský	gruzínský	k2eAgInSc1d1
lari	lari	k1gInSc1
•	•	k?
Moldavský	moldavský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Ruský	ruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Ukrajinská	ukrajinský	k2eAgFnSc1d1
hřivna	hřivna	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
Podněsterský	podněsterský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
<g/>
)	)	kIx)
Jižní	jižní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Albánský	albánský	k2eAgInSc1d1
lek	lek	k1gInSc1
•	•	k?
Bosenskohercegovská	bosenskohercegovský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Makedonský	makedonský	k2eAgInSc1d1
denár	denár	k1gInSc1
•	•	k?
Srbský	srbský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
Západní	západní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Britská	britský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
šterlinků	šterlink	k1gInPc2
•	•	k?
Faerská	Faerský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Gibraltarská	gibraltarský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Guernseyská	Guernseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Islandská	islandský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Jerseyská	Jerseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Manská	manský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Norská	norský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Švýcarský	švýcarský	k2eAgInSc4d1
frank	frank	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Maďarsko	Maďarsko	k1gNnSc1
</s>
