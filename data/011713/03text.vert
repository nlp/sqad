<p>
<s>
Widelands	Widelands	k6eAd1	Widelands
je	být	k5eAaImIp3nS	být
pomalu	pomalu	k6eAd1	pomalu
plynoucí	plynoucí	k2eAgFnSc1d1	plynoucí
realtimeová	realtimeový	k2eAgFnSc1d1	realtimeová
strategická	strategický	k2eAgFnSc1d1	strategická
počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
šířená	šířený	k2eAgFnSc1d1	šířená
pod	pod	k7c7	pod
licencí	licence	k1gFnSc7	licence
GPL	GPL	kA	GPL
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nápadně	nápadně	k6eAd1	nápadně
podobná	podobný	k2eAgFnSc1d1	podobná
a	a	k8xC	a
přebírá	přebírat	k5eAaImIp3nS	přebírat
mnoho	mnoho	k4c4	mnoho
nápadů	nápad	k1gInPc2	nápad
z	z	k7c2	z
her	hra	k1gFnPc2	hra
Settlers	Settlers	k1gInSc1	Settlers
a	a	k8xC	a
Settlers	Settlers	k1gInSc1	Settlers
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
o	o	k7c6	o
těchto	tento	k3xDgFnPc2	tento
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
open	open	k1gInSc4	open
source	sourec	k1gInSc2	sourec
projekt	projekt	k1gInSc1	projekt
a	a	k8xC	a
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
zapotřebí	zapotřebí	k6eAd1	zapotřebí
zejména	zejména	k9	zejména
v	v	k7c6	v
grafice	grafika	k1gFnSc6	grafika
a	a	k8xC	a
opravě	oprava	k1gFnSc6	oprava
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rysy	rys	k1gInPc1	rys
hry	hra	k1gFnSc2	hra
==	==	k?	==
</s>
</p>
<p>
<s>
Widelands	Widelands	k6eAd1	Widelands
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
hrána	hrát	k5eAaImNgFnS	hrát
jako	jako	k8xC	jako
singleplayer	singleplayer	k1gInSc1	singleplayer
hra	hra	k1gFnSc1	hra
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
umělé	umělý	k2eAgFnSc3d1	umělá
inteligenci	inteligence	k1gFnSc3	inteligence
<g/>
)	)	kIx)	)
i	i	k8xC	i
jako	jako	k9	jako
multiplayer	multiplayer	k1gInSc4	multiplayer
přes	přes	k7c4	přes
lokální	lokální	k2eAgFnSc4d1	lokální
síť	síť	k1gFnSc4	síť
nebo	nebo	k8xC	nebo
Internet	Internet	k1gInSc4	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
hrají	hrát	k5eAaImIp3nP	hrát
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
kmenů	kmen	k1gInPc2	kmen
-	-	kIx~	-
Barbary	barbar	k1gMnPc7	barbar
<g/>
,	,	kIx,	,
Římany	Říman	k1gMnPc7	Říman
nebo	nebo	k8xC	nebo
Atlantiďany	Atlantiďan	k1gMnPc7	Atlantiďan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
importovat	importovat	k5eAaBmF	importovat
mapy	mapa	k1gFnPc4	mapa
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
Settlers	Settlers	k1gInSc1	Settlers
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Widelands	Widelands	k6eAd1	Widelands
byla	být	k5eAaImAgNnP	být
lokalizována	lokalizovat	k5eAaBmNgNnP	lokalizovat
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Hru	hra	k1gFnSc4	hra
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
spustit	spustit	k5eAaPmF	spustit
na	na	k7c6	na
několika	několik	k4yIc6	několik
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
jako	jako	k8xS	jako
GNU	gnu	k1gMnSc1	gnu
<g/>
/	/	kIx~	/
<g/>
Linux	Linux	kA	Linux
<g/>
,	,	kIx,	,
BSD	BSD	kA	BSD
<g/>
,	,	kIx,	,
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
a	a	k8xC	a
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Widelands	Widelandsa	k1gFnPc2	Widelandsa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
projektu	projekt	k1gInSc2	projekt
</s>
</p>
<p>
<s>
Widelands	Widelands	k1gInSc1	Widelands
Wiki	Wik	k1gFnSc2	Wik
</s>
</p>
