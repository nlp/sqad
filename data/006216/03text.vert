<s>
Masakr	masakr	k1gInSc1	masakr
na	na	k7c4	na
Virginia	Virginium	k1gNnPc4	Virginium
Tech	Tech	k?	Tech
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2007	[number]	k4	2007
na	na	k7c6	na
kampusu	kampus	k1gInSc6	kampus
Virginia	Virginium	k1gNnSc2	Virginium
Polytechnic	Polytechnice	k1gFnPc2	Polytechnice
Institute	institut	k1gInSc5	institut
and	and	k?	and
State	status	k1gInSc5	status
University	universita	k1gFnSc2	universita
v	v	k7c6	v
Blacksburgu	Blacksburg	k1gInSc6	Blacksburg
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
Útočník	útočník	k1gMnSc1	útočník
<g/>
,	,	kIx,	,
jihokorejský	jihokorejský	k2eAgMnSc1d1	jihokorejský
student	student	k1gMnSc1	student
školy	škola	k1gFnSc2	škola
Čo	Čo	k1gFnSc2	Čo
Sung-hui	Sungu	k1gFnSc2	Sung-hu
<g/>
,	,	kIx,	,
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
zabil	zabít	k5eAaPmAgMnS	zabít
32	[number]	k4	32
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
desítky	desítka	k1gFnSc2	desítka
zranil	zranit	k5eAaPmAgMnS	zranit
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
pak	pak	k6eAd1	pak
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Masakr	masakr	k1gInSc1	masakr
na	na	k7c4	na
Virginia	Virginium	k1gNnPc4	Virginium
Tech	Tech	k?	Tech
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
masakrů	masakr	k1gInPc2	masakr
na	na	k7c6	na
území	území	k1gNnSc6	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
masakrem	masakr	k1gInSc7	masakr
na	na	k7c6	na
americké	americký	k2eAgFnSc6d1	americká
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
celkem	celkem	k6eAd1	celkem
32	[number]	k4	32
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
učitelů	učitel	k1gMnPc2	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Emily	Emil	k1gMnPc4	Emil
Jane	Jan	k1gMnSc5	Jan
Hilscher	Hilschra	k1gFnPc2	Hilschra
<g/>
,	,	kIx,	,
19	[number]	k4	19
Ryan	Ryano	k1gNnPc2	Ryano
Christopher	Christophra	k1gFnPc2	Christophra
"	"	kIx"	"
<g/>
Stack	Stack	k1gInSc1	Stack
<g/>
"	"	kIx"	"
Clark	Clark	k1gInSc1	Clark
<g/>
,	,	kIx,	,
22	[number]	k4	22
Studenti	student	k1gMnPc1	student
Ross	Rossa	k1gFnPc2	Rossa
Abdallah	Abdallah	k1gInSc1	Abdallah
Alameddine	Alameddin	k1gInSc5	Alameddin
<g/>
,	,	kIx,	,
20	[number]	k4	20
Brian	Brian	k1gMnSc1	Brian
Roy	Roy	k1gMnSc1	Roy
Bluhm	Bluhm	k1gMnSc1	Bluhm
<g/>
,	,	kIx,	,
25	[number]	k4	25
Austin	Austin	k2eAgInSc1d1	Austin
Michelle	Michelle	k1gInSc1	Michelle
Cloyd	Cloyda	k1gFnPc2	Cloyda
<g/>
,	,	kIx,	,
18	[number]	k4	18
Matthew	Matthew	k1gMnPc4	Matthew
Gregory	Gregor	k1gMnPc4	Gregor
Gwaltney	Gwaltnea	k1gFnSc2	Gwaltnea
<g/>
,	,	kIx,	,
24	[number]	k4	24
Caitlin	Caitlin	k2eAgInSc1d1	Caitlin
Millar	Millar	k1gInSc1	Millar
Hammaren	Hammarna	k1gFnPc2	Hammarna
<g/>
,	,	kIx,	,
19	[number]	k4	19
Jeremy	Jerem	k1gInPc1	Jerem
Michael	Michael	k1gMnSc1	Michael
<g />
.	.	kIx.	.
</s>
<s>
Herbstritt	Herbstritt	k1gMnSc1	Herbstritt
<g/>
,	,	kIx,	,
27	[number]	k4	27
Rachael	Rachalo	k1gNnPc2	Rachalo
Elizabeth	Elizabeth	k1gFnPc2	Elizabeth
Hill	Hilla	k1gFnPc2	Hilla
<g/>
,	,	kIx,	,
18	[number]	k4	18
Matthew	Matthew	k1gFnPc2	Matthew
Joseph	Josepha	k1gFnPc2	Josepha
La	la	k1gNnSc2	la
Porte	port	k1gInSc5	port
<g/>
,	,	kIx,	,
20	[number]	k4	20
Jarrett	Jarrett	k2eAgInSc1d1	Jarrett
Lee	Lea	k1gFnSc3	Lea
Lane	Lan	k1gFnSc2	Lan
<g/>
,	,	kIx,	,
22	[number]	k4	22
Henry	henry	k1gInSc1	henry
J.	J.	kA	J.
Lee	Lea	k1gFnSc6	Lea
<g/>
,	,	kIx,	,
20	[number]	k4	20
Partahi	Partah	k1gFnSc2	Partah
Mamora	Mamora	k1gFnSc1	Mamora
Halomoan	Halomoan	k1gMnSc1	Halomoan
"	"	kIx"	"
<g/>
Mora	mora	k1gFnSc1	mora
<g/>
"	"	kIx"	"
Lumbantoruan	Lumbantoruan	k1gMnSc1	Lumbantoruan
<g/>
,	,	kIx,	,
34	[number]	k4	34
Lauren	Laurna	k1gFnPc2	Laurna
Ashley	Ashlea	k1gFnSc2	Ashlea
McCain	McCaina	k1gFnPc2	McCaina
<g/>
,	,	kIx,	,
20	[number]	k4	20
Daniel	Daniel	k1gMnSc1	Daniel
Patrick	Patrick	k1gMnSc1	Patrick
O	o	k7c6	o
<g />
.	.	kIx.	.
</s>
<s>
<g/>
'	'	kIx"	'
<g/>
Neil	Neil	k1gMnSc1	Neil
<g/>
,	,	kIx,	,
23	[number]	k4	23
Juan	Juan	k1gMnSc1	Juan
Ramón	Ramón	k1gMnSc1	Ramón
Ortiz-Ortiz	Ortiz-Ortiz	k1gMnSc1	Ortiz-Ortiz
<g/>
,	,	kIx,	,
26	[number]	k4	26
Minal	Minal	k1gInSc1	Minal
Hiralal	Hiralal	k1gFnSc2	Hiralal
"	"	kIx"	"
<g/>
Minu	minout	k5eAaImIp1nS	minout
<g/>
"	"	kIx"	"
Panchal	Panchal	k1gMnSc1	Panchal
<g/>
,	,	kIx,	,
26	[number]	k4	26
Daniel	Daniela	k1gFnPc2	Daniela
Alejandro	Alejandra	k1gFnSc5	Alejandra
Pérez	Pérez	k1gMnSc1	Pérez
Cueva	Cueva	k1gFnSc1	Cueva
<g/>
,	,	kIx,	,
21	[number]	k4	21
Erin	Erin	k1gInSc1	Erin
Nicole	Nicole	k1gFnSc2	Nicole
Peterson	Petersona	k1gFnPc2	Petersona
<g/>
,	,	kIx,	,
18	[number]	k4	18
Michael	Michaela	k1gFnPc2	Michaela
Steven	Stevna	k1gFnPc2	Stevna
Pohle	Pohl	k1gMnSc5	Pohl
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc5	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
23	[number]	k4	23
Julia	Julius	k1gMnSc2	Julius
Kathleen	Kathleen	k1gInSc4	Kathleen
<g />
.	.	kIx.	.
</s>
<s>
Pryde	Pryde	k6eAd1	Pryde
<g/>
,	,	kIx,	,
23	[number]	k4	23
Mary	Mary	k1gFnSc1	Mary
Karen	Karna	k1gFnPc2	Karna
Read	Reada	k1gFnPc2	Reada
<g/>
,	,	kIx,	,
19	[number]	k4	19
Reema	Reemum	k1gNnSc2	Reemum
Joseph	Joseph	k1gMnSc1	Joseph
Samaha	Samaha	k1gMnSc1	Samaha
<g/>
,	,	kIx,	,
18	[number]	k4	18
Waleed	Waleed	k1gMnSc1	Waleed
Mohamed	Mohamed	k1gMnSc1	Mohamed
Shaalan	Shaalan	k1gMnSc1	Shaalan
<g/>
,	,	kIx,	,
32	[number]	k4	32
Leslie	Leslie	k1gFnSc1	Leslie
Geraldine	Geraldin	k1gInSc5	Geraldin
Sherman	Sherman	k1gMnSc1	Sherman
<g/>
,	,	kIx,	,
20	[number]	k4	20
Maxine	Maxin	k1gInSc5	Maxin
Shelly	Shella	k1gFnPc1	Shella
Turner	turner	k1gMnSc1	turner
<g/>
,	,	kIx,	,
22	[number]	k4	22
Nicole	Nicole	k1gFnSc1	Nicole
Regina	Regina	k1gFnSc1	Regina
White	Whit	k1gInSc5	Whit
<g/>
,	,	kIx,	,
20	[number]	k4	20
Vyučující	vyučující	k2eAgInSc1d1	vyučující
Christopher	Christophra	k1gFnPc2	Christophra
James	James	k1gMnSc1	James
"	"	kIx"	"
<g/>
Jamie	Jamie	k1gFnSc1	Jamie
<g/>
"	"	kIx"	"
Bishop	Bishop	k1gInSc1	Bishop
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
35	[number]	k4	35
Jocelyne	Jocelyn	k1gInSc5	Jocelyn
Couture-Nowak	Couture-Nowak	k1gMnSc1	Couture-Nowak
<g/>
,	,	kIx,	,
49	[number]	k4	49
Kevin	Kevin	k1gInSc1	Kevin
P.	P.	kA	P.
Granata	Granat	k1gMnSc2	Granat
<g/>
,	,	kIx,	,
45	[number]	k4	45
Liviu	Livium	k1gNnSc6	Livium
Librescu	Librescus	k1gInSc2	Librescus
<g/>
,	,	kIx,	,
76	[number]	k4	76
G.	G.	kA	G.
V.	V.	kA	V.
Loganathan	Loganathan	k1gMnSc1	Loganathan
<g/>
,	,	kIx,	,
53	[number]	k4	53
Masakr	masakr	k1gInSc1	masakr
na	na	k7c4	na
Columbine	Columbin	k1gInSc5	Columbin
High	Higha	k1gFnPc2	Higha
School	Schoola	k1gFnPc2	Schoola
Školní	školní	k2eAgInSc4d1	školní
masakr	masakr	k1gInSc4	masakr
v	v	k7c6	v
Kauhajoki	Kauhajok	k1gFnSc6	Kauhajok
Masakr	masakr	k1gInSc1	masakr
na	na	k7c4	na
Sandy	Sanda	k1gFnPc4	Sanda
Hook	Hooka	k1gFnPc2	Hooka
Elementary	Elementara	k1gFnPc4	Elementara
School	School	k1gInSc4	School
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Masakr	masakr	k1gInSc4	masakr
na	na	k7c6	na
Virginia	Virginium	k1gNnPc4	Virginium
Tech	Tech	k?	Tech
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
iDnes	iDnes	k1gInSc4	iDnes
online	onlinout	k5eAaPmIp3nS	onlinout
Zprávy	zpráva	k1gFnPc1	zpráva
na	na	k7c4	na
Novinky	novinka	k1gFnPc4	novinka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
ČTK	ČTK	kA	ČTK
Reportáž	reportáž	k1gFnSc1	reportáž
ve	v	k7c6	v
Střepinách	střepina	k1gFnPc6	střepina
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
</s>
