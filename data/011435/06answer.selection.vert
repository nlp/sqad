<s>
Přílivová	přílivový	k2eAgFnSc1d1	přílivová
vlna	vlna	k1gFnSc1	vlna
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
pro	pro	k7c4	pro
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
stoupající	stoupající	k2eAgInSc1d1	stoupající
příliv	příliv	k1gInSc1	příliv
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
vlnu	vlna	k1gFnSc4	vlna
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
stoupá	stoupat	k5eAaImIp3nS	stoupat
vzhůru	vzhůru	k6eAd1	vzhůru
zálivem	záliv	k1gInSc7	záliv
nebo	nebo	k8xC	nebo
řekou	řeka	k1gFnSc7	řeka
<g/>
.	.	kIx.	.
</s>
