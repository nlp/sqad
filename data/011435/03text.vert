<p>
<s>
Přílivová	přílivový	k2eAgFnSc1d1	přílivová
vlna	vlna	k1gFnSc1	vlna
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
pro	pro	k7c4	pro
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
stoupající	stoupající	k2eAgInSc1d1	stoupající
příliv	příliv	k1gInSc1	příliv
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
vlnu	vlna	k1gFnSc4	vlna
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
stoupá	stoupat	k5eAaImIp3nS	stoupat
vzhůru	vzhůru	k6eAd1	vzhůru
zálivem	záliv	k1gInSc7	záliv
nebo	nebo	k8xC	nebo
řekou	řeka	k1gFnSc7	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
dmutím	dmutí	k1gNnSc7	dmutí
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
nad	nad	k7c4	nad
šest	šest	k4xCc4	šest
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
záliv	záliv	k1gInSc1	záliv
či	či	k8xC	či
řeka	řeka	k1gFnSc1	řeka
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
zužuje	zužovat	k5eAaImIp3nS	zužovat
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
mělčím	mělký	k2eAgMnSc7d2	mělčí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
přílivové	přílivový	k2eAgFnPc1d1	přílivová
vlny	vlna	k1gFnPc1	vlna
na	na	k7c6	na
světě	svět	k1gInSc6	svět
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Čínské	čínský	k2eAgFnSc6d1	čínská
lidové	lidový	k2eAgFnSc6d1	lidová
republice	republika	k1gFnSc6	republika
u	u	k7c2	u
města	město	k1gNnSc2	město
Chang-čou	Chang-čou	k1gNnSc2	Chang-čou
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
řeka	řeka	k1gFnSc1	řeka
Čchien-tchang	Čchienchanga	k1gFnPc2	Čchien-tchanga
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
zátoky	zátoka	k1gFnSc2	zátoka
Chang-čou	Chang-čou	k1gNnSc2	Chang-čou
<g/>
.	.	kIx.	.
</s>
<s>
Přílivová	přílivový	k2eAgFnSc1d1	přílivová
vlna	vlna	k1gFnSc1	vlna
zde	zde	k6eAd1	zde
bývá	bývat	k5eAaImIp3nS	bývat
až	až	k9	až
devět	devět	k4xCc4	devět
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
má	můj	k3xOp1gFnSc1	můj
rychlost	rychlost	k1gFnSc1	rychlost
až	až	k9	až
40	[number]	k4	40
kilometrů	kilometr	k1gInPc2	kilometr
v	v	k7c6	v
hodině	hodina	k1gFnSc6	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
známou	známý	k2eAgFnSc7d1	známá
přílivovou	přílivový	k2eAgFnSc7d1	přílivová
vlnou	vlna	k1gFnSc7	vlna
je	být	k5eAaImIp3nS	být
pororoca	pororoca	k6eAd1	pororoca
na	na	k7c6	na
Amazonce	Amazonka	k1gFnSc6	Amazonka
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
sice	sice	k8xC	sice
bývá	bývat	k5eAaImIp3nS	bývat
menší	malý	k2eAgFnSc1d2	menší
a	a	k8xC	a
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
rychlostí	rychlost	k1gFnSc7	rychlost
do	do	k7c2	do
25	[number]	k4	25
kilometrů	kilometr	k1gInPc2	kilometr
v	v	k7c6	v
hodině	hodina	k1gFnSc6	hodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
rozeznatelná	rozeznatelný	k2eAgFnSc1d1	rozeznatelná
až	až	k9	až
800	[number]	k4	800
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
přílivová	přílivový	k2eAgFnSc1d1	přílivová
vlna	vlna	k1gFnSc1	vlna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
