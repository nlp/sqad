<s>
Hnědý	hnědý	k2eAgMnSc1d1
trpaslík	trpaslík	k1gMnSc1
je	být	k5eAaImIp3nS
subhvězdný	subhvězdný	k2eAgInSc1d1
objekt	objekt	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
nevyzařuje	vyzařovat	k5eNaImIp3nS
světlo	světlo	k1gNnSc4
a	a	k8xC
energii	energie	k1gFnSc4
díky	díky	k7c3
termonukleárním	termonukleární	k2eAgFnPc3d1
reakcím	reakce	k1gFnPc3
jako	jako	k8xC,k8xS
hvězdy	hvězda	k1gFnPc1
hlavní	hlavní	k2eAgFnSc2d1
posloupnosti	posloupnost	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
má	mít	k5eAaImIp3nS
vodivý	vodivý	k2eAgInSc4d1
povrch	povrch	k1gInSc4
a	a	k8xC
jádro	jádro	k1gNnSc4
<g/>
.	.	kIx.
</s>