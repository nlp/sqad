<s>
Na	na	k7c6	na
plovárně	plovárna	k1gFnSc6	plovárna
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgMnSc1d1	populární
talk	talk	k1gMnSc1	talk
show	show	k1gFnSc2	show
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
rozhovor	rozhovor	k1gInSc1	rozhovor
byl	být	k5eAaImAgInS	být
vysílán	vysílat	k5eAaImNgInS	vysílat
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
s	s	k7c7	s
Michaelem	Michael	k1gMnSc7	Michael
Kocábem	Kocáb	k1gMnSc7	Kocáb
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byl	být	k5eAaImAgInS	být
pořad	pořad	k1gInSc1	pořad
natáčen	natáčet	k5eAaImNgInS	natáčet
na	na	k7c6	na
Občanské	občanský	k2eAgFnSc6d1	občanská
plovárně	plovárna	k1gFnSc6	plovárna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dala	dát	k5eAaPmAgFnS	dát
pořadu	pořad	k1gInSc6	pořad
název	název	k1gInSc1	název
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
kresba	kresba	k1gFnSc1	kresba
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
úvodní	úvodní	k2eAgFnSc6d1	úvodní
znělce	znělka	k1gFnSc6	znělka
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
natáčení	natáčení	k1gNnSc1	natáčení
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
Avion	avion	k1gInSc1	avion
<g/>
.	.	kIx.	.
</s>
<s>
Moderátor	moderátor	k1gMnSc1	moderátor
Marek	Marek	k1gMnSc1	Marek
Eben	eben	k1gInSc4	eben
si	se	k3xPyFc3	se
k	k	k7c3	k
rozhovoru	rozhovor	k1gInSc3	rozhovor
zve	zvát	k5eAaImIp3nS	zvát
české	český	k2eAgNnSc1d1	české
i	i	k9	i
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
osobnosti	osobnost	k1gFnPc4	osobnost
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
oborů	obor	k1gInPc2	obor
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Moderátor	moderátor	k1gInSc1	moderátor
do	do	k7c2	do
pořadu	pořad	k1gInSc2	pořad
úmyslně	úmyslně	k6eAd1	úmyslně
nezve	zvát	k5eNaImIp3nS	zvát
aktivní	aktivní	k2eAgFnSc2d1	aktivní
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
jsou	být	k5eAaImIp3nP	být
dělány	dělán	k2eAgInPc4d1	dělán
rozhovory	rozhovor	k1gInPc4	rozhovor
až	až	k9	až
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
jejich	jejich	k3xOp3gFnSc2	jejich
aktivní	aktivní	k2eAgFnSc2d1	aktivní
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
odvysílán	odvysílat	k5eAaPmNgInS	odvysílat
500	[number]	k4	500
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pořad	pořad	k1gInSc1	pořad
pravidelně	pravidelně	k6eAd1	pravidelně
vysílán	vysílán	k2eAgInSc1d1	vysílán
každou	každý	k3xTgFnSc4	každý
středu	středa	k1gFnSc4	středa
večer	večer	k6eAd1	večer
na	na	k7c6	na
kanále	kanál	k1gInSc6	kanál
ČT	ČT	kA	ČT
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
sledovanost	sledovanost	k1gFnSc1	sledovanost
měl	mít	k5eAaImAgInS	mít
rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Dagmar	Dagmar	k1gFnSc7	Dagmar
Havlovou	Havlová	k1gFnSc7	Havlová
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2002	[number]	k4	2002
zhlédlo	zhlédnout	k5eAaPmAgNnS	zhlédnout
894	[number]	k4	894
tisíc	tisíc	k4xCgInPc2	tisíc
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
odvysíláno	odvysílat	k5eAaPmNgNnS	odvysílat
přes	přes	k7c4	přes
667	[number]	k4	667
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
rozhovorů	rozhovor	k1gInPc2	rozhovor
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
knižně	knižně	k6eAd1	knižně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zvukové	zvukový	k2eAgFnSc6d1	zvuková
podobě	podoba	k1gFnSc6	podoba
na	na	k7c6	na
CD	CD	kA	CD
vyšly	vyjít	k5eAaPmAgFnP	vyjít
u	u	k7c2	u
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Lotos	lotos	k1gInSc1	lotos
<g/>
.	.	kIx.	.
</s>
