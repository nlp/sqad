<s>
Samec	samec	k1gMnSc1	samec
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k1gNnSc4	málo
menší	malý	k2eAgFnSc2d2	menší
velikosti	velikost	k1gFnSc2	velikost
než	než	k8xS	než
krocan	krocan	k1gMnSc1	krocan
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
74	[number]	k4	74
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
menší	malý	k2eAgMnSc1d2	menší
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
54	[number]	k4	54
<g/>
–	–	k?	–
<g/>
63	[number]	k4	63
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
