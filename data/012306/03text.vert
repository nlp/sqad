<p>
<s>
Tetřev	tetřev	k1gMnSc1	tetřev
hlušec	hlušec	k1gMnSc1	hlušec
(	(	kIx(	(
<g/>
Tetrao	Tetrao	k1gMnSc1	Tetrao
urogallus	urogallus	k1gMnSc1	urogallus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
hrabavého	hrabavý	k2eAgMnSc2d1	hrabavý
ptáka	pták	k1gMnSc2	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
tetřevovitých	tetřevovitý	k2eAgMnPc2d1	tetřevovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Obvykle	obvykle	k6eAd1	obvykle
bývají	bývat	k5eAaImIp3nP	bývat
rozlišovány	rozlišován	k2eAgInPc4d1	rozlišován
3	[number]	k4	3
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
:	:	kIx,	:
tetřev	tetřev	k1gMnSc1	tetřev
hlušec	hlušec	k1gMnSc1	hlušec
evropský	evropský	k2eAgMnSc1d1	evropský
(	(	kIx(	(
<g/>
T.	T.	kA	T.
u.	u.	k?	u.
urogallus	urogallus	k1gInSc1	urogallus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgFnSc4d1	obývající
většinu	většina	k1gFnSc4	většina
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
tetřev	tetřev	k1gMnSc1	tetřev
hlušec	hlušec	k1gMnSc1	hlušec
pyrenejský	pyrenejský	k2eAgMnSc1d1	pyrenejský
(	(	kIx(	(
<g/>
T.	T.	kA	T.
u.	u.	k?	u.
aquitanicus	aquitanicus	k1gInSc1	aquitanicus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
pohořích	pohoří	k1gNnPc6	pohoří
Pyrenejí	Pyreneje	k1gFnPc2	Pyreneje
a	a	k8xC	a
severozápadního	severozápadní	k2eAgNnSc2d1	severozápadní
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
tetřev	tetřev	k1gMnSc1	tetřev
hlušec	hlušec	k1gMnSc1	hlušec
sibiřský	sibiřský	k2eAgMnSc1d1	sibiřský
(	(	kIx(	(
<g/>
T.	T.	kA	T.
u.	u.	k?	u.
taczanowskii	taczanowskie	k1gFnSc6	taczanowskie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgFnSc4d1	obývající
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
a	a	k8xC	a
území	území	k1gNnSc4	území
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
dělí	dělit	k5eAaImIp3nP	dělit
populace	populace	k1gFnSc2	populace
na	na	k7c4	na
více	hodně	k6eAd2	hodně
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
T.	T.	kA	T.
u.	u.	k?	u.
rudolfi	rudolf	k1gFnSc2	rudolf
<g/>
,	,	kIx,	,
T.	T.	kA	T.
u.	u.	k?	u.
major	major	k1gMnSc1	major
nebo	nebo	k8xC	nebo
T.	T.	kA	T.
u.	u.	k?	u.
uralensis	uralensis	k1gInSc1	uralensis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Samec	samec	k1gMnSc1	samec
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k1gNnSc4	málo
menší	malý	k2eAgFnSc2d2	menší
velikosti	velikost	k1gFnSc2	velikost
než	než	k8xS	než
krocan	krocan	k1gMnSc1	krocan
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
74	[number]	k4	74
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
menší	malý	k2eAgMnSc1d2	menší
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
54	[number]	k4	54
<g/>
–	–	k?	–
<g/>
63	[number]	k4	63
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
nezaměnitelný	zaměnitelný	k2eNgInSc1d1	nezaměnitelný
<g/>
,	,	kIx,	,
tmavý	tmavý	k2eAgInSc1d1	tmavý
<g/>
,	,	kIx,	,
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
skvrnou	skvrna	k1gFnSc7	skvrna
v	v	k7c6	v
ohbí	ohbí	k1gNnSc6	ohbí
křídla	křídlo	k1gNnSc2	křídlo
a	a	k8xC	a
zblízka	zblízka	k6eAd1	zblízka
patrným	patrný	k2eAgInSc7d1	patrný
modrozeleným	modrozelený	k2eAgInSc7d1	modrozelený
leskem	lesk	k1gInSc7	lesk
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
a	a	k8xC	a
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
<g/>
,	,	kIx,	,
oble	oble	k6eAd1	oble
zakončeném	zakončený	k2eAgInSc6d1	zakončený
ocase	ocas	k1gInSc6	ocas
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc1	několik
bílých	bílý	k2eAgFnPc2d1	bílá
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
očima	oko	k1gNnPc7	oko
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
patrné	patrný	k2eAgFnPc1d1	patrná
červené	červený	k2eAgFnPc1d1	červená
poušky	pouška	k1gFnPc1	pouška
<g/>
,	,	kIx,	,
zobák	zobák	k1gInSc1	zobák
je	být	k5eAaImIp3nS	být
slámově	slámově	k6eAd1	slámově
žlutý	žlutý	k2eAgInSc1d1	žlutý
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
nenápadně	nápadně	k6eNd1	nápadně
hnědá	hnědý	k2eAgFnSc1d1	hnědá
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
proužkovaná	proužkovaný	k2eAgFnSc1d1	proužkovaná
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
podobné	podobný	k2eAgFnSc2d1	podobná
samice	samice	k1gFnSc2	samice
tetřívka	tetřívek	k1gMnSc2	tetřívek
obecného	obecný	k2eAgNnSc2d1	obecné
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgFnSc7d2	veliký
velikostí	velikost	k1gFnSc7	velikost
<g/>
,	,	kIx,	,
oranžovohnědou	oranžovohnědý	k2eAgFnSc7d1	oranžovohnědá
hrudí	hruď	k1gFnSc7	hruď
bez	bez	k7c2	bez
proužků	proužek	k1gInPc2	proužek
<g/>
,	,	kIx,	,
nápadnějšími	nápadní	k2eAgNnPc7d2	nápadní
ramínky	ramínko	k1gNnPc7	ramínko
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
poněkud	poněkud	k6eAd1	poněkud
světlejším	světlý	k2eAgNnSc7d2	světlejší
a	a	k8xC	a
rezavějším	rezavý	k2eAgNnSc7d2	rezavější
opeřením	opeření	k1gNnSc7	opeření
a	a	k8xC	a
delším	dlouhý	k2eAgInSc7d2	delší
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
zaoblenějším	zaoblený	k2eAgMnPc3d2	zaoblenější
a	a	k8xC	a
rezavěji	rezavě	k6eAd2	rezavě
zbarveným	zbarvený	k2eAgInSc7d1	zbarvený
ocasem	ocas	k1gInSc7	ocas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Tetřev	tetřev	k1gMnSc1	tetřev
hlušec	hlušec	k1gMnSc1	hlušec
má	mít	k5eAaImIp3nS	mít
palearktický	palearktický	k2eAgInSc4d1	palearktický
typ	typ	k1gInSc4	typ
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Těžiště	těžiště	k1gNnSc1	těžiště
výskytu	výskyt	k1gInSc2	výskyt
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
,	,	kIx,	,
Pobaltí	Pobaltí	k1gNnSc6	Pobaltí
<g/>
,	,	kIx,	,
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
pouze	pouze	k6eAd1	pouze
ostrůvkovitý	ostrůvkovitý	k2eAgInSc1d1	ostrůvkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
skandinávských	skandinávský	k2eAgInPc2d1	skandinávský
a	a	k8xC	a
pobaltských	pobaltský	k2eAgInPc2d1	pobaltský
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
také	také	k9	také
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
,	,	kIx,	,
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
alpských	alpský	k2eAgFnPc6d1	alpská
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
Balkánského	balkánský	k2eAgInSc2d1	balkánský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
areál	areál	k1gInSc1	areál
se	se	k3xPyFc4	se
zvláště	zvláště	k6eAd1	zvláště
vlivem	vliv	k1gInSc7	vliv
ničení	ničení	k1gNnSc2	ničení
vhodného	vhodný	k2eAgNnSc2d1	vhodné
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
přímého	přímý	k2eAgNnSc2d1	přímé
pronásledování	pronásledování	k1gNnSc2	pronásledování
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
člověka	člověk	k1gMnSc2	člověk
značně	značně	k6eAd1	značně
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
a	a	k8xC	a
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
úspěšně	úspěšně	k6eAd1	úspěšně
introdukován	introdukován	k2eAgMnSc1d1	introdukován
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Dánsku	Dánsko	k1gNnSc6	Dánsko
a	a	k8xC	a
Belgii	Belgie	k1gFnSc3	Belgie
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
zcela	zcela	k6eAd1	zcela
vyhuben	vyhuben	k2eAgInSc1d1	vyhuben
<g/>
.	.	kIx.	.
</s>
<s>
Stálý	stálý	k2eAgInSc1d1	stálý
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
však	však	k8xC	však
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
podniká	podnikat	k5eAaImIp3nS	podnikat
pravidelné	pravidelný	k2eAgInPc4d1	pravidelný
přelety	přelet	k1gInPc4	přelet
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
za	za	k7c7	za
potravou	potrava	k1gFnSc7	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
760.000	[number]	k4	760.000
<g/>
-	-	kIx~	-
<g/>
1.000	[number]	k4	1.000
000	[number]	k4	000
párů	pár	k1gInPc2	pár
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
stabilizovanou	stabilizovaný	k2eAgFnSc4d1	stabilizovaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgMnS	být
tetřev	tetřev	k1gMnSc1	tetřev
hlušec	hlušec	k1gMnSc1	hlušec
na	na	k7c4	na
území	území	k1gNnSc4	území
ČR	ČR	kA	ČR
relativně	relativně	k6eAd1	relativně
běžným	běžný	k2eAgInSc7d1	běžný
druhem	druh	k1gInSc7	druh
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
od	od	k7c2	od
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
začaly	začít	k5eAaPmAgInP	začít
jeho	jeho	k3xOp3gInPc1	jeho
stavy	stav	k1gInPc1	stav
prudce	prudko	k6eAd1	prudko
klesat	klesat	k5eAaImF	klesat
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
vymizel	vymizet	k5eAaPmAgMnS	vymizet
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
obsazených	obsazený	k2eAgFnPc2d1	obsazená
lokalit	lokalita	k1gFnPc2	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1973-77	[number]	k4	1973-77
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
znám	znát	k5eAaImIp1nS	znát
jen	jen	k9	jen
ze	z	k7c2	z
Třeboňska	Třeboňsko	k1gNnSc2	Třeboňsko
<g/>
,	,	kIx,	,
Písecka	Písecko	k1gNnSc2	Písecko
<g/>
,	,	kIx,	,
Brd	Brdy	k1gInPc2	Brdy
<g/>
,	,	kIx,	,
Nečtin	Nečtina	k1gFnPc2	Nečtina
a	a	k8xC	a
Džbánu	džbán	k1gInSc2	džbán
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1985-89	[number]	k4	1985-89
již	již	k6eAd1	již
nebyl	být	k5eNaImAgInS	být
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
nikde	nikde	k6eAd1	nikde
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
byl	být	k5eAaImAgInS	být
omezen	omezit	k5eAaPmNgInS	omezit
na	na	k7c4	na
některá	některý	k3yIgNnPc4	některý
hraniční	hraniční	k2eAgNnPc4d1	hraniční
horstva	horstvo	k1gNnPc4	horstvo
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
žije	žít	k5eAaImIp3nS	žít
jediná	jediný	k2eAgFnSc1d1	jediná
životaschopná	životaschopný	k2eAgFnSc1d1	životaschopná
populace	populace	k1gFnSc1	populace
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
90	[number]	k4	90
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
našich	náš	k3xOp1gMnPc2	náš
tetřevů	tetřev	k1gMnPc2	tetřev
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdiště	hnízdiště	k1gNnSc1	hnízdiště
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházejí	nacházet	k5eAaImIp3nP	nacházet
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1000	[number]	k4	1000
<g/>
–	–	k?	–
<g/>
1370	[number]	k4	1370
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
soustředěna	soustředěn	k2eAgNnPc1d1	soustředěno
především	především	k6eAd1	především
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
centrální	centrální	k2eAgFnSc2d1	centrální
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
centrální	centrální	k2eAgFnPc4d1	centrální
pláně	pláň	k1gFnPc4	pláň
modravské	modravský	k2eAgFnPc4d1	modravský
a	a	k8xC	a
kvildské	kvildský	k2eAgFnPc4d1	kvildský
a	a	k8xC	a
vysoké	vysoký	k2eAgFnPc4d1	vysoká
polohy	poloha	k1gFnPc4	poloha
železnorudské	železnorudský	k2eAgFnSc2d1	Železnorudská
hornatiny	hornatina	k1gFnSc2	hornatina
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
kotliny	kotlina	k1gFnSc2	kotlina
horní	horní	k2eAgFnSc1d1	horní
Křemelné	křemelný	k2eAgFnSc3d1	Křemelná
<g/>
.	.	kIx.	.
</s>
<s>
Početnost	početnost	k1gFnSc4	početnost
zde	zde	k6eAd1	zde
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
mírně	mírně	k6eAd1	mírně
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Zbytkové	zbytkový	k2eAgFnPc1d1	zbytková
populace	populace	k1gFnPc1	populace
tetřeva	tetřev	k1gMnSc2	tetřev
hlušce	hlušec	k1gMnSc2	hlušec
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
také	také	k9	také
ve	v	k7c6	v
Slavkovském	slavkovský	k2eAgInSc6d1	slavkovský
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
Beskydech	Beskyd	k1gInPc6	Beskyd
<g/>
,	,	kIx,	,
Jeseníkách	Jeseníky	k1gInPc6	Jeseníky
a	a	k8xC	a
Králickém	králický	k2eAgInSc6d1	králický
Sněžníku	Sněžník	k1gInSc6	Sněžník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vybraných	vybraný	k2eAgNnPc6d1	vybrané
místech	místo	k1gNnPc6	místo
(	(	kIx(	(
<g/>
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
<g/>
,	,	kIx,	,
Jeseníky	Jeseník	k1gInPc1	Jeseník
<g/>
,	,	kIx,	,
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
Brdy	Brdy	k1gInPc1	Brdy
<g/>
)	)	kIx)	)
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
reintrodučkní	reintrodučknit	k5eAaPmIp3nP	reintrodučknit
pokusy	pokus	k1gInPc1	pokus
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
však	však	k9	však
nepodařilo	podařit	k5eNaPmAgNnS	podařit
docílit	docílit	k5eAaPmF	docílit
vytvoření	vytvoření	k1gNnSc1	vytvoření
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
životaschopné	životaschopný	k2eAgFnSc2d1	životaschopná
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
početnost	početnost	k1gFnSc1	početnost
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
na	na	k7c4	na
400-500	[number]	k4	400-500
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1973-77	[number]	k4	1973-77
na	na	k7c4	na
530-700	[number]	k4	530-700
kohoutů	kohout	k1gInPc2	kohout
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1985-89	[number]	k4	1985-89
na	na	k7c4	na
100-150	[number]	k4	100-150
kohoutů	kohout	k1gInPc2	kohout
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
na	na	k7c4	na
100-200	[number]	k4	100-200
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prostředí	prostředí	k1gNnSc2	prostředí
==	==	k?	==
</s>
</p>
<p>
<s>
Žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
starých	stará	k1gFnPc6	stará
<g/>
,	,	kIx,	,
rozlehlých	rozlehlý	k2eAgInPc6d1	rozlehlý
<g/>
,	,	kIx,	,
jehličnatých	jehličnatý	k2eAgInPc6d1	jehličnatý
nebo	nebo	k8xC	nebo
smíšených	smíšený	k2eAgInPc6d1	smíšený
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
se	s	k7c7	s
vzrostlými	vzrostlý	k2eAgFnPc7d1	vzrostlá
borovicemi	borovice	k1gFnPc7	borovice
<g/>
,	,	kIx,	,
početnými	početný	k2eAgInPc7d1	početný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepříliš	příliš	k6eNd1	příliš
velkými	velký	k2eAgFnPc7d1	velká
mýtinami	mýtina	k1gFnPc7	mýtina
a	a	k8xC	a
bohatým	bohatý	k2eAgNnSc7d1	bohaté
zastoupením	zastoupení	k1gNnSc7	zastoupení
bobulonosných	bobulonosný	k2eAgInPc2d1	bobulonosný
keříků	keřík	k1gInPc2	keřík
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
borůvek	borůvka	k1gFnPc2	borůvka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tok	tok	k1gInSc4	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
žijí	žít	k5eAaImIp3nP	žít
obě	dva	k4xCgNnPc4	dva
pohlaví	pohlaví	k1gNnPc4	pohlaví
odděleně	odděleně	k6eAd1	odděleně
a	a	k8xC	a
setkávají	setkávat	k5eAaImIp3nP	setkávat
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
jen	jen	k9	jen
během	během	k7c2	během
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
probíhá	probíhat	k5eAaImIp3nS	probíhat
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
května	květen	k1gInSc2	květen
na	na	k7c6	na
tradičních	tradiční	k2eAgNnPc6d1	tradiční
místech	místo	k1gNnPc6	místo
(	(	kIx(	(
<g/>
tokaništích	tokaniště	k1gNnPc6	tokaniště
<g/>
)	)	kIx)	)
na	na	k7c6	na
silných	silný	k2eAgFnPc6d1	silná
větvích	větev	k1gFnPc6	větev
nebo	nebo	k8xC	nebo
v	v	k7c6	v
řídkém	řídký	k2eAgInSc6d1	řídký
porostu	porost	k1gInSc6	porost
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
večer	večer	k6eAd1	večer
předtím	předtím	k6eAd1	předtím
vydávají	vydávat	k5eAaPmIp3nP	vydávat
pronikavé	pronikavý	k2eAgInPc4d1	pronikavý
říhavé	říhavý	k2eAgInPc4d1	říhavý
zvuky	zvuk	k1gInPc4	zvuk
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
vržou	vrzat	k5eAaImIp3nP	vrzat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tokat	tokat	k5eAaImF	tokat
začínají	začínat	k5eAaImIp3nP	začínat
brzy	brzy	k6eAd1	brzy
při	při	k7c6	při
rozednění	rozednění	k1gNnSc6	rozednění
<g/>
.	.	kIx.	.
</s>
<s>
Tok	tok	k1gInSc1	tok
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
následujícími	následující	k2eAgFnPc7d1	následující
fázemi	fáze	k1gFnPc7	fáze
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
pukáním	pukání	k1gNnSc7	pukání
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
t-k	t	k?	t-k
t-k	t	k?	t-k
t-k	t	k?	t-k
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
trylkem	trylek	k1gInSc7	trylek
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zrychlené	zrychlený	k2eAgFnPc4d1	zrychlená
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
opakované	opakovaný	k2eAgNnSc1d1	opakované
"	"	kIx"	"
<g/>
pukání	pukání	k1gNnSc1	pukání
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
vylousknutím	vylousknutí	k1gNnSc7	vylousknutí
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
znějící	znějící	k2eAgMnPc1d1	znějící
jako	jako	k8xC	jako
mlasknutí	mlasknutý	k2eAgMnPc1d1	mlasknutý
jazykem	jazyk	k1gInSc7	jazyk
<g/>
;	;	kIx,	;
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
zcela	zcela	k6eAd1	zcela
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
broušením	broušení	k1gNnSc7	broušení
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
trvá	trvat	k5eAaImIp3nS	trvat
3-5	[number]	k4	3-5
sekund	sekunda	k1gFnPc2	sekunda
a	a	k8xC	a
připomíná	připomínat	k5eAaImIp3nS	připomínat
zvuk	zvuk	k1gInSc1	zvuk
rychlého	rychlý	k2eAgNnSc2d1	rychlé
přejíždění	přejíždění	k1gNnSc2	přejíždění
brousku	brousek	k1gInSc2	brousek
po	po	k7c6	po
kose	kosa	k1gFnSc6	kosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
krátkého	krátký	k2eAgInSc2d1	krátký
okamžiku	okamžik	k1gInSc2	okamžik
při	při	k7c6	při
"	"	kIx"	"
<g/>
broušení	broušení	k1gNnSc6	broušení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
neslyší	slyšet	k5eNaImIp3nS	slyšet
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
hluší	hluchý	k2eAgMnPc1d1	hluchý
-	-	kIx~	-
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
hlušec	hlušec	k1gMnSc1	hlušec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
velmi	velmi	k6eAd1	velmi
ostražití	ostražitý	k2eAgMnPc1d1	ostražitý
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
hlučně	hlučně	k6eAd1	hlučně
odlétají	odlétat	k5eAaImIp3nP	odlétat
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
však	však	k9	však
bývají	bývat	k5eAaImIp3nP	bývat
zaznamenáváni	zaznamenáván	k2eAgMnPc1d1	zaznamenáván
samci	samec	k1gMnPc1	samec
s	s	k7c7	s
narušeným	narušený	k2eAgNnSc7d1	narušené
chováním	chování	k1gNnSc7	chování
<g/>
,	,	kIx,	,
projevujícím	projevující	k2eAgMnSc7d1	projevující
se	se	k3xPyFc4	se
neobvyklou	obvyklý	k2eNgFnSc7d1	neobvyklá
nebojácností	nebojácnost	k1gFnSc7	nebojácnost
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
agresivitou	agresivita	k1gFnSc7	agresivita
vůči	vůči	k7c3	vůči
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
postupně	postupně	k6eAd1	postupně
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
několik	několik	k4yIc4	několik
tokanišť	tokaniště	k1gNnPc2	tokaniště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
páří	pářit	k5eAaImIp3nS	pářit
s	s	k7c7	s
dominantním	dominantní	k2eAgInSc7d1	dominantní
samcem	samec	k1gInSc7	samec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
je	být	k5eAaImIp3nS	být
důlek	důlek	k1gInSc4	důlek
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
při	při	k7c6	při
kmeni	kmen	k1gInSc6	kmen
stromu	strom	k1gInSc2	strom
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vývratem	vývrat	k1gInSc7	vývrat
nebo	nebo	k8xC	nebo
hromadou	hromada	k1gFnSc7	hromada
klestí	klest	k1gFnPc2	klest
<g/>
,	,	kIx,	,
vystlaný	vystlaný	k2eAgInSc1d1	vystlaný
rostlinným	rostlinný	k2eAgInSc7d1	rostlinný
materiálem	materiál	k1gInSc7	materiál
a	a	k8xC	a
peřím	peří	k1gNnSc7	peří
<g/>
.	.	kIx.	.
</s>
<s>
Snůška	snůška	k1gFnSc1	snůška
čítá	čítat	k5eAaImIp3nS	čítat
9-10	[number]	k4	9-10
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
světle	světlo	k1gNnSc6	světlo
žlutě	žlutě	k6eAd1	žlutě
hnědavých	hnědavý	k2eAgInPc2d1	hnědavý
<g/>
,	,	kIx,	,
jemně	jemně	k6eAd1	jemně
tmavě	tmavě	k6eAd1	tmavě
skvrnitých	skvrnitý	k2eAgNnPc2d1	skvrnité
vajec	vejce	k1gNnPc2	vejce
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
56,3	[number]	k4	56,3
x	x	k?	x
41,7	[number]	k4	41,7
mm	mm	kA	mm
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
inkubace	inkubace	k1gFnSc1	inkubace
trvá	trvat	k5eAaImIp3nS	trvat
26-28	[number]	k4	26-28
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
opouštějí	opouštět	k5eAaImIp3nP	opouštět
hnízdo	hnízdo	k1gNnSc4	hnízdo
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
a	a	k8xC	a
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
dnech	den	k1gInPc6	den
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
náchylná	náchylný	k2eAgFnSc1d1	náchylná
na	na	k7c4	na
vlhko	vlhko	k1gNnSc4	vlhko
<g/>
,	,	kIx,	,
chlad	chlad	k1gInSc4	chlad
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc4	nedostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vývoj	vývoj	k1gInSc1	vývoj
je	být	k5eAaImIp3nS	být
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
;	;	kIx,	;
po	po	k7c6	po
8-9	[number]	k4	8-9
dnech	den	k1gInPc6	den
již	již	k6eAd1	již
poletují	poletovat	k5eAaImIp3nP	poletovat
<g/>
,	,	kIx,	,
po	po	k7c6	po
14	[number]	k4	14
dnech	den	k1gInPc6	den
dokáží	dokázat	k5eAaPmIp3nP	dokázat
létat	létat	k5eAaImF	létat
i	i	k9	i
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
a	a	k8xC	a
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
30-40	[number]	k4	30-40
dnů	den	k1gInPc2	den
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
opeřují	opeřovat	k5eAaImIp3nP	opeřovat
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
samicí	samice	k1gFnSc7	samice
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
až	až	k9	až
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
do	do	k7c2	do
menších	malý	k2eAgNnPc2d2	menší
hejn	hejno	k1gNnPc2	hejno
podle	podle	k7c2	podle
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgFnPc1d1	pohlavní
dospělosti	dospělost	k1gFnPc1	dospělost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
kalendářním	kalendářní	k2eAgInSc6d1	kalendářní
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
do	do	k7c2	do
jara	jaro	k1gNnSc2	jaro
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
převládá	převládat	k5eAaImIp3nS	převládat
jehličí	jehličí	k1gNnSc1	jehličí
a	a	k8xC	a
pupeny	pupen	k1gInPc1	pupen
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
požírá	požírat	k5eAaImIp3nS	požírat
také	také	k9	také
různé	různý	k2eAgInPc4d1	různý
plody	plod	k1gInPc4	plod
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
bobule	bobule	k1gFnSc1	bobule
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výhonky	výhonek	k1gInPc1	výhonek
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
lístky	lístek	k1gInPc4	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Živočišná	živočišný	k2eAgFnSc1d1	živočišná
složka	složka	k1gFnSc1	složka
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
hlavně	hlavně	k9	hlavně
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
jen	jen	k9	jen
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mláďat	mládě	k1gNnPc2	mládě
je	být	k5eAaImIp3nS	být
potrava	potrava	k1gFnSc1	potrava
zpočátku	zpočátku	k6eAd1	zpočátku
výhradně	výhradně	k6eAd1	výhradně
živočišná	živočišný	k2eAgFnSc1d1	živočišná
s	s	k7c7	s
postupně	postupně	k6eAd1	postupně
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
podílem	podíl	k1gInSc7	podíl
bobulí	bobule	k1gFnPc2	bobule
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
hledá	hledat	k5eAaImIp3nS	hledat
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
i	i	k8xC	i
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Tetřev	tetřev	k1gMnSc1	tetřev
hlušec	hlušec	k1gMnSc1	hlušec
patřil	patřit	k5eAaImAgMnS	patřit
dříve	dříve	k6eAd2	dříve
mezi	mezi	k7c4	mezi
oblíbenou	oblíbený	k2eAgFnSc4d1	oblíbená
lovnou	lovný	k2eAgFnSc4d1	lovná
zvěř	zvěř	k1gFnSc4	zvěř
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
maso	maso	k1gNnSc1	maso
se	se	k3xPyFc4	se
počítalo	počítat	k5eAaImAgNnS	počítat
mezi	mezi	k7c4	mezi
velikonoční	velikonoční	k2eAgFnPc4d1	velikonoční
pochoutky	pochoutka	k1gFnPc4	pochoutka
a	a	k8xC	a
poddaní	poddaný	k1gMnPc1	poddaný
měli	mít	k5eAaImAgMnP	mít
někde	někde	k6eAd1	někde
povinnost	povinnost	k1gFnSc4	povinnost
chytat	chytat	k5eAaImF	chytat
tetřevy	tetřev	k1gMnPc4	tetřev
a	a	k8xC	a
odvádět	odvádět	k5eAaImF	odvádět
je	on	k3xPp3gFnPc4	on
vrchnosti	vrchnost	k1gFnPc4	vrchnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
dřívější	dřívější	k2eAgNnPc1d1	dřívější
rozšíření	rozšíření	k1gNnPc1	rozšíření
připomínají	připomínat	k5eAaImIp3nP	připomínat
zejména	zejména	k9	zejména
místní	místní	k2eAgNnPc1d1	místní
jména	jméno	k1gNnPc1	jméno
odvozená	odvozený	k2eAgNnPc1d1	odvozené
od	od	k7c2	od
tokanišť	tokaniště	k1gNnPc2	tokaniště
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
lovným	lovný	k2eAgMnPc3d1	lovný
druhům	druh	k1gMnPc3	druh
patřil	patřit	k5eAaImAgInS	patřit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
je	být	k5eAaImIp3nS	být
celoročně	celoročně	k6eAd1	celoročně
hájen	hájen	k2eAgInSc1d1	hájen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
příloze	příloha	k1gFnSc6	příloha
I	i	k8xC	i
Směrnice	směrnice	k1gFnPc4	směrnice
o	o	k7c6	o
ptácích	pták	k1gMnPc6	pták
a	a	k8xC	a
podle	podle	k7c2	podle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
faktory	faktor	k1gInPc7	faktor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
tetřeva	tetřev	k1gMnSc4	tetřev
nejvíce	hodně	k6eAd3	hodně
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
hlavně	hlavně	k9	hlavně
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
lesnické	lesnický	k2eAgNnSc1d1	lesnické
hospodaření	hospodaření	k1gNnSc1	hospodaření
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gFnSc1	vedoucí
k	k	k7c3	k
ničení	ničení	k1gNnSc3	ničení
přirozeného	přirozený	k2eAgInSc2d1	přirozený
biotopu	biotop	k1gInSc2	biotop
<g/>
,	,	kIx,	,
nadměrné	nadměrný	k2eAgNnSc4d1	nadměrné
rušení	rušení	k1gNnSc4	rušení
<g/>
,	,	kIx,	,
narušující	narušující	k2eAgInSc4d1	narušující
průběh	průběh	k1gInSc4	průběh
toku	tok	k1gInSc2	tok
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
úspěšnost	úspěšnost	k1gFnSc4	úspěšnost
hnízdění	hnízdění	k1gNnSc2	hnízdění
<g/>
,	,	kIx,	,
a	a	k8xC	a
vysoké	vysoký	k2eAgInPc1d1	vysoký
stavy	stav	k1gInPc1	stav
predátorů	predátor	k1gMnPc2	predátor
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
zvláště	zvláště	k6eAd1	zvláště
kuna	kuna	k1gFnSc1	kuna
a	a	k8xC	a
liška	liška	k1gFnSc1	liška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
ornitologická	ornitologický	k2eAgFnSc1d1	ornitologická
-	-	kIx~	-
Pták	pták	k1gMnSc1	pták
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
==	==	k?	==
Heraldika	heraldika	k1gFnSc1	heraldika
==	==	k?	==
</s>
</p>
<p>
<s>
Tokající	tokající	k2eAgMnSc1d1	tokající
tetřev	tetřev	k1gMnSc1	tetřev
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
české	český	k2eAgFnSc2d1	Česká
obce	obec	k1gFnSc2	obec
Obecnice	obecnice	k1gFnSc2	obecnice
<g/>
,	,	kIx,	,
přítomen	přítomen	k2eAgMnSc1d1	přítomen
je	být	k5eAaImIp3nS	být
i	i	k9	i
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
znacích	znak	k1gInPc6	znak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tetřev	tetřev	k1gMnSc1	tetřev
hlušec	hlušec	k1gMnSc1	hlušec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Tetrao	Tetrao	k1gMnSc1	Tetrao
urogallus	urogallus	k1gMnSc1	urogallus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Hlas	hlas	k1gInSc1	hlas
tetřeva	tetřev	k1gMnSc2	tetřev
hlušce	hlušec	k1gMnSc2	hlušec
</s>
</p>
<p>
<s>
http://www.biolib.cz/cz/taxon/id8559/pos0,200/	[url]	k4	http://www.biolib.cz/cz/taxon/id8559/pos0,200/
</s>
</p>
<p>
<s>
http://www.birdlife.cz/index.php?ID=2262	[url]	k4	http://www.birdlife.cz/index.php?ID=2262
</s>
</p>
<p>
<s>
http://www.naturfoto.cz/tetrev-hlusec-fotografie-15997.html	[url]	k1gMnSc1	http://www.naturfoto.cz/tetrev-hlusec-fotografie-15997.html
</s>
</p>
<p>
<s>
http://www.myslivost.cz/Casopis-Myslivost/Myslivost/2012/Duben---2012/Tetrev-hlusec---ptak-roku.aspx	[url]	k1gInSc1	http://www.myslivost.cz/Casopis-Myslivost/Myslivost/2012/Duben---2012/Tetrev-hlusec---ptak-roku.aspx
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
tetřev	tetřev	k1gMnSc1	tetřev
hlušec	hlušec	k1gMnSc1	hlušec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
