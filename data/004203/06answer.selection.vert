<s>
Získal	získat	k5eAaPmAgInS	získat
čtyři	čtyři	k4xCgInPc1	čtyři
České	český	k2eAgInPc1d1	český
lvy	lev	k1gInPc1	lev
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
ženská	ženský	k2eAgFnSc1d1	ženská
role	role	k1gFnSc1	role
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc1	scénář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
byl	být	k5eAaImAgInS	být
vyslán	vyslat	k5eAaPmNgInS	vyslat
na	na	k7c6	na
77	[number]	k4	77
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
předávání	předávání	k1gNnSc2	předávání
Oscarů	Oscar	k1gInPc2	Oscar
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neprošel	projít	k5eNaPmAgInS	projít
nominacemi	nominace	k1gFnPc7	nominace
<g/>
.	.	kIx.	.
</s>
