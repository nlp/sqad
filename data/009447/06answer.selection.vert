<s>
Konstruktor	konstruktor	k1gMnSc1	konstruktor
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
ostatním	ostatní	k2eAgFnPc3d1	ostatní
metodám	metoda	k1gFnPc3	metoda
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
nikdy	nikdy	k6eAd1	nikdy
explicitní	explicitní	k2eAgInSc1d1	explicitní
návratový	návratový	k2eAgInSc1d1	návratový
typ	typ	k1gInSc1	typ
<g/>
,	,	kIx,	,
nedědí	dědit	k5eNaImIp3nS	dědit
se	se	k3xPyFc4	se
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
má	mít	k5eAaImIp3nS	mít
jiná	jiný	k2eAgNnPc4d1	jiné
pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
modifikátory	modifikátor	k1gInPc4	modifikátor
přístupu	přístup	k1gInSc2	přístup
<g/>
.	.	kIx.	.
</s>
