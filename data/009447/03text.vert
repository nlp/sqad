<p>
<s>
Konstruktor	konstruktor	k1gMnSc1	konstruktor
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
ctor	ctor	k1gInSc1	ctor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
objektově	objektově	k6eAd1	objektově
orientovaném	orientovaný	k2eAgNnSc6d1	orientované
programování	programování	k1gNnSc6	programování
speciální	speciální	k2eAgFnSc1d1	speciální
metoda	metoda	k1gFnSc1	metoda
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
volá	volat	k5eAaImIp3nS	volat
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
vytváření	vytváření	k1gNnSc2	vytváření
instance	instance	k1gFnSc2	instance
této	tento	k3xDgFnSc2	tento
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konstruktor	konstruktor	k1gMnSc1	konstruktor
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
ostatním	ostatní	k2eAgFnPc3d1	ostatní
metodám	metoda	k1gFnPc3	metoda
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
nikdy	nikdy	k6eAd1	nikdy
explicitní	explicitní	k2eAgInSc1d1	explicitní
návratový	návratový	k2eAgInSc1d1	návratový
typ	typ	k1gInSc1	typ
<g/>
,	,	kIx,	,
nedědí	dědit	k5eNaImIp3nS	dědit
se	se	k3xPyFc4	se
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
má	mít	k5eAaImIp3nS	mít
jiná	jiný	k2eAgNnPc4d1	jiné
pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
modifikátory	modifikátor	k1gInPc4	modifikátor
přístupu	přístup	k1gInSc2	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Konstruktory	konstruktor	k1gMnPc4	konstruktor
inicializují	inicializovat	k5eAaImIp3nP	inicializovat
datové	datový	k2eAgMnPc4d1	datový
členy	člen	k1gMnPc4	člen
instance	instance	k1gFnSc2	instance
<g/>
.	.	kIx.	.
</s>
<s>
Správně	správně	k6eAd1	správně
napsaný	napsaný	k2eAgMnSc1d1	napsaný
konstruktor	konstruktor	k1gMnSc1	konstruktor
nechá	nechat	k5eAaPmIp3nS	nechat
objekt	objekt	k1gInSc1	objekt
v	v	k7c6	v
"	"	kIx"	"
<g/>
platném	platný	k2eAgInSc6d1	platný
<g/>
"	"	kIx"	"
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
programovacích	programovací	k2eAgInPc2d1	programovací
jazyků	jazyk	k1gInPc2	jazyk
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
konstruktor	konstruktor	k1gMnSc1	konstruktor
přetížen	přetížen	k2eAgMnSc1d1	přetížen
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jedna	jeden	k4xCgFnSc1	jeden
třída	třída	k1gFnSc1	třída
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
konstruktorů	konstruktor	k1gMnPc2	konstruktor
s	s	k7c7	s
odlišnými	odlišný	k2eAgInPc7d1	odlišný
parametry	parametr	k1gInPc7	parametr
a	a	k8xC	a
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
funkcionalitou	funkcionalita	k1gFnSc7	funkcionalita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
C	C	kA	C
<g/>
++	++	k?	++
<g/>
)	)	kIx)	)
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
speciální	speciální	k2eAgInPc4d1	speciální
typy	typ	k1gInPc4	typ
konstruktorů	konstruktor	k1gMnPc2	konstruktor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Defaultní	Defaultní	k2eAgMnSc1d1	Defaultní
konstruktor	konstruktor	k1gMnSc1	konstruktor
–	–	k?	–
konstruktor	konstruktor	k1gMnSc1	konstruktor
bez	bez	k7c2	bez
parametrů	parametr	k1gInPc2	parametr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kopírovací	kopírovací	k2eAgMnSc1d1	kopírovací
konstruktor	konstruktor	k1gMnSc1	konstruktor
–	–	k?	–
konstruktor	konstruktor	k1gMnSc1	konstruktor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
jeden	jeden	k4xCgInSc1	jeden
parametr	parametr	k1gInSc1	parametr
typu	typ	k1gInSc2	typ
dané	daný	k2eAgFnSc2d1	daná
třídy	třída	k1gFnSc2	třída
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
reference	reference	k1gFnSc1	reference
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Implicitní	implicitní	k2eAgMnSc1d1	implicitní
konstruktor	konstruktor	k1gMnSc1	konstruktor
–	–	k?	–
konstruktor	konstruktor	k1gMnSc1	konstruktor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
výslovně	výslovně	k6eAd1	výslovně
definován	definovat	k5eAaBmNgInS	definovat
programátorem	programátor	k1gInSc7	programátor
a	a	k8xC	a
kompilátor	kompilátor	k1gInSc1	kompilátor
jazyka	jazyk	k1gInSc2	jazyk
ho	on	k3xPp3gMnSc4	on
umí	umět	k5eAaImIp3nS	umět
vytvořit	vytvořit	k5eAaPmF	vytvořit
automaticky	automaticky	k6eAd1	automaticky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
C	C	kA	C
<g/>
++	++	k?	++
jsou	být	k5eAaImIp3nP	být
jak	jak	k6eAd1	jak
defaultní	defaultní	k2eAgFnPc1d1	defaultní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
kopírovací	kopírovací	k2eAgMnSc1d1	kopírovací
konstruktor	konstruktor	k1gMnSc1	konstruktor
implicitní	implicitní	k2eAgMnSc1d1	implicitní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Destruktor	destruktor	k1gInSc1	destruktor
</s>
</p>
