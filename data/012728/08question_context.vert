<s>
Elon	Elon	k1gMnSc1	Elon
Musk	Musk	k1gMnSc1	Musk
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
,	,	kIx,	,
inženýr	inženýr	k1gMnSc1	inženýr
a	a	k8xC	a
filantrop	filantrop	k1gMnSc1	filantrop
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
jihoafrické	jihoafrický	k2eAgNnSc4d1	jihoafrické
<g/>
,	,	kIx,	,
kanadské	kanadský	k2eAgNnSc4d1	kanadské
a	a	k8xC	a
americké	americký	k2eAgNnSc4d1	americké
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Spoluvlastnil	spoluvlastnit	k5eAaImAgInS	spoluvlastnit
internetový	internetový	k2eAgInSc1d1	internetový
platební	platební	k2eAgInSc1d1	platební
systém	systém	k1gInSc1	systém
PayPal	PayPal	k1gInSc1	PayPal
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgInS	založit
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
společnost	společnost	k1gFnSc4	společnost
SpaceX	SpaceX	k1gFnSc2	SpaceX
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
investorem	investor	k1gMnSc7	investor
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
CEO	CEO	kA	CEO
vede	vést	k5eAaImIp3nS	vést
automobilku	automobilka	k1gFnSc4	automobilka
Tesla	Tesla	k1gFnSc1	Tesla
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
</s>
