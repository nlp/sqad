<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
památky	památka	k1gFnSc2	památka
obětí	oběť	k1gFnPc2	oběť
holokaustu	holokaust	k1gInSc2	holokaust
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vyhlášen	vyhlášen	k2eAgInSc1d1	vyhlášen
Valným	valný	k2eAgNnSc7d1	Valné
shromážděním	shromáždění	k1gNnSc7	shromáždění
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
na	na	k7c4	na
jeho	jeho	k3xOp3gNnPc2	jeho
42	[number]	k4	42
<g/>
.	.	kIx.	.
plenárním	plenární	k2eAgNnSc6d1	plenární
zasedání	zasedání	k1gNnSc6	zasedání
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
má	mít	k5eAaImIp3nS	mít
připomínat	připomínat	k5eAaImF	připomínat
utrpení	utrpení	k1gNnSc4	utrpení
přibližně	přibližně	k6eAd1	přibližně
šesti	šest	k4xCc2	šest
milionů	milion	k4xCgInPc2	milion
židovských	židovská	k1gFnPc2	židovská
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
2	[number]	k4	2
milionů	milion	k4xCgInPc2	milion
Romů	Rom	k1gMnPc2	Rom
<g/>
,	,	kIx,	,
15	[number]	k4	15
tisíc	tisíc	k4xCgInSc1	tisíc
homosexuálů	homosexuál	k1gMnPc2	homosexuál
a	a	k8xC	a
milionů	milion	k4xCgInPc2	milion
dalších	další	k2eAgFnPc2d1	další
nevinných	vinný	k2eNgFnPc2d1	nevinná
obětí	oběť	k1gFnPc2	oběť
v	v	k7c6	v
době	doba	k1gFnSc6	doba
holokaustu	holokaust	k1gInSc2	holokaust
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
bylo	být	k5eAaImAgNnS	být
vybráno	vybrat	k5eAaPmNgNnS	vybrat
záměrně	záměrně	k6eAd1	záměrně
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
osvobozen	osvobozen	k2eAgInSc1d1	osvobozen
nacistický	nacistický	k2eAgInSc1d1	nacistický
koncentrační	koncentrační	k2eAgInSc1d1	koncentrační
a	a	k8xC	a
vyhlazovací	vyhlazovací	k2eAgInSc1d1	vyhlazovací
tábor	tábor	k1gInSc1	tábor
Auschwitz-Birkenau	Auschwitz-Birkenaus	k1gInSc2	Auschwitz-Birkenaus
(	(	kIx(	(
<g/>
Osvětim-Březinka	Osvětim-Březinka	k1gFnSc1	Osvětim-Březinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
byl	být	k5eAaImAgInS	být
podán	podat	k5eAaPmNgInS	podat
Izraelem	Izrael	k1gInSc7	Izrael
<g/>
,	,	kIx,	,
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
Austrálií	Austrálie	k1gFnSc7	Austrálie
<g/>
,	,	kIx,	,
Kanadou	Kanada	k1gFnSc7	Kanada
a	a	k8xC	a
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Podpořilo	podpořit	k5eAaPmAgNnS	podpořit
jej	on	k3xPp3gNnSc2	on
91	[number]	k4	91
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
