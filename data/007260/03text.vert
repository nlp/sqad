<s>
Primáti	primát	k1gMnPc1	primát
(	(	kIx(	(
<g/>
Primates	Primates	k1gInSc1	Primates
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
řád	řád	k1gInSc4	řád
živorodých	živorodý	k2eAgMnPc2d1	živorodý
placentálních	placentální	k2eAgMnPc2d1	placentální
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnPc4d1	zahrnující
poloopice	poloopice	k1gFnPc4	poloopice
a	a	k8xC	a
vyšší	vysoký	k2eAgInPc4d2	vyšší
primáty	primát	k1gInPc4	primát
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	on	k3xPp3gInPc4	on
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
primatologie	primatologie	k1gFnSc1	primatologie
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
druhů	druh	k1gInPc2	druh
primátů	primát	k1gInPc2	primát
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
nebo	nebo	k8xC	nebo
subtropech	subtropy	k1gInPc6	subtropy
Starého	Starého	k2eAgInSc2d1	Starého
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
poměrně	poměrně	k6eAd1	poměrně
nejsnáze	snadno	k6eAd3	snadno
uživit	uživit	k5eAaPmF	uživit
i	i	k8xC	i
přežít	přežít	k5eAaPmF	přežít
nepřízeň	nepřízeň	k1gFnSc4	nepřízeň
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Jedině	jedině	k6eAd1	jedině
rod	rod	k1gInSc1	rod
Homo	Homo	k6eAd1	Homo
obývá	obývat	k5eAaImIp3nS	obývat
trvale	trvale	k6eAd1	trvale
souš	souš	k1gFnSc4	souš
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnSc2d1	celá
zeměkoule	zeměkoule	k1gFnSc2	zeměkoule
<g/>
.	.	kIx.	.
</s>
<s>
Poloopice	poloopice	k1gFnPc1	poloopice
a	a	k8xC	a
lidoopi	lidoop	k1gMnPc1	lidoop
nejčastěji	často	k6eAd3	často
obývají	obývat	k5eAaImIp3nP	obývat
Jihovýchodní	jihovýchodní	k2eAgFnSc4d1	jihovýchodní
Asii	Asie	k1gFnSc4	Asie
a	a	k8xC	a
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc4d3	nejstarší
historii	historie	k1gFnSc4	historie
řádu	řád	k1gInSc2	řád
primáti	primát	k1gMnPc1	primát
zjišťujeme	zjišťovat	k5eAaImIp1nP	zjišťovat
z	z	k7c2	z
mnohdy	mnohdy	k6eAd1	mnohdy
neúplných	úplný	k2eNgInPc2d1	neúplný
zlomkovitých	zlomkovitý	k2eAgInPc2d1	zlomkovitý
fosilních	fosilní	k2eAgInPc2d1	fosilní
nálezu	nález	k1gInSc3	nález
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
protichůdně	protichůdně	k6eAd1	protichůdně
interpretovaných	interpretovaný	k2eAgInPc2d1	interpretovaný
<g/>
,	,	kIx,	,
a	a	k8xC	a
odvozujeme	odvozovat	k5eAaImIp1nP	odvozovat
od	od	k7c2	od
genetických	genetický	k2eAgFnPc2d1	genetická
studií	studie	k1gFnPc2	studie
žijících	žijící	k2eAgMnPc2d1	žijící
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumem	výzkum	k1gInSc7	výzkum
molekulární	molekulární	k2eAgFnSc2d1	molekulární
genetiky	genetika	k1gFnSc2	genetika
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
primáti	primát	k1gMnPc1	primát
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
evoluční	evoluční	k2eAgFnSc2d1	evoluční
větve	větev	k1gFnSc2	větev
savců	savec	k1gMnPc2	savec
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
Euarchontoglires	Euarchontogliresa	k1gFnPc2	Euarchontogliresa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
štěpí	štěpit	k5eAaImIp3nS	štěpit
na	na	k7c4	na
skupinu	skupina	k1gFnSc4	skupina
Glires	Gliresa	k1gFnPc2	Gliresa
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
hlodavci	hlodavec	k1gMnPc1	hlodavec
(	(	kIx(	(
<g/>
Rodentia	Rodentia	k1gFnSc1	Rodentia
<g/>
)	)	kIx)	)
a	a	k8xC	a
zajícovci	zajícovec	k1gMnPc1	zajícovec
(	(	kIx(	(
<g/>
Lagomorpha	Lagomorpha	k1gMnSc1	Lagomorpha
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
skupinu	skupina	k1gFnSc4	skupina
Euarchonta	Euarchont	k1gInSc2	Euarchont
<g/>
,	,	kIx,	,
do	do	k7c2	do
která	který	k3yIgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
primáti	primát	k1gMnPc1	primát
<g/>
,	,	kIx,	,
letuchy	letuch	k1gMnPc4	letuch
(	(	kIx(	(
<g/>
Dermaptera	Dermapter	k1gMnSc4	Dermapter
<g/>
)	)	kIx)	)
a	a	k8xC	a
tany	tany	k?	tany
(	(	kIx(	(
<g/>
Scandentia	Scandentius	k1gMnSc2	Scandentius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Linie	linie	k1gFnSc1	linie
Euarchonta	Euarchonta	k1gFnSc1	Euarchonta
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
před	před	k7c4	před
87,9	[number]	k4	87,9
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
<g/>
,	,	kIx,	,
tany	tany	k?	tany
se	se	k3xPyFc4	se
od	od	k7c2	od
primátů	primát	k1gInPc2	primát
oddělily	oddělit	k5eAaPmAgFnP	oddělit
před	před	k7c7	před
86,2	[number]	k4	86,2
a	a	k8xC	a
letuchy	letuch	k1gInPc1	letuch
před	před	k7c4	před
79,6	[number]	k4	79,6
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
ve	v	k7c6	v
svrchní	svrchní	k2eAgFnSc6d1	svrchní
křídě	křída	k1gFnSc6	křída
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
sporného	sporný	k2eAgMnSc4d1	sporný
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
primáta	primát	k1gMnSc4	primát
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
Purgatorius	Purgatorius	k1gInSc1	Purgatorius
<g/>
,	,	kIx,	,
rod	rod	k1gInSc1	rod
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
druhohor	druhohory	k1gFnPc2	druhohory
a	a	k8xC	a
třetihor	třetihory	k1gFnPc2	třetihory
(	(	kIx(	(
<g/>
stáří	stáří	k1gNnSc2	stáří
65-63	[number]	k4	65-63
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
má	mít	k5eAaImIp3nS	mít
nesporné	sporný	k2eNgInPc4d1	nesporný
znaky	znak	k1gInPc4	znak
blízkého	blízký	k2eAgInSc2d1	blízký
příbuzného	příbuzný	k2eAgInSc2d1	příbuzný
primátů	primát	k1gInPc2	primát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
řazen	řadit	k5eAaImNgInS	řadit
do	do	k7c2	do
samostatného	samostatný	k2eAgInSc2d1	samostatný
řádu	řád	k1gInSc2	řád
Primatiformes	Primatiformesa	k1gFnPc2	Primatiformesa
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
za	za	k7c2	za
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
prokazatelné	prokazatelný	k2eAgMnPc4d1	prokazatelný
první	první	k4xOgInPc4	první
primáty	primát	k1gInPc4	primát
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
zástupci	zástupce	k1gMnPc1	zástupce
podřádu	podřád	k1gInSc2	podřád
Plesiadapiformes	Plesiadapiformes	k1gInSc1	Plesiadapiformes
<g/>
,	,	kIx,	,
např.	např.	kA	např.
rody	rod	k1gInPc1	rod
Plesiadapis	Plesiadapis	k1gFnSc2	Plesiadapis
<g/>
,	,	kIx,	,
Pandemonium	pandemonium	k1gNnSc1	pandemonium
<g/>
,	,	kIx,	,
Ignaciaus	Ignaciaus	k1gInSc1	Ignaciaus
a	a	k8xC	a
Carpolestes	Carpolestes	k1gInSc1	Carpolestes
<g/>
.	.	kIx.	.
</s>
<s>
Plesiadapiformes	Plesiadapiformes	k1gMnSc1	Plesiadapiformes
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
primátů	primát	k1gMnPc2	primát
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
primáti	primát	k1gMnPc1	primát
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nazývají	nazývat	k5eAaImIp3nP	nazývat
Euprimates	Euprimates	k1gInSc4	Euprimates
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
až	až	k9	až
rody	rod	k1gInPc1	rod
řazené	řazený	k2eAgInPc1d1	řazený
do	do	k7c2	do
podřádu	podřád	k1gInSc2	podřád
Adapiformes	Adapiformes	k1gInSc1	Adapiformes
mají	mít	k5eAaImIp3nP	mít
všechny	všechen	k3xTgInPc1	všechen
znaky	znak	k1gInPc1	znak
důležité	důležitý	k2eAgInPc1d1	důležitý
pro	pro	k7c4	pro
zařazení	zařazení	k1gNnSc4	zařazení
do	do	k7c2	do
řádu	řád	k1gInSc2	řád
Primates	Primatesa	k1gFnPc2	Primatesa
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
např.	např.	kA	např.
rody	rod	k1gInPc7	rod
Altiatlasius	Altiatlasius	k1gMnSc1	Altiatlasius
a	a	k8xC	a
Altanius	Altanius	k1gMnSc1	Altanius
(	(	kIx(	(
<g/>
z	z	k7c2	z
pozdního	pozdní	k2eAgInSc2d1	pozdní
paleocénu	paleocén	k1gInSc2	paleocén
asi	asi	k9	asi
před	před	k7c4	před
60	[number]	k4	60
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
podřád	podřád	k1gInSc1	podřád
řádu	řád	k1gInSc2	řád
Primates	Primatesa	k1gFnPc2	Primatesa
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
eocénu	eocén	k1gInSc6	eocén
asi	asi	k9	asi
před	před	k7c4	před
50	[number]	k4	50
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
rozvětvil	rozvětvit	k5eAaPmAgInS	rozvětvit
na	na	k7c4	na
nadčeleď	nadčeleď	k1gFnSc4	nadčeleď
Adapoidea	Adapoide	k1gInSc2	Adapoide
s	s	k7c7	s
rody	rod	k1gInPc7	rod
např.	např.	kA	např.
Donruselia	Donruselia	k1gFnSc1	Donruselia
a	a	k8xC	a
Cantius	Cantius	k1gInSc1	Cantius
a	a	k8xC	a
nadčeleď	nadčeleď	k1gFnSc1	nadčeleď
Omomyoidea	Omomyoidea	k1gFnSc1	Omomyoidea
s	s	k7c7	s
rody	rod	k1gInPc7	rod
Teilhardina	Teilhardin	k2eAgNnSc2d1	Teilhardin
a	a	k8xC	a
Steinius	Steinius	k1gInSc1	Steinius
<g/>
.	.	kIx.	.
</s>
<s>
Převládá	převládat	k5eAaImIp3nS	převládat
mínění	mínění	k1gNnSc1	mínění
<g/>
,	,	kIx,	,
že	že	k8xS	že
čeleď	čeleď	k1gFnSc1	čeleď
Adapoidea	Adapoide	k1gInSc2	Adapoide
je	být	k5eAaImIp3nS	být
blízce	blízce	k6eAd1	blízce
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
dnešním	dnešní	k2eAgMnSc7d1	dnešní
poloopicím	poloopice	k1gFnPc3	poloopice
a	a	k8xC	a
čeleď	čeleď	k1gFnSc1	čeleď
Omomyoidea	Omomyoide	k1gInSc2	Omomyoide
dnešním	dnešní	k2eAgMnPc3d1	dnešní
nártounům	nártoun	k1gMnPc3	nártoun
<g/>
.	.	kIx.	.
</s>
<s>
Primát	primát	k1gMnSc1	primát
Archicebus	Archicebus	k1gMnSc1	Archicebus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
před	před	k7c7	před
55	[number]	k4	55
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgInPc3d3	nejstarší
nalezeným	nalezený	k2eAgInPc3d1	nalezený
<g/>
.	.	kIx.	.
</s>
<s>
Panují	panovat	k5eAaImIp3nP	panovat
rozličné	rozličný	k2eAgInPc4d1	rozličný
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
vyšší	vysoký	k2eAgMnPc1d2	vyšší
primáti	primát	k1gMnPc1	primát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
úvahy	úvaha	k1gFnSc2	úvaha
připadají	připadat	k5eAaPmIp3nP	připadat
čtyři	čtyři	k4xCgInPc4	čtyři
možností	možnost	k1gFnPc2	možnost
<g/>
:	:	kIx,	:
z	z	k7c2	z
adapoidů	adapoid	k1gInPc2	adapoid
<g/>
,	,	kIx,	,
omomyoidů	omomyoid	k1gInPc2	omomyoid
nebo	nebo	k8xC	nebo
nártounů	nártoun	k1gMnPc2	nártoun
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
docela	docela	k6eAd1	docela
vážně	vážně	k6eAd1	vážně
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
možností	možnost	k1gFnPc2	možnost
vzniku	vznik	k1gInSc2	vznik
(	(	kIx(	(
<g/>
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
eocénu	eocén	k1gInSc6	eocén
asi	asi	k9	asi
před	před	k7c4	před
40	[number]	k4	40
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
z	z	k7c2	z
primátů	primát	k1gInPc2	primát
podobných	podobný	k2eAgInPc2d1	podobný
rodu	rod	k1gInSc3	rod
Altiatlasius	Altiatlasius	k1gMnSc1	Altiatlasius
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vyřešení	vyřešení	k1gNnSc4	vyřešení
této	tento	k3xDgFnSc2	tento
hádanky	hádanka	k1gFnSc2	hádanka
není	být	k5eNaImIp3nS	být
dosud	dosud	k6eAd1	dosud
dostatek	dostatek	k1gInSc4	dostatek
věrohodných	věrohodný	k2eAgInPc2d1	věrohodný
důkazů	důkaz	k1gInPc2	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
Propliothecidae	Propliothecida	k1gFnSc2	Propliothecida
(	(	kIx(	(
<g/>
z	z	k7c2	z
raného	raný	k2eAgInSc2d1	raný
oligocénu	oligocén	k1gInSc2	oligocén
asi	asi	k9	asi
před	před	k7c4	před
30	[number]	k4	30
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zastoupena	zastoupen	k2eAgFnSc1d1	zastoupena
rody	rod	k1gInPc4	rod
Propliopithecus	Propliopithecus	k1gInSc1	Propliopithecus
<g/>
,	,	kIx,	,
Moeripithecus	Moeripithecus	k1gInSc1	Moeripithecus
a	a	k8xC	a
Aegyptopithecus	Aegyptopithecus	k1gInSc1	Aegyptopithecus
které	který	k3yQgNnSc4	který
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
první	první	k4xOgInPc4	první
známé	známý	k2eAgInPc4d1	známý
vyšší	vysoký	k2eAgInPc4d2	vyšší
primáty	primát	k1gInPc4	primát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
období	období	k1gNnSc2	období
dělení	dělení	k1gNnSc2	dělení
na	na	k7c6	na
ploskonosé	ploskonosý	k2eAgFnSc6d1	ploskonosý
a	a	k8xC	a
úzkonosé	úzkonosý	k2eAgFnSc6d1	úzkonosý
<g/>
.	.	kIx.	.
</s>
<s>
Různorodost	různorodost	k1gFnSc1	různorodost
primátů	primát	k1gMnPc2	primát
je	být	k5eAaImIp3nS	být
veliká	veliký	k2eAgFnSc1d1	veliká
<g/>
,	,	kIx,	,
nejmenší	malý	k2eAgFnSc1d3	nejmenší
váží	vážit	k5eAaImIp3nS	vážit
70	[number]	k4	70
gramů	gram	k1gInPc2	gram
a	a	k8xC	a
největší	veliký	k2eAgInSc1d3	veliký
200	[number]	k4	200
kg	kg	kA	kg
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
jak	jak	k8xS	jak
noční	noční	k2eAgMnSc1d1	noční
lemur	lemur	k1gMnSc1	lemur
<g/>
,	,	kIx,	,
tak	tak	k9	tak
člověk	člověk	k1gMnSc1	člověk
přistanuvší	přistanuvší	k2eAgMnSc1d1	přistanuvší
na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
velká	velký	k2eAgFnSc1d1	velká
vnitrodruhová	vnitrodruhový	k2eAgFnSc1d1	vnitrodruhová
a	a	k8xC	a
mezidruhová	mezidruhový	k2eAgFnSc1d1	mezidruhová
potravní	potravní	k2eAgFnSc1d1	potravní
variabilita	variabilita	k1gFnSc1	variabilita
a	a	k8xC	a
ekologická	ekologický	k2eAgFnSc1d1	ekologická
diversita	diversita	k1gFnSc1	diversita
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
základní	základní	k2eAgFnPc4d1	základní
specifičnosti	specifičnost	k1gFnPc4	specifičnost
kostry	kostra	k1gFnSc2	kostra
primátů	primát	k1gMnPc2	primát
patří	patřit	k5eAaImIp3nP	patřit
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
rysy	rys	k1gInPc1	rys
lebky	lebka	k1gFnSc2	lebka
<g/>
,	,	kIx,	,
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
kost	kost	k1gFnSc1	kost
loketní	loketní	k2eAgFnSc1d1	loketní
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
končetině	končetina	k1gFnSc6	končetina
a	a	k8xC	a
kosti	kost	k1gFnPc4	kost
lýtkové	lýtkový	k2eAgFnPc4d1	lýtková
a	a	k8xC	a
holenní	holenní	k2eAgFnPc4d1	holenní
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
končetině	končetina	k1gFnSc6	končetina
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
klíční	klíční	k2eAgFnSc4d1	klíční
kost	kost	k1gFnSc4	kost
i	i	k9	i
velmi	velmi	k6eAd1	velmi
pohyblivý	pohyblivý	k2eAgInSc4d1	pohyblivý
ramenní	ramenní	k2eAgInSc4d1	ramenní
kloub	kloub	k1gInSc4	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
je	být	k5eAaImIp3nS	být
chápavá	chápavý	k2eAgFnSc1d1	chápavá
pětiprstá	pětiprstý	k2eAgFnSc1d1	pětiprstá
končetina	končetina	k1gFnSc1	končetina
<g/>
,	,	kIx,	,
palec	palec	k1gInSc1	palec
bývá	bývat	k5eAaImIp3nS	bývat
zakončen	zakončit	k5eAaPmNgInS	zakončit
nehtem	nehet	k1gInSc7	nehet
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgInPc4d1	ostatní
prsty	prst	k1gInPc4	prst
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
<g/>
.	.	kIx.	.
</s>
<s>
Žaludek	žaludek	k1gInSc1	žaludek
je	být	k5eAaImIp3nS	být
jednokomorový	jednokomorový	k2eAgInSc1d1	jednokomorový
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
i	i	k8xC	i
u	u	k7c2	u
vegetariánů	vegetarián	k1gMnPc2	vegetarián
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
polozavřené	polozavřený	k2eAgFnPc1d1	polozavřený
nebo	nebo	k8xC	nebo
zavřené	zavřený	k2eAgFnPc1d1	zavřená
očnice	očnice	k1gFnPc1	očnice
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
mozek	mozek	k1gInSc1	mozek
s	s	k7c7	s
progresivně	progresivně	k6eAd1	progresivně
zvětšeným	zvětšený	k2eAgInSc7d1	zvětšený
mozečkem	mozeček	k1gInSc7	mozeček
<g/>
.	.	kIx.	.
</s>
<s>
Relativně	relativně	k6eAd1	relativně
je	být	k5eAaImIp3nS	být
redukovaný	redukovaný	k2eAgInSc4d1	redukovaný
čich	čich	k1gInSc4	čich
(	(	kIx(	(
<g/>
mimo	mimo	k6eAd1	mimo
poloopic	poloopice	k1gFnPc2	poloopice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgInSc7d1	hlavní
smyslem	smysl	k1gInSc7	smysl
je	být	k5eAaImIp3nS	být
zrak	zrak	k1gInSc1	zrak
se	s	k7c7	s
stereoskopickým	stereoskopický	k2eAgNnSc7d1	stereoskopické
viděním	vidění	k1gNnSc7	vidění
(	(	kIx(	(
<g/>
mnohdy	mnohdy	k6eAd1	mnohdy
i	i	k9	i
barevným	barevný	k2eAgInSc7d1	barevný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
důležitý	důležitý	k2eAgInSc1d1	důležitý
je	být	k5eAaImIp3nS	být
sluch	sluch	k1gInSc1	sluch
i	i	k8xC	i
hmat	hmat	k1gInSc1	hmat
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
je	být	k5eAaImIp3nS	být
morfologie	morfologie	k1gFnSc1	morfologie
chrupu	chrup	k1gInSc2	chrup
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
řezáky	řezák	k1gInPc7	řezák
<g/>
,	,	kIx,	,
vyčnívajícím	vyčnívající	k2eAgInSc7d1	vyčnívající
špičákem	špičák	k1gInSc7	špičák
<g/>
,	,	kIx,	,
2	[number]	k4	2
až	až	k8xS	až
4	[number]	k4	4
třeňovými	třeňový	k2eAgInPc7d1	třeňový
zuby	zub	k1gInPc7	zub
a	a	k8xC	a
3	[number]	k4	3
stoličkami	stolička	k1gFnPc7	stolička
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
rysy	rys	k1gInPc7	rys
primátů	primát	k1gMnPc2	primát
patří	patřit	k5eAaImIp3nS	patřit
poměrně	poměrně	k6eAd1	poměrně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
vývoj	vývoj	k1gInSc1	vývoj
plodu	plod	k1gInSc2	plod
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
faktická	faktický	k2eAgFnSc1d1	faktická
neschopnost	neschopnost	k1gFnSc1	neschopnost
mláděte	mládě	k1gNnSc2	mládě
přežít	přežít	k5eAaPmF	přežít
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
bez	bez	k7c2	bez
péče	péče	k1gFnSc2	péče
matky	matka	k1gFnSc2	matka
i	i	k9	i
jen	jen	k9	jen
několik	několik	k4yIc1	několik
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
potřeba	potřeba	k6eAd1	potřeba
dlouhodobé	dlouhodobý	k2eAgFnPc4d1	dlouhodobá
starostlivosti	starostlivost	k1gFnPc4	starostlivost
spojené	spojený	k2eAgFnPc4d1	spojená
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
rodů	rod	k1gInPc2	rod
s	s	k7c7	s
učením	učení	k1gNnSc7	učení
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vyspělejších	vyspělý	k2eAgFnPc2d2	vyspělejší
skupin	skupina	k1gFnPc2	skupina
bývá	bývat	k5eAaImIp3nS	bývat
častá	častý	k2eAgFnSc1d1	častá
podpora	podpora	k1gFnSc1	podpora
druhých	druhý	k4xOgInPc2	druhý
při	při	k7c6	při
výchově	výchova	k1gFnSc6	výchova
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
relativně	relativně	k6eAd1	relativně
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
aktivní	aktivní	k2eAgInSc4d1	aktivní
život	život	k1gInSc4	život
mají	mít	k5eAaImIp3nP	mít
nízkou	nízký	k2eAgFnSc4d1	nízká
rozmnožovací	rozmnožovací	k2eAgFnSc4d1	rozmnožovací
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
,	,	kIx,	,
pozdě	pozdě	k6eAd1	pozdě
dospívají	dospívat	k5eAaImIp3nP	dospívat
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
rodí	rodit	k5eAaImIp3nP	rodit
po	po	k7c6	po
jednom	jeden	k4xCgNnSc6	jeden
nebo	nebo	k8xC	nebo
dvou	dva	k4xCgMnPc6	dva
potomcích	potomek	k1gMnPc6	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kapitola	kapitola	k1gFnSc1	kapitola
o	o	k7c6	o
chování	chování	k1gNnSc6	chování
primátů	primát	k1gInPc2	primát
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
nevztahuje	vztahovat	k5eNaImIp3nS	vztahovat
na	na	k7c4	na
rod	rod	k1gInSc4	rod
Homo	Homo	k1gMnSc1	Homo
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
zde	zde	k6eAd1	zde
popisovaného	popisovaný	k2eAgInSc2d1	popisovaný
již	již	k6eAd1	již
hodně	hodně	k6eAd1	hodně
vzdálil	vzdálit	k5eAaPmAgMnS	vzdálit
<g/>
,	,	kIx,	,
právě	právě	k9	právě
to	ten	k3xDgNnSc1	ten
ho	on	k3xPp3gMnSc4	on
nejvíce	nejvíce	k6eAd1	nejvíce
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
primátů	primát	k1gMnPc2	primát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chování	chování	k1gNnSc6	chování
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
primátů	primát	k1gMnPc2	primát
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
důležitá	důležitý	k2eAgFnSc1d1	důležitá
teritorialita	teritorialita	k1gFnSc1	teritorialita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ale	ale	k9	ale
málokdy	málokdy	k6eAd1	málokdy
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
fyzickým	fyzický	k2eAgInSc7d1	fyzický
zápasem	zápas	k1gInSc7	zápas
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
"	"	kIx"	"
<g/>
svého	své	k1gNnSc2	své
<g/>
"	"	kIx"	"
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
primáty	primát	k1gInPc4	primát
existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgInPc4d1	různý
způsoby	způsob	k1gInPc4	způsob
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
jak	jak	k6eAd1	jak
verbální	verbální	k2eAgFnSc1d1	verbální
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
neverbální	verbální	k2eNgFnSc1d1	neverbální
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
olfaktorická	olfaktorický	k2eAgFnSc1d1	olfaktorická
(	(	kIx(	(
<g/>
vůně	vůně	k1gFnSc1	vůně
<g/>
,	,	kIx,	,
pachy	pach	k1gInPc1	pach
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zraková	zrakový	k2eAgFnSc1d1	zraková
(	(	kIx(	(
<g/>
mimika	mimika	k1gFnSc1	mimika
<g/>
,	,	kIx,	,
postoje	postoj	k1gInPc1	postoj
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
taktilní	taktilní	k2eAgInPc4d1	taktilní
(	(	kIx(	(
<g/>
doteky	dotek	k1gInPc4	dotek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupujícím	postupující	k2eAgInSc7d1	postupující
stupněm	stupeň	k1gInSc7	stupeň
vývoje	vývoj	k1gInSc2	vývoj
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
rostoucí	rostoucí	k2eAgNnSc4d1	rostoucí
sociální	sociální	k2eAgNnSc4d1	sociální
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
u	u	k7c2	u
lidoopů	lidoop	k1gMnPc2	lidoop
<g/>
.	.	kIx.	.
</s>
<s>
Rozvinula	rozvinout	k5eAaPmAgNnP	rozvinout
se	se	k3xPyFc4	se
rituální	rituální	k2eAgNnPc1d1	rituální
jednání	jednání	k1gNnPc1	jednání
jako	jako	k8xC	jako
grooming	grooming	k1gInSc1	grooming
(	(	kIx(	(
<g/>
čištění	čištění	k1gNnSc2	čištění
<g/>
)	)	kIx)	)
a	a	k8xC	a
zdravení	zdravení	k1gNnSc1	zdravení
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
primátů	primát	k1gMnPc2	primát
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
infanticida	infanticida	k1gFnSc1	infanticida
(	(	kIx(	(
<g/>
zabíjení	zabíjení	k1gNnSc1	zabíjení
mláďat	mládě	k1gNnPc2	mládě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
vysvětlována	vysvětlovat	k5eAaImNgFnS	vysvětlovat
teorii	teorie	k1gFnSc3	teorie
o	o	k7c6	o
reprodukční	reprodukční	k2eAgFnSc6d1	reprodukční
strategií	strategie	k1gFnSc7	strategie
samců	samec	k1gMnPc2	samec
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
ale	ale	k8xC	ale
nelze	lze	k6eNd1	lze
přijímat	přijímat	k5eAaImF	přijímat
bez	bez	k7c2	bez
výhrad	výhrada	k1gFnPc2	výhrada
<g/>
.	.	kIx.	.
</s>
<s>
Primáti	primát	k1gMnPc1	primát
mohou	moct	k5eAaImIp3nP	moct
průběžne	průběžnout	k5eAaPmIp3nS	průběžnout
žít	žít	k5eAaImF	žít
solitérně	solitérně	k6eAd1	solitérně
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
dobu	doba	k1gFnSc4	doba
páření	páření	k1gNnSc2	páření
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
rozličných	rozličný	k2eAgFnPc6d1	rozličná
sociálních	sociální	k2eAgFnPc6d1	sociální
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
monogamních	monogamní	k2eAgFnPc2d1	monogamní
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgInSc1	jeden
samec	samec	k1gInSc1	samec
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
samice	samice	k1gFnSc1	samice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednosamcových	jednosamcův	k2eAgFnPc2d1	jednosamcův
(	(	kIx(	(
<g/>
jediný	jediný	k2eAgMnSc1d1	jediný
samec	samec	k1gMnSc1	samec
a	a	k8xC	a
více	hodně	k6eAd2	hodně
samic	samice	k1gFnPc2	samice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
harémových	harémový	k2eAgFnPc2d1	harémová
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
samců	samec	k1gMnPc2	samec
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgFnPc4	svůj
samice	samice	k1gFnPc4	samice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mnohosamco-samicových	mnohosamcoamicových	k2eAgFnSc1d1	mnohosamco-samicových
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
samců	samec	k1gInPc2	samec
a	a	k8xC	a
více	hodně	k6eAd2	hodně
samic	samice	k1gFnPc2	samice
<g/>
,	,	kIx,	,
o	o	k7c6	o
páření	páření	k1gNnSc6	páření
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
samice	samice	k1gFnSc1	samice
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jednosamicových	jednosamicových	k2eAgFnSc1d1	jednosamicových
(	(	kIx(	(
<g/>
jediná	jediný	k2eAgFnSc1d1	jediná
samice	samice	k1gFnSc1	samice
blokuje	blokovat	k5eAaImIp3nS	blokovat
ostatní	ostatní	k2eAgFnSc1d1	ostatní
samice	samice	k1gFnSc1	samice
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
znaků	znak	k1gInPc2	znak
sehrávající	sehrávající	k2eAgFnSc4d1	sehrávající
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
životě	život	k1gInSc6	život
primátů	primát	k1gMnPc2	primát
žijících	žijící	k2eAgMnPc2d1	žijící
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
je	být	k5eAaImIp3nS	být
matrilinearita	matrilinearita	k1gFnSc1	matrilinearita
–	–	k?	–
příbuzenské	příbuzenský	k2eAgInPc4d1	příbuzenský
vztahy	vztah	k1gInPc4	vztah
po	po	k7c6	po
mateřské	mateřský	k2eAgFnSc6d1	mateřská
linii	linie	k1gFnSc6	linie
<g/>
.	.	kIx.	.
</s>
<s>
Nejmladší	mladý	k2eAgFnPc1d3	nejmladší
dcery	dcera	k1gFnPc1	dcera
vysoce	vysoce	k6eAd1	vysoce
postavených	postavený	k2eAgFnPc2d1	postavená
samic	samice	k1gFnPc2	samice
ve	v	k7c6	v
společenské	společenský	k2eAgFnSc6d1	společenská
hierarchii	hierarchie	k1gFnSc6	hierarchie
skupiny	skupina	k1gFnSc2	skupina
získávají	získávat	k5eAaImIp3nP	získávat
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
v	v	k7c6	v
domácí	domácí	k2eAgFnSc6d1	domácí
skupině	skupina	k1gFnSc6	skupina
stejného	stejný	k2eAgNnSc2d1	stejné
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgNnSc4	jaký
měla	mít	k5eAaImAgFnS	mít
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
synové	syn	k1gMnPc1	syn
jen	jen	k9	jen
někdy	někdy	k6eAd1	někdy
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tito	tento	k3xDgMnPc1	tento
synové	syn	k1gMnPc1	syn
odejdou	odejít	k5eAaPmIp3nP	odejít
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
založí	založit	k5eAaPmIp3nP	založit
nové	nový	k2eAgFnPc4d1	nová
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
získávají	získávat	k5eAaImIp3nP	získávat
tam	tam	k6eAd1	tam
zpravidla	zpravidla	k6eAd1	zpravidla
vysoké	vysoký	k2eAgNnSc4d1	vysoké
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
to	ten	k3xDgNnSc1	ten
i	i	k9	i
pro	pro	k7c4	pro
adoptované	adoptovaný	k2eAgMnPc4d1	adoptovaný
potomky	potomek	k1gMnPc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
životě	život	k1gInSc6	život
primátů	primát	k1gInPc2	primát
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
důležité	důležitý	k2eAgFnPc1d1	důležitá
emigrace	emigrace	k1gFnPc1	emigrace
a	a	k8xC	a
imigrace	imigrace	k1gFnPc1	imigrace
jedinců	jedinec	k1gMnPc2	jedinec
mezi	mezi	k7c7	mezi
ustavenými	ustavený	k2eAgFnPc7d1	ustavená
skupinami	skupina	k1gFnPc7	skupina
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zabraňují	zabraňovat	k5eAaImIp3nP	zabraňovat
příbuzenskému	příbuzenský	k2eAgNnSc3d1	příbuzenské
páření	páření	k1gNnSc3	páření
a	a	k8xC	a
udržují	udržovat	k5eAaImIp3nP	udržovat
heterogenitu	heterogenita	k1gFnSc4	heterogenita
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
druhů	druh	k1gInPc2	druh
s	s	k7c7	s
početnějšími	početní	k2eAgFnPc7d2	početnější
skupinami	skupina	k1gFnPc7	skupina
odcházejí	odcházet	k5eAaImIp3nP	odcházet
především	především	k6eAd1	především
mladí	mladý	k2eAgMnPc1d1	mladý
pohlavně	pohlavně	k6eAd1	pohlavně
zralí	zralý	k2eAgMnPc1d1	zralý
samci	samec	k1gMnPc1	samec
do	do	k7c2	do
jiných	jiný	k2eAgNnPc2d1	jiné
seskupení	seskupení	k1gNnPc2	seskupení
nebo	nebo	k8xC	nebo
zakládají	zakládat	k5eAaImIp3nP	zakládat
nová	nový	k2eAgNnPc1d1	nové
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
druhů	druh	k1gInPc2	druh
žijících	žijící	k2eAgInPc2d1	žijící
v	v	k7c6	v
párech	pár	k1gInPc6	pár
odcházejí	odcházet	k5eAaImIp3nP	odcházet
pohlavně	pohlavně	k6eAd1	pohlavně
dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
i	i	k8xC	i
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Chování	chování	k1gNnSc1	chování
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
primátů	primát	k1gMnPc2	primát
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
neměnné	měnný	k2eNgNnSc1d1	neměnné
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
a	a	k8xC	a
prolínání	prolínání	k1gNnSc4	prolínání
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
způsobů	způsob	k1gInPc2	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
dostupnosti	dostupnost	k1gFnSc6	dostupnost
potravních	potravní	k2eAgInPc2d1	potravní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
charakteru	charakter	k1gInSc2	charakter
ekosystému	ekosystém	k1gInSc2	ekosystém
<g/>
,	,	kIx,	,
tlaku	tlak	k1gInSc2	tlak
predátorů	predátor	k1gMnPc2	predátor
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
faktorech	faktor	k1gInPc6	faktor
vnějšího	vnější	k2eAgNnSc2d1	vnější
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
ontogeneze	ontogeneze	k1gFnSc2	ontogeneze
<g/>
.	.	kIx.	.
</s>
<s>
Lokomoce	lokomoce	k1gFnSc1	lokomoce
je	být	k5eAaImIp3nS	být
forma	forma	k1gFnSc1	forma
aktivita	aktivita	k1gFnSc1	aktivita
sloužící	sloužící	k1gFnSc1	sloužící
k	k	k7c3	k
přemísťování	přemísťování	k1gNnSc3	přemísťování
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
složky	složka	k1gFnPc4	složka
která	který	k3yIgNnPc1	který
plynule	plynule	k6eAd1	plynule
přecházejí	přecházet	k5eAaImIp3nP	přecházet
jedna	jeden	k4xCgFnSc1	jeden
v	v	k7c4	v
druhou	druhý	k4xOgFnSc4	druhý
"	"	kIx"	"
<g/>
držení	držení	k1gNnSc4	držení
–	–	k?	–
pohyb	pohyb	k1gInSc1	pohyb
–	–	k?	–
držení	držení	k1gNnSc2	držení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
primátů	primát	k1gMnPc2	primát
rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
tyto	tento	k3xDgFnPc4	tento
možnosti	možnost	k1gFnPc4	možnost
lokomoce	lokomoce	k1gFnSc2	lokomoce
<g/>
:	:	kIx,	:
kvadrupedie	kvadrupedie	k1gFnSc2	kvadrupedie
(	(	kIx(	(
<g/>
stromovou	stromový	k2eAgFnSc4d1	stromová
nebo	nebo	k8xC	nebo
pozemní	pozemní	k2eAgFnSc4d1	pozemní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vertikální	vertikální	k2eAgNnSc4d1	vertikální
lpění	lpění	k1gNnSc4	lpění
a	a	k8xC	a
skákaní	skákaný	k2eAgMnPc1d1	skákaný
<g/>
,	,	kIx,	,
brachiace	brachiace	k1gFnPc4	brachiace
<g/>
,	,	kIx,	,
bipedie	bipedie	k1gFnPc4	bipedie
<g/>
,	,	kIx,	,
šplhání	šplhání	k1gNnSc4	šplhání
a	a	k8xC	a
skok	skok	k1gInSc4	skok
(	(	kIx(	(
<g/>
vodorovný	vodorovný	k2eAgInSc4d1	vodorovný
nebo	nebo	k8xC	nebo
svislý	svislý	k2eAgInSc4d1	svislý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žijící	žijící	k2eAgMnPc1d1	žijící
primáti	primát	k1gMnPc1	primát
–	–	k?	–
dříve	dříve	k6eAd2	dříve
nehetnatci	nehetnatec	k1gMnPc1	nehetnatec
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
klasifikováni	klasifikován	k2eAgMnPc1d1	klasifikován
kladisticky	kladisticky	k6eAd1	kladisticky
<g/>
,	,	kIx,	,
fylogeneticky	fylogeneticky	k6eAd1	fylogeneticky
<g/>
.	.	kIx.	.
</s>
<s>
Zařazování	zařazování	k1gNnSc1	zařazování
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
vyšších	vysoký	k2eAgInPc2d2	vyšší
taxonů	taxon	k1gInPc2	taxon
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
ne	ne	k9	ne
podle	podle	k7c2	podle
morfologické	morfologický	k2eAgNnSc1d1	morfologické
podobností	podobnost	k1gFnSc7	podobnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
principů	princip	k1gInPc2	princip
odrážejících	odrážející	k2eAgInPc2d1	odrážející
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
genetických	genetický	k2eAgInPc2d1	genetický
poznatků	poznatek	k1gInPc2	poznatek
<g/>
,	,	kIx,	,
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
příbuznost	příbuznost	k1gFnSc4	příbuznost
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
monofyleticky	monofyleticky	k6eAd1	monofyleticky
rozdělení	rozdělení	k1gNnSc4	rozdělení
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
základních	základní	k2eAgInPc2d1	základní
podřádů	podřád	k1gInPc2	podřád
<g/>
:	:	kIx,	:
poloopice	poloopice	k1gFnSc1	poloopice
(	(	kIx(	(
<g/>
Strepsirrhini	Strepsirrhin	k1gMnPc1	Strepsirrhin
<g/>
)	)	kIx)	)
–	–	k?	–
dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
Prosimii	Prosimie	k1gFnSc3	Prosimie
<g/>
,	,	kIx,	,
Prosimiae	Prosimiae	k1gFnSc4	Prosimiae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
podřádu	podřád	k1gInSc2	podřád
jsou	být	k5eAaImIp3nP	být
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
infrařády	infrařáda	k1gFnPc1	infrařáda
lemurů	lemur	k1gMnPc2	lemur
a	a	k8xC	a
ksukolů	ksukol	k1gInPc2	ksukol
žijící	žijící	k2eAgInSc1d1	žijící
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
a	a	k8xC	a
loriů	lori	k1gMnPc2	lori
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
Asií	Asie	k1gFnSc7	Asie
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgMnSc1d2	vyšší
primáti	primát	k1gMnPc1	primát
(	(	kIx(	(
<g/>
Haplorrhini	Haplorrhin	k1gMnPc1	Haplorrhin
<g/>
)	)	kIx)	)
–	–	k?	–
dříve	dříve	k6eAd2	dříve
opice	opice	k1gFnSc2	opice
(	(	kIx(	(
<g/>
Anthropoidea	Anthropoidea	k1gFnSc1	Anthropoidea
<g/>
,	,	kIx,	,
Simii	Simie	k1gFnSc4	Simie
<g/>
,	,	kIx,	,
Simiae	Simiae	k1gFnSc4	Simiae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
podřád	podřád	k1gInSc1	podřád
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
infrařádem	infrařád	k1gMnSc7	infrařád
nártounů	nártoun	k1gMnPc2	nártoun
a	a	k8xC	a
infrařádem	infrařád	k1gInSc7	infrařád
opic	opice	k1gFnPc2	opice
společným	společný	k2eAgInSc7d1	společný
pro	pro	k7c4	pro
ploskonosé	ploskonosý	k2eAgFnPc4d1	ploskonosý
z	z	k7c2	z
Nového	Nového	k2eAgInSc2d1	Nového
světa	svět	k1gInSc2	svět
a	a	k8xC	a
úzkonosé	úzkonosý	k2eAgFnPc1d1	úzkonosý
ze	z	k7c2	z
Starého	Starého	k2eAgInSc2d1	Starého
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Úplné	úplný	k2eAgNnSc1d1	úplné
členění	členění	k1gNnSc1	členění
řádu	řád	k1gInSc2	řád
primátů	primát	k1gMnPc2	primát
<g/>
:	:	kIx,	:
Primáti	primát	k1gMnPc1	primát
(	(	kIx(	(
<g/>
Primates	Primates	k1gMnSc1	Primates
<g/>
)	)	kIx)	)
│	│	k?	│
├	├	k?	├
<g/>
─	─	k?	─
podřád	podřád	k1gInSc1	podřád
poloopice	poloopice	k1gFnSc1	poloopice
(	(	kIx(	(
<g/>
Strepsirrhini	Strepsirrhin	k1gMnPc1	Strepsirrhin
<g/>
)	)	kIx)	)
│	│	k?	│
├	├	k?	├
<g/>
─	─	k?	─
infrařád	infrařáda	k1gFnPc2	infrařáda
lemuři	lemur	k1gMnPc1	lemur
(	(	kIx(	(
<g/>
Lemuriformes	Lemuriformes	k1gMnSc1	Lemuriformes
<g/>
)	)	kIx)	)
│	│	k?	│
│	│	k?	│
├	├	k?	├
<g/>
─	─	k?	─
nadčeleď	nadčeleď	k1gFnSc1	nadčeleď
lemurové	lemurové	k?	lemurové
(	(	kIx(	(
<g/>
Lemuroidea	Lemuroidea	k1gMnSc1	Lemuroidea
<g/>
)	)	kIx)	)
│	│	k?	│
│	│	k?	│
│	│	k?	│
├	├	k?	├
<g />
.	.	kIx.	.
</s>
<s>
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc1	čeleď
lemurovití	lemurovitý	k2eAgMnPc1d1	lemurovitý
denní	denní	k2eAgFnSc6d1	denní
(	(	kIx(	(
<g/>
Lemuridae	Lemuridae	k1gFnSc6	Lemuridae
<g/>
)	)	kIx)	)
│	│	k?	│
│	│	k?	│
│	│	k?	│
├	├	k?	├
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc4	čeleď
lemurovití	lemurovitý	k2eAgMnPc1d1	lemurovitý
noční	noční	k2eAgMnPc1d1	noční
(	(	kIx(	(
<g/>
Lepilemuridae	Lepilemuridae	k1gNnSc7	Lepilemuridae
<g/>
)	)	kIx)	)
│	│	k?	│
│	│	k?	│
│	│	k?	│
└	└	k?	└
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc1	čeleď
indriovití	indriovití	k1gMnPc5	indriovití
(	(	kIx(	(
<g/>
Indridae	Indridae	k1gFnSc1	Indridae
<g/>
)	)	kIx)	)
│	│	k?	│
│	│	k?	│
└	└	k?	└
<g/>
─	─	k?	─
nadčeleď	nadčeleď	k1gFnSc4	nadčeleď
makiové	makiový	k2eAgInPc1d1	makiový
(	(	kIx(	(
<g/>
Cheirogaleoidea	Cheirogaleoidea	k1gFnSc1	Cheirogaleoidea
<g/>
)	)	kIx)	)
│	│	k?	│
<g />
.	.	kIx.	.
</s>
<s>
│	│	k?	│
└	└	k?	└
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc1	čeleď
makiovití	makiovití	k1gMnPc5	makiovití
(	(	kIx(	(
<g/>
Cheirogaleidae	Cheirogaleidae	k1gFnSc1	Cheirogaleidae
<g/>
)	)	kIx)	)
│	│	k?	│
├	├	k?	├
<g/>
─	─	k?	─
infrařád	infrařáda	k1gFnPc2	infrařáda
Chiromyiformes	Chiromyiformes	k1gMnSc1	Chiromyiformes
│	│	k?	│
│	│	k?	│
└	└	k?	└
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc1	čeleď
ksukolovití	ksukolovitý	k2eAgMnPc1d1	ksukolovitý
(	(	kIx(	(
<g/>
Daubentoniidae	Daubentoniidae	k1gNnSc7	Daubentoniidae
<g/>
)	)	kIx)	)
│	│	k?	│
└	└	k?	└
<g/>
─	─	k?	─
infrařád	infrařáda	k1gFnPc2	infrařáda
Lorisiformes	Lorisiformes	k1gMnSc1	Lorisiformes
│	│	k?	│
├	├	k?	├
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc1	čeleď
outloňovití	outloňovití	k1gMnPc5	outloňovití
(	(	kIx(	(
<g/>
Lorisidae	Lorisidae	k1gFnSc1	Lorisidae
<g/>
)	)	kIx)	)
│	│	k?	│
└	└	k?	└
<g/>
─	─	k?	─
<g />
.	.	kIx.	.
</s>
<s>
čeleď	čeleď	k1gFnSc1	čeleď
kombovití	kombovitý	k2eAgMnPc1d1	kombovitý
(	(	kIx(	(
<g/>
Galagonidae	Galagonidae	k1gNnSc7	Galagonidae
<g/>
)	)	kIx)	)
│	│	k?	│
└	└	k?	└
<g/>
─	─	k?	─
podřád	podřád	k1gInSc4	podřád
vyšší	vysoký	k2eAgMnPc1d2	vyšší
primáti	primát	k1gMnPc1	primát
(	(	kIx(	(
<g/>
Haplorrhini	Haplorrhin	k1gMnPc1	Haplorrhin
<g/>
)	)	kIx)	)
│	│	k?	│
<g/>
─	─	k?	─
infrařád	infrařáda	k1gFnPc2	infrařáda
nártouni	nártoun	k1gMnPc1	nártoun
(	(	kIx(	(
<g/>
Tarsiiformes	Tarsiiformes	k1gMnSc1	Tarsiiformes
<g/>
)	)	kIx)	)
│	│	k?	│
└	└	k?	└
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc1	čeleď
archicebusovití	archicebusovitý	k2eAgMnPc1d1	archicebusovitý
(	(	kIx(	(
<g/>
†	†	k?	†
<g/>
Archicebus	Archicebus	k1gInSc1	Archicebus
<g/>
)	)	kIx)	)
│	│	k?	│
└	└	k?	└
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc1	čeleď
omomyidovití	omomyidovitý	k2eAgMnPc1d1	omomyidovitý
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
†	†	k?	†
<g/>
Omomyidae	Omomyidae	k1gFnSc1	Omomyidae
<g/>
)	)	kIx)	)
│	│	k?	│
└	└	k?	└
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc1	čeleď
nártounovití	nártounovitý	k2eAgMnPc1d1	nártounovitý
(	(	kIx(	(
<g/>
Tarsiidae	Tarsiidae	k1gNnSc7	Tarsiidae
<g/>
)	)	kIx)	)
└	└	k?	└
<g/>
─	─	k?	─
infrařád	infrařáda	k1gFnPc2	infrařáda
opice	opice	k1gFnSc1	opice
(	(	kIx(	(
<g/>
Simiiformes	Simiiformes	k1gInSc1	Simiiformes
<g/>
)	)	kIx)	)
├	├	k?	├
<g/>
─	─	k?	─
skupina	skupina	k1gFnSc1	skupina
ploskonosí	ploskonosý	k2eAgMnPc1d1	ploskonosý
(	(	kIx(	(
<g/>
Platyrrhini	Platyrrhin	k1gMnPc1	Platyrrhin
<g/>
)	)	kIx)	)
│	│	k?	│
├	├	k?	├
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc1	čeleď
malpovití	malpovití	k1gMnPc5	malpovití
(	(	kIx(	(
<g/>
Cebidae	Cebidae	k1gFnSc1	Cebidae
<g/>
)	)	kIx)	)
│	│	k?	│
├	├	k?	├
<g/>
<g />
.	.	kIx.	.
</s>
<s>
─	─	k?	─
čeleď	čeleď	k1gFnSc1	čeleď
chápanovití	chápanovitý	k2eAgMnPc1d1	chápanovitý
(	(	kIx(	(
<g/>
Atelidae	Atelidae	k1gNnSc7	Atelidae
<g/>
)	)	kIx)	)
│	│	k?	│
├	├	k?	├
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc1	čeleď
kosmanovití	kosmanovitý	k2eAgMnPc1d1	kosmanovitý
(	(	kIx(	(
<g/>
Callitrichidae	Callitrichidae	k1gNnSc7	Callitrichidae
<g/>
)	)	kIx)	)
│	│	k?	│
├	├	k?	├
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc1	čeleď
mirikinovití	mirikinovitý	k2eAgMnPc1d1	mirikinovitý
(	(	kIx(	(
<g/>
Aotidae	Aotidae	k1gNnSc7	Aotidae
<g/>
)	)	kIx)	)
│	│	k?	│
└	└	k?	└
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc1	čeleď
chvostanovití	chvostanovitý	k2eAgMnPc1d1	chvostanovitý
(	(	kIx(	(
<g/>
Pitheciidae	Pitheciidae	k1gNnSc7	Pitheciidae
<g/>
)	)	kIx)	)
└	└	k?	└
<g/>
─	─	k?	─
skupina	skupina	k1gFnSc1	skupina
úzkonosí	úzkonosý	k2eAgMnPc1d1	úzkonosý
(	(	kIx(	(
<g/>
Catarrhini	Catarrhin	k1gMnPc1	Catarrhin
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
├	├	k?	├
<g/>
─	─	k?	─
nadčeleď	nadčeleď	k1gFnSc1	nadčeleď
kočkodani	kočkodan	k1gMnPc5	kočkodan
(	(	kIx(	(
<g/>
Cercopithecoidea	Cercopithecoidea	k1gFnSc1	Cercopithecoidea
<g/>
)	)	kIx)	)
│	│	k?	│
└	└	k?	└
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc1	čeleď
kočkodanovití	kočkodanovitý	k2eAgMnPc1d1	kočkodanovitý
(	(	kIx(	(
<g/>
Cercopithecidae	Cercopithecidae	k1gNnSc7	Cercopithecidae
<g/>
)	)	kIx)	)
└	└	k?	└
<g/>
─	─	k?	─
nadčeleď	nadčeleď	k1gFnSc1	nadčeleď
hominoidi	hominoid	k1gMnPc5	hominoid
(	(	kIx(	(
<g/>
Hominoidea	Hominoidea	k1gFnSc1	Hominoidea
<g/>
)	)	kIx)	)
├	├	k?	├
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc1	čeleď
gibonovití	gibonovitý	k2eAgMnPc1d1	gibonovitý
(	(	kIx(	(
<g/>
Hylobatidae	Hylobatidae	k1gNnSc7	Hylobatidae
<g/>
)	)	kIx)	)
└	└	k?	└
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc1	čeleď
hominidi	hominid	k1gMnPc5	hominid
(	(	kIx(	(
<g/>
Hominidae	Hominidae	k1gFnSc1	Hominidae
<g/>
)	)	kIx)	)
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
zařazení	zařazení	k1gNnSc4	zařazení
ploskonosých	ploskonosý	k2eAgInPc2d1	ploskonosý
a	a	k8xC	a
úzkonosých	úzkonosý	k2eAgInPc2d1	úzkonosý
používán	používán	k2eAgInSc4d1	používán
taxon	taxon	k1gInSc4	taxon
parvorder	parvordra	k1gFnPc2	parvordra
nebo	nebo	k8xC	nebo
falanx	falanx	k1gFnSc4	falanx
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
použito	použít	k5eAaPmNgNnS	použít
české	český	k2eAgNnSc1d1	české
pojmenování	pojmenování	k1gNnSc1	pojmenování
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
fylogenetická	fylogenetický	k2eAgFnSc1d1	fylogenetická
studie	studie	k1gFnSc1	studie
z	z	k7c2	z
r.	r.	kA	r.
2011	[number]	k4	2011
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
shodu	shoda	k1gFnSc4	shoda
fylogenetických	fylogenetický	k2eAgInPc2d1	fylogenetický
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
čeleděmi	čeleď	k1gFnPc7	čeleď
recentních	recentní	k2eAgMnPc2d1	recentní
primátů	primát	k1gMnPc2	primát
s	s	k7c7	s
předchozím	předchozí	k2eAgNnSc7d1	předchozí
taxonomickým	taxonomický	k2eAgNnSc7d1	taxonomické
členěním	členění	k1gNnSc7	členění
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
makiovité	makiovitý	k2eAgInPc4d1	makiovitý
(	(	kIx(	(
<g/>
Cheirogaleidae	Cheirogaleida	k1gInPc4	Cheirogaleida
<g/>
)	)	kIx)	)
určila	určit	k5eAaPmAgFnS	určit
jako	jako	k8xS	jako
sesterskou	sesterský	k2eAgFnSc4d1	sesterská
skupinu	skupina	k1gFnSc4	skupina
nočních	noční	k2eAgFnPc2d1	noční
lemurovitých	lemurovitý	k2eAgFnPc2d1	lemurovitý
(	(	kIx(	(
<g/>
Lepilemuridae	Lepilemuridae	k1gFnPc2	Lepilemuridae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Upřesnila	upřesnit	k5eAaPmAgFnS	upřesnit
též	též	k9	též
vztahy	vztah	k1gInPc4	vztah
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
taxonomické	taxonomický	k2eAgNnSc1d1	taxonomické
členění	členění	k1gNnSc1	členění
není	být	k5eNaImIp3nS	být
dichotomické	dichotomický	k2eAgNnSc1d1	dichotomické
-	-	kIx~	-
v	v	k7c6	v
poloopicích	poloopice	k1gFnPc6	poloopice
byli	být	k5eAaImAgMnP	být
určeni	určen	k2eAgMnPc1d1	určen
ksukolovití	ksukolovitý	k2eAgMnPc1d1	ksukolovitý
jako	jako	k8xS	jako
sesterská	sesterský	k2eAgFnSc1d1	sesterská
skupina	skupina	k1gFnSc1	skupina
k	k	k7c3	k
lemurům	lemur	k1gMnPc3	lemur
<g/>
,	,	kIx,	,
v	v	k7c6	v
ploskonosých	ploskonosý	k2eAgFnPc6d1	ploskonosý
opicích	opice	k1gFnPc6	opice
chápanovití	chápanovitý	k2eAgMnPc1d1	chápanovitý
jako	jako	k8xC	jako
sesterská	sesterský	k2eAgFnSc1d1	sesterská
skupina	skupina	k1gFnSc1	skupina
malpovitých	malpovití	k1gMnPc2	malpovití
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
malpovití	malpovití	k1gMnPc1	malpovití
vč.	vč.	k?	vč.
kosmanovitých	kosmanovitý	k2eAgMnPc2d1	kosmanovitý
a	a	k8xC	a
mirikinovitých	mirikinovitý	k2eAgFnPc2d1	mirikinovitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řádu	řád	k1gInSc6	řád
primátů	primát	k1gMnPc2	primát
je	být	k5eAaImIp3nS	být
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
obrovské	obrovský	k2eAgNnSc1d1	obrovské
množství	množství	k1gNnSc1	množství
vzhledově	vzhledově	k6eAd1	vzhledově
různorodých	různorodý	k2eAgMnPc2d1	různorodý
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
velká	velký	k2eAgFnSc1d1	velká
skupina	skupina	k1gFnSc1	skupina
obývá	obývat	k5eAaImIp3nS	obývat
stromové	stromový	k2eAgInPc4d1	stromový
biotopy	biotop	k1gInPc4	biotop
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
již	již	k6eAd1	již
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
nová	nový	k2eAgFnSc1d1	nová
taxonomie	taxonomie	k1gFnSc1	taxonomie
prodělala	prodělat	k5eAaPmAgFnS	prodělat
také	také	k9	také
několik	několik	k4yIc4	několik
zásadních	zásadní	k2eAgFnPc2d1	zásadní
změn	změna	k1gFnPc2	změna
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
klasifikaci	klasifikace	k1gFnSc6	klasifikace
podřádů	podřád	k1gInPc2	podřád
a	a	k8xC	a
čeledí	čeleď	k1gFnPc2	čeleď
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Primáti	primát	k1gMnPc1	primát
mají	mít	k5eAaImIp3nP	mít
relativně	relativně	k6eAd1	relativně
velký	velký	k2eAgInSc4d1	velký
mozek	mozek	k1gInSc4	mozek
s	s	k7c7	s
redukovaným	redukovaný	k2eAgInSc7d1	redukovaný
čichovým	čichový	k2eAgInSc7d1	čichový
lalokem	lalok	k1gInSc7	lalok
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc7	jejich
hlavními	hlavní	k2eAgInPc7d1	hlavní
smyslovými	smyslový	k2eAgInPc7d1	smyslový
orgány	orgán	k1gInPc7	orgán
jsou	být	k5eAaImIp3nP	být
zrak	zrak	k1gInSc4	zrak
a	a	k8xC	a
sluch	sluch	k1gInSc4	sluch
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
poloopic	poloopice	k1gFnPc2	poloopice
a	a	k8xC	a
drápkatých	drápkatý	k2eAgFnPc2d1	drápkatá
opic	opice	k1gFnPc2	opice
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc4d1	důležitý
i	i	k9	i
čich	čich	k1gInSc4	čich
pro	pro	k7c4	pro
vymezování	vymezování	k1gNnSc4	vymezování
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
způsobu	způsob	k1gInSc3	způsob
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
během	během	k7c2	během
evoluce	evoluce	k1gFnSc2	evoluce
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
chápavé	chápavý	k2eAgFnPc1d1	chápavá
končetiny	končetina	k1gFnPc1	končetina
a	a	k8xC	a
protistojný	protistojný	k2eAgInSc1d1	protistojný
palec	palec	k1gInSc1	palec
(	(	kIx(	(
<g/>
existují	existovat	k5eAaImIp3nP	existovat
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
žijí	žít	k5eAaImIp3nP	žít
soliterně	soliterně	k6eAd1	soliterně
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
různé	různý	k2eAgFnPc4d1	různá
početné	početný	k2eAgFnPc4d1	početná
sociální	sociální	k2eAgFnPc4d1	sociální
struktury	struktura	k1gFnPc4	struktura
<g/>
,	,	kIx,	,
s	s	k7c7	s
vyvinutými	vyvinutý	k2eAgFnPc7d1	vyvinutá
komunikačními	komunikační	k2eAgFnPc7d1	komunikační
a	a	k8xC	a
manuálními	manuální	k2eAgFnPc7d1	manuální
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
jsou	být	k5eAaImIp3nP	být
mnohé	mnohý	k2eAgInPc1d1	mnohý
druhy	druh	k1gInPc1	druh
primátů	primát	k1gMnPc2	primát
chovány	chován	k2eAgInPc1d1	chován
snad	snad	k9	snad
již	již	k6eAd1	již
od	od	k7c2	od
prvopočátku	prvopočátek	k1gInSc2	prvopočátek
zakládání	zakládání	k1gNnSc2	zakládání
zoo	zoo	k1gFnSc2	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
činorodosti	činorodost	k1gFnSc3	činorodost
a	a	k8xC	a
denní	denní	k2eAgFnSc3d1	denní
aktivitě	aktivita	k1gFnSc3	aktivita
<g/>
,	,	kIx,	,
roztomilosti	roztomilost	k1gFnPc1	roztomilost
a	a	k8xC	a
jisté	jistý	k2eAgFnPc1d1	jistá
"	"	kIx"	"
<g/>
podobnosti	podobnost	k1gFnPc1	podobnost
<g/>
"	"	kIx"	"
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
vždy	vždy	k6eAd1	vždy
vyhledáváni	vyhledáván	k2eAgMnPc1d1	vyhledáván
pro	pro	k7c4	pro
chovy	chov	k1gInPc4	chov
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
tehdejší	tehdejší	k2eAgInPc4d1	tehdejší
trendy	trend	k1gInPc4	trend
v	v	k7c6	v
chovech	chov	k1gInPc6	chov
neumožňovaly	umožňovat	k5eNaImAgFnP	umožňovat
poskytnout	poskytnout	k5eAaPmF	poskytnout
zvířatům	zvíře	k1gNnPc3	zvíře
vhodné	vhodný	k2eAgFnPc4d1	vhodná
životní	životní	k2eAgFnSc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
opic	opice	k1gFnPc2	opice
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
až	až	k9	až
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dodávány	dodávat	k5eAaImNgInP	dodávat
z	z	k7c2	z
odchytů	odchyt	k1gInPc2	odchyt
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
jim	on	k3xPp3gInPc3	on
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
schopná	schopný	k2eAgFnSc1d1	schopná
zajistit	zajistit	k5eAaPmF	zajistit
důstojné	důstojný	k2eAgNnSc4d1	důstojné
žití	žití	k1gNnSc4	žití
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
je	být	k5eAaImIp3nS	být
vše	všechen	k3xTgNnSc1	všechen
již	již	k6eAd1	již
minulostí	minulost	k1gFnSc7	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
<g/>
.	.	kIx.	.
stále	stále	k6eAd1	stále
však	však	k9	však
ještě	ještě	k6eAd1	ještě
existují	existovat	k5eAaImIp3nP	existovat
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
zvířata	zvíře	k1gNnPc1	zvíře
chována	chován	k2eAgNnPc1d1	chováno
v	v	k7c6	v
zastaralých	zastaralý	k2eAgFnPc6d1	zastaralá
podmínkách	podmínka	k1gFnPc6	podmínka
(	(	kIx(	(
<g/>
rozvojové	rozvojový	k2eAgFnSc2d1	rozvojová
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
zoo	zoo	k1gFnPc1	zoo
dnes	dnes	k6eAd1	dnes
chovají	chovat	k5eAaImIp3nP	chovat
primáty	primát	k1gInPc1	primát
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dlouholetých	dlouholetý	k2eAgInPc2d1	dlouholetý
chovných	chovný	k2eAgInPc2d1	chovný
programů	program	k1gInPc2	program
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
pod	pod	k7c7	pod
přísnou	přísný	k2eAgFnSc7d1	přísná
kontrolou	kontrola	k1gFnSc7	kontrola
odborníků	odborník	k1gMnPc2	odborník
<g/>
.	.	kIx.	.
</s>
<s>
Pavilony	pavilon	k1gInPc1	pavilon
a	a	k8xC	a
expozice	expozice	k1gFnPc1	expozice
jsou	být	k5eAaImIp3nP	být
stavěny	stavit	k5eAaImNgFnP	stavit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
podobaly	podobat	k5eAaImAgInP	podobat
jejich	jejich	k3xOp3gInSc3	jejich
přirozenému	přirozený	k2eAgInSc3d1	přirozený
biotopu	biotop	k1gInSc3	biotop
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
co	co	k3yRnSc4	co
největší	veliký	k2eAgNnSc4d3	veliký
prosvětlení	prosvětlení	k1gNnSc4	prosvětlení
<g/>
,	,	kIx,	,
dostatek	dostatek	k1gInSc4	dostatek
oddělovacích	oddělovací	k2eAgFnPc2d1	oddělovací
místností	místnost	k1gFnPc2	místnost
a	a	k8xC	a
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
zázemí	zázemí	k1gNnSc4	zázemí
<g/>
,	,	kIx,	,
vybavené	vybavený	k2eAgInPc1d1	vybavený
nejmodernější	moderní	k2eAgFnSc7d3	nejmodernější
technikou	technika	k1gFnSc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
chovány	chovat	k5eAaImNgInP	chovat
v	v	k7c6	v
kontaktních	kontaktní	k2eAgInPc6d1	kontaktní
chovech	chov	k1gInPc6	chov
-	-	kIx~	-
většinou	většinou	k6eAd1	většinou
malé	malý	k2eAgInPc1d1	malý
druhy	druh	k1gInPc1	druh
např.	např.	kA	např.
lemuři	lemur	k1gMnPc1	lemur
-	-	kIx~	-
nebo	nebo	k8xC	nebo
v	v	k7c6	v
polokontaktních	polokontaktní	k2eAgInPc6d1	polokontaktní
či	či	k8xC	či
nekontaktních	kontaktní	k2eNgInPc6d1	nekontaktní
chovech	chov	k1gInPc6	chov
-	-	kIx~	-
makakové	makakové	k?	makakové
<g/>
,	,	kIx,	,
malpy	malpa	k1gFnPc1	malpa
<g/>
,	,	kIx,	,
kočkodani	kočkodan	k1gMnPc1	kočkodan
<g/>
...	...	k?	...
</s>
