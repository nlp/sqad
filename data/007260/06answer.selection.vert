<s>
Různorodost	různorodost	k1gFnSc1	různorodost
primátů	primát	k1gMnPc2	primát
je	být	k5eAaImIp3nS	být
veliká	veliký	k2eAgFnSc1d1	veliká
<g/>
,	,	kIx,	,
nejmenší	malý	k2eAgFnSc1d3	nejmenší
váží	vážit	k5eAaImIp3nS	vážit
70	[number]	k4	70
gramů	gram	k1gInPc2	gram
a	a	k8xC	a
největší	veliký	k2eAgInSc1d3	veliký
200	[number]	k4	200
kg	kg	kA	kg
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
jak	jak	k8xS	jak
noční	noční	k2eAgMnSc1d1	noční
lemur	lemur	k1gMnSc1	lemur
<g/>
,	,	kIx,	,
tak	tak	k9	tak
člověk	člověk	k1gMnSc1	člověk
přistanuvší	přistanuvší	k2eAgMnSc1d1	přistanuvší
na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
