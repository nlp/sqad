<s>
Železo	železo	k1gNnSc1	železo
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Fe	Fe	k1gFnSc2	Fe
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Ferrum	Ferrum	k1gInSc1	Ferrum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejrozšířenější	rozšířený	k2eAgInSc4d3	nejrozšířenější
přechodný	přechodný	k2eAgInSc4d1	přechodný
kovový	kovový	k2eAgInSc4d1	kovový
prvek	prvek	k1gInSc4	prvek
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
nejrozšířenější	rozšířený	k2eAgInSc4d3	nejrozšířenější
kov	kov	k1gInSc4	kov
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
hojně	hojně	k6eAd1	hojně
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
i	i	k9	i
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
