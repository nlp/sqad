<s>
Železo	železo	k1gNnSc1	železo
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Fe	Fe	k1gFnSc2	Fe
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Ferrum	Ferrum	k1gInSc1	Ferrum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejrozšířenější	rozšířený	k2eAgInSc4d3	nejrozšířenější
přechodný	přechodný	k2eAgInSc4d1	přechodný
kovový	kovový	k2eAgInSc4d1	kovový
prvek	prvek	k1gInSc4	prvek
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
nejrozšířenější	rozšířený	k2eAgInSc4d3	nejrozšířenější
kov	kov	k1gInSc4	kov
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
hojně	hojně	k6eAd1	hojně
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
i	i	k9	i
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Lidstvu	lidstvo	k1gNnSc3	lidstvo
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
již	již	k6eAd1	již
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
minerály	minerál	k1gInPc1	minerál
železa	železo	k1gNnSc2	železo
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
velmi	velmi	k6eAd1	velmi
hojně	hojně	k6eAd1	hojně
a	a	k8xC	a
železo	železo	k1gNnSc1	železo
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
(	(	kIx(	(
<g/>
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
slitiny	slitina	k1gFnSc2	slitina
s	s	k7c7	s
uhlíkem	uhlík	k1gInSc7	uhlík
<g/>
)	)	kIx)	)
získává	získávat	k5eAaImIp3nS	získávat
redukcí	redukce	k1gFnPc2	redukce
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
peci	pec	k1gFnSc6	pec
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
využití	využití	k1gNnSc2	využití
železa	železo	k1gNnSc2	železo
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
momentů	moment	k1gInPc2	moment
vzniku	vznik	k1gInSc2	vznik
současné	současný	k2eAgFnSc2d1	současná
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Železo	železo	k1gNnSc1	železo
má	mít	k5eAaImIp3nS	mít
mnohostranné	mnohostranný	k2eAgNnSc1d1	mnohostranné
využití	využití	k1gNnSc1	využití
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
většiny	většina	k1gFnSc2	většina
základních	základní	k2eAgInPc2d1	základní
technických	technický	k2eAgInPc2d1	technický
prostředků	prostředek	k1gInPc2	prostředek
používaných	používaný	k2eAgInPc2d1	používaný
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
zhotovovaných	zhotovovaný	k2eAgInPc2d1	zhotovovaný
ze	z	k7c2	z
slitin	slitina	k1gFnPc2	slitina
železa	železo	k1gNnSc2	železo
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
litiny	litina	k1gFnSc2	litina
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významné	významný	k2eAgFnPc1d1	významná
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
sloučeniny	sloučenina	k1gFnPc1	sloučenina
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
anorganické	anorganický	k2eAgFnPc4d1	anorganická
<g/>
,	,	kIx,	,
organické	organický	k2eAgFnPc4d1	organická
nebo	nebo	k8xC	nebo
komplexní	komplexní	k2eAgFnPc4d1	komplexní
<g/>
.	.	kIx.	.
</s>
<s>
Železo	železo	k1gNnSc1	železo
je	být	k5eAaImIp3nS	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
významným	významný	k2eAgInSc7d1	významný
biogenním	biogenní	k2eAgInSc7d1	biogenní
prvkem	prvek	k1gInSc7	prvek
<g/>
,	,	kIx,	,
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c4	na
přenášení	přenášení	k1gNnSc4	přenášení
kyslíku	kyslík	k1gInSc2	kyslík
k	k	k7c3	k
buňkám	buňka	k1gFnPc3	buňka
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
život	život	k1gInSc4	život
mnoha	mnoho	k4c2	mnoho
organismů	organismus	k1gInPc2	organismus
na	na	k7c6	na
naší	náš	k3xOp1gFnSc6	náš
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
železa	železo	k1gNnSc2	železo
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
chudokrevnosti	chudokrevnost	k1gFnSc3	chudokrevnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
sníženou	snížený	k2eAgFnSc7d1	snížená
kapacitou	kapacita	k1gFnSc7	kapacita
krve	krev	k1gFnSc2	krev
pro	pro	k7c4	pro
dýchací	dýchací	k2eAgInPc4d1	dýchací
plyny	plyn	k1gInPc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Železo	železo	k1gNnSc1	železo
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
měkké	měkký	k2eAgNnSc1d1	měkké
<g/>
,	,	kIx,	,
světle	světle	k6eAd1	světle
šedé	šedý	k2eAgFnPc1d1	šedá
až	až	k8xS	až
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ferromagnetický	ferromagnetický	k2eAgInSc1d1	ferromagnetický
kov	kov	k1gInSc1	kov
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
odolností	odolnost	k1gFnSc7	odolnost
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Železo	železo	k1gNnSc1	železo
je	být	k5eAaImIp3nS	být
ferromagnetické	ferromagnetický	k2eAgNnSc1d1	ferromagnetické
do	do	k7c2	do
teploty	teplota	k1gFnSc2	teplota
768	[number]	k4	768
°	°	k?	°
<g/>
C	C	kA	C
-	-	kIx~	-
Curieův	Curieův	k2eAgInSc4d1	Curieův
bod	bod	k1gInSc4	bod
-	-	kIx~	-
kdy	kdy	k6eAd1	kdy
tuto	tento	k3xDgFnSc4	tento
vlastnost	vlastnost	k1gFnSc4	vlastnost
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
<g/>
.	.	kIx.	.
</s>
<s>
Lidstvu	lidstvo	k1gNnSc3	lidstvo
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
již	již	k6eAd1	již
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
(	(	kIx(	(
<g/>
objeveno	objevit	k5eAaPmNgNnS	objevit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
pravěku	pravěk	k1gInSc2	pravěk
-	-	kIx~	-
období	období	k1gNnSc3	období
halštatské	halštatský	k2eAgFnSc2d1	halštatská
a	a	k8xC	a
laténské	laténský	k2eAgFnSc2d1	laténská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
jeho	jeho	k3xOp3gFnSc1	jeho
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
v	v	k7c6	v
masovém	masový	k2eAgNnSc6d1	masové
měřítku	měřítko	k1gNnSc6	měřítko
začíná	začínat	k5eAaImIp3nS	začínat
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Železo	železo	k1gNnSc1	železo
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
přechodné	přechodný	k2eAgInPc4d1	přechodný
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
valenční	valenční	k2eAgInPc1d1	valenční
elektrony	elektron	k1gInPc1	elektron
v	v	k7c6	v
d-orbitalu	drbital	k1gMnSc6	d-orbital
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
především	především	k9	především
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Fe	Fe	k1gFnSc2	Fe
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
a	a	k8xC	a
Fe	Fe	k1gFnSc1	Fe
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
sloučeniny	sloučenina	k1gFnPc4	sloučenina
Fe	Fe	k1gFnSc2	Fe
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
nestálé	stálý	k2eNgFnPc1d1	nestálá
a	a	k8xC	a
nemají	mít	k5eNaImIp3nP	mít
praktický	praktický	k2eAgInSc4d1	praktický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
Fe	Fe	k1gFnSc1	Fe
<g/>
6	[number]	k4	6
<g/>
+	+	kIx~	+
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgNnPc1d1	silné
oxidační	oxidační	k2eAgNnPc1d1	oxidační
činidla	činidlo	k1gNnPc1	činidlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
stabilní	stabilní	k2eAgFnPc1d1	stabilní
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nevyužívají	využívat	k5eNaImIp3nP	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
je	být	k5eAaImIp3nS	být
elementární	elementární	k2eAgNnSc4d1	elementární
železo	železo	k1gNnSc4	železo
značně	značně	k6eAd1	značně
nestálé	stálý	k2eNgFnPc1d1	nestálá
a	a	k8xC	a
reaktivní	reaktivní	k2eAgFnPc1d1	reaktivní
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
působením	působení	k1gNnSc7	působení
minerálních	minerální	k2eAgFnPc2d1	minerální
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnPc1d1	chlorovodíková
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nP	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
chloridu	chlorid	k1gInSc2	chlorid
železnatého	železnatý	k2eAgInSc2d1	železnatý
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
rozpuštěným	rozpuštěný	k2eAgInSc7d1	rozpuštěný
kyslíkem	kyslík	k1gInSc7	kyslík
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
na	na	k7c4	na
chlorid	chlorid	k1gInSc4	chlorid
železitý	železitý	k2eAgInSc4d1	železitý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zředěné	zředěný	k2eAgFnSc6d1	zředěná
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc6d1	sírová
se	se	k3xPyFc4	se
železo	železo	k1gNnSc1	železo
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
síranu	síran	k1gInSc2	síran
železnatého	železnatý	k2eAgInSc2d1	železnatý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
delším	dlouhý	k2eAgNnSc7d2	delší
stáním	stání	k1gNnSc7	stání
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
na	na	k7c4	na
síran	síran	k1gInSc4	síran
železitý	železitý	k2eAgInSc4d1	železitý
<g/>
,	,	kIx,	,
v	v	k7c6	v
koncentrované	koncentrovaný	k2eAgFnSc6d1	koncentrovaná
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc6d1	sírová
se	se	k3xPyFc4	se
železo	železo	k1gNnSc1	železo
nerozpouští	rozpouštět	k5eNaImIp3nS	rozpouštět
vlivem	vliv	k1gInSc7	vliv
pasivace	pasivace	k1gFnSc2	pasivace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zředěné	zředěný	k2eAgFnSc6d1	zředěná
kyselině	kyselina	k1gFnSc6	kyselina
dusičné	dusičný	k2eAgFnSc6d1	dusičná
se	se	k3xPyFc4	se
železo	železo	k1gNnSc1	železo
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
dusičnanu	dusičnan	k1gInSc2	dusičnan
železitého	železitý	k2eAgNnSc2d1	železité
a	a	k8xC	a
v	v	k7c4	v
koncentrované	koncentrovaný	k2eAgNnSc4d1	koncentrované
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
pasivuje	pasivovat	k5eAaBmIp3nS	pasivovat
a	a	k8xC	a
nerozpouští	rozpouštět	k5eNaImIp3nS	rozpouštět
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxidy	hydroxid	k1gInPc1	hydroxid
na	na	k7c4	na
železo	železo	k1gNnSc4	železo
za	za	k7c4	za
normální	normální	k2eAgFnPc4d1	normální
teploty	teplota	k1gFnPc4	teplota
nepůsobí	působit	k5eNaImIp3nP	působit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zahříváním	zahřívání	k1gNnSc7	zahřívání
železných	železný	k2eAgFnPc2d1	železná
pilin	pilina	k1gFnPc2	pilina
s	s	k7c7	s
hydroxidem	hydroxid	k1gInSc7	hydroxid
alkalického	alkalický	k2eAgInSc2d1	alkalický
kovu	kov	k1gInSc2	kov
vznikají	vznikat	k5eAaImIp3nP	vznikat
železany	železan	k1gInPc1	železan
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
vlhkosti	vlhkost	k1gFnSc2	vlhkost
se	se	k3xPyFc4	se
železo	železo	k1gNnSc1	železo
snadno	snadno	k6eAd1	snadno
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
za	za	k7c2	za
tvorby	tvorba	k1gFnSc2	tvorba
hydratovaných	hydratovaný	k2eAgInPc2d1	hydratovaný
oxidů	oxid	k1gInPc2	oxid
(	(	kIx(	(
<g/>
rez	rez	k1gInSc1	rez
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
přitom	přitom	k6eAd1	přitom
nevede	vést	k5eNaImIp3nS	vést
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
materiálu	materiál	k1gInSc2	materiál
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
pasivací	pasivace	k1gFnSc7	pasivace
jako	jako	k8xC	jako
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
kovových	kovový	k2eAgInPc2d1	kovový
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vrstva	vrstva	k1gFnSc1	vrstva
rzi	rez	k1gFnSc2	rez
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
odlupuje	odlupovat	k5eAaImIp3nS	odlupovat
a	a	k8xC	a
koroze	koroze	k1gFnSc1	koroze
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
suchém	suchý	k2eAgInSc6d1	suchý
vzduchu	vzduch	k1gInSc6	vzduch
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
)	)	kIx)	)
reaguje	reagovat	k5eAaBmIp3nS	reagovat
železo	železo	k1gNnSc1	železo
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
až	až	k9	až
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
150	[number]	k4	150
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
se	s	k7c7	s
samotnou	samotný	k2eAgFnSc7d1	samotná
vodou	voda	k1gFnSc7	voda
železo	železo	k1gNnSc4	železo
bez	bez	k7c2	bez
přítomnosti	přítomnost	k1gFnSc2	přítomnost
kyslíku	kyslík	k1gInSc2	kyslík
vůbec	vůbec	k9	vůbec
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
žíhání	žíhání	k1gNnSc6	žíhání
železa	železo	k1gNnSc2	železo
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnPc2	jeho
reakcí	reakce	k1gFnPc2	reakce
s	s	k7c7	s
vodní	vodní	k2eAgFnSc7d1	vodní
párou	pára	k1gFnSc7	pára
vzniká	vznikat	k5eAaImIp3nS	vznikat
oxid	oxid	k1gInSc4	oxid
železnato-železitý	železnato-železitý	k2eAgInSc4d1	železnato-železitý
Fe	Fe	k1gFnSc7	Fe
<g/>
3	[number]	k4	3
<g/>
O	O	kA	O
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Železo	železo	k1gNnSc1	železo
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
za	za	k7c4	za
horka	horko	k1gNnPc4	horko
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
chlórem	chlór	k1gInSc7	chlór
<g/>
,	,	kIx,	,
sírou	síra	k1gFnSc7	síra
a	a	k8xC	a
fosforem	fosfor	k1gInSc7	fosfor
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
schopnost	schopnost	k1gFnSc4	schopnost
se	se	k3xPyFc4	se
slučovat	slučovat	k5eAaImF	slučovat
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
slévat	slévat	k5eAaImF	slévat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
s	s	k7c7	s
uhlíkem	uhlík	k1gInSc7	uhlík
a	a	k8xC	a
křemíkem	křemík	k1gInSc7	křemík
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
železo	železo	k1gNnSc1	železo
vůbec	vůbec	k9	vůbec
neslučuje	slučovat	k5eNaImIp3nS	slučovat
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
vlastností	vlastnost	k1gFnSc7	vlastnost
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
železa	železo	k1gNnSc2	železo
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
vazebnou	vazebný	k2eAgFnSc7d1	vazebná
energií	energie	k1gFnSc7	energie
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
známých	známý	k2eAgInPc2d1	známý
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
termojaderné	termojaderný	k2eAgFnSc6d1	termojaderná
fúzi	fúze	k1gFnSc6	fúze
<g/>
,	,	kIx,	,
probíhající	probíhající	k2eAgFnPc1d1	probíhající
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
hvězd	hvězda	k1gFnPc2	hvězda
je	být	k5eAaImIp3nS	být
železo	železo	k1gNnSc1	železo
posledním	poslední	k2eAgInSc7d1	poslední
prvkem	prvek	k1gInSc7	prvek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
lze	lze	k6eAd1	lze
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
připravit	připravit	k5eAaPmF	připravit
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
energetického	energetický	k2eAgInSc2d1	energetický
zisku	zisk	k1gInSc2	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
při	při	k7c6	při
štěpení	štěpení	k1gNnSc6	štěpení
jader	jádro	k1gNnPc2	jádro
těžkých	těžký	k2eAgInPc2d1	těžký
prvků	prvek	k1gInPc2	prvek
je	být	k5eAaImIp3nS	být
jádro	jádro	k1gNnSc1	jádro
atomu	atom	k1gInSc2	atom
železa	železo	k1gNnSc2	železo
prvním	první	k4xOgMnSc6	první
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
nelze	lze	k6eNd1	lze
štěpením	štěpení	k1gNnSc7	štěpení
získat	získat	k5eAaPmF	získat
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Železo	železo	k1gNnSc1	železo
je	být	k5eAaImIp3nS	být
lidstvu	lidstvo	k1gNnSc3	lidstvo
známo	znám	k2eAgNnSc1d1	známo
již	již	k9	již
od	od	k7c2	od
prehistorické	prehistorický	k2eAgFnSc2d1	prehistorická
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ne	ne	k9	ne
všechny	všechen	k3xTgInPc1	všechen
nálezy	nález	k1gInPc1	nález
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
lze	lze	k6eAd1	lze
pokládat	pokládat	k5eAaImF	pokládat
za	za	k7c4	za
lidské	lidský	k2eAgInPc4d1	lidský
výtvory	výtvor	k1gInPc4	výtvor
<g/>
.	.	kIx.	.
</s>
<s>
Železné	železný	k2eAgFnPc1d1	železná
kuličky	kulička	k1gFnPc1	kulička
staré	starý	k2eAgFnPc1d1	stará
6000	[number]	k4	6000
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
meteoritického	meteoritický	k2eAgMnSc4d1	meteoritický
původu	původa	k1gMnSc4	původa
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
později	pozdě	k6eAd2	pozdě
nalezené	nalezený	k2eAgInPc4d1	nalezený
vzorky	vzorek	k1gInPc4	vzorek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
redukcí	redukce	k1gFnSc7	redukce
rud	ruda	k1gFnPc2	ruda
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
železo	železo	k1gNnSc4	železo
dřevěným	dřevěný	k2eAgNnSc7d1	dřevěné
uhlím	uhlí	k1gNnSc7	uhlí
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
odlité	odlitý	k2eAgNnSc1d1	odlité
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
měchů	měch	k1gInPc2	měch
nelze	lze	k6eNd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
patřičné	patřičný	k2eAgFnPc4d1	patřičná
teploty	teplota	k1gFnPc4	teplota
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
redukci	redukce	k1gFnSc4	redukce
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
železo	železo	k1gNnSc1	železo
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
činností	činnost	k1gFnSc7	činnost
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
opracovávat	opracovávat	k5eAaImF	opracovávat
kovářským	kovářský	k2eAgInSc7d1	kovářský
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
houbovité	houbovitý	k2eAgNnSc1d1	houbovité
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
nízkoteplotní	nízkoteplotní	k2eAgFnSc7d1	nízkoteplotní
redukcí	redukce	k1gFnSc7	redukce
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
v	v	k7c6	v
zkujňovacím	zkujňovací	k2eAgNnSc6d1	zkujňovací
ohništi	ohniště	k1gNnSc6	ohniště
<g/>
.	.	kIx.	.
</s>
<s>
Železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
se	se	k3xPyFc4	se
zahřívala	zahřívat	k5eAaImAgFnS	zahřívat
v	v	k7c6	v
mělkých	mělký	k2eAgInPc6d1	mělký
jamách	jamách	k?	jamách
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
přebytkem	přebytek	k1gInSc7	přebytek
dřevěného	dřevěný	k2eAgNnSc2d1	dřevěné
uhlí	uhlí	k1gNnSc2	uhlí
rozdmýchávaného	rozdmýchávaný	k2eAgInSc2d1	rozdmýchávaný
měchem	měch	k1gInSc7	měch
<g/>
.	.	kIx.	.
</s>
<s>
Získaly	získat	k5eAaPmAgInP	získat
se	se	k3xPyFc4	se
tak	tak	k9	tak
slinuté	slinutý	k2eAgInPc1d1	slinutý
kusy	kus	k1gInPc1	kus
kujného	kujný	k2eAgNnSc2d1	kujné
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
stavovaly	stavovat	k5eAaImAgFnP	stavovat
mocným	mocný	k2eAgNnSc7d1	mocné
kováním	kování	k1gNnSc7	kování
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
výroby	výroba	k1gFnSc2	výroba
železa	železo	k1gNnSc2	železo
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
prvně	prvně	k?	prvně
u	u	k7c2	u
Chetitů	Chetit	k1gMnPc2	Chetit
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Chetité	Chetita	k1gMnPc1	Chetita
výrobu	výrob	k1gInSc2	výrob
železa	železo	k1gNnSc2	železo
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
střežili	střežit	k5eAaImAgMnP	střežit
a	a	k8xC	a
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
rozšíření	rozšíření	k1gNnSc3	rozšíření
tak	tak	k6eAd1	tak
došlo	dojít	k5eAaPmAgNnS	dojít
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Chetitské	chetitský	k2eAgFnSc2d1	Chetitská
říše	říš	k1gFnSc2	říš
někdy	někdy	k6eAd1	někdy
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1200	[number]	k4	1200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začíná	začínat	k5eAaImIp3nS	začínat
doba	doba	k1gFnSc1	doba
železná	železný	k2eAgFnSc1d1	železná
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
při	při	k7c6	při
zpracovávání	zpracovávání	k1gNnSc6	zpracovávání
přešlo	přejít	k5eAaPmAgNnS	přejít
od	od	k7c2	od
jam	jáma	k1gFnPc2	jáma
či	či	k8xC	či
plochých	plochý	k2eAgInPc2d1	plochý
krbů	krb	k1gInPc2	krb
k	k	k7c3	k
malým	malý	k2eAgFnPc3d1	malá
šachtovým	šachtový	k2eAgFnPc3d1	šachtová
pecím	pec	k1gFnPc3	pec
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
dnešní	dnešní	k2eAgFnPc1d1	dnešní
vysoké	vysoký	k2eAgFnPc1d1	vysoká
pece	pec	k1gFnPc1	pec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
zaveden	zaveden	k2eAgInSc1d1	zaveden
pohon	pohon	k1gInSc1	pohon
dmýchadel	dmýchadlo	k1gNnPc2	dmýchadlo
vodní	vodní	k2eAgNnSc1d1	vodní
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
peci	pec	k1gFnSc6	pec
a	a	k8xC	a
získalo	získat	k5eAaPmAgNnS	získat
se	se	k3xPyFc4	se
tak	tak	k9	tak
železo	železo	k1gNnSc1	železo
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
obsahem	obsah	k1gInSc7	obsah
uhlíku	uhlík	k1gInSc2	uhlík
-	-	kIx~	-
litina	litina	k1gFnSc1	litina
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
sice	sice	k8xC	sice
není	být	k5eNaImIp3nS	být
kujná	kujný	k2eAgFnSc1d1	kujná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
naučili	naučit	k5eAaPmAgMnP	naučit
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
na	na	k7c4	na
kujné	kujný	k2eAgNnSc4d1	kujné
železo	železo	k1gNnSc4	železo
novým	nový	k2eAgNnSc7d1	nové
zahříváním	zahřívání	k1gNnSc7	zahřívání
za	za	k7c2	za
vydatného	vydatný	k2eAgInSc2d1	vydatný
přívodu	přívod	k1gInSc2	přívod
vzduchu	vzduch	k1gInSc2	vzduch
-	-	kIx~	-
zkujňování	zkujňování	k1gNnSc1	zkujňování
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
krok	krok	k1gInSc1	krok
ve	v	k7c6	v
zpracování	zpracování	k1gNnSc6	zpracování
železných	železný	k2eAgFnPc2d1	železná
rud	ruda	k1gFnPc2	ruda
bylo	být	k5eAaImAgNnS	být
zavedení	zavedení	k1gNnSc1	zavedení
koksu	koks	k1gInSc2	koks
jako	jako	k8xS	jako
redukčního	redukční	k2eAgNnSc2d1	redukční
činidla	činidlo	k1gNnSc2	činidlo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
použití	použití	k1gNnSc1	použití
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
faktorů	faktor	k1gInPc2	faktor
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Zkujňování	zkujňování	k1gNnSc1	zkujňování
železa	železo	k1gNnSc2	železo
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
výrazně	výrazně	k6eAd1	výrazně
zlepšeno	zlepšit	k5eAaPmNgNnS	zlepšit
pomocí	pomocí	k7c2	pomocí
zkujňování	zkujňování	k1gNnSc2	zkujňování
větrem	vítr	k1gInSc7	vítr
(	(	kIx(	(
<g/>
pochod	pochod	k1gInSc1	pochod
Bessemerův	Bessemerův	k2eAgInSc1d1	Bessemerův
-	-	kIx~	-
Bessemerace	Bessemerace	k1gFnSc1	Bessemerace
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
a	a	k8xC	a
topení	topení	k1gNnSc1	topení
s	s	k7c7	s
regenerací	regenerace	k1gFnSc7	regenerace
tepla	teplo	k1gNnSc2	teplo
(	(	kIx(	(
<g/>
proces	proces	k1gInSc1	proces
Siemensův-Martinův	Siemensův-Martinův	k2eAgInSc1d1	Siemensův-Martinův
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
vysoce	vysoce	k6eAd1	vysoce
kvalitních	kvalitní	k2eAgFnPc2d1	kvalitní
ocelí	ocel	k1gFnPc2	ocel
zavedeno	zaveden	k2eAgNnSc1d1	zavedeno
tavení	tavení	k1gNnSc1	tavení
v	v	k7c6	v
elektrické	elektrický	k2eAgFnSc6d1	elektrická
peci	pec	k1gFnSc6	pec
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
minerály	minerál	k1gInPc1	minerál
železa	železo	k1gNnSc2	železo
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c4	v
kategorii	kategorie	k1gFnSc4	kategorie
Minerály	minerál	k1gInPc4	minerál
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Železo	železo	k1gNnSc1	železo
patří	patřit	k5eAaImIp3nS	patřit
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
stálosti	stálost	k1gFnSc3	stálost
svého	svůj	k3xOyFgNnSc2	svůj
jádra	jádro	k1gNnSc2	jádro
mezi	mezi	k7c4	mezi
prvky	prvek	k1gInPc4	prvek
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
významným	významný	k2eAgNnSc7d1	významné
zastoupením	zastoupení	k1gNnSc7	zastoupení
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
i	i	k8xC	i
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Železo	železo	k1gNnSc1	železo
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
také	také	k9	také
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gNnSc4	jeho
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
kůře	kůra	k1gFnSc6	kůra
činí	činit	k5eAaImIp3nS	činit
9	[number]	k4	9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
měsíčním	měsíční	k2eAgInSc6d1	měsíční
prachu	prach	k1gInSc6	prach
je	být	k5eAaImIp3nS	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
kovové	kovový	k2eAgFnSc6d1	kovová
formě	forma	k1gFnSc6	forma
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
okolo	okolo	k7c2	okolo
0,5	[number]	k4	0,5
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
průměrně	průměrně	k6eAd1	průměrně
dvanáctimetrové	dvanáctimetrový	k2eAgFnSc6d1	dvanáctimetrová
povrchové	povrchový	k2eAgFnSc6d1	povrchová
vrstvě	vrstva	k1gFnSc6	vrstva
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
okolo	okolo	k7c2	okolo
1012	[number]	k4	1012
tun	tuna	k1gFnPc2	tuna
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
činí	činit	k5eAaImIp3nS	činit
průměrný	průměrný	k2eAgInSc4d1	průměrný
obsah	obsah	k1gInSc4	obsah
železa	železo	k1gNnSc2	železo
4,7	[number]	k4	4,7
-	-	kIx~	-
6,2	[number]	k4	6,2
%	%	kIx~	%
(	(	kIx(	(
<g/>
62	[number]	k4	62
000	[number]	k4	000
ppm	ppm	k?	ppm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
podle	podle	k7c2	podle
výskytu	výskyt	k1gInSc2	výskyt
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
kyslík	kyslík	k1gInSc1	kyslík
<g/>
,	,	kIx,	,
křemík	křemík	k1gInSc1	křemík
a	a	k8xC	a
hliník	hliník	k1gInSc1	hliník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemské	zemský	k2eAgNnSc1d1	zemské
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
složeno	složen	k2eAgNnSc1d1	složeno
především	především	k9	především
ze	z	k7c2	z
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
kobaltu	kobalt	k1gInSc2	kobalt
a	a	k8xC	a
niklu	nikl	k1gInSc2	nikl
a	a	k8xC	a
celkový	celkový	k2eAgInSc1d1	celkový
podíl	podíl	k1gInSc1	podíl
železa	železo	k1gNnSc2	železo
na	na	k7c4	na
složení	složení	k1gNnSc4	složení
naší	náš	k3xOp1gFnSc2	náš
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
podstatně	podstatně	k6eAd1	podstatně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
údajů	údaj	k1gInPc2	údaj
až	až	k9	až
35	[number]	k4	35
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
0,01	[number]	k4	0,01
miligramů	miligram	k1gInPc2	miligram
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
železa	železo	k1gNnSc2	železo
přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
000	[number]	k4	000
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
železo	železo	k1gNnSc1	železo
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
rudách	ruda	k1gFnPc6	ruda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
průmyslově	průmyslově	k6eAd1	průmyslově
využity	využit	k2eAgInPc4d1	využit
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
výrobě	výroba	k1gFnSc3	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
oxidů	oxid	k1gInPc2	oxid
a	a	k8xC	a
uhličitanů	uhličitan	k1gInPc2	uhličitan
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaImF	jmenovat
například	například	k6eAd1	například
hematit	hematit	k1gInSc4	hematit
(	(	kIx(	(
<g/>
krevel	krevel	k1gInSc1	krevel
<g/>
)	)	kIx)	)
Fe	Fe	k1gFnSc1	Fe
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
magnetit	magnetit	k1gInSc1	magnetit
(	(	kIx(	(
<g/>
magnetovec	magnetovec	k1gInSc1	magnetovec
<g/>
)	)	kIx)	)
Fe	Fe	k1gFnSc1	Fe
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
limonit	limonit	k1gInSc1	limonit
(	(	kIx(	(
<g/>
hnědel	hnědel	k1gInSc1	hnědel
<g/>
)	)	kIx)	)
Fe	Fe	k1gFnSc1	Fe
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
·	·	k?	·
x	x	k?	x
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
ilmenit	ilmenit	k1gInSc1	ilmenit
FeTiO	FeTiO	k1gFnSc1	FeTiO
<g/>
3	[number]	k4	3
nebo	nebo	k8xC	nebo
siderit	siderit	k1gInSc1	siderit
(	(	kIx(	(
<g/>
ocelek	ocelek	k1gInSc1	ocelek
<g/>
)	)	kIx)	)
FeCO	FeCO	k1gFnSc1	FeCO
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
i	i	k9	i
sulfidický	sulfidický	k2eAgInSc1d1	sulfidický
pyrit	pyrit	k1gInSc1	pyrit
FeS	fes	k1gNnSc1	fes
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
kvůli	kvůli	k7c3	kvůli
obtížnému	obtížný	k2eAgNnSc3d1	obtížné
oddělení	oddělení	k1gNnSc3	oddělení
síry	síra	k1gFnSc2	síra
od	od	k7c2	od
železa	železo	k1gNnSc2	železo
průmyslově	průmyslově	k6eAd1	průmyslově
tolik	tolik	k4xDc4	tolik
nezpracovává	zpracovávat	k5eNaImIp3nS	zpracovávat
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
do	do	k7c2	do
obsahu	obsah	k1gInSc2	obsah
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
rud	ruda	k1gFnPc2	ruda
nejhodnotnější	hodnotný	k2eAgInSc4d3	nejhodnotnější
magnetit	magnetit	k1gInSc4	magnetit
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
téměř	téměř	k6eAd1	téměř
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
hmotnosti	hmotnost	k1gFnSc2	hmotnost
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejhojněji	hojně	k6eAd3	hojně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgFnSc7d1	vyskytující
rudou	ruda	k1gFnSc7	ruda
je	být	k5eAaImIp3nS	být
hematit	hematit	k1gInSc1	hematit
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
dostupnosti	dostupnost	k1gFnSc3	dostupnost
železných	železný	k2eAgFnPc2d1	železná
rud	ruda	k1gFnPc2	ruda
se	se	k3xPyFc4	se
vybírají	vybírat	k5eAaImIp3nP	vybírat
ty	ten	k3xDgInPc1	ten
kvalitnější	kvalitní	k2eAgInPc1d2	kvalitnější
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
méně	málo	k6eAd2	málo
známým	známý	k2eAgFnPc3d1	známá
rudám	ruda	k1gFnPc3	ruda
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
nelze	lze	k6eNd1	lze
využívat	využívat	k5eAaPmF	využívat
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
výrobě	výroba	k1gFnSc3	výroba
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
augit	augit	k1gInSc1	augit
(	(	kIx(	(
<g/>
Ca	ca	kA	ca
<g/>
,	,	kIx,	,
<g/>
Mg	mg	kA	mg
<g/>
,	,	kIx,	,
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
)	)	kIx)	)
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
chromit	chromit	k1gInSc1	chromit
(	(	kIx(	(
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
,	,	kIx,	,
Mg	mg	kA	mg
<g/>
)	)	kIx)	)
<g/>
Cr	cr	k0	cr
<g/>
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
kobaltin	kobaltin	k1gMnSc1	kobaltin
(	(	kIx(	(
<g/>
Co	co	k3yRnSc1	co
<g/>
,	,	kIx,	,
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
)	)	kIx)	)
<g/>
AsS	AsS	k1gMnSc1	AsS
<g/>
,	,	kIx,	,
jakobsit	jakobsit	k1gInSc1	jakobsit
MnFe	MnFe	k1gFnSc1	MnFe
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
andradit	andradit	k5eAaPmF	andradit
Ca	ca	kA	ca
<g/>
3	[number]	k4	3
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
23	[number]	k4	23
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
olivín	olivín	k1gInSc1	olivín
(	(	kIx(	(
<g/>
Mg	mg	kA	mg
<g/>
,	,	kIx,	,
Fe	Fe	k1gFnSc1	Fe
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
SiO	SiO	k1gFnPc2	SiO
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
triplit	triplit	k1gInSc1	triplit
(	(	kIx(	(
<g/>
Mn	Mn	k1gMnSc1	Mn
<g/>
,	,	kIx,	,
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
,	,	kIx,	,
<g/>
Mg	mg	kA	mg
<g/>
,	,	kIx,	,
<g/>
Ca	ca	kA	ca
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
F	F	kA	F
<g/>
,	,	kIx,	,
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vivianit	vivianit	k1gInSc1	vivianit
Fe	Fe	k1gFnSc1	Fe
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
·	·	k?	·
8	[number]	k4	8
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
a	a	k8xC	a
wolframit	wolframit	k1gInSc1	wolframit
(	(	kIx(	(
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
,	,	kIx,	,
<g/>
Mn	Mn	k1gMnSc1	Mn
<g/>
)	)	kIx)	)
<g/>
WO	WO	kA	WO
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgNnSc4d1	elementární
kovové	kovový	k2eAgNnSc4d1	kovové
železo	železo	k1gNnSc4	železo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
pozemské	pozemský	k2eAgNnSc1d1	pozemské
nebo	nebo	k8xC	nebo
meteoritické	meteoritický	k2eAgNnSc1d1	meteoritické
<g/>
.	.	kIx.	.
</s>
<s>
Pozemské	pozemský	k2eAgNnSc1d1	pozemské
je	být	k5eAaImIp3nS	být
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
nejspíše	nejspíše	k9	nejspíše
"	"	kIx"	"
<g/>
přirozeným	přirozený	k2eAgNnSc7d1	přirozené
zhutněním	zhutnění	k1gNnSc7	zhutnění
<g/>
"	"	kIx"	"
železa	železo	k1gNnSc2	železo
obsaženého	obsažený	k2eAgNnSc2d1	obsažené
v	v	k7c6	v
bazaltech	bazalt	k1gInPc6	bazalt
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
uhelných	uhelný	k2eAgFnPc2d1	uhelná
vrstev	vrstva	k1gFnPc2	vrstva
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc2	jejich
uzavřenin	uzavřenina	k1gFnPc2	uzavřenina
(	(	kIx(	(
<g/>
výskyty	výskyt	k1gInPc7	výskyt
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Disko	disko	k1gNnSc2	disko
u	u	k7c2	u
Grónska	Grónsko	k1gNnSc2	Grónsko
či	či	k8xC	či
v	v	k7c6	v
Bühlu	Bühl	k1gInSc6	Bühl
u	u	k7c2	u
Kasselu	Kassel	k1gInSc2	Kassel
(	(	kIx(	(
<g/>
Hesensko	Hesensko	k1gNnSc1	Hesensko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
vzácně	vzácně	k6eAd1	vzácně
ze	z	k7c2	z
Strak	straka	k1gFnPc2	straka
u	u	k7c2	u
Duchcova	Duchcův	k2eAgInSc2d1	Duchcův
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Meteorický	meteorický	k2eAgInSc1d1	meteorický
původ	původ	k1gInSc1	původ
železa	železo	k1gNnSc2	železo
je	být	k5eAaImIp3nS	být
praktickým	praktický	k2eAgInSc7d1	praktický
důkazem	důkaz	k1gInSc7	důkaz
významné	významný	k2eAgFnSc2d1	významná
přítomnosti	přítomnost	k1gFnSc2	přítomnost
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
okolním	okolní	k2eAgInSc6d1	okolní
vesmíru	vesmír	k1gInSc6	vesmír
a	a	k8xC	a
současně	současně	k6eAd1	současně
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
bylo	být	k5eAaImAgNnS	být
jediným	jediný	k2eAgInSc7d1	jediný
zdrojem	zdroj	k1gInSc7	zdroj
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
železných	železný	k2eAgInPc2d1	železný
nástrojů	nástroj	k1gInPc2	nástroj
(	(	kIx(	(
<g/>
nože	nůž	k1gInPc4	nůž
<g/>
,	,	kIx,	,
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Výroba	výroba	k1gFnSc1	výroba
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
čisté	čistý	k2eAgNnSc4d1	čisté
železo	železo	k1gNnSc4	železo
se	se	k3xPyFc4	se
v	v	k7c6	v
menším	malý	k2eAgNnSc6d2	menší
množství	množství	k1gNnSc6	množství
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
redukcí	redukce	k1gFnSc7	redukce
čistého	čistý	k2eAgInSc2d1	čistý
oxidu	oxid	k1gInSc2	oxid
nebo	nebo	k8xC	nebo
hydroxidu	hydroxid	k1gInSc2	hydroxid
vodíkem	vodík	k1gInSc7	vodík
nebo	nebo	k8xC	nebo
karbonylovým	karbonylový	k2eAgInSc7d1	karbonylový
způsobem	způsob	k1gInSc7	způsob
zahříváním	zahřívání	k1gNnSc7	zahřívání
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
CO	co	k6eAd1	co
při	při	k7c6	při
zvýšeném	zvýšený	k2eAgInSc6d1	zvýšený
tlaku	tlak	k1gInSc6	tlak
a	a	k8xC	a
následným	následný	k2eAgInSc7d1	následný
rozkladem	rozklad	k1gInSc7	rozklad
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
pentakarbonylu	pentakarbonyl	k1gInSc2	pentakarbonyl
železa	železo	k1gNnSc2	železo
Fe	Fe	k1gFnSc2	Fe
<g/>
(	(	kIx(	(
<g/>
CO	co	k6eAd1	co
<g/>
)	)	kIx)	)
<g/>
5	[number]	k4	5
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
250	[number]	k4	250
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c4	na
práškový	práškový	k2eAgInSc4d1	práškový
kov	kov	k1gInSc4	kov
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
běžné	běžný	k2eAgNnSc4d1	běžné
použití	použití	k1gNnSc4	použití
se	se	k3xPyFc4	se
železo	železo	k1gNnSc1	železo
získává	získávat	k5eAaImIp3nS	získávat
jakožto	jakožto	k8xS	jakožto
surové	surový	k2eAgNnSc1d1	surové
železo	železo	k1gNnSc1	železo
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
peci	pec	k1gFnSc6	pec
redukcí	redukce	k1gFnPc2	redukce
svých	svůj	k3xOyFgInPc2	svůj
oxidů	oxid	k1gInPc2	oxid
koksem	koks	k1gInSc7	koks
nebo	nebo	k8xC	nebo
oxidem	oxid	k1gInSc7	oxid
uhelnatým	uhelnatý	k2eAgInSc7d1	uhelnatý
<g/>
.	.	kIx.	.
</s>
<s>
Upravené	upravený	k2eAgFnPc1d1	upravená
rudy	ruda	k1gFnPc1	ruda
se	se	k3xPyFc4	se
střídavě	střídavě	k6eAd1	střídavě
naváží	navážet	k5eAaImIp3nP	navážet
se	s	k7c7	s
struskotvornými	struskotvorný	k2eAgFnPc7d1	struskotvorná
látkami	látka	k1gFnPc7	látka
(	(	kIx(	(
<g/>
vápenec	vápenec	k1gInSc1	vápenec
<g/>
,	,	kIx,	,
oxidy	oxid	k1gInPc1	oxid
křemíku	křemík	k1gInSc2	křemík
<g/>
)	)	kIx)	)
a	a	k8xC	a
koksem	koks	k1gInSc7	koks
do	do	k7c2	do
vysoké	vysoký	k2eAgFnSc2d1	vysoká
pece	pec	k1gFnSc2	pec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
peci	pec	k1gFnSc6	pec
probíhá	probíhat	k5eAaImIp3nS	probíhat
několik	několik	k4yIc1	několik
dějů	děj	k1gInPc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
pece	pec	k1gFnSc2	pec
se	se	k3xPyFc4	se
spaluje	spalovat	k5eAaImIp3nS	spalovat
koks	koks	k1gInSc1	koks
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgMnSc1d1	uhličitý
CO2	CO2	k1gMnSc1	CO2
díky	díky	k7c3	díky
vhánění	vhánění	k1gNnSc3	vhánění
předehřátého	předehřátý	k2eAgInSc2d1	předehřátý
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
vzduchu	vzduch	k1gInSc2	vzduch
obsahujícího	obsahující	k2eAgMnSc2d1	obsahující
topný	topný	k2eAgInSc4d1	topný
olej	olej	k1gInSc4	olej
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
+	+	kIx~	+
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
+	+	kIx~	+
<g/>
O_	O_	k1gMnSc1	O_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
CO_	CO_	k1gMnSc1	CO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
}	}	kIx)	}
:	:	kIx,	:
Touto	tento	k3xDgFnSc7	tento
exotermickou	exotermický	k2eAgFnSc7d1	exotermická
reakcí	reakce	k1gFnSc7	reakce
se	se	k3xPyFc4	se
pec	pec	k1gFnSc1	pec
v	v	k7c6	v
tavicí	tavicí	k2eAgFnSc6d1	tavicí
zóně	zóna	k1gFnSc6	zóna
vyhřívá	vyhřívat	k5eAaImIp3nS	vyhřívat
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
okolo	okolo	k7c2	okolo
2	[number]	k4	2
000	[number]	k4	000
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobí	způsobit	k5eAaPmIp3nS	způsobit
roztavení	roztavení	k1gNnSc4	roztavení
surového	surový	k2eAgNnSc2d1	surové
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
tvorbu	tvorba	k1gFnSc4	tvorba
strusky	struska	k1gFnSc2	struska
<g/>
.	.	kIx.	.
</s>
<s>
Struska	struska	k1gFnSc1	struska
je	být	k5eAaImIp3nS	být
vrstva	vrstva	k1gFnSc1	vrstva
lehkých	lehký	k2eAgInPc2d1	lehký
oxidů	oxid	k1gInPc2	oxid
a	a	k8xC	a
silikátů	silikát	k1gInPc2	silikát
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
taveninu	tavenina	k1gFnSc4	tavenina
před	před	k7c7	před
zpětnou	zpětný	k2eAgFnSc7d1	zpětná
oxidací	oxidace	k1gFnSc7	oxidace
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
i	i	k9	i
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
C	C	kA	C
a	a	k8xC	a
O	O	kA	O
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
C	C	kA	C
a	a	k8xC	a
S	s	k7c7	s
i	i	k9	i
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
SiO_	SiO_	k1gFnSc1	SiO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
CaO	CaO	k1gFnSc1	CaO
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
CaSiO_	CaSiO_	k1gMnSc1	CaSiO_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
}	}	kIx)	}
:	:	kIx,	:
Na	na	k7c6	na
dně	dno	k1gNnSc6	dno
se	se	k3xPyFc4	se
hromadí	hromadit	k5eAaImIp3nS	hromadit
roztavené	roztavený	k2eAgNnSc1d1	roztavené
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
vypouští	vypouštět	k5eAaImIp3nS	vypouštět
odděleně	odděleně	k6eAd1	odděleně
od	od	k7c2	od
strusky	struska	k1gFnSc2	struska
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
vrstvách	vrstva	k1gFnPc6	vrstva
pece	pec	k1gFnSc2	pec
probíhá	probíhat	k5eAaImIp3nS	probíhat
nepřímá	přímý	k2eNgFnSc1d1	nepřímá
redukce	redukce	k1gFnSc1	redukce
oxidů	oxid	k1gInPc2	oxid
železa	železo	k1gNnSc2	železo
oxidem	oxid	k1gInSc7	oxid
uhelnatým	uhelnatý	k2eAgInSc7d1	uhelnatý
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
získají	získat	k5eAaPmIp3nP	získat
asi	asi	k9	asi
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc4	třetina
vyrobeného	vyrobený	k2eAgNnSc2d1	vyrobené
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
redukci	redukce	k1gFnSc3	redukce
oxidu	oxid	k1gInSc2	oxid
železitého	železitý	k2eAgInSc2d1	železitý
(	(	kIx(	(
<g/>
přechodně	přechodně	k6eAd1	přechodně
vzniká	vznikat	k5eAaImIp3nS	vznikat
oxid	oxid	k1gInSc4	oxid
železnato-železitý	železnato-železitý	k2eAgInSc4d1	železnato-železitý
Fe	Fe	k1gFnSc7	Fe
<g/>
3	[number]	k4	3
<g/>
O	O	kA	O
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
rozkladu	rozklad	k1gInSc3	rozklad
vápence	vápenec	k1gInSc2	vápenec
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
vápenatý	vápenatý	k2eAgMnSc1d1	vápenatý
CaO	CaO	k1gMnSc1	CaO
a	a	k8xC	a
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
(	(	kIx(	(
<g/>
reakce	reakce	k1gFnPc1	reakce
potřebující	potřebující	k2eAgFnPc1d1	potřebující
méně	málo	k6eAd2	málo
energie	energie	k1gFnPc4	energie
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
e	e	k0	e
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
C	C	kA	C
O	o	k7c4	o
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
C	C	kA	C
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
Fe_	Fe_	k1gMnSc1	Fe_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O_	O_	k1gFnSc2	O_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
CO	co	k3yInSc1	co
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
Fe_	Fe_	k1gMnSc1	Fe_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
O_	O_	k1gFnSc1	O_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
CO_	CO_	k1gMnSc1	CO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
C	C	kA	C
O	o	k7c4	o
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
F	F	kA	F
e	e	k0	e
O	O	kA	O
+	+	kIx~	+
C	C	kA	C
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
Fe_	Fe_	k1gFnSc1	Fe_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
O_	O_	k1gFnSc2	O_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
CO	co	k3yInSc1	co
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
FeO	FeO	k1gFnSc1	FeO
<g/>
+	+	kIx~	+
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
CO_	CO_	k1gFnSc1	CO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
a	a	k8xC	a
C	C	kA	C
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
C	C	kA	C
a	a	k8xC	a
O	O	kA	O
+	+	kIx~	+
C	C	kA	C
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
CaCO_	CaCO_	k1gFnSc1	CaCO_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
CaO	CaO	k1gMnSc1	CaO
<g/>
+	+	kIx~	+
<g/>
CO_	CO_	k1gMnSc1	CO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
}	}	kIx)	}
:	:	kIx,	:
V	v	k7c6	v
teplejší	teplý	k2eAgFnSc6d2	teplejší
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
redukuje	redukovat	k5eAaBmIp3nS	redukovat
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
oxid	oxid	k1gInSc1	oxid
železnatý	železnatý	k2eAgInSc1d1	železnatý
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nedokonalému	dokonalý	k2eNgNnSc3d1	nedokonalé
spalování	spalování	k1gNnSc3	spalování
koksu	koks	k1gInSc2	koks
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
dalšího	další	k2eAgInSc2d1	další
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
F	F	kA	F
e	e	k0	e
O	O	kA	O
+	+	kIx~	+
C	C	kA	C
O	o	k7c4	o
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
F	F	kA	F
e	e	k0	e
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
)	)	kIx)	)
+	+	kIx~	+
C	C	kA	C
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
FeO	FeO	k1gFnSc1	FeO
<g/>
+	+	kIx~	+
<g/>
CO	co	k3yQnSc4	co
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
Fe	Fe	k1gMnSc1	Fe
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
CO_	CO_	k1gMnSc1	CO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
+	+	kIx~	+
C	C	kA	C
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
C	C	kA	C
O	O	kA	O
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
+	+	kIx~	+
<g/>
CO_	CO_	k1gMnSc1	CO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
CO	co	k3yInSc1	co
<g/>
}	}	kIx)	}
}	}	kIx)	}
:	:	kIx,	:
Ve	v	k7c6	v
spodních	spodní	k2eAgFnPc6d1	spodní
částech	část	k1gFnPc6	část
probíhá	probíhat	k5eAaImIp3nS	probíhat
přímá	přímý	k2eAgFnSc1d1	přímá
redukce	redukce	k1gFnSc1	redukce
oxidů	oxid	k1gInPc2	oxid
železa	železo	k1gNnSc2	železo
uhlíkem	uhlík	k1gInSc7	uhlík
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
3	[number]	k4	3
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
F	F	kA	F
e	e	k0	e
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
)	)	kIx)	)
+	+	kIx~	+
3	[number]	k4	3
:	:	kIx,	:
C	C	kA	C
O	O	kA	O
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
Fe_	Fe_	k1gFnSc1	Fe_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
O_	O_	k1gFnSc1	O_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
C	C	kA	C
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
CO	co	k3yInSc1	co
<g/>
}	}	kIx)	}
}	}	kIx)	}
:	:	kIx,	:
Celý	celý	k2eAgInSc1d1	celý
proces	proces	k1gInSc1	proces
probíhá	probíhat	k5eAaImIp3nS	probíhat
prakticky	prakticky	k6eAd1	prakticky
nepřetržitě	přetržitě	k6eNd1	přetržitě
-	-	kIx~	-
na	na	k7c4	na
horní	horní	k2eAgFnSc4d1	horní
část	část	k1gFnSc4	část
vysoké	vysoký	k2eAgFnSc2d1	vysoká
pece	pec	k1gFnSc2	pec
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
zaváží	zavážit	k5eAaPmIp3nS	zavážit
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
<g/>
,	,	kIx,	,
železný	železný	k2eAgInSc1d1	železný
šrot	šrot	k1gInSc1	šrot
<g/>
,	,	kIx,	,
koks	koks	k1gInSc1	koks
a	a	k8xC	a
struskotvorné	struskotvorný	k2eAgFnPc1d1	struskotvorná
přísady	přísada	k1gFnPc1	přísada
a	a	k8xC	a
naspodu	naspodu	k6eAd1	naspodu
se	se	k3xPyFc4	se
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
intervalech	interval	k1gInPc6	interval
odpouští	odpouštět	k5eAaImIp3nS	odpouštět
roztavené	roztavený	k2eAgNnSc4d1	roztavené
kovové	kovový	k2eAgNnSc4d1	kovové
železo	železo	k1gNnSc4	železo
-	-	kIx~	-
odpich	odpich	k1gInSc1	odpich
vysoké	vysoký	k2eAgFnSc2d1	vysoká
pece	pec	k1gFnSc2	pec
<g/>
.	.	kIx.	.
</s>
<s>
Surové	surový	k2eAgNnSc1d1	surové
železo	železo	k1gNnSc1	železo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řadu	řada	k1gFnSc4	řada
nežádoucích	žádoucí	k2eNgFnPc2d1	nežádoucí
příměsí	příměs	k1gFnPc2	příměs
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
uhlík	uhlík	k1gInSc4	uhlík
C	C	kA	C
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
křemík	křemík	k1gInSc1	křemík
Si	se	k3xPyFc3	se
a	a	k8xC	a
fosfor	fosfor	k1gInSc4	fosfor
P.	P.	kA	P.
V	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
přijímá	přijímat	k5eAaImIp3nS	přijímat
i	i	k9	i
síru	síra	k1gFnSc4	síra
S	s	k7c7	s
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
více	hodně	k6eAd2	hodně
zadržena	zadržet	k5eAaPmNgFnS	zadržet
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
sulfidu	sulfid	k1gInSc2	sulfid
manganatého	manganatý	k2eAgMnSc4d1	manganatý
MnS	MnS	k1gMnSc4	MnS
ve	v	k7c6	v
strusce	struska	k1gFnSc6	struska
<g/>
.	.	kIx.	.
</s>
<s>
Surové	surový	k2eAgNnSc1d1	surové
železo	železo	k1gNnSc1	železo
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
odlévá	odlévat	k5eAaImIp3nS	odlévat
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
do	do	k7c2	do
forem	forma	k1gFnPc2	forma
požadované	požadovaný	k2eAgFnSc2d1	požadovaná
velikosti	velikost	k1gFnSc2	velikost
nebo	nebo	k8xC	nebo
do	do	k7c2	do
ingotů	ingot	k1gInPc2	ingot
(	(	kIx(	(
<g/>
housek	houska	k1gFnPc2	houska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
produkt	produkt	k1gInSc1	produkt
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
pevný	pevný	k2eAgInSc4d1	pevný
a	a	k8xC	a
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
křehký	křehký	k2eAgInSc1d1	křehký
<g/>
,	,	kIx,	,
a	a	k8xC	a
možnost	možnost	k1gFnSc4	možnost
jeho	on	k3xPp3gNnSc2	on
dalšího	další	k2eAgNnSc2d1	další
mechanického	mechanický	k2eAgNnSc2d1	mechanické
opracování	opracování	k1gNnSc2	opracování
po	po	k7c6	po
odlití	odlití	k1gNnSc6	odlití
je	být	k5eAaImIp3nS	být
minimální	minimální	k2eAgInSc1d1	minimální
<g/>
;	;	kIx,	;
prakticky	prakticky	k6eAd1	prakticky
všechno	všechen	k3xTgNnSc1	všechen
surové	surový	k2eAgNnSc1d1	surové
železo	železo	k1gNnSc1	železo
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
na	na	k7c4	na
ocel	ocel	k1gFnSc4	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Struska	struska	k1gFnSc1	struska
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
tvárnic	tvárnice	k1gFnPc2	tvárnice
<g/>
,	,	kIx,	,
izolací	izolace	k1gFnPc2	izolace
stěn	stěna	k1gFnPc2	stěna
a	a	k8xC	a
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
cementu	cement	k1gInSc2	cement
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Výroba	výroba	k1gFnSc1	výroba
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
uhlíku	uhlík	k1gInSc2	uhlík
v	v	k7c6	v
surovém	surový	k2eAgNnSc6d1	surové
železe	železo	k1gNnSc6	železo
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
vysoký	vysoký	k2eAgInSc1d1	vysoký
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
jej	on	k3xPp3gMnSc4	on
oxidačním	oxidační	k2eAgInSc7d1	oxidační
procesem	proces	k1gInSc7	proces
v	v	k7c6	v
ocelářských	ocelářský	k2eAgNnPc6d1	ocelářské
zařízeních	zařízení	k1gNnPc6	zařízení
snížit	snížit	k5eAaPmF	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
dociluje	docilovat	k5eAaImIp3nS	docilovat
oxidací	oxidace	k1gFnSc7	oxidace
uhlíku	uhlík	k1gInSc2	uhlík
buďto	buďto	k8xC	buďto
kyslíkem	kyslík	k1gInSc7	kyslík
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
Thomasův	Thomasův	k2eAgInSc1d1	Thomasův
konvertor	konvertor	k1gInSc1	konvertor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profoukáváním	profoukávání	k1gNnSc7	profoukávání
kyslíkem	kyslík	k1gInSc7	kyslík
(	(	kIx(	(
<g/>
LD	LD	kA	LD
konvertor	konvertor	k1gInSc1	konvertor
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
přisazováním	přisazování	k1gNnSc7	přisazování
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
a	a	k8xC	a
ocelového	ocelový	k2eAgInSc2d1	ocelový
odpadu	odpad	k1gInSc2	odpad
do	do	k7c2	do
taveniny	tavenina	k1gFnSc2	tavenina
v	v	k7c6	v
nístějových	nístějový	k2eAgFnPc6d1	nístějová
pecích	pec	k1gFnPc6	pec
(	(	kIx(	(
<g/>
Siemens-Martinův	Siemens-Martinův	k2eAgInSc1d1	Siemens-Martinův
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
elektrická	elektrický	k2eAgFnSc1d1	elektrická
oblouková	obloukový	k2eAgFnSc1d1	oblouková
pec	pec	k1gFnSc1	pec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Získaná	získaný	k2eAgFnSc1d1	získaná
nelegovaná	legovaný	k2eNgFnSc1d1	nelegovaná
neboli	neboli	k8xC	neboli
měkká	měkký	k2eAgFnSc1d1	měkká
ocel	ocel	k1gFnSc1	ocel
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
měkká	měkký	k2eAgFnSc1d1	měkká
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
mechanicky	mechanicky	k6eAd1	mechanicky
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
(	(	kIx(	(
<g/>
tažení	tažení	k1gNnSc1	tažení
<g/>
,	,	kIx,	,
kování	kování	k1gNnSc1	kování
<g/>
,	,	kIx,	,
ohýbání	ohýbání	k1gNnSc1	ohýbání
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1	mechanická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
dále	daleko	k6eAd2	daleko
upravovat	upravovat	k5eAaImF	upravovat
tepelným	tepelný	k2eAgNnSc7d1	tepelné
zpracováním	zpracování	k1gNnSc7	zpracování
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kalením	kalení	k1gNnSc7	kalení
(	(	kIx(	(
<g/>
zahřátím	zahřátí	k1gNnSc7	zahřátí
do	do	k7c2	do
červeného	červený	k2eAgInSc2d1	červený
žáru	žár	k1gInSc2	žár
a	a	k8xC	a
prudkým	prudký	k2eAgNnSc7d1	prudké
zchlazením	zchlazení	k1gNnSc7	zchlazení
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
minerálním	minerální	k2eAgInSc7d1	minerální
olejem	olej	k1gInSc7	olej
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
popouštěním	popouštění	k1gNnSc7	popouštění
(	(	kIx(	(
<g/>
zahřátím	zahřátí	k1gNnSc7	zahřátí
na	na	k7c6	na
200-300	[number]	k4	200-300
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
pomalým	pomalý	k2eAgNnSc7d1	pomalé
chlazením	chlazení	k1gNnSc7	chlazení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
drátů	drát	k1gInPc2	drát
<g/>
,	,	kIx,	,
plechů	plech	k1gInPc2	plech
<g/>
,	,	kIx,	,
hřebíků	hřebík	k1gInPc2	hřebík
a	a	k8xC	a
podobných	podobný	k2eAgInPc2d1	podobný
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
zkvalitnění	zkvalitnění	k1gNnSc4	zkvalitnění
vyrobené	vyrobený	k2eAgFnSc2d1	vyrobená
oceli	ocel	k1gFnSc2	ocel
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
legováním	legování	k1gNnSc7	legování
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
přídavky	přídavek	k1gInPc1	přídavek
definovaných	definovaný	k2eAgNnPc2d1	definované
množství	množství	k1gNnPc2	množství
jiných	jiný	k2eAgInPc2d1	jiný
kovů	kov	k1gInPc2	kov
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
slitiny	slitina	k1gFnSc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
prvky	prvek	k1gInPc7	prvek
pro	pro	k7c4	pro
legování	legování	k1gNnSc4	legování
ocelí	ocel	k1gFnPc2	ocel
jsou	být	k5eAaImIp3nP	být
nikl	nikl	k1gInSc1	nikl
<g/>
,	,	kIx,	,
chrom	chrom	k1gInSc1	chrom
<g/>
,	,	kIx,	,
vanad	vanad	k1gInSc1	vanad
<g/>
,	,	kIx,	,
mangan	mangan	k1gInSc1	mangan
<g/>
,	,	kIx,	,
wolfram	wolfram	k1gInSc1	wolfram
<g/>
,	,	kIx,	,
kobalt	kobalt	k1gInSc1	kobalt
a	a	k8xC	a
ve	v	k7c6	v
speciálních	speciální	k2eAgFnPc6d1	speciální
aplikacích	aplikace	k1gFnPc6	aplikace
ještě	ještě	k9	ještě
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
uklidněné	uklidněná	k1gFnPc4	uklidněná
a	a	k8xC	a
neuklidněné	uklidněný	k2eNgFnPc4d1	uklidněný
oceli	ocel	k1gFnPc4	ocel
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
uklidněných	uklidněný	k2eAgFnPc2d1	uklidněná
ocelí	ocel	k1gFnPc2	ocel
je	být	k5eAaImIp3nS	být
rozpuštěný	rozpuštěný	k2eAgInSc1d1	rozpuštěný
kyslík	kyslík	k1gInSc1	kyslík
vázán	vázat	k5eAaImNgInS	vázat
přísadou	přísada	k1gFnSc7	přísada
hliníku	hliník	k1gInSc2	hliník
(	(	kIx(	(
<g/>
Al	ala	k1gFnPc2	ala
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
křemíku	křemík	k1gInSc2	křemík
(	(	kIx(	(
<g/>
Si	se	k3xPyFc3	se
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
000	[number]	k4	000
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
ocelí	ocel	k1gFnPc2	ocel
s	s	k7c7	s
přesně	přesně	k6eAd1	přesně
definovaným	definovaný	k2eAgNnSc7d1	definované
složením	složení	k1gNnSc7	složení
a	a	k8xC	a
mechanickými	mechanický	k2eAgFnPc7d1	mechanická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
,	,	kIx,	,
tvrdost	tvrdost	k1gFnSc1	tvrdost
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
odolnost	odolnost	k1gFnSc1	odolnost
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgNnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ocelové	ocelový	k2eAgInPc1d1	ocelový
polotovary	polotovar	k1gInPc1	polotovar
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
zpracovány	zpracovat	k5eAaPmNgInP	zpracovat
ve	v	k7c6	v
válcovnách	válcovna	k1gFnPc6	válcovna
na	na	k7c4	na
drát	drát	k1gInSc4	drát
<g/>
,	,	kIx,	,
plech	plech	k1gInSc4	plech
<g/>
,	,	kIx,	,
nosníky	nosník	k1gInPc4	nosník
<g/>
,	,	kIx,	,
kolejnice	kolejnice	k1gFnPc4	kolejnice
<g/>
,	,	kIx,	,
profily	profil	k1gInPc4	profil
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
široce	široko	k6eAd1	široko
používány	používat	k5eAaImNgFnP	používat
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
atd.	atd.	kA	atd.
Část	část	k1gFnSc1	část
těchto	tento	k3xDgInPc2	tento
polotovarů	polotovar	k1gInPc2	polotovar
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
výchozí	výchozí	k2eAgInSc1d1	výchozí
materiál	materiál	k1gInSc1	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
výkovků	výkovek	k1gInPc2	výkovek
v	v	k7c6	v
kovárnách	kovárna	k1gFnPc6	kovárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
4	[number]	k4	4
izotopy	izotop	k1gInPc7	izotop
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
přírodním	přírodní	k2eAgInSc7d1	přírodní
izotopem	izotop	k1gInSc7	izotop
je	být	k5eAaImIp3nS	být
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
stabilitě	stabilita	k1gFnSc3	stabilita
svého	svůj	k3xOyFgNnSc2	svůj
jádra	jádro	k1gNnSc2	jádro
56	[number]	k4	56
<g/>
Fe	Fe	k1gFnPc2	Fe
(	(	kIx(	(
<g/>
91,75	[number]	k4	91,75
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uvedený	uvedený	k2eAgInSc1d1	uvedený
izotop	izotop	k1gInSc1	izotop
má	mít	k5eAaImIp3nS	mít
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
hmotnostní	hmotnostní	k2eAgInSc1d1	hmotnostní
schodek	schodek	k1gInSc1	schodek
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
největší	veliký	k2eAgFnSc4d3	veliký
vazebnou	vazebný	k2eAgFnSc4d1	vazebná
energii	energie	k1gFnSc4	energie
jádra	jádro	k1gNnSc2	jádro
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
známých	známý	k2eAgInPc2d1	známý
izotopů	izotop	k1gInPc2	izotop
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
také	také	k9	také
konečným	konečný	k2eAgInSc7d1	konečný
produktem	produkt	k1gInSc7	produkt
nukleosyntézy	nukleosyntéza	k1gFnSc2	nukleosyntéza
ve	v	k7c6	v
hvězdách	hvězda	k1gFnPc6	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgInPc1d1	zbývající
tři	tři	k4xCgInPc1	tři
stabilní	stabilní	k2eAgInPc1d1	stabilní
izotopy	izotop	k1gInPc1	izotop
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
54	[number]	k4	54
<g/>
Fe	Fe	k1gFnPc2	Fe
(	(	kIx(	(
<g/>
5,85	[number]	k4	5,85
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,57	,57	k4	,57
<g/>
Fe	Fe	k1gFnPc2	Fe
(	(	kIx(	(
<g/>
2,12	[number]	k4	2,12
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
58	[number]	k4	58
<g/>
Fe	Fe	k1gFnPc2	Fe
(	(	kIx(	(
<g/>
0,28	[number]	k4	0,28
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
60	[number]	k4	60
<g/>
Fe	Fe	k1gFnPc2	Fe
je	být	k5eAaImIp3nS	být
zaniklý	zaniklý	k2eAgInSc1d1	zaniklý
radionuklid	radionuklid	k1gInSc1	radionuklid
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
prací	práce	k1gFnPc2	práce
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
týkajících	týkající	k2eAgNnPc2d1	týkající
se	se	k3xPyFc4	se
měření	měření	k1gNnPc2	měření
izotopového	izotopový	k2eAgNnSc2d1	izotopové
složení	složení	k1gNnSc2	složení
Fe	Fe	k1gFnSc1	Fe
se	se	k3xPyFc4	se
soustřeďovala	soustřeďovat	k5eAaImAgFnS	soustřeďovat
na	na	k7c4	na
určování	určování	k1gNnSc4	určování
změn	změna	k1gFnPc2	změna
60	[number]	k4	60
<g/>
Fe	Fe	k1gFnPc2	Fe
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
procesům	proces	k1gInPc3	proces
doprovázející	doprovázející	k2eAgFnSc4d1	doprovázející
nukleosyntézu	nukleosyntéza	k1gFnSc4	nukleosyntéza
(	(	kIx(	(
<g/>
vznik	vznik	k1gInSc4	vznik
jádra	jádro	k1gNnSc2	jádro
atomu	atom	k1gInSc2	atom
z	z	k7c2	z
nukleonu	nukleon	k1gInSc2	nukleon
<g/>
)	)	kIx)	)
a	a	k8xC	a
formaci	formace	k1gFnSc4	formace
rudy	ruda	k1gFnSc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
desetiletí	desetiletí	k1gNnSc6	desetiletí
pokrok	pokrok	k1gInSc1	pokrok
v	v	k7c6	v
hmotnostní	hmotnostní	k2eAgFnSc6d1	hmotnostní
spektrometrii	spektrometrie	k1gFnSc6	spektrometrie
umožnil	umožnit	k5eAaPmAgInS	umožnit
detekci	detekce	k1gFnSc4	detekce
rozdílů	rozdíl	k1gInPc2	rozdíl
řádově	řádově	k6eAd1	řádově
v	v	k7c6	v
minutách	minuta	k1gFnPc6	minuta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
rozdíly	rozdíl	k1gInPc1	rozdíl
stabilních	stabilní	k2eAgInPc2d1	stabilní
izotopů	izotop	k1gInPc2	izotop
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
této	tento	k3xDgFnSc2	tento
práce	práce	k1gFnSc2	práce
byla	být	k5eAaImAgFnS	být
hnána	hnát	k5eAaImNgFnS	hnát
komunitou	komunita	k1gFnSc7	komunita
vědců	vědec	k1gMnPc2	vědec
zabývajících	zabývající	k2eAgMnPc2d1	zabývající
se	se	k3xPyFc4	se
Zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
planetárními	planetární	k2eAgFnPc7d1	planetární
vědami	věda	k1gFnPc7	věda
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
objevovat	objevovat	k5eAaImF	objevovat
využití	využití	k1gNnSc4	využití
i	i	k9	i
v	v	k7c6	v
biologických	biologický	k2eAgInPc6d1	biologický
a	a	k8xC	a
průmyslových	průmyslový	k2eAgInPc6d1	průmyslový
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
sloučeniny	sloučenina	k1gFnPc1	sloučenina
železa	železo	k1gNnSc2	železo
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Sloučeniny	sloučenina	k1gFnSc2	sloučenina
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
železo	železo	k1gNnSc1	železo
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
především	především	k9	především
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Fe	Fe	k1gFnSc2	Fe
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
a	a	k8xC	a
Fe	Fe	k1gFnSc1	Fe
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Redox	redox	k2eAgInSc1d1	redox
potenciál	potenciál	k1gInSc1	potenciál
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
přechodu	přechod	k1gInSc2	přechod
výše	výše	k1gFnSc2	výše
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
iontů	ion	k1gInPc2	ion
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
současnou	současný	k2eAgFnSc4d1	současná
existenci	existence	k1gFnSc4	existence
obou	dva	k4xCgFnPc2	dva
forem	forma	k1gFnPc2	forma
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Železo	železo	k1gNnSc1	železo
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
oxidačním	oxidační	k2eAgInSc6d1	oxidační
stavu	stav	k1gInSc6	stav
Fe	Fe	k1gFnSc2	Fe
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
a	a	k8xC	a
Fe	Fe	k1gFnSc1	Fe
<g/>
6	[number]	k4	6
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
těchto	tento	k3xDgInPc2	tento
oxidačních	oxidační	k2eAgInPc2d1	oxidační
stupňů	stupeň	k1gInPc2	stupeň
železa	železo	k1gNnSc2	železo
nejsou	být	k5eNaImIp3nP	být
příliš	příliš	k6eAd1	příliš
stabilní	stabilní	k2eAgFnPc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
běžnými	běžný	k2eAgFnPc7d1	běžná
minerálními	minerální	k2eAgFnPc7d1	minerální
kyselinami	kyselina	k1gFnPc7	kyselina
tvoří	tvořit	k5eAaImIp3nS	tvořit
železo	železo	k1gNnSc1	železo
soli	sůl	k1gFnSc2	sůl
v	v	k7c6	v
obou	dva	k4xCgNnPc6	dva
mocenstvích	mocenství	k1gNnPc6	mocenství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
solí	sůl	k1gFnPc2	sůl
bezbarvých	bezbarvý	k2eAgInPc2d1	bezbarvý
a	a	k8xC	a
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
mají	mít	k5eAaImIp3nP	mít
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc1	všechen
barvu	barva	k1gFnSc4	barva
-	-	kIx~	-
Fe	Fe	k1gFnSc1	Fe
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
soli	sůl	k1gFnPc1	sůl
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
bezbarvé	bezbarvý	k2eAgInPc1d1	bezbarvý
až	až	k9	až
světle	světle	k6eAd1	světle
zelené	zelený	k2eAgInPc1d1	zelený
a	a	k8xC	a
Fe	Fe	k1gFnSc1	Fe
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
soli	sůl	k1gFnPc1	sůl
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
tmavě	tmavě	k6eAd1	tmavě
žluté	žlutý	k2eAgFnPc1d1	žlutá
až	až	k8xS	až
hnědé	hnědý	k2eAgFnPc1d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
železnatou	železnatý	k2eAgFnSc7d1	železnatá
sloučeninou	sloučenina	k1gFnSc7	sloučenina
je	být	k5eAaImIp3nS	být
síran	síran	k1gInSc1	síran
železnatý	železnatý	k2eAgInSc1d1	železnatý
<g/>
,	,	kIx,	,
triviálně	triviálně	k6eAd1	triviálně
zelená	zelený	k2eAgFnSc1d1	zelená
skalice	skalice	k1gFnSc1	skalice
<g/>
.	.	kIx.	.
</s>
<s>
Železnaté	železnatý	k2eAgFnPc1d1	železnatá
soli	sůl	k1gFnPc1	sůl
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgFnPc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
jsou	být	k5eAaImIp3nP	být
bezbarvé	bezbarvý	k2eAgInPc1d1	bezbarvý
až	až	k9	až
světle	světle	k6eAd1	světle
zelené	zelený	k2eAgInPc1d1	zelený
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
oxidují	oxidovat	k5eAaBmIp3nP	oxidovat
na	na	k7c4	na
železité	železitý	k2eAgFnPc4d1	železitá
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
vznikající	vznikající	k2eAgFnSc1d1	vznikající
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
viditelného	viditelný	k2eAgNnSc2d1	viditelné
spektra	spektrum	k1gNnSc2	spektrum
železnatých	železnatý	k2eAgFnPc2d1	železnatá
sloučenin	sloučenina	k1gFnPc2	sloučenina
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
díky	díky	k7c3	díky
komplexu	komplex	k1gInSc3	komplex
[	[	kIx(	[
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
železnatý	železnatý	k2eAgInSc1d1	železnatý
FeO	FeO	k1gFnSc4	FeO
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
hmota	hmota	k1gFnSc1	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Nerozpouští	rozpouštět	k5eNaImIp3nS	rozpouštět
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
pyroforickém	pyroforický	k2eAgInSc6d1	pyroforický
stavu	stav	k1gInSc6	stav
(	(	kIx(	(
<g/>
jemně	jemně	k6eAd1	jemně
rozptýleném	rozptýlený	k2eAgNnSc6d1	rozptýlené
<g/>
)	)	kIx)	)
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vodu	voda	k1gFnSc4	voda
rozkládat	rozkládat	k5eAaImF	rozkládat
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
zvláště	zvláště	k6eAd1	zvláště
za	za	k7c2	za
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
tepelným	tepelný	k2eAgInSc7d1	tepelný
rozkladem	rozklad	k1gInSc7	rozklad
šťavelanu	šťavelan	k1gInSc2	šťavelan
železnatého	železnatý	k2eAgInSc2d1	železnatý
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
rozkladu	rozklad	k1gInSc6	rozklad
se	se	k3xPyFc4	se
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
pyroforické	pyroforický	k2eAgFnSc6d1	pyroforický
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
stálý	stálý	k2eAgInSc4d1	stálý
jen	jen	k6eAd1	jen
nad	nad	k7c7	nad
teplotou	teplota	k1gFnSc7	teplota
575	[number]	k4	575
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
touto	tento	k3xDgFnSc7	tento
teplotou	teplota	k1gFnSc7	teplota
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
železo	železo	k1gNnSc4	železo
a	a	k8xC	a
oxid	oxid	k1gInSc4	oxid
železnato-železitý	železnato-železitý	k2eAgInSc4d1	železnato-železitý
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
železnatý	železnatý	k2eAgInSc1d1	železnatý
Fe	Fe	k1gMnSc7	Fe
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
vločkovitá	vločkovitý	k2eAgFnSc1d1	vločkovitá
sraženina	sraženina	k1gFnSc1	sraženina
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
přechází	přecházet	k5eAaImIp3nS	přecházet
přes	přes	k7c4	přes
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
až	až	k9	až
do	do	k7c2	do
hněda	hnědo	k1gNnSc2	hnědo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
barevný	barevný	k2eAgInSc1d1	barevný
pochod	pochod	k1gInSc1	pochod
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
oxidací	oxidace	k1gFnSc7	oxidace
hydroxidu	hydroxid	k1gInSc2	hydroxid
železnatého	železnatý	k2eAgInSc2d1	železnatý
na	na	k7c4	na
hydrát	hydrát	k1gInSc4	hydrát
oxidu	oxid	k1gInSc2	oxid
železitého	železitý	k2eAgInSc2d1	železitý
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
železnatý	železnatý	k2eAgMnSc1d1	železnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
srážením	srážení	k1gNnSc7	srážení
železnatých	železnatý	k2eAgInPc2d1	železnatý
iontů	ion	k1gInPc2	ion
ionty	ion	k1gInPc7	ion
hydroxidovými	hydroxidový	k2eAgInPc7d1	hydroxidový
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
železnatý	železnatý	k2eAgInSc1d1	železnatý
FeS	fes	k1gNnSc4	fes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
červenavě	červenavě	k6eAd1	červenavě
hnědá	hnědý	k2eAgFnSc1d1	hnědá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
zahříváním	zahřívání	k1gNnSc7	zahřívání
železa	železo	k1gNnSc2	železo
se	s	k7c7	s
sírou	síra	k1gFnSc7	síra
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
černá	černý	k2eAgFnSc1d1	černá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
srážením	srážení	k1gNnSc7	srážení
roztoků	roztok	k1gInPc2	roztok
železnatých	železnatý	k2eAgMnPc2d1	železnatý
solí	solit	k5eAaImIp3nP	solit
roztokem	roztok	k1gInSc7	roztok
alkalického	alkalický	k2eAgInSc2d1	alkalický
sulfidu	sulfid	k1gInSc2	sulfid
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
železnatý	železnatý	k2eAgInSc1d1	železnatý
je	být	k5eAaImIp3nS	být
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc1	nerost
pyrhotin	pyrhotin	k1gInSc1	pyrhotin
<g/>
.	.	kIx.	.
</s>
<s>
Disulfid	Disulfid	k1gInSc1	Disulfid
železnatý	železnatý	k2eAgInSc1d1	železnatý
FeS	fes	k1gNnSc4	fes
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
mosazně	mosazně	k6eAd1	mosazně
žlutá	žlutý	k2eAgFnSc1d1	žlutá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
s	s	k7c7	s
kovovým	kovový	k2eAgInSc7d1	kovový
leskem	lesk	k1gInSc7	lesk
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nerozpouští	rozpouštět	k5eNaImIp3nS	rozpouštět
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc4	nerost
pyrit	pyrit	k1gInSc1	pyrit
a	a	k8xC	a
markazit	markazit	k1gInSc1	markazit
<g/>
.	.	kIx.	.
</s>
<s>
Laboratorně	laboratorně	k6eAd1	laboratorně
jej	on	k3xPp3gMnSc4	on
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
zahříváním	zahřívání	k1gNnSc7	zahřívání
práškového	práškový	k2eAgNnSc2d1	práškové
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
nadbytku	nadbytek	k1gInSc6	nadbytek
síry	síra	k1gFnSc2	síra
<g/>
.	.	kIx.	.
</s>
<s>
Fluorid	fluorid	k1gInSc4	fluorid
železnatý	železnatý	k2eAgInSc4d1	železnatý
FeF	FeF	k1gFnSc7	FeF
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
tvořit	tvořit	k5eAaImF	tvořit
komplexní	komplexní	k2eAgFnPc4d1	komplexní
sloučeniny	sloučenina	k1gFnPc4	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
fluoroželeznatany	fluoroželeznatan	k1gInPc1	fluoroželeznatan
<g/>
.	.	kIx.	.
</s>
<s>
Fluorid	fluorid	k1gInSc1	fluorid
železnatý	železnatý	k2eAgInSc1d1	železnatý
připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
fluorovodíkové	fluorovodíkový	k2eAgFnSc6d1	fluorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
železnatý	železnatý	k2eAgInSc4d1	železnatý
FeCl	FeCl	k1gInSc4	FeCl
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
světle	světle	k6eAd1	světle
žlutá	žlutý	k2eAgFnSc1d1	žlutá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
formě	forma	k1gFnSc6	forma
má	mít	k5eAaImIp3nS	mít
modrozelenou	modrozelený	k2eAgFnSc4d1	modrozelená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
lehce	lehko	k6eAd1	lehko
se	se	k3xPyFc4	se
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
na	na	k7c4	na
chlorid	chlorid	k1gInSc4	chlorid
železitý	železitý	k2eAgInSc1d1	železitý
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vytvářet	vytvářet	k5eAaImF	vytvářet
komplexní	komplexní	k2eAgFnSc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
chloroželeznatany	chloroželeznatan	k1gInPc1	chloroželeznatan
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
železnatý	železnatý	k2eAgMnSc1d1	železnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
převáděním	převádění	k1gNnSc7	převádění
par	para	k1gFnPc2	para
chlorovodíku	chlorovodík	k1gInSc2	chlorovodík
přes	přes	k7c4	přes
rozžhavené	rozžhavený	k2eAgFnPc4d1	rozžhavená
železné	železný	k2eAgFnPc4d1	železná
piliny	pilina	k1gFnPc4	pilina
<g/>
.	.	kIx.	.
</s>
<s>
Bromid	bromid	k1gInSc1	bromid
železnatý	železnatý	k2eAgInSc4d1	železnatý
FeBr	FeBr	k1gInSc4	FeBr
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
žlutozelená	žlutozelený	k2eAgFnSc1d1	žlutozelená
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
zelená	zelený	k2eAgFnSc1d1	zelená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
převáděním	převádění	k1gNnSc7	převádění
par	para	k1gFnPc2	para
bromu	brom	k1gInSc2	brom
přes	přes	k7c4	přes
rozžhavené	rozžhavený	k2eAgFnPc4d1	rozžhavená
železnaté	železnatý	k2eAgFnPc4d1	železnatá
piliny	pilina	k1gFnPc4	pilina
<g/>
.	.	kIx.	.
</s>
<s>
Jodid	jodid	k1gInSc4	jodid
železnatý	železnatý	k2eAgInSc4d1	železnatý
FeI	FeI	k1gFnSc7	FeI
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
šedá	šedat	k5eAaImIp3nS	šedat
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
hydratovaném	hydratovaný	k2eAgInSc6d1	hydratovaný
stavu	stav	k1gInSc6	stav
zelená	zelený	k2eAgFnSc1d1	zelená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
syntézou	syntéza	k1gFnSc7	syntéza
z	z	k7c2	z
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jodu	jod	k1gInSc2	jod
a	a	k8xC	a
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Rhodanid	Rhodanid	k1gInSc1	Rhodanid
železnatý	železnatý	k2eAgInSc1d1	železnatý
neboli	neboli	k8xC	neboli
thiokyanatan	thiokyanatan	k1gInSc1	thiokyanatan
železnatý	železnatý	k2eAgInSc1d1	železnatý
Fe	Fe	k1gMnSc7	Fe
<g/>
(	(	kIx(	(
<g/>
SCN	SCN	kA	SCN
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
zelená	zelený	k2eAgFnSc1d1	zelená
krystalické	krystalický	k2eAgFnPc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
tvořit	tvořit	k5eAaImF	tvořit
komplexní	komplexní	k2eAgFnPc4d1	komplexní
sloučeniny	sloučenina	k1gFnPc4	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
rhodanoželeznatany	rhodanoželeznatan	k1gInPc1	rhodanoželeznatan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
na	na	k7c4	na
rhodanid	rhodanid	k1gInSc4	rhodanid
železitý	železitý	k2eAgInSc4d1	železitý
<g/>
.	.	kIx.	.
</s>
<s>
Rhodanid	Rhodanid	k1gInSc1	Rhodanid
železnatý	železnatý	k2eAgMnSc1d1	železnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
rhodanovodíkové	rhodanovodíková	k1gFnSc2	rhodanovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc1	dusičnan
železnatý	železnatý	k2eAgInSc1d1	železnatý
Fe	Fe	k1gMnSc7	Fe
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
formě	forma	k1gFnSc6	forma
zelená	zelený	k2eAgFnSc1d1	zelená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
je	být	k5eAaImIp3nS	být
stálý	stálý	k2eAgInSc1d1	stálý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
vyšší	vysoký	k2eAgFnPc4d2	vyšší
teploty	teplota	k1gFnPc4	teplota
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
na	na	k7c4	na
dusičnan	dusičnan	k1gInSc4	dusičnan
železitý	železitý	k2eAgInSc4d1	železitý
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc1	dusičnan
železnatý	železnatý	k2eAgMnSc1d1	železnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
železa	železo	k1gNnSc2	železo
ve	v	k7c6	v
studené	studený	k2eAgFnSc6d1	studená
zředěné	zředěný	k2eAgFnSc6d1	zředěná
kyselině	kyselina	k1gFnSc6	kyselina
dusičné	dusičný	k2eAgFnSc6d1	dusičná
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc4	síran
železnatý	železnatý	k2eAgInSc4d1	železnatý
FeSO	FeSO	k1gFnSc7	FeSO
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgFnSc1d3	nejznámější
jako	jako	k8xS	jako
FeSO	FeSO	k1gFnSc1	FeSO
<g/>
4	[number]	k4	4
<g/>
·	·	k?	·
7	[number]	k4	7
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
triviálně	triviálně	k6eAd1	triviálně
zelená	zelený	k2eAgFnSc1d1	zelená
skalice	skalice	k1gFnSc1	skalice
<g/>
,	,	kIx,	,
zelená	zelený	k2eAgFnSc1d1	zelená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
<g/>
,	,	kIx,	,
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
a	a	k8xC	a
za	za	k7c4	za
vyšší	vysoký	k2eAgFnPc4d2	vyšší
teploty	teplota	k1gFnPc4	teplota
probíhá	probíhat	k5eAaImIp3nS	probíhat
oxidace	oxidace	k1gFnSc1	oxidace
na	na	k7c4	na
síran	síran	k1gInSc4	síran
železitý	železitý	k2eAgInSc4d1	železitý
rychleji	rychle	k6eAd2	rychle
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc1	síran
železnatý	železnatý	k2eAgMnSc1d1	železnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
železa	železo	k1gNnSc2	železo
ve	v	k7c6	v
zředěné	zředěný	k2eAgFnSc6d1	zředěná
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
inkoustu	inkoust	k1gInSc2	inkoust
<g/>
,	,	kIx,	,
berlínské	berlínský	k2eAgFnSc2d1	Berlínská
modři	modř	k1gFnSc2	modř
<g/>
,	,	kIx,	,
v	v	k7c6	v
barvířství	barvířství	k1gNnSc6	barvířství
(	(	kIx(	(
<g/>
barvení	barvení	k1gNnSc1	barvení
vlny	vlna	k1gFnSc2	vlna
na	na	k7c4	na
černo	černo	k1gNnSc4	černo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
konzervování	konzervování	k1gNnSc3	konzervování
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
k	k	k7c3	k
ničení	ničení	k1gNnSc3	ničení
plevele	plevel	k1gInSc2	plevel
a	a	k8xC	a
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
některých	některý	k3yIgInPc2	některý
léků	lék	k1gInPc2	lék
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
dostatečného	dostatečný	k2eAgInSc2d1	dostatečný
příjmu	příjem	k1gInSc2	příjem
železa	železo	k1gNnSc2	železo
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Mohrova	Mohrův	k2eAgFnSc1d1	Mohrova
sůl	sůl	k1gFnSc1	sůl
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
Fe	Fe	k1gFnPc2	Fe
<g/>
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
·	·	k?	·
<g/>
6	[number]	k4	6
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
je	být	k5eAaImIp3nS	být
triviální	triviální	k2eAgInSc4d1	triviální
název	název	k1gInSc4	název
pro	pro	k7c4	pro
hexahydrát	hexahydrát	k1gInSc4	hexahydrát
síranu	síran	k1gInSc2	síran
amonnoželeznatého	amonnoželeznatý	k2eAgInSc2d1	amonnoželeznatý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
podvojná	podvojný	k2eAgFnSc1d1	podvojná
sloučenina	sloučenina	k1gFnSc1	sloučenina
síranu	síran	k1gInSc2	síran
železnatého	železnatý	k2eAgInSc2d1	železnatý
a	a	k8xC	a
síranu	síran	k1gInSc2	síran
amonného	amonný	k2eAgInSc2d1	amonný
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	s	k7c7	s
krystalizací	krystalizace	k1gFnSc7	krystalizace
směsného	směsný	k2eAgInSc2d1	směsný
roztoku	roztok	k1gInSc2	roztok
síranu	síran	k1gInSc2	síran
železnatého	železnatý	k2eAgInSc2d1	železnatý
a	a	k8xC	a
síranu	síran	k1gInSc2	síran
amonného	amonný	k2eAgInSc2d1	amonný
<g/>
.	.	kIx.	.
</s>
<s>
Uhličitan	uhličitan	k1gInSc4	uhličitan
železnatý	železnatý	k2eAgInSc4d1	železnatý
FeCO	FeCO	k1gFnSc7	FeCO
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
na	na	k7c4	na
uhličitan	uhličitan	k1gInSc4	uhličitan
železitý	železitý	k2eAgInSc1d1	železitý
a	a	k8xC	a
oxid	oxid	k1gInSc1	oxid
železitý	železitý	k2eAgInSc1d1	železitý
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
změnou	změna	k1gFnSc7	změna
barvy	barva	k1gFnSc2	barva
z	z	k7c2	z
bílé	bílý	k2eAgFnSc2d1	bílá
na	na	k7c4	na
hnědou	hnědý	k2eAgFnSc4d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Uhličitan	uhličitan	k1gInSc1	uhličitan
železnatý	železnatý	k2eAgMnSc1d1	železnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
srážením	srážení	k1gNnSc7	srážení
roztoků	roztok	k1gInPc2	roztok
železnatých	železnatý	k2eAgMnPc2d1	železnatý
solí	solit	k5eAaImIp3nP	solit
roztokem	roztok	k1gInSc7	roztok
alkalického	alkalický	k2eAgInSc2d1	alkalický
uhličitanu	uhličitan	k1gInSc2	uhličitan
<g/>
.	.	kIx.	.
</s>
<s>
Kyanid	kyanid	k1gInSc1	kyanid
železnatý	železnatý	k2eAgInSc1d1	železnatý
nebyl	být	k5eNaImAgInS	být
doposud	doposud	k6eAd1	doposud
připraven	připravit	k5eAaPmNgInS	připravit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
jeho	jeho	k3xOp3gFnPc1	jeho
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
kyanoželeznatany	kyanoželeznatan	k1gInPc1	kyanoželeznatan
<g/>
.	.	kIx.	.
</s>
<s>
Železité	železitý	k2eAgNnSc1d1	železité
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
oxidací	oxidace	k1gFnPc2	oxidace
železnatých	železnatý	k2eAgFnPc2d1	železnatá
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
jsou	být	k5eAaImIp3nP	být
rozpustné	rozpustný	k2eAgFnPc1d1	rozpustná
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
soli	sůl	k1gFnPc4	sůl
železnaté	železnatý	k2eAgFnPc4d1	železnatá
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jejich	jejich	k3xOp3gInPc1	jejich
roztoky	roztok	k1gInPc1	roztok
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
přebytečnou	přebytečný	k2eAgFnSc4d1	přebytečná
kyselinu	kyselina	k1gFnSc4	kyselina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
roztok	roztok	k1gInSc1	roztok
hnědožlutý	hnědožlutý	k2eAgInSc1d1	hnědožlutý
až	až	k6eAd1	až
tmavohnědý	tmavohnědý	k2eAgInSc1d1	tmavohnědý
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
hydrolýzy	hydrolýza	k1gFnSc2	hydrolýza
železitých	železitý	k2eAgFnPc2d1	železitá
solí	sůl	k1gFnPc2	sůl
na	na	k7c4	na
hydroxid	hydroxid	k1gInSc4	hydroxid
železitý	železitý	k2eAgInSc4d1	železitý
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
vznikem	vznik	k1gInSc7	vznik
zásaditých	zásaditý	k2eAgFnPc2d1	zásaditá
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
budeme	být	k5eAaImBp1nP	být
chtít	chtít	k5eAaImF	chtít
hydrolýze	hydrolýza	k1gFnSc6	hydrolýza
zabránit	zabránit	k5eAaPmF	zabránit
přidáním	přidání	k1gNnSc7	přidání
nadbytečné	nadbytečný	k2eAgFnSc2d1	nadbytečná
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
získáme	získat	k5eAaPmIp1nP	získat
acidokomplexní	acidokomplexní	k2eAgFnPc1d1	acidokomplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgInPc1	takovýto
roztoky	roztok	k1gInPc1	roztok
mají	mít	k5eAaImIp3nP	mít
potom	potom	k6eAd1	potom
narůžovělou	narůžovělý	k2eAgFnSc4d1	narůžovělá
nebo	nebo	k8xC	nebo
žlutou	žlutý	k2eAgFnSc4d1	žlutá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
bezbarvé	bezbarvý	k2eAgFnPc1d1	bezbarvá
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
velmi	velmi	k6eAd1	velmi
čisté	čistý	k2eAgFnPc4d1	čistá
železité	železitý	k2eAgFnPc4d1	železitá
sloučeniny	sloučenina	k1gFnPc4	sloučenina
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
upravit	upravit	k5eAaPmF	upravit
pH	ph	kA	ph
roztoku	roztok	k1gInSc3	roztok
téměř	téměř	k6eAd1	téměř
na	na	k7c4	na
0	[number]	k4	0
<g/>
,	,	kIx,	,
při	při	k7c6	při
vyšším	vysoký	k2eAgNnSc6d2	vyšší
pH	ph	kA	ph
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
koloidní	koloidní	k2eAgInPc4d1	koloidní
gely	gel	k1gInPc4	gel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
červenohnědé	červenohnědý	k2eAgFnSc2d1	červenohnědá
sraženiny	sraženina	k1gFnSc2	sraženina
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
redukčních	redukční	k2eAgNnPc2d1	redukční
činidel	činidlo	k1gNnPc2	činidlo
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
lehce	lehko	k6eAd1	lehko
ze	z	k7c2	z
solí	sůl	k1gFnPc2	sůl
železitých	železitý	k2eAgMnPc2d1	železitý
připraví	připravit	k5eAaPmIp3nS	připravit
soli	sůl	k1gFnPc4	sůl
železnaté	železnatý	k2eAgFnPc4d1	železnatá
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc4	oxid
železitý	železitý	k2eAgInSc4d1	železitý
Fe	Fe	k1gFnSc7	Fe
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
červenohnědý	červenohnědý	k2eAgInSc1d1	červenohnědý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc4	nerost
hematit	hematit	k1gInSc1	hematit
a	a	k8xC	a
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
jako	jako	k9	jako
nerost	nerost	k1gInSc4	nerost
limonit	limonit	k1gInSc1	limonit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
žíháním	žíhání	k1gNnSc7	žíhání
hydroxidu	hydroxid	k1gInSc2	hydroxid
železitého	železitý	k2eAgInSc2d1	železitý
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
železitý	železitý	k2eAgInSc1d1	železitý
Fe	Fe	k1gMnSc7	Fe
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
červenohnědý	červenohnědý	k2eAgInSc1d1	červenohnědý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc4	nerost
goethit	goethit	k1gInSc1	goethit
a	a	k8xC	a
lepidokrokit	lepidokrokit	k1gInSc1	lepidokrokit
<g/>
.	.	kIx.	.
</s>
<s>
Čerstvě	čerstvě	k6eAd1	čerstvě
sražený	sražený	k2eAgInSc1d1	sražený
hydroxid	hydroxid	k1gInSc1	hydroxid
železitý	železitý	k2eAgInSc1d1	železitý
je	být	k5eAaImIp3nS	být
amfoterní	amfoterní	k2eAgInSc1d1	amfoterní
a	a	k8xC	a
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
se	se	k3xPyFc4	se
v	v	k7c6	v
kyselinách	kyselina	k1gFnPc6	kyselina
na	na	k7c6	na
železité	železitý	k2eAgFnSc6d1	železitá
soli	sůl	k1gFnSc6	sůl
i	i	k8xC	i
hydroxidech	hydroxid	k1gInPc6	hydroxid
na	na	k7c4	na
železitany	železitan	k1gInPc4	železitan
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
železitý	železitý	k2eAgInSc1d1	železitý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
srážením	srážení	k1gNnSc7	srážení
roztoků	roztok	k1gInPc2	roztok
železitých	železitý	k2eAgMnPc2d1	železitý
solí	solit	k5eAaImIp3nP	solit
roztokem	roztok	k1gInSc7	roztok
alkalického	alkalický	k2eAgInSc2d1	alkalický
hydroxidu	hydroxid	k1gInSc2	hydroxid
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc4	sulfid
železitý	železitý	k2eAgInSc4d1	železitý
Fe	Fe	k1gFnSc7	Fe
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
nerostu	nerost	k1gInSc2	nerost
bornitu	bornit	k1gInSc2	bornit
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
železitý	železitý	k2eAgInSc1d1	železitý
připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
srážením	srážení	k1gNnSc7	srážení
roztoků	roztok	k1gInPc2	roztok
železitých	železitý	k2eAgMnPc2d1	železitý
solí	solit	k5eAaImIp3nP	solit
roztokem	roztok	k1gInSc7	roztok
alkalického	alkalický	k2eAgInSc2d1	alkalický
sulfidu	sulfid	k1gInSc2	sulfid
<g/>
.	.	kIx.	.
</s>
<s>
Diželezitan	Diželezitan	k1gInSc4	Diželezitan
železnatý	železnatý	k2eAgInSc4d1	železnatý
neboli	neboli	k8xC	neboli
oxid	oxid	k1gInSc4	oxid
železnato-železitý	železnato-železitý	k2eAgInSc4d1	železnato-železitý
Fe	Fe	k1gFnSc7	Fe
<g/>
3	[number]	k4	3
<g/>
O	O	kA	O
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgInSc1d1	černý
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
hydroxidech	hydroxid	k1gInPc6	hydroxid
a	a	k8xC	a
kyselinách	kyselina	k1gFnPc6	kyselina
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
<g/>
,	,	kIx,	,
ferromagnatický	ferromagnatický	k2eAgInSc1d1	ferromagnatický
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
magnetit	magnetit	k1gInSc1	magnetit
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
oxidů	oxid	k1gInPc2	oxid
železa	železo	k1gNnSc2	železo
z	z	k7c2	z
praktického	praktický	k2eAgNnSc2d1	praktické
hlediska	hledisko	k1gNnSc2	hledisko
největší	veliký	k2eAgInSc4d3	veliký
význam	význam	k1gInSc4	význam
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
pigmentů	pigment	k1gInPc2	pigment
červené	červený	k2eAgFnSc2d1	červená
až	až	k8xS	až
hnědé	hnědý	k2eAgFnSc2d1	hnědá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
jej	on	k3xPp3gNnSc2	on
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
působením	působení	k1gNnSc7	působení
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
na	na	k7c4	na
rozžhavený	rozžhavený	k2eAgInSc4d1	rozžhavený
prášek	prášek	k1gInSc4	prášek
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Ferity	ferit	k1gInPc1	ferit
jsou	být	k5eAaImIp3nP	být
pevné	pevný	k2eAgFnPc4d1	pevná
<g/>
,	,	kIx,	,
nerozpustné	rozpustný	k2eNgFnPc4d1	nerozpustná
<g/>
,	,	kIx,	,
práškovité	práškovitý	k2eAgFnPc4d1	práškovitá
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
nereagující	reagující	k2eNgFnPc4d1	nereagující
s	s	k7c7	s
kyselinami	kyselina	k1gFnPc7	kyselina
ani	ani	k8xC	ani
hydroxidy	hydroxid	k1gInPc7	hydroxid
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
obecný	obecný	k2eAgInSc4d1	obecný
vzorec	vzorec	k1gInSc4	vzorec
M	M	kA	M
<g/>
2	[number]	k4	2
<g/>
IFe	IFe	k1gFnSc2	IFe
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
a	a	k8xC	a
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
systematického	systematický	k2eAgNnSc2d1	systematické
názvosloví	názvosloví	k1gNnSc2	názvosloví
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
diželezitany	diželezitan	k1gInPc4	diželezitan
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
písmeno	písmeno	k1gNnSc4	písmeno
M	M	kA	M
v	v	k7c6	v
obecném	obecný	k2eAgInSc6d1	obecný
vzorci	vzorec	k1gInSc6	vzorec
lze	lze	k6eAd1	lze
dosadtit	dosadtit	k5eAaPmF	dosadtit
libovolný	libovolný	k2eAgInSc4d1	libovolný
kov	kov	k1gInSc4	kov
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dosadil	dosadit	k5eAaPmAgInS	dosadit
trojmocný	trojmocný	k2eAgInSc1d1	trojmocný
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
látka	látka	k1gFnSc1	látka
s	s	k7c7	s
antiferromagnetismem	antiferromagnetismus	k1gInSc7	antiferromagnetismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ferimagnetismus	ferimagnetismus	k1gInSc1	ferimagnetismus
<g/>
.	.	kIx.	.
</s>
<s>
Ferity	ferit	k1gInPc1	ferit
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
magnetických	magnetický	k2eAgNnPc2d1	magnetické
jader	jádro	k1gNnPc2	jádro
pro	pro	k7c4	pro
vysokofrekvenční	vysokofrekvenční	k2eAgInPc4d1	vysokofrekvenční
transformátory	transformátor	k1gInPc4	transformátor
<g/>
.	.	kIx.	.
</s>
<s>
Ferity	ferit	k1gInPc1	ferit
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
hexagonální	hexagonální	k2eAgInSc4d1	hexagonální
např.	např.	kA	např.
BaFe	BaFe	k?	BaFe
<g/>
12	[number]	k4	12
<g/>
O	o	k7c4	o
<g/>
19	[number]	k4	19
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
ferimagnetické	ferimagnetický	k2eAgInPc1d1	ferimagnetický
a	a	k8xC	a
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
permanentní	permanentní	k2eAgMnPc1d1	permanentní
magnety	magnet	k1gInPc4	magnet
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc7	třetí
z	z	k7c2	z
ferimagnetických	ferimagnetický	k2eAgFnPc2d1	ferimagnetický
sloučenin	sloučenina	k1gFnPc2	sloučenina
jsou	být	k5eAaImIp3nP	být
granáty	granát	k1gInPc4	granát
s	s	k7c7	s
obecným	obecný	k2eAgInSc7d1	obecný
vzorcem	vzorec	k1gInSc7	vzorec
M	M	kA	M
<g/>
3	[number]	k4	3
<g/>
IIIFe	IIIF	k1gInSc2	IIIF
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnPc1d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
granát	granát	k1gInSc4	granát
yttrium-železo	yttrium-železa	k1gFnSc5	yttrium-železa
používaný	používaný	k2eAgInSc4d1	používaný
jako	jako	k8xC	jako
mikrovlnný	mikrovlnný	k2eAgInSc4d1	mikrovlnný
filtr	filtr	k1gInSc4	filtr
a	a	k8xC	a
v	v	k7c6	v
radarech	radar	k1gInPc6	radar
<g/>
.	.	kIx.	.
</s>
<s>
Fluorid	fluorid	k1gInSc1	fluorid
železitý	železitý	k2eAgInSc1d1	železitý
FeF	FeF	k1gFnSc2	FeF
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
nazelenalá	nazelenalý	k2eAgFnSc1d1	nazelenalá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
a	a	k8xC	a
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bledě	bledě	k6eAd1	bledě
růžová	růžový	k2eAgFnSc1d1	růžová
až	až	k9	až
světle	světle	k6eAd1	světle
žlutá	žlutý	k2eAgFnSc1d1	žlutá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
špatně	špatně	k6eAd1	špatně
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
tvoří	tvořit	k5eAaImIp3nP	tvořit
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
fluoroželezitany	fluoroželezitan	k1gInPc1	fluoroželezitan
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
hydroxidu	hydroxid	k1gInSc2	hydroxid
železitého	železitý	k2eAgInSc2d1	železitý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
fluorovodíkové	fluorovodíkový	k2eAgFnSc6d1	fluorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
železitý	železitý	k2eAgInSc1d1	železitý
FeCl	FeCl	k1gInSc1	FeCl
<g/>
3	[number]	k4	3
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
černohnědá	černohnědý	k2eAgFnSc1d1	černohnědá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
žlutohnědá	žlutohnědý	k2eAgFnSc1d1	žlutohnědá
hygroskopická	hygroskopický	k2eAgFnSc1d1	hygroskopická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
působením	působení	k1gNnSc7	působení
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
vlhkosti	vlhkost	k1gFnSc2	vlhkost
samovolně	samovolně	k6eAd1	samovolně
roztéká	roztékat	k5eAaImIp3nS	roztékat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plynné	plynný	k2eAgFnSc6d1	plynná
fázi	fáze	k1gFnSc6	fáze
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
dimer	dimer	k1gInSc1	dimer
Fe	Fe	k1gFnSc2	Fe
<g/>
2	[number]	k4	2
<g/>
Cl	Cl	k1gMnPc2	Cl
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
tvoří	tvořit	k5eAaImIp3nP	tvořit
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
sloučeniny	sloučenina	k1gFnPc1	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
chloroželezitany	chloroželezitan	k1gInPc1	chloroželezitan
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
působením	působení	k1gNnSc7	působení
chloru	chlor	k1gInSc2	chlor
na	na	k7c4	na
rozžhavené	rozžhavený	k2eAgFnPc4d1	rozžhavená
železné	železný	k2eAgFnPc4d1	železná
piliny	pilina	k1gFnPc4	pilina
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
železitý	železitý	k2eAgInSc1d1	železitý
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
elektrotechnice	elektrotechnika	k1gFnSc6	elektrotechnika
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
plošných	plošný	k2eAgInPc2d1	plošný
spojů	spoj	k1gInPc2	spoj
jako	jako	k8xS	jako
leptadlo	leptadlo	k1gNnSc4	leptadlo
<g/>
,	,	kIx,	,
rozpouštějící	rozpouštějící	k2eAgFnSc4d1	rozpouštějící
nechráněnou	chráněný	k2eNgFnSc4d1	nechráněná
vrstvu	vrstva	k1gFnSc4	vrstva
kovové	kovový	k2eAgFnSc2d1	kovová
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jistých	jistý	k2eAgFnPc2d1	jistá
podmínek	podmínka	k1gFnPc2	podmínka
jsou	být	k5eAaImIp3nP	být
kyselé	kyselý	k2eAgInPc1d1	kyselý
roztoky	roztok	k1gInPc1	roztok
chloridu	chlorid	k1gInSc2	chlorid
železitého	železitý	k2eAgInSc2d1	železitý
schopny	schopen	k2eAgFnPc1d1	schopna
rozpouštět	rozpouštět	k5eAaImF	rozpouštět
i	i	k9	i
kovové	kovový	k2eAgNnSc4d1	kovové
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
jako	jako	k9	jako
vločkovací	vločkovací	k2eAgFnSc1d1	vločkovací
přísada	přísada	k1gFnSc1	přísada
při	při	k7c6	při
čištění	čištění	k1gNnSc6	čištění
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Bromid	bromid	k1gInSc1	bromid
železitý	železitý	k2eAgInSc1d1	železitý
FeBr	FeBr	k1gInSc1	FeBr
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
hnědočervená	hnědočervený	k2eAgFnSc1d1	hnědočervená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
a	a	k8xC	a
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
je	být	k5eAaImIp3nS	být
tmavě	tmavě	k6eAd1	tmavě
zelená	zelený	k2eAgFnSc1d1	zelená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
svými	svůj	k3xOyFgFnPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
chloridu	chlorid	k1gInSc2	chlorid
železitému	železitý	k2eAgInSc3d1	železitý
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
bromu	brom	k1gInSc2	brom
s	s	k7c7	s
rozžhavenými	rozžhavený	k2eAgFnPc7d1	rozžhavená
železnými	železný	k2eAgFnPc7d1	železná
pilinami	pilina	k1gFnPc7	pilina
<g/>
.	.	kIx.	.
</s>
<s>
Jodid	jodid	k1gInSc1	jodid
železitý	železitý	k2eAgInSc1d1	železitý
FeI	FeI	k1gFnSc2	FeI
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nelze	lze	k6eNd1	lze
získat	získat	k5eAaPmF	získat
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
dokáže	dokázat	k5eAaPmIp3nS	dokázat
existovat	existovat	k5eAaImF	existovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
jodidu	jodid	k1gInSc2	jodid
železnatého	železnatý	k2eAgInSc2d1	železnatý
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
železité	železitý	k2eAgFnSc2d1	železitá
soli	sůl	k1gFnSc2	sůl
s	s	k7c7	s
jodidovými	jodidový	k2eAgInPc7d1	jodidový
aniony	anion	k1gInPc7	anion
totiž	totiž	k9	totiž
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
oxidaci	oxidace	k1gFnSc3	oxidace
jodidu	jodid	k1gInSc2	jodid
na	na	k7c4	na
jod	jod	k1gInSc4	jod
a	a	k8xC	a
redukci	redukce	k1gFnSc4	redukce
železitého	železitý	k2eAgInSc2d1	železitý
kationu	kation	k1gInSc2	kation
na	na	k7c4	na
železnatý	železnatý	k2eAgInSc4d1	železnatý
<g/>
.	.	kIx.	.
</s>
<s>
Chloristan	chloristan	k1gInSc1	chloristan
železitý	železitý	k2eAgInSc1d1	železitý
Fe	Fe	k1gFnSc3	Fe
<g/>
(	(	kIx(	(
<g/>
ClO	clo	k1gNnSc4	clo
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
slabě	slabě	k6eAd1	slabě
narůžovělá	narůžovělý	k2eAgFnSc1d1	narůžovělá
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
hygroskopická	hygroskopický	k2eAgFnSc1d1	hygroskopická
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Chloristan	chloristan	k1gInSc1	chloristan
železitý	železitý	k2eAgInSc1d1	železitý
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
hydrolyzuje	hydrolyzovat	k5eAaBmIp3nS	hydrolyzovat
přičemž	přičemž	k6eAd1	přičemž
často	často	k6eAd1	často
polymeruje	polymerovat	k5eAaBmIp3nS	polymerovat
<g/>
.	.	kIx.	.
</s>
<s>
Chloristan	chloristan	k1gInSc1	chloristan
železitý	železitý	k2eAgInSc1d1	železitý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
odkouřením	odkouření	k1gNnSc7	odkouření
chloridu	chlorid	k1gInSc2	chlorid
železitého	železitý	k2eAgMnSc4d1	železitý
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
chloristou	chloristý	k2eAgFnSc7d1	chloristá
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc1	dusičnan
železitý	železitý	k2eAgInSc1d1	železitý
Fe	Fe	k1gMnSc7	Fe
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
však	však	k9	však
hydrolyzuje	hydrolyzovat	k5eAaBmIp3nS	hydrolyzovat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
hnědého	hnědý	k2eAgInSc2d1	hnědý
roztoku	roztok	k1gInSc2	roztok
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
lze	lze	k6eAd1	lze
zamezit	zamezit	k5eAaPmF	zamezit
přidáním	přidání	k1gNnSc7	přidání
nadbytečné	nadbytečný	k2eAgFnSc2d1	nadbytečná
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgFnSc2d1	dusičná
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc1	dusičnan
železitý	železitý	k2eAgInSc1d1	železitý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
%	%	kIx~	%
kyselině	kyselina	k1gFnSc6	kyselina
dusičné	dusičný	k2eAgFnSc6d1	dusičná
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc4	síran
železitý	železitý	k2eAgInSc4d1	železitý
Fe	Fe	k1gFnSc7	Fe
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stav	stav	k1gInSc1	stav
bílý	bílý	k2eAgInSc4d1	bílý
prášek	prášek	k1gInSc4	prášek
a	a	k8xC	a
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
hydratované	hydratovaný	k2eAgFnSc2d1	hydratovaná
soli	sůl	k1gFnSc2	sůl
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc4	nerost
coquimbit	coquimbit	k5eAaPmF	coquimbit
a	a	k8xC	a
quenstedtit	quenstedtit	k5eAaPmF	quenstedtit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
snadno	snadno	k6eAd1	snadno
podvojné	podvojný	k2eAgFnPc4d1	podvojná
soli	sůl	k1gFnPc4	sůl
kamence	kamenec	k1gInPc4	kamenec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
bezbarvé	bezbarvý	k2eAgFnPc1d1	bezbarvá
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
mořidlo	mořidlo	k1gNnSc1	mořidlo
a	a	k8xC	a
v	v	k7c6	v
barvířství	barvířství	k1gNnSc6	barvířství
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
kamenec	kamenec	k1gInSc1	kamenec
železito-amonný	železitomonný	k2eAgInSc1d1	železito-amonný
a	a	k8xC	a
kamenec	kamenec	k1gInSc1	kamenec
železito-draselný	železitoraselný	k2eAgInSc1d1	železito-draselný
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc1	síran
železitý	železitý	k2eAgInSc1d1	železitý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
oxidací	oxidace	k1gFnSc7	oxidace
síranu	síran	k1gInSc2	síran
železnatého	železnatý	k2eAgInSc2d1	železnatý
nebo	nebo	k8xC	nebo
rozpouštění	rozpouštění	k1gNnSc2	rozpouštění
oxidu	oxid	k1gInSc2	oxid
železitého	železitý	k2eAgInSc2d1	železitý
v	v	k7c6	v
koncentrované	koncentrovaný	k2eAgFnSc6d1	koncentrovaná
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc6d1	sírová
<g/>
.	.	kIx.	.
</s>
<s>
Fosforečnan	fosforečnan	k1gInSc1	fosforečnan
železitý	železitý	k2eAgInSc1d1	železitý
FePO	FePO	k1gFnSc2	FePO
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
nažloutlá	nažloutlý	k2eAgFnSc1d1	nažloutlá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztocích	roztok	k1gInPc6	roztok
snadno	snadno	k6eAd1	snadno
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
komplexní	komplexní	k2eAgFnPc4d1	komplexní
sloučeniny	sloučenina	k1gFnPc4	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
fosfatoželezitany	fosfatoželezitan	k1gInPc1	fosfatoželezitan
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
rozpustné	rozpustný	k2eAgInPc1d1	rozpustný
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Fosforečnan	fosforečnan	k1gInSc1	fosforečnan
železitý	železitý	k2eAgInSc1d1	železitý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
srážením	srážení	k1gNnSc7	srážení
roztoků	roztok	k1gInPc2	roztok
železité	železitý	k2eAgFnSc2d1	železitá
soli	sůl	k1gFnSc2	sůl
roztokem	roztok	k1gInSc7	roztok
alkalického	alkalický	k2eAgInSc2d1	alkalický
fosforečnanu	fosforečnan	k1gInSc2	fosforečnan
<g/>
.	.	kIx.	.
</s>
<s>
Thiokyanatan	Thiokyanatan	k1gInSc1	Thiokyanatan
železitý	železitý	k2eAgInSc1d1	železitý
Fe	Fe	k1gMnSc7	Fe
<g/>
(	(	kIx(	(
<g/>
SCN	SCN	kA	SCN
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
krvavě	krvavě	k6eAd1	krvavě
červená	červený	k2eAgFnSc1d1	červená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
snadno	snadno	k6eAd1	snadno
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
komplexní	komplexní	k2eAgFnPc4d1	komplexní
sloučeniny	sloučenina	k1gFnPc4	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
rhodanoželezitany	rhodanoželezitan	k1gInPc1	rhodanoželezitan
<g/>
.	.	kIx.	.
</s>
<s>
Thiokyanatan	Thiokyanatan	k1gInSc1	Thiokyanatan
železitý	železitý	k2eAgInSc1d1	železitý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
reakcí	reakce	k1gFnSc7	reakce
železité	železitý	k2eAgFnSc2d1	železitá
soli	sůl	k1gFnSc2	sůl
s	s	k7c7	s
thiokyanatanovými	thiokyanatanový	k2eAgInPc7d1	thiokyanatanový
aniony	anion	k1gInPc7	anion
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
vytřepáním	vytřepání	k1gNnSc7	vytřepání
v	v	k7c6	v
etheru	ether	k1gInSc6	ether
<g/>
.	.	kIx.	.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
železičité	železičitý	k2eAgFnPc1d1	železičitý
nejsou	být	k5eNaImIp3nP	být
pro	pro	k7c4	pro
železo	železo	k1gNnSc4	železo
vůbec	vůbec	k9	vůbec
typické	typický	k2eAgNnSc1d1	typické
a	a	k8xC	a
běžný	běžný	k2eAgMnSc1d1	běžný
člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
nesetká	setkat	k5eNaPmIp3nS	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Železičité	Železičitý	k2eAgFnPc1d1	Železičitý
sloučeniny	sloučenina	k1gFnPc1	sloučenina
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
železičitany	železičitan	k1gInPc4	železičitan
M	M	kA	M
<g/>
4	[number]	k4	4
<g/>
IFeO	IFeO	k1gFnSc1	IFeO
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
černý	černý	k2eAgInSc4d1	černý
krystalický	krystalický	k2eAgInSc4d1	krystalický
prášek	prášek	k1gInSc4	prášek
<g/>
.	.	kIx.	.
</s>
<s>
Získávají	získávat	k5eAaImIp3nP	získávat
se	se	k3xPyFc4	se
redukcí	redukce	k1gFnSc7	redukce
železanů	železan	k1gInPc2	železan
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
koncentrovaného	koncentrovaný	k2eAgInSc2d1	koncentrovaný
hydroxidu	hydroxid	k1gInSc2	hydroxid
nebo	nebo	k8xC	nebo
například	například	k6eAd1	například
zahříváním	zahřívání	k1gNnSc7	zahřívání
směsi	směs	k1gFnSc2	směs
hydroxidu	hydroxid	k1gInSc2	hydroxid
s	s	k7c7	s
hexahydroxoželezitanem	hexahydroxoželezitan	k1gInSc7	hexahydroxoželezitan
v	v	k7c6	v
proudu	proud	k1gInSc6	proud
kyslíku	kyslík	k1gInSc2	kyslík
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
500	[number]	k4	500
<g/>
-	-	kIx~	-
<g/>
800	[number]	k4	800
°	°	k?	°
<g/>
C.	C.	kA	C.
Sloučeniny	sloučenina	k1gFnSc2	sloučenina
železové	železový	k2eAgFnSc2d1	Železová
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupen	k2eAgMnPc4d1	zastoupen
železany	železan	k1gMnPc4	železan
FeO	FeO	k1gFnSc2	FeO
<g/>
42	[number]	k4	42
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
jsou	být	k5eAaImIp3nP	být
relativně	relativně	k6eAd1	relativně
stálé	stálý	k2eAgFnPc1d1	stálá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
vodném	vodné	k1gNnSc6	vodné
roztoku	roztok	k1gInSc2	roztok
v	v	k7c6	v
neutrálním	neutrální	k2eAgNnSc6d1	neutrální
a	a	k8xC	a
zejména	zejména	k6eAd1	zejména
kyselém	kyselý	k2eAgNnSc6d1	kyselé
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
rozpadají	rozpadat	k5eAaImIp3nP	rozpadat
<g/>
.	.	kIx.	.
</s>
<s>
Připravují	připravovat	k5eAaImIp3nP	připravovat
se	s	k7c7	s
zahříváním	zahřívání	k1gNnSc7	zahřívání
železných	železný	k2eAgFnPc2d1	železná
pilin	pilina	k1gFnPc2	pilina
s	s	k7c7	s
dusičnanem	dusičnan	k1gInSc7	dusičnan
nebo	nebo	k8xC	nebo
oxidací	oxidace	k1gFnSc7	oxidace
čerstvě	čerstvě	k6eAd1	čerstvě
sraženého	sražený	k2eAgInSc2d1	sražený
hydroxidu	hydroxid	k1gInSc2	hydroxid
železitého	železitý	k2eAgInSc2d1	železitý
chlorem	chlor	k1gInSc7	chlor
nebo	nebo	k8xC	nebo
bromem	brom	k1gInSc7	brom
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
hydroxidu	hydroxid	k1gInSc2	hydroxid
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vychladnutí	vychladnutí	k1gNnSc6	vychladnutí
se	se	k3xPyFc4	se
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
hmota	hmota	k1gFnSc1	hmota
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
na	na	k7c4	na
jasně	jasně	k6eAd1	jasně
červenofialový	červenofialový	k2eAgInSc4d1	červenofialový
roztok	roztok	k1gInSc4	roztok
a	a	k8xC	a
s	s	k7c7	s
chloridem	chlorid	k1gInSc7	chlorid
barnatým	barnatý	k2eAgInSc7d1	barnatý
dává	dávat	k5eAaImIp3nS	dávat
sraženinu	sraženina	k1gFnSc4	sraženina
železanu	železan	k1gInSc2	železan
barnatého	barnatý	k2eAgInSc2d1	barnatý
<g/>
.	.	kIx.	.
</s>
<s>
Železany	Železana	k1gFnPc1	Železana
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgNnPc1d1	silné
oxidační	oxidační	k2eAgNnPc1d1	oxidační
činidla	činidlo	k1gNnPc1	činidlo
(	(	kIx(	(
<g/>
ještě	ještě	k9	ještě
silnější	silný	k2eAgMnSc1d2	silnější
než	než	k8xS	než
manganistany	manganistan	k1gInPc1	manganistan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oxidují	oxidovat	k5eAaBmIp3nP	oxidovat
například	například	k6eAd1	například
amoniak	amoniak	k1gInSc4	amoniak
již	již	k9	již
za	za	k7c4	za
studena	studeno	k1gNnPc4	studeno
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
dusík	dusík	k1gInSc4	dusík
a	a	k8xC	a
samy	sám	k3xTgFnPc1	sám
se	se	k3xPyFc4	se
redukují	redukovat	k5eAaBmIp3nP	redukovat
na	na	k7c4	na
železitou	železitý	k2eAgFnSc4d1	železitá
sůl	sůl	k1gFnSc4	sůl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kyselém	kyselý	k2eAgNnSc6d1	kyselé
prostředí	prostředí	k1gNnSc6	prostředí
přechází	přecházet	k5eAaImIp3nP	přecházet
železany	železana	k1gFnPc1	železana
za	za	k7c2	za
vývoje	vývoj	k1gInSc2	vývoj
kyslíku	kyslík	k1gInSc2	kyslík
na	na	k7c4	na
železitou	železitý	k2eAgFnSc4d1	železitá
sůl	sůl	k1gFnSc4	sůl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
koncentrovaného	koncentrovaný	k2eAgInSc2d1	koncentrovaný
hydroxidu	hydroxid	k1gInSc2	hydroxid
přecházejí	přecházet	k5eAaImIp3nP	přecházet
železany	železan	k1gInPc1	železan
na	na	k7c4	na
železičitany	železičitan	k1gInPc4	železičitan
za	za	k7c2	za
současného	současný	k2eAgNnSc2d1	současné
odštěpování	odštěpování	k1gNnSc2	odštěpování
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
běžné	běžný	k2eAgInPc4d1	běžný
přírodní	přírodní	k2eAgInPc4d1	přírodní
materiály	materiál	k1gInPc4	materiál
patří	patřit	k5eAaImIp3nP	patřit
oxid	oxid	k1gInSc4	oxid
železnato-železitý	železnato-železitý	k2eAgInSc4d1	železnato-železitý
Fe	Fe	k1gFnSc7	Fe
<g/>
3	[number]	k4	3
<g/>
O	O	kA	O
<g/>
4	[number]	k4	4
(	(	kIx(	(
<g/>
FeIIO-II	FeIIO-II	k1gMnPc2	FeIIO-II
<g/>
·	·	k?	·
<g/>
Fe	Fe	k1gFnSc7	Fe
<g/>
2	[number]	k4	2
<g/>
IIIO	IIIO	kA	IIIO
<g/>
3	[number]	k4	3
<g/>
-II	-II	k?	-II
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
se	se	k3xPyFc4	se
jako	jako	k9	jako
minerál	minerál	k1gInSc1	minerál
magnetit	magnetit	k1gInSc1	magnetit
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc4d1	podobná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
má	mít	k5eAaImIp3nS	mít
oxid	oxid	k1gInSc1	oxid
Fe	Fe	k1gFnSc2	Fe
<g/>
4	[number]	k4	4
<g/>
O	o	k7c4	o
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
uměle	uměle	k6eAd1	uměle
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
r.	r.	kA	r.
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
existence	existence	k1gFnSc1	existence
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
vysokých	vysoký	k2eAgInPc6d1	vysoký
tlacích	tlak	k1gInPc6	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgInPc1d1	stavební
prvky	prvek	k1gInPc1	prvek
Fe	Fe	k1gFnSc1	Fe
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
a	a	k8xC	a
FeO	FeO	k1gFnPc1	FeO
jsou	být	k5eAaImIp3nP	být
svázány	svázat	k5eAaPmNgFnP	svázat
do	do	k7c2	do
komplexnější	komplexní	k2eAgFnSc2d2	komplexnější
struktury	struktura	k1gFnSc2	struktura
izomorfní	izomorfní	k2eAgFnSc2d1	izomorfní
s	s	k7c7	s
CaFe	CaFe	k1gFnSc7	CaFe
<g/>
3	[number]	k4	3
<g/>
O	O	kA	O
<g/>
5	[number]	k4	5
umožňující	umožňující	k2eAgFnSc2d1	umožňující
zachovat	zachovat	k5eAaPmF	zachovat
pevný	pevný	k2eAgInSc4d1	pevný
stechiometrický	stechiometrický	k2eAgInSc4d1	stechiometrický
poměr	poměr	k1gInSc4	poměr
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
komplexní	komplexní	k2eAgFnSc4d1	komplexní
strukturu	struktura	k1gFnSc4	struktura
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
oxid	oxid	k1gInSc1	oxid
Fe	Fe	k1gFnSc2	Fe
<g/>
5	[number]	k4	5
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
existující	existující	k2eAgFnSc4d1	existující
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
vysokého	vysoký	k2eAgInSc2d1	vysoký
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Železité	železitý	k2eAgInPc1d1	železitý
komplexy	komplex	k1gInPc1	komplex
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ruthenitých	ruthenitý	k2eAgFnPc2d1	ruthenitý
a	a	k8xC	a
osmitých	osmitý	k2eAgFnPc2d1	osmitý
<g/>
)	)	kIx)	)
vysokospinové	vysokospinové	k2eAgFnPc2d1	vysokospinové
(	(	kIx(	(
<g/>
častěji	často	k6eAd2	často
<g/>
)	)	kIx)	)
i	i	k9	i
nízkospinové	nízkospinové	k2eAgFnSc1d1	nízkospinové
(	(	kIx(	(
<g/>
s	s	k7c7	s
bipyridylen	bipyridylna	k1gFnPc2	bipyridylna
(	(	kIx(	(
<g/>
bipy	bipa	k1gFnPc1	bipa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fenanthrolinem	fenanthrolin	k1gInSc7	fenanthrolin
(	(	kIx(	(
<g/>
phen	phen	k1gInSc1	phen
<g/>
)	)	kIx)	)
a	a	k8xC	a
kyanidovými	kyanidový	k2eAgInPc7d1	kyanidový
aniony	anion	k1gInPc7	anion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
komplexů	komplex	k1gInPc2	komplex
má	mít	k5eAaImIp3nS	mít
oktaedrické	oktaedrický	k2eAgNnSc4d1	oktaedrický
uspořádání	uspořádání	k1gNnSc4	uspořádání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
objevovat	objevovat	k5eAaImF	objevovat
i	i	k9	i
jiná	jiný	k2eAgFnSc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Komplexy	komplex	k1gInPc1	komplex
FeIII	FeIII	k1gFnSc2	FeIII
nejsou	být	k5eNaImIp3nP	být
příliš	příliš	k6eAd1	příliš
stabilní	stabilní	k2eAgFnPc1d1	stabilní
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
se	se	k3xPyFc4	se
redukovat	redukovat	k5eAaBmF	redukovat
<g/>
.	.	kIx.	.
</s>
<s>
Trojmocné	trojmocný	k2eAgNnSc1d1	trojmocné
železo	železo	k1gNnSc1	železo
tvoří	tvořit	k5eAaImIp3nS	tvořit
řadu	řada	k1gFnSc4	řada
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
kationtů	kation	k1gInPc2	kation
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
tvoří	tvořit	k5eAaImIp3nP	tvořit
soli	sůl	k1gFnPc4	sůl
s	s	k7c7	s
anionty	anion	k1gInPc7	anion
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
nemají	mít	k5eNaImIp3nP	mít
např.	např.	kA	např.
jako	jako	k8xC	jako
jodidy	jodid	k1gInPc1	jodid
redukční	redukční	k2eAgFnSc2d1	redukční
schopnosti	schopnost	k1gFnSc2	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnPc1	sůl
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
získat	získat	k5eAaPmF	získat
z	z	k7c2	z
roztoků	roztok	k1gInPc2	roztok
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
krystalických	krystalický	k2eAgFnPc2d1	krystalická
světle	světle	k6eAd1	světle
růžových	růžový	k2eAgFnPc2d1	růžová
nebo	nebo	k8xC	nebo
světle	světle	k6eAd1	světle
fialových	fialový	k2eAgInPc2d1	fialový
hydrátů	hydrát	k1gInPc2	hydrát
s	s	k7c7	s
hexaaquaželezitým	hexaaquaželezitý	k2eAgInSc7d1	hexaaquaželezitý
kationtem	kation	k1gInSc7	kation
[	[	kIx(	[
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
přítomen	přítomen	k2eAgMnSc1d1	přítomen
v	v	k7c6	v
kamencích	kamenec	k1gInPc6	kamenec
[	[	kIx(	[
<g/>
MI	já	k3xPp1nSc3	já
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
[	[	kIx(	[
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c4	v
barvířství	barvířství	k1gNnPc4	barvířství
jako	jako	k8xC	jako
mořidla	mořidlo	k1gNnPc4	mořidlo
<g/>
.	.	kIx.	.
</s>
<s>
Nejlevnější	levný	k2eAgFnSc7d3	nejlevnější
solí	sůl	k1gFnSc7	sůl
je	být	k5eAaImIp3nS	být
síran	síran	k1gInSc4	síran
železitý	železitý	k2eAgInSc4d1	železitý
Fe	Fe	k1gFnSc7	Fe
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
•	•	k?	•
<g/>
n	n	k0	n
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
,	,	kIx,	,
10	[number]	k4	10
a	a	k8xC	a
12	[number]	k4	12
<g/>
-hydrát	ydrát	k1gInSc1	-hydrát
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgInSc1d3	nejčastější
je	být	k5eAaImIp3nS	být
nonahydrát	nonahydrát	k1gInSc1	nonahydrát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
kolagulační	kolagulační	k2eAgNnSc1d1	kolagulační
činidlo	činidlo	k1gNnSc1	činidlo
při	při	k7c6	při
úpravě	úprava	k1gFnSc6	úprava
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
čištění	čištění	k1gNnSc2	čištění
městských	městský	k2eAgFnPc2d1	městská
a	a	k8xC	a
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
při	při	k7c6	při
krystalizaci	krystalizace	k1gFnSc6	krystalizace
z	z	k7c2	z
roztoků	roztok	k1gInPc2	roztok
nedodržíme	dodržet	k5eNaPmIp1nP	dodržet
nízké	nízký	k2eAgInPc1d1	nízký
pH	ph	kA	ph
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
hydrolýze	hydrolýza	k1gFnSc3	hydrolýza
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
žlutých	žlutý	k2eAgInPc2d1	žlutý
produktů	produkt	k1gInPc2	produkt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
znečišťují	znečišťovat	k5eAaImIp3nP	znečišťovat
vylučující	vylučující	k2eAgFnSc4d1	vylučující
se	se	k3xPyFc4	se
sůl	sůl	k1gFnSc4	sůl
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hydrolýze	hydrolýza	k1gFnSc3	hydrolýza
dochází	docházet	k5eAaImIp3nS	docházet
také	také	k9	také
při	při	k7c6	při
rozpouštění	rozpouštění	k1gNnSc6	rozpouštění
železitých	železitý	k2eAgFnPc2d1	železitá
solí	sůl	k1gFnPc2	sůl
a	a	k8xC	a
žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
roztoku	roztok	k1gInSc2	roztok
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
hnědou	hnědý	k2eAgFnSc4d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
roztoky	roztok	k1gInPc1	roztok
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
kyselé	kyselý	k2eAgFnPc1d1	kyselá
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vyšším	vysoký	k2eAgMnSc6d2	vyšší
pH	ph	kA	ph
se	se	k3xPyFc4	se
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
červenohnědá	červenohnědý	k2eAgFnSc1d1	červenohnědá
sraženina	sraženina	k1gFnSc1	sraženina
hydratovaného	hydratovaný	k2eAgInSc2d1	hydratovaný
oxidu	oxid	k1gInSc2	oxid
železitého	železitý	k2eAgMnSc4d1	železitý
Fe	Fe	k1gMnSc4	Fe
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
<g/>
•	•	k?	•
<g/>
n	n	k0	n
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
FeIII	FeIII	k1gFnPc2	FeIII
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
kyslíkatým	kyslíkatý	k2eAgInPc3d1	kyslíkatý
ligandům	ligand	k1gInPc3	ligand
před	před	k7c7	před
dusíkatými	dusíkatý	k2eAgMnPc7d1	dusíkatý
<g/>
.	.	kIx.	.
</s>
<s>
Amonné	amonný	k2eAgInPc1d1	amonný
komplexy	komplex	k1gInPc1	komplex
nejsou	být	k5eNaImIp3nP	být
stabilní	stabilní	k2eAgInPc1d1	stabilní
a	a	k8xC	a
ve	v	k7c6	v
vodném	vodné	k1gNnSc6	vodné
roztoku	roztok	k1gInSc2	roztok
disociují	disociovat	k5eAaBmIp3nP	disociovat
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
stálejší	stálý	k2eAgInPc1d2	stálejší
jsou	být	k5eAaImIp3nP	být
chelátotvorné	chelátotvorný	k2eAgInPc1d1	chelátotvorný
komplexy	komplex	k1gInPc1	komplex
(	(	kIx(	(
<g/>
ligand	ligand	k1gInSc1	ligand
s	s	k7c7	s
více	hodně	k6eAd2	hodně
donorovými	donorův	k2eAgInPc7d1	donorův
elektrony	elektron	k1gInPc7	elektron
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
bipyridyl	bipyridyl	k1gInSc4	bipyridyl
(	(	kIx(	(
<g/>
bipy	bip	k2eAgInPc4d1	bip
<g/>
)	)	kIx)	)
a	a	k8xC	a
fenanthrolin	fenanthrolin	k2eAgInSc4d1	fenanthrolin
(	(	kIx(	(
<g/>
phen	phen	k1gInSc4	phen
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
nízkospinové	nízkospinové	k2eAgFnSc1d1	nízkospinové
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
stabilní	stabilní	k2eAgFnPc1d1	stabilní
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
analogické	analogický	k2eAgFnPc1d1	analogická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
železnaté	železnatý	k2eAgFnPc1d1	železnatá
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
tmavě	tmavě	k6eAd1	tmavě
červený	červený	k2eAgMnSc1d1	červený
[	[	kIx(	[
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
phen	phen	k1gMnSc1	phen
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vodném	vodný	k2eAgInSc6d1	vodný
roztoku	roztok	k1gInSc6	roztok
velice	velice	k6eAd1	velice
stálý	stálý	k2eAgInSc1d1	stálý
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
tmavě	tmavě	k6eAd1	tmavě
modrý	modrý	k2eAgMnSc1d1	modrý
[	[	kIx(	[
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
phen	phen	k1gMnSc1	phen
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
se	se	k3xPyFc4	se
hydrolyzuje	hydrolyzovat	k5eAaBmIp3nS	hydrolyzovat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
polymerních	polymerní	k2eAgFnPc2d1	polymerní
hydroxo-částic	hydroxo-částice	k1gFnPc2	hydroxo-částice
a	a	k8xC	a
roztok	roztok	k1gInSc1	roztok
mění	měnit	k5eAaImIp3nS	měnit
barvu	barva	k1gFnSc4	barva
na	na	k7c4	na
khaki	khaki	k1gNnSc4	khaki
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
fenoly	fenol	k1gInPc7	fenol
nebo	nebo	k8xC	nebo
enoly	enol	k1gInPc7	enol
dává	dávat	k5eAaImIp3nS	dávat
FeIII	FeIII	k1gFnSc4	FeIII
intenzivní	intenzivní	k2eAgNnSc1d1	intenzivní
zbarvení	zbarvení	k1gNnSc1	zbarvení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
důkaz	důkaz	k1gInSc1	důkaz
těchto	tento	k3xDgFnPc2	tento
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Přidáním	přidání	k1gNnSc7	přidání
kyseliny	kyselina	k1gFnSc2	kyselina
fosforečné	fosforečný	k2eAgFnSc6d1	fosforečná
do	do	k7c2	do
žlutého	žlutý	k2eAgInSc2d1	žlutý
roztoku	roztok	k1gInSc2	roztok
FeCl	FeCl	k1gInSc4	FeCl
<g/>
3	[number]	k4	3
vznikají	vznikat	k5eAaImIp3nP	vznikat
bezbarvé	bezbarvý	k2eAgInPc1d1	bezbarvý
komplexy	komplex	k1gInPc1	komplex
[	[	kIx(	[
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
a	a	k8xC	a
[	[	kIx(	[
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
HPO	HPO	kA	HPO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
komplexy	komplex	k1gInPc7	komplex
s	s	k7c7	s
kyslíkatými	kyslíkatý	k2eAgInPc7d1	kyslíkatý
donory	donor	k1gInPc7	donor
jsou	být	k5eAaImIp3nP	být
zelené	zelený	k2eAgInPc1d1	zelený
oxalatoželezitany	oxalatoželezitan	k1gInPc1	oxalatoželezitan
[	[	kIx(	[
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
a	a	k8xC	a
tmavě	tmavě	k6eAd1	tmavě
červený	červený	k2eAgInSc1d1	červený
[	[	kIx(	[
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
acac	acac	k1gFnSc1	acac
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Oxalatoželezitany	Oxalatoželezitan	k1gInPc1	Oxalatoželezitan
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
stálé	stálý	k2eAgFnPc1d1	stálá
vůči	vůči	k7c3	vůči
disociaci	disociace	k1gFnSc3	disociace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
fotosensitivní	fotosensitivní	k2eAgMnPc1d1	fotosensitivní
a	a	k8xC	a
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
Fe	Fe	k1gFnSc6	Fe
<g/>
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
a	a	k8xC	a
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
komplexy	komplex	k1gInPc1	komplex
s	s	k7c7	s
ligandy	ligand	k1gInPc7	ligand
obsahující	obsahující	k2eAgFnSc2d1	obsahující
kyslíkové	kyslíkový	k2eAgFnSc2d1	kyslíková
i	i	k8xC	i
dusíkové	dusíkový	k2eAgFnSc2d1	dusíková
donorové	donorová	k1gFnSc2	donorová
atomy	atom	k1gInPc1	atom
<g/>
,	,	kIx,	,
např.	např.	kA	např.
EDTA	EDTA	kA	EDTA
a	a	k8xC	a
Schiffovy	Schiffův	k2eAgFnSc2d1	Schiffova
báze	báze	k1gFnSc2	báze
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
[	[	kIx(	[
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
edta	edta	k6eAd1	edta
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
koordinační	koordinační	k2eAgNnSc1d1	koordinační
číslo	číslo	k1gNnSc1	číslo
7	[number]	k4	7
(	(	kIx(	(
<g/>
pentagonální	pentagonální	k2eAgFnSc1d1	pentagonální
bipyramida	bipyramida	k1gFnSc1	bipyramida
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c4	v
[	[	kIx(	[
<g/>
FeCl	FeCl	k1gInSc4	FeCl
<g/>
(	(	kIx(	(
<g/>
salen	salen	k1gInSc4	salen
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
koordinační	koordinační	k2eAgNnSc1d1	koordinační
číslo	číslo	k1gNnSc1	číslo
5	[number]	k4	5
(	(	kIx(	(
<g/>
čtvercová	čtvercový	k2eAgFnSc1d1	čtvercová
pyramida	pyramida	k1gFnSc1	pyramida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stálost	stálost	k1gFnSc1	stálost
halogenidových	halogenidový	k2eAgInPc2d1	halogenidový
komplexů	komplex	k1gInPc2	komplex
klesá	klesat	k5eAaImIp3nS	klesat
od	od	k7c2	od
fluoru	fluor	k1gInSc2	fluor
k	k	k7c3	k
bromu	brom	k1gInSc3	brom
(	(	kIx(	(
<g/>
komplexy	komplex	k1gInPc1	komplex
jodu	jod	k1gInSc2	jod
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstálejší	stálý	k2eAgInPc1d3	nejstálejší
jsou	být	k5eAaImIp3nP	být
fluoroželezitany	fluoroželezitan	k1gInPc1	fluoroželezitan
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
[	[	kIx(	[
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
F	F	kA	F
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
typu	typ	k1gInSc2	typ
[	[	kIx(	[
<g/>
FeF	FeF	k1gFnSc1	FeF
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
a	a	k8xC	a
[	[	kIx(	[
<g/>
FeF	FeF	k1gFnSc1	FeF
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Chloroželezitany	Chloroželezitan	k1gInPc1	Chloroželezitan
se	se	k3xPyFc4	se
častěji	často	k6eAd2	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
tetraedrickém	tetraedrický	k2eAgNnSc6d1	tetraedrický
uspořádání	uspořádání	k1gNnSc6	uspořádání
[	[	kIx(	[
<g/>
FeCl	FeCl	k1gInSc1	FeCl
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
-	-	kIx~	-
a	a	k8xC	a
méně	málo	k6eAd2	málo
častý	častý	k2eAgMnSc1d1	častý
je	být	k5eAaImIp3nS	být
[	[	kIx(	[
<g/>
FeCl	FeCl	k1gInSc1	FeCl
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Bromoželezitany	Bromoželezitan	k1gInPc1	Bromoželezitan
[	[	kIx(	[
<g/>
FeBr	FeBr	k1gInSc1	FeBr
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
tepelně	tepelně	k6eAd1	tepelně
nestálé	stálý	k2eNgInPc1d1	nestálý
vlivem	vlivem	k7c2	vlivem
redoxních	redoxní	k2eAgInPc2d1	redoxní
procesů	proces	k1gInPc2	proces
(	(	kIx(	(
<g/>
FeII	FeII	k1gMnSc1	FeII
a	a	k8xC	a
Br	br	k0	br
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
krvavě	krvavě	k6eAd1	krvavě
červené	červený	k2eAgNnSc1d1	červené
zbarvení	zbarvení	k1gNnPc1	zbarvení
dávají	dávat	k5eAaImIp3nP	dávat
železité	železitý	k2eAgFnPc4d1	železitá
soli	sůl	k1gFnPc4	sůl
po	po	k7c4	po
přidání	přidání	k1gNnSc4	přidání
SCN-	SCN-	k1gFnSc2	SCN-
<g/>
.	.	kIx.	.
</s>
<s>
Způsobují	způsobovat	k5eAaImIp3nP	způsobovat
ho	on	k3xPp3gInSc4	on
vznikající	vznikající	k2eAgInSc4d1	vznikající
thiokyanatoželezitany	thiokyanatoželezitan	k1gInPc4	thiokyanatoželezitan
(	(	kIx(	(
<g/>
rhodanoželezitany	rhodanoželezitan	k1gInPc4	rhodanoželezitan
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
SCN	SCN	kA	SCN
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
SCN	SCN	kA	SCN
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
Fe	Fe	k1gMnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
SCN	SCN	kA	SCN
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
a	a	k8xC	a
převládající	převládající	k2eAgMnSc1d1	převládající
[	[	kIx(	[
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
SCN	SCN	kA	SCN
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInPc1d3	nejznámější
nízkospinové	nízkospinový	k2eAgInPc1d1	nízkospinový
komplexy	komplex	k1gInPc1	komplex
jsou	být	k5eAaImIp3nP	být
kyanoželezitany	kyanoželezitan	k1gInPc4	kyanoželezitan
[	[	kIx(	[
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
např.	např.	kA	např.
červená	červenat	k5eAaImIp3nS	červenat
krevní	krevní	k2eAgFnSc1d1	krevní
sůl	sůl	k1gFnSc1	sůl
(	(	kIx(	(
<g/>
hexakyanoželezitan	hexakyanoželezitan	k1gInSc1	hexakyanoželezitan
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
,	,	kIx,	,
ferrikyanid	ferrikyanid	k1gInSc1	ferrikyanid
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
)	)	kIx)	)
K	k	k7c3	k
<g/>
3	[number]	k4	3
<g/>
[	[	kIx(	[
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
oxidací	oxidace	k1gFnSc7	oxidace
[	[	kIx(	[
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
např.	např.	kA	např.
chlorem	chlor	k1gInSc7	chlor
<g/>
.	.	kIx.	.
</s>
<s>
Kyanidové	kyanidový	k2eAgFnPc1d1	kyanidová
ligandy	liganda	k1gFnPc1	liganda
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
vázány	vázat	k5eAaImNgFnP	vázat
natolik	natolik	k6eAd1	natolik
slabě	slabě	k6eAd1	slabě
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
hexakyanoželezitany	hexakyanoželezitan	k1gInPc1	hexakyanoželezitan
toxické	toxický	k2eAgInPc1d1	toxický
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
také	také	k9	také
nízké	nízký	k2eAgInPc4d1	nízký
oxidační	oxidační	k2eAgInPc4d1	oxidační
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
solemi	sůl	k1gFnPc7	sůl
Fe	Fe	k1gFnPc2	Fe
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
modrou	modrý	k2eAgFnSc4d1	modrá
sraženinu	sraženina	k1gFnSc4	sraženina
<g/>
,	,	kIx,	,
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
turnbullova	turnbullův	k2eAgFnSc1d1	turnbullův
modř	modř	k1gFnSc1	modř
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
analytických	analytický	k2eAgInPc2d1	analytický
důkazů	důkaz	k1gInPc2	důkaz
přítomnosti	přítomnost	k1gFnSc2	přítomnost
dvojmocného	dvojmocný	k2eAgNnSc2d1	dvojmocné
železa	železo	k1gNnSc2	železo
ve	v	k7c6	v
vzorku	vzorek	k1gInSc6	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Železité	železitý	k2eAgInPc1d1	železitý
komplexy	komplex	k1gInPc1	komplex
s	s	k7c7	s
pěti	pět	k4xCc7	pět
kyanidovými	kyanidový	k2eAgInPc7d1	kyanidový
ligandy	ligand	k1gInPc7	ligand
a	a	k8xC	a
jedním	jeden	k4xCgNnSc7	jeden
jiným	jiné	k1gNnSc7	jiné
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
prussidy	prussida	k1gFnPc1	prussida
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
[	[	kIx(	[
<g/>
Fe	Fe	k1gFnSc2	Fe
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
můžeme	moct	k5eAaImIp1nP	moct
působením	působení	k1gNnSc7	působení
zředěných	zředěný	k2eAgFnPc2d1	zředěná
kyselin	kyselina	k1gFnPc2	kyselina
připravit	připravit	k5eAaPmF	připravit
[	[	kIx(	[
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nitroprussid	nitroprussid	k1gInSc1	nitroprussid
sodný	sodný	k2eAgInSc1d1	sodný
Na	na	k7c6	na
<g/>
2	[number]	k4	2
<g/>
[	[	kIx(	[
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Nitroprussidy	Nitroprussida	k1gFnPc1	Nitroprussida
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
důkazu	důkaz	k1gInSc3	důkaz
S2-	S2-	k1gFnSc2	S2-
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
červeného	červený	k2eAgInSc2d1	červený
[	[	kIx(	[
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
NOS	nos	k1gInSc1	nos
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Železnaté	železnatý	k2eAgInPc1d1	železnatý
komplexy	komplex	k1gInPc1	komplex
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
železitých	železitý	k2eAgInPc2d1	železitý
velmi	velmi	k6eAd1	velmi
stabilní	stabilní	k2eAgFnSc4d1	stabilní
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
komplexní	komplexní	k2eAgFnSc2d1	komplexní
soli	sůl	k1gFnSc2	sůl
téměř	téměř	k6eAd1	téměř
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
anionty	anion	k1gInPc7	anion
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
dusitanový	dusitanový	k2eAgInSc4d1	dusitanový
(	(	kIx(	(
<g/>
redukce	redukce	k1gFnSc1	redukce
na	na	k7c4	na
NO	no	k9	no
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bromičnanový	bromičnanový	k2eAgInSc1d1	bromičnanový
a	a	k8xC	a
jodičnanový	jodičnanový	k2eAgInSc1d1	jodičnanový
anion	anion	k1gInSc1	anion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
roztoky	roztok	k1gInPc1	roztok
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
hexaaquaželeznaté	hexaaquaželeznatý	k2eAgInPc1d1	hexaaquaželeznatý
kationty	kation	k1gInPc1	kation
[	[	kIx(	[
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
také	také	k9	také
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
např.	např.	kA	např.
v	v	k7c6	v
Fe	Fe	k1gFnSc6	Fe
<g/>
(	(	kIx(	(
<g/>
ClO	clo	k1gNnSc1	clo
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
•	•	k?	•
6	[number]	k4	6
H	H	kA	H
<g/>
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
FeSO	FeSO	k1gFnSc1	FeSO
<g/>
4	[number]	k4	4
•	•	k?	•
7	[number]	k4	7
H2O	H2O	k1gFnSc1	H2O
(	(	kIx(	(
<g/>
zelená	zelený	k2eAgFnSc1d1	zelená
skalice	skalice	k1gFnSc1	skalice
<g/>
)	)	kIx)	)
a	a	k8xC	a
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
•	•	k?	•
FeSO	FeSO	k1gFnPc2	FeSO
<g/>
4	[number]	k4	4
•	•	k?	•
7	[number]	k4	7
H2O	H2O	k1gFnSc1	H2O
(	(	kIx(	(
<g/>
Mohrova	Mohrův	k2eAgFnSc1d1	Mohrova
sůl	sůl	k1gFnSc1	sůl
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
odměrné	odměrný	k2eAgFnSc6d1	odměrná
analýze	analýza	k1gFnSc6	analýza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Železnaté	železnatý	k2eAgFnPc1d1	železnatá
soli	sůl	k1gFnPc1	sůl
nepodléhají	podléhat	k5eNaImIp3nP	podléhat
hydrolýze	hydrolýza	k1gFnSc3	hydrolýza
<g/>
,	,	kIx,	,
po	po	k7c6	po
přidání	přidání	k1gNnSc6	přidání
CO32-	CO32-	k1gFnSc2	CO32-
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
sráží	srážet	k5eAaImIp3nS	srážet
FeCO	FeCO	k1gFnSc1	FeCO
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
následně	následně	k6eAd1	následně
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
CO2	CO2	k1gFnSc4	CO2
v	v	k7c6	v
nadbytku	nadbytek	k1gInSc6	nadbytek
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
Fe	Fe	k1gMnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
HCO	HCO	kA	HCO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
minerálních	minerální	k2eAgFnPc6d1	minerální
vodách	voda	k1gFnPc6	voda
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vylučování	vylučování	k1gNnSc1	vylučování
FeCO	FeCO	k1gFnSc2	FeCO
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
na	na	k7c4	na
hydratované	hydratovaný	k2eAgInPc4d1	hydratovaný
oxidy	oxid	k1gInPc4	oxid
železité	železitý	k2eAgFnSc2d1	železitá
Fe	Fe	k1gFnSc2	Fe
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
•	•	k?	•
<g/>
n	n	k0	n
H2O	H2O	k1gFnPc1	H2O
(	(	kIx(	(
<g/>
hnědé	hnědý	k2eAgFnPc1d1	hnědá
sraženiny	sraženina	k1gFnPc1	sraženina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
železnatých	železnatý	k2eAgFnPc2d1	železnatá
solí	sůl	k1gFnPc2	sůl
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
snadná	snadný	k2eAgFnSc1d1	snadná
oxidace	oxidace	k1gFnSc1	oxidace
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
)	)	kIx)	)
na	na	k7c4	na
železité	železitý	k2eAgFnPc4d1	železitá
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
zase	zase	k9	zase
stabilní	stabilní	k2eAgFnSc4d1	stabilní
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Stálost	stálost	k1gFnSc1	stálost
vůči	vůči	k7c3	vůči
oxidaci	oxidace	k1gFnSc3	oxidace
na	na	k7c4	na
železité	železitý	k2eAgFnPc4d1	železitá
sloučeniny	sloučenina	k1gFnPc4	sloučenina
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
některé	některý	k3yIgFnPc1	některý
podvojné	podvojný	k2eAgFnPc1d1	podvojná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
-	-	kIx~	-
zejména	zejména	k9	zejména
sírany	síran	k1gInPc1	síran
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
Mohrova	Mohrův	k2eAgFnSc1d1	Mohrova
sůl	sůl	k1gFnSc1	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Železnatý	železnatý	k2eAgInSc1d1	železnatý
kation	kation	k1gInSc1	kation
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
železitého	železitý	k2eAgNnSc2d1	železité
<g/>
,	,	kIx,	,
výraznější	výrazný	k2eAgInSc4d2	výraznější
sklon	sklon	k1gInSc4	sklon
ke	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
komplexů	komplex	k1gInPc2	komplex
s	s	k7c7	s
dusíkatými	dusíkatý	k2eAgInPc7d1	dusíkatý
ligandy	ligand	k1gInPc7	ligand
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgInSc4d2	menší
sklon	sklon	k1gInSc4	sklon
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
ligandů	ligand	k1gInPc2	ligand
s	s	k7c7	s
kyslíkatými	kyslíkatý	k2eAgInPc7d1	kyslíkatý
ligandy	ligand	k1gInPc7	ligand
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
existuje	existovat	k5eAaImIp3nS	existovat
[	[	kIx(	[
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
a	a	k8xC	a
méně	málo	k6eAd2	málo
stále	stále	k6eAd1	stále
komplexy	komplex	k1gInPc1	komplex
s	s	k7c7	s
acac	acac	k6eAd1	acac
a	a	k8xC	a
šťavelanovým	šťavelanův	k2eAgInSc7d1	šťavelanův
aniontem	anion	k1gInSc7	anion
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
železnatých	železnatý	k2eAgInPc2d1	železnatý
komplexů	komplex	k1gInPc2	komplex
je	být	k5eAaImIp3nS	být
oktaedrická	oktaedrický	k2eAgFnSc1d1	oktaedrický
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tetraedrickým	tetraedrický	k2eAgInPc3d1	tetraedrický
komplexům	komplex	k1gInPc3	komplex
patří	patřit	k5eAaImIp3nS	patřit
komplexy	komplex	k1gInPc4	komplex
s	s	k7c7	s
chlorem	chlor	k1gInSc7	chlor
<g/>
,	,	kIx,	,
bromem	brom	k1gInSc7	brom
<g/>
,	,	kIx,	,
jodem	jod	k1gInSc7	jod
a	a	k8xC	a
rhodanidem	rhodanid	k1gInSc7	rhodanid
[	[	kIx(	[
<g/>
FeX	FeX	k1gFnSc1	FeX
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Železnaté	železnatý	k2eAgInPc1d1	železnatý
komplexy	komplex	k1gInPc1	komplex
jsou	být	k5eAaImIp3nP	být
častěji	často	k6eAd2	často
vysokospinové	vysokospinový	k2eAgInPc1d1	vysokospinový
<g/>
,	,	kIx,	,
nízkospinové	nízkospinový	k2eAgInPc1d1	nízkospinový
komplexy	komplex	k1gInPc1	komplex
tvoří	tvořit	k5eAaImIp3nP	tvořit
železo	železo	k1gNnSc4	železo
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
bipyridylen	bipyridylna	k1gFnPc2	bipyridylna
(	(	kIx(	(
<g/>
bipy	bipa	k1gFnPc1	bipa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fenanthrolinem	fenanthrolin	k1gInSc7	fenanthrolin
(	(	kIx(	(
<g/>
phen	phen	k1gInSc1	phen
<g/>
)	)	kIx)	)
a	a	k8xC	a
kyanidovými	kyanidový	k2eAgInPc7d1	kyanidový
anionty	anion	k1gInPc7	anion
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Fe	Fe	k1gFnPc1	Fe
<g/>
(	(	kIx(	(
<g/>
bipy	bipa	k1gFnPc1	bipa
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
a	a	k8xC	a
[	[	kIx(	[
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
phen	phen	k1gInSc1	phen
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
jsou	být	k5eAaImIp3nP	být
intenzivně	intenzivně	k6eAd1	intenzivně
červeně	červeně	k6eAd1	červeně
zbarvené	zbarvený	k2eAgFnPc1d1	zbarvená
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
jmenovaný	jmenovaný	k2eAgInSc1d1	jmenovaný
komplex	komplex	k1gInSc1	komplex
slouží	sloužit	k5eAaImIp3nS	sloužit
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
jako	jako	k9	jako
redoxní	redoxní	k2eAgInSc4d1	redoxní
indikátor	indikátor	k1gInSc4	indikátor
ferroin	ferroina	k1gFnPc2	ferroina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
při	při	k7c6	při
styku	styk	k1gInSc6	styk
s	s	k7c7	s
oxidačním	oxidační	k2eAgNnSc7d1	oxidační
činidlem	činidlo	k1gNnSc7	činidlo
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
na	na	k7c4	na
modrý	modrý	k2eAgInSc4d1	modrý
[	[	kIx(	[
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
phen	phen	k1gInSc1	phen
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Vysokospinové	Vysokospin	k1gMnPc1	Vysokospin
mono-	mono-	k?	mono-
a	a	k8xC	a
bisfenathrolinové	bisfenathrolinový	k2eAgInPc4d1	bisfenathrolinový
komplexy	komplex	k1gInPc4	komplex
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
obtížně	obtížně	k6eAd1	obtížně
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
kyanoželeznatany	kyanoželeznatan	k1gInPc4	kyanoželeznatan
[	[	kIx(	[
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
patří	patřit	k5eAaImIp3nS	patřit
světle	světle	k6eAd1	světle
žlutý	žlutý	k2eAgInSc1d1	žlutý
hexakyanoželeznatan	hexakyanoželeznatan	k1gInSc1	hexakyanoželeznatan
draselný	draselný	k2eAgInSc1d1	draselný
K	k	k7c3	k
<g/>
4	[number]	k4	4
<g/>
[	[	kIx(	[
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
žlutá	žlutý	k2eAgFnSc1d1	žlutá
krevní	krevní	k2eAgFnSc1d1	krevní
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
ferrokyanid	ferrokyanid	k1gInSc1	ferrokyanid
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
připravoval	připravovat	k5eAaImAgInS	připravovat
tavením	tavení	k1gNnSc7	tavení
dusíkatých	dusíkatý	k2eAgInPc2d1	dusíkatý
živočišných	živočišný	k2eAgInPc2d1	živočišný
zbytků	zbytek	k1gInPc2	zbytek
(	(	kIx(	(
<g/>
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
rohy	roh	k1gInPc1	roh
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
se	s	k7c7	s
železem	železo	k1gNnSc7	železo
a	a	k8xC	a
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
CO	co	k8xS	co
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
netoxický	toxický	k2eNgMnSc1d1	netoxický
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
solemi	sůl	k1gFnPc7	sůl
Fe	Fe	k1gFnPc2	Fe
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
intenzivně	intenzivně	k6eAd1	intenzivně
modrou	modrý	k2eAgFnSc4d1	modrá
sraženinu	sraženina	k1gFnSc4	sraženina
<g/>
,	,	kIx,	,
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
berlínská	berlínský	k2eAgFnSc1d1	Berlínská
modř	modř	k1gFnSc1	modř
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
analytických	analytický	k2eAgInPc2d1	analytický
důkazů	důkaz	k1gInPc2	důkaz
přítomnosti	přítomnost	k1gFnSc2	přítomnost
trojmocného	trojmocný	k2eAgNnSc2d1	trojmocné
železa	železo	k1gNnSc2	železo
ve	v	k7c6	v
vzorku	vzorek	k1gInSc6	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Oxalatoželeznatany	Oxalatoželeznatan	k1gInPc1	Oxalatoželeznatan
jsou	být	k5eAaImIp3nP	být
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
s	s	k7c7	s
obecným	obecný	k2eAgInSc7d1	obecný
vzorcem	vzorec	k1gInSc7	vzorec
M	M	kA	M
<g/>
2	[number]	k4	2
<g/>
I	I	kA	I
<g/>
[	[	kIx(	[
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
Komplexy	komplex	k1gInPc1	komplex
v	v	k7c6	v
nižších	nízký	k2eAgNnPc6d2	nižší
oxidačních	oxidační	k2eAgNnPc6d1	oxidační
číslech	číslo	k1gNnPc6	číslo
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
připravit	připravit	k5eAaPmF	připravit
s	s	k7c7	s
karbonyly	karbonyl	k1gInPc7	karbonyl
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
Fe	Fe	k1gFnSc1	Fe
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Pentakarbonyl	Pentakarbonyl	k1gInSc1	Pentakarbonyl
železa	železo	k1gNnSc2	železo
Fe	Fe	k1gFnSc2	Fe
<g/>
(	(	kIx(	(
<g/>
CO	co	k6eAd1	co
<g/>
)	)	kIx)	)
<g/>
5	[number]	k4	5
je	být	k5eAaImIp3nS	být
kapalná	kapalný	k2eAgFnSc1d1	kapalná
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgInSc1d1	vznikající
tlakovým	tlakový	k2eAgNnSc7d1	tlakové
zahříváním	zahřívání	k1gNnSc7	zahřívání
práškového	práškový	k2eAgNnSc2d1	práškové
kovového	kovový	k2eAgNnSc2d1	kovové
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nestálá	stálý	k2eNgFnSc1d1	nestálá
toxická	toxický	k2eAgFnSc1d1	toxická
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
velmi	velmi	k6eAd1	velmi
čistého	čistý	k2eAgNnSc2d1	čisté
železa	železo	k1gNnSc2	železo
-	-	kIx~	-
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
snadno	snadno	k6eAd1	snadno
destilačně	destilačně	k6eAd1	destilačně
oddělit	oddělit	k5eAaPmF	oddělit
od	od	k7c2	od
solí	sůl	k1gFnPc2	sůl
ostatních	ostatní	k2eAgInPc2d1	ostatní
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Železo	železo	k1gNnSc1	železo
je	být	k5eAaImIp3nS	být
biogenním	biogenní	k2eAgInSc7d1	biogenní
prvkem	prvek	k1gInSc7	prvek
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
organokovových	organokovový	k2eAgFnPc2d1	organokovová
sloučenin	sloučenina	k1gFnPc2	sloučenina
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
biogenní	biogenní	k2eAgFnPc1d1	biogenní
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
organokovovou	organokovový	k2eAgFnSc7d1	organokovová
sloučeninou	sloučenina	k1gFnSc7	sloučenina
železa	železo	k1gNnSc2	železo
je	být	k5eAaImIp3nS	být
bezpochyby	bezpochyby	k6eAd1	bezpochyby
hemoglobin	hemoglobin	k1gInSc4	hemoglobin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přenáší	přenášet	k5eAaImIp3nS	přenášet
kyslík	kyslík	k1gInSc4	kyslík
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
udržuje	udržovat	k5eAaImIp3nS	udržovat
tak	tak	k6eAd1	tak
celý	celý	k2eAgInSc1d1	celý
organismus	organismus	k1gInSc1	organismus
při	při	k7c6	při
životě	život	k1gInSc6	život
před	před	k7c7	před
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
zadušením	zadušení	k1gNnSc7	zadušení
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
sloučeninám	sloučenina	k1gFnPc3	sloučenina
železa	železo	k1gNnSc2	železo
však	však	k8xC	však
také	také	k9	také
patří	patřit	k5eAaImIp3nP	patřit
soli	sůl	k1gFnPc1	sůl
organických	organický	k2eAgFnPc2d1	organická
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
karbonyly	karbonyl	k1gInPc4	karbonyl
železa	železo	k1gNnSc2	železo
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šťavelan	šťavelan	k1gInSc4	šťavelan
železnatý	železnatý	k2eAgInSc4d1	železnatý
FeC	FeC	k1gFnSc7	FeC
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
žlutá	žlutý	k2eAgFnSc1d1	žlutá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
i	i	k9	i
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
oxalatoželeznatany	oxalatoželeznatan	k1gInPc1	oxalatoželeznatan
<g/>
.	.	kIx.	.
</s>
<s>
Šťavelan	šťavelan	k1gInSc1	šťavelan
železnatý	železnatý	k2eAgMnSc1d1	železnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
sražením	sražení	k1gNnSc7	sražení
železnatých	železnatý	k2eAgFnPc2d1	železnatá
solí	sůl	k1gFnPc2	sůl
v	v	k7c6	v
kyselinou	kyselina	k1gFnSc7	kyselina
šťavelovou	šťavelový	k2eAgFnSc7d1	šťavelová
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
pigment	pigment	k1gInSc1	pigment
<g/>
.	.	kIx.	.
</s>
<s>
Šťavelan	šťavelan	k1gInSc4	šťavelan
železitý	železitý	k2eAgInSc4d1	železitý
Fe	Fe	k1gFnSc7	Fe
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
hnědá	hnědý	k2eAgFnSc1d1	hnědá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
nestálá	stálý	k2eNgFnSc1d1	nestálá
na	na	k7c6	na
světle	světlo	k1gNnSc6	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světle	světlo	k1gNnSc6	světlo
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
šťavelan	šťavelan	k1gInSc1	šťavelan
železitý	železitý	k2eAgInSc1d1	železitý
redukuje	redukovat	k5eAaBmIp3nS	redukovat
na	na	k7c4	na
šťavelan	šťavelan	k1gInSc4	šťavelan
železnatý	železnatý	k2eAgInSc4d1	železnatý
<g/>
.	.	kIx.	.
</s>
<s>
Šťavelan	šťavelan	k1gInSc4	šťavelan
železitý	železitý	k2eAgInSc4d1	železitý
v	v	k7c6	v
roztocích	roztok	k1gInPc6	roztok
vytváří	vytvářit	k5eAaPmIp3nP	vytvářit
rozpustné	rozpustný	k2eAgFnPc1d1	rozpustná
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
oxalatoželezitany	oxalatoželezitan	k1gInPc1	oxalatoželezitan
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
šťavelové	šťavelový	k2eAgFnSc6d1	šťavelová
<g/>
.	.	kIx.	.
</s>
<s>
Železo	železo	k1gNnSc1	železo
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
mikrobiogenní	mikrobiogenní	k2eAgInPc4d1	mikrobiogenní
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
obvykle	obvykle	k6eAd1	obvykle
méně	málo	k6eAd2	málo
než	než	k8xS	než
0,005	[number]	k4	0,005
<g/>
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
těle	tělo	k1gNnSc6	tělo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
asi	asi	k9	asi
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
gramy	gram	k1gInPc7	gram
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
množství	množství	k1gNnSc2	množství
65	[number]	k4	65
<g/>
-	-	kIx~	-
<g/>
70	[number]	k4	70
%	%	kIx~	%
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
molekulách	molekula	k1gFnPc6	molekula
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
%	%	kIx~	%
v	v	k7c4	v
myoglobinu	myoglobina	k1gFnSc4	myoglobina
asi	asi	k9	asi
1	[number]	k4	1
%	%	kIx~	%
v	v	k7c6	v
enzymech	enzym	k1gInPc6	enzym
(	(	kIx(	(
<g/>
cytochromy	cytochrom	k1gInPc1	cytochrom
<g/>
,	,	kIx,	,
cytochromoxidasa	cytochromoxidasa	k1gFnSc1	cytochromoxidasa
<g/>
,	,	kIx,	,
peroxidasa	peroxidasa	k1gFnSc1	peroxidasa
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
0,1	[number]	k4	0,1
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
transportní	transportní	k2eAgInSc1d1	transportní
(	(	kIx(	(
<g/>
plazmatické	plazmatický	k2eAgNnSc1d1	plazmatické
<g/>
)	)	kIx)	)
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
ve	v	k7c6	v
vazbě	vazba	k1gFnSc6	vazba
na	na	k7c4	na
transferrin	transferrin	k1gInSc4	transferrin
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
%	%	kIx~	%
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
zásobní	zásobní	k2eAgNnSc4d1	zásobní
železo	železo	k1gNnSc4	železo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
vázáno	vázat	k5eAaImNgNnS	vázat
na	na	k7c4	na
bílkoviny	bílkovina	k1gFnPc4	bílkovina
(	(	kIx(	(
<g/>
ferritin	ferritin	k1gInSc1	ferritin
<g/>
,	,	kIx,	,
hemosiderin	hemosiderin	k1gInSc1	hemosiderin
<g/>
)	)	kIx)	)
Anémie	anémie	k1gFnSc1	anémie
z	z	k7c2	z
nedostatku	nedostatek	k1gInSc2	nedostatek
železa	železo	k1gNnSc2	železo
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgFnSc4d3	nejčastější
nutriční	nutriční	k2eAgFnSc4d1	nutriční
deficienci	deficience	k1gFnSc4	deficience
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
populaci	populace	k1gFnSc6	populace
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
něco	něco	k3yInSc1	něco
okolo	okolo	k7c2	okolo
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ztráty	ztráta	k1gFnPc1	ztráta
železa	železo	k1gNnSc2	železo
jsou	být	k5eAaImIp3nP	být
asi	asi	k9	asi
0,5	[number]	k4	0,5
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
mg	mg	kA	mg
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
větší	veliký	k2eAgFnSc2d2	veliký
ztráty	ztráta	k1gFnSc2	ztráta
1,5	[number]	k4	1,5
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
mg	mg	kA	mg
mají	mít	k5eAaImIp3nP	mít
příčinu	příčina	k1gFnSc4	příčina
v	v	k7c6	v
menstruaci	menstruace	k1gFnSc6	menstruace
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
vstřebaného	vstřebaný	k2eAgNnSc2d1	vstřebané
železa	železo	k1gNnSc2	železo
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
denní	denní	k2eAgFnSc1d1	denní
dávka	dávka	k1gFnSc1	dávka
je	být	k5eAaImIp3nS	být
20	[number]	k4	20
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgInSc1d1	minimální
denní	denní	k2eAgInSc1d1	denní
příjem	příjem	k1gInSc1	příjem
železa	železo	k1gNnSc2	železo
nezbytný	nezbytný	k2eAgInSc1d1	nezbytný
pro	pro	k7c4	pro
červenou	červený	k2eAgFnSc4d1	červená
krvetvorbu	krvetvorba	k1gFnSc4	krvetvorba
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
je	být	k5eAaImIp3nS	být
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
především	především	k9	především
vnitřnosti	vnitřnost	k1gFnPc1	vnitřnost
jako	jako	k8xC	jako
játra	játra	k1gNnPc1	játra
<g/>
,	,	kIx,	,
srdce	srdce	k1gNnSc1	srdce
a	a	k8xC	a
slezina	slezina	k1gFnSc1	slezina
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
železa	železo	k1gNnSc2	železo
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
i	i	k9	i
luštěniny	luštěnina	k1gFnPc1	luštěnina
<g/>
,	,	kIx,	,
listová	listový	k2eAgFnSc1d1	listová
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
,	,	kIx,	,
houby	houby	k6eAd1	houby
a	a	k8xC	a
některé	některý	k3yIgNnSc4	některý
ovoce	ovoce	k1gNnSc4	ovoce
jako	jako	k9	jako
například	například	k6eAd1	například
jahody	jahoda	k1gFnSc2	jahoda
<g/>
.	.	kIx.	.
</s>
<s>
Příjem	příjem	k1gInSc1	příjem
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
iontové	iontový	k2eAgFnSc6d1	iontová
formě	forma	k1gFnSc6	forma
(	(	kIx(	(
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
a	a	k8xC	a
Fe	Fe	k1gFnSc1	Fe
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
optimální	optimální	k2eAgInSc4d1	optimální
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
ion	ion	k1gInSc1	ion
Fe	Fe	k1gFnSc2	Fe
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
rizikový	rizikový	k2eAgInSc1d1	rizikový
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
maximální	maximální	k2eAgInSc1d1	maximální
obsah	obsah	k1gInSc1	obsah
v	v	k7c6	v
pitné	pitný	k2eAgFnSc6d1	pitná
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
normativně	normativně	k6eAd1	normativně
omezen	omezit	k5eAaPmNgInS	omezit
na	na	k7c4	na
maximálně	maximálně	k6eAd1	maximálně
0,2	[number]	k4	0,2
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
č.	č.	k?	č.
252	[number]	k4	252
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
..	..	k?	..
špatně	špatně	k6eAd1	špatně
vyvážená	vyvážený	k2eAgFnSc1d1	vyvážená
vegetariánská	vegetariánský	k2eAgFnSc1d1	vegetariánská
strava	strava	k1gFnSc1	strava
<g/>
,	,	kIx,	,
chronické	chronický	k2eAgFnPc1d1	chronická
nízkoenergetické	nízkoenergetický	k2eAgFnPc4d1	nízkoenergetická
diety	dieta	k1gFnPc4	dieta
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
stravovací	stravovací	k2eAgFnPc4d1	stravovací
zvyklosti	zvyklost	k1gFnPc4	zvyklost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vzácný	vzácný	k2eAgInSc4d1	vzácný
příjem	příjem	k1gInSc4	příjem
<g />
.	.	kIx.	.
</s>
<s>
červeného	červený	k2eAgNnSc2d1	červené
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nedostatečné	dostatečný	k2eNgNnSc1d1	nedostatečné
nahrazení	nahrazení	k1gNnSc1	nahrazení
adekvátními	adekvátní	k2eAgFnPc7d1	adekvátní
potravinami	potravina	k1gFnPc7	potravina
atletky	atletka	k1gFnSc2	atletka
(	(	kIx(	(
<g/>
menstruace	menstruace	k1gFnSc1	menstruace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dospívající	dospívající	k2eAgMnPc1d1	dospívající
sportovci	sportovec	k1gMnPc1	sportovec
<g/>
,	,	kIx,	,
těhotné	těhotný	k2eAgFnPc1d1	těhotná
sportovkyně	sportovkyně	k1gFnPc1	sportovkyně
<g/>
,	,	kIx,	,
sportovci	sportovec	k1gMnPc1	sportovec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
přizpůsobují	přizpůsobovat	k5eAaImIp3nP	přizpůsobovat
nadmořské	nadmořský	k2eAgFnSc3d1	nadmořská
výšce	výška	k1gFnSc3	výška
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
v	v	k7c6	v
horkém	horký	k2eAgNnSc6d1	horké
podnebí	podnebí	k1gNnSc6	podnebí
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
ztráty	ztráta	k1gFnSc2	ztráta
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
krvácení	krvácení	k1gNnSc2	krvácení
do	do	k7c2	do
gastrointestinálního	gastrointestinální	k2eAgInSc2d1	gastrointestinální
traktu	trakt	k1gInSc2	trakt
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
např.	např.	kA	např.
vředy	vřed	k1gInPc1	vřed
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nadměrná	nadměrný	k2eAgFnSc1d1	nadměrná
hemolýza	hemolýza	k1gFnSc1	hemolýza
kvůli	kvůli	k7c3	kvůli
zvýšenému	zvýšený	k2eAgNnSc3d1	zvýšené
namáhání	namáhání	k1gNnSc3	namáhání
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
krevní	krevní	k2eAgFnSc2d1	krevní
ztráty	ztráta	k1gFnSc2	ztráta
(	(	kIx(	(
<g/>
např.	např.	kA	např.
krvácení	krvácení	k1gNnSc1	krvácení
z	z	k7c2	z
nosu	nos	k1gInSc2	nos
<g/>
,	,	kIx,	,
kontaktní	kontaktní	k2eAgInPc4d1	kontaktní
sporty	sport	k1gInPc4	sport
<g/>
)	)	kIx)	)
špatné	špatný	k2eAgNnSc4d1	špatné
vstřebávání	vstřebávání	k1gNnSc4	vstřebávání
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
klinických	klinický	k2eAgFnPc2d1	klinická
poruch	porucha	k1gFnPc2	porucha
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
celiakie	celiakie	k1gFnSc1	celiakie
Organokovová	organokovový	k2eAgFnSc1d1	organokovová
komplexní	komplexní	k2eAgFnSc1d1	komplexní
sloučenina	sloučenina	k1gFnSc1	sloučenina
hemoglobin	hemoglobin	k1gInSc1	hemoglobin
hraje	hrát	k5eAaImIp3nS	hrát
totiž	totiž	k9	totiž
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
v	v	k7c6	v
transportu	transport	k1gInSc6	transport
kyslíku	kyslík	k1gInSc2	kyslík
z	z	k7c2	z
plic	plíce	k1gFnPc2	plíce
do	do	k7c2	do
tělesných	tělesný	k2eAgFnPc2d1	tělesná
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nezbytná	nezbytný	k2eAgFnSc1d1	nezbytná
pro	pro	k7c4	pro
dýchání	dýchání	k1gNnSc4	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
je	být	k5eAaImIp3nS	být
hemoglobin	hemoglobin	k1gInSc1	hemoglobin
tvořen	tvořit	k5eAaImNgInS	tvořit
porfyrinovým	porfyrinův	k2eAgInSc7d1	porfyrinův
skeletem	skelet	k1gInSc7	skelet
složeným	složený	k2eAgInSc7d1	složený
ze	z	k7c2	z
4	[number]	k4	4
pyrrolových	pyrrolový	k2eAgNnPc2d1	pyrrolový
jader	jádro	k1gNnPc2	jádro
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
dusíkové	dusíkový	k2eAgInPc1d1	dusíkový
atomy	atom	k1gInPc1	atom
směřují	směřovat	k5eAaImIp3nP	směřovat
do	do	k7c2	do
středu	střed	k1gInSc2	střed
kruhu	kruh	k1gInSc2	kruh
a	a	k8xC	a
komplexně	komplexně	k6eAd1	komplexně
váží	vážit	k5eAaImIp3nS	vážit
atom	atom	k1gInSc4	atom
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Porfyrinová	Porfyrinový	k2eAgFnSc1d1	Porfyrinový
struktura	struktura	k1gFnSc1	struktura
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
velmi	velmi	k6eAd1	velmi
běžná	běžný	k2eAgFnSc1d1	běžná
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgNnSc4d1	zelené
rostlinné	rostlinný	k2eAgNnSc4d1	rostlinné
barvivo	barvivo	k1gNnSc4	barvivo
chlorofyl	chlorofyl	k1gInSc1	chlorofyl
s	s	k7c7	s
porfyrinovým	porfyrinův	k2eAgInSc7d1	porfyrinův
skeletem	skelet	k1gInSc7	skelet
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
centrální	centrální	k2eAgInSc4d1	centrální
atom	atom	k1gInSc4	atom
hořčík	hořčík	k1gInSc1	hořčík
a	a	k8xC	a
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
produkují	produkovat	k5eAaImIp3nP	produkovat
rostliny	rostlina	k1gFnPc1	rostlina
kyslík	kyslík	k1gInSc1	kyslík
(	(	kIx(	(
<g/>
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
<g/>
)	)	kIx)	)
a	a	k8xC	a
současně	současně	k6eAd1	současně
přeměňují	přeměňovat	k5eAaImIp3nP	přeměňovat
energii	energie	k1gFnSc4	energie
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
na	na	k7c6	na
energeticky	energeticky	k6eAd1	energeticky
využitelné	využitelný	k2eAgFnPc1d1	využitelná
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
sacharidy	sacharid	k1gInPc4	sacharid
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
nositelem	nositel	k1gMnSc7	nositel
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
jsou	být	k5eAaImIp3nP	být
červené	červený	k2eAgFnPc4d1	červená
krvinky	krvinka	k1gFnPc4	krvinka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
přenašeč	přenašeč	k1gInSc4	přenašeč
kyslíku	kyslík	k1gInSc2	kyslík
z	z	k7c2	z
plic	plíce	k1gFnPc2	plíce
do	do	k7c2	do
organizmu	organizmus	k1gInSc2	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Důležitý	důležitý	k2eAgInSc1d1	důležitý
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
vazebná	vazebný	k2eAgFnSc1d1	vazebná
energie	energie	k1gFnSc1	energie
mezi	mezi	k7c7	mezi
atomem	atom	k1gInSc7	atom
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
molekulou	molekula	k1gFnSc7	molekula
kyslíku	kyslík	k1gInSc2	kyslík
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
značně	značně	k6eAd1	značně
nízká	nízký	k2eAgFnSc1d1	nízká
a	a	k8xC	a
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
hemoglobin	hemoglobin	k1gInSc1	hemoglobin
v	v	k7c6	v
tkáních	tkáň	k1gFnPc6	tkáň
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
obsahem	obsah	k1gInSc7	obsah
kyslíku	kyslík	k1gInSc2	kyslík
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
jeho	jeho	k3xOp3gFnSc4	jeho
molekulu	molekula	k1gFnSc4	molekula
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
plicních	plicní	k2eAgInPc6d1	plicní
sklípcích	sklípek	k1gInPc6	sklípek
bohatých	bohatý	k2eAgInPc6d1	bohatý
kyslíkem	kyslík	k1gInSc7	kyslík
molekulu	molekula	k1gFnSc4	molekula
kyslíku	kyslík	k1gInSc2	kyslík
vázat	vázat	k5eAaImF	vázat
a	a	k8xC	a
transportovat	transportovat	k5eAaBmF	transportovat
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Porucha	porucha	k1gFnSc1	porucha
přenosu	přenos	k1gInSc2	přenos
kyslíku	kyslík	k1gInSc2	kyslík
nastává	nastávat	k5eAaImIp3nS	nastávat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
dostane	dostat	k5eAaPmIp3nS	dostat
molekula	molekula	k1gFnSc1	molekula
s	s	k7c7	s
podobnými	podobný	k2eAgFnPc7d1	podobná
vazebnými	vazebný	k2eAgFnPc7d1	vazebná
charakteristikami	charakteristika	k1gFnPc7	charakteristika
jako	jako	k9	jako
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnějším	běžný	k2eAgInSc7d3	nejběžnější
případem	případ	k1gInSc7	případ
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
oxid	oxid	k1gInSc1	oxid
uhelnatý	uhelnatý	k2eAgInSc1d1	uhelnatý
CO	co	k6eAd1	co
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
hemoglobin	hemoglobin	k1gInSc4	hemoglobin
stejně	stejně	k6eAd1	stejně
ochotně	ochotně	k6eAd1	ochotně
jako	jako	k8xS	jako
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
této	tento	k3xDgFnSc2	tento
vazby	vazba	k1gFnSc2	vazba
je	být	k5eAaImIp3nS	být
však	však	k9	však
větší	veliký	k2eAgMnSc1d2	veliký
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
tak	tak	k6eAd1	tak
k	k	k7c3	k
zablokování	zablokování	k1gNnSc3	zablokování
přenosu	přenos	k1gInSc2	přenos
kyslíku	kyslík	k1gInSc2	kyslík
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nevratného	vratný	k2eNgNnSc2d1	nevratné
obsazení	obsazení	k1gNnSc2	obsazení
molekul	molekula	k1gFnPc2	molekula
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
oxidem	oxid	k1gInSc7	oxid
uhelnatým	uhelnatý	k2eAgInSc7d1	uhelnatý
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
komplexní	komplexní	k2eAgFnSc1d1	komplexní
sloučenina	sloučenina	k1gFnSc1	sloučenina
CO	co	k6eAd1	co
s	s	k7c7	s
hemoglobinem	hemoglobin	k1gInSc7	hemoglobin
má	mít	k5eAaImIp3nS	mít
intenzivně	intenzivně	k6eAd1	intenzivně
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
otrava	otrava	k1gFnSc1	otrava
oxidem	oxid	k1gInSc7	oxid
uhelnatým	uhelnatý	k2eAgInSc7d1	uhelnatý
zčervenáním	zčervenání	k1gNnPc3	zčervenání
sliznic	sliznice	k1gFnPc2	sliznice
postižené	postižený	k2eAgFnSc2d1	postižená
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Metabolické	metabolický	k2eAgFnPc1d1	metabolická
příčiny	příčina	k1gFnPc1	příčina
snížené	snížený	k2eAgFnSc2d1	snížená
tvorby	tvorba	k1gFnSc2	tvorba
červeného	červený	k2eAgNnSc2d1	červené
krevního	krevní	k2eAgNnSc2d1	krevní
barviva	barvivo	k1gNnSc2	barvivo
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
vyvolány	vyvolat	k5eAaPmNgFnP	vyvolat
nedostatkem	nedostatek	k1gInSc7	nedostatek
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
přijímané	přijímaný	k2eAgFnSc6d1	přijímaná
potravě	potrava	k1gFnSc6	potrava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nízké	nízký	k2eAgNnSc1d1	nízké
množství	množství	k1gNnSc1	množství
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
i	i	k9	i
vysokou	vysoký	k2eAgFnSc7d1	vysoká
ztrátou	ztráta	k1gFnSc7	ztráta
krve	krev	k1gFnSc2	krev
při	při	k7c6	při
úrazu	úraz	k1gInSc6	úraz
<g/>
,	,	kIx,	,
porodu	porod	k1gInSc6	porod
apod.	apod.	kA	apod.
Navenek	navenek	k6eAd1	navenek
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
projevuje	projevovat	k5eAaImIp3nS	projevovat
chorobnou	chorobný	k2eAgFnSc7d1	chorobná
bledostí	bledost	k1gFnSc7	bledost
a	a	k8xC	a
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
únavou	únava	k1gFnSc7	únava
postiženého	postižený	k2eAgMnSc2d1	postižený
<g/>
,	,	kIx,	,
medicínsky	medicínsky	k6eAd1	medicínsky
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
chudokrevnost	chudokrevnost	k1gFnSc1	chudokrevnost
neboli	neboli	k8xC	neboli
anémie	anémie	k1gFnSc1	anémie
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšené	zvýšený	k2eAgNnSc1d1	zvýšené
riziko	riziko	k1gNnSc1	riziko
anémie	anémie	k1gFnSc2	anémie
nastává	nastávat	k5eAaImIp3nS	nastávat
především	především	k9	především
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
při	při	k7c6	při
porodu	porod	k1gInSc6	porod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
částečně	částečně	k6eAd1	částečně
i	i	k9	i
při	při	k7c6	při
běžné	běžný	k2eAgFnSc6d1	běžná
menstruaci	menstruace	k1gFnSc6	menstruace
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgInPc4d1	ostatní
minerály	minerál	k1gInPc4	minerál
<g/>
,	,	kIx,	,
i	i	k8xC	i
železo	železo	k1gNnSc4	železo
rostlina	rostlina	k1gFnSc1	rostlina
přijímá	přijímat	k5eAaImIp3nS	přijímat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
z	z	k7c2	z
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Fe	Fe	k?	Fe
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
ionty	ion	k1gInPc7	ion
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
jsou	být	k5eAaImIp3nP	být
obtížně	obtížně	k6eAd1	obtížně
rozpustné	rozpustný	k2eAgFnPc1d1	rozpustná
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
rostlina	rostlina	k1gFnSc1	rostlina
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
organické	organický	k2eAgFnSc2d1	organická
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
s	s	k7c7	s
železem	železo	k1gNnSc7	železo
reagují	reagovat	k5eAaBmIp3nP	reagovat
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
cheláty	chelát	k1gInPc1	chelát
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
cheláty	chelát	k1gInPc1	chelát
se	se	k3xPyFc4	se
vážou	vázat	k5eAaImIp3nP	vázat
na	na	k7c4	na
membránu	membrána	k1gFnSc4	membrána
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
železo	železo	k1gNnSc1	železo
redukuje	redukovat	k5eAaBmIp3nS	redukovat
z	z	k7c2	z
Fe	Fe	k1gFnSc2	Fe
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
na	na	k7c6	na
Fe	Fe	k1gFnSc6	Fe
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
Fe	Fe	k1gFnSc1	Fe
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
chelátreduktasou	chelátreduktasa	k1gFnSc7	chelátreduktasa
<g/>
.	.	kIx.	.
</s>
<s>
Fe	Fe	k?	Fe
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
pak	pak	k6eAd1	pak
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lipnicovitých	lipnicovitý	k2eAgFnPc2d1	lipnicovitý
(	(	kIx(	(
<g/>
trav	tráva	k1gFnPc2	tráva
<g/>
,	,	kIx,	,
Poaceae	Poaceae	k1gFnSc1	Poaceae
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vylučované	vylučovaný	k2eAgFnPc1d1	vylučovaná
látky	látka	k1gFnPc1	látka
nazývají	nazývat	k5eAaImIp3nP	nazývat
fytosiderofory	fytosiderofor	k1gInPc4	fytosiderofor
a	a	k8xC	a
skrz	skrz	k7c4	skrz
membránu	membrána	k1gFnSc4	membrána
procházejí	procházet	k5eAaImIp3nP	procházet
neredukované	redukovaný	k2eNgFnPc1d1	neredukovaná
Fe	Fe	k1gFnPc1	Fe
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
ionty	ion	k1gInPc7	ion
<g/>
.	.	kIx.	.
</s>
<s>
Fytosiderofory	Fytosiderofora	k1gFnPc1	Fytosiderofora
jsou	být	k5eAaImIp3nP	být
případem	případ	k1gInSc7	případ
fytometaloforů	fytometalofor	k1gMnPc2	fytometalofor
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
takto	takto	k6eAd1	takto
lze	lze	k6eAd1	lze
přijímat	přijímat	k5eAaImF	přijímat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
kovy	kov	k1gInPc4	kov
<g/>
.	.	kIx.	.
</s>
<s>
Železo	železo	k1gNnSc1	železo
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
transportuje	transportovat	k5eAaBmIp3nS	transportovat
xylémem	xylém	k1gInSc7	xylém
<g/>
.	.	kIx.	.
</s>
<s>
Skladováno	skladován	k2eAgNnSc1d1	skladováno
je	být	k5eAaImIp3nS	být
ve	v	k7c4	v
fytoferitinu	fytoferitina	k1gFnSc4	fytoferitina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
proteinový	proteinový	k2eAgInSc4d1	proteinový
obal	obal	k1gInSc4	obal
<g/>
,	,	kIx,	,
obalující	obalující	k2eAgInSc4d1	obalující
5000	[number]	k4	5000
<g/>
-	-	kIx~	-
<g/>
6000	[number]	k4	6000
iontů	ion	k1gInPc2	ion
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
železa	železo	k1gNnSc2	železo
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
světlé	světlý	k2eAgFnPc4d1	světlá
skvrny	skvrna	k1gFnPc4	skvrna
na	na	k7c6	na
listech	list	k1gInPc6	list
<g/>
,	,	kIx,	,
přebytek	přebytek	k1gInSc1	přebytek
jejich	jejich	k3xOp3gInSc4	jejich
bronzový	bronzový	k2eAgInSc4d1	bronzový
nádech	nádech	k1gInSc4	nádech
<g/>
.	.	kIx.	.
</s>
