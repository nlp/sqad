<p>
<s>
Lemur	lemur	k1gMnSc1	lemur
rudočelý	rudočelý	k2eAgMnSc1d1	rudočelý
(	(	kIx(	(
<g/>
Eulemur	Eulemur	k1gMnSc1	Eulemur
rufifrons	rufifrons	k6eAd1	rufifrons
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gMnSc1	druh
lemura	lemur	k1gMnSc2	lemur
žijící	žijící	k2eAgFnSc2d1	žijící
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgMnSc1	tento
lemur	lemur	k1gMnSc1	lemur
má	mít	k5eAaImIp3nS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
komplikovanou	komplikovaný	k2eAgFnSc4d1	komplikovaná
taxonomickou	taxonomický	k2eAgFnSc4d1	Taxonomická
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
byl	být	k5eAaImAgInS	být
veden	vést	k5eAaImNgInS	vést
jako	jako	k9	jako
poddruh	poddruh	k1gInSc1	poddruh
lemura	lemur	k1gMnSc2	lemur
bělohlavého	bělohlavý	k2eAgMnSc2d1	bělohlavý
(	(	kIx(	(
<g/>
Eulemur	Eulemur	k1gMnSc1	Eulemur
fulvus	fulvus	k1gMnSc1	fulvus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgMnS	být
E.	E.	kA	E.
fulvus	fulvus	k1gMnSc1	fulvus
rozdělen	rozdělit	k5eAaPmNgMnS	rozdělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jeden	jeden	k4xCgMnSc1	jeden
byl	být	k5eAaImAgMnS	být
lemur	lemur	k1gMnSc1	lemur
červenavý	červenavý	k2eAgMnSc1d1	červenavý
(	(	kIx(	(
<g/>
E.	E.	kA	E.
rufus	rufus	k1gInSc1	rufus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zkoumání	zkoumání	k1gNnSc2	zkoumání
publikovaného	publikovaný	k2eAgInSc2d1	publikovaný
r.	r.	kA	r.
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
lemura	lemur	k1gMnSc4	lemur
rudočelého	rudočelý	k2eAgNnSc2d1	rudočelý
(	(	kIx(	(
<g/>
E.	E.	kA	E.
rufifrons	rufifrons	k1gInSc1	rufifrons
<g/>
)	)	kIx)	)
a	a	k8xC	a
lemura	lemur	k1gMnSc2	lemur
červenavého	červenavý	k2eAgMnSc2d1	červenavý
(	(	kIx(	(
<g/>
E.	E.	kA	E.
rufus	rufus	k1gInSc1	rufus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
odštěpení	odštěpení	k1gNnSc3	odštěpení
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
základě	základ	k1gInSc6	základ
genetické	genetický	k2eAgFnPc4d1	genetická
a	a	k8xC	a
morfologické	morfologický	k2eAgFnPc4d1	morfologická
odlišnosti	odlišnost	k1gFnPc4	odlišnost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
zkoumání	zkoumání	k1gNnPc1	zkoumání
dále	daleko	k6eAd2	daleko
zjistila	zjistit	k5eAaPmAgNnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lemur	lemur	k1gMnSc1	lemur
rudočelý	rudočelý	k2eAgMnSc1d1	rudočelý
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
příbuzný	příbuzný	k2eAgMnSc1d1	příbuzný
lemurovi	lemur	k1gMnSc3	lemur
běločelému	běločelý	k2eAgInSc3d1	běločelý
(	(	kIx(	(
<g/>
E.	E.	kA	E.
albifrons	albifrons	k1gInSc1	albifrons
<g/>
)	)	kIx)	)
a	a	k8xC	a
lemurovi	lemurův	k2eAgMnPc1d1	lemurův
Sanfordovu	Sanfordův	k2eAgFnSc4d1	Sanfordův
(	(	kIx(	(
<g/>
E.	E.	kA	E.
sanfordi	sanford	k1gMnPc1	sanford
<g/>
)	)	kIx)	)
než	než	k8xS	než
lemurovi	lemur	k1gMnSc3	lemur
červenavému	červenavý	k2eAgMnSc3d1	červenavý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Lemur	lemur	k1gMnSc1	lemur
rudočelý	rudočelý	k2eAgMnSc1d1	rudočelý
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
jihozápadě	jihozápad	k1gInSc6	jihozápad
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Tsiribihina	Tsiribihino	k1gNnSc2	Tsiribihino
a	a	k8xC	a
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
v	v	k7c6	v
pásu	pás	k1gInSc6	pás
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Mangoro	Mangora	k1gFnSc5	Mangora
a	a	k8xC	a
Onive	Oniev	k1gFnSc2	Oniev
k	k	k7c3	k
Andringitra	Andringitr	k1gMnSc4	Andringitr
Massif	Massif	k1gInSc4	Massif
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
biotopem	biotop	k1gInSc7	biotop
na	na	k7c6	na
západě	západ	k1gInSc6	západ
jsou	být	k5eAaImIp3nP	být
suché	suchý	k2eAgInPc1d1	suchý
nížinné	nížinný	k2eAgInPc1d1	nížinný
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
pak	pak	k6eAd1	pak
vlhké	vlhký	k2eAgInPc1d1	vlhký
tropické	tropický	k2eAgInPc1d1	tropický
lesy	les	k1gInPc1	les
středních	střední	k2eAgFnPc2d1	střední
a	a	k8xC	a
vyšších	vysoký	k2eAgFnPc2d2	vyšší
nadmořských	nadmořský	k2eAgFnPc2d1	nadmořská
výšek	výška	k1gFnPc2	výška
(	(	kIx(	(
<g/>
až	až	k9	až
do	do	k7c2	do
1670	[number]	k4	1670
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
měří	měřit	k5eAaImIp3nS	měřit
35	[number]	k4	35
až	až	k9	až
48	[number]	k4	48
cm	cm	kA	cm
a	a	k8xC	a
ocas	ocas	k1gInSc1	ocas
45	[number]	k4	45
až	až	k9	až
55	[number]	k4	55
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Váží	vážit	k5eAaImIp3nS	vážit
mezi	mezi	k7c7	mezi
2,2	[number]	k4	2,2
až	až	k9	až
2,3	[number]	k4	2,3
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
srsti	srst	k1gFnSc2	srst
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
šedé	šedý	k2eAgNnSc1d1	šedé
s	s	k7c7	s
černou	černý	k2eAgFnSc7d1	černá
barvou	barva	k1gFnSc7	barva
na	na	k7c6	na
obličeji	obličej	k1gInSc6	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
očí	oko	k1gNnPc2	oko
je	být	k5eAaImIp3nS	být
srst	srst	k1gFnSc1	srst
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
mají	mít	k5eAaImIp3nP	mít
bílé	bílý	k2eAgNnSc1d1	bílé
zbarvení	zbarvení	k1gNnSc4	zbarvení
výrazné	výrazný	k2eAgNnSc4d1	výrazné
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
jasně	jasně	k6eAd1	jasně
patrné	patrný	k2eAgInPc1d1	patrný
vousy	vous	k1gInPc1	vous
<g/>
,	,	kIx,	,
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
je	být	k5eAaImIp3nS	být
tatáž	týž	k3xTgFnSc1	týž
oblast	oblast	k1gFnSc1	oblast
spíše	spíše	k9	spíše
krémová	krémový	k2eAgFnSc1d1	krémová
nebo	nebo	k8xC	nebo
nazrzlá	nazrzlý	k2eAgFnSc1d1	nazrzlá
<g/>
,	,	kIx,	,
vousy	vous	k1gInPc7	vous
málo	málo	k6eAd1	málo
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
.	.	kIx.	.
</s>
<s>
Temeno	temeno	k1gNnSc1	temeno
je	být	k5eAaImIp3nS	být
zrzavé	zrzavý	k2eAgNnSc1d1	zrzavé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
<g/>
,	,	kIx,	,
chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
chováním	chování	k1gNnSc7	chování
populací	populace	k1gFnPc2	populace
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
východě	východ	k1gInSc6	východ
panují	panovat	k5eAaImIp3nP	panovat
značné	značný	k2eAgInPc4d1	značný
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnPc1d1	západní
populace	populace	k1gFnPc1	populace
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c6	na
menších	malý	k2eAgNnPc6d2	menší
územích	území	k1gNnPc6	území
a	a	k8xC	a
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
populační	populační	k2eAgFnSc4d1	populační
hustotu	hustota	k1gFnSc4	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnPc1d1	východní
populace	populace	k1gFnPc1	populace
obývají	obývat	k5eAaImIp3nP	obývat
území	území	k1gNnSc4	území
až	až	k9	až
o	o	k7c6	o
100	[number]	k4	100
hektarech	hektar	k1gInPc6	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Lemuři	lemur	k1gMnPc1	lemur
rodočelí	rodočelý	k2eAgMnPc1d1	rodočelý
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
čítajících	čítající	k2eAgFnPc2d1	čítající
4	[number]	k4	4
až	až	k9	až
18	[number]	k4	18
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
západní	západní	k2eAgFnPc1d1	západní
populace	populace	k1gFnPc1	populace
bývají	bývat	k5eAaImIp3nP	bývat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
o	o	k7c4	o
jednoho	jeden	k4xCgMnSc4	jeden
člena	člen	k1gMnSc4	člen
větší	veliký	k2eAgFnSc1d2	veliký
(	(	kIx(	(
<g/>
9	[number]	k4	9
ku	k	k7c3	k
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skupinové	skupinový	k2eAgNnSc1d1	skupinové
chování	chování	k1gNnSc1	chování
nevykazuje	vykazovat	k5eNaImIp3nS	vykazovat
žádnou	žádný	k3yNgFnSc4	žádný
striktní	striktní	k2eAgFnSc4d1	striktní
hierarchii	hierarchie	k1gFnSc4	hierarchie
a	a	k8xC	a
vnitroskupinová	vnitroskupinový	k2eAgFnSc1d1	vnitroskupinový
agresivita	agresivita	k1gFnSc1	agresivita
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
o	o	k7c4	o
denní	denní	k2eAgMnPc4d1	denní
tvory	tvor	k1gMnPc4	tvor
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
západní	západní	k2eAgFnSc1d1	západní
populace	populace	k1gFnSc1	populace
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
uchylovat	uchylovat	k5eAaImF	uchylovat
i	i	k9	i
k	k	k7c3	k
nočnímu	noční	k2eAgInSc3d1	noční
způsobu	způsob	k1gInSc3	způsob
života	život	k1gInSc2	život
během	během	k7c2	během
suchých	suchý	k2eAgNnPc2d1	suché
období	období	k1gNnPc2	období
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc1d1	východní
populace	populace	k1gFnSc1	populace
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
===	===	k?	===
</s>
</p>
<p>
<s>
Potrava	potrava	k1gFnSc1	potrava
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
semena	semeno	k1gNnPc4	semeno
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnPc4	ovoce
<g/>
,	,	kIx,	,
nektar	nektar	k1gInSc4	nektar
a	a	k8xC	a
květy	květ	k1gInPc4	květ
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnPc1d1	západní
populace	populace	k1gFnPc1	populace
jí	on	k3xPp3gFnSc3	on
méně	málo	k6eAd2	málo
pestrou	pestrý	k2eAgFnSc4d1	pestrá
stravu	strava	k1gFnSc4	strava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
výrazně	výrazně	k6eAd1	výrazně
dominují	dominovat	k5eAaImIp3nP	dominovat
listy	list	k1gInPc1	list
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
===	===	k?	===
</s>
</p>
<p>
<s>
Rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
je	být	k5eAaImIp3nS	být
sezónní	sezónní	k2eAgFnSc7d1	sezónní
záležitostí	záležitost	k1gFnSc7	záležitost
<g/>
,	,	kIx,	,
k	k	k7c3	k
páření	páření	k1gNnSc3	páření
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
<g/>
,	,	kIx,	,
k	k	k7c3	k
narození	narození	k1gNnSc3	narození
mláďat	mládě	k1gNnPc2	mládě
v	v	k7c6	v
září	září	k1gNnSc6	září
až	až	k8xS	až
říjnu	říjen	k1gInSc6	říjen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
populacích	populace	k1gFnPc6	populace
si	se	k3xPyFc3	se
jeden	jeden	k4xCgMnSc1	jeden
samec	samec	k1gMnSc1	samec
přivlastňuje	přivlastňovat	k5eAaImIp3nS	přivlastňovat
všechny	všechen	k3xTgFnPc4	všechen
samice	samice	k1gFnPc4	samice
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
nedochází	docházet	k5eNaImIp3nS	docházet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hrozby	hrozba	k1gFnPc1	hrozba
<g/>
,	,	kIx,	,
ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
hrozbou	hrozba	k1gFnSc7	hrozba
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
je	být	k5eAaImIp3nS	být
ztráta	ztráta	k1gFnSc1	ztráta
biotopu	biotop	k1gInSc2	biotop
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vypalování	vypalování	k1gNnSc2	vypalování
a	a	k8xC	a
těžby	těžba	k1gFnSc2	těžba
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
je	být	k5eAaImIp3nS	být
lov	lov	k1gInSc4	lov
na	na	k7c4	na
jídlo	jídlo	k1gNnSc4	jídlo
-	-	kIx~	-
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejčastěji	často	k6eAd3	často
lovené	lovený	k2eAgMnPc4d1	lovený
lemury	lemur	k1gMnPc4	lemur
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
<g/>
.	.	kIx.	.
<g/>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
ochranným	ochranný	k2eAgNnSc7d1	ochranné
opatřením	opatření	k1gNnSc7	opatření
je	být	k5eAaImIp3nS	být
zakládání	zakládání	k1gNnSc1	zakládání
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Lemuři	lemur	k1gMnPc1	lemur
rudočelí	rudočelý	k2eAgMnPc1d1	rudočelý
jsou	být	k5eAaImIp3nP	být
Mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
svazem	svaz	k1gInSc7	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
vedeni	vést	k5eAaImNgMnP	vést
jako	jako	k9	jako
taxon	taxon	k1gInSc4	taxon
téměř	téměř	k6eAd1	téměř
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
a	a	k8xC	a
v	v	k7c6	v
úmluvě	úmluva	k1gFnSc6	úmluva
o	o	k7c6	o
obchodu	obchod	k1gInSc6	obchod
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
CITES	CITES	kA	CITES
jsou	být	k5eAaImIp3nP	být
zahrnuti	zahrnut	k2eAgMnPc1d1	zahrnut
do	do	k7c2	do
nejpřísnější	přísný	k2eAgFnSc2d3	nejpřísnější
kategorie	kategorie	k1gFnSc2	kategorie
Appendix	Appendix	k1gInSc1	Appendix
I.	I.	kA	I.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Eulemur	Eulemura	k1gFnPc2	Eulemura
rufifrons	rufifrons	k6eAd1	rufifrons
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lemur	lemur	k1gMnSc1	lemur
rudočelý	rudočelý	k2eAgMnSc1d1	rudočelý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
