<s>
Pamětní	pamětní	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
krále	král	k1gMnSc2
Haakona	Haakon	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
krále	král	k1gMnSc2
Haakona	Haakon	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
Kong	Kongo	k1gNnPc2
Haakon	Haakona	k1gFnPc2
VIIs	VIIsa	k1gFnPc2
minnemedalje	minnemedalj	k1gFnSc2
</s>
<s>
Uděluje	udělovat	k5eAaImIp3nS
Norský	norský	k2eAgMnSc1d1
král	král	k1gMnSc1
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
pamětní	pamětní	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
</s>
<s>
Založeno	založen	k2eAgNnSc1d1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1957	#num#	k4
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Norsko	Norsko	k1gNnSc1
Norsko	Norsko	k1gNnSc1
</s>
<s>
Uděluje	udělovat	k5eAaImIp3nS
se	se	k3xPyFc4
za	za	k7c4
</s>
<s>
podíl	podíl	k1gInSc1
na	na	k7c4
organizace	organizace	k1gFnPc4
pohřbu	pohřeb	k1gInSc2
krále	král	k1gMnSc2
Haakona	Haakon	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
Status	status	k1gInSc1
</s>
<s>
nadále	nadále	k6eAd1
neudílena	udílen	k2eNgFnSc1d1
</s>
<s>
Ostatní	ostatní	k2eAgNnSc1d1
vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2
</s>
<s>
Medaile	medaile	k1gFnSc1
stého	stý	k4xOgNnSc2
výročí	výročí	k1gNnSc2
královské	královský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2
</s>
<s>
Jubilejní	jubilejní	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
krále	král	k1gMnSc2
Haakona	Haakon	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
1905	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
</s>
<s>
Tomuto	tento	k3xDgInSc3
článku	článek	k1gInSc3
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
obrázky	obrázek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Víte	vědět	k5eAaImIp2nP
<g/>
-li	-li	k?
o	o	k7c6
nějakých	nějaký	k3yIgFnPc6
svobodně	svobodně	k6eAd1
šiřitelných	šiřitelný	k2eAgFnPc6d1
<g/>
,	,	kIx,
neváhejte	váhat	k5eNaImRp2nP
je	on	k3xPp3gMnPc4
načíst	načíst	k5eAaPmF,k5eAaBmF
a	a	k8xC
přidat	přidat	k5eAaPmF
do	do	k7c2
článku	článek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
rychlejší	rychlý	k2eAgNnSc4d2
přidání	přidání	k1gNnSc4
obrázku	obrázek	k1gInSc2
můžete	moct	k5eAaImIp2nP
přidat	přidat	k5eAaPmF
žádost	žádost	k1gFnSc4
i	i	k9
sem	sem	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
WikiProjekt	WikiProjekt	k1gInSc1
Fotografování	fotografování	k1gNnSc2
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
krále	král	k1gMnSc2
Haakona	Haakon	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
norsky	norsky	k6eAd1
Kong	Kongo	k1gNnPc2
Haakon	Haakona	k1gFnPc2
VIIs	VIIsa	k1gFnPc2
minnemedalje	minnemedalj	k1gFnSc2
<g/>
)	)	kIx)
či	či	k8xC
Pamětní	pamětní	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
krále	král	k1gMnSc2
Haakona	Haakon	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1957	#num#	k4
(	(	kIx(
<g/>
norsky	norsky	k6eAd1
Kong	Kongo	k1gNnPc2
Haakon	Haakon	k1gInSc4
VIIs	VIIs	k1gInSc1
minnemedalje	minnemedalje	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
oktober	oktober	k1gInSc1
1957	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
norská	norský	k2eAgFnSc1d1
pamětní	pamětní	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c4
památku	památka	k1gFnSc4
úmrtí	úmrtí	k1gNnSc2
panovníka	panovník	k1gMnSc2
Haakona	Haakon	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
a	a	k8xC
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
pohřbu	pohřeb	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohřeb	pohřeb	k1gInSc1
krále	král	k1gMnSc4
proběhl	proběhnout	k5eAaPmAgInS
dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1957	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnPc1
a	a	k8xC
pravidla	pravidlo	k1gNnPc1
udílení	udílení	k1gNnSc1
</s>
<s>
Medaile	medaile	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1957	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pamětní	pamětní	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
byla	být	k5eAaImAgFnS
udělena	udělit	k5eAaPmNgFnS
osobám	osoba	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
plnily	plnit	k5eAaImAgFnP
povinnosti	povinnost	k1gFnPc4
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
organizací	organizace	k1gFnSc7
králova	králův	k2eAgInSc2d1
pohřbu	pohřeb	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Zlatá	zlatý	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
byla	být	k5eAaImAgFnS
udělena	udělit	k5eAaPmNgFnS
26	#num#	k4
lidem	lid	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
většinou	většina	k1gFnSc7
příslušníkům	příslušník	k1gMnPc3
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stříbrnou	stříbrný	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
obdrželo	obdržet	k5eAaPmAgNnS
54	#num#	k4
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
48	#num#	k4
důstojníků	důstojník	k1gMnPc2
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
námořnictva	námořnictvo	k1gNnSc2
a	a	k8xC
letectva	letectvo	k1gNnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
během	během	k7c2
pohřbu	pohřeb	k1gInSc2
pochodovali	pochodovat	k5eAaImAgMnP
v	v	k7c6
čele	čelo	k1gNnSc6
čestných	čestný	k2eAgInPc2d1
oddílů	oddíl	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
medaile	medaile	k1gFnSc2
</s>
<s>
Medaile	medaile	k1gFnSc1
je	být	k5eAaImIp3nS
vyrobena	vyrobit	k5eAaPmNgFnS
ze	z	k7c2
zlata	zlato	k1gNnSc2
nebo	nebo	k8xC
stříbra	stříbro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
přední	přední	k2eAgFnSc6d1
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
podobizna	podobizna	k1gFnSc1
krále	král	k1gMnSc2
Haakona	Haakon	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
je	být	k5eAaImIp3nS
vyobrazen	vyobrazit	k5eAaPmNgMnS
bez	bez	k7c2
koruny	koruna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autorem	autor	k1gMnSc7
výjevu	výjev	k1gInSc2
je	být	k5eAaImIp3nS
rytec	rytec	k1gMnSc1
Ivar	Ivar	k1gMnSc1
Throndsen	Throndsno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
portrétu	portrét	k1gInSc2
je	být	k5eAaImIp3nS
nápis	nápis	k1gInSc1
HAAKON	HAAKON	kA
•	•	k?
VII	VII	kA
•	•	k?
NORGES	NORGES	kA
•	•	k?
KONGE	KONGE	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
zadní	zadní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
královský	královský	k2eAgInSc4d1
monogram	monogram	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
stuze	stuha	k1gFnSc3
je	být	k5eAaImIp3nS
medaile	medaile	k1gFnSc1
připojena	připojen	k2eAgFnSc1d1
pomocí	pomocí	k7c2
přechodového	přechodový	k2eAgInSc2d1
prvku	prvek	k1gInSc2
ve	v	k7c6
tvaru	tvar	k1gInSc6
královské	královský	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stuha	stuha	k1gFnSc1
je	být	k5eAaImIp3nS
červená	červený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
stuze	stuha	k1gFnSc3
je	být	k5eAaImIp3nS
připojena	připojit	k5eAaPmNgFnS
stříbrná	stříbrný	k2eAgFnSc1d1
spona	spona	k1gFnSc1
s	s	k7c7
nápisem	nápis	k1gInSc7
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OKTOBER	OKTOBER	kA
1957	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
King	King	k1gMnSc1
Haakon	Haakon	k1gMnSc1
VII	VII	kA
Commemorative	Commemorativ	k1gInSc5
Medal	Medal	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
King	King	k1gMnSc1
Haakon	Haakon	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Last	Last	k1gInSc1
Journey	Journey	k1gInPc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Royal	Royal	k1gMnSc1
Funeral	Funeral	k1gMnSc1
in	in	k?
Norway	Norwaa	k1gFnSc2
<g/>
.	.	kIx.
1957	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Illustrated	Illustrated	k1gMnSc1
London	London	k1gMnSc1
News	News	k1gInSc4
231	#num#	k4
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
598	#num#	k4
<g/>
↑	↑	k?
King	King	k1gMnSc1
Haakon	Haakon	k1gMnSc1
VII	VII	kA
Commemorative	Commemorativ	k1gInSc5
Medal	Medal	k1gInSc1
<g/>
.	.	kIx.
wawards	wawards	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
H.	H.	kA
M.	M.	kA
Kong	Kongo	k1gNnPc2
Haakon	Haakon	k1gMnSc1
VII	VII	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
erindringsmedalje	erindringsmedalje	k1gFnSc1
med	med	k1gInSc4
spenne	spennout	k5eAaPmIp3nS,k5eAaImIp3nS
1	#num#	k4
<g/>
.	.	kIx.
oktober	oktober	k1gInSc1
1957	#num#	k4
<g/>
.	.	kIx.
1960	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nordisk	Nordisk	k1gInSc1
Numismatisk	Numismatisk	k1gInSc4
Unions	Unions	k1gInSc1
Medlemsblad	Medlemsblad	k1gInSc1
10	#num#	k4
<g/>
:	:	kIx,
1991	#num#	k4
2	#num#	k4
3	#num#	k4
Hallberg	Hallberg	k1gMnSc1
<g/>
,	,	kIx,
Harald	Harald	k1gMnSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Norske	Norske	k1gNnSc7
dekorasjoner	dekorasjonra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tildelt	Tildelt	k2eAgInSc4d1
med	med	k1gInSc4
bå	bå	k1gInSc1
til	til	k1gInSc1
å	å	k?
bæ	bæ	k1gInSc1
på	på	k?
uniform	uniform	k1gInSc1
eller	eller	k1gMnSc1
sivilt	sivilt	k1gMnSc1
antrekk	antrekk	k1gMnSc1
<g/>
.	.	kIx.
Å	Å	k1gMnSc1
<g/>
:	:	kIx,
Dreyer	Dreyer	k1gInSc1
bok	bok	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
237	#num#	k4
<g/>
–	–	k?
<g/>
239	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ODM	ODM	kA
of	of	k?
Norway	Norwaa	k1gMnSc2
<g/>
:	:	kIx,
King	King	k1gMnSc1
Haakon	Haakon	k1gMnSc1
VII	VII	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Commemorative	Commemorativ	k1gInSc5
Medal	Medal	k1gMnSc1
<g/>
.	.	kIx.
www.medals.org.uk	www.medals.org.uk	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Monarchie	monarchie	k1gFnSc1
</s>
