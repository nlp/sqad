<s>
Pamětní	pamětní	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
krále	král	k1gMnSc2
Haakona	Haakon	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
norsky	norsky	k6eAd1
Kong	Kongo	k1gNnPc2
Haakon	Haakona	k1gFnPc2
VIIs	VIIsa	k1gFnPc2
minnemedalje	minnemedalj	k1gFnSc2
<g/>
)	)	kIx)
či	či	k8xC
Pamětní	pamětní	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
krále	král	k1gMnSc2
Haakona	Haakon	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1957	#num#	k4
(	(	kIx(
<g/>
norsky	norsky	k6eAd1
Kong	Kongo	k1gNnPc2
Haakon	Haakon	k1gInSc4
VIIs	VIIs	k1gInSc1
minnemedalje	minnemedalje	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
oktober	oktober	k1gInSc1
1957	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
norská	norský	k2eAgFnSc1d1
pamětní	pamětní	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c4
památku	památka	k1gFnSc4
úmrtí	úmrtí	k1gNnSc2
panovníka	panovník	k1gMnSc2
Haakona	Haakon	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
a	a	k8xC
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
pohřbu	pohřeb	k1gInSc2
<g/>
.	.	kIx.
</s>