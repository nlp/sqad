<s>
Hannibal	Hannibal	k1gInSc4	Hannibal
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Římané	Říman	k1gMnPc1	Říman
mohou	moct	k5eAaImIp3nP	moct
napadat	napadat	k5eAaImF	napadat
a	a	k8xC	a
ničit	ničit	k5eAaImF	ničit
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
kusy	kus	k1gInPc4	kus
kartaginské	kartaginský	k2eAgFnSc2d1	kartaginská
říše	říš	k1gFnSc2	říš
odděleně	odděleně	k6eAd1	odděleně
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
on	on	k3xPp3gMnSc1	on
mohl	moct	k5eAaImAgMnS	moct
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
dostatečně	dostatečně	k6eAd1	dostatečně
efektivně	efektivně	k6eAd1	efektivně
přesouvat	přesouvat	k5eAaImF	přesouvat
vojska	vojsko	k1gNnPc4	vojsko
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
hrozícím	hrozící	k2eAgInPc3d1	hrozící
vpádům	vpád	k1gInPc3	vpád
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Kartága	Kartágo	k1gNnSc2	Kartágo
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
předejít	předejít	k5eAaPmF	předejít
vlastním	vlastní	k2eAgNnSc7d1	vlastní
tažením	tažení	k1gNnSc7	tažení
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
uskutečněným	uskutečněný	k2eAgNnSc7d1	uskutečněné
vnitrozemím	vnitrozemí	k1gNnSc7	vnitrozemí
a	a	k8xC	a
završeným	završený	k2eAgInSc7d1	završený
přechodem	přechod	k1gInSc7	přechod
Alp	Alpy	k1gFnPc2	Alpy
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
hodlal	hodlat	k5eAaImAgMnS	hodlat
Římany	Říman	k1gMnPc4	Říman
překvapit	překvapit	k5eAaPmF	překvapit
<g/>
.	.	kIx.	.
</s>
