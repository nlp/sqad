<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
punská	punský	k2eAgFnSc1d1	punská
válka	válka	k1gFnSc1	válka
byla	být	k5eAaImAgFnS	být
vybojována	vybojovat	k5eAaPmNgFnS	vybojovat
mezi	mezi	k7c7	mezi
Římany	Říman	k1gMnPc7	Říman
a	a	k8xC	a
Kartaginci	Kartaginec	k1gMnPc7	Kartaginec
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
218	[number]	k4	218
až	až	k9	až
201	[number]	k4	201
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Kartaginský	kartaginský	k2eAgMnSc1d1	kartaginský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Hannibal	Hannibal	k1gInSc4	Hannibal
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Barkasů	Barkas	k1gInPc2	Barkas
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nejprve	nejprve	k6eAd1	nejprve
Římu	Řím	k1gInSc3	Řím
uštědřil	uštědřit	k5eAaPmAgMnS	uštědřit
řadu	řada	k1gFnSc4	řada
porážek	porážka	k1gFnPc2	porážka
v	v	k7c6	v
takticky	takticky	k6eAd1	takticky
geniálně	geniálně	k6eAd1	geniálně
vedených	vedený	k2eAgFnPc6d1	vedená
bitvách	bitva	k1gFnPc6	bitva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
uváděny	uvádět	k5eAaImNgInP	uvádět
ve	v	k7c6	v
vojenských	vojenský	k2eAgFnPc6d1	vojenská
učebnicích	učebnice	k1gFnPc6	učebnice
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
následně	následně	k6eAd1	následně
přešli	přejít	k5eAaPmAgMnP	přejít
do	do	k7c2	do
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
trvající	trvající	k2eAgFnSc2d1	trvající
opotřebovávací	opotřebovávací	k2eAgFnSc2d1	opotřebovávací
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
zlikvidovali	zlikvidovat	k5eAaPmAgMnP	zlikvidovat
nebo	nebo	k8xC	nebo
neutralizovali	neutralizovat	k5eAaBmAgMnP	neutralizovat
spojence	spojenec	k1gMnSc4	spojenec
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc1d1	hlavní
kolonie	kolonie	k1gFnSc1	kolonie
Kartága	Kartágo	k1gNnSc2	Kartágo
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Publia	Publius	k1gMnSc2	Publius
Cornelia	Cornelius	k1gMnSc2	Cornelius
Scipiona	Scipion	k1gMnSc2	Scipion
Africana	African	k1gMnSc2	African
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Zamy	Zama	k1gFnSc2	Zama
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
válka	válka	k1gFnSc1	válka
definitivně	definitivně	k6eAd1	definitivně
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
boj	boj	k1gInSc1	boj
obou	dva	k4xCgNnPc2	dva
měst	město	k1gNnPc2	město
o	o	k7c6	o
dominanci	dominance	k1gFnSc6	dominance
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
úplného	úplný	k2eAgNnSc2d1	úplné
zničení	zničení	k1gNnSc2	zničení
Kartága	Kartágo	k1gNnSc2	Kartágo
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
punské	punský	k2eAgFnSc6d1	punská
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
146	[number]	k4	146
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
hegemonie	hegemonie	k1gFnSc2	hegemonie
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
se	se	k3xPyFc4	se
nedochovaly	dochovat	k5eNaPmAgInP	dochovat
žádné	žádný	k3yNgInPc1	žádný
historické	historický	k2eAgInPc1d1	historický
prameny	pramen	k1gInPc1	pramen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
popisovaly	popisovat	k5eAaImAgInP	popisovat
průběh	průběh	k1gInSc4	průběh
války	válka	k1gFnSc2	válka
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
pozadí	pozadí	k1gNnSc4	pozadí
z	z	k7c2	z
kartaginského	kartaginský	k2eAgMnSc2d1	kartaginský
nebo	nebo	k8xC	nebo
skutečně	skutečně	k6eAd1	skutečně
neutrálního	neutrální	k2eAgInSc2d1	neutrální
pohledu	pohled	k1gInSc2	pohled
<g/>
.	.	kIx.	.
</s>
<s>
Historikové	historik	k1gMnPc1	historik
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
mohou	moct	k5eAaImIp3nP	moct
opírat	opírat	k5eAaImF	opírat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
díla	dílo	k1gNnPc4	dílo
řeckých	řecký	k2eAgMnPc2d1	řecký
a	a	k8xC	a
římských	římský	k2eAgMnPc2d1	římský
antických	antický	k2eAgMnPc2d1	antický
autorů	autor	k1gMnPc2	autor
–	–	k?	–
především	především	k6eAd1	především
Polybia	Polybium	k1gNnSc2	Polybium
a	a	k8xC	a
Livia	Livium	k1gNnSc2	Livium
–	–	k?	–
a	a	k8xC	a
musejí	muset	k5eAaImIp3nP	muset
je	on	k3xPp3gInPc4	on
tudíž	tudíž	k8xC	tudíž
interpretovat	interpretovat	k5eAaBmF	interpretovat
velmi	velmi	k6eAd1	velmi
opatrně	opatrně	k6eAd1	opatrně
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
porážky	porážka	k1gFnPc1	porážka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
Římané	Říman	k1gMnPc1	Říman
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
jimi	on	k3xPp3gMnPc7	on
vždy	vždy	k6eAd1	vždy
vykládány	vykládán	k2eAgMnPc4d1	vykládán
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nijak	nijak	k6eAd1	nijak
nezpochybňovaly	zpochybňovat	k5eNaImAgFnP	zpochybňovat
politické	politický	k2eAgFnPc1d1	politická
a	a	k8xC	a
sociální	sociální	k2eAgNnSc1d1	sociální
uspořádání	uspořádání	k1gNnSc1	uspořádání
římského	římský	k2eAgInSc2d1	římský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
při	při	k7c6	při
popisování	popisování	k1gNnSc6	popisování
katastrofálních	katastrofální	k2eAgFnPc2d1	katastrofální
proher	prohra	k1gFnPc2	prohra
na	na	k7c6	na
bitevním	bitevní	k2eAgNnSc6d1	bitevní
poli	pole	k1gNnSc6	pole
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
velikost	velikost	k1gFnSc1	velikost
Říma	Řím	k1gInSc2	Řím
vždy	vždy	k6eAd1	vždy
zachována	zachovat	k5eAaPmNgFnS	zachovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
určitý	určitý	k2eAgInSc1d1	určitý
obětní	obětní	k2eAgInSc1d1	obětní
beránek	beránek	k1gInSc1	beránek
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgNnSc3	jenž
byla	být	k5eAaImAgFnS	být
vina	vina	k1gFnSc1	vina
za	za	k7c4	za
daný	daný	k2eAgInSc4d1	daný
neúspěch	neúspěch	k1gInSc4	neúspěch
připsána	připsán	k2eAgFnSc1d1	připsána
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
to	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
ničivou	ničivý	k2eAgFnSc4d1	ničivá
římskou	římský	k2eAgFnSc4d1	římská
porážku	porážka	k1gFnSc4	porážka
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kann	Kanny	k1gFnPc2	Kanny
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vypuknutí	vypuknutí	k1gNnPc4	vypuknutí
války	válka	k1gFnSc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
první	první	k4xOgFnSc2	první
punské	punský	k2eAgFnSc2d1	punská
války	válka	k1gFnSc2	válka
muselo	muset	k5eAaImAgNnS	muset
Kartágo	Kartágo	k1gNnSc4	Kartágo
odstoupit	odstoupit	k5eAaPmF	odstoupit
Římu	Řím	k1gInSc3	Řím
své	svůj	k3xOyFgFnPc4	svůj
pozice	pozice	k1gFnPc4	pozice
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
během	během	k7c2	během
žoldnéřské	žoldnéřský	k2eAgFnSc2d1	žoldnéřská
války	válka	k1gFnSc2	válka
také	také	k9	také
na	na	k7c6	na
Sardinii	Sardinie	k1gFnSc6	Sardinie
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
Punové	Pun	k1gMnPc1	Pun
vynahradili	vynahradit	k5eAaPmAgMnP	vynahradit
tyto	tento	k3xDgFnPc4	tento
územní	územní	k2eAgFnPc4d1	územní
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
,	,	kIx,	,
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
podrobení	podrobení	k1gNnSc6	podrobení
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
si	se	k3xPyFc3	se
obzvlášť	obzvlášť	k6eAd1	obzvlášť
energicky	energicky	k6eAd1	energicky
počínal	počínat	k5eAaImAgInS	počínat
kartaginský	kartaginský	k2eAgInSc1d1	kartaginský
rod	rod	k1gInSc1	rod
Barkasů	Barkas	k1gInPc2	Barkas
<g/>
:	:	kIx,	:
Hamilkar	Hamilkar	k1gInSc1	Hamilkar
Barkas	barkasa	k1gFnPc2	barkasa
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
zeť	zeť	k1gMnSc1	zeť
Hasdrubal	Hasdrubal	k1gMnSc1	Hasdrubal
Sličný	sličný	k2eAgMnSc1d1	sličný
a	a	k8xC	a
Hamilkarův	Hamilkarův	k2eAgMnSc1d1	Hamilkarův
syn	syn	k1gMnSc1	syn
Hannibal	Hannibal	k1gInSc1	Hannibal
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
Hasdrubal	Hasdrubal	k1gMnSc1	Hasdrubal
byl	být	k5eAaImAgMnS	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
zde	zde	k6eAd1	zde
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
mocenskou	mocenský	k2eAgFnSc4d1	mocenská
základnu	základna	k1gFnSc4	základna
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
"	"	kIx"	"
<g/>
říše	říše	k1gFnSc1	říše
Barků	Bark	k1gInPc2	Bark
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Diplomatickou	diplomatický	k2eAgFnSc7d1	diplomatická
nebo	nebo	k8xC	nebo
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
cestou	cesta	k1gFnSc7	cesta
si	se	k3xPyFc3	se
podrobil	podrobit	k5eAaPmAgMnS	podrobit
mnohé	mnohý	k2eAgInPc4d1	mnohý
zdejší	zdejší	k2eAgInPc4d1	zdejší
iberské	iberský	k2eAgInPc4d1	iberský
kmeny	kmen	k1gInPc4	kmen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
227	[number]	k4	227
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
založil	založit	k5eAaPmAgMnS	založit
město	město	k1gNnSc4	město
Carthago	Carthago	k1gNnSc4	Carthago
Nova	novum	k1gNnSc2	novum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
fungovalo	fungovat	k5eAaImAgNnS	fungovat
jako	jako	k9	jako
centrum	centrum	k1gNnSc1	centrum
kartaginského	kartaginský	k2eAgNnSc2d1	kartaginské
pronikání	pronikání	k1gNnSc2	pronikání
do	do	k7c2	do
Hispánie	Hispánie	k1gFnSc2	Hispánie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kartágo	Kartágo	k1gNnSc4	Kartágo
tak	tak	k9	tak
díky	díky	k7c3	díky
Barkasům	Barkas	k1gMnPc3	Barkas
získalo	získat	k5eAaPmAgNnS	získat
v	v	k7c6	v
Hispánii	Hispánie	k1gFnSc6	Hispánie
novou	nový	k2eAgFnSc4d1	nová
provincii	provincie	k1gFnSc4	provincie
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
zdroje	zdroj	k1gInPc4	zdroj
k	k	k7c3	k
odvetné	odvetný	k2eAgFnSc3d1	odvetná
válce	válka	k1gFnSc3	válka
proti	proti	k7c3	proti
Římu	Řím	k1gInSc3	Řím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
226	[number]	k4	226
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
Hasdrubal	Hasdrubal	k1gFnSc4	Hasdrubal
s	s	k7c7	s
Římany	Říman	k1gMnPc7	Říman
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
byla	být	k5eAaImAgFnS	být
řeka	řeka	k1gFnSc1	řeka
Ebro	Ebro	k6eAd1	Ebro
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
země	zem	k1gFnSc2	zem
stanovena	stanovit	k5eAaPmNgFnS	stanovit
za	za	k7c4	za
hranici	hranice	k1gFnSc4	hranice
kartaginské	kartaginský	k2eAgFnSc2d1	kartaginská
a	a	k8xC	a
římské	římský	k2eAgFnSc2d1	římská
sféry	sféra	k1gFnSc2	sféra
vlivu	vliv	k1gInSc2	vliv
v	v	k7c6	v
Hispánii	Hispánie	k1gFnSc6	Hispánie
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
Hasdrubal	Hasdrubal	k1gFnSc4	Hasdrubal
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
pětadvacetiletý	pětadvacetiletý	k2eAgInSc4d1	pětadvacetiletý
Hannibal	Hannibal	k1gInSc4	Hannibal
Barkas	barkasa	k1gFnPc2	barkasa
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
projevil	projevit	k5eAaPmAgInS	projevit
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
schopný	schopný	k2eAgMnSc1d1	schopný
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
potlačení	potlačení	k1gNnSc6	potlačení
povstání	povstání	k1gNnSc2	povstání
iberských	iberský	k2eAgInPc2d1	iberský
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
po	po	k7c6	po
Hasdrubalově	Hasdrubalův	k2eAgFnSc6d1	Hasdrubalova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
napadl	napadnout	k5eAaPmAgMnS	napadnout
pod	pod	k7c7	pod
římskou	římský	k2eAgFnSc7d1	římská
ochranou	ochrana	k1gFnSc7	ochrana
stojící	stojící	k2eAgFnSc4d1	stojící
řeckou	řecký	k2eAgFnSc4d1	řecká
kolonii	kolonie	k1gFnSc4	kolonie
jménem	jméno	k1gNnSc7	jméno
Saguntum	Saguntum	k1gNnSc1	Saguntum
(	(	kIx(	(
<g/>
Zakantha	Zakantha	k1gFnSc1	Zakantha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osmiměsíčním	osmiměsíční	k2eAgNnSc6d1	osmiměsíční
obléhání	obléhání	k1gNnSc6	obléhání
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
219	[number]	k4	219
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dobyl	dobýt	k5eAaPmAgMnS	dobýt
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vyprovokoval	vyprovokovat	k5eAaPmAgInS	vyprovokovat
římský	římský	k2eAgInSc1d1	římský
senát	senát	k1gInSc1	senát
k	k	k7c3	k
vyslání	vyslání	k1gNnSc3	vyslání
delegace	delegace	k1gFnSc2	delegace
do	do	k7c2	do
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
žádala	žádat	k5eAaImAgFnS	žádat
Hannibalovo	Hannibalův	k2eAgNnSc4d1	Hannibalovo
vydání	vydání	k1gNnSc4	vydání
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
aktu	akt	k1gInSc6	akt
viděli	vidět	k5eAaImAgMnP	vidět
Římané	Říman	k1gMnPc1	Říman
porušení	porušení	k1gNnSc2	porušení
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
Hasdrubalem	Hasdrubal	k1gInSc7	Hasdrubal
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
Saguntum	Saguntum	k1gNnSc1	Saguntum
leželo	ležet	k5eAaImAgNnS	ležet
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Ebro	Ebro	k6eAd1	Ebro
v	v	k7c6	v
kartaginské	kartaginský	k2eAgFnSc6d1	kartaginská
sféře	sféra	k1gFnSc6	sféra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kartaginci	Kartaginec	k1gMnPc1	Kartaginec
tento	tento	k3xDgInSc4	tento
požadavek	požadavek	k1gInSc4	požadavek
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
následovalo	následovat	k5eAaImAgNnS	následovat
římské	římský	k2eAgNnSc1d1	římské
vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
římský	římský	k2eAgInSc1d1	římský
senát	senát	k1gInSc1	senát
učinil	učinit	k5eAaPmAgInS	učinit
toto	tento	k3xDgNnSc4	tento
závažné	závažný	k2eAgNnSc1d1	závažné
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
si	se	k3xPyFc3	se
senátoři	senátor	k1gMnPc1	senátor
jistí	jistit	k5eAaImIp3nP	jistit
kartaginskou	kartaginský	k2eAgFnSc7d1	kartaginská
porážkou	porážka	k1gFnSc7	porážka
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
kartaginská	kartaginský	k2eAgFnSc1d1	kartaginská
armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
takřka	takřka	k6eAd1	takřka
stejně	stejně	k6eAd1	stejně
silná	silný	k2eAgFnSc1d1	silná
jako	jako	k8xS	jako
římská	římský	k2eAgFnSc1d1	římská
–	–	k?	–
domnívali	domnívat	k5eAaImAgMnP	domnívat
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
jejich	jejich	k3xOp3gFnSc1	jejich
drtivá	drtivý	k2eAgFnSc1d1	drtivá
námořní	námořní	k2eAgFnSc1d1	námořní
převaha	převaha	k1gFnSc1	převaha
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Hannibal	Hannibal	k1gInSc4	Hannibal
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Římané	Říman	k1gMnPc1	Říman
mohou	moct	k5eAaImIp3nP	moct
napadat	napadat	k5eAaBmF	napadat
a	a	k8xC	a
ničit	ničit	k5eAaImF	ničit
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
kusy	kus	k1gInPc4	kus
kartaginské	kartaginský	k2eAgFnSc2d1	kartaginská
říše	říš	k1gFnSc2	říš
odděleně	odděleně	k6eAd1	odděleně
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
on	on	k3xPp3gMnSc1	on
mohl	moct	k5eAaImAgMnS	moct
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
dostatečně	dostatečně	k6eAd1	dostatečně
efektivně	efektivně	k6eAd1	efektivně
přesouvat	přesouvat	k5eAaImF	přesouvat
vojska	vojsko	k1gNnPc4	vojsko
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
hrozícím	hrozící	k2eAgInPc3d1	hrozící
vpádům	vpád	k1gInPc3	vpád
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Kartága	Kartágo	k1gNnSc2	Kartágo
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
předejít	předejít	k5eAaPmF	předejít
vlastním	vlastní	k2eAgNnSc7d1	vlastní
tažením	tažení	k1gNnSc7	tažení
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
uskutečněným	uskutečněný	k2eAgNnSc7d1	uskutečněné
vnitrozemím	vnitrozemí	k1gNnSc7	vnitrozemí
a	a	k8xC	a
završeným	završený	k2eAgInSc7d1	završený
přechodem	přechod	k1gInSc7	přechod
Alp	Alpy	k1gFnPc2	Alpy
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
hodlal	hodlat	k5eAaImAgMnS	hodlat
Římany	Říman	k1gMnPc4	Říman
překvapit	překvapit	k5eAaPmF	překvapit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Překročení	překročení	k1gNnSc1	překročení
Alp	Alpy	k1gFnPc2	Alpy
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
218	[number]	k4	218
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
začalo	začít	k5eAaPmAgNnS	začít
tažení	tažení	k1gNnSc1	tažení
proti	proti	k7c3	proti
Římu	Řím	k1gInSc3	Řím
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
na	na	k7c6	na
kartaginské	kartaginský	k2eAgFnSc6d1	kartaginská
straně	strana	k1gFnSc6	strana
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
přes	přes	k7c4	přes
Pyreneje	Pyreneje	k1gFnPc4	Pyreneje
byl	být	k5eAaImAgInS	být
namáhavý	namáhavý	k2eAgInSc1d1	namáhavý
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
boji	boj	k1gInSc6	boj
s	s	k7c7	s
místními	místní	k2eAgInPc7d1	místní
domorodými	domorodý	k2eAgInPc7d1	domorodý
kmeny	kmen	k1gInPc7	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Kelty	Kelt	k1gMnPc4	Kelt
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Galii	Galie	k1gFnSc6	Galie
se	se	k3xPyFc4	se
Hannibalovi	Hannibal	k1gMnSc3	Hannibal
podařilo	podařit	k5eAaPmAgNnS	podařit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
jejich	jejich	k3xOp3gMnSc7	jejich
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
jeden	jeden	k4xCgInSc1	jeden
kmen	kmen	k1gInSc1	kmen
postavil	postavit	k5eAaPmAgInS	postavit
do	do	k7c2	do
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
překročit	překročit	k5eAaPmF	překročit
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Rhodanus	Rhodanus	k1gMnSc1	Rhodanus
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
zdolal	zdolat	k5eAaPmAgMnS	zdolat
tuto	tento	k3xDgFnSc4	tento
vodní	vodní	k2eAgFnSc4d1	vodní
překážku	překážka	k1gFnSc4	překážka
i	i	k8xC	i
Galy	Gal	k1gMnPc4	Gal
stojící	stojící	k2eAgFnSc6d1	stojící
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
vydal	vydat	k5eAaPmAgInS	vydat
vstříc	vstříc	k6eAd1	vstříc
Alpám	Alpy	k1gFnPc3	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Disponoval	disponovat	k5eAaBmAgMnS	disponovat
nyní	nyní	k6eAd1	nyní
celkem	celkem	k6eAd1	celkem
38	[number]	k4	38
000	[number]	k4	000
muži	muž	k1gMnPc7	muž
a	a	k8xC	a
37	[number]	k4	37
válečnými	válečný	k2eAgMnPc7d1	válečný
slony	slon	k1gMnPc7	slon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezitím	mezitím	k6eAd1	mezitím
konzul	konzul	k1gMnSc1	konzul
Publius	Publius	k1gMnSc1	Publius
Cornelius	Cornelius	k1gMnSc1	Cornelius
Scipio	Scipio	k1gMnSc1	Scipio
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
směřoval	směřovat	k5eAaImAgInS	směřovat
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
Hispánie	Hispánie	k1gFnSc2	Hispánie
<g/>
,	,	kIx,	,
spěchal	spěchat	k5eAaImAgMnS	spěchat
k	k	k7c3	k
Rhodanu	Rhodan	k1gMnSc3	Rhodan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tu	tu	k6eAd1	tu
zadržel	zadržet	k5eAaPmAgInS	zadržet
Hannibala	Hannibal	k1gMnSc4	Hannibal
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dorazil	dorazit	k5eAaPmAgMnS	dorazit
příliš	příliš	k6eAd1	příliš
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
částí	část	k1gFnSc7	část
svého	svůj	k3xOyFgNnSc2	svůj
vojska	vojsko	k1gNnSc2	vojsko
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
přeplavil	přeplavit	k5eAaPmAgMnS	přeplavit
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
řeky	řeka	k1gFnSc2	řeka
Pádu	Pád	k1gInSc2	Pád
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyčkával	vyčkávat	k5eAaImAgMnS	vyčkávat
příchodu	příchod	k1gInSc3	příchod
Kartaginců	Kartaginec	k1gMnPc2	Kartaginec
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
vyslal	vyslat	k5eAaPmAgMnS	vyslat
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
Gnaea	Gnaeus	k1gMnSc4	Gnaeus
Cornelia	Cornelius	k1gMnSc4	Cornelius
Scipiona	Scipion	k1gMnSc4	Scipion
s	s	k7c7	s
menší	malý	k2eAgFnSc7d2	menší
částí	část	k1gFnSc7	část
vojska	vojsko	k1gNnSc2	vojsko
do	do	k7c2	do
Hispánie	Hispánie	k1gFnSc2	Hispánie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Překročení	překročení	k1gNnSc1	překročení
Alp	Alpy	k1gFnPc2	Alpy
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
Hannibalovu	Hannibalův	k2eAgNnSc3d1	Hannibalovo
vojsku	vojsko	k1gNnSc3	vojsko
značné	značný	k2eAgFnSc2d1	značná
ztráty	ztráta	k1gFnSc2	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
trpěli	trpět	k5eAaImAgMnP	trpět
hladem	hlad	k1gInSc7	hlad
a	a	k8xC	a
zimou	zima	k1gFnSc7	zima
(	(	kIx(	(
<g/>
tažení	tažení	k1gNnSc1	tažení
probíhalo	probíhat	k5eAaImAgNnS	probíhat
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
podzimu	podzim	k1gInSc2	podzim
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
museli	muset	k5eAaImAgMnP	muset
odrážet	odrážet	k5eAaImF	odrážet
neustálé	neustálý	k2eAgInPc4d1	neustálý
útoky	útok	k1gInPc4	útok
dotírajících	dotírající	k2eAgInPc2d1	dotírající
keltských	keltský	k2eAgInPc2d1	keltský
horských	horský	k2eAgInPc2d1	horský
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
patnácti	patnáct	k4xCc2	patnáct
dnech	den	k1gInPc6	den
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
Hannibal	Hannibal	k1gInSc1	Hannibal
konečně	konečně	k6eAd1	konečně
údolí	údolí	k1gNnSc4	údolí
řeky	řeka	k1gFnSc2	řeka
Pádu	Pád	k1gInSc2	Pád
<g/>
.	.	kIx.	.
</s>
<s>
Zkonsolidoval	zkonsolidovat	k5eAaPmAgMnS	zkonsolidovat
své	své	k1gNnSc4	své
prořídlé	prořídlý	k2eAgFnSc2d1	prořídlá
síly	síla	k1gFnSc2	síla
(	(	kIx(	(
<g/>
zbylo	zbýt	k5eAaPmAgNnS	zbýt
mu	on	k3xPp3gNnSc3	on
jen	jen	k9	jen
26	[number]	k4	26
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
a	a	k8xC	a
dobyl	dobýt	k5eAaPmAgInS	dobýt
dnešní	dnešní	k2eAgInSc1d1	dnešní
Turín	Turín	k1gInSc1	Turín
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnPc1d1	místní
Keltové	Kelt	k1gMnPc1	Kelt
teprve	teprve	k6eAd1	teprve
nedávno	nedávno	k6eAd1	nedávno
podrobení	podrobený	k2eAgMnPc1d1	podrobený
Římany	Říman	k1gMnPc4	Říman
využili	využít	k5eAaPmAgMnP	využít
Hannibalova	Hannibalův	k2eAgInSc2d1	Hannibalův
příchodu	příchod	k1gInSc2	příchod
k	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
připojili	připojit	k5eAaPmAgMnP	připojit
k	k	k7c3	k
Hannibalovu	Hannibalův	k2eAgNnSc3d1	Hannibalovo
vojsku	vojsko	k1gNnSc3	vojsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jakkoliv	jakkoliv	k8xS	jakkoliv
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
neuvěřitelný	uvěřitelný	k2eNgInSc4d1	neuvěřitelný
podnik	podnik	k1gInSc4	podnik
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
lze	lze	k6eAd1	lze
přechod	přechod	k1gInSc1	přechod
Alp	Alpy	k1gFnPc2	Alpy
zhodnotit	zhodnotit	k5eAaPmF	zhodnotit
různě	různě	k6eAd1	různě
<g/>
:	:	kIx,	:
buďto	buďto	k8xC	buďto
jako	jako	k9	jako
geniální	geniální	k2eAgInSc4d1	geniální
strategický	strategický	k2eAgInSc4d1	strategický
tah	tah	k1gInSc4	tah
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přinesl	přinést	k5eAaPmAgInS	přinést
Hannibalovi	Hannibalův	k2eAgMnPc1d1	Hannibalův
plody	plod	k1gInPc4	plod
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
snadných	snadný	k2eAgNnPc2d1	snadné
vítězství	vítězství	k1gNnPc2	vítězství
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
šílený	šílený	k2eAgInSc4d1	šílený
hazard	hazard	k1gInSc4	hazard
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
Hannibal	Hannibal	k1gInSc4	Hannibal
ztratil	ztratit	k5eAaPmAgMnS	ztratit
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
vycvičených	vycvičený	k2eAgMnPc2d1	vycvičený
válečných	válečný	k2eAgMnPc2d1	válečný
slonů	slon	k1gMnPc2	slon
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
kdyby	kdyby	kYmCp3nS	kdyby
Hannibal	Hannibal	k1gInSc1	Hannibal
zaútočil	zaútočit	k5eAaPmAgInS	zaútočit
přes	přes	k7c4	přes
opevněné	opevněný	k2eAgNnSc4d1	opevněné
pohraničí	pohraničí	k1gNnSc4	pohraničí
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
by	by	kYmCp3nS	by
tak	tak	k8xC	tak
strašlivé	strašlivý	k2eAgFnSc2d1	strašlivá
ztráty	ztráta	k1gFnSc2	ztráta
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgNnSc4	jaký
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hannibalova	Hannibalův	k2eAgNnSc2d1	Hannibalovo
vítězství	vítězství	k1gNnSc2	vítězství
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
většímu	veliký	k2eAgInSc3d2	veliký
střetu	střet	k1gInSc3	střet
mezi	mezi	k7c7	mezi
Římany	Říman	k1gMnPc7	Říman
a	a	k8xC	a
Kartaginci	Kartaginec	k1gMnPc7	Kartaginec
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
218	[number]	k4	218
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Ticino	Ticino	k1gNnSc1	Ticino
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
boj	boj	k1gInSc1	boj
vedly	vést	k5eAaImAgInP	vést
převážně	převážně	k6eAd1	převážně
jízdní	jízdní	k2eAgInPc1d1	jízdní
oddíly	oddíl	k1gInPc1	oddíl
a	a	k8xC	a
ztráty	ztráta	k1gFnPc1	ztráta
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
byly	být	k5eAaImAgFnP	být
jen	jen	k6eAd1	jen
nepatrné	nepatrný	k2eAgFnPc1d1	nepatrná
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
jasně	jasně	k6eAd1	jasně
projevila	projevit	k5eAaPmAgFnS	projevit
převaha	převaha	k1gFnSc1	převaha
Hannibalových	Hannibalův	k2eAgMnPc2d1	Hannibalův
zkušených	zkušený	k2eAgMnPc2d1	zkušený
numidských	numidský	k2eAgMnPc2d1	numidský
jezdců	jezdec	k1gMnPc2	jezdec
nad	nad	k7c7	nad
římskou	římský	k2eAgFnSc7d1	římská
jízdou	jízda	k1gFnSc7	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
těžce	těžce	k6eAd1	těžce
zraněný	zraněný	k2eAgMnSc1d1	zraněný
Scipio	Scipio	k1gMnSc1	Scipio
jen	jen	k9	jen
stěží	stěží	k6eAd1	stěží
unikl	uniknout	k5eAaPmAgMnS	uniknout
z	z	k7c2	z
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
byli	být	k5eAaImAgMnP	být
poraženi	poražen	k2eAgMnPc1d1	poražen
a	a	k8xC	a
Scipionovi	Scipionův	k2eAgMnPc1d1	Scipionův
se	se	k3xPyFc4	se
jen	jen	k9	jen
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
vypětím	vypětí	k1gNnSc7	vypětí
sil	síla	k1gFnPc2	síla
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
ustoupit	ustoupit	k5eAaPmF	ustoupit
za	za	k7c4	za
řeku	řeka	k1gFnSc4	řeka
Pád	Pád	k1gInSc1	Pád
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
dorazil	dorazit	k5eAaPmAgMnS	dorazit
druhý	druhý	k4xOgMnSc1	druhý
konzul	konzul	k1gMnSc1	konzul
Tiberius	Tiberius	k1gMnSc1	Tiberius
Sempronius	Sempronius	k1gMnSc1	Sempronius
Longus	Longus	k1gMnSc1	Longus
<g/>
.	.	kIx.	.
</s>
<s>
Spojil	spojit	k5eAaPmAgMnS	spojit
své	svůj	k3xOyFgNnSc4	svůj
vojsko	vojsko	k1gNnSc4	vojsko
se	se	k3xPyFc4	se
zbytky	zbytek	k1gInPc1	zbytek
Scipionova	Scipionův	k2eAgMnSc2d1	Scipionův
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
Kartagincům	Kartaginec	k1gMnPc3	Kartaginec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Trebie	Trebie	k1gFnSc2	Trebie
se	se	k3xPyFc4	se
ale	ale	k9	ale
Hannibalovi	Hannibal	k1gMnSc3	Hannibal
podařilo	podařit	k5eAaPmAgNnS	podařit
vylákat	vylákat	k5eAaPmF	vylákat
římské	římský	k2eAgNnSc1d1	římské
vojsko	vojsko	k1gNnSc1	vojsko
za	za	k7c4	za
řeku	řeka	k1gFnSc4	řeka
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
obklíčit	obklíčit	k5eAaPmF	obklíčit
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Kartaginci	Kartaginec	k1gMnPc1	Kartaginec
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
<g/>
,	,	kIx,	,
naproti	naproti	k6eAd1	naproti
tomu	ten	k3xDgMnSc3	ten
Římané	Říman	k1gMnPc1	Říman
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
těžké	těžký	k2eAgFnPc4d1	těžká
ztráty	ztráta	k1gFnPc4	ztráta
(	(	kIx(	(
<g/>
měli	mít	k5eAaImAgMnP	mít
kolem	kolem	k7c2	kolem
10	[number]	k4	10
000	[number]	k4	000
padlých	padlý	k1gMnPc2	padlý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
přezimoval	přezimovat	k5eAaBmAgMnS	přezimovat
v	v	k7c6	v
Bologni	Bologna	k1gFnSc6	Bologna
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
se	se	k3xPyFc4	se
Hannibal	Hannibal	k1gInSc1	Hannibal
v	v	k7c6	v
roce	rok	k1gInSc6	rok
217	[number]	k4	217
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
přes	přes	k7c4	přes
Apeniny	Apeniny	k1gFnPc4	Apeniny
na	na	k7c4	na
jih	jih	k1gInSc4	jih
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
četnými	četný	k2eAgMnPc7d1	četný
sliby	slib	k1gInPc4	slib
snažil	snažit	k5eAaImAgMnS	snažit
získat	získat	k5eAaPmF	získat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
italické	italický	k2eAgInPc1d1	italický
kmeny	kmen	k1gInPc1	kmen
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Trasimenského	Trasimenský	k2eAgNnSc2d1	Trasimenské
jezera	jezero	k1gNnSc2	jezero
narazil	narazit	k5eAaPmAgMnS	narazit
na	na	k7c4	na
postupující	postupující	k2eAgMnPc4d1	postupující
římské	římský	k2eAgMnPc4d1	římský
legionáře	legionář	k1gMnPc4	legionář
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
konzula	konzul	k1gMnSc2	konzul
Gaia	Gaius	k1gMnSc2	Gaius
Flaminia	Flaminium	k1gNnSc2	Flaminium
a	a	k8xC	a
přichystal	přichystat	k5eAaPmAgMnS	přichystat
jim	on	k3xPp3gMnPc3	on
zde	zde	k6eAd1	zde
dokonalou	dokonalý	k2eAgFnSc4d1	dokonalá
léčku	léčka	k1gFnSc4	léčka
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Římané	Říman	k1gMnPc1	Říman
postupovali	postupovat	k5eAaImAgMnP	postupovat
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
podél	podél	k7c2	podél
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
Kartaginci	Kartaginec	k1gMnPc1	Kartaginec
přehradili	přehradit	k5eAaPmAgMnP	přehradit
cestu	cesta	k1gFnSc4	cesta
vpřed	vpřed	k6eAd1	vpřed
i	i	k9	i
vzad	vzad	k6eAd1	vzad
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
spustili	spustit	k5eAaPmAgMnP	spustit
z	z	k7c2	z
kopců	kopec	k1gInPc2	kopec
nad	nad	k7c7	nad
jezerem	jezero	k1gNnSc7	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
ocitli	ocitnout	k5eAaPmAgMnP	ocitnout
v	v	k7c6	v
obklíčení	obklíčení	k1gNnSc6	obklíčení
a	a	k8xC	a
15	[number]	k4	15
000	[number]	k4	000
jich	on	k3xPp3gMnPc2	on
padlo	padnout	k5eAaImAgNnS	padnout
včetně	včetně	k7c2	včetně
samotného	samotný	k2eAgMnSc2d1	samotný
konzula	konzul	k1gMnSc2	konzul
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
Římanů	Říman	k1gMnPc2	Říman
rovněž	rovněž	k6eAd1	rovněž
upadlo	upadnout	k5eAaPmAgNnS	upadnout
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Italské	italský	k2eAgMnPc4d1	italský
spojence	spojenec	k1gMnPc4	spojenec
nechal	nechat	k5eAaPmAgInS	nechat
Hannibal	Hannibal	k1gInSc1	Hannibal
propustit	propustit	k5eAaPmF	propustit
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
požadoval	požadovat	k5eAaImAgMnS	požadovat
výkupné	výkupné	k1gNnSc4	výkupné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
svou	svůj	k3xOyFgFnSc7	svůj
mírností	mírnost	k1gFnSc7	mírnost
chtěl	chtít	k5eAaImAgMnS	chtít
přimět	přimět	k5eAaPmF	přimět
k	k	k7c3	k
odpadnutí	odpadnutí	k1gNnSc3	odpadnutí
od	od	k7c2	od
Římanů	Říman	k1gMnPc2	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
vyslali	vyslat	k5eAaPmAgMnP	vyslat
proti	proti	k7c3	proti
Hannibalovi	Hannibal	k1gMnSc3	Hannibal
další	další	k2eAgFnPc1d1	další
oddíly	oddíl	k1gInPc4	oddíl
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
však	však	k9	však
byly	být	k5eAaImAgFnP	být
taktéž	taktéž	k?	taktéž
poraženy	porazit	k5eAaPmNgInP	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
tisíce	tisíc	k4xCgInPc1	tisíc
legionářů	legionář	k1gMnPc2	legionář
padlo	padnout	k5eAaImAgNnS	padnout
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
tisíce	tisíc	k4xCgInPc1	tisíc
legionářů	legionář	k1gMnPc2	legionář
bylo	být	k5eAaImAgNnS	být
zajato	zajmout	k5eAaPmNgNnS	zajmout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
dojmem	dojem	k1gInSc7	dojem
těchto	tento	k3xDgFnPc2	tento
porážek	porážka	k1gFnPc2	porážka
se	se	k3xPyFc4	se
římský	římský	k2eAgInSc1d1	římský
senát	senát	k1gInSc1	senát
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
zvolit	zvolit	k5eAaPmF	zvolit
diktátora	diktátor	k1gMnSc4	diktátor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
by	by	kYmCp3nS	by
Hannibala	Hannibal	k1gMnSc4	Hannibal
konečně	konečně	k6eAd1	konečně
zastavil	zastavit	k5eAaPmAgMnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Zvolen	Zvolen	k1gInSc4	Zvolen
byl	být	k5eAaImAgMnS	být
Fabius	Fabius	k1gMnSc1	Fabius
Maximus	Maximus	k1gMnSc1	Maximus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
okamžitě	okamžitě	k6eAd1	okamžitě
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
legiemi	legie	k1gFnPc7	legie
<g/>
.	.	kIx.	.
</s>
<s>
Přímému	přímý	k2eAgInSc3d1	přímý
střetu	střet	k1gInSc3	střet
s	s	k7c7	s
Hannibalem	Hannibal	k1gInSc7	Hannibal
se	se	k3xPyFc4	se
však	však	k9	však
vyhýbal	vyhýbat	k5eAaImAgMnS	vyhýbat
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
vyčerpat	vyčerpat	k5eAaPmF	vyčerpat
kartaginské	kartaginský	k2eAgNnSc1d1	kartaginské
vojsko	vojsko	k1gNnSc1	vojsko
drobnými	drobný	k2eAgInPc7d1	drobný
nájezdy	nájezd	k1gInPc7	nájezd
a	a	k8xC	a
útoky	útok	k1gInPc7	útok
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
spojence	spojenka	k1gFnSc6	spojenka
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
postup	postup	k1gInSc1	postup
byl	být	k5eAaImAgInS	být
však	však	k9	však
záhy	záhy	k6eAd1	záhy
vystaven	vystaven	k2eAgInSc1d1	vystaven
silné	silný	k2eAgFnSc3d1	silná
kritice	kritika	k1gFnSc3	kritika
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
jeho	jeho	k3xOp3gMnPc2	jeho
spoluobčanů	spoluobčan	k1gMnPc2	spoluobčan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
chtěli	chtít	k5eAaImAgMnP	chtít
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
Hannibalem	Hannibal	k1gInSc7	Hannibal
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
rázně	rázně	k6eAd1	rázně
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
diktátorovi	diktátor	k1gMnSc3	diktátor
vyneslo	vynést	k5eAaPmAgNnS	vynést
přezdívku	přezdívka	k1gFnSc4	přezdívka
Cunctator	Cunctator	k1gInSc1	Cunctator
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Váhavec	váhavec	k1gMnSc1	váhavec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hannibal	Hannibal	k1gInSc1	Hannibal
táhl	táhnout	k5eAaImAgInS	táhnout
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
obešel	obejít	k5eAaPmAgInS	obejít
Řím	Řím	k1gInSc1	Řím
a	a	k8xC	a
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Kampánie	Kampánie	k1gFnSc2	Kampánie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chtěl	chtít	k5eAaImAgMnS	chtít
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Volturno	Volturno	k1gNnSc4	Volturno
rozbít	rozbít	k5eAaPmF	rozbít
zimní	zimní	k2eAgInSc4d1	zimní
tábor	tábor	k1gInSc4	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Maximus	Maximus	k1gMnSc1	Maximus
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vnutit	vnutit	k5eAaPmF	vnutit
svému	svůj	k3xOyFgMnSc3	svůj
nepříteli	nepřítel	k1gMnSc3	nepřítel
bitvu	bitva	k1gFnSc4	bitva
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Hannibalovi	Hannibal	k1gMnSc3	Hannibal
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
díky	díky	k7c3	díky
lsti	lest	k1gFnSc3	lest
uniknout	uniknout	k5eAaPmF	uniknout
a	a	k8xC	a
odtáhnout	odtáhnout	k5eAaPmF	odtáhnout
dále	daleko	k6eAd2	daleko
k	k	k7c3	k
městu	město	k1gNnSc3	město
Gerunium	Gerunium	k1gNnSc1	Gerunium
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
nakonec	nakonec	k6eAd1	nakonec
přezimoval	přezimovat	k5eAaBmAgMnS	přezimovat
<g/>
.	.	kIx.	.
</s>
<s>
Římanům	Říman	k1gMnPc3	Říman
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
drobného	drobný	k2eAgNnSc2d1	drobné
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
,	,	kIx,	,
když	když	k8xS	když
velitel	velitel	k1gMnSc1	velitel
jízdy	jízda	k1gFnSc2	jízda
Marcus	Marcus	k1gMnSc1	Marcus
Minutius	Minutius	k1gMnSc1	Minutius
Rufus	Rufus	k1gMnSc1	Rufus
úspěšně	úspěšně	k6eAd1	úspěšně
napadl	napadnout	k5eAaPmAgMnS	napadnout
punský	punský	k2eAgInSc4d1	punský
předvoj	předvoj	k1gInSc4	předvoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lidové	lidový	k2eAgNnSc1d1	lidové
shromáždění	shromáždění	k1gNnSc1	shromáždění
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
zvolilo	zvolit	k5eAaPmAgNnS	zvolit
Rufa	Rufum	k1gNnPc1	Rufum
druhým	druhý	k4xOgMnSc7	druhý
diktátorem	diktátor	k1gMnSc7	diktátor
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
tak	tak	k6eAd1	tak
nyní	nyní	k6eAd1	nyní
měli	mít	k5eAaImAgMnP	mít
dva	dva	k4xCgMnPc4	dva
diktátory	diktátor	k1gMnPc4	diktátor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
ústavou	ústava	k1gFnSc7	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
tato	tento	k3xDgFnSc1	tento
volba	volba	k1gFnSc1	volba
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
již	již	k6eAd1	již
tak	tak	k6eAd1	tak
existující	existující	k2eAgNnSc4d1	existující
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgMnPc7	dva
veliteli	velitel	k1gMnPc7	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
když	když	k8xS	když
Hannibal	Hannibal	k1gInSc1	Hannibal
vytáhl	vytáhnout	k5eAaPmAgInS	vytáhnout
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
tábora	tábor	k1gInSc2	tábor
a	a	k8xC	a
napadl	napadnout	k5eAaPmAgMnS	napadnout
Rufovo	Rufův	k2eAgNnSc4d1	Rufův
vojsko	vojsko	k1gNnSc4	vojsko
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
hrozilo	hrozit	k5eAaImAgNnS	hrozit
zničení	zničení	k1gNnSc4	zničení
<g/>
,	,	kIx,	,
Maximus	Maximus	k1gMnSc1	Maximus
nehleděl	hledět	k5eNaImAgMnS	hledět
na	na	k7c4	na
vzájemné	vzájemný	k2eAgInPc4d1	vzájemný
rozpory	rozpor	k1gInPc4	rozpor
a	a	k8xC	a
přispěchal	přispěchat	k5eAaPmAgMnS	přispěchat
Rufovi	Rufa	k1gMnSc3	Rufa
ihned	ihned	k6eAd1	ihned
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Hannibal	Hannibal	k1gInSc1	Hannibal
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
nehodlal	hodlat	k5eNaImAgMnS	hodlat
utkat	utkat	k5eAaPmF	utkat
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
římskou	římský	k2eAgFnSc7d1	římská
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
ležení	ležení	k1gNnSc2	ležení
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
omezili	omezit	k5eAaPmAgMnP	omezit
jen	jen	k9	jen
na	na	k7c4	na
izolované	izolovaný	k2eAgInPc4d1	izolovaný
útoky	útok	k1gInPc4	útok
proti	proti	k7c3	proti
Punům	Pun	k1gMnPc3	Pun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Kann	Kanny	k1gFnPc2	Kanny
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
216	[number]	k4	216
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
chtěl	chtít	k5eAaImAgMnS	chtít
Hannibal	Hannibal	k1gInSc4	Hannibal
přinutit	přinutit	k5eAaPmF	přinutit
Římany	Říman	k1gMnPc4	Říman
k	k	k7c3	k
rozhodné	rozhodný	k2eAgFnSc3d1	rozhodná
bitvě	bitva	k1gFnSc3	bitva
a	a	k8xC	a
za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
jménem	jméno	k1gNnSc7	jméno
Kanny	Kanny	k1gFnPc4	Kanny
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
shromáždili	shromáždit	k5eAaPmAgMnP	shromáždit
mohutnou	mohutný	k2eAgFnSc4d1	mohutná
armádu	armáda	k1gFnSc4	armáda
čítající	čítající	k2eAgFnSc4d1	čítající
celkem	celkem	k6eAd1	celkem
16	[number]	k4	16
římských	římský	k2eAgFnPc2d1	římská
a	a	k8xC	a
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
legií	legie	k1gFnPc2	legie
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stáli	stát	k5eAaImAgMnP	stát
oba	dva	k4xCgMnPc1	dva
nově	nově	k6eAd1	nově
zvolení	zvolený	k2eAgMnPc1d1	zvolený
konzulové	konzul	k1gMnPc1	konzul
Lucius	Lucius	k1gMnSc1	Lucius
Aemilius	Aemilius	k1gMnSc1	Aemilius
Paullus	Paullus	k1gMnSc1	Paullus
a	a	k8xC	a
Gaius	Gaius	k1gMnSc1	Gaius
Terentius	Terentius	k1gInSc4	Terentius
Varro	Varro	k1gNnSc1	Varro
<g/>
.	.	kIx.	.
</s>
<s>
Převaha	převaha	k1gFnSc1	převaha
byla	být	k5eAaImAgFnS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
na	na	k7c6	na
římské	římský	k2eAgFnSc6d1	římská
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Římané	Říman	k1gMnPc1	Říman
disponovali	disponovat	k5eAaBmAgMnP	disponovat
celkem	celkem	k6eAd1	celkem
80	[number]	k4	80
000	[number]	k4	000
pěšáky	pěšák	k1gMnPc7	pěšák
a	a	k8xC	a
6000	[number]	k4	6000
jezdci	jezdec	k1gInPc7	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Hannibalovo	Hannibalův	k2eAgNnSc1d1	Hannibalovo
vojsko	vojsko	k1gNnSc1	vojsko
se	se	k3xPyFc4	se
skládalo	skládat	k5eAaImAgNnS	skládat
jen	jen	k9	jen
z	z	k7c2	z
asi	asi	k9	asi
40	[number]	k4	40
000	[number]	k4	000
pěšáků	pěšák	k1gMnPc2	pěšák
a	a	k8xC	a
10	[number]	k4	10
000	[number]	k4	000
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Vážnou	vážný	k2eAgFnSc4d1	vážná
překážku	překážka	k1gFnSc4	překážka
pro	pro	k7c4	pro
římské	římský	k2eAgNnSc4d1	římské
vojsko	vojsko	k1gNnSc4	vojsko
však	však	k9	však
představovala	představovat	k5eAaImAgFnS	představovat
rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
taktika	taktika	k1gFnSc1	taktika
obou	dva	k4xCgMnPc2	dva
konzulů	konzul	k1gMnPc2	konzul
<g/>
:	:	kIx,	:
Zatímco	zatímco	k8xS	zatímco
Paullus	Paullus	k1gMnSc1	Paullus
radil	radit	k5eAaImAgMnS	radit
k	k	k7c3	k
opatrnému	opatrný	k2eAgInSc3d1	opatrný
postupu	postup	k1gInSc3	postup
<g/>
,	,	kIx,	,
Varro	Varro	k1gNnSc4	Varro
vybízel	vybízet	k5eAaImAgInS	vybízet
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
216	[number]	k4	216
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
u	u	k7c2	u
Kann	Kanny	k1gFnPc2	Kanny
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
součástí	součást	k1gFnSc7	součást
prakticky	prakticky	k6eAd1	prakticky
všech	všecek	k3xTgFnPc2	všecek
učebnic	učebnice	k1gFnPc2	učebnice
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
vojenské	vojenský	k2eAgFnSc2d1	vojenská
taktiky	taktika	k1gFnSc2	taktika
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
plánovali	plánovat	k5eAaImAgMnP	plánovat
klasický	klasický	k2eAgInSc4d1	klasický
frontální	frontální	k2eAgInSc4d1	frontální
útok	útok	k1gInSc4	útok
na	na	k7c4	na
střed	střed	k1gInSc4	střed
kartaginské	kartaginský	k2eAgFnSc2d1	kartaginská
sestavy	sestava	k1gFnSc2	sestava
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
Hannibal	Hannibal	k1gInSc4	Hannibal
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
využít	využít	k5eAaPmF	využít
a	a	k8xC	a
použil	použít	k5eAaPmAgMnS	použít
zde	zde	k6eAd1	zde
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
unikátní	unikátní	k2eAgFnSc4d1	unikátní
obkličovací	obkličovací	k2eAgFnSc4d1	obkličovací
taktiku	taktika	k1gFnSc4	taktika
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgMnPc3	svůj
vojákům	voják	k1gMnPc3	voják
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
linie	linie	k1gFnSc2	linie
nařídil	nařídit	k5eAaPmAgMnS	nařídit
Hannibal	Hannibal	k1gInSc4	Hannibal
pomalu	pomalu	k6eAd1	pomalu
ustupovat	ustupovat	k5eAaImF	ustupovat
před	před	k7c7	před
římskými	římský	k2eAgMnPc7d1	římský
legionáři	legionář	k1gMnPc7	legionář
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
ale	ale	k9	ale
nechal	nechat	k5eAaPmAgInS	nechat
posílit	posílit	k5eAaPmF	posílit
křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Svému	svůj	k3xOyFgNnSc3	svůj
jezdectvu	jezdectvo	k1gNnSc3	jezdectvo
přikázal	přikázat	k5eAaPmAgInS	přikázat
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
římskou	římský	k2eAgFnSc4d1	římská
jízdu	jízda	k1gFnSc4	jízda
<g/>
,	,	kIx,	,
přemoci	přemoct	k5eAaPmF	přemoct
jí	on	k3xPp3gFnSc3	on
a	a	k8xC	a
následně	následně	k6eAd1	následně
vpadnout	vpadnout	k5eAaPmF	vpadnout
legionářům	legionář	k1gMnPc3	legionář
do	do	k7c2	do
zad	záda	k1gNnPc2	záda
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
také	také	k9	také
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
postupovali	postupovat	k5eAaImAgMnP	postupovat
kupředu	kupředu	k6eAd1	kupředu
zdánlivě	zdánlivě	k6eAd1	zdánlivě
hroutícím	hroutící	k2eAgMnSc7d1	hroutící
se	se	k3xPyFc4	se
středem	střed	k1gInSc7	střed
kartaginských	kartaginský	k2eAgFnPc2d1	kartaginská
linií	linie	k1gFnPc2	linie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
probíjeli	probíjet	k5eAaImAgMnP	probíjet
do	do	k7c2	do
pasti	past	k1gFnSc2	past
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
kartaginská	kartaginský	k2eAgNnPc4d1	kartaginské
křídla	křídlo	k1gNnPc4	křídlo
zůstala	zůstat	k5eAaPmAgFnS	zůstat
neochvějně	neochvějně	k6eAd1	neochvějně
stát	stát	k5eAaPmF	stát
na	na	k7c6	na
svých	svůj	k3xOyFgNnPc6	svůj
místech	místo	k1gNnPc6	místo
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
začala	začít	k5eAaPmAgFnS	začít
zatlačovat	zatlačovat	k5eAaImF	zatlačovat
a	a	k8xC	a
obchvacovat	obchvacovat	k5eAaImF	obchvacovat
křídla	křídlo	k1gNnPc4	křídlo
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Vítězná	vítězný	k2eAgFnSc1d1	vítězná
kartaginská	kartaginský	k2eAgFnSc1d1	kartaginská
jízda	jízda	k1gFnSc1	jízda
pak	pak	k6eAd1	pak
již	již	k6eAd1	již
jen	jen	k9	jen
dokončila	dokončit	k5eAaPmAgFnS	dokončit
obklíčení	obklíčení	k1gNnSc2	obklíčení
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
sevření	sevřený	k2eAgMnPc1d1	sevřený
na	na	k7c6	na
úzkém	úzký	k2eAgInSc6d1	úzký
prostoru	prostor	k1gInSc6	prostor
se	se	k3xPyFc4	se
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
rozvinout	rozvinout	k5eAaPmF	rozvinout
a	a	k8xC	a
využít	využít	k5eAaPmF	využít
tak	tak	k6eAd1	tak
svoji	svůj	k3xOyFgFnSc4	svůj
početní	početní	k2eAgFnSc4d1	početní
převahu	převaha	k1gFnSc4	převaha
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
naprosto	naprosto	k6eAd1	naprosto
rozdrceni	rozdrcen	k2eAgMnPc1d1	rozdrcen
<g/>
.	.	kIx.	.
</s>
<s>
Současníci	současník	k1gMnPc1	současník
podávají	podávat	k5eAaImIp3nP	podávat
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
téměř	téměř	k6eAd1	téměř
70	[number]	k4	70
000	[number]	k4	000
padlých	padlý	k1gMnPc2	padlý
legionářích	legionář	k1gMnPc6	legionář
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
celé	celý	k2eAgFnSc3d1	celá
armádě	armáda	k1gFnSc3	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
zahynul	zahynout	k5eAaPmAgMnS	zahynout
také	také	k9	také
konzul	konzul	k1gMnSc1	konzul
Paullus	Paullus	k1gMnSc1	Paullus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hannibal	Hannibal	k1gInSc1	Hannibal
však	však	k9	však
nyní	nyní	k6eAd1	nyní
učinil	učinit	k5eAaPmAgMnS	učinit
závažnou	závažný	k2eAgFnSc4d1	závažná
chybu	chyba	k1gFnSc4	chyba
<g/>
,	,	kIx,	,
když	když	k8xS	když
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
využít	využít	k5eAaPmF	využít
tohoto	tento	k3xDgNnSc2	tento
svého	své	k1gNnSc2	své
velkolepého	velkolepý	k2eAgNnSc2d1	velkolepé
vítězství	vítězství	k1gNnSc2	vítězství
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
na	na	k7c4	na
Řím	Řím	k1gInSc4	Řím
a	a	k8xC	a
oblehl	oblehnout	k5eAaPmAgMnS	oblehnout
jej	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
moc	moc	k1gFnSc1	moc
Říma	Řím	k1gInSc2	Řím
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
nadobro	nadobro	k6eAd1	nadobro
zlomena	zlomit	k5eAaPmNgFnS	zlomit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
armáda	armáda	k1gFnSc1	armáda
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
také	také	k6eAd1	také
značné	značný	k2eAgFnPc4d1	značná
ztráty	ztráta	k1gFnPc4	ztráta
a	a	k8xC	a
proto	proto	k8xC	proto
zřejmě	zřejmě	k6eAd1	zřejmě
pochyboval	pochybovat	k5eAaImAgInS	pochybovat
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
schopností	schopnost	k1gFnPc2	schopnost
zdolat	zdolat	k5eAaPmF	zdolat
mocná	mocný	k2eAgNnPc4d1	mocné
opevnění	opevnění	k1gNnPc4	opevnění
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
zvlášť	zvlášť	k6eAd1	zvlášť
když	když	k8xS	když
postrádal	postrádat	k5eAaImAgInS	postrádat
těžká	těžký	k2eAgNnPc4d1	těžké
obléhací	obléhací	k2eAgNnPc4d1	obléhací
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
k	k	k7c3	k
obléhání	obléhání	k1gNnSc3	obléhání
nutně	nutně	k6eAd1	nutně
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
doufal	doufat	k5eAaImAgMnS	doufat
v	v	k7c4	v
psychologický	psychologický	k2eAgInSc4d1	psychologický
efekt	efekt	k1gInSc4	efekt
svého	svůj	k3xOyFgNnSc2	svůj
vítězství	vítězství	k1gNnSc2	vítězství
a	a	k8xC	a
očekával	očekávat	k5eAaImAgInS	očekávat
římskou	římský	k2eAgFnSc4d1	římská
nabídku	nabídka	k1gFnSc4	nabídka
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
však	však	k9	však
nepřicházela	přicházet	k5eNaImAgFnS	přicházet
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
projevila	projevit	k5eAaPmAgFnS	projevit
největší	veliký	k2eAgFnSc1d3	veliký
slabina	slabina	k1gFnSc1	slabina
Hannibalova	Hannibalův	k2eAgNnSc2d1	Hannibalovo
tažení	tažení	k1gNnSc2	tažení
–	–	k?	–
podpora	podpora	k1gFnSc1	podpora
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostávalo	dostávat	k5eAaImAgNnS	dostávat
z	z	k7c2	z
vlastního	vlastní	k2eAgNnSc2d1	vlastní
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
omezeného	omezený	k2eAgInSc2d1	omezený
rozsahu	rozsah	k1gInSc2	rozsah
a	a	k8xC	a
spojenci	spojenec	k1gMnPc1	spojenec
<g/>
,	,	kIx,	,
kterými	který	k3yRgMnPc7	který
nahrazoval	nahrazovat	k5eAaImAgInS	nahrazovat
ztráty	ztráta	k1gFnPc1	ztráta
svých	svůj	k3xOyFgFnPc2	svůj
elitních	elitní	k2eAgFnPc2d1	elitní
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
postrádali	postrádat	k5eAaImAgMnP	postrádat
patřičnou	patřičný	k2eAgFnSc4d1	patřičná
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Jakkoliv	jakkoliv	k8xS	jakkoliv
tedy	tedy	k9	tedy
drtivě	drtivě	k6eAd1	drtivě
vítězil	vítězit	k5eAaImAgInS	vítězit
v	v	k7c6	v
takticky	takticky	k6eAd1	takticky
brilantně	brilantně	k6eAd1	brilantně
vedených	vedený	k2eAgFnPc6d1	vedená
bitvách	bitva	k1gFnPc6	bitva
<g/>
,	,	kIx,	,
po	po	k7c6	po
strategické	strategický	k2eAgFnSc6d1	strategická
stránce	stránka	k1gFnSc6	stránka
svá	svůj	k3xOyFgNnPc4	svůj
vítězství	vítězství	k1gNnSc4	vítězství
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
využít	využít	k5eAaPmF	využít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Situace	situace	k1gFnSc1	situace
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
si	se	k3xPyFc3	se
zatím	zatím	k6eAd1	zatím
zoufalá	zoufalý	k2eAgFnSc1d1	zoufalá
situace	situace	k1gFnSc1	situace
žádala	žádat	k5eAaImAgFnS	žádat
drastická	drastický	k2eAgNnPc1d1	drastické
opatření	opatření	k1gNnPc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
předtím	předtím	k6eAd1	předtím
ani	ani	k8xC	ani
nikdy	nikdy	k6eAd1	nikdy
potom	potom	k6eAd1	potom
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
žádný	žádný	k3yNgInSc1	žádný
jiný	jiný	k2eAgInSc1d1	jiný
stát	stát	k1gInSc1	stát
neustál	ustát	k5eNaPmAgInS	ustát
tak	tak	k9	tak
katastrofální	katastrofální	k2eAgFnSc4d1	katastrofální
sérii	série	k1gFnSc4	série
porážek	porážka	k1gFnPc2	porážka
v	v	k7c6	v
tak	tak	k6eAd1	tak
krátkém	krátký	k2eAgInSc6d1	krátký
časovém	časový	k2eAgInSc6d1	časový
sledu	sled	k1gInSc6	sled
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
nebylo	být	k5eNaImAgNnS	být
snad	snad	k9	snad
žádné	žádný	k3yNgFnSc2	žádný
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
nepřišla	přijít	k5eNaPmAgFnS	přijít
o	o	k7c4	o
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
manžela	manžel	k1gMnSc4	manžel
nebo	nebo	k8xC	nebo
o	o	k7c4	o
otce	otec	k1gMnSc4	otec
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
zvolený	zvolený	k2eAgMnSc1d1	zvolený
diktátor	diktátor	k1gMnSc1	diktátor
Marcus	Marcus	k1gMnSc1	Marcus
Iunius	Iunius	k1gMnSc1	Iunius
Perra	Perra	k1gMnSc1	Perra
přesto	přesto	k8xC	přesto
zakázal	zakázat	k5eAaPmAgInS	zakázat
Římankám	Římanka	k1gFnPc3	Římanka
truchlit	truchlit	k5eAaImF	truchlit
nad	nad	k7c7	nad
ztrátou	ztráta	k1gFnSc7	ztráta
svých	svůj	k3xOyFgMnPc2	svůj
bližních	bližní	k1gMnPc2	bližní
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
doplnil	doplnit	k5eAaPmAgMnS	doplnit
stavy	stav	k1gInPc4	stav
zdevastované	zdevastovaný	k2eAgFnSc2d1	zdevastovaná
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
naverboval	naverbovat	k5eAaPmAgMnS	naverbovat
každého	každý	k3xTgMnSc4	každý
muže	muž	k1gMnSc4	muž
schopného	schopný	k2eAgMnSc4d1	schopný
unést	unést	k5eAaPmF	unést
zbraň	zbraň	k1gFnSc4	zbraň
včetně	včetně	k7c2	včetně
17	[number]	k4	17
<g/>
letých	letý	k2eAgMnPc2d1	letý
chlapců	chlapec	k1gMnPc2	chlapec
a	a	k8xC	a
60	[number]	k4	60
<g/>
letých	letý	k2eAgMnPc2d1	letý
starců	stařec	k1gMnPc2	stařec
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
nestačilo	stačit	k5eNaBmAgNnS	stačit
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgInS	nechat
postavit	postavit	k5eAaPmF	postavit
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
legie	legie	k1gFnPc4	legie
z	z	k7c2	z
vězňů	vězeň	k1gMnPc2	vězeň
a	a	k8xC	a
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
syrakuský	syrakuský	k2eAgMnSc1d1	syrakuský
král	král	k1gMnSc1	král
a	a	k8xC	a
římský	římský	k2eAgMnSc1d1	římský
spojenec	spojenec	k1gMnSc1	spojenec
Hierón	Hierón	k1gMnSc1	Hierón
II	II	kA	II
<g/>
.	.	kIx.	.
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
Římanům	Říman	k1gMnPc3	Říman
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
jej	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Hannibal	Hannibal	k1gInSc1	Hannibal
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
jej	on	k3xPp3gMnSc4	on
také	také	k6eAd1	také
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
nelichotivá	lichotivý	k2eNgFnSc1d1	nelichotivá
přezdívka	přezdívka	k1gFnSc1	přezdívka
Cunctator	Cunctator	k1gInSc1	Cunctator
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
spoluobčané	spoluobčan	k1gMnPc1	spoluobčan
udělili	udělit	k5eAaPmAgMnP	udělit
Fabiu	Fabius	k1gMnSc3	Fabius
Maximovi	Maxim	k1gMnSc3	Maxim
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
někdejší	někdejší	k2eAgNnSc4d1	někdejší
opatrné	opatrný	k2eAgNnSc4d1	opatrné
vedení	vedení	k1gNnSc4	vedení
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
vyslovovat	vyslovovat	k5eAaImF	vyslovovat
s	s	k7c7	s
úctou	úcta	k1gFnSc7	úcta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
pak	pak	k6eAd1	pak
Maximus	Maximus	k1gMnSc1	Maximus
dvakrát	dvakrát	k6eAd1	dvakrát
zvolen	zvolit	k5eAaPmNgMnS	zvolit
konzulem	konzul	k1gMnSc7	konzul
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
se	se	k3xPyFc4	se
od	od	k7c2	od
nynějška	nynějšek	k1gInSc2	nynějšek
úzkostlivě	úzkostlivě	k6eAd1	úzkostlivě
vyhýbali	vyhýbat	k5eAaImAgMnP	vyhýbat
jakémukoliv	jakýkoliv	k3yIgInSc3	jakýkoliv
přímému	přímý	k2eAgInSc3d1	přímý
střetu	střet	k1gInSc3	střet
s	s	k7c7	s
Hannibalem	Hannibal	k1gInSc7	Hannibal
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
do	do	k7c2	do
několika	několik	k4yIc2	několik
menších	malý	k2eAgFnPc2d2	menší
armád	armáda	k1gFnPc2	armáda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
znepokojovaly	znepokojovat	k5eAaImAgFnP	znepokojovat
a	a	k8xC	a
vyčerpávaly	vyčerpávat	k5eAaImAgFnP	vyčerpávat
Hannibalovo	Hannibalův	k2eAgNnSc4d1	Hannibalovo
vojsko	vojsko	k1gNnSc1	vojsko
únavnými	únavný	k2eAgInPc7d1	únavný
pochody	pochod	k1gInPc7	pochod
<g/>
,	,	kIx,	,
neustávajícími	ustávající	k2eNgInPc7d1	neustávající
drobnými	drobný	k2eAgInPc7d1	drobný
střety	střet	k1gInPc7	střet
a	a	k8xC	a
hladem	hlad	k1gInSc7	hlad
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
úsilí	úsilí	k1gNnSc1	úsilí
bylo	být	k5eAaImAgNnS	být
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
na	na	k7c6	na
likvidaci	likvidace	k1gFnSc6	likvidace
kartaginské	kartaginský	k2eAgFnSc2d1	kartaginská
Hispánie	Hispánie	k1gFnSc2	Hispánie
a	a	k8xC	a
ostatních	ostatní	k2eAgMnPc2d1	ostatní
Hannibalových	Hannibalův	k2eAgMnPc2d1	Hannibalův
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hannibal	Hannibal	k1gInSc1	Hannibal
doufal	doufat	k5eAaImAgInS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
porážka	porážka	k1gFnSc1	porážka
povede	povést	k5eAaPmIp3nS	povést
k	k	k7c3	k
rozkladu	rozklad	k1gInSc3	rozklad
spojeneckého	spojenecký	k2eAgInSc2d1	spojenecký
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
nástrojem	nástroj	k1gInSc7	nástroj
římské	římský	k2eAgFnSc2d1	římská
nadvlády	nadvláda	k1gFnSc2	nadvláda
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Samnité	Samnitý	k2eAgNnSc1d1	Samnité
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
mnohé	mnohý	k2eAgInPc1d1	mnohý
jiné	jiný	k2eAgInPc1d1	jiný
kmeny	kmen	k1gInPc1	kmen
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
některých	některý	k3yIgNnPc2	některý
bohatých	bohatý	k2eAgNnPc2d1	bohaté
a	a	k8xC	a
mocných	mocný	k2eAgNnPc2d1	mocné
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
například	například	k6eAd1	například
Capua	Capu	k2eAgFnSc1d1	Capu
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skutečně	skutečně	k6eAd1	skutečně
odpadly	odpadnout	k5eAaPmAgInP	odpadnout
od	od	k7c2	od
Římanů	Říman	k1gMnPc2	Říman
a	a	k8xC	a
připojily	připojit	k5eAaPmAgFnP	připojit
se	se	k3xPyFc4	se
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
většina	většina	k1gFnSc1	většina
italských	italský	k2eAgInPc2d1	italský
kmenů	kmen	k1gInPc2	kmen
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
setrvala	setrvat	k5eAaPmAgFnS	setrvat
po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
boku	bok	k1gInSc6	bok
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
jediným	jediný	k2eAgInSc7d1	jediný
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
byli	být	k5eAaImAgMnP	být
Římané	Říman	k1gMnPc1	Říman
i	i	k9	i
po	po	k7c6	po
Kannách	Kanny	k1gFnPc6	Kanny
schopni	schopen	k2eAgMnPc1d1	schopen
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
že	že	k9	že
byl	být	k5eAaImAgMnS	být
Hannibal	Hannibal	k1gInSc4	Hannibal
skvělý	skvělý	k2eAgMnSc1d1	skvělý
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
zdatným	zdatný	k2eAgMnSc7d1	zdatný
diplomatem	diplomat	k1gMnSc7	diplomat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
215	[number]	k4	215
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
Filipa	Filip	k1gMnSc4	Filip
V.	V.	kA	V.
Makedonského	makedonský	k2eAgMnSc4d1	makedonský
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
Římanům	Říman	k1gMnPc3	Říman
(	(	kIx(	(
<g/>
tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
tzv.	tzv.	kA	tzv.
první	první	k4xOgFnSc1	první
makedonská	makedonský	k2eAgFnSc1d1	makedonská
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
římské	římský	k2eAgNnSc1d1	římské
loďstvo	loďstvo	k1gNnSc1	loďstvo
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
Filipovi	Filipův	k2eAgMnPc1d1	Filipův
v	v	k7c6	v
účinné	účinný	k2eAgFnSc6d1	účinná
podpoře	podpora	k1gFnSc6	podpora
Hannibala	Hannibal	k1gMnSc2	Hannibal
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
navíc	navíc	k6eAd1	navíc
proti	proti	k7c3	proti
Makedoncům	Makedonec	k1gMnPc3	Makedonec
podněcovali	podněcovat	k5eAaImAgMnP	podněcovat
jejich	jejich	k3xOp3gMnPc1	jejich
vlastní	vlastní	k2eAgMnPc1d1	vlastní
nepřátelé	nepřítel	k1gMnPc1	nepřítel
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
–	–	k?	–
Aitóly	Aitól	k1gInPc4	Aitól
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
syrakuského	syrakuský	k2eAgMnSc2d1	syrakuský
krále	král	k1gMnSc2	král
Hieróna	Hierón	k1gMnSc2	Hierón
II	II	kA	II
<g/>
.	.	kIx.	.
získal	získat	k5eAaPmAgInS	získat
Hannibal	Hannibal	k1gInSc1	Hannibal
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
také	také	k9	také
nového	nový	k2eAgMnSc4d1	nový
vládce	vládce	k1gMnSc4	vládce
tohoto	tento	k3xDgNnSc2	tento
mocného	mocný	k2eAgNnSc2d1	mocné
řeckého	řecký	k2eAgNnSc2d1	řecké
města	město	k1gNnSc2	město
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Hierónův	Hierónův	k2eAgMnSc1d1	Hierónův
vnuk	vnuk	k1gMnSc1	vnuk
Hierónymos	Hierónymos	k1gMnSc1	Hierónymos
získal	získat	k5eAaPmAgMnS	získat
od	od	k7c2	od
Punů	Pun	k1gMnPc2	Pun
příslib	příslib	k1gInSc4	příslib
nadvlády	nadvláda	k1gFnSc2	nadvláda
nad	nad	k7c7	nad
celým	celý	k2eAgInSc7d1	celý
ostrovem	ostrov	k1gInSc7	ostrov
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
jevilo	jevit	k5eAaImAgNnS	jevit
jako	jako	k9	jako
dostatečně	dostatečně	k6eAd1	dostatečně
lákavý	lákavý	k2eAgInSc1d1	lákavý
motiv	motiv	k1gInSc1	motiv
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obrat	obrat	k1gInSc1	obrat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
před	před	k7c7	před
Hannibalovým	Hannibalův	k2eAgInSc7d1	Hannibalův
přechodem	přechod	k1gInSc7	přechod
Alp	Alpy	k1gFnPc2	Alpy
působil	působit	k5eAaImAgInS	působit
v	v	k7c6	v
Hispánii	Hispánie	k1gFnSc6	Hispánie
Gnaeus	Gnaeus	k1gMnSc1	Gnaeus
Scipio	Scipio	k1gMnSc1	Scipio
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
opevnil	opevnit	k5eAaPmAgMnS	opevnit
na	na	k7c6	na
území	území	k1gNnSc6	území
severně	severně	k6eAd1	severně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Ebro	Ebro	k1gNnSc1	Ebro
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
i	i	k9	i
s	s	k7c7	s
posilami	posila	k1gFnPc7	posila
připojil	připojit	k5eAaPmAgMnS	připojit
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Publius	Publius	k1gMnSc1	Publius
Scipio	Scipio	k1gMnSc1	Scipio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
společně	společně	k6eAd1	společně
rozšiřovali	rozšiřovat	k5eAaImAgMnP	rozšiřovat
římskou	římský	k2eAgFnSc4d1	římská
moc	moc	k1gFnSc4	moc
podél	podél	k7c2	podél
pyrenejského	pyrenejský	k2eAgNnSc2d1	pyrenejské
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
211	[number]	k4	211
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Saguntum	Saguntum	k1gNnSc4	Saguntum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bránilo	bránit	k5eAaImAgNnS	bránit
Hannibalovu	Hannibalův	k2eAgMnSc3d1	Hannibalův
bratrovi	bratr	k1gMnSc3	bratr
<g/>
,	,	kIx,	,
Hasdrubalovi	Hasdrubal	k1gMnSc3	Hasdrubal
Barkasovi	Barkas	k1gMnSc3	Barkas
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
Hannibal	Hannibal	k1gInSc1	Hannibal
zanechal	zanechat	k5eAaPmAgInS	zanechat
s	s	k7c7	s
částí	část	k1gFnSc7	část
vojska	vojsko	k1gNnSc2	vojsko
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
Hispánie	Hispánie	k1gFnSc2	Hispánie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
mohl	moct	k5eAaImAgInS	moct
účinně	účinně	k6eAd1	účinně
podporovat	podporovat	k5eAaImF	podporovat
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
italském	italský	k2eAgNnSc6d1	italské
tažení	tažení	k1gNnSc6	tažení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
se	se	k3xPyFc4	se
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
znovu	znovu	k6eAd1	znovu
zmocnit	zmocnit	k5eAaPmF	zmocnit
svých	svůj	k3xOyFgNnPc2	svůj
starých	starý	k2eAgNnPc2d1	staré
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Sardinii	Sardinie	k1gFnSc6	Sardinie
ale	ale	k8xC	ale
Kartaginci	Kartaginec	k1gMnPc1	Kartaginec
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
zničující	zničující	k2eAgFnSc4d1	zničující
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Silné	silný	k2eAgNnSc1d1	silné
kartaginské	kartaginský	k2eAgNnSc1d1	kartaginské
vojsko	vojsko	k1gNnSc1	vojsko
přistálo	přistát	k5eAaPmAgNnS	přistát
také	také	k9	také
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
novému	nový	k2eAgMnSc3d1	nový
kartaginskému	kartaginský	k2eAgMnSc3d1	kartaginský
spojenci	spojenec	k1gMnSc3	spojenec
–	–	k?	–
Syrakusám	Syrakusy	k1gFnPc3	Syrakusy
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgInSc1d1	římský
senát	senát	k1gInSc1	senát
sem	sem	k6eAd1	sem
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
214	[number]	k4	214
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vyslal	vyslat	k5eAaPmAgMnS	vyslat
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
legiemi	legie	k1gFnPc7	legie
konzula	konzul	k1gMnSc2	konzul
Marca	Marcus	k1gMnSc2	Marcus
Claudia	Claudia	k1gFnSc1	Claudia
Marcella	Marcella	k1gFnSc1	Marcella
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
"	"	kIx"	"
<g/>
mečem	meč	k1gInSc7	meč
Říma	Řím	k1gInSc2	Řím
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgMnSc3	ten
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
punské	punský	k2eAgNnSc1d1	punské
vojsko	vojsko	k1gNnSc1	vojsko
porazit	porazit	k5eAaPmF	porazit
a	a	k8xC	a
následně	následně	k6eAd1	následně
oblehnout	oblehnout	k5eAaPmF	oblehnout
Syrakusy	Syrakusy	k1gFnPc4	Syrakusy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Hannibal	Hannibal	k1gInSc1	Hannibal
snažil	snažit	k5eAaImAgInS	snažit
podkopat	podkopat	k5eAaPmF	podkopat
římský	římský	k2eAgInSc1d1	římský
vliv	vliv	k1gInSc1	vliv
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
v	v	k7c6	v
četných	četný	k2eAgInPc6d1	četný
drobnějších	drobný	k2eAgInPc6d2	drobnější
bojích	boj	k1gInPc6	boj
a	a	k8xC	a
dobýváním	dobývání	k1gNnPc3	dobývání
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgNnPc1d1	důležité
města	město	k1gNnPc1	město
jako	jako	k8xC	jako
Neapolis	Neapolis	k1gFnPc1	Neapolis
nebo	nebo	k8xC	nebo
Nola	Nola	k1gFnSc1	Nola
přesto	přesto	k8xC	přesto
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
věrná	věrný	k2eAgFnSc1d1	věrná
Římanům	Říman	k1gMnPc3	Říman
a	a	k8xC	a
zabránila	zabránit	k5eAaPmAgFnS	zabránit
tak	tak	k6eAd1	tak
Hannibalovi	Hannibal	k1gMnSc3	Hannibal
ve	v	k7c6	v
vytvoření	vytvoření	k1gNnSc6	vytvoření
pevnější	pevný	k2eAgFnSc2d2	pevnější
mocenské	mocenský	k2eAgFnSc2d1	mocenská
základny	základna	k1gFnSc2	základna
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
212	[number]	k4	212
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
podařil	podařit	k5eAaPmAgMnS	podařit
Hannibalovi	Hannibal	k1gMnSc3	Hannibal
pomocí	pomocí	k7c2	pomocí
zrady	zrada	k1gFnSc2	zrada
získat	získat	k5eAaPmF	získat
Tarent	Tarent	k1gInSc4	Tarent
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
citadela	citadela	k1gFnSc1	citadela
ovšem	ovšem	k9	ovšem
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
Římanů	Říman	k1gMnPc2	Říman
a	a	k8xC	a
Hannibal	Hannibal	k1gInSc1	Hannibal
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
donucen	donucen	k2eAgInSc1d1	donucen
k	k	k7c3	k
dlouhému	dlouhý	k2eAgNnSc3d1	dlouhé
obléhání	obléhání	k1gNnSc3	obléhání
<g/>
.	.	kIx.	.
</s>
<s>
Hannibalovy	Hannibalův	k2eAgFnPc4d1	Hannibalova
zaneprázdněnosti	zaneprázdněnost	k1gFnPc4	zaneprázdněnost
dokonale	dokonale	k6eAd1	dokonale
využili	využít	k5eAaPmAgMnP	využít
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
když	když	k8xS	když
postoupili	postoupit	k5eAaPmAgMnP	postoupit
ke	k	k7c3	k
Capue	Capue	k1gFnSc3	Capue
a	a	k8xC	a
obklíčili	obklíčit	k5eAaPmAgMnP	obklíčit
jí	on	k3xPp3gFnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Hannibal	Hannibal	k1gInSc1	Hannibal
upustil	upustit	k5eAaPmAgInS	upustit
od	od	k7c2	od
dalšího	další	k2eAgNnSc2d1	další
obléhání	obléhání	k1gNnSc2	obléhání
v	v	k7c6	v
Tarentu	Tarento	k1gNnSc6	Tarento
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
Řím	Řím	k1gInSc4	Řím
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
onen	onen	k3xDgInSc1	onen
slavný	slavný	k2eAgInSc1d1	slavný
citát	citát	k1gInSc1	citát
"	"	kIx"	"
<g/>
Hannibal	Hannibal	k1gInSc1	Hannibal
ante	ante	k1gNnSc1	ante
portas	portas	k1gInSc1	portas
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Hannibal	Hannibal	k1gInSc1	Hannibal
před	před	k7c7	před
branami	brána	k1gFnPc7	brána
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postoupil	postoupit	k5eAaPmAgMnS	postoupit
až	až	k9	až
k	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
milníku	milník	k1gInSc3	milník
silnice	silnice	k1gFnSc2	silnice
via	via	k7c4	via
Appia	Appius	k1gMnSc4	Appius
<g/>
,	,	kIx,	,
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
město	město	k1gNnSc4	město
samotné	samotný	k2eAgFnSc2d1	samotná
se	se	k3xPyFc4	se
ale	ale	k9	ale
neodvážil	odvážit	k5eNaPmAgMnS	odvážit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
manévrem	manévr	k1gInSc7	manévr
snažil	snažit	k5eAaImAgInS	snažit
pouze	pouze	k6eAd1	pouze
odlákat	odlákat	k5eAaPmF	odlákat
římské	římský	k2eAgFnPc4d1	římská
legie	legie	k1gFnPc4	legie
od	od	k7c2	od
Capuy	Capua	k1gFnSc2	Capua
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
ale	ale	k9	ale
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c4	v
obléhání	obléhání	k1gNnSc4	obléhání
a	a	k8xC	a
Hannibal	Hannibal	k1gInSc4	Hannibal
tak	tak	k6eAd1	tak
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
zabránit	zabránit	k5eAaPmF	zabránit
pádu	pád	k1gInSc3	pád
tohoto	tento	k3xDgInSc2	tento
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc2	on
klíčového	klíčový	k2eAgNnSc2d1	klíčové
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
neúspěch	neúspěch	k1gInSc1	neúspěch
měl	mít	k5eAaImAgInS	mít
dalekosáhlé	dalekosáhlý	k2eAgInPc4d1	dalekosáhlý
důsledky	důsledek	k1gInPc4	důsledek
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zlomit	zlomit	k5eAaPmF	zlomit
římský	římský	k2eAgInSc4d1	římský
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
Italiky	italika	k1gFnPc4	italika
a	a	k8xC	a
jednak	jednak	k8xC	jednak
nebyl	být	k5eNaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
uchránit	uchránit	k5eAaPmF	uchránit
své	svůj	k3xOyFgMnPc4	svůj
nejdůležitější	důležitý	k2eAgMnPc4d3	nejdůležitější
spojence	spojenec	k1gMnPc4	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
moment	moment	k1gInSc1	moment
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
okamžik	okamžik	k1gInSc4	okamžik
obratu	obrat	k1gInSc2	obrat
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
druhé	druhý	k4xOgFnSc2	druhý
punské	punský	k2eAgFnSc2d1	punská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
Římané	Říman	k1gMnPc1	Říman
konečně	konečně	k6eAd1	konečně
dobyli	dobýt	k5eAaPmAgMnP	dobýt
také	také	k9	také
Syrakusy	Syrakusy	k1gFnPc4	Syrakusy
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
padly	padnout	k5eAaPmAgFnP	padnout
po	po	k7c4	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvouletém	dvouletý	k2eAgNnSc6d1	dvouleté
obléhání	obléhání	k1gNnSc6	obléhání
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c6	na
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
odporu	odpor	k1gInSc6	odpor
města	město	k1gNnSc2	město
měl	mít	k5eAaImAgInS	mít
Archimédés	Archimédés	k1gInSc1	Archimédés
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
důmyslné	důmyslný	k2eAgInPc4d1	důmyslný
válečné	válečný	k2eAgInPc4d1	válečný
stroje	stroj	k1gInPc4	stroj
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgMnS	být
zabit	zabít	k5eAaPmNgMnS	zabít
během	během	k7c2	během
drancování	drancování	k1gNnSc2	drancování
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
řekl	říct	k5eAaPmAgMnS	říct
římskému	římský	k2eAgMnSc3d1	římský
vojákovi	voják	k1gMnSc3	voják
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gMnSc4	on
přišel	přijít	k5eAaPmAgMnS	přijít
zajmout	zajmout	k5eAaPmF	zajmout
<g/>
,	,	kIx,	,
mezitím	mezitím	k6eAd1	mezitím
co	co	k3yRnSc1	co
kreslil	kreslit	k5eAaImAgMnS	kreslit
geometrické	geometrický	k2eAgInPc4d1	geometrický
obrazce	obrazec	k1gInPc4	obrazec
do	do	k7c2	do
písku	písek	k1gInSc2	písek
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Neruš	rušit	k5eNaImRp2nS	rušit
mé	můj	k3xOp1gInPc4	můj
kruhy	kruh	k1gInPc4	kruh
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
legionáře	legionář	k1gMnSc4	legionář
natolik	natolik	k6eAd1	natolik
rozzuřil	rozzuřit	k5eAaPmAgMnS	rozzuřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gInSc4	on
ten	ten	k3xDgMnSc1	ten
usmrtil	usmrtit	k5eAaPmAgMnS	usmrtit
svým	svůj	k3xOyFgInSc7	svůj
mečem	meč	k1gInSc7	meč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Publiu	Publius	k1gMnSc3	Publius
Corneliovi	Cornelius	k1gMnSc3	Cornelius
Scipionovi	Scipion	k1gMnSc3	Scipion
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc3	jeho
bratru	bratr	k1gMnSc3	bratr
Gnaeovi	Gnaeus	k1gMnSc3	Gnaeus
se	se	k3xPyFc4	se
v	v	k7c6	v
Hispánii	Hispánie	k1gFnSc6	Hispánie
dařilo	dařit	k5eAaImAgNnS	dařit
skvěle	skvěle	k6eAd1	skvěle
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
rozdělit	rozdělit	k5eAaPmF	rozdělit
své	svůj	k3xOyFgNnSc4	svůj
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
počátkem	počátkem	k7c2	počátkem
jejich	jejich	k3xOp3gFnSc2	jejich
katastrofy	katastrofa	k1gFnSc2	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Hasdrubal	Hasdrubat	k5eAaPmAgMnS	Hasdrubat
Barkas	barkasa	k1gFnPc2	barkasa
porazil	porazit	k5eAaPmAgMnS	porazit
a	a	k8xC	a
zabil	zabít	k5eAaPmAgMnS	zabít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
211	[number]	k4	211
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
nejprve	nejprve	k6eAd1	nejprve
Publia	Publium	k1gNnSc2	Publium
a	a	k8xC	a
potom	potom	k6eAd1	potom
také	také	k9	také
Gnaea	Gnaea	k1gFnSc1	Gnaea
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
úspěch	úspěch	k1gInSc4	úspěch
však	však	k9	však
Hasdrubal	Hasdrubal	k1gFnSc4	Hasdrubal
nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
Římany	Říman	k1gMnPc4	Říman
zcela	zcela	k6eAd1	zcela
vypudit	vypudit	k5eAaPmF	vypudit
z	z	k7c2	z
Hispánie	Hispánie	k1gFnSc2	Hispánie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Metauru	Metaur	k1gInSc2	Metaur
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
vyslal	vyslat	k5eAaPmAgInS	vyslat
senát	senát	k1gInSc1	senát
na	na	k7c4	na
Pyrenejský	pyrenejský	k2eAgInSc4d1	pyrenejský
poloostrov	poloostrov	k1gInSc4	poloostrov
syna	syn	k1gMnSc2	syn
a	a	k8xC	a
jmenovce	jmenovec	k1gMnSc2	jmenovec
Publia	Publius	k1gMnSc2	Publius
Scipiona	Scipion	k1gMnSc2	Scipion
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
24	[number]	k4	24
<g/>
letému	letý	k2eAgMnSc3d1	letý
Publiu	Publius	k1gMnSc3	Publius
Corneliu	Cornelius	k1gMnSc3	Cornelius
Scipionovi	Scipion	k1gMnSc3	Scipion
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
později	pozdě	k6eAd2	pozdě
obdržel	obdržet	k5eAaPmAgMnS	obdržet
čestné	čestný	k2eAgNnSc4d1	čestné
přízvisko	přízvisko	k1gNnSc4	přízvisko
Africanus	Africanus	k1gMnSc1	Africanus
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
hodnost	hodnost	k1gFnSc1	hodnost
prokonzula	prokonzul	k1gMnSc2	prokonzul
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
dosud	dosud	k6eAd1	dosud
nezastával	zastávat	k5eNaImAgInS	zastávat
žádný	žádný	k3yNgInSc1	žádný
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
úřad	úřad	k1gInSc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Přísahal	přísahat	k5eAaImAgMnS	přísahat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pomstí	pomstit	k5eAaImIp3nS	pomstit
smrt	smrt	k1gFnSc1	smrt
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
a	a	k8xC	a
strýce	strýc	k1gMnSc4	strýc
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
slib	slib	k1gInSc1	slib
také	také	k9	také
vyplnil	vyplnit	k5eAaPmAgInS	vyplnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
209	[number]	k4	209
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
nejprve	nejprve	k6eAd1	nejprve
obsadil	obsadit	k5eAaPmAgMnS	obsadit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
kartaginskou	kartaginský	k2eAgFnSc4d1	kartaginská
základnu	základna	k1gFnSc4	základna
v	v	k7c6	v
Hispánii	Hispánie	k1gFnSc6	Hispánie
–	–	k?	–
Carthago	Carthago	k1gNnSc1	Carthago
Nova	novum	k1gNnSc2	novum
–	–	k?	–
a	a	k8xC	a
poté	poté	k6eAd1	poté
porazil	porazit	k5eAaPmAgMnS	porazit
Hasdrubala	Hasdrubala	k1gFnSc4	Hasdrubala
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Baeculy	Baecula	k1gFnSc2	Baecula
<g/>
.	.	kIx.	.
</s>
<s>
Hasdrubal	Hasdrubat	k5eAaPmAgInS	Hasdrubat
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
prakticky	prakticky	k6eAd1	prakticky
odříznut	odříznout	k5eAaPmNgMnS	odříznout
od	od	k7c2	od
spojení	spojení	k1gNnSc2	spojení
s	s	k7c7	s
Kartágem	Kartágo	k1gNnSc7	Kartágo
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
vydat	vydat	k5eAaPmF	vydat
ve	v	k7c6	v
stopách	stopa	k1gFnPc6	stopa
svého	své	k1gNnSc2	své
bratra	bratr	k1gMnSc2	bratr
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
on	on	k3xPp3gMnSc1	on
překročil	překročit	k5eAaPmAgMnS	překročit
Alpy	Alpy	k1gFnPc4	Alpy
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
čelil	čelit	k5eAaImAgInS	čelit
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
protivenstvím	protivenství	k1gNnSc7	protivenství
<g/>
,	,	kIx,	,
než	než	k8xS	než
kterým	který	k3yRgNnSc7	který
byl	být	k5eAaImAgMnS	být
před	před	k7c7	před
lety	léto	k1gNnPc7	léto
vystaven	vystaven	k2eAgInSc4d1	vystaven
Hannibal	Hannibal	k1gInSc4	Hannibal
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
konečně	konečně	k6eAd1	konečně
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Předalpské	předalpský	k2eAgFnSc2d1	Předalpská
Galie	Galie	k1gFnSc2	Galie
<g/>
,	,	kIx,	,
připojilo	připojit	k5eAaPmAgNnS	připojit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
Keltů	Kelt	k1gMnPc2	Kelt
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
vojsko	vojsko	k1gNnSc1	vojsko
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
nim	on	k3xPp3gMnPc3	on
téměř	téměř	k6eAd1	téměř
zdvojnásobilo	zdvojnásobit	k5eAaPmAgNnS	zdvojnásobit
a	a	k8xC	a
čítalo	čítat	k5eAaImAgNnS	čítat
nyní	nyní	k6eAd1	nyní
kolem	kolem	k7c2	kolem
50	[number]	k4	50
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Římané	Říman	k1gMnPc1	Říman
si	se	k3xPyFc3	se
uvědomovali	uvědomovat	k5eAaImAgMnP	uvědomovat
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
vpádu	vpád	k1gInSc2	vpád
druhého	druhý	k4xOgNnSc2	druhý
kartaginského	kartaginský	k2eAgNnSc2d1	kartaginské
vojska	vojsko	k1gNnSc2	vojsko
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
Hannibalovi	Hannibal	k1gMnSc3	Hannibal
a	a	k8xC	a
Hasdrubalovi	Hasdrubal	k1gMnSc3	Hasdrubal
podařilo	podařit	k5eAaPmAgNnS	podařit
spojit	spojit	k5eAaPmF	spojit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
by	by	kYmCp3nS	by
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterou	který	k3yRgFnSc7	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
Římané	Říman	k1gMnPc1	Říman
mohli	moct	k5eAaImAgMnP	moct
jen	jen	k9	jen
stěží	stěží	k6eAd1	stěží
měřit	měřit	k5eAaImF	měřit
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
byl	být	k5eAaImAgMnS	být
Hasdrubal	Hasdrubal	k1gFnPc4	Hasdrubal
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
stejně	stejně	k6eAd1	stejně
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
jako	jako	k8xS	jako
Hannibal	Hannibal	k1gInSc1	Hannibal
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
přivedl	přivést	k5eAaPmAgMnS	přivést
by	by	kYmCp3nP	by
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
zdejší	zdejší	k2eAgInSc1d1	zdejší
dosavadní	dosavadní	k2eAgFnSc3d1	dosavadní
římské	římský	k2eAgFnSc3d1	římská
spojence	spojenka	k1gFnSc3	spojenka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
by	by	kYmCp3nS	by
Řím	Řím	k1gInSc4	Řím
rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
způsobem	způsob	k1gInSc7	způsob
oslaben	oslabit	k5eAaPmNgInS	oslabit
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
římské	římský	k2eAgFnSc2d1	římská
plánu	plán	k1gInSc3	plán
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
zabránit	zabránit	k5eAaPmF	zabránit
spojení	spojení	k1gNnSc4	spojení
obou	dva	k4xCgFnPc2	dva
kartaginských	kartaginský	k2eAgFnPc2d1	kartaginská
armád	armáda	k1gFnPc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hasdrubal	Hasdrubat	k5eAaPmAgMnS	Hasdrubat
však	však	k9	však
během	během	k7c2	během
postupu	postup	k1gInSc2	postup
na	na	k7c4	na
jih	jih	k1gInSc4	jih
učinil	učinit	k5eAaPmAgMnS	učinit
závažnou	závažný	k2eAgFnSc4d1	závažná
strategickou	strategický	k2eAgFnSc4d1	strategická
chybu	chyba	k1gFnSc4	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Obléháním	obléhání	k1gNnSc7	obléhání
nedůležitého	důležitý	k2eNgNnSc2d1	nedůležité
města	město	k1gNnSc2	město
Placentia	Placentia	k1gFnSc1	Placentia
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Piacenza	Piacenza	k1gFnSc1	Piacenza
<g/>
)	)	kIx)	)
ztratil	ztratit	k5eAaPmAgInS	ztratit
cenný	cenný	k2eAgInSc4d1	cenný
čas	čas	k1gInSc4	čas
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgMnS	umožnit
tak	tak	k9	tak
Římanům	Říman	k1gMnPc3	Říman
shromáždit	shromáždit	k5eAaPmF	shromáždit
silnou	silný	k2eAgFnSc4d1	silná
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mu	on	k3xPp3gMnSc3	on
vyslali	vyslat	k5eAaPmAgMnP	vyslat
vstříc	vstříc	k6eAd1	vstříc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sedmidenním	sedmidenní	k2eAgInSc6d1	sedmidenní
usilovném	usilovný	k2eAgInSc6d1	usilovný
pochodu	pochod	k1gInSc6	pochod
narazili	narazit	k5eAaPmAgMnP	narazit
Římané	Říman	k1gMnPc1	Říman
na	na	k7c4	na
Hasdrubalovu	Hasdrubalův	k2eAgFnSc4d1	Hasdrubalova
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Metaurus	Metaurus	k1gMnSc1	Metaurus
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
207	[number]	k4	207
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zcela	zcela	k6eAd1	zcela
zničili	zničit	k5eAaPmAgMnP	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Hasdrubal	Hasdrubal	k1gMnSc1	Hasdrubal
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vše	všechen	k3xTgNnSc1	všechen
ztraceno	ztratit	k5eAaPmNgNnS	ztratit
<g/>
,	,	kIx,	,
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
do	do	k7c2	do
nejhustšího	hustý	k2eAgInSc2d3	nejhustší
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
padl	padnout	k5eAaImAgInS	padnout
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vinu	vina	k1gFnSc4	vina
na	na	k7c6	na
bratrově	bratrův	k2eAgFnSc6d1	bratrova
porážce	porážka	k1gFnSc6	porážka
měl	mít	k5eAaImAgMnS	mít
i	i	k9	i
Hannibal	Hannibal	k1gInSc4	Hannibal
–	–	k?	–
Římané	Říman	k1gMnPc1	Říman
totiž	totiž	k9	totiž
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
proti	proti	k7c3	proti
Hasdrubalovi	Hasdrubal	k1gMnSc3	Hasdrubal
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ho	on	k3xPp3gMnSc4	on
blokovalo	blokovat	k5eAaImAgNnS	blokovat
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
toho	ten	k3xDgNnSc2	ten
Hannibal	Hannibal	k1gInSc1	Hannibal
všiml	všimnout	k5eAaPmAgInS	všimnout
a	a	k8xC	a
nějak	nějak	k6eAd1	nějak
toho	ten	k3xDgMnSc4	ten
využil	využít	k5eAaPmAgMnS	využít
<g/>
.	.	kIx.	.
</s>
<s>
Zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
se	se	k3xPyFc4	se
Hannibal	Hannibal	k1gInSc1	Hannibal
dozvěděl	dozvědět	k5eAaPmAgInS	dozvědět
drastickým	drastický	k2eAgInSc7d1	drastický
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
když	když	k8xS	když
římský	římský	k2eAgMnSc1d1	římský
jezdec	jezdec	k1gMnSc1	jezdec
vhodil	vhodit	k5eAaPmAgMnS	vhodit
Hasdrubalovu	Hasdrubalův	k2eAgFnSc4d1	Hasdrubalova
hlavu	hlava	k1gFnSc4	hlava
do	do	k7c2	do
punského	punský	k2eAgInSc2d1	punský
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hannibalův	Hannibalův	k2eAgInSc4d1	Hannibalův
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Zamy	Zama	k1gFnSc2	Zama
==	==	k?	==
</s>
</p>
<p>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Scipio	Scipio	k1gMnSc1	Scipio
zatím	zatím	k6eAd1	zatím
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
Hispánii	Hispánie	k1gFnSc6	Hispánie
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
tažení	tažení	k1gNnSc6	tažení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
206	[number]	k4	206
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
porazil	porazit	k5eAaPmAgInS	porazit
nejmladšího	mladý	k2eAgMnSc4d3	nejmladší
Hannibalova	Hannibalův	k2eAgMnSc4d1	Hannibalův
bratra	bratr	k1gMnSc4	bratr
<g/>
,	,	kIx,	,
Magona	Magon	k1gMnSc4	Magon
<g/>
,	,	kIx,	,
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Ilipy	Ilipa	k1gFnSc2	Ilipa
a	a	k8xC	a
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
roce	rok	k1gInSc6	rok
dobyl	dobýt	k5eAaPmAgInS	dobýt
kartaginský	kartaginský	k2eAgInSc1d1	kartaginský
přístav	přístav	k1gInSc1	přístav
Gades	Gades	k1gInSc1	Gades
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Cádiz	Cádiz	k1gInSc1	Cádiz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
definitivní	definitivní	k2eAgInSc4d1	definitivní
zánik	zánik	k1gInSc4	zánik
kartaginské	kartaginský	k2eAgFnSc2d1	kartaginská
přítomnosti	přítomnost	k1gFnSc2	přítomnost
v	v	k7c6	v
Hispánii	Hispánie	k1gFnSc6	Hispánie
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
napřel	napřít	k5eAaPmAgInS	napřít
Scipio	Scipio	k6eAd1	Scipio
diplomatické	diplomatický	k2eAgNnSc4d1	diplomatické
úsilí	úsilí	k1gNnSc4	úsilí
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
získat	získat	k5eAaPmF	získat
spojence	spojenec	k1gMnPc4	spojenec
mezi	mezi	k7c4	mezi
Numiďany	Numiďana	k1gFnPc4	Numiďana
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
římští	římský	k2eAgMnPc1d1	římský
senátoři	senátor	k1gMnPc1	senátor
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
zdráhali	zdráhat	k5eAaImAgMnP	zdráhat
dovolit	dovolit	k5eAaPmF	dovolit
svému	svůj	k1gMnSc3	svůj
vojevůdci	vojevůdce	k1gMnSc3	vojevůdce
útok	útok	k1gInSc4	útok
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozhodujícího	rozhodující	k2eAgNnSc2d1	rozhodující
vítězství	vítězství	k1gNnSc2	vítězství
nad	nad	k7c7	nad
Kartaginci	Kartaginec	k1gMnPc7	Kartaginec
bude	být	k5eAaImBp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Scipio	Scipio	k1gMnSc1	Scipio
chtěl	chtít	k5eAaImAgMnS	chtít
přesto	přesto	k8xC	přesto
podniknout	podniknout	k5eAaPmF	podniknout
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
přinutit	přinutit	k5eAaPmF	přinutit
tak	tak	k9	tak
Hannibala	Hannibal	k1gMnSc4	Hannibal
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
odmítavé	odmítavý	k2eAgNnSc4d1	odmítavé
stanovisko	stanovisko	k1gNnSc4	stanovisko
senátu	senát	k1gInSc2	senát
vyslal	vyslat	k5eAaPmAgMnS	vyslat
k	k	k7c3	k
jejím	její	k3xOp3gInPc3	její
břehům	břeh	k1gInPc3	břeh
menší	malý	k2eAgFnPc4d2	menší
flotilu	flotila	k1gFnSc4	flotila
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
drancovala	drancovat	k5eAaImAgFnS	drancovat
pobřeží	pobřeží	k1gNnSc3	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Kartaginci	Kartaginec	k1gMnPc1	Kartaginec
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
cítili	cítit	k5eAaImAgMnP	cítit
ohrožení	ohrožení	k1gNnSc4	ohrožení
a	a	k8xC	a
hledali	hledat	k5eAaImAgMnP	hledat
pomoc	pomoc	k1gFnSc4	pomoc
u	u	k7c2	u
různých	různý	k2eAgInPc2d1	různý
domorodých	domorodý	k2eAgInPc2d1	domorodý
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osud	osud	k1gInSc1	osud
Kartága	Kartágo	k1gNnSc2	Kartágo
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
naplňovat	naplňovat	k5eAaImF	naplňovat
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
spojenci	spojenec	k1gMnPc7	spojenec
od	od	k7c2	od
něho	on	k3xPp3gMnSc4	on
odpadli	odpadnout	k5eAaPmAgMnP	odpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
v	v	k7c6	v
Ibérii	Ibérie	k1gFnSc6	Ibérie
byly	být	k5eAaImAgFnP	být
skončeny	skončit	k5eAaPmNgFnP	skončit
naprostým	naprostý	k2eAgNnSc7d1	naprosté
vítězstvím	vítězství	k1gNnSc7	vítězství
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
makedonské	makedonský	k2eAgFnSc2d1	makedonská
aspirace	aspirace	k1gFnSc2	aspirace
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
byly	být	k5eAaImAgFnP	být
odraženy	odrazit	k5eAaPmNgFnP	odrazit
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
nyní	nyní	k6eAd1	nyní
mohli	moct	k5eAaImAgMnP	moct
shromáždit	shromáždit	k5eAaPmF	shromáždit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
zahájit	zahájit	k5eAaPmF	zahájit
přímý	přímý	k2eAgInSc4d1	přímý
postup	postup	k1gInSc4	postup
proti	proti	k7c3	proti
Kartagincům	Kartaginec	k1gMnPc3	Kartaginec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
204	[number]	k4	204
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
přistál	přistát	k5eAaImAgInS	přistát
Scipio	Scipio	k6eAd1	Scipio
s	s	k7c7	s
legiemi	legie	k1gFnPc7	legie
u	u	k7c2	u
Uticy	Utica	k1gFnSc2	Utica
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Kartaginci	Kartaginec	k1gMnPc1	Kartaginec
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
odříznout	odříznout	k5eAaPmF	odříznout
svojí	svůj	k3xOyFgFnSc7	svůj
flotilou	flotila	k1gFnSc7	flotila
římské	římský	k2eAgNnSc1d1	římské
vojsko	vojsko	k1gNnSc1	vojsko
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neuspěli	uspět	k5eNaPmAgMnP	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
počátečním	počáteční	k2eAgInSc6d1	počáteční
diplomatickém	diplomatický	k2eAgInSc6d1	diplomatický
nezdaru	nezdar	k1gInSc6	nezdar
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
s	s	k7c7	s
Kartágem	Kartágo	k1gNnSc7	Kartágo
spojil	spojit	k5eAaPmAgMnS	spojit
jeho	jeho	k3xOp3gNnSc4	jeho
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
nepřítel	nepřítel	k1gMnSc1	nepřítel
<g/>
,	,	kIx,	,
numidský	numidský	k2eAgMnSc1d1	numidský
král	král	k1gMnSc1	král
Syfax	Syfax	k1gInSc4	Syfax
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Scipionovi	Scipion	k1gMnSc3	Scipion
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
jiného	jiný	k2eAgMnSc2d1	jiný
numidského	numidský	k2eAgMnSc2d1	numidský
náčelníka	náčelník	k1gMnSc2	náčelník
jménem	jméno	k1gNnSc7	jméno
Massinissa	Massiniss	k1gMnSc4	Massiniss
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
proti	proti	k7c3	proti
Kartágu	Kartágo	k1gNnSc3	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
pak	pak	k6eAd1	pak
Scipio	Scipio	k6eAd1	Scipio
překvapil	překvapit	k5eAaPmAgMnS	překvapit
a	a	k8xC	a
porazil	porazit	k5eAaPmAgMnS	porazit
Syfakovy	Syfakův	k2eAgMnPc4d1	Syfakův
vojáky	voják	k1gMnPc4	voják
tábořící	tábořící	k2eAgMnPc4d1	tábořící
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Bagrades	Bagrades	k1gInSc1	Bagrades
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Medžerda	Medžerd	k1gMnSc2	Medžerd
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kartaginský	kartaginský	k2eAgInSc1d1	kartaginský
senát	senát	k1gInSc1	senát
správně	správně	k6eAd1	správně
vyhodnotil	vyhodnotit	k5eAaPmAgInS	vyhodnotit
závažnost	závažnost	k1gFnSc4	závažnost
situace	situace	k1gFnSc2	situace
a	a	k8xC	a
vyslal	vyslat	k5eAaPmAgMnS	vyslat
ke	k	k7c3	k
Scipionovi	Scipion	k1gMnSc3	Scipion
delegaci	delegace	k1gFnSc4	delegace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gMnSc4	on
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c4	o
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgMnSc1d1	římský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
však	však	k9	však
stanovil	stanovit	k5eAaPmAgMnS	stanovit
tvrdé	tvrdý	k2eAgFnPc4d1	tvrdá
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
:	:	kIx,	:
Kartágo	Kartágo	k1gNnSc1	Kartágo
mělo	mít	k5eAaImAgNnS	mít
stáhnout	stáhnout	k5eAaPmF	stáhnout
svá	svůj	k3xOyFgNnPc4	svůj
vojska	vojsko	k1gNnPc4	vojsko
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
všech	všecek	k3xTgNnPc2	všecek
svých	svůj	k3xOyFgNnPc2	svůj
evropských	evropský	k2eAgInPc6d1	evropský
územích	území	k1gNnPc6	území
a	a	k8xC	a
vydat	vydat	k5eAaPmF	vydat
svoji	svůj	k3xOyFgFnSc4	svůj
flotilu	flotila	k1gFnSc4	flotila
Římanům	Říman	k1gMnPc3	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
podmínky	podmínka	k1gFnPc1	podmínka
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
Kartagince	Kartaginec	k1gMnPc4	Kartaginec
nepřijatelné	přijatelný	k2eNgNnSc4d1	nepřijatelné
a	a	k8xC	a
vyzvali	vyzvat	k5eAaPmAgMnP	vyzvat
proto	proto	k8xC	proto
Hannibala	Hannibal	k1gMnSc2	Hannibal
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Hannibal	Hannibal	k1gInSc1	Hannibal
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
203	[number]	k4	203
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
po	po	k7c6	po
dlouhých	dlouhý	k2eAgNnPc6d1	dlouhé
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
bojů	boj	k1gInPc2	boj
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
opustit	opustit	k5eAaPmF	opustit
Itálii	Itálie	k1gFnSc4	Itálie
a	a	k8xC	a
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Kartaginci	Kartaginec	k1gMnPc1	Kartaginec
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
nabízené	nabízený	k2eAgFnPc4d1	nabízená
mírové	mírový	k2eAgFnPc4d1	mírová
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
obnovil	obnovit	k5eAaPmAgMnS	obnovit
Scipio	Scipio	k6eAd1	Scipio
svůj	svůj	k3xOyFgInSc4	svůj
postup	postup	k1gInSc4	postup
na	na	k7c4	na
Kartágo	Kartágo	k1gNnSc4	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
Hannibalovo	Hannibalův	k2eAgNnSc1d1	Hannibalovo
vojsko	vojsko	k1gNnSc1	vojsko
bylo	být	k5eAaImAgNnS	být
početně	početně	k6eAd1	početně
silnější	silný	k2eAgMnSc1d2	silnější
než	než	k8xS	než
římské	římský	k2eAgNnSc1d1	římské
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gFnSc1	jeho
jízda	jízda	k1gFnSc1	jízda
byla	být	k5eAaImAgFnS	být
slabší	slabý	k2eAgFnSc1d2	slabší
<g/>
.	.	kIx.	.
</s>
<s>
Hannibal	Hannibat	k5eAaPmAgMnS	Hannibat
sám	sám	k3xTgMnSc1	sám
si	se	k3xPyFc3	se
nedělal	dělat	k5eNaImAgMnS	dělat
žádné	žádný	k3yNgFnPc4	žádný
iluze	iluze	k1gFnPc4	iluze
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
šance	šance	k1gFnSc1	šance
zvítězit	zvítězit	k5eAaPmF	zvítězit
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
je	být	k5eAaImIp3nS	být
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
<g/>
,	,	kIx,	,
čeho	co	k3yInSc2	co
mohl	moct	k5eAaImAgMnS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zmírnění	zmírnění	k1gNnSc4	zmírnění
mírových	mírový	k2eAgFnPc2d1	mírová
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
Kartágo	Kartágo	k1gNnSc4	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
obě	dva	k4xCgNnPc1	dva
nepřátelská	přátelský	k2eNgNnPc1d1	nepřátelské
vojska	vojsko	k1gNnPc1	vojsko
setkala	setkat	k5eAaPmAgNnP	setkat
u	u	k7c2	u
Zamy	Zama	k1gMnSc2	Zama
asi	asi	k9	asi
200	[number]	k4	200
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
,	,	kIx,	,
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
jednou	jednou	k6eAd1	jednou
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soukromé	soukromý	k2eAgFnSc6d1	soukromá
rozmluvě	rozmluva	k1gFnSc6	rozmluva
se	s	k7c7	s
Scipionem	Scipion	k1gInSc7	Scipion
projevil	projevit	k5eAaPmAgInS	projevit
ochotu	ochota	k1gFnSc4	ochota
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
Římané	Říman	k1gMnPc1	Říman
upustí	upustit	k5eAaPmIp3nP	upustit
od	od	k7c2	od
požadovaného	požadovaný	k2eAgInSc2d1	požadovaný
tributu	tribut	k1gInSc2	tribut
a	a	k8xC	a
rozpuštění	rozpuštění	k1gNnSc1	rozpuštění
kartaginské	kartaginský	k2eAgFnSc2d1	kartaginská
válečné	válečný	k2eAgFnSc2d1	válečná
flotily	flotila	k1gFnSc2	flotila
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Scipio	Scipio	k1gMnSc1	Scipio
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
nastal	nastat	k5eAaPmAgInS	nastat
poslední	poslední	k2eAgInSc4d1	poslední
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
boj	boj	k1gInSc4	boj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plánování	plánování	k1gNnSc6	plánování
a	a	k8xC	a
řízení	řízení	k1gNnSc6	řízení
bitvy	bitva	k1gFnSc2	bitva
a	a	k8xC	a
především	především	k9	především
v	v	k7c6	v
dodatečném	dodatečný	k2eAgNnSc6d1	dodatečné
nasazení	nasazení	k1gNnSc6	nasazení
veteránů	veterán	k1gMnPc2	veterán
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
jednou	jednou	k6eAd1	jednou
projevila	projevit	k5eAaPmAgFnS	projevit
Hannibalova	Hannibalův	k2eAgFnSc1d1	Hannibalova
taktická	taktický	k2eAgFnSc1d1	taktická
genialita	genialita	k1gFnSc1	genialita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klíčovém	klíčový	k2eAgInSc6d1	klíčový
momentě	moment	k1gInSc6	moment
bitvy	bitva	k1gFnSc2	bitva
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
kartaginské	kartaginský	k2eAgNnSc1d1	kartaginské
vítězství	vítězství	k1gNnSc1	vítězství
na	na	k7c4	na
dosah	dosah	k1gInSc4	dosah
<g/>
,	,	kIx,	,
však	však	k9	však
vpadla	vpadnout	k5eAaPmAgFnS	vpadnout
Kartagincům	Kartaginec	k1gMnPc3	Kartaginec
do	do	k7c2	do
zad	záda	k1gNnPc2	záda
římská	římský	k2eAgFnSc1d1	římská
jízda	jízda	k1gFnSc1	jízda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
předtím	předtím	k6eAd1	předtím
zdolala	zdolat	k5eAaPmAgFnS	zdolat
kartaginskou	kartaginský	k2eAgFnSc7d1	kartaginská
a	a	k8xC	a
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
tak	tak	k6eAd1	tak
bitvu	bitva	k1gFnSc4	bitva
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Římanů	Říman	k1gMnPc2	Říman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Uzavření	uzavření	k1gNnSc1	uzavření
míru	mír	k1gInSc2	mír
==	==	k?	==
</s>
</p>
<p>
<s>
Touto	tento	k3xDgFnSc7	tento
porážkou	porážka	k1gFnSc7	porážka
definitivně	definitivně	k6eAd1	definitivně
ustal	ustat	k5eAaPmAgInS	ustat
odpor	odpor	k1gInSc4	odpor
Kartaginců	Kartaginec	k1gMnPc2	Kartaginec
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
svého	svůj	k3xOyFgNnSc2	svůj
vítězství	vítězství	k1gNnSc2	vítězství
využili	využít	k5eAaPmAgMnP	využít
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
vystupňování	vystupňování	k1gNnSc3	vystupňování
už	už	k6eAd1	už
tak	tak	k6eAd1	tak
tvrdých	tvrdý	k2eAgFnPc2d1	tvrdá
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Hispánie	Hispánie	k1gFnSc1	Hispánie
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
Kartágo	Kartágo	k1gNnSc4	Kartágo
navždy	navždy	k6eAd1	navždy
ztracena	ztratit	k5eAaPmNgFnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Výše	výše	k1gFnSc1	výše
válečného	válečný	k2eAgNnSc2d1	válečné
odškodnění	odškodnění	k1gNnSc2	odškodnění
byla	být	k5eAaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
na	na	k7c4	na
impozantní	impozantní	k2eAgFnSc4d1	impozantní
částku	částka	k1gFnSc4	částka
10	[number]	k4	10
000	[number]	k4	000
talentů	talent	k1gInPc2	talent
<g/>
,	,	kIx,	,
kartaginské	kartaginský	k2eAgNnSc1d1	kartaginské
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
bylo	být	k5eAaImAgNnS	být
redukováno	redukovat	k5eAaBmNgNnS	redukovat
na	na	k7c4	na
pouhých	pouhý	k2eAgFnPc2d1	pouhá
10	[number]	k4	10
trirém	triréma	k1gFnPc2	triréma
<g/>
,	,	kIx,	,
sloužících	sloužící	k2eAgFnPc6d1	sloužící
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
kartaginského	kartaginský	k2eAgNnSc2d1	kartaginské
pobřeží	pobřeží	k1gNnSc2	pobřeží
před	před	k7c7	před
piráty	pirát	k1gMnPc7	pirát
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
bylo	být	k5eAaImAgNnS	být
Punům	Pun	k1gMnPc3	Pun
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
vést	vést	k5eAaImF	vést
válku	válka	k1gFnSc4	válka
bez	bez	k7c2	bez
svolení	svolení	k1gNnSc2	svolení
římského	římský	k2eAgInSc2d1	římský
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
získali	získat	k5eAaPmAgMnP	získat
Numiďané	Numiďaná	k1gFnSc3	Numiďaná
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
králem	král	k1gMnSc7	král
a	a	k8xC	a
římským	římský	k2eAgMnSc7d1	římský
spojencem	spojenec	k1gMnSc7	spojenec
Massinissou	Massinissý	k2eAgFnSc4d1	Massinissý
<g/>
,	,	kIx,	,
bezvadnou	bezvadný	k2eAgFnSc4d1	bezvadná
příležitost	příležitost	k1gFnSc4	příležitost
k	k	k7c3	k
drancování	drancování	k1gNnSc3	drancování
a	a	k8xC	a
dobývání	dobývání	k1gNnSc4	dobývání
kartaginského	kartaginský	k2eAgNnSc2d1	kartaginské
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Punové	Pun	k1gMnPc1	Pun
tak	tak	k6eAd1	tak
pozbyli	pozbýt	k5eAaPmAgMnP	pozbýt
svou	svůj	k3xOyFgFnSc4	svůj
zahraničněpolitickou	zahraničněpolitický	k2eAgFnSc4d1	zahraničněpolitická
suverenitu	suverenita	k1gFnSc4	suverenita
a	a	k8xC	a
upadli	upadnout	k5eAaPmAgMnP	upadnout
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
druhořadé	druhořadý	k2eAgFnSc2d1	druhořadá
mocnosti	mocnost	k1gFnSc2	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
padesát	padesát	k4xCc4	padesát
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Kartágo	Kartágo	k1gNnSc1	Kartágo
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
numidským	numidský	k2eAgInPc3d1	numidský
vpádům	vpád	k1gInPc3	vpád
zamezit	zamezit	k5eAaPmF	zamezit
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
bylo	být	k5eAaImAgNnS	být
Římany	Říman	k1gMnPc4	Říman
zničeno	zničen	k2eAgNnSc1d1	zničeno
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
punské	punský	k2eAgFnSc6d1	punská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Řím	Řím	k1gInSc1	Řím
učinil	učinit	k5eAaPmAgMnS	učinit
svým	svůj	k3xOyFgNnSc7	svůj
vítězstvím	vítězství	k1gNnSc7	vítězství
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
krok	krok	k1gInSc4	krok
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
dominance	dominance	k1gFnSc2	dominance
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
středomořském	středomořský	k2eAgInSc6d1	středomořský
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
války	válka	k1gFnSc2	válka
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
přijímán	přijímat	k5eAaImNgMnS	přijímat
všemi	všecek	k3xTgFnPc7	všecek
s	s	k7c7	s
nadšením	nadšení	k1gNnSc7	nadšení
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
římský	římský	k2eAgInSc1d1	římský
senát	senát	k1gInSc1	senát
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
Kartágem	Kartágo	k1gNnSc7	Kartágo
<g/>
,	,	kIx,	,
někdejší	někdejší	k2eAgMnSc1d1	někdejší
konzul	konzul	k1gMnSc1	konzul
Quintus	Quintus	k1gMnSc1	Quintus
Caecilius	Caecilius	k1gMnSc1	Caecilius
Metellus	Metellus	k1gMnSc1	Metellus
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepokládá	pokládat	k5eNaImIp3nS	pokládat
ukončení	ukončení	k1gNnSc1	ukončení
války	válka	k1gFnSc2	válka
za	za	k7c4	za
požehnání	požehnání	k1gNnSc4	požehnání
pro	pro	k7c4	pro
Řím	Řím	k1gInSc4	Řím
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
římský	římský	k2eAgInSc1d1	římský
lid	lid	k1gInSc1	lid
znovu	znovu	k6eAd1	znovu
upadne	upadnout	k5eAaPmIp3nS	upadnout
do	do	k7c2	do
oné	onen	k3xDgFnSc2	onen
letargie	letargie	k1gFnSc2	letargie
<g/>
,	,	kIx,	,
z	z	k7c2	z
které	který	k3yRgFnSc2	který
ho	on	k3xPp3gMnSc4	on
dokázala	dokázat	k5eAaPmAgFnS	dokázat
vyburcovat	vyburcovat	k5eAaPmF	vyburcovat
teprve	teprve	k6eAd1	teprve
Hannibalova	Hannibalův	k2eAgFnSc1d1	Hannibalova
přítomnost	přítomnost	k1gFnSc1	přítomnost
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
<g/>
,	,	kIx,	,
především	především	k9	především
Cato	Cato	k6eAd1	Cato
starší	starý	k2eAgMnSc1d2	starší
<g/>
,	,	kIx,	,
zase	zase	k9	zase
žádali	žádat	k5eAaImAgMnP	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
Kartágo	Kartágo	k1gNnSc1	Kartágo
zcela	zcela	k6eAd1	zcela
vyvráceno	vyvrátit	k5eAaPmNgNnS	vyvrátit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
opět	opět	k6eAd1	opět
nabýt	nabýt	k5eAaPmF	nabýt
svojí	svojit	k5eAaImIp3nS	svojit
dřívější	dřívější	k2eAgInSc1d1	dřívější
moc	moc	k6eAd1	moc
a	a	k8xC	a
představovat	představovat	k5eAaImF	představovat
tak	tak	k6eAd1	tak
pro	pro	k7c4	pro
Řím	Řím	k1gInSc4	Řím
znovu	znovu	k6eAd1	znovu
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hannibal	Hannibal	k1gInSc1	Hannibal
se	se	k3xPyFc4	se
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
stal	stát	k5eAaPmAgMnS	stát
politickým	politický	k2eAgMnSc7d1	politický
vůdcem	vůdce	k1gMnSc7	vůdce
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
kartaginská	kartaginský	k2eAgFnSc1d1	kartaginská
aristokracie	aristokracie	k1gFnSc1	aristokracie
<g/>
,	,	kIx,	,
rozčarovaná	rozčarovaný	k2eAgFnSc1d1	rozčarovaná
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
demokratizačních	demokratizační	k2eAgFnPc2d1	demokratizační
snah	snaha	k1gFnPc2	snaha
a	a	k8xC	a
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
<g/>
,	,	kIx,	,
přesvědčila	přesvědčit	k5eAaPmAgFnS	přesvědčit
Římany	Říman	k1gMnPc4	Říman
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ho	on	k3xPp3gMnSc4	on
donutili	donutit	k5eAaPmAgMnP	donutit
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
střetl	střetnout	k5eAaPmAgInS	střetnout
na	na	k7c6	na
bitevním	bitevní	k2eAgNnSc6d1	bitevní
poli	pole	k1gNnSc6	pole
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
někdejšími	někdejší	k2eAgMnPc7d1	někdejší
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
ale	ale	k8xC	ale
dal	dát	k5eAaPmAgMnS	dát
přednost	přednost	k1gFnSc4	přednost
sebevraždě	sebevražda	k1gFnSc3	sebevražda
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
v	v	k7c6	v
římském	římský	k2eAgNnSc6d1	římské
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Kartágo	Kartágo	k1gNnSc1	Kartágo
</s>
</p>
<p>
<s>
Římská	římský	k2eAgFnSc1d1	římská
republika	republika	k1gFnSc1	republika
</s>
</p>
<p>
<s>
Punské	punský	k2eAgFnPc1d1	punská
války	válka	k1gFnPc1	válka
</s>
</p>
<p>
<s>
Hannibal	Hannibal	k1gInSc1	Hannibal
</s>
</p>
<p>
<s>
Scipio	Scipio	k1gMnSc1	Scipio
Africanus	Africanus	k1gMnSc1	Africanus
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
druhá	druhý	k4xOgFnSc1	druhý
punská	punský	k2eAgFnSc1d1	punská
válka	válka	k1gFnSc1	válka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Druhá	druhý	k4xOgFnSc1	druhý
punská	punský	k2eAgFnSc1d1	punská
válka	válka	k1gFnSc1	válka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
punská	punský	k2eAgFnSc1d1	punská
válka	válka	k1gFnSc1	válka
</s>
</p>
<p>
<s>
==	==	k?	==
Bibliografie	bibliografie	k1gFnSc2	bibliografie
==	==	k?	==
</s>
</p>
<p>
<s>
GRANT	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
antického	antický	k2eAgInSc2d1	antický
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
BB	BB	kA	BB
<g/>
/	/	kIx~	/
<g/>
art	art	k?	art
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7341-930-0	[number]	k4	80-7341-930-0
</s>
</p>
<p>
<s>
LIVIUS	LIVIUS	kA	LIVIUS
<g/>
,	,	kIx,	,
Titus	Titus	k1gInSc1	Titus
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
IV	IV	kA	IV
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
</s>
</p>
<p>
<s>
ZAMAROVSKÝ	ZAMAROVSKÝ	kA	ZAMAROVSKÝ
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
psané	psaný	k2eAgNnSc1d1	psané
Římem	Řím	k1gInSc7	Řím
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Perfekt	perfektum	k1gNnPc2	perfektum
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-8046-297-6	[number]	k4	80-8046-297-6
</s>
</p>
