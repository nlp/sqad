<s>
Vodík	vodík	k1gInSc1	vodík
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
H	H	kA	H
latinsky	latinsky	k6eAd1	latinsky
Hydrogenium	hydrogenium	k1gNnSc1	hydrogenium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejlehčí	lehký	k2eAgMnSc1d3	nejlehčí
a	a	k8xC	a
nejjednodušší	jednoduchý	k2eAgInSc1d3	nejjednodušší
plynný	plynný	k2eAgInSc1d1	plynný
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnSc4d1	tvořící
převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
hmoty	hmota	k1gFnSc2	hmota
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
