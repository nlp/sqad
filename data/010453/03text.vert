<p>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
výjevy	výjev	k1gInPc1	výjev
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
originální	originální	k2eAgInSc4d1	originální
dánský	dánský	k2eAgInSc4d1	dánský
název	název	k1gInSc4	název
Nye	Nye	k1gMnSc1	Nye
scener	scener	k1gMnSc1	scener
fra	fra	k?	fra
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dánský	dánský	k2eAgInSc4d1	dánský
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
režiséra	režisér	k1gMnSc2	režisér
Jø	Jø	k1gMnSc2	Jø
Letha	Leth	k1gMnSc2	Leth
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pokračování	pokračování	k1gNnSc4	pokračování
jeho	jeho	k3xOp3gInSc2	jeho
filmu	film	k1gInSc2	film
66	[number]	k4	66
scener	scener	k1gMnSc1	scener
fra	fra	k?	fra
Amerika	Amerika	k1gFnSc1	Amerika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
natáčení	natáčení	k1gNnSc1	natáčení
v	v	k7c6	v
New	New	k1gFnPc6	New
Yorku	York	k1gInSc2	York
probíhalo	probíhat	k5eAaImAgNnS	probíhat
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
před	před	k7c7	před
teroristickými	teroristický	k2eAgInPc7d1	teroristický
útoky	útok	k1gInPc7	útok
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
vedle	vedle	k7c2	vedle
jiných	jiný	k1gMnPc2	jiný
například	například	k6eAd1	například
herec	herec	k1gMnSc1	herec
Dennis	Dennis	k1gFnPc2	Dennis
Hopper	Hopper	k1gMnSc1	Hopper
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
John	John	k1gMnSc1	John
Cale	Cale	k1gFnSc1	Cale
či	či	k8xC	či
básník	básník	k1gMnSc1	básník
John	John	k1gMnSc1	John
Ashbery	Ashbera	k1gFnSc2	Ashbera
<g/>
.	.	kIx.	.
</s>
<s>
Cale	Cale	k6eAd1	Cale
rovněž	rovněž	k9	rovněž
složil	složit	k5eAaPmAgMnS	složit
originální	originální	k2eAgFnSc4d1	originální
hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2003	[number]	k4	2003
na	na	k7c6	na
Mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
festivalu	festival	k1gInSc6	festival
dokumentárních	dokumentární	k2eAgInPc2d1	dokumentární
filmů	film	k1gInPc2	film
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
výjevy	výjev	k1gInPc1	výjev
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Nye	Nye	k?	Nye
scener	scener	k1gInSc1	scener
fra	fra	k?	fra
Amerika	Amerika	k1gFnSc1	Amerika
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
výjevy	výjev	k1gInPc1	výjev
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
