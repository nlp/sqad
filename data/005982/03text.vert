<s>
Rongorongo	Rongorongo	k6eAd1	Rongorongo
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
glyfů	glyf	k1gInPc2	glyf
<g/>
,	,	kIx,	,
objevený	objevený	k2eAgInSc1d1	objevený
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c6	na
Velikonočním	velikonoční	k2eAgInSc6d1	velikonoční
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
proto-písmo	protoísmo	k6eAd1	proto-písmo
či	či	k8xC	či
skutečné	skutečný	k2eAgNnSc4d1	skutečné
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Znaky	znak	k1gInPc1	znak
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
Zipfův	Zipfův	k2eAgInSc4d1	Zipfův
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoli	nikoli	k9	nikoli
postačující	postačující	k2eAgFnSc1d1	postačující
podmínka	podmínka	k1gFnSc1	podmínka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Znaky	znak	k1gInPc1	znak
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
množství	množství	k1gNnSc3	množství
pokusů	pokus	k1gInPc2	pokus
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
nepodařilo	podařit	k5eNaPmAgNnS	podařit
rozluštit	rozluštit	k5eAaPmF	rozluštit
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
několik	několik	k4yIc4	několik
glyfů	glyf	k1gMnPc2	glyf
vytesaných	vytesaný	k2eAgMnPc2d1	vytesaný
do	do	k7c2	do
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc1	všechen
nalezené	nalezený	k2eAgInPc1d1	nalezený
znaky	znak	k1gInPc1	znak
vyřezány	vyřezán	k2eAgInPc1d1	vyřezán
do	do	k7c2	do
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
způsobu	způsob	k1gInSc6	způsob
čtení	čtení	k1gNnSc2	čtení
vět	věta	k1gFnPc2	věta
psaných	psaný	k2eAgFnPc2d1	psaná
písmem	písmo	k1gNnSc7	písmo
Rongorongo	Rongorongo	k6eAd1	Rongorongo
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k6eAd1	mnoho
teorii	teorie	k1gFnSc4	teorie
ovšem	ovšem	k9	ovšem
jedna	jeden	k4xCgFnSc1	jeden
jediná	jediný	k2eAgFnSc1d1	jediná
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
vědecké	vědecký	k2eAgFnPc1d1	vědecká
teorie	teorie	k1gFnPc1	teorie
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
text	text	k1gInSc1	text
např.	např.	kA	např.
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
odspoda	odspoda	k?	odspoda
nahoru	nahoru	k6eAd1	nahoru
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
reverzní	reverzní	k2eAgInSc4d1	reverzní
bustrofédon	bustrofédon	k1gInSc4	bustrofédon
<g/>
.	.	kIx.	.
</s>
<s>
Písmem	písmo	k1gNnSc7	písmo
rongorongo	rongorongo	k6eAd1	rongorongo
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
zapsán	zapsat	k5eAaPmNgInS	zapsat
jazyk	jazyk	k1gInSc1	jazyk
Polynésanů	Polynésan	k1gMnPc2	Polynésan
příbuzný	příbuzný	k2eAgMnSc1d1	příbuzný
k	k	k7c3	k
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
jazyku	jazyk	k1gInSc3	jazyk
Rapa	rap	k1gMnSc2	rap
Nui	Nui	k1gMnSc2	Nui
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Stáří	stáří	k1gNnSc1	stáří
písma	písmo	k1gNnSc2	písmo
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
existuje	existovat	k5eAaImIp3nS	existovat
domněnka	domněnka	k1gFnSc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
písmo	písmo	k1gNnSc1	písmo
bylo	být	k5eAaImAgNnS	být
převedené	převedený	k2eAgNnSc1d1	převedené
na	na	k7c6	na
Rapa	rap	k1gMnSc2	rap
Nui	Nui	k1gFnSc6	Nui
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
také	také	k9	také
že	že	k8xS	že
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
výsledek	výsledek	k1gInSc4	výsledek
kontaktů	kontakt	k1gInPc2	kontakt
Evropanů	Evropan	k1gMnPc2	Evropan
s	s	k7c7	s
Polynésany	Polynésan	k1gMnPc7	Polynésan
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Také	také	k9	také
existuje	existovat	k5eAaImIp3nS	existovat
domněnka	domněnka	k1gFnSc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
první	první	k4xOgMnSc1	první
obyvatel	obyvatel	k1gMnSc1	obyvatel
legendárního	legendární	k2eAgInSc2d1	legendární
Hotu	Hotus	k1gInSc2	Hotus
Matu	mást	k5eAaImIp1nS	mást
<g/>
'	'	kIx"	'
<g/>
a	a	k8xC	a
donesl	donést	k5eAaPmAgInS	donést
67	[number]	k4	67
destiček	destička	k1gFnPc2	destička
s	s	k7c7	s
nápisy	nápis	k1gInPc7	nápis
rongorongo	rongorongo	k6eAd1	rongorongo
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mnoho	mnoho	k4c4	mnoho
současných	současný	k2eAgFnPc2d1	současná
teorií	teorie	k1gFnPc2	teorie
se	se	k3xPyFc4	se
také	také	k9	také
točí	točit	k5eAaImIp3nS	točit
kolem	kolem	k7c2	kolem
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
písmo	písmo	k1gNnSc1	písmo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
až	až	k6eAd1	až
jako	jako	k8xC	jako
kopie	kopie	k1gFnSc1	kopie
evropského	evropský	k2eAgNnSc2d1	Evropské
písma	písmo	k1gNnSc2	písmo
po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
ostrova	ostrov	k1gInSc2	ostrov
Evropany	Evropan	k1gMnPc4	Evropan
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tabulky	tabulka	k1gFnPc1	tabulka
písma	písmo	k1gNnSc2	písmo
rongorongo	rongorongo	k6eAd1	rongorongo
se	se	k3xPyFc4	se
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
a	a	k8xC	a
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
nedostatek	nedostatek	k1gInSc1	nedostatek
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Velikonočního	velikonoční	k2eAgInSc2d1	velikonoční
ostrova	ostrov	k1gInSc2	ostrov
byli	být	k5eAaImAgMnP	být
potomky	potomek	k1gMnPc4	potomek
polynéských	polynéský	k2eAgInPc2d1	polynéský
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
osídlily	osídlit	k5eAaPmAgFnP	osídlit
velké	velký	k2eAgFnPc4d1	velká
plochy	plocha	k1gFnPc4	plocha
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
písmo	písmo	k1gNnSc1	písmo
rongorongo	rongorongo	k6eAd1	rongorongo
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
polynéský	polynéský	k2eAgInSc4d1	polynéský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
proti	proti	k7c3	proti
tomuto	tento	k3xDgInSc3	tento
názoru	názor	k1gInSc3	názor
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
izolace	izolace	k1gFnSc1	izolace
Velikonočního	velikonoční	k2eAgInSc2d1	velikonoční
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
takový	takový	k3xDgInSc1	takový
jazyk	jazyk	k1gInSc1	jazyk
jistě	jistě	k9	jistě
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
destičky	destička	k1gFnPc1	destička
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
vlastní	vlastní	k2eAgInSc4d1	vlastní
a	a	k8xC	a
izolovaný	izolovaný	k2eAgInSc4d1	izolovaný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
rozluštění	rozluštění	k1gNnSc2	rozluštění
písma	písmo	k1gNnSc2	písmo
je	být	k5eAaImIp3nS	být
nemožné	možný	k2eNgFnPc4d1	nemožná
teorie	teorie	k1gFnPc4	teorie
ověřit	ověřit	k5eAaPmF	ověřit
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
toho	ten	k3xDgNnSc2	ten
nevíme	vědět	k5eNaImIp1nP	vědět
ani	ani	k8xC	ani
o	o	k7c4	o
rozvíjení	rozvíjení	k1gNnSc4	rozvíjení
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
písem	písmo	k1gNnPc2	písmo
v	v	k7c6	v
Tichooceánské	tichooceánský	k2eAgFnSc6d1	Tichooceánská
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
text	text	k1gInSc1	text
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stále	stále	k6eAd1	stále
záhadou	záhada	k1gFnSc7	záhada
<g/>
.	.	kIx.	.
</s>
