<s>
Roman	Roman	k1gMnSc1	Roman
Onderka	Onderka	k1gMnSc1	Onderka
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1965	[number]	k4	1965
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
až	až	k9	až
2011	[number]	k4	2011
místopředseda	místopředseda	k1gMnSc1	místopředseda
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2006	[number]	k4	2006
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
primátor	primátor	k1gMnSc1	primátor
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
zastupitel	zastupitel	k1gMnSc1	zastupitel
města	město	k1gNnSc2	město
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002	[number]	k4	2002
až	až	k9	až
2006	[number]	k4	2006
zastupitel	zastupitel	k1gMnSc1	zastupitel
Městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-Starý	Brno-Starý	k2eAgInSc1d1	Brno-Starý
Lískovec	Lískovec	k1gInSc1	Lískovec
<g/>
.	.	kIx.	.
</s>
