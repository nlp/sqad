<s>
Roman	Roman	k1gMnSc1	Roman
Onderka	Onderka	k1gMnSc1	Onderka
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1965	[number]	k4	1965
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
až	až	k9	až
2011	[number]	k4	2011
místopředseda	místopředseda	k1gMnSc1	místopředseda
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2006	[number]	k4	2006
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
primátor	primátor	k1gMnSc1	primátor
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
zastupitel	zastupitel	k1gMnSc1	zastupitel
města	město	k1gNnSc2	město
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002	[number]	k4	2002
až	až	k9	až
2006	[number]	k4	2006
zastupitel	zastupitel	k1gMnSc1	zastupitel
Městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-Starý	Brno-Starý	k2eAgInSc1d1	Brno-Starý
Lískovec	Lískovec	k1gInSc1	Lískovec
<g/>
.	.	kIx.	.
</s>
<s>
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Fakulta	fakulta	k1gFnSc1	fakulta
podnikatelská	podnikatelský	k2eAgFnSc1d1	podnikatelská
VUT	VUT	kA	VUT
<g/>
:	:	kIx,	:
program	program	k1gInSc1	program
Master	master	k1gMnSc1	master
of	of	k?	of
Business	business	k1gInSc1	business
Administration	Administration	k1gInSc1	Administration
<g/>
,	,	kIx,	,
validováno	validován	k2eAgNnSc1d1	validováno
Nottingham	Nottingham	k1gInSc4	Nottingham
Trent	Trent	k1gInSc4	Trent
University	universita	k1gFnSc2	universita
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
Karla	Karel	k1gMnSc2	Karel
Engliše	Engliš	k1gMnSc2	Engliš
<g/>
:	:	kIx,	:
bakalářské	bakalářský	k2eAgNnSc1d1	bakalářské
studium	studium	k1gNnSc1	studium
ve	v	k7c6	v
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
Ekonomika	ekonomika	k1gFnSc1	ekonomika
a	a	k8xC	a
management	management	k1gInSc1	management
<g/>
,	,	kIx,	,
obor	obor	k1gInSc1	obor
Ekonomika	ekonomika	k1gFnSc1	ekonomika
a	a	k8xC	a
právo	právo	k1gNnSc1	právo
v	v	k7c6	v
podnikání	podnikání	k1gNnSc6	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Akreditační	akreditační	k2eAgFnSc2d1	akreditační
komise	komise	k1gFnSc2	komise
byla	být	k5eAaImAgFnS	být
bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
nevyhovující	vyhovující	k2eNgFnSc1d1	nevyhovující
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
Vladimíra	Vladimíra	k1gFnSc1	Vladimíra
Dvořáková	Dvořáková	k1gFnSc1	Dvořáková
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
práce	práce	k1gFnSc2	práce
k	k	k7c3	k
obhajobě	obhajoba	k1gFnSc3	obhajoba
neměla	mít	k5eNaImAgFnS	mít
být	být	k5eAaImF	být
ani	ani	k8xC	ani
připuštěna	připuštěn	k2eAgFnSc1d1	připuštěna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
reportér	reportér	k1gMnSc1	reportér
ČT	ČT	kA	ČT
Michal	Michal	k1gMnSc1	Michal
Šebela	Šebela	k1gMnSc1	Šebela
pokusil	pokusit	k5eAaPmAgMnS	pokusit
získat	získat	k5eAaPmF	získat
jeho	jeho	k3xOp3gNnSc4	jeho
vyjádření	vyjádření	k1gNnSc4	vyjádření
<g/>
,	,	kIx,	,
brněnský	brněnský	k2eAgMnSc1d1	brněnský
primátor	primátor	k1gMnSc1	primátor
nejdříve	dříve	k6eAd3	dříve
mlčel	mlčet	k5eAaImAgMnS	mlčet
a	a	k8xC	a
poté	poté	k6eAd1	poté
reportéra	reportér	k1gMnSc2	reportér
obvinil	obvinit	k5eAaPmAgMnS	obvinit
z	z	k7c2	z
porušování	porušování	k1gNnSc2	porušování
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2012	[number]	k4	2012
rozeslal	rozeslat	k5eAaPmAgMnS	rozeslat
anonym	anonym	k1gMnSc1	anonym
médiím	médium	k1gNnPc3	médium
e-mail	eail	k1gInSc4	e-mail
obsahující	obsahující	k2eAgInSc4d1	obsahující
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
i	i	k8xC	i
Onderkovu	Onderkův	k2eAgFnSc4d1	Onderkova
bakalářskou	bakalářský	k2eAgFnSc4d1	Bakalářská
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozvedený	rozvedený	k2eAgMnSc1d1	rozvedený
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
Romana	Roman	k1gMnSc4	Roman
a	a	k8xC	a
Zbyňka	Zbyněk	k1gMnSc4	Zbyněk
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
primátorem	primátor	k1gMnSc7	primátor
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zastává	zastávat	k5eAaImIp3nS	zastávat
(	(	kIx(	(
<g/>
či	či	k8xC	či
zastával	zastávat	k5eAaImAgMnS	zastávat
<g/>
)	)	kIx)	)
funkce	funkce	k1gFnSc1	funkce
<g/>
:	:	kIx,	:
stínový	stínový	k2eAgMnSc1d1	stínový
ministr	ministr	k1gMnSc1	ministr
dopravy	doprava	k1gFnSc2	doprava
za	za	k7c4	za
ČSSD	ČSSD	kA	ČSSD
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
místopředseda	místopředseda	k1gMnSc1	místopředseda
ČSSD	ČSSD	kA	ČSSD
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
místopředseda	místopředseda	k1gMnSc1	místopředseda
MěVV	MěVV	k1gFnSc2	MěVV
ČSSD	ČSSD	kA	ČSSD
Brno-město	Brnoěsta	k1gMnSc5	Brno-města
Byl	být	k5eAaImAgInS	být
mj.	mj.	kA	mj.
iniciátorem	iniciátor	k1gInSc7	iniciátor
postavení	postavení	k1gNnSc2	postavení
tzv.	tzv.	kA	tzv.
brněnského	brněnský	k2eAgInSc2d1	brněnský
orloje	orloj	k1gInSc2	orloj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2013	[number]	k4	2013
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
lídrem	lídr	k1gMnSc7	lídr
ČSSD	ČSSD	kA	ČSSD
pro	pro	k7c4	pro
komunální	komunální	k2eAgFnPc4d1	komunální
volby	volba	k1gFnPc4	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
do	do	k7c2	do
Zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
i	i	k8xC	i
kandidátem	kandidát	k1gMnSc7	kandidát
ČSSD	ČSSD	kA	ČSSD
na	na	k7c4	na
post	post	k1gInSc4	post
primátora	primátor	k1gMnSc2	primátor
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
se	se	k3xPyFc4	se
ucházel	ucházet	k5eAaImAgInS	ucházet
již	již	k6eAd1	již
potřetí	potřetí	k4xO	potřetí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnových	říjnový	k2eAgFnPc6d1	říjnová
volbách	volba	k1gFnPc6	volba
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stal	stát	k5eAaPmAgInS	stát
už	už	k6eAd1	už
po	po	k7c6	po
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
zastupitelem	zastupitel	k1gMnSc7	zastupitel
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
ČSSD	ČSSD	kA	ČSSD
však	však	k9	však
zůstala	zůstat	k5eAaPmAgFnS	zůstat
mimo	mimo	k7c4	mimo
radniční	radniční	k2eAgFnSc4d1	radniční
koalici	koalice	k1gFnSc4	koalice
a	a	k8xC	a
primátorem	primátor	k1gMnSc7	primátor
se	s	k7c7	s
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
stal	stát	k5eAaPmAgMnS	stát
Petr	Petr	k1gMnSc1	Petr
Vokřál	Vokřál	k1gMnSc1	Vokřál
z	z	k7c2	z
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Roman	Roman	k1gMnSc1	Roman
Onderka	Onderka	k1gMnSc1	Onderka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osobní	osobní	k2eAgFnSc1d1	osobní
stránka	stránka	k1gFnSc1	stránka
Roman	Roman	k1gMnSc1	Roman
Onderka	Onderka	k1gMnSc1	Onderka
pro	pro	k7c4	pro
rádio	rádio	k1gNnSc4	rádio
Impuls	impuls	k1gInSc1	impuls
<g/>
,	,	kIx,	,
Impulsy	impuls	k1gInPc1	impuls
Václava	Václav	k1gMnSc2	Václav
Moravce	Moravec	k1gMnSc2	Moravec
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
</s>
