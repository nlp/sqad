<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
svislých	svislý	k2eAgInPc2d1	svislý
pruhů	pruh	k1gInPc2	pruh
–	–	k?	–
levého	levý	k2eAgNnSc2d1	levé
zeleného	zelený	k2eAgNnSc2d1	zelené
a	a	k8xC	a
pravého	pravý	k2eAgNnSc2d1	pravé
červeného	červené	k1gNnSc2	červené
<g/>
.	.	kIx.	.
</s>
<s>
Pruhy	pruh	k1gInPc1	pruh
jsou	být	k5eAaImIp3nP	být
nestejné	stejný	k2eNgFnSc2d1	nestejná
šíře	šíř	k1gFnSc2	šíř
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zelený	zelený	k2eAgInSc1d1	zelený
zabírá	zabírat	k5eAaImIp3nS	zabírat
dvě	dva	k4xCgFnPc4	dva
a	a	k8xC	a
červený	červený	k2eAgInSc1d1	červený
tři	tři	k4xCgFnPc1	tři
pětiny	pětina	k1gFnPc1	pětina
délky	délka	k1gFnSc2	délka
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
znaku	znak	k1gInSc2	znak
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
obou	dva	k4xCgFnPc2	dva
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
<g/>
Červená	červený	k2eAgFnSc1d1	červená
část	část	k1gFnSc1	část
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
portugalskou	portugalský	k2eAgFnSc4d1	portugalská
revoluci	revoluce	k1gFnSc4	revoluce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
a	a	k8xC	a
zelená	zelený	k2eAgFnSc1d1	zelená
část	část	k1gFnSc1	část
naději	nadát	k5eAaBmIp1nS	nadát
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
znak	znak	k1gInSc1	znak
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pět	pět	k4xCc1	pět
malých	malý	k2eAgInPc2d1	malý
modrých	modrý	k2eAgInPc2d1	modrý
znaků	znak	k1gInPc2	znak
s	s	k7c7	s
pěti	pět	k4xCc7	pět
bílými	bílý	k2eAgInPc7d1	bílý
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Modré	modrý	k2eAgInPc1d1	modrý
znaky	znak	k1gInPc1	znak
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
prvního	první	k4xOgMnSc4	první
portugalského	portugalský	k2eAgMnSc4d1	portugalský
krále	král	k1gMnSc4	král
Alfonse	Alfons	k1gMnSc4	Alfons
Jindřicha	Jindřich	k1gMnSc4	Jindřich
I.	I.	kA	I.
<g/>
,	,	kIx,	,
vítěze	vítěz	k1gMnSc2	vítěz
nad	nad	k7c7	nad
Maury	Maur	k1gMnPc7	Maur
<g/>
,	,	kIx,	,
a	a	k8xC	a
božská	božský	k2eAgFnSc1d1	božská
pomoc	pomoc	k1gFnSc1	pomoc
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
přitom	přitom	k6eAd1	přitom
dostalo	dostat	k5eAaPmAgNnS	dostat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
představována	představovat	k5eAaImNgFnS	představovat
pěti	pět	k4xCc7	pět
bílými	bílý	k2eAgInPc7d1	bílý
body	bod	k1gInPc7	bod
jako	jako	k8xC	jako
pěti	pět	k4xCc7	pět
Kristovými	Kristův	k2eAgFnPc7d1	Kristova
ranami	rána	k1gFnPc7	rána
<g/>
.	.	kIx.	.
</s>
<s>
Seskupení	seskupení	k1gNnSc1	seskupení
znaků	znak	k1gInPc2	znak
do	do	k7c2	do
kříže	kříž	k1gInSc2	kříž
pak	pak	k6eAd1	pak
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgInSc4d1	červený
okraj	okraj	k1gInSc4	okraj
znaku	znak	k1gInSc2	znak
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
sedm	sedm	k4xCc1	sedm
hradů	hrad	k1gInPc2	hrad
představuje	představovat	k5eAaImIp3nS	představovat
rozsah	rozsah	k1gInSc1	rozsah
portugalského	portugalský	k2eAgNnSc2d1	portugalské
území	území	k1gNnSc2	území
včetně	včetně	k7c2	včetně
provincie	provincie	k1gFnSc2	provincie
Algarve	Algarev	k1gFnSc2	Algarev
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
navigační	navigační	k2eAgInSc1d1	navigační
přístroj	přístroj	k1gInSc1	přístroj
–	–	k?	–
armilární	armilární	k2eAgFnSc1d1	armilární
sféra	sféra	k1gFnSc1	sféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
oslavou	oslava	k1gFnSc7	oslava
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Mořeplavce	mořeplavec	k1gMnSc2	mořeplavec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
inicioval	iniciovat	k5eAaBmAgInS	iniciovat
zámořské	zámořský	k2eAgFnPc4d1	zámořská
plavby	plavba	k1gFnPc4	plavba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
z	z	k7c2	z
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
učinily	učinit	k5eAaImAgFnP	učinit
kdysi	kdysi	k6eAd1	kdysi
koloniální	koloniální	k2eAgNnSc4d1	koloniální
impérium	impérium	k1gNnSc4	impérium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Portugalská	portugalský	k2eAgFnSc1d1	portugalská
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
užívána	užívat	k5eAaImNgFnS	užívat
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
od	od	k7c2	od
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1910	[number]	k4	1910
svrhla	svrhnout	k5eAaPmAgFnS	svrhnout
monarchii	monarchie	k1gFnSc4	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
oficiálně	oficiálně	k6eAd1	oficiálně
deklaruje	deklarovat	k5eAaBmIp3nS	deklarovat
svou	svůj	k3xOyFgFnSc4	svůj
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c4	na
království	království	k1gNnSc4	království
Léon	Léona	k1gFnPc2	Léona
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1128	[number]	k4	1128
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1170	[number]	k4	1170
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
nezávislost	nezávislost	k1gFnSc1	nezávislost
uznána	uznat	k5eAaPmNgFnS	uznat
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
</s>
<s>
Symboly	symbol	k1gInPc1	symbol
na	na	k7c6	na
portugalské	portugalský	k2eAgFnSc6d1	portugalská
vlajce	vlajka	k1gFnSc6	vlajka
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
k	k	k7c3	k
dvanáctému	dvanáctý	k4xOgNnSc3	dvanáctý
století	století	k1gNnSc3	století
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
znak	znak	k1gInSc1	znak
s	s	k7c7	s
pěti	pět	k4xCc7	pět
modrými	modrý	k2eAgInPc7d1	modrý
znaky	znak	k1gInPc7	znak
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
králem	král	k1gMnSc7	král
Alfonsem	Alfons	k1gMnSc7	Alfons
Jindřichem	Jindřich	k1gMnSc7	Jindřich
I.	I.	kA	I.
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Maurů	Maur	k1gMnPc2	Maur
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc4	vytvoření
Křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
království	království	k1gNnSc2	království
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1139	[number]	k4	1139
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlajky	vlajka	k1gFnPc1	vlajka
portugalských	portugalský	k2eAgFnPc2d1	portugalská
kolonií	kolonie	k1gFnPc2	kolonie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
portugalský	portugalský	k2eAgMnSc1d1	portugalský
heraldik	heraldik	k1gMnSc1	heraldik
Franz	Franz	k1gMnSc1	Franz
Paul	Paul	k1gMnSc1	Paul
Almeido	Almeida	k1gFnSc5	Almeida
vlajky	vlajka	k1gFnPc4	vlajka
portugalských	portugalský	k2eAgNnPc2d1	portugalské
závislých	závislý	k2eAgNnPc2d1	závislé
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
schváleny	schválen	k2eAgInPc1d1	schválen
<g/>
.	.	kIx.	.
</s>
<s>
Vlajky	vlajka	k1gFnPc4	vlajka
těchto	tento	k3xDgNnPc2	tento
území	území	k1gNnPc2	území
ale	ale	k8xC	ale
nebyly	být	k5eNaImAgFnP	být
nikdy	nikdy	k6eAd1	nikdy
zavedeny	zaveden	k2eAgFnPc1d1	zavedena
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
užívaly	užívat	k5eAaImAgFnP	užívat
portugalské	portugalský	k2eAgFnPc1d1	portugalská
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
není	být	k5eNaImIp3nS	být
kompletní	kompletní	k2eAgInSc1d1	kompletní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnSc2	vlajka
portugalských	portugalský	k2eAgInPc2d1	portugalský
autonomních	autonomní	k2eAgInPc2d1	autonomní
regionů	region	k1gInPc2	region
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
republikánské	republikánský	k2eAgFnSc6d1	republikánská
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
množství	množství	k1gNnSc1	množství
návrhů	návrh	k1gInPc2	návrh
vlajky	vlajka	k1gFnSc2	vlajka
vznikající	vznikající	k2eAgFnSc2d1	vznikající
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnSc7d1	zásadní
otázkou	otázka	k1gFnSc7	otázka
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
převzít	převzít	k5eAaPmF	převzít
modrou	modrý	k2eAgFnSc7d1	modrá
a	a	k8xC	a
bílou	bílý	k2eAgFnSc7d1	bílá
jako	jako	k8xC	jako
barvy	barva	k1gFnPc4	barva
bývalého	bývalý	k2eAgNnSc2d1	bývalé
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
červenou	červený	k2eAgFnSc4d1	červená
a	a	k8xC	a
zelenou	zelený	k2eAgFnSc4d1	zelená
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
užívala	užívat	k5eAaImAgFnS	užívat
republikánská	republikánský	k2eAgFnSc1d1	republikánská
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
zeleno-červenou	zeleno-červený	k2eAgFnSc4d1	zeleno-červená
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
</s>
</p>
<p>
<s>
Portugalská	portugalský	k2eAgFnSc1d1	portugalská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Portugalská	portugalský	k2eAgFnSc1d1	portugalská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
