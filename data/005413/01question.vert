<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
produkují	produkovat	k5eAaImIp3nP	produkovat
z	z	k7c2	z
biomasy	biomasa	k1gFnSc2	biomasa
výrobky	výrobek	k1gInPc1	výrobek
získávané	získávaný	k2eAgInPc1d1	získávaný
obvykle	obvykle	k6eAd1	obvykle
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
ropy	ropa	k1gFnSc2	ropa
<g/>
?	?	kIx.	?
</s>
