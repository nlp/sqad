<s>
Biomasa	biomasa	k1gFnSc1	biomasa
je	být	k5eAaImIp3nS	být
souhrn	souhrn	k1gInSc4	souhrn
látek	látka	k1gFnPc2	látka
tvořících	tvořící	k2eAgFnPc2d1	tvořící
těla	tělo	k1gNnSc2	tělo
všech	všecek	k3xTgInPc2	všecek
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
sinic	sinice	k1gFnPc2	sinice
a	a	k8xC	a
hub	houba	k1gFnPc2	houba
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
pojmem	pojem	k1gInSc7	pojem
často	často	k6eAd1	často
označujeme	označovat	k5eAaImIp1nP	označovat
rostlinnou	rostlinný	k2eAgFnSc4d1	rostlinná
biomasu	biomasa	k1gFnSc4	biomasa
využitelnou	využitelný	k2eAgFnSc4d1	využitelná
pro	pro	k7c4	pro
energetické	energetický	k2eAgInPc4d1	energetický
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
biomasy	biomasa	k1gFnSc2	biomasa
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
prapůvod	prapůvod	k1gInSc4	prapůvod
ve	v	k7c6	v
slunečním	sluneční	k2eAgNnSc6d1	sluneční
záření	záření	k1gNnSc6	záření
a	a	k8xC	a
fotosyntéze	fotosyntéza	k1gFnSc6	fotosyntéza
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
obnovitelný	obnovitelný	k2eAgInSc4d1	obnovitelný
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
hmotnost	hmotnost	k1gFnSc1	hmotnost
biomasy	biomasa	k1gFnSc2	biomasa
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
stanovena	stanovit	k5eAaPmNgFnS	stanovit
vážením	vážení	k1gNnSc7	vážení
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
též	též	k9	též
odhadem	odhad	k1gInSc7	odhad
z	z	k7c2	z
objemu	objem	k1gInSc2	objem
nebo	nebo	k8xC	nebo
délky	délka	k1gFnSc2	délka
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
čerstvě	čerstvě	k6eAd1	čerstvě
nalovených	nalovený	k2eAgInPc2d1	nalovený
organismů	organismus	k1gInPc2	organismus
je	být	k5eAaImIp3nS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
živá	živý	k2eAgFnSc1d1	živá
nebo	nebo	k8xC	nebo
čerstvá	čerstvý	k2eAgFnSc1d1	čerstvá
biomasa	biomasa	k1gFnSc1	biomasa
<g/>
.	.	kIx.	.
</s>
<s>
Přesnější	přesný	k2eAgNnSc1d2	přesnější
je	být	k5eAaImIp3nS	být
stanovení	stanovení	k1gNnSc1	stanovení
biomasy	biomasa	k1gFnSc2	biomasa
suché	suchý	k2eAgFnSc2d1	suchá
(	(	kIx(	(
<g/>
sušiny	sušina	k1gFnSc2	sušina
<g/>
)	)	kIx)	)
a	a	k8xC	a
sušiny	sušina	k1gFnSc2	sušina
bez	bez	k7c2	bez
popelovin	popelovina	k1gFnPc2	popelovina
<g/>
.	.	kIx.	.
</s>
<s>
Energetická	energetický	k2eAgFnSc1d1	energetická
hodnota	hodnota	k1gFnSc1	hodnota
biomasy	biomasa	k1gFnSc2	biomasa
je	být	k5eAaImIp3nS	být
stanovena	stanoven	k2eAgFnSc1d1	stanovena
buď	buď	k8xC	buď
spálením	spálení	k1gNnSc7	spálení
v	v	k7c6	v
joulometru	joulometr	k1gInSc6	joulometr
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
základě	základ	k1gInSc6	základ
podílu	podíl	k1gInSc2	podíl
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
cukrů	cukr	k1gInPc2	cukr
a	a	k8xC	a
tuků	tuk	k1gInPc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Biomasa	biomasa	k1gFnSc1	biomasa
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ekologii	ekologie	k1gFnSc6	ekologie
termín	termín	k1gInSc1	termín
definovaný	definovaný	k2eAgInSc1d1	definovaný
jako	jako	k8xS	jako
úhrn	úhrn	k1gInSc1	úhrn
hmoty	hmota	k1gFnSc2	hmota
jedinců	jedinec	k1gMnPc2	jedinec
určitého	určitý	k2eAgInSc2d1	určitý
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
skupiny	skupina	k1gFnSc2	skupina
druhů	druh	k1gInPc2	druh
nebo	nebo	k8xC	nebo
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
společenstva	společenstvo	k1gNnSc2	společenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Úhrn	úhrn	k1gInSc1	úhrn
je	být	k5eAaImIp3nS	být
součtem	součet	k1gInSc7	součet
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
na	na	k7c6	na
dané	daný	k2eAgFnSc6d1	daná
ploše	plocha	k1gFnSc6	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rozlišen	rozlišen	k2eAgInSc4d1	rozlišen
stav	stav	k1gInSc4	stav
daných	daný	k2eAgInPc2d1	daný
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
možnosti	možnost	k1gFnPc4	možnost
technického	technický	k2eAgNnSc2d1	technické
využití	využití	k1gNnSc2	využití
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
takto	takto	k6eAd1	takto
rozlišována	rozlišován	k2eAgFnSc1d1	rozlišována
biomasa	biomasa	k1gFnSc1	biomasa
podzemní	podzemní	k2eAgFnSc1d1	podzemní
nebo	nebo	k8xC	nebo
nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
<g/>
,	,	kIx,	,
biomasa	biomasa	k1gFnSc1	biomasa
suchá	suchý	k2eAgFnSc1d1	suchá
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vegetativním	vegetativní	k2eAgInSc6d1	vegetativní
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterých	který	k3yIgNnPc6	který
jsou	být	k5eAaImIp3nP	být
vyjadřovány	vyjadřován	k2eAgFnPc4d1	vyjadřována
tyto	tento	k3xDgFnPc4	tento
veličiny	veličina	k1gFnPc4	veličina
je	být	k5eAaImIp3nS	být
celková	celkový	k2eAgFnSc1d1	celková
hmotnost	hmotnost	k1gFnSc1	hmotnost
sušiny	sušina	k1gFnSc2	sušina
nebo	nebo	k8xC	nebo
objemové	objemový	k2eAgFnPc1d1	objemová
jednotky	jednotka	k1gFnPc1	jednotka
(	(	kIx(	(
<g/>
litr	litr	k1gInSc1	litr
<g/>
,	,	kIx,	,
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
u	u	k7c2	u
čerstvé	čerstvý	k2eAgFnSc2d1	čerstvá
hmotnosti	hmotnost	k1gFnSc2	hmotnost
je	být	k5eAaImIp3nS	být
používáno	používán	k2eAgNnSc1d1	používáno
jednotek	jednotka	k1gFnPc2	jednotka
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
joule	joule	k1gInSc1	joule
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klimatologie	klimatologie	k1gFnSc1	klimatologie
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
souvislosti	souvislost	k1gFnPc4	souvislost
mezi	mezi	k7c7	mezi
změnami	změna	k1gFnPc7	změna
klimatu	klima	k1gNnSc2	klima
a	a	k8xC	a
nárůstem	nárůst	k1gInSc7	nárůst
či	či	k8xC	či
poklesem	pokles	k1gInSc7	pokles
tvorby	tvorba	k1gFnSc2	tvorba
určité	určitý	k2eAgFnSc2d1	určitá
složky	složka	k1gFnSc2	složka
biomasy	biomasa	k1gFnSc2	biomasa
(	(	kIx(	(
<g/>
např.	např.	kA	např.
rostlin	rostlina	k1gFnPc2	rostlina
nebo	nebo	k8xC	nebo
řas	řasa	k1gFnPc2	řasa
nebo	nebo	k8xC	nebo
organismů	organismus	k1gInPc2	organismus
v	v	k7c6	v
krychlovém	krychlový	k2eAgInSc6d1	krychlový
metru	metr	k1gInSc6	metr
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
například	například	k6eAd1	například
o	o	k7c4	o
vliv	vliv	k1gInSc4	vliv
globálního	globální	k2eAgNnSc2d1	globální
oteplení	oteplení	k1gNnSc2	oteplení
nebo	nebo	k8xC	nebo
snížení	snížení	k1gNnSc2	snížení
prostupnosti	prostupnost	k1gFnSc2	prostupnost
atmosféry	atmosféra	k1gFnSc2	atmosféra
pro	pro	k7c4	pro
sluneční	sluneční	k2eAgNnSc4d1	sluneční
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Energetické	energetický	k2eAgNnSc1d1	energetické
využití	využití	k1gNnSc1	využití
biomasy	biomasa	k1gFnSc2	biomasa
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
<g/>
:	:	kIx,	:
Rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
především	především	k6eAd1	především
zbytkovou	zbytkový	k2eAgFnSc4d1	zbytková
(	(	kIx(	(
<g/>
odpadní	odpadní	k2eAgFnSc4d1	odpadní
<g/>
)	)	kIx)	)
biomasu	biomasa	k1gFnSc4	biomasa
-	-	kIx~	-
dřevní	dřevní	k2eAgInPc1d1	dřevní
odpady	odpad	k1gInPc1	odpad
z	z	k7c2	z
lesního	lesní	k2eAgNnSc2d1	lesní
hospodářství	hospodářství	k1gNnSc2	hospodářství
a	a	k8xC	a
celulózo-papírenského	celulózoapírenský	k2eAgMnSc2d1	celulózo-papírenský
<g/>
,	,	kIx,	,
dřevařského	dřevařský	k2eAgInSc2d1	dřevařský
a	a	k8xC	a
nábytkářského	nábytkářský	k2eAgInSc2d1	nábytkářský
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
zbytky	zbytek	k1gInPc4	zbytek
ze	z	k7c2	z
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
prvovýroby	prvovýroba	k1gFnSc2	prvovýroba
a	a	k8xC	a
údržby	údržba	k1gFnSc2	údržba
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
komunální	komunální	k2eAgInSc4d1	komunální
bioodpad	bioodpad	k1gInSc4	bioodpad
a	a	k8xC	a
odpady	odpad	k1gInPc4	odpad
z	z	k7c2	z
potravinářského	potravinářský	k2eAgInSc2d1	potravinářský
průmyslu	průmysl	k1gInSc2	průmysl
-	-	kIx~	-
a	a	k8xC	a
cíleně	cíleně	k6eAd1	cíleně
pěstovanou	pěstovaný	k2eAgFnSc4d1	pěstovaná
biomasu	biomasa	k1gFnSc4	biomasa
-	-	kIx~	-
energetické	energetický	k2eAgFnPc4d1	energetická
byliny	bylina	k1gFnPc4	bylina
a	a	k8xC	a
rychlerostoucí	rychlerostoucí	k2eAgFnPc4d1	rychlerostoucí
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Statková	statkový	k2eAgNnPc1d1	statkové
hnojiva	hnojivo	k1gNnPc1	hnojivo
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
§	§	k?	§
2	[number]	k4	2
zák	zák	k?	zák
<g/>
.	.	kIx.	.
č.	č.	k?	č.
308	[number]	k4	308
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
o	o	k7c6	o
hnojivech	hnojivo	k1gNnPc6	hnojivo
<g/>
"	"	kIx"	"
hnůj	hnůj	k1gInSc1	hnůj
<g/>
,	,	kIx,	,
hnojůvka	hnojůvka	k1gFnSc1	hnojůvka
<g/>
,	,	kIx,	,
močůvka	močůvka	k1gFnSc1	močůvka
<g/>
,	,	kIx,	,
kejda	kejda	k1gFnSc1	kejda
<g/>
,	,	kIx,	,
sláma	sláma	k1gFnSc1	sláma
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
zbytky	zbytek	k1gInPc4	zbytek
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
původu	původ	k1gInSc2	původ
vznikající	vznikající	k2eAgFnSc2d1	vznikající
zejména	zejména	k9	zejména
v	v	k7c6	v
zemědělské	zemědělský	k2eAgFnSc6d1	zemědělská
prvovýrobě	prvovýroba	k1gFnSc6	prvovýroba
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
<g/>
-li	i	k?	-li
dále	daleko	k6eAd2	daleko
upravovány	upravovat	k5eAaImNgFnP	upravovat
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
produkují	produkovat	k5eAaImIp3nP	produkovat
z	z	k7c2	z
biomasy	biomasa	k1gFnSc2	biomasa
výrobky	výrobek	k1gInPc1	výrobek
získávané	získávaný	k2eAgInPc1d1	získávaný
obvykle	obvykle	k6eAd1	obvykle
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
biorafinérie	biorafinérie	k1gFnPc1	biorafinérie
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
ve	v	k7c6	v
studii	studie	k1gFnSc6	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
stanovila	stanovit	k5eAaPmAgFnS	stanovit
tzv.	tzv.	kA	tzv.
evropský	evropský	k2eAgInSc4d1	evropský
potenciál	potenciál	k1gInSc4	potenciál
biomasy	biomasa	k1gFnSc2	biomasa
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
by	by	kYmCp3nS	by
respektoval	respektovat	k5eAaImAgMnS	respektovat
ochranu	ochrana	k1gFnSc4	ochrana
biologické	biologický	k2eAgFnSc2d1	biologická
rozmanitosti	rozmanitost	k1gFnSc2	rozmanitost
a	a	k8xC	a
vedl	vést	k5eAaImAgInS	vést
jen	jen	k9	jen
k	k	k7c3	k
minimu	minimum	k1gNnSc3	minimum
nepříznivých	příznivý	k2eNgInPc2d1	nepříznivý
dopadů	dopad	k1gInPc2	dopad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2030	[number]	k4	2030
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
asi	asi	k9	asi
15	[number]	k4	15
%	%	kIx~	%
energetické	energetický	k2eAgFnSc2d1	energetická
poptávky	poptávka	k1gFnSc2	poptávka
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
energií	energie	k1gFnSc7	energie
vyrobenou	vyrobený	k2eAgFnSc7d1	vyrobená
ze	z	k7c2	z
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
<g/>
,	,	kIx,	,
lesnických	lesnický	k2eAgInPc2d1	lesnický
a	a	k8xC	a
odpadních	odpadní	k2eAgInPc2d1	odpadní
produktů	produkt	k1gInPc2	produkt
z	z	k7c2	z
čistě	čistě	k6eAd1	čistě
evropských	evropský	k2eAgInPc2d1	evropský
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2030	[number]	k4	2030
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
asi	asi	k9	asi
18	[number]	k4	18
%	%	kIx~	%
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
12,5	[number]	k4	12,5
%	%	kIx~	%
elektřiny	elektřina	k1gFnSc2	elektřina
a	a	k8xC	a
5,4	[number]	k4	5,4
%	%	kIx~	%
paliva	palivo	k1gNnPc4	palivo
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
biomasy	biomasa	k1gFnSc2	biomasa
evropského	evropský	k2eAgInSc2d1	evropský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Biomasa	biomasa	k1gFnSc1	biomasa
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
ale	ale	k8xC	ale
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
dováží	dovážet	k5eAaImIp3nP	dovážet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
místní	místní	k2eAgFnSc1d1	místní
produkce	produkce	k1gFnSc1	produkce
nestačí	stačit	k5eNaBmIp3nS	stačit
pokrýt	pokrýt	k5eAaPmF	pokrýt
cíle	cíl	k1gInPc4	cíl
využití	využití	k1gNnSc3	využití
paliv	palivo	k1gNnPc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
biopaliva	biopalivo	k1gNnSc2	biopalivo
<g/>
.	.	kIx.	.
</s>
<s>
Biopalivo	Biopalivo	k1gNnSc1	Biopalivo
vzniká	vznikat	k5eAaImIp3nS	vznikat
cílenou	cílený	k2eAgFnSc7d1	cílená
výrobou	výroba	k1gFnSc7	výroba
či	či	k8xC	či
přípravou	příprava	k1gFnSc7	příprava
z	z	k7c2	z
biomasy	biomasa	k1gFnSc2	biomasa
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
tedy	tedy	k9	tedy
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
možných	možný	k2eAgNnPc2d1	možné
využití	využití	k1gNnPc2	využití
biomasy	biomasa	k1gFnSc2	biomasa
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
lze	lze	k6eAd1	lze
jinak	jinak	k6eAd1	jinak
použít	použít	k5eAaPmF	použít
jako	jako	k9	jako
surovinu	surovina	k1gFnSc4	surovina
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
<g/>
,	,	kIx,	,
nábytek	nábytek	k1gInSc1	nábytek
<g/>
,	,	kIx,	,
balení	balení	k1gNnSc1	balení
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
výrobky	výrobek	k1gInPc4	výrobek
z	z	k7c2	z
papíru	papír	k1gInSc2	papír
atd.	atd.	kA	atd.
Možné	možný	k2eAgNnSc1d1	možné
rozdělení	rozdělení	k1gNnSc1	rozdělení
biopaliv	biopalit	k5eAaPmDgInS	biopalit
<g/>
:	:	kIx,	:
tuhá	tuhý	k2eAgNnPc4d1	tuhé
biopaliva	biopalivo	k1gNnPc4	biopalivo
kapalná	kapalný	k2eAgNnPc4d1	kapalné
biopaliva	biopalivo	k1gNnPc4	biopalivo
plynná	plynný	k2eAgNnPc4d1	plynné
biopaliva	biopalivo	k1gNnPc4	biopalivo
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
chemická	chemický	k2eAgFnSc1d1	chemická
energie	energie	k1gFnSc1	energie
z	z	k7c2	z
biopaliv	biopalit	k5eAaPmDgInS	biopalit
uvolňována	uvolňován	k2eAgFnSc1d1	uvolňována
hlavně	hlavně	k9	hlavně
jejich	jejich	k3xOp3gNnSc7	jejich
spalováním	spalování	k1gNnSc7	spalování
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vyvíjeny	vyvíjen	k2eAgFnPc1d1	vyvíjena
jiné	jiný	k2eAgFnPc1d1	jiná
účinnější	účinný	k2eAgFnPc1d2	účinnější
metody	metoda	k1gFnPc1	metoda
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
využití	využití	k1gNnSc4	využití
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
pomocí	pomocí	k7c2	pomocí
palivových	palivový	k2eAgInPc2d1	palivový
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Biopaliva	Biopalivo	k1gNnPc1	Biopalivo
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
15	[number]	k4	15
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
světové	světový	k2eAgFnSc2d1	světová
spotřeby	spotřeba	k1gFnSc2	spotřeba
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
především	především	k9	především
ve	v	k7c6	v
Třetím	třetí	k4xOgInSc6	třetí
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
slouží	sloužit	k5eAaImIp3nS	sloužit
převážně	převážně	k6eAd1	převážně
k	k	k7c3	k
vaření	vaření	k1gNnSc3	vaření
a	a	k8xC	a
vytápění	vytápění	k1gNnSc3	vytápění
domácností	domácnost	k1gFnPc2	domácnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
relativně	relativně	k6eAd1	relativně
vysoký	vysoký	k2eAgInSc4d1	vysoký
podíl	podíl	k1gInSc4	podíl
mají	mít	k5eAaImIp3nP	mít
biopaliva	biopaliva	k1gFnSc1	biopaliva
i	i	k8xC	i
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
Finsku	Finsko	k1gNnSc6	Finsko
(	(	kIx(	(
<g/>
17	[number]	k4	17
%	%	kIx~	%
a	a	k8xC	a
19	[number]	k4	19
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zda	zda	k8xS	zda
a	a	k8xC	a
případně	případně	k6eAd1	případně
o	o	k7c4	o
kolik	kolik	k4yQc4	kolik
biopaliva	biopalivo	k1gNnSc2	biopalivo
snižují	snižovat	k5eAaImIp3nP	snižovat
produkci	produkce	k1gFnSc4	produkce
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
předmětem	předmět	k1gInSc7	předmět
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Biopaliva	Biopalivo	k1gNnPc4	Biopalivo
uhlíkově	uhlíkově	k6eAd1	uhlíkově
neutrální	neutrální	k2eAgFnPc1d1	neutrální
nejsou	být	k5eNaImIp3nP	být
-	-	kIx~	-
už	už	k9	už
jenom	jenom	k9	jenom
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
účinnému	účinný	k2eAgInSc3d1	účinný
růstu	růst	k1gInSc2	růst
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
hnojivo	hnojivo	k1gNnSc1	hnojivo
<g/>
,	,	kIx,	,
rostliny	rostlina	k1gFnPc1	rostlina
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
nějak	nějak	k6eAd1	nějak
sklidit	sklidit	k5eAaPmF	sklidit
<g/>
,	,	kIx,	,
přetransformovat	přetransformovat	k5eAaPmF	přetransformovat
na	na	k7c4	na
biopaliva	biopaliv	k1gMnSc4	biopaliv
a	a	k8xC	a
přemístit	přemístit	k5eAaPmF	přemístit
do	do	k7c2	do
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
biopaliva	biopalivo	k1gNnSc2	biopalivo
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
vodní	vodní	k2eAgFnPc1d1	vodní
řasy	řasa	k1gFnPc1	řasa
<g/>
.	.	kIx.	.
</s>
