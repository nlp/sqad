<s>
Hans	Hans	k1gMnSc1	Hans
Christian	Christian	k1gMnSc1	Christian
Andersen	Andersen	k1gMnSc1	Andersen
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
<g/>
:	:	kIx,	:
Háns	Háns	k1gInSc1	Háns
Kristián	Kristián	k1gMnSc1	Kristián
Andrsen	Andrsen	k1gInSc1	Andrsen
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1805	[number]	k4	1805
Odense	Odense	k1gFnSc2	Odense
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
dánský	dánský	k2eAgMnSc1d1	dánský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
proslul	proslout	k5eAaPmAgMnS	proslout
především	především	k6eAd1	především
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
světových	světový	k2eAgMnPc2d1	světový
pohádkářů	pohádkář	k1gMnPc2	pohádkář
<g/>
.	.	kIx.	.
</s>
