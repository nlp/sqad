<s>
Hans	Hans	k1gMnSc1	Hans
Christian	Christian	k1gMnSc1	Christian
Andersen	Andersen	k1gMnSc1	Andersen
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
<g/>
:	:	kIx,	:
Háns	Háns	k1gInSc1	Háns
Kristián	Kristián	k1gMnSc1	Kristián
Andrsen	Andrsen	k1gInSc1	Andrsen
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1805	[number]	k4	1805
Odense	Odense	k1gFnSc2	Odense
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
dánský	dánský	k2eAgMnSc1d1	dánský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
proslul	proslout	k5eAaPmAgMnS	proslout
především	především	k6eAd1	především
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
světových	světový	k2eAgMnPc2d1	světový
pohádkářů	pohádkář	k1gMnPc2	pohádkář
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
156	[number]	k4	156
pohádek	pohádka	k1gFnPc2	pohádka
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
již	již	k6eAd1	již
klasikou	klasika	k1gFnSc7	klasika
(	(	kIx(	(
<g/>
Princezna	princezna	k1gFnSc1	princezna
na	na	k7c6	na
hrášku	hrášek	k1gInSc6	hrášek
<g/>
,	,	kIx,	,
Ošklivé	ošklivý	k2eAgNnSc1d1	ošklivé
káčátko	káčátko	k1gNnSc1	káčátko
<g/>
,	,	kIx,	,
Císařův	Císařův	k2eAgInSc1d1	Císařův
slavík	slavík	k1gInSc1	slavík
<g/>
,	,	kIx,	,
Císařovy	Císařův	k2eAgInPc1d1	Císařův
nové	nový	k2eAgInPc1d1	nový
šaty	šat	k1gInPc1	šat
<g/>
,	,	kIx,	,
Statečný	statečný	k2eAgMnSc1d1	statečný
cínový	cínový	k2eAgMnSc1d1	cínový
vojáček	vojáček	k1gMnSc1	vojáček
<g/>
,	,	kIx,	,
Křesadlo	křesadlo	k1gNnSc1	křesadlo
a	a	k8xC	a
Malá	malý	k2eAgFnSc1d1	malá
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
ovšem	ovšem	k9	ovšem
i	i	k9	i
verše	verš	k1gInPc1	verš
a	a	k8xC	a
romány	román	k1gInPc1	román
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
čtena	čten	k2eAgFnSc1d1	čtena
autobiografie	autobiografie	k1gFnSc1	autobiografie
Pohádka	pohádka	k1gFnSc1	pohádka
mého	můj	k3xOp1gInSc2	můj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
světoznámých	světoznámý	k2eAgFnPc2d1	světoznámá
pohádek	pohádka	k1gFnPc2	pohádka
napsal	napsat	k5eAaPmAgMnS	napsat
také	také	k9	také
několik	několik	k4yIc4	několik
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k4c1	málo
úspěšných	úspěšný	k2eAgFnPc2d1	úspěšná
básnických	básnický	k2eAgFnPc2d1	básnická
sbírek	sbírka	k1gFnPc2	sbírka
a	a	k8xC	a
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Odense	Odensa	k1gFnSc6	Odensa
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Fynu	Fynus	k1gInSc2	Fynus
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1805	[number]	k4	1805
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
synem	syn	k1gMnSc7	syn
chorého	chorý	k2eAgNnSc2d1	choré
22	[number]	k4	22
<g/>
letého	letý	k2eAgMnSc4d1	letý
ševce	švec	k1gMnSc4	švec
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
starší	starý	k2eAgFnSc2d2	starší
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
chudých	chudý	k2eAgInPc2d1	chudý
poměrů	poměr	k1gInPc2	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
žila	žít	k5eAaImAgFnS	žít
a	a	k8xC	a
spala	spát	k5eAaImAgFnS	spát
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
malé	malý	k2eAgFnSc6d1	malá
místnosti	místnost	k1gFnSc6	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
pradlena	pradlena	k1gFnSc1	pradlena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ke	k	k7c3	k
sklonku	sklonek	k1gInSc3	sklonek
života	život	k1gInSc2	život
propadla	propadnout	k5eAaPmAgFnS	propadnout
alkoholu	alkohol	k1gInSc3	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
nevlastní	vlastní	k2eNgFnSc1d1	nevlastní
sestra	sestra	k1gFnSc1	sestra
byla	být	k5eAaImAgFnS	být
prostitutka	prostitutka	k1gFnSc1	prostitutka
<g/>
,	,	kIx,	,
dědeček	dědeček	k1gMnSc1	dědeček
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
pomatenec	pomatenec	k1gMnSc1	pomatenec
a	a	k8xC	a
známá	známý	k2eAgFnSc1d1	známá
odenseská	odenseský	k2eAgFnSc1d1	odenseský
figurka	figurka	k1gFnSc1	figurka
<g/>
,	,	kIx,	,
kmotr	kmotr	k1gMnSc1	kmotr
byl	být	k5eAaImAgMnS	být
vrátný	vrátný	k1gMnSc1	vrátný
v	v	k7c6	v
městském	městský	k2eAgInSc6d1	městský
chudobinci	chudobinec	k1gInSc6	chudobinec
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
přecitlivělý	přecitlivělý	k2eAgInSc1d1	přecitlivělý
<g/>
,	,	kIx,	,
ctižádostivý	ctižádostivý	k2eAgInSc1d1	ctižádostivý
a	a	k8xC	a
marnivý	marnivý	k2eAgInSc1d1	marnivý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
původ	původ	k1gInSc1	původ
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
znamenal	znamenat	k5eAaImAgInS	znamenat
neustálé	neustálý	k2eAgNnSc4d1	neustálé
traumatizující	traumatizující	k2eAgNnSc4d1	traumatizující
ohrožení	ohrožení	k1gNnSc4	ohrožení
–	–	k?	–
ten	ten	k3xDgMnSc1	ten
zároveň	zároveň	k6eAd1	zároveň
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
jeho	jeho	k3xOp3gNnSc3	jeho
až	až	k9	až
výstřední	výstřední	k2eAgFnSc4d1	výstřední
potřebu	potřeba	k1gFnSc4	potřeba
společenského	společenský	k2eAgNnSc2d1	společenské
a	a	k8xC	a
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
uznání	uznání	k1gNnSc2	uznání
<g/>
,	,	kIx,	,
přecitlivělost	přecitlivělost	k1gFnSc1	přecitlivělost
na	na	k7c4	na
kritiku	kritika	k1gFnSc4	kritika
i	i	k8xC	i
nezdolnou	zdolný	k2eNgFnSc4d1	nezdolná
vytrvalost	vytrvalost	k1gFnSc4	vytrvalost
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
za	za	k7c7	za
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
brzo	brzo	k6eAd1	brzo
prokazoval	prokazovat	k5eAaImAgMnS	prokazovat
představivost	představivost	k1gFnSc4	představivost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
shovívavostí	shovívavost	k1gFnSc7	shovívavost
jeho	jeho	k3xOp3gMnPc2	jeho
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
pověrčivostí	pověrčivost	k1gFnPc2	pověrčivost
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1816	[number]	k4	1816
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
přestat	přestat	k5eAaPmF	přestat
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
začít	začít	k5eAaPmF	začít
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
továrně	továrna	k1gFnSc6	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Postavil	postavit	k5eAaPmAgInS	postavit
si	se	k3xPyFc3	se
malé	malý	k2eAgNnSc4d1	malé
loutkové	loutkový	k2eAgNnSc4d1	loutkové
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
pro	pro	k7c4	pro
své	své	k1gNnSc4	své
loutky	loutka	k1gFnSc2	loutka
oblečky	obleček	k1gInPc4	obleček
<g/>
.	.	kIx.	.
</s>
<s>
Přečetl	přečíst	k5eAaPmAgMnS	přečíst
všechny	všechen	k3xTgFnPc4	všechen
možné	možný	k2eAgFnPc4d1	možná
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgInS	moct
vypůjčit	vypůjčit	k5eAaPmF	vypůjčit
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
také	také	k9	také
díla	dílo	k1gNnSc2	dílo
Ludviga	Ludvig	k1gMnSc2	Ludvig
Holberga	Holberg	k1gMnSc2	Holberg
a	a	k8xC	a
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
.	.	kIx.	.
</s>
<s>
Andersen	Andersen	k1gMnSc1	Andersen
celé	celá	k1gFnSc2	celá
své	svůj	k3xOyFgNnSc4	svůj
dětství	dětství	k1gNnSc4	dětství
vášnivě	vášnivě	k6eAd1	vášnivě
miloval	milovat	k5eAaImAgMnS	milovat
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
známý	známý	k1gMnSc1	známý
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
znal	znát	k5eAaImAgInS	znát
nazpaměť	nazpaměť	k6eAd1	nazpaměť
celé	celý	k2eAgFnPc4d1	celá
Shakespearovy	Shakespearův	k2eAgFnPc4d1	Shakespearova
hry	hra	k1gFnPc4	hra
a	a	k8xC	a
přednášel	přednášet	k5eAaImAgMnS	přednášet
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
používal	používat	k5eAaImAgInS	používat
své	svůj	k3xOyFgFnPc4	svůj
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
panenky	panenka	k1gFnPc4	panenka
jako	jako	k8xC	jako
postavy	postava	k1gFnPc4	postava
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
brzy	brzy	k6eAd1	brzy
utekl	utéct	k5eAaPmAgMnS	utéct
do	do	k7c2	do
Kodaně	Kodaň	k1gFnSc2	Kodaň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
uchytit	uchytit	k5eAaPmF	uchytit
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k9	jako
loutkoherec	loutkoherec	k1gMnSc1	loutkoherec
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
ředitelem	ředitel	k1gMnSc7	ředitel
divadla	divadlo	k1gNnSc2	divadlo
Jonasem	Jonas	k1gMnSc7	Jonas
Collinem	Collin	k1gInSc7	Collin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jej	on	k3xPp3gMnSc4	on
poslal	poslat	k5eAaPmAgInS	poslat
studovat	studovat	k5eAaImF	studovat
a	a	k8xC	a
zajistil	zajistit	k5eAaPmAgMnS	zajistit
mu	on	k3xPp3gMnSc3	on
stipendium	stipendium	k1gNnSc4	stipendium
krále	král	k1gMnSc4	král
Frederika	Frederik	k1gMnSc2	Frederik
VI	VI	kA	VI
<g/>
.	.	kIx.	.
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
ve	v	k7c6	v
Slagelse	Slagelsa	k1gFnSc6	Slagelsa
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Helsingø	Helsingø	k1gFnSc6	Helsingø
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgInS	zůstat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1827	[number]	k4	1827
<g/>
.	.	kIx.	.
</s>
<s>
Andersenovi	Andersenův	k2eAgMnPc1d1	Andersenův
studium	studium	k1gNnSc1	studium
příliš	příliš	k6eAd1	příliš
nešlo	jít	k5eNaImAgNnS	jít
a	a	k8xC	a
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
získal	získat	k5eAaPmAgMnS	získat
pouze	pouze	k6eAd1	pouze
základy	základ	k1gInPc4	základ
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nerozšířil	rozšířit	k5eNaPmAgMnS	rozšířit
<g/>
,	,	kIx,	,
dobu	doba	k1gFnSc4	doba
strávenou	strávený	k2eAgFnSc4d1	strávená
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
později	pozdě	k6eAd2	pozdě
nazýval	nazývat	k5eAaImAgInS	nazývat
nejčernějšími	černý	k2eAgInPc7d3	nejčernější
a	a	k8xC	a
nejtrpčími	trpký	k2eAgInPc7d3	trpký
roky	rok	k1gInPc7	rok
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
na	na	k7c4	na
školu	škola	k1gFnSc4	škola
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
<g/>
,	,	kIx,	,
Andersen	Andersen	k1gMnSc1	Andersen
vydal	vydat	k5eAaPmAgMnS	vydat
první	první	k4xOgFnSc4	první
knížku	knížka	k1gFnSc4	knížka
Duch	duch	k1gMnSc1	duch
v	v	k7c6	v
Palnatokově	Palnatokův	k2eAgInSc6d1	Palnatokův
hrobě	hrob	k1gInSc6	hrob
(	(	kIx(	(
<g/>
1822	[number]	k4	1822
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
po	po	k7c6	po
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
nezbavil	zbavit	k5eNaPmAgMnS	zbavit
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
svých	svůj	k3xOyFgMnPc6	svůj
mecenáších	mecenáš	k1gMnPc6	mecenáš
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
nebyla	být	k5eNaImAgFnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgInS	moct
finančně	finančně	k6eAd1	finančně
vystačit	vystačit	k5eAaBmF	vystačit
pouze	pouze	k6eAd1	pouze
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
literární	literární	k2eAgFnSc7d1	literární
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
až	až	k9	až
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1845	[number]	k4	1845
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
začalo	začít	k5eAaPmAgNnS	začít
dařit	dařit	k5eAaImF	dařit
lépe	dobře	k6eAd2	dobře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
podnikl	podniknout	k5eAaPmAgMnS	podniknout
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
mj.	mj.	kA	mj.
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
a	a	k8xC	a
Neapol	Neapol	k1gFnSc1	Neapol
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
literárních	literární	k2eAgMnPc2d1	literární
historiků	historik	k1gMnPc2	historik
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
utrpení	utrpení	k1gNnSc4	utrpení
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
odlišnosti	odlišnost	k1gFnSc2	odlišnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvíce	nejvíce	k6eAd1	nejvíce
vypovídajících	vypovídající	k2eAgInPc2d1	vypovídající
příběhů	příběh	k1gInPc2	příběh
Malá	malý	k2eAgFnSc1d1	malá
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
vezme	vzít	k5eAaPmIp3nS	vzít
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
milována	milovat	k5eAaImNgFnS	milovat
krásným	krásný	k2eAgMnSc7d1	krásný
princem	princ	k1gMnSc7	princ
<g/>
.	.	kIx.	.
</s>
<s>
Hans	Hans	k1gMnSc1	Hans
Christian	Christian	k1gMnSc1	Christian
Andersen	Andersen	k1gMnSc1	Andersen
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
vyrovnával	vyrovnávat	k5eAaImAgMnS	vyrovnávat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
homosexualitou	homosexualita	k1gFnSc7	homosexualita
a	a	k8xC	a
právě	právě	k6eAd1	právě
Malá	malý	k2eAgFnSc1d1	malá
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
zpodobnění	zpodobnění	k1gNnSc4	zpodobnění
jeho	jeho	k3xOp3gFnSc2	jeho
lásky	láska	k1gFnSc2	láska
k	k	k7c3	k
mladému	mladý	k2eAgMnSc3d1	mladý
Edvardu	Edvard	k1gMnSc3	Edvard
Collinovi	Collin	k1gMnSc3	Collin
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chřadnu	chřadnout	k5eAaImIp1nS	chřadnout
kvůli	kvůli	k7c3	kvůli
Tobě	ty	k3xPp2nSc3	ty
jako	jako	k9	jako
kvůli	kvůli	k7c3	kvůli
krásné	krásný	k2eAgFnSc3d1	krásná
Kalábrijské	kalábrijský	k2eAgFnSc3d1	kalábrijská
nevěstce	nevěstka	k1gFnSc3	nevěstka
<g/>
...	...	k?	...
mé	můj	k3xOp1gNnSc1	můj
city	city	k1gNnSc1	city
k	k	k7c3	k
Tobě	ty	k3xPp2nSc3	ty
jsou	být	k5eAaImIp3nP	být
ženské	ženská	k1gFnPc1	ženská
<g/>
.	.	kIx.	.
</s>
<s>
Ženskost	ženskost	k1gFnSc1	ženskost
mé	můj	k3xOp1gFnSc2	můj
povahy	povaha	k1gFnSc2	povaha
a	a	k8xC	a
naše	náš	k3xOp1gNnSc4	náš
přátelství	přátelství	k1gNnSc4	přátelství
musí	muset	k5eAaImIp3nS	muset
zůstat	zůstat	k5eAaPmF	zůstat
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Collin	Collin	k1gInSc4	Collin
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
muži	muž	k1gMnPc1	muž
eroticky	eroticky	k6eAd1	eroticky
nepřitahovali	přitahovat	k5eNaImAgMnP	přitahovat
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaBmAgMnS	napsat
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
pamětech	paměť	k1gFnPc6	paměť
"	"	kIx"	"
<g/>
Zjistil	zjistit	k5eAaPmAgMnS	zjistit
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsem	být	k5eNaImIp1nS	být
schopný	schopný	k2eAgMnSc1d1	schopný
opětovat	opětovat	k5eAaImF	opětovat
tuto	tento	k3xDgFnSc4	tento
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
To	ten	k3xDgNnSc1	ten
mu	on	k3xPp3gMnSc3	on
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
mnoho	mnoho	k4c4	mnoho
trápení	trápení	k1gNnPc2	trápení
<g/>
,	,	kIx,	,
na	na	k7c6	na
platonické	platonický	k2eAgFnSc6d1	platonická
úrovni	úroveň	k1gFnSc6	úroveň
zůstala	zůstat	k5eAaPmAgFnS	zůstat
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
dánskému	dánský	k2eAgMnSc3d1	dánský
tanečníkovi	tanečník	k1gMnSc3	tanečník
Harlodu	Harlod	k1gMnSc3	Harlod
Scharfovi	Scharf	k1gMnSc3	Scharf
a	a	k8xC	a
mladému	mladý	k1gMnSc3	mladý
knížeti	kníže	k1gMnSc3	kníže
Výmarskému	výmarský	k2eAgMnSc3d1	výmarský
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
že	že	k8xS	že
Andersena	Andersen	k1gMnSc4	Andersen
přitahovali	přitahovat	k5eAaImAgMnP	přitahovat
jak	jak	k6eAd1	jak
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
tak	tak	k9	tak
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c6	o
obojím	obojí	k4xRgInSc6	obojí
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známá	k1gFnSc1	známá
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc4	jeho
slabost	slabost	k1gFnSc4	slabost
pro	pro	k7c4	pro
operní	operní	k2eAgFnSc4d1	operní
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
Jenny	Jenna	k1gFnSc2	Jenna
Lindovou	Lindový	k2eAgFnSc4d1	Lindový
<g/>
,	,	kIx,	,
přezdívanou	přezdívaný	k2eAgFnSc4d1	přezdívaná
švédský	švédský	k2eAgInSc1d1	švédský
slavík	slavík	k1gInSc1	slavík
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ovšem	ovšem	k9	ovšem
Andersena	Andersen	k1gMnSc2	Andersen
brala	brát	k5eAaImAgFnS	brát
spíše	spíše	k9	spíše
jako	jako	k9	jako
bratra	bratr	k1gMnSc4	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Pohádka	pohádka	k1gFnSc1	pohádka
Slavík	slavík	k1gInSc1	slavík
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
právě	právě	k9	právě
touhou	touha	k1gFnSc7	touha
po	po	k7c6	po
Lindové	Lindová	k1gFnSc6	Lindová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
soukromých	soukromý	k2eAgInPc6d1	soukromý
zápiscích	zápisek	k1gInPc6	zápisek
odmítal	odmítat	k5eAaImAgMnS	odmítat
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
sexuální	sexuální	k2eAgInSc4d1	sexuální
vztah	vztah	k1gInSc4	vztah
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mužem	muž	k1gMnSc7	muž
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dnešních	dnešní	k2eAgNnPc2d1	dnešní
hledisek	hledisko	k1gNnPc2	hledisko
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
asexuála	asexuál	k1gMnSc4	asexuál
<g/>
.	.	kIx.	.
</s>
<s>
Otázkou	otázka	k1gFnSc7	otázka
ovšem	ovšem	k9	ovšem
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
do	do	k7c2	do
jaké	jaký	k3yRgFnSc2	jaký
míry	míra	k1gFnSc2	míra
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
jeho	jeho	k3xOp3gFnSc1	jeho
asexualita	asexualita	k1gFnSc1	asexualita
dobrovolná	dobrovolný	k2eAgFnSc1d1	dobrovolná
a	a	k8xC	a
zda	zda	k8xS	zda
mu	on	k3xPp3gMnSc3	on
nebyla	být	k5eNaImAgFnS	být
vnucena	vnucen	k2eAgFnSc1d1	vnucena
jen	jen	k9	jen
jeho	jeho	k3xOp3gFnSc7	jeho
přehnanou	přehnaný	k2eAgFnSc7d1	přehnaná
ostýchavostí	ostýchavost	k1gFnSc7	ostýchavost
a	a	k8xC	a
společenskými	společenský	k2eAgFnPc7d1	společenská
konvencemi	konvence	k1gFnPc7	konvence
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
se	se	k3xPyFc4	se
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
spisovatelem	spisovatel	k1gMnSc7	spisovatel
Karlem	Karel	k1gMnSc7	Karel
Mariou	Maria	k1gMnSc7	Maria
Kertbenym	Kertbenym	k1gInSc4	Kertbenym
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
o	o	k7c6	o
9	[number]	k4	9
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
použil	použít	k5eAaPmAgMnS	použít
slovo	slovo	k1gNnSc4	slovo
homosexuell	homosexuella	k1gFnPc2	homosexuella
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
setkání	setkání	k1gNnSc1	setkání
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
u	u	k7c2	u
Andersena	Andersen	k1gMnSc2	Andersen
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
beznaděj	beznaděj	k1gFnSc4	beznaděj
a	a	k8xC	a
zoufalství	zoufalství	k1gNnSc4	zoufalství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
při	při	k7c6	při
něm	on	k3xPp3gMnSc6	on
událo	udát	k5eAaPmAgNnS	udát
<g/>
.	.	kIx.	.
</s>
<s>
Henning	Henning	k1gInSc1	Henning
Bech	Bech	k1gInSc1	Bech
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
mohla	moct	k5eAaImAgFnS	moct
hrát	hrát	k5eAaImF	hrát
roli	role	k1gFnSc3	role
Andersenova	Andersenův	k2eAgFnSc1d1	Andersenova
sexuální	sexuální	k2eAgFnSc1d1	sexuální
orientace	orientace	k1gFnSc1	orientace
a	a	k8xC	a
strach	strach	k1gInSc1	strach
z	z	k7c2	z
jejího	její	k3xOp3gNnSc2	její
pojmenování	pojmenování	k1gNnSc2	pojmenování
<g/>
.	.	kIx.	.
</s>
<s>
Sexuální	sexuální	k2eAgMnSc1d1	sexuální
psycholog	psycholog	k1gMnSc1	psycholog
Petr	Petr	k1gMnSc1	Petr
Weiss	Weiss	k1gMnSc1	Weiss
jej	on	k3xPp3gMnSc4	on
občas	občas	k6eAd1	občas
uvádí	uvádět	k5eAaImIp3nS	uvádět
mezi	mezi	k7c7	mezi
typickými	typický	k2eAgInPc7d1	typický
příklady	příklad	k1gInPc7	příklad
společensky	společensky	k6eAd1	společensky
úspěšných	úspěšný	k2eAgMnPc2d1	úspěšný
pedofilů	pedofil	k1gMnPc2	pedofil
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
také	také	k6eAd1	také
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
mezi	mezi	k7c7	mezi
nejvýznamnějšími	významný	k2eAgInPc7d3	nejvýznamnější
autisty	autist	k1gInPc7	autist
<g/>
,	,	kIx,	,
příznaky	příznak	k1gInPc1	příznak
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
např.	např.	kA	např.
podivné	podivný	k2eAgInPc4d1	podivný
záchvaty	záchvat	k1gInPc4	záchvat
vzteku	vztek	k1gInSc2	vztek
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
neschopnost	neschopnost	k1gFnSc1	neschopnost
navázat	navázat	k5eAaPmF	navázat
blízký	blízký	k2eAgInSc4d1	blízký
citový	citový	k2eAgInSc4d1	citový
vztah	vztah	k1gInSc4	vztah
a	a	k8xC	a
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
nedosažitelných	dosažitelný	k2eNgMnPc6d1	nedosažitelný
mužích	muž	k1gMnPc6	muž
a	a	k8xC	a
ženách	žena	k1gFnPc6	žena
či	či	k8xC	či
hluboký	hluboký	k2eAgInSc4d1	hluboký
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
zmatek	zmatek	k1gInSc4	zmatek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
příbězích	příběh	k1gInPc6	příběh
<g/>
,	,	kIx,	,
a	a	k8xC	a
podivné	podivný	k2eAgFnSc2d1	podivná
a	a	k8xC	a
neuchopitelné	uchopitelný	k2eNgFnSc2d1	neuchopitelná
nešťastné	šťastný	k2eNgFnSc2d1	nešťastná
postavy	postava	k1gFnSc2	postava
v	v	k7c6	v
jeho	jeho	k3xOp3gNnPc6	jeho
dílech	dílo	k1gNnPc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
spadl	spadnout	k5eAaPmAgInS	spadnout
z	z	k7c2	z
postele	postel	k1gFnSc2	postel
a	a	k8xC	a
vážně	vážně	k6eAd1	vážně
se	se	k3xPyFc4	se
zranil	zranit	k5eAaPmAgMnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
neuzdravil	uzdravit	k5eNaPmAgMnS	uzdravit
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1875	[number]	k4	1875
v	v	k7c6	v
domě	dům	k1gInSc6	dům
zvaném	zvaný	k2eAgInSc6d1	zvaný
Rolighed	Rolighed	k1gMnSc1	Rolighed
nedaleko	nedaleko	k7c2	nedaleko
Kodaně	Kodaň	k1gFnSc2	Kodaň
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
na	na	k7c6	na
Assistenském	Assistenský	k2eAgInSc6d1	Assistenský
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pohřeb	pohřeb	k1gInSc1	pohřeb
<g/>
,	,	kIx,	,
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
i	i	k9	i
král	král	k1gMnSc1	král
Kristián	Kristián	k1gMnSc1	Kristián
IX	IX	kA	IX
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
dnem	den	k1gInSc7	den
národního	národní	k2eAgInSc2d1	národní
smutku	smutek	k1gInSc2	smutek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
psal	psát	k5eAaImAgMnS	psát
divadelní	divadelní	k2eAgFnPc4d1	divadelní
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
však	však	k9	však
nebyly	být	k5eNaImAgFnP	být
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
nebyly	být	k5eNaImAgFnP	být
ani	ani	k8xC	ani
vydány	vydán	k2eAgFnPc1d1	vydána
<g/>
:	:	kIx,	:
konec	konec	k1gInSc1	konec
Mikulášské	mikulášský	k2eAgFnSc6d1	Mikulášská
věži	věž	k1gFnSc6	věž
<g/>
,	,	kIx,	,
1829	[number]	k4	1829
Anežka	Anežka	k1gFnSc1	Anežka
a	a	k8xC	a
vodník	vodník	k1gMnSc1	vodník
<g/>
,	,	kIx,	,
1833	[number]	k4	1833
Ahasver	Ahasver	k1gMnSc1	Ahasver
<g/>
,	,	kIx,	,
1837	[number]	k4	1837
Jeho	jeho	k3xOp3gFnSc4	jeho
básnické	básnický	k2eAgFnPc1d1	básnická
sbírky	sbírka	k1gFnPc1	sbírka
se	se	k3xPyFc4	se
nesetkaly	setkat	k5eNaPmAgFnP	setkat
prakticky	prakticky	k6eAd1	prakticky
s	s	k7c7	s
žádným	žádný	k3yNgInSc7	žádný
ohlasem	ohlas	k1gInSc7	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Básně	báseň	k1gFnPc4	báseň
Básníkův	básníkův	k2eAgInSc4d1	básníkův
bazar	bazar	k1gInSc4	bazar
Být	být	k5eAaImF	být
či	či	k8xC	či
nebýt	být	k5eNaImF	být
Jeho	jeho	k3xOp3gFnPc4	jeho
pohádky	pohádka	k1gFnPc4	pohádka
vycházely	vycházet	k5eAaImAgFnP	vycházet
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
sbírkách	sbírka	k1gFnPc6	sbírka
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
složité	složitý	k2eAgNnSc1d1	složité
uvádět	uvádět	k5eAaImF	uvádět
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
jeho	jeho	k3xOp3gFnPc1	jeho
nejznámější	známý	k2eAgFnPc1d3	nejznámější
pohádky	pohádka	k1gFnPc1	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
Andersen	Andersen	k1gMnSc1	Andersen
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
pohádek	pohádka	k1gFnPc2	pohádka
vycházel	vycházet	k5eAaImAgInS	vycházet
ze	z	k7c2	z
světové	světový	k2eAgFnSc2d1	světová
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
vlastních	vlastní	k2eAgFnPc2d1	vlastní
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Pohádky	pohádka	k1gFnPc4	pohádka
nepovažoval	považovat	k5eNaImAgMnS	považovat
za	za	k7c4	za
plnohodnotnou	plnohodnotný	k2eAgFnSc4d1	plnohodnotná
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
,	,	kIx,	,
nevážil	vážit	k5eNaImAgMnS	vážit
si	se	k3xPyFc3	se
jich	on	k3xPp3gMnPc2	on
a	a	k8xC	a
nepovažoval	považovat	k5eNaImAgInS	považovat
se	se	k3xPyFc4	se
za	za	k7c2	za
pohádkáře	pohádkář	k1gMnSc2	pohádkář
–	–	k?	–
cítil	cítit	k5eAaImAgMnS	cítit
se	se	k3xPyFc4	se
být	být	k5eAaImF	být
zneuznaným	zneuznaný	k2eAgMnSc7d1	zneuznaný
básníkem	básník	k1gMnSc7	básník
a	a	k8xC	a
dramatikem	dramatik	k1gMnSc7	dramatik
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
jeho	jeho	k3xOp3gFnPc2	jeho
pohádek	pohádka	k1gFnPc2	pohádka
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
byly	být	k5eAaImAgInP	být
přijaty	přijmout	k5eAaPmNgInP	přijmout
i	i	k9	i
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
pohádkách	pohádka	k1gFnPc6	pohádka
je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpravidla	zpravidla	k6eAd1	zpravidla
nekončí	končit	k5eNaImIp3nS	končit
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
příběhy	příběh	k1gInPc4	příběh
s	s	k7c7	s
pohádkovými	pohádkový	k2eAgInPc7d1	pohádkový
motivy	motiv	k1gInPc7	motiv
<g/>
,	,	kIx,	,
určené	určený	k2eAgInPc4d1	určený
starším	starý	k2eAgFnPc3d2	starší
dětem	dítě	k1gFnPc3	dítě
a	a	k8xC	a
dospělým	dospělí	k1gMnPc3	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Cínový	cínový	k2eAgMnSc1d1	cínový
vojáček	vojáček	k1gMnSc1	vojáček
Císařovy	Císařův	k2eAgFnSc2d1	Císařova
nové	nový	k2eAgFnSc2d1	nová
šaty	šata	k1gFnSc2	šata
Čarovná	čarovný	k2eAgFnSc1d1	čarovná
truhlice	truhlice	k1gFnSc1	truhlice
Křesadlo	křesadlo	k1gNnSc1	křesadlo
Křišťálové	křišťálový	k2eAgFnSc2d1	Křišťálová
vidiny	vidina	k1gFnSc2	vidina
Kubík	Kubík	k1gMnSc1	Kubík
a	a	k8xC	a
Kuba	Kuba	k1gFnSc1	Kuba
Malá	malý	k2eAgFnSc1d1	malá
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
Děvčátko	děvčátko	k1gNnSc1	děvčátko
se	s	k7c7	s
sirkami	sirka	k1gFnPc7	sirka
O	o	k7c6	o
princezně	princezna	k1gFnSc6	princezna
na	na	k7c6	na
hrášku	hrášek	k1gInSc6	hrášek
Ošklivé	ošklivý	k2eAgNnSc1d1	ošklivé
káčátko	káčátko	k1gNnSc1	káčátko
Pasáček	pasáček	k1gMnSc1	pasáček
vepřů	vepř	k1gMnPc2	vepř
Sněhová	sněhový	k2eAgFnSc1d1	sněhová
královna	královna	k1gFnSc1	královna
Sněženka	sněženka	k1gFnSc1	sněženka
Stín	stín	k1gInSc4	stín
Velký	velký	k2eAgMnSc1d1	velký
mořský	mořský	k2eAgMnSc1d1	mořský
had	had	k1gMnSc1	had
O	o	k7c6	o
Malence	Malenka	k1gFnSc6	Malenka
Stará	starý	k2eAgFnSc1d1	stará
svítilna	svítilna	k1gFnSc1	svítilna
Létající	létající	k2eAgFnSc1d1	létající
jehla	jehla	k1gFnSc1	jehla
Kapka	kapka	k1gFnSc1	kapka
vody	voda	k1gFnSc2	voda
Čarovná	čarovný	k2eAgFnSc1d1	čarovná
truhlice	truhlice	k1gFnSc1	truhlice
Kníže	kníže	k1gNnSc1wR	kníže
zla	zlo	k1gNnSc2	zlo
O	o	k7c6	o
dívce	dívka	k1gFnSc6	dívka
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
která	který	k3yRgFnSc1	který
šlápla	šlápnout	k5eAaPmAgFnS	šlápnout
na	na	k7c4	na
chléb	chléb	k1gInSc4	chléb
Nezpůsobný	způsobný	k2eNgMnSc1d1	nezpůsobný
chlapec	chlapec	k1gMnSc1	chlapec
Střízlička	Střízlička	k1gFnSc1	Střízlička
Sedmikráska	sedmikráska	k1gFnSc1	sedmikráska
Divoké	divoký	k2eAgFnSc2d1	divoká
labutě	labuť	k1gFnSc2	labuť
Létající	létající	k2eAgInSc1d1	létající
kufr	kufr	k1gInSc1	kufr
Růže	růž	k1gFnSc2	růž
z	z	k7c2	z
hrobu	hrob	k1gInSc2	hrob
Homérova	Homérův	k2eAgInSc2d1	Homérův
Čápi	čáp	k1gMnPc1	čáp
Uspávač	uspávač	k1gMnSc1	uspávač
Růžový	růžový	k2eAgMnSc1d1	růžový
skřítek	skřítek	k1gMnSc1	skřítek
Slavík	Slavík	k1gMnSc1	Slavík
Snoubenci	snoubenec	k1gMnSc3	snoubenec
Červené	Červené	k2eAgFnSc2d1	Červené
střevíčky	střevíček	k1gInPc1	střevíček
Kdo	kdo	k3yQnSc1	kdo
skočí	skočit	k5eAaPmIp3nS	skočit
nejvýš	nejvýš	k6eAd1	nejvýš
Holger	Holger	k1gMnSc1	Holger
Dánský	dánský	k2eAgMnSc1d1	dánský
Z	z	k7c2	z
okna	okno	k1gNnSc2	okno
ve	v	k7c6	v
Vartou	varta	k1gFnSc7	varta
Len	len	k1gInSc4	len
Malý	malý	k2eAgInSc4d1	malý
ťuk	ťuk	k1gInSc4	ťuk
Starý	starý	k2eAgInSc1d1	starý
dům	dům	k1gInSc1	dům
Příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
celém	celý	k2eAgInSc6d1	celý
roku	rok	k1gInSc6	rok
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zcela	zcela	k6eAd1	zcela
jisté	jistý	k2eAgInPc1d1	jistý
Poklad	poklad	k1gInSc1	poklad
Přezůvky	přezůvka	k1gFnSc2	přezůvka
štěstěny	štěstěna	k1gFnSc2	štěstěna
Hloupý	hloupý	k2eAgInSc4d1	hloupý
<g />
.	.	kIx.	.
</s>
<s>
Honza	Honza	k1gMnSc1	Honza
Běžci	běžec	k1gMnSc3	běžec
Hrdlo	hrdnout	k5eAaImAgNnS	hrdnout
od	od	k7c2	od
láhve	láhev	k1gFnSc2	láhev
Špejlová	Špejlová	k1gFnSc1	Špejlová
polévka	polévka	k1gFnSc1	polévka
Poslední	poslední	k2eAgFnSc1d1	poslední
sen	sen	k1gInSc4	sen
starého	starý	k2eAgInSc2d1	starý
dubu	dub	k1gInSc2	dub
Pták	pták	k1gMnSc1	pták
fénix	fénix	k1gMnSc1	fénix
Vítr	vítr	k1gInSc4	vítr
vypravuje	vypravovat	k5eAaImIp3nS	vypravovat
o	o	k7c4	o
Valdemaru	Valdemara	k1gFnSc4	Valdemara
Daaeovi	Daaeus	k1gMnSc3	Daaeus
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc6	jeho
dcerách	dcera	k1gFnPc6	dcera
Dětské	dětský	k2eAgFnSc2d1	dětská
řeči	řeč	k1gFnSc2	řeč
Loutkář	loutkář	k1gMnSc1	loutkář
Dvanáct	dvanáct	k4xCc1	dvanáct
z	z	k7c2	z
dostavníku	dostavník	k1gInSc2	dostavník
Chrobák	chrobák	k1gMnSc1	chrobák
Co	co	k8xS	co
táta	táta	k1gMnSc1	táta
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
dobré	dobrý	k2eAgNnSc1d1	dobré
Sněhulák	sněhulák	k1gMnSc1	sněhulák
V	v	k7c6	v
kachním	kachní	k2eAgInSc6d1	kachní
dvorku	dvorek	k1gInSc6	dvorek
Zvon	zvon	k1gInSc1	zvon
Vítr	vítr	k1gInSc1	vítr
štíty	štít	k1gInPc1	štít
přenáší	přenášet	k5eAaImIp3nS	přenášet
Čajová	čajový	k2eAgFnSc1d1	čajová
konvice	konvice	k1gFnSc1	konvice
Sněženka	sněženka	k1gFnSc1	sněženka
Lístek	lístek	k1gInSc4	lístek
z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
<g />
.	.	kIx.	.
</s>
<s>
Pero	pero	k1gNnSc1	pero
a	a	k8xC	a
kalamář	kalamář	k1gInSc1	kalamář
Němá	němý	k2eAgFnSc1d1	němá
kniha	kniha	k1gFnSc1	kniha
Povídka	povídka	k1gFnSc1	povídka
z	z	k7c2	z
písečných	písečný	k2eAgFnPc2d1	písečná
dun	duna	k1gFnPc2	duna
Matka	matka	k1gFnSc1	matka
Pohádky	pohádka	k1gFnSc2	pohádka
a	a	k8xC	a
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
a	a	k8xC	a
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
2	[number]	k4	2
díly	díl	k1gInPc7	díl
Flétnové	flétnový	k2eAgFnSc2d1	flétnová
hodiny	hodina	k1gFnSc2	hodina
Čarovná	čarovný	k2eAgFnSc1d1	čarovná
truhlice	truhlice	k1gFnSc1	truhlice
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
pohádky	pohádka	k1gFnSc2	pohádka
Pohádky	pohádka	k1gFnSc2	pohádka
Vílí	vílí	k2eAgFnSc1d1	vílí
hora	hora	k1gFnSc1	hora
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
pohádky	pohádka	k1gFnPc1	pohádka
Pohádky	pohádka	k1gFnSc2	pohádka
<g/>
,	,	kIx,	,
BRIO	brio	k1gNnSc1	brio
2005	[number]	k4	2005
Křišťálové	křišťálový	k2eAgFnSc2d1	Křišťálová
vidiny	vidina	k1gFnSc2	vidina
<g/>
,	,	kIx,	,
F.	F.	kA	F.
<g/>
KOSEK-PRAHA	KOSEK-PRAHA	k1gFnSc1	KOSEK-PRAHA
1947	[number]	k4	1947
Deníky	deník	k1gInPc1	deník
1825	[number]	k4	1825
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
12	[number]	k4	12
dílů	díl	k1gInPc2	díl
Malá	malý	k2eAgFnSc1d1	malá
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
<g/>
,	,	kIx,	,
audiokniha	audiokniha	k1gFnSc1	audiokniha
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pro	pro	k7c4	pro
Noc	noc	k1gFnSc4	noc
s	s	k7c7	s
Andersenem	Andersen	k1gMnSc7	Andersen
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
načetl	načíst	k5eAaBmAgMnS	načíst
Ladislav	Ladislav	k1gMnSc1	Ladislav
Frej	frej	k1gInSc4	frej
<g/>
,	,	kIx,	,
vydala	vydat	k5eAaPmAgFnS	vydat
Audiotéka	Audiotéka	k1gFnSc1	Audiotéka
Císařovy	Císařův	k2eAgFnSc2d1	Císařova
nové	nový	k2eAgFnSc2d1	nová
šaty	šata	k1gFnSc2	šata
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
pohádky	pohádka	k1gFnSc2	pohádka
<g/>
,	,	kIx,	,
audiokniha	audiokniha	k1gFnSc1	audiokniha
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pro	pro	k7c4	pro
Noc	noc	k1gFnSc4	noc
s	s	k7c7	s
Andersenem	Andersen	k1gMnSc7	Andersen
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
načetli	načíst	k5eAaBmAgMnP	načíst
Martina	Martin	k1gMnSc4	Martin
Preissová	Preissová	k1gFnSc1	Preissová
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Preiss	Preissa	k1gFnPc2	Preissa
<g/>
,	,	kIx,	,
vydala	vydat	k5eAaPmAgFnS	vydat
Audiotéka	Audiotéka	k1gFnSc1	Audiotéka
</s>
