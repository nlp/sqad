<p>
<s>
Vychylovské	Vychylovský	k2eAgFnPc1d1	Vychylovský
skály	skála	k1gFnPc1	skála
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
Vychylovské	Vychylovský	k2eAgFnSc2d1	Vychylovský
skálie	skálie	k1gFnSc2	skálie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Kysuce	Kysuce	k1gFnSc2	Kysuce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
obce	obec	k1gFnSc2	obec
Nová	nový	k2eAgFnSc1d1	nová
Bystrica	Bystrica	k1gFnSc1	Bystrica
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Čadca	Čadca	k1gFnSc1	Čadca
v	v	k7c6	v
Žilinském	žilinský	k2eAgInSc6d1	žilinský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
či	či	k8xC	či
novelizováno	novelizovat	k5eAaBmNgNnS	novelizovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
na	na	k7c6	na
rozloze	rozloha	k1gFnSc6	rozloha
26,720	[number]	k4	26,720
<g/>
0	[number]	k4	0
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Ochranné	ochranný	k2eAgNnSc1d1	ochranné
pásmo	pásmo	k1gNnSc1	pásmo
nebylo	být	k5eNaImAgNnS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Vychylovské	Vychylovský	k2eAgInPc1d1	Vychylovský
prahy	práh	k1gInPc1	práh
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Vychylovské	Vychylovský	k2eAgFnSc2d1	Vychylovský
skálie	skálie	k1gFnSc2	skálie
<g/>
,	,	kIx,	,
Štátny	Štátna	k1gFnSc2	Štátna
zoznam	zoznam	k6eAd1	zoznam
osobitne	osobitnout	k5eAaPmIp3nS	osobitnout
chránených	chránená	k1gFnPc2	chránená
častí	častit	k5eAaImIp3nP	častit
prírody	prírod	k1gInPc1	prírod
SR	SR	kA	SR
</s>
</p>
<p>
<s>
Chránené	Chránená	k1gFnPc1	Chránená
územia	územia	k1gFnSc1	územia
<g/>
,	,	kIx,	,
Štátna	Štáten	k2eAgFnSc1d1	Štátna
ochrana	ochrana	k1gFnSc1	ochrana
prírody	príroda	k1gFnSc2	príroda
Slovenskej	Slovenskej	k?	Slovenskej
republiky	republika	k1gFnSc2	republika
</s>
</p>
