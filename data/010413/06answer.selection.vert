<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
urychleně	urychleně	k6eAd1	urychleně
zavést	zavést	k5eAaPmF	zavést
novou	nový	k2eAgFnSc4d1	nová
měnu	měna	k1gFnSc4	měna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
zemi	zem	k1gFnSc3	zem
oddělila	oddělit	k5eAaPmAgFnS	oddělit
od	od	k7c2	od
měn	měna	k1gFnPc2	měna
okolních	okolní	k2eAgInPc2d1	okolní
států	stát	k1gInPc2	stát
zasažených	zasažený	k2eAgInPc2d1	zasažený
vysokou	vysoký	k2eAgFnSc7d1	vysoká
inflací	inflace	k1gFnSc7	inflace
<g/>
.	.	kIx.	.
</s>
