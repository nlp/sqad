<p>
<s>
První	první	k4xOgFnSc1	první
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
Československo	Československo	k1gNnSc4	Československo
v	v	k7c4	v
období	období	k1gNnSc4	období
od	od	k7c2	od
jeho	jeho	k3xOp3gInSc2	jeho
vzniku	vznik	k1gInSc2	vznik
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
do	do	k7c2	do
přijetí	přijetí	k1gNnSc2	přijetí
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
a	a	k8xC	a
postoupení	postoupení	k1gNnSc1	postoupení
pohraničí	pohraničí	k1gNnSc1	pohraničí
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
druhá	druhý	k4xOgFnSc1	druhý
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
a	a	k8xC	a
třetí	třetí	k4xOgFnSc1	třetí
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
toto	tento	k3xDgNnSc1	tento
názvosloví	názvosloví	k1gNnSc1	názvosloví
bylo	být	k5eAaImAgNnS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
Francouzských	francouzský	k2eAgFnPc2d1	francouzská
republik	republika	k1gFnPc2	republika
číslovaných	číslovaný	k2eAgFnPc2d1	číslovaná
od	od	k7c2	od
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
Československa	Československo	k1gNnSc2	Československo
==	==	k?	==
</s>
</p>
<p>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
Československa	Československo	k1gNnSc2	Československo
byla	být	k5eAaImAgFnS	být
prohlášena	prohlášen	k2eAgFnSc1d1	prohlášena
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
pětičlenným	pětičlenný	k2eAgInSc7d1	pětičlenný
Národním	národní	k2eAgInSc7d1	národní
výborem	výbor	k1gInSc7	výbor
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
jehož	jehož	k3xOyRp3gInPc4	jehož
členy	člen	k1gInPc4	člen
byli	být	k5eAaImAgMnP	být
Antonín	Antonín	k1gMnSc1	Antonín
Švehla	Švehla	k1gMnSc1	Švehla
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Stříbrný	stříbrný	k1gInSc1	stříbrný
<g/>
,	,	kIx,	,
Vavro	Vavro	k1gNnSc1	Vavro
Šrobár	Šrobár	k1gMnSc1	Šrobár
a	a	k8xC	a
František	František	k1gMnSc1	František
Soukup	Soukup	k1gMnSc1	Soukup
-	-	kIx~	-
"	"	kIx"	"
<g/>
muži	muž	k1gMnSc3	muž
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
součinnosti	součinnost	k1gFnSc6	součinnost
se	s	k7c7	s
zahraniční	zahraniční	k2eAgFnSc7d1	zahraniční
akcí	akce	k1gFnSc7	akce
Československé	československý	k2eAgFnSc2d1	Československá
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
o	o	k7c4	o
4	[number]	k4	4
dny	den	k1gInPc7	den
dříve	dříve	k6eAd2	dříve
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
za	za	k7c4	za
prozatímní	prozatímní	k2eAgFnSc4d1	prozatímní
vládu	vláda	k1gFnSc4	vláda
nového	nový	k2eAgInSc2d1	nový
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
prosadil	prosadit	k5eAaPmAgInS	prosadit
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
uchování	uchování	k1gNnSc2	uchování
veřejného	veřejný	k2eAgInSc2d1	veřejný
pořádku	pořádek	k1gInSc2	pořádek
kontinuitu	kontinuita	k1gFnSc4	kontinuita
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
vládl	vládnout	k5eAaImAgMnS	vládnout
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
dekretů	dekret	k1gInPc2	dekret
<g/>
.	.	kIx.	.
</s>
<s>
Počátečním	počáteční	k2eAgInSc7d1	počáteční
problémem	problém	k1gInSc7	problém
bylo	být	k5eAaImAgNnS	být
sestavení	sestavení	k1gNnSc1	sestavení
československého	československý	k2eAgInSc2d1	československý
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
volby	volba	k1gFnPc1	volba
byly	být	k5eAaImAgFnP	být
zatím	zatím	k6eAd1	zatím
nereálné	reálný	k2eNgFnPc1d1	nereálná
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
parlament	parlament	k1gInSc1	parlament
–	–	k?	–
Revoluční	revoluční	k2eAgNnSc1d1	revoluční
národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
–	–	k?	–
poskládán	poskládán	k2eAgMnSc1d1	poskládán
na	na	k7c6	na
základě	základ	k1gInSc6	základ
voleb	volba	k1gFnPc2	volba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
do	do	k7c2	do
rakousko-uherského	rakouskoherský	k2eAgInSc2d1	rakousko-uherský
sněmu	sněm	k1gInSc2	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
bylo	být	k5eAaImAgNnS	být
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
44	[number]	k4	44
jmenovanými	jmenovaný	k2eAgMnPc7d1	jmenovaný
poslanci	poslanec	k1gMnPc7	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
předal	předat	k5eAaPmAgInS	předat
své	svůj	k3xOyFgFnPc4	svůj
pravomoci	pravomoc	k1gFnPc4	pravomoc
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
zasedání	zasedání	k1gNnSc6	zasedání
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
prezidentem	prezident	k1gMnSc7	prezident
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gNnSc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
<g/>
,	,	kIx,	,
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
a	a	k8xC	a
ministrem	ministr	k1gMnSc7	ministr
vojenství	vojenství	k1gNnSc2	vojenství
Milan	Milan	k1gMnSc1	Milan
Rastislav	Rastislav	k1gMnSc1	Rastislav
Štefánik	Štefánik	k1gMnSc1	Štefánik
<g/>
;	;	kIx,	;
také	také	k9	také
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
sesazen	sesazen	k2eAgInSc1d1	sesazen
rod	rod	k1gInSc1	rod
Habsburků	Habsburk	k1gMnPc2	Habsburk
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
ústava	ústava	k1gFnSc1	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
neměli	mít	k5eNaImAgMnP	mít
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
parlamentu	parlament	k1gInSc6	parlament
zastoupení	zastoupení	k1gNnSc2	zastoupení
žádné	žádný	k3yNgFnSc2	žádný
(	(	kIx(	(
<g/>
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
se	se	k3xPyFc4	se
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
poválečné	poválečný	k2eAgFnSc2d1	poválečná
deklarace	deklarace	k1gFnSc2	deklarace
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
Thomase	Thomas	k1gMnSc2	Thomas
Woodrowa	Woodrowus	k1gMnSc2	Woodrowus
Wilsona	Wilson	k1gMnSc2	Wilson
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
právu	právo	k1gNnSc6	právo
národů	národ	k1gInPc2	národ
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
na	na	k7c4	na
autonomii	autonomie	k1gFnSc4	autonomie
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
jako	jako	k8xC	jako
základní	základní	k2eAgInSc4d1	základní
princip	princip	k1gInSc4	princip
mírového	mírový	k2eAgNnSc2d1	Mírové
ujednání	ujednání	k1gNnSc2	ujednání
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
sudetských	sudetský	k2eAgMnPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
hlásila	hlásit	k5eAaImAgFnS	hlásit
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
či	či	k8xC	či
Rakousku	Rakousko	k1gNnSc3	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nárokovaných	nárokovaný	k2eAgFnPc6d1	nárokovaná
oblastech	oblast	k1gFnPc6	oblast
formálně	formálně	k6eAd1	formálně
převzaly	převzít	k5eAaPmAgInP	převzít
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
německé	německý	k2eAgInPc4d1	německý
národní	národní	k2eAgInPc4d1	národní
orgány	orgán	k1gInPc4	orgán
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
císaře	císař	k1gMnSc2	císař
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
reprezentace	reprezentace	k1gFnSc1	reprezentace
se	se	k3xPyFc4	se
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
pohraničních	pohraniční	k2eAgNnPc6d1	pohraniční
území	území	k1gNnSc6	území
vzdát	vzdát	k5eAaPmF	vzdát
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
nový	nový	k2eAgInSc1d1	nový
československý	československý	k2eAgInSc1d1	československý
stát	stát	k1gInSc1	stát
nemůže	moct	k5eNaImIp3nS	moct
ekonomicky	ekonomicky	k6eAd1	ekonomicky
<g/>
,	,	kIx,	,
sociálně	sociálně	k6eAd1	sociálně
ani	ani	k8xC	ani
politicky	politicky	k6eAd1	politicky
existovat	existovat	k5eAaImF	existovat
bez	bez	k7c2	bez
pohraničí	pohraničí	k1gNnSc2	pohraničí
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
než	než	k8xS	než
jako	jako	k9	jako
koloniální	koloniální	k2eAgInSc1d1	koloniální
protektorát	protektorát	k1gInSc1	protektorát
Velkého	velký	k2eAgNnSc2d1	velké
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgNnSc4d1	vojenské
obsazení	obsazení	k1gNnSc4	obsazení
nárokovaného	nárokovaný	k2eAgNnSc2d1	nárokované
území	území	k1gNnSc2	území
Sudet	Sudety	k1gFnPc2	Sudety
a	a	k8xC	a
vojenské	vojenský	k2eAgNnSc1d1	vojenské
potlačení	potlačení	k1gNnSc1	potlačení
následné	následný	k2eAgFnSc2d1	následná
sudetské	sudetský	k2eAgFnSc2d1	sudetská
demonstrace	demonstrace	k1gFnSc2	demonstrace
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
či	či	k8xC	či
Rakousku	Rakousko	k1gNnSc3	Rakousko
se	se	k3xPyFc4	se
Maďaři	Maďar	k1gMnPc1	Maďar
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Slovenska	Slovensko	k1gNnSc2	Slovensko
zase	zase	k9	zase
hlásili	hlásit	k5eAaImAgMnP	hlásit
k	k	k7c3	k
Maďarsku	Maďarsko	k1gNnSc3	Maďarsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mírová	mírový	k2eAgFnSc1d1	mírová
konference	konference	k1gFnSc1	konference
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
většinu	většina	k1gFnSc4	většina
územních	územní	k2eAgInPc2d1	územní
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Československou	československý	k2eAgFnSc4d1	Československá
delegaci	delegace	k1gFnSc4	delegace
vedli	vést	k5eAaImAgMnP	vést
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
a	a	k8xC	a
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Konference	konference	k1gFnSc1	konference
schválila	schválit	k5eAaPmAgFnS	schválit
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
rozprostírající	rozprostírající	k2eAgFnSc2d1	rozprostírající
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
historických	historický	k2eAgFnPc2d1	historická
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
(	(	kIx(	(
<g/>
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
malé	malý	k2eAgFnSc2d1	malá
části	část	k1gFnSc2	část
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
části	část	k1gFnPc1	část
Horních	horní	k2eAgFnPc2d1	horní
Uher	Uhry	k1gFnPc2	Uhry
(	(	kIx(	(
<g/>
Slovensko	Slovensko	k1gNnSc1	Slovensko
a	a	k8xC	a
Podkarpatská	podkarpatský	k2eAgFnSc1d1	Podkarpatská
Rus	Rus	k1gFnSc1	Rus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Lužice	Lužice	k1gFnSc2	Lužice
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
Beneš	Beneš	k1gMnSc1	Beneš
nárokoval	nárokovat	k5eAaImAgMnS	nárokovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
příslušnosti	příslušnost	k1gFnSc2	příslušnost
Českému	český	k2eAgNnSc3d1	české
království	království	k1gNnSc3	království
do	do	k7c2	do
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
a	a	k8xC	a
Kladské	kladský	k2eAgNnSc1d1	Kladské
hrabství	hrabství	k1gNnSc1	hrabství
<g/>
,	,	kIx,	,
ztracené	ztracený	k2eAgFnPc1d1	ztracená
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Slezskem	Slezsko	k1gNnSc7	Slezsko
za	za	k7c2	za
Tereziánských	tereziánský	k2eAgFnPc2d1	Tereziánská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
německé	německý	k2eAgFnPc1d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
stvrzující	stvrzující	k2eAgFnSc4d1	stvrzující
konferenci	konference	k1gFnSc4	konference
byla	být	k5eAaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
s	s	k7c7	s
německými	německý	k2eAgMnPc7d1	německý
delegáty	delegát	k1gMnPc7	delegát
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1919	[number]	k4	1919
a	a	k8xC	a
v	v	k7c4	v
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
platnost	platnost	k1gFnSc4	platnost
měla	mít	k5eAaImAgFnS	mít
vstoupit	vstoupit	k5eAaPmF	vstoupit
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
Československo	Československo	k1gNnSc1	Československo
nezískalo	získat	k5eNaPmAgNnS	získat
ani	ani	k8xC	ani
širší	široký	k2eAgNnSc4d2	širší
území	území	k1gNnSc4	území
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
patřící	patřící	k2eAgFnSc1d1	patřící
Rakousku	Rakousko	k1gNnSc6	Rakousko
(	(	kIx(	(
<g/>
jemuž	jenž	k3xRgNnSc3	jenž
byly	být	k5eAaImAgFnP	být
mírové	mírový	k2eAgFnPc1d1	mírová
podmínky	podmínka	k1gFnPc1	podmínka
předloženy	předložen	k2eAgFnPc4d1	předložena
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1919	[number]	k4	1919
a	a	k8xC	a
po	po	k7c6	po
menších	malý	k2eAgFnPc6d2	menší
změnách	změna	k1gFnPc6	změna
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
týkajících	týkající	k2eAgNnPc2d1	týkající
se	se	k3xPyFc4	se
hospodářství	hospodářství	k1gNnPc2	hospodářství
a	a	k8xC	a
financí	finance	k1gFnPc2	finance
byla	být	k5eAaImAgFnS	být
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
podepsána	podepsat	k5eAaPmNgFnS	podepsat
v	v	k7c4	v
Saint-Germain-en	Saint-Germainn	k2eAgInSc4d1	Saint-Germain-en
Laye	Laye	k1gInSc4	Laye
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
rozsáhlejší	rozsáhlý	k2eAgFnPc1d2	rozsáhlejší
oblasti	oblast	k1gFnPc1	oblast
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
pohoří	pohoří	k1gNnSc2	pohoří
Mátra	Mátr	k1gMnSc2	Mátr
a	a	k8xC	a
Bük	Bük	k1gMnSc2	Bük
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
městem	město	k1gNnSc7	město
Miskolc	Miskolc	k1gInSc1	Miskolc
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
si	se	k3xPyFc3	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
republiky	republika	k1gFnSc2	republika
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
začleněno	začlenit	k5eAaPmNgNnS	začlenit
6	[number]	k4	6
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
historickými	historický	k2eAgFnPc7d1	historická
<g/>
,	,	kIx,	,
politickými	politický	k2eAgFnPc7d1	politická
a	a	k8xC	a
ekonomickými	ekonomický	k2eAgFnPc7d1	ekonomická
tradicemi	tradice	k1gFnPc7	tradice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
článku	článek	k1gInSc6	článek
86	[number]	k4	86
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
se	se	k3xPyFc4	se
však	však	k9	však
Československo	Československo	k1gNnSc1	Československo
zavázalo	zavázat	k5eAaPmAgNnS	zavázat
přijmout	přijmout	k5eAaPmF	přijmout
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
případná	případný	k2eAgNnPc1d1	případné
opatření	opatření	k1gNnPc1	opatření
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
by	by	kYmCp3nP	by
vítězné	vítězný	k2eAgFnPc1d1	vítězná
mocnosti	mocnost	k1gFnPc1	mocnost
udělaly	udělat	k5eAaPmAgFnP	udělat
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
menšin	menšina	k1gFnPc2	menšina
na	na	k7c6	na
československém	československý	k2eAgNnSc6d1	Československé
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Problémem	problém	k1gInSc7	problém
byly	být	k5eAaImAgFnP	být
samotné	samotný	k2eAgFnPc1d1	samotná
hranice	hranice	k1gFnPc1	hranice
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
Rakouskem	Rakousko	k1gNnSc7	Rakousko
se	se	k3xPyFc4	se
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
průběhu	průběh	k1gInSc2	průběh
hranic	hranice	k1gFnPc2	hranice
vyřešila	vyřešit	k5eAaPmAgFnS	vyřešit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
historického	historický	k2eAgInSc2d1	historický
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
aplikací	aplikace	k1gFnSc7	aplikace
historického	historický	k2eAgNnSc2d1	historické
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Saint-germainská	Saintermainský	k2eAgFnSc1d1	Saint-germainská
konference	konference	k1gFnSc1	konference
Československu	Československo	k1gNnSc6	Československo
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
obrany	obrana	k1gFnSc2	obrana
navíc	navíc	k6eAd1	navíc
přidělila	přidělit	k5eAaPmAgFnS	přidělit
i	i	k9	i
severní	severní	k2eAgInPc1d1	severní
svahy	svah	k1gInPc1	svah
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
nezrealizovalo	zrealizovat	k5eNaPmAgNnS	zrealizovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
však	však	k9	však
žádné	žádný	k3yNgFnPc1	žádný
historické	historický	k2eAgFnPc1d1	historická
hranice	hranice	k1gFnPc1	hranice
neexistovaly	existovat	k5eNaImAgFnP	existovat
–	–	k?	–
ty	ten	k3xDgInPc4	ten
proto	proto	k6eAd1	proto
určila	určit	k5eAaPmAgFnS	určit
zmiňovaná	zmiňovaný	k2eAgFnSc1d1	zmiňovaná
konference	konference	k1gFnSc1	konference
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Benešových	Benešových	k2eAgNnPc2d1	Benešových
7	[number]	k4	7
memorand	memorandum	k1gNnPc2	memorandum
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
potvrzení	potvrzení	k1gNnSc4	potvrzení
Trianonské	trianonský	k2eAgFnSc2d1	Trianonská
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
musela	muset	k5eAaImAgFnS	muset
na	na	k7c6	na
území	území	k1gNnSc6	území
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
československá	československý	k2eAgFnSc1d1	Československá
a	a	k8xC	a
rumunská	rumunský	k2eAgFnSc1d1	rumunská
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Slovenska	Slovensko	k1gNnSc2	Slovensko
bylo	být	k5eAaImAgNnS	být
tisíc	tisíc	k4xCgInSc1	tisíc
let	let	k1gInSc1	let
součástí	součást	k1gFnPc2	součást
koruny	koruna	k1gFnSc2	koruna
uherské	uherský	k2eAgFnSc2d1	uherská
a	a	k8xC	a
zde	zde	k6eAd1	zde
se	s	k7c7	s
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezkem	Slezek	k1gInSc7	Slezek
<g/>
,	,	kIx,	,
uplatnilo	uplatnit	k5eAaPmAgNnS	uplatnit
přirozené	přirozený	k2eAgNnSc1d1	přirozené
právo	právo	k1gNnSc1	právo
<g/>
.	.	kIx.	.
</s>
<s>
Metropolí	metropol	k1gFnPc2	metropol
Slovenska	Slovensko	k1gNnSc2	Slovensko
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Bratislava	Bratislava	k1gFnSc1	Bratislava
(	(	kIx(	(
<g/>
původním	původní	k2eAgNnSc7d1	původní
jménem	jméno	k1gNnSc7	jméno
Prešpurk	Prešpurk	k?	Prešpurk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
etnicky	etnicky	k6eAd1	etnicky
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
80	[number]	k4	80
procent	procento	k1gNnPc2	procento
německá	německý	k2eAgFnSc1d1	německá
a	a	k8xC	a
maďarská	maďarský	k2eAgFnSc1d1	maďarská
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gInSc2	Garrigu
Masaryk	Masaryk	k1gMnSc1	Masaryk
ale	ale	k8xC	ale
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Československo	Československo	k1gNnSc1	Československo
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
Dunaj	Dunaj	k1gInSc1	Dunaj
a	a	k8xC	a
velmoci	velmoc	k1gFnSc2	velmoc
se	se	k3xPyFc4	se
připojily	připojit	k5eAaPmAgInP	připojit
k	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
nárokům	nárok	k1gInPc3	nárok
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
i	i	k9	i
spor	spor	k1gInSc1	spor
o	o	k7c6	o
území	území	k1gNnSc6	území
Těšínska	Těšínsko	k1gNnSc2	Těšínsko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
historického	historický	k2eAgNnSc2d1	historické
práva	právo	k1gNnSc2	právo
náleželo	náležet	k5eAaImAgNnS	náležet
celé	celý	k2eAgNnSc4d1	celé
Těšínsko	Těšínsko	k1gNnSc4	Těšínsko
k	k	k7c3	k
zemím	zem	k1gFnPc3	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnPc4d1	Česká
<g/>
,	,	kIx,	,
nárok	nárok	k1gInSc1	nárok
si	se	k3xPyFc3	se
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
z	z	k7c2	z
etnických	etnický	k2eAgInPc2d1	etnický
důvodů	důvod	k1gInPc2	důvod
ale	ale	k8xC	ale
činilo	činit	k5eAaImAgNnS	činit
i	i	k9	i
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
československo-polském	československoolský	k2eAgNnSc6d1	československo-polský
vojenském	vojenský	k2eAgNnSc6d1	vojenské
střetnutí	střetnutí	k1gNnSc6	střetnutí
bylo	být	k5eAaImAgNnS	být
Těšínsko	Těšínsko	k1gNnSc1	Těšínsko
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
arbitráže	arbitráž	k1gFnSc2	arbitráž
ve	v	k7c6	v
Spa	Spa	k1gFnSc6	Spa
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
oba	dva	k4xCgInPc4	dva
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
spor	spor	k1gInSc1	spor
byl	být	k5eAaImAgInS	být
ohledně	ohledně	k7c2	ohledně
průmyslově	průmyslově	k6eAd1	průmyslově
zaostalé	zaostalý	k2eAgFnSc2d1	zaostalá
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
nárokovala	nárokovat	k5eAaImAgFnS	nárokovat
kromě	kromě	k7c2	kromě
Československa	Československo	k1gNnSc2	Československo
také	také	k6eAd1	také
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
19	[number]	k4	19
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
však	však	k9	však
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
Podkarpatská	podkarpatský	k2eAgFnSc1d1	Podkarpatská
Rus	Rus	k1gFnSc1	Rus
vůbec	vůbec	k9	vůbec
nesousedila	sousedit	k5eNaImAgFnS	sousedit
(	(	kIx(	(
<g/>
byla	být	k5eAaImAgFnS	být
oddělena	oddělen	k2eAgFnSc1d1	oddělena
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
opět	opět	k6eAd1	opět
musela	muset	k5eAaImAgFnS	muset
řešit	řešit	k5eAaImF	řešit
československá	československý	k2eAgFnSc1d1	Československá
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
legionáři	legionář	k1gMnPc7	legionář
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rumunskou	rumunský	k2eAgFnSc7d1	rumunská
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
problémy	problém	k1gInPc1	problém
nově	nově	k6eAd1	nově
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
státu	stát	k1gInSc2	stát
byly	být	k5eAaImAgFnP	být
také	také	k9	také
výrazné	výrazný	k2eAgInPc4d1	výrazný
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
a	a	k8xC	a
sociální	sociální	k2eAgInPc4d1	sociální
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
historickými	historický	k2eAgFnPc7d1	historická
zeměmi	zem	k1gFnPc7	zem
(	(	kIx(	(
<g/>
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
,	,	kIx,	,
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
Slezsko	Slezsko	k1gNnSc1	Slezsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
Slovenskem	Slovensko	k1gNnSc7	Slovensko
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Podkarpatskou	podkarpatský	k2eAgFnSc7d1	Podkarpatská
Rusí	Rus	k1gFnSc7	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
odcházeli	odcházet	k5eAaImAgMnP	odcházet
čeští	český	k2eAgMnPc1d1	český
lékaři	lékař	k1gMnPc1	lékař
<g/>
,	,	kIx,	,
učitelé	učitel	k1gMnPc1	učitel
a	a	k8xC	a
úředníci	úředník	k1gMnPc1	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Bratislavská	bratislavský	k2eAgFnSc1d1	Bratislavská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
Slovenské	slovenský	k2eAgNnSc4d1	slovenské
národní	národní	k2eAgNnSc4d1	národní
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
však	však	k9	však
část	část	k1gFnSc1	část
Slováků	Slovák	k1gMnPc2	Slovák
a	a	k8xC	a
zejména	zejména	k9	zejména
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
Němců	Němec	k1gMnPc2	Němec
a	a	k8xC	a
700	[number]	k4	700
tisíc	tisíc	k4xCgInPc2	tisíc
Maďarů	maďar	k1gInPc2	maďar
necítili	cítit	k5eNaImAgMnP	cítit
být	být	k5eAaImF	být
rovnocennou	rovnocenný	k2eAgFnSc7d1	rovnocenná
součástí	součást	k1gFnSc7	součást
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
tak	tak	k6eAd1	tak
narůstaly	narůstat	k5eAaImAgFnP	narůstat
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
rozpory	rozpor	k1gInPc4	rozpor
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
Slováci	Slovák	k1gMnPc1	Slovák
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
pociťovali	pociťovat	k5eAaImAgMnP	pociťovat
nostalgii	nostalgie	k1gFnSc4	nostalgie
z	z	k7c2	z
odštěpení	odštěpení	k1gNnSc2	odštěpení
od	od	k7c2	od
Uherska	Uhersko	k1gNnSc2	Uhersko
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ve	v	k7c4	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
již	již	k6eAd1	již
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
okrajovou	okrajový	k2eAgFnSc4d1	okrajová
záležitost	záležitost	k1gFnSc4	záležitost
<g/>
.	.	kIx.	.
<g/>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
Slovenska	Slovensko	k1gNnSc2	Slovensko
existovala	existovat	k5eAaImAgFnS	existovat
i	i	k9	i
tzv.	tzv.	kA	tzv.
Lemko-rusínská	Lemkousínský	k2eAgFnSc1d1	Lemko-rusínský
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
část	část	k1gFnSc1	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
chtěla	chtít	k5eAaImAgFnS	chtít
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
připojit	připojit	k5eAaPmF	připojit
k	k	k7c3	k
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
nechtěje	chtít	k5eNaImSgMnS	chtít
po	po	k7c4	po
sporu	spora	k1gFnSc4	spora
o	o	k7c4	o
Těšínsko	Těšínsko	k1gNnSc4	Těšínsko
jitřit	jitřit	k5eAaImF	jitřit
česko-polské	českoolský	k2eAgInPc4d1	česko-polský
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
neprojevil	projevit	k5eNaPmAgMnS	projevit
o	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
zemi	zem	k1gFnSc4	zem
zájem	zájem	k1gInSc1	zájem
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1920	[number]	k4	1920
ji	on	k3xPp3gFnSc4	on
proto	proto	k8xC	proto
zabralo	zabrat	k5eAaPmAgNnS	zabrat
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
nového	nový	k2eAgInSc2d1	nový
státu	stát	k1gInSc2	stát
==	==	k?	==
</s>
</p>
<p>
<s>
Nová	nový	k2eAgFnSc1d1	nová
ústava	ústava	k1gFnSc1	ústava
z	z	k7c2	z
29	[number]	k4	29
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1920	[number]	k4	1920
nahradila	nahradit	k5eAaPmAgFnS	nahradit
onu	onen	k3xDgFnSc4	onen
provizorní	provizorní	k2eAgFnSc4d1	provizorní
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
bylo	být	k5eAaImAgNnS	být
ustanoveno	ustanovit	k5eAaPmNgNnS	ustanovit
jako	jako	k8xC	jako
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
,	,	kIx,	,
řízená	řízený	k2eAgFnSc1d1	řízená
parlamentem	parlament	k1gInSc7	parlament
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
skládal	skládat	k5eAaImAgInS	skládat
ze	z	k7c2	z
Senátu	senát	k1gInSc2	senát
a	a	k8xC	a
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
parlamentu	parlament	k1gInSc2	parlament
byli	být	k5eAaImAgMnP	být
voleni	volit	k5eAaImNgMnP	volit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
poměrného	poměrný	k2eAgInSc2d1	poměrný
systému	systém	k1gInSc2	systém
ve	v	k7c6	v
všeobecných	všeobecný	k2eAgFnPc6d1	všeobecná
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
měl	mít	k5eAaImAgInS	mít
jak	jak	k6eAd1	jak
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
tak	tak	k8xC	tak
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
později	pozdě	k6eAd2	pozdě
přešla	přejít	k5eAaPmAgFnS	přejít
výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
na	na	k7c4	na
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
prezidenta	prezident	k1gMnSc4	prezident
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
volen	volit	k5eAaImNgInS	volit
parlamentem	parlament	k1gInSc7	parlament
na	na	k7c6	na
7	[number]	k4	7
let	léto	k1gNnPc2	léto
a	a	k8xC	a
jmenován	jmenován	k2eAgInSc4d1	jmenován
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
vládě	vláda	k1gFnSc3	vláda
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgFnPc4d1	velká
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ohledně	ohledně	k7c2	ohledně
lokálních	lokální	k2eAgInPc2d1	lokální
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
bylo	být	k5eAaImAgNnS	být
Československo	Československo	k1gNnSc1	Československo
unitárním	unitární	k2eAgInSc7d1	unitární
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1928	[number]	k4	1928
bylo	být	k5eAaImAgNnS	být
Československo	Československo	k1gNnSc1	Československo
administrativně	administrativně	k6eAd1	administrativně
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
pět	pět	k4xCc4	pět
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
zemi	zem	k1gFnSc4	zem
Českou	český	k2eAgFnSc4d1	Česká
<g/>
,	,	kIx,	,
zemi	zem	k1gFnSc4	zem
Moravskou	moravský	k2eAgFnSc4d1	Moravská
<g/>
,	,	kIx,	,
zemi	zem	k1gFnSc4	zem
Slezskou	Slezská	k1gFnSc4	Slezská
<g/>
,	,	kIx,	,
zemi	zem	k1gFnSc4	zem
Slovenskou	slovenský	k2eAgFnSc4d1	slovenská
a	a	k8xC	a
zemi	zem	k1gFnSc4	zem
Podkarpatoruskou	podkarpatoruský	k2eAgFnSc4d1	Podkarpatoruská
<g/>
.	.	kIx.	.
</s>
<s>
Centry	centr	k1gInPc1	centr
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zemí	zem	k1gFnPc2	zem
byla	být	k5eAaImAgNnP	být
zemská	zemský	k2eAgNnPc1d1	zemské
města	město	k1gNnPc1	město
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
s	s	k7c7	s
částečným	částečný	k2eAgNnSc7d1	částečné
uznáním	uznání	k1gNnSc7	uznání
tradičního	tradiční	k2eAgNnSc2d1	tradiční
druhého	druhý	k4xOgNnSc2	druhý
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
byla	být	k5eAaImAgFnS	být
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Opava	Opava	k1gFnSc1	Opava
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
a	a	k8xC	a
Užhorod	Užhorod	k1gInSc1	Užhorod
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
Podkarpatské	podkarpatský	k2eAgFnSc6d1	Podkarpatská
Rusi	Rus	k1gFnSc6	Rus
tak	tak	k6eAd1	tak
ostatní	ostatní	k2eAgFnPc1d1	ostatní
země	zem	k1gFnPc1	zem
navazovaly	navazovat	k5eAaImAgFnP	navazovat
na	na	k7c4	na
předchozí	předchozí	k2eAgInSc4d1	předchozí
správní	správní	k2eAgInSc4d1	správní
model	model	k1gInSc4	model
existující	existující	k2eAgInSc4d1	existující
s	s	k7c7	s
jistými	jistý	k2eAgFnPc7d1	jistá
menšími	malý	k2eAgFnPc7d2	menší
úpravami	úprava	k1gFnPc7	úprava
již	již	k6eAd1	již
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
Slezská	Slezská	k1gFnSc1	Slezská
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
Moravskou	moravský	k2eAgFnSc7d1	Moravská
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
oslabil	oslabit	k5eAaPmAgInS	oslabit
vliv	vliv	k1gInSc1	vliv
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1928-1939	[number]	k4	1928-1939
se	se	k3xPyFc4	se
tak	tak	k9	tak
ČSR	ČSR	kA	ČSR
dělila	dělit	k5eAaImAgFnS	dělit
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
samosprávné	samosprávný	k2eAgFnPc4d1	samosprávná
země	zem	k1gFnPc4	zem
<g/>
:	:	kIx,	:
zemi	zem	k1gFnSc4	zem
Českou	český	k2eAgFnSc4d1	Česká
<g/>
,	,	kIx,	,
zemi	zem	k1gFnSc4	zem
Moravskoslezskou	moravskoslezský	k2eAgFnSc4d1	Moravskoslezská
<g/>
,	,	kIx,	,
zemi	zem	k1gFnSc4	zem
Slovenskou	slovenský	k2eAgFnSc4d1	slovenská
a	a	k8xC	a
zemi	zem	k1gFnSc4	zem
Podkarpatoruskou	podkarpatoruský	k2eAgFnSc4d1	Podkarpatoruská
<g/>
.	.	kIx.	.
</s>
<s>
Lokální	lokální	k2eAgInPc4d1	lokální
problémy	problém	k1gInPc4	problém
si	se	k3xPyFc3	se
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
země	zem	k1gFnPc1	zem
řešily	řešit	k5eAaImAgFnP	řešit
samy	sám	k3xTgFnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
a	a	k8xC	a
Podkarpatská	podkarpatský	k2eAgFnSc1d1	Podkarpatská
Rus	Rus	k1gFnSc1	Rus
měly	mít	k5eAaImAgFnP	mít
přislíbenou	přislíbený	k2eAgFnSc4d1	přislíbená
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
době	doba	k1gFnSc6	doba
tzv.	tzv.	kA	tzv.
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
nedočkaly	dočkat	k5eNaPmAgInP	dočkat
<g/>
,	,	kIx,	,
požadavkům	požadavek	k1gInPc3	požadavek
sudetských	sudetský	k2eAgMnPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
byla	být	k5eAaImAgFnS	být
vláda	vláda	k1gFnSc1	vláda
ochotná	ochotný	k2eAgFnSc1d1	ochotná
ustoupit	ustoupit	k5eAaPmF	ustoupit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
byli	být	k5eAaImAgMnP	být
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
příslušníky	příslušník	k1gMnPc4	příslušník
"	"	kIx"	"
<g/>
československého	československý	k2eAgInSc2d1	československý
národa	národ	k1gInSc2	národ
<g/>
"	"	kIx"	"
a	a	k8xC	a
oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
jazyk	jazyk	k1gInSc1	jazyk
československý	československý	k2eAgInSc1d1	československý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořit	vytvořit	k5eAaPmF	vytvořit
pojem	pojem	k1gInSc4	pojem
československý	československý	k2eAgInSc4d1	československý
národ	národ	k1gInSc4	národ
a	a	k8xC	a
spojit	spojit	k5eAaPmF	spojit
tak	tak	k6eAd1	tak
Čechy	Čech	k1gMnPc4	Čech
a	a	k8xC	a
Slováky	Slovák	k1gMnPc4	Slovák
v	v	k7c4	v
Čechoslováky	Čechoslovák	k1gMnPc4	Čechoslovák
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
stát	stát	k1gInSc1	stát
měl	mít	k5eAaImAgInS	mít
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
obyvatel	obyvatel	k1gMnPc2	obyvatel
hlásících	hlásící	k2eAgNnPc2d1	hlásící
se	se	k3xPyFc4	se
k	k	k7c3	k
národnosti	národnost	k1gFnSc3	národnost
německé	německý	k2eAgFnSc2d1	německá
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
menšinám	menšina	k1gFnPc3	menšina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
však	však	k9	však
tvořily	tvořit	k5eAaImAgFnP	tvořit
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
20	[number]	k4	20
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
naprostou	naprostý	k2eAgFnSc4d1	naprostá
rovnoprávnost	rovnoprávnost	k1gFnSc4	rovnoprávnost
(	(	kIx(	(
<g/>
užívání	užívání	k1gNnSc2	užívání
svého	svůj	k3xOyFgInSc2	svůj
jazyka	jazyk	k1gInSc2	jazyk
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
úřední	úřední	k2eAgNnPc1d1	úřední
jednání	jednání	k1gNnPc1	jednání
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rovnoprávnost	rovnoprávnost	k1gFnSc1	rovnoprávnost
byla	být	k5eAaImAgFnS	být
však	však	k9	však
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
narušována	narušovat	k5eAaImNgNnP	narušovat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
tvorbou	tvorba	k1gFnSc7	tvorba
takových	takový	k3xDgInPc6	takový
volebního	volební	k2eAgInSc2d1	volební
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měli	mít	k5eAaImAgMnP	mít
převahu	převaha	k1gFnSc4	převaha
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
či	či	k8xC	či
přijímáním	přijímání	k1gNnSc7	přijímání
pouze	pouze	k6eAd1	pouze
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
do	do	k7c2	do
některých	některý	k3yIgInPc2	některý
vojenských	vojenský	k2eAgInPc2d1	vojenský
útvarů	útvar	k1gInPc2	útvar
apod.	apod.	kA	apod.
Němci	Němec	k1gMnPc7	Němec
i	i	k8xC	i
přesto	přesto	k6eAd1	přesto
měli	mít	k5eAaImAgMnP	mít
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
správě	správa	k1gFnSc6	správa
vysoký	vysoký	k2eAgInSc1d1	vysoký
počet	počet	k1gInSc1	počet
volených	volený	k2eAgMnPc2d1	volený
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
,	,	kIx,	,
existovalo	existovat	k5eAaImAgNnS	existovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
německých	německý	k2eAgFnPc2d1	německá
základních	základní	k2eAgFnPc2d1	základní
<g/>
,	,	kIx,	,
středních	střední	k2eAgFnPc2d1	střední
i	i	k8xC	i
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
či	či	k8xC	či
vydavatelství	vydavatelství	k1gNnPc2	vydavatelství
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
knih	kniha	k1gFnPc2	kniha
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Národnostní	národnostní	k2eAgFnPc1d1	národnostní
menšiny	menšina	k1gFnPc1	menšina
mohly	moct	k5eAaImAgFnP	moct
svobodně	svobodně	k6eAd1	svobodně
zakládat	zakládat	k5eAaImF	zakládat
organizace	organizace	k1gFnPc4	organizace
a	a	k8xC	a
různé	různý	k2eAgInPc4d1	různý
spolky	spolek	k1gInPc4	spolek
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jejich	jejich	k3xOp3gInPc4	jejich
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
autonomii	autonomie	k1gFnSc4	autonomie
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
vláda	vláda	k1gFnSc1	vláda
ochotná	ochotný	k2eAgFnSc1d1	ochotná
splnit	splnit	k5eAaPmF	splnit
až	až	k6eAd1	až
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
v	v	k7c6	v
krizovém	krizový	k2eAgInSc6d1	krizový
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
státu	stát	k1gInSc2	stát
a	a	k8xC	a
občanů	občan	k1gMnPc2	občan
se	se	k3xPyFc4	se
nezakládal	zakládat	k5eNaImAgMnS	zakládat
na	na	k7c6	na
národnostním	národnostní	k2eAgInSc6d1	národnostní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
občanském	občanský	k2eAgInSc6d1	občanský
principu	princip	k1gInSc6	princip
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
stabilitu	stabilita	k1gFnSc4	stabilita
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc4	základ
tvořily	tvořit	k5eAaImAgFnP	tvořit
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
vedení	vedení	k1gNnPc1	vedení
se	se	k3xPyFc4	se
dokázala	dokázat	k5eAaPmAgNnP	dokázat
skoro	skoro	k6eAd1	skoro
vždy	vždy	k6eAd1	vždy
nějak	nějak	k6eAd1	nějak
dohodnout	dohodnout	k5eAaPmF	dohodnout
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c4	vyjma
období	období	k1gNnSc4	období
od	od	k7c2	od
března	březen	k1gInSc2	březen
1926	[number]	k4	1926
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nebyla	být	k5eNaImAgFnS	být
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
koalice	koalice	k1gFnSc1	koalice
<g/>
,	,	kIx,	,
tvořilo	tvořit	k5eAaImAgNnS	tvořit
páteř	páteř	k1gFnSc4	páteř
vlády	vláda	k1gFnSc2	vláda
pět	pět	k4xCc4	pět
zástupců	zástupce	k1gMnPc2	zástupce
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Pětka	pětka	k1gFnSc1	pětka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pěti	pět	k4xCc2	pět
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
představovaly	představovat	k5eAaImAgInP	představovat
<g/>
:	:	kIx,	:
Republikánská	republikánský	k2eAgFnSc1d1	republikánská
strana	strana	k1gFnSc1	strana
zemědělského	zemědělský	k2eAgInSc2d1	zemědělský
a	a	k8xC	a
malorolnického	malorolnický	k2eAgInSc2d1	malorolnický
lidu	lid	k1gInSc2	lid
(	(	kIx(	(
<g/>
agrárníci	agrárník	k1gMnPc1	agrárník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Československá	československý	k2eAgFnSc1d1	Československá
sociálně-demokratická	sociálněemokratický	k2eAgFnSc1d1	sociálně-demokratická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
Národní	národní	k2eAgMnPc1d1	národní
socialisté	socialist	k1gMnPc1	socialist
<g/>
,	,	kIx,	,
Národní	národní	k2eAgFnSc1d1	národní
demokracie	demokracie	k1gFnSc1	demokracie
a	a	k8xC	a
Lidová	lidový	k2eAgFnSc1d1	lidová
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Pětka	pětka	k1gFnSc1	pětka
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
Antonínem	Antonín	k1gMnSc7	Antonín
Švehlou	Švehla	k1gMnSc7	Švehla
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
premiérem	premiér	k1gMnSc7	premiér
skoro	skoro	k6eAd1	skoro
celá	celý	k2eAgFnSc1d1	celá
20	[number]	k4	20
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
točila	točit	k5eAaImAgFnS	točit
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
kolem	kolem	k6eAd1	kolem
známých	známý	k2eAgMnPc2d1	známý
politiků	politik	k1gMnPc2	politik
<g/>
.	.	kIx.	.
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
zástupci	zástupce	k1gMnPc1	zástupce
se	se	k3xPyFc4	se
dostávali	dostávat	k5eAaImAgMnP	dostávat
do	do	k7c2	do
vlády	vláda	k1gFnSc2	vláda
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
maďarské	maďarský	k2eAgFnSc2d1	maďarská
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
i	i	k9	i
přes	přes	k7c4	přes
silnou	silný	k2eAgFnSc4d1	silná
agitaci	agitace	k1gFnSc4	agitace
sousedního	sousední	k2eAgNnSc2d1	sousední
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
do	do	k7c2	do
vlády	vláda	k1gFnSc2	vláda
nikdy	nikdy	k6eAd1	nikdy
nedostaly	dostat	k5eNaPmAgInP	dostat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
prvorepublikového	prvorepublikový	k2eAgNnSc2d1	prvorepublikové
Československa	Československo	k1gNnSc2	Československo
===	===	k?	===
</s>
</p>
<p>
<s>
Republikánská	republikánský	k2eAgFnSc1d1	republikánská
strana	strana	k1gFnSc1	strana
zemědělského	zemědělský	k2eAgInSc2d1	zemědělský
a	a	k8xC	a
malorolnického	malorolnický	k2eAgInSc2d1	malorolnický
lidu	lid	k1gInSc2	lid
(	(	kIx(	(
<g/>
RSZML	RSZML	kA	RSZML
<g/>
,	,	kIx,	,
Agrární	agrární	k2eAgFnSc1d1	agrární
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
spojením	spojení	k1gNnSc7	spojení
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
agrární	agrární	k2eAgFnSc2d1	agrární
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
Antonín	Antonín	k1gMnSc1	Antonín
Švehla	Švehla	k1gMnSc1	Švehla
a	a	k8xC	a
hlavními	hlavní	k2eAgMnPc7d1	hlavní
voliči	volič	k1gMnPc7	volič
byli	být	k5eAaImAgMnP	být
malí	malý	k2eAgMnPc1d1	malý
a	a	k8xC	a
střední	střední	k2eAgMnPc1d1	střední
zemědělci	zemědělec	k1gMnPc1	zemědělec
<g/>
.	.	kIx.	.
</s>
<s>
Švehla	Švehla	k1gMnSc1	Švehla
kombinoval	kombinovat	k5eAaImAgMnS	kombinovat
sociální	sociální	k2eAgInSc4d1	sociální
a	a	k8xC	a
demokratické	demokratický	k2eAgFnPc4d1	demokratická
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
byla	být	k5eAaImAgFnS	být
jádrem	jádro	k1gNnSc7	jádro
vládních	vládní	k2eAgFnPc2d1	vládní
koalicí	koalice	k1gFnPc2	koalice
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1922	[number]	k4	1922
až	až	k9	až
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
<g/>
Československá	československý	k2eAgFnSc1d1	Československá
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
(	(	kIx(	(
<g/>
ČSDSD	ČSDSD	kA	ČSDSD
<g/>
)	)	kIx)	)
navazovala	navazovat	k5eAaImAgFnS	navazovat
na	na	k7c4	na
předválečnou	předválečný	k2eAgFnSc4d1	předválečná
Sociální	sociální	k2eAgFnSc4d1	sociální
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
největší	veliký	k2eAgFnSc7d3	veliký
stranou	strana	k1gFnSc7	strana
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
relativního	relativní	k2eAgInSc2d1	relativní
rozkvětu	rozkvět	k1gInSc2	rozkvět
však	však	k9	však
přišel	přijít	k5eAaPmAgInS	přijít
rozkol	rozkol	k1gInSc1	rozkol
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
vnitrostranická	vnitrostranický	k2eAgFnSc1d1	vnitrostranická
levice	levice	k1gFnSc1	levice
<g/>
,	,	kIx,	,
s	s	k7c7	s
jistým	jistý	k2eAgInSc7d1	jistý
odstupem	odstup	k1gInSc7	odstup
i	i	k8xC	i
pravice	pravice	k1gFnSc1	pravice
<g/>
,	,	kIx,	,
střed	střed	k1gInSc1	střed
zůstával	zůstávat	k5eAaImAgInS	zůstávat
dlouho	dlouho	k6eAd1	dlouho
neutrální	neutrální	k2eAgMnSc1d1	neutrální
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
nakonec	nakonec	k6eAd1	nakonec
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
odložení	odložení	k1gNnSc3	odložení
řádného	řádný	k2eAgInSc2d1	řádný
termínu	termín	k1gInSc2	termín
sjezdu	sjezd	k1gInSc2	sjezd
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
přineslo	přinést	k5eAaPmAgNnS	přinést
spíše	spíše	k9	spíše
sympatie	sympatie	k1gFnPc1	sympatie
Šmeralově	šmeralově	k6eAd1	šmeralově
levici	levice	k1gFnSc3	levice
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
posléze	posléze	k6eAd1	posléze
založila	založit	k5eAaPmAgFnS	založit
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
Komunistickou	komunistický	k2eAgFnSc4d1	komunistická
stranu	strana	k1gFnSc4	strana
Československa-sekci	Československaekce	k1gFnSc4	Československa-sekce
Třetí	třetí	k4xOgFnSc2	třetí
Internacionály	Internacionála	k1gFnSc2	Internacionála
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
byla	být	k5eAaImAgFnS	být
citelně	citelně	k6eAd1	citelně
oslabena	oslabit	k5eAaPmNgFnS	oslabit
a	a	k8xC	a
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
zažila	zažít	k5eAaPmAgFnS	zažít
debakl	debakl	k1gInSc4	debakl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
síle	síla	k1gFnSc6	síla
začala	začít	k5eAaPmAgFnS	začít
nabírat	nabírat	k5eAaImF	nabírat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgMnS	stát
Antonín	Antonín	k1gMnSc1	Antonín
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
Ivan	Ivan	k1gMnSc1	Ivan
Dérer	Dérer	k1gMnSc1	Dérer
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
představovala	představovat	k5eAaImAgFnS	představovat
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
Hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
KSČ	KSČ	kA	KSČ
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
po	po	k7c6	po
výše	vysoce	k6eAd2	vysoce
popsaném	popsaný	k2eAgInSc6d1	popsaný
sporu	spor	k1gInSc6	spor
uvnitř	uvnitř	k7c2	uvnitř
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
o	o	k7c4	o
specifickou	specifický	k2eAgFnSc4d1	specifická
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
socialismu	socialismus	k1gInSc3	socialismus
<g/>
,	,	kIx,	,
prosazovanou	prosazovaný	k2eAgFnSc7d1	prosazovaná
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
"	"	kIx"	"
<g/>
rudou	rudý	k2eAgFnSc7d1	rudá
většinou	většina	k1gFnSc7	většina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
"	"	kIx"	"
<g/>
bolševizace	bolševizace	k1gFnSc1	bolševizace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
stranou	stranou	k6eAd1	stranou
extrémní	extrémní	k2eAgFnSc1d1	extrémní
a	a	k8xC	a
antisystémovou	antisystémová	k1gFnSc7	antisystémová
<g/>
,	,	kIx,	,
plně	plně	k6eAd1	plně
závislou	závislý	k2eAgFnSc7d1	závislá
na	na	k7c6	na
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
KSČ	KSČ	kA	KSČ
představovala	představovat	k5eAaImAgFnS	představovat
v	v	k7c6	v
relativních	relativní	k2eAgNnPc6d1	relativní
číslech	číslo	k1gNnPc6	číslo
největší	veliký	k2eAgFnSc4d3	veliký
komunistickou	komunistický	k2eAgFnSc4d1	komunistická
stranu	strana	k1gFnSc4	strana
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
však	však	k9	však
členstvo	členstvo	k1gNnSc1	členstvo
ubývalo	ubývat	k5eAaImAgNnS	ubývat
(	(	kIx(	(
<g/>
vylučování	vylučování	k1gNnSc1	vylučování
<g/>
,	,	kIx,	,
štěpení	štěpení	k1gNnSc1	štěpení
frakcí	frakce	k1gFnPc2	frakce
<g/>
,	,	kIx,	,
návrat	návrat	k1gInSc1	návrat
k	k	k7c3	k
sociální	sociální	k2eAgFnSc3d1	sociální
demokracii	demokracie	k1gFnSc3	demokracie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
strana	strana	k1gFnSc1	strana
stála	stát	k5eAaImAgFnS	stát
proti	proti	k7c3	proti
Hradu	hrad	k1gInSc3	hrad
<g/>
,	,	kIx,	,
udržovali	udržovat	k5eAaImAgMnP	udržovat
někteří	některý	k3yIgMnPc1	některý
její	její	k3xOp3gMnPc1	její
reformní	reformní	k2eAgMnPc1d1	reformní
funkcionáři	funkcionář	k1gMnPc1	funkcionář
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
prezidentskou	prezidentský	k2eAgFnSc7d1	prezidentská
kanceláří	kancelář	k1gFnSc7	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
dohodě	dohoda	k1gFnSc6	dohoda
o	o	k7c6	o
toleranci	tolerance	k1gFnSc6	tolerance
tzv.	tzv.	kA	tzv.
rudo-černé	rudo-černý	k2eAgFnSc2d1	rudo-černá
aliance	aliance	k1gFnSc2	aliance
(	(	kIx(	(
<g/>
lidovci	lidovec	k1gMnPc1	lidovec
a	a	k8xC	a
socialisté	socialist	k1gMnPc1	socialist
<g/>
)	)	kIx)	)
komunisty	komunista	k1gMnSc2	komunista
se	se	k3xPyFc4	se
například	například	k6eAd1	například
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
před	před	k7c7	před
prezidentskou	prezidentský	k2eAgFnSc7d1	prezidentská
volbou	volba	k1gFnSc7	volba
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
KSČ	KSČ	kA	KSČ
si	se	k3xPyFc3	se
průměrně	průměrně	k6eAd1	průměrně
udržovala	udržovat	k5eAaImAgFnS	udržovat
zisk	zisk	k1gInSc4	zisk
nad	nad	k7c7	nad
10	[number]	k4	10
%	%	kIx~	%
voličstva	voličstvo	k1gNnSc2	voličstvo
<g/>
.	.	kIx.	.
<g/>
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
národně	národně	k6eAd1	národně
socialistická	socialistický	k2eAgFnSc1d1	socialistická
(	(	kIx(	(
<g/>
ČSNS	ČSNS	kA	ČSNS
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
socialistická	socialistický	k2eAgFnSc1d1	socialistická
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
středolevicová	středolevicový	k2eAgFnSc1d1	středolevicová
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
obhajující	obhajující	k2eAgFnSc1d1	obhajující
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
český	český	k2eAgInSc1d1	český
socialismus	socialismus	k1gInSc1	socialismus
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
popřípadě	popřípadě	k6eAd1	popřípadě
národní	národní	k2eAgInSc1d1	národní
socialismus	socialismus	k1gInSc1	socialismus
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
nar	nar	kA	nar
<g/>
.	.	kIx.	.
soc	soc	kA	soc
<g/>
.	.	kIx.	.
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
variace	variace	k1gFnPc1	variace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
strany	strana	k1gFnSc2	strana
stál	stát	k5eAaImAgMnS	stát
Václav	Václav	k1gMnSc1	Václav
Klofáč	klofáč	k1gInSc4	klofáč
<g/>
,	,	kIx,	,
jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
např.	např.	kA	např.
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
(	(	kIx(	(
<g/>
dočasně	dočasně	k6eAd1	dočasně
<g/>
)	)	kIx)	)
a	a	k8xC	a
Milada	Milada	k1gFnSc1	Milada
Horáková	Horáková	k1gFnSc1	Horáková
<g/>
.	.	kIx.	.
</s>
<s>
Katolík	katolík	k1gMnSc1	katolík
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
tam	tam	k6eAd1	tam
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigue	Garrigu	k1gMnSc2	Garrigu
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Patřila	patřit	k5eAaImAgFnS	patřit
ke	k	k7c3	k
stranám	strana	k1gFnPc3	strana
Hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
<g/>
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
lidová	lidový	k2eAgFnSc1d1	lidová
(	(	kIx(	(
<g/>
ČSL	ČSL	kA	ČSL
<g/>
)	)	kIx)	)
–	–	k?	–
sloučena	sloučit	k5eAaPmNgFnS	sloučit
z	z	k7c2	z
Moravsko-slezské	moravskolezský	k2eAgFnSc2d1	moravsko-slezská
křesťanskosociální	křesťanskosociální	k2eAgFnSc2d1	křesťanskosociální
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
Katolicko-národně	katolickoárodně	k6eAd1	katolicko-národně
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
Konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
lidové	lidový	k2eAgFnSc2d1	lidová
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
zastávala	zastávat	k5eAaImAgFnS	zastávat
křesťanské	křesťanský	k2eAgInPc4d1	křesťanský
názory	názor	k1gInPc4	názor
a	a	k8xC	a
sociální	sociální	k2eAgFnSc4d1	sociální
encykliku	encyklika	k1gFnSc4	encyklika
papeže	papež	k1gMnSc2	papež
Lva	Lev	k1gMnSc2	Lev
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
katolickým	katolický	k2eAgMnSc7d1	katolický
knězem	kněz	k1gMnSc7	kněz
Janem	Jan	k1gMnSc7	Jan
Šrámkem	šrámek	k1gInSc7	šrámek
<g/>
.	.	kIx.	.
<g/>
Československá	československý	k2eAgFnSc1d1	Československá
národní	národní	k2eAgFnSc1d1	národní
demokracie	demokracie	k1gFnSc1	demokracie
(	(	kIx(	(
<g/>
ČsND	ČsND	k1gFnSc1	ČsND
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
spojením	spojení	k1gNnSc7	spojení
Mladočechů	mladočech	k1gMnPc2	mladočech
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
pravicovými	pravicový	k2eAgFnPc7d1	pravicová
a	a	k8xC	a
středovými	středový	k2eAgFnPc7d1	středová
stranami	strana	k1gFnPc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ideologicky	ideologicky	k6eAd1	ideologicky
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
jako	jako	k9	jako
národně	národně	k6eAd1	národně
radikální	radikální	k2eAgFnSc1d1	radikální
a	a	k8xC	a
ekonomicky	ekonomicky	k6eAd1	ekonomicky
liberální	liberální	k2eAgFnSc1d1	liberální
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Vedli	vést	k5eAaImAgMnP	vést
ji	on	k3xPp3gFnSc4	on
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
a	a	k8xC	a
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
předčasné	předčasný	k2eAgFnSc2d1	předčasná
smrti	smrt	k1gFnSc2	smrt
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
<g/>
.	.	kIx.	.
</s>
<s>
Volena	volen	k2eAgFnSc1d1	volena
byla	být	k5eAaImAgFnS	být
vyššími	vysoký	k2eAgFnPc7d2	vyšší
a	a	k8xC	a
středními	střední	k2eAgFnPc7d1	střední
vrstvami	vrstva	k1gFnPc7	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Podporovala	podporovat	k5eAaImAgFnS	podporovat
soukromé	soukromý	k2eAgNnSc4d1	soukromé
podnikání	podnikání	k1gNnSc4	podnikání
<g/>
,	,	kIx,	,
bankovnictví	bankovnictví	k1gNnSc4	bankovnictví
a	a	k8xC	a
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Německé	německý	k2eAgFnSc2d1	německá
strany	strana	k1gFnSc2	strana
====	====	k?	====
</s>
</p>
<p>
<s>
Německá	německý	k2eAgFnSc1d1	německá
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
(	(	kIx(	(
<g/>
DSAP	DSAP	kA	DSAP
<g/>
,	,	kIx,	,
Deutsche	Deutschus	k1gMnSc5	Deutschus
sozialdemokratische	sozialdemokratischus	k1gMnSc5	sozialdemokratischus
Arbeiterpartei	Arbeiterparte	k1gMnSc5	Arbeiterparte
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
obdržela	obdržet	k5eAaPmAgFnS	obdržet
téměř	téměř	k6eAd1	téměř
590	[number]	k4	590
000	[number]	k4	000
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
činilo	činit	k5eAaImAgNnS	činit
přes	přes	k7c4	přes
43,5	[number]	k4	43,5
%	%	kIx~	%
německých	německý	k2eAgInPc2d1	německý
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
11,1	[number]	k4	11,1
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
československých	československý	k2eAgInPc2d1	československý
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
31	[number]	k4	31
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
třetím	třetí	k4xOgInSc7	třetí
nejsilnějším	silný	k2eAgInSc7d3	nejsilnější
subjektem	subjekt	k1gInSc7	subjekt
na	na	k7c6	na
území	území	k1gNnSc6	území
celého	celý	k2eAgInSc2d1	celý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
tato	tento	k3xDgFnSc1	tento
strana	strana	k1gFnSc1	strana
nadále	nadále	k6eAd1	nadále
trvala	trvat	k5eAaImAgFnS	trvat
na	na	k7c6	na
negativním	negativní	k2eAgInSc6d1	negativní
postoji	postoj	k1gInSc6	postoj
k	k	k7c3	k
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
pojaté	pojatý	k2eAgFnPc4d1	pojatá
jako	jako	k8xS	jako
dominantně	dominantně	k6eAd1	dominantně
český	český	k2eAgInSc1d1	český
stát	stát	k1gInSc1	stát
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
menšinami	menšina	k1gFnPc7	menšina
<g/>
,	,	kIx,	,
zabránila	zabránit	k5eAaPmAgFnS	zabránit
ústavní	ústavní	k2eAgFnSc4d1	ústavní
krizi	krize	k1gFnSc4	krize
<g/>
:	:	kIx,	:
umožnila	umožnit	k5eAaPmAgFnS	umožnit
vznik	vznik	k1gInSc4	vznik
první	první	k4xOgFnSc2	první
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
když	když	k8xS	když
při	při	k7c6	při
hlasování	hlasování	k1gNnSc6	hlasování
o	o	k7c6	o
důvěře	důvěra	k1gFnSc6	důvěra
nové	nový	k2eAgFnSc3d1	nová
vládě	vláda	k1gFnSc3	vláda
opustili	opustit	k5eAaPmAgMnP	opustit
poslanci	poslanec	k1gMnPc1	poslanec
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
sněmovnu	sněmovna	k1gFnSc4	sněmovna
a	a	k8xC	a
nezabránili	zabránit	k5eNaPmAgMnP	zabránit
tak	tak	k6eAd1	tak
těsnému	těsný	k2eAgNnSc3d1	těsné
zvolení	zvolení	k1gNnSc3	zvolení
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Česká	český	k2eAgFnSc1d1	Česká
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
doplatila	doplatit	k5eAaPmAgFnS	doplatit
na	na	k7c4	na
rozkol	rozkol	k1gInSc4	rozkol
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Komunistické	komunistický	k2eAgNnSc1d1	komunistické
křídlo	křídlo	k1gNnSc1	křídlo
opustilo	opustit	k5eAaPmAgNnS	opustit
stranu	strana	k1gFnSc4	strana
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
prohrálo	prohrát	k5eAaPmAgNnS	prohrát
boj	boj	k1gInSc4	boj
o	o	k7c4	o
vedení	vedení	k1gNnSc4	vedení
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
doplatila	doplatit	k5eAaPmAgFnS	doplatit
strana	strana	k1gFnSc1	strana
při	při	k7c6	při
druhých	druhý	k4xOgFnPc6	druhý
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgInSc1d1	volební
výsledek	výsledek	k1gInSc1	výsledek
činil	činit	k5eAaImAgInS	činit
pouhých	pouhý	k2eAgNnPc6d1	pouhé
6	[number]	k4	6
%	%	kIx~	%
a	a	k8xC	a
17	[number]	k4	17
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
<g/>
Německý	německý	k2eAgInSc1d1	německý
svaz	svaz	k1gInSc1	svaz
zemědělců	zemědělec	k1gMnPc2	zemědělec
(	(	kIx(	(
<g/>
BdL	BdL	k1gFnSc1	BdL
<g/>
,	,	kIx,	,
Bund	bund	k1gInSc1	bund
der	drát	k5eAaImRp2nS	drát
Landwirte	Landwirt	k1gMnSc5	Landwirt
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
aktivistickou	aktivistický	k2eAgFnSc7d1	aktivistická
stranou	strana	k1gFnSc7	strana
německých	německý	k2eAgMnPc2d1	německý
zemědělců	zemědělec	k1gMnPc2	zemědělec
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnPc2	součást
několika	několik	k4yIc2	několik
československých	československý	k2eAgFnPc2d1	Československá
vládních	vládní	k2eAgFnPc2d1	vládní
koalicí	koalice	k1gFnPc2	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
představitelem	představitel	k1gMnSc7	představitel
byl	být	k5eAaImAgMnS	být
Franz	Franz	k1gMnSc1	Franz
Spina	spina	k1gFnSc1	spina
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
též	též	k9	též
opakovaně	opakovaně	k6eAd1	opakovaně
ministrem	ministr	k1gMnSc7	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
sloučením	sloučení	k1gNnSc7	sloučení
se	s	k7c7	s
Sudetoněmeckou	sudetoněmecký	k2eAgFnSc7d1	Sudetoněmecká
stranou	strana	k1gFnSc7	strana
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
<g/>
Německá	německý	k2eAgFnSc1d1	německá
křesťansko	křesťansko	k6eAd1	křesťansko
sociální	sociální	k2eAgFnSc1d1	sociální
strana	strana	k1gFnSc1	strana
lidová	lidový	k2eAgFnSc1d1	lidová
(	(	kIx(	(
<g/>
DCV	DCV	kA	DCV
<g/>
,	,	kIx,	,
Deutsche	Deutsche	k1gFnSc6	Deutsche
Christlichsoziale	Christlichsoziala	k1gFnSc6	Christlichsoziala
Volkspartei	Volksparte	k1gFnSc2	Volksparte
<g/>
)	)	kIx)	)
představovala	představovat	k5eAaImAgFnS	představovat
další	další	k2eAgFnSc4d1	další
německou	německý	k2eAgFnSc4d1	německá
aktivistickou	aktivistický	k2eAgFnSc4d1	aktivistická
stranu	strana	k1gFnSc4	strana
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
katolickou	katolický	k2eAgFnSc7d1	katolická
stranou	strana	k1gFnSc7	strana
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
součástí	součást	k1gFnPc2	součást
několika	několik	k4yIc2	několik
československých	československý	k2eAgFnPc2d1	Československá
vládních	vládní	k2eAgFnPc2d1	vládní
koalicí	koalice	k1gFnPc2	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
představitelem	představitel	k1gMnSc7	představitel
byl	být	k5eAaImAgMnS	být
Robert	Robert	k1gMnSc1	Robert
Mayr-Harting	Mayr-Harting	k1gInSc4	Mayr-Harting
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
SdP	SdP	k1gFnSc2	SdP
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
<g/>
Sudetoněmecká	sudetoněmecký	k2eAgFnSc1d1	Sudetoněmecká
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
SdP	SdP	k1gFnSc1	SdP
<g/>
,	,	kIx,	,
Sudetendeutsche	Sudetendeutsche	k1gFnSc1	Sudetendeutsche
Partei	Parte	k1gFnSc2	Parte
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
silně	silně	k6eAd1	silně
nacionalistickou	nacionalistický	k2eAgFnSc7d1	nacionalistická
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
založena	založen	k2eAgFnSc1d1	založena
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1933	[number]	k4	1933
Konradem	Konrad	k1gInSc7	Konrad
Henleinem	Henlein	k1gMnSc7	Henlein
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
jejím	její	k3xOp3gMnSc7	její
jediným	jediný	k2eAgMnSc7d1	jediný
předsedou	předseda	k1gMnSc7	předseda
až	až	k8xS	až
do	do	k7c2	do
sloučení	sloučení	k1gNnSc2	sloučení
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
s	s	k7c7	s
Hitlerovou	Hitlerová	k1gFnSc7	Hitlerová
NSDAP	NSDAP	kA	NSDAP
po	po	k7c6	po
mnichovském	mnichovský	k2eAgInSc6d1	mnichovský
diktátu	diktát	k1gInSc6	diktát
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
získala	získat	k5eAaPmAgFnS	získat
strana	strana	k1gFnSc1	strana
největší	veliký	k2eAgNnSc4d3	veliký
procento	procento	k1gNnSc4	procento
hlasů	hlas	k1gInPc2	hlas
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
kandidujících	kandidující	k2eAgFnPc2d1	kandidující
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
se	se	k3xPyFc4	se
však	však	k9	však
nepodílela	podílet	k5eNaImAgFnS	podílet
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
byla	být	k5eAaImAgFnS	být
silně	silně	k6eAd1	silně
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
vládě	vláda	k1gFnSc3	vláda
a	a	k8xC	a
pod	pod	k7c7	pod
jejím	její	k3xOp3gInSc7	její
tlakem	tlak	k1gInSc7	tlak
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
rozpadly	rozpadnout	k5eAaPmAgInP	rozpadnout
či	či	k8xC	či
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc2	on
sloučily	sloučit	k5eAaPmAgFnP	sloučit
všechny	všechen	k3xTgFnPc1	všechen
tehdejší	tehdejší	k2eAgFnPc1d1	tehdejší
významné	významný	k2eAgFnPc1d1	významná
německé	německý	k2eAgFnPc1d1	německá
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
(	(	kIx(	(
<g/>
DSAP	DSAP	kA	DSAP
<g/>
,	,	kIx,	,
BdL	BdL	k1gFnSc1	BdL
<g/>
,	,	kIx,	,
DCV	DCV	kA	DCV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
===	===	k?	===
</s>
</p>
<p>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
v	v	k7c6	v
letech	let	k1gInPc6	let
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1935	[number]	k4	1935
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
se	s	k7c7	s
soustavou	soustava	k1gFnSc7	soustava
demokratických	demokratický	k2eAgInPc2d1	demokratický
západních	západní	k2eAgInPc2d1	západní
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
podporovalo	podporovat	k5eAaImAgNnS	podporovat
Československo	Československo	k1gNnSc1	Československo
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
půdě	půda	k1gFnSc6	půda
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Beneš	Beneš	k1gMnSc1	Beneš
věřil	věřit	k5eAaImAgMnS	věřit
ve	v	k7c4	v
Společnost	společnost	k1gFnSc4	společnost
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
propagovala	propagovat	k5eAaImAgFnS	propagovat
poválečný	poválečný	k2eAgInSc4d1	poválečný
status	status	k1gInSc4	status
quo	quo	k?	quo
<g/>
,	,	kIx,	,
mírový	mírový	k2eAgInSc4d1	mírový
rozvoj	rozvoj	k1gInSc4	rozvoj
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
zaručovala	zaručovat	k5eAaImAgFnS	zaručovat
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
těch	ten	k3xDgFnPc2	ten
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyjednal	vyjednat	k5eAaPmAgMnS	vyjednat
také	také	k9	také
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
Jugoslávií	Jugoslávie	k1gFnSc7	Jugoslávie
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k8xS	jako
Malá	malý	k2eAgFnSc1d1	malá
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
měla	mít	k5eAaImAgFnS	mít
zabránit	zabránit	k5eAaPmF	zabránit
maďarské	maďarský	k2eAgFnSc3d1	maďarská
snaze	snaha	k1gFnSc3	snaha
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
svého	svůj	k3xOyFgInSc2	svůj
původního	původní	k2eAgInSc2d1	původní
vlivu	vliv	k1gInSc2	vliv
a	a	k8xC	a
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ohrožoval	ohrožovat	k5eAaImAgMnS	ohrožovat
všechny	všechen	k3xTgInPc4	všechen
tři	tři	k4xCgInPc4	tři
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
neslo	nést	k5eAaImAgNnS	nést
první	první	k4xOgInPc4	první
meziválečné	meziválečný	k2eAgInPc4d1	meziválečný
roky	rok	k1gInPc4	rok
těžce	těžce	k6eAd1	těžce
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
ekonomika	ekonomika	k1gFnSc1	ekonomika
byla	být	k5eAaImAgFnS	být
těžce	těžce	k6eAd1	těžce
poničená	poničený	k2eAgFnSc1d1	poničená
přerušením	přerušení	k1gNnSc7	přerušení
staletých	staletý	k2eAgFnPc2d1	staletá
vazeb	vazba	k1gFnPc2	vazba
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgNnSc2	svůj
historického	historický	k2eAgNnSc2d1	historické
území	území	k1gNnSc2	území
muselo	muset	k5eAaImAgNnS	muset
odstoupit	odstoupit	k5eAaPmF	odstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
upínala	upínat	k5eAaImAgFnS	upínat
k	k	k7c3	k
fašistickým	fašistický	k2eAgFnPc3d1	fašistická
a	a	k8xC	a
revanšistickým	revanšistický	k2eAgFnPc3d1	revanšistická
snahám	snaha	k1gFnPc3	snaha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ministr	ministr	k1gMnSc1	ministr
Beneš	Beneš	k1gMnSc1	Beneš
orientoval	orientovat	k5eAaBmAgMnS	orientovat
politiku	politika	k1gFnSc4	politika
země	zem	k1gFnSc2	zem
na	na	k7c4	na
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
i	i	k9	i
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jiných	jiný	k2eAgFnPc6d1	jiná
ohledem	ohled	k1gInSc7	ohled
vzorem	vzor	k1gInSc7	vzor
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
udržovala	udržovat	k5eAaImAgFnS	udržovat
politiku	politika	k1gFnSc4	politika
izolace	izolace	k1gFnSc2	izolace
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
považovala	považovat	k5eAaImAgFnS	považovat
Československo	Československo	k1gNnSc4	Československo
za	za	k7c4	za
příliš	příliš	k6eAd1	příliš
vzdálenou	vzdálený	k2eAgFnSc4d1	vzdálená
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
se	se	k3xPyFc4	se
spolupráce	spolupráce	k1gFnSc1	spolupráce
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
mnohem	mnohem	k6eAd1	mnohem
otevřeněji	otevřeně	k6eAd2	otevřeně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
Československo	Československo	k1gNnSc1	Československo
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
spojeneckou	spojenecký	k2eAgFnSc7d1	spojenecká
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Locarnu	Locarno	k1gNnSc6	Locarno
podepsáno	podepsat	k5eAaPmNgNnS	podepsat
několik	několik	k4yIc1	několik
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
základě	základ	k1gInSc6	základ
bylo	být	k5eAaImAgNnS	být
Německo	Německo	k1gNnSc1	Německo
zrovnoprávněno	zrovnoprávněn	k2eAgNnSc1d1	zrovnoprávněno
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
evropskými	evropský	k2eAgInPc7d1	evropský
státy	stát	k1gInPc7	stát
z	z	k7c2	z
mnohých	mnohý	k2eAgNnPc2d1	mnohé
politických	politický	k2eAgNnPc2d1	politické
hledisek	hledisko	k1gNnPc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgFnPc2	tento
smluv	smlouva	k1gFnPc2	smlouva
bylo	být	k5eAaImAgNnS	být
Německo	Německo	k1gNnSc1	Německo
přijato	přijmout	k5eAaPmNgNnS	přijmout
do	do	k7c2	do
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
zaručila	zaručit	k5eAaPmAgFnS	zaručit
garantovat	garantovat	k5eAaBmF	garantovat
západní	západní	k2eAgFnSc1d1	západní
hranice	hranice	k1gFnSc1	hranice
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Belgií	Belgie	k1gFnSc7	Belgie
<g/>
,	,	kIx,	,
řešení	řešení	k1gNnSc1	řešení
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Československem	Československo	k1gNnSc7	Československo
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
se	se	k3xPyFc4	se
však	však	k9	však
odsunulo	odsunout	k5eAaPmAgNnS	odsunout
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
a	a	k8xC	a
řešit	řešit	k5eAaImF	řešit
to	ten	k3xDgNnSc4	ten
měla	mít	k5eAaImAgFnS	mít
případná	případný	k2eAgFnSc1d1	případná
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
arbitráž	arbitráž	k1gFnSc1	arbitráž
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
s	s	k7c7	s
Československem	Československo	k1gNnSc7	Československo
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
následně	následně	k6eAd1	následně
vojenské	vojenský	k2eAgFnSc2d1	vojenská
smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
případné	případný	k2eAgFnSc6d1	případná
pomoci	pomoc	k1gFnSc6	pomoc
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
budou	být	k5eAaImBp3nP	být
Německem	Německo	k1gNnSc7	Německo
napadeny	napaden	k2eAgFnPc4d1	napadena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
dostali	dostat	k5eAaPmAgMnP	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
nacisté	nacista	k1gMnPc1	nacista
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
sympatie	sympatie	k1gFnPc1	sympatie
začaly	začít	k5eAaPmAgFnP	začít
růst	růst	k5eAaImF	růst
i	i	k9	i
v	v	k7c6	v
pohraničních	pohraniční	k2eAgFnPc6d1	pohraniční
oblastech	oblast	k1gFnPc6	oblast
západní	západní	k2eAgFnSc2d1	západní
poloviny	polovina	k1gFnSc2	polovina
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
se	se	k3xPyFc4	se
tlak	tlak	k1gInSc1	tlak
na	na	k7c4	na
případnou	případný	k2eAgFnSc4d1	případná
úpravu	úprava	k1gFnSc4	úprava
hranic	hranice	k1gFnPc2	hranice
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
diplomacie	diplomacie	k1gFnSc1	diplomacie
proto	proto	k8xC	proto
začala	začít	k5eAaPmAgFnS	začít
hledat	hledat	k5eAaImF	hledat
nové	nový	k2eAgMnPc4d1	nový
spojence	spojenec	k1gMnPc4	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Beneš	Beneš	k1gMnSc1	Beneš
se	se	k3xPyFc4	se
obrátil	obrátit	k5eAaPmAgMnS	obrátit
k	k	k7c3	k
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
Československo-sovětskou	československoovětský	k2eAgFnSc4d1	československo-sovětská
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
pomoc	pomoc	k1gFnSc4	pomoc
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
přijde	přijít	k5eAaPmIp3nS	přijít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgInSc1	první
provede	provést	k5eAaPmIp3nS	provést
vojenský	vojenský	k2eAgInSc4d1	vojenský
zásah	zásah	k1gInSc4	zásah
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Československou	československý	k2eAgFnSc4d1	Československá
reprezentaci	reprezentace	k1gFnSc4	reprezentace
však	však	k9	však
ostře	ostro	k6eAd1	ostro
zchladily	zchladit	k5eAaPmAgFnP	zchladit
snahy	snaha	k1gFnPc4	snaha
západních	západní	k2eAgMnPc2d1	západní
spojenců	spojenec	k1gMnPc2	spojenec
řešit	řešit	k5eAaImF	řešit
stále	stále	k6eAd1	stále
akutnější	akutní	k2eAgInSc4d2	akutnější
problém	problém	k1gInSc4	problém
česko-německého	českoěmecký	k2eAgNnSc2d1	česko-německé
soužití	soužití	k1gNnSc2	soužití
v	v	k7c6	v
pohraničních	pohraniční	k2eAgInPc6d1	pohraniční
Sudetech	Sudety	k1gInPc6	Sudety
odstoupením	odstoupení	k1gNnSc7	odstoupení
problémových	problémový	k2eAgNnPc2d1	problémové
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgInPc1d1	západní
státy	stát	k1gInPc1	stát
měly	mít	k5eAaImAgInP	mít
z	z	k7c2	z
případné	případný	k2eAgFnSc2d1	případná
války	válka	k1gFnSc2	válka
obavy	obava	k1gFnSc2	obava
a	a	k8xC	a
chtěly	chtít	k5eAaImAgFnP	chtít
se	se	k3xPyFc4	se
případnému	případný	k2eAgInSc3d1	případný
konfliktu	konflikt	k1gInSc3	konflikt
vyhnout	vyhnout	k5eAaPmF	vyhnout
jakoukoliv	jakýkoliv	k3yIgFnSc7	jakýkoliv
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vláda	vláda	k1gFnSc1	vláda
===	===	k?	===
</s>
</p>
<p>
<s>
první	první	k4xOgMnSc1	první
československý	československý	k2eAgMnSc1d1	československý
prezident	prezident	k1gMnSc1	prezident
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1918	[number]	k4	1918
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
první	první	k4xOgMnSc1	první
premiér	premiér	k1gMnSc1	premiér
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
(	(	kIx(	(
<g/>
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
(	(	kIx(	(
<g/>
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
</s>
</p>
<p>
<s>
ministr	ministr	k1gMnSc1	ministr
vojenství	vojenství	k1gNnSc2	vojenství
Milan	Milan	k1gMnSc1	Milan
Rastislav	Rastislav	k1gMnSc1	Rastislav
Štefánik	Štefánik	k1gMnSc1	Štefánik
(	(	kIx(	(
<g/>
zahynul	zahynout	k5eAaPmAgInS	zahynout
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
druhý	druhý	k4xOgMnSc1	druhý
československý	československý	k2eAgMnSc1d1	československý
prezident	prezident	k1gMnSc1	prezident
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
(	(	kIx(	(
<g/>
zvolen	zvolen	k2eAgMnSc1d1	zvolen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Národnostní	národnostní	k2eAgNnSc4d1	národnostní
složení	složení	k1gNnSc4	složení
==	==	k?	==
</s>
</p>
<p>
<s>
Národnostní	národnostní	k2eAgNnSc1d1	národnostní
složení	složení	k1gNnSc1	složení
podle	podle	k7c2	podle
sčítaní	sčítaný	k2eAgMnPc1d1	sčítaný
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
Československo	Československo	k1gNnSc1	Československo
bylo	být	k5eAaImAgNnS	být
hospodářsky	hospodářsky	k6eAd1	hospodářsky
velmi	velmi	k6eAd1	velmi
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
vděčilo	vděčit	k5eAaImAgNnS	vděčit
zejména	zejména	k9	zejména
prudkému	prudký	k2eAgInSc3d1	prudký
rozvoji	rozvoj	k1gInSc3	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
nově	nově	k6eAd1	nově
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
národ	národ	k1gInSc1	národ
z	z	k7c2	z
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
čítal	čítat	k5eAaImAgInS	čítat
přibližně	přibližně	k6eAd1	přibližně
jen	jen	k9	jen
asi	asi	k9	asi
14,8	[number]	k4	14,8
milionů	milion	k4xCgInPc2	milion
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
převzalo	převzít	k5eAaPmAgNnS	převzít
asi	asi	k9	asi
70	[number]	k4	70
až	až	k9	až
80	[number]	k4	80
%	%	kIx~	%
celého	celý	k2eAgInSc2d1	celý
průmyslu	průmysl	k1gInSc2	průmysl
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2	Rakouska-Uhersko
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
výroby	výroba	k1gFnSc2	výroba
porcelánu	porcelán	k1gInSc2	porcelán
a	a	k8xC	a
sklářských	sklářský	k2eAgInPc2d1	sklářský
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
cukrovarů	cukrovar	k1gInPc2	cukrovar
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
40	[number]	k4	40
%	%	kIx~	%
lihovarů	lihovar	k1gInPc2	lihovar
a	a	k8xC	a
pivovarů	pivovar	k1gInPc2	pivovar
<g/>
,	,	kIx,	,
zbrojařských	zbrojařský	k2eAgFnPc2d1	zbrojařská
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
výroby	výroba	k1gFnSc2	výroba
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
<g/>
,	,	kIx,	,
automobilů	automobil	k1gInPc2	automobil
a	a	k8xC	a
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
chemický	chemický	k2eAgInSc4d1	chemický
průmysl	průmysl	k1gInSc4	průmysl
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
17	[number]	k4	17
%	%	kIx~	%
průmyslu	průmysl	k1gInSc2	průmysl
někdejšího	někdejší	k2eAgInSc2d1	někdejší
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
království	království	k1gNnSc1	království
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
vybudovalo	vybudovat	k5eAaPmAgNnS	vybudovat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
rovněž	rovněž	k6eAd1	rovněž
připadlo	připadnout	k5eAaPmAgNnS	připadnout
republice	republika	k1gFnSc3	republika
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
desátým	desátý	k4xOgNnSc7	desátý
průmyslově	průmyslově	k6eAd1	průmyslově
nejrozvinutějším	rozvinutý	k2eAgInSc7d3	nejrozvinutější
státem	stát	k1gInSc7	stát
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1920	[number]	k4	1920
až	až	k9	až
1935	[number]	k4	1935
dokonce	dokonce	k9	dokonce
9	[number]	k4	9
<g/>
.	.	kIx.	.
nejbohatším	bohatý	k2eAgInSc7d3	nejbohatší
státem	stát	k1gInSc7	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
byly	být	k5eAaImAgFnP	být
již	již	k9	již
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
rakousko-uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
monarchie	monarchie	k1gFnSc2	monarchie
daleko	daleko	k6eAd1	daleko
silněji	silně	k6eAd2	silně
industrializované	industrializovaný	k2eAgNnSc1d1	industrializované
nežli	nežli	k8xS	nežli
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
bylo	být	k5eAaImAgNnS	být
39	[number]	k4	39
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
31	[number]	k4	31
%	%	kIx~	%
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
lehkého	lehký	k2eAgInSc2d1	lehký
i	i	k8xC	i
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
bylo	být	k5eAaImAgNnS	být
soustředěna	soustředěn	k2eAgNnPc1d1	soustředěno
v	v	k7c6	v
pohraničí	pohraničí	k1gNnSc6	pohraničí
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Sudetech	Sudety	k1gInPc6	Sudety
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
podniků	podnik	k1gInPc2	podnik
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
německých	německý	k2eAgMnPc2d1	německý
občanů	občan	k1gMnPc2	občan
nebo	nebo	k8xC	nebo
bank	banka	k1gFnPc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
asi	asi	k9	asi
jen	jen	k9	jen
20	[number]	k4	20
až	až	k9	až
30	[number]	k4	30
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
výstupu	výstup	k1gInSc2	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
jen	jen	k6eAd1	jen
asi	asi	k9	asi
17,1	[number]	k4	17,1
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
60,4	[number]	k4	60,4
%	%	kIx~	%
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
či	či	k8xC	či
lesnictví	lesnictví	k1gNnSc6	lesnictví
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
asi	asi	k9	asi
5	[number]	k4	5
%	%	kIx~	%
celého	celý	k2eAgInSc2d1	celý
průmyslu	průmysl	k1gInSc2	průmysl
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
slovenských	slovenský	k2eAgFnPc6d1	slovenská
rukou	ruka	k1gFnPc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Podkarpatská	podkarpatský	k2eAgFnSc1d1	Podkarpatská
Rus	Rus	k1gFnSc1	Rus
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
bez	bez	k7c2	bez
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
příjmy	příjem	k1gInPc1	příjem
tvořily	tvořit	k5eAaImAgInP	tvořit
pouze	pouze	k6eAd1	pouze
turismus	turismus	k1gInSc4	turismus
a	a	k8xC	a
těžba	těžba	k1gFnSc1	těžba
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
samostatnosti	samostatnost	k1gFnSc2	samostatnost
zaveden	zaveden	k2eAgInSc1d1	zaveden
reformní	reformní	k2eAgInSc1d1	reformní
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
setřít	setřít	k5eAaPmF	setřít
nerovnosti	nerovnost	k1gFnPc4	nerovnost
rozdělení	rozdělení	k1gNnSc2	rozdělení
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1929	[number]	k4	1929
až	až	k9	až
1933	[number]	k4	1933
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
také	také	k9	také
Československo	Československo	k1gNnSc4	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
nezaměstnaných	nezaměstnaný	k1gMnPc2	nezaměstnaný
činil	činit	k5eAaImAgInS	činit
přibližně	přibližně	k6eAd1	přibližně
milión	milión	k4xCgInSc1	milión
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
následným	následný	k2eAgInSc7d1	následný
poklesem	pokles	k1gInSc7	pokles
pokles	pokles	k1gInSc1	pokles
výkonu	výkon	k1gInSc2	výkon
hospodářství	hospodářství	k1gNnSc2	hospodářství
o	o	k7c4	o
40,4	[number]	k4	40,4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Měna	měna	k1gFnSc1	měna
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
urychleně	urychleně	k6eAd1	urychleně
zavést	zavést	k5eAaPmF	zavést
novou	nový	k2eAgFnSc4d1	nová
měnu	měna	k1gFnSc4	měna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
zemi	zem	k1gFnSc3	zem
oddělila	oddělit	k5eAaPmAgFnS	oddělit
od	od	k7c2	od
měn	měna	k1gFnPc2	měna
okolních	okolní	k2eAgInPc2d1	okolní
států	stát	k1gInPc2	stát
zasažených	zasažený	k2eAgInPc2d1	zasažený
vysokou	vysoký	k2eAgFnSc7d1	vysoká
inflací	inflace	k1gFnSc7	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Dočasně	dočasně	k6eAd1	dočasně
ovšem	ovšem	k9	ovšem
na	na	k7c6	na
území	území	k1gNnSc6	území
Československa	Československo	k1gNnSc2	Československo
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
platily	platit	k5eAaImAgInP	platit
bankovky	bankovka	k1gFnPc4	bankovka
a	a	k8xC	a
mince	mince	k1gFnPc4	mince
rakousko-uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
měnová	měnový	k2eAgFnSc1d1	měnová
reforma	reforma	k1gFnSc1	reforma
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
Československá	československý	k2eAgFnSc1d1	Československá
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Kčs	Kčs	kA	Kčs
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
objevily	objevit	k5eAaPmAgInP	objevit
první	první	k4xOgFnPc4	první
bankovky	bankovka	k1gFnPc4	bankovka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
pak	pak	k6eAd1	pak
následovaly	následovat	k5eAaImAgInP	následovat
první	první	k4xOgFnPc4	první
vlastní	vlastní	k2eAgFnPc4d1	vlastní
mince	mince	k1gFnPc4	mince
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nahradily	nahradit	k5eAaPmAgFnP	nahradit
dosud	dosud	k6eAd1	dosud
platné	platný	k2eAgNnSc4d1	platné
rakousko-uherské	rakouskoherský	k2eAgNnSc4d1	rakousko-uherské
<g/>
.	.	kIx.	.
</s>
<s>
Staré	staré	k1gNnSc1	staré
zlaté	zlatý	k2eAgFnSc2d1	zlatá
a	a	k8xC	a
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
koruny	koruna	k1gFnSc2	koruna
prakticky	prakticky	k6eAd1	prakticky
zmizely	zmizet	k5eAaPmAgInP	zmizet
z	z	k7c2	z
oběhu	oběh	k1gInSc2	oběh
již	již	k6eAd1	již
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Korunová	korunový	k2eAgFnSc1d1	korunová
měna	měna	k1gFnSc1	měna
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
prošla	projít	k5eAaPmAgFnS	projít
ještě	ještě	k6eAd1	ještě
několika	několik	k4yIc7	několik
reformami	reforma	k1gFnPc7	reforma
a	a	k8xC	a
změnami	změna	k1gFnPc7	změna
<g/>
.	.	kIx.	.
</s>
<s>
Zákonem	zákon	k1gInSc7	zákon
ze	z	k7c2	z
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1929	[number]	k4	1929
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
stanovena	stanovit	k5eAaPmNgFnS	stanovit
pro	pro	k7c4	pro
obchodní	obchodní	k2eAgInSc4d1	obchodní
styk	styk	k1gInSc4	styk
zlatá	zlatý	k2eAgFnSc1d1	zlatá
parita	parita	k1gFnSc1	parita
1	[number]	k4	1
koruny	koruna	k1gFnPc4	koruna
na	na	k7c4	na
44,85	[number]	k4	44,85
mg	mg	kA	mg
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
měna	měna	k1gFnSc1	měna
krytá	krytý	k2eAgFnSc1d1	krytá
zlatem	zlato	k1gNnSc7	zlato
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
době	doba	k1gFnSc6	doba
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1923	[number]	k4	1923
až	až	k8xS	až
1929	[number]	k4	1929
měla	mít	k5eAaImAgFnS	mít
koruna	koruna	k1gFnSc1	koruna
relativně	relativně	k6eAd1	relativně
stabilní	stabilní	k2eAgFnSc4d1	stabilní
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
se	se	k3xPyFc4	se
průměrně	průměrně	k6eAd1	průměrně
mezi	mezi	k7c7	mezi
15,36	[number]	k4	15,36
až	až	k9	až
16,37	[number]	k4	16,37
švýcarských	švýcarský	k2eAgInPc2d1	švýcarský
franků	frank	k1gInPc2	frank
na	na	k7c4	na
100	[number]	k4	100
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Směnný	směnný	k2eAgInSc1d1	směnný
kurz	kurz	k1gInSc1	kurz
vůči	vůči	k7c3	vůči
říšské	říšský	k2eAgFnSc3d1	říšská
marce	marka	k1gFnSc3	marka
činil	činit	k5eAaImAgInS	činit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
0,85	[number]	k4	0,85
koruny	koruna	k1gFnPc1	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zemědělství	zemědělství	k1gNnSc1	zemědělství
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
pracovalo	pracovat	k5eAaImAgNnS	pracovat
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
35	[number]	k4	35
%	%	kIx~	%
čsl	čsl	kA	čsl
<g/>
.	.	kIx.	.
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
silné	silný	k2eAgFnSc3d1	silná
industrializaci	industrializace	k1gFnSc3	industrializace
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
přes	přes	k7c4	přes
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
několik	několik	k4yIc1	několik
vln	vlna	k1gFnPc2	vlna
znárodňování	znárodňování	k1gNnSc2	znárodňování
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
závodů	závod	k1gInPc2	závod
a	a	k8xC	a
selských	selský	k2eAgInPc2d1	selský
dvorů	dvůr	k1gInPc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc1	zdroj
obživy	obživa	k1gFnSc2	obživa
převládalo	převládat	k5eAaImAgNnS	převládat
zejména	zejména	k9	zejména
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
Podkarpatské	podkarpatský	k2eAgFnSc6d1	Podkarpatská
Rusi	Rus	k1gFnSc6	Rus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průmysl	průmysl	k1gInSc1	průmysl
===	===	k?	===
</s>
</p>
<p>
<s>
Československý	československý	k2eAgInSc1d1	československý
průmysl	průmysl	k1gInSc1	průmysl
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Modernější	moderní	k2eAgFnSc1d2	modernější
rukodělná	rukodělný	k2eAgFnSc1d1	rukodělná
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
strojírenství	strojírenství	k1gNnSc2	strojírenství
a	a	k8xC	a
nové	nový	k2eAgFnSc2d1	nová
technologie	technologie	k1gFnSc2	technologie
postupně	postupně	k6eAd1	postupně
nahradily	nahradit	k5eAaPmAgInP	nahradit
již	již	k6eAd1	již
neefektivní	efektivní	k2eNgInPc1d1	neefektivní
starší	starý	k2eAgInPc1d2	starší
systémy	systém	k1gInPc1	systém
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýraznějších	výrazný	k2eAgFnPc2d3	nejvýraznější
postav	postava	k1gFnPc2	postava
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
hospodářství	hospodářství	k1gNnSc2	hospodářství
byl	být	k5eAaImAgMnS	být
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
(	(	kIx(	(
<g/>
Baťovy	Baťův	k2eAgInPc1d1	Baťův
závody	závod	k1gInPc1	závod
zal	zal	k?	zal
<g/>
.	.	kIx.	.
1894	[number]	k4	1894
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
již	jenž	k3xRgFnSc4	jenž
době	doba	k1gFnSc6	doba
císařství	císařství	k1gNnSc2	císařství
dával	dávat	k5eAaImAgMnS	dávat
práci	práce	k1gFnSc4	práce
tisícům	tisíc	k4xCgInPc3	tisíc
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
když	když	k8xS	když
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
obouval	obouvat	k5eAaImAgMnS	obouvat
armádu	armáda	k1gFnSc4	armáda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Závody	závod	k1gInPc1	závod
textilního	textilní	k2eAgInSc2d1	textilní
<g/>
,	,	kIx,	,
sklářského	sklářský	k2eAgInSc2d1	sklářský
a	a	k8xC	a
obuvnického	obuvnický	k2eAgInSc2d1	obuvnický
průmyslu	průmysl	k1gInSc2	průmysl
byly	být	k5eAaImAgInP	být
tehdy	tehdy	k6eAd1	tehdy
nejmodernější	moderní	k2eAgFnSc4d3	nejmodernější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
také	také	k9	také
zbrojní	zbrojní	k2eAgInSc1d1	zbrojní
průmysl	průmysl	k1gInSc1	průmysl
(	(	kIx(	(
<g/>
Škodovy	Škodův	k2eAgInPc1d1	Škodův
závody	závod	k1gInPc1	závod
zal	zal	k?	zal
<g/>
.	.	kIx.	.
1866	[number]	k4	1866
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
elektrifikace	elektrifikace	k1gFnSc2	elektrifikace
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
prudce	prudko	k6eAd1	prudko
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
zásobování	zásobování	k1gNnSc1	zásobování
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
energií	energie	k1gFnSc7	energie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
jen	jen	k9	jen
38	[number]	k4	38
000	[number]	k4	000
nezaměstnaných	nezaměstnaný	k1gMnPc2	nezaměstnaný
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představovalo	představovat	k5eAaImAgNnS	představovat
asi	asi	k9	asi
1	[number]	k4	1
%	%	kIx~	%
práceschopného	práceschopný	k2eAgNnSc2d1	práceschopné
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
do	do	k7c2	do
objemu	objem	k1gInSc2	objem
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
produkce	produkce	k1gFnSc2	produkce
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
republika	republika	k1gFnSc1	republika
na	na	k7c6	na
desátém	desátý	k4xOgNnSc6	desátý
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Turismus	turismus	k1gInSc4	turismus
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
patřila	patřit	k5eAaImAgFnS	patřit
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
mezi	mezi	k7c4	mezi
oblíbené	oblíbený	k2eAgInPc4d1	oblíbený
turistické	turistický	k2eAgInPc4d1	turistický
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
turistů	turist	k1gMnPc2	turist
trávila	trávit	k5eAaImAgFnS	trávit
dovolenou	dovolená	k1gFnSc4	dovolená
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
nejnavštěvovanější	navštěvovaný	k2eAgNnSc1d3	nejnavštěvovanější
částí	část	k1gFnSc7	část
země	zem	k1gFnSc2	zem
byly	být	k5eAaImAgFnP	být
historické	historický	k2eAgFnPc1d1	historická
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
symboly	symbol	k1gInPc4	symbol
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
nebo	nebo	k8xC	nebo
Staroměstské	staroměstský	k2eAgNnSc4d1	Staroměstské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
rovněž	rovněž	k9	rovněž
oblíbenými	oblíbený	k2eAgNnPc7d1	oblíbené
cíli	cíl	k1gInSc6	cíl
mnoha	mnoho	k4c2	mnoho
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
metropoli	metropole	k1gFnSc4	metropole
ročně	ročně	k6eAd1	ročně
až	až	k6eAd1	až
osm	osm	k4xCc1	osm
milionů	milion	k4xCgInPc2	milion
turistů	turist	k1gMnPc2	turist
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
pro	pro	k7c4	pro
státní	státní	k2eAgFnSc4d1	státní
pokladnu	pokladna	k1gFnSc4	pokladna
znamenalo	znamenat	k5eAaImAgNnS	znamenat
přínos	přínos	k1gInSc4	přínos
asi	asi	k9	asi
900	[number]	k4	900
milionů	milion	k4xCgInPc2	milion
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
===	===	k?	===
</s>
</p>
<p>
<s>
Infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
v	v	k7c6	v
předlitavské	předlitavský	k2eAgFnSc6d1	předlitavská
i	i	k8xC	i
zalitavské	zalitavský	k2eAgFnSc6d1	zalitavský
části	část	k1gFnSc6	část
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2	Rakouska-Uhersko
nebyla	být	k5eNaImAgFnS	být
dosti	dosti	k6eAd1	dosti
dobře	dobře	k6eAd1	dobře
použitelná	použitelný	k2eAgFnSc1d1	použitelná
pro	pro	k7c4	pro
samostatné	samostatný	k2eAgNnSc4d1	samostatné
Československo	Československo	k1gNnSc4	Československo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byla	být	k5eAaImAgFnS	být
koncipována	koncipovat	k5eAaBmNgFnS	koncipovat
přdevším	přdevší	k2eAgInSc7d1	přdevší
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
rakousko-uherského	rakouskoherský	k2eAgNnSc2d1	rakousko-uherské
soustátí	soustátí	k1gNnSc2	soustátí
jako	jako	k8xC	jako
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
zcela	zcela	k6eAd1	zcela
chybělo	chybět	k5eAaImAgNnS	chybět
přímé	přímý	k2eAgNnSc1d1	přímé
železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
Čech	Čechy	k1gFnPc2	Čechy
s	s	k7c7	s
Podkarpatskou	podkarpatský	k2eAgFnSc7d1	Podkarpatská
Rusí	Rus	k1gFnSc7	Rus
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
území	území	k1gNnSc1	území
před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
samostatných	samostatný	k2eAgInPc2d1	samostatný
následnických	následnický	k2eAgInPc2d1	následnický
států	stát	k1gInPc2	stát
spadala	spadat	k5eAaImAgFnS	spadat
pod	pod	k7c4	pod
uherskou	uherský	k2eAgFnSc4d1	uherská
správu	správa	k1gFnSc4	správa
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
spojení	spojení	k1gNnSc4	spojení
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
změnila	změnit	k5eAaPmAgFnS	změnit
teprve	teprve	k6eAd1	teprve
výstavba	výstavba	k1gFnSc1	výstavba
nových	nový	k2eAgInPc2d1	nový
železničních	železniční	k2eAgInPc2d1	železniční
úseků	úsek	k1gInPc2	úsek
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
byly	být	k5eAaImAgInP	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
dva	dva	k4xCgInPc1	dva
hlavní	hlavní	k2eAgInPc1d1	hlavní
přes	přes	k7c4	přes
1	[number]	k4	1
800	[number]	k4	800
km	km	kA	km
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
koridory	koridor	k1gInPc4	koridor
na	na	k7c4	na
až	až	k9	až
doposud	doposud	k6eAd1	doposud
izolované	izolovaný	k2eAgNnSc4d1	izolované
Zakarpatí	Zakarpatí	k1gNnSc4	Zakarpatí
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pak	pak	k6eAd1	pak
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
zbytek	zbytek	k1gInSc4	zbytek
země	zem	k1gFnSc2	zem
zásobován	zásobovat	k5eAaImNgMnS	zásobovat
dřevem	dřevo	k1gNnSc7	dřevo
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
surovinami	surovina	k1gFnPc7	surovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Světová	světový	k2eAgFnSc1d1	světová
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
===	===	k?	===
</s>
</p>
<p>
<s>
Nezávislá	závislý	k2eNgFnSc1d1	nezávislá
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
byla	být	k5eAaImAgFnS	být
hospodářsky	hospodářsky	k6eAd1	hospodářsky
nejsilnějším	silný	k2eAgInSc7d3	nejsilnější
následnickým	následnický	k2eAgInSc7d1	následnický
státem	stát	k1gInSc7	stát
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
vysokým	vysoký	k2eAgInSc7d1	vysoký
stupněm	stupeň	k1gInSc7	stupeň
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
rozvoje	rozvoj	k1gInSc2	rozvoj
země	zem	k1gFnSc2	zem
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
Zakarpatí	Zakarpatí	k1gNnSc4	Zakarpatí
byla	být	k5eAaImAgFnS	být
ekonomika	ekonomika	k1gFnSc1	ekonomika
výrazně	výrazně	k6eAd1	výrazně
méně	málo	k6eAd2	málo
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
znovu	znovu	k6eAd1	znovu
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
stavu	stav	k1gInSc2	stav
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
ji	on	k3xPp3gFnSc4	on
dokonce	dokonce	k9	dokonce
o	o	k7c4	o
41	[number]	k4	41
%	%	kIx~	%
překonala	překonat	k5eAaPmAgFnS	překonat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Světová	světový	k2eAgFnSc1d1	světová
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
však	však	k9	však
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
také	také	k9	také
Československo	Československo	k1gNnSc4	Československo
<g/>
,	,	kIx,	,
a	a	k8xC	a
výkon	výkon	k1gInSc1	výkon
hospodářství	hospodářství	k1gNnSc2	hospodářství
se	se	k3xPyFc4	se
silně	silně	k6eAd1	silně
propadl	propadnout	k5eAaPmAgInS	propadnout
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
podniků	podnik	k1gInPc2	podnik
se	se	k3xPyFc4	se
ocitlo	ocitnout	k5eAaPmAgNnS	ocitnout
v	v	k7c6	v
úpadku	úpadek	k1gInSc6	úpadek
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc1	množství
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
řemeslníků	řemeslník	k1gMnPc2	řemeslník
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
docházelo	docházet	k5eAaImAgNnS	docházet
až	až	k9	až
k	k	k7c3	k
podvýživě	podvýživa	k1gFnSc3	podvýživa
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
začala	začít	k5eAaPmAgFnS	začít
krize	krize	k1gFnSc1	krize
opět	opět	k6eAd1	opět
sílit	sílit	k5eAaImF	sílit
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
svého	své	k1gNnSc2	své
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
1,3	[number]	k4	1,3
milionů	milion	k4xCgInPc2	milion
nezaměstnaných	nezaměstnaný	k1gMnPc2	nezaměstnaný
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
ani	ani	k8xC	ani
po	po	k7c6	po
odeznění	odeznění	k1gNnSc6	odeznění
krize	krize	k1gFnSc2	krize
nedošlo	dojít	k5eNaPmAgNnS	dojít
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
k	k	k7c3	k
většímu	veliký	k2eAgNnSc3d2	veliký
obnovení	obnovení	k1gNnSc3	obnovení
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
ještě	ještě	k6eAd1	ještě
zesílil	zesílit	k5eAaPmAgInS	zesílit
<g/>
.	.	kIx.	.
</s>
<s>
Motorem	motor	k1gInSc7	motor
ekonomiky	ekonomika	k1gFnPc1	ekonomika
byly	být	k5eAaImAgFnP	být
především	především	k9	především
chemický	chemický	k2eAgInSc4d1	chemický
<g/>
,	,	kIx,	,
hutnický	hutnický	k2eAgInSc4d1	hutnický
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInSc4d1	textilní
a	a	k8xC	a
papírenský	papírenský	k2eAgInSc4d1	papírenský
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
znovu	znovu	k6eAd1	znovu
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
předkrizové	předkrizový	k2eAgFnPc4d1	předkrizová
úrovně	úroveň	k1gFnPc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
ekonomika	ekonomika	k1gFnSc1	ekonomika
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
o	o	k7c4	o
38	[number]	k4	38
%	%	kIx~	%
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
znovu	znovu	k6eAd1	znovu
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
původní	původní	k2eAgFnSc2d1	původní
kondice	kondice	k1gFnSc2	kondice
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgInPc1d1	Malé
podniky	podnik	k1gInPc1	podnik
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
během	během	k7c2	během
krize	krize	k1gFnSc2	krize
vyhlásily	vyhlásit	k5eAaPmAgFnP	vyhlásit
úpadek	úpadek	k1gInSc4	úpadek
<g/>
,	,	kIx,	,
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
znovu	znovu	k6eAd1	znovu
vybudovány	vybudován	k2eAgFnPc4d1	vybudována
a	a	k8xC	a
obnovit	obnovit	k5eAaPmF	obnovit
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
pomalého	pomalý	k2eAgInSc2d1	pomalý
vzestupu	vzestup	k1gInSc2	vzestup
ekonomiky	ekonomika	k1gFnSc2	ekonomika
během	během	k7c2	během
krize	krize	k1gFnSc2	krize
byly	být	k5eAaImAgInP	být
zhoršené	zhoršený	k2eAgInPc1d1	zhoršený
zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
vztahy	vztah	k1gInPc1	vztah
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterým	který	k3yQgInPc3	který
nepřicházela	přicházet	k5eNaImAgFnS	přicházet
žádná	žádný	k3yNgFnSc1	žádný
vnější	vnější	k2eAgFnSc1d1	vnější
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
podpora	podpora	k1gFnSc1	podpora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dluh	dluh	k1gInSc4	dluh
===	===	k?	===
</s>
</p>
<p>
<s>
Československo	Československo	k1gNnSc1	Československo
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc4	Lichtenštejnsko
<g/>
)	)	kIx)	)
v	v	k7c6	v
meziválečné	meziválečný	k2eAgFnSc6d1	meziválečná
době	doba	k1gFnSc6	doba
nemělo	mít	k5eNaImAgNnS	mít
státní	státní	k2eAgInSc4d1	státní
dluh	dluh	k1gInSc4	dluh
<g/>
,	,	kIx,	,
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
si	se	k3xPyFc3	se
dovolit	dovolit	k5eAaPmF	dovolit
půjčovat	půjčovat	k5eAaImF	půjčovat
značné	značný	k2eAgFnPc4d1	značná
sumy	suma	k1gFnPc4	suma
jiným	jiný	k2eAgInPc3d1	jiný
státům	stát	k1gInPc3	stát
(	(	kIx(	(
<g/>
Jugoslávské	jugoslávský	k2eAgNnSc1d1	jugoslávské
království	království	k1gNnSc1	království
a	a	k8xC	a
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
Československo	Československo	k1gNnSc1	Československo
silně	silně	k6eAd1	silně
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
odkázáno	odkázat	k5eAaPmNgNnS	odkázat
na	na	k7c4	na
půjčky	půjčka	k1gFnPc4	půjčka
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
začalo	začít	k5eAaPmAgNnS	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
tlak	tlak	k1gInSc4	tlak
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
na	na	k7c4	na
Rumunsko	Rumunsko	k1gNnSc4	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
však	však	k9	však
dlužníci	dlužník	k1gMnPc1	dlužník
nemohli	moct	k5eNaImAgMnP	moct
půjčky	půjčka	k1gFnPc4	půjčka
splatit	splatit	k5eAaPmF	splatit
<g/>
,	,	kIx,	,
zavedlo	zavést	k5eAaPmAgNnS	zavést
Československo	Československo	k1gNnSc1	Československo
obchodní	obchodní	k2eAgFnSc2d1	obchodní
sankce	sankce	k1gFnSc2	sankce
a	a	k8xC	a
silně	silně	k6eAd1	silně
zredukovalo	zredukovat	k5eAaPmAgNnS	zredukovat
vývoz	vývoz	k1gInSc4	vývoz
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zhoršilo	zhoršit	k5eAaPmAgNnS	zhoršit
zahraničněpolitické	zahraničněpolitický	k2eAgInPc4d1	zahraničněpolitický
vztahy	vztah	k1gInPc4	vztah
a	a	k8xC	a
následně	následně	k6eAd1	následně
nedošlo	dojít	k5eNaPmAgNnS	dojít
ani	ani	k8xC	ani
k	k	k7c3	k
očekávanému	očekávaný	k2eAgInSc3d1	očekávaný
zásahu	zásah	k1gInSc3	zásah
někdejších	někdejší	k2eAgMnPc2d1	někdejší
spojenců	spojenec	k1gMnPc2	spojenec
v	v	k7c6	v
první	první	k4xOgFnSc6	první
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
arbitráži	arbitráž	k1gFnSc6	arbitráž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1929-1933	[number]	k4	1929-1933
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Československo	Československo	k1gNnSc4	Československo
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
velká	velký	k2eAgFnSc1d1	velká
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
<g/>
.	.	kIx.	.
</s>
<s>
Krizí	krize	k1gFnSc7	krize
byly	být	k5eAaImAgFnP	být
nejvíce	hodně	k6eAd3	hodně
postiženy	postihnout	k5eAaPmNgFnP	postihnout
Sudety	Sudety	k1gFnPc1	Sudety
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Školství	školství	k1gNnSc2	školství
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnPc1d1	základní
školy	škola	k1gFnPc1	škola
představovaly	představovat	k5eAaImAgFnP	představovat
především	především	k9	především
školy	škola	k1gFnPc1	škola
obecné	obecný	k2eAgFnPc1d1	obecná
a	a	k8xC	a
měšťanské	měšťanský	k2eAgFnPc1d1	měšťanská
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgInSc1d1	střední
pak	pak	k6eAd1	pak
gymnázia	gymnázium	k1gNnSc2	gymnázium
(	(	kIx(	(
<g/>
klasická	klasický	k2eAgFnSc1d1	klasická
a	a	k8xC	a
reálná	reálný	k2eAgFnSc1d1	reálná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
reálky	reálka	k1gFnSc2	reálka
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgFnSc2d1	obchodní
akademie	akademie	k1gFnSc2	akademie
<g/>
,	,	kIx,	,
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
(	(	kIx(	(
<g/>
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
<g/>
)	)	kIx)	)
a	a	k8xC	a
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
mezi	mezi	k7c7	mezi
školami	škola	k1gFnPc7	škola
středními	střední	k2eAgFnPc7d1	střední
a	a	k8xC	a
vysokými	vysoký	k2eAgFnPc7d1	vysoká
stály	stát	k5eAaImAgFnP	stát
různorodé	různorodý	k2eAgFnPc1d1	různorodá
vyšší	vysoký	k2eAgFnPc1d2	vyšší
odborné	odborný	k2eAgFnPc1d1	odborná
školy	škola	k1gFnPc1	škola
a	a	k8xC	a
učiliště	učiliště	k1gNnPc1	učiliště
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
např.	např.	kA	např.
o	o	k7c4	o
Státní	státní	k2eAgFnSc4d1	státní
školu	škola	k1gFnSc4	škola
umělecko-průmyslovou	uměleckorůmyslový	k2eAgFnSc4d1	umělecko-průmyslová
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnPc4d1	státní
konzervatoře	konzervatoř	k1gFnPc4	konzervatoř
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
německá	německý	k2eAgFnSc1d1	německá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
pedagogické	pedagogický	k2eAgFnPc1d1	pedagogická
akademie	akademie	k1gFnPc1	akademie
také	také	k9	také
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
německá	německý	k2eAgFnSc1d1	německá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
Vyšší	vysoký	k2eAgFnSc4d2	vyšší
školu	škola	k1gFnSc4	škola
sociální	sociální	k2eAgFnSc2d1	sociální
péče	péče	k1gFnSc2	péče
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Státní	státní	k2eAgFnSc4d1	státní
archivní	archivní	k2eAgFnSc4d1	archivní
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
nebo	nebo	k8xC	nebo
Státní	státní	k2eAgInPc1d1	státní
školu	škola	k1gFnSc4	škola
knihovnickou	knihovnický	k2eAgFnSc4d1	knihovnická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zařadit	zařadit	k5eAaPmF	zařadit
sem	sem	k6eAd1	sem
lze	lze	k6eAd1	lze
i	i	k8xC	i
bohoslovecké	bohoslovecký	k2eAgInPc1d1	bohoslovecký
semináře	seminář	k1gInPc1	seminář
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
Litoměřice	Litoměřice	k1gInPc1	Litoměřice
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Vidnava	Vidnava	k1gFnSc1	Vidnava
<g/>
,	,	kIx,	,
Trnava	Trnava	k1gFnSc1	Trnava
<g/>
,	,	kIx,	,
Nitra	Nitra	k1gFnSc1	Nitra
<g/>
,	,	kIx,	,
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
,	,	kIx,	,
Spiš	Spiš	k1gFnSc1	Spiš
<g/>
,	,	kIx,	,
Rožňava	Rožňava	k1gFnSc1	Rožňava
<g/>
,	,	kIx,	,
Košice	Košice	k1gInPc1	Košice
<g/>
,	,	kIx,	,
Prešov	Prešov	k1gInSc1	Prešov
<g/>
,	,	kIx,	,
Užhorod	Užhorod	k1gInSc1	Užhorod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Území	území	k1gNnSc2	území
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
pod	pod	k7c7	pod
ČSR	ČSR	kA	ČSR
patřila	patřit	k5eAaImAgFnS	patřit
následující	následující	k2eAgNnSc4d1	následující
území	území	k1gNnSc4	území
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Čechy	Čechy	k1gFnPc1	Čechy
(	(	kIx(	(
<g/>
Země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Morava	Morava	k1gFnSc1	Morava
(	(	kIx(	(
<g/>
Země	země	k1gFnSc1	země
moravská	moravský	k2eAgFnSc1d1	Moravská
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
Slezsko	Slezsko	k1gNnSc1	Slezsko
(	(	kIx(	(
<g/>
Země	země	k1gFnSc1	země
slezská	slezský	k2eAgFnSc1d1	Slezská
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
(	(	kIx(	(
<g/>
Země	země	k1gFnSc1	země
slovenská	slovenský	k2eAgFnSc1d1	slovenská
<g/>
/	/	kIx~	/
<g/>
Krajina	Krajina	k1gFnSc1	Krajina
slovenská	slovenský	k2eAgFnSc1d1	slovenská
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Podkarpatská	podkarpatský	k2eAgFnSc1d1	Podkarpatská
Rus	Rus	k1gFnSc1	Rus
(	(	kIx(	(
<g/>
Země	země	k1gFnSc1	země
podkarpatoruská	podkarpatoruský	k2eAgFnSc1d1	Podkarpatoruská
<g/>
;	;	kIx,	;
dnes	dnes	k6eAd1	dnes
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
Zakarpatská	zakarpatský	k2eAgFnSc1d1	Zakarpatská
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
Právě	právě	k9	právě
Podkarpatská	podkarpatský	k2eAgFnSc1d1	Podkarpatská
Rus	Rus	k1gFnSc1	Rus
(	(	kIx(	(
<g/>
východní	východní	k2eAgMnSc1d1	východní
soused	soused	k1gMnSc1	soused
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
odtržena	odtrhnout	k5eAaPmNgFnS	odtrhnout
od	od	k7c2	od
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gNnSc1	její
území	území	k1gNnSc1	území
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
vojsky	vojsky	k6eAd1	vojsky
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
pak	pak	k6eAd1	pak
na	na	k7c4	na
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
opět	opět	k6eAd1	opět
formálně	formálně	k6eAd1	formálně
náležela	náležet	k5eAaImAgFnS	náležet
k	k	k7c3	k
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
slovenské	slovenský	k2eAgFnSc2d1	slovenská
<g/>
)	)	kIx)	)
obce	obec	k1gFnSc2	obec
Lekárovce	Lekárovec	k1gMnSc2	Lekárovec
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
pruhem	pruh	k1gInSc7	pruh
území	území	k1gNnSc2	území
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
východního	východní	k2eAgNnSc2d1	východní
Slovenska	Slovensko	k1gNnSc2	Slovensko
"	"	kIx"	"
<g/>
dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
velmi	velmi	k6eAd1	velmi
problematického	problematický	k2eAgNnSc2d1	problematické
hlasování	hlasování	k1gNnSc2	hlasování
prosovětské	prosovětský	k2eAgFnSc2d1	prosovětská
skupiny	skupina	k1gFnSc2	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
odstoupena	odstoupen	k2eAgFnSc1d1	odstoupen
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1945	[number]	k4	1945
československou	československý	k2eAgFnSc7d1	Československá
vládou	vláda	k1gFnSc7	vláda
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ji	on	k3xPp3gFnSc4	on
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1946	[number]	k4	1946
začlenil	začlenit	k5eAaPmAgInS	začlenit
jako	jako	k9	jako
Zakarpatskou	zakarpatský	k2eAgFnSc4d1	Zakarpatská
oblast	oblast	k1gFnSc4	oblast
do	do	k7c2	do
rámce	rámec	k1gInSc2	rámec
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
SSR	SSR	kA	SSR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Urbanizovány	urbanizován	k2eAgFnPc1d1	urbanizována
byly	být	k5eAaImAgFnP	být
především	především	k9	především
české	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Československa	Československo	k1gNnSc2	Československo
byly	být	k5eAaImAgInP	být
deseti	deset	k4xCc7	deset
největšími	veliký	k2eAgNnPc7d3	veliký
městy	město	k1gNnPc7	město
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1930	[number]	k4	1930
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
849	[number]	k4	849
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
265	[number]	k4	265
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moravská	moravský	k2eAgFnSc1d1	Moravská
Ostrava	Ostrava	k1gFnSc1	Ostrava
(	(	kIx(	(
<g/>
125	[number]	k4	125
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
(	(	kIx(	(
<g/>
124	[number]	k4	124
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
(	(	kIx(	(
<g/>
115	[number]	k4	115
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
(	(	kIx(	(
<g/>
66	[number]	k4	66
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Košice	Košice	k1gInPc1	Košice
(	(	kIx(	(
<g/>
58	[number]	k4	58
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
44	[number]	k4	44
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
(	(	kIx(	(
<g/>
44	[number]	k4	44
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pardubice	Pardubice	k1gInPc1	Pardubice
(	(	kIx(	(
<g/>
41	[number]	k4	41
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
CESAR	CESAR	kA	CESAR
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Revoluční	revoluční	k2eAgNnSc1d1	revoluční
hnutí	hnutí	k1gNnSc1	hnutí
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
československých	československý	k2eAgFnPc2d1	Československá
a	a	k8xC	a
světových	světový	k2eAgFnPc2d1	světová
dějin	dějiny	k1gFnPc2	dějiny
ČSAV	ČSAV	kA	ČSAV
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
468	[number]	k4	468
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Houska	houska	k1gFnSc1	houska
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
proti	proti	k7c3	proti
Římu	Řím	k1gInSc3	Řím
–	–	k?	–
Československo-italské	československotalský	k2eAgInPc4d1	československo-italský
vztahy	vztah	k1gInPc4	vztah
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
–	–	k?	–
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7308-340-3	[number]	k4	978-80-7308-340-3
</s>
</p>
<p>
<s>
JANČÍK	Jančík	k1gMnSc1	Jančík
<g/>
,	,	kIx,	,
Drahomír	Drahomír	k1gMnSc1	Drahomír
<g/>
.	.	kIx.	.
</s>
<s>
Wirtschaftsdiplomatie	Wirtschaftsdiplomatie	k1gFnPc4	Wirtschaftsdiplomatie
des	des	k1gNnSc2	des
Deutschen	Deutschen	k2eAgMnSc1d1	Deutschen
Reiches	Reiches	k1gMnSc1	Reiches
und	und	k?	und
der	drát	k5eAaImRp2nS	drát
tschechoslowakische	tschechoslowakische	k1gNnSc4	tschechoslowakische
Wirtschaftsregionalismus	Wirtschaftsregionalismus	k1gInSc1	Wirtschaftsregionalismus
im	im	k?	im
Kampf	Kampf	k1gInSc1	Kampf
um	um	k1gInSc1	um
Mitteleuropa	Mitteleuropa	k1gFnSc1	Mitteleuropa
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prague	Prague	k1gFnSc1	Prague
Papers	Papersa	k1gFnPc2	Papersa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
History	Histor	k1gInPc4	Histor
of	of	k?	of
International	International	k1gFnSc2	International
Relations	Relationsa	k1gFnPc2	Relationsa
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
,	,	kIx,	,
s.	s.	k?	s.
293	[number]	k4	293
<g/>
-	-	kIx~	-
<g/>
341	[number]	k4	341
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
[	[	kIx(	[
<g/>
PDF	PDF	kA	PDF
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7308	[number]	k4	7308
<g/>
-	-	kIx~	-
<g/>
208	[number]	k4	208
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KÁRNÍK	Kárník	k1gMnSc1	Kárník
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
První	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
první	první	k4xOgInSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
<g/>
,	,	kIx,	,
budování	budování	k1gNnSc4	budování
a	a	k8xC	a
zlatá	zlatý	k2eAgNnPc4d1	Zlaté
léta	léto	k1gNnPc4	léto
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
571	[number]	k4	571
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
27	[number]	k4	27
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KÁRNÍK	Kárník	k1gMnSc1	Kárník
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
První	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
druhý	druhý	k4xOgInSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
a	a	k8xC	a
české	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
v	v	k7c6	v
krizi	krize	k1gFnSc6	krize
a	a	k8xC	a
v	v	k7c6	v
ohrožení	ohrožení	k1gNnSc6	ohrožení
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
-	-	kIx~	-
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
577	[number]	k4	577
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
27	[number]	k4	27
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KÁRNÍK	Kárník	k1gMnSc1	Kárník
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
První	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
třetí	třetí	k4xOgInSc1	třetí
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
přežití	přežití	k1gNnSc4	přežití
a	a	k8xC	a
o	o	k7c4	o
život	život	k1gInSc4	život
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
803	[number]	k4	803
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
119	[number]	k4	119
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KÁRNÍK	Kárník	k1gMnSc1	Kárník
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
dějiny	dějiny	k1gFnPc1	dějiny
československé	československý	k2eAgFnSc2d1	Československá
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Dokořán	dokořán	k6eAd1	dokořán
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
502	[number]	k4	502
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7363	[number]	k4	7363
<g/>
-	-	kIx~	-
<g/>
146	[number]	k4	146
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KLIMEK	Klimek	k1gMnSc1	Klimek
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
Hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Vnitropolitický	vnitropolitický	k2eAgInSc4d1	vnitropolitický
vývoj	vývoj	k1gInSc4	vývoj
Československa	Československo	k1gNnSc2	Československo
1918-1926	[number]	k4	1918-1926
na	na	k7c6	na
půdorysu	půdorys	k1gInSc6	půdorys
zápasu	zápas	k1gInSc2	zápas
o	o	k7c4	o
prezidentské	prezidentský	k2eAgNnSc4d1	prezidentské
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
a	a	k8xC	a
Pětka	pětka	k1gFnSc1	pětka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panevropa	Panevropa	k1gFnSc1	Panevropa
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
432	[number]	k4	432
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85846	[number]	k4	85846
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KLIMEK	Klimek	k1gMnSc1	Klimek
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
Hrad	hrad	k1gInSc4	hrad
:	:	kIx,	:
vnitropolitický	vnitropolitický	k2eAgInSc4d1	vnitropolitický
vývoj	vývoj	k1gInSc4	vývoj
Československa	Československo	k1gNnSc2	Československo
1926-1935	[number]	k4	1926-1935
na	na	k7c6	na
půdorysu	půdorys	k1gInSc6	půdorys
zápasu	zápas	k1gInSc2	zápas
o	o	k7c4	o
prezidentské	prezidentský	k2eAgNnSc4d1	prezidentské
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yQnSc1	kdo
po	po	k7c6	po
Masarykovi	Masaryk	k1gMnSc6	Masaryk
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panevropa	Panevropa	k1gFnSc1	Panevropa
;	;	kIx,	;
Institut	institut	k1gInSc1	institut
pro	pro	k7c4	pro
středoevropskou	středoevropský	k2eAgFnSc4d1	středoevropská
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
591	[number]	k4	591
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86130	[number]	k4	86130
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KLIMEK	Klimek	k1gMnSc1	Klimek
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Říjen	říjen	k1gInSc1	říjen
1918	[number]	k4	1918
:	:	kIx,	:
vznik	vznik	k1gInSc4	vznik
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
279	[number]	k4	279
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
175	[number]	k4	175
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KLIMEK	Klimek	k1gMnSc1	Klimek
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
822	[number]	k4	822
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
328	[number]	k4	328
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KLIMEK	Klimek	k1gMnSc1	Klimek
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
768	[number]	k4	768
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
425	[number]	k4	425
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KLIMEK	Klimek	k1gMnSc1	Klimek
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Vítejte	vítat	k5eAaImRp2nP	vítat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Havran	Havran	k1gMnSc1	Havran
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
301	[number]	k4	301
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86515	[number]	k4	86515
<g/>
-	-	kIx~	-
<g/>
33	[number]	k4	33
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KVAČEK	KVAČEK	k?	KVAČEK
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
Evropou	Evropa	k1gFnSc7	Evropa
zataženo	zatáhnout	k5eAaPmNgNnS	zatáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
a	a	k8xC	a
Evropa	Evropa	k1gFnSc1	Evropa
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
450	[number]	k4	450
s.	s.	k?	s.
</s>
</p>
<p>
<s>
OLIVOVÁ	Olivová	k1gFnSc1	Olivová
<g/>
,	,	kIx,	,
Věra	Věra	k1gFnSc1	Věra
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
355	[number]	k4	355
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7184	[number]	k4	7184
<g/>
-	-	kIx~	-
<g/>
791	[number]	k4	791
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PEROUTKA	Peroutka	k1gMnSc1	Peroutka
<g/>
,	,	kIx,	,
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc4	budování
státu	stát	k1gInSc2	stát
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
867	[number]	k4	867
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1095	[number]	k4	1095
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PEROUTKA	Peroutka	k1gMnSc1	Peroutka
<g/>
,	,	kIx,	,
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc4	budování
státu	stát	k1gInSc2	stát
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
961	[number]	k4	961
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1122	[number]	k4	1122
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SKŘIVAN	Skřivan	k1gMnSc1	Skřivan
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Elite	Elit	k1gMnSc5	Elit
in	in	k?	in
the	the	k?	the
New	New	k1gFnSc2	New
State	status	k1gInSc5	status
–	–	k?	–
the	the	k?	the
Role	role	k1gFnSc1	role
of	of	k?	of
Foreign	Foreign	k1gInSc1	Foreign
Trade	Trad	k1gInSc5	Trad
and	and	k?	and
the	the	k?	the
Dispute	Dispute	k?	Dispute
about	about	k2eAgMnSc1d1	about
Tariff	Tariff	k1gMnSc1	Tariff
Policy	Polica	k1gFnSc2	Polica
after	after	k1gMnSc1	after
the	the	k?	the
Establishment	establishment	k1gInSc1	establishment
of	of	k?	of
Czechoslovakia	Czechoslovakia	k1gFnSc1	Czechoslovakia
<g/>
.	.	kIx.	.
</s>
<s>
Prague	Prague	k1gFnSc1	Prague
Papers	Papersa	k1gFnPc2	Papersa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
History	Histor	k1gInPc4	Histor
of	of	k?	of
International	International	k1gFnSc2	International
Relations	Relationsa	k1gFnPc2	Relationsa
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
,	,	kIx,	,
s.	s.	k?	s.
489	[number]	k4	489
<g/>
-	-	kIx~	-
<g/>
493	[number]	k4	493
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
[	[	kIx(	[
<g/>
PDF	PDF	kA	PDF
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7308	[number]	k4	7308
<g/>
-	-	kIx~	-
<g/>
208	[number]	k4	208
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Tóth	Tóth	k1gMnSc1	Tóth
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Stehlík	Stehlík	k1gMnSc1	Stehlík
<g/>
:	:	kIx,	:
Národnostní	národnostní	k2eAgFnPc1d1	národnostní
menšiny	menšina	k1gFnPc1	menšina
v	v	k7c6	v
ústavněprávním	ústavněprávní	k2eAgInSc6d1	ústavněprávní
rámci	rámec	k1gInSc6	rámec
první	první	k4xOgFnSc2	první
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
–	–	k?	–
časopis	časopis	k1gInSc1	časopis
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
dějepisu	dějepis	k1gInSc2	dějepis
a	a	k8xC	a
popularizaci	popularizace	k1gFnSc4	popularizace
historie	historie	k1gFnSc2	historie
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
256-269	[number]	k4	256-269
</s>
</p>
<p>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Tóth	Tóth	k1gMnSc1	Tóth
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Stehlík	Stehlík	k1gMnSc1	Stehlík
<g/>
:	:	kIx,	:
Národnostní	národnostní	k2eAgFnPc1d1	národnostní
menšiny	menšina	k1gFnPc1	menšina
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
1918-1938	[number]	k4	1918-1938
–	–	k?	–
Od	od	k7c2	od
státu	stát	k1gInSc2	stát
národního	národní	k2eAgInSc2d1	národní
ke	k	k7c3	k
státu	stát	k1gInSc3	stát
národnostnímu	národnostní	k2eAgInSc3d1	národnostní
<g/>
?	?	kIx.	?
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
–	–	k?	–
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
/	/	kIx~	/
Togga	Togga	k1gFnSc1	Togga
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7308-413-4	[number]	k4	978-80-7308-413-4
(	(	kIx(	(
<g/>
FF	ff	kA	ff
UK	UK	kA	UK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
978-80-87258-58-3	[number]	k4	978-80-87258-58-3
(	(	kIx(	(
<g/>
Togga	Togga	k1gFnSc1	Togga
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Osvaldová	Osvaldová	k1gFnSc1	Osvaldová
<g/>
,	,	kIx,	,
Barbora	Barbora	k1gFnSc1	Barbora
<g/>
,	,	kIx,	,
Čeňková	Čeňkový	k2eAgFnSc1d1	Čeňková
Jana	Jana	k1gFnSc1	Jana
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
publicistika	publicistika	k1gFnSc1	publicistika
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
světovými	světový	k2eAgFnPc7d1	světová
válkami	válka	k1gFnPc7	válka
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
:	:	kIx,	:
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-200-2754-2	[number]	k4	978-80-200-2754-2
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
Československa	Československo	k1gNnSc2	Československo
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
měnová	měnový	k2eAgFnSc1d1	měnová
reforma	reforma	k1gFnSc1	reforma
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
republika	republika	k1gFnSc1	republika
</s>
</p>
<p>
<s>
Protektorát	protektorát	k1gInSc1	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
první	první	k4xOgFnSc1	první
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
první	první	k4xOgFnSc1	první
republika	republika	k1gFnSc1	republika
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
