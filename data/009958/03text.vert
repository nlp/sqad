<p>
<s>
Crystalex	Crystalex	k1gInSc1	Crystalex
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc4d1	český
podnik	podnik	k1gInSc4	podnik
vyrábějící	vyrábějící	k2eAgNnSc1d1	vyrábějící
užitkové	užitkový	k2eAgNnSc1d1	užitkové
sklo	sklo	k1gNnSc1	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnPc3d3	veliký
domácím	domácí	k2eAgMnPc3d1	domácí
a	a	k8xC	a
předním	přední	k2eAgMnSc7d1	přední
světovým	světový	k2eAgMnSc7d1	světový
výrobcem	výrobce	k1gMnSc7	výrobce
nápojového	nápojový	k2eAgNnSc2d1	nápojové
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
název	název	k1gInSc1	název
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
Crystalex	Crystalex	k1gInSc1	Crystalex
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Hlavní	hlavní	k2eAgInSc4d1	hlavní
výrobní	výrobní	k2eAgInSc4d1	výrobní
závod	závod	k1gInSc4	závod
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Boru	bor	k1gInSc6	bor
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
výrobní	výrobní	k2eAgInSc1d1	výrobní
závod	závod	k1gInSc1	závod
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Karolince	Karolinka	k1gFnSc6	Karolinka
na	na	k7c6	na
Vsetínsku	Vsetínsko	k1gNnSc6	Vsetínsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Produkty	produkt	k1gInPc1	produkt
firmy	firma	k1gFnSc2	firma
jsou	být	k5eAaImIp3nP	být
strojově	strojově	k6eAd1	strojově
vyráběné	vyráběný	k2eAgInPc4d1	vyráběný
výrobky	výrobek	k1gInPc4	výrobek
ze	z	k7c2	z
sodnodraselného	sodnodraselný	k2eAgNnSc2d1	sodnodraselný
skla	sklo	k1gNnSc2	sklo
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
crystalin	crystalin	k2eAgInSc1d1	crystalin
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
kalíšky	kalíšek	k1gInPc1	kalíšek
<g/>
,	,	kIx,	,
odlivky	odlivka	k1gFnPc1	odlivka
<g/>
,	,	kIx,	,
dekantery	dekanter	k1gInPc1	dekanter
<g/>
,	,	kIx,	,
džbány	džbán	k1gInPc1	džbán
a	a	k8xC	a
vázy	váza	k1gFnPc1	váza
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
výrobky	výrobek	k1gInPc1	výrobek
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
zušlechťovány	zušlechťovat	k5eAaImNgInP	zušlechťovat
dekoračními	dekorační	k2eAgFnPc7d1	dekorační
technikami	technika	k1gFnPc7	technika
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
jak	jak	k6eAd1	jak
tradičními	tradiční	k2eAgInPc7d1	tradiční
tak	tak	k8xC	tak
moderními	moderní	k2eAgInPc7d1	moderní
(	(	kIx(	(
<g/>
pantograph	pantograph	k1gInSc1	pantograph
<g/>
,	,	kIx,	,
diaryt	diaryt	k1gInSc1	diaryt
<g/>
,	,	kIx,	,
stříkání	stříkání	k1gNnSc1	stříkání
<g/>
,	,	kIx,	,
pískování	pískování	k1gNnSc1	pískování
<g/>
,	,	kIx,	,
malování	malování	k1gNnSc1	malování
<g/>
,	,	kIx,	,
vysoký	vysoký	k2eAgInSc1d1	vysoký
smalt	smalt	k1gInSc1	smalt
a	a	k8xC	a
sítotisk	sítotisk	k1gInSc1	sítotisk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
Dekret	dekret	k1gInSc1	dekret
o	o	k7c4	o
znárodnění	znárodnění	k1gNnSc4	znárodnění
velkých	velký	k2eAgInPc2d1	velký
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
národní	národní	k2eAgInSc1d1	národní
podnik	podnik	k1gInSc1	podnik
Borocrystal	Borocrystal	k1gFnSc1	Borocrystal
<g/>
,	,	kIx,	,
sdružující	sdružující	k2eAgFnSc1d1	sdružující
55	[number]	k4	55
původních	původní	k2eAgFnPc2d1	původní
drobných	drobný	k2eAgFnPc2d1	drobná
provozoven	provozovna	k1gFnPc2	provozovna
a	a	k8xC	a
skláren	sklárna	k1gFnPc2	sklárna
<g/>
,	,	kIx,	,
a	a	k8xC	a
národní	národní	k2eAgInSc4d1	národní
podnik	podnik	k1gInSc4	podnik
Umělecké	umělecký	k2eAgNnSc1d1	umělecké
sklo	sklo	k1gNnSc1	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Konsolidací	konsolidace	k1gFnSc7	konsolidace
těchto	tento	k3xDgInPc2	tento
podniků	podnik	k1gInPc2	podnik
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
n.	n.	k?	n.
p.	p.	k?	p.
Borské	Borské	k2eAgFnSc2d1	Borské
sklárny	sklárna	k1gFnSc2	sklárna
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
státní	státní	k2eAgInSc4d1	státní
podnik	podnik	k1gInSc4	podnik
Borské	Borské	k2eAgNnSc1d1	Borské
sklo	sklo	k1gNnSc1	sklo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
výstavba	výstavba	k1gFnSc1	výstavba
nového	nový	k2eAgInSc2d1	nový
výrobního	výrobní	k2eAgInSc2d1	výrobní
areálu	areál	k1gInSc2	areál
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Boru	bor	k1gInSc6	bor
<g/>
.	.	kIx.	.
</s>
<s>
Budovy	budova	k1gFnPc1	budova
sklářského	sklářský	k2eAgInSc2d1	sklářský
kombinátu	kombinát	k1gInSc2	kombinát
byly	být	k5eAaImAgFnP	být
stavěny	stavit	k5eAaImNgFnP	stavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1965	[number]	k4	1965
až	až	k9	až
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
první	první	k4xOgFnSc1	první
velká	velký	k2eAgFnSc1d1	velká
investiční	investiční	k2eAgFnSc1d1	investiční
akce	akce	k1gFnSc1	akce
svěřená	svěřený	k2eAgFnSc1d1	svěřená
zahraniční	zahraniční	k2eAgFnSc3d1	zahraniční
firmě	firma	k1gFnSc3	firma
<g/>
,	,	kIx,	,
sdružení	sdružení	k1gNnSc3	sdružení
jugoslávkých	jugoslávký	k2eAgInPc2d1	jugoslávký
podniků	podnik	k1gInPc2	podnik
Union	union	k1gInSc1	union
Engineering	Engineering	k1gInSc1	Engineering
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
kombinátu	kombinát	k1gInSc6	kombinát
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
výroba	výroba	k1gFnSc1	výroba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
zde	zde	k6eAd1	zde
fungovala	fungovat	k5eAaImAgFnS	fungovat
první	první	k4xOgFnSc1	první
automatizovaná	automatizovaný	k2eAgFnSc1d1	automatizovaná
linka	linka	k1gFnSc1	linka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
i	i	k9	i
sériová	sériový	k2eAgFnSc1d1	sériová
výroba	výroba	k1gFnSc1	výroba
(	(	kIx(	(
<g/>
strojně	strojně	k6eAd1	strojně
foukaná	foukaný	k2eAgFnSc1d1	foukaná
kalíškovina	kalíškovina	k1gFnSc1	kalíškovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
podnik	podnik	k1gInSc1	podnik
změnil	změnit	k5eAaPmAgInS	změnit
název	název	k1gInSc4	název
na	na	k7c4	na
Crystalex	Crystalex	k1gInSc4	Crystalex
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
pod	pod	k7c7	pod
koncern	koncern	k1gInSc1	koncern
Crystalex	Crystalex	k1gInSc1	Crystalex
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Boru	bor	k1gInSc6	bor
postupně	postupně	k6eAd1	postupně
přešla	přejít	k5eAaPmAgFnS	přejít
výroba	výroba	k1gFnSc1	výroba
užitkového	užitkový	k2eAgNnSc2d1	užitkové
skla	sklo	k1gNnSc2	sklo
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Crystalex	Crystalex	k1gInSc1	Crystalex
tak	tak	k9	tak
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
sdružoval	sdružovat	k5eAaImAgInS	sdružovat
většinu	většina	k1gFnSc4	většina
českých	český	k2eAgFnPc2d1	Česká
skláren	sklárna	k1gFnPc2	sklárna
vyrábějících	vyrábějící	k2eAgInPc2d1	vyrábějící
užitkové	užitkový	k2eAgNnSc4d1	užitkové
sklo	sklo	k1gNnSc4	sklo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prodej	prodej	k1gInSc1	prodej
výrobků	výrobek	k1gInPc2	výrobek
Crystalexu	Crystalex	k1gInSc2	Crystalex
na	na	k7c6	na
zahraničních	zahraniční	k2eAgInPc6d1	zahraniční
trzích	trh	k1gInPc6	trh
zajišťoval	zajišťovat	k5eAaImAgInS	zajišťovat
podnik	podnik	k1gInSc1	podnik
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
Skloexport	Skloexport	k1gInSc1	Skloexport
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
řada	řada	k1gFnSc1	řada
provozoven	provozovna	k1gFnPc2	provozovna
státního	státní	k2eAgInSc2d1	státní
podniku	podnik	k1gInSc2	podnik
osamostatnila	osamostatnit	k5eAaPmAgFnS	osamostatnit
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
privatizována	privatizovat	k5eAaImNgFnS	privatizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Boru	bor	k1gInSc6	bor
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
malých	malý	k2eAgFnPc2d1	malá
skláren	sklárna	k1gFnPc2	sklárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Centrální	centrální	k2eAgInSc1d1	centrální
podnik	podnik	k1gInSc1	podnik
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Boru	bor	k1gInSc6	bor
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1994	[number]	k4	1994
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1997	[number]	k4	1997
schválila	schválit	k5eAaPmAgFnS	schválit
vláda	vláda	k1gFnSc1	vláda
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
návrh	návrh	k1gInSc4	návrh
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
Ivana	Ivan	k1gMnSc2	Ivan
Kočárníka	kočárník	k1gMnSc2	kočárník
na	na	k7c4	na
prodej	prodej	k1gInSc4	prodej
72	[number]	k4	72
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
společnosti	společnost	k1gFnSc2	společnost
Porcela	Porcela	k1gFnSc2	Porcela
Plus	plus	k1gInSc1	plus
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
společnost	společnost	k1gFnSc1	společnost
Bohemia	bohemia	k1gFnSc1	bohemia
Crystalex	Crystalex	k1gInSc4	Crystalex
Trading	Trading	k1gInSc1	Trading
(	(	kIx(	(
<g/>
BCT	BCT	kA	BCT
<g/>
)	)	kIx)	)
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
vývoz	vývoz	k1gInSc4	vývoz
na	na	k7c4	na
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
trhy	trh	k1gInPc4	trh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
měl	mít	k5eAaImAgInS	mít
Crystalex	Crystalex	k1gInSc1	Crystalex
čtyři	čtyři	k4xCgInPc4	čtyři
výrobní	výrobní	k2eAgInPc4d1	výrobní
závody	závod	k1gInPc4	závod
<g/>
:	:	kIx,	:
Nový	nový	k2eAgInSc1d1	nový
Bor	bor	k1gInSc1	bor
<g/>
,	,	kIx,	,
Hostomice	Hostomika	k1gFnSc6	Hostomika
nad	nad	k7c7	nad
Bílinou	Bílina	k1gFnSc7	Bílina
<g/>
,	,	kIx,	,
Karolinka	Karolinka	k1gFnSc1	Karolinka
a	a	k8xC	a
Vrbno	Vrbno	k1gNnSc1	Vrbno
pod	pod	k7c7	pod
Pradědem	praděd	k1gMnSc7	praděd
<g/>
.	.	kIx.	.
</s>
<s>
Karolinka	Karolinka	k1gFnSc1	Karolinka
i	i	k9	i
Vrbno	Vrbno	k6eAd1	Vrbno
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
největšímu	veliký	k2eAgInSc3d3	veliký
rozvoji	rozvoj	k1gInSc3	rozvoj
výroby	výroba	k1gFnSc2	výroba
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
firma	firma	k1gFnSc1	firma
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
do	do	k7c2	do
sklářské	sklářský	k2eAgFnSc2d1	sklářská
skupiny	skupina	k1gFnSc2	skupina
BCT	BCT	kA	BCT
a	a	k8xC	a
mající	mající	k2eAgInSc1d1	mající
obrat	obrat	k1gInSc1	obrat
1,5	[number]	k4	1,5
miliardy	miliarda	k4xCgFnSc2	miliarda
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
zkrachovala	zkrachovat	k5eAaPmAgFnS	zkrachovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2008	[number]	k4	2008
společnost	společnost	k1gFnSc1	společnost
BCT	BCT	kA	BCT
sama	sám	k3xTgFnSc1	sám
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
podala	podat	k5eAaPmAgFnS	podat
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
zahájení	zahájení	k1gNnSc4	zahájení
insolvence	insolvence	k1gFnSc2	insolvence
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
začalo	začít	k5eAaPmAgNnS	začít
platit	platit	k5eAaImF	platit
tříměsíční	tříměsíční	k2eAgNnSc4d1	tříměsíční
moratorium	moratorium	k1gNnSc4	moratorium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
znamenalo	znamenat	k5eAaImAgNnS	znamenat
ochranu	ochrana	k1gFnSc4	ochrana
dodávek	dodávka	k1gFnPc2	dodávka
energií	energie	k1gFnPc2	energie
a	a	k8xC	a
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Majitelé	majitel	k1gMnPc1	majitel
museli	muset	k5eAaImAgMnP	muset
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
lhůtě	lhůta	k1gFnSc6	lhůta
najít	najít	k5eAaPmF	najít
nového	nový	k2eAgMnSc4d1	nový
investora	investor	k1gMnSc4	investor
<g/>
.	.	kIx.	.
</s>
<s>
Moratorium	moratorium	k1gNnSc1	moratorium
skončilo	skončit	k5eAaPmAgNnS	skončit
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
BCT	BCT	kA	BCT
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
měsíční	měsíční	k2eAgNnSc1d1	měsíční
prodloužení	prodloužení	k1gNnSc1	prodloužení
<g/>
,	,	kIx,	,
Crystalex	Crystalex	k1gInSc1	Crystalex
zamířil	zamířit	k5eAaPmAgInS	zamířit
do	do	k7c2	do
konkurzu	konkurz	k1gInSc2	konkurz
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
zastavena	zastaven	k2eAgFnSc1d1	zastavena
výroba	výroba	k1gFnSc1	výroba
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
závodech	závod	k1gInPc6	závod
<g/>
,	,	kIx,	,
všech	všecek	k3xTgInPc2	všecek
1800	[number]	k4	1800
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
1200	[number]	k4	1200
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Boru	bor	k1gInSc6	bor
<g/>
)	)	kIx)	)
dostalo	dostat	k5eAaPmAgNnS	dostat
výpověď	výpověď	k1gFnSc4	výpověď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vývoj	vývoj	k1gInSc1	vývoj
názvu	název	k1gInSc2	název
===	===	k?	===
</s>
</p>
<p>
<s>
1948	[number]	k4	1948
Borocrystal	Borocrystal	k1gFnSc1	Borocrystal
<g/>
,	,	kIx,	,
národní	národní	k2eAgInSc1d1	národní
podnik	podnik	k1gInSc1	podnik
</s>
</p>
<p>
<s>
1953	[number]	k4	1953
Borské	Borské	k2eAgInSc4d1	Borské
sklo	sklo	k1gNnSc1	sklo
</s>
</p>
<p>
<s>
1972	[number]	k4	1972
Crystalex	Crystalex	k1gInSc1	Crystalex
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
Crystalex	Crystalex	k1gInSc1	Crystalex
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
Crystalex	Crystalex	k1gInSc1	Crystalex
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
koupila	koupit	k5eAaPmAgFnS	koupit
výrobní	výrobní	k2eAgInPc4d1	výrobní
závody	závod	k1gInPc4	závod
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Boru	bor	k1gInSc6	bor
a	a	k8xC	a
Karolince	Karolinka	k1gFnSc6	Karolinka
za	za	k7c4	za
362,7	[number]	k4	362,7
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
sklářská	sklářský	k2eAgFnSc1d1	sklářská
skupina	skupina	k1gFnSc1	skupina
CBC	CBC	kA	CBC
Invest	Invest	k1gFnSc4	Invest
<g/>
,	,	kIx,	,
za	za	k7c2	za
níž	jenž	k3xRgFnSc2	jenž
stojí	stát	k5eAaImIp3nS	stát
ruský	ruský	k2eAgInSc1d1	ruský
a	a	k8xC	a
slovenský	slovenský	k2eAgInSc1d1	slovenský
kapitál	kapitál	k1gInSc1	kapitál
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
novým	nový	k2eAgMnSc7d1	nový
vlastníkem	vlastník	k1gMnSc7	vlastník
obnovena	obnovit	k5eAaPmNgNnP	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vykázal	vykázat	k5eAaPmAgInS	vykázat
Crystalex	Crystalex	k1gInSc1	Crystalex
mnohamilionový	mnohamilionový	k2eAgInSc1d1	mnohamilionový
zisk	zisk	k1gInSc4	zisk
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
trhy	trh	k1gInPc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zaměstnaly	zaměstnat	k5eAaPmAgFnP	zaměstnat
sklárny	sklárna	k1gFnPc1	sklárna
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Boru	bor	k1gInSc6	bor
500	[number]	k4	500
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
170	[number]	k4	170
v	v	k7c6	v
Karolince	Karolinka	k1gFnSc6	Karolinka
<g/>
.	.	kIx.	.
<g/>
Novoborská	novoborský	k2eAgFnSc1d1	Novoborská
sklárna	sklárna	k1gFnSc1	sklárna
vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
své	svůj	k3xOyFgInPc4	svůj
sklářské	sklářský	k2eAgInPc4d1	sklářský
produkty	produkt	k1gInPc4	produkt
do	do	k7c2	do
60	[number]	k4	60
zemí	zem	k1gFnPc2	zem
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
největším	veliký	k2eAgMnSc7d3	veliký
výrobcem	výrobce	k1gMnSc7	výrobce
nápojového	nápojový	k2eAgNnSc2d1	nápojové
skla	sklo	k1gNnSc2	sklo
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
ceněnou	ceněný	k2eAgFnSc4d1	ceněná
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
známku	známka	k1gFnSc4	známka
Bohemia	bohemia	k1gFnSc1	bohemia
Crystal	Crystal	k1gFnSc1	Crystal
<g/>
.	.	kIx.	.
</s>
<s>
Podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
organizaci	organizace	k1gFnSc4	organizace
sklářských	sklářský	k2eAgNnPc2d1	sklářské
sympozií	sympozion	k1gNnPc2	sympozion
IGS	IGS	kA	IGS
(	(	kIx(	(
<g/>
International	International	k1gMnSc2	International
Glass	Glass	k1gInSc1	Glass
Sympozium	sympozium	k1gNnSc1	sympozium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konaných	konaný	k2eAgFnPc2d1	konaná
každé	každý	k3xTgInPc4	každý
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
nová	nový	k2eAgFnSc1d1	nová
sklářská	sklářský	k2eAgFnSc1d1	sklářská
pec	pec	k1gFnSc1	pec
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
výrobními	výrobní	k2eAgFnPc7d1	výrobní
linkami	linka	k1gFnPc7	linka
na	na	k7c4	na
větší	veliký	k2eAgInSc4d2	veliký
kusy	kus	k1gInPc4	kus
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
výroby	výroba	k1gFnSc2	výroba
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
před	před	k7c7	před
krachem	krach	k1gInSc7	krach
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
pracovníků	pracovník	k1gMnPc2	pracovník
stoupl	stoupnout	k5eAaPmAgInS	stoupnout
na	na	k7c4	na
646	[number]	k4	646
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
měl	mít	k5eAaImAgInS	mít
podnik	podnik	k1gInSc1	podnik
tržby	tržba	k1gFnSc2	tržba
870	[number]	k4	870
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2014	[number]	k4	2014
je	být	k5eAaImIp3nS	být
odhad	odhad	k1gInSc4	odhad
tržeb	tržba	k1gFnPc2	tržba
1	[number]	k4	1
miliarda	miliarda	k4xCgFnSc1	miliarda
Kč	Kč	kA	Kč
při	při	k7c6	při
stabilizovaném	stabilizovaný	k2eAgInSc6d1	stabilizovaný
počtu	počet	k1gInSc6	počet
650	[number]	k4	650
kmenových	kmenový	k2eAgFnPc2d1	kmenová
a	a	k8xC	a
130	[number]	k4	130
agenturních	agenturní	k2eAgMnPc2d1	agenturní
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Karolince	Karolinka	k1gFnSc6	Karolinka
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
provádí	provádět	k5eAaImIp3nS	provádět
pouze	pouze	k6eAd1	pouze
dekorace	dekorace	k1gFnSc1	dekorace
výrobků	výrobek	k1gInPc2	výrobek
dovezených	dovezený	k2eAgInPc2d1	dovezený
z	z	k7c2	z
Nového	Nového	k2eAgInSc2d1	Nového
Boru	bor	k1gInSc2	bor
<g/>
.	.	kIx.	.
</s>
<s>
Hutní	hutní	k2eAgFnSc1d1	hutní
výroba	výroba	k1gFnSc1	výroba
skla	sklo	k1gNnSc2	sklo
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Karolince	Karolinka	k1gFnSc6	Karolinka
ukončena	ukončit	k5eAaPmNgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sbírka	sbírka	k1gFnSc1	sbírka
skla	sklo	k1gNnSc2	sklo
==	==	k?	==
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
cenná	cenný	k2eAgFnSc1d1	cenná
<g/>
,	,	kIx,	,
unikátní	unikátní	k2eAgFnSc1d1	unikátní
sbírka	sbírka	k1gFnSc1	sbírka
1500	[number]	k4	1500
kusů	kus	k1gInPc2	kus
skla	sklo	k1gNnSc2	sklo
zkrachovalého	zkrachovalý	k2eAgInSc2d1	zkrachovalý
podniku	podnik	k1gInSc2	podnik
byla	být	k5eAaImAgFnS	být
zakoupena	zakoupit	k5eAaPmNgFnS	zakoupit
za	za	k7c4	za
8,5	[number]	k4	8,5
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
Nadací	nadace	k1gFnPc2	nadace
Josefa	Josef	k1gMnSc4	Josef
Viewegha	Viewegh	k1gMnSc4	Viewegh
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
až	až	k9	až
2011	[number]	k4	2011
vystavena	vystavit	k5eAaPmNgFnS	vystavit
v	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
patře	patro	k1gNnSc6	patro
zámku	zámek	k1gInSc2	zámek
Lemberk	Lemberk	k1gInSc1	Lemberk
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
odstěhovala	odstěhovat	k5eAaPmAgFnS	odstěhovat
do	do	k7c2	do
nově	nově	k6eAd1	nově
vytvořeného	vytvořený	k2eAgNnSc2d1	vytvořené
Centra	centrum	k1gNnSc2	centrum
sklářského	sklářský	k2eAgNnSc2d1	sklářské
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
městečku	městečko	k1gNnSc6	městečko
Sázava	Sázava	k1gFnSc1	Sázava
na	na	k7c6	na
Benešovsku	Benešovsko	k1gNnSc6	Benešovsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Crystalex	Crystalex	k1gInSc1	Crystalex
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
webové	webový	k2eAgFnPc1d1	webová
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Podnik	podnik	k1gInSc1	podnik
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2011	[number]	k4	2011
ziskový	ziskový	k2eAgInSc4d1	ziskový
</s>
</p>
<p>
<s>
Crystalex	Crystalex	k1gInSc1	Crystalex
ukončí	ukončit	k5eAaPmIp3nS	ukončit
výrobu	výroba	k1gFnSc4	výroba
v	v	k7c6	v
Hostomicích	Hostomice	k1gFnPc6	Hostomice
<g/>
,	,	kIx,	,
o	o	k7c6	o
práci	práce	k1gFnSc6	práce
přijde	přijít	k5eAaPmIp3nS	přijít
136	[number]	k4	136
lidí	člověk	k1gMnPc2	člověk
-	-	kIx~	-
iDnes	iDnes	k1gMnSc1	iDnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c4	o
znovu	znovu	k6eAd1	znovu
otevření	otevření	k1gNnSc4	otevření
Crystalexu	Crystalex	k1gInSc2	Crystalex
</s>
</p>
<p>
<s>
Aktuálně	aktuálně	k6eAd1	aktuálně
Českolipský	českolipský	k2eAgInSc1d1	českolipský
deník	deník	k1gInSc1	deník
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
Soudní	soudní	k2eAgNnSc1d1	soudní
řízení	řízení	k1gNnSc1	řízení
2016	[number]	k4	2016
</s>
</p>
