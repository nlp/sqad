<s>
Jednodomost	jednodomost	k1gFnSc1	jednodomost
(	(	kIx(	(
<g/>
monoecie	monoecie	k1gFnSc1	monoecie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc4	jejíž
každý	každý	k3xTgMnSc1	každý
jedinec	jedinec	k1gMnSc1	jedinec
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
jak	jak	k6eAd1	jak
samčí	samčí	k2eAgInSc1d1	samčí
tak	tak	k9	tak
i	i	k9	i
samičí	samičí	k2eAgInPc4d1	samičí
květy	květ	k1gInPc4	květ
na	na	k7c6	na
témže	týž	k3xTgNnSc6	týž
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Opakem	opak	k1gInSc7	opak
je	být	k5eAaImIp3nS	být
dvoudomost	dvoudomost	k1gFnSc1	dvoudomost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jednodomé	jednodomý	k2eAgFnPc4d1	jednodomá
rostliny	rostlina	k1gFnPc4	rostlina
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
bolševník	bolševník	k1gInSc1	bolševník
velkolepý	velkolepý	k2eAgInSc1d1	velkolepý
<g/>
,	,	kIx,	,
líska	líska	k1gFnSc1	líska
<g/>
,	,	kIx,	,
bříza	bříza	k1gFnSc1	bříza
<g/>
,	,	kIx,	,
olše	olše	k1gFnSc1	olše
<g/>
,	,	kIx,	,
smrk	smrk	k1gInSc1	smrk
ztepilý	ztepilý	k2eAgInSc1d1	ztepilý
<g/>
,	,	kIx,	,
kopřiva	kopřiva	k1gFnSc1	kopřiva
žahavka	žahavka	k1gFnSc1	žahavka
či	či	k8xC	či
ořešák	ořešák	k1gInSc1	ořešák
královský	královský	k2eAgMnSc1d1	královský
<g/>
.	.	kIx.	.
</s>
