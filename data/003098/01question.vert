<s>
Jaký	jaký	k3yQgInSc1	jaký
termín	termín	k1gInSc1	termín
označuje	označovat	k5eAaImIp3nS	označovat
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc4	jejíž
každý	každý	k3xTgMnSc1	každý
jedinec	jedinec	k1gMnSc1	jedinec
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
samčí	samčí	k2eAgInPc4d1	samčí
i	i	k8xC	i
samičí	samičí	k2eAgInPc4d1	samičí
květy	květ	k1gInPc4	květ
v	v	k7c6	v
témže	týž	k3xTgNnSc6	týž
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
?	?	kIx.	?
</s>
