<s>
Jednodomost	jednodomost	k1gFnSc1	jednodomost
(	(	kIx(	(
<g/>
monoecie	monoecie	k1gFnSc1	monoecie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc4	jejíž
každý	každý	k3xTgMnSc1	každý
jedinec	jedinec	k1gMnSc1	jedinec
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
jak	jak	k6eAd1	jak
samčí	samčí	k2eAgInSc1d1	samčí
tak	tak	k9	tak
i	i	k9	i
samičí	samičí	k2eAgInPc4d1	samičí
květy	květ	k1gInPc4	květ
na	na	k7c6	na
témže	týž	k3xTgNnSc6	týž
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
.	.	kIx.	.
</s>
