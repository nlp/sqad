<s>
Jan	Jan	k1gMnSc1
Kotík	Kotík	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Doc.	doc.	kA
ak.	ak.	k?
mal	málit	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Kotík	Kotík	k1gMnSc1
Jan	Jan	k1gMnSc1
Kotík	Kotík	k1gMnSc1
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
Georg	Georg	k1gMnSc1
Frietzsche	Frietzsch	k1gFnSc2
(	(	kIx(
<g/>
1903	#num#	k4
<g/>
–	–	k?
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
při	při	k7c6
rozhovoru	rozhovor	k1gInSc6
v	v	k7c6
Berlíně	Berlín	k1gInSc6
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1977	#num#	k4
Narození	narození	k1gNnPc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1916	#num#	k4
Turnov	Turnov	k1gInSc1
Rakousko-Uhersko	Rakousko-Uherska	k1gFnSc5
Rakousko-Uhersko	Rakousko-Uherska	k1gFnSc5
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2002	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
86	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Berlín	Berlín	k1gInSc1
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
malíř	malíř	k1gMnSc1
<g/>
,	,	kIx,
překladatel	překladatel	k1gMnSc1
<g/>
,	,	kIx,
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
grafik	grafik	k1gMnSc1
a	a	k8xC
teoretik	teoretik	k1gMnSc1
umění	umění	k1gNnSc2
Rodiče	rodič	k1gMnSc2
</s>
<s>
Pravoslav	Pravoslav	k1gMnSc1
Kotík	Kotík	k1gMnSc1
otec	otec	k1gMnSc1
Děti	dítě	k1gFnPc1
</s>
<s>
Martin	Martin	k1gMnSc1
Kotík	Kotík	k1gMnSc1
synPetr	synPetr	k1gMnSc1
Kotík	Kotík	k1gMnSc1
syn	syn	k1gMnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
Jakub	Jakub	k1gMnSc1
Kotík	Kotík	k1gMnSc1
vnukTomas	vnukTomas	k1gMnSc1
Kotík	Kotík	k1gMnSc1
vnukOndřej	vnukOndřát	k5eAaPmRp2nS,k5eAaImRp2nS
Kotík	Kotík	k1gInSc4
vnukTereza	vnukTerez	k1gMnSc2
Kotíková	Kotíkový	k2eAgFnSc1d1
vnučkaBenjamine	vnučkaBenjamin	k1gInSc5
Kotík	Kotík	k1gMnSc1
vnuk	vnuk	k1gMnSc1
Ocenění	ocenění	k1gNnSc4
</s>
<s>
bronzová	bronzový	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
databázi	databáze	k1gFnSc6
Národní	národní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jan	Jan	k1gMnSc1
Kotík	Kotík	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1916	#num#	k4
Turnov	Turnov	k1gInSc1
–	–	k?
23	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2002	#num#	k4
Berlín	Berlín	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
malíř	malíř	k1gMnSc1
<g/>
,	,	kIx,
průmyslový	průmyslový	k2eAgMnSc1d1
výtvarník	výtvarník	k1gMnSc1
<g/>
,	,	kIx,
grafik	grafik	k1gMnSc1
a	a	k8xC
pedagog	pedagog	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Kotík	Kotík	k1gMnSc1
byl	být	k5eAaImAgMnS
synem	syn	k1gMnSc7
malíře	malíř	k1gMnSc2
a	a	k8xC
profesora	profesor	k1gMnSc2
kreslení	kreslení	k1gNnSc2
na	na	k7c6
reálce	reálka	k1gFnSc6
v	v	k7c6
Mladé	mladý	k2eAgFnSc6d1
Boleslavi	Boleslaev	k1gFnSc6
Pravoslava	Pravoslav	k1gMnSc2
Kotíka	Kotík	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyučil	vyučit	k5eAaPmAgMnS
se	se	k3xPyFc4
typografem	typograf	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1935	#num#	k4
<g/>
–	–	k?
<g/>
1941	#num#	k4
studoval	studovat	k5eAaImAgMnS
na	na	k7c6
Uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc6d1
škole	škola	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
prof.	prof.	kA
Jaroslav	Jaroslav	k1gMnSc1
Benda	Benda	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podnikl	podniknout	k5eAaPmAgMnS
studijní	studijní	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
do	do	k7c2
Drážďan	Drážďany	k1gInPc2
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
do	do	k7c2
Paříže	Paříž	k1gFnSc2
(	(	kIx(
<g/>
1937	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
války	válka	k1gFnSc2
v	v	k7c6
letech	let	k1gInPc6
1941	#num#	k4
<g/>
–	–	k?
<g/>
1943	#num#	k4
vyučoval	vyučovat	k5eAaImAgMnS
na	na	k7c6
soukromé	soukromý	k2eAgFnSc6d1
umělecké	umělecký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Člen	člen	k1gMnSc1
Skupiny	skupina	k1gFnSc2
42	#num#	k4
(	(	kIx(
<g/>
1942	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Umělecké	umělecký	k2eAgFnPc1d1
besedy	beseda	k1gFnPc1
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
SČUG	SČUG	kA
Hollar	Hollar	k1gInSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
redakčního	redakční	k2eAgInSc2d1
kruhu	kruh	k1gInSc2
časopisu	časopis	k1gInSc2
Tvar	tvar	k1gInSc1
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
–	–	k?
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1947	#num#	k4
<g/>
–	–	k?
<g/>
1953	#num#	k4
pracoval	pracovat	k5eAaImAgInS
v	v	k7c6
ÚLUV	ÚLUV	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1951	#num#	k4
vedl	vést	k5eAaImAgInS
návrhářské	návrhářský	k2eAgInPc4d1
ateliéry	ateliér	k1gInPc4
a	a	k8xC
vzorové	vzorový	k2eAgFnPc4d1
dílny	dílna	k1gFnPc4
a	a	k8xC
spolupracoval	spolupracovat	k5eAaImAgInS
se	se	k3xPyFc4
sklárnou	sklárna	k1gFnSc7
ve	v	k7c6
Škrdlovicích	Škrdlovice	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1957	#num#	k4
<g/>
–	–	k?
<g/>
1968	#num#	k4
byl	být	k5eAaImAgInS
členem	člen	k1gInSc7
skupiny	skupina	k1gFnSc2
průmyslových	průmyslový	k2eAgMnPc2d1
výtvarníků	výtvarník	k1gMnPc2
Bilance	bilance	k1gFnSc2
při	při	k7c6
UB	UB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zúčastnil	zúčastnit	k5eAaPmAgMnS
se	se	k3xPyFc4
Světových	světový	k2eAgFnPc2d1
výstav	výstava	k1gFnPc2
Expo	Expo	k1gNnSc1
58	#num#	k4
v	v	k7c6
Bruselu	Brusel	k1gInSc6
a	a	k8xC
Expo	Expo	k1gNnSc1
67	#num#	k4
v	v	k7c6
Montrealu	Montreal	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
jako	jako	k8xC,k8xS
člen	člen	k1gMnSc1
předsednictva	předsednictvo	k1gNnSc2
SČVU	SČVU	kA
také	také	k6eAd1
32	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Benátského	benátský	k2eAgNnSc2d1
bienále	bienále	k1gNnSc2
roku	rok	k1gInSc2
1964	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1965	#num#	k4
byl	být	k5eAaImAgInS
na	na	k7c4
pozvání	pozvání	k1gNnSc4
British	British	k1gMnSc1
Art	Art	k1gMnSc1
Council	Council	k1gMnSc1
na	na	k7c6
studijním	studijní	k2eAgInSc6d1
pobytu	pobyt	k1gInSc6
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Články	článek	k1gInPc1
přispíval	přispívat	k5eAaImAgMnS
do	do	k7c2
časopisů	časopis	k1gInPc2
Tvar	tvar	k1gInSc1
<g/>
,	,	kIx,
Výtvarná	výtvarný	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
,	,	kIx,
Umění	umění	k1gNnPc1
a	a	k8xC
řemesla	řemeslo	k1gNnPc1
<g/>
,	,	kIx,
Ateliér	ateliér	k1gInSc1
<g/>
,	,	kIx,
aj.	aj.	kA
</s>
<s>
Roku	rok	k1gInSc2
1969	#num#	k4
byl	být	k5eAaImAgInS
na	na	k7c6
jednoročním	jednoroční	k2eAgInSc6d1
pobytu	pobyt	k1gInSc6
v	v	k7c6
Západním	západní	k2eAgInSc6d1
Berlíně	Berlín	k1gInSc6
v	v	k7c6
rámci	rámec	k1gInSc6
Deutscher	Deutschra	k1gFnPc2
Akademischer	Akademischra	k1gFnPc2
Austausch	Austausch	k1gInSc1
Dienst	Dienst	k1gMnSc1
a	a	k8xC
do	do	k7c2
Československa	Československo	k1gNnSc2
se	se	k3xPyFc4
už	už	k6eAd1
nevrátil	vrátit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1976	#num#	k4
byl	být	k5eAaImAgInS
v	v	k7c6
nepřítomnosti	nepřítomnost	k1gFnSc6
odsouzen	odsouzet	k5eAaImNgMnS,k5eAaPmNgMnS
ke	k	k7c3
třem	tři	k4xCgNnPc3
letům	léto	k1gNnPc3
vězení	vězení	k1gNnSc2
za	za	k7c4
nepovolený	povolený	k2eNgInSc4d1
pobyt	pobyt	k1gInSc4
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
a	a	k8xC
roku	rok	k1gInSc6
1979	#num#	k4
zbaven	zbaven	k2eAgMnSc1d1
čs	čs	kA
<g/>
.	.	kIx.
státního	státní	k2eAgNnSc2d1
občanství	občanství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1972	#num#	k4
působil	působit	k5eAaImAgMnS
jako	jako	k9
pedagog	pedagog	k1gMnSc1
na	na	k7c6
Hochschule	Hochschula	k1gFnSc6
für	für	k?
Gestaltung	Gestaltunga	k1gFnPc2
v	v	k7c6
Ulmu	Ulmus	k1gInSc6
<g/>
,	,	kIx,
1974	#num#	k4
zakladatel	zakladatel	k1gMnSc1
skupiny	skupina	k1gFnSc2
Systhema	Systhema	k1gFnSc1
a	a	k8xC
pracovní	pracovní	k2eAgInSc1d1
pobyt	pobyt	k1gInSc1
v	v	k7c6
Olevanu	Olevan	k1gMnSc6
Romanu	Roman	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1980	#num#	k4
byl	být	k5eAaImAgMnS
hostem	host	k1gMnSc7
Cité	Citý	k2eAgFnSc2d1
Internationale	Internationale	k1gFnSc2
des	des	k1gNnSc2
Arts	Artsa	k1gFnPc2
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1982	#num#	k4
<g/>
–	–	k?
<g/>
1983	#num#	k4
byl	být	k5eAaImAgInS
na	na	k7c6
pracovním	pracovní	k2eAgInSc6d1
pobytu	pobyt	k1gInSc6
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Československa	Československo	k1gNnSc2
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
poprvé	poprvé	k6eAd1
roku	rok	k1gInSc2
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1991	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
působil	působit	k5eAaImAgMnS
jako	jako	k9
externí	externí	k2eAgMnSc1d1
docent	docent	k1gMnSc1
AVU	AVU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1992	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
berlínské	berlínský	k2eAgFnSc2d1
Akademie	akademie	k1gFnSc2
der	drát	k5eAaImRp2nS
Kunste	Kunst	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Rané	raný	k2eAgNnSc1d1
období	období	k1gNnSc1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
civilizačním	civilizační	k2eAgInSc7d1
programem	program	k1gInSc7
Skupiny	skupina	k1gFnSc2
42	#num#	k4
a	a	k8xC
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
postkubistické	postkubistický	k2eAgFnSc2d1
stylizace	stylizace	k1gFnSc2
reality	realita	k1gFnSc2
(	(	kIx(
<g/>
Deštivý	deštivý	k2eAgInSc1d1
den	den	k1gInSc1
<g/>
,	,	kIx,
Cyklista	cyklista	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Procházel	procházet	k5eAaImAgMnS
obdobím	období	k1gNnSc7
geometrické	geometrický	k2eAgFnSc2d1
abstrakce	abstrakce	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
v	v	k7c6
obrazu	obraz	k1gInSc6
uplatnil	uplatnit	k5eAaPmAgInS
písmo	písmo	k1gNnSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
kaligrafických	kaligrafický	k2eAgInPc2d1
znaků	znak	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
experimentoval	experimentovat	k5eAaImAgMnS
s	s	k7c7
nekonvenčním	konvenční	k2eNgInSc7d1
obrazovým	obrazový	k2eAgInSc7d1
formátem	formát	k1gInSc7
<g/>
,	,	kIx,
malířským	malířský	k2eAgInSc7d1
environmentem	environment	k1gInSc7
<g/>
,	,	kIx,
konstruktivními	konstruktivní	k2eAgFnPc7d1
a	a	k8xC
rituálními	rituální	k2eAgFnPc7d1
variabily	variabit	k5eAaImAgFnP,k5eAaPmAgFnP
(	(	kIx(
<g/>
Anatomie	anatomie	k1gFnSc2
MUDr.	MUDr.	kA
Tulpa	Tulpa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgMnS
transformací	transformace	k1gFnSc7
některých	některý	k3yIgInPc2
principů	princip	k1gInPc2
kinetismu	kinetismus	k1gInSc2
<g/>
,	,	kIx,
minimalu	minimal	k1gInSc2
<g/>
,	,	kIx,
poverismu	poverismus	k1gInSc2
aj.	aj.	kA
<g/>
,	,	kIx,
opíraje	opírat	k5eAaImSgMnS
se	se	k3xPyFc4
o	o	k7c4
objevné	objevný	k2eAgInPc4d1
aspekty	aspekt	k1gInPc4
konceptuálních	konceptuální	k2eAgInPc2d1
postupů	postup	k1gInPc2
(	(	kIx(
<g/>
Transformační	transformační	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgInS
k	k	k7c3
malbě	malba	k1gFnSc3
živelné	živelný	k2eAgFnSc2d1
barevnosti	barevnost	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
náznakem	náznak	k1gInSc7
figurální	figurální	k2eAgFnSc2d1
malby	malba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
se	se	k3xPyFc4
též	též	k9
sklářské	sklářský	k2eAgFnSc3d1
tvorbě	tvorba	k1gFnSc3
(	(	kIx(
<g/>
kolekce	kolekce	k1gFnSc1
váz	váza	k1gFnPc2
–	–	k?
objektů	objekt	k1gInPc2
pro	pro	k7c4
Trienále	trienále	k1gNnSc4
v	v	k7c6
Miláně	Milán	k1gInSc6
<g/>
,	,	kIx,
vitráže	vitráž	k1gFnPc4
Skláři	sklář	k1gMnSc3
pro	pro	k7c4
EXPO	Expo	k1gNnSc4
’	’	k?
<g/>
58	#num#	k4
v	v	k7c6
Bruselu	Brusel	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autor	autor	k1gMnSc1
publikací	publikace	k1gFnPc2
Tradice	tradice	k1gFnSc2
a	a	k8xC
kultura	kultura	k1gFnSc1
československé	československý	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
<g/>
,	,	kIx,
Neúplný	úplný	k2eNgInSc4d1
kompas	kompas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Autorův	autorův	k2eAgInSc1d1
raný	raný	k2eAgInSc1d1
projev	projev	k1gInSc1
vyrůstal	vyrůstat	k5eAaImAgInS
z	z	k7c2
bohaté	bohatý	k2eAgFnSc2d1
domácí	domácí	k2eAgFnSc2d1
i	i	k8xC
evropské	evropský	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
mohl	moct	k5eAaImAgInS
navázat	navázat	k5eAaPmF
na	na	k7c4
živé	živý	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
českými	český	k2eAgMnPc7d1
a	a	k8xC
zahraničními	zahraniční	k2eAgMnPc7d1
umělci	umělec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
v	v	k7c6
časných	časný	k2eAgFnPc6d1
kresbách	kresba	k1gFnPc6
a	a	k8xC
grafických	grafický	k2eAgInPc6d1
listech	list	k1gInPc6
se	se	k3xPyFc4
u	u	k7c2
něj	on	k3xPp3gMnSc2
projevil	projevit	k5eAaPmAgInS
smysl	smysl	k1gInSc4
pro	pro	k7c4
zjednodušení	zjednodušení	k1gNnSc4
tvaru	tvar	k1gInSc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gNnSc4
uspořádání	uspořádání	k1gNnSc4
do	do	k7c2
přehledných	přehledný	k2eAgFnPc2d1
a	a	k8xC
zřetelných	zřetelný	k2eAgFnPc2d1
soustav	soustava	k1gFnPc2
a	a	k8xC
vyjadření	vyjadření	k1gNnSc1
skutečnosti	skutečnost	k1gFnSc2
formou	forma	k1gFnSc7
srozumitelných	srozumitelný	k2eAgInPc2d1
znaků	znak	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
v	v	k7c6
nich	on	k3xPp3gFnPc6
zakotven	zakotven	k2eAgMnSc1d1
cit	cit	k1gInSc4
pro	pro	k7c4
pevnou	pevný	k2eAgFnSc4d1
stavbu	stavba	k1gFnSc4
objemu	objem	k1gInSc2
a	a	k8xC
sošnost	sošnost	k1gFnSc4
tvaru	tvar	k1gInSc2
<g/>
,	,	kIx,
pro	pro	k7c4
jejich	jejich	k3xOp3gNnSc4
začlenění	začlenění	k1gNnSc4
do	do	k7c2
prostoru	prostor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výraz	výraz	k1gInSc1
reaguje	reagovat	k5eAaBmIp3nS
na	na	k7c4
proměny	proměna	k1gFnPc4
doby	doba	k1gFnSc2
a	a	k8xC
přitom	přitom	k6eAd1
si	se	k3xPyFc3
zachovává	zachovávat	k5eAaImIp3nS
některé	některý	k3yIgInPc4
příznačné	příznačný	k2eAgInPc4d1
rysy	rys	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Patřil	patřit	k5eAaImAgMnS
k	k	k7c3
významným	významný	k2eAgMnPc3d1
představitelům	představitel	k1gMnPc3
českého	český	k2eAgNnSc2d1
výtvarného	výtvarný	k2eAgNnSc2d1
umění	umění	k1gNnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
své	svůj	k3xOyFgNnSc4
dílo	dílo	k1gNnSc4
představil	představit	k5eAaPmAgMnS
na	na	k7c6
sklonku	sklonek	k1gInSc6
třicátých	třicátý	k4xOgNnPc2
let	léto	k1gNnPc2
u	u	k7c2
E.	E.	kA
F.	F.	kA
Buriana	Burian	k1gMnSc2
a	a	k8xC
když	když	k8xS
nemohl	moct	k5eNaImAgMnS
v	v	k7c6
padesátých	padesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
vystavovat	vystavovat	k5eAaImF
<g/>
,	,	kIx,
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
se	se	k3xPyFc4
designu	design	k1gInSc2
–	–	k?
jako	jako	k8xC,k8xS
tvůrce	tvůrce	k1gMnSc1
i	i	k8xC
teoretik	teoretik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
malovat	malovat	k5eAaImF
nepřestal	přestat	k5eNaPmAgInS
a	a	k8xC
jeho	jeho	k3xOp3gInPc1
obrazy	obraz	k1gInPc1
v	v	k7c6
duchu	duch	k1gMnSc6
expresívní	expresívní	k2eAgFnPc1d1
abstrakce	abstrakce	k1gFnPc1
získaly	získat	k5eAaPmAgFnP
značný	značný	k2eAgInSc4d1
ohlas	ohlas	k1gInSc4
i	i	k9
na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uznání	uznání	k1gNnPc2
se	se	k3xPyFc4
dočkal	dočkat	k5eAaPmAgInS
také	také	k9
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
kam	kam	k6eAd1
v	v	k7c6
sedmdesátých	sedmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
emigroval	emigrovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
domů	domů	k6eAd1
do	do	k7c2
Čech	Čechy	k1gFnPc2
se	se	k3xPyFc4
pořád	pořád	k6eAd1
vracel	vracet	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Publikace	publikace	k1gFnSc1
</s>
<s>
Tradice	tradice	k1gFnSc1
a	a	k8xC
kultura	kultura	k1gFnSc1
čc	čc	k?
<g/>
.	.	kIx.
výroby	výroba	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1954	#num#	k4
</s>
<s>
Hrách	hrách	k1gInSc1
na	na	k7c4
stěnu	stěna	k1gFnSc4
házeti	házet	k5eAaImF
<g/>
,	,	kIx,
aneb	aneb	k?
O	o	k7c6
užitečnosti	užitečnost	k1gFnSc6
věcí	věc	k1gFnPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1969	#num#	k4
</s>
<s>
Konsum	konsum	k1gInSc1
oder	odra	k1gFnPc2
Verbrauch	Verbraucha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Versuch	Versuch	k1gMnSc1
über	über	k1gMnSc1
Gebrauchswert	Gebrauchswert	k1gMnSc1
und	und	k?
bedürfnisse	bedürfnisse	k1gFnSc1
<g/>
,	,	kIx,
Hamburg	Hamburg	k1gInSc1
1974	#num#	k4
</s>
<s>
Neúplný	úplný	k2eNgInSc1d1
kompas	kompas	k1gInSc1
<g/>
,	,	kIx,
Köln	Köln	k1gInSc1
am	am	k?
Rhein	Rhein	k1gInSc1
1986	#num#	k4
</s>
<s>
Texty	text	k1gInPc1
<g/>
,	,	kIx,
překlady	překlad	k1gInPc1
a	a	k8xC
parafráze	parafráze	k1gFnPc1
<g/>
,	,	kIx,
žerty	žert	k1gInPc1
a	a	k8xC
rozhovor	rozhovor	k1gInSc1
s	s	k7c7
Ladislavem	Ladislav	k1gMnSc7
Novákem	Novák	k1gMnSc7
<g/>
,	,	kIx,
Třebíč	Třebíč	k1gFnSc1
1993	#num#	k4
</s>
<s>
Překlady	překlad	k1gInPc4
</s>
<s>
Lao-c	Lao-c	k1gFnSc1
<g/>
'	'	kIx"
<g/>
:	:	kIx,
Tao	Tao	k1gMnSc1
te	te	k?
ťing	ťing	k1gMnSc1
:	:	kIx,
v	v	k7c6
převodu	převod	k1gInSc6
a	a	k8xC
s	s	k7c7
poznámkami	poznámka	k1gFnPc7
Jana	Jan	k1gMnSc2
Kotíka	Kotík	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Galerie	galerie	k1gFnSc1
Jiří	Jiří	k1gMnSc1
Švestka	Švestka	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-238-7480-2	80-238-7480-2	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Lubomír	Lubomír	k1gMnSc1
Slavíček	Slavíček	k1gMnSc1
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Slovník	slovník	k1gInSc1
historiků	historik	k1gMnPc2
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
výtvarných	výtvarný	k2eAgMnPc2d1
kritiků	kritik	k1gMnPc2
<g/>
,	,	kIx,
teoretiků	teoretik	k1gMnPc2
a	a	k8xC
publicistů	publicista	k1gMnPc2
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
a	a	k8xC
jejich	jejich	k3xOp3gMnPc2
spolupracovníků	spolupracovník	k1gMnPc2
z	z	k7c2
příbuzných	příbuzný	k2eAgInPc2d1
oborů	obor	k1gInPc2
(	(	kIx(
<g/>
asi	asi	k9
1800	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sv.	sv.	kA
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
681	#num#	k4
<g/>
–	–	k?
<g/>
682	#num#	k4
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
Praha	Praha	k1gFnSc1
2016	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-200-2094-9	978-80-200-2094-9	k4
</s>
<s>
VAŇOUS	VAŇOUS	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
-	-	kIx~
MLADIČOVÁ	MLADIČOVÁ	kA
<g/>
,	,	kIx,
Iva	Iva	k1gFnSc1
<g/>
:	:	kIx,
Ano	ano	k9
i	i	k9
ne	ne	k9
aneb	aneb	k?
Důležitost	důležitost	k1gFnSc4
metody	metoda	k1gFnSc2
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
JAKUBEC	Jakubec	k1gMnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
-	-	kIx~
MILTOVÁ	MILTOVÁ	kA
<g/>
,	,	kIx,
Radka	Radka	k1gFnSc1
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Umění	umění	k1gNnSc1
a	a	k8xC
politika	politika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sborník	sborník	k1gInSc1
4	#num#	k4
<g/>
.	.	kIx.
sjezdu	sjezd	k1gInSc6
historiků	historik	k1gMnPc2
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
Barrister	Barrister	k1gMnSc1
&	&	k?
Principal	Principal	k1gMnSc1
a	a	k8xC
MU	MU	kA
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
2013	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
67	#num#	k4
<g/>
-	-	kIx~
<g/>
79	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-7485-023-3	978-80-7485-023-3	k4
(	(	kIx(
<g/>
Barrister	Barrister	k1gMnSc1
&	&	k?
Principal	Principal	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-210-6804-9	978-80-210-6804-9	k4
(	(	kIx(
<g/>
MU	MU	kA
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
MLADIČOVÁ	MLADIČOVÁ	kA
<g/>
,	,	kIx,
Iva	Iva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kotík	Kotík	k1gInSc1
(	(	kIx(
<g/>
1916	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Publikace	publikace	k1gFnSc1
věnovaná	věnovaný	k2eAgFnSc1d1
českému	český	k2eAgMnSc3d1
malíři	malíř	k1gMnSc3
Janu	Jan	k1gMnSc3
Kotíkovi	Kotík	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788070354735	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
KOTÍK	KOTÍK	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neúplný	úplný	k2eNgInSc1d1
kompas	kompas	k1gInSc1
–	–	k?
Jan	Jan	k1gMnSc1
Kotík	Kotík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Köln	Köln	k1gInSc1
<g/>
:	:	kIx,
Index	index	k1gInSc1
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
108	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
Květoslav	Květoslava	k1gFnPc2
Chvatík	Chvatík	k1gMnSc1
o	o	k7c6
Janu	Jan	k1gMnSc6
Kotíkovi	Kotík	k1gMnSc6
a	a	k8xC
českém	český	k2eAgNnSc6d1
moderním	moderní	k2eAgNnSc6d1
umění	umění	k1gNnSc6
Ivan	Ivan	k1gMnSc1
Bystřina	bystřina	k1gFnSc1
v	v	k7c6
rozhovoru	rozhovor	k1gInSc6
s	s	k7c7
Janem	Jan	k1gMnSc7
Kotíkem	Kotík	k1gMnSc7
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Jan	Jan	k1gMnSc1
Kotík	Kotík	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Kotík	Kotík	k1gMnSc1
v	v	k7c6
informačním	informační	k2eAgInSc6d1
systému	systém	k1gInSc6
abART	abART	k?
</s>
<s>
Rozhovor	rozhovor	k1gInSc1
s	s	k7c7
Janem	Jan	k1gMnSc7
Kotíkem	Kotík	k1gMnSc7
</s>
<s>
Životopis	životopis	k1gInSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Artlist	Artlist	k1gMnSc1
Jan	Jan	k1gMnSc1
Kotík	Kotík	k1gMnSc1
ARTLIST	ARTLIST	kA
–	–	k?
databáze	databáze	k1gFnSc1
současného	současný	k2eAgNnSc2d1
českého	český	k2eAgNnSc2d1
výtvarného	výtvarný	k2eAgNnSc2d1
umění	umění	k1gNnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Skupina	skupina	k1gFnSc1
42	#num#	k4
(	(	kIx(
<g/>
1942	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
Básníci	básník	k1gMnPc1
</s>
<s>
Ivan	Ivan	k1gMnSc1
Blatný	blatný	k2eAgMnSc1d1
•	•	k?
Jan	Jan	k1gMnSc1
Hanč	Hanča	k1gFnPc2
•	•	k?
Jiřina	Jiřina	k1gFnSc1
Hauková	Hauková	k1gFnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Kainar	Kainar	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Kolář	Kolář	k1gMnSc1
Malíři	malíř	k1gMnSc3
</s>
<s>
František	František	k1gMnSc1
Gross	Gross	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Hudeček	Hudeček	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Kotík	Kotík	k1gMnSc1
•	•	k?
Kamil	Kamil	k1gMnSc1
Lhoták	Lhoták	k1gMnSc1
•	•	k?
Bohumír	Bohumír	k1gMnSc1
Matal	Matal	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Smetana	Smetana	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Souček	Souček	k1gMnSc1
Další	další	k2eAgFnSc7d1
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Zívr	Zívr	k1gMnSc1
(	(	kIx(
<g/>
sochař	sochař	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Hák	Hák	k1gMnSc1
(	(	kIx(
<g/>
fotograf	fotograf	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Jindřich	Jindřich	k1gMnSc1
Chalupecký	Chalupecký	k2eAgMnSc1d1
(	(	kIx(
<g/>
teoretik	teoretik	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Jiří	Jiří	k1gMnSc1
Kotalík	Kotalík	k1gMnSc1
(	(	kIx(
<g/>
teoretik	teoretik	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1062155	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
119254409	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0927	#num#	k4
628X	628X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
83233511	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500100546	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
96425764	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
83233511	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Umění	umění	k1gNnSc1
</s>
