<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
sportovní	sportovní	k2eAgFnSc7d1	sportovní
událostí	událost	k1gFnSc7	událost
je	být	k5eAaImIp3nS	být
závod	závod	k1gInSc1	závod
světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
-	-	kIx~	-
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
lyže	lyže	k1gFnSc1	lyže
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
konec	konec	k1gInSc1	konec
ledna	leden	k1gInSc2	leden
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
jezdí	jezdit	k5eAaImIp3nS	jezdit
v	v	k7c6	v
malebném	malebný	k2eAgNnSc6d1	malebné
okolí	okolí	k1gNnSc6	okolí
hotelu	hotel	k1gInSc6	hotel
Ski	ski	k1gFnPc1	ski
v	v	k7c6	v
lese	les	k1gInSc6	les
Ochoza	Ochoza	k?	Ochoza
<g/>
.	.	kIx.	.
</s>
