<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
závod	závod	k1gInSc1	závod
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
pořádá	pořádat	k5eAaImIp3nS	pořádat
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
?	?	kIx.	?
</s>
