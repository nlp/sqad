<s>
20	[number]	k4	20
%	%	kIx~	%
Evropy	Evropa	k1gFnSc2	Evropa
je	být	k5eAaImIp3nS	být
odvodňováno	odvodňován	k2eAgNnSc1d1	odvodňován
do	do	k7c2	do
bezodtokého	bezodtoký	k2eAgNnSc2d1	bezodtoké
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
ústí	ústit	k5eAaImIp3nS	ústit
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
evropská	evropský	k2eAgFnSc1d1	Evropská
řeka	řeka	k1gFnSc1	řeka
Volha	Volha	k1gFnSc1	Volha
(	(	kIx(	(
<g/>
3531	[number]	k4	3531
km	km	kA	km
<g/>
,	,	kIx,	,
povodí	povodit	k5eAaPmIp3nS	povodit
1	[number]	k4	1
360	[number]	k4	360
000	[number]	k4	000
km2	km2	k4	km2
<g/>
,	,	kIx,	,
s	s	k7c7	s
průměrným	průměrný	k2eAgInSc7d1	průměrný
ročním	roční	k2eAgInSc7d1	roční
průtokem	průtok	k1gInSc7	průtok
8220	[number]	k4	8220
m3	m3	k4	m3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
