<s>
Svými	svůj	k3xOyFgInPc7	svůj
osmi	osm	k4xCc2	osm
dokončenými	dokončený	k2eAgFnPc7d1	dokončená
operami	opera	k1gFnPc7	opera
různých	různý	k2eAgInPc2d1	různý
žánrů	žánr	k1gInPc2	žánr
položil	položit	k5eAaPmAgInS	položit
základ	základ	k1gInSc1	základ
českého	český	k2eAgInSc2d1	český
repertoáru	repertoár	k1gInSc2	repertoár
<g/>
;	;	kIx,	;
největší	veliký	k2eAgFnSc2d3	veliký
popularity	popularita	k1gFnSc2	popularita
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
komická	komický	k2eAgFnSc1d1	komická
opera	opera	k1gFnSc1	opera
z	z	k7c2	z
venkovského	venkovský	k2eAgNnSc2d1	venkovské
prostředí	prostředí	k1gNnSc2	prostředí
Prodaná	Prodaná	k1gFnSc1	Prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
prototyp	prototyp	k1gInSc4	prototyp
české	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
