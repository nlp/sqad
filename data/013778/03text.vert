<s>
Pompejská	pompejský	k2eAgFnSc1d1
novéna	novéna	k1gFnSc1
</s>
<s>
Pompejská	pompejský	k2eAgFnSc1d1
novéna	novéna	k1gFnSc1
je	být	k5eAaImIp3nS
růžencová	růžencový	k2eAgFnSc1d1
modlitba	modlitba	k1gFnSc1
k	k	k7c3
Panně	Panna	k1gFnSc3
Marii	Maria	k1gFnSc3
z	z	k7c2
Pompejí	Pompeje	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
r.	r.	kA
1884	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
nazývaná	nazývaný	k2eAgFnSc1d1
také	také	k9
neodolatelnou	odolatelný	k2eNgFnSc7d1
novénou	novéna	k1gFnSc7
<g/>
,	,	kIx,
protože	protože	k8xS
Matka	matka	k1gFnSc1
Boží	božit	k5eAaImIp3nS
slíbila	slíbit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
pomodlí	pomodlit	k5eAaPmIp3nS
za	za	k7c4
konkrétní	konkrétní	k2eAgInSc4d1
úmysl	úmysl	k1gInSc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
ho	on	k3xPp3gMnSc4
obdrží	obdržet	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Začátek	začátek	k1gInSc1
Pompejské	pompejský	k2eAgFnSc2d1
novény	novéna	k1gFnSc2
se	se	k3xPyFc4
pojí	pojit	k5eAaImIp3nS,k5eAaPmIp3nS
se	s	k7c7
zjevením	zjevení	k1gNnSc7
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
těžce	těžce	k6eAd1
nemocné	nemocný	k2eAgFnSc3d1,k2eNgFnSc3d1
Fortunatině	Fortunatina	k1gFnSc3
Agrelli	Agrelle	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
jí	on	k3xPp3gFnSc3
sdělila	sdělit	k5eAaPmAgFnS
<g/>
,	,	kIx,
ať	ať	k9
se	s	k7c7
54	#num#	k4
dní	den	k1gInPc2
modlí	modlit	k5eAaImIp3nS
celý	celý	k2eAgInSc1d1
<g/>
(	(	kIx(
<g/>
3	#num#	k4
části	část	k1gFnPc1
<g/>
)	)	kIx)
růženec	růženec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
skončení	skončení	k1gNnSc6
novény	novéna	k1gFnSc2
se	se	k3xPyFc4
zázračně	zázračně	k6eAd1
uzdravila	uzdravit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
W	W	kA
r.	r.	kA
<g/>
1890	#num#	k4
papež	papež	k1gMnSc1
Lev	Lev	k1gMnSc1
XIII	XIII	kA
slavnostně	slavnostně	k6eAd1
uznal	uznat	k5eAaPmAgInS
pravost	pravost	k1gFnSc4
zázračného	zázračný	k2eAgNnSc2d1
uzdravení	uzdravení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pompejská	pompejský	k2eAgFnSc1d1
novéna	novéna	k1gFnSc1
byla	být	k5eAaImAgFnS
rozšířená	rozšířený	k2eAgFnSc1d1
díky	díky	k7c3
bl.	bl.	k?
Bartolu	Bartola	k1gFnSc4
Longovi	Long	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
horlivým	horlivý	k2eAgMnSc7d1
šiřitelem	šiřitel	k1gMnSc7
růžence	růženec	k1gInSc2
a	a	k8xC
mariánské	mariánský	k2eAgFnSc2d1
úcty	úcta	k1gFnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
Pompejí	Pompeje	k1gFnPc2
poblíž	poblíž	k7c2
Neapole	Neapol	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
r.	r.	kA
1872	#num#	k4
se	se	k3xPyFc4
tam	tam	k6eAd1
rozhodl	rozhodnout	k5eAaPmAgInS
začít	začít	k5eAaPmF
stavět	stavět	k5eAaImF
kostel	kostel	k1gInSc4
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Růžencové	růžencový	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
dostavět	dostavět	k5eAaPmF
v	v	k7c6
r.	r.	kA
1926	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
r.	r.	kA
1875	#num#	k4
byl	být	k5eAaImAgMnS
v	v	k7c6
ní	on	k3xPp3gFnSc6
umístěn	umístit	k5eAaPmNgInS
zázračný	zázračný	k2eAgInSc1d1
obraz	obraz	k1gInSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Pompejské	pompejský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgNnSc7d1
zázračným	zázračný	k2eAgNnSc7d1
uzdravením	uzdravení	k1gNnSc7
se	se	k3xPyFc4
událo	udát	k5eAaPmAgNnS
v	v	k7c6
r.	r.	kA
<g/>
1902	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marís	Marís	k1gInSc1
Lucía	Lucí	k1gInSc2
Calviñ	Calviñ	k6eAd1
byla	být	k5eAaImAgFnS
uzdravená	uzdravený	k2eAgFnSc1d1
po	po	k7c6
modlitbě	modlitba	k1gFnSc6
novény	novéna	k1gFnSc2
a	a	k8xC
pouti	pouť	k1gFnSc2
do	do	k7c2
pompejské	pompejský	k2eAgFnSc2d1
baziliky	bazilika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
r.	r.	kA
1885	#num#	k4
bylo	být	k5eAaImAgNnS
nahlášeno	nahlásit	k5eAaPmNgNnS
už	už	k6eAd1
okolo	okolo	k7c2
tisíce	tisíc	k4xCgInSc2
zázračných	zázračný	k2eAgFnPc2d1
uzdravení	uzdravení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Struktura	struktura	k1gFnSc1
novény	novéna	k1gFnSc2
</s>
<s>
Struktura	struktura	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
od	od	k7c2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
dala	dát	k5eAaPmAgFnS
instrukce	instrukce	k1gFnPc4
mladé	mladý	k2eAgFnSc3d1
dívce	dívka	k1gFnSc3
Fortunatině	Fortunatina	k1gFnSc3
Agrelli	Agrelle	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Doba	doba	k1gFnSc1
trvání	trvání	k1gNnSc2
je	být	k5eAaImIp3nS
54	#num#	k4
dnů	den	k1gInPc2
<g/>
,	,	kIx,
skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
totiž	totiž	k9
z	z	k7c2
3	#num#	k4
novén	novéna	k1gFnPc2
prosebných	prosebný	k2eAgFnPc2d1
a	a	k8xC
3	#num#	k4
novén	novéna	k1gFnPc2
děkovných	děkovný	k2eAgMnPc2d1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
každá	každý	k3xTgFnSc1
trvá	trvat	k5eAaImIp3nS
9	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
o	o	k7c4
27	#num#	k4
dní	den	k1gInPc2
prosebné	prosebný	k2eAgFnSc2d1
a	a	k8xC
27	#num#	k4
dní	den	k1gInPc2
děkovné	děkovný	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
novéna	novéna	k1gFnSc1
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
modlit	modlit	k5eAaImF
pouze	pouze	k6eAd1
za	za	k7c4
jeden	jeden	k4xCgInSc4
konkrétní	konkrétní	k2eAgInSc4d1
úmysl	úmysl	k1gInSc4
a	a	k8xC
pouze	pouze	k6eAd1
jednou	jeden	k4xCgFnSc7
osobou	osoba	k1gFnSc7
<g/>
,	,	kIx,
tzn.	tzn.	kA
nejde	jít	k5eNaImIp3nS
rozložit	rozložit	k5eAaPmF
růženec	růženec	k1gInSc4
mezi	mezi	k7c4
více	hodně	k6eAd2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotným	samotný	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
je	být	k5eAaImIp3nS
modlitba	modlitba	k1gFnSc1
3	#num#	k4
růženců	růženec	k1gInPc2
denně	denně	k6eAd1
<g/>
(	(	kIx(
<g/>
radostného	radostný	k2eAgMnSc2d1
<g/>
,	,	kIx,
bolestného	bolestný	k2eAgMnSc2d1
<g/>
,	,	kIx,
slavného	slavný	k2eAgNnSc2d1
<g/>
)	)	kIx)
společně	společně	k6eAd1
s	s	k7c7
krátkou	krátký	k2eAgFnSc7d1
prosebnou	prosebný	k2eAgFnSc7d1
<g/>
/	/	kIx~
<g/>
děkovnou	děkovný	k2eAgFnSc7d1
modlitbou	modlitba	k1gFnSc7
na	na	k7c6
konci	konec	k1gInSc6
každé	každý	k3xTgFnSc2
části	část	k1gFnSc2
růžence	růženec	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Přesná	přesný	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
společně	společně	k6eAd1
s	s	k7c7
textem	text	k1gInSc7
modliteb	modlitba	k1gFnPc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
mezi	mezi	k7c7
odkazy	odkaz	k1gInPc7
dole	dole	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děkovnou	děkovný	k2eAgFnSc4d1
část	část	k1gFnSc4
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
pomodlit	pomodlit	k5eAaPmF
se	se	k3xPyFc4
i	i	k9
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
prosba	prosba	k1gFnSc1
ještě	ještě	k6eAd1
nebyla	být	k5eNaImAgFnS
vyslyšena	vyslyšet	k5eAaPmNgFnS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
totiž	totiž	k9
integrální	integrální	k2eAgFnSc1d1
část	část	k1gFnSc1
novény	novéna	k1gFnSc2
<g/>
,	,	kIx,
nelze	lze	k6eNd1
ji	on	k3xPp3gFnSc4
rozbít	rozbít	k5eAaPmF
na	na	k7c4
dva	dva	k4xCgInPc4
díly	díl	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitou	důležitý	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
je	být	k5eAaImIp3nS
také	také	k9
důvěra	důvěra	k1gFnSc1
a	a	k8xC
odevzdání	odevzdání	k1gNnSc1
se	se	k3xPyFc4
do	do	k7c2
rukou	ruka	k1gFnPc2
Božích	boží	k2eAgFnPc2d1
prostřednictvím	prostřednictvím	k7c2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
http://www.fatym.com/storage/1480779177_sb_novena.pdf	http://www.fatym.com/storage/1480779177_sb_novena.pdf	k1gMnSc1
↑	↑	k?
http://www.maticecm.cz/?act=getdata&	http://www.maticecm.cz/?act=getdata&	k?
s.	s.	k?
<g/>
3	#num#	k4
<g/>
↑	↑	k?
Novéna	novéna	k1gFnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
z	z	k7c2
Pompejí	Pompeje	k1gFnPc2
←	←	k?
Patroni	patron	k1gMnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
http://www.fatym.com/storage/1480779177_sb_novena.pdf	http://www.fatym.com/storage/1480779177_sb_novena.pdf	k1gInSc1
Podrobná	podrobný	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
novény	novéna	k1gFnSc2
s	s	k7c7
textem	text	k1gInSc7
modliteb	modlitba	k1gFnPc2
</s>
<s>
https://saletinirozkvet.webnode.sk/products/panna-maria-z-pompeji/	https://saletinirozkvet.webnode.sk/products/panna-maria-z-pompeji/	k?
Podrobněji	podrobně	k6eAd2
o	o	k7c6
novéně	novéna	k1gFnSc6
</s>
<s>
http://patroni.cz/novena-panny-marie-z-pompeji/	http://patroni.cz/novena-panny-marie-z-pompeji/	k?
</s>
<s>
https://www.maticecm.cz/?act=obchod&	https://www.maticecm.cz/?act=obchod&	k?
Brožurka	brožurka	k1gFnSc1
s	s	k7c7
informacemi	informace	k1gFnPc7
o	o	k7c6
Pompejské	pompejský	k2eAgFnSc6d1
novéně	novéna	k1gFnSc6
<g/>
,	,	kIx,
Bartolu	Bartol	k1gInSc6
Longovi	Long	k1gMnSc3
atd.	atd.	kA
od	od	k7c2
Matice	matice	k1gFnSc2
cyrilometodějské	cyrilometodějský	k2eAgFnSc2d1
</s>
<s>
https://pompejanska.rosemaria.pl/	https://pompejanska.rosemaria.pl/	k?
Web	web	k1gInSc1
věnovaný	věnovaný	k2eAgInSc1d1
pompejské	pompejský	k2eAgFnSc3d1
novéně	novéna	k1gFnSc3
s	s	k7c7
tisícovkami	tisícovka	k1gFnPc7
svědectví	svědectví	k1gNnPc2
vyslyšených	vyslyšený	k2eAgFnPc2d1
modliteb	modlitba	k1gFnPc2
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
</s>
