<s>
Pompejská	pompejský	k2eAgFnSc1d1
novéna	novéna	k1gFnSc1
je	být	k5eAaImIp3nS
růžencová	růžencový	k2eAgFnSc1d1
modlitba	modlitba	k1gFnSc1
k	k	k7c3
Panně	Panna	k1gFnSc3
Marii	Maria	k1gFnSc3
z	z	k7c2
Pompejí	Pompeje	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
r.	r.	kA
1884	#num#	k4
<g/>
.	.	kIx.
</s>