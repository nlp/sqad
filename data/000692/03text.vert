<s>
DVD	DVD	kA	DVD
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Digital	Digital	kA	Digital
Versatile	Versatil	k1gMnSc5	Versatil
Disc	disco	k1gNnPc2	disco
nebo	nebo	k8xC	nebo
Digital	Digital	kA	Digital
Video	video	k1gNnSc1	video
Disc	disco	k1gNnPc2	disco
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
formát	formát	k1gInSc4	formát
digitálního	digitální	k2eAgInSc2d1	digitální
optického	optický	k2eAgInSc2d1	optický
datového	datový	k2eAgInSc2d1	datový
nosiče	nosič	k1gInSc2	nosič
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
filmy	film	k1gInPc4	film
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
obrazové	obrazový	k2eAgFnSc6d1	obrazová
a	a	k8xC	a
zvukové	zvukový	k2eAgFnSc6d1	zvuková
kvalitě	kvalita	k1gFnSc6	kvalita
nebo	nebo	k8xC	nebo
jiná	jiný	k2eAgNnPc4d1	jiné
data	datum	k1gNnPc4	datum
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
DVD	DVD	kA	DVD
byl	být	k5eAaImAgInS	být
kladen	klást	k5eAaImNgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
zpětnou	zpětný	k2eAgFnSc4d1	zpětná
kompatibilitu	kompatibilita	k1gFnSc4	kompatibilita
s	s	k7c7	s
CD	CD	kA	CD
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
DVD	DVD	kA	DVD
disk	disk	k1gInSc4	disk
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
<g/>
.	.	kIx.	.
</s>
<s>
DVD	DVD	kA	DVD
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
světa	svět	k1gInSc2	svět
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc4d1	oficiální
standard	standard	k1gInSc4	standard
zapisovatelných	zapisovatelný	k2eAgInPc2d1	zapisovatelný
<g/>
/	/	kIx~	/
<g/>
přepisovatelných	přepisovatelný	k2eAgInPc2d1	přepisovatelný
disků	disk	k1gInPc2	disk
DVD-R	DVD-R	k1gMnSc1	DVD-R
<g/>
(	(	kIx(	(
<g/>
W	W	kA	W
<g/>
)	)	kIx)	)
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
DVD	DVD	kA	DVD
Fórum	fórum	k1gNnSc1	fórum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Ceny	cena	k1gFnPc1	cena
licencí	licence	k1gFnPc2	licence
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
technologii	technologie	k1gFnSc4	technologie
však	však	k9	však
byly	být	k5eAaImAgFnP	být
tak	tak	k9	tak
vysoké	vysoký	k2eAgFnPc1d1	vysoká
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jiná	jiný	k2eAgFnSc1d1	jiná
skupina	skupina	k1gFnSc1	skupina
-	-	kIx~	-
DVD	DVD	kA	DVD
<g/>
+	+	kIx~	+
<g/>
RW	RW	kA	RW
Alliance	Alliance	k1gFnSc2	Alliance
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
standard	standard	k1gInSc4	standard
DVD	DVD	kA	DVD
<g/>
+	+	kIx~	+
<g/>
R	R	kA	R
<g/>
(	(	kIx(	(
<g/>
W	W	kA	W
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
licence	licence	k1gFnPc1	licence
byly	být	k5eAaImAgFnP	být
levnější	levný	k2eAgFnPc1d2	levnější
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
dokončením	dokončení	k1gNnSc7	dokončení
specifikace	specifikace	k1gFnSc2	specifikace
DVD	DVD	kA	DVD
byl	být	k5eAaImAgMnS	být
návrháři	návrhář	k1gMnSc3	návrhář
neoficiálně	oficiálně	k6eNd1	oficiálně
používán	používán	k2eAgInSc4d1	používán
název	název	k1gInSc4	název
Digital	Digital	kA	Digital
Video	video	k1gNnSc1	video
Disc	disco	k1gNnPc2	disco
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
digitální	digitální	k2eAgInSc1d1	digitální
videodisk	videodisk	k1gInSc1	videodisk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
dokončována	dokončován	k2eAgFnSc1d1	dokončována
specifikace	specifikace	k1gFnSc1	specifikace
formátu	formát	k1gInSc2	formát
<g/>
,	,	kIx,	,
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jeho	on	k3xPp3gNnSc2	on
daleko	daleko	k6eAd1	daleko
širších	široký	k2eAgFnPc2d2	širší
možností	možnost	k1gFnPc2	možnost
využití	využití	k1gNnSc2	využití
bude	být	k5eAaImBp3nS	být
oficiálně	oficiálně	k6eAd1	oficiálně
znít	znít	k5eAaImF	znít
Digital	Digital	kA	Digital
Versatile	Versatil	k1gMnSc5	Versatil
Disc	disco	k1gNnPc2	disco
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
digitální	digitální	k2eAgInSc4d1	digitální
víceúčelový	víceúčelový	k2eAgInSc4d1	víceúčelový
disk	disk	k1gInSc4	disk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hollywood	Hollywood	k1gInSc1	Hollywood
při	při	k7c6	při
distribuci	distribuce	k1gFnSc6	distribuce
a	a	k8xC	a
propagaci	propagace	k1gFnSc4	propagace
svých	svůj	k3xOyFgInPc2	svůj
filmů	film	k1gInPc2	film
na	na	k7c6	na
DVD	DVD	kA	DVD
používá	používat	k5eAaImIp3nS	používat
původní	původní	k2eAgInSc1d1	původní
neoficiální	oficiální	k2eNgInSc1d1	neoficiální
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
rozšířenější	rozšířený	k2eAgNnSc1d2	rozšířenější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
není	být	k5eNaImIp3nS	být
DVD	DVD	kA	DVD
žádnou	žádný	k3yNgFnSc7	žádný
zkratkou	zkratka	k1gFnSc7	zkratka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
média	médium	k1gNnSc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Média	médium	k1gNnPc1	médium
DVD	DVD	kA	DVD
jsou	být	k5eAaImIp3nP	být
plastové	plastový	k2eAgInPc1d1	plastový
disky	disk	k1gInPc1	disk
<g/>
,	,	kIx,	,
navenek	navenek	k6eAd1	navenek
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xC	jako
média	médium	k1gNnPc4	médium
CD	CD	kA	CD
<g/>
.	.	kIx.	.
</s>
<s>
Disky	disk	k1gInPc1	disk
DVD	DVD	kA	DVD
mají	mít	k5eAaImIp3nP	mít
průměr	průměr	k1gInSc1	průměr
120	[number]	k4	120
mm	mm	kA	mm
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
1,2	[number]	k4	1,2
mm	mm	kA	mm
silná	silný	k2eAgFnSc1d1	silná
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
se	se	k3xPyFc4	se
ukládají	ukládat	k5eAaImIp3nP	ukládat
pod	pod	k7c4	pod
povrch	povrch	k1gInSc4	povrch
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
nebo	nebo	k8xC	nebo
dvou	dva	k4xCgFnPc2	dva
vrstev	vrstva	k1gFnPc2	vrstva
ve	v	k7c6	v
stopě	stopa	k1gFnSc6	stopa
tvaru	tvar	k1gInSc2	tvar
spirály	spirála	k1gFnSc2	spirála
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
CD	CD	kA	CD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
dat	datum	k1gNnPc2	datum
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
laserové	laserový	k2eAgNnSc1d1	laserové
světlo	světlo	k1gNnSc1	světlo
s	s	k7c7	s
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
660	[number]	k4	660
nm	nm	k?	nm
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
kratší	krátký	k2eAgInPc1d2	kratší
než	než	k8xS	než
v	v	k7c6	v
případě	případ	k1gInSc6	případ
CD	CD	kA	CD
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
také	také	k9	také
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jejich	jejich	k3xOp3gFnSc4	jejich
vyšší	vysoký	k2eAgFnSc4d2	vyšší
kapacitu	kapacita	k1gFnSc4	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
příčný	příčný	k2eAgInSc1d1	příčný
odstup	odstup	k1gInSc1	odstup
stop	stopa	k1gFnPc2	stopa
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
-	-	kIx~	-
0,74	[number]	k4	0,74
μ	μ	k?	μ
oproti	oproti	k7c3	oproti
1,6	[number]	k4	1,6
μ	μ	k?	μ
u	u	k7c2	u
CD	CD	kA	CD
<g/>
.	.	kIx.	.
</s>
<s>
DVD	DVD	kA	DVD
oproti	oproti	k7c3	oproti
CD	CD	kA	CD
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
<g/>
:	:	kIx,	:
efektivnější	efektivní	k2eAgFnSc4d2	efektivnější
korekci	korekce	k1gFnSc4	korekce
chyb	chyba	k1gFnPc2	chyba
vyšší	vysoký	k2eAgFnSc4d2	vyšší
kapacitu	kapacita	k1gFnSc4	kapacita
záznamu	záznam	k1gInSc2	záznam
(	(	kIx(	(
<g/>
asi	asi	k9	asi
4,7	[number]	k4	4,7
GB	GB	kA	GB
<g/>
/	/	kIx~	/
<g/>
4,4	[number]	k4	4,4
GiB	GiB	k1gFnPc2	GiB
oproti	oproti	k7c3	oproti
0,7	[number]	k4	0,7
GB	GB	kA	GB
<g/>
)	)	kIx)	)
odlišný	odlišný	k2eAgInSc1d1	odlišný
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
Universal	Universal	k1gFnSc2	Universal
Disk	disk	k1gInSc1	disk
Format	Format	k1gInSc1	Format
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
není	být	k5eNaImIp3nS	být
zpětně	zpětně	k6eAd1	zpětně
kompatibilní	kompatibilní	k2eAgInSc1d1	kompatibilní
s	s	k7c7	s
ISO	ISO	kA	ISO
9660	[number]	k4	9660
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
CD-ROM	CD-ROM	k1gFnSc4	CD-ROM
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
mechaniky	mechanika	k1gFnSc2	mechanika
typu	typ	k1gInSc2	typ
DVD	DVD	kA	DVD
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
jako	jako	k9	jako
násobek	násobek	k1gInSc4	násobek
1350	[number]	k4	1350
kB	kb	kA	kb
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mechanika	mechanika	k1gFnSc1	mechanika
s	s	k7c7	s
rychlostí	rychlost	k1gFnSc7	rychlost
16	[number]	k4	16
<g/>
×	×	k?	×
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přenosovou	přenosový	k2eAgFnSc4d1	přenosová
rychlost	rychlost	k1gFnSc4	rychlost
16	[number]	k4	16
×	×	k?	×
1350	[number]	k4	1350
=	=	kIx~	=
21600	[number]	k4	21600
kB	kb	kA	kb
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
21,09	[number]	k4	21,09
MB	MB	kA	MB
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Médium	médium	k1gNnSc1	médium
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zápis	zápis	k1gInSc4	zápis
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
nebo	nebo	k8xC	nebo
obě	dva	k4xCgFnPc1	dva
dvě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
nebo	nebo	k8xC	nebo
dvou	dva	k4xCgMnPc6	dva
vrstvách	vrstva	k1gFnPc6	vrstva
na	na	k7c4	na
každou	každý	k3xTgFnSc4	každý
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počtu	počet	k1gInSc6	počet
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
vrstev	vrstva	k1gFnPc2	vrstva
závisí	záviset	k5eAaImIp3nS	záviset
kapacita	kapacita	k1gFnSc1	kapacita
média	médium	k1gNnSc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
DVD-	DVD-	k?	DVD-
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
jedna	jeden	k4xCgFnSc1	jeden
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
kapacita	kapacita	k1gFnSc1	kapacita
4,7	[number]	k4	4,7
GB	GB	kA	GB
(	(	kIx(	(
<g/>
4,38	[number]	k4	4,38
GiB	GiB	k1gFnPc2	GiB
<g/>
)	)	kIx)	)
DVD-	DVD-	k1gFnSc1	DVD-
<g/>
9	[number]	k4	9
<g/>
:	:	kIx,	:
jedna	jeden	k4xCgFnSc1	jeden
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
,	,	kIx,	,
8,5	[number]	k4	8,5
GB	GB	kA	GB
(	(	kIx(	(
<g/>
7,92	[number]	k4	7,92
GiB	GiB	k1gFnPc2	GiB
<g/>
)	)	kIx)	)
DVD-	DVD-	k1gFnSc1	DVD-
<g/>
10	[number]	k4	10
<g/>
:	:	kIx,	:
dvě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
vrstva	vrstva	k1gFnSc1	vrstva
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
9,4	[number]	k4	9,4
GB	GB	kA	GB
(	(	kIx(	(
<g/>
8,75	[number]	k4	8,75
GiB	GiB	k1gFnPc2	GiB
<g/>
)	)	kIx)	)
DVD-	DVD-	k1gFnSc1	DVD-
<g/>
14	[number]	k4	14
<g/>
:	:	kIx,	:
dvě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
vrstvy	vrstva	k1gFnPc1	vrstva
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
vrstva	vrstva	k1gFnSc1	vrstva
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
<g/>
,	,	kIx,	,
13,2	[number]	k4	13,2
GB	GB	kA	GB
(	(	kIx(	(
<g/>
12,3	[number]	k4	12,3
GiB	GiB	k1gFnPc2	GiB
<g/>
)	)	kIx)	)
DVD-	DVD-	k1gFnSc1	DVD-
<g/>
18	[number]	k4	18
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
dvě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
vrstvy	vrstva	k1gFnPc1	vrstva
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
17,1	[number]	k4	17,1
GB	GB	kA	GB
(	(	kIx(	(
<g/>
15,9	[number]	k4	15,9
GiB	GiB	k1gMnSc1	GiB
<g/>
)	)	kIx)	)
Uživatel	uživatel	k1gMnSc1	uživatel
může	moct	k5eAaImIp3nS	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
DVD-nosiče	DVDosič	k1gInPc4	DVD-nosič
těchto	tento	k3xDgMnPc2	tento
typů	typ	k1gInPc2	typ
<g/>
:	:	kIx,	:
DVD-Video	DVD-Video	k6eAd1	DVD-Video
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
filmy	film	k1gInPc4	film
(	(	kIx(	(
<g/>
obraz	obraz	k1gInSc4	obraz
a	a	k8xC	a
zvuk	zvuk	k1gInSc4	zvuk
<g/>
))	))	k?	))
DVD-Audio	DVD-Audio	k6eAd1	DVD-Audio
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zvuk	zvuk	k1gInSc4	zvuk
v	v	k7c6	v
kvalitě	kvalita	k1gFnSc6	kvalita
CD	CD	kA	CD
a	a	k8xC	a
lepší	dobrý	k2eAgFnSc7d2	lepší
<g/>
)	)	kIx)	)
DVD	DVD	kA	DVD
Data	datum	k1gNnSc2	datum
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
údaje	údaj	k1gInPc4	údaj
<g/>
)	)	kIx)	)
Označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
+	+	kIx~	+
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
plus	plus	k1gInSc1	plus
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
-	-	kIx~	-
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
mínus	mínus	k1gInSc1	mínus
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
dva	dva	k4xCgInPc4	dva
různé	různý	k2eAgInPc4d1	různý
technické	technický	k2eAgInPc4d1	technický
standardy	standard	k1gInPc4	standard
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
kompatibilní	kompatibilní	k2eAgFnSc1d1	kompatibilní
<g/>
.	.	kIx.	.
</s>
<s>
Médium	médium	k1gNnSc1	médium
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
typu	typ	k1gInSc3	typ
<g/>
:	:	kIx,	:
DVD-ROM	DVD-ROM	k1gFnSc1	DVD-ROM
(	(	kIx(	(
<g/>
Read	Read	k1gInSc1	Read
Only	Onla	k1gFnSc2	Onla
Memory	Memora	k1gFnSc2	Memora
<g/>
,	,	kIx,	,
paměť	paměť	k1gFnSc4	paměť
jen	jen	k9	jen
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
<g/>
,	,	kIx,	,
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
lisováním	lisování	k1gNnSc7	lisování
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pomyslný	pomyslný	k2eAgMnSc1d1	pomyslný
nástupce	nástupce	k1gMnSc1	nástupce
formátu	formát	k1gInSc2	formát
CD-ROM	CD-ROM	k1gFnSc2	CD-ROM
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
víceúčelový	víceúčelový	k2eAgInSc4d1	víceúčelový
formát	formát	k1gInSc4	formát
pro	pro	k7c4	pro
přehrávání	přehrávání	k1gNnSc4	přehrávání
počítačových	počítačový	k2eAgNnPc2d1	počítačové
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
multimediálních	multimediální	k2eAgFnPc2d1	multimediální
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Čtení	čtení	k1gNnSc1	čtení
DVD	DVD	kA	DVD
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ve	v	k7c6	v
všech	všecek	k3xTgInPc2	všecek
PC	PC	kA	PC
(	(	kIx(	(
<g/>
a	a	k8xC	a
ostatních	ostatní	k2eAgFnPc2d1	ostatní
platforem	platforma	k1gFnPc2	platforma
<g/>
)	)	kIx)	)
vybavených	vybavený	k2eAgInPc2d1	vybavený
jednotkou	jednotka	k1gFnSc7	jednotka
DVD	DVD	kA	DVD
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
logického	logický	k2eAgInSc2d1	logický
formátu	formát	k1gInSc2	formát
UDF	UDF	kA	UDF
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc1	tři
typy	typ	k1gInPc1	typ
zapisovatelných	zapisovatelný	k2eAgInPc2d1	zapisovatelný
a	a	k8xC	a
přepisovatelných	přepisovatelný	k2eAgInPc2d1	přepisovatelný
DVD	DVD	kA	DVD
disků	disk	k1gInPc2	disk
<g/>
:	:	kIx,	:
DVD-R	DVD-R	k1gMnSc1	DVD-R
<g/>
/	/	kIx~	/
<g/>
RW	RW	kA	RW
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
<g/>
+	+	kIx~	+
<g/>
R	R	kA	R
<g/>
/	/	kIx~	/
<g/>
RW	RW	kA	RW
(	(	kIx(	(
<g/>
plus	plus	k1gInSc1	plus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
DVD-RAM	DVD-RAM	k1gFnSc4	DVD-RAM
<g/>
.	.	kIx.	.
</s>
<s>
DVD	DVD	kA	DVD
<g/>
+	+	kIx~	+
<g/>
R	R	kA	R
<g/>
/	/	kIx~	/
<g/>
RW	RW	kA	RW
(	(	kIx(	(
<g/>
R	R	kA	R
=	=	kIx~	=
Recordable	Recordable	k1gMnSc2	Recordable
<g/>
,	,	kIx,	,
jen	jen	k9	jen
pro	pro	k7c4	pro
jeden	jeden	k4xCgInSc4	jeden
zápis	zápis	k1gInSc4	zápis
<g/>
,	,	kIx,	,
RW	RW	kA	RW
=	=	kIx~	=
ReWritable	ReWritable	k1gMnSc1	ReWritable
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
přepisování	přepisování	k1gNnSc4	přepisování
<g/>
)	)	kIx)	)
Formát	formát	k1gInSc1	formát
DVD	DVD	kA	DVD
<g/>
+	+	kIx~	+
<g/>
R	R	kA	R
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
široce	široko	k6eAd1	široko
rozšířenými	rozšířený	k2eAgInPc7d1	rozšířený
formáty	formát	k1gInPc7	formát
nejmladší	mladý	k2eAgMnPc1d3	nejmladší
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
mladší	mladý	k2eAgMnSc1d2	mladší
než	než	k8xS	než
formát	formát	k1gInSc1	formát
DVD	DVD	kA	DVD
<g/>
+	+	kIx~	+
<g/>
RW	RW	kA	RW
<g/>
.	.	kIx.	.
</s>
<s>
Disky	disk	k1gInPc1	disk
DVD	DVD	kA	DVD
<g/>
+	+	kIx~	+
<g/>
R	R	kA	R
lze	lze	k6eAd1	lze
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
běžně	běžně	k6eAd1	běžně
zapisovat	zapisovat	k5eAaImF	zapisovat
osminásobnou	osminásobný	k2eAgFnSc7d1	osminásobná
rychlostí	rychlost	k1gFnSc7	rychlost
oproti	oproti	k7c3	oproti
standardní	standardní	k2eAgFnSc3d1	standardní
rychlosti	rychlost	k1gFnSc3	rychlost
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
10	[number]	k4	10
800	[number]	k4	800
kB	kb	kA	kb
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
rychlostí	rychlost	k1gFnSc7	rychlost
trvá	trvat	k5eAaImIp3nS	trvat
zápis	zápis	k1gInSc1	zápis
na	na	k7c4	na
disk	disk	k1gInSc4	disk
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
DVD	DVD	kA	DVD
<g/>
+	+	kIx~	+
<g/>
RW	RW	kA	RW
je	být	k5eAaImIp3nS	být
přepisovatelná	přepisovatelný	k2eAgFnSc1d1	přepisovatelná
verze	verze	k1gFnSc1	verze
formátu	formát	k1gInSc2	formát
DVD	DVD	kA	DVD
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1	standardní
rychlost	rychlost	k1gFnSc1	rychlost
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
médium	médium	k1gNnSc4	médium
je	být	k5eAaImIp3nS	být
čtyřnásobná	čtyřnásobný	k2eAgFnSc1d1	čtyřnásobná
oproti	oproti	k7c3	oproti
základní	základní	k2eAgFnSc3d1	základní
rychlosti	rychlost	k1gFnSc3	rychlost
čtení	čtení	k1gNnSc2	čtení
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
mechaniky	mechanika	k1gFnPc1	mechanika
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
schopny	schopen	k2eAgFnPc1d1	schopna
zapisovat	zapisovat	k5eAaImF	zapisovat
DVD-R	DVD-R	k1gFnSc4	DVD-R
<g/>
,	,	kIx,	,
DVD-RW	DVD-RW	k1gFnSc4	DVD-RW
<g/>
,	,	kIx,	,
CD-R	CD-R	k1gFnSc4	CD-R
a	a	k8xC	a
CD-RW	CD-RW	k1gFnSc4	CD-RW
<g/>
,	,	kIx,	,
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
firma	firma	k1gFnSc1	firma
Pioneer	Pioneer	kA	Pioneer
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Kompatibilita	kompatibilita	k1gFnSc1	kompatibilita
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
<g/>
,	,	kIx,	,
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
chybné	chybný	k2eAgFnSc3d1	chybná
identifikaci	identifikace	k1gFnSc3	identifikace
medií	medium	k1gNnPc2	medium
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
odrazivostí	odrazivost	k1gFnSc7	odrazivost
<g/>
.	.	kIx.	.
</s>
<s>
DVD	DVD	kA	DVD
<g/>
+	+	kIx~	+
<g/>
R	R	kA	R
DL	DL	kA	DL
(	(	kIx(	(
<g/>
R	R	kA	R
=	=	kIx~	=
Recordable	Recordable	k1gMnSc2	Recordable
<g/>
,	,	kIx,	,
jen	jen	k9	jen
pro	pro	k7c4	pro
jeden	jeden	k4xCgInSc4	jeden
zápis	zápis	k1gInSc4	zápis
<g/>
,	,	kIx,	,
DL	DL	kA	DL
=	=	kIx~	=
DualLayer	DualLayer	k1gMnSc1	DualLayer
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
)	)	kIx)	)
DVD-R	DVD-R	k1gMnSc1	DVD-R
<g/>
/	/	kIx~	/
<g/>
RW	RW	kA	RW
(	(	kIx(	(
<g/>
R	R	kA	R
=	=	kIx~	=
Recordable	Recordable	k1gMnSc2	Recordable
<g/>
,	,	kIx,	,
jen	jen	k9	jen
pro	pro	k7c4	pro
jeden	jeden	k4xCgInSc4	jeden
zápis	zápis	k1gInSc4	zápis
<g/>
,	,	kIx,	,
RW	RW	kA	RW
=	=	kIx~	=
ReWritable	ReWritable	k1gMnSc1	ReWritable
<g/>
,	,	kIx,	,
na	na	k7c4	na
přepisování	přepisování	k1gNnSc4	přepisování
<g/>
)	)	kIx)	)
Formát	formát	k1gInSc1	formát
DVD-R	DVD-R	k1gFnSc2	DVD-R
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
technologie	technologie	k1gFnSc2	technologie
klasického	klasický	k2eAgInSc2d1	klasický
kompaktního	kompaktní	k2eAgInSc2d1	kompaktní
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
tedy	tedy	k9	tedy
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
verzích	verze	k1gFnPc6	verze
-	-	kIx~	-
verze	verze	k1gFnSc1	verze
R	R	kA	R
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
lze	lze	k6eAd1	lze
pouze	pouze	k6eAd1	pouze
zapisovat	zapisovat	k5eAaImF	zapisovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
verze	verze	k1gFnSc1	verze
RW	RW	kA	RW
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
lze	lze	k6eAd1	lze
přepisovat	přepisovat	k5eAaImF	přepisovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
formát	formát	k1gInSc1	formát
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
co	co	k9	co
nejkompatibilnější	kompatibilní	k2eAgFnSc4d3	nejkompatibilnější
s	s	k7c7	s
lisovanými	lisovaný	k2eAgInPc7d1	lisovaný
disky	disk	k1gInPc7	disk
DVD	DVD	kA	DVD
(	(	kIx(	(
<g/>
DVD-ROM	DVD-ROM	k1gMnSc1	DVD-ROM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plyne	plynout	k5eAaImIp3nS	plynout
výhoda	výhoda	k1gFnSc1	výhoda
tohoto	tento	k3xDgInSc2	tento
formátu	formát	k1gInSc2	formát
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
kompatibilita	kompatibilita	k1gFnSc1	kompatibilita
se	s	k7c7	s
staršími	starý	k2eAgFnPc7d2	starší
mechanikami	mechanika	k1gFnPc7	mechanika
a	a	k8xC	a
přehrávači	přehrávač	k1gInPc7	přehrávač
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
na	na	k7c6	na
DVD	DVD	kA	DVD
zapisovat	zapisovat	k5eAaImF	zapisovat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
výhoda	výhoda	k1gFnSc1	výhoda
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc1	všechen
vyráběné	vyráběný	k2eAgInPc1d1	vyráběný
přehrávače	přehrávač	k1gInPc1	přehrávač
a	a	k8xC	a
mechaniky	mechanika	k1gFnPc1	mechanika
dokáží	dokázat	k5eAaPmIp3nP	dokázat
přehrávat	přehrávat	k5eAaImF	přehrávat
jak	jak	k8xC	jak
DVD-R	DVD-R	k1gFnSc4	DVD-R
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
DVD	DVD	kA	DVD
<g/>
+	+	kIx~	+
<g/>
R	R	kA	R
formáty	formát	k1gInPc1	formát
<g/>
.	.	kIx.	.
</s>
<s>
DVD-RAM	DVD-RAM	k?	DVD-RAM
-	-	kIx~	-
Random	Random	k1gInSc1	Random
Access	Accessa	k1gFnPc2	Accessa
Memory	Memora	k1gFnSc2	Memora
<g/>
,	,	kIx,	,
libovolně	libovolně	k6eAd1	libovolně
přepisovatelné	přepisovatelný	k2eAgNnSc4d1	přepisovatelné
médium	médium	k1gNnSc4	médium
-	-	kIx~	-
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
pracovat	pracovat	k5eAaImF	pracovat
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xS	jako
s	s	k7c7	s
pevným	pevný	k2eAgNnSc7d1	pevné
diskem	disco	k1gNnSc7	disco
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
Hamburskou	hamburský	k2eAgFnSc7d1	hamburská
firmou	firma	k1gFnSc7	firma
Optical	Optical	k1gFnSc7	Optical
Disc	disco	k1gNnPc2	disco
Service	Service	k1gFnSc2	Service
patentován	patentovat	k5eAaBmNgMnS	patentovat
a	a	k8xC	a
uveden	uvést	k5eAaPmNgMnS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
tzv.	tzv.	kA	tzv.
EcoDisc	EcoDisc	k1gInSc1	EcoDisc
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
uváděn	uvádět	k5eAaImNgMnS	uvádět
také	také	k9	také
jako	jako	k9	jako
EcoDisk	EcoDisk	k1gInSc4	EcoDisk
<g/>
.	.	kIx.	.
</s>
<s>
Formát	formát	k1gInSc1	formát
dat	datum	k1gNnPc2	datum
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xC	jako
běžné	běžný	k2eAgInPc1d1	běžný
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vylepšené	vylepšený	k2eAgNnSc1d1	vylepšené
některé	některý	k3yIgFnPc4	některý
mechanické	mechanický	k2eAgFnPc4d1	mechanická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
nižší	nízký	k2eAgFnSc4d2	nižší
kompatibilitu	kompatibilita	k1gFnSc4	kompatibilita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
z	z	k7c2	z
polovičního	poloviční	k2eAgNnSc2d1	poloviční
množství	množství	k1gNnSc2	množství
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
polykarbonátu	polykarbonát	k1gInSc2	polykarbonát
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
tak	tak	k6eAd1	tak
poloviční	poloviční	k2eAgFnSc4d1	poloviční
tloušťku	tloušťka	k1gFnSc4	tloušťka
(	(	kIx(	(
<g/>
0,6	[number]	k4	0,6
mm	mm	kA	mm
<g/>
)	)	kIx)	)
a	a	k8xC	a
poloviční	poloviční	k2eAgFnSc4d1	poloviční
hmotnost	hmotnost	k1gFnSc4	hmotnost
(	(	kIx(	(
<g/>
8	[number]	k4	8
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ohebnější	ohebný	k2eAgFnSc1d2	ohebnější
a	a	k8xC	a
odolnější	odolný	k2eAgFnSc1d2	odolnější
proti	proti	k7c3	proti
poškrábání	poškrábání	k1gNnSc3	poškrábání
<g/>
.	.	kIx.	.
</s>
<s>
Uváděná	uváděný	k2eAgFnSc1d1	uváděná
životnost	životnost	k1gFnSc1	životnost
je	být	k5eAaImIp3nS	být
200	[number]	k4	200
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
kompatibilní	kompatibilní	k2eAgFnSc1d1	kompatibilní
se	s	k7c7	s
štěrbinovými	štěrbinový	k2eAgFnPc7d1	štěrbinová
mechanikami	mechanika	k1gFnPc7	mechanika
a	a	k8xC	a
nekompatibilita	nekompatibilita	k1gFnSc1	nekompatibilita
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
malou	malý	k2eAgFnSc7d1	malá
grafickou	grafický	k2eAgFnSc7d1	grafická
ikonou	ikona	k1gFnSc7	ikona
na	na	k7c6	na
disku	disk	k1gInSc6	disk
<g/>
.	.	kIx.	.
</s>
