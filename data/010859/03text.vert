<p>
<s>
Theremin	Theremin	k1gInSc1	Theremin
(	(	kIx(	(
<g/>
také	také	k9	také
těremin	těremin	k1gInSc1	těremin
<g/>
,	,	kIx,	,
thereminvox	thereminvox	k1gInSc1	thereminvox
<g/>
,	,	kIx,	,
termenvox	termenvox	k1gInSc1	termenvox
<g/>
,	,	kIx,	,
aetherophon	aetherophon	k1gInSc1	aetherophon
<g/>
,	,	kIx,	,
etherophon	etherophon	k1gInSc1	etherophon
<g/>
,	,	kIx,	,
éterové	éterový	k2eAgFnPc1d1	éterová
vlny	vlna	k1gFnPc1	vlna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
elektronický	elektronický	k2eAgInSc1d1	elektronický
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
Lev	Lev	k1gMnSc1	Lev
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Těrmen	Těrmen	k2eAgMnSc1d1	Těrmen
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
též	též	k9	též
jako	jako	k9	jako
Léon	Léon	k1gInSc1	Léon
Theremin	Theremin	k2eAgInSc1d1	Theremin
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
hráč	hráč	k1gMnSc1	hráč
jakkoli	jakkoli	k6eAd1	jakkoli
dotýkal	dotýkat	k5eAaImAgMnS	dotýkat
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
na	na	k7c4	na
theremin	theremin	k1gInSc4	theremin
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
výjimečně	výjimečně	k6eAd1	výjimečně
náročná	náročný	k2eAgFnSc1d1	náročná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Theremin	Theremin	k1gInSc1	Theremin
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dřevěnou	dřevěný	k2eAgFnSc7d1	dřevěná
skříní	skříň	k1gFnSc7	skříň
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
anténami	anténa	k1gFnPc7	anténa
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ovládán	ovládat	k5eAaImNgInS	ovládat
pouze	pouze	k6eAd1	pouze
pohybem	pohyb	k1gInSc7	pohyb
paží	paže	k1gFnPc2	paže
<g/>
,	,	kIx,	,
dlaní	dlaň	k1gFnPc2	dlaň
a	a	k8xC	a
prstů	prst	k1gInPc2	prst
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
ruky	ruka	k1gFnSc2	ruka
od	od	k7c2	od
příslušné	příslušný	k2eAgFnSc2d1	příslušná
antény	anténa	k1gFnSc2	anténa
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
vlastnost	vlastnost	k1gFnSc1	vlastnost
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Pravá	pravý	k2eAgFnSc1d1	pravá
ruka	ruka	k1gFnSc1	ruka
ovládá	ovládat	k5eAaImIp3nS	ovládat
výšku	výška	k1gFnSc4	výška
tónu	tón	k1gInSc2	tón
(	(	kIx(	(
<g/>
frekvenci	frekvence	k1gFnSc4	frekvence
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
levá	levý	k2eAgFnSc1d1	levá
jeho	jeho	k3xOp3gFnSc4	jeho
sílu	síla	k1gFnSc4	síla
(	(	kIx(	(
<g/>
amplitudu	amplituda	k1gFnSc4	amplituda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvuková	zvukový	k2eAgFnSc1d1	zvuková
barva	barva	k1gFnSc1	barva
thereminu	theremin	k1gInSc2	theremin
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
zvuku	zvuk	k1gInSc2	zvuk
smyčcových	smyčcový	k2eAgInPc2d1	smyčcový
nástrojů	nástroj	k1gInPc2	nástroj
nebo	nebo	k8xC	nebo
lidskému	lidský	k2eAgInSc3d1	lidský
hlasu	hlas	k1gInSc3	hlas
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
prvních	první	k4xOgInPc2	první
nástrojů	nástroj	k1gInPc2	nástroj
byla	být	k5eAaImAgFnS	být
zvuková	zvukový	k2eAgFnSc1d1	zvuková
barva	barva	k1gFnSc1	barva
neměnná	měnný	k2eNgFnSc1d1	neměnná
<g/>
,	,	kIx,	,
u	u	k7c2	u
novějších	nový	k2eAgInPc2d2	novější
nástrojů	nástroj	k1gInPc2	nástroj
ji	on	k3xPp3gFnSc4	on
často	často	k6eAd1	často
lze	lze	k6eAd1	lze
regulovat	regulovat	k5eAaImF	regulovat
<g/>
.	.	kIx.	.
</s>
<s>
Zvukový	zvukový	k2eAgInSc1d1	zvukový
projev	projev	k1gInSc1	projev
thereminu	theremin	k1gInSc2	theremin
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
typické	typický	k2eAgNnSc1d1	typické
glissando	glissando	k1gNnSc1	glissando
a	a	k8xC	a
vibráto	vibráto	k1gNnSc1	vibráto
<g/>
.	.	kIx.	.
</s>
<s>
Staccato	staccato	k6eAd1	staccato
lze	lze	k6eAd1	lze
zahrát	zahrát	k5eAaPmF	zahrát
také	také	k9	také
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
vyrábí	vyrábět	k5eAaImIp3nP	vyrábět
thereminy	theremin	k1gInPc7	theremin
např.	např.	kA	např.
americká	americký	k2eAgFnSc1d1	americká
firma	firma	k1gFnSc1	firma
Moog	Mooga	k1gFnPc2	Mooga
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
Etherwave	Etherwav	k1gInSc5	Etherwav
lze	lze	k6eAd1	lze
zakoupit	zakoupit	k5eAaPmF	zakoupit
také	také	k9	také
jako	jako	k8xC	jako
stavebnici	stavebnice	k1gFnSc4	stavebnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
občasného	občasný	k2eAgNnSc2d1	občasné
využití	využití	k1gNnSc2	využití
ve	v	k7c6	v
vážné	vážný	k2eAgFnSc6d1	vážná
hudbě	hudba	k1gFnSc6	hudba
se	se	k3xPyFc4	se
theremin	theremin	k1gInSc1	theremin
objevuje	objevovat	k5eAaImIp3nS	objevovat
především	především	k9	především
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
experimentální	experimentální	k2eAgFnSc6d1	experimentální
<g/>
,	,	kIx,	,
populární	populární	k2eAgFnSc3d1	populární
a	a	k8xC	a
filmové	filmový	k2eAgFnSc3d1	filmová
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
používání	používání	k1gNnSc1	používání
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
k	k	k7c3	k
hororům	horor	k1gInPc3	horor
a	a	k8xC	a
fantastickým	fantastický	k2eAgInPc3d1	fantastický
filmům	film	k1gInPc3	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Theremin	Theremin	k1gInSc4	Theremin
používali	používat	k5eAaImAgMnP	používat
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
skladbách	skladba	k1gFnPc6	skladba
někteří	některý	k3yIgMnPc1	některý
skladatelé	skladatel	k1gMnPc1	skladatel
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
Edgard	Edgard	k1gMnSc1	Edgard
Varè	Varè	k1gMnSc1	Varè
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
Schnittke	Schnittk	k1gFnSc2	Schnittk
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Cage	Cag	k1gFnSc2	Cag
a	a	k8xC	a
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Martinů	Martinů	k1gMnSc1	Martinů
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgMnPc3d3	nejznámější
hráčům	hráč	k1gMnPc3	hráč
patří	patřit	k5eAaImIp3nS	patřit
Clara	Clara	k1gFnSc1	Clara
Rockmore	Rockmor	k1gInSc5	Rockmor
<g/>
,	,	kIx,	,
Lydia	Lydia	k1gFnSc1	Lydia
Kavina	Kavina	k1gFnSc1	Kavina
<g/>
,	,	kIx,	,
Pamelia	Pamelia	k1gFnSc1	Pamelia
Kurstin	Kurstin	k1gMnSc1	Kurstin
a	a	k8xC	a
Konstantin	Konstantin	k1gMnSc1	Konstantin
Kovalsky	Kovalsky	k1gMnSc1	Kovalsky
<g/>
.	.	kIx.	.
</s>
<s>
Theremin	Theremin	k1gInSc1	Theremin
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
např.	např.	kA	např.
v	v	k7c6	v
nahrávkách	nahrávka	k1gFnPc6	nahrávka
skupin	skupina	k1gFnPc2	skupina
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
(	(	kIx(	(
<g/>
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Whole	Whole	k1gInSc1	Whole
Lotta	Lott	k1gInSc2	Lott
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
Ven	ven	k6eAd1	ven
Robert	Robert	k1gMnSc1	Robert
Hal	halit	k5eAaImRp2nS	halit
<g/>
,	,	kIx,	,
Riverside	Riversid	k1gMnSc5	Riversid
<g/>
,	,	kIx,	,
Portishead	Portishead	k1gInSc1	Portishead
<g/>
,	,	kIx,	,
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gFnSc2	People
of	of	k?	of
the	the	k?	the
Universe	Universe	k1gFnSc2	Universe
či	či	k8xC	či
u	u	k7c2	u
Toma	Tom	k1gMnSc2	Tom
Waitse	Waits	k1gMnSc2	Waits
nebo	nebo	k8xC	nebo
Jean-Michel	Jean-Michel	k1gInSc4	Jean-Michel
Jarre	Jarr	k1gMnSc5	Jarr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
koncertech	koncert	k1gInPc6	koncert
ho	on	k3xPp3gNnSc4	on
používá	používat	k5eAaImIp3nS	používat
kytarista	kytarista	k1gMnSc1	kytarista
Joe	Joe	k1gMnSc1	Joe
Bonamassa	Bonamassa	k1gFnSc1	Bonamassa
<g/>
.	.	kIx.	.
</s>
<s>
Theremin	Theremin	k1gInSc1	Theremin
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
též	též	k9	též
americkou	americký	k2eAgFnSc4d1	americká
skupinu	skupina	k1gFnSc4	skupina
Beach	Beacha	k1gFnPc2	Beacha
Boys	boy	k1gMnPc2	boy
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
skladbu	skladba	k1gFnSc4	skladba
Good	Gooda	k1gFnPc2	Gooda
Vibrations	Vibrations	k1gInSc4	Vibrations
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
nechala	nechat	k5eAaPmAgFnS	nechat
postavit	postavit	k5eAaPmF	postavit
snáze	snadno	k6eAd2	snadno
ovladatelný	ovladatelný	k2eAgInSc1d1	ovladatelný
nástroj	nástroj	k1gInSc1	nástroj
s	s	k7c7	s
podobným	podobný	k2eAgInSc7d1	podobný
charakterem	charakter	k1gInSc7	charakter
zvuku	zvuk	k1gInSc2	zvuk
Tannerin	Tannerin	k1gInSc1	Tannerin
(	(	kIx(	(
<g/>
Electro-theremin	Electroheremin	k1gInSc1	Electro-theremin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Theremin	Theremin	k1gInSc1	Theremin
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
i	i	k9	i
ve	v	k7c6	v
znělce	znělka	k1gFnSc6	znělka
seriálu	seriál	k1gInSc2	seriál
Vraždy	vražda	k1gFnSc2	vražda
v	v	k7c6	v
Midsomeru	Midsomer	k1gInSc6	Midsomer
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hraje	hrát	k5eAaImIp3nS	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
melodii	melodie	k1gFnSc4	melodie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
byla	být	k5eAaImAgNnP	být
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
složena	složen	k2eAgFnSc1d1	složena
úvodní	úvodní	k2eAgFnSc1d1	úvodní
melodie	melodie	k1gFnSc1	melodie
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
The	The	k1gFnSc2	The
Legend	legenda	k1gFnPc2	legenda
of	of	k?	of
Zelda	Zelda	k1gMnSc1	Zelda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInSc1d1	základní
princip	princip	k1gInSc1	princip
thereminu	theremin	k1gInSc2	theremin
==	==	k?	==
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
elektronický	elektronický	k2eAgInSc4d1	elektronický
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
zvuk	zvuk	k1gInSc1	zvuk
vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
elektrické	elektrický	k2eAgInPc4d1	elektrický
kmity	kmit	k1gInPc4	kmit
<g/>
.	.	kIx.	.
</s>
<s>
Theremin	Theremin	k1gInSc1	Theremin
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
záznějovém	záznějový	k2eAgInSc6d1	záznějový
principu	princip	k1gInSc6	princip
-	-	kIx~	-
elektrické	elektrický	k2eAgInPc1d1	elektrický
kmity	kmit	k1gInPc1	kmit
s	s	k7c7	s
frekvencí	frekvence	k1gFnSc7	frekvence
ve	v	k7c6	v
slyšitelné	slyšitelný	k2eAgFnSc6d1	slyšitelná
oblasti	oblast	k1gFnSc6	oblast
vznikají	vznikat	k5eAaImIp3nP	vznikat
jako	jako	k8xS	jako
zázněj	zázněj	k1gFnSc1	zázněj
(	(	kIx(	(
<g/>
rozdílová	rozdílový	k2eAgFnSc1d1	rozdílová
složka	složka	k1gFnSc1	složka
<g/>
)	)	kIx)	)
mezi	mezi	k7c4	mezi
kmity	kmit	k1gInPc4	kmit
dvojice	dvojice	k1gFnSc2	dvojice
nesynchronizovaných	synchronizovaný	k2eNgInPc2d1	nesynchronizovaný
oscilátorů	oscilátor	k1gInPc2	oscilátor
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
tvořených	tvořený	k2eAgInPc2d1	tvořený
LC	LC	kA	LC
obvody	obvod	k1gInPc1	obvod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
kmitají	kmitat	k5eAaImIp3nP	kmitat
na	na	k7c6	na
frekvenci	frekvence	k1gFnSc6	frekvence
několika	několik	k4yIc2	několik
set	set	k1gInSc4	set
kHz	khz	kA	khz
<g/>
.	.	kIx.	.
</s>
<s>
Frekvence	frekvence	k1gFnSc1	frekvence
jednoho	jeden	k4xCgInSc2	jeden
oscilátoru	oscilátor	k1gInSc2	oscilátor
je	být	k5eAaImIp3nS	být
pevná	pevný	k2eAgFnSc1d1	pevná
<g/>
,	,	kIx,	,
frekvence	frekvence	k1gFnSc1	frekvence
druhého	druhý	k4xOgMnSc4	druhý
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
přibližováním	přibližování	k1gNnSc7	přibližování
pravé	pravý	k2eAgFnSc2d1	pravá
ruky	ruka	k1gFnSc2	ruka
k	k	k7c3	k
anténě	anténa	k1gFnSc3	anténa
připojené	připojený	k2eAgNnSc1d1	připojené
k	k	k7c3	k
laděnému	laděný	k2eAgInSc3d1	laděný
LC	LC	kA	LC
obvodu	obvod	k1gInSc6	obvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Signály	signál	k1gInPc1	signál
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
oscilátorů	oscilátor	k1gInPc2	oscilátor
jsou	být	k5eAaImIp3nP	být
vedeny	vést	k5eAaImNgInP	vést
do	do	k7c2	do
směšovače	směšovač	k1gInSc2	směšovač
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc1	dva
signály	signál	k1gInPc1	signál
sloučí	sloučit	k5eAaPmIp3nP	sloučit
<g/>
.	.	kIx.	.
</s>
<s>
Vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
tzv.	tzv.	kA	tzv.
zázněj	zázněj	k1gFnSc4	zázněj
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
frekvence	frekvence	k1gFnSc1	frekvence
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
rozdílu	rozdíl	k1gInSc2	rozdíl
frekvencí	frekvence	k1gFnSc7	frekvence
vstupních	vstupní	k2eAgInPc2d1	vstupní
signálů	signál	k1gInPc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgInPc1	dva
oscilátory	oscilátor	k1gInPc1	oscilátor
naladěny	naladěn	k2eAgInPc1d1	naladěn
na	na	k7c4	na
shodnou	shodný	k2eAgFnSc4d1	shodná
frekvenci	frekvence	k1gFnSc4	frekvence
<g/>
,	,	kIx,	,
zázněj	zázněj	k1gFnSc4	zázněj
zanikne	zaniknout	k5eAaPmIp3nS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Malým	malý	k2eAgNnSc7d1	malé
rozladěním	rozladění	k1gNnSc7	rozladění
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
oscilátorů	oscilátor	k1gInPc2	oscilátor
lze	lze	k6eAd1	lze
přelaďovat	přelaďovat	k5eAaImF	přelaďovat
výstupní	výstupní	k2eAgInSc4d1	výstupní
zvukový	zvukový	k2eAgInSc4d1	zvukový
signál	signál	k1gInSc4	signál
v	v	k7c6	v
širokém	široký	k2eAgInSc6d1	široký
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvukový	zvukový	k2eAgInSc1d1	zvukový
signál	signál	k1gInSc1	signál
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
veden	vést	k5eAaImNgInS	vést
do	do	k7c2	do
obvodu	obvod	k1gInSc2	obvod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ovládá	ovládat	k5eAaImIp3nS	ovládat
amplitudu	amplituda	k1gFnSc4	amplituda
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
řízen	řídit	k5eAaImNgInS	řídit
třetím	třetí	k4xOgInSc7	třetí
oscilátorem	oscilátor	k1gInSc7	oscilátor
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
anténou	anténa	k1gFnSc7	anténa
určenou	určený	k2eAgFnSc7d1	určená
pro	pro	k7c4	pro
levou	levý	k2eAgFnSc4d1	levá
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
theremin	theremin	k1gInSc1	theremin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Znáte	znát	k5eAaImIp2nP	znát
theremin	theremin	k1gInSc4	theremin
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Theremin	Theremin	k1gInSc1	Theremin
<g/>
,	,	kIx,	,
prosíme	prosit	k5eAaImIp1nP	prosit
nesahat	sahat	k5eNaImF	sahat
</s>
</p>
<p>
<s>
www.thereminworld.com	www.thereminworld.com	k1gInSc1	www.thereminworld.com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Samostatný	samostatný	k2eAgInSc1d1	samostatný
projekt	projekt	k1gInSc1	projekt
Theremin	Theremin	k2eAgInSc1d1	Theremin
na	na	k7c4	na
Soustředění	soustředění	k1gNnSc4	soustředění
mladých	mladý	k2eAgMnPc2d1	mladý
fyziků	fyzik	k1gMnPc2	fyzik
a	a	k8xC	a
matematiků	matematik	k1gMnPc2	matematik
</s>
</p>
<p>
<s>
Theremin	Theremin	k1gInSc1	Theremin
–	–	k?	–
konstrukce	konstrukce	k1gFnSc2	konstrukce
hudebního	hudební	k2eAgInSc2d1	hudební
nástroje	nástroj	k1gInSc2	nástroj
(	(	kIx(	(
<g/>
Alternativní	alternativní	k2eAgInSc1d1	alternativní
odkaz	odkaz	k1gInSc1	odkaz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Katica	Katica	k1gFnSc1	Katica
Illenyi	Illeny	k1gFnSc2	Illeny
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
theremin	theremin	k1gInSc4	theremin
melodii	melodie	k1gFnSc4	melodie
z	z	k7c2	z
filmu	film	k1gInSc2	film
Tenkrát	tenkrát	k6eAd1	tenkrát
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
</s>
</p>
