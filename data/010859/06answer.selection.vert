<s>
Theremin	Theremin	k1gInSc1	Theremin
(	(	kIx(	(
<g/>
také	také	k9	také
těremin	těremin	k1gInSc1	těremin
<g/>
,	,	kIx,	,
thereminvox	thereminvox	k1gInSc1	thereminvox
<g/>
,	,	kIx,	,
termenvox	termenvox	k1gInSc1	termenvox
<g/>
,	,	kIx,	,
aetherophon	aetherophon	k1gInSc1	aetherophon
<g/>
,	,	kIx,	,
etherophon	etherophon	k1gInSc1	etherophon
<g/>
,	,	kIx,	,
éterové	éterový	k2eAgFnPc1d1	éterová
vlny	vlna	k1gFnPc1	vlna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
elektronický	elektronický	k2eAgInSc1d1	elektronický
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
Lev	Lev	k1gMnSc1	Lev
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Těrmen	Těrmen	k2eAgMnSc1d1	Těrmen
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
též	též	k9	též
jako	jako	k9	jako
Léon	Léon	k1gInSc1	Léon
Theremin	Theremin	k2eAgInSc1d1	Theremin
<g/>
.	.	kIx.	.
</s>
