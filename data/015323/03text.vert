<s>
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalová	k1gFnSc1
</s>
<s>
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalová	k1gFnSc1
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalová	k1gFnSc1
během	během	k7c2
natáčení	natáčení	k1gNnSc2
Růžového	růžový	k2eAgMnSc2d1
pantera	panter	k1gMnSc2
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
Rodné	rodný	k2eAgInPc1d1
jméno	jméno	k1gNnSc4
</s>
<s>
Claude	Claud	k1gMnSc5
Joséphine	Joséphin	k1gMnSc5
Rose	Rose	k1gMnSc5
Cardinale	Cardinale	k1gMnSc5
Narození	narození	k1gNnSc2
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1938	#num#	k4
(	(	kIx(
<g/>
82	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Tunis	Tunis	k1gInSc1
Francouzský	francouzský	k2eAgInSc1d1
protektorát	protektorát	k1gInSc1
v	v	k7c6
Tunisku	Tunisko	k1gNnSc6
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Experimental	Experimental	k1gMnSc1
Centre	centr	k1gInSc5
of	of	k?
Cinematography	Cinematograph	k1gInPc1
Aktivní	aktivní	k2eAgInPc1d1
roky	rok	k1gInPc4
</s>
<s>
1958	#num#	k4
<g/>
–	–	k?
<g/>
dodnes	dodnes	k6eAd1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Franco	Franco	k6eAd1
CristaldiPasquale	CristaldiPasquala	k1gFnSc3
Squitieri	Squitier	k1gFnSc2
Partner	partner	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Pasquale	Pasquale	k6eAd1
Squitieri	Squitieri	k1gNnSc1
(	(	kIx(
<g/>
Desetiletí	desetiletí	k1gNnPc4
od	od	k7c2
1970	#num#	k4
–	–	k?
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
Marcello	Marcella	k1gMnSc5
Mastroianni	Mastroianň	k1gMnSc5
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalová	k1gFnSc1
<g/>
,	,	kIx,
nepřechýleně	přechýleně	k6eNd1
Claudia	Claudia	k1gFnSc1
Cardinale	Cardinale	k1gFnSc2
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
označována	označován	k2eAgNnPc4d1
CC	CC	kA
<g/>
,	,	kIx,
*	*	kIx~
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1938	#num#	k4
Tunis	Tunis	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
italská	italský	k2eAgFnSc1d1
divadelní	divadelní	k2eAgFnSc1d1
a	a	k8xC
filmová	filmový	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
hereckou	herecký	k2eAgFnSc7d1
legendou	legenda	k1gFnSc7
a	a	k8xC
sexuálním	sexuální	k2eAgInSc7d1
symbolem	symbol	k1gInSc7
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
60	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
jí	on	k3xPp3gFnSc2
byly	být	k5eAaImAgFnP
svěřovány	svěřovat	k5eAaImNgFnP
filmové	filmový	k2eAgFnPc1d1
role	role	k1gFnPc1
žen	žena	k1gFnPc2
s	s	k7c7
italským	italský	k2eAgInSc7d1
temperamentem	temperament	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
své	svůj	k3xOyFgFnSc2
kariéry	kariéra	k1gFnSc2
spolupracovala	spolupracovat	k5eAaImAgFnS
s	s	k7c7
řadou	řada	k1gFnSc7
známých	známý	k2eAgMnPc2d1
režisérů	režisér	k1gMnPc2
a	a	k8xC
získala	získat	k5eAaPmAgFnS
řadu	řada	k1gFnSc4
filmových	filmový	k2eAgNnPc2d1
ocenění	ocenění	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
kariéra	kariéra	k1gFnSc1
</s>
<s>
Dětství	dětství	k1gNnSc4
a	a	k8xC
mládí	mládí	k1gNnSc4
</s>
<s>
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalová	k1gFnSc1
<g/>
,	,	kIx,
1957	#num#	k4
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1938	#num#	k4
jako	jako	k8xS,k8xC
Claude	Claud	k1gMnSc5
Joséphine	Joséphin	k1gMnSc5
Rose	Rose	k1gMnSc5
Cardinale	Cardinale	k1gMnSc5
rodičům	rodič	k1gMnPc3
sicilského	sicilský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
po	po	k7c4
tři	tři	k4xCgFnPc4
generace	generace	k1gFnPc4
žili	žít	k5eAaImAgMnP
v	v	k7c6
Tunisku	Tunisko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
I	i	k8xC
když	když	k8xS
její	její	k3xOp3gMnPc1
předci	předek	k1gMnPc1
pocházeli	pocházet	k5eAaImAgMnP
ze	z	k7c2
Sicílie	Sicílie	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
jejich	jejich	k3xOp3gFnSc6
rodině	rodina	k1gFnSc6
v	v	k7c6
Tunisku	Tunisko	k1gNnSc6
se	se	k3xPyFc4
mluvilo	mluvit	k5eAaImAgNnS
pouze	pouze	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
francouzsky	francouzsky	k6eAd1
<g/>
,	,	kIx,
neboť	neboť	k8xC
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
úředních	úřední	k2eAgFnPc2d1
řečí	řeč	k1gFnPc2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
jí	on	k3xPp3gFnSc3
samé	samý	k3xTgFnPc4
se	se	k3xPyFc4
dostalo	dostat	k5eAaPmAgNnS
francouzské	francouzský	k2eAgNnSc1d1
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
italské	italský	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k8xC,k8xS
dítě	dítě	k1gNnSc1
zažila	zažít	k5eAaPmAgFnS
v	v	k7c6
Tunisku	Tunisko	k1gNnSc6
boje	boj	k1gInSc2
na	na	k7c6
africké	africký	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
znamenalo	znamenat	k5eAaImAgNnS
ukrývání	ukrývání	k1gNnSc1
se	se	k3xPyFc4
ve	v	k7c6
sklepeních	sklepení	k1gNnPc6
společně	společně	k6eAd1
s	s	k7c7
rodiči	rodič	k1gMnPc7
a	a	k8xC
mladšími	mladý	k2eAgMnPc7d2
sourozenci	sourozenec	k1gMnPc7
(	(	kIx(
<g/>
sestra	sestra	k1gFnSc1
a	a	k8xC
dva	dva	k4xCgMnPc1
bratři	bratr	k1gMnPc1
<g/>
)	)	kIx)
a	a	k8xC
časté	častý	k2eAgNnSc1d1
stěhování	stěhování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Již	již	k6eAd1
jako	jako	k8xC,k8xS
dítě	dítě	k1gNnSc4
nebyla	být	k5eNaImAgFnS
k	k	k7c3
přehlédnutí	přehlédnutí	k1gNnSc3
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
díky	díky	k7c3
svému	svůj	k3xOyFgInSc3
fotogenickému	fotogenický	k2eAgInSc3d1
vzhledu	vzhled	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
mladá	mladý	k2eAgFnSc1d1
dívka	dívka	k1gFnSc1
vypadala	vypadat	k5eAaPmAgFnS,k5eAaImAgFnS
mladší	mladý	k2eAgFnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
skutečně	skutečně	k6eAd1
byla	být	k5eAaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1957	#num#	k4
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
devatenácti	devatenáct	k4xCc6
letech	léto	k1gNnPc6
vyhrála	vyhrát	k5eAaPmAgFnS
titul	titul	k1gInSc4
nejkrásnější	krásný	k2eAgFnSc1d3
Italky	Italka	k1gFnPc1
v	v	k7c6
Tunisku	Tunisko	k1gNnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
to	ten	k3xDgNnSc1
údajně	údajně	k6eAd1
proti	proti	k7c3
své	svůj	k3xOyFgFnSc3
vůli	vůle	k1gFnSc3
<g/>
:	:	kIx,
byla	být	k5eAaImAgFnS
nejkrásnější	krásný	k2eAgFnSc7d3
dívkou	dívka	k1gFnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
sále	sál	k1gInSc6
a	a	k8xC
tak	tak	k6eAd1
ji	on	k3xPp3gFnSc4
vytáhli	vytáhnout	k5eAaPmAgMnP
na	na	k7c4
pódium	pódium	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k8xC,k8xS
odměnu	odměna	k1gFnSc4
pak	pak	k6eAd1
dostala	dostat	k5eAaPmAgFnS
zájezd	zájezd	k1gInSc4
na	na	k7c4
Benátský	benátský	k2eAgInSc4d1
filmový	filmový	k2eAgInSc4d1
festival	festival	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
kde	kde	k6eAd1
její	její	k3xOp3gFnSc1
exotická	exotický	k2eAgFnSc1d1
krása	krása	k1gFnSc1
sklidila	sklidit	k5eAaPmAgFnS
velké	velký	k2eAgNnSc4d1
uznání	uznání	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
dvou	dva	k4xCgInPc2
<g />
.	.	kIx.
</s>
<s hack="1">
měsíců	měsíc	k1gInPc2
získala	získat	k5eAaPmAgFnS
kontrakt	kontrakt	k1gInSc4
na	na	k7c4
filmovou	filmový	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
,	,	kIx,
přestože	přestože	k8xS
předtím	předtím	k6eAd1
nikdy	nikdy	k6eAd1
herečkou	herečka	k1gFnSc7
být	být	k5eAaImF
nechtěla	chtít	k5eNaImAgFnS
<g/>
,	,	kIx,
filmové	filmový	k2eAgFnSc2d1
role	role	k1gFnSc2
odmítala	odmítat	k5eAaImAgFnS
a	a	k8xC
chtěla	chtít	k5eAaImAgFnS
být	být	k5eAaImF
například	například	k6eAd1
učitelkou	učitelka	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
jejím	její	k3xOp3gInPc3
vzorům	vzor	k1gInPc3
patřila	patřit	k5eAaImAgFnS
Brigitte	Brigitte	k1gFnSc1
Bardotová	Bardotový	k2eAgFnSc1d1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
v	v	k7c6
mládí	mládí	k1gNnSc6
někdy	někdy	k6eAd1
dokonce	dokonce	k9
napodobovala	napodobovat	k5eAaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Herecká	herecký	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Jako	jako	k9
dítě	dítě	k1gNnSc1
„	„	k?
<g/>
vynikala	vynikat	k5eAaImAgFnS
<g/>
“	“	k?
poněkud	poněkud	k6eAd1
výraznějším	výrazný	k2eAgInSc7d2
hlubokým	hluboký	k2eAgInSc7d1
<g/>
,	,	kIx,
až	až	k9
chraplavým	chraplavý	k2eAgInSc7d1
<g/>
,	,	kIx,
hlasem	hlasem	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
její	její	k3xOp3gFnSc3
půvabné	půvabný	k2eAgFnSc3d1
tváři	tvář	k1gFnSc3
a	a	k8xC
tělu	tělo	k1gNnSc3
patří	patřit	k5eAaImIp3nS
tedy	tedy	k9
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
poměrně	poměrně	k6eAd1
drsný	drsný	k2eAgInSc4d1
hlas	hlas	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
podle	podle	k7c2
většiny	většina	k1gFnSc2
filmových	filmový	k2eAgMnPc2d1
režisérů	režisér	k1gMnPc2
vůbec	vůbec	k9
neladil	ladit	k5eNaImAgMnS
s	s	k7c7
jejím	její	k3xOp3gInSc7
exotickým	exotický	k2eAgInSc7d1
zevnějškem	zevnějšek	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
ji	on	k3xPp3gFnSc4
nechali	nechat	k5eAaPmAgMnP
dabovat	dabovat	k5eAaBmF
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
–	–	k?
a	a	k8xC
to	ten	k3xDgNnSc1
dokonce	dokonce	k9
i	i	k9
v	v	k7c6
italštině	italština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
režisér	režisér	k1gMnSc1
Federico	Federico	k1gMnSc1
Fellini	Fellin	k2eAgMnPc1d1
ji	on	k3xPp3gFnSc4
nechal	nechat	k5eAaPmAgInS
promluvit	promluvit	k5eAaPmF
vlastním	vlastní	k2eAgInSc7d1
hlasem	hlas	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1963	#num#	k4
ve	v	k7c6
filmu	film	k1gInSc6
Osm	osm	k4xCc1
a	a	k8xC
půl	půl	k1xP
(	(	kIx(
<g/>
Otto	Otto	k1gMnSc1
e	e	k0
mezzo	mezza	k1gFnSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
Luchino	Luchin	k2eAgNnSc4d1
Visconti	Visconti	k1gNnSc4
nenechal	nechat	k5eNaPmAgMnS
jako	jako	k8xC,k8xS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
mála	málo	k1gNnSc2
film	film	k1gInSc4
Hvězdy	hvězda	k1gFnSc2
velkého	velký	k2eAgInSc2d1
vozu	vůz	k1gInSc2
(	(	kIx(
<g/>
Vaghe	Vaghe	k1gInSc1
stelle	stelle	k1gNnSc1
dell	dell	k1gInSc1
<g/>
'	'	kIx"
<g/>
Orsa	Orsa	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
)	)	kIx)
předabovat	předabovat	k5eAaBmF,k5eAaPmF,k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalová	k1gFnSc1
<g/>
,	,	kIx,
1960	#num#	k4
</s>
<s>
Jejím	její	k3xOp3gInSc7
filmovým	filmový	k2eAgInSc7d1
debutem	debut	k1gInSc7
byl	být	k5eAaImAgInS
snímek	snímek	k1gInSc1
Zmýlená	zmýlená	k1gFnSc1
neplatí	platit	k5eNaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1958	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Objevila	objevit	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
nejednom	nejeden	k4xCyIgInSc6
americkém	americký	k2eAgInSc6d1
filmu	film	k1gInSc6
<g/>
,	,	kIx,
například	například	k6eAd1
v	v	k7c6
Růžovém	růžový	k2eAgMnSc6d1
panterovi	panter	k1gMnSc6
a	a	k8xC
ve	v	k7c6
filmu	film	k1gInSc6
Svět	svět	k1gInSc1
cirkus	cirkus	k1gInSc1
(	(	kIx(
<g/>
Il	Il	k1gMnSc1
circo	circo	k1gMnSc1
e	e	k0
la	la	k0
sua	sua	k?
grande	grand	k1gMnSc5
avventura	avventura	k1gFnSc1
<g/>
)	)	kIx)
vedle	vedle	k7c2
Johna	John	k1gMnSc2
Wayna	Wayn	k1gMnSc2
a	a	k8xC
Rity	Rita	k1gFnSc2
Hayworthové	Hayworthová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
jeden	jeden	k4xCgInSc4
ze	z	k7c2
symbolů	symbol	k1gInPc2
italského	italský	k2eAgInSc2d1
filmu	film	k1gInSc2
a	a	k8xC
zároveň	zároveň	k6eAd1
je	být	k5eAaImIp3nS
nejslavnější	slavný	k2eAgFnSc7d3
tuniskou	tuniský	k2eAgFnSc7d1
herečkou	herečka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1962	#num#	k4
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
po	po	k7c6
boku	bok	k1gInSc6
Jeana-Paula	Jeana-Paul	k1gMnSc2
Belmonda	Belmond	k1gMnSc2
ve	v	k7c6
filmů	film	k1gInPc2
Cartouche	Cartouche	k1gNnPc2
<g/>
,	,	kIx,
z	z	k7c2
dalších	další	k2eAgInPc2d1
výrazných	výrazný	k2eAgInPc2d1
titulů	titul	k1gInPc2
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c6
Tenkrát	tenkrát	k6eAd1
na	na	k7c6
Západě	západ	k1gInSc6
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Hezký	hezký	k2eAgInSc1d1
<g/>
,	,	kIx,
charakterní	charakterní	k2eAgMnSc1d1
Ital	Ital	k1gMnSc1
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
hledá	hledat	k5eAaImIp3nS
krajanku	krajanka	k1gFnSc4
za	za	k7c7
účelem	účel	k1gInSc7
sňatku	sňatek	k1gInSc2
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrála	hrát	k5eAaImAgFnS
také	také	k9
ve	v	k7c6
snímcích	snímek	k1gInPc6
Petrolejářky	Petrolejářka	k1gFnSc2
(	(	kIx(
<g/>
Les	les	k1gInSc1
pétroleuses	pétroleuses	k1gInSc1
<g/>
,	,	kIx,
režisér	režisér	k1gMnSc1
Christian-Jaque	Christian-Jaqu	k1gFnSc2
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Červený	červený	k2eAgInSc1d1
stan	stan	k1gInSc1
<g/>
,	,	kIx,
Zavázané	zavázaný	k2eAgNnSc4d1
oči	oko	k1gNnPc4
<g/>
,	,	kIx,
Děvče	děvče	k1gNnSc4
se	s	k7c7
zavazadlem	zavazadlo	k1gNnSc7
a	a	k8xC
dalších	další	k2eAgFnPc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
byla	být	k5eAaImAgFnS
hostem	host	k1gMnSc7
karlovarského	karlovarský	k2eAgInSc2d1
filmového	filmový	k2eAgInSc2d1
festivalu	festival	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Celosvětovou	celosvětový	k2eAgFnSc4d1
známost	známost	k1gFnSc4
získala	získat	k5eAaPmAgFnS
především	především	k9
ze	z	k7c2
dvou	dva	k4xCgInPc2
filmů	film	k1gInPc2
režiséra	režisér	k1gMnSc2
Luchina	Luchin	k1gMnSc2
Viscontiho	Visconti	k1gMnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgMnPc6,k3yQgMnPc6,k3yRgMnPc6
hrála	hrát	k5eAaImAgFnS
po	po	k7c6
boku	bok	k1gInSc6
Alaina	Alain	k1gMnSc2
Delona	Delon	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
to	ten	k3xDgNnSc1
snímky	snímek	k1gInPc1
Rocco	Rocco	k6eAd1
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
bratři	bratr	k1gMnPc1
(	(	kIx(
<g/>
Rocco	Rocco	k6eAd1
e	e	k0
i	i	k9
suoi	suoi	k6eAd1
fratelli	fratelnout	k5eAaImAgMnP,k5eAaPmAgMnP
<g/>
)	)	kIx)
z	z	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
a	a	k8xC
Gepard	gepard	k1gMnSc1
(	(	kIx(
<g/>
Il	Il	k1gFnSc6
Gattopardo	Gattopardo	k1gNnSc4
<g/>
)	)	kIx)
z	z	k7c2
roku	rok	k1gInSc2
1963	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předlohou	předloha	k1gFnSc7
významného	významný	k2eAgInSc2d1
filmu	film	k1gInSc2
Gepard	gepard	k1gMnSc1
<g/>
,	,	kIx,
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
nejlepších	dobrý	k2eAgMnPc2d3
snímků	snímek	k1gInPc2
v	v	k7c6
dějinách	dějiny	k1gFnPc6
filmu	film	k1gInSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
stejnojmenný	stejnojmenný	k2eAgInSc1d1
román	román	k1gInSc1
italského	italský	k2eAgMnSc2d1
knížete	kníže	k1gMnSc2
Giuseppe	Giusepp	k1gInSc5
Tomasi	Tomas	k1gMnPc1
di	di	k?
Lampedusa	Lampedus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Také	také	k6eAd1
Marcello	Marcello	k1gNnSc4
Mastroianni	Mastroiann	k1gMnPc1
byl	být	k5eAaImAgInS
hereckým	herecký	k2eAgMnSc7d1
partnerem	partner	k1gMnSc7
Claudie	Claudia	k1gFnSc2
Cardinalové	Cardinalové	k2eAgFnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ve	v	k7c6
snímku	snímek	k1gInSc6
Krásný	krásný	k2eAgMnSc1d1
Antonio	Antonio	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
natočil	natočit	k5eAaBmAgMnS
režisér	režisér	k1gMnSc1
Mauro	Mauro	k1gNnSc4
Bolognini	Bolognin	k2eAgMnPc1d1
v	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
ve	v	k7c6
známém	známý	k2eAgInSc6d1
epizodovém	epizodový	k2eAgInSc6d1
filmu	film	k1gInSc6
Federica	Federicus	k1gMnSc2
Felliniho	Fellini	k1gMnSc2
Osm	osm	k4xCc1
a	a	k8xC
půl	půl	k1xP
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
filmu	film	k1gInSc6
Gepard	gepard	k1gMnSc1
s	s	k7c7
Burtem	Burt	k1gMnSc7
Lancasterem	Lancaster	k1gMnSc7
a	a	k8xC
Alainem	Alain	k1gMnSc7
Delonem	Delon	k1gMnSc7
</s>
<s>
Dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
roku	rok	k1gInSc2
1968	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Itálii	Itálie	k1gFnSc6
uskutečnila	uskutečnit	k5eAaPmAgFnS
premiéra	premiéra	k1gFnSc1
westernu	western	k1gInSc2
Tenkrát	tenkrát	k6eAd1
na	na	k7c6
Západě	západ	k1gInSc6
(	(	kIx(
<g/>
C	C	kA
<g/>
'	'	kIx"
<g/>
era	era	k?
una	una	k?
volta	volta	k1gFnSc1
i	i	k8xC
West	West	k1gInSc1
<g/>
)	)	kIx)
režiséra	režisér	k1gMnSc2
Sergia	Sergius	k1gMnSc2
Leoneho	Leone	k1gMnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
hrála	hrát	k5eAaImAgFnS
roli	role	k1gFnSc4
vdovy	vdova	k1gFnSc2
po	po	k7c6
irském	irský	k2eAgMnSc6d1
přistěhovalci	přistěhovalec	k1gMnSc6
McBainovi	McBain	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
italowestern	italowestern	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
kultovním	kultovní	k2eAgInSc7d1
filmem	film	k1gInSc7
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
západní	západní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
se	se	k3xPyFc4
promítal	promítat	k5eAaImAgInS
nepřetržitě	přetržitě	k6eNd1
šest	šest	k4xCc4
let	léto	k1gNnPc2
a	a	k8xC
Cardinalovou	Cardinalová	k1gFnSc7
proslavil	proslavit	k5eAaPmAgMnS
ještě	ještě	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
umělecky	umělecky	k6eAd1
významnější	významný	k2eAgInPc1d2
dřívější	dřívější	k2eAgInPc1d1
filmy	film	k1gInPc1
režisérů	režisér	k1gMnPc2
Viscontiho	Visconti	k1gMnSc2
a	a	k8xC
Felliniho	Fellini	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrála	hrát	k5eAaImAgFnS
v	v	k7c6
něm	on	k3xPp3gNnSc6
po	po	k7c6
boku	bok	k1gInSc6
Henry	henry	k1gInSc2
Fondy	fond	k1gInPc4
<g/>
,	,	kIx,
Charlese	Charles	k1gMnSc2
Bronsona	Bronson	k1gMnSc2
nebo	nebo	k8xC
Jasona	Jason	k1gMnSc2
Robardse	Robards	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Herečka	herečka	k1gFnSc1
údajně	údajně	k6eAd1
nabídku	nabídka	k1gFnSc4
na	na	k7c4
tuto	tento	k3xDgFnSc4
roli	role	k1gFnSc4
přijala	přijmout	k5eAaPmAgFnS
ihned	ihned	k6eAd1
potom	potom	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
si	se	k3xPyFc3
poslechla	poslechnout	k5eAaPmAgFnS
hudbu	hudba	k1gFnSc4
k	k	k7c3
filmu	film	k1gInSc3
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
složil	složit	k5eAaPmAgMnS
Ennio	Ennio	k6eAd1
Morricone	Morricon	k1gInSc5
na	na	k7c4
motivy	motiv	k1gInPc4
staré	starý	k2eAgFnSc2d1
německé	německý	k2eAgFnSc2d1
balady	balada	k1gFnSc2
Spiel	Spiel	k1gMnSc1
mir	mir	k1gInSc1
das	das	k?
Lied	Lied	k1gInSc1
vom	vom	k?
Tod	Tod	k1gFnSc2
(	(	kIx(
<g/>
Zpívej	zpívat	k5eAaImRp2nS
mi	já	k3xPp1nSc3
píseň	píseň	k1gFnSc4
o	o	k7c6
smrti	smrt	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
natáčeních	natáčení	k1gNnPc6
se	se	k3xPyFc4
naučila	naučit	k5eAaPmAgFnS
jezdit	jezdit	k5eAaImF
na	na	k7c6
koni	kůň	k1gMnSc6
a	a	k8xC
točila	točit	k5eAaImAgFnS
i	i	k9
nebezpečnější	bezpečný	k2eNgFnPc4d2
scény	scéna	k1gFnPc4
včetně	včetně	k7c2
žebříkových	žebříkový	k2eAgMnPc2d1
pod	pod	k7c7
vrtulníkem	vrtulník	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
svému	svůj	k3xOyFgInSc3
africkému	africký	k2eAgInSc3d1
původu	původ	k1gInSc3
byla	být	k5eAaImAgFnS
také	také	k9
většinou	většinou	k6eAd1
jediná	jediný	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
neměla	mít	k5eNaImAgFnS
žaludeční	žaludeční	k2eAgFnPc4d1
potíže	potíž	k1gFnPc4
při	při	k7c6
natáčení	natáčení	k1gNnSc6
v	v	k7c6
takových	takový	k3xDgFnPc6
oblastech	oblast	k1gFnPc6
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
amazonské	amazonský	k2eAgInPc4d1
pralesy	prales	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Natočila	natočit	k5eAaBmAgFnS
přibližně	přibližně	k6eAd1
150	#num#	k4
filmů	film	k1gInPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
od	od	k7c2
postav	postava	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgMnPc6,k3yQgMnPc6,k3yRgMnPc6
uplatňovala	uplatňovat	k5eAaImAgFnS
zejména	zejména	k9
svůj	svůj	k3xOyFgInSc4
zjev	zjev	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
později	pozdě	k6eAd2
dostala	dostat	k5eAaPmAgFnS
k	k	k7c3
charakternějším	charakterní	k2eAgFnPc3d2
rolím	role	k1gFnPc3
silných	silný	k2eAgFnPc2d1
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
začala	začít	k5eAaPmAgFnS
kromě	kromě	k7c2
filmu	film	k1gInSc2
hrát	hrát	k5eAaImF
rovněž	rovněž	k9
v	v	k7c6
divadle	divadlo	k1gNnSc6
<g/>
,	,	kIx,
například	například	k6eAd1
ve	v	k7c6
hrách	hra	k1gFnPc6
Luigiho	Luigi	k1gMnSc2
Pirandella	Pirandell	k1gMnSc2
nebo	nebo	k8xC
Tennesseeho	Tennessee	k1gMnSc2
Williamse	Williams	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osobní	osobní	k2eAgInSc4d1
život	život	k1gInSc4
a	a	k8xC
další	další	k2eAgFnPc4d1
aktivity	aktivita	k1gFnPc4
</s>
<s>
Ve	v	k7c6
scéně	scéna	k1gFnSc6
filmu	film	k1gInSc2
Ztracená	ztracený	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
Její	její	k3xOp3gInSc4
život	život	k1gInSc4
zpočátku	zpočátku	k6eAd1
ovlivňovaly	ovlivňovat	k5eAaImAgInP
dodatky	dodatek	k1gInPc1
ke	k	k7c3
smlouvě	smlouva	k1gFnSc3
s	s	k7c7
filmovým	filmový	k2eAgMnSc7d1
producentem	producent	k1gMnSc7
Francem	Franc	k1gMnSc7
Cristaldim	Cristaldim	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
kterým	který	k3yRgMnPc3,k3yQgMnPc3,k3yIgMnPc3
žila	žít	k5eAaImAgFnS
15	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesně	přesně	k6eAd1
tak	tak	k6eAd1
dlouho	dlouho	k6eAd1
trvala	trvat	k5eAaImAgFnS
jejich	jejich	k3xOp3gFnSc1
smlouva	smlouva	k1gFnSc1
a	a	k8xC
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
nesměla	smět	k5eNaImAgFnS
po	po	k7c4
tu	ten	k3xDgFnSc4
dobu	doba	k1gFnSc4
přibrat	přibrat	k5eAaPmF
na	na	k7c6
váze	váha	k1gFnSc6
<g/>
,	,	kIx,
měnit	měnit	k5eAaImF
svévolně	svévolně	k6eAd1
způsob	způsob	k1gInSc4
oblékání	oblékání	k1gNnSc2
ani	ani	k8xC
líčení	líčení	k1gNnSc2
a	a	k8xC
hlavně	hlavně	k6eAd1
musela	muset	k5eAaImAgFnS
před	před	k7c7
novináři	novinář	k1gMnPc7
tajit	tajit	k5eAaImF
existenci	existence	k1gFnSc4
svého	svůj	k3xOyFgMnSc2
malého	malý	k2eAgMnSc2d1
syna	syn	k1gMnSc2
Patricka	Patricko	k1gNnSc2
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yIgMnSc4,k3yQgMnSc4
porodila	porodit	k5eAaPmAgFnS
v	v	k7c6
devatenácti	devatenáct	k4xCc6
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patricka	Patricka	k1gFnSc1
její	její	k3xOp3gFnSc1
rodina	rodina	k1gFnSc1
vydávala	vydávat	k5eAaPmAgFnS,k5eAaImAgFnS
za	za	k7c4
jejího	její	k3xOp3gMnSc4
nejmladšího	mladý	k2eAgMnSc4d3
sourozence	sourozenec	k1gMnSc4
a	a	k8xC
ani	ani	k8xC
dítě	dítě	k1gNnSc1
nesmělo	smět	k5eNaImAgNnS
vědět	vědět	k5eAaImF
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc1
pravá	pravý	k2eAgFnSc1d1
matka	matka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Počátkem	počátkem	k7c2
šedesátých	šedesátý	k4xOgNnPc2
let	léto	k1gNnPc2
ale	ale	k9
novináři	novinář	k1gMnPc1
tajemství	tajemství	k1gNnSc4
odhalili	odhalit	k5eAaPmAgMnP
a	a	k8xC
herečka	herečka	k1gFnSc1
byla	být	k5eAaImAgFnS
nucena	nucen	k2eAgFnSc1d1
přiznat	přiznat	k5eAaPmF
své	svůj	k3xOyFgNnSc4
mateřství	mateřství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Má	mít	k5eAaImIp3nS
se	se	k3xPyFc4
zato	zato	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
byla	být	k5eAaImAgFnS
provdaná	provdaný	k2eAgFnSc1d1
za	za	k7c2
Franca	Franca	k?
Cristaldiho	Cristaldi	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželství	manželství	k1gNnSc1
trvalo	trvat	k5eAaImAgNnS
od	od	k7c2
roku	rok	k1gInSc2
1966	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
do	do	k7c2
rozvodu	rozvod	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
1975	#num#	k4
žije	žít	k5eAaImIp3nS
s	s	k7c7
režisérem	režisér	k1gMnSc7
Pasqualem	Pasqual	k1gMnSc7
Squitierim	Squitierima	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Squitieri	Squitier	k1gMnPc7
je	být	k5eAaImIp3nS
otcem	otec	k1gMnSc7
jejího	její	k3xOp3gMnSc2
druhého	druhý	k4xOgNnSc2
dítěte	dítě	k1gNnSc2
<g/>
,	,	kIx,
dcery	dcera	k1gFnSc2
Claudie	Claudia	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc3
narodila	narodit	k5eAaPmAgFnS
až	až	k6eAd1
po	po	k7c6
čtyřicítce	čtyřicítka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
babičkou	babička	k1gFnSc7
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
narodila	narodit	k5eAaPmAgFnS
vnučka	vnučka	k1gFnSc1
Lucille	Lucille	k1gFnSc1
<g/>
,	,	kIx,
dcera	dcera	k1gFnSc1
jejího	její	k3xOp3gMnSc2
syna	syn	k1gMnSc2
Patricka	Patricko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Dcera	dcera	k1gFnSc1
Claudia	Claudia	k1gFnSc1
absolvovala	absolvovat	k5eAaPmAgFnS
francouzskou	francouzský	k2eAgFnSc4d1
Sorbonnu	Sorbonna	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
se	se	k3xPyFc4
zúčastnila	zúčastnit	k5eAaPmAgFnS
demonstrace	demonstrace	k1gFnSc1
za	za	k7c4
legalizaci	legalizace	k1gFnSc4
rozvodů	rozvod	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
filmování	filmování	k1gNnSc6
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
kuřačkou	kuřačka	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
zasedala	zasedat	k5eAaImAgFnS
v	v	k7c6
porotě	porota	k1gFnSc6
České	český	k2eAgFnSc2d1
Miss	miss	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Bydlí	bydlet	k5eAaImIp3nS
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
velvyslankyní	velvyslankyně	k1gFnSc7
UNESCO	UNESCO	kA
pro	pro	k7c4
ženy	žena	k1gFnPc4
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
aktivistkou	aktivistka	k1gFnSc7
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
ženských	ženský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
v	v	k7c6
rodném	rodný	k2eAgNnSc6d1
Tunisku	Tunisko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Filmografie	filmografie	k1gFnSc1
(	(	kIx(
<g/>
výběr	výběr	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1958	#num#	k4
–	–	k?
Zmýlená	zmýlená	k1gFnSc1
neplatí	platit	k5eNaImIp3nS
(	(	kIx(
<g/>
I	i	k9
soliti	solit	k5eAaImF
ignoti	ignot	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Mario	Mario	k1gMnSc1
Monicelli	Monicelle	k1gFnSc6
</s>
<s>
1958	#num#	k4
–	–	k?
Tre	Tre	k1gMnSc1
straniere	stranirat	k5eAaPmIp3nS
a	a	k8xC
Roma	Rom	k1gMnSc2
(	(	kIx(
<g/>
Tre	Tre	k1gMnSc2
straniere	stranirat	k5eAaPmIp3nS
a	a	k8xC
Roma	Rom	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Claudio	Claudio	k1gMnSc1
Gora	Gora	k1gMnSc1
</s>
<s>
1958	#num#	k4
–	–	k?
Goha	Goha	k1gMnSc1
(	(	kIx(
<g/>
Goha	Goha	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Jean	Jean	k1gMnSc1
Bourgoin	Bourgoin	k1gMnSc1
</s>
<s>
1959	#num#	k4
–	–	k?
Vento	Vento	k1gNnSc4
del	del	k?
sud	sud	k1gInSc1
(	(	kIx(
<g/>
Vento	Vento	k1gNnSc1
del	del	k?
sud	suda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Enzo	Enzo	k6eAd1
Provenzale	Provenzal	k1gInSc5
</s>
<s>
1959	#num#	k4
–	–	k?
La	la	k1gNnSc2
prima	prima	k1gFnSc1
notte	notte	k5eAaPmIp2nP
(	(	kIx(
<g/>
La	la	k1gNnSc4
prima	prima	k6eAd1
notte	notte	k5eAaPmIp2nP
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Alberto	Alberta	k1gFnSc5
Cavalcanti	Cavalcant	k1gMnPc1
</s>
<s>
1959	#num#	k4
–	–	k?
Slepá	slepý	k2eAgFnSc1d1
ulička	ulička	k1gFnSc1
(	(	kIx(
<g/>
Il	Il	k1gFnSc1
magistrato	magistrato	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Luigi	Luig	k1gFnSc2
Zampa	Zampa	k1gFnSc1
</s>
<s>
1959	#num#	k4
–	–	k?
Un	Un	k1gFnSc2
maledetto	maledett	k2eAgNnSc1d1
imbroglio	imbroglio	k1gNnSc1
(	(	kIx(
<g/>
Un	Un	k1gMnSc5
maledetto	maledetta	k1gMnSc5
imbroglio	imbroglia	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Pietro	Pietro	k1gNnSc1
Germi	Ger	k1gFnPc7
</s>
<s>
1960	#num#	k4
–	–	k?
Znovu	znovu	k6eAd1
v	v	k7c6
nesnázích	nesnáz	k1gFnPc6
(	(	kIx(
<g/>
Audace	Audace	k1gFnSc1
colpo	colpa	k1gFnSc5
dei	dei	k?
soliti	solit	k5eAaImF
ignoti	ignot	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Nanni	Nanen	k2eAgMnPc1d1
Loy	Loy	k1gMnPc1
</s>
<s>
1960	#num#	k4
–	–	k?
Rocco	Rocco	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
bratři	bratr	k1gMnPc1
(	(	kIx(
<g/>
Rocco	Rocco	k6eAd1
e	e	k0
i	i	k9
suoi	suoi	k6eAd1
fratelli	fratelnout	k5eAaImAgMnP,k5eAaPmAgMnP
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Luchino	Luchino	k1gNnSc1
Visconti	Visconť	k1gFnSc2
</s>
<s>
1960	#num#	k4
–	–	k?
Napoleon	Napoleon	k1gMnSc1
(	(	kIx(
<g/>
Austerlitz	Austerlitz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Abel	Abel	k1gMnSc1
Gance	Gance	k1gMnSc1
</s>
<s>
1960	#num#	k4
–	–	k?
Krásný	krásný	k2eAgMnSc1d1
Antonio	Antonio	k1gMnSc1
(	(	kIx(
<g/>
Il	Il	k1gFnSc1
bell	bell	k1gInSc1
<g/>
'	'	kIx"
<g/>
Antonio	Antonio	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Mauro	Mauro	k1gNnSc1
Bolognini	Bolognin	k2eAgMnPc5d1
</s>
<s>
1960	#num#	k4
–	–	k?
Já	já	k3xPp1nSc1
<g/>
,	,	kIx,
delfín	delfín	k1gMnSc1
(	(	kIx(
<g/>
I	i	k9
delfini	delfin	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Francesco	Francesco	k6eAd1
Maselli	Maselle	k1gFnSc4
</s>
<s>
1961	#num#	k4
–	–	k?
Statek	statek	k1gInSc1
(	(	kIx(
<g/>
La	la	k1gNnSc1
viaccia	viaccium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Mauro	Mauro	k1gNnSc1
Bolognini	Bolognin	k2eAgMnPc5d1
</s>
<s>
1961	#num#	k4
–	–	k?
Děvče	děvče	k1gNnSc1
se	s	k7c7
zavazadlem	zavazadlo	k1gNnSc7
(	(	kIx(
<g/>
La	la	k1gNnSc1
ragazza	ragazz	k1gMnSc2
con	con	k?
la	la	k1gNnSc1
valigia	valigia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Valerio	Valerio	k6eAd1
Zurlini	Zurlin	k2eAgMnPc5d1
</s>
<s>
1962	#num#	k4
–	–	k?
Stárnutí	stárnutí	k1gNnSc1
(	(	kIx(
<g/>
Senilità	Senilità	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Mauro	Mauro	k1gNnSc1
Bolognini	Bolognin	k2eAgMnPc5d1
</s>
<s>
1962	#num#	k4
–	–	k?
Cartouche	Cartouche	k1gInSc1
(	(	kIx(
<g/>
Cartouche	Cartouche	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Philippe	Philipp	k1gInSc5
de	de	k?
Broca	Broca	k1gMnSc1
</s>
<s>
1963	#num#	k4
–	–	k?
8	#num#	k4
<g/>
½	½	k?
(	(	kIx(
<g/>
8	#num#	k4
<g/>
½	½	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Federico	Federico	k6eAd1
Fellini	Fellin	k2eAgMnPc1d1
</s>
<s>
1963	#num#	k4
–	–	k?
Růžový	růžový	k2eAgMnSc1d1
panter	panter	k1gMnSc1
(	(	kIx(
<g/>
The	The	k1gFnSc1
Pink	pink	k2eAgFnPc2d1
Panther	Panthra	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Blake	Blak	k1gFnSc2
Edwards	Edwardsa	k1gFnPc2
</s>
<s>
1963	#num#	k4
–	–	k?
Gepard	gepard	k1gMnSc1
(	(	kIx(
<g/>
Il	Il	k1gMnSc5
Gattopardo	Gattoparda	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Luchino	Luchino	k1gNnSc1
Visconti	Visconť	k1gFnSc2
</s>
<s>
1963	#num#	k4
–	–	k?
Bubovo	Bubův	k2eAgNnSc1d1
děvče	děvče	k1gNnSc1
(	(	kIx(
<g/>
La	la	k1gNnSc1
ragazza	ragazz	k1gMnSc2
di	di	k?
Bube	Bub	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Luigi	Luig	k1gFnSc2
Comencini	Comencin	k1gMnPc1
</s>
<s>
1964	#num#	k4
–	–	k?
Gli	Gli	k1gFnPc2
indifferenti	indifferent	k1gMnPc1
(	(	kIx(
<g/>
Gli	Gli	k1gFnPc2
indifferenti	indifferent	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Francesco	Francesco	k6eAd1
Maselli	Maselle	k1gFnSc4
</s>
<s>
1964	#num#	k4
–	–	k?
Cirkusový	cirkusový	k2eAgInSc4d1
svět	svět	k1gInSc4
(	(	kIx(
<g/>
Circus	Circus	k1gMnSc1
World	World	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Henry	henry	k1gInSc2
Hathaway	Hathawaa	k1gFnSc2
</s>
<s>
1964	#num#	k4
–	–	k?
Báječný	báječný	k2eAgMnSc1d1
paroháč	paroháč	k1gMnSc1
(	(	kIx(
<g/>
Il	Il	k1gMnSc1
magnifico	magnifico	k6eAd1
cornuto	cornout	k5eAaPmNgNnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Antonio	Antonio	k1gMnSc1
Pietrangeli	Pietrangel	k1gInSc6
</s>
<s>
1965	#num#	k4
–	–	k?
Naslepo	naslepo	k6eAd1
(	(	kIx(
<g/>
Blindfold	Blindfold	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Philip	Philip	k1gMnSc1
Dunne	Dunn	k1gInSc5
</s>
<s>
1965	#num#	k4
–	–	k?
Hvězdy	Hvězda	k1gMnPc4
Velkého	velký	k2eAgInSc2d1
vozu	vůz	k1gInSc2
(	(	kIx(
<g/>
Vaghe	Vaghe	k1gInSc1
stelle	stelle	k1gNnSc1
dell	dell	k1gInSc1
<g/>
'	'	kIx"
<g/>
Orsa	Orsa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Luchino	Luchino	k1gNnSc1
Visconti	Visconť	k1gFnSc2
</s>
<s>
1966	#num#	k4
–	–	k?
Ztracená	ztracený	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
(	(	kIx(
<g/>
Lost	Lost	k2eAgInSc1d1
Command	Command	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Mark	Mark	k1gMnSc1
Robson	Robson	k1gMnSc1
</s>
<s>
1966	#num#	k4
–	–	k?
Profesionálové	profesionál	k1gMnPc1
(	(	kIx(
<g/>
The	The	k1gFnSc1
Professionals	Professionalsa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Richard	Richard	k1gMnSc1
Brooks	Brooks	k1gInSc1
</s>
<s>
1967	#num#	k4
–	–	k?
Nedělejte	dělat	k5eNaImRp2nP
zmatek	zmatek	k1gInSc4
(	(	kIx(
<g/>
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Make	Make	k1gInSc1
Waves	Waves	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Alexander	Alexandra	k1gFnPc2
Mackendrick	Mackendricka	k1gFnPc2
</s>
<s>
1967	#num#	k4
–	–	k?
Una	Una	k1gFnSc1
rosa	rosa	k1gFnSc1
per	pero	k1gNnPc2
tutti	tutti	k2eAgInSc2d1
(	(	kIx(
<g/>
Una	Una	k1gFnSc1
rosa	rosa	k1gFnSc1
per	pero	k1gNnPc2
tutti	tutti	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Franco	Franco	k6eAd1
Rossi	Rosse	k1gFnSc4
</s>
<s>
1968	#num#	k4
–	–	k?
Tenkrát	tenkrát	k6eAd1
na	na	k7c6
Západě	západ	k1gInSc6
(	(	kIx(
<g/>
C	C	kA
<g/>
'	'	kIx"
<g/>
era	era	k?
una	una	k?
volta	volta	k1gFnSc1
il	il	k?
West	West	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Sergio	Sergio	k1gMnSc1
Leone	Leo	k1gMnSc5
</s>
<s>
1968	#num#	k4
–	–	k?
K	k	k7c3
čertu	čert	k1gMnSc3
s	s	k7c7
hrdiny	hrdina	k1gMnPc7
(	(	kIx(
<g/>
The	The	k1gMnPc7
Hell	Hell	k1gMnSc1
with	with	k1gMnSc1
Heroes	Heroes	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Joseph	Joseph	k1gMnSc1
Sargent	Sargent	k1gMnSc1
</s>
<s>
1968	#num#	k4
–	–	k?
Den	den	k1gInSc1
sovy	sova	k1gFnSc2
(	(	kIx(
<g/>
Il	Il	k1gMnSc1
giorno	giorno	k1gNnSc4
della	dell	k1gMnSc2
civetta	civett	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Damiano	Damiana	k1gFnSc5
Damiani	Damiaň	k1gFnSc5
</s>
<s>
1969	#num#	k4
–	–	k?
Ruba	Rubum	k1gNnSc2
al	ala	k1gFnPc2
prossimo	prossimo	k6eAd1
tuo	tuo	k?
(	(	kIx(
<g/>
Ruba	Ruba	k1gFnSc1
al	ala	k1gFnPc2
prossimo	prossimo	k6eAd1
tuo	tuo	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Francesco	Francesco	k6eAd1
Maselli	Maselle	k1gFnSc4
</s>
<s>
1969	#num#	k4
–	–	k?
Červený	červený	k2eAgInSc4d1
stan	stan	k1gInSc4
(	(	kIx(
<g/>
Krasnaja	Krasnaj	k2eAgFnSc1d1
palatka	palatka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Michail	Michail	k1gMnSc1
Kalatozov	Kalatozov	k1gInSc1
</s>
<s>
1969	#num#	k4
–	–	k?
Certo	Certo	k1gNnSc1
<g/>
,	,	kIx,
certissimo	certissimo	k6eAd1
<g/>
,	,	kIx,
anzi	anzi	k6eAd1
<g/>
...	...	k?
probabile	probabile	k6eAd1
(	(	kIx(
<g/>
Certo	Certo	k1gNnSc1
<g/>
,	,	kIx,
certissimo	certissimo	k6eAd1
<g/>
,	,	kIx,
anzi	anzi	k6eAd1
<g/>
...	...	k?
probabile	probabile	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Marcello	Marcello	k1gNnSc1
Fondato	Fondat	k2eAgNnSc1d1
</s>
<s>
1970	#num#	k4
–	–	k?
Gerardova	Gerardův	k2eAgNnSc2d1
dobrodružství	dobrodružství	k1gNnSc2
(	(	kIx(
<g/>
The	The	k1gMnSc1
Adventures	Adventures	k1gMnSc1
of	of	k?
Gerard	Gerard	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Jerzy	Jerza	k1gFnSc2
Skolimowski	Skolimowsk	k1gFnSc2
</s>
<s>
1971	#num#	k4
–	–	k?
Popsy	Pops	k1gInPc1
Pop	pop	k1gInSc1
(	(	kIx(
<g/>
Fuori	Fuori	k1gNnSc1
il	il	k?
malloppo	malloppa	k1gFnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Jean	Jean	k1gMnSc1
Herman	Herman	k1gMnSc1
</s>
<s>
1971	#num#	k4
–	–	k?
Petrolejářky	Petrolejářka	k1gFnSc2
(	(	kIx(
<g/>
Les	les	k1gInSc1
Pétroleuses	Pétroleuses	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Christian-Jaque	Christian-Jaque	k1gFnSc1
</s>
<s>
1971	#num#	k4
–	–	k?
Hezký	hezký	k2eAgInSc1d1
<g/>
,	,	kIx,
charakterní	charakterní	k2eAgMnSc1d1
Ital	Ital	k1gMnSc1
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
hledá	hledat	k5eAaImIp3nS
krajanku	krajanka	k1gFnSc4
za	za	k7c7
účelem	účel	k1gInSc7
sňatku	sňatek	k1gInSc2
(	(	kIx(
<g/>
Bello	Bello	k1gNnSc1
<g/>
,	,	kIx,
onesto	onesto	k6eAd1
<g/>
,	,	kIx,
emigrato	emigrato	k6eAd1
Australia	Australia	k1gFnSc1
sposerebbe	sposerebbat	k5eAaPmIp3nS
compaesana	compaesana	k1gFnSc1
illibata	illibata	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Luigi	Luig	k1gFnSc2
Zampa	Zampa	k1gFnSc1
</s>
<s>
1971	#num#	k4
–	–	k?
Audience	audience	k1gFnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
'	'	kIx"
<g/>
udienza	udienz	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Marco	Marco	k6eAd1
Ferreri	Ferrere	k1gFnSc4
</s>
<s>
1972	#num#	k4
–	–	k?
Smůla	smůla	k1gFnSc1
(	(	kIx(
<g/>
La	la	k0
Scoumoune	Scoumoun	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
José	Josá	k1gFnSc2
Giovanni	Giovann	k1gMnPc1
</s>
<s>
1974	#num#	k4
–	–	k?
Rodinný	rodinný	k2eAgInSc1d1
portrét	portrét	k1gInSc1
(	(	kIx(
<g/>
Gruppo	Gruppa	k1gFnSc5
di	di	k?
famiglia	famiglium	k1gNnSc2
in	in	k?
un	un	k?
interno	interna	k1gFnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Luchino	Luchino	k1gNnSc1
Visconti	Visconť	k1gFnSc2
</s>
<s>
1974	#num#	k4
–	–	k?
I	i	k9
guappi	guappi	k6eAd1
(	(	kIx(
<g/>
I	i	k9
guappi	guappi	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Pasquale	Pasquala	k1gFnSc3
Squitieri	Squitiere	k1gFnSc4
</s>
<s>
1975	#num#	k4
–	–	k?
Qui	Qui	k1gMnSc1
comincia	comincius	k1gMnSc2
l	l	kA
<g/>
'	'	kIx"
<g/>
avventura	avventura	k1gFnSc1
(	(	kIx(
<g/>
Qui	Qui	k1gMnSc1
comincia	comincius	k1gMnSc2
l	l	kA
<g/>
'	'	kIx"
<g/>
avventura	avventura	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Carlo	Carlo	k1gNnSc1
Di	Di	k1gMnSc2
Palma	palma	k1gFnSc1
</s>
<s>
1975	#num#	k4
–	–	k?
Po	po	k7c6
půlnoci	půlnoc	k1gFnSc6
to	ten	k3xDgNnSc1
roztočíme	roztočit	k5eAaPmIp1nP
(	(	kIx(
<g/>
A	a	k8xC
mezzanotte	mezzanotte	k5eAaPmIp2nP
va	va	k0wR
la	la	k1gNnSc2
ronda	ronda	k1gFnSc1
del	del	k?
piacere	piacrat	k5eAaPmIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Marcello	Marcello	k1gNnSc1
Fondato	Fondata	k1gFnSc5
</s>
<s>
1975	#num#	k4
–	–	k?
Libera	Liber	k1gMnSc4
<g/>
,	,	kIx,
amore	amor	k1gMnSc5
mio	mio	k?
<g/>
...	...	k?
(	(	kIx(
<g/>
Libera	Libera	k1gFnSc1
<g/>
,	,	kIx,
amore	amor	k1gMnSc5
mio	mio	k?
<g/>
...	...	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Mauro	Mauro	k1gNnSc1
Bolognini	Bolognin	k2eAgMnPc5d1
</s>
<s>
1976	#num#	k4
–	–	k?
Společný	společný	k2eAgInSc4d1
pocit	pocit	k1gInSc4
studu	stud	k1gInSc2
(	(	kIx(
<g/>
Il	Il	k1gMnSc5
comune	comun	k1gMnSc5
senso	sensa	k1gFnSc5
del	del	k?
pudore	pudor	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Alberto	Alberta	k1gFnSc5
Sordi	Sord	k1gMnPc5
</s>
<s>
1977	#num#	k4
–	–	k?
Železný	Železný	k1gMnSc1
šéf	šéf	k1gMnSc1
(	(	kIx(
<g/>
Il	Il	k1gMnSc1
prefetto	prefett	k2eAgNnSc1d1
di	di	k?
ferro	ferro	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Pasquale	Pasquala	k1gFnSc3
Squitieri	Squitiere	k1gFnSc4
</s>
<s>
1977	#num#	k4
–	–	k?
Sbohem	sbohem	k0
a	a	k8xC
amen	amen	k1gNnPc2
(	(	kIx(
<g/>
Goodbye	Goodbye	k1gFnSc1
&	&	k?
Amen	amen	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Damiano	Damiana	k1gFnSc5
Damiani	Damiaň	k1gFnSc6
</s>
<s>
1977	#num#	k4
–	–	k?
Ježíš	Ježíš	k1gMnSc1
Nazaretský	nazaretský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Gesù	Gesù	k1gMnSc1
di	di	k?
Nazareth	Nazareth	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Franco	Franco	k6eAd1
Zeffirelli	Zeffirelle	k1gFnSc4
</s>
<s>
1978	#num#	k4
–	–	k?
La	la	k1gNnSc2
Part	parta	k1gFnPc2
du	du	k?
feu	feu	k?
(	(	kIx(
<g/>
Fire	Fir	k1gMnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Share	Shar	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Etienne	Etienn	k1gMnSc5
Périer	Périero	k1gNnPc2
</s>
<s>
1978	#num#	k4
–	–	k?
Dívka	dívka	k1gFnSc1
v	v	k7c6
modrém	modrý	k2eAgInSc6d1
sametu	samet	k1gInSc6
(	(	kIx(
<g/>
La	la	k1gNnSc6
Petite	petit	k1gInSc5
fille	fill	k1gMnPc4
en	en	k?
velours	velours	k6eAd1
bleu	bleu	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Alan	Alan	k1gMnSc1
Bridges	Bridges	k1gMnSc1
</s>
<s>
1978	#num#	k4
–	–	k?
Corleone	Corleon	k1gInSc5
(	(	kIx(
<g/>
Corleone	Corleon	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Pasquale	Pasquala	k1gFnSc3
Squitieri	Squitiere	k1gFnSc4
</s>
<s>
1978	#num#	k4
–	–	k?
L	L	kA
<g/>
'	'	kIx"
<g/>
arma	arma	k1gMnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
'	'	kIx"
<g/>
arma	arma	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Pasquale	Pasquala	k1gFnSc3
Squitieri	Squitiere	k1gFnSc4
</s>
<s>
1979	#num#	k4
–	–	k?
Útěk	útěk	k1gInSc1
do	do	k7c2
Atén	Atény	k1gFnPc2
(	(	kIx(
<g/>
Escape	Escap	k1gInSc5
to	ten	k3xDgNnSc4
Athena	Athena	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
George	Georg	k1gFnSc2
Pan	Pan	k1gMnSc1
Cosmatos	Cosmatos	k1gMnSc1
</s>
<s>
1981	#num#	k4
–	–	k?
Salamendr	Salamendr	k1gInSc1
(	(	kIx(
<g/>
The	The	k1gMnSc1
Salamander	Salamander	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Peter	Peter	k1gMnSc1
Zinner	Zinner	k1gMnSc1
</s>
<s>
1981	#num#	k4
–	–	k?
Dvě	dva	k4xCgFnPc1
tváře	tvář	k1gFnPc1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
La	la	k1gNnSc1
Pelle	Pelle	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Liliana	Liliana	k1gFnSc1
Cavaniová	Cavaniový	k2eAgFnSc1d1
</s>
<s>
1982	#num#	k4
–	–	k?
Fitzcarraldo	Fitzcarraldo	k1gNnSc1
(	(	kIx(
<g/>
Fitzcarraldo	Fitzcarraldo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Werner	Werner	k1gMnSc1
Herzog	Herzog	k1gMnSc1
</s>
<s>
1982	#num#	k4
–	–	k?
Dárek	dárek	k1gInSc1
(	(	kIx(
<g/>
Le	Le	k1gFnSc1
Cadeau	Cadeaus	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Michel	Michel	k1gMnSc1
Lang	Lang	k1gMnSc1
</s>
<s>
1983	#num#	k4
–	–	k?
Drsný	drsný	k2eAgMnSc1d1
chlapík	chlapík	k1gMnSc1
(	(	kIx(
<g/>
Le	Le	k1gMnSc1
ruffian	ruffian	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
José	Josá	k1gFnSc2
Giovanni	Giovann	k1gMnPc1
</s>
<s>
1984	#num#	k4
–	–	k?
Jindřich	Jindřich	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Enrico	Enrico	k1gNnSc1
IV	Iva	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Marco	Marco	k1gMnSc1
Bellocchio	Bellocchio	k1gMnSc1
</s>
<s>
1984	#num#	k4
–	–	k?
Claretta	Claretta	k1gMnSc1
(	(	kIx(
<g/>
Claretta	Claretta	k1gMnSc1
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Pasquale	Pasquala	k1gFnSc3
Squitieri	Squitiere	k1gFnSc4
</s>
<s>
1985	#num#	k4
–	–	k?
Příští	příští	k2eAgNnSc1d1
léto	léto	k1gNnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
'	'	kIx"
<g/>
Été	Été	k1gMnSc1
prochain	prochain	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Nadine	Nadin	k1gMnSc5
Trintignantová	Trintignantový	k2eAgNnPc1d1
</s>
<s>
1985	#num#	k4
–	–	k?
Žena	žena	k1gFnSc1
snů	sen	k1gInPc2
(	(	kIx(
<g/>
La	la	k1gNnSc1
Donna	donna	k1gFnSc1
delle	delle	k1gFnSc1
meraviglie	meraviglie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Alberto	Alberta	k1gFnSc5
Bevilacqua	Bevilacqu	k2eAgNnPc1d1
</s>
<s>
1986	#num#	k4
–	–	k?
La	la	k1gNnSc1
Storia	Storium	k1gNnSc2
(	(	kIx(
<g/>
La	la	k1gNnSc1
Storia	Storium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Luigi	Luig	k1gFnSc2
Comencini	Comencin	k1gMnPc1
</s>
<s>
1986	#num#	k4
–	–	k?
Naso	Naso	k1gMnSc1
di	di	k?
cane	cane	k1gInSc1
(	(	kIx(
<g/>
Naso	Naso	k1gNnSc1
di	di	k?
cane	can	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Pasquale	Pasquala	k1gFnSc3
Squitieri	Squitiere	k1gFnSc4
</s>
<s>
2012	#num#	k4
–	–	k?
Deauville	Deauville	k1gInSc1
(	(	kIx(
<g/>
Deauville	Deauville	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Miguel	Miguel	k1gMnSc1
Cruz	Cruz	k1gMnSc1
Carretero	Carretero	k1gNnSc4
</s>
<s>
2012	#num#	k4
–	–	k?
Diventare	Diventar	k1gMnSc5
italiano	italiana	k1gFnSc5
con	con	k?
la	la	k1gNnSc2
signora	signor	k1gMnSc2
Enrica	Enricus	k1gMnSc2
(	(	kIx(
<g/>
Diventare	Diventar	k1gMnSc5
italiano	italiana	k1gFnSc5
con	con	k?
la	la	k1gNnSc2
signora	signor	k1gMnSc2
Enrica	Enricus	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Ali	Ali	k1gMnSc1
Ilhan	Ilhan	k1gMnSc1
</s>
<s>
2012	#num#	k4
–	–	k?
Effie	Effie	k1gFnSc1
(	(	kIx(
<g/>
Effie	Effie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Richard	Richard	k1gMnSc1
Laxton	Laxton	k1gInSc1
</s>
<s>
2012	#num#	k4
–	–	k?
Piccolina	Piccolin	k2eAgFnSc1d1
bella	bella	k1gFnSc1
(	(	kIx(
<g/>
Piccolina	Piccolin	k2eAgFnSc1d1
bella	bella	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Anna	Anna	k1gFnSc1
Scaglione	Scaglion	k1gInSc5
</s>
<s>
2012	#num#	k4
–	–	k?
Umělec	umělec	k1gMnSc1
a	a	k8xC
modelka	modelka	k1gFnSc1
(	(	kIx(
<g/>
El	Ela	k1gFnPc2
artista	artista	k1gMnSc1
y	y	k?
la	la	k1gNnSc1
modelo	modet	k5eAaPmAgNnS,k5eAaBmAgNnS,k5eAaImAgNnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Fernando	Fernanda	k1gFnSc5
Trueba	Trueba	k1gMnSc1
</s>
<s>
2013	#num#	k4
–	–	k?
La	la	k1gNnSc2
montagna	montagen	k2eAgFnSc1d1
silenziosa	silenziosa	k1gFnSc1
(	(	kIx(
<g/>
La	la	k1gNnSc1
montagna	montagna	k1gFnSc1
silenziosa	silenziosa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Ernst	Ernst	k1gMnSc1
Gossner	Gossner	k1gMnSc1
</s>
<s>
2013	#num#	k4
–	–	k?
Joy	Joy	k1gMnSc3
de	de	k?
V.	V.	kA
(	(	kIx(
<g/>
Joy	Joy	k1gMnSc3
de	de	k?
V.	V.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Nadia	Nadium	k1gNnSc2
Szold	Szolda	k1gFnPc2
</s>
<s>
2013	#num#	k4
–	–	k?
Ultima	ultimo	k1gNnSc2
fermata	fermata	k1gFnSc1
(	(	kIx(
<g/>
Ultima	ultimo	k1gNnSc2
fermata	fermata	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Giambattista	Giambattista	k1gMnSc1
Assanti	Assant	k1gMnPc5
</s>
<s>
2013	#num#	k4
–	–	k?
Più	Più	k1gMnSc1
buio	buio	k6eAd1
di	di	k?
mezzanotte	mezzanotte	k5eAaPmIp2nP
non	non	k?
può	può	k?
fare	fare	k1gInSc1
(	(	kIx(
<g/>
Più	Più	k1gMnSc1
buio	buio	k6eAd1
di	di	k?
mezzanotte	mezzanotte	k5eAaPmIp2nP
non	non	k?
può	può	k?
fare	fare	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Sebastiano	Sebastiana	k1gFnSc5
Riso	Risa	k1gFnSc5
</s>
<s>
2013	#num#	k4
–	–	k?
The	The	k1gMnSc1
third	third	k1gMnSc1
person	persona	k1gFnPc2
(	(	kIx(
<g/>
The	The	k1gFnSc1
third	thirda	k1gFnPc2
person	persona	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Paul	Paul	k1gMnSc1
Haggis	Haggis	k1gInSc1
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalová	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
</s>
<s>
Filmová	filmový	k2eAgNnPc4d1
ocenění	ocenění	k1gNnPc4
</s>
<s>
1984	#num#	k4
–	–	k?
cena	cena	k1gFnSc1
za	za	k7c4
nejlepší	dobrý	k2eAgFnSc4d3
herečku	herečka	k1gFnSc4
za	za	k7c4
film	film	k1gInSc4
Claretta	Clarett	k1gInSc2
na	na	k7c6
Benátském	benátský	k2eAgInSc6d1
filmovém	filmový	k2eAgInSc6d1
festivalu	festival	k1gInSc6
</s>
<s>
1993	#num#	k4
–	–	k?
Zlatý	zlatý	k2eAgMnSc1d1
lev	lev	k1gMnSc1
za	za	k7c4
přínos	přínos	k1gInSc4
světové	světový	k2eAgFnSc3d1
kinematografii	kinematografie	k1gFnSc3
na	na	k7c6
Benátském	benátský	k2eAgInSc6d1
filmovém	filmový	k2eAgInSc6d1
festivalu	festival	k1gInSc6
</s>
<s>
2002	#num#	k4
–	–	k?
Čestný	čestný	k2eAgMnSc1d1
Zlatý	zlatý	k2eAgMnSc1d1
medvěd	medvěd	k1gMnSc1
za	za	k7c4
celoživotní	celoživotní	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
na	na	k7c6
Berlínském	berlínský	k2eAgInSc6d1
filmovém	filmový	k2eAgInSc6d1
festivalu	festival	k1gInSc6
</s>
<s>
2006	#num#	k4
–	–	k?
cena	cena	k1gFnSc1
Kristián	Kristián	k1gMnSc1
za	za	k7c4
přínos	přínos	k1gInSc4
světové	světový	k2eAgFnSc3d1
kinematografii	kinematografie	k1gFnSc3
na	na	k7c6
festivalu	festival	k1gInSc6
Febiofest	Febiofest	k1gFnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2011	#num#	k4
–	–	k?
Zlatý	zlatý	k2eAgMnSc1d1
leopard	leopard	k1gMnSc1
za	za	k7c4
celoživotní	celoživotní	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
na	na	k7c6
Filmovém	filmový	k2eAgInSc6d1
festivalu	festival	k1gInSc6
v	v	k7c6
Locarnu	Locarno	k1gNnSc6
</s>
<s>
Státní	státní	k2eAgNnSc1d1
vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Italská	italský	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
Komtur	komtur	k1gMnSc1
Řádu	řád	k1gInSc2
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
o	o	k7c4
Italskou	italský	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
</s>
<s>
Velkodůstojník	Velkodůstojník	k1gMnSc1
Řádu	řád	k1gInSc2
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
o	o	k7c4
Italskou	italský	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
Důstojník	důstojník	k1gMnSc1
čestné	čestný	k2eAgFnSc2d1
legie	legie	k1gFnSc2
</s>
<s>
Rytíř	Rytíř	k1gMnSc1
čestné	čestný	k2eAgFnSc2d1
legie	legie	k1gFnSc2
</s>
<s>
Velitel	velitel	k1gMnSc1
čestné	čestný	k2eAgFnSc2d1
legie	legie	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
TUŠKOVÁ	TUŠKOVÁ	kA
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sexsymbol	Sexsymbol	k1gInSc1
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
Claudia	Claudia	k1gFnSc1
Cardinale	Cardinale	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vybere	vybrat	k5eAaPmIp3nS
Českou	český	k2eAgFnSc4d1
Miss	miss	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blesk	blesk	k1gInSc1
<g/>
,	,	kIx,
2012-03-07	2012-03-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
LANDOVSKÁ	LANDOVSKÁ	kA
<g/>
,	,	kIx,
Helena	Helena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalová	k1gFnSc1
se	se	k3xPyFc4
s	s	k7c7
věkem	věk	k1gInSc7
netají	tajit	k5eNaImIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2004-05-13	2004-05-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
V	v	k7c6
porotě	porota	k1gFnSc6
České	český	k2eAgFnSc2d1
Miss	miss	k1gFnSc2
bude	být	k5eAaImBp3nS
i	i	k9
slavná	slavný	k2eAgFnSc1d1
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalová	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2012-03-31	2012-03-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
HERRMANNOVÁ	HERRMANNOVÁ	kA
<g/>
,	,	kIx,
Nela	Nela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lifting	Lifting	k1gInSc1
není	být	k5eNaImIp3nS
třeba	třeba	k6eAd1
<g/>
,	,	kIx,
stačí	stačit	k5eAaBmIp3nS
úsměv	úsměv	k1gInSc4
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalová	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2012-03-29	2012-03-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
SPÁČILOVÁ	Spáčilová	k1gFnSc1
<g/>
,	,	kIx,
Tereza	Tereza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalová	k1gFnSc1
exkluzívně	exkluzívně	k6eAd1
pro	pro	k7c4
Reflex	reflex	k1gInSc4
<g/>
:	:	kIx,
Nahých	nahý	k2eAgFnPc2d1
hereček	herečka	k1gFnPc2
je	být	k5eAaImIp3nS
mi	já	k3xPp1nSc3
líto	líto	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reflex	reflex	k1gInSc1
<g/>
,	,	kIx,
2012-04-16	2012-04-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalová	k1gFnSc1
<g/>
:	:	kIx,
filmová	filmový	k2eAgFnSc1d1
sexbomba	sexbomba	k1gFnSc1
slaví	slavit	k5eAaImIp3nS
75	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
,	,	kIx,
2013-04-14	2013-04-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalová	k1gFnSc1
slaví	slavit	k5eAaImIp3nS
narozeniny	narozeniny	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2008-04-15	2008-04-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Parade	Parad	k1gInSc5
Magazine	Magazin	k1gInSc5
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1967	#num#	k4
<g/>
↑	↑	k?
lovegoddess	lovegoddess	k1gInSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gNnSc1
–	–	k?
love	lov	k1gInSc5
goddess	goddessit	k5eAaPmRp2nS
Resources	Resources	k1gInSc1
and	and	k?
Information	Information	k1gInSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
CARDINALOVÁ	CARDINALOVÁ	kA
<g/>
,	,	kIx,
Claudia	Claudia	k1gFnSc1
<g/>
;	;	kIx,
MORIOVÁ	MORIOVÁ	kA
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
Maria	Maria	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Já	já	k3xPp1nSc1
<g/>
,	,	kIx,
Claudia	Claudia	k1gFnSc1
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
<g/>
,	,	kIx,
Claudie	Claudia	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Albatros	albatros	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
215	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1551	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalový	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalová	k1gFnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalový	k2eAgFnSc1d1
</s>
<s>
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalový	k2eAgFnSc1d1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalový	k2eAgFnSc1d1
na	na	k7c6
Kinoboxu	Kinobox	k1gInSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalový	k2eAgFnSc1d1
ve	v	k7c6
Filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalový	k2eAgFnSc1d1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalový	k2eAgFnSc1d1
v	v	k7c6
rozhovoru	rozhovor	k1gInSc6
pro	pro	k7c4
Reflex	reflex	k1gInSc4
<g/>
,	,	kIx,
duben	duben	k1gInSc4
2012	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Donatellův	Donatellův	k2eAgMnSc1d1
David	David	k1gMnSc1
pro	pro	k7c4
nejlepší	dobrý	k2eAgFnSc4d3
herečku	herečka	k1gFnSc4
1956	#num#	k4
<g/>
–	–	k?
<g/>
1959	#num#	k4
</s>
<s>
Gina	Gin	k2eAgFnSc1d1
Lollobrigida	Lollobrigida	k1gFnSc1
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Anna	Anna	k1gFnSc1
Magnani	Magnaň	k1gFnSc6
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Anna	Anna	k1gFnSc1
Magnani	Magnaň	k1gFnSc6
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
)	)	kIx)
1960	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
</s>
<s>
Sophia	Sophia	k1gFnSc1
Lorenová	Lorenová	k1gFnSc1
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Silvana	Silvana	k1gFnSc1
Manganová	manganový	k2eAgFnSc1d1
/	/	kIx~
Gina	Gin	k2eAgFnSc1d1
Lollobrigida	Lollobrigida	k1gFnSc1
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sophia	Sophia	k1gFnSc1
Lorenová	Lorenová	k1gFnSc1
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sophia	Sophia	k1gFnSc1
Lorenová	Lorenová	k1gFnSc1
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Giulietta	Giulietta	k1gMnSc1
Masina	Masina	k1gMnSc1
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Silvana	Silvana	k1gFnSc1
Manganová	manganový	k2eAgFnSc1d1
/	/	kIx~
Graziella	Graziella	k1gFnSc1
Granata	Granata	k1gFnSc1
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalová	k1gFnSc1
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gina	Gin	k2eAgFnSc1d1
Lollobrigida	Lollobrigida	k1gFnSc1
/	/	kIx~
Monica	Monica	k1gFnSc1
Vittiová	Vittiový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
1970	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
</s>
<s>
Sophia	Sophia	k1gFnSc1
Lorenová	Lorenová	k1gFnSc1
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Florinda	Florinda	k1gFnSc1
Bolkanová	Bolkanová	k1gFnSc1
/	/	kIx~
Monica	Monica	k1gFnSc1
Vittiová	Vittiový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalová	k1gFnSc1
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Florinda	Florinda	k1gFnSc1
Bolkanová	Bolkanová	k1gFnSc1
/	/	kIx~
Silvana	Silvana	k1gFnSc1
Manganová	manganový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sophia	Sophia	k1gFnSc1
Lorenová	Lorenová	k1gFnSc1
/	/	kIx~
Monica	Monica	k1gFnSc1
Vittiová	Vittiový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mariangela	Mariangela	k1gFnSc1
Melato	Melat	k2eAgNnSc1d1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Monica	Monica	k1gFnSc1
Vittiová	Vittiový	k2eAgFnSc1d1
/	/	kIx~
Agostina	Agostin	k2eAgFnSc1d1
Belli	Bell	k1gMnPc7
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mariangela	Mariangela	k1gFnSc1
Melato	Melat	k2eAgNnSc1d1
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mariangela	Mariangela	k1gFnSc1
Melato	Melat	k2eAgNnSc1d1
/	/	kIx~
Sophia	Sophia	k1gFnSc1
Lorenová	Lorenová	k1gFnSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Monica	Monica	k1gFnSc1
Vittiová	Vittiový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
1980	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
</s>
<s>
Virna	Virna	k6eAd1
Lisiová	Lisiový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mariangela	Mariangela	k1gFnSc1
Melato	Melat	k2eAgNnSc1d1
/	/	kIx~
Valeria	Valerium	k1gNnPc1
D	D	kA
<g/>
'	'	kIx"
<g/>
Obici	Obice	k1gMnSc3
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Eleonora	Eleonora	k1gFnSc1
Giorgi	Giorg	k1gFnSc2
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Giuliana	Giulian	k1gMnSc2
De	De	k?
Sio	Sio	k1gMnSc2
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lina	Lina	k1gFnSc1
Sastri	Sastr	k1gFnSc2
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lina	Lina	k1gFnSc1
Sastri	Sastr	k1gFnSc2
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ángela	Ángela	k1gFnSc1
Molinová	molinový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Liv	Liv	k1gFnSc1
Ullmannová	Ullmannová	k1gFnSc1
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Elena	Elena	k1gFnSc1
Safonova	Safonův	k2eAgFnSc1d1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Stefania	Stefanium	k1gNnSc2
Sandrelliová	Sandrelliový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
1990	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
</s>
<s>
Elena	Elena	k1gFnSc1
Sofia	Sofia	k1gFnSc1
Ricci	Ricce	k1gMnSc6
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Margherita	Margherita	k1gFnSc1
Buy	Buy	k1gFnSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Giuliana	Giulian	k1gMnSc2
De	De	k?
Sio	Sio	k1gMnSc2
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Antonella	Antonella	k1gMnSc1
Ponziani	Ponzian	k1gMnPc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Asia	Asia	k1gFnSc1
Argento	Argento	k1gNnSc4
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Anna	Anna	k1gFnSc1
Bonaiuto	Bonaiut	k2eAgNnSc1d1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Valeria	Valerium	k1gNnSc2
Bruni	Brun	k1gMnPc1
Tedeschiová	Tedeschiový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Asia	Asia	k1gFnSc1
Argento	Argento	k1gNnSc4
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Valeria	Valerium	k1gNnSc2
Bruni	Brun	k1gMnPc1
Tedeschiová	Tedeschiový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Margherita	Margherita	k1gFnSc1
Buy	Buy	k1gFnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
2000	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
</s>
<s>
Licia	Licia	k1gFnSc1
Maglietta	Maglietta	k1gFnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Laura	Laura	k1gFnSc1
Morante	Morant	k1gMnSc5
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Marina	Marina	k1gFnSc1
Confalone	Confalon	k1gInSc5
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Giovanna	Giovanna	k1gFnSc1
Mezzogiorno	Mezzogiorno	k1gNnSc4
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Penélope	Penélop	k1gInSc5
Cruzová	Cruzová	k1gFnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Barbora	Barbora	k1gFnSc1
Bobulova	Bobulův	k2eAgFnSc1d1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Valeria	Valerium	k1gNnSc2
Golinová	Golinová	k1gFnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ksenija	Ksenija	k1gFnSc1
Rappaport	Rappaport	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Margherita	Margherita	k1gFnSc1
Buy	Buy	k1gFnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alba	album	k1gNnSc2
Rohrwacher	Rohrwachra	k1gFnPc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
od	od	k7c2
2010	#num#	k4
</s>
<s>
Micaela	Micaela	k1gFnSc1
Ramazzotti	Ramazzotť	k1gFnSc2
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Paola	Paola	k1gFnSc1
Cortellesiová	Cortellesiový	k2eAgFnSc1d1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zhao	Zhao	k1gMnSc1
Tao	Tao	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Margherita	Margherita	k1gFnSc1
Buy	Buy	k1gFnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Valeria	Valerium	k1gNnSc2
Bruni	Brun	k1gMnPc1
Tedeschiová	Tedeschiový	k2eAgFnSc1d1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Margherita	Margherita	k1gFnSc1
Buy	Buy	k1gFnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ilenia	Ilenium	k1gNnSc2
Pastorelli	Pastorell	k1gMnSc6
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Valeria	Valerium	k1gNnSc2
Bruni	Brun	k1gMnPc1
Tedeschiová	Tedeschiový	k2eAgFnSc1d1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jasmine	Jasmin	k1gInSc5
Trincaová	Trincaová	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Elena	Elena	k1gFnSc1
Sofia	Sofia	k1gFnSc1
Ricci	Ricce	k1gMnSc6
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jasmine	Jasmin	k1gInSc5
Trincaová	Trincaová	k1gFnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000700278	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118667092	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1037	#num#	k4
0281	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
96011712	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
7497466	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
96011712	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
|	|	kIx~
Itálie	Itálie	k1gFnSc1
</s>
