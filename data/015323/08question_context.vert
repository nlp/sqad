<s desamb="1">
K	k	k7c3
její	její	k3xOp3gFnSc3
půvabné	půvabný	k2eAgFnSc3d1
tváři	tvář	k1gFnSc3
a	a	k8xC
tělu	tělo	k1gNnSc3
patří	patřit	k5eAaImIp3nS
tedy	tedy	k9
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
poměrně	poměrně	k6eAd1
drsný	drsný	k2eAgInSc4d1
hlas	hlas	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
podle	podle	k7c2
většiny	většina	k1gFnSc2
filmových	filmový	k2eAgMnPc2d1
režisérů	režisér	k1gMnPc2
vůbec	vůbec	k9
neladil	ladit	k5eNaImAgMnS
s	s	k7c7
jejím	její	k3xOp3gInSc7
exotickým	exotický	k2eAgInSc7d1
zevnějškem	zevnějšek	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
ji	on	k3xPp3gFnSc4
nechali	nechat	k5eAaPmAgMnP
dabovat	dabovat	k5eAaBmF
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
–	–	k?
a	a	k8xC
to	ten	k3xDgNnSc1
dokonce	dokonce	k9
i	i	k9
v	v	k7c6
italštině	italština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Claudia	Claudia	k1gFnSc1
Cardinalová	Cardinalová	k1gFnSc1
<g/>
,	,	kIx,
nepřechýleně	přechýleně	k6eNd1
Claudia	Claudia	k1gFnSc1
Cardinale	Cardinale	k1gFnSc2
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
označována	označován	k2eAgNnPc4d1
CC	CC	kA
<g/>
,	,	kIx,
*	*	kIx~
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1938	#num#	k4
Tunis	Tunis	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
italská	italský	k2eAgFnSc1d1
divadelní	divadelní	k2eAgFnSc1d1
a	a	k8xC
filmová	filmový	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
hereckou	herecký	k2eAgFnSc7d1
legendou	legenda	k1gFnSc7
a	a	k8xC
sexuálním	sexuální	k2eAgInSc7d1
symbolem	symbol	k1gInSc7
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
60	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>