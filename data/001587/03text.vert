<s>
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Ernst	Ernst	k1gMnSc1	Ernst
Pauli	Paule	k1gFnSc4	Paule
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
Curych	Curych	k1gInSc1	Curych
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
teorii	teorie	k1gFnSc4	teorie
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
kvantovou	kvantový	k2eAgFnSc4d1	kvantová
mechaniku	mechanika	k1gFnSc4	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
formulaci	formulace	k1gFnSc4	formulace
tzv.	tzv.	kA	tzv.
Pauliho	Pauli	k1gMnSc2	Pauli
vylučovacího	vylučovací	k2eAgInSc2d1	vylučovací
principu	princip	k1gInSc2	princip
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
byl	být	k5eAaImAgMnS	být
nadaný	nadaný	k2eAgMnSc1d1	nadaný
na	na	k7c4	na
řešení	řešení	k1gNnPc4	řešení
matematických	matematický	k2eAgFnPc2d1	matematická
i	i	k8xC	i
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
úloh	úloha	k1gFnPc2	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
fyziku	fyzika	k1gFnSc4	fyzika
v	v	k7c6	v
Göttingenu	Göttingen	k1gInSc6	Göttingen
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1923-28	[number]	k4	1923-28
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
rodil	rodit	k5eAaImAgInS	rodit
nový	nový	k2eAgInSc1d1	nový
obor	obor	k1gInSc1	obor
kvantová	kvantový	k2eAgFnSc1d1	kvantová
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
zformuloval	zformulovat	k5eAaPmAgMnS	zformulovat
tzv.	tzv.	kA	tzv.
Pauliho	Pauli	k1gMnSc2	Pauli
vylučovací	vylučovací	k2eAgInSc4d1	vylučovací
princip	princip	k1gInSc4	princip
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
v	v	k7c6	v
jednom	jeden	k4xCgMnSc6	jeden
a	a	k8xC	a
témže	týž	k3xTgInSc6	týž
stavu	stav	k1gInSc6	stav
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
2	[number]	k4	2
elektrony	elektron	k1gInPc1	elektron
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
všechna	všechen	k3xTgNnPc1	všechen
kvantová	kvantový	k2eAgNnPc1d1	kvantové
čísla	číslo	k1gNnPc1	číslo
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
,	,	kIx,	,
l	l	kA	l
<g/>
,	,	kIx,	,
m	m	kA	m
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
)	)	kIx)	)
stejná	stejná	k1gFnSc1	stejná
resp.	resp.	kA	resp.
každý	každý	k3xTgInSc4	každý
kvantový	kvantový	k2eAgInSc4d1	kvantový
stav	stav	k1gInSc4	stav
v	v	k7c6	v
atomu	atom	k1gInSc6	atom
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obsazen	obsadit	k5eAaPmNgInS	obsadit
jen	jen	k9	jen
jedním	jeden	k4xCgInSc7	jeden
elektronem	elektron	k1gInSc7	elektron
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
matematicky	matematicky	k6eAd1	matematicky
předpověděl	předpovědět	k5eAaPmAgInS	předpovědět
existenci	existence	k1gFnSc4	existence
neutrina	neutrino	k1gNnSc2	neutrino
<g/>
,	,	kIx,	,
když	když	k8xS	když
správně	správně	k6eAd1	správně
uhodl	uhodnout	k5eAaPmAgMnS	uhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
mizící	mizící	k2eAgFnSc4d1	mizící
energii	energie	k1gFnSc4	energie
odnáší	odnášet	k5eAaImIp3nS	odnášet
nějaká	nějaký	k3yIgFnSc1	nějaký
neznámá	známý	k2eNgFnSc1d1	neznámá
částice	částice	k1gFnSc1	částice
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
částice	částice	k1gFnSc1	částice
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
lehká	lehký	k2eAgFnSc1d1	lehká
a	a	k8xC	a
elektricky	elektricky	k6eAd1	elektricky
neutrální	neutrální	k2eAgFnPc1d1	neutrální
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Enrico	Enrico	k6eAd1	Enrico
Fermi	Fer	k1gFnPc7	Fer
jako	jako	k8xS	jako
neutrino	neutrino	k1gNnSc1	neutrino
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikové	fyzik	k1gMnPc1	fyzik
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
existenci	existence	k1gFnSc6	existence
nechtěli	chtít	k5eNaImAgMnP	chtít
dlouho	dlouho	k6eAd1	dlouho
uvěřit	uvěřit	k5eAaPmF	uvěřit
<g/>
.	.	kIx.	.
</s>
<s>
Neutrino	neutrino	k1gNnSc1	neutrino
s	s	k7c7	s
ostatní	ostatní	k2eAgFnSc7d1	ostatní
hmotou	hmota	k1gFnSc7	hmota
skoro	skoro	k6eAd1	skoro
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgNnSc1d1	těžké
ho	on	k3xPp3gInSc4	on
zachytit	zachytit	k5eAaPmF	zachytit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
existenci	existence	k1gFnSc4	existence
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
potvrdit	potvrdit	k5eAaPmF	potvrdit
až	až	k9	až
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
v	v	k7c6	v
Princetonu	Princeton	k1gInSc6	Princeton
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
vylučovací	vylučovací	k2eAgInSc4d1	vylučovací
princip	princip	k1gInSc4	princip
získal	získat	k5eAaPmAgMnS	získat
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
společně	společně	k6eAd1	společně
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bývalým	bývalý	k2eAgMnSc7d1	bývalý
spolužákem	spolužák	k1gMnSc7	spolužák
Wernerem	Werner	k1gMnSc7	Werner
Heisenbergem	Heisenberg	k1gInSc7	Heisenberg
nový	nový	k2eAgInSc1d1	nový
způsob	způsob	k1gInSc1	způsob
popisu	popis	k1gInSc2	popis
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kvantovou	kvantový	k2eAgFnSc4d1	kvantová
teorii	teorie	k1gFnSc4	teorie
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
spojuje	spojovat	k5eAaImIp3nS	spojovat
kvantovou	kvantový	k2eAgFnSc4d1	kvantová
mechaniku	mechanika	k1gFnSc4	mechanika
s	s	k7c7	s
Einsteinovou	Einsteinův	k2eAgFnSc7d1	Einsteinova
speciální	speciální	k2eAgFnSc7d1	speciální
teorií	teorie	k1gFnSc7	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
myšlenkou	myšlenka	k1gFnSc7	myšlenka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc1	všechen
částice	částice	k1gFnPc1	částice
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
kvanta	kvantum	k1gNnPc4	kvantum
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
pole	pole	k1gNnSc2	pole
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
základem	základ	k1gInSc7	základ
celé	celý	k2eAgFnSc2d1	celá
fyziky	fyzika	k1gFnSc2	fyzika
elementárních	elementární	k2eAgFnPc2d1	elementární
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Sodomka	Sodomka	k1gFnSc1	Sodomka
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
Sodomková	Sodomková	k1gFnSc1	Sodomková
<g/>
,	,	kIx,	,
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SET	set	k1gInSc1	set
OUT	OUT	kA	OUT
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-902058-5-2	[number]	k4	80-902058-5-2
Enz	Enz	k1gFnSc2	Enz
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
,	,	kIx,	,
and	and	k?	and
von	von	k1gInSc1	von
Meyenn	Meyenna	k1gFnPc2	Meyenna
<g/>
,	,	kIx,	,
Karl	Karla	k1gFnPc2	Karla
<g/>
,	,	kIx,	,
editors	editorsa	k1gFnPc2	editorsa
<g/>
,	,	kIx,	,
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Pauli	Paule	k1gFnSc4	Paule
-	-	kIx~	-
Writings	Writings	k1gInSc1	Writings
on	on	k3xPp3gInSc1	on
physics	physics	k6eAd1	physics
and	and	k?	and
philosophy	philosopha	k1gFnPc4	philosopha
<g/>
,	,	kIx,	,
translated	translated	k1gInSc4	translated
by	by	kYmCp3nS	by
Robert	Robert	k1gMnSc1	Robert
Schlapp	Schlapp	k1gMnSc1	Schlapp
(	(	kIx(	(
<g/>
Springer	Springer	k1gInSc1	Springer
Verlag	Verlaga	k1gFnPc2	Verlaga
<g/>
,	,	kIx,	,
Berlin	berlina	k1gFnPc2	berlina
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
540	[number]	k4	540
<g/>
-	-	kIx~	-
<g/>
56859	[number]	k4	56859
<g/>
-X	-X	k?	-X
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
540	[number]	k4	540
<g/>
-	-	kIx~	-
<g/>
56859	[number]	k4	56859
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Pauli	Paule	k1gFnSc4	Paule
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Pauli	Paule	k1gFnSc4	Paule
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
