<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
nejmenší	malý	k2eAgFnSc1d3	nejmenší
částice	částice	k1gFnSc1	částice
běžné	běžný	k2eAgFnSc2d1	běžná
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nelze	lze	k6eNd1	lze
chemickými	chemický	k2eAgInPc7d1	chemický
prostředky	prostředek	k1gInPc7	prostředek
dále	daleko	k6eAd2	daleko
dělit	dělit	k5eAaImF	dělit
<g/>
?	?	kIx.	?
</s>
