<s>
Atom	atom	k1gInSc1	atom
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
ἄ	ἄ	k?	ἄ
<g/>
,	,	kIx,	,
átomos	átomos	k1gInSc1	átomos
–	–	k?	–
nedělitelný	dělitelný	k2eNgInSc1d1	nedělitelný
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgFnSc1d3	nejmenší
částice	částice	k1gFnSc1	částice
běžné	běžný	k2eAgFnSc2d1	běžná
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
částice	částice	k1gFnSc2	částice
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
už	už	k6eAd1	už
chemickými	chemický	k2eAgInPc7d1	chemický
prostředky	prostředek	k1gInPc7	prostředek
dále	daleko	k6eAd2	daleko
nelze	lze	k6eNd1	lze
dělit	dělit	k5eAaImF	dělit
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
fyzikálními	fyzikální	k2eAgInPc7d1	fyzikální
ano	ano	k9	ano
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
jaderná	jaderný	k2eAgFnSc1d1	jaderná
reakce	reakce	k1gFnSc1	reakce
<g/>
)	)	kIx)	)
a	a	k8xC	a
která	který	k3yQgFnSc1	který
definuje	definovat	k5eAaBmIp3nS	definovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
daného	daný	k2eAgInSc2d1	daný
chemického	chemický	k2eAgInSc2d1	chemický
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Atom	atom	k1gInSc1	atom
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
obsahujícího	obsahující	k2eAgNnSc2d1	obsahující
protony	proton	k1gInPc1	proton
a	a	k8xC	a
neutrony	neutron	k1gInPc1	neutron
a	a	k8xC	a
obalu	obal	k1gInSc6	obal
obsahujícího	obsahující	k2eAgInSc2d1	obsahující
elektrony	elektron	k1gInPc1	elektron
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
představy	představa	k1gFnPc1	představa
o	o	k7c6	o
atomu	atom	k1gInSc6	atom
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
antického	antický	k2eAgNnSc2d1	antické
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
<s>
Démokritos	Démokritos	k1gMnSc1	Démokritos
představil	představit	k5eAaPmAgMnS	představit
filosofickou	filosofický	k2eAgFnSc4d1	filosofická
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
nelze	lze	k6eNd1	lze
hmotu	hmota	k1gFnSc4	hmota
dělit	dělit	k5eAaImF	dělit
donekonečna	donekonečna	k6eAd1	donekonečna
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c6	na
nejnižší	nízký	k2eAgFnSc6d3	nejnižší
úrovni	úroveň	k1gFnSc6	úroveň
existují	existovat	k5eAaImIp3nP	existovat
dále	daleko	k6eAd2	daleko
nedělitelné	dělitelný	k2eNgFnPc1d1	nedělitelná
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
označil	označit	k5eAaPmAgMnS	označit
slovem	slovem	k6eAd1	slovem
atomos	atomos	k1gMnSc1	atomos
(	(	kIx(	(
<g/>
ἄ	ἄ	k?	ἄ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
je	být	k5eAaImIp3nS	být
veškerá	veškerý	k3xTgFnSc1	veškerý
hmota	hmota	k1gFnSc1	hmota
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
různě	různě	k6eAd1	různě
uspořádaných	uspořádaný	k2eAgInPc2d1	uspořádaný
atomů	atom	k1gInPc2	atom
různého	různý	k2eAgInSc2d1	různý
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nedělitelné	dělitelný	k2eNgFnPc1d1	nedělitelná
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
nelze	lze	k6eNd1	lze
vytvářet	vytvářet	k5eAaImF	vytvářet
ani	ani	k8xC	ani
ničit	ničit	k5eAaImF	ničit
<g/>
.	.	kIx.	.
</s>
<s>
Vědeckou	vědecký	k2eAgFnSc4d1	vědecká
formu	forma	k1gFnSc4	forma
atomové	atomový	k2eAgFnSc3d1	atomová
teorii	teorie	k1gFnSc3	teorie
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
John	John	k1gMnSc1	John
Dalton	Dalton	k1gInSc1	Dalton
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc1	každý
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
stejných	stejný	k2eAgInPc2d1	stejný
atomů	atom	k1gInPc2	atom
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
nelze	lze	k6eNd1	lze
měnit	měnit	k5eAaImF	měnit
ani	ani	k8xC	ani
ničit	ničit	k5eAaImF	ničit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
skládat	skládat	k5eAaImF	skládat
do	do	k7c2	do
složitějších	složitý	k2eAgFnPc2d2	složitější
struktur	struktura	k1gFnPc2	struktura
(	(	kIx(	(
<g/>
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
některé	některý	k3yIgFnPc4	některý
otevřené	otevřený	k2eAgFnPc4d1	otevřená
otázky	otázka	k1gFnPc4	otázka
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
proč	proč	k6eAd1	proč
při	při	k7c6	při
chemických	chemický	k2eAgFnPc6d1	chemická
reakcích	reakce	k1gFnPc6	reakce
reagují	reagovat	k5eAaBmIp3nP	reagovat
vždy	vždy	k6eAd1	vždy
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
poměry	poměr	k1gInPc1	poměr
množství	množství	k1gNnSc2	množství
příslušných	příslušný	k2eAgFnPc2d1	příslušná
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
zákon	zákon	k1gInSc1	zákon
násobných	násobný	k2eAgInPc2d1	násobný
poměrů	poměr	k1gInPc2	poměr
slučovacích	slučovací	k2eAgInPc2d1	slučovací
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Thomsonův	Thomsonův	k2eAgInSc4d1	Thomsonův
model	model	k1gInSc4	model
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
nedělitelných	dělitelný	k2eNgInPc6d1	nedělitelný
atomech	atom	k1gInPc6	atom
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
atomy	atom	k1gInPc4	atom
nazval	nazvat	k5eAaPmAgInS	nazvat
Dalton	Dalton	k1gInSc1	Dalton
<g/>
)	)	kIx)	)
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
vyvrátil	vyvrátit	k5eAaPmAgMnS	vyvrátit
J.	J.	kA	J.
J.	J.	kA	J.
Thomson	Thomson	k1gMnSc1	Thomson
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
katodového	katodový	k2eAgNnSc2d1	katodové
záření	záření	k1gNnSc2	záření
objevil	objevit	k5eAaPmAgInS	objevit
elektron	elektron	k1gInSc1	elektron
–	–	k?	–
tedy	tedy	k8xC	tedy
první	první	k4xOgFnSc4	první
subatomární	subatomární	k2eAgFnSc4d1	subatomární
částici	částice	k1gFnSc4	částice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgInSc2	tento
objevu	objev	k1gInSc2	objev
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
tzv.	tzv.	kA	tzv.
Thomsonův	Thomsonův	k2eAgInSc1d1	Thomsonův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
(	(	kIx(	(
<g/>
též	též	k9	též
pudinkový	pudinkový	k2eAgInSc1d1	pudinkový
model	model	k1gInSc1	model
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
atom	atom	k1gInSc1	atom
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
rozloženou	rozložený	k2eAgFnSc4d1	rozložená
kladně	kladně	k6eAd1	kladně
nabitou	nabitý	k2eAgFnSc4d1	nabitá
hmotou	hmota	k1gFnSc7	hmota
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc2	který
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
rozinky	rozinka	k1gFnPc4	rozinka
v	v	k7c6	v
pudinku	pudink	k1gInSc6	pudink
<g/>
)	)	kIx)	)
rozptýleny	rozptýlit	k5eAaPmNgFnP	rozptýlit
záporně	záporně	k6eAd1	záporně
nabité	nabitý	k2eAgFnPc1d1	nabitá
elektrony	elektron	k1gInPc4	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rutherfordův	Rutherfordův	k2eAgInSc4d1	Rutherfordův
model	model	k1gInSc4	model
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Thomsonův	Thomsonův	k2eAgInSc1d1	Thomsonův
model	model	k1gInSc1	model
překonal	překonat	k5eAaPmAgInS	překonat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Ernest	Ernest	k1gMnSc1	Ernest
Rutherford	Rutherford	k1gMnSc1	Rutherford
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
analýzou	analýza	k1gFnSc7	analýza
experimentů	experiment	k1gInPc2	experiment
Geigera	Geigero	k1gNnSc2	Geigero
a	a	k8xC	a
Marsdena	Marsdeno	k1gNnSc2	Marsdeno
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
hmoty	hmota	k1gFnSc2	hmota
s	s	k7c7	s
kladným	kladný	k2eAgInSc7d1	kladný
nábojem	náboj	k1gInSc7	náboj
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
malém	malý	k2eAgInSc6d1	malý
prostoru	prostor	k1gInSc6	prostor
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ho	on	k3xPp3gMnSc4	on
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
Rutherfordově	Rutherfordův	k2eAgFnSc3d1	Rutherfordova
modelu	model	k1gInSc2	model
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
atom	atom	k1gInSc1	atom
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
kladně	kladně	k6eAd1	kladně
nabitého	nabitý	k2eAgNnSc2d1	nabité
hutného	hutný	k2eAgNnSc2d1	hutné
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
kterého	který	k3yIgNnSc2	který
obíhají	obíhat	k5eAaImIp3nP	obíhat
záporně	záporně	k6eAd1	záporně
nabité	nabitý	k2eAgInPc1d1	nabitý
elektrony	elektron	k1gInPc1	elektron
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xS	jako
planety	planeta	k1gFnPc1	planeta
obíhají	obíhat	k5eAaImIp3nP	obíhat
Slunce	slunce	k1gNnSc1	slunce
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
nazývá	nazývat	k5eAaImIp3nS	nazývat
též	též	k9	též
planetární	planetární	k2eAgInSc1d1	planetární
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
také	také	k9	také
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jádro	jádro	k1gNnSc1	jádro
atomu	atom	k1gInSc2	atom
vodíku	vodík	k1gInSc2	vodík
je	být	k5eAaImIp3nS	být
nejjednodušším	jednoduchý	k2eAgNnSc7d3	nejjednodušší
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
jedinou	jediný	k2eAgFnSc7d1	jediná
částicí	částice	k1gFnSc7	částice
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tato	tento	k3xDgFnSc1	tento
částice	částice	k1gFnSc1	částice
je	být	k5eAaImIp3nS	být
obsažena	obsáhnout	k5eAaPmNgFnS	obsáhnout
také	také	k9	také
v	v	k7c6	v
jádrech	jádro	k1gNnPc6	jádro
ostatních	ostatní	k2eAgMnPc2d1	ostatní
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
částice	částice	k1gFnSc1	částice
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
proton	proton	k1gInSc1	proton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
pak	pak	k6eAd1	pak
James	James	k1gMnSc1	James
Chadwick	Chadwick	k1gMnSc1	Chadwick
objevil	objevit	k5eAaPmAgMnS	objevit
neutron	neutron	k1gInSc4	neutron
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
nachází	nacházet	k5eAaImIp3nS	nacházet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
protony	proton	k1gInPc7	proton
<g/>
.	.	kIx.	.
</s>
<s>
Planetární	planetární	k2eAgInSc1d1	planetární
model	model	k1gInSc1	model
však	však	k9	však
trpěl	trpět	k5eAaImAgInS	trpět
mnoha	mnoho	k4c7	mnoho
zásadními	zásadní	k2eAgInPc7d1	zásadní
nedostatky	nedostatek	k1gInPc7	nedostatek
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
podle	podle	k7c2	podle
všech	všecek	k3xTgInPc2	všecek
známých	známý	k2eAgInPc2d1	známý
zákonů	zákon	k1gInPc2	zákon
by	by	kYmCp3nS	by
elektricky	elektricky	k6eAd1	elektricky
nabité	nabitý	k2eAgNnSc1d1	nabité
těleso	těleso	k1gNnSc1	těleso
(	(	kIx(	(
<g/>
elektron	elektron	k1gInSc1	elektron
<g/>
)	)	kIx)	)
obíhající	obíhající	k2eAgMnSc1d1	obíhající
po	po	k7c6	po
kruhové	kruhový	k2eAgFnSc6d1	kruhová
či	či	k8xC	či
eliptické	eliptický	k2eAgFnSc6d1	eliptická
dráze	dráha	k1gFnSc6	dráha
muselo	muset	k5eAaImAgNnS	muset
vysílat	vysílat	k5eAaImF	vysílat
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nS	by
ztrácelo	ztrácet	k5eAaImAgNnS	ztrácet
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
krátkém	krátký	k2eAgInSc6d1	krátký
čase	čas	k1gInSc6	čas
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
všechny	všechen	k3xTgInPc1	všechen
elektrony	elektron	k1gInPc1	elektron
spirálovitě	spirálovitě	k6eAd1	spirálovitě
zřítily	zřítit	k5eAaPmAgFnP	zřítit
do	do	k7c2	do
atomových	atomový	k2eAgNnPc2d1	atomové
jader	jádro	k1gNnPc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bohrův	Bohrův	k2eAgInSc4d1	Bohrův
model	model	k1gInSc4	model
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgInPc1d1	zásadní
problémy	problém	k1gInPc1	problém
Rutherfordova	Rutherfordův	k2eAgInSc2d1	Rutherfordův
modelu	model	k1gInSc2	model
překonala	překonat	k5eAaPmAgFnS	překonat
až	až	k9	až
nová	nový	k2eAgFnSc1d1	nová
kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
je	být	k5eAaImIp3nS	být
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
vysíláno	vysílat	k5eAaImNgNnS	vysílat
i	i	k8xC	i
pohlcováno	pohlcovat	k5eAaImNgNnS	pohlcovat
po	po	k7c6	po
nedělitelných	dělitelný	k2eNgInPc6d1	nedělitelný
množstvích	množství	k1gNnPc6	množství
<g/>
,	,	kIx,	,
kvantech	kvantum	k1gNnPc6	kvantum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Niels	Niels	k1gInSc4	Niels
Bohr	Bohra	k1gFnPc2	Bohra
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
Bohrův	Bohrův	k2eAgInSc4d1	Bohrův
model	model	k1gInSc4	model
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
modelu	model	k1gInSc2	model
obíhají	obíhat	k5eAaImIp3nP	obíhat
elektrony	elektron	k1gInPc1	elektron
atomové	atomový	k2eAgInPc1d1	atomový
jádro	jádro	k1gNnSc4	jádro
jen	jen	k9	jen
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
dovolených	dovolený	k2eAgFnPc6d1	dovolená
kruhových	kruhový	k2eAgFnPc6d1	kruhová
drahách	draha	k1gFnPc6	draha
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nemohou	moct	k5eNaImIp3nP	moct
vyzařovat	vyzařovat	k5eAaImF	vyzařovat
a	a	k8xC	a
spirálovitě	spirálovitě	k6eAd1	spirálovitě
padat	padat	k5eAaImF	padat
do	do	k7c2	do
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mezilehlé	mezilehlý	k2eAgFnPc1d1	mezilehlá
dráhy	dráha	k1gFnPc1	dráha
nejsou	být	k5eNaImIp3nP	být
možné	možný	k2eAgFnPc1d1	možná
a	a	k8xC	a
vyzařování	vyzařování	k1gNnSc1	vyzařování
energie	energie	k1gFnSc2	energie
není	být	k5eNaImIp3nS	být
spojité	spojitý	k2eAgNnSc1d1	spojité
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
podmínek	podmínka	k1gFnPc2	podmínka
jednorázovými	jednorázový	k2eAgFnPc7d1	jednorázová
změnami	změna	k1gFnPc7	změna
"	"	kIx"	"
<g/>
přeskočit	přeskočit	k5eAaPmF	přeskočit
<g/>
"	"	kIx"	"
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
energetické	energetický	k2eAgFnSc2d1	energetická
hladiny	hladina	k1gFnSc2	hladina
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Bohrův	Bohrův	k2eAgInSc1d1	Bohrův
model	model	k1gInSc1	model
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
ad	ad	k7c4	ad
hoc	hoc	k?	hoc
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
poměrně	poměrně	k6eAd1	poměrně
komplikovaný	komplikovaný	k2eAgInSc1d1	komplikovaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokázal	dokázat	k5eAaPmAgMnS	dokázat
předpovědět	předpovědět	k5eAaPmF	předpovědět
několik	několik	k4yIc4	několik
důležitých	důležitý	k2eAgInPc2d1	důležitý
faktů	fakt	k1gInPc2	fakt
o	o	k7c6	o
atomových	atomový	k2eAgNnPc6d1	atomové
spektrech	spektrum	k1gNnPc6	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
ohledech	ohled	k1gInPc6	ohled
však	však	k9	však
stále	stále	k6eAd1	stále
selhával	selhávat	k5eAaImAgMnS	selhávat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
štěpení	štěpení	k1gNnSc2	štěpení
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
čar	čára	k1gFnPc2	čára
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bohrův	Bohrův	k2eAgInSc1d1	Bohrův
model	model	k1gInSc1	model
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
klasické	klasický	k2eAgFnSc6d1	klasická
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
planetárního	planetární	k2eAgInSc2d1	planetární
modelu	model	k1gInSc2	model
však	však	k9	však
doplnil	doplnit	k5eAaPmAgMnS	doplnit
některé	některý	k3yIgInPc4	některý
postuláty	postulát	k1gInPc4	postulát
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
odstranit	odstranit	k5eAaPmF	odstranit
rozpory	rozpor	k1gInPc4	rozpor
planetárního	planetární	k2eAgInSc2d1	planetární
modelu	model	k1gInSc2	model
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
soubor	soubor	k1gInSc4	soubor
uměle	uměle	k6eAd1	uměle
definovaných	definovaný	k2eAgNnPc2d1	definované
fenomenologických	fenomenologický	k2eAgNnPc2d1	fenomenologické
tvrzení	tvrzení	k1gNnPc2	tvrzení
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgInSc6	který
byl	být	k5eAaImAgInS	být
samotný	samotný	k2eAgInSc1d1	samotný
Bohr	Bohr	k1gInSc1	Bohr
přesvědčen	přesvědčit	k5eAaPmNgInS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
konečným	konečný	k2eAgNnSc7d1	konečné
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Bohrovy	Bohrův	k2eAgInPc1d1	Bohrův
postuláty	postulát	k1gInPc1	postulát
však	však	k9	však
byly	být	k5eAaImAgInP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
podstatných	podstatný	k2eAgInPc2d1	podstatný
podnětů	podnět	k1gInPc2	podnět
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
iniciovaly	iniciovat	k5eAaBmAgFnP	iniciovat
vznik	vznik	k1gInSc4	vznik
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
–	–	k?	–
nového	nový	k2eAgInSc2d1	nový
teoretického	teoretický	k2eAgInSc2d1	teoretický
základu	základ	k1gInSc2	základ
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
postuláty	postulát	k1gInPc1	postulát
přirozeně	přirozeně	k6eAd1	přirozeně
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
<g/>
.	.	kIx.	.
</s>
<s>
Arnold	Arnold	k1gMnSc1	Arnold
Sommerfeld	Sommerfeld	k1gMnSc1	Sommerfeld
doplnil	doplnit	k5eAaPmAgMnS	doplnit
Bohrův	Bohrův	k2eAgInSc4d1	Bohrův
model	model	k1gInSc4	model
o	o	k7c6	o
vybrané	vybraná	k1gFnSc6	vybraná
eliptické	eliptický	k2eAgFnSc2d1	eliptická
dráhy	dráha	k1gFnSc2	dráha
oběhu	oběh	k1gInSc2	oběh
elektronů	elektron	k1gInPc2	elektron
a	a	k8xC	a
přidal	přidat	k5eAaPmAgMnS	přidat
příslušné	příslušný	k2eAgInPc4d1	příslušný
postuláty	postulát	k1gInPc4	postulát
<g/>
.	.	kIx.	.
</s>
<s>
Umožnil	umožnit	k5eAaPmAgMnS	umožnit
tak	tak	k9	tak
dílčí	dílčí	k2eAgNnSc4d1	dílčí
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
pro	pro	k7c4	pro
rozštěpení	rozštěpení	k1gNnSc4	rozštěpení
některých	některý	k3yIgFnPc2	některý
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
linií	linie	k1gFnPc2	linie
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
změny	změna	k1gFnPc4	změna
spekter	spektrum	k1gNnPc2	spektrum
v	v	k7c6	v
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
kvantověmechanický	kvantověmechanický	k2eAgInSc1d1	kvantověmechanický
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
de	de	k?	de
Broglieho	Broglie	k1gMnSc4	Broglie
teorie	teorie	k1gFnSc2	teorie
částicových	částicový	k2eAgFnPc2d1	částicová
vln	vlna	k1gFnPc2	vlna
a	a	k8xC	a
následné	následný	k2eAgFnSc2d1	následná
Schrödingerovy	Schrödingerův	k2eAgFnSc2d1	Schrödingerova
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
představil	představit	k5eAaPmAgMnS	představit
tzv.	tzv.	kA	tzv.
Schrödingerovu	Schrödingerův	k2eAgFnSc4d1	Schrödingerova
rovnici	rovnice	k1gFnSc4	rovnice
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
elektron	elektron	k1gInSc1	elektron
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
částice	částice	k1gFnPc1	částice
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
popisován	popisován	k2eAgInSc4d1	popisován
jako	jako	k8xC	jako
hmotný	hmotný	k2eAgInSc4d1	hmotný
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
vlnová	vlnový	k2eAgFnSc1d1	vlnová
funkce	funkce	k1gFnSc1	funkce
definující	definující	k2eAgFnSc2d1	definující
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
výskytu	výskyt	k1gInSc2	výskyt
elektronu	elektron	k1gInSc2	elektron
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Heisenbergovými	Heisenbergův	k2eAgFnPc7d1	Heisenbergova
relacemi	relace	k1gFnPc7	relace
neurčitosti	neurčitost	k1gFnSc2	neurčitost
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mechanistické	mechanistický	k2eAgFnPc1d1	mechanistická
eliptické	eliptický	k2eAgFnPc1d1	eliptická
dráhy	dráha	k1gFnPc1	dráha
Bohrova-Sommerfeldova	Bohrova-Sommerfeldův	k2eAgInSc2d1	Bohrova-Sommerfeldův
modelu	model	k1gInSc2	model
byly	být	k5eAaImAgInP	být
opuštěny	opustit	k5eAaPmNgInP	opustit
a	a	k8xC	a
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
neostře	ostro	k6eNd1	ostro
definovanými	definovaný	k2eAgFnPc7d1	definovaná
oblastmi	oblast	k1gFnPc7	oblast
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgNnPc6	který
se	se	k3xPyFc4	se
elektron	elektron	k1gInSc1	elektron
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
nalézá	nalézat	k5eAaImIp3nS	nalézat
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
orbitaly	orbital	k1gInPc7	orbital
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
Schrödingerově	Schrödingerův	k2eAgFnSc6d1	Schrödingerova
kvantové	kvantový	k2eAgFnSc6d1	kvantová
mechanice	mechanika	k1gFnSc6	mechanika
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
mnoho	mnoho	k4c4	mnoho
atomových	atomový	k2eAgFnPc2d1	atomová
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
dřívějšími	dřívější	k2eAgFnPc7d1	dřívější
teoriemi	teorie	k1gFnPc7	teorie
nepředpověditelné	předpověditelný	k2eNgFnSc2d1	nepředpověditelná
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
přechodů	přechod	k1gInPc2	přechod
a	a	k8xC	a
tedy	tedy	k8xC	tedy
intenzity	intenzita	k1gFnSc2	intenzita
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
čar	čára	k1gFnPc2	čára
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
jevy	jev	k1gInPc1	jev
v	v	k7c6	v
jemné	jemný	k2eAgFnSc6d1	jemná
struktuře	struktura	k1gFnSc6	struktura
spekter	spektrum	k1gNnPc2	spektrum
se	se	k3xPyFc4	se
však	však	k9	však
pomocí	pomocí	k7c2	pomocí
něho	on	k3xPp3gMnSc2	on
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přesnějšímu	přesný	k2eAgNnSc3d2	přesnější
vystižení	vystižení	k1gNnSc3	vystižení
vlastností	vlastnost	k1gFnSc7	vlastnost
atomového	atomový	k2eAgInSc2d1	atomový
obalu	obal	k1gInSc2	obal
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
relativistická	relativistický	k2eAgFnSc1d1	relativistická
kvantová	kvantový	k2eAgFnSc1d1	kvantová
mechanika	mechanika	k1gFnSc1	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Chování	chování	k1gNnSc1	chování
elektronů	elektron	k1gInPc2	elektron
tak	tak	k9	tak
lépe	dobře	k6eAd2	dobře
popisuje	popisovat	k5eAaImIp3nS	popisovat
relativistická	relativistický	k2eAgFnSc1d1	relativistická
Diracova	Diracův	k2eAgFnSc1d1	Diracova
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
přirozeně	přirozeně	k6eAd1	přirozeně
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
i	i	k9	i
korekce	korekce	k1gFnPc1	korekce
k	k	k7c3	k
Schrödingerovu	Schrödingerův	k2eAgInSc3d1	Schrödingerův
modelu	model	k1gInSc3	model
<g/>
,	,	kIx,	,
mající	mající	k2eAgInSc1d1	mající
původ	původ	k1gInSc1	původ
v	v	k7c6	v
relativistické	relativistický	k2eAgFnSc6d1	relativistická
změně	změna	k1gFnSc6	změna
hmotnosti	hmotnost	k1gFnSc2	hmotnost
a	a	k8xC	a
v	v	k7c6	v
interakci	interakce	k1gFnSc6	interakce
spinů	spin	k1gInPc2	spin
elektronů	elektron	k1gInPc2	elektron
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
jejich	jejich	k3xOp3gFnPc2	jejich
magnetických	magnetický	k2eAgFnPc2d1	magnetická
polí	pole	k1gFnPc2	pole
<g/>
)	)	kIx)	)
ve	v	k7c6	v
víceelektronových	víceelektronův	k2eAgInPc6d1	víceelektronův
atomových	atomový	k2eAgInPc6d1	atomový
obalech	obal	k1gInPc6	obal
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
Kleinova-Gordonova	Kleinova-Gordonův	k2eAgFnSc1d1	Kleinova-Gordonova
rovnice	rovnice	k1gFnSc1	rovnice
popisuje	popisovat	k5eAaImIp3nS	popisovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
exotických	exotický	k2eAgInPc2d1	exotický
mezoatomů	mezoatom	k1gInPc2	mezoatom
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
atomový	atomový	k2eAgInSc4d1	atomový
obal	obal	k1gInSc4	obal
je	být	k5eAaImIp3nS	být
tvořený	tvořený	k2eAgInSc4d1	tvořený
mezony	mezon	k1gInPc4	mezon
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
záporné	záporný	k2eAgInPc4d1	záporný
piony	pion	k1gInPc4	pion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
atomové	atomový	k2eAgFnSc2d1	atomová
jádro	jádro	k1gNnSc4	jádro
a	a	k8xC	a
elektronový	elektronový	k2eAgInSc4d1	elektronový
obal	obal	k1gInSc4	obal
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
podle	podle	k7c2	podle
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
atom	atom	k1gInSc1	atom
"	"	kIx"	"
<g/>
nedělitelný	dělitelný	k2eNgMnSc1d1	nedělitelný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jej	on	k3xPp3gNnSc2	on
lze	lze	k6eAd1	lze
rozložit	rozložit	k5eAaPmF	rozložit
na	na	k7c4	na
menší	malý	k2eAgFnPc4d2	menší
složky	složka	k1gFnPc4	složka
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
subatomární	subatomární	k2eAgFnPc1d1	subatomární
částice	částice	k1gFnPc1	částice
<g/>
:	:	kIx,	:
elektrony	elektron	k1gInPc1	elektron
jsou	být	k5eAaImIp3nP	být
záporně	záporně	k6eAd1	záporně
nabité	nabitý	k2eAgFnPc1d1	nabitá
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
atomovém	atomový	k2eAgInSc6d1	atomový
obalu	obal	k1gInSc6	obal
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
relativně	relativně	k6eAd1	relativně
snadno	snadno	k6eAd1	snadno
<g />
.	.	kIx.	.
</s>
<s>
vyjmout	vyjmout	k5eAaPmF	vyjmout
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
tím	ten	k3xDgNnSc7	ten
nabitý	nabitý	k2eAgInSc1d1	nabitý
iont	iont	k1gInSc4	iont
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
ionizace	ionizace	k1gFnSc2	ionizace
<g/>
)	)	kIx)	)
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
atomu	atom	k1gInSc2	atom
je	být	k5eAaImIp3nS	být
atomové	atomový	k2eAgNnSc1d1	atomové
jádro	jádro	k1gNnSc1	jádro
obsahující	obsahující	k2eAgFnSc2d1	obsahující
tzv.	tzv.	kA	tzv.
nukleony	nukleon	k1gInPc1	nukleon
(	(	kIx(	(
<g/>
jádro	jádro	k1gNnSc1	jádro
zabírá	zabírat	k5eAaImIp3nS	zabírat
jen	jen	k9	jen
nepatrnou	patrný	k2eNgFnSc4d1	patrný
část	část	k1gFnSc4	část
atomu	atom	k1gInSc2	atom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tvoří	tvořit	k5eAaImIp3nP	tvořit
naprostou	naprostý	k2eAgFnSc4d1	naprostá
většinu	většina	k1gFnSc4	většina
jeho	jeho	k3xOp3gFnSc2	jeho
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
protony	proton	k1gInPc1	proton
jsou	být	k5eAaImIp3nP	být
kladně	kladně	k6eAd1	kladně
nabité	nabitý	k2eAgFnPc1d1	nabitá
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
1836	[number]	k4	1836
<g/>
krát	krát	k6eAd1	krát
hmotnější	hmotný	k2eAgInPc1d2	hmotnější
než	než	k8xS	než
elektrony	elektron	k1gInPc1	elektron
<g/>
,	,	kIx,	,
neutrony	neutron	k1gInPc1	neutron
jsou	být	k5eAaImIp3nP	být
elektricky	elektricky	k6eAd1	elektricky
neutrální	neutrální	k2eAgFnPc1d1	neutrální
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
jen	jen	k9	jen
o	o	k7c4	o
trochu	trochu	k6eAd1	trochu
hmotnější	hmotný	k2eAgInPc4d2	hmotnější
než	než	k8xS	než
protony	proton	k1gInPc1	proton
<g/>
.	.	kIx.	.
</s>
<s>
Elektrony	elektron	k1gInPc1	elektron
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
atomovému	atomový	k2eAgNnSc3d1	atomové
jádru	jádro	k1gNnSc3	jádro
vázány	vázat	k5eAaImNgFnP	vázat
elektromagnetickou	elektromagnetický	k2eAgFnSc7d1	elektromagnetická
silou	síla	k1gFnSc7	síla
zprostředkovávanou	zprostředkovávaný	k2eAgFnSc7d1	zprostředkovávaná
fotony	foton	k1gInPc4	foton
<g/>
.	.	kIx.	.
</s>
<s>
Protony	proton	k1gInPc1	proton
a	a	k8xC	a
neutrony	neutron	k1gInPc1	neutron
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
vázány	vázat	k5eAaImNgFnP	vázat
silnou	silný	k2eAgFnSc7d1	silná
jadernou	jaderný	k2eAgFnSc7d1	jaderná
silou	síla	k1gFnSc7	síla
zprostředkovanou	zprostředkovaný	k2eAgFnSc4d1	zprostředkovaná
gluony	gluon	k1gInPc1	gluon
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgInPc1d1	samotný
protony	proton	k1gInPc1	proton
a	a	k8xC	a
neutrony	neutron	k1gInPc1	neutron
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
kvarků	kvark	k1gInPc2	kvark
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
atomy	atom	k1gInPc1	atom
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
svým	svůj	k3xOyFgNnSc7	svůj
složením	složení	k1gNnSc7	složení
<g/>
:	:	kIx,	:
počtem	počet	k1gInSc7	počet
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Atomy	atom	k1gInPc1	atom
různých	různý	k2eAgInPc2d1	různý
prvků	prvek	k1gInPc2	prvek
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
svým	svůj	k3xOyFgNnSc7	svůj
atomovým	atomový	k2eAgNnSc7d1	atomové
číslem	číslo	k1gNnSc7	číslo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
počet	počet	k1gInSc1	počet
protonů	proton	k1gInPc2	proton
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
(	(	kIx(	(
<g/>
např.	např.	kA	např.
atom	atom	k1gInSc1	atom
se	s	k7c7	s
šesti	šest	k4xCc7	šest
protony	proton	k1gInPc7	proton
je	být	k5eAaImIp3nS	být
atomem	atom	k1gInSc7	atom
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
;	;	kIx,	;
počet	počet	k1gInSc1	počet
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
obalu	obal	k1gInSc6	obal
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
běžného	běžný	k2eAgMnSc2d1	běžný
elektricky	elektricky	k6eAd1	elektricky
neutrálního	neutrální	k2eAgInSc2d1	neutrální
atomu	atom	k1gInSc2	atom
shodný	shodný	k2eAgMnSc1d1	shodný
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
protonů	proton	k1gInPc2	proton
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Atomy	atom	k1gInPc1	atom
jednoho	jeden	k4xCgInSc2	jeden
prvku	prvek	k1gInSc2	prvek
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
počtem	počet	k1gInSc7	počet
neutronů	neutron	k1gInPc2	neutron
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
nukleonovým	nukleonův	k2eAgNnSc7d1	nukleonův
číslem	číslo	k1gNnSc7	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
tvoří	tvořit	k5eAaImIp3nS	tvořit
různé	různý	k2eAgInPc1d1	různý
izotopy	izotop	k1gInPc1	izotop
(	(	kIx(	(
<g/>
např.	např.	kA	např.
atom	atom	k1gInSc1	atom
s	s	k7c7	s
šesti	šest	k4xCc7	šest
protony	proton	k1gInPc7	proton
a	a	k8xC	a
osmi	osm	k4xCc7	osm
neutrony	neutron	k1gInPc7	neutron
je	být	k5eAaImIp3nS	být
izotop	izotop	k1gInSc4	izotop
uhlík	uhlík	k1gInSc4	uhlík
14	[number]	k4	14
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
z	z	k7c2	z
elektricky	elektricky	k6eAd1	elektricky
neutrálního	neutrální	k2eAgInSc2d1	neutrální
atomu	atom	k1gInSc2	atom
vyjme	vyjmout	k5eAaPmIp3nS	vyjmout
elektron	elektron	k1gInSc1	elektron
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
vloží	vložit	k5eAaPmIp3nP	vložit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
nabitý	nabitý	k2eAgInSc1d1	nabitý
iont	iont	k1gInSc1	iont
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
asi	asi	k9	asi
256	[number]	k4	256
druhů	druh	k1gInPc2	druh
atomů	atom	k1gInPc2	atom
(	(	kIx(	(
<g/>
nuklidů	nuklid	k1gInPc2	nuklid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
stabilní	stabilní	k2eAgFnPc1d1	stabilní
<g/>
,	,	kIx,	,
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k1gNnPc2	další
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
jádra	jádro	k1gNnPc1	jádro
jsou	být	k5eAaImIp3nP	být
nestabilní	stabilní	k2eNgNnPc1d1	nestabilní
a	a	k8xC	a
samovolně	samovolně	k6eAd1	samovolně
se	se	k3xPyFc4	se
radioaktivně	radioaktivně	k6eAd1	radioaktivně
rozpadají	rozpadat	k5eAaImIp3nP	rozpadat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Exotické	exotický	k2eAgInPc4d1	exotický
atomy	atom	k1gInPc4	atom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
atomovém	atomový	k2eAgNnSc6d1	atomové
jádře	jádro	k1gNnSc6	jádro
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nukleon	nukleon	k1gInSc1	nukleon
nahrazen	nahradit	k5eAaPmNgInS	nahradit
hyperonem	hyperon	k1gMnSc7	hyperon
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
hyperonem	hyperon	k1gInSc7	hyperon
Λ	Λ	k?	Λ
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
o	o	k7c4	o
atom	atom	k1gInSc4	atom
s	s	k7c7	s
hyperjádrem	hyperjádr	k1gInSc7	hyperjádr
<g/>
.	.	kIx.	.
</s>
<s>
Elektron	elektron	k1gInSc1	elektron
v	v	k7c6	v
atomovém	atomový	k2eAgInSc6d1	atomový
obalu	obal	k1gInSc6	obal
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
mionem	mion	k1gInSc7	mion
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
lehčím	lehčit	k5eAaImIp1nS	lehčit
záporně	záporně	k6eAd1	záporně
nabitým	nabitý	k2eAgInSc7d1	nabitý
mezonem	mezon	k1gInSc7	mezon
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pionem	pion	k1gInSc7	pion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
atom	atom	k1gInSc1	atom
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nazývá	nazývat	k5eAaImIp3nS	nazývat
mioatomem	mioatom	k1gMnSc7	mioatom
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
mezoatomem	mezoatom	k1gMnSc7	mezoatom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
větší	veliký	k2eAgFnSc3d2	veliký
hmotnosti	hmotnost	k1gFnSc3	hmotnost
je	být	k5eAaImIp3nS	být
klasický	klasický	k2eAgInSc1d1	klasický
Bohrův	Bohrův	k2eAgInSc1d1	Bohrův
poloměr	poloměr	k1gInSc1	poloměr
mioatomu	mioatom	k1gInSc2	mioatom
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
mion	mion	k1gInSc1	mion
je	být	k5eAaImIp3nS	být
vázán	vázat	k5eAaImNgInS	vázat
těsněji	těsně	k6eAd2	těsně
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
záchytu	záchyt	k1gInSc2	záchyt
mionu	mionout	k5eAaImIp1nS	mionout
jádrem	jádro	k1gNnSc7	jádro
(	(	kIx(	(
<g/>
obdoba	obdoba	k1gFnSc1	obdoba
záchytu	záchyt	k1gInSc2	záchyt
elektronu	elektron	k1gInSc2	elektron
u	u	k7c2	u
radioaktivní	radioaktivní	k2eAgFnSc2d1	radioaktivní
přeměny	přeměna	k1gFnSc2	přeměna
beta	beta	k1gNnSc1	beta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Atomové	atomový	k2eAgInPc1d1	atomový
orbitaly	orbital	k1gInPc1	orbital
mezoatomů	mezoatom	k1gInPc2	mezoatom
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
nejen	nejen	k6eAd1	nejen
kvůli	kvůli	k7c3	kvůli
odlišné	odlišný	k2eAgFnSc3d1	odlišná
hmotnosti	hmotnost	k1gFnSc3	hmotnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
kvantověmechanického	kvantověmechanický	k2eAgNnSc2d1	kvantověmechanický
chování	chování	k1gNnSc2	chování
mezonu	mezon	k1gInSc2	mezon
v	v	k7c6	v
obalu	obal	k1gInSc6	obal
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
použít	použít	k5eAaPmF	použít
Kleinovu-Gordonovu	Kleinovu-Gordonův	k2eAgFnSc4d1	Kleinovu-Gordonův
rovnici	rovnice	k1gFnSc4	rovnice
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Diracovy	Diracův	k2eAgFnSc2d1	Diracova
rovnice	rovnice	k1gFnSc2	rovnice
pro	pro	k7c4	pro
elektron	elektron	k1gInSc4	elektron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jádro	jádro	k1gNnSc1	jádro
(	(	kIx(	(
<g/>
proton	proton	k1gInSc1	proton
u	u	k7c2	u
atomu	atom	k1gInSc2	atom
lehkého	lehký	k2eAgInSc2d1	lehký
vodíku	vodík	k1gInSc2	vodík
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k6eAd1	také
nahrazeno	nahrazen	k2eAgNnSc4d1	nahrazeno
antimionem	antimion	k1gInSc7	antimion
nebo	nebo	k8xC	nebo
kladně	kladně	k6eAd1	kladně
nabitým	nabitý	k2eAgInSc7d1	nabitý
mezonem	mezon	k1gInSc7	mezon
-	-	kIx~	-
v	v	k7c6	v
r.	r.	kA	r.
2016	[number]	k4	2016
tak	tak	k8xC	tak
např.	např.	kA	např.
byla	být	k5eAaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
existence	existence	k1gFnSc1	existence
exotických	exotický	k2eAgInPc2d1	exotický
atomů	atom	k1gInPc2	atom
složených	složený	k2eAgInPc2d1	složený
z	z	k7c2	z
pionu	pion	k1gInSc2	pion
a	a	k8xC	a
kaonu	kaonout	k5eAaImIp1nS	kaonout
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
K	k	k7c3	k
<g/>
+	+	kIx~	+
<g/>
π	π	k?	π
<g/>
−	−	k?	−
tak	tak	k8xC	tak
π	π	k?	π
<g/>
+	+	kIx~	+
<g/>
K	k	k7c3	k
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
za	za	k7c4	za
exotický	exotický	k2eAgInSc4d1	exotický
atom	atom	k1gInSc4	atom
považováno	považován	k2eAgNnSc1d1	považováno
i	i	k8xC	i
tzv.	tzv.	kA	tzv.
pozitronium	pozitronium	k1gNnSc1	pozitronium
<g/>
,	,	kIx,	,
vázaná	vázaný	k2eAgFnSc1d1	vázaná
soustava	soustava	k1gFnSc1	soustava
pozitronu	pozitron	k1gInSc2	pozitron
a	a	k8xC	a
elektronu	elektron	k1gInSc2	elektron
<g/>
,	,	kIx,	,
a	a	k8xC	a
mionium	mionium	k1gNnSc1	mionium
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
nesystematicky	systematicky	k6eNd1	systematicky
používaný	používaný	k2eAgInSc1d1	používaný
jak	jak	k8xC	jak
pro	pro	k7c4	pro
soustavu	soustava	k1gFnSc4	soustava
antimionu	antimion	k1gInSc2	antimion
a	a	k8xC	a
elektronu	elektron	k1gInSc2	elektron
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
antimionu	antimion	k1gInSc2	antimion
a	a	k8xC	a
mionu	mion	k1gInSc2	mion
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
kladně	kladně	k6eAd1	kladně
nabitý	nabitý	k2eAgInSc1d1	nabitý
antilepton	antilepton	k1gInSc1	antilepton
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
klasické	klasický	k2eAgNnSc4d1	klasické
atomové	atomový	k2eAgNnSc4d1	atomové
jádro	jádro	k1gNnSc4	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
existovat	existovat	k5eAaImF	existovat
i	i	k9	i
vázané	vázaný	k2eAgFnPc1d1	vázaná
soustavy	soustava	k1gFnPc1	soustava
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
antiprotonu	antiproton	k1gInSc2	antiproton
<g/>
,	,	kIx,	,
nahrazujícího	nahrazující	k2eAgInSc2d1	nahrazující
elektron	elektron	k1gInSc1	elektron
v	v	k7c6	v
obalu	obal	k1gInSc6	obal
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
soustavy	soustava	k1gFnPc1	soustava
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
baryonové	baryonový	k2eAgInPc1d1	baryonový
atomy	atom	k1gInPc1	atom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1991	[number]	k4	1991
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
antiprotonové	antiprotonový	k2eAgNnSc1d1	antiprotonový
hélium	hélium	k1gNnSc1	hélium
(	(	kIx(	(
<g/>
atomové	atomový	k2eAgNnSc1d1	atomové
jádro	jádro	k1gNnSc1	jádro
hélia	hélium	k1gNnSc2	hélium
s	s	k7c7	s
"	"	kIx"	"
<g/>
obalem	obal	k1gInSc7	obal
<g/>
"	"	kIx"	"
tvořeným	tvořený	k2eAgInSc7d1	tvořený
antiprotonem	antiproton	k1gInSc7	antiproton
a	a	k8xC	a
elektronem	elektron	k1gInSc7	elektron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
r.	r.	kA	r.
2006	[number]	k4	2006
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
prokázána	prokázán	k2eAgFnSc1d1	prokázána
produkce	produkce	k1gFnSc1	produkce
protonia	protonium	k1gNnSc2	protonium
čili	čili	k8xC	čili
antiprotonového	antiprotonový	k2eAgInSc2d1	antiprotonový
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
vázané	vázaný	k2eAgFnPc1d1	vázaná
soustavy	soustava	k1gFnPc1	soustava
protonu	proton	k1gInSc2	proton
a	a	k8xC	a
antiprotonu	antiproton	k1gInSc2	antiproton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
antiprotonového	antiprotonový	k2eAgNnSc2d1	antiprotonový
hélia	hélium	k1gNnSc2	hélium
<g/>
,	,	kIx,	,
jakéhosi	jakýsi	k3yIgInSc2	jakýsi
hybridu	hybrid	k1gInSc2	hybrid
mezi	mezi	k7c7	mezi
atomem	atom	k1gInSc7	atom
a	a	k8xC	a
molekulou	molekula	k1gFnSc7	molekula
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
též	též	k9	též
název	název	k1gInSc1	název
atomkule	atomkule	k1gFnSc2	atomkule
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
exotické	exotický	k2eAgInPc1d1	exotický
atomy	atom	k1gInPc1	atom
jsou	být	k5eAaImIp3nP	být
nestabilní	stabilní	k2eNgInPc1d1	nestabilní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plynech	plyn	k1gInPc6	plyn
<g/>
,	,	kIx,	,
kapalinách	kapalina	k1gFnPc6	kapalina
a	a	k8xC	a
některých	některý	k3yIgFnPc6	některý
pevných	pevný	k2eAgFnPc6d1	pevná
látkách	látka	k1gFnPc6	látka
jsou	být	k5eAaImIp3nP	být
atomy	atom	k1gInPc1	atom
chemickými	chemický	k2eAgNnPc7d1	chemické
vazbami	vazba	k1gFnPc7	vazba
vázány	vázat	k5eAaImNgFnP	vázat
do	do	k7c2	do
molekul	molekula	k1gFnPc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
pevných	pevný	k2eAgFnPc6d1	pevná
látkách	látka	k1gFnPc6	látka
jsou	být	k5eAaImIp3nP	být
atomy	atom	k1gInPc1	atom
vázány	vázat	k5eAaImNgInP	vázat
přímo	přímo	k6eAd1	přímo
bez	bez	k7c2	bez
tvorby	tvorba	k1gFnSc2	tvorba
molekul	molekula	k1gFnPc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikají	vznikat	k5eAaImIp3nP	vznikat
krystalické	krystalický	k2eAgFnPc4d1	krystalická
látky	látka	k1gFnPc4	látka
<g/>
;	;	kIx,	;
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
případem	případ	k1gInSc7	případ
jsou	být	k5eAaImIp3nP	být
molekulární	molekulární	k2eAgInPc1d1	molekulární
krystaly	krystal	k1gInPc1	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
atom	atom	k1gInSc1	atom
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
stabilitu	stabilita	k1gFnSc4	stabilita
důležitá	důležitý	k2eAgFnSc1d1	důležitá
vyváženost	vyváženost	k1gFnSc1	vyváženost
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
působí	působit	k5eAaImIp3nS	působit
<g/>
:	:	kIx,	:
Gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
působící	působící	k2eAgFnSc3d1	působící
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgFnPc7	všecek
formami	forma	k1gFnPc7	forma
hmoty	hmota	k1gFnSc2	hmota
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
atomech	atom	k1gInPc6	atom
zanedbatelná	zanedbatelný	k2eAgFnSc1d1	zanedbatelná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
subatomární	subatomární	k2eAgFnPc1d1	subatomární
částice	částice	k1gFnPc1	částice
mají	mít	k5eAaImIp3nP	mít
velkou	velký	k2eAgFnSc4d1	velká
hybnost	hybnost	k1gFnSc4	hybnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
gravitace	gravitace	k1gFnSc1	gravitace
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
interakcí	interakce	k1gFnPc2	interakce
nejslabší	slabý	k2eAgFnSc1d3	nejslabší
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
mezi	mezi	k7c7	mezi
protonem	proton	k1gInSc7	proton
a	a	k8xC	a
elektronem	elektron	k1gInSc7	elektron
vzdáleným	vzdálený	k2eAgInSc7d1	vzdálený
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
10	[number]	k4	10
<g/>
m	m	kA	m
(	(	kIx(	(
<g/>
poloměr	poloměr	k1gInSc1	poloměr
atomu	atom	k1gInSc2	atom
<g/>
)	)	kIx)	)
působí	působit	k5eAaImIp3nS	působit
gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
47	[number]	k4	47
<g/>
N	N	kA	N
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
síla	síla	k1gFnSc1	síla
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
protony	proton	k1gInPc7	proton
(	(	kIx(	(
<g/>
či	či	k8xC	či
neutrony	neutron	k1gInPc1	neutron
<g/>
)	)	kIx)	)
vzdálenými	vzdálený	k2eAgFnPc7d1	vzdálená
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
15	[number]	k4	15
<g/>
m	m	kA	m
(	(	kIx(	(
<g/>
poloměr	poloměr	k1gInSc1	poloměr
jádra	jádro	k1gNnSc2	jádro
atomu	atom	k1gInSc2	atom
<g/>
)	)	kIx)	)
působí	působit	k5eAaImIp3nS	působit
gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
34	[number]	k4	34
<g/>
N.	N.	kA	N.
Elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
síla	síla	k1gFnSc1	síla
nekonečného	konečný	k2eNgInSc2d1	nekonečný
dosahu	dosah	k1gInSc2	dosah
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
částice	částice	k1gFnPc4	částice
s	s	k7c7	s
elektrickým	elektrický	k2eAgInSc7d1	elektrický
nábojem	náboj	k1gInSc7	náboj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
atomu	atom	k1gInSc6	atom
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
elektron	elektron	k1gInSc1	elektron
a	a	k8xC	a
proton	proton	k1gInSc1	proton
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
neutron	neutron	k1gInSc1	neutron
je	být	k5eAaImIp3nS	být
elektricky	elektricky	k6eAd1	elektricky
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
.	.	kIx.	.
</s>
<s>
Přitažlivá	přitažlivý	k2eAgFnSc1d1	přitažlivá
elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
síla	síla	k1gFnSc1	síla
mezi	mezi	k7c7	mezi
záporně	záporně	k6eAd1	záporně
nabitým	nabitý	k2eAgInSc7d1	nabitý
elektronem	elektron	k1gInSc7	elektron
a	a	k8xC	a
kladně	kladně	k6eAd1	kladně
nabitým	nabitý	k2eAgInSc7d1	nabitý
protonem	proton	k1gInSc7	proton
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
drží	držet	k5eAaImIp3nS	držet
elektrony	elektron	k1gInPc4	elektron
v	v	k7c6	v
elektronovém	elektronový	k2eAgInSc6d1	elektronový
obalu	obal	k1gInSc6	obal
a	a	k8xC	a
brání	bránit	k5eAaImIp3nS	bránit
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
atom	atom	k1gInSc1	atom
opustily	opustit	k5eAaPmAgFnP	opustit
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
silou	síla	k1gFnSc7	síla
řádu	řád	k1gInSc2	řád
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
8	[number]	k4	8
<g/>
N.	N.	kA	N.
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
mezi	mezi	k7c7	mezi
samotnými	samotný	k2eAgInPc7d1	samotný
protony	proton	k1gInPc7	proton
působí	působit	k5eAaImIp3nP	působit
odpudivá	odpudivý	k2eAgFnSc1d1	odpudivá
elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
100	[number]	k4	100
N.	N.	kA	N.
Silná	silný	k2eAgFnSc1d1	silná
jaderná	jaderný	k2eAgFnSc1d1	jaderná
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
přitažlivá	přitažlivý	k2eAgFnSc1d1	přitažlivá
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgInPc7	všecek
kvarky	kvark	k1gInPc7	kvark
a	a	k8xC	a
gluony	gluon	k1gInPc7	gluon
až	až	k6eAd1	až
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
15	[number]	k4	15
<g/>
m.	m.	k?	m.
V	v	k7c6	v
atomu	atom	k1gInSc6	atom
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
kvarků	kvark	k1gInPc2	kvark
složeny	složen	k2eAgFnPc1d1	složena
proton	proton	k1gInSc4	proton
a	a	k8xC	a
neutron	neutron	k1gInSc4	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Silná	silný	k2eAgFnSc1d1	silná
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
protony	proton	k1gInPc1	proton
a	a	k8xC	a
neutrony	neutron	k1gInPc1	neutron
drží	držet	k5eAaImIp3nP	držet
pohromadě	pohromadě	k6eAd1	pohromadě
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
atomové	atomový	k2eAgNnSc1d1	atomové
jádro	jádro	k1gNnSc1	jádro
a	a	k8xC	a
především	především	k9	především
<g/>
,	,	kIx,	,
že	že	k8xS	že
drží	držet	k5eAaImIp3nP	držet
pohromadě	pohromadě	k6eAd1	pohromadě
protony	proton	k1gInPc4	proton
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
elektricky	elektricky	k6eAd1	elektricky
odpuzují	odpuzovat	k5eAaImIp3nP	odpuzovat
<g/>
.	.	kIx.	.
</s>
<s>
Slabá	slabý	k2eAgFnSc1d1	slabá
jaderná	jaderný	k2eAgFnSc1d1	jaderná
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
síla	síla	k1gFnSc1	síla
působící	působící	k2eAgFnSc1d1	působící
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgInPc7	všecek
fermiony	fermion	k1gInPc7	fermion
až	až	k6eAd1	až
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
18	[number]	k4	18
<g/>
m.	m.	k?	m.
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
atomu	atom	k1gInSc6	atom
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc1	všechen
částice	částice	k1gFnPc1	částice
fermiony	fermion	k1gInPc4	fermion
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mají	mít	k5eAaImIp3nP	mít
spin	spin	k1gInSc4	spin
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
interakce	interakce	k1gFnPc4	interakce
v	v	k7c6	v
atomovém	atomový	k2eAgInSc6d1	atomový
obalu	obal	k1gInSc6	obal
(	(	kIx(	(
<g/>
elektrony	elektron	k1gInPc7	elektron
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
elektrony	elektron	k1gInPc1	elektron
navzájem	navzájem	k6eAd1	navzájem
<g/>
,	,	kIx,	,
přechody	přechod	k1gInPc1	přechod
mezi	mezi	k7c7	mezi
energetickými	energetický	k2eAgInPc7d1	energetický
stavy	stav	k1gInPc7	stav
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
interakce	interakce	k1gFnSc1	interakce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
atomovém	atomový	k2eAgNnSc6d1	atomové
jádře	jádro	k1gNnSc6	jádro
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
projevuje	projevovat	k5eAaImIp3nS	projevovat
silná	silný	k2eAgFnSc1d1	silná
interakce	interakce	k1gFnSc1	interakce
(	(	kIx(	(
<g/>
drží	držet	k5eAaImIp3nP	držet
jádro	jádro	k1gNnSc4	jádro
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
radioaktivní	radioaktivní	k2eAgFnSc4d1	radioaktivní
přeměnu	přeměna	k1gFnSc4	přeměna
alfa	alfa	k1gNnSc2	alfa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
slabá	slabý	k2eAgFnSc1d1	slabá
interakce	interakce	k1gFnSc1	interakce
(	(	kIx(	(
<g/>
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
radioaktivní	radioaktivní	k2eAgFnSc4d1	radioaktivní
přeměnu	přeměna	k1gFnSc4	přeměna
beta	beta	k1gNnSc2	beta
<g/>
)	)	kIx)	)
a	a	k8xC	a
elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
interakce	interakce	k1gFnSc1	interakce
(	(	kIx(	(
<g/>
snižuje	snižovat	k5eAaImIp3nS	snižovat
vazbovou	vazbový	k2eAgFnSc4d1	vazbová
energii	energie	k1gFnSc4	energie
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
přechody	přechod	k1gInPc4	přechod
mezi	mezi	k7c7	mezi
energetickými	energetický	k2eAgInPc7d1	energetický
stavy	stav	k1gInPc7	stav
jádra	jádro	k1gNnSc2	jádro
–	–	k?	–
radioaktivitu	radioaktivita	k1gFnSc4	radioaktivita
gama	gama	k1gNnSc1	gama
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vizualizaci	vizualizace	k1gFnSc4	vizualizace
makroskopických	makroskopický	k2eAgInPc2d1	makroskopický
objektů	objekt	k1gInPc2	objekt
lze	lze	k6eAd1	lze
bezproblémově	bezproblémově	k6eAd1	bezproblémově
provést	provést	k5eAaPmF	provést
opticky	opticky	k6eAd1	opticky
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pomocí	pomocí	k7c2	pomocí
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
lze	lze	k6eAd1	lze
proces	proces	k1gInSc4	proces
vizualizace	vizualizace	k1gFnSc2	vizualizace
popsat	popsat	k5eAaPmF	popsat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
Světlo	světlo	k1gNnSc1	světlo
putuje	putovat	k5eAaImIp3nS	putovat
ze	z	k7c2	z
zdroje	zdroj	k1gInSc2	zdroj
záření	záření	k1gNnSc2	záření
na	na	k7c4	na
objekt	objekt	k1gInSc4	objekt
od	od	k7c2	od
nějž	jenž	k3xRgNnSc2	jenž
se	se	k3xPyFc4	se
odrazí	odrazit	k5eAaPmIp3nS	odrazit
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
světlo	světlo	k1gNnSc1	světlo
pohlceno	pohlcen	k2eAgNnSc1d1	pohlceno
a	a	k8xC	a
následně	následně	k6eAd1	následně
vyzářeno	vyzářen	k2eAgNnSc1d1	vyzářeno
<g/>
)	)	kIx)	)
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
měřícího	měřící	k2eAgInSc2d1	měřící
přístroje	přístroj	k1gInSc2	přístroj
<g/>
,	,	kIx,	,
např.	např.	kA	např.
mikroskopu	mikroskop	k1gInSc2	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
Makroskopické	makroskopický	k2eAgInPc1d1	makroskopický
objekty	objekt	k1gInPc1	objekt
jsou	být	k5eAaImIp3nP	být
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
hmotnosti	hmotnost	k1gFnSc3	hmotnost
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
světelným	světelný	k2eAgInSc7d1	světelný
tokem	tok	k1gInSc7	tok
pouze	pouze	k6eAd1	pouze
nepatrně	patrně	k6eNd1	patrně
a	a	k8xC	a
vliv	vliv	k1gInSc4	vliv
měření	měření	k1gNnSc2	měření
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
zanedbává	zanedbávat	k5eAaImIp3nS	zanedbávat
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
nastává	nastávat	k5eAaImIp3nS	nastávat
u	u	k7c2	u
mikroskopických	mikroskopický	k2eAgInPc2d1	mikroskopický
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
atomy	atom	k1gInPc1	atom
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vliv	vliv	k1gInSc4	vliv
měření	měření	k1gNnSc2	měření
zanedbat	zanedbat	k5eAaPmF	zanedbat
nelze	lze	k6eNd1	lze
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
přesnost	přesnost	k1gFnSc1	přesnost
měření	měření	k1gNnSc2	měření
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
principem	princip	k1gInSc7	princip
neurčitosti	neurčitost	k1gFnSc2	neurčitost
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
△	△	k?	△
x	x	k?	x
△	△	k?	△
p	p	k?	p
≥	≥	k?	≥
ħ	ħ	k?	ħ
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
triangle	triangl	k1gInSc5	triangl
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
triangle	triangl	k1gInSc5	triangl
p	p	k?	p
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
\	\	kIx~	\
<g/>
hbar	hbar	k1gMnSc1	hbar
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
polohu	poloha	k1gFnSc4	poloha
atomu	atom	k1gInSc2	atom
nemůžeme	moct	k5eNaImIp1nP	moct
změřit	změřit	k5eAaPmF	změřit
naprosto	naprosto	k6eAd1	naprosto
přesně	přesně	k6eAd1	přesně
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
obraz	obraz	k1gInSc1	obraz
bude	být	k5eAaImBp3nS	být
vždy	vždy	k6eAd1	vždy
rozostřen	rozostřen	k2eAgInSc1d1	rozostřen
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
omezení	omezení	k1gNnSc1	omezení
nelze	lze	k6eNd1	lze
nijak	nijak	k6eAd1	nijak
obejít	obejít	k5eAaPmF	obejít
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yRnSc7	co
menší	malý	k2eAgNnSc1d2	menší
rozostření	rozostření	k1gNnSc1	rozostření
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
△	△	k?	△
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
triangle	triangl	k1gInSc5	triangl
x	x	k?	x
<g/>
}	}	kIx)	}
chceme	chtít	k5eAaImIp1nP	chtít
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
hybnost	hybnost	k1gFnSc1	hybnost
světlo	světlo	k1gNnSc4	světlo
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ale	ale	k8xC	ale
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c4	o
rozostření	rozostření	k1gNnSc4	rozostření
menším	menšit	k5eAaImIp1nS	menšit
než	než	k8xS	než
velikost	velikost	k1gFnSc1	velikost
atomu	atom	k1gInSc2	atom
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
△	△	k?	△
x	x	k?	x
≈	≈	k?	≈
:	:	kIx,	:
10	[number]	k4	10
:	:	kIx,	:
−	−	k?	−
10	[number]	k4	10
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
triangle	triangl	k1gInSc5	triangl
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
approx	approx	k1gInSc1	approx
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
tak	tak	k8xC	tak
hybnost	hybnost	k1gFnSc1	hybnost
fotonů	foton	k1gInPc2	foton
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
△	△	k?	△
p	p	k?	p
≈	≈	k?	≈
:	:	kIx,	:
10	[number]	k4	10
:	:	kIx,	:
−	−	k?	−
24	[number]	k4	24
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
kgms	kgms	k6eAd1	kgms
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
<g />
.	.	kIx.	.
</s>
<s hack="1">
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
triangle	triangl	k1gInSc5	triangl
p	p	k?	p
<g/>
\	\	kIx~	\
<g/>
approx	approx	k1gInSc1	approx
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
kgms	kgms	k1gInSc1	kgms
<g/>
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
,	,	kIx,	,
neboli	neboli	k8xC	neboli
minimální	minimální	k2eAgFnSc1d1	minimální
frekvence	frekvence	k1gFnSc1	frekvence
použitelného	použitelný	k2eAgNnSc2d1	použitelné
světla	světlo	k1gNnSc2	světlo
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ν	ν	k?	ν
≈	≈	k?	≈
:	:	kIx,	:
10	[number]	k4	10
:	:	kIx,	:
18	[number]	k4	18
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Hz	Hz	kA	Hz
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nu	nu	k9	nu
\	\	kIx~	\
<g/>
approx	approx	k1gInSc1	approx
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
18	[number]	k4	18
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
Hz	Hz	kA	Hz
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
frekvence	frekvence	k1gFnSc1	frekvence
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
řádu	řád	k1gInSc2	řád
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
10	[number]	k4	10
:	:	kIx,	:
14	[number]	k4	14
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Hz	Hz	kA	Hz
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
14	[number]	k4	14
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
Hz	Hz	kA	Hz
<g/>
}}}	}}}	k?	}}}
a	a	k8xC	a
tedy	tedy	k9	tedy
viditelným	viditelný	k2eAgNnSc7d1	viditelné
světlem	světlo	k1gNnSc7	světlo
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgInPc1d1	možný
atomy	atom	k1gInPc1	atom
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Zpětný	zpětný	k2eAgInSc1d1	zpětný
výpočet	výpočet	k1gInSc1	výpočet
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
viditelným	viditelný	k2eAgNnSc7d1	viditelné
světlem	světlo	k1gNnSc7	světlo
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
pouze	pouze	k6eAd1	pouze
objekty	objekt	k1gInPc4	objekt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
alespoň	alespoň	k9	alespoň
10000	[number]	k4	10000
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
atom	atom	k1gInSc4	atom
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
omezení	omezení	k1gNnSc1	omezení
je	být	k5eAaImIp3nS	být
platné	platný	k2eAgNnSc1d1	platné
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
optické	optický	k2eAgInPc4d1	optický
mikroskopy	mikroskop	k1gInPc4	mikroskop
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
konstrukci	konstrukce	k1gFnSc6	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
daly	dát	k5eAaPmAgInP	dát
atomy	atom	k1gInPc1	atom
vizualizovat	vizualizovat	k5eAaImF	vizualizovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
místo	místo	k1gNnSc1	místo
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
použít	použít	k5eAaPmF	použít
něco	něco	k3yInSc1	něco
jiného	jiný	k2eAgNnSc2d1	jiné
nebo	nebo	k8xC	nebo
použít	použít	k5eAaPmF	použít
úplně	úplně	k6eAd1	úplně
jiný	jiný	k2eAgInSc4d1	jiný
způsob	způsob	k1gInSc4	způsob
vizualizace	vizualizace	k1gFnSc2	vizualizace
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
základní	základní	k2eAgNnPc4d1	základní
zařízení	zařízení	k1gNnPc4	zařízení
pro	pro	k7c4	pro
vizualizaci	vizualizace	k1gFnSc4	vizualizace
atomů	atom	k1gInPc2	atom
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Elektronový	elektronový	k2eAgInSc1d1	elektronový
mikroskop	mikroskop	k1gInSc1	mikroskop
–	–	k?	–
Namísto	namísto	k7c2	namísto
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
fotonů	foton	k1gInPc2	foton
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
elektrony	elektron	k1gInPc1	elektron
a	a	k8xC	a
namísto	namísto	k7c2	namísto
optických	optický	k2eAgFnPc2d1	optická
čoček	čočka	k1gFnPc2	čočka
používá	používat	k5eAaImIp3nS	používat
elektromagnetické	elektromagnetický	k2eAgFnPc4d1	elektromagnetická
čočky	čočka	k1gFnPc4	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
elektronů	elektron	k1gInPc2	elektron
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
kvantová	kvantový	k2eAgFnSc1d1	kvantová
mechanika	mechanika	k1gFnSc1	mechanika
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
mají	mít	k5eAaImIp3nP	mít
všechny	všechen	k3xTgFnPc1	všechen
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
elektron	elektron	k1gInSc1	elektron
<g/>
,	,	kIx,	,
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
povahu	povaha	k1gFnSc4	povaha
(	(	kIx(	(
<g/>
korpuskulárně-vlnový	korpuskulárnělnový	k2eAgInSc1d1	korpuskulárně-vlnový
dualismus	dualismus	k1gInSc1	dualismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výhoda	výhoda	k1gFnSc1	výhoda
elektronového	elektronový	k2eAgInSc2d1	elektronový
mikroskopu	mikroskop	k1gInSc2	mikroskop
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
mnohostrannost	mnohostrannost	k1gFnSc1	mnohostrannost
<g/>
.	.	kIx.	.
</s>
<s>
Řádkovací	řádkovací	k2eAgInSc1d1	řádkovací
tunelový	tunelový	k2eAgInSc1d1	tunelový
mikroskop	mikroskop	k1gInSc1	mikroskop
–	–	k?	–
Pro	pro	k7c4	pro
zobrazení	zobrazení	k1gNnSc4	zobrazení
povrchu	povrch	k1gInSc2	povrch
využívá	využívat	k5eAaPmIp3nS	využívat
tunelový	tunelový	k2eAgInSc1d1	tunelový
jev	jev	k1gInSc1	jev
<g/>
.	.	kIx.	.
</s>
<s>
Ostrý	ostrý	k2eAgInSc1d1	ostrý
hrot	hrot	k1gInSc1	hrot
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
zkoumaného	zkoumaný	k2eAgInSc2d1	zkoumaný
vzorku	vzorek	k1gInSc2	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Tunelový	tunelový	k2eAgInSc1d1	tunelový
jev	jev	k1gInSc1	jev
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přechod	přechod	k1gInSc4	přechod
proudu	proud	k1gInSc2	proud
(	(	kIx(	(
<g/>
elektronů	elektron	k1gInPc2	elektron
<g/>
)	)	kIx)	)
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
na	na	k7c4	na
hrot	hrot	k1gInSc4	hrot
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
hrot	hrot	k1gInSc1	hrot
povrchu	povrch	k1gInSc2	povrch
"	"	kIx"	"
<g/>
nedotýká	dotýkat	k5eNaImIp3nS	dotýkat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mikroskop	mikroskop	k1gInSc1	mikroskop
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
poloze	poloha	k1gFnSc6	poloha
hrotu	hrot	k1gInSc2	hrot
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
[	[	kIx(	[
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
,	,	kIx,	,
0	[number]	k4	0
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
[	[	kIx(	[
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
,0	,0	k4	,0
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
z	z	k7c2	z
velikosti	velikost	k1gFnSc2	velikost
procházejícího	procházející	k2eAgInSc2d1	procházející
proudu	proud	k1gInSc2	proud
určí	určit	k5eAaPmIp3nS	určit
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
hrotem	hrot	k1gInSc7	hrot
a	a	k8xC	a
povrchem	povrch	k1gInSc7	povrch
a	a	k8xC	a
tedy	tedy	k9	tedy
výšku	výška	k1gFnSc4	výška
povrchu	povrch	k1gInSc2	povrch
(	(	kIx(	(
<g/>
z-ovou	zvý	k2eAgFnSc4d1	z-ová
souřadnici	souřadnice	k1gFnSc4	souřadnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
zmapování	zmapování	k1gNnSc4	zmapování
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
hrotu	hrot	k1gInSc2	hrot
a	a	k8xC	a
povrchu	povrch	k1gInSc2	povrch
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
bodech	bod	k1gInPc6	bod
roviny	rovina	k1gFnSc2	rovina
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
hrot	hrot	k1gInSc1	hrot
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
<g/>
,	,	kIx,	,
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
mikroskop	mikroskop	k1gInSc4	mikroskop
obraz	obraz	k1gInSc1	obraz
reliéfu	reliéf	k1gInSc2	reliéf
zkoumaného	zkoumaný	k2eAgInSc2d1	zkoumaný
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
tunelovacího	tunelovací	k2eAgInSc2d1	tunelovací
mikroskopu	mikroskop	k1gInSc2	mikroskop
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
použít	použít	k5eAaPmF	použít
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
vizualizaci	vizualizace	k1gFnSc4	vizualizace
vodivých	vodivý	k2eAgInPc2d1	vodivý
povrchů	povrch	k1gInPc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
AFM	AFM	kA	AFM
mikroskop	mikroskop	k1gInSc1	mikroskop
–	–	k?	–
Pro	pro	k7c4	pro
zobrazení	zobrazení	k1gNnSc4	zobrazení
povrchu	povrch	k1gInSc2	povrch
využívá	využívat	k5eAaPmIp3nS	využívat
atomárních	atomární	k2eAgFnPc2d1	atomární
sil	síla	k1gFnPc2	síla
atomů	atom	k1gInPc2	atom
povrchu	povrch	k1gInSc2	povrch
zkoumaného	zkoumaný	k2eAgInSc2d1	zkoumaný
vzorku	vzorek	k1gInSc2	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Ostrý	ostrý	k2eAgInSc1d1	ostrý
hrot	hrot	k1gInSc1	hrot
připevněný	připevněný	k2eAgInSc1d1	připevněný
na	na	k7c6	na
ohebném	ohebný	k2eAgInSc6d1	ohebný
nosníku	nosník	k1gInSc6	nosník
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
vzorku	vzorek	k1gInSc2	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Atomární	atomární	k2eAgFnPc1d1	atomární
síly	síla	k1gFnPc1	síla
povrchových	povrchový	k2eAgInPc2d1	povrchový
atomů	atom	k1gInPc2	atom
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
hrot	hrot	k1gInSc4	hrot
a	a	k8xC	a
ohýbají	ohýbat	k5eAaImIp3nP	ohýbat
nosník	nosník	k1gInSc4	nosník
<g/>
.	.	kIx.	.
</s>
<s>
Mikroskop	mikroskop	k1gInSc1	mikroskop
sledováním	sledování	k1gNnSc7	sledování
ohybu	ohyb	k1gInSc2	ohyb
nosníku	nosník	k1gInSc2	nosník
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
poloze	poloha	k1gFnSc6	poloha
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
[	[	kIx(	[
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
[	[	kIx(	[
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
určí	určit	k5eAaPmIp3nS	určit
polohu	poloha	k1gFnSc4	poloha
hrotu	hrot	k1gInSc2	hrot
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
výšku	výška	k1gFnSc4	výška
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vizualizaci	vizualizace	k1gFnSc3	vizualizace
reliéfu	reliéf	k1gInSc2	reliéf
celého	celý	k2eAgInSc2d1	celý
povrchu	povrch	k1gInSc2	povrch
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
hrotem	hrot	k1gInSc7	hrot
zmapovat	zmapovat	k5eAaPmF	zmapovat
celý	celý	k2eAgInSc4d1	celý
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Výhoda	výhoda	k1gFnSc1	výhoda
AFM	AFM	kA	AFM
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
použít	použít	k5eAaPmF	použít
i	i	k9	i
pro	pro	k7c4	pro
vizualizaci	vizualizace	k1gFnSc4	vizualizace
nevodivých	vodivý	k2eNgInPc2d1	nevodivý
povrchů	povrch	k1gInPc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
atomy	atom	k1gInPc7	atom
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
podařilo	podařit	k5eAaPmAgNnS	podařit
manipulovat	manipulovat	k5eAaImF	manipulovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
Donaldu	Donald	k1gMnSc3	Donald
Eiglerovi	Eigler	k1gMnSc3	Eigler
z	z	k7c2	z
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ze	z	k7c2	z
35	[number]	k4	35
atomů	atom	k1gInPc2	atom
xenonu	xenon	k1gInSc2	xenon
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
nápis	nápis	k1gInSc4	nápis
"	"	kIx"	"
<g/>
IBM	IBM	kA	IBM
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
manipulaci	manipulace	k1gFnSc4	manipulace
s	s	k7c7	s
atomy	atom	k1gInPc7	atom
použil	použít	k5eAaPmAgInS	použít
řádkovací	řádkovací	k2eAgInSc1d1	řádkovací
tunelový	tunelový	k2eAgInSc1d1	tunelový
mikroskop	mikroskop	k1gInSc1	mikroskop
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
nízkých	nízký	k2eAgFnPc6d1	nízká
teplotách	teplota	k1gFnPc6	teplota
a	a	k8xC	a
v	v	k7c6	v
ultravysokém	ultravysoký	k2eAgNnSc6d1	ultravysoké
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
.	.	kIx.	.
</s>
<s>
Manipulace	manipulace	k1gFnSc1	manipulace
s	s	k7c7	s
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
atomy	atom	k1gInPc7	atom
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležitá	důležitý	k2eAgFnSc1d1	důležitá
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgInSc4d1	budoucí
rozvoj	rozvoj	k1gInSc4	rozvoj
nanotechnologie	nanotechnologie	k1gFnSc2	nanotechnologie
<g/>
,	,	kIx,	,
oboru	obor	k1gInSc2	obor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
molekul	molekula	k1gFnPc2	molekula
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
vytvářet	vytvářet	k5eAaImF	vytvářet
materiály	materiál	k1gInPc4	materiál
speciálních	speciální	k2eAgFnPc2d1	speciální
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
materiály	materiál	k1gInPc1	materiál
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
pevností	pevnost	k1gFnSc7	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwood	k1gInSc1	Greenwood
–	–	k?	–
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc2	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
fyzika	fyzika	k1gFnSc1	fyzika
Chemie	chemie	k1gFnSc2	chemie
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
atom	atom	k1gInSc4	atom
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Galerie	galerie	k1gFnSc2	galerie
atom	atom	k1gInSc4	atom
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
atom	atom	k1gInSc1	atom
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
