<s>
Atom	atom	k1gInSc1	atom
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
ἄ	ἄ	k?	ἄ
<g/>
,	,	kIx,	,
átomos	átomos	k1gInSc1	átomos
–	–	k?	–
nedělitelný	dělitelný	k2eNgInSc1d1	nedělitelný
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgFnSc1d3	nejmenší
částice	částice	k1gFnSc1	částice
běžné	běžný	k2eAgFnSc2d1	běžná
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
částice	částice	k1gFnSc2	částice
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
už	už	k6eAd1	už
chemickými	chemický	k2eAgInPc7d1	chemický
prostředky	prostředek	k1gInPc7	prostředek
dále	daleko	k6eAd2	daleko
nelze	lze	k6eNd1	lze
dělit	dělit	k5eAaImF	dělit
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
fyzikálními	fyzikální	k2eAgInPc7d1	fyzikální
ano	ano	k9	ano
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
jaderná	jaderný	k2eAgFnSc1d1	jaderná
reakce	reakce	k1gFnSc1	reakce
<g/>
)	)	kIx)	)
a	a	k8xC	a
která	který	k3yQgFnSc1	který
definuje	definovat	k5eAaBmIp3nS	definovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
daného	daný	k2eAgInSc2d1	daný
chemického	chemický	k2eAgInSc2d1	chemický
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
