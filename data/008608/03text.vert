<p>
<s>
Do	do	k7c2	do
soutěže	soutěž	k1gFnSc2	soutěž
dvouhry	dvouhra	k1gFnSc2	dvouhra
juniorů	junior	k1gMnPc2	junior
na	na	k7c6	na
US	US	kA	US
Open	Open	k1gInSc4	Open
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
sedmý	sedmý	k4xOgInSc4	sedmý
den	den	k1gInSc4	den
newyorského	newyorský	k2eAgInSc2d1	newyorský
grandslamu	grandslam	k1gInSc2	grandslam
<g/>
,	,	kIx,	,
nastoupilo	nastoupit	k5eAaPmAgNnS	nastoupit
šedesát	šedesát	k4xCc1	šedesát
čtyři	čtyři	k4xCgNnPc4	čtyři
tenistů	tenista	k1gMnPc2	tenista
<g/>
.	.	kIx.	.
</s>
<s>
Obhájcem	obhájce	k1gMnSc7	obhájce
titulu	titul	k1gInSc2	titul
byl	být	k5eAaImAgMnS	být
australský	australský	k2eAgMnSc1d1	australský
hráč	hráč	k1gMnSc1	hráč
Bernard	Bernard	k1gMnSc1	Bernard
Tomic	Tomic	k1gMnSc1	Tomic
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
sezóny	sezóna	k1gFnSc2	sezóna
2010	[number]	k4	2010
přestal	přestat	k5eAaPmAgMnS	přestat
účastnit	účastnit	k5eAaImF	účastnit
turnajů	turnaj	k1gInPc2	turnaj
juniorské	juniorský	k2eAgFnSc2d1	juniorská
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
byl	být	k5eAaImAgInS	být
ke	k	k7c3	k
startu	start	k1gInSc3	start
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
stále	stále	k6eAd1	stále
způsobilý	způsobilý	k2eAgInSc1d1	způsobilý
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Američan	Američan	k1gMnSc1	Američan
Jack	Jack	k1gMnSc1	Jack
Sock	Sock	k1gMnSc1	Sock
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgMnSc1d1	hrající
na	na	k7c4	na
divokou	divoký	k2eAgFnSc4d1	divoká
kartu	karta	k1gFnSc4	karta
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
porazil	porazit	k5eAaPmAgMnS	porazit
krajana	krajan	k1gMnSc4	krajan
a	a	k8xC	a
turnajovou	turnajový	k2eAgFnSc4d1	turnajová
desítku	desítka	k1gFnSc4	desítka
Denise	Denisa	k1gFnSc6	Denisa
Kudlu	kudla	k1gFnSc4	kudla
po	po	k7c6	po
třísetovém	třísetový	k2eAgInSc6d1	třísetový
průběhu	průběh	k1gInSc6	průběh
utkání	utkání	k1gNnSc1	utkání
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nasazení	nasazení	k1gNnSc1	nasazení
hráčů	hráč	k1gMnPc2	hráč
==	==	k?	==
</s>
</p>
<p>
<s>
Juan	Juan	k1gMnSc1	Juan
Sebastián	Sebastián	k1gMnSc1	Sebastián
Gómez	Gómez	k1gMnSc1	Gómez
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Márton	Márton	k1gInSc1	Márton
Fucsovics	Fucsovics	k1gInSc1	Fucsovics
(	(	kIx(	(
<g/>
semifinále	semifinále	k1gNnSc1	semifinále
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tiago	Tiago	k1gMnSc1	Tiago
Fernandes	Fernandes	k1gMnSc1	Fernandes
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Veselý	Veselý	k1gMnSc1	Veselý
(	(	kIx(	(
<g/>
čtvrtfinále	čtvrtfinále	k1gNnSc1	čtvrtfinále
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Damir	Damir	k1gMnSc1	Damir
Džumhur	Džumhur	k1gMnSc1	Džumhur
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jason	Jason	k1gMnSc1	Jason
Kubler	Kubler	k1gMnSc1	Kubler
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Duilio	Duilio	k1gMnSc1	Duilio
Beretta	Beretta	k1gMnSc1	Beretta
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
<g/>
,	,	kIx,	,
skreč	skreč	k1gFnSc1	skreč
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Agustín	Agustín	k1gInSc1	Agustín
Velotti	Velotti	k1gNnSc2	Velotti
(	(	kIx(	(
<g/>
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
James	James	k1gMnSc1	James
Duckworth	Duckworth	k1gMnSc1	Duckworth
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Denis	Denisa	k1gFnPc2	Denisa
Kudla	kudla	k1gFnSc1	kudla
(	(	kIx(	(
<g/>
finále	finále	k1gNnSc1	finále
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Renzo	Renza	k1gFnSc5	Renza
Olivo	oliva	k1gFnSc5	oliva
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Máté	Máté	k6eAd1	Máté
Zsiga	Zsiga	k1gFnSc1	Zsiga
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Victor	Victor	k1gMnSc1	Victor
Baluda	Baludo	k1gNnSc2	Baludo
(	(	kIx(	(
<g/>
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dominic	Dominice	k1gFnPc2	Dominice
Thiem	Thiem	k1gInSc1	Thiem
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Roberto	Roberta	k1gFnSc5	Roberta
Quiroz	Quiroz	k1gMnSc1	Quiroz
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mitchell	Mitchell	k1gMnSc1	Mitchell
Frank	Frank	k1gMnSc1	Frank
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Pavouk	pavouk	k1gMnSc1	pavouk
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Finálová	finálový	k2eAgFnSc1d1	finálová
fáze	fáze	k1gFnSc1	fáze
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Horní	horní	k2eAgFnSc1d1	horní
polovina	polovina	k1gFnSc1	polovina
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
1	[number]	k4	1
<g/>
.	.	kIx.	.
sekce	sekce	k1gFnSc1	sekce
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
2	[number]	k4	2
<g/>
.	.	kIx.	.
sekce	sekce	k1gFnSc1	sekce
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Dolní	dolní	k2eAgFnSc1d1	dolní
polovina	polovina	k1gFnSc1	polovina
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
3	[number]	k4	3
<g/>
.	.	kIx.	.
sekce	sekce	k1gFnSc1	sekce
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
4	[number]	k4	4
<g/>
.	.	kIx.	.
sekce	sekce	k1gFnSc1	sekce
====	====	k?	====
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
2010	[number]	k4	2010
US	US	kA	US
Open	Opena	k1gFnPc2	Opena
–	–	k?	–
Boys	boy	k1gMnPc2	boy
<g/>
'	'	kIx"	'
Singles	Singles	k1gMnSc1	Singles
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Pavouk	pavouk	k1gMnSc1	pavouk
dvouhry	dvouhra	k1gFnSc2	dvouhra
juniorů	junior	k1gMnPc2	junior
na	na	k7c6	na
US	US	kA	US
Open	Open	k1gInSc4	Open
2010	[number]	k4	2010
</s>
</p>
