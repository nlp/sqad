<s>
Do	do	k7c2	do
soutěže	soutěž	k1gFnSc2	soutěž
dvouhry	dvouhra	k1gFnSc2	dvouhra
juniorů	junior	k1gMnPc2	junior
na	na	k7c6	na
US	US	kA	US
Open	Open	k1gInSc4	Open
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
sedmý	sedmý	k4xOgInSc4	sedmý
den	den	k1gInSc4	den
newyorského	newyorský	k2eAgInSc2d1	newyorský
grandslamu	grandslam	k1gInSc2	grandslam
<g/>
,	,	kIx,	,
nastoupilo	nastoupit	k5eAaPmAgNnS	nastoupit
šedesát	šedesát	k4xCc1	šedesát
čtyři	čtyři	k4xCgNnPc4	čtyři
tenistů	tenista	k1gMnPc2	tenista
<g/>
.	.	kIx.	.
</s>
