<p>
<s>
Aaron	Aaron	k1gMnSc1	Aaron
Goodwin	Goodwin	k1gMnSc1	Goodwin
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1976	[number]	k4	1976
Portland	Portland	k1gInSc1	Portland
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
paranormální	paranormální	k2eAgMnSc1d1	paranormální
vyšetřovatel	vyšetřovatel	k1gMnSc1	vyšetřovatel
a	a	k8xC	a
kameraman	kameraman	k1gMnSc1	kameraman
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Zakem	Zak	k1gMnSc7	Zak
Bagansem	Bagans	k1gMnSc7	Bagans
a	a	k8xC	a
audio-techniky	audioechnik	k1gMnPc7	audio-technik
Billy	Bill	k1gMnPc7	Bill
Tolleym	Tolleymum	k1gNnPc2	Tolleymum
a	a	k8xC	a
Jayem	Jay	k1gMnSc7	Jay
Wasleyem	Wasley	k1gMnSc7	Wasley
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
pořadu	pořad	k1gInSc6	pořad
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
duchů	duch	k1gMnPc2	duch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Portlandu	Portland	k1gInSc6	Portland
v	v	k7c6	v
Oregonu	Oregon	k1gInSc6	Oregon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prožil	prožít	k5eAaPmAgMnS	prožít
i	i	k9	i
své	svůj	k3xOyFgNnSc4	svůj
dětství	dětství	k1gNnSc4	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Zakem	Zak	k1gMnSc7	Zak
Bagansem	Bagans	k1gMnSc7	Bagans
a	a	k8xC	a
Nickem	Nicek	k1gMnSc7	Nicek
Groffem	Groff	k1gMnSc7	Groff
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
kamerman	kamerman	k1gMnSc1	kamerman
v	v	k7c4	v
World	World	k1gInSc4	World
Wrestling	Wrestling	k1gInSc1	Wrestling
Entertainment	Entertainment	k1gInSc1	Entertainment
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Zakem	Zak	k1gMnSc7	Zak
Bagansem	Bagans	k1gMnSc7	Bagans
a	a	k8xC	a
Nickem	Nicek	k1gMnSc7	Nicek
Groffem	Groff	k1gMnSc7	Groff
a	a	k8xC	a
společně	společně	k6eAd1	společně
na	na	k7c6	na
kanále	kanál	k1gInSc6	kanál
Travel	Travela	k1gFnPc2	Travela
Channel	Channel	k1gInSc4	Channel
začali	začít	k5eAaPmAgMnP	začít
točit	točit	k5eAaImF	točit
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
seriál	seriál	k1gInSc4	seriál
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
duchů	duch	k1gMnPc2	duch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgMnS	být
i	i	k9	i
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
.	.	kIx.	.
</s>
</p>
