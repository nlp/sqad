<s>
Henry	Henry	k1gMnSc1	Henry
Martin	Martin	k1gMnSc1	Martin
Ford	ford	k1gInSc1	ford
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1863	[number]	k4	1863
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
průkopník	průkopník	k1gMnSc1	průkopník
automobilového	automobilový	k2eAgInSc2d1	automobilový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Nestal	stát	k5eNaPmAgInS	stát
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
představ	představa	k1gFnPc2	představa
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
farmářem	farmář	k1gMnSc7	farmář
a	a	k8xC	a
v	v	k7c6	v
šestnácti	šestnáct	k4xCc6	šestnáct
letech	léto	k1gNnPc6	léto
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Zkusil	zkusit	k5eAaPmAgMnS	zkusit
štěstí	štěstí	k1gNnSc1	štěstí
v	v	k7c6	v
Detroitu	Detroit	k1gInSc6	Detroit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
u	u	k7c2	u
firem	firma	k1gFnPc2	firma
James	James	k1gMnSc1	James
Flower	Flower	k1gMnSc1	Flower
&	&	k?	&
Bros	Bros	k1gInSc1	Bros
a	a	k8xC	a
Detroit	Detroit	k1gInSc1	Detroit
Dry	Dry	k1gMnSc1	Dry
Dock	Dock	k1gMnSc1	Dock
vyučil	vyučit	k5eAaPmAgMnS	vyučit
zámečníkem	zámečník	k1gMnSc7	zámečník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyučení	vyučení	k1gNnSc6	vyučení
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
firmy	firma	k1gFnSc2	firma
Westinghouse	Westinghouse	k1gFnSc2	Westinghouse
jako	jako	k8xS	jako
opravář	opravář	k1gMnSc1	opravář
parních	parní	k2eAgInPc2d1	parní
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
získal	získat	k5eAaPmAgMnS	získat
dobré	dobrý	k2eAgNnSc4d1	dobré
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
Thomase	Thomas	k1gMnSc2	Thomas
Alvy	Alva	k1gMnSc2	Alva
Edisona	Edison	k1gMnSc2	Edison
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
hlavního	hlavní	k2eAgMnSc2d1	hlavní
inženýra	inženýr	k1gMnSc2	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Ford	ford	k1gInSc1	ford
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
zabýval	zabývat	k5eAaImAgMnS	zabývat
experimenty	experiment	k1gInPc4	experiment
s	s	k7c7	s
benzinovými	benzinový	k2eAgInPc7d1	benzinový
motory	motor	k1gInPc7	motor
a	a	k8xC	a
stavbou	stavba	k1gFnSc7	stavba
vlastního	vlastní	k2eAgInSc2d1	vlastní
automobilu	automobil	k1gInSc2	automobil
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
sestrojil	sestrojit	k5eAaPmAgInS	sestrojit
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
vyzkoušel	vyzkoušet	k5eAaPmAgInS	vyzkoušet
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
automobil	automobil	k1gInSc4	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vůz	vůz	k1gInSc1	vůz
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
zájem	zájem	k1gInSc4	zájem
jak	jak	k8xC	jak
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
,	,	kIx,	,
tak	tak	k9	tak
investorů	investor	k1gMnPc2	investor
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
pomocí	pomoc	k1gFnSc7	pomoc
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
založil	založit	k5eAaPmAgMnS	založit
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
automobilovou	automobilový	k2eAgFnSc4d1	automobilová
firmu	firma	k1gFnSc4	firma
Detroit	Detroit	k1gInSc1	Detroit
Automobile	automobil	k1gInSc6	automobil
company	compana	k1gFnSc2	compana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ale	ale	k9	ale
brzy	brzy	k6eAd1	brzy
zkrachovala	zkrachovat	k5eAaPmAgFnS	zkrachovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Edsel	Edsel	k1gMnSc1	Edsel
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
společníky	společník	k1gMnPc7	společník
Ford	ford	k1gInSc4	ford
Motor	motor	k1gInSc1	motor
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
