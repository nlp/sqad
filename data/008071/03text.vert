<s>
Henry	Henry	k1gMnSc1	Henry
Martin	Martin	k1gMnSc1	Martin
Ford	ford	k1gInSc1	ford
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1863	[number]	k4	1863
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
průkopník	průkopník	k1gMnSc1	průkopník
automobilového	automobilový	k2eAgInSc2d1	automobilový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Nestal	stát	k5eNaPmAgInS	stát
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
představ	představa	k1gFnPc2	představa
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
farmářem	farmář	k1gMnSc7	farmář
a	a	k8xC	a
v	v	k7c6	v
šestnácti	šestnáct	k4xCc6	šestnáct
letech	léto	k1gNnPc6	léto
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Zkusil	zkusit	k5eAaPmAgMnS	zkusit
štěstí	štěstí	k1gNnSc1	štěstí
v	v	k7c6	v
Detroitu	Detroit	k1gInSc6	Detroit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
u	u	k7c2	u
firem	firma	k1gFnPc2	firma
James	James	k1gMnSc1	James
Flower	Flower	k1gMnSc1	Flower
&	&	k?	&
Bros	Bros	k1gInSc1	Bros
a	a	k8xC	a
Detroit	Detroit	k1gInSc1	Detroit
Dry	Dry	k1gMnSc1	Dry
Dock	Dock	k1gMnSc1	Dock
vyučil	vyučit	k5eAaPmAgMnS	vyučit
zámečníkem	zámečník	k1gMnSc7	zámečník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyučení	vyučení	k1gNnSc6	vyučení
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
firmy	firma	k1gFnSc2	firma
Westinghouse	Westinghouse	k1gFnSc2	Westinghouse
jako	jako	k8xS	jako
opravář	opravář	k1gMnSc1	opravář
parních	parní	k2eAgInPc2d1	parní
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
získal	získat	k5eAaPmAgMnS	získat
dobré	dobrý	k2eAgNnSc4d1	dobré
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
Thomase	Thomas	k1gMnSc2	Thomas
Alvy	Alva	k1gMnSc2	Alva
Edisona	Edison	k1gMnSc2	Edison
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
hlavního	hlavní	k2eAgMnSc2d1	hlavní
inženýra	inženýr	k1gMnSc2	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Ford	ford	k1gInSc1	ford
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
zabýval	zabývat	k5eAaImAgMnS	zabývat
experimenty	experiment	k1gInPc4	experiment
s	s	k7c7	s
benzinovými	benzinový	k2eAgInPc7d1	benzinový
motory	motor	k1gInPc7	motor
a	a	k8xC	a
stavbou	stavba	k1gFnSc7	stavba
vlastního	vlastní	k2eAgInSc2d1	vlastní
automobilu	automobil	k1gInSc2	automobil
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
sestrojil	sestrojit	k5eAaPmAgInS	sestrojit
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
vyzkoušel	vyzkoušet	k5eAaPmAgInS	vyzkoušet
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
automobil	automobil	k1gInSc4	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vůz	vůz	k1gInSc1	vůz
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
zájem	zájem	k1gInSc4	zájem
jak	jak	k8xC	jak
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
,	,	kIx,	,
tak	tak	k9	tak
investorů	investor	k1gMnPc2	investor
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
pomocí	pomoc	k1gFnSc7	pomoc
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
založil	založit	k5eAaPmAgMnS	založit
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
automobilovou	automobilový	k2eAgFnSc4d1	automobilová
firmu	firma	k1gFnSc4	firma
Detroit	Detroit	k1gInSc1	Detroit
Automobile	automobil	k1gInSc6	automobil
company	compana	k1gFnSc2	compana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ale	ale	k9	ale
brzy	brzy	k6eAd1	brzy
zkrachovala	zkrachovat	k5eAaPmAgFnS	zkrachovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Edsel	Edsel	k1gMnSc1	Edsel
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
společníky	společník	k1gMnPc7	společník
Ford	ford	k1gInSc4	ford
Motor	motor	k1gInSc1	motor
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
nová	nový	k2eAgFnSc1d1	nová
firma	firma	k1gFnSc1	firma
dokázala	dokázat	k5eAaPmAgFnS	dokázat
s	s	k7c7	s
počátečním	počáteční	k2eAgInSc7d1	počáteční
kapitálem	kapitál	k1gInSc7	kapitál
pouhých	pouhý	k2eAgInPc2d1	pouhý
28000	[number]	k4	28000
dolarů	dolar	k1gInPc2	dolar
už	už	k6eAd1	už
za	za	k7c4	za
první	první	k4xOgInSc4	první
rok	rok	k1gInSc4	rok
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
vyrobit	vyrobit	k5eAaPmF	vyrobit
až	až	k9	až
1700	[number]	k4	1700
automobilů	automobil	k1gInPc2	automobil
a	a	k8xC	a
několik	několik	k4yIc1	několik
prototypů	prototyp	k1gInPc2	prototyp
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
přinesly	přinést	k5eAaPmAgInP	přinést
jeho	jeho	k3xOp3gNnPc3	jeho
firmě	firma	k1gFnSc6	firma
mnoho	mnoho	k4c4	mnoho
vítězství	vítězství	k1gNnPc2	vítězství
v	v	k7c6	v
automobilových	automobilový	k2eAgInPc6d1	automobilový
závodech	závod	k1gInPc6	závod
a	a	k8xC	a
několik	několik	k4yIc1	několik
rychlostních	rychlostní	k2eAgInPc2d1	rychlostní
rekordů	rekord	k1gInPc2	rekord
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
prvním	první	k4xOgInSc7	první
rokem	rok	k1gInSc7	rok
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
vcelku	vcelku	k6eAd1	vcelku
spokojen	spokojen	k2eAgMnSc1d1	spokojen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
odhodlán	odhodlán	k2eAgMnSc1d1	odhodlán
splnit	splnit	k5eAaPmF	splnit
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
cíl	cíl	k1gInSc4	cíl
vyrobit	vyrobit	k5eAaPmF	vyrobit
levné	levný	k2eAgNnSc1d1	levné
kvalitní	kvalitní	k2eAgNnSc1d1	kvalitní
auto	auto	k1gNnSc1	auto
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1906	[number]	k4	1906
až	až	k9	až
1908	[number]	k4	1908
zaváděl	zavádět	k5eAaImAgInS	zavádět
po	po	k7c6	po
několikaměsíčním	několikaměsíční	k2eAgInSc6d1	několikaměsíční
vývoji	vývoj	k1gInSc6	vývoj
výrobu	výrob	k1gInSc2	výrob
legendárních	legendární	k2eAgInPc2d1	legendární
modelů	model	k1gInPc2	model
N	N	kA	N
a	a	k8xC	a
T	T	kA	T
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
prodávaly	prodávat	k5eAaImAgInP	prodávat
jen	jen	k9	jen
za	za	k7c4	za
825	[number]	k4	825
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
vlastně	vlastně	k9	vlastně
splnil	splnit	k5eAaPmAgMnS	splnit
svůj	svůj	k3xOyFgInSc4	svůj
daný	daný	k2eAgInSc4d1	daný
cíl	cíl	k1gInSc4	cíl
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Ford	ford	k1gInSc1	ford
model	model	k1gInSc1	model
T	T	kA	T
překonal	překonat	k5eAaPmAgInS	překonat
orientaci	orientace	k1gFnSc4	orientace
automobilového	automobilový	k2eAgInSc2d1	automobilový
průmyslu	průmysl	k1gInSc2	průmysl
jen	jen	k9	jen
pro	pro	k7c4	pro
bohaté	bohatý	k2eAgMnPc4d1	bohatý
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
na	na	k7c4	na
tisíc	tisíc	k4xCgInSc4	tisíc
kusů	kus	k1gInPc2	kus
modelu	model	k1gInSc2	model
T.	T.	kA	T.
Aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
uspokojit	uspokojit	k5eAaPmF	uspokojit
poptávku	poptávka	k1gFnSc4	poptávka
po	po	k7c6	po
levných	levný	k2eAgInPc6d1	levný
autech	aut	k1gInPc6	aut
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
otevřít	otevřít	k5eAaPmF	otevřít
novou	nový	k2eAgFnSc4d1	nová
továrnu	továrna	k1gFnSc4	továrna
Highland	Highlanda	k1gFnPc2	Highlanda
Park	park	k1gInSc4	park
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
technologie	technologie	k1gFnPc1	technologie
pásové	pásový	k2eAgFnSc2d1	pásová
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
sami	sám	k3xTgMnPc1	sám
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
Ford	ford	k1gInSc4	ford
Motor	motor	k1gInSc1	motor
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
nechali	nechat	k5eAaPmAgMnP	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
pásovým	pásový	k2eAgNnSc7d1	pásové
zařízením	zařízení	k1gNnSc7	zařízení
na	na	k7c6	na
jatkách	jatka	k1gFnPc6	jatka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
byla	být	k5eAaImAgFnS	být
instalace	instalace	k1gFnSc1	instalace
pohyblivého	pohyblivý	k2eAgInSc2d1	pohyblivý
pásu	pás	k1gInSc2	pás
dokončena	dokončit	k5eAaPmNgFnS	dokončit
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
až	až	k9	až
250	[number]	k4	250
tisíc	tisíc	k4xCgInPc2	tisíc
kusů	kus	k1gInPc2	kus
aut	auto	k1gNnPc2	auto
<g/>
.	.	kIx.	.
</s>
<s>
Ford	ford	k1gInSc1	ford
pak	pak	k6eAd1	pak
nařídil	nařídit	k5eAaPmAgInS	nařídit
zvýšit	zvýšit	k5eAaPmF	zvýšit
všem	všecek	k3xTgMnPc3	všecek
dělníkům	dělník	k1gMnPc3	dělník
denní	denní	k2eAgFnSc4d1	denní
mzdu	mzda	k1gFnSc4	mzda
o	o	k7c4	o
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
dvojnásobek	dvojnásobek	k1gInSc1	dvojnásobek
na	na	k7c4	na
5	[number]	k4	5
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
krokem	krok	k1gInSc7	krok
si	se	k3xPyFc3	se
chtěl	chtít	k5eAaImAgMnS	chtít
také	také	k9	také
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podnik	podnik	k1gInSc1	podnik
měl	mít	k5eAaImAgInS	mít
stálé	stálý	k2eAgMnPc4d1	stálý
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
namísto	namísto	k7c2	namísto
nádeníků	nádeník	k1gMnPc2	nádeník
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
zavedl	zavést	k5eAaPmAgMnS	zavést
ještě	ještě	k6eAd1	ještě
svůj	svůj	k3xOyFgInSc4	svůj
sociální	sociální	k2eAgInSc4d1	sociální
program	program	k1gInSc4	program
pro	pro	k7c4	pro
stálé	stálý	k2eAgMnPc4d1	stálý
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgNnSc2	který
patřila	patřit	k5eAaImAgFnS	patřit
podniková	podnikový	k2eAgFnSc1d1	podniková
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
péče	péče	k1gFnSc1	péče
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgNnPc1d1	sportovní
a	a	k8xC	a
kulturní	kulturní	k2eAgNnPc1d1	kulturní
vyžití	vyžití	k1gNnPc1	vyžití
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
speciální	speciální	k2eAgFnPc4d1	speciální
prémie	prémie	k1gFnPc4	prémie
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
benefity	benefit	k1gInPc4	benefit
ovšem	ovšem	k9	ovšem
mohli	moct	k5eAaImAgMnP	moct
mít	mít	k5eAaImF	mít
pouze	pouze	k6eAd1	pouze
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
žijící	žijící	k2eAgFnSc4d1	žijící
počestným	počestný	k2eAgInSc7d1	počestný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
podniku	podnik	k1gInSc6	podnik
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
ještě	ještě	k6eAd1	ještě
sociologické	sociologický	k2eAgNnSc1d1	sociologické
oddělení	oddělení	k1gNnSc1	oddělení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
dohlížet	dohlížet	k5eAaImF	dohlížet
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
dělníci	dělník	k1gMnPc1	dělník
nejsou	být	k5eNaImIp3nP	být
závislí	závislý	k2eAgMnPc1d1	závislý
na	na	k7c6	na
alkoholu	alkohol	k1gInSc6	alkohol
či	či	k8xC	či
na	na	k7c6	na
hazardu	hazard	k1gInSc6	hazard
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
docházelo	docházet	k5eAaImAgNnS	docházet
díky	díky	k7c3	díky
zdokonalování	zdokonalování	k1gNnSc3	zdokonalování
technologií	technologie	k1gFnPc2	technologie
pásové	pásový	k2eAgFnSc2d1	pásová
výroby	výroba	k1gFnSc2	výroba
k	k	k7c3	k
ještě	ještě	k9	ještě
většímu	veliký	k2eAgNnSc3d2	veliký
zlevňování	zlevňování	k1gNnSc3	zlevňování
modelu	model	k1gInSc2	model
T.	T.	kA	T.
Tím	ten	k3xDgNnSc7	ten
automobilka	automobilka	k1gFnSc1	automobilka
Ford	ford	k1gInSc1	ford
Motor	motor	k1gInSc1	motor
Company	Compana	k1gFnSc2	Compana
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
až	až	k9	až
50	[number]	k4	50
%	%	kIx~	%
amerického	americký	k2eAgInSc2d1	americký
trhu	trh	k1gInSc2	trh
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
15	[number]	k4	15
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
model	model	k1gInSc1	model
T.	T.	kA	T.
Tento	tento	k3xDgInSc4	tento
rekord	rekord	k1gInSc4	rekord
si	se	k3xPyFc3	se
podržel	podržet	k5eAaPmAgMnS	podržet
ještě	ještě	k9	ještě
na	na	k7c4	na
dalších	další	k2eAgNnPc2d1	další
45	[number]	k4	45
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
postupně	postupně	k6eAd1	postupně
odkoupil	odkoupit	k5eAaPmAgInS	odkoupit
akcie	akcie	k1gFnPc4	akcie
své	svůj	k3xOyFgFnSc2	svůj
společnosti	společnost	k1gFnSc2	společnost
od	od	k7c2	od
všech	všecek	k3xTgMnPc2	všecek
akcionářů	akcionář	k1gMnPc2	akcionář
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jediným	jediný	k2eAgMnSc7d1	jediný
vlastníkem	vlastník	k1gMnSc7	vlastník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
výroby	výroba	k1gFnSc2	výroba
modelu	model	k1gInSc2	model
T	T	kA	T
byla	být	k5eAaImAgFnS	být
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
<g/>
,	,	kIx,	,
než	než	k8xS	než
vývojáři	vývojář	k1gMnPc1	vývojář
vedení	vedení	k1gNnSc2	vedení
jeho	jeho	k3xOp3gMnSc7	jeho
synem	syn	k1gMnSc7	syn
Edselem	Edsel	k1gMnSc7	Edsel
dokončili	dokončit	k5eAaPmAgMnP	dokončit
design	design	k1gInSc4	design
dalšího	další	k2eAgInSc2d1	další
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
modelu	model	k1gInSc2	model
A.	A.	kA	A.
Ford	ford	k1gInSc1	ford
se	se	k3xPyFc4	se
také	také	k9	také
zajímal	zajímat	k5eAaImAgMnS	zajímat
o	o	k7c4	o
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
německými	německý	k2eAgMnPc7d1	německý
nacisty	nacista	k1gMnPc7	nacista
a	a	k8xC	a
hlásil	hlásit	k5eAaImAgMnS	hlásit
se	se	k3xPyFc4	se
k	k	k7c3	k
antisemitismu	antisemitismus	k1gInSc3	antisemitismus
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
novinách	novina	k1gFnPc6	novina
Dearborn	Dearborn	k1gMnSc1	Dearborn
Independent	independent	k1gMnSc1	independent
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
sepsal	sepsat	k5eAaPmAgMnS	sepsat
knihu	kniha	k1gFnSc4	kniha
Mezinárodní	mezinárodní	k2eAgMnSc1d1	mezinárodní
Žid	Žid	k1gMnSc1	Žid
<g/>
:	:	kIx,	:
Světový	světový	k2eAgInSc1d1	světový
problém	problém	k1gInSc1	problém
a	a	k8xC	a
financováním	financování	k1gNnSc7	financování
pomohl	pomoct	k5eAaPmAgMnS	pomoct
šířit	šířit	k5eAaImF	šířit
Protokoly	protokol	k1gInPc4	protokol
sionských	sionský	k2eAgMnPc2d1	sionský
mudrců	mudrc	k1gMnPc2	mudrc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
založil	založit	k5eAaPmAgInS	založit
pobočku	pobočka	k1gFnSc4	pobočka
Ford	ford	k1gInSc1	ford
Werke	Werke	k1gNnSc2	Werke
AB	AB	kA	AB
a	a	k8xC	a
dodával	dodávat	k5eAaImAgInS	dodávat
německé	německý	k2eAgFnSc3d1	německá
armádě	armáda	k1gFnSc3	armáda
nákladní	nákladní	k2eAgInPc4d1	nákladní
vozy	vůz	k1gInPc4	vůz
a	a	k8xC	a
civilistům	civilista	k1gMnPc3	civilista
upravenou	upravený	k2eAgFnSc4d1	upravená
verzi	verze	k1gFnSc4	verze
modelu	model	k1gInSc2	model
T.	T.	kA	T.
Hitlerovi	Hitler	k1gMnSc3	Hitler
posílal	posílat	k5eAaImAgInS	posílat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
k	k	k7c3	k
narozeninám	narozeniny	k1gFnPc3	narozeniny
50	[number]	k4	50
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
veškerý	veškerý	k3xTgInSc1	veškerý
výtěžek	výtěžek	k1gInSc1	výtěžek
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
automobilů	automobil	k1gInPc2	automobil
značky	značka	k1gFnSc2	značka
Ford	ford	k1gInSc1	ford
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
věnoval	věnovat	k5eAaPmAgMnS	věnovat
NSDAP	NSDAP	kA	NSDAP
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Henry	henry	k1gInSc1	henry
Ford	ford	k1gInSc1	ford
nechtěl	chtít	k5eNaImAgInS	chtít
zapojit	zapojit	k5eAaPmF	zapojit
do	do	k7c2	do
válečné	válečný	k2eAgFnSc2d1	válečná
mašinérie	mašinérie	k1gFnSc2	mašinérie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
Rooseveltovou	Rooseveltův	k2eAgFnSc7d1	Rooseveltova
politikou	politika	k1gFnSc7	politika
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
USA	USA	kA	USA
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
Ford	ford	k1gInSc1	ford
Motor	motor	k1gInSc1	motor
Company	Compana	k1gFnSc2	Compana
plně	plně	k6eAd1	plně
zapojila	zapojit	k5eAaPmAgFnS	zapojit
do	do	k7c2	do
válečného	válečný	k2eAgNnSc2d1	válečné
úsilí	úsilí	k1gNnSc2	úsilí
<g/>
.	.	kIx.	.
</s>
<s>
Podnik	podnik	k1gInSc1	podnik
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
vedl	vést	k5eAaImAgMnS	vést
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Edsel	Edsel	k1gMnSc1	Edsel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Henry	henry	k1gInSc1	henry
Ford	ford	k1gInSc1	ford
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
neměl	mít	k5eNaImAgMnS	mít
dost	dost	k6eAd1	dost
mentálních	mentální	k2eAgFnPc2d1	mentální
a	a	k8xC	a
fyzických	fyzický	k2eAgFnPc2d1	fyzická
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
faktické	faktický	k2eAgNnSc4d1	faktické
vedení	vedení	k1gNnSc4	vedení
podniku	podnik	k1gInSc2	podnik
převzala	převzít	k5eAaPmAgFnS	převzít
skupina	skupina	k1gFnSc1	skupina
vrcholových	vrcholový	k2eAgMnPc2d1	vrcholový
manažerů	manažer	k1gMnPc2	manažer
a	a	k8xC	a
inženýrů	inženýr	k1gMnPc2	inženýr
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Charlesem	Charles	k1gMnSc7	Charles
Sorensenem	Sorensen	k1gMnSc7	Sorensen
a	a	k8xC	a
Harry	Harra	k1gFnSc2	Harra
Benettem	Benetto	k1gNnSc7	Benetto
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
zahájila	zahájit	k5eAaPmAgFnS	zahájit
Fordova	Fordův	k2eAgFnSc1d1	Fordova
letecká	letecký	k2eAgFnSc1d1	letecká
továrna	továrna	k1gFnSc1	továrna
ve	v	k7c6	v
Willow	Willow	k1gFnPc6	Willow
Run	Runa	k1gFnPc2	Runa
výrobu	výrob	k1gInSc2	výrob
bombardérů	bombardér	k1gMnPc2	bombardér
B-24	B-24	k1gMnSc1	B-24
Liberator	Liberator	k1gMnSc1	Liberator
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
již	již	k6eAd1	již
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
650	[number]	k4	650
bombardérů	bombardér	k1gInPc2	bombardér
B-24	B-24	k1gFnPc2	B-24
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Fordova	Fordův	k2eAgFnSc1d1	Fordova
rodina	rodina	k1gFnSc1	rodina
chtěla	chtít	k5eAaImAgFnS	chtít
dostat	dostat	k5eAaPmF	dostat
podnik	podnik	k1gInSc4	podnik
znovu	znovu	k6eAd1	znovu
pod	pod	k7c4	pod
svou	svůj	k3xOyFgFnSc4	svůj
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
,	,	kIx,	,
Sorensen	Sorensen	k1gInSc1	Sorensen
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
podniku	podnik	k1gInSc2	podnik
vypuzen	vypuzen	k2eAgInSc4d1	vypuzen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
Benett	Benett	k2eAgInSc1d1	Benett
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Ford	ford	k1gInSc1	ford
podnik	podnik	k1gInSc1	podnik
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
předal	předat	k5eAaPmAgMnS	předat
svému	svůj	k3xOyFgMnSc3	svůj
vnukovi	vnuk	k1gMnSc3	vnuk
Henrymu	Henry	k1gMnSc3	Henry
Fordovi	Forda	k1gMnSc3	Forda
mladšímu	mladý	k2eAgMnSc3d2	mladší
<g/>
.	.	kIx.	.
</s>
<s>
Podnik	podnik	k1gInSc1	podnik
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
pokraj	pokraj	k1gInSc4	pokraj
bankrotu	bankrot	k1gInSc2	bankrot
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nakonec	nakonec	k6eAd1	nakonec
nezbankrotoval	zbankrotovat	k5eNaPmAgMnS	zbankrotovat
<g/>
.	.	kIx.	.
</s>
<s>
Henry	Henry	k1gMnSc1	Henry
Ford	ford	k1gInSc4	ford
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
mozkovou	mozkový	k2eAgFnSc4d1	mozková
příhodu	příhoda	k1gFnSc4	příhoda
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
úspěch	úspěch	k1gInSc1	úspěch
znamenal	znamenat	k5eAaImAgInS	znamenat
nejen	nejen	k6eAd1	nejen
revoluci	revoluce	k1gFnSc4	revoluce
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
moderní	moderní	k2eAgFnSc4d1	moderní
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
teoretici	teoretik	k1gMnPc1	teoretik
nazývají	nazývat	k5eAaImIp3nP	nazývat
toto	tento	k3xDgNnSc4	tento
období	období	k1gNnSc4	období
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
historie	historie	k1gFnSc2	historie
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Fordismus	fordismus	k1gInSc1	fordismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
na	na	k7c6	na
světě	svět	k1gInSc6	svět
také	také	k9	také
zavedl	zavést	k5eAaPmAgMnS	zavést
pásovou	pásový	k2eAgFnSc4d1	pásová
výrobu	výroba	k1gFnSc4	výroba
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Henry	henry	k1gInSc1	henry
Ford	ford	k1gInSc1	ford
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
marketing	marketing	k1gInSc4	marketing
a	a	k8xC	a
management	management	k1gInSc4	management
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgFnSc2d1	krátká
doby	doba	k1gFnSc2	doba
si	se	k3xPyFc3	se
dokázal	dokázat	k5eAaPmAgMnS	dokázat
naklonit	naklonit	k5eAaPmF	naklonit
trh	trh	k1gInSc4	trh
s	s	k7c7	s
automobily	automobil	k1gInPc7	automobil
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
a	a	k8xC	a
vydělal	vydělat	k5eAaPmAgInS	vydělat
několik	několik	k4yIc4	několik
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Uvědomoval	uvědomovat	k5eAaImAgInS	uvědomovat
si	se	k3xPyFc3	se
sílu	síla	k1gFnSc4	síla
reklamy	reklama	k1gFnSc2	reklama
a	a	k8xC	a
propagace	propagace	k1gFnSc2	propagace
<g/>
.	.	kIx.	.
</s>
<s>
Každého	každý	k3xTgMnSc4	každý
svého	svůj	k1gMnSc4	svůj
pracovníka	pracovník	k1gMnSc4	pracovník
dokázal	dokázat	k5eAaPmAgMnS	dokázat
nadchnout	nadchnout	k5eAaPmF	nadchnout
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
vizi	vize	k1gFnSc4	vize
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
pracovníci	pracovník	k1gMnPc1	pracovník
stávali	stávat	k5eAaImAgMnP	stávat
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
"	"	kIx"	"
<g/>
motorizovali	motorizovat	k5eAaBmAgMnP	motorizovat
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Henry	henry	k1gInSc2	henry
Ford	ford	k1gInSc1	ford
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Henry	henry	k1gInSc1	henry
Ford	ford	k1gInSc1	ford
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Ford	ford	k1gInSc1	ford
Motor	motor	k1gInSc1	motor
Company	Compana	k1gFnSc2	Compana
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Ford	ford	k1gInSc1	ford
Motor	motor	k1gInSc1	motor
Company	Compana	k1gFnSc2	Compana
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
