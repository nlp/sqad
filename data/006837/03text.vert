<s>
Mikroregion	mikroregion	k1gInSc1	mikroregion
Tábor	Tábor	k1gInSc1	Tábor
je	být	k5eAaImIp3nS	být
dobrovolný	dobrovolný	k2eAgInSc1d1	dobrovolný
svazek	svazek	k1gInSc1	svazek
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
okresu	okres	k1gInSc6	okres
Jičín	Jičín	k1gInSc1	Jičín
a	a	k8xC	a
okresu	okres	k1gInSc3	okres
Semily	Semily	k1gInPc4	Semily
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
sídlem	sídlo	k1gNnSc7	sídlo
je	být	k5eAaImIp3nS	být
Lomnice	Lomnice	k1gFnSc1	Lomnice
nad	nad	k7c7	nad
Popelkou	Popelka	k1gFnSc7	Popelka
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
spolupráce	spolupráce	k1gFnSc1	spolupráce
a	a	k8xC	a
koordinace	koordinace	k1gFnSc1	koordinace
činností	činnost	k1gFnPc2	činnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rozvoje	rozvoj	k1gInSc2	rozvoj
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
celkem	celkem	k6eAd1	celkem
11	[number]	k4	11
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Lomnice	Lomnice	k1gFnSc1	Lomnice
nad	nad	k7c7	nad
Popelkou	Popelka	k1gFnSc7	Popelka
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
nad	nad	k7c7	nad
Popelkou	Popelka	k1gFnSc7	Popelka
Bradlecká	Bradlecký	k2eAgFnSc1d1	Bradlecká
Lhota	Lhota	k1gFnSc1	Lhota
Syřenov	Syřenovo	k1gNnPc2	Syřenovo
Kyje	kyj	k1gInSc2	kyj
Železnice	železnice	k1gFnSc2	železnice
Holenice	holenice	k1gFnSc2	holenice
Rovensko	Rovensko	k1gNnSc4	Rovensko
pod	pod	k7c7	pod
Troskami	troska	k1gFnPc7	troska
Tatobity	Tatobita	k1gMnSc2	Tatobita
Veselá	Veselá	k1gFnSc1	Veselá
Žernov	žernov	k1gInSc1	žernov
Mikroregion	mikroregion	k1gInSc1	mikroregion
Tábor	Tábor	k1gInSc1	Tábor
na	na	k7c6	na
Regionálním	regionální	k2eAgInSc6d1	regionální
informačním	informační	k2eAgInSc6d1	informační
servisu	servis	k1gInSc6	servis
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
</s>
