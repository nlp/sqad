<s>
Mikroregion	mikroregion	k1gInSc1	mikroregion
Tábor	Tábor	k1gInSc1	Tábor
je	být	k5eAaImIp3nS	být
dobrovolný	dobrovolný	k2eAgInSc1d1	dobrovolný
svazek	svazek	k1gInSc1	svazek
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
okresu	okres	k1gInSc6	okres
Jičín	Jičín	k1gInSc1	Jičín
a	a	k8xC	a
okresu	okres	k1gInSc3	okres
Semily	Semily	k1gInPc4	Semily
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
sídlem	sídlo	k1gNnSc7	sídlo
je	být	k5eAaImIp3nS	být
Lomnice	Lomnice	k1gFnSc1	Lomnice
nad	nad	k7c7	nad
Popelkou	Popelka	k1gFnSc7	Popelka
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
spolupráce	spolupráce	k1gFnSc1	spolupráce
a	a	k8xC	a
koordinace	koordinace	k1gFnSc1	koordinace
činností	činnost	k1gFnPc2	činnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rozvoje	rozvoj	k1gInSc2	rozvoj
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
