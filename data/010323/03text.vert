<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Složil	Složil	k1gMnSc1	Složil
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1955	[number]	k4	1955
Opava	Opava	k1gFnSc1	Opava
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
československý	československý	k2eAgMnSc1d1	československý
profesionální	profesionální	k2eAgMnSc1d1	profesionální
tenista	tenista	k1gMnSc1	tenista
a	a	k8xC	a
tenisový	tenisový	k2eAgMnSc1d1	tenisový
trenér	trenér	k1gMnSc1	trenér
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
svých	svůj	k3xOyFgInPc2	svůj
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
sportovních	sportovní	k2eAgInPc2d1	sportovní
úspěchů	úspěch	k1gInPc2	úspěch
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Profesionální	profesionální	k2eAgFnSc1d1	profesionální
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
klubové	klubový	k2eAgFnSc6d1	klubová
úrovni	úroveň	k1gFnSc6	úroveň
byl	být	k5eAaImAgMnS	být
hráčem	hráč	k1gMnSc7	hráč
Slavie	slavie	k1gFnSc2	slavie
Praha	Praha	k1gFnSc1	Praha
IPS	IPS	kA	IPS
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
reprezentant	reprezentant	k1gMnSc1	reprezentant
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1975	[number]	k4	1975
společně	společně	k6eAd1	společně
s	s	k7c7	s
Tomášem	Tomáš	k1gMnSc7	Tomáš
Šmídem	Šmíd	k1gMnSc7	Šmíd
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
dvojnásobný	dvojnásobný	k2eAgInSc4d1	dvojnásobný
zisk	zisk	k1gInSc4	zisk
ČSSR	ČSSR	kA	ČSSR
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
Galeova	Galeův	k2eAgInSc2d1	Galeův
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
vítězného	vítězný	k2eAgInSc2d1	vítězný
týmu	tým	k1gInSc2	tým
v	v	k7c6	v
Davis	Davis	k1gFnSc6	Davis
Cupu	cup	k1gInSc2	cup
<g/>
,	,	kIx,	,
ve	v	k7c6	v
finálovém	finálový	k2eAgInSc6d1	finálový
zápase	zápas	k1gInSc6	zápas
se	se	k3xPyFc4	se
na	na	k7c4	na
kurt	kurt	k1gInSc4	kurt
nedostal	dostat	k5eNaPmAgMnS	dostat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
na	na	k7c4	na
French	French	k1gInSc4	French
Open	Opena	k1gFnPc2	Opena
ve	v	k7c6	v
smíšené	smíšený	k2eAgFnSc6d1	smíšená
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Renátou	Renáta	k1gFnSc7	Renáta
Tomanovou	Tomanová	k1gFnSc7	Tomanová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
se	se	k3xPyFc4	se
probojoval	probojovat	k5eAaPmAgMnS	probojovat
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
turnaje	turnaj	k1gInSc2	turnaj
ATP	atp	kA	atp
v	v	k7c6	v
Kitzbühlu	Kitzbühl	k1gInSc6	Kitzbühl
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Nancy	Nancy	k1gFnSc6	Nancy
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
v	v	k7c6	v
Kitzbühlu	Kitzbühl	k1gInSc6	Kitzbühl
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
úspěchy	úspěch	k1gInPc4	úspěch
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
dokázal	dokázat	k5eAaPmAgMnS	dokázat
zvítězit	zvítězit	k5eAaPmF	zvítězit
na	na	k7c6	na
31	[number]	k4	31
turnajích	turnaj	k1gInPc6	turnaj
ATP	atp	kA	atp
(	(	kIx(	(
<g/>
čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
30	[number]	k4	30
turnajích	turnaj	k1gInPc6	turnaj
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
partnerem	partner	k1gMnSc7	partner
až	až	k9	až
ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
finálové	finálový	k2eAgFnSc2d1	finálová
účasti	účast	k1gFnSc2	účast
na	na	k7c4	na
French	French	k1gInSc4	French
Open	Opena	k1gFnPc2	Opena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
ATP	atp	kA	atp
pro	pro	k7c4	pro
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
byl	být	k5eAaImAgInS	být
nejlépe	dobře	k6eAd3	dobře
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
na	na	k7c4	na
35	[number]	k4	35
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1978	[number]	k4	1978
<g/>
–	–	k?	–
<g/>
1986	[number]	k4	1986
byl	být	k5eAaImAgInS	být
také	také	k9	také
členem	člen	k1gMnSc7	člen
daviscupového	daviscupový	k2eAgInSc2d1	daviscupový
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
s	s	k7c7	s
bilancí	bilance	k1gFnSc7	bilance
zápasů	zápas	k1gInPc2	zápas
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
vítězství-prohry	vítězstvírohra	k1gFnSc2	vítězství-prohra
<g/>
)	)	kIx)	)
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
a	a	k8xC	a
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
československého	československý	k2eAgInSc2d1	československý
vítězného	vítězný	k2eAgInSc2d1	vítězný
týmu	tým	k1gInSc2	tým
Davis	Davis	k1gFnSc2	Davis
Cupu	cup	k1gInSc2	cup
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Trenérská	trenérský	k2eAgFnSc1d1	trenérská
činnost	činnost	k1gFnSc1	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
aktivní	aktivní	k2eAgFnSc2d1	aktivní
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
trénování	trénování	k1gNnSc4	trénování
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gNnSc4	jeho
svěřenkyně	svěřenkyně	k1gFnPc1	svěřenkyně
patřily	patřit	k5eAaImAgFnP	patřit
např.	např.	kA	např.
Steffi	Steffi	k1gFnSc1	Steffi
Grafová	Grafová	k1gFnSc1	Grafová
<g/>
,	,	kIx,	,
Jennifer	Jennifra	k1gFnPc2	Jennifra
Capriatiová	Capriatiový	k2eAgFnSc1d1	Capriatiová
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
Malejevová	Malejevová	k1gFnSc1	Malejevová
nebo	nebo	k8xC	nebo
Anna	Anna	k1gFnSc1	Anna
Kurnikovová	Kurnikovová	k1gFnSc1	Kurnikovová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
je	být	k5eAaImIp3nS	být
tenisovým	tenisový	k2eAgMnSc7d1	tenisový
ředitelem	ředitel	k1gMnSc7	ředitel
v	v	k7c6	v
soukromém	soukromý	k2eAgInSc6d1	soukromý
World	World	k1gInSc1	World
Tennis	Tennis	k1gFnPc2	Tennis
Clubu	club	k1gInSc2	club
ve	v	k7c6	v
floridském	floridský	k2eAgInSc6d1	floridský
Naples	Naples	k1gInSc1	Naples
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vítězství	vítězství	k1gNnSc1	vítězství
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
ATP	atp	kA	atp
(	(	kIx(	(
<g/>
čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
Pavla	Pavel	k1gMnSc2	Pavel
Složila	Složil	k1gMnSc2	Složil
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
ATP	atp	kA	atp
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
Pavla	Pavel	k1gMnSc2	Pavel
Složila	Složil	k1gMnSc2	Složil
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
tenisové	tenisový	k2eAgFnSc2d1	tenisová
federace	federace	k1gFnSc2	federace
</s>
</p>
