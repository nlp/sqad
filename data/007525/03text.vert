<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
kapucínská	kapucínský	k2eAgFnSc1d1	Kapucínská
hrobka	hrobka	k1gFnSc1	hrobka
je	být	k5eAaImIp3nS	být
památkově	památkově	k6eAd1	památkově
chráněnou	chráněný	k2eAgFnSc7d1	chráněná
hrobkou	hrobka	k1gFnSc7	hrobka
nacházející	nacházející	k2eAgFnSc7d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
suterénu	suterén	k1gInSc6	suterén
kapucínského	kapucínský	k2eAgInSc2d1	kapucínský
kostela	kostel	k1gInSc2	kostel
Nalezení	nalezení	k1gNnSc4	nalezení
svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
na	na	k7c6	na
Kapucínském	kapucínský	k2eAgNnSc6d1	Kapucínské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
hrobce	hrobka	k1gFnSc6	hrobka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
kronice	kronika	k1gFnSc6	kronika
brněnského	brněnský	k2eAgInSc2d1	brněnský
kapucínského	kapucínský	k2eAgInSc2d1	kapucínský
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
dostavbě	dostavba	k1gFnSc6	dostavba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1656	[number]	k4	1656
přenesli	přenést	k5eAaPmAgMnP	přenést
kapucíni	kapucín	k1gMnPc1	kapucín
ostatky	ostatek	k1gInPc1	ostatek
svých	svůj	k3xOyFgMnPc2	svůj
bratří	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
původně	původně	k6eAd1	původně
pohřbeni	pohřbít	k5eAaPmNgMnP	pohřbít
v	v	k7c6	v
kryptě	krypta	k1gFnSc6	krypta
prvního	první	k4xOgInSc2	první
kláštera	klášter	k1gInSc2	klášter
za	za	k7c7	za
hradbami	hradba	k1gFnPc7	hradba
<g/>
,	,	kIx,	,
do	do	k7c2	do
hrobky	hrobka	k1gFnSc2	hrobka
pod	pod	k7c7	pod
novým	nový	k2eAgInSc7d1	nový
kostelem	kostel	k1gInSc7	kostel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1726	[number]	k4	1726
byla	být	k5eAaImAgFnS	být
krypta	krypta	k1gFnSc1	krypta
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
sály	sál	k1gInPc4	sál
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
vedl	vést	k5eAaImAgMnS	vést
stavitel	stavitel	k1gMnSc1	stavitel
Mořic	Mořic	k1gMnSc1	Mořic
Grimm	Grimm	k1gMnSc1	Grimm
<g/>
.	.	kIx.	.
</s>
<s>
Prostory	prostor	k1gInPc1	prostor
hrobky	hrobka	k1gFnSc2	hrobka
byly	být	k5eAaImAgInP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zbudované	zbudovaný	k2eAgInPc1d1	zbudovaný
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
sklepních	sklepní	k2eAgFnPc2d1	sklepní
místností	místnost	k1gFnPc2	místnost
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
stávaly	stávat	k5eAaImAgFnP	stávat
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
než	než	k8xS	než
byly	být	k5eAaImAgFnP	být
vykoupeny	vykoupit	k5eAaPmNgFnP	vykoupit
a	a	k8xC	a
zbořeny	zbořit	k5eAaPmNgFnP	zbořit
kvůli	kvůli	k7c3	kvůli
stavbě	stavba	k1gFnSc3	stavba
kapucínského	kapucínský	k2eAgInSc2d1	kapucínský
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
tomu	ten	k3xDgMnSc3	ten
nestejná	stejný	k2eNgFnSc1d1	nestejná
výška	výška	k1gFnSc1	výška
podlah	podlaha	k1gFnPc2	podlaha
i	i	k8xC	i
typů	typ	k1gInPc2	typ
kleneb	klenba	k1gFnPc2	klenba
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
částech	část	k1gFnPc6	část
krypty	krypta	k1gFnSc2	krypta
<g/>
.	.	kIx.	.
</s>
<s>
Vhodná	vhodný	k2eAgFnSc1d1	vhodná
geologická	geologický	k2eAgFnSc1d1	geologická
skladba	skladba	k1gFnSc1	skladba
půdy	půda	k1gFnSc2	půda
v	v	k7c6	v
podloží	podloží	k1gNnSc6	podloží
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
důmyslný	důmyslný	k2eAgInSc1d1	důmyslný
systém	systém	k1gInSc1	systém
vzduchových	vzduchový	k2eAgInPc2d1	vzduchový
průduchů	průduch	k1gInPc2	průduch
umožnily	umožnit	k5eAaPmAgInP	umožnit
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
mumifikaci	mumifikace	k1gFnSc4	mumifikace
těl	tělo	k1gNnPc2	tělo
zemřelých	zemřelý	k1gMnPc2	zemřelý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapucínské	kapucínský	k2eAgFnSc6d1	Kapucínská
hrobce	hrobka	k1gFnSc6	hrobka
bylo	být	k5eAaImAgNnS	být
celkem	celkem	k6eAd1	celkem
pohřbeno	pohřbít	k5eAaPmNgNnS	pohřbít
205	[number]	k4	205
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
153	[number]	k4	153
bratří	bratr	k1gMnPc2	bratr
kapucínů	kapucín	k1gMnPc2	kapucín
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
slouží	sloužit	k5eAaImIp3nS	sloužit
podzemí	podzemí	k1gNnSc1	podzemí
kostela	kostel	k1gInSc2	kostel
jako	jako	k8xC	jako
místo	místo	k1gNnSc4	místo
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
pro	pro	k7c4	pro
41	[number]	k4	41
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Těla	tělo	k1gNnPc1	tělo
zemřelých	zemřelý	k1gMnPc2	zemřelý
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnSc2	staletí
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
uložena	uložit	k5eAaPmNgNnP	uložit
ve	v	k7c6	v
zděné	zděný	k2eAgFnSc6d1	zděná
tumbě	tumba	k1gFnSc6	tumba
hrobky	hrobka	k1gFnSc2	hrobka
<g/>
.	.	kIx.	.
</s>
<s>
Hrobka	hrobka	k1gFnSc1	hrobka
sloužila	sloužit	k5eAaImAgFnS	sloužit
svému	svůj	k3xOyFgInSc3	svůj
účelu	účel	k1gInSc3	účel
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1784	[number]	k4	1784
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
císař	císař	k1gMnSc1	císař
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
kvůli	kvůli	k7c3	kvůli
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
šíření	šíření	k1gNnSc2	šíření
epidemií	epidemie	k1gFnPc2	epidemie
zakázal	zakázat	k5eAaPmAgMnS	zakázat
pohřbívání	pohřbívání	k1gNnSc3	pohřbívání
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
šedesáti	šedesát	k4xCc2	šedesát
průduchů	průduch	k1gInPc2	průduch
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgInP	nacházet
ve	v	k7c6	v
zdech	zeď	k1gFnPc6	zeď
krypty	krypta	k1gFnSc2	krypta
a	a	k8xC	a
spojovaly	spojovat	k5eAaImAgInP	spojovat
se	se	k3xPyFc4	se
do	do	k7c2	do
několika	několik	k4yIc2	několik
komínů	komín	k1gInPc2	komín
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
vyúsťovaly	vyúsťovat	k5eAaImAgInP	vyúsťovat
pod	pod	k7c7	pod
střechou	střecha	k1gFnSc7	střecha
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
jich	on	k3xPp3gInPc2	on
dodnes	dodnes	k6eAd1	dodnes
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
funkčních	funkční	k2eAgFnPc2d1	funkční
jen	jen	k8xS	jen
několik	několik	k4yIc4	několik
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
byla	být	k5eAaImAgFnS	být
zazděna	zazdít	k5eAaPmNgFnS	zazdít
zřejmě	zřejmě	k6eAd1	zřejmě
už	už	k6eAd1	už
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
byla	být	k5eAaImAgFnS	být
kapucínská	kapucínský	k2eAgFnSc1d1	Kapucínská
hrobka	hrobka	k1gFnSc1	hrobka
otevřena	otevřen	k2eAgFnSc1d1	otevřena
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
se	se	k3xPyFc4	se
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
nejvíce	hodně	k6eAd3	hodně
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
kapucín	kapucín	k1gMnSc1	kapucín
Zeno	Zeno	k1gMnSc1	Zeno
Diviš	Diviš	k1gMnSc1	Diviš
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
kustod	kustod	k1gMnSc1	kustod
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
sepsal	sepsat	k5eAaPmAgMnS	sepsat
i	i	k9	i
malého	malý	k1gMnSc4	malý
průvodce	průvodce	k1gMnSc4	průvodce
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
hrobky	hrobka	k1gFnSc2	hrobka
prošel	projít	k5eAaPmAgInS	projít
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
řadou	řada	k1gFnSc7	řada
úprav	úprava	k1gFnPc2	úprava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
stále	stále	k6eAd1	stále
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
instalována	instalovat	k5eAaBmNgFnS	instalovat
výstava	výstava	k1gFnSc1	výstava
barokních	barokní	k2eAgFnPc2d1	barokní
fresek	freska	k1gFnPc2	freska
Ars	Ars	k1gMnPc2	Ars
Moriendi	Moriend	k1gMnPc1	Moriend
z	z	k7c2	z
krypty	krypta	k1gFnSc2	krypta
loretánského	loretánský	k2eAgInSc2d1	loretánský
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Hradčanech	Hradčany	k1gInPc6	Hradčany
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
kompletně	kompletně	k6eAd1	kompletně
zrekonstruovaná	zrekonstruovaný	k2eAgFnSc1d1	zrekonstruovaná
kaple	kaple	k1gFnSc1	kaple
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
je	být	k5eAaImIp3nS	být
pohřbený	pohřbený	k2eAgMnSc1d1	pohřbený
František	František	k1gMnSc1	František
baron	baron	k1gMnSc1	baron
Trenck	Trenck	k1gMnSc1	Trenck
<g/>
.	.	kIx.	.
</s>
<s>
Kapucíni	kapucín	k1gMnPc1	kapucín
měli	mít	k5eAaImAgMnP	mít
velmi	velmi	k6eAd1	velmi
prostý	prostý	k2eAgInSc4d1	prostý
způsob	způsob	k1gInSc4	způsob
pohřbívání	pohřbívání	k1gNnSc2	pohřbívání
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
odrážel	odrážet	k5eAaImAgMnS	odrážet
jejich	jejich	k3xOp3gNnSc4	jejich
základní	základní	k2eAgNnSc4d1	základní
charisma	charisma	k1gNnSc4	charisma
<g/>
:	:	kIx,	:
chudobu	chudoba	k1gFnSc4	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Zemřelého	zemřelý	k1gMnSc4	zemřelý
bratra	bratr	k1gMnSc4	bratr
uložili	uložit	k5eAaPmAgMnP	uložit
do	do	k7c2	do
dubové	dubový	k2eAgFnSc2d1	dubová
rakve	rakev	k1gFnSc2	rakev
s	s	k7c7	s
vysunovacím	vysunovací	k2eAgInSc7d1	vysunovací
dnem	den	k1gInSc7	den
a	a	k8xC	a
po	po	k7c6	po
obřadech	obřad	k1gInPc6	obřad
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
ho	on	k3xPp3gNnSc4	on
po	po	k7c6	po
schodech	schod	k1gInPc6	schod
snesli	snést	k5eAaPmAgMnP	snést
do	do	k7c2	do
hrobky	hrobka	k1gFnSc2	hrobka
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
ho	on	k3xPp3gMnSc4	on
uložili	uložit	k5eAaPmAgMnP	uložit
na	na	k7c4	na
holou	holý	k2eAgFnSc4d1	holá
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
cihlou	cihla	k1gFnSc7	cihla
či	či	k8xC	či
dvěma	dva	k4xCgFnPc7	dva
pod	pod	k7c7	pod
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Rakev	rakev	k1gFnSc1	rakev
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
uschována	uschovat	k5eAaPmNgFnS	uschovat
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
pohřeb	pohřeb	k1gInSc4	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapucínské	kapucínský	k2eAgFnSc6d1	Kapucínská
hrobce	hrobka	k1gFnSc6	hrobka
byli	být	k5eAaImAgMnP	být
pohřbívání	pohřbívání	k1gNnSc4	pohřbívání
také	také	k6eAd1	také
dobrodinci	dobrodinec	k1gMnPc1	dobrodinec
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
přispěli	přispět	k5eAaPmAgMnP	přispět
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
a	a	k8xC	a
chodu	chod	k1gInSc3	chod
brněnského	brněnský	k2eAgInSc2d1	brněnský
kapucínského	kapucínský	k2eAgInSc2d1	kapucínský
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
dobrodincem	dobrodinec	k1gMnSc7	dobrodinec
je	být	k5eAaImIp3nS	být
bezpochyby	bezpochyby	k6eAd1	bezpochyby
slavný	slavný	k2eAgMnSc1d1	slavný
velitel	velitel	k1gMnSc1	velitel
pandurů	pandur	k1gMnPc2	pandur
František	František	k1gMnSc1	František
baron	baron	k1gMnSc1	baron
Trenck	Trenck	k1gMnSc1	Trenck
(	(	kIx(	(
<g/>
†	†	k?	†
1749	[number]	k4	1749
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
brněnský	brněnský	k2eAgInSc4d1	brněnský
klášter	klášter	k1gInSc4	klášter
byl	být	k5eAaImAgInS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
postavou	postava	k1gFnSc7	postava
také	také	k9	také
Jiří	Jiří	k1gMnSc1	Jiří
Barnabáš	Barnabáš	k1gMnSc1	Barnabáš
Orelli	Orelle	k1gFnSc4	Orelle
(	(	kIx(	(
<g/>
†	†	k?	†
1757	[number]	k4	1757
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kominický	kominický	k2eAgMnSc1d1	kominický
cechmistr	cechmistr	k1gMnSc1	cechmistr
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
tzv.	tzv.	kA	tzv.
Parens	Parensa	k1gFnPc2	Parensa
spiritualis	spiritualis	k1gFnPc2	spiritualis
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
důvěryhodný	důvěryhodný	k2eAgMnSc1d1	důvěryhodný
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
bratřím	bratřit	k5eAaImIp1nS	bratřit
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
s	s	k7c7	s
řešením	řešení	k1gNnSc7	řešení
světských	světský	k2eAgFnPc2d1	světská
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
souvisely	souviset	k5eAaImAgFnP	souviset
s	s	k7c7	s
provozem	provoz	k1gInSc7	provoz
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
brněnského	brněnský	k2eAgInSc2d1	brněnský
konventu	konvent	k1gInSc2	konvent
neodmyslitelně	odmyslitelně	k6eNd1	odmyslitelně
patří	patřit	k5eAaImIp3nP	patřit
také	také	k9	také
Grimmovi	Grimmův	k2eAgMnPc1d1	Grimmův
<g/>
,	,	kIx,	,
slavná	slavný	k2eAgFnSc1d1	slavná
rodina	rodina	k1gFnSc1	rodina
stavitelů	stavitel	k1gMnPc2	stavitel
a	a	k8xC	a
architektů	architekt	k1gMnPc2	architekt
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kapucínskou	kapucínský	k2eAgFnSc7d1	Kapucínská
komunitou	komunita	k1gFnSc7	komunita
ji	on	k3xPp3gFnSc4	on
pojily	pojit	k5eAaImAgFnP	pojit
nejen	nejen	k6eAd1	nejen
vztahy	vztah	k1gInPc1	vztah
pracovní	pracovní	k2eAgInPc1d1	pracovní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
přátelské	přátelský	k2eAgInPc1d1	přátelský
<g/>
.	.	kIx.	.
</s>
<s>
Mořic	Mořic	k1gMnSc1	Mořic
Grimm	Grimm	k1gMnSc1	Grimm
(	(	kIx(	(
<g/>
†	†	k?	†
1757	[number]	k4	1757
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
stavitel	stavitel	k1gMnSc1	stavitel
bavorského	bavorský	k2eAgInSc2d1	bavorský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
stavebních	stavební	k2eAgFnPc2d1	stavební
aktivit	aktivita	k1gFnPc2	aktivita
pro	pro	k7c4	pro
kapucíny	kapucín	k1gMnPc4	kapucín
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
jeho	jeho	k3xOp3gNnPc3	jeho
nejznámějším	známý	k2eAgNnPc3d3	nejznámější
dílům	dílo	k1gNnPc3	dílo
práce	práce	k1gFnSc2	práce
pro	pro	k7c4	pro
brněnskou	brněnský	k2eAgFnSc4d1	brněnská
komunitu	komunita	k1gFnSc4	komunita
řádu	řád	k1gInSc2	řád
minoritů	minorita	k1gMnPc2	minorita
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Antonín	Antonín	k1gMnSc1	Antonín
Grimm	Grimm	k1gMnSc1	Grimm
(	(	kIx(	(
<g/>
†	†	k?	†
1784	[number]	k4	1784
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
velkého	velký	k2eAgNnSc2d1	velké
uznání	uznání	k1gNnSc2	uznání
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnPc3	jeho
dílům	dílo	k1gNnPc3	dílo
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
stavba	stavba	k1gFnSc1	stavba
kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Felixe	Felix	k1gMnSc2	Felix
v	v	k7c6	v
kapucínském	kapucínský	k2eAgInSc6d1	kapucínský
kostele	kostel	k1gInSc6	kostel
i	i	k9	i
tzv.	tzv.	kA	tzv.
Trenckova	Trenckův	k2eAgNnSc2d1	Trenckovo
křídla	křídlo	k1gNnSc2	křídlo
s	s	k7c7	s
honosnou	honosný	k2eAgFnSc7d1	honosná
knihovnou	knihovna	k1gFnSc7	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapucínské	kapucínský	k2eAgFnSc6d1	Kapucínská
hrobce	hrobka	k1gFnSc6	hrobka
spočinul	spočinout	k5eAaPmAgMnS	spočinout
také	také	k9	také
Václav	Václav	k1gMnSc1	Václav
Michael	Michael	k1gMnSc1	Michael
Josef	Josef	k1gMnSc1	Josef
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Vrbna	Vrbn	k1gInSc2	Vrbn
a	a	k8xC	a
Bruntálu	Bruntál	k1gInSc2	Bruntál
(	(	kIx(	(
<g/>
†	†	k?	†
1755	[number]	k4	1755
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrchní	vrchní	k2eAgMnSc1d1	vrchní
soudce	soudce	k1gMnSc1	soudce
Markrabství	markrabství	k1gNnSc2	markrabství
moravského	moravský	k2eAgNnSc2d1	Moravské
<g/>
,	,	kIx,	,
i	i	k8xC	i
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Marií	Maria	k1gFnSc7	Maria
Eleonorou	Eleonora	k1gFnSc7	Eleonora
(	(	kIx(	(
<g/>
†	†	k?	†
1761	[number]	k4	1761
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podpora	podpora	k1gFnSc1	podpora
kapucínů	kapucín	k1gMnPc2	kapucín
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
jejich	jejich	k3xOp3gFnSc2	jejich
rodové	rodový	k2eAgFnSc2d1	rodová
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Take	Take	k1gFnSc1	Take
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
pohřbena	pohřben	k2eAgFnSc1d1	pohřbena
Marie	Marie	k1gFnSc1	Marie
Johanna	Johanna	k1gFnSc1	Johanna
Elisabetha	Elisabetha	k1gFnSc1	Elisabetha
Polyxena	Polyxena	k1gFnSc1	Polyxena
hraběnku	hraběnka	k1gFnSc4	hraběnka
z	z	k7c2	z
Waldorffu	Waldorff	k1gInSc2	Waldorff
(	(	kIx(	(
<g/>
†	†	k?	†
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
štědrou	štědrý	k2eAgFnSc7d1	štědrá
dárkyní	dárkyně	k1gFnSc7	dárkyně
nejen	nejen	k6eAd1	nejen
kapucínů	kapucín	k1gMnPc2	kapucín
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
sester	sestra	k1gFnPc2	sestra
Alžbětinek	alžbětinka	k1gFnPc2	alžbětinka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
brněnský	brněnský	k2eAgInSc1d1	brněnský
klášter	klášter	k1gInSc1	klášter
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1748	[number]	k4	1748
založila	založit	k5eAaPmAgFnS	založit
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
bratři	bratr	k1gMnPc1	bratr
kapucíni	kapucín	k1gMnPc1	kapucín
byli	být	k5eAaImAgMnP	být
pohřbívání	pohřbívání	k1gNnSc4	pohřbívání
na	na	k7c4	na
holou	holý	k2eAgFnSc4d1	holá
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
dobrodinci	dobrodinec	k1gMnPc1	dobrodinec
byli	být	k5eAaImAgMnP	být
ukládáni	ukládat	k5eAaImNgMnP	ukládat
do	do	k7c2	do
dubových	dubový	k2eAgFnPc2d1	dubová
nebo	nebo	k8xC	nebo
borových	borový	k2eAgFnPc2d1	Borová
rakví	rakev	k1gFnPc2	rakev
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
ručně	ručně	k6eAd1	ručně
zdobeny	zdoben	k2eAgFnPc1d1	zdobena
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
kapucínského	kapucínský	k2eAgInSc2d1	kapucínský
hrobce	hrobec	k1gInSc2	hrobec
se	se	k3xPyFc4	se
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
ojediněle	ojediněle	k6eAd1	ojediněle
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
původních	původní	k2eAgFnPc2d1	původní
barokních	barokní	k2eAgFnPc2d1	barokní
rakví	rakev	k1gFnPc2	rakev
<g/>
,	,	kIx,	,
ručně	ručně	k6eAd1	ručně
zdobených	zdobený	k2eAgMnPc2d1	zdobený
<g/>
.	.	kIx.	.
</s>
