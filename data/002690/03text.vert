<s>
Cidlina	Cidlina	k1gFnSc1	Cidlina
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
severovýchodních	severovýchodní	k2eAgFnPc6d1	severovýchodní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
činí	činit	k5eAaImIp3nS	činit
87,3	[number]	k4	87,3
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
měří	měřit	k5eAaImIp3nS	měřit
1167,0	[number]	k4	1167,0
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nP	pramenit
na	na	k7c6	na
západních	západní	k2eAgInPc6d1	západní
svazích	svah	k1gInPc6	svah
vrchu	vrch	k1gInSc2	vrch
Tábor	Tábor	k1gInSc1	Tábor
(	(	kIx(	(
<g/>
678	[number]	k4	678
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poblíž	poblíž	k6eAd1	poblíž
Lomnice	Lomnice	k1gFnSc1	Lomnice
nad	nad	k7c7	nad
Popelkou	Popelka	k1gFnSc7	Popelka
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
místní	místní	k2eAgFnSc2d1	místní
části	část	k1gFnSc2	část
Košov	Košovo	k1gNnPc2	Košovo
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
okolo	okolo	k7c2	okolo
565	[number]	k4	565
m.	m.	k?	m.
Pod	pod	k7c7	pod
Jičínem	Jičín	k1gInSc7	Jičín
středně	středně	k6eAd1	středně
až	až	k9	až
silně	silně	k6eAd1	silně
znečištěná	znečištěný	k2eAgNnPc1d1	znečištěné
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
rozkolísanost	rozkolísanost	k1gFnSc1	rozkolísanost
průtoků	průtok	k1gInPc2	průtok
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
187	[number]	k4	187
m	m	kA	m
u	u	k7c2	u
Libice	Libice	k1gFnPc1	Libice
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
zprava	zprava	k6eAd1	zprava
do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
soutoku	soutok	k1gInSc2	soutok
s	s	k7c7	s
Labem	Labe	k1gNnSc7	Labe
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Libický	Libický	k2eAgInSc1d1	Libický
luh	luh	k1gInSc1	luh
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
představuje	představovat	k5eAaImIp3nS	představovat
největší	veliký	k2eAgInSc4d3	veliký
souvislý	souvislý	k2eAgInSc4d1	souvislý
komplex	komplex	k1gInSc4	komplex
úvalového	úvalový	k2eAgInSc2d1	úvalový
lužního	lužní	k2eAgInSc2d1	lužní
lesa	les	k1gInSc2	les
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
asi	asi	k9	asi
500	[number]	k4	500
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
narušený	narušený	k2eAgMnSc1d1	narušený
výstavbou	výstavba	k1gFnSc7	výstavba
dálnice	dálnice	k1gFnSc2	dálnice
D	D	kA	D
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
protéká	protékat	k5eAaImIp3nS	protékat
městy	město	k1gNnPc7	město
<g/>
:	:	kIx,	:
Železnice	železnice	k1gFnSc1	železnice
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Jičín	Jičín	k1gInSc1	Jičín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jičín	Jičín	k1gInSc1	Jičín
<g/>
,	,	kIx,	,
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
Veselí	veselí	k1gNnSc1	veselí
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Bydžov	Bydžov	k1gInSc1	Bydžov
a	a	k8xC	a
Chlumec	Chlumec	k1gInSc1	Chlumec
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
přítokem	přítok	k1gInSc7	přítok
Cidliny	Cidlina	k1gFnSc2	Cidlina
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
délky	délka	k1gFnSc2	délka
toku	tok	k1gInSc2	tok
<g/>
,	,	kIx,	,
plochy	plocha	k1gFnPc1	plocha
povodí	povodí	k1gNnSc2	povodí
a	a	k8xC	a
vodnosti	vodnost	k1gFnSc2	vodnost
týče	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
Bystřice	Bystřice	k1gFnSc1	Bystřice
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
přítokem	přítok	k1gInSc7	přítok
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
Javorka	Javorka	k1gFnSc1	Javorka
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
říční	říční	k2eAgFnSc2d1	říční
sítě	síť	k1gFnSc2	síť
činí	činit	k5eAaImIp3nS	činit
0,95	[number]	k4	0,95
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Cidliny	Cidlina	k1gFnSc2	Cidlina
nachází	nacházet	k5eAaImIp3nS	nacházet
985	[number]	k4	985
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
kilometru	kilometr	k1gInSc2	kilometr
a	a	k8xC	a
377	[number]	k4	377
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
1	[number]	k4	1
až	až	k9	až
10	[number]	k4	10
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Vodotečí	vodoteč	k1gFnSc7	vodoteč
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
km	km	kA	km
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
celkem	celkem	k6eAd1	celkem
deset	deset	k4xCc1	deset
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
mezi	mezi	k7c7	mezi
20	[number]	k4	20
až	až	k9	až
40	[number]	k4	40
km	km	kA	km
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgMnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
délce	délka	k1gFnSc6	délka
40	[number]	k4	40
až	až	k9	až
60	[number]	k4	60
km	km	kA	km
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Cidliny	Cidlina	k1gFnSc2	Cidlina
ani	ani	k8xC	ani
jeden	jeden	k4xCgInSc4	jeden
vodní	vodní	k2eAgInSc4d1	vodní
tok	tok	k1gInSc4	tok
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
toky	toka	k1gFnPc1	toka
delší	dlouhý	k2eAgFnPc1d2	delší
než	než	k8xS	než
60	[number]	k4	60
kilometrů	kilometr	k1gInPc2	kilometr
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
dva	dva	k4xCgInPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
samotné	samotný	k2eAgFnSc2d1	samotná
Cidliny	Cidlina	k1gFnSc2	Cidlina
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
řeka	řeka	k1gFnSc1	řeka
Bystřice	Bystřice	k1gFnSc1	Bystřice
<g/>
.	.	kIx.	.
</s>
<s>
Doubravický	Doubravický	k2eAgInSc1d1	Doubravický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
81,2	[number]	k4	81,2
Doubravický	Doubravický	k2eAgInSc4d1	Doubravický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
79,7	[number]	k4	79,7
Ploužnický	Ploužnický	k2eAgInSc4d1	Ploužnický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
79,2	[number]	k4	79,2
Dílecký	Dílecký	k2eAgInSc4d1	Dílecký
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
77,9	[number]	k4	77,9
Kbelnický	Kbelnický	k2eAgInSc4d1	Kbelnický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
76,6	[number]	k4	76,6
<g />
.	.	kIx.	.
</s>
<s>
Holínský	Holínský	k2eAgInSc1d1	Holínský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
74,8	[number]	k4	74,8
Valdický	valdický	k2eAgInSc4d1	valdický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
72,6	[number]	k4	72,6
Porák	Porák	k1gInSc4	Porák
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
71,7	[number]	k4	71,7
Popovický	popovický	k2eAgInSc4d1	popovický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
69,8	[number]	k4	69,8
Černý	černý	k2eAgInSc1d1	černý
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
69,1	[number]	k4	69,1
Úlibický	Úlibický	k2eAgMnSc1d1	Úlibický
<g />
.	.	kIx.	.
</s>
<s>
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
66,8	[number]	k4	66,8
Nemyčeveský	Nemyčeveský	k2eAgInSc4d1	Nemyčeveský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
59,6	[number]	k4	59,6
Stříbrnice	Stříbrnice	k1gFnPc1	Stříbrnice
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
58,1	[number]	k4	58,1
Žeretický	Žeretický	k2eAgInSc4d1	Žeretický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
56,3	[number]	k4	56,3
Volanka	Volanka	k1gFnSc1	Volanka
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
54,8	[number]	k4	54,8
Řečice	Řečice	k1gFnSc2	Řečice
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
47,4	[number]	k4	47,4
Javorka	Javorka	k1gFnSc1	Javorka
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
44,4	[number]	k4	44,4
Kněžovka	Kněžovka	k1gFnSc1	Kněžovka
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
42,2	[number]	k4	42,2
Požíračka	požíračka	k1gFnSc1	požíračka
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
41,5	[number]	k4	41,5
Králický	králický	k2eAgInSc4d1	králický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
40,2	[number]	k4	40,2
Zábědovský	Zábědovský	k2eAgInSc4d1	Zábědovský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
ř.	ř.	k?	ř.
km	km	kA	km
37,9	[number]	k4	37,9
Lužecký	Lužecký	k2eAgInSc4d1	Lužecký
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
30,4	[number]	k4	30,4
Bystřice	Bystřice	k1gFnSc2	Bystřice
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
29,0	[number]	k4	29,0
Olešnický	olešnický	k2eAgInSc4d1	olešnický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
27,0	[number]	k4	27,0
Milešovický	Milešovický	k2eAgInSc4d1	Milešovický
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
8,6	[number]	k4	8,6
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
Cidliny	Cidlina	k1gFnSc2	Cidlina
v	v	k7c6	v
Sánech	Sány	k1gInPc6	Sány
činí	činit	k5eAaImIp3nS	činit
5,20	[number]	k4	5,20
m3	m3	k4	m3
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Průměrné	průměrný	k2eAgInPc4d1	průměrný
dlouhodobé	dlouhodobý	k2eAgInPc4d1	dlouhodobý
měsíční	měsíční	k2eAgInPc4d1	měsíční
průtoky	průtok	k1gInPc4	průtok
Cidliny	Cidlina	k1gFnSc2	Cidlina
(	(	kIx(	(
<g/>
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Sány	sán	k2eAgFnPc1d1	sán
<g/>
:	:	kIx,	:
Průměrné	průměrný	k2eAgInPc1d1	průměrný
měsíční	měsíční	k2eAgInPc1d1	měsíční
průtoky	průtok	k1gInPc1	průtok
Cidliny	Cidlina	k1gFnSc2	Cidlina
(	(	kIx(	(
<g/>
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Sány	sán	k2eAgFnPc1d1	sán
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Hlásné	hlásný	k2eAgInPc4d1	hlásný
profily	profil	k1gInPc4	profil
<g/>
:	:	kIx,	:
Od	od	k7c2	od
Jičína	Jičín	k1gInSc2	Jičín
je	být	k5eAaImIp3nS	být
tok	tok	k1gInSc1	tok
sjízdný	sjízdný	k2eAgInSc1d1	sjízdný
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
protéká	protékat	k5eAaImIp3nS	protékat
významnou	významný	k2eAgFnSc7d1	významná
rybníkářskou	rybníkářský	k2eAgFnSc7d1	rybníkářská
oblastí	oblast	k1gFnSc7	oblast
-	-	kIx~	-
četné	četný	k2eAgInPc1d1	četný
náhony	náhon	k1gInPc1	náhon
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Sánský	Sánský	k2eAgInSc1d1	Sánský
kanál	kanál	k1gInSc1	kanál
spojující	spojující	k2eAgFnSc4d1	spojující
Cidlinu	Cidlina	k1gFnSc4	Cidlina
s	s	k7c7	s
Labem	Labe	k1gNnSc7	Labe
a	a	k8xC	a
Mrlinou	Mrlina	k1gFnSc7	Mrlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
toku	tok	k1gInSc6	tok
a	a	k8xC	a
přítocích	přítok	k1gInPc6	přítok
velké	velká	k1gFnSc2	velká
množství	množství	k1gNnSc2	množství
rybníků	rybník	k1gInPc2	rybník
-	-	kIx~	-
např.	např.	kA	např.
Valcha	valcha	k1gFnSc1	valcha
(	(	kIx(	(
<g/>
u	u	k7c2	u
Železnice	železnice	k1gFnSc2	železnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kníže	kníže	k1gMnSc1	kníže
(	(	kIx(	(
<g/>
v	v	k7c6	v
Jičíně	Jičín	k1gInSc6	Jičín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ostruženské	Ostruženský	k2eAgInPc1d1	Ostruženský
rybníky	rybník	k1gInPc1	rybník
<g/>
,	,	kIx,	,
Dvorecký	dvorecký	k2eAgInSc1d1	dvorecký
rybník	rybník	k1gInSc1	rybník
<g/>
,	,	kIx,	,
Vysokoveselský	Vysokoveselský	k2eAgInSc1d1	Vysokoveselský
rybník	rybník	k1gInSc1	rybník
<g/>
,	,	kIx,	,
Chlumecký	chlumecký	k2eAgInSc1d1	chlumecký
rybník	rybník	k1gInSc1	rybník
a	a	k8xC	a
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
Žehuňský	Žehuňský	k2eAgInSc1d1	Žehuňský
rybník	rybník	k1gInSc1	rybník
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
roku	rok	k1gInSc2	rok
1492	[number]	k4	1492
Štěpánkem	Štěpánek	k1gMnSc7	Štěpánek
Netolickým	netolický	k2eAgMnSc7d1	netolický
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
Cidlině	Cidlina	k1gFnSc6	Cidlina
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
Rychlík	rychlík	k1gInSc4	rychlík
854	[number]	k4	854
a	a	k8xC	a
855	[number]	k4	855
z	z	k7c2	z
Trutnova	Trutnov	k1gInSc2	Trutnov
hl.	hl.	k?	hl.
<g/>
n.	n.	k?	n.
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
hl.	hl.	k?	hl.
<g/>
n.	n.	k?	n.
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
