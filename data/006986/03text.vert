<s>
Jan	Jan	k1gMnSc1	Jan
Matura	matura	k1gFnSc1	matura
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1980	[number]	k4	1980
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
skokan	skokan	k1gMnSc1	skokan
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
a	a	k8xC	a
sdruženář	sdruženář	k1gMnSc1	sdruženář
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
především	především	k9	především
severské	severský	k2eAgFnSc3d1	severská
kombinaci	kombinace	k1gFnSc3	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
vystartoval	vystartovat	k5eAaPmAgMnS	vystartovat
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
juniorů	junior	k1gMnPc2	junior
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
družstev	družstvo	k1gNnPc2	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
seniorských	seniorský	k2eAgFnPc2d1	seniorská
světových	světový	k2eAgFnPc2d1	světová
soutěží	soutěž	k1gFnPc2	soutěž
nahlédl	nahlédnout	k5eAaPmAgMnS	nahlédnout
hned	hned	k6eAd1	hned
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jako	jako	k9	jako
sedmnáctiletý	sedmnáctiletý	k2eAgMnSc1d1	sedmnáctiletý
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
českého	český	k2eAgInSc2d1	český
sdruženářského	sdruženářský	k2eAgInSc2d1	sdruženářský
týmu	tým	k1gInSc2	tým
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Trondheimu	Trondheim	k1gInSc6	Trondheim
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1998	[number]	k4	1998
v	v	k7c6	v
japonském	japonský	k2eAgNnSc6d1	Japonské
Naganu	Nagano	k1gNnSc6	Nagano
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
na	na	k7c4	na
35	[number]	k4	35
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
družstvech	družstvo	k1gNnPc6	družstvo
pak	pak	k6eAd1	pak
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
osmém	osmý	k4xOgNnSc6	osmý
<g/>
.	.	kIx.	.
</s>
<s>
Častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
až	až	k9	až
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
skončil	skončit	k5eAaPmAgMnS	skončit
devátý	devátý	k4xOgMnSc1	devátý
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Schonachu	Schonach	k1gInSc6	Schonach
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
v	v	k7c4	v
Berchtesgadenu	Berchtesgaden	k2eAgFnSc4d1	Berchtesgaden
skončil	skončit	k5eAaPmAgMnS	skončit
třetí	třetí	k4xOgMnSc1	třetí
v	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
části	část	k1gFnSc6	část
série	série	k1gFnSc2	série
světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tento	tento	k3xDgInSc4	tento
výkon	výkon	k1gInSc4	výkon
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nepřekonal	překonat	k5eNaPmAgMnS	překonat
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Matura	matura	k1gFnSc1	matura
měl	mít	k5eAaImAgInS	mít
vždy	vždy	k6eAd1	vždy
výrazně	výrazně	k6eAd1	výrazně
lepší	dobrý	k2eAgFnSc4d2	lepší
skokanskou	skokanský	k2eAgFnSc4d1	skokanská
část	část	k1gFnSc4	část
a	a	k8xC	a
v	v	k7c6	v
běžecké	běžecký	k2eAgFnSc6d1	běžecká
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
propadal	propadat	k5eAaPmAgMnS	propadat
ve	v	k7c6	v
výsledkové	výsledkový	k2eAgFnSc6d1	výsledková
listině	listina	k1gFnSc6	listina
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
velký	velký	k2eAgInSc1d1	velký
sdruženářský	sdruženářský	k2eAgInSc1d1	sdruženářský
závod	závod	k1gInSc1	závod
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Lahti	Lahť	k1gFnSc6	Lahť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
44	[number]	k4	44
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
přešel	přejít	k5eAaPmAgInS	přejít
mezi	mezi	k7c7	mezi
skokany-specialisty	skokanypecialista	k1gMnPc7	skokany-specialista
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
velkou	velký	k2eAgFnSc4d1	velká
premiéru	premiéra	k1gFnSc4	premiéra
mezi	mezi	k7c7	mezi
skokany	skokan	k1gMnPc7	skokan
si	se	k3xPyFc3	se
odbyl	odbýt	k5eAaPmAgInS	odbýt
už	už	k6eAd1	už
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1997	[number]	k4	1997
v	v	k7c6	v
Trondheimu	Trondheim	k1gInSc6	Trondheim
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
na	na	k7c6	na
velkém	velký	k2eAgInSc6d1	velký
můstku	můstek	k1gInSc6	můstek
tehdy	tehdy	k6eAd1	tehdy
obsadil	obsadit	k5eAaPmAgMnS	obsadit
39	[number]	k4	39
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
samostatný	samostatný	k2eAgInSc1d1	samostatný
skokanský	skokanský	k2eAgInSc1d1	skokanský
závod	závod	k1gInSc1	závod
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
až	až	k9	až
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
"	"	kIx"	"
<g/>
druholigovém	druholigový	k2eAgMnSc6d1	druholigový
<g/>
"	"	kIx"	"
Kontinentálním	kontinentální	k2eAgInSc6d1	kontinentální
poháru	pohár	k1gInSc6	pohár
získal	získat	k5eAaPmAgInS	získat
nejdříve	dříve	k6eAd3	dříve
nebodované	bodovaný	k2eNgInPc4d1	nebodovaný
45	[number]	k4	45
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
již	již	k6eAd1	již
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
poté	poté	k6eAd1	poté
obsadil	obsadit	k5eAaPmAgMnS	obsadit
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
pozici	pozice	k1gFnSc4	pozice
a	a	k8xC	a
zanedlouho	zanedlouho	k6eAd1	zanedlouho
si	se	k3xPyFc3	se
odbyl	odbýt	k5eAaPmAgMnS	odbýt
premiéru	premiéra	k1gFnSc4	premiéra
i	i	k9	i
ve	v	k7c6	v
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
bodované	bodovaný	k2eAgNnSc4d1	bodované
umístění	umístění	k1gNnSc4	umístění
ovšem	ovšem	k9	ovšem
ještě	ještě	k6eAd1	ještě
dlouho	dlouho	k6eAd1	dlouho
nestačil	stačit	k5eNaBmAgInS	stačit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
mistrovství	mistrovství	k1gNnSc4	mistrovství
ČR	ČR	kA	ČR
na	na	k7c6	na
velkém	velký	k2eAgInSc6d1	velký
můstku	můstek	k1gInSc6	můstek
<g/>
,	,	kIx,	,
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
také	také	k9	také
ZOH	ZOH	kA	ZOH
2002	[number]	k4	2002
(	(	kIx(	(
<g/>
střední	střední	k2eAgFnSc1d1	střední
můstek	můstek	k1gInSc1	můstek
–	–	k?	–
47	[number]	k4	47
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc4d1	velký
můstek	můstek	k1gInSc4	můstek
–	–	k?	–
37	[number]	k4	37
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
družstva	družstvo	k1gNnSc2	družstvo
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
na	na	k7c4	na
20	[number]	k4	20
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
velký	velký	k2eAgInSc1d1	velký
můstek	můstek	k1gInSc1	můstek
<g/>
)	)	kIx)	)
a	a	k8xC	a
28	[number]	k4	28
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
střední	střední	k2eAgInSc4d1	střední
můstek	můstek	k1gInSc4	můstek
<g/>
)	)	kIx)	)
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
závodě	závod	k1gInSc6	závod
sezony	sezona	k1gFnSc2	sezona
2003	[number]	k4	2003
získal	získat	k5eAaPmAgInS	získat
i	i	k9	i
první	první	k4xOgInPc4	první
body	bod	k1gInPc4	bod
do	do	k7c2	do
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
Planici	Planice	k1gFnSc6	Planice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příchod	příchod	k1gInSc1	příchod
nového	nový	k2eAgMnSc2d1	nový
reprezentačního	reprezentační	k2eAgMnSc2d1	reprezentační
trenéra	trenér	k1gMnSc2	trenér
Vasji	Vasje	k1gFnSc4	Vasje
Bajce	bajka	k1gFnSc3	bajka
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2005	[number]	k4	2005
prospěla	prospět	k5eAaPmAgFnS	prospět
nejen	nejen	k6eAd1	nejen
Jakubu	Jakub	k1gMnSc3	Jakub
Jandovi	Janda	k1gMnSc3	Janda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
začala	začít	k5eAaPmAgFnS	začít
pozitivně	pozitivně	k6eAd1	pozitivně
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
i	i	k9	i
Maturovu	Maturův	k2eAgFnSc4d1	Maturova
výkonnost	výkonnost	k1gFnSc4	výkonnost
<g/>
.	.	kIx.	.
</s>
<s>
Častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
dostával	dostávat	k5eAaImAgInS	dostávat
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
třicítku	třicítka	k1gFnSc4	třicítka
skokanů	skokan	k1gMnPc2	skokan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sapporu	Sappor	k1gInSc6	Sappor
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
svého	svůj	k3xOyFgNnSc2	svůj
dosavadního	dosavadní	k2eAgNnSc2d1	dosavadní
maxima	maximum	k1gNnSc2	maximum
<g/>
:	:	kIx,	:
11	[number]	k4	11
<g/>
.	.	kIx.	.
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
hodnocení	hodnocení	k1gNnSc6	hodnocení
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
44	[number]	k4	44
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
letní	letní	k2eAgFnSc6d1	letní
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
obsadil	obsadit	k5eAaPmAgMnS	obsadit
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Courchevelu	Courchevel	k1gInSc6	Courchevel
<g/>
,	,	kIx,	,
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
závodů	závod	k1gInPc2	závod
skončil	skončit	k5eAaPmAgMnS	skončit
"	"	kIx"	"
<g/>
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
do	do	k7c2	do
šestého	šestý	k4xOgNnSc2	šestý
místa	místo	k1gNnSc2	místo
<g/>
)	)	kIx)	)
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
obsadil	obsadit	k5eAaPmAgMnS	obsadit
šestou	šestý	k4xOgFnSc4	šestý
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
sezóny	sezóna	k1gFnSc2	sezóna
2006	[number]	k4	2006
měl	mít	k5eAaImAgInS	mít
kolísající	kolísající	k2eAgFnSc4d1	kolísající
výkonnost	výkonnost	k1gFnSc4	výkonnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
půdě	půda	k1gFnSc6	půda
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
solidním	solidní	k2eAgMnSc6d1	solidní
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
na	na	k7c6	na
Turné	turné	k1gNnSc6	turné
čtyř	čtyři	k4xCgInPc2	čtyři
můstků	můstek	k1gInPc2	můstek
dokázal	dokázat	k5eAaPmAgInS	dokázat
bodovat	bodovat	k5eAaImF	bodovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
závodě	závod	k1gInSc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
Sapporu	Sappor	k1gInSc6	Sappor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tradičně	tradičně	k6eAd1	tradičně
chyběla	chybět	k5eAaImAgFnS	chybět
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
skokanské	skokanský	k2eAgFnSc2d1	skokanská
špičky	špička	k1gFnSc2	špička
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
prvně	prvně	k?	prvně
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
a	a	k8xC	a
skončil	skončit	k5eAaPmAgInS	skončit
devátý	devátý	k4xOgMnSc1	devátý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
olympiádu	olympiáda	k1gFnSc4	olympiáda
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
odjížděl	odjíždět	k5eAaImAgMnS	odjíždět
jako	jako	k8xS	jako
česká	český	k2eAgFnSc1d1	Česká
dvojka	dvojka	k1gFnSc1	dvojka
za	za	k7c7	za
Jakubem	Jakub	k1gMnSc7	Jakub
Jandou	Janda	k1gMnSc7	Janda
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
místem	místem	k6eAd1	místem
splnil	splnit	k5eAaPmAgMnS	splnit
očekávání	očekávání	k1gNnSc4	očekávání
(	(	kIx(	(
<g/>
české	český	k2eAgNnSc4d1	české
družstvo	družstvo	k1gNnSc4	družstvo
skončilo	skončit	k5eAaPmAgNnS	skončit
deváté	devátý	k4xOgFnSc2	devátý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
poté	poté	k6eAd1	poté
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
mistrovství	mistrovství	k1gNnSc4	mistrovství
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
obsadil	obsadit	k5eAaPmAgMnS	obsadit
konečné	konečný	k2eAgNnSc4d1	konečné
34	[number]	k4	34
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzestupu	vzestup	k1gInSc6	vzestup
v	v	k7c6	v
předchozích	předchozí	k2eAgInPc6d1	předchozí
3	[number]	k4	3
sezónách	sezóna	k1gFnPc6	sezóna
přišla	přijít	k5eAaPmAgFnS	přijít
stagnace	stagnace	k1gFnSc1	stagnace
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výsledek	výsledek	k1gInSc4	výsledek
Matura	matura	k1gFnSc1	matura
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
na	na	k7c6	na
mamutím	mamutí	k2eAgInSc6d1	mamutí
můstku	můstek	k1gInSc6	můstek
ve	v	k7c6	v
Vikersundu	Vikersund	k1gInSc6	Vikersund
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bodovat	bodovat	k5eAaImF	bodovat
dokázal	dokázat	k5eAaPmAgMnS	dokázat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
3	[number]	k4	3
závodech	závod	k1gInPc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Sapporu	Sappor	k1gInSc6	Sappor
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
do	do	k7c2	do
týmového	týmový	k2eAgInSc2d1	týmový
závodu	závod	k1gInSc2	závod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Češi	Čech	k1gMnPc1	Čech
obsadili	obsadit	k5eAaPmAgMnP	obsadit
9	[number]	k4	9
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2008	[number]	k4	2008
přinesla	přinést	k5eAaPmAgFnS	přinést
mírné	mírný	k2eAgNnSc4d1	mírné
zlepšení	zlepšení	k1gNnSc4	zlepšení
<g/>
.	.	kIx.	.
</s>
<s>
Nejlépe	dobře	k6eAd3	dobře
se	se	k3xPyFc4	se
Maturovi	Matur	k1gMnSc3	Matur
dařilo	dařit	k5eAaImAgNnS	dařit
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
půdě	půda	k1gFnSc6	půda
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
doskočil	doskočit	k5eAaPmAgMnS	doskočit
pro	pro	k7c4	pro
15	[number]	k4	15
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tohoto	tento	k3xDgInSc2	tento
výsledku	výsledek	k1gInSc2	výsledek
Matura	matura	k1gFnSc1	matura
několikrát	několikrát	k6eAd1	několikrát
úspěšně	úspěšně	k6eAd1	úspěšně
atakoval	atakovat	k5eAaBmAgMnS	atakovat
bodované	bodovaný	k2eAgNnSc4d1	bodované
umístění	umístění	k1gNnSc4	umístění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výraznějšího	výrazný	k2eAgInSc2d2	výraznější
výkonu	výkon	k1gInSc2	výkon
už	už	k6eAd1	už
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
závodě	závod	k1gInSc6	závod
sezóny	sezóna	k1gFnSc2	sezóna
byl	být	k5eAaImAgInS	být
Matura	matura	k1gFnSc1	matura
součástí	součást	k1gFnPc2	součást
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
získal	získat	k5eAaPmAgInS	získat
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
týmovém	týmový	k2eAgInSc6d1	týmový
závodě	závod	k1gInSc6	závod
na	na	k7c6	na
můstku	můstek	k1gInSc6	můstek
Letalnica	Letalnic	k1gInSc2	Letalnic
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
následující	následující	k2eAgFnSc2d1	následující
sezóny	sezóna	k1gFnSc2	sezóna
Matura	matura	k1gFnSc1	matura
strávil	strávit	k5eAaPmAgInS	strávit
v	v	k7c6	v
Kontinentálním	kontinentální	k2eAgInSc6d1	kontinentální
poháru	pohár	k1gInSc6	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
se	se	k3xPyFc4	se
podíval	podívat	k5eAaPmAgMnS	podívat
na	na	k7c4	na
stupně	stupeň	k1gInPc4	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
Braunlage	Braunlage	k1gFnSc6	Braunlage
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Zakopanem	Zakopan	k1gMnSc7	Zakopan
<g/>
.	.	kIx.	.
</s>
<s>
Maximem	maximum	k1gNnSc7	maximum
ve	v	k7c6	v
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
bylo	být	k5eAaImAgNnS	být
23	[number]	k4	23
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
Engelbergu	Engelberg	k1gInSc6	Engelberg
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
podobná	podobný	k2eAgFnSc1d1	podobná
těm	ten	k3xDgMnPc3	ten
předchozím	předchozí	k2eAgMnPc3d1	předchozí
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
umístění	umístění	k1gNnSc1	umístění
v	v	k7c6	v
elitní	elitní	k2eAgFnSc6d1	elitní
dvacítce	dvacítka	k1gFnSc6	dvacítka
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Klingenthalu	Klingenthal	k1gInSc6	Klingenthal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
dobrých	dobrý	k2eAgNnPc2d1	dobré
umístění	umístění	k1gNnPc2	umístění
Matura	matura	k1gFnSc1	matura
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
v	v	k7c6	v
Kontinentálním	kontinentální	k2eAgInSc6d1	kontinentální
poháru	pohár	k1gInSc6	pohár
ve	v	k7c6	v
slovinské	slovinský	k2eAgFnSc6d1	slovinská
Kranji	Kranj	k1gFnSc6	Kranj
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Brotterode	Brotterod	k1gMnSc5	Brotterod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
závodě	závod	k1gInSc6	závod
skončil	skončit	k5eAaPmAgInS	skončit
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
dokonce	dokonce	k9	dokonce
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
letech	let	k1gInPc6	let
skončil	skončit	k5eAaPmAgMnS	skončit
v	v	k7c6	v
týmové	týmový	k2eAgFnSc6d1	týmová
soutěži	soutěž	k1gFnSc6	soutěž
pátý	pátý	k4xOgInSc1	pátý
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
Maturu	matura	k1gFnSc4	matura
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
půdě	půda	k1gFnSc6	půda
v	v	k7c6	v
Harrachově	Harrachov	k1gInSc6	Harrachov
skončil	skončit	k5eAaPmAgMnS	skončit
jedenáctý	jedenáctý	k4xOgMnSc1	jedenáctý
a	a	k8xC	a
dvacátý	dvacátý	k4xOgMnSc1	dvacátý
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
v	v	k7c6	v
Sapporu	Sappor	k1gInSc6	Sappor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chyběla	chybět	k5eAaImAgFnS	chybět
část	část	k1gFnSc1	část
světové	světový	k2eAgFnSc2d1	světová
špičky	špička	k1gFnSc2	špička
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
závodě	závod	k1gInSc6	závod
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
kvalifikovalo	kvalifikovat	k5eAaBmAgNnS	kvalifikovat
30	[number]	k4	30
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
,	,	kIx,	,
skončil	skončit	k5eAaPmAgMnS	skončit
Matura	matura	k1gFnSc1	matura
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
zakončil	zakončit	k5eAaPmAgMnS	zakončit
tak	tak	k9	tak
svoji	svůj	k3xOyFgFnSc4	svůj
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
sezónu	sezóna	k1gFnSc4	sezóna
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
27	[number]	k4	27
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2013	[number]	k4	2013
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
závod	závod	k1gInSc1	závod
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
Sapporu	Sappor	k1gInSc6	Sappor
<g/>
,	,	kIx,	,
když	když	k8xS	když
svými	svůj	k3xOyFgInPc7	svůj
skoky	skok	k1gInPc7	skok
132	[number]	k4	132
m	m	kA	m
a	a	k8xC	a
135	[number]	k4	135
m	m	kA	m
(	(	kIx(	(
<g/>
249,5	[number]	k4	249,5
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
porazil	porazit	k5eAaPmAgInS	porazit
druhého	druhý	k4xOgMnSc4	druhý
Nora	Nor	k1gMnSc4	Nor
Toma	Tom	k1gMnSc4	Tom
Hildeho	Hilde	k1gMnSc4	Hilde
rozdílem	rozdíl	k1gInSc7	rozdíl
sedmi	sedm	k4xCc2	sedm
desetin	desetina	k1gFnPc2	desetina
bodu	bod	k1gInSc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
úspěch	úspěch	k1gInSc1	úspěch
zopakoval	zopakovat	k5eAaPmAgInS	zopakovat
po	po	k7c6	po
vyrovnaných	vyrovnaný	k2eAgInPc6d1	vyrovnaný
skocích	skok	k1gInPc6	skok
132,5	[number]	k4	132,5
m	m	kA	m
a	a	k8xC	a
133	[number]	k4	133
m.	m.	k?	m.
Startoval	startovat	k5eAaBmAgInS	startovat
také	také	k9	také
na	na	k7c4	na
ZOH	ZOH	kA	ZOH
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
můstku	můstek	k1gInSc6	můstek
skončil	skončit	k5eAaPmAgInS	skončit
ma	ma	k?	ma
23	[number]	k4	23
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
,	,	kIx,	,
na	na	k7c6	na
velkém	velký	k2eAgInSc6d1	velký
můstku	můstek	k1gInSc6	můstek
byl	být	k5eAaImAgMnS	být
osmnáctý	osmnáctý	k4xOgMnSc1	osmnáctý
a	a	k8xC	a
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
týmem	tým	k1gInSc7	tým
obsadil	obsadit	k5eAaPmAgMnS	obsadit
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
družstev	družstvo	k1gNnPc2	družstvo
sedmé	sedmý	k4xOgNnSc4	sedmý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2017	[number]	k4	2017
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
ukončení	ukončení	k1gNnSc4	ukončení
své	svůj	k3xOyFgFnSc2	svůj
sportovní	sportovní	k2eAgFnSc2d1	sportovní
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
72	[number]	k4	72
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
7	[number]	k4	7
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
51	[number]	k4	51
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
32	[number]	k4	32
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
44	[number]	k4	44
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
66	[number]	k4	66
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
34	[number]	k4	34
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
121	[number]	k4	121
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
57	[number]	k4	57
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
33	[number]	k4	33
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
44	[number]	k4	44
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
39	[number]	k4	39
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
66	[number]	k4	66
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
10	[number]	k4	10
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
53	[number]	k4	53
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
45	[number]	k4	45
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
27	[number]	k4	27
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
180	[number]	k4	180
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
52	[number]	k4	52
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
40	[number]	k4	40
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
631	[number]	k4	631
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
26	[number]	k4	26
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
40	[number]	k4	40
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
33	[number]	k4	33
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
</s>
