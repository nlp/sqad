<p>
<s>
Plochý	plochý	k2eAgInSc4d1	plochý
pletací	pletací	k2eAgInSc4d1	pletací
stroj	stroj	k1gInSc4	stroj
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
Američan	Američan	k1gMnSc1	Američan
William	William	k1gInSc4	William
Lamb	Lamb	k1gInSc4	Lamb
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
<g/>
.	.	kIx.	.
</s>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
zpravidla	zpravidla	k6eAd1	zpravidla
s	s	k7c7	s
jazýčkovými	jazýčkový	k2eAgFnPc7d1	jazýčková
jehlami	jehla	k1gFnPc7	jehla
umístěnými	umístěný	k2eAgFnPc7d1	umístěná
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
řadě	řada	k1gFnSc6	řada
v	v	k7c6	v
jehlovém	jehlový	k2eAgNnSc6d1	jehlové
lůžku	lůžko	k1gNnSc6	lůžko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Postup	postup	k1gInSc1	postup
výroby	výroba	k1gFnSc2	výroba
pleteniny	pletenina	k1gFnSc2	pletenina
==	==	k?	==
</s>
</p>
<p>
<s>
Jehly	jehla	k1gFnPc1	jehla
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
jednotlivě	jednotlivě	k6eAd1	jednotlivě
v	v	k7c6	v
časovém	časový	k2eAgInSc6d1	časový
sledu	sled	k1gInSc6	sled
<g/>
.	.	kIx.	.
</s>
<s>
Sáně	sáně	k1gFnPc1	sáně
se	se	k3xPyFc4	se
zámkem	zámek	k1gInSc7	zámek
jezdí	jezdit	k5eAaImIp3nS	jezdit
podél	podél	k7c2	podél
jehel	jehla	k1gFnPc2	jehla
a	a	k8xC	a
uchopí	uchopit	k5eAaPmIp3nS	uchopit
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
patky	patka	k1gFnPc1	patka
jehel	jehla	k1gFnPc2	jehla
jednu	jeden	k4xCgFnSc4	jeden
po	po	k7c4	po
druhé	druhý	k4xOgMnPc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
patky	patka	k1gFnPc1	patka
prochází	procházet	k5eAaImIp3nP	procházet
zakřiveným	zakřivený	k2eAgInSc7d1	zakřivený
kanálem	kanál	k1gInSc7	kanál
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
nahoru	nahoru	k6eAd1	nahoru
a	a	k8xC	a
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
hlavičky	hlavička	k1gFnSc2	hlavička
jehel	jehla	k1gFnPc2	jehla
zachytí	zachytit	k5eAaPmIp3nS	zachytit
předloženou	předložený	k2eAgFnSc4d1	předložená
nit	nit	k1gFnSc4	nit
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
kontinuálně	kontinuálně	k6eAd1	kontinuálně
řádek	řádek	k1gInSc4	řádek
pleteniny	pletenina	k1gFnSc2	pletenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
fáze	fáze	k1gFnPc1	fáze
(	(	kIx(	(
<g/>
polohy	poloha	k1gFnPc1	poloha
<g/>
)	)	kIx)	)
jehel	jehla	k1gFnPc2	jehla
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
základní	základní	k2eAgFnSc1d1	základní
–	–	k?	–
chytová	chytový	k2eAgFnSc1d1	chytový
–	–	k?	–
uzavírací	uzavírací	k2eAgFnSc1d1	uzavírací
–	–	k?	–
nanášecí	nanášecí	k2eAgFnSc1d1	nanášecí
–	–	k?	–
zatahovací	zatahovací	k2eAgFnSc1d1	zatahovací
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vybavení	vybavení	k1gNnSc1	vybavení
stroje	stroj	k1gInSc2	stroj
==	==	k?	==
</s>
</p>
<p>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
jednolícní	jednolícní	k2eAgInPc1d1	jednolícní
stroje	stroj	k1gInPc1	stroj
s	s	k7c7	s
jedním	jeden	k4xCgNnSc7	jeden
jehlovým	jehlový	k2eAgNnSc7d1	jehlové
lůžkem	lůžko	k1gNnSc7	lůžko
<g/>
,	,	kIx,	,
oboulícní	oboulícní	k2eAgFnSc7d1	oboulícní
se	s	k7c7	s
dvěma	dva	k4xCgNnPc7	dva
lůžky	lůžko	k1gNnPc7	lůžko
postavenými	postavený	k2eAgMnPc7d1	postavený
vzájemně	vzájemně	k6eAd1	vzájemně
v	v	k7c6	v
</s>
</p>
<p>
<s>
pravém	pravý	k2eAgInSc6d1	pravý
úhlu	úhel	k1gInSc6	úhel
a	a	k8xC	a
obourubní	obourubní	k2eAgFnSc4d1	obourubní
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
lůžky	lůžko	k1gNnPc7	lůžko
umístěnými	umístěný	k2eAgInPc7d1	umístěný
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
rovině	rovina	k1gFnSc6	rovina
opatřenými	opatřený	k2eAgFnPc7d1	opatřená
dvojjazýčkovými	dvojjazýčkový	k2eAgFnPc7d1	dvojjazýčkový
jehlami	jehla	k1gFnPc7	jehla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pracovní	pracovní	k2eAgFnSc1d1	pracovní
šíře	šíře	k1gFnSc1	šíře
je	být	k5eAaImIp3nS	být
60-305	[number]	k4	60-305
cm	cm	kA	cm
<g/>
,	,	kIx,	,
jemnost	jemnost	k1gFnSc1	jemnost
0,8	[number]	k4	0,8
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
jehel	jehla	k1gFnPc2	jehla
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
pletení	pletení	k1gNnSc2	pletení
je	být	k5eAaImIp3nS	být
1,3	[number]	k4	1,3
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
vt	vt	k?	vt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
dvoulůžkových	dvoulůžkový	k2eAgInPc6d1	dvoulůžkový
strojích	stroj	k1gInPc6	stroj
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
vyrábět	vyrábět	k5eAaImF	vyrábět
pleteniny	pletenina	k1gFnPc4	pletenina
v	v	k7c6	v
prakticky	prakticky	k6eAd1	prakticky
všech	všecek	k3xTgFnPc6	všecek
známých	známý	k2eAgFnPc6d1	známá
vazbách	vazba	k1gFnPc6	vazba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
všemi	všecek	k3xTgFnPc7	všecek
kombinacemi	kombinace	k1gFnPc7	kombinace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
vzorovacím	vzorovací	k2eAgNnPc3d1	vzorovací
zařízením	zařízení	k1gNnPc3	zařízení
patří	patřit	k5eAaImIp3nS	patřit
zejména	zejména	k9	zejména
ovládání	ovládání	k1gNnSc1	ovládání
zámků	zámek	k1gInPc2	zámek
(	(	kIx(	(
<g/>
skupinová	skupinový	k2eAgFnSc1d1	skupinová
nebo	nebo	k8xC	nebo
individuální	individuální	k2eAgFnSc1d1	individuální
volba	volba	k1gFnSc1	volba
jehel	jehla	k1gFnPc2	jehla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posunování	posunování	k1gNnSc1	posunování
jehelního	jehelní	k2eAgNnSc2d1	jehelní
lůžka	lůžko	k1gNnSc2	lůžko
<g/>
,	,	kIx,	,
převěšování	převěšování	k1gNnSc1	převěšování
oček	očko	k1gNnPc2	očko
<g/>
,	,	kIx,	,
záměna	záměna	k1gFnSc1	záměna
pletacích	pletací	k2eAgFnPc2d1	pletací
nití	nit	k1gFnPc2	nit
atd.	atd.	kA	atd.
<g/>
Ke	k	k7c3	k
zhotovení	zhotovení	k1gNnSc3	zhotovení
plyšové	plyšový	k2eAgFnSc2d1	plyšová
pleteniny	pletenina	k1gFnSc2	pletenina
a	a	k8xC	a
pro	pro	k7c4	pro
pleteniny	pletenina	k1gFnPc4	pletenina
s	s	k7c7	s
útkem	útek	k1gInSc7	útek
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
plošném	plošný	k2eAgInSc6d1	plošný
stroji	stroj	k1gInSc6	stroj
instalovány	instalovat	k5eAaBmNgFnP	instalovat
nejméně	málo	k6eAd3	málo
dva	dva	k4xCgInPc4	dva
vodiče	vodič	k1gInPc4	vodič
příze	příz	k1gFnSc2	příz
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
krycí	krycí	k2eAgInPc4d1	krycí
vzory	vzor	k1gInPc4	vzor
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
vodič	vodič	k1gInSc1	vodič
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
prochází	procházet	k5eAaImIp3nS	procházet
základní	základní	k2eAgFnSc1d1	základní
a	a	k8xC	a
krycí	krycí	k2eAgFnSc1d1	krycí
nit	nit	k1gFnSc1	nit
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stroje	stroj	k1gInPc4	stroj
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
žakárovým	žakárový	k2eAgNnSc7d1	žakárové
ústrojím	ústrojí	k1gNnSc7	ústrojí
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
kovový	kovový	k2eAgInSc1d1	kovový
pás	pás	k1gInSc1	pás
s	s	k7c7	s
výstupky	výstupek	k1gInPc7	výstupek
a	a	k8xC	a
děrováním	děrování	k1gNnSc7	děrování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
programuje	programovat	k5eAaImIp3nS	programovat
a	a	k8xC	a
ovládá	ovládat	k5eAaImIp3nS	ovládat
přes	přes	k7c4	přes
počítač	počítač	k1gInSc4	počítač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
stroje	stroj	k1gInPc1	stroj
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
specializované	specializovaný	k2eAgFnPc1d1	specializovaná
na	na	k7c4	na
pleteniny	pletenina	k1gFnPc4	pletenina
s	s	k7c7	s
určitým	určitý	k2eAgNnSc7d1	určité
vzorováním	vzorování	k1gNnSc7	vzorování
na	na	k7c4	na
intarziové	intarziový	k2eAgFnPc4d1	intarziový
pleteniny	pletenina	k1gFnPc4	pletenina
<g/>
,	,	kIx,	,
na	na	k7c4	na
hotové	hotový	k2eAgInPc4d1	hotový
svrchní	svrchní	k2eAgInPc4d1	svrchní
oděvy	oděv	k1gInPc4	oděv
fully	fulla	k1gFnSc2	fulla
fashioned	fashioned	k1gInSc1	fashioned
(	(	kIx(	(
<g/>
úplety	úplet	k1gInPc4	úplet
bez	bez	k7c2	bez
sešívání	sešívání	k1gNnSc2	sešívání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rukavice	rukavice	k1gFnSc1	rukavice
<g/>
,	,	kIx,	,
ponožky	ponožka	k1gFnPc1	ponožka
<g/>
,	,	kIx,	,
stuhy	stuha	k1gFnPc1	stuha
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
K	k	k7c3	k
amatérskému	amatérský	k2eAgNnSc3d1	amatérské
pletení	pletení	k1gNnSc3	pletení
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
ploché	plochý	k2eAgInPc1d1	plochý
stroje	stroj	k1gInPc1	stroj
s	s	k7c7	s
ručním	ruční	k2eAgInSc7d1	ruční
pohonem	pohon	k1gInSc7	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ruční	ruční	k2eAgInSc1d1	ruční
pletací	pletací	k2eAgInSc1d1	pletací
stroj	stroj	k1gInSc1	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Ruční	ruční	k2eAgInSc1d1	ruční
pletací	pletací	k2eAgInSc1d1	pletací
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
Okrouhlé	okrouhlý	k2eAgInPc1d1	okrouhlý
pletací	pletací	k2eAgInPc1d1	pletací
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
Pletenina	pletenina	k1gFnSc1	pletenina
<g/>
,	,	kIx,	,
Pletací	pletací	k2eAgInSc1d1	pletací
systém	systém	k1gInSc1	systém
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Příručka	příručka	k1gFnSc1	příručka
textilního	textilní	k2eAgMnSc2d1	textilní
odborníka	odborník	k1gMnSc2	odborník
(	(	kIx(	(
<g/>
SNTL	SNTL	kA	SNTL
Praha	Praha	k1gFnSc1	Praha
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
740-750	[number]	k4	740-750
</s>
</p>
<p>
<s>
Denninger	Denninger	k1gInSc1	Denninger
<g/>
/	/	kIx~	/
<g/>
Giese	Giese	k1gFnSc1	Giese
<g/>
:	:	kIx,	:
Textil	textil	k1gInSc1	textil
und	und	k?	und
Modelexikon	Modelexikon	k1gInSc1	Modelexikon
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-87150-848-9	[number]	k4	3-87150-848-9
Deutscher	Deutschra	k1gFnPc2	Deutschra
Fachverlag	Fachverlaga	k1gFnPc2	Fachverlaga
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
<g/>
/	/	kIx~	/
<g/>
Main	Main	k1gInSc1	Main
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
234-	[number]	k4	234-
236	[number]	k4	236
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
