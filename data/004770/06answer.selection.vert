<s>
Oběšení	oběšení	k1gNnSc1	oběšení
je	být	k5eAaImIp3nS	být
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
způsob	způsob	k1gInSc4	způsob
popravy	poprava	k1gFnSc2	poprava
a	a	k8xC	a
sebevraždy	sebevražda	k1gFnSc2	sebevražda
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
vraždy	vražda	k1gFnSc2	vražda
a	a	k8xC	a
vzácně	vzácně	k6eAd1	vzácně
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
nehody	nehoda	k1gFnSc2	nehoda
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
též	tenž	k3xDgFnSc2	tenž
náboženské	náboženský	k2eAgFnSc2d1	náboženská
oběti	oběť	k1gFnSc2	oběť
<g/>
.	.	kIx.	.
</s>
