<s>
Oběšení	oběšení	k1gNnSc1	oběšení
je	být	k5eAaImIp3nS	být
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
způsob	způsob	k1gInSc4	způsob
popravy	poprava	k1gFnSc2	poprava
a	a	k8xC	a
sebevraždy	sebevražda	k1gFnSc2	sebevražda
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
vraždy	vražda	k1gFnSc2	vražda
a	a	k8xC	a
vzácně	vzácně	k6eAd1	vzácně
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
nehody	nehoda	k1gFnSc2	nehoda
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
též	tenž	k3xDgFnSc2	tenž
náboženské	náboženský	k2eAgFnSc2d1	náboženská
oběti	oběť	k1gFnSc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
poprava	poprava	k1gFnSc1	poprava
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
historických	historický	k2eAgFnPc6d1	historická
kulturách	kultura	k1gFnPc6	kultura
už	už	k6eAd1	už
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
vznešený	vznešený	k2eAgInSc4d1	vznešený
<g/>
,	,	kIx,	,
aristokratický	aristokratický	k2eAgInSc4d1	aristokratický
způsob	způsob	k1gInSc4	způsob
popravy	poprava	k1gFnSc2	poprava
(	(	kIx(	(
<g/>
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
staří	starý	k2eAgMnPc1d1	starý
Germáni	Germán	k1gMnPc1	Germán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
rituální	rituální	k2eAgFnSc7d1	rituální
sebevraždou	sebevražda	k1gFnSc7	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Evropě	Evropa	k1gFnSc6	Evropa
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
naopak	naopak	k6eAd1	naopak
potupný	potupný	k2eAgInSc1d1	potupný
způsob	způsob	k1gInSc1	způsob
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
pro	pro	k7c4	pro
zločince	zločinec	k1gMnPc4	zločinec
z	z	k7c2	z
nízkých	nízký	k2eAgFnPc2d1	nízká
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Pravěcí	pravěký	k2eAgMnPc1d1	pravěký
Evropané	Evropan	k1gMnPc1	Evropan
často	často	k6eAd1	často
oběšením	oběšení	k1gNnPc3	oběšení
obětovali	obětovat	k5eAaBmAgMnP	obětovat
bohům	bůh	k1gMnPc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
Keltové	Kelt	k1gMnPc1	Kelt
věšeli	věšet	k5eAaImAgMnP	věšet
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
oběti	oběť	k1gFnSc2	oběť
Esusovi	Esus	k1gMnSc3	Esus
<g/>
,	,	kIx,	,
bohu	bůh	k1gMnSc3	bůh
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
je	on	k3xPp3gFnPc4	on
tak	tak	k9	tak
mohl	moct	k5eAaImAgInS	moct
náležitě	náležitě	k6eAd1	náležitě
pohoupat	pohoupat	k5eAaPmF	pohoupat
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Germáni	Germán	k1gMnPc1	Germán
a	a	k8xC	a
Vikingové	Viking	k1gMnPc1	Viking
oběšením	oběšení	k1gNnPc3	oběšení
vzdávali	vzdávat	k5eAaImAgMnP	vzdávat
úctu	úcta	k1gFnSc4	úcta
bohu	bůh	k1gMnSc3	bůh
Ódinovi	Ódin	k1gMnSc3	Ódin
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
také	také	k9	také
sám	sám	k3xTgMnSc1	sám
oběsil	oběsit	k5eAaPmAgInS	oběsit
na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
stromě	strom	k1gInSc6	strom
Yggdrasilu	Yggdrasil	k1gInSc2	Yggdrasil
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
osvícení	osvícení	k1gNnSc4	osvícení
<g/>
.	.	kIx.	.
</s>
<s>
Oběšení	oběšení	k1gNnSc1	oběšení
je	být	k5eAaImIp3nS	být
obvyklým	obvyklý	k2eAgInSc7d1	obvyklý
způsobem	způsob	k1gInSc7	způsob
sebevraždy	sebevražda	k1gFnSc2	sebevražda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
(	(	kIx(	(
<g/>
ženy	žena	k1gFnPc1	žena
častěji	často	k6eAd2	často
volí	volit	k5eAaImIp3nP	volit
nějakou	nějaký	k3yIgFnSc4	nějaký
formu	forma	k1gFnSc4	forma
otravy	otrava	k1gFnSc2	otrava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oběšení	oběšení	k1gNnSc1	oběšení
je	být	k5eAaImIp3nS	být
také	také	k9	také
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejčastějších	častý	k2eAgInPc2d3	nejčastější
způsobů	způsob	k1gInPc2	způsob
sebevraždy	sebevražda	k1gFnSc2	sebevražda
ve	v	k7c6	v
věznicích	věznice	k1gFnPc6	věznice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
věznicích	věznice	k1gFnPc6	věznice
skončili	skončit	k5eAaPmAgMnP	skončit
své	svůj	k3xOyFgInPc4	svůj
životy	život	k1gInPc4	život
oběšením	oběšení	k1gNnSc7	oběšení
např.	např.	kA	např.
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Malý	Malý	k1gMnSc1	Malý
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Večeřa	Večeřa	k1gMnSc1	Večeřa
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Kopáč	kopáč	k1gMnSc1	kopáč
či	či	k8xC	či
Otakar	Otakar	k1gMnSc1	Otakar
Tomek	Tomek	k1gMnSc1	Tomek
<g/>
.	.	kIx.	.
</s>
<s>
Oběšení	oběšení	k1gNnSc1	oběšení
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
častým	častý	k2eAgInSc7d1	častý
způsobem	způsob	k1gInSc7	způsob
rituální	rituální	k2eAgFnSc2d1	rituální
sebevraždy	sebevražda	k1gFnSc2	sebevražda
vdov	vdova	k1gFnPc2	vdova
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
starých	starý	k2eAgInPc2d1	starý
Achájů	Acháj	k1gInPc2	Acháj
<g/>
,	,	kIx,	,
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
,	,	kIx,	,
u	u	k7c2	u
Germánů	Germán	k1gMnPc2	Germán
(	(	kIx(	(
<g/>
Prokopios	Prokopios	k1gMnSc1	Prokopios
z	z	k7c2	z
Kaisareie	Kaisareie	k1gFnSc2	Kaisareie
tento	tento	k3xDgInSc1	tento
zvyk	zvyk	k1gInSc1	zvyk
dosvědčuje	dosvědčovat	k5eAaImIp3nS	dosvědčovat
u	u	k7c2	u
Herulů	Herul	k1gInPc2	Herul
<g/>
,	,	kIx,	,
usazených	usazený	k2eAgFnPc2d1	usazená
tehdy	tehdy	k6eAd1	tehdy
i	i	k9	i
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
až	až	k9	až
do	do	k7c2	do
novověku	novověk	k1gInSc2	novověk
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
zvyk	zvyk	k1gInSc4	zvyk
zachoval	zachovat	k5eAaPmAgMnS	zachovat
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc4	způsob
provedení	provedení	k1gNnSc3	provedení
poprav	poprava	k1gFnPc2	poprava
ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
a	a	k8xC	a
raně	raně	k6eAd1	raně
novověké	novověký	k2eAgFnSc6d1	novověká
Evropě	Evropa	k1gFnSc6	Evropa
byl	být	k5eAaImAgInS	být
různý	různý	k2eAgInSc1d1	různý
<g/>
,	,	kIx,	,
od	od	k7c2	od
prostého	prostý	k2eAgNnSc2d1	prosté
vytažení	vytažení	k1gNnSc2	vytažení
na	na	k7c4	na
strom	strom	k1gInSc4	strom
po	po	k7c6	po
použití	použití	k1gNnSc6	použití
speciálně	speciálně	k6eAd1	speciálně
postavené	postavený	k2eAgFnSc2d1	postavená
šibenice	šibenice	k1gFnSc2	šibenice
<g/>
,	,	kIx,	,
obyčejně	obyčejně	k6eAd1	obyčejně
na	na	k7c6	na
veřejném	veřejný	k2eAgInSc6d1	veřejný
<g/>
,	,	kIx,	,
zdálky	zdálky	k6eAd1	zdálky
viditelném	viditelný	k2eAgNnSc6d1	viditelné
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
šibeniční	šibeniční	k2eAgInSc1d1	šibeniční
vrch	vrch	k1gInSc1	vrch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odsouzenec	odsouzenec	k1gMnSc1	odsouzenec
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
umíral	umírat	k5eAaImAgInS	umírat
pomalým	pomalý	k2eAgNnSc7d1	pomalé
zadušením	zadušení	k1gNnSc7	zadušení
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
byla	být	k5eAaImAgFnS	být
dokonce	dokonce	k9	dokonce
místo	místo	k7c2	místo
provazu	provaz	k1gInSc2	provaz
použita	použit	k2eAgFnSc1d1	použita
houžev	houžev	k1gFnSc1	houžev
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
co	co	k9	co
nejdelší	dlouhý	k2eAgFnSc2d3	nejdelší
agonie	agonie	k1gFnSc2	agonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
období	období	k1gNnSc6	období
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
popravovat	popravovat	k5eAaImF	popravovat
oběšením	oběšení	k1gNnSc7	oběšení
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nechutným	chutný	k2eNgInPc3d1	nechutný
projevům	projev	k1gInPc3	projev
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
udušení	udušení	k1gNnSc1	udušení
v	v	k7c6	v
oprátce	oprátka	k1gFnSc6	oprátka
provázely	provázet	k5eAaImAgFnP	provázet
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc6	Rakousku-Uhersek
zavedeno	zaveden	k2eAgNnSc4d1	zavedeno
speciální	speciální	k2eAgNnSc4d1	speciální
zařízení	zařízení	k1gNnSc4	zařízení
na	na	k7c4	na
vykonání	vykonání	k1gNnSc4	vykonání
trestu	trest	k1gInSc2	trest
oběšení	oběšení	k1gNnSc2	oběšení
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Würgegalgen	Würgegalgen	k1gInSc1	Würgegalgen
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
doslova	doslova	k6eAd1	doslova
škrtící	škrtící	k2eAgFnSc1d1	škrtící
šibenice	šibenice	k1gFnSc1	šibenice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
popravčí	popravčí	k2eAgNnSc1d1	popravčí
prkno	prkno	k1gNnSc1	prkno
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
součástí	součást	k1gFnSc7	součást
byla	být	k5eAaImAgFnS	být
bytelná	bytelný	k2eAgFnSc1d1	bytelná
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
fošna	fošna	k1gFnSc1	fošna
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
trám	trám	k1gInSc1	trám
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
sloup	sloup	k1gInSc1	sloup
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
3-5	[number]	k4	3-5
metrů	metr	k1gInPc2	metr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
upevněné	upevněný	k2eAgNnSc1d1	upevněné
ve	v	k7c6	v
svislé	svislý	k2eAgFnSc6d1	svislá
poloze	poloha	k1gFnSc6	poloha
například	například	k6eAd1	například
zapuštěním	zapuštění	k1gNnSc7	zapuštění
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
prkna	prkno	k1gNnSc2	prkno
byl	být	k5eAaImAgInS	být
upevněn	upevněn	k2eAgInSc1d1	upevněn
hák	hák	k1gInSc1	hák
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
byla	být	k5eAaImAgFnS	být
přivázána	přivázán	k2eAgFnSc1d1	přivázána
škrtící	škrtící	k2eAgFnSc1d1	škrtící
smyčka	smyčka	k1gFnSc1	smyčka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
prkna	prkno	k1gNnSc2	prkno
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
horní	horní	k2eAgFnSc6d1	horní
hraně	hrana	k1gFnSc6	hrana
ještě	ještě	k9	ještě
kladka	kladka	k1gFnSc1	kladka
s	s	k7c7	s
provazem	provaz	k1gInSc7	provaz
<g/>
.	.	kIx.	.
</s>
<s>
Odsouzenec	odsouzenec	k1gMnSc1	odsouzenec
byl	být	k5eAaImAgMnS	být
spoután	spoutat	k5eAaPmNgMnS	spoutat
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
hrudníku	hrudník	k1gInSc2	hrudník
opásán	opásán	k2eAgInSc4d1	opásán
pevným	pevný	k2eAgInSc7d1	pevný
popruhem	popruh	k1gInSc7	popruh
a	a	k8xC	a
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
provazem	provaz	k1gInSc7	provaz
<g/>
,	,	kIx,	,
provlečeným	provlečený	k2eAgNnSc7d1	provlečené
kladkou	kladka	k1gFnSc7	kladka
<g/>
,	,	kIx,	,
vytažen	vytažen	k2eAgMnSc1d1	vytažen
k	k	k7c3	k
hornímu	horní	k2eAgInSc3d1	horní
konci	konec	k1gInSc3	konec
prkna	prkno	k1gNnSc2	prkno
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
mu	on	k3xPp3gNnSc3	on
kat	kat	k1gInSc4	kat
stojící	stojící	k2eAgFnSc4d1	stojící
na	na	k7c6	na
žebříku	žebřík	k1gInSc6	žebřík
na	na	k7c4	na
krk	krk	k1gInSc4	krk
nasadil	nasadit	k5eAaPmAgMnS	nasadit
smyčku	smyčka	k1gFnSc4	smyčka
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
provazem	provaz	k1gInSc7	provaz
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
popravovaný	popravovaný	k2eAgInSc4d1	popravovaný
spoután	spoután	k2eAgInSc4d1	spoután
na	na	k7c6	na
kotnících	kotník	k1gInPc6	kotník
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
složitějších	složitý	k2eAgNnPc2d2	složitější
zařízení	zařízení	k1gNnPc2	zařízení
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
prkna	prkno	k1gNnSc2	prkno
další	další	k2eAgFnSc1d1	další
kladka	kladka	k1gFnSc1	kladka
s	s	k7c7	s
provazem	provaz	k1gInSc7	provaz
připoutaným	připoutaný	k2eAgInSc7d1	připoutaný
ke	k	k7c3	k
kotníkům	kotník	k1gInPc3	kotník
odsouzeného	odsouzený	k2eAgNnSc2d1	odsouzené
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
potom	potom	k6eAd1	potom
kat	kat	k1gMnSc1	kat
povolil	povolit	k5eAaPmAgMnS	povolit
lano	lano	k1gNnSc1	lano
provlečené	provlečený	k2eAgNnSc1d1	provlečené
kladkou	kladka	k1gFnSc7	kladka
<g/>
,	,	kIx,	,
poklesl	poklesnout	k5eAaPmAgMnS	poklesnout
prudce	prudko	k6eAd1	prudko
odsouzenec	odsouzenec	k1gMnSc1	odsouzenec
a	a	k8xC	a
smyčka	smyčka	k1gFnSc1	smyčka
stahující	stahující	k2eAgFnSc1d1	stahující
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
jeho	jeho	k3xOp3gInSc2	jeho
krku	krk	k1gInSc2	krk
ho	on	k3xPp3gInSc4	on
zbavila	zbavit	k5eAaPmAgFnS	zbavit
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
umírání	umírání	k1gNnSc1	umírání
rychlejší	rychlý	k2eAgNnSc1d2	rychlejší
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
ještě	ještě	k9	ještě
katovi	katův	k2eAgMnPc1d1	katův
pomocníci	pomocník	k1gMnPc1	pomocník
táhnout	táhnout	k5eAaImF	táhnout
přes	přes	k7c4	přes
spodní	spodní	k2eAgFnSc4d1	spodní
kladku	kladka	k1gFnSc4	kladka
za	za	k7c4	za
provaz	provaz	k1gInSc4	provaz
upevněný	upevněný	k2eAgInSc4d1	upevněný
na	na	k7c6	na
kotnících	kotník	k1gInPc6	kotník
a	a	k8xC	a
zkrátit	zkrátit	k5eAaPmF	zkrátit
tak	tak	k6eAd1	tak
popravovanému	popravovaný	k2eAgNnSc3d1	popravovaný
utrpení	utrpení	k1gNnSc3	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
popravy	poprava	k1gFnSc2	poprava
přejaly	přejmout	k5eAaPmAgInP	přejmout
nástupnické	nástupnický	k2eAgInPc1d1	nástupnický
státy	stát	k1gInPc1	stát
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
i	i	k8xC	i
Československo	Československo	k1gNnSc1	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byli	být	k5eAaImAgMnP	být
takto	takto	k6eAd1	takto
popraveni	popraven	k2eAgMnPc1d1	popraven
např.	např.	kA	např.
Martin	Martin	k1gMnSc1	Martin
Lecián	Lecián	k1gMnSc1	Lecián
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Hermann	Hermann	k1gMnSc1	Hermann
Frank	Frank	k1gMnSc1	Frank
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Čurda	Čurda	k1gMnSc1	Čurda
<g/>
,	,	kIx,	,
JUDr.	JUDr.	kA	JUDr.
Milada	Milada	k1gFnSc1	Milada
Horáková	Horáková	k1gFnSc1	Horáková
<g/>
,	,	kIx,	,
mjr.	mjr.	kA	mjr.
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nechanský	Nechanský	k2eAgMnSc1d1	Nechanský
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Slánský	Slánský	k1gMnSc1	Slánský
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
Janda	Janda	k1gMnSc1	Janda
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Buchal	buchat	k5eAaImAgMnS	buchat
a	a	k8xC	a
stovky	stovka	k1gFnPc4	stovka
dalších	další	k2eAgFnPc2d1	další
až	až	k8xS	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
bylo	být	k5eAaImAgNnS	být
užíváno	užíván	k2eAgNnSc1d1	užíváno
zařízení	zařízení	k1gNnSc1	zařízení
na	na	k7c6	na
podobném	podobný	k2eAgInSc6d1	podobný
principu	princip	k1gInSc6	princip
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
šlo	jít	k5eAaImAgNnS	jít
pořád	pořád	k6eAd1	pořád
o	o	k7c4	o
škrtící	škrtící	k2eAgFnSc4d1	škrtící
smyčku	smyčka	k1gFnSc4	smyčka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ale	ale	k9	ale
byla	být	k5eAaImAgFnS	být
upevněna	upevnit	k5eAaPmNgFnS	upevnit
hákem	hák	k1gInSc7	hák
na	na	k7c6	na
stěně	stěna	k1gFnSc6	stěna
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
popravy	poprava	k1gFnPc1	poprava
se	se	k3xPyFc4	se
vykonávaly	vykonávat	k5eAaImAgFnP	vykonávat
už	už	k6eAd1	už
jen	jen	k9	jen
na	na	k7c6	na
jediném	jediný	k2eAgNnSc6d1	jediné
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
mobilní	mobilní	k2eAgNnSc4d1	mobilní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1713	[number]	k4	1713
byl	být	k5eAaImAgMnS	být
legendární	legendární	k2eAgMnSc1d1	legendární
slovenský	slovenský	k2eAgMnSc1d1	slovenský
zbojník	zbojník	k1gMnSc1	zbojník
Juraj	Juraj	k1gMnSc1	Juraj
Jánošík	Jánošík	k1gMnSc1	Jánošík
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
zavěšením	zavěšení	k1gNnSc7	zavěšení
za	za	k7c4	za
levá	levý	k2eAgNnPc4d1	levé
žebra	žebro	k1gNnPc4	žebro
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
ponechán	ponechán	k2eAgInSc1d1	ponechán
pomalé	pomalý	k2eAgFnSc3d1	pomalá
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Postup	postup	k1gInSc1	postup
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgFnSc1d1	následující
(	(	kIx(	(
<g/>
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
státech	stát	k1gInPc6	stát
USA	USA	kA	USA
trochu	trochu	k6eAd1	trochu
lišit	lišit	k5eAaImF	lišit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
před	před	k7c7	před
popravou	poprava	k1gFnSc7	poprava
je	být	k5eAaImIp3nS	být
odsouzenec	odsouzenec	k1gMnSc1	odsouzenec
zvážen	zvážen	k2eAgMnSc1d1	zvážen
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
provedena	provést	k5eAaPmNgFnS	provést
zkouška	zkouška	k1gFnSc1	zkouška
s	s	k7c7	s
pytlem	pytel	k1gInSc7	pytel
písku	písek	k1gInSc2	písek
o	o	k7c6	o
stejné	stejný	k2eAgFnSc6d1	stejná
hmotnosti	hmotnost	k1gFnSc6	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
nezbytné	nezbytný	k2eAgFnSc2d1	nezbytná
délky	délka	k1gFnSc2	délka
pádu	pád	k1gInSc2	pád
(	(	kIx(	(
<g/>
provazu	provaz	k1gInSc2	provaz
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
rychlé	rychlý	k2eAgFnSc2d1	rychlá
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
provaz	provaz	k1gInSc4	provaz
příliš	příliš	k6eAd1	příliš
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vězeň	vězeň	k1gMnSc1	vězeň
dekapitován	dekapitován	k2eAgMnSc1d1	dekapitován
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
provaz	provaz	k1gInSc4	provaz
příliš	příliš	k6eAd1	příliš
krátký	krátký	k2eAgInSc4d1	krátký
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
vězeň	vězeň	k1gMnSc1	vězeň
dusit	dusit	k5eAaImF	dusit
až	až	k9	až
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Provaz	provaz	k1gInSc1	provaz
(	(	kIx(	(
<g/>
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
cm	cm	kA	cm
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
)	)	kIx)	)
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dostatečně	dostatečně	k6eAd1	dostatečně
natažen	natažen	k2eAgMnSc1d1	natažen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nepružil	pružit	k5eNaImAgMnS	pružit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
hladkého	hladký	k2eAgInSc2d1	hladký
průběhu	průběh	k1gInSc2	průběh
je	být	k5eAaImIp3nS	být
uzel	uzel	k1gInSc1	uzel
u	u	k7c2	u
smyčky	smyčka	k1gFnSc2	smyčka
(	(	kIx(	(
<g/>
oprátky	oprátka	k1gFnSc2	oprátka
<g/>
)	)	kIx)	)
namazán	namazat	k5eAaPmNgMnS	namazat
mýdlem	mýdlo	k1gNnSc7	mýdlo
nebo	nebo	k8xC	nebo
voskem	vosk	k1gInSc7	vosk
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
popravou	poprava	k1gFnSc7	poprava
jsou	být	k5eAaImIp3nP	být
vězňovi	vězňův	k2eAgMnPc1d1	vězňův
zabezpečeny	zabezpečen	k2eAgInPc1d1	zabezpečen
ruce	ruka	k1gFnPc4	ruka
a	a	k8xC	a
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
zavázány	zavázán	k2eAgFnPc4d1	zavázána
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
kolem	kolem	k7c2	kolem
krku	krk	k1gInSc2	krk
umístěna	umístit	k5eAaPmNgFnS	umístit
smyčka	smyčka	k1gFnSc1	smyčka
(	(	kIx(	(
<g/>
uzel	uzel	k1gInSc1	uzel
za	za	k7c7	za
levým	levý	k2eAgNnSc7d1	levé
uchem	ucho	k1gNnSc7	ucho
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
poklop	poklop	k1gInSc1	poklop
a	a	k8xC	a
vězeň	vězeň	k1gMnSc1	vězeň
se	se	k3xPyFc4	se
propadne	propadnout	k5eAaPmIp3nS	propadnout
<g/>
.	.	kIx.	.
</s>
<s>
Vězňova	vězňův	k2eAgFnSc1d1	vězňova
hmotnost	hmotnost	k1gFnSc1	hmotnost
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
rychle	rychle	k6eAd1	rychle
způsobit	způsobit	k5eAaPmF	způsobit
zlomení	zlomení	k1gNnSc4	zlomení
krčních	krční	k2eAgInPc2d1	krční
obratlů	obratel	k1gInPc2	obratel
a	a	k8xC	a
následnou	následný	k2eAgFnSc4d1	následná
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
vězeň	vězeň	k1gMnSc1	vězeň
silné	silný	k2eAgInPc4d1	silný
krční	krční	k2eAgInPc4d1	krční
svaly	sval	k1gInPc4	sval
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
nízkou	nízký	k2eAgFnSc4d1	nízká
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
pád	pád	k1gInSc1	pád
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
krátký	krátký	k2eAgInSc1d1	krátký
nebo	nebo	k8xC	nebo
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
smyčka	smyčka	k1gFnSc1	smyčka
špatně	špatně	k6eAd1	špatně
upevněna	upevněn	k2eAgFnSc1d1	upevněna
<g/>
,	,	kIx,	,
zlomení	zlomení	k1gNnSc1	zlomení
krčních	krční	k2eAgInPc2d1	krční
obratlů	obratel	k1gInPc2	obratel
není	být	k5eNaImIp3nS	být
rychlé	rychlý	k2eAgNnSc1d1	rychlé
a	a	k8xC	a
smrt	smrt	k1gFnSc1	smrt
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
zadušením	zadušení	k1gNnSc7	zadušení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
nastane	nastat	k5eAaPmIp3nS	nastat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
poprava	poprava	k1gFnSc1	poprava
doprovázena	doprovázen	k2eAgFnSc1d1	doprovázena
defekací	defekace	k1gFnSc7	defekace
a	a	k8xC	a
vystrčením	vystrčení	k1gNnSc7	vystrčení
jazyka	jazyk	k1gInSc2	jazyk
<g/>
;	;	kIx,	;
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgInPc1d1	patrný
prudké	prudký	k2eAgInPc1d1	prudký
vězňovy	vězňův	k2eAgInPc1d1	vězňův
pohyby	pohyb	k1gInPc1	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
země	zem	k1gFnPc1	zem
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Írán	Írán	k1gInSc1	Írán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
používán	používán	k2eAgInSc4d1	používán
"	"	kIx"	"
<g/>
zastaralý	zastaralý	k2eAgInSc4d1	zastaralý
<g/>
"	"	kIx"	"
způsob	způsob	k1gInSc4	způsob
popravy	poprava	k1gFnSc2	poprava
oběšením	oběšení	k1gNnSc7	oběšení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
nenastavuje	nastavovat	k5eNaImIp3nS	nastavovat
délka	délka	k1gFnSc1	délka
provazu	provaz	k1gInSc2	provaz
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
krátký	krátký	k2eAgInSc4d1	krátký
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
zlomení	zlomení	k1gNnSc2	zlomení
obratlů	obratel	k1gInPc2	obratel
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
nepoužívá	používat	k5eNaImIp3nS	používat
metoda	metoda	k1gFnSc1	metoda
pádu	pád	k1gInSc2	pád
a	a	k8xC	a
odsouzenec	odsouzenec	k1gMnSc1	odsouzenec
je	být	k5eAaImIp3nS	být
vytažen	vytáhnout	k5eAaPmNgMnS	vytáhnout
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
pomalu	pomalu	k6eAd1	pomalu
jeřábem	jeřáb	k1gInSc7	jeřáb
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
způsobena	způsobit	k5eAaPmNgFnS	způsobit
zadušením	zadušení	k1gNnSc7	zadušení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soudním	soudní	k2eAgNnSc6d1	soudní
lékařství	lékařství	k1gNnSc6	lékařství
je	být	k5eAaImIp3nS	být
oběšení	oběšení	k1gNnSc4	oběšení
definováno	definovat	k5eAaBmNgNnS	definovat
jako	jako	k8xC	jako
strangulace	strangulace	k1gFnSc1	strangulace
(	(	kIx(	(
<g/>
zaškrcení	zaškrcený	k2eAgMnPc1d1	zaškrcený
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
škrtidlo	škrtidlo	k1gNnSc1	škrtidlo
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
koncem	konec	k1gInSc7	konec
upevněným	upevněný	k2eAgInSc7d1	upevněný
k	k	k7c3	k
nějakému	nějaký	k3yIgInSc3	nějaký
předmětu	předmět	k1gInSc3	předmět
a	a	k8xC	a
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
obtočené	obtočený	k2eAgFnSc2d1	obtočená
kolem	kolem	k7c2	kolem
krku	krk	k1gInSc2	krk
zadrhne	zadrhnout	k5eAaPmIp3nS	zadrhnout
vahou	váha	k1gFnSc7	váha
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úspěšnému	úspěšný	k2eAgNnSc3d1	úspěšné
oběšení	oběšení	k1gNnSc3	oběšení
postačuje	postačovat	k5eAaImIp3nS	postačovat
jen	jen	k9	jen
zhruba	zhruba	k6eAd1	zhruba
váha	váha	k1gFnSc1	váha
5	[number]	k4	5
kg	kg	kA	kg
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
i	i	k9	i
oběšení	oběšení	k1gNnSc1	oběšení
vkleče	vkleče	k6eAd1	vkleče
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
vleže	vleže	k6eAd1	vleže
<g/>
.	.	kIx.	.
</s>
<s>
Škrtidlo	škrtidlo	k1gNnSc1	škrtidlo
nebývá	bývat	k5eNaImIp3nS	bývat
kolem	kolem	k7c2	kolem
celého	celý	k2eAgInSc2d1	celý
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
postačuje	postačovat	k5eAaImIp3nS	postačovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
jde	jít	k5eAaImIp3nS	jít
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Strangulační	strangulační	k2eAgFnSc1d1	strangulační
rýha	rýha	k1gFnSc1	rýha
proto	proto	k8xC	proto
nebývá	bývat	k5eNaImIp3nS	bývat
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
<g/>
,	,	kIx,	,
mívá	mívat	k5eAaImIp3nS	mívat
šikmý	šikmý	k2eAgInSc4d1	šikmý
průběh	průběh	k1gInSc4	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
při	při	k7c6	při
oběšení	oběšení	k1gNnSc6	oběšení
nastává	nastávat	k5eAaImIp3nS	nastávat
spolupůsobením	spolupůsobení	k1gNnSc7	spolupůsobení
několika	několik	k4yIc2	několik
faktorů	faktor	k1gInPc2	faktor
<g/>
:	:	kIx,	:
podráždění	podráždění	k1gNnSc2	podráždění
bloudivého	bloudivý	k2eAgInSc2d1	bloudivý
nervu	nerv	k1gInSc2	nerv
stlačení	stlačení	k1gNnSc2	stlačení
krkavic	krkavice	k1gFnPc2	krkavice
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zamezení	zamezení	k1gNnSc4	zamezení
toku	tok	k1gInSc2	tok
krve	krev	k1gFnSc2	krev
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
uzavření	uzavření	k1gNnSc2	uzavření
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
Bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
nastává	nastávat	k5eAaImIp3nS	nastávat
prakticky	prakticky	k6eAd1	prakticky
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
oběšení	oběšení	k1gNnSc6	oběšení
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
až	až	k6eAd1	až
jeden	jeden	k4xCgInSc4	jeden
a	a	k8xC	a
půl	půl	k1xP	půl
minuty	minuta	k1gFnSc2	minuta
se	se	k3xPyFc4	se
na	na	k7c4	na
okamžik	okamžik	k1gInSc4	okamžik
zastaví	zastavit	k5eAaPmIp3nS	zastavit
srdce	srdce	k1gNnSc1	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
činnost	činnost	k1gFnSc1	činnost
obnoví	obnovit	k5eAaPmIp3nS	obnovit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
pulz	pulz	k1gInSc1	pulz
je	být	k5eAaImIp3nS	být
slabý	slabý	k2eAgInSc1d1	slabý
a	a	k8xC	a
nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	s	k7c7	s
záškuby	záškub	k1gInPc7	záškub
svalstva	svalstvo	k1gNnSc2	svalstvo
<g/>
,	,	kIx,	,
nejvýraznější	výrazný	k2eAgFnPc4d3	nejvýraznější
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
dolních	dolní	k2eAgFnPc6d1	dolní
končetinách	končetina	k1gFnPc6	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
minutách	minuta	k1gFnPc6	minuta
křeče	křeč	k1gFnSc2	křeč
přestávají	přestávat	k5eAaImIp3nP	přestávat
a	a	k8xC	a
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
se	se	k3xPyFc4	se
pulz	pulz	k1gInSc1	pulz
a	a	k8xC	a
zvýrazňuje	zvýrazňovat	k5eAaImIp3nS	zvýrazňovat
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
nepravidelnost	nepravidelnost	k1gFnSc1	nepravidelnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalších	další	k2eAgFnPc6d1	další
čtyřech	čtyři	k4xCgFnPc6	čtyři
minutách	minuta	k1gFnPc6	minuta
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
deseti	deset	k4xCc6	deset
minutách	minuta	k1gFnPc6	minuta
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
škrcení	škrcení	k1gNnSc2	škrcení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
výrazné	výrazný	k2eAgFnPc4d1	výrazná
nevratné	vratný	k2eNgFnPc4d1	nevratná
změny	změna	k1gFnPc4	změna
na	na	k7c6	na
mozku	mozek	k1gInSc6	mozek
a	a	k8xC	a
tedy	tedy	k9	tedy
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
při	při	k7c6	při
oběšení	oběšení	k1gNnSc6	oběšení
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
uskřinutí	uskřinutý	k2eAgMnPc1d1	uskřinutý
jazyka	jazyk	k1gInSc2	jazyk
mezi	mezi	k7c7	mezi
zuby	zub	k1gInPc7	zub
<g/>
,	,	kIx,	,
uvolnění	uvolněný	k2eAgMnPc1d1	uvolněný
moči	moč	k1gFnSc2	moč
<g/>
,	,	kIx,	,
stolice	stolice	k1gFnSc2	stolice
či	či	k8xC	či
spermatu	sperma	k1gNnSc2	sperma
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
k	k	k7c3	k
erekci	erekce	k1gFnSc3	erekce
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
i	i	k9	i
krvácení	krvácení	k1gNnSc4	krvácení
u	u	k7c2	u
nosu	nos	k1gInSc2	nos
nebo	nebo	k8xC	nebo
z	z	k7c2	z
uší	ucho	k1gNnPc2	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Strangulace	strangulace	k1gFnSc1	strangulace
Uškrcení	uškrcení	k1gNnSc2	uškrcení
Zardoušení	zardoušení	k1gNnSc2	zardoušení
Poprava	poprava	k1gFnSc1	poprava
Sebevražda	sebevražda	k1gFnSc1	sebevražda
Vražda	vražda	k1gFnSc1	vražda
TESAŘ	Tesař	k1gMnSc1	Tesař
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgNnSc1d1	soudní
lékařství	lékařství	k1gNnSc1	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Avicenum	Avicenum	k1gNnSc1	Avicenum
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
264	[number]	k4	264
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
oběšení	oběšení	k1gNnSc2	oběšení
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
