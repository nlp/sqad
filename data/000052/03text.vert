<s>
Kilogram	kilogram	k1gInSc1	kilogram
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
kilo	kilo	k1gNnSc1	kilo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
jednotka	jednotka	k1gFnSc1	jednotka
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
je	být	k5eAaImIp3nS	být
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přibližně	přibližně	k6eAd1	přibližně
hmotnosti	hmotnost	k1gFnPc1	hmotnost
1	[number]	k4	1
litru	litr	k1gInSc2	litr
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
soustavy	soustava	k1gFnSc2	soustava
SI	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
kilogram	kilogram	k1gInSc1	kilogram
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k8xS	jako
hmotnost	hmotnost	k1gFnSc1	hmotnost
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
prototypu	prototyp	k1gInSc2	prototyp
kilogramu	kilogram	k1gInSc2	kilogram
uloženého	uložený	k2eAgInSc2d1	uložený
u	u	k7c2	u
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
míry	míra	k1gFnPc4	míra
a	a	k8xC	a
váhy	váha	k1gFnPc4	váha
v	v	k7c6	v
Sè	Sè	k1gFnSc6	Sè
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
definice	definice	k1gFnSc1	definice
kilogramu	kilogram	k1gInSc2	kilogram
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
předcházelo	předcházet	k5eAaImAgNnS	předcházet
pověření	pověření	k1gNnSc1	pověření
vědců	vědec	k1gMnPc2	vědec
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
stanovili	stanovit	k5eAaPmAgMnP	stanovit
jednotky	jednotka	k1gFnPc4	jednotka
v	v	k7c6	v
desítkové	desítkový	k2eAgFnSc6d1	desítková
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
kořene	kořen	k1gInSc2	kořen
grámma	grámmum	k1gNnSc2	grámmum
<g/>
,	,	kIx,	,
plus	plus	k6eAd1	plus
předpona	předpona	k1gFnSc1	předpona
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
kilo	kilo	k1gNnSc1	kilo
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
označení	označení	k1gNnSc1	označení
jednotky	jednotka	k1gFnSc2	jednotka
již	již	k9	již
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
předponu	předpona	k1gFnSc4	předpona
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
základní	základní	k2eAgFnSc4d1	základní
jednotku	jednotka	k1gFnSc4	jednotka
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
gram	gram	k1gInSc1	gram
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
násobek	násobek	k1gInSc4	násobek
této	tento	k3xDgFnSc2	tento
základní	základní	k2eAgFnSc2d1	základní
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Kilogram	kilogram	k1gInSc1	kilogram
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc4d1	jediná
takovou	takový	k3xDgFnSc7	takový
jednotkou	jednotka	k1gFnSc7	jednotka
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	si	k1gNnSc2	si
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
3	[number]	k4	3
Generální	generální	k2eAgFnSc1d1	generální
konference	konference	k1gFnSc1	konference
pro	pro	k7c4	pro
míry	míra	k1gFnPc4	míra
a	a	k8xC	a
váhy	váha	k1gFnPc4	váha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
je	být	k5eAaImIp3nS	být
kilogram	kilogram	k1gInSc1	kilogram
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k8xC	jako
jednotka	jednotka	k1gFnSc1	jednotka
hmotnosti	hmotnost	k1gFnSc2	hmotnost
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
–	–	k?	–
<g/>
CGPM	CGPM	kA	CGPM
<g/>
,	,	kIx,	,
Declaration	Declaration	k1gInSc1	Declaration
on	on	k3xPp3gInSc1	on
the	the	k?	the
unit	unit	k1gInSc1	unit
of	of	k?	of
mass	mass	k1gInSc1	mass
and	and	k?	and
on	on	k3xPp3gMnSc1	on
the	the	k?	the
definition	definition	k1gInSc1	definition
of	of	k?	of
weight	weight	k1gInSc1	weight
<g/>
;	;	kIx,	;
conventional	conventionat	k5eAaPmAgInS	conventionat
value	value	k1gInSc1	value
of	of	k?	of
gn	gn	k?	gn
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
prototyp	prototyp	k1gInSc1	prototyp
kilogramu	kilogram	k1gInSc2	kilogram
je	být	k5eAaImIp3nS	být
válec	válec	k1gInSc1	válec
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
i	i	k8xC	i
průměru	průměr	k1gInSc6	průměr
39	[number]	k4	39
mm	mm	kA	mm
vyrobený	vyrobený	k2eAgMnSc1d1	vyrobený
ze	z	k7c2	z
slitiny	slitina	k1gFnSc2	slitina
90	[number]	k4	90
%	%	kIx~	%
platiny	platina	k1gFnSc2	platina
a	a	k8xC	a
10	[number]	k4	10
%	%	kIx~	%
iridia	iridium	k1gNnSc2	iridium
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
byly	být	k5eAaImAgFnP	být
vyrobeny	vyroben	k2eAgFnPc1d1	vyrobena
co	co	k9	co
možná	možná	k9	možná
identické	identický	k2eAgFnPc4d1	identická
kopie	kopie	k1gFnPc4	kopie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
příslušné	příslušný	k2eAgInPc4d1	příslušný
instituty	institut	k1gInPc4	institut
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgFnPc2	tento
kopií	kopie	k1gFnPc2	kopie
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
celkem	celek	k1gInSc7	celek
80	[number]	k4	80
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
tento	tento	k3xDgInSc4	tento
státní	státní	k2eAgInSc4d1	státní
etalon	etalon	k1gInSc4	etalon
(	(	kIx(	(
<g/>
kopie	kopie	k1gFnSc1	kopie
č.	č.	k?	č.
67	[number]	k4	67
<g/>
)	)	kIx)	)
Český	český	k2eAgInSc1d1	český
metrologický	metrologický	k2eAgInSc1d1	metrologický
institut	institut	k1gInSc1	institut
<g/>
.	.	kIx.	.
</s>
<s>
Materiál	materiál	k1gInSc1	materiál
pro	pro	k7c4	pro
zhotovení	zhotovení	k1gNnSc4	zhotovení
prototypu	prototyp	k1gInSc2	prototyp
byl	být	k5eAaImAgInS	být
volen	volit	k5eAaImNgInS	volit
podle	podle	k7c2	podle
následujících	následující	k2eAgNnPc2d1	následující
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
:	:	kIx,	:
odolnost	odolnost	k1gFnSc1	odolnost
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hustota	hustota	k1gFnSc1	hustota
(	(	kIx(	(
<g/>
omezení	omezení	k1gNnSc1	omezení
vlivu	vliv	k1gInSc2	vliv
vztlaku	vztlak	k1gInSc2	vztlak
při	při	k7c6	při
měření	měření	k1gNnSc6	měření
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dobrá	dobrý	k2eAgFnSc1d1	dobrá
elektrická	elektrický	k2eAgFnSc1d1	elektrická
vodivost	vodivost	k1gFnSc1	vodivost
(	(	kIx(	(
<g/>
eliminace	eliminace	k1gFnSc1	eliminace
vlivu	vliv	k1gInSc2	vliv
statické	statický	k2eAgFnSc2d1	statická
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nízká	nízký	k2eAgFnSc1d1	nízká
magnetická	magnetický	k2eAgFnSc1d1	magnetická
vodivost	vodivost	k1gFnSc1	vodivost
-	-	kIx~	-
diamagnetismus	diamagnetismus	k1gInSc1	diamagnetismus
(	(	kIx(	(
<g/>
omezení	omezení	k1gNnSc1	omezení
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
magnetických	magnetický	k2eAgInPc2d1	magnetický
vlivů	vliv	k1gInPc2	vliv
-	-	kIx~	-
přitažlivosti	přitažlivost	k1gFnSc2	přitažlivost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tepelná	tepelný	k2eAgFnSc1d1	tepelná
stabilita	stabilita	k1gFnSc1	stabilita
<g/>
,	,	kIx,	,
tvrdost	tvrdost	k1gFnSc1	tvrdost
(	(	kIx(	(
<g/>
odolnost	odolnost	k1gFnSc1	odolnost
proti	proti	k7c3	proti
otěru	otěr	k1gInSc3	otěr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kilogram	kilogram	k1gInSc1	kilogram
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc1d1	poslední
jednotka	jednotka	k1gFnSc1	jednotka
SI	si	k1gNnSc2	si
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
definovaná	definovaný	k2eAgFnSc1d1	definovaná
pomocí	pomocí	k7c2	pomocí
prototypu	prototyp	k1gInSc2	prototyp
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
fyzikální	fyzikální	k2eAgFnSc7d1	fyzikální
definicí	definice	k1gFnSc7	definice
<g/>
.	.	kIx.	.
</s>
<s>
Kilogram	kilogram	k1gInSc1	kilogram
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odpovídal	odpovídat	k5eAaImAgInS	odpovídat
hmotnosti	hmotnost	k1gFnSc2	hmotnost
1	[number]	k4	1
litru	litr	k1gInSc2	litr
vody	voda	k1gFnSc2	voda
prosté	prostý	k2eAgFnSc2d1	prostá
vzduchu	vzduch	k1gInSc2	vzduch
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
má	mít	k5eAaImIp3nS	mít
voda	voda	k1gFnSc1	voda
maximální	maximální	k2eAgFnSc4d1	maximální
hustotu	hustota	k1gFnSc4	hustota
(	(	kIx(	(
<g/>
3,98	[number]	k4	3,98
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
normálním	normální	k2eAgInSc6d1	normální
atmosférickém	atmosférický	k2eAgInSc6d1	atmosférický
tlaku	tlak	k1gInSc6	tlak
(	(	kIx(	(
<g/>
760	[number]	k4	760
mm	mm	kA	mm
Hg	Hg	k1gFnPc2	Hg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
původní	původní	k2eAgFnSc1d1	původní
definice	definice	k1gFnSc1	definice
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
závažné	závažný	k2eAgInPc4d1	závažný
nedostatky	nedostatek	k1gInPc4	nedostatek
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
totiž	totiž	k9	totiž
kruhovou	kruhový	k2eAgFnSc4d1	kruhová
závislost	závislost	k1gFnSc4	závislost
<g/>
:	:	kIx,	:
jednotka	jednotka	k1gFnSc1	jednotka
hmotnosti	hmotnost	k1gFnSc2	hmotnost
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
definuje	definovat	k5eAaBmIp3nS	definovat
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
definován	definovat	k5eAaBmNgInS	definovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
těmto	tento	k3xDgInPc3	tento
problémům	problém	k1gInPc3	problém
byl	být	k5eAaImAgMnS	být
tedy	tedy	k9	tedy
kilogram	kilogram	k1gInSc4	kilogram
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
definován	definovat	k5eAaBmNgMnS	definovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
prototypu	prototyp	k1gInSc2	prototyp
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kilogram	kilogram	k1gInSc1	kilogram
přibližně	přibližně	k6eAd1	přibližně
vyhovoval	vyhovovat	k5eAaImAgInS	vyhovovat
původní	původní	k2eAgFnSc4d1	původní
definici	definice	k1gFnSc4	definice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
původního	původní	k2eAgInSc2d1	původní
standardu	standard	k1gInSc2	standard
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
malé	malý	k2eAgFnSc3d1	malá
odchylce	odchylka	k1gFnSc3	odchylka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
1	[number]	k4	1
kilogram	kilogram	k1gInSc1	kilogram
vody	voda	k1gFnSc2	voda
nemá	mít	k5eNaImIp3nS	mít
objem	objem	k1gInSc1	objem
přesně	přesně	k6eAd1	přesně
1	[number]	k4	1
litr	litr	k1gInSc4	litr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
1,000	[number]	k4	1,000
028	[number]	k4	028
l.	l.	k?	l.
Definice	definice	k1gFnSc2	definice
prototypem	prototyp	k1gInSc7	prototyp
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
další	další	k2eAgInPc4d1	další
problémy	problém	k1gInPc4	problém
<g/>
:	:	kIx,	:
Z	z	k7c2	z
nejasných	jasný	k2eNgFnPc2d1	nejasná
příčin	příčina	k1gFnPc2	příčina
za	za	k7c2	za
posledních	poslední	k2eAgNnPc2d1	poslední
100	[number]	k4	100
let	léto	k1gNnPc2	léto
prototyp	prototyp	k1gInSc1	prototyp
ztratil	ztratit	k5eAaPmAgInS	ztratit
přibližně	přibližně	k6eAd1	přibližně
50	[number]	k4	50
mikrogramů	mikrogram	k1gInPc2	mikrogram
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
však	však	k9	však
kilogram	kilogram	k1gInSc1	kilogram
definován	definován	k2eAgInSc1d1	definován
jako	jako	k8xC	jako
aktuální	aktuální	k2eAgFnSc1d1	aktuální
hmotnost	hmotnost	k1gFnSc1	hmotnost
prototypu	prototyp	k1gInSc2	prototyp
<g/>
,	,	kIx,	,
změnila	změnit	k5eAaPmAgFnS	změnit
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
definovaná	definovaný	k2eAgFnSc1d1	definovaná
velikost	velikost	k1gFnSc1	velikost
kilogramu	kilogram	k1gInSc2	kilogram
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
objekt	objekt	k1gInSc1	objekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
před	před	k7c7	před
100	[number]	k4	100
lety	léto	k1gNnPc7	léto
hmotnost	hmotnost	k1gFnSc4	hmotnost
1	[number]	k4	1
000	[number]	k4	000
kg	kg	kA	kg
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nezměnil	změnit	k5eNaPmAgInS	změnit
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
hmotnost	hmotnost	k1gFnSc1	hmotnost
cca	cca	kA	cca
1	[number]	k4	1
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0,000	[number]	k4	0,000
05	[number]	k4	05
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
změny	změna	k1gFnSc2	změna
hmotnosti	hmotnost	k1gFnSc2	hmotnost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ztráta	ztráta	k1gFnSc1	ztráta
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
slitiny	slitina	k1gFnSc2	slitina
dostaly	dostat	k5eAaPmAgInP	dostat
jako	jako	k9	jako
parazitní	parazitní	k2eAgFnPc1d1	parazitní
příměsi	příměs	k1gFnPc1	příměs
při	při	k7c6	při
její	její	k3xOp3gFnSc6	její
přípravě	příprava	k1gFnSc6	příprava
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
dovozovanou	dovozovaný	k2eAgFnSc7d1	dovozovaná
příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
lidský	lidský	k2eAgInSc4d1	lidský
faktor	faktor	k1gInSc4	faktor
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
při	při	k7c6	při
opakovaném	opakovaný	k2eAgInSc6d1	opakovaný
<g/>
,	,	kIx,	,
i	i	k8xC	i
jemném	jemný	k2eAgMnSc6d1	jemný
<g/>
,	,	kIx,	,
čištění	čištění	k1gNnSc4	čištění
prototypu	prototyp	k1gInSc2	prototyp
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
100	[number]	k4	100
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgMnS	být
prototyp	prototyp	k1gInSc4	prototyp
prostě	prostě	k6eAd1	prostě
odřen	odřen	k2eAgMnSc1d1	odřen
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
mírně	mírně	k6eAd1	mírně
ztratil	ztratit	k5eAaPmAgMnS	ztratit
na	na	k7c6	na
hmotnosti	hmotnost	k1gFnSc6	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
ztrátu	ztráta	k1gFnSc4	ztráta
hmotnosti	hmotnost	k1gFnSc2	hmotnost
právě	právě	k6eAd1	právě
tohoto	tento	k3xDgInSc2	tento
hlavního	hlavní	k2eAgInSc2d1	hlavní
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
prototypu	prototyp	k1gInSc2	prototyp
<g/>
;	;	kIx,	;
situace	situace	k1gFnSc1	situace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
opačná	opačný	k2eAgFnSc1d1	opačná
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
neznámé	známý	k2eNgFnSc2d1	neznámá
příčiny	příčina	k1gFnSc2	příčina
narostla	narůst	k5eAaPmAgFnS	narůst
hmotnost	hmotnost	k1gFnSc1	hmotnost
ostatních	ostatní	k2eAgInPc2d1	ostatní
národních	národní	k2eAgInPc2d1	národní
prototypů	prototyp	k1gInPc2	prototyp
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
přesnosti	přesnost	k1gFnSc2	přesnost
měření	měření	k1gNnSc2	měření
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
i	i	k9	i
přesnost	přesnost	k1gFnSc1	přesnost
tzv.	tzv.	kA	tzv.
komparačních	komparační	k2eAgFnPc2d1	komparační
vah	váha	k1gFnPc2	váha
(	(	kIx(	(
<g/>
prototypy	prototyp	k1gInPc4	prototyp
nelze	lze	k6eNd1	lze
vážit	vážit	k5eAaImF	vážit
absolutně	absolutně	k6eAd1	absolutně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
definice	definice	k1gFnSc2	definice
jediným	jediný	k2eAgInSc7d1	jediný
prototypem	prototyp	k1gInSc7	prototyp
hrozí	hrozit	k5eAaImIp3nS	hrozit
teoretická	teoretický	k2eAgFnSc1d1	teoretická
možnost	možnost	k1gFnSc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
tento	tento	k3xDgInSc1	tento
prototyp	prototyp	k1gInSc1	prototyp
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
ztracen	ztratit	k5eAaPmNgInS	ztratit
nebo	nebo	k8xC	nebo
zničen	zničit	k5eAaPmNgInS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Ryze	ryze	k6eAd1	ryze
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
definice	definice	k1gFnSc1	definice
by	by	kYmCp3nS	by
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
možnost	možnost	k1gFnSc1	možnost
jej	on	k3xPp3gMnSc4	on
kdykoli	kdykoli	k6eAd1	kdykoli
a	a	k8xC	a
kdekoli	kdekoli	k6eAd1	kdekoli
znovu	znovu	k6eAd1	znovu
vyrobit	vyrobit	k5eAaPmF	vyrobit
<g/>
.	.	kIx.	.
</s>
<s>
Definici	definice	k1gFnSc4	definice
prototypem	prototyp	k1gInSc7	prototyp
nelze	lze	k6eNd1	lze
předat	předat	k5eAaPmF	předat
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
hypotetické	hypotetický	k2eAgFnSc6d1	hypotetická
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
kilogram	kilogram	k1gInSc4	kilogram
popsat	popsat	k5eAaPmF	popsat
někomu	někdo	k3yInSc3	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
k	k	k7c3	k
prototypu	prototyp	k1gInSc3	prototyp
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
obyvatelé	obyvatel	k1gMnPc1	obyvatel
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
planety	planeta	k1gFnSc2	planeta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čistě	čistě	k6eAd1	čistě
fyzikální	fyzikální	k2eAgFnSc3d1	fyzikální
definici	definice	k1gFnSc3	definice
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
prostě	prostě	k6eAd1	prostě
odeslat	odeslat	k5eAaPmF	odeslat
jako	jako	k9	jako
zprávu	zpráva	k1gFnSc4	zpráva
a	a	k8xC	a
o	o	k7c4	o
realizaci	realizace	k1gFnSc4	realizace
prototypu	prototyp	k1gInSc2	prototyp
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
konverzi	konverze	k1gFnSc4	konverze
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
již	již	k9	již
adresát	adresát	k1gMnSc1	adresát
postaral	postarat	k5eAaPmAgMnS	postarat
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgInPc2d1	uvedený
problémů	problém	k1gInPc2	problém
je	být	k5eAaImIp3nS	být
i	i	k9	i
z	z	k7c2	z
principiálních	principiální	k2eAgInPc2d1	principiální
důvodů	důvod	k1gInPc2	důvod
definice	definice	k1gFnSc1	definice
prototypem	prototyp	k1gInSc7	prototyp
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c7	za
neuspokojivou	uspokojivý	k2eNgFnSc7d1	neuspokojivá
a	a	k8xC	a
hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
definice	definice	k1gFnSc1	definice
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
neměnných	měnný	k2eNgFnPc6d1	neměnná
vlastnostech	vlastnost	k1gFnPc6	vlastnost
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Problematikou	problematika	k1gFnSc7	problematika
definice	definice	k1gFnSc2	definice
jednotky	jednotka	k1gFnSc2	jednotka
se	se	k3xPyFc4	se
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2011	[number]	k4	2011
zabývala	zabývat	k5eAaImAgFnS	zabývat
i	i	k9	i
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
konference	konference	k1gFnSc1	konference
vědců	vědec	k1gMnPc2	vědec
z	z	k7c2	z
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
míry	míra	k1gFnPc4	míra
a	a	k8xC	a
váhy	váha	k1gFnPc4	váha
<g/>
,	,	kIx,	,
konaná	konaný	k2eAgFnSc1d1	konaná
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
s	s	k7c7	s
úkolem	úkol	k1gInSc7	úkol
stanovit	stanovit	k5eAaPmF	stanovit
směry	směr	k1gInPc4	směr
v	v	k7c6	v
definování	definování	k1gNnSc4	definování
jednotky	jednotka	k1gFnSc2	jednotka
kilogramu	kilogram	k1gInSc2	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
zvažovaných	zvažovaný	k2eAgInPc2d1	zvažovaný
způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
přistoupit	přistoupit	k5eAaPmF	přistoupit
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
definici	definice	k1gFnSc3	definice
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jaká	jaký	k3yIgFnSc1	jaký
fundamentální	fundamentální	k2eAgFnSc1d1	fundamentální
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
konstanta	konstanta	k1gFnSc1	konstanta
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
definicí	definice	k1gFnSc7	definice
zafixovala	zafixovat	k5eAaPmAgFnS	zafixovat
<g/>
:	:	kIx,	:
Pevným	pevný	k2eAgNnSc7d1	pevné
stanovením	stanovení	k1gNnSc7	stanovení
Planckovy	Planckův	k2eAgFnSc2d1	Planckova
konstanty	konstanta	k1gFnSc2	konstanta
by	by	kYmCp3nP	by
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
kvantové	kvantový	k2eAgFnSc2d1	kvantová
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
relativistického	relativistický	k2eAgInSc2d1	relativistický
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
energií	energie	k1gFnSc7	energie
a	a	k8xC	a
hmotností	hmotnost	k1gFnSc7	hmotnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
=	=	kIx~	=
h	h	k?	h
f	f	k?	f
=	=	kIx~	=
m	m	kA	m
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
hf	hf	k?	hf
<g/>
=	=	kIx~	=
<g/>
mc	mc	k?	mc
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
definovat	definovat	k5eAaBmF	definovat
jednotku	jednotka	k1gFnSc4	jednotka
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Možnou	možný	k2eAgFnSc7d1	možná
realizací	realizace	k1gFnSc7	realizace
jsou	být	k5eAaImIp3nP	být
wattové	wattový	k2eAgFnPc1d1	wattová
váhy	váha	k1gFnPc1	váha
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
watt	watt	k1gInSc1	watt
balance	balanc	k1gFnSc2	balanc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
porovnávají	porovnávat	k5eAaImIp3nP	porovnávat
tíhu	tíha	k1gFnSc4	tíha
tělesa	těleso	k1gNnSc2	těleso
s	s	k7c7	s
magnetickou	magnetický	k2eAgFnSc7d1	magnetická
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
tento	tento	k3xDgInSc4	tento
postup	postup	k1gInSc4	postup
použít	použít	k5eAaPmF	použít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
relativní	relativní	k2eAgFnSc2d1	relativní
nejistoty	nejistota	k1gFnSc2	nejistota
měření	měření	k1gNnSc2	měření
asi	asi	k9	asi
1	[number]	k4	1
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
nejistoty	nejistota	k1gFnSc2	nejistota
asi	asi	k9	asi
5,2	[number]	k4	5,2
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Pevným	pevný	k2eAgNnSc7d1	pevné
stanovením	stanovení	k1gNnSc7	stanovení
Avogadrovy	Avogadrův	k2eAgFnSc2d1	Avogadrova
konstanty	konstanta	k1gFnSc2	konstanta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
definuje	definovat	k5eAaBmIp3nS	definovat
jednotku	jednotka	k1gFnSc4	jednotka
látkového	látkový	k2eAgNnSc2d1	látkové
množství	množství	k1gNnSc2	množství
jeden	jeden	k4xCgInSc4	jeden
mol	mol	k1gInSc4	mol
by	by	kYmCp3nP	by
bylo	být	k5eAaImAgNnS	být
kdykoli	kdykoli	k6eAd1	kdykoli
možno	možno	k6eAd1	možno
realizovat	realizovat	k5eAaBmF	realizovat
etalon	etalon	k1gInSc4	etalon
kilogramu	kilogram	k1gInSc2	kilogram
jako	jako	k8xC	jako
Avogadrovu	Avogadrův	k2eAgFnSc4d1	Avogadrova
kouli	koule	k1gFnSc4	koule
z	z	k7c2	z
křemíku	křemík	k1gInSc2	křemík
s	s	k7c7	s
přesně	přesně	k6eAd1	přesně
určeným	určený	k2eAgInSc7d1	určený
počtem	počet	k1gInSc7	počet
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
atomů	atom	k1gInPc2	atom
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
molu	mol	k1gInSc6	mol
látky	látka	k1gFnSc2	látka
je	být	k5eAaImIp3nS	být
však	však	k9	však
hodně	hodně	k6eAd1	hodně
velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
bude	být	k5eAaImBp3nS	být
existovat	existovat	k5eAaImF	existovat
malá	malý	k2eAgFnSc1d1	malá
odchylka	odchylka	k1gFnSc1	odchylka
způsobená	způsobený	k2eAgFnSc1d1	způsobená
chybou	chyba	k1gFnSc7	chyba
počítání	počítání	k1gNnSc2	počítání
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
problémem	problém	k1gInSc7	problém
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
zabývat	zabývat	k5eAaImF	zabývat
Spolkový	spolkový	k2eAgInSc4d1	spolkový
fyzikálně-technický	fyzikálněechnický	k2eAgInSc4d1	fyzikálně-technický
institut	institut	k1gInSc4	institut
v	v	k7c6	v
Braunschweigu	Braunschweig	k1gInSc6	Braunschweig
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Projekt	projekt	k1gInSc1	projekt
Avogadro	Avogadra	k1gFnSc5	Avogadra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
byl	být	k5eAaImAgInS	být
zkonstruován	zkonstruován	k2eAgInSc1d1	zkonstruován
předmět	předmět	k1gInSc1	předmět
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
koule	koule	k1gFnSc2	koule
z	z	k7c2	z
chemicky	chemicky	k6eAd1	chemicky
čistého	čistý	k2eAgInSc2d1	čistý
křemíku	křemík	k1gInSc2	křemík
s	s	k7c7	s
atomovým	atomový	k2eAgNnSc7d1	atomové
číslem	číslo	k1gNnSc7	číslo
28	[number]	k4	28
a	a	k8xC	a
poloměrem	poloměr	k1gInSc7	poloměr
lišícím	lišící	k2eAgInSc7d1	lišící
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
jednotky	jednotka	k1gFnPc4	jednotka
atomů	atom	k1gInPc2	atom
-	-	kIx~	-
a	a	k8xC	a
současně	současně	k6eAd1	současně
nejdokonaleji	dokonale	k6eAd3	dokonale
kulatý	kulatý	k2eAgInSc1d1	kulatý
předmět	předmět	k1gInSc1	předmět
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Stanovením	stanovení	k1gNnSc7	stanovení
elementárního	elementární	k2eAgInSc2d1	elementární
náboje	náboj	k1gInSc2	náboj
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
kilogram	kilogram	k1gInSc4	kilogram
definovat	definovat	k5eAaBmF	definovat
pomocí	pomocí	k7c2	pomocí
magnetické	magnetický	k2eAgFnSc2d1	magnetická
síly	síla	k1gFnSc2	síla
mezi	mezi	k7c7	mezi
vodiči	vodič	k1gInPc7	vodič
se	s	k7c7	s
známým	známý	k2eAgInSc7d1	známý
proudem	proud	k1gInSc7	proud
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tělesu	těleso	k1gNnSc3	těleso
udělí	udělit	k5eAaPmIp3nS	udělit
stanovené	stanovený	k2eAgNnSc1d1	stanovené
zrychlení	zrychlení	k1gNnSc1	zrychlení
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nové	Nové	k2eAgFnSc2d1	Nové
definice	definice	k1gFnSc2	definice
SI	si	k1gNnSc2	si
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecné	všeobecný	k2eAgFnSc3d1	všeobecná
konferenci	konference	k1gFnSc3	konference
pro	pro	k7c4	pro
váhy	váha	k1gFnPc4	váha
a	a	k8xC	a
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
připraven	připravit	k5eAaPmNgInS	připravit
návrh	návrh	k1gInSc1	návrh
budoucí	budoucí	k2eAgFnSc2d1	budoucí
revize	revize	k1gFnSc2	revize
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
definice	definice	k1gFnSc1	definice
kilogramu	kilogram	k1gInSc2	kilogram
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
Planckovy	Planckův	k2eAgFnSc2d1	Planckova
konstanty	konstanta	k1gFnSc2	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
však	však	k9	však
zatím	zatím	k6eAd1	zatím
nebyly	být	k5eNaImAgFnP	být
splněny	splnit	k5eAaPmNgInP	splnit
požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
přesnost	přesnost	k1gFnSc4	přesnost
jejího	její	k3xOp3gNnSc2	její
měření	měření	k1gNnSc2	měření
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
revize	revize	k1gFnSc1	revize
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
zasedání	zasedání	k1gNnSc6	zasedání
zatím	zatím	k6eAd1	zatím
přijata	přijat	k2eAgFnSc1d1	přijata
<g/>
.	.	kIx.	.
</s>
<s>
Předpony	předpona	k1gFnPc4	předpona
lze	lze	k6eAd1	lze
dávat	dávat	k5eAaImF	dávat
k	k	k7c3	k
základu	základ	k1gInSc3	základ
gram	gram	k1gInSc1	gram
(	(	kIx(	(
<g/>
nanogram	nanogram	k1gInSc1	nanogram
<g/>
,	,	kIx,	,
gigagram	gigagram	k1gInSc1	gigagram
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
k	k	k7c3	k
základnímu	základní	k2eAgInSc3d1	základní
kilogramu	kilogram	k1gInSc3	kilogram
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
nikoli	nikoli	k9	nikoli
milikilogram	milikilogram	k1gInSc1	milikilogram
<g/>
,	,	kIx,	,
megakilogram	megakilogram	k1gInSc1	megakilogram
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
praktických	praktický	k2eAgInPc2d1	praktický
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
však	však	k9	však
užívá	užívat	k5eAaImIp3nS	užívat
také	také	k9	také
kilotuna	kilotuna	k1gFnSc1	kilotuna
a	a	k8xC	a
megatuna	megatuna	k1gFnSc1	megatuna
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
kilogramu	kilogram	k1gInSc2	kilogram
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
následující	následující	k2eAgFnPc1d1	následující
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
:	:	kIx,	:
</s>
<s>
Nanogram	Nanogram	k1gInSc1	Nanogram
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
ng	ng	k?	ng
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
tisícina	tisícina	k1gFnSc1	tisícina
mikrogramu	mikrogram	k1gInSc2	mikrogram
<g/>
.	.	kIx.	.
</s>
<s>
Mikrogram	mikrogram	k1gInSc1	mikrogram
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
μ	μ	k?	μ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tisícina	tisícina	k1gFnSc1	tisícina
miligramu	miligram	k1gInSc2	miligram
(	(	kIx(	(
<g/>
miliontina	miliontina	k1gFnSc1	miliontina
gramu	gram	k1gInSc2	gram
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
miliardtina	miliardtina	k1gFnSc1	miliardtina
kilogramu	kilogram	k1gInSc2	kilogram
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
příliš	příliš	k6eAd1	příliš
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mělo	mít	k5eAaImAgNnS	mít
nějaký	nějaký	k3yIgInSc4	nějaký
praktický	praktický	k2eAgInSc4d1	praktický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
sledování	sledování	k1gNnSc6	sledování
výskytu	výskyt	k1gInSc2	výskyt
superstopových	superstopový	k2eAgNnPc2d1	superstopový
množství	množství	k1gNnPc2	množství
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
některé	některý	k3yIgInPc1	některý
vzácné	vzácný	k2eAgInPc1d1	vzácný
prvky	prvek	k1gInPc1	prvek
se	se	k3xPyFc4	se
mořské	mořský	k2eAgFnSc3d1	mořská
vodě	voda	k1gFnSc3	voda
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
koncentrací	koncentrace	k1gFnPc2	koncentrace
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
,	,	kIx,	,
doporučená	doporučený	k2eAgFnSc1d1	Doporučená
denní	denní	k2eAgFnSc1d1	denní
dávka	dávka	k1gFnSc1	dávka
vitaminu	vitamin	k1gInSc2	vitamin
B12	B12	k1gFnSc2	B12
je	být	k5eAaImIp3nS	být
2,5	[number]	k4	2,5
μ	μ	k?	μ
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jaderné	jaderný	k2eAgFnSc6d1	jaderná
fyzice	fyzika	k1gFnSc6	fyzika
při	při	k7c6	při
udávání	udávání	k1gNnSc6	udávání
obsahu	obsah	k1gInSc2	obsah
krátkodobě	krátkodobě	k6eAd1	krátkodobě
žijících	žijící	k2eAgInPc2d1	žijící
izotopů	izotop	k1gInPc2	izotop
(	(	kIx(	(
<g/>
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Miligram	miligram	k1gInSc1	miligram
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
mg	mg	kA	mg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tisícina	tisícina	k1gFnSc1	tisícina
gramu	gram	k1gInSc2	gram
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
miliontina	miliontina	k1gFnSc1	miliontina
kilogramu	kilogram	k1gInSc2	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
chemii	chemie	k1gFnSc6	chemie
či	či	k8xC	či
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
obsahy	obsah	k1gInPc1	obsah
běžných	běžný	k2eAgInPc2d1	běžný
kovových	kovový	k2eAgInPc2d1	kovový
prvků	prvek	k1gInPc2	prvek
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
měď	měď	k1gFnSc1	měď
nebo	nebo	k8xC	nebo
zinek	zinek	k1gInSc1	zinek
se	se	k3xPyFc4	se
v	v	k7c6	v
živočišných	živočišný	k2eAgFnPc6d1	živočišná
a	a	k8xC	a
rostlinných	rostlinný	k2eAgFnPc6d1	rostlinná
tkáních	tkáň	k1gFnPc6	tkáň
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
jednotek	jednotka	k1gFnPc2	jednotka
až	až	k8xS	až
stovek	stovka	k1gFnPc2	stovka
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Obsahy	obsah	k1gInPc1	obsah
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
nebo	nebo	k8xC	nebo
typických	typický	k2eAgInPc2d1	typický
aniontů	anion	k1gInPc2	anion
jako	jako	k8xC	jako
uhličitany	uhličitan	k1gInPc1	uhličitan
se	se	k3xPyFc4	se
v	v	k7c6	v
minerálních	minerální	k2eAgFnPc6d1	minerální
vodách	voda	k1gFnPc6	voda
obvykle	obvykle	k6eAd1	obvykle
uvádějí	uvádět	k5eAaImIp3nP	uvádět
v	v	k7c6	v
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
</s>
<s>
Gram	gram	k1gInSc1	gram
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
g	g	kA	g
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
tisícina	tisícina	k1gFnSc1	tisícina
kilogramu	kilogram	k1gInSc2	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
často	často	k6eAd1	často
využívá	využívat	k5eAaPmIp3nS	využívat
jako	jako	k9	jako
jednotka	jednotka	k1gFnSc1	jednotka
pro	pro	k7c4	pro
vážení	vážení	k1gNnSc4	vážení
přísad	přísada	k1gFnPc2	přísada
při	při	k7c6	při
vaření	vaření	k1gNnSc6	vaření
a	a	k8xC	a
nákupu	nákup	k1gInSc6	nákup
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
pro	pro	k7c4	pro
potraviny	potravina	k1gFnPc4	potravina
prodávané	prodávaný	k2eAgFnPc4d1	prodávaná
v	v	k7c6	v
menším	malý	k2eAgNnSc6d2	menší
množství	množství	k1gNnSc6	množství
než	než	k8xS	než
jeden	jeden	k4xCgInSc1	jeden
kilogram	kilogram	k1gInSc1	kilogram
bývá	bývat	k5eAaImIp3nS	bývat
běžně	běžně	k6eAd1	běžně
uváděna	uvádět	k5eAaImNgFnS	uvádět
jako	jako	k8xC	jako
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
100	[number]	k4	100
g.	g.	k?	g.
Také	také	k9	také
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
obsahu	obsah	k1gInSc6	obsah
a	a	k8xC	a
složení	složení	k1gNnSc6	složení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
potravin	potravina	k1gFnPc2	potravina
bývají	bývat	k5eAaImIp3nP	bývat
vztahovány	vztahován	k2eAgFnPc1d1	vztahována
k	k	k7c3	k
hmotnosti	hmotnost	k1gFnSc3	hmotnost
100	[number]	k4	100
g	g	kA	g
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
procentům	procento	k1gNnPc3	procento
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Gram	gram	k1gInSc1	gram
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
hmotnosti	hmotnost	k1gFnSc2	hmotnost
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
soustavě	soustava	k1gFnSc6	soustava
CGS	CGS	kA	CGS
<g/>
.	.	kIx.	.
</s>
<s>
Dekagram	dekagram	k1gInSc1	dekagram
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgFnSc1d1	oficiální
značka	značka	k1gFnSc1	značka
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	si	k1gNnSc2	si
je	být	k5eAaImIp3nS	být
dag	dag	kA	dag
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
se	se	k3xPyFc4	se
častěji	často	k6eAd2	často
používá	používat	k5eAaImIp3nS	používat
zastaralé	zastaralý	k2eAgNnSc4d1	zastaralé
označení	označení	k1gNnSc4	označení
dkg	dkg	kA	dkg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
gramů	gram	k1gInPc2	gram
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jedna	jeden	k4xCgFnSc1	jeden
setina	setina	k1gFnSc1	setina
kilogramu	kilogram	k1gInSc2	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jednotka	jednotka	k1gFnSc1	jednotka
používaná	používaný	k2eAgFnSc1d1	používaná
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
maloobchodě	maloobchod	k1gInSc6	maloobchod
s	s	k7c7	s
potravinami	potravina	k1gFnPc7	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Čech	Čech	k1gMnSc1	Čech
mluvící	mluvící	k2eAgFnSc7d1	mluvící
hovorovou	hovorový	k2eAgFnSc7d1	hovorová
češtinou	čeština	k1gFnSc7	čeština
kupující	kupující	k2eAgNnSc1d1	kupující
množství	množství	k1gNnSc1	množství
menší	malý	k2eAgNnSc1d2	menší
než	než	k8xS	než
jeden	jeden	k4xCgInSc1	jeden
kilogram	kilogram	k1gInSc1	kilogram
většinou	většinou	k6eAd1	většinou
definuje	definovat	k5eAaBmIp3nS	definovat
požadované	požadovaný	k2eAgNnSc4d1	požadované
množství	množství	k1gNnSc4	množství
v	v	k7c6	v
dekagramech	dekagram	k1gInPc6	dekagram
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
deka	deka	k1gNnSc1	deka
<g/>
:	:	kIx,	:
Dejte	dát	k5eAaPmRp2nP	dát
mi	já	k3xPp1nSc3	já
20	[number]	k4	20
deka	deka	k1gNnPc2	deka
šunky	šunka	k1gFnSc2	šunka
<g/>
,	,	kIx,	,
prosím	prosit	k5eAaImIp1nS	prosit
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jednotková	jednotkový	k2eAgFnSc1d1	jednotková
cena	cena	k1gFnSc1	cena
se	se	k3xPyFc4	se
v	v	k7c6	v
maloobchodě	maloobchod	k1gInSc6	maloobchod
zpravidla	zpravidla	k6eAd1	zpravidla
udává	udávat	k5eAaImIp3nS	udávat
na	na	k7c4	na
100	[number]	k4	100
gramů	gram	k1gInPc2	gram
nebo	nebo	k8xC	nebo
na	na	k7c4	na
kilogram	kilogram	k1gInSc4	kilogram
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
zákazník	zákazník	k1gMnSc1	zákazník
kupuje	kupovat	k5eAaImIp3nS	kupovat
na	na	k7c6	na
deka	deka	k1gNnSc6	deka
<g/>
.	.	kIx.	.
</s>
<s>
Hovorově	hovorově	k6eAd1	hovorově
metrák	metrák	k1gInSc1	metrák
<g/>
,	,	kIx,	,
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
100	[number]	k4	100
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Značí	značit	k5eAaImIp3nP	značit
se	s	k7c7	s
q.	q.	k?	q.
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Metrický	metrický	k2eAgInSc4d1	metrický
cent	cent	k1gInSc4	cent
<g/>
.	.	kIx.	.
</s>
<s>
Tuna	tuna	k1gFnSc1	tuna
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
t	t	k?	t
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
Mg	mg	kA	mg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nepatří	patřit	k5eNaImIp3nS	patřit
do	do	k7c2	do
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
používat	používat	k5eAaImF	používat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jednotkami	jednotka	k1gFnPc7	jednotka
SI	si	k1gNnPc2	si
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
1000	[number]	k4	1000
kilogramům	kilogram	k1gInPc3	kilogram
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
totéž	týž	k3xTgNnSc1	týž
co	co	k9	co
megagram	megagram	k1gInSc1	megagram
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgInPc1d2	vyšší
řády	řád	k1gInPc1	řád
hmotností	hmotnost	k1gFnPc2	hmotnost
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
k	k	k7c3	k
tuně	tuna	k1gFnSc3	tuna
(	(	kIx(	(
<g/>
kilotuna	kilotuna	k1gFnSc1	kilotuna
<g/>
,	,	kIx,	,
megatuna	megatuna	k1gFnSc1	megatuna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
tuna	tuna	k1gFnSc1	tuna
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
např.	např.	kA	např.
v	v	k7c6	v
dopravním	dopravní	k2eAgNnSc6d1	dopravní
značení	značení	k1gNnSc6	značení
pro	pro	k7c4	pro
vyjadřování	vyjadřování	k1gNnSc4	vyjadřování
povolené	povolený	k2eAgFnSc2d1	povolená
hmotnosti	hmotnost	k1gFnSc2	hmotnost
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Kilotuna	Kilotuna	k1gFnSc1	Kilotuna
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
kt	kt	k?	kt
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
SI	si	k1gNnSc2	si
gigagram	gigagram	k1gInSc1	gigagram
<g/>
,	,	kIx,	,
Gg	Gg	k1gFnSc1	Gg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tisíc	tisíc	k4xCgInSc4	tisíc
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
milion	milion	k4xCgInSc4	milion
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
Megatuna	megatuna	k1gFnSc1	megatuna
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
Mt	Mt	k1gFnSc2	Mt
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
prakticky	prakticky	k6eAd1	prakticky
nepoužívané	používaný	k2eNgFnSc2d1	nepoužívaná
definice	definice	k1gFnSc2	definice
SI	se	k3xPyFc3	se
teragram	teragram	k1gInSc4	teragram
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
Tg	tg	kA	tg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
milion	milion	k4xCgInSc1	milion
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
miliarda	miliarda	k4xCgFnSc1	miliarda
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ekvivalentech	ekvivalent	k1gInPc6	ekvivalent
kilotun	kilotuna	k1gFnPc2	kilotuna
a	a	k8xC	a
megatun	megatuna	k1gFnPc2	megatuna
TNT	TNT	kA	TNT
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
udává	udávat	k5eAaImIp3nS	udávat
energie	energie	k1gFnSc1	energie
uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
výbuchem	výbuch	k1gInSc7	výbuch
jaderné	jaderný	k2eAgFnPc4d1	jaderná
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
známá	známý	k2eAgFnSc1d1	známá
jaderná	jaderný	k2eAgFnSc1d1	jaderná
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
,	,	kIx,	,
sovětská	sovětský	k2eAgFnSc1d1	sovětská
Car-bomba	Caromba	k1gFnSc1	Car-bomba
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
sílu	síla	k1gFnSc4	síla
okolo	okolo	k7c2	okolo
57	[number]	k4	57
Mt	Mt	k1gFnPc2	Mt
TNT	TNT	kA	TNT
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Systémem	systém	k1gInSc7	systém
měření	měření	k1gNnSc2	měření
a	a	k8xC	a
váhami	váha	k1gFnPc7	váha
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
následující	následující	k2eAgFnPc1d1	následující
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
instituce	instituce	k1gFnPc1	instituce
<g/>
:	:	kIx,	:
BIPM	BIPM	kA	BIPM
(	(	kIx(	(
<g/>
Bureau	Burea	k2eAgFnSc4d1	Burea
International	International	k1gFnSc4	International
des	des	k1gNnSc2	des
Poids	Poidsa	k1gFnPc2	Poidsa
et	et	k?	et
Mesures	Mesures	k1gMnSc1	Mesures
<g/>
)	)	kIx)	)
-	-	kIx~	-
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
míry	míra	k1gFnPc4	míra
a	a	k8xC	a
váhy	váha	k1gFnPc4	váha
<g/>
;	;	kIx,	;
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Sè	Sè	k1gFnSc6	Sè
u	u	k7c2	u
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
CGPM	CGPM	kA	CGPM
(	(	kIx(	(
<g/>
Conférence	Conférenec	k1gMnSc2	Conférenec
Générale	Général	k1gMnSc5	Général
des	des	k1gNnSc7	des
Poids	Poids	k1gInSc1	Poids
et	et	k?	et
Mesures	Mesures	k1gInSc1	Mesures
<g/>
)	)	kIx)	)
-	-	kIx~	-
Generální	generální	k2eAgFnSc1d1	generální
konference	konference	k1gFnSc1	konference
pro	pro	k7c4	pro
míry	míra	k1gFnPc4	míra
a	a	k8xC	a
váhy	váha	k1gFnPc4	váha
CIPM	CIPM	kA	CIPM
(	(	kIx(	(
<g/>
Comité	Comitý	k2eAgFnPc1d1	Comitý
International	International	k1gFnPc1	International
des	des	k1gNnSc2	des
Poids	Poidsa	k1gFnPc2	Poidsa
et	et	k?	et
Mesures	Mesures	k1gMnSc1	Mesures
<g/>
)	)	kIx)	)
-	-	kIx~	-
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
míry	míra	k1gFnPc4	míra
a	a	k8xC	a
váhy	váha	k1gFnPc4	váha
</s>
