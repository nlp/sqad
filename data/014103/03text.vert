<s>
Operalia	Operalia	k1gFnSc1
</s>
<s>
Operalia	Operalia	k1gFnSc1
<g/>
,	,	kIx,
soutěž	soutěž	k1gFnSc1
světové	světový	k2eAgFnSc2d1
opery	opera	k1gFnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
The	The	k1gFnSc1
World	Worlda	k1gFnPc2
Opera	opera	k1gFnSc1
Competition	Competition	k1gInSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
každoroční	každoroční	k2eAgFnSc1d1
mezinárodní	mezinárodní	k2eAgFnSc1d1
pěvecká	pěvecký	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
pro	pro	k7c4
mladé	mladý	k2eAgMnPc4d1
operní	operní	k2eAgMnPc4d1
zpěváky	zpěvák	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založil	založit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
Plácido	Plácida	k1gFnSc5
Domingo	Domingo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomohla	pomoct	k5eAaPmAgFnS
zahájit	zahájit	k5eAaPmF
kariéru	kariéra	k1gFnSc4
řadě	řada	k1gFnSc3
významných	významný	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
Joseph	Joseph	k1gMnSc1
Calleja	Calleja	k1gMnSc1
<g/>
,	,	kIx,
Giuseppe	Giusepp	k1gInSc5
Filianoti	Filianot	k1gMnPc1
<g/>
,	,	kIx,
Rolando	Rolanda	k1gFnSc5
Villazón	Villazón	k1gInSc4
<g/>
,	,	kIx,
José	José	k1gNnSc4
Cura	Curus	k1gMnSc2
<g/>
,	,	kIx,
Joyce	Joyce	k1gMnSc2
DiDonato	DiDonat	k2eAgNnSc1d1
<g/>
,	,	kIx,
Elizabeth	Elizabeth	k1gFnSc1
Futral	Futral	k1gFnSc1
<g/>
,	,	kIx,
Inva	Inv	k2eAgFnSc1d1
Mula	mula	k1gFnSc1
<g/>
,	,	kIx,
Ana	Ana	k1gMnSc1
María	María	k1gMnSc1
Martínez	Martínez	k1gMnSc1
a	a	k8xC
Sonya	Sony	k2eAgFnSc1d1
Yoncheva	Yoncheva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Podmínky	podmínka	k1gFnPc1
a	a	k8xC
ceny	cena	k1gFnPc1
</s>
<s>
Soutěže	soutěž	k1gFnPc1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
zúčastnit	zúčastnit	k5eAaPmF
zpěváci	zpěvák	k1gMnPc1
všech	všecek	k3xTgFnPc2
hlasových	hlasový	k2eAgFnPc2d1
poloh	poloha	k1gFnPc2
ve	v	k7c6
věku	věk	k1gInSc6
18	#num#	k4
až	až	k9
32	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každoročně	každoročně	k6eAd1
se	se	k3xPyFc4
přihlásí	přihlásit	k5eAaPmIp3nP
přibližně	přibližně	k6eAd1
tisíc	tisíc	k4xCgInSc4
uchazečů	uchazeč	k1gMnPc2
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yIgFnPc2,k3yQgFnPc2,k3yRgFnPc2
porota	porota	k1gFnSc1
tří	tři	k4xCgMnPc2
odborníků	odborník	k1gMnPc2
vybere	vybrat	k5eAaPmIp3nS
čtyřicet	čtyřicet	k4xCc1
a	a	k8xC
pozve	pozvat	k5eAaPmIp3nS
je	on	k3xPp3gNnSc4
na	na	k7c4
soutěž	soutěž	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
v	v	k7c6
jiné	jiný	k2eAgFnSc6d1
světové	světový	k2eAgFnSc6d1
metropoli	metropol	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodnocení	hodnocení	k1gNnSc1
účastníků	účastník	k1gMnPc2
provádí	provádět	k5eAaImIp3nS
odborná	odborný	k2eAgFnSc1d1
porota	porota	k1gFnSc1
složená	složený	k2eAgFnSc1d1
z	z	k7c2
deseti	deset	k4xCc2
zástupců	zástupce	k1gMnPc2
nejdůležitějších	důležitý	k2eAgFnPc2d3
operních	operní	k2eAgFnPc2d1
scén	scéna	k1gFnPc2
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgInSc6
kole	kolo	k1gNnSc6
všech	všecek	k3xTgMnPc2
čtyřicet	čtyřicet	k4xCc4
kandidátů	kandidát	k1gMnPc2
představí	představit	k5eAaPmIp3nP
dvě	dva	k4xCgFnPc1
operní	operní	k2eAgFnPc1d1
árie	árie	k1gFnPc1
volně	volně	k6eAd1
vybrané	vybraný	k2eAgFnPc1d1
ze	z	k7c2
seznamu	seznam	k1gInSc2
čtyř	čtyři	k4xCgFnPc2
předložených	předložený	k2eAgFnPc2d1
árií	árie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
těchto	tento	k3xDgMnPc2
čtyřiceti	čtyřicet	k4xCc2
kandidátů	kandidát	k1gMnPc2
se	se	k3xPyFc4
jich	on	k3xPp3gMnPc2
do	do	k7c2
semifinále	semifinále	k1gNnSc2
vybírá	vybírat	k5eAaImIp3nS
dvacet	dvacet	k4xCc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ty	k3xPp2nSc3
v	v	k7c6
něm	on	k3xPp3gMnSc6
pak	pak	k6eAd1
představí	představit	k5eAaPmIp3nS
árii	árie	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
jim	on	k3xPp3gMnPc3
určí	určit	k5eAaPmIp3nS
odborná	odborný	k2eAgFnSc1d1
porota	porota	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dvaceti	dvacet	k4xCc2
kandidátů	kandidát	k1gMnPc2
druhého	druhý	k4xOgNnSc2
kola	kolo	k1gNnSc2
soutěže	soutěž	k1gFnSc2
vybere	vybrat	k5eAaPmIp3nS
odborná	odborný	k2eAgFnSc1d1
porota	porota	k1gFnSc1
do	do	k7c2
finále	finále	k1gNnSc2
deset	deset	k4xCc1
postupujících	postupující	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtvrtfinálové	čtvrtfinálový	k2eAgInPc1d1
i	i	k8xC
semifinálové	semifinálový	k2eAgNnSc4d1
kolo	kolo	k1gNnSc4
doprovází	doprovázet	k5eAaImIp3nS
korepetitor	korepetitor	k1gMnSc1
<g/>
,	,	kIx,
finále	finále	k1gNnSc1
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
jako	jako	k9
galakoncert	galakoncert	k1gInSc1
s	s	k7c7
orchestrem	orchestr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diriguje	dirigovat	k5eAaImIp3nS
Plácido	Plácida	k1gFnSc5
Domingo	Domingo	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
také	také	k9
vede	vést	k5eAaImIp3nS
desetičlennou	desetičlenný	k2eAgFnSc4d1
porotu	porota	k1gFnSc4
jako	jako	k8xS,k8xC
její	její	k3xOp3gMnSc1
nehlasující	hlasující	k2eNgMnSc1d1
člen	člen	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpěváci	Zpěvák	k1gMnPc1
mohou	moct	k5eAaImIp3nP
také	také	k9
soutěžit	soutěžit	k5eAaImF
v	v	k7c6
samostatné	samostatný	k2eAgFnSc6d1
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
tak	tak	k6eAd1
rozhodnou	rozhodnout	k5eAaPmIp3nP
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
v	v	k7c6
každém	každý	k3xTgInSc6
kole	kolo	k1gNnSc6
zazpívat	zazpívat	k5eAaPmF
další	další	k2eAgFnSc4d1
zarzuelu	zarzuela	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
první	první	k4xOgFnSc2
<g/>
,	,	kIx,
druhé	druhý	k4xOgFnPc1
a	a	k8xC
třetí	třetí	k4xOgFnPc1
ceny	cena	k1gFnPc1
se	se	k3xPyFc4
ještě	ještě	k6eAd1
udílí	udílet	k5eAaImIp3nS
cena	cena	k1gFnSc1
za	za	k7c4
zarzuelu	zarzuela	k1gFnSc4
<g/>
,	,	kIx,
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
<g/>
,	,	kIx,
cena	cena	k1gFnSc1
Birgit	Birgit	k1gMnSc1
Nilsson	Nilsson	k1gMnSc1
(	(	kIx(
<g/>
za	za	k7c4
vystoupení	vystoupení	k1gNnSc4
v	v	k7c6
německém	německý	k2eAgInSc6d1
repertoáru	repertoár	k1gInSc6
Richarda	Richard	k1gMnSc2
Strausse	Strauss	k1gMnSc2
a	a	k8xC
Richarda	Richard	k1gMnSc2
Wagnera	Wagner	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cenu	cena	k1gFnSc4
CulturArte	CulturArt	k1gInSc5
poskytují	poskytovat	k5eAaImIp3nP
Bertita	Bertit	k1gMnSc2
a	a	k8xC
Guillermo	Guillerma	k1gFnSc5
Martinez	Martinez	k1gMnSc1
z	z	k7c2
instituce	instituce	k1gFnSc2
CulturArte	CulturArt	k1gInSc5
de	de	k?
Puerto	Puerta	k1gFnSc5
Rico	Rico	k1gNnSc5
<g/>
.	.	kIx.
</s>
<s>
Ocenění	oceněný	k2eAgMnPc1d1
uchazeči	uchazeč	k1gMnPc1
</s>
<s>
Roky	rok	k1gInPc1
2020	#num#	k4
–	–	k?
2029	#num#	k4
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Hlas	hlas	k1gInSc1
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
a	a	k8xC
místo	místo	k1gNnSc1
</s>
<s>
-	-	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
2020	#num#	k4
<g/>
,	,	kIx,
Tel	tel	kA
Aviv	Aviv	k1gInSc1
-	-	kIx~
nekonalo	konat	k5eNaImAgNnS
sa	sa	k?
<g/>
,	,	kIx,
odložené	odložený	k2eAgFnSc2d1
</s>
<s>
Roky	rok	k1gInPc1
2010	#num#	k4
–	–	k?
2019	#num#	k4
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Hlas	hlas	k1gInSc1
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
a	a	k8xC
místo	místo	k1gNnSc1
</s>
<s>
Adriana	Adriana	k1gFnSc1
Gonzalez	Gonzaleza	k1gFnPc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Guatemala	Guatemala	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
</s>
<s>
2019	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Xabier	Xabier	k1gMnSc1
Anduaga	Anduag	k1gMnSc2
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
</s>
<s>
2019	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Gihoon	Gihoon	k1gMnSc1
Kim	Kim	k1gMnSc1
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2019	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Maria	Maria	k1gFnSc1
Kataeva	Kataeva	k1gFnSc1
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2019	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
AryehNussbaum	AryehNussbaum	k1gNnSc1
Cohen	Cohna	k1gFnPc2
</s>
<s>
kontratenor	kontratenor	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2019	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Christina	Christina	k1gFnSc1
Nilsson	Nilssona	k1gFnPc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
Birgit	Birgita	k1gFnPc2
Nilson	Nilsona	k1gFnPc2
</s>
<s>
2019	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Felicia	felicia	k1gFnSc1
Moore	Moor	k1gMnSc5
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Cena	cena	k1gFnSc1
Birgit	Birgit	k1gMnSc1
Nilsson	Nilsson	k1gMnSc1
</s>
<s>
2019	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Anna	Anna	k1gFnSc1
Shapovalova	Shapovalův	k2eAgFnSc1d1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gInSc5
</s>
<s>
2019	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Emily	Emil	k1gMnPc7
D	D	kA
<g/>
’	’	k?
<g/>
Angelo	Angela	k1gFnSc5
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Itálie	Itálie	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
cena	cena	k1gFnSc1
Birgit	Birgit	k1gMnSc1
Nilson	Nilson	k1gMnSc1
<g/>
,	,	kIx,
od	od	k7c2
diváků	divák	k1gMnPc2
a	a	k8xC
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
</s>
<s>
2018	#num#	k4
<g/>
,	,	kIx,
Lisabon	Lisabon	k1gInSc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Petrov	Petrov	k1gInSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
</s>
<s>
2018	#num#	k4
<g/>
,	,	kIx,
Lisabon	Lisabon	k1gInSc1
</s>
<s>
Migran	Migran	k1gInSc1
Agadzhanyan	Agadzhanyany	k1gInPc2
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2018	#num#	k4
<g/>
,	,	kIx,
Lisabon	Lisabon	k1gInSc1
</s>
<s>
Samantha	Samantha	k1gFnSc1
Hankey	Hankea	k1gFnSc2
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
Birgit	Birgita	k1gFnPc2
Nilsson	Nilssona	k1gFnPc2
</s>
<s>
2018	#num#	k4
<g/>
,	,	kIx,
Lisabon	Lisabon	k1gInSc1
</s>
<s>
Arseny	arsen	k1gInPc1
Yakovlev	Yakovlev	k1gFnSc2
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2018	#num#	k4
<g/>
,	,	kIx,
Lisabon	Lisabon	k1gInSc1
</s>
<s>
Rihab	Rihab	k1gInSc1
Chaieb	Chaiba	k1gFnPc2
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2018	#num#	k4
<g/>
,	,	kIx,
Lisabon	Lisabon	k1gInSc1
</s>
<s>
Josy	Josa	k1gFnPc1
Santos	Santosa	k1gFnPc2
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
Brazilie	Brazilie	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gInSc5
</s>
<s>
2018	#num#	k4
<g/>
,	,	kIx,
Lisabon	Lisabon	k1gInSc1
</s>
<s>
Luis	Luisa	k1gFnPc2
Gomes	Gomesa	k1gFnPc2
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2018	#num#	k4
<g/>
,	,	kIx,
Lisabon	Lisabon	k1gInSc1
</s>
<s>
Levy	Levy	k?
Sekgapane	Sekgapan	k1gMnSc5
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2017	#num#	k4
<g/>
,	,	kIx,
Astana	Astana	k1gFnSc1
</s>
<s>
Adela	Adela	k1gFnSc1
Zaharia	Zaharium	k1gNnSc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
</s>
<s>
2017	#num#	k4
<g/>
,	,	kIx,
Astana	Astana	k1gFnSc1
</s>
<s>
Kristina	Kristina	k1gFnSc1
Mkhitaryan	Mkhitaryana	k1gFnPc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2017	#num#	k4
<g/>
,	,	kIx,
Astana	Astana	k1gFnSc1
</s>
<s>
Davide	David	k1gMnSc5
Giusti	Giust	k1gFnPc5
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2017	#num#	k4
<g/>
,	,	kIx,
Astana	Astana	k1gFnSc1
</s>
<s>
Maria	Maria	k1gFnSc1
Mudryak	Mudryak	k1gInSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Kazachstan	Kazachstan	k1gInSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2017	#num#	k4
<g/>
,	,	kIx,
Astana	Astana	k1gFnSc1
</s>
<s>
Leon	Leona	k1gFnPc2
Kim	Kim	k1gFnSc2
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2017	#num#	k4
<g/>
,	,	kIx,
Astana	Astana	k1gFnSc1
</s>
<s>
Oksana	Oksana	k1gFnSc1
Sekerina	Sekerina	k1gFnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
Birgit	Birgit	k1gMnSc1
Nilson	Nilson	k1gMnSc1
</s>
<s>
2017	#num#	k4
<g/>
,	,	kIx,
Astana	Astana	k1gFnSc1
</s>
<s>
Boris	Boris	k1gMnSc1
Prýgl	Prýgl	k1gMnSc1
</s>
<s>
basbaryton	basbaryton	k1gMnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
Birgit	Birgit	k1gMnSc1
Nilson	Nilson	k1gMnSc1
</s>
<s>
2017	#num#	k4
<g/>
,	,	kIx,
Astana	Astana	k1gFnSc1
</s>
<s>
Sooyeon	Sooyeon	k1gMnSc1
Lee	Lea	k1gFnSc3
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gMnSc5
</s>
<s>
2017	#num#	k4
<g/>
,	,	kIx,
Astana	Astana	k1gFnSc1
</s>
<s>
Marco	Marco	k6eAd1
Ciaponi	Ciapon	k1gMnPc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2017	#num#	k4
<g/>
,	,	kIx,
Astana	Astana	k1gFnSc1
</s>
<s>
Elsa	Elsa	k1gFnSc1
Dreisig	Dreisiga	k1gFnPc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2016	#num#	k4
<g/>
,	,	kIx,
Guadalajara	Guadalajara	k1gFnSc1
</s>
<s>
Keon-Woo	Keon-Woo	k1gMnSc1
Kim	Kim	k1gMnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2016	#num#	k4
<g/>
,	,	kIx,
Guadalajara	Guadalajara	k1gFnSc1
</s>
<s>
Bogdan	Bogdan	k1gInSc1
Volkov	Volkov	k1gInSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2016	#num#	k4
<g/>
,	,	kIx,
Guadalajara	Guadalajara	k1gFnSc1
</s>
<s>
Elena	Elena	k1gFnSc1
Stikhina	Stikhina	k1gFnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2016	#num#	k4
<g/>
,	,	kIx,
Guadalajara	Guadalajara	k1gFnSc1
</s>
<s>
Juan	Juan	k1gMnSc1
Carlos	Carlos	k1gMnSc1
Heredia	Heredium	k1gNnSc2
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2016	#num#	k4
<g/>
,	,	kIx,
Guadalajara	Guadalajara	k1gFnSc1
</s>
<s>
Olga	Olga	k1gFnSc1
Kulchinskaya	Kulchinskay	k1gInSc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2016	#num#	k4
<g/>
,	,	kIx,
Guadalajara	Guadalajara	k1gFnSc1
</s>
<s>
Lise	Lisa	k1gFnSc3
Davidsen	Davidsna	k1gFnPc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Norsko	Norsko	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
a	a	k8xC
cena	cena	k1gFnSc1
Birgit	Birgita	k1gFnPc2
Nilson	Nilsona	k1gFnPc2
</s>
<s>
2015	#num#	k4
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
</s>
<s>
Ioan	Ioan	k1gMnSc1
Hotea	Hotea	k1gMnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
</s>
<s>
2015	#num#	k4
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
</s>
<s>
Darren	Darrna	k1gFnPc2
Pene	Pen	k1gFnSc2
Pati	Pat	k1gFnSc2
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2015	#num#	k4
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
</s>
<s>
Hyesang	Hyesang	k1gInSc1
Park	park	k1gInSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
</s>
<s>
2015	#num#	k4
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
</s>
<s>
Edward	Edward	k1gMnSc1
Parks	Parksa	k1gFnPc2
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2015	#num#	k4
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
</s>
<s>
Noluvuyiso	Noluvuyisa	k1gFnSc5
Mpofu	Mpof	k1gInSc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2015	#num#	k4
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
</s>
<s>
Kiandra	Kiandra	k1gFnSc1
Howarth	Howartha	k1gFnPc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gMnSc5
</s>
<s>
2015	#num#	k4
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
</s>
<s>
Rachel	Rachel	k1gInSc1
Willis-Sø	Willis-Sø	k1gFnPc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
a	a	k8xC
cena	cena	k1gFnSc1
Birgit	Birgita	k1gFnPc2
Nilson	Nilsona	k1gFnPc2
</s>
<s>
2014	#num#	k4
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc4
</s>
<s>
Mario	Mario	k1gMnSc1
Chang	Chang	k1gMnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Guatemala	Guatemala	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2014	#num#	k4
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc4
</s>
<s>
Amanda	Amanda	k1gFnSc1
Woodbury	Woodbura	k1gFnSc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2014	#num#	k4
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc4
</s>
<s>
Joshua	Joshua	k6eAd1
Guerrero	Guerrero	k1gNnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
USA	USA	kA
<g/>
/	/	kIx~
<g/>
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gInSc5
</s>
<s>
2014	#num#	k4
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc4
</s>
<s>
Andrey	Andrea	k1gFnPc1
Nemzer	Nemzra	k1gFnPc2
</s>
<s>
kontratenor	kontratenor	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2014	#num#	k4
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc4
</s>
<s>
Mariangela	Mariangela	k1gFnSc1
Sicilia	Sicilia	k1gFnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2014	#num#	k4
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc4
</s>
<s>
John	John	k1gMnSc1
Holiday	Holidaa	k1gFnSc2
</s>
<s>
kontratenor	kontratenor	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2014	#num#	k4
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc4
</s>
<s>
Anaï	Anaï	k6eAd1
Constans	Constans	k1gInSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2014	#num#	k4
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc4
</s>
<s>
Ao	Ao	k?
Li	li	k9
</s>
<s>
basbaryton	basbaryton	k1gMnSc1
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2013	#num#	k4
<g/>
,	,	kIx,
Verona	Verona	k1gFnSc1
</s>
<s>
Aida	Aida	k1gFnSc1
Garifullina	Garifullina	k1gFnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2013	#num#	k4
<g/>
,	,	kIx,
Verona	Verona	k1gFnSc1
</s>
<s>
Julie	Julie	k1gFnSc1
Fuchs	Fuchs	k1gMnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2013	#num#	k4
<g/>
,	,	kIx,
Verona	Verona	k1gFnSc1
</s>
<s>
Simone	Simon	k1gMnSc5
Piazzola	Piazzola	k1gFnSc1
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2013	#num#	k4
<g/>
,	,	kIx,
Verona	Verona	k1gFnSc1
</s>
<s>
Kathryn	Kathryn	k1gMnSc1
Lewek	Lewek	k1gMnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2013	#num#	k4
<g/>
,	,	kIx,
Verona	Verona	k1gFnSc1
</s>
<s>
Zach	Zach	k1gMnSc1
Borichevsky	Borichevsko	k1gNnPc7
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2013	#num#	k4
<g/>
,	,	kIx,
Verona	Verona	k1gFnSc1
</s>
<s>
Claudia	Claudia	k1gFnSc1
Huckle	Huckl	k1gInSc5
</s>
<s>
kontraalt	kontraalt	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
král	král	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Cena	cena	k1gFnSc1
Birgit	Birgit	k1gMnSc1
Nilsson	Nilsson	k1gMnSc1
</s>
<s>
2013	#num#	k4
<g/>
,	,	kIx,
Verona	Verona	k1gFnSc1
</s>
<s>
Tracy	Trac	k2eAgFnPc1d1
Cox	Cox	k1gFnPc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Cena	cena	k1gFnSc1
Birgit	Birgit	k1gMnSc1
Nilsson	Nilsson	k1gMnSc1
</s>
<s>
2013	#num#	k4
<g/>
,	,	kIx,
Verona	Verona	k1gFnSc1
</s>
<s>
Vladimir	Vladimir	k1gMnSc1
Dmitruk	Dmitruk	k1gMnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gInSc5
</s>
<s>
2013	#num#	k4
<g/>
,	,	kIx,
Verona	Verona	k1gFnSc1
</s>
<s>
Hae	Hae	k?
Ji	on	k3xPp3gFnSc4
Chang	Chang	k1gMnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2013	#num#	k4
<g/>
,	,	kIx,
Verona	Verona	k1gFnSc1
</s>
<s>
Benjamin	Benjamin	k1gMnSc1
Bliss	Blissa	k1gFnPc2
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2013	#num#	k4
<g/>
,	,	kIx,
Verona	Verona	k1gFnSc1
</s>
<s>
AnthonyRoth	AnthonyRoth	k1gMnSc1
Costanzo	Costanza	k1gFnSc5
</s>
<s>
kontratenor	kontratenor	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2012	#num#	k4
<g/>
,	,	kIx,
Peking	Peking	k1gInSc1
</s>
<s>
EnkhbatynAmartüvshin	EnkhbatynAmartüvshin	k1gMnSc1
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Mongolsko	Mongolsko	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2012	#num#	k4
<g/>
,	,	kIx,
Peking	Peking	k1gInSc1
</s>
<s>
Janai	Janai	k6eAd1
Brugger	Brugger	k1gInSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2012	#num#	k4
<g/>
,	,	kIx,
Peking	Peking	k1gInSc1
</s>
<s>
Guanqun	Guanqun	k1gMnSc1
Yu	Yu	k1gMnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2012	#num#	k4
<g/>
,	,	kIx,
Peking	Peking	k1gInSc1
</s>
<s>
Brian	Brian	k1gMnSc1
Jagde	Jagd	k1gInSc5
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
Birgit	Birgita	k1gFnPc2
Nilsson	Nilssona	k1gFnPc2
</s>
<s>
2012	#num#	k4
<g/>
,	,	kIx,
Peking	Peking	k1gInSc1
</s>
<s>
Yunpeng	Yunpeng	k1gMnSc1
Wang	Wang	k1gMnSc1
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2012	#num#	k4
<g/>
,	,	kIx,
Peking	Peking	k1gInSc1
</s>
<s>
Antonio	Antonio	k1gMnSc1
Poli	pole	k1gFnSc4
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gInSc5
</s>
<s>
2012	#num#	k4
<g/>
,	,	kIx,
Peking	Peking	k1gInSc1
</s>
<s>
Nadezhda	Nadezhda	k1gMnSc1
Karyazina	Karyazina	k1gMnSc1
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2012	#num#	k4
<g/>
,	,	kIx,
Peking	Peking	k1gInSc1
</s>
<s>
Roman	Roman	k1gMnSc1
Burdenko	Burdenka	k1gFnSc5
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2012	#num#	k4
<g/>
,	,	kIx,
Peking	Peking	k1gInSc1
</s>
<s>
Pretty	Pretta	k1gFnPc1
Yende	Yend	k1gMnSc5
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2011	#num#	k4
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
</s>
<s>
René	René	k1gMnSc1
Barbera	Barbero	k1gNnSc2
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2011	#num#	k4
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
</s>
<s>
Olga	Olga	k1gFnSc1
Busuioc	Busuioc	k1gFnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
</s>
<s>
2011	#num#	k4
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
</s>
<s>
Konstantin	Konstantin	k1gMnSc1
Shushakov	Shushakov	k1gInSc4
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2011	#num#	k4
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
</s>
<s>
Olga	Olga	k1gFnSc1
Pudova	Pudov	k1gInSc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2011	#num#	k4
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
</s>
<s>
Jaesig	Jaesig	k1gMnSc1
Lee	Lea	k1gFnSc3
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2011	#num#	k4
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
</s>
<s>
Javier	Javier	k1gInSc1
Arrey	Arrea	k1gFnSc2
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Chile	Chile	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gMnSc5
</s>
<s>
2011	#num#	k4
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
</s>
<s>
Sonya	Sony	k2eAgFnSc1d1
Yoncheva	Yoncheva	k1gFnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Bulharsk	Bulharsk	k1gInSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gMnSc5
</s>
<s>
2010	#num#	k4
<g/>
,	,	kIx,
Milán	Milán	k1gInSc1
</s>
<s>
Stefan	Stefan	k1gMnSc1
Pop	pop	k1gMnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2010	#num#	k4
<g/>
,	,	kIx,
Milán	Milán	k1gInSc1
</s>
<s>
Rosa	Rosa	k1gMnSc1
Feola	Feola	k1gMnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2010	#num#	k4
<g/>
,	,	kIx,
Milán	Milán	k1gInSc1
</s>
<s>
Giordano	Giordana	k1gFnSc5
Lucà	Lucà	k1gFnSc5
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2010	#num#	k4
<g/>
,	,	kIx,
Milán	Milán	k1gInSc1
</s>
<s>
Ievgen	Ievgen	k1gInSc1
Orlov	Orlov	k1gInSc1
</s>
<s>
bas	bas	k1gInSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2010	#num#	k4
<g/>
,	,	kIx,
Milán	Milán	k1gInSc1
</s>
<s>
Dinara	Dinara	k1gFnSc1
Aliyeva	Aliyeva	k1gFnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2010	#num#	k4
<g/>
,	,	kIx,
Milán	Milán	k1gInSc1
</s>
<s>
Chae	Chae	k6eAd1
Jun	jun	k1gMnSc1
Lim	Lima	k1gFnPc2
</s>
<s>
bas	bas	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2010	#num#	k4
<g/>
,	,	kIx,
Milán	Milán	k1gInSc1
</s>
<s>
Nathaniel	Nathaniel	k1gInSc1
Peake	Peak	k1gFnSc2
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2010	#num#	k4
<g/>
,	,	kIx,
Milán	Milán	k1gInSc1
</s>
<s>
Ryan	Ryan	k1gMnSc1
McKinny	McKinna	k1gFnSc2
</s>
<s>
USA	USA	kA
</s>
<s>
2010	#num#	k4
<g/>
,	,	kIx,
Milán	Milán	k1gInSc1
</s>
<s>
Roky	rok	k1gInPc1
2000	#num#	k4
–	–	k?
2009	#num#	k4
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Hlas	hlas	k1gInSc1
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
a	a	k8xC
místo	místo	k1gNnSc1
</s>
<s>
Jordan	Jordan	k1gMnSc1
Bisch	Bisch	k1gMnSc1
</s>
<s>
bas	bas	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2009	#num#	k4
<g/>
,	,	kIx,
Budapešť	Budapešť	k1gFnSc1
</s>
<s>
Angel	angel	k1gMnSc1
Blue	Blu	k1gFnSc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2009	#num#	k4
<g/>
,	,	kIx,
Budapešť	Budapešť	k1gFnSc1
</s>
<s>
Dimitrios	Dimitrios	k1gMnSc1
Flemotomos	Flemotomos	k1gMnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2009	#num#	k4
<g/>
,	,	kIx,
Budapešť	Budapešť	k1gFnSc1
</s>
<s>
Alexey	Alexea	k1gFnPc1
Kudrya	Kudryum	k1gNnSc2
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
zvláštní	zvláštní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
</s>
<s>
2009	#num#	k4
<g/>
,	,	kIx,
Budapešť	Budapešť	k1gFnSc1
</s>
<s>
Julia	Julius	k1gMnSc4
Novikova	Novikův	k2eAgInSc2d1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2009	#num#	k4
<g/>
,	,	kIx,
Budapešť	Budapešť	k1gFnSc1
</s>
<s>
Arnold	Arnold	k1gMnSc1
Rutkowski	Rutkowsk	k1gFnSc2
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gInSc5
</s>
<s>
2009	#num#	k4
<g/>
,	,	kIx,
Budapešť	Budapešť	k1gFnSc1
</s>
<s>
Auxuliadora	Auxuliadora	k1gFnSc1
Toledano	Toledana	k1gFnSc5
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
zvláštní	zvláštní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
</s>
<s>
2009	#num#	k4
<g/>
,	,	kIx,
Budapešť	Budapešť	k1gFnSc1
</s>
<s>
Anita	Anita	k1gMnSc1
Watson	Watson	k1gMnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2009	#num#	k4
<g/>
,	,	kIx,
Budapešť	Budapešť	k1gFnSc1
</s>
<s>
Wenwei	Wenwei	k6eAd1
Zhang	Zhang	k1gInSc1
</s>
<s>
bas	bas	k1gInSc1
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2009	#num#	k4
<g/>
,	,	kIx,
Budapešť	Budapešť	k1gFnSc1
</s>
<s>
Kostas	Kostas	k1gMnSc1
Smoriginas	Smoriginas	k1gMnSc1
</s>
<s>
basbaryton	basbaryton	k1gMnSc1
</s>
<s>
Litva	Litva	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2009	#num#	k4
<g/>
,	,	kIx,
Budapešť	Budapešť	k1gFnSc1
</s>
<s>
María	María	k1gMnSc1
Alejandres	Alejandres	k1gMnSc1
Katzarava	Katzarava	k1gFnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
</s>
<s>
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Québec	Québec	k1gInSc1
</s>
<s>
Joel	Joel	k1gInSc1
Prieto	Prieto	k1gNnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Portoriko	Portoriko	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
a	a	k8xC
CulturArte	CulturArt	k1gInSc5
</s>
<s>
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Québec	Québec	k1gInSc1
</s>
<s>
Thiago	Thiago	k6eAd1
Arancam	Arancam	k1gInSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Brazilie	Brazilie	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
a	a	k8xC
od	od	k7c2
diváků	divák	k1gMnPc2
</s>
<s>
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Québec	Québec	k1gInSc1
</s>
<s>
Oksana	Oksana	k1gFnSc1
Kramaryeva	Kramaryeva	k1gFnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Québec	Québec	k1gInSc1
</s>
<s>
Elena	Elena	k1gFnSc1
Xanthoudakis	Xanthoudakis	k1gFnSc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Řecko	Řecko	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Québec	Québec	k1gInSc1
</s>
<s>
Karoly	Karola	k1gFnPc1
Szemeredy	Szemereda	k1gMnSc2
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Québec	Québec	k1gInSc1
</s>
<s>
Ketevan	Ketevan	k1gMnSc1
Kemoklidze	Kemoklidze	k1gFnSc2
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Québec	Québec	k1gInSc1
</s>
<s>
Ekaterina	Ekaterin	k2eAgFnSc1d1
Lekhina	Lekhina	k1gFnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2007	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Tae	Tae	k?
Joong	Joong	k1gMnSc1
Yang	Yang	k1gMnSc1
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2007	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Olga	Olga	k1gFnSc1
Peretyatko	Peretyatka	k1gFnSc5
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2007	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
David	David	k1gMnSc1
Bižić	Bižić	k1gMnSc1
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2007	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Dmytro	Dmytro	k6eAd1
Popov	Popov	k1gInSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2007	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Marco	Marco	k6eAd1
Caria	Caria	k1gFnSc1
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2007	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Lisette	Lisette	k5eAaPmIp2nP
Oropesa	Oropesa	k1gFnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
</s>
<s>
2007	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Carmen	Carmen	k2eAgInSc1d1
Solis	Solis	k1gInSc1
Gonzalez	Gonzaleza	k1gFnPc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gInSc5
</s>
<s>
2007	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Rachele	Rachel	k1gMnSc5
Gilmore	Gilmor	k1gMnSc5
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2007	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Carine	Carinout	k5eAaPmIp3nS
Sechehaye	Sechehaye	k1gFnSc1
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2007	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Aurelio	Aurelio	k1gMnSc1
Gabaldon	Gabaldon	k1gMnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2007	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Maija	Maij	k2eAgNnPc1d1
Kovaļ	Kovaļ	k1gNnPc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Litva	Litva	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2006	#num#	k4
<g/>
,	,	kIx,
Valencie	Valencie	k1gFnSc1
</s>
<s>
David	David	k1gMnSc1
Lomeli	Lomel	k1gInSc6
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
</s>
<s>
2006	#num#	k4
<g/>
,	,	kIx,
Valencie	Valencie	k1gFnSc1
</s>
<s>
Ailyn	Ailyn	k1gMnSc1
Pérez	Pérez	k1gMnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2006	#num#	k4
<g/>
,	,	kIx,
Valencie	Valencie	k1gFnSc1
</s>
<s>
Sébastien	Sébastien	k1gInSc1
Guè	Guè	k1gFnSc2
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2006	#num#	k4
<g/>
,	,	kIx,
Valencie	Valencie	k1gFnSc1
</s>
<s>
Maria	Maria	k1gFnSc1
Teresa	Teresa	k1gFnSc1
Alberola	Alberola	k1gFnSc1
Banula	Banula	k1gFnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
</s>
<s>
2006	#num#	k4
<g/>
,	,	kIx,
Valencie	Valencie	k1gFnSc1
</s>
<s>
Trevor	Trevor	k1gMnSc1
Scheunemann	Scheunemann	k1gMnSc1
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2006	#num#	k4
<g/>
,	,	kIx,
Valencie	Valencie	k1gFnSc1
</s>
<s>
Karen	Karen	k2eAgInSc1d1
Vuong	Vuong	k1gInSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gMnSc5
</s>
<s>
2006	#num#	k4
<g/>
,	,	kIx,
Valencie	Valencie	k1gFnSc1
</s>
<s>
Susanna	Susanna	k1gFnSc1
Phillips	Phillipsa	k1gFnPc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2005	#num#	k4
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
</s>
<s>
Vasily	Vasila	k1gFnPc1
Ladyuk	Ladyuka	k1gFnPc2
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2005	#num#	k4
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
</s>
<s>
Joseph	Joseph	k1gMnSc1
Kaiser	Kaiser	k1gMnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2005	#num#	k4
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
</s>
<s>
Diogenes	Diogenes	k1gMnSc1
Randes	Randes	k1gMnSc1
</s>
<s>
bas	bas	k1gInSc1
</s>
<s>
Brazilie	Brazilie	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2005	#num#	k4
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
</s>
<s>
David	David	k1gMnSc1
Menendez	Menendez	k1gMnSc1
Diaz	Diaz	k1gMnSc1
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2005	#num#	k4
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
</s>
<s>
Joshua	Joshua	k6eAd1
Langston	Langston	k1gInSc1
Hopkins	Hopkinsa	k1gFnPc2
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2005	#num#	k4
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
</s>
<s>
Arturo	Artura	k1gFnSc5
Chacón	Chacón	k1gMnSc1
Cruz	Cruz	k1gInSc4
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
a	a	k8xC
cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gMnSc5
</s>
<s>
2005	#num#	k4
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
</s>
<s>
Kinga	Kinga	k1gFnSc1
Dobay	Dobaa	k1gFnSc2
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2005	#num#	k4
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
</s>
<s>
Woo	Woo	k?
Kyung	Kyung	k1gMnSc1
Kim	Kim	k1gMnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2004	#num#	k4
<g/>
,	,	kIx,
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
</s>
<s>
Nataliya	Nataliy	k2eAgFnSc1d1
Kovalova	Kovalův	k2eAgFnSc1d1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2004	#num#	k4
<g/>
,	,	kIx,
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
</s>
<s>
Dimitry	Dimitr	k1gInPc1
Voropaev	Voropava	k1gFnPc2
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2004	#num#	k4
<g/>
,	,	kIx,
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
</s>
<s>
Mikhail	Mikhail	k1gMnSc1
Petrenko	Petrenka	k1gFnSc5
</s>
<s>
bas	bas	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2004	#num#	k4
<g/>
,	,	kIx,
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
</s>
<s>
Maria	Maria	k1gFnSc1
Jooste	Joost	k1gInSc5
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2004	#num#	k4
<g/>
,	,	kIx,
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
</s>
<s>
Vitaly	Vitala	k1gFnPc1
Bilyy	Bilya	k1gFnSc2
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2004	#num#	k4
<g/>
,	,	kIx,
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
</s>
<s>
Dmitry	Dmitr	k1gInPc1
Korchak	Korchak	k1gInSc4
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
</s>
<s>
2004	#num#	k4
<g/>
,	,	kIx,
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
</s>
<s>
Irina	Irina	k1gFnSc1
Lungu	Lung	k1gInSc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gInSc5
</s>
<s>
2004	#num#	k4
<g/>
,	,	kIx,
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
</s>
<s>
In-Sung	In-Sung	k1gInSc1
Sim	sima	k1gFnPc2
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Jižná	jižný	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2004	#num#	k4
<g/>
,	,	kIx,
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
</s>
<s>
Adriana	Adriana	k1gFnSc1
Damato	Damat	k2eAgNnSc1d1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2003	#num#	k4
<g/>
,	,	kIx,
Bodensee	Bodensee	k1gFnSc1
</s>
<s>
Jozef	Jozef	k1gMnSc1
Gjipali	Gjipali	k1gMnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Albánie	Albánie	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2003	#num#	k4
<g/>
,	,	kIx,
Bodensee	Bodensee	k1gFnSc1
</s>
<s>
Israel	Israel	k1gMnSc1
Lozano	Lozana	k1gFnSc5
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
a	a	k8xC
od	od	k7c2
diváků	divák	k1gMnPc2
</s>
<s>
2003	#num#	k4
<g/>
,	,	kIx,
Bodensee	Bodensee	k1gFnSc1
</s>
<s>
Jesus	Jesus	k1gInSc1
Garcia	Garcium	k1gNnSc2
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2003	#num#	k4
<g/>
,	,	kIx,
Bodensee	Bodensee	k1gFnSc1
</s>
<s>
Sabina	Sabina	k1gMnSc1
Puértolas	Puértolas	k1gMnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2003	#num#	k4
<g/>
,	,	kIx,
Bodensee	Bodensee	k1gFnSc1
</s>
<s>
Jennifer	Jennifer	k1gMnSc1
Check	Check	k1gMnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2003	#num#	k4
<g/>
,	,	kIx,
Bodensee	Bodensee	k1gFnSc1
</s>
<s>
Mario	Mario	k1gMnSc1
Cassi	Cass	k1gMnSc3
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2003	#num#	k4
<g/>
,	,	kIx,
Bodensee	Bodensee	k1gFnSc1
</s>
<s>
Carmen	Carmen	k2eAgMnSc1d1
Giannattasio	Giannattasio	k1gMnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2002	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Elena	Elena	k1gFnSc1
Manistina	Manistina	k1gFnSc1
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2002	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Stéphane	Stéphanout	k5eAaPmIp3nS
Degout	Degout	k1gMnSc1
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2002	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
John	John	k1gMnSc1
Matz	Matz	k1gMnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
</s>
<s>
2002	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Maria	Maria	k1gFnSc1
Fontosh	Fontosha	k1gFnPc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2002	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Kate	kat	k1gMnSc5
Aldrich	Aldrich	k1gMnSc1
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gInSc5
</s>
<s>
2002	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Anna	Anna	k1gFnSc1
Kiknadze	Kiknadze	k1gFnSc2
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2002	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Jae	Jae	k?
Hyoung	Hyoung	k1gMnSc1
Kim	Kim	k1gMnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2002	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Guang	Guang	k1gMnSc1
Yang	Yang	k1gMnSc1
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2001	#num#	k4
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
</s>
<s>
Alessandra	Alessandra	k1gFnSc1
Rezza	Rezza	k1gFnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2001	#num#	k4
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
</s>
<s>
Hyoung-Kyoo	Hyoung-Kyoo	k1gMnSc1
Kang	Kang	k1gMnSc1
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2001	#num#	k4
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
</s>
<s>
Maya	Maya	k?
Daskuk	Daskuk	k1gInSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2001	#num#	k4
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
</s>
<s>
Lasha	Lasha	k1gFnSc1
Nikabadze	Nikabadze	k1gFnSc2
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gInSc5
</s>
<s>
2001	#num#	k4
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
</s>
<s>
Elisaveta	Elisaveta	k1gFnSc1
Martirosyan	Martirosyana	k1gFnPc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2001	#num#	k4
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
</s>
<s>
Valeriano	Valeriana	k1gFnSc5
Lanchas	Lanchas	k1gMnSc1
</s>
<s>
bas	bas	k1gInSc1
</s>
<s>
Kolumbie	Kolumbie	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2001	#num#	k4
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
</s>
<s>
Jossie	Jossie	k1gFnSc1
Perez	Pereza	k1gFnPc2
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2001	#num#	k4
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
</s>
<s>
Antonio	Antonio	k1gMnSc1
Gandia	Gandium	k1gNnSc2
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2001	#num#	k4
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
</s>
<s>
Eugenia	Eugenium	k1gNnPc1
Garza	Garza	k1gFnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Mexilo	Mexit	k5eAaImAgNnS,k5eAaPmAgNnS,k5eAaBmAgNnS
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2001	#num#	k4
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
</s>
<s>
Isabel	Isabela	k1gFnPc2
Bayrakdarian	Bayrakdariana	k1gFnPc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2000	#num#	k4
<g/>
,	,	kIx,
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
</s>
<s>
He	he	k0
Hui	Hui	k1gFnPc5
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2000	#num#	k4
<g/>
,	,	kIx,
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
</s>
<s>
Daniil	Daniil	k1gMnSc1
Shtoda	Shtoda	k1gMnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2000	#num#	k4
<g/>
,	,	kIx,
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
</s>
<s>
Robert	Robert	k1gMnSc1
Pomakov	Pomakov	k1gInSc4
</s>
<s>
bas	bas	k1gInSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
2000	#num#	k4
<g/>
,	,	kIx,
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
</s>
<s>
Konstyantyn	Konstyantyn	k1gNnSc1
Andreyev	Andreyva	k1gFnPc2
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
</s>
<s>
2000	#num#	k4
<g/>
,	,	kIx,
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
</s>
<s>
Arnold	Arnold	k1gMnSc1
Kocharyan	Kocharyan	k1gMnSc1
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Arménie	Arménie	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
2000	#num#	k4
<g/>
,	,	kIx,
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
</s>
<s>
Virginia	Virginium	k1gNnPc1
Tola	Tol	k2eAgNnPc1d1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Argentina	Argentina	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
2000	#num#	k4
<g/>
,	,	kIx,
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
</s>
<s>
Roky	rok	k1gInPc1
1993	#num#	k4
–	–	k?
1999	#num#	k4
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
Hlas	hlas	k1gInSc1
</s>
<s>
Země	země	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
a	a	k8xC
místo	místo	k1gNnSc1
</s>
<s>
Orlin	Orlin	k2eAgInSc1d1
Anastassov	Anastassov	k1gInSc1
</s>
<s>
bas	bas	k1gInSc1
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
1999	#num#	k4
<g/>
,	,	kIx,
Portoriko	Portoriko	k1gNnSc1
</s>
<s>
Joseph	Joseph	k1gMnSc1
Calleja	Calleja	k1gMnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Malta	Malta	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gMnSc5
</s>
<s>
1999	#num#	k4
<g/>
,	,	kIx,
Portoriko	Portoriko	k1gNnSc1
</s>
<s>
Mariola	Mariola	k1gFnSc1
Cantarero	Cantarero	k1gNnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
1999	#num#	k4
<g/>
,	,	kIx,
Portoriko	Portoriko	k1gNnSc1
</s>
<s>
Giuseppe	Giuseppat	k5eAaPmIp3nS
Filianoti	Filianot	k1gMnPc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
1999	#num#	k4
<g/>
,	,	kIx,
Portoriko	Portoriko	k1gNnSc1
</s>
<s>
Vitalij	Vitalít	k5eAaPmRp2nS
Kowaljow	Kowaljow	k1gMnSc5
</s>
<s>
bas	bas	k1gInSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gInSc5
</s>
<s>
1999	#num#	k4
<g/>
,	,	kIx,
Portoriko	Portoriko	k1gNnSc1
</s>
<s>
Rolando	Rolando	k6eAd1
Villazón	Villazón	k1gInSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
cena	cena	k1gFnSc1
za	za	k7c4
Zarzuelu	Zarzuela	k1gFnSc4
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
1999	#num#	k4
<g/>
,	,	kIx,
Portoriko	Portoriko	k1gNnSc1
</s>
<s>
Yali-Marie	Yali-Marie	k1gFnSc1
Williams	Williamsa	k1gFnPc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Portoriko	Portoriko	k1gNnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
1999	#num#	k4
<g/>
,	,	kIx,
Portoriko	Portoriko	k1gNnSc1
</s>
<s>
Bae	Bae	k?
Jae-chul	Jae-chul	k1gInSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
CulturArte	CulturArt	k1gInSc5
</s>
<s>
1998	#num#	k4
<g/>
,	,	kIx,
Hamburg	Hamburg	k1gInSc1
</s>
<s>
Carlos	Carlos	k1gMnSc1
Cosías	Cosías	k1gMnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
1998	#num#	k4
<g/>
,	,	kIx,
Hamburg	Hamburg	k1gInSc1
</s>
<s>
Joyce	Joyko	k6eAd1
DiDonato	DiDonat	k2eAgNnSc1d1
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
1998	#num#	k4
<g/>
,	,	kIx,
Hamburg	Hamburg	k1gInSc1
</s>
<s>
Andión	Andión	k1gMnSc1
Fernández	Fernández	k1gMnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Filipíny	Filipíny	k1gFnPc4
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
1998	#num#	k4
<g/>
,	,	kIx,
Hamburg	Hamburg	k1gInSc1
</s>
<s>
Maki	Maki	k1gNnSc1
Mori	Mor	k1gFnSc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
1998	#num#	k4
<g/>
,	,	kIx,
Hamburg	Hamburg	k1gInSc1
</s>
<s>
Erwin	Erwin	k1gMnSc1
Schrott	Schrott	k1gMnSc1
</s>
<s>
bas	bas	k1gInSc1
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
1998	#num#	k4
<g/>
,	,	kIx,
Hamburg	Hamburg	k1gInSc1
</s>
<s>
Ludovic	Ludovice	k1gFnPc2
Tezier	Tezira	k1gFnPc2
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
1998	#num#	k4
<g/>
,	,	kIx,
Hamburg	Hamburg	k1gInSc1
</s>
<s>
Carla	Carl	k1gMnSc4
Maria	Mario	k1gMnSc4
Izzo	Izzo	k6eAd1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Italie	Italie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
1997	#num#	k4
<g/>
,	,	kIx,
Tokio	Tokio	k1gNnSc1
</s>
<s>
Chang	Chang	k1gMnSc1
Yong	Yong	k1gMnSc1
Liao	Liao	k1gMnSc1
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
1997	#num#	k4
<g/>
,	,	kIx,
Tokio	Tokio	k1gNnSc1
</s>
<s>
Aquiles	Aquiles	k1gMnSc1
Machado	Machada	k1gFnSc5
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Venezuela	Venezuela	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
1997	#num#	k4
<g/>
,	,	kIx,
Tokio	Tokio	k1gNnSc1
</s>
<s>
Angel	angel	k1gMnSc1
Rodriguez	Rodriguez	k1gMnSc1
Rivero	Rivero	k1gNnSc4
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
1997	#num#	k4
<g/>
,	,	kIx,
Tokio	Tokio	k1gNnSc1
</s>
<s>
Seo	Seo	k?
Jung-hack	Jung-hack	k1gInSc1
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
1997	#num#	k4
<g/>
,	,	kIx,
Tokio	Tokio	k1gNnSc1
</s>
<s>
Sun	Sun	kA
Xiuwei	Xiuwei	k1gNnSc1
</s>
<s>
siprán	siprán	k1gInSc1
</s>
<s>
Čína	Čína	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
1997	#num#	k4
<g/>
,	,	kIx,
Tokio	Tokio	k1gNnSc1
</s>
<s>
Lynette	Lynette	k5eAaPmIp2nP
Tapia	Tapia	k1gFnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
1996	#num#	k4
<g/>
,	,	kIx,
Bordeaux	Bordeaux	k1gNnSc1
</s>
<s>
John	John	k1gMnSc1
Osborn	Osborn	k1gMnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
1996	#num#	k4
<g/>
,	,	kIx,
Bordeaux	Bordeaux	k1gNnSc1
</s>
<s>
Eric	Eric	k1gFnSc1
Owens	Owensa	k1gFnPc2
</s>
<s>
bas	bas	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
1996	#num#	k4
<g/>
,	,	kIx,
Bordeaux	Bordeaux	k1gNnSc1
</s>
<s>
Phyllis	Phyllis	k1gFnSc1
Pancella	Pancella	k1gFnSc1
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
1996	#num#	k4
<g/>
,	,	kIx,
Bordeaux	Bordeaux	k1gNnSc1
</s>
<s>
Vittorio	Vittorio	k6eAd1
Vitelli	Vitell	k1gMnPc1
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
1996	#num#	k4
<g/>
,	,	kIx,
Bordeaux	Bordeaux	k1gNnSc1
</s>
<s>
Oziel	Oziel	k1gMnSc1
Garza-Ornelas	Garza-Ornelas	k1gMnSc1
</s>
<s>
baryton	baryton	k1gMnSc1
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
1996	#num#	k4
<g/>
,	,	kIx,
Bordeaux	Bordeaux	k1gNnSc1
</s>
<s>
Nancy	Nancy	k1gFnSc1
Herrera	Herrera	k1gFnSc1
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
1996	#num#	k4
<g/>
,	,	kIx,
Bordeaux	Bordeaux	k1gNnSc1
</s>
<s>
Carlos	Carlos	k1gMnSc1
Moreno	Moren	k2eAgNnSc4d1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
1996	#num#	k4
<g/>
,	,	kIx,
Bordeaux	Bordeaux	k1gNnSc1
</s>
<s>
Sung	Sung	k1gMnSc1
Eun	Eun	k1gMnSc1
Kim	Kim	k1gMnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
1995	#num#	k4
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
</s>
<s>
Miguel	Miguel	k1gMnSc1
Angel	angel	k1gMnSc1
Zapater	Zapater	k1gMnSc1
Tapia	Tapia	k1gFnSc1
</s>
<s>
bas	bas	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
1995	#num#	k4
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
</s>
<s>
Elizabeth	Elizabeth	k1gFnSc1
Futral	Futral	k1gFnSc2
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
1995	#num#	k4
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
</s>
<s>
Ana	Ana	k?
María	María	k1gMnSc1
Martínez	Martínez	k1gMnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
1995	#num#	k4
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
</s>
<s>
Carmen	Carmen	k1gInSc1
Oprisanu	Oprisan	k1gInSc2
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
cena	cena	k1gFnSc1
</s>
<s>
1995	#num#	k4
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
</s>
<s>
Rafael	Rafael	k1gMnSc1
Rojas	Rojas	k1gMnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Zarzuela	Zarzuel	k1gMnSc2
</s>
<s>
1995	#num#	k4
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
</s>
<s>
Dimitra	Dimitra	k1gFnSc1
Theodossiou	Theodossia	k1gFnSc7
</s>
<s>
sopprán	sopprán	k1gInSc1
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
cena	cena	k1gFnSc1
a	a	k8xC
cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
1995	#num#	k4
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
</s>
<s>
Simone	Simon	k1gMnSc5
Alberghini	Alberghin	k1gMnPc5
</s>
<s>
bas	bas	k1gInSc1
</s>
<s>
Italie	Italie	k1gFnSc1
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
1994	#num#	k4
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Oxana	Oxaen	k2eAgFnSc1d1
Arkaeva	Arkaeva	k1gFnSc1
</s>
<s>
sopran	sopran	k1gInSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
1994	#num#	k4
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Brian	Brian	k1gMnSc1
Asawa	Asawa	k1gMnSc1
</s>
<s>
kontratenor	kontratenor	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
1994	#num#	k4
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
José	José	k6eAd1
Cura	Cur	k2eAgFnSc1d1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
Argentina	Argentina	k1gFnSc1
</s>
<s>
Ocenění	ocenění	k1gNnSc1
a	a	k8xC
Cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
1994	#num#	k4
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Maria-Cécilia	Maria-Cécilia	k1gFnSc1
Diaz	Diaza	k1gFnPc2
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
Argentina	Argentina	k1gFnSc1
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
1994	#num#	k4
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Bruce	Bruce	k1gMnSc1
Fowler	Fowler	k1gMnSc1
</s>
<s>
tenor	tenor	k1gInSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
1994	#num#	k4
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Masako	Masako	k6eAd1
Teshima	Teshima	k1gFnSc1
</s>
<s>
mezzosoprán	mezzosoprán	k1gInSc1
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
1994	#num#	k4
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
</s>
<s>
Ainhoa	Ainho	k2eAgFnSc1d1
Arteta	Arteta	k1gFnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Ocenění	ocenění	k1gNnSc1
a	a	k8xC
Cena	cena	k1gFnSc1
diváků	divák	k1gMnPc2
</s>
<s>
1993	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Inva	Inv	k2eAgFnSc1d1
Mula	mula	k1gFnSc1
Tschako	Tschako	k1gNnSc1
</s>
<s>
soprán	soprán	k1gInSc1
</s>
<s>
Albanie	Albanie	k1gFnSc1
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
1993	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Nina	Nina	k1gFnSc1
Stemme	Stemme	k1gFnSc2
</s>
<s>
mezzosopáan	mezzosopáan	k1gMnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
1993	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Kwangchul	Kwangchul	k1gInSc1
Youn	Youna	k1gFnPc2
</s>
<s>
bas	bas	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
1993	#num#	k4
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Operalia	Operalium	k1gNnSc2
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
,	,	kIx,
Operalia	Operalia	k1gFnSc1
<g/>
,	,	kIx,
The	The	k1gFnSc1
World	Worlda	k1gFnPc2
Opera	opera	k1gFnSc1
Competition	Competition	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Operalia	Operalia	k1gFnSc1
na	na	k7c6
polské	polský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
Webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
</s>
<s>
Domingova	Domingův	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
na	na	k7c6
iDNES	iDNES	k?
</s>
<s>
Operalia	Operalia	k1gFnSc1
na	na	k7c6
OPERA	opera	k1gFnSc1
<g/>
+	+	kIx~
</s>
<s>
Ceny	cena	k1gFnPc1
a	a	k8xC
odměny	odměna	k1gFnPc1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
</s>
