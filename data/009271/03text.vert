<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Turkmenistánu	Turkmenistán	k1gInSc2	Turkmenistán
má	mít	k5eAaImIp3nS	mít
zelený	zelený	k2eAgInSc1d1	zelený
list	list	k1gInSc1	list
<g/>
,	,	kIx,	,
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
při	při	k7c6	při
stožáru	stožár	k1gInSc6	stožár
nalézá	nalézat	k5eAaImIp3nS	nalézat
kobercový	kobercový	k2eAgInSc4d1	kobercový
motiv	motiv	k1gInSc4	motiv
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
něho	on	k3xPp3gMnSc2	on
nahoře	nahoře	k6eAd1	nahoře
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
půlměsíc	půlměsíc	k1gInSc1	půlměsíc
a	a	k8xC	a
pět	pět	k4xCc1	pět
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
<g/>
Vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
nejsložitější	složitý	k2eAgInSc4d3	nejsložitější
vzor	vzor	k1gInSc4	vzor
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
vlajek	vlajka	k1gFnPc2	vlajka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Turkmenistán	Turkmenistán	k1gInSc1	Turkmenistán
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgMnSc1d1	významný
výrobce	výrobce	k1gMnSc1	výrobce
koberců	koberec	k1gInPc2	koberec
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
kobercový	kobercový	k2eAgInSc1d1	kobercový
motiv	motiv	k1gInSc1	motiv
dostal	dostat	k5eAaPmAgInS	dostat
i	i	k9	i
na	na	k7c4	na
turkmenskou	turkmenský	k2eAgFnSc4d1	Turkmenská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
pět	pět	k4xCc4	pět
vzorů	vzor	k1gInPc2	vzor
kobercového	kobercový	k2eAgInSc2d1	kobercový
svislého	svislý	k2eAgInSc2d1	svislý
pruhu	pruh	k1gInSc2	pruh
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
pět	pět	k4xCc1	pět
hlavních	hlavní	k2eAgFnPc2d1	hlavní
skupin	skupina	k1gFnPc2	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
pět	pět	k4xCc1	pět
provincií	provincie	k1gFnPc2	provincie
země	zem	k1gFnSc2	zem
–	–	k?	–
Achal	achat	k5eAaImAgMnS	achat
<g/>
,	,	kIx,	,
Bałkan	Bałkan	k1gMnSc1	Bałkan
<g/>
,	,	kIx,	,
Lebap	Lebap	k1gMnSc1	Lebap
<g/>
,	,	kIx,	,
Mary	Mary	k1gFnSc1	Mary
a	a	k8xC	a
Taszauz	Taszauz	k1gInSc1	Taszauz
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	Zelená	k1gFnSc1	Zelená
je	být	k5eAaImIp3nS	být
barvou	barva	k1gFnSc7	barva
islámu	islám	k1gInSc2	islám
a	a	k8xC	a
půlměsíc	půlměsíc	k1gInSc1	půlměsíc
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
muslimské	muslimský	k2eAgFnSc2d1	muslimská
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
podoba	podoba	k1gFnSc1	podoba
vlajky	vlajka	k1gFnSc2	vlajka
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
užívala	užívat	k5eAaImAgFnS	užívat
vlajka	vlajka	k1gFnSc1	vlajka
Turkmenské	turkmenský	k2eAgFnSc2d1	Turkmenská
SSR	SSR	kA	SSR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Turkménska	Turkménsko	k1gNnPc4	Turkménsko
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Turkmenistánu	Turkmenistán	k1gInSc2	Turkmenistán
</s>
</p>
<p>
<s>
Turkmenská	turkmenský	k2eAgFnSc1d1	Turkmenská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vlajka	vlajka	k1gFnSc1	vlajka
Turkmenistánu	Turkmenistán	k1gInSc6	Turkmenistán
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
