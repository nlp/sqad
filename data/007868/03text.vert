<s>
Křovinář	křovinář	k1gMnSc1	křovinář
němý	němý	k2eAgMnSc1d1	němý
(	(	kIx(	(
<g/>
Lachesis	Lachesis	k1gInSc1	Lachesis
muta	mut	k1gInSc2	mut
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
druh	druh	k1gInSc1	druh
chřestýšovitých	chřestýšovitý	k2eAgMnPc2d1	chřestýšovitý
hadů	had	k1gMnPc2	had
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
není	být	k5eNaImIp3nS	být
živorodý	živorodý	k2eAgMnSc1d1	živorodý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
klade	klást	k5eAaImIp3nS	klást
vejce	vejce	k1gNnSc1	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
délkou	délka	k1gFnSc7	délka
až	až	k9	až
360	[number]	k4	360
cm	cm	kA	cm
největším	veliký	k2eAgInSc7d3	veliký
druhem	druh	k1gInSc7	druh
chřestýšovitých	chřestýšovití	k1gMnPc2	chřestýšovití
i	i	k8xC	i
všech	všecek	k3xTgMnPc2	všecek
jedovatých	jedovatý	k2eAgMnPc2d1	jedovatý
hadů	had	k1gMnPc2	had
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
jedovatost	jedovatost	k1gFnSc4	jedovatost
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
domorodé	domorodý	k2eAgNnSc4d1	domorodé
jméno	jméno	k1gNnSc4	jméno
bushmaster	bushmastra	k1gFnPc2	bushmastra
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
vládce	vládce	k1gMnSc4	vládce
buše	buš	k1gFnSc2	buš
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
had	had	k1gMnSc1	had
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
vlhčích	vlhký	k2eAgInPc6d2	vlhčí
pralesích	prales	k1gInPc6	prales
Střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
z	z	k7c2	z
Karibských	karibský	k2eAgInPc2d1	karibský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c6	na
území	území	k1gNnSc6	území
těchto	tento	k3xDgInPc2	tento
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
Kostarika	Kostarika	k1gFnSc1	Kostarika
Panama	Panama	k1gFnSc1	Panama
Trinidad	Trinidad	k1gInSc4	Trinidad
Brazílie	Brazílie	k1gFnSc2	Brazílie
Peru	Peru	k1gNnSc2	Peru
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
také	také	k9	také
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
Amazonce	Amazonka	k1gFnSc6	Amazonka
Může	moct	k5eAaImIp3nS	moct
dorůst	dorůst	k5eAaPmF	dorůst
délky	délka	k1gFnSc2	délka
až	až	k9	až
360	[number]	k4	360
cm	cm	kA	cm
<g/>
,	,	kIx,	,
průměrně	průměrně	k6eAd1	průměrně
však	však	k9	však
jen	jen	k9	jen
210	[number]	k4	210
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
světle	světle	k6eAd1	světle
hnědé	hnědý	k2eAgMnPc4d1	hnědý
až	až	k6eAd1	až
narůžovělé	narůžovělý	k2eAgInPc1d1	narůžovělý
s	s	k7c7	s
tmavými	tmavý	k2eAgFnPc7d1	tmavá
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
,	,	kIx,	,
pruhy	pruh	k1gInPc7	pruh
či	či	k8xC	či
mřížkovitou	mřížkovitý	k2eAgFnSc7d1	mřížkovitý
kresbou	kresba	k1gFnSc7	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
zploštělá	zploštělý	k2eAgFnSc1d1	zploštělá
<g/>
,	,	kIx,	,
shora	shora	k6eAd1	shora
má	mít	k5eAaImIp3nS	mít
trojúhelníkovitý	trojúhelníkovitý	k2eAgInSc1d1	trojúhelníkovitý
nebo	nebo	k8xC	nebo
šípovitý	šípovitý	k2eAgInSc1d1	šípovitý
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
typický	typický	k2eAgInSc1d1	typický
pro	pro	k7c4	pro
křovináře	křovinář	k1gMnPc4	křovinář
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
pravých	pravý	k2eAgMnPc2d1	pravý
chřestýšů	chřestýš	k1gMnPc2	chřestýš
nemá	mít	k5eNaImIp3nS	mít
vyvinuté	vyvinutý	k2eAgNnSc1d1	vyvinuté
chřestidlo	chřestidlo	k1gNnSc1	chřestidlo
a	a	k8xC	a
ani	ani	k8xC	ani
před	před	k7c7	před
útokem	útok	k1gInSc7	útok
nevaruje	varovat	k5eNaImIp3nS	varovat
syčením	syčení	k1gNnSc7	syčení
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
proto	proto	k8xC	proto
získal	získat	k5eAaPmAgInS	získat
své	svůj	k3xOyFgNnSc4	svůj
druhové	druhový	k2eAgNnSc4d1	druhové
jméno	jméno	k1gNnSc4	jméno
němý	němý	k2eAgInSc1d1	němý
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
obávaný	obávaný	k2eAgInSc1d1	obávaný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
noční	noční	k2eAgInSc1d1	noční
druh	druh	k1gInSc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
tráví	trávit	k5eAaImIp3nS	trávit
v	v	k7c6	v
úkrytu	úkryt	k1gInSc6	úkryt
v	v	k7c6	v
podzemní	podzemní	k2eAgFnSc6d1	podzemní
noře	nora	k1gFnSc6	nora
či	či	k8xC	či
v	v	k7c6	v
hustém	hustý	k2eAgNnSc6d1	husté
křoví	křoví	k1gNnSc6	křoví
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
po	po	k7c6	po
setmění	setmění	k1gNnSc6	setmění
vyráží	vyrážet	k5eAaImIp3nS	vyrážet
za	za	k7c7	za
potravou	potrava	k1gFnSc7	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
především	především	k9	především
drobnými	drobný	k2eAgMnPc7d1	drobný
savci	savec	k1gMnPc7	savec
<g/>
,	,	kIx,	,
jinými	jiný	k2eAgInPc7d1	jiný
plazy	plaz	k1gInPc7	plaz
či	či	k8xC	či
žábami	žába	k1gFnPc7	žába
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
lovu	lov	k1gInSc6	lov
savců	savec	k1gMnPc2	savec
mu	on	k3xPp3gMnSc3	on
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
tepločivné	tepločivný	k2eAgFnPc1d1	tepločivný
jamky	jamka	k1gFnPc1	jamka
na	na	k7c6	na
čenichu	čenich	k1gInSc6	čenich
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
si	se	k3xPyFc3	se
hledá	hledat	k5eAaImIp3nS	hledat
na	na	k7c4	na
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
na	na	k7c4	na
stromy	strom	k1gInPc4	strom
nešplhá	šplhat	k5eNaImIp3nS	šplhat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
jed	jed	k1gInSc1	jed
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
hemotoxický	hemotoxický	k2eAgInSc1d1	hemotoxický
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
oběti	oběť	k1gFnSc2	oběť
je	být	k5eAaImIp3nS	být
vstřikován	vstřikovat	k5eAaImNgInS	vstřikovat
jedovými	jedový	k2eAgInPc7d1	jedový
zuby	zub	k1gInPc7	zub
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
délka	délka	k1gFnSc1	délka
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
velkého	velký	k2eAgMnSc2d1	velký
křovináře	křovinář	k1gMnSc2	křovinář
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
37	[number]	k4	37
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc4	jeho
uštknutí	uštknutí	k1gNnPc4	uštknutí
díky	díky	k7c3	díky
účinnosti	účinnost	k1gFnSc3	účinnost
i	i	k8xC	i
množství	množství	k1gNnSc1	množství
jedu	jed	k1gInSc2	jed
končí	končit	k5eAaImIp3nS	končit
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
bez	bez	k7c2	bez
lékařské	lékařský	k2eAgFnSc2d1	lékařská
pomoci	pomoc	k1gFnSc2	pomoc
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
smrtelně	smrtelně	k6eAd1	smrtelně
a	a	k8xC	a
člověk	člověk	k1gMnSc1	člověk
může	moct	k5eAaImIp3nS	moct
zemřít	zemřít	k5eAaPmF	zemřít
do	do	k7c2	do
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikde	nikde	k6eAd1	nikde
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
hojný	hojný	k2eAgMnSc1d1	hojný
<g/>
,	,	kIx,	,
však	však	k9	však
jeho	jeho	k3xOp3gNnSc4	jeho
uštknutí	uštknutí	k1gNnSc4	uštknutí
nejsou	být	k5eNaImIp3nP	být
příliš	příliš	k6eAd1	příliš
častá	častý	k2eAgNnPc4d1	časté
<g/>
.	.	kIx.	.
</s>
<s>
Chřestýšovití	Chřestýšovití	k1gMnPc1	Chřestýšovití
Křovinář	křovinář	k1gMnSc1	křovinář
Křovinář	křovinář	k1gMnSc1	křovinář
sametový	sametový	k2eAgMnSc1d1	sametový
</s>
