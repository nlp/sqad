<s>
Křovinář	křovinář	k1gMnSc1	křovinář
němý	němý	k2eAgMnSc1d1	němý
(	(	kIx(	(
<g/>
Lachesis	Lachesis	k1gInSc1	Lachesis
muta	mut	k1gInSc2	mut
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
druh	druh	k1gInSc1	druh
chřestýšovitých	chřestýšovitý	k2eAgMnPc2d1	chřestýšovitý
hadů	had	k1gMnPc2	had
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
není	být	k5eNaImIp3nS	být
živorodý	živorodý	k2eAgMnSc1d1	živorodý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
klade	klást	k5eAaImIp3nS	klást
vejce	vejce	k1gNnSc1	vejce
<g/>
.	.	kIx.	.
</s>
