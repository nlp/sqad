<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
dřeviny	dřevina	k1gFnPc1	dřevina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
kmen	kmen	k1gInSc4	kmen
a	a	k8xC	a
bývají	bývat	k5eAaImIp3nP	bývat
rozvětvené	rozvětvený	k2eAgInPc4d1	rozvětvený
již	již	k6eAd1	již
nízko	nízko	k6eAd1	nízko
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
?	?	kIx.	?
</s>
