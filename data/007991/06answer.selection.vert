<s>
Keř	keř	k1gInSc1	keř
<g/>
,	,	kIx,	,
zdrobněle	zdrobněle	k6eAd1	zdrobněle
také	také	k9	také
keřík	keřík	k1gInSc1	keřík
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dřevnatá	dřevnatý	k2eAgFnSc1d1	dřevnatá
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
hlavní	hlavní	k2eAgFnSc7d1	hlavní
odlišností	odlišnost	k1gFnSc7	odlišnost
od	od	k7c2	od
stromu	strom	k1gInSc2	strom
je	být	k5eAaImIp3nS	být
absence	absence	k1gFnSc1	absence
hlavního	hlavní	k2eAgInSc2d1	hlavní
kmene	kmen	k1gInSc2	kmen
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
nízko	nízko	k6eAd1	nízko
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
se	se	k3xPyFc4	se
keře	keř	k1gInPc1	keř
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
větví	větev	k1gFnPc2	větev
<g/>
.	.	kIx.	.
</s>
