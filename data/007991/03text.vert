<s>
Keř	keř	k1gInSc1	keř
<g/>
,	,	kIx,	,
zdrobněle	zdrobněle	k6eAd1	zdrobněle
také	také	k9	také
keřík	keřík	k1gInSc1	keřík
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dřevnatá	dřevnatý	k2eAgFnSc1d1	dřevnatá
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
hlavní	hlavní	k2eAgFnSc7d1	hlavní
odlišností	odlišnost	k1gFnSc7	odlišnost
od	od	k7c2	od
stromu	strom	k1gInSc2	strom
je	být	k5eAaImIp3nS	být
absence	absence	k1gFnSc1	absence
hlavního	hlavní	k2eAgInSc2d1	hlavní
kmene	kmen	k1gInSc2	kmen
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
nízko	nízko	k6eAd1	nízko
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
se	se	k3xPyFc4	se
keře	keř	k1gInPc1	keř
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
větví	větev	k1gFnPc2	větev
<g/>
.	.	kIx.	.
</s>
<s>
Keře	keř	k1gInPc1	keř
bývají	bývat	k5eAaImIp3nP	bývat
nižší	nízký	k2eAgInPc1d2	nižší
než	než	k8xS	než
stromy	strom	k1gInPc1	strom
a	a	k8xC	a
vyšší	vysoký	k2eAgInPc1d2	vyšší
než	než	k8xS	než
trávy	tráva	k1gFnPc4	tráva
a	a	k8xC	a
byliny	bylina	k1gFnPc4	bylina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ovocnářství	ovocnářství	k1gNnSc6	ovocnářství
jsou	být	k5eAaImIp3nP	být
keře	keř	k1gInPc1	keř
označením	označení	k1gNnSc7	označení
pro	pro	k7c4	pro
ovocné	ovocný	k2eAgFnPc4d1	ovocná
dřeviny	dřevina	k1gFnPc4	dřevina
bez	bez	k7c2	bez
kmínku	kmínek	k1gInSc2	kmínek
<g/>
.	.	kIx.	.
</s>
<s>
Keře	keř	k1gInPc1	keř
mají	mít	k5eAaImIp3nP	mít
nezastupitelné	zastupitelný	k2eNgNnSc4d1	nezastupitelné
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
nezbytné	zbytný	k2eNgFnPc1d1	zbytný
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Vývojem	vývoj	k1gInSc7	vývoj
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
adaptaci	adaptace	k1gFnSc4	adaptace
mnoha	mnoho	k4c2	mnoho
živočichů	živočich	k1gMnPc2	živočich
na	na	k7c4	na
původní	původní	k2eAgInPc4d1	původní
druhy	druh	k1gInPc4	druh
keřů	keř	k1gInPc2	keř
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
používat	používat	k5eAaImF	používat
i	i	k9	i
původní	původní	k2eAgInPc4d1	původní
druhy	druh	k1gInPc4	druh
keřů	keř	k1gInPc2	keř
pro	pro	k7c4	pro
výsadby	výsadba	k1gFnPc4	výsadba
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
a	a	k8xC	a
zahradách	zahrada	k1gFnPc6	zahrada
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
biodiverzity	biodiverzita	k1gFnSc2	biodiverzita
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
skupina	skupina	k1gFnSc1	skupina
keřů	keř	k1gInPc2	keř
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
křoví	křoví	k1gNnSc1	křoví
<g/>
.	.	kIx.	.
</s>
<s>
Keře	keř	k1gInPc1	keř
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
používány	používat	k5eAaImNgInP	používat
v	v	k7c6	v
parcích	park	k1gInPc6	park
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bývají	bývat	k5eAaImIp3nP	bývat
používány	používat	k5eAaImNgFnP	používat
jako	jako	k9	jako
solitéry	solitéra	k1gFnPc1	solitéra
<g/>
,	,	kIx,	,
k	k	k7c3	k
oddělení	oddělení	k1gNnSc3	oddělení
prostor	prostor	k1gInSc4	prostor
nebo	nebo	k8xC	nebo
sestříhávány	sestříháván	k2eAgFnPc4d1	sestříháván
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
tvarů	tvar	k1gInPc2	tvar
v	v	k7c6	v
živých	živý	k2eAgInPc6d1	živý
plotech	plot	k1gInPc6	plot
<g/>
.	.	kIx.	.
</s>
<s>
Ovocné	ovocný	k2eAgInPc1d1	ovocný
keře	keř	k1gInPc1	keř
jsou	být	k5eAaImIp3nP	být
pěstovány	pěstovat	k5eAaImNgInP	pěstovat
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
dřevitých	dřevitý	k2eAgInPc2d1	dřevitý
řízků	řízek	k1gInPc2	řízek
pro	pro	k7c4	pro
nadzemní	nadzemní	k2eAgFnSc4d1	nadzemní
část	část	k1gFnSc4	část
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
horní	horní	k2eAgInPc1d1	horní
tři	tři	k4xCgInPc1	tři
pupeny	pupen	k1gInPc1	pupen
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgInPc1d1	spodní
pupeny	pupen	k1gInPc1	pupen
se	se	k3xPyFc4	se
vyslepují	vyslepovat	k5eAaPmIp3nP	vyslepovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
bývají	bývat	k5eAaImIp3nP	bývat
přírůstky	přírůstek	k1gInPc1	přírůstek
krátké	krátký	k2eAgInPc1d1	krátký
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
keře	keř	k1gInSc2	keř
stačí	stačit	k5eAaBmIp3nP	stačit
tři	tři	k4xCgFnPc1	tři
základní	základní	k2eAgFnPc1d1	základní
větve	větev	k1gFnPc1	větev
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
založení	založení	k1gNnSc4	založení
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
výhonu	výhon	k1gInSc2	výhon
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
stávající	stávající	k2eAgInPc4d1	stávající
výhony	výhon	k1gInPc4	výhon
zkracovány	zkracován	k2eAgInPc4d1	zkracován
z	z	k7c2	z
jara	jaro	k1gNnSc2	jaro
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
roce	rok	k1gInSc6	rok
po	po	k7c6	po
výsadbě	výsadba	k1gFnSc6	výsadba
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
letorostů	letorost	k1gInPc2	letorost
je	být	k5eAaImIp3nS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
jako	jako	k9	jako
korunka	korunka	k1gFnSc1	korunka
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
začátkem	začátkem	k7c2	začátkem
června	červen	k1gInSc2	červen
zkrátíme	zkrátit	k5eAaPmIp1nP	zkrátit
nejvyšší	vysoký	k2eAgInPc1d3	Nejvyšší
letorosty	letorost	k1gInPc1	letorost
<g/>
.	.	kIx.	.
</s>
<s>
Sazenice	sazenice	k1gFnSc1	sazenice
keřů	keř	k1gInPc2	keř
se	se	k3xPyFc4	se
sklízejí	sklízet	k5eAaImIp3nP	sklízet
na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhého	druhý	k4xOgInSc2	druhý
roku	rok	k1gInSc2	rok
po	po	k7c6	po
výsadbě	výsadba	k1gFnSc6	výsadba
řízku	řízek	k1gInSc2	řízek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobrých	dobrý	k2eAgFnPc6d1	dobrá
vegetačních	vegetační	k2eAgFnPc6d1	vegetační
podmínkách	podmínka	k1gFnPc6	podmínka
sklízíme	sklízet	k5eAaImIp1nP	sklízet
sazenice	sazenice	k1gFnSc2	sazenice
již	již	k6eAd1	již
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
pěstování	pěstování	k1gNnSc2	pěstování
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
sazenice	sazenice	k1gFnPc1	sazenice
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
slabší	slabý	k2eAgMnPc1d2	slabší
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
označené	označený	k2eAgInPc1d1	označený
keře	keř	k1gInPc1	keř
*	*	kIx~	*
se	se	k3xPyFc4	se
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Latinský	latinský	k2eAgInSc1d1	latinský
název	název	k1gInSc1	název
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
-	-	kIx~	-
dva	dva	k4xCgInPc1	dva
názvy	název	k1gInPc1	název
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lidový	lidový	k2eAgInSc4d1	lidový
název	název	k1gInSc4	název
Hořící	hořící	k2eAgInSc1d1	hořící
keř	keř	k1gInSc1	keř
Polokeř	polokeř	k1gInSc1	polokeř
původní	původní	k2eAgInSc1d1	původní
keře	keř	k1gInSc2	keř
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
keř	keř	k1gInSc1	keř
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
keř	keř	k1gInSc1	keř
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
