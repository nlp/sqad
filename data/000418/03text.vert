<s>
Héfaistos	Héfaistos	k1gMnSc1	Héfaistos
je	být	k5eAaImIp3nS	být
syn	syn	k1gMnSc1	syn
Dia	Dia	k1gFnSc2	Dia
a	a	k8xC	a
Héry	Héra	k1gFnSc2	Héra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
byl	být	k5eAaImAgMnS	být
bohem	bůh	k1gMnSc7	bůh
ohně	oheň	k1gInSc2	oheň
a	a	k8xC	a
kovářství	kovářství	k1gNnSc2	kovářství
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
římský	římský	k2eAgInSc1d1	římský
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
je	být	k5eAaImIp3nS	být
Vulkán	vulkán	k1gInSc4	vulkán
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Vulcanus	Vulcanus	k1gInSc1	Vulcanus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Héfaistos	Héfaistos	k1gInSc1	Héfaistos
byl	být	k5eAaImAgInS	být
ošklivý	ošklivý	k2eAgMnSc1d1	ošklivý
a	a	k8xC	a
slabý	slabý	k2eAgMnSc1d1	slabý
hned	hned	k6eAd1	hned
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
narození	narození	k1gNnSc6	narození
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
matka	matka	k1gFnSc1	matka
ho	on	k3xPp3gNnSc2	on
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
nedokonalost	nedokonalost	k1gFnSc4	nedokonalost
svrhla	svrhnout	k5eAaPmAgFnS	svrhnout
z	z	k7c2	z
Olympu	Olymp	k1gInSc2	Olymp
<g/>
.	.	kIx.	.
</s>
<s>
Zachránily	zachránit	k5eAaPmAgFnP	zachránit
ho	on	k3xPp3gNnSc4	on
bohyně	bohyně	k1gFnPc1	bohyně
Thetis	Thetis	k1gFnPc2	Thetis
a	a	k8xC	a
Eurynomé	Eurynomý	k2eAgFnSc2d1	Eurynomý
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ho	on	k3xPp3gMnSc4	on
i	i	k9	i
vychovaly	vychovat	k5eAaPmAgFnP	vychovat
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k9	jednou
pak	pak	k6eAd1	pak
Héra	Héra	k1gFnSc1	Héra
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
překrásný	překrásný	k2eAgInSc1d1	překrásný
šperk	šperk	k1gInSc1	šperk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
měla	mít	k5eAaImAgFnS	mít
Thetis	Thetis	k1gFnSc1	Thetis
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Héfaistos	Héfaistos	k1gMnSc1	Héfaistos
<g/>
.	.	kIx.	.
</s>
<s>
Přivedla	přivést	k5eAaPmAgFnS	přivést
proto	proto	k8xC	proto
Héfaista	Héfaista	k1gMnSc1	Héfaista
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Olymp	Olymp	k1gInSc4	Olymp
a	a	k8xC	a
zařídila	zařídit	k5eAaPmAgFnS	zařídit
mu	on	k3xPp3gMnSc3	on
bohatě	bohatě	k6eAd1	bohatě
vybavenou	vybavený	k2eAgFnSc4d1	vybavená
kovárnu	kovárna	k1gFnSc4	kovárna
s	s	k7c7	s
dvaceti	dvacet	k4xCc7	dvacet
měchy	měch	k1gInPc7	měch
<g/>
.	.	kIx.	.
</s>
<s>
Héfaistos	Héfaistos	k1gMnSc1	Héfaistos
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
smířil	smířit	k5eAaPmAgMnS	smířit
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
statečně	statečně	k6eAd1	statečně
zastával	zastávat	k5eAaImAgMnS	zastávat
proti	proti	k7c3	proti
Diovi	Diův	k2eAgMnPc1d1	Diův
<g/>
,	,	kIx,	,
srazil	srazit	k5eAaPmAgMnS	srazit
ho	on	k3xPp3gInSc4	on
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
podruhé	podruhé	k6eAd1	podruhé
z	z	k7c2	z
Olympu	Olymp	k1gInSc2	Olymp
<g/>
.	.	kIx.	.
</s>
<s>
Héfaistos	Héfaistos	k1gInSc1	Héfaistos
padal	padat	k5eAaImAgInS	padat
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
a	a	k8xC	a
když	když	k8xS	když
konečně	konečně	k6eAd1	konečně
dopadl	dopadnout	k5eAaPmAgInS	dopadnout
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Lémnos	Lémnos	k1gInSc4	Lémnos
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k8xC	jako
Limnos	Limnos	k1gMnSc1	Limnos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zlomil	zlomit	k5eAaPmAgMnS	zlomit
si	se	k3xPyFc3	se
obě	dva	k4xCgFnPc4	dva
nohy	noha	k1gFnPc4	noha
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
potřeboval	potřebovat	k5eAaImAgInS	potřebovat
pomoc	pomoc	k1gFnSc4	pomoc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ho	on	k3xPp3gMnSc4	on
vlídně	vlídně	k6eAd1	vlídně
přijali	přijmout	k5eAaPmAgMnP	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
srovnáván	srovnáván	k2eAgInSc1d1	srovnáván
s	s	k7c7	s
bohyní	bohyně	k1gFnSc7	bohyně
Pallas	Pallas	k1gMnSc1	Pallas
Athénou	Athéna	k1gFnSc7	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
zručným	zručný	k2eAgMnSc7d1	zručný
kovářem	kovář	k1gMnSc7	kovář
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zhotovuje	zhotovovat	k5eAaImIp3nS	zhotovovat
mistrná	mistrný	k2eAgNnPc4d1	mistrné
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
nevyrovná	vyrovnat	k5eNaPmIp3nS	vyrovnat
se	se	k3xPyFc4	se
však	však	k9	však
Pallas	Pallas	k1gMnSc1	Pallas
Athéně	Athéna	k1gFnSc3	Athéna
duchem	duch	k1gMnSc7	duch
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
základem	základ	k1gInSc7	základ
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Héfaistos	Héfaistos	k1gInSc1	Héfaistos
má	mít	k5eAaImIp3nS	mít
dílny	dílna	k1gFnSc2	dílna
pod	pod	k7c7	pod
Etnou	Etna	k1gFnSc7	Etna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vládne	vládnout	k5eAaImIp3nS	vládnout
základnímu	základní	k2eAgInSc3d1	základní
prvku	prvek	k1gInSc3	prvek
kovářského	kovářský	k2eAgNnSc2d1	kovářské
řemesla	řemeslo	k1gNnSc2	řemeslo
<g/>
,	,	kIx,	,
ohni	oheň	k1gInSc6	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
pomocníky	pomocník	k1gMnPc7	pomocník
jsou	být	k5eAaImIp3nP	být
Kratos	Kratos	k1gMnSc1	Kratos
(	(	kIx(	(
<g/>
síla	síla	k1gFnSc1	síla
<g/>
)	)	kIx)	)
a	a	k8xC	a
Biá	Biá	k1gFnSc1	Biá
(	(	kIx(	(
<g/>
Násilí	násilí	k1gNnSc1	násilí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pallas	Pallas	k1gMnSc1	Pallas
Athéna	Athéna	k1gFnSc1	Athéna
předčila	předčít	k5eAaPmAgFnS	předčít
Héfaista	Héfaista	k1gMnSc1	Héfaista
nejen	nejen	k6eAd1	nejen
duchem	duch	k1gMnSc7	duch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
svou	svůj	k3xOyFgFnSc7	svůj
ladností	ladnost	k1gFnSc7	ladnost
-	-	kIx~	-
Héfaistos	Héfaistos	k1gMnSc1	Héfaistos
byl	být	k5eAaImAgMnS	být
belhající	belhající	k2eAgMnPc4d1	belhající
se	se	k3xPyFc4	se
mrzák	mrzák	k1gMnSc1	mrzák
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
se	se	k3xPyFc4	se
bohové	bůh	k1gMnPc1	bůh
často	často	k6eAd1	často
vysmívali	vysmívat	k5eAaImAgMnP	vysmívat
<g/>
.	.	kIx.	.
</s>
<s>
Přece	přece	k9	přece
však	však	k9	však
Héfaistos	Héfaistos	k1gInSc1	Héfaistos
<g/>
,	,	kIx,	,
zdánlivě	zdánlivě	k6eAd1	zdánlivě
nemotorný	nemotorný	k2eAgMnSc1d1	nemotorný
a	a	k8xC	a
slabý	slabý	k2eAgMnSc1d1	slabý
<g/>
,	,	kIx,	,
vystavěl	vystavět	k5eAaPmAgInS	vystavět
bohům	bůh	k1gMnPc3	bůh
jejich	jejich	k3xOp3gInPc1	jejich
nádherné	nádherný	k2eAgInPc1d1	nádherný
paláce	palác	k1gInPc1	palác
na	na	k7c6	na
Olympu	Olymp	k1gInSc6	Olymp
<g/>
,	,	kIx,	,
zhotovil	zhotovit	k5eAaPmAgInS	zhotovit
zbroj	zbroj	k1gFnSc4	zbroj
největšího	veliký	k2eAgMnSc2d3	veliký
hrdiny	hrdina	k1gMnSc2	hrdina
Achillea	Achilleus	k1gMnSc2	Achilleus
<g/>
,	,	kIx,	,
brnění	brnění	k1gNnSc3	brnění
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
řeckých	řecký	k2eAgMnPc2d1	řecký
reků	rek	k1gMnPc2	rek
Dioméda	Diomédo	k1gNnSc2	Diomédo
a	a	k8xC	a
na	na	k7c4	na
výzvu	výzva	k1gFnSc4	výzva
Diovu	Diův	k2eAgFnSc4d1	Diova
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
země	zem	k1gFnSc2	zem
bytost	bytost	k1gFnSc4	bytost
Pandoru	Pandora	k1gFnSc4	Pandora
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Odysseje	Odyssea	k1gFnSc2	Odyssea
byla	být	k5eAaImAgFnS	být
manželkou	manželka	k1gFnSc7	manželka
Héfaistovou	Héfaistův	k2eAgFnSc7d1	Héfaistova
bohyně	bohyně	k1gFnPc4	bohyně
Afrodíté	Afrodítý	k2eAgFnSc2d1	Afrodítý
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gMnSc4	on
podváděla	podvádět	k5eAaImAgFnS	podvádět
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
Áreem	Áres	k1gMnSc7	Áres
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
Héfaistos	Héfaistos	k1gInSc1	Héfaistos
milence	milenec	k1gMnSc4	milenec
překvapil	překvapit	k5eAaPmAgInS	překvapit
a	a	k8xC	a
zavolal	zavolat	k5eAaPmAgInS	zavolat
bohy	bůh	k1gMnPc4	bůh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
viděli	vidět	k5eAaImAgMnP	vidět
tuto	tento	k3xDgFnSc4	tento
nevěru	nevěra	k1gFnSc4	nevěra
<g/>
,	,	kIx,	,
sklidil	sklidit	k5eAaPmAgMnS	sklidit
opět	opět	k6eAd1	opět
jen	jen	k9	jen
výsměch	výsměch	k1gInSc4	výsměch
a	a	k8xC	a
Afrodíté	Afrodítý	k2eAgNnSc4d1	Afrodíté
zůstala	zůstat	k5eAaPmAgFnS	zůstat
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Héfaistos	Héfaistos	k1gInSc1	Héfaistos
nejvíce	nejvíce	k6eAd1	nejvíce
miloval	milovat	k5eAaImAgInS	milovat
ostrov	ostrov	k1gInSc1	ostrov
Lémnos	Lémnos	k1gInSc4	Lémnos
(	(	kIx(	(
<g/>
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
zbytky	zbytek	k1gInPc1	zbytek
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
pojmenovaného	pojmenovaný	k2eAgInSc2d1	pojmenovaný
právě	právě	k6eAd1	právě
po	po	k7c6	po
Héfaistovi	Héfaista	k1gMnSc6	Héfaista
-	-	kIx~	-
Ifaistia	Ifaistium	k1gNnSc2	Ifaistium
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
též	též	k9	též
soustředěn	soustředěn	k2eAgInSc1d1	soustředěn
jeho	jeho	k3xOp3gInSc1	jeho
kult	kult	k1gInSc1	kult
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Héfaistově	Héfaistův	k2eAgFnSc3d1	Héfaistova
poctě	pocta	k1gFnSc3	pocta
konali	konat	k5eAaImAgMnP	konat
Řekové	Řek	k1gMnPc1	Řek
okázalé	okázalý	k2eAgFnSc2d1	okázalá
slavnosti	slavnost	k1gFnSc2	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stojí	stát	k5eAaImIp3nS	stát
dodnes	dodnes	k6eAd1	dodnes
jeho	jeho	k3xOp3gInSc1	jeho
chrám	chrám	k1gInSc1	chrám
(	(	kIx(	(
Héfaisteion	Héfaisteion	k1gInSc1	Héfaisteion
/	/	kIx~	/
Hephaisteon	Hephaisteon	k1gInSc1	Hephaisteon
-	-	kIx~	-
chrám	chrám	k1gInSc1	chrám
v	v	k7c6	v
dórském	dórský	k2eAgInSc6d1	dórský
slohu	sloh	k1gInSc6	sloh
se	se	k3xPyFc4	se
nápadně	nápadně	k6eAd1	nápadně
tyčí	tyčit	k5eAaImIp3nP	tyčit
nad	nad	k7c7	nad
korunami	koruna	k1gFnPc7	koruna
stromů	strom	k1gInPc2	strom
v	v	k7c6	v
rohu	roh	k1gInSc6	roh
Agorá	Agorý	k2eAgFnSc1d1	Agorá
a	a	k8xC	a
připomíná	připomínat	k5eAaImIp3nS	připomínat
zmenšeninu	zmenšenina	k1gFnSc4	zmenšenina
Parthenonu	Parthenon	k1gInSc2	Parthenon
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
zároveň	zároveň	k6eAd1	zároveň
konkuruje	konkurovat	k5eAaImIp3nS	konkurovat
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejlépe	dobře	k6eAd3	dobře
udržovaných	udržovaný	k2eAgInPc2d1	udržovaný
řeckých	řecký	k2eAgInPc2d1	řecký
chrámů	chrám	k1gInPc2	chrám
)	)	kIx)	)
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
dvojího	dvojí	k4xRgMnSc2	dvojí
druhu	druh	k1gInSc2	druh
<g/>
:	:	kIx,	:
héfaisteie	héfaisteie	k1gFnPc1	héfaisteie
a	a	k8xC	a
chalkeie	chalkeie	k1gFnPc1	chalkeie
<g/>
;	;	kIx,	;
první	první	k4xOgFnSc1	první
se	se	k3xPyFc4	se
slavila	slavit	k5eAaImAgFnS	slavit
zpočátku	zpočátku	k6eAd1	zpočátku
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
od	od	k7c2	od
roku	rok	k1gInSc2	rok
329	[number]	k4	329
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
každý	každý	k3xTgInSc4	každý
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
rok	rok	k1gInSc4	rok
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
listopadu	listopad	k1gInSc2	listopad
<g/>
;	;	kIx,	;
druhá	druhý	k4xOgFnSc1	druhý
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
každoročně	každoročně	k6eAd1	každoročně
a	a	k8xC	a
zúčastňovali	zúčastňovat	k5eAaImAgMnP	zúčastňovat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
zejména	zejména	k9	zejména
kováři	kovář	k1gMnPc1	kovář
<g/>
.	.	kIx.	.
</s>
<s>
Héfaistos	Héfaistos	k1gInSc1	Héfaistos
byl	být	k5eAaImAgInS	být
zobrazován	zobrazovat	k5eAaImNgInS	zobrazovat
v	v	k7c6	v
pracovní	pracovní	k2eAgFnSc6d1	pracovní
haleně	halena	k1gFnSc6	halena
(	(	kIx(	(
<g/>
exomis	exomis	k1gInSc1	exomis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pravou	pravý	k2eAgFnSc4d1	pravá
ruku	ruka	k1gFnSc4	ruka
nechávala	nechávat	k5eAaImAgFnS	nechávat
zcela	zcela	k6eAd1	zcela
volnou	volný	k2eAgFnSc7d1	volná
<g/>
,	,	kIx,	,
s	s	k7c7	s
kovářskou	kovářský	k2eAgFnSc7d1	Kovářská
zašpičatělou	zašpičatělý	k2eAgFnSc7d1	zašpičatělá
čapkou	čapka	k1gFnSc7	čapka
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
s	s	k7c7	s
odznaky	odznak	k1gInPc7	odznak
své	svůj	k3xOyFgFnSc2	svůj
profese	profes	k1gFnSc2	profes
-	-	kIx~	-
kleštěmi	kleště	k1gFnPc7	kleště
a	a	k8xC	a
kladívkem	kladívko	k1gNnSc7	kladívko
-	-	kIx~	-
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Prométheus	Prométheus	k1gMnSc1	Prométheus
kradl	krást	k5eAaImAgMnS	krást
bohům	bůh	k1gMnPc3	bůh
oheň	oheň	k1gInSc4	oheň
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
z	z	k7c2	z
Héfaistovy	Héfaistův	k2eAgFnSc2d1	Héfaistova
dílny	dílna	k1gFnSc2	dílna
v	v	k7c6	v
hoře	hora	k1gFnSc6	hora
Moschylos	Moschylosa	k1gFnPc2	Moschylosa
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Lémnu	Lémn	k1gInSc2	Lémn
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
symboly	symbol	k1gInPc1	symbol
jsou	být	k5eAaImIp3nP	být
kovářské	kovářský	k2eAgNnSc4d1	kovářské
kladivo	kladivo	k1gNnSc4	kladivo
<g/>
,	,	kIx,	,
kovadlina	kovadlina	k1gFnSc1	kovadlina
<g/>
,	,	kIx,	,
a	a	k8xC	a
pár	pár	k4xCyI	pár
kleští	kleště	k1gFnPc2	kleště
<g/>
.	.	kIx.	.
</s>
<s>
Brnění	brnění	k1gNnSc1	brnění
Achillea	Achilleus	k1gMnSc2	Achilleus
Dioméda	Dioméda	k1gFnSc1	Dioméda
Bytost	bytost	k1gFnSc1	bytost
Pandóra	Pandóra	k1gFnSc1	Pandóra
Hádova	Hádův	k2eAgFnSc1d1	Hádova
přilba	přilba	k1gFnSc1	přilba
neviditelnosti	neviditelnost	k1gFnSc2	neviditelnost
Héliův	Héliův	k2eAgInSc4d1	Héliův
vůz	vůz	k1gInSc4	vůz
Okřídlená	okřídlený	k2eAgFnSc1d1	okřídlená
helma	helma	k1gFnSc1	helma
a	a	k8xC	a
sandály	sandál	k1gInPc1	sandál
boha	bůh	k1gMnSc2	bůh
Herma	Hermes	k1gMnSc2	Hermes
Ženské	ženský	k2eAgInPc1d1	ženský
stroje	stroj	k1gInPc1	stroj
-	-	kIx~	-
Alfa	alfa	k1gFnSc1	alfa
<g/>
,	,	kIx,	,
Beta	beta	k1gNnSc2	beta
<g/>
,	,	kIx,	,
Gamma	Gammum	k1gNnSc2	Gammum
<g/>
,	,	kIx,	,
Delta	delta	k1gNnSc2	delta
<g/>
,	,	kIx,	,
Epsilon	epsilon	k1gNnSc2	epsilon
<g/>
,	,	kIx,	,
Zéta	Zétum	k1gNnSc2	Zétum
<g/>
,	,	kIx,	,
Éta	éta	k1gNnSc2	éta
<g/>
,	,	kIx,	,
Théta	théta	k1gNnSc2	théta
<g/>
,	,	kIx,	,
Ióta	Iótum	k1gNnSc2	Iótum
<g/>
,	,	kIx,	,
Kappa	kappa	k1gNnSc2	kappa
<g/>
,	,	kIx,	,
Lambda	lambda	k1gNnSc2	lambda
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Mí	mí	k1gNnSc1	mí
<g/>
,	,	kIx,	,
Ný	Ný	k1gFnSc1	Ný
<g/>
,	,	kIx,	,
Ksí	Ksí	k1gFnSc1	Ksí
<g/>
,	,	kIx,	,
Omikron	omikron	k1gInSc1	omikron
<g/>
,	,	kIx,	,
Pí	pí	k1gNnSc1	pí
<g/>
,	,	kIx,	,
Ró	ró	k1gNnSc1	ró
<g/>
,	,	kIx,	,
Sigma	sigma	k1gNnSc1	sigma
<g/>
,	,	kIx,	,
Tau	tau	k1gNnSc1	tau
<g/>
,	,	kIx,	,
Ypsilon	ypsilon	k1gNnSc1	ypsilon
<g/>
,	,	kIx,	,
Fí	fí	k0	fí
<g/>
,	,	kIx,	,
Chí	chí	k1gNnSc1	chí
<g/>
,	,	kIx,	,
Psí	psí	k1gNnSc1	psí
a	a	k8xC	a
Omega	omega	k1gNnSc1	omega
(	(	kIx(	(
<g/>
Jména	jméno	k1gNnPc4	jméno
podle	podle	k7c2	podle
řecké	řecký	k2eAgFnSc2d1	řecká
abecedy	abeceda	k1gFnSc2	abeceda
alfabeta	alfabeta	k1gFnSc1	alfabeta
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Héfaistos	Héfaistosa	k1gFnPc2	Héfaistosa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovník	slovník	k1gInSc4	slovník
antické	antický	k2eAgFnSc2d1	antická
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
hrdinové	hrdina	k1gMnPc1	hrdina
antických	antický	k2eAgFnPc2d1	antická
bájí	báj	k1gFnPc2	báj
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
<g/>
,	,	kIx,	,
nakl	naknout	k5eAaPmAgMnS	naknout
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
Starořecké	starořecký	k2eAgFnSc2d1	starořecká
báje	báj	k1gFnSc2	báj
<g/>
,	,	kIx,	,
N.	N.	kA	N.
A.	A.	kA	A.
Kun	kuna	k1gFnPc2	kuna
<g/>
,	,	kIx,	,
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Řecko	Řecko	k1gNnSc1	Řecko
-	-	kIx~	-
velký	velký	k2eAgMnSc1d1	velký
průvodce	průvodce	k1gMnSc1	průvodce
od	od	k7c2	od
National	National	k1gFnSc2	National
Geographic	Geographice	k1gInPc2	Geographice
</s>
