<p>
<s>
Mikrocefalie	mikrocefalie	k1gFnSc1	mikrocefalie
je	být	k5eAaImIp3nS	být
těžká	těžký	k2eAgFnSc1d1	těžká
vývojová	vývojový	k2eAgFnSc1d1	vývojová
porucha	porucha	k1gFnSc1	porucha
projevující	projevující	k2eAgFnSc1d1	projevující
se	s	k7c7	s
zakrněním	zakrnění	k1gNnSc7	zakrnění
<g/>
/	/	kIx~	/
<g/>
předčasným	předčasný	k2eAgNnSc7d1	předčasné
ukončením	ukončení	k1gNnSc7	ukončení
růstu	růst	k1gInSc2	růst
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
i	i	k9	i
celé	celý	k2eAgFnSc2d1	celá
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
příznakem	příznak	k1gInSc7	příznak
řady	řada	k1gFnSc2	řada
závažných	závažný	k2eAgNnPc2d1	závažné
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
genetických	genetický	k2eAgInPc2d1	genetický
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
Cri	Cri	k1gFnPc2	Cri
du	du	k?	du
chat	chata	k1gFnPc2	chata
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
příčinou	příčina	k1gFnSc7	příčina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
infekce	infekce	k1gFnSc2	infekce
matky	matka	k1gFnSc2	matka
virem	vir	k1gInSc7	vir
Zika	Zika	k1gMnSc1	Zika
během	během	k7c2	během
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
příznakem	příznak	k1gInSc7	příznak
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
obvod	obvod	k1gInSc1	obvod
lebky	lebka	k1gFnSc2	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Postižené	postižený	k2eAgFnPc1d1	postižená
děti	dítě	k1gFnPc1	dítě
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
také	také	k9	také
menší	malý	k2eAgInSc1d2	menší
mozek	mozek	k1gInSc1	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Mikrocefalie	mikrocefalie	k1gFnSc1	mikrocefalie
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
duševní	duševní	k2eAgFnSc7d1	duševní
a	a	k8xC	a
tělesnou	tělesný	k2eAgFnSc7d1	tělesná
poruchou	porucha	k1gFnSc7	porucha
<g/>
.	.	kIx.	.
<g/>
U	u	k7c2	u
stopkovýtrusných	stopkovýtrusný	k2eAgFnPc2d1	stopkovýtrusná
hub	houba	k1gFnPc2	houba
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
zakrnělých	zakrnělý	k2eAgFnPc2d1	zakrnělá
plodnic	plodnice	k1gFnPc2	plodnice
či	či	k8xC	či
klobouků	klobouk	k1gInPc2	klobouk
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
při	při	k7c6	při
rozličných	rozličný	k2eAgFnPc6d1	rozličná
virových	virový	k2eAgFnPc6d1	virová
infekcích	infekce	k1gFnPc6	infekce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mikrocefalie	mikrocefalie	k1gFnSc2	mikrocefalie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Hydrocephalus	Hydrocephalus	k1gMnSc1	Hydrocephalus
</s>
</p>
