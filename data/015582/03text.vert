<s>
Ochotsk	Ochotsk	k1gInSc1
</s>
<s>
Ochotsk	Ochotsk	k1gInSc1
О	О	k?
Ivan	Ivan	k1gMnSc1
Děmjanovič	Děmjanovič	k1gMnSc1
Bulyčev	Bulyčev	k1gMnSc1
<g/>
:	:	kIx,
Ochotsk	Ochotsk	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1856	#num#	k4
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
59	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
143	#num#	k4
<g/>
°	°	k?
<g/>
18	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
m	m	kA
n.	n.	k?
m.	m.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
UTC	UTC	kA
<g/>
+	+	kIx~
<g/>
10	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Stát	stát	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
Rusko	Rusko	k1gNnSc4
federální	federální	k2eAgInSc1d1
okruh	okruh	k1gInSc1
</s>
<s>
Dálněvýchodní	dálněvýchodní	k2eAgInSc1d1
federální	federální	k2eAgInSc1d1
okruh	okruh	k1gInSc1
kraj	kraj	k1gInSc1
</s>
<s>
Chabarovský	chabarovský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Chabarovský	chabarovský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
Ruska	Rusko	k1gNnSc2
</s>
<s>
Ochotsk	Ochotsk	k1gInSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
3	#num#	k4
576	#num#	k4
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Správa	správa	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
sídlo	sídlo	k1gNnSc1
městského	městský	k2eAgInSc2d1
typu	typ	k1gInSc2
Vznik	vznik	k1gInSc1
</s>
<s>
1647	#num#	k4
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
+	+	kIx~
<g/>
7	#num#	k4
<g/>
)	)	kIx)
42141	#num#	k4
PSČ	PSČ	kA
</s>
<s>
682480	#num#	k4
Označení	označení	k1gNnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
27	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ochotsk	Ochotsk	k1gInSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
О	О	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ruské	ruský	k2eAgNnSc1d1
přístavní	přístavní	k2eAgNnSc1d1
město	město	k1gNnSc1
ležící	ležící	k2eAgNnSc1d1
v	v	k7c6
Chabarovském	chabarovský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
administrativní	administrativní	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
Ochotského	ochotský	k2eAgInSc2d1
okresu	okres	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
leží	ležet	k5eAaImIp3nP
u	u	k7c2
ústí	ústí	k1gNnSc2
řeky	řeka	k1gFnSc2
Ochoty	ochota	k1gFnSc2
při	při	k7c6
pobřeží	pobřeží	k1gNnSc6
Ochotského	ochotský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
3	#num#	k4
183	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Etymologie	etymologie	k1gFnSc1
</s>
<s>
Název	název	k1gInSc1
pevnosti	pevnost	k1gFnSc2
a	a	k8xC
poté	poté	k6eAd1
města	město	k1gNnPc1
pochází	pocházet	k5eAaImIp3nP
od	od	k7c2
Evenků	Evenek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Buď	buď	k8xC
od	od	k7c2
slova	slovo	k1gNnSc2
achot	achot	k1gInSc1
-	-	kIx~
velký	velký	k2eAgInSc1d1
nebo	nebo	k8xC
od	od	k7c2
ochat	ochat	k5eAaImF
-	-	kIx~
řeka	řeka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
kozáci	kozák	k1gMnPc1
si	se	k3xPyFc3
jedno	jeden	k4xCgNnSc1
z	z	k7c2
těchto	tento	k3xDgNnPc2
dvou	dva	k4xCgNnPc2
slov	slovo	k1gNnPc2
poruštili	poruštit	k5eAaPmAgMnP
a	a	k8xC
vzniklo	vzniknout	k5eAaPmAgNnS
z	z	k7c2
toho	ten	k3xDgNnSc2
Ochotsk	Ochotsk	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Založení	založení	k1gNnSc1
osady	osada	k1gFnSc2
</s>
<s>
Ochotsk	Ochotsk	k1gInSc4
je	být	k5eAaImIp3nS
první	první	k4xOgNnSc1
město	město	k1gNnSc1
a	a	k8xC
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejstarších	starý	k2eAgFnPc2d3
ruských	ruský	k2eAgFnPc2d1
osad	osada	k1gFnPc2
na	na	k7c6
Dálném	dálný	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1639	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
kozácký	kozácký	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
vedený	vedený	k2eAgInSc1d1
Ivanem	Ivan	k1gMnSc7
Moskvitinem	Moskvitin	k1gMnSc7
východního	východní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
Asijské	asijský	k2eAgFnSc2d1
pevniny	pevnina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stali	stát	k5eAaPmAgMnP
se	se	k3xPyFc4
tak	tak	k9
prvními	první	k4xOgMnPc7
Rusy	Rus	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
dosáhli	dosáhnout	k5eAaPmAgMnP
Ochotského	ochotský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
a	a	k8xC
Tichécho	Tichécha	k1gFnSc5
oceánu	oceán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založili	založit	k5eAaPmAgMnP
zde	zde	k6eAd1
první	první	k4xOgMnSc1
malý	malý	k1gMnSc1
ostrožek	ostrožka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
si	se	k3xPyFc3
na	na	k7c6
místě	místo	k1gNnSc6
pozdějšího	pozdní	k2eAgNnSc2d2
města	město	k1gNnSc2
vybudoval	vybudovat	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
zimní	zimní	k2eAgInSc4d1
tábor	tábor	k1gInSc4
ruský	ruský	k2eAgMnSc1d1
objevitel	objevitel	k1gMnSc1
<g/>
,	,	kIx,
kozák	kozák	k1gMnSc1
Semjon	Semjon	k1gMnSc1
Andrejevič	Andrejevič	k1gMnSc1
Šelkovnikov	Šelkovnikov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
zde	zde	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1647	#num#	k4
vybudoval	vybudovat	k5eAaPmAgMnS
chaty	chata	k1gFnPc4
pro	pro	k7c4
přezimování	přezimování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1649	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vybudování	vybudování	k1gNnSc3
opevnění	opevnění	k1gNnSc2
těchto	tento	k3xDgFnPc2
chat	chata	k1gFnPc2
a	a	k8xC
vzniku	vznik	k1gInSc2
malé	malý	k2eAgFnSc2d1
opevněné	opevněný	k2eAgFnSc2d1
osady	osada	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
pojmenovali	pojmenovat	k5eAaPmAgMnP
Kosoj	Kosoj	k1gInSc1
Ostrožok	Ostrožok	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
přítomnost	přítomnost	k1gFnSc1
se	se	k3xPyFc4
nelíbila	líbit	k5eNaImAgFnS
místním	místní	k2eAgMnPc3d1
Lamutům	Lamut	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
nechtěli	chtít	k5eNaImAgMnP
platit	platit	k5eAaImF
jasak	jasak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lamutové	Lamutový	k2eAgFnSc2d1
v	v	k7c6
roce	rok	k1gInSc6
1653	#num#	k4
na	na	k7c4
Kosoj	Kosoj	k1gInSc4
Ostrožok	Ostrožok	k1gInSc4
zaútočili	zaútočit	k5eAaPmAgMnP
<g/>
,	,	kIx,
po	po	k7c6
několika	několik	k4yIc6
dnech	den	k1gInPc6
ho	on	k3xPp3gMnSc4
dobyli	dobýt	k5eAaPmAgMnP
a	a	k8xC
vypálili	vypálit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rusové	k2eAgFnSc7d1
brzy	brzy	k6eAd1
osadu	osada	k1gFnSc4
znovu	znovu	k6eAd1
vybudovali	vybudovat	k5eAaPmAgMnP
a	a	k8xC
lépe	dobře	k6eAd2
opevnili	opevnit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
osada	osada	k1gFnSc1
ležela	ležet	k5eAaImAgFnS
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
nesloužila	sloužit	k5eNaImAgFnS
jako	jako	k9
přístav	přístav	k1gInSc1
<g/>
,	,	kIx,
první	první	k4xOgMnPc1
osadníci	osadník	k1gMnPc1
a	a	k8xC
průzkumníci	průzkumník	k1gMnPc1
v	v	k7c6
osadě	osada	k1gFnSc6
neuměli	umět	k5eNaImAgMnP
stavět	stavět	k5eAaImF
lodě	loď	k1gFnPc4
vhodné	vhodný	k2eAgFnPc4d1
pro	pro	k7c4
plavbu	plavba	k1gFnSc4
po	po	k7c6
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavěli	stavět	k5eAaImAgMnP
pouze	pouze	k6eAd1
říční	říční	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
veškeré	veškerý	k3xTgNnSc1
spojení	spojení	k1gNnSc1
s	s	k7c7
okolím	okolí	k1gNnSc7
probíhalo	probíhat	k5eAaImAgNnS
pouze	pouze	k6eAd1
přes	přes	k7c4
těžko	těžko	k6eAd1
schůdné	schůdný	k2eAgFnPc4d1
pozemní	pozemní	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osada	osada	k1gFnSc1
tak	tak	k9
zůstávala	zůstávat	k5eAaImAgFnS
pouze	pouze	k6eAd1
odříznutým	odříznutý	k2eAgNnSc7d1
sídlem	sídlo	k1gNnSc7
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
</s>
<s>
Přístavní	přístavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1713	#num#	k4
vyslal	vyslat	k5eAaPmAgMnS
car	car	k1gMnSc1
Petr	Petr	k1gMnSc1
I.	I.	kA
Veliký	veliký	k2eAgMnSc1d1
do	do	k7c2
Ochotska	Ochotsko	k1gNnSc2
několik	několik	k4yIc1
zkušených	zkušený	k2eAgMnPc2d1
stavitelů	stavitel	k1gMnPc2
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
zde	zde	k6eAd1
postavili	postavit	k5eAaPmAgMnP
Vostok	Vostok	k1gInSc4
<g/>
,	,	kIx,
první	první	k4xOgNnSc1
ruské	ruský	k2eAgNnSc1d1
plavidlo	plavidlo	k1gNnSc1
postavené	postavený	k2eAgNnSc1d1
na	na	k7c6
březích	břeh	k1gInPc6
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
lodi	loď	k1gFnSc6
Vostok	Vostok	k1gInSc1
kozák	kozák	k1gMnSc1
Kuzma	Kuzma	k1gFnSc1
Sokolov	Sokolovo	k1gNnPc2
jako	jako	k8xS,k8xC
první	první	k4xOgFnSc7
přeplul	přeplout	k5eAaPmAgMnS
z	z	k7c2
Ochotska	Ochotsk	k1gInSc2
na	na	k7c4
Kamčatku	Kamčatka	k1gFnSc4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
objevil	objevit	k5eAaPmAgMnS
nové	nový	k2eAgNnSc4d1
dopravní	dopravní	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
po	po	k7c6
moři	moře	k1gNnSc6
s	s	k7c7
územím	území	k1gNnSc7
Kamčatky	Kamčatka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
vlády	vláda	k1gFnSc2
carevny	carevna	k1gFnSc2
Anny	Anna	k1gFnSc2
Ivanovny	Ivanovna	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
založení	založení	k1gNnSc3
Ochotské	ochotský	k2eAgFnSc2d1
flotily	flotila	k1gFnSc2
a	a	k8xC
začalo	začít	k5eAaPmAgNnS
se	se	k3xPyFc4
se	s	k7c7
stavbou	stavba	k1gFnSc7
přístavu	přístav	k1gInSc2
<g/>
,	,	kIx,
kolem	kolem	k7c2
kterého	který	k3yIgNnSc2,k3yQgNnSc2,k3yRgNnSc2
později	pozdě	k6eAd2
vyrostlo	vyrůst	k5eAaPmAgNnS
město	město	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Díky	díky	k7c3
Sokolovovi	Sokolova	k1gMnSc3
se	se	k3xPyFc4
z	z	k7c2
Ochotsku	Ochotsek	k1gInSc2
stal	stát	k5eAaPmAgInS
přístav	přístav	k1gInSc1
<g/>
,	,	kIx,
výchozí	výchozí	k2eAgInSc1d1
bod	bod	k1gInSc1
námořních	námořní	k2eAgFnPc2d1
expedic	expedice	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
prozkoumaly	prozkoumat	k5eAaPmAgFnP
severní	severní	k2eAgInSc4d1
Tichý	tichý	k2eAgInSc4d1
oceán	oceán	k1gInSc4
a	a	k8xC
objevily	objevit	k5eAaPmAgFnP
západní	západní	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
ruské	ruský	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1729	#num#	k4
v	v	k7c6
Ochotsku	Ochotsek	k1gInSc6
stála	stát	k5eAaImAgFnS
kaple	kaple	k1gFnSc1
<g/>
,	,	kIx,
chata	chata	k1gFnSc1
pro	pro	k7c4
skladování	skladování	k1gNnSc4
jasaku	jasak	k1gInSc2
<g/>
,	,	kIx,
stodola	stodola	k1gFnSc1
a	a	k8xC
12	#num#	k4
obytných	obytný	k2eAgInPc2d1
domů	dům	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1731	#num#	k4
získal	získat	k5eAaPmAgInS
Ochotsk	Ochotsk	k1gInSc1
status	status	k1gInSc4
ruského	ruský	k2eAgNnSc2d1
přístavního	přístavní	k2eAgNnSc2d1
města	město	k1gNnSc2
a	a	k8xC
prvním	první	k4xOgMnSc7
velitelem	velitel	k1gMnSc7
přístavu	přístav	k1gInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Grigorij	Grigorij	k1gMnSc1
Grigorjevič	Grigorjevič	k1gMnSc1
Skornjakov-Pisarev	Skornjakov-Pisarev	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1732	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
Ochotsku	Ochotsek	k1gInSc6
zřízena	zřízen	k2eAgFnSc1d1
z	z	k7c2
iniciativy	iniciativa	k1gFnSc2
Vituse	Vituse	k1gFnSc2
Beringa	Bering	k1gMnSc2
Ochotská	ochotský	k2eAgFnSc1d1
námořní	námořní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1737	#num#	k4
byly	být	k5eAaImAgFnP
Ochotsku	Ochotska	k1gFnSc4
vyrobeny	vyroben	k2eAgFnPc1d1
a	a	k8xC
spuštěny	spustit	k5eAaPmNgFnP
na	na	k7c4
moře	moře	k1gNnSc4
lodě	loď	k1gFnSc2
Svatý	svatý	k1gMnSc1
Petr	Petr	k1gMnSc1
a	a	k8xC
Svatý	svatý	k2eAgMnSc1d1
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c6
nichž	jenž	k3xRgInPc6
se	se	k3xPyFc4
Vitus	Vitus	k1gMnSc1
Bering	Bering	k1gMnSc1
a	a	k8xC
Alexej	Alexej	k1gMnSc1
Čirikov	Čirikov	k1gInSc4
plavili	plavit	k5eAaImAgMnP
do	do	k7c2
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
počest	počest	k1gFnSc4
těchto	tento	k3xDgFnPc2
dvou	dva	k4xCgFnPc2
lodí	loď	k1gFnPc2
bylo	být	k5eAaImAgNnS
později	pozdě	k6eAd2
pojmenováno	pojmenován	k2eAgNnSc1d1
Petropavlovsk-Kamčatskij	Petropavlovsk-Kamčatskij	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1740	#num#	k4
se	se	k3xPyFc4
velitelem	velitel	k1gMnSc7
Ochotsku	Ochotsek	k1gInSc2
stal	stát	k5eAaPmAgMnS
hrabě	hrabě	k1gMnSc1
Anton	Anton	k1gMnSc1
Manujlovič	Manujlovič	k1gMnSc1
Devier	Devier	k1gMnSc1
<g/>
,	,	kIx,
blízký	blízký	k2eAgMnSc1d1
spolupracovník	spolupracovník	k1gMnSc1
a	a	k8xC
přítel	přítel	k1gMnSc1
cara	car	k1gMnSc2
Petra	Petr	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
finančně	finančně	k6eAd1
podporoval	podporovat	k5eAaImAgInS
expedici	expedice	k1gFnSc4
do	do	k7c2
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
vyplatil	vyplatit	k5eAaPmAgMnS
všechny	všechen	k3xTgFnPc4
dlužné	dlužný	k2eAgFnPc4d1
mzdy	mzda	k1gFnPc4
kozákům	kozák	k1gMnPc3
ve	v	k7c6
městě	město	k1gNnSc6
a	a	k8xC
naplánoval	naplánovat	k5eAaBmAgMnS
rozvoj	rozvoj	k1gInSc4
přístavu	přístav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1742	#num#	k4
se	se	k3xPyFc4
novým	nový	k2eAgMnSc7d1
velitelem	velitel	k1gMnSc7
města	město	k1gNnSc2
stal	stát	k5eAaPmAgMnS
Atanasius	Atanasius	k1gMnSc1
Nikitič	Nikitič	k1gMnSc1
Zybin	Zybin	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
spravoval	spravovat	k5eAaImAgMnS
město	město	k1gNnSc4
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1760	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nechal	nechat	k5eAaPmAgMnS
postavit	postavit	k5eAaPmF
první	první	k4xOgInSc4
maják	maják	k1gInSc4
v	v	k7c6
ústí	ústí	k1gNnSc6
řeky	řeka	k1gFnSc2
Kuchtuj	Kuchtuj	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
jeho	jeho	k3xOp3gNnPc2
dohlížení	dohlížení	k1gNnPc2
nad	nad	k7c7
městem	město	k1gNnSc7
bylo	být	k5eAaImAgNnS
na	na	k7c4
moře	moře	k1gNnSc4
spuštěno	spustit	k5eAaPmNgNnS
sedm	sedm	k4xCc1
nových	nový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
vyrobeny	vyrobit	k5eAaPmNgInP
v	v	k7c6
Ochotsku	Ochotsek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1742	#num#	k4
stál	stát	k5eAaImAgMnS
ve	v	k7c6
městě	město	k1gNnSc6
Kostel	kostel	k1gInSc4
Proměnění	proměnění	k1gNnSc2
Páně	páně	k2eAgFnSc1d1
<g/>
,	,	kIx,
Zybinova	Zybinův	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
<g/>
,	,	kIx,
panovnické	panovnický	k2eAgNnSc1d1
nádvoří	nádvoří	k1gNnSc1
<g/>
,	,	kIx,
kasárna	kasárna	k1gNnPc4
<g/>
,	,	kIx,
3	#num#	k4
dílny	dílna	k1gFnSc2
<g/>
,	,	kIx,
5	#num#	k4
stodol	stodola	k1gFnPc2
<g/>
,	,	kIx,
40	#num#	k4
obytných	obytný	k2eAgInPc2d1
domů	dům	k1gInPc2
<g/>
,	,	kIx,
5	#num#	k4
obchodů	obchod	k1gInPc2
a	a	k8xC
1	#num#	k4
kovárna	kovárna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1750	#num#	k4
bylo	být	k5eAaImAgNnS
do	do	k7c2
Ochotsku	Ochotsek	k1gInSc2
posláno	poslán	k2eAgNnSc1d1
přibližně	přibližně	k6eAd1
6	#num#	k4
tisíc	tisíc	k4xCgInPc2
koní	kůň	k1gMnPc2
s	s	k7c7
jídlem	jídlo	k1gNnSc7
a	a	k8xC
vybavením	vybavení	k1gNnSc7
z	z	k7c2
Jakutska	Jakutsk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
Ochotsku	Ochotsek	k1gInSc2
je	být	k5eAaImIp3nS
spojen	spojit	k5eAaPmNgInS
s	s	k7c7
obchodníkem	obchodník	k1gMnSc7
Grigorijem	Grigorij	k1gMnSc7
Šelechovem	Šelechov	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
do	do	k7c2
města	město	k1gNnSc2
dorazil	dorazit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1776	#num#	k4
a	a	k8xC
udělal	udělat	k5eAaPmAgInS
z	z	k7c2
něj	on	k3xPp3gMnSc2
svou	svůj	k3xOyFgFnSc4
obchodní	obchodní	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
pro	pro	k7c4
obchod	obchod	k1gInSc4
s	s	k7c7
Ruskou	ruský	k2eAgFnSc7d1
Amerikou	Amerika	k1gFnSc7
a	a	k8xC
vysílání	vysílání	k1gNnSc1
expedic	expedice	k1gFnPc2
na	na	k7c4
Kurilské	kurilský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kožešiny	kožešina	k1gFnSc2
z	z	k7c2
Aljašky	Aljaška	k1gFnSc2
a	a	k8xC
Aleutských	aleutský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
byly	být	k5eAaImAgFnP
dováženy	dovážet	k5eAaImNgFnP,k5eAaPmNgFnP
do	do	k7c2
Ochotsku	Ochotsek	k1gInSc2
a	a	k8xC
odsud	odsud	k6eAd1
je	být	k5eAaImIp3nS
Šelechov	Šelechov	k1gInSc1
nechával	nechávat	k5eAaImAgInS
posílat	posílat	k5eAaImF
do	do	k7c2
vnitrozemí	vnitrozemí	k1gNnSc2
na	na	k7c4
prodej	prodej	k1gInSc4
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
byla	být	k5eAaImAgFnS
prodávána	prodávat	k5eAaImNgFnS
čínským	čínský	k2eAgInSc7d1
obchodníkům	obchodník	k1gMnPc3
v	v	k7c6
Kjachtě	Kjachta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1783	#num#	k4
se	se	k3xPyFc4
Ochotsk	Ochotsk	k1gInSc1
stal	stát	k5eAaPmAgInS
centrem	centrum	k1gNnSc7
ochotského	ochotský	k2eAgInSc2d1
okresu	okres	k1gInSc2
Irkutského	irkutský	k2eAgNnSc2d1
místokrálovství	místokrálovství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1790	#num#	k4
byl	být	k5eAaImAgInS
přijat	přijmout	k5eAaPmNgInS
současný	současný	k2eAgInSc1d1
městský	městský	k2eAgInSc1d1
znak	znak	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
štítu	štít	k1gInSc2
je	být	k5eAaImIp3nS
irkutský	irkutský	k2eAgInSc1d1
znak	znak	k1gInSc1
a	a	k8xC
v	v	k7c6
dolní	dolní	k2eAgFnSc6d1
části	část	k1gFnSc6
v	v	k7c6
modrém	modrý	k2eAgNnSc6d1
poli	pole	k1gNnSc6
jsou	být	k5eAaImIp3nP
dvě	dva	k4xCgFnPc4
kotvy	kotva	k1gFnPc4
a	a	k8xC
nad	nad	k7c7
nimi	on	k3xPp3gMnPc7
vlajka	vlajka	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
znamení	znamení	k1gNnSc6
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
městě	město	k1gNnSc6
je	být	k5eAaImIp3nS
přístav	přístav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1786	#num#	k4
bylo	být	k5eAaImAgNnS
ve	v	k7c6
městě	město	k1gNnSc6
již	již	k6eAd1
150	#num#	k4
domů	dům	k1gInPc2
a	a	k8xC
2	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Centrum	centrum	k1gNnSc1
regionu	region	k1gInSc2
</s>
<s>
Roku	rok	k1gInSc2
1796	#num#	k4
<g/>
,	,	kIx,
za	za	k7c2
vlády	vláda	k1gFnSc2
cara	car	k1gMnSc2
Pavla	Pavel	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
na	na	k7c6
ruském	ruský	k2eAgInSc6d1
Dálném	dálný	k2eAgInSc6d1
východě	východ	k1gInSc6
změněny	změnit	k5eAaPmNgFnP
správní	správní	k2eAgFnPc1d1
hranice	hranice	k1gFnSc1
a	a	k8xC
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
Ochotská	ochotský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
zahrnovala	zahrnovat	k5eAaImAgFnS
Kamčatku	Kamčatka	k1gFnSc4
<g/>
,	,	kIx,
Čukotku	Čukotka	k1gFnSc4
a	a	k8xC
Ochotské	ochotský	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ochotsk	Ochotsk	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
regionálním	regionální	k2eAgMnSc7d1
centrem	centr	k1gMnSc7
nové	nový	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgMnPc7d1
dvěma	dva	k4xCgInPc7
městy	město	k1gNnPc7
byly	být	k5eAaImAgFnP
Nižněkamčatsk	Nižněkamčatsk	k1gInSc4
a	a	k8xC
Gižiginsk	Gižiginsk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1815	#num#	k4
byl	být	k5eAaImAgInS
Ochotsk	Ochotsk	k1gInSc1
přesunut	přesunout	k5eAaPmNgInS
na	na	k7c4
opačnou	opačný	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
společného	společný	k2eAgNnSc2d1
ústí	ústí	k1gNnSc2
řek	řeka	k1gFnPc2
Ochota	ochota	k1gFnSc1
a	a	k8xC
Kuchtuj	Kuchtuj	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
Ochotsk	Ochotsk	k1gInSc1
více	hodně	k6eAd2
rozrůstal	rozrůstat	k5eAaImAgInS
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
stále	stále	k6eAd1
zřejmější	zřejmý	k2eAgNnSc1d2
<g/>
,	,	kIx,
že	že	k8xS
není	být	k5eNaImIp3nS
vhodným	vhodný	k2eAgNnSc7d1
místem	místo	k1gNnSc7
pro	pro	k7c4
regionální	regionální	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
obtížné	obtížný	k2eAgFnSc2d1
vnitrozemské	vnitrozemský	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
měl	mít	k5eAaImAgInS
přístav	přístav	k1gInSc1
chudé	chudý	k2eAgFnSc2d1
a	a	k8xC
velmi	velmi	k6eAd1
krátké	krátký	k2eAgNnSc4d1
vegetační	vegetační	k2eAgNnSc4d1
období	období	k1gNnSc4
<g/>
,	,	kIx,
nedostatek	nedostatek	k1gInSc1
orby	orba	k1gFnSc2
znamenal	znamenat	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
muselo	muset	k5eAaImAgNnS
téměř	téměř	k6eAd1
všechno	všechen	k3xTgNnSc1
obilí	obilí	k1gNnSc1
draze	draha	k1gFnSc3
dovážet	dovážet	k5eAaImF
z	z	k7c2
Jakutska	Jakutsk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
bylo	být	k5eAaImAgNnS
velmi	velmi	k6eAd1
málo	málo	k4c1
pastvin	pastvina	k1gFnPc2
<g/>
,	,	kIx,
koně	kůň	k1gMnPc1
se	se	k3xPyFc4
někdy	někdy	k6eAd1
musely	muset	k5eAaImAgFnP
posílat	posílat	k5eAaImF
z	z	k7c2
Ochotsku	Ochotsek	k1gInSc2
do	do	k7c2
Jakutska	Jakutsk	k1gInSc2
i	i	k8xC
bez	bez	k7c2
nákladu	náklad	k1gInSc2
<g/>
,	,	kIx,
protože	protože	k8xS
hrozilo	hrozit	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
pomřou	pomřít	k5eAaPmIp3nP
hlady	hlady	k6eAd1
než	než	k8xS
dorazí	dorazit	k5eAaPmIp3nP
z	z	k7c2
Ruské	ruský	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
vhodné	vhodný	k2eAgInPc4d1
náklady	náklad	k1gInPc4
na	na	k7c4
transport	transport	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
pak	pak	k6eAd1
kožešiny	kožešina	k1gFnPc1
z	z	k7c2
Ameriky	Amerika	k1gFnSc2
dorazily	dorazit	k5eAaPmAgInP
do	do	k7c2
Ochotsku	Ochotsek	k1gInSc2
<g/>
,	,	kIx,
často	často	k6eAd1
tam	tam	k6eAd1
nebyly	být	k5eNaImAgFnP
koně	kůň	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
by	by	kYmCp3nP
je	on	k3xPp3gMnPc4
mohly	moct	k5eAaImAgFnP
odvézt	odvézt	k5eAaPmF
do	do	k7c2
Kjachty	Kjacht	k1gInPc1
nebo	nebo	k8xC
Jakutska	Jakutsk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
května	květen	k1gInSc2
do	do	k7c2
listopadu	listopad	k1gInSc2
bylo	být	k5eAaImAgNnS
moře	moře	k1gNnSc1
bez	bez	k7c2
ledu	led	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
sezona	sezona	k1gFnSc1
plachtění	plachtění	k1gNnSc2
trvala	trvat	k5eAaImAgFnS
pouze	pouze	k6eAd1
od	od	k7c2
června	červen	k1gInSc2
do	do	k7c2
září	září	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústí	ústí	k1gNnSc1
řek	řeka	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgMnSc6
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
postavené	postavený	k2eAgNnSc1d1
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
blátivé	blátivý	k2eAgNnSc1d1
a	a	k8xC
mělké	mělký	k2eAgNnSc1d1
<g/>
,	,	kIx,
při	při	k7c6
jarní	jarní	k2eAgFnSc6d1
oblevě	obleva	k1gFnSc6
tak	tak	k9
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
často	často	k6eAd1
celé	celý	k2eAgNnSc4d1
zalité	zalitý	k2eAgNnSc4d1
vodou	voda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čerstvá	čerstvý	k2eAgFnSc1d1
voda	voda	k1gFnSc1
se	se	k3xPyFc4
často	často	k6eAd1
musela	muset	k5eAaImAgFnS
dovážet	dovážet	k5eAaImF
až	až	k9
ze	z	k7c2
vzdálenosti	vzdálenost	k1gFnSc2
4	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
skončení	skončení	k1gNnSc6
zimy	zima	k1gFnSc2
se	s	k7c7
břehy	břeh	k1gInPc7
řeky	řeka	k1gFnSc2
Kuchtuj	Kuchtuj	k1gFnSc2
proměnily	proměnit	k5eAaPmAgFnP
v	v	k7c4
bažiny	bažina	k1gFnPc4
a	a	k8xC
nedalo	dát	k5eNaPmAgNnS
se	se	k3xPyFc4
po	po	k7c6
nich	on	k3xPp3gInPc6
cestovat	cestovat	k5eAaImF
do	do	k7c2
vnitrozemí	vnitrozemí	k1gNnSc2
suchou	suchý	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Úpadek	úpadek	k1gInSc1
města	město	k1gNnSc2
</s>
<s>
V	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
s	s	k7c7
prudkým	prudký	k2eAgInSc7d1
rozvojem	rozvoj	k1gInSc7
ruského	ruský	k2eAgInSc2d1
Dálného	dálný	k2eAgInSc2d1
východu	východ	k1gInSc2
klesal	klesat	k5eAaImAgInS
význam	význam	k1gInSc4
Ochotsku	Ochotsek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1822	#num#	k4
byl	být	k5eAaImAgInS
ještě	ještě	k6eAd1
Ochotsk	Ochotsk	k1gInSc1
určen	určit	k5eAaPmNgInS
jako	jako	k8xS,k8xC
centrum	centrum	k1gNnSc1
Přímoří	Přímoří	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
jejím	její	k3xOp3gNnSc6
zrušení	zrušení	k1gNnSc6
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1849	#num#	k4
Ochotsk	Ochotsk	k1gInSc1
stal	stát	k5eAaPmAgInS
jedním	jeden	k4xCgNnSc7
z	z	k7c2
několika	několik	k4yIc2
okresních	okresní	k2eAgNnPc2d1
měst	město	k1gNnPc2
Jakutské	jakutský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1840	#num#	k4
se	se	k3xPyFc4
Rusko-americká	rusko-americký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1799	#num#	k4
založil	založit	k5eAaPmAgMnS
Šelechov	Šelechov	k1gInSc4
<g/>
,	,	kIx,
rozhodla	rozhodnout	k5eAaPmAgFnS
opustit	opustit	k5eAaPmF
Ochotsk	Ochotsk	k1gInSc4
a	a	k8xC
za	za	k7c4
své	svůj	k3xOyFgNnSc4
nové	nový	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
si	se	k3xPyFc3
vybrala	vybrat	k5eAaPmAgFnS
jižnější	jižní	k2eAgInSc4d2
Ajan	Ajan	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
nová	nový	k2eAgFnSc1d1
pozemní	pozemní	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
Jakutsk-Ajan	Jakutsk-Ajana	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
Ochotsk	Ochotsk	k1gInSc4
zcela	zcela	k6eAd1
vynechala	vynechat	k5eAaPmAgFnS
z	z	k7c2
kožešinového	kožešinový	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
s	s	k7c7
Amerikou	Amerika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1849	#num#	k4
sibiřiský	sibiřiský	k1gMnSc1
guvernér	guvernér	k1gMnSc1
Nikolaj	Nikolaj	k1gMnSc1
Muravjov-Amurskij	Muravjov-Amurskij	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
přesunout	přesunout	k5eAaPmF
sibiřskou	sibiřský	k2eAgFnSc4d1
flotilu	flotila	k1gFnSc4
a	a	k8xC
místní	místní	k2eAgFnSc4d1
navigátorskou	navigátorský	k2eAgFnSc4d1
školu	škola	k1gFnSc4
do	do	k7c2
Petropavlovsku-Kamčatského	Petropavlovsku-Kamčatský	k2eAgInSc2d1
a	a	k8xC
všechny	všechen	k3xTgMnPc4
vládní	vládní	k2eAgMnPc4d1
úředníky	úředník	k1gMnPc4
do	do	k7c2
Ajanu	Ajan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Připojení	připojení	k1gNnSc1
oblasti	oblast	k1gFnSc2
kolem	kolem	k7c2
řeky	řeka	k1gFnSc2
Amur	Amur	k1gInSc1
k	k	k7c3
Rusku	Rusko	k1gNnSc3
v	v	k7c6
roce	rok	k1gInSc6
1860	#num#	k4
přinesla	přinést	k5eAaPmAgFnS
přesun	přesun	k1gInSc4
ruského	ruský	k2eAgInSc2d1
zájmu	zájem	k1gInSc2
více	hodně	k6eAd2
na	na	k7c4
jih	jih	k1gInSc4
<g/>
,	,	kIx,
o	o	k7c4
Ochotsk	Ochotsk	k1gInSc4
přestal	přestat	k5eAaPmAgInS
být	být	k5eAaImF
zájem	zájem	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1867	#num#	k4
Rusko	Rusko	k1gNnSc1
prodalo	prodat	k5eAaPmAgNnS
Aljašku	Aljaška	k1gFnSc4
USA	USA	kA
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
dalšímu	další	k2eAgNnSc3d1
vylidnění	vylidnění	k1gNnSc3
Ochotsku	Ochotsek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1865	#num#	k4
žilo	žít	k5eAaImAgNnS
v	v	k7c6
Ochotsku	Ochotsko	k1gNnSc6
přibližně	přibližně	k6eAd1
sto	sto	k4xCgNnSc1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1849	#num#	k4
až	až	k9
1866	#num#	k4
křižovaly	křižovat	k5eAaImAgFnP
americké	americký	k2eAgFnPc1d1
velrybářské	velrybářský	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
nedaleko	nedaleko	k7c2
Ochotsku	Ochotsek	k1gInSc2
při	při	k7c6
lovu	lov	k1gInSc6
velryb	velryba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
velryb	velryba	k1gFnPc2
bylo	být	k5eAaImAgNnS
Američany	Američan	k1gMnPc7
uloveno	uloven	k2eAgNnSc1d1
na	na	k7c4
dohled	dohled	k1gInSc4
od	od	k7c2
Ochotsku	Ochotsek	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
málokterá	málokterý	k3yIgFnSc1
americká	americký	k2eAgFnSc1d1
velrybářská	velrybářský	k2eAgFnSc1d1
loď	loď	k1gFnSc1
zakotvila	zakotvit	k5eAaPmAgFnS
přímo	přímo	k6eAd1
v	v	k7c6
Ochotsku	Ochotsek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
Krymské	krymský	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
objevila	objevit	k5eAaPmAgFnS
spojená	spojený	k2eAgFnSc1d1
britsko-francouzská	britsko-francouzský	k2eAgFnSc1d1
eskadra	eskadra	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
napadala	napadat	k5eAaBmAgNnP,k5eAaImAgNnP,k5eAaPmAgNnP
ruská	ruský	k2eAgNnPc1d1
města	město	k1gNnPc1
<g/>
,	,	kIx,
nejvíce	nejvíce	k6eAd1,k6eAd3
zasaženy	zasažen	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
Ajan	Ajan	k1gNnSc4
a	a	k8xC
Petropavlovsk-Kamčatskij	Petropavlovsk-Kamčatskij	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britsko-francouzské	britsko-francouzský	k2eAgFnSc6d1
eskadře	eskadra	k1gFnSc6
připadal	připadat	k5eAaPmAgInS,k5eAaImAgInS
už	už	k6eAd1
Ochotsk	Ochotsk	k1gInSc1
natolik	natolik	k6eAd1
nedůležitý	důležitý	k2eNgInSc1d1
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
něj	on	k3xPp3gMnSc4
nebyl	být	k5eNaImAgInS
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
války	válka	k1gFnSc2
podniknut	podniknut	k2eAgInSc4d1
jediný	jediný	k2eAgInSc4d1
útok	útok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
se	se	k3xPyFc4
do	do	k7c2
Ochotsku	Ochotsek	k1gInSc2
dostala	dostat	k5eAaPmAgFnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
města	město	k1gNnSc2
zmocnili	zmocnit	k5eAaPmAgMnP
bílí	bílý	k2eAgMnPc1d1
s	s	k7c7
podporou	podpora	k1gFnSc7
japonských	japonský	k2eAgInPc2d1
dělových	dělový	k2eAgInPc2d1
člunů	člun	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místní	místní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
se	se	k3xPyFc4
obávali	obávat	k5eAaImAgMnP
odvety	odveta	k1gFnPc4
od	od	k7c2
ozbrojených	ozbrojený	k2eAgMnPc2d1
těžařů	těžař	k1gMnPc2
zlata	zlato	k1gNnSc2
a	a	k8xC
proto	proto	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
většina	většina	k1gFnSc1
lovců	lovec	k1gMnPc2
opustila	opustit	k5eAaPmAgFnS
město	město	k1gNnSc4
a	a	k8xC
vydala	vydat	k5eAaPmAgFnS
se	se	k3xPyFc4
na	na	k7c4
cestu	cesta	k1gFnSc4
do	do	k7c2
Jakutska	Jakutsk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
z	z	k7c2
nich	on	k3xPp3gInPc2
zemřelo	zemřít	k5eAaPmAgNnS
cestou	cesta	k1gFnSc7
na	na	k7c6
nemoci	nemoc	k1gFnSc6
<g/>
,	,	kIx,
vyčerpání	vyčerpání	k1gNnSc6
a	a	k8xC
také	také	k6eAd1
bylo	být	k5eAaImAgNnS
zabito	zabít	k5eAaPmNgNnS
bandity	bandita	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bělogvardějci	bělogvardějec	k1gMnPc1
se	se	k3xPyFc4
vydali	vydat	k5eAaPmAgMnP
z	z	k7c2
Ochotska	Ochotsk	k1gInSc2
na	na	k7c4
pochod	pochod	k1gInSc4
do	do	k7c2
Jakutska	Jakutsk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
1922	#num#	k4
přistál	přistát	k5eAaPmAgInS,k5eAaImAgInS
ve	v	k7c6
městě	město	k1gNnSc6
bělogvardějský	bělogvardějský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Anatolij	Anatolij	k1gMnSc1
Nikolajevič	Nikolajevič	k1gMnSc1
Pepeljajev	Pepeljajev	k1gMnSc1
míříci	mířík	k1gMnPc1
k	k	k7c3
Ajanu	Ajan	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ochotsk	Ochotsk	k1gInSc1
byl	být	k5eAaImAgInS
jedním	jeden	k4xCgNnSc7
z	z	k7c2
posledních	poslední	k2eAgNnPc2d1
ruských	ruský	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
dostaly	dostat	k5eAaPmAgInP
v	v	k7c6
roce	rok	k1gInSc6
1923	#num#	k4
pod	pod	k7c4
sovětskou	sovětský	k2eAgFnSc4d1
moc	moc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Znak	znak	k1gInSc1
města	město	k1gNnSc2
během	během	k7c2
sovětské	sovětský	k2eAgFnSc2d1
éry	éra	k1gFnSc2
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sovětské	sovětský	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Roku	rok	k1gInSc2
1927	#num#	k4
byl	být	k5eAaImAgInS
upadající	upadající	k2eAgInSc1d1
Ochotsk	Ochotsk	k1gInSc1
zbaven	zbaven	k2eAgInSc1d1
statusu	status	k1gInSc2
města	město	k1gNnSc2
a	a	k8xC
přeměněn	přeměnit	k5eAaPmNgMnS
na	na	k7c4
venkovskou	venkovský	k2eAgFnSc4d1
osadu	osada	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
získal	získat	k5eAaPmAgInS
status	status	k1gInSc4
osady	osada	k1gFnSc2
městského	městský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sovětských	sovětský	k2eAgFnPc6d1
dobách	doba	k1gFnPc6
byl	být	k5eAaImAgInS
přístav	přístav	k1gInSc1
dále	daleko	k6eAd2
rozvíjen	rozvíjet	k5eAaImNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
městě	město	k1gNnSc6
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
velký	velký	k2eAgInSc1d1
závod	závod	k1gInSc1
na	na	k7c4
zpracování	zpracování	k1gNnSc4
ryb	ryba	k1gFnPc2
a	a	k8xC
závod	závod	k1gInSc1
na	na	k7c4
opravu	oprava	k1gFnSc4
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
závody	závod	k1gInPc1
fungovaly	fungovat	k5eAaImAgInP
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Nacházela	nacházet	k5eAaImAgFnS
se	se	k3xPyFc4
zde	zde	k6eAd1
6	#num#	k4
<g/>
.	.	kIx.
divize	divize	k1gFnSc2
11	#num#	k4
<g/>
.	.	kIx.
samostatné	samostatný	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
protivzdušné	protivzdušný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
hraniční	hraniční	k2eAgNnSc1d1
stanoviště	stanoviště	k1gNnSc1
Ochotsk	Ochotsk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Postsovětské	postsovětský	k2eAgNnSc4d1
období	období	k1gNnSc4
a	a	k8xC
současnost	současnost	k1gFnSc4
</s>
<s>
Po	po	k7c6
rozpadu	rozpad	k1gInSc6
SSSR	SSSR	kA
se	se	k3xPyFc4
zhroutila	zhroutit	k5eAaPmAgFnS
ekonomika	ekonomika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
závodů	závod	k1gInPc2
a	a	k8xC
podniků	podnik	k1gInPc2
ve	v	k7c6
městě	město	k1gNnSc6
byla	být	k5eAaImAgFnS
uzavřena	uzavřít	k5eAaPmNgFnS
<g/>
,	,	kIx,
včetně	včetně	k7c2
loděnice	loděnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ochotský	ochotský	k2eAgInSc4d1
přístav	přístav	k1gInSc4
ztratil	ztratit	k5eAaPmAgMnS
zcela	zcela	k6eAd1
svůj	svůj	k3xOyFgInSc4
význam	význam	k1gInSc4
a	a	k8xC
je	být	k5eAaImIp3nS
nazýván	nazývat	k5eAaImNgInS
přístavní	přístavní	k2eAgInSc1d1
bod	bod	k1gInSc1
(	(	kIx(
<g/>
mini	mini	k2eAgInSc1d1
přístav	přístav	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhoršující	zhoršující	k2eAgFnSc1d1
se	se	k3xPyFc4
ekonomická	ekonomický	k2eAgFnSc1d1
situace	situace	k1gFnSc1
vedla	vést	k5eAaImAgFnS
k	k	k7c3
vylidnění	vylidnění	k1gNnSc3
Ochotsku	Ochotsek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
žilo	žít	k5eAaImAgNnS
v	v	k7c6
Ochotsku	Ochotsek	k1gInSc6
9	#num#	k4
289	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
jen	jen	k9
3	#num#	k4
183	#num#	k4
a	a	k8xC
lidé	člověk	k1gMnPc1
neustále	neustále	k6eAd1
odchází	odcházet	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
byl	být	k5eAaImAgInS
Ochotsk	Ochotsk	k1gInSc1
součástí	součást	k1gFnPc2
pohraničního	pohraniční	k2eAgNnSc2d1
pásma	pásmo	k1gNnSc2
<g/>
,	,	kIx,
toto	tento	k3xDgNnSc1
pásmo	pásmo	k1gNnSc1
bylo	být	k5eAaImAgNnS
ale	ale	k9
již	již	k6eAd1
zrušeno	zrušit	k5eAaPmNgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
</s>
<s>
Ochotsk	Ochotsk	k1gInSc1
má	mít	k5eAaImIp3nS
rybářský	rybářský	k2eAgInSc1d1
přístav	přístav	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
představuje	představovat	k5eAaImIp3nS
ekonomické	ekonomický	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
okolí	okolí	k1gNnSc6
Ochotsku	Ochotsek	k1gInSc2
se	se	k3xPyFc4
zpracovávají	zpracovávat	k5eAaImIp3nP
ulovené	ulovený	k2eAgFnPc1d1
ryby	ryba	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Ochotsku	Ochotsko	k1gNnSc6
sídlí	sídlet	k5eAaImIp3nP
těžařské	těžařský	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
v	v	k7c6
okolí	okolí	k1gNnSc6
města	město	k1gNnSc2
hledají	hledat	k5eAaImIp3nP
nová	nový	k2eAgNnPc4d1
ložiska	ložisko	k1gNnPc4
zlata	zlato	k1gNnSc2
a	a	k8xC
stříbra	stříbro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Ochotsku	Ochotsek	k1gInSc6
je	být	k5eAaImIp3nS
elektrárna	elektrárna	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
pět	pět	k4xCc4
dieselových	dieselový	k2eAgInPc2d1
generátorů	generátor	k1gInPc2
(	(	kIx(
<g/>
DG-	DG-	k1gFnPc2
<g/>
72	#num#	k4
<g/>
;	;	kIx,
0,8	0,8	k4
MW	MW	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc3d3
roli	role	k1gFnSc3
v	v	k7c6
ochotské	ochotský	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
představuje	představovat	k5eAaImIp3nS
Ochotské	ochotský	k2eAgNnSc1d1
letiště	letiště	k1gNnSc1
pro	pro	k7c4
menší	malý	k2eAgNnPc4d2
letadla	letadlo	k1gNnPc4
a	a	k8xC
vrtulníky	vrtulník	k1gInPc4
všech	všecek	k3xTgInPc2
typů	typ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letiště	letiště	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
odbavilo	odbavit	k5eAaPmAgNnS
30	#num#	k4
500	#num#	k4
cestujících	cestující	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Podnebí	podnebí	k1gNnSc1
</s>
<s>
Ochotské	ochotský	k2eAgNnSc1d1
klima	klima	k1gNnSc1
je	být	k5eAaImIp3nS
subarktické	subarktický	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
zmírněné	zmírněný	k2eAgNnSc1d1
Tichým	tichý	k2eAgInSc7d1
oceánem	oceán	k1gInSc7
<g/>
,	,	kIx,
proto	proto	k8xC
jsou	být	k5eAaImIp3nP
zimy	zima	k1gFnPc1
teplejší	teplý	k2eAgFnSc1d2
než	než	k8xS
v	v	k7c6
podobných	podobný	k2eAgFnPc6d1
zeměpisných	zeměpisný	k2eAgFnPc6d1
šířkách	šířka	k1gFnPc6
na	na	k7c6
Sibiři	Sibiř	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
naopak	naopak	k6eAd1
léta	léto	k1gNnPc1
jsou	být	k5eAaImIp3nP
mnohem	mnohem	k6eAd1
chladnější	chladný	k2eAgFnPc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejteplejším	teplý	k2eAgInSc7d3
měsícem	měsíc	k1gInSc7
je	být	k5eAaImIp3nS
srpen	srpen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Ochotsk	Ochotsk	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
zóně	zóna	k1gFnSc6
permafrostu	permafrost	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1
roční	roční	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
je	být	k5eAaImIp3nS
-3,8	-3,8	k4
°	°	k?
<g/>
C	C	kA
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1
roční	roční	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
větru	vítr	k1gInSc2
je	být	k5eAaImIp3nS
3,5	3,5	k4
m	m	kA
/	/	kIx~
s	s	k7c7
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1
roční	roční	k2eAgFnSc1d1
vlhkost	vlhkost	k1gFnSc1
vzduchu	vzduch	k1gInSc2
je	být	k5eAaImIp3nS
75	#num#	k4
%	%	kIx~
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
hlavních	hlavní	k2eAgFnPc2d1
ochotských	ochotský	k2eAgFnPc2d1
ulic	ulice	k1gFnPc2
(	(	kIx(
<g/>
rok	rok	k1gInSc1
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Centrální	centrální	k2eAgNnSc1d1
ochotské	ochotský	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
<g/>
,	,	kIx,
socha	socha	k1gFnSc1
Lenina	Lenin	k1gMnSc2
<g/>
,	,	kIx,
Dům	dům	k1gInSc1
Kultury	kultura	k1gFnSc2
(	(	kIx(
<g/>
rok	rok	k1gInSc1
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ochotské	ochotský	k2eAgNnSc4d1
nábřeží	nábřeží	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohled	pohled	k1gInSc1
na	na	k7c4
východ	východ	k1gInSc4
směrem	směr	k1gInSc7
k	k	k7c3
mysu	mys	k1gInSc3
Marekan	Marekana	k1gFnPc2
a	a	k8xC
hřebenu	hřeben	k1gInSc2
Lanžinského	Lanžinský	k2eAgInSc2d1
(	(	kIx(
<g/>
rok	rok	k1gInSc4
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Směr	směr	k1gInSc1
na	na	k7c4
letiště	letiště	k1gNnSc4
(	(	kIx(
<g/>
rok	rok	k1gInSc1
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
Ochotsk	Ochotsk	k1gInSc4
od	od	k7c2
moře	moře	k1gNnSc2
-	-	kIx~
konec	konec	k1gInSc1
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Global	globat	k5eAaImAgInS
Gazetteer	Gazetteer	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falling	Falling	k1gInSc1
Rain	Rain	k1gNnSc1
Genomics	Genomics	k1gInSc1
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ruský	ruský	k2eAgInSc1d1
federální	federální	k2eAgInSc1d1
zákon	zákon	k1gInSc1
248-Ф	248-Ф	k?
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
П	П	k?
Р	Р	k?
Ф	Ф	k?
<g/>
,	,	kIx,
2014-07-21	2014-07-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
П	П	k?
з	з	k?
в	в	k?
о	о	k?
П	П	k?
в	в	k?
Е	Е	k?
-	-	kIx~
г	г	k?
О	О	k?
<g/>
.	.	kIx.
Р	Р	k?
г	г	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Šťastný	Šťastný	k1gMnSc1
Deň	Deň	k1gMnSc1
Tichomorskej	Tichomorskej	k1gFnSc4
Flotily	flotila	k1gFnSc2
Ruska	Rusko	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Л	Л	k?
<g/>
,	,	kIx,
Г	Г	k?
М	М	k?
<g/>
.	.	kIx.
Н	Н	k?
х	х	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
246	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Г	Г	k?
О	О	k?
<g/>
.	.	kIx.
www.hrono.info	www.hrono.info	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WebCite	WebCit	k1gInSc5
query	quer	k1gInPc4
result	result	k2eAgMnSc1d1
<g/>
.	.	kIx.
www.webcitation.org	www.webcitation.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
П	П	k?
Ф	Ф	k?
Р	Р	k?
о	о	k?
14	#num#	k4
а	а	k?
2006	#num#	k4
г	г	k?
N	N	kA
150	#num#	k4
"	"	kIx"
<g/>
О	О	k?
п	п	k?
п	п	k?
з	з	k?
н	н	k?
т	т	k?
Х	Х	k?
к	к	k?
<g/>
"	"	kIx"
(	(	kIx(
<g/>
с	с	k?
и	и	k?
о	о	k?
2	#num#	k4
м	м	k?
2007	#num#	k4
г	г	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.mccme.ru	www.mccme.ra	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ochotsk	Ochotsk	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rusko	Rusko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7568473-1	7568473-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
88212530	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
125598723	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
88212530	#num#	k4
</s>
