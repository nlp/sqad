<s>
Ochotsk	Ochotsk	k1gInSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
О	О	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ruské	ruský	k2eAgNnSc1d1
přístavní	přístavní	k2eAgNnSc1d1
město	město	k1gNnSc1
ležící	ležící	k2eAgNnSc1d1
v	v	k7c6
Chabarovském	chabarovský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Buď	buď	k8xC
od	od	k7c2
slova	slovo	k1gNnSc2
achot	achot	k1gInSc1
-	-	kIx~
velký	velký	k2eAgInSc1d1
nebo	nebo	k8xC
od	od	k7c2
ochat	ochat	k5eAaImF
-	-	kIx~
řeka	řeka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
kozáci	kozák	k1gMnPc1
si	se	k3xPyFc3
jedno	jeden	k4xCgNnSc1
z	z	k7c2
těchto	tento	k3xDgNnPc2
dvou	dva	k4xCgNnPc2
slov	slovo	k1gNnPc2
poruštili	poruštit	k5eAaPmAgMnP
a	a	k8xC
vzniklo	vzniknout	k5eAaPmAgNnS
z	z	k7c2
toho	ten	k3xDgNnSc2
Ochotsk	Ochotsk	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>