<s>
Marmolada	Marmolada	k1gFnSc1	Marmolada
(	(	kIx(	(
<g/>
ladinsky	ladinsky	k6eAd1	ladinsky
<g/>
:	:	kIx,	:
Marmoleda	Marmoleda	k1gMnSc1	Marmoleda
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Marmolata	Marmole	k1gNnPc1	Marmole
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
3343	[number]	k4	3343
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Dolomit	Dolomity	k1gInPc2	Dolomity
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
východně	východně	k6eAd1	východně
od	od	k7c2	od
Trenta	Trent	k1gInSc2	Trent
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Itálii	Itálie	k1gFnSc6	Itálie
asi	asi	k9	asi
100	[number]	k4	100
kilometrů	kilometr	k1gInPc2	kilometr
severo-severovýchodně	severoeverovýchodně	k6eAd1	severo-severovýchodně
od	od	k7c2	od
Benátek	Benátky	k1gFnPc2	Benátky
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgFnPc2	který
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
jasného	jasný	k2eAgInSc2d1	jasný
dne	den	k1gInSc2	den
viditelná	viditelný	k2eAgFnSc1d1	viditelná
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
hřeben	hřeben	k1gInSc1	hřeben
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
východu	východ	k1gInSc3	východ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jih	jih	k1gInSc4	jih
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
kolmo	kolmo	k6eAd1	kolmo
láme	lámat	k5eAaImIp3nS	lámat
do	do	k7c2	do
skalní	skalní	k2eAgFnSc2d1	skalní
stěny	stěna	k1gFnSc2	stěna
2	[number]	k4	2
km	km	kA	km
široké	široký	k2eAgFnSc2d1	široká
a	a	k8xC	a
800	[number]	k4	800
m	m	kA	m
vysoké	vysoká	k1gFnSc2	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
plochý	plochý	k2eAgInSc1d1	plochý
Marmoladský	Marmoladský	k2eAgInSc1d1	Marmoladský
ledovec	ledovec	k1gInSc1	ledovec
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgInSc1d1	jediný
velký	velký	k2eAgInSc1d1	velký
ledovec	ledovec	k1gInSc1	ledovec
v	v	k7c6	v
Dolomitech	Dolomity	k1gInPc6	Dolomity
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
je	být	k5eAaImIp3nS	být
během	během	k7c2	během
léta	léto	k1gNnSc2	léto
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
malá	malý	k2eAgFnSc1d1	malá
horská	horský	k2eAgFnSc1d1	horská
chata	chata	k1gFnSc1	chata
<g/>
.	.	kIx.	.
</s>
<s>
Hřeben	hřeben	k1gInSc1	hřeben
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
několika	několik	k4yIc2	několik
vrcholů	vrchol	k1gInPc2	vrchol
se	s	k7c7	s
snižující	snižující	k2eAgFnSc7d1	snižující
se	se	k3xPyFc4	se
výškou	výška	k1gFnSc7	výška
od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
východu	východ	k1gInSc3	východ
<g/>
:	:	kIx,	:
Punta	punto	k1gNnSc2	punto
Penia	Penium	k1gNnSc2	Penium
(	(	kIx(	(
<g/>
3343	[number]	k4	3343
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Punta	punto	k1gNnSc2	punto
Rocca	Roccum	k1gNnSc2	Roccum
(	(	kIx(	(
<g/>
3309	[number]	k4	3309
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Punta	punto	k1gNnSc2	punto
Ombretta	Ombretto	k1gNnSc2	Ombretto
(	(	kIx(	(
<g/>
3230	[number]	k4	3230
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Monte	Mont	k1gInSc5	Mont
Serauta	Seraut	k1gMnSc2	Seraut
(	(	kIx(	(
<g/>
3069	[number]	k4	3069
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Pizzo	pizza	k1gFnSc5	pizza
Serauta	Seraut	k1gMnSc2	Seraut
(	(	kIx(	(
<g/>
3035	[number]	k4	3035
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
lyžařské	lyžařský	k2eAgFnSc2d1	lyžařská
sezony	sezona	k1gFnSc2	sezona
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
sjezdovka	sjezdovka	k1gFnSc1	sjezdovka
Marmolady	Marmolad	k1gInPc4	Marmolad
otevřená	otevřený	k2eAgFnSc1d1	otevřená
pro	pro	k7c4	pro
lyžaře	lyžař	k1gMnPc4	lyžař
a	a	k8xC	a
snowboardisty	snowboardista	k1gMnPc4	snowboardista
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
probíhala	probíhat	k5eAaImAgFnS	probíhat
Marmoladou	Marmoladý	k2eAgFnSc4d1	Marmoladý
část	část	k1gFnSc4	část
hranice	hranice	k1gFnSc2	hranice
a	a	k8xC	a
fronty	fronta	k1gFnSc2	fronta
mezi	mezi	k7c7	mezi
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Rakušané	Rakušan	k1gMnPc1	Rakušan
v	v	k7c6	v
ledovci	ledovec	k1gInSc6	ledovec
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
celé	celý	k2eAgNnSc4d1	celé
vojenské	vojenský	k2eAgNnSc4d1	vojenské
městečko	městečko	k1gNnSc4	městečko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
největším	veliký	k2eAgNnSc6d3	veliký
lavinovém	lavinový	k2eAgNnSc6d1	lavinové
neštěstí	neštěstí	k1gNnSc6	neštěstí
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c4	o
300	[number]	k4	300
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Ústup	ústup	k1gInSc1	ústup
ledovců	ledovec	k1gInPc2	ledovec
dnes	dnes	k6eAd1	dnes
občas	občas	k6eAd1	občas
vyplavuje	vyplavovat	k5eAaImIp3nS	vyplavovat
mrtvé	mrtvý	k1gMnPc4	mrtvý
<g/>
,	,	kIx,	,
výzbroj	výzbroj	k1gFnSc4	výzbroj
a	a	k8xC	a
výstroj	výstroj	k1gFnSc4	výstroj
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
výstup	výstup	k1gInSc1	výstup
-	-	kIx~	-
rakouský	rakouský	k2eAgMnSc1d1	rakouský
horolezec	horolezec	k1gMnSc1	horolezec
Paul	Paula	k1gFnPc2	Paula
Grohmann	Grohmann	k1gInSc4	Grohmann
s	s	k7c7	s
italskými	italský	k2eAgMnPc7d1	italský
vůdci	vůdce	k1gMnPc7	vůdce
A.	A.	kA	A.
a	a	k8xC	a
F.	F.	kA	F.
Dimaiovými	Dimaiový	k2eAgFnPc7d1	Dimaiový
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c4	na
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
bod	bod	k1gInSc4	bod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
předtím	předtím	k6eAd1	předtím
byly	být	k5eAaImAgInP	být
uskutečněny	uskutečněn	k2eAgInPc1d1	uskutečněn
výstupy	výstup	k1gInPc1	výstup
na	na	k7c4	na
hřeben	hřeben	k1gInSc4	hřeben
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
na	na	k7c4	na
Punta	punto	k1gNnPc4	punto
Penia	Penium	k1gNnSc2	Penium
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
vede	vést	k5eAaImIp3nS	vést
převážně	převážně	k6eAd1	převážně
po	po	k7c6	po
ledovci	ledovec	k1gInSc6	ledovec
-	-	kIx~	-
překračuje	překračovat	k5eAaImIp3nS	překračovat
ledové	ledový	k2eAgFnPc4d1	ledová
trhliny	trhlina	k1gFnPc4	trhlina
a	a	k8xC	a
ve	v	k7c6	v
skále	skála	k1gFnSc6	skála
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
obtížnost	obtížnost	k1gFnSc4	obtížnost
II	II	kA	II
UIAA	UIAA	kA	UIAA
<g/>
.	.	kIx.	.
</s>
<s>
Prvovýstup	prvovýstup	k1gInSc4	prvovýstup
západním	západní	k2eAgInSc7d1	západní
hřebenem	hřeben	k1gInSc7	hřeben
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
H.	H.	kA	H.
Seyffert	Seyffert	k1gInSc1	Seyffert
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Dittmann	Dittmann	k1gInSc1	Dittmann
<g/>
,	,	kIx,	,
L.	L.	kA	L.
Rizzi	Rizze	k1gFnSc4	Rizze
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
tam	tam	k6eAd1	tam
již	již	k6eAd1	již
vedla	vést	k5eAaImAgFnS	vést
zajištěná	zajištěný	k2eAgFnSc1d1	zajištěná
cesta	cesta	k1gFnSc1	cesta
Hans-Seyffert-Weg	Hans-Seyffert-Wega	k1gFnPc2	Hans-Seyffert-Wega
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
trasou	trasa	k1gFnSc7	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
severu	sever	k1gInSc2	sever
se	se	k3xPyFc4	se
stoupá	stoupat	k5eAaImIp3nS	stoupat
nejprve	nejprve	k6eAd1	nejprve
po	po	k7c6	po
sněhu	sníh	k1gInSc6	sníh
a	a	k8xC	a
poté	poté	k6eAd1	poté
po	po	k7c6	po
skále	skála	k1gFnSc6	skála
<g/>
,	,	kIx,	,
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
pouze	pouze	k6eAd1	pouze
po	po	k7c6	po
skále	skála	k1gFnSc6	skála
<g/>
.	.	kIx.	.
</s>
<s>
Prvovýstup	prvovýstup	k1gInSc4	prvovýstup
jižní	jižní	k2eAgFnSc7d1	jižní
stěnou	stěna	k1gFnSc7	stěna
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
Beatrice	Beatrice	k1gFnSc1	Beatrice
Tomassonová	Tomassonová	k1gFnSc1	Tomassonová
<g/>
,	,	kIx,	,
Michele	Michel	k1gInPc1	Michel
Bettega	Betteg	k1gMnSc2	Betteg
<g/>
,	,	kIx,	,
B	B	kA	B
Zagonel	Zagonel	k1gInSc1	Zagonel
<g/>
.	.	kIx.	.
</s>
<s>
Průlomový	průlomový	k2eAgInSc1d1	průlomový
výkon	výkon	k1gInSc1	výkon
ve	v	k7c6	v
strmé	strmý	k2eAgFnSc6d1	strmá
stěně	stěna	k1gFnSc6	stěna
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
horolezeckým	horolezecký	k2eAgInSc7d1	horolezecký
cílem	cíl	k1gInSc7	cíl
obtížnosti	obtížnost	k1gFnSc2	obtížnost
4	[number]	k4	4
<g/>
+	+	kIx~	+
UIAA	UIAA	kA	UIAA
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
na	na	k7c4	na
zkrácení	zkrácení	k1gNnSc4	zkrácení
túr	túra	k1gFnPc2	túra
používá	používat	k5eAaImIp3nS	používat
lanovka	lanovka	k1gFnSc1	lanovka
Lago	Lago	k6eAd1	Lago
Fedaia	Fedaius	k1gMnSc2	Fedaius
-	-	kIx~	-
Rif	rif	k1gInSc1	rif
<g/>
.	.	kIx.	.
</s>
<s>
Pian	piano	k1gNnPc2	piano
dei	dei	k?	dei
Fiacchoni	Fiacchon	k1gMnPc1	Fiacchon
(	(	kIx(	(
<g/>
2626	[number]	k4	2626
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kabinovou	kabinový	k2eAgFnSc7d1	kabinová
lanovkou	lanovka	k1gFnSc7	lanovka
z	z	k7c2	z
Malga	Malg	k1gMnSc2	Malg
Ciapela	Ciapel	k1gMnSc2	Ciapel
je	být	k5eAaImIp3nS	být
zpřístupněn	zpřístupnit	k5eAaPmNgInS	zpřístupnit
i	i	k9	i
vrchol	vrchol	k1gInSc1	vrchol
Punta	punto	k1gNnSc2	punto
di	di	k?	di
Rocca	Rocca	k1gMnSc1	Rocca
<g/>
.	.	kIx.	.
</s>
<s>
Skialpinisté	Skialpinista	k1gMnPc1	Skialpinista
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
po	po	k7c6	po
ledovci	ledovec	k1gInSc6	ledovec
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
nebo	nebo	k8xC	nebo
lezecky	lezecky	k6eAd1	lezecky
po	po	k7c6	po
zajištěné	zajištěný	k2eAgFnSc6d1	zajištěná
cestě	cesta	k1gFnSc6	cesta
západním	západní	k2eAgInSc7d1	západní
hřebenem	hřeben	k1gInSc7	hřeben
a	a	k8xC	a
sjíždějí	sjíždět	k5eAaImIp3nP	sjíždět
ledovcem	ledovec	k1gInSc7	ledovec
přerušeným	přerušený	k2eAgFnPc3d1	přerušená
jedním	jeden	k4xCgInSc7	jeden
skalním	skalní	k2eAgInSc7d1	skalní
výšvihem	výšvih	k1gInSc7	výšvih
<g/>
.	.	kIx.	.
</s>
<s>
Freeriding	Freeriding	k1gInSc1	Freeriding
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
i	i	k8xC	i
snowboardu	snowboard	k1gInSc6	snowboard
se	se	k3xPyFc4	se
provozuje	provozovat	k5eAaImIp3nS	provozovat
od	od	k7c2	od
horní	horní	k2eAgFnSc2d1	horní
stanice	stanice	k1gFnSc2	stanice
lanovky	lanovka	k1gFnSc2	lanovka
Punta	punto	k1gNnSc2	punto
di	di	k?	di
Rocca	Rocca	k1gMnSc1	Rocca
po	po	k7c6	po
ledovci	ledovec	k1gInSc6	ledovec
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
Fedaia	Fedaium	k1gNnSc2	Fedaium
<g/>
.	.	kIx.	.
</s>
<s>
Heliskiingová	Heliskiingový	k2eAgFnSc1d1	Heliskiingový
trasa	trasa	k1gFnSc1	trasa
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
Punta	punto	k1gNnSc2	punto
Penia	Penium	k1gNnSc2	Penium
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
Fedaia	Fedaium	k1gNnSc2	Fedaium
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
do	do	k7c2	do
obce	obec	k1gFnSc2	obec
Malga	Malg	k1gMnSc2	Malg
Ciapela	Ciapel	k1gMnSc2	Ciapel
<g/>
.	.	kIx.	.
</s>
