<s>
Server	server	k1gInSc1	server
Message	Message	k1gFnSc1	Message
Block	Block	k1gMnSc1	Block
(	(	kIx(	(
<g/>
SMB	SMB	kA	SMB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
Common	Common	k1gInSc4	Common
Internet	Internet	k1gInSc1	Internet
File	Fil	k1gInSc2	Fil
System	Systo	k1gNnSc7	Systo
(	(	kIx(	(
<g/>
CIFS	CIFS	kA	CIFS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
síťový	síťový	k2eAgInSc4d1	síťový
komunikační	komunikační	k2eAgInSc4d1	komunikační
protokol	protokol	k1gInSc4	protokol
aplikační	aplikační	k2eAgFnSc2d1	aplikační
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
sdílenému	sdílený	k2eAgInSc3d1	sdílený
přístupu	přístup	k1gInSc3	přístup
k	k	k7c3	k
souborům	soubor	k1gInPc3	soubor
<g/>
,	,	kIx,	,
tiskárnám	tiskárna	k1gFnPc3	tiskárna
<g/>
,	,	kIx,	,
sériovým	sériový	k2eAgNnPc3d1	sériové
portům	porto	k1gNnPc3	porto
a	a	k8xC	a
další	další	k2eAgFnSc3d1	další
komunikaci	komunikace	k1gFnSc3	komunikace
mezi	mezi	k7c4	mezi
uzly	uzel	k1gInPc4	uzel
na	na	k7c6	na
síti	síť	k1gFnSc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
Poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
také	také	k9	také
autentizovaný	autentizovaný	k2eAgInSc1d1	autentizovaný
mechanismus	mechanismus	k1gInSc1	mechanismus
pro	pro	k7c4	pro
meziprocesovou	meziprocesový	k2eAgFnSc4d1	meziprocesová
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
počítačích	počítač	k1gInPc6	počítač
s	s	k7c7	s
operačními	operační	k2eAgInPc7d1	operační
systémy	systém	k1gInPc7	systém
rodiny	rodina	k1gFnSc2	rodina
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Protokol	protokol	k1gInSc1	protokol
SMB	SMB	kA	SMB
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
Barry	Barra	k1gFnSc2	Barra
Feigenbaum	Feigenbaum	k1gInSc1	Feigenbaum
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
IBM	IBM	kA	IBM
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
změnit	změnit	k5eAaPmF	změnit
DOSové	dosový	k2eAgNnSc4d1	dosové
"	"	kIx"	"
<g/>
přerušení	přerušení	k1gNnSc4	přerušení
33	[number]	k4	33
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
lokálním	lokální	k2eAgInPc3d1	lokální
souborům	soubor	k1gInPc3	soubor
na	na	k7c4	na
síťový	síťový	k2eAgInSc4d1	síťový
systém	systém	k1gInSc4	systém
souborů	soubor	k1gInPc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
provedl	provést	k5eAaPmAgInS	provést
významné	významný	k2eAgFnPc4d1	významná
změny	změna	k1gFnPc4	změna
a	a	k8xC	a
protokol	protokol	k1gInSc4	protokol
včlenil	včlenit	k5eAaPmAgMnS	včlenit
do	do	k7c2	do
produktu	produkt	k1gInSc2	produkt
LAN	lano	k1gNnPc2	lano
Manager	manager	k1gMnSc1	manager
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
3	[number]	k4	3
<g/>
Com	Com	k1gFnPc2	Com
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
protokol	protokol	k1gInSc1	protokol
dále	daleko	k6eAd2	daleko
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
ve	v	k7c6	v
Windows	Windows	kA	Windows
for	forum	k1gNnPc2	forum
Workgroups	Workgroups	k1gInSc1	Workgroups
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
i	i	k8xC	i
dalších	další	k2eAgFnPc6d1	další
verzích	verze	k1gFnPc6	verze
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
Samba	samba	k1gFnSc1	samba
přinesl	přinést	k5eAaPmAgInS	přinést
svobodnou	svobodný	k2eAgFnSc4d1	svobodná
implementaci	implementace	k1gFnSc4	implementace
SMB	SMB	kA	SMB
protokolu	protokol	k1gInSc2	protokol
vyvinutou	vyvinutý	k2eAgFnSc7d1	vyvinutá
pomocí	pomoc	k1gFnSc7	pomoc
reverzního	reverzní	k2eAgNnSc2d1	reverzní
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Windows	Windows	kA	Windows
Vista	vista	k2eAgFnSc1d1	vista
(	(	kIx(	(
<g/>
vydané	vydaný	k2eAgNnSc1d1	vydané
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
představena	představen	k2eAgFnSc1d1	představena
2	[number]	k4	2
<g/>
.	.	kIx.	.
verze	verze	k1gFnSc1	verze
protokolu	protokol	k1gInSc2	protokol
(	(	kIx(	(
<g/>
Server	server	k1gInSc1	server
Message	Message	k1gNnSc1	Message
Block	Block	k1gInSc1	Block
verze	verze	k1gFnSc1	verze
2.0	[number]	k4	2.0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protokol	protokol	k1gInSc1	protokol
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
firmě	firma	k1gFnSc3	firma
Microsoft	Microsoft	kA	Microsoft
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
v	v	k7c4	v
oblasti	oblast	k1gFnPc4	oblast
souborových	souborový	k2eAgInPc2d1	souborový
serverů	server	k1gInPc2	server
a	a	k8xC	a
klientů	klient	k1gMnPc2	klient
sítí	síť	k1gFnPc2	síť
LAN	lano	k1gNnPc2	lano
<g/>
.	.	kIx.	.
</s>
<s>
Protokol	protokol	k1gInSc4	protokol
SMB	SMB	kA	SMB
využívají	využívat	k5eAaImIp3nP	využívat
souborové	souborový	k2eAgInPc1d1	souborový
a	a	k8xC	a
tiskové	tiskový	k2eAgInPc4d1	tiskový
servery	server	k1gInPc4	server
síťových	síťový	k2eAgInPc2d1	síťový
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
IBM	IBM	kA	IBM
(	(	kIx(	(
<g/>
např.	např.	kA	např.
LAN	lano	k1gNnPc2	lano
Server	server	k1gInSc1	server
<g/>
,	,	kIx,	,
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
Microsoft	Microsoft	kA	Microsoft
(	(	kIx(	(
<g/>
např.	např.	kA	např.
LAN	lano	k1gNnPc2	lano
Manager	manager	k1gMnSc1	manager
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
NT	NT	kA	NT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protokol	protokol	k1gInSc1	protokol
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
klient-server	klienterver	k1gInSc1	klient-server
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
klientům	klient	k1gMnPc3	klient
sítě	síť	k1gFnSc2	síť
přistupovat	přistupovat	k5eAaImF	přistupovat
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
sdíleným	sdílený	k2eAgInPc3d1	sdílený
prostředkům	prostředek	k1gInPc3	prostředek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
sdíleným	sdílený	k2eAgInPc3d1	sdílený
diskům	disk	k1gInPc3	disk
<g/>
,	,	kIx,	,
adresářům	adresář	k1gInPc3	adresář
<g/>
,	,	kIx,	,
tiskovým	tiskový	k2eAgFnPc3d1	tisková
frontám	fronta	k1gFnPc3	fronta
nebo	nebo	k8xC	nebo
pojmenovaným	pojmenovaný	k2eAgInPc3d1	pojmenovaný
kanálům	kanál	k1gInPc3	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Sdílené	sdílený	k2eAgInPc1d1	sdílený
prostředky	prostředek	k1gInPc1	prostředek
jsou	být	k5eAaImIp3nP	být
rozpoznávány	rozpoznáván	k2eAgInPc1d1	rozpoznáván
pomocí	pomocí	k7c2	pomocí
síťové	síťový	k2eAgFnSc2d1	síťová
adresy	adresa	k1gFnSc2	adresa
UNC	UNC	kA	UNC
(	(	kIx(	(
<g/>
\\	\\	k?	\\
<g/>
jméno_serveru	jméno_servrat	k5eAaPmIp1nS	jméno_servrat
<g/>
\	\	kIx~	\
<g/>
jméno_zdroje	jméno_zdroj	k1gInPc4	jméno_zdroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klientská	klientský	k2eAgFnSc1d1	klientská
část	část	k1gFnSc1	část
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
definovat	definovat	k5eAaBmF	definovat
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
sdílené	sdílený	k2eAgInPc4d1	sdílený
prostředky	prostředek	k1gInPc4	prostředek
serveru	server	k1gInSc2	server
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
rozbor	rozbor	k1gInSc4	rozbor
požadavků	požadavek	k1gInPc2	požadavek
odeslaných	odeslaný	k2eAgInPc2d1	odeslaný
klientem	klient	k1gMnSc7	klient
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
bloku	blok	k1gInSc2	blok
(	(	kIx(	(
<g/>
paketu	paket	k1gInSc2	paket
<g/>
)	)	kIx)	)
SMB	SMB	kA	SMB
<g/>
,	,	kIx,	,
porovnává	porovnávat	k5eAaImIp3nS	porovnávat
přístupová	přístupový	k2eAgNnPc4d1	přístupové
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
na	na	k7c6	na
podkladě	podklad	k1gInSc6	podklad
přístupových	přístupový	k2eAgNnPc2d1	přístupové
práv	právo	k1gNnPc2	právo
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
operaci	operace	k1gFnSc4	operace
(	(	kIx(	(
<g/>
vytvoření	vytvoření	k1gNnSc4	vytvoření
adresáře	adresář	k1gInSc2	adresář
<g/>
,	,	kIx,	,
spuštění	spuštění	k1gNnSc2	spuštění
souboru	soubor	k1gInSc2	soubor
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
odpovědí	odpověď	k1gFnSc7	odpověď
poslán	poslat	k5eAaPmNgMnS	poslat
klientu	klient	k1gMnSc3	klient
identickým	identický	k2eAgInSc7d1	identický
blokem	blok	k1gInSc7	blok
SMB	SMB	kA	SMB
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
serveru	server	k1gInSc2	server
SMB	SMB	kA	SMB
k	k	k7c3	k
přístupu	přístup	k1gInSc3	přístup
ke	k	k7c3	k
sdíleným	sdílený	k2eAgInPc3d1	sdílený
prostředkům	prostředek	k1gInPc3	prostředek
máme	mít	k5eAaImIp1nP	mít
dva	dva	k4xCgInPc4	dva
pohledy	pohled	k1gInPc4	pohled
<g/>
:	:	kIx,	:
řízení	řízení	k1gNnSc2	řízení
přístupu	přístup	k1gInSc2	přístup
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
sdíleného	sdílený	k2eAgInSc2d1	sdílený
prostředku	prostředek	k1gInSc2	prostředek
(	(	kIx(	(
<g/>
share	shar	k1gInSc5	shar
level	level	k1gInSc1	level
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řízení	řízení	k1gNnSc1	řízení
přístupu	přístup	k1gInSc2	přístup
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
na	na	k7c4	na
uživatelskou	uživatelský	k2eAgFnSc4d1	Uživatelská
úroveň	úroveň	k1gFnSc4	úroveň
(	(	kIx(	(
<g/>
user	usrat	k5eAaPmRp2nS	usrat
level	level	k1gInSc1	level
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
případě	případ	k1gInSc6	případ
server	server	k1gInSc1	server
povoluje	povolovat	k5eAaImIp3nS	povolovat
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
sdíleným	sdílený	k2eAgInPc3d1	sdílený
prostředkům	prostředek	k1gInPc3	prostředek
na	na	k7c6	na
základě	základ	k1gInSc6	základ
správného	správný	k2eAgNnSc2d1	správné
hesla	heslo	k1gNnSc2	heslo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
přiřazeno	přiřazen	k2eAgNnSc1d1	přiřazeno
k	k	k7c3	k
jednotlivému	jednotlivý	k2eAgInSc3d1	jednotlivý
sdílenému	sdílený	k2eAgInSc3d1	sdílený
prostředku	prostředek	k1gInSc3	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zadání	zadání	k1gNnSc6	zadání
správného	správný	k2eAgNnSc2d1	správné
hesla	heslo	k1gNnSc2	heslo
klientem	klient	k1gMnSc7	klient
je	být	k5eAaImIp3nS	být
klientu	klient	k1gMnSc3	klient
přidělen	přidělit	k5eAaPmNgInS	přidělit
identifikátor	identifikátor	k1gInSc1	identifikátor
prostředku	prostředek	k1gInSc2	prostředek
NID	NID	kA	NID
(	(	kIx(	(
<g/>
Network	network	k1gInSc1	network
ID	Ida	k1gFnPc2	Ida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
něhož	jenž	k3xRgInSc2	jenž
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
k	k	k7c3	k
prostředku	prostředek	k1gInSc3	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
klient	klient	k1gMnSc1	klient
přihlašuje	přihlašovat	k5eAaImIp3nS	přihlašovat
na	na	k7c4	na
server	server	k1gInSc4	server
hned	hned	k6eAd1	hned
pomocí	pomocí	k7c2	pomocí
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
hesla	heslo	k1gNnSc2	heslo
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
správnosti	správnost	k1gFnSc2	správnost
server	server	k1gInSc1	server
přiřadí	přiřadit	k5eAaPmIp3nS	přiřadit
klientu	klient	k1gMnSc3	klient
uživatelský	uživatelský	k2eAgInSc4d1	uživatelský
identifikátor	identifikátor	k1gInSc4	identifikátor
UID	UID	kA	UID
(	(	kIx(	(
<g/>
User	usrat	k5eAaPmRp2nS	usrat
ID	ido	k1gNnPc2	ido
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
něhož	jenž	k3xRgNnSc2	jenž
server	server	k1gInSc1	server
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
přístupová	přístupový	k2eAgNnPc4d1	přístupové
práva	právo	k1gNnPc4	právo
při	při	k7c6	při
manipulaci	manipulace	k1gFnSc6	manipulace
se	s	k7c7	s
sdíleným	sdílený	k2eAgInSc7d1	sdílený
prostředkem	prostředek	k1gInSc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Klient	klient	k1gMnSc1	klient
zasílá	zasílat	k5eAaImIp3nS	zasílat
serveru	server	k1gInSc3	server
požadavek	požadavek	k1gInSc1	požadavek
–	–	k?	–
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
specifikaci	specifikace	k1gFnSc3	specifikace
parametru	parametr	k1gInSc2	parametr
spojení	spojení	k1gNnPc2	spojení
a	a	k8xC	a
verzí	verze	k1gFnPc2	verze
protokolu	protokol	k1gInSc2	protokol
mezi	mezi	k7c7	mezi
serverem	server	k1gInSc7	server
a	a	k8xC	a
klientem	klient	k1gMnSc7	klient
<g/>
.	.	kIx.	.
</s>
<s>
Klient	klient	k1gMnSc1	klient
odešle	odeslat	k5eAaPmIp3nS	odeslat
požadavek	požadavek	k1gInSc4	požadavek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
jméno	jméno	k1gNnSc1	jméno
a	a	k8xC	a
heslo	heslo	k1gNnSc1	heslo
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
server	server	k1gInSc4	server
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
user-level	userevela	k1gFnPc2	user-levela
<g/>
,	,	kIx,	,
přidělí	přidělit	k5eAaPmIp3nP	přidělit
uživateli	uživatel	k1gMnPc7	uživatel
UID	UID	kA	UID
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
představil	představit	k5eAaPmAgInS	představit
novou	nový	k2eAgFnSc4d1	nová
verzi	verze	k1gFnSc4	verze
Server	server	k1gInSc1	server
Message	Message	k1gNnSc2	Message
Block	Blocka	k1gFnPc2	Blocka
(	(	kIx(	(
<g/>
SMB	SMB	kA	SMB
<g/>
)	)	kIx)	)
protokolu	protokol	k1gInSc2	protokol
(	(	kIx(	(
<g/>
SMB	SMB	kA	SMB
2.0	[number]	k4	2.0
nebo	nebo	k8xC	nebo
SMB	SMB	kA	SMB
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
s	s	k7c7	s
Windows	Windows	kA	Windows
Vista	vista	k2eAgInSc2d1	vista
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
protokol	protokol	k1gInSc4	protokol
proprietární	proprietární	k2eAgNnSc1d1	proprietární
<g/>
,	,	kIx,	,
specifikace	specifikace	k1gFnPc1	specifikace
byly	být	k5eAaImAgFnP	být
publikovány	publikovat	k5eAaBmNgFnP	publikovat
a	a	k8xC	a
ostatním	ostatní	k2eAgInPc3d1	ostatní
systémům	systém	k1gInPc3	systém
byla	být	k5eAaImAgFnS	být
umožněna	umožnit	k5eAaPmNgFnS	umožnit
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
operačními	operační	k2eAgInPc7d1	operační
systémy	systém	k1gInPc7	systém
společnosti	společnost	k1gFnSc2	společnost
Microsoft	Microsoft	kA	Microsoft
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
používají	používat	k5eAaImIp3nP	používat
nový	nový	k2eAgInSc4d1	nový
protokol	protokol	k1gInSc4	protokol
<g/>
.	.	kIx.	.
</s>
<s>
SMB2	SMB2	k4	SMB2
redukuje	redukovat	k5eAaBmIp3nS	redukovat
velikost	velikost	k1gFnSc1	velikost
protokolu	protokol	k1gInSc2	protokol
SMB	SMB	kA	SMB
<g/>
1.0	[number]	k4	1.0
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
snižuje	snižovat	k5eAaImIp3nS	snižovat
počet	počet	k1gInSc1	počet
příkazů	příkaz	k1gInPc2	příkaz
a	a	k8xC	a
podpříkazů	podpříkaz	k1gInPc2	podpříkaz
ze	z	k7c2	z
stovek	stovka	k1gFnPc2	stovka
na	na	k7c4	na
pouhých	pouhý	k2eAgNnPc2d1	pouhé
devatenáct	devatenáct	k4xCc4	devatenáct
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
pipelining	pipelining	k1gInSc1	pipelining
(	(	kIx(	(
<g/>
řetězení	řetězení	k1gNnSc1	řetězení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
odeslání	odeslání	k1gNnSc4	odeslání
dalších	další	k2eAgInPc2d1	další
požadavků	požadavek	k1gInPc2	požadavek
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
přijde	přijít	k5eAaPmIp3nS	přijít
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
předchozí	předchozí	k2eAgInSc4d1	předchozí
požadavek	požadavek	k1gInSc4	požadavek
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
výkonu	výkon	k1gInSc2	výkon
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
dodává	dodávat	k5eAaImIp3nS	dodávat
schopnost	schopnost	k1gFnSc4	schopnost
sloučit	sloučit	k5eAaPmF	sloučit
více	hodně	k6eAd2	hodně
akcí	akce	k1gFnPc2	akce
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
požadavku	požadavek	k1gInSc2	požadavek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
významně	významně	k6eAd1	významně
redukuje	redukovat	k5eAaBmIp3nS	redukovat
počet	počet	k1gInSc1	počet
kruhových	kruhový	k2eAgFnPc2d1	kruhová
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
výsledkem	výsledek	k1gInSc7	výsledek
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
zlepšení	zlepšení	k1gNnSc1	zlepšení
výkonu	výkon	k1gInSc2	výkon
<g/>
.	.	kIx.	.
</s>
<s>
SMB1	SMB1	k4	SMB1
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
slučovací	slučovací	k2eAgInSc1d1	slučovací
mechanismus	mechanismus	k1gInSc1	mechanismus
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
jako	jako	k9	jako
AndX	AndX	k1gFnSc7	AndX
<g/>
,	,	kIx,	,
k	k	k7c3	k
sloučení	sloučení	k1gNnSc3	sloučení
více	hodně	k6eAd2	hodně
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
klienti	klient	k1gMnPc1	klient
Microsoftu	Microsoft	k1gInSc2	Microsoft
používají	používat	k5eAaImIp3nP	používat
AndX	AndX	k1gFnSc4	AndX
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
kdy	kdy	k6eAd1	kdy
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
také	také	k9	také
zavádí	zavádět	k5eAaImIp3nS	zavádět
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
Odolný	odolný	k2eAgInSc1d1	odolný
přenos	přenos	k1gInSc1	přenos
souborů	soubor	k1gInPc2	soubor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
připojení	připojení	k1gNnSc4	připojení
přežít	přežít	k5eAaPmF	přežít
krátké	krátký	k2eAgInPc4d1	krátký
výpadky	výpadek	k1gInPc4	výpadek
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc1d1	typický
v	v	k7c6	v
bezdrátové	bezdrátový	k2eAgFnSc6d1	bezdrátová
síti	síť	k1gFnSc6	síť
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
se	se	k3xPyFc4	se
opětovně	opětovně	k6eAd1	opětovně
připojovat	připojovat	k5eAaImF	připojovat
<g/>
.	.	kIx.	.
</s>
<s>
SMB2	SMB2	k4	SMB2
má	mít	k5eAaImIp3nS	mít
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
symbolické	symbolický	k2eAgFnPc4d1	symbolická
adresy	adresa	k1gFnPc4	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnSc4d1	další
vylepšení	vylepšení	k1gNnSc4	vylepšení
patří	patřit	k5eAaImIp3nS	patřit
kešování	kešování	k1gNnSc4	kešování
vlastností	vlastnost	k1gFnPc2	vlastnost
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
zlepšení	zlepšení	k1gNnSc4	zlepšení
podepisování	podepisování	k1gNnSc2	podepisování
zpráv	zpráva	k1gFnPc2	zpráva
pomocí	pomocí	k7c2	pomocí
hašovacího	hašovací	k2eAgInSc2d1	hašovací
algoritmu	algoritmus	k1gInSc2	algoritmus
HMAC	HMAC	kA	HMAC
SHA-256	SHA-256	k1gMnSc1	SHA-256
a	a	k8xC	a
lepší	dobrý	k2eAgFnSc1d2	lepší
škálovatelnost	škálovatelnost	k1gFnSc1	škálovatelnost
zvýšením	zvýšení	k1gNnSc7	zvýšení
počtu	počet	k1gInSc2	počet
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
sdílených	sdílený	k2eAgMnPc2d1	sdílený
a	a	k8xC	a
otevřených	otevřený	k2eAgInPc2d1	otevřený
souborů	soubor	k1gInPc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Protokol	protokol	k1gInSc1	protokol
SMB1	SMB1	k1gFnSc2	SMB1
používá	používat	k5eAaImIp3nS	používat
16	[number]	k4	16
bitovou	bitový	k2eAgFnSc4d1	bitová
velikost	velikost	k1gFnSc4	velikost
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
omezuje	omezovat	k5eAaImIp3nS	omezovat
maximální	maximální	k2eAgFnSc4d1	maximální
velikost	velikost	k1gFnSc4	velikost
bloku	blok	k1gInSc2	blok
na	na	k7c4	na
64	[number]	k4	64
KiB	KiB	k1gFnPc2	KiB
<g/>
.	.	kIx.	.
</s>
<s>
SMB2	SMB2	k4	SMB2
využívá	využívat	k5eAaPmIp3nS	využívat
pro	pro	k7c4	pro
offset	offset	k1gInSc4	offset
v	v	k7c6	v
souboru	soubor	k1gInSc6	soubor
32	[number]	k4	32
bitů	bit	k1gInPc2	bit
nebo	nebo	k8xC	nebo
64	[number]	k4	64
a	a	k8xC	a
128	[number]	k4	128
bitů	bit	k1gInPc2	bit
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
předchozí	předchozí	k2eAgNnSc4d1	předchozí
omezení	omezení	k1gNnSc4	omezení
na	na	k7c4	na
velikost	velikost	k1gFnSc4	velikost
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
výkon	výkon	k1gInSc4	výkon
a	a	k8xC	a
přenos	přenos	k1gInSc4	přenos
velkých	velký	k2eAgInPc2d1	velký
souborů	soubor	k1gInPc2	soubor
přes	přes	k7c4	přes
rychlé	rychlý	k2eAgFnPc4d1	rychlá
sítě	síť	k1gFnPc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
Vista	vista	k2eAgInSc1d1	vista
<g/>
/	/	kIx~	/
<g/>
Server	server	k1gInSc1	server
2008	[number]	k4	2008
a	a	k8xC	a
starší	starý	k2eAgInPc1d2	starší
operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
používají	používat	k5eAaImIp3nP	používat
SMB	SMB	kA	SMB
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
když	když	k8xS	když
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
stroji	stroj	k1gInPc7	stroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
schopny	schopen	k2eAgInPc1d1	schopen
používat	používat	k5eAaImF	používat
SMB	SMB	kA	SMB
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Protokol	protokol	k1gInSc1	protokol
SMB1	SMB1	k1gFnSc2	SMB1
je	být	k5eAaImIp3nS	být
nadále	nadále	k6eAd1	nadále
používán	používat	k5eAaImNgInS	používat
při	při	k7c6	při
komunikaci	komunikace	k1gFnSc6	komunikace
se	s	k7c7	s
staršími	starý	k2eAgFnPc7d2	starší
verzemi	verze	k1gFnPc7	verze
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
Sambou	samba	k1gFnSc7	samba
či	či	k8xC	či
NAS	NAS	kA	NAS
<g/>
.	.	kIx.	.
</s>
<s>
Samba	samba	k1gFnSc1	samba
3.5	[number]	k4	3.5
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
experimentální	experimentální	k2eAgFnSc4d1	experimentální
podporu	podpora	k1gFnSc4	podpora
SMB	SMB	kA	SMB
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Samba	samba	k1gFnSc1	samba
3.6	[number]	k4	3.6
již	již	k6eAd1	již
SMB2	SMB2	k1gFnSc4	SMB2
plně	plně	k6eAd1	plně
podporuje	podporovat	k5eAaImIp3nS	podporovat
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
změny	změna	k1gFnSc2	změna
uživatelských	uživatelský	k2eAgFnPc2d1	Uživatelská
kvót	kvóta	k1gFnPc2	kvóta
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
nástrojů	nástroj	k1gInPc2	nástroj
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
kvót	kvóta	k1gFnPc2	kvóta
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
SMB2	SMB2	k1gMnPc3	SMB2
představen	představen	k2eAgMnSc1d1	představen
<g/>
,	,	kIx,	,
přinesl	přinést	k5eAaPmAgInS	přinést
mnoho	mnoho	k4c4	mnoho
výhod	výhoda	k1gFnPc2	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
SMB	SMB	kA	SMB
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
vyvinut	vyvinut	k2eAgMnSc1d1	vyvinut
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
operačních	operační	k2eAgInPc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
Windows	Windows	kA	Windows
například	například	k6eAd1	například
Xenix	Xenix	k1gInSc1	Xenix
<g/>
,	,	kIx,	,
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
a	a	k8xC	a
VMS	VMS	kA	VMS
<g/>
.	.	kIx.	.
</s>
<s>
SMB	SMB	kA	SMB
2.1	[number]	k4	2.1
byl	být	k5eAaImAgInS	být
zaveden	zaveden	k2eAgInSc1d1	zaveden
s	s	k7c7	s
Windows	Windows	kA	Windows
7	[number]	k4	7
a	a	k8xC	a
Windows	Windows	kA	Windows
Server	server	k1gInSc4	server
2008	[number]	k4	2008
R	R	kA	R
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
přinesl	přinést	k5eAaPmAgInS	přinést
drobné	drobný	k2eAgNnSc4d1	drobné
vylepšení	vylepšení	k1gNnSc4	vylepšení
výkonu	výkon	k1gInSc2	výkon
<g/>
.	.	kIx.	.
</s>
<s>
SMB	SMB	kA	SMB
3.0	[number]	k4	3.0
(	(	kIx(	(
<g/>
předchozí	předchozí	k2eAgInSc1d1	předchozí
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
SMB	SMB	kA	SMB
2.2	[number]	k4	2.2
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
zaveden	zaveden	k2eAgInSc1d1	zaveden
s	s	k7c7	s
Windows	Windows	kA	Windows
8	[number]	k4	8
a	a	k8xC	a
Windows	Windows	kA	Windows
Server	server	k1gInSc4	server
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Přinesl	přinést	k5eAaPmAgMnS	přinést
několik	několik	k4yIc4	několik
významných	významný	k2eAgFnPc2d1	významná
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
SMB	SMB	kA	SMB
Direct	Directa	k1gFnPc2	Directa
Protocol	Protocola	k1gFnPc2	Protocola
a	a	k8xC	a
SMB	SMB	kA	SMB
Multichannel	Multichannel	k1gInSc1	Multichannel
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
přinášejí	přinášet	k5eAaImIp3nP	přinášet
nové	nový	k2eAgFnPc4d1	nová
funkce	funkce	k1gFnPc4	funkce
a	a	k8xC	a
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
výkon	výkon	k1gInSc4	výkon
SMB	SMB	kA	SMB
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
ve	v	k7c6	v
virtualizovaných	virtualizovaný	k2eAgNnPc6d1	virtualizované
datových	datový	k2eAgNnPc6d1	datové
centrech	centrum	k1gNnPc6	centrum
<g/>
.	.	kIx.	.
</s>
<s>
SMB	SMB	kA	SMB
3.02	[number]	k4	3.02
byl	být	k5eAaImAgInS	být
zaveden	zaveden	k2eAgInSc1d1	zaveden
s	s	k7c7	s
Windows	Windows	kA	Windows
8.1	[number]	k4	8.1
a	a	k8xC	a
Windows	Windows	kA	Windows
Server	server	k1gInSc4	server
2012	[number]	k4	2012
R	R	kA	R
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Fedor	Fedor	k1gInSc1	Fedor
Kállay	Kállaa	k1gFnSc2	Kállaa
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Peniak	Peniak	k1gMnSc1	Peniak
<g/>
:	:	kIx,	:
Počítačové	počítačový	k2eAgFnPc1d1	počítačová
sítě	síť	k1gFnPc1	síť
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
aplikace	aplikace	k1gFnPc1	aplikace
<g/>
,	,	kIx,	,
GRADA	GRADA	kA	GRADA
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
;	;	kIx,	;
ISBN	ISBN	kA	ISBN
80-247-0545-1	[number]	k4	80-247-0545-1
</s>
