<s>
Neoklasicismus	neoklasicismus	k1gInSc1
(	(	kIx(
<g/>
literatura	literatura	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Neoklasicismus	neoklasicismus	k1gInSc1
(	(	kIx(
<g/>
též	též	k9
novoklasicismus	novoklasicismus	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
literatuře	literatura	k1gFnSc6
byl	být	k5eAaImAgInS
v	v	k7c6
českém	český	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
nevyhraněný	vyhraněný	k2eNgInSc4d1
umělecký	umělecký	k2eAgInSc4d1
směr	směr	k1gInSc4
z	z	k7c2
počátku	počátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jiných	jiný	k2eAgFnPc6d1
jazykových	jazykový	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vnímán	vnímat	k5eAaImNgInS
odlišně	odlišně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Pojem	pojem	k1gInSc1
neoklasicismus	neoklasicismus	k1gInSc1
v	v	k7c6
evropské	evropský	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
</s>
<s>
Neoklasicismus	neoklasicismus	k1gInSc1
v	v	k7c6
literatuře	literatura	k1gFnSc6
nemá	mít	k5eNaImIp3nS
přesnou	přesný	k2eAgFnSc4d1
definici	definice	k1gFnSc4
a	a	k8xC
v	v	k7c6
různých	různý	k2eAgFnPc6d1
evropských	evropský	k2eAgFnPc6d1
kulturách	kultura	k1gFnPc6
je	být	k5eAaImIp3nS
vykládán	vykládat	k5eAaImNgInS
různě	různě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
německé	německý	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
směr	směr	k1gInSc4
z	z	k7c2
období	období	k1gNnSc2
po	po	k7c6
roce	rok	k1gInSc6
1900	#num#	k4
<g/>
,	,	kIx,
záměrně	záměrně	k6eAd1
se	se	k3xPyFc4
odvracející	odvracející	k2eAgMnSc1d1
od	od	k7c2
moderny	moderna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představitelem	představitel	k1gMnSc7
byla	být	k5eAaImAgNnP
pozdní	pozdní	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
Gerharta	Gerhart	k1gMnSc2
Hauptmanna	Hauptmann	k1gMnSc2
(	(	kIx(
<g/>
1862	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Francii	Francie	k1gFnSc6
se	se	k3xPyFc4
jako	jako	k9
neoklasicismus	neoklasicismus	k1gInSc1
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
néo-classicisme	néo-classicismus	k1gInSc5
<g/>
)	)	kIx)
označuje	označovat	k5eAaImIp3nS
umělecký	umělecký	k2eAgInSc4d1
směr	směr	k1gInSc4
po	po	k7c6
roce	rok	k1gInSc6
1750	#num#	k4
<g/>
,	,	kIx,
odmítající	odmítající	k2eAgNnSc1d1
rokoko	rokoko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
anglické	anglický	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
(	(	kIx(
<g/>
angl.	angl.	k?
neoclassicism	neoclassicism	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
jím	on	k3xPp3gNnSc7
rozumí	rozumět	k5eAaImIp3nS
směr	směr	k1gInSc4
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
končící	končící	k2eAgFnSc2d1
rokem	rok	k1gInSc7
1740	#num#	k4
(	(	kIx(
<g/>
např.	např.	kA
Jonathan	Jonathan	k1gMnSc1
Swift	Swift	k1gMnSc1
a	a	k8xC
Alexander	Alexandra	k1gFnPc2
Pope	pop	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Čechách	Čechy	k1gFnPc6
se	se	k3xPyFc4
<g/>
,	,	kIx,
obdobně	obdobně	k6eAd1
jako	jako	k8xC,k8xS
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
termín	termín	k1gInSc1
vztahuje	vztahovat	k5eAaImIp3nS
především	především	k9
k	k	k7c3
prvnímu	první	k4xOgNnSc3
dvacetiletí	dvacetiletí	k1gNnSc4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ottův	Ottův	k2eAgInSc4d1
slovník	slovník	k1gInSc4
naučný	naučný	k2eAgInSc4d1
</s>
<s>
Ottův	Ottův	k2eAgInSc4d1
slovník	slovník	k1gInSc4
naučný	naučný	k2eAgInSc4d1
nové	nový	k2eAgFnPc4d1
doby	doba	k1gFnPc4
definoval	definovat	k5eAaBmAgInS
novoklasicismus	novoklasicismus	k1gInSc1
v	v	k7c6
literatuře	literatura	k1gFnSc6
jako	jako	k8xC,k8xS
„	„	k?
<g/>
…	…	k?
<g/>
hromadný	hromadný	k2eAgInSc4d1
název	název	k1gInSc4
několika	několik	k4yIc2
literárních	literární	k2eAgNnPc2d1
hnutí	hnutí	k1gNnPc2
v	v	k7c6
XIX	XIX	kA
<g/>
.	.	kIx.
a	a	k8xC
XX	XX	kA
<g/>
.	.	kIx.
století	století	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
usilují	usilovat	k5eAaImIp3nP
o	o	k7c4
obnovu	obnova	k1gFnSc4
literárních	literární	k2eAgFnPc2d1
a	a	k8xC
kulturních	kulturní	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
doby	doba	k1gFnSc2
klasické	klasický	k2eAgFnSc2d1
…	…	k?
pravidelně	pravidelně	k6eAd1
s	s	k7c7
hrotem	hrot	k1gInSc7
proti	proti	k7c3
romantice	romantika	k1gFnSc3
<g/>
…	…	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Čech	Čechy	k1gFnPc2
pronikal	pronikat	k5eAaImAgMnS
podle	podle	k7c2
Slovníku	slovník	k1gInSc2
novoklasicismus	novoklasicismus	k1gInSc4
především	především	k9
pod	pod	k7c7
německým	německý	k2eAgInSc7d1
vlivem	vliv	k1gInSc7
okolo	okolo	k7c2
roku	rok	k1gInSc2
1912	#num#	k4
a	a	k8xC
„	„	k?
<g/>
…	…	k?
<g/>
znamenal	znamenat	k5eAaImAgInS
u	u	k7c2
nás	my	k3xPp1nPc2
překonávání	překonávání	k1gNnSc1
dekorativnosti	dekorativnost	k1gFnSc2
novoromantické	novoromantický	k2eAgFnSc2d1
a	a	k8xC
dekadentní	dekadentní	k2eAgFnSc2d1
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
době	doba	k1gFnSc6
válečné	válečný	k2eAgFnSc6d1
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
vyžil	vyžít	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
“	“	k?
Jako	jako	k8xS,k8xC
představitele	představitel	k1gMnSc4
uvedl	uvést	k5eAaPmAgMnS
z	z	k7c2
českých	český	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
Karla	Karel	k1gMnSc2
a	a	k8xC
Josefa	Josef	k1gMnSc2
Čapka	Čapek	k1gMnSc2
<g/>
,	,	kIx,
Františka	František	k1gMnSc2
Langera	Langer	k1gMnSc2
a	a	k8xC
Františka	František	k1gMnSc2
Khola	Khol	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Národní	národní	k2eAgInPc1d1
listy	list	k1gInPc1
1912	#num#	k4
<g/>
,	,	kIx,
úvod	úvod	k1gInSc4
Šaldovy	Šaldův	k2eAgFnSc2d1
studie	studie	k1gFnSc2
o	o	k7c6
neoklasicismu	neoklasicismus	k1gInSc6
v	v	k7c6
literatuře	literatura	k1gFnSc6
</s>
<s>
Neoklasicismus	neoklasicismus	k1gInSc1
v	v	k7c6
české	český	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
</s>
<s>
Pojem	pojem	k1gInSc1
neoklasicismus	neoklasicismus	k1gInSc1
v	v	k7c6
literatuře	literatura	k1gFnSc6
byl	být	k5eAaImAgInS
v	v	k7c6
češtině	čeština	k1gFnSc6
užíván	užíván	k2eAgMnSc1d1
už	už	k6eAd1
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
F.	F.	kA
X.	X.	kA
Šalda	Šalda	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
p	p	k?
2	#num#	k4
<g/>
]	]	kIx)
Např.	např.	kA
Jan	Jan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
charakterisoval	charakterisovat	k5eAaImAgMnS,k5eAaBmAgMnS,k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1911	#num#	k4
tehdejší	tehdejší	k2eAgFnSc4d1
současnou	současný	k2eAgFnSc4d1
ruskou	ruský	k2eAgFnSc4d1
literaturu	literatura	k1gFnSc4
(	(	kIx(
<g/>
Valerij	Valerij	k1gFnPc1
Brjusov	Brjusovo	k1gNnPc2
<g/>
,	,	kIx,
Alexander	Alexandra	k1gFnPc2
Blok	blok	k1gInSc1
<g/>
,	,	kIx,
Fedor	Fedor	k1gMnSc1
Sologub	Sologub	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnPc1d1
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
novoklasicistní	novoklasicistní	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Novoklasicismus	novoklasicismus	k1gInSc1
nepředstavoval	představovat	k5eNaImAgInS
vyhraněný	vyhraněný	k2eAgInSc4d1
literární	literární	k2eAgInSc4d1
směr	směr	k1gInSc4
<g/>
;	;	kIx,
šlo	jít	k5eAaImAgNnS
spíše	spíše	k9
o	o	k7c4
snahu	snaha	k1gFnSc4
o	o	k7c4
oživení	oživení	k1gNnSc4
zdrojů	zdroj	k1gInPc2
klasické	klasický	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc2
využití	využití	k1gNnSc2
při	při	k7c6
hledání	hledání	k1gNnSc6
nových	nový	k2eAgInPc2d1
výrazových	výrazový	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
neosobní	osobní	k2eNgNnSc4d1
vyprávění	vyprávění	k1gNnSc4
<g/>
,	,	kIx,
jednoduché	jednoduchý	k2eAgNnSc4d1
a	a	k8xC
kompozičně	kompozičně	k6eAd1
vyvážené	vyvážený	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Témata	téma	k1gNnPc4
byla	být	k5eAaImAgFnS
nadčasová	nadčasový	k2eAgFnSc1d1
<g/>
,	,	kIx,
autoři	autor	k1gMnPc1
zdůrazňovali	zdůrazňovat	k5eAaImAgMnP
nadřazenost	nadřazenost	k1gFnSc4
rozumu	rozum	k1gInSc2
a	a	k8xC
morálních	morální	k2eAgFnPc2d1
zásad	zásada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikaly	vznikat	k5eAaImAgFnP
pochyby	pochyba	k1gFnPc1
<g/>
,	,	kIx,
zda	zda	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
moderní	moderní	k2eAgInSc4d1
směr	směr	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
chce	chtít	k5eAaImIp3nS
navrátit	navrátit	k5eAaPmF
dílům	dílo	k1gNnPc3
řád	řád	k1gInSc4
<g/>
,	,	kIx,
či	či	k8xC
zda	zda	k8xS
jde	jít	k5eAaImIp3nS
pouze	pouze	k6eAd1
o	o	k7c4
návrat	návrat	k1gInSc4
k	k	k7c3
dřívějším	dřívější	k2eAgFnPc3d1
formám	forma	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Neoklasicismus	neoklasicismus	k1gInSc1
a	a	k8xC
F.	F.	kA
X.	X.	kA
Šalda	Šalda	k1gMnSc1
</s>
<s>
F.	F.	kA
X.	X.	kA
Šalda	Šalda	k1gMnSc1
široce	široko	k6eAd1
pojednal	pojednat	k5eAaPmAgMnS
teoreticky	teoreticky	k6eAd1
o	o	k7c6
novoklasicismu	novoklasicismus	k1gInSc6
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
článcích	článek	k1gInPc6
v	v	k7c6
Národních	národní	k2eAgInPc6d1
listech	list	k1gInPc6
v	v	k7c6
roce	rok	k1gInSc6
1912	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Šalda	Šalda	k1gMnSc1
oceňoval	oceňovat	k5eAaImAgMnS
novoklasicismus	novoklasicismus	k1gInSc4
jako	jako	k9
jediný	jediný	k2eAgInSc4d1
z	z	k7c2
moderních	moderní	k2eAgInPc2d1
literárních	literární	k2eAgInPc2d1
směrů	směr	k1gInPc2
<g/>
,	,	kIx,
především	především	k9
pro	pro	k7c4
jeho	jeho	k3xOp3gFnSc4
snahu	snaha	k1gFnSc4
o	o	k7c4
obnovení	obnovení	k1gNnSc4
minulých	minulý	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Neoklasicismus	neoklasicismus	k1gInSc1
a	a	k8xC
Josef	Josef	k1gMnSc1
Čapek	Čapek	k1gMnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Čapek	Čapek	k1gMnSc1
kladně	kladně	k6eAd1
hodnotil	hodnotit	k5eAaImAgMnS
především	především	k9
novoklasicistické	novoklasicistický	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
německého	německý	k2eAgMnSc2d1
autora	autor	k1gMnSc2
Paula	Paul	k1gMnSc2
Ernsta	Ernst	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
novelu	novela	k1gFnSc4
Vězeň	vězeň	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
literární	literární	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
byl	být	k5eAaImAgInS
ve	v	k7c6
středu	střed	k1gInSc6
zájmu	zájem	k1gInSc2
Josefa	Josef	k1gMnSc2
Čapka	Čapek	k1gMnSc2
pouze	pouze	k6eAd1
krátké	krátká	k1gFnSc6
období	období	k1gNnSc6
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
výsledkem	výsledek	k1gInSc7
byla	být	k5eAaImAgFnS
povídka	povídka	k1gFnSc1
Živý	živý	k1gMnSc1
plamen	plamen	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2
názory	názor	k1gInPc1
</s>
<s>
Několik	několik	k4yIc1
let	léto	k1gNnPc2
po	po	k7c6
Šaldových	Šaldových	k2eAgInPc6d1
článcích	článek	k1gInPc6
již	již	k6eAd1
byl	být	k5eAaImAgInS
novoklasicismus	novoklasicismus	k1gInSc1
v	v	k7c6
literatuře	literatura	k1gFnSc6
považován	považován	k2eAgInSc1d1
kritikou	kritika	k1gFnSc7
za	za	k7c4
překonaný	překonaný	k2eAgInSc4d1
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
...	...	k?
<g/>
lze	lze	k6eAd1
prostě	prostě	k9
konstatovati	konstatovat	k5eAaBmF
<g/>
,	,	kIx,
že	že	k8xS
novoklassicismus	novoklassicismus	k1gInSc1
jako	jako	k8xS,k8xC
takový	takový	k3xDgMnSc1
u	u	k7c2
nás	my	k3xPp1nPc2
venkoncem	venkoncem	k?
dohrál	dohrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kdož	kdož	k3yRnSc1
přijali	přijmout	k5eAaPmAgMnP
jej	on	k3xPp3gInSc4
bez	bez	k1gInSc4
výhrad	výhrada	k1gFnPc2
jako	jako	k8xC,k8xS
módní	módní	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
,	,	kIx,
opustili	opustit	k5eAaPmAgMnP
jeho	jeho	k3xOp3gFnSc4
korouhev	korouhev	k1gFnSc4
s	s	k7c7
touž	tenž	k3xDgFnSc7,k3xTgFnSc7
ochotou	ochota	k1gFnSc7
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
se	se	k3xPyFc4
zatím	zatím	k6eAd1
přiklonili	přiklonit	k5eAaPmAgMnP
k	k	k7c3
heslům	heslo	k1gNnPc3
ještě	ještě	k9
časovějším	časový	k2eAgMnSc7d2
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Karel	Karel	k1gMnSc1
Sezima	Sezima	k1gNnSc1
<g/>
,	,	kIx,
Lumír	Lumír	k1gMnSc1
<g/>
,	,	kIx,
26.2	26.2	k4
<g/>
.1915	.1915	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I	i	k9
Josef	Josef	k1gMnSc1
Čapek	Čapek	k1gMnSc1
psal	psát	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
o	o	k7c6
novoklasicismu	novoklasicismus	k1gInSc6
již	již	k6eAd1
v	v	k7c6
minulém	minulý	k2eAgInSc6d1
čase	čas	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
neoklasicistických	neoklasicistický	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
</s>
<s>
Autoři	autor	k1gMnPc1
v	v	k7c6
literatuře	literatura	k1gFnSc6
se	s	k7c7
svými	svůj	k3xOyFgNnPc7
tématy	téma	k1gNnPc7
obvykle	obvykle	k6eAd1
vraceli	vracet	k5eAaImAgMnP
do	do	k7c2
minulosti	minulost	k1gFnSc2
<g/>
,	,	kIx,
neusilovali	usilovat	k5eNaImAgMnP
ovšem	ovšem	k9
o	o	k7c4
zachycení	zachycení	k1gNnSc4
dobového	dobový	k2eAgInSc2d1
koloritu	kolorit	k1gInSc2
<g/>
,	,	kIx,
šlo	jít	k5eAaImAgNnS
o	o	k7c4
zpodobení	zpodobení	k1gNnSc4
nadčasových	nadčasový	k2eAgInPc2d1
<g/>
,	,	kIx,
základních	základní	k2eAgInPc2d1
rysů	rys	k1gInPc2
osudu	osud	k1gInSc2
člověka	člověk	k1gMnSc2
ve	v	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikoliv	nikoliv	k9
osobní	osobní	k2eAgInPc4d1
dojmy	dojem	k1gInPc4
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
psychologická	psychologický	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
<g/>
;	;	kIx,
jejich	jejich	k3xOp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
sevřené	sevřený	k2eAgNnSc1d1
neosobní	osobní	k2eNgNnSc1d1
vyprávění	vyprávění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Typickými	typický	k2eAgMnPc7d1
rysy	rys	k1gMnPc7
novoklasicistických	novoklasicistický	k2eAgNnPc2d1
literárních	literární	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
byla	být	k5eAaImAgFnS
tvarová	tvarový	k2eAgFnSc1d1
kázeň	kázeň	k1gFnSc1
<g/>
,	,	kIx,
stylová	stylový	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
<g/>
,	,	kIx,
přesnost	přesnost	k1gFnSc1
a	a	k8xC
preciznost	preciznost	k1gFnSc1
vyjádření	vyjádření	k1gNnSc2
<g/>
,	,	kIx,
kompoziční	kompoziční	k2eAgFnSc4d1
vyváženost	vyváženost	k1gFnSc4
<g/>
;	;	kIx,
díla	dílo	k1gNnSc2
nevyzdvihovla	vyzdvihovnout	k5eNaPmAgFnS
cit	cit	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
intelekt	intelekt	k1gInSc1
a	a	k8xC
nadčasové	nadčasový	k2eAgInPc1d1
jevy	jev	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Znaky	znak	k1gInPc1
novoklasicistní	novoklasicistní	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
klade	klást	k5eAaImIp3nS
důraz	důraz	k1gInSc4
na	na	k7c4
intelekt	intelekt	k1gInSc4
</s>
<s>
důraz	důraz	k1gInSc1
na	na	k7c4
svébytný	svébytný	k2eAgInSc4d1
tvar	tvar	k1gInSc4
uměleckého	umělecký	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
</s>
<s>
důraz	důraz	k1gInSc1
na	na	k7c4
přísně	přísně	k6eAd1
vyváženou	vyvážený	k2eAgFnSc4d1
tvorbu	tvorba	k1gFnSc4
</s>
<s>
soustředění	soustředění	k1gNnSc4
k	k	k7c3
souvislé	souvislý	k2eAgFnSc3d1
epické	epický	k2eAgFnSc3d1
linii	linie	k1gFnSc3
</s>
<s>
spád	spád	k1gInSc4
děje	dít	k5eAaImIp3nS
</s>
<s>
neosobní	osobní	k2eNgInSc1d1
vypravěčský	vypravěčský	k2eAgInSc1d1
styl	styl	k1gInSc1
</s>
<s>
Žánry	žánr	k1gInPc1
typické	typický	k2eAgInPc1d1
pro	pro	k7c4
novoklasicismus	novoklasicismus	k1gInSc4
byly	být	k5eAaImAgFnP
povídka	povídka	k1gFnSc1
a	a	k8xC
novela	novela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
novoklasistická	novoklasistický	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
(	(	kIx(
<g/>
výběr	výběr	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Antonín	Antonín	k1gMnSc1
Sova	Sova	k1gMnSc1
<g/>
:	:	kIx,
Pankrác	Pankrác	k1gMnSc1
Budecius	Budecius	k1gMnSc1
<g/>
,	,	kIx,
kantor	kantor	k1gMnSc1
(	(	kIx(
<g/>
1916	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
příklady	příklad	k1gInPc4
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
vydána	vydat	k5eAaPmNgFnS
zhruba	zhruba	k6eAd1
od	od	k7c2
začátku	začátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
do	do	k7c2
konce	konec	k1gInSc2
německé	německý	k2eAgFnSc2d1
okupace	okupace	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
závorce	závorka	k1gFnSc6
uvedeno	uvést	k5eAaPmNgNnS
datum	datum	k1gNnSc1
prvního	první	k4xOgNnSc2
vydání	vydání	k1gNnSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
František	František	k1gMnSc1
Langer	Langer	k1gMnSc1
–	–	k?
Zlatá	zlatá	k1gFnSc1
Venuše	Venuše	k1gFnSc2
–	–	k?
(	(	kIx(
<g/>
kniha	kniha	k1gFnSc1
prosy	prosa	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Grosman	Grosman	k1gMnSc1
a	a	k8xC
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1910	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Snílci	snílek	k1gMnPc1
a	a	k8xC
Vrahové	vrah	k1gMnPc1
–	–	k?
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
–	–	k?
Král	Král	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vinohrady	Vinohrady	k1gInPc1
<g/>
,	,	kIx,
G.	G.	kA
Voleský	Voleský	k2eAgMnSc1d1
<g/>
,	,	kIx,
1920	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
Čapek	Čapek	k1gMnSc1
–	–	k?
Živý	živý	k2eAgInSc1d1
plamen	plamen	k1gInSc1
(	(	kIx(
<g/>
V	v	k7c6
souboru	soubor	k1gInSc6
bratří	bratr	k1gMnPc2
Čapků	Čapek	k1gMnPc2
Zářivé	zářivý	k2eAgFnSc2d1
hlubiny	hlubina	k1gFnSc2
<g/>
,	,	kIx,
František	František	k1gMnSc1
Borový	borový	k2eAgMnSc1d1
<g/>
,	,	kIx,
1916	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
Čapek	Čapek	k1gMnSc1
–	–	k?
Mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
polibky	polibek	k1gInPc7
(	(	kIx(
<g/>
V	v	k7c6
souboru	soubor	k1gInSc6
Zářivé	zářivý	k2eAgFnSc2d1
hlubiny	hlubina	k1gFnSc2
<g/>
,	,	kIx,
František	František	k1gMnSc1
Borový	borový	k2eAgMnSc1d1
<g/>
,	,	kIx,
1916	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Antonín	Antonín	k1gMnSc1
Sova	Sova	k1gMnSc1
–	–	k?
Pankrác	Pankrác	k1gMnSc1
Budecius	Budecius	k1gMnSc1
<g/>
,	,	kIx,
kantor	kantor	k1gMnSc1
(	(	kIx(
<g/>
quasi	quasi	k6eAd1
legenda	legenda	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Hejda	Hejda	k1gMnSc1
a	a	k8xC
Tuček	Tuček	k1gMnSc1
<g/>
,	,	kIx,
1916	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Viktor	Viktor	k1gMnSc1
Dyk	Dyk	k?
–	–	k?
Krysař	krysař	k1gMnSc1
–	–	k?
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Borový	borový	k2eAgMnSc1d1
<g/>
,	,	kIx,
1915	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Božena	Božena	k1gFnSc1
Benešová	Benešová	k1gFnSc1
–	–	k?
V	v	k7c6
soumraku	soumrak	k1gInSc6
(	(	kIx(
<g/>
časopisecky	časopisecky	k6eAd1
<g/>
,	,	kIx,
Beseda	beseda	k1gFnSc1
času	čas	k1gInSc2
<g/>
,	,	kIx,
1900	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Kruté	krutý	k2eAgNnSc4d1
mládí	mládí	k1gNnSc4
–	–	k?
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Jos	Jos	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
R.	R.	kA
Vilímek	Vilímek	k1gMnSc1
<g/>
,	,	kIx,
1919	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nedobytá	dobytý	k2eNgNnPc4d1
vítězství	vítězství	k1gNnPc4
–	–	k?
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Vilímek	Vilímek	k1gMnSc1
<g/>
,	,	kIx,
1910	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Růžena	Růžena	k1gFnSc1
Svobodová	Svobodová	k1gFnSc1
–	–	k?
Černí	černit	k5eAaImIp3nS
myslivci	myslivec	k1gMnPc1
(	(	kIx(
<g/>
horské	horský	k2eAgInPc1d1
romány	román	k1gInPc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Laichter	Laichter	k1gMnSc1
<g/>
,	,	kIx,
1908	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Hrdinné	hrdinný	k2eAgNnSc1d1
a	a	k8xC
bezpomocné	bezpomocný	k2eAgNnSc1d1
dětství	dětství	k1gNnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Unie	unie	k1gFnSc1
<g/>
,	,	kIx,
1920	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
František	František	k1gMnSc1
Xaver	Xaver	k1gMnSc1
Šalda	Šalda	k1gMnSc1
–	–	k?
Dřevoryty	dřevoryt	k1gInPc1
staré	starý	k2eAgFnSc2d1
i	i	k8xC
nové	nový	k2eAgFnSc2d1
povídky	povídka	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Melantrich	Melantrich	k1gInSc1
<g/>
,	,	kIx,
1935	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Hilbert	Hilbert	k1gMnSc1
–	–	k?
Rytíř	Rytíř	k1gMnSc1
Kura	kur	k1gMnSc2
(	(	kIx(
<g/>
román	román	k1gInSc1
<g/>
,	,	kIx,
Bursík	Bursík	k1gMnSc1
a	a	k8xC
Kohout	Kohout	k1gMnSc1
<g/>
,	,	kIx,
1910	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
František	František	k1gMnSc1
Khol	Khol	k1gMnSc1
–	–	k?
Illusionisté	Illusionista	k1gMnPc1
(	(	kIx(
<g/>
povídky	povídka	k1gFnPc4
a	a	k8xC
krátká	krátký	k2eAgFnSc1d1
prosa	prosa	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Pelcl	Pelcl	k1gInSc1
<g/>
,	,	kIx,
1911	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Rozmary	rozmar	k1gInPc1
lásky	láska	k1gFnSc2
(	(	kIx(
<g/>
novely	novela	k1gFnSc2
z	z	k7c2
renaissance	renaissance	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Spolek	spolek	k1gInSc1
českých	český	k2eAgMnPc2d1
bibliofilů	bibliofil	k1gMnPc2
<g/>
,	,	kIx,
1915	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Zrcadlo	zrcadlo	k1gNnSc1
v	v	k7c6
baru	bar	k1gInSc6
(	(	kIx(
<g/>
novely	novela	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Borový	borový	k2eAgMnSc1d1
<g/>
,	,	kIx,
1916	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
František	František	k1gMnSc1
Kubka	Kubka	k1gMnSc1
–	–	k?
Skytský	skytský	k2eAgMnSc1d1
jezdec	jezdec	k1gMnSc1
(	(	kIx(
<g/>
povídky	povídka	k1gFnPc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Borový	borový	k2eAgMnSc1d1
<g/>
,	,	kIx,
1941	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pražské	pražský	k2eAgNnSc1d1
nokturno	nokturno	k1gNnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Borový	borový	k2eAgMnSc1d1
<g/>
,	,	kIx,
1943	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Karlštejnské	karlštejnský	k2eAgFnPc1d1
vigilie	vigilie	k1gFnPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Borový	borový	k2eAgMnSc1d1
<g/>
,	,	kIx,
1944	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
František	František	k1gMnSc1
Heřmánek	Heřmánek	k1gMnSc1
–	–	k?
U	u	k7c2
bratra	bratr	k1gMnSc2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Československý	československý	k2eAgInSc1d1
Kompas	kompas	k1gInSc1
<g/>
,	,	kIx,
1944	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
Mařánek	Mařánek	k1gMnSc1
–	–	k?
Živé	živý	k2eAgInPc1d1
návraty	návrat	k1gInPc1
(	(	kIx(
<g/>
povídky	povídka	k1gFnPc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
R.	R.	kA
Vilímek	Vilímek	k1gMnSc1
<g/>
,	,	kIx,
1944	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zahraniční	zahraniční	k2eAgMnPc1d1
autoři	autor	k1gMnPc1
těsně	těsně	k6eAd1
či	či	k8xC
volně	volně	k6eAd1
spjati	spjat	k2eAgMnPc1d1
s	s	k7c7
novoklasicismem	novoklasicismus	k1gInSc7
(	(	kIx(
<g/>
příklady	příklad	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Paul	Paul	k1gMnSc1
Ernst	Ernst	k1gMnSc1
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Paul	Paul	k1gMnSc1
Valéry	valér	k1gInPc1
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Osip	Osip	k1gInSc1
Mandelštam	Mandelštam	k1gInSc1
(	(	kIx(
<g/>
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
T.	T.	kA
S.	S.	kA
Eliot	Eliot	k1gMnSc1
(	(	kIx(
<g/>
Anglie	Anglie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Současný	současný	k2eAgInSc1d1
neoklasicismus	neoklasicismus	k1gInSc1
</s>
<s>
Od	od	k7c2
konce	konec	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
s	s	k7c7
pojmem	pojem	k1gInSc7
neoklasicismus	neoklasicismus	k1gInSc1
nakládalo	nakládat	k5eAaImAgNnS
volně	volně	k6eAd1
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
ve	v	k7c6
smyslu	smysl	k1gInSc6
výše	výše	k1gFnSc2,k1gFnSc2wB
uvedené	uvedený	k2eAgFnSc2d1
tradiční	tradiční	k2eAgFnSc2d1
interpretace	interpretace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viz	vidět	k5eAaImRp2nS
v	v	k7c6
literatuře	literatura	k1gFnSc6
např.	např.	kA
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
v	v	k7c6
hudbě	hudba	k1gFnSc6
pojem	pojem	k1gInSc1
„	„	k?
<g/>
neoklasický	neoklasický	k2eAgInSc1d1
rock	rock	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Míní	mínit	k5eAaImIp3nS
se	s	k7c7
1	#num#	k4
<g/>
.	.	kIx.
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
V	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
vývojem	vývoj	k1gInSc7
pravopisu	pravopis	k1gInSc2
byly	být	k5eAaImAgFnP
v	v	k7c6
češtině	čeština	k1gFnSc6
používány	používat	k5eAaImNgFnP
i	i	k9
odchylné	odchylný	k2eAgFnPc1d1
pravopisné	pravopisný	k2eAgFnPc1d1
varianty	varianta	k1gFnPc1
slova	slovo	k1gNnSc2
jako	jako	k8xS,k8xC
neoklassicism	neoklassicisma	k1gFnPc2
či	či	k8xC
novoklassicism	novoklassicisma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Neoclassicism	Neoclassicismo	k1gNnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
,	,	kIx,
Néo-classicisme	Néo-classicismus	k1gInSc5
na	na	k7c6
francouzské	francouzský	k2eAgFnSc3d1
Wikipedii	Wikipedie	k1gFnSc3
a	a	k8xC
Klassizismus	Klassizismus	k1gInSc4
(	(	kIx(
<g/>
Literatur	literatura	k1gFnPc2
<g/>
)	)	kIx)
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Ottův	Ottův	k2eAgInSc4d1
slovník	slovník	k1gInSc4
naučný	naučný	k2eAgInSc4d1
nové	nový	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
nakladatelství	nakladatelství	k1gNnSc2
J.	J.	kA
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
1930	#num#	k4
<g/>
-	-	kIx~
<g/>
1934	#num#	k4
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Novoklasicismus	novoklasicismus	k1gInSc1
v	v	k7c6
literatuře	literatura	k1gFnSc6
<g/>
,	,	kIx,
Sv.	sv.	kA
7	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
638	#num#	k4
<g/>
↑	↑	k?
Renaissance	Renaissance	k1gFnSc2
ruské	ruský	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novia	Novium	k1gNnPc1
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1911	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
178	#num#	k4
<g/>
-	-	kIx~
<g/>
180	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
KOVÁČOVÁ	Kováčová	k1gFnSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rané	raný	k2eAgNnSc4d1
literární	literární	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
Josefa	Josef	k1gMnSc2
Čapka	Čapek	k1gMnSc2
v	v	k7c6
kontextu	kontext	k1gInSc6
moderního	moderní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Daniel	Daniel	k1gMnSc1
Vojtěch	Vojtěch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Novoklassicism	Novoklassicism	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1912	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Novoklassicism	Novoklassicism	k1gInSc1
II	II	kA
<g/>
..	..	k?
Národní	národní	k2eAgFnSc2d1
listy	lista	k1gFnSc2
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1912	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Neoklassicism	Neoklassicism	k1gInSc1
III	III	kA
<g/>
..	..	k?
Národní	národní	k2eAgFnSc2d1
listy	lista	k1gFnSc2
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1912	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Karel	Karel	k1gMnSc1
Sezima	Sezim	k1gMnSc2
<g/>
:	:	kIx,
Z	z	k7c2
nové	nový	k2eAgFnSc2d1
české	český	k2eAgFnSc2d1
belletrie	belletrie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lumír	Lumír	k1gMnSc1
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1915	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
114	#num#	k4
<g/>
-	-	kIx~
<g/>
116	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Josef	Josef	k1gMnSc1
Čapek	Čapek	k1gMnSc1
<g/>
:	:	kIx,
O	o	k7c6
tradici	tradice	k1gFnSc6
a	a	k8xC
tvoření	tvoření	k1gNnSc6
kollektivním	kollektivní	k2eAgNnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přítomnost	přítomnost	k1gFnSc1
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
18	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1924	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
570	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Miroslav	Miroslav	k1gMnSc1
Rutte	Rutt	k1gInSc5
<g/>
:	:	kIx,
Novoklassicismus	Novoklassicismus	k1gInSc1
a	a	k8xC
kubismus	kubismus	k1gInSc1
v	v	k7c6
mladé	mladý	k2eAgFnSc6d1
české	český	k2eAgFnSc6d1
próse	prósa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1921	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SBČ	SBČ	kA
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
Pěkná	pěkná	k1gFnSc1
<g/>
:	:	kIx,
Zářivé	zářivý	k2eAgFnSc2d1
hlubiny	hlubina	k1gFnSc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Josef	Josef	k1gMnSc1
Čapek	Čapek	k1gMnSc1
<g/>
:	:	kIx,
Živý	živý	k2eAgInSc1d1
plamen	plamen	k1gInSc1
<g/>
↑	↑	k?
Karel	Karel	k1gMnSc1
Čapek	Čapek	k1gMnSc1
<g/>
:	:	kIx,
Mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
polibky	polibek	k1gInPc7
<g/>
↑	↑	k?
František	František	k1gMnSc1
Khol	Khol	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1930	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Martin	Martin	k1gMnSc1
Reiner	Reiner	k1gMnSc1
<g/>
:	:	kIx,
Neoklasicismus	neoklasicismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Literární	literární	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
.	.	kIx.
36	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
a	a	k8xC
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VACULÍKOVÁ	Vaculíková	k1gFnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neoklasický	neoklasický	k2eAgInSc4d1
styl	styl	k1gInSc4
rocku	rock	k1gInSc2
na	na	k7c6
příkladu	příklad	k1gInSc6
tvorby	tvorba	k1gFnSc2
Y.	Y.	kA
Malmsteena	Malmsteen	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Magisterská	magisterský	k2eAgFnSc1d1
diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Palackého	Palackého	k2eAgMnPc2d1
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulat	fakulat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Jan	Jan	k1gMnSc1
Blüml	Blüml	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Rutte	Rutt	k1gInSc5
<g/>
:	:	kIx,
Novoklassicismus	Novoklassicismus	k1gInSc1
a	a	k8xC
kubismus	kubismus	k1gInSc1
v	v	k7c6
mladé	mladý	k2eAgFnSc6d1
české	český	k2eAgFnSc6d1
próse	prósa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1921	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
KOVÁČOVÁ	Kováčová	k1gFnSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rané	raný	k2eAgNnSc4d1
literární	literární	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
Josefa	Josef	k1gMnSc2
Čapka	Čapek	k1gMnSc2
v	v	k7c6
kontextu	kontext	k1gInSc6
moderního	moderní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Daniel	Daniel	k1gMnSc1
Vojtěch	Vojtěch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Ottův	Ottův	k2eAgInSc4d1
slovník	slovník	k1gInSc4
naučný	naučný	k2eAgInSc4d1
nové	nový	k2eAgNnSc4d1
doby	doba	k1gFnPc4
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Novoklasicismus	novoklasicismus	k1gInSc1
v	v	k7c6
literatuře	literatura	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
7	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
638	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Literární	literární	k2eAgFnPc1d1
epochy	epocha	k1gFnPc1
<g/>
,	,	kIx,
proudy	proud	k1gInPc1
a	a	k8xC
směry	směr	k1gInPc1
Starší	starý	k2eAgInPc1d2
literatura	literatura	k1gFnSc1
</s>
<s>
starověká	starověký	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
•	•	k?
středověká	středověký	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
•	•	k?
renesanční	renesanční	k2eAgNnSc1d1
<g/>
/	/	kIx~
<g/>
humanistická	humanistický	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
•	•	k?
manýristická	manýristický	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
barokní	barokní	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
•	•	k?
klasicismus	klasicismus	k1gInSc1
•	•	k?
osvícenská	osvícenský	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
•	•	k?
preromantismus	preromantismus	k1gInSc1
<g/>
/	/	kIx~
<g/>
sentimentalismus	sentimentalismus	k1gInSc1
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
romantismus	romantismus	k1gInSc1
•	•	k?
biedermeier	biedermeier	k1gInSc1
•	•	k?
realismus	realismus	k1gInSc1
•	•	k?
naturalismus	naturalismus	k1gInSc1
•	•	k?
novoromantismus	novoromantismus	k1gInSc1
Modernismus	modernismus	k1gInSc1
</s>
<s>
parnasismus	parnasismus	k1gInSc1
•	•	k?
impresionismus	impresionismus	k1gInSc1
•	•	k?
symbolismus	symbolismus	k1gInSc1
•	•	k?
dekadence	dekadence	k1gFnSc1
•	•	k?
civilismus	civilismus	k1gInSc1
•	•	k?
imagismus	imagismus	k1gInSc1
•	•	k?
imažinismus	imažinismus	k1gInSc1
•	•	k?
akméismus	akméismus	k1gInSc1
Avantgarda	avantgarda	k1gFnSc1
</s>
<s>
futurismus	futurismus	k1gInSc1
•	•	k?
expresionismus	expresionismus	k1gInSc1
•	•	k?
vitalismus	vitalismus	k1gInSc1
•	•	k?
dadaismus	dadaismus	k1gInSc1
•	•	k?
surrealismus	surrealismus	k1gInSc1
•	•	k?
poetismus	poetismus	k1gInSc1
Další	další	k2eAgInPc1d1
směry	směr	k1gInPc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
neoklasicismus	neoklasicismus	k1gInSc1
•	•	k?
ruralismus	ruralismus	k1gInSc1
•	•	k?
socialistický	socialistický	k2eAgInSc1d1
realismus	realismus	k1gInSc1
•	•	k?
existencialismus	existencialismus	k1gInSc1
•	•	k?
magický	magický	k2eAgInSc1d1
realismus	realismus	k1gInSc1
•	•	k?
postmodernismus	postmodernismus	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
</s>
