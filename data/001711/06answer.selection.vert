<s>
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
vznikli	vzniknout	k5eAaPmAgMnP	vzniknout
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
v	v	k7c6	v
Birminghamu	Birmingham	k1gInSc6	Birmingham
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Polka	Polka	k1gFnSc1	Polka
Tulk	Tulk	k1gInSc4	Tulk
Blues	blues	k1gInSc1	blues
Band	banda	k1gFnPc2	banda
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
zkráceno	zkrátit	k5eAaPmNgNnS	zkrátit
na	na	k7c4	na
Polka	Polka	k1gFnSc1	Polka
Tulk	Tulk	k1gMnSc1	Tulk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
jej	on	k3xPp3gMnSc4	on
změnili	změnit	k5eAaPmAgMnP	změnit
na	na	k7c4	na
Earth	Earth	k1gInSc4	Earth
<g/>
,	,	kIx,	,
hráli	hrát	k5eAaImAgMnP	hrát
styly	styl	k1gInPc4	styl
blues	blues	k1gFnSc2	blues
rock	rock	k1gInSc1	rock
a	a	k8xC	a
hard	hard	k6eAd1	hard
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
