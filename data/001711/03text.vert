<s>
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
jsou	být	k5eAaImIp3nP	být
anglická	anglický	k2eAgFnSc1d1	anglická
heavymetalová	heavymetalový	k2eAgFnSc1d1	heavymetalová
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
považovaná	považovaný	k2eAgFnSc1d1	považovaná
též	též	k9	též
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prakticky	prakticky	k6eAd1	prakticky
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
mnoho	mnoho	k4c4	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
stylů	styl	k1gInPc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
sestava	sestava	k1gFnSc1	sestava
<g/>
:	:	kIx,	:
Ozzy	Ozzy	k1gInPc1	Ozzy
Osbourne	Osbourn	k1gInSc5	Osbourn
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc4	zpěv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tony	Tony	k1gFnSc1	Tony
Iommi	Iom	k1gFnPc7	Iom
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Terrance	Terrance	k1gFnSc1	Terrance
"	"	kIx"	"
<g/>
Geezer	Geezer	k1gInSc1	Geezer
<g/>
"	"	kIx"	"
Butler	Butler	k1gInSc1	Butler
(	(	kIx(	(
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bill	Bill	k1gMnSc1	Bill
Ward	Ward	k1gMnSc1	Ward
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
dominantních	dominantní	k2eAgFnPc2d1	dominantní
a	a	k8xC	a
zakládajících	zakládající	k2eAgFnPc2d1	zakládající
kapel	kapela	k1gFnPc2	kapela
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
stále	stále	k6eAd1	stále
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
stanice	stanice	k1gFnSc2	stanice
VH1	VH1	k1gFnSc2	VH1
a	a	k8xC	a
programu	program	k1gInSc2	program
100	[number]	k4	100
Greatest	Greatest	k1gInSc1	Greatest
Artists	Artists	k1gInSc4	Artists
of	of	k?	of
Hard	Hard	k1gInSc1	Hard
Rock	rock	k1gInSc1	rock
se	se	k3xPyFc4	se
umístili	umístit	k5eAaPmAgMnP	umístit
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
mezi	mezi	k7c7	mezi
nejvýznamnějšími	významný	k2eAgMnPc7d3	nejvýznamnější
představiteli	představitel	k1gMnPc7	představitel
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
za	za	k7c7	za
skupinou	skupina	k1gFnSc7	skupina
Led	led	k1gInSc4	led
Zeppelin	Zeppelin	k2eAgInSc4d1	Zeppelin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
-	-	kIx~	-
2010	[number]	k4	2010
sestava	sestava	k1gFnSc1	sestava
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
-	-	kIx~	-
Tony	Tony	k1gFnSc1	Tony
Iommi	Iom	k1gFnPc7	Iom
<g/>
,	,	kIx,	,
Geezer	Geezer	k1gMnSc1	Geezer
Butler	Butler	k1gMnSc1	Butler
<g/>
,	,	kIx,	,
Ronnie	Ronnie	k1gFnSc1	Ronnie
James	James	k1gMnSc1	James
Dio	Dio	k1gMnSc1	Dio
a	a	k8xC	a
Vinny	vinen	k2eAgFnPc4d1	vinna
Appice	Appice	k1gFnPc4	Appice
-	-	kIx~	-
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Heaven	Heavna	k1gFnPc2	Heavna
and	and	k?	and
Hell	Hella	k1gFnPc2	Hella
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
alba	album	k1gNnSc2	album
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Black	Black	k6eAd1	Black
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
měli	mít	k5eAaImAgMnP	mít
vliv	vliv	k1gInSc4	vliv
i	i	k9	i
na	na	k7c4	na
hudbu	hudba	k1gFnSc4	hudba
grungeových	grungeův	k2eAgFnPc2d1	grungeův
kapel	kapela	k1gFnPc2	kapela
jako	jako	k8xS	jako
například	například	k6eAd1	například
Nirvana	Nirvan	k1gMnSc2	Nirvan
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
zásadně	zásadně	k6eAd1	zásadně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
grunge	grunge	k1gFnSc1	grunge
<g/>
)	)	kIx)	)
a	a	k8xC	a
Black	Black	k1gMnSc1	Black
Flag	flag	k1gInSc1	flag
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
My	my	k3xPp1nPc1	my
War	War	k1gFnPc1	War
ovlivněné	ovlivněný	k2eAgFnPc1d1	ovlivněná
Black	Black	k1gInSc4	Black
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
mělo	mít	k5eAaImAgNnS	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
zvuk	zvuk	k1gInSc4	zvuk
hardcore	hardcor	k1gInSc5	hardcor
a	a	k8xC	a
grunge	grunge	k1gNnPc2	grunge
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
vznikli	vzniknout	k5eAaPmAgMnP	vzniknout
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
v	v	k7c6	v
Birminghamu	Birmingham	k1gInSc6	Birmingham
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Polka	Polka	k1gFnSc1	Polka
Tulk	Tulk	k1gInSc4	Tulk
Blues	blues	k1gInSc1	blues
Band	banda	k1gFnPc2	banda
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
zkráceno	zkrátit	k5eAaPmNgNnS	zkrátit
na	na	k7c4	na
Polka	Polka	k1gFnSc1	Polka
Tulk	Tulk	k1gMnSc1	Tulk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
jej	on	k3xPp3gMnSc4	on
změnili	změnit	k5eAaPmAgMnP	změnit
na	na	k7c4	na
Earth	Earth	k1gInSc4	Earth
<g/>
,	,	kIx,	,
hráli	hrát	k5eAaImAgMnP	hrát
styly	styl	k1gInPc4	styl
blues	blues	k1gFnSc2	blues
rock	rock	k1gInSc1	rock
a	a	k8xC	a
hard	hard	k6eAd1	hard
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
měli	mít	k5eAaImAgMnP	mít
jedinečný	jedinečný	k2eAgInSc4d1	jedinečný
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyplýval	vyplývat	k5eAaImAgInS	vyplývat
z	z	k7c2	z
odlišných	odlišný	k2eAgInPc2d1	odlišný
hudebních	hudební	k2eAgInPc2d1	hudební
vzorů	vzor	k1gInPc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Iommi	Iom	k1gFnPc7	Iom
byl	být	k5eAaImAgInS	být
značně	značně	k6eAd1	značně
ovlivněný	ovlivněný	k2eAgInSc1d1	ovlivněný
Hankem	Hanek	k1gMnSc7	Hanek
Marvinem	Marvin	k1gMnSc7	Marvin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
heavy-kytaru	heavyytara	k1gFnSc4	heavy-kytara
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
The	The	k1gMnPc2	The
Shadows	Shadowsa	k1gFnPc2	Shadowsa
a	a	k8xC	a
také	také	k9	také
jazzovou	jazzový	k2eAgFnSc4d1	jazzová
kytaru	kytara	k1gFnSc4	kytara
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gInSc7	jeho
idolem	idol	k1gInSc7	idol
byl	být	k5eAaImAgInS	být
i	i	k9	i
Django	Django	k1gMnSc1	Django
Reinhardt	Reinhardt	k1gMnSc1	Reinhardt
<g/>
.	.	kIx.	.
</s>
<s>
Bill	Bill	k1gMnSc1	Bill
Ward	Ward	k1gMnSc1	Ward
také	také	k9	také
ukázal	ukázat	k5eAaPmAgMnS	ukázat
slabost	slabost	k1gFnSc4	slabost
pro	pro	k7c4	pro
jazz	jazz	k1gInSc4	jazz
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
bubeníky	bubeník	k1gMnPc4	bubeník
Buddyho	Buddy	k1gMnSc2	Buddy
Riche	Rich	k1gInSc2	Rich
a	a	k8xC	a
Gene	gen	k1gInSc5	gen
Krupu	krup	k1gInSc3	krup
<g/>
.	.	kIx.	.
</s>
<s>
Geezer	Geezer	k1gMnSc1	Geezer
Butler	Butler	k1gMnSc1	Butler
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
baskytaristu	baskytarista	k1gMnSc4	baskytarista
a	a	k8xC	a
zpěváka	zpěvák	k1gMnSc4	zpěvák
Jacka	Jacek	k1gMnSc4	Jacek
Bruceho	Bruce	k1gMnSc4	Bruce
z	z	k7c2	z
britské	britský	k2eAgFnSc2d1	britská
bluesové	bluesový	k2eAgFnSc2d1	bluesová
skupiny	skupina	k1gFnSc2	skupina
Cream	Cream	k1gInSc4	Cream
jako	jako	k8xC	jako
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
hudebním	hudební	k2eAgNnSc6d1	hudební
formování	formování	k1gNnSc6	formování
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
On	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
baskytarista	baskytarista	k1gMnSc1	baskytarista
<g/>
,	,	kIx,	,
jakého	jaký	k3yRgMnSc4	jaký
jsem	být	k5eAaImIp1nS	být
kdy	kdy	k6eAd1	kdy
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ohýbal	ohýbat	k5eAaImAgMnS	ohýbat
struny	struna	k1gFnSc2	struna
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
baskytaru	baskytara	k1gFnSc4	baskytara
jako	jako	k8xC	jako
na	na	k7c6	na
úplně	úplně	k6eAd1	úplně
nezávislém	závislý	k2eNgInSc6d1	nezávislý
hudebním	hudební	k2eAgInSc6d1	hudební
nástroji	nástroj	k1gInSc6	nástroj
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Skupina	skupina	k1gFnSc1	skupina
spojovala	spojovat	k5eAaImAgFnS	spojovat
hudební	hudební	k2eAgInPc4d1	hudební
styly	styl	k1gInPc4	styl
blues	blues	k1gNnSc2	blues
<g/>
,	,	kIx,	,
jazz	jazz	k1gInSc1	jazz
<g/>
,	,	kIx,	,
rock	rock	k1gInSc1	rock
a	a	k8xC	a
hrála	hrát	k5eAaImAgFnS	hrát
coververze	coververze	k1gFnSc1	coververze
skladeb	skladba	k1gFnPc2	skladba
inerpretů	inerpret	k1gInPc2	inerpret
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
<g/>
,	,	kIx,	,
Blue	Blue	k1gInSc4	Blue
Cheer	Chera	k1gFnPc2	Chera
a	a	k8xC	a
již	již	k6eAd1	již
zmínění	zmíněný	k2eAgMnPc1d1	zmíněný
Cream	Cream	k1gInSc4	Cream
<g/>
.	.	kIx.	.
</s>
<s>
Ozzy	Ozza	k1gFnPc1	Ozza
Osbourne	Osbourn	k1gInSc5	Osbourn
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
hluboce	hluboko	k6eAd1	hluboko
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
skupinou	skupina	k1gFnSc7	skupina
The	The	k1gFnPc2	The
Beatles	Beatles	k1gFnPc2	Beatles
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
nejoblíbenějším	oblíbený	k2eAgNnSc7d3	nejoblíbenější
albem	album	k1gNnSc7	album
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
je	být	k5eAaImIp3nS	být
Revolver	revolver	k1gInSc1	revolver
<g/>
.	.	kIx.	.
</s>
<s>
Earth	Earth	k1gInSc1	Earth
se	se	k3xPyFc4	se
posunuli	posunout	k5eAaPmAgMnP	posunout
do	do	k7c2	do
temnějšího	temný	k2eAgNnSc2d2	temnější
směřování	směřování	k1gNnSc2	směřování
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
baskytarista	baskytarista	k1gMnSc1	baskytarista
Geezer	Geezer	k1gMnSc1	Geezer
Butler	Butler	k1gMnSc1	Butler
<g/>
,	,	kIx,	,
fanoušek	fanoušek	k1gMnSc1	fanoušek
spisovatele	spisovatel	k1gMnSc2	spisovatel
Dennise	Dennise	k1gFnSc2	Dennise
Wheatleyho	Wheatley	k1gMnSc2	Wheatley
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
románů	román	k1gInPc2	román
s	s	k7c7	s
tematikou	tematika	k1gFnSc7	tematika
černé	černý	k2eAgFnSc2d1	černá
magie	magie	k1gFnSc2	magie
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaBmAgMnS	napsat
skladbu	skladba	k1gFnSc4	skladba
s	s	k7c7	s
okultním	okultní	k2eAgNnSc7d1	okultní
tématem	téma	k1gNnSc7	téma
s	s	k7c7	s
názvem	název	k1gInSc7	název
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
(	(	kIx(	(
<g/>
skladba	skladba	k1gFnSc1	skladba
byla	být	k5eAaImAgFnS	být
očividně	očividně	k6eAd1	očividně
inspirovaná	inspirovaný	k2eAgNnPc4d1	inspirované
filmem	film	k1gInSc7	film
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
režiséra	režisér	k1gMnSc2	režisér
Maria	Mario	k1gMnSc2	Mario
Bava	Bavus	k1gMnSc2	Bavus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
filmu	film	k1gInSc6	film
o	o	k7c4	o
spojení	spojení	k1gNnSc4	spojení
skupiny	skupina	k1gFnSc2	skupina
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
skladba	skladba	k1gFnSc1	skladba
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
Butlerova	Butlerův	k2eAgInSc2d1	Butlerův
zážitku	zážitek	k1gInSc2	zážitek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jednou	jeden	k4xCgFnSc7	jeden
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
viděl	vidět	k5eAaImAgInS	vidět
černý	černý	k2eAgInSc1d1	černý
přelud	přelud	k1gInSc1	přelud
s	s	k7c7	s
kapucí	kapuce	k1gFnSc7	kapuce
na	na	k7c6	na
konci	konec	k1gInSc6	konec
své	svůj	k3xOyFgFnSc2	svůj
postele	postel	k1gFnSc2	postel
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgMnS	dát
Ozzy	Ozza	k1gFnPc4	Ozza
okultní	okultní	k2eAgFnSc4d1	okultní
knihu	kniha	k1gFnSc4	kniha
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Earth	Eartha	k1gFnPc2	Eartha
mátlo	mást	k5eAaImAgNnS	mást
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
už	už	k9	už
jedna	jeden	k4xCgFnSc1	jeden
taková	takový	k3xDgFnSc1	takový
kapela	kapela	k1gFnSc1	kapela
hrála	hrát	k5eAaImAgFnS	hrát
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
se	se	k3xPyFc4	se
přejmenovali	přejmenovat	k5eAaPmAgMnP	přejmenovat
na	na	k7c4	na
Black	Black	k1gInSc4	Black
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
objevila	objevit	k5eAaPmAgFnS	objevit
svůj	svůj	k3xOyFgInSc4	svůj
konečný	konečný	k2eAgInSc4d1	konečný
zvuk	zvuk	k1gInSc4	zvuk
a	a	k8xC	a
styl	styl	k1gInSc4	styl
náhodou	náhodou	k6eAd1	náhodou
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
měli	mít	k5eAaImAgMnP	mít
zkoušku	zkouška	k1gFnSc4	zkouška
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
naproti	naproti	k7c3	naproti
kinu	kino	k1gNnSc3	kino
promítajícímu	promítající	k2eAgInSc3d1	promítající
horory	horor	k1gInPc4	horor
<g/>
,	,	kIx,	,
Ozzy	Ozzy	k1gInPc4	Ozzy
si	se	k3xPyFc3	se
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
Tony	Tony	k1gFnSc1	Tony
Iommi	Iom	k1gFnPc7	Iom
říkal	říkat	k5eAaImAgMnS	říkat
ostatním	ostatní	k2eAgMnPc3d1	ostatní
členům	člen	k1gMnPc3	člen
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
divné	divný	k2eAgNnSc1d1	divné
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidi	člověk	k1gMnPc4	člověk
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
viděli	vidět	k5eAaImAgMnP	vidět
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	on	k3xPp3gNnSc4	on
vyděsí	vyděsit	k5eAaPmIp3nS	vyděsit
až	až	k9	až
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Skupina	skupina	k1gFnSc1	skupina
začala	začít	k5eAaPmAgFnS	začít
záměrně	záměrně	k6eAd1	záměrně
psát	psát	k5eAaImF	psát
temné	temný	k2eAgInPc4d1	temný
a	a	k8xC	a
hrozivé	hrozivý	k2eAgInPc4d1	hrozivý
texty	text	k1gInPc4	text
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
o	o	k7c4	o
hudební	hudební	k2eAgFnSc4d1	hudební
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
hororové	hororový	k2eAgInPc4d1	hororový
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
jejich	jejich	k3xOp3gInSc2	jejich
nového	nový	k2eAgInSc2d1	nový
temného	temný	k2eAgInSc2d1	temný
zvuku	zvuk	k1gInSc2	zvuk
s	s	k7c7	s
chováním	chování	k1gNnSc7	chování
Ozzyho	Ozzy	k1gMnSc2	Ozzy
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
zabodovala	zabodovat	k5eAaPmAgFnS	zabodovat
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
prvním	první	k4xOgNnSc7	první
albem	album	k1gNnSc7	album
s	s	k7c7	s
eponymním	eponymní	k2eAgInSc7d1	eponymní
názvem	název	k1gInSc7	název
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
vydala	vydat	k5eAaPmAgFnS	vydat
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Podepsali	podepsat	k5eAaPmAgMnP	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Warner	Warner	k1gMnSc1	Warner
Bros	Bros	k1gInSc1	Bros
<g/>
.	.	kIx.	.
pro	pro	k7c4	pro
trh	trh	k1gInSc4	trh
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Vertigo	Vertigo	k6eAd1	Vertigo
Records	Records	k1gInSc1	Records
měl	mít	k5eAaImAgInS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
trh	trh	k1gInSc1	trh
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgFnPc4d1	ostatní
dostupné	dostupný	k2eAgFnPc4d1	dostupná
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
následující	následující	k2eAgFnSc1d1	následující
album	album	k1gNnSc4	album
Paranoid	Paranoid	k1gInSc1	Paranoid
(	(	kIx(	(
<g/>
také	také	k9	také
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
jim	on	k3xPp3gFnPc3	on
obstaralo	obstarat	k5eAaPmAgNnS	obstarat
ještě	ještě	k9	ještě
větší	veliký	k2eAgFnSc4d2	veliký
pozornost	pozornost	k1gFnSc4	pozornost
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
War	War	k1gMnPc2	War
Pigs	Pigsa	k1gFnPc2	Pigsa
byla	být	k5eAaImAgFnS	být
napsaná	napsaný	k2eAgFnSc1d1	napsaná
jako	jako	k9	jako
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
a	a	k8xC	a
původně	původně	k6eAd1	původně
ji	on	k3xPp3gFnSc4	on
plánovali	plánovat	k5eAaImAgMnP	plánovat
zařadit	zařadit	k5eAaPmF	zařadit
jako	jako	k9	jako
titulní	titulní	k2eAgInSc4d1	titulní
singl	singl	k1gInSc4	singl
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
nahrála	nahrát	k5eAaPmAgFnS	nahrát
skladbu	skladba	k1gFnSc4	skladba
Paranoid	Paranoid	k1gInSc1	Paranoid
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prodloužila	prodloužit	k5eAaPmAgFnS	prodloužit
stopáž	stopáž	k1gFnSc1	stopáž
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Paranoid	Paranoid	k1gInSc1	Paranoid
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
titulním	titulní	k2eAgInSc7d1	titulní
singlem	singl	k1gInSc7	singl
alba	album	k1gNnSc2	album
a	a	k8xC	a
také	také	k9	také
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
jejich	jejich	k3xOp3gInSc4	jejich
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
výrazně	výrazně	k6eAd1	výrazně
zabodoval	zabodovat	k5eAaPmAgMnS	zabodovat
v	v	k7c6	v
rádiích	rádio	k1gNnPc6	rádio
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
skladeb	skladba	k1gFnPc2	skladba
(	(	kIx(	(
<g/>
originálů	originál	k1gInPc2	originál
a	a	k8xC	a
coververzí	coververze	k1gFnPc2	coververze
<g/>
)	)	kIx)	)
z	z	k7c2	z
obou	dva	k4xCgNnPc2	dva
alb	album	k1gNnPc2	album
dával	dávat	k5eAaImAgInS	dávat
jemně	jemně	k6eAd1	jemně
ironicky	ironicky	k6eAd1	ironicky
najevo	najevo	k6eAd1	najevo
zájem	zájem	k1gInSc4	zájem
skupiny	skupina	k1gFnSc2	skupina
o	o	k7c4	o
okultismus	okultismus	k1gInSc4	okultismus
a	a	k8xC	a
černou	černý	k2eAgFnSc4d1	černá
magii	magie	k1gFnSc4	magie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
rozhodující	rozhodující	k2eAgInSc1d1	rozhodující
krok	krok	k1gInSc1	krok
v	v	k7c6	v
určení	určení	k1gNnSc6	určení
jejich	jejich	k3xOp3gFnPc2	jejich
"	"	kIx"	"
<g/>
temných	temný	k2eAgFnPc2d1	temná
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
hlubokých	hluboký	k2eAgInPc6d1	hluboký
<g/>
"	"	kIx"	"
heavymetalových	heavymetalový	k2eAgInPc6d1	heavymetalový
textech	text	k1gInPc6	text
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
byli	být	k5eAaImAgMnP	být
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
s	s	k7c7	s
lyrickým	lyrický	k2eAgInSc7d1	lyrický
obsahem	obsah	k1gInSc7	obsah
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
se	se	k3xPyFc4	se
nevěnovali	věnovat	k5eNaImAgMnP	věnovat
jiným	jiný	k2eAgNnPc3d1	jiné
tématům	téma	k1gNnPc3	téma
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
<g/>
,	,	kIx,	,
The	The	k1gFnPc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
poukázali	poukázat	k5eAaPmAgMnP	poukázat
na	na	k7c4	na
magii	magie	k1gFnSc4	magie
nebo	nebo	k8xC	nebo
okultismus	okultismus	k1gInSc4	okultismus
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
textech	text	k1gInPc6	text
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k4c4	málo
kapel	kapela	k1gFnPc2	kapela
(	(	kIx(	(
<g/>
i	i	k8xC	i
současných	současný	k2eAgInPc2d1	současný
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
textům	text	k1gInPc3	text
Black	Black	k1gInSc4	Black
Sabbath	Sabbath	k1gInSc4	Sabbath
(	(	kIx(	(
<g/>
většinu	většina	k1gFnSc4	většina
napsal	napsat	k5eAaBmAgMnS	napsat
Geezer	Geezer	k1gMnSc1	Geezer
Butler	Butler	k1gMnSc1	Butler
<g/>
)	)	kIx)	)
s	s	k7c7	s
jejich	jejich	k3xOp3gInSc7	jejich
přímým	přímý	k2eAgInSc7d1	přímý
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
inovací	inovace	k1gFnSc7	inovace
byl	být	k5eAaImAgInS	být
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
efekt	efekt	k1gInSc1	efekt
nehody	nehoda	k1gFnSc2	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Tony	Tony	k1gMnSc1	Tony
Iommi	Iom	k1gFnPc7	Iom
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
konečky	koneček	k1gInPc4	koneček
dvou	dva	k4xCgInPc2	dva
prstů	prst	k1gInPc2	prst
na	na	k7c6	na
ruce	ruka	k1gFnSc6	ruka
<g/>
,	,	kIx,	,
když	když	k8xS	když
pracoval	pracovat	k5eAaImAgMnS	pracovat
s	s	k7c7	s
plechem	plech	k1gInSc7	plech
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
si	se	k3xPyFc3	se
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
protézu	protéza	k1gFnSc4	protéza
z	z	k7c2	z
roztavené	roztavený	k2eAgFnSc2d1	roztavená
plastové	plastový	k2eAgFnSc2d1	plastová
lahve	lahev	k1gFnSc2	lahev
od	od	k7c2	od
čisticího	čisticí	k2eAgInSc2d1	čisticí
prostředku	prostředek	k1gInSc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Poraněné	poraněný	k2eAgInPc1d1	poraněný
prsty	prst	k1gInPc1	prst
byly	být	k5eAaImAgInP	být
pochopitelně	pochopitelně	k6eAd1	pochopitelně
citlivé	citlivý	k2eAgInPc1d1	citlivý
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
Iommi	Iom	k1gFnPc7	Iom
přeladil	přeladit	k5eAaPmAgInS	přeladit
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
kytaře	kytara	k1gFnSc6	kytara
Gibson	Gibsona	k1gFnPc2	Gibsona
standardní	standardní	k2eAgInSc1d1	standardní
tón	tón	k1gInSc1	tón
E	E	kA	E
na	na	k7c6	na
Cis	cis	k1gNnSc6	cis
<g/>
.	.	kIx.	.
</s>
<s>
Zmenšené	zmenšený	k2eAgNnSc1d1	zmenšené
napětí	napětí	k1gNnSc1	napětí
strun	struna	k1gFnPc2	struna
mu	on	k3xPp3gMnSc3	on
dovolovalo	dovolovat	k5eAaImAgNnS	dovolovat
hrát	hrát	k5eAaImF	hrát
s	s	k7c7	s
menší	malý	k2eAgFnSc7d2	menší
bolestí	bolest	k1gFnSc7	bolest
a	a	k8xC	a
zátěží	zátěž	k1gFnSc7	zátěž
na	na	k7c4	na
prsty	prst	k1gInPc4	prst
<g/>
.	.	kIx.	.
</s>
<s>
Geezer	Geezer	k1gInSc1	Geezer
svou	svůj	k3xOyFgFnSc4	svůj
baskytaru	baskytara	k1gFnSc4	baskytara
také	také	k9	také
vyladil	vyladit	k5eAaPmAgMnS	vyladit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
přizpůsobil	přizpůsobit	k5eAaPmAgMnS	přizpůsobit
Tonymu	Tony	k1gMnSc3	Tony
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2	nižší
poloha	poloha	k1gFnSc1	poloha
dala	dát	k5eAaPmAgFnS	dát
hudbě	hudba	k1gFnSc3	hudba
hlubší	hluboký	k2eAgFnSc3d2	hlubší
či	či	k8xC	či
smysluplnější	smysluplný	k2eAgInSc1d2	smysluplnější
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
tak	tak	k6eAd1	tak
ladila	ladit	k5eAaImAgFnS	ladit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
texty	text	k1gInPc7	text
<g/>
.	.	kIx.	.
</s>
<s>
Black	Black	k6eAd1	Black
Sabbath	Sabbath	k1gInSc1	Sabbath
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
vydali	vydat	k5eAaPmAgMnP	vydat
své	svůj	k3xOyFgNnSc4	svůj
další	další	k2eAgNnSc4d1	další
album	album	k1gNnSc4	album
Master	master	k1gMnSc1	master
of	of	k?	of
Reality	realita	k1gFnSc2	realita
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
množství	množství	k1gNnSc4	množství
významných	významný	k2eAgFnPc2d1	významná
akustických	akustický	k2eAgFnPc2d1	akustická
věcí	věc	k1gFnPc2	věc
(	(	kIx(	(
<g/>
Solitude	Solitud	k1gInSc5	Solitud
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Tonyho	Tony	k1gMnSc4	Tony
flétnové	flétnový	k2eAgNnSc1d1	flétnové
sólo	sólo	k1gNnSc1	sólo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
přehlíženo	přehlížen	k2eAgNnSc1d1	přehlíženo
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
přechod	přechod	k1gInSc4	přechod
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
Black	Blacka	k1gFnPc2	Blacka
Sabbathu	Sabbath	k1gInSc2	Sabbath
<g/>
.	.	kIx.	.
</s>
<s>
Přidali	přidat	k5eAaPmAgMnP	přidat
mnoho	mnoho	k4c4	mnoho
měnivých	měnivý	k2eAgInPc2d1	měnivý
hudebních	hudební	k2eAgInPc2d1	hudební
prvků	prvek	k1gInPc2	prvek
do	do	k7c2	do
vydání	vydání	k1gNnSc2	vydání
svého	své	k1gNnSc2	své
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
alba	album	k1gNnSc2	album
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
4	[number]	k4	4
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
baladu	balada	k1gFnSc4	balada
Changes	Changesa	k1gFnPc2	Changesa
(	(	kIx(	(
<g/>
zní	znět	k5eAaImIp3nS	znět
zde	zde	k6eAd1	zde
jen	jen	k9	jen
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc1	klavír
a	a	k8xC	a
syntezátor	syntezátor	k1gInSc1	syntezátor
<g/>
)	)	kIx)	)
a	a	k8xC	a
hardrockové	hardrockový	k2eAgFnSc2d1	hardrocková
hymny	hymna	k1gFnSc2	hymna
jako	jako	k8xC	jako
Supernaut	Supernaut	k1gMnSc1	Supernaut
a	a	k8xC	a
Snowblind	Snowblind	k1gMnSc1	Snowblind
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
smyčcové	smyčcový	k2eAgInPc4d1	smyčcový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
"	"	kIx"	"
<g/>
nejzralejším	zralý	k2eAgMnSc7d3	nejzralejší
<g/>
"	"	kIx"	"
albem	album	k1gNnSc7	album
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
byli	být	k5eAaImAgMnP	být
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgFnPc2d3	nejpopulárnější
heavymetalových	heavymetalový	k2eAgFnPc2d1	heavymetalová
skupin	skupina	k1gFnPc2	skupina
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc7d1	hlavní
koncertní	koncertní	k2eAgFnSc7d1	koncertní
atrakcí	atrakce	k1gFnSc7	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dalším	další	k2eAgNnSc6d1	další
albu	album	k1gNnSc6	album
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
Bloody	Blooda	k1gFnSc2	Blooda
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
s	s	k7c7	s
klávesistou	klávesista	k1gMnSc7	klávesista
kapely	kapela	k1gFnSc2	kapela
Yes	Yes	k1gMnSc7	Yes
Rickem	Ricek	k1gMnSc7	Ricek
Wakemanem	Wakeman	k1gMnSc7	Wakeman
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
některých	některý	k3yIgFnPc2	některý
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
skladeb	skladba	k1gFnPc2	skladba
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
titulní	titulní	k2eAgFnSc7d1	titulní
skladbou	skladba	k1gFnSc7	skladba
album	album	k1gNnSc1	album
také	také	k6eAd1	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
spacerockovou	spacerockový	k2eAgFnSc4d1	spacerockový
skladbu	skladba	k1gFnSc4	skladba
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Spiral	Spiral	k1gFnSc4	Spiral
Architect	Architecta	k1gFnPc2	Architecta
a	a	k8xC	a
A	a	k9	a
National	National	k1gFnSc4	National
Acrobat	Acrobat	k1gFnSc2	Acrobat
inspirovanou	inspirovaný	k2eAgFnSc4d1	inspirovaná
prog	prog	k1gInSc1	prog
rockem	rock	k1gInSc7	rock
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byli	být	k5eAaImAgMnP	být
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
závislí	závislý	k2eAgMnPc1d1	závislý
na	na	k7c6	na
drogách	droga	k1gFnPc6	droga
<g/>
,	,	kIx,	,
Ozzy	Ozza	k1gFnPc1	Ozza
a	a	k8xC	a
Bill	Bill	k1gMnSc1	Bill
Ward	Ward	k1gMnSc1	Ward
údajně	údajně	k6eAd1	údajně
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
po	po	k7c4	po
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
brali	brát	k5eAaImAgMnP	brát
LSD	LSD	kA	LSD
<g/>
.	.	kIx.	.
</s>
<s>
Ozzy	Ozza	k1gFnPc4	Ozza
byl	být	k5eAaImAgInS	být
zapletený	zapletený	k2eAgInSc1d1	zapletený
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohé	k1gNnSc1	mnohé
ze	z	k7c2	z
skladeb	skladba	k1gFnPc2	skladba
o	o	k7c6	o
drogách	droga	k1gFnPc6	droga
také	také	k9	také
hovoří	hovořit	k5eAaImIp3nS	hovořit
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
otevřeně	otevřeně	k6eAd1	otevřeně
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nepřímo	přímo	k6eNd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
sužovaly	sužovat	k5eAaImAgInP	sužovat
hlavně	hlavně	k9	hlavně
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
managementem	management	k1gInSc7	management
(	(	kIx(	(
<g/>
kapelu	kapela	k1gFnSc4	kapela
řídil	řídit	k5eAaImAgMnS	řídit
Ozzyho	Ozzy	k1gMnSc4	Ozzy
budoucí	budoucí	k2eAgMnSc1d1	budoucí
tchán	tchán	k1gMnSc1	tchán
Don	dona	k1gFnPc2	dona
Arden	Ardeny	k1gFnPc2	Ardeny
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
s	s	k7c7	s
managementem	management	k1gInSc7	management
a	a	k8xC	a
pozdější	pozdní	k2eAgFnSc1d2	pozdější
změna	změna	k1gFnSc1	změna
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
(	(	kIx(	(
<g/>
Vertigo	Vertigo	k6eAd1	Vertigo
vyměnili	vyměnit	k5eAaPmAgMnP	vyměnit
za	za	k7c4	za
WWA	WWA	kA	WWA
<g/>
)	)	kIx)	)
narušily	narušit	k5eAaPmAgFnP	narušit
vydání	vydání	k1gNnSc4	vydání
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tyto	tento	k3xDgFnPc4	tento
těžkosti	těžkost	k1gFnPc4	těžkost
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc4	album
Sabotage	Sabotag	k1gFnSc2	Sabotag
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
a	a	k8xC	a
setkalo	setkat	k5eAaPmAgNnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
drogové	drogový	k2eAgInPc1d1	drogový
problémy	problém	k1gInPc1	problém
<g/>
,	,	kIx,	,
pokračující	pokračující	k2eAgInPc1d1	pokračující
pokusy	pokus	k1gInPc1	pokus
v	v	k7c6	v
hudebním	hudební	k2eAgInSc6d1	hudební
stylu	styl	k1gInSc6	styl
(	(	kIx(	(
<g/>
gregoriánské	gregoriánský	k2eAgInPc4d1	gregoriánský
a	a	k8xC	a
mnišské	mnišský	k2eAgInPc4d1	mnišský
chorály	chorál	k1gInPc4	chorál
zdůrazněné	zdůrazněný	k2eAgInPc4d1	zdůrazněný
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
Supertzar	Supertzara	k1gFnPc2	Supertzara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc4	změna
na	na	k7c6	na
hardrockové	hardrockový	k2eAgFnSc6d1	hardrocková
scéně	scéna	k1gFnSc6	scéna
a	a	k8xC	a
vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
problémy	problém	k1gInPc1	problém
narušily	narušit	k5eAaPmAgInP	narušit
stabilitu	stabilita	k1gFnSc4	stabilita
a	a	k8xC	a
tvorbu	tvorba	k1gFnSc4	tvorba
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Technical	Technical	k1gFnSc2	Technical
Ecstasy	Ecstasa	k1gFnSc2	Ecstasa
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
komerčním	komerční	k2eAgInSc7d1	komerční
neúspěchem	neúspěch	k1gInSc7	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
na	na	k7c6	na
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
orchestrů	orchestr	k1gInPc2	orchestr
<g/>
,	,	kIx,	,
syntenzátorů	syntenzátor	k1gInPc2	syntenzátor
a	a	k8xC	a
zpěvu	zpěv	k1gInSc2	zpěv
Billa	Bill	k1gMnSc2	Bill
Warda	Ward	k1gMnSc2	Ward
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Ozzyho	Ozzy	k1gMnSc4	Ozzy
zastoupil	zastoupit	k5eAaPmAgMnS	zastoupit
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
náhlém	náhlý	k2eAgInSc6d1	náhlý
odchodu	odchod	k1gInSc6	odchod
uprostřed	uprostřed	k7c2	uprostřed
nahrávání	nahrávání	k1gNnSc2	nahrávání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
turné	turné	k1gNnSc6	turné
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
se	se	k3xPyFc4	se
Ozzy	Ozza	k1gFnSc2	Ozza
přestal	přestat	k5eAaPmAgInS	přestat
objevovat	objevovat	k5eAaImF	objevovat
na	na	k7c6	na
zkouškách	zkouška	k1gFnPc6	zkouška
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
zbývající	zbývající	k2eAgMnPc1d1	zbývající
členové	člen	k1gMnPc1	člen
nahráli	nahrát	k5eAaPmAgMnP	nahrát
nějakou	nějaký	k3yIgFnSc4	nějaký
tu	tu	k6eAd1	tu
hudbu	hudba	k1gFnSc4	hudba
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
a	a	k8xC	a
bývalým	bývalý	k2eAgMnSc7d1	bývalý
členem	člen	k1gMnSc7	člen
Fleetwood	Fleetwooda	k1gFnPc2	Fleetwooda
Mac	Mac	kA	Mac
Davem	Dav	k1gInSc7	Dav
Walkerem	Walkero	k1gNnSc7	Walkero
<g/>
.	.	kIx.	.
</s>
<s>
Ozzy	Ozz	k1gMnPc4	Ozz
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
před	před	k7c7	před
nahráním	nahrání	k1gNnSc7	nahrání
alba	album	k1gNnSc2	album
Never	Never	k1gInSc1	Never
Say	Say	k1gFnSc4	Say
Die	Die	k1gFnPc1	Die
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
roku	rok	k1gInSc3	rok
1978	[number]	k4	1978
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
určitě	určitě	k6eAd1	určitě
albem	album	k1gNnSc7	album
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
skupina	skupina	k1gFnSc1	skupina
nejvíc	hodně	k6eAd3	hodně
experimentovala	experimentovat	k5eAaImAgFnS	experimentovat
a	a	k8xC	a
použila	použít	k5eAaPmAgFnS	použít
přitom	přitom	k6eAd1	přitom
prvky	prvek	k1gInPc7	prvek
mnoha	mnoho	k4c2	mnoho
žánrů	žánr	k1gInPc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
předešlé	předešlý	k2eAgNnSc1d1	předešlé
album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
mizerně	mizerně	k6eAd1	mizerně
prodávalo	prodávat	k5eAaImAgNnS	prodávat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
album	album	k1gNnSc1	album
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Ronniem	Ronnium	k1gNnSc7	Ronnium
Jamesem	James	k1gMnSc7	James
Diem	Diem	k1gMnSc1	Diem
<g/>
)	)	kIx)	)
Heaven	Heavna	k1gFnPc2	Heavna
and	and	k?	and
Hell	Hella	k1gFnPc2	Hella
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
skupině	skupina	k1gFnSc3	skupina
navrátit	navrátit	k5eAaPmF	navrátit
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
nejvyššího	vysoký	k2eAgMnSc4d3	nejvyšší
umístění	umístění	k1gNnSc4	umístění
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
od	od	k7c2	od
alba	album	k1gNnSc2	album
Sabotage	Sabotag	k1gFnSc2	Sabotag
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Dio	Dio	k1gMnSc1	Dio
stal	stát	k5eAaPmAgMnS	stát
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
díky	dík	k1gInPc7	dík
znamení	znamení	k1gNnSc1	znamení
"	"	kIx"	"
<g/>
ďáblovy	ďáblův	k2eAgInPc1d1	ďáblův
rohy	roh	k1gInPc1	roh
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vztyčený	vztyčený	k2eAgInSc1d1	vztyčený
ukazovák	ukazovák	k1gInSc1	ukazovák
a	a	k8xC	a
malíček	malíček	k1gInSc1	malíček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
symbolem	symbol	k1gInSc7	symbol
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
klávesista	klávesista	k1gMnSc1	klávesista
Geoff	Geoff	k1gMnSc1	Geoff
Nicholls	Nicholls	k1gInSc1	Nicholls
(	(	kIx(	(
<g/>
Nicholls	Nicholls	k1gInSc1	Nicholls
nebyl	být	k5eNaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
oficiálního	oficiální	k2eAgMnSc4d1	oficiální
člena	člen	k1gMnSc4	člen
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
často	často	k6eAd1	často
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
nucen	nutit	k5eAaImNgMnS	nutit
hrát	hrát	k5eAaImF	hrát
ze	z	k7c2	z
zákulisí	zákulisí	k1gNnSc2	zákulisí
kvůli	kvůli	k7c3	kvůli
údajným	údajný	k2eAgInPc3d1	údajný
estetickým	estetický	k2eAgInPc3d1	estetický
záměrům	záměr	k1gInPc3	záměr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
napsání	napsání	k1gNnSc6	napsání
mnoha	mnoho	k4c2	mnoho
skladeb	skladba	k1gFnPc2	skladba
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
s	s	k7c7	s
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
i	i	k9	i
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
následujících	následující	k2eAgFnPc6d1	následující
sestavách	sestava	k1gFnPc6	sestava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
turné	turné	k1gNnSc2	turné
bubeník	bubeník	k1gMnSc1	bubeník
Bill	Bill	k1gMnSc1	Bill
Ward	Ward	k1gMnSc1	Ward
skupinu	skupina	k1gFnSc4	skupina
opustil	opustit	k5eAaPmAgMnS	opustit
kvůli	kvůli	k7c3	kvůli
osobním	osobní	k2eAgInPc3d1	osobní
důvodům	důvod	k1gInPc3	důvod
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
jeho	jeho	k3xOp3gInSc7	jeho
rodiče	rodič	k1gMnPc1	rodič
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
během	během	k7c2	během
krátkého	krátké	k1gNnSc2	krátké
časového	časový	k2eAgNnSc2d1	časové
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
Ward	Ward	k1gMnSc1	Ward
zápasil	zápasit	k5eAaImAgMnS	zápasit
s	s	k7c7	s
alkoholem	alkohol	k1gInSc7	alkohol
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
závislostmi	závislost	k1gFnPc7	závislost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
bubeník	bubeník	k1gMnSc1	bubeník
Vinny	vinen	k2eAgInPc4d1	vinen
Appice	Appice	k1gInPc4	Appice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokončil	dokončit	k5eAaPmAgInS	dokončit
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
následujícím	následující	k2eAgNnSc6d1	následující
albu	album	k1gNnSc6	album
Mob	Mob	k1gMnSc1	Mob
Rules	Rules	k1gMnSc1	Rules
<g/>
;	;	kIx,	;
titulní	titulní	k2eAgInSc1d1	titulní
singl	singl	k1gInSc1	singl
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Heavy	Heava	k1gFnSc2	Heava
Metal	metat	k5eAaImAgInS	metat
<g/>
.	.	kIx.	.
</s>
<s>
Neupravené	upravený	k2eNgNnSc1d1	neupravené
vydání	vydání	k1gNnSc1	vydání
koncertní	koncertní	k2eAgFnSc2d1	koncertní
nahrávky	nahrávka	k1gFnSc2	nahrávka
Live	Liv	k1gFnSc2	Liv
at	at	k?	at
Last	Last	k1gInSc1	Last
(	(	kIx(	(
<g/>
pořízené	pořízený	k2eAgNnSc1d1	pořízené
ještě	ještě	k9	ještě
s	s	k7c7	s
Ozzym	Ozzymum	k1gNnPc2	Ozzymum
během	během	k7c2	během
turné	turné	k1gNnSc2	turné
1972	[number]	k4	1972
Volume	volum	k1gInSc5	volum
4	[number]	k4	4
Tour	Toura	k1gFnPc2	Toura
<g/>
)	)	kIx)	)
podnítilo	podnítit	k5eAaPmAgNnS	podnítit
skupinu	skupina	k1gFnSc4	skupina
kvalitně	kvalitně	k6eAd1	kvalitně
nahrát	nahrát	k5eAaBmF	nahrát
živák	živák	k1gInSc4	živák
z	z	k7c2	z
turné	turné	k1gNnSc2	turné
Mob	Mob	k1gMnSc1	Mob
Rules	Rules	k1gMnSc1	Rules
s	s	k7c7	s
názvem	název	k1gInSc7	název
Live	Live	k1gNnSc2	Live
Evil	Evila	k1gFnPc2	Evila
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
během	během	k7c2	během
jeho	jeho	k3xOp3gNnSc2	jeho
dolaďování	dolaďování	k1gNnSc2	dolaďování
se	se	k3xPyFc4	se
projevily	projevit	k5eAaPmAgInP	projevit
vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
problémy	problém	k1gInPc1	problém
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
odchod	odchod	k1gInSc4	odchod
Dia	Dia	k1gFnSc2	Dia
a	a	k8xC	a
Appice	Appice	k1gFnSc2	Appice
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
založili	založit	k5eAaPmAgMnP	založit
skupinu	skupina	k1gFnSc4	skupina
Dio	Dio	k1gFnSc2	Dio
<g/>
.	.	kIx.	.
</s>
<s>
Bill	Bill	k1gMnSc1	Bill
Ward	Ward	k1gMnSc1	Ward
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
bicím	bicí	k2eAgMnPc3d1	bicí
a	a	k8xC	a
novým	nový	k2eAgMnSc7d1	nový
zpěvákem	zpěvák	k1gMnSc7	zpěvák
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Ian	Ian	k1gMnSc1	Ian
Gillan	Gillan	k1gMnSc1	Gillan
z	z	k7c2	z
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc2	Purple
<g/>
.	.	kIx.	.
</s>
<s>
Gillan	Gillan	k1gMnSc1	Gillan
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Neplánoval	plánovat	k5eNaImAgMnS	plánovat
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
připojit	připojit	k5eAaPmF	připojit
k	k	k7c3	k
Black	Blacko	k1gNnPc2	Blacko
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
<g/>
.	.	kIx.	.
</s>
<s>
Vyšel	vyjít	k5eAaPmAgMnS	vyjít
jsem	být	k5eAaImIp1nS	být
ven	ven	k6eAd1	ven
s	s	k7c7	s
Geezerem	Geezer	k1gMnSc7	Geezer
a	a	k8xC	a
Tonym	Tony	k1gMnSc7	Tony
<g/>
,	,	kIx,	,
ožrali	ožrat	k5eAaPmAgMnP	ožrat
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
jsem	být	k5eAaImIp1nS	být
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
oni	onen	k3xDgMnPc1	onen
jsou	být	k5eAaImIp3nP	být
takoví	takový	k3xDgMnPc1	takový
milí	milý	k2eAgMnPc1d1	milý
chlapíci	chlapík	k1gMnPc1	chlapík
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
ohromná	ohromný	k2eAgFnSc1d1	ohromná
zábava	zábava	k1gFnSc1	zábava
<g/>
,	,	kIx,	,
platily	platit	k5eAaImAgInP	platit
se	se	k3xPyFc4	se
účty	účet	k1gInPc7	účet
a	a	k8xC	a
prožil	prožít	k5eAaPmAgMnS	prožít
jsem	být	k5eAaImIp1nS	být
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
krásný	krásný	k2eAgInSc4d1	krásný
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tato	tento	k3xDgFnSc1	tento
sestava	sestava	k1gFnSc1	sestava
nahrála	nahrát	k5eAaPmAgFnS	nahrát
album	album	k1gNnSc4	album
Born	Borna	k1gFnPc2	Borna
Again	Againa	k1gFnPc2	Againa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Bill	Bill	k1gMnSc1	Bill
Ward	Ward	k1gMnSc1	Ward
znovu	znovu	k6eAd1	znovu
opustil	opustit	k5eAaPmAgMnS	opustit
skupinu	skupina	k1gFnSc4	skupina
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
turné	turné	k1gNnSc2	turné
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgMnS	nahradit
ho	on	k3xPp3gMnSc4	on
Bev	Bev	k1gMnSc1	Bev
Bevan	Bevan	k1gMnSc1	Bevan
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Electric	Electric	k1gMnSc1	Electric
Light	Light	k1gMnSc1	Light
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
album	album	k1gNnSc1	album
překvapivě	překvapivě	k6eAd1	překvapivě
stalo	stát	k5eAaPmAgNnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
jejich	jejich	k3xOp3gMnPc2	jejich
nejúspěšnějších	úspěšný	k2eAgMnPc2d3	nejúspěšnější
(	(	kIx(	(
<g/>
#	#	kIx~	#
<g/>
4	[number]	k4	4
v	v	k7c6	v
UK	UK	kA	UK
charts	charts	k6eAd1	charts
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
věci	věc	k1gFnPc1	věc
se	se	k3xPyFc4	se
nevyvíjely	vyvíjet	k5eNaImAgFnP	vyvíjet
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
měly	mít	k5eAaImAgInP	mít
<g/>
,	,	kIx,	,
Ian	Ian	k1gFnPc1	Ian
Gillan	Gillany	k1gInPc2	Gillany
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
Deep	Deep	k1gInSc1	Deep
Purple	Purple	k1gMnPc1	Purple
<g/>
.	.	kIx.	.
</s>
<s>
Bubeník	Bubeník	k1gMnSc1	Bubeník
Bill	Bill	k1gMnSc1	Bill
Ward	Ward	k1gMnSc1	Ward
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgMnS	vrátit
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1984	[number]	k4	1984
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
ohlášeno	ohlášen	k2eAgNnSc1d1	ohlášeno
angažování	angažování	k1gNnSc1	angažování
nového	nový	k2eAgMnSc2d1	nový
zpěváka	zpěvák	k1gMnSc2	zpěvák
Davida	David	k1gMnSc2	David
Donata	Donat	k1gMnSc2	Donat
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
po	po	k7c6	po
šesti	šest	k4xCc6	šest
měsících	měsíc	k1gInPc6	měsíc
zkoušení	zkoušení	k1gNnSc2	zkoušení
byl	být	k5eAaImAgInS	být
vyhozen	vyhodit	k5eAaPmNgInS	vyhodit
managementem	management	k1gInSc7	management
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Tony	Tony	k1gMnSc1	Tony
a	a	k8xC	a
Geezer	Geezer	k1gMnSc1	Geezer
se	se	k3xPyFc4	se
hádali	hádat	k5eAaImAgMnP	hádat
ohledně	ohledně	k7c2	ohledně
finančních	finanční	k2eAgFnPc2d1	finanční
otázek	otázka	k1gFnPc2	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
měnící	měnící	k2eAgFnSc1d1	měnící
sestava	sestava	k1gFnSc1	sestava
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
úspěch	úspěch	k1gInSc1	úspěch
Ozzyho	Ozzy	k1gMnSc2	Ozzy
na	na	k7c6	na
sólové	sólový	k2eAgFnSc6d1	sólová
dráze	dráha	k1gFnSc6	dráha
a	a	k8xC	a
smíšené	smíšený	k2eAgFnPc1d1	smíšená
hudební	hudební	k2eAgFnPc1d1	hudební
kritiky	kritika	k1gFnPc1	kritika
uvrhly	uvrhnout	k5eAaPmAgFnP	uvrhnout
skupinu	skupina	k1gFnSc4	skupina
do	do	k7c2	do
Ozzyho	Ozzy	k1gMnSc2	Ozzy
stínu	stín	k1gInSc2	stín
<g/>
.	.	kIx.	.
</s>
<s>
Zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
Geezer	Geezer	k1gMnSc1	Geezer
Butler	Butler	k1gMnSc1	Butler
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
novou	nový	k2eAgFnSc4d1	nová
skupinu	skupina	k1gFnSc4	skupina
Geezer	Geezer	k1gInSc1	Geezer
Butler	Butler	k1gMnSc1	Butler
Band	banda	k1gFnPc2	banda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ale	ale	k9	ale
nevydala	vydat	k5eNaPmAgFnS	vydat
žádné	žádný	k3yNgNnSc4	žádný
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
sestava	sestava	k1gFnSc1	sestava
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
(	(	kIx(	(
<g/>
Osbourne	Osbourn	k1gInSc5	Osbourn
<g/>
,	,	kIx,	,
Iommi	Iom	k1gFnPc7	Iom
<g/>
,	,	kIx,	,
Butler	Butler	k1gMnSc1	Butler
<g/>
,	,	kIx,	,
Ward	Ward	k1gMnSc1	Ward
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
sešla	sejít	k5eAaPmAgFnS	sejít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zahrála	zahrát	k5eAaPmAgFnS	zahrát
na	na	k7c6	na
benefičním	benefiční	k2eAgInSc6d1	benefiční
koncertě	koncert	k1gInSc6	koncert
Live	Liv	k1gFnSc2	Liv
Aid	Aida	k1gFnPc2	Aida
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
show	show	k1gFnPc2	show
se	se	k3xPyFc4	se
Tony	Tony	k1gMnSc1	Tony
Iommi	Iom	k1gFnPc7	Iom
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
nahrát	nahrát	k5eAaPmF	nahrát
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
a	a	k8xC	a
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
dlouholetého	dlouholetý	k2eAgMnSc4d1	dlouholetý
klávesistu	klávesista	k1gMnSc4	klávesista
Sabbath	Sabbath	k1gMnSc1	Sabbath
Geoffa	Geoff	k1gMnSc4	Geoff
Nichollse	Nicholls	k1gMnSc4	Nicholls
(	(	kIx(	(
<g/>
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
oficiálním	oficiální	k2eAgInSc7d1	oficiální
členem	člen	k1gInSc7	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
)	)	kIx)	)
a	a	k8xC	a
zpěváka	zpěvák	k1gMnSc2	zpěvák
Glenna	Glenn	k1gMnSc2	Glenn
Hughese	Hughese	k1gFnSc2	Hughese
<g/>
,	,	kIx,	,
bývalého	bývalý	k2eAgMnSc2d1	bývalý
člena	člen	k1gMnSc2	člen
skupin	skupina	k1gFnPc2	skupina
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
a	a	k8xC	a
Trapeze	Trapeze	k1gFnSc1	Trapeze
<g/>
.	.	kIx.	.
</s>
<s>
Tony	Tony	k1gMnSc1	Tony
Iommi	Iom	k1gFnPc7	Iom
se	se	k3xPyFc4	se
zasnoubil	zasnoubit	k5eAaPmAgMnS	zasnoubit
se	s	k7c7	s
známou	známý	k2eAgFnSc7d1	známá
ženskou	ženský	k2eAgFnSc7d1	ženská
heavymetalovou	heavymetalový	k2eAgFnSc7d1	heavymetalová
hvězdou	hvězda	k1gFnSc7	hvězda
Litou	litý	k2eAgFnSc7d1	litá
Ford	ford	k1gInSc4	ford
a	a	k8xC	a
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
baskytaristu	baskytarista	k1gMnSc4	baskytarista
(	(	kIx(	(
<g/>
Dava	Dava	k1gMnSc1	Dava
Spitze	Spitze	k1gFnSc2	Spitze
<g/>
)	)	kIx)	)
a	a	k8xC	a
bubeníka	bubeník	k1gMnSc2	bubeník
Erica	Ericus	k1gMnSc2	Ericus
Singera	Singer	k1gMnSc2	Singer
(	(	kIx(	(
<g/>
Kiss	Kiss	k1gInSc1	Kiss
a	a	k8xC	a
Alice	Alice	k1gFnSc1	Alice
Cooper	Coopra	k1gFnPc2	Coopra
<g/>
)	)	kIx)	)
z	z	k7c2	z
její	její	k3xOp3gFnSc2	její
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokončil	dokončit	k5eAaPmAgMnS	dokončit
formaci	formace	k1gFnSc4	formace
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
management	management	k1gInSc1	management
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
nahrávací	nahrávací	k2eAgFnSc2d1	nahrávací
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
přiměl	přimět	k5eAaPmAgMnS	přimět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
album	album	k1gNnSc1	album
Seventh	Seventha	k1gFnPc2	Seventha
Star	Star	kA	Star
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jako	jako	k8xC	jako
Black	Black	k1gInSc4	Black
Sabbath	Sabbath	k1gInSc1	Sabbath
featuring	featuring	k1gInSc1	featuring
Tony	Tony	k1gFnSc1	Tony
Iommi	Iom	k1gFnPc7	Iom
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
vede	vést	k5eAaImIp3nS	vést
polemika	polemik	k1gMnSc2	polemik
kvůli	kvůli	k7c3	kvůli
Jeffovi	Jeff	k1gMnSc3	Jeff
Fenholtovi	Fenholt	k1gMnSc3	Fenholt
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
na	na	k7c6	na
albu	album	k1gNnSc6	album
také	také	k9	také
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
okolo	okolo	k7c2	okolo
sedmi	sedm	k4xCc2	sedm
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
kapelu	kapela	k1gFnSc4	kapela
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mu	on	k3xPp3gNnSc3	on
prý	prý	k9	prý
nevěřili	věřit	k5eNaImAgMnP	věřit
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
trvají	trvat	k5eAaImIp3nP	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgInS	být
členem	člen	k1gInSc7	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
nahrál	nahrát	k5eAaPmAgInS	nahrát
jen	jen	k9	jen
nějaká	nějaký	k3yIgFnSc1	nějaký
dema	dema	k1gFnSc1	dema
pro	pro	k7c4	pro
Tonyho	Tony	k1gMnSc4	Tony
sólové	sólový	k2eAgNnSc1d1	sólové
album	album	k1gNnSc1	album
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
napsal	napsat	k5eAaPmAgMnS	napsat
některé	některý	k3yIgFnPc4	některý
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
albu	album	k1gNnSc6	album
Seventh	Seventha	k1gFnPc2	Seventha
Star	star	k1gFnSc1	star
objevily	objevit	k5eAaPmAgInP	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
turné	turné	k1gNnSc2	turné
propagujícího	propagující	k2eAgInSc2d1	propagující
album	album	k1gNnSc1	album
Seventh	Seventh	k1gInSc4	Seventh
Star	star	k1gFnSc2	star
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
se	se	k3xPyFc4	se
Glenn	Glenn	k1gMnSc1	Glenn
Hughes	Hughes	k1gMnSc1	Hughes
porval	porvat	k5eAaPmAgMnS	porvat
a	a	k8xC	a
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
vážnou	vážný	k2eAgFnSc4d1	vážná
krevní	krevní	k2eAgFnSc4d1	krevní
sraženinu	sraženina	k1gFnSc4	sraženina
v	v	k7c6	v
hrdle	hrdla	k1gFnSc6	hrdla
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
které	který	k3yRgFnSc3	který
ztratil	ztratit	k5eAaPmAgMnS	ztratit
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Neznámý	známý	k2eNgMnSc1d1	neznámý
mladý	mladý	k2eAgMnSc1d1	mladý
americký	americký	k2eAgMnSc1d1	americký
zpěvák	zpěvák	k1gMnSc1	zpěvák
Ray	Ray	k1gMnSc1	Ray
Gillen	Gillen	k1gInSc4	Gillen
byl	být	k5eAaImAgMnS	být
vybran	vybran	k1gInSc4	vybran
jako	jako	k8xS	jako
záskok	záskok	k1gInSc4	záskok
a	a	k8xC	a
dokončil	dokončit	k5eAaPmAgMnS	dokončit
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Morálka	morálka	k1gFnSc1	morálka
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
když	když	k8xS	když
začali	začít	k5eAaPmAgMnP	začít
nahrávat	nahrávat	k5eAaImF	nahrávat
album	album	k1gNnSc4	album
The	The	k1gFnSc2	The
Eternal	Eternal	k1gFnPc2	Eternal
Idol	idol	k1gInSc1	idol
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
Bev	Bev	k1gMnSc1	Bev
Bevan	Bevan	k1gMnSc1	Bevan
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
jako	jako	k9	jako
bubeník	bubeník	k1gMnSc1	bubeník
a	a	k8xC	a
baskytarista	baskytarista	k1gMnSc1	baskytarista
Bob	Bob	k1gMnSc1	Bob
Daisley	Daislea	k1gFnSc2	Daislea
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c6	na
albu	album	k1gNnSc6	album
podílel	podílet	k5eAaImAgMnS	podílet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nový	nový	k2eAgInSc4d1	nový
Black	Black	k1gInSc4	Black
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
série	série	k1gFnSc1	série
neštěstí	neštěstí	k1gNnSc2	neštěstí
jako	jako	k9	jako
zlé	zlý	k2eAgNnSc1d1	zlé
vedení	vedení	k1gNnSc1	vedení
a	a	k8xC	a
finanční	finanční	k2eAgInSc1d1	finanční
dluh	dluh	k1gInSc1	dluh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pocházel	pocházet	k5eAaImAgInS	pocházet
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
nevhodně	vhodně	k6eNd1	vhodně
naplánovaného	naplánovaný	k2eAgNnSc2d1	naplánované
využití	využití	k1gNnSc2	využití
drahých	drahý	k2eAgFnPc2d1	drahá
nahrávacích	nahrávací	k2eAgFnPc2d1	nahrávací
studií	studie	k1gFnPc2	studie
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ray	Ray	k1gFnSc1	Ray
Gillen	Gillna	k1gFnPc2	Gillna
během	během	k7c2	během
nahrávání	nahrávání	k1gNnSc2	nahrávání
skupinu	skupina	k1gFnSc4	skupina
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgMnS	dát
dohromady	dohromady	k6eAd1	dohromady
s	s	k7c7	s
bývalým	bývalý	k2eAgMnSc7d1	bývalý
kytaristou	kytarista	k1gMnSc7	kytarista
Ozzyho	Ozzy	k1gMnSc2	Ozzy
Osbourna	Osbourno	k1gNnSc2	Osbourno
Jakem	jak	k1gMnSc7	jak
E.	E.	kA	E.
Leem	Leo	k1gMnSc7	Leo
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
skupinu	skupina	k1gFnSc4	skupina
Badlands	Badlandsa	k1gFnPc2	Badlandsa
(	(	kIx(	(
<g/>
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgInS	přidat
Eric	Eric	k1gInSc1	Eric
Singer	Singer	k1gInSc1	Singer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
Tony	Tony	k1gMnSc1	Tony
Martin	Martin	k1gMnSc1	Martin
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
The	The	k1gFnSc2	The
Alliance	Alliance	k1gFnSc2	Alliance
<g/>
)	)	kIx)	)
přinesl	přinést	k5eAaPmAgMnS	přinést
všechny	všechen	k3xTgInPc4	všechen
znova	znova	k6eAd1	znova
přezpívané	přezpívaný	k2eAgInPc4d1	přezpívaný
Gillenovy	Gillenův	k2eAgInPc4d1	Gillenův
původní	původní	k2eAgInPc4d1	původní
vokály	vokál	k1gInPc4	vokál
na	na	k7c6	na
kazetách	kazeta	k1gFnPc6	kazeta
Eternal	Eternal	k1gFnSc2	Eternal
Idol	idol	k1gInSc1	idol
a	a	k8xC	a
album	album	k1gNnSc4	album
nakonec	nakonec	k6eAd1	nakonec
vydali	vydat	k5eAaPmAgMnP	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Tony	Tony	k1gMnSc1	Tony
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Cat	Cat	k1gMnSc1	Cat
<g/>
"	"	kIx"	"
Martin	Martin	k1gMnSc1	Martin
se	se	k3xPyFc4	se
jevil	jevit	k5eAaImAgMnS	jevit
jako	jako	k9	jako
dokonalý	dokonalý	k2eAgMnSc1d1	dokonalý
zpěvák	zpěvák	k1gMnSc1	zpěvák
pro	pro	k7c4	pro
znovuoživené	znovuoživený	k2eAgNnSc4d1	znovuoživené
složení	složení	k1gNnSc4	složení
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
trochu	trochu	k6eAd1	trochu
podobal	podobat	k5eAaImAgMnS	podobat
Diovi	Diův	k2eAgMnPc1d1	Diův
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
Martin	Martin	k1gMnSc1	Martin
evidentně	evidentně	k6eAd1	evidentně
vlastní	vlastní	k2eAgInSc4d1	vlastní
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nahrání	nahrání	k1gNnSc6	nahrání
The	The	k1gFnPc2	The
Eternal	Eternal	k1gFnSc1	Eternal
Idol	idol	k1gInSc1	idol
většina	většina	k1gFnSc1	většina
skupiny	skupina	k1gFnSc2	skupina
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
opouští	opouštět	k5eAaImIp3nS	opouštět
<g/>
.	.	kIx.	.
</s>
<s>
Iommi	Iom	k1gFnPc7	Iom
<g/>
,	,	kIx,	,
Martin	Martina	k1gFnPc2	Martina
a	a	k8xC	a
Nicholls	Nichollsa	k1gFnPc2	Nichollsa
najali	najmout	k5eAaPmAgMnP	najmout
baskytaristu	baskytarista	k1gMnSc4	baskytarista
Joa	Joa	k1gMnSc2	Joa
Burta	Burt	k1gMnSc2	Burt
a	a	k8xC	a
bývalého	bývalý	k2eAgMnSc2d1	bývalý
bubeníka	bubeník	k1gMnSc2	bubeník
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc1	The
Clash	Clash	k1gMnSc1	Clash
-	-	kIx~	-
Terryho	Terry	k1gMnSc2	Terry
Chimese	Chimese	k1gFnSc2	Chimese
pro	pro	k7c4	pro
krátké	krátký	k2eAgNnSc4d1	krátké
turné	turné	k1gNnSc4	turné
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
Eternal	Eternal	k1gMnSc2	Eternal
Idol	idol	k1gInSc4	idol
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
bylo	být	k5eAaImAgNnS	být
složení	složení	k1gNnSc1	složení
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
opět	opět	k6eAd1	opět
stabilnější	stabilní	k2eAgInSc1d2	stabilnější
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
hráli	hrát	k5eAaImAgMnP	hrát
Tony	Tony	k1gMnSc1	Tony
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
Geoff	Geoff	k1gMnSc1	Geoff
Nicholls	Nicholls	k1gInSc1	Nicholls
a	a	k8xC	a
přidal	přidat	k5eAaPmAgMnS	přidat
se	se	k3xPyFc4	se
místní	místní	k2eAgMnPc1d1	místní
bubeník	bubeník	k1gMnSc1	bubeník
Cozy	Coza	k1gFnSc2	Coza
Powell	Powell	k1gMnSc1	Powell
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Terryho	Terry	k1gMnSc4	Terry
Chimesa	Chimesa	k1gFnSc1	Chimesa
<g/>
.	.	kIx.	.
</s>
<s>
Powell	Powell	k1gMnSc1	Powell
<g/>
,	,	kIx,	,
legendární	legendární	k2eAgMnSc1d1	legendární
bubeník	bubeník	k1gMnSc1	bubeník
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
předtím	předtím	k6eAd1	předtím
úspěch	úspěch	k1gInSc1	úspěch
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
původní	původní	k2eAgFnSc7d1	původní
skupinou	skupina	k1gFnSc7	skupina
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
s	s	k7c7	s
kapelami	kapela	k1gFnPc7	kapela
Rainbow	Rainbow	k1gFnPc2	Rainbow
<g/>
,	,	kIx,	,
Whitesnake	Whitesnake	k1gFnPc2	Whitesnake
<g/>
,	,	kIx,	,
ELP	ELP	kA	ELP
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
dalšími	další	k2eAgFnPc7d1	další
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
baskytaristou	baskytarista	k1gMnSc7	baskytarista
Laurencem	Laurenec	k1gMnSc7	Laurenec
Cottlem	Cottl	k1gMnSc7	Cottl
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Joa	Joa	k1gFnSc4	Joa
Burta	Burta	k1gMnSc1	Burta
<g/>
,	,	kIx,	,
Sabbath	Sabbath	k1gMnSc1	Sabbath
vydali	vydat	k5eAaPmAgMnP	vydat
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
kritikou	kritika	k1gFnSc7	kritika
vysoce	vysoce	k6eAd1	vysoce
hodnocené	hodnocený	k2eAgNnSc1d1	hodnocené
album	album	k1gNnSc1	album
Headless	Headlessa	k1gFnPc2	Headlessa
Cross	Crossa	k1gFnPc2	Crossa
<g/>
.	.	kIx.	.
</s>
<s>
MTV	MTV	kA	MTV
video	video	k1gNnSc1	video
pro	pro	k7c4	pro
titulní	titulní	k2eAgInSc4d1	titulní
singl	singl	k1gInSc4	singl
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
velmi	velmi	k6eAd1	velmi
hraným	hraný	k2eAgMnSc7d1	hraný
a	a	k8xC	a
těšilo	těšit	k5eAaImAgNnS	těšit
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
pozitivním	pozitivní	k2eAgFnPc3d1	pozitivní
recenzím	recenze	k1gFnPc3	recenze
<g/>
.	.	kIx.	.
</s>
<s>
Baskytaristu	baskytarista	k1gMnSc4	baskytarista
Laurenca	Laurenca	k1gMnSc1	Laurenca
Cottleho	Cottle	k1gMnSc4	Cottle
nahradil	nahradit	k5eAaPmAgMnS	nahradit
veterán	veterán	k1gMnSc1	veterán
Neil	Neil	k1gMnSc1	Neil
Murray	Murraa	k1gFnSc2	Murraa
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgMnSc1d1	bývalý
spoluhráč	spoluhráč	k1gMnSc1	spoluhráč
Cozyho	Cozy	k1gMnSc2	Cozy
Powella	Powell	k1gMnSc2	Powell
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Whitesnake	Whitesnak	k1gFnSc2	Whitesnak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
vydali	vydat	k5eAaPmAgMnP	vydat
album	album	k1gNnSc4	album
Tyr	Tyr	k1gFnSc2	Tyr
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
1990	[number]	k4	1990
až	až	k8xS	až
1991	[number]	k4	1991
skupina	skupina	k1gFnSc1	skupina
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
turné	turné	k1gNnSc4	turné
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
sešla	sejít	k5eAaPmAgFnS	sejít
klasická	klasický	k2eAgFnSc1d1	klasická
sestava	sestava	k1gFnSc1	sestava
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
z	z	k7c2	z
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
Geezer	Geezer	k1gMnSc1	Geezer
Butler	Butler	k1gMnSc1	Butler
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Diem	Die	k1gNnSc7	Die
a	a	k8xC	a
Appicem	Appic	k1gMnSc7	Appic
se	se	k3xPyFc4	se
připojili	připojit	k5eAaPmAgMnP	připojit
k	k	k7c3	k
Iommimu	Iommimo	k1gNnSc3	Iommimo
(	(	kIx(	(
<g/>
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
stejná	stejný	k2eAgFnSc1d1	stejná
sestava	sestava	k1gFnSc1	sestava
jako	jako	k8xS	jako
na	na	k7c6	na
albech	album	k1gNnPc6	album
Mob	Mob	k1gMnPc2	Mob
Rules	Rules	k1gInSc1	Rules
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
a	a	k8xC	a
Live	Live	k1gNnSc4	Live
Evil	Evila	k1gFnPc2	Evila
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
a	a	k8xC	a
společně	společně	k6eAd1	společně
nahráli	nahrát	k5eAaBmAgMnP	nahrát
album	album	k1gNnSc4	album
Dehumanizer	Dehumanizra	k1gFnPc2	Dehumanizra
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sabbath	Sabbath	k1gInSc1	Sabbath
s	s	k7c7	s
albem	album	k1gNnSc7	album
opět	opět	k6eAd1	opět
zažívali	zažívat	k5eAaImAgMnP	zažívat
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
turné	turné	k1gNnSc4	turné
Dehumanizer	Dehumanizra	k1gFnPc2	Dehumanizra
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Ozzy	Ozza	k1gFnSc2	Ozza
chtěl	chtít	k5eAaImAgMnS	chtít
stáhnout	stáhnout	k5eAaPmF	stáhnout
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Sabbathu	Sabbatha	k1gFnSc4	Sabbatha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
otevřel	otevřít	k5eAaPmAgMnS	otevřít
jeho	jeho	k3xOp3gNnPc4	jeho
poslední	poslední	k2eAgNnPc4d1	poslední
dvě	dva	k4xCgNnPc4	dva
vystoupení	vystoupení	k1gNnPc4	vystoupení
v	v	k7c4	v
Costa	Costo	k1gNnPc4	Costo
Mesa	Mes	k2eAgFnSc1d1	Mesa
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Dio	Dio	k1gMnSc1	Dio
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
<g/>
,	,	kIx,	,
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Iommi	Iom	k1gFnPc7	Iom
<g/>
,	,	kIx,	,
Butler	Butler	k1gInSc1	Butler
a	a	k8xC	a
Appice	Appice	k1gFnSc1	Appice
bez	bez	k7c2	bez
něho	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Dio	Dio	k?	Dio
opustil	opustit	k5eAaPmAgMnS	opustit
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
sólové	sólový	k2eAgFnSc2d1	sólová
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
a	a	k8xC	a
Rob	roba	k1gFnPc2	roba
Halford	Halford	k1gMnSc1	Halford
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
skupiny	skupina	k1gFnSc2	skupina
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
<g/>
,	,	kIx,	,
ho	on	k3xPp3gMnSc4	on
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
vystoupení	vystoupení	k1gNnSc4	vystoupení
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgMnS	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
sestava	sestava	k1gFnSc1	sestava
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
s	s	k7c7	s
Billem	Bill	k1gMnSc7	Bill
Wardem	Ward	k1gMnSc7	Ward
se	se	k3xPyFc4	se
spojila	spojit	k5eAaPmAgNnP	spojit
<g/>
,	,	kIx,	,
aby	aby	k9	aby
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1992	[number]	k4	1992
zakončila	zakončit	k5eAaPmAgFnS	zakončit
druhou	druhý	k4xOgFnSc4	druhý
noc	noc	k1gFnSc4	noc
vystoupení	vystoupení	k1gNnSc2	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
se	se	k3xPyFc4	se
Ozzy	Ozza	k1gFnSc2	Ozza
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
dál	daleko	k6eAd2	daleko
pokračovat	pokračovat	k5eAaImF	pokračovat
(	(	kIx(	(
<g/>
následovalo	následovat	k5eAaImAgNnS	následovat
turné	turné	k1gNnSc4	turné
"	"	kIx"	"
<g/>
No	no	k9	no
More	mor	k1gInSc5	mor
Tours	Tours	k1gInSc1	Tours
<g/>
"	"	kIx"	"
s	s	k7c7	s
výstižným	výstižný	k2eAgInSc7d1	výstižný
názvem	název	k1gInSc7	název
Retirement	Retirement	k1gInSc1	Retirement
Sucks	Sucksa	k1gFnPc2	Sucksa
<g/>
)	)	kIx)	)
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
připraveny	připravit	k5eAaPmNgFnP	připravit
smlouvy	smlouva	k1gFnPc1	smlouva
na	na	k7c4	na
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
a	a	k8xC	a
turné	turné	k1gNnSc4	turné
původního	původní	k2eAgInSc2d1	původní
Sabbathu	Sabbath	k1gInSc2	Sabbath
<g/>
,	,	kIx,	,
na	na	k7c4	na
poslední	poslední	k2eAgFnSc4d1	poslední
chvíli	chvíle	k1gFnSc4	chvíle
však	však	k9	však
Ozzy	Ozza	k1gFnPc4	Ozza
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Diově	Diův	k2eAgInSc6d1	Diův
a	a	k8xC	a
Halfordově	Halfordův	k2eAgInSc6d1	Halfordův
odchodu	odchod	k1gInSc6	odchod
byl	být	k5eAaImAgInS	být
Vinny	vinen	k2eAgMnPc4d1	vinen
Appice	Appic	k1gMnPc4	Appic
nahrazen	nahradit	k5eAaPmNgInS	nahradit
bývalým	bývalý	k2eAgMnSc7d1	bývalý
bubeníkem	bubeník	k1gMnSc7	bubeník
skupiny	skupina	k1gFnSc2	skupina
Rainbow	Rainbow	k1gFnSc2	Rainbow
Bobbym	Bobbymum	k1gNnPc2	Bobbymum
Rondinellim	Rondinellim	k1gMnSc1	Rondinellim
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
Tony	Tony	k1gMnSc1	Tony
Martin	Martin	k1gMnSc1	Martin
a	a	k8xC	a
klávesista	klávesista	k1gMnSc1	klávesista
Geoff	Geoff	k1gMnSc1	Geoff
Nicholls	Nichollsa	k1gFnPc2	Nichollsa
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
a	a	k8xC	a
Sabbath	Sabbath	k1gInSc4	Sabbath
nahráli	nahrát	k5eAaBmAgMnP	nahrát
Cross	Cross	k1gInSc4	Cross
Purposes	Purposesa	k1gFnPc2	Purposesa
a	a	k8xC	a
Cross	Crossa	k1gFnPc2	Crossa
Purposes	Purposesa	k1gFnPc2	Purposesa
Live	Liv	k1gFnSc2	Liv
-	-	kIx~	-
kombinaci	kombinace	k1gFnSc3	kombinace
CD	CD	kA	CD
a	a	k8xC	a
videa	video	k1gNnSc2	video
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Bobby	Bobba	k1gFnPc4	Bobba
Rondinelli	Rondinelle	k1gFnSc4	Rondinelle
skupinu	skupina	k1gFnSc4	skupina
v	v	k7c6	v
půlce	půlka	k1gFnSc6	půlka
turné	turné	k1gNnSc2	turné
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Překvapivě	překvapivě	k6eAd1	překvapivě
ho	on	k3xPp3gNnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
původní	původní	k2eAgMnSc1d1	původní
bubeník	bubeník	k1gMnSc1	bubeník
Sabbath	Sabbath	k1gMnSc1	Sabbath
Bill	Bill	k1gMnSc1	Bill
Ward	Ward	k1gMnSc1	Ward
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
turné	turné	k1gNnSc6	turné
se	se	k3xPyFc4	se
Ward	Ward	k1gInSc1	Ward
a	a	k8xC	a
Butler	Butler	k1gInSc1	Butler
s	s	k7c7	s
Iommim	Iommi	k1gNnSc7	Iommi
<g/>
,	,	kIx,	,
Martinem	Martin	k1gInSc7	Martin
a	a	k8xC	a
Nichollsem	Nicholls	k1gInSc7	Nicholls
rozešli	rozejít	k5eAaPmAgMnP	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
střetnutí	střetnutí	k1gNnSc1	střetnutí
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
síly	síla	k1gFnPc4	síla
spojila	spojit	k5eAaPmAgFnS	spojit
formace	formace	k1gFnSc1	formace
alba	album	k1gNnSc2	album
Tyr	Tyr	k1gFnSc2	Tyr
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
bubeník	bubeník	k1gMnSc1	bubeník
Cozy	Coza	k1gFnSc2	Coza
Powell	Powell	k1gMnSc1	Powell
a	a	k8xC	a
baskytarista	baskytarista	k1gMnSc1	baskytarista
Neil	Neil	k1gMnSc1	Neil
Murray	Murraa	k1gFnSc2	Murraa
připojili	připojit	k5eAaPmAgMnP	připojit
k	k	k7c3	k
Iommimu	Iommim	k1gMnSc3	Iommim
<g/>
,	,	kIx,	,
Martinovi	Martin	k1gMnSc3	Martin
<g/>
,	,	kIx,	,
Nichollsovi	Nicholls	k1gMnSc3	Nicholls
a	a	k8xC	a
nahráli	nahrát	k5eAaPmAgMnP	nahrát
album	album	k1gNnSc4	album
Forbidden	Forbiddna	k1gFnPc2	Forbiddna
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc4	album
produkoval	produkovat	k5eAaImAgMnS	produkovat
Ernie	Ernie	k1gFnPc4	Ernie
C	C	kA	C
z	z	k7c2	z
rapcorové	rapcorový	k2eAgFnSc2d1	rapcorový
skupiny	skupina	k1gFnSc2	skupina
Body	bod	k1gInPc1	bod
Count	Counta	k1gFnPc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nahrání	nahrání	k1gNnSc6	nahrání
alba	album	k1gNnSc2	album
Cozy	Coza	k1gFnSc2	Coza
Powell	Powell	k1gMnSc1	Powell
znovu	znovu	k6eAd1	znovu
kapelu	kapela	k1gFnSc4	kapela
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgInS	nahradit
ho	on	k3xPp3gMnSc4	on
vracející	vracející	k2eAgMnSc1d1	vracející
se	se	k3xPyFc4	se
Bobby	Bobba	k1gFnSc2	Bobba
Rondinelli	Rondinelle	k1gFnSc3	Rondinelle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
u	u	k7c2	u
Castle	Castle	k1gFnSc2	Castle
Records	Recordsa	k1gFnPc2	Recordsa
znovu	znovu	k6eAd1	znovu
nahráli	nahrát	k5eAaBmAgMnP	nahrát
a	a	k8xC	a
vydali	vydat	k5eAaPmAgMnP	vydat
kompilaci	kompilace	k1gFnSc4	kompilace
skladeb	skladba	k1gFnPc2	skladba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
Born	Borna	k1gFnPc2	Borna
Again	Againa	k1gFnPc2	Againa
<g/>
)	)	kIx)	)
po	po	k7c4	po
rok	rok	k1gInSc4	rok
1995	[number]	k4	1995
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
Forbidden	Forbiddna	k1gFnPc2	Forbiddna
<g/>
)	)	kIx)	)
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gMnSc1	The
Sabbath	Sabbath	k1gMnSc1	Sabbath
Stones	Stones	k1gMnSc1	Stones
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
skončila	skončit	k5eAaPmAgFnS	skončit
smlouva	smlouva	k1gFnSc1	smlouva
Tonyho	Tony	k1gMnSc2	Tony
Iommiho	Iommi	k1gMnSc2	Iommi
s	s	k7c7	s
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
Ozzy	Ozza	k1gFnSc2	Ozza
Osbourne	Osbourn	k1gInSc5	Osbourn
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
první	první	k4xOgInSc4	první
ročník	ročník	k1gInSc4	ročník
velmi	velmi	k6eAd1	velmi
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
metalového	metalový	k2eAgInSc2d1	metalový
festivalu	festival	k1gInSc2	festival
Ozzfest	Ozzfest	k1gMnSc1	Ozzfest
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
před	před	k7c7	před
posledním	poslední	k2eAgNnSc7d1	poslední
vystoupením	vystoupení	k1gNnSc7	vystoupení
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
Butlerovi	Butler	k1gMnSc3	Butler
a	a	k8xC	a
Iommimu	Iommim	k1gMnSc3	Iommim
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zahráli	zahrát	k5eAaPmAgMnP	zahrát
několik	několik	k4yIc4	několik
skladeb	skladba	k1gFnPc2	skladba
(	(	kIx(	(
<g/>
na	na	k7c4	na
bicí	bicí	k2eAgNnSc4d1	bicí
hrál	hrát	k5eAaImAgMnS	hrát
Mike	Mik	k1gInSc2	Mik
Bordin	Bordin	k2eAgMnSc1d1	Bordin
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
skupiny	skupina	k1gFnSc2	skupina
Faith	Faitha	k1gFnPc2	Faitha
No	no	k9	no
More	mor	k1gInSc5	mor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1997	[number]	k4	1997
Bill	Bill	k1gMnSc1	Bill
Ward	Ward	k1gMnSc1	Ward
spojil	spojit	k5eAaPmAgMnS	spojit
síly	síl	k1gInPc4	síl
s	s	k7c7	s
Ozzym	Ozzym	k1gInSc1	Ozzym
<g/>
,	,	kIx,	,
Tonym	Tony	k1gMnSc7	Tony
<g/>
,	,	kIx,	,
Geezerem	Geezer	k1gInSc7	Geezer
a	a	k8xC	a
obnovili	obnovit	k5eAaPmAgMnP	obnovit
původní	původní	k2eAgFnSc4d1	původní
sestavu	sestava	k1gFnSc4	sestava
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
jejich	jejich	k3xOp3gNnSc2	jejich
náhlého	náhlý	k2eAgNnSc2d1	náhlé
setkání	setkání	k1gNnSc2	setkání
na	na	k7c4	na
Live	Live	k1gNnSc4	Live
Aid	Aida	k1gFnPc2	Aida
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1985	[number]	k4	1985
a	a	k8xC	a
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
vydali	vydat	k5eAaPmAgMnP	vydat
minimálně	minimálně	k6eAd1	minimálně
jednu	jeden	k4xCgFnSc4	jeden
autorizovanou	autorizovaný	k2eAgFnSc4d1	autorizovaná
dvoudiskovou	dvoudiskový	k2eAgFnSc4d1	dvoudisková
kompilaci	kompilace	k1gFnSc4	kompilace
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
dvoudiskovou	dvoudiskový	k2eAgFnSc4d1	dvoudisková
live	live	k1gFnSc4	live
kompilaci	kompilace	k1gFnSc4	kompilace
a	a	k8xC	a
osmidiskový	osmidiskový	k2eAgInSc4d1	osmidiskový
boxset	boxset	k1gInSc4	boxset
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
napsali	napsat	k5eAaBmAgMnP	napsat
nové	nový	k2eAgInPc4d1	nový
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
na	na	k7c6	na
následujícím	následující	k2eAgNnSc6d1	následující
turné	turné	k1gNnSc6	turné
odehráli	odehrát	k5eAaPmAgMnP	odehrát
novou	nový	k2eAgFnSc4d1	nová
skladbu	skladba	k1gFnSc4	skladba
Scary	Scara	k1gFnSc2	Scara
Dreams	Dreams	k1gInSc1	Dreams
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
začali	začít	k5eAaPmAgMnP	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
s	s	k7c7	s
legendárním	legendární	k2eAgMnSc7d1	legendární
producentem	producent	k1gMnSc7	producent
Rickem	Ricek	k1gMnSc7	Ricek
Rubinem	Rubin	k1gMnSc7	Rubin
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Ozzyho	Ozzy	k1gMnSc4	Ozzy
sólový	sólový	k2eAgInSc1d1	sólový
kontrakt	kontrakt	k1gInSc1	kontrakt
se	se	k3xPyFc4	se
zdržel	zdržet	k5eAaPmAgInS	zdržet
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
překazil	překazit	k5eAaPmAgMnS	překazit
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
znova	znova	k6eAd1	znova
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c6	na
Ozzfestu	Ozzfest	k1gInSc6	Ozzfest
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
svého	svůj	k3xOyFgNnSc2	svůj
35	[number]	k4	35
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
Ozzy	Ozza	k1gFnSc2	Ozza
znovu	znovu	k6eAd1	znovu
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
s	s	k7c7	s
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
na	na	k7c6	na
Ozzfestu	Ozzfest	k1gInSc6	Ozzfest
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
také	také	k9	také
legenda	legenda	k1gFnSc1	legenda
Iron	iron	k1gInSc1	iron
Maiden	Maidno	k1gNnPc2	Maidno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
klávesy	klávesa	k1gFnPc4	klávesa
nyní	nyní	k6eAd1	nyní
hrál	hrát	k5eAaImAgMnS	hrát
syn	syn	k1gMnSc1	syn
Ricka	Ricek	k1gMnSc4	Ricek
Wakemana	Wakeman	k1gMnSc4	Wakeman
Adam	Adam	k1gMnSc1	Adam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2005	[number]	k4	2005
byli	být	k5eAaImAgMnP	být
Black	Black	k1gInSc4	Black
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
slavnostně	slavnostně	k6eAd1	slavnostně
uvedeni	uvést	k5eAaPmNgMnP	uvést
do	do	k7c2	do
britské	britský	k2eAgFnSc2d1	britská
hudební	hudební	k2eAgFnSc2d1	hudební
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
UK	UK	kA	UK
Music	Music	k1gMnSc1	Music
Hall	Hall	k1gMnSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
,	,	kIx,	,
během	během	k7c2	během
slavnosti	slavnost	k1gFnSc2	slavnost
zahrála	zahrát	k5eAaPmAgFnS	zahrát
původní	původní	k2eAgFnSc1d1	původní
sestava	sestava	k1gFnSc1	sestava
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
také	také	k9	také
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
.	.	kIx.	.
</s>
<s>
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
ale	ale	k9	ale
žádnou	žádný	k3yNgFnSc4	žádný
ze	z	k7c2	z
skladeb	skladba	k1gFnPc2	skladba
nehrálo	hrát	k5eNaImAgNnS	hrát
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
zahrála	zahrát	k5eAaPmAgFnS	zahrát
dvě	dva	k4xCgFnPc1	dva
jejich	jejich	k3xOp3gFnPc1	jejich
skladby	skladba	k1gFnPc1	skladba
Hole	hole	k1gFnSc2	hole
in	in	k?	in
the	the	k?	the
Sky	Sky	k1gFnSc4	Sky
a	a	k8xC	a
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
Metallica	Metallic	k1gInSc2	Metallic
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
Ozbourne	Ozbourn	k1gInSc5	Ozbourn
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
webu	web	k1gInSc6	web
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
původní	původní	k2eAgFnSc1d1	původní
sestava	sestava	k1gFnSc1	sestava
může	moct	k5eAaImIp3nS	moct
spojit	spojit	k5eAaPmF	spojit
síly	síla	k1gFnPc4	síla
a	a	k8xC	a
nahrát	nahrát	k5eAaBmF	nahrát
nové	nový	k2eAgNnSc4d1	nové
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
po	po	k7c4	po
30	[number]	k4	30
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
návrat	návrat	k1gInSc1	návrat
původní	původní	k2eAgFnSc2d1	původní
sestavy	sestava	k1gFnSc2	sestava
a	a	k8xC	a
potvrzení	potvrzení	k1gNnSc2	potvrzení
alba	album	k1gNnSc2	album
a	a	k8xC	a
turné	turné	k1gNnSc2	turné
se	se	k3xPyFc4	se
ale	ale	k9	ale
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
až	až	k9	až
11	[number]	k4	11
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
v	v	k7c4	v
11	[number]	k4	11
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
11	[number]	k4	11
minut	minuta	k1gFnPc2	minuta
pacifického	pacifický	k2eAgInSc2d1	pacifický
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
Rick	Rick	k1gMnSc1	Rick
Rubin	Rubin	k1gMnSc1	Rubin
a	a	k8xC	a
album	album	k1gNnSc1	album
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
13	[number]	k4	13
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
bicí	bicí	k2eAgFnSc4d1	bicí
zde	zde	k6eAd1	zde
usedl	usednout	k5eAaPmAgMnS	usednout
bubeník	bubeník	k1gMnSc1	bubeník
skupin	skupina	k1gFnPc2	skupina
Rage	Rage	k1gInSc4	Rage
Against	Against	k1gFnSc4	Against
the	the	k?	the
Machine	Machin	k1gMnSc5	Machin
a	a	k8xC	a
Audioslave	Audioslav	k1gMnSc5	Audioslav
Brad	brada	k1gFnPc2	brada
Wilk	Wilk	k1gInSc1	Wilk
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Bill	Bill	k1gMnSc1	Bill
Ward	Ward	k1gMnSc1	Ward
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
ještě	ještě	k9	ještě
v	v	k7c4	v
září	září	k1gNnSc4	září
2014	[number]	k4	2014
Ozzy	Ozza	k1gFnSc2	Ozza
Ozbourne	Ozbourn	k1gInSc5	Ozbourn
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
nahrát	nahrát	k5eAaBmF	nahrát
příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
ještě	ještě	k6eAd1	ještě
jednu	jeden	k4xCgFnSc4	jeden
desku	deska	k1gFnSc4	deska
a	a	k8xC	a
absolvovat	absolvovat	k5eAaPmF	absolvovat
další	další	k2eAgNnSc4d1	další
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
toto	tento	k3xDgNnSc1	tento
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
změnili	změnit	k5eAaPmAgMnP	změnit
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
tvorbu	tvorba	k1gFnSc4	tvorba
nového	nový	k2eAgInSc2d1	nový
materiálu	materiál	k1gInSc2	materiál
příliš	příliš	k6eAd1	příliš
staří	starý	k2eAgMnPc1d1	starý
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
pojedou	pojet	k5eAaPmIp3nP	pojet
na	na	k7c4	na
závěrečné	závěrečný	k2eAgNnSc4d1	závěrečné
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
The	The	k1gMnPc1	The
End	End	k1gMnSc1	End
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgNnSc2	tento
turné	turné	k1gNnSc2	turné
se	s	k7c7	s
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
údajně	údajně	k6eAd1	údajně
naposledy	naposledy	k6eAd1	naposledy
představili	představit	k5eAaPmAgMnP	představit
i	i	k9	i
před	před	k7c7	před
vyprodanou	vyprodaný	k2eAgFnSc7d1	vyprodaná
pražskou	pražský	k2eAgFnSc7d1	Pražská
O2	O2	k1gFnSc7	O2
arénou	aréna	k1gFnSc7	aréna
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
skladeb	skladba	k1gFnPc2	skladba
zaznělo	zaznět	k5eAaImAgNnS	zaznět
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
studiového	studiový	k2eAgNnSc2d1	studiové
alba	album	k1gNnSc2	album
kapely	kapela	k1gFnSc2	kapela
Paranoid	Paranoid	k1gInSc1	Paranoid
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
dva	dva	k4xCgInPc1	dva
plánované	plánovaný	k2eAgInPc1d1	plánovaný
koncerty	koncert	k1gInPc1	koncert
odehrají	odehrát	k5eAaPmIp3nP	odehrát
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2017	[number]	k4	2017
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
Genting	Genting	k1gInSc4	Genting
Arena	Areno	k1gNnSc2	Areno
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domovském	domovský	k2eAgInSc6d1	domovský
Birminghamu	Birmingham	k1gInSc6	Birmingham
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
od	od	k7c2	od
obnovení	obnovení	k1gNnSc2	obnovení
původní	původní	k2eAgFnSc2d1	původní
sestavy	sestava	k1gFnSc2	sestava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
dosud	dosud	k6eAd1	dosud
nevydali	vydat	k5eNaPmAgMnP	vydat
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
zpěváka	zpěvák	k1gMnSc2	zpěvák
Ozzyho	Ozzy	k1gMnSc2	Ozzy
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
vydal	vydat	k5eAaPmAgMnS	vydat
kompilaci	kompilace	k1gFnSc4	kompilace
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
koncertní	koncertní	k2eAgInSc4d1	koncertní
záznam	záznam	k1gInSc4	záznam
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
znovu	znovu	k6eAd1	znovu
kompilaci	kompilace	k1gFnSc4	kompilace
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
kolekci	kolekce	k1gFnSc4	kolekce
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
album	album	k1gNnSc1	album
Black	Blacka	k1gFnPc2	Blacka
Rain	Raina	k1gFnPc2	Raina
<g/>
.	.	kIx.	.
</s>
<s>
Tony	Tony	k1gMnSc1	Tony
Iommi	Iom	k1gFnPc7	Iom
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
vydal	vydat	k5eAaPmAgMnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
Iommi	Iom	k1gFnPc7	Iom
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc4	jehož
příprava	příprava	k1gFnSc1	příprava
mu	on	k3xPp3gMnSc3	on
trvala	trvat	k5eAaImAgFnS	trvat
celkem	celkem	k6eAd1	celkem
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vydal	vydat	k5eAaPmAgMnS	vydat
nahrávky	nahrávka	k1gFnPc4	nahrávka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
The	The	k1gMnSc2	The
DEP	depo	k1gNnPc2	depo
sessions	sessions	k6eAd1	sessions
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zpívá	zpívat	k5eAaImIp3nS	zpívat
Glenn	Glenn	k1gMnSc1	Glenn
Hughes	Hughes	k1gMnSc1	Hughes
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
album	album	k1gNnSc1	album
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Fused	Fused	k1gInSc4	Fused
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
začaly	začít	k5eAaPmAgFnP	začít
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Geezera	Geezer	k1gMnSc2	Geezer
Butlera	Butler	k1gMnSc2	Butler
G	G	kA	G
<g/>
/	/	kIx~	/
<g/>
Z	z	k7c2	z
<g/>
/	/	kIx~	/
<g/>
R	R	kA	R
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
album	album	k1gNnSc1	album
Plastic	Plastice	k1gFnPc2	Plastice
Planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Bill	Bill	k1gMnSc1	Bill
Ward	Ward	k1gMnSc1	Ward
vydal	vydat	k5eAaPmAgMnS	vydat
pouze	pouze	k6eAd1	pouze
singl	singl	k1gInSc4	singl
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
Straws	Straws	k1gInSc4	Straws
a	a	k8xC	a
internetový	internetový	k2eAgInSc4d1	internetový
singl	singl	k1gInSc4	singl
The	The	k1gMnSc1	The
Dark	Dark	k1gMnSc1	Dark
Hour	Hour	k1gMnSc1	Hour
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
hudbu	hudba	k1gFnSc4	hudba
je	být	k5eAaImIp3nS	být
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
na	na	k7c4	na
Black	Black	k1gInSc4	Black
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
září	zářit	k5eAaImIp3nS	zářit
2006	[number]	k4	2006
španělský	španělský	k2eAgInSc1d1	španělský
webzine	webzin	k1gInSc5	webzin
Rafabasa	Rafabasa	k1gFnSc1	Rafabasa
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tony	Tony	k1gFnSc3	Tony
Iommi	Iom	k1gFnPc7	Iom
<g/>
,	,	kIx,	,
Geezer	Geezer	k1gMnSc1	Geezer
Butler	Butler	k1gMnSc1	Butler
a	a	k8xC	a
Bill	Bill	k1gMnSc1	Bill
Ward	Ward	k1gMnSc1	Ward
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2007	[number]	k4	2007
spojit	spojit	k5eAaPmF	spojit
s	s	k7c7	s
Ronniem	Ronnium	k1gNnSc7	Ronnium
Jamesem	James	k1gMnSc7	James
Diem	Diem	k1gInSc4	Diem
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
alba	album	k1gNnSc2	album
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Dio	Dio	k1gFnSc2	Dio
Years	Yearsa	k1gFnPc2	Yearsa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
poslední	poslední	k2eAgFnSc4d1	poslední
nahrávky	nahrávka	k1gFnPc4	nahrávka
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
nové	nový	k2eAgFnPc4d1	nová
skladby	skladba	k1gFnPc4	skladba
napsané	napsaný	k2eAgFnPc4d1	napsaná
Iommim	Iommi	k1gNnSc7	Iommi
a	a	k8xC	a
Diem	Die	k1gNnSc7	Die
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Devil	Devil	k1gMnSc1	Devil
Cried	Cried	k1gMnSc1	Cried
(	(	kIx(	(
<g/>
singl	singl	k1gInSc1	singl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Shadow	Shadow	k1gMnSc1	Shadow
of	of	k?	of
the	the	k?	the
Wind	Wind	k1gMnSc1	Wind
a	a	k8xC	a
Ear	Ear	k1gMnSc1	Ear
in	in	k?	in
the	the	k?	the
Wall	Wall	k1gInSc1	Wall
<g/>
.	.	kIx.	.
</s>
<s>
Bill	Bill	k1gMnSc1	Bill
Ward	Ward	k1gMnSc1	Ward
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
projektu	projekt	k1gInSc2	projekt
nezapojí	zapojit	k5eNaPmIp3nS	zapojit
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgMnS	připojit
Vinny	vinen	k2eAgFnPc4d1	vinna
Appice	Appice	k1gFnPc4	Appice
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc4	seznam
členů	člen	k1gMnPc2	člen
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
Ozzy	Ozza	k1gFnSc2	Ozza
Osbourne	Osbourn	k1gInSc5	Osbourn
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
79	[number]	k4	79
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
Tony	Tony	k1gFnSc1	Tony
Iommi	Iom	k1gFnPc7	Iom
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
Geezer	Geezer	k1gMnSc1	Geezer
Butler	Butler	k1gMnSc1	Butler
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
Koncertní	koncertní	k2eAgMnPc1d1	koncertní
hudebníci	hudebník	k1gMnPc1	hudebník
Adam	Adam	k1gMnSc1	Adam
Wakeman	Wakeman	k1gMnSc1	Wakeman
-	-	kIx~	-
klávesy	klávesa	k1gFnSc2	klávesa
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
Tommy	Tomm	k1gInPc4	Tomm
Clufetos	Clufetos	k1gInSc1	Clufetos
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Zasedací	zasedací	k2eAgMnPc1d1	zasedací
<g/>
"	"	kIx"	"
hudebníci	hudebník	k1gMnPc1	hudebník
Brad	brada	k1gFnPc2	brada
Wilk	Wilk	k1gInSc1	Wilk
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
-	-	kIx~	-
13	[number]	k4	13
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
původní	původní	k2eAgMnPc1d1	původní
členové	člen	k1gMnPc1	člen
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
(	(	kIx(	(
<g/>
Iommi	Iom	k1gFnPc7	Iom
<g/>
,	,	kIx,	,
Butler	Butler	k1gInSc4	Butler
<g/>
,	,	kIx,	,
Osbourne	Osbourn	k1gInSc5	Osbourn
a	a	k8xC	a
Ward	Wardo	k1gNnPc2	Wardo
<g/>
)	)	kIx)	)
spojili	spojit	k5eAaPmAgMnP	spojit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zahráli	zahrát	k5eAaPmAgMnP	zahrát
na	na	k7c6	na
benefičním	benefiční	k2eAgInSc6d1	benefiční
koncertě	koncert	k1gInSc6	koncert
Live	Live	k1gNnPc2	Live
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odehráli	odehrát	k5eAaPmAgMnP	odehrát
tři	tři	k4xCgFnPc4	tři
skladby	skladba	k1gFnPc4	skladba
(	(	kIx(	(
<g/>
Children	Childrno	k1gNnPc2	Childrno
of	of	k?	of
the	the	k?	the
Grave	grave	k1gNnSc4	grave
<g/>
,	,	kIx,	,
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
a	a	k8xC	a
Paranoid	Paranoid	k1gInSc1	Paranoid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
sestava	sestava	k1gFnSc1	sestava
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1980	[number]	k4	1980
<g/>
-	-	kIx~	-
<g/>
1982	[number]	k4	1982
(	(	kIx(	(
<g/>
Iommi	Iom	k1gFnPc7	Iom
<g/>
,	,	kIx,	,
Butler	Butler	k1gMnSc1	Butler
<g/>
,	,	kIx,	,
Dio	Dio	k1gMnSc1	Dio
a	a	k8xC	a
Appice	Appice	k1gFnSc1	Appice
<g/>
)	)	kIx)	)
znovu	znovu	k6eAd1	znovu
spojila	spojit	k5eAaPmAgFnS	spojit
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
alba	album	k1gNnSc2	album
Dehumanizer	Dehumanizra	k1gFnPc2	Dehumanizra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
původní	původní	k2eAgMnPc1d1	původní
členové	člen	k1gMnPc1	člen
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
odehráli	odehrát	k5eAaPmAgMnP	odehrát
tři	tři	k4xCgFnPc4	tři
skladby	skladba	k1gFnPc4	skladba
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
Osbournových	Osbournův	k2eAgInPc2d1	Osbournův
"	"	kIx"	"
<g/>
posledních	poslední	k2eAgInPc2d1	poslední
<g/>
"	"	kIx"	"
koncertů	koncert	k1gInPc2	koncert
v	v	k7c4	v
Costa	Cost	k1gMnSc4	Cost
Mesa	Mesus	k1gMnSc4	Mesus
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
sestava	sestava	k1gFnSc1	sestava
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
(	(	kIx(	(
<g/>
Iommi	Iom	k1gFnPc7	Iom
<g/>
,	,	kIx,	,
Powell	Powell	k1gMnSc1	Powell
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
Murray	Murraa	k1gFnPc1	Murraa
a	a	k8xC	a
Nicholls	Nicholls	k1gInSc1	Nicholls
<g/>
)	)	kIx)	)
znovu	znovu	k6eAd1	znovu
spojila	spojit	k5eAaPmAgFnS	spojit
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
alba	album	k1gNnSc2	album
Forbidden	Forbiddna	k1gFnPc2	Forbiddna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
se	se	k3xPyFc4	se
původní	původní	k2eAgMnPc1d1	původní
členové	člen	k1gMnPc1	člen
znovu	znovu	k6eAd1	znovu
spojili	spojit	k5eAaPmAgMnP	spojit
<g/>
,	,	kIx,	,
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
a	a	k8xC	a
vydali	vydat	k5eAaPmAgMnP	vydat
dvoudiskové	dvoudiskový	k2eAgNnSc4d1	dvoudiskové
koncertní	koncertní	k2eAgNnSc4d1	koncertní
album	album	k1gNnSc4	album
Reunion	Reunion	k1gInSc4	Reunion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
a	a	k8xC	a
2005	[number]	k4	2005
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
spojili	spojit	k5eAaPmAgMnP	spojit
a	a	k8xC	a
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c6	na
Ozzfestu	Ozzfest	k1gInSc6	Ozzfest
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nP	šířit
fámy	fáma	k1gFnPc1	fáma
o	o	k7c6	o
vydaní	vydaný	k2eAgMnPc1d1	vydaný
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
11	[number]	k4	11
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
oficiálním	oficiální	k2eAgInSc7d1	oficiální
návratem	návrat	k1gInSc7	návrat
a	a	k8xC	a
obřím	obří	k2eAgNnSc7d1	obří
turné	turné	k1gNnSc7	turné
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
návratu	návrat	k1gInSc6	návrat
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2011	[number]	k4	2011
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
listu	list	k1gInSc6	list
Birmingham	Birmingham	k1gInSc1	Birmingham
Mirror	Mirror	k1gInSc1	Mirror
<g/>
.	.	kIx.	.
</s>
<s>
Prozradil	prozradit	k5eAaPmAgMnS	prozradit
to	ten	k3xDgNnSc4	ten
Tony	Tony	k1gMnSc1	Tony
Iommi	Iom	k1gFnPc7	Iom
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vzápětí	vzápětí	k6eAd1	vzápětí
začal	začít	k5eAaPmAgMnS	začít
rozčilovat	rozčilovat	k5eAaImF	rozčilovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nic	nic	k3yNnSc1	nic
takového	takový	k3xDgMnSc4	takový
neřekl	říct	k5eNaPmAgMnS	říct
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novinách	novina	k1gFnPc6	novina
ale	ale	k8xC	ale
doslova	doslova	k6eAd1	doslova
citovali	citovat	k5eAaBmAgMnP	citovat
Iommiho	Iommi	k1gMnSc4	Iommi
větu	věta	k1gFnSc4	věta
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Opravdu	opravdu	k6eAd1	opravdu
se	se	k3xPyFc4	se
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
těšíme	těšit	k5eAaImIp1nP	těšit
(	(	kIx(	(
<g/>
na	na	k7c4	na
návrat	návrat	k1gInSc4	návrat
<g/>
)	)	kIx)	)
a	a	k8xC	a
ty	ten	k3xDgFnPc4	ten
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
píšeme	psát	k5eAaImIp1nP	psát
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
skutečně	skutečně	k6eAd1	skutečně
dobré	dobrý	k2eAgFnPc1d1	dobrá
<g/>
,	,	kIx,	,
blíží	blížit	k5eAaImIp3nS	blížit
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
starým	starý	k2eAgFnPc3d1	stará
původním	původní	k2eAgFnPc3d1	původní
písním	píseň	k1gFnPc3	píseň
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2011	[number]	k4	2011
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
konala	konat	k5eAaImAgFnS	konat
tisková	tiskový	k2eAgFnSc1d1	tisková
konference	konference	k1gFnSc1	konference
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skupina	skupina	k1gFnSc1	skupina
konečně	konečně	k6eAd1	konečně
oficiálně	oficiálně	k6eAd1	oficiálně
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
návrat	návrat	k1gInSc4	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
internetových	internetový	k2eAgFnPc2d1	internetová
stránek	stránka	k1gFnPc2	stránka
Metal	metal	k1gInSc1	metal
Hammer	Hammer	k1gMnSc1	Hammer
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
složení	složení	k1gNnSc6	složení
Ozzy	Ozza	k1gFnSc2	Ozza
Osbourne	Osbourn	k1gInSc5	Osbourn
<g/>
,	,	kIx,	,
Tony	Tony	k1gFnSc3	Tony
Iommi	Iom	k1gFnPc7	Iom
<g/>
,	,	kIx,	,
Geezer	Geezer	k1gMnSc1	Geezer
Butler	Butler	k1gMnSc1	Butler
a	a	k8xC	a
Bill	Bill	k1gMnSc1	Bill
Ward	Warda	k1gFnPc2	Warda
navíc	navíc	k6eAd1	navíc
rozjela	rozjet	k5eAaPmAgFnS	rozjet
přípravy	příprava	k1gFnPc4	příprava
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Těšíme	těšit	k5eAaImIp1nP	těšit
se	se	k3xPyFc4	se
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
bych	by	kYmCp1nS	by
<g/>
,	,	kIx,	,
že	že	k8xS	že
věci	věc	k1gFnPc1	věc
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
píšeme	psát	k5eAaImIp1nP	psát
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
fakt	fakt	k9	fakt
dobré	dobrý	k2eAgFnPc1d1	dobrá
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
slyšet	slyšet	k5eAaImF	slyšet
Iommi	Iom	k1gFnPc7	Iom
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
deska	deska	k1gFnSc1	deska
vyjde	vyjít	k5eAaPmIp3nS	vyjít
patrně	patrně	k6eAd1	patrně
příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Dát	dát	k5eAaPmF	dát
Black	Black	k1gInSc4	Black
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
opět	opět	k6eAd1	opět
dohromady	dohromady	k6eAd1	dohromady
ovšem	ovšem	k9	ovšem
nebylo	být	k5eNaImAgNnS	být
vůbec	vůbec	k9	vůbec
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
.	.	kIx.	.
</s>
<s>
Bubeníka	Bubeník	k1gMnSc4	Bubeník
Warda	Ward	k1gMnSc4	Ward
trápí	trápit	k5eAaImIp3nS	trápit
srdce	srdce	k1gNnSc4	srdce
a	a	k8xC	a
před	před	k7c7	před
několika	několik	k4yIc7	několik
měsíci	měsíc	k1gInPc7	měsíc
navíc	navíc	k6eAd1	navíc
podstoupil	podstoupit	k5eAaPmAgInS	podstoupit
operaci	operace	k1gFnSc4	operace
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Uvidíme	uvidět	k5eAaPmIp1nP	uvidět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
teď	teď	k6eAd1	teď
bude	být	k5eAaImBp3nS	být
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
Iommi	Iom	k1gFnPc7	Iom
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
"	"	kIx"	"
<g/>
potíže	potíž	k1gFnPc1	potíž
<g/>
"	"	kIx"	"
dělal	dělat	k5eAaImAgInS	dělat
i	i	k9	i
stále	stále	k6eAd1	stále
zaneprázdněný	zaneprázdněný	k2eAgInSc1d1	zaneprázdněný
Ozzy	Ozzy	k1gInPc7	Ozzy
Osbourne	Osbourn	k1gInSc5	Osbourn
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
klasická	klasický	k2eAgFnSc1d1	klasická
hláška	hláška	k1gFnSc1	hláška
"	"	kIx"	"
<g/>
nikdy	nikdy	k6eAd1	nikdy
neříkej	říkat	k5eNaImRp2nS	říkat
nikdy	nikdy	k6eAd1	nikdy
<g/>
"	"	kIx"	"
a	a	k8xC	a
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
rozběhnout	rozběhnout	k5eAaPmF	rozběhnout
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
.	.	kIx.	.
</s>
<s>
Studiová	studiový	k2eAgFnSc1d1	studiová
alba	alba	k1gFnSc1	alba
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Paranoid	Paranoid	k1gInSc1	Paranoid
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Master	master	k1gMnSc1	master
of	of	k?	of
Reality	realita	k1gFnSc2	realita
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
4	[number]	k4	4
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Sabbath	Sabbath	k1gMnSc1	Sabbath
Bloody	Blooda	k1gFnSc2	Blooda
Sabbath	Sabbath	k1gMnSc1	Sabbath
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Sabotage	Sabotag	k1gInSc2	Sabotag
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Technical	Technical	k1gFnSc2	Technical
Ecstasy	Ecstasa	k1gFnSc2	Ecstasa
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
Never	Never	k1gMnSc1	Never
Say	Say	k1gMnSc1	Say
Die	Die	k1gMnSc1	Die
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
Heaven	Heaven	k2eAgInSc1d1	Heaven
and	and	k?	and
Hell	Hell	k1gInSc1	Hell
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
Mob	Mob	k1gMnSc1	Mob
Rules	Rules	k1gMnSc1	Rules
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
Born	Born	k1gMnSc1	Born
Again	Again	k1gMnSc1	Again
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
Seventh	Seventh	k1gInSc1	Seventh
Star	Star	kA	Star
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Eternal	Eternal	k1gMnSc1	Eternal
Idol	idol	k1gInSc1	idol
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Headless	Headless	k1gInSc1	Headless
Cross	Cross	k1gInSc1	Cross
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Tyr	Tyr	k1gFnSc1	Tyr
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Dehumanizer	Dehumanizra	k1gFnPc2	Dehumanizra
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Cross	Cross	k1gInSc1	Cross
Purposes	Purposes	k1gInSc1	Purposes
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Forbidden	Forbiddna	k1gFnPc2	Forbiddna
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
13	[number]	k4	13
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Autorizovaná	autorizovaný	k2eAgFnSc1d1	autorizovaná
informační	informační	k2eAgFnSc1d1	informační
stránka	stránka	k1gFnSc1	stránka
Black	Black	k1gInSc4	Black
Sabbath	Sabbath	k1gInSc1	Sabbath
Live	Liv	k1gInSc2	Liv
Project	Project	k2eAgInSc1d1	Project
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
Tonyho	Tony	k1gMnSc2	Tony
Iommiho	Iommi	k1gMnSc2	Iommi
Stránky	stránka	k1gFnSc2	stránka
o	o	k7c4	o
heavy	heava	k1gFnPc4	heava
metalové	metalový	k2eAgFnSc3d1	metalová
legendě	legenda	k1gFnSc3	legenda
Ozzy	Ozza	k1gFnSc2	Ozza
Osbourne	Osbourn	k1gInSc5	Osbourn
</s>
