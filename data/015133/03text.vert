<s>
Kanton	Kanton	k1gInSc1
Nérac	Nérac	k1gInSc1
</s>
<s>
Kanton	Kanton	k1gInSc1
Nérac	Nérac	k1gInSc1
Kanton	Kanton	k1gInSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
arrondissementu	arrondissement	k1gInSc2
Nérac	Nérac	k1gInSc4
Stát	stát	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
Region	region	k1gInSc1
</s>
<s>
Akvitánie	Akvitánie	k1gFnSc1
Departement	departement	k1gInSc1
</s>
<s>
Lot-et-Garonne	Lot-et-Garonnout	k5eAaPmIp3nS,k5eAaImIp3nS
Arrondissement	Arrondissement	k1gInSc1
</s>
<s>
Nérac	Nérac	k6eAd1
Počet	počet	k1gInSc1
obcí	obec	k1gFnPc2
</s>
<s>
8	#num#	k4
Sídlo	sídlo	k1gNnSc4
správy	správa	k1gFnSc2
</s>
<s>
Nérac	Nérac	k1gFnSc1
Rozloha	rozloha	k1gFnSc1
</s>
<s>
165,59	165,59	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
9	#num#	k4
820	#num#	k4
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
59,3	59,3	k4
ob.	ob.	k?
<g/>
/	/	kIx~
<g/>
km²	km²	k?
</s>
<s>
Kanton	Kanton	k1gInSc1
Nérac	Nérac	k1gInSc1
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
Canton	Canton	k1gInSc1
de	de	k?
Nérac	Nérac	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
francouzský	francouzský	k2eAgInSc4d1
kanton	kanton	k1gInSc4
v	v	k7c6
departementu	departement	k1gInSc6
Lot-et-Garonne	Lot-et-Garonn	k1gInSc5
v	v	k7c6
regionu	region	k1gInSc6
Akvitánie	Akvitánie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
ho	on	k3xPp3gInSc4
osm	osm	k4xCc4
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Obce	obec	k1gFnPc1
kantonu	kanton	k1gInSc2
</s>
<s>
Andiran	Andiran	k1gInSc1
</s>
<s>
Calignac	Calignac	k6eAd1
</s>
<s>
Espiens	Espiens	k6eAd1
</s>
<s>
Fréchou	Frécha	k1gFnSc7
</s>
<s>
Moncaut	Moncaut	k1gMnSc1
</s>
<s>
Montagnac-sur-Auvignon	Montagnac-sur-Auvignon	k1gMnSc1
</s>
<s>
Nérac	Nérac	k6eAd1
</s>
<s>
Saumont	Saumont	k1gMnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kantony	Kanton	k1gInPc1
v	v	k7c6
departementu	departement	k1gInSc6
Lot-et-Garonne	Lot-et-Garonn	k1gMnSc5
</s>
<s>
Agen-Centre	Agen-Centr	k1gMnSc5
•	•	k?
Agen-Nord	Agen-Nord	k1gMnSc1
•	•	k?
Agen-Nord-Est	Agen-Nord-Est	k1gMnSc1
•	•	k?
Agen-Ouest	Agen-Ouest	k1gMnSc1
•	•	k?
Agen-Sud-Est	Agen-Sud-Est	k1gMnSc1
•	•	k?
Astaffort	Astaffort	k1gInSc1
•	•	k?
Beauville	Beauville	k1gInSc1
•	•	k?
Bouglon	Bouglon	k1gInSc1
•	•	k?
Cancon	Cancon	k1gInSc1
•	•	k?
Casteljaloux	Casteljaloux	k1gInSc1
•	•	k?
Castelmoron-sur-Lot	Castelmoron-sur-Lot	k1gInSc1
•	•	k?
Castillonnè	Castillonnè	k1gInSc1
•	•	k?
Damazan	Damazan	k1gMnSc1
•	•	k?
Duras	Duras	k1gMnSc1
•	•	k?
Francescas	Francescas	k1gMnSc1
•	•	k?
Fumel	Fumel	k1gMnSc1
•	•	k?
Houeillè	Houeillè	k1gInSc1
•	•	k?
Laplume	Laplum	k1gInSc5
•	•	k?
Laroque-Timbaut	Laroque-Timbaut	k2eAgInSc1d1
•	•	k?
Lauzun	Lauzun	k1gInSc1
•	•	k?
Lavardac	Lavardac	k1gInSc1
•	•	k?
Marmande-Est	Marmande-Est	k1gMnSc1
•	•	k?
Marmande-Ouest	Marmande-Ouest	k1gMnSc1
•	•	k?
Le	Le	k1gMnSc1
Mas-d	Mas-d	k1gMnSc1
<g/>
'	'	kIx"
<g/>
Agenais	Agenais	k1gFnSc1
•	•	k?
Meilhan-sur-Garonne	Meilhan-sur-Garonn	k1gInSc5
•	•	k?
Mézin	Mézin	k1gMnSc1
•	•	k?
Monclar	Monclar	k1gMnSc1
•	•	k?
Monflanquin	Monflanquin	k1gMnSc1
•	•	k?
Nérac	Nérac	k1gInSc1
•	•	k?
Penne-d	Penne-d	k1gInSc1
<g/>
'	'	kIx"
<g/>
Agenais	Agenais	k1gInSc1
•	•	k?
Port-Sainte-Marie	Port-Sainte-Marie	k1gFnSc2
•	•	k?
Prayssas	Prayssas	k1gInSc1
•	•	k?
Puymirol	Puymirol	k1gInSc1
•	•	k?
Sainte-Livrade-sur-Lot	Sainte-Livrade-sur-Lot	k1gInSc1
•	•	k?
Seyches	Seyches	k1gInSc1
•	•	k?
Tonneins	Tonneins	k1gInSc1
•	•	k?
Tournon-d	Tournon-d	k1gInSc1
<g/>
'	'	kIx"
<g/>
Agenais	Agenais	k1gFnPc1
•	•	k?
Villeneuve-sur-Lot-Nord	Villeneuve-sur-Lot-Nord	k1gMnSc1
•	•	k?
Villeneuve-sur-Lot-Sud	Villeneuve-sur-Lot-Sud	k1gMnSc1
•	•	k?
Villeréal	Villeréal	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
</s>
