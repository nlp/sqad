<p>
<s>
Kaskáda	kaskáda	k1gFnSc1	kaskáda
je	být	k5eAaImIp3nS	být
stupňovitý	stupňovitý	k2eAgInSc1d1	stupňovitý
vodopád	vodopád	k1gInSc1	vodopád
či	či	k8xC	či
řada	řada	k1gFnSc1	řada
stupňovitých	stupňovitý	k2eAgInPc2d1	stupňovitý
vodopádů	vodopád	k1gInPc2	vodopád
<g/>
,	,	kIx,	,
přirozených	přirozený	k2eAgNnPc2d1	přirozené
nebo	nebo	k8xC	nebo
umělých	umělý	k2eAgNnPc2d1	umělé
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodohospodářství	vodohospodářství	k1gNnSc6	vodohospodářství
je	být	k5eAaImIp3nS	být
kaskáda	kaskáda	k1gFnSc1	kaskáda
soustava	soustava	k1gFnSc1	soustava
vodních	vodní	k2eAgFnPc2d1	vodní
přehrad	přehrada	k1gFnPc2	přehrada
nebo	nebo	k8xC	nebo
vyšších	vysoký	k2eAgInPc2d2	vyšší
jezů	jez	k1gInPc2	jez
využívající	využívající	k2eAgFnSc2d1	využívající
vodní	vodní	k2eAgFnSc2d1	vodní
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
Vltavská	vltavský	k2eAgFnSc1d1	Vltavská
vodní	vodní	k2eAgFnSc1d1	vodní
kaskáda	kaskáda	k1gFnSc1	kaskáda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
stupňovitá	stupňovitý	k2eAgFnSc1d1	stupňovitá
svodnice	svodnice	k1gFnSc1	svodnice
přelivu	přeliv	k1gInSc2	přeliv
(	(	kIx(	(
<g/>
přepadu	přepad	k1gInSc2	přepad
<g/>
)	)	kIx)	)
u	u	k7c2	u
nádržné	nádržný	k2eAgFnSc2d1	nádržný
hráze	hráz	k1gFnSc2	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
další	další	k2eAgInPc4d1	další
<g/>
,	,	kIx,	,
přenesené	přenesený	k2eAgInPc4d1	přenesený
významy	význam	k1gInPc4	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vodáckém	vodácký	k2eAgNnSc6d1	vodácké
názvosloví	názvosloví	k1gNnSc6	názvosloví
se	se	k3xPyFc4	se
slovem	slovem	k6eAd1	slovem
kaskáda	kaskáda	k1gFnSc1	kaskáda
obvykle	obvykle	k6eAd1	obvykle
označuje	označovat	k5eAaImIp3nS	označovat
množina	množina	k1gFnSc1	množina
přírodních	přírodní	k2eAgFnPc2d1	přírodní
či	či	k8xC	či
umělých	umělý	k2eAgFnPc2d1	umělá
vodních	vodní	k2eAgFnPc2d1	vodní
překážek	překážka	k1gFnPc2	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
vodáckého	vodácký	k2eAgInSc2d1	vodácký
sportu	sport	k1gInSc2	sport
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgFnSc1d1	přírodní
kaskáda	kaskáda	k1gFnSc1	kaskáda
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
voda	voda	k1gFnSc1	voda
překonává	překonávat	k5eAaImIp3nS	překonávat
překážky	překážka	k1gFnPc4	překážka
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
skalnatých	skalnatý	k2eAgInPc2d1	skalnatý
prahů	práh	k1gInPc2	práh
a	a	k8xC	a
malých	malý	k2eAgInPc2d1	malý
nebo	nebo	k8xC	nebo
rozrušených	rozrušený	k2eAgInPc2d1	rozrušený
vodopádů	vodopád	k1gInPc2	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
peřeje	peřej	k1gFnSc2	peřej
se	se	k3xPyFc4	se
kaskáda	kaskáda	k1gFnSc1	kaskáda
liší	lišit	k5eAaImIp3nS	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
překonává	překonávat	k5eAaImIp3nS	překonávat
vyšší	vysoký	k2eAgInSc1d2	vyšší
výškový	výškový	k2eAgInSc1d1	výškový
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
,	,	kIx,	,
od	od	k7c2	od
vodopádu	vodopád	k1gInSc2	vodopád
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgFnSc4d2	veliký
délku	délka	k1gFnSc4	délka
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
kaskády	kaskáda	k1gFnPc1	kaskáda
jsou	být	k5eAaImIp3nP	být
sjízdné	sjízdný	k2eAgInPc4d1	sjízdný
pro	pro	k7c4	pro
kajaky	kajak	k1gInPc4	kajak
<g/>
,	,	kIx,	,
rafty	rafta	k1gFnPc4	rafta
nebo	nebo	k8xC	nebo
nafukovací	nafukovací	k2eAgNnSc4d1	nafukovací
kanoe	kanoe	k1gNnSc4	kanoe
určené	určený	k2eAgNnSc4d1	určené
pro	pro	k7c4	pro
divokou	divoký	k2eAgFnSc4d1	divoká
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
kaskády	kaskáda	k1gFnPc1	kaskáda
např.	např.	kA	např.
na	na	k7c6	na
Bílém	bílý	k2eAgNnSc6d1	bílé
Labi	Labe	k1gNnSc6	Labe
<g/>
,	,	kIx,	,
Bílé	bílý	k2eAgFnSc6d1	bílá
Opavě	Opava	k1gFnSc6	Opava
<g/>
,	,	kIx,	,
vodácky	vodácky	k6eAd1	vodácky
zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
je	být	k5eAaImIp3nS	být
kaskáda	kaskáda	k1gFnSc1	kaskáda
na	na	k7c4	na
Ostravici	Ostravice	k1gFnSc4	Ostravice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
==	==	k?	==
</s>
</p>
<p>
<s>
v	v	k7c6	v
elektrotechnice	elektrotechnika	k1gFnSc6	elektrotechnika
–	–	k?	–
řada	řada	k1gFnSc1	řada
strojů	stroj	k1gInPc2	stroj
nebo	nebo	k8xC	nebo
zařízení	zařízení	k1gNnPc2	zařízení
zapojených	zapojený	k2eAgMnPc2d1	zapojený
za	za	k7c7	za
sebou	se	k3xPyFc7	se
v	v	k7c6	v
sérii	série	k1gFnSc6	série
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
v	v	k7c6	v
elektrice	elektrika	k1gFnSc6	elektrika
–	–	k?	–
stupňovité	stupňovitý	k2eAgNnSc4d1	stupňovité
zapojení	zapojení	k1gNnSc4	zapojení
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
kaskáda	kaskáda	k1gFnSc1	kaskáda
měřicích	měřicí	k2eAgInPc2d1	měřicí
transformátorů	transformátor	k1gInPc2	transformátor
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
artistická	artistický	k2eAgFnSc1d1	artistická
produkce	produkce	k1gFnSc1	produkce
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
rychlých	rychlý	k2eAgInPc2d1	rychlý
a	a	k8xC	a
nebezpečných	bezpečný	k2eNgInPc2d1	nebezpečný
skoků	skok	k1gInPc2	skok
<g/>
,	,	kIx,	,
přemetů	přemet	k1gInPc2	přemet
a	a	k8xC	a
pádů	pád	k1gInPc2	pád
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
s	s	k7c7	s
komickými	komický	k2eAgInPc7d1	komický
prvky	prvek	k1gInPc7	prvek
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
knižně	knižně	k6eAd1	knižně
–	–	k?	–
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
představu	představa	k1gFnSc4	představa
mnohosti	mnohost	k1gFnSc2	mnohost
<g/>
,	,	kIx,	,
přívalu	příval	k1gInSc2	příval
<g/>
,	,	kIx,	,
prudkého	prudký	k2eAgInSc2d1	prudký
stupňovitého	stupňovitý	k2eAgInSc2d1	stupňovitý
proudu	proud	k1gInSc2	proud
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
kaskáda	kaskáda	k1gFnSc1	kaskáda
jisker	jiskra	k1gFnPc2	jiskra
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kaskáda	kaskáda	k1gFnSc1	kaskáda
tónů	tón	k1gInPc2	tón
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kaskáda	kaskáda	k1gFnSc1	kaskáda
smíchu	smích	k1gInSc2	smích
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kaskáda	kaskáda	k1gFnSc1	kaskáda
veršů	verš	k1gInPc2	verš
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kaskáda	kaskáda	k1gFnSc1	kaskáda
not	nota	k1gFnPc2	nota
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kaskáda	kaskáda	k1gFnSc1	kaskáda
barev	barva	k1gFnPc2	barva
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
kaskáda	kaskáda	k1gFnSc1	kaskáda
roucha	roucho	k1gNnSc2	roucho
<g/>
"	"	kIx"	"
aj.	aj.	kA	aj.
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
Kemblova	Kemblův	k2eAgFnSc1d1	Kemblův
kaskáda	kaskáda	k1gFnSc1	kaskáda
<g/>
,	,	kIx,	,
Golfový	golfový	k2eAgInSc1d1	golfový
areál	areál	k1gInSc1	areál
Kaskáda	kaskáda	k1gFnSc1	kaskáda
<g/>
,	,	kIx,	,
signalizační	signalizační	k2eAgFnSc1d1	signalizační
kaskáda	kaskáda	k1gFnSc1	kaskáda
<g/>
,	,	kIx,	,
Travertinová	travertinový	k2eAgFnSc1d1	travertinová
kaskáda	kaskáda	k1gFnSc1	kaskáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Peřej	peřej	k1gFnSc1	peřej
</s>
</p>
<p>
<s>
Vodopád	vodopád	k1gInSc1	vodopád
</s>
</p>
<p>
<s>
Katarakt	katarakt	k1gInSc1	katarakt
</s>
</p>
<p>
<s>
Říční	říční	k2eAgInSc1d1	říční
práh	práh	k1gInSc1	práh
</s>
</p>
<p>
<s>
Přehradní	přehradní	k2eAgFnSc1d1	přehradní
hráz	hráz	k1gFnSc1	hráz
</s>
</p>
<p>
<s>
Přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
</s>
</p>
<p>
<s>
Jez	jez	k1gInSc1	jez
</s>
</p>
<p>
<s>
Klasifikace	klasifikace	k1gFnSc1	klasifikace
vodácké	vodácký	k2eAgFnSc2d1	vodácká
obtížnosti	obtížnost	k1gFnSc2	obtížnost
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kaskáda	kaskáda	k1gFnSc1	kaskáda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
vodní	vodní	k2eAgFnSc1d1	vodní
kaskáda	kaskáda	k1gFnSc1	kaskáda
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
http://prirucka.ujc.cas.cz/?slovo=kask%C3%A1da	[url]	k1gFnSc1	http://prirucka.ujc.cas.cz/?slovo=kask%C3%A1da
slovníky	slovník	k1gInPc1	slovník
<g/>
:	:	kIx,	:
ASCS	ASCS	kA	ASCS
<g/>
,	,	kIx,	,
SSČ	SSČ	kA	SSČ
<g/>
,	,	kIx,	,
SSJČ	SSJČ	kA	SSJČ
</s>
</p>
