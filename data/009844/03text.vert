<p>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
je	být	k5eAaImIp3nS	být
tvrz	tvrz	k1gFnSc1	tvrz
přestavěná	přestavěný	k2eAgFnSc1d1	přestavěná
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgNnSc6d1	stejnojmenné
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
čtrnáctého	čtrnáctý	k4xOgNnSc2	čtrnáctý
století	století	k1gNnSc2	století
stávala	stávat	k5eAaImAgFnS	stávat
v	v	k7c6	v
těsném	těsný	k2eAgNnSc6d1	těsné
sousedství	sousedství	k1gNnSc6	sousedství
mladšího	mladý	k2eAgInSc2d2	mladší
zámku	zámek	k1gInSc2	zámek
gotická	gotický	k2eAgFnSc1d1	gotická
tvrz	tvrz	k1gFnSc1	tvrz
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
hlavní	hlavní	k2eAgNnSc4d1	hlavní
sídlo	sídlo	k1gNnSc4	sídlo
rodu	rod	k1gInSc2	rod
Kladenských	kladenský	k2eAgInPc2d1	kladenský
z	z	k7c2	z
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vymření	vymření	k1gNnSc6	vymření
kladenské	kladenský	k2eAgFnSc2d1	kladenská
větve	větev	k1gFnSc2	větev
rodu	rod	k1gInSc2	rod
tvrz	tvrz	k1gFnSc1	tvrz
zdědili	zdědit	k5eAaPmAgMnP	zdědit
Žďárští	Žďárský	k1gMnPc1	Žďárský
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
šestnáctého	šestnáctý	k4xOgNnSc2	šestnáctý
století	století	k1gNnSc2	století
přestavěli	přestavět	k5eAaPmAgMnP	přestavět
na	na	k7c4	na
renesanční	renesanční	k2eAgInSc4d1	renesanční
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
podoba	podoba	k1gFnSc1	podoba
zámku	zámek	k1gInSc2	zámek
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
barokní	barokní	k2eAgFnSc2d1	barokní
přestavby	přestavba	k1gFnSc2	přestavba
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
osmnáctého	osmnáctý	k4xOgNnSc2	osmnáctý
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
Kilián	Kilián	k1gMnSc1	Kilián
Ignác	Ignác	k1gMnSc1	Ignác
Dientzenhofer	Dientzenhofer	k1gMnSc1	Dientzenhofer
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
Zádušní	zádušní	k2eAgFnSc6d1	zádušní
ulici	ulice	k1gFnSc6	ulice
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
kladenského	kladenský	k2eAgInSc2d1	kladenský
zámku	zámek	k1gInSc2	zámek
byla	být	k5eAaImAgFnS	být
gotická	gotický	k2eAgFnSc1d1	gotická
tvrz	tvrz	k1gFnSc1	tvrz
ze	z	k7c2	z
čtrnáctého	čtrnáctý	k4xOgNnSc2	čtrnáctý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
patřilo	patřit	k5eAaImAgNnS	patřit
rozvětvenému	rozvětvený	k2eAgInSc3d1	rozvětvený
rodu	rod	k1gInSc3	rod
Kladenských	kladenský	k2eAgFnPc2d1	kladenská
z	z	k7c2	z
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
na	na	k7c6	na
území	území	k1gNnSc6	území
Kladna	Kladno	k1gNnSc2	Kladno
tři	tři	k4xCgNnPc4	tři
sídla	sídlo	k1gNnPc4	sídlo
<g/>
:	:	kIx,	:
Dolejší	Dolejší	k1gFnSc1	Dolejší
tvrz	tvrz	k1gFnSc1	tvrz
<g/>
,	,	kIx,	,
Hořejší	Hořejší	k2eAgFnSc4d1	Hořejší
tvrz	tvrz	k1gFnSc4	tvrz
a	a	k8xC	a
Vlaškovu	Vlaškův	k2eAgFnSc4d1	Vlaškův
tvrz	tvrz	k1gFnSc4	tvrz
<g/>
.	.	kIx.	.
</s>
<s>
Původním	původní	k2eAgNnSc7d1	původní
rodovým	rodový	k2eAgNnSc7d1	rodové
sídlem	sídlo	k1gNnSc7	sídlo
byla	být	k5eAaImAgFnS	být
Hořejší	Hořejší	k2eAgFnSc1d1	Hořejší
tvrz	tvrz	k1gFnSc1	tvrz
<g/>
,	,	kIx,	,
připomínaná	připomínaný	k2eAgFnSc1d1	připomínaná
poprvé	poprvé	k6eAd1	poprvé
roku	rok	k1gInSc2	rok
1453	[number]	k4	1453
za	za	k7c4	za
Přecha	Přech	k1gMnSc4	Přech
z	z	k7c2	z
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
Stávala	stávat	k5eAaImAgFnS	stávat
v	v	k7c6	v
zámecké	zámecký	k2eAgFnSc6d1	zámecká
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
asi	asi	k9	asi
dvacet	dvacet	k4xCc1	dvacet
metrů	metr	k1gInPc2	metr
západně	západně	k6eAd1	západně
od	od	k7c2	od
zámku	zámek	k1gInSc2	zámek
dochovaly	dochovat	k5eAaPmAgInP	dochovat
gotické	gotický	k2eAgInPc1d1	gotický
sklepy	sklep	k1gInPc1	sklep
<g/>
.	.	kIx.	.
<g/>
Přech	Přech	k1gInSc1	Přech
z	z	k7c2	z
Kladna	Kladno	k1gNnSc2	Kladno
dokázal	dokázat	k5eAaPmAgMnS	dokázat
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1453	[number]	k4	1453
získat	získat	k5eAaPmF	získat
většinu	většina	k1gFnSc4	většina
Kladna	Kladno	k1gNnSc2	Kladno
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
tvrzemi	tvrz	k1gFnPc7	tvrz
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
patřila	patřit	k5eAaImAgFnS	patřit
nejprve	nejprve	k6eAd1	nejprve
k	k	k7c3	k
buštěhradskému	buštěhradský	k2eAgNnSc3d1	Buštěhradské
panství	panství	k1gNnSc3	panství
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1530	[number]	k4	1530
<g/>
–	–	k?	–
<g/>
1540	[number]	k4	1540
byla	být	k5eAaImAgFnS	být
připojena	připojit	k5eAaPmNgFnS	připojit
ke	k	k7c3	k
Smečnu	Smečno	k1gNnSc3	Smečno
<g/>
.	.	kIx.	.
</s>
<s>
Přech	Přech	k1gMnSc1	Přech
z	z	k7c2	z
Kladna	Kladno	k1gNnSc2	Kladno
zemřel	zemřít	k5eAaPmAgMnS	zemřít
nejpozději	pozdě	k6eAd3	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1474	[number]	k4	1474
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
panství	panství	k1gNnSc4	panství
zdědil	zdědit	k5eAaPmAgMnS	zdědit
syn	syn	k1gMnSc1	syn
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
z	z	k7c2	z
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgFnPc2d1	jiná
vesnic	vesnice	k1gFnPc2	vesnice
mu	on	k3xPp3gMnSc3	on
patřily	patřit	k5eAaImAgInP	patřit
dvě	dva	k4xCgFnPc4	dva
kladenské	kladenský	k2eAgFnPc4d1	kladenská
tvrze	tvrz	k1gFnPc4	tvrz
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
poplužními	poplužní	k2eAgInPc7d1	poplužní
dvory	dvůr	k1gInPc7	dvůr
<g/>
,	,	kIx,	,
Tuchlovice	Tuchlovice	k1gFnSc1	Tuchlovice
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Dobrá	dobrá	k1gFnSc1	dobrá
nebo	nebo	k8xC	nebo
Doksy	Doksy	k1gInPc1	Doksy
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
posledním	poslední	k2eAgMnSc7d1	poslední
mužským	mužský	k2eAgMnSc7d1	mužský
členem	člen	k1gMnSc7	člen
kladenské	kladenský	k2eAgFnSc2d1	kladenská
větve	větev	k1gFnSc2	větev
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
Marie	Marie	k1gFnSc1	Marie
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
Jana	Jan	k1gMnSc2	Jan
Jiřího	Jiří	k1gMnSc2	Jiří
Žďárského	Žďárský	k1gMnSc2	Žďárský
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
proto	proto	k8xC	proto
většinu	většina	k1gFnSc4	většina
majetku	majetek	k1gInSc2	majetek
roku	rok	k1gInSc2	rok
1542	[number]	k4	1542
odkázal	odkázat	k5eAaPmAgInS	odkázat
sestřinu	sestřin	k2eAgMnSc3d1	sestřin
synovi	syn	k1gMnSc3	syn
Oldřichovi	Oldřich	k1gMnSc3	Oldřich
Žďárskému	Žďárský	k1gMnSc3	Žďárský
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc3	jeho
dětem	dítě	k1gFnPc3	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
pohřben	pohřben	k2eAgMnSc1d1	pohřben
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
kladenském	kladenský	k2eAgInSc6d1	kladenský
kostele	kostel	k1gInSc6	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Žďárský	Žďárský	k1gMnSc1	Žďárský
však	však	k9	však
zemřel	zemřít	k5eAaPmAgMnS	zemřít
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1542	[number]	k4	1542
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
majetek	majetek	k1gInSc1	majetek
tak	tak	k6eAd1	tak
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
syny	syn	k1gMnPc4	syn
Jana	Jan	k1gMnSc2	Jan
<g/>
,	,	kIx,	,
Jiřího	Jiří	k1gMnSc2	Jiří
a	a	k8xC	a
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
drželi	držet	k5eAaImAgMnP	držet
Kladno	Kladno	k1gNnSc4	Kladno
v	v	k7c6	v
nedílu	nedíl	k1gInSc6	nedíl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
1548	[number]	k4	1548
se	se	k3xPyFc4	se
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgFnPc1	dva
kladenské	kladenský	k2eAgFnPc1d1	kladenská
tvrze	tvrz	k1gFnPc1	tvrz
se	se	k3xPyFc4	se
dvory	dvůr	k1gInPc7	dvůr
<g/>
,	,	kIx,	,
pivovarem	pivovar	k1gInSc7	pivovar
v	v	k7c6	v
Dolejší	Dolejší	k2eAgFnSc6d1	Dolejší
tvrzi	tvrz	k1gFnSc6	tvrz
<g/>
,	,	kIx,	,
vsi	ves	k1gFnPc1	ves
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
,	,	kIx,	,
Motyčín	Motyčín	k1gInSc1	Motyčín
<g/>
,	,	kIx,	,
Cvrčovice	Cvrčovice	k1gFnPc1	Cvrčovice
<g/>
,	,	kIx,	,
Kročehlavy	Kročehlava	k1gFnPc1	Kročehlava
<g/>
,	,	kIx,	,
Újezdec	Újezdec	k1gMnSc1	Újezdec
pod	pod	k7c7	pod
Kladnem	Kladno	k1gNnSc7	Kladno
<g/>
,	,	kIx,	,
Unhošť	Unhošť	k1gFnSc1	Unhošť
<g/>
,	,	kIx,	,
Hřebeč	Hřebeč	k1gInSc1	Hřebeč
a	a	k8xC	a
Žehrovice	Žehrovice	k1gFnSc1	Žehrovice
dostal	dostat	k5eAaPmAgMnS	dostat
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
však	však	k9	však
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc2	jeho
panství	panství	k1gNnPc2	panství
zdědil	zdědit	k5eAaPmAgMnS	zdědit
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Žďárský	Žďárský	k1gMnSc1	Žďárský
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1542	[number]	k4	1542
<g/>
–	–	k?	–
<g/>
1543	[number]	k4	1543
účastnil	účastnit	k5eAaImAgInS	účastnit
válek	válek	k1gInSc1	válek
s	s	k7c7	s
Turky	turek	k1gInPc7	turek
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
a	a	k8xC	a
roku	rok	k1gInSc6	rok
1564	[number]	k4	1564
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
hejtmana	hejtman	k1gMnSc2	hejtman
Slánského	Slánský	k1gMnSc2	Slánský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
tvrz	tvrz	k1gFnSc1	tvrz
mu	on	k3xPp3gMnSc3	on
již	již	k6eAd1	již
nevyhovovala	vyhovovat	k5eNaImAgFnS	vyhovovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
ji	on	k3xPp3gFnSc4	on
ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
až	až	k9	až
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
letech	léto	k1gNnPc6	léto
šestnáctého	šestnáctý	k4xOgNnSc2	šestnáctý
století	století	k1gNnSc2	století
nechal	nechat	k5eAaPmAgInS	nechat
přestavět	přestavět	k5eAaPmF	přestavět
na	na	k7c4	na
renesanční	renesanční	k2eAgInSc4d1	renesanční
zámek	zámek	k1gInSc4	zámek
dokončený	dokončený	k2eAgInSc1d1	dokončený
roku	rok	k1gInSc2	rok
1566	[number]	k4	1566
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
neměl	mít	k5eNaImAgMnS	mít
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
panství	panství	k1gNnSc4	panství
odkázal	odkázat	k5eAaPmAgInS	odkázat
bratrovi	bratr	k1gMnSc3	bratr
Janu	Jan	k1gMnSc3	Jan
Žďárskému	Žďárský	k1gMnSc3	Žďárský
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
Janově	Janův	k2eAgFnSc6d1	Janova
smrti	smrt	k1gFnSc6	smrt
je	on	k3xPp3gNnSc4	on
získá	získat	k5eAaPmIp3nS	získat
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Ctibor	Ctibor	k1gMnSc1	Ctibor
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
po	po	k7c6	po
Janově	Janův	k2eAgFnSc6d1	Janova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1578	[number]	k4	1578
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
proti	proti	k7c3	proti
se	se	k3xPyFc4	se
postavili	postavit	k5eAaPmAgMnP	postavit
Ctiborovi	Ctiborův	k2eAgMnPc1d1	Ctiborův
bratři	bratr	k1gMnPc1	bratr
Gothart	Gothart	k1gInSc4	Gothart
Florián	Florián	k1gMnSc1	Florián
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Vok.	Vok.	k1gFnSc2	Vok.
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1578	[number]	k4	1578
proto	proto	k8xC	proto
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
se	s	k7c7	s
Ctiborem	Ctibor	k1gMnSc7	Ctibor
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
každému	každý	k3xTgInSc3	každý
vyplatí	vyplatit	k5eAaPmIp3nS	vyplatit
sedm	sedm	k4xCc1	sedm
tisíc	tisíc	k4xCgInPc2	tisíc
kop	kopa	k1gFnPc2	kopa
míšeňských	míšeňský	k2eAgInPc2d1	míšeňský
grošů	groš	k1gInPc2	groš
<g/>
.	.	kIx.	.
<g/>
Ctibor	Ctibor	k1gMnSc1	Ctibor
Žďárský	Žďárský	k1gMnSc1	Žďárský
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
roku	rok	k1gInSc2	rok
1564	[number]	k4	1564
vojenského	vojenský	k2eAgNnSc2d1	vojenské
tažení	tažení	k1gNnSc2	tažení
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pobýval	pobývat	k5eAaImAgMnS	pobývat
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Tyrolského	tyrolský	k2eAgMnSc2d1	tyrolský
a	a	k8xC	a
po	po	k7c6	po
korunovaci	korunovace	k1gFnSc6	korunovace
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
často	často	k6eAd1	často
cestoval	cestovat	k5eAaImAgMnS	cestovat
jako	jako	k9	jako
diplomat	diplomat	k1gMnSc1	diplomat
především	především	k6eAd1	především
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1598	[number]	k4	1598
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
hejtmana	hejtman	k1gMnSc2	hejtman
malostranského	malostranský	k2eAgMnSc2d1	malostranský
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1612	[number]	k4	1612
<g/>
–	–	k?	–
<g/>
1613	[number]	k4	1613
purkrabího	purkrabí	k1gMnSc2	purkrabí
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
za	za	k7c4	za
rytířský	rytířský	k2eAgInSc4d1	rytířský
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Sibylou	Sibyla	k1gFnSc7	Sibyla
Hradištskou	Hradištský	k2eAgFnSc4d1	Hradištská
z	z	k7c2	z
Hořovic	Hořovice	k1gFnPc2	Hořovice
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
sedmnáctého	sedmnáctý	k4xOgNnSc2	sedmnáctý
století	století	k1gNnSc2	století
zřídili	zřídit	k5eAaPmAgMnP	zřídit
v	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
lazaret	lazaret	k1gInSc4	lazaret
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
učitele	učitel	k1gMnPc4	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1615	[number]	k4	1615
<g/>
.	.	kIx.	.
</s>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
zdědil	zdědit	k5eAaPmAgMnS	zdědit
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
Jiří	Jiří	k1gMnSc1	Jiří
Žďárský	Žďárský	k1gMnSc1	Žďárský
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
a	a	k8xC	a
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
byl	být	k5eAaImAgInS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
účasti	účast	k1gFnSc2	účast
na	na	k7c6	na
vzpouře	vzpoura	k1gFnSc6	vzpoura
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
díky	díky	k7c3	díky
přímluvě	přímluva	k1gFnSc3	přímluva
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
zproštěn	zprostit	k5eAaPmNgMnS	zprostit
obžaloby	obžaloba	k1gFnSc2	obžaloba
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
bezdětný	bezdětný	k2eAgMnSc1d1	bezdětný
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1626	[number]	k4	1626
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1619	[number]	k4	1619
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
uložen	uložit	k5eAaPmNgInS	uložit
archiv	archiv	k1gInSc1	archiv
ostrovského	ostrovský	k2eAgInSc2d1	ostrovský
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
neklidné	klidný	k2eNgFnSc6d1	neklidná
době	doba	k1gFnSc6	doba
v	v	k7c6	v
bezpečí	bezpečí	k1gNnSc6	bezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
již	již	k9	již
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1620	[number]	k4	1620
vydrancoval	vydrancovat	k5eAaPmAgInS	vydrancovat
městečko	městečko	k1gNnSc4	městečko
se	s	k7c7	s
zámkem	zámek	k1gInSc7	zámek
oddíl	oddíl	k1gInSc1	oddíl
polských	polský	k2eAgMnPc2d1	polský
kozáků	kozák	k1gMnPc2	kozák
a	a	k8xC	a
žoldnéřů	žoldnéř	k1gMnPc2	žoldnéř
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
táhli	táhnout	k5eAaImAgMnP	táhnout
do	do	k7c2	do
bitvy	bitva	k1gFnSc2	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
a	a	k8xC	a
archiv	archiv	k1gInSc1	archiv
byl	být	k5eAaImAgInS	být
zničen	zničit	k5eAaPmNgInS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgInS	být
zámek	zámek	k1gInSc1	zámek
během	během	k7c2	během
dalšího	další	k2eAgNnSc2d1	další
desetiletí	desetiletí	k1gNnSc2	desetiletí
opraven	opravna	k1gFnPc2	opravna
<g/>
,	,	kIx,	,
majitelé	majitel	k1gMnPc1	majitel
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
bydleli	bydlet	k5eAaImAgMnP	bydlet
jen	jen	k9	jen
příležitostně	příležitostně	k6eAd1	příležitostně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
trávili	trávit	k5eAaImAgMnP	trávit
u	u	k7c2	u
panovnického	panovnický	k2eAgInSc2d1	panovnický
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Neudržovaný	udržovaný	k2eNgInSc4d1	neudržovaný
zámek	zámek	k1gInSc4	zámek
proto	proto	k8xC	proto
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
zbytek	zbytek	k1gInSc4	zbytek
sedmnáctého	sedmnáctý	k4xOgMnSc4	sedmnáctý
století	století	k1gNnPc2	století
chátral	chátrat	k5eAaImAgInS	chátrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Novým	nový	k2eAgMnSc7d1	nový
pánem	pán	k1gMnSc7	pán
Kladna	Kladno	k1gNnSc2	Kladno
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1626	[number]	k4	1626
stal	stát	k5eAaPmAgMnS	stát
Florián	Florián	k1gMnSc1	Florián
Jetřich	Jetřich	k1gMnSc1	Jetřich
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Gotharta	Gothart	k1gMnSc2	Gothart
Žďárského	Žďárský	k1gMnSc2	Žďárský
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
<g/>
.	.	kIx.	.
</s>
<s>
Oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
Eliškou	Eliška	k1gFnSc7	Eliška
Koronou	Korona	k1gFnSc7	Korona
z	z	k7c2	z
Martinic	Martinice	k1gFnPc2	Martinice
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
povýšení	povýšení	k1gNnSc4	povýšení
do	do	k7c2	do
hraběcího	hraběcí	k2eAgInSc2d1	hraběcí
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Panství	panství	k1gNnSc1	panství
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1653	[number]	k4	1653
převzal	převzít	k5eAaPmAgMnS	převzít
syn	syn	k1gMnSc1	syn
František	František	k1gMnSc1	František
Adam	Adam	k1gMnSc1	Adam
Eusebius	Eusebius	k1gMnSc1	Eusebius
Žďárský	Žďárský	k1gMnSc1	Žďárský
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
neoženil	oženit	k5eNaPmAgInS	oženit
a	a	k8xC	a
katolická	katolický	k2eAgFnSc1d1	katolická
větev	větev	k1gFnSc1	větev
rodu	rod	k1gInSc2	rod
jím	jíst	k5eAaImIp1nS	jíst
vymřela	vymřít	k5eAaPmAgFnS	vymřít
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
bezdětný	bezdětný	k2eAgMnSc1d1	bezdětný
odkázal	odkázat	k5eAaPmAgInS	odkázat
majetek	majetek	k1gInSc1	majetek
jednomu	jeden	k4xCgMnSc3	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
protestantských	protestantský	k2eAgMnPc2d1	protestantský
strýců	strýc	k1gMnPc2	strýc
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
Sasku	Sasko	k1gNnSc6	Sasko
<g/>
,	,	kIx,	,
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
přejde	přejít	k5eAaPmIp3nS	přejít
ke	k	k7c3	k
katolictví	katolictví	k1gNnSc3	katolictví
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
uznán	uznán	k2eAgInSc1d1	uznán
Martinici	Martinice	k1gFnSc3	Martinice
<g/>
.	.	kIx.	.
<g/>
Dědické	dědický	k2eAgNnSc1d1	dědické
řízení	řízení	k1gNnSc1	řízení
trvalo	trvat	k5eAaImAgNnS	trvat
šestnáct	šestnáct	k4xCc4	šestnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádný	žádný	k3yNgMnSc1	žádný
ze	z	k7c2	z
saských	saský	k2eAgMnPc2d1	saský
příbuzných	příbuzný	k1gMnPc2	příbuzný
se	se	k3xPyFc4	se
nechtěl	chtít	k5eNaImAgMnS	chtít
zříct	zříct	k5eAaPmF	zříct
své	svůj	k3xOyFgInPc4	svůj
víry	vír	k1gInPc4	vír
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1686	[number]	k4	1686
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
správci	správce	k1gMnPc7	správce
kladenského	kladenský	k2eAgInSc2d1	kladenský
majetku	majetek	k1gInSc2	majetek
stali	stát	k5eAaPmAgMnP	stát
smečenští	smečenský	k2eAgMnPc1d1	smečenský
Martinicové	Martinicový	k2eAgInPc1d1	Martinicový
<g/>
.	.	kIx.	.
</s>
<s>
Majitelkami	majitelka	k1gFnPc7	majitelka
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
staly	stát	k5eAaPmAgFnP	stát
Polyxena	Polyxena	k1gFnSc1	Polyxena
Lidmila	Lidmila	k1gFnSc1	Lidmila
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Maxmiliána	Maxmiliána	k1gFnSc1	Maxmiliána
Hieseerlová	Hieseerlová	k1gFnSc1	Hieseerlová
z	z	k7c2	z
Chodova	Chodov	k1gInSc2	Chodov
<g/>
,	,	kIx,	,
Johanka	Johanka	k1gFnSc1	Johanka
Eusebia	Eusebia	k1gFnSc1	Eusebia
Karettová	Karettová	k1gFnSc1	Karettová
<g/>
,	,	kIx,	,
Terezie	Terezie	k1gFnSc1	Terezie
Eleonora	Eleonora	k1gFnSc1	Eleonora
Ugartová	Ugartová	k1gFnSc1	Ugartová
a	a	k8xC	a
Anna	Anna	k1gFnSc1	Anna
Kateřina	Kateřina	k1gFnSc1	Kateřina
z	z	k7c2	z
Magni	Magň	k1gMnSc5	Magň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
Žďárským	Žďárský	k1gMnPc3	Žďárský
vyplatily	vyplatit	k5eAaPmAgInP	vyplatit
53	[number]	k4	53
tisíc	tisíc	k4xCgInPc2	tisíc
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1588	[number]	k4	1588
si	se	k3xPyFc3	se
panství	panství	k1gNnSc1	panství
rozdělily	rozdělit	k5eAaPmAgFnP	rozdělit
na	na	k7c4	na
pět	pět	k4xCc4	pět
stejných	stejný	k2eAgInPc2d1	stejný
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Kladno	Kladno	k1gNnSc1	Kladno
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Marii	Maria	k1gFnSc3	Maria
Maxmiliáně	Maxmiliána	k1gFnSc3	Maxmiliána
Hieseerlové	Hieseerlová	k1gFnSc3	Hieseerlová
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
vztah	vztah	k1gInSc1	vztah
ke	k	k7c3	k
Kladnu	Kladno	k1gNnSc3	Kladno
je	být	k5eAaImIp3nS	být
nejasný	jasný	k2eNgMnSc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Augusta	August	k1gMnSc2	August
Sedláčka	Sedláček	k1gMnSc2	Sedláček
v	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
žila	žít	k5eAaImAgFnS	žít
a	a	k8xC	a
podílela	podílet	k5eAaImAgFnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
úpravách	úprava	k1gFnPc6	úprava
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Rudolf	Rudolf	k1gMnSc1	Rudolf
Anděl	Anděl	k1gMnSc1	Anděl
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
o	o	k7c4	o
kladenský	kladenský	k2eAgInSc4d1	kladenský
majetek	majetek	k1gInSc4	majetek
takřka	takřka	k6eAd1	takřka
nezajímala	zajímat	k5eNaImAgFnS	zajímat
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
zemřela	zemřít	k5eAaPmAgFnS	zemřít
roku	rok	k1gInSc2	rok
1693	[number]	k4	1693
a	a	k8xC	a
majetek	majetek	k1gInSc4	majetek
zdědila	zdědit	k5eAaPmAgFnS	zdědit
Anna	Anna	k1gFnSc1	Anna
Ludovika	Ludovika	k1gFnSc1	Ludovika
z	z	k7c2	z
Lamberka	Lamberka	k1gFnSc1	Lamberka
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
za	za	k7c2	za
Mariina	Mariin	k2eAgMnSc2d1	Mariin
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gNnPc4	její
vnoučata	vnouče	k1gNnPc4	vnouče
Karel	Karel	k1gMnSc1	Karel
Benedikt	Benedikt	k1gMnSc1	Benedikt
a	a	k8xC	a
Františka	František	k1gMnSc4	František
Antonie	Antonie	k1gFnSc2	Antonie
z	z	k7c2	z
Lamberka	Lamberka	k1gFnSc1	Lamberka
<g/>
.	.	kIx.	.
</s>
<s>
Zadlužení	zadlužený	k2eAgMnPc1d1	zadlužený
Lamberkové	Lamberek	k1gMnPc1	Lamberek
prodali	prodat	k5eAaPmAgMnP	prodat
roku	rok	k1gInSc2	rok
1701	[number]	k4	1701
kladenské	kladenský	k2eAgFnSc2d1	kladenská
panství	panství	k1gNnSc1	panství
Anně	Anna	k1gFnSc3	Anna
Marii	Maria	k1gFnSc3	Maria
Toskánské	toskánský	k2eAgFnSc2d1	Toskánská
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnSc2d1	rozená
z	z	k7c2	z
Engru	Engr	k1gInSc2	Engr
a	a	k8xC	a
Vestfálska	Vestfálsko	k1gNnSc2	Vestfálsko
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
si	se	k3xPyFc3	se
je	on	k3xPp3gMnPc4	on
ponechala	ponechat	k5eAaPmAgFnS	ponechat
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1705	[number]	k4	1705
je	být	k5eAaImIp3nS	být
prodala	prodat	k5eAaPmAgFnS	prodat
břevnovskému	břevnovský	k2eAgInSc3d1	břevnovský
klášteru	klášter	k1gInSc3	klášter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
kláštera	klášter	k1gInSc2	klášter
Kladno	Kladno	k1gNnSc1	Kladno
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
až	až	k9	až
do	do	k7c2	do
zrušení	zrušení	k1gNnSc2	zrušení
poddanství	poddanství	k1gNnSc2	poddanství
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1705	[number]	k4	1705
břevnovsko-broumovský	břevnovskoroumovský	k2eAgMnSc1d1	břevnovsko-broumovský
opat	opat	k1gMnSc1	opat
přebíral	přebírat	k5eAaImAgMnS	přebírat
kladenské	kladenský	k2eAgNnSc4d1	kladenské
panství	panství	k1gNnSc4	panství
<g/>
,	,	kIx,	,
hrozily	hrozit	k5eAaImAgFnP	hrozit
některé	některý	k3yIgNnSc1	některý
části	část	k1gFnPc1	část
zámku	zámek	k1gInSc2	zámek
zřícením	zřícení	k1gNnPc3	zřícení
<g/>
.	.	kIx.	.
</s>
<s>
Opat	opat	k1gMnSc1	opat
Otmar	Otmar	k1gMnSc1	Otmar
Daniel	Daniel	k1gMnSc1	Daniel
Zinke	Zink	k1gFnSc2	Zink
proto	proto	k8xC	proto
přistoupil	přistoupit	k5eAaPmAgInS	přistoupit
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
opevnění	opevnění	k1gNnSc1	opevnění
a	a	k8xC	a
přilehlý	přilehlý	k2eAgInSc1d1	přilehlý
poplužní	poplužní	k2eAgInSc1d1	poplužní
dvůr	dvůr	k1gInSc1	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
podobu	podoba	k1gFnSc4	podoba
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
muselo	muset	k5eAaImAgNnS	muset
ustoupit	ustoupit	k5eAaPmF	ustoupit
jižní	jižní	k2eAgNnSc4d1	jižní
křídlo	křídlo	k1gNnSc4	křídlo
s	s	k7c7	s
válcovou	válcový	k2eAgFnSc7d1	válcová
věží	věž	k1gFnSc7	věž
<g/>
,	,	kIx,	,
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
architekt	architekt	k1gMnSc1	architekt
Kilián	Kilián	k1gMnSc1	Kilián
Ignác	Ignác	k1gMnSc1	Ignác
Dientzenhofer	Dientzenhofer	k1gMnSc1	Dientzenhofer
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnPc1d1	vnější
úpravy	úprava	k1gFnPc1	úprava
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
dokončeny	dokončit	k5eAaPmNgInP	dokončit
roku	rok	k1gInSc2	rok
1740	[number]	k4	1740
za	za	k7c2	za
opata	opat	k1gMnSc2	opat
Benno	Benno	k6eAd1	Benno
Löbla	Löbla	k1gMnSc1	Löbla
a	a	k8xC	a
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
pracemi	práce	k1gFnPc7	práce
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yIgFnPc2	který
byly	být	k5eAaImAgFnP	být
chodby	chodba	k1gFnPc1	chodba
a	a	k8xC	a
kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
vyzdobeny	vyzdoben	k2eAgInPc4d1	vyzdoben
freskami	freska	k1gFnPc7	freska
od	od	k7c2	od
Karla	Karel	k1gMnSc4	Karel
Kováře	Kovář	k1gMnSc2	Kovář
<g/>
.	.	kIx.	.
</s>
<s>
Celou	celý	k2eAgFnSc4d1	celá
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
završila	završit	k5eAaPmAgFnS	završit
stavba	stavba	k1gFnSc1	stavba
hodinové	hodinový	k2eAgFnSc2d1	hodinová
vížky	vížka	k1gFnSc2	vížka
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
příčného	příčný	k2eAgNnSc2d1	příčné
křídla	křídlo	k1gNnSc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
sloužil	sloužit	k5eAaImAgInS	sloužit
především	především	k9	především
jako	jako	k9	jako
sídlo	sídlo	k1gNnSc4	sídlo
správy	správa	k1gFnSc2	správa
panství	panství	k1gNnSc1	panství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opaté	opata	k1gMnPc1	opata
ho	on	k3xPp3gMnSc4	on
využívali	využívat	k5eAaPmAgMnP	využívat
také	také	k9	také
k	k	k7c3	k
občasným	občasný	k2eAgInPc3d1	občasný
letním	letní	k2eAgInPc3d1	letní
pobytům	pobyt	k1gInPc3	pobyt
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
úpravách	úprava	k1gFnPc6	úprava
v	v	k7c6	v
devatenáctém	devatenáctý	k4xOgInSc6	devatenáctý
a	a	k8xC	a
dvacátém	dvacátý	k4xOgInSc6	dvacátý
století	století	k1gNnSc2	století
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
kaple	kaple	k1gFnSc2	kaple
<g/>
,	,	kIx,	,
fresky	freska	k1gFnSc2	freska
i	i	k8xC	i
ostatní	ostatní	k2eAgInPc1d1	ostatní
prvky	prvek	k1gInPc1	prvek
výzdoby	výzdoba	k1gFnSc2	výzdoba
interiérů	interiér	k1gInPc2	interiér
<g/>
.	.	kIx.	.
</s>
<s>
Dochovala	dochovat	k5eAaPmAgFnS	dochovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
dvojice	dvojice	k1gFnSc1	dvojice
barokních	barokní	k2eAgNnPc2d1	barokní
kamen	kamna	k1gNnPc2	kamna
ze	z	k7c2	z
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
osmnáctého	osmnáctý	k4xOgNnSc2	osmnáctý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
poddanství	poddanství	k1gNnSc2	poddanství
v	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
dále	daleko	k6eAd2	daleko
sídlila	sídlit	k5eAaImAgFnS	sídlit
správa	správa	k1gFnSc1	správa
velkostatku	velkostatek	k1gInSc2	velkostatek
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
budovu	budova	k1gFnSc4	budova
koupilo	koupit	k5eAaPmAgNnS	koupit
město	město	k1gNnSc1	město
Kladno	Kladno	k1gNnSc1	Kladno
a	a	k8xC	a
umístilo	umístit	k5eAaPmAgNnS	umístit
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
knihovnu	knihovna	k1gFnSc4	knihovna
<g/>
,	,	kIx,	,
archiv	archiv	k1gInSc1	archiv
a	a	k8xC	a
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
sklepení	sklepení	k1gNnSc6	sklepení
zřízena	zřízen	k2eAgFnSc1d1	zřízena
maketa	maketa	k1gFnSc1	maketa
důlního	důlní	k2eAgNnSc2d1	důlní
pracoviště	pracoviště	k1gNnSc2	pracoviště
s	s	k7c7	s
ukázkami	ukázka	k1gFnPc7	ukázka
hornických	hornický	k2eAgFnPc2d1	hornická
technik	technika	k1gFnPc2	technika
používaných	používaný	k2eAgFnPc2d1	používaná
na	na	k7c6	na
Kladensku	Kladensko	k1gNnSc6	Kladensko
<g/>
.	.	kIx.	.
<g/>
Ke	k	k7c3	k
kulturním	kulturní	k2eAgFnPc3d1	kulturní
účelům	účel	k1gInPc3	účel
zámek	zámek	k1gInSc1	zámek
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zámecká	zámecký	k2eAgFnSc1d1	zámecká
kaple	kaple	k1gFnSc1	kaple
a	a	k8xC	a
zahrada	zahrada	k1gFnSc1	zahrada
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
konání	konání	k1gNnSc3	konání
svatebních	svatební	k2eAgInPc2d1	svatební
obřadů	obřad	k1gInPc2	obřad
a	a	k8xC	a
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
sídlí	sídlet	k5eAaImIp3nS	sídlet
Galerie	galerie	k1gFnSc1	galerie
Kladenského	kladenský	k2eAgInSc2d1	kladenský
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Konají	konat	k5eAaImIp3nP	konat
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
také	také	k6eAd1	také
koncerty	koncert	k1gInPc7	koncert
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
přednášky	přednáška	k1gFnSc2	přednáška
nebo	nebo	k8xC	nebo
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
a	a	k8xC	a
tvůrčí	tvůrčí	k2eAgFnSc2d1	tvůrčí
dílny	dílna	k1gFnSc2	dílna
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
zahrady	zahrada	k1gFnSc2	zahrada
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
medvědárium	medvědárium	k1gNnSc4	medvědárium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavební	stavební	k2eAgFnSc1d1	stavební
podoba	podoba	k1gFnSc1	podoba
==	==	k?	==
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnSc4d1	původní
gotickou	gotický	k2eAgFnSc4d1	gotická
tvrz	tvrz	k1gFnSc4	tvrz
tvořila	tvořit	k5eAaImAgFnS	tvořit
čtverhranná	čtverhranný	k2eAgFnSc1d1	čtverhranná
budova	budova	k1gFnSc1	budova
chráněná	chráněný	k2eAgFnSc1d1	chráněná
příkopem	příkop	k1gInSc7	příkop
a	a	k8xC	a
valy	val	k1gInPc7	val
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnPc2	součást
opevnění	opevnění	k1gNnPc2	opevnění
byla	být	k5eAaImAgFnS	být
také	také	k9	také
zvonice	zvonice	k1gFnSc1	zvonice
blízkého	blízký	k2eAgInSc2d1	blízký
kostela	kostel	k1gInSc2	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
renesanční	renesanční	k2eAgFnSc6d1	renesanční
přestavbě	přestavba	k1gFnSc6	přestavba
měla	mít	k5eAaImAgFnS	mít
zámecká	zámecký	k2eAgFnSc1d1	zámecká
budova	budova	k1gFnSc1	budova
čtyři	čtyři	k4xCgNnPc4	čtyři
dvoupatrová	dvoupatrový	k2eAgNnPc4d1	dvoupatrové
křídla	křídlo	k1gNnPc4	křídlo
kolem	kolem	k7c2	kolem
nádvoří	nádvoří	k1gNnSc2	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
z	z	k7c2	z
nároží	nároží	k1gNnPc2	nároží
stávala	stávat	k5eAaImAgFnS	stávat
mohutná	mohutný	k2eAgFnSc1d1	mohutná
válcová	válcový	k2eAgFnSc1d1	válcová
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Opevnění	opevnění	k1gNnSc4	opevnění
tvořil	tvořit	k5eAaImAgMnS	tvořit
na	na	k7c6	na
třech	tři	k4xCgFnPc6	tři
stranách	strana	k1gFnPc6	strana
příkop	příkop	k1gInSc4	příkop
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
napustit	napustit	k5eAaPmF	napustit
vodou	voda	k1gFnSc7	voda
ze	z	k7c2	z
zahradních	zahradní	k2eAgFnPc2d1	zahradní
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
příkop	příkop	k1gInSc4	příkop
se	se	k3xPyFc4	se
přecházelo	přecházet	k5eAaImAgNnS	přecházet
po	po	k7c6	po
padacím	padací	k2eAgInSc6d1	padací
mostě	most	k1gInSc6	most
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
nahrazen	nahradit	k5eAaPmNgInS	nahradit
mostem	most	k1gInSc7	most
zděným	zděný	k2eAgInSc7d1	zděný
<g/>
.	.	kIx.	.
</s>
<s>
Obytným	obytný	k2eAgFnPc3d1	obytná
potřebám	potřeba	k1gFnPc3	potřeba
majitelů	majitel	k1gMnPc2	majitel
sloužilo	sloužit	k5eAaImAgNnS	sloužit
druhé	druhý	k4xOgNnSc1	druhý
patro	patro	k1gNnSc1	patro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
snad	snad	k9	snad
byly	být	k5eAaImAgInP	být
také	také	k9	také
kanceláře	kancelář	k1gFnSc2	kancelář
správy	správa	k1gFnSc2	správa
panství	panství	k1gNnSc1	panství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
patře	patro	k1gNnSc6	patro
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
reprezentační	reprezentační	k2eAgFnPc1d1	reprezentační
prostory	prostora	k1gFnPc1	prostora
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yQgMnPc4	který
patřil	patřit	k5eAaImAgInS	patřit
velký	velký	k2eAgInSc1d1	velký
sál	sál	k1gInSc1	sál
s	s	k7c7	s
kazetovým	kazetový	k2eAgInSc7d1	kazetový
stropem	strop	k1gInSc7	strop
a	a	k8xC	a
od	od	k7c2	od
konce	konec	k1gInSc2	konec
šestnáctého	šestnáctý	k4xOgNnSc2	šestnáctý
století	století	k1gNnSc2	století
také	také	k6eAd1	také
zámecká	zámecký	k2eAgFnSc1d1	zámecká
kaple	kaple	k1gFnSc1	kaple
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
byly	být	k5eAaImAgFnP	být
pouze	pouze	k6eAd1	pouze
provozní	provozní	k2eAgFnPc1d1	provozní
místnosti	místnost	k1gFnPc1	místnost
<g/>
:	:	kIx,	:
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
,	,	kIx,	,
spižírna	spižírna	k1gFnSc1	spižírna
<g/>
,	,	kIx,	,
pekárna	pekárna	k1gFnSc1	pekárna
<g/>
,	,	kIx,	,
lázeň	lázeň	k1gFnSc1	lázeň
<g/>
,	,	kIx,	,
prádelna	prádelna	k1gFnSc1	prádelna
<g/>
,	,	kIx,	,
čeledník	čeledník	k1gInSc1	čeledník
a	a	k8xC	a
další	další	k2eAgInSc1d1	další
<g/>
.	.	kIx.	.
<g/>
Kladenský	kladenský	k2eAgInSc1d1	kladenský
zámek	zámek	k1gInSc1	zámek
si	se	k3xPyFc3	se
uchoval	uchovat	k5eAaPmAgInS	uchovat
svou	svůj	k3xOyFgFnSc4	svůj
vrcholně	vrcholně	k6eAd1	vrcholně
barokní	barokní	k2eAgFnSc4d1	barokní
podobu	podoba	k1gFnSc4	podoba
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
osmnáctého	osmnáctý	k4xOgNnSc2	osmnáctý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
podlaží	podlaží	k1gNnPc1	podlaží
jednopatrové	jednopatrový	k2eAgFnSc2d1	jednopatrová
budovy	budova	k1gFnSc2	budova
s	s	k7c7	s
půdorysem	půdorys	k1gInSc7	půdorys
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmena	písmeno	k1gNnSc2	písmeno
U	U	kA	U
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
kordonová	kordonový	k2eAgFnSc1d1	kordonová
římsa	římsa	k1gFnSc1	římsa
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
římsy	římsa	k1gFnSc2	římsa
fasádu	fasáda	k1gFnSc4	fasáda
člení	členit	k5eAaImIp3nP	členit
okna	okno	k1gNnPc1	okno
v	v	k7c6	v
pískovcových	pískovcový	k2eAgFnPc6d1	pískovcová
šambránách	šambrána	k1gFnPc6	šambrána
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nádvorní	nádvorní	k2eAgFnSc6d1	nádvorní
straně	strana	k1gFnSc6	strana
původně	původně	k6eAd1	původně
býval	bývat	k5eAaImAgInS	bývat
přízemní	přízemní	k2eAgInSc1d1	přízemní
ochoz	ochoz	k1gInSc1	ochoz
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
oblouky	oblouk	k1gInPc1	oblouk
byly	být	k5eAaImAgInP	být
až	až	k9	až
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
výjimky	výjimka	k1gFnPc4	výjimka
zazděny	zazděn	k2eAgFnPc4d1	zazděna
v	v	k7c6	v
devatenáctém	devatenáctý	k4xOgInSc6	devatenáctý
století	století	k1gNnSc6	století
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ochozu	ochoz	k1gInSc2	ochoz
se	se	k3xPyFc4	se
vstupovalo	vstupovat	k5eAaImAgNnS	vstupovat
do	do	k7c2	do
kanceláří	kancelář	k1gFnPc2	kancelář
správy	správa	k1gFnSc2	správa
panství	panství	k1gNnSc1	panství
<g/>
.	.	kIx.	.
</s>
<s>
Prostory	prostora	k1gFnPc1	prostora
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
jsou	být	k5eAaImIp3nP	být
zaklenuté	zaklenutý	k2eAgInPc1d1	zaklenutý
křížovými	křížový	k2eAgFnPc7d1	křížová
nebo	nebo	k8xC	nebo
valenými	valený	k2eAgFnPc7d1	valená
klenbami	klenba	k1gFnPc7	klenba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejspíše	nejspíše	k9	nejspíše
pochází	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
renesanční	renesanční	k2eAgFnSc2d1	renesanční
stavební	stavební	k2eAgFnSc2d1	stavební
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
místnosti	místnost	k1gFnPc1	místnost
v	v	k7c6	v
patře	patro	k1gNnSc6	patro
mají	mít	k5eAaImIp3nP	mít
stropy	strop	k1gInPc1	strop
ploché	plochý	k2eAgInPc1d1	plochý
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
památkově	památkově	k6eAd1	památkově
chráněnému	chráněný	k2eAgInSc3d1	chráněný
areálu	areál	k1gInSc3	areál
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
bývalá	bývalý	k2eAgFnSc1d1	bývalá
stodola	stodola	k1gFnSc1	stodola
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
Josífka	Josífek	k1gMnSc2	Josífek
na	na	k7c6	na
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bývala	bývat	k5eAaImAgFnS	bývat
součástí	součást	k1gFnSc7	součást
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
dvora	dvůr	k1gInSc2	dvůr
zrušeného	zrušený	k2eAgInSc2d1	zrušený
při	při	k7c6	při
barokní	barokní	k2eAgFnSc6d1	barokní
přestavbě	přestavba	k1gFnSc6	přestavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
zámků	zámek	k1gInPc2	zámek
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kladno	Kladno	k1gNnSc4	Kladno
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
