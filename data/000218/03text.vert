<s>
Bryan	Bryan	k1gInSc1	Bryan
Adams	Adams	k1gInSc1	Adams
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
Kingston	Kingston	k1gInSc1	Kingston
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rockový	rockový	k2eAgMnSc1d1	rockový
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
hudebních	hudební	k2eAgMnPc2d1	hudební
textů	text	k1gInPc2	text
a	a	k8xC	a
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnSc4	jeho
nejznámější	známý	k2eAgNnPc1d3	nejznámější
alba	album	k1gNnPc1	album
patří	patřit	k5eAaImIp3nP	patřit
Reckless	Reckless	k1gInSc4	Reckless
<g/>
,	,	kIx,	,
18	[number]	k4	18
til	til	k1gInSc1	til
I	I	kA	I
Die	Die	k1gFnSc2	Die
a	a	k8xC	a
Waking	Waking	k1gInSc1	Waking
Up	Up	k1gFnSc2	Up
the	the	k?	the
Neighbours	Neighbours	k1gInSc1	Neighbours
<g/>
.	.	kIx.	.
</s>
<s>
Adams	Adams	k1gInSc1	Adams
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
Řádem	řád	k1gInSc7	řád
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
Order	Order	k1gInSc1	Order
of	of	k?	of
Canada	Canada	k1gFnSc1	Canada
<g/>
)	)	kIx)	)
a	a	k8xC	a
Řádem	řád	k1gInSc7	řád
Britské	britský	k2eAgFnSc2d1	britská
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
(	(	kIx(	(
<g/>
Order	Order	k1gInSc1	Order
of	of	k?	of
British	British	k1gInSc1	British
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
)	)	kIx)	)
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
příspěvek	příspěvek	k1gInSc4	příspěvek
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
za	za	k7c4	za
dobročinné	dobročinný	k2eAgFnPc4d1	dobročinná
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
hudební	hudební	k2eAgFnSc2d1	hudební
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
při	při	k7c6	při
předávání	předávání	k1gNnSc6	předávání
kanadských	kanadský	k2eAgFnPc2d1	kanadská
hudebních	hudební	k2eAgFnPc2d1	hudební
cen	cena	k1gFnPc2	cena
Juno	Juno	k1gFnSc2	Juno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
svou	svůj	k3xOyFgFnSc4	svůj
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
Kanadském	kanadský	k2eAgInSc6d1	kanadský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgInS	nominovat
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
Ceny	cena	k1gFnPc4	cena
akademie	akademie	k1gFnSc2	akademie
a	a	k8xC	a
pětkrát	pětkrát	k6eAd1	pětkrát
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Zlatý	zlatý	k2eAgInSc4d1	zlatý
globus	globus	k1gInSc4	globus
(	(	kIx(	(
<g/>
naposled	naposled	k6eAd1	naposled
za	za	k7c4	za
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
Bobby	Bobba	k1gFnSc2	Bobba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bryan	Bryan	k1gInSc1	Bryan
Adams	Adamsa	k1gFnPc2	Adamsa
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
anglickým	anglický	k2eAgMnSc7d1	anglický
rodičům	rodič	k1gMnPc3	rodič
v	v	k7c6	v
Kingstonu	Kingston	k1gInSc6	Kingston
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
byli	být	k5eAaImAgMnP	být
diplomaté	diplomat	k1gMnPc1	diplomat
<g/>
,	,	kIx,	,
strávil	strávit	k5eAaPmAgMnS	strávit
mládí	mládí	k1gNnSc3	mládí
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
cizích	cizí	k2eAgFnPc6d1	cizí
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
žili	žít	k5eAaImAgMnP	žít
krátce	krátce	k6eAd1	krátce
v	v	k7c6	v
Ottawě	Ottawa	k1gFnSc6	Ottawa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozvodu	rozvod	k1gInSc6	rozvod
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
se	se	k3xPyFc4	se
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
Brucem	Bruce	k1gMnSc7	Bruce
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
natrvalo	natrvalo	k6eAd1	natrvalo
do	do	k7c2	do
Vancouveru	Vancouver	k1gInSc2	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
Adams	Adams	k6eAd1	Adams
v	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
ukončil	ukončit	k5eAaPmAgInS	ukončit
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
naplno	naplno	k6eAd1	naplno
věnovat	věnovat	k5eAaPmF	věnovat
hudební	hudební	k2eAgFnSc3d1	hudební
kariéře	kariéra	k1gFnSc3	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Začínal	začínat	k5eAaImAgMnS	začínat
jako	jako	k9	jako
zpěvák	zpěvák	k1gMnSc1	zpěvák
v	v	k7c6	v
kapelách	kapela	k1gFnPc6	kapela
Shock	Shocka	k1gFnPc2	Shocka
a	a	k8xC	a
Sweeney	Sweenea	k1gFnSc2	Sweenea
Todd	Toddo	k1gNnPc2	Toddo
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgMnPc7	který
vydal	vydat	k5eAaPmAgMnS	vydat
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
If	If	k1gFnSc2	If
Wishes	Wishesa	k1gFnPc2	Wishesa
Were	Were	k1gFnSc7	Were
Horses	Horsesa	k1gFnPc2	Horsesa
<g/>
.	.	kIx.	.
</s>
<s>
Vydělával	vydělávat	k5eAaImAgMnS	vydělávat
si	se	k3xPyFc3	se
mytím	mytí	k1gNnSc7	mytí
nádobí	nádobí	k1gNnSc2	nádobí
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
prodavač	prodavač	k1gMnSc1	prodavač
ve	v	k7c6	v
zverimexu	zverimex	k1gInSc6	zverimex
nebo	nebo	k8xC	nebo
v	v	k7c6	v
obchodu	obchod	k1gInSc6	obchod
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
také	také	k6eAd1	také
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
potkal	potkat	k5eAaPmAgMnS	potkat
s	s	k7c7	s
bubeníkem	bubeník	k1gMnSc7	bubeník
Jimem	Jim	k1gMnSc7	Jim
Vallancem	Vallanec	k1gMnSc7	Vallanec
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
tak	tak	k6eAd1	tak
zatím	zatím	k6eAd1	zatím
nepřerušená	přerušený	k2eNgFnSc1d1	nepřerušená
spolupráce	spolupráce	k1gFnSc1	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
osmnácti	osmnáct	k4xCc2	osmnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
poslal	poslat	k5eAaPmAgMnS	poslat
Adams	Adams	k1gInSc4	Adams
několik	několik	k4yIc4	několik
svých	svůj	k3xOyFgFnPc2	svůj
demo	demo	k2eAgFnPc2d1	demo
nahrávek	nahrávka	k1gFnPc2	nahrávka
do	do	k7c2	do
nahrávacího	nahrávací	k2eAgNnSc2d1	nahrávací
studia	studio	k1gNnSc2	studio
A	a	k8xC	a
<g/>
&	&	k?	&
<g/>
M	M	kA	M
Records	Records	k1gInSc1	Records
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
a	a	k8xC	a
zanedlouho	zanedlouho	k6eAd1	zanedlouho
poté	poté	k6eAd1	poté
dostal	dostat	k5eAaPmAgMnS	dostat
první	první	k4xOgFnSc4	první
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
dolar	dolar	k1gInSc4	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zde	zde	k6eAd1	zde
vydal	vydat	k5eAaPmAgInS	vydat
už	už	k6eAd1	už
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
čtrnáct	čtrnáct	k4xCc4	čtrnáct
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Bryan	Bryan	k1gInSc4	Bryan
Adams	Adams	k1gInSc1	Adams
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
skladeb	skladba	k1gFnPc2	skladba
Remember	Remembra	k1gFnPc2	Remembra
a	a	k8xC	a
Wastin	Wastina	k1gFnPc2	Wastina
<g/>
'	'	kIx"	'
Time	Time	k1gFnSc4	Time
bylo	být	k5eAaImAgNnS	být
celé	celý	k2eAgNnSc1d1	celé
album	album	k1gNnSc1	album
natočeno	natočit	k5eAaBmNgNnS	natočit
od	od	k7c2	od
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1979	[number]	k4	1979
v	v	k7c6	v
Manta	manto	k1gNnPc4	manto
Studios	Studios	k?	Studios
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
a	a	k8xC	a
producenty	producent	k1gMnPc4	producent
byli	být	k5eAaImAgMnP	být
Adams	Adamsa	k1gFnPc2	Adamsa
a	a	k8xC	a
Vallance	Vallance	k1gFnSc2	Vallance
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
však	však	k9	však
stalo	stát	k5eAaPmAgNnS	stát
zlatým	zlatý	k1gInSc7	zlatý
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
Adams	Adamsa	k1gFnPc2	Adamsa
a	a	k8xC	a
Vallance	Vallance	k1gFnSc2	Vallance
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
písní	píseň	k1gFnPc2	píseň
pro	pro	k7c4	pro
Vallencovu	Vallencův	k2eAgFnSc4d1	Vallencův
kapelu	kapela	k1gFnSc4	kapela
Prism	Prisma	k1gFnPc2	Prisma
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jinými	jiný	k1gMnPc7	jiný
napsali	napsat	k5eAaPmAgMnP	napsat
například	například	k6eAd1	například
píseň	píseň	k1gFnSc4	píseň
Don	dona	k1gFnPc2	dona
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Let	let	k1gInSc1	let
Him	Him	k1gMnSc1	Him
Know	Know	k1gFnSc1	Know
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
jediným	jediný	k2eAgInSc7d1	jediný
hitem	hit	k1gInSc7	hit
této	tento	k3xDgFnSc2	tento
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
Top	topit	k5eAaImRp2nS	topit
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Adamse	Adams	k1gMnSc4	Adams
to	ten	k3xDgNnSc1	ten
však	však	k9	však
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Adamsovo	Adamsův	k2eAgNnSc4d1	Adamsovo
druhé	druhý	k4xOgNnSc4	druhý
album	album	k1gNnSc4	album
You	You	k1gMnSc2	You
Want	Want	k1gMnSc1	Want
It	It	k1gMnSc2	It
You	You	k1gFnSc2	You
Got	Got	k1gMnSc2	Got
It	It	k1gMnSc2	It
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
během	během	k7c2	během
pouhých	pouhý	k2eAgInPc2d1	pouhý
dvou	dva	k4xCgInPc2	dva
týdnů	týden	k1gInPc2	týden
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
prvním	první	k4xOgNnSc7	první
albem	album	k1gNnSc7	album
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Bobem	Bob	k1gMnSc7	Bob
Clearmountainem	Clearmountain	k1gMnSc7	Clearmountain
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
a	a	k8xC	a
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
rádiový	rádiový	k2eAgInSc4d1	rádiový
hit	hit	k1gInSc4	hit
Lonely	Lonela	k1gFnSc2	Lonela
Nights	Nightsa	k1gFnPc2	Nightsa
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
až	až	k9	až
s	s	k7c7	s
třetím	třetí	k4xOgNnSc7	třetí
albem	album	k1gNnSc7	album
Cuts	Cutsa	k1gFnPc2	Cutsa
Like	Lik	k1gInSc2	Lik
a	a	k8xC	a
Knife	Knife	k?	Knife
vydaným	vydaný	k2eAgInSc7d1	vydaný
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
přišel	přijít	k5eAaPmAgInS	přijít
dlouho	dlouho	k6eAd1	dlouho
očekávaný	očekávaný	k2eAgInSc4d1	očekávaný
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
skončilo	skončit	k5eAaPmAgNnS	skončit
na	na	k7c6	na
osmém	osmý	k4xOgInSc6	osmý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
prodeje	prodej	k1gInSc2	prodej
alb	album	k1gNnPc2	album
a	a	k8xC	a
v	v	k7c6	v
hitparádě	hitparáda	k1gFnSc6	hitparáda
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
přední	přední	k2eAgFnPc4d1	přední
příčky	příčka	k1gFnPc4	příčka
hned	hned	k6eAd1	hned
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
skladbami	skladba	k1gFnPc7	skladba
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
tou	ten	k3xDgFnSc7	ten
nejznámější	známý	k2eAgFnSc7d3	nejznámější
je	být	k5eAaImIp3nS	být
pilotní	pilotní	k2eAgInSc1d1	pilotní
singl	singl	k1gInSc1	singl
Cuts	Cutsa	k1gFnPc2	Cutsa
Like	Lik	k1gInSc2	Lik
a	a	k8xC	a
Knife	Knife	k?	Knife
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
zůstala	zůstat	k5eAaPmAgFnS	zůstat
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejoblíbenějších	oblíbený	k2eAgFnPc2d3	nejoblíbenější
a	a	k8xC	a
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
Adamsových	Adamsův	k2eAgFnPc2d1	Adamsova
písní	píseň	k1gFnPc2	píseň
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Reckless	Recklessa	k1gFnPc2	Recklessa
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c4	v
den	den	k1gInSc4	den
Bryanových	Bryanových	k2eAgFnSc2d1	Bryanových
25	[number]	k4	25
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
prodeji	prodej	k1gInSc6	prodej
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
hity	hit	k1gInPc4	hit
patří	patřit	k5eAaImIp3nS	patřit
Run	run	k1gInSc1	run
to	ten	k3xDgNnSc1	ten
You	You	k1gMnSc1	You
<g/>
,	,	kIx,	,
Summer	Summer	k1gMnSc1	Summer
of	of	k?	of
'	'	kIx"	'
<g/>
69	[number]	k4	69
a	a	k8xC	a
první	první	k4xOgFnSc4	první
Adamsova	Adamsův	k2eAgFnSc1d1	Adamsova
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vyhoupla	vyhoupnout	k5eAaPmAgFnS	vyhoupnout
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
až	až	k9	až
na	na	k7c4	na
samý	samý	k3xTgInSc4	samý
vrchol	vrchol	k1gInSc4	vrchol
-	-	kIx~	-
Heaven	Heavna	k1gFnPc2	Heavna
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
album	album	k1gNnSc4	album
dostal	dostat	k5eAaPmAgMnS	dostat
Bryan	Bryan	k1gInSc4	Bryan
v	v	k7c6	v
USA	USA	kA	USA
již	již	k9	již
pět	pět	k4xCc4	pět
platinových	platinový	k2eAgNnPc2d1	platinové
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
také	také	k9	také
natočil	natočit	k5eAaBmAgMnS	natočit
duet	duet	k1gInSc4	duet
s	s	k7c7	s
Tinou	Tina	k1gFnSc7	Tina
Turnerovou	turnerův	k2eAgFnSc7d1	Turnerova
It	It	k1gFnSc4	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Only	Only	k1gInPc7	Only
Love	lov	k1gInSc5	lov
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
stala	stát	k5eAaPmAgFnS	stát
hlavní	hlavní	k2eAgFnSc7d1	hlavní
znělkou	znělka	k1gFnSc7	znělka
mnoha	mnoho	k4c2	mnoho
rádií	rádio	k1gNnPc2	rádio
a	a	k8xC	a
MTV	MTV	kA	MTV
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
také	také	k9	také
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
rockový	rockový	k2eAgInSc4d1	rockový
duet	duet	k1gInSc4	duet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
vydává	vydávat	k5eAaPmIp3nS	vydávat
již	již	k9	již
páté	pátý	k4xOgNnSc4	pátý
album	album	k1gNnSc4	album
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Into	Into	k1gNnSc1	Into
the	the	k?	the
Fire	Fir	k1gInSc2	Fir
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
dostalo	dostat	k5eAaPmAgNnS	dostat
také	také	k9	také
platinovou	platinový	k2eAgFnSc4d1	platinová
desku	deska	k1gFnSc4	deska
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
posledním	poslední	k2eAgNnSc7d1	poslední
albem	album	k1gNnSc7	album
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
kompletně	kompletně	k6eAd1	kompletně
celé	celá	k1gFnSc2	celá
napsala	napsat	k5eAaPmAgFnS	napsat
dvojice	dvojice	k1gFnSc1	dvojice
Adams	Adamsa	k1gFnPc2	Adamsa
-	-	kIx~	-
Vallance	Vallance	k1gFnSc1	Vallance
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
vychází	vycházet	k5eAaImIp3nS	vycházet
album	album	k1gNnSc4	album
Live	Liv	k1gFnSc2	Liv
<g/>
!	!	kIx.	!
</s>
<s>
Live	Live	k6eAd1	Live
<g/>
!	!	kIx.	!
</s>
<s>
Live	Live	k6eAd1	Live
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
nahrávkou	nahrávka	k1gFnSc7	nahrávka
koncertu	koncert	k1gInSc2	koncert
ve	v	k7c6	v
Werchteru	Werchter	k1gInSc6	Werchter
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
z	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
byl	být	k5eAaImAgInS	být
vysílán	vysílat	k5eAaImNgInS	vysílat
na	na	k7c6	na
CBS	CBS	kA	CBS
a	a	k8xC	a
MTV	MTV	kA	MTV
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejznámějším	známý	k2eAgNnSc7d3	nejznámější
Adamsovým	Adamsův	k2eAgNnSc7d1	Adamsovo
albem	album	k1gNnSc7	album
je	být	k5eAaImIp3nS	být
Waking	Waking	k1gInSc1	Waking
up	up	k?	up
the	the	k?	the
Neighbours	Neighboursa	k1gFnPc2	Neighboursa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
produkoval	produkovat	k5eAaImAgMnS	produkovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
"	"	kIx"	"
<g/>
Muttem	Mutt	k1gInSc7	Mutt
<g/>
"	"	kIx"	"
Langem	Lang	k1gMnSc7	Lang
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
a	a	k8xC	a
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
největší	veliký	k2eAgInSc4d3	veliký
hit	hit	k1gInSc4	hit
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
(	(	kIx(	(
<g/>
Everything	Everything	k1gInSc4	Everything
I	i	k9	i
Do	do	k7c2	do
<g/>
)	)	kIx)	)
I	i	k9	i
Do	do	k7c2	do
It	It	k1gFnPc2	It
for	forum	k1gNnPc2	forum
You	You	k1gMnPc2	You
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ústřední	ústřední	k2eAgFnSc1d1	ústřední
píseň	píseň	k1gFnSc1	píseň
z	z	k7c2	z
filmu	film	k1gInSc2	film
Robin	robin	k2eAgInSc4d1	robin
Hood	Hood	k1gInSc4	Hood
-	-	kIx~	-
Král	Král	k1gMnSc1	Král
zbojníků	zbojník	k1gMnPc2	zbojník
s	s	k7c7	s
Kevinem	Kevin	k1gMnSc7	Kevin
Costnerem	Costner	k1gMnSc7	Costner
a	a	k8xC	a
Alanem	Alan	k1gMnSc7	Alan
Rickmanem	Rickman	k1gMnSc7	Rickman
<g/>
.	.	kIx.	.
</s>
<s>
Vysloužila	vysloužit	k5eAaPmAgFnS	vysloužit
si	se	k3xPyFc3	se
i	i	k9	i
zápis	zápis	k1gInSc4	zápis
do	do	k7c2	do
Guinessovy	Guinessův	k2eAgFnSc2d1	Guinessova
knihy	kniha	k1gFnSc2	kniha
rekordů	rekord	k1gInPc2	rekord
díky	díky	k7c3	díky
neuvěřitelným	uvěřitelný	k2eNgFnPc3d1	neuvěřitelná
šestnácti	šestnáct	k4xCc7	šestnáct
týdnům	týden	k1gInPc3	týden
stráveným	strávený	k2eAgInPc3d1	strávený
na	na	k7c6	na
první	první	k4xOgFnSc6	první
příčce	příčka	k1gFnSc6	příčka
hitparády	hitparáda	k1gFnSc2	hitparáda
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
sedmnácti	sedmnáct	k4xCc3	sedmnáct
týdnům	týden	k1gInPc3	týden
v	v	k7c4	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
album	album	k1gNnSc1	album
tak	tak	k8xS	tak
singl	singl	k1gInSc1	singl
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgInP	dostat
na	na	k7c4	na
první	první	k4xOgFnPc4	první
příčky	příčka	k1gFnPc4	příčka
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jenom	jenom	k9	jenom
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
tři	tři	k4xCgInPc4	tři
milióny	milión	k4xCgInPc1	milión
kopií	kopie	k1gFnPc2	kopie
této	tento	k3xDgFnSc2	tento
písně	píseň	k1gFnSc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
albem	album	k1gNnSc7	album
vydaným	vydaný	k2eAgNnSc7d1	vydané
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byl	být	k5eAaImAgInS	být
výběr	výběr	k1gInSc1	výběr
největších	veliký	k2eAgInPc2d3	veliký
Adamsových	Adamsův	k2eAgInPc2d1	Adamsův
hitů	hit	k1gInPc2	hit
So	So	kA	So
Far	fara	k1gFnPc2	fara
So	So	kA	So
Good	Good	k1gInSc1	Good
<g/>
.	.	kIx.	.
</s>
<s>
Objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
jedna	jeden	k4xCgFnSc1	jeden
nová	nový	k2eAgFnSc1d1	nová
píseň	píseň	k1gFnSc1	píseň
Please	Pleas	k1gInSc6	Pleas
Forgive	Forgiev	k1gFnPc4	Forgiev
Me	Me	k1gFnSc2	Me
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
stala	stát	k5eAaPmAgFnS	stát
hitem	hit	k1gInSc7	hit
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
jen	jen	k9	jen
v	v	k7c6	v
USA	USA	kA	USA
pět	pět	k4xCc4	pět
platinových	platinový	k2eAgFnPc2d1	platinová
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
natočil	natočit	k5eAaBmAgMnS	natočit
Bryan	Bryan	k1gMnSc1	Bryan
společně	společně	k6eAd1	společně
se	s	k7c7	s
Stingem	Sting	k1gInSc7	Sting
a	a	k8xC	a
Rodem	rod	k1gInSc7	rod
Stewartem	Stewart	k1gMnSc7	Stewart
singl	singl	k1gInSc4	singl
All	All	k1gFnSc2	All
for	forum	k1gNnPc2	forum
Love	lov	k1gInSc5	lov
k	k	k7c3	k
filmu	film	k1gInSc6	film
Tři	tři	k4xCgMnPc1	tři
mušketýři	mušketýr	k1gMnPc1	mušketýr
<g/>
.	.	kIx.	.
</s>
<s>
Hit	hit	k1gInSc1	hit
All	All	k1gMnPc2	All
for	forum	k1gNnPc2	forum
Love	lov	k1gInSc5	lov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
zazpíval	zazpívat	k5eAaPmAgInS	zazpívat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Lucianem	Lucian	k1gMnSc7	Lucian
Pavarotti	Pavarotti	k1gNnSc2	Pavarotti
&	&	k?	&
Friends	Friendsa	k1gFnPc2	Friendsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
vydal	vydat	k5eAaPmAgMnS	vydat
Bryan	Bryan	k1gMnSc1	Bryan
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
knihu	kniha	k1gFnSc4	kniha
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Bryan	Bryany	k1gInPc2	Bryany
Adams	Adamsa	k1gFnPc2	Adamsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996	[number]	k4	1996
-	-	kIx~	-
1999	[number]	k4	1999
vydal	vydat	k5eAaPmAgInS	vydat
Bryan	Bryan	k1gInSc1	Bryan
Adams	Adams	k1gInSc1	Adams
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
jedno	jeden	k4xCgNnSc4	jeden
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
18	[number]	k4	18
til	til	k1gInSc4	til
I	i	k9	i
Die	Die	k1gFnSc1	Die
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
hit	hit	k1gInSc1	hit
Have	Hav	k1gMnSc2	Hav
You	You	k1gMnSc2	You
Ever	Ever	k1gMnSc1	Ever
Really	Realla	k1gMnSc2	Realla
Loved	Loved	k1gMnSc1	Loved
a	a	k8xC	a
Woman	Woman	k1gMnSc1	Woman
z	z	k7c2	z
filmu	film	k1gInSc2	film
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
DeMarco	DeMarco	k1gMnSc1	DeMarco
s	s	k7c7	s
Johnym	Johnym	k1gInSc1	Johnym
Deppem	Depp	k1gInSc7	Depp
a	a	k8xC	a
Marlonem	Marlon	k1gInSc7	Marlon
Brando	Brando	k1gNnSc1	Brando
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
mu	on	k3xPp3gMnSc3	on
vynesla	vynést	k5eAaPmAgFnS	vynést
již	již	k6eAd1	již
druhou	druhý	k4xOgFnSc4	druhý
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
jen	jen	k9	jen
v	v	k7c6	v
USA	USA	kA	USA
více	hodně	k6eAd2	hodně
jak	jak	k6eAd1	jak
jeden	jeden	k4xCgInSc4	jeden
milión	milión	k4xCgInSc4	milión
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Bryan	Bryana	k1gFnPc2	Bryana
Adams	Adamsa	k1gFnPc2	Adamsa
MTV	MTV	kA	MTV
Unplugged	Unplugged	k1gMnSc1	Unplugged
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
On	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Day	Day	k1gMnSc2	Day
Like	Lik	k1gMnSc2	Lik
Today	Todaa	k1gMnSc2	Todaa
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
a	a	k8xC	a
již	již	k6eAd1	již
druhý	druhý	k4xOgInSc4	druhý
výběr	výběr	k1gInSc4	výběr
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Me	Me	k1gMnSc1	Me
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
natočil	natočit	k5eAaBmAgInS	natočit
Bryan	Bryan	k1gInSc1	Bryan
tři	tři	k4xCgInPc4	tři
duety	duet	k1gInPc4	duet
-	-	kIx~	-
Rock	rock	k1gInSc1	rock
Steady	Steada	k1gFnSc2	Steada
s	s	k7c7	s
Bonnie	Bonnie	k1gFnSc1	Bonnie
Raittovou	Raittová	k1gFnSc7	Raittová
<g/>
,	,	kIx,	,
I	i	k9	i
finally	finall	k1gInPc1	finall
Found	Founda	k1gFnPc2	Founda
Someone	Someon	k1gInSc5	Someon
s	s	k7c7	s
Barbrou	Barbrý	k2eAgFnSc7d1	Barbrý
Streisandovou	Streisandový	k2eAgFnSc7d1	Streisandová
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
mu	on	k3xPp3gMnSc3	on
přinesla	přinést	k5eAaPmAgFnS	přinést
již	již	k6eAd1	již
třetí	třetí	k4xOgFnSc4	třetí
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
)	)	kIx)	)
a	a	k8xC	a
When	When	k1gMnSc1	When
You	You	k1gMnSc1	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
Gone	Gone	k1gFnSc4	Gone
s	s	k7c7	s
Melanii	Melanie	k1gFnSc3	Melanie
C	C	kA	C
<g/>
,	,	kIx,	,
bývalou	bývalý	k2eAgFnSc7d1	bývalá
členkou	členka	k1gFnSc7	členka
dívčí	dívčí	k2eAgFnSc2d1	dívčí
skupiny	skupina	k1gFnSc2	skupina
Spice	Spice	k1gFnSc2	Spice
Girls	girl	k1gFnPc2	girl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
nazpíval	nazpívat	k5eAaPmAgInS	nazpívat
Adams	Adams	k1gInSc4	Adams
vokály	vokál	k1gInPc4	vokál
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
Don	dona	k1gFnPc2	dona
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Give	Giv	k1gMnSc4	Giv
Up	Up	k1gMnSc4	Up
od	od	k7c2	od
britského	britský	k2eAgMnSc2d1	britský
producenta	producent	k1gMnSc2	producent
Nicka	nicka	k1gFnSc1	nicka
Bracegirdlea	Bracegirdlea	k1gFnSc1	Bracegirdlea
alias	alias	k9	alias
Chicane	Chican	k1gMnSc5	Chican
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
hitparádě	hitparáda	k1gFnSc6	hitparáda
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
vydává	vydávat	k5eAaPmIp3nS	vydávat
první	první	k4xOgFnPc4	první
DVD	DVD	kA	DVD
Live	Liv	k1gFnPc1	Liv
at	at	k?	at
Slane	Slan	k1gInSc5	Slan
Castle	Castle	k1gFnSc2	Castle
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
živého	živý	k2eAgInSc2d1	živý
koncertu	koncert	k1gInSc2	koncert
z	z	k7c2	z
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
vydává	vydávat	k5eAaPmIp3nS	vydávat
Live	Live	k1gFnSc1	Live
at	at	k?	at
Budokan	Budokan	k1gInSc4	Budokan
<g/>
,	,	kIx,	,
záznam	záznam	k1gInSc4	záznam
koncertu	koncert	k1gInSc2	koncert
z	z	k7c2	z
Tokia	Tokio	k1gNnSc2	Tokio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
vydává	vydávat	k5eAaImIp3nS	vydávat
soundtrack	soundtrack	k1gInSc1	soundtrack
Spirit	Spirit	k1gInSc1	Spirit
<g/>
:	:	kIx,	:
Stallion	Stallion	k1gInSc1	Stallion
of	of	k?	of
the	the	k?	the
Cimarron	Cimarron	k1gInSc4	Cimarron
ke	k	k7c3	k
stejnojmennému	stejnojmenný	k2eAgInSc3d1	stejnojmenný
kreslenému	kreslený	k2eAgInSc3d1	kreslený
filmu	film	k1gInSc3	film
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
pouze	pouze	k6eAd1	pouze
jeho	jeho	k3xOp3gFnPc4	jeho
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yQgFnPc4	který
v	v	k7c6	v
USA	USA	kA	USA
získal	získat	k5eAaPmAgInS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
nebyl	být	k5eNaImAgInS	být
snímek	snímek	k1gInSc1	snímek
bohužel	bohužel	k6eAd1	bohužel
uveden	uvést	k5eAaPmNgInS	uvést
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
vychází	vycházet	k5eAaImIp3nS	vycházet
po	po	k7c6	po
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
první	první	k4xOgNnSc4	první
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
Room	Roomo	k1gNnPc2	Roomo
Service	Service	k1gFnSc2	Service
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
příčku	příčka	k1gFnSc4	příčka
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
jen	jen	k9	jen
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
týdne	týden	k1gInSc2	týden
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
prodalo	prodat	k5eAaPmAgNnS	prodat
440	[number]	k4	440
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vychází	vycházet	k5eAaImIp3nS	vycházet
již	již	k6eAd1	již
třetí	třetí	k4xOgFnSc1	třetí
kolekce	kolekce	k1gFnSc1	kolekce
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Anthology	Antholog	k1gMnPc7	Antholog
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dva	dva	k4xCgInPc4	dva
disky	disk	k1gInPc4	disk
a	a	k8xC	a
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
Adamsovu	Adamsův	k2eAgFnSc4d1	Adamsova
pětadvacetiletou	pětadvacetiletý	k2eAgFnSc4d1	pětadvacetiletá
kariéru	kariéra	k1gFnSc4	kariéra
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
velkými	velký	k2eAgInPc7d1	velký
hity	hit	k1gInPc7	hit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
vydává	vydávat	k5eAaImIp3nS	vydávat
i	i	k8xC	i
třetí	třetí	k4xOgFnPc4	třetí
DVD	DVD	kA	DVD
Live	Liv	k1gFnPc1	Liv
in	in	k?	in
Lisbon	Lisbon	k1gMnSc1	Lisbon
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
filmu	film	k1gInSc3	film
The	The	k1gMnSc1	The
Guardian	Guardian	k1gMnSc1	Guardian
s	s	k7c7	s
Kevinem	Kevin	k1gMnSc7	Kevin
Costnerem	Costner	k1gMnSc7	Costner
a	a	k8xC	a
Ashtonem	Ashton	k1gInSc7	Ashton
Kutcherem	Kutchero	k1gNnSc7	Kutchero
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
napsal	napsat	k5eAaBmAgMnS	napsat
a	a	k8xC	a
natočil	natočit	k5eAaBmAgMnS	natočit
píseň	píseň	k1gFnSc4	píseň
Never	Nevra	k1gFnPc2	Nevra
Let	léto	k1gNnPc2	léto
Go	Go	k1gMnPc2	Go
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
spoluautorem	spoluautor	k1gMnSc7	spoluautor
písně	píseň	k1gFnSc2	píseň
Never	Nevra	k1gFnPc2	Nevra
Gonna	Gonna	k1gFnSc1	Gonna
Break	break	k1gInSc1	break
My	my	k3xPp1nPc1	my
Faith	Faith	k1gMnSc1	Faith
k	k	k7c3	k
filmu	film	k1gInSc3	film
Bobby	Bobba	k1gFnSc2	Bobba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zpívají	zpívat	k5eAaImIp3nP	zpívat
Aretha	Aretha	k1gFnSc1	Aretha
Franklin	Franklin	k1gInSc4	Franklin
a	a	k8xC	a
Mary	Mary	k1gFnSc1	Mary
J	J	kA	J
Blige	Blige	k1gFnSc1	Blige
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
píseň	píseň	k1gFnSc4	píseň
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
nominaci	nominace	k1gFnSc6	nominace
na	na	k7c4	na
Zlatý	zlatý	k2eAgInSc4d1	zlatý
globus	globus	k1gInSc4	globus
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
také	také	k9	také
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
Colour	Colour	k1gMnSc1	Colour
Me	Me	k1gMnSc1	Me
Kubrick	Kubrick	k1gMnSc1	Kubrick
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Bryan	Bryan	k1gInSc4	Bryan
Adams	Adams	k1gInSc1	Adams
11	[number]	k4	11
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
album	album	k1gNnSc4	album
Bare	bar	k1gInSc5	bar
Bones	Bones	k1gInSc4	Bones
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
album	album	k1gNnSc1	album
Track	Tracka	k1gFnPc2	Tracka
Of	Of	k1gFnPc2	Of
My	my	k3xPp1nPc1	my
Years	Years	k1gInSc4	Years
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
má	mít	k5eAaImIp3nS	mít
Bryan	Bryan	k1gInSc4	Bryan
Adams	Adamsa	k1gFnPc2	Adamsa
poměrně	poměrně	k6eAd1	poměrně
početnou	početný	k2eAgFnSc4d1	početná
skupinu	skupina	k1gFnSc4	skupina
příznivců	příznivec	k1gMnPc2	příznivec
a	a	k8xC	a
obdivovatelů	obdivovatel	k1gMnPc2	obdivovatel
<g/>
.	.	kIx.	.
</s>
<s>
Vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
šestkrát	šestkrát	k6eAd1	šestkrát
a	a	k8xC	a
pokaždé	pokaždé	k6eAd1	pokaždé
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
kempu	kemp	k1gInSc6	kemp
Džbán	džbán	k1gInSc1	džbán
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
T-mobile	Tobila	k1gFnSc6	T-mobila
aréně	aréna	k1gFnSc6	aréna
<g/>
,	,	kIx,	,
potřetí	potřetí	k4xO	potřetí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
v	v	k7c6	v
Sazka	Sazka	k1gFnSc1	Sazka
Aréně	aréna	k1gFnSc6	aréna
<g/>
,	,	kIx,	,
počtvrté	počtvrté	k4xO	počtvrté
v	v	k7c6	v
O2	O2	k1gFnSc6	O2
aréně	aréna	k1gFnSc6	aréna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
19	[number]	k4	19
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
popáté	popáté	k4xO	popáté
27	[number]	k4	27
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
festivalu	festival	k1gInSc2	festival
Benátská	benátský	k2eAgFnSc1d1	Benátská
noc	noc	k1gFnSc1	noc
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
O2	O2	k1gFnSc6	O2
Aréně	aréna	k1gFnSc6	aréna
5	[number]	k4	5
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgMnPc3d1	jiný
sólovým	sólový	k2eAgMnPc3d1	sólový
umělcům	umělec	k1gMnPc3	umělec
nestřídá	střídat	k5eNaImIp3nS	střídat
Adams	Adams	k1gInSc1	Adams
členy	člen	k1gInPc4	člen
svého	svůj	k3xOyFgInSc2	svůj
hudebního	hudební	k2eAgInSc2d1	hudební
doprovodu	doprovod	k1gInSc2	doprovod
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
skupinku	skupinka	k1gFnSc4	skupinka
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
i	i	k8xC	i
nahrávají	nahrávat	k5eAaImIp3nP	nahrávat
většinu	většina	k1gFnSc4	většina
jeho	jeho	k3xOp3gFnSc2	jeho
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
Bryan	Bryan	k1gInSc1	Bryan
Adams	Adamsa	k1gFnPc2	Adamsa
často	často	k6eAd1	často
vnímán	vnímat	k5eAaImNgInS	vnímat
spíše	spíše	k9	spíše
jako	jako	k8xS	jako
kapela	kapela	k1gFnSc1	kapela
než	než	k8xS	než
sólový	sólový	k2eAgMnSc1d1	sólový
umělec	umělec	k1gMnSc1	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
sestava	sestava	k1gFnSc1	sestava
<g/>
:	:	kIx,	:
Keith	Keith	k1gMnSc1	Keith
Scott	Scott	k1gMnSc1	Scott
-	-	kIx~	-
sólová	sólový	k2eAgFnSc1d1	sólová
kytara	kytara	k1gFnSc1	kytara
Mickey	Micke	k2eAgMnPc4d1	Micke
Curry	Curr	k1gMnPc4	Curr
-	-	kIx~	-
bicí	bicí	k2eAgMnPc4d1	bicí
Gary	Gar	k1gMnPc4	Gar
Breit	Breita	k1gFnPc2	Breita
-	-	kIx~	-
klávesy	klávesa	k1gFnSc2	klávesa
Norm	Norm	k1gMnSc1	Norm
Fisher	Fishra	k1gFnPc2	Fishra
-	-	kIx~	-
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
Scott	Scott	k1gMnSc1	Scott
a	a	k8xC	a
Curry	Curra	k1gFnPc1	Curra
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
s	s	k7c7	s
Adamsem	Adams	k1gInSc7	Adams
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Fisher	Fishra	k1gFnPc2	Fishra
a	a	k8xC	a
Breit	Breita	k1gFnPc2	Breita
se	se	k3xPyFc4	se
ke	k	k7c3	k
kapele	kapela	k1gFnSc3	kapela
připojili	připojit	k5eAaPmAgMnP	připojit
teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
klávesy	kláves	k1gInPc4	kláves
Tommy	Tomma	k1gFnSc2	Tomma
Mandel	mandel	k1gInSc1	mandel
a	a	k8xC	a
na	na	k7c6	na
basu	bas	k1gInSc6	bas
Dave	Dav	k1gInSc5	Dav
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
působili	působit	k5eAaImAgMnP	působit
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
už	už	k6eAd1	už
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Bryan	Bryan	k1gInSc1	Bryan
Adams	Adams	k1gInSc1	Adams
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bryan	Bryany	k1gInPc2	Bryany
Adams	Adamsa	k1gFnPc2	Adamsa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Bryan	Bryan	k1gInSc1	Bryan
Adams	Adams	k1gInSc1	Adams
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Oficiální	oficiální	k2eAgInSc4d1	oficiální
web	web	k1gInSc4	web
Interview	interview	k1gNnSc2	interview
s	s	k7c7	s
Fender	Fender	k1gInSc1	Fender
guitars	guitars	k1gInSc4	guitars
<g/>
,	,	kIx,	,
květen	květen	k1gInSc4	květen
2007	[number]	k4	2007
</s>
