<s>
Řím	Řím	k1gInSc1	Řím
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
italským	italský	k2eAgInSc7d1	italský
a	a	k8xC	a
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
nejlidnatějším	lidnatý	k2eAgNnSc7d3	nejlidnatější
městem	město	k1gNnSc7	město
v	v	k7c6	v
EU	EU	kA	EU
<g/>
,	,	kIx,	,
politickým	politický	k2eAgInSc7d1	politický
<g/>
,	,	kIx,	,
hospodářským	hospodářský	k2eAgInSc7d1	hospodářský
a	a	k8xC	a
kulturním	kulturní	k2eAgInSc7d1	kulturní
centrem	centr	k1gInSc7	centr
mimořádně	mimořádně	k6eAd1	mimořádně
bohatým	bohatý	k2eAgInSc7d1	bohatý
na	na	k7c4	na
umělecké	umělecký	k2eAgFnPc4d1	umělecká
a	a	k8xC	a
historické	historický	k2eAgFnPc4d1	historická
památky	památka	k1gFnPc4	památka
<g/>
.	.	kIx.	.
</s>
