<s>
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Bolívarovská	Bolívarovský	k2eAgFnSc1d1	Bolívarovská
republika	republika	k1gFnSc1	republika
Venezuela	Venezuela	k1gFnSc1	Venezuela
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
República	Repúblic	k2eAgFnSc1d1	República
Bolivariana	Bolivariana	k1gFnSc1	Bolivariana
de	de	k?	de
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
federativní	federativní	k2eAgFnSc1d1	federativní
republika	republika	k1gFnSc1	republika
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Karibského	karibský	k2eAgNnSc2d1	Karibské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
severně	severně	k6eAd1	severně
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
