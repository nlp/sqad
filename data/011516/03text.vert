<p>
<s>
Kanton	Kanton	k1gInSc1	Kanton
Tassin-la-Demi-Lune	Tassina-Demi-Lun	k1gInSc5	Tassin-la-Demi-Lun
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Canton	Canton	k1gInSc1	Canton
de	de	k?	de
Tassin-la-Demi-Lune	Tassina-Demi-Lun	k1gMnSc5	Tassin-la-Demi-Lun
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgInSc4d1	francouzský
kanton	kanton	k1gInSc4	kanton
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Rhône	Rhôn	k1gInSc5	Rhôn
v	v	k7c6	v
regionu	region	k1gInSc2	region
Rhône-Alpes	Rhône-Alpesa	k1gFnPc2	Rhône-Alpesa
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ho	on	k3xPp3gInSc4	on
dvě	dva	k4xCgFnPc1	dva
obce	obec	k1gFnPc1	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obce	obec	k1gFnSc2	obec
kantonu	kanton	k1gInSc2	kanton
==	==	k?	==
</s>
</p>
<p>
<s>
Francheville	Francheville	k6eAd1	Francheville
</s>
</p>
<p>
<s>
Tassin-la-Demi-Lune	Tassina-Demi-Lun	k1gMnSc5	Tassin-la-Demi-Lun
</s>
</p>
