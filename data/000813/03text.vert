<s>
RMS	RMS	kA	RMS
Mauretania	Mauretanium	k1gNnSc2	Mauretanium
byl	být	k5eAaImAgInS	být
zaoceánský	zaoceánský	k2eAgInSc1d1	zaoceánský
parník	parník	k1gInSc1	parník
postavený	postavený	k2eAgInSc1d1	postavený
společností	společnost	k1gFnSc7	společnost
Cunard	Cunarda	k1gFnPc2	Cunarda
Line	linout	k5eAaImIp3nS	linout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Sloužil	sloužit	k5eAaImAgInS	sloužit
k	k	k7c3	k
přepravě	přeprava	k1gFnSc3	přeprava
cestujících	cestující	k1gFnPc2	cestující
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Liverpool	Liverpool	k1gInSc1	Liverpool
-	-	kIx~	-
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
konkurenční	konkurenční	k2eAgFnSc1d1	konkurenční
společnost	společnost	k1gFnSc1	společnost
White	Whit	k1gInSc5	Whit
Star	Star	kA	Star
Line	linout	k5eAaImIp3nS	linout
postavila	postavit	k5eAaPmAgFnS	postavit
loď	loď	k1gFnSc4	loď
Olympic	Olympice	k1gFnPc2	Olympice
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Mauretanie	Mauretanie	k1gFnSc1	Mauretanie
největší	veliký	k2eAgFnSc1d3	veliký
lodí	loď	k1gFnSc7	loď
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1909	[number]	k4	1909
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
Modrou	modrý	k2eAgFnSc4d1	modrá
stuhu	stuha	k1gFnSc4	stuha
za	za	k7c4	za
nejrychlejší	rychlý	k2eAgFnSc4d3	nejrychlejší
plavbu	plavba	k1gFnSc4	plavba
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rekord	rekord	k1gInSc4	rekord
držela	držet	k5eAaImAgFnS	držet
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1935	[number]	k4	1935
byla	být	k5eAaImAgFnS	být
prodána	prodat	k5eAaPmNgFnS	prodat
k	k	k7c3	k
rozebrání	rozebrání	k1gNnSc3	rozebrání
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vleku	vlek	k1gInSc6	vlek
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
poslední	poslední	k2eAgFnSc4d1	poslední
plavbu	plavba	k1gFnSc4	plavba
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Sloužila	sloužit	k5eAaImAgFnS	sloužit
tak	tak	k9	tak
několikanásobně	několikanásobně	k6eAd1	několikanásobně
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
její	její	k3xOp3gFnSc1	její
sesterská	sesterský	k2eAgFnSc1d1	sesterská
loď	loď	k1gFnSc1	loď
Lusitania	Lusitanium	k1gNnSc2	Lusitanium
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
potopena	potopit	k5eAaPmNgFnS	potopit
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
RMS	RMS	kA	RMS
Mauretania	Mauretanium	k1gNnSc2	Mauretanium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
