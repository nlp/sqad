<s>
Brailleovo	Brailleův	k2eAgNnSc1d1	Brailleovo
slepecké	slepecký	k2eAgNnSc1d1	slepecké
písmo	písmo	k1gNnSc1	písmo
(	(	kIx(	(
<g/>
psáno	psán	k2eAgNnSc1d1	psáno
také	také	k9	také
Braillovo	Braillův	k2eAgNnSc1d1	Braillovo
<g/>
;	;	kIx,	;
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
brajovo	brajův	k2eAgNnSc1d1	brajův
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
speciální	speciální	k2eAgInSc4d1	speciální
druh	druh	k1gInSc4	druh
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
systému	systém	k1gInSc2	systém
psaní	psaní	k1gNnSc2	psaní
<g/>
,	,	kIx,	,
určeného	určený	k2eAgInSc2d1	určený
pro	pro	k7c4	pro
nevidomé	nevidomý	k1gMnPc4	nevidomý
<g/>
,	,	kIx,	,
slabozraké	slabozraký	k1gMnPc4	slabozraký
a	a	k8xC	a
se	s	k7c7	s
zbytky	zbytek	k1gInPc7	zbytek
zraku	zrak	k1gInSc2	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
plastických	plastický	k2eAgInPc2d1	plastický
bodů	bod	k1gInPc2	bod
vyražených	vyražený	k2eAgInPc2d1	vyražený
do	do	k7c2	do
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
čtenář	čtenář	k1gMnSc1	čtenář
vnímá	vnímat	k5eAaImIp3nS	vnímat
hmatem	hmat	k1gInSc7	hmat
<g/>
.	.	kIx.	.
</s>
<s>
Písmo	písmo	k1gNnSc1	písmo
je	být	k5eAaImIp3nS	být
pojmenováno	pojmenován	k2eAgNnSc1d1	pojmenováno
podle	podle	k7c2	podle
francouzského	francouzský	k2eAgMnSc2d1	francouzský
učitele	učitel	k1gMnSc2	učitel
Louise	Louis	k1gMnSc2	Louis
Brailla	Braill	k1gMnSc2	Braill
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
ztratil	ztratit	k5eAaPmAgInS	ztratit
zrak	zrak	k1gInSc4	zrak
<g/>
,	,	kIx,	,
v	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
letech	let	k1gInPc6	let
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
toto	tento	k3xDgNnSc4	tento
písmo	písmo	k1gNnSc4	písmo
úpravou	úprava	k1gFnSc7	úprava
francouzského	francouzský	k2eAgInSc2d1	francouzský
vojenského	vojenský	k2eAgInSc2d1	vojenský
systému	systém	k1gInSc2	systém
umožňujícího	umožňující	k2eAgNnSc2d1	umožňující
čtení	čtení	k1gNnSc2	čtení
za	za	k7c4	za
tmy	tma	k1gFnPc4	tma
<g/>
.	.	kIx.	.
</s>
<s>
Braillovo	Braillův	k2eAgNnSc1d1	Braillovo
písmo	písmo	k1gNnSc1	písmo
je	být	k5eAaImIp3nS	být
netradičním	tradiční	k2eNgInSc7d1	netradiční
příkladem	příklad	k1gInSc7	příklad
binárního	binární	k2eAgInSc2d1	binární
kódu	kód	k1gInSc2	kód
používaného	používaný	k2eAgInSc2d1	používaný
mimo	mimo	k7c4	mimo
svět	svět	k1gInSc4	svět
počítačů	počítač	k1gMnPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
znak	znak	k1gInSc1	znak
Braillova	Braillův	k2eAgNnSc2d1	Braillovo
písma	písmo	k1gNnSc2	písmo
tvoří	tvořit	k5eAaImIp3nS	tvořit
mřížka	mřížka	k1gFnSc1	mřížka
šesti	šest	k4xCc2	šest
bodů	bod	k1gInPc2	bod
uspořádaných	uspořádaný	k2eAgInPc2d1	uspořádaný
do	do	k7c2	do
obdélníku	obdélník	k1gInSc2	obdélník
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
body	bod	k1gInPc4	bod
se	se	k3xPyFc4	se
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
čísly	číslo	k1gNnPc7	číslo
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
šesti	šest	k4xCc2	šest
míst	místo	k1gNnPc2	místo
buď	buď	k8xC	buď
bod	bod	k1gInSc4	bod
(	(	kIx(	(
<g/>
vyvýšené	vyvýšený	k2eAgNnSc4d1	vyvýšené
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zakódovat	zakódovat	k5eAaPmF	zakódovat
26	[number]	k4	26
=	=	kIx~	=
64	[number]	k4	64
různých	různý	k2eAgInPc2d1	různý
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
úplně	úplně	k6eAd1	úplně
prázdný	prázdný	k2eAgInSc1d1	prázdný
znak	znak	k1gInSc1	znak
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
mezera	mezera	k1gFnSc1	mezera
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
zbývá	zbývat	k5eAaImIp3nS	zbývat
63	[number]	k4	63
použitelných	použitelný	k2eAgInPc2d1	použitelný
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
znaků	znak	k1gInPc2	znak
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
základní	základní	k2eAgFnSc1d1	základní
sada	sada	k1gFnSc1	sada
hlavně	hlavně	k6eAd1	hlavně
znaky	znak	k1gInPc7	znak
pro	pro	k7c4	pro
písmena	písmeno	k1gNnPc4	písmeno
a	a	k8xC	a
interpunkci	interpunkce	k1gFnSc4	interpunkce
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
písmena	písmeno	k1gNnPc1	písmeno
a	a	k8xC	a
číslice	číslice	k1gFnPc1	číslice
se	se	k3xPyFc4	se
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
pomocí	pomocí	k7c2	pomocí
těchto	tento	k3xDgNnPc2	tento
písmen	písmeno	k1gNnPc2	písmeno
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
předchází	předcházet	k5eAaImIp3nS	předcházet
speciální	speciální	k2eAgInSc4d1	speciální
symbol	symbol	k1gInSc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
způsoby	způsob	k1gInPc1	způsob
zápisu	zápis	k1gInSc2	zápis
dalších	další	k2eAgInPc2d1	další
symbolů	symbol	k1gInPc2	symbol
(	(	kIx(	(
<g/>
jako	jako	k9	jako
např.	např.	kA	např.
z	z	k7c2	z
matematiky	matematika	k1gFnSc2	matematika
či	či	k8xC	či
hudby	hudba	k1gFnSc2	hudba
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
Braillova	Braillův	k2eAgNnSc2d1	Braillovo
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
šestibodové	šestibodový	k2eAgFnSc3d1	šestibodová
mřížce	mřížka	k1gFnSc3	mřížka
přidána	přidán	k2eAgFnSc1d1	přidána
další	další	k2eAgFnSc1d1	další
řada	řada	k1gFnSc1	řada
dvou	dva	k4xCgInPc2	dva
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
počet	počet	k1gInSc1	počet
použitelných	použitelný	k2eAgInPc2d1	použitelný
symbolů	symbol	k1gInPc2	symbol
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
na	na	k7c4	na
255	[number]	k4	255
<g/>
.	.	kIx.	.
</s>
<s>
Takovou	takový	k3xDgFnSc7	takový
sadou	sada	k1gFnSc7	sada
symbolů	symbol	k1gInPc2	symbol
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zapisovat	zapisovat	k5eAaImF	zapisovat
všechny	všechen	k3xTgInPc4	všechen
znaky	znak	k1gInPc4	znak
ASCII	ascii	kA	ascii
<g/>
.	.	kIx.	.
</s>
<s>
Sada	sada	k1gFnSc1	sada
všech	všecek	k3xTgInPc2	všecek
těchto	tento	k3xDgInPc2	tento
symbolů	symbol	k1gInPc2	symbol
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
standardu	standard	k1gInSc2	standard
Unicode	Unicod	k1gInSc5	Unicod
(	(	kIx(	(
<g/>
Braille	Braille	k1gNnPc1	Braille
Patterns	Patternsa	k1gFnPc2	Patternsa
<g/>
,	,	kIx,	,
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
2800	[number]	k4	2800
<g/>
–	–	k?	–
<g/>
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
28	[number]	k4	28
<g/>
FF	ff	kA	ff
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osmibodové	osmibodový	k2eAgNnSc1d1	osmibodové
Braillovo	Braillův	k2eAgNnSc1d1	Braillovo
písmo	písmo	k1gNnSc1	písmo
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
příliš	příliš	k6eAd1	příliš
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
sada	sada	k1gFnSc1	sada
symbolů	symbol	k1gInPc2	symbol
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
následující	následující	k2eAgInPc4d1	následující
znaky	znak	k1gInPc4	znak
<g/>
:	:	kIx,	:
Základní	základní	k2eAgInPc1d1	základní
znaky	znak	k1gInPc1	znak
abecedy	abeceda	k1gFnSc2	abeceda
Znaky	znak	k1gInPc1	znak
s	s	k7c7	s
diakritikou	diakritika	k1gFnSc7	diakritika
Interpunkce	interpunkce	k1gFnSc2	interpunkce
Speciální	speciální	k2eAgInPc1d1	speciální
prefixy	prefix	k1gInPc1	prefix
Pomocí	pomocí	k7c2	pomocí
speciálních	speciální	k2eAgInPc2d1	speciální
prefixů	prefix	k1gInPc2	prefix
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
význam	význam	k1gInSc1	význam
následujícího	následující	k2eAgInSc2d1	následující
znaku	znak	k1gInSc2	znak
či	či	k8xC	či
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
pro	pro	k7c4	pro
psaní	psaní	k1gNnSc4	psaní
číslic	číslice	k1gFnPc2	číslice
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
symbolů	symbol	k1gInPc2	symbol
pro	pro	k7c4	pro
písmena	písmeno	k1gNnPc4	písmeno
A	a	k8xC	a
<g/>
–	–	k?	–
<g/>
J	J	kA	J
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
značí	značit	k5eAaImIp3nS	značit
číslice	číslice	k1gFnSc1	číslice
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
a	a	k8xC	a
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
za	za	k7c2	za
normální	normální	k2eAgFnSc2d1	normální
situace	situace	k1gFnSc2	situace
označuje	označovat	k5eAaImIp3nS	označovat
písmeno	písmeno	k1gNnSc1	písmeno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokud	pokud	k8xS	pokud
ho	on	k3xPp3gMnSc4	on
předchází	předcházet	k5eAaImIp3nS	předcházet
prefix	prefix	k1gInSc1	prefix
pro	pro	k7c4	pro
číslici	číslice	k1gFnSc4	číslice
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
číslice	číslice	k1gFnSc2	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
ve	v	k7c6	v
výtahu	výtah	k1gInSc6	výtah
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
<g/>
:	:	kIx,	:
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
tlačítko	tlačítko	k1gNnSc1	tlačítko
šestého	šestý	k4xOgNnSc2	šestý
patra	patro	k1gNnSc2	patro
(	(	kIx(	(
<g/>
prefix	prefix	k1gInSc1	prefix
pro	pro	k7c4	pro
číslici	číslice	k1gFnSc4	číslice
+	+	kIx~	+
písmeno	písmeno	k1gNnSc4	písmeno
F	F	kA	F
znamenající	znamenající	k2eAgFnSc4d1	znamenající
číslici	číslice	k1gFnSc4	číslice
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc4	některý
interpunkční	interpunkční	k2eAgInPc4d1	interpunkční
symboly	symbol	k1gInPc4	symbol
nejsou	být	k5eNaImIp3nP	být
jednoznačné	jednoznačný	k2eAgInPc1d1	jednoznačný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
uvozovky	uvozovka	k1gFnSc2	uvozovka
a	a	k8xC	a
rovnítko	rovnítko	k1gNnSc1	rovnítko
sdílí	sdílet	k5eAaImIp3nS	sdílet
jeden	jeden	k4xCgInSc4	jeden
znak	znak	k1gInSc4	znak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
podle	podle	k7c2	podle
kontextu	kontext	k1gInSc2	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Braillovo	Braillův	k2eAgNnSc1d1	Braillovo
písmo	písmo	k1gNnSc1	písmo
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
ručně	ručně	k6eAd1	ručně
psát	psát	k5eAaImF	psát
pomocí	pomocí	k7c2	pomocí
speciální	speciální	k2eAgFnSc2d1	speciální
destičky	destička	k1gFnSc2	destička
s	s	k7c7	s
perem	pero	k1gNnSc7	pero
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
se	se	k3xPyFc4	se
zezadu	zezadu	k6eAd1	zezadu
(	(	kIx(	(
<g/>
zrcadlově	zrcadlově	k6eAd1	zrcadlově
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
dírky	dírka	k1gFnPc4	dírka
v	v	k7c6	v
destičce	destička	k1gFnSc6	destička
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
v	v	k7c6	v
papíru	papír	k1gInSc6	papír
body	bod	k1gInPc1	bod
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
pražská	pražský	k2eAgFnSc1d1	Pražská
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rychlejšímu	rychlý	k2eAgNnSc3d2	rychlejší
psaní	psaní	k1gNnSc3	psaní
slouží	sloužit	k5eAaImIp3nS	sloužit
Pichtův	Pichtův	k2eAgInSc4d1	Pichtův
psací	psací	k2eAgInSc4d1	psací
stroj	stroj	k1gInSc4	stroj
či	či	k8xC	či
speciální	speciální	k2eAgFnPc4d1	speciální
počítačové	počítačový	k2eAgFnPc4d1	počítačová
tiskárny	tiskárna	k1gFnPc4	tiskárna
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
nástroj	nástroj	k1gInSc1	nástroj
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
počítačem	počítač	k1gInSc7	počítač
slouží	sloužit	k5eAaImIp3nS	sloužit
nevidomým	nevidomý	k1gMnPc3	nevidomý
hmatový	hmatový	k2eAgInSc4d1	hmatový
displej	displej	k1gInSc4	displej
(	(	kIx(	(
<g/>
též	též	k9	též
braillský	braillský	k2eAgInSc4d1	braillský
řádek	řádek	k1gInSc4	řádek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
převádí	převádět	k5eAaImIp3nS	převádět
část	část	k1gFnSc1	část
jednoho	jeden	k4xCgInSc2	jeden
řádku	řádek	k1gInSc2	řádek
textu	text	k1gInSc2	text
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
z	z	k7c2	z
počítačové	počítačový	k2eAgFnSc2d1	počítačová
obrazovky	obrazovka	k1gFnSc2	obrazovka
<g/>
)	)	kIx)	)
do	do	k7c2	do
hmatové	hmatový	k2eAgFnSc2d1	hmatová
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
základní	základní	k2eAgFnSc2d1	základní
podoby	podoba	k1gFnSc2	podoba
(	(	kIx(	(
<g/>
plnopis	plnopis	k1gInSc1	plnopis
<g/>
,	,	kIx,	,
Grade	grad	k1gInSc5	grad
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
slova	slovo	k1gNnPc1	slovo
přepisují	přepisovat	k5eAaImIp3nP	přepisovat
znak	znak	k1gInSc4	znak
po	po	k7c6	po
znaku	znak	k1gInSc6	znak
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
komplexnější	komplexní	k2eAgFnPc1d2	komplexnější
formy	forma	k1gFnPc1	forma
(	(	kIx(	(
<g/>
zkratkopis	zkratkopis	k1gInSc1	zkratkopis
<g/>
,	,	kIx,	,
Grade	grad	k1gInSc5	grad
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
každý	každý	k3xTgInSc4	každý
znak	znak	k1gInSc1	znak
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
celou	celý	k2eAgFnSc4d1	celá
skupinu	skupina	k1gFnSc4	skupina
hlásek	hláska	k1gFnPc2	hláska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
příliš	příliš	k6eAd1	příliš
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
např.	např.	kA	např.
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
či	či	k8xC	či
němčině	němčina	k1gFnSc6	němčina
se	se	k3xPyFc4	se
Braillovo	Braillův	k2eAgNnSc1d1	Braillovo
písmo	písmo	k1gNnSc1	písmo
používá	používat	k5eAaImIp3nS	používat
převážně	převážně	k6eAd1	převážně
právě	právě	k6eAd1	právě
takto	takto	k6eAd1	takto
a	a	k8xC	a
základní	základní	k2eAgInSc4d1	základní
přepis	přepis	k1gInSc4	přepis
používají	používat	k5eAaImIp3nP	používat
prakticky	prakticky	k6eAd1	prakticky
pouze	pouze	k6eAd1	pouze
začátečníci	začátečník	k1gMnPc1	začátečník
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
Braillova	Braillův	k2eAgNnSc2d1	Braillovo
písma	písmo	k1gNnSc2	písmo
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
znaky	znak	k1gInPc1	znak
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgInPc1d2	veliký
než	než	k8xS	než
znaky	znak	k1gInPc1	znak
běžného	běžný	k2eAgInSc2d1	běžný
barevného	barevný	k2eAgInSc2d1	barevný
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
velikost	velikost	k1gFnSc1	velikost
znaku	znak	k1gInSc2	znak
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
7,5	[number]	k4	7,5
<g/>
×	×	k?	×
<g/>
5	[number]	k4	5
mm	mm	kA	mm
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
na	na	k7c6	na
běžné	běžný	k2eAgFnSc6d1	běžná
straně	strana	k1gFnSc6	strana
formátu	formát	k1gInSc2	formát
A4	A4	k1gFnPc2	A4
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
asi	asi	k9	asi
800	[number]	k4	800
<g/>
–	–	k?	–
<g/>
900	[number]	k4	900
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
běžným	běžný	k2eAgInSc7d1	běžný
tiskem	tisk	k1gInSc7	tisk
se	se	k3xPyFc4	se
na	na	k7c4	na
stejný	stejný	k2eAgInSc4d1	stejný
rozměr	rozměr	k1gInSc4	rozměr
vejde	vejít	k5eAaPmIp3nS	vejít
zhruba	zhruba	k6eAd1	zhruba
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
tisíc	tisíc	k4xCgInPc2	tisíc
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
v	v	k7c6	v
Braillově	Braillův	k2eAgNnSc6d1	Braillovo
písmu	písmo	k1gNnSc6	písmo
jsou	být	k5eAaImIp3nP	být
tudíž	tudíž	k8xC	tudíž
výrazně	výrazně	k6eAd1	výrazně
rozsáhlejší	rozsáhlý	k2eAgFnPc1d2	rozsáhlejší
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
souvisí	souviset	k5eAaImIp3nP	souviset
i	i	k9	i
typografické	typografický	k2eAgInPc1d1	typografický
problémy	problém	k1gInPc1	problém
<g/>
:	:	kIx,	:
jelikož	jelikož	k8xS	jelikož
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
řádek	řádek	k1gInSc4	řádek
se	se	k3xPyFc4	se
vejde	vejít	k5eAaPmIp3nS	vejít
pouze	pouze	k6eAd1	pouze
asi	asi	k9	asi
30	[number]	k4	30
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
používat	používat	k5eAaImF	používat
tabulky	tabulka	k1gFnPc1	tabulka
či	či	k8xC	či
jiné	jiný	k2eAgInPc1d1	jiný
běžné	běžný	k2eAgInPc1d1	běžný
formátovací	formátovací	k2eAgInPc1d1	formátovací
postupy	postup	k1gInPc1	postup
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
všechny	všechen	k3xTgFnPc4	všechen
informace	informace	k1gFnPc4	informace
psát	psát	k5eAaImF	psát
sekvenčně	sekvenčně	k6eAd1	sekvenčně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jen	jen	k9	jen
dále	daleko	k6eAd2	daleko
zvýrazňuje	zvýrazňovat	k5eAaImIp3nS	zvýrazňovat
potřebu	potřeba	k1gFnSc4	potřeba
zvýšeného	zvýšený	k2eAgInSc2d1	zvýšený
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
velkým	velký	k2eAgInSc7d1	velký
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
nejednotnost	nejednotnost	k1gFnSc1	nejednotnost
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
země	zem	k1gFnPc1	zem
používají	používat	k5eAaImIp3nP	používat
různé	různý	k2eAgFnPc1d1	různá
normy	norma	k1gFnPc1	norma
Braillova	Braillův	k2eAgNnSc2d1	Braillovo
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
poměrně	poměrně	k6eAd1	poměrně
výrazně	výrazně	k6eAd1	výrazně
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
i	i	k9	i
jenom	jenom	k9	jenom
mezi	mezi	k7c7	mezi
českou	český	k2eAgFnSc7d1	Česká
a	a	k8xC	a
slovenskou	slovenský	k2eAgFnSc7d1	slovenská
normou	norma	k1gFnSc7	norma
je	být	k5eAaImIp3nS	být
už	už	k9	už
v	v	k7c6	v
základních	základní	k2eAgNnPc6d1	základní
písmenech	písmeno	k1gNnPc6	písmeno
abecedy	abeceda	k1gFnSc2	abeceda
7	[number]	k4	7
rozdílů	rozdíl	k1gInPc2	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
výraznou	výrazný	k2eAgFnSc7d1	výrazná
komplikací	komplikace	k1gFnSc7	komplikace
je	být	k5eAaImIp3nS	být
linearita	linearita	k1gFnSc1	linearita
zápisu	zápis	k1gInSc2	zápis
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
zvláště	zvláště	k6eAd1	zvláště
při	při	k7c6	při
přepisu	přepis	k1gInSc6	přepis
matematických	matematický	k2eAgInPc2d1	matematický
<g/>
,	,	kIx,	,
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
a	a	k8xC	a
chemických	chemický	k2eAgInPc2d1	chemický
textů	text	k1gInPc2	text
-	-	kIx~	-
i	i	k9	i
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
rovnice	rovnice	k1gFnPc1	rovnice
mohou	moct	k5eAaImIp3nP	moct
zabírat	zabírat	k5eAaImF	zabírat
několik	několik	k4yIc4	několik
řádků	řádek	k1gInPc2	řádek
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Braillovo	Braillův	k2eAgNnSc4d1	Braillovo
písmo	písmo	k1gNnSc4	písmo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
