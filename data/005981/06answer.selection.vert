<s>
Brailleovo	Brailleův	k2eAgNnSc1d1	Brailleovo
slepecké	slepecký	k2eAgNnSc1d1	slepecké
písmo	písmo	k1gNnSc1	písmo
(	(	kIx(	(
<g/>
psáno	psán	k2eAgNnSc1d1	psáno
také	také	k9	také
Braillovo	Braillův	k2eAgNnSc1d1	Braillovo
<g/>
;	;	kIx,	;
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
brajovo	brajův	k2eAgNnSc1d1	brajův
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
speciální	speciální	k2eAgInSc4d1	speciální
druh	druh	k1gInSc4	druh
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
systému	systém	k1gInSc2	systém
psaní	psaní	k1gNnSc2	psaní
<g/>
,	,	kIx,	,
určeného	určený	k2eAgInSc2d1	určený
pro	pro	k7c4	pro
nevidomé	nevidomý	k1gMnPc4	nevidomý
<g/>
,	,	kIx,	,
slabozraké	slabozraký	k1gMnPc4	slabozraký
a	a	k8xC	a
se	s	k7c7	s
zbytky	zbytek	k1gInPc7	zbytek
zraku	zrak	k1gInSc2	zrak
<g/>
.	.	kIx.	.
</s>
