<s>
Citadela	citadela	k1gFnSc1	citadela
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
od	od	k7c2	od
Antoina	Antoino	k1gNnSc2	Antoino
de	de	k?	de
Saint	Saint	k1gInSc4	Saint
Exupéryho	Exupéry	k1gMnSc2	Exupéry
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nikdy	nikdy	k6eAd1	nikdy
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
až	až	k9	až
posmrtně	posmrtně	k6eAd1	posmrtně
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
můžou	můžou	k?	můžou
myšlenky	myšlenka	k1gFnPc1	myšlenka
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zdát	zdát	k5eAaPmF	zdát
neucelené	ucelený	k2eNgFnPc1d1	neucelená
a	a	k8xC	a
opakující	opakující	k2eAgFnPc1d1	opakující
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
vydána	vydán	k2eAgFnSc1d1	vydána
v	v	k7c6	v
krácené	krácený	k2eAgFnSc6d1	krácená
verzi	verze	k1gFnSc6	verze
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
čeští	český	k2eAgMnPc1d1	český
čtenáři	čtenář	k1gMnPc1	čtenář
dočkali	dočkat	k5eAaPmAgMnP	dočkat
plné	plný	k2eAgFnPc4d1	plná
verze	verze	k1gFnPc4	verze
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
plná	plný	k2eAgFnSc1d1	plná
filosofických	filosofický	k2eAgFnPc2d1	filosofická
myšlenek	myšlenka	k1gFnPc2	myšlenka
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
popsat	popsat	k5eAaPmF	popsat
o	o	k7c6	o
čem	co	k3yQnSc6	co
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
poutě	pouť	k1gFnSc2	pouť
jedné	jeden	k4xCgFnSc2	jeden
karavany	karavana	k1gFnSc2	karavana
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
autorovy	autorův	k2eAgFnPc4d1	autorova
myšlenky	myšlenka	k1gFnPc4	myšlenka
nastíněné	nastíněný	k2eAgFnPc4d1	nastíněná
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
Malém	malé	k1gNnSc6	malé
Princi	princa	k1gFnSc2	princa
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
jako	jako	k8xC	jako
taková	takový	k3xDgFnSc1	takový
nemá	mít	k5eNaImIp3nS	mít
klasický	klasický	k2eAgInSc4d1	klasický
děj	děj	k1gInSc4	děj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
sled	sled	k1gInSc1	sled
zamyšlení	zamyšlení	k1gNnSc2	zamyšlení
nad	nad	k7c7	nad
různými	různý	k2eAgNnPc7d1	různé
tématy	téma	k1gNnPc7	téma
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
lidského	lidský	k2eAgInSc2d1	lidský
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
směřování	směřování	k1gNnSc2	směřování
a	a	k8xC	a
tužeb	tužba	k1gFnPc2	tužba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
psáná	psáná	k1gFnSc1	psáná
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
moudrého	moudrý	k2eAgMnSc2d1	moudrý
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vede	vést	k5eAaImIp3nS	vést
svůj	svůj	k3xOyFgInSc4	svůj
lid	lid	k1gInSc4	lid
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
naplnit	naplnit	k5eAaPmF	naplnit
<g/>
.	.	kIx.	.
</s>
<s>
Byť	byť	k8xS	byť
někdy	někdy	k6eAd1	někdy
i	i	k9	i
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gFnSc3	jeho
vůli	vůle	k1gFnSc3	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Líčí	líčit	k5eAaImIp3nS	líčit
tedy	tedy	k9	tedy
i	i	k9	i
smysl	smysl	k1gInSc4	smysl
"	"	kIx"	"
<g/>
zla	zlo	k1gNnSc2	zlo
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
lidské	lidský	k2eAgFnSc2d1	lidská
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
lid	lid	k1gInSc4	lid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
člověka	člověk	k1gMnSc4	člověk
samotného	samotný	k2eAgMnSc4d1	samotný
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
k	k	k7c3	k
lodi	loď	k1gFnSc3	loď
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
získává	získávat	k5eAaImIp3nS	získávat
svůj	svůj	k3xOyFgInSc4	svůj
tvar	tvar	k1gInSc4	tvar
(	(	kIx(	(
<g/>
sílu	síla	k1gFnSc4	síla
<g/>
)	)	kIx)	)
díky	díky	k7c3	díky
neustálému	neustálý	k2eAgInSc3d1	neustálý
boji	boj	k1gInSc3	boj
s	s	k7c7	s
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Antoine	Antoinout	k5eAaPmIp3nS	Antoinout
de	de	k?	de
Saint-Exupéry	Saint-Exupér	k1gInPc4	Saint-Exupér
<g/>
:	:	kIx,	:
Citadela	citadela	k1gFnSc1	citadela
<g/>
,	,	kIx,	,
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
překl	překl	k1gInSc1	překl
<g/>
.	.	kIx.	.
</s>
<s>
Věra	Věra	k1gFnSc1	Věra
Dvořáková	Dvořáková	k1gFnSc1	Dvořáková
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
Antoine	Antoin	k1gInSc5	Antoin
de	de	k?	de
Saint-Exupéry	Saint-Exupéra	k1gFnPc1	Saint-Exupéra
<g/>
:	:	kIx,	:
Citadela	citadela	k1gFnSc1	citadela
<g/>
,	,	kIx,	,
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
překl	překl	k1gInSc1	překl
<g/>
.	.	kIx.	.
</s>
<s>
Věra	Věra	k1gFnSc1	Věra
Dvořáková	Dvořáková	k1gFnSc1	Dvořáková
<g/>
,	,	kIx,	,
kompletní	kompletní	k2eAgNnSc1d1	kompletní
vydání	vydání	k1gNnSc1	vydání
Antoine	Antoin	k1gInSc5	Antoin
de	de	k?	de
Saint-Exupéry	Saint-Exupéra	k1gFnSc2	Saint-Exupéra
Malý	Malý	k1gMnSc1	Malý
princ	princ	k1gMnSc1	princ
</s>
