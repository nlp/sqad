<s>
Pony	pony	k1gMnSc1
Express	express	k1gInSc1
</s>
<s>
Pony	pony	k1gMnSc1
Express	express	k1gInSc1
LogoZákladní	LogoZákladný	k2eAgMnPc1d1
údaje	údaj	k1gInSc2
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
1860	#num#	k4
Zakladatelé	zakladatel	k1gMnPc1
</s>
<s>
William	William	k6eAd1
Hepburn	Hepburn	k1gInSc1
RussellAlexander	RussellAlexander	k1gInSc1
MajorsWilliam	MajorsWilliam	k1gInSc4
B.	B.	kA
Waddell	Waddell	k1gInSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pony	pony	k1gMnSc1
Express	express	k1gInSc4
byla	být	k5eAaImAgFnS
kurýrní	kurýrní	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
v	v	k7c6
letech	let	k1gInPc6
1860	#num#	k4
<g/>
–	–	k?
<g/>
61	#num#	k4
přepravovala	přepravovat	k5eAaImAgFnS
poštovní	poštovní	k2eAgFnSc1d1
zásilky	zásilka	k1gFnPc1
z	z	k7c2
východního	východní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
USA	USA	kA
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
západní	západní	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Pomník	pomník	k1gInSc4
Pony	pony	k1gMnSc1
Expressu	express	k1gInSc2
v	v	k7c4
Saint	Saint	k1gInSc4
Joseph	Josepha	k1gFnPc2
</s>
<s>
Frank	Frank	k1gMnSc1
E.	E.	kA
Webner	Webner	k1gMnSc1
<g/>
,	,	kIx,
jezdec	jezdec	k1gMnSc1
Pony	pony	k1gMnSc1
Express	express	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1861	#num#	k4
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Zatímco	zatímco	k8xS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vznikla	vzniknout	k5eAaPmAgFnS
zásluhou	zásluha	k1gFnSc7
šlechtického	šlechtický	k2eAgInSc2d1
rodu	rod	k1gInSc2
Thurn-Taxisů	Thurn-Taxis	k1gInPc2
poštovní	poštovní	k2eAgFnSc1d1
služba	služba	k1gFnSc1
za	za	k7c2
pomoci	pomoc	k1gFnSc2
dostavníků	dostavník	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
Americe	Amerika	k1gFnSc6
bez	bez	k7c2
silnic	silnice	k1gFnPc2
se	se	k3xPyFc4
vývoj	vývoj	k1gInSc1
odlišoval	odlišovat	k5eAaImAgInS
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
přestala	přestat	k5eAaPmAgFnS
být	být	k5eAaImF
města	město	k1gNnPc1
na	na	k7c6
východním	východní	k2eAgInSc6d1
pobřeží	pobřeží	k1gNnSc2
jedinou	jediný	k2eAgFnSc7d1
výspou	výspa	k1gFnSc7
civilizace	civilizace	k1gFnSc2
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
nálezům	nález	k1gInPc3
zlata	zlato	k1gNnSc2
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
prudce	prudko	k6eAd1
rozvíjet	rozvíjet	k5eAaImF
i	i	k9
americký	americký	k2eAgInSc4d1
Západ	západ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Železniční	železniční	k2eAgInSc4d1
spojení	spojení	k1gNnSc1
ještě	ještě	k6eAd1
neexistovalo	existovat	k5eNaImAgNnS
a	a	k8xC
tak	tak	k6eAd1
tím	ten	k3xDgNnSc7
jediným	jediný	k2eAgNnSc7d1
<g/>
,	,	kIx,
co	co	k9
obě	dva	k4xCgNnPc1
pobřeží	pobřeží	k1gNnPc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
spojovalo	spojovat	k5eAaImAgNnS
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
poštovní	poštovní	k2eAgFnPc4d1
linky	linka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInPc1d3
a	a	k8xC
nejznámější	známý	k2eAgInPc1d3
z	z	k7c2
nich	on	k3xPp3gInPc2
byla	být	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
Wells	Wellsa	k1gFnPc2
<g/>
,	,	kIx,
Fargo	Fargo	k1gNnSc4
&	&	k?
Co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
ani	ani	k8xC
její	její	k3xOp3gInPc1
dostavníky	dostavník	k1gInPc1
nedokázaly	dokázat	k5eNaPmAgInP
zásilku	zásilka	k1gFnSc4
z	z	k7c2
východu	východ	k1gInSc2
na	na	k7c4
západ	západ	k1gInSc4
dostat	dostat	k5eAaPmF
dřív	dříve	k6eAd2
než	než	k8xS
za	za	k7c4
tři	tři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
přišla	přijít	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
Russel	Russel	k1gMnSc1
<g/>
,	,	kIx,
Major	major	k1gMnSc1
a	a	k8xC
Waddel	Waddlo	k1gNnPc2
s	s	k7c7
nápadem	nápad	k1gInSc7
na	na	k7c4
zřízení	zřízení	k1gNnSc4
expresní	expresní	k2eAgFnSc2d1
poštovní	poštovní	k2eAgFnSc2d1
linky	linka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
by	by	kYmCp3nS
dobu	doba	k1gFnSc4
přepravy	přeprava	k1gFnSc2
zásilky	zásilka	k1gFnSc2
přes	přes	k7c4
kontinent	kontinent	k1gInSc4
zkrátila	zkrátit	k5eAaPmAgFnS
na	na	k7c4
několik	několik	k4yIc4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeprava	přeprava	k1gFnSc1
zásilek	zásilka	k1gFnPc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
chtěli	chtít	k5eAaImAgMnP
provozovat	provozovat	k5eAaImF
měla	mít	k5eAaImAgNnP
být	být	k5eAaImF
především	především	k6eAd1
rychlá	rychlý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
rozhodli	rozhodnout	k5eAaPmAgMnP
nikoliv	nikoliv	k9
pro	pro	k7c4
dostavníky	dostavník	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
osamělé	osamělý	k2eAgInPc1d1
jezdce	jezdec	k1gInPc1
na	na	k7c6
koních	kůň	k1gMnPc6
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
by	by	kYmCp3nP
dokázali	dokázat	k5eAaPmAgMnP
překonat	překonat	k5eAaPmF
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
za	za	k7c4
mnohem	mnohem	k6eAd1
kratší	krátký	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevýhodou	nevýhoda	k1gFnSc7
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
osamělý	osamělý	k2eAgMnSc1d1
jezdec	jezdec	k1gMnSc1
byl	být	k5eAaImAgMnS
mnohem	mnohem	k6eAd1
zranitelnější	zranitelný	k2eAgMnSc1d2
ze	z	k7c2
strany	strana	k1gFnSc2
Indiánů	Indián	k1gMnPc2
či	či	k8xC
bílých	bílý	k2eAgMnPc2d1
banditů	bandita	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgMnSc1
jezdec	jezdec	k1gMnSc1
(	(	kIx(
<g/>
byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
buď	buď	k8xC
William	William	k1gInSc1
Richarson	Richarsona	k1gFnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
Johny	John	k1gMnPc7
Fry	Fry	k1gFnSc2
<g/>
)	)	kIx)
vyrazil	vyrazit	k5eAaPmAgMnS
na	na	k7c4
trasu	trasa	k1gFnSc4
8	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1860	#num#	k4
(	(	kIx(
<g/>
některé	některý	k3yIgInPc1
zdroje	zdroj	k1gInPc1
uvádějí	uvádět	k5eAaImIp3nP
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1860	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začínalo	začínat	k5eAaImAgNnS
se	se	k3xPyFc4
v	v	k7c4
Saint	Saint	k1gInSc4
Joseph	Josepha	k1gFnPc2
ve	v	k7c6
státě	stát	k1gInSc6
Missouri	Missouri	k1gFnSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
linka	linka	k1gFnSc1
vedla	vést	k5eAaImAgFnS
přes	přes	k7c4
Kansas	Kansas	k1gInSc4
<g/>
,	,	kIx,
Nebrasku	Nebraska	k1gFnSc4
<g/>
,	,	kIx,
Colorado	Colorado	k1gNnSc4
<g/>
,	,	kIx,
Wyoming	Wyoming	k1gInSc4
<g/>
,	,	kIx,
Utah	Utah	k1gInSc4
<g/>
,	,	kIx,
Nevadu	Nevada	k1gFnSc4
a	a	k8xC
končila	končit	k5eAaImAgFnS
v	v	k7c6
Sacramentu	Sacrament	k1gInSc6
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotní	samotný	k2eAgMnPc1d1
poslové	posel	k1gMnPc1
byli	být	k5eAaImAgMnP
vybaveni	vybavit	k5eAaPmNgMnP
jídlem	jídlo	k1gNnSc7
(	(	kIx(
<g/>
suchary	suchar	k1gMnPc4
<g/>
,	,	kIx,
špek	špek	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
polní	polní	k2eAgFnSc7d1
lahví	lahev	k1gFnSc7
s	s	k7c7
pitím	pití	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
svou	svůj	k3xOyFgFnSc4
obranu	obrana	k1gFnSc4
měli	mít	k5eAaImAgMnP
střelnou	střelný	k2eAgFnSc4d1
zbraň	zbraň	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
školení	školení	k1gNnSc6
jim	on	k3xPp3gMnPc3
ale	ale	k9
bylo	být	k5eAaImAgNnS
doporučeno	doporučit	k5eAaPmNgNnS
nemíchat	míchat	k5eNaImF
se	se	k3xPyFc4
do	do	k7c2
žádných	žádný	k3yNgInPc2
sporů	spor	k1gInPc2
a	a	k8xC
pokud	pokud	k8xS
možno	možno	k6eAd1
zmizet	zmizet	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poštovní	poštovní	k2eAgFnPc4d1
zásilky	zásilka	k1gFnPc4
převáželi	převážet	k5eAaImAgMnP
ve	v	k7c6
čtyřech	čtyři	k4xCgFnPc6
kožených	kožený	k2eAgFnPc6d1
brašnách	brašna	k1gFnPc6
<g/>
,	,	kIx,
tzv.	tzv.	kA
mochillách	mochilla	k1gFnPc6
<g/>
,	,	kIx,
upevněných	upevněný	k2eAgInPc2d1
u	u	k7c2
sedla	sedlo	k1gNnSc2
a	a	k8xC
naplněných	naplněný	k2eAgFnPc2d1
asi	asi	k9
15	#num#	k4
kilogramy	kilogram	k1gInPc4
zásilek	zásilka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jezdec	jezdec	k1gInSc1
měl	mít	k5eAaImAgInS
za	za	k7c4
povinnost	povinnost	k1gFnSc4
tryskem	tryskem	k6eAd1
ujíždět	ujíždět	k5eAaImF
k	k	k7c3
nejbližší	blízký	k2eAgFnSc3d3
stanici	stanice	k1gFnSc3
<g/>
,	,	kIx,
vzdálené	vzdálený	k2eAgInPc1d1
asi	asi	k9
20	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
vyměnil	vyměnit	k5eAaPmAgMnS
koně	kůň	k1gMnPc4
a	a	k8xC
pokračoval	pokračovat	k5eAaImAgMnS
v	v	k7c6
cestě	cesta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Zakladatelé	zakladatel	k1gMnPc1
Pony	pony	k1gMnPc1
Expressu	express	k1gInSc2
doufali	doufat	k5eAaImAgMnP
ve	v	k7c4
státní	státní	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
celá	celý	k2eAgFnSc1d1
linka	linka	k1gFnSc1
byla	být	k5eAaImAgFnS
silně	silně	k6eAd1
ztrátová	ztrátový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podpora	podpora	k1gFnSc1
však	však	k9
nepřišla	přijít	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgInSc7
hlavním	hlavní	k2eAgMnSc7d1
<g/>
,	,	kIx,
co	co	k9
Pony	pony	k1gMnPc4
Express	express	k1gInSc1
drželo	držet	k5eAaImAgNnS
nad	nad	k7c7
vodou	voda	k1gFnSc7
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
rychlé	rychlý	k2eAgNnSc1d1
dodávání	dodávání	k1gNnSc1
čerstvých	čerstvý	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
pro	pro	k7c4
noviny	novina	k1gFnPc4
na	na	k7c6
obou	dva	k4xCgNnPc6
pobřežích	pobřeží	k1gNnPc6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomu	ten	k3xDgNnSc3
ale	ale	k9
učinilo	učinit	k5eAaImAgNnS,k5eAaPmAgNnS
konec	konec	k1gInSc4
dokončení	dokončení	k1gNnSc2
výstavby	výstavba	k1gFnSc2
transkontinentálního	transkontinentální	k2eAgInSc2d1
telegrafu	telegraf	k1gInSc2
24	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1861	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pony	pony	k1gMnSc1
Express	express	k1gInSc1
ukončil	ukončit	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
činnost	činnost	k1gFnSc4
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1861	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Statistické	statistický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
celá	celý	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
měřila	měřit	k5eAaImAgFnS
3000	#num#	k4
km	km	kA
</s>
<s>
na	na	k7c6
trase	trasa	k1gFnSc6
bylo	být	k5eAaImAgNnS
190	#num#	k4
stanic	stanice	k1gFnPc2
s	s	k7c7
500	#num#	k4
koňmi	kůň	k1gMnPc7
na	na	k7c6
střídání	střídání	k1gNnSc6
</s>
<s>
vzdálenost	vzdálenost	k1gFnSc1
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgFnPc7d1
stanicemi	stanice	k1gFnPc7
byla	být	k5eAaImAgFnS
v	v	k7c6
průměru	průměr	k1gInSc6
kolem	kolem	k7c2
20	#num#	k4
km	km	kA
</s>
<s>
průměrný	průměrný	k2eAgInSc1d1
věk	věk	k1gInSc1
jezdců	jezdec	k1gInPc2
byl	být	k5eAaImAgInS
18	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
týdenní	týdenní	k2eAgInSc1d1
plat	plat	k1gInSc1
činil	činit	k5eAaImAgInS
25	#num#	k4
dolarů	dolar	k1gInPc2
(	(	kIx(
<g/>
dvojnásobek	dvojnásobek	k1gInSc1
průměrného	průměrný	k2eAgInSc2d1
dělnického	dělnický	k2eAgInSc2d1
platu	plat	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
průměrná	průměrný	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
jezdce	jezdec	k1gInSc2
činila	činit	k5eAaImAgFnS
20	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
jezdec	jezdec	k1gMnSc1
na	na	k7c6
svém	svůj	k3xOyFgInSc6
úseku	úsek	k1gInSc6
vystřídal	vystřídat	k5eAaPmAgInS
asi	asi	k9
10	#num#	k4
koní	kůň	k1gMnPc2
</s>
<s>
výměna	výměna	k1gFnSc1
koně	kůň	k1gMnSc2
trvala	trvat	k5eAaImAgFnS
asi	asi	k9
minutu	minuta	k1gFnSc4
</s>
<s>
traťový	traťový	k2eAgInSc1d1
rekord	rekord	k1gInSc1
<g/>
:	:	kIx,
Robert	Robert	k1gMnSc1
Halsam	Halsam	k1gInSc4
projel	projet	k5eAaPmAgMnS
celou	celý	k2eAgFnSc4d1
trasu	trasa	k1gFnSc4
za	za	k7c4
7	#num#	k4
dní	den	k1gInPc2
a	a	k8xC
17	#num#	k4
hodin	hodina	k1gFnPc2
</s>
<s>
linka	linka	k1gFnSc1
fungovala	fungovat	k5eAaImAgFnS
18	#num#	k4
měsíců	měsíc	k1gInPc2
</s>
<s>
jezdci	jezdec	k1gMnPc1
uskutečnili	uskutečnit	k5eAaPmAgMnP
305	#num#	k4
jízd	jízda	k1gFnPc2
v	v	k7c6
obou	dva	k4xCgInPc6
směrech	směr	k1gInPc6
</s>
<s>
poštovné	poštovné	k1gNnSc1
činilo	činit	k5eAaImAgNnS
5	#num#	k4
dolarů	dolar	k1gInPc2
za	za	k7c4
zásilku	zásilka	k1gFnSc4
do	do	k7c2
půl	půl	k1xP
unce	unce	k1gFnSc2
<g/>
,	,	kIx,
za	za	k7c4
každou	každý	k3xTgFnSc4
půlunci	půlunec	k1gMnSc3
byl	být	k5eAaImAgMnS
příplatek	příplatek	k1gInSc4
1	#num#	k4
dolar	dolar	k1gInSc4
</s>
<s>
za	za	k7c4
dobu	doba	k1gFnSc4
své	svůj	k3xOyFgFnSc2
existence	existence	k1gFnSc2
Pony	pony	k1gMnSc1
Express	express	k1gInSc4
doručil	doručit	k5eAaPmAgMnS
34	#num#	k4
753	#num#	k4
zásilek	zásilka	k1gFnPc2
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Jezdci	jezdec	k1gMnPc1
skládali	skládat	k5eAaImAgMnP
při	při	k7c6
přijetí	přijetí	k1gNnSc6
do	do	k7c2
Pony	pony	k1gMnPc1
Expressu	express	k1gInSc6
tuto	tento	k3xDgFnSc4
přísahu	přísaha	k1gFnSc4
<g/>
:	:	kIx,
Nebudu	být	k5eNaImBp1nS
klít	klít	k5eAaImF
<g/>
,	,	kIx,
nebudu	být	k5eNaImBp1nS
požívat	požívat	k5eAaImF
opojné	opojný	k2eAgInPc4d1
nápoje	nápoj	k1gInPc4
<g/>
,	,	kIx,
nebudu	být	k5eNaImBp1nS
vyhledávat	vyhledávat	k5eAaImF
hádky	hádka	k1gFnPc4
a	a	k8xC
spory	spor	k1gInPc4
<g/>
,	,	kIx,
budu	být	k5eAaImBp1nS
jednat	jednat	k5eAaImF
čestně	čestně	k6eAd1
a	a	k8xC
svěřený	svěřený	k2eAgInSc4d1
úsek	úsek	k1gInSc4
zdolám	zdolat	k5eAaPmIp1nS
i	i	k9
za	za	k7c4
cenu	cena	k1gFnSc4
života	život	k1gInSc2
ve	v	k7c6
stanoveném	stanovený	k2eAgInSc6d1
čase	čas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgNnSc3
mi	já	k3xPp1nSc3
dopomáhej	dopomáhat	k5eAaImRp2nS
Bůh	bůh	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3
jezdcem	jezdec	k1gMnSc7
Pony	pony	k1gMnSc1
Expressu	express	k1gInSc2
byl	být	k5eAaImAgMnS
William	William	k1gInSc4
Frederic	Frederic	k1gMnSc1
Cody	coda	k1gFnSc2
zvaný	zvaný	k2eAgMnSc1d1
Buffalo	Buffalo	k1gMnSc1
Bill	Bill	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
začal	začít	k5eAaPmAgMnS
na	na	k7c6
lince	linka	k1gFnSc6
jezdit	jezdit	k5eAaImF
už	už	k9
ve	v	k7c6
svých	svůj	k3xOyFgNnPc6
patnácti	patnáct	k4xCc6
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1
z	z	k7c2
jezdců	jezdec	k1gMnPc2
se	se	k3xPyFc4
přihlásili	přihlásit	k5eAaPmAgMnP
na	na	k7c4
proslulý	proslulý	k2eAgInSc4d1
inzerát	inzerát	k1gInSc4
<g/>
:	:	kIx,
Nabízíme	nabízet	k5eAaImIp1nP
zaměstnání	zaměstnání	k1gNnSc4
-	-	kIx~
mladým	mladý	k1gMnPc3
<g/>
,	,	kIx,
hubeným	hubený	k2eAgMnPc3d1
<g/>
,	,	kIx,
šlachovitým	šlachovitý	k2eAgMnPc3d1
mužům	muž	k1gMnPc3
do	do	k7c2
osmnácti	osmnáct	k4xCc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
zdatní	zdatný	k2eAgMnPc1d1
jezdci	jezdec	k1gMnPc1
<g/>
,	,	kIx,
odhodlaní	odhodlaný	k2eAgMnPc1d1
denně	denně	k6eAd1
podstupovat	podstupovat	k5eAaImF
riziko	riziko	k1gNnSc4
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sirotci	Sirotek	k1gMnPc1
mají	mít	k5eAaImIp3nP
přednost	přednost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mzda	mzda	k1gFnSc1
25	#num#	k4
dolarů	dolar	k1gInPc2
týdně	týdně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Pony	pony	k1gMnSc1
Express	express	k1gInSc4
si	se	k3xPyFc3
i	i	k9
přes	přes	k7c4
krátkou	krátký	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
své	svůj	k3xOyFgFnSc2
existence	existence	k1gFnSc2
získal	získat	k5eAaPmAgInS
nesmrtelnou	smrtelný	k2eNgFnSc4d1
slávu	sláva	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
později	pozdě	k6eAd2
přiživovali	přiživovat	k5eAaImAgMnP
i	i	k9
slavní	slavný	k2eAgMnPc1d1
pistolníci	pistolník	k1gMnPc1
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
nikdy	nikdy	k6eAd1
žádnou	žádný	k3yNgFnSc4
poštu	pošta	k1gFnSc4
nevezli	vézt	k5eNaImAgMnP
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
DOLEŽAL	Doležal	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
DOLEŽALOVÁ	Doležalová	k1gFnSc1
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
a	a	k8xC
kůň	kůň	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
:	:	kIx,
Dona	dona	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85463	#num#	k4
<g/>
-	-	kIx~
<g/>
52	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Cesta	cesta	k1gFnSc1
na	na	k7c4
vrchol	vrchol	k1gInSc4
a	a	k8xC
pád	pád	k1gInSc4
<g/>
,	,	kIx,
s.	s.	k?
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Pony	pony	k1gMnPc1
Express	express	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Stanice	stanice	k1gFnSc1
Pony	pony	k1gMnSc1
Expressu	express	k1gInSc2
v	v	k7c6
Kansasu	Kansas	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Koně	kůň	k1gMnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4456071-0	4456071-0	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85104826	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85104826	#num#	k4
</s>
