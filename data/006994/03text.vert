<s>
Lauri	Lauri	k6eAd1	Lauri
Kulpsoo	Kulpsoo	k6eAd1	Kulpsoo
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
estonský	estonský	k2eAgMnSc1d1	estonský
fotograf	fotograf	k1gMnSc1	fotograf
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
gymnáziu	gymnázium	k1gNnSc6	gymnázium
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
geografii	geografie	k1gFnSc4	geografie
na	na	k7c4	na
Tartu	Tarta	k1gFnSc4	Tarta
Ülikool	Ülikoola	k1gFnPc2	Ülikoola
<g/>
.	.	kIx.	.
</s>
<s>
Lauri	Lauri	k6eAd1	Lauri
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
fotograf	fotograf	k1gMnSc1	fotograf
pro	pro	k7c4	pro
noviny	novina	k1gFnPc4	novina
Postimees	Postimeesa	k1gFnPc2	Postimeesa
<g/>
,	,	kIx,	,
časopis	časopis	k1gInSc1	časopis
Pere	prát	k5eAaImIp3nS	prát
ja	ja	k?	ja
Kodu	Kodus	k1gInSc2	Kodus
(	(	kIx(	(
<g/>
Domov	domov	k1gInSc1	domov
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
přispívá	přispívat	k5eAaImIp3nS	přispívat
do	do	k7c2	do
estonských	estonský	k2eAgNnPc2d1	Estonské
a	a	k8xC	a
finských	finský	k2eAgNnPc2d1	finské
médií	médium	k1gNnPc2	médium
<g/>
.	.	kIx.	.
</s>
