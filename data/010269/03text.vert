<p>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
Těšínská	Těšínská	k1gFnSc1	Těšínská
(	(	kIx(	(
<g/>
†	†	k?	†
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1416	[number]	k4	1416
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
těšínská	těšínský	k2eAgFnSc1d1	těšínská
knížecí	knížecí	k2eAgFnSc1d1	knížecí
princezna	princezna	k1gFnSc1	princezna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prožila	prožít	k5eAaPmAgFnS	prožít
většinu	většina	k1gFnSc4	většina
života	život	k1gInSc2	život
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
dcera	dcera	k1gFnSc1	dcera
těšínského	těšínský	k2eAgMnSc2d1	těšínský
knížete	kníže	k1gMnSc2	kníže
Přemysla	Přemysl	k1gMnSc2	Přemysl
I.	I.	kA	I.
Nošáka	Nošák	k1gMnSc2	Nošák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
vyjednávačů	vyjednávač	k1gMnPc2	vyjednávač
sňatkové	sňatkový	k2eAgFnSc2d1	sňatková
aliance	aliance	k1gFnSc2	aliance
mezi	mezi	k7c7	mezi
Lucemburky	Lucemburk	k1gInPc7	Lucemburk
a	a	k8xC	a
Plantagenety	Plantagenet	k1gInPc7	Plantagenet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1381	[number]	k4	1381
se	se	k3xPyFc4	se
Markéta	Markéta	k1gFnSc1	Markéta
dostala	dostat	k5eAaPmAgFnS	dostat
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
Anny	Anna	k1gFnSc2	Anna
České	český	k2eAgFnSc2d1	Česká
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
svatby	svatba	k1gFnSc2	svatba
své	svůj	k3xOyFgFnSc2	svůj
paní	paní	k1gFnSc2	paní
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
Simona	Simon	k1gMnSc4	Simon
Felbrigga	Felbrigg	k1gMnSc4	Felbrigg
<g/>
,	,	kIx,	,
budoucího	budoucí	k2eAgMnSc4d1	budoucí
praporečníka	praporečník	k1gMnSc4	praporečník
krále	král	k1gMnSc4	král
Richarda	Richard	k1gMnSc4	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
připojila	připojit	k5eAaPmAgFnS	připojit
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
k	k	k7c3	k
vlně	vlna	k1gFnSc3	vlna
českoanglických	českoanglický	k2eAgInPc2d1	českoanglický
sňatků	sňatek	k1gInPc2	sňatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Právě	právě	k9	právě
svatba	svatba	k1gFnSc1	svatba
se	s	k7c7	s
Šimonem	Šimon	k1gMnSc7	Šimon
ji	on	k3xPp3gFnSc4	on
zachránila	zachránit	k5eAaPmAgFnS	zachránit
před	před	k7c7	před
vyhoštěním	vyhoštění	k1gNnSc7	vyhoštění
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
postihlo	postihnout	k5eAaPmAgNnS	postihnout
většinu	většina	k1gFnSc4	většina
českého	český	k2eAgInSc2d1	český
doprovodu	doprovod	k1gInSc2	doprovod
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1388	[number]	k4	1388
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1416	[number]	k4	1416
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Markéty	Markéta	k1gFnSc2	Markéta
ve	v	k7c6	v
Felbriggu	Felbrigg	k1gInSc6	Felbrigg
(	(	kIx(	(
<g/>
hrabství	hrabství	k1gNnSc1	hrabství
Norfolk	Norfolka	k1gFnPc2	Norfolka
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
mosazným	mosazný	k2eAgInSc7d1	mosazný
náhrobkem	náhrobek	k1gInSc7	náhrobek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
její	její	k3xOp3gFnSc2	její
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
kniha	kniha	k1gFnSc1	kniha
hodinek	hodinka	k1gFnPc2	hodinka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c4	v
Huntington	Huntington	k1gInSc4	Huntington
Library	Librara	k1gFnSc2	Librara
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
modliteb	modlitba	k1gFnPc2	modlitba
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
a	a	k8xC	a
angličtině	angličtina	k1gFnSc6	angličtina
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
jednu	jeden	k4xCgFnSc4	jeden
modlitbu	modlitba	k1gFnSc4	modlitba
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bibliografie	bibliografie	k1gFnSc2	bibliografie
==	==	k?	==
</s>
</p>
<p>
<s>
Małysz	Małysz	k1gMnSc1	Małysz
<g/>
,	,	kIx,	,
Bohdan	Bohdan	k1gMnSc1	Bohdan
<g/>
:	:	kIx,	:
Markéta	Markéta	k1gFnSc1	Markéta
Těšínská	Těšínská	k1gFnSc1	Těšínská
<g/>
:	:	kIx,	:
zapomenutá	zapomenutý	k2eAgFnSc1d1	zapomenutá
dcera	dcera	k1gFnSc1	dcera
Přemysla	Přemysl	k1gMnSc2	Přemysl
I.	I.	kA	I.
Nošáka	Nošák	k1gMnSc2	Nošák
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
Richarda	Richard	k1gMnSc2	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Těšínsko	Těšínsko	k1gNnSc1	Těšínsko
<g/>
:	:	kIx,	:
vlastivědný	vlastivědný	k2eAgInSc1d1	vlastivědný
časopis	časopis	k1gInSc1	časopis
okresů	okres	k1gInPc2	okres
Frýdek-Místek	Frýdek-Místek	k1gInSc1	Frýdek-Místek
a	a	k8xC	a
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
55	[number]	k4	55
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prinke	Prink	k1gFnPc1	Prink
<g/>
,	,	kIx,	,
Rafał	Rafał	k1gFnPc1	Rafał
T.	T.	kA	T.
–	–	k?	–
Sikorski	Sikorski	k1gNnSc1	Sikorski
<g/>
,	,	kIx,	,
Andrzej	Andrzej	k1gFnSc1	Andrzej
<g/>
:	:	kIx,	:
Małgorzata	Małgorzata	k1gFnSc1	Małgorzata
z	z	k7c2	z
Felbrigg	Felbrigga	k1gFnPc2	Felbrigga
<g/>
:	:	kIx,	:
Piastówna	Piastówen	k2eAgFnSc1d1	Piastówen
cieszyńska	cieszyńska	k1gFnSc1	cieszyńska
na	na	k7c4	na
dworze	dworze	k1gFnPc4	dworze
Ryszarda	Ryszard	k1gMnSc2	Ryszard
II	II	kA	II
króla	król	k1gMnSc2	król
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Roczniki	Roczniki	k1gNnSc1	Roczniki
Historyczne	Historyczne	k1gFnPc2	Historyczne
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
67	[number]	k4	67
<g/>
,	,	kIx,	,
s.	s.	k?	s.
107	[number]	k4	107
<g/>
-	-	kIx~	-
<g/>
130	[number]	k4	130
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
<g/>
:	:	kIx,	:
Margaret	Margareta	k1gFnPc2	Margareta
of	of	k?	of
Teschen	Teschna	k1gFnPc2	Teschna
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Czech	Czech	k1gMnSc1	Czech
Prayer	Prayer	k1gMnSc1	Prayer
<g/>
:	:	kIx,	:
Transnatinalism	Transnatinalism	k1gMnSc1	Transnatinalism
and	and	k?	and
Female	Femala	k1gFnSc3	Femala
Literacy	Literaca	k1gFnSc2	Literaca
in	in	k?	in
the	the	k?	the
Later	Later	k1gInSc1	Later
Middle	Middle	k1gMnSc1	Middle
Ages	Ages	k1gInSc1	Ages
<g/>
.	.	kIx.	.
</s>
<s>
Huntington	Huntington	k1gInSc1	Huntington
Library	Librara	k1gFnSc2	Librara
Quaterly	Quaterla	k1gFnSc2	Quaterla
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
74	[number]	k4	74
<g/>
,	,	kIx,	,
č.	č.	k?	č.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
309	[number]	k4	309
<g/>
-	-	kIx~	-
<g/>
323	[number]	k4	323
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
Cieszyńska	Cieszyńska	k1gFnSc1	Cieszyńska
księżniczka	księżniczka	k1gFnSc1	księżniczka
na	na	k7c6	na
Wyspach	Wyspa	k1gFnPc6	Wyspa
(	(	kIx(	(
<g/>
Gazeta	gazeta	k1gFnSc1	gazeta
Codzienna	Codzienna	k1gFnSc1	Codzienna
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
Dodatkowe	Dodatkowe	k1gFnSc1	Dodatkowe
ilustracje	ilustracje	k1gFnSc1	ilustracje
i	i	k8xC	i
tablice	tablice	k1gFnSc1	tablice
do	do	k7c2	do
artykułu	artykułus	k1gInSc2	artykułus
Małgorzata	Małgorzat	k1gMnSc2	Małgorzat
z	z	k7c2	z
Felbrigg	Felbrigga	k1gFnPc2	Felbrigga
</s>
</p>
