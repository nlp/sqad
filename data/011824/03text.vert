<p>
<s>
Exoplaneta	Exoplaneta	k1gFnSc1	Exoplaneta
<g/>
,	,	kIx,	,
též	též	k9	též
extrasolární	extrasolární	k2eAgFnSc1d1	extrasolární
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
planeta	planeta	k1gFnSc1	planeta
obíhající	obíhající	k2eAgFnSc1d1	obíhající
kolem	kolem	k7c2	kolem
jiné	jiný	k2eAgFnSc2d1	jiná
hvězdy	hvězda	k1gFnSc2	hvězda
než	než	k8xS	než
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
exoplaneta	exoplaneta	k1gFnSc1	exoplaneta
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
potvrzení	potvrzení	k1gNnSc4	potvrzení
musela	muset	k5eAaImAgFnS	muset
počkat	počkat	k5eAaPmF	počkat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
;	;	kIx,	;
první	první	k4xOgFnSc1	první
potvrzená	potvrzený	k2eAgFnSc1d1	potvrzená
exoplaneta	exoplaneta	k1gFnSc1	exoplaneta
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prvního	první	k4xOgInSc2	první
objevu	objev	k1gInSc2	objev
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
3	[number]	k4	3
903	[number]	k4	903
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
<g/>
.	.	kIx.	.
</s>
<s>
Nalézají	nalézat	k5eAaImIp3nP	nalézat
se	se	k3xPyFc4	se
v	v	k7c6	v
2	[number]	k4	2
909	[number]	k4	909
planetárních	planetární	k2eAgFnPc6d1	planetární
soustavách	soustava	k1gFnPc6	soustava
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
647	[number]	k4	647
je	být	k5eAaImIp3nS	být
vícenásobných	vícenásobný	k2eAgFnPc2d1	vícenásobná
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
exoplanet	exoplanet	k1gMnSc1	exoplanet
objevil	objevit	k5eAaPmAgMnS	objevit
vesmírný	vesmírný	k2eAgInSc4d1	vesmírný
teleskop	teleskop	k1gInSc4	teleskop
Kepler	Kepler	k1gMnSc1	Kepler
<g/>
,	,	kIx,	,
aktivní	aktivní	k2eAgMnSc1d1	aktivní
především	především	k9	především
v	v	k7c6	v
letech	let	k1gInPc6	let
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
kontě	konto	k1gNnSc6	konto
přes	přes	k7c4	přes
2	[number]	k4	2
000	[number]	k4	000
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
a	a	k8xC	a
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInPc2	tisíc
planetárních	planetární	k2eAgMnPc2d1	planetární
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
</s>
<s>
Prvním	první	k4xOgInSc7	první
dalekohledem	dalekohled	k1gInSc7	dalekohled
specializovaným	specializovaný	k2eAgInSc7d1	specializovaný
na	na	k7c4	na
exoplanety	exoplanet	k1gInPc4	exoplanet
byl	být	k5eAaImAgInS	být
spektrograf	spektrograf	k1gInSc1	spektrograf
HARPS	HARPS	kA	HARPS
stále	stále	k6eAd1	stále
pracující	pracující	k2eAgMnSc1d1	pracující
na	na	k7c6	na
observatoři	observatoř	k1gFnSc6	observatoř
La	la	k1gNnSc2	la
Silla	Sillo	k1gNnSc2	Sillo
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
objevil	objevit	k5eAaPmAgInS	objevit
přes	přes	k7c4	přes
130	[number]	k4	130
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
dosavadních	dosavadní	k2eAgNnPc2d1	dosavadní
pozorování	pozorování	k1gNnPc2	pozorování
připadá	připadat	k5eAaImIp3nS	připadat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
na	na	k7c4	na
každou	každý	k3xTgFnSc4	každý
hvězdu	hvězda	k1gFnSc4	hvězda
nejméně	málo	k6eAd3	málo
jedna	jeden	k4xCgFnSc1	jeden
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
velké	velký	k2eAgNnSc1d1	velké
procento	procento	k1gNnSc1	procento
hvězd	hvězda	k1gFnPc2	hvězda
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
hvězd	hvězda	k1gFnPc2	hvězda
podobných	podobný	k2eAgFnPc2d1	podobná
Slunci	slunce	k1gNnSc3	slunce
má	mít	k5eAaImIp3nS	mít
planetu	planeta	k1gFnSc4	planeta
velikosti	velikost	k1gFnSc2	velikost
Země	zem	k1gFnSc2	zem
v	v	k7c6	v
obyvatelné	obyvatelný	k2eAgFnSc6d1	obyvatelná
zóně	zóna	k1gFnSc6	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
Galaxii	galaxie	k1gFnSc6	galaxie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
200	[number]	k4	200
miliard	miliarda	k4xCgFnPc2	miliarda
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
existuje	existovat	k5eAaImIp3nS	existovat
11	[number]	k4	11
miliard	miliarda	k4xCgFnPc2	miliarda
potenciálně	potenciálně	k6eAd1	potenciálně
obyvatelných	obyvatelný	k2eAgFnPc2d1	obyvatelná
planet	planeta	k1gFnPc2	planeta
pozemského	pozemský	k2eAgInSc2d1	pozemský
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
40	[number]	k4	40
miliard	miliarda	k4xCgFnPc2	miliarda
<g/>
,	,	kIx,	,
pokud	pokud	k6eAd1	pokud
zahrneme	zahrnout	k5eAaPmIp1nP	zahrnout
i	i	k9	i
planety	planeta	k1gFnPc1	planeta
obíhající	obíhající	k2eAgFnPc1d1	obíhající
okolo	okolo	k7c2	okolo
početných	početný	k2eAgFnPc2d1	početná
červených	červená	k1gFnPc2	červená
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
.	.	kIx.	.
<g/>
Nejméně	málo	k6eAd3	málo
hmotnou	hmotný	k2eAgFnSc7d1	hmotná
známou	známý	k2eAgFnSc7d1	známá
planetou	planeta	k1gFnSc7	planeta
je	být	k5eAaImIp3nS	být
Draugr	Draugr	k1gInSc1	Draugr
(	(	kIx(	(
<g/>
PSR	PSR	kA	PSR
B	B	kA	B
<g/>
1257	[number]	k4	1257
<g/>
+	+	kIx~	+
<g/>
12	[number]	k4	12
A	A	kA	A
nebo	nebo	k8xC	nebo
PSR	PSR	kA	PSR
B	B	kA	B
<g/>
1257	[number]	k4	1257
<g/>
+	+	kIx~	+
<g/>
12	[number]	k4	12
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
obíhá	obíhat	k5eAaImIp3nS	obíhat
okolo	okolo	k7c2	okolo
pulsaru	pulsar	k1gInSc2	pulsar
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
planeta	planeta	k1gFnSc1	planeta
uvedená	uvedený	k2eAgFnSc1d1	uvedená
v	v	k7c6	v
NASA	NASA	kA	NASA
Exoplanet	Exoplanet	k1gMnSc1	Exoplanet
Archive	archiv	k1gInSc5	archiv
je	být	k5eAaImIp3nS	být
DENIS-P	DENIS-P	k1gFnPc2	DENIS-P
J	J	kA	J
<g/>
0	[number]	k4	0
<g/>
82303.1	[number]	k4	82303.1
<g/>
-	-	kIx~	-
<g/>
491201	[number]	k4	491201
b	b	k?	b
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
29	[number]	k4	29
<g/>
násobek	násobek	k1gInSc4	násobek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
definic	definice	k1gFnPc2	definice
na	na	k7c4	na
planetu	planeta	k1gFnSc4	planeta
příliš	příliš	k6eAd1	příliš
hmotná	hmotný	k2eAgFnSc1d1	hmotná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
hnědým	hnědý	k2eAgMnSc7d1	hnědý
trpaslíkem	trpaslík	k1gMnSc7	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
oběhnou	oběhnout	k5eAaPmIp3nP	oběhnout
svou	svůj	k3xOyFgFnSc4	svůj
hvězdu	hvězda	k1gFnSc4	hvězda
za	za	k7c4	za
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
oběžná	oběžný	k2eAgFnSc1d1	oběžná
doba	doba	k1gFnSc1	doba
trvá	trvat	k5eAaImIp3nS	trvat
tisíce	tisíc	k4xCgInSc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
gravitačně	gravitačně	k6eAd1	gravitačně
vázané	vázaný	k2eAgNnSc1d1	vázané
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
dosud	dosud	k6eAd1	dosud
zjištěné	zjištěný	k2eAgFnPc1d1	zjištěná
planety	planeta	k1gFnPc1	planeta
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
uvnitř	uvnitř	k7c2	uvnitř
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
možné	možný	k2eAgFnPc1d1	možná
detekce	detekce	k1gFnPc1	detekce
extragalaktických	extragalaktický	k2eAgFnPc2d1	extragalaktická
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgFnSc1d3	nejbližší
exoplaneta	exoplanet	k2eAgFnSc1d1	exoplaneta
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
b	b	k?	b
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
4,2	[number]	k4	4,2
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1,3	[number]	k4	1,3
parseků	parsec	k1gInPc2	parsec
<g/>
)	)	kIx)	)
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
obíhá	obíhat	k5eAaImIp3nS	obíhat
okolo	okolo	k7c2	okolo
Proximy	Proxima	k1gFnSc2	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
,	,	kIx,	,
hvězdy	hvězda	k1gFnSc2	hvězda
nejblíže	blízce	k6eAd3	blízce
Slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
<g/>
Objev	objev	k1gInSc4	objev
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
zesílil	zesílit	k5eAaPmAgMnS	zesílit
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
hledání	hledání	k1gNnSc4	hledání
mimozemského	mimozemský	k2eAgInSc2d1	mimozemský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
pozornost	pozornost	k1gFnSc1	pozornost
především	především	k6eAd1	především
planetám	planeta	k1gFnPc3	planeta
obíhajícím	obíhající	k2eAgFnPc3d1	obíhající
v	v	k7c6	v
obyvatelné	obyvatelný	k2eAgFnSc6d1	obyvatelná
zóně	zóna	k1gFnSc6	zóna
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
povrchu	povrch	k1gInSc6	povrch
existuje	existovat	k5eAaImIp3nS	existovat
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
kapalném	kapalný	k2eAgNnSc6d1	kapalné
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
předpokladem	předpoklad	k1gInSc7	předpoklad
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
planetární	planetární	k2eAgFnSc2d1	planetární
habitability	habitabilita	k1gFnSc2	habitabilita
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
řadou	řada	k1gFnSc7	řada
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
obyvatelnost	obyvatelnost	k1gFnSc4	obyvatelnost
planety	planeta	k1gFnSc2	planeta
pro	pro	k7c4	pro
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
<g/>
Vedle	vedle	k7c2	vedle
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
toulavé	toulavý	k2eAgFnPc1d1	Toulavá
planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
neobíhají	obíhat	k5eNaImIp3nP	obíhat
kolem	kolem	k7c2	kolem
žádné	žádný	k3yNgFnSc2	žádný
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
samostatné	samostatný	k2eAgInPc4d1	samostatný
objekty	objekt	k1gInPc4	objekt
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
plynnými	plynný	k2eAgMnPc7d1	plynný
obry	obr	k1gMnPc7	obr
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
WISE	WISE	kA	WISE
0	[number]	k4	0
<g/>
855	[number]	k4	855
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
714	[number]	k4	714
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
hnědým	hnědý	k2eAgInSc7d1	hnědý
podtrpaslíkem	podtrpaslík	k1gInSc7	podtrpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
toulavých	toulavý	k2eAgFnPc2d1	Toulavá
planet	planeta	k1gFnPc2	planeta
v	v	k7c4	v
naši	náš	k3xOp1gFnSc4	náš
Galaxii	galaxie	k1gFnSc4	galaxie
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
miliard	miliarda	k4xCgFnPc2	miliarda
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pojmenování	pojmenování	k1gNnPc1	pojmenování
==	==	k?	==
</s>
</p>
<p>
<s>
Konvence	konvence	k1gFnSc1	konvence
pro	pro	k7c4	pro
pojmenování	pojmenování	k1gNnSc4	pojmenování
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
je	být	k5eAaImIp3nS	být
rozšířením	rozšíření	k1gNnSc7	rozšíření
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
označování	označování	k1gNnSc4	označování
vícenásobných	vícenásobný	k2eAgInPc2d1	vícenásobný
hvězdných	hvězdný	k2eAgInPc2d1	hvězdný
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
přijatého	přijatý	k2eAgNnSc2d1	přijaté
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
astronomickou	astronomický	k2eAgFnSc7d1	astronomická
unií	unie	k1gFnSc7	unie
(	(	kIx(	(
<g/>
IAU	IAU	kA	IAU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Exoplanety	Exoplanet	k1gInPc1	Exoplanet
obíhající	obíhající	k2eAgFnSc4d1	obíhající
jednu	jeden	k4xCgFnSc4	jeden
hvězdu	hvězda	k1gFnSc4	hvězda
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
obvykle	obvykle	k6eAd1	obvykle
jménem	jméno	k1gNnSc7	jméno
nebo	nebo	k8xC	nebo
častěji	často	k6eAd2	často
označením	označení	k1gNnSc7	označení
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
přidáním	přidání	k1gNnSc7	přidání
malého	malý	k2eAgNnSc2d1	malé
písmena	písmeno	k1gNnSc2	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
planeta	planeta	k1gFnSc1	planeta
objevená	objevený	k2eAgFnSc1d1	objevená
v	v	k7c6	v
systému	systém	k1gInSc6	systém
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
písmenem	písmeno	k1gNnSc7	písmeno
"	"	kIx"	"
<g/>
b	b	k?	b
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
mateřská	mateřský	k2eAgFnSc1d1	mateřská
hvězda	hvězda	k1gFnSc1	hvězda
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
"	"	kIx"	"
<g/>
a	a	k8xC	a
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
objevené	objevený	k2eAgFnPc1d1	objevená
planety	planeta	k1gFnPc1	planeta
se	se	k3xPyFc4	se
pojmenují	pojmenovat	k5eAaPmIp3nP	pojmenovat
vždy	vždy	k6eAd1	vždy
následujícím	následující	k2eAgNnSc7d1	následující
písmenem	písmeno	k1gNnSc7	písmeno
v	v	k7c6	v
abecedě	abeceda	k1gFnSc6	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
předběžná	předběžný	k2eAgFnSc1d1	předběžná
norma	norma	k1gFnSc1	norma
schválená	schválený	k2eAgNnPc4d1	schválené
IAU	IAU	kA	IAU
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
řeší	řešit	k5eAaImIp3nS	řešit
označování	označování	k1gNnSc4	označování
planet	planeta	k1gFnPc2	planeta
obíhajících	obíhající	k2eAgFnPc2d1	obíhající
okolo	okolo	k7c2	okolo
dvojhvězd	dvojhvězda	k1gFnPc2	dvojhvězda
<g/>
.	.	kIx.	.
</s>
<s>
Omezený	omezený	k2eAgInSc1d1	omezený
počet	počet	k1gInSc1	počet
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgNnPc4d1	vlastní
jména	jméno	k1gNnPc4	jméno
schválená	schválený	k2eAgNnPc4d1	schválené
IAU	IAU	kA	IAU
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
standardy	standard	k1gInPc1	standard
pro	pro	k7c4	pro
pojmenování	pojmenování	k1gNnSc4	pojmenování
exoplanet	exoplanet	k1gInSc4	exoplanet
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Rané	raný	k2eAgFnPc1d1	raná
spekulace	spekulace	k1gFnPc1	spekulace
===	===	k?	===
</s>
</p>
<p>
<s>
Vědci	vědec	k1gMnPc1	vědec
<g/>
,	,	kIx,	,
filozofové	filozof	k1gMnPc1	filozof
a	a	k8xC	a
autoři	autor	k1gMnPc1	autor
science	scienec	k1gInSc2	scienec
fiction	fiction	k1gInSc1	fiction
se	se	k3xPyFc4	se
po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
staletí	staletí	k1gNnPc4	staletí
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
planety	planeta	k1gFnPc1	planeta
mimo	mimo	k7c4	mimo
naši	náš	k3xOp1gFnSc4	náš
sluneční	sluneční	k2eAgFnSc4d1	sluneční
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neexistoval	existovat	k5eNaImAgInS	existovat
žádný	žádný	k3yNgInSc1	žádný
způsob	způsob	k1gInSc1	způsob
jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
objevit	objevit	k5eAaPmF	objevit
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
zjistit	zjistit	k5eAaPmF	zjistit
jejich	jejich	k3xOp3gFnSc1	jejich
početnost	početnost	k1gFnSc1	početnost
či	či	k8xC	či
podobnost	podobnost	k1gFnSc1	podobnost
s	s	k7c7	s
planetami	planeta	k1gFnPc7	planeta
naší	náš	k3xOp1gFnSc2	náš
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Víru	víra	k1gFnSc4	víra
v	v	k7c6	v
existenci	existence	k1gFnSc6	existence
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
již	již	k6eAd1	již
v	v	k7c6	v
učení	učení	k1gNnSc6	učení
starořeckého	starořecký	k2eAgMnSc2d1	starořecký
filozofa	filozof	k1gMnSc2	filozof
Epikúra	Epikúr	k1gMnSc2	Epikúr
ze	z	k7c2	z
Samu	Samos	k1gInSc2	Samos
(	(	kIx(	(
<g/>
341	[number]	k4	341
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
270	[number]	k4	270
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
šestnáctém	šestnáctý	k4xOgInSc6	šestnáctý
století	století	k1gNnSc6	století
italský	italský	k2eAgMnSc1d1	italský
filozof	filozof	k1gMnSc1	filozof
Giordano	Giordana	k1gFnSc5	Giordana
Bruno	Bruno	k1gMnSc1	Bruno
<g/>
,	,	kIx,	,
zastánce	zastánce	k1gMnSc1	zastánce
Koperníkovy	Koperníkův	k2eAgFnSc2d1	Koperníkova
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
domněnku	domněnka	k1gFnSc4	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
Země	země	k1gFnSc1	země
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
planetami	planeta	k1gFnPc7	planeta
obíhá	obíhat	k5eAaImIp3nS	obíhat
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
heliocentrismus	heliocentrismus	k1gInSc1	heliocentrismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc1d1	ostatní
hvězdy	hvězda	k1gFnPc1	hvězda
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
Slunci	slunce	k1gNnSc3	slunce
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
doprovázeny	doprovázet	k5eAaImNgInP	doprovázet
planetami	planeta	k1gFnPc7	planeta
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
osmnáctém	osmnáctý	k4xOgInSc6	osmnáctý
století	století	k1gNnSc6	století
<g />
.	.	kIx.	.
</s>
<s>
Isaac	Isaac	k6eAd1	Isaac
Newton	Newton	k1gMnSc1	Newton
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
"	"	kIx"	"
<g/>
General	General	k1gFnSc6	General
Scholium	Scholium	k1gNnSc4	Scholium
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
jeho	jeho	k3xOp3gFnSc4	jeho
Principii	principie	k1gFnSc4	principie
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
A	a	k9	a
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
pevné	pevný	k2eAgFnPc4d1	pevná
hvězdy	hvězda	k1gFnPc4	hvězda
centry	centr	k1gInPc1	centr
jiných	jiný	k2eAgInPc2d1	jiný
podobných	podobný	k2eAgInPc2d1	podobný
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
<g/>
,	,	kIx,	,
formované	formovaný	k2eAgInPc4d1	formovaný
s	s	k7c7	s
obdobným	obdobný	k2eAgInSc7d1	obdobný
záměrem	záměr	k1gInSc7	záměr
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
všechny	všechen	k3xTgFnPc4	všechen
podrobeny	podroben	k2eAgFnPc4d1	podrobena
Jeho	jeho	k3xOp3gFnSc1	jeho
nadvládě	nadvláda	k1gFnSc3	nadvláda
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
let	léto	k1gNnPc2	léto
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgMnS	být
objeven	objeven	k2eAgMnSc1d1	objeven
první	první	k4xOgMnSc1	první
horký	horký	k2eAgMnSc1d1	horký
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaBmAgMnS	napsat
Otto	Otto	k1gMnSc1	Otto
Struve	Struev	k1gFnSc2	Struev
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistuje	existovat	k5eNaImIp3nS	existovat
žádný	žádný	k3yNgInSc4	žádný
přesvědčivý	přesvědčivý	k2eAgInSc4d1	přesvědčivý
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
planety	planeta	k1gFnPc1	planeta
nemohly	moct	k5eNaImAgFnP	moct
přiblížit	přiblížit	k5eAaPmF	přiblížit
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
mateřské	mateřský	k2eAgFnSc3d1	mateřská
hvězdě	hvězda	k1gFnSc3	hvězda
více	hodně	k6eAd2	hodně
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
dopplerovská	dopplerovský	k2eAgFnSc1d1	dopplerovská
spektroskopie	spektroskopie	k1gFnSc1	spektroskopie
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tranzitní	tranzitní	k2eAgFnSc7d1	tranzitní
metodou	metoda	k1gFnSc7	metoda
mohla	moct	k5eAaImAgFnS	moct
objevit	objevit	k5eAaPmF	objevit
super	super	k2eAgInPc4d1	super
Jupitery	Jupiter	k1gInPc4	Jupiter
na	na	k7c6	na
oběžných	oběžný	k2eAgFnPc6d1	oběžná
drahách	draha	k1gFnPc6	draha
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vyvrácené	vyvrácený	k2eAgInPc4d1	vyvrácený
objevy	objev	k1gInPc4	objev
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
hledání	hledání	k1gNnSc4	hledání
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
byly	být	k5eAaImAgFnP	být
uskutečněny	uskutečněn	k2eAgFnPc1d1	uskutečněna
již	již	k6eAd1	již
v	v	k7c6	v
devatenáctém	devatenáctý	k4xOgInSc6	devatenáctý
století	století	k1gNnSc6	století
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k6eAd1	William
Stephen	Stephen	k1gInSc1	Stephen
Jacob	Jacoba	k1gFnPc2	Jacoba
z	z	k7c2	z
Britské	britský	k2eAgFnSc2d1	britská
východoindické	východoindický	k2eAgFnSc2d1	Východoindická
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c6	na
observatoři	observatoř	k1gFnSc6	observatoř
Madras	madras	k1gInSc4	madras
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Indii	Indie	k1gFnSc6	Indie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
pozorování	pozorování	k1gNnSc2	pozorování
dvojhvězdy	dvojhvězda	k1gFnSc2	dvojhvězda
70	[number]	k4	70
Ophiuchi	Ophiuchi	k1gNnSc2	Ophiuchi
usoudil	usoudit	k5eAaPmAgMnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
obou	dva	k4xCgFnPc2	dva
hvězd	hvězda	k1gFnPc2	hvězda
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
anomálie	anomálie	k1gFnPc4	anomálie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
systému	systém	k1gInSc6	systém
existuje	existovat	k5eAaImIp3nS	existovat
planetární	planetární	k2eAgNnSc1d1	planetární
těleso	těleso	k1gNnSc1	těleso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc6	století
Thomas	Thomas	k1gMnSc1	Thomas
J.	J.	kA	J.
J.	J.	kA	J.
See	See	k1gMnSc1	See
z	z	k7c2	z
Chicagské	chicagský	k2eAgFnSc2d1	Chicagská
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
Naval	navalit	k5eAaPmRp2nS	navalit
Observatory	Observator	k1gMnPc4	Observator
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
anomálie	anomálie	k1gFnSc1	anomálie
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
existenci	existence	k1gFnSc4	existence
tmavého	tmavý	k2eAgNnSc2d1	tmavé
tělesa	těleso	k1gNnSc2	těleso
v	v	k7c6	v
systému	systém	k1gInSc6	systém
70	[number]	k4	70
Ophiuchi	Ophiuche	k1gFnSc4	Ophiuche
s	s	k7c7	s
36	[number]	k4	36
<g/>
letou	letý	k2eAgFnSc7d1	letá
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dobou	doba	k1gFnSc7	doba
vůči	vůči	k7c3	vůči
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
astronom	astronom	k1gMnSc1	astronom
Forest	Forest	k1gMnSc1	Forest
Ray	Ray	k1gMnSc1	Ray
Moulton	Moulton	k1gInSc1	Moulton
ale	ale	k8xC	ale
publikoval	publikovat	k5eAaBmAgMnS	publikovat
dokument	dokument	k1gInSc4	dokument
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
systém	systém	k1gInSc1	systém
tří	tři	k4xCgNnPc2	tři
těles	těleso	k1gNnPc2	těleso
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
parametry	parametr	k1gInPc7	parametr
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
vysoce	vysoce	k6eAd1	vysoce
nestabilní	stabilní	k2eNgInSc1d1	nestabilní
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
padesátých	padesátý	k4xOgNnPc2	padesátý
a	a	k8xC	a
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
provedl	provést	k5eAaPmAgMnS	provést
Peter	Peter	k1gMnSc1	Peter
van	vana	k1gFnPc2	vana
de	de	k?	de
Kamp	Kampa	k1gFnPc2	Kampa
z	z	k7c2	z
Swarthmore	Swarthmor	k1gInSc5	Swarthmor
College	Colleg	k1gFnSc2	Colleg
další	další	k2eAgNnPc1d1	další
měření	měření	k1gNnPc1	měření
<g/>
,	,	kIx,	,
týkající	týkající	k2eAgNnPc1d1	týkající
se	se	k3xPyFc4	se
tentokrát	tentokrát	k6eAd1	tentokrát
planet	planeta	k1gFnPc2	planeta
obíhajících	obíhající	k2eAgFnPc2d1	obíhající
Barnardovu	Barnardův	k2eAgFnSc4d1	Barnardova
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
nyní	nyní	k6eAd1	nyní
považují	považovat	k5eAaImIp3nP	považovat
všechny	všechen	k3xTgFnPc4	všechen
výše	vysoce	k6eAd2	vysoce
zmiňované	zmiňovaný	k2eAgFnPc4d1	zmiňovaná
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
objevech	objev	k1gInPc6	objev
exoplanet	exoplanet	k5eAaBmF	exoplanet
za	za	k7c4	za
chybné	chybný	k2eAgNnSc4d1	chybné
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
Andrew	Andrew	k1gFnSc1	Andrew
Lyne	Lyne	k1gFnSc1	Lyne
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Bailes	Bailes	k1gInSc1	Bailes
a	a	k8xC	a
S.	S.	kA	S.
L.	L.	kA	L.
Shemar	Shemar	k1gMnSc1	Shemar
zveřejnili	zveřejnit	k5eAaPmAgMnP	zveřejnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
objevili	objevit	k5eAaPmAgMnP	objevit
planetu	planeta	k1gFnSc4	planeta
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
kolem	kolem	k7c2	kolem
pulsaru	pulsar	k1gInSc2	pulsar
PSR	PSR	kA	PSR
1829	[number]	k4	1829
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
variací	variace	k1gFnPc2	variace
periody	perioda	k1gFnSc2	perioda
rádiových	rádiový	k2eAgInPc2d1	rádiový
impulsů	impuls	k1gInPc2	impuls
pulsaru	pulsar	k1gInSc2	pulsar
<g/>
.	.	kIx.	.
</s>
<s>
Zprávě	zpráva	k1gFnSc3	zpráva
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
dostalo	dostat	k5eAaPmAgNnS	dostat
velké	velký	k2eAgFnSc3d1	velká
pozornosti	pozornost	k1gFnSc3	pozornost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Lyne	Lyne	k1gInSc1	Lyne
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
tým	tým	k1gInSc1	tým
ji	on	k3xPp3gFnSc4	on
brzy	brzy	k6eAd1	brzy
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Potvrzené	potvrzený	k2eAgInPc4d1	potvrzený
objevy	objev	k1gInPc4	objev
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
v	v	k7c4	v
Extrasolar	Extrasolar	k1gInSc4	Extrasolar
Planets	Planetsa	k1gFnPc2	Planetsa
Encyclopaedia	Encyclopaedium	k1gNnSc2	Encyclopaedium
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
celkem	celkem	k6eAd1	celkem
3	[number]	k4	3
903	[number]	k4	903
potvrzených	potvrzený	k2eAgFnPc2d1	potvrzená
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
několika	několik	k4yIc2	několik
sporných	sporný	k2eAgFnPc2d1	sporná
potvrzených	potvrzený	k2eAgFnPc2d1	potvrzená
planet	planeta	k1gFnPc2	planeta
z	z	k7c2	z
konce	konec	k1gInSc2	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
přímým	přímý	k2eAgNnSc7d1	přímé
zobrazením	zobrazení	k1gNnSc7	zobrazení
<g/>
,	,	kIx,	,
převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
nepřímými	přímý	k2eNgFnPc7d1	nepřímá
metodami	metoda	k1gFnPc7	metoda
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
tranzitní	tranzitní	k2eAgFnSc7d1	tranzitní
metodou	metoda	k1gFnSc7	metoda
či	či	k8xC	či
měřením	měření	k1gNnSc7	měření
radiální	radiální	k2eAgFnSc2d1	radiální
rychlosti	rychlost	k1gFnSc2	rychlost
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
zveřejněný	zveřejněný	k2eAgInSc1d1	zveřejněný
objev	objev	k1gInSc1	objev
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
následné	následný	k2eAgNnSc4d1	následné
potvrzení	potvrzení	k1gNnSc4	potvrzení
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
oznámen	oznámit	k5eAaPmNgInS	oznámit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
kanadskými	kanadský	k2eAgMnPc7d1	kanadský
astronomy	astronom	k1gMnPc7	astronom
Brucem	Bruce	k1gMnSc7	Bruce
Campbellem	Campbell	k1gMnSc7	Campbell
<g/>
,	,	kIx,	,
G.	G.	kA	G.
A.	A.	kA	A.
H.	H.	kA	H.
Walkerem	Walker	k1gMnSc7	Walker
a	a	k8xC	a
Stephensonem	Stephenson	k1gMnSc7	Stephenson
Yangem	Yang	k1gMnSc7	Yang
z	z	k7c2	z
University	universita	k1gFnSc2	universita
of	of	k?	of
Victoria	Victorium	k1gNnPc4	Victorium
a	a	k8xC	a
University	universita	k1gFnPc4	universita
of	of	k?	of
British	British	k1gInSc1	British
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
byli	být	k5eAaImAgMnP	být
opatrní	opatrný	k2eAgMnPc1d1	opatrný
při	při	k7c6	při
tvrzení	tvrzení	k1gNnSc6	tvrzení
o	o	k7c6	o
objevu	objev	k1gInSc6	objev
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnPc1	jejich
měření	měření	k1gNnPc1	měření
radiální	radiální	k2eAgFnSc2d1	radiální
rychlosti	rychlost	k1gFnSc2	rychlost
naznačovalo	naznačovat	k5eAaImAgNnS	naznačovat
existenci	existence	k1gFnSc4	existence
planety	planeta	k1gFnSc2	planeta
obíhající	obíhající	k2eAgFnSc2d1	obíhající
okolo	okolo	k7c2	okolo
hvězdy	hvězda	k1gFnSc2	hvězda
Gama	gama	k1gNnSc2	gama
Cephei	Cephe	k1gFnSc2	Cephe
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozorování	pozorování	k1gNnPc1	pozorování
byla	být	k5eAaImAgNnP	být
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
možností	možnost	k1gFnPc2	možnost
přístrojů	přístroj	k1gInPc2	přístroj
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
astronomové	astronom	k1gMnPc1	astronom
zůstávali	zůstávat	k5eAaImAgMnP	zůstávat
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
skeptičtí	skeptický	k2eAgMnPc1d1	skeptický
ohledně	ohledně	k7c2	ohledně
pozorování	pozorování	k1gNnPc2	pozorování
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
podobných	podobný	k2eAgNnPc2d1	podobné
pozorování	pozorování	k1gNnPc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
předpoklad	předpoklad	k1gInSc1	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
ze	z	k7c2	z
zdánlivých	zdánlivý	k2eAgFnPc2d1	zdánlivá
planet	planeta	k1gFnPc2	planeta
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
hnědými	hnědý	k2eAgMnPc7d1	hnědý
trpaslíky	trpaslík	k1gMnPc7	trpaslík
<g/>
,	,	kIx,	,
objekty	objekt	k1gInPc7	objekt
řazenými	řazený	k2eAgInPc7d1	řazený
mezi	mezi	k7c4	mezi
planety	planeta	k1gFnPc4	planeta
a	a	k8xC	a
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
publikována	publikován	k2eAgFnSc1d1	publikována
další	další	k2eAgNnSc4d1	další
pozorování	pozorování	k1gNnSc4	pozorování
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podporovala	podporovat	k5eAaImAgFnS	podporovat
existenci	existence	k1gFnSc4	existence
planety	planeta	k1gFnSc2	planeta
obíhající	obíhající	k2eAgFnSc1d1	obíhající
Gama	gama	k1gNnSc7	gama
Cephei	Cephe	k1gFnSc2	Cephe
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
následná	následný	k2eAgNnPc1d1	následné
pozorování	pozorování	k1gNnPc1	pozorování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
znovu	znovu	k6eAd1	znovu
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
vážné	vážný	k2eAgFnPc4d1	vážná
pochybnosti	pochybnost	k1gFnPc4	pochybnost
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
vylepšená	vylepšený	k2eAgFnSc1d1	vylepšená
technika	technika	k1gFnSc1	technika
umožnila	umožnit	k5eAaPmAgFnS	umožnit
potvrzení	potvrzení	k1gNnSc4	potvrzení
existence	existence	k1gFnSc2	existence
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1992	[number]	k4	1992
v	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgNnSc6d1	rozhlasové
vysílání	vysílání	k1gNnSc6	vysílání
astronomové	astronom	k1gMnPc1	astronom
Aleksander	Aleksandero	k1gNnPc2	Aleksandero
Wolszczan	Wolszczana	k1gFnPc2	Wolszczana
a	a	k8xC	a
Dale	Dal	k1gFnSc2	Dal
Frail	Fraila	k1gFnPc2	Fraila
oznámili	oznámit	k5eAaPmAgMnP	oznámit
objev	objev	k1gInSc4	objev
dvou	dva	k4xCgFnPc2	dva
planet	planeta	k1gFnPc2	planeta
obíhajících	obíhající	k2eAgFnPc2d1	obíhající
okolo	okolo	k7c2	okolo
pulsaru	pulsar	k1gInSc2	pulsar
PSR	PSR	kA	PSR
1257	[number]	k4	1257
<g/>
+	+	kIx~	+
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
objev	objev	k1gInSc1	objev
byl	být	k5eAaImAgInS	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
první	první	k4xOgInSc4	první
potvrzený	potvrzený	k2eAgInSc4d1	potvrzený
objev	objev	k1gInSc4	objev
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
<g/>
.	.	kIx.	.
</s>
<s>
Následná	následný	k2eAgNnPc1d1	následné
pozorování	pozorování	k1gNnPc1	pozorování
tyto	tento	k3xDgInPc4	tento
výsledky	výsledek	k1gInPc4	výsledek
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
a	a	k8xC	a
potvrzení	potvrzení	k1gNnSc4	potvrzení
třetí	třetí	k4xOgFnSc2	třetí
planety	planeta	k1gFnSc2	planeta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
oživilo	oživit	k5eAaPmAgNnS	oživit
téma	téma	k1gNnSc1	téma
v	v	k7c6	v
populárním	populární	k2eAgInSc6d1	populární
tisku	tisk	k1gInSc6	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
planety	planeta	k1gFnPc1	planeta
obíhající	obíhající	k2eAgFnPc1d1	obíhající
okolo	okolo	k7c2	okolo
pulsaru	pulsar	k1gInSc2	pulsar
se	se	k3xPyFc4	se
zformovaly	zformovat	k5eAaPmAgFnP	zformovat
ze	z	k7c2	z
zbytků	zbytek	k1gInPc2	zbytek
po	po	k7c6	po
výbuchu	výbuch	k1gInSc6	výbuch
supernovy	supernova	k1gFnSc2	supernova
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
zbývajícími	zbývající	k2eAgFnPc7d1	zbývající
skalnatými	skalnatý	k2eAgFnPc7d1	skalnatá
jádry	jádro	k1gNnPc7	jádro
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
přežily	přežít	k5eAaPmAgFnP	přežít
výbuch	výbuch	k1gInSc4	výbuch
supernovy	supernova	k1gFnSc2	supernova
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
rozpadly	rozpadnout	k5eAaPmAgInP	rozpadnout
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1995	[number]	k4	1995
oznámili	oznámit	k5eAaPmAgMnP	oznámit
Michel	Michel	k1gMnSc1	Michel
Mayor	Mayor	k1gMnSc1	Mayor
a	a	k8xC	a
Didier	Didier	k1gMnSc1	Didier
Queloz	Queloz	k1gMnSc1	Queloz
z	z	k7c2	z
Ženevské	ženevský	k2eAgFnSc2d1	Ženevská
univerzity	univerzita	k1gFnSc2	univerzita
první	první	k4xOgFnSc3	první
definitivní	definitivní	k2eAgFnSc3d1	definitivní
detekci	detekce	k1gFnSc3	detekce
exoplanety	exoplanet	k1gInPc4	exoplanet
obíhající	obíhající	k2eAgFnSc4d1	obíhající
hvězdu	hvězda	k1gFnSc4	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
hvězdy	hvězda	k1gFnSc2	hvězda
typu	typ	k1gInSc2	typ
G	G	kA	G
51	[number]	k4	51
Pegasi	Pegas	k1gMnPc1	Pegas
<g/>
.	.	kIx.	.
</s>
<s>
Exoplaneta	Exoplaneta	k1gFnSc1	Exoplaneta
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
přístroji	přístroj	k1gInSc3	přístroj
v	v	k7c6	v
Observatoire	Observatoir	k1gInSc5	Observatoir
de	de	k?	de
Haute-Provence	Haute-Provence	k1gFnSc1	Haute-Provence
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
detekce	detekce	k1gFnSc1	detekce
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
objevů	objev	k1gInPc2	objev
exoplanet	exoplanet	k5eAaPmF	exoplanet
<g/>
.	.	kIx.	.
</s>
<s>
Technologický	technologický	k2eAgInSc4d1	technologický
pokrok	pokrok	k1gInSc4	pokrok
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
spektroskopii	spektroskopie	k1gFnSc6	spektroskopie
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
rozlišením	rozlišení	k1gNnSc7	rozlišení
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
rychlému	rychlý	k2eAgNnSc3d1	rychlé
objevování	objevování	k1gNnSc3	objevování
mnoha	mnoho	k4c2	mnoho
nových	nový	k2eAgFnPc2d1	nová
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
<g/>
,	,	kIx,	,
astronomové	astronom	k1gMnPc1	astronom
mohli	moct	k5eAaImAgMnP	moct
prokazovat	prokazovat	k5eAaImF	prokazovat
existenci	existence	k1gFnSc4	existence
exoplanet	exoplanet	k5eAaImF	exoplanet
nepřímo	přímo	k6eNd1	přímo
měřením	měření	k1gNnSc7	měření
jejich	jejich	k3xOp3gInSc2	jejich
gravitačního	gravitační	k2eAgInSc2d1	gravitační
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
pohyb	pohyb	k1gInSc4	pohyb
jejich	jejich	k3xOp3gFnPc2	jejich
hostitelských	hostitelský	k2eAgFnPc2d1	hostitelská
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
extrasolárních	extrasolární	k2eAgFnPc2d1	extrasolární
planet	planeta	k1gFnPc2	planeta
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
sledováním	sledování	k1gNnSc7	sledování
změny	změna	k1gFnSc2	změna
v	v	k7c6	v
zdánlivé	zdánlivý	k2eAgFnSc6d1	zdánlivá
velikosti	velikost	k1gFnSc6	velikost
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
procházela	procházet	k5eAaImAgFnS	procházet
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ji	on	k3xPp3gFnSc4	on
obíhá	obíhat	k5eAaImIp3nS	obíhat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
známých	známý	k2eAgFnPc2d1	známá
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
značně	značně	k6eAd1	značně
hmotné	hmotný	k2eAgFnPc4d1	hmotná
planety	planeta	k1gFnPc4	planeta
obíhající	obíhající	k2eAgFnPc1d1	obíhající
velmi	velmi	k6eAd1	velmi
blízko	blízko	k7c2	blízko
své	svůj	k3xOyFgFnSc2	svůj
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
"	"	kIx"	"
<g/>
horké	horký	k2eAgInPc1d1	horký
Jupitery	Jupiter	k1gInPc1	Jupiter
<g/>
"	"	kIx"	"
byly	být	k5eAaImAgInP	být
pro	pro	k7c4	pro
astronomy	astronom	k1gMnPc4	astronom
překvapením	překvapení	k1gNnSc7	překvapení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
teorie	teorie	k1gFnSc1	teorie
vzniku	vznik	k1gInSc2	vznik
planet	planeta	k1gFnPc2	planeta
naznačovaly	naznačovat	k5eAaImAgFnP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
obří	obří	k2eAgFnPc1d1	obří
planety	planeta	k1gFnPc1	planeta
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
tvořit	tvořit	k5eAaImF	tvořit
jen	jen	k9	jen
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
mateřské	mateřský	k2eAgFnSc2d1	mateřská
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
i	i	k9	i
další	další	k2eAgFnPc1d1	další
planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
horké	horký	k2eAgInPc1d1	horký
Jupitery	Jupiter	k1gInPc1	Jupiter
netvoří	tvořit	k5eNaImIp3nP	tvořit
většinu	většina	k1gFnSc4	většina
exoplanet	exoplanet	k5eAaPmF	exoplanet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
se	se	k3xPyFc4	se
Ypsilon	ypsilon	k1gNnSc7	ypsilon
Andromedae	Andromeda	k1gInSc2	Andromeda
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
hvězdou	hvězda	k1gFnSc7	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
obíhá	obíhat	k5eAaImIp3nS	obíhat
několik	několik	k4yIc4	několik
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
kolem	kolem	k7c2	kolem
hvězdy	hvězda	k1gFnSc2	hvězda
Kepler-	Kepler-	k1gFnSc2	Kepler-
<g/>
16	[number]	k4	16
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
známá	známý	k2eAgFnSc1d1	známá
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
dvojhvězdy	dvojhvězda	k1gFnSc2	dvojhvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
NASA	NASA	kA	NASA
oznámila	oznámit	k5eAaPmAgFnS	oznámit
objev	objev	k1gInSc1	objev
715	[number]	k4	715
potvrzených	potvrzený	k2eAgFnPc2d1	potvrzená
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
u	u	k7c2	u
305	[number]	k4	305
hvězd	hvězda	k1gFnPc2	hvězda
vesmírným	vesmírný	k2eAgInSc7d1	vesmírný
dalekohledem	dalekohled	k1gInSc7	dalekohled
Kepler	Kepler	k1gMnSc1	Kepler
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
exoplanety	exoplanet	k1gInPc1	exoplanet
byly	být	k5eAaImAgInP	být
zkontrolovány	zkontrolován	k2eAgInPc1d1	zkontrolován
novou	nový	k2eAgFnSc7d1	nová
statistickou	statistický	k2eAgFnSc7d1	statistická
metodou	metoda	k1gFnSc7	metoda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
nazvána	nazván	k2eAgFnSc1d1	nazvána
"	"	kIx"	"
<g/>
potvrzení	potvrzení	k1gNnPc2	potvrzení
díky	díky	k7c3	díky
mnohačetnosti	mnohačetnost	k1gFnSc3	mnohačetnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
těmito	tento	k3xDgInPc7	tento
výsledky	výsledek	k1gInPc7	výsledek
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c7	mezi
potvrzenými	potvrzený	k2eAgFnPc7d1	potvrzená
planetami	planeta	k1gFnPc7	planeta
nejvíce	nejvíce	k6eAd1	nejvíce
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
velikosti	velikost	k1gFnSc2	velikost
srovnatelné	srovnatelný	k2eAgFnPc4d1	srovnatelná
s	s	k7c7	s
Jupiterem	Jupiter	k1gInSc7	Jupiter
nebo	nebo	k8xC	nebo
větších	veliký	k2eAgFnPc2d2	veliký
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
snadněji	snadno	k6eAd2	snadno
objevit	objevit	k5eAaPmF	objevit
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
planety	planeta	k1gFnSc2	planeta
objevené	objevený	k2eAgFnSc2d1	objevená
sondou	sonda	k1gFnSc7	sonda
Kepler	Kepler	k1gMnSc1	Kepler
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
velikost	velikost	k1gFnSc4	velikost
mezi	mezi	k7c7	mezi
velikostí	velikost	k1gFnSc7	velikost
Neptunu	Neptun	k1gInSc2	Neptun
a	a	k8xC	a
Země	země	k1gFnSc1	země
<g/>
.23	.23	k4	.23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
NASA	NASA	kA	NASA
oznámila	oznámit	k5eAaPmAgFnS	oznámit
objev	objev	k1gInSc4	objev
planety	planeta	k1gFnSc2	planeta
Kepler-	Kepler-	k1gFnSc1	Kepler-
<g/>
452	[number]	k4	452
<g/>
b	b	k?	b
obíhající	obíhající	k2eAgInPc1d1	obíhající
obyvatelnou	obyvatelný	k2eAgFnSc7d1	obyvatelná
zónou	zóna	k1gFnSc7	zóna
hvězdy	hvězda	k1gFnSc2	hvězda
typu	typ	k1gInSc2	typ
G2	G2	k1gFnSc2	G2
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Objevování	objevování	k1gNnSc1	objevování
kandidátů	kandidát	k1gMnPc2	kandidát
planet	planeta	k1gFnPc2	planeta
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
identifikovala	identifikovat	k5eAaBmAgFnS	identifikovat
sonda	sonda	k1gFnSc1	sonda
Kepler	Kepler	k1gMnSc1	Kepler
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2900	[number]	k4	2900
planetárních	planetární	k2eAgMnPc2d1	planetární
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
některé	některý	k3yIgInPc4	některý
jsou	být	k5eAaImIp3nP	být
planetami	planeta	k1gFnPc7	planeta
pozemského	pozemský	k2eAgInSc2d1	pozemský
typu	typ	k1gInSc2	typ
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
obyvatelné	obyvatelný	k2eAgFnSc6d1	obyvatelná
zóně	zóna	k1gFnSc6	zóna
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
obíhají	obíhat	k5eAaImIp3nP	obíhat
okolo	okolo	k7c2	okolo
hvězd	hvězda	k1gFnPc2	hvězda
podobných	podobný	k2eAgFnPc2d1	podobná
Slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Metody	metoda	k1gFnSc2	metoda
výzkumu	výzkum	k1gInSc2	výzkum
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
exoplaneta	exoplaneta	k1gFnSc1	exoplaneta
obíhající	obíhající	k2eAgFnSc4d1	obíhající
hvězdu	hvězda	k1gFnSc4	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
byla	být	k5eAaImAgFnS	být
objevena	objeven	k2eAgFnSc1d1	objevena
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1995	[number]	k4	1995
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
jméno	jméno	k1gNnSc4	jméno
51	[number]	k4	51
Pegasi	Pegas	k1gMnPc1	Pegas
b.	b.	k?	b.
Když	když	k8xS	když
exoplaneta	exoplaneta	k1gFnSc1	exoplaneta
přechází	přecházet	k5eAaImIp3nS	přecházet
(	(	kIx(	(
<g/>
tranzituje	tranzitovat	k5eAaImIp3nS	tranzitovat
<g/>
)	)	kIx)	)
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
astronomové	astronom	k1gMnPc1	astronom
posoudit	posoudit	k5eAaPmF	posoudit
některé	některý	k3yIgFnPc4	některý
její	její	k3xOp3gFnPc4	její
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
z	z	k7c2	z
mezihvězdné	mezihvězdný	k2eAgFnSc2d1	mezihvězdná
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
její	její	k3xOp3gFnSc2	její
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
velikosti	velikost	k1gFnSc2	velikost
a	a	k8xC	a
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
jim	on	k3xPp3gMnPc3	on
základní	základní	k2eAgInSc4d1	základní
údaje	údaj	k1gInPc4	údaj
pro	pro	k7c4	pro
modelování	modelování	k1gNnSc4	modelování
její	její	k3xOp3gFnSc2	její
fyzické	fyzický	k2eAgFnSc2d1	fyzická
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
události	událost	k1gFnPc1	událost
někdy	někdy	k6eAd1	někdy
navíc	navíc	k6eAd1	navíc
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
studovat	studovat	k5eAaImF	studovat
dynamiku	dynamika	k1gFnSc4	dynamika
a	a	k8xC	a
chemii	chemie	k1gFnSc4	chemie
její	její	k3xOp3gFnSc2	její
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
<g/>
Statistické	statistický	k2eAgInPc1d1	statistický
průzkumy	průzkum	k1gInPc1	průzkum
a	a	k8xC	a
individuální	individuální	k2eAgNnSc1d1	individuální
posuzování	posuzování	k1gNnSc1	posuzování
planet	planeta	k1gFnPc2	planeta
jsou	být	k5eAaImIp3nP	být
klíčem	klíč	k1gInSc7	klíč
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
základních	základní	k2eAgFnPc2d1	základní
otázek	otázka	k1gFnPc2	otázka
v	v	k7c6	v
exoplanetologii	exoplanetologie	k1gFnSc6	exoplanetologie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
května	květen	k1gInSc2	květen
2017	[number]	k4	2017
byly	být	k5eAaImAgInP	být
k	k	k7c3	k
objevu	objev	k1gInSc3	objev
3	[number]	k4	3
610	[number]	k4	610
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
využity	využít	k5eAaPmNgFnP	využít
různé	různý	k2eAgFnPc1d1	různá
techniky	technika	k1gFnPc1	technika
<g/>
.	.	kIx.	.
</s>
<s>
Dokumentování	dokumentování	k1gNnSc4	dokumentování
vlastností	vlastnost	k1gFnPc2	vlastnost
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
exoplanet	exoplanet	k1gMnSc1	exoplanet
různého	různý	k2eAgNnSc2d1	různé
stáří	stáří	k1gNnSc2	stáří
<g/>
,	,	kIx,	,
obíhajících	obíhající	k2eAgMnPc2d1	obíhající
hvězdy	hvězda	k1gFnSc2	hvězda
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
přispěje	přispět	k5eAaPmIp3nS	přispět
k	k	k7c3	k
většímu	veliký	k2eAgNnSc3d2	veliký
porozumění	porozumění	k1gNnSc3	porozumění
-	-	kIx~	-
nebo	nebo	k8xC	nebo
lepším	dobrý	k2eAgInPc3d2	lepší
modelům	model	k1gInPc3	model
-	-	kIx~	-
planetární	planetární	k2eAgFnSc2d1	planetární
formace	formace	k1gFnSc2	formace
(	(	kIx(	(
<g/>
akrece	akrece	k1gFnSc1	akrece
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
geologické	geologický	k2eAgFnSc2d1	geologická
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
,	,	kIx,	,
migrace	migrace	k1gFnSc2	migrace
jejich	jejich	k3xOp3gFnPc4	jejich
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
možné	možný	k2eAgFnPc4d1	možná
obyvatelnosti	obyvatelnost	k1gFnPc4	obyvatelnost
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
atmosfér	atmosféra	k1gFnPc2	atmosféra
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
je	být	k5eAaImIp3nS	být
novou	nový	k2eAgFnSc7d1	nová
hranicí	hranice	k1gFnSc7	hranice
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
o	o	k7c6	o
exoplanetách	exoplaneta	k1gFnPc6	exoplaneta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Metody	metoda	k1gFnPc1	metoda
objevování	objevování	k1gNnSc2	objevování
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
===	===	k?	===
</s>
</p>
<p>
<s>
Asi	asi	k9	asi
97	[number]	k4	97
procent	procento	k1gNnPc2	procento
všech	všecek	k3xTgFnPc2	všecek
potvrzených	potvrzený	k2eAgFnPc2d1	potvrzená
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
nepřímými	přímý	k2eNgFnPc7d1	nepřímá
detekčními	detekční	k2eAgFnPc7d1	detekční
metodami	metoda	k1gFnPc7	metoda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
radiálním	radiální	k2eAgNnSc7d1	radiální
měřením	měření	k1gNnSc7	měření
rychlosti	rychlost	k1gFnSc2	rychlost
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
tranzitní	tranzitní	k2eAgFnSc7d1	tranzitní
metodou	metoda	k1gFnSc7	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
metody	metoda	k1gFnPc1	metoda
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
pro	pro	k7c4	pro
objevení	objevení	k1gNnSc4	objevení
nové	nový	k2eAgFnSc2d1	nová
planety	planeta	k1gFnSc2	planeta
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
potvrzení	potvrzení	k1gNnSc4	potvrzení
již	již	k6eAd1	již
objevené	objevený	k2eAgFnSc2d1	objevená
planety	planeta	k1gFnSc2	planeta
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Radiální	radiální	k2eAgInSc1d1	radiální
rychlostEfekt	rychlostEfekt	k1gInSc1	rychlostEfekt
gravitační	gravitační	k2eAgFnSc2d1	gravitační
čočkyPřímé	čočkyPřímý	k2eAgFnSc2d1	čočkyPřímý
zobrazeníPolarimetrieAstrometrieTranzitní	zobrazeníPolarimetrieAstrometrieTranzitní	k2eAgFnSc2d1	zobrazeníPolarimetrieAstrometrieTranzitní
metoda	metoda	k1gFnSc1	metoda
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
planet	planeta	k1gFnPc2	planeta
==	==	k?	==
</s>
</p>
<p>
<s>
Planety	planeta	k1gFnPc1	planeta
se	se	k3xPyFc4	se
formují	formovat	k5eAaImIp3nP	formovat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
vývoje	vývoj	k1gInSc2	vývoj
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
stáří	stáří	k1gNnSc4	stáří
deset	deset	k4xCc4	deset
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
planet	planeta	k1gFnPc2	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
současné	současný	k2eAgFnSc6d1	současná
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
nám	my	k3xPp1nPc3	my
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
studium	studium	k1gNnSc4	studium
exoplanet	exoplanet	k5eAaPmF	exoplanet
jejich	jejich	k3xOp3gNnSc4	jejich
pozorování	pozorování	k1gNnSc4	pozorování
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
fázích	fáze	k1gFnPc6	fáze
planetární	planetární	k2eAgFnSc2d1	planetární
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
planety	planeta	k1gFnPc1	planeta
vznikají	vznikat	k5eAaImIp3nP	vznikat
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
vodíkové	vodíkový	k2eAgFnPc1d1	vodíková
obálky	obálka	k1gFnPc1	obálka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
chladnou	chladnout	k5eAaImIp3nP	chladnout
a	a	k8xC	a
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
se	se	k3xPyFc4	se
průběhu	průběh	k1gInSc2	průběh
času	čas	k1gInSc2	čas
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
hmotnosti	hmotnost	k1gFnSc6	hmotnost
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
planety	planeta	k1gFnPc1	planeta
veškerý	veškerý	k3xTgInSc4	veškerý
vodík	vodík	k1gInSc4	vodík
nakonec	nakonec	k6eAd1	nakonec
ztratí	ztratit	k5eAaPmIp3nS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
planety	planeta	k1gFnSc2	planeta
pozemského	pozemský	k2eAgInSc2d1	pozemský
typu	typ	k1gInSc2	typ
planety	planeta	k1gFnSc2	planeta
mohou	moct	k5eAaImIp3nP	moct
začít	začít	k5eAaPmF	začít
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
poloměry	poloměr	k1gInPc7	poloměr
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Kepler-	Kepler-	k1gFnSc1	Kepler-
<g/>
51	[number]	k4	51
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
zhruba	zhruba	k6eAd1	zhruba
dvojnásobnou	dvojnásobný	k2eAgFnSc4d1	dvojnásobná
hmotnost	hmotnost	k1gFnSc4	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
tak	tak	k6eAd1	tak
veliký	veliký	k2eAgInSc1d1	veliký
jako	jako	k8xC	jako
Saturn	Saturn	k1gInSc1	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Kepler-	Kepler-	k?	Kepler-
<g/>
51	[number]	k4	51
<g/>
b	b	k?	b
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
mladá	mladý	k2eAgFnSc1d1	mladá
planeta	planeta	k1gFnSc1	planeta
stáří	stáří	k1gNnSc2	stáří
stovek	stovka	k1gFnPc2	stovka
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
planetami	planeta	k1gFnPc7	planeta
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
existuje	existovat	k5eAaImIp3nS	existovat
jedna	jeden	k4xCgFnSc1	jeden
planeta	planeta	k1gFnSc1	planeta
na	na	k7c4	na
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
hvězd	hvězda	k1gFnPc2	hvězda
podobných	podobný	k2eAgFnPc2d1	podobná
Slunci	slunce	k1gNnSc3	slunce
má	mít	k5eAaImIp3nS	mít
planetu	planeta	k1gFnSc4	planeta
"	"	kIx"	"
<g/>
velikosti	velikost	k1gFnSc2	velikost
Země	zem	k1gFnSc2	zem
<g/>
"	"	kIx"	"
v	v	k7c6	v
obyvatelné	obyvatelný	k2eAgFnSc6d1	obyvatelná
zóně	zóna	k1gFnSc6	zóna
<g/>
.	.	kIx.	.
<g/>
Většina	většina	k1gFnSc1	většina
známých	známý	k2eAgFnPc2d1	známá
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
mají	mít	k5eAaImIp3nP	mít
exoplanety	exoplanet	k1gInPc7	exoplanet
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
Slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
hvězdy	hvězda	k1gFnPc4	hvězda
hlavní	hlavní	k2eAgFnPc4d1	hlavní
posloupností	posloupnost	k1gFnSc7	posloupnost
spektrální	spektrální	k2eAgFnSc2d1	spektrální
klasifikace	klasifikace	k1gFnSc2	klasifikace
F	F	kA	F
<g/>
,	,	kIx,	,
G	G	kA	G
nebo	nebo	k8xC	nebo
K.	K.	kA	K.
U	u	k7c2	u
menších	malý	k2eAgFnPc2d2	menší
hvězd	hvězda	k1gFnPc2	hvězda
(	(	kIx(	(
<g/>
červených	červená	k1gFnPc2	červená
trpaslíků	trpaslík	k1gMnPc2	trpaslík
spektrální	spektrální	k2eAgFnSc2d1	spektrální
kategorie	kategorie	k1gFnSc2	kategorie
M	M	kA	M
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
planety	planeta	k1gFnPc1	planeta
s	s	k7c7	s
dostatečnou	dostatečný	k2eAgFnSc7d1	dostatečná
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
lze	lze	k6eAd1	lze
objevit	objevit	k5eAaPmF	objevit
měřením	měření	k1gNnSc7	měření
radiální	radiální	k2eAgFnSc2d1	radiální
rychlosti	rychlost	k1gFnSc2	rychlost
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
několik	několik	k4yIc4	několik
planet	planeta	k1gFnPc2	planeta
u	u	k7c2	u
červených	červený	k2eAgMnPc2d1	červený
trpaslíků	trpaslík	k1gMnPc2	trpaslík
objevila	objevit	k5eAaPmAgFnS	objevit
kosmická	kosmický	k2eAgFnSc1d1	kosmická
sonda	sonda	k1gFnSc1	sonda
Kepler	Kepler	k1gMnSc1	Kepler
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
využívá	využívat	k5eAaPmIp3nS	využívat
tranzitní	tranzitní	k2eAgFnSc4d1	tranzitní
metodu	metoda	k1gFnSc4	metoda
k	k	k7c3	k
objevům	objev	k1gInPc3	objev
menších	malý	k2eAgFnPc2d2	menší
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
metalicitou	metalicita	k1gFnSc7	metalicita
než	než	k8xS	než
Slunce	slunce	k1gNnSc2	slunce
mají	mít	k5eAaImIp3nP	mít
pravděpodobněji	pravděpodobně	k6eAd2	pravděpodobně
planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
obří	obří	k2eAgFnSc2d1	obří
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
než	než	k8xS	než
hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
metalicitou	metalicita	k1gFnSc7	metalicita
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgFnPc1	některý
planety	planeta	k1gFnPc1	planeta
obíhají	obíhat	k5eAaImIp3nP	obíhat
okolo	okolo	k7c2	okolo
jedné	jeden	k4xCgFnSc2	jeden
složky	složka	k1gFnSc2	složka
dvojhvězdy	dvojhvězda	k1gFnSc2	dvojhvězda
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
i	i	k9	i
několik	několik	k4yIc1	několik
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obíhají	obíhat	k5eAaImIp3nP	obíhat
okolo	okolo	k7c2	okolo
obou	dva	k4xCgFnPc2	dva
složek	složka	k1gFnPc2	složka
dvojhvězdy	dvojhvězda	k1gFnSc2	dvojhvězda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
několik	několik	k4yIc1	několik
planet	planeta	k1gFnPc2	planeta
v	v	k7c6	v
trojhvězdách	trojhvězda	k1gFnPc6	trojhvězda
<g/>
,	,	kIx,	,
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
planeta	planeta	k1gFnSc1	planeta
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
čtyřhvězdy	čtyřhvězda	k1gFnSc2	čtyřhvězda
Kepler-	Kepler-	k1gFnSc2	Kepler-
<g/>
64	[number]	k4	64
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obecné	obecný	k2eAgFnPc4d1	obecná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Barva	barva	k1gFnSc1	barva
a	a	k8xC	a
jasnost	jasnost	k1gFnSc1	jasnost
planet	planeta	k1gFnPc2	planeta
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
určena	určen	k2eAgFnSc1d1	určena
barva	barva	k1gFnSc1	barva
exoplanety	exoplanet	k1gInPc4	exoplanet
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
albeda	albed	k1gMnSc2	albed
HD	HD	kA	HD
189733	[number]	k4	189733
<g/>
b	b	k?	b
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
planeta	planeta	k1gFnSc1	planeta
je	být	k5eAaImIp3nS	být
azurově	azurově	k6eAd1	azurově
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
<g/>
GJ	GJ	kA	GJ
504	[number]	k4	504
b	b	k?	b
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
viditelného	viditelný	k2eAgNnSc2d1	viditelné
spektra	spektrum	k1gNnSc2	spektrum
růžovofialovou	růžovofialový	k2eAgFnSc4d1	růžovofialová
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
<g/>
Kappa	kappa	k1gNnSc4	kappa
Andromedae	Andromeda	k1gFnSc2	Andromeda
b	b	k?	b
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
z	z	k7c2	z
blízkosti	blízkost	k1gFnSc2	blízkost
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
vypadala	vypadat	k5eAaImAgFnS	vypadat
načervenale	načervenale	k6eAd1	načervenale
<g/>
.	.	kIx.	.
<g/>
Zdánlivá	zdánlivý	k2eAgFnSc1d1	zdánlivá
<g />
.	.	kIx.	.
</s>
<s>
jasnost	jasnost	k1gFnSc1	jasnost
planety	planeta	k1gFnSc2	planeta
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
v	v	k7c6	v
jaké	jaký	k3yQgFnSc6	jaký
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc7	jaký
má	mít	k5eAaImIp3nS	mít
planeta	planeta	k1gFnSc1	planeta
odrazivost	odrazivost	k1gFnSc1	odrazivost
(	(	kIx(	(
<g/>
albedo	albedo	k1gNnSc1	albedo
<g/>
)	)	kIx)	)
a	a	k8xC	a
kolik	kolik	k4yRc4	kolik
světla	světlo	k1gNnSc2	světlo
přijímá	přijímat	k5eAaImIp3nS	přijímat
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
hvězdy	hvězda	k1gFnSc2	hvězda
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
zase	zase	k9	zase
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
v	v	k7c6	v
jaké	jaký	k3yIgFnSc6	jaký
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
je	být	k5eAaImIp3nS	být
planeta	planeta	k1gFnSc1	planeta
od	od	k7c2	od
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
jakou	jaký	k3yRgFnSc4	jaký
jasnost	jasnost	k1gFnSc4	jasnost
má	mít	k5eAaImIp3nS	mít
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
albedem	albed	k1gInSc7	albed
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
blízko	blízko	k7c2	blízko
své	svůj	k3xOyFgFnSc2	svůj
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jevit	jevit	k5eAaImF	jevit
jasnější	jasný	k2eAgFnSc1d2	jasnější
než	než	k8xS	než
planeta	planeta	k1gFnSc1	planeta
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
albedem	albed	k1gInSc7	albed
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
<g/>
Nejtemnější	temný	k2eAgFnSc4d3	nejtemnější
známou	známý	k2eAgFnSc4d1	známá
planetu	planeta	k1gFnSc4	planeta
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
geometrického	geometrický	k2eAgMnSc2d1	geometrický
albeda	albed	k1gMnSc2	albed
je	být	k5eAaImIp3nS	být
TrES-	TrES-	k1gFnSc4	TrES-
<g/>
2	[number]	k4	2
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
horký	horký	k2eAgMnSc1d1	horký
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
odráží	odrážet	k5eAaImIp3nS	odrážet
méně	málo	k6eAd2	málo
než	než	k8xS	než
1	[number]	k4	1
procento	procento	k1gNnSc4	procento
světla	světlo	k1gNnSc2	světlo
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Horké	Horké	k2eAgInPc1d1	Horké
Jupitery	Jupiter	k1gInPc1	Jupiter
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nP	jevit
poměrně	poměrně	k6eAd1	poměrně
tmavé	tmavý	k2eAgFnPc1d1	tmavá
kvůli	kvůli	k7c3	kvůli
přítomnosti	přítomnost	k1gFnSc3	přítomnost
sodíku	sodík	k1gInSc2	sodík
a	a	k8xC	a
draslíku	draslík	k1gInSc2	draslík
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
důvod	důvod	k1gInSc1	důvod
temného	temný	k2eAgNnSc2d1	temné
zbarvení	zbarvení	k1gNnSc2	zbarvení
TrES-	TrES-	k1gFnSc2	TrES-
<g/>
2	[number]	k4	2
<g/>
b	b	k?	b
však	však	k9	však
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
-	-	kIx~	-
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
zatím	zatím	k6eAd1	zatím
neznámou	známý	k2eNgFnSc7d1	neznámá
chemickou	chemický	k2eAgFnSc7d1	chemická
látkou	látka	k1gFnSc7	látka
<g/>
.	.	kIx.	.
<g/>
U	u	k7c2	u
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
geometrické	geometrický	k2eAgNnSc1d1	geometrické
albedo	albedo	k1gNnSc1	albedo
klesá	klesat	k5eAaImIp3nS	klesat
se	s	k7c7	s
zvyšující	zvyšující	k2eAgFnSc7d1	zvyšující
se	se	k3xPyFc4	se
metalicitou	metalicita	k1gFnSc7	metalicita
a	a	k8xC	a
atmosférickou	atmosférický	k2eAgFnSc7d1	atmosférická
teplotou	teplota	k1gFnSc7	teplota
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
neprojeví	projevit	k5eNaPmIp3nS	projevit
vliv	vliv	k1gInSc1	vliv
planetární	planetární	k2eAgFnSc2d1	planetární
oblačnosti	oblačnost	k1gFnSc2	oblačnost
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
oblačnost	oblačnost	k1gFnSc1	oblačnost
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
albedo	albedo	k1gNnSc4	albedo
ve	v	k7c6	v
viditelných	viditelný	k2eAgFnPc6d1	viditelná
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snižuje	snižovat	k5eAaImIp3nS	snižovat
ho	on	k3xPp3gNnSc4	on
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
infračervených	infračervený	k2eAgFnPc2d1	infračervená
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Optické	optický	k2eAgNnSc1d1	optické
albedo	albedo	k1gNnSc1	albedo
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
starší	starý	k2eAgFnPc1d2	starší
planety	planeta	k1gFnPc1	planeta
mají	mít	k5eAaImIp3nP	mít
vyšší	vysoký	k2eAgFnSc4d2	vyšší
oblačnost	oblačnost	k1gFnSc4	oblačnost
<g/>
.	.	kIx.	.
</s>
<s>
Optické	optický	k2eAgNnSc4d1	optické
albedo	albedo	k1gNnSc4	albedo
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
klesá	klesat	k5eAaImIp3nS	klesat
se	s	k7c7	s
zvyšující	zvyšující	k2eAgFnSc7d1	zvyšující
se	se	k3xPyFc4	se
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obří	obří	k2eAgFnPc1d1	obří
planety	planeta	k1gFnPc1	planeta
mají	mít	k5eAaImIp3nP	mít
vyšší	vysoký	k2eAgFnSc4d2	vyšší
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
gravitaci	gravitace	k1gFnSc4	gravitace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
nižší	nízký	k2eAgFnSc3d2	nižší
oblačnosti	oblačnost	k1gFnSc3	oblačnost
<g/>
.	.	kIx.	.
</s>
<s>
Eliptické	eliptický	k2eAgFnPc1d1	eliptická
dráhy	dráha	k1gFnPc1	dráha
mohou	moct	k5eAaImIp3nP	moct
rovněž	rovněž	k9	rovněž
způsobit	způsobit	k5eAaPmF	způsobit
velké	velký	k2eAgNnSc4d1	velké
kolísání	kolísání	k1gNnSc4	kolísání
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
opět	opět	k6eAd1	opět
významný	významný	k2eAgInSc4d1	významný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
jasnost	jasnost	k1gFnSc4	jasnost
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
infračerveném	infračervený	k2eAgInSc6d1	infračervený
oboru	obor	k1gInSc6	obor
spektra	spektrum	k1gNnSc2	spektrum
mladí	mladý	k2eAgMnPc1d1	mladý
a	a	k8xC	a
hmotní	hmotný	k2eAgMnPc1d1	hmotný
plynní	plynný	k2eAgMnPc1d1	plynný
obři	obr	k1gMnPc1	obr
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
více	hodně	k6eAd2	hodně
tepelných	tepelný	k2eAgFnPc2d1	tepelná
emisí	emise	k1gFnPc2	emise
než	než	k8xS	než
odráží	odrážet	k5eAaImIp3nP	odrážet
světla	světlo	k1gNnPc1	světlo
od	od	k7c2	od
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
optická	optický	k2eAgFnSc1d1	optická
jasnost	jasnost	k1gFnSc1	jasnost
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
fázi	fáze	k1gFnSc6	fáze
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
infračervené	infračervený	k2eAgFnSc6d1	infračervená
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
<g/>
Teplota	teplota	k1gFnSc1	teplota
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
klesá	klesat	k5eAaImIp3nS	klesat
v	v	k7c6	v
čase	čas	k1gInSc6	čas
a	a	k8xC	a
se	s	k7c7	s
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
od	od	k7c2	od
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Snížení	snížení	k1gNnSc1	snížení
teploty	teplota	k1gFnSc2	teplota
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
optické	optický	k2eAgNnSc1d1	optické
albedo	albedo	k1gNnSc1	albedo
i	i	k9	i
bez	bez	k7c2	bez
oblačnosti	oblačnost	k1gFnSc2	oblačnost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dostatečně	dostatečně	k6eAd1	dostatečně
nízké	nízký	k2eAgFnSc6d1	nízká
teplotě	teplota	k1gFnSc6	teplota
vznikají	vznikat	k5eAaImIp3nP	vznikat
mračna	mračno	k1gNnPc4	mračno
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
dále	daleko	k6eAd2	daleko
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
optické	optický	k2eAgNnSc4d1	optické
albedo	albedo	k1gNnSc4	albedo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ještě	ještě	k6eAd1	ještě
nižších	nízký	k2eAgFnPc6d2	nižší
teplotách	teplota	k1gFnPc6	teplota
vznikají	vznikat	k5eAaImIp3nP	vznikat
mračna	mračno	k1gNnPc1	mračno
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
největšímu	veliký	k2eAgMnSc3d3	veliký
albedu	albed	k1gMnSc3	albed
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
optických	optický	k2eAgFnPc2d1	optická
a	a	k8xC	a
blízkých	blízký	k2eAgFnPc2d1	blízká
infračervených	infračervený	k2eAgFnPc2d1	infračervená
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
okolo	okolo	k7c2	okolo
hvězdy	hvězda	k1gFnSc2	hvězda
HD	HD	kA	HD
209458	[number]	k4	209458
b	b	k?	b
zjištěno	zjištěn	k2eAgNnSc1d1	zjištěno
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
jakým	jaký	k3yRgInSc7	jaký
planeta	planeta	k1gFnSc1	planeta
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
vodík	vodík	k1gInSc4	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgFnSc1	první
nepřímá	přímý	k2eNgFnSc1d1	nepřímá
detekce	detekce	k1gFnSc1	detekce
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
u	u	k7c2	u
exoplanety	exoplanet	k1gInPc1	exoplanet
<g/>
.	.	kIx.	.
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
má	mít	k5eAaImIp3nS	mít
intenzitu	intenzita	k1gFnSc4	intenzita
jedné	jeden	k4xCgFnSc2	jeden
desetiny	desetina	k1gFnSc2	desetina
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
<g/>
Interakce	interakce	k1gFnSc1	interakce
mezi	mezi	k7c7	mezi
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
blízké	blízký	k2eAgFnSc2d1	blízká
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
hvězdou	hvězda	k1gFnSc7	hvězda
může	moct	k5eAaImIp3nS	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
na	na	k7c6	na
hvězdě	hvězda	k1gFnSc6	hvězda
skvrny	skvrna	k1gFnSc2	skvrna
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgNnSc7	jaký
Galileovy	Galileův	k2eAgInPc4d1	Galileův
měsíce	měsíc	k1gInPc4	měsíc
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
polární	polární	k2eAgFnSc1d1	polární
záře	záře	k1gFnSc1	záře
na	na	k7c6	na
Jupiteru	Jupiter	k1gInSc6	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Rádiové	rádiový	k2eAgFnPc1d1	rádiová
emise	emise	k1gFnPc1	emise
těchto	tento	k3xDgFnPc2	tento
skvrn	skvrna	k1gFnPc2	skvrna
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
pomocí	pomoc	k1gFnSc7	pomoc
rádiových	rádiový	k2eAgInPc2d1	rádiový
dalekohledů	dalekohled	k1gInPc2	dalekohled
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
LOFAR	LOFAR	kA	LOFAR
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
rádiových	rádiový	k2eAgFnPc2d1	rádiová
emisí	emise	k1gFnPc2	emise
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
rychlost	rychlost	k1gFnSc4	rychlost
rotace	rotace	k1gFnSc2	rotace
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
jinak	jinak	k6eAd1	jinak
obtížně	obtížně	k6eAd1	obtížně
zjistitelná	zjistitelný	k2eAgFnSc1d1	zjistitelná
<g/>
.	.	kIx.	.
<g/>
Zemské	zemský	k2eAgNnSc1d1	zemské
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vzniká	vznikat	k5eAaImIp3nS	vznikat
díky	díky	k7c3	díky
tekutému	tekutý	k2eAgNnSc3d1	tekuté
kovovému	kovový	k2eAgNnSc3d1	kovové
jádru	jádro	k1gNnSc3	jádro
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
hmotných	hmotný	k2eAgNnPc6d1	hmotné
superzemích	superzemí	k1gNnPc6	superzemí
pod	pod	k7c7	pod
vysokým	vysoký	k2eAgInSc7d1	vysoký
tlakem	tlak	k1gInSc7	tlak
mohou	moct	k5eAaImIp3nP	moct
vznikají	vznikat	k5eAaImIp3nP	vznikat
jiné	jiný	k2eAgFnPc1d1	jiná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
než	než	k8xS	než
vytvořené	vytvořený	k2eAgInPc1d1	vytvořený
za	za	k7c2	za
suchozemských	suchozemský	k2eAgFnPc2d1	suchozemská
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
větší	veliký	k2eAgFnSc4d2	veliký
viskozitu	viskozita	k1gFnSc4	viskozita
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc4d2	vyšší
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc2	tání
<g/>
,	,	kIx,	,
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
zabránit	zabránit	k5eAaPmF	zabránit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vnitřek	vnitřek	k1gInSc1	vnitřek
planety	planeta	k1gFnSc2	planeta
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
do	do	k7c2	do
různých	různý	k2eAgFnPc2d1	různá
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
nediferencovaným	diferencovaný	k2eNgInPc3d1	nediferencovaný
plášťům	plášť	k1gInPc3	plášť
bez	bez	k7c2	bez
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Formy	forma	k1gFnPc1	forma
oxidu	oxid	k1gInSc2	oxid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
MgSi	MgSi	k1gNnSc1	MgSi
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
tekutým	tekutý	k2eAgInSc7d1	tekutý
kovem	kov	k1gInSc7	kov
při	při	k7c6	při
tlacích	tlak	k1gInPc6	tlak
a	a	k8xC	a
teplotách	teplota	k1gFnPc6	teplota
nacházejících	nacházející	k2eAgFnPc2d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
superzemi	superze	k1gFnPc7	superze
a	a	k8xC	a
mohly	moct	k5eAaImAgFnP	moct
by	by	kYmCp3nP	by
vytvářet	vytvářet	k5eAaImF	vytvářet
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
v	v	k7c6	v
pláštích	plášť	k1gInPc6	plášť
superzemě	superzemě	k6eAd1	superzemě
<g/>
.	.	kIx.	.
<g/>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
horké	horký	k2eAgInPc1d1	horký
Jupitery	Jupiter	k1gInPc1	Jupiter
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgInSc4d2	veliký
poloměr	poloměr	k1gInSc4	poloměr
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
očekávalo	očekávat	k5eAaImAgNnS	očekávat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
interakcí	interakce	k1gFnPc2	interakce
mezi	mezi	k7c7	mezi
hvězdným	hvězdný	k2eAgInSc7d1	hvězdný
větrem	vítr	k1gInSc7	vítr
a	a	k8xC	a
magnetosférou	magnetosféra	k1gFnSc7	magnetosféra
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
elektrické	elektrický	k2eAgInPc4d1	elektrický
proudy	proud	k1gInPc4	proud
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
planetu	planeta	k1gFnSc4	planeta
ohřívají	ohřívat	k5eAaImIp3nP	ohřívat
a	a	k8xC	a
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
její	její	k3xOp3gNnSc4	její
rozpínání	rozpínání	k1gNnSc4	rozpínání
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
větší	veliký	k2eAgMnSc1d2	veliký
je	být	k5eAaImIp3nS	být
magnetismus	magnetismus	k1gInSc1	magnetismus
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgInSc1d2	veliký
intenzitu	intenzita	k1gFnSc4	intenzita
má	mít	k5eAaImIp3nS	mít
hvězdný	hvězdný	k2eAgInSc1d1	hvězdný
vítr	vítr	k1gInSc1	vítr
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
vzniká	vznikat	k5eAaImIp3nS	vznikat
i	i	k9	i
větší	veliký	k2eAgInSc4d2	veliký
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
většímu	veliký	k2eAgNnSc3d2	veliký
zahřívání	zahřívání	k1gNnSc3	zahřívání
a	a	k8xC	a
rozpínání	rozpínání	k1gNnSc3	rozpínání
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
pozorování	pozorování	k1gNnPc4	pozorování
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
aktivita	aktivita	k1gFnSc1	aktivita
má	mít	k5eAaImIp3nS	mít
přímou	přímý	k2eAgFnSc4d1	přímá
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
nafouknutými	nafouknutý	k2eAgInPc7d1	nafouknutý
planetárními	planetární	k2eAgInPc7d1	planetární
poloměry	poloměr	k1gInPc7	poloměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
dospěly	dochvít	k5eAaPmAgInP	dochvít
dva	dva	k4xCgInPc1	dva
nezávislé	závislý	k2eNgInPc1d1	nezávislý
týmy	tým	k1gInPc1	tým
vědců	vědec	k1gMnPc2	vědec
k	k	k7c3	k
protikladným	protikladný	k2eAgInPc3d1	protikladný
závěrům	závěr	k1gInPc3	závěr
o	o	k7c6	o
pravděpodobnosti	pravděpodobnost	k1gFnSc6	pravděpodobnost
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
na	na	k7c6	na
větších	veliký	k2eAgNnPc6d2	veliký
superzemích	superzemí	k1gNnPc6	superzemí
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jeden	jeden	k4xCgInSc1	jeden
tým	tým	k1gInSc1	tým
vydal	vydat	k5eAaPmAgInS	vydat
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
nebo	nebo	k8xC	nebo
stagnující	stagnující	k2eAgInSc1d1	stagnující
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
tým	tým	k1gInSc1	tým
vydal	vydat	k5eAaPmAgInS	vydat
protichůdnou	protichůdný	k2eAgFnSc4d1	protichůdná
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
na	na	k7c6	na
superzemích	superzemí	k1gNnPc6	superzemí
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
planeta	planeta	k1gFnSc1	planeta
je	být	k5eAaImIp3nS	být
suchá	suchý	k2eAgFnSc1d1	suchá
<g/>
.	.	kIx.	.
<g/>
Pokud	pokud	k8xS	pokud
superzemě	superzemě	k6eAd1	superzemě
mají	mít	k5eAaImIp3nP	mít
80	[number]	k4	80
<g/>
krát	krát	k6eAd1	krát
více	hodně	k6eAd2	hodně
vody	voda	k1gFnSc2	voda
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
stanou	stanout	k5eAaPmIp3nP	stanout
se	se	k3xPyFc4	se
oceánskými	oceánský	k2eAgFnPc7d1	oceánská
planetami	planeta	k1gFnPc7	planeta
s	s	k7c7	s
zcela	zcela	k6eAd1	zcela
zaplaveným	zaplavený	k2eAgInSc7d1	zaplavený
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
pokud	pokud	k8xS	pokud
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
tato	tento	k3xDgFnSc1	tento
hranice	hranice	k1gFnSc1	hranice
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgInSc1d1	vodní
cyklus	cyklus	k1gInSc1	cyklus
přesune	přesunout	k5eAaPmIp3nS	přesunout
dostatek	dostatek	k1gInSc4	dostatek
vody	voda	k1gFnSc2	voda
hluboko	hluboko	k6eAd1	hluboko
mezi	mezi	k7c7	mezi
oceány	oceán	k1gInPc7	oceán
a	a	k8xC	a
plášť	plášť	k1gInSc1	plášť
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
umožní	umožnit	k5eAaPmIp3nS	umožnit
existenci	existence	k1gFnSc4	existence
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vulkanismus	vulkanismus	k1gInSc1	vulkanismus
===	===	k?	===
</s>
</p>
<p>
<s>
Velké	velký	k2eAgFnPc1d1	velká
změny	změna	k1gFnPc1	změna
povrchové	povrchový	k2eAgFnSc2d1	povrchová
teploty	teplota	k1gFnSc2	teplota
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
u	u	k7c2	u
55	[number]	k4	55
Cancri	Cancri	k1gNnPc2	Cancri
jsou	být	k5eAaImIp3nP	být
připisovány	připisovat	k5eAaImNgInP	připisovat
možnému	možný	k2eAgNnSc3d1	možné
působení	působení	k1gNnSc3	působení
vulkanismu	vulkanismus	k1gInSc2	vulkanismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
velká	velký	k2eAgNnPc4d1	velké
prachová	prachový	k2eAgNnPc4d1	prachové
oblaka	oblaka	k1gNnPc4	oblaka
zakrývající	zakrývající	k2eAgInSc4d1	zakrývající
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
blokující	blokující	k2eAgFnSc2d1	blokující
tepelné	tepelný	k2eAgFnSc2d1	tepelná
emise	emise	k1gFnSc2	emise
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Prstence	prstenec	k1gInPc4	prstenec
===	===	k?	===
</s>
</p>
<p>
<s>
Hvězdu	hvězda	k1gFnSc4	hvězda
1SWASP	[number]	k4	1SWASP
J	J	kA	J
<g/>
140747.93	[number]	k4	140747.93
<g/>
-	-	kIx~	-
<g/>
394542.6	[number]	k4	394542.6
obíhá	obíhat	k5eAaImIp3nS	obíhat
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInPc4d2	veliký
prstence	prstenec	k1gInPc4	prstenec
než	než	k8xS	než
Saturn	Saturn	k1gInSc4	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
objektu	objekt	k1gInSc2	objekt
však	však	k9	však
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
;	;	kIx,	;
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
hnědého	hnědý	k2eAgMnSc4d1	hnědý
trpaslíka	trpaslík	k1gMnSc4	trpaslík
nebo	nebo	k8xC	nebo
hvězdu	hvězda	k1gFnSc4	hvězda
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
.	.	kIx.	.
<g/>
Jas	jas	k1gInSc1	jas
optických	optický	k2eAgInPc2d1	optický
obrazů	obraz	k1gInPc2	obraz
Fomalhautu	Fomalhaut	k1gInSc2	Fomalhaut
b	b	k?	b
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsoben	způsoben	k2eAgInSc1d1	způsoben
hvězdným	hvězdný	k2eAgNnSc7d1	Hvězdné
světlem	světlo	k1gNnSc7	světlo
odrážejícím	odrážející	k2eAgNnSc7d1	odrážející
se	se	k3xPyFc4	se
od	od	k7c2	od
prstence	prstenec	k1gInSc2	prstenec
s	s	k7c7	s
poloměrem	poloměr	k1gInSc7	poloměr
odpovídajícím	odpovídající	k2eAgInSc7d1	odpovídající
20	[number]	k4	20
až	až	k9	až
40	[number]	k4	40
<g/>
násobku	násobek	k1gInSc2	násobek
poloměru	poloměr	k1gInSc2	poloměr
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zhruba	zhruba	k6eAd1	zhruba
poloměru	poloměr	k1gInSc2	poloměr
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
galileových	galileův	k2eAgInPc2d1	galileův
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
<g/>
Prstence	prstenec	k1gInPc1	prstenec
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
rovině	rovina	k1gFnSc6	rovina
s	s	k7c7	s
rovníkem	rovník	k1gInSc7	rovník
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obíhají	obíhat	k5eAaImIp3nP	obíhat
blízko	blízko	k7c2	blízko
své	svůj	k3xOyFgFnSc2	svůj
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
však	však	k9	však
slapové	slapový	k2eAgFnPc4d1	slapová
síly	síla	k1gFnPc4	síla
hvězdy	hvězda	k1gFnSc2	hvězda
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejvzdálenější	vzdálený	k2eAgInPc1d3	nejvzdálenější
prstence	prstenec	k1gInPc1	prstenec
planety	planeta	k1gFnSc2	planeta
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
nacházet	nacházet	k5eAaImF	nacházet
v	v	k7c6	v
orbitální	orbitální	k2eAgFnSc6d1	orbitální
rovině	rovina	k1gFnSc6	rovina
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nejvnitřnější	vnitřní	k2eAgInPc1d3	nejvnitřnější
prstence	prstenec	k1gInPc1	prstenec
stále	stále	k6eAd1	stále
budou	být	k5eAaImBp3nP	být
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
rovině	rovina	k1gFnSc6	rovina
s	s	k7c7	s
planetárním	planetární	k2eAgInSc7d1	planetární
rovníkem	rovník	k1gInSc7	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
tedy	tedy	k8xC	tedy
taková	takový	k3xDgFnSc1	takový
planeta	planeta	k1gFnSc1	planeta
nakloněnou	nakloněný	k2eAgFnSc4d1	nakloněná
rotační	rotační	k2eAgFnSc4d1	rotační
osu	osa	k1gFnSc4	osa
<g/>
,	,	kIx,	,
odlišné	odlišný	k2eAgInPc4d1	odlišný
sklony	sklon	k1gInPc4	sklon
rovin	rovina	k1gFnPc2	rovina
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
a	a	k8xC	a
vnějších	vnější	k2eAgInPc2d1	vnější
prstenců	prstenec	k1gInPc2	prstenec
povedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
systému	systém	k1gInSc2	systém
s	s	k7c7	s
"	"	kIx"	"
<g/>
pokroucenými	pokroucený	k2eAgInPc7d1	pokroucený
<g/>
"	"	kIx"	"
prstenci	prstenec	k1gInPc7	prstenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Měsíce	měsíc	k1gInSc2	měsíc
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgMnS	být
oznámen	oznámen	k2eAgMnSc1d1	oznámen
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c6	na
exoměsíc	exoměsit	k5eAaImSgFnS	exoměsit
u	u	k7c2	u
toulavé	toulavý	k2eAgFnSc2d1	Toulavá
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2018	[number]	k4	2018
byl	být	k5eAaImAgInS	být
oznámen	oznámen	k2eAgInSc1d1	oznámen
objev	objev	k1gInSc1	objev
prvního	první	k4xOgInSc2	první
exoměsíce	exoměsit	k5eAaImSgFnP	exoměsit
obíhajícího	obíhající	k2eAgNnSc2d1	obíhající
planetu	planeta	k1gFnSc4	planeta
Kepler-	Kepler-	k1gFnPc2	Kepler-
<g/>
1625	[number]	k4	1625
<g/>
b.	b.	k?	b.
</s>
</p>
<p>
<s>
===	===	k?	===
Atmosféra	atmosféra	k1gFnSc1	atmosféra
===	===	k?	===
</s>
</p>
<p>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
byla	být	k5eAaImAgFnS	být
zjištěna	zjistit	k5eAaPmNgFnS	zjistit
u	u	k7c2	u
několika	několik	k4yIc2	několik
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
u	u	k7c2	u
hvězdy	hvězda	k1gFnSc2	hvězda
HD	HD	kA	HD
209458	[number]	k4	209458
b	b	k?	b
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
<g/>
KIC	KIC	kA	KIC
12557548	[number]	k4	12557548
b	b	k?	b
je	být	k5eAaImIp3nS	být
malou	malý	k2eAgFnSc7d1	malá
planetou	planeta	k1gFnSc7	planeta
se	s	k7c7	s
skalnatým	skalnatý	k2eAgInSc7d1	skalnatý
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obíhá	obíhat	k5eAaImIp3nS	obíhat
velmi	velmi	k6eAd1	velmi
blízko	blízko	k7c2	blízko
své	svůj	k3xOyFgFnSc2	svůj
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
planety	planeta	k1gFnSc2	planeta
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obrovský	obrovský	k2eAgInSc1d1	obrovský
oblak	oblak	k1gInSc1	oblak
prachu	prach	k1gInSc2	prach
jako	jako	k8xS	jako
u	u	k7c2	u
komety	kometa	k1gFnSc2	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Prach	prach	k1gInSc1	prach
může	moct	k5eAaImIp3nS	moct
pocházet	pocházet	k5eAaImF	pocházet
se	se	k3xPyFc4	se
sopečných	sopečný	k2eAgInPc2d1	sopečný
výbuchů	výbuch	k1gInPc2	výbuch
a	a	k8xC	a
z	z	k7c2	z
planety	planeta	k1gFnSc2	planeta
uniká	unikat	k5eAaImIp3nS	unikat
díky	díky	k7c3	díky
její	její	k3xOp3gFnSc3	její
nízké	nízký	k2eAgFnSc3d1	nízká
gravitaci	gravitace	k1gFnSc3	gravitace
<g/>
,	,	kIx,	,
a	a	k8xC	a
díky	díky	k7c3	díky
malé	malý	k2eAgFnSc3d1	malá
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
od	od	k7c2	od
hvězdy	hvězda	k1gFnSc2	hvězda
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vypařuje	vypařovat	k5eAaImIp3nS	vypařovat
<g/>
.	.	kIx.	.
</s>
<s>
Nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
kovy	kov	k1gInPc4	kov
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
malé	malý	k2eAgFnSc3d1	malá
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
od	od	k7c2	od
hvězdy	hvězda	k1gFnSc2	hvězda
postupně	postupně	k6eAd1	postupně
vypařují	vypařovat	k5eAaImIp3nP	vypařovat
a	a	k8xC	a
které	který	k3yQgFnPc1	který
poté	poté	k6eAd1	poté
zkondenzují	zkondenzovat	k5eAaPmIp3nP	zkondenzovat
na	na	k7c4	na
prach	prach	k1gInSc4	prach
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2015	[number]	k4	2015
vědci	vědec	k1gMnPc1	vědec
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
atmosféra	atmosféra	k1gFnSc1	atmosféra
planety	planeta	k1gFnSc2	planeta
GJ	GJ	kA	GJ
436	[number]	k4	436
b	b	k?	b
se	se	k3xPyFc4	se
odpařuje	odpařovat	k5eAaImIp3nS	odpařovat
vlivem	vliv	k1gInSc7	vliv
záření	záření	k1gNnSc2	záření
hostitelské	hostitelský	k2eAgFnSc2d1	hostitelská
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
obrovského	obrovský	k2eAgInSc2d1	obrovský
oblaku	oblak	k1gInSc2	oblak
za	za	k7c7	za
planetou	planeta	k1gFnSc7	planeta
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
ohonu	ohon	k1gInSc2	ohon
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
14	[number]	k4	14
<g/>
×	×	k?	×
<g/>
106	[number]	k4	106
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
zachycen	zachycen	k2eAgInSc1d1	zachycen
odraz	odraz	k1gInSc1	odraz
světla	světlo	k1gNnSc2	světlo
z	z	k7c2	z
ledových	ledový	k2eAgInPc2d1	ledový
krystalů	krystal	k1gInPc2	krystal
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Technologie	technologie	k1gFnSc1	technologie
použitá	použitý	k2eAgFnSc1d1	použitá
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
nalezení	nalezení	k1gNnSc3	nalezení
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
užitečná	užitečný	k2eAgNnPc1d1	užitečné
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
atmosfér	atmosféra	k1gFnPc2	atmosféra
vzdálených	vzdálený	k2eAgInPc2d1	vzdálený
světů	svět	k1gInPc2	svět
včetně	včetně	k7c2	včetně
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vázaná	vázaný	k2eAgFnSc1d1	vázaná
rotace	rotace	k1gFnSc1	rotace
===	===	k?	===
</s>
</p>
<p>
<s>
U	u	k7c2	u
planet	planeta	k1gFnPc2	planeta
s	s	k7c7	s
vázanou	vázaný	k2eAgFnSc7d1	vázaná
rotaci	rotace	k1gFnSc6	rotace
s	s	k7c7	s
rezonancí	rezonance	k1gFnSc7	rezonance
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
polokouli	polokoule	k1gFnSc4	polokoule
stále	stále	k6eAd1	stále
svítí	svítit	k5eAaImIp3nS	svítit
její	její	k3xOp3gFnSc1	její
hostitelská	hostitelský	k2eAgFnSc1d1	hostitelská
hvězda	hvězda	k1gFnSc1	hvězda
a	a	k8xC	a
polokoule	polokoule	k1gFnSc1	polokoule
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
velice	velice	k6eAd1	velice
horká	horký	k2eAgFnSc1d1	horká
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
druhá	druhý	k4xOgFnSc1	druhý
polokoule	polokoule	k1gFnSc1	polokoule
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
temnotě	temnota	k1gFnSc6	temnota
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
mrazivá	mrazivý	k2eAgFnSc1d1	mrazivá
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
planeta	planeta	k1gFnSc1	planeta
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
připomínat	připomínat	k5eAaImF	připomínat
oční	oční	k2eAgFnSc4d1	oční
bulvu	bulva	k1gFnSc4	bulva
s	s	k7c7	s
horkou	horký	k2eAgFnSc7d1	horká
skvrnou	skvrna	k1gFnSc7	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Planety	planeta	k1gFnPc1	planeta
s	s	k7c7	s
excentrickou	excentrický	k2eAgFnSc7d1	excentrická
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dráhou	dráha	k1gFnSc7	dráha
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
uzamčeny	uzamknout	k5eAaPmNgInP	uzamknout
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
rezonanci	rezonance	k1gFnSc6	rezonance
<g/>
.	.	kIx.	.
</s>
<s>
Rezonance	rezonance	k1gFnSc1	rezonance
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
a	a	k8xC	a
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
by	by	kYmCp3nP	by
vedly	vést	k5eAaImAgInP	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
dvou	dva	k4xCgFnPc2	dva
horkých	horký	k2eAgFnPc2d1	horká
skvrn	skvrna	k1gFnPc2	skvrna
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
i	i	k8xC	i
západní	západní	k2eAgFnSc6d1	západní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Planety	planeta	k1gFnPc1	planeta
s	s	k7c7	s
excentrickou	excentrický	k2eAgFnSc7d1	excentrická
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dráhou	dráha	k1gFnSc7	dráha
a	a	k8xC	a
se	s	k7c7	s
skloněnou	skloněný	k2eAgFnSc7d1	skloněná
osou	osa	k1gFnSc7	osa
rotace	rotace	k1gFnSc2	rotace
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
složitější	složitý	k2eAgInSc4d2	složitější
pohyb	pohyb	k1gInSc4	pohyb
své	svůj	k3xOyFgFnSc2	svůj
hvězdy	hvězda	k1gFnSc2	hvězda
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
jsou	být	k5eAaImIp3nP	být
objevovány	objevován	k2eAgInPc4d1	objevován
další	další	k2eAgInPc4d1	další
exoplanety	exoplanet	k1gInPc4	exoplanet
<g/>
,	,	kIx,	,
exoplanetologie	exoplanetologie	k1gFnPc4	exoplanetologie
má	mít	k5eAaImIp3nS	mít
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
objektů	objekt	k1gInPc2	objekt
pro	pro	k7c4	pro
podrobnější	podrobný	k2eAgNnSc4d2	podrobnější
studium	studium	k1gNnSc4	studium
extrasolárních	extrasolární	k2eAgInPc2d1	extrasolární
světů	svět	k1gInPc2	svět
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
zabývat	zabývat	k5eAaImF	zabývat
perspektivou	perspektiva	k1gFnSc7	perspektiva
života	život	k1gInSc2	život
na	na	k7c6	na
planetách	planeta	k1gFnPc6	planeta
mimo	mimo	k7c4	mimo
sluneční	sluneční	k2eAgInSc4d1	sluneční
soustavou	soustava	k1gFnSc7	soustava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kosmických	kosmický	k2eAgFnPc6d1	kosmická
vzdálenostech	vzdálenost	k1gFnPc6	vzdálenost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
život	život	k1gInSc1	život
zjištěn	zjistit	k5eAaPmNgInS	zjistit
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
v	v	k7c6	v
planetárním	planetární	k2eAgNnSc6d1	planetární
měřítku	měřítko	k1gNnSc6	měřítko
a	a	k8xC	a
silně	silně	k6eAd1	silně
modifikuje	modifikovat	k5eAaBmIp3nS	modifikovat
planetární	planetární	k2eAgNnSc4d1	planetární
prostředí	prostředí	k1gNnSc4	prostředí
takovým	takový	k3xDgInSc7	takový
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc4	tento
modifikace	modifikace	k1gFnPc4	modifikace
nelze	lze	k6eNd1	lze
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
klasickými	klasický	k2eAgInPc7d1	klasický
fyzikálně-chemickými	fyzikálněhemický	k2eAgInPc7d1	fyzikálně-chemický
procesy	proces	k1gInPc7	proces
(	(	kIx(	(
<g/>
z	z	k7c2	z
rovnovážných	rovnovážný	k2eAgInPc2d1	rovnovážný
procesů	proces	k1gInPc2	proces
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
molekulární	molekulární	k2eAgInSc4d1	molekulární
kyslík	kyslík	k1gInSc4	kyslík
(	(	kIx(	(
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
živých	živý	k2eAgFnPc2d1	živá
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgInPc2d1	další
druhů	druh	k1gInPc2	druh
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
kyslík	kyslík	k1gInSc1	kyslík
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
indikátorů	indikátor	k1gInPc2	indikátor
života	život	k1gInSc2	život
na	na	k7c6	na
exoplanetách	exoplaneta	k1gFnPc6	exoplaneta
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
kyslíku	kyslík	k1gInSc2	kyslík
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rovněž	rovněž	k6eAd1	rovněž
produkováno	produkovat	k5eAaImNgNnS	produkovat
nebiologickými	biologický	k2eNgInPc7d1	nebiologický
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Potenciálně	potenciálně	k6eAd1	potenciálně
obyvatelná	obyvatelný	k2eAgFnSc1d1	obyvatelná
planeta	planeta	k1gFnSc1	planeta
navíc	navíc	k6eAd1	navíc
musí	muset	k5eAaImIp3nS	muset
obíhat	obíhat	k5eAaImF	obíhat
stabilní	stabilní	k2eAgFnSc4d1	stabilní
hvězdu	hvězda	k1gFnSc4	hvězda
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
mohou	moct	k5eAaImIp3nP	moct
objekty	objekt	k1gInPc1	objekt
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
planety	planeta	k1gFnSc2	planeta
s	s	k7c7	s
dostatečným	dostatečný	k2eAgInSc7d1	dostatečný
atmosférickým	atmosférický	k2eAgInSc7d1	atmosférický
tlakem	tlak	k1gInSc7	tlak
umožňovat	umožňovat	k5eAaImF	umožňovat
existenci	existence	k1gFnSc4	existence
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kulturní	kulturní	k2eAgInSc1d1	kulturní
dopad	dopad	k1gInSc1	dopad
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
americký	americký	k2eAgInSc1d1	americký
Kongres	kongres	k1gInSc1	kongres
slyšení	slyšení	k1gNnSc2	slyšení
dvou	dva	k4xCgInPc2	dva
podvýborů	podvýbor	k1gInPc2	podvýbor
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
o	o	k7c6	o
objevech	objev	k1gInPc6	objev
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
<g/>
:	:	kIx,	:
Našli	najít	k5eAaPmAgMnP	najít
jsme	být	k5eAaImIp1nP	být
jiné	jiný	k2eAgFnPc4d1	jiná
země	zem	k1gFnPc4	zem
<g/>
?	?	kIx.	?
</s>
<s>
vyvolaného	vyvolaný	k2eAgInSc2d1	vyvolaný
objevem	objev	k1gInSc7	objev
exoplanety	exoplanet	k1gInPc4	exoplanet
Kepler-	Kepler-	k1gFnSc2	Kepler-
<g/>
62	[number]	k4	62
<g/>
f	f	k?	f
<g/>
,	,	kIx,	,
Kepler-	Kepler-	k1gFnSc1	Kepler-
<g/>
62	[number]	k4	62
<g/>
e	e	k0	e
a	a	k8xC	a
Kepler-	Kepler-	k1gFnSc6	Kepler-
<g/>
62	[number]	k4	62
<g/>
c.	c.	k?	c.
Zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
vydání	vydání	k1gNnSc4	vydání
časopisu	časopis	k1gInSc2	časopis
Science	Science	k1gFnSc2	Science
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
objevy	objev	k1gInPc4	objev
exoplanet	exoplanet	k5eAaBmF	exoplanet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Exoplanet	Exoplaneta	k1gFnPc2	Exoplaneta
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Extrasolární	Extrasolární	k2eAgFnSc1d1	Extrasolární
kometa	kometa	k1gFnSc1	kometa
</s>
</p>
<p>
<s>
Planetární	planetární	k2eAgFnSc1d1	planetární
soustava	soustava	k1gFnSc1	soustava
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
hvězd	hvězda	k1gFnPc2	hvězda
s	s	k7c7	s
potvrzenými	potvrzený	k2eAgFnPc7d1	potvrzená
planetami	planeta	k1gFnPc7	planeta
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
potenciálně	potenciálně	k6eAd1	potenciálně
obyvatelných	obyvatelný	k2eAgFnPc2d1	obyvatelná
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Exoplanety	Exoplanet	k1gInPc7	Exoplanet
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
exoplaneta	exoplaneta	k1gFnSc1	exoplaneta
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Extrasolar	Extrasolar	k1gMnSc1	Extrasolar
Planets	Planets	k1gInSc4	Planets
Encyclopaedia	Encyclopaedium	k1gNnSc2	Encyclopaedium
(	(	kIx(	(
<g/>
Paris	Paris	k1gMnSc1	Paris
Observatory	Observator	k1gMnPc7	Observator
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
NASA	NASA	kA	NASA
Exoplanet	Exoplanet	k1gMnSc1	Exoplanet
Archive	archiv	k1gInSc5	archiv
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Open	Open	k1gNnSc1	Open
Exoplanet	Exoplaneta	k1gFnPc2	Exoplaneta
Catalogue	Catalogue	k1gNnSc2	Catalogue
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Habitable	Habitable	k1gFnSc2	Habitable
Exoplanets	Exoplanetsa	k1gFnPc2	Exoplanetsa
Catalog	Catalog	k1gMnSc1	Catalog
(	(	kIx(	(
<g/>
PHL	PHL	kA	PHL
<g/>
/	/	kIx~	/
<g/>
UPR	UPR	kA	UPR
Arecibo	Areciba	k1gMnSc5	Areciba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Extrasolar	Extrasolar	k1gInSc1	Extrasolar
Planets	Planets	k1gInSc1	Planets
–	–	k?	–
D.	D.	kA	D.
Montes	Montes	k1gInSc1	Montes
<g/>
,	,	kIx,	,
UCM	UCM	kA	UCM
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Exoplanets	Exoplanetsa	k1gFnPc2	Exoplanetsa
at	at	k?	at
Paris	Paris	k1gMnSc1	Paris
Observatory	Observator	k1gInPc1	Observator
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Graphical	Graphical	k1gFnSc1	Graphical
Comparison	Comparison	k1gInSc1	Comparison
of	of	k?	of
Extrasolar	Extrasolar	k1gInSc1	Extrasolar
Planets	Planets	k1gInSc1	Planets
</s>
</p>
