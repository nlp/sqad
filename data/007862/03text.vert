<s>
Zmije	zmije	k1gFnSc1	zmije
růžkatá	růžkatý	k2eAgFnSc1d1	růžkatá
(	(	kIx(	(
<g/>
Vipera	Vipera	k1gFnSc1	Vipera
ammodytes	ammodytes	k1gMnSc1	ammodytes
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
had	had	k1gMnSc1	had
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
zmijovitých	zmijovitý	k2eAgMnPc2d1	zmijovitý
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
a	a	k8xC	a
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
pověst	pověst	k1gFnSc4	pověst
nejnebezpečnějšího	bezpečný	k2eNgMnSc4d3	nejnebezpečnější
evropského	evropský	k2eAgMnSc4d1	evropský
hada	had	k1gMnSc4	had
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
velikosti	velikost	k1gFnSc3	velikost
<g/>
,	,	kIx,	,
délce	délka	k1gFnSc3	délka
jedových	jedový	k2eAgInPc2d1	jedový
zubů	zub	k1gInPc2	zub
(	(	kIx(	(
<g/>
až	až	k9	až
13	[number]	k4	13
mm	mm	kA	mm
<g/>
)	)	kIx)	)
a	a	k8xC	a
vysoké	vysoký	k2eAgFnSc2d1	vysoká
účinnosti	účinnost	k1gFnSc2	účinnost
jedu	jed	k1gInSc2	jed
<g/>
.	.	kIx.	.
</s>
<s>
Vědecké	vědecký	k2eAgNnSc1d1	vědecké
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
řeckých	řecký	k2eAgNnPc2d1	řecké
slov	slovo	k1gNnPc2	slovo
ammos	ammos	k1gMnSc1	ammos
a	a	k8xC	a
dytes	dytes	k1gMnSc1	dytes
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
písek	písek	k1gInSc4	písek
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
norník	norník	k1gMnSc1	norník
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k6eAd1	právě
nejvýstižnější	výstižný	k2eAgNnSc1d3	nejvýstižnější
jméno	jméno	k1gNnSc1	jméno
pro	pro	k7c4	pro
hada	had	k1gMnSc4	had
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc1	přednost
skalnatému	skalnatý	k2eAgNnSc3d1	skalnaté
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
až	až	k9	až
do	do	k7c2	do
délky	délka	k1gFnSc2	délka
95	[number]	k4	95
cm	cm	kA	cm
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
jedinců	jedinec	k1gMnPc2	jedinec
však	však	k9	však
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
85	[number]	k4	85
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
bývají	bývat	k5eAaImIp3nP	bývat
trochu	trochu	k6eAd1	trochu
menší	malý	k2eAgFnSc3d2	menší
než	než	k8xS	než
samci	samec	k1gMnSc3	samec
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
délka	délka	k1gFnSc1	délka
také	také	k9	také
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
rase	rasa	k1gFnSc6	rasa
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
severní	severní	k2eAgInPc1d1	severní
poddruhy	poddruh	k1gInPc1	poddruh
jsou	být	k5eAaImIp3nP	být
zřetelně	zřetelně	k6eAd1	zřetelně
větší	veliký	k2eAgInPc1d2	veliký
než	než	k8xS	než
jižní	jižní	k2eAgInPc1d1	jižní
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Strugaria	Strugarium	k1gNnSc2	Strugarium
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
činí	činit	k5eAaImIp3nS	činit
průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
cm	cm	kA	cm
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
byli	být	k5eAaImAgMnP	být
spatřeni	spatřen	k2eAgMnPc1d1	spatřen
jedinci	jedinec	k1gMnPc1	jedinec
delší	dlouhý	k2eAgMnSc1d2	delší
než	než	k8xS	než
1	[number]	k4	1
m.	m.	k?	m.
Samice	samice	k1gFnPc1	samice
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
větší	veliký	k2eAgFnSc1d2	veliký
a	a	k8xC	a
robustnější	robustní	k2eAgFnSc1d2	robustnější
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
největší	veliký	k2eAgMnPc1d3	veliký
zaznamenaní	zaznamenaný	k2eAgMnPc1d1	zaznamenaný
jedinci	jedinec	k1gMnPc1	jedinec
jsou	být	k5eAaImIp3nP	být
samci	samec	k1gMnPc1	samec
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zmije	zmije	k1gFnSc1	zmije
růžkatá	růžkatý	k2eAgFnSc1d1	růžkatá
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Zmije	zmije	k1gFnSc1	zmije
růžkatá	růžkatý	k2eAgFnSc1d1	růžkatá
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
