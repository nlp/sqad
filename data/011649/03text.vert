<p>
<s>
Sýr	sýr	k1gInSc1	sýr
je	být	k5eAaImIp3nS	být
mléčný	mléčný	k2eAgInSc4d1	mléčný
výrobek	výrobek	k1gInSc4	výrobek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mléčnou	mléčný	k2eAgFnSc4d1	mléčná
bílkovinu	bílkovina	k1gFnSc4	bílkovina
<g/>
,	,	kIx,	,
tuky	tuk	k1gInPc4	tuk
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc4d1	ostatní
složky	složka	k1gFnPc4	složka
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
výroba	výroba	k1gFnSc1	výroba
sýra	sýr	k1gInSc2	sýr
je	být	k5eAaImIp3nS	být
datována	datovat	k5eAaImNgFnS	datovat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
8000	[number]	k4	8000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
poprvé	poprvé	k6eAd1	poprvé
domestikovány	domestikován	k2eAgFnPc4d1	domestikována
ovce	ovce	k1gFnPc4	ovce
<g/>
.	.	kIx.	.
</s>
<s>
Kůže	kůže	k1gFnPc1	kůže
a	a	k8xC	a
nafouknuté	nafouknutý	k2eAgInPc1d1	nafouknutý
vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
orgány	orgán	k1gInPc1	orgán
zvířat	zvíře	k1gNnPc2	zvíře
byly	být	k5eAaImAgInP	být
od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
používány	používat	k5eAaImNgFnP	používat
pro	pro	k7c4	pro
skladování	skladování	k1gNnSc4	skladování
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
vznik	vznik	k1gInSc4	vznik
tvarohu	tvaroh	k1gInSc2	tvaroh
a	a	k8xC	a
syrovátky	syrovátka	k1gFnSc2	syrovátka
omylem	omylem	k6eAd1	omylem
objeven	objevit	k5eAaPmNgInS	objevit
při	při	k7c6	při
uložení	uložení	k1gNnSc6	uložení
mléka	mléko	k1gNnSc2	mléko
do	do	k7c2	do
žaludku	žaludek	k1gInSc2	žaludek
zvířete	zvíře	k1gNnSc2	zvíře
díky	díky	k7c3	díky
obsaženému	obsažený	k2eAgNnSc3d1	obsažené
syřidlu	syřidlo	k1gNnSc3	syřidlo
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
variant	varianta	k1gFnPc2	varianta
legendy	legenda	k1gFnSc2	legenda
o	o	k7c6	o
arabském	arabský	k2eAgMnSc6d1	arabský
obchodníkovi	obchodník	k1gMnSc6	obchodník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
takto	takto	k6eAd1	takto
ukládal	ukládat	k5eAaImAgInS	ukládat
mléko	mléko	k1gNnSc4	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
archeologicky	archeologicky	k6eAd1	archeologicky
doložená	doložený	k2eAgFnSc1d1	doložená
výroba	výroba	k1gFnSc1	výroba
sýra	sýr	k1gInSc2	sýr
je	být	k5eAaImIp3nS	být
datována	datovat	k5eAaImNgFnS	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
5500	[number]	k4	5500
př.n.l.	př.n.l.	k?	př.n.l.
(	(	kIx(	(
<g/>
Kujawy	Kujawa	k1gMnSc2	Kujawa
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
cedník	cedník	k1gInSc1	cedník
s	s	k7c7	s
povlakem	povlak	k1gInSc7	povlak
s	s	k7c7	s
molekulami	molekula	k1gFnPc7	molekula
tuku	tuk	k1gInSc2	tuk
z	z	k7c2	z
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
egyptského	egyptský	k2eAgInSc2d1	egyptský
sýra	sýr	k1gInSc2	sýr
byly	být	k5eAaImAgFnP	být
učiněny	učiněn	k2eAgFnPc1d1	učiněna
ve	v	k7c6	v
staroegyptských	staroegyptský	k2eAgFnPc6d1	staroegyptská
pohřebních	pohřební	k2eAgFnPc6d1	pohřební
komorách	komora	k1gFnPc6	komora
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
datovány	datovat	k5eAaImNgFnP	datovat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
př.n.l.	př.n.l.	k?	př.n.l.
<g/>
Staročeské	staročeský	k2eAgInPc1d1	staročeský
"	"	kIx"	"
<g/>
kozí	kozí	k2eAgInPc1d1	kozí
syrečky	syreček	k1gInPc1	syreček
<g/>
"	"	kIx"	"
bývaly	bývat	k5eAaImAgFnP	bývat
výnosným	výnosný	k2eAgNnSc7d1	výnosné
exportním	exportní	k2eAgNnSc7d1	exportní
zbožím	zboží	k1gNnSc7	zboží
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
požadovány	požadovat	k5eAaImNgInP	požadovat
i	i	k9	i
pro	pro	k7c4	pro
svatbu	svatba	k1gFnSc4	svatba
krále	král	k1gMnSc2	král
Vladislava	Vladislav	k1gMnSc2	Vladislav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1502	[number]	k4	1502
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Olomoucké	olomoucký	k2eAgInPc1d1	olomoucký
tvarůžky	tvarůžek	k1gInPc1	tvarůžek
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
s	s	k7c7	s
oštěpky	oštěpek	k1gInPc7	oštěpek
a	a	k8xC	a
brynzou	brynza	k1gFnSc7	brynza
stálou	stálý	k2eAgFnSc7d1	stálá
specialitou	specialita	k1gFnSc7	specialita
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Sýr	sýr	k1gInSc1	sýr
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
srážením	srážení	k1gNnSc7	srážení
mléka	mléko	k1gNnSc2	mléko
syřidlem	syřidlo	k1gNnSc7	syřidlo
nebo	nebo	k8xC	nebo
kyselinou	kyselina	k1gFnSc7	kyselina
mléčnou	mléčný	k2eAgFnSc7d1	mléčná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ke	k	k7c3	k
sražení	sražení	k1gNnSc3	sražení
mléka	mléko	k1gNnSc2	mléko
použita	použít	k5eAaPmNgFnS	použít
i	i	k9	i
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
vinný	vinný	k2eAgInSc1d1	vinný
ocet	ocet	k1gInSc1	ocet
nebo	nebo	k8xC	nebo
citrónová	citrónový	k2eAgFnSc1d1	citrónová
šťáva	šťáva	k1gFnSc1	šťáva
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
mléko	mléko	k1gNnSc1	mléko
kravské	kravský	k2eAgFnSc2d1	kravská
<g/>
,	,	kIx,	,
ovčí	ovčí	k2eAgFnSc2d1	ovčí
<g/>
,	,	kIx,	,
kozí	kozí	k2eAgMnSc1d1	kozí
i	i	k9	i
mléko	mléko	k1gNnSc1	mléko
jiných	jiný	k2eAgMnPc2d1	jiný
savců	savec	k1gMnPc2	savec
(	(	kIx(	(
<g/>
buvolí	buvolí	k2eAgInSc1d1	buvolí
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
i	i	k9	i
velbloudí	velbloudí	k2eAgFnSc1d1	velbloudí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
mléka	mléko	k1gNnSc2	mléko
přidáván	přidáván	k2eAgInSc4d1	přidáván
chlorid	chlorid	k1gInSc4	chlorid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
výtěžnost	výtěžnost	k1gFnSc4	výtěžnost
mléka	mléko	k1gNnSc2	mléko
a	a	k8xC	a
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
jeho	jeho	k3xOp3gFnSc1	jeho
syřitelnost	syřitelnost	k1gFnSc1	syřitelnost
(	(	kIx(	(
<g/>
srážlivost	srážlivost	k1gFnSc1	srážlivost
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
dostatku	dostatek	k1gInSc6	dostatek
vápníku	vápník	k1gInSc2	vápník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
pasterizaci	pasterizace	k1gFnSc6	pasterizace
mléka	mléko	k1gNnSc2	mléko
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
kvůli	kvůli	k7c3	kvůli
zvýšení	zvýšení	k1gNnSc3	zvýšení
trvanlivosti	trvanlivost	k1gFnSc2	trvanlivost
odstraněny	odstraněn	k2eAgMnPc4d1	odstraněn
mikrobiální	mikrobiální	k2eAgMnPc4d1	mikrobiální
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
zahřátím	zahřátí	k1gNnSc7	zahřátí
je	být	k5eAaImIp3nS	být
též	též	k9	též
snížen	snížen	k2eAgInSc1d1	snížen
obsah	obsah	k1gInSc1	obsah
vápníku	vápník	k1gInSc2	vápník
<g/>
,	,	kIx,	,
oboje	oboj	k1gFnPc4	oboj
je	být	k5eAaImIp3nS	být
však	však	k9	však
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
pro	pro	k7c4	pro
proces	proces	k1gInSc4	proces
vytváření	vytváření	k1gNnSc2	vytváření
sýra	sýr	k1gInSc2	sýr
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vápník	vápník	k1gInSc4	vápník
i	i	k8xC	i
kultury	kultura	k1gFnSc2	kultura
do	do	k7c2	do
pasterovaného	pasterovaný	k2eAgNnSc2d1	pasterované
mléka	mléko	k1gNnSc2	mléko
zpětně	zpětně	k6eAd1	zpětně
přidat	přidat	k5eAaPmF	přidat
<g/>
.	.	kIx.	.
</s>
<s>
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
kultury	kultura	k1gFnSc2	kultura
lze	lze	k6eAd1	lze
doplnit	doplnit	k5eAaPmF	doplnit
přidáním	přidání	k1gNnSc7	přidání
zakysané	zakysaný	k2eAgFnSc2d1	zakysaná
smetany	smetana	k1gFnSc2	smetana
<g/>
,	,	kIx,	,
vápník	vápník	k1gInSc4	vápník
pomocí	pomocí	k7c2	pomocí
chloridu	chlorid	k1gInSc2	chlorid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
<g/>
.	.	kIx.	.
<g/>
Různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
a	a	k8xC	a
příchuti	příchuť	k1gFnPc1	příchuť
sýrů	sýr	k1gInPc2	sýr
jsou	být	k5eAaImIp3nP	být
výsledkem	výsledek	k1gInSc7	výsledek
použití	použití	k1gNnSc2	použití
mléka	mléko	k1gNnSc2	mléko
různých	různý	k2eAgMnPc2d1	různý
savců	savec	k1gMnPc2	savec
nebo	nebo	k8xC	nebo
různým	různý	k2eAgInSc7d1	různý
procentem	procent	k1gInSc7	procent
tuku	tuk	k1gInSc2	tuk
v	v	k7c6	v
sušině	sušina	k1gFnSc6	sušina
<g/>
,	,	kIx,	,
použití	použití	k1gNnSc6	použití
určitých	určitý	k2eAgInPc2d1	určitý
druhů	druh	k1gInPc2	druh
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
plísní	plíseň	k1gFnPc2	plíseň
nebo	nebo	k8xC	nebo
různou	různý	k2eAgFnSc7d1	různá
délkou	délka	k1gFnSc7	délka
zrání	zrání	k1gNnSc2	zrání
a	a	k8xC	a
jinými	jiný	k2eAgInPc7d1	jiný
postupy	postup	k1gInPc7	postup
při	při	k7c6	při
zpracovávání	zpracovávání	k1gNnSc6	zpracovávání
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
faktory	faktor	k1gInPc1	faktor
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
skladbu	skladba	k1gFnSc4	skladba
potravy	potrava	k1gFnSc2	potrava
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
také	také	k9	také
přidávání	přidávání	k1gNnSc1	přidávání
chuťových	chuťový	k2eAgFnPc2d1	chuťová
přísad	přísada	k1gFnPc2	přísada
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
bylinky	bylinka	k1gFnPc1	bylinka
nebo	nebo	k8xC	nebo
koření	koření	k1gNnPc1	koření
<g/>
.	.	kIx.	.
</s>
<s>
Chuť	chuť	k1gFnSc1	chuť
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
také	také	k6eAd1	také
pasterizací	pasterizace	k1gFnSc7	pasterizace
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konzumace	konzumace	k1gFnSc2	konzumace
==	==	k?	==
</s>
</p>
<p>
<s>
Sýr	sýr	k1gInSc1	sýr
se	se	k3xPyFc4	se
požívá	požívat	k5eAaImIp3nS	požívat
syrový	syrový	k2eAgMnSc1d1	syrový
<g/>
,	,	kIx,	,
vařený	vařený	k2eAgMnSc1d1	vařený
<g/>
,	,	kIx,	,
pečený	pečený	k2eAgMnSc1d1	pečený
<g/>
,	,	kIx,	,
uzený	uzený	k2eAgMnSc1d1	uzený
i	i	k8xC	i
smažený	smažený	k2eAgMnSc1d1	smažený
<g/>
,	,	kIx,	,
samotný	samotný	k2eAgMnSc1d1	samotný
nebo	nebo	k8xC	nebo
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
přílohami	příloha	k1gFnPc7	příloha
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ale	ale	k8xC	ale
i	i	k9	i
součástí	součást	k1gFnSc7	součást
různých	různý	k2eAgFnPc2d1	různá
omáček	omáčka	k1gFnPc2	omáčka
nebo	nebo	k8xC	nebo
zálivek	zálivka	k1gFnPc2	zálivka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
sýrů	sýr	k1gInPc2	sýr
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nP	vyrábět
stovky	stovka	k1gFnPc1	stovka
typů	typ	k1gInPc2	typ
sýra	sýr	k1gInSc2	sýr
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
různých	různý	k2eAgInPc2d1	různý
systémů	systém	k1gInPc2	systém
dělení	dělení	k1gNnSc2	dělení
sýrů	sýr	k1gInPc2	sýr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dle	dle	k7c2	dle
způsobu	způsob	k1gInSc2	způsob
srážení	srážení	k1gNnSc2	srážení
===	===	k?	===
</s>
</p>
<p>
<s>
tvarohové	tvarohový	k2eAgInPc4d1	tvarohový
(	(	kIx(	(
<g/>
kyselé	kyselý	k2eAgInPc4d1	kyselý
<g/>
)	)	kIx)	)
sýry	sýr	k1gInPc1	sýr
–	–	k?	–
do	do	k7c2	do
upraveného	upravený	k2eAgNnSc2d1	upravené
mléka	mléko	k1gNnSc2	mléko
se	se	k3xPyFc4	se
přidá	přidat	k5eAaPmIp3nS	přidat
zákys	zákys	k1gInSc1	zákys
<g/>
,	,	kIx,	,
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
syřidla	syřidlo	k1gNnSc2	syřidlo
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
srážení	srážení	k1gNnSc3	srážení
působením	působení	k1gNnSc7	působení
kys	kys	k?	kys
<g/>
.	.	kIx.	.
mléčné	mléčný	k2eAgFnSc2d1	mléčná
<g/>
.	.	kIx.	.
</s>
<s>
Sraženina	sraženina	k1gFnSc1	sraženina
po	po	k7c6	po
několika	několik	k4yIc6	několik
hodinách	hodina	k1gFnPc6	hodina
tuhne	tuhnout	k5eAaImIp3nS	tuhnout
<g/>
.	.	kIx.	.
</s>
<s>
Sraženina	sraženina	k1gFnSc1	sraženina
se	se	k3xPyFc4	se
vypouští	vypouštět	k5eAaImIp3nS	vypouštět
do	do	k7c2	do
tvarožníků	tvarožník	k1gInPc2	tvarožník
–	–	k?	–
klasická	klasický	k2eAgFnSc1d1	klasická
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
odstřeďuje	odstřeďovat	k5eAaImIp3nS	odstřeďovat
na	na	k7c6	na
tvarohářských	tvarohářský	k2eAgFnPc6d1	tvarohářský
odstředivkách	odstředivka	k1gFnPc6	odstředivka
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
tvaroh	tvaroh	k1gInSc1	tvaroh
vychladí	vychladit	k5eAaPmIp3nS	vychladit
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
<g/>
.	.	kIx.	.
</s>
<s>
Syrovátka	syrovátka	k1gFnSc1	syrovátka
která	který	k3yRgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
pří	pře	k1gFnSc7	pře
výrobě	výroba	k1gFnSc3	výroba
tvarohů	tvaroh	k1gInPc2	tvaroh
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
krmné	krmný	k2eAgInPc4d1	krmný
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
sýřené	sýřený	k2eAgFnPc1d1	sýřený
(	(	kIx(	(
<g/>
sladké	sladký	k2eAgFnPc1d1	sladká
–	–	k?	–
přírodní	přírodní	k2eAgInPc1d1	přírodní
<g/>
)	)	kIx)	)
sýry	sýr	k1gInPc1	sýr
–	–	k?	–
Do	do	k7c2	do
upraveného	upravený	k2eAgNnSc2d1	upravené
mléka	mléko	k1gNnSc2	mléko
ohřátého	ohřátý	k2eAgNnSc2d1	ohřáté
na	na	k7c6	na
cca	cca	kA	cca
31	[number]	k4	31
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
přidá	přidat	k5eAaPmIp3nS	přidat
smetanový	smetanový	k2eAgInSc1d1	smetanový
zákys	zákys	k1gInSc1	zákys
<g/>
,	,	kIx,	,
pomocné	pomocný	k2eAgFnPc1d1	pomocná
kultury	kultura	k1gFnPc1	kultura
<g/>
,	,	kIx,	,
syřidlo	syřidlo	k1gNnSc1	syřidlo
a	a	k8xC	a
chlorid	chlorid	k1gInSc1	chlorid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
.	.	kIx.	.
</s>
<s>
Mléko	mléko	k1gNnSc1	mléko
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
srážet	srážet	k5eAaImF	srážet
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
syřenina	syřenina	k1gFnSc1	syřenina
(	(	kIx(	(
<g/>
cca	cca	kA	cca
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
min	mina	k1gFnPc2	mina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
krájí	krájet	k5eAaImIp3nS	krájet
<g/>
,	,	kIx,	,
vytužuje	vytužovat	k5eAaImIp3nS	vytužovat
<g/>
,	,	kIx,	,
přidává	přidávat	k5eAaImIp3nS	přidávat
se	se	k3xPyFc4	se
prací	prací	k2eAgFnSc1d1	prací
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
dosouší	dosoušet	k5eAaImIp3nS	dosoušet
a	a	k8xC	a
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
vypouští	vypouštět	k5eAaImIp3nS	vypouštět
do	do	k7c2	do
lisovacích	lisovací	k2eAgFnPc2d1	lisovací
van	vana	k1gFnPc2	vana
<g/>
.	.	kIx.	.
</s>
<s>
Lisuje	lisovat	k5eAaImIp3nS	lisovat
se	se	k3xPyFc4	se
cca	cca	kA	cca
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
hodiny	hodina	k1gFnSc2	hodina
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
sýrů	sýr	k1gInPc2	sýr
až	až	k9	až
20	[number]	k4	20
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Vylisovaný	vylisovaný	k2eAgInSc1d1	vylisovaný
sýr	sýr	k1gInSc1	sýr
se	s	k7c7	s
solí	sůl	k1gFnSc7	sůl
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
dny	den	k1gInPc4	den
v	v	k7c6	v
solné	solný	k2eAgFnSc6d1	solná
lázni	lázeň	k1gFnSc6	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vysolení	vysolení	k1gNnSc6	vysolení
se	se	k3xPyFc4	se
zabalí	zabalit	k5eAaPmIp3nS	zabalit
do	do	k7c2	do
zracího	zrací	k2eAgInSc2d1	zrací
obalu	obal	k1gInSc2	obal
a	a	k8xC	a
nechá	nechat	k5eAaPmIp3nS	nechat
se	se	k3xPyFc4	se
zrát	zrát	k5eAaImF	zrát
ve	v	k7c6	v
zracích	zrak	k1gInPc6	zrak
sklepech	sklep	k1gInPc6	sklep
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
zrání	zrání	k1gNnSc2	zrání
je	být	k5eAaImIp3nS	být
různá	různý	k2eAgNnPc1d1	různé
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
sýra	sýr	k1gInSc2	sýr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
tavené	tavený	k2eAgInPc1d1	tavený
sýry	sýr	k1gInPc1	sýr
–	–	k?	–
z	z	k7c2	z
přírodních	přírodní	k2eAgInPc2d1	přírodní
sýrů	sýr	k1gInPc2	sýr
<g/>
,	,	kIx,	,
tvarohu	tvaroh	k1gInSc2	tvaroh
<g/>
,	,	kIx,	,
sušeného	sušený	k2eAgNnSc2d1	sušené
mléka	mléko	k1gNnSc2	mléko
<g/>
,	,	kIx,	,
sušeného	sušený	k2eAgNnSc2d1	sušené
podmáslí	podmáslí	k1gNnSc2	podmáslí
<g/>
,	,	kIx,	,
másla	máslo	k1gNnSc2	máslo
–	–	k?	–
příp	příp	kA	příp
<g/>
.	.	kIx.	.
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
kaseinu	kasein	k1gInSc2	kasein
<g/>
.	.	kIx.	.
</s>
<s>
Suroviny	surovina	k1gFnPc1	surovina
se	se	k3xPyFc4	se
rozemelou	rozemlít	k5eAaPmIp3nP	rozemlít
<g/>
,	,	kIx,	,
přidá	přidat	k5eAaPmIp3nS	přidat
se	se	k3xPyFc4	se
tavící	tavící	k2eAgFnSc1d1	tavící
sůl	sůl	k1gFnSc1	sůl
a	a	k8xC	a
směs	směs	k1gFnSc1	směs
se	se	k3xPyFc4	se
taví	tavit	k5eAaImIp3nS	tavit
ve	v	k7c6	v
speciální	speciální	k2eAgFnSc6d1	speciální
tavičce	tavička	k1gFnSc6	tavička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dle	dle	k7c2	dle
tvrdosti	tvrdost	k1gFnSc2	tvrdost
===	===	k?	===
</s>
</p>
<p>
<s>
čerstvé	čerstvý	k2eAgInPc1d1	čerstvý
sýry	sýr	k1gInPc1	sýr
–	–	k?	–
nemají	mít	k5eNaImIp3nP	mít
kůrku	kůrka	k1gFnSc4	kůrka
<g/>
,	,	kIx,	,
nedávají	dávat	k5eNaImIp3nP	dávat
se	se	k3xPyFc4	se
zrát	zrát	k5eAaImF	zrát
<g/>
;	;	kIx,	;
mají	mít	k5eAaImIp3nP	mít
konzistenci	konzistence	k1gFnSc4	konzistence
podobnou	podobný	k2eAgFnSc4d1	podobná
tvarohu	tvaroh	k1gInSc3	tvaroh
</s>
</p>
<p>
<s>
Lučina	lučina	k1gFnSc1	lučina
<g/>
,	,	kIx,	,
žervé	žervé	k1gNnSc1	žervé
<g/>
,	,	kIx,	,
ricotta	ricotta	k1gFnSc1	ricotta
<g/>
,	,	kIx,	,
cottage	cottage	k1gFnSc1	cottage
<g/>
,	,	kIx,	,
mascarpone	mascarpon	k1gMnSc5	mascarpon
</s>
</p>
<p>
<s>
měkké	měkký	k2eAgInPc1d1	měkký
sýry	sýr	k1gInPc1	sýr
–	–	k?	–
zrají	zrát	k5eAaImIp3nP	zrát
krátce	krátce	k6eAd1	krátce
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
menší	malý	k2eAgNnSc4d2	menší
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
tuku	tuk	k1gInSc2	tuk
</s>
</p>
<p>
<s>
smetanový	smetanový	k2eAgInSc1d1	smetanový
sýr	sýr	k1gInSc1	sýr
<g/>
,	,	kIx,	,
bryndza	bryndza	k1gFnSc1	bryndza
</s>
</p>
<p>
<s>
plísňové	plísňový	k2eAgInPc1d1	plísňový
sýry	sýr	k1gInPc1	sýr
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
plísní	plíseň	k1gFnSc7	plíseň
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
–	–	k?	–
obsah	obsah	k1gInSc1	obsah
tuku	tuk	k1gInSc2	tuk
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
%	%	kIx~	%
</s>
</p>
<p>
<s>
brie	brie	k1gFnSc1	brie
<g/>
,	,	kIx,	,
Hermelín	hermelín	k1gInSc1	hermelín
<g/>
,	,	kIx,	,
Camembert	camembert	k1gInSc1	camembert
<g/>
,	,	kIx,	,
Roquefort	Roquefort	k1gInSc1	Roquefort
</s>
</p>
<p>
<s>
polotvrdé	polotvrdý	k2eAgInPc1d1	polotvrdý
sýry	sýr	k1gInPc1	sýr
–	–	k?	–
zrají	zrát	k5eAaImIp3nP	zrát
déle	dlouho	k6eAd2	dlouho
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
krájí	krájet	k5eAaImIp3nP	krájet
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
méně	málo	k6eAd2	málo
vody	voda	k1gFnPc1	voda
</s>
</p>
<p>
<s>
čedar	čedar	k1gInSc1	čedar
<g/>
,	,	kIx,	,
Gouda	gouda	k1gFnSc1	gouda
<g/>
,	,	kIx,	,
Eidam	eidam	k1gInSc1	eidam
<g/>
,	,	kIx,	,
Ementál	ementál	k1gInSc1	ementál
<g/>
,	,	kIx,	,
Raclette	Raclett	k1gMnSc5	Raclett
<g/>
,	,	kIx,	,
Cantal	Cantal	k1gMnSc1	Cantal
</s>
</p>
<p>
<s>
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
sýry	sýr	k1gInPc1	sýr
–	–	k?	–
zrají	zrát	k5eAaImIp3nP	zrát
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
nízký	nízký	k2eAgInSc1d1	nízký
obsah	obsah	k1gInSc1	obsah
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc4	obsah
tuku	tuk	k1gInSc2	tuk
až	až	k9	až
50	[number]	k4	50
%	%	kIx~	%
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
strouhají	strouhat	k5eAaImIp3nP	strouhat
</s>
</p>
<p>
<s>
parmezán	parmezán	k1gInSc1	parmezán
<g/>
,	,	kIx,	,
pecorino	pecorino	k1gNnSc1	pecorino
<g/>
,	,	kIx,	,
sbrinz	sbrinz	k1gInSc1	sbrinz
</s>
</p>
<p>
<s>
tavené	tavený	k2eAgInPc1d1	tavený
sýry	sýr	k1gInPc1	sýr
–	–	k?	–
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
jednoho	jeden	k4xCgMnSc2	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
druhů	druh	k1gInPc2	druh
sýrů	sýr	k1gInPc2	sýr
(	(	kIx(	(
<g/>
tvrdých	tvrdý	k2eAgInPc2d1	tvrdý
i	i	k8xC	i
měkkých	měkký	k2eAgInPc2d1	měkký
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
tavicích	tavicí	k2eAgFnPc2d1	tavicí
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
trvanlivé	trvanlivý	k2eAgInPc1d1	trvanlivý
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
různě	různě	k6eAd1	různě
dochucované	dochucovaný	k2eAgNnSc1d1	dochucované
</s>
</p>
<p>
<s>
===	===	k?	===
Dle	dle	k7c2	dle
obsahu	obsah	k1gInSc2	obsah
tuku	tuk	k1gInSc2	tuk
v	v	k7c6	v
sušině	sušina	k1gFnSc6	sušina
===	===	k?	===
</s>
</p>
<p>
<s>
vysokotučné	vysokotučný	k2eAgFnPc4d1	vysokotučná
–	–	k?	–
nad	nad	k7c7	nad
60	[number]	k4	60
%	%	kIx~	%
</s>
</p>
<p>
<s>
smetanové	smetanový	k2eAgInPc1d1	smetanový
–	–	k?	–
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
%	%	kIx~	%
</s>
</p>
<p>
<s>
plnotučné	plnotučný	k2eAgInPc1d1	plnotučný
–	–	k?	–
45	[number]	k4	45
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
%	%	kIx~	%
</s>
</p>
<p>
<s>
tučné	tučný	k2eAgInPc1d1	tučný
–	–	k?	–
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
%	%	kIx~	%
</s>
</p>
<p>
<s>
tříčtvrtětučné	tříčtvrtětučný	k2eAgInPc1d1	tříčtvrtětučný
–	–	k?	–
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
%	%	kIx~	%
</s>
</p>
<p>
<s>
polotučné	polotučný	k2eAgInPc1d1	polotučný
–	–	k?	–
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
%	%	kIx~	%
</s>
</p>
<p>
<s>
nízkotučné	nízkotučný	k2eAgNnSc1d1	nízkotučné
–	–	k?	–
do	do	k7c2	do
10	[number]	k4	10
%	%	kIx~	%
</s>
</p>
<p>
<s>
==	==	k?	==
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
sýra	sýr	k1gInSc2	sýr
==	==	k?	==
</s>
</p>
<p>
<s>
Abondance	Abondance	k1gFnSc1	Abondance
</s>
</p>
<p>
<s>
Balkánský	balkánský	k2eAgInSc1d1	balkánský
sýr	sýr	k1gInSc1	sýr
</s>
</p>
<p>
<s>
Beaufort	Beaufort	k1gInSc1	Beaufort
</s>
</p>
<p>
<s>
Bleu	Bleu	k6eAd1	Bleu
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Auvergne	Auvergn	k1gInSc5	Auvergn
–	–	k?	–
sýr	sýr	k1gInSc1	sýr
s	s	k7c7	s
modrou	modrý	k2eAgFnSc7d1	modrá
plísní	plíseň	k1gFnSc7	plíseň
</s>
</p>
<p>
<s>
Bresse	Bresse	k1gFnSc1	Bresse
Bleu	Bleus	k1gInSc2	Bleus
–	–	k?	–
sýr	sýr	k1gInSc1	sýr
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
i	i	k8xC	i
modrou	modrý	k2eAgFnSc7d1	modrá
plísní	plíseň	k1gFnSc7	plíseň
</s>
</p>
<p>
<s>
Buchette	Buchette	k5eAaPmIp2nP	Buchette
–	–	k?	–
kozí	kozí	k2eAgInSc1d1	kozí
sýr	sýr	k1gInSc1	sýr
</s>
</p>
<p>
<s>
Brynza	brynza	k1gFnSc1	brynza
</s>
</p>
<p>
<s>
Chavroux	Chavroux	k1gInSc1	Chavroux
–	–	k?	–
čerstvý	čerstvý	k2eAgInSc1d1	čerstvý
kozí	kozí	k2eAgInSc1d1	kozí
sýr	sýr	k1gInSc1	sýr
</s>
</p>
<p>
<s>
Comté	Comta	k1gMnPc1	Comta
</s>
</p>
<p>
<s>
Cottage	Cottage	k6eAd1	Cottage
</s>
</p>
<p>
<s>
Eidam	eidam	k1gInSc1	eidam
</s>
</p>
<p>
<s>
Epoisses	Epoisses	k1gMnSc1	Epoisses
</s>
</p>
<p>
<s>
Gorgonzola	gorgonzola	k1gFnSc1	gorgonzola
–	–	k?	–
tučný	tučný	k2eAgInSc1d1	tučný
italský	italský	k2eAgInSc1d1	italský
sýr	sýr	k1gInSc1	sýr
ostřejší	ostrý	k2eAgFnSc2d2	ostřejší
chuti	chuť	k1gFnSc2	chuť
<g/>
,	,	kIx,	,
prorostlý	prorostlý	k2eAgInSc1d1	prorostlý
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
modrou	modrý	k2eAgFnSc7d1	modrá
plísní	plíseň	k1gFnSc7	plíseň
</s>
</p>
<p>
<s>
Jadel	Jadel	k1gInSc1	Jadel
–	–	k?	–
český	český	k2eAgInSc1d1	český
pletený	pletený	k2eAgInSc1d1	pletený
pařený	pařený	k2eAgInSc1d1	pařený
ovčí	ovčí	k2eAgInSc1d1	ovčí
sýr	sýr	k1gInSc1	sýr
</s>
</p>
<p>
<s>
Mozzarella	Mozzarella	k6eAd1	Mozzarella
</s>
</p>
<p>
<s>
Niva	niva	k1gFnSc1	niva
</s>
</p>
<p>
<s>
Olomoucké	olomoucký	k2eAgInPc1d1	olomoucký
tvarůžky	tvarůžek	k1gInPc1	tvarůžek
</s>
</p>
<p>
<s>
Oštěpek	oštěpek	k1gInSc1	oštěpek
</s>
</p>
<p>
<s>
Parenica	parenica	k1gFnSc1	parenica
</s>
</p>
<p>
<s>
Roquefort	Roquefort	k1gInSc1	Roquefort
</s>
</p>
<p>
<s>
==	==	k?	==
Náhražky	náhražka	k1gFnPc4	náhražka
sýrů	sýr	k1gInPc2	sýr
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
imitace	imitace	k1gFnPc4	imitace
sýrů	sýr	k1gInPc2	sýr
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
alternativní	alternativní	k2eAgInPc4d1	alternativní
sýry	sýr	k1gInPc4	sýr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
kaseinů	kasein	k1gInPc2	kasein
<g/>
,	,	kIx,	,
z	z	k7c2	z
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
soli	sůl	k1gFnSc2	sůl
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
bílkovina	bílkovina	k1gFnSc1	bílkovina
bývá	bývat	k5eAaImIp3nS	bývat
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
bílkovinou	bílkovina	k1gFnSc7	bílkovina
rostlinnou	rostlinný	k2eAgFnSc7d1	rostlinná
nebo	nebo	k8xC	nebo
škroby	škrob	k1gInPc4	škrob
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zlevňuje	zlevňovat	k5eAaImIp3nS	zlevňovat
výrobek	výrobek	k1gInSc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
náhražky	náhražka	k1gFnPc1	náhražka
mají	mít	k5eAaImIp3nP	mít
ovšem	ovšem	k9	ovšem
se	s	k7c7	s
sýry	sýr	k1gInPc7	sýr
pramálo	pramálo	k6eAd1	pramálo
společného	společný	k2eAgInSc2d1	společný
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
naprosto	naprosto	k6eAd1	naprosto
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
chuť	chuť	k1gFnSc4	chuť
i	i	k8xC	i
vůni	vůně	k1gFnSc4	vůně
<g/>
,	,	kIx,	,
možnosti	možnost	k1gFnPc4	možnost
jejich	jejich	k3xOp3gNnSc4	jejich
zpracování	zpracování	k1gNnSc4	zpracování
jsou	být	k5eAaImIp3nP	být
omezené	omezený	k2eAgFnPc1d1	omezená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Nakládaný	nakládaný	k2eAgInSc1d1	nakládaný
hermelín	hermelín	k1gInSc1	hermelín
</s>
</p>
<p>
<s>
Smažený	smažený	k2eAgInSc1d1	smažený
sýr	sýr	k1gInSc1	sýr
</s>
</p>
<p>
<s>
Syrovátka	syrovátka	k1gFnSc1	syrovátka
</s>
</p>
<p>
<s>
Sýrohub	sýrohub	k1gMnSc1	sýrohub
</s>
</p>
<p>
<s>
Sýrorodka	Sýrorodka	k1gFnSc1	Sýrorodka
</s>
</p>
<p>
<s>
Tvaroh	tvaroh	k1gInSc1	tvaroh
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sýr	sýr	k1gInSc1	sýr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
sýr	sýr	k1gInSc1	sýr
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Sýr	sýr	k1gInSc1	sýr
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
