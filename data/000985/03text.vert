<s>
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Stát	stát	k5eAaPmF	stát
Mongolsko	Mongolsko	k1gNnSc4	Mongolsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
stát	stát	k1gInSc1	stát
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
hraničící	hraničící	k2eAgFnSc6d1	hraničící
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Čínskou	čínský	k2eAgFnSc7d1	čínská
lidovou	lidový	k2eAgFnSc7d1	lidová
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
plochu	plocha	k1gFnSc4	plocha
1566500	[number]	k4	1566500
km2	km2	k4	km2
(	(	kIx(	(
<g/>
dvacetkrát	dvacetkrát	k6eAd1	dvacetkrát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
pouze	pouze	k6eAd1	pouze
2,7	[number]	k4	2,7
milionům	milion	k4xCgInPc3	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
státem	stát	k1gInSc7	stát
s	s	k7c7	s
nejnižší	nízký	k2eAgFnSc7d3	nejnižší
hustotou	hustota	k1gFnSc7	hustota
zalidnění	zalidnění	k1gNnPc2	zalidnění
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
stepi	step	k1gFnPc4	step
s	s	k7c7	s
drsným	drsný	k2eAgNnSc7d1	drsné
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
obživy	obživa	k1gFnSc2	obživa
je	být	k5eAaImIp3nS	být
kočovné	kočovný	k2eAgNnSc1d1	kočovné
pastevectví	pastevectví	k1gNnSc1	pastevectví
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
hospodářství	hospodářství	k1gNnSc4	hospodářství
země	zem	k1gFnSc2	zem
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
vývoz	vývoz	k1gInSc4	vývoz
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
výrobky	výrobek	k1gInPc1	výrobek
z	z	k7c2	z
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
látky	látka	k1gFnPc1	látka
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
výrobky	výrobek	k1gInPc1	výrobek
z	z	k7c2	z
kašmíru	kašmír	k1gInSc2	kašmír
<g/>
,	,	kIx,	,
a	a	k8xC	a
výrobky	výrobek	k1gInPc1	výrobek
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
velkým	velký	k2eAgNnSc7d1	velké
tempem	tempo	k1gNnSc7	tempo
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
těžba	těžba	k1gFnSc1	těžba
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
příjmů	příjem	k1gInPc2	příjem
státu	stát	k1gInSc2	stát
již	již	k6eAd1	již
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Ulánbátar	Ulánbátar	k1gInSc1	Ulánbátar
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
1,3	[number]	k4	1,3
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
představuje	představovat	k5eAaImIp3nS	představovat
téměř	téměř	k6eAd1	téměř
polovinu	polovina	k1gFnSc4	polovina
populace	populace	k1gFnSc2	populace
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgNnPc7d1	další
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Erdenet	Erdeneta	k1gFnPc2	Erdeneta
<g/>
,	,	kIx,	,
Darchan	Darchana	k1gFnPc2	Darchana
a	a	k8xC	a
Čojbalsan	Čojbalsana	k1gFnPc2	Čojbalsana
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Státu	stát	k1gInSc2	stát
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
části	část	k1gFnSc6	část
území	území	k1gNnSc2	území
tzv.	tzv.	kA	tzv.
Vnějšího	vnější	k2eAgNnSc2d1	vnější
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
tzv.	tzv.	kA	tzv.
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
jako	jako	k9	jako
autonomní	autonomní	k2eAgFnSc1d1	autonomní
oblast	oblast	k1gFnSc1	oblast
součástí	součást	k1gFnPc2	součást
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Kočovné	kočovný	k2eAgInPc1d1	kočovný
kmeny	kmen	k1gInPc1	kmen
Siungnuové	Siungnuová	k1gFnSc3	Siungnuová
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
nimž	jenž	k3xRgInPc3	jenž
byla	být	k5eAaImAgFnS	být
budována	budován	k2eAgFnSc1d1	budována
Velká	velký	k2eAgFnSc1d1	velká
čínská	čínský	k2eAgFnSc1d1	čínská
zeď	zeď	k1gFnSc1	zeď
<g/>
,	,	kIx,	,
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
na	na	k7c6	na
území	území	k1gNnSc6	území
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
mocný	mocný	k2eAgInSc4d1	mocný
kmenový	kmenový	k2eAgInSc4d1	kmenový
svaz	svaz	k1gInSc4	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
Siungnuové	Siungnuus	k1gMnPc1	Siungnuus
se	se	k3xPyFc4	se
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
smísili	smísit	k5eAaPmAgMnP	smísit
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
etniky	etnikum	k1gNnPc7	etnikum
a	a	k8xC	a
dali	dát	k5eAaPmAgMnP	dát
vznik	vznik	k1gInSc4	vznik
Hunům	Hun	k1gMnPc3	Hun
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc4	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
centrem	centrum	k1gNnSc7	centrum
Mongolské	mongolský	k2eAgFnSc2d1	mongolská
říše	říš	k1gFnSc2	říš
založené	založený	k2eAgNnSc1d1	založené
Čingischánem	Čingischán	k1gMnSc7	Čingischán
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
říše	říše	k1gFnSc1	říše
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Roku	rok	k1gInSc2	rok
1241	[number]	k4	1241
táhla	táhnout	k5eAaImAgNnP	táhnout
mongolská	mongolský	k2eAgNnPc1d1	mongolské
vojska	vojsko	k1gNnPc1	vojsko
Moravou	Morava	k1gFnSc7	Morava
a	a	k8xC	a
vyplenila	vyplenit	k5eAaPmAgFnS	vyplenit
klášter	klášter	k1gInSc4	klášter
Hradisko	hradisko	k1gNnSc1	hradisko
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Mongolská	mongolský	k2eAgFnSc1d1	mongolská
říše	říše	k1gFnSc1	říše
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
a	a	k8xC	a
země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
sporů	spor	k1gInPc2	spor
a	a	k8xC	a
potyček	potyčka	k1gFnPc2	potyčka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
mongolské	mongolský	k2eAgFnSc2d1	mongolská
dynastie	dynastie	k1gFnSc2	dynastie
Jüan	jüan	k1gInSc1	jüan
v	v	k7c6	v
r.	r.	kA	r.
1368	[number]	k4	1368
začal	začít	k5eAaPmAgInS	začít
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
střet	střet	k1gInSc1	střet
mezi	mezi	k7c7	mezi
východními	východní	k2eAgInPc7d1	východní
chalšskými	chalšský	k2eAgInPc7d1	chalšský
a	a	k8xC	a
západními	západní	k2eAgInPc7d1	západní
ojratskými	ojratský	k2eAgInPc7d1	ojratský
Mongoly	mongol	k1gInPc7	mongol
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
postupné	postupný	k2eAgNnSc4d1	postupné
vytlačování	vytlačování	k1gNnSc4	vytlačování
Ojratů	Ojrat	k1gInPc2	Ojrat
na	na	k7c4	na
západ	západ	k1gInSc4	západ
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
Ojratů	Ojrat	k1gInPc2	Ojrat
migrovala	migrovat	k5eAaImAgFnS	migrovat
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
k	k	k7c3	k
povodí	povodí	k1gNnSc3	povodí
Volhy	Volha	k1gFnSc2	Volha
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jsou	být	k5eAaImIp3nP	být
známi	znám	k2eAgMnPc1d1	znám
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Kalmykové	Kalmyk	k1gMnPc1	Kalmyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
dobytí	dobytí	k1gNnSc4	dobytí
části	část	k1gFnSc2	část
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
mandžuskou	mandžuský	k2eAgFnSc7d1	mandžuská
dynastií	dynastie	k1gFnSc7	dynastie
Čching	Čching	k1gInSc1	Čching
<g/>
,	,	kIx,	,
ovládající	ovládající	k2eAgFnSc4d1	ovládající
Čínu	Čína	k1gFnSc4	Čína
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1636	[number]	k4	1636
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
země	zem	k1gFnSc2	zem
na	na	k7c4	na
Vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
(	(	kIx(	(
<g/>
ovládané	ovládaný	k2eAgInPc4d1	ovládaný
Čchingy	Čching	k1gInPc4	Čching
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vnější	vnější	k2eAgNnSc1d1	vnější
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
Vnější	vnější	k2eAgNnSc1d1	vnější
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1691	[number]	k4	1691
podmaněno	podmanit	k5eAaPmNgNnS	podmanit
<g/>
.	.	kIx.	.
</s>
<s>
Džúngarský	Džúngarský	k2eAgInSc4d1	Džúngarský
chanát	chanát	k1gInSc4	chanát
ovládaný	ovládaný	k2eAgInSc4d1	ovládaný
Ojraty	Ojrat	k1gInPc7	Ojrat
byl	být	k5eAaImAgMnS	být
zničen	zničen	k2eAgMnSc1d1	zničen
dynastaií	dynastaie	k1gFnPc2	dynastaie
Čching	Čching	k1gInSc1	Čching
roku	rok	k1gInSc2	rok
1757	[number]	k4	1757
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
Ojratů	Ojrat	k1gMnPc2	Ojrat
zahynula	zahynout	k5eAaPmAgFnS	zahynout
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
s	s	k7c7	s
čínskou	čínský	k2eAgFnSc7d1	čínská
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
a	a	k8xC	a
Vnější	vnější	k2eAgNnSc1d1	vnější
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Číně	Čína	k1gFnSc6	Čína
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nezávislosti	nezávislost	k1gFnSc3	nezávislost
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
pouze	pouze	k6eAd1	pouze
Vnější	vnější	k2eAgNnSc1d1	vnější
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Bogdgegéna	Bogdgegén	k1gInSc2	Bogdgegén
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
bylo	být	k5eAaImAgNnS	být
Vnější	vnější	k2eAgNnSc1d1	vnější
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
opět	opět	k6eAd1	opět
obsazeno	obsazen	k2eAgNnSc4d1	obsazeno
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozšíření	rozšíření	k1gNnSc6	rozšíření
ruské	ruský	k2eAgFnSc2d1	ruská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
na	na	k7c4	na
východ	východ	k1gInSc4	východ
byli	být	k5eAaImAgMnP	být
Číňané	Číňan	k1gMnPc1	Číňan
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
bělogvardějskými	bělogvardějský	k2eAgFnPc7d1	bělogvardějská
jednotkami	jednotka	k1gFnPc7	jednotka
barona	baron	k1gMnSc2	baron
Ungerna	Ungerna	k1gFnSc1	Ungerna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
léta	léto	k1gNnSc2	léto
1921	[number]	k4	1921
byli	být	k5eAaImAgMnP	být
bělogvardějci	bělogvardějec	k1gMnPc1	bělogvardějec
poraženi	porazit	k5eAaPmNgMnP	porazit
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
jejími	její	k3xOp3gMnPc7	její
mongolskými	mongolský	k2eAgMnPc7d1	mongolský
spojenci	spojenec	k1gMnPc7	spojenec
vedenými	vedený	k2eAgFnPc7d1	vedená
Süchbátarem	Süchbátar	k1gMnSc7	Süchbátar
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislé	závislý	k2eNgNnSc1d1	nezávislé
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
bylo	být	k5eAaImAgNnS	být
znovuvyhlášeno	znovuvyhlášet	k5eAaImNgNnS	znovuvyhlášet
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
byla	být	k5eAaImAgFnS	být
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
Mongolská	mongolský	k2eAgFnSc1d1	mongolská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zůstala	zůstat	k5eAaPmAgFnS	zůstat
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
SSSR	SSSR	kA	SSSR
po	po	k7c4	po
dalších	další	k2eAgNnPc2d1	další
sedmdesát	sedmdesát	k4xCc4	sedmdesát
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
se	se	k3xPyFc4	se
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
státu	stát	k1gInSc2	stát
postavil	postavit	k5eAaPmAgMnS	postavit
Chorlogín	Chorlogín	k1gMnSc1	Chorlogín
Čojbalsan	Čojbalsan	k1gMnSc1	Čojbalsan
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
přetvářet	přetvářet	k5eAaImF	přetvářet
Mongolsko	Mongolsko	k1gNnSc4	Mongolsko
podle	podle	k7c2	podle
sovětského	sovětský	k2eAgInSc2d1	sovětský
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
násilné	násilný	k2eAgFnSc3d1	násilná
kolektivizaci	kolektivizace	k1gFnSc3	kolektivizace
<g/>
,	,	kIx,	,
zavírání	zavírání	k1gNnSc3	zavírání
a	a	k8xC	a
bourání	bourání	k1gNnSc3	bourání
buddhistických	buddhistický	k2eAgInPc2d1	buddhistický
chrámů	chrám	k1gInPc2	chrám
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
především	především	k9	především
mnichů	mnich	k1gMnPc2	mnich
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
obětí	oběť	k1gFnSc7	oběť
hledání	hledání	k1gNnSc2	hledání
domnělých	domnělý	k2eAgMnPc2d1	domnělý
nepřátel	nepřítel	k1gMnPc2	nepřítel
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
stalinistických	stalinistický	k2eAgFnPc2d1	stalinistická
čistek	čistka	k1gFnPc2	čistka
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
obsadila	obsadit	k5eAaPmAgFnS	obsadit
armáda	armáda	k1gFnSc1	armáda
Japonského	japonský	k2eAgNnSc2d1	Japonské
císařství	císařství	k1gNnSc2	císařství
sousední	sousední	k2eAgNnSc4d1	sousední
Mandžusko	Mandžusko	k1gNnSc4	Mandžusko
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
loutkový	loutkový	k2eAgInSc1d1	loutkový
stát	stát	k5eAaPmF	stát
Mandžukuo	Mandžukuo	k6eAd1	Mandžukuo
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
hrozba	hrozba	k1gFnSc1	hrozba
dolehla	dolehnout	k5eAaPmAgFnS	dolehnout
až	až	k9	až
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
krátkého	krátký	k2eAgInSc2d1	krátký
sovětsko-japonského	sovětskoaponský	k2eAgInSc2d1	sovětsko-japonský
pohraničního	pohraniční	k2eAgInSc2d1	pohraniční
konfliktu	konflikt	k1gInSc2	konflikt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
byl	být	k5eAaImAgInS	být
japonský	japonský	k2eAgInSc1d1	japonský
útok	útok	k1gInSc1	útok
na	na	k7c4	na
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
společnými	společný	k2eAgFnPc7d1	společná
sovětsko-mongolskými	sovětskoongolský	k2eAgFnPc7d1	sovětsko-mongolský
silami	síla	k1gFnPc7	síla
odražen	odrazit	k5eAaPmNgInS	odrazit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
vojska	vojsko	k1gNnPc1	vojsko
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
podílela	podílet	k5eAaImAgNnP	podílet
na	na	k7c6	na
sovětském	sovětský	k2eAgInSc6d1	sovětský
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
Mandžusko	Mandžusko	k1gNnSc4	Mandžusko
<g/>
.	.	kIx.	.
</s>
<s>
Přímá	přímý	k2eAgFnSc1d1	přímá
hrozba	hrozba	k1gFnSc1	hrozba
ztráty	ztráta	k1gFnSc2	ztráta
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
územím	území	k1gNnSc7	území
Vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
vedla	vést	k5eAaImAgFnS	vést
Čínu	Čína	k1gFnSc4	Čína
k	k	k7c3	k
uznání	uznání	k1gNnSc3	uznání
Vnějšího	vnější	k2eAgNnSc2d1	vnější
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
jako	jako	k8xS	jako
nezávislého	závislý	k2eNgInSc2d1	nezávislý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
uznala	uznat	k5eAaPmAgFnS	uznat
nezávislost	nezávislost	k1gFnSc4	nezávislost
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
po	po	k7c6	po
referendu	referendum	k1gNnSc6	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
referendum	referendum	k1gNnSc1	referendum
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Mongolové	Mongol	k1gMnPc1	Mongol
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
jednoznačně	jednoznačně	k6eAd1	jednoznačně
vyslovili	vyslovit	k5eAaPmAgMnP	vyslovit
pro	pro	k7c4	pro
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
<g/>
)	)	kIx)	)
nezávislost	nezávislost	k1gFnSc4	nezávislost
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
de	de	k?	de
iure	iure	k1gFnSc1	iure
doposud	doposud	k6eAd1	doposud
neuznala	uznat	k5eNaPmAgFnS	uznat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
se	se	k3xPyFc4	se
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
dostal	dostat	k5eAaPmAgMnS	dostat
Jumdžágín	Jumdžágín	k1gMnSc1	Jumdžágín
Cedenbal	Cedenbal	k1gInSc4	Cedenbal
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
kult	kult	k1gInSc1	kult
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
trval	trvat	k5eAaImAgInS	trvat
až	až	k6eAd1	až
do	do	k7c2	do
jeho	on	k3xPp3gNnSc2	on
odvolání	odvolání	k1gNnSc2	odvolání
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
úzkou	úzký	k2eAgFnSc4d1	úzká
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
po	po	k7c6	po
výrazném	výrazný	k2eAgNnSc6d1	výrazné
ochlazením	ochlazení	k1gNnSc7	ochlazení
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
byl	být	k5eAaImAgInS	být
Cedenbal	Cedenbal	k1gInSc1	Cedenbal
nuceně	nuceně	k6eAd1	nuceně
poslán	poslat	k5eAaPmNgInS	poslat
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
špatného	špatný	k2eAgInSc2d1	špatný
duševního	duševní	k2eAgInSc2d1	duševní
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
byl	být	k5eAaImAgMnS	být
dosazen	dosazen	k2eAgMnSc1d1	dosazen
Jambyn	Jambyn	k1gMnSc1	Jambyn
Batmönkh	Batmönkh	k1gMnSc1	Batmönkh
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
marně	marně	k6eAd1	marně
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
o	o	k7c4	o
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
reformy	reforma	k1gFnPc4	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Zavedení	zavedení	k1gNnSc1	zavedení
programů	program	k1gInPc2	program
perestrojky	perestrojka	k1gFnSc2	perestrojka
a	a	k8xC	a
glasnosti	glasnost	k1gFnSc2	glasnost
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
Michailem	Michail	k1gMnSc7	Michail
Gorbačovem	Gorbačov	k1gInSc7	Gorbačov
vedlo	vést	k5eAaImAgNnS	vést
v	v	k7c6	v
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
k	k	k7c3	k
poklidné	poklidný	k2eAgFnSc3d1	poklidná
Demokratické	demokratický	k2eAgFnSc3d1	demokratická
revoluci	revoluce	k1gFnSc3	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Revoluce	revoluce	k1gFnSc1	revoluce
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
svým	svůj	k3xOyFgInSc7	svůj
klidným	klidný	k2eAgInSc7d1	klidný
průběhem	průběh	k1gInSc7	průběh
přirovnat	přirovnat	k5eAaPmF	přirovnat
k	k	k7c3	k
Sametové	sametový	k2eAgFnSc3d1	sametová
revoluci	revoluce	k1gFnSc3	revoluce
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
počet	počet	k1gInSc1	počet
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
požadovaly	požadovat	k5eAaImAgFnP	požadovat
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
perestrojky	perestrojka	k1gFnSc2	perestrojka
a	a	k8xC	a
glasnosti	glasnost	k1gFnSc2	glasnost
reformy	reforma	k1gFnSc2	reforma
jak	jak	k8xS	jak
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
politické	politický	k2eAgFnPc1d1	politická
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
deset	deset	k4xCc1	deset
disidentů	disident	k1gMnPc2	disident
protestní	protestní	k2eAgFnSc4d1	protestní
hladovku	hladovka	k1gFnSc4	hladovka
<g/>
.	.	kIx.	.
</s>
<s>
Tisíce	tisíc	k4xCgInPc1	tisíc
jejich	jejich	k3xOp3gInPc1	jejich
příznivců	příznivec	k1gMnPc2	příznivec
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
přidalo	přidat	k5eAaPmAgNnS	přidat
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tíhou	tíha	k1gFnSc7	tíha
neudržitelnosti	neudržitelnost	k1gFnSc2	neudržitelnost
situace	situace	k1gFnSc2	situace
komunistická	komunistický	k2eAgFnSc1d1	komunistická
vláda	vláda	k1gFnSc1	vláda
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
rezignovala	rezignovat	k5eAaBmAgFnS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
demokratické	demokratický	k2eAgFnPc4d1	demokratická
volby	volba	k1gFnPc4	volba
na	na	k7c4	na
červenec	červenec	k1gInSc4	červenec
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Opoziční	opoziční	k2eAgFnPc1d1	opoziční
síly	síla	k1gFnPc1	síla
ale	ale	k9	ale
nedokázaly	dokázat	k5eNaPmAgFnP	dokázat
shromáždit	shromáždit	k5eAaPmF	shromáždit
dostatek	dostatek	k1gInSc4	dostatek
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ovládly	ovládnout	k5eAaPmAgFnP	ovládnout
parlament	parlament	k1gInSc4	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vzešla	vzejít	k5eAaPmAgFnS	vzejít
z	z	k7c2	z
voleb	volba	k1gFnPc2	volba
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k6eAd1	tak
ale	ale	k8xC	ale
začala	začít	k5eAaPmAgFnS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
opozicí	opozice	k1gFnSc7	opozice
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
zavádět	zavádět	k5eAaImF	zavádět
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
reformy	reforma	k1gFnPc4	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
volebním	volební	k2eAgInSc7d1	volební
úspěchem	úspěch	k1gInSc7	úspěch
opozice	opozice	k1gFnSc2	opozice
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgFnP	být
prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
opoziční	opoziční	k2eAgMnSc1d1	opoziční
kandidát	kandidát	k1gMnSc1	kandidát
Punsalmágín	Punsalmágín	k1gMnSc1	Punsalmágín
Očirbat	Očirbat	k1gMnSc1	Očirbat
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
lidem	lid	k1gInSc7	lid
a	a	k8xC	a
následně	následně	k6eAd1	následně
parlament	parlament	k1gInSc1	parlament
zvolí	zvolit	k5eAaPmIp3nS	zvolit
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
dominují	dominovat	k5eAaImIp3nP	dominovat
dvě	dva	k4xCgFnPc1	dva
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
komunistická	komunistický	k2eAgFnSc1d1	komunistická
Mongolská	mongolský	k2eAgFnSc1d1	mongolská
lidová	lidový	k2eAgFnSc1d1	lidová
revoluční	revoluční	k2eAgFnSc1d1	revoluční
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
MLRS	MLRS	kA	MLRS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
přejmenovaná	přejmenovaný	k2eAgFnSc1d1	přejmenovaná
na	na	k7c4	na
Mongolskou	mongolský	k2eAgFnSc4d1	mongolská
lidovou	lidový	k2eAgFnSc4d1	lidová
stranu	strana	k1gFnSc4	strana
(	(	kIx(	(
<g/>
MLS	mls	k1gInSc4	mls
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
výraznější	výrazný	k2eAgInSc1d2	výraznější
příklon	příklon	k1gInSc1	příklon
spíše	spíše	k9	spíše
k	k	k7c3	k
sociální	sociální	k2eAgFnSc3d1	sociální
demokracii	demokracie	k1gFnSc3	demokracie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
střídá	střídat	k5eAaImIp3nS	střídat
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
s	s	k7c7	s
Demokratickou	demokratický	k2eAgFnSc7d1	demokratická
stranou	strana	k1gFnSc7	strana
(	(	kIx(	(
<g/>
DS	DS	kA	DS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
DS	DS	kA	DS
poprvé	poprvé	k6eAd1	poprvé
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
obhájila	obhájit	k5eAaPmAgFnS	obhájit
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
mandát	mandát	k1gInSc4	mandát
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
síly	síla	k1gFnPc1	síla
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
téměř	téměř	k6eAd1	téměř
vyrovnaly	vyrovnat	k5eAaBmAgInP	vyrovnat
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
skončily	skončit	k5eAaPmAgInP	skončit
vítězstvím	vítězství	k1gNnSc7	vítězství
pro	pro	k7c4	pro
MLRS	MLRS	kA	MLRS
poměrem	poměr	k1gInSc7	poměr
48,23	[number]	k4	48,23
%	%	kIx~	%
ku	k	k7c3	k
44,27	[number]	k4	44,27
%	%	kIx~	%
DS	DS	kA	DS
<g/>
.	.	kIx.	.
</s>
<s>
MLRS	MLRS	kA	MLRS
však	však	k9	však
získala	získat	k5eAaPmAgFnS	získat
pouze	pouze	k6eAd1	pouze
36	[number]	k4	36
ze	z	k7c2	z
74	[number]	k4	74
obsazených	obsazený	k2eAgInPc2d1	obsazený
mandátů	mandát	k1gInPc2	mandát
a	a	k8xC	a
tak	tak	k6eAd1	tak
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
velkou	velký	k2eAgFnSc4d1	velká
koalici	koalice	k1gFnSc4	koalice
právě	právě	k6eAd1	právě
s	s	k7c7	s
DS	DS	kA	DS
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
získala	získat	k5eAaPmAgFnS	získat
MLRS	MLRS	kA	MLRS
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
52	[number]	k4	52
%	%	kIx~	%
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
voleb	volba	k1gFnPc2	volba
bylo	být	k5eAaImAgNnS	být
ústředí	ústředí	k1gNnSc1	ústředí
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
zcela	zcela	k6eAd1	zcela
vypáleno	vypálit	k5eAaPmNgNnS	vypálit
protestujícími	protestující	k2eAgInPc7d1	protestující
<g/>
.	.	kIx.	.
</s>
<s>
DS	DS	kA	DS
získala	získat	k5eAaPmAgFnS	získat
necelých	celý	k2eNgNnPc6d1	necelé
40,5	[number]	k4	40,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
podruhé	podruhé	k6eAd1	podruhé
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
DS	DS	kA	DS
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
35,32	[number]	k4	35,32
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
MLS	mls	k1gInSc1	mls
skončila	skončit	k5eAaPmAgFnS	skončit
druhá	druhý	k4xOgFnSc1	druhý
s	s	k7c7	s
31,31	[number]	k4	31,31
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
s	s	k7c7	s
22,31	[number]	k4	22,31
%	%	kIx~	%
Mongolská	mongolský	k2eAgFnSc1d1	mongolská
lidová	lidový	k2eAgFnSc1d1	lidová
revoluční	revoluční	k2eAgFnSc1d1	revoluční
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
MLRS	MLRS	kA	MLRS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
odštěpením	odštěpení	k1gNnSc7	odštěpení
od	od	k7c2	od
MLS	mls	k1gInSc4	mls
a	a	k8xC	a
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
návrat	návrat	k1gInSc4	návrat
od	od	k7c2	od
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
ke	k	k7c3	k
komunismu	komunismus	k1gInSc3	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Premiérem	premiér	k1gMnSc7	premiér
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Norovyn	Norovyn	k1gMnSc1	Norovyn
Altanchujag	Altanchujag	k1gMnSc1	Altanchujag
<g/>
,	,	kIx,	,
zástupce	zástupce	k1gMnSc1	zástupce
DS	DS	kA	DS
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
překvapivě	překvapivě	k6eAd1	překvapivě
utvořila	utvořit	k5eAaPmAgFnS	utvořit
koalici	koalice	k1gFnSc4	koalice
s	s	k7c7	s
radikálnější	radikální	k2eAgFnSc7d2	radikálnější
MLRS	MLRS	kA	MLRS
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
přestalo	přestat	k5eAaPmAgNnS	přestat
být	být	k5eAaImF	být
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
největším	veliký	k2eAgMnSc7d3	veliký
vnitrozemským	vnitrozemský	k2eAgInSc7d1	vnitrozemský
státem	stát	k1gInSc7	stát
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
druhou	druhý	k4xOgFnSc4	druhý
pozici	pozice	k1gFnSc4	pozice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
státy	stát	k1gInPc4	stát
nejvíce	hodně	k6eAd3	hodně
vzdálené	vzdálený	k2eAgInPc4d1	vzdálený
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
700	[number]	k4	700
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západovýchodním	západovýchodní	k2eAgInSc6d1	západovýchodní
směru	směr	k1gInSc6	směr
měří	měřit	k5eAaImIp3nS	měřit
2	[number]	k4	2
392	[number]	k4	392
km	km	kA	km
<g/>
,	,	kIx,	,
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
1	[number]	k4	1
259	[number]	k4	259
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
hranic	hranice	k1gFnPc2	hranice
činí	činit	k5eAaImIp3nS	činit
8	[number]	k4	8
161	[number]	k4	161
km	km	kA	km
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
3	[number]	k4	3
485	[number]	k4	485
km	km	kA	km
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
4	[number]	k4	4
676	[number]	k4	676
km	km	kA	km
představuje	představovat	k5eAaImIp3nS	představovat
pomezí	pomezí	k1gNnSc4	pomezí
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
země	zem	k1gFnSc2	zem
má	mít	k5eAaImIp3nS	mít
charakter	charakter	k1gInSc4	charakter
náhorní	náhorní	k2eAgFnSc2d1	náhorní
plošiny	plošina	k1gFnSc2	plošina
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
přesahující	přesahující	k2eAgFnSc4d1	přesahující
1	[number]	k4	1
500	[number]	k4	500
m	m	kA	m
<g/>
;	;	kIx,	;
pouze	pouze	k6eAd1	pouze
19	[number]	k4	19
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
1	[number]	k4	1
000	[number]	k4	000
m.	m.	k?	m.
Nejnižším	nízký	k2eAgInSc7d3	nejnižší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
hladina	hladina	k1gFnSc1	hladina
jezera	jezero	k1gNnSc2	jezero
Choch	Chocha	k1gFnPc2	Chocha
núr	núr	k?	núr
(	(	kIx(	(
<g/>
532	[number]	k4	532
m	m	kA	m
<g/>
)	)	kIx)	)
v	v	k7c6	v
nejseverovýchodnějším	severovýchodný	k2eAgInSc6d3	severovýchodný
cípu	cíp	k1gInSc6	cíp
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
jihovýchod	jihovýchod	k1gInSc1	jihovýchod
a	a	k8xC	a
jih	jih	k1gInSc1	jih
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
mírně	mírně	k6eAd1	mírně
zvlněný	zvlněný	k2eAgInSc1d1	zvlněný
a	a	k8xC	a
zbrázděný	zbrázděný	k2eAgInSc1d1	zbrázděný
stržemi	strž	k1gFnPc7	strž
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
zejména	zejména	k9	zejména
na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
zvedají	zvedat	k5eAaImIp3nP	zvedat
vysoká	vysoká	k1gFnSc1	vysoká
pohoří	pohořet	k5eAaPmIp3nP	pohořet
Východní	východní	k2eAgInSc4d1	východní
Sajan	Sajan	k1gInSc4	Sajan
<g/>
,	,	kIx,	,
Changaj	Changaj	k1gFnSc4	Changaj
a	a	k8xC	a
Mongolský	mongolský	k2eAgInSc4d1	mongolský
Altaj	Altaj	k1gInSc4	Altaj
(	(	kIx(	(
<g/>
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
země	zem	k1gFnSc2	zem
Chüjtun	Chüjtun	k1gInSc1	Chüjtun
vysokou	vysoká	k1gFnSc4	vysoká
4	[number]	k4	4
374	[number]	k4	374
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozčleněná	rozčleněný	k2eAgFnSc1d1	rozčleněná
hlubokými	hluboký	k2eAgNnPc7d1	hluboké
údolími	údolí	k1gNnPc7	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
pak	pak	k6eAd1	pak
daleko	daleko	k6eAd1	daleko
k	k	k7c3	k
východu	východ	k1gInSc3	východ
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
o	o	k7c4	o
něco	něco	k3yInSc4	něco
nižší	nízký	k2eAgNnSc4d2	nižší
pásmo	pásmo	k1gNnSc4	pásmo
Gobijského	gobijský	k2eAgInSc2d1	gobijský
Altaje	Altaj	k1gInSc2	Altaj
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
jih	jih	k1gInSc1	jih
a	a	k8xC	a
jihovýchod	jihovýchod	k1gInSc1	jihovýchod
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
nejsušší	suchý	k2eAgFnSc1d3	nejsušší
a	a	k8xC	a
nejnehostinější	hostiný	k2eNgFnSc1d3	hostiný
část	část	k1gFnSc1	část
země	zem	k1gFnSc2	zem
-	-	kIx~	-
poušť	poušť	k1gFnSc1	poušť
Gobi	Gobi	k1gFnSc1	Gobi
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
horami	hora	k1gFnPc7	hora
na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
velkých	velký	k2eAgNnPc2d1	velké
bezodtokých	bezodtoký	k2eAgNnPc2d1	bezodtoké
jezer	jezero	k1gNnPc2	jezero
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
bývá	bývat	k5eAaImIp3nS	bývat
proto	proto	k8xC	proto
nazývána	nazýván	k2eAgFnSc1d1	nazývána
Jezerní	jezerní	k2eAgFnSc1d1	jezerní
pánev	pánev	k1gFnSc1	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
prostírá	prostírat	k5eAaImIp3nS	prostírat
největší	veliký	k2eAgNnSc1d3	veliký
mongolské	mongolský	k2eAgNnSc1d1	mongolské
jezero	jezero	k1gNnSc1	jezero
Uvs	Uvs	k1gFnSc2	Uvs
núr	núr	k?	núr
(	(	kIx(	(
<g/>
3	[number]	k4	3
350	[number]	k4	350
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Char	Char	k1gMnSc1	Char
Us	Us	k1gFnSc2	Us
núr	núr	k?	núr
<g/>
,	,	kIx,	,
Chjargas	Chjargas	k1gMnSc1	Chjargas
núr	núr	k?	núr
<g/>
,	,	kIx,	,
Ačit	Ačit	k1gInSc1	Ačit
núr	núr	k?	núr
aj.	aj.	kA	aj.
Další	další	k2eAgFnSc1d1	další
velké	velký	k2eAgNnSc4d1	velké
jezero	jezero	k1gNnSc4	jezero
<g/>
,	,	kIx,	,
Chövsgöl	Chövsgöl	k1gInSc1	Chövsgöl
núr	núr	k?	núr
(	(	kIx(	(
<g/>
2	[number]	k4	2
620	[number]	k4	620
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
jihem	jih	k1gInSc7	jih
<g/>
,	,	kIx,	,
postrádajícím	postrádající	k2eAgInSc7d1	postrádající
stálé	stálý	k2eAgNnSc1d1	stálé
vodní	vodní	k2eAgInPc1d1	vodní
toky	tok	k1gInPc1	tok
<g/>
,	,	kIx,	,
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
existuje	existovat	k5eAaImIp3nS	existovat
poměrně	poměrně	k6eAd1	poměrně
hustá	hustý	k2eAgFnSc1d1	hustá
říční	říční	k2eAgFnSc1d1	říční
síť	síť	k1gFnSc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgFnPc7d3	veliký
řekami	řeka	k1gFnPc7	řeka
jsou	být	k5eAaImIp3nP	být
Orchon	Orchon	k1gNnSc1	Orchon
a	a	k8xC	a
Selenga	Selenga	k1gFnSc1	Selenga
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
odvádějí	odvádět	k5eAaImIp3nP	odvádět
vodu	voda	k1gFnSc4	voda
do	do	k7c2	do
ruského	ruský	k2eAgNnSc2d1	ruské
jezera	jezero	k1gNnSc2	jezero
Bajkal	Bajkal	k1gInSc1	Bajkal
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
tedy	tedy	k9	tedy
k	k	k7c3	k
úmoří	úmoří	k1gNnSc3	úmoří
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Selenga	Selenga	k1gFnSc1	Selenga
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
při	při	k7c6	při
ruských	ruský	k2eAgFnPc6d1	ruská
hranicích	hranice	k1gFnPc6	hranice
dokonce	dokonce	k9	dokonce
využívána	využívat	k5eAaImNgFnS	využívat
pro	pro	k7c4	pro
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Východ	východ	k1gInSc1	východ
země	zem	k1gFnSc2	zem
odvodňuje	odvodňovat	k5eAaImIp3nS	odvodňovat
do	do	k7c2	do
čínského	čínský	k2eAgNnSc2d1	čínské
jezera	jezero	k1gNnSc2	jezero
Chu-lun	Chuuna	k1gFnPc2	Chu-luna
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
přes	přes	k7c4	přes
Amur	Amur	k1gInSc4	Amur
do	do	k7c2	do
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
řeka	řek	k1gMnSc2	řek
Cherlen	Cherlen	k2eAgInSc1d1	Cherlen
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
je	být	k5eAaImIp3nS	být
Mongolsko	Mongolsko	k1gNnSc4	Mongolsko
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Mongolský	mongolský	k2eAgInSc1d1	mongolský
parlament	parlament	k1gInSc1	parlament
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Velký	velký	k2eAgInSc1d1	velký
lidový	lidový	k2eAgInSc1d1	lidový
chural	chural	k1gInSc1	chural
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
nejsilnější	silný	k2eAgFnPc1d3	nejsilnější
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
nesou	nést	k5eAaImIp3nP	nést
názvy	název	k1gInPc4	název
Mongolská	mongolský	k2eAgFnSc1d1	mongolská
lidově	lidově	k6eAd1	lidově
revoluční	revoluční	k2eAgFnSc1d1	revoluční
strana	strana	k1gFnSc1	strana
a	a	k8xC	a
Mongolská	mongolský	k2eAgFnSc1d1	mongolská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
oficiálního	oficiální	k2eAgInSc2d1	oficiální
názvu	název	k1gInSc2	název
státu	stát	k1gInSc2	stát
z	z	k7c2	z
"	"	kIx"	"
<g/>
Mongolská	mongolský	k2eAgFnSc1d1	mongolská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
"	"	kIx"	"
na	na	k7c4	na
"	"	kIx"	"
<g/>
Mongolská	mongolský	k2eAgFnSc1d1	mongolská
republika	republika	k1gFnSc1	republika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1992	[number]	k4	1992
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
nová	nový	k2eAgFnSc1d1	nová
ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
vzdalo	vzdát	k5eAaPmAgNnS	vzdát
socialismu	socialismus	k1gInSc2	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
obdržela	obdržet	k5eAaPmAgFnS	obdržet
země	zem	k1gFnPc4	zem
nový	nový	k2eAgInSc4d1	nový
název	název	k1gInSc4	název
Stát	stát	k1gInSc1	stát
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
ústava	ústava	k1gFnSc1	ústava
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
základ	základ	k1gInSc4	základ
dalšího	další	k2eAgInSc2d1	další
demokratického	demokratický	k2eAgInSc2d1	demokratický
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
však	však	k9	však
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
mezinárodně-politicky	mezinárodněoliticky	k6eAd1	mezinárodně-politicky
neutrální	neutrální	k2eAgMnSc1d1	neutrální
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
volený	volený	k2eAgMnSc1d1	volený
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
4	[number]	k4	4
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
Natsagyn	Natsagyn	k1gMnSc1	Natsagyn
Bagabandi	Bagaband	k1gMnPc1	Bagaband
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
prezidentem	prezident	k1gMnSc7	prezident
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Cachjagín	Cachjagín	k1gMnSc1	Cachjagín
Elbegdordž	Elbegdordž	k1gFnSc4	Elbegdordž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
velká	velký	k2eAgFnSc1d1	velká
koalice	koalice	k1gFnSc1	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgInSc1d1	vládní
akční	akční	k2eAgInSc1d1	akční
program	program	k1gInSc1	program
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
Velkým	velký	k2eAgInSc7d1	velký
Churalem	chural	k1gInSc7	chural
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
zdůrazňoval	zdůrazňovat	k5eAaImAgInS	zdůrazňovat
potřebu	potřeba	k1gFnSc4	potřeba
konsolidace	konsolidace	k1gFnSc2	konsolidace
demokratického	demokratický	k2eAgNnSc2d1	demokratické
zřízení	zřízení	k1gNnSc2	zřízení
státu	stát	k1gInSc2	stát
a	a	k8xC	a
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
také	také	k9	také
usnesení	usnesení	k1gNnSc4	usnesení
o	o	k7c6	o
potřebě	potřeba	k1gFnSc6	potřeba
rychlého	rychlý	k2eAgInSc2d1	rychlý
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prioritních	prioritní	k2eAgFnPc2d1	prioritní
zemí	zem	k1gFnPc2	zem
české	český	k2eAgFnSc2d1	Česká
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
rozvojové	rozvojový	k2eAgFnSc2d1	rozvojová
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
projekty	projekt	k1gInPc1	projekt
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
na	na	k7c4	na
pět	pět	k4xCc4	pět
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
rozvoj	rozvoj	k1gInSc1	rozvoj
včetně	včetně	k7c2	včetně
předávání	předávání	k1gNnSc2	předávání
zkušeností	zkušenost	k1gFnPc2	zkušenost
z	z	k7c2	z
transformace	transformace	k1gFnSc2	transformace
na	na	k7c4	na
tržní	tržní	k2eAgNnSc4d1	tržní
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
,	,	kIx,	,
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnPc4d1	sociální
otázky	otázka	k1gFnPc4	otázka
a	a	k8xC	a
snižování	snižování	k1gNnSc4	snižování
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
Jistou	jistý	k2eAgFnSc4d1	jistá
důležitost	důležitost	k1gFnSc4	důležitost
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
prevence	prevence	k1gFnSc1	prevence
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
působí	působit	k5eAaImIp3nP	působit
i	i	k9	i
české	český	k2eAgFnPc1d1	Česká
nevládní	vládní	k2eNgFnPc1d1	nevládní
organizace	organizace	k1gFnPc1	organizace
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
<g/>
,	,	kIx,	,
Charita	charita	k1gFnSc1	charita
a	a	k8xC	a
ADRA	ADRA	kA	ADRA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
žije	žít	k5eAaImIp3nS	žít
zhruba	zhruba	k6eAd1	zhruba
5000	[number]	k4	5000
Mongolů	mongol	k1gInPc2	mongol
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Mongolské	mongolský	k2eAgFnSc2d1	mongolská
ajmagy	ajmaga	k1gFnSc2	ajmaga
<g/>
.	.	kIx.	.
</s>
<s>
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
21	[number]	k4	21
ajmagů	ajmag	k1gInPc2	ajmag
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
329	[number]	k4	329
somonů	somon	k1gInPc2	somon
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
spravováno	spravovat	k5eAaImNgNnS	spravovat
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
žádného	žádný	k3yNgInSc2	žádný
ajmagu	ajmag	k1gInSc2	ajmag
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
rychle	rychle	k6eAd1	rychle
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
hrubého	hrubý	k2eAgInSc2d1	hrubý
domácího	domácí	k2eAgInSc2d1	domácí
produktu	produkt	k1gInSc2	produkt
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
ještě	ještě	k6eAd1	ještě
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
tradičním	tradiční	k2eAgInSc7d1	tradiční
velkým	velký	k2eAgInSc7d1	velký
významem	význam	k1gInSc7	význam
kočovného	kočovný	k2eAgNnSc2d1	kočovné
pastevectví	pastevectví	k1gNnSc2	pastevectví
pro	pro	k7c4	pro
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
včetně	včetně	k7c2	včetně
pastevectví	pastevectví	k1gNnSc2	pastevectví
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
podíl	podíl	k1gInSc1	podíl
15,8	[number]	k4	15,8
%	%	kIx~	%
na	na	k7c6	na
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
průmysl	průmysl	k1gInSc1	průmysl
však	však	k9	však
již	již	k6eAd1	již
vytvářel	vytvářet	k5eAaImAgInS	vytvářet
32,6	[number]	k4	32,6
%	%	kIx~	%
a	a	k8xC	a
služby	služba	k1gFnPc4	služba
včetně	včetně	k7c2	včetně
státní	státní	k2eAgFnSc2d1	státní
administrativy	administrativa	k1gFnSc2	administrativa
<g/>
,	,	kIx,	,
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
51,6	[number]	k4	51,6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
v	v	k7c6	v
paritě	parita	k1gFnSc6	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
odhadován	odhadovat	k5eAaImNgMnS	odhadovat
na	na	k7c4	na
13,4	[number]	k4	13,4
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
4800	[number]	k4	4800
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
obyvatele	obyvatel	k1gMnSc4	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
doposud	doposud	k6eAd1	doposud
relativně	relativně	k6eAd1	relativně
chudá	chudý	k2eAgFnSc1d1	chudá
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kolem	kolem	k7c2	kolem
39,2	[number]	k4	39,2
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
kolem	kolem	k7c2	kolem
9,9	[number]	k4	9,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc7d1	důležitá
složkou	složka	k1gFnSc7	složka
hospodářství	hospodářství	k1gNnSc2	hospodářství
je	být	k5eAaImIp3nS	být
těžba	těžba	k1gFnSc1	těžba
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
významná	významný	k2eAgNnPc1d1	významné
ložiska	ložisko	k1gNnPc1	ložisko
hnědého	hnědý	k2eAgNnSc2d1	hnědé
a	a	k8xC	a
černého	černý	k2eAgNnSc2d1	černé
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
<g/>
,	,	kIx,	,
wolframu	wolfram	k1gInSc2	wolfram
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
také	také	k9	také
zajímají	zajímat	k5eAaImIp3nP	zajímat
o	o	k7c4	o
potenciál	potenciál	k1gInSc4	potenciál
těžby	těžba	k1gFnSc2	těžba
kovů	kov	k1gInPc2	kov
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
v	v	k7c6	v
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
rovněž	rovněž	k9	rovněž
naleziště	naleziště	k1gNnSc2	naleziště
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Mongolskou	mongolský	k2eAgFnSc7d1	mongolská
měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
tugrik	tugrik	k1gInSc1	tugrik
(	(	kIx(	(
<g/>
MNT	MNT	kA	MNT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
peněžní	peněžní	k2eAgFnSc1d1	peněžní
jednotka	jednotka	k1gFnSc1	jednotka
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
100	[number]	k4	100
mungů	mungo	k1gMnPc2	mungo
<g/>
.	.	kIx.	.
</s>
<s>
Směnný	směnný	k2eAgInSc1d1	směnný
kurs	kurs	k1gInSc1	kurs
mongolské	mongolský	k2eAgFnSc2d1	mongolská
měny	měna	k1gFnSc2	měna
se	se	k3xPyFc4	se
začátkem	začátek	k1gInSc7	začátek
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
okolo	okolo	k7c2	okolo
100	[number]	k4	100
tugriků	tugrik	k1gInPc2	tugrik
=	=	kIx~	=
1,30	[number]	k4	1,30
až	až	k9	až
1,50	[number]	k4	1,50
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
mongolského	mongolský	k2eAgNnSc2d1	mongolské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
donedávna	donedávna	k6eAd1	donedávna
žila	žít	k5eAaImAgFnS	žít
kočovným	kočovný	k2eAgInSc7d1	kočovný
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Pokračující	pokračující	k2eAgFnSc1d1	pokračující
urbanizace	urbanizace	k1gFnSc1	urbanizace
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
soustřeďování	soustřeďování	k1gNnSc3	soustřeďování
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Ulánbátaru	Ulánbátar	k1gInSc2	Ulánbátar
a	a	k8xC	a
několika	několik	k4yIc7	několik
mnohem	mnohem	k6eAd1	mnohem
menších	malý	k2eAgNnPc6d2	menší
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Ulánbátar	Ulánbátar	k1gInSc1	Ulánbátar
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozvíjející	rozvíjející	k2eAgFnSc7d1	rozvíjející
se	se	k3xPyFc4	se
civilizační	civilizační	k2eAgFnSc7d1	civilizační
strukturou	struktura	k1gFnSc7	struktura
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
bývalé	bývalý	k2eAgMnPc4d1	bývalý
kočovné	kočovný	k2eAgMnPc4d1	kočovný
pastýře	pastýř	k1gMnPc4	pastýř
dobytka	dobytek	k1gInSc2	dobytek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
stáda	stádo	k1gNnPc1	stádo
uhynula	uhynout	k5eAaPmAgNnP	uhynout
v	v	k7c6	v
tvrdých	tvrdý	k2eAgFnPc6d1	tvrdá
zimách	zima	k1gFnPc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgMnSc2	ten
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Ulánbátaru	Ulánbátar	k1gInSc6	Ulánbátar
několik	několik	k4yIc4	několik
sídlišť	sídliště	k1gNnPc2	sídliště
sestávajících	sestávající	k2eAgInPc2d1	sestávající
z	z	k7c2	z
jurt	jurta	k1gFnPc2	jurta
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
náskokem	náskok	k1gInSc7	náskok
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Ulánbátar	Ulánbátar	k1gInSc1	Ulánbátar
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
1	[number]	k4	1
318	[number]	k4	318
000	[number]	k4	000
(	(	kIx(	(
<g/>
r.	r.	kA	r.
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
tedy	tedy	k8xC	tedy
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třetina	třetina	k1gFnSc1	třetina
mongolské	mongolský	k2eAgFnSc2d1	mongolská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
odstupem	odstup	k1gInSc7	odstup
následují	následovat	k5eAaImIp3nP	následovat
Erdenet	Erdenet	k1gInSc4	Erdenet
(	(	kIx(	(
<g/>
83	[number]	k4	83
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Darchan	Darchan	k1gMnSc1	Darchan
(	(	kIx(	(
<g/>
75	[number]	k4	75
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čojbalsan	Čojbalsan	k1gMnSc1	Čojbalsan
(	(	kIx(	(
<g/>
39	[number]	k4	39
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mörön	Mörön	k1gMnSc1	Mörön
(	(	kIx(	(
<g/>
36	[number]	k4	36
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
města	město	k1gNnPc1	město
mají	mít	k5eAaImIp3nP	mít
již	již	k6eAd1	již
méně	málo	k6eAd2	málo
než	než	k8xS	než
30	[number]	k4	30
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
země	zem	k1gFnSc2	zem
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
95	[number]	k4	95
%	%	kIx~	%
Mongolové	Mongol	k1gMnPc1	Mongol
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
menšinou	menšina	k1gFnSc7	menšina
v	v	k7c6	v
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
jsou	být	k5eAaImIp3nP	být
Kazaši	Kazach	k1gMnPc1	Kazach
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
podíl	podíl	k1gInSc1	podíl
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
4,5	[number]	k4	4,5
%	%	kIx~	%
všeho	všecek	k3xTgNnSc2	všecek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
zastoupeni	zastoupen	k2eAgMnPc1d1	zastoupen
Rusové	Rus	k1gMnPc1	Rus
<g/>
,	,	kIx,	,
Číňané	Číňan	k1gMnPc1	Číňan
<g/>
,	,	kIx,	,
Korejci	Korejec	k1gMnPc1	Korejec
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
jiných	jiný	k2eAgFnPc2d1	jiná
národností	národnost	k1gFnPc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Mongolové	Mongol	k1gMnPc1	Mongol
nejsou	být	k5eNaImIp3nP	být
etnicky	etnicky	k6eAd1	etnicky
zcela	zcela	k6eAd1	zcela
jednotný	jednotný	k2eAgInSc1d1	jednotný
národ	národ	k1gInSc1	národ
<g/>
,	,	kIx,	,
na	na	k7c4	na
území	území	k1gNnSc4	území
jejich	jejich	k3xOp3gInSc2	jejich
vlastního	vlastní	k2eAgInSc2d1	vlastní
státu	stát	k1gInSc2	stát
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
národnostní	národnostní	k2eAgFnPc1d1	národnostní
skupiny	skupina	k1gFnPc1	skupina
Chalchové	Chalchová	k1gFnSc2	Chalchová
(	(	kIx(	(
<g/>
86	[number]	k4	86
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ojrati	Ojrat	k1gMnPc1	Ojrat
a	a	k8xC	a
Burjati	Burjat	k1gMnPc1	Burjat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc7	jejich
náboženstvím	náboženství	k1gNnSc7	náboženství
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
buddhismus	buddhismus	k1gInSc4	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Kazaši	Kazach	k1gMnPc1	Kazach
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
islám	islám	k1gInSc4	islám
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
používáno	používán	k2eAgNnSc1d1	používáno
vlastní	vlastní	k2eAgNnSc1d1	vlastní
písmo	písmo	k1gNnSc1	písmo
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
však	však	k9	však
úředním	úřední	k2eAgNnSc7d1	úřední
písmem	písmo	k1gNnSc7	písmo
upravená	upravený	k2eAgFnSc1d1	upravená
cyrilice	cyrilika	k1gFnSc3	cyrilika
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
mongolština	mongolština	k1gFnSc1	mongolština
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
nejpoužívanějším	používaný	k2eAgInSc7d3	nejpoužívanější
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
domluvit	domluvit	k5eAaPmF	domluvit
i	i	k9	i
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českých	český	k2eAgInPc2d1	český
zdrojů	zdroj	k1gInPc2	zdroj
až	až	k9	až
1	[number]	k4	1
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
má	mít	k5eAaImIp3nS	mít
znalosti	znalost	k1gFnPc4	znalost
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
