<s>
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Stát	stát	k5eAaImF	stát
Mongolsko	Mongolsko	k1gNnSc4	Mongolsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
stát	stát	k1gInSc1	stát
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
hraničící	hraničící	k2eAgFnSc6d1	hraničící
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Čínskou	čínský	k2eAgFnSc7d1	čínská
lidovou	lidový	k2eAgFnSc7d1	lidová
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
