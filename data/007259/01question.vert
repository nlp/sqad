<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
sada	sada	k1gFnSc1
zubů	zub	k1gInPc2
u	u	k7c2
mnoha	mnoho	k4c2
mláďat	mládě	k1gNnPc2
savců	savec	k1gMnPc2
,	,	kIx,
která	který	k3yQgFnSc1
vypadne	vypadnout	k5eAaPmIp3nS
a	a	k8xC
je	být	k5eAaImIp3nS
nahrazena	nahradit	k5eAaPmNgFnS
dospělými	dospělý	k2eAgInPc7d1
zuby	zub	k1gInPc7
?	?	kIx.
</s>