<s>
Chrup	chrup	k1gInSc4	chrup
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
dentice	dentice	k1gFnPc1	dentice
<g/>
,	,	kIx,	,
je	on	k3xPp3gFnPc4	on
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
soubor	soubor	k1gInSc4	soubor
zubů	zub	k1gInPc2	zub
v	v	k7c6	v
ústní	ústní	k2eAgFnSc6d1	ústní
dutině	dutina	k1gFnSc6	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
typů	typ	k1gInPc2	typ
zubů	zub	k1gInPc2	zub
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
chrup	chrup	k1gInSc4	chrup
homodontní	homodontní	k2eAgInSc4d1	homodontní
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgInPc1	všechen
zuby	zub	k1gInPc1	zub
jsou	být	k5eAaImIp3nP	být
tvarově	tvarově	k6eAd1	tvarově
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
kytovců	kytovec	k1gMnPc2	kytovec
nebo	nebo	k8xC	nebo
u	u	k7c2	u
pásovce	pásovec	k1gMnSc2	pásovec
<g/>
)	)	kIx)	)
a	a	k8xC	a
heterodontní	heterodontní	k2eAgInSc4d1	heterodontní
chrup	chrup	k1gInSc4	chrup
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
rozlišen	rozlišit	k5eAaPmNgInS	rozlišit
až	až	k9	až
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
s	s	k7c7	s
maximálním	maximální	k2eAgNnSc7d1	maximální
a	a	k8xC	a
druhově	druhově	k6eAd1	druhově
specifickým	specifický	k2eAgInSc7d1	specifický
počtem	počet	k1gInSc7	počet
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
řezák	řezák	k1gInSc4	řezák
<g/>
,	,	kIx,	,
špičák	špičák	k1gInSc4	špičák
<g/>
,	,	kIx,	,
třenový	třenový	k2eAgInSc4d1	třenový
zub	zub	k1gInSc4	zub
(	(	kIx(	(
<g/>
premolár	premolár	k1gInSc4	premolár
<g/>
)	)	kIx)	)
a	a	k8xC	a
stolička	stolička	k1gFnSc1	stolička
(	(	kIx(	(
<g/>
molár	molár	k1gInSc1	molár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zubní	zubní	k2eAgInSc4d1	zubní
vzorec	vzorec	k1gInSc4	vzorec
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
zubů	zub	k1gInPc2	zub
každého	každý	k3xTgInSc2	každý
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
jako	jako	k9	jako
zubní	zubní	k2eAgInSc1d1	zubní
vzorec	vzorec	k1gInSc1	vzorec
<g/>
:	:	kIx,	:
Lidský	lidský	k2eAgInSc1d1	lidský
zubní	zubní	k2eAgInSc1d1	zubní
vzorec	vzorec	k1gInSc1	vzorec
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
2.1.2.3	[number]	k4	2.1.2.3
2.1.2.3	[number]	k4	2.1.2.3
Což	což	k3yRnSc4	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
polovině	polovina	k1gFnSc6	polovina
horní	horní	k2eAgFnSc2d1	horní
čelisti	čelist	k1gFnSc2	čelist
dva	dva	k4xCgInPc4	dva
řezáky	řezák	k1gInPc4	řezák
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
špičák	špičák	k1gInSc4	špičák
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
třenové	třenový	k2eAgInPc4d1	třenový
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
stoličky	stolička	k1gFnPc4	stolička
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
v	v	k7c6	v
čelisti	čelist	k1gFnSc6	čelist
dolní	dolní	k2eAgFnSc6d1	dolní
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zjištění	zjištění	k1gNnSc4	zjištění
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
zubů	zub	k1gInPc2	zub
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
vynásobit	vynásobit	k5eAaPmF	vynásobit
jejich	jejich	k3xOp3gInSc4	jejich
součet	součet	k1gInSc4	součet
dvěma	dva	k4xCgFnPc7	dva
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vzorec	vzorec	k1gInSc1	vzorec
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
poloviční	poloviční	k2eAgInSc4d1	poloviční
počet	počet	k1gInSc4	počet
zubů	zub	k1gInPc2	zub
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
i	i	k8xC	i
dolní	dolní	k2eAgFnSc6d1	dolní
čelisti	čelist	k1gFnSc6	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgInSc4d1	maximální
zubní	zubní	k2eAgInSc4d1	zubní
vzorec	vzorec	k1gInSc4	vzorec
heterodontních	heterodontní	k2eAgMnPc2d1	heterodontní
savců	savec	k1gMnPc2	savec
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
5.1.4.4	[number]	k4	5.1.4.4
4.1.4.4	[number]	k4	4.1.4.4
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
nejvíc	hodně	k6eAd3	hodně
blíží	blížit	k5eAaImIp3nS	blížit
vačice	vačice	k1gFnSc1	vačice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
třenové	třenový	k2eAgInPc1d1	třenový
zuby	zub	k1gInPc1	zub
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dočasný	dočasný	k2eAgInSc4d1	dočasný
chrup	chrup	k1gInSc4	chrup
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
savců	savec	k1gMnPc2	savec
má	mít	k5eAaImIp3nS	mít
mládě	mládě	k1gNnSc1	mládě
sadu	sad	k1gInSc2	sad
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vypadne	vypadnout	k5eAaPmIp3nS	vypadnout
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
dospělými	dospělý	k2eAgInPc7d1	dospělý
zuby	zub	k1gInPc7	zub
<g/>
.	.	kIx.	.
</s>
<s>
Nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
dočasné	dočasný	k2eAgInPc1d1	dočasný
zuby	zub	k1gInPc1	zub
<g/>
,	,	kIx,	,
dětské	dětský	k2eAgInPc1d1	dětský
zuby	zub	k1gInPc1	zub
nebo	nebo	k8xC	nebo
mléčné	mléčný	k2eAgInPc1d1	mléčný
zuby	zub	k1gInPc1	zub
<g/>
.	.	kIx.	.
</s>
<s>
Živočichové	živočich	k1gMnPc1	živočich
mající	mající	k2eAgMnPc1d1	mající
2	[number]	k4	2
sady	sada	k1gFnSc2	sada
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jedna	jeden	k4xCgFnSc1	jeden
následuje	následovat	k5eAaImIp3nS	následovat
druhou	druhý	k4xOgFnSc4	druhý
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
difyodonti	difyodont	k5eAaBmF	difyodont
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
difyodontní	difyodontní	k2eAgFnSc4d1	difyodontní
dentici	dentice	k1gFnSc4	dentice
<g/>
.	.	kIx.	.
</s>
<s>
Zubní	zubní	k2eAgInSc1d1	zubní
vzorec	vzorec	k1gInSc1	vzorec
pro	pro	k7c4	pro
mléčné	mléčný	k2eAgInPc4d1	mléčný
zuby	zub	k1gInPc4	zub
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
chyběním	chybění	k1gNnSc7	chybění
třenových	třenový	k2eAgInPc2d1	třenový
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Zubní	zubní	k2eAgInSc1d1	zubní
vzorec	vzorec	k1gInSc1	vzorec
mléčného	mléčný	k2eAgInSc2d1	mléčný
chrupu	chrup	k1gInSc2	chrup
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
2.1.0.2	[number]	k4	2.1.0.2
2.1.0.2	[number]	k4	2.1.0.2
Začínají	začínat	k5eAaImIp3nP	začínat
se	se	k3xPyFc4	se
prořezávat	prořezávat	k5eAaImF	prořezávat
mezi	mezi	k7c7	mezi
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
měsícem	měsíc	k1gInSc7	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
se	se	k3xPyFc4	se
prořezává	prořezávat	k5eAaImIp3nS	prořezávat
dolní	dolní	k2eAgInSc1d1	dolní
řezák	řezák	k1gInSc1	řezák
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zdravého	zdravý	k2eAgNnSc2d1	zdravé
dítěte	dítě	k1gNnSc2	dítě
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
všechny	všechen	k3xTgInPc4	všechen
mléčné	mléčný	k2eAgInPc4d1	mléčný
zuby	zub	k1gInPc4	zub
prořezány	prořezán	k2eAgInPc4d1	prořezán
do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
anatomii	anatomie	k1gFnSc6	anatomie
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
lidské	lidský	k2eAgInPc1d1	lidský
zuby	zub	k1gInPc1	zub
popisovány	popisován	k2eAgInPc1d1	popisován
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
I	i	k9	i
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
Dens	Dens	k1gInSc1	Dens
incisivus	incisivus	k1gMnSc1	incisivus
medialis	medialis	k1gInSc1	medialis
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
řezák	řezák	k1gInSc1	řezák
<g/>
)	)	kIx)	)
I	i	k9	i
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
Dens	Dens	k1gInSc1	Dens
incisivus	incisivus	k1gMnSc1	incisivus
lateralis	lateralis	k1gInSc1	lateralis
(	(	kIx(	(
<g/>
druhý	druhý	k4xOgInSc1	druhý
řezák	řezák	k1gInSc1	řezák
<g/>
)	)	kIx)	)
C	C	kA	C
<g/>
:	:	kIx,	:
Dens	Dens	k1gInSc1	Dens
caninus	caninus	k1gInSc1	caninus
(	(	kIx(	(
<g/>
špičák	špičák	k1gInSc1	špičák
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
P	P	kA	P
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
Dens	Dens	k1gInSc1	Dens
premolaris	premolaris	k1gInSc1	premolaris
primus	primus	k1gInSc1	primus
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
třenový	třenový	k2eAgInSc1d1	třenový
zub	zub	k1gInSc1	zub
<g/>
)	)	kIx)	)
P	P	kA	P
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
Dens	Dens	k1gInSc1	Dens
premolaris	premolaris	k1gInSc1	premolaris
secundus	secundus	k1gInSc1	secundus
(	(	kIx(	(
<g/>
druhý	druhý	k4xOgInSc1	druhý
třenový	třenový	k2eAgInSc1d1	třenový
zub	zub	k1gInSc1	zub
<g/>
)	)	kIx)	)
M	M	kA	M
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
Dens	Dens	k1gInSc1	Dens
molaris	molaris	k1gInSc1	molaris
primus	primus	k1gInSc4	primus
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
stolička	stolička	k1gFnSc1	stolička
<g/>
)	)	kIx)	)
M	M	kA	M
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
Dens	Dens	k1gInSc1	Dens
molaris	molaris	k1gInSc1	molaris
secundus	secundus	k1gInSc4	secundus
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
stolička	stolička	k1gFnSc1	stolička
<g/>
)	)	kIx)	)
M	M	kA	M
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
Dens	Densa	k1gFnPc2	Densa
molaris	molaris	k1gInSc1	molaris
tertius	tertius	k1gMnSc1	tertius
(	(	kIx(	(
<g/>
Dens	Dens	k1gInSc1	Dens
serotinus	serotinus	k1gInSc1	serotinus
<g/>
,	,	kIx,	,
Dens	Dens	k1gInSc1	Dens
sapientiae	sapientia	k1gFnSc2	sapientia
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
stolička	stolička	k1gFnSc1	stolička
<g/>
,	,	kIx,	,
zub	zub	k1gInSc1	zub
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
)	)	kIx)	)
Ve	v	k7c6	v
stomatologické	stomatologický	k2eAgFnSc6d1	stomatologická
praxi	praxe	k1gFnSc6	praxe
je	být	k5eAaImIp3nS	být
chrup	chrup	k1gInSc1	chrup
popsán	popsat	k5eAaPmNgInS	popsat
pomocí	pomocí	k7c2	pomocí
takzvaného	takzvaný	k2eAgInSc2d1	takzvaný
dentálního	dentální	k2eAgInSc2d1	dentální
kříže	kříž	k1gInSc2	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
zub	zub	k1gInSc1	zub
je	být	k5eAaImIp3nS	být
představován	představován	k2eAgInSc1d1	představován
jedním	jeden	k4xCgInSc7	jeden
dvouciferným	dvouciferný	k2eAgNnSc7d1	dvouciferné
číslem	číslo	k1gNnSc7	číslo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
jeho	jeho	k3xOp3gFnSc4	jeho
pozici	pozice	k1gFnSc4	pozice
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
cifra	cifra	k1gFnSc1	cifra
tzv.	tzv.	kA	tzv.
zubní	zubní	k2eAgInSc4d1	zubní
kvadrant	kvadrant	k1gInSc4	kvadrant
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
cifra	cifra	k1gFnSc1	cifra
pořadí	pořadí	k1gNnSc2	pořadí
od	od	k7c2	od
strědu	strěd	k1gInSc2	strěd
čelisti	čelist	k1gFnSc2	čelist
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
číslu	číslo	k1gNnSc3	číslo
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
doplnit	doplnit	k5eAaPmF	doplnit
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
či	či	k8xC	či
chybění	chybění	k1gNnSc6	chybění
zubu	zub	k1gInSc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Ukázka	ukázka	k1gFnSc1	ukázka
kompletního	kompletní	k2eAgInSc2d1	kompletní
dentálního	dentální	k2eAgInSc2d1	dentální
kříže	kříž	k1gInSc2	kříž
dospělého	dospělý	k1gMnSc4	dospělý
člověka	člověk	k1gMnSc4	člověk
<g/>
:	:	kIx,	:
18	[number]	k4	18
17	[number]	k4	17
16	[number]	k4	16
15	[number]	k4	15
14	[number]	k4	14
13	[number]	k4	13
12	[number]	k4	12
11	[number]	k4	11
|	|	kIx~	|
21	[number]	k4	21
22	[number]	k4	22
23	[number]	k4	23
24	[number]	k4	24
25	[number]	k4	25
26	[number]	k4	26
27	[number]	k4	27
28	[number]	k4	28
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
-	-	kIx~	-
48	[number]	k4	48
47	[number]	k4	47
46	[number]	k4	46
45	[number]	k4	45
44	[number]	k4	44
43	[number]	k4	43
42	[number]	k4	42
41	[number]	k4	41
|	|	kIx~	|
31	[number]	k4	31
32	[number]	k4	32
33	[number]	k4	33
34	[number]	k4	34
35	[number]	k4	35
36	[number]	k4	36
37	[number]	k4	37
38	[number]	k4	38
Zdravý	zdravý	k2eAgInSc4d1	zdravý
a	a	k8xC	a
čistý	čistý	k2eAgInSc4d1	čistý
chrup	chrup	k1gInSc4	chrup
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
celkové	celkový	k2eAgNnSc4d1	celkové
zdraví	zdraví	k1gNnSc4	zdraví
a	a	k8xC	a
kvalitu	kvalita	k1gFnSc4	kvalita
života	život	k1gInSc2	život
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
působíme	působit	k5eAaImIp1nP	působit
na	na	k7c4	na
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
většiny	většina	k1gFnSc2	většina
zánětlivých	zánětlivý	k2eAgNnPc2d1	zánětlivé
onemocnění	onemocnění	k1gNnPc2	onemocnění
dásní	dáseň	k1gFnPc2	dáseň
a	a	k8xC	a
zubních	zubní	k2eAgInPc2d1	zubní
kazů	kaz	k1gInPc2	kaz
je	být	k5eAaImIp3nS	být
plak	plak	k1gInSc1	plak
(	(	kIx(	(
<g/>
bakterie	bakterie	k1gFnSc2	bakterie
shromážděné	shromážděný	k2eAgFnSc2d1	shromážděná
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
na	na	k7c6	na
zubech	zub	k1gInPc6	zub
a	a	k8xC	a
měkkých	měkký	k2eAgFnPc6d1	měkká
ústních	ústní	k2eAgFnPc6d1	ústní
tkáních	tkáň	k1gFnPc6	tkáň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tento	tento	k3xDgInSc4	tento
plak	plak	k1gInSc4	plak
včas	včas	k6eAd1	včas
a	a	k8xC	a
řádně	řádně	k6eAd1	řádně
neodstraníme	odstranit	k5eNaPmIp1nP	odstranit
<g/>
,	,	kIx,	,
vyvolají	vyvolat	k5eAaPmIp3nP	vyvolat
jeho	jeho	k3xOp3gFnPc4	jeho
toxické	toxický	k2eAgFnPc4d1	toxická
zplodiny	zplodina	k1gFnPc4	zplodina
záněty	zánět	k1gInPc1	zánět
dásní	dáseň	k1gFnPc2	dáseň
a	a	k8xC	a
narušení	narušení	k1gNnSc2	narušení
skloviny	sklovina	k1gFnSc2	sklovina
na	na	k7c6	na
korunce	korunka	k1gFnSc6	korunka
zubu	zub	k1gInSc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
nepříjemným	příjemný	k2eNgInSc7d1	nepříjemný
zápachem	zápach	k1gInSc7	zápach
z	z	k7c2	z
úst	ústa	k1gNnPc2	ústa
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
končit	končit	k5eAaImF	končit
až	až	k9	až
nenávratnou	návratný	k2eNgFnSc7d1	nenávratná
ztrátou	ztráta	k1gFnSc7	ztráta
zubu	zub	k1gInSc2	zub
<g/>
.	.	kIx.	.
</s>
