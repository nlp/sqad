<s>
Nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
z	z	k7c2	z
věd	věda	k1gFnPc2	věda
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Aristotela	Aristoteles	k1gMnSc4	Aristoteles
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
(	(	kIx(	(
<g/>
politiké	politiká	k1gFnSc6	politiká
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
každý	každý	k3xTgMnSc1	každý
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
musí	muset	k5eAaImIp3nS	muset
naučit	naučit	k5eAaPmF	naučit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
