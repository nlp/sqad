<p>
<s>
Malazská	Malazský	k2eAgFnSc1d1	Malazská
kniha	kniha	k1gFnSc1	kniha
padlých	padlý	k1gMnPc2	padlý
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Malazan	Malazan	k1gMnSc1	Malazan
Book	Book	k1gMnSc1	Book
of	of	k?	of
the	the	k?	the
Fallen	Fallna	k1gFnPc2	Fallna
je	být	k5eAaImIp3nS	být
epická	epický	k2eAgFnSc1d1	epická
knižní	knižní	k2eAgFnSc1d1	knižní
série	série	k1gFnSc1	série
žánru	žánr	k1gInSc2	žánr
fantasy	fantas	k1gInPc4	fantas
Stevena	Steven	k2eAgFnSc1d1	Stevena
Eriksona	Eriksona	k1gFnSc1	Eriksona
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
Gardens	Gardens	k1gInSc1	Gardens
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gInSc1	Moon
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Měsíční	měsíční	k2eAgFnSc2d1	měsíční
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vyšel	vyjít	k5eAaPmAgMnS	vyjít
desátý	desátý	k4xOgMnSc1	desátý
a	a	k8xC	a
poslední	poslední	k2eAgInSc4d1	poslední
plánovaný	plánovaný	k2eAgInSc4d1	plánovaný
díl	díl	k1gInSc4	díl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Malazská	Malazský	k2eAgFnSc1d1	Malazská
kniha	kniha	k1gFnSc1	kniha
padlých	padlý	k1gMnPc2	padlý
dává	dávat	k5eAaImIp3nS	dávat
žánru	žánr	k1gInSc2	žánr
epické	epický	k2eAgInPc4d1	epický
fantasy	fantas	k1gInPc4	fantas
nový	nový	k2eAgInSc4d1	nový
rozměr	rozměr	k1gInSc4	rozměr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
nespočetném	spočetný	k2eNgNnSc6d1	nespočetné
množství	množství	k1gNnSc6	množství
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
není	být	k5eNaImIp3nS	být
ústřední	ústřední	k2eAgFnSc1d1	ústřední
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
několika	několik	k4yIc6	několik
kontinentech	kontinent	k1gInPc6	kontinent
<g/>
,	,	kIx,	,
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
časech	čas	k1gInPc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
jsou	být	k5eAaImIp3nP	být
plné	plný	k2eAgFnSc2d1	plná
magie	magie	k1gFnSc2	magie
<g/>
,	,	kIx,	,
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
mocných	mocný	k2eAgFnPc2d1	mocná
bytostí	bytost	k1gFnPc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Běžní	běžný	k2eAgMnPc1d1	běžný
smrtelníci	smrtelník	k1gMnPc1	smrtelník
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
stávají	stávat	k5eAaImIp3nP	stávat
pouze	pouze	k6eAd1	pouze
nástroji	nástroj	k1gInPc7	nástroj
v	v	k7c4	v
rukou	ruka	k1gFnPc2	ruka
těchto	tento	k3xDgFnPc2	tento
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
nástroji	nástroj	k1gInPc7	nástroj
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc2d1	vlastní
vůli	vůle	k1gFnSc3	vůle
a	a	k8xC	a
s	s	k7c7	s
vlastními	vlastní	k2eAgInPc7d1	vlastní
plány	plán	k1gInPc7	plán
uvnitř	uvnitř	k7c2	uvnitř
plánů	plán	k1gInPc2	plán
těch	ten	k3xDgInPc2	ten
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
slouží	sloužit	k5eAaImIp3nS	sloužit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
například	například	k6eAd1	například
od	od	k7c2	od
Pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
nelze	lze	k6eNd1	lze
jednoznačně	jednoznačně	k6eAd1	jednoznačně
rozdělit	rozdělit	k5eAaPmF	rozdělit
postavy	postava	k1gFnPc4	postava
na	na	k7c4	na
dobré	dobrý	k2eAgFnPc4d1	dobrá
a	a	k8xC	a
zlé	zlá	k1gFnPc4	zlá
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
vše	všechen	k3xTgNnSc4	všechen
relativizuje	relativizovat	k5eAaImIp3nS	relativizovat
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
různé	různý	k2eAgInPc4d1	různý
úhly	úhel	k1gInPc4	úhel
pohledu	pohled	k1gInSc2	pohled
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
jednou	jednou	k6eAd1	jednou
předkládá	předkládat	k5eAaImIp3nS	předkládat
jako	jako	k9	jako
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podruhé	podruhé	k6eAd1	podruhé
může	moct	k5eAaImIp3nS	moct
ukázat	ukázat	k5eAaPmF	ukázat
neplatné	platný	k2eNgNnSc4d1	neplatné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc1	přehled
dílů	díl	k1gInPc2	díl
série	série	k1gFnSc2	série
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Malazská	Malazský	k2eAgFnSc1d1	Malazská
kniha	kniha	k1gFnSc1	kniha
padlých	padlý	k1gMnPc2	padlý
===	===	k?	===
</s>
</p>
<p>
<s>
Měsíční	měsíční	k2eAgFnPc4d1	měsíční
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
(	(	kIx(	(
<g/>
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
Gardens	Gardens	k1gInSc1	Gardens
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gMnSc1	Moon
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
(	(	kIx(	(
<g/>
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
Deadhouse	Deadhouse	k6eAd1	Deadhouse
Gates	Gates	k1gMnSc1	Gates
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
Memories	Memories	k1gMnSc1	Memories
of	of	k?	of
Ice	Ice	k1gMnSc1	Ice
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dóm	dóm	k1gInSc1	dóm
řetězů	řetěz	k1gInPc2	řetěz
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
House	house	k1gNnSc1	house
of	of	k?	of
Chains	Chainsa	k1gFnPc2	Chainsa
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Půlnoční	půlnoční	k1gFnSc1	půlnoční
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
Midnight	Midnight	k1gMnSc1	Midnight
Tides	Tides	k1gMnSc1	Tides
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lovci	lovec	k1gMnPc1	lovec
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Bonehunters	Bonehunters	k1gInSc1	Bonehunters
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vichr	vichr	k1gInSc1	vichr
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
(	(	kIx(	(
<g/>
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
Reaper	Reaper	k1gInSc1	Reaper
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Gale	Gale	k1gMnSc1	Gale
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Daň	daň	k1gFnSc1	daň
pro	pro	k7c4	pro
ohaře	ohař	k1gMnPc4	ohař
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
Toll	Toll	k1gInSc1	Toll
the	the	k?	the
Hounds	Hounds	k1gInSc1	Hounds
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Prach	prach	k1gInSc1	prach
snů	sen	k1gInPc2	sen
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
Dust	Dust	k2eAgInSc1d1	Dust
of	of	k?	of
Dreams	Dreams	k1gInSc1	Dreams
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chromý	chromý	k2eAgMnSc1d1	chromý
bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
Crippled	Crippled	k1gMnSc1	Crippled
God	God	k1gMnSc1	God
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Příběhy	příběh	k1gInPc7	příběh
z	z	k7c2	z
Malazské	Malazský	k2eAgFnSc2d1	Malazská
říše	říš	k1gFnSc2	říš
===	===	k?	===
</s>
</p>
<p>
<s>
Knihy	kniha	k1gFnPc1	kniha
zasazené	zasazený	k2eAgFnPc1d1	zasazená
do	do	k7c2	do
světa	svět	k1gInSc2	svět
Malazu	Malaz	k1gInSc2	Malaz
od	od	k7c2	od
Ian	Ian	k1gFnSc2	Ian
Cameron	Cameron	k1gMnSc1	Cameron
Esslemonta	Esslemonta	k1gMnSc1	Esslemonta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Noc	noc	k1gFnSc1	noc
nožů	nůž	k1gInPc2	nůž
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
(	(	kIx(	(
<g/>
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
Night	Night	k1gMnSc1	Night
of	of	k?	of
Knives	Knives	k1gMnSc1	Knives
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Návrat	návrat	k1gInSc1	návrat
Rudé	rudý	k2eAgFnSc2d1	rudá
gardy	garda	k1gFnSc2	garda
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
(	(	kIx(	(
<g/>
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
Return	Return	k1gInSc1	Return
of	of	k?	of
the	the	k?	the
Crimson	Crimson	k1gMnSc1	Crimson
Guard	Guard	k1gMnSc1	Guard
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Meč	meč	k1gInSc1	meč
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
(	(	kIx(	(
<g/>
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
<g/>
Stonewielder	Stonewielder	k1gMnSc1	Stonewielder
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jablko	jablko	k1gNnSc1	jablko
<g/>
,	,	kIx,	,
žezlo	žezlo	k1gNnSc1	žezlo
<g/>
,	,	kIx,	,
trůn	trůn	k1gInSc1	trůn
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
(	(	kIx(	(
<g/>
Orb	Orb	k1gMnSc5	Orb
<g/>
,	,	kIx,	,
Sceptre	Sceptr	k1gMnSc5	Sceptr
<g/>
,	,	kIx,	,
Throne	Thron	k1gMnSc5	Thron
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Krev	krev	k1gFnSc1	krev
a	a	k8xC	a
kosti	kost	k1gFnSc2	kost
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
(	(	kIx(	(
<g/>
Blood	Blood	k1gInSc1	Blood
and	and	k?	and
Bone	bon	k1gInSc5	bon
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Assail	Assail	k1gMnSc1	Assail
<g/>
,	,	kIx,	,
2019	[number]	k4	2019
(	(	kIx(	(
<g/>
Assail	Assail	k1gMnSc1	Assail
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Povídky	povídka	k1gFnPc1	povídka
===	===	k?	===
</s>
</p>
<p>
<s>
Povídky	povídka	k1gFnPc1	povídka
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
Malazu	Malaz	k1gInSc2	Malaz
o	o	k7c6	o
dvou	dva	k4xCgNnPc6	dva
nekromancerech	nekromancero	k1gNnPc6	nekromancero
<g/>
,	,	kIx,	,
Bauchelainovi	Bauchelain	k1gMnSc3	Bauchelain
a	a	k8xC	a
Korlbalu	Korlbal	k1gMnSc3	Korlbal
Špičákovi	Špičák	k1gMnSc3	Špičák
<g/>
,	,	kIx,	,
od	od	k7c2	od
Steven	Stevno	k1gNnPc2	Stevno
Eriksona	Erikson	k1gMnSc2	Erikson
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
tři	tři	k4xCgFnPc1	tři
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
Potoky	potok	k1gInPc1	potok
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
Zdraví	zdravý	k2eAgMnPc1d1	zdravý
mrtví	mrtvý	k1gMnPc1	mrtvý
a	a	k8xC	a
Klidné	klidný	k2eAgFnPc1d1	klidná
vody	voda	k1gFnPc1	voda
Přejdesmíchu	Přejdesmích	k1gInSc2	Přejdesmích
<g/>
)	)	kIx)	)
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
také	také	k9	také
vyšly	vyjít	k5eAaPmAgFnP	vyjít
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
knize	kniha	k1gFnSc6	kniha
Potoky	potok	k1gInPc1	potok
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Potoky	potok	k1gInPc1	potok
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Pevnost	pevnost	k1gFnSc1	pevnost
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
(	(	kIx(	(
<g/>
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
Blood	Blood	k1gInSc1	Blood
Follows	Followsa	k1gFnPc2	Followsa
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zdraví	zdravý	k2eAgMnPc1d1	zdravý
mrtví	mrtvý	k1gMnPc1	mrtvý
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Legie	legie	k1gFnSc2	legie
nesmrtelných	nesmrtelný	k1gMnPc2	nesmrtelný
(	(	kIx(	(
<g/>
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Healthy	Healtha	k1gFnPc1	Healtha
Dead	Deada	k1gFnPc2	Deada
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Klidné	klidný	k2eAgFnPc1d1	klidná
vody	voda	k1gFnPc1	voda
Přejdesmíchu	Přejdesmích	k1gInSc2	Přejdesmích
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
(	(	kIx(	(
<g/>
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Lees	Lees	k1gInSc1	Lees
of	of	k?	of
Laughter	Laughter	k1gInSc1	Laughter
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
End	End	k1gFnSc7	End
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Crack	Crack	k1gInSc1	Crack
<g/>
'	'	kIx"	'
<g/>
d	d	k?	d
Pot	pot	k1gInSc1	pot
Trail	Traila	k1gFnPc2	Traila
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
The	The	k?	The
Wurms	Wurms	k1gInSc1	Wurms
of	of	k?	of
Blearmouth	Blearmouth	k1gInSc1	Blearmouth
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
===	===	k?	===
Charkanaská	Charkanaský	k2eAgFnSc1d1	Charkanaský
trilogie	trilogie	k1gFnSc1	trilogie
===	===	k?	===
</s>
</p>
<p>
<s>
Prequel	Prequel	k1gInSc1	Prequel
o	o	k7c6	o
rase	rasa	k1gFnSc6	rasa
Tiste	Tist	k1gInSc5	Tist
zasazený	zasazený	k2eAgInSc4d1	zasazený
do	do	k7c2	do
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
minulosti	minulost	k1gFnSc2	minulost
malazského	malazský	k2eAgInSc2d1	malazský
světa	svět	k1gInSc2	svět
od	od	k7c2	od
Steven	Stevno	k1gNnPc2	Stevno
Eriksona	Erikson	k1gMnSc2	Erikson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stvoření	stvoření	k1gNnSc1	stvoření
temnoty	temnota	k1gFnSc2	temnota
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
(	(	kIx(	(
<g/>
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
Forge	Forge	k1gFnSc1	Forge
of	of	k?	of
Darkness	Darknessa	k1gFnPc2	Darknessa
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pád	Pád	k1gInSc1	Pád
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
(	(	kIx(	(
<g/>
orig	origa	k1gFnPc2	origa
<g/>
.	.	kIx.	.
</s>
<s>
Fall	Fall	k1gMnSc1	Fall
of	of	k?	of
Light	Light	k1gMnSc1	Light
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Walk	Walk	k1gMnSc1	Walk
in	in	k?	in
Shadow	Shadow	k1gMnSc1	Shadow
<g/>
,	,	kIx,	,
pozastaveno	pozastaven	k2eAgNnSc1d1	pozastaveno
</s>
</p>
<p>
<s>
===	===	k?	===
The	The	k1gFnPc1	The
Witness	Witness	k1gInSc4	Witness
Trilogy	Triloga	k1gFnSc2	Triloga
===	===	k?	===
</s>
</p>
<p>
<s>
Sequelová	Sequelový	k2eAgFnSc1d1	Sequelový
trilogie	trilogie	k1gFnSc1	trilogie
k	k	k7c3	k
hlavní	hlavní	k2eAgFnSc3d1	hlavní
sérii	série	k1gFnSc3	série
od	od	k7c2	od
Steven	Stevna	k1gFnPc2	Stevna
Eriksona	Erikson	k1gMnSc2	Erikson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
The	The	k?	The
God	God	k1gFnSc1	God
is	is	k?	is
Not	nota	k1gFnPc2	nota
Willing	Willing	k1gInSc1	Willing
<g/>
,	,	kIx,	,
plánováno	plánován	k2eAgNnSc1d1	plánováno
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2019	[number]	k4	2019
</s>
</p>
<p>
<s>
===	===	k?	===
Path	Path	k1gInSc1	Path
to	ten	k3xDgNnSc1	ten
Ascendancy	Ascendanca	k1gFnPc1	Ascendanca
===	===	k?	===
</s>
</p>
<p>
<s>
Prequel	Prequel	k1gInSc1	Prequel
série	série	k1gFnSc2	série
o	o	k7c6	o
Tanečníkovi	tanečník	k1gMnSc6	tanečník
a	a	k8xC	a
Kellanvedovi	Kellanved	k1gMnSc6	Kellanved
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc2	jejich
nástupu	nástup	k1gInSc2	nástup
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
od	od	k7c2	od
Ian	Ian	k1gFnSc2	Ian
Cameron	Cameron	k1gMnSc1	Cameron
Esslemonta	Esslemonta	k1gMnSc1	Esslemonta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dancer	Dancer	k1gInSc1	Dancer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Lament	Lament	k?	Lament
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
</s>
</p>
<p>
<s>
Deadhouse	Deadhouse	k6eAd1	Deadhouse
Landing	Landing	k1gInSc1	Landing
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
</s>
</p>
<p>
<s>
Kellanved	Kellanved	k1gInSc1	Kellanved
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Reach	Reach	k1gInSc1	Reach
<g/>
,	,	kIx,	,
plánováno	plánován	k2eAgNnSc1d1	plánováno
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2019	[number]	k4	2019
</s>
</p>
<p>
<s>
==	==	k?	==
Postavy	postava	k1gFnPc1	postava
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
stovky	stovka	k1gFnPc1	stovka
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
výčet	výčet	k1gInSc4	výčet
jen	jen	k9	jen
několika	několik	k4yIc2	několik
málo	málo	k4c4	málo
významných	významný	k2eAgFnPc2d1	významná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Malažané	Malažan	k1gMnPc5	Malažan
===	===	k?	===
</s>
</p>
<p>
<s>
Ben	Ben	k1gInSc1	Ben
Adaefeon	Adaefeon	k1gInSc1	Adaefeon
Delat	Delat	k1gInSc4	Delat
alias	alias	k9	alias
Rychlej	Rychlej	k?	Rychlej
Ben	Ben	k1gInSc1	Ben
</s>
</p>
<p>
<s>
Císařovna	císařovna	k1gFnSc1	císařovna
Laseen	Lasena	k1gFnPc2	Lasena
-	-	kIx~	-
Mrzena	mrzen	k2eAgFnSc1d1	Mrzena
</s>
</p>
<p>
<s>
Whiskeyjack	Whiskeyjack	k6eAd1	Whiskeyjack
</s>
</p>
<p>
<s>
Dujek	Dujek	k6eAd1	Dujek
Jednoruký	jednoruký	k2eAgInSc1d1	jednoruký
</s>
</p>
<p>
<s>
Šumař	šumař	k1gMnSc1	šumař
</s>
</p>
<p>
<s>
Ganoes	Ganoes	k1gInSc1	Ganoes
Stabro	Stabro	k1gNnSc1	Stabro
Paran	Parana	k1gFnPc2	Parana
</s>
</p>
<p>
<s>
Heborik	Heborik	k1gMnSc1	Heborik
</s>
</p>
<p>
<s>
Kalam	Kalam	k6eAd1	Kalam
Méchar	Méchar	k1gInSc1	Méchar
</s>
</p>
<p>
<s>
Apsalar	Apsalar	k1gMnSc1	Apsalar
-	-	kIx~	-
Neapsalar	Neapsalar	k1gMnSc1	Neapsalar
</s>
</p>
<p>
<s>
Tavore	Tavor	k1gMnSc5	Tavor
Paran	Paran	k1gMnSc1	Paran
</s>
</p>
<p>
<s>
Felisín	Felisín	k1gMnSc1	Felisín
Paran	Paran	k1gMnSc1	Paran
</s>
</p>
<p>
<s>
Toc	Toc	k?	Toc
Mladší	mladý	k2eAgMnSc1d2	mladší
</s>
</p>
<p>
<s>
Besana	Besana	k1gFnSc1	Besana
</s>
</p>
<p>
<s>
===	===	k?	===
Tiste	Tist	k1gInSc5	Tist
Edur	Edura	k1gFnPc2	Edura
===	===	k?	===
</s>
</p>
<p>
<s>
Rhulad	Rhulad	k6eAd1	Rhulad
Sengar	Sengar	k1gInSc1	Sengar
</s>
</p>
<p>
<s>
Trull	Trull	k1gMnSc1	Trull
Sengar	Sengar	k1gMnSc1	Sengar
</s>
</p>
<p>
<s>
Strach	strach	k1gInSc1	strach
Sengar	Sengara	k1gFnPc2	Sengara
</s>
</p>
<p>
<s>
Hannan	Hannan	k1gMnSc1	Hannan
Mosag	Mosag	k1gMnSc1	Mosag
</s>
</p>
<p>
<s>
===	===	k?	===
Tiste	Tist	k1gInSc5	Tist
Andii	Andius	k1gMnPc7	Andius
===	===	k?	===
</s>
</p>
<p>
<s>
Anomander	Anomander	k1gMnSc1	Anomander
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
vlas	vlas	k1gInSc4	vlas
</s>
</p>
<p>
<s>
Korlat	Korlat	k5eAaPmF	Korlat
</s>
</p>
<p>
<s>
===	===	k?	===
T	T	kA	T
<g/>
́	́	k?	́
<g/>
lan	lano	k1gNnPc2	lano
Imass	Imass	k1gInSc1	Imass
===	===	k?	===
</s>
</p>
<p>
<s>
Tolar	tolar	k1gInSc1	tolar
</s>
</p>
<p>
<s>
Kilava	Kilava	k1gFnSc1	Kilava
</s>
</p>
<p>
<s>
===	===	k?	===
Bohové	bůh	k1gMnPc1	bůh
===	===	k?	===
</s>
</p>
<p>
<s>
Stínupán	Stínupán	k2eAgInSc1d1	Stínupán
</s>
</p>
<p>
<s>
Kotilion	Kotilion	k1gInSc1	Kotilion
</s>
</p>
<p>
<s>
Mistr	mistr	k1gMnSc1	mistr
Kápě	kápě	k1gFnSc2	kápě
</s>
</p>
<p>
<s>
Chromý	chromý	k2eAgMnSc1d1	chromý
Bůh	bůh	k1gMnSc1	bůh
</s>
</p>
<p>
<s>
Ohnice	ohnice	k1gFnSc1	ohnice
-	-	kIx~	-
Spící	spící	k2eAgFnSc1d1	spící
bohyně	bohyně	k1gFnSc1	bohyně
</s>
</p>
<p>
<s>
K	k	k7c3	k
<g/>
́	́	k?	́
<g/>
rul	rula	k1gFnPc2	rula
-	-	kIx~	-
Starší	starší	k1gMnSc1	starší
bůh	bůh	k1gMnSc1	bůh
</s>
</p>
<p>
<s>
Draconus	Draconus	k1gMnSc1	Draconus
-	-	kIx~	-
Starší	starší	k1gMnSc1	starší
bůh	bůh	k1gMnSc1	bůh
</s>
</p>
<p>
<s>
Mael	Mael	k1gMnSc1	Mael
-	-	kIx~	-
Starší	starší	k1gMnSc1	starší
bůh	bůh	k1gMnSc1	bůh
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
===	===	k?	===
</s>
</p>
<p>
<s>
Karsa	Karsa	k1gFnSc1	Karsa
Orlong	Orlonga	k1gFnPc2	Orlonga
</s>
</p>
<p>
<s>
Icarium	Icarium	k1gNnSc1	Icarium
</s>
</p>
<p>
<s>
Tehol	Tehol	k1gInSc1	Tehol
Beddikt	Beddikt	k1gInSc1	Beddikt
</s>
</p>
<p>
<s>
Kvítko	kvítko	k1gNnSc1	kvítko
Berka	Berka	k1gMnSc1	Berka
&	&	k?	&
Řezník	řezník	k1gMnSc1	řezník
</s>
</p>
<p>
<s>
Rallik	Rallik	k1gMnSc1	Rallik
Nom	Nom	k1gMnSc1	Nom
</s>
</p>
<p>
<s>
Torvald	Torvald	k1gMnSc1	Torvald
Nom	Nom	k1gMnSc1	Nom
</s>
</p>
<p>
<s>
Kruppe	Kruppat	k5eAaPmIp3nS	Kruppat
</s>
</p>
<p>
<s>
Iskaral	Iskarat	k5eAaImAgMnS	Iskarat
Pust	pusta	k1gFnPc2	pusta
</s>
</p>
<p>
<s>
Mappo	Mappa	k1gFnSc5	Mappa
Poříz	poříz	k1gMnSc1	poříz
</s>
</p>
<p>
<s>
Barúk	Barúk	k6eAd1	Barúk
</s>
</p>
<p>
<s>
Leoman	Leoman	k1gMnSc1	Leoman
s	s	k7c7	s
Cepy	cep	k1gInPc7	cep
</s>
</p>
<p>
<s>
Ša	Ša	k?	Ša
<g/>
'	'	kIx"	'
<g/>
ik	ik	k?	ik
</s>
</p>
<p>
<s>
Šerana	Šerana	k1gFnSc1	Šerana
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
MalazPedie	MalazPedie	k1gFnSc1	MalazPedie
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
http://www.legie.info	[url]	k6eAd1	http://www.legie.info
</s>
</p>
<p>
<s>
Malazská	Malazský	k2eAgFnSc1d1	Malazská
kniha	kniha	k1gFnSc1	kniha
padlých	padlý	k1gMnPc2	padlý
na	na	k7c4	na
postavy	postava	k1gFnPc4	postava
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
