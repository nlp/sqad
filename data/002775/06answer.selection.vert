<s>
Sapfó	Sapfó	k?	Sapfó
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
v	v	k7c6	v
attickém	attický	k2eAgInSc6d1	attický
dialektu	dialekt	k1gInSc6	dialekt
Σ	Σ	k?	Σ
<g/>
,	,	kIx,	,
v	v	k7c6	v
aiolském	aiolský	k2eAgInSc6d1	aiolský
dialektu	dialekt	k1gInSc6	dialekt
Ψ	Ψ	k?	Ψ
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
630	[number]	k4	630
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
612	[number]	k4	612
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
-	-	kIx~	-
po	po	k7c6	po
roce	rok	k1gInSc6	rok
600	[number]	k4	600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
starořecká	starořecký	k2eAgFnSc1d1	starořecká
básnířka	básnířka	k1gFnSc1	básnířka
z	z	k7c2	z
Mytilény	Mytiléna	k1gFnSc2	Mytiléna
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Lesbos	Lesbos	k1gInSc1	Lesbos
<g/>
,	,	kIx,	,
kulturním	kulturní	k2eAgNnSc6d1	kulturní
centru	centrum	k1gNnSc6	centrum
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
zde	zde	k6eAd1	zde
dívčí	dívčí	k2eAgFnSc4d1	dívčí
internátní	internátní	k2eAgFnSc4d1	internátní
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
