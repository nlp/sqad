<s>
Sapfó	Sapfó	k?	Sapfó
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
v	v	k7c6	v
attickém	attický	k2eAgInSc6d1	attický
dialektu	dialekt	k1gInSc6	dialekt
Σ	Σ	k?	Σ
<g/>
,	,	kIx,	,
v	v	k7c6	v
aiolském	aiolský	k2eAgInSc6d1	aiolský
dialektu	dialekt	k1gInSc6	dialekt
Ψ	Ψ	k?	Ψ
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
630	[number]	k4	630
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
612	[number]	k4	612
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
-	-	kIx~	-
po	po	k7c6	po
roce	rok	k1gInSc6	rok
600	[number]	k4	600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
starořecká	starořecký	k2eAgFnSc1d1	starořecká
básnířka	básnířka	k1gFnSc1	básnířka
z	z	k7c2	z
Mytilény	Mytiléna	k1gFnSc2	Mytiléna
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Lesbos	Lesbos	k1gInSc1	Lesbos
<g/>
,	,	kIx,	,
kulturním	kulturní	k2eAgNnSc6d1	kulturní
centru	centrum	k1gNnSc6	centrum
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
zde	zde	k6eAd1	zde
dívčí	dívčí	k2eAgFnSc4d1	dívčí
internátní	internátní	k2eAgFnSc4d1	internátní
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Sapfó	Sapfó	k?	Sapfó
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
Eresu	Eres	k1gInSc6	Eres
(	(	kIx(	(
<g/>
Eressos	Eressos	k1gMnSc1	Eressos
<g/>
)	)	kIx)	)
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Lesbos	Lesbos	k1gInSc1	Lesbos
v	v	k7c6	v
Egejském	egejský	k2eAgNnSc6d1	Egejské
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
v	v	k7c6	v
Mytileně	Mytilena	k1gFnSc6	Mytilena
(	(	kIx(	(
<g/>
Mytilini	Mytilin	k2eAgMnPc1d1	Mytilin
<g/>
)	)	kIx)	)
na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
ostrově	ostrov	k1gInSc6	ostrov
nejenom	nejenom	k6eAd1	nejenom
prožila	prožít	k5eAaPmAgFnS	prožít
většinu	většina	k1gFnSc4	většina
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
aristokratické	aristokratický	k2eAgFnSc2d1	aristokratická
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
angažovala	angažovat	k5eAaBmAgFnS	angažovat
v	v	k7c6	v
politickém	politický	k2eAgInSc6d1	politický
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Provdala	provdat	k5eAaPmAgFnS	provdat
se	se	k3xPyFc4	se
za	za	k7c4	za
bohatého	bohatý	k2eAgMnSc4d1	bohatý
kupce	kupec	k1gMnSc4	kupec
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
Andros	Androsa	k1gFnPc2	Androsa
-	-	kIx~	-
Kerkilase	Kerkilas	k1gMnSc5	Kerkilas
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
měla	mít	k5eAaImAgFnS	mít
dceru	dcera	k1gFnSc4	dcera
Kleis	Kleis	k1gFnSc2	Kleis
a	a	k8xC	a
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
zdědila	zdědit	k5eAaPmAgFnS	zdědit
značné	značný	k2eAgNnSc4d1	značné
jmění	jmění	k1gNnSc4	jmění
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
ji	on	k3xPp3gFnSc4	on
roku	rok	k1gInSc2	rok
604	[number]	k4	604
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
přiměly	přimět	k5eAaPmAgInP	přimět
k	k	k7c3	k
útěku	útěk	k1gInSc3	útěk
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
rodný	rodný	k2eAgInSc4d1	rodný
ostrov	ostrov	k1gInSc4	ostrov
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jejím	její	k3xOp3gInSc7	její
životem	život	k1gInSc7	život
je	být	k5eAaImIp3nS	být
spojena	spojen	k2eAgFnSc1d1	spojena
řada	řada	k1gFnSc1	řada
legend	legenda	k1gFnPc2	legenda
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
lze	lze	k6eAd1	lze
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
pravdou	pravda	k1gFnSc7	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
nešťastné	šťastný	k2eNgFnSc2d1	nešťastná
lásky	láska	k1gFnSc2	láska
ke	k	k7c3	k
krásnému	krásný	k2eAgMnSc3d1	krásný
Faonovi	Faon	k1gMnSc3	Faon
skočila	skočit	k5eAaPmAgFnS	skočit
z	z	k7c2	z
Leukadské	Leukadský	k2eAgFnSc2d1	Leukadský
skály	skála	k1gFnSc2	skála
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
dodnes	dodnes	k6eAd1	dodnes
cílem	cíl	k1gInSc7	cíl
mnoha	mnoho	k4c2	mnoho
turistických	turistický	k2eAgFnPc2d1	turistická
výprav	výprava	k1gFnPc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Sapfó	Sapfó	k?	Sapfó
vedla	vést	k5eAaImAgFnS	vést
dívčí	dívčí	k2eAgInSc4d1	dívčí
kroužek	kroužek	k1gInSc4	kroužek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
uctíval	uctívat	k5eAaImAgInS	uctívat
hudbou	hudba	k1gFnSc7	hudba
a	a	k8xC	a
poezií	poezie	k1gFnSc7	poezie
bohyni	bohyně	k1gFnSc4	bohyně
lásky	láska	k1gFnSc2	láska
Afroditu	Afrodita	k1gFnSc4	Afrodita
a	a	k8xC	a
devět	devět	k4xCc4	devět
Múz	Múza	k1gFnPc2	Múza
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
obecného	obecný	k2eAgNnSc2d1	obecné
povědomí	povědomí	k1gNnSc2	povědomí
se	se	k3xPyFc4	se
Sapfó	Sapfó	k1gFnSc1	Sapfó
zapsala	zapsat	k5eAaPmAgFnS	zapsat
upřímnou	upřímný	k2eAgFnSc7d1	upřímná
a	a	k8xC	a
citovou	citový	k2eAgFnSc7d1	citová
náklonností	náklonnost	k1gFnSc7	náklonnost
ke	k	k7c3	k
členkám	členka	k1gFnPc3	členka
svého	své	k1gNnSc2	své
kroužku	kroužek	k1gInSc2	kroužek
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
z	z	k7c2	z
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
básní	báseň	k1gFnPc2	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
básnířčina	básnířčin	k2eAgNnSc2d1	básnířčin
rodiště	rodiště	k1gNnSc2	rodiště
(	(	kIx(	(
<g/>
Lesbos	Lesbos	k1gInSc1	Lesbos
<g/>
)	)	kIx)	)
dodnes	dodnes	k6eAd1	dodnes
přežívá	přežívat	k5eAaImIp3nS	přežívat
v	v	k7c6	v
označení	označení	k1gNnSc6	označení
ženské	ženský	k2eAgFnSc2d1	ženská
homosexuality	homosexualita	k1gFnSc2	homosexualita
-	-	kIx~	-
lesbická	lesbický	k2eAgFnSc1d1	lesbická
láska	láska	k1gFnSc1	láska
či	či	k8xC	či
lesbismus	lesbismus	k1gInSc1	lesbismus
<g/>
.	.	kIx.	.
</s>
<s>
Sapfó	Sapfó	k?	Sapfó
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nebyla	být	k5eNaImAgFnS	být
vyhraněná	vyhraněný	k2eAgFnSc1d1	vyhraněná
lesba	lesba	k1gFnSc1	lesba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
její	její	k3xOp3gNnPc4	její
manželství	manželství	k1gNnPc4	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Doklady	doklad	k1gInPc1	doklad
o	o	k7c6	o
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
opačným	opačný	k2eAgNnSc7d1	opačné
pohlavím	pohlaví	k1gNnSc7	pohlaví
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
i	i	k9	i
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
Alkaiových	Alkaiový	k2eAgFnPc2d1	Alkaiový
básní	báseň	k1gFnPc2	báseň
věnována	věnovat	k5eAaImNgFnS	věnovat
právě	právě	k9	právě
jí	on	k3xPp3gFnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Sapfó	Sapfó	k?	Sapfó
byla	být	k5eAaImAgFnS	být
představitelka	představitelka	k1gFnSc1	představitelka
tzv.	tzv.	kA	tzv.
sólové	sólový	k2eAgFnSc2d1	sólová
lyriky	lyrika	k1gFnSc2	lyrika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejího	její	k3xOp3gNnSc2	její
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
pouze	pouze	k6eAd1	pouze
zlomky	zlomek	k1gInPc1	zlomek
milostné	milostný	k2eAgFnSc2d1	milostná
a	a	k8xC	a
svatební	svatební	k2eAgFnSc2d1	svatební
písně	píseň	k1gFnSc2	píseň
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
Písně	píseň	k1gFnPc1	píseň
z	z	k7c2	z
Lesbu	Lesbos	k1gInSc2	Lesbos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
politicky	politicky	k6eAd1	politicky
aktivními	aktivní	k2eAgMnPc7d1	aktivní
a	a	k8xC	a
vzdělanými	vzdělaný	k2eAgMnPc7d1	vzdělaný
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
období	období	k1gNnSc4	období
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
pravidlech	pravidlo	k1gNnPc6	pravidlo
ústní	ústní	k2eAgFnSc2d1	ústní
(	(	kIx(	(
<g/>
orální	orální	k2eAgFnSc2d1	orální
<g/>
)	)	kIx)	)
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
autoři	autor	k1gMnPc1	autor
čerpali	čerpat	k5eAaImAgMnP	čerpat
náměty	námět	k1gInPc4	námět
<g/>
,	,	kIx,	,
větnou	větný	k2eAgFnSc4d1	větná
skladbu	skladba	k1gFnSc4	skladba
i	i	k8xC	i
literární	literární	k2eAgNnPc4d1	literární
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
řeckou	řecký	k2eAgFnSc4d1	řecká
milostnou	milostný	k2eAgFnSc4d1	milostná
lyriku	lyrika	k1gFnSc4	lyrika
byl	být	k5eAaImAgInS	být
poměrně	poměrně	k6eAd1	poměrně
velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
po	po	k7c4	po
celé	celý	k2eAgNnSc4d1	celé
trvání	trvání	k1gNnSc4	trvání
starořecké	starořecký	k2eAgFnSc2d1	starořecká
poezie	poezie	k1gFnSc2	poezie
se	se	k3xPyFc4	se
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
tvorbě	tvorba	k1gFnSc3	tvorba
řada	řada	k1gFnSc1	řada
básníků	básník	k1gMnPc2	básník
vracela	vracet	k5eAaImAgFnS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sapfó	Sapfó	k1gFnSc1	Sapfó
skládala	skládat	k5eAaImAgFnS	skládat
v	v	k7c6	v
aiolském	aiolský	k2eAgNnSc6d1	aiolský
nářečí	nářečí	k1gNnSc6	nářečí
řečtiny	řečtina	k1gFnSc2	řečtina
a	a	k8xC	a
že	že	k8xS	že
psala	psát	k5eAaImAgFnS	psát
především	především	k9	především
elegie	elegie	k1gFnSc1	elegie
a	a	k8xC	a
hymny	hymna	k1gFnSc2	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Sapfó	Sapfó	k?	Sapfó
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
označována	označovat	k5eAaImNgFnS	označovat
(	(	kIx(	(
<g/>
Platónem	Platón	k1gMnSc7	Platón
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
desátá	desátý	k4xOgFnSc1	desátý
Múza	Múza	k1gFnSc1	Múza
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Antipatros	Antipatrosa	k1gFnPc2	Antipatrosa
ji	on	k3xPp3gFnSc4	on
nazval	nazvat	k5eAaBmAgMnS	nazvat
"	"	kIx"	"
<g/>
Homérem	Homér	k1gMnSc7	Homér
ženského	ženský	k2eAgInSc2d1	ženský
rodu	rod	k1gInSc2	rod
<g/>
"	"	kIx"	"
a	a	k8xC	a
Lúkianos	Lúkianosa	k1gFnPc2	Lúkianosa
"	"	kIx"	"
<g/>
sladkou	sladký	k2eAgFnSc7d1	sladká
chloubou	chlouba	k1gFnSc7	chlouba
ostrova	ostrov	k1gInSc2	ostrov
Lesbos	Lesbos	k1gInSc1	Lesbos
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejoblíbenějších	oblíbený	k2eAgFnPc2d3	nejoblíbenější
(	(	kIx(	(
<g/>
v	v	k7c6	v
antice	antika	k1gFnSc6	antika
<g/>
)	)	kIx)	)
metrických	metrický	k2eAgFnPc2d1	metrická
staveb	stavba	k1gFnPc2	stavba
verše	verš	k1gInSc2	verš
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
sapfická	sapfický	k2eAgFnSc1d1	sapfická
strofa	strofa	k1gFnSc1	strofa
<g/>
.	.	kIx.	.
</s>
<s>
Sapfó	Sapfó	k?	Sapfó
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
výrazné	výrazný	k2eAgFnPc4d1	výrazná
osobnosti	osobnost	k1gFnPc4	osobnost
antické	antický	k2eAgFnSc2d1	antická
poezie	poezie	k1gFnSc2	poezie
římské	římský	k2eAgFnSc2d1	římská
(	(	kIx(	(
<g/>
Catullus	Catullus	k1gMnSc1	Catullus
<g/>
,	,	kIx,	,
Ovidius	Ovidius	k1gMnSc1	Ovidius
<g/>
,	,	kIx,	,
Horatius	Horatius	k1gMnSc1	Horatius
<g/>
)	)	kIx)	)
i	i	k9	i
básnictví	básnictví	k1gNnSc2	básnictví
epochy	epocha	k1gFnSc2	epocha
romantismu	romantismus	k1gInSc2	romantismus
(	(	kIx(	(
<g/>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Gottlieb	Gottliba	k1gFnPc2	Gottliba
Klopstock	Klopstock	k1gMnSc1	Klopstock
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
Hölderlin	Hölderlin	k1gInSc1	Hölderlin
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Grillparzer	Grillparzer	k1gMnSc1	Grillparzer
<g/>
,	,	kIx,	,
Rainer	Rainer	k1gMnSc1	Rainer
Maria	Mario	k1gMnSc2	Mario
Rilke	Rilk	k1gFnSc2	Rilk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hru	hra	k1gFnSc4	hra
o	o	k7c4	o
Sapfo	Sapfo	k1gNnSc4	Sapfo
napsala	napsat	k5eAaBmAgFnS	napsat
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
francouzská	francouzský	k2eAgFnSc1d1	francouzská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Madame	madame	k1gFnSc2	madame
de	de	k?	de
Stael	Stael	k1gMnSc1	Stael
<g/>
.	.	kIx.	.
</s>
