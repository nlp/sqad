<s>
Gepard	gepard	k1gMnSc1	gepard
štíhlý	štíhlý	k2eAgMnSc1d1	štíhlý
(	(	kIx(	(
<g/>
Acinonyx	Acinonyx	k1gInSc1	Acinonyx
jubatus	jubatus	k1gInSc1	jubatus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kočkovitá	kočkovitý	k2eAgFnSc1d1	kočkovitá
šelma	šelma	k1gFnSc1	šelma
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
nejrychlejší	rychlý	k2eAgFnSc1d3	nejrychlejší
suchozemské	suchozemský	k2eAgNnSc4d1	suchozemské
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
rychlost	rychlost	k1gFnSc4	rychlost
okolo	okolo	k7c2	okolo
100	[number]	k4	100
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
.	.	kIx.	.
</s>

