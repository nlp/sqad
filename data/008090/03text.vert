<s>
Gepard	gepard	k1gMnSc1	gepard
štíhlý	štíhlý	k2eAgMnSc1d1	štíhlý
(	(	kIx(	(
<g/>
Acinonyx	Acinonyx	k1gInSc1	Acinonyx
jubatus	jubatus	k1gInSc1	jubatus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kočkovitá	kočkovitý	k2eAgFnSc1d1	kočkovitá
šelma	šelma	k1gFnSc1	šelma
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
nejrychlejší	rychlý	k2eAgFnSc1d3	nejrychlejší
suchozemské	suchozemský	k2eAgNnSc4d1	suchozemské
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
rychlost	rychlost	k1gFnSc4	rychlost
okolo	okolo	k7c2	okolo
100	[number]	k4	100
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
.	.	kIx.	.
</s>
<s>
Rychlosti	rychlost	k1gFnPc1	rychlost
nad	nad	k7c4	nad
110	[number]	k4	110
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
mýtem	mýtus	k1gInSc7	mýtus
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
podstatně	podstatně	k6eAd1	podstatně
zajímavější	zajímavý	k2eAgFnSc1d2	zajímavější
je	být	k5eAaImIp3nS	být
neuvěřitelná	uvěřitelný	k2eNgFnSc1d1	neuvěřitelná
možnost	možnost	k1gFnSc1	možnost
měnit	měnit	k5eAaImF	měnit
akceleraci	akcelerace	k1gFnSc4	akcelerace
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
činí	činit	k5eAaImIp3nS	činit
jednoznačně	jednoznačně	k6eAd1	jednoznačně
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejhbitějších	hbitý	k2eAgMnPc2d3	nejhbitější
savců	savec	k1gMnPc2	savec
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
zprávy	zpráva	k1gFnSc2	zpráva
Sarah	Sarah	k1gFnSc2	Sarah
Durantové	Durantový	k2eAgFnSc2d1	Durantová
z	z	k7c2	z
Londýnské	londýnský	k2eAgFnSc2d1	londýnská
zoologické	zoologický	k2eAgFnSc2d1	zoologická
společnosti	společnost	k1gFnSc2	společnost
z	z	k7c2	z
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
klesl	klesnout	k5eAaPmAgInS	klesnout
počet	počet	k1gInSc1	počet
gepardů	gepard	k1gMnPc2	gepard
na	na	k7c4	na
pouhých	pouhý	k2eAgInPc2d1	pouhý
7100	[number]	k4	7100
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
i	i	k9	i
nadále	nadále	k6eAd1	nadále
rapidně	rapidně	k6eAd1	rapidně
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
gepard	gepard	k1gMnSc1	gepard
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
italského	italský	k2eAgInSc2d1	italský
gattopardo	gattoparda	k1gMnSc5	gattoparda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
kompozitum	kompozitum	k1gNnSc4	kompozitum
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
gatto	gatto	k1gNnSc1	gatto
"	"	kIx"	"
<g/>
kocour	kocour	k1gInSc1	kocour
<g/>
"	"	kIx"	"
a	a	k8xC	a
pardo	pardo	k1gNnSc1	pardo
"	"	kIx"	"
<g/>
pardál	pardál	k1gMnSc1	pardál
<g/>
,	,	kIx,	,
levhart	levhart	k1gMnSc1	levhart
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
cheetah	cheetah	k1gInSc1	cheetah
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
hindského	hindský	k2eAgInSc2d1	hindský
číta	čít	k1gInSc2	čít
a	a	k8xC	a
sanskrtského	sanskrtský	k2eAgInSc2d1	sanskrtský
čitraká	čitraký	k2eAgNnPc1d1	čitraký
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
skvrnitý	skvrnitý	k2eAgMnSc1d1	skvrnitý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
názvem	název	k1gInSc7	název
gepard	gepard	k1gMnSc1	gepard
africký	africký	k2eAgMnSc1d1	africký
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
gepardi	gepard	k1gMnPc1	gepard
žijí	žít	k5eAaImIp3nP	žít
mimo	mimo	k7c4	mimo
Afriku	Afrika	k1gFnSc4	Afrika
i	i	k9	i
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
21	[number]	k4	21
<g/>
–	–	k?	–
<g/>
72	[number]	k4	72
kg	kg	kA	kg
Délka	délka	k1gFnSc1	délka
<g/>
:	:	kIx,	:
105	[number]	k4	105
<g/>
–	–	k?	–
<g/>
152	[number]	k4	152
cm	cm	kA	cm
+	+	kIx~	+
65	[number]	k4	65
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
ocas	ocas	k1gInSc1	ocas
Výška	výška	k1gFnSc1	výška
<g/>
:	:	kIx,	:
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
94	[number]	k4	94
cm	cm	kA	cm
Gepard	gepard	k1gMnSc1	gepard
štíhlý	štíhlý	k2eAgMnSc1d1	štíhlý
se	se	k3xPyFc4	se
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
typického	typický	k2eAgNnSc2d1	typické
tělesného	tělesný	k2eAgNnSc2d1	tělesné
schématu	schéma	k1gNnSc2	schéma
kočkovité	kočkovitý	k2eAgFnSc2d1	kočkovitá
šelmy	šelma	k1gFnSc2	šelma
<g/>
,	,	kIx,	,
některými	některý	k3yIgInPc7	některý
znaky	znak	k1gInPc7	znak
připomíná	připomínat	k5eAaImIp3nS	připomínat
spíše	spíše	k9	spíše
šelmy	šelma	k1gFnPc4	šelma
psovité	psovitý	k2eAgFnPc4d1	psovitá
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
malou	malý	k2eAgFnSc4d1	malá
<g/>
,	,	kIx,	,
kulatou	kulatý	k2eAgFnSc4d1	kulatá
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
končetiny	končetina	k1gFnPc4	končetina
i	i	k8xC	i
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Stavbou	stavba	k1gFnSc7	stavba
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
chrtům	chrt	k1gMnPc3	chrt
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgInSc7d1	známý
faktem	fakt	k1gInSc7	fakt
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
kočkovitých	kočkovitý	k2eAgFnPc2d1	kočkovitá
šelem	šelma	k1gFnPc2	šelma
nemá	mít	k5eNaImIp3nS	mít
úplně	úplně	k6eAd1	úplně
zatažitelné	zatažitelný	k2eAgInPc4d1	zatažitelný
drápy	dráp	k1gInPc4	dráp
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgMnPc1d2	veliký
než	než	k8xS	než
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
zlatohnědá	zlatohnědý	k2eAgFnSc1d1	zlatohnědá
<g/>
,	,	kIx,	,
tváře	tvář	k1gFnPc1	tvář
a	a	k8xC	a
břicho	břicho	k1gNnSc1	břicho
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
.	.	kIx.	.
</s>
<s>
Gepardí	Gepardí	k1gNnSc1	Gepardí
kresba	kresba	k1gFnSc1	kresba
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
černých	černý	k2eAgFnPc2d1	černá
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
levharta	levhart	k1gMnSc2	levhart
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
kresba	kresba	k1gFnSc1	kresba
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
černými	černý	k2eAgFnPc7d1	černá
rozetami	rozeta	k1gFnPc7	rozeta
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgMnSc7d1	typický
gepardím	gepardit	k5eAaPmIp1nS	gepardit
znakem	znak	k1gInSc7	znak
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgInPc4d1	černý
pruhy	pruh	k1gInPc4	pruh
táhnoucí	táhnoucí	k2eAgInPc4d1	táhnoucí
se	se	k3xPyFc4	se
z	z	k7c2	z
koutku	koutek	k1gInSc2	koutek
oka	oko	k1gNnSc2	oko
až	až	k9	až
k	k	k7c3	k
tlamě	tlama	k1gFnSc3	tlama
-	-	kIx~	-
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
zoology	zoolog	k1gMnPc4	zoolog
nazýván	nazýván	k2eAgInSc4d1	nazýván
"	"	kIx"	"
<g/>
kočka	kočka	k1gFnSc1	kočka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pláče	plakat	k5eAaImIp3nS	plakat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Koťata	kotě	k1gNnPc1	kotě
jsou	být	k5eAaImIp3nP	být
zbarvena	zbarvit	k5eAaPmNgNnP	zbarvit
nenápadně	nápadně	k6eNd1	nápadně
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
mají	mít	k5eAaImIp3nP	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
světlou	světlý	k2eAgFnSc4d1	světlá
hřívu	hříva	k1gFnSc4	hříva
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
během	během	k7c2	během
dospívání	dospívání	k1gNnSc2	dospívání
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
královští	královský	k2eAgMnPc1d1	královský
gepardi	gepard	k1gMnPc1	gepard
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jedinci	jedinec	k1gMnPc1	jedinec
s	s	k7c7	s
neobvyklou	obvyklý	k2eNgFnSc7d1	neobvyklá
kresbou	kresba	k1gFnSc7	kresba
-	-	kIx~	-
místo	místo	k1gNnSc4	místo
malých	malý	k2eAgFnPc2d1	malá
černých	černý	k2eAgFnPc2d1	černá
teček	tečka	k1gFnPc2	tečka
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
kresba	kresba	k1gFnSc1	kresba
tvořena	tvořit	k5eAaImNgFnS	tvořit
velkými	velký	k2eAgFnPc7d1	velká
<g/>
,	,	kIx,	,
splývajícími	splývající	k2eAgFnPc7d1	splývající
skvrnami	skvrna	k1gFnPc7	skvrna
a	a	k8xC	a
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dlouho	dlouho	k6eAd1	dlouho
byli	být	k5eAaImAgMnP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
mýtické	mýtický	k2eAgNnSc4d1	mýtické
zvíře	zvíře	k1gNnSc4	zvíře
nebo	nebo	k8xC	nebo
za	za	k7c4	za
nový	nový	k2eAgInSc4d1	nový
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
neznámý	známý	k2eNgInSc4d1	neznámý
druh	druh	k1gInSc4	druh
kočkovité	kočkovitý	k2eAgFnSc2d1	kočkovitá
šelmy	šelma	k1gFnSc2	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
se	se	k3xPyFc4	se
v	v	k7c6	v
jihoafrickém	jihoafrický	k2eAgInSc6d1	jihoafrický
Krugerově	Krugerův	k2eAgInSc6d1	Krugerův
parku	park	k1gInSc6	park
podařilo	podařit	k5eAaPmAgNnS	podařit
kryptozoologům	kryptozoolog	k1gMnPc3	kryptozoolog
Paulovi	Paul	k1gMnSc6	Paul
a	a	k8xC	a
Leně	Lena	k1gFnSc6	Lena
Bottriellovým	Bottriellová	k1gFnPc3	Bottriellová
pozorovat	pozorovat	k5eAaImF	pozorovat
a	a	k8xC	a
vyfotografovat	vyfotografovat	k5eAaPmF	vyfotografovat
královského	královský	k2eAgMnSc4d1	královský
geparda	gepard	k1gMnSc4	gepard
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
křížence	kříženka	k1gFnSc6	kříženka
mezi	mezi	k7c7	mezi
gepardem	gepard	k1gMnSc7	gepard
a	a	k8xC	a
levhartem	levhart	k1gMnSc7	levhart
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vyvrácena	vyvrátit	k5eAaPmNgFnS	vyvrátit
po	po	k7c6	po
provedení	provedení	k1gNnSc6	provedení
genetických	genetický	k2eAgInPc2d1	genetický
testů	test	k1gInPc2	test
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tzv.	tzv.	kA	tzv.
královský	královský	k2eAgMnSc1d1	královský
gepard	gepard	k1gMnSc1	gepard
je	být	k5eAaImIp3nS	být
barevnou	barevný	k2eAgFnSc7d1	barevná
odchylkou	odchylka	k1gFnSc7	odchylka
<g/>
,	,	kIx,	,
nesenou	nesený	k2eAgFnSc7d1	nesená
recesivní	recesivní	k2eAgFnSc7d1	recesivní
alelou	alela	k1gFnSc7	alela
pro	pro	k7c4	pro
mramorované	mramorovaný	k2eAgNnSc4d1	mramorované
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
mramorovaná	mramorovaný	k2eAgFnSc1d1	mramorovaná
(	(	kIx(	(
<g/>
=	=	kIx~	=
<g/>
mourovatá	mourovatý	k2eAgFnSc1d1	mourovatá
<g/>
)	)	kIx)	)
kresba	kresba	k1gFnSc1	kresba
domácích	domácí	k2eAgFnPc2d1	domácí
koček	kočka	k1gFnPc2	kočka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
první	první	k4xOgMnSc1	první
královský	královský	k2eAgMnSc1d1	královský
gepard	gepard	k1gMnSc1	gepard
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
nesprávně	správně	k6eNd1	správně
<g/>
,	,	kIx,	,
královští	královský	k2eAgMnPc1d1	královský
gepardi	gepard	k1gMnPc1	gepard
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
poddruh	poddruh	k1gInSc4	poddruh
<g/>
,	,	kIx,	,
A.	A.	kA	A.
j.	j.	k?	j.
rex	rex	k?	rex
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
areál	areál	k1gInSc1	areál
geparda	gepard	k1gMnSc2	gepard
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
,	,	kIx,	,
Arabský	arabský	k2eAgInSc4d1	arabský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
,	,	kIx,	,
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
a	a	k8xC	a
Indii	Indie	k1gFnSc4	Indie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
především	především	k9	především
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc6d1	západní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
nežijí	žít	k5eNaImIp3nP	žít
<g/>
,	,	kIx,	,
gepard	gepard	k1gMnSc1	gepard
středoasijský	středoasijský	k2eAgMnSc1d1	středoasijský
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
vyhuben	vyhubit	k5eAaPmNgMnS	vyhubit
<g/>
,	,	kIx,	,
gepard	gepard	k1gMnSc1	gepard
indický	indický	k2eAgMnSc1d1	indický
je	být	k5eAaImIp3nS	být
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
populace	populace	k1gFnSc1	populace
(	(	kIx(	(
<g/>
max	max	kA	max
<g/>
.	.	kIx.	.
200	[number]	k4	200
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
snad	snad	k9	snad
ještě	ještě	k6eAd1	ještě
přežívá	přežívat	k5eAaImIp3nS	přežívat
v	v	k7c6	v
nedostupných	dostupný	k2eNgFnPc6d1	nedostupná
oblastech	oblast	k1gFnPc6	oblast
Íránu	Írán	k1gInSc2	Írán
a	a	k8xC	a
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
populace	populace	k1gFnSc1	populace
gepardů	gepard	k1gMnPc2	gepard
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Namibii	Namibie	k1gFnSc6	Namibie
<g/>
,	,	kIx,	,
2000-3000	[number]	k4	2000-3000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Gepardi	gepard	k1gMnPc1	gepard
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
kočkami	kočka	k1gFnPc7	kočka
jedineční	jedinečný	k2eAgMnPc1d1	jedinečný
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kořist	kořist	k1gFnSc4	kořist
štvou	štvát	k5eAaImIp3nP	štvát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
znak	znak	k1gInSc1	znak
jinak	jinak	k6eAd1	jinak
spíše	spíše	k9	spíše
typický	typický	k2eAgMnSc1d1	typický
pro	pro	k7c4	pro
psovité	psovití	k1gMnPc4	psovití
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
aktivní	aktivní	k2eAgMnPc1d1	aktivní
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vyhnuli	vyhnout	k5eAaPmAgMnP	vyhnout
jiným	jiný	k2eAgFnPc3d1	jiná
<g/>
,	,	kIx,	,
silnějším	silný	k2eAgFnPc3d2	silnější
šelmám	šelma	k1gFnPc3	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Gepardi	gepard	k1gMnPc1	gepard
jsou	být	k5eAaImIp3nP	být
stvořeni	stvořit	k5eAaPmNgMnP	stvořit
pro	pro	k7c4	pro
rychlý	rychlý	k2eAgInSc4d1	rychlý
běh	běh	k1gInSc4	běh
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
neobyčejně	obyčejně	k6eNd1	obyčejně
ohebnou	ohebný	k2eAgFnSc4d1	ohebná
páteř	páteř	k1gFnSc4	páteř
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
při	při	k7c6	při
běhu	běh	k1gInSc6	běh
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
pružina	pružina	k1gFnSc1	pružina
<g/>
.	.	kIx.	.
</s>
<s>
Gepard	gepard	k1gMnSc1	gepard
se	se	k3xPyFc4	se
při	při	k7c6	při
běhu	běh	k1gInSc6	běh
prakticky	prakticky	k6eAd1	prakticky
nedotýká	dotýkat	k5eNaImIp3nS	dotýkat
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
až	až	k9	až
7	[number]	k4	7
m	m	kA	m
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
skoky	skok	k1gInPc7	skok
<g/>
.	.	kIx.	.
</s>
<s>
Drápy	dráp	k1gInPc4	dráp
mu	on	k3xPp3gMnSc3	on
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k8xC	jako
hřeby	hřeb	k1gInPc4	hřeb
treter	tretra	k1gFnPc2	tretra
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
hrubými	hrubý	k2eAgInPc7d1	hrubý
chlupy	chlup	k1gInPc7	chlup
na	na	k7c6	na
chodidlech	chodidlo	k1gNnPc6	chodidlo
mu	on	k3xPp3gMnSc3	on
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
co	co	k9	co
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
odrazy	odraz	k1gInPc4	odraz
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
při	při	k7c6	při
běhu	běh	k1gInSc6	běh
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
kormidlo	kormidlo	k1gNnSc1	kormidlo
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
manévrovat	manévrovat	k5eAaImF	manévrovat
i	i	k9	i
při	při	k7c6	při
vysoké	vysoký	k2eAgFnSc6d1	vysoká
rychlosti	rychlost	k1gFnSc6	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Gepardi	gepard	k1gMnPc1	gepard
kořist	kořist	k1gFnSc4	kořist
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
zrakem	zrak	k1gInSc7	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
připlížit	připlížit	k5eAaPmF	připlížit
se	se	k3xPyFc4	se
co	co	k9	co
nejblíže	blízce	k6eAd3	blízce
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
pak	pak	k6eAd1	pak
rychle	rychle	k6eAd1	rychle
vyrazí	vyrazit	k5eAaPmIp3nP	vyrazit
<g/>
.	.	kIx.	.
</s>
<s>
Běh	běh	k1gInSc1	běh
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
velice	velice	k6eAd1	velice
vyčerpávající	vyčerpávající	k2eAgInSc1d1	vyčerpávající
<g/>
,	,	kIx,	,
gepard	gepard	k1gMnSc1	gepard
se	se	k3xPyFc4	se
přehřívá	přehřívat	k5eAaImIp3nS	přehřívat
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
unavuje	unavovat	k5eAaImIp3nS	unavovat
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
běžet	běžet	k5eAaImF	běžet
rychlostí	rychlost	k1gFnSc7	rychlost
okolo	okolo	k7c2	okolo
100	[number]	k4	100
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Rychlosti	rychlost	k1gFnSc2	rychlost
nad	nad	k7c4	nad
110	[number]	k4	110
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
mýtem	mýtus	k1gInSc7	mýtus
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
podstatně	podstatně	k6eAd1	podstatně
zajímavější	zajímavý	k2eAgFnSc1d2	zajímavější
je	být	k5eAaImIp3nS	být
neuvěřitelná	uvěřitelný	k2eNgFnSc1d1	neuvěřitelná
možnost	možnost	k1gFnSc1	možnost
měnit	měnit	k5eAaImF	měnit
akceleraci	akcelerace	k1gFnSc4	akcelerace
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
jednoznačně	jednoznačně	k6eAd1	jednoznačně
dělá	dělat	k5eAaImIp3nS	dělat
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejhbitějších	hbitý	k2eAgMnPc2d3	nejhbitější
savců	savec	k1gMnPc2	savec
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ale	ale	k9	ale
kořist	kořist	k1gFnSc4	kořist
do	do	k7c2	do
10-20	[number]	k4	10-20
sekund	sekunda	k1gFnPc2	sekunda
nedostihne	dostihnout	k5eNaPmIp3nS	dostihnout
<g/>
,	,	kIx,	,
lov	lov	k1gInSc1	lov
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
gepard	gepard	k1gMnSc1	gepard
nejúspěšnější	úspěšný	k2eAgMnSc1d3	nejúspěšnější
samotářský	samotářský	k2eAgMnSc1d1	samotářský
lovec	lovec	k1gMnSc1	lovec
<g/>
,	,	kIx,	,
až	až	k9	až
70	[number]	k4	70
%	%	kIx~	%
jeho	jeho	k3xOp3gInPc2	jeho
loveckých	lovecký	k2eAgInPc2d1	lovecký
pokusů	pokus	k1gInPc2	pokus
končí	končit	k5eAaImIp3nS	končit
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
gepard	gepard	k1gMnSc1	gepard
chován	chován	k2eAgMnSc1d1	chován
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
zhruba	zhruba	k6eAd1	zhruba
jen	jen	k9	jen
poloviční	poloviční	k2eAgFnSc3d1	poloviční
rychlosti	rychlost	k1gFnSc3	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
rychlosti	rychlost	k1gFnSc2	rychlost
s	s	k7c7	s
GPS	GPS	kA	GPS
obojkem	obojek	k1gInSc7	obojek
GPS	GPS	kA	GPS
obojky	obojek	k1gInPc1	obojek
odhalily	odhalit	k5eAaPmAgInP	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
gepardi	gepard	k1gMnPc1	gepard
obvykle	obvykle	k6eAd1	obvykle
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
maximální	maximální	k2eAgFnSc4d1	maximální
rychlost	rychlost	k1gFnSc4	rychlost
nespoléhají	spoléhat	k5eNaImIp3nP	spoléhat
<g/>
.	.	kIx.	.
</s>
<s>
Zvířatům	zvíře	k1gNnPc3	zvíře
byla	být	k5eAaImAgFnS	být
naměřena	naměřen	k2eAgFnSc1d1	naměřena
okamžitá	okamžitý	k2eAgFnSc1d1	okamžitá
rychlost	rychlost	k1gFnSc1	rychlost
až	až	k9	až
93	[number]	k4	93
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
367	[number]	k4	367
pokusů	pokus	k1gInPc2	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
rychlost	rychlost	k1gFnSc1	rychlost
dosažená	dosažený	k2eAgFnSc1d1	dosažená
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
byla	být	k5eAaImAgFnS	být
zhruba	zhruba	k6eAd1	zhruba
55	[number]	k4	55
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jedné	jeden	k4xCgFnSc2	jeden
až	až	k9	až
dvou	dva	k4xCgFnPc2	dva
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Gepard	gepard	k1gMnSc1	gepard
loví	lovit	k5eAaImIp3nS	lovit
převážně	převážně	k6eAd1	převážně
malé	malý	k2eAgInPc4d1	malý
kopytníky	kopytník	k1gInPc4	kopytník
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
gazela	gazela	k1gFnSc1	gazela
Thomsonova	Thomsonův	k2eAgFnSc1d1	Thomsonova
<g/>
,	,	kIx,	,
gazela	gazela	k1gFnSc1	gazela
Grantova	Grantův	k2eAgFnSc1d1	Grantova
nebo	nebo	k8xC	nebo
impala	impala	k1gFnSc1	impala
<g/>
.	.	kIx.	.
</s>
<s>
Příležitostně	příležitostně	k6eAd1	příležitostně
uloví	ulovit	k5eAaPmIp3nS	ulovit
i	i	k9	i
drobnější	drobný	k2eAgMnPc4d2	drobnější
savce	savec	k1gMnPc4	savec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zabití	zabití	k1gNnSc6	zabití
kořisti	kořist	k1gFnSc2	kořist
dlouho	dlouho	k6eAd1	dlouho
odpočívá	odpočívat	k5eAaImIp3nS	odpočívat
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
zkonzumovat	zkonzumovat	k5eAaPmF	zkonzumovat
najednou	najednou	k6eAd1	najednou
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
kořist	kořist	k1gFnSc4	kořist
často	často	k6eAd1	často
přichází	přicházet	k5eAaImIp3nS	přicházet
<g/>
,	,	kIx,	,
lvi	lev	k1gMnPc1	lev
nebo	nebo	k8xC	nebo
hyeny	hyena	k1gFnPc1	hyena
ho	on	k3xPp3gNnSc2	on
snadno	snadno	k6eAd1	snadno
odeženou	odehnat	k5eAaPmIp3nP	odehnat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
gepard	gepard	k1gMnSc1	gepard
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
kořisti	kořist	k1gFnSc3	kořist
nikdy	nikdy	k6eAd1	nikdy
nevrací	vracet	k5eNaImIp3nS	vracet
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
žerou	žrát	k5eAaImIp3nP	žrát
mršiny	mršin	k2eAgFnPc1d1	mršina
<g/>
.	.	kIx.	.
</s>
<s>
Gepardi	gepard	k1gMnPc1	gepard
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
pářit	pářit	k5eAaImF	pářit
celoročně	celoročně	k6eAd1	celoročně
<g/>
,	,	kIx,	,
k	k	k7c3	k
námluvám	námluva	k1gFnPc3	námluva
ale	ale	k8xC	ale
dochází	docházet	k5eAaImIp3nS	docházet
především	především	k9	především
v	v	k7c6	v
období	období	k1gNnSc6	období
dešťů	dešť	k1gInPc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
březí	březí	k2eAgFnSc1d1	březí
90-98	[number]	k4	90-98
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
v	v	k7c6	v
hnízdě	hnízdo	k1gNnSc6	hnízdo
z	z	k7c2	z
trávy	tráva	k1gFnSc2	tráva
vrhne	vrhnout	k5eAaPmIp3nS	vrhnout
3	[number]	k4	3
až	až	k8xS	až
6	[number]	k4	6
koťata	kotě	k1gNnPc1	kotě
<g/>
.	.	kIx.	.
</s>
<s>
Mortalita	mortalita	k1gFnSc1	mortalita
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
protože	protože	k8xS	protože
lvi	lev	k1gMnPc1	lev
<g/>
,	,	kIx,	,
hyeny	hyena	k1gFnPc1	hyena
nebo	nebo	k8xC	nebo
paviáni	pavián	k1gMnPc1	pavián
je	on	k3xPp3gMnPc4	on
zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	on	k3xPp3gMnPc4	on
najdou	najít	k5eAaPmIp3nP	najít
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
proto	proto	k8xC	proto
koťata	kotě	k1gNnPc4	kotě
často	často	k6eAd1	často
přenáší	přenášet	k5eAaImIp3nS	přenášet
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ale	ale	k9	ale
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
věku	věk	k1gInSc2	věk
přežije	přežít	k5eAaPmIp3nS	přežít
jen	jen	k9	jen
5	[number]	k4	5
%	%	kIx~	%
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
je	on	k3xPp3gInPc4	on
odstaví	odstavit	k5eAaPmIp3nS	odstavit
ve	v	k7c6	v
4	[number]	k4	4
měsících	měsíc	k1gInPc6	měsíc
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
mláďata	mládě	k1gNnPc1	mládě
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
a	a	k8xC	a
půl	půl	k1xP	půl
věku	věk	k1gInSc2	věk
a	a	k8xC	a
učí	učit	k5eAaImIp3nS	učit
se	se	k3xPyFc4	se
lovit	lovit	k5eAaImF	lovit
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
letech	let	k1gInPc6	let
<g/>
,	,	kIx,	,
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
stáří	stáří	k1gNnSc3	stáří
až	až	k9	až
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
až	až	k9	až
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
gepardi	gepard	k1gMnPc1	gepard
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
velmi	velmi	k6eAd1	velmi
špatně	špatně	k6eAd1	špatně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
jsou	být	k5eAaImIp3nP	být
chováni	chovat	k5eAaImNgMnP	chovat
v	v	k7c4	v
ZOO	zoo	k1gFnSc4	zoo
Dvůr	Dvůr	k1gInSc4	Dvůr
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Liberec	Liberec	k1gInSc1	Liberec
a	a	k8xC	a
Zoo	zoo	k1gFnPc1	zoo
Chleby	chléb	k1gInPc4	chléb
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zařazeni	zařadit	k5eAaPmNgMnP	zařadit
do	do	k7c2	do
EEP	EEP	kA	EEP
<g/>
.	.	kIx.	.
</s>
<s>
Gepardi	gepard	k1gMnPc1	gepard
nejsou	být	k5eNaImIp3nP	být
takoví	takový	k3xDgMnPc1	takový
samotáři	samotář	k1gMnPc1	samotář
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
kočkovité	kočkovitý	k2eAgFnPc1d1	kočkovitá
šelmy	šelma	k1gFnPc1	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
dlouho	dlouho	k6eAd1	dlouho
s	s	k7c7	s
odrostlými	odrostlý	k2eAgNnPc7d1	odrostlé
koťaty	kotě	k1gNnPc7	kotě
<g/>
,	,	kIx,	,
sourozenci	sourozenec	k1gMnSc3	sourozenec
také	také	k9	také
často	často	k6eAd1	často
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
spolu	spolu	k6eAd1	spolu
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
tvoří	tvořit	k5eAaImIp3nP	tvořit
skupiny	skupina	k1gFnPc4	skupina
o	o	k7c6	o
dvou	dva	k4xCgNnPc2	dva
až	až	k9	až
pěti	pět	k4xCc2	pět
jedincích	jedinec	k1gMnPc6	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nežijí	žít	k5eNaImIp3nP	žít
žádné	žádný	k3yNgFnPc4	žádný
další	další	k2eAgFnPc4d1	další
velké	velký	k2eAgFnPc4d1	velká
šelmy	šelma	k1gFnPc4	šelma
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
skupiny	skupina	k1gFnPc1	skupina
i	i	k9	i
19	[number]	k4	19
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Koalice	koalice	k1gFnSc1	koalice
gepardů	gepard	k1gMnPc2	gepard
jsou	být	k5eAaImIp3nP	být
úspěšnější	úspěšný	k2eAgMnPc1d2	úspěšnější
v	v	k7c6	v
lovu	lov	k1gInSc6	lov
<g/>
,	,	kIx,	,
lépe	dobře	k6eAd2	dobře
si	se	k3xPyFc3	se
hájí	hájit	k5eAaImIp3nP	hájit
teritorium	teritorium	k1gNnSc4	teritorium
a	a	k8xC	a
nacházejí	nacházet	k5eAaImIp3nP	nacházet
partnery	partner	k1gMnPc7	partner
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
Persii	Persie	k1gFnSc6	Persie
a	a	k8xC	a
Indii	Indie	k1gFnSc6	Indie
byli	být	k5eAaImAgMnP	být
cvičeni	cvičit	k5eAaImNgMnP	cvičit
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
gazel	gazela	k1gFnPc2	gazela
<g/>
.	.	kIx.	.
</s>
<s>
Lovci	lovec	k1gMnPc1	lovec
přivezli	přivézt	k5eAaPmAgMnP	přivézt
gepardy	gepard	k1gMnPc7	gepard
na	na	k7c6	na
vozech	vůz	k1gInPc6	vůz
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
pasoucích	pasoucí	k2eAgFnPc2d1	pasoucí
se	se	k3xPyFc4	se
gazel	gazela	k1gFnPc2	gazela
či	či	k8xC	či
antilop	antilopa	k1gFnPc2	antilopa
a	a	k8xC	a
tam	tam	k6eAd1	tam
je	on	k3xPp3gMnPc4	on
vypustili	vypustit	k5eAaPmAgMnP	vypustit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
gepard	gepard	k1gMnSc1	gepard
kořist	kořist	k1gFnSc4	kořist
strhl	strhnout	k5eAaPmAgMnS	strhnout
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
za	za	k7c4	za
odměnu	odměna	k1gFnSc4	odměna
gazelí	gazelí	k2eAgFnSc4d1	gazelí
krev	krev	k1gFnSc4	krev
a	a	k8xC	a
vnitřnosti	vnitřnost	k1gFnPc4	vnitřnost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Persie	Persie	k1gFnSc2	Persie
se	se	k3xPyFc4	se
lov	lov	k1gInSc1	lov
s	s	k7c7	s
gepardy	gepard	k1gMnPc7	gepard
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
i	i	k9	i
do	do	k7c2	do
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
gepardi	gepard	k1gMnPc1	gepard
nikdy	nikdy	k6eAd1	nikdy
volně	volně	k6eAd1	volně
nežili	žít	k5eNaImAgMnP	žít
<g/>
,	,	kIx,	,
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
byl	být	k5eAaImAgInS	být
výsadou	výsada	k1gFnSc7	výsada
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Lov	lov	k1gInSc1	lov
s	s	k7c7	s
gepardy	gepard	k1gMnPc7	gepard
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
Kublaje	Kublaje	k1gFnSc2	Kublaje
<g/>
,	,	kIx,	,
mongolského	mongolský	k2eAgMnSc2d1	mongolský
vládce	vládce	k1gMnSc2	vládce
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
popsal	popsat	k5eAaPmAgMnS	popsat
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
<g/>
.	.	kIx.	.
</s>
<s>
Indický	indický	k2eAgMnSc1d1	indický
vládce	vládce	k1gMnSc1	vládce
Akbar	Akbar	k1gMnSc1	Akbar
choval	chovat	k5eAaImAgMnS	chovat
kolem	kolem	k6eAd1	kolem
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1570	[number]	k4	1570
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
dvoře	dvůr	k1gInSc6	dvůr
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
gepardů	gepard	k1gMnPc2	gepard
a	a	k8xC	a
lovil	lovit	k5eAaImAgMnS	lovit
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
hlavně	hlavně	k9	hlavně
antilopy	antilopa	k1gFnPc4	antilopa
jelení	jelení	k2eAgFnPc4d1	jelení
<g/>
.	.	kIx.	.
</s>
<s>
Gepardi	gepard	k1gMnPc1	gepard
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
volně	volně	k6eAd1	volně
pohybovat	pohybovat	k5eAaImF	pohybovat
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
nejsou	být	k5eNaImIp3nP	být
domestikovaným	domestikovaný	k2eAgNnSc7d1	domestikované
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
jen	jen	k9	jen
vzácně	vzácně	k6eAd1	vzácně
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
ochočených	ochočený	k2eAgMnPc2d1	ochočený
gepardů	gepard	k1gMnPc2	gepard
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
volné	volný	k2eAgFnSc2d1	volná
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
renesance	renesance	k1gFnSc2	renesance
a	a	k8xC	a
baroka	baroko	k1gNnSc2	baroko
lovili	lovit	k5eAaImAgMnP	lovit
s	s	k7c7	s
gepardy	gepard	k1gMnPc7	gepard
i	i	k8xC	i
někteří	některý	k3yIgMnPc1	některý
evropští	evropský	k2eAgMnPc1d1	evropský
panovníci	panovník	k1gMnPc1	panovník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
je	on	k3xPp3gNnSc4	on
získali	získat	k5eAaPmAgMnP	získat
darem	dar	k1gInSc7	dar
z	z	k7c2	z
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
či	či	k8xC	či
z	z	k7c2	z
Persie	Persie	k1gFnSc2	Persie
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
např.	např.	kA	např.
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Navarrský	navarrský	k2eAgMnSc1d1	navarrský
nebo	nebo	k8xC	nebo
římskoněmecký	římskoněmecký	k2eAgMnSc1d1	římskoněmecký
císař	císař	k1gMnSc1	císař
Leopold	Leopolda	k1gFnPc2	Leopolda
I.	I.	kA	I.
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
Íránu	Írán	k1gInSc2	Írán
se	se	k3xPyFc4	se
lov	lov	k1gInSc1	lov
s	s	k7c7	s
gepardy	gepard	k1gMnPc7	gepard
udržel	udržet	k5eAaPmAgMnS	udržet
ještě	ještě	k9	ještě
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
však	však	k9	však
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
provozován	provozovat	k5eAaImNgInS	provozovat
<g/>
.	.	kIx.	.
</s>
<s>
Vědecké	vědecký	k2eAgNnSc1d1	vědecké
rodové	rodový	k2eAgNnSc1d1	rodové
jméno	jméno	k1gNnSc1	jméno
geparda	gepard	k1gMnSc2	gepard
<g/>
,	,	kIx,	,
Acinonyx	Acinonyx	k1gInSc1	Acinonyx
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
nepohyblivý	pohyblivý	k2eNgInSc4d1	nepohyblivý
dráp	dráp	k1gInSc4	dráp
<g/>
,	,	kIx,	,
druhové	druhový	k2eAgNnSc4d1	druhové
jméno	jméno	k1gNnSc4	jméno
jubatus	jubatus	k1gInSc4	jubatus
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
latinsky	latinsky	k6eAd1	latinsky
hřívnatý	hřívnatý	k2eAgInSc1d1	hřívnatý
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
pojmenování	pojmenování	k1gNnSc1	pojmenování
gepard	gepard	k1gMnSc1	gepard
pak	pak	k6eAd1	pak
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
středověkého	středověký	k2eAgNnSc2d1	středověké
latinského	latinský	k2eAgNnSc2d1	latinské
jména	jméno	k1gNnSc2	jméno
gattus-pardus	gattusardus	k1gInSc1	gattus-pardus
<g/>
,	,	kIx,	,
kočka-levhart	kočkaevhart	k1gInSc1	kočka-levhart
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byli	být	k5eAaImAgMnP	být
gepardi	gepard	k1gMnPc1	gepard
řazeni	řadit	k5eAaImNgMnP	řadit
do	do	k7c2	do
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
podčeledi	podčeleď	k1gFnSc2	podčeleď
kočkovitých	kočkovitý	k2eAgFnPc2d1	kočkovitá
šelem	šelma	k1gFnPc2	šelma
Acinonychinae	Acinonychina	k1gInSc2	Acinonychina
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
malým	malý	k2eAgFnPc3d1	malá
kočkám	kočka	k1gFnPc3	kočka
<g/>
,	,	kIx,	,
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
pumy	puma	k1gFnSc2	puma
americké	americký	k2eAgFnSc2d1	americká
a	a	k8xC	a
jaguarundi	jaguarund	k1gMnPc1	jaguarund
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
velkých	velký	k2eAgFnPc2d1	velká
koček	kočka	k1gFnPc2	kočka
neumějí	umět	k5eNaImIp3nP	umět
řvát	řvát	k5eAaImF	řvát
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
však	však	k9	však
dokážou	dokázat	k5eAaPmIp3nP	dokázat
příst	příst	k5eAaImF	příst
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
malé	malý	k2eAgFnPc1d1	malá
kočky	kočka	k1gFnPc1	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Gepard	gepard	k1gMnSc1	gepard
je	být	k5eAaImIp3nS	být
nelegálně	legálně	k6eNd1	legálně
loven	lovit	k5eAaImNgInS	lovit
kvůli	kvůli	k7c3	kvůli
atraktivní	atraktivní	k2eAgFnSc3d1	atraktivní
kožešině	kožešina	k1gFnSc3	kožešina
<g/>
,	,	kIx,	,
také	také	k9	také
je	být	k5eAaImIp3nS	být
pronásledován	pronásledovat	k5eAaImNgMnS	pronásledovat
zemědělci	zemědělec	k1gMnPc7	zemědělec
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nárůstu	nárůst	k1gInSc3	nárůst
lidské	lidský	k2eAgFnSc2d1	lidská
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
zatlačován	zatlačovat	k5eAaImNgInS	zatlačovat
do	do	k7c2	do
rezervací	rezervace	k1gFnPc2	rezervace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgMnPc6	který
žijí	žít	k5eAaImIp3nP	žít
také	také	k9	také
lvi	lev	k1gMnPc1	lev
a	a	k8xC	a
hyeny	hyena	k1gFnPc1	hyena
<g/>
,	,	kIx,	,
po	po	k7c6	po
člověku	člověk	k1gMnSc6	člověk
největší	veliký	k2eAgFnSc4d3	veliký
nepřátelé	nepřítel	k1gMnPc1	nepřítel
geparda	gepard	k1gMnSc4	gepard
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
gepardi	gepard	k1gMnPc1	gepard
ohroženi	ohrozit	k5eAaPmNgMnP	ohrozit
dalším	další	k2eAgInSc7d1	další
problémem	problém	k1gInSc7	problém
<g/>
:	:	kIx,	:
Jako	jako	k8xC	jako
efekt	efekt	k1gInSc4	efekt
hrdla	hrdlo	k1gNnSc2	hrdlo
lahve	lahev	k1gFnSc2	lahev
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nastane	nastat	k5eAaPmIp3nS	nastat
při	při	k7c6	při
prudkém	prudký	k2eAgInSc6d1	prudký
poklesu	pokles	k1gInSc6	pokles
počtu	počet	k1gInSc2	počet
jedinců	jedinec	k1gMnPc2	jedinec
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
příbuzenskému	příbuzenský	k2eAgNnSc3d1	příbuzenské
křížení	křížení	k1gNnSc3	křížení
a	a	k8xC	a
nevratnému	vratný	k2eNgNnSc3d1	nevratné
snížení	snížení	k1gNnSc3	snížení
genetické	genetický	k2eAgFnSc2d1	genetická
variability	variabilita	k1gFnSc2	variabilita
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumy	výzkum	k1gInPc1	výzkum
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
něčemu	něco	k3yInSc3	něco
takovému	takový	k3xDgNnSc3	takový
došlo	dojít	k5eAaPmAgNnS	dojít
u	u	k7c2	u
gepardů	gepard	k1gMnPc2	gepard
asi	asi	k9	asi
před	před	k7c7	před
10	[number]	k4	10
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
žijící	žijící	k2eAgMnPc1d1	žijící
gepardi	gepard	k1gMnPc1	gepard
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
vzájemně	vzájemně	k6eAd1	vzájemně
velmi	velmi	k6eAd1	velmi
příbuzní	příbuzný	k2eAgMnPc1d1	příbuzný
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
kožní	kožní	k2eAgInSc4d1	kožní
štěp	štěp	k1gInSc4	štěp
přenesený	přenesený	k2eAgInSc4d1	přenesený
z	z	k7c2	z
jednoho	jeden	k4xCgMnSc2	jeden
geparda	gepard	k1gMnSc2	gepard
na	na	k7c6	na
jiného	jiný	k2eAgMnSc4d1	jiný
není	být	k5eNaImIp3nS	být
odmítnut	odmítnut	k2eAgMnSc1d1	odmítnut
imunitním	imunitní	k2eAgInSc7d1	imunitní
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Vzory	vzor	k1gInPc1	vzor
kreseb	kresba	k1gFnPc2	kresba
gepardů	gepard	k1gMnPc2	gepard
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
koutů	kout	k1gInPc2	kout
Afriky	Afrika	k1gFnSc2	Afrika
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
prakticky	prakticky	k6eAd1	prakticky
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
příbuzenského	příbuzenský	k2eAgNnSc2d1	příbuzenské
křížení	křížení	k1gNnSc2	křížení
klesá	klesat	k5eAaImIp3nS	klesat
vitalita	vitalita	k1gFnSc1	vitalita
mláďat	mládě	k1gNnPc2	mládě
a	a	k8xC	a
oplozovací	oplozovací	k2eAgFnSc4d1	oplozovací
schopnost	schopnost	k1gFnSc4	schopnost
spermií	spermie	k1gFnPc2	spermie
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
spermií	spermie	k1gFnPc2	spermie
v	v	k7c6	v
ejakulátu	ejakulát	k1gInSc6	ejakulát
geparda	gepard	k1gMnSc2	gepard
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
90	[number]	k4	90
%	%	kIx~	%
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
u	u	k7c2	u
lva	lev	k1gMnSc2	lev
nebo	nebo	k8xC	nebo
tygra	tygr	k1gMnSc2	tygr
<g/>
,	,	kIx,	,
a	a	k8xC	a
75	[number]	k4	75
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
abnormální	abnormální	k2eAgMnSc1d1	abnormální
(	(	kIx(	(
<g/>
mají	mít	k5eAaImIp3nP	mít
dva	dva	k4xCgInPc4	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
bičíků	bičík	k1gInPc2	bičík
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc4	žádný
bičík	bičík	k1gInSc4	bičík
<g/>
,	,	kIx,	,
deformovanou	deformovaný	k2eAgFnSc4d1	deformovaná
hlavičku	hlavička	k1gFnSc4	hlavička
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Budoucnost	budoucnost	k1gFnSc1	budoucnost
gepardů	gepard	k1gMnPc2	gepard
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
značně	značně	k6eAd1	značně
nejistá	jistý	k2eNgFnSc1d1	nejistá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červené	červený	k2eAgFnSc6d1	červená
knize	kniha	k1gFnSc6	kniha
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
jsou	být	k5eAaImIp3nP	být
gepardi	gepard	k1gMnPc1	gepard
klasifikováni	klasifikován	k2eAgMnPc1d1	klasifikován
jako	jako	k8xS	jako
zranitelný	zranitelný	k2eAgInSc1d1	zranitelný
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
asijské	asijský	k2eAgInPc1d1	asijský
poddruhy	poddruh	k1gInPc1	poddruh
jsou	být	k5eAaImIp3nP	být
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgFnPc1d1	ohrožená
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
záchranného	záchranný	k2eAgInSc2d1	záchranný
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
se	se	k3xPyFc4	se
je	on	k3xPp3gMnPc4	on
daří	dařit	k5eAaImIp3nP	dařit
odchovat	odchovat	k5eAaPmF	odchovat
i	i	k9	i
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
umělého	umělý	k2eAgNnSc2d1	umělé
oplodnění	oplodnění	k1gNnSc2	oplodnění
<g/>
.	.	kIx.	.
</s>
<s>
Gepard	gepard	k1gMnSc1	gepard
je	být	k5eAaImIp3nS	být
zapsán	zapsat	k5eAaPmNgMnS	zapsat
v	v	k7c6	v
příloze	příloha	k1gFnSc6	příloha
I	I	kA	I
<g/>
,	,	kIx,	,
CITES	CITES	kA	CITES
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
studie	studie	k1gFnSc2	studie
Sarah	Sarah	k1gFnSc2	Sarah
Durantové	Durantový	k2eAgFnSc2d1	Durantová
z	z	k7c2	z
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
gepardů	gepard	k1gMnPc2	gepard
na	na	k7c6	na
světě	svět	k1gInSc6	svět
klesl	klesnout	k5eAaPmAgInS	klesnout
na	na	k7c4	na
7100	[number]	k4	7100
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
i	i	k9	i
nadále	nadále	k6eAd1	nadále
rapidně	rapidně	k6eAd1	rapidně
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
veliké	veliký	k2eAgFnSc3d1	veliká
poptávce	poptávka	k1gFnSc3	poptávka
po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
kožešině	kožešina	k1gFnSc6	kožešina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
žádoucím	žádoucí	k2eAgInSc7d1	žádoucí
artiklem	artikl	k1gInSc7	artikl
movitých	movitý	k2eAgMnPc2d1	movitý
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zoo	zoo	k1gFnSc1	zoo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
chovala	chovat	k5eAaImAgFnS	chovat
geparda	gepard	k1gMnSc4	gepard
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1829	[number]	k4	1829
Londýnská	londýnský	k2eAgFnSc1d1	londýnská
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
tento	tento	k3xDgMnSc1	tento
jedinec	jedinec	k1gMnSc1	jedinec
zde	zde	k6eAd1	zde
ale	ale	k8xC	ale
žil	žít	k5eAaImAgMnS	žít
méně	málo	k6eAd2	málo
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
se	se	k3xPyFc4	se
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
nečekaně	nečekaně	k6eAd1	nečekaně
narodila	narodit	k5eAaPmAgFnS	narodit
tři	tři	k4xCgNnPc4	tři
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
odchov	odchov	k1gInSc4	odchov
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
až	až	k9	až
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
ze	z	k7c2	z
zoo	zoo	k1gFnSc2	zoo
v	v	k7c6	v
Krefeldu	Krefeld	k1gInSc6	Krefeld
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
využita	využít	k5eAaPmNgFnS	využít
kočka	kočka	k1gFnSc1	kočka
domácí	domácí	k2eAgFnSc1d1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
přirozený	přirozený	k2eAgInSc1d1	přirozený
odchov	odchov	k1gInSc1	odchov
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
ze	z	k7c2	z
soukromého	soukromý	k2eAgInSc2d1	soukromý
chovu	chov	k1gInSc2	chov
doktora	doktor	k1gMnSc4	doktor
Spinelliho	Spinelli	k1gMnSc4	Spinelli
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
chovala	chovat	k5eAaImAgFnS	chovat
geparda	gepard	k1gMnSc4	gepard
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
pražská	pražský	k2eAgFnSc1d1	Pražská
zoo	zoo	k1gFnSc1	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
od	od	k7c2	od
cirkusu	cirkus	k1gInSc2	cirkus
Kludský	Kludský	k2eAgMnSc1d1	Kludský
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
samce	samec	k1gMnPc4	samec
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Mignon	Mignon	k1gInSc1	Mignon
<g/>
"	"	kIx"	"
možná	možná	k9	možná
indického	indický	k2eAgInSc2d1	indický
poddruhu	poddruh	k1gInSc2	poddruh
<g/>
,	,	kIx,	,
uhynul	uhynout	k5eAaPmAgMnS	uhynout
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
gepardi	gepard	k1gMnPc1	gepard
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
až	až	k9	až
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
podařil	podařit	k5eAaPmAgInS	podařit
první	první	k4xOgInSc1	první
odchov	odchov	k1gInSc1	odchov
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
narodilo	narodit	k5eAaPmAgNnS	narodit
38	[number]	k4	38
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
64	[number]	k4	64
gepardů	gepard	k1gMnPc2	gepard
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
narodilo	narodit	k5eAaPmAgNnS	narodit
také	také	k6eAd1	také
ve	v	k7c6	v
Dvoře	Dvůr	k1gInSc6	Dvůr
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
čtyři	čtyři	k4xCgNnPc1	čtyři
mláďata	mládě	k1gNnPc1	mládě
odchovala	odchovat	k5eAaPmAgNnP	odchovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
zoo	zoo	k1gNnSc7	zoo
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tuzemské	tuzemský	k2eAgMnPc4d1	tuzemský
chovatele	chovatel	k1gMnPc4	chovatel
gepardů	gepard	k1gMnPc2	gepard
patří	patřit	k5eAaImIp3nS	patřit
nebo	nebo	k8xC	nebo
patřila	patřit	k5eAaImAgFnS	patřit
také	také	k9	také
brněnská	brněnský	k2eAgFnSc1d1	brněnská
zoo	zoo	k1gFnSc1	zoo
<g/>
,	,	kIx,	,
Zoo	zoo	k1gFnSc1	zoo
Zlín	Zlín	k1gInSc1	Zlín
-	-	kIx~	-
Lešná	Lešná	k1gFnSc1	Lešná
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
rekordní	rekordní	k2eAgInSc4d1	rekordní
věk	věk	k1gInSc4	věk
<g/>
;	;	kIx,	;
Zoo	zoo	k1gFnSc1	zoo
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
unikátní	unikátní	k2eAgFnPc1d1	unikátní
operace	operace	k1gFnPc1	operace
samičky	samička	k1gFnSc2	samička
Jane	Jan	k1gMnSc5	Jan
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Gepardy	gepard	k1gMnPc4	gepard
chová	chovat	k5eAaImIp3nS	chovat
také	také	k9	také
plzeňská	plzeňský	k2eAgFnSc1d1	Plzeňská
zoo	zoo	k1gFnSc1	zoo
(	(	kIx(	(
<g/>
získáni	získán	k2eAgMnPc1d1	získán
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
-	-	kIx~	-
gepardi	gepard	k1gMnPc1	gepard
súdánští	súdánský	k2eAgMnPc1d1	súdánský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zájem	zájem	k1gInSc4	zájem
ventilovala	ventilovat	k5eAaImAgFnS	ventilovat
také	také	k9	také
zoo	zoo	k1gFnSc1	zoo
Chleby	chléb	k1gInPc1	chléb
u	u	k7c2	u
Nymburka	Nymburk	k1gInSc2	Nymburk
(	(	kIx(	(
<g/>
mladou	mladý	k2eAgFnSc4d1	mladá
samičku	samička	k1gFnSc4	samička
ze	z	k7c2	z
SRN	srna	k1gFnPc2	srna
na	na	k7c6	na
umělém	umělý	k2eAgInSc6d1	umělý
odchovu	odchov	k1gInSc6	odchov
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chovatelem	chovatel	k1gMnSc7	chovatel
byl	být	k5eAaImAgInS	být
nový	nový	k2eAgInSc1d1	nový
Zoopark	zoopark	k1gInSc1	zoopark
Zájezd	zájezd	k1gInSc1	zájezd
na	na	k7c6	na
Kladensku	Kladensko	k1gNnSc6	Kladensko
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
byl	být	k5eAaImAgMnS	být
gepard	gepard	k1gMnSc1	gepard
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Tábor-Větrovy	Tábor-Větrův	k2eAgInPc1d1	Tábor-Větrův
<g/>
.	.	kIx.	.
</s>
<s>
Soukromě	soukromě	k6eAd1	soukromě
byl	být	k5eAaImAgInS	být
chován	chován	k2eAgInSc4d1	chován
1	[number]	k4	1
samec	samec	k1gInSc4	samec
na	na	k7c6	na
Chrudimsku	Chrudimsko	k1gNnSc6	Chrudimsko
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
jedinci	jedinec	k1gMnPc1	jedinec
např.	např.	kA	např.
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
deníku	deník	k1gInSc2	deník
Aha	aha	k1gNnSc2	aha
gepardy	gepard	k1gMnPc7	gepard
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
rozmnožil	rozmnožit	k5eAaPmAgMnS	rozmnožit
soukromý	soukromý	k2eAgMnSc1d1	soukromý
chovatel	chovatel	k1gMnSc1	chovatel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
gepardi	gepard	k1gMnPc1	gepard
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Dvora	Dvůr	k1gInSc2	Dvůr
Králové	Králová	k1gFnSc2	Králová
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1967	[number]	k4	1967
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
samec	samec	k1gMnSc1	samec
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
samice	samice	k1gFnPc1	samice
<g/>
)	)	kIx)	)
a	a	k8xC	a
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
divokých	divoký	k2eAgFnPc2d1	divoká
afrických	africký	k2eAgFnPc2d1	africká
populací	populace	k1gFnPc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
1975	[number]	k4	1975
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
organizovala	organizovat	k5eAaBmAgFnS	organizovat
odchyty	odchyt	k1gInPc4	odchyt
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
tím	ten	k3xDgNnSc7	ten
dalších	další	k2eAgInPc2d1	další
24	[number]	k4	24
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
zoo	zoo	k1gNnSc2	zoo
s	s	k7c7	s
gepardy	gepard	k1gMnPc7	gepard
obchodovala	obchodovat	k5eAaImAgFnS	obchodovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
dovezených	dovezený	k2eAgMnPc2d1	dovezený
jedinců	jedinec	k1gMnPc2	jedinec
čítal	čítat	k5eAaImAgMnS	čítat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
42	[number]	k4	42
kusů	kus	k1gInPc2	kus
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgFnPc4	všechen
původně	původně	k6eAd1	původně
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
29	[number]	k4	29
odešlo	odejít	k5eAaPmAgNnS	odejít
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
a	a	k8xC	a
3	[number]	k4	3
se	se	k3xPyFc4	se
rozmnožili	rozmnožit	k5eAaPmAgMnP	rozmnožit
(	(	kIx(	(
<g/>
první	první	k4xOgInPc4	první
odchovy	odchov	k1gInPc4	odchov
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1988	[number]	k4	1988
až	až	k9	až
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
samec	samec	k1gMnSc1	samec
Luso	Luso	k1gMnSc1	Luso
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
Gaia	Gaia	k1gFnSc1	Gaia
a	a	k8xC	a
Asmara	Asmara	k1gFnSc1	Asmara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
do	do	k7c2	do
zoo	zoo	k1gFnSc2	zoo
přišlo	přijít	k5eAaPmAgNnS	přijít
dalších	další	k2eAgMnPc2d1	další
10	[number]	k4	10
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
včetně	včetně	k7c2	včetně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
narodilo	narodit	k5eAaPmAgNnS	narodit
64	[number]	k4	64
mláďat	mládě	k1gNnPc2	mládě
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
odchováno	odchovat	k5eAaPmNgNnS	odchovat
42	[number]	k4	42
kusů	kus	k1gInPc2	kus
(	(	kIx(	(
<g/>
úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
67,2	[number]	k4	67,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zoo	zoo	k1gFnSc1	zoo
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
chovné	chovný	k2eAgNnSc4d1	chovné
centrum	centrum	k1gNnSc4	centrum
nepřístupné	přístupný	k2eNgFnSc2d1	nepřístupná
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
odchovává	odchovávat	k5eAaImIp3nS	odchovávat
gepardy	gepard	k1gMnPc4	gepard
i	i	k9	i
pro	pro	k7c4	pro
jiná	jiný	k2eAgNnPc4d1	jiné
zoo	zoo	k1gNnPc4	zoo
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
