<s>
Rorýs	rorýs	k1gMnSc1	rorýs
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Apus	Apus	k1gInSc1	Apus
apus	apus	k1gInSc1	apus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgMnSc1d1	malý
stěhovavý	stěhovavý	k2eAgMnSc1d1	stěhovavý
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
svišťounů	svišťoun	k1gMnPc2	svišťoun
(	(	kIx(	(
<g/>
Apodiformes	Apodiformes	k1gMnSc1	Apodiformes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgMnPc1d1	patřící
k	k	k7c3	k
nejlepším	dobrý	k2eAgMnPc3d3	nejlepší
letcům	letec	k1gMnPc3	letec
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
pták	pták	k1gMnSc1	pták
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
16	[number]	k4	16
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
cm	cm	kA	cm
a	a	k8xC	a
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
měří	měřit	k5eAaImIp3nS	měřit
42	[number]	k4	42
<g/>
–	–	k?	–
<g/>
48	[number]	k4	48
cm	cm	kA	cm
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
štíhlý	štíhlý	k2eAgMnSc1d1	štíhlý
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc1d1	celý
černohnědě	černohnědě	k6eAd1	černohnědě
zbarvený	zbarvený	k2eAgInSc1d1	zbarvený
se	s	k7c7	s
světlým	světlý	k2eAgNnSc7d1	světlé
hrdlem	hrdlo	k1gNnSc7	hrdlo
<g/>
,	,	kIx,	,
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
srpovitými	srpovitý	k2eAgNnPc7d1	srpovitý
křídly	křídlo	k1gNnPc7	křídlo
<g/>
,	,	kIx,	,
vidličitým	vidličitý	k2eAgInSc7d1	vidličitý
ocasem	ocas	k1gInSc7	ocas
<g/>
,	,	kIx,	,
nápadně	nápadně	k6eAd1	nápadně
krátkým	krátký	k2eAgInSc7d1	krátký
zobákem	zobák	k1gInSc7	zobák
a	a	k8xC	a
zakrslými	zakrslý	k2eAgFnPc7d1	zakrslá
končetinami	končetina	k1gFnPc7	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
se	s	k7c7	s
zbarvením	zbarvení	k1gNnSc7	zbarvení
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městech	město	k1gNnPc6	město
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
budovách	budova	k1gFnPc6	budova
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
západě	západ	k1gInSc6	západ
Evropy	Evropa	k1gFnSc2	Evropa
žije	žít	k5eAaImIp3nS	žít
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgMnSc1d1	podobný
rorýs	rorýs	k1gMnSc1	rorýs
šedohnědý	šedohnědý	k2eAgMnSc1d1	šedohnědý
(	(	kIx(	(
<g/>
A.	A.	kA	A.
pallidus	pallidus	k1gInSc1	pallidus
<g/>
)	)	kIx)	)
a	a	k8xC	a
rorýs	rorýs	k1gMnSc1	rorýs
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Tachymarptis	Tachymarptis	k1gInSc1	Tachymarptis
melba	melb	k1gMnSc2	melb
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vzácně	vzácně	k6eAd1	vzácně
zaletuje	zaletovat	k5eAaImIp3nS	zaletovat
i	i	k9	i
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
rorýse	rorýs	k1gMnSc5	rorýs
obecného	obecný	k2eAgNnSc2d1	obecné
liší	lišit	k5eAaImIp3nS	lišit
bílým	bílý	k2eAgNnSc7d1	bílé
zbarvením	zbarvení	k1gNnSc7	zbarvení
břicha	břicho	k1gNnSc2	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgNnSc1d1	známé
pronikavé	pronikavý	k2eAgNnSc1d1	pronikavé
"	"	kIx"	"
<g/>
sríí	sríí	k1gNnSc1	sríí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
na	na	k7c6	na
rozsáhlém	rozsáhlý	k2eAgNnSc6d1	rozsáhlé
území	území	k1gNnSc6	území
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tažný	tažný	k2eAgInSc1d1	tažný
se	s	k7c7	s
zimovišti	zimoviště	k1gNnPc7	zimoviště
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polovině	polovina	k1gFnSc6	polovina
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
táhne	táhnout	k5eAaImIp3nS	táhnout
v	v	k7c6	v
září	září	k1gNnSc6	září
a	a	k8xC	a
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
začíná	začínat	k5eAaImIp3nS	začínat
vracet	vracet	k5eAaImF	vracet
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
městech	město	k1gNnPc6	město
a	a	k8xC	a
na	na	k7c6	na
vesnicích	vesnice	k1gFnPc6	vesnice
na	na	k7c6	na
budovách	budova	k1gFnPc6	budova
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
v	v	k7c6	v
lesích	les	k1gInPc6	les
nebo	nebo	k8xC	nebo
skalnatých	skalnatý	k2eAgFnPc6d1	skalnatá
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
chráněný	chráněný	k2eAgInSc4d1	chráněný
jako	jako	k8xS	jako
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
hnízdil	hnízdit	k5eAaImAgInS	hnízdit
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
60	[number]	k4	60
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
000	[number]	k4	000
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Rorýs	rorýs	k1gMnSc1	rorýs
obecný	obecný	k2eAgMnSc1d1	obecný
se	se	k3xPyFc4	se
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
hnízdního	hnízdní	k2eAgNnSc2d1	hnízdní
období	období	k1gNnSc2	období
téměř	téměř	k6eAd1	téměř
neustále	neustále	k6eAd1	neustále
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dokonce	dokonce	k9	dokonce
i	i	k9	i
spí	spát	k5eAaImIp3nS	spát
<g/>
,	,	kIx,	,
pije	pít	k5eAaImIp3nS	pít
a	a	k8xC	a
páří	pářit	k5eAaImIp3nS	pářit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
střídá	střídat	k5eAaImIp3nS	střídat
přitom	přitom	k6eAd1	přitom
svižný	svižný	k2eAgInSc4d1	svižný
let	let	k1gInSc4	let
s	s	k7c7	s
plachtěním	plachtění	k1gNnSc7	plachtění
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yRgMnSc2	který
nabírá	nabírat	k5eAaImIp3nS	nabírat
výšku	výška	k1gFnSc4	výška
okolo	okolo	k7c2	okolo
jednoho	jeden	k4xCgNnSc2	jeden
až	až	k6eAd1	až
dvou	dva	k4xCgInPc2	dva
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
následně	následně	k6eAd1	následně
během	během	k7c2	během
spánku	spánek	k1gInSc2	spánek
mohl	moct	k5eAaImAgMnS	moct
klesat	klesat	k5eAaImF	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
klesavého	klesavý	k2eAgInSc2d1	klesavý
letu	let	k1gInSc2	let
upadá	upadat	k5eAaImIp3nS	upadat
do	do	k7c2	do
mikrospánku	mikrospánek	k1gInSc2	mikrospánek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vypíná	vypínat	k5eAaImIp3nS	vypínat
jednu	jeden	k4xCgFnSc4	jeden
polovinu	polovina	k1gFnSc4	polovina
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
druhá	druhý	k4xOgFnSc1	druhý
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
let	let	k1gInSc1	let
<g/>
.	.	kIx.	.
</s>
<s>
Rorýsové	rorýs	k1gMnPc1	rorýs
nocují	nocovat	k5eAaImIp3nP	nocovat
za	za	k7c2	za
letu	let	k1gInSc2	let
v	v	k7c6	v
hejnech	hejno	k1gNnPc6	hejno
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
létá	létat	k5eAaImIp3nS	létat
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
rychlostí	rychlost	k1gFnSc7	rychlost
35	[number]	k4	35
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
i	i	k9	i
rychlosti	rychlost	k1gFnSc2	rychlost
přesahující	přesahující	k2eAgFnSc2d1	přesahující
200	[number]	k4	200
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Minimální	minimální	k2eAgInSc1d1	minimální
pohyb	pohyb	k1gInSc1	pohyb
po	po	k7c6	po
pevném	pevný	k2eAgInSc6d1	pevný
povrchu	povrch	k1gInSc6	povrch
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
také	také	k9	také
jeho	jeho	k3xOp3gFnPc1	jeho
zakrnělé	zakrnělý	k2eAgFnPc1d1	zakrnělá
končetiny	končetina	k1gFnPc1	končetina
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yIgInPc3	který
druh	druh	k1gInSc1	druh
získal	získat	k5eAaPmAgInS	získat
i	i	k9	i
binomické	binomický	k2eAgNnSc4d1	binomické
jméno	jméno	k1gNnSc4	jméno
(	(	kIx(	(
<g/>
Apus	Apus	k1gInSc1	Apus
apus	apus	k1gInSc1	apus
–	–	k?	–
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
apous	apous	k1gInSc4	apous
=	=	kIx~	=
bez	bez	k7c2	bez
nohou	noha	k1gFnPc2	noha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
létajícím	létající	k2eAgInSc7d1	létající
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
přilétá	přilétat	k5eAaImIp3nS	přilétat
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
do	do	k7c2	do
zimovišť	zimoviště	k1gNnPc2	zimoviště
odlétá	odlétat	k5eAaImIp3nS	odlétat
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
hnízdil	hnízdit	k5eAaImAgMnS	hnízdit
na	na	k7c6	na
skalách	skála	k1gFnPc6	skála
a	a	k8xC	a
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
však	však	k9	však
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
využívá	využívat	k5eAaImIp3nS	využívat
převážně	převážně	k6eAd1	převážně
otvory	otvor	k1gInPc4	otvor
a	a	k8xC	a
štěrbiny	štěrbina	k1gFnPc4	štěrbina
v	v	k7c6	v
lidských	lidský	k2eAgFnPc6d1	lidská
stavbách	stavba	k1gFnPc6	stavba
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
hnízdiště	hnízdiště	k1gNnSc4	hnízdiště
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
současný	současný	k2eAgInSc1d1	současný
trend	trend	k1gInSc1	trend
zateplování	zateplování	k1gNnSc2	zateplování
budov	budova	k1gFnPc2	budova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
jednou	jeden	k4xCgFnSc7	jeden
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
Samička	samička	k1gFnSc1	samička
snáší	snášet	k5eAaImIp3nS	snášet
2	[number]	k4	2
-	-	kIx~	-
3	[number]	k4	3
vejce	vejce	k1gNnPc4	vejce
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
sedí	sedit	k5eAaImIp3nS	sedit
sama	sám	k3xTgFnSc1	sám
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
sameček	sameček	k1gMnSc1	sameček
ji	on	k3xPp3gFnSc4	on
krmí	krmit	k5eAaImIp3nS	krmit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
18	[number]	k4	18
až	až	k9	až
21	[number]	k4	21
dnech	den	k1gInPc6	den
se	se	k3xPyFc4	se
vylíhnou	vylíhnout	k5eAaPmIp3nP	vylíhnout
mláďata	mládě	k1gNnPc1	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
35	[number]	k4	35
až	až	k9	až
42	[number]	k4	42
dnů	den	k1gInPc2	den
opouštějí	opouštět	k5eAaImIp3nP	opouštět
mláďata	mládě	k1gNnPc1	mládě
hnízdo	hnízdo	k1gNnSc1	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
nepříznivého	příznivý	k2eNgNnSc2d1	nepříznivé
počasí	počasí	k1gNnSc2	počasí
dokáží	dokázat	k5eAaPmIp3nP	dokázat
mláďata	mládě	k1gNnPc4	mládě
snížit	snížit	k5eAaPmF	snížit
teplotu	teplota	k1gFnSc4	teplota
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
zpomalit	zpomalit	k5eAaPmF	zpomalit
tak	tak	k9	tak
svůj	svůj	k3xOyFgInSc4	svůj
růst	růst	k1gInSc4	růst
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
dokáží	dokázat	k5eAaPmIp3nP	dokázat
přežít	přežít	k5eAaPmF	přežít
i	i	k9	i
jedno-	jedno-	k?	jedno-
až	až	k6eAd1	až
dvoutýdenní	dvoutýdenní	k2eAgInSc4d1	dvoutýdenní
nedostatek	nedostatek	k1gInSc4	nedostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
vzhledem	vzhled	k1gInSc7	vzhled
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
výborným	výborný	k2eAgFnPc3d1	výborná
manévrovacím	manévrovací	k2eAgFnPc3d1	manévrovací
schopnostem	schopnost	k1gFnPc3	schopnost
v	v	k7c6	v
letu	let	k1gInSc6	let
jen	jen	k9	jen
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
predátorů	predátor	k1gMnPc2	predátor
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yQgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
zejména	zejména	k9	zejména
ostříž	ostříž	k1gMnSc1	ostříž
lesní	lesní	k2eAgMnSc1d1	lesní
(	(	kIx(	(
<g/>
Falco	Falco	k1gMnSc1	Falco
subbuteo	subbuteo	k1gMnSc1	subbuteo
<g/>
)	)	kIx)	)
a	a	k8xC	a
sokol	sokol	k1gMnSc1	sokol
stěhovavý	stěhovavý	k2eAgMnSc1d1	stěhovavý
(	(	kIx(	(
<g/>
Falco	Falco	k1gMnSc1	Falco
peregrinus	peregrinus	k1gMnSc1	peregrinus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
nepřístupných	přístupný	k2eNgNnPc6d1	nepřístupné
hnízdech	hnízdo	k1gNnPc6	hnízdo
obvykle	obvykle	k6eAd1	obvykle
dobře	dobře	k6eAd1	dobře
chráněna	chránit	k5eAaImNgFnS	chránit
<g/>
,	,	kIx,	,
ohrozit	ohrozit	k5eAaPmF	ohrozit
je	on	k3xPp3gInPc4	on
dokáže	dokázat	k5eAaPmIp3nS	dokázat
např.	např.	kA	např.
poštolka	poštolka	k1gFnSc1	poštolka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Falco	Falco	k1gMnSc1	Falco
tinnunculus	tinnunculus	k1gMnSc1	tinnunculus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
ornitologická	ornitologický	k2eAgFnSc1d1	ornitologická
–	–	k?	–
Pták	pták	k1gMnSc1	pták
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
ptáci	pták	k1gMnPc1	pták
vydrží	vydržet	k5eAaPmIp3nP	vydržet
extrémně	extrémně	k6eAd1	extrémně
dlouho	dlouho	k6eAd1	dlouho
létat	létat	k5eAaImF	létat
bez	bez	k7c2	bez
přistání	přistání	k1gNnSc2	přistání
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
švédští	švédský	k2eAgMnPc1d1	švédský
biologové	biolog	k1gMnPc1	biolog
předvídali	předvídat	k5eAaImAgMnP	předvídat
dobu	doba	k1gFnSc4	doba
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vyhodnocením	vyhodnocení	k1gNnSc7	vyhodnocení
"	"	kIx"	"
<g/>
mikrobatůžku	mikrobatůžka	k1gFnSc4	mikrobatůžka
<g/>
"	"	kIx"	"
s	s	k7c7	s
několika	několik	k4yIc7	několik
funkcemi	funkce	k1gFnPc7	funkce
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
let	let	k1gInSc1	let
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
10	[number]	k4	10
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
pták	pták	k1gMnSc1	pták
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zřejmě	zřejmě	k6eAd1	zřejmě
rekordmanem	rekordman	k1gMnSc7	rekordman
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
pobytu	pobyt	k1gInSc2	pobyt
mimo	mimo	k7c4	mimo
pevnou	pevný	k2eAgFnSc4d1	pevná
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
