<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
(	(	kIx(	(
<g/>
dánsky	dánsky	k6eAd1	dánsky
Danmark	Danmark	k1gInSc1	Danmark
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
země	země	k1gFnSc1	země
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Grónskem	Grónsko	k1gNnSc7	Grónsko
a	a	k8xC	a
Faerskými	Faerský	k2eAgInPc7d1	Faerský
ostrovy	ostrov	k1gInPc7	ostrov
tvoří	tvořit	k5eAaImIp3nP	tvořit
státní	státní	k2eAgInSc4d1	státní
celek	celek	k1gInSc4	celek
Dánské	dánský	k2eAgNnSc1d1	dánské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
Kongeriget	Kongeriget	k1gInSc1	Kongeriget
Danmark	Danmark	k1gInSc1	Danmark
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Grónsko	Grónsko	k1gNnSc4	Grónsko
a	a	k8xC	a
Faerské	Faerský	k2eAgInPc4d1	Faerský
ostrovy	ostrov	k1gInPc4	ostrov
patřící	patřící	k2eAgInPc4d1	patřící
oficiálně	oficiálně	k6eAd1	oficiálně
také	také	k6eAd1	také
Dánsku	Dánsko	k1gNnSc3	Dánsko
(	(	kIx(	(
<g/>
Dánskému	dánský	k2eAgNnSc3d1	dánské
království	království	k1gNnSc3	království
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
disponují	disponovat	k5eAaBmIp3nP	disponovat
autonomií	autonomie	k1gFnSc7	autonomie
<g/>
,	,	kIx,	,
nepatří	patřit	k5eNaImIp3nS	patřit
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
deleguje	delegovat	k5eAaBmIp3nS	delegovat
dva	dva	k4xCgMnPc4	dva
zástupce	zástupce	k1gMnPc4	zástupce
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
Folketingu	Folketing	k1gInSc2	Folketing
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vztah	vztah	k1gInSc4	vztah
kontinentálního	kontinentální	k2eAgNnSc2d1	kontinentální
Dánska	Dánsko	k1gNnSc2	Dánsko
a	a	k8xC	a
obou	dva	k4xCgNnPc2	dva
autonomních	autonomní	k2eAgNnPc2d1	autonomní
území	území	k1gNnPc2	území
(	(	kIx(	(
<g/>
Faerských	Faerský	k2eAgInPc2d1	Faerský
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
polooficiální	polooficiální	k2eAgInSc1d1	polooficiální
termín	termín	k1gInSc1	termín
Rigsfæ	Rigsfæ	k1gFnSc2	Rigsfæ
<g/>
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgFnSc1d3	nejmenší
ze	z	k7c2	z
severských	severský	k2eAgFnPc2d1	severská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
neleží	ležet	k5eNaImIp3nS	ležet
však	však	k9	však
na	na	k7c6	na
Skandinávském	skandinávský	k2eAgInSc6d1	skandinávský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
Jutským	jutský	k2eAgInSc7d1	jutský
poloostrovem	poloostrov	k1gInSc7	poloostrov
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc4	ostrov
Fyn	Fyn	k1gFnSc2	Fyn
<g/>
,	,	kIx,	,
Sjæ	Sjæ	k1gFnSc2	Sjæ
a	a	k8xC	a
stovkami	stovka	k1gFnPc7	stovka
dalších	další	k2eAgInPc2d1	další
menších	malý	k2eAgInPc2d2	menší
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
je	být	k5eAaImIp3nS	být
omýváno	omývat	k5eAaImNgNnS	omývat
Baltským	baltský	k2eAgNnSc7d1	Baltské
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
mořem	moře	k1gNnSc7	moře
Severním	severní	k2eAgNnSc7d1	severní
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc4d1	jediná
suchou	suchý	k2eAgFnSc4d1	suchá
hranici	hranice	k1gFnSc4	hranice
má	mít	k5eAaImIp3nS	mít
Dánsko	Dánsko	k1gNnSc1	Dánsko
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
blízké	blízký	k2eAgFnPc1d1	blízká
země	zem	k1gFnPc1	zem
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
severovýchodě	severovýchod	k1gInSc6	severovýchod
Švédsko	Švédsko	k1gNnSc4	Švédsko
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
.	.	kIx.	.
6	[number]	k4	6
z	z	k7c2	z
10	[number]	k4	10
Dánů	Dán	k1gMnPc2	Dán
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Kodaňské	kodaňský	k2eAgFnSc6d1	Kodaňská
aglomeraci	aglomerace	k1gFnSc6	aglomerace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
metropolitní	metropolitní	k2eAgFnSc4d1	metropolitní
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
malou	malý	k2eAgFnSc7d1	malá
částí	část	k1gFnSc7	část
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
i	i	k9	i
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Dánska	Dánsko	k1gNnSc2	Dánsko
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
germánskými	germánský	k2eAgInPc7d1	germánský
kmeny	kmen	k1gInPc7	kmen
(	(	kIx(	(
<g/>
Angly	Angl	k1gMnPc4	Angl
<g/>
,	,	kIx,	,
Sasy	Sas	k1gMnPc4	Sas
<g/>
,	,	kIx,	,
Teutony	Teuton	k1gMnPc4	Teuton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přicházeli	přicházet	k5eAaImAgMnP	přicházet
Normané	Norman	k1gMnPc1	Norman
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
ovládnuto	ovládnout	k5eAaPmNgNnS	ovládnout
Haraldem	Haraldo	k1gNnSc7	Haraldo
Modrozubým	Modrozubý	k2eAgNnSc7d1	Modrozubý
(	(	kIx(	(
<g/>
Harald	Harald	k1gInSc1	Harald
Blå	Blå	k1gFnSc2	Blå
<g/>
)	)	kIx)	)
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
980	[number]	k4	980
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
byli	být	k5eAaImAgMnP	být
Dáni	Dán	k1gMnPc1	Dán
známi	znám	k2eAgMnPc1d1	znám
jako	jako	k8xC	jako
Vikingové	Viking	k1gMnPc1	Viking
-	-	kIx~	-
kolonizátoři	kolonizátor	k1gMnPc1	kolonizátor
<g/>
,	,	kIx,	,
nájezdníci	nájezdník	k1gMnPc1	nájezdník
a	a	k8xC	a
obchodníci	obchodník	k1gMnPc1	obchodník
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
dobách	doba	k1gFnPc6	doba
Dánsko	Dánsko	k1gNnSc1	Dánsko
ovládalo	ovládat	k5eAaImAgNnS	ovládat
území	území	k1gNnSc4	území
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
baltského	baltský	k2eAgNnSc2d1	Baltské
pobřeží	pobřeží	k1gNnSc2	pobřeží
a	a	k8xC	a
území	území	k1gNnSc2	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
severního	severní	k2eAgNnSc2d1	severní
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Skandinávského	skandinávský	k2eAgInSc2d1	skandinávský
poloostrova	poloostrov	k1gInSc2	poloostrov
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
Dánska	Dánsko	k1gNnSc2	Dánsko
od	od	k7c2	od
jeho	jeho	k3xOp3gFnSc2	jeho
rané	raný	k2eAgFnSc2d1	raná
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
postoupeno	postoupit	k5eAaPmNgNnS	postoupit
Švédsku	Švédsko	k1gNnSc6	Švédsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1658	[number]	k4	1658
<g/>
.	.	kIx.	.
</s>
<s>
Personální	personální	k2eAgFnSc1d1	personální
unie	unie	k1gFnSc1	unie
s	s	k7c7	s
Norskem	Norsko	k1gNnSc7	Norsko
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1814	[number]	k4	1814
<g/>
,	,	kIx,	,
když	když	k8xS	když
Norsko	Norsko	k1gNnSc1	Norsko
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
personální	personální	k2eAgFnSc2d1	personální
unie	unie	k1gFnSc2	unie
se	s	k7c7	s
Švédskem	Švédsko	k1gNnSc7	Švédsko
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
pak	pak	k6eAd1	pak
trvala	trvat	k5eAaImAgFnS	trvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Grónsko	Grónsko	k1gNnSc1	Grónsko
a	a	k8xC	a
Faerské	Faerský	k2eAgInPc1d1	Faerský
ostrovy	ostrov	k1gInPc1	ostrov
však	však	k9	však
stále	stále	k6eAd1	stále
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
součástí	součást	k1gFnSc7	součást
dánského	dánský	k2eAgInSc2d1	dánský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
reformy	reforma	k1gFnSc2	reforma
osvícenského	osvícenský	k2eAgInSc2d1	osvícenský
absolutismu	absolutismus	k1gInSc2	absolutismus
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
průmyslové	průmyslový	k2eAgFnSc3d1	průmyslová
revoluci	revoluce	k1gFnSc3	revoluce
a	a	k8xC	a
k	k	k7c3	k
pozdější	pozdní	k2eAgFnSc3d2	pozdější
přeměně	přeměna	k1gFnSc3	přeměna
Dánska	Dánsko	k1gNnSc2	Dánsko
na	na	k7c4	na
konstituční	konstituční	k2eAgFnSc4d1	konstituční
monarchii	monarchie	k1gFnSc4	monarchie
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
N.	N.	kA	N.
F.	F.	kA	F.
S.	S.	kA	S.
Grundtvig	Grundtvig	k1gMnSc1	Grundtvig
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
šlesvické	šlesvický	k2eAgFnSc6d1	šlesvický
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
Dánsko	Dánsko	k1gNnSc1	Dánsko
muselo	muset	k5eAaImAgNnS	muset
postoupit	postoupit	k5eAaPmF	postoupit
Prusku	Prusko	k1gNnSc3	Prusko
Holštýnsko	Holštýnsko	k1gNnSc1	Holštýnsko
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Šlesvicka	Šlesvicka	k1gFnSc1	Šlesvicka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
převládalo	převládat	k5eAaImAgNnS	převládat
německé	německý	k2eAgNnSc1d1	německé
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnSc6	událost
přijalo	přijmout	k5eAaPmAgNnS	přijmout
Dánsko	Dánsko	k1gNnSc1	Dánsko
politiku	politika	k1gFnSc4	politika
neutrality	neutralita	k1gFnSc2	neutralita
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
i	i	k9	i
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
část	část	k1gFnSc1	část
Šlesvicka	Šlesvicka	k1gFnSc1	Šlesvicka
vrácena	vrácen	k2eAgFnSc1d1	vrácena
Dánsku	Dánsko	k1gNnSc3	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1940	[number]	k4	1940
bylo	být	k5eAaImAgNnS	být
Dánsko	Dánsko	k1gNnSc1	Dánsko
napadeno	napaden	k2eAgNnSc1d1	napadeno
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
(	(	kIx(	(
<g/>
operace	operace	k1gFnSc2	operace
Weserübung	Weserübung	k1gInSc1	Weserübung
<g/>
)	)	kIx)	)
a	a	k8xC	a
navzdory	navzdory	k7c3	navzdory
domácímu	domácí	k2eAgInSc3d1	domácí
odboji	odboj	k1gInSc3	odboj
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
okupováno	okupován	k2eAgNnSc1d1	okupováno
až	až	k9	až
do	do	k7c2	do
osvobození	osvobození	k1gNnSc2	osvobození
britskými	britský	k2eAgInPc7d1	britský
oddíly	oddíl	k1gInPc7	oddíl
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
Dánsko	Dánsko	k1gNnSc1	Dánsko
stalo	stát	k5eAaPmAgNnS	stát
členem	člen	k1gInSc7	člen
NATO	nato	k6eAd1	nato
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
se	se	k3xPyFc4	se
připojilo	připojit	k5eAaPmAgNnS	připojit
také	také	k9	také
k	k	k7c3	k
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
Jutském	jutský	k2eAgInSc6d1	jutský
poloostrově	poloostrov	k1gInSc6	poloostrov
a	a	k8xC	a
na	na	k7c6	na
443	[number]	k4	443
pojmenovaných	pojmenovaný	k2eAgInPc6d1	pojmenovaný
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgInPc2	který
je	být	k5eAaImIp3nS	být
76	[number]	k4	76
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
celoročně	celoročně	k6eAd1	celoročně
obývaných	obývaný	k2eAgMnPc2d1	obývaný
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgInPc1d3	nejdůležitější
jsou	být	k5eAaImIp3nP	být
ostrovy	ostrov	k1gInPc1	ostrov
Fyn	Fyn	k1gFnSc2	Fyn
a	a	k8xC	a
Sjæ	Sjæ	k1gFnSc2	Sjæ
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
velkými	velký	k2eAgInPc7d1	velký
ostrovy	ostrov	k1gInPc7	ostrov
jsou	být	k5eAaImIp3nP	být
Lolland	Lolland	k1gInSc4	Lolland
<g/>
,	,	kIx,	,
Falster	Falster	k1gInSc1	Falster
<g/>
,	,	kIx,	,
Langeland	Langeland	k1gInSc1	Langeland
<g/>
,	,	kIx,	,
Mø	Mø	k1gFnSc1	Mø
<g/>
,	,	kIx,	,
Æ	Æ	k?	Æ
<g/>
,	,	kIx,	,
Als	Als	k1gFnSc1	Als
a	a	k8xC	a
Bornholm	Bornholm	k1gInSc1	Bornholm
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
poněkud	poněkud	k6eAd1	poněkud
východněji	východně	k6eAd2	východně
v	v	k7c6	v
Baltském	baltský	k2eAgNnSc6d1	Baltské
moři	moře	k1gNnSc6	moře
než	než	k8xS	než
zbytek	zbytek	k1gInSc1	zbytek
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
existovaly	existovat	k5eAaImAgInP	existovat
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
tři	tři	k4xCgInPc4	tři
národní	národní	k2eAgInPc4d1	národní
parky	park	k1gInPc4	park
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
rovinami	rovina	k1gFnPc7	rovina
a	a	k8xC	a
nížinami	nížina	k1gFnPc7	nížina
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
výška	výška	k1gFnSc1	výška
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
jen	jen	k9	jen
30	[number]	k4	30
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Nejvyššími	vysoký	k2eAgInPc7d3	Nejvyšší
body	bod	k1gInPc7	bod
Dánska	Dánsko	k1gNnSc2	Dánsko
jsou	být	k5eAaImIp3nP	být
Mø	Mø	k1gMnSc1	Mø
a	a	k8xC	a
Yding	Yding	k1gMnSc1	Yding
Skovhø	Skovhø	k1gMnSc1	Skovhø
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
171	[number]	k4	171
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Dánské	dánský	k2eAgNnSc1d1	dánské
podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
oceánské	oceánský	k2eAgNnSc1d1	oceánské
s	s	k7c7	s
rostoucími	rostoucí	k2eAgInPc7d1	rostoucí
kontinentálními	kontinentální	k2eAgInPc7d1	kontinentální
vlivy	vliv	k1gInPc7	vliv
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Baltskému	baltský	k2eAgNnSc3d1	Baltské
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Léta	léto	k1gNnPc1	léto
jsou	být	k5eAaImIp3nP	být
chladná	chladný	k2eAgNnPc1d1	chladné
<g/>
,	,	kIx,	,
zimy	zima	k1gFnSc2	zima
pak	pak	k6eAd1	pak
mírné	mírný	k2eAgNnSc1d1	mírné
a	a	k8xC	a
deštivé	deštivý	k2eAgNnSc1d1	deštivé
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgInPc1d1	častý
jsou	být	k5eAaImIp3nP	být
větry	vítr	k1gInPc1	vítr
a	a	k8xC	a
mlhy	mlha	k1gFnPc1	mlha
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
řekou	řeka	k1gFnSc7	řeka
je	být	k5eAaImIp3nS	být
Gudenå	Gudenå	k1gFnSc1	Gudenå
<g/>
.	.	kIx.	.
</s>
<s>
Západ	západ	k1gInSc4	západ
země	zem	k1gFnSc2	zem
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
vřesoviště	vřesoviště	k1gNnSc4	vřesoviště
a	a	k8xC	a
rašeliniště	rašeliniště	k1gNnSc4	rašeliniště
<g/>
.	.	kIx.	.
</s>
<s>
Lesy	les	k1gInPc1	les
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
11	[number]	k4	11
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
(	(	kIx(	(
<g/>
před	před	k7c4	před
sto	sto	k4xCgNnSc4	sto
lety	let	k1gInPc7	let
4	[number]	k4	4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
jsou	být	k5eAaImIp3nP	být
listnaté	listnatý	k2eAgFnPc1d1	listnatá
<g/>
.	.	kIx.	.
</s>
<s>
Jehličnaté	jehličnatý	k2eAgInPc1d1	jehličnatý
lesy	les	k1gInPc1	les
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaImNgInP	využívat
pro	pro	k7c4	pro
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
účely	účel	k1gInPc4	účel
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
ochrana	ochrana	k1gFnSc1	ochrana
pobřeží	pobřeží	k1gNnSc2	pobřeží
(	(	kIx(	(
<g/>
Sandflugtplantagen	Sandflugtplantagen	k1gInSc1	Sandflugtplantagen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Dánské	dánský	k2eAgNnSc1d1	dánské
království	království	k1gNnSc1	království
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
konstituční	konstituční	k2eAgFnSc7d1	konstituční
monarchií	monarchie	k1gFnSc7	monarchie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
moc	moc	k1gFnSc4	moc
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
formálně	formálně	k6eAd1	formálně
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
královna	královna	k1gFnSc1	královna
Markéta	Markéta	k1gFnSc1	Markéta
II	II	kA	II
<g/>
.	.	kIx.	.
jako	jako	k8xS	jako
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
moc	moc	k1gFnSc1	moc
výkonná	výkonný	k2eAgFnSc1d1	výkonná
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
ministerského	ministerský	k2eAgMnSc2d1	ministerský
předsedy	předseda	k1gMnSc2	předseda
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
ministrů	ministr	k1gMnPc2	ministr
tvořících	tvořící	k2eAgFnPc2d1	tvořící
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
ministři	ministr	k1gMnPc1	ministr
jsou	být	k5eAaImIp3nP	být
odpovědní	odpovědný	k2eAgMnPc1d1	odpovědný
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
moci	moc	k1gFnSc2	moc
zákonodárné	zákonodárný	k2eAgFnSc2d1	zákonodárná
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
jednokomorový	jednokomorový	k2eAgInSc1d1	jednokomorový
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
179	[number]	k4	179
křesel	křeslo	k1gNnPc2	křeslo
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
Folketinget	Folketinget	k1gMnSc1	Folketinget
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
jsou	být	k5eAaImIp3nP	být
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
křesla	křeslo	k1gNnPc4	křeslo
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
náleží	náležet	k5eAaImIp3nS	náležet
i	i	k9	i
reprezentantům	reprezentant	k1gMnPc3	reprezentant
Grónska	Grónsko	k1gNnSc2	Grónsko
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
reprezentantům	reprezentant	k1gMnPc3	reprezentant
Faerských	Faerský	k2eAgMnPc2d1	Faerský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
vlády	vláda	k1gFnSc2	vláda
koalice	koalice	k1gFnSc2	koalice
vedená	vedený	k2eAgFnSc1d1	vedená
středo-pravicovou	středoravicový	k2eAgFnSc7d1	středo-pravicová
stranou	strana	k1gFnSc7	strana
Venstre	Venstr	k1gInSc5	Venstr
<g/>
.	.	kIx.	.
</s>
<s>
Ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
Lars	Lars	k1gInSc1	Lars
Lø	Lø	k1gFnSc2	Lø
Rasmussen	Rasmussna	k1gFnPc2	Rasmussna
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
Anders	Andersa	k1gFnPc2	Andersa
Fogh	Fogh	k1gMnSc1	Fogh
Rasmussen	Rasmussen	k2eAgMnSc1d1	Rasmussen
se	se	k3xPyFc4	se
úřadu	úřad	k1gInSc3	úřad
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
generálním	generální	k2eAgMnSc7d1	generální
sekretářem	sekretář	k1gMnSc7	sekretář
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2011	[number]	k4	2011
mají	mít	k5eAaImIp3nP	mít
koaliční	koaliční	k2eAgFnSc4d1	koaliční
většinu	většina	k1gFnSc4	většina
levicové	levicový	k2eAgFnSc2d1	levicová
strany	strana	k1gFnSc2	strana
s	s	k7c7	s
92	[number]	k4	92
mandáty	mandát	k1gInPc7	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Koaliční	koaliční	k2eAgFnSc4d1	koaliční
vládu	vláda	k1gFnSc4	vláda
tak	tak	k6eAd1	tak
tvoří	tvořit	k5eAaImIp3nP	tvořit
tři	tři	k4xCgFnPc1	tři
strany	strana	k1gFnPc1	strana
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
ji	on	k3xPp3gFnSc4	on
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
Sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
Helle	Helle	k1gFnSc2	Helle
Thorningová-Schmidtová	Thorningová-Schmidtová	k1gFnSc1	Thorningová-Schmidtová
<g/>
.	.	kIx.	.
</s>
<s>
Opoziční	opoziční	k2eAgFnPc1d1	opoziční
pravicové	pravicový	k2eAgFnPc1d1	pravicová
strany	strana	k1gFnPc1	strana
mají	mít	k5eAaImIp3nP	mít
87	[number]	k4	87
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
je	být	k5eAaImIp3nS	být
Dánsko	Dánsko	k1gNnSc1	Dánsko
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
regionů	region	k1gInPc2	region
(	(	kIx(	(
<g/>
v	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
jsou	být	k5eAaImIp3nP	být
správní	správní	k2eAgInPc1d1	správní
sídla	sídlo	k1gNnSc2	sídlo
těchto	tento	k3xDgInPc2	tento
regionů	region	k1gInPc2	region
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Nordjylland	Nordjylland	k1gInSc1	Nordjylland
(	(	kIx(	(
<g/>
Aalborg	Aalborg	k1gInSc1	Aalborg
<g/>
)	)	kIx)	)
Midtjylland	Midtjylland	k1gInSc1	Midtjylland
(	(	kIx(	(
<g/>
Viborg	Viborg	k1gInSc1	Viborg
<g/>
)	)	kIx)	)
Syddanmark	Syddanmark	k1gInSc1	Syddanmark
(	(	kIx(	(
<g/>
Vejle	vejl	k1gInSc5	vejl
<g/>
)	)	kIx)	)
Hovedstaden	Hovedstaden	k2eAgMnSc1d1	Hovedstaden
(	(	kIx(	(
<g/>
Hillerø	Hillerø	k1gMnSc1	Hillerø
<g/>
)	)	kIx)	)
Sjæ	Sjæ	k1gMnSc1	Sjæ
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Sorø	Sorø	k1gMnPc2	Sorø
<g/>
)	)	kIx)	)
Dánsko	Dánsko	k1gNnSc1	Dánsko
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1970	[number]	k4	1970
a	a	k8xC	a
2006	[number]	k4	2006
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
13	[number]	k4	13
samosprávních	samosprávní	k2eAgInPc2d1	samosprávní
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
amt	amt	k?	amt
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
270	[number]	k4	270
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
kommune	kommun	k1gInSc5	kommun
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Grónsko	Grónsko	k1gNnSc1	Grónsko
a	a	k8xC	a
Faerské	Faerský	k2eAgInPc1d1	Faerský
ostrovy	ostrov	k1gInPc1	ostrov
patří	patřit	k5eAaImIp3nP	patřit
také	také	k9	také
Dánskému	dánský	k2eAgNnSc3d1	dánské
království	království	k1gNnSc3	království
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
jim	on	k3xPp3gInPc3	on
ponechána	ponechat	k5eAaPmNgFnS	ponechat
autonomie	autonomie	k1gFnSc1	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
deleguje	delegovat	k5eAaBmIp3nS	delegovat
dva	dva	k4xCgMnPc4	dva
zástupce	zástupce	k1gMnPc4	zástupce
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
s	s	k7c7	s
moderním	moderní	k2eAgNnSc7d1	moderní
tržním	tržní	k2eAgNnSc7d1	tržní
hospodářstvím	hospodářství	k1gNnSc7	hospodářství
<g/>
,	,	kIx,	,
technicky	technicky	k6eAd1	technicky
vyspělým	vyspělý	k2eAgNnSc7d1	vyspělé
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
a	a	k8xC	a
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
vládní	vládní	k2eAgFnSc7d1	vládní
sociální	sociální	k2eAgFnSc7d1	sociální
politikou	politika	k1gFnSc7	politika
(	(	kIx(	(
<g/>
sociálně-demokratický	sociálněemokratický	k2eAgInSc1d1	sociálně-demokratický
model	model	k1gInSc1	model
welfare	welfar	k1gMnSc5	welfar
state	status	k1gInSc5	status
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyspělost	vyspělost	k1gFnSc1	vyspělost
dánské	dánský	k2eAgFnSc2d1	dánská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
životní	životní	k2eAgFnSc7d1	životní
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
,	,	kIx,	,
pevnou	pevný	k2eAgFnSc7d1	pevná
měnou	měna	k1gFnSc7	měna
i	i	k8xC	i
značnou	značný	k2eAgFnSc7d1	značná
závislostí	závislost	k1gFnSc7	závislost
na	na	k7c6	na
obchodu	obchod	k1gInSc6	obchod
se	s	k7c7	s
zahraničím	zahraničí	k1gNnSc7	zahraničí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aktivní	aktivní	k2eAgFnSc7d1	aktivní
obchodní	obchodní	k2eAgFnSc7d1	obchodní
bilancí	bilance	k1gFnSc7	bilance
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Dánsko	Dánsko	k1gNnSc1	Dánsko
splňovalo	splňovat	k5eAaImAgNnS	splňovat
ekonomická	ekonomický	k2eAgNnPc4d1	ekonomické
kritéria	kritérion	k1gNnPc4	kritérion
Evropské	evropský	k2eAgFnSc2d1	Evropská
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
EMU	emu	k1gMnSc1	emu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
Dánové	Dán	k1gMnPc1	Dán
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
v	v	k7c6	v
září	září	k1gNnSc6	září
2000	[number]	k4	2000
o	o	k7c6	o
nepřipojení	nepřipojení	k1gNnSc6	nepřipojení
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
pracuje	pracovat	k5eAaImIp3nS	pracovat
jen	jen	k9	jen
6	[number]	k4	6
%	%	kIx~	%
ekonomicky	ekonomicky	k6eAd1	ekonomicky
činného	činný	k2eAgNnSc2d1	činné
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Obdělávaná	obdělávaný	k2eAgFnSc1d1	obdělávaná
půda	půda	k1gFnSc1	půda
tvoří	tvořit	k5eAaImIp3nS	tvořit
asi	asi	k9	asi
56	[number]	k4	56
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Dánské	dánský	k2eAgNnSc1d1	dánské
zemědělství	zemědělství	k1gNnSc1	zemědělství
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
velkou	velký	k2eAgFnSc7d1	velká
intenzitou	intenzita	k1gFnSc7	intenzita
<g/>
,	,	kIx,	,
vysokou	vysoký	k2eAgFnSc7d1	vysoká
produktivitou	produktivita	k1gFnSc7	produktivita
a	a	k8xC	a
mechanizací	mechanizace	k1gFnSc7	mechanizace
a	a	k8xC	a
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
spotřebou	spotřeba	k1gFnSc7	spotřeba
umělých	umělý	k2eAgNnPc2d1	umělé
hnojiv	hnojivo	k1gNnPc2	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Pěstují	pěstovat	k5eAaImIp3nP	pěstovat
se	se	k3xPyFc4	se
obilniny	obilnina	k1gFnSc2	obilnina
(	(	kIx(	(
<g/>
pšenice	pšenice	k1gFnSc2	pšenice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brambory	brambor	k1gInPc1	brambor
a	a	k8xC	a
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
výroby	výroba	k1gFnSc2	výroba
je	být	k5eAaImIp3nS	být
chov	chov	k1gInSc4	chov
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
prasat	prase	k1gNnPc2	prase
a	a	k8xC	a
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
je	být	k5eAaImIp3nS	být
vedoucím	vedoucí	k2eAgMnSc7d1	vedoucí
producentem	producent	k1gMnSc7	producent
vepřového	vepřové	k1gNnSc2	vepřové
a	a	k8xC	a
největším	veliký	k2eAgMnSc7d3	veliký
vývozcem	vývozce	k1gMnSc7	vývozce
vepřových	vepřový	k2eAgInPc2d1	vepřový
produktů	produkt	k1gInPc2	produkt
v	v	k7c6	v
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgMnSc1d1	významný
je	být	k5eAaImIp3nS	být
mořský	mořský	k2eAgInSc1d1	mořský
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
má	mít	k5eAaImIp3nS	mít
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
černé	černý	k2eAgNnSc1d1	černé
uhlí	uhlí	k1gNnSc1	uhlí
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgInPc1d1	stavební
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
elektráren	elektrárna	k1gFnPc2	elektrárna
je	být	k5eAaImIp3nS	být
tepelných	tepelný	k2eAgFnPc2d1	tepelná
<g/>
.	.	kIx.	.
</s>
<s>
Specifický	specifický	k2eAgMnSc1d1	specifický
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
podíl	podíl	k1gInSc1	podíl
větrných	větrný	k2eAgFnPc2d1	větrná
elektráren	elektrárna	k1gFnPc2	elektrárna
na	na	k7c6	na
spotřebě	spotřeba	k1gFnSc6	spotřeba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
25,9	[number]	k4	25,9
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
zastoupení	zastoupení	k1gNnSc1	zastoupení
má	mít	k5eAaImIp3nS	mít
průmysl	průmysl	k1gInSc1	průmysl
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
(	(	kIx(	(
<g/>
máslo	máslo	k1gNnSc1	máslo
<g/>
,	,	kIx,	,
sýry	sýr	k1gInPc1	sýr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
(	(	kIx(	(
<g/>
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
stroje	stroj	k1gInPc4	stroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc1d1	chemický
(	(	kIx(	(
<g/>
umělá	umělý	k2eAgNnPc1d1	umělé
hnojiva	hnojivo	k1gNnPc1	hnojivo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dřevozpracující	dřevozpracující	k2eAgInSc1d1	dřevozpracující
a	a	k8xC	a
elektrotechnický	elektrotechnický	k2eAgInSc1d1	elektrotechnický
<g/>
.	.	kIx.	.
</s>
<s>
Takřka	takřka	k6eAd1	takřka
polovina	polovina	k1gFnSc1	polovina
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
kapacit	kapacita	k1gFnPc2	kapacita
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
a	a	k8xC	a
nejbližším	blízký	k2eAgNnSc6d3	nejbližší
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
pracuje	pracovat	k5eAaImIp3nS	pracovat
přes	přes	k7c4	přes
60	[number]	k4	60
%	%	kIx~	%
ekonomicky	ekonomicky	k6eAd1	ekonomicky
činného	činný	k2eAgNnSc2d1	činné
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
postavení	postavení	k1gNnSc1	postavení
má	mít	k5eAaImIp3nS	mít
námořní	námořní	k2eAgFnSc1d1	námořní
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
plavba	plavba	k1gFnSc1	plavba
a	a	k8xC	a
automobilová	automobilový	k2eAgFnSc1d1	automobilová
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
spojují	spojovat	k5eAaImIp3nP	spojovat
mosty	most	k1gInPc4	most
nebo	nebo	k8xC	nebo
trajektové	trajektový	k2eAgFnPc4d1	trajektová
linky	linka	k1gFnPc4	linka
<g/>
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
dopravní	dopravní	k2eAgFnSc7d1	dopravní
spojnicí	spojnice	k1gFnSc7	spojnice
mezi	mezi	k7c7	mezi
Skandinávií	Skandinávie	k1gFnSc7	Skandinávie
a	a	k8xC	a
zbytkem	zbytek	k1gInSc7	zbytek
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
přístavy	přístav	k1gInPc7	přístav
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Kodaň	Kodaň	k1gFnSc1	Kodaň
<g/>
,	,	kIx,	,
Esbjerg	Esbjerg	k1gInSc1	Esbjerg
<g/>
,	,	kIx,	,
Å	Å	k?	Å
<g/>
,	,	kIx,	,
Å	Å	k?	Å
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
je	být	k5eAaImIp3nS	být
také	také	k9	také
významné	významný	k2eAgNnSc4d1	významné
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Dánska	Dánsko	k1gNnSc2	Dánsko
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
aglomeraci	aglomerace	k1gFnSc6	aglomerace
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Přirozený	přirozený	k2eAgInSc1d1	přirozený
přírůstek	přírůstek	k1gInSc1	přírůstek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
0,28	[number]	k4	0,28
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
79	[number]	k4	79
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
je	být	k5eAaImIp3nS	být
národním	národní	k2eAgInSc7d1	národní
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
94	[number]	k4	94
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
Dánové	Dán	k1gMnPc1	Dán
<g/>
,	,	kIx,	,
západogermánský	západogermánský	k2eAgInSc1d1	západogermánský
národ	národ	k1gInSc1	národ
<g/>
,	,	kIx,	,
jazykově	jazykově	k6eAd1	jazykově
patřící	patřící	k2eAgMnSc1d1	patřící
k	k	k7c3	k
severogermánské	severogermánský	k2eAgFnSc3d1	severogermánský
skupině	skupina	k1gFnSc3	skupina
indoevropské	indoevropský	k2eAgFnSc2d1	indoevropská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
potomky	potomek	k1gMnPc4	potomek
germánského	germánský	k2eAgInSc2d1	germánský
kmene	kmen	k1gInSc2	kmen
Dánů	Dán	k1gMnPc2	Dán
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
poloostrov	poloostrov	k1gInSc4	poloostrov
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
z	z	k7c2	z
jižního	jižní	k2eAgNnSc2d1	jižní
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Vytlačili	vytlačit	k5eAaPmAgMnP	vytlačit
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
si	se	k3xPyFc3	se
podmanili	podmanit	k5eAaPmAgMnP	podmanit
původní	původní	k2eAgMnPc4d1	původní
obyvatele	obyvatel	k1gMnPc4	obyvatel
(	(	kIx(	(
<g/>
Angly	Angl	k1gMnPc4	Angl
<g/>
,	,	kIx,	,
Juty	juta	k1gFnPc4	juta
<g/>
,	,	kIx,	,
Sasy	Sas	k1gMnPc4	Sas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
podnikali	podnikat	k5eAaImAgMnP	podnikat
výpravy	výprava	k1gFnSc2	výprava
jako	jako	k8xS	jako
Vikingové	Viking	k1gMnPc1	Viking
<g/>
.	.	kIx.	.
</s>
<s>
Etnicky	etnicky	k6eAd1	etnicky
a	a	k8xC	a
kulturně	kulturně	k6eAd1	kulturně
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
obyvatele	obyvatel	k1gMnSc4	obyvatel
Západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Normandie	Normandie	k1gFnSc1	Normandie
<g/>
,	,	kIx,	,
Island	Island	k1gInSc1	Island
i	i	k8xC	i
Grónsko	Grónsko	k1gNnSc1	Grónsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
památky	památka	k1gFnPc1	památka
jsou	být	k5eAaImIp3nP	být
psané	psaný	k2eAgFnPc1d1	psaná
runami	runa	k1gFnPc7	runa
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
píší	psát	k5eAaImIp3nP	psát
latinkou	latinka	k1gFnSc7	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gFnPc2	on
žije	žít	k5eAaImIp3nS	žít
u	u	k7c2	u
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
malá	malý	k2eAgFnSc1d1	malá
německá	německý	k2eAgFnSc1d1	německá
menšina	menšina	k1gFnSc1	menšina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
žije	žít	k5eAaImIp3nS	žít
i	i	k9	i
komunita	komunita	k1gFnSc1	komunita
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Dánsko	Dánsko	k1gNnSc1	Dánsko
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
multikulturní	multikulturní	k2eAgFnSc4d1	multikulturní
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
emigrantů	emigrant	k1gMnPc2	emigrant
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
9,5	[number]	k4	9,5
<g/>
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
cca	cca	kA	cca
526	[number]	k4	526
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
skupinu	skupina	k1gFnSc4	skupina
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
následují	následovat	k5eAaImIp3nP	následovat
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
z	z	k7c2	z
Bosny	Bosna	k1gFnSc2	Bosna
<g/>
,	,	kIx,	,
Kosova	Kosův	k2eAgFnSc1d1	Kosova
<g/>
,	,	kIx,	,
Maroka	Maroko	k1gNnSc2	Maroko
<g/>
,	,	kIx,	,
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
,	,	kIx,	,
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
z	z	k7c2	z
Chile	Chile	k1gNnSc2	Chile
či	či	k8xC	či
z	z	k7c2	z
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Malou	malý	k2eAgFnSc4d1	malá
skupinu	skupina	k1gFnSc4	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
i	i	k9	i
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
pobaltských	pobaltský	k2eAgFnPc2d1	pobaltská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
státní	státní	k2eAgFnSc7d1	státní
církví	církev	k1gFnSc7	církev
od	od	k7c2	od
reformace	reformace	k1gFnSc2	reformace
dánská	dánský	k2eAgFnSc1d1	dánská
(	(	kIx(	(
<g/>
evangelická	evangelický	k2eAgFnSc1d1	evangelická
<g/>
)	)	kIx)	)
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
vliv	vliv	k1gInSc1	vliv
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
i	i	k9	i
pomalu	pomalu	k6eAd1	pomalu
klesá	klesat	k5eAaImIp3nS	klesat
počet	počet	k1gInSc1	počet
jejích	její	k3xOp3gMnPc2	její
příslušníků	příslušník	k1gMnPc2	příslušník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
4	[number]	k4	4
430	[number]	k4	430
643	[number]	k4	643
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představovalo	představovat	k5eAaImAgNnS	představovat
79,1	[number]	k4	79,1
<g/>
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
89,3	[number]	k4	89,3
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Snižuje	snižovat	k5eAaImIp3nS	snižovat
se	se	k3xPyFc4	se
i	i	k9	i
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
bohoslužeb	bohoslužba	k1gFnPc2	bohoslužba
a	a	k8xC	a
členství	členství	k1gNnSc2	členství
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
formální	formální	k2eAgInSc1d1	formální
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
narůstá	narůstat	k5eAaImIp3nS	narůstat
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
emigrantů	emigrant	k1gMnPc2	emigrant
stoupá	stoupat	k5eAaImIp3nS	stoupat
počet	počet	k1gInSc1	počet
muslimů	muslim	k1gMnPc2	muslim
(	(	kIx(	(
<g/>
Turci	Turek	k1gMnPc1	Turek
<g/>
,	,	kIx,	,
Arabové	Arab	k1gMnPc1	Arab
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Islám	islám	k1gInSc1	islám
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
druhým	druhý	k4xOgInSc7	druhý
nejrozšířenějším	rozšířený	k2eAgNnSc7d3	nejrozšířenější
náboženstvím	náboženství	k1gNnSc7	náboženství
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
a	a	k8xC	a
počet	počet	k1gInSc1	počet
jeho	jeho	k3xOp3gMnPc2	jeho
přívrženců	přívrženec	k1gMnPc2	přívrženec
stále	stále	k6eAd1	stále
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
270	[number]	k4	270
000	[number]	k4	000
muslimů	muslim	k1gMnPc2	muslim
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gInPc2	on
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
i	i	k9	i
skupina	skupina	k1gFnSc1	skupina
buddhistů	buddhista	k1gMnPc2	buddhista
(	(	kIx(	(
<g/>
20	[number]	k4	20
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
tj	tj	kA	tj
0,4	[number]	k4	0,4
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomalu	pomalu	k6eAd1	pomalu
stoupá	stoupat	k5eAaImIp3nS	stoupat
i	i	k9	i
počet	počet	k1gInSc1	počet
příslušníků	příslušník	k1gMnPc2	příslušník
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
39	[number]	k4	39
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
asi	asi	k9	asi
0,7	[number]	k4	0,7
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
reformace	reformace	k1gFnSc2	reformace
bylo	být	k5eAaImAgNnS	být
Dánsko	Dánsko	k1gNnSc1	Dánsko
římskokatolickou	římskokatolický	k2eAgFnSc7d1	Římskokatolická
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
žijí	žít	k5eAaImIp3nP	žít
i	i	k9	i
skupinky	skupinka	k1gFnPc1	skupinka
hinduistů	hinduista	k1gMnPc2	hinduista
<g/>
,	,	kIx,	,
mormonů	mormon	k1gMnPc2	mormon
<g/>
,	,	kIx,	,
baptistů	baptista	k1gMnPc2	baptista
či	či	k8xC	či
pravoslavných	pravoslavný	k2eAgMnPc2d1	pravoslavný
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kultura	kultura	k1gFnSc1	kultura
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
Dánem	Dán	k1gMnSc7	Dán
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Hans	Hans	k1gMnSc1	Hans
Christian	Christian	k1gMnSc1	Christian
Andersen	Andersen	k1gMnSc1	Andersen
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
pohádky	pohádka	k1gFnPc4	pohádka
zná	znát	k5eAaImIp3nS	znát
celý	celý	k2eAgInSc1d1	celý
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
významní	významný	k2eAgMnPc1d1	významný
Dánové	Dán	k1gMnPc1	Dán
<g/>
:	:	kIx,	:
Niels	Niels	k1gInSc1	Niels
Bohr	Bohr	k1gInSc1	Bohr
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
;	;	kIx,	;
Tycho	Tyc	k1gMnSc4	Tyc
Brahe	Brah	k1gMnSc4	Brah
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
<g/>
;	;	kIx,	;
Karen	Karen	k1gInSc1	Karen
Blixenová	Blixenová	k1gFnSc1	Blixenová
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
;	;	kIx,	;
Sø	Sø	k1gMnSc1	Sø
Kierkegaard	Kierkegaard	k1gMnSc1	Kierkegaard
<g/>
,	,	kIx,	,
existencialistický	existencialistický	k2eAgMnSc1d1	existencialistický
filosof	filosof	k1gMnSc1	filosof
<g/>
;	;	kIx,	;
Lars	Lars	k1gInSc1	Lars
von	von	k1gInSc1	von
Trier	Trier	k1gInSc4	Trier
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
;	;	kIx,	;
Lars	Larsa	k1gFnPc2	Larsa
Ulrich	Ulrich	k1gMnSc1	Ulrich
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
<g/>
,	,	kIx,	,
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
skupiny	skupina	k1gFnSc2	skupina
Metallica	Metallica	k1gMnSc1	Metallica
Mads	Madsa	k1gFnPc2	Madsa
Mikkelsen	Mikkelsen	k1gInSc1	Mikkelsen
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Coster-Waldau	Coster-Waldaa	k1gFnSc4	Coster-Waldaa
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Viggo	Viggo	k6eAd1	Viggo
Mortensen	Mortensen	k2eAgMnSc1d1	Mortensen
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Caroline	Carolin	k1gInSc5	Carolin
Wozniacká	Wozniacký	k2eAgFnSc1d1	Wozniacká
<g/>
,	,	kIx,	,
tenistka	tenistka	k1gFnSc1	tenistka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fotografie	fotografia	k1gFnSc2	fotografia
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníci	průkopník	k1gMnPc1	průkopník
umění	umění	k1gNnSc2	umění
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byli	být	k5eAaImAgMnP	být
Mads	Mads	k1gInSc4	Mads
Alstrup	Alstrup	k1gMnSc1	Alstrup
a	a	k8xC	a
Georg	Georg	k1gMnSc1	Georg
Emil	Emil	k1gMnSc1	Emil
Hansen	Hansna	k1gFnPc2	Hansna
<g/>
,	,	kIx,	,
připravili	připravit	k5eAaPmAgMnP	připravit
cestu	cesta	k1gFnSc4	cesta
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
rychle	rychle	k6eAd1	rychle
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
profesi	profese	k1gFnSc4	profese
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
byli	být	k5eAaImAgMnP	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávanými	uznávaný	k2eAgFnPc7d1	uznávaná
zároveň	zároveň	k6eAd1	zároveň
uměleckými	umělecký	k2eAgFnPc7d1	umělecká
a	a	k8xC	a
novinářskými	novinářský	k2eAgMnPc7d1	novinářský
fotografy	fotograf	k1gMnPc7	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejúspěšnější	úspěšný	k2eAgMnPc4d3	nejúspěšnější
současné	současný	k2eAgMnPc4d1	současný
dánské	dánský	k2eAgMnPc4d1	dánský
fotografy	fotograf	k1gMnPc4	fotograf
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
Jacob	Jacoba	k1gFnPc2	Jacoba
Aue	Aue	k1gMnSc1	Aue
Sobol	Sobol	k1gMnSc1	Sobol
nebo	nebo	k8xC	nebo
Claus	Claus	k1gMnSc1	Claus
Bjø	Bjø	k1gFnPc2	Bjø
Larsen	larsena	k1gFnPc2	larsena
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
výstav	výstava	k1gFnPc2	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
LGBT	LGBT	kA	LGBT
práva	právo	k1gNnSc2	právo
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
přes	přes	k7c4	přes
Velký	velký	k2eAgInSc4d1	velký
Belt	Belt	k1gInSc4	Belt
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgFnSc1d1	spojující
ostrovy	ostrov	k1gInPc4	ostrov
Sjæ	Sjæ	k1gMnPc2	Sjæ
a	a	k8xC	a
Fyn	Fyn	k1gMnPc2	Fyn
<g/>
,	,	kIx,	,
uvedený	uvedený	k2eAgInSc4d1	uvedený
v	v	k7c4	v
provoz	provoz	k1gInSc4	provoz
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
-	-	kIx~	-
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druhý	druhý	k4xOgInSc4	druhý
nejdelší	dlouhý	k2eAgInSc4d3	nejdelší
visutý	visutý	k2eAgInSc4d1	visutý
most	most	k1gInSc4	most
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
;	;	kIx,	;
Zábavní	zábavní	k2eAgInSc4d1	zábavní
park	park	k1gInSc4	park
Tivoli	Tivole	k1gFnSc4	Tivole
-	-	kIx~	-
nejstarší	starý	k2eAgInSc1d3	nejstarší
zábavní	zábavní	k2eAgInSc1d1	zábavní
park	park	k1gInSc1	park
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
;	;	kIx,	;
Z	z	k7c2	z
Dánska	Dánsko	k1gNnSc2	Dánsko
pochází	pocházet	k5eAaImIp3nS	pocházet
stavebnice	stavebnice	k1gFnSc1	stavebnice
Lego	lego	k1gNnSc4	lego
<g/>
.	.	kIx.	.
</s>
