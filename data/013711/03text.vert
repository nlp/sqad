<s>
Ústav	ústav	k1gInSc1
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
</s>
<s>
Ústavy	ústav	k1gInPc1
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
byly	být	k5eAaImAgFnP
v	v	k7c6
systému	systém	k1gInSc6
československé	československý	k2eAgFnSc2d1
veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
instituce	instituce	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
zřízeny	zřídit	k5eAaPmNgFnP
národními	národní	k2eAgFnPc7d1
výbory	výbor	k1gInPc1
pro	pro	k7c4
výkon	výkon	k1gInSc4
jejich	jejich	k3xOp3gFnSc2
působnosti	působnost	k1gFnSc2
na	na	k7c6
úseku	úsek	k1gInSc6
zdravotnictví	zdravotnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
první	první	k4xOgFnSc6
fázi	fáze	k1gFnSc6
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
zařízení	zařízení	k1gNnPc2
poskytující	poskytující	k2eAgFnSc4d1
poradenskou	poradenský	k2eAgFnSc4d1
zdravotní	zdravotní	k2eAgFnSc4d1
péči	péče	k1gFnSc4
<g/>
,	,	kIx,
posléze	posléze	k6eAd1
se	se	k3xPyFc4
jejich	jejich	k3xOp3gFnSc1
povaha	povaha	k1gFnSc1
změnila	změnit	k5eAaPmAgFnS
v	v	k7c6
instituci	instituce	k1gFnSc6
sdružující	sdružující	k2eAgNnPc1d1
a	a	k8xC
zastřešující	zastřešující	k2eAgNnPc1d1
zdravotnická	zdravotnický	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
v	v	k7c6
daném	daný	k2eAgInSc6d1
správním	správní	k2eAgInSc6d1
obvodě	obvod	k1gInSc6
(	(	kIx(
<g/>
okrese	okres	k1gInSc6
<g/>
,	,	kIx,
kraji	kraj	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ústavy	ústav	k1gInPc1
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
do	do	k7c2
roku	rok	k1gInSc2
1952	#num#	k4
</s>
<s>
Ústavy	ústav	k1gInPc1
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
byly	být	k5eAaImAgInP
zřizovány	zřizován	k2eAgInPc1d1
okresními	okresní	k2eAgInPc7d1
národními	národní	k2eAgInPc7d1
výbory	výbor	k1gInPc7
na	na	k7c6
základě	základ	k1gInSc6
zákona	zákon	k1gInSc2
č.	č.	k?
49	#num#	k4
<g/>
/	/	kIx~
<g/>
1947	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
poradenské	poradenský	k2eAgFnSc6d1
zdravotní	zdravotní	k2eAgFnSc6d1
péči	péče	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Poslání	poslání	k1gNnSc1
</s>
<s>
Ze	z	k7c2
zákona	zákon	k1gInSc2
poskytovaly	poskytovat	k5eAaImAgFnP
preventivní	preventivní	k2eAgFnPc1d1
(	(	kIx(
<g/>
ochrannou	ochranný	k2eAgFnSc4d1
<g/>
)	)	kIx)
zdravotní	zdravotní	k2eAgFnSc4d1
péči	péče	k1gFnSc4
<g/>
:	:	kIx,
jejích	její	k3xOp3gMnPc2
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
odborně	odborně	k6eAd1
vyšetřovat	vyšetřovat	k5eAaImF
a	a	k8xC
sledovat	sledovat	k5eAaImF
zdravotní	zdravotní	k2eAgInSc4d1
stav	stav	k1gInSc4
občanů	občan	k1gMnPc2
a	a	k8xC
zjišťovat	zjišťovat	k5eAaImF
jejich	jejich	k3xOp3gInSc2
sociálně	sociálně	k6eAd1
zdravotní	zdravotní	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
ochraně	ochrana	k1gFnSc3
zdraví	zdraví	k1gNnSc2
občanů	občan	k1gMnPc2
měly	mít	k5eAaImAgInP
působit	působit	k5eAaImF
svou	svůj	k3xOyFgFnSc7
poradní	poradní	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
<g/>
,	,	kIx,
výchovou	výchova	k1gFnSc7
a	a	k8xC
zprostředkováním	zprostředkování	k1gNnSc7
nápravy	náprava	k1gFnSc2
zjištěných	zjištěný	k2eAgInPc2d1
sociálně	sociálně	k6eAd1
zdravotních	zdravotní	k2eAgFnPc2d1
závad	závada	k1gFnPc2
a	a	k8xC
potřebné	potřebný	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1
nepominutelnou	pominutelný	k2eNgFnSc7d1
oblastí	oblast	k1gFnSc7
byl	být	k5eAaImAgInS
pro	pro	k7c4
ústavy	ústav	k1gInPc4
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
boj	boj	k1gInSc1
proti	proti	k7c3
vzniku	vznik	k1gInSc3
<g/>
,	,	kIx,
rozšiřování	rozšiřování	k1gNnSc3
a	a	k8xC
následkům	následek	k1gInPc3
chorob	choroba	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
pohlavních	pohlavní	k2eAgInPc2d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
a	a	k8xC
dále	daleko	k6eAd2
alkoholismu	alkoholismus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Organizace	organizace	k1gFnSc1
</s>
<s>
Ústavy	ústav	k1gInPc1
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
se	se	k3xPyFc4
členily	členit	k5eAaImAgFnP
na	na	k7c4
ústředí	ústředí	k1gNnSc4
<g/>
,	,	kIx,
pobočky	pobočka	k1gFnPc4
a	a	k8xC
místní	místní	k2eAgFnPc4d1
poradny	poradna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Ústředí	ústředí	k1gNnSc1
a	a	k8xC
pobočky	pobočka	k1gFnPc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
členily	členit	k5eAaImAgInP
na	na	k7c4
zdravotní	zdravotní	k2eAgFnPc4d1
poradny	poradna	k1gFnPc4
a	a	k8xC
oddělení	oddělení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Poradny	poradna	k1gFnPc1
zajišťovaly	zajišťovat	k5eAaImAgFnP
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
</s>
<s>
veškeré	veškerý	k3xTgNnSc4
zdravotní	zdravotní	k2eAgNnSc4d1
poradenství	poradenství	k1gNnSc4
<g/>
,	,	kIx,
zdravotní	zdravotní	k2eAgFnSc4d1
evidenci	evidence	k1gFnSc4
všeobecnou	všeobecný	k2eAgFnSc4d1
a	a	k8xC
speciální	speciální	k2eAgFnSc4d1
<g/>
,	,	kIx,
sociálně	sociálně	k6eAd1
zdravotní	zdravotní	k2eAgFnSc4d1
statistiku	statistika	k1gFnSc4
<g/>
,	,	kIx,
zdravotnický	zdravotnický	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
a	a	k8xC
zdravotnickou	zdravotnický	k2eAgFnSc4d1
výchovu	výchova	k1gFnSc4
<g/>
,	,	kIx,
</s>
<s>
zdravotnické	zdravotnický	k2eAgFnPc4d1
prohlídky	prohlídka	k1gFnPc4
obyvatelstva	obyvatelstvo	k1gNnSc2
a	a	k8xC
veškerou	veškerý	k3xTgFnSc4
s	s	k7c7
tím	ten	k3xDgNnSc7
spojenou	spojený	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
</s>
<s>
zprostředkování	zprostředkování	k1gNnSc4
léčebné	léčebný	k2eAgFnSc2d1
péče	péče	k1gFnSc2
ústavní	ústavní	k2eAgFnSc2d1
a	a	k8xC
ambulatorní	ambulatorní	k2eAgFnSc2d1
<g/>
,	,	kIx,
a	a	k8xC
dále	daleko	k6eAd2
zprostředkování	zprostředkování	k1gNnSc4
péče	péče	k1gFnSc2
rekreační	rekreační	k2eAgInPc1d1
a	a	k8xC
podpůrné	podpůrný	k2eAgInPc1d1
<g/>
,	,	kIx,
</s>
<s>
hromadnou	hromadný	k2eAgFnSc4d1
bezplatnou	bezplatný	k2eAgFnSc4d1
léčebnou	léčebný	k2eAgFnSc4d1
akci	akce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Oddělením	oddělení	k1gNnSc7
příslušelo	příslušet	k5eAaImAgNnS
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
</s>
<s>
organizovat	organizovat	k5eAaBmF
ošetřovatelskou	ošetřovatelský	k2eAgFnSc4d1
a	a	k8xC
zdravotní	zdravotní	k2eAgFnSc4d1
službu	služba	k1gFnSc4
v	v	k7c6
rodinách	rodina	k1gFnPc6
<g/>
,	,	kIx,
sociálně	sociálně	k6eAd1
zdravotní	zdravotní	k2eAgFnSc4d1
službu	služba	k1gFnSc4
v	v	k7c6
léčebných	léčebný	k2eAgInPc6d1
a	a	k8xC
ošetřovacích	ošetřovací	k2eAgInPc6d1
ústavech	ústav	k1gInPc6
<g/>
,	,	kIx,
pomocnou	pomocný	k2eAgFnSc4d1
zdravotní	zdravotní	k2eAgFnSc4d1
záchrannou	záchranný	k2eAgFnSc4d1
službu	služba	k1gFnSc4
a	a	k8xC
ozdravnou	ozdravný	k2eAgFnSc4d1
péči	péče	k1gFnSc4
<g/>
,	,	kIx,
</s>
<s>
spolupracovat	spolupracovat	k5eAaImF
při	při	k7c6
provádění	provádění	k1gNnSc6
hygienických	hygienický	k2eAgInPc2d1
a	a	k8xC
epidemiologických	epidemiologický	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
a	a	k8xC
na	na	k7c6
pracovních	pracovní	k2eAgInPc6d1
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
a	a	k8xC
dále	daleko	k6eAd2
i	i	k9
v	v	k7c6
oblasti	oblast	k1gFnSc6
hygieny	hygiena	k1gFnSc2
výživy	výživa	k1gFnSc2
<g/>
,	,	kIx,
</s>
<s>
provádět	provádět	k5eAaImF
diagnostické	diagnostický	k2eAgInPc4d1
a	a	k8xC
laboratorní	laboratorní	k2eAgFnPc4d1
práce	práce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Ústavy	ústava	k1gFnPc1
povinně	povinně	k6eAd1
zřizovaly	zřizovat	k5eAaImAgFnP
poradny	poradna	k1gFnPc1
pro	pro	k7c4
těhotné	těhotný	k2eAgFnPc4d1
ženy	žena	k1gFnPc4
<g/>
,	,	kIx,
pro	pro	k7c4
matky	matka	k1gFnPc4
a	a	k8xC
děti	dítě	k1gFnPc4
(	(	kIx(
<g/>
včetně	včetně	k7c2
školního	školní	k2eAgNnSc2d1
lékařství	lékařství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protituberkulózní	protituberkulózní	k2eAgFnSc2d1
a	a	k8xC
poradny	poradna	k1gFnSc2
proti	proti	k7c3
pohlavním	pohlavní	k2eAgFnPc3d1
nemocem	nemoc	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Vyžadovaly	vyžadovat	k5eAaImAgFnP
si	se	k3xPyFc3
toho	ten	k3xDgNnSc2
poměry	poměr	k1gInPc1
<g/>
,	,	kIx,
zřizovaly	zřizovat	k5eAaImAgInP
ústavy	ústav	k1gInPc1
i	i	k8xC
další	další	k2eAgFnPc1d1
specializované	specializovaný	k2eAgFnPc1d1
poradny	poradna	k1gFnPc1
jako	jako	k8xC,k8xS
<g/>
:	:	kIx,
</s>
<s>
pro	pro	k7c4
péči	péče	k1gFnSc4
o	o	k7c4
chrup	chrup	k1gInSc4
<g/>
,	,	kIx,
předsňatkové	předsňatkový	k2eAgFnPc1d1
(	(	kIx(
<g/>
eugenické	eugenický	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tělovýchovné	tělovýchovný	k2eAgFnPc1d1
<g/>
,	,	kIx,
pro	pro	k7c4
duševní	duševní	k2eAgFnSc4d1
hygienu	hygiena	k1gFnSc4
a	a	k8xC
výchovu	výchova	k1gFnSc4
<g/>
,	,	kIx,
pro	pro	k7c4
bezdětné	bezdětný	k2eAgMnPc4d1
manžely	manžel	k1gMnPc4
<g/>
,	,	kIx,
gynekologické	gynekologický	k2eAgFnPc1d1
<g/>
,	,	kIx,
pro	pro	k7c4
choroby	choroba	k1gFnPc4
srdce	srdce	k1gNnSc2
a	a	k8xC
cév	céva	k1gFnPc2
<g/>
,	,	kIx,
pro	pro	k7c4
výživu	výživa	k1gFnSc4
a	a	k8xC
dietetiku	dietetika	k1gFnSc4
<g/>
,	,	kIx,
pro	pro	k7c4
potírání	potírání	k1gNnSc4
revmatismu	revmatismus	k1gInSc2
<g/>
,	,	kIx,
zhoubných	zhoubný	k2eAgInPc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
nádorů	nádor	k1gInPc2
<g/>
,	,	kIx,
lupusu	lupus	k1gInSc2
<g/>
,	,	kIx,
trachomu	trachom	k1gInSc2
<g/>
,	,	kIx,
malárie	malárie	k1gFnSc2
<g/>
,	,	kIx,
cukrovky	cukrovka	k1gFnSc2
<g/>
,	,	kIx,
chorob	choroba	k1gFnPc2
žláz	žláza	k1gFnPc2
s	s	k7c7
vnitřní	vnitřní	k2eAgFnSc7d1
sekrecí	sekrece	k1gFnSc7
<g/>
,	,	kIx,
chorob	choroba	k1gFnPc2
nervových	nervový	k2eAgFnPc2d1
<g/>
,	,	kIx,
krevních	krevní	k2eAgFnPc2d1
<g/>
,	,	kIx,
pro	pro	k7c4
boj	boj	k1gInSc4
proti	proti	k7c3
alkoholismu	alkoholismus	k1gInSc3
<g/>
,	,	kIx,
pro	pro	k7c4
duševně	duševně	k6eAd1
vadné	vadný	k2eAgFnPc4d1
<g/>
,	,	kIx,
pro	pro	k7c4
vady	vada	k1gFnPc4
sluchu	sluch	k1gInSc2
<g/>
,	,	kIx,
řeči	řeč	k1gFnSc2
a	a	k8xC
zraku	zrak	k1gInSc2
<g/>
,	,	kIx,
pro	pro	k7c4
ortopedii	ortopedie	k1gFnSc4
<g/>
,	,	kIx,
pro	pro	k7c4
tělesně	tělesně	k6eAd1
vadné	vadný	k2eAgFnPc4d1
<g/>
,	,	kIx,
pro	pro	k7c4
nemoci	nemoc	k1gFnPc4
stáří	stáří	k1gNnSc2
atd.	atd.	kA
</s>
<s>
Ústavy	ústav	k1gInPc1
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
v	v	k7c6
letech	let	k1gInPc6
1952	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
</s>
<s>
Novou	nový	k2eAgFnSc4d1
povahu	povaha	k1gFnSc4
ústavům	ústav	k1gInPc3
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
vtiskl	vtisknout	k5eAaPmAgMnS
zákon	zákon	k1gInSc4
č.	č.	k?
103	#num#	k4
<g/>
/	/	kIx~
<g/>
1951	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
jednotné	jednotný	k2eAgFnSc6d1
preventivní	preventivní	k2eAgFnSc3d1
a	a	k8xC
léčebné	léčebný	k2eAgFnSc3d1
péči	péče	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
něho	on	k3xPp3gMnSc2
se	se	k3xPyFc4
ústavy	ústava	k1gFnSc2
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
staly	stát	k5eAaPmAgFnP
výkonnými	výkonný	k2eAgMnPc7d1
orgány	orgán	k1gInPc7
národních	národní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
majících	mající	k2eAgInPc2d1
plánovat	plánovat	k5eAaImF
<g/>
,	,	kIx,
organizovat	organizovat	k5eAaBmF
<g/>
,	,	kIx,
řídit	řídit	k5eAaImF
a	a	k8xC
kontrolovat	kontrolovat	k5eAaImF
jednotnou	jednotný	k2eAgFnSc4d1
preventivní	preventivní	k2eAgFnSc4d1
a	a	k8xC
léčebnou	léčebný	k2eAgFnSc4d1
péči	péče	k1gFnSc4
v	v	k7c6
daném	daný	k2eAgInSc6d1
správním	správní	k2eAgInSc6d1
obvodě	obvod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgFnSc4d1
úpravu	úprava	k1gFnSc4
pak	pak	k6eAd1
obsahovalo	obsahovat	k5eAaImAgNnS
nařízení	nařízení	k1gNnSc1
ministra	ministr	k1gMnSc2
zdravotnictví	zdravotnictví	k1gNnSc2
č.	č.	k?
24	#num#	k4
<g/>
/	/	kIx~
<g/>
1952	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c4
organizaci	organizace	k1gFnSc4
preventivní	preventivní	k2eAgFnSc2d1
a	a	k8xC
léčebné	léčebný	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Organizace	organizace	k1gFnSc1
</s>
<s>
Ke	k	k7c3
sdružení	sdružení	k1gNnSc3
zdravotnických	zdravotnický	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
se	se	k3xPyFc4
přikročilo	přikročit	k5eAaPmAgNnS
„	„	k?
<g/>
v	v	k7c6
zájmu	zájem	k1gInSc6
prohloubení	prohloubení	k1gNnSc1
preventivní	preventivní	k2eAgFnSc2d1
a	a	k8xC
léčebné	léčebný	k2eAgFnSc2d1
péče	péče	k1gFnSc2
a	a	k8xC
hospodárnějšího	hospodárný	k2eAgNnSc2d2
zvládnutí	zvládnutí	k1gNnSc2
úkolů	úkol	k1gInPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Okresní	okresní	k2eAgInPc1d1
ústavy	ústav	k1gInPc1
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
(	(	kIx(
<g/>
OÚNZ	OÚNZ	kA
<g/>
)	)	kIx)
</s>
<s>
Podle	podle	k7c2
uvedeného	uvedený	k2eAgNnSc2d1
ministerského	ministerský	k2eAgNnSc2d1
nařízení	nařízení	k1gNnSc2
se	se	k3xPyFc4
všechny	všechen	k3xTgFnPc1
okresní	okresní	k2eAgFnPc1d1
nemocnice	nemocnice	k1gFnPc1
s	s	k7c7
okresními	okresní	k2eAgNnPc7d1
zdravotnickými	zdravotnický	k2eAgNnPc7d1
středisky	středisko	k1gNnPc7
<g/>
,	,	kIx,
porodnice	porodnice	k1gFnSc1
<g/>
,	,	kIx,
okresní	okresní	k2eAgNnPc1d1
zdravotnická	zdravotnický	k2eAgNnPc1d1
střediska	středisko	k1gNnPc1
<g/>
,	,	kIx,
obvodní	obvodní	k2eAgNnPc1d1
zdravotnická	zdravotnický	k2eAgNnPc1d1
střediska	středisko	k1gNnPc1
<g/>
,	,	kIx,
lékařské	lékařský	k2eAgFnPc1d1
stanice	stanice	k1gFnPc1
<g/>
,	,	kIx,
ženské	ženská	k1gFnPc1
poradny	poradna	k1gFnSc2
<g/>
,	,	kIx,
dětské	dětský	k2eAgFnSc2d1
poradny	poradna	k1gFnSc2
<g/>
,	,	kIx,
ošetřovatelské	ošetřovatelský	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
<g/>
,	,	kIx,
stanice	stanice	k1gFnSc2
záchranné	záchranný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
,	,	kIx,
noční	noční	k2eAgNnPc1d1
sanatoria	sanatorium	k1gNnPc1
a	a	k8xC
transfúzní	transfúzní	k2eAgFnPc1d1
stanice	stanice	k1gFnPc1
zřízené	zřízený	k2eAgFnPc1d1
mimo	mimo	k7c4
sídlo	sídlo	k1gNnSc4
krajského	krajský	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
sdružily	sdružit	k5eAaPmAgFnP
v	v	k7c6
okresním	okresní	k2eAgInSc6d1
ústavu	ústav	k1gInSc6
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
zařízení	zařízení	k1gNnSc1
okresního	okresní	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
(	(	kIx(
<g/>
městského	městský	k2eAgMnSc2d1
<g/>
,	,	kIx,
obvodního	obvodní	k2eAgInSc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Okresní	okresní	k2eAgInPc1d1
ústavy	ústav	k1gInPc1
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
zůstaly	zůstat	k5eAaPmAgFnP
základní	základní	k2eAgFnSc7d1
plánovací	plánovací	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
a	a	k8xC
jednotným	jednotný	k2eAgInSc7d1
organizačním	organizační	k2eAgInSc7d1
<g/>
,	,	kIx,
administrativním	administrativní	k2eAgInSc7d1
a	a	k8xC
hospodářským	hospodářský	k2eAgInSc7d1
celkem	celek	k1gInSc7
<g/>
,	,	kIx,
samostatně	samostatně	k6eAd1
rozpočtujícím	rozpočtující	k2eAgInSc7d1
i	i	k8xC
účtujícím	účtující	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
jeho	jeho	k3xOp3gNnSc6
čele	čelo	k1gNnSc6
stál	stát	k5eAaImAgMnS
ředitel	ředitel	k1gMnSc1
lékař	lékař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Nařízení	nařízení	k1gNnSc1
umožňovalo	umožňovat	k5eAaImAgNnS
i	i	k9
vznik	vznik	k1gInSc4
závodních	závodní	k2eAgInPc2d1
ústavů	ústav	k1gInPc2
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Krajské	krajský	k2eAgInPc1d1
ústavy	ústav	k1gInPc1
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
(	(	kIx(
<g/>
KÚNZ	KÚNZ	kA
<g/>
)	)	kIx)
</s>
<s>
Obdobou	obdoba	k1gFnSc7
okresních	okresní	k2eAgInPc2d1
ústavů	ústav	k1gInPc2
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
na	na	k7c6
krajské	krajský	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
byly	být	k5eAaImAgInP
krajské	krajský	k2eAgInPc1d1
ústavy	ústav	k1gInPc1
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ty	ten	k3xDgFnPc1
sdružovaly	sdružovat	k5eAaImAgFnP
krajské	krajský	k2eAgFnPc4d1
nemocnice	nemocnice	k1gFnPc4
s	s	k7c7
krajskými	krajský	k2eAgNnPc7d1
zdravotnickými	zdravotnický	k2eAgNnPc7d1
středisky	středisko	k1gNnPc7
a	a	k8xC
zpravidla	zpravidla	k6eAd1
i	i	k9
transfúzní	transfúzní	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
a	a	k8xC
stanice	stanice	k1gFnSc2
záchranné	záchranný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
v	v	k7c6
sídle	sídlo	k1gNnSc6
krajského	krajský	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
ústavů	ústav	k1gInPc2
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1990	#num#	k4
<g/>
,	,	kIx,
tj.	tj.	kA
dnem	dno	k1gNnSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nabyl	nabýt	k5eAaPmAgInS
účinnosti	účinnost	k1gFnSc2
zákon	zákon	k1gInSc1
č.	č.	k?
367	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
obcích	obec	k1gFnPc6
(	(	kIx(
<g/>
obecní	obecní	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
zákon	zákon	k1gInSc1
č.	č.	k?
425	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
okresních	okresní	k2eAgInPc6d1
úřadech	úřad	k1gInPc6
<g/>
,	,	kIx,
úpravě	úprava	k1gFnSc3
jejich	jejich	k3xOp3gFnSc2
působnosti	působnost	k1gFnSc2
a	a	k8xC
o	o	k7c6
některých	některý	k3yIgNnPc6
dalších	další	k2eAgNnPc6d1
opatřeních	opatření	k1gNnPc6
s	s	k7c7
tím	ten	k3xDgInSc7
souvisejících	související	k2eAgInPc2d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
ústavy	ústava	k1gFnSc2
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
staly	stát	k5eAaPmAgFnP
organizacemi	organizace	k1gFnPc7
okresních	okresní	k2eAgMnPc2d1
úřadů	úřada	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Nové	Nové	k2eAgInPc1d1
společenské	společenský	k2eAgInPc1d1
poměry	poměr	k1gInPc1
po	po	k7c6
sametové	sametový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
otevřely	otevřít	k5eAaPmAgFnP
dveře	dveře	k1gFnPc1
vzniku	vznik	k1gInSc2
zdravotnických	zdravotnický	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
s	s	k7c7
právní	právní	k2eAgFnSc7d1
subjektivitou	subjektivita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Vyhláška	vyhláška	k1gFnSc1
ministerstva	ministerstvo	k1gNnSc2
zdravotnictví	zdravotnictví	k1gNnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
č.	č.	k?
242	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
soustavě	soustava	k1gFnSc6
zdravotnických	zdravotnický	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
zřizovaných	zřizovaný	k2eAgFnPc2d1
okresními	okresní	k2eAgInPc7d1
úřady	úřad	k1gInPc7
a	a	k8xC
obcemi	obec	k1gFnPc7
<g/>
,	,	kIx,
s	s	k7c7
účinností	účinnost	k1gFnSc7
od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1991	#num#	k4
stanovila	stanovit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
právní	právní	k2eAgFnSc1d1
subjektivita	subjektivita	k1gFnSc1
u	u	k7c2
druhově	druhově	k6eAd1
vyjmenovaných	vyjmenovaný	k2eAgNnPc2d1
zdravotnických	zdravotnický	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
bude	být	k5eAaImBp3nS
napříště	napříště	k6eAd1
pravidlem	pravidlem	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Ústavy	ústav	k1gInPc1
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
zanikly	zaniknout	k5eAaPmAgInP
dovršením	dovršení	k1gNnSc7
delimitace	delimitace	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
v	v	k7c6
druhém	druhý	k4xOgInSc6
pololetí	pololetí	k1gNnSc6
roku	rok	k1gInSc2
1991	#num#	k4
<g/>
;	;	kIx,
delimitovali	delimitovat	k5eAaBmAgMnP
se	se	k3xPyFc4
jak	jak	k6eAd1
zaměstnanci	zaměstnanec	k1gMnPc1
<g/>
,	,	kIx,
tak	tak	k9
majetek	majetek	k1gInSc1
spravovaný	spravovaný	k2eAgInSc1d1
ústavy	ústav	k1gInPc4
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Srov.	srov.	kA
zákon	zákon	k1gInSc1
č.	č.	k?
158	#num#	k4
<g/>
/	/	kIx~
<g/>
1949	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
pohlavním	pohlavní	k2eAgFnPc3d1
nemocem	nemoc	k1gFnPc3
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Srov.	srov.	kA
zákon	zákon	k1gInSc1
č.	č.	k?
87	#num#	k4
<g/>
/	/	kIx~
<g/>
1948	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
potírání	potírání	k1gNnSc6
alkoholismu	alkoholismus	k1gInSc2
<g/>
.1	.1	k4
2	#num#	k4
Viz	vidět	k5eAaImRp2nS
§	§	k?
3	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
vládního	vládní	k2eAgNnSc2d1
nařízení	nařízení	k1gNnSc2
č.	č.	k?
219	#num#	k4
<g/>
/	/	kIx~
<g/>
1948	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
ústavech	ústav	k1gInPc6
národního	národní	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Viz	vidět	k5eAaImRp2nS
§	§	k?
21	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
nařízení	nařízení	k1gNnSc2
č.	č.	k?
24	#num#	k4
<g/>
/	/	kIx~
<g/>
1952	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
</s>
