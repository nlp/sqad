<p>
<s>
Atomové	atomový	k2eAgNnSc1d1	atomové
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
kladně	kladně	k6eAd1	kladně
nabitá	nabitý	k2eAgFnSc1d1	nabitá
část	část	k1gFnSc1	část
atomu	atom	k1gInSc2	atom
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
jeho	jeho	k3xOp3gNnSc4	jeho
hmotnostní	hmotnostní	k2eAgNnSc4d1	hmotnostní
i	i	k8xC	i
prostorové	prostorový	k2eAgNnSc4d1	prostorové
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Atomové	atomový	k2eAgNnSc1d1	atomové
jádro	jádro	k1gNnSc1	jádro
představuje	představovat	k5eAaImIp3nS	představovat
99,9	[number]	k4	99,9
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
jádra	jádro	k1gNnSc2	jádro
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
15	[number]	k4	15
m	m	kA	m
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
×	×	k?	×
méně	málo	k6eAd2	málo
než	než	k8xS	než
průměr	průměr	k1gInSc4	průměr
celého	celý	k2eAgInSc2d1	celý
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existence	existence	k1gFnSc1	existence
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
v	v	k7c6	v
Rutherfordově	Rutherfordův	k2eAgInSc6d1	Rutherfordův
experimentu	experiment	k1gInSc6	experiment
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
základě	základ	k1gInSc6	základ
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tzv.	tzv.	kA	tzv.
planetární	planetární	k2eAgInSc4d1	planetární
model	model	k1gInSc4	model
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Věci	věc	k1gFnPc1	věc
kolem	kolem	k7c2	kolem
nás	my	k3xPp1nPc2	my
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgFnP	složit
z	z	k7c2	z
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
látka	látka	k1gFnSc1	látka
z	z	k7c2	z
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
molekuly	molekula	k1gFnPc1	molekula
z	z	k7c2	z
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
atom	atom	k1gInSc1	atom
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
atomového	atomový	k2eAgInSc2d1	atomový
obalu	obal	k1gInSc2	obal
a	a	k8xC	a
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
nukleonů	nukleon	k1gInPc2	nukleon
<g/>
,	,	kIx,	,
těmi	ten	k3xDgInPc7	ten
jsou	být	k5eAaImIp3nP	být
neutrony	neutron	k1gInPc1	neutron
a	a	k8xC	a
kladně	kladně	k6eAd1	kladně
nabité	nabitý	k2eAgInPc1d1	nabitý
protony	proton	k1gInPc1	proton
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
kvarků	kvark	k1gInPc2	kvark
a	a	k8xC	a
gluonů	gluon	k1gInPc2	gluon
<g/>
.	.	kIx.	.
</s>
<s>
Nukleony	nukleon	k1gInPc1	nukleon
uvnitř	uvnitř	k7c2	uvnitř
jádra	jádro	k1gNnSc2	jádro
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
k	k	k7c3	k
sobě	se	k3xPyFc3	se
poutány	poutat	k5eAaImNgFnP	poutat
silami	síla	k1gFnPc7	síla
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
vznikají	vznikat	k5eAaImIp3nP	vznikat
mezi	mezi	k7c7	mezi
jejich	jejich	k3xOp3gFnPc7	jejich
podsložkami	podsložka	k1gFnPc7	podsložka
tedy	tedy	k8xC	tedy
mezi	mezi	k7c7	mezi
kvarky	kvark	k1gInPc7	kvark
a	a	k8xC	a
gluony	gluon	k1gInPc7	gluon
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
silná	silný	k2eAgFnSc1d1	silná
interakce	interakce	k1gFnSc1	interakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
protonů	proton	k1gInPc2	proton
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
lehké	lehký	k2eAgInPc4d1	lehký
prvky	prvek	k1gInPc4	prvek
zhruba	zhruba	k6eAd1	zhruba
roven	roven	k2eAgInSc1d1	roven
počtu	počet	k1gInSc3	počet
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rostoucím	rostoucí	k2eAgNnSc7d1	rostoucí
protonovým	protonový	k2eAgNnSc7d1	protonové
číslem	číslo	k1gNnSc7	číslo
roste	růst	k5eAaImIp3nS	růst
počet	počet	k1gInSc1	počet
neutronů	neutron	k1gInPc2	neutron
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
protonů	proton	k1gInPc2	proton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nukleony	nukleon	k1gInPc1	nukleon
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
nehybné	hybný	k2eNgNnSc1d1	nehybné
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
překračuje	překračovat	k5eAaImIp3nS	překračovat
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
</s>
</p>
<p>
<s>
===	===	k?	===
Kvantová	kvantový	k2eAgNnPc1d1	kvantové
čísla	číslo	k1gNnPc1	číslo
charakterizující	charakterizující	k2eAgNnSc1d1	charakterizující
jádro	jádro	k1gNnSc1	jádro
===	===	k?	===
</s>
</p>
<p>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
jádra	jádro	k1gNnSc2	jádro
se	se	k3xPyFc4	se
vystihují	vystihovat	k5eAaImIp3nP	vystihovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
atomového	atomový	k2eAgNnSc2d1	atomové
(	(	kIx(	(
<g/>
protonového	protonový	k2eAgNnSc2d1	protonové
<g/>
)	)	kIx)	)
čísla	číslo	k1gNnSc2	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Z	z	k7c2	z
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
které	který	k3yIgInPc4	který
určuje	určovat	k5eAaImIp3nS	určovat
počet	počet	k1gInSc1	počet
protonů	proton	k1gInPc2	proton
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
a	a	k8xC	a
nukleonového	nukleonový	k2eAgNnSc2d1	nukleonový
čísla	číslo	k1gNnSc2	číslo
(	(	kIx(	(
<g/>
hmotnostního	hmotnostní	k2eAgNnSc2d1	hmotnostní
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
udává	udávat	k5eAaImIp3nS	udávat
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
nukleonů	nukleon	k1gInPc2	nukleon
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
tzv.	tzv.	kA	tzv.
neutronové	neutronový	k2eAgNnSc1d1	neutronové
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
udávající	udávající	k2eAgInSc1d1	udávající
počet	počet	k1gInSc1	počet
neutronů	neutron	k1gInPc2	neutron
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
rovné	rovný	k2eAgFnPc4d1	rovná
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
=	=	kIx~	=
<g/>
A-Z	A-Z	k1gMnSc1	A-Z
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
atomy	atom	k1gInPc1	atom
s	s	k7c7	s
jádry	jádro	k1gNnPc7	jádro
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
nukleonové	nukleonový	k2eAgNnSc1d1	nukleonový
číslo	číslo	k1gNnSc1	číslo
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
238	[number]	k4	238
a	a	k8xC	a
atomovými	atomový	k2eAgNnPc7d1	atomové
čísly	číslo	k1gNnPc7	číslo
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
92	[number]	k4	92
<g/>
.	.	kIx.	.
</s>
<s>
Laboratorně	laboratorně	k6eAd1	laboratorně
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vytvořit	vytvořit	k5eAaPmF	vytvořit
i	i	k9	i
atomy	atom	k1gInPc1	atom
s	s	k7c7	s
většími	veliký	k2eAgNnPc7d2	veliký
jádry	jádro	k1gNnPc7	jádro
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
stabilní	stabilní	k2eAgFnPc1d1	stabilní
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozpadají	rozpadat	k5eAaPmIp3nP	rozpadat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
náboj	náboj	k1gInSc1	náboj
protonu	proton	k1gInSc2	proton
je	být	k5eAaImIp3nS	být
kladný	kladný	k2eAgInSc1d1	kladný
a	a	k8xC	a
neutron	neutron	k1gInSc1	neutron
je	být	k5eAaImIp3nS	být
elektricky	elektricky	k6eAd1	elektricky
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
atom	atom	k1gInSc4	atom
neutrální	neutrální	k2eAgFnPc1d1	neutrální
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
počet	počet	k1gInSc4	počet
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
elektronovém	elektronový	k2eAgInSc6d1	elektronový
obalu	obal	k1gInSc6	obal
roven	roven	k2eAgInSc1d1	roven
počtu	počet	k1gInSc6	počet
protonů	proton	k1gInPc2	proton
v	v	k7c6	v
atomovém	atomový	k2eAgNnSc6d1	atomové
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
protonů	proton	k1gInPc2	proton
však	však	k9	však
určuje	určovat	k5eAaImIp3nS	určovat
polohu	poloha	k1gFnSc4	poloha
atomu	atom	k1gInSc2	atom
v	v	k7c6	v
periodické	periodický	k2eAgFnSc6d1	periodická
tabulce	tabulka	k1gFnSc6	tabulka
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlastnosti	vlastnost	k1gFnPc1	vlastnost
atomů	atom	k1gInPc2	atom
jsou	být	k5eAaImIp3nP	být
velkou	velký	k2eAgFnSc7d1	velká
měrou	míra	k1gFnSc7wR	míra
určovány	určovat	k5eAaImNgFnP	určovat
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
jejich	jejich	k3xOp3gNnPc2	jejich
jader	jádro	k1gNnPc2	jádro
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
kvantová	kvantový	k2eAgNnPc1d1	kvantové
čísla	číslo	k1gNnPc1	číslo
charakterizující	charakterizující	k2eAgNnPc1d1	charakterizující
jádro	jádro	k1gNnSc4	jádro
používána	používán	k2eAgFnSc1d1	používána
ke	k	k7c3	k
schematickému	schematický	k2eAgNnSc3d1	schematické
označovaní	označovaný	k2eAgMnPc1d1	označovaný
vlastností	vlastnost	k1gFnPc2	vlastnost
atomů	atom	k1gInPc2	atom
pomocí	pomocí	k7c2	pomocí
zápisu	zápis	k1gInSc2	zápis
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
A	a	k9	a
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
představuje	představovat	k5eAaImIp3nS	představovat
značku	značka	k1gFnSc4	značka
chemického	chemický	k2eAgInSc2d1	chemický
prvku	prvek	k1gInSc2	prvek
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Z	z	k7c2	z
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
atomové	atomový	k2eAgNnSc1d1	atomové
číslo	číslo	k1gNnSc1	číslo
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
nukleonové	nukleonový	k2eAgNnSc1d1	nukleonový
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
11	[number]	k4	11
H	H	kA	H
</s>
</p>
<p>
<s>
,	,	kIx,	,
42	[number]	k4	42
He	he	k0	he
<g/>
,	,	kIx,	,
23592	[number]	k4	23592
U	u	k7c2	u
atd	atd	kA	atd
</s>
</p>
<p>
<s>
===	===	k?	===
Rozměry	rozměr	k1gInPc7	rozměr
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c4	za
poloměr	poloměr	k1gInSc4	poloměr
jádra	jádro	k1gNnSc2	jádro
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
ještě	ještě	k9	ještě
na	na	k7c4	na
nukleon	nukleon	k1gInSc4	nukleon
působí	působit	k5eAaImIp3nP	působit
jaderné	jaderný	k2eAgFnPc1d1	jaderná
síly	síla	k1gFnPc1	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
velkých	velký	k2eAgNnPc2d1	velké
jader	jádro	k1gNnPc2	jádro
(	(	kIx(	(
<g/>
např.	např.	kA	např.
uran	uran	k1gInSc1	uran
<g/>
,	,	kIx,	,
thorium	thorium	k1gNnSc1	thorium
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
poloměry	poloměr	k1gInPc1	poloměr
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
kolem	kolem	k7c2	kolem
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
14	[number]	k4	14
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Experimentálně	experimentálně	k6eAd1	experimentálně
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
objem	objem	k1gInSc1	objem
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
přímo	přímo	k6eAd1	přímo
úměrný	úměrný	k2eAgInSc1d1	úměrný
počtu	počet	k1gInSc3	počet
nukleonů	nukleon	k1gInPc2	nukleon
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jádro	jádro	k1gNnSc1	jádro
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
nukleonů	nukleon	k1gInPc2	nukleon
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
určuje	určovat	k5eAaImIp3nS	určovat
nukleonové	nukleonový	k2eAgNnSc1d1	nukleonový
číslo	číslo	k1gNnSc1	číslo
(	(	kIx(	(
<g/>
hmotnostní	hmotnostní	k2eAgNnSc1d1	hmotnostní
číslo	číslo	k1gNnSc1	číslo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
předpokládáme	předpokládat	k5eAaImIp1nP	předpokládat
kulový	kulový	k2eAgInSc4d1	kulový
tvar	tvar	k1gInSc4	tvar
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
jeho	on	k3xPp3gInSc4	on
objem	objem	k1gInSc4	objem
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V	v	k7c6	v
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
R	R	kA	R
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
úměrný	úměrný	k2eAgInSc1d1	úměrný
nukleonovému	nukleonový	k2eAgInSc3d1	nukleonový
číslu	číslo	k1gNnSc3	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Poloměr	poloměr	k1gInSc1	poloměr
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
určuje	určovat	k5eAaImIp3nS	určovat
ze	z	k7c2	z
vztahu	vztah	k1gInSc2	vztah
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R	R	kA	R
<g/>
=	=	kIx~	=
<g/>
R_	R_	k1gMnSc1	R_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
A	A	kA	A
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
R0	R0	k1gFnSc1	R0
=	=	kIx~	=
1,3	[number]	k4	1,3
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
15	[number]	k4	15
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
úměry	úměra	k1gFnSc2	úměra
však	však	k9	však
existují	existovat	k5eAaImIp3nP	existovat
četné	četný	k2eAgFnPc4d1	četná
výjimky	výjimka	k1gFnPc4	výjimka
-	-	kIx~	-
některé	některý	k3yIgInPc1	některý
izotopy	izotop	k1gInPc1	izotop
mají	mít	k5eAaImIp3nP	mít
jádra	jádro	k1gNnPc4	jádro
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgFnPc4d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
jádra	jádro	k1gNnSc2	jádro
deuteria	deuterium	k1gNnSc2	deuterium
a	a	k8xC	a
tricia	tricium	k1gNnSc2	tricium
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgMnPc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
jádro	jádro	k1gNnSc1	jádro
helia	helium	k1gNnSc2	helium
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jádra	jádro	k1gNnSc2	jádro
olova	olovo	k1gNnSc2	olovo
181	[number]	k4	181
<g/>
Pb	Pb	k1gMnPc2	Pb
a	a	k8xC	a
183	[number]	k4	183
<g/>
Pb	Pb	k1gFnPc2	Pb
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgInPc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
izotopy	izotop	k1gInPc1	izotop
182	[number]	k4	182
<g/>
Pb	Pb	k1gFnSc2	Pb
<g/>
,184	,184	k4	,184
<g/>
Pb	Pb	k1gFnPc2	Pb
186	[number]	k4	186
<g/>
Pb-	Pb-	k1gFnPc2	Pb-
<g/>
197	[number]	k4	197
<g/>
Pb	Pb	k1gFnPc2	Pb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
případem	případ	k1gInSc7	případ
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
halo-jádra	haloádra	k1gFnSc1	halo-jádra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgInPc1	některý
nukleony	nukleon	k1gInPc1	nukleon
vytlačeny	vytlačit	k5eAaPmNgInP	vytlačit
výrazně	výrazně	k6eAd1	výrazně
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
středu	střed	k1gInSc2	střed
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
jádro	jádro	k1gNnSc1	jádro
lithia	lithium	k1gNnSc2	lithium
11	[number]	k4	11
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
velké	velký	k2eAgNnSc1d1	velké
jako	jako	k8xS	jako
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
těžší	těžký	k2eAgNnSc4d2	těžší
jádro	jádro	k1gNnSc4	jádro
olova	olovo	k1gNnSc2	olovo
(	(	kIx(	(
<g/>
poloměr	poloměr	k1gInSc1	poloměr
přibližně	přibližně	k6eAd1	přibližně
3,5	[number]	k4	3,5
femtometru	femtometr	k1gInSc2	femtometr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tvar	tvar	k1gInSc1	tvar
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
===	===	k?	===
</s>
</p>
<p>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jádro	jádro	k1gNnSc1	jádro
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
kouli	koule	k1gFnSc4	koule
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
však	však	k9	však
tvar	tvar	k1gInSc1	tvar
jádra	jádro	k1gNnSc2	jádro
od	od	k7c2	od
ideální	ideální	k2eAgFnSc2d1	ideální
koule	koule	k1gFnSc2	koule
často	často	k6eAd1	často
mírně	mírně	k6eAd1	mírně
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Jádra	jádro	k1gNnPc1	jádro
tak	tak	k9	tak
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
nejen	nejen	k6eAd1	nejen
tvar	tvar	k1gInSc4	tvar
koule	koule	k1gFnSc2	koule
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zploštělého	zploštělý	k2eAgInSc2d1	zploštělý
elipsoidu	elipsoid	k1gInSc2	elipsoid
(	(	kIx(	(
<g/>
uhlík	uhlík	k1gInSc1	uhlík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protáhlého	protáhlý	k2eAgInSc2d1	protáhlý
elipsoidu	elipsoid	k1gInSc2	elipsoid
(	(	kIx(	(
<g/>
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgNnPc2d1	další
jader	jádro	k1gNnPc2	jádro
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
i	i	k9	i
složitějších	složitý	k2eAgNnPc2d2	složitější
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
jádra	jádro	k1gNnPc1	jádro
mohou	moct	k5eAaImIp3nP	moct
existovat	existovat	k5eAaImF	existovat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
tvarových	tvarový	k2eAgFnPc6d1	tvarová
modifikacích	modifikace	k1gFnPc6	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
186	[number]	k4	186
<g/>
Pb	Pb	k1gMnPc2	Pb
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
kulový	kulový	k2eAgInSc4d1	kulový
<g/>
,	,	kIx,	,
protáhlý	protáhlý	k2eAgInSc4d1	protáhlý
i	i	k8xC	i
zploštělý	zploštělý	k2eAgInSc4d1	zploštělý
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hmotnost	hmotnost	k1gFnSc1	hmotnost
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
===	===	k?	===
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
jádra	jádro	k1gNnSc2	jádro
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
pomocí	pomocí	k7c2	pomocí
atomové	atomový	k2eAgFnSc2d1	atomová
hmotnostní	hmotnostní	k2eAgFnSc2d1	hmotnostní
jednotky	jednotka	k1gFnSc2	jednotka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
u	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
u	u	k7c2	u
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yQgFnSc4	který
platí	platit	k5eAaImIp3nS	platit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
u	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
≈	≈	k?	≈
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
660539040	[number]	k4	660539040
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
20	[number]	k4	20
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
27	[number]	k4	27
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
kg	kg	kA	kg
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
u	u	k7c2	u
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
approx	approx	k1gInSc1	approx
1,660	[number]	k4	1,660
<g/>
539040	[number]	k4	539040
<g/>
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
kg	kg	kA	kg
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
hodnota	hodnota	k1gFnSc1	hodnota
z	z	k7c2	z
adjustace	adjustace	k1gFnSc2	adjustace
konstant	konstanta	k1gFnPc2	konstanta
CODATA	CODATA	kA	CODATA
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
směrodatná	směrodatný	k2eAgFnSc1d1	směrodatná
odchylka	odchylka	k1gFnSc1	odchylka
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
posledních	poslední	k2eAgNnPc2d1	poslední
2	[number]	k4	2
platných	platný	k2eAgFnPc2d1	platná
číslic	číslice	k1gFnPc2	číslice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Atomová	atomový	k2eAgFnSc1d1	atomová
hmotnostní	hmotnostní	k2eAgFnSc1d1	hmotnostní
jednotka	jednotka	k1gFnSc1	jednotka
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
rovna	roven	k2eAgFnSc1d1	rovna
hmotnosti	hmotnost	k1gFnSc2	hmotnost
jednoho	jeden	k4xCgInSc2	jeden
nukleonu	nukleon	k1gInSc2	nukleon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
jádra	jádro	k1gNnSc2	jádro
dobře	dobře	k6eAd1	dobře
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
počet	počet	k1gInSc1	počet
jeho	jeho	k3xOp3gInPc2	jeho
nukleonů	nukleon	k1gInPc2	nukleon
daný	daný	k2eAgInSc1d1	daný
nukleonovým	nukleonův	k2eAgNnSc7d1	nukleonův
číslem	číslo	k1gNnSc7	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
hmotnost	hmotnost	k1gFnSc1	hmotnost
atomu	atom	k1gInSc2	atom
daného	daný	k2eAgInSc2d1	daný
nuklidu	nuklid	k1gInSc2	nuklid
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
u	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
u	u	k7c2	u
<g/>
}	}	kIx)	}
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zaokrouhlena	zaokrouhlit	k5eAaPmNgFnS	zaokrouhlit
na	na	k7c4	na
celé	celý	k2eAgNnSc4d1	celé
číslo	číslo	k1gNnSc4	číslo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
atomová	atomový	k2eAgFnSc1d1	atomová
hmotnost	hmotnost	k1gFnSc1	hmotnost
olova	olovo	k1gNnSc2	olovo
208	[number]	k4	208
Pb	Pb	k1gFnPc2	Pb
je	být	k5eAaImIp3nS	být
207,976	[number]	k4	207,976
<g/>
7	[number]	k4	7
u	u	k7c2	u
≅	≅	k?	≅
208	[number]	k4	208
u	u	k7c2	u
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
též	též	k9	též
hmotnostní	hmotnostní	k2eAgNnSc4d1	hmotnostní
či	či	k8xC	či
hmotové	hmotový	k2eAgNnSc4d1	hmotové
číslo	číslo	k1gNnSc4	číslo
(	(	kIx(	(
<g/>
hmotnost	hmotnost	k1gFnSc4	hmotnost
atomového	atomový	k2eAgInSc2d1	atomový
obalu	obal	k1gInSc2	obal
i	i	k9	i
hmotnostní	hmotnostní	k2eAgInSc4d1	hmotnostní
schodek	schodek	k1gInSc4	schodek
jádra	jádro	k1gNnSc2	jádro
jsou	být	k5eAaImIp3nP	být
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
hmotnosti	hmotnost	k1gFnSc3	hmotnost
jednoho	jeden	k4xCgInSc2	jeden
nukleonu	nukleon	k1gInSc2	nukleon
zanedbatelné	zanedbatelný	k2eAgNnSc1d1	zanedbatelné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vazebná	vazebný	k2eAgFnSc1d1	vazebná
energie	energie	k1gFnSc1	energie
===	===	k?	===
</s>
</p>
<p>
<s>
Bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nS	by
možné	možný	k2eAgNnSc1d1	možné
očekávat	očekávat	k5eAaImF	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
celková	celkový	k2eAgFnSc1d1	celková
hmotnost	hmotnost	k1gFnSc1	hmotnost
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
součtu	součet	k1gInSc2	součet
hmotností	hmotnost	k1gFnSc7	hmotnost
všech	všecek	k3xTgInPc2	všecek
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
neutronů	neutron	k1gInPc2	neutron
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
jádro	jádro	k1gNnSc1	jádro
skládá	skládat	k5eAaImIp3nS	skládat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
měření	měření	k1gNnSc2	měření
však	však	k9	však
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
celková	celkový	k2eAgFnSc1d1	celková
hmotnost	hmotnost	k1gFnSc1	hmotnost
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
očekávanou	očekávaný	k2eAgFnSc7d1	očekávaná
a	a	k8xC	a
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
hmotností	hmotnost	k1gFnSc7	hmotnost
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xC	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
[	[	kIx(	[
<g/>
Zm_	Zm_	k1gFnSc6	Zm_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
A-Z	A-Z	k1gFnSc1	A-Z
<g/>
)	)	kIx)	)
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
-m_	_	k?	-m_
<g/>
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Z	z	k7c2	z
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
atomové	atomový	k2eAgNnSc1d1	atomové
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
nukleonové	nukleonový	k2eAgNnSc1d1	nukleonový
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc4	hmotnost
protonu	proton	k1gInSc2	proton
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc4	hmotnost
neutronu	neutron	k1gInSc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Hodnotu	hodnota	k1gFnSc4	hodnota
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
B	B	kA	B
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
hmotnostní	hmotnostní	k2eAgInSc4d1	hmotnostní
rozdíl	rozdíl	k1gInSc4	rozdíl
<g/>
,	,	kIx,	,
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
hmotnostní	hmotnostní	k2eAgInSc4d1	hmotnostní
schodek	schodek	k1gInSc4	schodek
(	(	kIx(	(
<g/>
defekt	defekt	k1gInSc4	defekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnostní	hmotnostní	k2eAgInSc1d1	hmotnostní
schodek	schodek	k1gInSc1	schodek
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
kladný	kladný	k2eAgInSc1d1	kladný
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
<s>
>	>	kIx)	>
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
B	B	kA	B
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
jaderné	jaderný	k2eAgFnPc1d1	jaderná
síly	síla	k1gFnPc1	síla
(	(	kIx(	(
<g/>
silná	silný	k2eAgFnSc1d1	silná
interakce	interakce	k1gFnSc1	interakce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
přitahování	přitahování	k1gNnSc4	přitahování
nukleonů	nukleon	k1gInPc2	nukleon
<g/>
,	,	kIx,	,
vykonat	vykonat	k5eAaPmF	vykonat
určitou	určitý	k2eAgFnSc4d1	určitá
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vykonání	vykonání	k1gNnSc3	vykonání
této	tento	k3xDgFnSc2	tento
práce	práce	k1gFnSc2	práce
se	se	k3xPyFc4	se
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
určitá	určitý	k2eAgFnSc1d1	určitá
část	část	k1gFnSc1	část
celkové	celkový	k2eAgFnSc2d1	celková
energie	energie	k1gFnSc2	energie
soustavy	soustava	k1gFnSc2	soustava
nukleonů	nukleon	k1gInPc2	nukleon
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
celkové	celkový	k2eAgFnSc2d1	celková
energie	energie	k1gFnSc2	energie
soustavy	soustava	k1gFnSc2	soustava
nukleonů	nukleon	k1gInPc2	nukleon
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
jádra	jádro	k1gNnSc2	jádro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Einsteinova	Einsteinův	k2eAgInSc2d1	Einsteinův
vztahu	vztah	k1gInSc2	vztah
je	být	k5eAaImIp3nS	být
však	však	k9	však
celková	celkový	k2eAgFnSc1d1	celková
energie	energie	k1gFnSc1	energie
soustavy	soustava	k1gFnSc2	soustava
nukleonů	nukleon	k1gInPc2	nukleon
úměrná	úměrný	k2eAgFnSc1d1	úměrná
její	její	k3xOp3gFnSc3	její
celkové	celkový	k2eAgFnSc3d1	celková
hmotnosti	hmotnost	k1gFnSc3	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Zmenšení	zmenšení	k1gNnSc1	zmenšení
energie	energie	k1gFnSc2	energie
tedy	tedy	k9	tedy
znamená	znamenat	k5eAaImIp3nS	znamenat
zmenšení	zmenšení	k1gNnSc1	zmenšení
hmotnosti	hmotnost	k1gFnSc2	hmotnost
(	(	kIx(	(
<g/>
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
hmotnostní	hmotnostní	k2eAgInSc4d1	hmotnostní
schodek	schodek	k1gInSc4	schodek
pak	pak	k6eAd1	pak
platí	platit	k5eAaImIp3nS	platit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
W	W	kA	W
</s>
</p>
<p>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
W_	W_	k1gMnSc1	W_
<g/>
{	{	kIx(	{
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
Bc	Bc	k1gFnSc1	Bc
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Hmotnostní	hmotnostní	k2eAgInSc1d1	hmotnostní
schodek	schodek	k1gInSc1	schodek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
B	B	kA	B
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
tedy	tedy	k8xC	tedy
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
určité	určitý	k2eAgFnSc3d1	určitá
energii	energie	k1gFnSc3	energie
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
W	W	kA	W
</s>
</p>
<p>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
W_	W_	k1gMnSc1	W_
<g/>
{	{	kIx(	{
<g/>
B	B	kA	B
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
vazebná	vazebný	k2eAgFnSc1d1	vazebná
(	(	kIx(	(
<g/>
vazební	vazební	k2eAgFnSc1d1	vazební
<g/>
)	)	kIx)	)
energie	energie	k1gFnSc1	energie
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
takového	takový	k3xDgNnSc2	takový
označení	označení	k1gNnSc2	označení
plyne	plynout	k5eAaImIp3nS	plynout
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
W	W	kA	W
</s>
</p>
<p>
</p>
<p>
<s>
B	B	kA	B
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
W_	W_	k1gMnSc1	W_
<g/>
{	{	kIx(	{
<g/>
B	B	kA	B
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
představuje	představovat	k5eAaImIp3nS	představovat
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
jádra	jádro	k1gNnSc2	jádro
z	z	k7c2	z
volných	volný	k2eAgInPc2d1	volný
nukleonů	nukleon	k1gInPc2	nukleon
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
jádru	jádro	k1gNnSc6	jádro
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
rozdělení	rozdělení	k1gNnSc3	rozdělení
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
nukleony	nukleon	k1gInPc4	nukleon
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
energie	energie	k1gFnSc1	energie
tedy	tedy	k9	tedy
určuje	určovat	k5eAaImIp3nS	určovat
velikost	velikost	k1gFnSc1	velikost
vazby	vazba	k1gFnSc2	vazba
nukleonů	nukleon	k1gInPc2	nukleon
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vazebné	vazebný	k2eAgFnPc1d1	vazebná
energie	energie	k1gFnPc1	energie
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
hmotnostní	hmotnostní	k2eAgInPc4d1	hmotnostní
schodky	schodek	k1gInPc4	schodek
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
různých	různý	k2eAgInPc2d1	různý
atomů	atom	k1gInPc2	atom
různé	různý	k2eAgInPc1d1	různý
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozdílnému	rozdílný	k2eAgInSc3d1	rozdílný
počtu	počet	k1gInSc3	počet
nukleonů	nukleon	k1gInPc2	nukleon
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
jádrech	jádro	k1gNnPc6	jádro
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
uvádět	uvádět	k5eAaImF	uvádět
vazebnou	vazebný	k2eAgFnSc4d1	vazebná
energii	energie	k1gFnSc4	energie
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
nukleon	nukleon	k1gInSc4	nukleon
<g/>
.	.	kIx.	.
</s>
<s>
Hodnoty	hodnota	k1gFnPc1	hodnota
vazebné	vazebný	k2eAgFnSc2d1	vazebná
energie	energie	k1gFnSc2	energie
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnPc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
vazebná	vazebný	k2eAgFnSc1d1	vazebná
energie	energie	k1gFnSc1	energie
deuteronu	deuteron	k1gInSc2	deuteron
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
2,23	[number]	k4	2,23
MeV	MeV	k1gFnPc2	MeV
≈	≈	k?	≈
1014	[number]	k4	1014
J	J	kA	J
<g/>
·	·	k?	·
<g/>
kg-	kg-	k?	kg-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
např.	např.	kA	např.
s	s	k7c7	s
teplem	teplo	k1gNnSc7	teplo
uvolněným	uvolněný	k2eAgNnSc7d1	uvolněné
při	při	k7c6	při
spalování	spalování	k1gNnSc6	spalování
benzínu	benzín	k1gInSc2	benzín
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
5	[number]	k4	5
<g/>
×	×	k?	×
<g/>
107	[number]	k4	107
J	J	kA	J
<g/>
·	·	k?	·
<g/>
kg-	kg-	k?	kg-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
obrovská	obrovský	k2eAgFnSc1d1	obrovská
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vazebná	vazebný	k2eAgFnSc1d1	vazebná
energie	energie	k1gFnSc1	energie
úzce	úzko	k6eAd1	úzko
souvisí	souviset	k5eAaImIp3nS	souviset
se	se	k3xPyFc4	se
stabilitou	stabilita	k1gFnSc7	stabilita
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stabilita	stabilita	k1gFnSc1	stabilita
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
===	===	k?	===
</s>
</p>
<p>
<s>
Atomová	atomový	k2eAgNnPc1d1	atomové
jádra	jádro	k1gNnPc1	jádro
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Stabilní	stabilní	k2eAgNnPc1d1	stabilní
lehká	lehký	k2eAgNnPc1d1	lehké
jádra	jádro	k1gNnPc1	jádro
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
jádra	jádro	k1gNnSc2	jádro
s	s	k7c7	s
nukleonovým	nukleonův	k2eAgNnSc7d1	nukleonův
číslem	číslo	k1gNnSc7	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
<s>
<	<	kIx(	<
</s>
</p>
<p>
<s>
20	[number]	k4	20
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
<	<	kIx(	<
<g/>
20	[number]	k4	20
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
přibližně	přibližně	k6eAd1	přibližně
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těžších	těžký	k2eAgNnPc6d2	těžší
(	(	kIx(	(
<g/>
stabilních	stabilní	k2eAgNnPc6d1	stabilní
<g/>
)	)	kIx)	)
jádrech	jádro	k1gNnPc6	jádro
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
neutronů	neutron	k1gInPc2	neutron
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
počet	počet	k1gInSc1	počet
protonů	proton	k1gInPc2	proton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
diagramu	diagram	k1gInSc2	diagram
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
43	[number]	k4	43
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
43	[number]	k4	43
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
61	[number]	k4	61
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
61	[number]	k4	61
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
neexistují	existovat	k5eNaImIp3nP	existovat
stabilní	stabilní	k2eAgInPc4d1	stabilní
nuklidy	nuklid	k1gInPc4	nuklid
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
neexistují	existovat	k5eNaImIp3nP	existovat
stabilní	stabilní	k2eAgFnPc1d1	stabilní
nuklidy	nuklida	k1gFnPc1	nuklida
pro	pro	k7c4	pro
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
19	[number]	k4	19
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
35	[number]	k4	35
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
39	[number]	k4	39
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
45	[number]	k4	45
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
61	[number]	k4	61
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
89	[number]	k4	89
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
115	[number]	k4	115
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
123	[number]	k4	123
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
=	=	kIx~	=
<g/>
19,35	[number]	k4	19,35
<g/>
,39	,39	k4	,39
<g/>
,45	,45	k4	,45
<g/>
,61	,61	k4	,61
<g/>
,89	,89	k4	,89
<g/>
,115	,115	k4	,115
<g/>
,123	,123	k4	,123
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Také	také	k9	také
neexistují	existovat	k5eNaImIp3nP	existovat
stabilní	stabilní	k2eAgFnPc1d1	stabilní
nuklidy	nuklida	k1gFnPc1	nuklida
pro	pro	k7c4	pro
nukleonová	nukleonový	k2eAgNnPc4d1	nukleonový
čísla	číslo	k1gNnPc4	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
8	[number]	k4	8
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
=	=	kIx~	=
<g/>
N	N	kA	N
<g/>
+	+	kIx~	+
<g/>
Z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
5,8	[number]	k4	5,8
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Jádra	jádro	k1gNnPc4	jádro
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
počet	počet	k1gInSc1	počet
protonů	proton	k1gInPc2	proton
nebo	nebo	k8xC	nebo
neutronů	neutron	k1gInPc2	neutron
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgMnSc1d1	roven
2	[number]	k4	2
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
,	,	kIx,	,
82	[number]	k4	82
<g/>
,	,	kIx,	,
126	[number]	k4	126
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
stabilitou	stabilita	k1gFnSc7	stabilita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
stabilitou	stabilita	k1gFnSc7	stabilita
označujeme	označovat	k5eAaImIp1nP	označovat
tato	tento	k3xDgNnPc1	tento
čísla	číslo	k1gNnPc1	číslo
jako	jako	k8xC	jako
magická	magický	k2eAgFnSc1d1	magická
<g/>
.	.	kIx.	.
<g/>
Jaderné	jaderný	k2eAgFnPc1d1	jaderná
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
působí	působit	k5eAaImIp3nP	působit
mezi	mezi	k7c7	mezi
nukleony	nukleon	k1gInPc7	nukleon
v	v	k7c6	v
jádrech	jádro	k1gNnPc6	jádro
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc4d1	malý
dosah	dosah	k1gInSc4	dosah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těžších	těžký	k2eAgNnPc6d2	těžší
jádrech	jádro	k1gNnPc6	jádro
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
nukleonů	nukleon	k1gInPc2	nukleon
<g/>
,	,	kIx,	,
interaguje	interagovat	k5eAaPmIp3nS	interagovat
každý	každý	k3xTgInSc4	každý
nukleon	nukleon	k1gInSc4	nukleon
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
nukleony	nukleon	k1gInPc7	nukleon
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
nasycení	nasycení	k1gNnSc1	nasycení
jaderných	jaderný	k2eAgFnPc2d1	jaderná
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
elektrostatické	elektrostatický	k2eAgFnSc2d1	elektrostatická
odpudivé	odpudivý	k2eAgFnSc2d1	odpudivá
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
působí	působit	k5eAaImIp3nP	působit
protony	proton	k1gInPc1	proton
to	ten	k3xDgNnSc1	ten
však	však	k9	však
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Elektrostatická	elektrostatický	k2eAgFnSc1d1	elektrostatická
síla	síla	k1gFnSc1	síla
nepůsobí	působit	k5eNaImIp3nS	působit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
protonů	proton	k1gInPc2	proton
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
dostatečné	dostatečný	k2eAgFnSc6d1	dostatečná
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
elektrostatické	elektrostatický	k2eAgNnSc1d1	elektrostatické
odpuzování	odpuzování	k1gNnSc1	odpuzování
převažovat	převažovat	k5eAaImF	převažovat
<g/>
.	.	kIx.	.
</s>
<s>
Elektrostatické	elektrostatický	k2eAgNnSc1d1	elektrostatické
odpuzování	odpuzování	k1gNnSc1	odpuzování
je	být	k5eAaImIp3nS	být
vyrovnáváno	vyrovnávat	k5eAaImNgNnS	vyrovnávat
přebytkem	přebytek	k1gInSc7	přebytek
neutronů	neutron	k1gInPc2	neutron
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
působí	působit	k5eAaImIp3nP	působit
pouze	pouze	k6eAd1	pouze
přitažlivými	přitažlivý	k2eAgFnPc7d1	přitažlivá
jadernými	jaderný	k2eAgFnPc7d1	jaderná
silami	síla	k1gFnPc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
neutronů	neutron	k1gInPc2	neutron
však	však	k9	však
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
neomezený	omezený	k2eNgInSc1d1	neomezený
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
nukleonu	nukleon	k1gInSc2	nukleon
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
umístění	umístění	k1gNnSc4	umístění
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
jaderné	jaderný	k2eAgFnSc6d1	jaderná
energetické	energetický	k2eAgFnSc6d1	energetická
hladině	hladina	k1gFnSc6	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
přidali	přidat	k5eAaPmAgMnP	přidat
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c4	mnoho
neutronů	neutron	k1gInPc2	neutron
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
tyto	tento	k3xDgInPc4	tento
neutrony	neutron	k1gInPc4	neutron
nuceny	nucen	k2eAgInPc4d1	nucen
obsadit	obsadit	k5eAaPmF	obsadit
vyšší	vysoký	k2eAgFnSc2d2	vyšší
energetické	energetický	k2eAgFnSc2d1	energetická
hladiny	hladina	k1gFnSc2	hladina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
slaběji	slabo	k6eAd2	slabo
vázány	vázán	k2eAgFnPc1d1	vázána
a	a	k8xC	a
celé	celý	k2eAgNnSc1d1	celé
jádro	jádro	k1gNnSc1	jádro
tedy	tedy	k9	tedy
bude	být	k5eAaImBp3nS	být
náchylnější	náchylný	k2eAgFnSc1d2	náchylnější
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
velkých	velký	k2eAgNnPc2d1	velké
jader	jádro	k1gNnPc2	jádro
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
stability	stabilita	k1gFnSc2	stabilita
jádra	jádro	k1gNnSc2	jádro
nutné	nutný	k2eAgNnSc1d1	nutné
nalézt	nalézt	k5eAaBmF	nalézt
určitý	určitý	k2eAgInSc4d1	určitý
kompromis	kompromis	k1gInSc4	kompromis
mezi	mezi	k7c7	mezi
počtem	počet	k1gInSc7	počet
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
určitá	určitý	k2eAgFnSc1d1	určitá
hranice	hranice	k1gFnSc1	hranice
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yQgFnSc4	který
již	již	k6eAd1	již
neutrony	neutron	k1gInPc1	neutron
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
zajistit	zajistit	k5eAaPmF	zajistit
existenci	existence	k1gFnSc4	existence
stabilního	stabilní	k2eAgNnSc2d1	stabilní
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
hranicí	hranice	k1gFnSc7	hranice
je	být	k5eAaImIp3nS	být
izotop	izotop	k1gInSc4	izotop
20983	[number]	k4	20983
Bi	Bi	k1gMnSc1	Bi
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
nejtěžší	těžký	k2eAgInSc4d3	nejtěžší
stabilní	stabilní	k2eAgInSc4d1	stabilní
nuklid	nuklid	k1gInSc4	nuklid
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
jádra	jádro	k1gNnPc1	jádro
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
atomové	atomový	k2eAgNnSc1d1	atomové
číslo	číslo	k1gNnSc1	číslo
platí	platit	k5eAaImIp3nS	platit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
<s>
>	>	kIx)	>
</s>
</p>
<p>
<s>
83	[number]	k4	83
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Z	z	k7c2	z
<g/>
>	>	kIx)	>
<g/>
83	[number]	k4	83
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
nukleonové	nukleonový	k2eAgNnSc1d1	nukleonový
číslo	číslo	k1gNnSc1	číslo
splňuje	splňovat	k5eAaImIp3nS	splňovat
podmínku	podmínka	k1gFnSc4	podmínka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
A	a	k9	a
</s>
</p>
<p>
<s>
>	>	kIx)	>
</s>
</p>
<p>
<s>
209	[number]	k4	209
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
>	>	kIx)	>
<g/>
209	[number]	k4	209
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
podléhají	podléhat	k5eAaImIp3nP	podléhat
samovolnému	samovolný	k2eAgInSc3d1	samovolný
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
radioaktivnímu	radioaktivní	k2eAgInSc3d1	radioaktivní
<g/>
,	,	kIx,	,
rozpadu	rozpad	k1gInSc2	rozpad
na	na	k7c4	na
jádra	jádro	k1gNnPc4	jádro
lehčích	lehký	k2eAgInPc2d2	lehčí
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Příklady	příklad	k1gInPc1	příklad
rozpadů	rozpad	k1gInPc2	rozpad
jader	jádro	k1gNnPc2	jádro
===	===	k?	===
</s>
</p>
<p>
<s>
Jádra	jádro	k1gNnPc1	jádro
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
podstatně	podstatně	k6eAd1	podstatně
více	hodně	k6eAd2	hodně
neutronů	neutron	k1gInPc2	neutron
nebo	nebo	k8xC	nebo
protonů	proton	k1gInPc2	proton
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nestabilní	stabilní	k2eNgMnPc1d1	nestabilní
a	a	k8xC	a
rozpadají	rozpadat	k5eAaPmIp3nP	rozpadat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
velkém	velký	k2eAgInSc6d1	velký
počtu	počet	k1gInSc6	počet
neutronů	neutron	k1gInPc2	neutron
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
β	β	k?	β
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
nadbytečný	nadbytečný	k2eAgInSc1d1	nadbytečný
neutron	neutron	k1gInSc1	neutron
změní	změnit	k5eAaPmIp3nS	změnit
na	na	k7c4	na
proton	proton	k1gInSc4	proton
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
elektronu	elektron	k1gInSc2	elektron
(	(	kIx(	(
<g/>
záření	záření	k1gNnSc1	záření
β	β	k?	β
<g/>
)	)	kIx)	)
a	a	k8xC	a
antineutrina	antineutrina	k1gFnSc1	antineutrina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
reakce	reakce	k1gFnPc4	reakce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
K	k	k7c3	k
→	→	k?	→
Ca	ca	kA	ca
<g/>
:	:	kIx,	:
4019	[number]	k4	4019
K	K	kA	K
→	→	k?	→
4020	[number]	k4	4020
Ca	ca	kA	ca
+	+	kIx~	+
e	e	k0	e
<g/>
+	+	kIx~	+
+	+	kIx~	+
ve-	ve-	k?	ve-
,	,	kIx,	,
<g/>
Při	při	k7c6	při
nadbytku	nadbytek	k1gInSc6	nadbytek
protonů	proton	k1gInPc2	proton
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
β	β	k?	β
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgNnSc6	který
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
proton	proton	k1gInSc1	proton
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
na	na	k7c4	na
neutron	neutron	k1gInSc4	neutron
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
antielektronu	antielektron	k1gInSc2	antielektron
a	a	k8xC	a
neutrina	neutrino	k1gNnSc2	neutrino
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
velmi	velmi	k6eAd1	velmi
těžkých	těžký	k2eAgNnPc2d1	těžké
jader	jádro	k1gNnPc2	jádro
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
α	α	k?	α
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
opustí	opustit	k5eAaPmIp3nS	opustit
jádro	jádro	k1gNnSc1	jádro
částice	částice	k1gFnSc2	částice
alfa	alfa	k1gNnSc1	alfa
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jádro	jádro	k1gNnSc1	jádro
hélia	hélium	k1gNnSc2	hélium
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jako	jako	k8xC	jako
např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
U	u	k7c2	u
→	→	k?	→
Th	Th	k1gFnSc2	Th
<g/>
:	:	kIx,	:
23892	[number]	k4	23892
U	u	k7c2	u
→	→	k?	→
23490	[number]	k4	23490
Th	Th	k1gFnPc2	Th
+	+	kIx~	+
42	[number]	k4	42
He	he	k0	he
</s>
</p>
<p>
<s>
==	==	k?	==
Atomová	atomový	k2eAgNnPc1d1	atomové
jádra	jádro	k1gNnPc1	jádro
se	s	k7c7	s
speciálním	speciální	k2eAgInSc7d1	speciální
názvem	název	k1gInSc7	název
==	==	k?	==
</s>
</p>
<p>
<s>
Nejlehčí	lehký	k2eAgNnPc1d3	nejlehčí
atomová	atomový	k2eAgNnPc1d1	atomové
jádra	jádro	k1gNnPc1	jádro
mají	mít	k5eAaImIp3nP	mít
jakožto	jakožto	k8xS	jakožto
částice	částice	k1gFnPc1	částice
časté	častý	k2eAgFnPc1d1	častá
v	v	k7c6	v
přirozených	přirozený	k2eAgInPc6d1	přirozený
rozpadech	rozpad	k1gInPc6	rozpad
nebo	nebo	k8xC	nebo
jaderných	jaderný	k2eAgFnPc6d1	jaderná
reakcích	reakce	k1gFnPc6	reakce
speciální	speciální	k2eAgInPc1d1	speciální
názvy	název	k1gInPc1	název
a	a	k8xC	a
značky	značka	k1gFnPc1	značka
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgFnPc1d1	umožňující
zjednodušit	zjednodušit	k5eAaPmF	zjednodušit
zápis	zápis	k1gInSc4	zápis
reakcí	reakce	k1gFnPc2	reakce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
proton	proton	k1gInSc1	proton
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
p	p	k?	p
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jádro	jádro	k1gNnSc1	jádro
atomu	atom	k1gInSc2	atom
11	[number]	k4	11
H	H	kA	H
</s>
</p>
<p>
<s>
deuteron	deuteron	k1gInSc1	deuteron
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
d	d	k?	d
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jádro	jádro	k1gNnSc1	jádro
atomu	atom	k1gInSc2	atom
21	[number]	k4	21
H	H	kA	H
</s>
</p>
<p>
<s>
triton	triton	k1gInSc1	triton
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
t	t	k?	t
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
τ	τ	k?	τ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jádro	jádro	k1gNnSc1	jádro
atomu	atom	k1gInSc2	atom
31	[number]	k4	31
H	H	kA	H
</s>
</p>
<p>
<s>
helion	helion	k1gInSc1	helion
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
h	h	k?	h
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jádro	jádro	k1gNnSc1	jádro
atomu	atom	k1gInSc2	atom
32	[number]	k4	32
He	he	k0	he
</s>
</p>
<p>
<s>
částice	částice	k1gFnPc1	částice
alfa	alfa	k1gNnSc1	alfa
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
α	α	k?	α
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jádro	jádro	k1gNnSc1	jádro
atomu	atom	k1gInSc2	atom
42	[number]	k4	42
He	he	k0	he
</s>
</p>
<p>
<s>
==	==	k?	==
Hyperjádra	Hyperjádra	k1gFnSc1	Hyperjádra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
atomovém	atomový	k2eAgNnSc6d1	atomové
jádře	jádro	k1gNnSc6	jádro
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jeden	jeden	k4xCgInSc4	jeden
či	či	k8xC	či
dva	dva	k4xCgInPc4	dva
nukleony	nukleon	k1gInPc4	nukleon
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
hyperonem	hyperon	k1gMnSc7	hyperon
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
Λ	Λ	k?	Λ
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
Σ	Σ	k?	Σ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
jádro	jádro	k1gNnSc1	jádro
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
hyperjádro	hyperjádro	k6eAd1	hyperjádro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Elektronový	elektronový	k2eAgInSc4d1	elektronový
obal	obal	k1gInSc4	obal
</s>
</p>
<p>
<s>
Nukleon	nukleon	k1gInSc1	nukleon
</s>
</p>
<p>
<s>
Proton	proton	k1gInSc1	proton
</s>
</p>
<p>
<s>
Neutron	neutron	k1gInSc1	neutron
</s>
</p>
<p>
<s>
Elektron	elektron	k1gInSc1	elektron
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
reakce	reakce	k1gFnSc1	reakce
</s>
</p>
<p>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
energie	energie	k1gFnSc1	energie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Model	model	k1gInSc1	model
složeného	složený	k2eAgNnSc2d1	složené
jádra	jádro	k1gNnSc2	jádro
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
