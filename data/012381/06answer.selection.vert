<s>
Atomové	atomový	k2eAgNnSc1d1	atomové
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
kladně	kladně	k6eAd1	kladně
nabitá	nabitý	k2eAgFnSc1d1	nabitá
část	část	k1gFnSc1	část
atomu	atom	k1gInSc2	atom
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
jeho	jeho	k3xOp3gNnSc4	jeho
hmotnostní	hmotnostní	k2eAgNnSc4d1	hmotnostní
i	i	k8xC	i
prostorové	prostorový	k2eAgNnSc4d1	prostorové
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
</s>
