<s>
Významnou	významný	k2eAgFnSc4d1	významná
změnu	změna	k1gFnSc4	změna
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
představovala	představovat	k5eAaImAgFnS	představovat
vláda	vláda	k1gFnSc1	vláda
Ivana	Ivan	k1gMnSc2	Ivan
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Ugře	Ugře	k1gInSc1	Ugře
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
roku	rok	k1gInSc2	rok
1480	[number]	k4	1480
definitivního	definitivní	k2eAgNnSc2d1	definitivní
vítězství	vítězství	k1gNnSc2	vítězství
a	a	k8xC	a
vymanění	vymanění	k1gNnSc2	vymanění
z	z	k7c2	z
tatarské	tatarský	k2eAgFnSc2d1	tatarská
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1472	[number]	k4	1472
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
byzantskou	byzantský	k2eAgFnSc7d1	byzantská
princeznou	princezna	k1gFnSc7	princezna
Zóé	Zóé	k1gFnSc7	Zóé
Palaiologovnou	Palaiologovna	k1gFnSc7	Palaiologovna
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
na	na	k7c6	na
podobě	podoba	k1gFnSc6	podoba
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
Sofie	Sofie	k1gFnSc1	Sofie
pozvala	pozvat	k5eAaPmAgFnS	pozvat
architekty	architekt	k1gMnPc4	architekt
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>

