<s>
Významnou	významný	k2eAgFnSc4d1	významná
změnu	změna	k1gFnSc4	změna
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
představovala	představovat	k5eAaImAgFnS	představovat
vláda	vláda	k1gFnSc1	vláda
Ivana	Ivan	k1gMnSc2	Ivan
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Ugře	Ugře	k1gInSc1	Ugře
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
roku	rok	k1gInSc2	rok
1480	[number]	k4	1480
definitivního	definitivní	k2eAgNnSc2d1	definitivní
vítězství	vítězství	k1gNnSc2	vítězství
a	a	k8xC	a
vymanění	vymanění	k1gNnSc2	vymanění
z	z	k7c2	z
tatarské	tatarský	k2eAgFnSc2d1	tatarská
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
