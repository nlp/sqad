<s>
Moskva	Moskva	k1gFnSc1	Moskva
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
М	М	k?	М
[	[	kIx(	[
<g/>
mɐ	mɐ	k?	mɐ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Ruska	Rusko	k1gNnSc2	Rusko
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
2511	[number]	k4	2511
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
12	[number]	k4	12
108	[number]	k4	108
257	[number]	k4	257
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
politické	politický	k2eAgNnSc1d1	politické
<g/>
,	,	kIx,	,
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
a	a	k8xC	a
kulturní	kulturní	k2eAgNnSc1d1	kulturní
centrum	centrum	k1gNnSc1	centrum
země	zem	k1gFnSc2	zem
s	s	k7c7	s
asi	asi	k9	asi
60	[number]	k4	60
univerzitami	univerzita	k1gFnPc7	univerzita
a	a	k8xC	a
množstvím	množství	k1gNnSc7	množství
dalších	další	k2eAgFnPc2d1	další
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
divadel	divadlo	k1gNnPc2	divadlo
<g/>
,	,	kIx,	,
muzeí	muzeum	k1gNnPc2	muzeum
a	a	k8xC	a
galerií	galerie	k1gFnPc2	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
všechna	všechen	k3xTgNnPc1	všechen
ministerstva	ministerstvo	k1gNnPc1	ministerstvo
<g/>
,	,	kIx,	,
státní	státní	k2eAgInPc1d1	státní
úřady	úřad	k1gInPc1	úřad
i	i	k8xC	i
významné	významný	k2eAgFnPc1d1	významná
firmy	firma	k1gFnPc1	firma
a	a	k8xC	a
také	také	k9	také
patriarcha	patriarcha	k1gMnSc1	patriarcha
ruské	ruský	k2eAgFnSc2d1	ruská
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
je	být	k5eAaImIp3nS	být
nejsevernější	severní	k2eAgFnSc1d3	nejsevernější
megalopole	megalopole	k1gFnSc1	megalopole
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
zde	zde	k6eAd1	zde
TV	TV	kA	TV
věž	věž	k1gFnSc1	věž
Ostankino	Ostankino	k1gNnSc4	Ostankino
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
stavbou	stavba	k1gFnSc7	stavba
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
a	a	k8xC	a
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
zde	zde	k6eAd1	zde
i	i	k9	i
Věž	věž	k1gFnSc1	věž
Federace	federace	k1gFnSc1	federace
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
je	být	k5eAaImIp3nS	být
staré	starý	k2eAgFnPc4d1	stará
stovky	stovka	k1gFnPc4	stovka
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
středu	střed	k1gInSc6	střed
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
trojúhelníkový	trojúhelníkový	k2eAgInSc1d1	trojúhelníkový
Kreml	Kreml	k1gInSc1	Kreml
<g/>
,	,	kIx,	,
prohlášený	prohlášený	k2eAgInSc1d1	prohlášený
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Rudým	rudý	k2eAgNnSc7d1	Rudé
náměstím	náměstí	k1gNnSc7	náměstí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
za	za	k7c4	za
světové	světový	k2eAgNnSc4d1	světové
kulturní	kulturní	k2eAgNnSc4d1	kulturní
dědictví	dědictví	k1gNnSc4	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
nalézal	nalézat	k5eAaImAgMnS	nalézat
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
Rostovsko-suzdalského	Rostovskouzdalský	k2eAgNnSc2d1	Rostovsko-suzdalský
knížectví	knížectví	k1gNnSc2	knížectví
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Moskvy	Moskva	k1gFnSc2	Moskva
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
dvorec	dvorec	k1gInSc1	dvorec
bojara	bojar	k1gMnSc2	bojar
Štěpána	Štěpán	k1gMnSc2	Štěpán
Kučky	Kučka	k1gMnSc2	Kučka
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
řeky	řeka	k1gFnSc2	řeka
zván	zván	k2eAgInSc4d1	zván
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Rostovsko-suzdalský	Rostovskouzdalský	k2eAgMnSc1d1	Rostovsko-suzdalský
kníže	kníže	k1gMnSc1	kníže
Jurij	Jurij	k1gMnSc1	Jurij
Dolgorukij	Dolgorukij	k1gMnSc1	Dolgorukij
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začlenil	začlenit	k5eAaPmAgInS	začlenit
dvorec	dvorec	k1gInSc1	dvorec
do	do	k7c2	do
knížecího	knížecí	k2eAgNnSc2d1	knížecí
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
roku	rok	k1gInSc2	rok
1147	[number]	k4	1147
pochází	pocházet	k5eAaImIp3nS	pocházet
první	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
Jurij	Jurij	k1gMnSc1	Jurij
Dolgorukij	Dolgorukij	k1gMnSc1	Dolgorukij
pozval	pozvat	k5eAaPmAgMnS	pozvat
svého	svůj	k3xOyFgMnSc4	svůj
spojence	spojenec	k1gMnSc4	spojenec
<g/>
,	,	kIx,	,
novgorodsko-severského	novgorodskoeverský	k2eAgMnSc4d1	novgorodsko-severský
knížete	kníže	k1gMnSc4	kníže
Svjatoslava	Svjatoslav	k1gMnSc4	Svjatoslav
<g/>
,	,	kIx,	,
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgInSc6	který
letopisec	letopisec	k1gMnSc1	letopisec
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Přijď	přijít	k5eAaPmRp2nS	přijít
ke	k	k7c3	k
mně	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
bratře	bratr	k1gMnSc5	bratr
<g/>
,	,	kIx,	,
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Roku	rok	k1gInSc2	rok
1156	[number]	k4	1156
kníže	kníže	k1gMnSc1	kníže
Jurij	Jurij	k1gMnSc1	Jurij
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
posilováním	posilování	k1gNnSc7	posilování
obrany	obrana	k1gFnSc2	obrana
západní	západní	k2eAgFnSc2d1	západní
hranice	hranice	k1gFnSc2	hranice
suzdalského	suzdalský	k2eAgNnSc2d1	suzdalský
knížectví	knížectví	k1gNnSc2	knížectví
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
v	v	k7c6	v
osadě	osada	k1gFnSc6	osada
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
,	,	kIx,	,
obehnanou	obehnaný	k2eAgFnSc7d1	obehnaná
dřevěnou	dřevěný	k2eAgFnSc7d1	dřevěná
palisádou	palisáda	k1gFnSc7	palisáda
<g/>
,	,	kIx,	,
základ	základ	k1gInSc1	základ
dnešního	dnešní	k2eAgInSc2d1	dnešní
Kremlu	Kreml	k1gInSc2	Kreml
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
byla	být	k5eAaImAgFnS	být
Moskva	Moskva	k1gFnSc1	Moskva
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
obranný	obranný	k2eAgInSc4d1	obranný
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k9	jako
místo	místo	k1gNnSc4	místo
noclehu	nocleh	k1gInSc2	nocleh
suzdalských	suzdalský	k2eAgMnPc2d1	suzdalský
knížat	kníže	k1gMnPc2wR	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
na	na	k7c6	na
průsečíku	průsečík	k1gInSc6	průsečík
několika	několik	k4yIc2	několik
obchodních	obchodní	k2eAgFnPc2d1	obchodní
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ji	on	k3xPp3gFnSc4	on
přeurčilo	přeurčit	k5eAaPmAgNnS	přeurčit
pro	pro	k7c4	pro
roli	role	k1gFnSc4	role
významného	významný	k2eAgNnSc2d1	významné
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Budoucí	budoucí	k2eAgInSc1d1	budoucí
význam	význam	k1gInSc1	význam
Moskvy	Moskva	k1gFnSc2	Moskva
byl	být	k5eAaImAgInS	být
zapříčiněn	zapříčiněn	k2eAgMnSc1d1	zapříčiněn
jednak	jednak	k8xC	jednak
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
mocí	moc	k1gFnSc7	moc
vladimirských	vladimirský	k2eAgNnPc2d1	vladimirský
knížat	kníže	k1gNnPc2	kníže
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
nakonec	nakonec	k6eAd1	nakonec
získala	získat	k5eAaPmAgFnS	získat
nad	nad	k7c7	nad
ostatními	ostatní	k2eAgMnPc7d1	ostatní
severoruskými	severoruský	k2eAgMnPc7d1	severoruský
knížaty	kníže	k1gMnPc7wR	kníže
převahu	převah	k1gInSc2	převah
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
stěhováním	stěhování	k1gNnSc7	stěhování
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
do	do	k7c2	do
severovýchodních	severovýchodní	k2eAgFnPc2d1	severovýchodní
oblastí	oblast	k1gFnPc2	oblast
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
sužovaného	sužovaný	k2eAgInSc2d1	sužovaný
nájezdy	nájezd	k1gInPc7	nájezd
kočovníků	kočovník	k1gMnPc2	kočovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
se	se	k3xPyFc4	se
první	první	k4xOgFnSc7	první
usadil	usadit	k5eAaPmAgMnS	usadit
údělný	údělný	k2eAgMnSc1d1	údělný
kníže	kníže	k1gMnSc1	kníže
Daniil	Daniil	k1gMnSc1	Daniil
Alexandrovič	Alexandrovič	k1gMnSc1	Alexandrovič
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgInS	zemřít
1303	[number]	k4	1303
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Alexandra	Alexandr	k1gMnSc2	Alexandr
Něvského	něvský	k2eAgMnSc2d1	něvský
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
tím	ten	k3xDgNnSc7	ten
moskevskou	moskevský	k2eAgFnSc4d1	Moskevská
větev	větev	k1gFnSc4	větev
rodu	rod	k1gInSc2	rod
Rurikovců	Rurikovec	k1gMnPc2	Rurikovec
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
nad	nad	k7c7	nad
ostatními	ostatní	k2eAgFnPc7d1	ostatní
větvemi	větev	k1gFnPc7	větev
Rurikovců	Rurikovec	k1gMnPc2	Rurikovec
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgNnP	snažit
moskevská	moskevský	k2eAgNnPc1d1	moskevské
knížata	kníže	k1gNnPc1	kníže
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
i	i	k9	i
přenesením	přenesení	k1gNnSc7	přenesení
rezidence	rezidence	k1gFnSc2	rezidence
metropolity	metropolita	k1gMnSc2	metropolita
vší	veš	k1gFnPc2	veš
Rusi	Rus	k1gFnSc2	Rus
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Ruští	ruský	k2eAgMnPc1d1	ruský
metropolité	metropolita	k1gMnPc1	metropolita
sídlili	sídlit	k5eAaImAgMnP	sídlit
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
<g/>
,	,	kIx,	,
metropolita	metropolita	k1gMnSc1	metropolita
Maxim	Maxim	k1gMnSc1	Maxim
však	však	k9	však
přenesl	přenést	k5eAaPmAgMnS	přenést
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
do	do	k7c2	do
Vladimiru	Vladimir	k1gInSc2	Vladimir
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Petr	Petr	k1gMnSc1	Petr
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1307	[number]	k4	1307
<g/>
-	-	kIx~	-
<g/>
1326	[number]	k4	1326
<g/>
)	)	kIx)	)
vsadil	vsadit	k5eAaPmAgInS	vsadit
na	na	k7c6	na
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
moskevskými	moskevský	k2eAgMnPc7d1	moskevský
knížaty	kníže	k1gMnPc7wR	kníže
a	a	k8xC	a
často	často	k6eAd1	často
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Metropolita	metropolita	k1gMnSc1	metropolita
Theognost	Theognost	k1gFnSc1	Theognost
(	(	kIx(	(
<g/>
1328	[number]	k4	1328
<g/>
-	-	kIx~	-
<g/>
1323	[number]	k4	1323
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
přesídlil	přesídlit	k5eAaPmAgInS	přesídlit
natrvalo	natrvalo	k6eAd1	natrvalo
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nechal	nechat	k5eAaPmAgMnS	nechat
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Ivana	Ivan	k1gMnSc2	Ivan
Kality	Kalita	k1gMnSc2	Kalita
a	a	k8xC	a
Simeona	Simeon	k1gMnSc2	Simeon
Hrdého	Hrdý	k1gMnSc2	Hrdý
vybudovat	vybudovat	k5eAaPmF	vybudovat
pět	pět	k4xCc4	pět
kamenných	kamenný	k2eAgInPc2d1	kamenný
chrámů	chrám	k1gInPc2	chrám
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
prestižního	prestižní	k2eAgNnSc2d1	prestižní
postavení	postavení	k1gNnSc2	postavení
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Theognostovi	Theognostův	k2eAgMnPc1d1	Theognostův
nástupci	nástupce	k1gMnPc1	nástupce
již	již	k6eAd1	již
všichni	všechen	k3xTgMnPc1	všechen
sídlili	sídlit	k5eAaImAgMnP	sídlit
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
Moskva	Moskva	k1gFnSc1	Moskva
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
nejen	nejen	k6eAd1	nejen
politickým	politický	k2eAgNnSc7d1	politické
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
církevním	církevní	k2eAgMnSc7d1	církevní
centrem	centr	k1gMnSc7	centr
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
i	i	k9	i
na	na	k7c6	na
stoupajícím	stoupající	k2eAgInSc6d1	stoupající
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
-	-	kIx~	-
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
jich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
žilo	žít	k5eAaImAgNnS	žít
asi	asi	k9	asi
30	[number]	k4	30
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
dějiny	dějiny	k1gFnPc1	dějiny
Moskvy	Moskva	k1gFnSc2	Moskva
byly	být	k5eAaImAgInP	být
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
sídlo	sídlo	k1gNnSc4	sídlo
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
ruských	ruský	k2eAgMnPc2d1	ruský
knížat	kníže	k1gMnPc2wR	kníže
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
postupně	postupně	k6eAd1	postupně
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
horda	horda	k1gFnSc1	horda
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
mongolské	mongolský	k2eAgFnSc2d1	mongolská
invaze	invaze	k1gFnSc2	invaze
(	(	kIx(	(
<g/>
1238	[number]	k4	1238
<g/>
)	)	kIx)	)
vypálila	vypálit	k5eAaPmAgFnS	vypálit
město	město	k1gNnSc4	město
a	a	k8xC	a
zabila	zabít	k5eAaPmAgFnS	zabít
jeho	jeho	k3xOp3gMnPc4	jeho
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Mongolové	Mongol	k1gMnPc1	Mongol
s	s	k7c7	s
tatary	tatar	k1gMnPc7	tatar
také	také	k6eAd1	také
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
města	město	k1gNnSc2	město
Rjazaň	Rjazaň	k1gFnSc1	Rjazaň
<g/>
,	,	kIx,	,
Kolomna	Kolomna	k1gFnSc1	Kolomna
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gInSc1	Vladimir
<g/>
,	,	kIx,	,
Kyjev	Kyjev	k1gInSc1	Kyjev
a	a	k8xC	a
ruský	ruský	k2eAgInSc1d1	ruský
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
byl	být	k5eAaImAgInS	být
nucen	nutit	k5eAaImNgMnS	nutit
podrobit	podrobit	k5eAaPmF	podrobit
jejich	jejich	k3xOp3gFnSc3	jejich
nadvládě	nadvláda	k1gFnSc3	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1380	[number]	k4	1380
porazil	porazit	k5eAaPmAgMnS	porazit
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
Donský	donský	k2eAgMnSc1d1	donský
na	na	k7c6	na
Kulikovském	Kulikovský	k2eAgNnSc6d1	Kulikovský
poli	pole	k1gNnSc6	pole
tatarské	tatarský	k2eAgNnSc1d1	tatarské
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
čím	co	k3yRnSc7	co
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tataři	Tatar	k1gMnPc1	Tatar
nejsou	být	k5eNaImIp3nP	být
neporazitelní	porazitelný	k2eNgMnPc1d1	neporazitelný
<g/>
,	,	kIx,	,
sama	sám	k3xTgFnSc1	sám
Moskva	Moskva	k1gFnSc1	Moskva
však	však	k9	však
na	na	k7c4	na
Dimitrijovo	Dimitrijův	k2eAgNnSc4d1	Dimitrijův
vítězství	vítězství	k1gNnSc4	vítězství
doplatila	doplatit	k5eAaPmAgFnS	doplatit
odvetnou	odvetný	k2eAgFnSc7d1	odvetná
výpravou	výprava	k1gFnSc7	výprava
chána	chán	k1gMnSc2	chán
Tochtamyše	Tochtamyš	k1gMnSc2	Tochtamyš
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1382	[number]	k4	1382
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgFnS	být
vypálena	vypálit	k5eAaPmNgFnS	vypálit
a	a	k8xC	a
vypleněna	vyplenit	k5eAaPmNgFnS	vyplenit
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
nebyla	být	k5eNaImAgFnS	být
nyní	nyní	k6eAd1	nyní
zpustošena	zpustošit	k5eAaPmNgFnS	zpustošit
Tatary	tatar	k1gInPc7	tatar
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
však	však	k9	však
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
i	i	k8xC	i
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
vzpamatovala	vzpamatovat	k5eAaPmAgFnS	vzpamatovat
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
změnu	změna	k1gFnSc4	změna
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
představovala	představovat	k5eAaImAgFnS	představovat
vláda	vláda	k1gFnSc1	vláda
Ivana	Ivan	k1gMnSc2	Ivan
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Ugře	Ugře	k1gInSc1	Ugře
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
roku	rok	k1gInSc2	rok
1480	[number]	k4	1480
definitivního	definitivní	k2eAgNnSc2d1	definitivní
vítězství	vítězství	k1gNnSc2	vítězství
a	a	k8xC	a
vymanění	vymanění	k1gNnSc2	vymanění
z	z	k7c2	z
tatarské	tatarský	k2eAgFnSc2d1	tatarská
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1472	[number]	k4	1472
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
byzantskou	byzantský	k2eAgFnSc7d1	byzantská
princeznou	princezna	k1gFnSc7	princezna
Zóé	Zóé	k1gFnSc7	Zóé
Palaiologovnou	Palaiologovna	k1gFnSc7	Palaiologovna
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
na	na	k7c6	na
podobě	podoba	k1gFnSc6	podoba
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
Sofie	Sofie	k1gFnSc1	Sofie
pozvala	pozvat	k5eAaPmAgFnS	pozvat
architekty	architekt	k1gMnPc4	architekt
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
postavili	postavit	k5eAaPmAgMnP	postavit
Uspenský	Uspenský	k2eAgInSc4d1	Uspenský
<g/>
,	,	kIx,	,
Archangelský	archangelský	k2eAgInSc4d1	archangelský
a	a	k8xC	a
Blagověsčenský	Blagověsčenský	k2eAgInSc4d1	Blagověsčenský
chrám	chrám	k1gInSc4	chrám
<g/>
,	,	kIx,	,
Kreml	Kreml	k1gInSc1	Kreml
rozmnožili	rozmnožit	k5eAaPmAgMnP	rozmnožit
věžemi	věž	k1gFnPc7	věž
a	a	k8xC	a
hradbami	hradba	k1gFnPc7	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Kamenné	kamenný	k2eAgFnPc1d1	kamenná
stavby	stavba	k1gFnPc1	stavba
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
množit	množit	k5eAaImF	množit
<g/>
,	,	kIx,	,
kamenný	kamenný	k2eAgInSc4d1	kamenný
palác	palác	k1gInSc4	palác
si	se	k3xPyFc3	se
postavil	postavit	k5eAaPmAgMnS	postavit
například	například	k6eAd1	například
metropolita	metropolita	k1gMnSc1	metropolita
Iona	Iona	k1gMnSc1	Iona
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
již	již	k9	již
okolí	okolí	k1gNnSc1	okolí
Kremlu	Kreml	k1gInSc2	Kreml
mělo	mít	k5eAaImAgNnS	mít
charakter	charakter	k1gInSc4	charakter
evropského	evropský	k2eAgNnSc2d1	Evropské
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
čtvrti	čtvrt	k1gFnPc1	čtvrt
však	však	k9	však
byly	být	k5eAaImAgFnP	být
ještě	ještě	k9	ještě
převážně	převážně	k6eAd1	převážně
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1478	[number]	k4	1478
připojil	připojit	k5eAaPmAgMnS	připojit
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
státu	stát	k1gInSc3	stát
rozlehlé	rozlehlý	k2eAgNnSc1d1	rozlehlé
území	území	k1gNnSc1	území
Novgorodské	novgorodský	k2eAgFnSc2d1	Novgorodská
země	zem	k1gFnSc2	zem
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1485	[number]	k4	1485
knížectví	knížectví	k1gNnSc2	knížectví
tverské	tverský	k2eAgNnSc1d1	tverský
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Ivanem	Ivan	k1gMnSc7	Ivan
III	III	kA	III
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
ruské	ruský	k2eAgFnSc2d1	ruská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1571	[number]	k4	1571
Krymští	krymský	k2eAgMnPc1d1	krymský
Tataři	Tatar	k1gMnPc1	Tatar
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Turků	turek	k1gInPc2	turek
(	(	kIx(	(
<g/>
80.000	[number]	k4	80.000
tataři	tatar	k1gMnPc1	tatar
<g/>
,	,	kIx,	,
33.000	[number]	k4	33.000
turci	turek	k1gMnPc1	turek
a	a	k8xC	a
7,000	[number]	k4	7,000
janičáři	janičár	k1gMnPc5	janičár
<g/>
)	)	kIx)	)
vpadli	vpadnout	k5eAaPmAgMnP	vpadnout
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
Moskvu	Moskva	k1gFnSc4	Moskva
<g/>
,	,	kIx,	,
spálili	spálit	k5eAaPmAgMnP	spálit
vše	všechen	k3xTgNnSc4	všechen
kromě	kromě	k7c2	kromě
Kremlu	Kreml	k1gInSc2	Kreml
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
<g/>
,	,	kIx,	,
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
polský	polský	k2eAgMnSc1d1	polský
generál	generál	k1gMnSc1	generál
Stanisław	Stanisław	k1gFnSc2	Stanisław
Żółkiewski	Żółkiewsk	k1gFnSc2	Żółkiewsk
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k3yInSc1	co
porazil	porazit	k5eAaPmAgMnS	porazit
Rusy	Rus	k1gMnPc4	Rus
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
Klušino	Klušin	k2eAgNnSc1d1	Klušino
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
povstání	povstání	k1gNnSc6	povstání
vedeném	vedený	k2eAgNnSc6d1	vedené
Kuzma	Kuzma	k1gNnSc4	Kuzma
Mininem	Minin	k1gInSc7	Minin
a	a	k8xC	a
Dmitrij	Dmitrij	k1gFnSc7	Dmitrij
Požarskim	Požarskima	k1gFnPc2	Požarskima
došlo	dojít	k5eAaPmAgNnS	dojít
u	u	k7c2	u
bran	brána	k1gFnPc2	brána
Moskvy	Moskva	k1gFnSc2	Moskva
k	k	k7c3	k
rozhodujícímu	rozhodující	k2eAgInSc3d1	rozhodující
střetu	střet	k1gInSc3	střet
mezi	mezi	k7c7	mezi
Polsko-litevskou	polskoitevský	k2eAgFnSc7d1	polsko-litevská
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
Rusy	Rus	k1gMnPc7	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Polsko-lievská	Polskoievský	k2eAgNnPc1d1	Polsko-lievský
vojska	vojsko	k1gNnPc1	vojsko
byla	být	k5eAaImAgNnP	být
rozdrcena	rozdrcen	k2eAgFnSc1d1	rozdrcena
a	a	k8xC	a
několik	několik	k4yIc1	několik
okupantů	okupant	k1gMnPc2	okupant
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
drželo	držet	k5eAaImAgNnS	držet
za	za	k7c7	za
branami	brána	k1gFnPc7	brána
Kremlu	Kreml	k1gInSc2	Kreml
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
obléhání	obléhání	k1gNnSc6	obléhání
se	se	k3xPyFc4	se
vzdali	vzdát	k5eAaPmAgMnP	vzdát
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1612	[number]	k4	1612
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
přestalo	přestat	k5eAaPmAgNnS	přestat
být	být	k5eAaImF	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Ruska	Rusko	k1gNnSc2	Rusko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1712	[number]	k4	1712
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
období	období	k1gNnSc1	období
1728	[number]	k4	1728
<g/>
-	-	kIx~	-
<g/>
1732	[number]	k4	1732
<g/>
)	)	kIx)	)
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
Petrem	Petr	k1gMnSc7	Petr
Velikým	veliký	k2eAgMnSc7d1	veliký
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
pobřeží	pobřeží	k1gNnSc2	pobřeží
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1703	[number]	k4	1703
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
do	do	k7c2	do
země	zem	k1gFnSc2	zem
Napoleon	napoleon	k1gInSc1	napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gNnSc1	jeho
tažení	tažení	k1gNnSc1	tažení
zakončené	zakončený	k2eAgNnSc1d1	zakončené
okupací	okupace	k1gFnSc7	okupace
Moskvy	Moskva	k1gFnSc2	Moskva
skončilo	skončit	k5eAaPmAgNnS	skončit
debaklem	debakl	k1gInSc7	debakl
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
byl	být	k5eAaImAgMnS	být
poražen	porazit	k5eAaPmNgMnS	porazit
drsnou	drsný	k2eAgFnSc7d1	drsná
zimou	zima	k1gFnSc7	zima
a	a	k8xC	a
útoky	útok	k1gInPc1	útok
ruských	ruský	k2eAgFnPc2d1	ruská
vojenských	vojenský	k2eAgFnPc2d1	vojenská
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
ustoupit	ustoupit	k5eAaPmF	ustoupit
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
na	na	k7c4	na
380	[number]	k4	380
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
Grande	grand	k1gMnSc5	grand
Armée	Arméus	k1gMnSc5	Arméus
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
Brestlitevského	brestlitevský	k2eAgInSc2d1	brestlitevský
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
strachu	strach	k1gInSc2	strach
z	z	k7c2	z
možné	možný	k2eAgFnSc2d1	možná
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
invaze	invaze	k1gFnSc2	invaze
dne	den	k1gInSc2	den
12.3	[number]	k4	12.3
<g/>
.1918	.1918	k4	.1918
stala	stát	k5eAaPmAgFnS	stát
Moskva	Moskva	k1gFnSc1	Moskva
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
RSFSR	RSFSR	kA	RSFSR
<g/>
.	.	kIx.	.
</s>
<s>
Moskevské	moskevský	k2eAgNnSc1d1	moskevské
metro	metro	k1gNnSc1	metro
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
německé	německý	k2eAgFnSc6d1	německá
invazi	invaze	k1gFnSc6	invaze
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgInS	nacházet
výbor	výbor	k1gInSc1	výbor
obrany	obrana	k1gFnSc2	obrana
státu	stát	k1gInSc2	stát
i	i	k9	i
generální	generální	k2eAgInSc1d1	generální
štáb	štáb	k1gInSc1	štáb
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
skupina	skupina	k1gFnSc1	skupina
armád	armáda	k1gFnPc2	armáda
byla	být	k5eAaImAgFnS	být
zastavena	zastavit	k5eAaPmNgFnS	zastavit
a	a	k8xC	a
odehnána	odehnat	k5eAaPmNgFnS	odehnat
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
v	v	k7c6	v
Bitvě	bitva	k1gFnSc6	bitva
před	před	k7c7	před
Moskvou	Moskva	k1gFnSc7	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
hostila	hostit	k5eAaImAgFnS	hostit
Moskva	Moskva	k1gFnSc1	Moskva
letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
dějištěm	dějiště	k1gNnSc7	dějiště
neúspěšném	úspěšný	k2eNgNnSc6d1	neúspěšné
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
převrat	převrat	k1gInSc4	převrat
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Srpnový	srpnový	k2eAgInSc4d1	srpnový
puč	puč	k1gInSc4	puč
<g/>
)	)	kIx)	)
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
členů	člen	k1gMnPc2	člen
vlády	vláda	k1gFnSc2	vláda
proti	proti	k7c3	proti
reformám	reforma	k1gFnPc3	reforma
Michaila	Michail	k1gMnSc4	Michail
Gorbačova	Gorbačův	k2eAgFnSc1d1	Gorbačova
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
SSSR	SSSR	kA	SSSR
rozpuštěn	rozpustit	k5eAaPmNgInS	rozpustit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
i	i	k9	i
nadále	nadále	k6eAd1	nadále
zůstala	zůstat	k5eAaPmAgFnS	zůstat
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
bylo	být	k5eAaImAgNnS	být
spácháno	spáchat	k5eAaPmNgNnS	spáchat
několik	několik	k4yIc4	několik
teroristických	teroristický	k2eAgInPc2d1	teroristický
útoků	útok	k1gInPc2	útok
islámských	islámský	k2eAgMnPc2d1	islámský
radikálů	radikál	k1gMnPc2	radikál
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
bombové	bombový	k2eAgInPc4d1	bombový
útoky	útok	k1gInPc4	útok
v	v	k7c6	v
Moskevském	moskevský	k2eAgNnSc6d1	moskevské
metru	metro	k1gNnSc6	metro
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
a	a	k8xC	a
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
části	část	k1gFnSc6	část
Ruska	Rusko	k1gNnSc2	Rusko
na	na	k7c4	na
55,75	[number]	k4	55,75
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
37,62	[number]	k4	37,62
<g/>
°	°	k?	°
východní	východní	k2eAgFnSc2d1	východní
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
průměrné	průměrný	k2eAgFnSc6d1	průměrná
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
156	[number]	k4	156
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
kopcovité	kopcovitý	k2eAgFnSc6d1	kopcovitá
krajině	krajina	k1gFnSc6	krajina
mezi	mezi	k7c7	mezi
řekami	řeka	k1gFnPc7	řeka
Volhou	Volha	k1gFnSc7	Volha
a	a	k8xC	a
Okou	oka	k1gFnSc7	oka
<g/>
,	,	kIx,	,
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
přítoku	přítok	k1gInSc6	přítok
Oky	oka	k1gFnSc2	oka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Volhy	Volha	k1gFnSc2	Volha
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Moskva	Moskva	k1gFnSc1	Moskva
protéká	protékat	k5eAaImIp3nS	protékat
územím	území	k1gNnSc7	území
města	město	k1gNnSc2	město
v	v	k7c6	v
meandrech	meandr	k1gInPc6	meandr
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
severozápad-jihovýchod	severozápadihovýchod	k1gInSc1	severozápad-jihovýchod
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
asi	asi	k9	asi
80	[number]	k4	80
km	km	kA	km
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
120	[number]	k4	120
do	do	k7c2	do
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
našíř	našíř	k6eAd1	našíř
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
se	se	k3xPyFc4	se
v	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
města	město	k1gNnSc2	město
vlévá	vlévat	k5eAaImIp3nS	vlévat
další	další	k1gNnSc4	další
140	[number]	k4	140
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
jen	jen	k9	jen
14	[number]	k4	14
nyní	nyní	k6eAd1	nyní
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
byly	být	k5eAaImAgFnP	být
kanalizovány	kanalizován	k2eAgFnPc1d1	kanalizována
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
kanál	kanál	k1gInSc4	kanál
Moskva-Volha	Moskva-Volh	k1gMnSc2	Moskva-Volh
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
směřuje	směřovat	k5eAaImIp3nS	směřovat
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nS	tvořit
převážně	převážně	k6eAd1	převážně
dálniční	dálniční	k2eAgInSc1d1	dálniční
okruh	okruh	k1gInSc1	okruh
MKAD	MKAD	kA	MKAD
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
109	[number]	k4	109
km	km	kA	km
<g/>
,	,	kIx,	,
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
však	však	k8xC	však
město	město	k1gNnSc1	město
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
i	i	k9	i
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
Moskvy	Moskva	k1gFnSc2	Moskva
činí	činit	k5eAaImIp3nS	činit
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
1	[number]	k4	1
0	[number]	k4	0
<g/>
97,12	[number]	k4	97,12
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
přibližně	přibližně	k6eAd1	přibližně
třetinu	třetina	k1gFnSc4	třetina
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
zeleň	zeleň	k1gFnSc1	zeleň
tvořená	tvořený	k2eAgFnSc1d1	tvořená
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
parky	park	k1gInPc4	park
či	či	k8xC	či
lesíky	lesík	k1gInPc4	lesík
<g/>
;	;	kIx,	;
vodní	vodní	k2eAgFnSc4d1	vodní
plochu	plocha	k1gFnSc4	plocha
tvoří	tvořit	k5eAaImIp3nS	tvořit
kromě	kromě	k7c2	kromě
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
kanálů	kanál	k1gInPc2	kanál
asi	asi	k9	asi
500	[number]	k4	500
rybníků	rybník	k1gInPc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
30	[number]	k4	30
až	až	k9	až
40	[number]	k4	40
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
pás	pás	k1gInSc4	pás
výletních	výletní	k2eAgInPc2d1	výletní
lesíků	lesík	k1gInPc2	lesík
s	s	k7c7	s
četnými	četný	k2eAgFnPc7d1	četná
zábavními	zábavní	k2eAgFnPc7d1	zábavní
atrakcemi	atrakce	k1gFnPc7	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
mírné	mírný	k2eAgNnSc1d1	mírné
kontinentální	kontinentální	k2eAgInPc1d1	kontinentální
<g/>
.	.	kIx.	.
</s>
<s>
Zima	zima	k1gFnSc1	zima
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
konce	konec	k1gInSc2	konec
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
periody	perioda	k1gFnPc4	perioda
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
silných	silný	k2eAgInPc2d1	silný
mrazů	mráz	k1gInPc2	mráz
s	s	k7c7	s
noční	noční	k2eAgFnSc7d1	noční
teplotou	teplota	k1gFnSc7	teplota
okolo	okolo	k7c2	okolo
-20	-20	k4	-20
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výjimečně	výjimečně	k6eAd1	výjimečně
mohou	moct	k5eAaImIp3nP	moct
teploty	teplota	k1gFnPc1	teplota
spadnout	spadnout	k5eAaPmF	spadnout
až	až	k6eAd1	až
k	k	k7c3	k
-30	-30	k4	-30
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
ledna	leden	k1gInSc2	leden
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
oblevy	obleva	k1gFnPc1	obleva
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
teploty	teplota	k1gFnPc1	teplota
stoupají	stoupat	k5eAaImIp3nP	stoupat
k	k	k7c3	k
+5	+5	k4	+5
°	°	k?	°
<g/>
C.	C.	kA	C.
Přechodná	přechodný	k2eAgFnSc1d1	přechodná
roční	roční	k2eAgNnSc4d1	roční
období	období	k1gNnSc4	období
(	(	kIx(	(
<g/>
jaro	jaro	k1gNnSc4	jaro
<g/>
,	,	kIx,	,
podzim	podzim	k1gInSc4	podzim
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
krátká	krátký	k2eAgNnPc1d1	krátké
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
letní	letní	k2eAgFnPc1d1	letní
teploty	teplota	k1gFnPc1	teplota
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
už	už	k6eAd1	už
začátkem	začátkem	k7c2	začátkem
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
června	červen	k1gInSc2	červen
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
krátkodobě	krátkodobě	k6eAd1	krátkodobě
ochladit	ochladit	k5eAaPmF	ochladit
<g/>
.	.	kIx.	.
</s>
<s>
Léto	léto	k1gNnSc1	léto
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
polovici	polovice	k1gFnSc4	polovice
května	květen	k1gInSc2	květen
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
září	září	k1gNnSc2	září
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
často	často	k6eAd1	často
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
30	[number]	k4	30
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejnižší	nízký	k2eAgFnSc1d3	nejnižší
naměřená	naměřený	k2eAgFnSc1d1	naměřená
teplota	teplota	k1gFnSc1	teplota
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
130	[number]	k4	130
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
-42,2	-42,2	k4	-42,2
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
naměřena	naměřen	k2eAgFnSc1d1	naměřena
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
teplota	teplota	k1gFnSc1	teplota
byla	být	k5eAaImAgFnS	být
+39	+39	k4	+39
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
naměřena	naměřen	k2eAgFnSc1d1	naměřena
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
tvořilo	tvořit	k5eAaImAgNnS	tvořit
část	část	k1gFnSc4	část
moskevského	moskevský	k2eAgNnSc2d1	moskevské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
z	z	k7c2	z
92,65	[number]	k4	92,65
<g/>
%	%	kIx~	%
rusové	rus	k1gMnPc1	rus
<g/>
,	,	kIx,	,
1,42	[number]	k4	1,42
<g/>
%	%	kIx~	%
ukrajinci	ukrajinec	k1gMnPc1	ukrajinec
<g/>
,	,	kIx,	,
1,38	[number]	k4	1,38
<g/>
%	%	kIx~	%
tataři	tatar	k1gMnPc1	tatar
a	a	k8xC	a
0,98	[number]	k4	0,98
<g/>
%	%	kIx~	%
arméni	arméni	k5eAaImNgMnP	arméni
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
jako	jako	k8xC	jako
federální	federální	k2eAgNnSc1d1	federální
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
12	[number]	k4	12
administrativních	administrativní	k2eAgInPc2d1	administrativní
okruhů	okruh	k1gInPc2	okruh
<g/>
,	,	kIx,	,
125	[number]	k4	125
rajónů	rajón	k1gInPc2	rajón
<g/>
,	,	kIx,	,
21	[number]	k4	21
okresů	okres	k1gInPc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
každého	každý	k3xTgInSc2	každý
okresu	okres	k1gInSc2	okres
stojí	stát	k5eAaImIp3nS	stát
prefekt	prefekt	k1gMnSc1	prefekt
<g/>
,	,	kIx,	,
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
starostou	starosta	k1gMnSc7	starosta
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
představitelem	představitel	k1gMnSc7	představitel
moskevské	moskevský	k2eAgFnSc2d1	Moskevská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
a	a	k8xC	a
jemu	on	k3xPp3gMnSc3	on
bezprostředně	bezprostředně	k6eAd1	bezprostředně
podřízený	podřízený	k2eAgInSc1d1	podřízený
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
okres	okres	k1gInSc1	okres
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
radu	rada	k1gFnSc4	rada
sestávající	sestávající	k2eAgMnSc1d1	sestávající
z	z	k7c2	z
11	[number]	k4	11
volených	volený	k2eAgMnPc2d1	volený
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Kreml	Kreml	k1gInSc1	Kreml
s	s	k7c7	s
celkem	celek	k1gInSc7	celek
20	[number]	k4	20
věžemi	věž	k1gFnPc7	věž
je	být	k5eAaImIp3nS	být
nejobdivuhodnější	obdivuhodný	k2eAgFnSc7d3	nejobdivuhodnější
pamětihodností	pamětihodnost	k1gFnSc7	pamětihodnost
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
stavbu	stavba	k1gFnSc4	stavba
z	z	k7c2	z
červených	červený	k2eAgFnPc2d1	červená
pálených	pálený	k2eAgFnPc2d1	pálená
cihel	cihla	k1gFnPc2	cihla
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
italští	italský	k2eAgMnPc1d1	italský
stavební	stavební	k2eAgMnPc1d1	stavební
mistři	mistr	k1gMnPc1	mistr
pozvaní	pozvaný	k2eAgMnPc1d1	pozvaný
v	v	k7c4	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
carem	car	k1gMnSc7	car
Ivanem	Ivan	k1gMnSc7	Ivan
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Kreml	Kreml	k1gInSc1	Kreml
představuje	představovat	k5eAaImIp3nS	představovat
nejimpozantnější	impozantní	k2eAgInSc1d3	nejimpozantnější
soubor	soubor	k1gInSc1	soubor
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
architektonických	architektonický	k2eAgFnPc2d1	architektonická
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
kostely	kostel	k1gInPc1	kostel
a	a	k8xC	a
paláce	palác	k1gInPc1	palác
patří	patřit	k5eAaImIp3nP	patřit
chrám	chrám	k1gInSc1	chrám
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
У	У	k?	У
С	С	k?	С
<g/>
)	)	kIx)	)
se	s	k7c7	s
slavnými	slavný	k2eAgFnPc7d1	slavná
freskami	freska	k1gFnPc7	freska
<g/>
,	,	kIx,	,
chrám	chrám	k1gInSc1	chrám
Zvěstování	zvěstování	k1gNnSc2	zvěstování
(	(	kIx(	(
<g/>
Б	Б	k?	Б
с	с	k?	с
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chrám	chrám	k1gInSc1	chrám
sv.	sv.	kA	sv.
Archanděla	archanděl	k1gMnSc2	archanděl
Michaela	Michael	k1gMnSc2	Michael
(	(	kIx(	(
<g/>
А	А	k?	А
с	с	k?	с
<g/>
)	)	kIx)	)
a	a	k8xC	a
Velký	velký	k2eAgInSc1d1	velký
kremelský	kremelský	k2eAgInSc1d1	kremelský
palác	palác	k1gInSc1	palác
<g/>
.	.	kIx.	.
</s>
<s>
Zastavme	zastavit	k5eAaPmRp1nP	zastavit
se	se	k3xPyFc4	se
na	na	k7c6	na
Rudém	rudý	k2eAgNnSc6d1	Rudé
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
stran	strana	k1gFnPc2	strana
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
symboly	symbol	k1gInPc7	symbol
-	-	kIx~	-
chrámem	chrám	k1gInSc7	chrám
Vasila	Vasil	k1gMnSc2	Vasil
Blaženého	blažený	k2eAgMnSc2d1	blažený
<g/>
,	,	kIx,	,
mauzoleum	mauzoleum	k1gNnSc1	mauzoleum
V.	V.	kA	V.
I.	I.	kA	I.
Lenina	Lenin	k1gMnSc2	Lenin
a	a	k8xC	a
obchodním	obchodní	k2eAgInSc7d1	obchodní
domem	dům	k1gInSc7	dům
GUM	guma	k1gFnPc2	guma
(	(	kIx(	(
<g/>
zařazený	zařazený	k2eAgMnSc1d1	zařazený
mezi	mezi	k7c4	mezi
10	[number]	k4	10
nejluxusnějších	luxusní	k2eAgInPc2d3	nejluxusnější
obchodních	obchodní	k2eAgInPc2d1	obchodní
domů	dům	k1gInPc2	dům
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ústřední	ústřední	k2eAgFnSc4d1	ústřední
dvoranu	dvorana	k1gFnSc4	dvorana
se	s	k7c7	s
zastřešením	zastřešení	k1gNnSc7	zastřešení
ze	z	k7c2	z
skla	sklo	k1gNnSc2	sklo
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
galerie	galerie	k1gFnPc4	galerie
s	s	k7c7	s
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
obchody	obchod	k1gInPc7	obchod
a	a	k8xC	a
provozovnami	provozovna	k1gFnPc7	provozovna
služeb	služba	k1gFnPc2	služba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
následuje	následovat	k5eAaImIp3nS	následovat
Tverská	Tverský	k2eAgFnSc1d1	Tverská
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
Starý	starý	k2eAgInSc1d1	starý
a	a	k8xC	a
Nový	nový	k2eAgInSc1d1	nový
Arbat	Arbat	k1gInSc1	Arbat
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
Arbat	Arbat	k1gInSc1	Arbat
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
centrem	centrum	k1gNnSc7	centrum
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
pestrý	pestrý	k2eAgMnSc1d1	pestrý
<g/>
,	,	kIx,	,
životem	život	k1gInSc7	život
pulsující	pulsující	k2eAgInSc4d1	pulsující
bleší	bleší	k2eAgInSc4d1	bleší
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Arbatu	Arbat	k1gInSc6	Arbat
jde	jít	k5eAaImIp3nS	jít
posedět	posedět	k5eAaPmF	posedět
v	v	k7c6	v
některé	některý	k3yIgFnSc6	některý
z	z	k7c2	z
restaurací	restaurace	k1gFnPc2	restaurace
připravující	připravující	k2eAgFnSc2d1	připravující
speciality	specialita	k1gFnSc2	specialita
z	z	k7c2	z
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgFnPc2	všecek
oblastí	oblast	k1gFnPc2	oblast
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Dochovaly	dochovat	k5eAaPmAgFnP	dochovat
se	se	k3xPyFc4	se
překrásné	překrásný	k2eAgFnPc1d1	překrásná
stavby	stavba	k1gFnPc1	stavba
klasické	klasický	k2eAgFnSc2d1	klasická
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
opevněné	opevněný	k2eAgInPc1d1	opevněný
kláštery	klášter	k1gInPc1	klášter
a	a	k8xC	a
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
kostelů	kostel	k1gInPc2	kostel
postavených	postavený	k2eAgInPc2d1	postavený
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Bolšoj	Bolšoj	k1gInSc1	Bolšoj
těatr	těatr	k1gInSc1	těatr
je	být	k5eAaImIp3nS	být
světově	světově	k6eAd1	světově
proslulé	proslulý	k2eAgNnSc4d1	proslulé
divadlo	divadlo	k1gNnSc4	divadlo
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
návštěvností	návštěvnost	k1gFnSc7	návštěvnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozsáhlém	rozsáhlý	k2eAgInSc6d1	rozsáhlý
areálu	areál	k1gInSc6	areál
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Moskvy	Moskva	k1gFnSc2	Moskva
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Výstaviště	výstaviště	k1gNnSc1	výstaviště
VDNCh	VDNCha	k1gFnPc2	VDNCha
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Výstaviště	výstaviště	k1gNnSc1	výstaviště
úspěchů	úspěch	k1gInPc2	úspěch
národního	národní	k2eAgNnSc2d1	národní
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
VVC	VVC	kA	VVC
<g/>
)	)	kIx)	)
představující	představující	k2eAgFnPc4d1	představující
jak	jak	k8xS	jak
bývalé	bývalý	k2eAgFnPc4d1	bývalá
součásti	součást	k1gFnPc4	součást
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
v	v	k7c6	v
Muzeu	muzeum	k1gNnSc6	muzeum
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
jinak	jinak	k6eAd1	jinak
tematicky	tematicky	k6eAd1	tematicky
laděné	laděný	k2eAgFnPc4d1	laděná
čtvrti	čtvrt	k1gFnPc4	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Najdete	najít	k5eAaPmIp2nP	najít
tu	tu	k6eAd1	tu
tedy	tedy	k9	tedy
jak	jak	k6eAd1	jak
ukázky	ukázka	k1gFnPc1	ukázka
technického	technický	k2eAgInSc2d1	technický
pokroku	pokrok	k1gInSc2	pokrok
včetně	včetně	k7c2	včetně
rakety	raketa	k1gFnSc2	raketa
Sojuz	Sojuz	k1gInSc1	Sojuz
nebo	nebo	k8xC	nebo
obřích	obří	k2eAgInPc2d1	obří
prvků	prvek	k1gInPc2	prvek
sovětské	sovětský	k2eAgFnSc2d1	sovětská
elektrické	elektrický	k2eAgFnSc2d1	elektrická
přenosové	přenosový	k2eAgFnSc2d1	přenosová
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
velkovýkrmnu	velkovýkrmna	k1gFnSc4	velkovýkrmna
vepřů	vepř	k1gMnPc2	vepř
nebo	nebo	k8xC	nebo
vyasfaltované	vyasfaltovaný	k2eAgNnSc1d1	vyasfaltované
koňské	koňský	k2eAgNnSc1d1	koňské
závodiště	závodiště	k1gNnSc1	závodiště
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
i	i	k9	i
největší	veliký	k2eAgFnSc1d3	veliký
maketa	maketa	k1gFnSc1	maketa
Moskvy	Moskva	k1gFnSc2	Moskva
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
lunapark	lunapark	k1gInSc1	lunapark
<g/>
,	,	kIx,	,
Atomový	atomový	k2eAgInSc1d1	atomový
pavilon	pavilon	k1gInSc1	pavilon
<g/>
,	,	kIx,	,
Labyrint	labyrint	k1gInSc1	labyrint
strachu	strach	k1gInSc2	strach
<g/>
,	,	kIx,	,
Pavilon	pavilon	k1gInSc1	pavilon
vesmíru	vesmír	k1gInSc2	vesmír
či	či	k8xC	či
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
testovacích	testovací	k2eAgInPc2d1	testovací
modelů	model	k1gInPc2	model
raketoplánu	raketoplán	k1gInSc2	raketoplán
Buran	buran	k1gMnSc1	buran
<g/>
.	.	kIx.	.
</s>
<s>
Krásné	krásný	k2eAgFnPc1d1	krásná
jsou	být	k5eAaImIp3nP	být
fontány	fontána	k1gFnPc1	fontána
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
výstaviště	výstaviště	k1gNnSc2	výstaviště
působí	působit	k5eAaImIp3nP	působit
kontroverzním	kontroverzní	k2eAgInSc7d1	kontroverzní
dojmem	dojem	k1gInSc7	dojem
připomínající	připomínající	k2eAgInSc1d1	připomínající
těžký	těžký	k2eAgInSc1d1	těžký
život	život	k1gInSc1	život
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Gorky	Gorka	k1gMnSc2	Gorka
Parku	park	k1gInSc6	park
<g/>
,	,	kIx,	,
situovaném	situovaný	k2eAgInSc6d1	situovaný
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Moskvy	Moskva	k1gFnSc2	Moskva
a	a	k8xC	a
založeném	založený	k2eAgInSc6d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
najdeme	najít	k5eAaPmIp1nP	najít
různé	různý	k2eAgFnPc4d1	různá
zábavní	zábavní	k2eAgFnPc4d1	zábavní
atrakce	atrakce	k1gFnPc4	atrakce
<g/>
,	,	kIx,	,
horskou	horský	k2eAgFnSc4d1	horská
dráhu	dráha	k1gFnSc4	dráha
i	i	k8xC	i
ruské	ruský	k2eAgNnSc4d1	ruské
kolo	kolo	k1gNnSc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
se	se	k3xPyFc4	se
také	také	k9	také
koná	konat	k5eAaImIp3nS	konat
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
hlavními	hlavní	k2eAgInPc7d1	hlavní
ceny	cena	k1gFnPc1	cena
se	se	k3xPyFc4	se
udílí	udílet	k5eAaImIp3nP	udílet
zlaté	zlatý	k2eAgFnPc4d1	zlatá
a	a	k8xC	a
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
medaile	medaile	k1gFnPc4	medaile
a	a	k8xC	a
také	také	k9	také
hlavní	hlavní	k2eAgFnSc1d1	hlavní
ruská	ruský	k2eAgFnSc1d1	ruská
filmová	filmový	k2eAgFnSc1d1	filmová
cena	cena	k1gFnSc1	cena
Nika	nika	k1gFnSc1	nika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
zajímavosti	zajímavost	k1gFnPc4	zajímavost
patří	patřit	k5eAaImIp3nS	patřit
160	[number]	k4	160
tun	tuna	k1gFnPc2	tuna
těžký	těžký	k2eAgInSc1d1	těžký
zvon	zvon	k1gInSc1	zvon
Car	car	k1gMnSc1	car
kolokol	kolokol	k1gInSc1	kolokol
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
úřední	úřední	k2eAgFnSc2d1	úřední
budovy	budova	k1gFnSc2	budova
ruského	ruský	k2eAgMnSc2d1	ruský
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
Moskevská	moskevský	k2eAgFnSc1d1	Moskevská
univerzita	univerzita	k1gFnSc1	univerzita
s	s	k7c7	s
výhledem	výhled	k1gInSc7	výhled
na	na	k7c4	na
olympijský	olympijský	k2eAgInSc4d1	olympijský
stadion	stadion	k1gInSc4	stadion
v	v	k7c6	v
Lužnikách	Lužnika	k1gFnPc6	Lužnika
<g/>
,	,	kIx,	,
Treťjakova	Treťjakův	k2eAgFnSc1d1	Treťjakova
galerie	galerie	k1gFnSc1	galerie
se	s	k7c7	s
světově	světově	k6eAd1	světově
proslulou	proslulý	k2eAgFnSc7d1	proslulá
sbírkou	sbírka	k1gFnSc7	sbírka
a	a	k8xC	a
televizní	televizní	k2eAgMnSc1d1	televizní
vysílač	vysílač	k1gMnSc1	vysílač
Ostankino	Ostankino	k1gNnSc1	Ostankino
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
stmívá	stmívat	k5eAaImIp3nS	stmívat
až	až	k9	až
v	v	k7c4	v
jedenáct	jedenáct	k4xCc4	jedenáct
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
sídlí	sídlet	k5eAaImIp3nS	sídlet
tyto	tento	k3xDgInPc1	tento
sportovní	sportovní	k2eAgInPc1d1	sportovní
týmy	tým	k1gInPc1	tým
<g/>
:	:	kIx,	:
HC	HC	kA	HC
CSKA	CSKA	kA	CSKA
Moskva	Moskva	k1gFnSc1	Moskva
-	-	kIx~	-
KHL	KHL	kA	KHL
<g/>
,	,	kIx,	,
LSK	LSK	kA	LSK
CSKA	CSKA	kA	CSKA
OHK	OHK	kA	OHK
Dynamo	dynamo	k1gNnSc4	dynamo
Moskva	Moskva	k1gFnSc1	Moskva
-	-	kIx~	-
KHL	KHL	kA	KHL
<g/>
,	,	kIx,	,
Megasport	Megasport	k1gInSc4	Megasport
Arena	Aren	k1gMnSc2	Aren
HC	HC	kA	HC
Spartak	Spartak	k1gInSc1	Spartak
Moskva	Moskva	k1gFnSc1	Moskva
-	-	kIx~	-
KHL	KHL	kA	KHL
<g/>
,	,	kIx,	,
Sokolniki	Sokolniki	k1gNnSc2	Sokolniki
Arena	Areno	k1gNnSc2	Areno
FK	FK	kA	FK
Spartak	Spartak	k1gInSc1	Spartak
Moskva	Moskva	k1gFnSc1	Moskva
-	-	kIx~	-
Ruská	ruský	k2eAgFnSc1d1	ruská
Premier	Premier	k1gInSc1	Premier
Liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
Otkrytie	Otkrytie	k1gFnSc1	Otkrytie
Arena	Areno	k1gNnSc2	Areno
PFK	PFK	kA	PFK
CSKA	CSKA	kA	CSKA
Moskva	Moskva	k1gFnSc1	Moskva
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
Premier	Premier	k1gInSc1	Premier
Liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
Lužniki	Lužniki	k1gNnSc1	Lužniki
PBC	PBC	kA	PBC
CSKA	CSKA	kA	CSKA
Moskva	Moskva	k1gFnSc1	Moskva
-	-	kIx~	-
VTB	VTB	kA	VTB
United	United	k1gInSc1	United
League	Leagu	k1gMnSc2	Leagu
a	a	k8xC	a
Euroliga	Eurolig	k1gMnSc2	Eurolig
v	v	k7c6	v
basketbale	basketbal	k1gInSc6	basketbal
<g/>
,	,	kIx,	,
Megasport	Megasport	k1gInSc1	Megasport
Arena	Areno	k1gNnSc2	Areno
BK	BK	kA	BK
Dynamo	dynamo	k1gNnSc1	dynamo
Moskva	Moskva	k1gFnSc1	Moskva
-	-	kIx~	-
Russian	Russian	k1gInSc1	Russian
Basketball	Basketballa	k1gFnPc2	Basketballa
Super	super	k2eAgFnSc2d1	super
League	Leagu	k1gFnSc2	Leagu
<g/>
,	,	kIx,	,
Krylatskoje	Krylatskoj	k1gInSc2	Krylatskoj
Sport	sport	k1gInSc1	sport
Palace	Palace	k1gFnSc1	Palace
Slava	Slava	k1gFnSc1	Slava
-	-	kIx~	-
Professional	Professional	k1gFnSc1	Professional
Rugby	rugby	k1gNnSc2	rugby
League	League	k1gNnSc2	League
<g/>
,	,	kIx,	,
Slava	Slava	k1gFnSc1	Slava
Stadium	stadium	k1gNnSc1	stadium
Dynamo	dynamo	k1gNnSc1	dynamo
Moskva	Moskva	k1gFnSc1	Moskva
-	-	kIx~	-
Russian	Russian	k1gInSc1	Russian
Bandy	banda	k1gFnSc2	banda
Super	super	k2eAgInSc2d1	super
League	Leagu	k1gInSc2	Leagu
<g/>
,	,	kIx,	,
Krylatskoje	Krylatskoj	k1gInSc2	Krylatskoj
Sport	sport	k1gInSc1	sport
Palace	Palace	k1gFnSc2	Palace
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
jako	jako	k9	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgMnSc1d3	veliký
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
i	i	k8xC	i
jako	jako	k9	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Kříží	křížit	k5eAaImIp3nS	křížit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c4	mnoho
železničních	železniční	k2eAgFnPc2d1	železniční
<g/>
,	,	kIx,	,
silničních	silniční	k2eAgFnPc2d1	silniční
<g/>
,	,	kIx,	,
vodních	vodní	k2eAgFnPc2d1	vodní
i	i	k8xC	i
leteckých	letecký	k2eAgFnPc2d1	letecká
tras	trasa	k1gFnPc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
je	být	k5eAaImIp3nS	být
nejzápadnější	západní	k2eAgNnSc4d3	nejzápadnější
místo	místo	k1gNnSc4	místo
Transsibiřské	transsibiřský	k2eAgFnSc2d1	Transsibiřská
magistrály	magistrála	k1gFnSc2	magistrála
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pět	pět	k4xCc4	pět
mezinárodních	mezinárodní	k2eAgNnPc2d1	mezinárodní
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc4d3	veliký
jsou	být	k5eAaImIp3nP	být
Šeremeťjevo	Šeremeťjevo	k1gNnSc4	Šeremeťjevo
<g/>
,	,	kIx,	,
Domodědovo	Domodědův	k2eAgNnSc4d1	Domodědovo
a	a	k8xC	a
Vnukovo	vnukův	k2eAgNnSc4d1	Vnukovo
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k9	též
i	i	k9	i
systém	systém	k1gInSc1	systém
MHD	MHD	kA	MHD
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
a	a	k8xC	a
nejvíce	nejvíce	k6eAd1	nejvíce
vytížený	vytížený	k2eAgMnSc1d1	vytížený
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
;	;	kIx,	;
tvoří	tvořit	k5eAaImIp3nS	tvořit
jej	on	k3xPp3gMnSc4	on
dvanáct	dvanáct	k4xCc1	dvanáct
linek	linka	k1gFnPc2	linka
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
<g/>
,	,	kIx,	,
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
<g/>
,	,	kIx,	,
autobusová	autobusový	k2eAgFnSc1d1	autobusová
síť	síť	k1gFnSc1	síť
a	a	k8xC	a
monorail	monorail	k1gInSc1	monorail
<g/>
.	.	kIx.	.
</s>
<s>
Metro	metro	k1gNnSc1	metro
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejhlubších	hluboký	k2eAgInPc2d3	nejhlubší
systémů	systém	k1gInPc2	systém
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
stanice	stanice	k1gFnSc1	stanice
Park	park	k1gInSc1	park
Pobědy	poběda	k1gFnSc2	poběda
je	být	k5eAaImIp3nS	být
84	[number]	k4	84
<g/>
m	m	kA	m
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
nejdelší	dlouhý	k2eAgInPc4d3	nejdelší
eskalátory	eskalátor	k1gInPc4	eskalátor
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Metro	metro	k1gNnSc1	metro
přepraví	přepravit	k5eAaPmIp3nS	přepravit
až	až	k9	až
9,3	[number]	k4	9,3
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejvytíženější	vytížený	k2eAgNnSc1d3	nejvytíženější
metro	metro	k1gNnSc1	metro
mimo	mimo	k7c4	mimo
Asii	Asie	k1gFnSc4	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
:	:	kIx,	:
inspirace	inspirace	k1gFnSc1	inspirace
na	na	k7c4	na
cesty	cesta	k1gFnPc4	cesta
<g/>
..	..	k?	..
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
141	[number]	k4	141
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87471	[number]	k4	87471
<g/>
-	-	kIx~	-
<g/>
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
..	..	k?	..
LUKAVEC	LUKAVEC	kA	LUKAVEC
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Slované	Slovan	k1gMnPc1	Slovan
a	a	k8xC	a
probuzení	probuzení	k1gNnSc1	probuzení
lidství	lidství	k1gNnSc2	lidství
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
:	:	kIx,	:
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
Od	od	k7c2	od
českého	český	k2eAgNnSc2d1	české
Tokia	Tokio	k1gNnSc2	Tokio
k	k	k7c3	k
exotické	exotický	k2eAgFnSc3d1	exotická
Praze	Praha	k1gFnSc3	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Malvern	Malvern	k1gNnSc1	Malvern
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
317	[number]	k4	317
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87580	[number]	k4	87580
<g/>
-	-	kIx~	-
<g/>
61	[number]	k4	61
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Moskva	Moskva	k1gFnSc1	Moskva
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Moskva	Moskva	k1gFnSc1	Moskva
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Moskva	Moskva	k1gFnSc1	Moskva
-	-	kIx~	-
průvodce	průvodce	k1gMnSc1	průvodce
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
www.mos.ru	www.mos.r	k1gInSc3	www.mos.r
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Fotogalerie	Fotogalerie	k1gFnSc1	Fotogalerie
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Fotogalerie	Fotogalerie	k1gFnSc1	Fotogalerie
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
turistické	turistický	k2eAgFnPc1d1	turistická
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
Moskvě	Moskva	k1gFnSc6	Moskva
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Panoramata	panorama	k1gNnPc4	panorama
Moskvy	Moskva	k1gFnSc2	Moskva
na	na	k7c6	na
mapách	mapa	k1gFnPc6	mapa
Yandex	Yandex	k1gInSc1	Yandex
<g/>
.	.	kIx.	.
<g/>
ru	ru	k?	ru
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
</s>
