<s>
Pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
<g/>
,	,	kIx,	,
pohlavní	pohlavní	k2eAgFnSc1d1	pohlavní
dvojtvárnost	dvojtvárnost	k1gFnSc1	dvojtvárnost
či	či	k8xC	či
sexuální	sexuální	k2eAgInSc1d1	sexuální
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
je	být	k5eAaImIp3nS	být
výraz	výraz	k1gInSc4	výraz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
u	u	k7c2	u
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
samice	samice	k1gFnSc1	samice
vypadá	vypadat	k5eAaPmIp3nS	vypadat
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
samec	samec	k1gMnSc1	samec
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nejen	nejen	k6eAd1	nejen
v	v	k7c4	v
pohlaví	pohlaví	k1gNnSc4	pohlaví
jedince	jedinec	k1gMnSc2	jedinec
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc6	jeho
pohlavních	pohlavní	k2eAgInPc6d1	pohlavní
orgánech	orgán	k1gInPc6	orgán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
v	v	k7c6	v
sekundárních	sekundární	k2eAgInPc6d1	sekundární
pohlavních	pohlavní	k2eAgInPc6d1	pohlavní
znacích	znak	k1gInPc6	znak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
velikost	velikost	k1gFnSc4	velikost
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
zbarvení	zbarvení	k1gNnSc1	zbarvení
<g/>
,	,	kIx,	,
tvar	tvar	k1gInSc1	tvar
aj.	aj.	kA	aj.
Pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
se	se	k3xPyFc4	se
v	v	k7c6	v
různé	různý	k2eAgFnSc6d1	různá
míře	míra	k1gFnSc6	míra
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
zástupců	zástupce	k1gMnPc2	zástupce
mnoha	mnoho	k4c2	mnoho
skupin	skupina	k1gFnPc2	skupina
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
savců	savec	k1gMnPc2	savec
bývají	bývat	k5eAaImIp3nP	bývat
samci	samec	k1gMnPc1	samec
zpravidla	zpravidla	k6eAd1	zpravidla
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
50	[number]	k4	50
%	%	kIx~	%
(	(	kIx(	(
<g/>
např.	např.	kA	např.
rypouš	rypouš	k1gMnSc1	rypouš
sloní	sloní	k2eAgMnSc1d1	sloní
<g/>
,	,	kIx,	,
mrož	mrož	k1gMnSc1	mrož
<g/>
,	,	kIx,	,
vorvaň	vorvaň	k1gMnSc1	vorvaň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nepočetných	početný	k2eNgFnPc2d1	nepočetná
výjimek	výjimka	k1gFnPc2	výjimka
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
hyena	hyena	k1gFnSc1	hyena
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
savců	savec	k1gMnPc2	savec
poznáme	poznat	k5eAaPmIp1nP	poznat
podle	podle	k7c2	podle
uspořádání	uspořádání	k1gNnSc2	uspořádání
kopulačního	kopulační	k2eAgNnSc2d1	kopulační
ústrojí	ústrojí	k1gNnSc2	ústrojí
a	a	k8xC	a
více	hodně	k6eAd2	hodně
vyvinutých	vyvinutý	k2eAgFnPc2d1	vyvinutá
mléčných	mléčný	k2eAgFnPc2d1	mléčná
žláz	žláza	k1gFnPc2	žláza
(	(	kIx(	(
<g/>
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
kaloňů	kaloň	k1gMnPc2	kaloň
je	být	k5eAaImIp3nS	být
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
dobře	dobře	k6eAd1	dobře
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
a	a	k8xC	a
plně	plně	k6eAd1	plně
funkční	funkční	k2eAgInSc1d1	funkční
i	i	k8xC	i
samci	samec	k1gMnPc1	samec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
savců	savec	k1gMnPc2	savec
mívají	mívat	k5eAaImIp3nP	mívat
zpravidla	zpravidla	k6eAd1	zpravidla
dobře	dobře	k6eAd1	dobře
viditelný	viditelný	k2eAgInSc1d1	viditelný
penis	penis	k1gInSc1	penis
a	a	k8xC	a
až	až	k9	až
na	na	k7c4	na
několik	několik	k4yIc4	několik
výijmek	výijmka	k1gFnPc2	výijmka
(	(	kIx(	(
<g/>
kytovci	kytovec	k1gMnPc1	kytovec
<g/>
,	,	kIx,	,
sirény	siréna	k1gFnPc1	siréna
<g/>
,	,	kIx,	,
chobotnatci	chobotnatec	k1gMnPc1	chobotnatec
<g/>
,	,	kIx,	,
hrabáč	hrabáč	k1gMnSc1	hrabáč
<g/>
)	)	kIx)	)
také	také	k9	také
šourek	šourek	k1gInSc1	šourek
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
hyena	hyena	k1gFnSc1	hyena
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
vypadá	vypadat	k5eAaImIp3nS	vypadat
zevní	zevní	k2eAgInSc1d1	zevní
pohlavní	pohlavní	k2eAgFnSc7d1	pohlavní
ústrojí	ústroj	k1gFnSc7	ústroj
samců	samec	k1gMnPc2	samec
i	i	k8xC	i
samic	samice	k1gFnPc2	samice
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
klitoris	klitoris	k1gFnSc1	klitoris
samice	samice	k1gFnSc2	samice
je	být	k5eAaImIp3nS	být
mimořádně	mimořádně	k6eAd1	mimořádně
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgInSc1d1	připomínající
penis	penis	k1gInSc1	penis
a	a	k8xC	a
u	u	k7c2	u
jeho	jeho	k3xOp3gFnSc2	jeho
báze	báze	k1gFnSc2	báze
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jakýsi	jakýsi	k3yIgInSc4	jakýsi
nepravý	pravý	k2eNgInSc4d1	nepravý
šourek	šourek	k1gInSc4	šourek
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
ptakořitných	ptakořitný	k2eAgInPc2d1	ptakořitný
a	a	k8xC	a
většiny	většina	k1gFnSc2	většina
druhů	druh	k1gInPc2	druh
vačnatců	vačnatec	k1gMnPc2	vačnatec
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
vak	vak	k1gInSc4	vak
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
druhotné	druhotný	k2eAgInPc1d1	druhotný
pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
znaky	znak	k1gInPc1	znak
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
vždy	vždy	k6eAd1	vždy
výrazněji	výrazně	k6eAd2	výrazně
u	u	k7c2	u
savčích	savčí	k2eAgInPc2d1	savčí
samců	samec	k1gInPc2	samec
než	než	k8xS	než
samic	samice	k1gFnPc2	samice
<g/>
:	:	kIx,	:
výraznější	výrazný	k2eAgFnSc1d2	výraznější
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
tmavě	tmavě	k6eAd1	tmavě
zbarvené	zbarvený	k2eAgNnSc1d1	zbarvené
ochlupení	ochlupení	k1gNnSc1	ochlupení
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
šíji	šíj	k1gFnSc6	šíj
<g/>
:	:	kIx,	:
např.	např.	kA	např.
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
paviáni	pavián	k1gMnPc1	pavián
<g/>
,	,	kIx,	,
lev	lev	k1gMnSc1	lev
<g/>
,	,	kIx,	,
lachtan	lachtan	k1gMnSc1	lachtan
hřívnatý	hřívnatý	k2eAgMnSc1d1	hřívnatý
<g/>
,	,	kIx,	,
bizon	bizon	k1gMnSc1	bizon
americký	americký	k2eAgMnSc1d1	americký
<g/>
,	,	kIx,	,
kožní	kožní	k2eAgInPc1d1	kožní
výrůstky	výrůstek	k1gInPc1	výrůstek
a	a	k8xC	a
duplikatury	duplikatura	k1gFnPc1	duplikatura
nebo	nebo	k8xC	nebo
<g />
.	.	kIx.	.
</s>
<s>
výrazně	výrazně	k6eAd1	výrazně
zbarvené	zbarvený	k2eAgFnPc4d1	zbarvená
kožní	kožní	k2eAgFnPc4d1	kožní
plochy	plocha	k1gFnPc4	plocha
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
:	:	kIx,	:
např.	např.	kA	např.
mandril	mandril	k1gMnSc1	mandril
<g/>
,	,	kIx,	,
kahau	kahau	k5eAaPmIp1nS	kahau
nosatý	nosatý	k2eAgMnSc1d1	nosatý
<g/>
,	,	kIx,	,
rypouš	rypouš	k1gMnSc1	rypouš
sloní	slonit	k5eAaImIp3nS	slonit
<g/>
,	,	kIx,	,
čepcol	čepcol	k1gInSc1	čepcol
hřebenitý	hřebenitý	k2eAgInSc1d1	hřebenitý
nebo	nebo	k8xC	nebo
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
dželada	dželada	k1gFnSc1	dželada
<g/>
,	,	kIx,	,
rohy	roh	k1gInPc1	roh
a	a	k8xC	a
paroží	paroží	k1gNnSc1	paroží
<g/>
:	:	kIx,	:
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
u	u	k7c2	u
obou	dva	k4xCgNnPc2	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
silnější	silný	k2eAgFnSc1d2	silnější
<g/>
:	:	kIx,	:
turovití	turovití	k1gMnPc1	turovití
<g/>
,	,	kIx,	,
jelenovití	jelenovití	k1gMnPc1	jelenovití
<g/>
,	,	kIx,	,
žirafovití	žirafovitý	k2eAgMnPc1d1	žirafovitý
<g/>
,	,	kIx,	,
vidloroh	vidloroh	k1gMnSc1	vidloroh
<g/>
,	,	kIx,	,
nosorožci	nosorožec	k1gMnPc1	nosorožec
aj.	aj.	kA	aj.
Někdy	někdy	k6eAd1	někdy
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
rohy	roh	k1gInPc4	roh
samců	samec	k1gMnPc2	samec
i	i	k8xC	i
samic	samice	k1gFnPc2	samice
stejné	stejný	k2eAgFnSc2d1	stejná
velké	velká	k1gFnSc2	velká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
liší	lišit	k5eAaImIp3nP	lišit
se	s	k7c7	s
tvarem	tvar	k1gInSc7	tvar
(	(	kIx(	(
<g/>
některé	některý	k3yIgFnPc1	některý
antilopy	antilopa	k1gFnPc1	antilopa
<g/>
,	,	kIx,	,
kamzík	kamzík	k1gMnSc1	kamzík
horský	horský	k2eAgMnSc1d1	horský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
mohou	moct	k5eAaImIp3nP	moct
rohy	roh	k1gInPc4	roh
u	u	k7c2	u
obou	dva	k4xCgNnPc2	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
vypadat	vypadat	k5eAaImF	vypadat
stejně	stejně	k6eAd1	stejně
(	(	kIx(	(
<g/>
přímorožci	přímorožec	k1gMnPc1	přímorožec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kly	kel	k1gInPc1	kel
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
u	u	k7c2	u
obou	dva	k4xCgNnPc2	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
jsou	být	k5eAaImIp3nP	být
silnější	silný	k2eAgMnPc1d2	silnější
a	a	k8xC	a
delší	dlouhý	k2eAgMnPc1d2	delší
<g/>
:	:	kIx,	:
prasatovití	prasatovitý	k2eAgMnPc1d1	prasatovitý
<g/>
,	,	kIx,	,
hroši	hroch	k1gMnPc1	hroch
<g/>
,	,	kIx,	,
kabar	kabar	k1gMnSc1	kabar
pižmový	pižmový	k2eAgMnSc1d1	pižmový
<g/>
,	,	kIx,	,
kančilové	kančilové	k2eAgMnSc1d1	kančilové
<g/>
,	,	kIx,	,
mrož	mrož	k1gMnSc1	mrož
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
narval	narval	k1gMnSc1	narval
<g/>
,	,	kIx,	,
vorvaňovci	vorvaňovec	k1gMnPc1	vorvaňovec
aj.	aj.	kA	aj.
<g/>
,	,	kIx,	,
rozdíly	rozdíl	k1gInPc1	rozdíl
ve	v	k7c4	v
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
antilop	antilopa	k1gFnPc2	antilopa
a	a	k8xC	a
turů	tur	k1gMnPc2	tur
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
samci	samec	k1gMnPc1	samec
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
tmavší	tmavý	k2eAgFnPc1d2	tmavší
než	než	k8xS	než
samice	samice	k1gFnPc1	samice
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
nyala	nyala	k6eAd1	nyala
nížinná	nížinný	k2eAgFnSc1d1	nížinná
<g/>
,	,	kIx,	,
antilopa	antilopa	k1gFnSc1	antilopa
vraná	vraný	k2eAgFnSc1d1	Vraná
<g/>
,	,	kIx,	,
voduška	voduška	k1gFnSc1	voduška
abok	abok	k1gInSc1	abok
<g/>
,	,	kIx,	,
antilopa	antilopa	k1gFnSc1	antilopa
jelení	jelení	k2eAgFnSc1d1	jelení
<g/>
,	,	kIx,	,
banteng	banteng	k1gMnSc1	banteng
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
u	u	k7c2	u
paviána	pavián	k1gMnSc2	pavián
pláštíkového	pláštíkový	k2eAgMnSc2d1	pláštíkový
<g/>
;	;	kIx,	;
celkově	celkově	k6eAd1	celkově
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
,	,	kIx,	,
výrazné	výrazný	k2eAgInPc1d1	výrazný
rozdíly	rozdíl	k1gInPc1	rozdíl
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
těla	tělo	k1gNnSc2	tělo
včetně	včetně	k7c2	včetně
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
orgánů	orgán	k1gInPc2	orgán
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
výjimečné	výjimečný	k2eAgFnSc2d1	výjimečná
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
výjimek	výjimka	k1gFnPc2	výjimka
je	být	k5eAaImIp3nS	být
africký	africký	k2eAgMnSc1d1	africký
kaloň	kaloň	k1gMnSc1	kaloň
kladivohlavý	kladivohlavý	k2eAgMnSc1d1	kladivohlavý
-	-	kIx~	-
jeho	jeho	k3xOp3gMnPc1	jeho
samci	samec	k1gMnPc1	samec
mají	mít	k5eAaImIp3nP	mít
vedle	vedle	k7c2	vedle
nadměrné	nadměrný	k2eAgFnSc2d1	nadměrná
hlavy	hlava	k1gFnSc2	hlava
s	s	k7c7	s
rezonančními	rezonanční	k2eAgInPc7d1	rezonanční
vaky	vak	k1gInPc7	vak
také	také	k9	také
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
stavbu	stavba	k1gFnSc4	stavba
hrtanu	hrtan	k1gInSc2	hrtan
a	a	k8xC	a
průdušnice	průdušnice	k1gFnSc2	průdušnice
než	než	k8xS	než
samice	samice	k1gFnSc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
druhotné	druhotný	k2eAgInPc1d1	druhotný
pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
znaky	znak	k1gInPc1	znak
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
vždy	vždy	k6eAd1	vždy
výrazněji	výrazně	k6eAd2	výrazně
u	u	k7c2	u
savčích	savčí	k2eAgFnPc2d1	savčí
samic	samice	k1gFnPc2	samice
než	než	k8xS	než
samců	samec	k1gInPc2	samec
<g/>
:	:	kIx,	:
výrazně	výrazně	k6eAd1	výrazně
zduřelé	zduřelý	k2eAgFnSc2d1	zduřelá
mozloly	mozlola	k1gFnSc2	mozlola
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
řiti	řiť	k1gFnSc2	řiť
u	u	k7c2	u
říjných	říjný	k2eAgFnPc2d1	říjná
samic	samice	k1gFnPc2	samice
paviánů	pavián	k1gMnPc2	pavián
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
dalších	další	k2eAgFnPc2d1	další
opic	opice	k1gFnPc2	opice
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
dimorfismus	dimorfismus	k1gInSc4	dimorfismus
projevuje	projevovat	k5eAaImIp3nS	projevovat
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
polygamních	polygamní	k2eAgInPc2d1	polygamní
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
samec	samec	k1gInSc1	samec
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
samice	samice	k1gFnPc1	samice
<g/>
,	,	kIx,	,
u	u	k7c2	u
dravců	dravec	k1gMnPc2	dravec
a	a	k8xC	a
většiny	většina	k1gFnSc2	většina
druhů	druh	k1gMnPc2	druh
sov	sova	k1gFnPc2	sova
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
větší	veliký	k2eAgFnSc1d2	veliký
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
monogamních	monogamní	k2eAgInPc2d1	monogamní
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
husy	husa	k1gFnPc1	husa
<g/>
,	,	kIx,	,
jeřábi	jeřáb	k1gMnPc1	jeřáb
<g/>
,	,	kIx,	,
tučňáci	tučňák	k1gMnPc1	tučňák
<g/>
,	,	kIx,	,
sovy	sova	k1gFnPc1	sova
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
pěvců	pěvec	k1gMnPc2	pěvec
<g/>
,	,	kIx,	,
z	z	k7c2	z
hrabavých	hrabavý	k2eAgNnPc2d1	hrabavé
např.	např.	kA	např.
koroptev	koroptev	k1gFnSc4	koroptev
polní	polní	k2eAgMnSc1d1	polní
či	či	k8xC	či
jeřábek	jeřábek	k1gMnSc1	jeřábek
lesní	lesní	k2eAgMnPc1d1	lesní
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
zbarvením	zbarvení	k1gNnSc7	zbarvení
pohlaví	pohlaví	k1gNnSc2	pohlaví
méně	málo	k6eAd2	málo
výrazné	výrazný	k2eAgFnSc3d1	výrazná
než	než	k8xS	než
u	u	k7c2	u
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
druhů	druh	k1gInPc2	druh
polygamních	polygamní	k2eAgInPc2d1	polygamní
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgInPc1d3	nejčastější
jsou	být	k5eAaImIp3nP	být
rozdíly	rozdíl	k1gInPc1	rozdíl
ve	v	k7c4	v
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tvar	tvar	k1gInSc1	tvar
těla	tělo	k1gNnSc2	tělo
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
u	u	k7c2	u
obou	dva	k4xCgNnPc2	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgMnPc7d1	typický
představiteli	představitel	k1gMnPc7	představitel
jsou	být	k5eAaImIp3nP	být
někteří	některý	k3yIgMnPc1	některý
pěvci	pěvec	k1gMnPc1	pěvec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
slavík	slavík	k1gMnSc1	slavík
modráček	modráček	k1gMnSc1	modráček
<g/>
,	,	kIx,	,
žluva	žluva	k1gFnSc1	žluva
hajní	hajní	k2eAgFnSc1d1	hajní
<g/>
,	,	kIx,	,
křivka	křivka	k1gFnSc1	křivka
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
hýl	hýl	k1gMnSc1	hýl
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
kos	kos	k1gMnSc1	kos
černý	černý	k1gMnSc1	černý
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
kachen	kachna	k1gFnPc2	kachna
<g/>
,	,	kIx,	,
mnozí	mnohý	k2eAgMnPc1d1	mnohý
papoušci	papoušek	k1gMnPc1	papoušek
či	či	k8xC	či
někteří	některý	k3yIgMnPc1	některý
dravci	dravec	k1gMnPc1	dravec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
(	(	kIx(	(
<g/>
moták	moták	k1gMnSc1	moták
pochop	pochop	k1gMnSc1	pochop
či	či	k8xC	či
poštolka	poštolka	k1gFnSc1	poštolka
rudonohá	rudonohý	k2eAgFnSc1d1	rudonohá
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ptačí	ptačí	k2eAgMnPc1d1	ptačí
samci	samec	k1gMnPc1	samec
mají	mít	k5eAaImIp3nP	mít
pestřejší	pestrý	k2eAgNnPc4d2	pestřejší
zbarvení	zbarvení	k1gNnPc4	zbarvení
než	než	k8xS	než
samice	samice	k1gFnPc4	samice
<g/>
,	,	kIx,	,
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
pravidla	pravidlo	k1gNnSc2	pravidlo
však	však	k9	však
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
papoušek	papoušek	k1gMnSc1	papoušek
eklektus	eklektus	k1gInSc4	eklektus
různobarvý	různobarvý	k2eAgInSc4d1	různobarvý
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
samci	samec	k1gMnPc1	samec
jsou	být	k5eAaImIp3nP	být
zelení	zelení	k1gNnSc4	zelení
s	s	k7c7	s
červenými	červený	k2eAgNnPc7d1	červené
křídly	křídlo	k1gNnPc7	křídlo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
samice	samice	k1gFnSc1	samice
višňově	višňově	k6eAd1	višňově
červené	červená	k1gFnSc2	červená
s	s	k7c7	s
modrými	modrý	k2eAgInPc7d1	modrý
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
proto	proto	k8xC	proto
divu	div	k1gInSc2	div
že	že	k8xS	že
původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
obě	dva	k4xCgNnPc4	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
popsána	popsán	k2eAgFnSc1d1	popsána
jako	jako	k8xC	jako
dva	dva	k4xCgInPc4	dva
samostatné	samostatný	k2eAgInPc4d1	samostatný
druhy	druh	k1gInPc4	druh
papoušků	papoušek	k1gMnPc2	papoušek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
polyandrii	polyandrie	k1gFnSc6	polyandrie
jsou	být	k5eAaImIp3nP	být
samice	samice	k1gFnPc1	samice
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
pestřeji	pestro	k6eAd2	pestro
zbarvené	zbarvený	k2eAgFnPc1d1	zbarvená
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
ozývají	ozývat	k5eAaImIp3nP	ozývat
výraznějším	výrazný	k2eAgInSc7d2	výraznější
hlasem	hlas	k1gInSc7	hlas
než	než	k8xS	než
samci	samec	k1gMnPc1	samec
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
zvláště	zvláště	k6eAd1	zvláště
lyskonozi	lyskonoh	k1gMnPc1	lyskonoh
<g/>
,	,	kIx,	,
perepelové	perepel	k1gMnPc1	perepel
<g/>
,	,	kIx,	,
kulík	kulík	k1gMnSc1	kulík
hnědý	hnědý	k2eAgMnSc1d1	hnědý
či	či	k8xC	či
slučice	slučice	k1gFnSc1	slučice
zlatá	zlatý	k2eAgFnSc1d1	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
rozdíů	rozdí	k1gMnPc2	rozdí
ve	v	k7c6	v
zbarvení	zbarvení	k1gNnSc6	zbarvení
a	a	k8xC	a
velikosti	velikost	k1gFnSc6	velikost
se	se	k3xPyFc4	se
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
objevují	objevovat	k5eAaImIp3nP	objevovat
mezi	mezi	k7c7	mezi
pohlavími	pohlaví	k1gNnPc7	pohlaví
i	i	k8xC	i
rozdíly	rozdíl	k1gInPc7	rozdíl
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
mívají	mívat	k5eAaImIp3nP	mívat
prodloužená	prodloužený	k2eAgNnPc4d1	prodloužené
ocaní	ocaní	k2eAgNnPc4d1	ocaní
či	či	k8xC	či
křídelní	křídelní	k2eAgNnPc4d1	křídelní
pera	pero	k1gNnPc4	pero
<g/>
,	,	kIx,	,
chocholky	chocholka	k1gFnPc4	chocholka
<g/>
,	,	kIx,	,
krční	krční	k2eAgInPc4d1	krční
límce	límec	k1gInPc4	límec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
kožní	kožní	k2eAgInPc1d1	kožní
výrůstky	výrůstek	k1gInPc1	výrůstek
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
krku	krk	k1gInSc6	krk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tetřívek	tetřívek	k1gMnSc1	tetřívek
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
krocan	krocan	k1gMnSc1	krocan
divoký	divoký	k2eAgMnSc1d1	divoký
a	a	k8xC	a
domácí	domácí	k2eAgMnSc1d1	domácí
<g/>
,	,	kIx,	,
pižmovka	pižmovka	k1gFnSc1	pižmovka
velká	velká	k1gFnSc1	velká
<g/>
,	,	kIx,	,
kondor	kondor	k1gMnSc1	kondor
andský	andský	k2eAgMnSc1d1	andský
a	a	k8xC	a
královský	královský	k2eAgMnSc1d1	královský
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
pštros	pštros	k1gMnSc1	pštros
dvouprstý	dvouprstý	k2eAgMnSc1d1	dvouprstý
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
hrabavých	hrabavý	k2eAgInPc2d1	hrabavý
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnSc2	některý
kachny	kachna	k1gFnSc2	kachna
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kachnice	kachnice	k1gFnSc1	kachnice
laločnatá	laločnatý	k2eAgFnSc1d1	laločnatá
<g/>
,	,	kIx,	,
kajka	kajka	k1gFnSc1	kajka
královská	královský	k2eAgFnSc1d1	královská
<g/>
,	,	kIx,	,
ostralka	ostralka	k1gFnSc1	ostralka
štíhlá	štíhlý	k2eAgFnSc1d1	štíhlá
<g/>
,	,	kIx,	,
kachnička	kachnička	k1gFnSc1	kachnička
mandarinská	mandarinský	k2eAgFnSc1d1	mandarinská
a	a	k8xC	a
karolínská	karolínský	k2eAgFnSc1d1	karolínská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
lelků	lelek	k1gMnPc2	lelek
či	či	k8xC	či
pěvci	pěvec	k1gMnPc1	pěvec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
rajky	rajka	k1gFnPc1	rajka
<g/>
,	,	kIx,	,
vdovky	vdovka	k1gFnPc1	vdovka
či	či	k8xC	či
někteří	některý	k3yIgMnPc1	některý
snovači	snovač	k1gMnPc1	snovač
<g/>
.	.	kIx.	.
</s>
<s>
Naprostou	naprostý	k2eAgFnSc7d1	naprostá
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
rozdílný	rozdílný	k2eAgInSc1d1	rozdílný
tvar	tvar	k1gInSc1	tvar
zobáku	zobák	k1gInSc2	zobák
u	u	k7c2	u
obou	dva	k4xCgNnPc2	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
novozélandského	novozélandský	k2eAgMnSc2d1	novozélandský
laločníka	laločník	k1gMnSc2	laločník
ostrozobého	ostrozobý	k2eAgMnSc2d1	ostrozobý
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
však	však	k9	však
již	již	k6eAd1	již
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyhynul	vyhynout	k5eAaPmAgMnS	vyhynout
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
plazů	plaz	k1gMnPc2	plaz
bývají	bývat	k5eAaImIp3nP	bývat
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
pohlavími	pohlaví	k1gNnPc7	pohlaví
méně	málo	k6eAd2	málo
výrazné	výrazný	k2eAgNnSc1d1	výrazné
<g/>
;	;	kIx,	;
zejména	zejména	k9	zejména
u	u	k7c2	u
hadů	had	k1gMnPc2	had
<g/>
,	,	kIx,	,
hatérií	hatérie	k1gFnPc2	hatérie
a	a	k8xC	a
krokodýlů	krokodýl	k1gMnPc2	krokodýl
lze	lze	k6eAd1	lze
pohlaví	pohlaví	k1gNnSc4	pohlaví
rozlišit	rozlišit	k5eAaPmF	rozlišit
často	často	k6eAd1	často
jen	jen	k9	jen
podle	podle	k7c2	podle
stavby	stavba	k1gFnSc2	stavba
rozmnožovacích	rozmnožovací	k2eAgInPc2d1	rozmnožovací
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
mnoha	mnoho	k4c3	mnoho
druhů	druh	k1gInPc2	druh
ještěrů	ještěr	k1gMnPc2	ještěr
mají	mít	k5eAaImIp3nP	mít
samci	samec	k1gMnSc3	samec
výraznější	výrazný	k2eAgNnSc1d2	výraznější
zbarvení	zbarvení	k1gNnSc1	zbarvení
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ještěrka	ještěrka	k1gFnSc1	ještěrka
zelená	zelená	k1gFnSc1	zelená
<g/>
,	,	kIx,	,
leguánec	leguánec	k1gMnSc1	leguánec
obojkový	obojkový	k2eAgMnSc1d1	obojkový
<g/>
,	,	kIx,	,
mnozí	mnohý	k2eAgMnPc1d1	mnohý
chameleoni	chameleon	k1gMnPc1	chameleon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
leguánů	leguán	k1gMnPc2	leguán
<g/>
,	,	kIx,	,
chameleonů	chameleon	k1gMnPc2	chameleon
a	a	k8xC	a
agam	agam	k6eAd1	agam
mají	mít	k5eAaImIp3nP	mít
samci	samec	k1gMnPc1	samec
rovněž	rovněž	k9	rovněž
výrazněji	výrazně	k6eAd2	výrazně
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
kožní	kožní	k2eAgInPc1d1	kožní
lemy	lem	k1gInPc1	lem
<g/>
,	,	kIx,	,
hřebeny	hřeben	k1gInPc1	hřeben
<g/>
,	,	kIx,	,
vaky	vak	k1gInPc1	vak
<g/>
,	,	kIx,	,
přílby	přílba	k1gFnPc1	přílba
či	či	k8xC	či
rohy	roh	k1gInPc1	roh
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
,	,	kIx,	,
hrdle	hrdla	k1gFnSc6	hrdla
či	či	k8xC	či
hřbetě	hřbet	k1gInSc6	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
zástupce	zástupce	k1gMnPc4	zástupce
patří	patřit	k5eAaImIp3nS	patřit
chameleon	chameleon	k1gMnSc1	chameleon
Jacksonův	Jacksonův	k2eAgMnSc1d1	Jacksonův
<g/>
,	,	kIx,	,
bazilišci	bazilišek	k1gMnPc1	bazilišek
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
anolisové	anolisový	k2eAgInPc4d1	anolisový
či	či	k8xC	či
agamy	agama	k1gFnPc1	agama
rodu	rod	k1gInSc2	rod
Ceratophora	Ceratophor	k1gMnSc2	Ceratophor
ze	z	k7c2	z
Srí	Srí	k1gFnSc2	Srí
Lanky	lanko	k1gNnPc7	lanko
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
želv	želva	k1gFnPc2	želva
se	se	k3xPyFc4	se
pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
projevuje	projevovat	k5eAaImIp3nS	projevovat
především	především	k9	především
prodlouženými	prodloužený	k2eAgInPc7d1	prodloužený
drápy	dráp	k1gInPc7	dráp
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
končetinách	končetina	k1gFnPc6	končetina
samců	samec	k1gMnPc2	samec
želvy	želva	k1gFnSc2	želva
nádherné	nádherný	k2eAgFnSc2d1	nádherná
a	a	k8xC	a
jejích	její	k3xOp3gFnPc2	její
příbuzných	příbuzná	k1gFnPc2	příbuzná
z	z	k7c2	z
rodů	rod	k1gInPc2	rod
Trachemys	Trachemysa	k1gFnPc2	Trachemysa
<g/>
,	,	kIx,	,
Pseudemys	Pseudemysa	k1gFnPc2	Pseudemysa
<g/>
,	,	kIx,	,
Chrysemys	Chrysemysa	k1gFnPc2	Chrysemysa
a	a	k8xC	a
Graptemys	Graptemysa	k1gFnPc2	Graptemysa
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
ještěrů	ještěr	k1gMnPc2	ještěr
a	a	k8xC	a
krokodýlů	krokodýl	k1gMnPc2	krokodýl
bývají	bývat	k5eAaImIp3nP	bývat
většinou	většina	k1gFnSc7	většina
samci	samec	k1gMnPc1	samec
větší	veliký	k2eAgFnSc2d2	veliký
než	než	k8xS	než
samice	samice	k1gFnSc2	samice
<g/>
,	,	kIx,	,
u	u	k7c2	u
hadů	had	k1gMnPc2	had
a	a	k8xC	a
želv	želva	k1gFnPc2	želva
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
často	často	k6eAd1	často
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obojživelníků	obojživelník	k1gMnPc2	obojživelník
mají	mít	k5eAaImIp3nP	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
dimorfismus	dimorfismus	k1gInSc4	dimorfismus
zejména	zejména	k9	zejména
čolci	čolek	k1gMnPc1	čolek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
samečci	sameček	k1gMnPc1	sameček
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
období	období	k1gNnSc6	období
páření	páření	k1gNnSc2	páření
pestře	pestro	k6eAd1	pestro
zbarvení	zbarvený	k2eAgMnPc1d1	zbarvený
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
výrazné	výrazný	k2eAgInPc4d1	výrazný
kožní	kožní	k2eAgInPc4d1	kožní
lemy	lem	k1gInPc4	lem
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
a	a	k8xC	a
ocasu	ocas	k1gInSc6	ocas
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
žab	žába	k1gFnPc2	žába
a	a	k8xC	a
červorů	červor	k1gInPc2	červor
je	být	k5eAaImIp3nS	být
rozlišení	rozlišení	k1gNnSc1	rozlišení
pohlaví	pohlaví	k1gNnSc2	pohlaví
obvykle	obvykle	k6eAd1	obvykle
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
však	však	k9	však
bývají	bývat	k5eAaImIp3nP	bývat
většinou	většinou	k6eAd1	většinou
podstatně	podstatně	k6eAd1	podstatně
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
hrabatka	hrabatka	k1gFnSc1	hrabatka
drsná	drsný	k2eAgFnSc1d1	drsná
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInSc4d2	veliký
samec	samec	k1gInSc4	samec
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
je	být	k5eAaImIp3nS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
rovněž	rovněž	k9	rovněž
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
zbarvením	zbarvení	k1gNnSc7	zbarvení
<g/>
,	,	kIx,	,
tvarem	tvar	k1gInSc7	tvar
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
ploutví	ploutev	k1gFnPc2	ploutev
<g/>
,	,	kIx,	,
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
různými	různý	k2eAgInPc7d1	různý
výrůstky	výrůstek	k1gInPc7	výrůstek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
velikostí	velikost	k1gFnSc7	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
známým	známý	k2eAgInSc7d1	známý
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
lipan	lipan	k1gMnSc1	lipan
podhorní	podhorní	k2eAgMnSc1d1	podhorní
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc2	jehož
samci	samec	k1gMnPc1	samec
mají	mít	k5eAaImIp3nP	mít
zvětšenou	zvětšený	k2eAgFnSc4d1	zvětšená
a	a	k8xC	a
pestře	pestro	k6eAd1	pestro
zbarvenou	zbarvený	k2eAgFnSc4d1	zbarvená
hřbetní	hřbetní	k2eAgFnSc4d1	hřbetní
ploutev	ploutev	k1gFnSc4	ploutev
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
živorodka	živorodka	k1gFnSc1	živorodka
duhová	duhový	k2eAgFnSc1d1	Duhová
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tutéž	týž	k3xTgFnSc4	týž
funkci	funkce	k1gFnSc4	funkce
plní	plnit	k5eAaImIp3nS	plnit
ploutev	ploutev	k1gFnSc1	ploutev
ocasní	ocasní	k2eAgFnSc1d1	ocasní
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
kožních	kožní	k2eAgInPc2d1	kožní
výrůstků	výrůstek	k1gInPc2	výrůstek
jsou	být	k5eAaImIp3nP	být
mečovky	mečovka	k1gFnPc1	mečovka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
samci	samec	k1gMnPc1	samec
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
ocasní	ocasní	k2eAgFnSc2d1	ocasní
ploutve	ploutev	k1gFnSc2	ploutev
mečovitý	mečovitý	k2eAgInSc1d1	mečovitý
výběžek	výběžek	k1gInSc1	výběžek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
někteří	některý	k3yIgMnPc1	některý
krunýřovci	krunýřovec	k1gMnPc1	krunýřovec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
objevují	objevovat	k5eAaImIp3nP	objevovat
trny	trn	k1gInPc1	trn
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílné	rozdílný	k2eAgNnSc4d1	rozdílné
zbarvení	zbarvení	k1gNnSc4	zbarvení
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
tření	tření	k1gNnSc2	tření
např.	např.	kA	např.
střevle	střevle	k1gFnSc2	střevle
potoční	potoční	k2eAgFnSc2d1	potoční
<g/>
,	,	kIx,	,
hranáč	hranáč	k1gInSc1	hranáč
šedý	šedý	k2eAgInSc1d1	šedý
nebo	nebo	k8xC	nebo
z	z	k7c2	z
akvarijních	akvarijní	k2eAgFnPc2d1	akvarijní
ryb	ryba	k1gFnPc2	ryba
mnozí	mnohý	k2eAgMnPc1d1	mnohý
čichavci	čichavec	k1gMnPc1	čichavec
a	a	k8xC	a
rájovci	rájovec	k1gMnPc1	rájovec
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
živorodek	živorodka	k1gFnPc2	živorodka
i	i	k8xC	i
některých	některý	k3yIgInPc2	některý
dalších	další	k2eAgInPc2d1	další
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
s	s	k7c7	s
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
oplozením	oplození	k1gNnSc7	oplození
mají	mít	k5eAaImIp3nP	mít
dobře	dobře	k6eAd1	dobře
viditelný	viditelný	k2eAgInSc4d1	viditelný
pářící	pářící	k2eAgInSc4d1	pářící
orgán	orgán	k1gInSc4	orgán
gonopodium	gonopodium	k1gNnSc1	gonopodium
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
samice	samice	k1gFnSc1	samice
např.	např.	kA	např.
hořavky	hořavka	k1gFnPc4	hořavka
duhové	duhový	k2eAgFnPc4d1	Duhová
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
vyvinuto	vyvinut	k2eAgNnSc4d1	vyvinuto
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
kladélko	kladélko	k1gNnSc4	kladélko
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
jihoamerická	jihoamerický	k2eAgFnSc1d1	jihoamerická
tetra	tetra	k1gFnSc1	tetra
Weitzmannova	Weitzmannov	k1gInSc2	Weitzmannov
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
mají	mít	k5eAaImIp3nP	mít
samci	samec	k1gMnPc1	samec
a	a	k8xC	a
samičky	samička	k1gFnPc1	samička
zcela	zcela	k6eAd1	zcela
rozdílný	rozdílný	k2eAgInSc4d1	rozdílný
počet	počet	k1gInSc4	počet
<g/>
,	,	kIx,	,
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
typ	typ	k1gInSc4	typ
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
čtyř	čtyři	k4xCgFnPc2	čtyři
čeledí	čeleď	k1gFnPc2	čeleď
hlubinných	hlubinný	k2eAgFnPc2d1	hlubinná
ryb	ryba	k1gFnPc2	ryba
<g/>
:	:	kIx,	:
Ceratiidae	Ceratiidae	k1gNnSc2	Ceratiidae
<g/>
,	,	kIx,	,
Neoceratiidae	Neoceratiidae	k1gNnSc2	Neoceratiidae
<g/>
,	,	kIx,	,
Linophrynidae	Linophrynidae	k1gNnSc2	Linophrynidae
a	a	k8xC	a
Caulophrynidae	Caulophrynidae	k1gNnSc2	Caulophrynidae
se	se	k3xPyFc4	se
drobní	drobný	k2eAgMnPc1d1	drobný
samci	samec	k1gMnPc1	samec
přisávají	přisávat	k5eAaImIp3nP	přisávat
na	na	k7c4	na
tělo	tělo	k1gNnSc4	tělo
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnPc1d2	veliký
samice	samice	k1gFnPc1	samice
<g/>
,	,	kIx,	,
srůstají	srůstat	k5eAaImIp3nP	srůstat
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
degenerují	degenerovat	k5eAaBmIp3nP	degenerovat
jen	jen	k9	jen
na	na	k7c4	na
jakýsi	jakýsi	k3yIgInSc4	jakýsi
přívěsek	přívěsek	k1gInSc4	přívěsek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
produkce	produkce	k1gFnSc1	produkce
spermií	spermie	k1gFnPc2	spermie
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
dimorfismu	dimorfismus	k1gInSc2	dimorfismus
najdeme	najít	k5eAaPmIp1nP	najít
u	u	k7c2	u
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
členovců	členovec	k1gMnPc2	členovec
<g/>
.	.	kIx.	.
</s>
<s>
Škála	škála	k1gFnSc1	škála
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
nesmírně	smírně	k6eNd1	smírně
široká	široký	k2eAgFnSc1d1	široká
<g/>
,	,	kIx,	,
od	od	k7c2	od
pouhých	pouhý	k2eAgInPc2d1	pouhý
rozdílů	rozdíl	k1gInPc2	rozdíl
ve	v	k7c6	v
zbarvení	zbarvení	k1gNnSc6	zbarvení
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
vážek	vážka	k1gFnPc2	vážka
či	či	k8xC	či
motýlů	motýl	k1gMnPc2	motýl
až	až	k9	až
po	po	k7c4	po
zcela	zcela	k6eAd1	zcela
odlišný	odlišný	k2eAgInSc4d1	odlišný
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc4	velikost
i	i	k8xC	i
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
stavbu	stavba	k1gFnSc4	stavba
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
řásnokřídlých	řásnokřídlý	k2eAgMnPc2d1	řásnokřídlý
<g/>
,	,	kIx,	,
některých	některý	k3yIgMnPc2	některý
červců	červec	k1gMnPc2	červec
či	či	k8xC	či
motýli	motýl	k1gMnPc1	motýl
vakonošů	vakonoš	k1gMnPc2	vakonoš
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
členovců	členovec	k1gMnPc2	členovec
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
větší	veliký	k2eAgMnPc1d2	veliký
než	než	k8xS	než
samci	samec	k1gMnPc1	samec
<g/>
,	,	kIx,	,
u	u	k7c2	u
masožravých	masožravý	k2eAgInPc2d1	masožravý
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
kudlanky	kudlanka	k1gFnPc1	kudlanka
<g/>
,	,	kIx,	,
sklípkani	sklípkan	k1gMnPc1	sklípkan
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
pavouci	pavouk	k1gMnPc1	pavouk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgInSc7d1	častý
jevem	jev	k1gInSc7	jev
kanibalismus	kanibalismus	k1gInSc1	kanibalismus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
samec	samec	k1gMnSc1	samec
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c6	o
kopulaci	kopulace	k1gFnSc6	kopulace
může	moct	k5eAaImIp3nS	moct
padnout	padnout	k5eAaPmF	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc4d2	veliký
samici	samice	k1gFnSc4	samice
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
mnoha	mnoho	k4c3	mnoho
druhů	druh	k1gInPc2	druh
brouků	brouk	k1gMnPc2	brouk
<g/>
,	,	kIx,	,
k	k	k7c3	k
např.	např.	kA	např.
roháčů	roháč	k1gMnPc2	roháč
<g/>
,	,	kIx,	,
nosorožíků	nosorožík	k1gMnPc2	nosorožík
či	či	k8xC	či
některých	některý	k3yIgMnPc2	některý
krabů	krab	k1gMnPc2	krab
(	(	kIx(	(
<g/>
např.	např.	kA	např.
krabovec	krabovec	k1gMnSc1	krabovec
kamčatský	kamčatský	k2eAgMnSc1d1	kamčatský
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgMnPc1d2	veliký
samci	samec	k1gMnPc1	samec
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
bezobratlých	bezobratlí	k1gMnPc2	bezobratlí
poznáme	poznat	k5eAaPmIp1nP	poznat
podle	podle	k7c2	podle
vyvinutého	vyvinutý	k2eAgNnSc2d1	vyvinuté
kladélka	kladélko	k1gNnSc2	kladélko
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
to	ten	k3xDgNnSc1	ten
např.	např.	kA	např.
o	o	k7c6	o
lumcích	lumek	k1gMnPc6	lumek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
mnoha	mnoho	k4c6	mnoho
druzích	druh	k1gInPc6	druh
rovnokřídlých	rovnokřídlí	k1gMnPc2	rovnokřídlí
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
kobylek	kobylka	k1gFnPc2	kobylka
a	a	k8xC	a
cvrčků	cvrček	k1gMnPc2	cvrček
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
dvoukřídlých	dvoukřídlí	k1gMnPc2	dvoukřídlí
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
komárů	komár	k1gMnPc2	komár
a	a	k8xC	a
ovádů	ovád	k1gMnPc2	ovád
se	se	k3xPyFc4	se
pohlaví	pohlaví	k1gNnSc3	pohlaví
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
stavbou	stavba	k1gFnSc7	stavba
ústního	ústní	k2eAgNnSc2d1	ústní
ústrojí	ústrojí	k1gNnSc2	ústrojí
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
způsobem	způsob	k1gInSc7	způsob
přijímání	přijímání	k1gNnSc2	přijímání
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Extrémními	extrémní	k2eAgInPc7d1	extrémní
příklady	příklad	k1gInPc7	příklad
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
dimorfismu	dimorfismus	k1gInSc2	dimorfismus
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
termiti	termit	k1gMnPc1	termit
<g/>
,	,	kIx,	,
řásnokřídlí	řásnokřídlý	k2eAgMnPc1d1	řásnokřídlý
<g/>
,	,	kIx,	,
motýli	motýl	k1gMnPc1	motýl
vakonoši	vakonoš	k1gMnPc1	vakonoš
nebo	nebo	k8xC	nebo
někteří	některý	k3yIgMnPc1	některý
červci	červec	k1gMnPc1	červec
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
samičky	samička	k1gFnPc1	samička
se	s	k7c7	s
zbytnělým	zbytnělý	k2eAgInSc7d1	zbytnělý
zadečkem	zadeček	k1gInSc7	zadeček
a	a	k8xC	a
zakrnělými	zakrnělý	k2eAgInPc7d1	zakrnělý
smyslovými	smyslový	k2eAgInPc7d1	smyslový
orgány	orgán	k1gInPc7	orgán
<g/>
,	,	kIx,	,
křídly	křídlo	k1gNnPc7	křídlo
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
končetinami	končetina	k1gFnPc7	končetina
<g/>
,	,	kIx,	,
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
příliš	příliš	k6eAd1	příliš
hmyz	hmyz	k1gInSc4	hmyz
nepřipomínají	připomínat	k5eNaImIp3nP	připomínat
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
přizpůsobeny	přizpůsobit	k5eAaPmNgInP	přizpůsobit
ke	k	k7c3	k
kladení	kladení	k1gNnSc3	kladení
co	co	k9	co
největšího	veliký	k2eAgInSc2d3	veliký
počtu	počet	k1gInSc2	počet
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Extrémní	extrémní	k2eAgInPc1d1	extrémní
případy	případ	k1gInPc1	případ
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
dimorfismu	dimorfismus	k1gInSc2	dimorfismus
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgFnPc1	ten
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
partneři	partner	k1gMnPc1	partner
srůstají	srůstat	k5eAaImIp3nP	srůstat
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
degeneruje	degenerovat	k5eAaBmIp3nS	degenerovat
pouze	pouze	k6eAd1	pouze
v	v	k7c4	v
přívěsek	přívěsek	k1gInSc4	přívěsek
toho	ten	k3xDgNnSc2	ten
druhého	druhý	k4xOgNnSc2	druhý
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
hlubinná	hlubinný	k2eAgFnSc1d1	hlubinná
ryba	ryba	k1gFnSc1	ryba
Cryptopsaras	Cryptopsaras	k1gInSc1	Cryptopsaras
couesii	couesie	k1gFnSc4	couesie
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
ďasů	ďas	k1gMnPc2	ďas
či	či	k8xC	či
měchovec	měchovec	k1gMnSc1	měchovec
srostlice	srostlice	k1gFnSc2	srostlice
trvalá	trvalá	k1gFnSc1	trvalá
(	(	kIx(	(
<g/>
Syngamus	Syngamus	k1gInSc1	Syngamus
trachea	trachea	k1gFnSc1	trachea
<g/>
)	)	kIx)	)
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
hlístic	hlístice	k1gFnPc2	hlístice
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
extrémem	extrém	k1gInSc7	extrém
jsou	být	k5eAaImIp3nP	být
kostižerky	kostižerka	k1gFnPc1	kostižerka
(	(	kIx(	(
<g/>
rod	rod	k1gInSc1	rod
Osedax	Osedax	k1gInSc1	Osedax
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
mořský	mořský	k2eAgMnSc1d1	mořský
bezobratlý	bezobratlý	k2eAgMnSc1d1	bezobratlý
živočich	živočich	k1gMnSc1	živočich
rypohlavec	rypohlavec	k1gMnSc1	rypohlavec
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
obou	dva	k4xCgMnPc2	dva
měří	měřit	k5eAaImIp3nS	měřit
samci	samec	k1gMnSc3	samec
jen	jen	k9	jen
několik	několik	k4yIc4	několik
mm	mm	kA	mm
a	a	k8xC	a
žijí	žít	k5eAaImIp3nP	žít
uvnitř	uvnitř	k7c2	uvnitř
těla	tělo	k1gNnSc2	tělo
o	o	k7c4	o
několik	několik	k4yIc4	několik
řádů	řád	k1gInPc2	řád
větší	veliký	k2eAgFnSc1d2	veliký
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
pohlavími	pohlaví	k1gNnPc7	pohlaví
živočichů	živočich	k1gMnPc2	živočich
jen	jen	k9	jen
v	v	k7c6	v
barevnosti	barevnost	k1gFnSc6	barevnost
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
jeho	on	k3xPp3gInSc2	on
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
dichroismus	dichroismus	k1gInSc4	dichroismus
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pěvců	pěvec	k1gMnPc2	pěvec
či	či	k8xC	či
papoušků	papoušek	k1gMnPc2	papoušek
(	(	kIx(	(
<g/>
eklektus	eklektus	k1gMnSc1	eklektus
různobarvý	různobarvý	k2eAgMnSc1d1	různobarvý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
u	u	k7c2	u
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vážek	vážka	k1gFnPc2	vážka
<g/>
,	,	kIx,	,
motýlů	motýl	k1gMnPc2	motýl
nebo	nebo	k8xC	nebo
brouků	brouk	k1gMnPc2	brouk
<g/>
.	.	kIx.	.
</s>
