<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
pohlavími	pohlaví	k1gNnPc7	pohlaví
živočichů	živočich	k1gMnPc2	živočich
jen	jen	k9	jen
v	v	k7c6	v
barevnosti	barevnost	k1gFnSc6	barevnost
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
jeho	jeho	k3xOp3gInSc2	jeho
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
dichroismus	dichroismus	k1gInSc4	dichroismus
<g/>
.	.	kIx.	.
</s>
