<p>
<s>
Joachim	Joachim	k1gMnSc1	Joachim
Gauck	Gauck	k1gMnSc1	Gauck
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1940	[number]	k4	1940
Rostock	Rostock	k1gInSc1	Rostock
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
německý	německý	k2eAgMnSc1d1	německý
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
letech	let	k1gInPc6	let
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
zastával	zastávat	k5eAaImAgMnS	zastávat
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
bývalý	bývalý	k2eAgMnSc1d1	bývalý
evangelicko-luterský	evangelickouterský	k2eAgMnSc1d1	evangelicko-luterský
pastor	pastor	k1gMnSc1	pastor
a	a	k8xC	a
kazatel	kazatel	k1gMnSc1	kazatel
proslul	proslout	k5eAaPmAgMnS	proslout
především	především	k9	především
svým	svůj	k3xOyFgInSc7	svůj
bojem	boj	k1gInSc7	boj
za	za	k7c4	za
občanská	občanský	k2eAgNnPc4d1	občanské
a	a	k8xC	a
základní	základní	k2eAgNnPc4d1	základní
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
Německé	německý	k2eAgFnSc2d1	německá
demokratické	demokratický	k2eAgFnSc2d1	demokratická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
pastor	pastor	k1gMnSc1	pastor
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
evangelických	evangelický	k2eAgInPc6d1	evangelický
sborech	sbor	k1gInPc6	sbor
v	v	k7c6	v
městě	město	k1gNnSc6	město
Lüssow	Lüssow	k1gFnSc2	Lüssow
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
a	a	k8xC	a
následně	následně	k6eAd1	následně
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
Rostocku	Rostock	k1gInSc6	Rostock
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
žádost	žádost	k1gFnSc4	žádost
z	z	k7c2	z
úřadu	úřad	k1gInSc2	úřad
pastora	pastor	k1gMnSc2	pastor
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
veřejného	veřejný	k2eAgInSc2d1	veřejný
činitele	činitel	k1gInSc2	činitel
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
znovusjednocení	znovusjednocení	k1gNnSc6	znovusjednocení
Německa	Německo	k1gNnSc2	Německo
působil	působit	k5eAaImAgInS	působit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
až	až	k9	až
2000	[number]	k4	2000
jako	jako	k8xC	jako
ředitel	ředitel	k1gMnSc1	ředitel
Úřadu	úřad	k1gInSc2	úřad
spolkového	spolkový	k2eAgMnSc2d1	spolkový
zmocněnce	zmocněnec	k1gMnSc2	zmocněnec
pro	pro	k7c4	pro
dokumenty	dokument	k1gInPc4	dokument
dřívější	dřívější	k2eAgFnSc2d1	dřívější
tajné	tajný	k2eAgFnSc2d1	tajná
policie	policie	k1gFnSc2	policie
(	(	kIx(	(
<g/>
Stasi	stase	k1gFnSc4	stase
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	on	k3xPp3gNnSc2	on
působení	působení	k1gNnSc2	působení
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
úřadě	úřad	k1gInSc6	úřad
širokou	široký	k2eAgFnSc7d1	široká
veřejností	veřejnost	k1gFnSc7	veřejnost
zvaného	zvaný	k2eAgMnSc2d1	zvaný
"	"	kIx"	"
<g/>
Gauckův	Gauckův	k2eAgInSc1d1	Gauckův
úřad	úřad	k1gInSc1	úřad
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Gauck-Behörde	Gauck-Behörd	k1gMnSc5	Gauck-Behörd
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
německého	německý	k2eAgMnSc2d1	německý
spolkového	spolkový	k2eAgMnSc2d1	spolkový
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přes	přes	k7c4	přes
širokou	široký	k2eAgFnSc4d1	široká
podporu	podpora	k1gFnSc4	podpora
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
veřejnosti	veřejnost	k1gFnSc2	veřejnost
a	a	k8xC	a
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
SPD	SPD	kA	SPD
a	a	k8xC	a
Svazu	svaz	k1gInSc2	svaz
90	[number]	k4	90
<g/>
/	/	kIx~	/
<g/>
Zelených	Zelených	k2eAgMnPc2d1	Zelených
<g/>
)	)	kIx)	)
jej	on	k3xPp3gMnSc4	on
nevelkým	velký	k2eNgInSc7d1	nevelký
rozdílem	rozdíl	k1gInSc7	rozdíl
porazil	porazit	k5eAaPmAgMnS	porazit
Christian	Christian	k1gMnSc1	Christian
Wulff	Wulff	k1gMnSc1	Wulff
<g/>
,	,	kIx,	,
kandidát	kandidát	k1gMnSc1	kandidát
vládní	vládní	k2eAgFnSc2d1	vládní
koalice	koalice	k1gFnSc2	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Wulffově	Wulffův	k2eAgFnSc6d1	Wulffův
rezignaci	rezignace	k1gFnSc6	rezignace
na	na	k7c4	na
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
úřad	úřad	k1gInSc4	úřad
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2012	[number]	k4	2012
se	se	k3xPyFc4	se
německá	německý	k2eAgFnSc1d1	německá
vláda	vláda	k1gFnSc1	vláda
shodla	shodnout	k5eAaBmAgFnS	shodnout
s	s	k7c7	s
opozicí	opozice	k1gFnSc7	opozice
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
bude	být	k5eAaImBp3nS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
právě	právě	k9	právě
Joachim	Joachi	k1gNnSc7	Joachi
Gauck	Gaucka	k1gFnPc2	Gaucka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spolkový	spolkový	k2eAgMnSc1d1	spolkový
prezident	prezident	k1gMnSc1	prezident
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
spolkového	spolkový	k2eAgMnSc2d1	spolkový
prezidenta	prezident	k1gMnSc2	prezident
byl	být	k5eAaImAgMnS	být
Gauck	Gauck	k1gMnSc1	Gauck
zvolen	zvolen	k2eAgMnSc1d1	zvolen
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
bývalé	bývalý	k2eAgFnSc2d1	bývalá
NDR	NDR	kA	NDR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
německý	německý	k2eAgMnSc1d1	německý
spolkový	spolkový	k2eAgMnSc1d1	spolkový
prezident	prezident	k1gMnSc1	prezident
navštívil	navštívit	k5eAaPmAgMnS	navštívit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
své	svůj	k3xOyFgFnSc2	svůj
oficiální	oficiální	k2eAgFnSc2d1	oficiální
návštěvy	návštěva	k1gFnSc2	návštěva
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
památník	památník	k1gInSc1	památník
v	v	k7c6	v
Lidicích	Lidice	k1gInPc6	Lidice
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pamětníky	pamětník	k1gMnPc4	pamětník
vyhlazení	vyhlazení	k1gNnSc1	vyhlazení
Lidic	Lidice	k1gInPc2	Lidice
i	i	k8xC	i
českými	český	k2eAgMnPc7d1	český
politiky	politik	k1gMnPc7	politik
a	a	k8xC	a
veřejností	veřejnost	k1gFnPc2	veřejnost
vnímáno	vnímat	k5eAaImNgNnS	vnímat
jako	jako	k8xS	jako
vstřícné	vstřícný	k2eAgNnSc1d1	vstřícné
gesto	gesto	k1gNnSc1	gesto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2014	[number]	k4	2014
vykonal	vykonat	k5eAaPmAgInS	vykonat
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
prezidenta	prezident	k1gMnSc2	prezident
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
první	první	k4xOgFnSc4	první
státní	státní	k2eAgFnSc4d1	státní
návštěvu	návštěva	k1gFnSc4	návštěva
německého	německý	k2eAgMnSc2d1	německý
spolkového	spolkový	k2eAgMnSc2d1	spolkový
prezidenta	prezident	k1gMnSc2	prezident
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
od	od	k7c2	od
jejího	její	k3xOp3gInSc2	její
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2016	[number]	k4	2016
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Gauck	Gauck	k1gMnSc1	Gauck
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
úředním	úřední	k2eAgNnSc6d1	úřední
sídle	sídlo	k1gNnSc6	sídlo
<g/>
,	,	kIx,	,
zámku	zámek	k1gInSc6	zámek
Bellevue	bellevue	k1gFnSc2	bellevue
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
spolkového	spolkový	k2eAgMnSc2d1	spolkový
prezidenta	prezident	k1gMnSc2	prezident
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
ve	v	k7c6	v
Spolkovém	spolkový	k2eAgNnSc6d1	Spolkové
shromáždění	shromáždění	k1gNnSc6	shromáždění
(	(	kIx(	(
<g/>
Bundesversammlung	Bundesversammlung	k1gMnSc1	Bundesversammlung
<g/>
)	)	kIx)	)
nebude	být	k5eNaImBp3nS	být
znova	znova	k6eAd1	znova
kandidovat	kandidovat	k5eAaImF	kandidovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Soukromý	soukromý	k2eAgInSc4d1	soukromý
život	život	k1gInSc4	život
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Gerhildou	Gerhilda	k1gFnSc7	Gerhilda
Radtkeovou	Radtkeův	k2eAgFnSc7d1	Radtkeův
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
žijí	žít	k5eAaImIp3nP	žít
manželé	manžel	k1gMnPc1	manžel
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc7	jeho
partnerkou	partnerka	k1gFnSc7	partnerka
německá	německý	k2eAgFnSc1d1	německá
novinářka	novinářka	k1gFnSc1	novinářka
Daniela	Daniela	k1gFnSc1	Daniela
Schadtová	Schadtová	k1gFnSc1	Schadtová
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Joachim	Joachim	k1gMnSc1	Joachim
Gauck	Gauck	k1gMnSc1	Gauck
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Joachim	Joachima	k1gFnPc2	Joachima
Gauck	Gaucka	k1gFnPc2	Gaucka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Joachim	Joachima	k1gFnPc2	Joachima
Gauck	Gaucko	k1gNnPc2	Gaucko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Joachim	Joachimo	k1gNnPc2	Joachimo
Gauck	Gaucko	k1gNnPc2	Gaucko
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
