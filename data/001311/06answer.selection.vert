<s>
Cín	cín	k1gInSc1	cín
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Sn	Sn	k1gFnSc2	Sn
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Stannum	Stannum	k1gInSc1	Stannum
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
kovy	kov	k1gInPc7	kov
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
lidstvu	lidstvo	k1gNnSc3	lidstvo
již	již	k9	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
především	především	k6eAd1	především
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
slitiny	slitina	k1gFnSc2	slitina
zvané	zvaný	k2eAgFnSc2d1	zvaná
bronz	bronz	k1gInSc4	bronz
<g/>
.	.	kIx.	.
</s>
