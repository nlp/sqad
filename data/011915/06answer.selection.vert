<s>
Absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
studium	studium	k1gNnSc4	studium
práv	právo	k1gNnPc2	právo
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
ve	v	k7c6	v
Štýrském	štýrský	k2eAgInSc6d1	štýrský
Hradci	Hradec	k1gInSc6	Hradec
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
státních	státní	k2eAgFnPc6d1	státní
službách	služba	k1gFnPc6	služba
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
ve	v	k7c6	v
Štýrském	štýrský	k2eAgInSc6d1	štýrský
Hradci	Hradec	k1gInSc6	Hradec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
zemského	zemský	k2eAgInSc2d1	zemský
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
