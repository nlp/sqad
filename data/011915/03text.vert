<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Nepomuk	Nepomuk	k1gMnSc1	Nepomuk
Gleispach	Gleispach	k1gMnSc1	Gleispach
<g/>
,	,	kIx,	,
též	též	k9	též
hrabě	hrabě	k1gMnSc1	hrabě
Johann	Johann	k1gMnSc1	Johann
Gleispach	Gleispach	k1gMnSc1	Gleispach
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1840	[number]	k4	1840
Gorizia	Gorizium	k1gNnSc2	Gorizium
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
únor	únor	k1gInSc4	únor
1906	[number]	k4	1906
Štýrský	štýrský	k2eAgInSc1d1	štýrský
Hradec	Hradec	k1gInSc1	Hradec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
štýrský	štýrský	k2eAgMnSc1d1	štýrský
a	a	k8xC	a
rakousko-uherský	rakouskoherský	k2eAgMnSc1d1	rakousko-uherský
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
předlitavský	předlitavský	k2eAgMnSc1d1	předlitavský
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ministr	ministr	k1gMnSc1	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
Předlitavska	Předlitavsko	k1gNnSc2	Předlitavsko
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Kazimíra	Kazimír	k1gMnSc2	Kazimír
Badeniho	Badeni	k1gMnSc2	Badeni
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
studium	studium	k1gNnSc4	studium
práv	právo	k1gNnPc2	právo
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
ve	v	k7c6	v
Štýrském	štýrský	k2eAgInSc6d1	štýrský
Hradci	Hradec	k1gInSc6	Hradec
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
státních	státní	k2eAgFnPc6d1	státní
službách	služba	k1gFnPc6	služba
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
ve	v	k7c6	v
Štýrském	štýrský	k2eAgInSc6d1	štýrský
Hradci	Hradec	k1gInSc6	Hradec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
zemského	zemský	k2eAgInSc2d1	zemský
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
pozice	pozice	k1gFnSc2	pozice
dvorního	dvorní	k2eAgMnSc2d1	dvorní
rady	rada	k1gMnSc2	rada
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
tajného	tajný	k2eAgMnSc2d1	tajný
rady	rada	k1gMnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zasedal	zasedat	k5eAaImAgInS	zasedat
v	v	k7c6	v
Štýrském	štýrský	k2eAgInSc6d1	štýrský
zemském	zemský	k2eAgInSc6d1	zemský
sněmu	sněm	k1gInSc6	sněm
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zasedal	zasedat	k5eAaImAgInS	zasedat
ve	v	k7c6	v
velkostatkářské	velkostatkářský	k2eAgFnSc6d1	velkostatkářská
kurii	kurie	k1gFnSc6	kurie
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
měl	mít	k5eAaImAgInS	mít
původem	původ	k1gInSc7	původ
blízko	blízko	k6eAd1	blízko
ke	k	k7c3	k
konzervativcům	konzervativec	k1gMnPc3	konzervativec
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
hlasováních	hlasování	k1gNnPc6	hlasování
setrvale	setrvale	k6eAd1	setrvale
podporoval	podporovat	k5eAaImAgMnS	podporovat
pozice	pozice	k1gFnPc4	pozice
německé	německý	k2eAgFnSc2d1	německá
liberální	liberální	k2eAgFnSc2d1	liberální
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Ústavní	ústavní	k2eAgFnSc1d1	ústavní
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
vláda	vláda	k1gFnSc1	vláda
Eduarda	Eduard	k1gMnSc2	Eduard
Taaffeho	Taaffe	k1gMnSc2	Taaffe
<g/>
,	,	kIx,	,
shledal	shledat	k5eAaPmAgInS	shledat
jako	jako	k9	jako
nemožné	možný	k2eNgNnSc1d1	nemožné
dále	daleko	k6eAd2	daleko
setrvávat	setrvávat	k5eAaImF	setrvávat
vůči	vůči	k7c3	vůči
ní	on	k3xPp3gFnSc3	on
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
jako	jako	k8xS	jako
němečtí	německý	k2eAgMnPc1d1	německý
liberálové	liberál	k1gMnPc1	liberál
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
na	na	k7c4	na
mandát	mandát	k1gInSc4	mandát
v	v	k7c6	v
zemském	zemský	k2eAgInSc6d1	zemský
sněmu	sněm	k1gInSc6	sněm
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
byl	být	k5eAaImAgInS	být
zároveň	zároveň	k6eAd1	zároveň
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
doživotním	doživotní	k2eAgInSc7d1	doživotní
členem	člen	k1gInSc7	člen
Panské	panský	k2eAgFnSc2d1	Panská
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
<g/>
Vrchol	vrchol	k1gInSc1	vrchol
jeho	jeho	k3xOp3gFnSc2	jeho
politické	politický	k2eAgFnSc2d1	politická
kariéry	kariéra	k1gFnSc2	kariéra
nastal	nastat	k5eAaPmAgInS	nastat
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Kazimíra	Kazimír	k1gMnSc2	Kazimír
Badeniho	Badeni	k1gMnSc2	Badeni
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ministrem	ministr	k1gMnSc7	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
Předlitavska	Předlitavsko	k1gNnSc2	Předlitavsko
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
zastával	zastávat	k5eAaImAgMnS	zastávat
v	v	k7c6	v
období	období	k1gNnSc6	období
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1895	[number]	k4	1895
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
ministra	ministr	k1gMnSc2	ministr
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
prosazení	prosazení	k1gNnSc4	prosazení
reformy	reforma	k1gFnSc2	reforma
občanského	občanský	k2eAgNnSc2d1	občanské
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Čelil	čelit	k5eAaImAgMnS	čelit
obstrukcím	obstrukce	k1gFnPc3	obstrukce
opozice	opozice	k1gFnSc2	opozice
proti	proti	k7c3	proti
Badeniho	Badeniha	k1gFnSc5	Badeniha
jazykovým	jazykový	k2eAgNnSc7d1	jazykové
nařízením	nařízení	k1gNnSc7	nařízení
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
na	na	k7c4	na
post	post	k1gInSc4	post
prezidenta	prezident	k1gMnSc2	prezident
zemského	zemský	k2eAgInSc2d1	zemský
soudu	soud	k1gInSc2	soud
ve	v	k7c6	v
Štýrském	štýrský	k2eAgInSc6d1	štýrský
Hradci	Hradec	k1gInSc6	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
synem	syn	k1gMnSc7	syn
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
a	a	k8xC	a
německý	německý	k2eAgMnSc1d1	německý
právník	právník	k1gMnSc1	právník
Wenzeslaus	Wenzeslaus	k1gMnSc1	Wenzeslaus
von	von	k1gInSc4	von
Gleispach	Gleispacha	k1gFnPc2	Gleispacha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
