<s>
Francis	Francis	k1gFnSc1	Francis
Bacon	Bacona	k1gFnPc2	Bacona
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vikomt	vikomt	k1gMnSc1	vikomt
St.	st.	kA	st.
Albans	Albans	k1gInSc1	Albans
<g/>
,	,	kIx,	,
baron	baron	k1gMnSc1	baron
z	z	k7c2	z
Verulamu	Verulam	k1gInSc2	Verulam
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
také	také	k9	také
František	František	k1gMnSc1	František
Baco	Baco	k1gMnSc1	Baco
z	z	k7c2	z
Verulamu	Verulam	k1gInSc2	Verulam
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1561	[number]	k4	1561
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1626	[number]	k4	1626
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
zakladatele	zakladatel	k1gMnSc4	zakladatel
empirismu	empirismus	k1gInSc2	empirismus
a	a	k8xC	a
tvůrce	tvůrce	k1gMnSc1	tvůrce
nové	nový	k2eAgFnSc2d1	nová
vědecké	vědecký	k2eAgFnSc2d1	vědecká
metody	metoda	k1gFnSc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
úřadu	úřad	k1gInSc2	úřad
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pozice	pozice	k1gFnSc1	pozice
Lorda	lord	k1gMnSc4	lord
kancléře	kancléř	k1gMnSc4	kancléř
<g/>
.	.	kIx.	.
</s>
<s>
Francis	Francis	k1gInSc1	Francis
Bacon	Bacon	k1gInSc1	Bacon
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
Sira	sir	k1gMnSc2	sir
Nicholase	Nicholasa	k1gFnSc6	Nicholasa
Bacona	Bacon	k1gMnSc2	Bacon
<g/>
,	,	kIx,	,
lorda	lord	k1gMnSc2	lord
strážce	strážce	k1gMnSc2	strážce
pečeti	pečeť	k1gFnSc2	pečeť
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
druhé	druhý	k4xOgFnPc1	druhý
manželky	manželka	k1gFnPc1	manželka
Anne	Ann	k1gFnSc2	Ann
Cook	Cooka	k1gFnPc2	Cooka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
nové	nový	k2eAgFnSc2d1	nová
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
své	svůj	k3xOyFgInPc4	svůj
majetky	majetek	k1gInPc4	majetek
po	po	k7c6	po
likvidaci	likvidace	k1gFnSc6	likvidace
statků	statek	k1gInPc2	statek
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
poslán	poslán	k2eAgInSc1d1	poslán
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k9	již
jako	jako	k9	jako
dítě	dítě	k1gNnSc4	dítě
vynikal	vynikat	k5eAaImAgMnS	vynikat
svou	svůj	k3xOyFgFnSc7	svůj
genialitou	genialita	k1gFnSc7	genialita
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
viděl	vidět	k5eAaImAgInS	vidět
potřebu	potřeba	k1gFnSc4	potřeba
reformovat	reformovat	k5eAaBmF	reformovat
současnou	současný	k2eAgFnSc4d1	současná
vědu	věda	k1gFnSc4	věda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studie	k1gFnPc6	studie
doprovázel	doprovázet	k5eAaImAgInS	doprovázet
anglického	anglický	k2eAgMnSc4d1	anglický
velvyslance	velvyslanec	k1gMnSc4	velvyslanec
na	na	k7c4	na
dvůr	dvůr	k1gInSc4	dvůr
Jindřicha	Jindřich	k1gMnSc2	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
po	po	k7c6	po
necelých	celý	k2eNgNnPc6d1	necelé
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
smrti	smrt	k1gFnSc3	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1579	[number]	k4	1579
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
připravoval	připravovat	k5eAaImAgInS	připravovat
na	na	k7c4	na
právnickou	právnický	k2eAgFnSc4d1	právnická
kariéru	kariéra	k1gFnSc4	kariéra
a	a	k8xC	a
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
se	se	k3xPyFc4	se
vrhl	vrhnout	k5eAaPmAgMnS	vrhnout
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
judikatury	judikatura	k1gFnSc2	judikatura
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
nakonec	nakonec	k6eAd1	nakonec
přednost	přednost	k1gFnSc4	přednost
politice	politika	k1gFnSc3	politika
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
členem	člen	k1gInSc7	člen
parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
1584	[number]	k4	1584
<g/>
)	)	kIx)	)
a	a	k8xC	a
připojil	připojit	k5eAaPmAgMnS	připojit
se	se	k3xPyFc4	se
ke	k	k7c3	k
straníkům	straník	k1gMnPc3	straník
Roberta	Robert	k1gMnSc2	Robert
Devereuxe	Devereuxe	k1gFnSc2	Devereuxe
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
hraběte	hrabě	k1gMnSc4	hrabě
z	z	k7c2	z
Essexu	Essex	k1gInSc2	Essex
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
stavěl	stavět	k5eAaImAgMnS	stavět
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
I.	I.	kA	I.
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
někdy	někdy	k6eAd1	někdy
i	i	k9	i
proti	proti	k7c3	proti
svému	svůj	k3xOyFgMnSc3	svůj
chlebodárci	chlebodárce	k1gMnSc3	chlebodárce
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
jen	jen	k6eAd1	jen
čestný	čestný	k2eAgInSc1d1	čestný
titul	titul	k1gInSc1	titul
rádce	rádce	k1gMnSc2	rádce
a	a	k8xC	a
mimořádného	mimořádný	k2eAgMnSc2d1	mimořádný
advokáta	advokát	k1gMnSc2	advokát
královny	královna	k1gFnSc2	královna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
I.	I.	kA	I.
se	se	k3xPyFc4	se
Baconovi	Bacon	k1gMnSc3	Bacon
dostalo	dostat	k5eAaPmAgNnS	dostat
rychle	rychle	k6eAd1	rychle
mnoha	mnoho	k4c2	mnoho
poct	pocta	k1gFnPc2	pocta
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nový	nový	k2eAgMnSc1d1	nový
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
I.	I.	kA	I.
měl	mít	k5eAaImAgMnS	mít
rád	rád	k6eAd1	rád
učence	učenec	k1gMnPc4	učenec
a	a	k8xC	a
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
si	se	k3xPyFc3	se
i	i	k8xC	i
Bacona	Bacona	k1gFnSc1	Bacona
<g/>
.	.	kIx.	.
</s>
<s>
Jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
ho	on	k3xPp3gMnSc4	on
advokátem	advokát	k1gMnSc7	advokát
(	(	kIx(	(
<g/>
1607	[number]	k4	1607
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
generálním	generální	k2eAgMnSc7d1	generální
advokátem	advokát	k1gMnSc7	advokát
(	(	kIx(	(
<g/>
1615	[number]	k4	1615
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
členem	člen	k1gMnSc7	člen
tajné	tajný	k2eAgFnSc2d1	tajná
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
1616	[number]	k4	1616
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
strážcem	strážce	k1gMnSc7	strážce
pečeti	pečeť	k1gFnSc2	pečeť
(	(	kIx(	(
<g/>
1617	[number]	k4	1617
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k8xC	i
lordem	lord	k1gMnSc7	lord
kancléřem	kancléř	k1gMnSc7	kancléř
(	(	kIx(	(
<g/>
1618	[number]	k4	1618
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k6eAd1	též
jej	on	k3xPp3gMnSc4	on
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
rytířem	rytíř	k1gMnSc7	rytíř
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1603	[number]	k4	1603
<g/>
,	,	kIx,	,
baronem	baron	k1gMnSc7	baron
z	z	k7c2	z
Verulamu	Verulam	k1gInSc2	Verulam
(	(	kIx(	(
<g/>
1618	[number]	k4	1618
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vikomtem	vikomt	k1gMnSc7	vikomt
St	St	kA	St
Alban	Alban	k1gInSc4	Alban
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1621	[number]	k4	1621
<g/>
.	.	kIx.	.
</s>
<s>
Bacon	Bacon	k1gMnSc1	Bacon
mu	on	k3xPp3gMnSc3	on
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
svými	svůj	k3xOyFgFnPc7	svůj
radami	rada	k1gFnPc7	rada
zřídit	zřídit	k5eAaPmF	zřídit
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Skotska	Skotsko	k1gNnSc2	Skotsko
a	a	k8xC	a
provést	provést	k5eAaPmF	provést
mnoho	mnoho	k4c4	mnoho
užitečných	užitečný	k2eAgFnPc2d1	užitečná
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
doloženo	doložen	k2eAgNnSc1d1	doloženo
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
dvouletého	dvouletý	k2eAgNnSc2d1	dvouleté
působení	působení	k1gNnSc2	působení
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
kancléře	kancléř	k1gMnSc2	kancléř
přijal	přijmout	k5eAaPmAgInS	přijmout
peníze	peníz	k1gInPc4	peníz
za	za	k7c4	za
udělení	udělení	k1gNnSc4	udělení
koncesí	koncese	k1gFnPc2	koncese
a	a	k8xC	a
privilegií	privilegium	k1gNnPc2	privilegium
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
korupci	korupce	k1gFnSc4	korupce
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1621	[number]	k4	1621
odvolán	odvolat	k5eAaPmNgInS	odvolat
z	z	k7c2	z
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
soudem	soud	k1gInSc7	soud
peerů	peer	k1gMnPc2	peer
k	k	k7c3	k
uvěznění	uvěznění	k1gNnSc3	uvěznění
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
Toweru	Towero	k1gNnSc6	Towero
a	a	k8xC	a
pokutě	pokuta	k1gFnSc6	pokuta
40	[number]	k4	40
000	[number]	k4	000
liber	libra	k1gFnPc2	libra
šterlinků	šterlink	k1gInPc2	šterlink
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zbaven	zbavit	k5eAaPmNgMnS	zbavit
všech	všecek	k3xTgFnPc2	všecek
svých	svůj	k3xOyFgFnPc2	svůj
poct	pocta	k1gFnPc2	pocta
a	a	k8xC	a
veřejných	veřejný	k2eAgFnPc2d1	veřejná
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Bacon	Bacon	k1gInSc1	Bacon
svou	svůj	k3xOyFgFnSc4	svůj
chybu	chyba	k1gFnSc4	chyba
uznal	uznat	k5eAaPmAgMnS	uznat
<g/>
,	,	kIx,	,
pokutu	pokuta	k1gFnSc4	pokuta
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
nevstoupil	vstoupit	k5eNaPmAgMnS	vstoupit
na	na	k7c4	na
půdu	půda	k1gFnSc4	půda
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
však	však	k9	však
byl	být	k5eAaImAgInS	být
oblíben	oblíbit	k5eAaPmNgInS	oblíbit
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
též	též	k9	též
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
nestal	stát	k5eNaPmAgInS	stát
obětí	oběť	k1gFnSc7	oběť
politických	politický	k2eAgFnPc2d1	politická
intrik	intrika	k1gFnPc2	intrika
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
I.	I.	kA	I.
mu	on	k3xPp3gMnSc3	on
po	po	k7c6	po
pár	pár	k4xCyI	pár
dnech	den	k1gInPc6	den
ve	v	k7c4	v
vězení	vězení	k1gNnSc4	vězení
dal	dát	k5eAaPmAgInS	dát
milost	milost	k1gFnSc4	milost
a	a	k8xC	a
odpustil	odpustit	k5eAaPmAgInS	odpustit
mu	on	k3xPp3gNnSc3	on
i	i	k9	i
udělenou	udělený	k2eAgFnSc4d1	udělená
pokutu	pokuta	k1gFnSc4	pokuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1624	[number]	k4	1624
jej	on	k3xPp3gMnSc4	on
král	král	k1gMnSc1	král
dokonce	dokonce	k9	dokonce
osvobodil	osvobodit	k5eAaPmAgInS	osvobodit
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
obvinění	obvinění	k1gNnPc2	obvinění
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
roky	rok	k1gInPc1	rok
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
Bacon	Bacon	k1gMnSc1	Bacon
věnoval	věnovat	k5eAaPmAgMnS	věnovat
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
filozofických	filozofický	k2eAgInPc6d1	filozofický
spisech	spis	k1gInPc6	spis
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
při	při	k7c6	při
svých	svůj	k3xOyFgInPc6	svůj
pokusech	pokus	k1gInPc6	pokus
se	s	k7c7	s
zmrazováním	zmrazování	k1gNnSc7	zmrazování
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Francis	Francis	k1gFnSc3	Francis
Bacon	Bacon	k1gMnSc1	Bacon
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
zůstala	zůstat	k5eAaPmAgFnS	zůstat
nedokončena	dokončit	k5eNaPmNgFnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
jeho	jeho	k3xOp3gFnPc2	jeho
prací	práce	k1gFnPc2	práce
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
až	až	k9	až
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
své	svůj	k3xOyFgNnSc4	svůj
nejdůležitější	důležitý	k2eAgNnSc4d3	nejdůležitější
a	a	k8xC	a
vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
dílo	dílo	k1gNnSc4	dílo
plánoval	plánovat	k5eAaImAgMnS	plánovat
Bacon	Bacon	k1gMnSc1	Bacon
napsat	napsat	k5eAaPmF	napsat
spis	spis	k1gInSc4	spis
Velké	velká	k1gFnSc2	velká
obnovení	obnovení	k1gNnSc2	obnovení
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
skládat	skládat	k5eAaImF	skládat
ze	z	k7c2	z
6	[number]	k4	6
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
oddíly	oddíl	k1gInPc1	oddíl
měly	mít	k5eAaImAgInP	mít
obsahovat	obsahovat	k5eAaImF	obsahovat
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Klasifikaci	klasifikace	k1gFnSc4	klasifikace
a	a	k8xC	a
přehled	přehled	k1gInSc4	přehled
věd	věda	k1gFnPc2	věda
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc7d1	nová
induktivní	induktivní	k2eAgFnSc7d1	induktivní
metodou	metoda	k1gFnSc7	metoda
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Popis	popis	k1gInSc1	popis
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
základě	základ	k1gInSc6	základ
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
s	s	k7c7	s
metodou	metoda	k1gFnSc7	metoda
pracovat	pracovat	k5eAaImF	pracovat
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Výčet	výčet	k1gInSc1	výčet
objevů	objev	k1gInPc2	objev
dosažených	dosažený	k2eAgInPc2d1	dosažený
novou	nový	k2eAgFnSc7d1	nová
metodou	metoda	k1gFnSc7	metoda
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Částečné	částečný	k2eAgInPc1d1	částečný
a	a	k8xC	a
hypotetické	hypotetický	k2eAgInPc4d1	hypotetický
závěry	závěr	k1gInPc4	závěr
odvozené	odvozený	k2eAgInPc4d1	odvozený
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
experimentu	experiment	k1gInSc2	experiment
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
přijmout	přijmout	k5eAaPmF	přijmout
a	a	k8xC	a
používat	používat	k5eAaImF	používat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
nebude	být	k5eNaImBp3nS	být
uplatněna	uplatnit	k5eAaPmNgFnS	uplatnit
induktivní	induktivní	k2eAgFnSc1d1	induktivní
metoda	metoda	k1gFnSc1	metoda
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
syntéza	syntéza	k1gFnSc1	syntéza
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
plánu	plán	k1gInSc2	plán
Bacon	Bacon	k1gMnSc1	Bacon
dokončil	dokončit	k5eAaPmAgMnS	dokončit
pouze	pouze	k6eAd1	pouze
předmluvu	předmluva	k1gFnSc4	předmluva
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
část	část	k1gFnSc4	část
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Nové	Nové	k2eAgNnSc1d1	Nové
organon	organon	k1gNnSc1	organon
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
první	první	k4xOgFnSc2	první
části	část	k1gFnSc2	část
pouze	pouze	k6eAd1	pouze
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
dříve	dříve	k6eAd2	dříve
vydaný	vydaný	k2eAgInSc4d1	vydaný
spis	spis	k1gInSc4	spis
O	o	k7c6	o
pokroku	pokrok	k1gInSc6	pokrok
vědění	vědění	k1gNnSc2	vědění
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ostatních	ostatní	k2eAgFnPc2d1	ostatní
částí	část	k1gFnPc2	část
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
pouze	pouze	k6eAd1	pouze
některé	některý	k3yIgInPc4	některý
rozpracované	rozpracovaný	k2eAgInPc4d1	rozpracovaný
zlomky	zlomek	k1gInPc4	zlomek
jako	jako	k8xC	jako
například	například	k6eAd1	například
Popis	popis	k1gInSc1	popis
větrů	vítr	k1gInPc2	vítr
<g/>
,	,	kIx,	,
Popis	popis	k1gInSc1	popis
hustého	hustý	k2eAgNnSc2d1	husté
a	a	k8xC	a
řídkého	řídký	k2eAgNnSc2d1	řídké
<g/>
,	,	kIx,	,
Zkoumání	zkoumání	k1gNnSc2	zkoumání
magnetu	magnet	k1gInSc2	magnet
(	(	kIx(	(
<g/>
vše	všechen	k3xTgNnSc4	všechen
k	k	k7c3	k
3	[number]	k4	3
<g/>
.	.	kIx.	.
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
a	a	k8xC	a
2	[number]	k4	2
předmluvy	předmluva	k1gFnSc2	předmluva
(	(	kIx(	(
<g/>
ke	k	k7c3	k
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
Baconovy	Baconův	k2eAgInPc4d1	Baconův
nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
spisy	spis	k1gInPc4	spis
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Essays	Essays	k1gInSc1	Essays
(	(	kIx(	(
<g/>
1597	[number]	k4	1597
<g/>
,	,	kIx,	,
rozšířená	rozšířený	k2eAgNnPc4d1	rozšířené
vydání	vydání	k1gNnPc4	vydání
1612	[number]	k4	1612
a	a	k8xC	a
1625	[number]	k4	1625
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
"	"	kIx"	"
<g/>
Eseje	esej	k1gFnPc1	esej
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
"	"	kIx"	"
<g/>
Essaye	Essaye	k1gFnSc1	Essaye
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
česky	česky	k6eAd1	česky
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
]	]	kIx)	]
Valerius	Valerius	k1gInSc1	Valerius
Terminus	terminus	k1gInSc1	terminus
of	of	k?	of
the	the	k?	the
Interpretation	Interpretation	k1gInSc1	Interpretation
of	of	k?	of
Nature	Natur	k1gMnSc5	Natur
(	(	kIx(	(
<g/>
1603	[number]	k4	1603
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
"	"	kIx"	"
<g/>
Valerius	Valerius	k1gInSc1	Valerius
Terminus	terminus	k1gInSc1	terminus
o	o	k7c6	o
interpretaci	interpretace	k1gFnSc6	interpretace
přírody	příroda	k1gFnSc2	příroda
<g/>
"	"	kIx"	"
<g/>
]	]	kIx)	]
The	The	k1gMnSc1	The
Advancement	Advancement	k1gMnSc1	Advancement
of	of	k?	of
Learning	Learning	k1gInSc1	Learning
(	(	kIx(	(
<g/>
1605	[number]	k4	1605
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
"	"	kIx"	"
<g/>
O	o	k7c6	o
pokroku	pokrok	k1gInSc6	pokrok
vědění	vědění	k1gNnSc2	vědění
<g/>
"	"	kIx"	"
<g/>
]	]	kIx)	]
<g />
.	.	kIx.	.
</s>
<s>
Cogitata	Cogitata	k1gFnSc1	Cogitata
et	et	k?	et
Visa	viso	k1gNnSc2	viso
de	de	k?	de
Interpetatione	Interpetation	k1gInSc5	Interpetation
Naturae	Naturae	k1gNnPc1	Naturae
(	(	kIx(	(
<g/>
1607	[number]	k4	1607
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
"	"	kIx"	"
<g/>
Myšlenky	myšlenka	k1gFnPc1	myšlenka
a	a	k8xC	a
pozorování	pozorování	k1gNnPc1	pozorování
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
interpretace	interpretace	k1gFnPc1	interpretace
přírody	příroda	k1gFnSc2	příroda
<g/>
"	"	kIx"	"
<g/>
]	]	kIx)	]
Novum	novum	k1gNnSc1	novum
Organum	organum	k1gNnSc1	organum
(	(	kIx(	(
<g/>
1620	[number]	k4	1620
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
"	"	kIx"	"
<g/>
Nové	Nové	k2eAgNnSc1d1	Nové
organon	organon	k1gNnSc1	organon
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
]	]	kIx)	]
History	Histor	k1gInPc1	Histor
of	of	k?	of
the	the	k?	the
Reign	Reign	k1gMnSc1	Reign
of	of	k?	of
King	King	k1gMnSc1	King
Henry	Henry	k1gMnSc1	Henry
VII	VII	kA	VII
(	(	kIx(	(
<g/>
1622	[number]	k4	1622
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
"	"	kIx"	"
<g/>
Dějiny	dějiny	k1gFnPc1	dějiny
vlády	vláda	k1gFnSc2	vláda
krále	král	k1gMnSc4	král
Jindřicha	Jindřich	k1gMnSc4	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
De	De	k?	De
dignitate	dignitat	k1gInSc5	dignitat
et	et	k?	et
augmentis	augmentis	k1gInSc1	augmentis
scientiarum	scientiarum	k1gInSc1	scientiarum
(	(	kIx(	(
<g/>
1623	[number]	k4	1623
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
"	"	kIx"	"
<g/>
O	o	k7c6	o
důstojnosti	důstojnost	k1gFnSc6	důstojnost
a	a	k8xC	a
pokroku	pokrok	k1gInSc6	pokrok
věd	věda	k1gFnPc2	věda
<g/>
"	"	kIx"	"
<g/>
]	]	kIx)	]
Tento	tento	k3xDgInSc1	tento
spis	spis	k1gInSc1	spis
je	být	k5eAaImIp3nS	být
rozšířeným	rozšířený	k2eAgNnSc7d1	rozšířené
latinským	latinský	k2eAgNnSc7d1	latinské
zněním	znění	k1gNnSc7	znění
práce	práce	k1gFnSc2	práce
The	The	k1gMnSc1	The
Advancement	Advancement	k1gMnSc1	Advancement
of	of	k?	of
Learning	Learning	k1gInSc1	Learning
[	[	kIx(	[
<g/>
"	"	kIx"	"
<g/>
O	o	k7c6	o
pokroku	pokrok	k1gInSc6	pokrok
vědění	vědění	k1gNnSc2	vědění
<g/>
"	"	kIx"	"
<g/>
]	]	kIx)	]
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1605	[number]	k4	1605
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
zařadila	zařadit	k5eAaPmAgFnS	zařadit
uvedený	uvedený	k2eAgInSc4d1	uvedený
latinský	latinský	k2eAgInSc4d1	latinský
spis	spis	k1gInSc4	spis
dekretem	dekret	k1gInSc7	dekret
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1668	[number]	k4	1668
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Atlantis	Atlantis	k1gFnSc1	Atlantis
(	(	kIx(	(
<g/>
vydáno	vydat	k5eAaPmNgNnS	vydat
posmrtně	posmrtně	k6eAd1	posmrtně
1627	[number]	k4	1627
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
"	"	kIx"	"
<g/>
Nová	nový	k2eAgFnSc1d1	nová
Atlantis	Atlantis	k1gFnSc1	Atlantis
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Nová	nový	k2eAgFnSc1d1	nová
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
]	]	kIx)	]
Filozoficky	filozoficky	k6eAd1	filozoficky
nejdůležitější	důležitý	k2eAgNnSc4d3	nejdůležitější
dílo	dílo	k1gNnSc4	dílo
Francise	Francise	k1gFnSc2	Francise
Bacona	Bacona	k1gFnSc1	Bacona
je	být	k5eAaImIp3nS	být
spis	spis	k1gInSc4	spis
Nové	Nové	k2eAgNnSc1d1	Nové
organon	organon	k1gNnSc1	organon
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
úmyslně	úmyslně	k6eAd1	úmyslně
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
spis	spis	k1gInSc4	spis
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
a	a	k8xC	a
jehož	jehož	k3xOyRp3gInSc1	jehož
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
Organon	organon	k1gInSc1	organon
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
řecky	řecky	k6eAd1	řecky
nástroj	nástroj	k1gInSc4	nástroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Francis	Francis	k1gInSc1	Francis
Bacon	Bacon	k1gMnSc1	Bacon
chtěl	chtít	k5eAaImAgMnS	chtít
svým	svůj	k3xOyFgInSc7	svůj
dílem	díl	k1gInSc7	díl
dát	dát	k5eAaPmF	dát
vědcům	vědec	k1gMnPc3	vědec
a	a	k8xC	a
filozofům	filozof	k1gMnPc3	filozof
"	"	kIx"	"
<g/>
nový	nový	k2eAgInSc4d1	nový
nástroj	nástroj	k1gInSc4	nástroj
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
umožní	umožnit	k5eAaPmIp3nS	umožnit
překonání	překonání	k1gNnSc4	překonání
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Nástrojem	nástroj	k1gInSc7	nástroj
je	být	k5eAaImIp3nS	být
induktivní	induktivní	k2eAgFnSc1d1	induktivní
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
postupu	postup	k1gInSc6	postup
od	od	k7c2	od
konkrétních	konkrétní	k2eAgNnPc2d1	konkrétní
dat	datum	k1gNnPc2	datum
k	k	k7c3	k
obecným	obecný	k2eAgInPc3d1	obecný
závěrům	závěr	k1gInPc3	závěr
a	a	k8xC	a
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
tak	tak	k9	tak
od	od	k7c2	od
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
převažující	převažující	k2eAgFnSc2d1	převažující
metody	metoda	k1gFnSc2	metoda
dedukce	dedukce	k1gFnSc2	dedukce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
postupu	postup	k1gInSc6	postup
od	od	k7c2	od
obecných	obecný	k2eAgFnPc2d1	obecná
zákonitostí	zákonitost	k1gFnPc2	zákonitost
ke	k	k7c3	k
konkrétním	konkrétní	k2eAgFnPc3d1	konkrétní
jednotlivostem	jednotlivost	k1gFnPc3	jednotlivost
<g/>
.	.	kIx.	.
</s>
<s>
Baconova	Baconův	k2eAgFnSc1d1	Baconova
metoda	metoda	k1gFnSc1	metoda
tak	tak	k9	tak
klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
empiricky	empiricky	k6eAd1	empiricky
zachytitelná	zachytitelný	k2eAgNnPc4d1	zachytitelné
data	datum	k1gNnPc4	datum
<g/>
,	,	kIx,	,
na	na	k7c6	na
pozorování	pozorování	k1gNnSc6	pozorování
a	a	k8xC	a
experiment	experiment	k1gInSc1	experiment
a	a	k8xC	a
Bacona	Bacona	k1gFnSc1	Bacona
tak	tak	k6eAd1	tak
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c2	za
otce	otec	k1gMnSc2	otec
moderní	moderní	k2eAgFnSc2d1	moderní
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Bacon	Bacon	k1gMnSc1	Bacon
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
výsledky	výsledek	k1gInPc4	výsledek
staré	starý	k2eAgFnSc2d1	stará
scholastické	scholastický	k2eAgFnSc2d1	scholastická
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
filozofie	filozofie	k1gFnSc2	filozofie
jako	jako	k8xC	jako
neplodné	plodný	k2eNgNnSc1d1	neplodné
filozofování	filozofování	k1gNnSc1	filozofování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nemá	mít	k5eNaImIp3nS	mít
často	často	k6eAd1	často
užitek	užitek	k1gInSc4	užitek
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
požadoval	požadovat	k5eAaImAgMnS	požadovat
po	po	k7c6	po
vědě	věda	k1gFnSc6	věda
užitečnost	užitečnost	k1gFnSc4	užitečnost
člověku	člověk	k1gMnSc3	člověk
a	a	k8xC	a
zlepšování	zlepšování	k1gNnSc3	zlepšování
podmínek	podmínka	k1gFnPc2	podmínka
života	život	k1gInSc2	život
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Idoly	idol	k1gInPc1	idol
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Baconově	Baconův	k2eAgNnSc6d1	Baconovo
pojetí	pojetí	k1gNnSc6	pojetí
překážky	překážka	k1gFnSc2	překážka
objektivního	objektivní	k2eAgNnSc2d1	objektivní
poznání	poznání	k1gNnSc2	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Bacon	Bacon	k1gInSc1	Bacon
vyjmenovává	vyjmenovávat	k5eAaImIp3nS	vyjmenovávat
následující	následující	k2eAgInPc4d1	následující
čtyři	čtyři	k4xCgInPc4	čtyři
<g/>
:	:	kIx,	:
</s>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
Idol	idol	k1gInSc1	idol
lidského	lidský	k2eAgInSc2d1	lidský
rodu	rod	k1gInSc2	rod
(	(	kIx(	(
<g/>
idola	idola	k1gMnSc1	idola
tribus	tribus	k1gMnSc1	tribus
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Idol	idol	k1gInSc1	idol
jeskyně	jeskyně	k1gFnSc2	jeskyně
(	(	kIx(	(
<g/>
idola	idola	k1gMnSc1	idola
specus	specus	k1gMnSc1	specus
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Idol	idol	k1gInSc1	idol
tržiště	tržiště	k1gNnSc2	tržiště
(	(	kIx(	(
<g/>
idola	idola	k1gFnSc1	idola
fori	for	k1gFnSc2	for
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Idol	idol	k1gInSc1	idol
divadla	divadlo	k1gNnSc2	divadlo
(	(	kIx(	(
<g/>
idola	idola	k1gFnSc1	idola
theatri	theatr	k1gFnSc2	theatr
<g/>
)	)	kIx)	)
Idol	idol	k1gInSc1	idol
lidského	lidský	k2eAgInSc2d1	lidský
rodu	rod	k1gInSc2	rod
<g/>
:	:	kIx,	:
Jsme	být	k5eAaImIp1nP	být
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
příslušníci	příslušník	k1gMnPc1	příslušník
lidského	lidský	k2eAgInSc2d1	lidský
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc4	svět
kolem	kolem	k6eAd1	kolem
nás	my	k3xPp1nPc4	my
tedy	tedy	k9	tedy
vnímáme	vnímat	k5eAaImIp1nP	vnímat
právě	právě	k6eAd1	právě
jako	jako	k9	jako
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
ale	ale	k9	ale
můžeme	moct	k5eAaImIp1nP	moct
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
my	my	k3xPp1nPc1	my
lidé	člověk	k1gMnPc1	člověk
svět	svět	k1gInSc4	svět
vnímáme	vnímat	k5eAaImIp1nP	vnímat
pravdivě	pravdivě	k6eAd1	pravdivě
<g/>
?	?	kIx.	?
</s>
<s>
Například	například	k6eAd1	například
moucha	moucha	k1gFnSc1	moucha
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
složeným	složený	k2eAgNnSc7d1	složené
okem	oke	k1gNnSc7	oke
vnímá	vnímat	k5eAaImIp3nS	vnímat
svět	svět	k1gInSc1	svět
jistě	jistě	k9	jistě
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Nevnímá	vnímat	k5eNaImIp3nS	vnímat
ona	onen	k3xDgFnSc1	onen
svět	svět	k1gInSc4	svět
pravdivěji	pravdivě	k6eAd2	pravdivě
než	než	k8xS	než
člověk	člověk	k1gMnSc1	člověk
<g/>
?	?	kIx.	?
</s>
<s>
Idol	idol	k1gInSc1	idol
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
:	:	kIx,	:
Každý	každý	k3xTgMnSc1	každý
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
trochu	trochu	k6eAd1	trochu
odlišný	odlišný	k2eAgInSc1d1	odlišný
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
tedy	tedy	k9	tedy
můžu	můžu	k?	můžu
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
svět	svět	k1gInSc1	svět
vnímám	vnímat	k5eAaImIp1nS	vnímat
pravdivě	pravdivě	k6eAd1	pravdivě
právě	právě	k6eAd1	právě
já	já	k3xPp1nSc1	já
<g/>
?	?	kIx.	?
</s>
<s>
Například	například	k6eAd1	například
existují	existovat	k5eAaImIp3nP	existovat
barvoslepí	barvoslepý	k2eAgMnPc1d1	barvoslepý
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vnímají	vnímat	k5eAaImIp3nP	vnímat
svět	svět	k1gInSc4	svět
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vnímají	vnímat	k5eAaImIp3nP	vnímat
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
ale	ale	k9	ale
můžu	můžu	k?	můžu
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
nejsou	být	k5eNaImIp3nP	být
barvoslepí	barvoslepý	k2eAgMnPc1d1	barvoslepý
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nevnímají	vnímat	k5eNaImIp3nP	vnímat
svět	svět	k1gInSc4	svět
pravdivěji	pravdivě	k6eAd2	pravdivě
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
že	že	k8xS	že
i	i	k9	i
já	já	k3xPp1nSc1	já
nemám	mít	k5eNaImIp1nS	mít
jakousi	jakýsi	k3yIgFnSc4	jakýsi
vadu	vada	k1gFnSc4	vada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
brání	bránit	k5eAaImIp3nS	bránit
pravdivému	pravdivý	k2eAgNnSc3d1	pravdivé
poznání	poznání	k1gNnSc3	poznání
<g/>
?	?	kIx.	?
</s>
<s>
Idol	idol	k1gInSc1	idol
tržiště	tržiště	k1gNnSc2	tržiště
<g/>
:	:	kIx,	:
Lidé	člověk	k1gMnPc1	člověk
užívají	užívat	k5eAaImIp3nP	užívat
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
užívají	užívat	k5eAaImIp3nP	užívat
ho	on	k3xPp3gInSc4	on
špatně	špatně	k6eAd1	špatně
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
lidé	člověk	k1gMnPc1	člověk
používají	používat	k5eAaImIp3nP	používat
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
nic	nic	k6eAd1	nic
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
(	(	kIx(	(
<g/>
štěstěna	štěstěna	k1gFnSc1	štěstěna
<g/>
,	,	kIx,	,
živel	živel	k1gInSc1	živel
ohně	oheň	k1gInSc2	oheň
<g/>
)	)	kIx)	)
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
těchto	tento	k3xDgInPc2	tento
pojmů	pojem	k1gInPc2	pojem
potom	potom	k8xC	potom
člověk	člověk	k1gMnSc1	člověk
poznává	poznávat	k5eAaImIp3nS	poznávat
a	a	k8xC	a
interpretuje	interpretovat	k5eAaBmIp3nS	interpretovat
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Idol	idol	k1gInSc1	idol
divadla	divadlo	k1gNnSc2	divadlo
<g/>
:	:	kIx,	:
Lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP	mít
určitě	určitě	k6eAd1	určitě
vědecké	vědecký	k2eAgFnSc2d1	vědecká
<g/>
,	,	kIx,	,
filozofické	filozofický	k2eAgFnSc2d1	filozofická
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
nový	nový	k2eAgInSc4d1	nový
poznatek	poznatek	k1gInSc4	poznatek
tedy	tedy	k8xC	tedy
vykládáme	vykládat	k5eAaImIp1nP	vykládat
vždy	vždy	k6eAd1	vždy
z	z	k7c2	z
již	již	k6eAd1	již
existujících	existující	k2eAgInPc2d1	existující
předpokladů	předpoklad	k1gInPc2	předpoklad
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
ale	ale	k9	ale
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
teorie	teorie	k1gFnPc1	teorie
opravdu	opravdu	k6eAd1	opravdu
pravdivé	pravdivý	k2eAgFnPc1d1	pravdivá
<g/>
?	?	kIx.	?
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
Atlantis	Atlantis	k1gFnSc1	Atlantis
je	být	k5eAaImIp3nS	být
krátká	krátký	k2eAgFnSc1d1	krátká
novela	novela	k1gFnSc1	novela
vydána	vydán	k2eAgFnSc1d1	vydána
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1626	[number]	k4	1626
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
děl	dělo	k1gNnPc2	dělo
Thomase	Thomas	k1gMnSc2	Thomas
Mora	mora	k1gFnSc1	mora
a	a	k8xC	a
Tommasa	Tommasa	k1gFnSc1	Tommasa
Campanelly	Campanella	k1gFnSc2	Campanella
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
slavné	slavný	k2eAgFnPc4d1	slavná
utopie	utopie	k1gFnPc4	utopie
raného	raný	k2eAgInSc2d1	raný
novověku	novověk	k1gInSc2	novověk
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgMnSc2d1	řecký
ú-topos	úopos	k1gMnSc1	ú-topos
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
žádné	žádný	k3yNgNnSc4	žádný
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
nikde	nikde	k6eAd1	nikde
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bacon	Bacon	k1gInSc1	Bacon
v	v	k7c6	v
Nové	Nová	k1gFnSc6	Nová
Atlantis	Atlantis	k1gFnSc1	Atlantis
popisuje	popisovat	k5eAaImIp3nS	popisovat
zřízení	zřízení	k1gNnSc4	zřízení
fiktivního	fiktivní	k2eAgInSc2d1	fiktivní
ideálního	ideální	k2eAgInSc2d1	ideální
státu	stát	k1gInSc2	stát
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Bensalem	Bensal	k1gInSc7	Bensal
<g/>
.	.	kIx.	.
</s>
<s>
Klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
význam	význam	k1gInSc4	význam
vědy	věda	k1gFnSc2	věda
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
a	a	k8xC	a
z	z	k7c2	z
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
metod	metoda	k1gFnPc2	metoda
akcentuje	akcentovat	k5eAaImIp3nS	akcentovat
experiment	experiment	k1gInSc1	experiment
a	a	k8xC	a
pozorování	pozorování	k1gNnSc1	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
<g/>
:	:	kIx,	:
Evropská	evropský	k2eAgFnSc1d1	Evropská
loď	loď	k1gFnSc1	loď
narazí	narazit	k5eAaPmIp3nS	narazit
kdesi	kdesi	k6eAd1	kdesi
v	v	k7c6	v
odlehlé	odlehlý	k2eAgFnSc6d1	odlehlá
části	část	k1gFnSc6	část
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Bensalem	Bensal	k1gInSc7	Bensal
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
obyvatel	obyvatel	k1gMnSc1	obyvatel
námořníci	námořník	k1gMnPc1	námořník
dostanou	dostat	k5eAaPmIp3nP	dostat
povolení	povolení	k1gNnPc4	povolení
k	k	k7c3	k
návštěvě	návštěva	k1gFnSc3	návštěva
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ubytováni	ubytován	k2eAgMnPc1d1	ubytován
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Domě	dům	k1gInSc6	dům
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
zde	zde	k6eAd1	zde
poznávají	poznávat	k5eAaImIp3nP	poznávat
Evropané	Evropan	k1gMnPc1	Evropan
místní	místní	k2eAgFnSc2d1	místní
zvyklosti	zvyklost	k1gFnSc2	zvyklost
<g/>
,	,	kIx,	,
mravy	mrav	k1gInPc4	mrav
a	a	k8xC	a
uspořádání	uspořádání	k1gNnSc4	uspořádání
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádné	mimořádný	k2eAgNnSc1d1	mimořádné
postavení	postavení	k1gNnSc1	postavení
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
mají	mít	k5eAaImIp3nP	mít
členové	člen	k1gMnPc1	člen
společnosti	společnost	k1gFnSc2	společnost
Dům	dům	k1gInSc1	dům
Šalamounův	Šalamounův	k2eAgInSc1d1	Šalamounův
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
vlastním	vlastní	k2eAgInSc7d1	vlastní
výzkumem	výzkum	k1gInSc7	výzkum
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
cestami	cesta	k1gFnPc7	cesta
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
částí	část	k1gFnPc2	část
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
přinášejí	přinášet	k5eAaImIp3nP	přinášet
na	na	k7c4	na
Bensalem	Bensal	k1gInSc7	Bensal
vynálezy	vynález	k1gInPc4	vynález
<g/>
,	,	kIx,	,
poznatky	poznatek	k1gInPc4	poznatek
<g/>
,	,	kIx,	,
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Eseje	esej	k1gFnSc2	esej
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
59	[number]	k4	59
krátkých	krátký	k2eAgInPc2d1	krátký
textů	text	k1gInPc2	text
na	na	k7c4	na
nejrůznější	různý	k2eAgNnPc4d3	nejrůznější
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
společenskými	společenský	k2eAgFnPc7d1	společenská
a	a	k8xC	a
politickými	politický	k2eAgNnPc7d1	politické
tématy	téma	k1gNnPc7	téma
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
verzi	verze	k1gFnSc4	verze
Esejů	esej	k1gInPc2	esej
vydal	vydat	k5eAaPmAgInS	vydat
Bacon	Bacon	k1gInSc1	Bacon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1597	[number]	k4	1597
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
tehdy	tehdy	k6eAd1	tehdy
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
pouze	pouze	k6eAd1	pouze
10	[number]	k4	10
krátkých	krátký	k2eAgNnPc2d1	krátké
pojednání	pojednání	k1gNnPc2	pojednání
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
Bacon	Bacon	k1gInSc1	Bacon
dílo	dílo	k1gNnSc4	dílo
stále	stále	k6eAd1	stále
doplňoval	doplňovat	k5eAaImAgMnS	doplňovat
a	a	k8xC	a
rozšiřoval	rozšiřovat	k5eAaImAgMnS	rozšiřovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
esejů	esej	k1gInPc2	esej
zastavil	zastavit	k5eAaPmAgInS	zastavit
na	na	k7c6	na
čísle	číslo	k1gNnSc6	číslo
59	[number]	k4	59
<g/>
.	.	kIx.	.
</s>
<s>
Eseje	esej	k1gFnPc1	esej
nejsou	být	k5eNaImIp3nP	být
hlubokým	hluboký	k2eAgInSc7d1	hluboký
filozofickým	filozofický	k2eAgInSc7d1	filozofický
dílem	díl	k1gInSc7	díl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
osobním	osobní	k2eAgNnSc7d1	osobní
pojednáním	pojednání	k1gNnSc7	pojednání
o	o	k7c6	o
tématech	téma	k1gNnPc6	téma
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
pomsty	pomsta	k1gFnSc2	pomsta
<g/>
,	,	kIx,	,
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
ateismu	ateismus	k1gInSc2	ateismus
<g/>
,	,	kIx,	,
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
,	,	kIx,	,
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
panování	panování	k1gNnSc2	panování
atd.	atd.	kA	atd.
Podtitul	podtitul	k1gInSc1	podtitul
díla	dílo	k1gNnSc2	dílo
zní	znět	k5eAaImIp3nS	znět
Rady	rada	k1gFnPc4	rada
mravní	mravní	k2eAgFnSc2d1	mravní
a	a	k8xC	a
politické	politický	k2eAgFnSc2d1	politická
<g/>
.	.	kIx.	.
</s>
<s>
Bacon	Bacon	k1gInSc1	Bacon
v	v	k7c6	v
Esejích	esej	k1gFnPc6	esej
radí	radit	k5eAaImIp3nS	radit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
přistupovat	přistupovat	k5eAaImF	přistupovat
k	k	k7c3	k
jednotlivým	jednotlivý	k2eAgFnPc3d1	jednotlivá
situacím	situace	k1gFnPc3	situace
v	v	k7c6	v
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nP	vážit
vždy	vždy	k6eAd1	vždy
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
pozitiva	pozitivum	k1gNnPc4	pozitivum
i	i	k8xC	i
negativa	negativum	k1gNnPc4	negativum
<g/>
,	,	kIx,	,
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
přistupovat	přistupovat	k5eAaImF	přistupovat
k	k	k7c3	k
problémům	problém	k1gInPc3	problém
pragmaticky	pragmaticky	k6eAd1	pragmaticky
<g/>
.	.	kIx.	.
</s>
<s>
Esej	esej	k1gFnSc1	esej
O	o	k7c6	o
protivenství	protivenství	k1gNnSc6	protivenství
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ctnost	ctnost	k1gFnSc1	ctnost
je	být	k5eAaImIp3nS	být
zajisté	zajisté	k9	zajisté
jako	jako	k9	jako
vzácné	vzácný	k2eAgNnSc4d1	vzácné
koření	koření	k1gNnSc4	koření
<g/>
;	;	kIx,	;
nejvíc	hodně	k6eAd3	hodně
voní	vonět	k5eAaImIp3nP	vonět
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
pálí	pálit	k5eAaImIp3nS	pálit
nebo	nebo	k8xC	nebo
drtí	drtit	k5eAaImIp3nS	drtit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Esej	esej	k1gFnSc1	esej
O	o	k7c6	o
ateismu	ateismus	k1gInSc6	ateismus
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kdo	kdo	k3yRnSc1	kdo
Boha	bůh	k1gMnSc2	bůh
popírá	popírat	k5eAaImIp3nS	popírat
<g/>
,	,	kIx,	,
vraždí	vraždit	k5eAaImIp3nP	vraždit
lidskou	lidský	k2eAgFnSc4d1	lidská
důstojnost	důstojnost	k1gFnSc4	důstojnost
<g/>
;	;	kIx,	;
protože	protože	k8xS	protože
svým	svůj	k3xOyFgNnSc7	svůj
tělem	tělo	k1gNnSc7	tělo
je	být	k5eAaImIp3nS	být
zajisté	zajisté	k9	zajisté
člověk	člověk	k1gMnSc1	člověk
spřízněn	spřízněn	k2eAgMnSc1d1	spřízněn
se	se	k3xPyFc4	se
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
;	;	kIx,	;
a	a	k8xC	a
kdyby	kdyby	k9	kdyby
po	po	k7c6	po
duchu	duch	k1gMnSc6	duch
nebyl	být	k5eNaImAgMnS	být
spřízněn	spřízněn	k2eAgMnSc1d1	spřízněn
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nízké	nízký	k2eAgNnSc1d1	nízké
a	a	k8xC	a
neušlechtilé	ušlechtilý	k2eNgNnSc1d1	neušlechtilé
stvoření	stvoření	k1gNnSc1	stvoření
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Esej	esej	k1gFnSc1	esej
O	o	k7c6	o
lichvě	lichva	k1gFnSc6	lichva
<g/>
:	:	kIx,	:
V	v	k7c6	v
Baconově	Baconův	k2eAgNnSc6d1	Baconovo
podání	podání	k1gNnSc6	podání
lichva	lichva	k1gFnSc1	lichva
je	být	k5eAaImIp3nS	být
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
půjčování	půjčování	k1gNnSc1	půjčování
na	na	k7c4	na
úrok	úrok	k1gInSc4	úrok
<g/>
.	.	kIx.	.
</s>
<s>
Bacon	Bacon	k1gMnSc1	Bacon
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
budou	být	k5eAaImBp3nP	být
vždy	vždy	k6eAd1	vždy
existovat	existovat	k5eAaImF	existovat
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
budou	být	k5eAaImBp3nP	být
potřebovat	potřebovat	k5eAaImF	potřebovat
půjčku	půjčka	k1gFnSc4	půjčka
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nemají	mít	k5eNaImIp3nP	mít
momentálně	momentálně	k6eAd1	momentálně
dostatek	dostatek	k1gInSc4	dostatek
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
zboží	zboží	k1gNnSc2	zboží
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
ale	ale	k9	ale
nechtějí	chtít	k5eNaImIp3nP	chtít
půjčovat	půjčovat	k5eAaImF	půjčovat
jiným	jiný	k2eAgMnPc3d1	jiný
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
nemají	mít	k5eNaImIp3nP	mít
žádný	žádný	k3yNgInSc4	žádný
zisk	zisk	k1gInSc4	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
správné	správný	k2eAgNnSc1d1	správné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
existovala	existovat	k5eAaImAgFnS	existovat
možnost	možnost	k1gFnSc1	možnost
půjčování	půjčování	k1gNnSc2	půjčování
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
úrok	úrok	k1gInSc4	úrok
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
lichvy	lichva	k1gFnSc2	lichva
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
omezení	omezení	k1gNnSc4	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
stojí	stát	k5eAaImIp3nS	stát
Bacon	Bacon	k1gNnSc4	Bacon
za	za	k7c7	za
Shakespearovými	Shakespearův	k2eAgNnPc7d1	Shakespearovo
dramaty	drama	k1gNnPc7	drama
<g/>
.	.	kIx.	.
</s>
<s>
Vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
šifru	šifra	k1gFnSc4	šifra
k	k	k7c3	k
zabezpečení	zabezpečení	k1gNnSc3	zabezpečení
diplomatické	diplomatický	k2eAgFnSc2d1	diplomatická
korespondence	korespondence	k1gFnSc2	korespondence
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgInS	zabývat
se	s	k7c7	s
schématem	schéma	k1gNnSc7	schéma
univerzálního	univerzální	k2eAgInSc2d1	univerzální
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
–	–	k?	–
<g/>
Francis	Francis	k1gInSc1	Francis
Bacon	Bacon	k1gInSc1	Bacon
<g/>
,	,	kIx,	,
Eseje	esej	k1gInPc1	esej
</s>
