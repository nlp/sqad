<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Fuchs	Fuchs	k1gMnSc1	Fuchs
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1895	[number]	k4	1895
Všechovice	Všechovice	k1gFnSc1	Všechovice
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1972	[number]	k4	1972
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
urbanista	urbanista	k1gMnSc1	urbanista
a	a	k8xC	a
teoretik	teoretik	k1gMnSc1	teoretik
značného	značný	k2eAgInSc2d1	značný
vlivu	vliv	k1gInSc2	vliv
a	a	k8xC	a
evropského	evropský	k2eAgInSc2d1	evropský
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
popředních	popřední	k2eAgInPc2d1	popřední
pionýrů	pionýr	k1gInPc2	pionýr
moderního	moderní	k2eAgInSc2d1	moderní
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
vyspělým	vyspělý	k2eAgInSc7d1	vyspělý
<g/>
,	,	kIx,	,
časně	časně	k6eAd1	časně
moderním	moderní	k2eAgInSc6d1	moderní
a	a	k8xC	a
mimořádně	mimořádně	k6eAd1	mimořádně
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
dílem	díl	k1gInSc7	díl
zásadně	zásadně	k6eAd1	zásadně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
vývoj	vývoj	k1gInSc1	vývoj
české	český	k2eAgFnSc2d1	Česká
architektury	architektura	k1gFnSc2	architektura
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
zrání	zrání	k1gNnSc4	zrání
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
kontextu	kontext	k1gInSc6	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Realizoval	realizovat	k5eAaBmAgMnS	realizovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
150	[number]	k4	150
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
některé	některý	k3yIgNnSc1	některý
jako	jako	k9	jako
hotel	hotel	k1gInSc1	hotel
Avion	avion	k1gInSc1	avion
<g/>
,	,	kIx,	,
pavilon	pavilon	k1gInSc1	pavilon
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
lázně	lázeň	k1gFnPc4	lázeň
Zábrdovice	Zábrdovice	k1gFnPc4	Zábrdovice
nebo	nebo	k8xC	nebo
Zelená	zelený	k2eAgFnSc1d1	zelená
žába	žába	k1gFnSc1	žába
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
jedněmi	jeden	k4xCgFnPc7	jeden
ze	z	k7c2	z
stěžejních	stěžejní	k2eAgNnPc2d1	stěžejní
děl	dělo	k1gNnPc2	dělo
moderní	moderní	k2eAgFnSc2d1	moderní
epochy	epocha	k1gFnSc2	epocha
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Fuchs	Fuchs	k1gMnSc1	Fuchs
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
ve	v	k7c6	v
Všechovicích	Všechovice	k1gFnPc6	Všechovice
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
Moravě	Morava	k1gFnSc6	Morava
(	(	kIx(	(
<g/>
Hranice	hranice	k1gFnSc1	hranice
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odlehlé	odlehlý	k2eAgFnSc3d1	odlehlá
obci	obec	k1gFnSc3	obec
ale	ale	k8xC	ale
na	na	k7c4	na
dohled	dohled	k1gInSc4	dohled
hlavních	hlavní	k2eAgFnPc2d1	hlavní
továren	továrna	k1gFnPc2	továrna
pro	pro	k7c4	pro
slavné	slavný	k2eAgFnPc4d1	slavná
židle	židle	k1gFnPc4	židle
Thonet	Thoneta	k1gFnPc2	Thoneta
v	v	k7c6	v
Bystřici	Bystřice	k1gFnSc6	Bystřice
pod	pod	k7c7	pod
Hostýnem	Hostýn	k1gInSc7	Hostýn
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Augustin	Augustin	k1gMnSc1	Augustin
Fuchs	Fuchs	k1gMnSc1	Fuchs
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
vedle	vedle	k7c2	vedle
kostela	kostel	k1gInSc2	kostel
krámek	krámek	k1gInSc4	krámek
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
Byl	být	k5eAaImAgInS	být
obdařen	obdařen	k2eAgInSc1d1	obdařen
mimořádným	mimořádný	k2eAgInSc7d1	mimořádný
<g/>
,	,	kIx,	,
přirozeným	přirozený	k2eAgInSc7d1	přirozený
a	a	k8xC	a
komplexním	komplexní	k2eAgInSc7d1	komplexní
talentem	talent	k1gInSc7	talent
pro	pro	k7c4	pro
architekturu	architektura	k1gFnSc4	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
cítění	cítění	k1gNnSc4	cítění
<g/>
,	,	kIx,	,
výjimečnou	výjimečný	k2eAgFnSc4d1	výjimečná
prostorovou	prostorový	k2eAgFnSc4d1	prostorová
představivost	představivost	k1gFnSc4	představivost
a	a	k8xC	a
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
harmonii	harmonie	k1gFnSc4	harmonie
<g/>
.	.	kIx.	.
</s>
<s>
Nechyběla	chybět	k5eNaImAgFnS	chybět
mu	on	k3xPp3gNnSc3	on
ovšem	ovšem	k9	ovšem
technická	technický	k2eAgFnSc1d1	technická
zodpovědnost	zodpovědnost	k1gFnSc1	zodpovědnost
<g/>
.	.	kIx.	.
</s>
<s>
Nadání	nadání	k1gNnSc1	nadání
cílevědomě	cílevědomě	k6eAd1	cílevědomě
a	a	k8xC	a
účinně	účinně	k6eAd1	účinně
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
studiem	studio	k1gNnSc7	studio
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
málo	málo	k4c4	málo
let	léto	k1gNnPc2	léto
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
bylo	být	k5eAaImAgNnS	být
univerzitní	univerzitní	k2eAgNnSc4d1	univerzitní
studium	studium	k1gNnSc4	studium
pro	pro	k7c4	pro
architekty	architekt	k1gMnPc4	architekt
spíše	spíše	k9	spíše
výjimkou	výjimka	k1gFnSc7	výjimka
než	než	k8xS	než
pravidlem	pravidlo	k1gNnSc7	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
pomoc	pomoc	k1gFnSc4	pomoc
vynikajících	vynikající	k2eAgMnPc2d1	vynikající
učitelů	učitel	k1gMnPc2	učitel
byl	být	k5eAaImAgMnS	být
Fuchs	Fuchs	k1gMnSc1	Fuchs
přesto	přesto	k8xC	přesto
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
"	"	kIx"	"
<g/>
samoukem	samouk	k1gMnSc7	samouk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
reálce	reálka	k1gFnSc6	reálka
v	v	k7c6	v
Holešově	Holešov	k1gInSc6	Holešov
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
během	během	k7c2	během
jeho	jeho	k3xOp3gNnPc2	jeho
studií	studio	k1gNnPc2	studio
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c4	na
reálné	reálný	k2eAgNnSc4d1	reálné
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Kreativní	kreativní	k2eAgInPc4d1	kreativní
sklony	sklon	k1gInPc4	sklon
projevil	projevit	k5eAaPmAgInS	projevit
již	již	k6eAd1	již
tam	tam	k6eAd1	tam
při	při	k7c6	při
vydávání	vydávání	k1gNnSc6	vydávání
studentského	studentský	k2eAgInSc2d1	studentský
časopisu	časopis	k1gInSc2	časopis
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
byl	být	k5eAaImAgInS	být
ze	z	k7c2	z
studií	studie	k1gFnPc2	studie
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
tedy	tedy	k8xC	tedy
učit	učit	k5eAaImF	učit
zedníkem	zedník	k1gMnSc7	zedník
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
žákem	žák	k1gMnSc7	žák
české	český	k2eAgFnSc2d1	Česká
Průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
školy	škola	k1gFnSc2	škola
stavební	stavební	k2eAgFnSc2d1	stavební
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
během	během	k7c2	během
studií	studie	k1gFnPc2	studie
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
vyučil	vyučit	k5eAaPmAgMnS	vyučit
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
štěstí	štěstí	k1gNnSc4	štěstí
na	na	k7c4	na
mimořádné	mimořádný	k2eAgMnPc4d1	mimořádný
učitele	učitel	k1gMnPc4	učitel
-	-	kIx~	-
vynikající	vynikající	k2eAgMnPc4d1	vynikající
architekty	architekt	k1gMnPc4	architekt
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Syřiště	syřiště	k1gNnSc2	syřiště
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Oplta	Oplta	k1gFnSc1	Oplta
a	a	k8xC	a
především	především	k9	především
Emila	Emil	k1gMnSc4	Emil
Králíka	Králík	k1gMnSc4	Králík
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc4d1	oficiální
přípravu	příprava	k1gFnSc4	příprava
završil	završit	k5eAaPmAgInS	završit
studiem	studio	k1gNnSc7	studio
na	na	k7c6	na
Akademii	akademie	k1gFnSc6	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
u	u	k7c2	u
profesora	profesor	k1gMnSc2	profesor
Jana	Jan	k1gMnSc2	Jan
Kotěry	Kotěra	k1gFnSc2	Kotěra
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
-	-	kIx~	-
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jej	on	k3xPp3gMnSc4	on
osobně	osobně	k6eAd1	osobně
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
vybral	vybrat	k5eAaPmAgInS	vybrat
<g/>
.	.	kIx.	.
</s>
<s>
Vyhnul	vyhnout	k5eAaPmAgInS	vyhnout
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
také	také	k6eAd1	také
službě	služba	k1gFnSc6	služba
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
i	i	k9	i
náskok	náskok	k1gInSc4	náskok
v	v	k7c6	v
profesní	profesní	k2eAgFnSc6d1	profesní
dráze	dráha	k1gFnSc6	dráha
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Motivace	motivace	k1gFnSc2	motivace
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
mimořádných	mimořádný	k2eAgInPc2d1	mimořádný
výsledků	výsledek	k1gInPc2	výsledek
byla	být	k5eAaImAgFnS	být
ještě	ještě	k9	ještě
posílena	posílit	k5eAaPmNgFnS	posílit
touhou	touha	k1gFnSc7	touha
povznést	povznést	k5eAaPmF	povznést
se	se	k3xPyFc4	se
z	z	k7c2	z
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
poměrů	poměr	k1gInPc2	poměr
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
nesmírně	smírně	k6eNd1	smírně
rostla	růst	k5eAaImAgFnS	růst
s	s	k7c7	s
nadcházejícími	nadcházející	k2eAgFnPc7d1	nadcházející
výzvami	výzva	k1gFnPc7	výzva
<g/>
,	,	kIx,	,
přicházejícími	přicházející	k2eAgMnPc7d1	přicházející
s	s	k7c7	s
neopakovatelnými	opakovatelný	k2eNgInPc7d1	neopakovatelný
úkoly	úkol	k1gInPc7	úkol
výstavby	výstavba	k1gFnSc2	výstavba
mladého	mladý	k2eAgInSc2d1	mladý
státu	stát	k1gInSc2	stát
po	po	k7c6	po
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
za	za	k7c4	za
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
nebyly	být	k5eNaImAgFnP	být
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
velké	velký	k2eAgFnSc2d1	velká
architektury	architektura	k1gFnSc2	architektura
jako	jako	k8xS	jako
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
I.	I.	kA	I.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
skončení	skončení	k1gNnSc6	skončení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
dispozice	dispozice	k1gFnPc1	dispozice
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgFnPc1d1	kulturní
<g/>
,	,	kIx,	,
společenské	společenský	k2eAgFnPc1d1	společenská
<g/>
,	,	kIx,	,
politické	politický	k2eAgFnPc1d1	politická
<g/>
,	,	kIx,	,
hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
demografické	demografický	k2eAgNnSc1d1	demografické
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
přirozeně	přirozeně	k6eAd1	přirozeně
střídaly	střídat	k5eAaImAgFnP	střídat
pozice	pozice	k1gFnPc4	pozice
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
společnosti	společnost	k1gFnSc2	společnost
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
mužů	muž	k1gMnPc2	muž
starších	starý	k2eAgInPc2d2	starší
ročníků	ročník	k1gInPc2	ročník
ještě	ještě	k6eAd1	ještě
vázáno	vázat	k5eAaImNgNnS	vázat
na	na	k7c6	na
válečných	válečný	k2eAgFnPc6d1	válečná
frontách	fronta	k1gFnPc6	fronta
<g/>
,	,	kIx,	,
dostala	dostat	k5eAaPmAgFnS	dostat
tedy	tedy	k9	tedy
příležitost	příležitost	k1gFnSc1	příležitost
přesně	přesně	k6eAd1	přesně
ona	onen	k3xDgFnSc1	onen
generační	generační	k2eAgFnSc1d1	generační
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
Fuchs	Fuchs	k1gMnSc1	Fuchs
náležel	náležet	k5eAaImAgInS	náležet
<g/>
.	.	kIx.	.
</s>
<s>
Fuchs	Fuchs	k1gMnSc1	Fuchs
byl	být	k5eAaImAgMnS	být
koncentrovanou	koncentrovaný	k2eAgFnSc7d1	koncentrovaná
osobností	osobnost	k1gFnSc7	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
nijak	nijak	k6eAd1	nijak
neomalený	omalený	k2eNgInSc1d1	neomalený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pohotový	pohotový	k2eAgInSc1d1	pohotový
a	a	k8xC	a
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
příležitosti	příležitost	k1gFnSc2	příležitost
bez	bez	k7c2	bez
zaváhání	zaváhání	k1gNnSc2	zaváhání
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vytěžit	vytěžit	k5eAaPmF	vytěžit
maximum	maximum	k1gNnSc4	maximum
<g/>
.	.	kIx.	.
</s>
<s>
Řečník	řečník	k1gMnSc1	řečník
byl	být	k5eAaImAgMnS	být
neobratný	obratný	k2eNgMnSc1d1	neobratný
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
projev	projev	k1gInSc1	projev
jeho	jeho	k3xOp3gInSc2	jeho
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
světa	svět	k1gInSc2	svět
zcela	zcela	k6eAd1	zcela
usměrnil	usměrnit	k5eAaPmAgMnS	usměrnit
především	především	k9	především
jen	jen	k9	jen
na	na	k7c4	na
architektonickou	architektonický	k2eAgFnSc4d1	architektonická
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gMnSc3	on
šla	jít	k5eAaImAgFnS	jít
snadno	snadno	k6eAd1	snadno
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
architektonické	architektonický	k2eAgNnSc1d1	architektonické
dílo	dílo	k1gNnSc1	dílo
budilo	budit	k5eAaImAgNnS	budit
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc4d2	veliký
důvěru	důvěra	k1gFnSc4	důvěra
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
víc	hodně	k6eAd2	hodně
a	a	k8xC	a
více	hodně	k6eAd2	hodně
ověřováno	ověřován	k2eAgNnSc1d1	ověřováno
jako	jako	k8xS	jako
pro	pro	k7c4	pro
všech	všecek	k3xTgFnPc6	všecek
stránkách	stránka	k1gFnPc6	stránka
spolehlivé	spolehlivý	k2eAgNnSc1d1	spolehlivé
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
vzdělaný	vzdělaný	k2eAgInSc4d1	vzdělaný
s	s	k7c7	s
kreativním	kreativní	k2eAgInSc7d1	kreativní
rozhledem	rozhled	k1gInSc7	rozhled
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
nadbytek	nadbytek	k1gInSc4	nadbytek
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
projekční	projekční	k2eAgFnSc2d1	projekční
práce	práce	k1gFnSc2	práce
neměl	mít	k5eNaImAgInS	mít
čas	čas	k1gInSc1	čas
a	a	k8xC	a
ani	ani	k8xC	ani
příležitost	příležitost	k1gFnSc1	příležitost
teoretické	teoretický	k2eAgNnSc4d1	teoretické
myšlení	myšlení	k1gNnSc4	myšlení
rozvinout	rozvinout	k5eAaPmF	rozvinout
do	do	k7c2	do
jiskrnější	jiskrný	k2eAgFnSc2d2	jiskrnější
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
architekt	architekt	k1gMnSc1	architekt
-	-	kIx~	-
i	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
podstaty	podstata	k1gFnSc2	podstata
zdrženlivý	zdrženlivý	k2eAgMnSc1d1	zdrženlivý
člověk	člověk	k1gMnSc1	člověk
-	-	kIx~	-
se	se	k3xPyFc4	se
veřejně	veřejně	k6eAd1	veřejně
angažoval	angažovat	k5eAaBmAgInS	angažovat
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
ve	v	k7c6	v
Spolku	spolek	k1gInSc6	spolek
architektů	architekt	k1gMnPc2	architekt
v	v	k7c6	v
naději	naděje	k1gFnSc6	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
výchozí	výchozí	k2eAgFnSc4d1	výchozí
platformou	platforma	k1gFnSc7	platforma
pro	pro	k7c4	pro
komoru	komora	k1gFnSc4	komora
architektů	architekt	k1gMnPc2	architekt
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
československým	československý	k2eAgMnSc7d1	československý
delegátem	delegát	k1gMnSc7	delegát
v	v	k7c6	v
CIAM	CIAM	kA	CIAM
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
Le	Le	k1gMnSc7	Le
Corbusierem	Corbusier	k1gMnSc7	Corbusier
ale	ale	k8xC	ale
nevyvíjel	vyvíjet	k5eNaImAgInS	vyvíjet
takovou	takový	k3xDgFnSc4	takový
ofenzivní	ofenzivní	k2eAgFnSc4d1	ofenzivní
společenskou	společenský	k2eAgFnSc4d1	společenská
činnost	činnost	k1gFnSc4	činnost
ku	k	k7c3	k
prospěchu	prospěch	k1gInSc3	prospěch
modernismu	modernismus	k1gInSc2	modernismus
<g/>
.	.	kIx.	.
</s>
<s>
Fuchs	Fuchs	k1gMnSc1	Fuchs
byl	být	k5eAaImAgMnS	být
zlákán	zlákat	k5eAaPmNgMnS	zlákat
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
perspektivou	perspektiva	k1gFnSc7	perspektiva
odborného	odborný	k2eAgNnSc2d1	odborné
místa	místo	k1gNnSc2	místo
ve	v	k7c6	v
stavebním	stavební	k2eAgNnSc6d1	stavební
a	a	k8xC	a
plánovacím	plánovací	k2eAgNnSc6d1	plánovací
oddělení	oddělení	k1gNnSc6	oddělení
Stavebního	stavební	k2eAgNnSc2d1	stavební
ředitelství	ředitelství	k1gNnSc2	ředitelství
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
v	v	k7c6	v
konkurzu	konkurz	k1gInSc6	konkurz
vybídl	vybídnout	k5eAaPmAgMnS	vybídnout
Jindřich	Jindřich	k1gMnSc1	Jindřich
Kumpošt	Kumpošt	k1gMnSc1	Kumpošt
<g/>
.	.	kIx.	.
</s>
<s>
Brnu	Brno	k1gNnSc3	Brno
tehdy	tehdy	k6eAd1	tehdy
nezbývalo	zbývat	k5eNaImAgNnS	zbývat
než	než	k8xS	než
mnoho	mnoho	k6eAd1	mnoho
stavět	stavět	k5eAaImF	stavět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
skýtalo	skýtat	k5eAaImAgNnS	skýtat
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
naději	naděje	k1gFnSc4	naděje
hojné	hojný	k2eAgFnSc2d1	hojná
a	a	k8xC	a
jisté	jistý	k2eAgFnSc2d1	jistá
realizace	realizace	k1gFnSc2	realizace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
skoro	skoro	k6eAd1	skoro
žádné	žádný	k3yNgFnSc2	žádný
překážky	překážka	k1gFnSc2	překážka
modernímu	moderní	k2eAgInSc3d1	moderní
trendu	trend	k1gInSc3	trend
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
postu	post	k1gInSc6	post
mu	on	k3xPp3gMnSc3	on
jako	jako	k8xC	jako
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
byly	být	k5eAaImAgFnP	být
automaticky	automaticky	k6eAd1	automaticky
přidělovány	přidělován	k2eAgInPc1d1	přidělován
úkoly	úkol	k1gInPc1	úkol
(	(	kIx(	(
<g/>
Masná	masný	k2eAgFnSc1d1	Masná
burza	burza	k1gFnSc1	burza
<g/>
,	,	kIx,	,
Pavilon	pavilon	k1gInSc1	pavilon
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vešly	vejít	k5eAaPmAgFnP	vejít
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
zejména	zejména	k9	zejména
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1923	[number]	k4	1923
<g/>
-	-	kIx~	-
<g/>
1928	[number]	k4	1928
zajištěn	zajištěn	k2eAgInSc1d1	zajištěn
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
"	"	kIx"	"
<g/>
proud	proud	k1gInSc1	proud
<g/>
"	"	kIx"	"
přímo	přímo	k6eAd1	přímo
zadaných	zadaný	k2eAgInPc2d1	zadaný
úkolů	úkol	k1gInPc2	úkol
-	-	kIx~	-
dychtivě	dychtivě	k6eAd1	dychtivě
<g/>
,	,	kIx,	,
s	s	k7c7	s
plným	plný	k2eAgNnSc7d1	plné
nasazením	nasazení	k1gNnSc7	nasazení
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
architektonických	architektonický	k2eAgFnPc2d1	architektonická
i	i	k8xC	i
urbanistických	urbanistický	k2eAgFnPc2d1	urbanistická
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
i	i	k9	i
častým	častý	k2eAgMnSc7d1	častý
porotcem	porotce	k1gMnSc7	porotce
<g/>
.	.	kIx.	.
</s>
<s>
Fuchs	Fuchs	k1gMnSc1	Fuchs
byl	být	k5eAaImAgMnS	být
přesvědčeným	přesvědčený	k2eAgMnSc7d1	přesvědčený
soutěžním	soutěžní	k2eAgInSc7d1	soutěžní
typem	typ	k1gInSc7	typ
a	a	k8xC	a
přijímal	přijímat	k5eAaImAgMnS	přijímat
téměř	téměř	k6eAd1	téměř
každou	každý	k3xTgFnSc4	každý
výzvu	výzva	k1gFnSc4	výzva
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Vzdor	vzdor	k7c3	vzdor
kolujícím	kolující	k2eAgFnPc3d1	kolující
legendám	legenda	k1gFnPc3	legenda
o	o	k7c4	o
ovlivňování	ovlivňování	k1gNnSc4	ovlivňování
soutěží	soutěž	k1gFnPc2	soutěž
je	být	k5eAaImIp3nS	být
ověřeno	ověřen	k2eAgNnSc1d1	ověřeno
<g/>
[	[	kIx(	[
<g/>
kým	kdo	k3yRnSc7	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
choval	chovat	k5eAaImAgMnS	chovat
"	"	kIx"	"
<g/>
fair	fair	k6eAd1	fair
play	play	k0	play
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Za	za	k7c2	za
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
učitelem	učitel	k1gMnSc7	učitel
na	na	k7c6	na
legendární	legendární	k2eAgFnSc6d1	legendární
"	"	kIx"	"
<g/>
Baťově	Baťův	k2eAgFnSc6d1	Baťova
<g/>
"	"	kIx"	"
umělecké	umělecký	k2eAgFnSc6d1	umělecká
škole	škola	k1gFnSc6	škola
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
po	po	k7c6	po
obnově	obnova	k1gFnSc6	obnova
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
profesorem	profesor	k1gMnSc7	profesor
urbanismu	urbanismus	k1gInSc2	urbanismus
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
technické	technický	k2eAgFnSc2d1	technická
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
Jestliže	jestliže	k8xS	jestliže
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
poválečných	poválečný	k2eAgNnPc6d1	poválečné
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
před	před	k7c7	před
Fuchsem	Fuchs	k1gMnSc7	Fuchs
otevřela	otevřít	k5eAaPmAgFnS	otevřít
opět	opět	k6eAd1	opět
tvůrčí	tvůrčí	k2eAgFnSc1d1	tvůrčí
perspektiva	perspektiva	k1gFnSc1	perspektiva
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byl	být	k5eAaImAgMnS	být
logicky	logicky	k6eAd1	logicky
povolán	povolat	k5eAaPmNgMnS	povolat
za	za	k7c4	za
profesora	profesor	k1gMnSc4	profesor
architektury	architektura	k1gFnSc2	architektura
na	na	k7c4	na
brněnskou	brněnský	k2eAgFnSc4d1	brněnská
Techniku	technika	k1gFnSc4	technika
<g/>
,	,	kIx,	,
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
situace	situace	k1gFnSc1	situace
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
i	i	k8xC	i
profesi	profes	k1gFnSc6	profes
stále	stále	k6eAd1	stále
svízelnější	svízelný	k2eAgFnSc1d2	svízelnější
<g/>
.	.	kIx.	.
</s>
<s>
Veřejných	veřejný	k2eAgInPc2d1	veřejný
úkolů	úkol	k1gInPc2	úkol
valem	valem	k6eAd1	valem
ubývalo	ubývat	k5eAaImAgNnS	ubývat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
prohloubilo	prohloubit	k5eAaPmAgNnS	prohloubit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Soukromá	soukromý	k2eAgFnSc1d1	soukromá
klientela	klientela	k1gFnSc1	klientela
drobných	drobný	k2eAgMnPc2d1	drobný
stavebníků	stavebník	k1gMnPc2	stavebník
moravského	moravský	k2eAgInSc2d1	moravský
venkova	venkov	k1gInSc2	venkov
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
raných	raný	k2eAgNnPc2d1	rané
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
decimována	decimován	k2eAgFnSc1d1	decimována
<g/>
.	.	kIx.	.
</s>
<s>
Fuchs	Fuchs	k1gMnSc1	Fuchs
byl	být	k5eAaImAgMnS	být
sice	sice	k8xC	sice
nadále	nadále	k6eAd1	nadále
profesorem	profesor	k1gMnSc7	profesor
i	i	k9	i
dokonce	dokonce	k9	dokonce
děkanem	děkan	k1gMnSc7	děkan
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc4	jeho
práce	práce	k1gFnSc1	práce
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
zákulisně	zákulisně	k6eAd1	zákulisně
omezována	omezován	k2eAgFnSc1d1	omezována
a	a	k8xC	a
sabotována	sabotován	k2eAgFnSc1d1	sabotována
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
předmětem	předmět	k1gInSc7	předmět
žárlivosti	žárlivost	k1gFnSc2	žárlivost
a	a	k8xC	a
intrik	intrika	k1gFnPc2	intrika
mladších	mladý	k2eAgMnPc2d2	mladší
kolegů	kolega	k1gMnPc2	kolega
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mistrně	mistrně	k6eAd1	mistrně
zneužili	zneužít	k5eAaPmAgMnP	zneužít
politického	politický	k2eAgInSc2d1	politický
vývoje	vývoj	k1gInSc2	vývoj
v	v	k7c6	v
nedemokratických	demokratický	k2eNgInPc6d1	nedemokratický
poměrech	poměr	k1gInPc6	poměr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
inscenovaně	inscenovaně	k6eAd1	inscenovaně
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
šedé	šedý	k2eAgFnSc2d1	šedá
ekonomiky	ekonomika	k1gFnSc2	ekonomika
a	a	k8xC	a
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Fuchs	Fuchs	k1gMnSc1	Fuchs
zemřel	zemřít	k5eAaPmAgMnS	zemřít
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1972	[number]	k4	1972
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
na	na	k7c4	na
následky	následek	k1gInPc4	následek
vnitřních	vnitřní	k2eAgNnPc2d1	vnitřní
a	a	k8xC	a
četných	četný	k2eAgNnPc2d1	četné
zranění	zranění	k1gNnPc2	zranění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
při	při	k7c6	při
automobilové	automobilový	k2eAgFnSc6d1	automobilová
havárii	havárie	k1gFnSc6	havárie
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Fuchs	Fuchs	k1gMnSc1	Fuchs
byl	být	k5eAaImAgMnS	být
oceněn	ocenit	k5eAaPmNgMnS	ocenit
řadou	řada	k1gFnSc7	řada
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
uznání	uznání	k1gNnSc4	uznání
a	a	k8xC	a
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
prestižních	prestižní	k2eAgNnPc6d1	prestižní
uskupeních	uskupení	k1gNnPc6	uskupení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
je	být	k5eAaImIp3nS	být
Herderova	Herderův	k2eAgFnSc1d1	Herderova
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
čestným	čestný	k2eAgMnSc7d1	čestný
zahraničním	zahraniční	k2eAgMnSc7d1	zahraniční
korespondentem	korespondent	k1gMnSc7	korespondent
Královského	královský	k2eAgInSc2d1	královský
institutu	institut	k1gInSc2	institut
britských	britský	k2eAgMnPc2d1	britský
architektů	architekt	k1gMnPc2	architekt
-	-	kIx~	-
RIBA	RIBA	kA	RIBA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
půdě	půda	k1gFnSc6	půda
byl	být	k5eAaImAgInS	být
dekorován	dekorován	k2eAgInSc1d1	dekorován
titulem	titul	k1gInSc7	titul
Národní	národní	k2eAgMnSc1d1	národní
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
toto	tento	k3xDgNnSc4	tento
ocenění	ocenění	k1gNnSc1	ocenění
získalo	získat	k5eAaPmAgNnS	získat
opět	opět	k6eAd1	opět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
na	na	k7c4	na
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
získalo	získat	k5eAaPmAgNnS	získat
společenský	společenský	k2eAgInSc4d1	společenský
respekt	respekt	k1gInSc4	respekt
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
titul	titul	k1gInSc1	titul
zdiskreditován	zdiskreditovat	k5eAaPmNgInS	zdiskreditovat
<g/>
.	.	kIx.	.
</s>
<s>
Fuchsovi	Fuchs	k1gMnSc3	Fuchs
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
nabídnuta	nabídnut	k2eAgFnSc1d1	nabídnuta
profesura	profesura	k1gFnSc1	profesura
na	na	k7c6	na
Cambridžské	cambridžský	k2eAgFnSc6d1	Cambridžská
universitě	universita	k1gFnSc6	universita
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
studiích	studie	k1gFnPc6	studie
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
na	na	k7c6	na
profesionálním	profesionální	k2eAgInSc6d1	profesionální
základě	základ	k1gInSc6	základ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolutoriu	absolutorium	k1gNnSc6	absolutorium
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
ke	k	k7c3	k
dvěma	dva	k4xCgInPc3	dva
velkým	velký	k2eAgInPc3d1	velký
úkolům	úkol	k1gInPc3	úkol
-	-	kIx~	-
návrhům	návrh	k1gInPc3	návrh
horských	horský	k2eAgInPc2d1	horský
hotelů	hotel	k1gInPc2	hotel
-	-	kIx~	-
Klostermannovy	Klostermannův	k2eAgFnPc1d1	Klostermannova
a	a	k8xC	a
Masarykovy	Masarykův	k2eAgFnPc1d1	Masarykova
chaty	chata	k1gFnPc1	chata
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
realizovaly	realizovat	k5eAaBmAgInP	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
to	ten	k3xDgNnSc1	ten
velké	velký	k2eAgFnPc1d1	velká
stavby	stavba	k1gFnPc1	stavba
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
s	s	k7c7	s
tvrdým	tvrdý	k2eAgNnSc7d1	tvrdé
klimatem	klima	k1gNnSc7	klima
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
byly	být	k5eAaImAgInP	být
koncipovány	koncipovat	k5eAaBmNgInP	koncipovat
v	v	k7c6	v
principu	princip	k1gInSc6	princip
moderně	moderně	k6eAd1	moderně
i	i	k9	i
když	když	k8xS	když
s	s	k7c7	s
rustikálním	rustikální	k2eAgInSc7d1	rustikální
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
navenek	navenek	k6eAd1	navenek
měly	mít	k5eAaImAgFnP	mít
ohleduplný	ohleduplný	k2eAgInSc4d1	ohleduplný
výraz	výraz	k1gInSc4	výraz
<g/>
,	,	kIx,	,
rezonující	rezonující	k2eAgFnPc1d1	rezonující
s	s	k7c7	s
regionálními	regionální	k2eAgFnPc7d1	regionální
stavbami	stavba	k1gFnPc7	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Horské	Horské	k2eAgFnPc1d1	Horské
stavby	stavba	k1gFnPc1	stavba
dodnes	dodnes	k6eAd1	dodnes
nepřestavěny	přestavěn	k2eNgFnPc1d1	přestavěn
stojí	stát	k5eAaImIp3nP	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
epizodní	epizodní	k2eAgFnSc6d1	epizodní
koketerii	koketerie	k1gFnSc6	koketerie
s	s	k7c7	s
národním	národní	k2eAgInSc7d1	národní
dekorativismem	dekorativismus	k1gInSc7	dekorativismus
(	(	kIx(	(
<g/>
vila	vila	k1gFnSc1	vila
Plhákových	Plhákových	k2eAgFnSc1d1	Plhákových
v	v	k7c6	v
Háji	háj	k1gInSc6	háj
u	u	k7c2	u
Mohelnice	Mohelnice	k1gFnSc2	Mohelnice
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Fuchs	Fuchs	k1gMnSc1	Fuchs
soustředil	soustředit	k5eAaPmAgMnS	soustředit
na	na	k7c4	na
profilaci	profilace	k1gFnSc4	profilace
vlastního	vlastní	k2eAgInSc2d1	vlastní
architektonického	architektonický	k2eAgInSc2d1	architektonický
výrazu	výraz	k1gInSc2	výraz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nezadržitelně	zadržitelně	k6eNd1	zadržitelně
blížil	blížit	k5eAaImAgInS	blížit
jak	jak	k8xS	jak
moderní	moderní	k2eAgFnSc6d1	moderní
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
konstrukci	konstrukce	k1gFnSc4	konstrukce
a	a	k8xC	a
formování	formování	k1gNnSc4	formování
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
Le	Le	k1gFnPc4	Le
Corbusierem	Corbusiero	k1gNnSc7	Corbusiero
jen	jen	k9	jen
zcela	zcela	k6eAd1	zcela
nepatrnou	patrný	k2eNgFnSc4d1	patrný
prodlevu	prodleva	k1gFnSc4	prodleva
<g/>
,	,	kIx,	,
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
letmo	letmo	k6eAd1	letmo
jej	on	k3xPp3gMnSc4	on
sledoval	sledovat	k5eAaImAgInS	sledovat
pohroužen	pohroužen	k2eAgInSc1d1	pohroužen
v	v	k7c6	v
kultivaci	kultivace	k1gFnSc6	kultivace
vlastního	vlastní	k2eAgNnSc2d1	vlastní
díla	dílo	k1gNnSc2	dílo
vyvěrajícího	vyvěrající	k2eAgNnSc2d1	vyvěrající
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
originálních	originální	k2eAgFnPc2d1	originální
pohnutek	pohnutka	k1gFnPc2	pohnutka
a	a	k8xC	a
charakteru	charakter	k1gInSc2	charakter
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Klíčová	klíčový	k2eAgFnSc1d1	klíčová
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
stadiu	stadion	k1gNnSc6	stadion
stavba	stavba	k1gFnSc1	stavba
ředitelství	ředitelství	k1gNnPc2	ředitelství
firmy	firma	k1gFnSc2	firma
Tauber	Taubero	k1gNnPc2	Taubero
v	v	k7c6	v
Bystřici	Bystřice	k1gFnSc6	Bystřice
pod	pod	k7c7	pod
Hostýnem	Hostýn	k1gInSc7	Hostýn
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
těchto	tento	k3xDgNnPc2	tento
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
dozrála	dozrát	k5eAaPmAgFnS	dozrát
jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vlastní	vlastní	k2eAgFnSc2d1	vlastní
osobité	osobitý	k2eAgFnSc2d1	osobitá
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
plně	plně	k6eAd1	plně
moderní	moderní	k2eAgFnSc2d1	moderní
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
nejspíš	nejspíš	k9	nejspíš
částečně	částečně	k6eAd1	částečně
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
ranými	raný	k2eAgInPc7d1	raný
projevy	projev	k1gInPc7	projev
Le	Le	k1gMnSc2	Le
Corbusiera	Corbusier	k1gMnSc2	Corbusier
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
a	a	k8xC	a
holandskou	holandský	k2eAgFnSc7d1	holandská
architekturou	architektura	k1gFnSc7	architektura
-	-	kIx~	-
zejména	zejména	k9	zejména
počiny	počin	k1gInPc1	počin
J.	J.	kA	J.
J.	J.	kA	J.
P.	P.	kA	P.
Ouda	oud	k1gMnSc2	oud
-	-	kIx~	-
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
nápadně	nápadně	k6eAd1	nápadně
demonstruje	demonstrovat	k5eAaBmIp3nS	demonstrovat
jeho	jeho	k3xOp3gFnSc1	jeho
vlastní	vlastní	k2eAgFnSc1d1	vlastní
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
nezaměnitelný	zaměnitelný	k2eNgInSc1d1	nezaměnitelný
charakter	charakter	k1gInSc1	charakter
a	a	k8xC	a
"	"	kIx"	"
<g/>
rukopis	rukopis	k1gInSc1	rukopis
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
1923	[number]	k4	1923
<g/>
-	-	kIx~	-
<g/>
1928	[number]	k4	1928
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gNnPc2	jeho
klíčových	klíčový	k2eAgNnPc2d1	klíčové
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vešla	vejít	k5eAaPmAgFnS	vejít
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
české	český	k2eAgFnSc2d1	Česká
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Zemanovu	Zemanův	k2eAgFnSc4d1	Zemanova
kavárnu	kavárna	k1gFnSc4	kavárna
na	na	k7c6	na
Kolišti	koliště	k1gNnSc6	koliště
<g/>
,	,	kIx,	,
Masnou	masný	k2eAgFnSc4d1	Masná
burzu	burza	k1gFnSc4	burza
<g/>
,	,	kIx,	,
hotel	hotel	k1gInSc1	hotel
Avion	avion	k1gInSc1	avion
<g/>
,	,	kIx,	,
obřadní	obřadní	k2eAgFnSc1d1	obřadní
síň	síň	k1gFnSc1	síň
ústředního	ústřední	k2eAgInSc2d1	ústřední
hřbitova	hřbitov	k1gInSc2	hřbitov
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
-	-	kIx~	-
Štýřicích	Štýřice	k1gFnPc6	Štýřice
<g/>
,	,	kIx,	,
penzion	penzion	k1gInSc1	penzion
Radun	Radun	k1gInSc1	Radun
v	v	k7c6	v
Luhačovicích	Luhačovice	k1gFnPc6	Luhačovice
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Fuchse	Fuchs	k1gMnSc2	Fuchs
do	do	k7c2	do
odborného	odborný	k2eAgInSc2d1	odborný
života	život	k1gInSc2	život
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c4	v
mimořádně	mimořádně	k6eAd1	mimořádně
příznivou	příznivý	k2eAgFnSc4d1	příznivá
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
po	po	k7c6	po
studiu	studio	k1gNnSc6	studio
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zdařilo	zdařit	k5eAaPmAgNnS	zdařit
realizovat	realizovat	k5eAaBmF	realizovat
důležité	důležitý	k2eAgFnPc4d1	důležitá
stavby	stavba	k1gFnPc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Úkoly	úkol	k1gInPc1	úkol
-	-	kIx~	-
také	také	k9	také
díky	díky	k7c3	díky
šťastné	šťastný	k2eAgFnSc3d1	šťastná
periodizaci	periodizace	k1gFnSc3	periodizace
-	-	kIx~	-
přicházely	přicházet	k5eAaImAgFnP	přicházet
v	v	k7c6	v
mocném	mocný	k2eAgInSc6d1	mocný
přívalu	příval	k1gInSc6	příval
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mu	on	k3xPp3gMnSc3	on
ani	ani	k9	ani
nezbývalo	zbývat	k5eNaImAgNnS	zbývat
dost	dost	k6eAd1	dost
času	čas	k1gInSc2	čas
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
teoretických	teoretický	k2eAgFnPc2d1	teoretická
úvah	úvaha	k1gFnPc2	úvaha
a	a	k8xC	a
manifestačních	manifestační	k2eAgFnPc2d1	manifestační
doktrín	doktrína	k1gFnPc2	doktrína
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgMnPc7	všecek
pionýry	pionýr	k1gMnPc7	pionýr
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
v	v	k7c6	v
raném	raný	k2eAgNnSc6d1	rané
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
-	-	kIx~	-
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
realizoval	realizovat	k5eAaBmAgMnS	realizovat
několik	několik	k4yIc4	několik
staveb	stavba	k1gFnPc2	stavba
(	(	kIx(	(
<g/>
ředitelství	ředitelství	k1gNnSc2	ředitelství
společnosti	společnost	k1gFnSc2	společnost
Tauber	Taubero	k1gNnPc2	Taubero
<g/>
,	,	kIx,	,
Zemanovu	Zemanův	k2eAgFnSc4d1	Zemanova
kavárnu	kavárna	k1gFnSc4	kavárna
<g/>
,	,	kIx,	,
Masnou	masný	k2eAgFnSc4d1	Masná
burzu	burza	k1gFnSc4	burza
a	a	k8xC	a
obřadní	obřadní	k2eAgFnSc4d1	obřadní
síň	síň	k1gFnSc4	síň
Ústředního	ústřední	k2eAgInSc2d1	ústřední
hřbitova	hřbitov	k1gInSc2	hřbitov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
nedocenitelnou	docenitelný	k2eNgFnSc7d1	nedocenitelná
posilou	posila	k1gFnSc7	posila
pro	pro	k7c4	pro
uplatnění	uplatnění	k1gNnSc4	uplatnění
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
stavby	stavba	k1gFnPc1	stavba
byly	být	k5eAaImAgFnP	být
po	po	k7c6	po
stavebně	stavebně	k6eAd1	stavebně
technické	technický	k2eAgFnSc6d1	technická
stránce	stránka	k1gFnSc6	stránka
velmi	velmi	k6eAd1	velmi
solidní	solidní	k2eAgFnPc1d1	solidní
<g/>
.	.	kIx.	.
</s>
<s>
Nejméně	málo	k6eAd3	málo
dvě	dva	k4xCgFnPc1	dva
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
si	se	k3xPyFc3	se
pořídila	pořídit	k5eAaPmAgFnS	pořídit
respektovaná	respektovaný	k2eAgFnSc1d1	respektovaná
veřejná	veřejný	k2eAgFnSc1d1	veřejná
autorita	autorita	k1gFnSc1	autorita
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
pro	pro	k7c4	pro
legitimitu	legitimita	k1gFnSc4	legitimita
moderní	moderní	k2eAgFnSc2d1	moderní
(	(	kIx(	(
<g/>
funkcionalistické	funkcionalistický	k2eAgFnSc2d1	funkcionalistická
<g/>
)	)	kIx)	)
architektury	architektura	k1gFnSc2	architektura
také	také	k9	také
značný	značný	k2eAgInSc4d1	značný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
živou	živý	k2eAgFnSc7d1	živá
je	být	k5eAaImIp3nS	být
úvaha	úvaha	k1gFnSc1	úvaha
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byl	být	k5eAaImAgMnS	být
Fuchs	Fuchs	k1gMnSc1	Fuchs
více	hodně	k6eAd2	hodně
urbanistou	urbanista	k1gMnSc7	urbanista
než	než	k8xS	než
architektem	architekt	k1gMnSc7	architekt
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
množství	množství	k1gNnSc4	množství
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
a	a	k8xC	a
cítil	cítit	k5eAaImAgMnS	cítit
se	se	k3xPyFc4	se
především	především	k9	především
urbanistou	urbanista	k1gMnSc7	urbanista
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
Kumpoštem	Kumpošt	k1gInSc7	Kumpošt
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
moderního	moderní	k2eAgInSc2d1	moderní
urbanismu	urbanismus	k1gInSc2	urbanismus
<g/>
,	,	kIx,	,
regulačního	regulační	k2eAgNnSc2d1	regulační
a	a	k8xC	a
územního	územní	k2eAgNnSc2d1	územní
plánování	plánování	k1gNnSc2	plánování
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
urbanismus	urbanismus	k1gInSc1	urbanismus
opustil	opustit	k5eAaPmAgInS	opustit
metody	metoda	k1gFnPc4	metoda
monumentálního	monumentální	k2eAgMnSc2d1	monumentální
a	a	k8xC	a
především	především	k6eAd1	především
tvarového	tvarový	k2eAgNnSc2d1	tvarové
určování	určování	k1gNnSc2	určování
(	(	kIx(	(
<g/>
regulování	regulování	k1gNnSc2	regulování
<g/>
)	)	kIx)	)
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
uplatňoval	uplatňovat	k5eAaImAgMnS	uplatňovat
a	a	k8xC	a
rozvíjel	rozvíjet	k5eAaImAgMnS	rozvíjet
metody	metoda	k1gFnPc4	metoda
moderní	moderní	k2eAgFnSc2d1	moderní
komplexního	komplexní	k2eAgNnSc2d1	komplexní
fungovaní	fungovaný	k2eAgMnPc1d1	fungovaný
sídelního	sídelní	k2eAgInSc2d1	sídelní
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Fuchsovo	Fuchsův	k2eAgNnSc4d1	Fuchsovo
urbanistické	urbanistický	k2eAgNnSc4d1	Urbanistické
dílo	dílo	k1gNnSc4	dílo
je	být	k5eAaImIp3nS	být
podobně	podobně	k6eAd1	podobně
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
jako	jako	k9	jako
soubor	soubor	k1gInSc4	soubor
díla	dílo	k1gNnSc2	dílo
architektonického	architektonický	k2eAgNnSc2d1	architektonické
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
zárodky	zárodek	k1gInPc1	zárodek
spadají	spadat	k5eAaPmIp3nP	spadat
rovněž	rovněž	k9	rovněž
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Soutěžní	soutěžní	k2eAgInSc1d1	soutěžní
návrh	návrh	k1gInSc1	návrh
Letné	Letná	k1gFnSc2	Letná
1920	[number]	k4	1920
Soutěžní	soutěžní	k2eAgInSc4d1	soutěžní
urbanistický	urbanistický	k2eAgInSc4d1	urbanistický
návrh	návrh	k1gInSc4	návrh
Klárova	Klárov	k1gInSc2	Klárov
1923	[number]	k4	1923
Regulační	regulační	k2eAgFnSc2d1	regulační
studie	studie	k1gFnSc2	studie
Černých	Černých	k2eAgNnPc2d1	Černých
Polí	pole	k1gNnPc2	pole
1923	[number]	k4	1923
"	"	kIx"	"
<g/>
Tangenta	tangenta	k1gFnSc1	tangenta
<g/>
"	"	kIx"	"
-	-	kIx~	-
soutěžní	soutěžní	k2eAgInSc1d1	soutěžní
návrh	návrh	k1gInSc1	návrh
regulace	regulace	k1gFnSc2	regulace
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
1926-1927	[number]	k4	1926-1927
Organizace	organizace	k1gFnSc2	organizace
obytného	obytný	k2eAgInSc2d1	obytný
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
Světozor	světozor	k1gInSc1	světozor
1929	[number]	k4	1929
<g/>
/	/	kIx~	/
<g/>
82	[number]	k4	82
Radíme	radit	k5eAaImIp1nP	radit
státu	stát	k1gInSc3	stát
jak	jak	k8xC	jak
ušetřit	ušetřit	k5eAaPmF	ušetřit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
15	[number]	k4	15
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
Index	index	k1gInSc1	index
1	[number]	k4	1
1929	[number]	k4	1929
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
Pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
Index	index	k1gInSc1	index
10	[number]	k4	10
1938	[number]	k4	1938
<g/>
/	/	kIx~	/
<g/>
9	[number]	k4	9
Kompozice	kompozice	k1gFnSc2	kompozice
stavby	stavba	k1gFnSc2	stavba
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
1953	[number]	k4	1953
Fuchs	Fuchs	k1gMnSc1	Fuchs
byl	být	k5eAaImAgMnS	být
zejména	zejména	k9	zejména
pohroužen	pohroužen	k2eAgMnSc1d1	pohroužen
do	do	k7c2	do
navrhování	navrhování	k1gNnSc2	navrhování
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
urbanistických	urbanistický	k2eAgInPc2d1	urbanistický
konceptů	koncept	k1gInPc2	koncept
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vázalo	vázat	k5eAaImAgNnS	vázat
většinu	většina	k1gFnSc4	většina
jeho	on	k3xPp3gInSc2	on
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
i	i	k9	i
menší	malý	k2eAgInPc4d2	menší
soubory	soubor	k1gInPc4	soubor
svého	svůj	k3xOyFgInSc2	svůj
nábytku	nábytek	k1gInSc2	nábytek
-	-	kIx~	-
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
především	především	k9	především
nábytek	nábytek	k1gInSc4	nábytek
do	do	k7c2	do
jeho	jeho	k3xOp3gFnPc2	jeho
staveb	stavba	k1gFnPc2	stavba
<g/>
:	:	kIx,	:
vestavěné	vestavěný	k2eAgFnPc1d1	vestavěná
skříně	skříň	k1gFnPc1	skříň
<g/>
,	,	kIx,	,
bufety	bufet	k1gInPc1	bufet
a	a	k8xC	a
komody	komoda	k1gFnPc1	komoda
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
<g/>
,	,	kIx,	,
Fuchs	Fuchs	k1gMnSc1	Fuchs
řešil	řešit	k5eAaImAgMnS	řešit
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
přesností	přesnost	k1gFnSc7	přesnost
a	a	k8xC	a
určitostí	určitost	k1gFnSc7	určitost
i	i	k8xC	i
detaily	detail	k1gInPc7	detail
<g/>
.	.	kIx.	.
</s>
<s>
Vynikajícími	vynikající	k2eAgInPc7d1	vynikající
kusy	kus	k1gInPc7	kus
nábytku	nábytek	k1gInSc2	nábytek
byly	být	k5eAaImAgFnP	být
lavice	lavice	k1gFnPc1	lavice
pro	pro	k7c4	pro
nádražní	nádražní	k2eAgFnSc4d1	nádražní
poštu	pošta	k1gFnSc4	pošta
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Nepochybný	pochybný	k2eNgInSc1d1	nepochybný
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
trubkový	trubkový	k2eAgInSc1d1	trubkový
nábytek	nábytek	k1gInSc1	nábytek
navržený	navržený	k2eAgInSc1d1	navržený
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
lázeňské	lázeňský	k2eAgFnPc4d1	lázeňská
budovy	budova	k1gFnPc4	budova
a	a	k8xC	a
vyrobený	vyrobený	k2eAgInSc4d1	vyrobený
v	v	k7c6	v
Bystřici	Bystřice	k1gFnSc6	Bystřice
pod	pod	k7c7	pod
Hostýnem	Hostýn	k1gInSc7	Hostýn
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
byl	být	k5eAaImAgInS	být
-	-	kIx~	-
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc1	jeden
spolutvůrců	spolutvůrce	k1gMnPc2	spolutvůrce
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
-	-	kIx~	-
mezinárodně	mezinárodně	k6eAd1	mezinárodně
známou	známý	k2eAgFnSc7d1	známá
a	a	k8xC	a
respektovanou	respektovaný	k2eAgFnSc7d1	respektovaná
osobností	osobnost	k1gFnSc7	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
opravdovým	opravdový	k2eAgMnSc7d1	opravdový
(	(	kIx(	(
<g/>
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
přejícím	přející	k2eAgMnSc7d1	přející
<g/>
)	)	kIx)	)
kritikem	kritik	k1gMnSc7	kritik
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
Teige	Teig	k1gInSc2	Teig
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
přidali	přidat	k5eAaPmAgMnP	přidat
-	-	kIx~	-
ale	ale	k8xC	ale
jen	jen	k9	jen
letmými	letmý	k2eAgFnPc7d1	letmá
zmínkami	zmínka	k1gFnPc7	zmínka
-	-	kIx~	-
Henry-Russell	Henry-Russell	k1gInSc1	Henry-Russell
Hitchcock	Hitchcock	k1gInSc1	Hitchcock
<g/>
,	,	kIx,	,
Philip	Philip	k1gMnSc1	Philip
Johnson	Johnson	k1gMnSc1	Johnson
a	a	k8xC	a
J.G.	J.G.	k1gMnSc1	J.G.
<g/>
Watjes	Watjes	k1gMnSc1	Watjes
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
reflexe	reflexe	k1gFnPc1	reflexe
zeslábly	zeslábnout	k5eAaPmAgFnP	zeslábnout
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
domácích	domácí	k2eAgInPc2d1	domácí
vnitrospolečenských	vnitrospolečenský	k2eAgInPc2d1	vnitrospolečenský
zvratů	zvrat	k1gInPc2	zvrat
a	a	k8xC	a
zakrnělé	zakrnělý	k2eAgFnSc2d1	zakrnělá
domácí	domácí	k2eAgFnSc2d1	domácí
publicity	publicita	k1gFnSc2	publicita
<g/>
.	.	kIx.	.
</s>
<s>
Jenom	jenom	k9	jenom
částečně	částečně	k6eAd1	částečně
mohla	moct	k5eAaImAgFnS	moct
napravit	napravit	k5eAaPmF	napravit
tento	tento	k3xDgInSc4	tento
stav	stav	k1gInSc4	stav
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
úrovni	úroveň	k1gFnSc6	úroveň
snaha	snaha	k1gFnSc1	snaha
Borise	Boris	k1gMnSc2	Boris
Podreccy	Podrecca	k1gMnSc2	Podrecca
<g/>
,	,	kIx,	,
Friedricha	Friedrich	k1gMnSc2	Friedrich
Kurrenta	Kurrent	k1gMnSc2	Kurrent
<g/>
,	,	kIx,	,
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Šlapety	Šlapeta	k1gFnSc2	Šlapeta
a	a	k8xC	a
Mihály	Mihála	k1gFnSc2	Mihála
Kubinszkeho	Kubinszke	k1gMnSc2	Kubinszke
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Fuchs	Fuchs	k1gMnSc1	Fuchs
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
a	a	k8xC	a
přijetí	přijetí	k1gNnSc4	přijetí
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
širší	široký	k2eAgInSc4d2	širší
společností	společnost	k1gFnSc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
dokázal	dokázat	k5eAaPmAgInS	dokázat
dát	dát	k5eAaPmF	dát
většině	většina	k1gFnSc3	většina
výrazně	výrazně	k6eAd1	výrazně
moderních	moderní	k2eAgInPc2d1	moderní
a	a	k8xC	a
progresivních	progresivní	k2eAgInPc2d1	progresivní
projektů	projekt	k1gInPc2	projekt
mimořádně	mimořádně	k6eAd1	mimořádně
solidní	solidní	k2eAgFnSc4d1	solidní
stavební	stavební	k2eAgFnSc4d1	stavební
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
že	že	k8xS	že
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
věrohodným	věrohodný	k2eAgInSc7d1	věrohodný
způsobem	způsob	k1gInSc7	způsob
stavění	stavění	k1gNnSc2	stavění
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
že	že	k9	že
dostal	dostat	k5eAaPmAgMnS	dostat
jako	jako	k9	jako
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
městské	městský	k2eAgFnSc2d1	městská
kanceláře	kancelář	k1gFnSc2	kancelář
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
navrhnout	navrhnout	k5eAaPmF	navrhnout
klíčové	klíčový	k2eAgFnPc4d1	klíčová
stavby	stavba	k1gFnPc4	stavba
ve	v	k7c6	v
středobodu	středobod	k1gInSc6	středobod
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
obřadní	obřadní	k2eAgFnSc1d1	obřadní
síň	síň	k1gFnSc1	síň
<g/>
,	,	kIx,	,
masná	masný	k2eAgFnSc1d1	Masná
burza	burza	k1gFnSc1	burza
<g/>
,	,	kIx,	,
pavilon	pavilon	k1gInSc1	pavilon
m.	m.	k?	m.
Brna	Brno	k1gNnSc2	Brno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
dodalo	dodat	k5eAaPmAgNnS	dodat
moderní	moderní	k2eAgFnSc6d1	moderní
architektuře	architektura	k1gFnSc6	architektura
vůbec	vůbec	k9	vůbec
-	-	kIx~	-
v	v	k7c6	v
celosvětoivém	celosvětoivý	k2eAgInSc6d1	celosvětoivý
kontextu	kontext	k1gInSc6	kontext
-	-	kIx~	-
punc	punc	k1gInSc4	punc
věrohodnosti	věrohodnost	k1gFnSc2	věrohodnost
<g/>
,	,	kIx,	,
spolehlivosti	spolehlivost	k1gFnSc2	spolehlivost
a	a	k8xC	a
završilo	završit	k5eAaPmAgNnS	završit
to	ten	k3xDgNnSc1	ten
vítěznou	vítězný	k2eAgFnSc4d1	vítězná
dráhu	dráha	k1gFnSc4	dráha
jejího	její	k3xOp3gInSc2	její
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc4	jeho
nezaměnitelný	zaměnitelný	k2eNgInSc4d1	nezaměnitelný
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc4	konec
války	válka	k1gFnSc2	válka
zastihl	zastihnout	k5eAaPmAgMnS	zastihnout
Fuchse	Fuchs	k1gMnSc4	Fuchs
již	již	k6eAd1	již
jako	jako	k8xS	jako
padesátiletého	padesátiletý	k2eAgMnSc4d1	padesátiletý
muže	muž	k1gMnSc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
přišly	přijít	k5eAaPmAgFnP	přijít
zajímavé	zajímavý	k2eAgInPc4d1	zajímavý
úkoly	úkol	k1gInPc4	úkol
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgFnP	být
spojeny	spojit	k5eAaPmNgInP	spojit
s	s	k7c7	s
obnovou	obnova	k1gFnSc7	obnova
vybombardovaných	vybombardovaný	k2eAgFnPc2d1	vybombardovaná
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgInSc7d3	nejlepší
činem	čin	k1gInSc7	čin
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
je	být	k5eAaImIp3nS	být
přestavba	přestavba	k1gFnSc1	přestavba
Domu	dům	k1gInSc2	dům
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
hrozila	hrozit	k5eAaImAgFnS	hrozit
po	po	k7c6	po
vybombardování	vybombardování	k1gNnSc6	vybombardování
zkáza	zkáza	k1gFnSc1	zkáza
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
krokem	krok	k1gInSc7	krok
byl	být	k5eAaImAgInS	být
návrh	návrh	k1gInSc4	návrh
autobusového	autobusový	k2eAgNnSc2d1	autobusové
nádraží	nádraží	k1gNnSc2	nádraží
s	s	k7c7	s
futuristickou	futuristický	k2eAgFnSc7d1	futuristická
odbavovací	odbavovací	k2eAgFnSc7d1	odbavovací
halou	hala	k1gFnSc7	hala
<g/>
.	.	kIx.	.
</s>
<s>
Realizovány	realizován	k2eAgFnPc1d1	realizována
byly	být	k5eAaImAgFnP	být
jen	jen	k9	jen
střechy	střecha	k1gFnPc1	střecha
nad	nad	k7c7	nad
prvním	první	k4xOgNnSc7	první
nástupištěm	nástupiště	k1gNnSc7	nástupiště
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc4	jejich
tvar	tvar	k1gInSc4	tvar
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
společně	společně	k6eAd1	společně
s	s	k7c7	s
konstruktérem	konstruktér	k1gMnSc7	konstruktér
Konrádem	Konrád	k1gMnSc7	Konrád
Hrubanem	Hruban	k1gMnSc7	Hruban
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
1933	[number]	k4	1933
Praha	Praha	k1gFnSc1	Praha
1934	[number]	k4	1934
Brno	Brno	k1gNnSc4	Brno
1955	[number]	k4	1955
Bystřice	Bystřice	k1gFnSc1	Bystřice
pod	pod	k7c7	pod
Hostýnem	Hostýn	k1gInSc7	Hostýn
1992	[number]	k4	1992
Brno	Brno	k1gNnSc1	Brno
1995	[number]	k4	1995
-	-	kIx~	-
Dům	dům	k1gInSc1	dům
umění	umění	k1gNnSc6	umění
Berlín	Berlín	k1gInSc1	Berlín
1995	[number]	k4	1995
(	(	kIx(	(
<g/>
Kudělka	Kudělka	k1gFnSc1	Kudělka
<g/>
/	/	kIx~	/
<g/>
Kudělková	Kudělková	k1gFnSc1	Kudělková
<g/>
)	)	kIx)	)
Vídeň	Vídeň	k1gFnSc1	Vídeň
2010	[number]	k4	2010
-	-	kIx~	-
Architektur	architektura	k1gFnPc2	architektura
im	im	k?	im
Ringturm	Ringturm	k1gInSc1	Ringturm
(	(	kIx(	(
<g/>
7.12	[number]	k4	7.12
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
18.3	[number]	k4	18.3
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Brno	Brno	k1gNnSc1	Brno
2015	[number]	k4	2015
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
Architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Praha	Praha	k1gFnSc1	Praha
2016	[number]	k4	2016
-	-	kIx~	-
Galerie	galerie	k1gFnSc1	galerie
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Fragnera	Fragner	k1gMnSc2	Fragner
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
-	-	kIx~	-
)	)	kIx)	)
památník	památník	k1gInSc1	památník
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
ulic	ulice	k1gFnPc2	ulice
Lipová	lipový	k2eAgFnSc1d1	Lipová
a	a	k8xC	a
Neumannova	Neumannův	k2eAgFnSc1d1	Neumannova
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
na	na	k7c6	na
rodném	rodný	k2eAgInSc6d1	rodný
domě	dům	k1gInSc6	dům
ve	v	k7c6	v
Všechovicích	Všechovice	k1gFnPc6	Všechovice
pomník	pomník	k1gInSc4	pomník
na	na	k7c6	na
Ústředním	ústřední	k2eAgInSc6d1	ústřední
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
busta	busta	k1gFnSc1	busta
na	na	k7c6	na
VUT	VUT	kA	VUT
(	(	kIx(	(
<g/>
v	v	k7c6	v
přípravě	příprava	k1gFnSc6	příprava
<g/>
)	)	kIx)	)
</s>
