<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Chemnitz	Chemnitz	k1gMnSc1	Chemnitz
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1522	[number]	k4	1522
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1586	[number]	k4	1586
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
předním	přední	k2eAgMnSc7d1	přední
teologem	teolog	k1gMnSc7	teolog
a	a	k8xC	a
církevním	církevní	k2eAgMnSc7d1	církevní
představitelem	představitel	k1gMnSc7	představitel
2	[number]	k4	2
<g/>
.	.	kIx.	.
generace	generace	k1gFnPc1	generace
luteránů	luterán	k1gMnPc2	luterán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lutherské	lutherský	k2eAgFnSc6d1	lutherská
tradici	tradice	k1gFnSc6	tradice
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
Alter	Alter	k1gMnSc1	Alter
Martinus	Martinus	k1gMnSc1	Martinus
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
druhý	druhý	k4xOgMnSc1	druhý
Martin	Martin	k1gMnSc1	Martin
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Si	se	k3xPyFc3	se
Martinus	Martinus	k1gMnSc1	Martinus
non	non	k?	non
fuisset	fuisset	k1gMnSc1	fuisset
<g/>
,	,	kIx,	,
Martinus	Martinus	k1gMnSc1	Martinus
vix	vix	k?	vix
stetisset	stetisset	k1gMnSc1	stetisset
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
kdyby	kdyby	kYmCp3nS	kdyby
nebylo	být	k5eNaImAgNnS	být
Martina	Martina	k1gFnSc1	Martina
[	[	kIx(	[
<g/>
Chemnitze	Chemnitze	k1gFnSc1	Chemnitze
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
[	[	kIx(	[
<g/>
Luther	Luthra	k1gFnPc2	Luthra
<g/>
]	]	kIx)	]
by	by	kYmCp3nS	by
neobstál	obstát	k5eNaPmAgMnS	obstát
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c4	v
Treuenbritzen	Treuenbritzen	k2eAgInSc4d1	Treuenbritzen
v	v	k7c6	v
Braniborsku	Braniborsko	k1gNnSc6	Braniborsko
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
obchodníka	obchodník	k1gMnSc2	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
letech	léto	k1gNnPc6	léto
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
studia	studio	k1gNnPc4	studio
do	do	k7c2	do
Wittenberku	Wittenberk	k1gInSc6	Wittenberk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgMnS	mít
krátkou	krátký	k2eAgFnSc4d1	krátká
příležitost	příležitost	k1gFnSc4	příležitost
slyšet	slyšet	k5eAaImF	slyšet
Lutherova	Lutherův	k2eAgNnPc4d1	Lutherovo
kázání	kázání	k1gNnPc4	kázání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
v	v	k7c6	v
Magdeburgu	Magdeburg	k1gInSc6	Magdeburg
a	a	k8xC	a
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
magistra	magistra	k1gFnSc1	magistra
svobodných	svobodný	k2eAgNnPc2d1	svobodné
umění	umění	k1gNnPc2	umění
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Královci	Královec	k1gInSc6	Královec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pak	pak	k6eAd1	pak
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
knihovník	knihovník	k1gMnSc1	knihovník
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1553	[number]	k4	1553
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
Wittenberku	Wittenberk	k1gInSc2	Wittenberk
studovat	studovat	k5eAaImF	studovat
teologii	teologie	k1gFnSc4	teologie
u	u	k7c2	u
Philippa	Philipp	k1gMnSc2	Philipp
Melanchthona	Melanchthon	k1gMnSc2	Melanchthon
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
záhy	záhy	k6eAd1	záhy
začal	začít	k5eAaPmAgMnS	začít
zastupovat	zastupovat	k5eAaImF	zastupovat
při	při	k7c6	při
některých	některý	k3yIgFnPc6	některý
přednáškách	přednáška	k1gFnPc6	přednáška
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
ordinován	ordinován	k2eAgInSc1d1	ordinován
J.	J.	kA	J.
Bugenhagenem	Bugenhagen	k1gInSc7	Bugenhagen
ke	k	k7c3	k
službě	služba	k1gFnSc3	služba
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
a	a	k8xC	a
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k8xS	jako
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
k	k	k7c3	k
superintendentovi	superintendent	k1gMnSc3	superintendent
Joachimu	Joachim	k1gMnSc3	Joachim
Mörlinovi	Mörlin	k1gMnSc3	Mörlin
v	v	k7c6	v
Braunschweigu	Braunschweig	k1gInSc6	Braunschweig
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
oblasti	oblast	k1gFnSc2	oblast
teologie	teologie	k1gFnSc2	teologie
působil	působit	k5eAaImAgInS	působit
rovněž	rovněž	k9	rovněž
významným	významný	k2eAgInSc7d1	významný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xC	jako
organizátor	organizátor	k1gMnSc1	organizátor
církevního	církevní	k2eAgInSc2d1	církevní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1567	[number]	k4	1567
nahradil	nahradit	k5eAaPmAgInS	nahradit
Mörlina	Mörlin	k2eAgNnPc1d1	Mörlin
na	na	k7c6	na
postu	post	k1gInSc6	post
superintendenta	superintendent	k1gMnSc2	superintendent
a	a	k8xC	a
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
sepsání	sepsání	k1gNnSc4	sepsání
Corpus	corpus	k1gInSc2	corpus
Doctrinae	Doctrinae	k1gNnSc2	Doctrinae
Prutencium	Prutencium	k1gNnSc1	Prutencium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
předstupněm	předstupeň	k1gInSc7	předstupeň
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Formuli	formule	k1gFnSc3	formule
svornosti	svornost	k1gFnSc2	svornost
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
theology	theolog	k1gMnPc7	theolog
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
roku	rok	k1gInSc2	rok
1577	[number]	k4	1577
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byl	být	k5eAaImAgInS	být
završen	završit	k5eAaPmNgInS	završit
kánon	kánon	k1gInSc1	kánon
lutherských	lutherský	k2eAgFnPc2d1	lutherská
"	"	kIx"	"
<g/>
symbolických	symbolický	k2eAgFnPc2d1	symbolická
knih	kniha	k1gFnPc2	kniha
<g/>
"	"	kIx"	"
–	–	k?	–
autoritativních	autoritativní	k2eAgInPc2d1	autoritativní
doktrinálních	doktrinální	k2eAgInPc2d1	doktrinální
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
vydaných	vydaný	k2eAgInPc2d1	vydaný
r.	r.	kA	r.
1580	[number]	k4	1580
jako	jako	k8xC	jako
Kniha	kniha	k1gFnSc1	kniha
svornosti	svornost	k1gFnSc2	svornost
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Braunschweigu	Braunschweig	k1gInSc6	Braunschweig
r.	r.	kA	r.
1586	[number]	k4	1586
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejdůležitějším	důležitý	k2eAgInPc3d3	nejdůležitější
spisům	spis	k1gInPc3	spis
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
Chemnitz	Chemnitz	k1gMnSc1	Chemnitz
publikoval	publikovat	k5eAaBmAgMnS	publikovat
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
teologie	teologie	k1gFnSc2	teologie
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Loci	Loc	k1gMnSc3	Loc
theologici	theologic	k1gMnSc3	theologic
(	(	kIx(	(
<g/>
1554	[number]	k4	1554
<g/>
)	)	kIx)	)
–	–	k?	–
přednášky	přednáška	k1gFnPc4	přednáška
z	z	k7c2	z
dogmatické	dogmatický	k2eAgFnSc2d1	dogmatická
teologie	teologie	k1gFnSc2	teologie
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
jako	jako	k8xC	jako
komentáře	komentář	k1gInPc1	komentář
a	a	k8xC	a
výklady	výklad	k1gInPc1	výklad
k	k	k7c3	k
Melanchthonovým	Melanchthonová	k1gFnPc3	Melanchthonová
Loci	Loc	k1gFnSc2	Loc
communes	communes	k1gMnSc1	communes
</s>
</p>
<p>
<s>
Repititio	Repititio	k6eAd1	Repititio
sanae	sanaat	k5eAaPmIp3nS	sanaat
doctrinae	doctrinae	k1gFnSc4	doctrinae
de	de	k?	de
vera	ver	k2eAgFnSc1d1	vera
praesentia	praesentia	k1gFnSc1	praesentia
corporis	corporis	k1gFnSc2	corporis
et	et	k?	et
sangvinis	sangvinis	k1gFnSc2	sangvinis
Domini	Domin	k1gMnPc1	Domin
in	in	k?	in
Coena	Coena	k1gFnSc1	Coena
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Shrnutí	shrnutí	k1gNnSc1	shrnutí
správného	správný	k2eAgNnSc2d1	správné
učení	učení	k1gNnSc2	učení
o	o	k7c6	o
pravé	pravý	k2eAgFnSc6d1	pravá
přítomnosti	přítomnost	k1gFnSc6	přítomnost
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
krve	krev	k1gFnSc2	krev
Páně	páně	k2eAgFnSc2d1	páně
ve	v	k7c6	v
[	[	kIx(	[
<g/>
svaté	svatý	k2eAgFnSc6d1	svatá
<g/>
]	]	kIx)	]
Večeři	večeře	k1gFnSc6	večeře
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1561	[number]	k4	1561
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
vydání	vydání	k1gNnSc2	vydání
r.	r.	kA	r.
1979	[number]	k4	1979
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Lord	lord	k1gMnSc1	lord
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Supper	Supper	k1gInSc1	Supper
–	–	k?	–
"	"	kIx"	"
<g/>
Večeře	večeře	k1gFnSc1	večeře
Páně	páně	k2eAgFnSc1d1	páně
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
teologický	teologický	k2eAgInSc4d1	teologický
traktát	traktát	k1gInSc4	traktát
o	o	k7c6	o
svátosti	svátost	k1gFnSc6	svátost
Večeře	večeře	k1gFnSc2	večeře
Páně	páně	k2eAgFnSc2d1	páně
<g/>
,	,	kIx,	,
apologeticky	apologeticky	k6eAd1	apologeticky
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
zejména	zejména	k9	zejména
proti	proti	k7c3	proti
kalvínskému	kalvínský	k2eAgNnSc3d1	kalvínské
učení	učení	k1gNnSc3	učení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Examen	examen	k1gNnSc1	examen
Concilii	Concilie	k1gFnSc4	Concilie
Tridentini	Tridentin	k2eAgMnPc1d1	Tridentin
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Zkoumání	zkoumání	k1gNnSc1	zkoumání
Tridentského	tridentský	k2eAgInSc2d1	tridentský
sněmu	sněm	k1gInSc2	sněm
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1566	[number]	k4	1566
<g/>
–	–	k?	–
<g/>
72	[number]	k4	72
<g/>
)	)	kIx)	)
–	–	k?	–
čtyřdílná	čtyřdílný	k2eAgFnSc1d1	čtyřdílná
podrobná	podrobný	k2eAgFnSc1d1	podrobná
a	a	k8xC	a
systematická	systematický	k2eAgFnSc1d1	systematická
kritika	kritika	k1gFnSc1	kritika
římskokatolického	římskokatolický	k2eAgNnSc2d1	římskokatolické
protireformačního	protireformační	k2eAgNnSc2d1	protireformační
učení	učení	k1gNnSc2	učení
přijatého	přijatý	k2eAgNnSc2d1	přijaté
na	na	k7c6	na
Tridentském	tridentský	k2eAgInSc6d1	tridentský
koncilu	koncil	k1gInSc6	koncil
<g/>
.	.	kIx.	.
<g/>
Vedle	vedle	k7c2	vedle
těchto	tento	k3xDgInPc2	tento
textů	text	k1gInPc2	text
Chemnitz	Chemnitz	k1gMnSc1	Chemnitz
napsal	napsat	k5eAaPmAgInS	napsat
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgInPc2d1	další
dogmatických	dogmatický	k2eAgInPc2d1	dogmatický
<g/>
,	,	kIx,	,
apologetických	apologetický	k2eAgInPc2d1	apologetický
<g/>
,	,	kIx,	,
devocionálních	devocionální	k2eAgNnPc2d1	devocionální
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Chemnitz	Chemnitz	k1gInSc1	Chemnitz
je	být	k5eAaImIp3nS	být
přelomovou	přelomový	k2eAgFnSc7d1	přelomová
osobností	osobnost	k1gFnSc7	osobnost
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgFnSc7d1	stojící
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
reformace	reformace	k1gFnSc2	reformace
a	a	k8xC	a
poreformační	poreformační	k2eAgFnSc2d1	poreformační
lutherské	lutherský	k2eAgFnSc2d1	lutherská
orthodoxie	orthodoxie	k1gFnSc2	orthodoxie
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
měrou	míra	k1gFnSc7wR	míra
přispěl	přispět	k5eAaPmAgInS	přispět
ke	k	k7c3	k
stabilizaci	stabilizace	k1gFnSc3	stabilizace
a	a	k8xC	a
hlubšímu	hluboký	k2eAgNnSc3d2	hlubší
propracování	propracování	k1gNnSc3	propracování
lutherské	lutherský	k2eAgFnSc2d1	lutherská
věrouky	věrouka	k1gFnSc2	věrouka
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
prací	práce	k1gFnPc2	práce
na	na	k7c4	na
Formuli	formule	k1gFnSc4	formule
svornosti	svornost	k1gFnSc2	svornost
urovnávající	urovnávající	k2eAgFnSc2d1	urovnávající
vnitrocírkevní	vnitrocírkevní	k2eAgFnSc2d1	vnitrocírkevní
teologické	teologický	k2eAgFnSc2d1	teologická
spory	spora	k1gFnSc2	spora
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
polemickými	polemický	k2eAgFnPc7d1	polemická
diskusemi	diskuse	k1gFnPc7	diskuse
s	s	k7c7	s
římskokatolickými	římskokatolický	k2eAgInPc7d1	římskokatolický
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
jezuitskými	jezuitský	k2eAgFnPc7d1	jezuitská
<g/>
)	)	kIx)	)
a	a	k8xC	a
reformovanými	reformovaný	k2eAgMnPc7d1	reformovaný
teology	teolog	k1gMnPc7	teolog
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgFnSc7	první
formuloval	formulovat	k5eAaImAgMnS	formulovat
specificky	specificky	k6eAd1	specificky
lutherskou	lutherský	k2eAgFnSc4d1	lutherská
nauku	nauka	k1gFnSc4	nauka
o	o	k7c4	o
communicatio	communicatio	k1gNnSc4	communicatio
idiomatum	idiomatum	k1gNnSc1	idiomatum
(	(	kIx(	(
<g/>
o	o	k7c6	o
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
sdílení	sdílení	k1gNnSc6	sdílení
idiomat	idioma	k1gNnPc2	idioma
neboli	neboli	k8xC	neboli
vlastností	vlastnost	k1gFnPc2	vlastnost
Božské	božský	k2eAgFnSc2d1	božská
a	a	k8xC	a
lidské	lidský	k2eAgFnSc2d1	lidská
přirozenosti	přirozenost	k1gFnSc2	přirozenost
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
Kristově	Kristův	k2eAgFnSc6d1	Kristova
osobě	osoba	k1gFnSc6	osoba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
ve	v	k7c6	v
spisu	spis	k1gInSc6	spis
De	De	k?	De
duabus	duabus	k1gInSc1	duabus
naturis	naturis	k1gFnSc1	naturis
in	in	k?	in
Christo	Christa	k1gMnSc5	Christa
z	z	k7c2	z
r.	r.	kA	r.
1571	[number]	k4	1571
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Martin	Martin	k2eAgInSc4d1	Martin
Chemnitz	Chemnitz	k1gInSc4	Chemnitz
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Martin	Martin	k1gMnSc1	Martin
Chemnitz	Chemnitz	k1gMnSc1	Chemnitz
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
