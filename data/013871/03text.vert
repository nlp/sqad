<s>
Sergej	Sergej	k1gMnSc1
Valerjevič	Valerjevič	k1gMnSc1
Prokopjev	Prokopjev	k1gMnSc1
</s>
<s>
Sergej	Sergej	k1gMnSc1
Valerjevič	Valerjevič	k1gMnSc1
Prokopjev	Prokopjev	k1gMnSc1
Sergej	Sergej	k1gMnSc1
ProkopjevCPK	ProkopjevCPK	k1gMnSc1
Státní	státní	k2eAgFnSc4d1
příslušnost	příslušnost	k1gFnSc4
</s>
<s>
Rusko	Rusko	k1gNnSc1
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1975	#num#	k4
(	(	kIx(
<g/>
46	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Jekatěrinburg	Jekatěrinburg	k1gInSc1
Předchozízaměstnání	Předchozízaměstnání	k1gNnSc2
</s>
<s>
Vojenský	vojenský	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
Hodnost	hodnost	k1gFnSc4
</s>
<s>
podplukovník	podplukovník	k1gMnSc1
letectva	letectvo	k1gNnSc2
(	(	kIx(
<g/>
od	od	k7c2
2012	#num#	k4
v	v	k7c6
záloze	záloha	k1gFnSc6
<g/>
)	)	kIx)
Čas	čas	k1gInSc1
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
</s>
<s>
196	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
17	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
49	#num#	k4
minut	minuta	k1gFnPc2
Kosmonaut	kosmonaut	k1gMnSc1
od	od	k7c2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2011	#num#	k4
Mise	mise	k1gFnSc1
</s>
<s>
Expedice	expedice	k1gFnSc1
56	#num#	k4
<g/>
/	/	kIx~
<g/>
57	#num#	k4
(	(	kIx(
<g/>
Sojuz	Sojuz	k1gInSc1
MS-	MS-	k1gFnSc2
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
ISS	ISS	kA
<g/>
)	)	kIx)
Znaky	znak	k1gInPc1
misí	mise	k1gFnPc2
</s>
<s>
Kosmonaut	kosmonaut	k1gMnSc1
do	do	k7c2
</s>
<s>
aktivní	aktivní	k2eAgMnSc1d1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sergej	Sergej	k1gMnSc1
Prokopjev	Prokopjev	k1gMnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
С	С	k?
В	В	k?
П	П	k?
<g/>
;	;	kIx,
*	*	kIx~
19	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1975	#num#	k4
Jekatěrinburg	Jekatěrinburg	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
od	od	k7c2
února	únor	k1gInSc2
2011	#num#	k4
ruským	ruský	k2eAgMnSc7d1
kosmonautem	kosmonaut	k1gMnSc7
<g/>
,	,	kIx,
členem	člen	k1gInSc7
oddílu	oddíl	k1gInSc2
kosmonautů	kosmonaut	k1gMnPc2
Roskosmosu	Roskosmos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
rokem	rok	k1gInSc7
2011	#num#	k4
byl	být	k5eAaImAgInS
pilotem	pilot	k1gMnSc7
ruského	ruský	k2eAgNnSc2d1
vojenského	vojenský	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
jara	jaro	k1gNnSc2
2017	#num#	k4
se	se	k3xPyFc4
připravoval	připravovat	k5eAaImAgMnS
na	na	k7c4
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
kosmický	kosmický	k2eAgInSc4d1
let	let	k1gInSc4
na	na	k7c4
Mezinárodní	mezinárodní	k2eAgFnSc4d1
vesmírnou	vesmírný	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
(	(	kIx(
<g/>
ISS	ISS	kA
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
člen	člen	k1gMnSc1
Expedice	expedice	k1gFnSc2
56	#num#	k4
a	a	k8xC
57	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
proběhl	proběhnout	k5eAaPmAgInS
v	v	k7c6
červnu	červen	k1gInSc6
–	–	k?
prosinci	prosinec	k1gInSc6
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
roku	rok	k1gInSc2
1975	#num#	k4
ve	v	k7c6
městě	město	k1gNnSc6
Sverdlovsk	Sverdlovsk	k1gInSc4
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
Jekatěrinburg	Jekatěrinburg	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
vyrůstal	vyrůstat	k5eAaImAgMnS
a	a	k8xC
vychodil	vychodit	k5eAaPmAgMnS,k5eAaImAgMnS
zde	zde	k6eAd1
i	i	k9
střední	střední	k2eAgFnSc4d1
školu	škola	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
dokončil	dokončit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
navštěvoval	navštěvovat	k5eAaImAgMnS
Tambovskou	Tambovský	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
vysokou	vysoký	k2eAgFnSc4d1
pilotní	pilotní	k2eAgFnSc4d1
leteckou	letecký	k2eAgFnSc4d1
školu	škola	k1gFnSc4
v	v	k7c6
Tambově	Tambův	k2eAgFnSc6d1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
dokončil	dokončit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1997	#num#	k4
s	s	k7c7
kvalifikací	kvalifikace	k1gFnSc7
inženýr-pilot	inženýr-pilota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2002	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
navštěvoval	navštěvovat	k5eAaImAgInS
státní	státní	k2eAgFnSc4d1
zemědělskou	zemědělský	k2eAgFnSc4d1
univerzitu	univerzita	k1gFnSc4
v	v	k7c6
Mičurinsku	Mičurinsko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
získal	získat	k5eAaPmAgMnS
titul	titul	k1gInSc4
v	v	k7c6
oboru	obor	k1gInSc6
ekonomie	ekonomie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
době	doba	k1gFnSc6
od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
sloužil	sloužit	k5eAaImAgMnS
u	u	k7c2
letectva	letectvo	k1gNnSc2
ve	v	k7c6
městech	město	k1gNnPc6
Orsk	Orska	k1gFnPc2
<g/>
,	,	kIx,
Rjazaň	Rjazaň	k1gFnSc1
a	a	k8xC
Vozdviženka	Vozdviženka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
pilotoval	pilotovat	k5eAaImAgMnS
letadla	letadlo	k1gNnPc4
Tupolev	Tupolev	k1gFnSc2
Tu-	Tu-	k1gFnSc1
<g/>
22	#num#	k4
<g/>
M	M	kA
a	a	k8xC
následně	následně	k6eAd1
rok	rok	k1gInSc4
na	na	k7c6
strategickém	strategický	k2eAgInSc6d1
bombardéru	bombardér	k1gInSc6
Tupolev	Tupolev	k1gFnSc2
Tu-	Tu-	k1gFnSc2
<g/>
160	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
má	mít	k5eAaImIp3nS
nalétáno	nalétat	k5eAaBmNgNnS,k5eAaImNgNnS
850	#num#	k4
hodin	hodina	k1gFnPc2
na	na	k7c6
strojích	stroj	k1gInPc6
Tu-	Tu-	k1gFnSc2
<g/>
160	#num#	k4
<g/>
,	,	kIx,
Tu-	Tu-	k1gFnSc1
<g/>
22	#num#	k4
<g/>
M	M	kA
<g/>
,	,	kIx,
Tupolev	Tupolev	k1gFnSc1
Tu-	Tu-	k1gFnSc1
<g/>
134	#num#	k4
<g/>
,	,	kIx,
Jakovlev	Jakovlev	k1gFnSc1
Jak-	Jak-	k1gFnSc1
<g/>
52	#num#	k4
a	a	k8xC
Aero	aero	k1gNnSc4
L-39	L-39	k1gMnSc1
Albatros	albatros	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
2010	#num#	k4
byl	být	k5eAaImAgMnS
vybrán	vybrat	k5eAaPmNgMnS
mezi	mezi	k7c4
ruské	ruský	k2eAgMnPc4d1
kosmonauty	kosmonaut	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgInSc1d1
výcvik	výcvik	k1gInSc1
dokončil	dokončit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
rovněž	rovněž	k9
úspěšně	úspěšně	k6eAd1
složil	složit	k5eAaPmAgMnS
závěrečné	závěrečný	k2eAgFnPc4d1
zkoušky	zkouška	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
k	k	k7c3
dlouhodobému	dlouhodobý	k2eAgInSc3d1
pobytu	pobyt	k1gInSc3
na	na	k7c6
Mezinárodní	mezinárodní	k2eAgFnSc3d1
kosmické	kosmický	k2eAgFnSc3d1
stanici	stanice	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
první	první	k4xOgFnSc7
nominací	nominace	k1gFnSc7
bylo	být	k5eAaImAgNnS
místo	místo	k1gNnSc1
v	v	k7c6
záložní	záložní	k2eAgFnSc6d1
posádce	posádka	k1gFnSc6
Sojuzu	Sojuz	k1gInSc2
TMA-	TMA-	k1gFnSc2
<g/>
18	#num#	k4
<g/>
M.	M.	kA
Později	pozdě	k6eAd2
získal	získat	k5eAaPmAgMnS
letovou	letový	k2eAgFnSc4d1
nominaci	nominace	k1gFnSc4
do	do	k7c2
posádky	posádka	k1gFnSc2
Sojuzu	Sojuz	k1gInSc2
MS-	MS-	k1gFnSc2
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
velitelem	velitel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
by	by	kYmCp3nS
měli	mít	k5eAaImAgMnP
odstartovat	odstartovat	k5eAaPmF
i	i	k9
Oleg	Oleg	k1gMnSc1
Artěmjev	Artěmjev	k1gMnSc1
a	a	k8xC
Andrew	Andrew	k1gMnSc1
Feustel	Feustel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několika	několik	k4yIc2
změnách	změna	k1gFnPc6
byl	být	k5eAaImAgInS
nakonec	nakonec	k6eAd1
v	v	k7c6
dubnu	duben	k1gInSc6
2017	#num#	k4
jmenován	jmenovat	k5eAaImNgInS,k5eAaBmNgInS
do	do	k7c2
záložní	záložní	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
Sojuzu	Sojuz	k1gInSc2
MS-07	MS-07	k1gFnSc2
a	a	k8xC
hlavní	hlavní	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
Sojuzu	Sojuz	k1gInSc2
MS-	MS-	k1gFnSc2
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
vesmíru	vesmír	k1gInSc2
v	v	k7c6
Sojuzu	Sojuz	k1gInSc6
MS-09	MS-09	k1gFnSc2
vzlétl	vzlétnout	k5eAaPmAgInS
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2018	#num#	k4
<g/>
,	,	kIx,
společně	společně	k6eAd1
se	s	k7c7
Serenou	Serena	k1gFnSc7
Auñ	Auñ	k2eAgFnSc7d1
a	a	k8xC
Alexanderem	Alexander	k1gMnSc7
Gerstem	Gerst	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trojice	trojice	k1gFnSc1
kosmonautů	kosmonaut	k1gMnPc2
zamířila	zamířit	k5eAaPmAgFnS
k	k	k7c3
ISS	ISS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
stanici	stanice	k1gFnSc6
pracoval	pracovat	k5eAaImAgMnS
ve	v	k7c6
funkci	funkce	k1gFnSc6
palubního	palubní	k2eAgMnSc2d1
inženýra	inženýr	k1gMnSc2
šest	šest	k4xCc1
měsíců	měsíc	k1gInPc2
<g/>
,	,	kIx,
dvakrát	dvakrát	k6eAd1
vystoupil	vystoupit	k5eAaPmAgMnS
do	do	k7c2
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2018	#num#	k4
se	se	k3xPyFc4
ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
sestavě	sestava	k1gFnSc6
posádka	posádka	k1gFnSc1
Sojuzu	Sojuz	k1gInSc2
MS-09	MS-09	k1gFnSc1
vrátila	vrátit	k5eAaPmAgFnS
na	na	k7c4
Zem	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
let	let	k1gInSc1
trval	trvat	k5eAaImAgInS
196	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
17	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
49	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prokopjev	Prokopjet	k5eAaPmDgInS
je	být	k5eAaImIp3nS
ženatý	ženatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
dceru	dcera	k1gFnSc4
a	a	k8xC
syna	syn	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tituly	titul	k1gInPc1
<g/>
,	,	kIx,
řády	řád	k1gInPc1
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Hrdina	Hrdina	k1gMnSc1
Ruské	ruský	k2eAgFnSc2d1
federace	federace	k1gFnSc2
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Letec-kosmonaut	Letec-kosmonaut	k1gInSc1
Ruské	ruský	k2eAgFnSc2d1
federace	federace	k1gFnSc2
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Řád	řád	k1gInSc1
„	„	k?
<g/>
Za	za	k7c4
vojenské	vojenský	k2eAgFnPc4d1
zásluhy	zásluha	k1gFnPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
IVANOV	IVANOV	kA
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
К	К	k?
э	э	k?
ASTROnote	ASTROnot	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2019-11-17	2019-11-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
С	С	k?
В	В	k?
П	П	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Sergej	Sergej	k1gMnSc1
Valerjevič	Valerjevič	k1gInSc4
Prokopjev	Prokopjev	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1168873088	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
1441153954891005680007	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
</s>
