<s desamb="1">
Stanice	stanice	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
jako	jako	k9
čtvrtá	čtvrtý	k4xOgFnSc1
celoplošná	celoplošný	k2eAgFnSc1d1
a	a	k8xC
volně	volně	k6eAd1
dostupná	dostupný	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
doplnila	doplnit	k5eAaPmAgFnS
„	„	k?
<g/>
velkou	velký	k2eAgFnSc4d1
trojku	trojka	k1gFnSc4
<g/>
“	“	k?
tradičních	tradiční	k2eAgFnPc2d1
amerických	americký	k2eAgFnPc2d1
televizí	televize	k1gFnPc2
(	(	kIx(
<g/>
ABC	ABC	kA
<g/>
,	,	kIx,
CBS	CBS	kA
<g/>
,	,	kIx,
NBC	NBC	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>