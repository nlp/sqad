<s>
Fox	fox	k1gInSc1
Broadcasting	Broadcasting	k1gInSc1
Company	Company	k1gInSc1
</s>
<s>
Fox	fox	k1gInSc1
Broadcasting	Broadcasting	k1gInSc4
Company	Compana	k1gFnSc2
Logo	logo	k1gNnSc4
staniceZahájení	staniceZahájení	k1gNnSc2
vysílání	vysílání	k1gNnSc2
</s>
<s>
19861009	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1986	#num#	k4
Provozovatel	provozovatel	k1gMnSc1
</s>
<s>
Fox	fox	k1gInSc1
Corporation	Corporation	k1gInSc1
Formát	formát	k1gInSc1
obrazu	obraz	k1gInSc3
</s>
<s>
720	#num#	k4
<g/>
p	p	k?
(	(	kIx(
<g/>
HDTV	HDTV	kA
<g/>
)	)	kIx)
Slogan	slogan	k1gInSc1
</s>
<s>
We	We	k?
Are	ar	k1gInSc5
FOX	fox	k1gInSc4
Země	zem	k1gFnSc2
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
New	New	k?
York	York	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
Oblast	oblast	k1gFnSc4
vysílání	vysílání	k1gNnSc2
</s>
<s>
mezinárodní	mezinárodní	k2eAgInSc1d1
Web	web	k1gInSc1
</s>
<s>
www.fox.com	www.fox.com	k1gInSc1
</s>
<s>
Fox	fox	k1gInSc1
Broadcasting	Broadcasting	k1gInSc1
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
běžně	běžně	k6eAd1
zkracovaná	zkracovaný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
Fox	fox	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
americká	americký	k2eAgFnSc1d1
televizní	televizní	k2eAgFnSc1d1
síť	síť	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
součást	součást	k1gFnSc4
skupiny	skupina	k1gFnSc2
Fox	fox	k1gInSc1
Corporation	Corporation	k1gInSc1
mediálního	mediální	k2eAgMnSc2d1
magnáta	magnát	k1gMnSc2
Ruperta	Rupert	k1gMnSc2
Murdocha	Murdoch	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanice	stanice	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
jako	jako	k9
čtvrtá	čtvrtý	k4xOgFnSc1
celoplošná	celoplošný	k2eAgFnSc1d1
a	a	k8xC
volně	volně	k6eAd1
dostupná	dostupný	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
doplnila	doplnit	k5eAaPmAgFnS
„	„	k?
<g/>
velkou	velký	k2eAgFnSc4d1
trojku	trojka	k1gFnSc4
<g/>
“	“	k?
tradičních	tradiční	k2eAgFnPc2d1
amerických	americký	k2eAgFnPc2d1
televizí	televize	k1gFnPc2
(	(	kIx(
<g/>
ABC	ABC	kA
<g/>
,	,	kIx,
CBS	CBS	kA
<g/>
,	,	kIx,
NBC	NBC	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některou	některý	k3yIgFnSc4
ze	z	k7c2
stanic	stanice	k1gFnPc2
sítě	síť	k1gFnSc2
může	moct	k5eAaImIp3nS
přijímat	přijímat	k5eAaImF
až	až	k9
96,18	96,18	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
září	září	k1gNnSc2
2004	#num#	k4
stanice	stanice	k1gFnSc2
vysílá	vysílat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
HDTV	HDTV	kA
kvalitě	kvalita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Zpravodajství	zpravodajství	k1gNnSc1
Fox	fox	k1gInSc1
News	News	k1gInSc4
je	být	k5eAaImIp3nS
známé	známý	k2eAgNnSc1d1
silnou	silný	k2eAgFnSc7d1
podporou	podpora	k1gFnSc7
republikánů	republikán	k1gMnPc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
se	se	k3xPyFc4
odlišuje	odlišovat	k5eAaImIp3nS
od	od	k7c2
většiny	většina	k1gFnSc2
ostatních	ostatní	k2eAgNnPc2d1
amerických	americký	k2eAgNnPc2d1
masmédií	masmédium	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
podporují	podporovat	k5eAaImIp3nP
demokraty	demokrat	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Aktuální	aktuální	k2eAgInSc1d1
program	program	k1gInSc1
</s>
<s>
Animované	animovaný	k2eAgInPc1d1
seriály	seriál	k1gInPc1
</s>
<s>
Simpsonovi	Simpson	k1gMnSc3
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Griffinovi	Griffin	k1gMnSc3
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Bobovy	Bobův	k2eAgFnPc1d1
burgery	burgera	k1gFnPc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Bless	Bless	k1gInSc1
the	the	k?
Harts	Harts	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Duncanville	Duncanville	k1gFnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Teen	Teen	k1gInSc1
Choice	Choice	k1gFnSc2
Awards	Awardsa	k1gFnPc2
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Miss	miss	k1gFnSc1
Universe	Universe	k1gFnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Miss	miss	k1gFnSc1
USA	USA	kA
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
iHeartRadio	iHeartRadio	k1gNnSc1
Music	Musice	k1gFnPc2
Awards	Awardsa	k1gFnPc2
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Drama	drama	k1gNnSc1
</s>
<s>
Empire	empir	k1gInSc5
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Záchranáři	záchranář	k1gMnSc3
L.	L.	kA
A.	A.	kA
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Doktoři	doktor	k1gMnPc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prodigal	Prodigal	k1gInSc1
Son	son	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Almost	Almost	k1gFnSc1
Family	Famila	k1gFnSc2
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Deputy	Deput	k1gInPc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
plamenech	plamen	k1gInPc6
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Komedie	komedie	k1gFnSc1
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1
chlap	chlap	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
,	,	kIx,
přesunuto	přesunut	k2eAgNnSc1d1
ze	z	k7c2
stanice	stanice	k1gFnSc2
ABC	ABC	kA
<g/>
)	)	kIx)
</s>
<s>
Outmatched	Outmatched	k1gInSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zpravodajství	zpravodajství	k1gNnSc1
</s>
<s>
Fox	fox	k1gInSc1
News	Newsa	k1gFnPc2
Sunday	Sundaa	k1gFnSc2
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Reality-show	Reality-show	k?
</s>
<s>
Umíte	umět	k5eAaImIp2nP
tančit	tančit	k5eAaImF
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pekelná	pekelný	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Masterchef	Masterchef	k1gInSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Masterchef	Masterchef	k1gMnSc1
Junior	junior	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Beat	beat	k1gInSc1
Shazam	Shazam	k1gInSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Gordon	Gordon	k1gMnSc1
Ramsay	Ramsaa	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
24	#num#	k4
Hours	Hours	k1gInSc1
to	ten	k3xDgNnSc4
Hell	Hell	k1gMnSc1
and	and	k?
Back	Back	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Masked	Masked	k1gMnSc1
Singer	Singer	k1gMnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Mentel	Mentel	k1gInSc1
Samurai	Samura	k1gFnSc2
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
First	First	k1gFnSc1
Responders	Respondersa	k1gFnPc2
Live	Live	k1gFnPc2
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Spin	spin	k1gInSc1
the	the	k?
Wheel	Wheel	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
What	What	k1gInSc1
Just	just	k6eAd1
Happened	Happened	k1gInSc1
<g/>
??!	??!	k?
with	with	k1gInSc1
Fred	Fred	k1gMnSc1
Savage	Savage	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Flirty	flirt	k1gInPc1
Dancing	dancing	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
LEGO	lego	k1gNnSc1
Masters	Mastersa	k1gFnPc2
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Nadcházející	nadcházející	k2eAgInPc1d1
programy	program	k1gInPc1
</s>
<s>
Animované	animovaný	k2eAgInPc1d1
seriály	seriál	k1gInPc1
</s>
<s>
The	The	k?
Great	Great	k2eAgMnSc1d1
North	North	k1gMnSc1
(	(	kIx(
<g/>
N	N	kA
<g/>
/	/	kIx~
<g/>
A	A	kA
<g/>
)	)	kIx)
</s>
<s>
Housebroken	Housebroken	k1gInSc1
(	(	kIx(
<g/>
N	N	kA
<g/>
/	/	kIx~
<g/>
A	A	kA
<g/>
)	)	kIx)
</s>
<s>
Drama	drama	k1gNnSc1
</s>
<s>
Filthy	Filth	k1gInPc1
Rich	Richa	k1gFnPc2
(	(	kIx(
<g/>
jaro	jaro	k1gNnSc1
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
NeXT	NeXT	k?
(	(	kIx(
<g/>
jaro	jaro	k1gNnSc4
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Komedie	komedie	k1gFnSc1
</s>
<s>
Carla	Carla	k1gFnSc1
(	(	kIx(
<g/>
podzim	podzim	k1gInSc1
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Reality	realita	k1gFnPc1
show	show	k1gFnPc2
</s>
<s>
Ultimate	Ultimat	k1gInSc5
Tag	tag	k1gInSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Masked	Masked	k1gMnSc1
Dancer	Dancer	k1gMnSc1
(	(	kIx(
<g/>
N	N	kA
<g/>
/	/	kIx~
<g/>
A	A	kA
<g/>
)	)	kIx)
</s>
<s>
Big	Big	k?
Bounce	Bounec	k1gInPc1
Battle	Battle	k1gFnSc1
(	(	kIx(
<g/>
N	N	kA
<g/>
/	/	kIx~
<g/>
A	A	kA
<g/>
)	)	kIx)
</s>
<s>
nepojmenovaná	pojmenovaný	k2eNgFnSc1d1
talk	talk	k6eAd1
show	show	k1gNnSc7
Neala	Nealo	k1gNnSc2
Brenanna	Brenanna	k1gFnSc1
(	(	kIx(
<g/>
N	N	kA
<g/>
/	/	kIx~
<g/>
A	A	kA
<g/>
)	)	kIx)
</s>
<s>
Minulé	minulý	k2eAgInPc1d1
programy	program	k1gInPc1
</s>
<s>
24	#num#	k4
hodin	hodina	k1gFnPc2
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
24	#num#	k4
Hodin	hodina	k1gFnPc2
<g/>
:	:	kIx,
Nezastavitelný	zastavitelný	k2eNgMnSc1d1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Akta	akta	k1gNnPc1
X	X	kA
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ally	Ally	k1gInPc1
McBealová	McBealová	k1gFnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
-	-	kIx~
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Anatomie	anatomie	k1gFnSc1
lži	lež	k1gFnSc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
American	American	k1gInSc1
Idol	idol	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Barva	barva	k1gFnSc1
moci	moc	k1gFnSc2
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Beverly	Beverl	k1gInPc1
Hills	Hills	k1gInSc1
90210	#num#	k4
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
-	-	kIx~
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
BH90210	BH90210	k4
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
neznáma	neznámo	k1gNnSc2
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
-	-	kIx~
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Doteky	dotek	k1gInPc1
osudu	osud	k1gInSc2
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
House	house	k1gNnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Drive	drive	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dům	dům	k1gInSc1
loutek	loutka	k1gFnPc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Exorcista	exorcista	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Firefly	Firefnout	k5eAaPmAgFnP
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Futurama	Futurama	k?
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
-	-	kIx~
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hranice	hranice	k1gFnSc1
nemožného	možný	k2eNgInSc2d1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Glee	Glee	k1gFnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gotham	Gotham	k1gInSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
LA	la	k1gNnSc1
to	ten	k3xDgNnSc4
Vegas	Vegas	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lebkouni	Lebkoun	k1gMnPc1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lidský	lidský	k2eAgInSc1d1
terč	terč	k1gInSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lucifer	Lucifer	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Making	Making	k1gInSc1
History	Histor	k1gInPc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Městečko	městečko	k1gNnSc1
Pines	Pinesa	k1gFnPc2
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Melrose	Melrosa	k1gFnSc3
Place	plac	k1gInSc6
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
-	-	kIx~
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Milénium	milénium	k1gNnSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
-	-	kIx~
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nová	nový	k2eAgFnSc1d1
holka	holka	k1gFnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
North	North	k1gMnSc1
Shore	Shor	k1gInSc5
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
-	-	kIx~
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
O.C.	O.C.	k?
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ospalá	ospalý	k2eAgFnSc1d1
díra	díra	k1gFnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Point	pointa	k1gFnPc2
Pleasant	Pleasanta	k1gFnPc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1
chlap	chlap	k1gMnSc1
na	na	k7c6
Zemi	zem	k1gFnSc6
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Proven	Proven	k2eAgInSc1d1
Innocent	Innocent	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Red	Red	k?
Band	band	k1gInSc1
Society	societa	k1gFnSc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rosewood	Rosewood	k1gInSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sběratelé	sběratel	k1gMnPc1
kostí	kost	k1gFnPc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Scream	Scream	k1gInSc1
Queens	Queens	k1gInSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Smrtonosná	smrtonosný	k2eAgFnSc1d1
zbraň	zbraň	k1gFnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Son	son	k1gInSc1
of	of	k?
Zorn	Zorn	k1gInSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Star	Star	kA
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Stoupenci	stoupenec	k1gMnPc1
zla	zlo	k1gNnSc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Terminátor	terminátor	k1gInSc1
<g/>
:	:	kIx,
Příběh	příběh	k1gInSc1
Sáry	Sára	k1gFnSc2
Connorové	Connorová	k1gFnSc2
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Terra	Terra	k1gFnSc1
Nova	nova	k1gFnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Passage	Passage	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Orville	Orville	k1gInSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Útěk	útěk	k1gInSc1
z	z	k7c2
vězení	vězení	k1gNnSc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
2009	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Věřte	věřit	k5eAaImRp2nP
nevěřte	věřit	k5eNaImRp2nP
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Volání	volání	k1gNnSc1
mrtvých	mrtvý	k1gMnPc2
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
-	-	kIx~
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vysněná	vysněný	k2eAgFnSc1d1
meta	meta	k1gFnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Weird	Weird	k1gInSc1
Loners	Loners	k1gInSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Wonderfalls	Wonderfalls	k1gInSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
X-Factor	X-Factor	k1gInSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
X-Men	X-Men	k1gInSc1
<g/>
:	:	kIx,
Nová	nový	k2eAgFnSc1d1
generace	generace	k1gFnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zlatá	zlatá	k1gFnSc1
sedmdesátá	sedmdesátý	k4xOgFnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
-	-	kIx~
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
www.journalism.org	www.journalism.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2010	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
26	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Emily	Emil	k1gMnPc4
VanCamp	VanCamp	k1gInSc1
to	ten	k3xDgNnSc4
Star	star	k1gFnSc1
in	in	k?
Fox	fox	k1gInSc1
Drama	drama	k1gFnSc1
Pilot	pilot	k1gMnSc1
‘	‘	k?
<g/>
The	The	k1gMnSc1
Resident	resident	k1gMnSc1
<g/>
’	’	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SETH	SETH	kA
MACFARLANE	MACFARLANE	kA
TO	to	k9
CREATE	CREATE	kA
<g/>
,	,	kIx,
EXECUTIVE-PRODUCE	EXECUTIVE-PRODUCE	k1gMnPc1
AND	Anda	k1gFnPc2
STAR	Star	kA
IN	IN	kA
NEW	NEW	kA
SERIES	SERIES	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
X-Men	X-Men	k1gInSc1
TV	TV	kA
Series	Series	k1gInSc4
'	'	kIx"
<g/>
Gifted	Gifted	k1gInSc4
<g/>
'	'	kIx"
Official	Official	k1gInSc4
Title	titla	k1gFnSc3
Revealed	Revealed	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Fox	fox	k1gInSc1
Broadcasting	Broadcasting	k1gInSc4
Company	Compana	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Televize	televize	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4246443-2	4246443-2	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
88219099	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
262287583	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
88219099	#num#	k4
</s>
