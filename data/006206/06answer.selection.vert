<s>
V	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
se	se	k3xPyFc4	se
Al	ala	k1gFnPc2	ala
Pacino	Pacino	k1gNnSc4	Pacino
blýskl	blýsknout	k5eAaPmAgInS	blýsknout
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Insomnie	insomnie	k1gFnSc2	insomnie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
veterána	veterán	k1gMnSc4	veterán
losangelské	losangelský	k2eAgFnSc2d1	losangelská
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
thrilleru	thriller	k1gInSc2	thriller
o	o	k7c4	o
CIA	CIA	kA	CIA
Test	test	k1gMnSc1	test
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Recruit	Recruit	k1gMnSc1	Recruit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
adaptaci	adaptace	k1gFnSc6	adaptace
klasické	klasický	k2eAgFnSc2d1	klasická
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
Williama	William	k1gMnSc4	William
Shakespeara	Shakespeare	k1gMnSc4	Shakespeare
<g/>
,	,	kIx,	,
Kupec	kupec	k1gMnSc1	kupec
benátský	benátský	k2eAgMnSc1d1	benátský
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Merchant	Merchant	k1gMnSc1	Merchant
of	of	k?	of
Venice	Venice	k1gFnSc2	Venice
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
Maximální	maximální	k2eAgInSc4d1	maximální
limit	limit	k1gInSc4	limit
(	(	kIx(	(
<g/>
Two	Two	k1gFnPc2	Two
for	forum	k1gNnPc2	forum
the	the	k?	the
Money	Monea	k1gFnSc2	Monea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
postavu	postava	k1gFnSc4	postava
mocného	mocný	k2eAgMnSc2d1	mocný
šéfa	šéf	k1gMnSc2	šéf
obrovské	obrovský	k2eAgFnSc2d1	obrovská
národní	národní	k2eAgFnSc2d1	národní
korporace	korporace	k1gFnSc2	korporace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dává	dávat	k5eAaImIp3nS	dávat
tipy	tip	k1gInPc4	tip
na	na	k7c4	na
sportovní	sportovní	k2eAgFnPc4d1	sportovní
sázky	sázka	k1gFnPc4	sázka
<g/>
.	.	kIx.	.
</s>
