<s>
Členění	členění	k1gNnSc1
Prahy	Praha	k1gFnSc2
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
komplikované	komplikovaný	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
je	být	k5eAaImIp3nS
v	v	k7c6
platnosti	platnost	k1gFnSc6
souběžně	souběžně	k6eAd1
několik	několik	k4yIc1
různých	různý	k2eAgInPc2d1
způsobů	způsob	k1gInPc2
členění	členění	k1gNnSc2
<g/>
:	:	kIx,
katastrální	katastrální	k2eAgInPc4d1
<g/>
,	,	kIx,
státně-územní	státně-územní	k2eAgInPc4d1
<g/>
,	,	kIx,
samosprávné	samosprávný	k2eAgInPc4d1
a	a	k8xC
několik	několik	k4yIc4
úrovní	úroveň	k1gFnPc2
a	a	k8xC
druhů	druh	k1gInPc2
správního	správní	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>