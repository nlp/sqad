<s>
Části	část	k1gFnPc1
Prahy	Praha	k1gFnSc2
</s>
<s>
Členění	členění	k1gNnSc1
Prahyod	Prahyoda	k1gFnPc2
roku	rok	k1gInSc2
1784	#num#	k4
do	do	k7c2
současnosti	současnost	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
Královské	královský	k2eAgNnSc1d1
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
r.	r.	kA
1784	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
vznik	vznik	k1gInSc1
1922	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
části	část	k1gFnPc1
Prahy	Praha	k1gFnSc2
(	(	kIx(
<g/>
od	od	k7c2
r.	r.	kA
1922	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Členění	členění	k1gNnSc1
Prahy	Praha	k1gFnSc2
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
komplikované	komplikovaný	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
je	být	k5eAaImIp3nS
v	v	k7c6
platnosti	platnost	k1gFnSc6
souběžně	souběžně	k6eAd1
několik	několik	k4yIc1
různých	různý	k2eAgInPc2d1
způsobů	způsob	k1gInPc2
členění	členění	k1gNnSc2
<g/>
:	:	kIx,
katastrální	katastrální	k2eAgInPc4d1
<g/>
,	,	kIx,
státně-územní	státně-územní	k2eAgInPc4d1
<g/>
,	,	kIx,
samosprávné	samosprávný	k2eAgInPc4d1
a	a	k8xC
několik	několik	k4yIc4
úrovní	úroveň	k1gFnPc2
a	a	k8xC
druhů	druh	k1gInPc2
správního	správní	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgNnSc1
navzájem	navzájem	k6eAd1
zvláště	zvláště	k6eAd1
v	v	k7c6
centrální	centrální	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
města	město	k1gNnSc2
nekorespondují	korespondovat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranice	hranice	k1gFnSc1
obvodů	obvod	k1gInPc2
se	se	k3xPyFc4
od	od	k7c2
hranic	hranice	k1gFnPc2
katastrálních	katastrální	k2eAgFnPc2d1
území	území	k1gNnSc4
začaly	začít	k5eAaPmAgFnP
vzdalovat	vzdalovat	k5eAaImF
v	v	k7c6
letech	léto	k1gNnPc6
1947	#num#	k4
a	a	k8xC
zejména	zejména	k9
1949	#num#	k4
<g/>
,	,	kIx,
obvody	obvod	k1gInPc1
podle	podle	k7c2
zákona	zákon	k1gInSc2
o	o	k7c6
územním	územní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
začaly	začít	k5eAaPmAgFnP
být	být	k5eAaImF
problematické	problematický	k2eAgFnPc1d1
v	v	k7c6
letech	let	k1gInPc6
1995	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
vytvořilo	vytvořit	k5eAaPmAgNnS
jiný	jiný	k2eAgInSc1d1
systém	systém	k1gInSc1
nejprve	nejprve	k6eAd1
15	#num#	k4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
22	#num#	k4
správních	správní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
zákon	zákon	k1gInSc1
o	o	k7c6
územním	územní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
<g/>
,	,	kIx,
podle	podle	k7c2
nějž	jenž	k3xRgNnSc2
je	být	k5eAaImIp3nS
Praha	Praha	k1gFnSc1
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
10	#num#	k4
obvodů	obvod	k1gInPc2
<g/>
,	,	kIx,
platí	platit	k5eAaImIp3nS
stále	stále	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zrušení	zrušení	k1gNnSc1
tohoto	tento	k3xDgInSc2
zákona	zákon	k1gInSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
nejméně	málo	k6eAd3
dvakrát	dvakrát	k6eAd1
v	v	k7c6
parlamentu	parlament	k1gInSc6
navrženo	navrhnout	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
10	#num#	k4
městských	městský	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
22	#num#	k4
správních	správní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
22	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
57	#num#	k4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
nebo	nebo	k8xC
112	#num#	k4
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Správní	správní	k2eAgNnSc1d1
členění	členění	k1gNnSc1
Prahy	Praha	k1gFnSc2
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Je	být	k5eAaImIp3nS
navrženo	navrhnout	k5eAaPmNgNnS
vyjmutí	vyjmutí	k1gNnSc1
této	tento	k3xDgFnSc2
části	část	k1gFnSc2
článku	článek	k1gInSc2
a	a	k8xC
její	její	k3xOp3gNnSc4
přesunutí	přesunutí	k1gNnSc4
na	na	k7c4
nový	nový	k2eAgInSc4d1
název	název	k1gInSc4
Správní	správní	k2eAgFnSc1d1
dějiny	dějiny	k1gFnPc1
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
návrhu	návrh	k1gInSc3
se	se	k3xPyFc4
můžete	moct	k5eAaImIp2nP
vyjádřit	vyjádřit	k5eAaPmF
v	v	k7c6
diskusi	diskuse	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Královské	královský	k2eAgNnSc1d1
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
1784	#num#	k4
<g/>
–	–	k?
<g/>
1921	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podívejte	podívat	k5eAaImRp2nP,k5eAaPmRp2nP
se	se	k3xPyFc4
také	také	k9
na	na	k7c4
článek	článek	k1gInSc4
Královské	královský	k2eAgNnSc1d1
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1784	#num#	k4
vzniká	vznikat	k5eAaImIp3nS
Královské	královský	k2eAgNnSc4d1
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Praha	Praha	k1gFnSc1
–	–	k?
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
pražské	pražský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Malá	malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
a	a	k8xC
Hradčany	Hradčany	k1gInPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
I	i	k9
až	až	k9
IV	IV	kA
<g/>
)	)	kIx)
sloučeny	sloučen	k2eAgFnPc1d1
dekretem	dekret	k1gInSc7
Josefa	Josef	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
rozhodnutím	rozhodnutí	k1gNnSc7
vlády	vláda	k1gFnSc2
</s>
<s>
1850	#num#	k4
připojen	připojen	k2eAgInSc1d1
Josefov	Josefov	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
V	v	k7c6
<g/>
)	)	kIx)
</s>
<s>
1883	#num#	k4
připojen	připojen	k2eAgInSc1d1
Vyšehrad	Vyšehrad	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
VI	VI	kA
<g/>
)	)	kIx)
(	(	kIx(
<g/>
z	z	k7c2
Kouřimského	kouřimský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
1884	#num#	k4
připojeny	připojen	k2eAgFnPc4d1
Holešovice-Bubny	Holešovice-Bubna	k1gFnPc4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
VII	VII	kA
<g/>
)	)	kIx)
</s>
<s>
1901	#num#	k4
připojena	připojen	k2eAgFnSc1d1
Libeň	Libeň	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
VIII	VIII	kA
<g/>
)	)	kIx)
</s>
<s>
1784	#num#	k4
</s>
<s>
1850	#num#	k4
</s>
<s>
1883	#num#	k4
</s>
<s>
1884	#num#	k4
</s>
<s>
1901	#num#	k4
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1922	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Období	období	k1gNnSc1
</s>
<s>
Obvody	obvod	k1gInPc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
(	(	kIx(
<g/>
přibližné	přibližný	k2eAgNnSc1d1
schéma	schéma	k1gNnSc1
návaznosti	návaznost	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
historické	historický	k2eAgFnPc1d1
</s>
<s>
I.	I.	kA
</s>
<s>
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
V.	V.	kA
</s>
<s>
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s>
XII	XII	kA
<g/>
.	.	kIx.
</s>
<s>
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s>
X.	X.	kA
</s>
<s>
IX	IX	kA
<g/>
.	.	kIx.
</s>
<s>
XI	XI	kA
</s>
<s>
XII	XII	kA
<g/>
.	.	kIx.
</s>
<s>
XX	XX	kA
<g/>
.	.	kIx.
</s>
<s>
XIII	XIII	kA
<g/>
.	.	kIx.
</s>
<s>
XIV	XIV	kA
<g/>
.	.	kIx.
</s>
<s>
XV	XV	kA
<g/>
.	.	kIx.
</s>
<s>
XVI	XVI	kA
<g/>
.	.	kIx.
</s>
<s>
XVII	XVII	kA
<g/>
.	.	kIx.
</s>
<s>
XVIII	XVIII	kA
<g/>
.	.	kIx.
</s>
<s>
XIX	XIX	kA
<g/>
.	.	kIx.
</s>
<s>
1949	#num#	k4
<g/>
–	–	k?
<g/>
1960	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
od	od	k7c2
1960	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
od	od	k7c2
2002	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Vznik	vznik	k1gInSc1
Velké	velký	k2eAgFnSc2d1
Prahy	Praha	k1gFnSc2
</s>
<s>
Vznik	vznik	k1gInSc1
Velké	velký	k2eAgFnSc2d1
Prahy	Praha	k1gFnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Velká	velký	k2eAgFnSc1d1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
Velká	velký	k2eAgFnSc1d1
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
vzniklo	vzniknout	k5eAaPmAgNnS
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1922	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
zákona	zákon	k1gInSc2
č.	č.	k?
114	#num#	k4
<g/>
/	/	kIx~
<g/>
1920	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
z.	z.	k?
a	a	k8xC
n.	n.	k?
připojením	připojení	k1gNnSc7
37	#num#	k4
obcí	obec	k1gFnPc2
a	a	k8xC
osad	osada	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
10	#num#	k4
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připojeno	připojen	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
<g/>
:	:	kIx,
</s>
<s>
8	#num#	k4
obcí	obec	k1gFnPc2
karlínského	karlínský	k2eAgInSc2d1
okresu	okres	k1gInSc2
<g/>
:	:	kIx,
Bohnice	Bohnice	k1gInPc1
<g/>
,	,	kIx,
Hloubětín	Hloubětín	k1gInSc1
<g/>
,	,	kIx,
město	město	k1gNnSc1
Karlín	Karlín	k1gInSc1
<g/>
,	,	kIx,
Kobylisy	Kobylisy	k1gInPc1
<g/>
,	,	kIx,
Prosek	proséct	k5eAaPmDgInS
<g/>
,	,	kIx,
Střížkov	Střížkov	k1gInSc1
<g/>
,	,	kIx,
Troja	Troja	k1gFnSc1
<g/>
,	,	kIx,
město	město	k1gNnSc1
Vysočany	Vysočany	k1gInPc1
</s>
<s>
14	#num#	k4
obcí	obec	k1gFnPc2
smíchovského	smíchovský	k2eAgInSc2d1
okresu	okres	k1gInSc2
<g/>
:	:	kIx,
město	město	k1gNnSc1
Břevnov	Břevnov	k1gInSc1
<g/>
,	,	kIx,
město	město	k1gNnSc1
Bubeneč	Bubeneč	k1gInSc1
<g/>
,	,	kIx,
Dejvice	Dejvice	k1gFnPc1
<g/>
,	,	kIx,
Hlubočepy	Hlubočep	k1gInPc1
<g/>
,	,	kIx,
Jinonice	Jinonice	k1gFnPc1
<g/>
,	,	kIx,
město	město	k1gNnSc1
Košíře	Košíře	k1gInPc1
<g/>
,	,	kIx,
Liboc	Liboc	k1gFnSc1
<g/>
,	,	kIx,
Motol	Motol	k1gInSc1
<g/>
,	,	kIx,
Radlice	radlice	k1gFnSc1
<g/>
,	,	kIx,
Sedlec	Sedlec	k1gInSc1
<g/>
,	,	kIx,
město	město	k1gNnSc1
Smíchov	Smíchov	k1gInSc1
<g/>
,	,	kIx,
Střešovice	Střešovice	k1gFnSc1
<g/>
,	,	kIx,
Veleslavín	Veleslavín	k1gInSc1
<g/>
,	,	kIx,
Vokovice	Vokovice	k1gFnSc1
</s>
<s>
10	#num#	k4
obcí	obec	k1gFnPc2
vinohradského	vinohradský	k2eAgInSc2d1
okresu	okres	k1gInSc2
(	(	kIx(
<g/>
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
současně	současně	k6eAd1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Braník	Braník	k1gInSc1
<g/>
,	,	kIx,
Hodkovičky	Hodkovička	k1gFnPc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
Lhotky	Lhotka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Hostivař	Hostivař	k1gFnSc1
(	(	kIx(
<g/>
bez	bez	k7c2
osad	osada	k1gFnPc2
Milíčov	Milíčovo	k1gNnPc2
a	a	k8xC
Háje	háj	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Krč	Krč	k1gFnSc1
<g/>
,	,	kIx,
Michle	Michl	k1gMnSc5
<g/>
,	,	kIx,
město	město	k1gNnSc1
Nusle	Nusle	k1gFnPc1
<g/>
,	,	kIx,
Podolí	Podolí	k1gNnSc1
<g/>
,	,	kIx,
město	město	k1gNnSc1
Vršovice	Vršovice	k1gFnPc1
<g/>
,	,	kIx,
město	město	k1gNnSc1
Královské	královský	k2eAgInPc1d1
Vinohrady	Vinohrady	k1gInPc1
<g/>
,	,	kIx,
Záběhlice	Záběhlice	k1gFnPc1
</s>
<s>
4	#num#	k4
obce	obec	k1gFnPc4
žižkovského	žižkovský	k2eAgInSc2d1
okresu	okres	k1gInSc2
(	(	kIx(
<g/>
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
současně	současně	k6eAd1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Hrdlořezy	Hrdlořezy	k1gInPc1
<g/>
,	,	kIx,
Malešice	Malešice	k1gFnPc1
<g/>
,	,	kIx,
Staré	Staré	k2eAgFnPc1d1
Strašnice	Strašnice	k1gFnPc1
<g/>
,	,	kIx,
město	město	k1gNnSc1
Žižkov	Žižkov	k1gInSc1
</s>
<s>
2	#num#	k4
části	část	k1gFnSc2
obcí	obec	k1gFnPc2
zbraslavského	zbraslavský	k2eAgInSc2d1
okresu	okres	k1gInSc2
<g/>
:	:	kIx,
osada	osada	k1gFnSc1
Malá	malý	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
obce	obec	k1gFnSc2
Velká	velká	k1gFnSc1
Chuchle	Chuchle	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
část	část	k1gFnSc1
Modřan	Modřany	k1gInPc2
zvaná	zvaný	k2eAgNnPc4d1
Zátiší	zátiší	k1gNnPc4
(	(	kIx(
<g/>
zároveň	zároveň	k6eAd1
připojena	připojen	k2eAgFnSc1d1
k	k	k7c3
Hodkovičkám	Hodkovička	k1gFnPc3
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
I	i	k8xC
–	–	k?
Praha	Praha	k1gFnSc1
XIX	XIX	kA
(	(	kIx(
<g/>
1923	#num#	k4
<g/>
–	–	k?
<g/>
1947	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vládní	vládní	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1923	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
s	s	k7c7
účinností	účinnost	k1gFnSc7
ode	ode	k7c2
dne	den	k1gInSc2
vyhlášení	vyhlášení	k1gNnSc2
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1923	#num#	k4
<g/>
)	)	kIx)
rozdělilo	rozdělit	k5eAaPmAgNnS
Prahu	Praha	k1gFnSc4
do	do	k7c2
19	#num#	k4
obvodů	obvod	k1gInPc2
označených	označený	k2eAgInPc2d1
římskými	římský	k2eAgNnPc7d1
čísly	číslo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
rozdělení	rozdělení	k1gNnSc1
platilo	platit	k5eAaImAgNnS
pouze	pouze	k6eAd1
pro	pro	k7c4
účely	účel	k1gInPc4
voleb	volba	k1gFnPc2
a	a	k8xC
místní	místní	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
státní	státní	k2eAgFnSc4d1
správu	správa	k1gFnSc4
zůstalo	zůstat	k5eAaPmAgNnS
v	v	k7c6
platnosti	platnost	k1gFnSc6
původní	původní	k2eAgNnSc4d1
uspořádání	uspořádání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvodů	obvod	k1gInPc2
bylo	být	k5eAaImAgNnS
vlastně	vlastně	k9
jen	jen	k9
13	#num#	k4
<g/>
,	,	kIx,
protože	protože	k8xS
Praha	Praha	k1gFnSc1
I	i	k8xC
<g/>
–	–	k?
<g/>
VII	VII	kA
tvořila	tvořit	k5eAaImAgFnS
jeden	jeden	k4xCgInSc4
obvod	obvod	k1gInSc4
(	(	kIx(
<g/>
přičemž	přičemž	k6eAd1
pro	pro	k7c4
orientaci	orientace	k1gFnSc4
bylo	být	k5eAaImAgNnS
na	na	k7c6
ulicích	ulice	k1gFnPc6
zachováno	zachovat	k5eAaPmNgNnS
původní	původní	k2eAgNnSc1d1
značení	značení	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
VIII	VIII	kA
byla	být	k5eAaImAgFnS
rozšířena	rozšířit	k5eAaPmNgFnS
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgInPc1d1
obvody	obvod	k1gInPc1
jsou	být	k5eAaImIp3nP
nové	nový	k2eAgMnPc4d1
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
spravovala	spravovat	k5eAaImAgFnS
městská	městský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
<g/>
,	,	kIx,
obvody	obvod	k1gInPc1
obvodní	obvodní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
vládní	vládní	k2eAgNnSc4d1
nařízení	nařízení	k1gNnSc4
č.	č.	k?
45	#num#	k4
<g/>
/	/	kIx~
<g/>
1945	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
zavedlo	zavést	k5eAaPmAgNnS
namísto	namísto	k7c2
nich	on	k3xPp3gMnPc2
ústřední	ústřední	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
a	a	k8xC
místní	místní	k2eAgInPc1d1
národní	národní	k2eAgInPc1d1
výbory	výbor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Praha	Praha	k1gFnSc1
I	i	k8xC
–	–	k?
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
*	*	kIx~
</s>
<s>
Praha	Praha	k1gFnSc1
II	II	kA
–	–	k?
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
*	*	kIx~
</s>
<s>
Praha	Praha	k1gFnSc1
III	III	kA
–	–	k?
Malá	malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
<g/>
*	*	kIx~
</s>
<s>
Praha	Praha	k1gFnSc1
IV	IV	kA
–	–	k?
Hradčany	Hradčany	k1gInPc4
<g/>
*	*	kIx~
</s>
<s>
Praha	Praha	k1gFnSc1
V	v	k7c6
–	–	k?
Josefov	Josefov	k1gInSc1
<g/>
*	*	kIx~
</s>
<s>
Praha	Praha	k1gFnSc1
VI	VI	kA
–	–	k?
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
*	*	kIx~
</s>
<s>
Praha	Praha	k1gFnSc1
VII	VII	kA
–	–	k?
Holešovice-Bubny	Holešovice-Bubna	k1gFnSc2
<g/>
*	*	kIx~
</s>
<s>
Praha	Praha	k1gFnSc1
VIII	VIII	kA
–	–	k?
Libeň	Libeň	k1gFnSc1
<g/>
,	,	kIx,
Střížkov	Střížkov	k1gInSc1
<g/>
,	,	kIx,
Kobylisy	Kobylisy	k1gInPc1
<g/>
,	,	kIx,
Troja	Trojum	k1gNnPc1
<g/>
,	,	kIx,
Bohnice	Bohnice	k1gInPc1
</s>
<s>
Praha	Praha	k1gFnSc1
IX	IX	kA
–	–	k?
Vysočany	Vysočany	k1gInPc1
<g/>
,	,	kIx,
Prosek	proséct	k5eAaPmDgInS
<g/>
,	,	kIx,
Hloubětín	Hloubětín	k1gInSc4
</s>
<s>
Praha	Praha	k1gFnSc1
X	X	kA
–	–	k?
Karlín	Karlín	k1gInSc1
</s>
<s>
Praha	Praha	k1gFnSc1
XI	XI	kA
–	–	k?
Žižkov	Žižkov	k1gInSc1
<g/>
,	,	kIx,
Hrdlořezy	Hrdlořezy	k1gInPc1
<g/>
,	,	kIx,
Malešice	Malešice	k1gFnPc1
</s>
<s>
Praha	Praha	k1gFnSc1
XII	XII	kA
–	–	k?
Královské	královský	k2eAgInPc1d1
Vinohrady	Vinohrady	k1gInPc1
</s>
<s>
Praha	Praha	k1gFnSc1
XIII	XIII	kA
–	–	k?
Vršovice	Vršovice	k1gFnPc4
<g/>
,	,	kIx,
Záběhlice	Záběhlice	k1gFnPc4
<g/>
,	,	kIx,
Hostivař	Hostivař	k1gFnSc1
<g/>
,	,	kIx,
Strašnice	Strašnice	k1gFnPc1
</s>
<s>
Praha	Praha	k1gFnSc1
XIV	XIV	kA
–	–	k?
Nusle	Nusle	k1gFnPc4
<g/>
,	,	kIx,
Michle	Michl	k1gMnSc5
<g/>
,	,	kIx,
Krč	krčit	k5eAaImRp2nS
</s>
<s>
Praha	Praha	k1gFnSc1
XV	XV	kA
–	–	k?
Podolí	Podolí	k1gNnSc1
<g/>
,	,	kIx,
Braník	Braník	k1gInSc1
<g/>
,	,	kIx,
Hodkovičky	Hodkovička	k1gFnPc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
Lhotky	Lhotka	k1gFnSc2
a	a	k8xC
Zátiší	zátiší	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
XVI	XVI	kA
–	–	k?
Smíchov	Smíchov	k1gInSc1
<g/>
,	,	kIx,
Radlice	Radlice	k1gInPc1
<g/>
,	,	kIx,
Hlubočepy	Hlubočep	k1gInPc1
<g/>
,	,	kIx,
Malá	malý	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
XVII	XVII	kA
–	–	k?
Košíře	Košíře	k1gInPc1
<g/>
,	,	kIx,
Motol	Motol	k1gInSc1
<g/>
,	,	kIx,
Jinonice	Jinonice	k1gFnPc1
</s>
<s>
Praha	Praha	k1gFnSc1
XVIII	XVIII	kA
–	–	k?
Břevnov	Břevnov	k1gInSc1
<g/>
,	,	kIx,
Střešovice	Střešovice	k1gFnSc1
<g/>
,	,	kIx,
Liboc	Liboc	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
XIX	XIX	kA
–	–	k?
Dejvice	Dejvice	k1gFnPc1
<g/>
,	,	kIx,
Bubeneč	Bubeneč	k1gInSc1
<g/>
,	,	kIx,
Sedlec	Sedlec	k1gInSc1
<g/>
,	,	kIx,
Vokovice	Vokovice	k1gFnSc1
<g/>
,	,	kIx,
Veleslavín	Veleslavín	k1gMnSc1
</s>
<s>
*	*	kIx~
Pro	pro	k7c4
volební	volební	k2eAgInPc4d1
účely	účel	k1gInPc4
tvořily	tvořit	k5eAaImAgFnP
Praha	Praha	k1gFnSc1
I	i	k8xC
<g/>
–	–	k?
<g/>
VII	VII	kA
jeden	jeden	k4xCgInSc4
obvod	obvod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Praha	Praha	k1gFnSc1
I	i	k8xC
–	–	k?
Praha	Praha	k1gFnSc1
XX	XX	kA
(	(	kIx(
<g/>
1947	#num#	k4
–	–	k?
1949	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vládní	vládní	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
ze	z	k7c2
dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1947	#num#	k4
o	o	k7c6
územním	územní	k2eAgNnSc6d1
rozdělení	rozdělení	k1gNnSc6
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
č.	č.	k?
187	#num#	k4
<g/>
/	/	kIx~
<g/>
1947	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
ustanovilo	ustanovit	k5eAaPmAgNnS
od	od	k7c2
svého	svůj	k3xOyFgNnSc2
vyhlášení	vyhlášení	k1gNnSc2
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1947	#num#	k4
téměř	téměř	k6eAd1
stejné	stejný	k2eAgInPc1d1
obvody	obvod	k1gInPc1
<g/>
,	,	kIx,
jaké	jaký	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
platily	platit	k5eAaImAgInP
od	od	k7c2
roku	rok	k1gInSc2
1923	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
byly	být	k5eAaImAgInP
obvody	obvod	k1gInPc1
pojmenovány	pojmenován	k2eAgInPc1d1
podle	podle	k7c2
hlavní	hlavní	k2eAgFnSc2d1
čtvrtě	čtvrt	k1gFnSc2
a	a	k8xC
staly	stát	k5eAaPmAgFnP
se	se	k3xPyFc4
též	též	k9
obvody	obvod	k1gInPc1
pro	pro	k7c4
státní	státní	k2eAgFnSc4d1
správu	správa	k1gFnSc4
<g/>
,	,	kIx,
obvody	obvod	k1gInPc4
I	i	k9
až	až	k6eAd1
VII	VII	kA
byly	být	k5eAaImAgFnP
opět	opět	k6eAd1
samostatné	samostatný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
XX	XX	kA
byla	být	k5eAaImAgFnS
vyčleněna	vyčlenit	k5eAaPmNgFnS
z	z	k7c2
obvodu	obvod	k1gInSc2
Praha	Praha	k1gFnSc1
XIII	XIII	kA
<g/>
,	,	kIx,
Záběhlice	Záběhlice	k1gFnPc1
přitom	přitom	k6eAd1
byly	být	k5eAaImAgFnP
roztrženy	roztržen	k2eAgFnPc1d1
do	do	k7c2
dvou	dva	k4xCgInPc2
obvodů	obvod	k1gInPc2
–	–	k?
Zahradní	zahradní	k2eAgNnSc1d1
Město	město	k1gNnSc1
se	se	k3xPyFc4
ocitlo	ocitnout	k5eAaPmAgNnS
v	v	k7c6
jiném	jiný	k2eAgInSc6d1
obvodu	obvod	k1gInSc6
než	než	k8xS
zbytek	zbytek	k1gInSc1
Záběhlic	Záběhlice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Praha	Praha	k1gFnSc1
I	i	k9
až	až	k9
Praha	Praha	k1gFnSc1
VII	VII	kA
jako	jako	k8xS,k8xC
dříve	dříve	k6eAd2
</s>
<s>
Praha	Praha	k1gFnSc1
VIII	VIII	kA
–	–	k?
Libeň	Libeň	k1gFnSc1
(	(	kIx(
<g/>
Libeň	Libeň	k1gFnSc1
<g/>
,	,	kIx,
Střížkov	Střížkov	k1gInSc1
<g/>
,	,	kIx,
Kobylisy	Kobylisy	k1gInPc1
<g/>
,	,	kIx,
Troja	Trojum	k1gNnPc1
<g/>
,	,	kIx,
Bohnice	Bohnice	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
IX	IX	kA
–	–	k?
Vysočany	Vysočany	k1gInPc4
(	(	kIx(
<g/>
Vysočany	Vysočany	k1gInPc4
<g/>
,	,	kIx,
Prosek	proséct	k5eAaPmDgInS
<g/>
,	,	kIx,
Hloubětín	Hloubětín	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
X	X	kA
–	–	k?
Karlín	Karlín	k1gInSc1
</s>
<s>
Praha	Praha	k1gFnSc1
XI	XI	kA
–	–	k?
Žižkov	Žižkov	k1gInSc1
(	(	kIx(
<g/>
Žižkov	Žižkov	k1gInSc1
<g/>
,	,	kIx,
Hrdlořezy	Hrdlořezy	k1gInPc1
<g/>
,	,	kIx,
Malešice	Malešice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
XII	XII	kA
–	–	k?
Královské	královský	k2eAgInPc1d1
Vinohrady	Vinohrady	k1gInPc1
</s>
<s>
Praha	Praha	k1gFnSc1
XIII	XIII	kA
–	–	k?
Vršovice	Vršovice	k1gFnPc1
(	(	kIx(
<g/>
Vršovice	Vršovice	k1gFnPc1
<g/>
,	,	kIx,
Záběhlice	Záběhlice	k1gFnPc1
bez	bez	k7c2
Zahradního	zahradní	k2eAgNnSc2d1
Města	město	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
XIV	XIV	kA
–	–	k?
Nusle	Nusle	k1gFnPc4
(	(	kIx(
<g/>
Nusle	Nusle	k1gFnPc4
<g/>
,	,	kIx,
Michle	Michl	k1gMnSc5
<g/>
,	,	kIx,
Krč	krčit	k5eAaImRp2nS
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
XV	XV	kA
–	–	k?
Braník	Braník	k1gInSc1
(	(	kIx(
<g/>
Podolí	Podolí	k1gNnSc1
<g/>
,	,	kIx,
Braník	Braník	k1gInSc1
<g/>
,	,	kIx,
Hodkovičky	Hodkovička	k1gFnPc1
včetně	včetně	k7c2
Lhotky	Lhotka	k1gFnSc2
a	a	k8xC
Zátiší	zátiší	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
XVI	XVI	kA
–	–	k?
Smíchov	Smíchov	k1gInSc1
(	(	kIx(
<g/>
Smíchov	Smíchov	k1gInSc1
<g/>
,	,	kIx,
Radlice	Radlice	k1gInPc1
<g/>
,	,	kIx,
Hlubočepy	Hlubočep	k1gInPc1
<g/>
,	,	kIx,
Malá	malý	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
XVII	XVII	kA
–	–	k?
Košíře	Košíře	k1gInPc4
(	(	kIx(
<g/>
Košíře	Košíře	k1gInPc4
<g/>
,	,	kIx,
Motol	Motol	k1gInSc1
<g/>
,	,	kIx,
Jinonice	Jinonice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
XVIII	XVIII	kA
–	–	k?
Břevnov	Břevnov	k1gInSc1
(	(	kIx(
<g/>
Břevnov	Břevnov	k1gInSc1
<g/>
,	,	kIx,
Střešovice	Střešovice	k1gFnSc1
<g/>
,	,	kIx,
Dolní	dolní	k2eAgFnSc1d1
Liboc	Liboc	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
XIX	XIX	kA
–	–	k?
Dejvice	Dejvice	k1gFnPc1
(	(	kIx(
<g/>
Dejvice	Dejvice	k1gFnPc1
<g/>
,	,	kIx,
Bubeneč	Bubeneč	k1gInSc1
<g/>
,	,	kIx,
Sedlec	Sedlec	k1gInSc1
<g/>
,	,	kIx,
Vokovice	Vokovice	k1gFnSc1
<g/>
,	,	kIx,
Veleslavín	Veleslavín	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
XX	XX	kA
–	–	k?
Strašnice	Strašnice	k1gFnPc4
(	(	kIx(
<g/>
Hostivař	Hostivař	k1gFnSc1
<g/>
,	,	kIx,
Staré	Staré	k2eAgFnPc1d1
Strašnice	Strašnice	k1gFnPc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
Záběhlic	Záběhlice	k1gFnPc2
s	s	k7c7
názvem	název	k1gInSc7
Zahradní	zahradní	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
1	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
16	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1949	#num#	k4
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1960	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vládní	vládní	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
č.	č.	k?
79	#num#	k4
<g/>
/	/	kIx~
<g/>
1949	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
zavedlo	zavést	k5eAaPmAgNnS
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1949	#num#	k4
zcela	zcela	k6eAd1
nové	nový	k2eAgNnSc4d1
členění	členění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
masově	masově	k6eAd1
přestaly	přestat	k5eAaPmAgFnP
být	být	k5eAaImF
respektovány	respektován	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
katastrálního	katastrální	k2eAgNnSc2d1
členění	členění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
označování	označování	k1gNnSc4
obvodů	obvod	k1gInPc2
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
používat	používat	k5eAaImF
arabská	arabský	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
číslovaných	číslovaný	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
se	se	k3xPyFc4
snížil	snížit	k5eAaPmAgInS
z	z	k7c2
20	#num#	k4
na	na	k7c4
16	#num#	k4
(	(	kIx(
<g/>
fakticky	fakticky	k6eAd1
se	se	k3xPyFc4
počet	počet	k1gInSc1
obvodů	obvod	k1gInPc2
zvýšil	zvýšit	k5eAaPmAgInS
ze	z	k7c2
14	#num#	k4
na	na	k7c4
16	#num#	k4
<g/>
,	,	kIx,
protože	protože	k8xS
dosavadních	dosavadní	k2eAgInPc2d1
prvních	první	k4xOgInPc2
7	#num#	k4
obvodů	obvod	k1gInPc2
bylo	být	k5eAaImAgNnS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
jediným	jediný	k2eAgInSc7d1
obvodem	obvod	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
žádný	žádný	k3yNgInSc1
nový	nový	k2eAgInSc1d1
obvod	obvod	k1gInSc1
nebyl	být	k5eNaImAgInS
vymezením	vymezení	k1gNnSc7
totožný	totožný	k2eAgInSc4d1
s	s	k7c7
předchozím	předchozí	k2eAgInSc7d1
<g/>
,	,	kIx,
nové	nový	k2eAgNnSc1d1
číslování	číslování	k1gNnSc1
si	se	k3xPyFc3
zachovalo	zachovat	k5eAaPmAgNnS
jistou	jistý	k2eAgFnSc4d1
míru	míra	k1gFnSc4
návaznosti	návaznost	k1gFnSc2
na	na	k7c4
předchozí	předchozí	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
:	:	kIx,
nové	nový	k2eAgInPc1d1
obvody	obvod	k1gInPc1
s	s	k7c7
čísly	číslo	k1gNnPc7
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
<g/>
,	,	kIx,
11	#num#	k4
<g/>
,	,	kIx,
12	#num#	k4
<g/>
,	,	kIx,
13	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
14	#num#	k4
<g/>
,	,	kIx,
15	#num#	k4
a	a	k8xC
16	#num#	k4
lze	lze	k6eAd1
považovat	považovat	k5eAaImF
za	za	k7c2
nástupce	nástupce	k1gMnSc2
obvodů	obvod	k1gInPc2
označených	označený	k2eAgInPc2d1
shodným	shodný	k2eAgNnSc7d1
římským	římský	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
<g/>
,	,	kIx,
dosavadní	dosavadní	k2eAgInPc1d1
obvody	obvod	k1gInPc1
III	III	kA
<g/>
,	,	kIx,
IV	IV	kA
a	a	k8xC
V	V	kA
byly	být	k5eAaImAgFnP
v	v	k7c6
podstatě	podstata	k1gFnSc6
začleněny	začleněn	k2eAgInPc1d1
do	do	k7c2
prvního	první	k4xOgInSc2
obvodu	obvod	k1gInSc2
a	a	k8xC
obvod	obvod	k1gInSc1
VI	VI	kA
do	do	k7c2
druhého	druhý	k4xOgInSc2
obvodu	obvod	k1gInSc2
<g/>
,	,	kIx,
u	u	k7c2
zbývajících	zbývající	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
lze	lze	k6eAd1
hovořit	hovořit	k5eAaImF
o	o	k7c4
přečíslování	přečíslování	k1gNnSc4
(	(	kIx(
<g/>
X	X	kA
na	na	k7c4
3	#num#	k4
<g/>
,	,	kIx,
XVII	XVII	kA
na	na	k7c4
4	#num#	k4
<g/>
,	,	kIx,
XVIII	XVIII	kA
na	na	k7c4
5	#num#	k4
<g/>
,	,	kIx,
XIX	XIX	kA
na	na	k7c4
6	#num#	k4
<g/>
,	,	kIx,
XX	XX	kA
na	na	k7c4
10	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Obvodní	obvodní	k2eAgFnPc1d1
rady	rada	k1gFnPc1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
obvodními	obvodní	k2eAgFnPc7d1
národními	národní	k2eAgFnPc7d1
výbory	výbor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Praha	Praha	k1gFnSc1
1	#num#	k4
–	–	k?
Josefov	Josefov	k1gInSc1
<g/>
,	,	kIx,
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
Malá	malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
<g/>
,	,	kIx,
část	část	k1gFnSc4
Holešovic-Buben	Holešovic-Buben	k2eAgMnSc1d1
a	a	k8xC
Hradčan	Hradčany	k1gInPc2
</s>
<s>
Praha	Praha	k1gFnSc1
2	#num#	k4
–	–	k?
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
Nového	Nového	k2eAgNnSc2d1
Města	město	k1gNnSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
Nuslí	Nusle	k1gFnPc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
Královských	královský	k2eAgInPc2d1
Vinohrad	Vinohrady	k1gInPc2
</s>
<s>
Praha	Praha	k1gFnSc1
3	#num#	k4
–	–	k?
Karlín	Karlín	k1gInSc1
(	(	kIx(
<g/>
vyjma	vyjma	k7c2
Rustonky	Rustonka	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
část	část	k1gFnSc1
Nového	Nového	k2eAgNnSc2d1
Města	město	k1gNnSc2
</s>
<s>
Praha	Praha	k1gFnSc1
4	#num#	k4
–	–	k?
Jinonice	Jinonice	k1gFnPc1
<g/>
,	,	kIx,
Košíře	Košíře	k1gInPc1
<g/>
,	,	kIx,
Motol	Motol	k1gInSc1
a	a	k8xC
část	část	k1gFnSc1
Smíchova	Smíchov	k1gInSc2
</s>
<s>
Praha	Praha	k1gFnSc1
5	#num#	k4
–	–	k?
Břevnov	Břevnov	k1gInSc1
<g/>
,	,	kIx,
části	část	k1gFnPc1
Dolní	dolní	k2eAgFnPc1d1
Liboce	Liboc	k1gFnPc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
Střešovic	Střešovice	k1gFnPc2
</s>
<s>
Praha	Praha	k1gFnSc1
6	#num#	k4
–	–	k?
Dejvice	Dejvice	k1gFnPc1
<g/>
,	,	kIx,
Sedlec	Sedlec	k1gInSc1
<g/>
,	,	kIx,
Veleslavín	Veleslavín	k1gInSc1
<g/>
,	,	kIx,
Vokovice	Vokovice	k1gFnSc1
a	a	k8xC
části	část	k1gFnSc2
Holešovic-Buben	Holešovic-Buben	k2eAgMnSc1d1
<g/>
,	,	kIx,
Hradčan	Hradčany	k1gInPc2
<g/>
,	,	kIx,
Dolní	dolní	k2eAgFnSc2d1
Liboce	Liboc	k1gFnSc2
a	a	k8xC
Střešovic	Střešovice	k1gFnPc2
</s>
<s>
Praha	Praha	k1gFnSc1
7	#num#	k4
–	–	k?
části	část	k1gFnSc2
Bubenče	Bubeneč	k1gFnSc2
<g/>
,	,	kIx,
Holešovic-Buben	Holešovic-Buben	k2eAgMnSc1d1
<g/>
,	,	kIx,
Libně	libně	k6eAd1
a	a	k8xC
Troje	troje	k4xRgInPc4
</s>
<s>
Praha	Praha	k1gFnSc1
8	#num#	k4
–	–	k?
Bohnice	Bohnice	k1gInPc1
<g/>
,	,	kIx,
Kobylisy	Kobylisy	k1gInPc1
<g/>
,	,	kIx,
Střížkov	Střížkov	k1gInSc1
<g/>
,	,	kIx,
části	část	k1gFnPc1
Karlína	Karlín	k1gInSc2
<g/>
,	,	kIx,
Libně	libně	k6eAd1
a	a	k8xC
Troje	troje	k4xRgInPc4
</s>
<s>
Praha	Praha	k1gFnSc1
9	#num#	k4
–	–	k?
Hloubětín	Hloubětín	k1gInSc1
<g/>
,	,	kIx,
Hrdlořezy	Hrdlořezy	k1gInPc1
<g/>
,	,	kIx,
Prosek	proséct	k5eAaPmDgInS
<g/>
,	,	kIx,
části	část	k1gFnSc3
Libně	Libeň	k1gFnSc2
a	a	k8xC
Vysočan	Vysočany	k1gInPc2
</s>
<s>
Praha	Praha	k1gFnSc1
10	#num#	k4
–	–	k?
Hostivař	Hostivař	k1gFnSc1
<g/>
,	,	kIx,
Malešice	Malešice	k1gFnPc1
<g/>
,	,	kIx,
Staré	Staré	k2eAgFnPc1d1
Strašnice	Strašnice	k1gFnPc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
Záběhlic	Záběhlice	k1gFnPc2
(	(	kIx(
<g/>
Zahradní	zahradní	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
11	#num#	k4
–	–	k?
Žižkov	Žižkov	k1gInSc1
a	a	k8xC
část	část	k1gFnSc1
Vysočan	Vysočany	k1gInPc2
</s>
<s>
Praha	Praha	k1gFnSc1
12	#num#	k4
–	–	k?
část	část	k1gFnSc1
Královských	královský	k2eAgInPc2d1
Vinohrad	Vinohrady	k1gInPc2
</s>
<s>
Praha	Praha	k1gFnSc1
13	#num#	k4
–	–	k?
Vršovice	Vršovice	k1gFnPc1
a	a	k8xC
části	část	k1gFnPc1
Michle	Michl	k1gMnSc5
a	a	k8xC
Záběhlic	Záběhlice	k1gFnPc2
</s>
<s>
Praha	Praha	k1gFnSc1
14	#num#	k4
–	–	k?
části	část	k1gFnSc2
Krče	Krč	k1gFnSc2
<g/>
,	,	kIx,
Lhotky	Lhotka	k1gFnSc2
<g/>
,	,	kIx,
Nových	Nových	k2eAgInPc2d1
Dvorů	Dvůr	k1gInPc2
<g/>
,	,	kIx,
Hodkoviček	Hodkovička	k1gFnPc2
<g/>
,	,	kIx,
Michle	Michl	k1gMnSc5
a	a	k8xC
Nuslí	Nusle	k1gFnPc2
</s>
<s>
Praha	Praha	k1gFnSc1
15	#num#	k4
–	–	k?
Braník	Braník	k1gInSc1
<g/>
,	,	kIx,
Podolí	Podolí	k1gNnSc4
<g/>
,	,	kIx,
části	část	k1gFnPc4
Hodkoviček	Hodkovička	k1gFnPc2
a	a	k8xC
Krče	Krč	k1gFnSc2
</s>
<s>
Praha	Praha	k1gFnSc1
16	#num#	k4
–	–	k?
Hlubočepy	Hlubočepa	k1gFnSc2
<g/>
,	,	kIx,
Malá	malý	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
<g/>
,	,	kIx,
Radlice	radlice	k1gFnSc1
a	a	k8xC
část	část	k1gFnSc1
Smíchova	Smíchov	k1gInSc2
</s>
<s>
Obvody	obvod	k1gInPc1
Praha	Praha	k1gFnSc1
1	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
10	#num#	k4
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Podle	podle	k7c2
§	§	k?
2	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
36	#num#	k4
<g/>
/	/	kIx~
<g/>
1960	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
územním	územní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
ode	ode	k7c2
dne	den	k1gInSc2
vyhlášení	vyhlášení	k1gNnSc2
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1960	#num#	k4
<g/>
)	)	kIx)
počet	počet	k1gInSc4
obvodů	obvod	k1gInPc2
zredukován	zredukován	k2eAgInSc4d1
na	na	k7c4
deset	deset	k4xCc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
s	s	k7c7
názvy	název	k1gInPc7
Praha	Praha	k1gFnSc1
1	#num#	k4
až	až	k8xS
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
obvodů	obvod	k1gInPc2
č.	č.	k?
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
a	a	k8xC
9	#num#	k4
lze	lze	k6eAd1
mluvit	mluvit	k5eAaImF
o	o	k7c6
úpravách	úprava	k1gFnPc6
území	území	k1gNnSc2
původních	původní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
,	,	kIx,
dosavadní	dosavadní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
Praha	Praha	k1gFnSc1
11	#num#	k4
byl	být	k5eAaImAgInS
v	v	k7c6
podstatě	podstata	k1gFnSc6
přečíslován	přečíslovat	k5eAaPmNgMnS
na	na	k7c4
Prahu	Praha	k1gFnSc4
3	#num#	k4
<g/>
,	,	kIx,
dosavadní	dosavadní	k2eAgFnSc1d1
Praha	Praha	k1gFnSc1
3	#num#	k4
(	(	kIx(
<g/>
Karlín	Karlín	k1gInSc1
<g/>
)	)	kIx)
včleněna	včleněn	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
do	do	k7c2
obvodu	obvod	k1gInSc2
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
,	,	kIx,
dosavadní	dosavadní	k2eAgFnSc1d1
Praha	Praha	k1gFnSc1
5	#num#	k4
(	(	kIx(
<g/>
Břevnov	Břevnov	k1gInSc1
<g/>
)	)	kIx)
do	do	k7c2
Prahy	Praha	k1gFnSc2
6	#num#	k4
<g/>
,	,	kIx,
dosavadní	dosavadní	k2eAgInPc1d1
obvody	obvod	k1gInPc1
Praha	Praha	k1gFnSc1
16	#num#	k4
a	a	k8xC
Praha	Praha	k1gFnSc1
4	#num#	k4
sloučeny	sloučen	k2eAgInPc1d1
do	do	k7c2
nového	nový	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
,	,	kIx,
dosavadní	dosavadní	k2eAgInPc1d1
obvody	obvod	k1gInPc1
Praha	Praha	k1gFnSc1
13	#num#	k4
a	a	k8xC
Praha	Praha	k1gFnSc1
10	#num#	k4
sloučeny	sloučen	k2eAgInPc1d1
do	do	k7c2
nového	nový	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
,	,	kIx,
dosavadní	dosavadní	k2eAgInPc1d1
obvody	obvod	k1gInPc1
Praha	Praha	k1gFnSc1
14	#num#	k4
a	a	k8xC
Praha	Praha	k1gFnSc1
15	#num#	k4
sloučeny	sloučen	k2eAgInPc1d1
do	do	k7c2
nového	nový	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
Praha	Praha	k1gFnSc1
4	#num#	k4
a	a	k8xC
dosavadní	dosavadní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
Praha	Praha	k1gFnSc1
12	#num#	k4
(	(	kIx(
<g/>
Král	Král	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vinohrady	Vinohrady	k1gInPc4
<g/>
)	)	kIx)
rozdělen	rozdělit	k5eAaPmNgInS
mezi	mezi	k7c4
obvody	obvod	k1gInPc4
Praha	Praha	k1gFnSc1
3	#num#	k4
a	a	k8xC
Praha	Praha	k1gFnSc1
10	#num#	k4
–	–	k?
krom	krom	k7c2
toho	ten	k3xDgNnSc2
zároveň	zároveň	k6eAd1
došlo	dojít	k5eAaPmAgNnS
i	i	k9
k	k	k7c3
různým	různý	k2eAgFnPc3d1
drobnějším	drobný	k2eAgFnPc3d2
změnám	změna	k1gFnPc3
vymezení	vymezení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
poměrně	poměrně	k6eAd1
složitým	složitý	k2eAgInSc7d1
postupem	postup	k1gInSc7
bylo	být	k5eAaImAgNnS
dosaženo	dosáhnout	k5eAaPmNgNnS
stavu	stav	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
obvody	obvod	k1gInPc1
4	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
jsou	být	k5eAaImIp3nP
uspořádány	uspořádat	k5eAaPmNgInP
kolem	kolem	k7c2
centra	centrum	k1gNnSc2
tvořeného	tvořený	k2eAgInSc2d1
centrálními	centrální	k2eAgInPc7d1
obvody	obvod	k1gInPc7
1	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
do	do	k7c2
prstence	prstenec	k1gInSc2
v	v	k7c6
pořadí	pořadí	k1gNnSc6
po	po	k7c6
směru	směr	k1gInSc6
hodinových	hodinový	k2eAgFnPc2d1
ručiček	ručička	k1gFnPc2
od	od	k7c2
jihojihovýchodu	jihojihovýchod	k1gInSc2
až	až	k9
k	k	k7c3
jihovýchodu	jihovýchod	k1gInSc3
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
kromě	kromě	k7c2
Prahy	Praha	k1gFnSc2
7	#num#	k4
dosahovaly	dosahovat	k5eAaImAgFnP
až	až	k9
k	k	k7c3
okraji	okraj	k1gInSc3
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Zákon	zákon	k1gInSc1
stanovil	stanovit	k5eAaPmAgInS
nové	nový	k2eAgNnSc4d1
členění	členění	k1gNnSc4
státu	stát	k1gInSc2
na	na	k7c4
kraje	kraj	k1gInPc4
a	a	k8xC
okresy	okres	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc4d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
dostalo	dostat	k5eAaPmAgNnS
status	status	k1gInSc1
rovnocenný	rovnocenný	k2eAgInSc1d1
krajům	kraj	k1gInPc3
<g/>
,	,	kIx,
obvody	obvod	k1gInPc1
status	status	k1gInSc1
obdobný	obdobný	k2eAgInSc1d1
okresům	okres	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
byly	být	k5eAaImAgInP
k	k	k7c3
Praze	Praha	k1gFnSc3
připojeny	připojen	k2eAgInPc4d1
Čimice	Čimice	k1gInPc4
a	a	k8xC
Ruzyně	Ruzyně	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výhodou	výhoda	k1gFnSc7
nového	nový	k2eAgNnSc2d1
rozdělení	rozdělení	k1gNnSc2
bylo	být	k5eAaImAgNnS
například	například	k6eAd1
opětovné	opětovný	k2eAgNnSc1d1
sloučení	sloučení	k1gNnSc1
centra	centrum	k1gNnSc2
města	město	k1gNnSc2
do	do	k7c2
jediného	jediný	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1968	#num#	k4
<g/>
,	,	kIx,
1970	#num#	k4
a	a	k8xC
1974	#num#	k4
byly	být	k5eAaImAgFnP
do	do	k7c2
okrajových	okrajový	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
včleňovány	včleňován	k2eAgFnPc1d1
i	i	k8xC
nově	nově	k6eAd1
připojované	připojovaný	k2eAgFnPc1d1
obce	obec	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
si	se	k3xPyFc3
však	však	k9
zachovaly	zachovat	k5eAaPmAgInP
vlastní	vlastní	k2eAgInPc1d1
místní	místní	k2eAgInPc1d1
národní	národní	k2eAgInPc1d1
výbory	výbor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Obvody	obvod	k1gInPc1
byly	být	k5eAaImAgFnP
původně	původně	k6eAd1
popsány	popsat	k5eAaPmNgFnP
výčtem	výčet	k1gInSc7
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
a	a	k8xC
vymezením	vymezení	k1gNnSc7
hranic	hranice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
novelizaci	novelizace	k1gFnSc6
zákona	zákon	k1gInSc2
č.	č.	k?
36	#num#	k4
<g/>
/	/	kIx~
<g/>
1960	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
přesné	přesný	k2eAgNnSc1d1
vymezení	vymezení	k1gNnSc1
území	území	k1gNnSc2
obvodů	obvod	k1gInPc2
stanovil	stanovit	k5eAaPmAgInS
prováděcí	prováděcí	k2eAgInSc1d1
předpis	předpis	k1gInSc1
ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
<g/>
,	,	kIx,
vyhláška	vyhláška	k1gFnSc1
č.	č.	k?
564	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
výčtem	výčet	k1gInSc7
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
základní	základní	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
samosprávy	samospráva	k1gFnSc2
i	i	k8xC
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
zákon	zákon	k1gInSc1
tedy	tedy	k9
nyní	nyní	k6eAd1
zaručuje	zaručovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
žádná	žádný	k3yNgFnSc1
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
rozdělena	rozdělit	k5eAaPmNgFnS
do	do	k7c2
více	hodně	k6eAd2
územních	územní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgInSc1d1
státně-územní	státně-územní	k2eAgInSc1d1
pojem	pojem	k1gInSc1
obvodu	obvod	k1gInSc2
nelze	lze	k6eNd1
ztotožňovat	ztotožňovat	k5eAaImF
s	s	k7c7
později	pozdě	k6eAd2
vzniklými	vzniklý	k2eAgFnPc7d1
22	#num#	k4
číslovanými	číslovaný	k2eAgFnPc7d1
městskými	městský	k2eAgFnPc7d1
částmi	část	k1gFnPc7
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
správními	správní	k2eAgInPc7d1
obvody	obvod	k1gInPc7
<g/>
)	)	kIx)
podle	podle	k7c2
Statutu	statut	k1gInSc2
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
deset	deset	k4xCc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
má	mít	k5eAaImIp3nS
stejné	stejný	k2eAgInPc1d1
názvy	název	k1gInPc1
jako	jako	k8xC,k8xS
městské	městský	k2eAgInPc1d1
obvody	obvod	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
správní	správní	k2eAgInPc4d1
účely	účel	k1gInPc4
bylo	být	k5eAaImAgNnS
později	pozdě	k6eAd2
zavedeno	zavést	k5eAaPmNgNnS
nejprve	nejprve	k6eAd1
15	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
později	pozdě	k6eAd2
22	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
)	)	kIx)
menších	malý	k2eAgInPc2d2
správních	správní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
například	například	k6eAd1
rozšířený	rozšířený	k2eAgInSc1d1
správní	správní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
17	#num#	k4
zasahuje	zasahovat	k5eAaImIp3nS
do	do	k7c2
městských	městský	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
Praha	Praha	k1gFnSc1
5	#num#	k4
i	i	k8xC
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Původně	původně	k6eAd1
bylo	být	k5eAaImAgNnS
deset	deset	k4xCc1
pražských	pražský	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
též	též	k6eAd1
jednotkami	jednotka	k1gFnPc7
samosprávy	samospráva	k1gFnSc2
a	a	k8xC
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
1988	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
orgánů	orgán	k1gInPc2
a	a	k8xC
pravomocí	pravomoc	k1gFnPc2
přenesena	přenést	k5eAaPmNgFnS
na	na	k7c4
městské	městský	k2eAgFnPc4d1
části	část	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosavadní	dosavadní	k2eAgInPc1d1
obvodní	obvodní	k2eAgInPc1d1
úřady	úřad	k1gInPc1
zúžily	zúžit	k5eAaPmAgFnP
svou	svůj	k3xOyFgFnSc4
územní	územní	k2eAgFnSc4d1
působnost	působnost	k1gFnSc4
a	a	k8xC
změnily	změnit	k5eAaPmAgFnP
se	se	k3xPyFc4
také	také	k9
na	na	k7c4
úřady	úřad	k1gInPc4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosud	dosud	k6eAd1
zůstávají	zůstávat	k5eAaImIp3nP
obvody	obvod	k1gInPc1
směrodatné	směrodatný	k2eAgInPc1d1
například	například	k6eAd1
pro	pro	k7c4
příslušnost	příslušnost	k1gFnSc4
v	v	k7c6
soudním	soudní	k2eAgNnSc6d1
řízení	řízení	k1gNnSc6
a	a	k8xC
jsou	být	k5eAaImIp3nP
podle	podle	k7c2
nich	on	k3xPp3gMnPc2
organizovány	organizovat	k5eAaBmNgInP
i	i	k9
další	další	k2eAgFnPc1d1
instituce	instituce	k1gFnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
pošty	pošta	k1gFnSc2
<g/>
.	.	kIx.
18.7	18.7	k4
<g/>
.2005	.2005	k4
vláda	vláda	k1gFnSc1
navrhla	navrhnout	k5eAaPmAgFnS
zrušení	zrušení	k1gNnSc4
zákona	zákon	k1gInSc2
č.	č.	k?
36	#num#	k4
<g/>
/	/	kIx~
<g/>
1960	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
územním	územní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
(	(	kIx(
<g/>
sněmovní	sněmovní	k2eAgInSc1d1
tisk	tisk	k1gInSc1
1047	#num#	k4
<g/>
)	)	kIx)
Vláda	vláda	k1gFnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
zákonem	zákon	k1gInSc7
uchovat	uchovat	k5eAaPmF
deset	deset	k4xCc4
soudních	soudní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
a	a	k8xC
zavést	zavést	k5eAaPmF
čtyři	čtyři	k4xCgInPc1
policejní	policejní	k2eAgInPc1d1
obvody	obvod	k1gInPc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
I	i	k9
až	až	k9
Praha	Praha	k1gFnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neochota	neochota	k1gFnSc1
vlády	vláda	k1gFnSc2
zcela	zcela	k6eAd1
přizpůsobit	přizpůsobit	k5eAaPmF
soudní	soudní	k2eAgInSc4d1
systém	systém	k1gInSc4
novému	nový	k2eAgNnSc3d1
správnímu	správní	k2eAgNnSc3d1
uspořádání	uspořádání	k1gNnSc3
však	však	k9
naráží	narážet	k5eAaPmIp3nS,k5eAaImIp3nS
na	na	k7c4
silný	silný	k2eAgInSc4d1
nesouhlas	nesouhlas	k1gInSc4
zejména	zejména	k9
u	u	k7c2
představitelů	představitel	k1gMnPc2
nových	nový	k2eAgInPc2d1
krajů	kraj	k1gInPc2
<g/>
,	,	kIx,
takže	takže	k8xS
zákon	zákon	k1gInSc1
je	být	k5eAaImIp3nS
nyní	nyní	k6eAd1
neprůchodný	průchodný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Připojování	připojování	k1gNnSc1
nových	nový	k2eAgNnPc2d1
území	území	k1gNnPc2
1960	#num#	k4
<g/>
–	–	k?
<g/>
1974	#num#	k4
</s>
<s>
Zákon	zákon	k1gInSc1
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1967	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
usnadnil	usnadnit	k5eAaPmAgInS
připojování	připojování	k1gNnSc4
nových	nový	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obce	obec	k1gFnPc1
byly	být	k5eAaImAgFnP
průběžně	průběžně	k6eAd1
začleňovány	začleňovat	k5eAaImNgFnP
do	do	k7c2
přilehlých	přilehlý	k2eAgInPc2d1
okrajových	okrajový	k2eAgInPc2d1
pražských	pražský	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
Praha	Praha	k1gFnSc1
4	#num#	k4
až	až	k8xS
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústavní	ústavní	k2eAgInSc1d1
zákon	zákon	k1gInSc1
č.	č.	k?
110	#num#	k4
<g/>
/	/	kIx~
<g/>
1967	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
umožnil	umožnit	k5eAaPmAgInS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
v	v	k7c6
Praze	Praha	k1gFnSc6
kromě	kromě	k7c2
Národního	národní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
a	a	k8xC
obvodních	obvodní	k2eAgInPc2d1
národních	národní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
mohly	moct	k5eAaImAgInP
působit	působit	k5eAaImF
též	též	k9
místní	místní	k2eAgInPc1d1
národní	národní	k2eAgInPc1d1
výbory	výbor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
možnost	možnost	k1gFnSc4
uvedl	uvést	k5eAaPmAgMnS
do	do	k7c2
praxe	praxe	k1gFnSc2
Zákon	zákon	k1gInSc1
o	o	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Praze	Praha	k1gFnSc6
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1967	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
v	v	k7c6
§	§	k?
19	#num#	k4
stanovil	stanovit	k5eAaPmAgInS
připojení	připojení	k1gNnSc4
území	území	k1gNnSc2
dalších	další	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
k	k	k7c3
Praze	Praha	k1gFnSc3
a	a	k8xC
v	v	k7c6
§	§	k?
20	#num#	k4
pokračující	pokračující	k2eAgFnSc4d1
existenci	existence	k1gFnSc4
jejich	jejich	k3xOp3gInPc2
místních	místní	k2eAgInPc2d1
národních	národní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1960	#num#	k4
–	–	k?
připojeny	připojen	k2eAgFnPc1d1
obec	obec	k1gFnSc1
Ruzyně	Ruzyně	k1gFnSc1
<g/>
,	,	kIx,
osada	osada	k1gFnSc1
Čimice	Čimice	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
malé	malý	k2eAgFnPc1d1
části	část	k1gFnPc1
deseti	deset	k4xCc2
dalších	další	k2eAgNnPc2d1
mimopražských	mimopražský	k2eAgNnPc2d1
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
(	(	kIx(
<g/>
Dolní	dolní	k2eAgFnPc1d1
Chabry	Chabra	k1gFnPc1
<g/>
,	,	kIx,
Ďáblice	ďáblice	k1gFnPc1
<g/>
,	,	kIx,
Háje	háj	k1gInPc1
<g/>
,	,	kIx,
Petrovice	Petrovice	k1gFnPc1
<g/>
,	,	kIx,
Kunratice	Kunratice	k1gFnPc1
<g/>
,	,	kIx,
Řeporyje	Řeporyje	k1gFnSc1
<g/>
,	,	kIx,
Přední	přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
<g/>
,	,	kIx,
Nebušice	Nebušice	k1gFnSc1
<g/>
,	,	kIx,
Lysolaje	Lysolaje	k1gFnPc1
<g/>
,	,	kIx,
Holyně	Holyně	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1968	#num#	k4
–	–	k?
připojeny	připojit	k5eAaPmNgFnP
městečko	městečko	k1gNnSc1
Čakovice	Čakovice	k1gFnPc1
<g/>
,	,	kIx,
Ďáblice	ďáblice	k1gFnPc1
<g/>
,	,	kIx,
Háje	háj	k1gInPc1
<g/>
,	,	kIx,
Dolní	dolní	k2eAgFnPc1d1
Chabry	Chabra	k1gFnPc1
<g/>
,	,	kIx,
Chodov	Chodov	k1gInSc1
<g/>
,	,	kIx,
Velká	velký	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
<g/>
,	,	kIx,
Kbely	Kbely	k1gInPc1
<g/>
,	,	kIx,
Kunratice	Kunratice	k1gFnPc1
<g/>
,	,	kIx,
Kyje	kyje	k1gFnPc1
včetně	včetně	k7c2
k.	k.	k?
ú.	ú.	k?
Hostavice	Hostavice	k1gFnSc1
<g/>
,	,	kIx,
Lahovice	Lahovice	k1gFnSc1
(	(	kIx(
<g/>
část	část	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Letňany	Letňan	k1gMnPc4
<g/>
,	,	kIx,
Libuš	Libuš	k1gInSc1
<g/>
,	,	kIx,
Lysolaje	Lysolaje	k1gFnPc1
<g/>
,	,	kIx,
Dolní	dolní	k2eAgFnPc1d1
Měcholupy	Měcholupa	k1gFnPc1
<g/>
,	,	kIx,
Horní	horní	k2eAgFnPc1d1
Měcholupy	Měcholupa	k1gFnPc1
<g/>
,	,	kIx,
Miškovice	Miškovice	k1gFnPc1
<g/>
,	,	kIx,
město	město	k1gNnSc1
Modřany	Modřany	k1gInPc1
<g/>
,	,	kIx,
Nebušice	Nebušice	k1gInPc1
<g/>
,	,	kIx,
Petrovice	Petrovice	k1gFnPc1
<g/>
,	,	kIx,
Řepy	řepa	k1gFnPc1
<g/>
,	,	kIx,
Suchdol	Suchdol	k1gInSc1
<g/>
,	,	kIx,
Štěrboholy	Štěrbohol	k1gInPc1
<g/>
,	,	kIx,
Třeboradice	Třeboradice	k1gFnSc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
Holyně	Holyně	k1gFnSc1
</s>
<s>
1970	#num#	k4
–	–	k?
podle	podle	k7c2
některých	některý	k3yIgInPc2
zdrojů	zdroj	k1gInPc2
připojeny	připojen	k2eAgFnPc1d1
Hostavice	Hostavice	k1gFnPc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
podle	podle	k7c2
jiných	jiný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
byly	být	k5eAaImAgFnP
Hostavice	Hostavice	k1gFnPc1
připojeny	připojit	k5eAaPmNgFnP
již	již	k6eAd1
roku	rok	k1gInSc2
1968	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc1
obce	obec	k1gFnSc2
Kyje	kyj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
1970	#num#	k4
–	–	k?
z	z	k7c2
důvodu	důvod	k1gInSc2
rozšíření	rozšíření	k1gNnSc2
letiště	letiště	k1gNnSc1
Ruzyně	Ruzyně	k1gFnSc2
připojeny	připojit	k5eAaPmNgFnP
části	část	k1gFnPc1
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
Přední	přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
<g/>
,	,	kIx,
Kněžívka	Kněžívka	k1gFnSc1
<g/>
,	,	kIx,
Kněževes	Kněževes	k1gFnSc1
<g/>
,	,	kIx,
Dobrovíz	Dobrovíz	k1gInSc1
<g/>
,	,	kIx,
Hostivice	Hostivice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnPc4d1
části	část	k1gFnPc4
těchto	tento	k3xDgFnPc2
obcí	obec	k1gFnPc2
(	(	kIx(
<g/>
zástavba	zástavba	k1gFnSc1
<g/>
)	)	kIx)
i	i	k9
nadále	nadále	k6eAd1
ležely	ležet	k5eAaImAgFnP
mimo	mimo	k7c4
hranice	hranice	k1gFnPc4
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1974	#num#	k4
–	–	k?
zákonem	zákon	k1gInSc7
ČNR	ČNR	kA
č.	č.	k?
31	#num#	k4
<g/>
/	/	kIx~
<g/>
1974	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
připojeno	připojit	k5eAaPmNgNnS
30	#num#	k4
obcí	obec	k1gFnPc2
zahrnujících	zahrnující	k2eAgFnPc2d1
37	#num#	k4
katastrálních	katastrální	k2eAgFnPc2d1
území	území	k1gNnSc4
Běchovice	Běchovice	k1gFnPc4
<g/>
,	,	kIx,
Benice	Benice	k1gFnPc4
<g/>
,	,	kIx,
Březiněves	Březiněves	k1gInSc1
<g/>
,	,	kIx,
Dolní	dolní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
<g/>
,	,	kIx,
Dubeč	Dubeč	k1gInSc1
<g/>
,	,	kIx,
Dubeček	dubeček	k1gInSc1
<g/>
,	,	kIx,
město	město	k1gNnSc1
Horní	horní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
Cholupice	Cholupice	k1gFnSc1
včetně	včetně	k7c2
<g />
.	.	kIx.
</s>
<s hack="1">
k.	k.	k?
ú.	ú.	k?
Točná	točný	k2eAgFnSc1d1
<g/>
,	,	kIx,
Klánovice	Klánovice	k1gFnPc1
<g/>
,	,	kIx,
Koloděje	Koloděje	k1gFnPc1
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
Kolovraty	kolovrat	k1gInPc1
včetně	včetně	k7c2
k.	k.	k?
ú.	ú.	k?
Lipany	lipan	k1gMnPc7
<g/>
,	,	kIx,
Královice	královic	k1gMnPc4
<g/>
,	,	kIx,
Křeslice	Křeslice	k1gFnSc1
<g/>
,	,	kIx,
Lipence	Lipence	k1gFnSc1
<g/>
,	,	kIx,
Lochkov	Lochkov	k1gInSc1
<g/>
,	,	kIx,
Nedvězí	Nedvězí	k1gNnSc1
<g/>
,	,	kIx,
Písnice	písnice	k1gFnSc1
<g/>
,	,	kIx,
Přední	přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
<g/>
,	,	kIx,
město	město	k1gNnSc1
Radotín	Radotín	k1gInSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
Řeporyje	Řeporyje	k1gFnSc2
včetně	včetně	k7c2
k.	k.	k?
ú.	ú.	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Zadní	zadní	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
<g/>
,	,	kIx,
Satalice	Satalice	k1gFnSc1
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
Slivenec	Slivenec	k1gInSc1
včetně	včetně	k7c2
k.	k.	k?
ú.	ú.	k?
Holyně	Holyně	k1gFnSc2
<g/>
,	,	kIx,
Stodůlky	stodůlka	k1gFnSc2
<g/>
,	,	kIx,
Šeberov	Šeberov	k1gInSc4
<g/>
,	,	kIx,
Třebonice	Třebonice	k1gFnPc4
<g/>
,	,	kIx,
město	město	k1gNnSc4
Uhříněves	Uhříněves	k1gFnSc1
včetně	včetně	k7c2
k.	k.	k?
ú.	ú.	k?
Hájek	Hájek	k1gMnSc1
a	a	k8xC
Pitkovice	Pitkovice	k1gFnSc1
<g/>
,	,	kIx,
Újezd	Újezd	k1gInSc1
nad	nad	k7c7
Lesy	les	k1gInPc7
<g/>
,	,	kIx,
Újezd	Újezd	k1gInSc1
u	u	k7c2
Průhonic	Průhonice	k1gFnPc2
<g/>
,	,	kIx,
Vinoř	Vinoř	k1gFnSc1
<g/>
,	,	kIx,
město	město	k1gNnSc1
Zbraslav	Zbraslav	k1gFnSc1
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
Zličín	Zličína	k1gFnPc2
včetně	včetně	k7c2
k.	k.	k?
ú.	ú.	k?
Sobín	Sobín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
1960	#num#	k4
</s>
<s>
1968	#num#	k4
</s>
<s>
1974	#num#	k4
</s>
<s>
Nová	nový	k2eAgNnPc1d1
katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
Prahy	Praha	k1gFnSc2
vznikla	vzniknout	k5eAaPmAgFnS
dvě	dva	k4xCgNnPc4
nová	nový	k2eAgNnPc4d1
katastrální	katastrální	k2eAgNnPc4d1
území	území	k1gNnPc4
<g/>
:	:	kIx,
</s>
<s>
Černý	černý	k2eAgInSc1d1
Most	most	k1gInSc1
<g/>
:	:	kIx,
vzniklo	vzniknout	k5eAaPmAgNnS
rozhodnutími	rozhodnutí	k1gNnPc7
pléna	plénum	k1gNnSc2
NVP	NVP	kA
č.	č.	k?
20	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
P	P	kA
z	z	k7c2
15	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1986	#num#	k4
a	a	k8xC
7	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
P	P	kA
ze	z	k7c2
17	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1987	#num#	k4
z	z	k7c2
částí	část	k1gFnPc2
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
Kyje	kyj	k1gInSc2
<g/>
,	,	kIx,
Horní	horní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
a	a	k8xC
Hloubětín	Hloubětín	k1gInSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kamýk	Kamýk	k1gInSc1
<g/>
:	:	kIx,
vzniklo	vzniknout	k5eAaPmAgNnS
usnesením	usnesení	k1gNnSc7
pléna	plénum	k1gNnSc2
NVP	NVP	kA
č.	č.	k?
10	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
P	P	kA
z	z	k7c2
20	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1988	#num#	k4
z	z	k7c2
částí	část	k1gFnPc2
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
Modřany	Modřany	k1gInPc7
a	a	k8xC
Lhotka	Lhotka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Obvody	obvod	k1gInPc1
Praha	Praha	k1gFnSc1
1	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
10	#num#	k4
(	(	kIx(
<g/>
podle	podle	k7c2
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
1	#num#	k4
–	–	k?
Josefov	Josefov	k1gInSc1
<g/>
,	,	kIx,
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
Malá	malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgFnSc1d1
část	část	k1gFnSc1
Hradčan	Hradčany	k1gInPc2
<g/>
,	,	kIx,
severní	severní	k2eAgFnSc1d1
část	část	k1gFnSc1
Nového	Nového	k2eAgNnSc2d1
Města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Praha	Praha	k1gFnSc1
2	#num#	k4
–	–	k?
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
jižní	jižní	k2eAgFnSc1d1
část	část	k1gFnSc1
Nového	Nového	k2eAgNnSc2d1
Města	město	k1gNnSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
Nuslí	Nusle	k1gFnPc2
<g/>
,	,	kIx,
západní	západní	k2eAgFnSc1d1
část	část	k1gFnSc1
Vinohrad	Vinohrady	k1gInPc2
</s>
<s>
Praha	Praha	k1gFnSc1
3	#num#	k4
–	–	k?
Žižkov	Žižkov	k1gInSc1
a	a	k8xC
severovýchodní	severovýchodní	k2eAgFnSc1d1
část	část	k1gFnSc1
Vinohrad	Vinohrady	k1gInPc2
</s>
<s>
Praha	Praha	k1gFnSc1
4	#num#	k4
–	–	k?
část	část	k1gFnSc4
Nuslí	Nusle	k1gFnPc2
(	(	kIx(
<g/>
bez	bez	k7c2
Nuselského	nuselský	k2eAgNnSc2d1
údolí	údolí	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Michle	Michl	k1gMnSc5
<g/>
,	,	kIx,
Krč	Krč	k1gFnSc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
Záběhlic	Záběhlice	k1gFnPc2
(	(	kIx(
<g/>
Spořilov	Spořilov	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Lhotka	Lhotka	k1gFnSc1
<g/>
,	,	kIx,
Podolí	Podolí	k1gNnSc1
<g/>
,	,	kIx,
Braník	Braník	k1gInSc1
a	a	k8xC
Hodkovičky	Hodkovička	k1gFnPc1
(	(	kIx(
<g/>
1968	#num#	k4
připojeny	připojen	k2eAgInPc1d1
Háje	háj	k1gInPc1
<g/>
,	,	kIx,
Chodov	Chodov	k1gInSc1
<g/>
,	,	kIx,
Kunratice	Kunratice	k1gFnPc1
<g/>
,	,	kIx,
Libuš	Libuš	k1gInSc1
<g/>
,	,	kIx,
Modřany	Modřany	k1gInPc1
<g/>
,	,	kIx,
1974	#num#	k4
připojeny	připojen	k2eAgFnPc1d1
Cholupice	Cholupice	k1gFnPc1
<g/>
,	,	kIx,
Točná	točný	k2eAgFnSc1d1
<g/>
,	,	kIx,
Písnice	písnice	k1gFnSc1
<g/>
,	,	kIx,
Šeberov	Šeberov	k1gInSc1
<g/>
,	,	kIx,
Újezd	Újezd	k1gInSc1
u	u	k7c2
Průhonic	Průhonice	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
5	#num#	k4
–	–	k?
Smíchov	Smíchov	k1gInSc1
<g/>
,	,	kIx,
Košíře	Košíře	k1gInPc1
<g/>
,	,	kIx,
Motol	Motol	k1gInSc1
<g/>
,	,	kIx,
Radlice	Radlice	k1gInPc1
<g/>
,	,	kIx,
Jinonice	Jinonice	k1gFnPc1
<g/>
,	,	kIx,
Hlubočepy	Hlubočep	k1gInPc1
<g/>
,	,	kIx,
Malá	malý	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
(	(	kIx(
<g/>
1968	#num#	k4
připojena	připojen	k2eAgFnSc1d1
Velká	velký	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
<g/>
,	,	kIx,
Lahovice	Lahovice	k1gFnSc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
Holyně	Holyně	k1gFnSc1
<g/>
,	,	kIx,
Zbraslav	Zbraslav	k1gFnSc1
<g/>
,	,	kIx,
Zličín	Zličín	k1gInSc1
<g/>
,	,	kIx,
1974	#num#	k4
připojeny	připojen	k2eAgFnPc1d1
zbytek	zbytek	k1gInSc4
Holyně	Holyně	k1gFnSc2
<g/>
,	,	kIx,
Zadní	zadní	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
<g/>
,	,	kIx,
Lipence	Lipence	k1gFnSc1
<g/>
,	,	kIx,
Lochkov	Lochkov	k1gInSc1
<g/>
,	,	kIx,
Radotín	Radotín	k1gMnSc1
<g/>
,	,	kIx,
Řeporyje	Řeporyje	k1gMnSc1
<g/>
,	,	kIx,
Slivenec	Slivenec	k1gMnSc1
<g/>
,	,	kIx,
Stodůlky	stodůlka	k1gFnPc1
<g/>
,	,	kIx,
Třebonice	Třebonice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
6	#num#	k4
–	–	k?
Dejvice	Dejvice	k1gFnPc1
<g/>
,	,	kIx,
Sedlec	Sedlec	k1gInSc1
<g/>
,	,	kIx,
Střešovice	Střešovice	k1gFnSc1
<g/>
,	,	kIx,
Břevnov	Břevnov	k1gInSc1
<g/>
,	,	kIx,
Veleslavín	Veleslavín	k1gInSc1
<g/>
,	,	kIx,
Vokovice	Vokovice	k1gFnSc1
<g/>
,	,	kIx,
Dolní	dolní	k2eAgFnSc1d1
Liboc	Liboc	k1gFnSc1
<g/>
,	,	kIx,
Ruzyně	Ruzyně	k1gFnSc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
Bubenče	Bubeneč	k1gFnSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
Hradčan	Hradčany	k1gInPc2
(	(	kIx(
<g/>
1968	#num#	k4
připojeny	připojen	k2eAgFnPc1d1
Lysolaje	Lysolaje	k1gFnPc1
<g/>
,	,	kIx,
Nebušice	Nebušice	k1gInPc1
<g/>
,	,	kIx,
Řepy	řepa	k1gFnPc1
<g/>
,	,	kIx,
Suchdol	Suchdol	k1gInSc1
<g/>
,	,	kIx,
1974	#num#	k4
připojena	připojen	k2eAgFnSc1d1
Přední	přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
7	#num#	k4
–	–	k?
Holešovice-Bubny	Holešovice-Bubna	k1gFnSc2
<g/>
,	,	kIx,
části	část	k1gFnSc2
Bubenče	Bubeneč	k1gFnSc2
<g/>
,	,	kIx,
dolní	dolní	k2eAgFnSc1d1
Troja	Troja	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
8	#num#	k4
–	–	k?
Bohnice	Bohnice	k1gInPc1
<g/>
,	,	kIx,
Čimice	Čimice	k1gFnPc1
<g/>
,	,	kIx,
Karlín	Karlín	k1gInSc1
<g/>
,	,	kIx,
Kobylisy	Kobylisy	k1gInPc1
<g/>
,	,	kIx,
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
(	(	kIx(
<g/>
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
mezi	mezi	k7c7
Karlínem	Karlín	k1gInSc7
a	a	k8xC
Žižkovem	Žižkov	k1gInSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Střížkov	Střížkov	k1gInSc1
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
Libně	Libeň	k1gFnSc2
<g/>
,	,	kIx,
horní	horní	k2eAgFnSc4d1
část	část	k1gFnSc4
Troji	troje	k4xRgMnPc1
(	(	kIx(
<g/>
1968	#num#	k4
připojeny	připojen	k2eAgFnPc1d1
Ďáblice	ďáblice	k1gFnPc1
<g/>
,	,	kIx,
Dolní	dolní	k2eAgInPc1d1
Chabry	Chabr	k1gInPc1
<g/>
,	,	kIx,
1974	#num#	k4
připojena	připojen	k2eAgFnSc1d1
Březiněves	Březiněves	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
9	#num#	k4
–	–	k?
Vysočany	Vysočany	k1gInPc1
<g/>
,	,	kIx,
Prosek	proséct	k5eAaPmDgInS
<g/>
,	,	kIx,
Hloubětín	Hloubětín	k1gInSc1
<g/>
,	,	kIx,
Hrdlořezy	Hrdlořezy	k1gInPc1
<g/>
,	,	kIx,
východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
Libně	Libeň	k1gFnSc2
(	(	kIx(
<g/>
1968	#num#	k4
připojeny	připojen	k2eAgFnPc1d1
Čakovice	Čakovice	k1gFnPc1
<g/>
,	,	kIx,
Kbely	Kbely	k1gInPc1
<g/>
,	,	kIx,
Kyje	kyje	k1gFnPc1
<g/>
,	,	kIx,
Letňany	Letňan	k1gMnPc7
<g/>
,	,	kIx,
Miškovice	Miškovice	k1gFnSc1
<g/>
,	,	kIx,
Třeboradice	Třeboradice	k1gFnSc1
<g/>
,	,	kIx,
1970	#num#	k4
připojeny	připojen	k2eAgFnPc4d1
Hostavice	Hostavice	k1gFnPc4
<g/>
,	,	kIx,
1974	#num#	k4
připojeny	připojen	k2eAgFnPc1d1
Běchovice	Běchovice	k1gFnPc1
<g/>
,	,	kIx,
Klánovice	Klánovice	k1gFnPc1
<g/>
,	,	kIx,
Koloděje	Koloděje	k1gFnPc1
<g/>
,	,	kIx,
Dolní	dolní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
<g/>
,	,	kIx,
Horní	horní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
<g/>
,	,	kIx,
Satalice	Satalice	k1gFnPc1
<g/>
,	,	kIx,
Újezd	Újezd	k1gInSc1
nad	nad	k7c7
Lesy	les	k1gInPc7
<g/>
,	,	kIx,
Vinoř	Vinoř	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
10	#num#	k4
–	–	k?
Vršovice	Vršovice	k1gFnPc1
<g/>
,	,	kIx,
Strašnice	Strašnice	k1gFnPc1
<g/>
,	,	kIx,
Hostivař	Hostivař	k1gFnSc1
<g/>
,	,	kIx,
Malešice	Malešice	k1gFnPc1
<g/>
,	,	kIx,
Záběhlice	Záběhlice	k1gFnPc1
bez	bez	k7c2
Spořilova	spořilův	k2eAgNnSc2d1
<g/>
,	,	kIx,
jihovýchodní	jihovýchodní	k2eAgFnSc4d1
část	část	k1gFnSc4
Vinohrad	Vinohrady	k1gInPc2
(	(	kIx(
<g/>
1968	#num#	k4
připojeny	připojen	k2eAgFnPc1d1
Dolní	dolní	k2eAgFnPc1d1
Měcholupy	Měcholupa	k1gFnPc1
<g/>
,	,	kIx,
Horní	horní	k2eAgFnPc1d1
Měcholupy	Měcholupa	k1gFnPc1
<g/>
,	,	kIx,
Petrovice	Petrovice	k1gFnPc1
<g/>
,	,	kIx,
Štěrboholy	Štěrbohol	k1gInPc1
<g/>
,	,	kIx,
1974	#num#	k4
připojeny	připojen	k2eAgFnPc1d1
Benice	Benice	k1gFnPc1
<g/>
,	,	kIx,
Dubeč	Dubeč	k1gInSc1
<g/>
,	,	kIx,
Dubeček	dubeček	k1gInSc1
<g/>
,	,	kIx,
Hájek	hájek	k1gInSc1
<g/>
,	,	kIx,
Kolovraty	kolovrat	k1gInPc1
<g/>
,	,	kIx,
Královice	královic	k1gMnPc4
<g/>
,	,	kIx,
Křeslice	Křeslice	k1gFnPc4
<g/>
,	,	kIx,
Lipany	lipan	k1gMnPc4
<g/>
,	,	kIx,
Nedvězí	Nedvězí	k1gNnSc4
<g/>
,	,	kIx,
Pitkovice	Pitkovice	k1gFnSc1
<g/>
,	,	kIx,
Uhříněves	Uhříněves	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Obvody	obvod	k1gInPc1
Praha	Praha	k1gFnSc1
1	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
10	#num#	k4
(	(	kIx(
<g/>
podle	podle	k7c2
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
1	#num#	k4
–	–	k?
zahrnuje	zahrnovat	k5eAaImIp3nS
městskou	městský	k2eAgFnSc4d1
část	část	k1gFnSc4
Praha	Praha	k1gFnSc1
1	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
2	#num#	k4
–	–	k?
zahrnuje	zahrnovat	k5eAaImIp3nS
městskou	městský	k2eAgFnSc4d1
část	část	k1gFnSc4
Praha	Praha	k1gFnSc1
2	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
3	#num#	k4
–	–	k?
zahrnuje	zahrnovat	k5eAaImIp3nS
městskou	městský	k2eAgFnSc4d1
část	část	k1gFnSc4
Praha	Praha	k1gFnSc1
3	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
4	#num#	k4
–	–	k?
zahrnuje	zahrnovat	k5eAaImIp3nS
městské	městský	k2eAgFnSc3d1
části	část	k1gFnSc3
Praha	Praha	k1gFnSc1
4	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
11	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
12	#num#	k4
<g/>
,	,	kIx,
Praha-Kunratice	Praha-Kunratice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Libuš	Praha-Libuš	k1gInSc1
<g/>
,	,	kIx,
Praha-Šeberov	Praha-Šeberov	k1gInSc1
<g/>
,	,	kIx,
Praha-Újezd	Praha-Újezd	k1gInSc1
</s>
<s>
Praha	Praha	k1gFnSc1
5	#num#	k4
–	–	k?
zahrnuje	zahrnovat	k5eAaImIp3nS
městské	městský	k2eAgFnSc3d1
části	část	k1gFnSc3
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
13	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
16	#num#	k4
<g/>
,	,	kIx,
Praha-Lipence	Praha-Lipence	k1gFnSc1
<g/>
,	,	kIx,
Praha-Lochkov	Praha-Lochkov	k1gInSc1
<g/>
,	,	kIx,
Praha-Řeporyje	Praha-Řeporyje	k1gFnSc1
<g/>
,	,	kIx,
Praha-Slivenec	Praha-Slivenec	k1gInSc1
<g/>
,	,	kIx,
Praha-Velká	Praha-Velký	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
<g/>
,	,	kIx,
Praha-Zbraslav	Praha-Zbraslav	k1gMnSc1
<g/>
,	,	kIx,
Praha-Zličín	Praha-Zličín	k1gMnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
6	#num#	k4
–	–	k?
zahrnuje	zahrnovat	k5eAaImIp3nS
městské	městský	k2eAgFnSc3d1
části	část	k1gFnSc3
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
17	#num#	k4
<g/>
,	,	kIx,
Praha-Lysolaje	Praha-Lysolaje	k1gFnSc1
<g/>
,	,	kIx,
Praha-Nebušice	Praha-Nebušice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Přední	Praha-Přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
<g/>
,	,	kIx,
Praha-Suchdol	Praha-Suchdol	k1gInSc1
</s>
<s>
Praha	Praha	k1gFnSc1
7	#num#	k4
–	–	k?
zahrnuje	zahrnovat	k5eAaImIp3nS
městské	městský	k2eAgFnSc3d1
části	část	k1gFnSc3
Praha	Praha	k1gFnSc1
7	#num#	k4
<g/>
,	,	kIx,
Praha-Troja	Praha-Troja	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
8	#num#	k4
–	–	k?
zahrnuje	zahrnovat	k5eAaImIp3nS
městské	městský	k2eAgFnSc3d1
části	část	k1gFnSc3
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
,	,	kIx,
Praha-Březiněves	Praha-Březiněves	k1gInSc1
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgInPc1d1
Chabry	Chabr	k1gInPc1
<g/>
,	,	kIx,
Praha-Ďáblice	Praha-Ďáblice	k1gFnPc1
</s>
<s>
Praha	Praha	k1gFnSc1
9	#num#	k4
–	–	k?
zahrnuje	zahrnovat	k5eAaImIp3nS
městské	městský	k2eAgFnSc3d1
části	část	k1gFnSc3
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
14	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
18	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
19	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
20	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
21	#num#	k4
<g/>
,	,	kIx,
Praha-Běchovice	Praha-Běchovice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Čakovice	Praha-Čakovice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Klánovice	Praha-Klánovice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Koloděje	Praha-Koloděje	k1gFnSc1
<g/>
,	,	kIx,
Praha-Satalice	Praha-Satalice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Vinoř	Praha-Vinoř	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
10	#num#	k4
–	–	k?
zahrnuje	zahrnovat	k5eAaImIp3nS
městské	městský	k2eAgFnSc3d1
části	část	k1gFnSc3
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
15	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
22	#num#	k4
<g/>
,	,	kIx,
Praha-Benice	Praha-Benice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgFnPc1d1
Měcholupy	Měcholupa	k1gFnPc1
<g/>
,	,	kIx,
Praha-Dubeč	Praha-Dubeč	k1gInSc1
<g/>
,	,	kIx,
Praha-Kolovraty	Praha-Kolovrat	k1gInPc1
<g/>
,	,	kIx,
Praha-Královice	Praha-Královice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Křeslice	Praha-Křeslice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Nedvězí	Praha-Nedvězí	k1gNnPc1
<g/>
,	,	kIx,
Praha-Petrovice	Praha-Petrovice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Štěrboholy	Praha-Štěrbohola	k1gFnPc1
</s>
<s>
Správní	správní	k2eAgNnSc1d1
a	a	k8xC
samosprávné	samosprávný	k2eAgNnSc1d1
členění	členění	k1gNnSc1
od	od	k7c2
roku	rok	k1gInSc2
1988	#num#	k4
</s>
<s>
Zákonem	zákon	k1gInSc7
č.	č.	k?
418	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
od	od	k7c2
24.11	24.11	k4
<g/>
.1990	.1990	k4
zavedeny	zaveden	k2eAgFnPc1d1
městské	městský	k2eAgFnPc1d1
části	část	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úřady	úřad	k1gInPc1
dosavadních	dosavadní	k2eAgInPc2d1
obvodních	obvodní	k2eAgInPc2d1
národních	národní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
a	a	k8xC
místních	místní	k2eAgInPc2d1
národních	národní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
jsou	být	k5eAaImIp3nP
přejmenovány	přejmenovat	k5eAaPmNgInP
na	na	k7c4
obvodní	obvodní	k2eAgFnSc4d1
a	a	k8xC
místní	místní	k2eAgInPc4d1
úřady	úřad	k1gInPc4
<g/>
,	,	kIx,
samosprávné	samosprávný	k2eAgInPc4d1
orgány	orgán	k1gInPc4
národních	národní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
na	na	k7c4
zastupitelstva	zastupitelstvo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákonem	zákon	k1gInSc7
č.	č.	k?
131	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
obvodní	obvodní	k2eAgInPc1d1
a	a	k8xC
místní	místní	k2eAgInPc1d1
úřady	úřad	k1gInPc1
přejmenovávají	přejmenovávat	k5eAaImIp3nP
na	na	k7c4
úřady	úřad	k1gInPc4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
byly	být	k5eAaImAgInP
správní	správní	k2eAgInPc1d1
a	a	k8xC
samosprávné	samosprávný	k2eAgInPc1d1
obvody	obvod	k1gInPc1
totožné	totožný	k2eAgInPc1d1
s	s	k7c7
deseti	deset	k4xCc7
státně-územními	státně-územní	k2eAgInPc7d1
obvody	obvod	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
území	území	k1gNnPc1
spadala	spadat	k5eAaPmAgNnP,k5eAaImAgNnP
přímo	přímo	k6eAd1
pod	pod	k7c4
některý	některý	k3yIgInSc4
z	z	k7c2
deseti	deset	k4xCc2
obvodních	obvodní	k2eAgInPc2d1
národních	národní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
připojené	připojený	k2eAgFnPc1d1
obce	obec	k1gFnPc1
si	se	k3xPyFc3
zachovaly	zachovat	k5eAaPmAgFnP
svůj	svůj	k3xOyFgInSc4
vlastní	vlastní	k2eAgInSc4d1
místní	místní	k2eAgInSc4d1
národní	národní	k2eAgInSc4d1
výbor	výbor	k1gInSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
některé	některý	k3yIgFnPc4
činnosti	činnost	k1gFnPc4
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
vykonával	vykonávat	k5eAaImAgInS
obvodní	obvodní	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
a	a	k8xC
i	i	k9
tyto	tento	k3xDgFnPc1
obce	obec	k1gFnPc1
patřily	patřit	k5eAaImAgFnP
zároveň	zároveň	k6eAd1
pod	pod	k7c4
působnost	působnost	k1gFnSc4
obvodní	obvodní	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Samosprávné	samosprávný	k2eAgFnPc1d1
městské	městský	k2eAgFnPc1d1
části	část	k1gFnPc1
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
na	na	k7c4
57	#num#	k4
samosprávných	samosprávný	k2eAgFnPc2d1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
vzniklo	vzniknout	k5eAaPmAgNnS
56	#num#	k4
takzvaných	takzvaný	k2eAgFnPc2d1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
(	(	kIx(
<g/>
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
zákon	zákon	k1gInSc1
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1967	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
používal	používat	k5eAaImAgInS
termín	termín	k1gInSc1
„	„	k?
<g/>
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
“	“	k?
jako	jako	k8xS,k8xC
synonymní	synonymní	k2eAgInSc4d1
s	s	k7c7
pojmem	pojem	k1gInSc7
„	„	k?
<g/>
katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1992	#num#	k4
přibyla	přibýt	k5eAaPmAgFnS
další	další	k2eAgInPc4d1
<g/>
,	,	kIx,
Praha-Troja	Praha-Troja	k1gFnSc1
<g/>
,	,	kIx,
odštěpením	odštěpení	k1gNnSc7
z	z	k7c2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
byla	být	k5eAaImAgFnS
samospráva	samospráva	k1gFnSc1
v	v	k7c6
původnějších	původní	k2eAgFnPc6d2
částech	část	k1gFnPc6
Prahy	Praha	k1gFnSc2
dvojstupňová	dvojstupňový	k2eAgNnPc1d1
a	a	k8xC
v	v	k7c4
okrajových	okrajový	k2eAgFnPc2d1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
připojených	připojený	k2eAgInPc2d1
<g/>
)	)	kIx)
částech	část	k1gFnPc6
trojstupňová	trojstupňový	k2eAgFnSc1d1
<g/>
,	,	kIx,
do	do	k7c2
území	území	k1gNnSc2
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
Praha	Praha	k1gFnSc1
1	#num#	k4
až	až	k8xS
Praha	Praha	k1gFnSc1
10	#num#	k4
vytvořených	vytvořený	k2eAgInPc2d1
roku	rok	k1gInSc2
1990	#num#	k4
již	již	k6eAd1
nespadaly	spadat	k5eNaPmAgFnP,k5eNaImAgFnP
ty	ten	k3xDgFnPc1
části	část	k1gFnPc1
obvodů	obvod	k1gInPc2
Praha	Praha	k1gFnSc1
1	#num#	k4
až	až	k8xS
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
měly	mít	k5eAaImAgFnP
vlastní	vlastní	k2eAgInPc4d1
místní	místní	k2eAgInPc4d1
národní	národní	k2eAgInPc4d1
výbory	výbor	k1gInPc4
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
vlastní	vlastnit	k5eAaImIp3nP
místní	místní	k2eAgInPc1d1
úřady	úřad	k1gInPc1
<g/>
,	,	kIx,
ještě	ještě	k9
později	pozdě	k6eAd2
úřady	úřad	k1gInPc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městské	městský	k2eAgFnPc4d1
části	část	k1gFnPc4
se	se	k3xPyFc4
tak	tak	k6eAd1
dělily	dělit	k5eAaImAgFnP
na	na	k7c6
„	„	k?
<g/>
velké	velký	k2eAgNnSc1d1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
malé	malý	k2eAgFnSc2d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
deseti	deset	k4xCc7
„	„	k?
<g/>
velkým	velký	k2eAgInSc7d1
<g/>
“	“	k?
postupně	postupně	k6eAd1
přibývaly	přibývat	k5eAaImAgFnP
některé	některý	k3yIgFnPc1
další	další	k2eAgFnPc1d1
<g/>
,	,	kIx,
původně	původně	k6eAd1
„	„	k?
<g/>
malé	malý	k2eAgFnSc2d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
18	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1994	#num#	k4
bylo	být	k5eAaImAgNnS
pět	pět	k4xCc1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
přejmenováno	přejmenovat	k5eAaPmNgNnS
na	na	k7c6
Prahu	práh	k1gInSc6
11	#num#	k4
až	až	k6eAd1
Prahu	Praha	k1gFnSc4
15	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
rámci	rámec	k1gInSc6
vytvoření	vytvoření	k1gNnPc2
správních	správní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
přenesené	přenesený	k2eAgFnSc2d1
působnosti	působnost	k1gFnSc2
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
některým	některý	k3yIgFnPc3
změnám	změna	k1gFnPc3
jejich	jejich	k3xOp3gNnPc4
vymezení	vymezení	k1gNnPc4
<g/>
,	,	kIx,
například	například	k6eAd1
Hostivař	Hostivař	k1gFnSc1
byla	být	k5eAaImAgFnS
z	z	k7c2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
10	#num#	k4
přeřazena	přeřadit	k5eAaPmNgFnS
do	do	k7c2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
2002	#num#	k4
bylo	být	k5eAaImAgNnS
podobným	podobný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
přejmenováno	přejmenovat	k5eAaPmNgNnS
dalších	další	k2eAgFnPc2d1
7	#num#	k4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
na	na	k7c6
Prahu	práh	k1gInSc6
16	#num#	k4
až	až	k6eAd1
Prahu	Praha	k1gFnSc4
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2005	#num#	k4
byla	být	k5eAaImAgFnS
dolní	dolní	k2eAgFnSc1d1
část	část	k1gFnSc1
Sedlce	Sedlec	k1gInSc2
přeřazena	přeřadit	k5eAaPmNgFnS
z	z	k7c2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha-Suchdol	Praha-Suchdola	k1gFnPc2
k	k	k7c3
městské	městský	k2eAgFnSc3d1
části	část	k1gFnSc3
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
samosprávné	samosprávný	k2eAgFnSc2d1
působnosti	působnost	k1gFnSc2
si	se	k3xPyFc3
je	být	k5eAaImIp3nS
všech	všecek	k3xTgFnPc2
57	#num#	k4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
rovno	roven	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městské	městský	k2eAgFnPc4d1
části	část	k1gFnPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
úřady	úřad	k1gInPc1
mají	mít	k5eAaImIp3nP
rozšířenou	rozšířený	k2eAgFnSc4d1
působnost	působnost	k1gFnSc4
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
názvy	název	k1gInPc1
podle	podle	k7c2
svých	svůj	k3xOyFgInPc2
správních	správní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbylých	zbylý	k2eAgFnPc2d1
35	#num#	k4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
zůstalo	zůstat	k5eAaPmAgNnS
pojmenováno	pojmenovat	k5eAaPmNgNnS
podle	podle	k7c2
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
<g/>
,	,	kIx,
kterými	který	k3yQgFnPc7,k3yIgFnPc7,k3yRgFnPc7
jsou	být	k5eAaImIp3nP
tvořeny	tvořen	k2eAgFnPc1d1
nebo	nebo	k8xC
v	v	k7c6
nichž	jenž	k3xRgInPc6
má	mít	k5eAaImIp3nS
sídlo	sídlo	k1gNnSc4
úřad	úřad	k1gInSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
se	se	k3xPyFc4
od	od	k7c2
názvu	název	k1gInSc2
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
liší	lišit	k5eAaImIp3nP
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
obsahuje	obsahovat	k5eAaImIp3nS
navíc	navíc	k6eAd1
slovo	slovo	k1gNnSc1
„	„	k?
<g/>
Praha	Praha	k1gFnSc1
<g/>
“	“	k?
se	s	k7c7
spojovníkem	spojovník	k1gInSc7
a	a	k8xC
v	v	k7c6
několika	několik	k4yIc6
případech	případ	k1gInPc6
je	být	k5eAaImIp3nS
zkrácen	zkrátit	k5eAaPmNgInS
o	o	k7c4
specifikující	specifikující	k2eAgInPc4d1
přívlastky	přívlastek	k1gInPc4
(	(	kIx(
<g/>
například	například	k6eAd1
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
Praha-Újezd	Praha-Újezda	k1gFnPc2
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
katastrálním	katastrální	k2eAgNnSc7d1
územím	území	k1gNnSc7
Újezd	Újezd	k1gInSc1
u	u	k7c2
Průhonic	Průhonice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranice	hranice	k1gFnSc1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
mnohde	mnohde	k6eAd1
jsou	být	k5eAaImIp3nP
z	z	k7c2
praktických	praktický	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
stanoveny	stanovit	k5eAaPmNgFnP
odlišně	odlišně	k6eAd1
od	od	k7c2
hranic	hranice	k1gFnPc2
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
změnách	změna	k1gFnPc6
počtu	počet	k1gInSc2
i	i	k8xC
hranic	hranice	k1gFnPc2
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
rozhodují	rozhodovat	k5eAaImIp3nP
orgány	orgán	k1gInPc1
města	město	k1gNnSc2
v	v	k7c6
souladu	soulad	k1gInSc6
se	s	k7c7
zákonem	zákon	k1gInSc7
o	o	k7c6
hl.	hl.	k?
m.	m.	k?
Praze	Praha	k1gFnSc3
a	a	k8xC
Statutem	statut	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
změny	změna	k1gFnPc1
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
projednány	projednat	k5eAaPmNgFnP
s	s	k7c7
katastrálním	katastrální	k2eAgInSc7d1
úřadem	úřad	k1gInSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
umožnit	umožnit	k5eAaPmF
koordinaci	koordinace	k1gFnSc4
obou	dva	k4xCgInPc2
druhů	druh	k1gInPc2
členění	členění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Státní	státní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
a	a	k8xC
správní	správní	k2eAgInPc1d1
obvody	obvod	k1gInPc1
</s>
<s>
První	první	k4xOgInPc1
experimenty	experiment	k1gInPc1
se	s	k7c7
zvýšením	zvýšení	k1gNnSc7
počtu	počet	k1gInSc2
správních	správní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Obvodní	obvodní	k2eAgInPc1d1
národní	národní	k2eAgInPc1d1
výbory	výbor	k1gInPc1
vykonávaly	vykonávat	k5eAaImAgInP
od	od	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
státní	státní	k2eAgFnSc4d1
správu	správa	k1gFnSc4
pro	pro	k7c4
území	území	k1gNnSc4
původních	původní	k2eAgMnPc2d1
šestnácti	šestnáct	k4xCc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
deseti	deset	k4xCc2
obvodů	obvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1988	#num#	k4
však	však	k8xC
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
experimentům	experiment	k1gInPc3
s	s	k7c7
přenášením	přenášení	k1gNnSc7
částí	část	k1gFnPc2
obvodních	obvodní	k2eAgFnPc2d1
pravomocí	pravomoc	k1gFnPc2
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
na	na	k7c4
některé	některý	k3yIgInPc4
místní	místní	k2eAgInPc4d1
úřady	úřad	k1gInPc4
(	(	kIx(
<g/>
tehdy	tehdy	k6eAd1
MNV	MNV	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejprve	nejprve	k6eAd1
pro	pro	k7c4
MNV	MNV	kA
Chodov-Jižní	Chodov-Jižní	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
pro	pro	k7c4
Modřany	Modřany	k1gInPc4
a	a	k8xC
Jihozápadní	jihozápadní	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Praha	Praha	k1gFnSc1
1	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
15	#num#	k4
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Od	od	k7c2
1.1	1.1	k4
<g/>
.1995	.1995	k4
bylo	být	k5eAaImAgNnS
vytvořeno	vytvořit	k5eAaPmNgNnS
dalších	další	k2eAgInPc2d1
pět	pět	k4xCc1
takzvaných	takzvaný	k2eAgInPc2d1
správních	správní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
a	a	k8xC
zároveň	zároveň	k6eAd1
k	k	k7c3
přejmenování	přejmenování	k1gNnSc3
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
místní	místní	k2eAgInPc1d1
úřady	úřad	k1gInPc1
tuto	tento	k3xDgFnSc4
rozšířenou	rozšířený	k2eAgFnSc4d1
působnost	působnost	k1gFnSc4
vykonávaly	vykonávat	k5eAaImAgFnP
<g/>
:	:	kIx,
</s>
<s>
Praha	Praha	k1gFnSc1
11	#num#	k4
–	–	k?
dříve	dříve	k6eAd2
Praha-Jižní	Praha-Jižní	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
i	i	k9
pro	pro	k7c4
Praha-Újezd	Praha-Újezd	k1gInSc4
a	a	k8xC
Praha-Šeberov	Praha-Šeberov	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
12	#num#	k4
–	–	k?
dříve	dříve	k6eAd2
Praha-Modřany	Praha-Modřan	k1gMnPc7
<g/>
:	:	kIx,
(	(	kIx(
<g/>
i	i	k9
pro	pro	k7c4
Praha-Libuš	Praha-Libuš	k1gInSc4
a	a	k8xC
Praha-Kunratice	Praha-Kunratice	k1gFnPc4
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
13	#num#	k4
–	–	k?
dříve	dříve	k6eAd2
Praha-Jihozápadní	Praha-Jihozápadní	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
i	i	k9
pro	pro	k7c4
Praha-Řeporyje	Praha-Řeporyje	k1gFnPc4
<g/>
,	,	kIx,
Praha-Řepy	Praha-Řepa	k1gFnPc4
a	a	k8xC
Praha-Zličín	Praha-Zličín	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
14	#num#	k4
–	–	k?
dříve	dříve	k6eAd2
Praha-Kyje	Praha-Kyje	k1gFnSc2
<g/>
:	:	kIx,
(	(	kIx(
<g/>
i	i	k9
pro	pro	k7c4
Praha-Horní	Praha-Horní	k2eAgFnPc4d1
Počernice	Počernice	k1gFnPc4
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgFnPc4d1
Počernice	Počernice	k1gFnPc4
<g/>
,	,	kIx,
Praha-Újezd	Praha-Újezd	k1gInSc1
nad	nad	k7c7
Lesy	les	k1gInPc7
<g/>
,	,	kIx,
Praha-Klánovice	Praha-Klánovice	k1gFnPc4
<g/>
,	,	kIx,
Praha-Koloděje	Praha-Koloděje	k1gFnPc4
a	a	k8xC
Praha-Běchovice	Praha-Běchovice	k1gFnPc4
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
15	#num#	k4
–	–	k?
dříve	dříve	k6eAd2
Praha-Horní	Praha-Horní	k2eAgInPc1d1
Měcholupy	Měcholup	k1gInPc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
i	i	k9
pro	pro	k7c4
Praha-Petrovice	Praha-Petrovice	k1gFnPc4
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgFnPc4d1
Měcholupy	Měcholupa	k1gFnPc4
<g/>
,	,	kIx,
Praha-Štěrboholy	Praha-Štěrbohola	k1gFnPc4
<g/>
,	,	kIx,
Praha-Uhříněves	Praha-Uhříněves	k1gInSc1
<g/>
,	,	kIx,
Praha-Dubeč	Praha-Dubeč	k1gInSc1
<g/>
,	,	kIx,
Praha-Královice	Praha-Královice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Nedvězí	Praha-Nedvězí	k1gNnPc1
<g/>
,	,	kIx,
Praha-Kolovraty	Praha-Kolovrat	k1gInPc1
<g/>
,	,	kIx,
Praha-Benice	Praha-Benice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Křeslice	Praha-Křeslice	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
nich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
některými	některý	k3yIgFnPc7
kompetencemi	kompetence	k1gFnPc7
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
stavební	stavební	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
živnostenský	živnostenský	k2eAgInSc1d1
odbor	odbor	k1gInSc1
<g/>
)	)	kIx)
pověřeno	pověřit	k5eAaPmNgNnS
dalších	další	k2eAgInPc2d1
10	#num#	k4
místních	místní	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Praha-Libuš	Praha-Libuš	k5eAaPmIp2nS
i	i	k9
pro	pro	k7c4
Praha-Kunratice	Praha-Kunratice	k1gFnPc4
<g/>
,	,	kIx,
</s>
<s>
Praha-Zbraslav	Praha-Zbraslav	k1gMnSc1
i	i	k9
pro	pro	k7c4
Praha-Lipence	Praha-Lipence	k1gFnPc4
<g/>
,	,	kIx,
</s>
<s>
Praha-Radotín	Praha-Radotín	k1gInSc1
i	i	k9
pro	pro	k7c4
Praha-Slivenec	Praha-Slivenec	k1gInSc4
a	a	k8xC
Praha-Velká	Praha-Velký	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Praha-Řepy	Praha-Řepa	k1gFnPc4
i	i	k9
pro	pro	k7c4
Praha-Zličín	Praha-Zličín	k1gInSc4
<g/>
,	,	kIx,
</s>
<s>
Praha-Letňany	Praha-Letňan	k1gMnPc4
i	i	k9
pro	pro	k7c4
Praha-Čakovice	Praha-Čakovice	k1gFnPc4
<g/>
,	,	kIx,
</s>
<s>
Praha-Ďáblice	Praha-Ďáblice	k1gFnSc1
i	i	k9
pro	pro	k7c4
Praha-Březiněves	Praha-Březiněves	k1gInSc4
a	a	k8xC
Dolní	dolní	k2eAgInPc1d1
Chabry	Chabr	k1gInPc1
<g/>
,	,	kIx,
</s>
<s>
Praha-Kbely	Praha-Kbela	k1gFnPc4
i	i	k9
pro	pro	k7c4
Praha-Vinoř	Praha-Vinoř	k1gFnSc4
a	a	k8xC
Praha-Satalice	Praha-Satalice	k1gFnPc4
<g/>
,	,	kIx,
</s>
<s>
Praha-Horní	Praha-Horní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
pro	pro	k7c4
sebe	sebe	k3xPyFc4
<g/>
,	,	kIx,
</s>
<s>
Praha-Újezd	Praha-Újezd	k1gInSc1
nad	nad	k7c7
Lesy	les	k1gInPc7
i	i	k9
pro	pro	k7c4
Praha-Klánovice	Praha-Klánovice	k1gFnPc4
<g/>
,	,	kIx,
Praha-Koloděje	Praha-Koloděje	k1gFnPc4
a	a	k8xC
Praha-Běchovice	Praha-Běchovice	k1gFnPc4
</s>
<s>
Praha-Uhříněves	Praha-Uhříněves	k1gInSc1
i	i	k9
pro	pro	k7c4
Praha-Dubeč	Praha-Dubeč	k1gInSc4
<g/>
,	,	kIx,
Praha-Královice	Praha-Královice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Nedvězí	Praha-Nedvězí	k1gNnPc1
<g/>
,	,	kIx,
Praha-Kolovraty	Praha-Kolovrat	k1gInPc1
<g/>
,	,	kIx,
Praha-Benice	Praha-Benice	k1gFnPc1
a	a	k8xC
Praha-Křeslice	Praha-Křeslice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Praha	Praha	k1gFnSc1
1	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
22	#num#	k4
(	(	kIx(
<g/>
od	od	k7c2
r.	r.	kA
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Správní	správní	k2eAgInPc1d1
obvody	obvod	k1gInPc1
pro	pro	k7c4
některé	některý	k3yIgFnPc4
kompetence	kompetence	k1gFnPc4
přenesené	přenesený	k2eAgFnSc2d1
působnosti	působnost	k1gFnSc2
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
vykonávané	vykonávaný	k2eAgInPc1d1
úřady	úřad	k1gInPc1
22	#num#	k4
vybraných	vybraný	k2eAgFnPc2d1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Nový	nový	k2eAgInSc1d1
zákon	zákon	k1gInSc1
o	o	k7c6
hl.	hl.	k?
m.	m.	k?
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
č.	č.	k?
131	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
přejmenoval	přejmenovat	k5eAaPmAgMnS
místní	místní	k2eAgMnSc1d1
a	a	k8xC
obvodní	obvodní	k2eAgInPc1d1
úřady	úřad	k1gInPc1
na	na	k7c4
úřady	úřad	k1gInPc4
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnPc1
a	a	k8xC
názvy	název	k1gInPc1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
a	a	k8xC
vymezení	vymezení	k1gNnSc4
jejich	jejich	k3xOp3gFnSc2
působnosti	působnost	k1gFnSc2
stanovilo	stanovit	k5eAaPmAgNnS
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
v	v	k7c6
obecně	obecně	k6eAd1
závazné	závazný	k2eAgFnSc6d1
vyhlášce	vyhláška	k1gFnSc6
č.	č.	k?
55	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
HMP	HMP	kA
<g/>
,	,	kIx,
přijaté	přijatý	k2eAgFnSc3d1
zastupitelstvem	zastupitelstvo	k1gNnSc7
města	město	k1gNnSc2
<g/>
,	,	kIx,
nazvané	nazvaný	k2eAgFnSc2d1
Statut	statut	k1gInSc4
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
a	a	k8xC
účinné	účinný	k2eAgNnSc1d1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
odstraněna	odstranit	k5eAaPmNgFnS
disproporce	disproporce	k1gFnSc1
mezi	mezi	k7c7
malými	malý	k2eAgFnPc7d1
a	a	k8xC
velkými	velký	k2eAgFnPc7d1
částmi	část	k1gFnPc7
<g/>
,	,	kIx,
kromě	kromě	k7c2
dosavadních	dosavadní	k2eAgFnPc2d1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
s	s	k7c7
obvodními	obvodní	k2eAgInPc7d1
úřady	úřad	k1gInPc7
bylo	být	k5eAaImAgNnS
stanoveno	stanoven	k2eAgNnSc1d1
ještě	ještě	k6eAd1
dalších	další	k2eAgFnPc2d1
12	#num#	k4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
byly	být	k5eAaImAgFnP
přejmenovány	přejmenovat	k5eAaPmNgFnP
na	na	k7c4
Praha	Praha	k1gFnSc1
11	#num#	k4
až	až	k8xS
Praha	Praha	k1gFnSc1
22	#num#	k4
a	a	k8xC
tím	ten	k3xDgNnSc7
byla	být	k5eAaImAgFnS
Praha	Praha	k1gFnSc1
rozdělena	rozdělit	k5eAaPmNgFnS
do	do	k7c2
22	#num#	k4
takzvaných	takzvaný	k2eAgInPc2d1
správních	správní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Jak	jak	k6eAd1
vidno	vidno	k6eAd1
<g/>
,	,	kIx,
Praha-Libuš	Praha-Libuš	k1gMnSc1
<g/>
,	,	kIx,
Praha-Zbraslav	Praha-Zbraslav	k1gMnSc1
a	a	k8xC
Praha-Ďáblice	Praha-Ďáblice	k1gFnSc1
o	o	k7c4
své	svůj	k3xOyFgNnSc4
privilegované	privilegovaný	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
obvodu	obvod	k1gInSc2
opět	opět	k6eAd1
přišly	přijít	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
nyní	nyní	k6eAd1
však	však	k9
neplatí	platit	k5eNaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
všech	všecek	k3xTgInPc6
22	#num#	k4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
„	„	k?
<g/>
s	s	k7c7
čísly	číslo	k1gNnPc7
<g/>
“	“	k?
mělo	mít	k5eAaImAgNnS
navzájem	navzájem	k6eAd1
zcela	zcela	k6eAd1
stejné	stejný	k2eAgFnSc2d1
pravomoci	pravomoc	k1gFnSc2
a	a	k8xC
zbylých	zbylý	k2eAgFnPc2d1
35	#num#	k4
„	„	k?
<g/>
malých	malý	k2eAgInPc2d1
<g/>
“	“	k?
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
také	také	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesné	přesný	k2eAgNnSc1d1
rozdělení	rozdělení	k1gNnSc4
pravomocí	pravomoc	k1gFnPc2
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
je	být	k5eAaImIp3nS
komplikovaným	komplikovaný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
rozepsáno	rozepsat	k5eAaPmNgNnS
ve	v	k7c6
Statutu	statut	k1gInSc6
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Novými	nový	k2eAgInPc7d1
správními	správní	k2eAgInPc7d1
obvody	obvod	k1gInPc7
<g/>
,	,	kIx,
zaváděnými	zaváděný	k2eAgInPc7d1
postupně	postupně	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
<g/>
,	,	kIx,
vznikl	vzniknout	k5eAaPmAgInS
i	i	k9
zmatek	zmatek	k1gInSc4
a	a	k8xC
nejasnosti	nejasnost	k1gFnPc4
<g/>
,	,	kIx,
protože	protože	k8xS
původních	původní	k2eAgInPc2d1
10	#num#	k4
obvodů	obvod	k1gInPc2
zůstalo	zůstat	k5eAaPmAgNnS
stále	stále	k6eAd1
v	v	k7c6
platnosti	platnost	k1gFnSc6
pro	pro	k7c4
evidenci	evidence	k1gFnSc4
a	a	k8xC
označování	označování	k1gNnSc4
budov	budova	k1gFnPc2
a	a	k8xC
pozemků	pozemek	k1gInPc2
<g/>
,	,	kIx,
pro	pro	k7c4
soudní	soudní	k2eAgFnSc4d1
příslušnost	příslušnost	k1gFnSc4
apod.	apod.	kA
Neobeznámení	obeznámený	k2eNgMnPc1d1
lidé	člověk	k1gMnPc1
si	se	k3xPyFc3
nebyli	být	k5eNaImAgMnP
a	a	k8xC
nejsou	být	k5eNaImIp3nP
jisti	jist	k2eAgMnPc1d1
<g/>
,	,	kIx,
jestli	jestli	k8xS
bydlí	bydlet	k5eAaImIp3nS
například	například	k6eAd1
v	v	k7c6
Praze	Praha	k1gFnSc6
13	#num#	k4
nebo	nebo	k8xC
v	v	k7c6
Praze	Praha	k1gFnSc6
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správní	správní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
může	moct	k5eAaImIp3nS
zasahovat	zasahovat	k5eAaImF
i	i	k9
do	do	k7c2
více	hodně	k6eAd2
státně-územních	státně-územní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
správní	správní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
Praha	Praha	k1gFnSc1
17	#num#	k4
zasahuje	zasahovat	k5eAaImIp3nS
do	do	k7c2
územních	územní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
Praha	Praha	k1gFnSc1
5	#num#	k4
i	i	k8xC
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
1	#num#	k4
až	až	k8xS
Praha	Praha	k1gFnSc1
3	#num#	k4
jsou	být	k5eAaImIp3nP
totožné	totožný	k2eAgInPc1d1
se	s	k7c7
stejnojmennými	stejnojmenný	k2eAgInPc7d1
správními	správní	k2eAgInPc7d1
i	i	k8xC
územními	územní	k2eAgInPc7d1
obvody	obvod	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samosprávné	samosprávný	k2eAgFnPc1d1
městské	městský	k2eAgFnPc1d1
části	část	k1gFnPc1
Praha	Praha	k1gFnSc1
4	#num#	k4
až	až	k8xS
Praha	Praha	k1gFnSc1
10	#num#	k4
tvoří	tvořit	k5eAaImIp3nS
jádra	jádro	k1gNnSc2
stejnojmenných	stejnojmenný	k2eAgInPc2d1
správních	správní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
a	a	k8xC
ty	ten	k3xDgInPc1
zase	zase	k9
jádra	jádro	k1gNnPc4
stejnojmenných	stejnojmenný	k2eAgInPc2d1
územních	územní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
účinností	účinnost	k1gFnSc7
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2007	#num#	k4
byla	být	k5eAaImAgFnS
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
Praha-Čakovice	Praha-Čakovice	k1gFnSc1
přeřazena	přeřazen	k2eAgFnSc1d1
ze	z	k7c2
správního	správní	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
Praha	Praha	k1gFnSc1
19	#num#	k4
(	(	kIx(
<g/>
Kbely	Kbely	k1gInPc1
<g/>
)	)	kIx)
do	do	k7c2
správního	správní	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
Praha	Praha	k1gFnSc1
18	#num#	k4
(	(	kIx(
<g/>
Letňany	Letňan	k1gMnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Nový	nový	k2eAgInSc1d1
stav	stav	k1gInSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
přibližuje	přibližovat	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
jaký	jaký	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
zde	zde	k6eAd1
platil	platit	k5eAaImAgInS
do	do	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jednání	jednání	k1gNnSc1
o	o	k7c4
připojení	připojení	k1gNnSc4
dalších	další	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
</s>
<s>
Přezletice	Přezletika	k1gFnSc3
</s>
<s>
Na	na	k7c6
jaře	jaro	k1gNnSc6
2007	#num#	k4
vyzvalo	vyzvat	k5eAaPmAgNnS
zastupitelstvo	zastupitelstvo	k1gNnSc1
obce	obec	k1gFnSc2
Přezletice	Přezletika	k1gFnSc3
Prahu	Praha	k1gFnSc4
k	k	k7c3
jednání	jednání	k1gNnSc3
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
o	o	k7c4
připojení	připojení	k1gNnSc4
této	tento	k3xDgFnSc2
obce	obec	k1gFnSc2
k	k	k7c3
Praze	Praha	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Rudolfa	Rudolf	k1gMnSc2
Blažka	Blažek	k1gMnSc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
první	první	k4xOgInSc1
taková	takový	k3xDgFnSc1
žádost	žádost	k1gFnSc1
od	od	k7c2
roku	rok	k1gInSc2
1974	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
anketě	anketa	k1gFnSc6
se	se	k3xPyFc4
z	z	k7c2
647	#num#	k4
rozdaných	rozdaný	k2eAgInPc2d1
lístků	lístek	k1gInPc2
vrátilo	vrátit	k5eAaPmAgNnS
377	#num#	k4
a	a	k8xC
pro	pro	k7c4
připojení	připojení	k1gNnSc4
k	k	k7c3
Praze	Praha	k1gFnSc3
se	se	k3xPyFc4
vyslovilo	vyslovit	k5eAaPmAgNnS
330	#num#	k4
občanů	občan	k1gMnPc2
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
měla	mít	k5eAaImAgFnS
tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
celkem	celkem	k6eAd1
801	#num#	k4
občanů	občan	k1gMnPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
dětí	dítě	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Rada	rada	k1gFnSc1
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
jejich	jejich	k3xOp3gFnSc4
žádost	žádost	k1gFnSc4
18	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2007	#num#	k4
usnesením	usnesení	k1gNnSc7
č.	č.	k?
1415	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
vzala	vzít	k5eAaPmAgFnS
na	na	k7c4
vědomí	vědomí	k1gNnSc4
a	a	k8xC
pověřila	pověřit	k5eAaPmAgFnS
náměstka	náměstek	k1gMnSc4
primátora	primátor	k1gMnSc4
Rudolfa	Rudolf	k1gMnSc4
Blažka	Blažek	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zajistil	zajistit	k5eAaPmAgInS
jednání	jednání	k1gNnSc4
o	o	k7c6
přípravě	příprava	k1gFnSc6
připojení	připojení	k1gNnSc2
obce	obec	k1gFnSc2
Přezletice	Přezletika	k1gFnSc6
k	k	k7c3
hlavnímu	hlavní	k2eAgNnSc3d1
městu	město	k1gNnSc3
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
s	s	k7c7
termínem	termín	k1gInSc7
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2007	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
o	o	k7c6
postupech	postup	k1gInPc6
informoval	informovat	k5eAaBmAgMnS
Radu	rada	k1gFnSc4
HMP	HMP	kA
do	do	k7c2
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
se	se	k3xPyFc4
hodlá	hodlat	k5eAaImIp3nS
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
dotázat	dotázat	k5eAaPmF
dalších	další	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
v	v	k7c6
okolí	okolí	k1gNnSc6
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
zda	zda	k8xS
nemají	mít	k5eNaImIp3nP
podobné	podobný	k2eAgInPc4d1
záměry	záměr	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Změna	změna	k1gFnSc1
by	by	kYmCp3nP
si	se	k3xPyFc3
vyžádala	vyžádat	k5eAaPmAgFnS
novelizaci	novelizace	k1gFnSc4
asi	asi	k9
deseti	deset	k4xCc2
zákonů	zákon	k1gInPc2
i	i	k8xC
například	například	k6eAd1
územního	územní	k2eAgInSc2d1
plánu	plán	k1gInSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Začátkem	začátkem	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
první	první	k4xOgFnSc1
schůzka	schůzka	k1gFnSc1
zástupců	zástupce	k1gMnPc2
obou	dva	k4xCgFnPc2
obcí	obec	k1gFnPc2
a	a	k8xC
Praha	Praha	k1gFnSc1
neměla	mít	k5eNaImAgFnS
námitky	námitka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Magistrát	magistrát	k1gInSc1
hodlal	hodlat	k5eAaImAgInS
utvořit	utvořit	k5eAaPmF
pracovní	pracovní	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
konce	konec	k1gInSc2
února	únor	k1gInSc2
2008	#num#	k4
chtěl	chtít	k5eAaImAgMnS
Rudolf	Rudolf	k1gMnSc1
Blažek	Blažka	k1gFnPc2
předložit	předložit	k5eAaPmF
radě	rada	k1gFnSc3
města	město	k1gNnSc2
materiál	materiál	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
by	by	kYmCp3nS
shrnul	shrnout	k5eAaPmAgInS
další	další	k2eAgInSc4d1
postup	postup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádné	žádný	k3yNgFnSc2
další	další	k2eAgFnSc2d1
obce	obec	k1gFnSc2
neoslovil	oslovit	k5eNaPmAgMnS
a	a	k8xC
žádná	žádný	k3yNgFnSc1
další	další	k2eAgFnSc1d1
obec	obec	k1gFnSc1
se	se	k3xPyFc4
s	s	k7c7
podobnou	podobný	k2eAgFnSc7d1
žádostí	žádost	k1gFnSc7
neozvala	ozvat	k5eNaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
informací	informace	k1gFnPc2
z	z	k7c2
dubna	duben	k1gInSc2
2008	#num#	k4
měla	mít	k5eAaImAgFnS
poslední	poslední	k2eAgFnSc1d1
fáze	fáze	k1gFnSc1
přípravy	příprava	k1gFnSc2
nastat	nastat	k5eAaPmF
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Obec	obec	k1gFnSc1
nakonec	nakonec	k6eAd1
svou	svůj	k3xOyFgFnSc4
žádost	žádost	k1gFnSc4
stáhla	stáhnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
obce	obec	k1gFnPc1
</s>
<s>
Uvažuje	uvažovat	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
o	o	k7c4
připojení	připojení	k1gNnSc4
města	město	k1gNnSc2
Roztoky	roztoka	k1gFnSc2
u	u	k7c2
Prahy	Praha	k1gFnSc2
nebo	nebo	k8xC
obcí	obec	k1gFnPc2
Zdiby	Zdiba	k1gFnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
přilehlých	přilehlý	k2eAgFnPc2d1
osad	osada	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Hovorčovice	Hovorčovice	k1gFnSc1
<g/>
,	,	kIx,
Šestajovice	Šestajovice	k1gFnSc1
<g/>
,	,	kIx,
Průhonice	Průhonice	k1gFnPc1
<g/>
,	,	kIx,
Hostivice	Hostivice	k1gFnPc1
nebo	nebo	k8xC
Horoměřice	Horoměřice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
v	v	k7c6
dubnu	duben	k1gInSc6
2008	#num#	k4
uvedly	uvést	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
neoficiálně	neoficiálně	k6eAd1,k6eNd1
se	se	k3xPyFc4
hovoří	hovořit	k5eAaImIp3nS
o	o	k7c4
připojení	připojení	k1gNnPc4
obcí	obec	k1gFnPc2
Hostivice	Hostivice	k1gFnSc1
<g/>
,	,	kIx,
Vestec	Vestec	k1gInSc1
<g/>
,	,	kIx,
Šestajovice	Šestajovice	k1gFnSc1
<g/>
,	,	kIx,
Jenštejn	Jenštejn	k1gNnSc1
a	a	k8xC
Radonice	Radonice	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
u	u	k7c2
posledních	poslední	k2eAgInPc2d1
dvou	dva	k4xCgInPc2
je	být	k5eAaImIp3nS
žádost	žádost	k1gFnSc1
málo	málo	k6eAd1
pravděpodobná	pravděpodobný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
schválilo	schválit	k5eAaPmAgNnS
záměr	záměr	k1gInSc4
připojení	připojení	k1gNnSc2
k	k	k7c3
Praze	Praha	k1gFnSc6
zastupitelstvo	zastupitelstvo	k1gNnSc1
krušnohorské	krušnohorský	k2eAgFnSc2d1
obce	obec	k1gFnSc2
Hora	Hora	k1gMnSc1
Svaté	svatý	k2eAgFnSc2d1
Kateřiny	Kateřina	k1gFnSc2
a	a	k8xC
vyjádřilo	vyjádřit	k5eAaPmAgNnS
přání	přání	k1gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
tak	tak	k6eAd1
učinily	učinit	k5eAaPmAgFnP,k5eAaImAgFnP
i	i	k9
další	další	k2eAgFnPc1d1
obce	obec	k1gFnPc1
jako	jako	k8xS,k8xC
protest	protest	k1gInSc1
proti	proti	k7c3
způsobu	způsob	k1gInSc3
rozdělování	rozdělování	k1gNnSc2
peněz	peníze	k1gInPc2
ze	z	k7c2
státního	státní	k2eAgInSc2d1
rozpočtu	rozpočet	k1gInSc2
obcím	obec	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Současné	současný	k2eAgFnPc1d1
části	část	k1gFnPc1
a	a	k8xC
obvody	obvod	k1gInPc1
</s>
<s>
Městské	městský	k2eAgFnSc3d1
části	část	k1gFnSc3
Prahy	Praha	k1gFnSc2
</s>
<s>
Praha	Praha	k1gFnSc1
se	se	k3xPyFc4
člení	členit	k5eAaImIp3nS
na	na	k7c4
10	#num#	k4
městských	městský	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
,	,	kIx,
22	#num#	k4
správních	správní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
a	a	k8xC
57	#num#	k4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správní	správní	k2eAgInPc4d1
obvody	obvod	k1gInPc4
upravuje	upravovat	k5eAaImIp3nS
s	s	k7c7
účinností	účinnost	k1gFnSc7
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2001	#num#	k4
Statut	statuta	k1gNnPc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
22	#num#	k4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
(	(	kIx(
<g/>
jednotek	jednotka	k1gFnPc2
územní	územní	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
<g/>
)	)	kIx)
vykonává	vykonávat	k5eAaImIp3nS
některé	některý	k3yIgFnPc4
přenesené	přenesený	k2eAgFnPc4d1
působnosti	působnost	k1gFnPc4
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
i	i	k9
pro	pro	k7c4
jiné	jiný	k2eAgFnPc4d1
městské	městský	k2eAgFnPc4d1
části	část	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
těchto	tento	k3xDgFnPc2
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
společně	společně	k6eAd1
označováno	označovat	k5eAaImNgNnS
jako	jako	k8xS,k8xC
správní	správní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Praha	Praha	k1gFnSc1
se	se	k3xPyFc4
člení	členit	k5eAaImIp3nS
na	na	k7c4
celkem	celkem	k6eAd1
57	#num#	k4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městské	městský	k2eAgFnPc4d1
části	část	k1gFnPc4
poprvé	poprvé	k6eAd1
stanovil	stanovit	k5eAaPmAgInS
s	s	k7c7
účinností	účinnost	k1gFnSc7
od	od	k7c2
24	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1990	#num#	k4
dnes	dnes	k6eAd1
již	již	k6eAd1
zrušený	zrušený	k2eAgInSc1d1
zákon	zákon	k1gInSc1
č.	č.	k?
418	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavení	postavení	k1gNnSc1
a	a	k8xC
působnost	působnost	k1gFnSc1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
upravuje	upravovat	k5eAaImIp3nS
zákon	zákon	k1gInSc1
č.	č.	k?
131	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
zvláštní	zvláštní	k2eAgInSc1d1
zákon	zákon	k1gInSc1
a	a	k8xC
obecně	obecně	k6eAd1
závazná	závazný	k2eAgFnSc1d1
vyhláška	vyhláška	k1gFnSc1
č.	č.	k?
55	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
samostatný	samostatný	k2eAgInSc1d1
celek	celek	k1gInSc1
spravovaný	spravovaný	k2eAgInSc1d1
voleným	volený	k2eAgNnSc7d1
zastupitelstvem	zastupitelstvo	k1gNnSc7
<g/>
,	,	kIx,
radou	rada	k1gFnSc7
a	a	k8xC
úřadem	úřad	k1gInSc7
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Praha	Praha	k1gFnSc1
i	i	k9
každá	každý	k3xTgFnSc1
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
sama	sám	k3xTgFnSc1
hospodaří	hospodařit	k5eAaImIp3nS
s	s	k7c7
vlastním	vlastní	k2eAgInSc7d1
rozpočtem	rozpočet	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
sestavován	sestavovat	k5eAaImNgInS
individuálně	individuálně	k6eAd1
dle	dle	k7c2
potřeb	potřeba	k1gFnPc2
dané	daný	k2eAgFnSc2d1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Praha	Praha	k1gFnSc1
se	se	k3xPyFc4
člení	členit	k5eAaImIp3nS
současně	současně	k6eAd1
na	na	k7c4
dalších	další	k2eAgNnPc2d1
celkem	celkem	k6eAd1
10	#num#	k4
městských	městský	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
číslovány	číslován	k2eAgInPc1d1
také	také	k9
arabskými	arabský	k2eAgFnPc7d1
číslicemi	číslice	k1gFnPc7
1	#num#	k4
až	až	k9
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
městské	městský	k2eAgInPc4d1
obvody	obvod	k1gInPc4
poprvé	poprvé	k6eAd1
stanovil	stanovit	k5eAaPmAgInS
s	s	k7c7
účinností	účinnost	k1gFnSc7
od	od	k7c2
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1960	#num#	k4
i	i	k8xC
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
platný	platný	k2eAgInSc4d1
zákon	zákon	k1gInSc4
č.	č.	k?
36	#num#	k4
<g/>
/	/	kIx~
<g/>
1960	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
územním	územní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Vymezení	vymezení	k1gNnSc4
těchto	tento	k3xDgInPc2
obvodů	obvod	k1gInPc2
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
upravuje	upravovat	k5eAaImIp3nS
vyhláška	vyhláška	k1gFnSc1
ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
č.	č.	k?
564	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c4
stanovení	stanovení	k1gNnSc4
území	území	k1gNnSc2
okresů	okres	k1gInPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
území	území	k1gNnSc6
obvodů	obvod	k1gInPc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
podobné	podobný	k2eAgFnSc6d1
bázi	báze	k1gFnSc6
fungují	fungovat	k5eAaImIp3nP
v	v	k7c6
ostatních	ostatní	k2eAgInPc6d1
krajích	kraj	k1gInPc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
okresy	okres	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
této	tento	k3xDgFnSc6
úrovni	úroveň	k1gFnSc6
funguje	fungovat	k5eAaImIp3nS
řada	řada	k1gFnSc1
článků	článek	k1gInPc2
řízení	řízení	k1gNnSc2
z	z	k7c2
jiných	jiný	k2eAgFnPc2d1
než	než	k8xS
administrativně	administrativně	k6eAd1
správních	správní	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
soudy	soud	k1gInPc1
<g/>
,	,	kIx,
pošty	pošta	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
městských	městský	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
</s>
<s>
Praha	Praha	k1gFnSc1
se	se	k3xPyFc4
i	i	k9
nadále	nadále	k6eAd1
člení	členit	k5eAaImIp3nS
dle	dle	k7c2
zákona	zákon	k1gInSc2
o	o	k7c6
územním	územní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
na	na	k7c4
10	#num#	k4
městských	městský	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
jsou	být	k5eAaImIp3nP
zároveň	zároveň	k6eAd1
soudními	soudní	k2eAgInPc7d1
obvody	obvod	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městské	městský	k2eAgInPc1d1
obvody	obvod	k1gInPc1
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2	#num#	k4
a	a	k8xC
Praha	Praha	k1gFnSc1
3	#num#	k4
jsou	být	k5eAaImIp3nP
územně	územně	k6eAd1
totožné	totožný	k2eAgInPc1d1
se	s	k7c7
stejnojmennými	stejnojmenný	k2eAgFnPc7d1
městskými	městský	k2eAgFnPc7d1
částmi	část	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgInPc1d1
městské	městský	k2eAgInPc1d1
obvody	obvod	k1gInPc1
jsou	být	k5eAaImIp3nP
již	již	k6eAd1
pouze	pouze	k6eAd1
formálními	formální	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
a	a	k8xC
nemají	mít	k5eNaImIp3nP
vlastní	vlastní	k2eAgFnSc4d1
samosprávu	samospráva	k1gFnSc4
ani	ani	k8xC
obecné	obecný	k2eAgInPc4d1
úřady	úřad	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
odlišné	odlišný	k2eAgInPc4d1
obvody	obvod	k1gInPc4
od	od	k7c2
22	#num#	k4
správních	správní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
,	,	kIx,
zavedených	zavedený	k2eAgFnPc2d1
Statutem	statut	k1gInSc7
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
22	#num#	k4
správních	správní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
22	#num#	k4
tzv.	tzv.	kA
velkých	velký	k2eAgFnPc2d1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
s	s	k7c7
názvy	název	k1gInPc7
Praha	Praha	k1gFnSc1
1	#num#	k4
až	až	k8xS
Praha	Praha	k1gFnSc1
22	#num#	k4
má	mít	k5eAaImIp3nS
rozšířenou	rozšířený	k2eAgFnSc4d1
působnost	působnost	k1gFnSc4
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
pro	pro	k7c4
své	svůj	k3xOyFgNnSc4
vlastní	vlastní	k2eAgNnSc4d1
území	území	k1gNnSc4
a	a	k8xC
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
i	i	k9
pro	pro	k7c4
další	další	k2eAgFnPc4d1
městské	městský	k2eAgFnPc4d1
části	část	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Praha	Praha	k1gFnSc1
1	#num#	k4
–	–	k?
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
Josefov	Josefov	k1gInSc1
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Hradčan	Hradčany	k1gInPc2
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Malé	Malé	k2eAgFnSc2d1
Strany	strana	k1gFnSc2
<g/>
,	,	kIx,
část	část	k1gFnSc4
Nového	Nového	k2eAgNnSc2d1
Města	město	k1gNnSc2
<g/>
,	,	kIx,
nepatrná	patrný	k2eNgFnSc1d1,k2eAgFnSc1d1
část	část	k1gFnSc1
Holešovic	Holešovice	k1gFnPc2
<g/>
,	,	kIx,
nepatrná	nepatrný	k2eAgFnSc1d1,k2eNgFnSc1d1
část	část	k1gFnSc1
Vinohrad	Vinohrady	k1gInPc2
</s>
<s>
Praha	Praha	k1gFnSc1
2	#num#	k4
–	–	k?
část	část	k1gFnSc4
Vinohrad	Vinohrady	k1gInPc2
<g/>
,	,	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
Nového	Nového	k2eAgNnSc2d1
Města	město	k1gNnSc2
<g/>
,	,	kIx,
menší	malý	k2eAgFnSc1d2
část	část	k1gFnSc1
Nuslí	Nusle	k1gFnPc2
(	(	kIx(
<g/>
Nuselské	nuselský	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
3	#num#	k4
–	–	k?
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Žižkova	Žižkov	k1gInSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
Vinohrad	Vinohrady	k1gInPc2
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
Vysočan	Vysočany	k1gInPc2
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
Strašnic	Strašnice	k1gFnPc2
</s>
<s>
Praha	Praha	k1gFnSc1
4	#num#	k4
–	–	k?
Braník	Braník	k1gInSc1
<g/>
,	,	kIx,
Hodkovičky	Hodkovička	k1gFnPc1
<g/>
,	,	kIx,
Krč	Krč	k1gFnSc1
<g/>
,	,	kIx,
Lhotka	Lhotka	k1gFnSc1
<g/>
,	,	kIx,
Podolí	Podolí	k1gNnSc1
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Michle	Michl	k1gMnSc5
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Nuslí	Nusle	k1gFnPc2
<g/>
,	,	kIx,
menší	malý	k2eAgFnSc1d2
část	část	k1gFnSc1
Záběhlic	Záběhlice	k1gFnPc2
<g/>
,	,	kIx,
nepatrná	patrný	k2eNgFnSc1d1,k2eAgFnSc1d1
část	část	k1gFnSc1
Vinohrad	Vinohrady	k1gInPc2
</s>
<s>
Praha-Kunratice	Praha-Kunratice	k1gFnPc4
–	–	k?
Kunratice	Kunratice	k1gFnPc4
</s>
<s>
Praha	Praha	k1gFnSc1
5	#num#	k4
–	–	k?
Smíchov	Smíchov	k1gInSc1
<g/>
,	,	kIx,
Hlubočepy	Hlubočep	k1gInPc1
<g/>
,	,	kIx,
Radlice	Radlice	k1gInPc1
<g/>
,	,	kIx,
Košíře	Košíře	k1gInPc1
<g/>
,	,	kIx,
Motol	Motol	k1gInSc1
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Jinonic	Jinonice	k1gFnPc2
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
Malé	Malé	k2eAgFnSc2d1
Strany	strana	k1gFnSc2
<g/>
,	,	kIx,
nepatrná	nepatrný	k2eAgFnSc1d1,k2eNgFnSc1d1
část	část	k1gFnSc1
Břevnova	Břevnov	k1gInSc2
</s>
<s>
Praha-Slivenec	Praha-Slivenec	k1gMnSc1
–	–	k?
Slivenec	Slivenec	k1gMnSc1
<g/>
,	,	kIx,
Holyně	Holyně	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
6	#num#	k4
–	–	k?
Dejvice	Dejvice	k1gFnPc1
<g/>
,	,	kIx,
Střešovice	Střešovice	k1gFnPc1
<g/>
,	,	kIx,
Veleslavín	Veleslavín	k1gInSc1
<g/>
,	,	kIx,
Vokovice	Vokovice	k1gFnSc1
<g/>
,	,	kIx,
Ruzyně	Ruzyně	k1gFnSc1
<g/>
,	,	kIx,
Liboc	Liboc	k1gFnSc1
<g/>
,	,	kIx,
téměř	téměř	k6eAd1
celý	celý	k2eAgInSc4d1
Břevnov	Břevnov	k1gInSc4
<g/>
,	,	kIx,
větší	veliký	k2eAgFnSc4d2
část	část	k1gFnSc4
Bubenče	Bubeneč	k1gInSc2
<g/>
,	,	kIx,
dolní	dolní	k2eAgFnSc1d1
část	část	k1gFnSc1
Sedlce	Sedlec	k1gInSc2
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
Hradčan	Hradčany	k1gInPc2
</s>
<s>
Praha-Suchdol	Praha-Suchdol	k1gInSc1
–	–	k?
Suchdol	Suchdol	k1gInSc1
<g/>
,	,	kIx,
horní	horní	k2eAgFnSc1d1
část	část	k1gFnSc1
Sedlce	Sedlec	k1gInSc2
</s>
<s>
Praha-Lysolaje	Praha-Lysolat	k5eAaImSgMnS
–	–	k?
Lysolaje	Lysolaje	k1gFnPc5
</s>
<s>
Praha-Nebušice	Praha-Nebušice	k1gFnSc1
–	–	k?
Nebušice	Nebušice	k1gFnSc2
</s>
<s>
Praha-Přední	Praha-Přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
–	–	k?
Přední	přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
7	#num#	k4
–	–	k?
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Holešovic	Holešovice	k1gFnPc2
<g/>
,	,	kIx,
menší	malý	k2eAgFnSc1d2
část	část	k1gFnSc1
Bubenče	Bubeneč	k1gInSc2
<g/>
,	,	kIx,
nepatrná	patrný	k2eNgFnSc1d1,k2eAgFnSc1d1
část	část	k1gFnSc1
Libně	Libeň	k1gFnSc2
</s>
<s>
Praha-Troja	Praha-Troja	k1gFnSc1
–	–	k?
dolní	dolní	k2eAgFnSc4d1
část	část	k1gFnSc4
Troji	troje	k4xRgMnPc1
</s>
<s>
Praha	Praha	k1gFnSc1
8	#num#	k4
–	–	k?
větší	veliký	k2eAgFnSc1d2
část	část	k1gFnSc1
Libně	Libeň	k1gFnSc2
<g/>
,	,	kIx,
Karlín	Karlín	k1gInSc1
<g/>
,	,	kIx,
Kobylisy	Kobylisy	k1gInPc1
<g/>
,	,	kIx,
Bohnice	Bohnice	k1gInPc1
<g/>
,	,	kIx,
Čimice	Čimice	k1gInPc1
<g/>
,	,	kIx,
významná	významný	k2eAgFnSc1d1
část	část	k1gFnSc1
Troji	troje	k4xRgFnSc4
(	(	kIx(
<g/>
horní	horní	k2eAgFnSc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
Nového	Nového	k2eAgNnSc2d1
Města	město	k1gNnSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
Střížkova	Střížkův	k2eAgFnSc1d1
<g/>
,	,	kIx,
nepatrná	patrný	k2eNgFnSc1d1,k2eAgFnSc1d1
část	část	k1gFnSc1
Žižkova	Žižkov	k1gInSc2
</s>
<s>
Praha-Ďáblice	Praha-Ďáblice	k1gFnSc1
–	–	k?
Ďáblice	ďáblice	k1gFnSc2
</s>
<s>
Praha-Březiněves	Praha-Březiněves	k1gMnSc1
–	–	k?
Březiněves	Březiněves	k1gMnSc1
</s>
<s>
Praha-Dolní	Praha-Dolní	k2eAgFnPc1d1
Chabry	Chabra	k1gFnPc1
–	–	k?
Dolní	dolní	k2eAgFnPc1d1
Chabry	Chabra	k1gFnPc1
</s>
<s>
Praha	Praha	k1gFnSc1
9	#num#	k4
–	–	k?
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Vysočan	Vysočany	k1gInPc2
<g/>
,	,	kIx,
menší	malý	k2eAgFnSc4d2
část	část	k1gFnSc4
Libně	Libeň	k1gFnSc2
<g/>
,	,	kIx,
Prosek	proséct	k5eAaPmDgInS
<g/>
,	,	kIx,
část	část	k1gFnSc1
Střížkova	Střížkův	k2eAgFnSc1d1
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Hrdlořez	Hrdlořezy	k1gInPc2
<g/>
,	,	kIx,
menší	malý	k2eAgFnSc1d2
část	část	k1gFnSc1
Hloubětína	Hloubětína	k1gFnSc1
<g/>
,	,	kIx,
nepatrná	patrný	k2eNgFnSc1d1,k2eAgFnSc1d1
část	část	k1gFnSc1
Malešic	Malešice	k1gFnPc2
</s>
<s>
Praha	Praha	k1gFnSc1
10	#num#	k4
–	–	k?
Vršovice	Vršovice	k1gFnPc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
Vinohrad	Vinohrady	k1gInPc2
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Strašnic	Strašnice	k1gFnPc2
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Malešic	Malešice	k1gFnPc2
<g/>
,	,	kIx,
větší	veliký	k2eAgFnSc1d2
část	část	k1gFnSc1
Záběhlic	Záběhlice	k1gFnPc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
Michle	Michl	k1gMnSc5
(	(	kIx(
<g/>
vrch	vrch	k1gInSc1
Bohdalec	Bohdalec	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nepatrná	nepatrný	k2eAgFnSc1d1,k2eNgFnSc1d1
část	část	k1gFnSc1
Žižkova	Žižkov	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
dům	dům	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nepatrná	nepatrný	k2eAgFnSc1d1,k2eNgFnSc1d1
nezastavěná	zastavěný	k2eNgFnSc1d1
část	část	k1gFnSc1
Hrdlořez	Hrdlořezy	k1gInPc2
<g/>
,	,	kIx,
nepatrná	nepatrný	k2eAgFnSc1d1,k2eNgFnSc1d1
nezastavěná	zastavěný	k2eNgFnSc1d1
část	část	k1gFnSc1
Hloubětína	Hloubětín	k1gInSc2
</s>
<s>
Praha	Praha	k1gFnSc1
11	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Jižní	Praha-Jižní	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
Chodov	Chodov	k1gInSc1
<g/>
,	,	kIx,
Háje	háj	k1gInPc1
</s>
<s>
Praha-Šeberov	Praha-Šeberov	k1gInSc1
–	–	k?
Šeberov	Šeberov	k1gInSc1
</s>
<s>
Praha-Újezd	Praha-Újezd	k1gInSc1
–	–	k?
Újezd	Újezd	k1gInSc1
u	u	k7c2
Průhonic	Průhonice	k1gFnPc2
</s>
<s>
Praha-Křeslice	Praha-Křeslice	k1gFnSc1
–	–	k?
Křeslice	Křeslice	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
12	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Modřany	Praha-Modřan	k1gMnPc4
<g/>
)	)	kIx)
–	–	k?
Modřany	Modřany	k1gInPc7
<g/>
,	,	kIx,
Komořany	Komořan	k1gMnPc7
<g/>
,	,	kIx,
Točná	točný	k2eAgFnSc1d1
<g/>
,	,	kIx,
Cholupice	Cholupice	k1gFnSc1
<g/>
,	,	kIx,
Kamýk	Kamýk	k1gInSc1
</s>
<s>
Praha-Libuš	Praha-Libuš	k1gMnSc1
–	–	k?
Libuš	Libuš	k1gMnSc1
<g/>
,	,	kIx,
Písnice	písnice	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
13	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Jihozápadní	Praha-Jihozápadní	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Stodůlek	stodůlka	k1gFnPc2
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Třebonic	Třebonice	k1gFnPc2
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
okrajová	okrajový	k2eAgFnSc1d1
část	část	k1gFnSc1
Jinonic	Jinonice	k1gFnPc2
<g/>
,	,	kIx,
nepatrná	nepatrný	k2eAgFnSc1d1,k2eNgFnSc1d1
nezastavěná	zastavěný	k2eNgFnSc1d1
část	část	k1gFnSc1
Řeporyj	Řeporyj	k1gFnSc1
</s>
<s>
Praha-Řeporyje	Praha-Řeporýt	k5eAaPmIp3nS
–	–	k?
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Řeporyj	Řeporyj	k1gFnSc1
<g/>
,	,	kIx,
Zadní	zadní	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
Stodůlek	stodůlka	k1gFnPc2
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
Třebonic	Třebonice	k1gFnPc2
</s>
<s>
Praha	Praha	k1gFnSc1
14	#num#	k4
–	–	k?
Kyje	kyje	k1gFnPc1
<g/>
,	,	kIx,
Hostavice	Hostavice	k1gFnPc1
<g/>
,	,	kIx,
Černý	černý	k2eAgInSc1d1
Most	most	k1gInSc1
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Hloubětína	Hloubětín	k1gInSc2
</s>
<s>
Praha-Dolní	Praha-Dolní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
–	–	k?
Dolní	dolní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
</s>
<s>
Praha	Praha	k1gFnSc1
15	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Horní	Praha-Horní	k2eAgFnSc2d1
Měcholupy	Měcholupa	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
Horní	horní	k2eAgFnSc2d1
Měcholupy	Měcholupa	k1gFnSc2
<g/>
,	,	kIx,
Hostivař	Hostivař	k1gFnSc1
</s>
<s>
Praha-Dolní	Praha-Dolní	k2eAgFnPc1d1
Měcholupy	Měcholupa	k1gFnPc1
–	–	k?
Dolní	dolní	k2eAgFnPc1d1
Měcholupy	Měcholupa	k1gFnPc1
</s>
<s>
Praha-Štěrboholy	Praha-Štěrbohola	k1gFnPc1
–	–	k?
Štěrboholy	Štěrbohola	k1gFnSc2
</s>
<s>
Praha-Petrovice	Praha-Petrovice	k1gFnSc1
–	–	k?
Petrovice	Petrovice	k1gFnSc1
</s>
<s>
Praha-Dubeč	Praha-Dubeč	k1gMnSc1
–	–	k?
Dubeč	Dubeč	k1gMnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
16	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Radotín	Praha-Radotín	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
Radotín	Radotín	k1gInSc1
</s>
<s>
Praha-Velká	Praha-Velký	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
–	–	k?
Velká	velký	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
<g/>
,	,	kIx,
Malá	malý	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
</s>
<s>
Praha-Lochkov	Praha-Lochkov	k1gInSc1
–	–	k?
Lochkov	Lochkov	k1gInSc1
</s>
<s>
Praha-Zbraslav	Praha-Zbraslav	k1gMnSc1
–	–	k?
Zbraslav	Zbraslav	k1gFnSc1
<g/>
,	,	kIx,
Lahovice	Lahovice	k1gFnSc1
</s>
<s>
Praha-Lipence	Praha-Lipence	k1gFnSc1
–	–	k?
Lipence	Lipence	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
17	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Řepy	Praha-Řepa	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
Řepy	řepa	k1gFnSc2
</s>
<s>
Praha-Zličín	Praha-Zličín	k1gMnSc1
–	–	k?
Zličín	Zličín	k1gMnSc1
<g/>
,	,	kIx,
Sobín	Sobín	k1gMnSc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
Třebonic	Třebonice	k1gFnPc2
(	(	kIx(
<g/>
okolí	okolí	k1gNnSc1
stanice	stanice	k1gFnSc2
metra	metro	k1gNnSc2
Zličín	Zličína	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
18	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Letňany	Praha-Letňan	k1gMnPc4
<g/>
)	)	kIx)
–	–	k?
Letňany	Letňan	k1gMnPc4
</s>
<s>
Praha-Čakovice	Praha-Čakovice	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
)	)	kIx)
–	–	k?
Čakovice	Čakovice	k1gFnSc1
<g/>
,	,	kIx,
Třeboradice	Třeboradice	k1gFnSc1
<g/>
,	,	kIx,
Miškovice	Miškovice	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
19	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Kbely	Praha-Kbela	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
Kbely	Kbely	k1gInPc1
</s>
<s>
Praha-Vinoř	Praha-Vinoř	k1gFnSc1
–	–	k?
Vinoř	Vinoř	k1gFnSc1
</s>
<s>
Praha-Satalice	Praha-Satalice	k1gFnSc1
–	–	k?
Satalice	Satalice	k1gFnSc1
</s>
<s>
Praha-Čakovice	Praha-Čakovice	k1gFnSc1
(	(	kIx(
<g/>
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
)	)	kIx)
–	–	k?
Čakovice	Čakovice	k1gFnSc1
<g/>
,	,	kIx,
Třeboradice	Třeboradice	k1gFnSc1
<g/>
,	,	kIx,
Miškovice	Miškovice	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
20	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Horní	Praha-Horní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
<g/>
)	)	kIx)
–	–	k?
Horní	horní	k2eAgFnPc4d1
Počernice	Počernice	k1gFnPc4
</s>
<s>
Praha	Praha	k1gFnSc1
21	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Újezd	Praha-Újezd	k1gInSc1
nad	nad	k7c7
Lesy	les	k1gInPc7
<g/>
)	)	kIx)
–	–	k?
Újezd	Újezd	k1gInSc1
nad	nad	k7c7
Lesy	les	k1gInPc7
</s>
<s>
Praha-Klánovice	Praha-Klánovice	k1gFnPc4
–	–	k?
Klánovice	Klánovice	k1gFnPc4
</s>
<s>
Praha-Koloděje	Praha-Kolodět	k5eAaPmIp3nS
–	–	k?
Koloděje	Koloděje	k1gFnPc4
</s>
<s>
Praha-Běchovice	Praha-Běchovice	k1gFnPc4
–	–	k?
Běchovice	Běchovice	k1gFnPc4
</s>
<s>
Praha	Praha	k1gFnSc1
22	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Uhříněves	Praha-Uhříněves	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
Uhříněves	Uhříněves	k1gFnSc1
<g/>
,	,	kIx,
Hájek	Hájek	k1gMnSc1
u	u	k7c2
Uhříněvsi	Uhříněves	k1gFnSc2
<g/>
,	,	kIx,
Pitkovice	Pitkovice	k1gFnSc2
</s>
<s>
Praha-Královice	Praha-Královice	k1gFnSc1
–	–	k?
Královice	královic	k1gMnSc2
</s>
<s>
Praha-Nedvězí	Praha-Nedvězí	k1gNnSc1
–	–	k?
Nedvězí	Nedvěze	k1gFnPc2
u	u	k7c2
Říčan	Říčany	k1gInPc2
</s>
<s>
Praha-Kolovraty	Praha-Kolovrat	k1gInPc1
–	–	k?
Kolovraty	kolovrat	k1gInPc7
<g/>
,	,	kIx,
Lipany	lipan	k1gMnPc7
</s>
<s>
Praha-Benice	Praha-Benice	k1gFnSc1
–	–	k?
Benice	Benice	k1gFnSc1
</s>
<s>
Městské	městský	k2eAgFnSc3d1
části	část	k1gFnSc3
</s>
<s>
Samosprávných	samosprávný	k2eAgFnPc2d1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
má	mít	k5eAaImIp3nS
Praha	Praha	k1gFnSc1
celkem	celek	k1gInSc7
57	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
22	#num#	k4
tzv.	tzv.	kA
velkých	velký	k2eAgFnPc2d1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
(	(	kIx(
<g/>
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Praha	Praha	k1gFnSc1
1	#num#	k4
–	–	k?
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
Josefov	Josefov	k1gInSc1
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Hradčan	Hradčany	k1gInPc2
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Malé	Malé	k2eAgFnSc2d1
Strany	strana	k1gFnSc2
<g/>
,	,	kIx,
část	část	k1gFnSc4
Nového	Nového	k2eAgNnSc2d1
Města	město	k1gNnSc2
<g/>
,	,	kIx,
nepatrná	nepatrný	k2eAgFnSc1d1,k2eNgFnSc1d1
část	část	k1gFnSc1
Holešovic	Holešovice	k1gFnPc2
<g/>
,	,	kIx,
nepatrná	patrný	k2eNgFnSc1d1,k2eAgFnSc1d1
část	část	k1gFnSc1
Vinohrad	Vinohrady	k1gInPc2
</s>
<s>
Praha	Praha	k1gFnSc1
2	#num#	k4
–	–	k?
část	část	k1gFnSc4
Vinohrad	Vinohrady	k1gInPc2
<g/>
,	,	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
Nového	Nového	k2eAgNnSc2d1
Města	město	k1gNnSc2
<g/>
,	,	kIx,
menší	malý	k2eAgFnSc1d2
část	část	k1gFnSc1
Nuslí	Nusle	k1gFnPc2
(	(	kIx(
<g/>
Nuselské	nuselský	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
3	#num#	k4
–	–	k?
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Žižkova	Žižkov	k1gInSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
Vinohrad	Vinohrady	k1gInPc2
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
Vysočan	Vysočany	k1gInPc2
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
Strašnic	Strašnice	k1gFnPc2
</s>
<s>
Praha	Praha	k1gFnSc1
4	#num#	k4
–	–	k?
Braník	Braník	k1gInSc1
<g/>
,	,	kIx,
Hodkovičky	Hodkovička	k1gFnPc1
<g/>
,	,	kIx,
Krč	Krč	k1gFnSc1
<g/>
,	,	kIx,
Lhotka	Lhotka	k1gFnSc1
<g/>
,	,	kIx,
Podolí	Podolí	k1gNnSc1
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Michle	Michl	k1gMnSc5
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Nuslí	Nusle	k1gFnPc2
<g/>
,	,	kIx,
menší	malý	k2eAgFnSc1d2
část	část	k1gFnSc1
Záběhlic	Záběhlice	k1gFnPc2
<g/>
,	,	kIx,
nepatrná	nepatrný	k2eAgFnSc1d1,k2eNgFnSc1d1
část	část	k1gFnSc1
Vinohrad	Vinohrady	k1gInPc2
</s>
<s>
Praha	Praha	k1gFnSc1
5	#num#	k4
–	–	k?
Smíchov	Smíchov	k1gInSc1
<g/>
,	,	kIx,
Hlubočepy	Hlubočep	k1gInPc1
<g/>
,	,	kIx,
Radlice	Radlice	k1gInPc1
<g/>
,	,	kIx,
Košíře	Košíře	k1gInPc1
<g/>
,	,	kIx,
Motol	Motol	k1gInSc1
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Jinonic	Jinonice	k1gFnPc2
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
Malé	Malé	k2eAgFnSc2d1
Strany	strana	k1gFnSc2
<g/>
,	,	kIx,
nepatrná	patrný	k2eNgFnSc1d1,k2eAgFnSc1d1
část	část	k1gFnSc1
Břevnova	Břevnov	k1gInSc2
</s>
<s>
Praha	Praha	k1gFnSc1
6	#num#	k4
–	–	k?
Dejvice	Dejvice	k1gFnPc1
<g/>
,	,	kIx,
Střešovice	Střešovice	k1gFnPc1
<g/>
,	,	kIx,
Veleslavín	Veleslavín	k1gInSc1
<g/>
,	,	kIx,
Vokovice	Vokovice	k1gFnSc1
<g/>
,	,	kIx,
Ruzyně	Ruzyně	k1gFnSc1
<g/>
,	,	kIx,
Liboc	Liboc	k1gFnSc1
<g/>
,	,	kIx,
téměř	téměř	k6eAd1
celý	celý	k2eAgInSc4d1
Břevnov	Břevnov	k1gInSc4
<g/>
,	,	kIx,
větší	veliký	k2eAgFnSc4d2
část	část	k1gFnSc4
Bubenče	Bubeneč	k1gInSc2
<g/>
,	,	kIx,
dolní	dolní	k2eAgFnSc1d1
část	část	k1gFnSc1
Sedlce	Sedlec	k1gInSc2
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
Hradčan	Hradčany	k1gInPc2
</s>
<s>
Praha	Praha	k1gFnSc1
7	#num#	k4
–	–	k?
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Holešovic	Holešovice	k1gFnPc2
<g/>
,	,	kIx,
menší	malý	k2eAgFnSc1d2
část	část	k1gFnSc1
Bubenče	Bubeneč	k1gInSc2
<g/>
,	,	kIx,
nepatrná	patrný	k2eNgFnSc1d1,k2eAgFnSc1d1
část	část	k1gFnSc1
Libně	Libeň	k1gFnSc2
</s>
<s>
Praha	Praha	k1gFnSc1
8	#num#	k4
–	–	k?
větší	veliký	k2eAgFnSc1d2
část	část	k1gFnSc1
Libně	Libeň	k1gFnSc2
<g/>
,	,	kIx,
Karlín	Karlín	k1gInSc1
<g/>
,	,	kIx,
Kobylisy	Kobylisy	k1gInPc1
<g/>
,	,	kIx,
Bohnice	Bohnice	k1gInPc1
<g/>
,	,	kIx,
Čimice	Čimice	k1gInPc1
<g/>
,	,	kIx,
významná	významný	k2eAgFnSc1d1
část	část	k1gFnSc1
Troji	troje	k4xRgFnSc4
(	(	kIx(
<g/>
horní	horní	k2eAgFnSc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
Nového	Nového	k2eAgNnSc2d1
Města	město	k1gNnSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
Střížkova	Střížkův	k2eAgFnSc1d1
<g/>
,	,	kIx,
nepatrná	nepatrný	k2eAgFnSc1d1,k2eNgFnSc1d1
část	část	k1gFnSc1
Žižkova	Žižkov	k1gInSc2
</s>
<s>
Praha	Praha	k1gFnSc1
9	#num#	k4
–	–	k?
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Vysočan	Vysočany	k1gInPc2
<g/>
,	,	kIx,
menší	malý	k2eAgFnSc4d2
část	část	k1gFnSc4
Libně	Libeň	k1gFnSc2
<g/>
,	,	kIx,
Prosek	proséct	k5eAaPmDgInS
<g/>
,	,	kIx,
část	část	k1gFnSc1
Střížkova	Střížkův	k2eAgFnSc1d1
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Hrdlořez	Hrdlořezy	k1gInPc2
<g/>
,	,	kIx,
menší	malý	k2eAgFnSc1d2
část	část	k1gFnSc1
Hloubětína	Hloubětína	k1gFnSc1
<g/>
,	,	kIx,
nepatrná	patrný	k2eNgFnSc1d1,k2eAgFnSc1d1
část	část	k1gFnSc1
Malešic	Malešice	k1gFnPc2
</s>
<s>
Praha	Praha	k1gFnSc1
10	#num#	k4
–	–	k?
Vršovice	Vršovice	k1gFnPc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
Vinohrad	Vinohrady	k1gInPc2
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Strašnic	Strašnice	k1gFnPc2
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Malešic	Malešice	k1gFnPc2
<g/>
,	,	kIx,
větší	veliký	k2eAgFnSc1d2
část	část	k1gFnSc1
Záběhlic	Záběhlice	k1gFnPc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
Michle	Michl	k1gMnSc5
(	(	kIx(
<g/>
vrch	vrch	k1gInSc1
Bohdalec	Bohdalec	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nepatrná	patrný	k2eNgFnSc1d1,k2eAgFnSc1d1
část	část	k1gFnSc1
Žižkova	Žižkov	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
dům	dům	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nepatrná	patrný	k2eNgFnSc1d1,k2eAgFnSc1d1
nezastavěná	zastavěný	k2eNgFnSc1d1
část	část	k1gFnSc1
Hrdlořez	Hrdlořezy	k1gInPc2
<g/>
,	,	kIx,
nepatrná	nepatrný	k2eAgFnSc1d1,k2eNgFnSc1d1
nezastavěná	zastavěný	k2eNgFnSc1d1
část	část	k1gFnSc1
Hloubětína	Hloubětín	k1gInSc2
</s>
<s>
Praha	Praha	k1gFnSc1
11	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Jižní	Praha-Jižní	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
Chodov	Chodov	k1gInSc1
<g/>
,	,	kIx,
Háje	háj	k1gInPc1
</s>
<s>
Praha	Praha	k1gFnSc1
12	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Modřany	Praha-Modřan	k1gMnPc4
<g/>
)	)	kIx)
–	–	k?
Modřany	Modřany	k1gInPc7
<g/>
,	,	kIx,
Komořany	Komořan	k1gMnPc7
<g/>
,	,	kIx,
Točná	točný	k2eAgFnSc1d1
<g/>
,	,	kIx,
Cholupice	Cholupice	k1gFnSc1
<g/>
,	,	kIx,
Kamýk	Kamýk	k1gInSc1
</s>
<s>
Praha	Praha	k1gFnSc1
13	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Jihozápadní	Praha-Jihozápadní	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Stodůlek	stodůlka	k1gFnPc2
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Třebonic	Třebonice	k1gFnPc2
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
okrajová	okrajový	k2eAgFnSc1d1
část	část	k1gFnSc1
Jinonic	Jinonice	k1gFnPc2
<g/>
,	,	kIx,
nepatrná	patrný	k2eNgFnSc1d1,k2eAgFnSc1d1
nezastavěná	zastavěný	k2eNgFnSc1d1
část	část	k1gFnSc1
Řeporyj	Řeporyj	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
14	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Kyje	Praha-Kyje	k1gMnSc2
<g/>
)	)	kIx)
–	–	k?
Kyje	kyje	k1gFnPc1
<g/>
,	,	kIx,
Hostavice	Hostavice	k1gFnPc1
<g/>
,	,	kIx,
Černý	černý	k2eAgInSc1d1
Most	most	k1gInSc1
<g/>
,	,	kIx,
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Hloubětína	Hloubětín	k1gInSc2
</s>
<s>
Praha	Praha	k1gFnSc1
15	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Horní	Praha-Horní	k2eAgFnSc2d1
Měcholupy	Měcholupa	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
Horní	horní	k2eAgFnSc2d1
Měcholupy	Měcholupa	k1gFnSc2
<g/>
,	,	kIx,
Hostivař	Hostivař	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
16	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Radotín	Praha-Radotín	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
Radotín	Radotín	k1gInSc1
</s>
<s>
Praha	Praha	k1gFnSc1
17	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Řepy	Praha-Řepa	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
Řepy	řepa	k1gFnSc2
</s>
<s>
Praha	Praha	k1gFnSc1
18	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Letňany	Praha-Letňan	k1gMnPc4
<g/>
)	)	kIx)
–	–	k?
Letňany	Letňan	k1gMnPc4
</s>
<s>
Praha	Praha	k1gFnSc1
19	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Kbely	Praha-Kbela	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
Kbely	Kbely	k1gInPc1
</s>
<s>
Praha	Praha	k1gFnSc1
20	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Horní	Praha-Horní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
<g/>
)	)	kIx)
–	–	k?
Horní	horní	k2eAgFnPc4d1
Počernice	Počernice	k1gFnPc4
</s>
<s>
Praha	Praha	k1gFnSc1
21	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Újezd	Praha-Újezd	k1gInSc1
nad	nad	k7c7
Lesy	les	k1gInPc7
<g/>
)	)	kIx)
–	–	k?
Újezd	Újezd	k1gInSc1
nad	nad	k7c7
Lesy	les	k1gInPc7
</s>
<s>
Praha	Praha	k1gFnSc1
22	#num#	k4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Praha-Uhříněves	Praha-Uhříněves	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
Uhříněves	Uhříněves	k1gFnSc1
<g/>
,	,	kIx,
Hájek	Hájek	k1gMnSc1
u	u	k7c2
Uhříněvsi	Uhříněves	k1gFnSc2
<g/>
,	,	kIx,
Pitkovice	Pitkovice	k1gFnSc2
</s>
<s>
35	#num#	k4
tzv.	tzv.	kA
malých	malý	k2eAgFnPc2d1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Col-begin	Col-begin	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Praha-Běchovice	Praha-Běchovice	k1gFnPc4
–	–	k?
Běchovice	Běchovice	k1gFnPc4
</s>
<s>
Praha-Benice	Praha-Benice	k1gFnSc1
–	–	k?
Benice	Benice	k1gFnSc2
</s>
<s>
Praha-Březiněves	Praha-Březiněves	k1gMnSc1
–	–	k?
Březiněves	Březiněves	k1gMnSc1
</s>
<s>
Praha-Čakovice	Praha-Čakovice	k1gFnSc1
–	–	k?
Čakovice	Čakovice	k1gFnSc1
<g/>
,	,	kIx,
Třeboradice	Třeboradice	k1gFnSc1
<g/>
,	,	kIx,
Miškovice	Miškovice	k1gFnSc1
</s>
<s>
Praha-Ďáblice	Praha-Ďáblice	k1gFnSc1
–	–	k?
Ďáblice	ďáblice	k1gFnSc2
</s>
<s>
Praha-Dolní	Praha-Dolní	k2eAgFnPc1d1
Chabry	Chabra	k1gFnPc1
–	–	k?
Dolní	dolní	k2eAgFnPc1d1
Chabry	Chabra	k1gFnPc1
</s>
<s>
Praha-Dolní	Praha-Dolní	k2eAgInPc1d1
Měcholupy	Měcholup	k1gInPc1
–	–	k?
Dolní	dolní	k2eAgInPc1d1
Měcholupy	Měcholup	k1gInPc1
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
pouze	pouze	k6eAd1
část	část	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Praha-Dolní	Praha-Dolní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
–	–	k?
Dolní	dolní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
</s>
<s>
Praha-Dubeč	Praha-Dubeč	k1gMnSc1
–	–	k?
Dubeč	Dubeč	k1gMnSc1
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
též	též	k6eAd1
malá	malý	k2eAgFnSc1d1
přilehlá	přilehlý	k2eAgFnSc1d1
část	část	k1gFnSc1
Dolních	dolní	k2eAgInPc2d1
Měcholup	Měcholup	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Praha-Klánovice	Praha-Klánovice	k1gFnPc4
–	–	k?
Klánovice	Klánovice	k1gFnPc4
</s>
<s>
Praha-Koloděje	Praha-Kolodět	k5eAaPmIp3nS
–	–	k?
Koloděje	Koloděje	k1gFnPc4
</s>
<s>
Praha-Kolovraty	Praha-Kolovrat	k1gInPc1
–	–	k?
Kolovraty	kolovrat	k1gInPc7
<g/>
,	,	kIx,
Lipany	lipan	k1gMnPc7
</s>
<s>
Praha-Královice	Praha-Královice	k1gFnSc1
–	–	k?
Královice	královic	k1gMnSc2
</s>
<s>
Praha-Křeslice	Praha-Křeslice	k1gFnSc1
–	–	k?
Křeslice	Křeslice	k1gFnSc2
</s>
<s>
Praha-Kunratice	Praha-Kunratice	k1gFnPc4
–	–	k?
Kunratice	Kunratice	k1gFnPc4
(	(	kIx(
<g/>
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2014	#num#	k4
oboustranné	oboustranný	k2eAgInPc4d1
přesahy	přesah	k1gInPc4
hranice	hranice	k1gFnSc2
s	s	k7c7
Šeberovem	Šeberovo	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
Praha-Libuš	Praha-Libuš	k1gMnSc1
–	–	k?
Libuš	Libuš	k1gMnSc1
<g/>
,	,	kIx,
Písnice	písnice	k1gFnSc1
</s>
<s>
Praha-Lipence	Praha-Lipence	k1gFnSc1
–	–	k?
Lipence	Lipence	k1gFnSc1
</s>
<s>
Praha-Lochkov	Praha-Lochkov	k1gInSc1
–	–	k?
Lochkov	Lochkov	k1gInSc1
</s>
<s>
Praha-Lysolaje	Praha-Lysolat	k5eAaImSgMnS
–	–	k?
Lysolaje	Lysolaje	k1gFnPc4
</s>
<s>
Praha-Nebušice	Praha-Nebušice	k1gFnSc1
–	–	k?
Nebušice	Nebušice	k1gFnSc1
</s>
<s>
Praha-Nedvězí	Praha-Nedvězí	k1gNnSc1
–	–	k?
Nedvězí	Nedvěze	k1gFnPc2
u	u	k7c2
Říčan	Říčany	k1gInPc2
</s>
<s>
Praha-Petrovice	Praha-Petrovice	k1gFnSc1
–	–	k?
Petrovice	Petrovice	k1gFnSc1
</s>
<s>
Praha-Přední	Praha-Přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
–	–	k?
Přední	přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
</s>
<s>
Praha-Řeporyje	Praha-Řeporýt	k5eAaPmIp3nS
–	–	k?
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
Řeporyj	Řeporyj	k1gFnSc1
<g/>
,	,	kIx,
Zadní	zadní	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
Stodůlek	stodůlka	k1gFnPc2
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
Třebonic	Třebonice	k1gFnPc2
</s>
<s>
Praha-Satalice	Praha-Satalice	k1gFnSc1
–	–	k?
Satalice	Satalice	k1gFnSc1
</s>
<s>
Praha-Slivenec	Praha-Slivenec	k1gMnSc1
–	–	k?
Slivenec	Slivenec	k1gMnSc1
<g/>
,	,	kIx,
Holyně	Holyně	k1gFnSc1
</s>
<s>
Praha-Suchdol	Praha-Suchdol	k1gInSc1
–	–	k?
Suchdol	Suchdol	k1gInSc1
<g/>
,	,	kIx,
horní	horní	k2eAgFnSc1d1
část	část	k1gFnSc1
Sedlce	Sedlec	k1gInSc2
</s>
<s>
Praha-Šeberov	Praha-Šeberov	k1gInSc1
–	–	k?
Šeberov	Šeberov	k1gInSc1
(	(	kIx(
<g/>
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2014	#num#	k4
oboustranné	oboustranný	k2eAgInPc4d1
přesahy	přesah	k1gInPc4
hranice	hranice	k1gFnSc2
s	s	k7c7
Kunraticemi	Kunratice	k1gFnPc7
<g/>
)	)	kIx)
</s>
<s>
Praha-Štěrboholy	Praha-Štěrbohola	k1gFnPc1
–	–	k?
Štěrboholy	Štěrbohola	k1gFnSc2
</s>
<s>
Praha-Troja	Praha-Troja	k1gFnSc1
–	–	k?
dolní	dolní	k2eAgFnSc4d1
část	část	k1gFnSc4
Troji	troje	k4xRgMnPc1
</s>
<s>
Praha-Újezd	Praha-Újezd	k1gInSc1
–	–	k?
Újezd	Újezd	k1gInSc1
u	u	k7c2
Průhonic	Průhonice	k1gFnPc2
</s>
<s>
Praha-Velká	Praha-Velký	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
–	–	k?
Velká	velký	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
<g/>
,	,	kIx,
Malá	malý	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
</s>
<s>
Praha-Vinoř	Praha-Vinoř	k1gFnSc1
–	–	k?
Vinoř	Vinoř	k1gFnSc1
</s>
<s>
Praha-Zbraslav	Praha-Zbraslav	k1gMnSc1
–	–	k?
Zbraslav	Zbraslav	k1gFnSc1
<g/>
,	,	kIx,
Lahovice	Lahovice	k1gFnSc1
</s>
<s>
Praha-Zličín	Praha-Zličín	k1gMnSc1
–	–	k?
Zličín	Zličín	k1gMnSc1
<g/>
,	,	kIx,
Sobín	Sobín	k1gMnSc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
Třebonic	Třebonice	k1gFnPc2
(	(	kIx(
<g/>
okolí	okolí	k1gNnSc1
stanice	stanice	k1gFnSc2
metra	metro	k1gNnSc2
Zličín	Zličína	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Katastrální	katastrální	k2eAgNnSc1d1
a	a	k8xC
evidenční	evidenční	k2eAgNnSc1d1
členění	členění	k1gNnSc1
Prahy	Praha	k1gFnSc2
</s>
<s>
Praha	Praha	k1gFnSc1
je	být	k5eAaImIp3nS
složena	složit	k5eAaPmNgFnS
z	z	k7c2
celkem	celkem	k6eAd1
112	#num#	k4
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
jinde	jinde	k6eAd1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
se	se	k3xPyFc4
nejedná	jednat	k5eNaImIp3nS
o	o	k7c4
jednotky	jednotka	k1gFnPc4
územní	územní	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
pražská	pražský	k2eAgNnPc1d1
katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
jsou	být	k5eAaImIp3nP
však	však	k9
územně	územně	k6eAd1
shodná	shodný	k2eAgFnSc1d1
s	s	k7c7
celým	celý	k2eAgNnSc7d1
územím	území	k1gNnSc7
některé	některý	k3yIgFnSc2
pražské	pražský	k2eAgFnSc2d1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
urbanistického	urbanistický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
mají	mít	k5eAaImIp3nP
mnohá	mnohý	k2eAgNnPc1d1
pražská	pražský	k2eAgNnPc1d1
katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
charakter	charakter	k1gInSc1
čtvrti	čtvrt	k1gFnSc2
či	či	k8xC
vesnice	vesnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pražská	pražský	k2eAgFnSc1d1
katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
jsou	být	k5eAaImIp3nP
totožná	totožný	k2eAgFnSc1d1
s	s	k7c7
pražskými	pražský	k2eAgFnPc7d1
evidenčními	evidenční	k2eAgFnPc7d1
částmi	část	k1gFnPc7
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
proto	proto	k8xC
také	také	k9
popisná	popisný	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
domů	dům	k1gInPc2
jsou	být	k5eAaImIp3nP
přidělována	přidělovat	k5eAaImNgNnP
podle	podle	k7c2
příslušnosti	příslušnost	k1gFnSc2
k	k	k7c3
evidenční	evidenční	k2eAgFnSc3d1
části	část	k1gFnSc3
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
podle	podle	k7c2
příslušnosti	příslušnost	k1gFnSc2
k	k	k7c3
samosprávné	samosprávný	k2eAgFnSc3d1
městské	městský	k2eAgFnSc3d1
části	část	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
zcela	zcela	k6eAd1
jiný	jiný	k2eAgInSc4d1
pojem	pojem	k1gInSc4
než	než	k8xS
samosprávné	samosprávný	k2eAgFnSc2d1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
katastrálního	katastrální	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
,	,	kIx,
č.	č.	k?
344	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
základní	základní	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
pro	pro	k7c4
evidenci	evidence	k1gFnSc4
pozemků	pozemek	k1gInPc2
a	a	k8xC
staveb	stavba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Obec	obec	k1gFnSc1
je	být	k5eAaImIp3nS
vymezena	vymezit	k5eAaPmNgFnS
výčtem	výčet	k1gInSc7
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
–	–	k?
žádné	žádný	k3yNgNnSc4
katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
podle	podle	k7c2
zákona	zákon	k1gInSc2
nemá	mít	k5eNaImIp3nS
být	být	k5eAaImF
rozděleno	rozdělit	k5eAaPmNgNnS
do	do	k7c2
více	hodně	k6eAd2
obcí	obec	k1gFnPc2
nebo	nebo	k8xC
okresů	okres	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
však	však	k9
zasahovat	zasahovat	k5eAaImF
do	do	k7c2
více	hodně	k6eAd2
pražských	pražský	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
nebo	nebo	k8xC
do	do	k7c2
více	hodně	k6eAd2
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
Praze	Praha	k1gFnSc6
z	z	k7c2
důvodů	důvod	k1gInPc2
historického	historický	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
zcela	zcela	k6eAd1
běžné	běžný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
Vinohrady	Vinohrady	k1gInPc1
jsou	být	k5eAaImIp3nP
rozděleny	rozdělit	k5eAaPmNgInP
do	do	k7c2
pěti	pět	k4xCc2
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
každá	každý	k3xTgFnSc1
patří	patřit	k5eAaImIp3nS
do	do	k7c2
jiného	jiný	k2eAgInSc2d1
územního	územní	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
i	i	k8xC
do	do	k7c2
jiného	jiný	k2eAgInSc2d1
správního	správní	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Původně	původně	k6eAd1
historicky	historicky	k6eAd1
bývalo	bývat	k5eAaImAgNnS
správní	správní	k2eAgNnSc1d1
a	a	k8xC
samosprávné	samosprávný	k2eAgNnSc1d1
členění	členění	k1gNnSc4
totožné	totožný	k2eAgNnSc4d1
s	s	k7c7
katastrálním	katastrální	k2eAgInSc7d1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1947	#num#	k4
a	a	k8xC
silně	silně	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
se	se	k3xPyFc4
však	však	k9
začínala	začínat	k5eAaImAgFnS
dosud	dosud	k6eAd1
souvislá	souvislý	k2eAgNnPc4d1
katastrální	katastrální	k2eAgNnPc4d1
území	území	k1gNnPc4
dělit	dělit	k5eAaImF
do	do	k7c2
více	hodně	k6eAd2
správních	správní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
již	již	k6eAd1
je	být	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc1
odlišnost	odlišnost	k1gFnSc1
natolik	natolik	k6eAd1
zásadní	zásadní	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
jejich	jejich	k3xOp3gNnSc7
opětovným	opětovný	k2eAgNnSc7d1
úplným	úplný	k2eAgNnSc7d1
sladěním	sladění	k1gNnSc7
se	se	k3xPyFc4
nepočítá	počítat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katastrální	katastrální	k2eAgInSc4d1
členění	členění	k1gNnSc1
musí	muset	k5eAaImIp3nS
mít	mít	k5eAaImF
trvalejší	trvalý	k2eAgInSc4d2
charakter	charakter	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
nepřizpůsobuje	přizpůsobovat	k5eNaImIp3nS
všem	všecek	k3xTgFnPc3
správně-organizačním	správně-organizační	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
ke	k	k7c3
změnám	změna	k1gFnPc3
občas	občas	k6eAd1
dochází	docházet	k5eAaImIp3nS
<g/>
,	,	kIx,
například	například	k6eAd1
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1988	#num#	k4
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
Černý	černý	k2eAgInSc1d1
Most	most	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1989	#num#	k4
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
Kamýk	Kamýk	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgFnPc6
oblastech	oblast	k1gFnPc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
mírným	mírný	k2eAgFnPc3d1
úpravám	úprava	k1gFnPc3
katastrálních	katastrální	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
současném	současný	k2eAgNnSc6d1
území	území	k1gNnSc6
Prahy	Praha	k1gFnSc2
po	po	k7c6
vzniku	vznik	k1gInSc6
katastrů	katastr	k1gInPc2
vzniklo	vzniknout	k5eAaPmAgNnS
9	#num#	k4
nových	nový	k2eAgNnPc2d1
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
(	(	kIx(
<g/>
Černý	černý	k2eAgInSc1d1
Most	most	k1gInSc1
<g/>
,	,	kIx,
Háje	háj	k1gInPc1
<g/>
,	,	kIx,
Hájek	Hájek	k1gMnSc1
u	u	k7c2
Uhříněvsi	Uhříněves	k1gFnSc2
<g/>
,	,	kIx,
Holyně	Holyně	k1gFnSc2
<g/>
,	,	kIx,
Kamýk	Kamýk	k1gInSc4
<g/>
,	,	kIx,
Klánovice	Klánovice	k1gFnPc4
<g/>
,	,	kIx,
Komořany	Komořan	k1gMnPc4
<g/>
,	,	kIx,
Újezd	Újezd	k1gInSc1
nad	nad	k7c7
Lesy	les	k1gInPc7
<g/>
,	,	kIx,
Žižkov	Žižkov	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
8	#num#	k4
jich	on	k3xPp3gMnPc2
zaniklo	zaniknout	k5eAaPmAgNnS
(	(	kIx(
<g/>
sloučené	sloučený	k2eAgFnPc4d1
Horní	horní	k2eAgFnSc4d1
Krč	Krč	k1gFnSc4
a	a	k8xC
Dolní	dolní	k2eAgFnSc4d1
Krč	Krč	k1gFnSc4
<g/>
,	,	kIx,
Chaby	Chaby	k?
<g/>
,	,	kIx,
Chvaly	Chval	k1gMnPc4
<g/>
,	,	kIx,
Lipany	lipan	k1gMnPc4
<g/>
,	,	kIx,
Roztyly	Roztyly	k?
<g/>
,	,	kIx,
Svépravice	Svépravice	k1gFnPc1
<g/>
,	,	kIx,
Záběhlice	Záběhlice	k1gFnPc1
u	u	k7c2
Zbraslavi	Zbraslav	k1gFnSc2
<g/>
,	,	kIx,
Žabovřesky	Žabovřesky	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mnohé	mnohé	k1gNnSc1
pražské	pražský	k2eAgFnSc2d1
čtvrti	čtvrt	k1gFnSc2
nebo	nebo	k8xC
osady	osada	k1gFnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
Barrandov	Barrandov	k1gInSc1
<g/>
,	,	kIx,
Spořilov	Spořilov	k1gInSc1
<g/>
,	,	kIx,
Košík	košík	k1gInSc1
<g/>
,	,	kIx,
Zahradní	zahradní	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
Pankrác	Pankrác	k1gFnSc1
<g/>
,	,	kIx,
Letná	Letná	k1gFnSc1
<g/>
,	,	kIx,
Bubny	Bubny	k1gInPc1
<g/>
,	,	kIx,
Zlíchov	Zlíchov	k1gInSc1
<g/>
,	,	kIx,
Klíčov	Klíčov	k1gInSc1
<g/>
,	,	kIx,
Butovice	Butovice	k1gFnSc1
<g/>
,	,	kIx,
Klukovice	Klukovice	k1gFnSc1
<g/>
,	,	kIx,
Kačerov	Kačerov	k1gInSc1
<g/>
,	,	kIx,
Jenerálka	jenerálka	k1gFnSc1
<g/>
,	,	kIx,
Šárka	Šárka	k1gFnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Strahov	Strahov	k1gInSc1
<g/>
,	,	kIx,
Chodovec	Chodovec	k1gInSc1
<g/>
,	,	kIx,
Litochleby	Litochleb	k1gInPc1
<g/>
,	,	kIx,
Dubeček	dubeček	k1gInSc1
<g/>
,	,	kIx,
Lázeňka	Lázeňka	k1gFnSc1
<g/>
,	,	kIx,
Netluky	Netluk	k1gInPc1
<g/>
,	,	kIx,
Zmrzlík	zmrzlík	k1gInSc1
<g/>
,	,	kIx,
Cikánka	cikánka	k1gFnSc1
<g/>
,	,	kIx,
Kateřinky	Kateřinka	k1gFnSc2
<g/>
,	,	kIx,
Hrnčíře	Hrnčíř	k1gMnSc2
<g/>
,	,	kIx,
Pitkovičky	Pitkovička	k1gFnSc2
<g/>
,	,	kIx,
Lahovičky	Lahovička	k1gFnPc1
<g/>
,	,	kIx,
Dolní	dolní	k2eAgFnPc1d1
Černošice	Černošice	k1gFnPc1
<g/>
,	,	kIx,
Kazín	Kazín	k1gInSc1
<g/>
,	,	kIx,
Závist	závist	k1gFnSc1
<g/>
,	,	kIx,
Baně	baně	k1gFnPc1
<g/>
,	,	kIx,
Strnady	strnad	k1gInPc1
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
,	,	kIx,
nemají	mít	k5eNaImIp3nP
vlastní	vlastní	k2eAgNnSc4d1
samostatné	samostatný	k2eAgNnSc4d1
katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
však	však	k9
přesto	přesto	k8xC
výjimečně	výjimečně	k6eAd1
bývají	bývat	k5eAaImIp3nP
na	na	k7c6
tabulkách	tabulka	k1gFnPc6
s	s	k7c7
číslem	číslo	k1gNnSc7
popisným	popisný	k2eAgNnSc7d1
uváděny	uvádět	k5eAaImNgInP
místo	místo	k7c2
názvu	název	k1gInSc2
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohé	mnohé	k1gNnSc1
bývalé	bývalý	k2eAgFnSc2d1
čtvrtě	čtvrt	k1gFnSc2
či	či	k8xC
osady	osada	k1gFnPc1
splynuly	splynout	k5eAaPmAgFnP
se	s	k7c7
souvislou	souvislý	k2eAgFnSc7d1
zástavbou	zástavba	k1gFnSc7
a	a	k8xC
jejich	jejich	k3xOp3gNnPc1
jména	jméno	k1gNnPc1
mají	mít	k5eAaImIp3nP
spíše	spíše	k9
charakter	charakter	k1gInSc4
pomístních	pomístní	k2eAgInPc2d1
názvů	název	k1gInPc2
či	či	k8xC
se	se	k3xPyFc4
dochovala	dochovat	k5eAaPmAgFnS
v	v	k7c6
názvech	název	k1gInPc6
ulic	ulice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
a	a	k8xC
evidenční	evidenční	k2eAgFnPc4d1
části	část	k1gFnPc4
Prahy	Praha	k1gFnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
katastrálních	katastrální	k2eAgFnPc2d1
území	území	k1gNnPc2
Prahy	Praha	k1gFnSc2
podle	podle	k7c2
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Katastrální	katastrální	k2eAgNnSc1d1
území	území	k1gNnSc1
v	v	k7c6
případě	případ	k1gInSc6
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
plní	plnit	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
funkci	funkce	k1gFnSc4
evidenčních	evidenční	k2eAgFnPc2d1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
jejichž	jejichž	k3xOyRp3gInSc6
rámci	rámec	k1gInSc6
jsou	být	k5eAaImIp3nP
přidělována	přidělován	k2eAgNnPc4d1
popisná	popisný	k2eAgNnPc4d1
a	a	k8xC
evidenční	evidenční	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
budov	budova	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
červenci	červenec	k1gInSc3
2016	#num#	k4
byly	být	k5eAaImAgFnP
dvě	dva	k4xCgFnPc4
evidenční	evidenční	k2eAgFnPc4d1
části	část	k1gFnPc4
Prahy	Praha	k1gFnSc2
přejmenovány	přejmenován	k2eAgFnPc1d1
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
jejich	jejich	k3xOp3gInPc1
názvy	název	k1gInPc1
byly	být	k5eAaImAgInP
zcela	zcela	k6eAd1
shodné	shodný	k2eAgInPc1d1
s	s	k7c7
odpovídajícími	odpovídající	k2eAgFnPc7d1
katastrálními	katastrální	k2eAgFnPc7d1
územími	území	k1gNnPc7
(	(	kIx(
<g/>
Nedvězí	Nedvězí	k1gNnSc1
u	u	k7c2
Říčan	Říčany	k1gInPc2
a	a	k8xC
Újezd	Újezd	k1gInSc1
u	u	k7c2
Průhonic	Průhonice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
závorce	závorka	k1gFnSc6
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgFnP
městské	městský	k2eAgFnPc1d1
části	část	k1gFnPc1
<g/>
,	,	kIx,
do	do	k7c2
kterých	který	k3yQgInPc2,k3yIgInPc2,k3yRgInPc2
katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnSc4
a	a	k8xC
část	část	k1gFnSc4
obce	obec	k1gFnSc2
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
(	(	kIx(
<g/>
nikoliv	nikoliv	k9
městské	městský	k2eAgInPc1d1
obvody	obvod	k1gInPc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kódy	kód	k1gInPc1
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc4
výměru	výměra	k1gFnSc4
uvádí	uvádět	k5eAaImIp3nS
celostátní	celostátní	k2eAgInSc1d1
Územně	územně	k6eAd1
identifikační	identifikační	k2eAgInSc1d1
registr	registr	k1gInSc1
–	–	k?
ÚIR	ÚIR	kA
Archivováno	archivován	k2eAgNnSc4d1
20	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Benice	Benice	k1gFnSc1
(	(	kIx(
<g/>
Praha-Benice	Praha-Benice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Běchovice	Běchovice	k1gFnPc1
(	(	kIx(
<g/>
Praha-Běchovice	Praha-Běchovice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Bohnice	Bohnice	k1gInPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Braník	Braník	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Břevnov	Břevnov	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Březiněves	Březiněves	k1gMnSc1
(	(	kIx(
<g/>
Praha-Březiněves	Praha-Březiněves	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Bubeneč	Bubeneč	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Čakovice	Čakovice	k1gFnSc1
(	(	kIx(
<g/>
Praha-Čakovice	Praha-Čakovice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Černý	černý	k2eAgInSc1d1
Most	most	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
14	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Čimice	Čimice	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dejvice	Dejvice	k1gFnPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dolní	dolní	k2eAgFnPc1d1
Chabry	Chabra	k1gFnPc1
(	(	kIx(
<g/>
Praha-Dolní	Praha-Dolní	k2eAgFnPc1d1
Chabry	Chabra	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Dolní	dolní	k2eAgFnPc1d1
Měcholupy	Měcholupa	k1gFnPc1
(	(	kIx(
<g/>
Praha-Dolní	Praha-Dolní	k2eAgFnPc1d1
Měcholupy	Měcholupa	k1gFnPc1
<g/>
,	,	kIx,
Praha-Dubeč	Praha-Dubeč	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Dolní	dolní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
(	(	kIx(
<g/>
Praha-Dolní	Praha-Dolní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Dubeč	Dubeč	k1gMnSc1
(	(	kIx(
<g/>
Praha-Dubeč	Praha-Dubeč	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Ďáblice	ďáblice	k1gFnSc1
(	(	kIx(
<g/>
Praha-Ďáblice	Praha-Ďáblice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Háje	háj	k1gInPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
11	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hájek	Hájek	k1gMnSc1
u	u	k7c2
Uhříněvsi	Uhříněves	k1gFnSc2
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
22	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hloubětín	Hloubětín	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
14	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hlubočepy	Hlubočep	k1gInPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hodkovičky	Hodkovička	k1gFnPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Holešovice	Holešovice	k1gFnPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
7	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Holyně	Holyně	k1gFnSc1
(	(	kIx(
<g/>
Praha-Slivenec	Praha-Slivenec	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Horní	horní	k2eAgInPc1d1
Měcholupy	Měcholup	k1gInPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
15	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Horní	horní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
20	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hostavice	Hostavice	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
14	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hostivař	Hostivař	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
15	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hradčany	Hradčany	k1gInPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hrdlořezy	Hrdlořezy	k1gInPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Chodov	Chodov	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
11	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cholupice	Cholupice	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
12	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jinonice	Jinonice	k1gFnPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
13	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Josefov	Josefov	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kamýk	Kamýk	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
12	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karlín	Karlín	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kbely	Kbely	k1gInPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
19	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Klánovice	Klánovice	k1gFnPc1
(	(	kIx(
<g/>
Praha-Klánovice	Praha-Klánovice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Kobylisy	Kobylisy	k1gInPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Koloděje	Koloděje	k1gFnPc1
(	(	kIx(
<g/>
Praha-Koloděje	Praha-Koloděje	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Kolovraty	kolovrat	k1gInPc1
(	(	kIx(
<g/>
Praha-Kolovraty	Praha-Kolovrat	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Komořany	Komořan	k1gMnPc4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
12	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Košíře	Košíře	k1gInPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Královice	královic	k1gMnSc4
(	(	kIx(
<g/>
Praha-Královice	Praha-Královice	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Krč	Krč	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Křeslice	Křeslice	k1gFnSc1
(	(	kIx(
<g/>
Praha-Křeslice	Praha-Křeslice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kunratice	Kunratice	k1gFnPc1
(	(	kIx(
<g/>
Praha-Kunratice	Praha-Kunratice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Kyje	kyje	k1gFnPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
14	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lahovice	Lahovice	k1gFnSc1
(	(	kIx(
<g/>
Praha-Zbraslav	Praha-Zbraslav	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Letňany	Letňan	k1gMnPc4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
18	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lhotka	Lhotka	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Libeň	Libeň	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
7	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Liboc	Liboc	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Libuš	Libuš	k1gMnSc1
(	(	kIx(
<g/>
Praha-Libuš	Praha-Libuš	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Lipany	lipan	k1gMnPc7
(	(	kIx(
<g/>
Praha-Kolovraty	Praha-Kolovrat	k1gMnPc7
<g/>
)	)	kIx)
</s>
<s>
Lipence	Lipence	k1gFnSc1
(	(	kIx(
<g/>
Praha-Lipence	Praha-Lipence	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Lochkov	Lochkov	k1gInSc1
(	(	kIx(
<g/>
Praha-Lochkov	Praha-Lochkov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Lysolaje	Lysolaje	k1gFnPc1
(	(	kIx(
<g/>
Praha-Lysolaje	Praha-Lysolaje	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Malá	malý	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
(	(	kIx(
<g/>
Praha-Velká	Praha-Velký	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Malá	malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Malešice	Malešice	k1gFnPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Michle	Michl	k1gMnSc5
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
4	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Miškovice	Miškovice	k1gFnSc1
(	(	kIx(
<g/>
Praha-Čakovice	Praha-Čakovice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Modřany	Modřany	k1gInPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
12	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Motol	Motol	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nebušice	Nebušice	k1gFnSc1
(	(	kIx(
<g/>
Praha-Nebušice	Praha-Nebušice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Nedvězí	Nedvězí	k1gNnSc1
u	u	k7c2
Říčan	Říčany	k1gInPc2
(	(	kIx(
<g/>
Praha-Nedvězí	Praha-Nedvěze	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nusle	Nusle	k1gFnPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
2	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Petrovice	Petrovice	k1gFnSc1
(	(	kIx(
<g/>
Praha-Petrovice	Praha-Petrovice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Písnice	písnice	k1gFnSc1
(	(	kIx(
<g/>
Praha-Libuš	Praha-Libuš	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Pitkovice	Pitkovice	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
22	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podolí	Podolí	k1gNnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Prosek	proséct	k5eAaPmDgInS
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Přední	přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
(	(	kIx(
<g/>
Praha-Přední	Praha-Přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Radlice	radlice	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Radotín	Radotín	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
16	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ruzyně	Ruzyně	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Řeporyje	Řeporýt	k5eAaPmIp3nS
(	(	kIx(
<g/>
Praha-Řeporyje	Praha-Řeporyje	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
13	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Řepy	řepa	k1gFnPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
17	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Satalice	Satalice	k1gFnSc1
(	(	kIx(
<g/>
Praha-Satalice	Praha-Satalice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Sedlec	Sedlec	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
,	,	kIx,
Praha-Suchdol	Praha-Suchdol	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Slivenec	Slivenec	k1gMnSc1
(	(	kIx(
<g/>
Praha-Slivenec	Praha-Slivenec	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Smíchov	Smíchov	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sobín	Sobín	k1gMnSc1
(	(	kIx(
<g/>
Praha-Zličín	Praha-Zličín	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Stodůlky	stodůlka	k1gFnPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
13	#num#	k4
<g/>
,	,	kIx,
Praha-Řeporyje	Praha-Řeporyje	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Strašnice	Strašnice	k1gFnPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
3	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Střešovice	Střešovice	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Střížkov	Střížkov	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Suchdol	Suchdol	k1gInSc1
(	(	kIx(
<g/>
Praha-Suchdol	Praha-Suchdol	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Šeberov	Šeberov	k1gInSc1
(	(	kIx(
<g/>
Praha-Šeberov	Praha-Šeberov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Štěrboholy	Štěrbohola	k1gFnPc1
(	(	kIx(
<g/>
Praha-Štěrboholy	Praha-Štěrbohola	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Točná	točný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
12	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Troja	Troja	k1gFnSc1
(	(	kIx(
<g/>
Praha-Troja	Praha-Troja	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Třebonice	Třebonice	k1gFnSc1
(	(	kIx(
<g/>
Praha-Zličín	Praha-Zličín	k1gMnSc1
<g/>
,	,	kIx,
Praha-Řeporyje	Praha-Řeporyje	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
13	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Třeboradice	Třeboradice	k1gFnSc1
(	(	kIx(
<g/>
Praha-Čakovice	Praha-Čakovice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Uhříněves	Uhříněves	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
22	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Újezd	Újezd	k1gInSc1
u	u	k7c2
Průhonic	Průhonice	k1gFnPc2
(	(	kIx(
<g/>
Praha-Újezd	Praha-Újezd	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Újezd	Újezd	k1gInSc1
nad	nad	k7c7
Lesy	les	k1gInPc7
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
21	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Veleslavín	Veleslavín	k1gMnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
(	(	kIx(
<g/>
Praha-Velká	Praha-Velký	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Vinohrady	Vinohrady	k1gInPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
3	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
4	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vinoř	Vinoř	k1gFnSc1
(	(	kIx(
<g/>
Praha-Vinoř	Praha-Vinoř	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Vokovice	Vokovice	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vršovice	Vršovice	k1gFnPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vysočany	Vysočany	k1gInPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
3	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vyšehrad	Vyšehrad	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Záběhlice	Záběhlice	k1gFnPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
4	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zadní	zadní	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
(	(	kIx(
<g/>
Praha-Řeporyje	Praha-Řeporyje	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Zbraslav	Zbraslav	k1gFnSc1
(	(	kIx(
<g/>
Praha-Zbraslav	Praha-Zbraslav	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Zličín	Zličín	k1gMnSc1
(	(	kIx(
<g/>
Praha-Zličín	Praha-Zličín	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Žižkov	Žižkov	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
3	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiné	jiný	k2eAgInPc1d1
pražské	pražský	k2eAgInPc1d1
obvody	obvod	k1gInPc1
</s>
<s>
Soudní	soudní	k2eAgInPc1d1
obvody	obvod	k1gInPc1
Praha	Praha	k1gFnSc1
1	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
10	#num#	k4
</s>
<s>
Soudní	soudní	k2eAgInPc1d1
obvody	obvod	k1gInPc1
jsou	být	k5eAaImIp3nP
vymezeny	vymezit	k5eAaPmNgInP
pro	pro	k7c4
jednotlivé	jednotlivý	k2eAgInPc4d1
obvodní	obvodní	k2eAgInPc4d1
soudy	soud	k1gInPc4
přílohou	příloha	k1gFnSc7
č.	č.	k?
4	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
soudech	soud	k1gInPc6
a	a	k8xC
soudcích	soudek	k1gInPc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
výčtem	výčet	k1gInSc7
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
takto	takto	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
Obvodní	obvodní	k2eAgInSc1d1
soud	soud	k1gInSc1
pro	pro	k7c4
Prahu	Praha	k1gFnSc4
1	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
1	#num#	k4
</s>
<s>
Obvodní	obvodní	k2eAgInSc1d1
soud	soud	k1gInSc1
pro	pro	k7c4
Prahu	Praha	k1gFnSc4
2	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
2	#num#	k4
</s>
<s>
Obvodní	obvodní	k2eAgInSc1d1
soud	soud	k1gInSc1
pro	pro	k7c4
Prahu	Praha	k1gFnSc4
3	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
3	#num#	k4
</s>
<s>
Obvodní	obvodní	k2eAgInSc1d1
soud	soud	k1gInSc1
pro	pro	k7c4
Prahu	Praha	k1gFnSc4
4	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
4	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
11	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
12	#num#	k4
<g/>
,	,	kIx,
Praha-Kunratice	Praha-Kunratice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Libuš	Praha-Libuš	k1gInSc1
<g/>
,	,	kIx,
Praha-Šeberov	Praha-Šeberov	k1gInSc1
a	a	k8xC
Praha-Újezd	Praha-Újezd	k1gInSc1
</s>
<s>
Obvodní	obvodní	k2eAgInSc1d1
soud	soud	k1gInSc1
pro	pro	k7c4
Prahu	Praha	k1gFnSc4
5	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
13	#num#	k4
<g/>
,	,	kIx,
Praha-Lipence	Praha-Lipence	k1gFnSc1
<g/>
,	,	kIx,
Praha-Lochkov	Praha-Lochkov	k1gInSc1
<g/>
,	,	kIx,
Praha-Radotín	Praha-Radotín	k1gMnSc1
<g/>
,	,	kIx,
Praha-Řeporyje	Praha-Řeporyje	k1gMnSc1
<g/>
,	,	kIx,
Praha-Slivenec	Praha-Slivenec	k1gMnSc1
<g/>
,	,	kIx,
Praha-Velká	Praha-Velký	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
<g/>
,	,	kIx,
Praha-Zbraslav	Praha-Zbraslav	k1gMnSc1
a	a	k8xC
Praha-Zličín	Praha-Zličín	k1gMnSc1
</s>
<s>
Obvodní	obvodní	k2eAgInSc1d1
soud	soud	k1gInSc1
pro	pro	k7c4
Prahu	Praha	k1gFnSc4
6	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
,	,	kIx,
Praha-Lysolaje	Praha-Lysolaje	k1gFnSc1
<g/>
,	,	kIx,
Praha-Nebušice	Praha-Nebušice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Přední	Praha-Přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
<g/>
,	,	kIx,
Praha-Řepy	Praha-Řepa	k1gFnPc1
a	a	k8xC
Praha-Suchdol	Praha-Suchdol	k1gInSc1
</s>
<s>
Obvodní	obvodní	k2eAgInSc1d1
soud	soud	k1gInSc1
pro	pro	k7c4
Prahu	Praha	k1gFnSc4
7	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
7	#num#	k4
a	a	k8xC
Praha-Troja	Praha-Troja	k1gMnSc1
</s>
<s>
Obvodní	obvodní	k2eAgInSc1d1
soud	soud	k1gInSc1
pro	pro	k7c4
Prahu	Praha	k1gFnSc4
8	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
,	,	kIx,
Praha-Březiněves	Praha-Březiněves	k1gInSc1
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgInPc1d1
Chabry	Chabr	k1gInPc1
a	a	k8xC
Praha-Ďáblice	Praha-Ďáblice	k1gFnPc1
</s>
<s>
Obvodní	obvodní	k2eAgInSc1d1
soud	soud	k1gInSc1
pro	pro	k7c4
Prahu	Praha	k1gFnSc4
9	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
14	#num#	k4
<g/>
,	,	kIx,
Praha-Běchovice	Praha-Běchovice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Čakovice	Praha-Čakovice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgFnPc4d1
Počernice	Počernice	k1gFnPc4
<g/>
,	,	kIx,
Praha-Horní	Praha-Horní	k2eAgFnPc4d1
Počernice	Počernice	k1gFnPc4
<g/>
,	,	kIx,
Praha-Kbely	Praha-Kbela	k1gFnPc4
<g/>
,	,	kIx,
Praha-Klánovice	Praha-Klánovice	k1gFnPc4
<g/>
,	,	kIx,
Praha-Koloděje	Praha-Koloděje	k1gMnPc4
<g/>
,	,	kIx,
Praha-Letňany	Praha-Letňan	k1gMnPc4
<g/>
,	,	kIx,
Praha-Satalice	Praha-Satalice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Újezd	Praha-Újezd	k1gInSc1
nad	nad	k7c7
Lesy	les	k1gInPc7
a	a	k8xC
Praha-Vinoř	Praha-Vinoř	k1gFnSc1
</s>
<s>
Obvodní	obvodní	k2eAgInSc1d1
soud	soud	k1gInSc1
pro	pro	k7c4
Prahu	Praha	k1gFnSc4
10	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
15	#num#	k4
<g/>
,	,	kIx,
Praha-Benice	Praha-Benice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgFnPc1d1
Měcholupy	Měcholupa	k1gFnPc1
<g/>
,	,	kIx,
Praha-Dubeč	Praha-Dubeč	k1gInSc1
<g/>
,	,	kIx,
Praha-Kolovraty	Praha-Kolovrat	k1gInPc1
<g/>
,	,	kIx,
Praha-Královice	Praha-Královice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Křeslice	Praha-Křeslice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Nedvězí	Praha-Nedvězí	k1gNnPc1
<g/>
,	,	kIx,
Praha-Petrovice	Praha-Petrovice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Štěrboholy	Praha-Štěrbohola	k1gFnPc1
a	a	k8xC
Praha-Uhříněves	Praha-Uhříněves	k1gInSc1
</s>
<s>
Fakticky	fakticky	k6eAd1
jsou	být	k5eAaImIp3nP
shodné	shodný	k2eAgInPc1d1
jako	jako	k8xC,k8xS
městské	městský	k2eAgInPc1d1
obvody	obvod	k1gInPc1
podle	podle	k7c2
zákona	zákon	k1gInSc2
o	o	k7c6
územním	územní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Policejní	policejní	k2eAgInPc4d1
obvody	obvod	k1gInPc4
Praha	Praha	k1gFnSc1
I	i	k8xC
–	–	k?
Praha	Praha	k1gFnSc1
IV	Iva	k1gFnPc2
</s>
<s>
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2004	#num#	k4
vytvořila	vytvořit	k5eAaPmAgFnS
Policie	policie	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
čtyři	čtyři	k4xCgInPc4
policejní	policejní	k2eAgInPc4d1
obvody	obvod	k1gInPc4
s	s	k7c7
obvodními	obvodní	k2eAgNnPc7d1
ředitelstvími	ředitelství	k1gNnPc7
<g/>
:	:	kIx,
</s>
<s>
Praha	Praha	k1gFnSc1
I	I	kA
(	(	kIx(
<g/>
obvody	obvod	k1gInPc1
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
II	II	kA
(	(	kIx(
<g/>
obvody	obvod	k1gInPc1
Praha	Praha	k1gFnSc1
2	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
III	III	kA
(	(	kIx(
<g/>
obvody	obvod	k1gInPc1
Praha	Praha	k1gFnSc1
3	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
IV	IV	kA
(	(	kIx(
<g/>
obvody	obvod	k1gInPc1
Praha	Praha	k1gFnSc1
4	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tyto	tento	k3xDgInPc1
obvody	obvod	k1gInPc1
zatím	zatím	k6eAd1
nejsou	být	k5eNaImIp3nP
dány	dát	k5eAaPmNgFnP
zákonem	zákon	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
případ	případ	k1gInSc4
zrušení	zrušení	k1gNnSc4
deseti	deset	k4xCc2
pražských	pražský	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
navrhuje	navrhovat	k5eAaImIp3nS
vláda	vláda	k1gFnSc1
uzákonění	uzákonění	k1gNnSc2
stejných	stejný	k2eAgInPc2d1
čtyř	čtyři	k4xCgInPc2
policejních	policejní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
v	v	k7c6
zákoně	zákon	k1gInSc6
redefinuje	redefinovat	k5eAaImIp3nS
výčtem	výčet	k1gInSc7
soudních	soudní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Správa	správa	k1gFnSc1
komunikací	komunikace	k1gFnPc2
</s>
<s>
Technická	technický	k2eAgFnSc1d1
správa	správa	k1gFnSc1
komunikací	komunikace	k1gFnPc2
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
vnitřní	vnitřní	k2eAgFnSc4d1
potřebu	potřeba	k1gFnSc4
a	a	k8xC
pro	pro	k7c4
kontakt	kontakt	k1gInSc4
s	s	k7c7
městskými	městský	k2eAgFnPc7d1
částmi	část	k1gFnPc7
a	a	k8xC
veřejností	veřejnost	k1gFnSc7
člení	členit	k5eAaImIp3nS
území	území	k1gNnSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
do	do	k7c2
oblastních	oblastní	k2eAgFnPc2d1
správ	správa	k1gFnPc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
OS	OS	kA
centrum	centrum	k1gNnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
OS	OS	kA
sever	sever	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
OS	OS	kA
východ	východ	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
OS	OS	kA
jih	jih	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
OS	OS	kA
jihozápad	jihozápad	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
OS	OS	kA
severozápad	severozápad	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Volební	volební	k2eAgInPc1d1
obvody	obvod	k1gInPc1
</s>
<s>
Pro	pro	k7c4
sněmovní	sněmovní	k2eAgFnPc4d1
volby	volba	k1gFnPc4
tvoří	tvořit	k5eAaImIp3nS
Praha	Praha	k1gFnSc1
podle	podle	k7c2
zákona	zákon	k1gInSc2
č.	č.	k?
247	#num#	k4
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
jediný	jediný	k2eAgInSc1d1
volební	volební	k2eAgInSc1d1
obvod	obvod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
senátní	senátní	k2eAgFnPc4d1
volby	volba	k1gFnPc4
je	být	k5eAaImIp3nS
území	území	k1gNnSc1
Prahy	Praha	k1gFnSc2
rozděleno	rozdělit	k5eAaPmNgNnS
zákonem	zákon	k1gInSc7
č.	č.	k?
247	#num#	k4
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
a	a	k8xC
jeho	jeho	k3xOp3gFnPc2
pravidelných	pravidelný	k2eAgFnPc2d1
novel	novela	k1gFnPc2
na	na	k7c4
10	#num#	k4
volebních	volební	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
přesné	přesný	k2eAgNnSc1d1
vymezení	vymezení	k1gNnSc1
je	být	k5eAaImIp3nS
novelami	novela	k1gFnPc7
zákona	zákon	k1gInSc2
příležitostně	příležitostně	k6eAd1
upravováno	upravovat	k5eAaImNgNnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
neodchyloval	odchylovat	k5eNaImAgInS
o	o	k7c4
více	hodně	k6eAd2
než	než	k8xS
15	#num#	k4
%	%	kIx~
oproti	oproti	k7c3
průměrnému	průměrný	k2eAgInSc3d1
počtu	počet	k1gInSc3
obyvatel	obyvatel	k1gMnPc2
republiky	republika	k1gFnSc2
připadajících	připadající	k2eAgMnPc2d1
na	na	k7c4
mandát	mandát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Senátní	senátní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
č.	č.	k?
17	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
12	#num#	k4
zahrnuje	zahrnovat	k5eAaImIp3nS
území	území	k1gNnSc4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
Praha	Praha	k1gFnSc1
12	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
16	#num#	k4
<g/>
,	,	kIx,
Praha-Lipence	Praha-Lipence	k1gFnSc1
<g/>
,	,	kIx,
Praha-Lochkov	Praha-Lochkov	k1gInSc1
<g/>
,	,	kIx,
Praha-Velká	Praha-Velký	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
<g/>
,	,	kIx,
Praha-Zbraslav	Praha-Zbraslav	k1gMnSc1
<g/>
,	,	kIx,
Praha-Kunratice	Praha-Kunratice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Šeberov	Praha-Šeberov	k1gInSc1
<g/>
,	,	kIx,
Praha-Újezd	Praha-Újezd	k1gInSc1
<g/>
,	,	kIx,
Praha-Libuš	Praha-Libuš	k1gInSc1
<g/>
,	,	kIx,
Praha-Petrovice	Praha-Petrovice	k1gFnSc1
a	a	k8xC
část	část	k1gFnSc1
území	území	k1gNnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
4	#num#	k4
tvořenou	tvořený	k2eAgFnSc7d1
katastrálními	katastrální	k2eAgNnPc7d1
územími	území	k1gNnPc7
Hodkovičky	Hodkovička	k1gFnSc2
a	a	k8xC
Lhotka	Lhotka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Senátní	senátní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
č.	č.	k?
19	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
11	#num#	k4
zahrnuje	zahrnovat	k5eAaImIp3nS
území	území	k1gNnSc4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
Praha	Praha	k1gFnSc1
11	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
15	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
22	#num#	k4
<g/>
,	,	kIx,
Praha-Benice	Praha-Benice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgFnPc1d1
Měcholupy	Měcholupa	k1gFnPc1
<g/>
,	,	kIx,
Praha-Kolovraty	Praha-Kolovrat	k1gInPc1
<g/>
,	,	kIx,
Praha-Královice	Praha-Královice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Křeslice	Praha-Křeslice	k1gFnPc1
a	a	k8xC
Praha-Nedvězí	Praha-Nedvězí	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Senátní	senátní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
č.	č.	k?
20	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
4	#num#	k4
zahrnuje	zahrnovat	k5eAaImIp3nS
území	území	k1gNnSc4
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
4	#num#	k4
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
katastrálních	katastrální	k2eAgFnPc2d1
území	území	k1gNnPc2
Hodkovičky	Hodkovička	k1gFnSc2
a	a	k8xC
Lhotka	Lhotka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Senátní	senátní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
č.	č.	k?
21	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
5	#num#	k4
zahrnuje	zahrnovat	k5eAaImIp3nS
území	území	k1gNnSc4
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
5	#num#	k4
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
části	část	k1gFnSc2
Malé	Malé	k2eAgFnSc2d1
Strany	strana	k1gFnSc2
ležící	ležící	k2eAgFnSc2d1
na	na	k7c6
území	území	k1gNnSc6
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
zahrnuje	zahrnovat	k5eAaImIp3nS
území	území	k1gNnSc4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
Praha	Praha	k1gFnSc1
13	#num#	k4
<g/>
,	,	kIx,
Praha-Řeporyje	Praha-Řeporyje	k1gMnSc1
<g/>
,	,	kIx,
Praha-Slivenec	Praha-Slivenec	k1gMnSc1
a	a	k8xC
Praha-Zličín	Praha-Zličín	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Senátní	senátní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
č.	č.	k?
22	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
10	#num#	k4
zahrnuje	zahrnovat	k5eAaImIp3nS
území	území	k1gNnSc4
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
10	#num#	k4
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
části	část	k1gFnSc2
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
Vinohrady	Vinohrady	k1gInPc4
<g/>
,	,	kIx,
dále	daleko	k6eAd2
zahrnuje	zahrnovat	k5eAaImIp3nS
území	území	k1gNnSc4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
Praha-Štěrboholy	Praha-Štěrbohola	k1gFnSc2
a	a	k8xC
Dubeč	Dubeč	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Senátní	senátní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
č.	č.	k?
23	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
8	#num#	k4
zahrnuje	zahrnovat	k5eAaImIp3nS
území	území	k1gNnSc4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
,	,	kIx,
Praha-Březiněves	Praha-Březiněves	k1gInSc1
<g/>
,	,	kIx,
Praha-Ďáblice	Praha-Ďáblice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgFnPc1d1
Chabry	Chabra	k1gFnPc1
a	a	k8xC
Praha-Čakovice	Praha-Čakovice	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Senátní	senátní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
č.	č.	k?
24	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
9	#num#	k4
zahrnuje	zahrnovat	k5eAaImIp3nS
území	území	k1gNnSc4
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
9	#num#	k4
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
Hrdlořezy	Hrdlořezy	k1gInPc7
<g/>
,	,	kIx,
Hloubětín	Hloubětín	k1gInSc1
a	a	k8xC
části	část	k1gFnPc1
Malešic	Malešice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
zahrnuje	zahrnovat	k5eAaImIp3nS
území	území	k1gNnSc4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
Praha	Praha	k1gFnSc1
14	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
18	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
19	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
20	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
21	#num#	k4
<g/>
,	,	kIx,
Praha-Běchovice	Praha-Běchovice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Klánovice	Praha-Klánovice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Koloděje	Praha-Koloděje	k1gFnSc1
<g/>
,	,	kIx,
Praha-Satalice	Praha-Satalice	k1gFnSc1
a	a	k8xC
Praha-Vinoř	Praha-Vinoř	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Senátní	senátní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
č.	č.	k?
25	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
6	#num#	k4
zahrnuje	zahrnovat	k5eAaImIp3nS
území	území	k1gNnSc4
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
6	#num#	k4
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
Střešovice	Střešovice	k1gFnSc2
<g/>
,	,	kIx,
části	část	k1gFnSc2
Bubenče	Bubeneč	k1gFnSc2
<g/>
,	,	kIx,
části	část	k1gFnSc2
Hradčan	Hradčany	k1gInPc2
a	a	k8xC
části	část	k1gFnSc2
Sedlce	Sedlec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
zahrnuje	zahrnovat	k5eAaImIp3nS
území	území	k1gNnSc4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
Praha	Praha	k1gFnSc1
17	#num#	k4
<g/>
,	,	kIx,
Praha-Přední	Praha-Přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
<g/>
,	,	kIx,
Praha-Nebušice	Praha-Nebušice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Suchdol	Praha-Suchdol	k1gInSc1
a	a	k8xC
Praha-Lysolaje	Praha-Lysolaje	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Senátní	senátní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
č.	č.	k?
26	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
2	#num#	k4
zahrnuje	zahrnovat	k5eAaImIp3nS
území	území	k1gNnSc4
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
2	#num#	k4
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
zahrnuje	zahrnovat	k5eAaImIp3nS
území	území	k1gNnSc4
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
3	#num#	k4
<g/>
,	,	kIx,
část	část	k1gFnSc1
území	území	k1gNnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
10	#num#	k4
tvořené	tvořený	k2eAgFnSc6d1
částí	část	k1gFnSc7
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
Vinohrady	Vinohrady	k1gInPc1
ležící	ležící	k2eAgMnSc1d1
na	na	k7c6
území	území	k1gNnSc6
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
část	část	k1gFnSc1
území	území	k1gNnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
9	#num#	k4
tvořenou	tvořený	k2eAgFnSc7d1
katastrálními	katastrální	k2eAgFnPc7d1
územími	území	k1gNnPc7
Hrdlořezy	Hrdlořezy	k1gInPc1
<g/>
,	,	kIx,
Hloubětín	Hloubětína	k1gFnPc2
a	a	k8xC
částí	část	k1gFnPc2
Malešic	Malešice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Senátní	senátní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
č.	č.	k?
27	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
1	#num#	k4
zahrnuje	zahrnovat	k5eAaImIp3nS
centrální	centrální	k2eAgFnSc1d1
část	část	k1gFnSc1
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
územím	území	k1gNnSc7
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
7	#num#	k4
a	a	k8xC
Praha-Troja	Praha-Troja	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
zahrnuje	zahrnovat	k5eAaImIp3nS
část	část	k1gFnSc1
území	území	k1gNnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
2	#num#	k4
<g/>
,	,	kIx,
tvořenou	tvořený	k2eAgFnSc4d1
k.	k.	k?
ú.	ú.	k?
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
území	území	k1gNnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
,	,	kIx,
tvořenou	tvořený	k2eAgFnSc7d1
částí	část	k1gFnSc7
k.	k.	k?
ú.	ú.	k?
Malá	malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
ležící	ležící	k2eAgFnSc1d1
na	na	k7c6
území	území	k1gNnSc6
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
část	část	k1gFnSc1
území	území	k1gNnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
6	#num#	k4
tvořenou	tvořený	k2eAgFnSc4d1
k.	k.	k?
ú.	ú.	k?
Střešovice	Střešovice	k1gFnSc2
<g/>
,	,	kIx,
částí	část	k1gFnPc2
k.	k.	k?
ú.	ú.	k?
Bubeneč	Bubeneč	k1gInSc1
<g/>
,	,	kIx,
částí	část	k1gFnPc2
k.	k.	k?
ú.	ú.	k?
Hradčany	Hradčany	k1gInPc7
a	a	k8xC
částí	část	k1gFnSc7
k.	k.	k?
ú.	ú.	k?
Sedlec	Sedlec	k1gInSc1
<g/>
,	,	kIx,
ležícími	ležící	k2eAgFnPc7d1
na	na	k7c4
území	území	k1gNnSc4
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zákonem	zákon	k1gInSc7
č.	č.	k?
195	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
byla	být	k5eAaImAgFnS
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
Praha-Zličín	Praha-Zličína	k1gFnPc2
přeřazena	přeřazen	k2eAgFnSc1d1
ze	z	k7c2
senátního	senátní	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
21	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
)	)	kIx)
do	do	k7c2
obvodu	obvod	k1gInSc2
25	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
Praha	Praha	k1gFnSc1
18	#num#	k4
byla	být	k5eAaImAgFnS
přeřazena	přeřadit	k5eAaPmNgFnS
z	z	k7c2
obvodu	obvod	k1gInSc2
24	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
)	)	kIx)
do	do	k7c2
obvodu	obvod	k1gInSc2
23	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
Vyšehrad	Vyšehrad	k1gInSc1
z	z	k7c2
obvodu	obvod	k1gInSc2
26	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
2	#num#	k4
<g/>
)	)	kIx)
do	do	k7c2
obvodu	obvod	k1gInSc2
27	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
volby	volba	k1gFnPc4
do	do	k7c2
Zastupitelstva	zastupitelstvo	k1gNnSc2
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
a	a	k8xC
2006	#num#	k4
tvořilo	tvořit	k5eAaImAgNnS
území	území	k1gNnSc1
města	město	k1gNnSc2
jediný	jediný	k2eAgInSc1d1
volební	volební	k2eAgInSc1d1
obvod	obvod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
Rada	rada	k1gFnSc1
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
vyhlásila	vyhlásit	k5eAaPmAgFnS
10	#num#	k4
volebních	volební	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
obvody	obvod	k1gInPc1
I.	I.	kA
<g/>
,	,	kIx,
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
V.	V.	kA
<g/>
,	,	kIx,
VI	VI	kA
<g/>
.	.	kIx.
a	a	k8xC
X.	X.	kA
byly	být	k5eAaImAgInP
pětimandátové	pětimandátový	k2eAgInPc1d1
a	a	k8xC
obvody	obvod	k1gInPc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
VII	VII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
VIII	VIII	kA
<g/>
.	.	kIx.
a	a	k8xC
IX	IX	kA
<g/>
.	.	kIx.
šestimandátové	šestimandátový	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
volby	volba	k1gFnPc4
do	do	k7c2
Zastupitelstva	zastupitelstvo	k1gNnSc2
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
tvořilo	tvořit	k5eAaImAgNnS
území	území	k1gNnSc1
města	město	k1gNnSc2
na	na	k7c6
základě	základ	k1gInSc6
usnesení	usnesení	k1gNnSc2
Zastupitelstva	zastupitelstvo	k1gNnSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
č.	č.	k?
42	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
pět	pět	k4xCc1
čtrnáctimandátových	čtrnáctimandátový	k2eAgInPc2d1
volebních	volební	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
I.	I.	kA
–	–	k?
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
7	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Troja	Troja	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Lysolaje	Lysolaje	k1gFnPc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Nebušice	Nebušice	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Přední	přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Suchdol	Suchdol	k1gInSc1
</s>
<s>
II	II	kA
<g/>
.	.	kIx.
–	–	k?
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
12	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
13	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
16	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
17	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Slivenec	Slivenec	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Libuš	Libuš	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Řeporyje	Řeporyje	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Lipence	Lipence	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Lochkov	Lochkov	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Velká	velký	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Zbraslav	Zbraslav	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Zličín	Zličín	k1gInSc1
</s>
<s>
III	III	kA
<g/>
.	.	kIx.
–	–	k?
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
4	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
11	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Kunratice	Kunratice	k1gFnPc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Křeslice	Křeslice	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Šeberov	Šeberov	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Újezd	Újezd	k1gInSc1
</s>
<s>
IV	IV	kA
<g/>
.	.	kIx.
–	–	k?
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
3	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
15	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
22	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Dolní	dolní	k2eAgFnPc4d1
Měcholupy	Měcholupa	k1gFnPc4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Dubeč	Dubeč	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Petrovice	Petrovice	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Štěrboholy	Štěrbohola	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Benice	Benice	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Kolovraty	kolovrat	k1gInPc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Královice	královic	k1gMnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Nedvězí	Nedvěze	k1gFnPc2
</s>
<s>
V.	V.	kA
–	–	k?
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
14	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
18	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
19	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
20	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
21	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Březiněves	Březiněves	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Dolní	dolní	k2eAgFnPc4d1
Chabry	Chabra	k1gFnPc4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
Ďáblice	ďáblice	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Dolní	dolní	k2eAgFnPc4d1
Počernice	Počernice	k1gFnPc4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Čakovice	Čakovice	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Satalice	Satalice	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Vinoř	Vinoř	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Běchovice	Běchovice	k1gFnPc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Klánovice	Klánovice	k1gFnPc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Koloděje	koloděj	k1gMnSc2
</s>
<s>
Pro	pro	k7c4
volby	volba	k1gFnPc4
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
bylo	být	k5eAaImAgNnS
vytvořeno	vytvořit	k5eAaPmNgNnS
7	#num#	k4
volebních	volební	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
se	se	k3xPyFc4
volí	volit	k5eAaImIp3nP
po	po	k7c6
9	#num#	k4
mandátech	mandát	k1gInPc6
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Zástupci	zástupce	k1gMnPc1
menších	malý	k2eAgFnPc2d2
stran	strana	k1gFnPc2
(	(	kIx(
<g/>
například	například	k6eAd1
Markéta	Markéta	k1gFnSc1
Reedová	Reedový	k2eAgFnSc1d1
za	za	k7c7
VV	VV	kA
nebo	nebo	k8xC
František	František	k1gMnSc1
Laudát	Laudát	k1gMnSc1
za	za	k7c4
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
proti	proti	k7c3
zavedení	zavedení	k1gNnSc3
volebních	volební	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
protestovali	protestovat	k5eAaBmAgMnP
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
podle	podle	k7c2
Reedové	Reedové	k2eAgMnPc2d1
fakticky	fakticky	k6eAd1
zvyšuje	zvyšovat	k5eAaImIp3nS
kvórum	kvórum	k1gNnSc4
pro	pro	k7c4
získání	získání	k1gNnSc4
mandátu	mandát	k1gInSc2
na	na	k7c4
cca	cca	kA
11	#num#	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petr	Petr	k1gMnSc1
Hulinský	Hulinský	k2eAgMnSc1d1
z	z	k7c2
ČSSD	ČSSD	kA
rozdělení	rozdělení	k1gNnSc3
vytýkal	vytýkat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
pouze	pouze	k6eAd1
sedmina	sedmina	k1gFnSc1
Pražanů	Pražan	k1gMnPc2
bude	být	k5eAaImBp3nS
moci	moct	k5eAaImF
volit	volit	k5eAaImF
hlavního	hlavní	k2eAgMnSc4d1
lídra	lídr	k1gMnSc4
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
Petr	Petr	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
(	(	kIx(
<g/>
SZ	SZ	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
spojeny	spojit	k5eAaPmNgInP
tak	tak	k9
nesourodé	sourodý	k2eNgFnPc1d1
části	část	k1gFnPc1
jako	jako	k8xS,k8xC
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
a	a	k8xC
Přední	přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ostatních	ostatní	k2eAgInPc6d1
krajích	kraj	k1gInPc6
zákon	zákon	k1gInSc1
nepřipouští	připouštět	k5eNaImIp3nP
dělit	dělit	k5eAaImF
kraj	kraj	k1gInSc4
do	do	k7c2
více	hodně	k6eAd2
volebních	volební	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
,	,	kIx,
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
statutárních	statutární	k2eAgNnPc2d1
měst	město	k1gNnPc2
však	však	k8xC
rozdělení	rozdělení	k1gNnSc1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Návrh	návrh	k1gInSc1
pěti	pět	k4xCc2
stran	strana	k1gFnPc2
(	(	kIx(
<g/>
SZ	SZ	kA
<g/>
,	,	kIx,
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
,	,	kIx,
SNK	SNK	kA
ED	ED	kA
a	a	k8xC
VV	VV	kA
<g/>
)	)	kIx)
z	z	k7c2
9	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2010	#num#	k4
na	na	k7c6
zrušení	zrušení	k1gNnSc6
rozhodnutí	rozhodnutí	k1gNnSc2
Nejvyšší	vysoký	k2eAgInSc4d3
správní	správní	k2eAgInSc4d1
soud	soud	k1gInSc4
9	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
zamítl	zamítnout	k5eAaPmAgInS
jako	jako	k9
nedůvodný	důvodný	k2eNgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
–	–	k?
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
17	#num#	k4
<g/>
,	,	kIx,
Praha-Zličín	Praha-Zličín	k1gInSc1
<g/>
,	,	kIx,
Praha-Přední	Praha-Přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
<g/>
,	,	kIx,
Praha-Nebušice	Praha-Nebušice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Lysolaje	Praha-Lysolaje	k1gFnSc1
<g/>
,	,	kIx,
Praha-Suchdol	Praha-Suchdol	k1gInSc1
</s>
<s>
II	II	kA
<g/>
.	.	kIx.
–	–	k?
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
13	#num#	k4
<g/>
,	,	kIx,
Praha-Řeporyje	Praha-Řeporyje	k1gFnSc1
<g/>
,	,	kIx,
Praha-Slivenec	Praha-Slivenec	k1gInSc1
<g/>
,	,	kIx,
Praha-Velká	Praha-Velký	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
<g/>
,	,	kIx,
Praha-Lochkov	Praha-Lochkov	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
16	#num#	k4
<g/>
,	,	kIx,
Praha-Zbraslav	Praha-Zbraslav	k1gMnSc1
<g/>
,	,	kIx,
Praha-Lipence	Praha-Lipence	k1gFnSc1
</s>
<s>
III	III	kA
<g/>
.	.	kIx.
–	–	k?
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
4	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
12	#num#	k4
</s>
<s>
IV	IV	kA
<g/>
.	.	kIx.
–	–	k?
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
7	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
,	,	kIx,
Praha-Troja	Praha-Troja	k1gFnSc1
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgFnPc1d1
Chabry	Chabra	k1gFnPc1
<g/>
,	,	kIx,
Praha-Březiněves	Praha-Březiněves	k1gInSc1
<g/>
,	,	kIx,
Praha-Čakovice	Praha-Čakovice	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
18	#num#	k4
<g/>
,	,	kIx,
Praha-Ďáblice	Praha-Ďáblice	k1gFnSc1
</s>
<s>
V.	V.	kA
–	–	k?
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
2	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
3	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
19	#num#	k4
<g/>
,	,	kIx,
Praha-Vinoř	Praha-Vinoř	k1gFnSc1
<g/>
,	,	kIx,
Praha-Satalice	Praha-Satalice	k1gFnSc1
</s>
<s>
VI	VI	kA
<g/>
.	.	kIx.
–	–	k?
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
14	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
20	#num#	k4
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgFnPc4d1
Počernice	Počernice	k1gFnPc4
<g/>
,	,	kIx,
Praha-Štěrboholy	Praha-Štěrbohol	k1gInPc4
</s>
<s>
VII	VII	kA
<g/>
.	.	kIx.
–	–	k?
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
11	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
15	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
21	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
22	#num#	k4
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgInPc1d1
Měcholupy	Měcholup	k1gInPc1
<g/>
,	,	kIx,
Praha-Petrovice	Praha-Petrovice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Křeslice	Praha-Křeslice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Újezd	Praha-Újezd	k1gInSc1
<g/>
,	,	kIx,
Praha-Šeberov	Praha-Šeberov	k1gInSc1
<g/>
,	,	kIx,
Praha-Kunratice	Praha-Kunratice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Libuš	Praha-Libuš	k1gInSc1
<g/>
,	,	kIx,
Praha-Benice	Praha-Benice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Dubeč	Praha-Dubeč	k1gInSc1
<g/>
,	,	kIx,
Praha-Běchovice	Praha-Běchovice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Koloděje	Praha-Koloděje	k1gFnSc1
<g/>
,	,	kIx,
Praha-Klánovice	Praha-Klánovice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Kolovraty	Praha-Kolovrat	k1gInPc1
<g/>
,	,	kIx,
Praha-Nedvězí	Praha-Nedvězí	k1gNnPc1
<g/>
,	,	kIx,
Praha-Královice	Praha-Královice	k1gFnPc1
</s>
<s>
Církevní	církevní	k2eAgNnSc1d1
členění	členění	k1gNnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Vikariáty	vikariát	k1gInPc4
a	a	k8xC
farnosti	farnost	k1gFnPc4
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
člení	členit	k5eAaImIp3nS
asi	asi	k9
70	#num#	k4
pražských	pražský	k2eAgMnPc2d1
farnosti	farnost	k1gFnSc2
do	do	k7c2
čtyř	čtyři	k4xCgInPc2
vikariátů	vikariát	k1gInPc2
rozlišených	rozlišený	k2eAgInPc2d1
římskými	římský	k2eAgNnPc7d1
čísly	číslo	k1gNnPc7
<g/>
,	,	kIx,
tyto	tento	k3xDgInPc4
vikariáty	vikariát	k1gInPc4
menší	malý	k2eAgFnSc7d2
částí	část	k1gFnSc7
přesahují	přesahovat	k5eAaImIp3nP
i	i	k9
za	za	k7c4
hranice	hranice	k1gFnPc4
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I.	I.	kA
pražský	pražský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
farnosti	farnost	k1gFnPc4
v	v	k7c6
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
<g/>
,	,	kIx,
II	II	kA
<g/>
.	.	kIx.
pražský	pražský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
západní	západní	k2eAgInSc4d1
břeh	břeh	k1gInSc4
Vltavy	Vltava	k1gFnSc2
<g/>
,	,	kIx,
III	III	kA
<g/>
.	.	kIx.
pražský	pražský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
jihovýchod	jihovýchod	k1gInSc4
Prahy	Praha	k1gFnSc2
a	a	k8xC
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
pražský	pražský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
severovýchod	severovýchod	k1gInSc1
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Českobratrská	českobratrský	k2eAgFnSc1d1
církev	církev	k1gFnSc1
evangelická	evangelický	k2eAgFnSc1d1
</s>
<s>
Českobratrská	českobratrský	k2eAgFnSc1d1
církev	církev	k1gFnSc1
evangelická	evangelický	k2eAgFnSc1d1
má	mít	k5eAaImIp3nS
v	v	k7c6
Praze	Praha	k1gFnSc6
21	#num#	k4
sborů	sbor	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
Pražského	pražský	k2eAgInSc2d1
seniorátu	seniorát	k1gInSc2
<g/>
;	;	kIx,
tento	tento	k3xDgInSc1
seniorát	seniorát	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
nejen	nejen	k6eAd1
území	území	k1gNnSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
podstatnou	podstatný	k2eAgFnSc4d1
část	část	k1gFnSc4
Středočeského	středočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
k	k	k7c3
němu	on	k3xPp3gInSc3
náleží	náležet	k5eAaImIp3nS
dalších	další	k2eAgInPc2d1
10	#num#	k4
sborů	sbor	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Církev	církev	k1gFnSc1
československá	československý	k2eAgFnSc1d1
husitská	husitský	k2eAgFnSc1d1
</s>
<s>
Církev	církev	k1gFnSc1
československá	československý	k2eAgFnSc1d1
husitská	husitský	k2eAgFnSc1d1
člení	členit	k5eAaImIp3nS
svých	svůj	k3xOyFgFnPc2
31	#num#	k4
náboženských	náboženský	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
do	do	k7c2
dvou	dva	k4xCgInPc2
vikariátů	vikariát	k1gInPc2
–	–	k?
Praha	Praha	k1gFnSc1
východ	východ	k1gInSc1
a	a	k8xC
Praha	Praha	k1gFnSc1
západ	západ	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
vikariáty	vikariát	k1gInPc1
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
Pražské	pražský	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1
obec	obec	k1gFnSc1
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1
obec	obec	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
zahrnuje	zahrnovat	k5eAaImIp3nS
celé	celý	k2eAgNnSc4d1
území	území	k1gNnSc4
Prahy	Praha	k1gFnSc2
<g/>
;	;	kIx,
celkově	celkově	k6eAd1
tato	tento	k3xDgFnSc1
obec	obec	k1gFnSc1
sdružuje	sdružovat	k5eAaImIp3nS
občany	občan	k1gMnPc4
<g/>
,	,	kIx,
hlásící	hlásící	k2eAgNnPc4d1
se	se	k3xPyFc4
k	k	k7c3
židovskému	židovský	k2eAgNnSc3d1
vyznání	vyznání	k1gNnSc3
<g/>
,	,	kIx,
národnosti	národnost	k1gFnSc3
nebo	nebo	k8xC
původu	původ	k1gInSc2
z	z	k7c2
více	hodně	k6eAd2
než	než	k8xS
40	#num#	k4
%	%	kIx~
území	území	k1gNnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
kolektiv	kolektiv	k1gInSc4
autorů	autor	k1gMnPc2
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
Prahy	Praha	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
nakl	nakl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paseka	paseka	k1gFnSc1
1998	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
521	#num#	k4
<g/>
,	,	kIx,
bod	bod	k1gInSc1
20	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7185-143-4	80-7185-143-4	k4
</s>
<s>
Členění	členění	k1gNnSc1
Prahy	Praha	k1gFnSc2
<g/>
↑	↑	k?
Úvod	úvod	k1gInSc1
o	o	k7c6
Praze	Praha	k1gFnSc6
14	#num#	k4
Archivováno	archivován	k2eAgNnSc4d1
27	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
(	(	kIx(
<g/>
web	web	k1gInSc4
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Lexikon	lexikon	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
2009	#num#	k4
<g/>
,	,	kIx,
ČSÚ	ČSÚ	kA
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
↑	↑	k?
Vyhláška	vyhláška	k1gFnSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
č.	č.	k?
13	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
HMP	HMP	kA
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc7,k3yRgFnSc7,k3yQgFnSc7
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
Statut	statut	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schválena	schválen	k2eAgFnSc1d1
20	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
účinná	účinný	k2eAgFnSc1d1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Zasedání	zasedání	k1gNnSc1
zastupitelstva	zastupitelstvo	k1gNnSc2
obce	obec	k1gFnSc2
Přezletice	Přezletika	k1gFnSc6
dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
Archivováno	archivován	k2eAgNnSc4d1
29	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
bod	bod	k1gInSc1
6	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
pověření	pověření	k1gNnSc4
starostky	starostka	k1gFnSc2
a	a	k8xC
místostarosty	místostarosta	k1gMnSc2
k	k	k7c3
zahájení	zahájení	k1gNnSc3
jednání	jednání	k1gNnSc2
o	o	k7c4
připojení	připojení	k1gNnSc4
k	k	k7c3
hlavnímu	hlavní	k2eAgNnSc3d1
městu	město	k1gNnSc3
Praze	Praha	k1gFnSc3
na	na	k7c6
základě	základ	k1gInSc6
podnětů	podnět	k1gInPc2
od	od	k7c2
občanů	občan	k1gMnPc2
a	a	k8xC
výsledků	výsledek	k1gInPc2
ankety	anketa	k1gFnSc2
<g/>
.1	.1	k4
2	#num#	k4
Přezletice	Přezletika	k1gFnSc6
chtějí	chtít	k5eAaImIp3nP
spadat	spadat	k5eAaPmF,k5eAaImF
pod	pod	k7c4
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
<g/>
,	,	kIx,
Pražský	pražský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
↑	↑	k?
Praha	Praha	k1gFnSc1
se	se	k3xPyFc4
možná	možná	k9
<g />
.	.	kIx.
</s>
<s hack="1">
zvětší	zvětšit	k5eAaPmIp3nS
<g/>
,	,	kIx,
připojit	připojit	k5eAaPmF
se	se	k3xPyFc4
chce	chtít	k5eAaImIp3nS
obec	obec	k1gFnSc1
Přezletice	Přezletika	k1gFnSc3
(	(	kIx(
<g/>
Deník	deník	k1gInSc1
veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
ČTK	ČTK	kA
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Přezletice	Přezletika	k1gFnSc6
chtějí	chtít	k5eAaImIp3nP
spadat	spadat	k5eAaImF,k5eAaPmF
pod	pod	k7c4
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
(	(	kIx(
<g/>
Pražský	pražský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
Praha	Praha	k1gFnSc1
se	se	k3xPyFc4
po	po	k7c6
30	#num#	k4
letech	léto	k1gNnPc6
rozroste	rozrůst	k5eAaPmIp3nS
(	(	kIx(
<g/>
lidovky	lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Usnesení	usnesení	k1gNnSc4
Rady	rada	k1gFnSc2
HMP	HMP	kA
č.	č.	k?
1415	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
k	k	k7c3
záměru	záměr	k1gInSc3
obce	obec	k1gFnSc2
Přezletice	Přezletika	k1gFnSc3
na	na	k7c4
připojení	připojení	k1gNnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
k	k	k7c3
území	území	k1gNnSc3
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Jakub	Jakub	k1gMnSc1
Mračno	mračno	k1gNnSc1
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
rozšířit	rozšířit	k5eAaPmF
o	o	k7c4
další	další	k2eAgFnPc4d1
obce	obec	k1gFnPc4
<g/>
,	,	kIx,
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
↑	↑	k?
Praha	Praha	k1gFnSc1
bude	být	k5eAaImBp3nS
nejspíš	nejspíš	k9
větší	veliký	k2eAgMnSc1d2
o	o	k7c6
Přezletice	Přezletika	k1gFnSc6
<g/>
,	,	kIx,
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
↑	↑	k?
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Jan	Jan	k1gMnSc1
Stuchlík	Stuchlík	k1gMnSc1
<g/>
:	:	kIx,
Hora	hora	k1gFnSc1
Svaté	svatý	k2eAgFnSc2d1
Kateřiny	Kateřina	k1gFnSc2
žádá	žádat	k5eAaImIp3nS
o	o	k7c4
připojení	připojení	k1gNnSc4
k	k	k7c3
Praze	Praha	k1gFnSc3
<g/>
,	,	kIx,
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
↑	↑	k?
Obecně	obecně	k6eAd1
závazná	závazný	k2eAgFnSc1d1
vyhláška	vyhláška	k1gFnSc1
č.	č.	k?
55	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
se	se	k3xPyFc4
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
Statut	statut	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
(	(	kIx(
<g/>
úplné	úplný	k2eAgNnSc1d1
znění	znění	k1gNnSc1
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
§	§	k?
4	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
36	#num#	k4
<g/>
/	/	kIx~
<g/>
1960	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
územním	územní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
<g/>
,	,	kIx,
§	§	k?
2	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vyhláška	vyhláška	k1gFnSc1
č.	č.	k?
564	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c4
stanovení	stanovení	k1gNnSc4
území	území	k1gNnSc2
okresů	okres	k1gInPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
území	území	k1gNnSc6
obvodů	obvod	k1gInPc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
§	§	k?
2	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Tomáš	Tomáš	k1gMnSc1
Havrda	Havrda	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Kuča	Kuča	k?
<g/>
:	:	kIx,
Urbanistický	urbanistický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
města	město	k1gNnSc2
–	–	k?
Výstup	výstup	k1gInSc1
pro	pro	k7c4
územně	územně	k6eAd1
analytické	analytický	k2eAgInPc4d1
podklady	podklad	k1gInPc4
<g/>
,	,	kIx,
červen	červen	k1gInSc1
2010	#num#	k4
<g/>
,	,	kIx,
pro	pro	k7c4
Útvar	útvar	k1gInSc4
rozvoje	rozvoj	k1gInSc2
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
<g/>
↑	↑	k?
ZÚK	ZÚK	kA
chodníků	chodník	k1gInPc2
-	-	kIx~
kontakty	kontakt	k1gInPc4
Oblastní	oblastní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
TSK	TSK	kA
HMP	HMP	kA
a.s.	a.s.	k?
<g/>
↑	↑	k?
Popis	popis	k1gInSc4
volebního	volební	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
číslo	číslo	k1gNnSc1
17	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Popis	popis	k1gInSc4
volebního	volební	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
číslo	číslo	k1gNnSc4
19	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Popis	popis	k1gInSc4
volebního	volební	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
číslo	číslo	k1gNnSc4
20	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Popis	popis	k1gInSc4
volebního	volební	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
číslo	číslo	k1gNnSc4
21	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
<g/>
↑	↑	k?
Popis	popis	k1gInSc4
volebního	volební	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
číslo	číslo	k1gNnSc4
22	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Popis	popis	k1gInSc4
volebního	volební	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
číslo	číslo	k1gNnSc4
23	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Popis	popis	k1gInSc4
volebního	volební	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
číslo	číslo	k1gNnSc4
24	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Popis	popis	k1gInSc4
volebního	volební	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
číslo	číslo	k1gNnSc1
25	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Popis	popis	k1gInSc4
volebního	volební	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
číslo	číslo	k1gNnSc4
26	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Popis	popis	k1gInSc4
volebního	volební	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
číslo	číslo	k1gNnSc4
27	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Vymezení	vymezení	k1gNnSc2
volebních	volební	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
pro	pro	k7c4
volby	volba	k1gFnPc4
do	do	k7c2
Zastupitelstva	zastupitelstvo	k1gNnSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
Informační	informační	k2eAgInSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
server	server	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
22	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
↑	↑	k?
Usnesení	usnesení	k1gNnSc1
Zastupitelstva	zastupitelstvo	k1gNnSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
č.	č.	k?
39	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
(	(	kIx(
<g/>
tisk	tisk	k1gInSc1
Z-	Z-	k1gFnSc2
<g/>
617	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ke	k	k7c3
stanovení	stanovení	k1gNnSc3
počtu	počet	k1gInSc2
členů	člen	k1gMnPc2
Zastupitelstva	zastupitelstvo	k1gNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
pro	pro	k7c4
volební	volební	k2eAgNnSc4d1
období	období	k1gNnSc4
2010	#num#	k4
-	-	kIx~
2014	#num#	k4
<g/>
,	,	kIx,
ke	k	k7c3
stanovení	stanovení	k1gNnSc3
volebních	volební	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
a	a	k8xC
počtu	počet	k1gInSc2
členů	člen	k1gMnPc2
Zastupitelstva	zastupitelstvo	k1gNnPc4
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
volených	volená	k1gFnPc2
v	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
volebních	volební	k2eAgInPc6d1
obvodech	obvod	k1gInPc6
pro	pro	k7c4
volby	volba	k1gFnPc4
do	do	k7c2
Zastupitelstva	zastupitelstvo	k1gNnSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Praha	Praha	k1gFnSc1
mění	měnit	k5eAaImIp3nS
volební	volební	k2eAgInPc4d1
obvody	obvod	k1gInPc4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
menší	malý	k2eAgFnPc1d2
strany	strana	k1gFnPc1
to	ten	k3xDgNnSc4
považují	považovat	k5eAaImIp3nP
za	za	k7c4
diskriminaci	diskriminace	k1gFnSc4
Archivováno	archivovat	k5eAaBmNgNnS
20	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
E	E	kA
<g/>
15	#num#	k4
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Holuša	Holuša	k1gMnSc1
<g/>
,	,	kIx,
Mediafax	Mediafax	k1gInSc1
<g/>
↑	↑	k?
,	,	kIx,
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
,	,	kIx,
15	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
,	,	kIx,
Lucie	Lucie	k1gFnSc1
Strašíková	Strašíková	k1gFnSc1
<g/>
↑	↑	k?
NSS	NSS	kA
zamítl	zamítnout	k5eAaPmAgInS
návrh	návrh	k1gInSc4
na	na	k7c4
zrušení	zrušení	k1gNnSc4
nových	nový	k2eAgInPc2d1
volebních	volební	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
[	[	kIx(
<g/>
rozsudek	rozsudek	k1gInSc4
4	#num#	k4
Ao	Ao	k1gFnSc1
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
195	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Brně	Brno	k1gNnSc6
dne	den	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2010	#num#	k4
<g/>
,	,	kIx,
Nejvyšší	vysoký	k2eAgInSc1d3
správní	správní	k2eAgInSc1d1
soud	soud	k1gInSc1
<g/>
↑	↑	k?
http://prazsky-seniorat.evangnet.cz/clanek/100-Sbory/index.htm	http://prazsky-seniorat.evangnet.cz/clanek/100-Sbory/index.htm	k1gInSc1
<g/>
↑	↑	k?
http://www.husiti.cz/ccshpd/vikariaty.htm	http://www.husiti.cz/ccshpd/vikariaty.htm	k1gInSc1
<g/>
↑	↑	k?
http://www.kehilaprag.cz/	http://www.kehilaprag.cz/	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Správní	správní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Městský	městský	k2eAgInSc1d1
obvod	obvod	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
<g/>
#	#	kIx~
<g/>
Členění	členění	k1gNnSc1
Prahy	Praha	k1gFnSc2
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Prahy	Praha	k1gFnSc2
</s>
<s>
Zaniklé	zaniklý	k2eAgFnPc4d1
vesnice	vesnice	k1gFnPc4
na	na	k7c6
území	území	k1gNnSc6
Prahy	Praha	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
Prahy	Praha	k1gFnSc2
podle	podle	k7c2
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
části	část	k1gFnSc2
Prahy	Praha	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Prahy	Praha	k1gFnSc2
I.	I.	kA
<g/>
,	,	kIx,
II	II	kA
<g/>
.	.	kIx.
-	-	kIx~
nakl	nakl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
,	,	kIx,
kolektiv	kolektiv	k1gInSc1
autorů	autor	k1gMnPc2
</s>
<s>
Pražská	pražský	k2eAgFnSc1d1
informační	informační	k2eAgFnPc4d1
služby	služba	k1gFnPc4
–	–	k?
členění	členění	k1gNnSc1
Prahy	Praha	k1gFnSc2
</s>
<s>
Zmiňované	zmiňovaný	k2eAgInPc1d1
právní	právní	k2eAgInPc1d1
předpisy	předpis	k1gInPc1
</s>
<s>
Vládní	vládní	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
č.	č.	k?
79	#num#	k4
<g/>
/	/	kIx~
<g/>
1949	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c4
rozdělení	rozdělení	k1gNnSc4
území	území	k1gNnSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
pro	pro	k7c4
účely	účel	k1gInPc4
veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
ze	z	k7c2
dne	den	k1gInSc2
31	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1949	#num#	k4
<g/>
,	,	kIx,
účinnost	účinnost	k1gFnSc1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1949	#num#	k4
</s>
<s>
Zpráva	zpráva	k1gFnSc1
Policie	policie	k1gFnSc2
ČR	ČR	kA
-	-	kIx~
neplatný	platný	k2eNgInSc1d1
odkaz	odkaz	k1gInSc1
</s>
<s>
Sněmovní	sněmovní	k2eAgInSc1d1
tisk	tisk	k1gInSc1
č.	č.	k?
1047	#num#	k4
–	–	k?
návrh	návrh	k1gInSc1
vlády	vláda	k1gFnSc2
na	na	k7c4
zrušení	zrušení	k1gNnSc4
10	#num#	k4
obvodů	obvod	k1gInPc2
</s>
<s>
Sněmovní	sněmovní	k2eAgInSc1d1
tisk	tisk	k1gInSc1
č.	č.	k?
1048	#num#	k4
–	–	k?
návrh	návrh	k1gInSc1
vlády	vláda	k1gFnSc2
na	na	k7c6
zřízení	zřízení	k1gNnSc6
soudních	soudní	k2eAgInPc2d1
a	a	k8xC
policejních	policejní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
</s>
<s>
http://www.praha.eu	http://www.praha.eu	k6eAd1
–	–	k?
web	web	k1gInSc4
Magistrátu	magistrát	k1gInSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
</s>
<s>
https://web.archive.org/web/20130328120025/http://mpp.praha.eu/VykresyUP/	https://web.archive.org/web/20130328120025/http://mpp.praha.eu/VykresyUP/	k4
-	-	kIx~
Mapa	mapa	k1gFnSc1
Prahy	Praha	k1gFnSc2
(	(	kIx(
<g/>
výkres	výkres	k1gInSc1
územního	územní	k2eAgInSc2d1
plánu	plán	k1gInSc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
pražského	pražský	k2eAgInSc2d1
magistrátu	magistrát	k1gInSc2
<g/>
)	)	kIx)
se	s	k7c7
zakreslením	zakreslení	k1gNnSc7
(	(	kIx(
<g/>
volitelným	volitelný	k2eAgInSc7d1
<g/>
)	)	kIx)
hranic	hranice	k1gFnPc2
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
,	,	kIx,
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
,	,	kIx,
parcel	parcela	k1gFnPc2
<g/>
,	,	kIx,
aj.	aj.	kA
</s>
<s>
Popis	popis	k1gInSc1
čtvrtí	čtvrt	k1gFnPc2
Velké	velký	k2eAgFnSc2d1
Prahy	Praha	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1929	#num#	k4
</s>
<s>
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Historie	historie	k1gFnSc1
vnitřního	vnitřní	k2eAgNnSc2d1
uspořádání	uspořádání	k1gNnSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
</s>
<s>
Územně	územně	k6eAd1
identifikační	identifikační	k2eAgInSc1d1
registr	registr	k1gInSc1
ČR	ČR	kA
-	-	kIx~
ÚIR	ÚIR	kA
</s>
<s>
Správní	správní	k2eAgInPc1d1
obvody	obvod	k1gInPc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
prahainfo	prahainfo	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Městské	městský	k2eAgFnSc3d1
části	část	k1gFnSc3
na	na	k7c6
stránkách	stránka	k1gFnPc6
prahainfo	prahainfo	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Městské	městský	k2eAgInPc1d1
obvody	obvod	k1gInPc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
prahainfo	prahainfo	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Městské	městský	k2eAgFnSc3d1
části	část	k1gFnSc3
na	na	k7c6
stránkách	stránka	k1gFnPc6
prahanadlani	prahanadlaň	k1gFnSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Praha	Praha	k1gFnSc1
–	–	k?
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
městské	městský	k2eAgInPc1d1
obvody	obvod	k1gInPc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
správní	správní	k2eAgInPc1d1
obvody	obvod	k1gInPc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
22	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
(	(	kIx(
<g/>
57	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
1	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
1	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
2	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
2	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
2	#num#	k4
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
3	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
3	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
3	#num#	k4
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
4	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
4	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
4	#num#	k4
<g/>
,	,	kIx,
Praha-Kunratice	Praha-Kunratice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
11	#num#	k4
(	(	kIx(
<g/>
část	část	k1gFnSc1
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
11	#num#	k4
<g/>
,	,	kIx,
Praha-Šeberov	Praha-Šeberov	k1gInSc1
<g/>
,	,	kIx,
Praha-Újezd	Praha-Újezd	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
12	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
12	#num#	k4
<g/>
,	,	kIx,
Praha-Libuš	Praha-Libuš	k1gInSc1
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
5	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
5	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
5	#num#	k4
<g/>
,	,	kIx,
Praha-Slivenec	Praha-Slivenec	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
13	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
13	#num#	k4
<g/>
,	,	kIx,
Praha-Řeporyje	Praha-Řeporyje	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
16	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
16	#num#	k4
<g/>
,	,	kIx,
Praha-Velká	Praha-Velký	k2eAgFnSc1d1
Chuchle	Chuchle	k1gFnSc1
<g/>
,	,	kIx,
Praha-Lochkov	Praha-Lochkov	k1gInSc1
<g/>
,	,	kIx,
Praha-Zbraslav	Praha-Zbraslav	k1gMnSc1
<g/>
,	,	kIx,
Praha-Lipence	Praha-Lipence	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
17	#num#	k4
(	(	kIx(
<g/>
část	část	k1gFnSc1
<g/>
:	:	kIx,
Praha-Zličín	Praha-Zličín	k1gInSc1
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
6	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
6	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
6	#num#	k4
<g/>
,	,	kIx,
Praha-Suchdol	Praha-Suchdol	k1gInSc1
<g/>
,	,	kIx,
Praha-Lysolaje	Praha-Lysolaje	k1gFnSc1
<g/>
,	,	kIx,
Praha-Nebušice	Praha-Nebušice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Přední	Praha-Přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
17	#num#	k4
(	(	kIx(
<g/>
část	část	k1gFnSc1
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
17	#num#	k4
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
7	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
7	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
7	#num#	k4
<g/>
,	,	kIx,
Praha-Troja	Praha-Troja	k1gFnSc1
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
8	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
8	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
,	,	kIx,
Praha-Ďáblice	Praha-Ďáblice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Březiněves	Praha-Březiněves	k1gInSc1
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgInPc1d1
Chabry	Chabr	k1gInPc1
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
9	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
9	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
14	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
14	#num#	k4
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgFnPc4d1
Počernice	Počernice	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
18	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
18	#num#	k4
<g/>
,	,	kIx,
Praha-Čakovice	Praha-Čakovice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
19	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
19	#num#	k4
<g/>
,	,	kIx,
Praha-Vinoř	Praha-Vinoř	k1gFnSc1
<g/>
,	,	kIx,
Praha-Satalice	Praha-Satalice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
20	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
20	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
21	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
21	#num#	k4
<g/>
,	,	kIx,
Praha-Klánovice	Praha-Klánovice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Koloděje	Praha-Koloděje	k1gFnSc1
<g/>
,	,	kIx,
Praha-Běchovice	Praha-Běchovice	k1gFnSc1
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
10	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
10	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
15	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
15	#num#	k4
<g/>
,	,	kIx,
Praha-Dolní	Praha-Dolní	k2eAgInPc1d1
Měcholupy	Měcholup	k1gInPc1
<g/>
,	,	kIx,
Praha-Štěrboholy	Praha-Štěrbohol	k1gInPc1
<g/>
,	,	kIx,
Praha-Petrovice	Praha-Petrovice	k1gFnPc1
<g/>
,	,	kIx,
Praha-Dubeč	Praha-Dubeč	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
22	#num#	k4
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
22	#num#	k4
<g/>
,	,	kIx,
Praha-Královice	Praha-Královice	k1gFnSc1
<g/>
,	,	kIx,
Praha-Nedvězí	Praha-Nedvězí	k1gNnPc1
<g/>
,	,	kIx,
Praha-Kolovraty	Praha-Kolovrat	k1gInPc1
<g/>
,	,	kIx,
Praha-Benice	Praha-Benice	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
11	#num#	k4
(	(	kIx(
<g/>
část	část	k1gFnSc1
<g/>
:	:	kIx,
Praha-Křeslice	Praha-Křeslice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
a	a	k8xC
části	část	k1gFnPc1
obce	obec	k1gFnSc2
(	(	kIx(
<g/>
112	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Běchovice	Běchovice	k1gFnPc1
•	•	k?
Benice	Benice	k1gFnSc1
•	•	k?
Bohnice	Bohnice	k1gInPc1
•	•	k?
Braník	Braník	k1gInSc1
•	•	k?
Břevnov	Břevnov	k1gInSc1
•	•	k?
Březiněves	Březiněves	k1gInSc1
•	•	k?
Bubeneč	Bubeneč	k1gInSc1
•	•	k?
Čakovice	Čakovice	k1gFnSc2
•	•	k?
Černý	černý	k2eAgInSc1d1
Most	most	k1gInSc1
•	•	k?
Čimice	Čimice	k1gFnSc2
•	•	k?
Dejvice	Dejvice	k1gFnPc1
•	•	k?
Dolní	dolní	k2eAgFnPc1d1
Chabry	Chabra	k1gFnPc1
•	•	k?
Dolní	dolní	k2eAgFnPc1d1
Měcholupy	Měcholupa	k1gFnPc1
•	•	k?
Dolní	dolní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
•	•	k?
Dubeč	Dubeč	k1gMnSc1
•	•	k?
Ďáblice	ďáblice	k1gFnSc2
•	•	k?
Háje	háj	k1gInSc2
•	•	k?
Hájek	Hájek	k1gMnSc1
u	u	k7c2
Uhříněvsi	Uhříněves	k1gFnSc2
(	(	kIx(
<g/>
dř	dř	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hájek	Hájek	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Hloubětín	Hloubětín	k1gInSc1
•	•	k?
Hlubočepy	Hlubočepa	k1gFnSc2
•	•	k?
Hodkovičky	Hodkovička	k1gFnSc2
•	•	k?
Holešovice	Holešovice	k1gFnPc1
(	(	kIx(
<g/>
dř	dř	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Holešovice-Bubny	Holešovice-Bubna	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Holyně	Holyně	k1gFnSc2
•	•	k?
Horní	horní	k2eAgFnSc2d1
Měcholupy	Měcholupa	k1gFnSc2
•	•	k?
Horní	horní	k2eAgFnPc1d1
Počernice	Počernice	k1gFnPc1
•	•	k?
Hostavice	Hostavice	k1gFnSc1
•	•	k?
Hostivař	Hostivař	k1gFnSc1
•	•	k?
Hradčany	Hradčany	k1gInPc1
•	•	k?
Hrdlořezy	Hrdlořezy	k1gInPc1
•	•	k?
Chodov	Chodov	k1gInSc1
•	•	k?
Cholupice	Cholupice	k1gFnSc2
•	•	k?
Jinonice	Jinonice	k1gFnPc4
•	•	k?
Josefov	Josefov	k1gInSc1
•	•	k?
Kamýk	Kamýk	k1gInSc1
•	•	k?
Karlín	Karlín	k1gInSc1
•	•	k?
Kbely	Kbely	k1gInPc1
•	•	k?
Klánovice	Klánovice	k1gFnPc4
•	•	k?
Kobylisy	Kobylisy	k1gInPc1
•	•	k?
Koloděje	koloděj	k1gMnSc2
•	•	k?
Kolovraty	kolovrat	k1gInPc1
•	•	k?
Komořany	Komořan	k1gMnPc4
•	•	k?
Košíře	Košíře	k1gInPc1
•	•	k?
Královice	královic	k1gMnSc2
•	•	k?
Krč	Krč	k1gFnSc4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Křeslice	Křeslice	k1gFnSc1
•	•	k?
Kunratice	Kunratice	k1gFnPc4
•	•	k?
Kyje	kyje	k1gFnPc4
•	•	k?
Lahovice	Lahovice	k1gFnSc2
•	•	k?
Letňany	Letňan	k1gMnPc4
•	•	k?
Lhotka	Lhotka	k1gFnSc1
•	•	k?
Libeň	Libeň	k1gFnSc1
•	•	k?
Liboc	Liboc	k1gFnSc1
•	•	k?
Libuš	Libuš	k1gInSc1
•	•	k?
Lipany	lipan	k1gMnPc7
•	•	k?
Lipence	Lipenec	k1gInSc2
•	•	k?
Lochkov	Lochkov	k1gInSc1
•	•	k?
Lysolaje	Lysolaje	k1gFnPc4
•	•	k?
Malá	Malá	k1gFnSc1
Chuchle	chuchel	k1gInSc2
•	•	k?
Malá	malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
•	•	k?
Malešice	Malešice	k1gFnPc4
•	•	k?
Michle	Michl	k1gMnSc5
•	•	k?
Miškovice	Miškovice	k1gFnSc1
•	•	k?
Modřany	Modřany	k1gInPc1
•	•	k?
Motol	Motol	k1gInSc1
•	•	k?
Nebušice	Nebušice	k1gFnSc1
•	•	k?
Nedvězí	Nedvěze	k1gFnPc2
u	u	k7c2
Říčan	Říčany	k1gInPc2
(	(	kIx(
<g/>
dř	dř	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedvězí	Nedvězí	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
•	•	k?
Nusle	Nusle	k1gFnPc4
•	•	k?
Petrovice	Petrovice	k1gFnSc2
•	•	k?
Písnice	písnice	k1gFnSc2
•	•	k?
Pitkovice	Pitkovice	k1gFnSc1
•	•	k?
Podolí	Podolí	k1gNnSc2
•	•	k?
Prosek	proséct	k5eAaPmDgInS
•	•	k?
Přední	přední	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
•	•	k?
Radlice	radlice	k1gFnSc2
•	•	k?
Radotín	Radotín	k1gMnSc1
•	•	k?
Ruzyně	Ruzyně	k1gFnSc2
•	•	k?
Řeporyje	Řeporyje	k1gFnSc2
•	•	k?
Řepy	řepa	k1gFnSc2
•	•	k?
Satalice	Satalice	k1gFnSc2
•	•	k?
Sedlec	Sedlec	k1gInSc1
•	•	k?
Slivenec	Slivenec	k1gInSc1
•	•	k?
Smíchov	Smíchov	k1gInSc1
•	•	k?
Sobín	Sobín	k1gMnSc1
•	•	k?
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
•	•	k?
Stodůlky	stodůlka	k1gFnPc4
•	•	k?
Strašnice	Strašnice	k1gFnPc4
•	•	k?
Střešovice	Střešovice	k1gFnSc2
•	•	k?
Střížkov	Střížkov	k1gInSc1
•	•	k?
Suchdol	Suchdol	k1gInSc1
•	•	k?
Šeberov	Šeberov	k1gInSc1
•	•	k?
Štěrboholy	Štěrbohola	k1gFnSc2
•	•	k?
Točná	točný	k2eAgFnSc1d1
•	•	k?
Troja	Troja	k1gFnSc1
•	•	k?
Třebonice	Třebonice	k1gFnSc2
•	•	k?
Třeboradice	Třeboradice	k1gFnSc2
•	•	k?
Uhříněves	Uhříněves	k1gFnSc1
•	•	k?
Újezd	Újezd	k1gInSc1
nad	nad	k7c7
Lesy	les	k1gInPc7
•	•	k?
Újezd	Újezd	k1gInSc1
u	u	k7c2
Průhonic	Průhonice	k1gFnPc2
(	(	kIx(
<g/>
dř	dř	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Újezd	Újezd	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Veleslavín	Veleslavín	k1gMnSc1
•	•	k?
Velká	velká	k1gFnSc1
Chuchle	Chuchle	k1gFnSc2
•	•	k?
Vinohrady	Vinohrady	k1gInPc4
(	(	kIx(
<g/>
dř	dř	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královské	královský	k2eAgInPc1d1
Vinohrady	Vinohrady	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Vinoř	Vinoř	k1gFnSc1
•	•	k?
Vokovice	Vokovice	k1gFnPc4
•	•	k?
Vršovice	Vršovice	k1gFnPc4
•	•	k?
Vysočany	Vysočany	k1gInPc1
•	•	k?
Vyšehrad	Vyšehrad	k1gInSc1
•	•	k?
Záběhlice	Záběhlice	k1gFnPc4
•	•	k?
Zadní	zadní	k2eAgFnSc1d1
Kopanina	kopanina	k1gFnSc1
•	•	k?
Zbraslav	Zbraslav	k1gFnSc1
•	•	k?
Zličín	Zličín	k1gInSc1
•	•	k?
Žižkov	Žižkov	k1gInSc1
O	o	k7c4
členění	členění	k1gNnSc4
Prahy	Praha	k1gFnSc2
na	na	k7c4
obvody	obvod	k1gInPc4
<g/>
,	,	kIx,
správní	správní	k2eAgInPc1d1
obvody	obvod	k1gInPc1
<g/>
,	,	kIx,
městské	městský	k2eAgInPc1d1
části	část	k1gFnPc4
a	a	k8xC
katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Části	část	k1gFnSc2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
</s>
