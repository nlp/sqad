<s>
Zuby	zub	k1gInPc1	zub
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
všechny	všechen	k3xTgInPc4	všechen
brachiodontní	brachiodontní	k2eAgInPc4d1	brachiodontní
zuby	zub	k1gInPc4	zub
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
z	z	k7c2	z
kořene	kořen	k1gInSc2	kořen
<g/>
,	,	kIx,	,
krčku	krček	k1gInSc2	krček
a	a	k8xC	a
korunky	korunka	k1gFnSc2	korunka
<g/>
.	.	kIx.	.
</s>
