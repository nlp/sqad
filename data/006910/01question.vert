<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
americký	americký	k2eAgInSc1d1	americký
seriál	seriál	k1gInSc1	seriál
o	o	k7c6	o
případech	případ	k1gInPc6	případ
detektiva	detektiv	k1gMnSc4	detektiv
Monka	Monek	k1gMnSc4	Monek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
trpí	trpět	k5eAaImIp3nS	trpět
obsedantně	obsedantně	k6eAd1	obsedantně
kompulzivní	kompulzivní	k2eAgFnSc7d1	kompulzivní
poruchou	porucha	k1gFnSc7	porucha
a	a	k8xC	a
různými	různý	k2eAgFnPc7d1	různá
fobiemi	fobie	k1gFnPc7	fobie
<g/>
?	?	kIx.	?
</s>
