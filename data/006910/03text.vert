<s>
Můj	můj	k3xOp1gMnSc1	můj
přítel	přítel	k1gMnSc1	přítel
Monk	Monk	k1gMnSc1	Monk
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Monk	Monk	k1gInSc1	Monk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
seriál	seriál	k1gInSc1	seriál
o	o	k7c6	o
případech	případ	k1gInPc6	případ
soukromého	soukromý	k2eAgMnSc4d1	soukromý
detektiva	detektiv	k1gMnSc4	detektiv
Adriana	Adrian	k1gMnSc4	Adrian
Monka	Monek	k1gMnSc4	Monek
(	(	kIx(	(
<g/>
Tony	Tony	k1gMnSc1	Tony
Shalhoub	Shalhoub	k1gMnSc1	Shalhoub
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trpícího	trpící	k2eAgInSc2d1	trpící
obsedantně	obsedantně	k6eAd1	obsedantně
kompulzivní	kompulzivní	k2eAgFnSc7d1	kompulzivní
poruchou	porucha	k1gFnSc7	porucha
a	a	k8xC	a
různými	různý	k2eAgFnPc7d1	různá
fobiemi	fobie	k1gFnPc7	fobie
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
vysílat	vysílat	k5eAaImF	vysílat
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2002	[number]	k4	2002
na	na	k7c6	na
USA	USA	kA	USA
Network	network	k1gInSc1	network
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc1d1	poslední
díl	díl	k1gInSc1	díl
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
osmé	osmý	k4xOgFnSc2	osmý
série	série	k1gFnSc2	série
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
USA	USA	kA	USA
odvysílán	odvysílán	k2eAgInSc1d1	odvysílán
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
byl	být	k5eAaImAgInS	být
seriál	seriál	k1gInSc1	seriál
uveden	uvést	k5eAaPmNgInS	uvést
na	na	k7c6	na
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
a	a	k8xC	a
Prima	prima	k2eAgInPc1d1	prima
family	famil	k1gInPc1	famil
<g/>
.	.	kIx.	.
</s>
<s>
Adrian	Adrian	k1gMnSc1	Adrian
Monk	Monk	k1gMnSc1	Monk
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
bratry	bratr	k1gMnPc4	bratr
<g/>
,	,	kIx,	,
vlastního	vlastní	k2eAgInSc2d1	vlastní
Ambrose	Ambrosa	k1gFnSc6	Ambrosa
Monka	Monek	k1gMnSc4	Monek
a	a	k8xC	a
nevlastního	vlastní	k2eNgMnSc4d1	nevlastní
Jacka	Jacek	k1gMnSc4	Jacek
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc4	Jr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
s	s	k7c7	s
mírnými	mírný	k2eAgInPc7d1	mírný
projevy	projev	k1gInPc7	projev
obsedantně	obsedantně	k6eAd1	obsedantně
kompulzivní	kompulzivnět	k5eAaPmIp3nP	kompulzivnět
poruchy	porucha	k1gFnPc1	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
pracoval	pracovat	k5eAaImAgMnS	pracovat
u	u	k7c2	u
San	San	k1gFnSc2	San
Francisské	Francisský	k2eAgFnSc2d1	Francisský
policie	policie	k1gFnSc2	policie
jako	jako	k8xS	jako
detektiv	detektiv	k1gMnSc1	detektiv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
po	po	k7c6	po
bombovém	bombový	k2eAgInSc6d1	bombový
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
manželku	manželka	k1gFnSc4	manželka
Trudy	trud	k1gInPc1	trud
Monkovou	Monková	k1gFnSc7	Monková
se	se	k3xPyFc4	se
nervově	nervově	k6eAd1	nervově
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
odmítal	odmítat	k5eAaImAgMnS	odmítat
opustit	opustit	k5eAaPmF	opustit
svůj	svůj	k3xOyFgInSc4	svůj
dům	dům	k1gInSc4	dům
<g/>
;	;	kIx,	;
daří	dařit	k5eAaImIp3nS	dařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
to	ten	k3xDgNnSc1	ten
až	až	k9	až
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
soukromé	soukromý	k2eAgFnSc2d1	soukromá
ošetřovatelky	ošetřovatelka	k1gFnSc2	ošetřovatelka
Sharony	Sharon	k1gMnPc4	Sharon
Flemingové	Flemingový	k2eAgFnSc2d1	Flemingová
(	(	kIx(	(
<g/>
Bitty	Bitta	k1gFnSc2	Bitta
Schram	Schram	k1gInSc1	Schram
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgInS	začít
vykonávat	vykonávat	k5eAaImF	vykonávat
konzultační	konzultační	k2eAgFnSc4d1	konzultační
práci	práce	k1gFnSc4	práce
pro	pro	k7c4	pro
policii	policie	k1gFnSc4	policie
SF	SF	kA	SF
na	na	k7c6	na
zvláště	zvláště	k6eAd1	zvláště
těžkých	těžký	k2eAgInPc6d1	těžký
případech	případ	k1gInPc6	případ
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
Leland	Lelanda	k1gFnPc2	Lelanda
Stottlemeyer	Stottlemeyer	k1gMnSc1	Stottlemeyer
(	(	kIx(	(
<g/>
Ted	Ted	k1gMnSc1	Ted
Levine	Levin	k1gInSc5	Levin
<g/>
)	)	kIx)	)
a	a	k8xC	a
nadporučík	nadporučík	k1gMnSc1	nadporučík
Randall	Randalla	k1gFnPc2	Randalla
Disher	Dishra	k1gFnPc2	Dishra
(	(	kIx(	(
<g/>
Jason	Jason	k1gMnSc1	Jason
Gray-Stanford	Gray-Stanford	k1gMnSc1	Gray-Stanford
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
policisté	policista	k1gMnPc1	policista
na	na	k7c6	na
kriminálním	kriminální	k2eAgNnSc6d1	kriminální
oddělení	oddělení	k1gNnSc6	oddělení
místní	místní	k2eAgFnSc2d1	místní
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
najímají	najímat	k5eAaImIp3nP	najímat
konzultační	konzultační	k2eAgFnPc4d1	konzultační
služby	služba	k1gFnPc4	služba
Adriana	Adrian	k1gMnSc2	Adrian
Monka	Monek	k1gMnSc2	Monek
<g/>
.	.	kIx.	.
</s>
<s>
Stottlemeyer	Stottlemeyer	k1gInSc1	Stottlemeyer
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
rozčilený	rozčilený	k2eAgInSc4d1	rozčilený
kvůli	kvůli	k7c3	kvůli
Monkovým	Monkův	k2eAgFnPc3d1	Monkova
tendencím	tendence	k1gFnPc3	tendence
k	k	k7c3	k
přehnanému	přehnaný	k2eAgInSc3d1	přehnaný
pořádku	pořádek	k1gInSc3	pořádek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
obsesemi	obsese	k1gFnPc7	obsese
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
respektuje	respektovat	k5eAaImIp3nS	respektovat
svého	svůj	k3xOyFgMnSc4	svůj
přítele	přítel	k1gMnSc4	přítel
a	a	k8xC	a
bývalého	bývalý	k2eAgMnSc4d1	bývalý
kolegu	kolega	k1gMnSc4	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Monkova	Monkův	k2eAgFnSc1d1	Monkova
genialita	genialita	k1gFnSc1	genialita
tříbená	tříbený	k2eAgFnSc1d1	tříbená
i	i	k9	i
obsedantně	obsedantně	k6eAd1	obsedantně
kompulsivní	kompulsivní	k2eAgFnSc7d1	kompulsivní
poruchou	porucha	k1gFnSc7	porucha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
naplno	naplno	k6eAd1	naplno
propukla	propuknout	k5eAaPmAgFnS	propuknout
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
manželky	manželka	k1gFnSc2	manželka
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
v	v	k7c6	v
řešení	řešení	k1gNnSc6	řešení
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k6eAd1	Monk
si	se	k3xPyFc3	se
všímá	všímat	k5eAaImIp3nS	všímat
zdánlivě	zdánlivě	k6eAd1	zdánlivě
nepodstatných	podstatný	k2eNgInPc2d1	nepodstatný
detailů	detail	k1gInPc2	detail
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
řešit	řešit	k5eAaImF	řešit
případy	případ	k1gInPc4	případ
policie	policie	k1gFnSc2	policie
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
sérii	série	k1gFnSc6	série
se	se	k3xPyFc4	se
Monkova	Monkův	k2eAgFnSc1d1	Monkova
ošetřovatelka	ošetřovatelka	k1gFnSc1	ošetřovatelka
Sharona	Sharona	k1gFnSc1	Sharona
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
provdat	provdat	k5eAaPmF	provdat
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
bývalého	bývalý	k2eAgMnSc4d1	bývalý
manžela	manžel	k1gMnSc4	manžel
a	a	k8xC	a
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
do	do	k7c2	do
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
náhodném	náhodný	k2eAgNnSc6d1	náhodné
setkáním	setkání	k1gNnSc7	setkání
se	se	k3xPyFc4	se
ošetřovatelkou	ošetřovatelka	k1gFnSc7	ošetřovatelka
Monka	Monek	k1gInSc2	Monek
po	po	k7c4	po
dalších	další	k2eAgNnPc2d1	další
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
stane	stanout	k5eAaPmIp3nS	stanout
Natalií	Natalie	k1gFnSc7	Natalie
Teegerovou	Teegerův	k2eAgFnSc7d1	Teegerův
(	(	kIx(	(
<g/>
Traylor	Traylora	k1gFnPc2	Traylora
Howardová	Howardový	k2eAgFnSc1d1	Howardový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teegerová	Teegerový	k2eAgFnSc1d1	Teegerový
je	být	k5eAaImIp3nS	být
vdova	vdova	k1gFnSc1	vdova
po	po	k7c6	po
námořním	námořní	k2eAgMnSc6d1	námořní
letci	letec	k1gMnSc6	letec
(	(	kIx(	(
<g/>
Mitch	Mitch	k1gMnSc1	Mitch
Teeger	Teeger	k1gMnSc1	Teeger
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zahynul	zahynout	k5eAaPmAgMnS	zahynout
při	při	k7c6	při
bojích	boj	k1gInPc6	boj
v	v	k7c4	v
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dceru	dcera	k1gFnSc4	dcera
Julii	Julie	k1gFnSc4	Julie
(	(	kIx(	(
<g/>
v	v	k7c6	v
sedmé	sedmý	k4xOgFnSc6	sedmý
sérii	série	k1gFnSc6	série
je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
17	[number]	k4	17
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dalších	další	k2eAgFnPc2d1	další
sérií	série	k1gFnPc2	série
se	se	k3xPyFc4	se
Adrian	Adrian	k1gMnSc1	Adrian
Monk	Monk	k1gMnSc1	Monk
snaží	snažit	k5eAaImIp3nS	snažit
zjistit	zjistit	k5eAaPmF	zjistit
podrobnosti	podrobnost	k1gFnPc4	podrobnost
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
<g/>
;	;	kIx,	;
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
až	až	k9	až
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
díle	dílo	k1gNnSc6	dílo
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Adrian	Adrian	k1gMnSc1	Adrian
Monk	Monk	k1gMnSc1	Monk
(	(	kIx(	(
<g/>
Tony	Tony	k1gMnSc1	Tony
Shalhoub	Shalhoub	k1gMnSc1	Shalhoub
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
detektiv	detektiv	k1gMnSc1	detektiv
z	z	k7c2	z
oddělení	oddělení	k1gNnSc2	oddělení
vražd	vražda	k1gFnPc2	vražda
a	a	k8xC	a
soukromý	soukromý	k2eAgMnSc1d1	soukromý
konzultant	konzultant	k1gMnSc1	konzultant
San	San	k1gFnSc2	San
Francisské	Francisský	k2eAgFnSc2d1	Francisský
policie	policie	k1gFnSc2	policie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
Police	police	k1gFnSc2	police
Department	department	k1gInSc1	department
<g/>
,	,	kIx,	,
SFPD	SFPD	kA	SFPD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trpí	trpět	k5eAaImIp3nS	trpět
výraznou	výrazný	k2eAgFnSc7d1	výrazná
formou	forma	k1gFnSc7	forma
OCD	OCD	kA	OCD
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
svými	svůj	k3xOyFgFnPc7	svůj
obavami	obava	k1gFnPc7	obava
a	a	k8xC	a
fobiemi	fobie	k1gFnPc7	fobie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
pouze	pouze	k6eAd1	pouze
fobií	fobie	k1gFnPc2	fobie
z	z	k7c2	z
výšek	výška	k1gFnPc2	výška
<g/>
,	,	kIx,	,
hadů	had	k1gMnPc2	had
<g/>
,	,	kIx,	,
skupin	skupina	k1gFnPc2	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
rodeí	rodeo	k1gNnPc2	rodeo
a	a	k8xC	a
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Trudy	trud	k1gInPc4	trud
Monková	Monková	k1gFnSc1	Monková
byla	být	k5eAaImAgFnS	být
zavražděna	zavraždit	k5eAaPmNgFnS	zavraždit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
a	a	k8xC	a
od	od	k7c2	od
to	ten	k3xDgNnSc1	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
Adrian	Adrian	k1gMnSc1	Adrian
Monk	Monk	k1gMnSc1	Monk
trápí	trápit	k5eAaImIp3nS	trápit
její	její	k3xOp3gFnSc7	její
smrtí	smrt	k1gFnSc7	smrt
a	a	k8xC	a
faktem	fakt	k1gInSc7	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyla	být	k5eNaImAgFnS	být
vyřešena	vyřešen	k2eAgFnSc1d1	vyřešena
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
posledního	poslední	k2eAgInSc2d1	poslední
dílu	díl	k1gInSc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Sharona	Sharona	k1gFnSc1	Sharona
Flemingová	Flemingový	k2eAgFnSc1d1	Flemingová
(	(	kIx(	(
<g/>
Bitty	Bitta	k1gFnPc1	Bitta
Schramová	Schramový	k2eAgFnSc1d1	Schramová
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ošetřovatelka	ošetřovatelka	k1gFnSc1	ošetřovatelka
pana	pan	k1gMnSc2	pan
Monka	Monek	k1gMnSc2	Monek
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
zmiňována	zmiňovat	k5eAaImNgFnS	zmiňovat
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
ošetřovatelka	ošetřovatelka	k1gFnSc1	ošetřovatelka
<g/>
.	.	kIx.	.
</s>
<s>
Odmítala	odmítat	k5eAaImAgFnS	odmítat
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
starat	starat	k5eAaImF	starat
jako	jako	k9	jako
o	o	k7c4	o
malé	malý	k2eAgNnSc4d1	malé
dítě	dítě	k1gNnSc4	dítě
a	a	k8xC	a
nutila	nutit	k5eAaImAgFnS	nutit
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dělal	dělat	k5eAaImAgMnS	dělat
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
mu	on	k3xPp3gNnSc3	on
nepříjemné	příjemný	k2eNgInPc1d1	nepříjemný
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
poslední	poslední	k2eAgNnSc4d1	poslední
vystoupení	vystoupení	k1gNnSc4	vystoupení
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
v	v	k7c6	v
roli	role	k1gFnSc6	role
Sharony	Sharon	k1gInPc4	Sharon
Flemingové	Flemingový	k2eAgNnSc4d1	Flemingové
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Monk	Monk	k1gMnSc1	Monk
začal	začít	k5eAaPmAgMnS	začít
brát	brát	k5eAaImF	brát
léky	lék	k1gInPc4	lék
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k6eAd1	Monk
Takes	Takes	k1gInSc1	Takes
His	his	k1gNnSc2	his
Medicine	Medicin	k1gMnSc5	Medicin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
3.09	[number]	k4	3.09
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
seriálu	seriál	k1gInSc2	seriál
se	se	k3xPyFc4	se
odstěhovala	odstěhovat	k5eAaPmAgFnS	odstěhovat
do	do	k7c2	do
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
řadě	řada	k1gFnSc6	řada
seriálu	seriál	k1gInSc2	seriál
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Monk	Monk	k1gMnSc1	Monk
a	a	k8xC	a
Sharona	Sharona	k1gFnSc1	Sharona
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k6eAd1	Monk
and	and	k?	and
Sharona	Sharona	k1gFnSc1	Sharona
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
8.10	[number]	k4	8.10
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Monk	Monk	k1gMnSc1	Monk
a	a	k8xC	a
konec	konec	k1gInSc1	konec
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k1gInSc1	Monk
and	and	k?	and
the	the	k?	the
End	End	k1gFnSc1	End
-	-	kIx~	-
Part	part	k1gInSc1	part
II	II	kA	II
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
8.16	[number]	k4	8.16
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
odhaleno	odhalit	k5eAaPmNgNnS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sharona	Sharona	k1gFnSc1	Sharona
a	a	k8xC	a
Randy	rand	k1gInPc1	rand
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
stěhují	stěhovat	k5eAaImIp3nP	stěhovat
do	do	k7c2	do
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
Natalie	Natalie	k1gFnSc1	Natalie
Teegerová	Teegerová	k1gFnSc1	Teegerová
(	(	kIx(	(
<g/>
Traylor	Traylor	k1gInSc1	Traylor
Howardová	Howardová	k1gFnSc1	Howardová
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc4	druhý
a	a	k8xC	a
poslední	poslední	k2eAgFnSc7d1	poslední
ošetřovatelkou	ošetřovatelka	k1gFnSc7	ošetřovatelka
pana	pan	k1gMnSc2	pan
Monka	Monek	k1gMnSc2	Monek
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
uctivá	uctivý	k2eAgFnSc1d1	uctivá
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
šéfovi	šéf	k1gMnSc3	šéf
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
Sharona	Sharona	k1gFnSc1	Sharona
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
nezdráhá	zdráhat	k5eNaImIp3nS	zdráhat
říci	říct	k5eAaPmF	říct
panu	pan	k1gMnSc3	pan
Monkovi	Monek	k1gMnSc3	Monek
<g/>
,	,	kIx,	,
když	když	k8xS	když
jeho	jeho	k3xOp3gFnPc1	jeho
výstřednosti	výstřednost	k1gFnPc1	výstřednost
zajdou	zajít	k5eAaPmIp3nP	zajít
příliš	příliš	k6eAd1	příliš
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mladou	mladý	k2eAgFnSc7d1	mladá
vdovou	vdova	k1gFnSc7	vdova
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
žije	žít	k5eAaImIp3nS	žít
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
dcerou	dcera	k1gFnSc7	dcera
Julií	Julie	k1gFnSc7	Julie
Teegerovou	Teegerová	k1gFnSc7	Teegerová
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Natalie	Natalie	k1gFnSc2	Natalie
Mitch	Mitch	k1gMnSc1	Mitch
Teeger	Teeger	k1gMnSc1	Teeger
zahynul	zahynout	k5eAaPmAgMnS	zahynout
po	po	k7c6	po
sestřelení	sestřelení	k1gNnSc6	sestřelení
letadla	letadlo	k1gNnSc2	letadlo
v	v	k7c4	v
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Monk	Monk	k1gMnSc1	Monk
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
falešné	falešný	k2eAgFnSc6d1	falešná
stopě	stopa	k1gFnSc6	stopa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k1gInSc1	Monk
and	and	k?	and
the	the	k?	the
Red	Red	k1gFnSc1	Red
Herring	Herring	k1gInSc1	Herring
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
3.10	[number]	k4	3.10
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Natalie	Natalie	k1gFnSc1	Natalie
byla	být	k5eAaImAgFnS	být
představena	představit	k5eAaPmNgFnS	představit
během	během	k7c2	během
třetí	třetí	k4xOgFnSc2	třetí
řady	řada	k1gFnSc2	řada
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Bitty	Bitta	k1gFnSc2	Bitta
Schramová	Schramový	k2eAgFnSc1d1	Schramová
(	(	kIx(	(
<g/>
Sharona	Sharona	k1gFnSc1	Sharona
Flemingová	Flemingový	k2eAgFnSc1d1	Flemingová
<g/>
)	)	kIx)	)
ukvapeně	ukvapeně	k6eAd1	ukvapeně
odešla	odejít	k5eAaPmAgFnS	odejít
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
problému	problém	k1gInSc2	problém
se	s	k7c7	s
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Traylor	Traylor	k1gMnSc1	Traylor
Howardová	Howardová	k1gFnSc1	Howardová
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nikdy	nikdy	k6eAd1	nikdy
neviděla	vidět	k5eNaImAgFnS	vidět
seriál	seriál	k1gInSc4	seriál
"	"	kIx"	"
<g/>
Můj	můj	k3xOp1gMnSc1	můj
přítel	přítel	k1gMnSc1	přítel
Monk	Monk	k1gMnSc1	Monk
<g/>
"	"	kIx"	"
a	a	k8xC	a
nebyla	být	k5eNaImAgFnS	být
nadšená	nadšený	k2eAgFnSc1d1	nadšená
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gMnSc1	její
manažer	manažer	k1gMnSc1	manažer
ji	on	k3xPp3gFnSc4	on
tlačil	tlačit	k5eAaImAgMnS	tlačit
do	do	k7c2	do
role	role	k1gFnSc2	role
náhrady	náhrada	k1gFnSc2	náhrada
za	za	k7c4	za
Bitty	Bitta	k1gFnPc4	Bitta
Schramovou	Schramový	k2eAgFnSc4d1	Schramová
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
zkusila	zkusit	k5eAaPmAgFnS	zkusit
vzít	vzít	k5eAaPmF	vzít
tuto	tento	k3xDgFnSc4	tento
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
"	"	kIx"	"
<g/>
chladnému	chladný	k2eAgMnSc3d1	chladný
<g/>
"	"	kIx"	"
přijetí	přijetí	k1gNnSc3	přijetí
fanoušky	fanoušek	k1gMnPc7	fanoušek
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
spolutvůrce	spolutvůrce	k1gMnSc1	spolutvůrce
seriálu	seriál	k1gInSc2	seriál
Andy	Anda	k1gFnSc2	Anda
Breckman	Breckman	k1gMnSc1	Breckman
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Traylor	Traylor	k1gInSc1	Traylor
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
naplní	naplnit	k5eAaPmIp3nS	naplnit
jeho	jeho	k3xOp3gFnPc4	jeho
představy	představa	k1gFnPc4	představa
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vždy	vždy	k6eAd1	vždy
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgInS	být
Traylor	Traylor	k1gInSc4	Traylor
vděčný	vděčný	k2eAgMnSc1d1	vděčný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přišla	přijít	k5eAaPmAgFnS	přijít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
seriál	seriál	k1gInSc1	seriál
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
krizi	krize	k1gFnSc6	krize
a	a	k8xC	a
zachránila	zachránit	k5eAaPmAgFnS	zachránit
naše	náš	k3xOp1gNnSc4	náš
dítě	dítě	k1gNnSc4	dítě
[	[	kIx(	[
<g/>
....	....	k?	....
<g/>
]	]	kIx)	]
Museli	muset	k5eAaImAgMnP	muset
jsme	být	k5eAaImIp1nP	být
udělat	udělat	k5eAaPmF	udělat
rychlou	rychlý	k2eAgFnSc4d1	rychlá
změnu	změna	k1gFnSc4	změna
a	a	k8xC	a
zdaleka	zdaleka	k6eAd1	zdaleka
ne	ne	k9	ne
každý	každý	k3xTgInSc1	každý
seriál	seriál	k1gInSc1	seriál
takovou	takový	k3xDgFnSc4	takový
změnu	změna	k1gFnSc4	změna
přežije	přežít	k5eAaPmIp3nS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
byl	být	k5eAaImAgMnS	být
vyděšený	vyděšený	k2eAgMnSc1d1	vyděšený
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kapitán	kapitán	k1gMnSc1	kapitán
Leland	Lelanda	k1gFnPc2	Lelanda
Stottlemeyer	Stottlemeyer	k1gMnSc1	Stottlemeyer
(	(	kIx(	(
<g/>
Ted	Ted	k1gMnSc1	Ted
Levine	Levin	k1gMnSc5	Levin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vedoucím	vedoucí	k1gMnSc7	vedoucí
oddělení	oddělení	k1gNnSc2	oddělení
vražd	vražda	k1gFnPc2	vražda
San	San	k1gFnSc2	San
Francisské	Francisský	k2eAgFnSc2d1	Francisský
policie	policie	k1gFnSc2	policie
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
a	a	k8xC	a
pan	pan	k1gMnSc1	pan
Monk	Monk	k1gMnSc1	Monk
byli	být	k5eAaImAgMnP	být
dobrými	dobrý	k2eAgMnPc7d1	dobrý
přáteli	přítel	k1gMnPc7	přítel
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Monk	Monk	k1gInSc1	Monk
stal	stát	k5eAaPmAgInS	stát
policistou	policista	k1gMnSc7	policista
a	a	k8xC	a
přátelství	přátelství	k1gNnSc1	přátelství
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
i	i	k9	i
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Dělá	dělat	k5eAaImIp3nS	dělat
to	ten	k3xDgNnSc1	ten
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
život	život	k1gInSc1	život
pana	pan	k1gMnSc2	pan
Monka	Monek	k1gMnSc2	Monek
usnadnil	usnadnit	k5eAaPmAgInS	usnadnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
občas	občas	k6eAd1	občas
ho	on	k3xPp3gNnSc4	on
fobie	fobie	k1gFnPc1	fobie
pana	pan	k1gMnSc2	pan
Monka	Monek	k1gMnSc2	Monek
a	a	k8xC	a
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	on	k3xPp3gInPc4	on
provází	provázet	k5eAaImIp3nS	provázet
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
rozčilují	rozčilovat	k5eAaImIp3nP	rozčilovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgFnPc6	první
dvou	dva	k4xCgFnPc6	dva
řadách	řada	k1gFnPc6	řada
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
Adrianem	Adrian	k1gMnSc7	Adrian
Monkem	Monek	k1gMnSc7	Monek
velmi	velmi	k6eAd1	velmi
neochotně	ochotně	k6eNd1	ochotně
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
obavy	obava	k1gFnPc4	obava
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
své	svůj	k3xOyFgInPc4	svůj
případy	případ	k1gInPc4	případ
nevyřeší	vyřešit	k5eNaPmIp3nS	vyřešit
vždy	vždy	k6eAd1	vždy
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
třetí	třetí	k4xOgFnSc2	třetí
řady	řada	k1gFnSc2	řada
seriálu	seriál	k1gInSc2	seriál
víra	víra	k1gFnSc1	víra
v	v	k7c4	v
Monkovy	Monkův	k2eAgFnPc4d1	Monkova
schopnosti	schopnost	k1gFnPc4	schopnost
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
pevnější	pevný	k2eAgFnSc1d2	pevnější
a	a	k8xC	a
spolupráce	spolupráce	k1gFnSc1	spolupráce
mezi	mezi	k7c7	mezi
Monkem	Monko	k1gNnSc7	Monko
a	a	k8xC	a
Stottlemeyerem	Stottlemeyero	k1gNnSc7	Stottlemeyero
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nepochybná	pochybný	k2eNgFnSc1d1	nepochybná
<g/>
.	.	kIx.	.
</s>
<s>
Poručík	poručík	k1gMnSc1	poručík
Randy	rand	k1gInPc1	rand
Disher	Dishra	k1gFnPc2	Dishra
(	(	kIx(	(
<g/>
Jason	Jason	k1gMnSc1	Jason
Gray-Stanford	Gray-Stanford	k1gMnSc1	Gray-Stanford
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
poručíkem	poručík	k1gMnSc7	poručík
oddělení	oddělení	k1gNnSc2	oddělení
vražd	vražda	k1gFnPc2	vražda
SFPD	SFPD	kA	SFPD
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
naivní	naivní	k2eAgMnSc1d1	naivní
a	a	k8xC	a
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
vyobrazen	vyobrazen	k2eAgInSc1d1	vyobrazen
jako	jako	k8xS	jako
trošku	trošku	k6eAd1	trošku
poťouchlý	poťouchlý	k2eAgMnSc1d1	poťouchlý
policista	policista	k1gMnSc1	policista
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
postavy	postava	k1gFnPc1	postava
jsou	být	k5eAaImIp3nP	být
jím	on	k3xPp3gInSc7	on
často	často	k6eAd1	často
iritovány	iritován	k2eAgInPc1d1	iritován
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
často	často	k6eAd1	často
dělají	dělat	k5eAaImIp3nP	dělat
starosti	starost	k1gFnPc4	starost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmé	osmý	k4xOgFnSc6	osmý
řadě	řada	k1gFnSc6	řada
seriálu	seriál	k1gInSc2	seriál
bylo	být	k5eAaImAgNnS	být
ukázáno	ukázat	k5eAaPmNgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
políbil	políbit	k5eAaPmAgMnS	políbit
Sharonu	Sharon	k1gInSc2	Sharon
Flemingovou	Flemingový	k2eAgFnSc7d1	Flemingová
a	a	k8xC	a
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
díle	dílo	k1gNnSc6	dílo
seriálu	seriál	k1gInSc2	seriál
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
do	do	k7c2	do
Summitu	summit	k1gInSc2	summit
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
získal	získat	k5eAaPmAgInS	získat
pozici	pozice	k1gFnSc4	pozice
policejního	policejní	k2eAgMnSc2d1	policejní
náčelníka	náčelník	k1gMnSc2	náčelník
<g/>
.	.	kIx.	.
</s>
<s>
Julie	Julie	k1gFnSc1	Julie
Teegerová	Teegerová	k1gFnSc1	Teegerová
(	(	kIx(	(
<g/>
Emmy	Emma	k1gFnSc2	Emma
Clarkeová	Clarkeový	k2eAgFnSc1d1	Clarkeový
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dcera	dcera	k1gFnSc1	dcera
Natalie	Natalie	k1gFnSc1	Natalie
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Monk	Monk	k1gMnSc1	Monk
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
falešné	falešný	k2eAgFnSc6d1	falešná
stopě	stopa	k1gFnSc6	stopa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k1gInSc1	Monk
and	and	k?	and
the	the	k?	the
Red	Red	k1gFnSc1	Red
Herring	Herring	k1gInSc1	Herring
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
3.10	[number]	k4	3.10
<g/>
)	)	kIx)	)
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Monk	Monk	k1gMnSc1	Monk
a	a	k8xC	a
konec	konec	k1gInSc1	konec
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k1gInSc1	Monk
and	and	k?	and
the	the	k?	the
End	End	k1gFnSc1	End
-	-	kIx~	-
Part	part	k1gInSc1	part
I	I	kA	I
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
8.15	[number]	k4	8.15
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
účinkování	účinkování	k1gNnSc6	účinkování
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
se	se	k3xPyFc4	se
Julie	Julie	k1gFnSc1	Julie
chystala	chystat	k5eAaImAgFnS	chystat
jít	jít	k5eAaImF	jít
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
studovat	studovat	k5eAaImF	studovat
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Charles	Charles	k1gMnSc1	Charles
Kroger	Kroger	k1gMnSc1	Kroger
(	(	kIx(	(
<g/>
Stanley	Stanlea	k1gMnSc2	Stanlea
Kamel	Kamel	k1gMnSc1	Kamel
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
během	během	k7c2	během
prvních	první	k4xOgMnPc2	první
šesti	šest	k4xCc2	šest
řad	řada	k1gFnPc2	řada
seriálu	seriál	k1gInSc2	seriál
psychiatr	psychiatr	k1gMnSc1	psychiatr
pana	pan	k1gMnSc2	pan
Monka	Monek	k1gMnSc2	Monek
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Monk	Monk	k1gMnSc1	Monk
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
mistrovské	mistrovský	k2eAgNnSc1d1	mistrovské
dílo	dílo	k1gNnSc1	dílo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k6eAd1	Monk
Paints	Paints	k1gInSc1	Paints
His	his	k1gNnSc2	his
Masterpiece	Masterpieec	k1gInSc2	Masterpieec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
6.14	[number]	k4	6.14
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmé	sedmý	k4xOgFnSc6	sedmý
řadě	řada	k1gFnSc6	řada
seriálu	seriál	k1gInSc2	seriál
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Kroger	Kroger	k1gMnSc1	Kroger
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
srdeční	srdeční	k2eAgInSc4d1	srdeční
infarkt	infarkt	k1gInSc4	infarkt
(	(	kIx(	(
<g/>
zatímco	zatímco	k8xS	zatímco
Stanley	Stanlea	k1gFnPc1	Stanlea
Kamel	Kamela	k1gFnPc2	Kamela
opravdu	opravdu	k6eAd1	opravdu
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
srdeční	srdeční	k2eAgInSc4d1	srdeční
infarkt	infarkt	k1gInSc4	infarkt
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
během	během	k7c2	během
přestávky	přestávka	k1gFnSc2	přestávka
v	v	k7c6	v
natáčení	natáčení	k1gNnSc6	natáčení
mezi	mezi	k7c7	mezi
šestou	šestý	k4xOgFnSc7	šestý
a	a	k8xC	a
sedmou	sedmý	k4xOgFnSc7	sedmý
řadou	řada	k1gFnSc7	řada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Neven	Nevena	k1gFnPc2	Nevena
Bell	bell	k1gInSc1	bell
(	(	kIx(	(
<g/>
Héctor	Héctor	k1gInSc1	Héctor
Elizondo	Elizondo	k1gNnSc1	Elizondo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgMnSc7	druhý
psychiatrem	psychiatr	k1gMnSc7	psychiatr
pana	pan	k1gMnSc2	pan
Monka	Monek	k1gMnSc2	Monek
<g/>
,	,	kIx,	,
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Monk	Monk	k1gMnSc1	Monk
kupuje	kupovat	k5eAaImIp3nS	kupovat
dům	dům	k1gInSc4	dům
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k6eAd1	Monk
Buys	Buys	k1gInSc1	Buys
A	a	k8xC	a
House	house	k1gNnSc1	house
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
7.01	[number]	k4	7.01
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
objevil	objevit	k5eAaPmAgMnS	objevit
jako	jako	k9	jako
náhrada	náhrada	k1gFnSc1	náhrada
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Krogera	Kroger	k1gMnSc2	Kroger
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
úmrtí	úmrtí	k1gNnSc2	úmrtí
jeho	on	k3xPp3gMnSc2	on
představitele	představitel	k1gMnSc2	představitel
<g/>
,	,	kIx,	,
Stanleyho	Stanley	k1gMnSc2	Stanley
Kamela	Kamel	k1gMnSc2	Kamel
<g/>
.	.	kIx.	.
</s>
<s>
Trudy	trud	k1gInPc1	trud
Monková	Monková	k1gFnSc1	Monková
(	(	kIx(	(
<g/>
Stellina	Stellin	k2eAgFnSc1d1	Stellina
Rusichová	Rusichová	k1gFnSc1	Rusichová
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
a	a	k8xC	a
Melora	Melora	k1gFnSc1	Melora
Hardinová	Hardinový	k2eAgFnSc1d1	Hardinová
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
řady	řada	k1gFnSc2	řada
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zemřelá	zemřelý	k2eAgFnSc1d1	zemřelá
manželka	manželka	k1gFnSc1	manželka
pana	pan	k1gMnSc2	pan
Monka	Monek	k1gMnSc2	Monek
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
vyřešit	vyřešit	k5eAaPmF	vyřešit
případ	případ	k1gInSc4	případ
úmrtí	úmrtí	k1gNnSc2	úmrtí
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
od	od	k7c2	od
první	první	k4xOgFnSc2	první
epizody	epizoda	k1gFnSc2	epizoda
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
děj	děj	k1gInSc1	děj
provází	provázet	k5eAaImIp3nS	provázet
seriálem	seriál	k1gInSc7	seriál
až	až	k9	až
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
epizody	epizoda	k1gFnSc2	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Kevin	Kevin	k1gMnSc1	Kevin
Dorfman	Dorfman	k1gMnSc1	Dorfman
(	(	kIx(	(
<g/>
Jarrad	Jarrad	k1gInSc1	Jarrad
Paul	Paul	k1gMnSc1	Paul
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
účetní	účetní	k1gMnSc1	účetní
a	a	k8xC	a
hlučný	hlučný	k2eAgMnSc1d1	hlučný
a	a	k8xC	a
upovídaný	upovídaný	k2eAgMnSc1d1	upovídaný
soused	soused	k1gMnSc1	soused
pana	pan	k1gMnSc2	pan
Monka	Monek	k1gMnSc2	Monek
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Monk	Monk	k1gMnSc1	Monk
a	a	k8xC	a
kluk	kluk	k1gMnSc1	kluk
s	s	k7c7	s
novinami	novina	k1gFnPc7	novina
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k1gInSc1	Monk
and	and	k?	and
the	the	k?	the
Paperboy	Paperboa	k1gFnSc2	Paperboa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
2.10	[number]	k4	2.10
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmé	sedmý	k4xOgFnSc6	sedmý
řadě	řada	k1gFnSc6	řada
seriálu	seriál	k1gInSc2	seriál
byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgInS	zavraždit
iluzionistou	iluzionista	k1gMnSc7	iluzionista
Karlem	Karel	k1gMnSc7	Karel
Torinim	Torinim	k1gInSc4	Torinim
<g/>
,	,	kIx,	,
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Monk	Monk	k1gMnSc1	Monk
a	a	k8xC	a
kouzelník	kouzelník	k1gMnSc1	kouzelník
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k1gInSc1	Monk
and	and	k?	and
the	the	k?	the
Magician	Magician	k1gInSc1	Magician
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
7.15	[number]	k4	7.15
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Harold	Harold	k1gMnSc1	Harold
Krenshaw	Krenshaw	k1gMnSc1	Krenshaw
(	(	kIx(	(
<g/>
Tim	Tim	k?	Tim
Bagley	Baglea	k1gFnSc2	Baglea
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rival	rival	k1gMnSc1	rival
pana	pan	k1gMnSc4	pan
Monka	Monek	k1gMnSc4	Monek
téměř	téměř	k6eAd1	téměř
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
trvání	trvání	k1gNnSc2	trvání
seriálu	seriál	k1gInSc2	seriál
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
pacient	pacient	k1gMnSc1	pacient
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Krogera	Kroger	k1gMnSc2	Kroger
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Monk	Monka	k1gFnPc2	Monka
mají	mít	k5eAaImIp3nP	mít
neustále	neustále	k6eAd1	neustále
rozpory	rozpor	k1gInPc4	rozpor
<g/>
,	,	kIx,	,
primárně	primárně	k6eAd1	primárně
způsobené	způsobený	k2eAgFnPc1d1	způsobená
jejich	jejich	k3xOp3gFnPc7	jejich
nekompatibilními	kompatibilní	k2eNgFnPc7d1	nekompatibilní
obsesemi	obsese	k1gFnPc7	obsese
<g/>
.	.	kIx.	.
</s>
<s>
Krenshaw	Krenshaw	k?	Krenshaw
se	se	k3xPyFc4	se
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Monk	Monk	k1gMnSc1	Monk
a	a	k8xC	a
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dělala	dělat	k5eAaImAgFnS	dělat
zbytečný	zbytečný	k2eAgInSc4d1	zbytečný
poplach	poplach	k1gInSc4	poplach
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k1gInSc1	Monk
and	and	k?	and
the	the	k?	the
Girl	girl	k1gFnSc2	girl
Who	Who	k1gMnSc1	Who
Cried	Cried	k1gMnSc1	Cried
Wolf	Wolf	k1gMnSc1	Wolf
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
3.06	[number]	k4	3.06
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
pan	pan	k1gMnSc1	pan
Monk	Monka	k1gFnPc2	Monka
dohadovali	dohadovat	k5eAaImAgMnP	dohadovat
o	o	k7c4	o
uspořádání	uspořádání	k1gNnSc4	uspořádání
časopisů	časopis	k1gInPc2	časopis
v	v	k7c6	v
čekárně	čekárna	k1gFnSc6	čekárna
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Krogera	Kroger	k1gMnSc2	Kroger
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Monk	Monk	k1gMnSc1	Monk
a	a	k8xC	a
malý	malý	k2eAgMnSc1d1	malý
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k1gInSc1	Monk
and	and	k?	and
the	the	k?	the
Election	Election	k1gInSc1	Election
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
3.15	[number]	k4	3.15
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Krenshaw	Krenshaw	k1gMnSc1	Krenshaw
protivníkem	protivník	k1gMnSc7	protivník
Natalie	Natalie	k1gFnSc2	Natalie
Teegerové	Teegerová	k1gFnSc2	Teegerová
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
školské	školský	k2eAgFnSc2d1	školská
rady	rada	k1gFnSc2	rada
Juliiny	Juliin	k2eAgFnSc2d1	Juliina
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
epizody	epizoda	k1gFnSc2	epizoda
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Monk	Monk	k1gMnSc1	Monk
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k1gInSc1	Monk
and	and	k?	and
the	the	k?	the
Actor	Actor	k1gInSc1	Actor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
5.01	[number]	k4	5.01
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
důrazně	důrazně	k6eAd1	důrazně
zmíněn	zmíněn	k2eAgInSc1d1	zmíněn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Monk	Monk	k1gMnSc1	Monk
na	na	k7c6	na
kolejním	kolejní	k2eAgInSc6d1	kolejní
srazu	sraz	k1gInSc6	sraz
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k6eAd1	Monk
Gets	Gets	k1gInSc1	Gets
a	a	k8xC	a
New	New	k1gFnSc1	New
Shrink	Shrink	k1gInSc1	Shrink
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
5.07	[number]	k4	5.07
<g/>
)	)	kIx)	)
hraje	hrát	k5eAaImIp3nS	hrát
velmi	velmi	k6eAd1	velmi
kritickou	kritický	k2eAgFnSc4d1	kritická
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Neobvyklou	obvyklý	k2eNgFnSc4d1	neobvyklá
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Monk	Monk	k1gMnSc1	Monk
a	a	k8xC	a
odvážlivec	odvážlivec	k1gMnSc1	odvážlivec
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k1gInSc1	Monk
and	and	k?	and
the	the	k?	the
Daredevil	Daredevil	k1gFnSc2	Daredevil
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
6.07	[number]	k4	6.07
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
detaily	detail	k1gInPc4	detail
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
minulosti	minulost	k1gFnSc6	minulost
a	a	k8xC	a
také	také	k9	také
představuje	představovat	k5eAaImIp3nS	představovat
svoji	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Krogera	Kroger	k1gMnSc2	Kroger
se	se	k3xPyFc4	se
Harold	Harold	k1gMnSc1	Harold
Krenshaw	Krenshaw	k1gMnSc1	Krenshaw
trvale	trvale	k6eAd1	trvale
snažil	snažit	k5eAaImAgMnS	snažit
zjistit	zjistit	k5eAaPmF	zjistit
identitu	identita	k1gFnSc4	identita
nového	nový	k2eAgMnSc2d1	nový
terapeuta	terapeut	k1gMnSc2	terapeut
pana	pan	k1gMnSc2	pan
Monka	Monek	k1gMnSc2	Monek
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
terapeuta	terapeut	k1gMnSc2	terapeut
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Bella	Bella	k1gMnSc1	Bella
odhalil	odhalit	k5eAaPmAgMnS	odhalit
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Monk	Monk	k1gMnSc1	Monk
bojuje	bojovat	k5eAaImIp3nS	bojovat
s	s	k7c7	s
radnicí	radnice	k1gFnSc7	radnice
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k1gInSc1	Monk
Fights	Fightsa	k1gFnPc2	Fightsa
City	city	k1gNnSc1	city
Hall	Hall	k1gMnSc1	Hall
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
7.16	[number]	k4	7.16
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Monk	Monk	k1gMnSc1	Monk
je	být	k5eAaImIp3nS	být
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k1gMnSc1	Monk
Is	Is	k1gMnSc1	Is
Someone	Someon	k1gInSc5	Someon
Else	Elsa	k1gFnSc6	Elsa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
8.04	[number]	k4	8.04
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
málem	málem	k6eAd1	málem
odhalil	odhalit	k5eAaPmAgMnS	odhalit
krytí	krytí	k1gNnSc4	krytí
pana	pan	k1gMnSc2	pan
Monka	Monek	k1gMnSc2	Monek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Monk	Monk	k1gMnSc1	Monk
na	na	k7c6	na
skupinové	skupinový	k2eAgFnSc6d1	skupinová
terapii	terapie	k1gFnSc6	terapie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k6eAd1	Monk
Goes	Goes	k1gInSc1	Goes
to	ten	k3xDgNnSc4	ten
Group	Group	k1gMnSc1	Group
Therapy	Therapa	k1gFnSc2	Therapa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
8.08	[number]	k4	8.08
se	se	k3xPyFc4	se
Harold	Harold	k1gMnSc1	Harold
a	a	k8xC	a
pan	pan	k1gMnSc1	pan
Monk	Monk	k1gMnSc1	Monk
stali	stát	k5eAaPmAgMnP	stát
přáteli	přítel	k1gMnPc7	přítel
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
společně	společně	k6eAd1	společně
bojovali	bojovat	k5eAaImAgMnP	bojovat
proti	proti	k7c3	proti
klaustrofobii	klaustrofobie	k1gFnSc3	klaustrofobie
<g/>
.	.	kIx.	.
</s>
<s>
Harold	Harold	k1gMnSc1	Harold
šlechetně	šlechetně	k6eAd1	šlechetně
odešel	odejít	k5eAaPmAgMnS	odejít
ze	z	k7c2	z
skupinové	skupinový	k2eAgFnSc2d1	skupinová
terapie	terapie	k1gFnSc2	terapie
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgMnS	umožnit
tak	tak	k6eAd1	tak
Adrianu	Adrian	k1gMnSc3	Adrian
Monkovi	Monek	k1gMnSc3	Monek
udržet	udržet	k5eAaPmF	udržet
si	se	k3xPyFc3	se
privátní	privátní	k2eAgNnPc4d1	privátní
terapistická	terapistický	k2eAgNnPc4d1	terapistický
sezení	sezení	k1gNnPc4	sezení
s	s	k7c7	s
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Bellem	bell	k1gInSc7	bell
<g/>
.	.	kIx.	.
</s>
<s>
Benjamin	Benjamin	k1gMnSc1	Benjamin
Fleming	Fleming	k1gInSc1	Fleming
(	(	kIx(	(
<g/>
Kane	kanout	k5eAaImIp3nS	kanout
Ritchotte	Ritchott	k1gInSc5	Ritchott
během	během	k7c2	během
pilotního	pilotní	k2eAgInSc2d1	pilotní
dílu	díl	k1gInSc2	díl
a	a	k8xC	a
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
sérii	série	k1gFnSc6	série
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
sérii	série	k1gFnSc6	série
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
Max	Max	k1gMnSc1	Max
Morrow	Morrow	k1gMnSc1	Morrow
-	-	kIx~	-
syn	syn	k1gMnSc1	syn
Sharony	Sharon	k1gMnPc7	Sharon
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
série	série	k1gFnPc4	série
<g/>
)	)	kIx)	)
Flemingové	Flemingový	k2eAgFnPc4d1	Flemingová
<g/>
.	.	kIx.	.
</s>
<s>
Trevor	Trevor	k1gMnSc1	Trevor
Howe	How	k1gFnSc2	How
(	(	kIx(	(
<g/>
Frank	Frank	k1gMnSc1	Frank
John	John	k1gMnSc1	John
Hughes	Hughes	k1gMnSc1	Hughes
<g/>
)	)	kIx)	)
-	-	kIx~	-
Sharonin	Sharonin	k2eAgMnSc1d1	Sharonin
bývalý	bývalý	k2eAgMnSc1d1	bývalý
manžel	manžel	k1gMnSc1	manžel
a	a	k8xC	a
Benjaminův	Benjaminův	k2eAgMnSc1d1	Benjaminův
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
znovu	znovu	k6eAd1	znovu
manžel	manžel	k1gMnSc1	manžel
Sharony	Sharon	k1gMnPc4	Sharon
Flemingové	Flemingový	k2eAgNnSc1d1	Flemingové
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
série	série	k1gFnSc2	série
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gail	Gaila	k1gFnPc2	Gaila
Flemingová	Flemingový	k2eAgFnSc1d1	Flemingová
(	(	kIx(	(
<g/>
Amy	Amy	k1gFnSc1	Amy
Sedaris	Sedaris	k1gFnSc1	Sedaris
<g/>
)	)	kIx)	)
-	-	kIx~	-
Sharonina	Sharonin	k2eAgFnSc1d1	Sharonin
mladší	mladý	k2eAgFnSc1d2	mladší
sestra	sestra	k1gFnSc1	sestra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
epizodách	epizoda	k1gFnPc6	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Karen	Karen	k1gInSc1	Karen
Stottlemeyerová	Stottlemeyerová	k1gFnSc1	Stottlemeyerová
(	(	kIx(	(
<g/>
Glenne	Glenn	k1gInSc5	Glenn
Headlyová	Headlyový	k2eAgNnPc1d1	Headlyový
<g/>
)	)	kIx)	)
-	-	kIx~	-
manželka	manželka	k1gFnSc1	manželka
Lelanda	Lelanda	k1gFnSc1	Lelanda
Stottlemeyera	Stottlemeyera	k1gFnSc1	Stottlemeyera
<g/>
,	,	kIx,	,
natáčela	natáčet	k5eAaImAgFnS	natáčet
pro	pro	k7c4	pro
televizi	televize	k1gFnSc4	televize
dokument	dokument	k1gInSc4	dokument
o	o	k7c4	o
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
policii	policie	k1gFnSc6	policie
<g/>
,	,	kIx,	,
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Pan	Pan	k1gMnSc1	Pan
Monk	Monk	k1gMnSc1	Monk
a	a	k8xC	a
kapitánovo	kapitánův	k2eAgNnSc1d1	kapitánovo
manželství	manželství	k1gNnSc1	manželství
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
rozvod	rozvod	k1gInSc4	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
Ambrose	Ambrosa	k1gFnSc3	Ambrosa
Monk	Monk	k1gMnSc1	Monk
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Turturro	Turturro	k1gNnSc4	Turturro
<g/>
)	)	kIx)	)
-	-	kIx~	-
bratr	bratr	k1gMnSc1	bratr
Monka	Monka	k1gMnSc1	Monka
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
také	také	k9	také
trpí	trpět	k5eAaImIp3nS	trpět
různými	různý	k2eAgFnPc7d1	různá
fóbiemi	fóbie	k1gFnPc7	fóbie
<g/>
,	,	kIx,	,
nejpozoruhodnější	pozoruhodný	k2eAgFnPc1d3	nejpozoruhodnější
agorafobie	agorafobie	k1gFnPc1	agorafobie
<g/>
.	.	kIx.	.
</s>
<s>
Ambrose	Ambrosa	k1gFnSc3	Ambrosa
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
téměř	téměř	k6eAd1	téměř
zemřel	zemřít	k5eAaPmAgMnS	zemřít
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
<g/>
.	.	kIx.	.
</s>
<s>
Jack	Jack	k1gMnSc1	Jack
Monk	Monk	k1gMnSc1	Monk
(	(	kIx(	(
<g/>
Dan	Dan	k1gMnSc1	Dan
Hedaya	Hedaya	k1gMnSc1	Hedaya
<g/>
)	)	kIx)	)
-	-	kIx~	-
otec	otec	k1gMnSc1	otec
Adriana	Adriana	k1gFnSc1	Adriana
a	a	k8xC	a
Ambrose	Ambrosa	k1gFnSc3	Ambrosa
a	a	k8xC	a
nevlastního	vlastní	k2eNgMnSc4d1	nevlastní
syna	syn	k1gMnSc4	syn
Jacka	Jacek	k1gMnSc4	Jacek
Jr	Jr	k1gMnSc4	Jr
<g/>
.	.	kIx.	.
</s>
<s>
Adrian	Adrian	k1gMnSc1	Adrian
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
setkává	setkávat	k5eAaImIp3nS	setkávat
po	po	k7c6	po
39	[number]	k4	39
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
děj	děj	k1gInSc1	děj
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
<g/>
,	,	kIx,	,
seriál	seriál	k1gInSc1	seriál
se	se	k3xPyFc4	se
natáčí	natáčet	k5eAaImIp3nS	natáčet
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Pilotní	pilotní	k2eAgInSc1d1	pilotní
díl	díl	k1gInSc1	díl
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
<g/>
,	,	kIx,	,
následující	následující	k2eAgFnPc4d1	následující
první	první	k4xOgFnPc4	první
série	série	k1gFnPc4	série
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
epizod	epizoda	k1gFnPc2	epizoda
2	[number]	k4	2
<g/>
.	.	kIx.	.
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
řady	řada	k1gFnSc2	řada
je	být	k5eAaImIp3nS	být
točena	točit	k5eAaImNgFnS	točit
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
také	také	k9	také
v	v	k7c6	v
kulisách	kulisa	k1gFnPc6	kulisa
Ren-Mar	Ren-Mar	k1gMnSc1	Ren-Mar
Studios	Studios	k?	Studios
(	(	kIx(	(
<g/>
Adrianův	Adrianův	k2eAgInSc1d1	Adrianův
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
Sharonin	Sharonin	k2eAgInSc1d1	Sharonin
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
kancelář	kancelář	k1gFnSc1	kancelář
policie	policie	k1gFnSc1	policie
<g/>
,	,	kIx,	,
kancelář	kancelář	k1gFnSc1	kancelář
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Krogera	Krogera	k1gFnSc1	Krogera
a	a	k8xC	a
dům	dům	k1gInSc1	dům
Natalie	Natalie	k1gFnSc2	Natalie
Teegerové	Teegerová	k1gFnSc2	Teegerová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
dílů	díl	k1gInPc2	díl
seriálu	seriál	k1gInSc2	seriál
Můj	můj	k3xOp1gMnSc1	můj
přítel	přítel	k1gMnSc1	přítel
Monk	Monk	k1gMnSc1	Monk
<g/>
.	.	kIx.	.
</s>
<s>
Soundtrack	soundtrack	k1gInSc1	soundtrack
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hudbu	hudba	k1gFnSc4	hudba
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
titulní	titulní	k2eAgFnSc4d1	titulní
skladbu	skladba	k1gFnSc4	skladba
nahrál	nahrát	k5eAaBmAgMnS	nahrát
a	a	k8xC	a
nazpíval	nazpívat	k5eAaBmAgMnS	nazpívat
Randy	rand	k1gInPc4	rand
Newman	Newman	k1gMnSc1	Newman
<g/>
.	.	kIx.	.
</s>
<s>
Podcast	Podcast	k1gFnSc1	Podcast
s	s	k7c7	s
informacemi	informace	k1gFnPc7	informace
ze	z	k7c2	z
zákulisí	zákulisí	k1gNnSc2	zákulisí
a	a	k8xC	a
od	od	k7c2	od
hlavních	hlavní	k2eAgMnPc2d1	hlavní
účinkujících	účinkující	k1gMnPc2	účinkující
je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
během	během	k7c2	během
vysílání	vysílání	k1gNnSc2	vysílání
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
Lee	Lea	k1gFnSc6	Lea
Goldberg	Goldberg	k1gInSc1	Goldberg
vydal	vydat	k5eAaPmAgInS	vydat
sérii	série	k1gFnSc4	série
románů	román	k1gInPc2	román
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
původním	původní	k2eAgInSc6d1	původní
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
romány	román	k1gInPc1	román
jsou	být	k5eAaImIp3nP	být
vyprávěny	vyprávět	k5eAaImNgInP	vyprávět
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
Natalie	Natalie	k1gFnSc2	Natalie
Teegerové	Teegerová	k1gFnSc2	Teegerová
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgFnSc2	druhý
asistentky	asistentka	k1gFnSc2	asistentka
pana	pan	k1gMnSc2	pan
Monky	Monka	k1gMnSc2	Monka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
valné	valný	k2eAgFnSc6d1	valná
většině	většina	k1gFnSc6	většina
děje	děj	k1gInSc2	děj
jsou	být	k5eAaImIp3nP	být
romány	román	k1gInPc1	román
věrné	věrný	k2eAgInPc1d1	věrný
televiznímu	televizní	k2eAgInSc3d1	televizní
seriálu	seriál	k1gInSc3	seriál
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
nepatrné	nepatrný	k2eAgInPc1d1	nepatrný
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
z	z	k7c2	z
románů	román	k1gInPc2	román
byly	být	k5eAaImAgInP	být
později	pozdě	k6eAd2	pozdě
adaptovány	adaptovat	k5eAaBmNgInP	adaptovat
v	v	k7c4	v
epizody	epizoda	k1gFnPc4	epizoda
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
poslední	poslední	k2eAgInSc1d1	poslední
román	román	k1gInSc1	román
napsaný	napsaný	k2eAgInSc1d1	napsaný
Lee	Lea	k1gFnSc6	Lea
Goldbergem	Goldberg	k1gInSc7	Goldberg
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
té	ten	k3xDgFnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
Goldberg	Goldberg	k1gMnSc1	Goldberg
přestal	přestat	k5eAaPmAgMnS	přestat
věnovat	věnovat	k5eAaPmF	věnovat
sérii	série	k1gFnSc4	série
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
další	další	k2eAgFnSc4d1	další
4	[number]	k4	4
knihy	kniha	k1gFnSc2	kniha
napsal	napsat	k5eAaBmAgMnS	napsat
Hy	hy	k0	hy
Conrad	Conrad	k1gInSc1	Conrad
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc1d1	poslední
díl	díl	k1gInSc1	díl
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k1gInSc1	Monk
and	and	k?	and
the	the	k?	the
New	New	k1gMnSc1	New
Lieutenant	Lieutenant	k1gMnSc1	Lieutenant
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Universal	Universat	k5eAaImAgInS	Universat
Studios	Studios	k?	Studios
Home	Home	k1gFnSc4	Home
Entertainment	Entertainment	k1gInSc4	Entertainment
vydalo	vydat	k5eAaPmAgNnS	vydat
všech	všecek	k3xTgFnPc2	všecek
osm	osm	k4xCc1	osm
řad	řada	k1gFnPc2	řada
seriálu	seriál	k1gInSc2	seriál
Můj	můj	k1gMnSc1	můj
přítel	přítel	k1gMnSc1	přítel
Monk	Monk	k1gMnSc1	Monk
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
v	v	k7c6	v
regionech	region	k1gInPc6	region
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
<g/>
,	,	kIx,	,
epizody	epizoda	k1gFnPc1	epizoda
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
až	až	k9	až
8	[number]	k4	8
<g/>
.	.	kIx.	.
řady	řada	k1gFnPc1	řada
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgFnPc1d1	dostupná
také	také	k9	také
na	na	k7c4	na
iTunes	iTunes	k1gInSc4	iTunes
<g/>
,	,	kIx,	,
včechny	včechen	k2eAgFnPc1d1	včechen
řady	řada	k1gFnPc1	řada
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgFnPc1d1	dostupná
i	i	k9	i
v	v	k7c6	v
HD	HD	kA	HD
kvalitě	kvalita	k1gFnSc6	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
role	role	k1gFnSc1	role
v	v	k7c6	v
komediálním	komediální	k2eAgInSc6d1	komediální
seriálu	seriál	k1gInSc6	seriál
<g/>
;	;	kIx,	;
Tony	Tony	k1gMnSc1	Tony
Shalhoub	Shalhoub	k1gMnSc1	Shalhoub
<g/>
;	;	kIx,	;
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Titulní	titulní	k2eAgFnSc1d1	titulní
hudba	hudba	k1gFnSc1	hudba
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
<g/>
;	;	kIx,	;
Jeff	Jeff	k1gMnSc1	Jeff
Beal	Beal	k1gMnSc1	Beal
<g/>
;	;	kIx,	;
2003	[number]	k4	2003
Vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
role	role	k1gFnSc2	role
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
<g/>
;	;	kIx,	;
John	John	k1gMnSc1	John
Turturro	Turturro	k1gNnSc4	Turturro
<g/>
;	;	kIx,	;
2004	[number]	k4	2004
Titulní	titulní	k2eAgFnSc1d1	titulní
hudba	hudba	k1gFnSc1	hudba
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
<g/>
;	;	kIx,	;
Randy	rand	k1gInPc1	rand
Newman	Newman	k1gMnSc1	Newman
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
2004	[number]	k4	2004
Vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
role	role	k1gFnPc4	role
v	v	k7c6	v
komediálním	komediální	k2eAgInSc6d1	komediální
seriálu	seriál	k1gInSc6	seriál
<g/>
;	;	kIx,	;
Stanley	Stanle	k2eAgFnPc1d1	Stanle
Tucci	Tucce	k1gFnSc3	Tucce
<g/>
;	;	kIx,	;
2007	[number]	k4	2007
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
herec	herec	k1gMnSc1	herec
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
-	-	kIx~	-
hudební	hudební	k2eAgMnSc1d1	hudební
nebo	nebo	k8xC	nebo
komediální	komediální	k2eAgMnSc1d1	komediální
<g/>
;	;	kIx,	;
Tony	Tony	k1gMnSc1	Tony
Shalhoub	Shalhoub	k1gMnSc1	Shalhoub
<g/>
;	;	kIx,	;
2003	[number]	k4	2003
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
<g/>
;	;	kIx,	;
Tony	Tony	k1gMnSc1	Tony
Shalhoub	Shalhoub	k1gMnSc1	Shalhoub
<g/>
;	;	kIx,	;
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
role	role	k1gFnPc1	role
v	v	k7c6	v
komediálním	komediální	k2eAgInSc6d1	komediální
seriálu	seriál	k1gInSc6	seriál
<g/>
;	;	kIx,	;
Tony	Tony	k1gMnSc1	Tony
Shalhoub	Shalhoub	k1gMnSc1	Shalhoub
<g/>
;	;	kIx,	;
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
;	;	kIx,	;
6	[number]	k4	6
nominací	nominace	k1gFnSc7	nominace
Režie	režie	k1gFnSc2	režie
komediálního	komediální	k2eAgInSc2d1	komediální
seriálu	seriál	k1gInSc2	seriál
<g/>
;	;	kIx,	;
za	za	k7c4	za
díl	díl	k1gInSc4	díl
"	"	kIx"	"
<g/>
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Monk	Monk	k6eAd1	Monk
Takes	Takes	k1gInSc1	Takes
His	his	k1gNnSc2	his
Medicine	Medicin	k1gInSc5	Medicin
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
Randall	Randall	k1gInSc1	Randall
Zisk	zisk	k1gInSc1	zisk
<g/>
;	;	kIx,	;
2005	[number]	k4	2005
Vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
role	role	k1gFnSc2	role
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
<g/>
;	;	kIx,	;
Laurie	Laurie	k1gFnSc1	Laurie
Metcalf	Metcalf	k1gMnSc1	Metcalf
<g/>
;	;	kIx,	;
2006	[number]	k4	2006
Casting	Casting	k1gInSc1	Casting
<g/>
;	;	kIx,	;
Anya	Anya	k1gMnSc1	Anya
Colloff	Colloff	k1gMnSc1	Colloff
<g/>
,	,	kIx,	,
Amy	Amy	k1gMnSc5	Amy
McIntyre	McIntyr	k1gMnSc5	McIntyr
Britt	Britt	k1gMnSc1	Britt
<g/>
,	,	kIx,	,
Meg	Meg	k1gMnSc1	Meg
Liberman	Liberman	k1gMnSc1	Liberman
<g/>
,	,	kIx,	,
Camille	Camille	k1gInSc1	Camille
H.	H.	kA	H.
Patton	Patton	k1gInSc1	Patton
<g/>
,	,	kIx,	,
Sandi	Sand	k1gMnPc1	Sand
Logan	Logan	k1gMnSc1	Logan
<g/>
,	,	kIx,	,
Lonnie	Lonnie	k1gFnSc1	Lonnie
<g />
.	.	kIx.	.
</s>
<s>
Hamerman	Hamerman	k1gMnSc1	Hamerman
<g/>
;	;	kIx,	;
2004	[number]	k4	2004
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
televizní	televizní	k2eAgInSc4d1	televizní
seriál	seriál	k1gInSc4	seriál
-	-	kIx~	-
hudební	hudební	k2eAgFnSc1d1	hudební
nebo	nebo	k8xC	nebo
komediální	komediální	k2eAgFnSc1d1	komediální
<g/>
;	;	kIx,	;
2004	[number]	k4	2004
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
herec	herec	k1gMnSc1	herec
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
-	-	kIx~	-
hudební	hudební	k2eAgMnSc1d1	hudební
nebo	nebo	k8xC	nebo
komediální	komediální	k2eAgMnSc1d1	komediální
<g/>
;	;	kIx,	;
Tony	Tony	k1gMnSc1	Tony
Shalhoub	Shalhoub	k1gMnSc1	Shalhoub
<g/>
;	;	kIx,	;
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
;	;	kIx,	;
5	[number]	k4	5
nominací	nominace	k1gFnPc2	nominace
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
herečka	herečka	k1gFnSc1	herečka
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
-	-	kIx~	-
hudební	hudební	k2eAgFnSc2d1	hudební
nebo	nebo	k8xC	nebo
komediální	komediální	k2eAgFnSc2d1	komediální
<g/>
;	;	kIx,	;
Bitty	Bitta	k1gFnSc2	Bitta
Schram	Schram	k1gInSc1	Schram
<g/>
;	;	kIx,	;
2004	[number]	k4	2004
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
<g/>
;	;	kIx,	;
Tony	Tony	k1gMnSc1	Tony
Shalhoub	Shalhoub	k1gMnSc1	Shalhoub
<g/>
;	;	kIx,	;
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
;	;	kIx,	;
5	[number]	k4	5
nominací	nominace	k1gFnPc2	nominace
</s>
