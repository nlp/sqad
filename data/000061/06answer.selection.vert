<s>
Anna	Anna	k1gFnSc1	Anna
Geislerová	Geislerová	k1gFnSc1	Geislerová
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
uváděna	uváděn	k2eAgFnSc1d1	uváděna
jako	jako	k8xC	jako
Aňa	Aňa	k1gFnSc1	Aňa
Geislerová	Geislerová	k1gFnSc1	Geislerová
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1976	[number]	k4	1976
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
držitelka	držitelka	k1gFnSc1	držitelka
pěti	pět	k4xCc2	pět
Českých	český	k2eAgMnPc2d1	český
lvů	lev	k1gMnPc2	lev
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
herečky	herečka	k1gFnSc2	herečka
Ester	Ester	k1gFnSc2	Ester
Geislerové	Geislerová	k1gFnSc2	Geislerová
a	a	k8xC	a
výtvarnice	výtvarnice	k1gFnSc2	výtvarnice
a	a	k8xC	a
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Lenky	Lenka	k1gFnSc2	Lenka
Geislerové	Geislerová	k1gFnSc2	Geislerová
<g/>
.	.	kIx.	.
</s>
