<s>
Anna	Anna	k1gFnSc1	Anna
Geislerová	Geislerová	k1gFnSc1	Geislerová
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
uváděna	uváděn	k2eAgFnSc1d1	uváděna
jako	jako	k8xC	jako
Aňa	Aňa	k1gFnSc1	Aňa
Geislerová	Geislerová	k1gFnSc1	Geislerová
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1976	[number]	k4	1976
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
držitelka	držitelka	k1gFnSc1	držitelka
pěti	pět	k4xCc2	pět
Českých	český	k2eAgMnPc2d1	český
lvů	lev	k1gMnPc2	lev
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
herečky	herečka	k1gFnSc2	herečka
Ester	Ester	k1gFnSc2	Ester
Geislerové	Geislerová	k1gFnSc2	Geislerová
a	a	k8xC	a
výtvarnice	výtvarnice	k1gFnSc2	výtvarnice
a	a	k8xC	a
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Lenky	Lenka	k1gFnSc2	Lenka
Geislerové	Geislerová	k1gFnSc2	Geislerová
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
umělecké	umělecký	k2eAgFnSc2d1	umělecká
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
Věra	Věra	k1gFnSc1	Věra
je	být	k5eAaImIp3nS	být
akademická	akademický	k2eAgFnSc1d1	akademická
malířka	malířka	k1gFnSc1	malířka
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
Petr	Petr	k1gMnSc1	Petr
Geisler	Geisler	k1gMnSc1	Geisler
byl	být	k5eAaImAgMnS	být
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
japanolog	japanolog	k1gMnSc1	japanolog
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
na	na	k7c4	na
těžkou	těžký	k2eAgFnSc4d1	těžká
cirhózu	cirhóza	k1gFnSc4	cirhóza
jater	játra	k1gNnPc2	játra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
babička	babička	k1gFnSc1	babička
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
Růžena	Růžena	k1gFnSc1	Růžena
Lysenková	Lysenkový	k2eAgFnSc1d1	Lysenková
byla	být	k5eAaImAgFnS	být
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
sestry	sestra	k1gFnPc4	sestra
Ester	Ester	k1gFnSc2	Ester
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
herečkou	herečka	k1gFnSc7	herečka
<g/>
,	,	kIx,	,
a	a	k8xC	a
Lelu	Lela	k1gFnSc4	Lela
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vdaná	vdaný	k2eAgFnSc1d1	vdaná
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
syna	syn	k1gMnSc4	syn
Bruna	Bruno	k1gMnSc4	Bruno
Fidelia	Fidelius	k1gMnSc4	Fidelius
(	(	kIx(	(
<g/>
*	*	kIx~	*
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Stellu	Stella	k1gFnSc4	Stella
Ginger	Gingra	k1gFnPc2	Gingra
<g/>
,	,	kIx,	,
29.6	[number]	k4	29.6
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Max	Max	k1gMnSc1	Max
<g/>
.	.	kIx.	.
Její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
si	se	k3xPyFc3	se
vzala	vzít	k5eAaPmAgFnS	vzít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
režisérem	režisér	k1gMnSc7	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
přítelem	přítel	k1gMnSc7	přítel
byl	být	k5eAaImAgMnS	být
miliardář	miliardář	k1gMnSc1	miliardář
Martin	Martin	k1gMnSc1	Martin
Shenar	Shenar	k1gMnSc1	Shenar
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gMnPc4	její
největší	veliký	k2eAgMnPc4d3	veliký
přátele	přítel	k1gMnPc4	přítel
patří	patřit	k5eAaImIp3nS	patřit
herečka	herečka	k1gFnSc1	herečka
Tatiana	Tatiana	k1gFnSc1	Tatiana
Vilhelmová	Vilhelmová	k1gFnSc1	Vilhelmová
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
angažuje	angažovat	k5eAaBmIp3nS	angažovat
v	v	k7c6	v
charitativních	charitativní	k2eAgInPc6d1	charitativní
projektech	projekt	k1gInPc6	projekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
do	do	k7c2	do
Milána	Milán	k1gInSc2	Milán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
stát	stát	k5eAaImF	stát
modelkou	modelka	k1gFnSc7	modelka
<g/>
,	,	kIx,	,
po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
bloudění	bloudění	k1gNnSc2	bloudění
po	po	k7c6	po
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
studovala	studovat	k5eAaImAgFnS	studovat
Pražskou	pražský	k2eAgFnSc4d1	Pražská
konzervatoř	konzervatoř	k1gFnSc4	konzervatoř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedokončila	dokončit	k5eNaPmAgFnS	dokončit
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
European	European	k1gInSc1	European
Shooting	Shooting	k1gInSc1	Shooting
Star	Star	kA	Star
-	-	kIx~	-
2004	[number]	k4	2004
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
nominace	nominace	k1gFnSc2	nominace
na	na	k7c4	na
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
"	"	kIx"	"
1994	[number]	k4	1994
-	-	kIx~	-
Jízda	jízda	k1gFnSc1	jízda
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
<g/>
"	"	kIx"	"
1999	[number]	k4	1999
-	-	kIx~	-
Návrat	návrat	k1gInSc1	návrat
idiota	idiot	k1gMnSc2	idiot
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
"	"	kIx"	"
2004	[number]	k4	2004
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Želary	Želar	k1gInPc4	Želar
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
vedlejší	vedlejší	k2eAgInSc4d1	vedlejší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
<g/>
"	"	kIx"	"
2005	[number]	k4	2005
-	-	kIx~	-
Štěstí	štěstí	k1gNnSc1	štěstí
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
"	"	kIx"	"
-	-	kIx~	-
2006	[number]	k4	2006
-	-	kIx~	-
Kráska	kráska	k1gFnSc1	kráska
v	v	k7c6	v
nesnázích	nesnáz	k1gFnPc6	nesnáz
nominace	nominace	k1gFnSc2	nominace
na	na	k7c4	na
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
vedlejší	vedlejší	k2eAgInSc4d1	vedlejší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
<g/>
"	"	kIx"	"
2007	[number]	k4	2007
-	-	kIx~	-
Medvídek	medvídek	k1gMnSc1	medvídek
nominace	nominace	k1gFnSc2	nominace
na	na	k7c4	na
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
<g />
.	.	kIx.	.
</s>
<s>
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
"	"	kIx"	"
2010	[number]	k4	2010
-	-	kIx~	-
Občanský	občanský	k2eAgInSc1d1	občanský
průkaz	průkaz	k1gInSc1	průkaz
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
"	"	kIx"	"
2011	[number]	k4	2011
-	-	kIx~	-
Nevinnost	nevinnost	k1gFnSc1	nevinnost
Stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
mušle	mušle	k1gFnSc1	mušle
na	na	k7c6	na
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
San	San	k1gMnSc6	San
Sebastiánu	Sebastián	k1gMnSc6	Sebastián
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
vedlejší	vedlejší	k2eAgInSc4d1	vedlejší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
<g/>
"	"	kIx"	"
2005	[number]	k4	2005
-	-	kIx~	-
Štěstí	štěstí	k1gNnSc1	štěstí
Roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
nafotila	nafotit	k5eAaPmAgFnS	nafotit
titulní	titulní	k2eAgFnSc4d1	titulní
<g />
.	.	kIx.	.
</s>
<s>
stranu	strana	k1gFnSc4	strana
prestižního	prestižní	k2eAgInSc2d1	prestižní
kalendáře	kalendář	k1gInSc2	kalendář
Stock	Stocka	k1gFnPc2	Stocka
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc4	výběr
<g/>
)	)	kIx)	)
Pějme	pět	k5eAaImRp1nP	pět
píseň	píseň	k1gFnSc4	píseň
dohola	dohola	k6eAd1	dohola
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Requiem	Requius	k1gMnSc7	Requius
pro	pro	k7c4	pro
panenku	panenka	k1gFnSc4	panenka
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
-	-	kIx~	-
Marika	Marika	k1gFnSc1	Marika
Přítelkyně	přítelkyně	k1gFnSc2	přítelkyně
z	z	k7c2	z
domu	dům	k1gInSc2	dům
smutku	smutek	k1gInSc2	smutek
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
-	-	kIx~	-
Líba	Líba	k1gFnSc1	Líba
Jízda	jízda	k1gFnSc1	jízda
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
-	-	kIx~	-
Anna	Anna	k1gFnSc1	Anna
Prima	primo	k1gNnSc2	primo
sezóna	sezóna	k1gFnSc1	sezóna
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
-	-	kIx~	-
Irena	Irena	k1gFnSc1	Irena
Výchova	výchova	k1gFnSc1	výchova
dívek	dívka	k1gFnPc2	dívka
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
-	-	kIx~	-
Beáta	Beáta	k1gFnSc1	Beáta
Návrat	návrat	k1gInSc1	návrat
idiota	idiot	k1gMnSc2	idiot
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
-	-	kIx~	-
Anna	Anna	k1gFnSc1	Anna
Kuře	kuře	k1gNnSc1	kuře
melancholik	melancholik	k1gMnSc1	melancholik
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
-	-	kIx~	-
Matka	matka	k1gFnSc1	matka
Kytice	kytice	k1gFnSc1	kytice
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
-	-	kIx~	-
Dornička	Dornička	k1gFnSc1	Dornička
Výlet	výlet	k1gInSc1	výlet
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Žena	žena	k1gFnSc1	žena
v	v	k7c6	v
porouchaném	porouchaný	k2eAgNnSc6d1	porouchané
autě	auto	k1gNnSc6	auto
Želary	Želara	k1gFnSc2	Želara
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
-	-	kIx~	-
Eliška	Eliška	k1gFnSc1	Eliška
/	/	kIx~	/
Hana	Hana	k1gFnSc1	Hana
Štěstí	štěstí	k1gNnSc2	štěstí
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
-	-	kIx~	-
Dagmar	Dagmar	k1gFnSc1	Dagmar
Kráska	kráska	k1gFnSc1	kráska
v	v	k7c6	v
nesnázích	nesnáz	k1gFnPc6	nesnáz
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
-	-	kIx~	-
Marcela	Marcela	k1gFnSc1	Marcela
Smutek	smutek	k1gInSc1	smutek
paní	paní	k1gFnSc2	paní
Šnajderové	Šnajderová	k1gFnSc2	Šnajderová
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Medvídek	medvídek	k1gMnSc1	medvídek
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
-	-	kIx~	-
Anna	Anna	k1gFnSc1	Anna
Ďáblova	ďáblův	k2eAgFnSc1d1	Ďáblova
<g />
.	.	kIx.	.
</s>
<s>
lest	lest	k1gFnSc1	lest
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
-	-	kIx~	-
policejní	policejní	k2eAgFnSc1d1	policejní
vyšetřovatelka	vyšetřovatelka	k1gFnSc1	vyšetřovatelka
Šímová	Šímová	k1gFnSc1	Šímová
Občanský	občanský	k2eAgInSc4d1	občanský
průkaz	průkaz	k1gInSc4	průkaz
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
-	-	kIx~	-
Petrova	Petrův	k2eAgFnSc1d1	Petrova
máma	máma	k1gFnSc1	máma
Nevinnost	nevinnost	k1gFnSc1	nevinnost
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
-	-	kIx~	-
Lída	Lída	k1gFnSc1	Lída
Ztracená	ztracený	k2eAgFnSc1d1	ztracená
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
-	-	kIx~	-
policejní	policejní	k2eAgFnSc1d1	policejní
vyšetřovatelka	vyšetřovatelka	k1gFnSc1	vyšetřovatelka
Šímová	Šímová	k1gFnSc1	Šímová
Líbánky	líbánky	k1gInPc1	líbánky
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
-	-	kIx~	-
nevěsta	nevěsta	k1gFnSc1	nevěsta
Tereza	Tereza	k1gFnSc1	Tereza
Pohádkář	pohádkář	k1gMnSc1	pohádkář
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
-	-	kIx~	-
Dana	Dana	k1gFnSc1	Dana
Fair	fair	k6eAd1	fair
Play	play	k0	play
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
-	-	kIx~	-
Irena	Irena	k1gFnSc1	Irena
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Anny	Anna	k1gFnSc2	Anna
Polednice	polednice	k1gFnSc2	polednice
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
-	-	kIx~	-
Eliška	Eliška	k1gFnSc1	Eliška
Anthropoid	Anthropoid	k1gInSc1	Anthropoid
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
-	-	kIx~	-
Lenka	Lenka	k1gFnSc1	Lenka
Fafková	Fafková	k1gFnSc1	Fafková
Hraje	hrát	k5eAaImIp3nS	hrát
ústřední	ústřední	k2eAgFnSc4d1	ústřední
roli	role	k1gFnSc4	role
ve	v	k7c6	v
videoklipu	videoklip	k1gInSc6	videoklip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
Boba	Bob	k1gMnSc2	Bob
Sinclara	Sinclar	k1gMnSc2	Sinclar
I	i	k8xC	i
Feel	Feel	k1gInSc4	Feel
For	forum	k1gNnPc2	forum
You	You	k1gFnSc7	You
<g/>
.	.	kIx.	.
</s>
<s>
Madona	Madona	k1gFnSc1	Madona
ve	v	k7c6	v
videoklipu	videoklip	k1gInSc6	videoklip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
Roba	roba	k1gFnSc1	roba
Grigorova	Grigorův	k2eAgFnSc1d1	Grigorova
Ona	onen	k3xDgFnSc1	onen
je	být	k5eAaImIp3nS	být
Madona	Madona	k1gFnSc1	Madona
P.	P.	kA	P.
<g/>
S.	S.	kA	S.
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
-	-	kIx~	-
deníkové	deníkový	k2eAgFnSc2d1	deníková
zápisky	zápiska	k1gFnSc2	zápiska
(	(	kIx(	(
<g/>
Kosmas	Kosmas	k1gMnSc1	Kosmas
Cena	cena	k1gFnSc1	cena
čtenářů	čtenář	k1gMnPc2	čtenář
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Anna	Anna	k1gFnSc1	Anna
Geislerová	Geislerový	k2eAgFnSc1d1	Geislerová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Zpráva	zpráva	k1gFnSc1	zpráva
Albánci	Albánec	k1gMnPc1	Albánec
vysílají	vysílat	k5eAaImIp3nP	vysílat
do	do	k7c2	do
boje	boj	k1gInSc2	boj
o	o	k7c4	o
Oscara	Oscar	k1gMnSc4	Oscar
film	film	k1gInSc4	film
s	s	k7c7	s
Geislerovou	Geislerová	k1gFnSc7	Geislerová
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
Osoba	osoba	k1gFnSc1	osoba
Anna	Anna	k1gFnSc1	Anna
Geislerová	Geislerová	k1gFnSc1	Geislerová
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Anna	Anna	k1gFnSc1	Anna
Geislerová	Geislerová	k1gFnSc1	Geislerová
Anna	Anna	k1gFnSc1	Anna
Geislerová	Geislerová	k1gFnSc1	Geislerová
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Anna	Anna	k1gFnSc1	Anna
Geislerová	Geislerová	k1gFnSc1	Geislerová
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Anna	Anna	k1gFnSc1	Anna
Geislerová	Geislerová	k1gFnSc1	Geislerová
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Aňa	Aňa	k1gFnSc1	Aňa
Geislerová	Geislerová	k1gFnSc1	Geislerová
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Miluju	milovat	k5eAaImIp1nS	milovat
<g/>
,	,	kIx,	,
miluju	milovat	k5eAaImIp1nS	milovat
<g/>
,	,	kIx,	,
miluju	milovat	k5eAaImIp1nS	milovat
<g/>
,	,	kIx,	,
-	-	kIx~	-
rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Geislerovou	Geislerová	k1gFnSc7	Geislerová
o	o	k7c6	o
herectví	herectví	k1gNnSc6	herectví
i	i	k8xC	i
osobním	osobní	k2eAgInSc6d1	osobní
životě	život	k1gInSc6	život
Geislerová	Geislerová	k1gFnSc1	Geislerová
<g/>
:	:	kIx,	:
down	down	k1gMnSc1	down
to	ten	k3xDgNnSc4	ten
earth	earth	k1gMnSc1	earth
stardom	stardom	k1gInSc1	stardom
-	-	kIx~	-
The	The	k1gFnSc1	The
leading	leading	k1gInSc4	leading
Czech	Czech	k1gInSc1	Czech
ingénue	ingénuat	k5eAaPmIp3nS	ingénuat
on	on	k3xPp3gInSc1	on
motherhood	motherhood	k1gInSc1	motherhood
<g/>
,	,	kIx,	,
nasty	nast	k1gInPc1	nast
people	people	k6eAd1	people
and	and	k?	and
performing	performing	k1gInSc1	performing
as	as	k1gNnSc2	as
therapy	therapa	k1gFnSc2	therapa
<g/>
,	,	kIx,	,
Kristina	Kristina	k1gFnSc1	Kristina
Alda	Alda	k1gMnSc1	Alda
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Prague	Pragu	k1gInSc2	Pragu
Post	posta	k1gFnPc2	posta
<g/>
,	,	kIx,	,
4.10	[number]	k4	4.10
<g/>
.2006	.2006	k4	.2006
-	-	kIx~	-
rozhovor	rozhovor	k1gInSc4	rozhovor
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Geislerovou	Geislerová	k1gFnSc7	Geislerová
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
