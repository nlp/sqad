<s>
Fotoelektrický	fotoelektrický	k2eAgInSc1d1	fotoelektrický
jev	jev	k1gInSc1	jev
či	či	k8xC	či
fotoefekt	fotoefekt	k1gInSc1	fotoefekt
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
jsou	být	k5eAaImIp3nP	být
elektrony	elektron	k1gInPc1	elektron
uvolňovány	uvolňován	k2eAgInPc1d1	uvolňován
(	(	kIx(	(
<g/>
vyzařovány	vyzařován	k2eAgInPc1d1	vyzařován
<g/>
,	,	kIx,	,
emitovány	emitován	k2eAgInPc1d1	emitován
<g/>
)	)	kIx)	)
z	z	k7c2	z
látky	látka	k1gFnSc2	látka
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
kovu	kov	k1gInSc2	kov
<g/>
)	)	kIx)	)
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
absorpce	absorpce	k1gFnSc2	absorpce
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
(	(	kIx(	(
<g/>
např.	např.	kA	např.
rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
záření	záření	k1gNnSc1	záření
nebo	nebo	k8xC	nebo
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
látkou	látka	k1gFnSc7	látka
<g/>
.	.	kIx.	.
</s>
