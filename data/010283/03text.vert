<p>
<s>
Indonesia	Indonesia	k1gFnSc1	Indonesia
AirAsia	AirAsium	k1gNnSc2	AirAsium
je	být	k5eAaImIp3nS	být
nízkonákladová	nízkonákladový	k2eAgFnSc1d1	nízkonákladová
letecká	letecký	k2eAgFnSc1d1	letecká
společnost	společnost	k1gFnSc1	společnost
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
indonéské	indonéský	k2eAgFnSc6d1	Indonéská
Jakartě	Jakarta	k1gFnSc6	Jakarta
<g/>
.	.	kIx.	.
</s>
<s>
Provozuje	provozovat	k5eAaImIp3nS	provozovat
vnitrostátní	vnitrostátní	k2eAgInPc4d1	vnitrostátní
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
lety	let	k1gInPc4	let
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Indonésie	Indonésie	k1gFnSc2	Indonésie
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
skupiny	skupina	k1gFnSc2	skupina
společností	společnost	k1gFnPc2	společnost
AirAsia	AirAsium	k1gNnSc2	AirAsium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2017	[number]	k4	2017
se	se	k3xPyFc4	se
flotila	flotila	k1gFnSc1	flotila
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
17	[number]	k4	17
letadel	letadlo	k1gNnPc2	letadlo
Airbus	airbus	k1gInSc4	airbus
A	a	k8xC	a
<g/>
320	[number]	k4	320
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k8xS	jako
AwAir	AwAir	k1gMnSc1	AwAir
(	(	kIx(	(
<g/>
Air	Air	k1gMnSc1	Air
Wagon	Wagon	k1gMnSc1	Wagon
International	International	k1gMnSc1	International
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc4	provoz
zahájila	zahájit	k5eAaPmAgFnS	zahájit
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2000	[number]	k4	2000
s	s	k7c7	s
flotilou	flotila	k1gFnSc7	flotila
letadel	letadlo	k1gNnPc2	letadlo
Airbus	airbus	k1gInSc1	airbus
A300	A300	k1gFnPc1	A300
a	a	k8xC	a
A	a	k9	a
<g/>
310	[number]	k4	310
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
skupiny	skupina	k1gFnSc2	skupina
AirAsia	AirAsia	k1gFnSc1	AirAsia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2005	[number]	k4	2005
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Indonesia	Indonesius	k1gMnSc4	Indonesius
AirAsia	AirAsius	k1gMnSc4	AirAsius
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nehody	nehoda	k1gFnSc2	nehoda
==	==	k?	==
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
se	se	k3xPyFc4	se
ztratil	ztratit	k5eAaPmAgInS	ztratit
let	let	k1gInSc1	let
8501	[number]	k4	8501
společnosti	společnost	k1gFnSc2	společnost
Indonesia	Indonesia	k1gFnSc1	Indonesia
AirAsia	AirAsia	k1gFnSc1	AirAsia
(	(	kIx(	(
<g/>
Airbus	airbus	k1gInSc1	airbus
A320-216	A320-216	k1gFnSc2	A320-216
registrace	registrace	k1gFnSc2	registrace
PK-AXC	PK-AXC	k1gFnSc2	PK-AXC
<g/>
)	)	kIx)	)
se	s	k7c7	s
155	[number]	k4	155
cestujícími	cestující	k1gMnPc7	cestující
a	a	k8xC	a
7	[number]	k4	7
členy	člen	k1gMnPc7	člen
posádky	posádka	k1gFnSc2	posádka
při	při	k7c6	při
letu	let	k1gInSc6	let
ze	z	k7c2	z
Surabaye	Surabay	k1gFnSc2	Surabay
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
do	do	k7c2	do
Singapuru	Singapur	k1gInSc2	Singapur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
AirAsia	AirAsia	k1gFnSc1	AirAsia
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Indonesia	Indonesius	k1gMnSc2	Indonesius
AirAsia	AirAsius	k1gMnSc2	AirAsius
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
společnosti	společnost	k1gFnSc2	společnost
AirAsia	AirAsia	k1gFnSc1	AirAsia
</s>
</p>
