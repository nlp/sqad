<s>
Moták	moták	k1gMnSc1	moták
pochop	pochop	k1gMnSc1	pochop
je	být	k5eAaImIp3nS	být
tažný	tažný	k2eAgInSc4d1	tažný
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c4	na
zimoviště	zimoviště	k1gNnSc4	zimoviště
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Africe	Afrika	k1gFnSc6	Afrika
odlétá	odlétat	k5eAaPmIp3nS	odlétat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
až	až	k8xS	až
říjnu	říjen	k1gInSc6	říjen
a	a	k8xC	a
ze	z	k7c2	z
zimovišť	zimoviště	k1gNnPc2	zimoviště
přilétá	přilétat	k5eAaImIp3nS	přilétat
během	během	k7c2	během
března	březen	k1gInSc2	březen
a	a	k8xC	a
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
