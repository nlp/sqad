<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Müller	Müller	k1gMnSc1	Müller
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1864	[number]	k4	1864
Mnichov	Mnichov	k1gInSc1	Mnichov
u	u	k7c2	u
Karlových	Karlův	k2eAgInPc2d1	Karlův
Varů	Vary	k1gInPc2	Vary
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1955	[number]	k4	1955
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
sociálně	sociálně	k6eAd1	sociálně
demokratický	demokratický	k2eAgMnSc1d1	demokratický
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
poslanec	poslanec	k1gMnSc1	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
poslanec	poslanec	k1gMnSc1	poslanec
rakouské	rakouský	k2eAgFnSc2d1	rakouská
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
člen	člen	k1gMnSc1	člen
Spolkové	spolkový	k2eAgFnSc2d1	spolková
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Vychodil	vychodit	k5eAaPmAgInS	vychodit
národní	národní	k2eAgFnSc4d1	národní
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
správcem	správce	k1gMnSc7	správce
německo-rakouské	německoakouský	k2eAgFnSc2d1	německo-rakouská
železničářské	železničářský	k2eAgFnSc2d1	Železničářská
organizace	organizace	k1gFnSc2	organizace
a	a	k8xC	a
ředitelem	ředitel	k1gMnSc7	ředitel
železničářského	železničářský	k2eAgInSc2d1	železničářský
domu	dům	k1gInSc2	dům
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Vydával	vydávat	k5eAaPmAgMnS	vydávat
odborné	odborný	k2eAgInPc4d1	odborný
spisy	spis	k1gInPc4	spis
<g/>
.	.	kIx.	.
</s>
<s>
Angažoval	angažovat	k5eAaBmAgInS	angažovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc3d1	demokratická
straně	strana	k1gFnSc3	strana
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgInS	zapojit
i	i	k9	i
do	do	k7c2	do
celostátní	celostátní	k2eAgFnSc2d1	celostátní
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
doplňovacích	doplňovací	k2eAgFnPc6d1	doplňovací
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
získal	získat	k5eAaPmAgInS	získat
mandát	mandát	k1gInSc4	mandát
v	v	k7c6	v
Říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
(	(	kIx(	(
<g/>
celostátní	celostátní	k2eAgInSc1d1	celostátní
zákonodárný	zákonodárný	k2eAgInSc1d1	zákonodárný
sbor	sbor	k1gInSc1	sbor
<g/>
)	)	kIx)	)
za	za	k7c4	za
obvod	obvod	k1gInSc4	obvod
Slezsko	Slezsko	k1gNnSc1	Slezsko
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Usedl	usednout	k5eAaPmAgMnS	usednout
do	do	k7c2	do
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
frakce	frakce	k1gFnSc2	frakce
Klub	klub	k1gInSc1	klub
německých	německý	k2eAgMnPc2d1	německý
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
obhájil	obhájit	k5eAaPmAgInS	obhájit
i	i	k9	i
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
za	za	k7c4	za
obvod	obvod	k1gInSc4	obvod
Dolní	dolní	k2eAgInPc4d1	dolní
Rakousy	Rakousy	k1gInPc4	Rakousy
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
parlamentu	parlament	k1gInSc6	parlament
setrval	setrvat	k5eAaPmAgInS	setrvat
až	až	k6eAd1	až
do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
<g/>
Profesně	profesně	k6eAd1	profesně
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1911	[number]	k4	1911
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k8xC	jako
člen	člen	k1gInSc1	člen
dělnického	dělnický	k2eAgInSc2d1	dělnický
poradního	poradní	k2eAgInSc2d1	poradní
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
zasedal	zasedat	k5eAaImAgInS	zasedat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1919	[number]	k4	1919
jako	jako	k8xS	jako
poslanec	poslanec	k1gMnSc1	poslanec
Provizorního	provizorní	k2eAgNnSc2d1	provizorní
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
Německého	německý	k2eAgNnSc2d1	německé
Rakouska	Rakousko	k1gNnSc2	Rakousko
(	(	kIx(	(
<g/>
Provisorische	Provisorische	k1gFnSc1	Provisorische
Nationalversammlung	Nationalversammlung	k1gMnSc1	Nationalversammlung
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1919	[number]	k4	1919
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1919	[number]	k4	1919
jako	jako	k8xC	jako
poslanec	poslanec	k1gMnSc1	poslanec
Ústavodárného	ústavodárný	k2eAgNnSc2d1	Ústavodárné
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
za	za	k7c4	za
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
sociálně	sociálně	k6eAd1	sociálně
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1920	[number]	k4	1920
do	do	k7c2	do
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1932	[number]	k4	1932
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Spolkové	spolkový	k2eAgFnSc2d1	spolková
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
horní	horní	k2eAgFnSc1d1	horní
komora	komora	k1gFnSc1	komora
rakouského	rakouský	k2eAgInSc2d1	rakouský
parlamentu	parlament	k1gInSc2	parlament
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
zasedal	zasedat	k5eAaImAgInS	zasedat
ve	v	k7c6	v
Vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
obecní	obecní	k2eAgFnSc6d1	obecní
radě	rada	k1gFnSc6	rada
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
poslancem	poslanec	k1gMnSc7	poslanec
Dolnorakouského	dolnorakouský	k2eAgInSc2d1	dolnorakouský
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
působil	působit	k5eAaImAgInS	působit
i	i	k9	i
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
tamního	tamní	k2eAgInSc2d1	tamní
zemského	zemský	k2eAgInSc2d1	zemský
výboru	výbor	k1gInSc2	výbor
(	(	kIx(	(
<g/>
zemský	zemský	k2eAgMnSc1d1	zemský
rada	rada	k1gMnSc1	rada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
