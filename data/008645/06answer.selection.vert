<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Müller	Müller	k1gMnSc1	Müller
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1864	[number]	k4	1864
Mnichov	Mnichov	k1gInSc1	Mnichov
u	u	k7c2	u
Karlových	Karlův	k2eAgInPc2d1	Karlův
Varů	Vary	k1gInPc2	Vary
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1955	[number]	k4	1955
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
sociálně	sociálně	k6eAd1	sociálně
demokratický	demokratický	k2eAgMnSc1d1	demokratický
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
poslanec	poslanec	k1gMnSc1	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
poslanec	poslanec	k1gMnSc1	poslanec
rakouské	rakouský	k2eAgFnSc2d1	rakouská
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
člen	člen	k1gMnSc1	člen
Spolkové	spolkový	k2eAgFnSc2d1	spolková
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
