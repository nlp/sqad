<s>
Bílý	bílý	k2eAgInSc1d1	bílý
čaj	čaj	k1gInSc1	čaj
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
sklízí	sklízet	k5eAaImIp3nS	sklízet
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Fu-ťien	Fu-ťina	k1gFnPc2	Fu-ťina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
i	i	k9	i
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
Nepálu	Nepál	k1gInSc6	Nepál
<g/>
,	,	kIx,	,
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
<g/>
,	,	kIx,	,
severním	severní	k2eAgNnSc6d1	severní
Thajsku	Thajsko	k1gNnSc6	Thajsko
<g/>
,	,	kIx,	,
Galle	Galla	k1gFnSc6	Galla
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc2d1	jižní
Srí	Srí	k1gFnSc2	Srí
Lance	lance	k1gNnSc2	lance
<g/>
)	)	kIx)	)
a	a	k8xC	a
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
