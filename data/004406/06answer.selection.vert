<s>
Unie	unie	k1gFnSc1	unie
evropských	evropský	k2eAgFnPc2d1	Evropská
fotbalových	fotbalový	k2eAgFnPc2d1	fotbalová
asociací	asociace	k1gFnPc2	asociace
(	(	kIx(	(
<g/>
UEFA	UEFA	kA	UEFA
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Union	union	k1gInSc1	union
Européenne	Européenn	k1gInSc5	Européenn
de	de	k?	de
Football	Footballum	k1gNnPc2	Footballum
Association	Association	k1gInSc1	Association
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
řídící	řídící	k2eAgFnSc7d1	řídící
organizací	organizace	k1gFnSc7	organizace
evropského	evropský	k2eAgInSc2d1	evropský
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
,	,	kIx,	,
futsalu	futsal	k1gInSc2	futsal
a	a	k8xC	a
plážového	plážový	k2eAgInSc2d1	plážový
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
.	.	kIx.	.
</s>
