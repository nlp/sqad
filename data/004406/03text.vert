<s>
Unie	unie	k1gFnSc1	unie
evropských	evropský	k2eAgFnPc2d1	Evropská
fotbalových	fotbalový	k2eAgFnPc2d1	fotbalová
asociací	asociace	k1gFnPc2	asociace
(	(	kIx(	(
<g/>
UEFA	UEFA	kA	UEFA
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Union	union	k1gInSc1	union
Européenne	Européenn	k1gInSc5	Européenn
de	de	k?	de
Football	Footballum	k1gNnPc2	Footballum
Association	Association	k1gInSc1	Association
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
řídící	řídící	k2eAgFnSc7d1	řídící
organizací	organizace	k1gFnSc7	organizace
evropského	evropský	k2eAgInSc2d1	evropský
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
,	,	kIx,	,
futsalu	futsal	k1gInSc2	futsal
a	a	k8xC	a
plážového	plážový	k2eAgInSc2d1	plážový
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Členem	člen	k1gInSc7	člen
UEFA	UEFA	kA	UEFA
je	být	k5eAaImIp3nS	být
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
národních	národní	k2eAgFnPc2d1	národní
fotbalových	fotbalový	k2eAgFnPc2d1	fotbalová
asociací	asociace	k1gFnPc2	asociace
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
UEFA	UEFA	kA	UEFA
pořádá	pořádat	k5eAaImIp3nS	pořádat
evropské	evropský	k2eAgInPc4d1	evropský
reprezentační	reprezentační	k2eAgInPc4d1	reprezentační
a	a	k8xC	a
klubové	klubový	k2eAgFnPc4d1	klubová
soutěže	soutěž	k1gFnPc4	soutěž
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
rozdělování	rozdělování	k1gNnSc2	rozdělování
odměn	odměna	k1gFnPc2	odměna
<g/>
,	,	kIx,	,
hlídá	hlídat	k5eAaImIp3nS	hlídat
dodržování	dodržování	k1gNnSc1	dodržování
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
prodává	prodávat	k5eAaImIp3nS	prodávat
vysílací	vysílací	k2eAgNnPc4d1	vysílací
práva	právo	k1gNnPc4	právo
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
soutěžím	soutěž	k1gFnPc3	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
jasnému	jasný	k2eAgNnSc3d1	jasné
vedení	vedení	k1gNnSc3	vedení
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Asií	Asie	k1gFnSc7	Asie
jsou	být	k5eAaImIp3nP	být
jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
také	také	k9	také
státy	stát	k1gInPc1	stát
jako	jako	k8xC	jako
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
,	,	kIx,	,
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
<g/>
,	,	kIx,	,
Gruzie	Gruzie	k1gFnSc1	Gruzie
<g/>
,	,	kIx,	,
Arménie	Arménie	k1gFnSc1	Arménie
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
větší	veliký	k2eAgFnSc7d2	veliký
či	či	k8xC	či
menší	malý	k2eAgFnSc7d2	menší
částí	část	k1gFnSc7	část
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
také	také	k9	také
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
teoreticky	teoreticky	k6eAd1	teoreticky
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
členy	člen	k1gInPc7	člen
AFC	AFC	kA	AFC
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
Izrael	Izrael	k1gInSc1	Izrael
a	a	k8xC	a
Kypr	Kypr	k1gInSc1	Kypr
leží	ležet	k5eAaImIp3nS	ležet
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
Evropou	Evropa	k1gFnSc7	Evropa
je	on	k3xPp3gNnPc4	on
pojí	pojit	k5eAaImIp3nP	pojit
historické	historický	k2eAgInPc4d1	historický
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgInPc4d1	kulturní
a	a	k8xC	a
politické	politický	k2eAgInPc4d1	politický
důvody	důvod	k1gInPc4	důvod
(	(	kIx(	(
<g/>
Izrael	Izrael	k1gInSc4	Izrael
a	a	k8xC	a
Kazachstán	Kazachstán	k1gInSc4	Kazachstán
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
bývalými	bývalý	k2eAgInPc7d1	bývalý
členy	člen	k1gInPc7	člen
AFC	AFC	kA	AFC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
nejsou	být	k5eNaImIp3nP	být
suverénními	suverénní	k2eAgInPc7d1	suverénní
státy	stát	k1gInPc7	stát
-	-	kIx~	-
například	například	k6eAd1	například
Faerské	Faerský	k2eAgInPc1d1	Faerský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc1	Skotsko
<g/>
,	,	kIx,	,
Wales	Wales	k1gInSc1	Wales
a	a	k8xC	a
Severní	severní	k2eAgNnSc1d1	severní
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Monako	Monako	k1gNnSc1	Monako
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
jediným	jediný	k2eAgMnSc7d1	jediný
evropským	evropský	k2eAgMnSc7d1	evropský
členem	člen	k1gMnSc7	člen
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
není	být	k5eNaImIp3nS	být
členem	člen	k1gMnSc7	člen
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
asociací	asociace	k1gFnSc7	asociace
–	–	k?	–
FIFA	FIFA	kA	FIFA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
UEFA	UEFA	kA	UEFA
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
kontinentálních	kontinentální	k2eAgFnPc2d1	kontinentální
konfederací	konfederace	k1gFnPc2	konfederace
pod	pod	k7c7	pod
FIFA	FIFA	kA	FIFA
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
konfederace	konfederace	k1gFnSc1	konfederace
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
vlivu	vliv	k1gInSc2	vliv
na	na	k7c6	na
klubové	klubový	k2eAgFnSc6d1	klubová
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
hráčů	hráč	k1gMnPc2	hráč
světa	svět	k1gInSc2	svět
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
reprezentačních	reprezentační	k2eAgInPc2d1	reprezentační
týmů	tým	k1gInPc2	tým
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
zóny	zóna	k1gFnSc2	zóna
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
32	[number]	k4	32
účastnických	účastnický	k2eAgNnPc2d1	účastnické
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
týmy	tým	k1gInPc4	tým
ze	z	k7c2	z
zóny	zóna	k1gFnSc2	zóna
UEFA	UEFA	kA	UEFA
alokováno	alokován	k2eAgNnSc1d1	alokováno
13	[number]	k4	13
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
FIFA	FIFA	kA	FIFA
z	z	k7c2	z
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
k	k	k7c3	k
nasazení	nasazení	k1gNnSc3	nasazení
do	do	k7c2	do
losovacích	losovací	k2eAgInPc2d1	losovací
košů	koš	k1gInPc2	koš
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
na	na	k7c4	na
MS	MS	kA	MS
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
12	[number]	k4	12
z	z	k7c2	z
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
20	[number]	k4	20
týmů	tým	k1gInPc2	tým
tohoto	tento	k3xDgInSc2	tento
žebříčku	žebříček	k1gInSc2	žebříček
ze	z	k7c2	z
zóny	zóna	k1gFnSc2	zóna
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentační	reprezentační	k2eAgInPc1d1	reprezentační
týmy	tým	k1gInPc1	tým
ze	z	k7c2	z
zóny	zóna	k1gFnSc2	zóna
UEFA	UEFA	kA	UEFA
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
podobně	podobně	k6eAd1	podobně
i	i	k9	i
v	v	k7c6	v
ženském	ženský	k2eAgInSc6d1	ženský
fotbale	fotbal	k1gInSc6	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
16	[number]	k4	16
účastnických	účastnický	k2eAgNnPc2d1	účastnické
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
žen	žena	k1gFnPc2	žena
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
zóně	zóna	k1gFnSc3	zóna
UEFA	UEFA	kA	UEFA
alokováno	alokovat	k5eAaImNgNnS	alokovat
5	[number]	k4	5
účastnických	účastnický	k2eAgNnPc2d1	účastnické
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
10	[number]	k4	10
týmů	tým	k1gInPc2	tým
ženského	ženský	k2eAgInSc2d1	ženský
žebříčku	žebříček	k1gInSc2	žebříček
FIFA	FIFA	kA	FIFA
po	po	k7c6	po
MS	MS	kA	MS
žen	žena	k1gFnPc2	žena
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
pětice	pětice	k1gFnSc1	pětice
evropská	evropský	k2eAgFnSc1d1	Evropská
<g/>
.	.	kIx.	.
</s>
<s>
UEFA	UEFA	kA	UEFA
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1954	[number]	k4	1954
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
po	po	k7c6	po
jednání	jednání	k1gNnSc6	jednání
mezi	mezi	k7c7	mezi
zástupci	zástupce	k1gMnPc7	zástupce
francouzské	francouzský	k2eAgFnSc2d1	francouzská
<g/>
,	,	kIx,	,
italské	italský	k2eAgFnSc2d1	italská
a	a	k8xC	a
belgické	belgický	k2eAgFnSc2d1	belgická
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
organizace	organizace	k1gFnSc2	organizace
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
do	do	k7c2	do
Bernu	Bern	k1gInSc2	Bern
<g/>
.	.	kIx.	.
</s>
<s>
Ebbe	Ebbe	k1gNnSc1	Ebbe
Schwartz	Schwartz	k1gMnSc1	Schwartz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
generálním	generální	k2eAgMnSc7d1	generální
sekretářem	sekretář	k1gMnSc7	sekretář
byl	být	k5eAaImAgMnS	být
Henri	Henri	k1gNnSc4	Henri
Delaunay	Delaunaa	k1gFnSc2	Delaunaa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
novým	nový	k2eAgNnSc7d1	nové
sídlem	sídlo	k1gNnSc7	sídlo
stal	stát	k5eAaPmAgInS	stát
švýcarský	švýcarský	k2eAgInSc1d1	švýcarský
Nyon	Nyon	k1gInSc1	Nyon
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měla	mít	k5eAaImAgFnS	mít
UEFA	UEFA	kA	UEFA
25	[number]	k4	25
členských	členský	k2eAgFnPc2d1	členská
národních	národní	k2eAgFnPc2d1	národní
asociací	asociace	k1gFnPc2	asociace
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
současností	současnost	k1gFnSc7	současnost
je	být	k5eAaImIp3nS	být
členů	člen	k1gInPc2	člen
55	[number]	k4	55
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
UEFA	UEFA	kA	UEFA
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
jedenáctkrát	jedenáctkrát	k6eAd1	jedenáctkrát
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
4	[number]	k4	4
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
4	[number]	k4	4
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc1	Španělsko
jednou	jednou	k6eAd1	jednou
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kluby	klub	k1gInPc1	klub
ze	z	k7c2	z
zóny	zóna	k1gFnSc2	zóna
UEFA	UEFA	kA	UEFA
vyhrály	vyhrát	k5eAaPmAgInP	vyhrát
21	[number]	k4	21
<g/>
x	x	k?	x
Interkontinentální	interkontinentální	k2eAgInSc4d1	interkontinentální
pohár	pohár	k1gInSc4	pohár
a	a	k8xC	a
devětkrát	devětkrát	k6eAd1	devětkrát
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
klubů	klub	k1gInPc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ženském	ženský	k2eAgInSc6d1	ženský
fotbale	fotbal	k1gInSc6	fotbal
vyhrály	vyhrát	k5eAaPmAgInP	vyhrát
národní	národní	k2eAgInPc4d1	národní
týmy	tým	k1gInPc4	tým
z	z	k7c2	z
UEFA	UEFA	kA	UEFA
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
žen	žena	k1gFnPc2	žena
třikrát	třikrát	k6eAd1	třikrát
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
2	[number]	k4	2
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
1	[number]	k4	1
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
Michel	Michel	k1gMnSc1	Michel
Platini	Platin	k2eAgMnPc1d1	Platin
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
UEFA	UEFA	kA	UEFA
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
55	[number]	k4	55
členů	člen	k1gInPc2	člen
(	(	kIx(	(
<g/>
platné	platný	k2eAgInPc4d1	platný
ke	k	k7c3	k
květnu	květen	k1gInSc3	květen
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
V	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
rok	rok	k1gInSc4	rok
přijetí	přijetí	k1gNnSc4	přijetí
za	za	k7c4	za
člena	člen	k1gMnSc4	člen
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
1	[number]	k4	1
Bývalý	bývalý	k2eAgInSc1d1	bývalý
člen	člen	k1gInSc1	člen
AFC	AFC	kA	AFC
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
a	a	k8xC	a
přidružený	přidružený	k2eAgMnSc1d1	přidružený
člen	člen	k1gMnSc1	člen
OFC	OFC	kA	OFC
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
UEFA	UEFA	kA	UEFA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
19942	[number]	k4	19942
Bývalý	bývalý	k2eAgInSc1d1	bývalý
člen	člen	k1gInSc1	člen
AFC	AFC	kA	AFC
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
UEFA	UEFA	kA	UEFA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
20023	[number]	k4	20023
Přijato	přijmout	k5eAaPmNgNnS	přijmout
jako	jako	k9	jako
ČSR4	ČSR4	k1gFnSc1	ČSR4
Přijato	přijmout	k5eAaPmNgNnS	přijmout
jako	jako	k8xS	jako
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
<g/>
5	[number]	k4	5
Přijato	přijmout	k5eAaPmNgNnS	přijmout
jako	jako	k9	jako
SSSR	SSSR	kA	SSSR
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
soutěží	soutěž	k1gFnSc7	soutěž
pro	pro	k7c4	pro
národní	národní	k2eAgInPc4d1	národní
týmy	tým	k1gInPc4	tým
je	být	k5eAaImIp3nS	být
mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
první	první	k4xOgInSc1	první
kvalifikační	kvalifikační	k2eAgInSc1d1	kvalifikační
cyklus	cyklus	k1gInSc1	cyklus
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
závěrečný	závěrečný	k2eAgInSc1d1	závěrečný
turnaj	turnaj	k1gInSc1	turnaj
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
UEFA	UEFA	kA	UEFA
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
záležitostí	záležitost	k1gFnPc2	záležitost
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
samotné	samotný	k2eAgFnSc2d1	samotná
MS	MS	kA	MS
pořádá	pořádat	k5eAaImIp3nS	pořádat
FIFA	FIFA	kA	FIFA
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
také	také	k9	také
alokuje	alokovat	k5eAaImIp3nS	alokovat
počet	počet	k1gInSc1	počet
míst	místo	k1gNnPc2	místo
na	na	k7c4	na
MS	MS	kA	MS
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
konfederace	konfederace	k1gFnPc4	konfederace
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
mluvit	mluvit	k5eAaImF	mluvit
do	do	k7c2	do
hracího	hrací	k2eAgInSc2d1	hrací
systému	systém	k1gInSc2	systém
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
záležitostí	záležitost	k1gFnPc2	záležitost
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc2	tento
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
Neméně	málo	k6eNd2	málo
důležité	důležitý	k2eAgFnPc1d1	důležitá
jsou	být	k5eAaImIp3nP	být
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
soutěže	soutěž	k1gFnPc1	soutěž
pro	pro	k7c4	pro
mládežnické	mládežnický	k2eAgInPc4d1	mládežnický
národní	národní	k2eAgInPc4d1	národní
týmy	tým	k1gInPc4	tým
jako	jako	k8xC	jako
ME	ME	kA	ME
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ME	ME	kA	ME
do	do	k7c2	do
19	[number]	k4	19
let	léto	k1gNnPc2	léto
a	a	k8xC	a
ME	ME	kA	ME
do	do	k7c2	do
17	[number]	k4	17
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ženské	ženský	k2eAgInPc4d1	ženský
týmy	tým	k1gInPc4	tým
je	být	k5eAaImIp3nS	být
pořádáno	pořádán	k2eAgNnSc1d1	pořádáno
mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
ME	ME	kA	ME
žen	žena	k1gFnPc2	žena
do	do	k7c2	do
19	[number]	k4	19
let	léto	k1gNnPc2	léto
a	a	k8xC	a
ME	ME	kA	ME
žen	žena	k1gFnPc2	žena
do	do	k7c2	do
17	[number]	k4	17
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
UEFA	UEFA	kA	UEFA
také	také	k9	také
spolupořádá	spolupořádat	k5eAaImIp3nS	spolupořádat
s	s	k7c7	s
organizací	organizace	k1gFnSc7	organizace
CAF	CAF	kA	CAF
pohár	pohár	k1gInSc4	pohár
UEFA	UEFA	kA	UEFA
<g/>
/	/	kIx~	/
<g/>
CAF	CAF	kA	CAF
Meridian	Meridian	k1gInSc1	Meridian
Cup	cup	k1gInSc1	cup
pro	pro	k7c4	pro
mládežnické	mládežnický	k2eAgInPc4d1	mládežnický
týmy	tým	k1gInPc4	tým
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
založila	založit	k5eAaPmAgFnS	založit
Pohár	pohár	k1gInSc4	pohár
regionů	region	k1gInPc2	region
UEFA	UEFA	kA	UEFA
pro	pro	k7c4	pro
poloprofesionální	poloprofesionální	k2eAgInPc4d1	poloprofesionální
týmy	tým	k1gInPc4	tým
reprezentující	reprezentující	k2eAgInSc4d1	reprezentující
svůj	svůj	k3xOyFgInSc4	svůj
region	region	k1gInSc4	region
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
futsalových	futsalův	k2eAgFnPc2d1	futsalův
soutěží	soutěž	k1gFnPc2	soutěž
pořádá	pořádat	k5eAaImIp3nS	pořádat
mistrovství	mistrovství	k1gNnSc1	mistrovství
UEFA	UEFA	kA	UEFA
ve	v	k7c6	v
futsale	futsala	k1gFnSc6	futsala
a	a	k8xC	a
mistrovství	mistrovství	k1gNnSc4	mistrovství
UEFA	UEFA	kA	UEFA
ve	v	k7c6	v
futsale	futsala	k1gFnSc6	futsala
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
UEFA	UEFA	kA	UEFA
také	také	k9	také
pořádá	pořádat	k5eAaImIp3nS	pořádat
dvě	dva	k4xCgFnPc4	dva
nejdůležitější	důležitý	k2eAgFnPc4d3	nejdůležitější
klubové	klubový	k2eAgFnPc4d1	klubová
soutěže	soutěž	k1gFnPc4	soutěž
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
Liga	liga	k1gFnSc1	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
LM	LM	kA	LM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1992	[number]	k4	1992
<g/>
/	/	kIx~	/
<g/>
93	[number]	k4	93
<g/>
.	.	kIx.	.
</s>
<s>
Účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
členské	členský	k2eAgFnSc2d1	členská
země	zem	k1gFnSc2	zem
UEFA	UEFA	kA	UEFA
jeden	jeden	k4xCgMnSc1	jeden
až	až	k9	až
čtyři	čtyři	k4xCgInPc1	čtyři
nejlépe	dobře	k6eAd3	dobře
umístěné	umístěný	k2eAgInPc1d1	umístěný
týmy	tým	k1gInPc1	tým
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
ročníku	ročník	k1gInSc6	ročník
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
ligy	liga	k1gFnSc2	liga
svého	svůj	k3xOyFgInSc2	svůj
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
počet	počet	k1gInSc1	počet
účastníků	účastník	k1gMnPc2	účastník
z	z	k7c2	z
dané	daný	k2eAgFnSc2d1	daná
země	zem	k1gFnSc2	zem
a	a	k8xC	a
fáze	fáze	k1gFnSc2	fáze
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
je	být	k5eAaImIp3nS	být
klub	klub	k1gInSc1	klub
nasazen	nasadit	k5eAaPmNgInS	nasadit
<g/>
,	,	kIx,	,
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
umístěni	umístěn	k2eAgMnPc1d1	umístěn
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
zemí	zem	k1gFnPc2	zem
sestaveném	sestavený	k2eAgInSc6d1	sestavený
podle	podle	k7c2	podle
klubových	klubový	k2eAgInPc2d1	klubový
koeficientů	koeficient	k1gInPc2	koeficient
UEFA	UEFA	kA	UEFA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Liga	liga	k1gFnSc1	liga
mistrů	mistr	k1gMnPc2	mistr
je	být	k5eAaImIp3nS	být
následovníkem	následovník	k1gMnSc7	následovník
Poháru	pohár	k1gInSc2	pohár
mistrů	mistr	k1gMnPc2	mistr
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
PMEZ	PMEZ	kA	PMEZ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1955	[number]	k4	1955
až	až	k9	až
1992	[number]	k4	1992
a	a	k8xC	a
účastnili	účastnit	k5eAaImAgMnP	účastnit
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
pouze	pouze	k6eAd1	pouze
mistři	mistr	k1gMnPc1	mistr
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
soutěží	soutěž	k1gFnSc7	soutěž
je	být	k5eAaImIp3nS	být
Evropská	evropský	k2eAgFnSc1d1	Evropská
liga	liga	k1gFnSc1	liga
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
EL	Ela	k1gFnPc2	Ela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
vítězové	vítěz	k1gMnPc1	vítěz
národních	národní	k2eAgInPc2d1	národní
pohárů	pohár	k1gInPc2	pohár
a	a	k8xC	a
nejvýše	nejvýše	k6eAd1	nejvýše
umístěné	umístěný	k2eAgInPc1d1	umístěný
týmy	tým	k1gInPc1	tým
v	v	k7c6	v
lize	liga	k1gFnSc6	liga
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nedostaly	dostat	k5eNaPmAgInP	dostat
do	do	k7c2	do
LM	LM	kA	LM
<g/>
.	.	kIx.	.
</s>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
jako	jako	k8xC	jako
Veletržní	veletržní	k2eAgInSc4d1	veletržní
pohár	pohár	k1gInSc4	pohár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
byla	být	k5eAaImAgFnS	být
plně	plně	k6eAd1	plně
převzata	převzít	k5eAaPmNgFnS	převzít
pod	pod	k7c4	pod
záštitu	záštita	k1gFnSc4	záštita
UEFA	UEFA	kA	UEFA
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Pohár	pohár	k1gInSc4	pohár
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
současný	současný	k2eAgInSc4d1	současný
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
soutěží	soutěž	k1gFnSc7	soutěž
kdysi	kdysi	k6eAd1	kdysi
byl	být	k5eAaImAgInS	být
Pohár	pohár	k1gInSc4	pohár
vítězů	vítěz	k1gMnPc2	vítěz
pohárů	pohár	k1gInPc2	pohár
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1960	[number]	k4	1960
až	až	k9	až
1999	[number]	k4	1999
účastnili	účastnit	k5eAaImAgMnP	účastnit
vítězové	vítěz	k1gMnPc1	vítěz
národních	národní	k2eAgInPc2d1	národní
pohárů	pohár	k1gInPc2	pohár
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
zrušení	zrušení	k1gNnSc6	zrušení
se	se	k3xPyFc4	se
vítězové	vítěz	k1gMnPc1	vítěz
národních	národní	k2eAgInPc2d1	národní
pohárů	pohár	k1gInPc2	pohár
účastní	účastnit	k5eAaImIp3nS	účastnit
Evropské	evropský	k2eAgFnSc2d1	Evropská
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Superpoháru	superpohár	k1gInSc6	superpohár
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
hraném	hraný	k2eAgNnSc6d1	hrané
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
zápas	zápas	k1gInSc4	zápas
na	na	k7c6	na
neutrální	neutrální	k2eAgFnSc6d1	neutrální
půdě	půda	k1gFnSc6	půda
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
utkává	utkávat	k5eAaImIp3nS	utkávat
vítěz	vítěz	k1gMnSc1	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
s	s	k7c7	s
vítězem	vítěz	k1gMnSc7	vítěz
Evropské	evropský	k2eAgFnSc2d1	Evropská
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
hrán	hrát	k5eAaImNgInS	hrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
UEFA	UEFA	kA	UEFA
převzala	převzít	k5eAaPmAgFnS	převzít
svoji	svůj	k3xOyFgFnSc4	svůj
záštitu	záštita	k1gFnSc4	záštita
nad	nad	k7c7	nad
Pohárem	pohár	k1gInSc7	pohár
Intertoto	Intertota	k1gFnSc5	Intertota
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
letní	letní	k2eAgFnSc4d1	letní
soutěží	soutěž	k1gFnSc7	soutěž
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
kvalifikovat	kvalifikovat	k5eAaBmF	kvalifikovat
do	do	k7c2	do
Poháru	pohár	k1gInSc2	pohár
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
organizací	organizace	k1gFnPc2	organizace
CONMEBOL	CONMEBOL	kA	CONMEBOL
UEFA	UEFA	kA	UEFA
pořádala	pořádat	k5eAaImAgFnS	pořádat
Interkontinentální	interkontinentální	k2eAgInSc4d1	interkontinentální
pohár	pohár	k1gInSc4	pohár
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
utkával	utkávat	k5eAaImAgMnS	utkávat
vítěz	vítěz	k1gMnSc1	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
a	a	k8xC	a
Poháru	pohár	k1gInSc2	pohár
osvoboditelů	osvoboditel	k1gMnPc2	osvoboditel
(	(	kIx(	(
<g/>
jihoamerické	jihoamerický	k2eAgFnSc2d1	jihoamerická
obdoby	obdoba	k1gFnSc2	obdoba
LM	LM	kA	LM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1960	[number]	k4	1960
až	až	k9	až
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
ho	on	k3xPp3gInSc4	on
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
klubů	klub	k1gInPc2	klub
pořádané	pořádaný	k2eAgNnSc1d1	pořádané
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
FIFA	FIFA	kA	FIFA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ženském	ženský	k2eAgInSc6d1	ženský
fotbale	fotbal	k1gInSc6	fotbal
UEFA	UEFA	kA	UEFA
pořádá	pořádat	k5eAaImIp3nS	pořádat
Ligu	liga	k1gFnSc4	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
UEFA	UEFA	kA	UEFA
pořádá	pořádat	k5eAaImIp3nS	pořádat
Juniorskou	juniorský	k2eAgFnSc4d1	juniorská
ligu	liga	k1gFnSc4	liga
UEFA	UEFA	kA	UEFA
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
Liga	liga	k1gFnSc1	liga
mistrů	mistr	k1gMnPc2	mistr
pro	pro	k7c4	pro
akademie	akademie	k1gFnPc4	akademie
a	a	k8xC	a
rezervy	rezerva	k1gFnPc4	rezerva
do	do	k7c2	do
19	[number]	k4	19
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
klubovou	klubový	k2eAgFnSc7d1	klubová
futsalovou	futsalův	k2eAgFnSc7d1	futsalův
soutěží	soutěž	k1gFnSc7	soutěž
je	být	k5eAaImIp3nS	být
Pohár	pohár	k1gInSc1	pohár
UEFA	UEFA	kA	UEFA
ve	v	k7c6	v
futsale	futsala	k1gFnSc6	futsala
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
nahradil	nahradit	k5eAaPmAgInS	nahradit
Evropské	evropský	k2eAgNnSc4d1	Evropské
klubové	klubový	k2eAgNnSc4d1	klubové
mistrovství	mistrovství	k1gNnSc4	mistrovství
ve	v	k7c6	v
futsale	futsala	k1gFnSc6	futsala
(	(	kIx(	(
<g/>
založeno	založit	k5eAaPmNgNnS	založit
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
UEFA	UEFA	kA	UEFA
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
UEFA	UEFA	kA	UEFA
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
UEFA	UEFA	kA	UEFA
Koeficienty	koeficient	k1gInPc7	koeficient
UEFA	UEFA	kA	UEFA
-	-	kIx~	-
pořadí	pořadí	k1gNnSc1	pořadí
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
statistiky	statistika	k1gFnSc2	statistika
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc2d1	Evropská
ligy	liga	k1gFnSc2	liga
Evropské	evropský	k2eAgFnSc2d1	Evropská
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
počty	počet	k1gInPc7	počet
aneb	aneb	k?	aneb
Proč	proč	k6eAd1	proč
má	mít	k5eAaImIp3nS	mít
Česko	Česko	k1gNnSc1	Česko
v	v	k7c6	v
pohárech	pohár	k1gInPc6	pohár
jen	jen	k9	jen
čtyři	čtyři	k4xCgInPc1	čtyři
týmy	tým	k1gInPc1	tým
</s>
