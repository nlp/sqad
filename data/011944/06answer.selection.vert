<s>
Zéthos	Zéthos	k1gInSc1	Zéthos
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Zethus	Zethus	k1gInSc1	Zethus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
synů	syn	k1gMnPc2	syn
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
boha	bůh	k1gMnSc2	bůh
Dia	Dia	k1gMnSc2	Dia
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
milenky	milenka	k1gFnSc2	milenka
Antiopy	Antiopa	k1gFnSc2	Antiopa
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
thébského	thébský	k2eAgMnSc2d1	thébský
krále	král	k1gMnSc2	král
Nyktea	Nykteus	k1gMnSc2	Nykteus
<g/>
.	.	kIx.	.
</s>
