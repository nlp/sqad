<p>
<s>
Zéthos	Zéthos	k1gInSc1	Zéthos
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Zethus	Zethus	k1gInSc1	Zethus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
synů	syn	k1gMnPc2	syn
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
boha	bůh	k1gMnSc2	bůh
Dia	Dia	k1gMnSc2	Dia
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
milenky	milenka	k1gFnSc2	milenka
Antiopy	Antiopa	k1gFnSc2	Antiopa
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
thébského	thébský	k2eAgMnSc2d1	thébský
krále	král	k1gMnSc2	král
Nyktea	Nykteus	k1gMnSc2	Nykteus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Antiopa	Antiopa	k1gFnSc1	Antiopa
byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
krásná	krásný	k2eAgFnSc1d1	krásná
<g/>
,	,	kIx,	,
že	že	k8xS	že
upoutala	upoutat	k5eAaPmAgFnS	upoutat
nejvyššího	vysoký	k2eAgMnSc4d3	nejvyšší
boha	bůh	k1gMnSc4	bůh
Dia	Dia	k1gFnSc2	Dia
a	a	k8xC	a
tohoto	tento	k3xDgInSc2	tento
mileneckého	milenecký	k2eAgInSc2d1	milenecký
vztahu	vztah	k1gInSc2	vztah
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
bratři	bratr	k1gMnPc1	bratr
Amfíón	Amfíón	k1gMnSc1	Amfíón
a	a	k8xC	a
Zéthos	Zéthos	k1gMnSc1	Zéthos
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
před	před	k7c7	před
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
pohodila	pohodit	k5eAaPmAgFnS	pohodit
dvojčata	dvojče	k1gNnPc4	dvojče
v	v	k7c6	v
lese	les	k1gInSc6	les
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
pramenů	pramen	k1gInPc2	pramen
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
lese	les	k1gInSc6	les
nechal	nechat	k5eAaPmAgMnS	nechat
Antiopin	Antiopin	k1gMnSc1	Antiopin
strýc	strýc	k1gMnSc1	strýc
Lykos	Lykos	k1gMnSc1	Lykos
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
Dirké	Dirká	k1gFnSc2	Dirká
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Našel	najít	k5eAaPmAgMnS	najít
je	být	k5eAaImIp3nS	být
pastýř	pastýř	k1gMnSc1	pastýř
<g/>
,	,	kIx,	,
vzal	vzít	k5eAaPmAgMnS	vzít
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
sobě	se	k3xPyFc3	se
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
jim	on	k3xPp3gMnPc3	on
vychování	vychování	k1gNnSc3	vychování
i	i	k8xC	i
vzdělání	vzdělání	k1gNnSc3	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Vyrostli	vyrůst	k5eAaPmAgMnP	vyrůst
v	v	k7c4	v
urostlé	urostlý	k2eAgMnPc4d1	urostlý
mládence	mládenec	k1gMnPc4	mládenec
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
ale	ale	k9	ale
každý	každý	k3xTgInSc4	každý
jiný	jiný	k2eAgInSc4d1	jiný
<g/>
:	:	kIx,	:
Amfíón	Amfíón	k1gInSc1	Amfíón
byl	být	k5eAaImAgInS	být
jemný	jemný	k2eAgInSc1d1	jemný
a	a	k8xC	a
mírný	mírný	k2eAgInSc1d1	mírný
<g/>
,	,	kIx,	,
miloval	milovat	k5eAaImAgInS	milovat
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Zéthos	Zéthos	k1gInSc4	Zéthos
byl	být	k5eAaImAgMnS	být
silák	silák	k1gMnSc1	silák
<g/>
,	,	kIx,	,
miloval	milovat	k5eAaImAgMnS	milovat
lov	lov	k1gInSc4	lov
a	a	k8xC	a
zápas	zápas	k1gInSc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
žili	žít	k5eAaImAgMnP	žít
spokojeně	spokojeně	k6eAd1	spokojeně
spolu	spolu	k6eAd1	spolu
a	a	k8xC	a
nic	nic	k3yNnSc4	nic
netušili	tušit	k5eNaImAgMnP	tušit
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
původu	původ	k1gInSc6	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
krále	král	k1gMnSc2	král
Nyktea	Nykteum	k1gNnSc2	Nykteum
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
thébský	thébský	k2eAgInSc4d1	thébský
trůn	trůn	k1gInSc4	trůn
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Lykos	Lykos	k1gMnSc1	Lykos
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Dirké	Dirká	k1gFnSc2	Dirká
Antiopu	Antiop	k1gInSc2	Antiop
nenáviděla	nenávidět	k5eAaImAgFnS	nenávidět
a	a	k8xC	a
tak	tak	k9	tak
jí	on	k3xPp3gFnSc3	on
připravovala	připravovat	k5eAaImAgFnS	připravovat
nejhorší	zlý	k2eAgInPc4d3	Nejhorší
ústrky	ústrk	k1gInPc4	ústrk
a	a	k8xC	a
utrpení	utrpení	k1gNnSc4	utrpení
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
ji	on	k3xPp3gFnSc4	on
uvrhla	uvrhnout	k5eAaPmAgFnS	uvrhnout
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
jí	on	k3xPp3gFnSc3	on
Zeus	Zeus	k1gInSc1	Zeus
dopomohl	dopomoct	k5eAaPmAgInS	dopomoct
k	k	k7c3	k
útěku	útěk	k1gInSc3	útěk
do	do	k7c2	do
hor.	hor.	k?	hor.
</s>
</p>
<p>
<s>
Možná	možná	k9	možná
náhodou	náhodou	k6eAd1	náhodou
<g/>
,	,	kIx,	,
možná	možná	k9	možná
z	z	k7c2	z
božské	božský	k2eAgFnSc2d1	božská
vůle	vůle	k1gFnSc2	vůle
<g/>
,	,	kIx,	,
dostala	dostat	k5eAaPmAgFnS	dostat
se	se	k3xPyFc4	se
právě	právě	k9	právě
k	k	k7c3	k
pastýři	pastýř	k1gMnSc3	pastýř
a	a	k8xC	a
svým	svůj	k3xOyFgMnPc3	svůj
synům	syn	k1gMnPc3	syn
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
všechno	všechen	k3xTgNnSc4	všechen
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
Dirké	Dirká	k1gFnPc4	Dirká
<g/>
,	,	kIx,	,
v	v	k7c4	v
které	který	k3yRgMnPc4	který
vzplála	vzplát	k5eAaPmAgFnS	vzplát
stará	starý	k2eAgFnSc1d1	stará
nenávist	nenávist	k1gFnSc1	nenávist
a	a	k8xC	a
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ta	ten	k3xDgFnSc1	ten
žena	žena	k1gFnSc1	žena
je	být	k5eAaImIp3nS	být
uprchlá	uprchlý	k2eAgFnSc1d1	uprchlá
otrokyně	otrokyně	k1gFnSc1	otrokyně
<g/>
.	.	kIx.	.
</s>
<s>
Poručila	poručit	k5eAaPmAgFnS	poručit
bratrům	bratr	k1gMnPc3	bratr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ji	on	k3xPp3gFnSc4	on
chytili	chytit	k5eAaPmAgMnP	chytit
<g/>
,	,	kIx,	,
přivázali	přivázat	k5eAaPmAgMnP	přivázat
k	k	k7c3	k
rohům	roh	k1gInPc3	roh
býka	býk	k1gMnSc2	býk
a	a	k8xC	a
nechali	nechat	k5eAaPmAgMnP	nechat
ji	on	k3xPp3gFnSc4	on
usmýkat	usmýkat	k5eAaPmF	usmýkat
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
nechtěli	chtít	k5eNaImAgMnP	chtít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
museli	muset	k5eAaImAgMnP	muset
královnin	královnin	k2eAgInSc4d1	královnin
rozkaz	rozkaz	k1gInSc4	rozkaz
poslechnout	poslechnout	k5eAaPmF	poslechnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
promluvil	promluvit	k5eAaPmAgMnS	promluvit
pastýř	pastýř	k1gMnSc1	pastýř
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jejich	jejich	k3xOp3gFnSc1	jejich
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
v	v	k7c6	v
hněvu	hněv	k1gInSc6	hněv
nad	nad	k7c7	nad
připravovaným	připravovaný	k2eAgInSc7d1	připravovaný
zločinem	zločin	k1gInSc7	zločin
Dirké	Dirká	k1gFnSc2	Dirká
krutě	krutě	k6eAd1	krutě
potrestali	potrestat	k5eAaPmAgMnP	potrestat
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
ona	onen	k3xDgFnSc1	onen
připravovala	připravovat	k5eAaImAgFnS	připravovat
pro	pro	k7c4	pro
Antiopu	Antiopa	k1gFnSc4	Antiopa
–	–	k?	–
divoký	divoký	k2eAgMnSc1d1	divoký
býk	býk	k1gMnSc1	býk
ji	on	k3xPp3gFnSc4	on
usmýkal	usmýkat	k5eAaPmAgMnS	usmýkat
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Antiopa	Antiopa	k1gFnSc1	Antiopa
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
synové	syn	k1gMnPc1	syn
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
Théb	Théby	k1gFnPc2	Théby
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
domluvu	domluva	k1gFnSc4	domluva
od	od	k7c2	od
boha	bůh	k1gMnSc2	bůh
Herma	Hermes	k1gMnSc2	Hermes
upustili	upustit	k5eAaPmAgMnP	upustit
od	od	k7c2	od
potrestání	potrestání	k1gNnSc2	potrestání
krále	král	k1gMnSc2	král
Lyka	Lykus	k1gMnSc2	Lykus
<g/>
,	,	kIx,	,
darovali	darovat	k5eAaPmAgMnP	darovat
mu	on	k3xPp3gMnSc3	on
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
města	město	k1gNnSc2	město
ho	on	k3xPp3gMnSc4	on
vyhnali	vyhnat	k5eAaPmAgMnP	vyhnat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
zbavili	zbavit	k5eAaPmAgMnP	zbavit
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
zabili	zabít	k5eAaPmAgMnP	zabít
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
líčí	líčit	k5eAaImIp3nS	líčit
jiná	jiný	k2eAgFnSc1d1	jiná
verze	verze	k1gFnSc1	verze
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
ujali	ujmout	k5eAaPmAgMnP	ujmout
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
dali	dát	k5eAaPmAgMnP	dát
se	se	k3xPyFc4	se
do	do	k7c2	do
zvelebování	zvelebování	k1gNnSc2	zvelebování
města	město	k1gNnSc2	město
a	a	k8xC	a
do	do	k7c2	do
stavby	stavba	k1gFnSc2	stavba
bytelných	bytelný	k2eAgFnPc2d1	bytelná
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
se	se	k3xPyFc4	se
příkladně	příkladně	k6eAd1	příkladně
zapojili	zapojit	k5eAaPmAgMnP	zapojit
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
podle	podle	k7c2	podle
svých	svůj	k3xOyFgFnPc2	svůj
schopností	schopnost	k1gFnPc2	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Silák	silák	k1gMnSc1	silák
Zéthos	Zéthos	k1gMnSc1	Zéthos
nosil	nosit	k5eAaImAgMnS	nosit
těžké	těžký	k2eAgInPc4d1	těžký
balvany	balvan	k1gInPc4	balvan
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
(	(	kIx(	(
<g/>
prý	prý	k9	prý
<g/>
)	)	kIx)	)
nepohnuli	pohnout	k5eNaPmAgMnP	pohnout
ani	ani	k9	ani
Titáni	Titán	k1gMnPc1	Titán
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
budoval	budovat	k5eAaImAgMnS	budovat
vskutku	vskutku	k9	vskutku
nedobytné	dobytný	k2eNgFnPc4d1	nedobytná
hradby	hradba	k1gFnPc4	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Amfíón	Amfíón	k1gMnSc1	Amfíón
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
neoplýval	oplývat	k5eNaImAgMnS	oplývat
fyzickou	fyzický	k2eAgFnSc7d1	fyzická
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
vložil	vložit	k5eAaPmAgMnS	vložit
své	svůj	k3xOyFgNnSc4	svůj
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
nádherně	nádherně	k6eAd1	nádherně
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
zlatou	zlatý	k2eAgFnSc4d1	zlatá
lyru	lyra	k1gFnSc4	lyra
<g/>
,	,	kIx,	,
dar	dar	k1gInSc4	dar
boha	bůh	k1gMnSc2	bůh
Apollóna	Apollón	k1gMnSc2	Apollón
<g/>
,	,	kIx,	,
že	že	k8xS	že
kameny	kámen	k1gInPc1	kámen
poslušně	poslušně	k6eAd1	poslušně
vstávaly	vstávat	k5eAaImAgInP	vstávat
a	a	k8xC	a
ukládaly	ukládat	k5eAaImAgFnP	ukládat
se	se	k3xPyFc4	se
na	na	k7c4	na
svá	svůj	k3xOyFgNnPc4	svůj
místa	místo	k1gNnPc4	místo
do	do	k7c2	do
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Sláva	Sláva	k1gFnSc1	Sláva
a	a	k8xC	a
oblíbenost	oblíbenost	k1gFnSc1	oblíbenost
obou	dva	k4xCgMnPc2	dva
bratří	bratr	k1gMnPc2	bratr
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
daleko	daleko	k6eAd1	daleko
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
také	také	k9	také
měli	mít	k5eAaImAgMnP	mít
podobný	podobný	k2eAgInSc4d1	podobný
osud	osud	k1gInSc4	osud
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc2	jejich
manželky	manželka	k1gFnSc2	manželka
stihlo	stihnout	k5eAaPmAgNnS	stihnout
velké	velký	k2eAgNnSc1d1	velké
neštěstí	neštěstí	k1gNnSc1	neštěstí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zéthovou	Zéthový	k2eAgFnSc7d1	Zéthový
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
Aédón	Aédón	k1gInSc4	Aédón
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
nezkrotná	zkrotný	k2eNgFnSc1d1	nezkrotná
závist	závist	k1gFnSc1	závist
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
šílené	šílený	k2eAgFnSc6d1	šílená
zaslepenosti	zaslepenost	k1gFnSc6	zaslepenost
přivedla	přivést	k5eAaPmAgFnS	přivést
až	až	k9	až
k	k	k7c3	k
zabití	zabití	k1gNnSc3	zabití
synáčka	synáček	k1gMnSc2	synáček
Ityla	Ityl	k1gMnSc2	Ityl
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
potom	potom	k6eAd1	potom
srdceryvně	srdceryvně	k6eAd1	srdceryvně
naříkala	naříkat	k5eAaBmAgFnS	naříkat
<g/>
,	,	kIx,	,
Zeus	Zeus	k1gInSc1	Zeus
ji	on	k3xPp3gFnSc4	on
proměnil	proměnit	k5eAaPmAgInS	proměnit
ve	v	k7c4	v
slavíka	slavík	k1gInSc4	slavík
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
oplakává	oplakávat	k5eAaImIp3nS	oplakávat
a	a	k8xC	a
každé	každý	k3xTgNnSc1	každý
jaro	jaro	k1gNnSc1	jaro
ho	on	k3xPp3gMnSc4	on
žalostně	žalostně	k6eAd1	žalostně
volá	volat	k5eAaImIp3nS	volat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Amfíón	Amfíón	k1gMnSc1	Amfíón
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
Niobé	Niobý	k2eAgFnPc1d1	Niobý
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
sipylského	sipylský	k2eAgMnSc2d1	sipylský
krále	král	k1gMnSc2	král
Tantala	Tantal	k1gMnSc2	Tantal
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
sedm	sedm	k4xCc4	sedm
synů	syn	k1gMnPc2	syn
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
dcer	dcera	k1gFnPc2	dcera
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechny	všechen	k3xTgFnPc4	všechen
jim	on	k3xPp3gMnPc3	on
je	on	k3xPp3gFnPc4	on
zahubili	zahubit	k5eAaPmAgMnP	zahubit
bůh	bůh	k1gMnSc1	bůh
Apollón	Apollón	k1gMnSc1	Apollón
a	a	k8xC	a
bohyně	bohyně	k1gFnSc1	bohyně
Artemis	Artemis	k1gFnSc1	Artemis
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc1	jejich
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
rouhala	rouhat	k5eAaImAgFnS	rouhat
a	a	k8xC	a
vyvyšovala	vyvyšovat	k5eAaImAgFnS	vyvyšovat
nad	nad	k7c4	nad
samotnou	samotný	k2eAgFnSc4d1	samotná
bohyni	bohyně	k1gFnSc4	bohyně
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
Niobé	Niobý	k2eAgNnSc4d1	Niobé
zkameněla	zkamenět	k5eAaPmAgFnS	zkamenět
v	v	k7c4	v
sochu	socha	k1gFnSc4	socha
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnPc4	jejíž
oči	oko	k1gNnPc4	oko
stále	stále	k6eAd1	stále
roní	ronit	k5eAaImIp3nS	ronit
slzy	slza	k1gFnPc4	slza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Král	Král	k1gMnSc1	Král
Amfíón	Amfíón	k1gMnSc1	Amfíón
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Zéthem	Zéth	k1gInSc7	Zéth
napadli	napadnout	k5eAaPmAgMnP	napadnout
Apollónovu	Apollónův	k2eAgFnSc4d1	Apollónova
svatyni	svatyně	k1gFnSc4	svatyně
v	v	k7c6	v
Delfách	Delfy	k1gFnPc6	Delfy
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Apollón	Apollón	k1gMnSc1	Apollón
jejich	jejich	k3xOp3gInSc4	jejich
útok	útok	k1gInSc4	útok
odrazil	odrazit	k5eAaPmAgMnS	odrazit
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
proklál	proklát	k5eAaPmAgInS	proklát
svými	svůj	k3xOyFgInPc7	svůj
šípy	šíp	k1gInPc7	šíp
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Opět	opět	k6eAd1	opět
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
jiná	jiný	k2eAgFnSc1d1	jiná
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svých	svůj	k3xOyFgFnPc2	svůj
dětí	dítě	k1gFnPc2	dítě
i	i	k8xC	i
manželky	manželka	k1gFnSc2	manželka
Amfíón	Amfíón	k1gMnSc1	Amfíón
sám	sám	k3xTgMnSc1	sám
proklál	proklát	k5eAaPmAgMnS	proklát
mečem	meč	k1gInSc7	meč
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Slovník	slovník	k1gInSc1	slovník
antické	antický	k2eAgFnSc2d1	antická
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
nakl	naknout	k5eAaPmAgMnS	naknout
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
<g/>
,	,	kIx,	,
Bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
hrdinové	hrdina	k1gMnPc1	hrdina
antických	antický	k2eAgFnPc2d1	antická
bájí	báj	k1gFnPc2	báj
</s>
</p>
<p>
<s>
Graves	Graves	k1gMnSc1	Graves
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
Řecké	řecký	k2eAgInPc4d1	řecký
mýty	mýtus	k1gInPc4	mýtus
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7309-153-4	[number]	k4	80-7309-153-4
</s>
</p>
<p>
<s>
Houtzager	Houtzager	k1gInSc1	Houtzager
<g/>
,	,	kIx,	,
Guus	Guus	k1gInSc1	Guus
<g/>
,	,	kIx,	,
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7234-287-8	[number]	k4	80-7234-287-8
</s>
</p>
<p>
<s>
Gerhard	Gerhard	k1gMnSc1	Gerhard
Löwe	Löw	k1gInSc2	Löw
<g/>
,	,	kIx,	,
Heindrich	Heindrich	k1gMnSc1	Heindrich
Alexander	Alexandra	k1gFnPc2	Alexandra
Stoll	Stoll	k1gMnSc1	Stoll
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
Antiky	antika	k1gFnSc2	antika
</s>
</p>
