<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Tomášek	Tomášek	k1gMnSc1	Tomášek
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
československý	československý	k2eAgMnSc1d1	československý
sportovec	sportovec	k1gMnSc1	sportovec
<g/>
,	,	kIx,	,
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
specializoval	specializovat	k5eAaBmAgMnS	specializovat
na	na	k7c4	na
skok	skok	k1gInSc4	skok
o	o	k7c6	o
tyči	tyč	k1gFnSc6	tyč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
československý	československý	k2eAgMnSc1d1	československý
tyčkař	tyčkař	k1gMnSc1	tyčkař
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
překonal	překonat	k5eAaPmAgMnS	překonat
450	[number]	k4	450
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
skončil	skončit	k5eAaPmAgInS	skončit
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
na	na	k7c6	na
osmém	osmý	k4xOgInSc6	osmý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
v	v	k7c6	v
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
na	na	k7c4	na
mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
(	(	kIx(	(
<g/>
460	[number]	k4	460
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
historicky	historicky	k6eAd1	historicky
prvním	první	k4xOgMnSc7	první
vítězem	vítěz	k1gMnSc7	vítěz
ankety	anketa	k1gFnSc2	anketa
Atlet	atlet	k1gMnSc1	atlet
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
obsadil	obsadit	k5eAaPmAgMnS	obsadit
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
výkonem	výkon	k1gInSc7	výkon
490	[number]	k4	490
cm	cm	kA	cm
šesté	šestý	k4xOgNnSc1	šestý
místo	místo	k1gNnSc1	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
Dortmundu	Dortmund	k1gInSc6	Dortmund
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
ročníku	ročník	k1gInSc6	ročník
Evropských	evropský	k2eAgFnPc2d1	Evropská
halových	halový	k2eAgFnPc2d1	halová
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
předchůdce	předchůdce	k1gMnSc2	předchůdce
Halového	halový	k2eAgNnSc2d1	halové
mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1964	[number]	k4	1964
v	v	k7c6	v
norském	norský	k2eAgNnSc6d1	norské
Oslo	Oslo	k1gNnSc6	Oslo
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
překonat	překonat	k5eAaPmF	překonat
jako	jako	k9	jako
prvnímu	první	k4xOgNnSc3	první
tyčkaři	tyčkař	k1gMnSc6	tyčkař
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
pětimetrovou	pětimetrový	k2eAgFnSc4d1	pětimetrová
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rudolf	Rudolf	k1gMnSc1	Rudolf
Tomášek	Tomáška	k1gFnPc2	Tomáška
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
www.sport-reference.com	www.sporteference.com	k1gInSc4	www.sport-reference.com
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c4	na
iaaf	iaaf	k1gInSc4	iaaf
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c4	o
Rudolfu	Rudolfa	k1gFnSc4	Rudolfa
Tomáškovi	Tomášek	k1gMnSc3	Tomášek
</s>
</p>
