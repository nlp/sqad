<s>
Letohrádek	letohrádek	k1gInSc1	letohrádek
Hvězda	hvězda	k1gFnSc1	hvězda
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Schloss	Schloss	k1gInSc1	Schloss
Stern	sternum	k1gNnPc2	sternum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
renesanční	renesanční	k2eAgInSc4d1	renesanční
letohrádek	letohrádek	k1gInSc4	letohrádek
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgFnSc4d1	stojící
ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
oboře	obora	k1gFnSc6	obora
v	v	k7c6	v
Praze-Liboci	Praze-Liboec	k1gInSc6	Praze-Liboec
<g/>
,	,	kIx,	,
vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
asi	asi	k9	asi
7	[number]	k4	7
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
středu	střed	k1gInSc2	střed
města	město	k1gNnSc2	město
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Praha	Praha	k1gFnSc1	Praha
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
