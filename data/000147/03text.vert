<s>
Letohrádek	letohrádek	k1gInSc1	letohrádek
Hvězda	hvězda	k1gFnSc1	hvězda
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Schloss	Schloss	k1gInSc1	Schloss
Stern	sternum	k1gNnPc2	sternum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
renesanční	renesanční	k2eAgInSc4d1	renesanční
letohrádek	letohrádek	k1gInSc4	letohrádek
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgFnSc4d1	stojící
ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
oboře	obora	k1gFnSc6	obora
v	v	k7c6	v
Praze-Liboci	Praze-Liboec	k1gInSc6	Praze-Liboec
<g/>
,	,	kIx,	,
vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
asi	asi	k9	asi
7	[number]	k4	7
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
středu	střed	k1gInSc2	střed
města	město	k1gNnSc2	město
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Praha	Praha	k1gFnSc1	Praha
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
originálním	originální	k2eAgInSc7d1	originální
příkladem	příklad	k1gInSc7	příklad
české	český	k2eAgFnSc2d1	Česká
renesanční	renesanční	k2eAgFnSc2d1	renesanční
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Oboru	obor	k1gInSc3	obor
Hvězda	hvězda	k1gFnSc1	hvězda
v	v	k7c6	v
lese	les	k1gInSc6	les
Malejově	Malejův	k2eAgInSc6d1	Malejův
nad	nad	k7c7	nad
vsí	ves	k1gFnSc7	ves
Liboc	Liboc	k1gFnSc4	Liboc
zřídil	zřídit	k5eAaPmAgMnS	zřídit
roku	rok	k1gInSc2	rok
1530	[number]	k4	1530
král	král	k1gMnSc1	král
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ji	on	k3xPp3gFnSc4	on
později	pozdě	k6eAd2	pozdě
nechal	nechat	k5eAaPmAgInS	nechat
obehnat	obehnat	k5eAaPmF	obehnat
zdí	zeď	k1gFnSc7	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
ještě	ještě	k9	ještě
zvána	zvát	k5eAaImNgFnS	zvát
prostě	prostě	k9	prostě
Nová	nový	k2eAgFnSc1d1	nová
královská	královský	k2eAgFnSc1d1	královská
obora	obora	k1gFnSc1	obora
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
králova	králův	k2eAgMnSc2d1	králův
mladšího	mladý	k2eAgMnSc2d2	mladší
syna	syn	k1gMnSc2	syn
rakouského	rakouský	k2eAgMnSc2d1	rakouský
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Tyrolského	tyrolský	k2eAgMnSc2d1	tyrolský
<g/>
,	,	kIx,	,
dlouholetého	dlouholetý	k2eAgMnSc2d1	dlouholetý
místodržícího	místodržící	k1gMnSc2	místodržící
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
pak	pak	k9	pak
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1555	[number]	k4	1555
až	až	k9	až
1558	[number]	k4	1558
postaven	postaven	k2eAgInSc4d1	postaven
letohrádek	letohrádek	k1gInSc4	letohrádek
Hvězda	Hvězda	k1gMnSc1	Hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Tyrolský	tyrolský	k2eAgMnSc1d1	tyrolský
sám	sám	k3xTgMnSc1	sám
položil	položit	k5eAaPmAgMnS	položit
základní	základní	k2eAgInSc4d1	základní
kámen	kámen	k1gInSc4	kámen
k	k	k7c3	k
letohrádku	letohrádek	k1gInSc3	letohrádek
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1555	[number]	k4	1555
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
prováděli	provádět	k5eAaImAgMnP	provádět
nejprve	nejprve	k6eAd1	nejprve
Juan	Juan	k1gMnSc1	Juan
Maria	Mario	k1gMnSc2	Mario
Avostalis	Avostalis	k1gFnSc2	Avostalis
del	del	k?	del
Pambio	Pambio	k1gMnSc1	Pambio
a	a	k8xC	a
Giovanni	Giovann	k1gMnPc1	Giovann
Lucchese	Lucchese	k1gFnSc2	Lucchese
<g/>
,	,	kIx,	,
po	po	k7c6	po
nich	on	k3xPp3gMnPc6	on
Hans	Hans	k1gMnSc1	Hans
Tirol	Tirol	k1gInSc1	Tirol
a	a	k8xC	a
Bonifác	Bonifác	k1gMnSc1	Bonifác
Wolmut	Wolmut	k1gMnSc1	Wolmut
<g/>
.	.	kIx.	.
</s>
<s>
Dvoupatrový	dvoupatrový	k2eAgInSc1d1	dvoupatrový
letohrádek	letohrádek	k1gInSc1	letohrádek
na	na	k7c6	na
unikátním	unikátní	k2eAgInSc6d1	unikátní
půdorysu	půdorys	k1gInSc6	půdorys
šesticípé	šesticípý	k2eAgFnPc4d1	šesticípá
hvězdy	hvězda	k1gFnPc4	hvězda
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
kterému	který	k3yQgMnSc3	který
získal	získat	k5eAaPmAgMnS	získat
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
tzv.	tzv.	kA	tzv.
filosofických	filosofický	k2eAgFnPc2d1	filosofická
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
uvnitř	uvnitř	k6eAd1	uvnitř
vyniká	vynikat	k5eAaImIp3nS	vynikat
bohatou	bohatý	k2eAgFnSc7d1	bohatá
štukovou	štukový	k2eAgFnSc7d1	štuková
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
.	.	kIx.	.
</s>
<s>
Letohrádek	letohrádek	k1gInSc1	letohrádek
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
přestavěn	přestavět	k5eAaPmNgInS	přestavět
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
Muzeum	muzeum	k1gNnSc1	muzeum
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
a	a	k8xC	a
M.	M.	kA	M.
Aleše	Aleš	k1gMnSc2	Aleš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
národních	národní	k2eAgFnPc2d1	národní
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
zpřístupněn	zpřístupněn	k2eAgInSc1d1	zpřístupněn
veřejnosti	veřejnost	k1gFnSc3	veřejnost
a	a	k8xC	a
uvnitř	uvnitř	k6eAd1	uvnitř
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
expozice	expozice	k1gFnSc1	expozice
o	o	k7c6	o
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
obory	obora	k1gFnSc2	obora
<g/>
.	.	kIx.	.
</s>
