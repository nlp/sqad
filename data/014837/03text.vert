<s>
Jaroslav	Jaroslav	k1gMnSc1
Brož	Brož	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
římskokatolickém	římskokatolický	k2eAgMnSc6d1
knězi	kněz	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Jaroslav	Jaroslav	k1gMnSc1
Brož	Brož	k1gMnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1
MilostThLic	MilostThLic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslav	Jaroslav	k1gMnSc1
Brož	Brož	k1gMnSc1
SSLsídelní	SSLsídelní	k2eAgMnSc1d1
kanovník	kanovník	k1gMnSc1
Kolegiátní	kolegiátní	k2eAgFnSc2d1
kapituly	kapitula	k1gFnSc2
Všech	všecek	k3xTgFnPc2
svatých	svatá	k1gFnPc2
na	na	k7c6
Hradě	hrad	k1gInSc6
pražském	pražský	k2eAgInSc6d1
Církev	církev	k1gFnSc4
</s>
<s>
římskokatolická	římskokatolický	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
česká	český	k2eAgFnSc1d1
Diecéze	diecéze	k1gFnSc1
</s>
<s>
královéhradecká	královéhradecký	k2eAgNnPc1d1
Svěcení	svěcení	k1gNnPc1
Kněžské	kněžský	k2eAgFnSc2d1
svěcení	svěcení	k1gNnPc5
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1988	#num#	k4
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
Osobní	osobní	k2eAgInPc4d1
údaje	údaj	k1gInPc4
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1963	#num#	k4
(	(	kIx(
<g/>
58	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Náchod	Náchod	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
kněz	kněz	k1gMnSc1
<g/>
,	,	kIx,
pedagog	pedagog	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Příbuzenstvo	příbuzenstvo	k1gNnSc1
</s>
<s>
bratr	bratr	k1gMnSc1
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Brož	Brož	k1gMnSc1
</s>
<s>
bratr	bratr	k1gMnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Brož	Brož	k1gMnSc1
</s>
<s>
bratr	bratr	k1gMnSc1
</s>
<s>
Prokop	Prokop	k1gMnSc1
Brož	Brož	k1gMnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Brož	Brož	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1963	#num#	k4
Náchod	Náchod	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
římskokatolický	římskokatolický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
<g/>
,	,	kIx,
biblista-novozákoník	biblista-novozákoník	k1gMnSc1
a	a	k8xC
nynější	nynější	k2eAgMnSc1d1
proděkan	proděkan	k1gMnSc1
Katolické	katolický	k2eAgFnSc2d1
teologické	teologický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
pro	pro	k7c4
studium	studium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
sedmi	sedm	k4xCc2
sourozenců	sourozenec	k1gMnPc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc7
bratrem	bratr	k1gMnSc7
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
děkan	děkan	k1gMnSc1
Katolické	katolický	k2eAgFnSc2d1
teologické	teologický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
ThLic	ThLic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prokop	Prokop	k1gMnSc1
Brož	Brož	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
rodina	rodina	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
Červeného	Červeného	k2eAgInSc2d1
Kostelce	Kostelec	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
absolvoval	absolvovat	k5eAaPmAgInS
gymnázium	gymnázium	k1gNnSc4
v	v	k7c6
Trutnově	Trutnov	k1gInSc6
a	a	k8xC
přihlásil	přihlásit	k5eAaPmAgMnS
se	se	k3xPyFc4
ke	k	k7c3
studiu	studio	k1gNnSc3
na	na	k7c6
Cyrilometodějské	cyrilometodějský	k2eAgFnSc6d1
bohoslovecké	bohoslovecký	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
v	v	k7c6
Litoměřicích	Litoměřice	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studium	studium	k1gNnSc1
úspěšně	úspěšně	k6eAd1
ukončil	ukončit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
a	a	k8xC
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1988	#num#	k4
byl	být	k5eAaImAgInS
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
vysvěcen	vysvětit	k5eAaPmNgInS
na	na	k7c4
kněze	kněz	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Poté	poté	k6eAd1
působil	působit	k5eAaImAgMnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
v	v	k7c6
kněžské	kněžský	k2eAgFnSc6d1
službě	služba	k1gFnSc6
v	v	k7c6
královéhradecké	královéhradecký	k2eAgFnSc6d1
diecézi	diecéze	k1gFnSc6
<g/>
,	,	kIx,
naposledy	naposledy	k6eAd1
jako	jako	k9
duchovní	duchovní	k1gMnSc1
správce	správce	k1gMnSc1
v	v	k7c6
Krucemburku	Krucemburk	k1gInSc6
(	(	kIx(
<g/>
tehdejší	tehdejší	k2eAgFnSc2d1
Křížové	Křížová	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
byl	být	k5eAaImAgInS
s	s	k7c7
přestávkami	přestávka	k1gFnPc7
také	také	k9
odborným	odborný	k2eAgMnSc7d1
asistentem	asistent	k1gMnSc7
Katolické	katolický	k2eAgFnSc2d1
teologické	teologický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
na	na	k7c6
ní	on	k3xPp3gFnSc6
získal	získat	k5eAaPmAgMnS
licenciát	licenciát	k1gMnSc1
teologie	teologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1994	#num#	k4
až	až	k9
1997	#num#	k4
studoval	studovat	k5eAaImAgInS
v	v	k7c6
Římě	Řím	k1gInSc6
na	na	k7c6
Papežském	papežský	k2eAgInSc6d1
biblickém	biblický	k2eAgInSc6d1
institutu	institut	k1gInSc6
(	(	kIx(
<g/>
Pontificium	Pontificium	k1gNnSc1
Institutum	Institutum	k1gNnSc1
Biblicum	Biblicum	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
toto	tento	k3xDgNnSc4
studium	studium	k1gNnSc4
zakončil	zakončit	k5eAaPmAgInS
licenciátem	licenciát	k1gInSc7
biblických	biblický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
(	(	kIx(
<g/>
SSL	SSL	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1997	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
absolvoval	absolvovat	k5eAaPmAgInS
doktorské	doktorský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
na	na	k7c6
Univerzitě	univerzita	k1gFnSc6
Karlově	Karlův	k2eAgFnSc6d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
čestným	čestný	k2eAgMnSc7d1
kanovníkem	kanovník	k1gMnSc7
Katedrální	katedrální	k2eAgFnSc2d1
kapituly	kapitula	k1gFnSc2
při	při	k7c6
chrámu	chrám	k1gInSc6
Svatého	svatý	k2eAgMnSc2d1
Ducha	duch	k1gMnSc2
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
a	a	k8xC
dne	den	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2010	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
také	také	k6eAd1
sídelním	sídelní	k2eAgMnSc7d1
kanovníkem	kanovník	k1gMnSc7
Kolegiátní	kolegiátní	k2eAgFnSc2d1
kapituly	kapitula	k1gFnSc2
Všech	všecek	k3xTgFnPc2
svatých	svatá	k1gFnPc2
na	na	k7c6
Hradě	hrad	k1gInSc6
pražském	pražský	k2eAgInSc6d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
únoru	únor	k1gInSc6
2011	#num#	k4
byl	být	k5eAaImAgInS
zmiňován	zmiňovat	k5eAaImNgInS
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
výběrem	výběr	k1gInSc7
nového	nový	k2eAgMnSc2d1
biskupa	biskup	k1gMnSc2
královéhradecké	královéhradecký	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
,	,	kIx,
jímž	jenž	k3xRgNnSc7
se	se	k3xPyFc4
však	však	k9
nakonec	nakonec	k6eAd1
stal	stát	k5eAaPmAgMnS
Jan	Jan	k1gMnSc1
Vokál	vokál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
slouží	sloužit	k5eAaImIp3nS
Jaroslav	Jaroslav	k1gMnSc1
Brož	Brož	k1gMnSc1
také	také	k9
jako	jako	k9
tzv.	tzv.	kA
externí	externí	k2eAgInSc4d1
spirituál	spirituál	k1gInSc4
(	(	kIx(
<g/>
vedle	vedle	k7c2
stávajícího	stávající	k2eAgMnSc2d1
spirituála	spirituál	k1gMnSc2
<g/>
,	,	kIx,
Mons	Mons	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josefa	Josef	k1gMnSc2
Žáka	Žák	k1gMnSc2
<g/>
)	)	kIx)
v	v	k7c6
pražském	pražský	k2eAgInSc6d1
kněžském	kněžský	k2eAgInSc6d1
semináři	seminář	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2018	#num#	k4
byl	být	k5eAaImAgInS
pak	pak	k6eAd1
jmenován	jmenovat	k5eAaBmNgInS,k5eAaImNgInS
sídelním	sídelní	k2eAgMnSc7d1
kanovníkem	kanovník	k1gMnSc7
Vyšehradské	vyšehradský	k2eAgFnSc2d1
kapituly	kapitula	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jaroslav	Jaroslav	k1gMnSc1
Brož	brož	k1gFnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Jaroslav	Jaroslav	k1gMnSc1
Brož	Brož	k1gMnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Brož	Brož	k1gMnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
Katolické	katolický	k2eAgFnSc2d1
teologické	teologický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
</s>
<s>
Rozhovor	rozhovor	k1gInSc1
<g/>
:	:	kIx,
Bratři	bratr	k1gMnPc1
Brožové	Brožová	k1gFnSc2
aneb	aneb	k?
čtyřnásobné	čtyřnásobný	k2eAgNnSc1d1
kněžské	kněžský	k2eAgNnSc1d1
povolání	povolání	k1gNnSc1
v	v	k7c6
jedné	jeden	k4xCgFnSc6
rodině	rodina	k1gFnSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
skuk	skuk	k1gInSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
174	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
85151454	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Bible	bible	k1gFnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
