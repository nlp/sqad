<s>
Vydra	vydra	k1gFnSc1	vydra
říční	říční	k2eAgFnSc1d1	říční
(	(	kIx(	(
<g/>
Lutra	Lutra	k1gFnSc1	Lutra
lutra	lutra	k1gFnSc1	lutra
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
druhem	druh	k1gInSc7	druh
vydry	vydra	k1gFnSc2	vydra
vyskytujícím	vyskytující	k2eAgInPc3d1	vyskytující
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
je	být	k5eAaImIp3nS	být
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
spatřit	spatřit	k5eAaPmF	spatřit
poblíž	poblíž	k7c2	poblíž
stojatých	stojatý	k2eAgFnPc2d1	stojatá
i	i	k8xC	i
tekoucích	tekoucí	k2eAgFnPc2d1	tekoucí
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
až	až	k9	až
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
nebo	nebo	k8xC	nebo
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
však	však	k9	však
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
sladké	sladký	k2eAgFnSc3d1	sladká
vodě	voda	k1gFnSc3	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
57	[number]	k4	57
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
ocasu	ocas	k1gInSc2	ocas
<g/>
:	:	kIx,	:
27	[number]	k4	27
<g/>
–	–	k?	–
<g/>
55	[number]	k4	55
cmVydra	cmVydra	k1gFnSc1	cmVydra
říční	říční	k2eAgFnSc2d1	říční
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
lasicovitá	lasicovitý	k2eAgFnSc1d1	lasicovitá
šelma	šelma	k1gFnSc1	šelma
s	s	k7c7	s
protáhlým	protáhlý	k2eAgNnSc7d1	protáhlé
<g/>
,	,	kIx,	,
štíhlým	štíhlý	k2eAgNnSc7d1	štíhlé
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
krátkými	krátký	k2eAgFnPc7d1	krátká
končetinami	končetina	k1gFnPc7	končetina
opatřenými	opatřený	k2eAgFnPc7d1	opatřená
plovacími	plovací	k2eAgFnPc7d1	plovací
blánami	blána	k1gFnPc7	blána
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
svalnatý	svalnatý	k2eAgInSc1d1	svalnatý
<g/>
,	,	kIx,	,
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
zúžený	zúžený	k2eAgMnSc1d1	zúžený
<g/>
.	.	kIx.	.
</s>
<s>
Nozdry	nozdra	k1gFnPc1	nozdra
a	a	k8xC	a
ušní	ušní	k2eAgInPc1d1	ušní
otvory	otvor	k1gInPc1	otvor
jsou	být	k5eAaImIp3nP	být
vybaveny	vybavit	k5eAaPmNgInP	vybavit
kožními	kožní	k2eAgInPc7d1	kožní
záhyby	záhyb	k1gInPc7	záhyb
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
vydře	vydře	k1gNnSc4	vydře
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
je	on	k3xPp3gNnSc4	on
při	při	k7c6	při
ponoru	ponor	k1gInSc6	ponor
zcela	zcela	k6eAd1	zcela
uzavřít	uzavřít	k5eAaPmF	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
krátkou	krátká	k1gFnSc4	krátká
světle	světle	k6eAd1	světle
až	až	k9	až
tmavě	tmavě	k6eAd1	tmavě
hnědou	hnědý	k2eAgFnSc4d1	hnědá
srst	srst	k1gFnSc4	srst
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
spodinou	spodina	k1gFnSc7	spodina
<g/>
.	.	kIx.	.
</s>
<s>
Pohlaví	pohlaví	k1gNnSc1	pohlaví
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
zbarvením	zbarvení	k1gNnSc7	zbarvení
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Vydra	vydra	k1gFnSc1	vydra
říční	říční	k2eAgNnSc1d1	říční
žije	žít	k5eAaImIp3nS	žít
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
samotářským	samotářský	k2eAgInSc7d1	samotářský
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
mnohdy	mnohdy	k6eAd1	mnohdy
větším	většit	k5eAaImIp1nS	většit
jak	jak	k6eAd1	jak
30	[number]	k4	30
km	km	kA	km
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
si	se	k3xPyFc3	se
značí	značit	k5eAaImIp3nS	značit
trusem	trus	k1gInSc7	trus
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
teritoria	teritorium	k1gNnSc2	teritorium
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
a	a	k8xC	a
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
kvalitě	kvalita	k1gFnSc6	kvalita
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
teritorium	teritorium	k1gNnSc4	teritorium
si	se	k3xPyFc3	se
však	však	k9	však
střeží	střežit	k5eAaImIp3nS	střežit
proti	proti	k7c3	proti
jedincům	jedinec	k1gMnPc3	jedinec
stejného	stejný	k2eAgNnSc2d1	stejné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nám	my	k3xPp1nPc3	my
často	často	k6eAd1	často
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dvě	dva	k4xCgNnPc1	dva
rozdílná	rozdílný	k2eAgNnPc1d1	rozdílné
teritoria	teritorium	k1gNnPc1	teritorium
jedinců	jedinec	k1gMnPc2	jedinec
různého	různý	k2eAgNnSc2d1	různé
pohlaví	pohlaví	k1gNnSc2	pohlaví
překrývají	překrývat	k5eAaImIp3nP	překrývat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vydra	Vydra	k1gMnSc1	Vydra
je	být	k5eAaImIp3nS	být
skvělý	skvělý	k2eAgMnSc1d1	skvělý
plavec	plavec	k1gMnSc1	plavec
a	a	k8xC	a
potápěč	potápěč	k1gMnSc1	potápěč
a	a	k8xC	a
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
může	moct	k5eAaImIp3nS	moct
vydržet	vydržet	k5eAaPmF	vydržet
i	i	k9	i
déle	dlouho	k6eAd2	dlouho
jak	jak	k8xS	jak
5	[number]	k4	5
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
může	moct	k5eAaImIp3nS	moct
uplavat	uplavat	k5eAaPmF	uplavat
až	až	k9	až
400	[number]	k4	400
m.	m.	k?	m.
Na	na	k7c4	na
lov	lov	k1gInSc4	lov
se	se	k3xPyFc4	se
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
lokalit	lokalita	k1gFnPc2	lokalita
vydává	vydávat	k5eAaPmIp3nS	vydávat
až	až	k9	až
v	v	k7c6	v
pozdní	pozdní	k2eAgFnSc6d1	pozdní
části	část	k1gFnSc6	část
odpoledne	odpoledne	k1gNnSc2	odpoledne
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
není	být	k5eNaImIp3nS	být
sluneční	sluneční	k2eAgNnSc1d1	sluneční
záření	záření	k1gNnSc1	záření
tak	tak	k6eAd1	tak
silné	silný	k2eAgNnSc1d1	silné
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrabává	vyhrabávat	k5eAaImIp3nS	vyhrabávat
si	se	k3xPyFc3	se
poměrně	poměrně	k6eAd1	poměrně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
noru	nora	k1gFnSc4	nora
s	s	k7c7	s
doupětem	doupě	k1gNnSc7	doupě
v	v	k7c6	v
hlinitých	hlinitý	k2eAgInPc6d1	hlinitý
březích	břeh	k1gInPc6	břeh
nebo	nebo	k8xC	nebo
pod	pod	k7c4	pod
kořeny	kořen	k1gInPc4	kořen
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
do	do	k7c2	do
ni	on	k3xPp3gFnSc4	on
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
vchod	vchod	k1gInSc1	vchod
pod	pod	k7c7	pod
vodní	vodní	k2eAgFnSc7d1	vodní
hladinou	hladina	k1gFnSc7	hladina
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
nepřístupnějším	přístupný	k2eNgMnSc6d2	nepřístupnější
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
vydřích	vydří	k2eAgMnPc2d1	vydří
predátorů	predátor	k1gMnPc2	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
zkonzumuje	zkonzumovat	k5eAaPmIp3nS	zkonzumovat
vydra	vydra	k1gFnSc1	vydra
říční	říční	k2eAgFnSc1d1	říční
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
především	především	k9	především
vodními	vodní	k2eAgMnPc7d1	vodní
živočichy	živočich	k1gMnPc7	živočich
<g/>
,	,	kIx,	,
zvláště	zvláště	k9	zvláště
pak	pak	k6eAd1	pak
rybami	ryba	k1gFnPc7	ryba
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
i	i	k9	i
měkkýši	měkkýš	k1gMnPc7	měkkýš
<g/>
,	,	kIx,	,
obojživelníky	obojživelník	k1gMnPc7	obojživelník
<g/>
,	,	kIx,	,
drobnými	drobný	k2eAgMnPc7d1	drobný
savci	savec	k1gMnPc7	savec
nebo	nebo	k8xC	nebo
mladými	mladý	k2eAgMnPc7d1	mladý
vodními	vodní	k2eAgMnPc7d1	vodní
ptáky	pták	k1gMnPc7	pták
<g/>
.	.	kIx.	.
</s>
