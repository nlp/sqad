<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
především	především	k9	především
vodními	vodní	k2eAgMnPc7d1	vodní
živočichy	živočich	k1gMnPc7	živočich
<g/>
,	,	kIx,	,
zvláště	zvláště	k9	zvláště
pak	pak	k6eAd1	pak
rybami	ryba	k1gFnPc7	ryba
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
i	i	k9	i
měkkýši	měkkýš	k1gMnPc7	měkkýš
<g/>
,	,	kIx,	,
obojživelníky	obojživelník	k1gMnPc7	obojživelník
<g/>
,	,	kIx,	,
drobnými	drobný	k2eAgMnPc7d1	drobný
savci	savec	k1gMnPc7	savec
nebo	nebo	k8xC	nebo
mladými	mladý	k2eAgMnPc7d1	mladý
vodními	vodní	k2eAgMnPc7d1	vodní
ptáky	pták	k1gMnPc7	pták
<g/>
.	.	kIx.	.
</s>
