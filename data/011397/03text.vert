<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
patagonský	patagonský	k2eAgMnSc1d1	patagonský
(	(	kIx(	(
<g/>
Aptenodytes	Aptenodytes	k1gMnSc1	Aptenodytes
patagonicus	patagonicus	k1gMnSc1	patagonicus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
tučňáku	tučňák	k1gMnSc6	tučňák
císařském	císařský	k2eAgMnSc6d1	císařský
druhým	druhý	k4xOgNnSc7	druhý
největším	veliký	k2eAgMnSc7d3	veliký
tučňákem	tučňák	k1gMnSc7	tučňák
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
představují	představovat	k5eAaImIp3nP	představovat
jediné	jediný	k2eAgMnPc4d1	jediný
dva	dva	k4xCgMnPc4	dva
zástupce	zástupce	k1gMnPc4	zástupce
rodu	rod	k1gInSc2	rod
Aptenodytes	Aptenodytes	k1gMnSc1	Aptenodytes
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
svým	svůj	k3xOyFgNnSc7	svůj
vzhledem	vzhledem	k7c3	vzhledem
natolik	natolik	k6eAd1	natolik
podobní	podobný	k2eAgMnPc1d1	podobný
<g/>
,	,	kIx,	,
že	že	k8xS	že
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
samotní	samotnět	k5eAaImIp3nP	samotnět
autoři	autor	k1gMnPc1	autor
naučné	naučný	k2eAgFnSc2d1	naučná
literatury	literatura	k1gFnSc2	literatura
nebo	nebo	k8xC	nebo
přírodovědných	přírodovědný	k2eAgInPc2d1	přírodovědný
magazínů	magazín	k1gInPc2	magazín
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
s	s	k7c7	s
chybně	chybně	k6eAd1	chybně
vyobrazeným	vyobrazený	k2eAgInSc7d1	vyobrazený
druhem	druh	k1gInSc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jejich	jejich	k3xOp3gNnSc2	jejich
podobného	podobný	k2eAgNnSc2d1	podobné
a	a	k8xC	a
pestrého	pestrý	k2eAgNnSc2d1	pestré
zbarvení	zbarvení	k1gNnSc2	zbarvení
jsou	být	k5eAaImIp3nP	být
specifičtí	specifický	k2eAgMnPc1d1	specifický
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
snášejí	snášet	k5eAaImIp3nP	snášet
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc4	jeden
velké	velký	k2eAgNnSc4d1	velké
vejce	vejce	k1gNnSc4	vejce
a	a	k8xC	a
vychovávají	vychovávat	k5eAaImIp3nP	vychovávat
tak	tak	k6eAd1	tak
jediné	jediný	k2eAgNnSc4d1	jediné
mládě	mládě	k1gNnSc4	mládě
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
chovnou	chovný	k2eAgFnSc4d1	chovná
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
patagonský	patagonský	k2eAgMnSc1d1	patagonský
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
především	především	k9	především
na	na	k7c6	na
subantarktických	subantarktický	k2eAgInPc6d1	subantarktický
ostrovech	ostrov	k1gInPc6	ostrov
v	v	k7c6	v
regionu	region	k1gInSc6	region
Antarktické	antarktický	k2eAgFnSc2d1	antarktická
konvergence	konvergence	k1gFnSc2	konvergence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
85	[number]	k4	85
<g/>
–	–	k?	–
<g/>
95	[number]	k4	95
cm	cm	kA	cm
a	a	k8xC	a
pokud	pokud	k8xS	pokud
nehladoví	hladovět	k5eNaImIp3nS	hladovět
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nS	vážit
až	až	k9	až
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
21	[number]	k4	21
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
bývá	bývat	k5eAaImIp3nS	bývat
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
samec	samec	k1gInSc4	samec
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
méně	málo	k6eAd2	málo
váží	vážit	k5eAaImIp3nP	vážit
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
především	především	k9	především
malými	malý	k2eAgFnPc7d1	malá
rybami	ryba	k1gFnPc7	ryba
a	a	k8xC	a
hlavonožci	hlavonožec	k1gMnPc7	hlavonožec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
součástí	součást	k1gFnSc7	součást
jeho	jeho	k3xOp3gInSc3	jeho
jídelníčku	jídelníček	k1gInSc3	jídelníček
je	být	k5eAaImIp3nS	být
i	i	k9	i
kril	kril	k1gMnSc1	kril
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
korýši	korýš	k1gMnPc1	korýš
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
loví	lovit	k5eAaImIp3nP	lovit
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
metrových	metrový	k2eAgFnPc6d1	metrová
hloubkách	hloubka	k1gFnPc6	hloubka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
dokáže	dokázat	k5eAaPmIp3nS	dokázat
ponořit	ponořit	k5eAaPmF	ponořit
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
větší	veliký	k2eAgFnSc2d2	veliký
než	než	k8xS	než
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
neobvykle	obvykle	k6eNd1	obvykle
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
chovný	chovný	k2eAgInSc1d1	chovný
cyklus	cyklus	k1gInSc1	cyklus
trvající	trvající	k2eAgInSc1d1	trvající
14	[number]	k4	14
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vychová	vychovat	k5eAaPmIp3nS	vychovat
za	za	k7c4	za
každé	každý	k3xTgInPc4	každý
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgMnPc4	dva
potomky	potomek	k1gMnPc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgMnSc4	svůj
partnera	partner	k1gMnSc4	partner
mění	měnit	k5eAaImIp3nS	měnit
prakticky	prakticky	k6eAd1	prakticky
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
a	a	k8xC	a
páry	pár	k1gInPc4	pár
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
konkrétním	konkrétní	k2eAgNnSc6d1	konkrétní
období	období	k1gNnSc6	období
hnízdění	hnízdění	k1gNnSc2	hnízdění
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdit	hnízdit	k5eAaImF	hnízdit
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
období	období	k1gNnSc6	období
antarktického	antarktický	k2eAgNnSc2d1	antarktické
léta	léto	k1gNnSc2	léto
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
září	září	k1gNnSc6	září
až	až	k8xS	až
listopadu	listopad	k1gInSc6	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Mládě	mládě	k1gNnSc1	mládě
se	se	k3xPyFc4	se
líhne	líhnout	k5eAaImIp3nS	líhnout
po	po	k7c6	po
52	[number]	k4	52
<g/>
–	–	k?	–
<g/>
56	[number]	k4	56
dnech	den	k1gInPc6	den
inkubace	inkubace	k1gFnSc2	inkubace
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgInPc2	první
30-40	[number]	k4	30-40
dnů	den	k1gInPc2	den
je	být	k5eAaImIp3nS	být
rodiči	rodič	k1gMnSc3	rodič
nepřetržitě	přetržitě	k6eNd1	přetržitě
střeženo	střežit	k5eAaImNgNnS	střežit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
nástupu	nástup	k1gInSc6	nástup
zimy	zima	k1gFnSc2	zima
je	být	k5eAaImIp3nS	být
opuštěné	opuštěný	k2eAgNnSc1d1	opuštěné
a	a	k8xC	a
utváří	utvářit	k5eAaPmIp3nS	utvářit
s	s	k7c7	s
vrstevníky	vrstevník	k1gMnPc7	vrstevník
tzv.	tzv.	kA	tzv.
školky	školka	k1gFnSc2	školka
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
je	on	k3xPp3gMnPc4	on
krmí	krmit	k5eAaImIp3nP	krmit
v	v	k7c6	v
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
intervalech	interval	k1gInPc6	interval
a	a	k8xC	a
mláďata	mládě	k1gNnPc4	mládě
tak	tak	k9	tak
hladoví	hladovět	k5eAaImIp3nS	hladovět
a	a	k8xC	a
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
na	na	k7c6	na
váze	váha	k1gFnSc6	váha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
opětovnému	opětovný	k2eAgNnSc3d1	opětovné
zvýšení	zvýšení	k1gNnSc3	zvýšení
množství	množství	k1gNnSc2	množství
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
rodiči	rodič	k1gMnPc7	rodič
krmeni	krmit	k5eAaImNgMnP	krmit
podstatně	podstatně	k6eAd1	podstatně
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
jsou	být	k5eAaImIp3nP	být
mláďata	mládě	k1gNnPc1	mládě
plně	plně	k6eAd1	plně
opeřena	opeřen	k2eAgNnPc1d1	opeřen
a	a	k8xC	a
vydají	vydat	k5eAaPmIp3nP	vydat
se	se	k3xPyFc4	se
samostatně	samostatně	k6eAd1	samostatně
lovit	lovit	k5eAaImF	lovit
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Schopni	schopen	k2eAgMnPc1d1	schopen
reprodukce	reprodukce	k1gFnSc2	reprodukce
jsou	být	k5eAaImIp3nP	být
asi	asi	k9	asi
ve	v	k7c6	v
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruhy	poddruh	k1gInPc4	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
uznávány	uznáván	k2eAgInPc1d1	uznáván
poddruhy	poddruh	k1gInPc1	poddruh
tučňáka	tučňák	k1gMnSc2	tučňák
patagonského	patagonský	k2eAgMnSc2d1	patagonský
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
údajné	údajný	k2eAgFnSc2d1	údajná
genetické	genetický	k2eAgFnSc2d1	genetická
odlišnosti	odlišnost	k1gFnSc2	odlišnost
<g/>
,	,	kIx,	,
z	z	k7c2	z
někdejšího	někdejší	k2eAgInSc2d1	někdejší
výzkumu	výzkum	k1gInSc2	výzkum
ornitologa	ornitolog	k1gMnSc4	ornitolog
Williama	William	k1gMnSc4	William
Mathewse	Mathews	k1gMnSc4	Mathews
;	;	kIx,	;
</s>
</p>
<p>
<s>
Aptenodytes	Aptenodytes	k1gMnSc1	Aptenodytes
patagonicus	patagonicus	k1gMnSc1	patagonicus
patagonicus	patagonicus	k1gMnSc1	patagonicus
(	(	kIx(	(
<g/>
obývající	obývající	k2eAgFnSc1d1	obývající
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Atlantického	atlantický	k2eAgInSc2d1	atlantický
oceánu	oceán	k1gInSc2	oceán
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aptenodytes	Aptenodytes	k1gMnSc1	Aptenodytes
patagonicus	patagonicus	k1gMnSc1	patagonicus
halli	halle	k1gFnSc4	halle
(	(	kIx(	(
<g/>
obývající	obývající	k2eAgFnSc1d1	obývající
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
<g/>
)	)	kIx)	)
<g/>
Poddruhy	poddruh	k1gInPc1	poddruh
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
nepatrně	patrně	k6eNd1	patrně
lišit	lišit	k5eAaImF	lišit
výškou	výška	k1gFnSc7	výška
těla	tělo	k1gNnPc1	tělo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
rozměrech	rozměr	k1gInPc6	rozměr
končetin	končetina	k1gFnPc2	končetina
či	či	k8xC	či
zobáku	zobák	k1gInSc2	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
prakticky	prakticky	k6eAd1	prakticky
totožní	totožný	k2eAgMnPc1d1	totožný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
patagonský	patagonský	k2eAgMnSc1d1	patagonský
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
především	především	k9	především
na	na	k7c6	na
subantarktických	subantarktický	k2eAgInPc6d1	subantarktický
ostrovech	ostrov	k1gInPc6	ostrov
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
od	od	k7c2	od
433	[number]	k4	433
do	do	k7c2	do
2745	[number]	k4	2745
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Antarktické	antarktický	k2eAgFnSc2d1	antarktická
konvergence	konvergence	k1gFnSc2	konvergence
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
poddruh	poddruh	k1gInSc1	poddruh
Aptenodytes	Aptenodytes	k1gMnSc1	Aptenodytes
patagonicus	patagonicus	k1gMnSc1	patagonicus
patagonicus	patagonicus	k1gMnSc1	patagonicus
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c4	v
Jižní	jižní	k2eAgFnPc4d1	jižní
Georgie	Georgie	k1gFnPc4	Georgie
<g/>
,	,	kIx,	,
Jižních	jižní	k2eAgInPc6d1	jižní
Sandwichových	Sandwichův	k2eAgInPc6d1	Sandwichův
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
Falklandských	Falklandský	k2eAgInPc6d1	Falklandský
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
vzácněji	vzácně	k6eAd2	vzácně
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
až	až	k9	až
na	na	k7c4	na
břehy	břeh	k1gInPc4	břeh
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc2	Brazílie
nebo	nebo	k8xC	nebo
jižního	jižní	k2eAgNnSc2d1	jižní
Chile	Chile	k1gNnSc2	Chile
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Ohňové	ohňový	k2eAgFnSc2d1	ohňová
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
poddruh	poddruh	k1gInSc1	poddruh
Aptenodytes	Aptenodytes	k1gMnSc1	Aptenodytes
patagonicus	patagonicus	k1gMnSc1	patagonicus
halli	halle	k1gFnSc3	halle
obývá	obývat	k5eAaImIp3nS	obývat
ostrovy	ostrov	k1gInPc4	ostrov
Crozetovy	Crozetův	k2eAgInPc4d1	Crozetův
<g/>
,	,	kIx,	,
Kergueleny	Kerguelen	k2eAgInPc4d1	Kerguelen
<g/>
,	,	kIx,	,
Heardův	Heardův	k2eAgInSc4d1	Heardův
ostrov	ostrov	k1gInSc4	ostrov
a	a	k8xC	a
McDonaldovy	McDonaldův	k2eAgInPc4d1	McDonaldův
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
ostrovy	ostrov	k1gInPc4	ostrov
Prince	princ	k1gMnSc2	princ
Edwarda	Edward	k1gMnSc2	Edward
<g/>
,	,	kIx,	,
ostrov	ostrov	k1gInSc1	ostrov
Marion	Marion	k1gInSc1	Marion
a	a	k8xC	a
ostrov	ostrov	k1gInSc1	ostrov
Macquarie	Macquarie	k1gFnSc2	Macquarie
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc4d2	menší
kolonii	kolonie	k1gFnSc4	kolonie
můžeme	moct	k5eAaImIp1nP	moct
zpozorovat	zpozorovat	k5eAaPmF	zpozorovat
i	i	k9	i
na	na	k7c6	na
Bouvetově	Bouvetův	k2eAgInSc6d1	Bouvetův
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
<g/>
Mimo	mimo	k6eAd1	mimo
hnízdní	hnízdní	k2eAgNnPc4d1	hnízdní
období	období	k1gNnPc4	období
se	se	k3xPyFc4	se
ptáci	pták	k1gMnPc1	pták
toulají	toulat	k5eAaImIp3nP	toulat
až	až	k9	až
k	k	k7c3	k
Antarktidě	Antarktida	k1gFnSc3	Antarktida
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Novému	nový	k2eAgInSc3d1	nový
Zélandu	Zéland	k1gInSc3	Zéland
či	či	k8xC	či
k	k	k7c3	k
Jižní	jižní	k2eAgFnSc3d1	jižní
Africe	Afrika	k1gFnSc3	Afrika
nebo	nebo	k8xC	nebo
Jižní	jižní	k2eAgFnSc3d1	jižní
Americe	Amerika	k1gFnSc3	Amerika
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
nevelký	velký	k2eNgInSc1d1	nevelký
počet	počet	k1gInSc1	počet
párů	pár	k1gInPc2	pár
může	moct	k5eAaImIp3nS	moct
taktéž	taktéž	k?	taktéž
zahnízdit	zahnízdit	k5eAaPmF	zahnízdit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
patagonský	patagonský	k2eAgMnSc1d1	patagonský
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgMnSc1d1	vysoký
85	[number]	k4	85
<g/>
–	–	k?	–
<g/>
95	[number]	k4	95
cm	cm	kA	cm
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
samice	samice	k1gFnSc1	samice
bývá	bývat	k5eAaImIp3nS	bývat
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
samec	samec	k1gInSc1	samec
–	–	k?	–
mívají	mívat	k5eAaImIp3nP	mívat
i	i	k9	i
menší	malý	k2eAgInSc4d2	menší
či	či	k8xC	či
užší	úzký	k2eAgInSc4d2	užší
zobák	zobák	k1gInSc4	zobák
nebo	nebo	k8xC	nebo
křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Váha	váha	k1gFnSc1	váha
tučňáka	tučňák	k1gMnSc2	tučňák
patagonského	patagonský	k2eAgInSc2d1	patagonský
je	být	k5eAaImIp3nS	být
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
nehladoví	hladovět	k5eNaImIp3nS	hladovět
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
váhy	váha	k1gFnSc2	váha
15	[number]	k4	15
až	až	k9	až
21	[number]	k4	21
kg	kg	kA	kg
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
váha	váha	k1gFnSc1	váha
samice	samice	k1gFnSc1	samice
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
9,3	[number]	k4	9,3
až	až	k9	až
12,8	[number]	k4	12,8
kg	kg	kA	kg
<g/>
,	,	kIx,	,
a	a	k8xC	a
váha	váha	k1gFnSc1	váha
samce	samec	k1gInSc2	samec
mezi	mezi	k7c7	mezi
14,1	[number]	k4	14,1
až	až	k9	až
18	[number]	k4	18
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hmotnost	hmotnost	k1gFnSc1	hmotnost
dospělého	dospělý	k1gMnSc2	dospělý
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
11,8	[number]	k4	11,8
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
tučňák	tučňák	k1gMnSc1	tučňák
patagonský	patagonský	k2eAgMnSc1d1	patagonský
má	mít	k5eAaImIp3nS	mít
modravě	modravě	k6eAd1	modravě
šedá	šedý	k2eAgNnPc4d1	šedé
záda	záda	k1gNnPc4	záda
<g/>
,	,	kIx,	,
a	a	k8xC	a
kromě	kromě	k7c2	kromě
hlavy	hlava	k1gFnSc2	hlava
je	být	k5eAaImIp3nS	být
přední	přední	k2eAgFnSc1d1	přední
část	část	k1gFnSc1	část
žlutavá	žlutavý	k2eAgFnSc1d1	žlutavá
nebo	nebo	k8xC	nebo
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnSc1d1	přední
část	část	k1gFnSc1	část
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
zadní	zadní	k2eAgFnSc2d1	zadní
částí	část	k1gFnSc7	část
těla	tělo	k1gNnSc2	tělo
podélně	podélně	k6eAd1	podélně
dělena	dělit	k5eAaImNgFnS	dělit
úzkým	úzký	k2eAgInSc7d1	úzký
černým	černý	k2eAgInSc7d1	černý
pruhem	pruh	k1gInSc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
krku	krk	k1gInSc6	krk
a	a	k8xC	a
hrudi	hruď	k1gFnSc6	hruď
má	mít	k5eAaImIp3nS	mít
žlutooranžové	žlutooranžový	k2eAgFnPc4d1	žlutooranžová
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
při	při	k7c6	při
námluvách	námluva	k1gFnPc6	námluva
k	k	k7c3	k
upoutání	upoutání	k1gNnSc3	upoutání
samic	samice	k1gFnPc2	samice
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
lžícovitá	lžícovitý	k2eAgFnSc1d1	lžícovitý
kresba	kresba	k1gFnSc1	kresba
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
část	část	k1gFnSc1	část
křídel	křídlo	k1gNnPc2	křídlo
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Náběžné	náběžný	k2eAgFnPc1d1	náběžná
hrany	hrana	k1gFnPc1	hrana
jsou	být	k5eAaImIp3nP	být
široce	široko	k6eAd1	široko
ohraničené	ohraničený	k2eAgInPc1d1	ohraničený
černo-modrou	černoodrý	k2eAgFnSc7d1	černo-modrá
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jejich	jejich	k3xOp3gFnSc1	jejich
špička	špička	k1gFnSc1	špička
<g/>
.	.	kIx.	.
</s>
<s>
Duhovka	duhovka	k1gFnSc1	duhovka
je	být	k5eAaImIp3nS	být
tmavě	tmavě	k6eAd1	tmavě
hnědá	hnědý	k2eAgNnPc1d1	hnědé
a	a	k8xC	a
víčka	víčko	k1gNnPc1	víčko
jsou	být	k5eAaImIp3nP	být
neopeřená	opeřený	k2eNgNnPc1d1	neopeřené
a	a	k8xC	a
zbarvena	zbarven	k2eAgFnSc1d1	zbarvena
do	do	k7c2	do
šedi	šeď	k1gFnSc2	šeď
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
mají	mít	k5eAaImIp3nP	mít
tmavě	tmavě	k6eAd1	tmavě
šedé	šedý	k2eAgFnPc1d1	šedá
až	až	k8xS	až
černé	černý	k2eAgFnPc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Zobák	zobák	k1gInSc1	zobák
je	být	k5eAaImIp3nS	být
štíhlý	štíhlý	k2eAgMnSc1d1	štíhlý
a	a	k8xC	a
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
až	až	k8xS	až
13	[number]	k4	13
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnPc6	jeho
stranách	strana	k1gFnPc6	strana
se	se	k3xPyFc4	se
táhnou	táhnout	k5eAaImIp3nP	táhnout
oranžové	oranžový	k2eAgFnPc1d1	oranžová
až	až	k8xS	až
purpurové	purpurový	k2eAgFnPc1d1	purpurová
skvrny	skvrna	k1gFnPc1	skvrna
reflektující	reflektující	k2eAgFnSc4d1	reflektující
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
(	(	kIx(	(
<g/>
UV	UV	kA	UV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
signalizaci	signalizace	k1gFnSc3	signalizace
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
zralosti	zralost	k1gFnSc2	zralost
<g/>
,	,	kIx,	,
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
stavu	stav	k1gInSc2	stav
nebo	nebo	k8xC	nebo
sociálního	sociální	k2eAgNnSc2d1	sociální
postavení	postavení	k1gNnSc2	postavení
jedince	jedinec	k1gMnSc2	jedinec
(	(	kIx(	(
<g/>
u	u	k7c2	u
mladých	mladý	k1gMnPc2	mladý
<g/>
,	,	kIx,	,
pohlavně	pohlavně	k6eAd1	pohlavně
nevyzrálých	vyzrálý	k2eNgMnPc2d1	nevyzrálý
ptáků	pták	k1gMnPc2	pták
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
neprokázala	prokázat	k5eNaPmAgFnS	prokázat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
zprvu	zprvu	k6eAd1	zprvu
čokoládově	čokoládově	k6eAd1	čokoládově
hnědá	hnědat	k5eAaImIp3nS	hnědat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dospělých	dospělí	k1gMnPc2	dospělí
se	se	k3xPyFc4	se
ale	ale	k9	ale
i	i	k9	i
v	v	k7c6	v
definitivním	definitivní	k2eAgInSc6d1	definitivní
šatu	šat	k1gInSc6	šat
liší	lišit	k5eAaImIp3nS	lišit
opeřením	opeření	k1gNnSc7	opeření
sytě	sytě	k6eAd1	sytě
černým	černý	k2eAgNnSc7d1	černé
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k9	především
nevýrazným	výrazný	k2eNgMnSc7d1	nevýrazný
<g/>
,	,	kIx,	,
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
světlým	světlý	k2eAgNnSc7d1	světlé
a	a	k8xC	a
do	do	k7c2	do
žluta	žluto	k1gNnSc2	žluto
zbarveným	zbarvený	k2eAgInSc7d1	zbarvený
zobákem	zobák	k1gInSc7	zobák
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
úplnou	úplný	k2eAgFnSc4d1	úplná
podobu	podoba	k1gFnSc4	podoba
až	až	k6eAd1	až
ve	v	k7c6	v
věku	věk	k1gInSc2	věk
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
vyzrálý	vyzrálý	k2eAgMnSc1d1	vyzrálý
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
tučňák	tučňák	k1gMnSc1	tučňák
patagonský	patagonský	k2eAgMnSc1d1	patagonský
dožil	dožít	k5eAaPmAgMnS	dožít
až	až	k6eAd1	až
26	[number]	k4	26
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
patagonský	patagonský	k2eAgMnSc1d1	patagonský
má	mít	k5eAaImIp3nS	mít
neobvykle	obvykle	k6eNd1	obvykle
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
chovný	chovný	k2eAgInSc1d1	chovný
cyklus	cyklus	k1gInSc1	cyklus
trvající	trvající	k2eAgInSc1d1	trvající
14	[number]	k4	14
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
vychová	vychovat	k5eAaPmIp3nS	vychovat
za	za	k7c4	za
každé	každý	k3xTgInPc4	každý
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
jen	jen	k9	jen
dva	dva	k4xCgMnPc4	dva
potomky	potomek	k1gMnPc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Mění	měnit	k5eAaImIp3nS	měnit
svého	svůj	k3xOyFgMnSc4	svůj
partnera	partner	k1gMnSc4	partner
prakticky	prakticky	k6eAd1	prakticky
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
a	a	k8xC	a
páry	pár	k1gInPc4	pár
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
konkrétním	konkrétní	k2eAgNnSc6d1	konkrétní
období	období	k1gNnSc6	období
hnízdění	hnízdění	k1gNnSc4	hnízdění
–	–	k?	–
nanejvýš	nanejvýš	k6eAd1	nanejvýš
spolu	spolu	k6eAd1	spolu
tentýž	týž	k3xTgInSc1	týž
pár	pár	k1gInSc1	pár
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
asi	asi	k9	asi
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
chovné	chovný	k2eAgFnSc2d1	chovná
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Nestaví	stavit	k5eNaBmIp3nS	stavit
si	se	k3xPyFc3	se
ani	ani	k8xC	ani
zcela	zcela	k6eAd1	zcela
prostá	prostý	k2eAgNnPc1d1	prosté
hnízda	hnízdo	k1gNnPc1	hnízdo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postává	postávat	k5eAaImIp3nS	postávat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
rovinaté	rovinatý	k2eAgFnSc6d1	rovinatá
půdě	půda	k1gFnSc6	půda
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
moře	moře	k1gNnSc2	moře
–	–	k?	–
vejce	vejce	k1gNnSc1	vejce
a	a	k8xC	a
mládě	mládě	k1gNnSc1	mládě
do	do	k7c2	do
určitého	určitý	k2eAgNnSc2d1	určité
stáří	stáří	k1gNnSc2	stáří
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
zahříváno	zahřívat	k5eAaImNgNnS	zahřívat
v	v	k7c6	v
teplé	teplý	k2eAgFnSc6d1	teplá
obrácené	obrácený	k2eAgFnSc6d1	obrácená
kapse	kapsa	k1gFnSc6	kapsa
nacházející	nacházející	k2eAgFnSc6d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
břišní	břišní	k2eAgFnSc6d1	břišní
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
každý	každý	k3xTgMnSc1	každý
jedinec	jedinec	k1gMnSc1	jedinec
brání	bránit	k5eAaImIp3nS	bránit
asi	asi	k9	asi
0,5	[number]	k4	0,5
metrový	metrový	k2eAgInSc4d1	metrový
prostor	prostor	k1gInSc4	prostor
kolem	kolem	k7c2	kolem
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgInS	mít
nezvaný	zvaný	k2eNgMnSc1d1	zvaný
host	host	k1gMnSc1	host
narušit	narušit	k5eAaPmF	narušit
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
koloniích	kolonie	k1gFnPc6	kolonie
čítajících	čítající	k2eAgMnPc2d1	čítající
až	až	k8xS	až
39	[number]	k4	39
000	[number]	k4	000
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
a	a	k8xC	a
způsob	způsob	k1gInSc1	způsob
lovu	lov	k1gInSc2	lov
===	===	k?	===
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
patagonský	patagonský	k2eAgMnSc1d1	patagonský
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
zpravidla	zpravidla	k6eAd1	zpravidla
rybami	ryba	k1gFnPc7	ryba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k6eAd1	především
lampovníkem	lampovník	k1gInSc7	lampovník
Carlsbergovým	Carlsbergův	k2eAgInSc7d1	Carlsbergův
(	(	kIx(	(
<g/>
Electrona	Electrona	k1gFnSc1	Electrona
carlsbergi	carlsberg	k1gFnSc2	carlsberg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lampovníkem	lampovník	k1gInSc7	lampovník
Anderssonovým	Anderssonův	k2eAgInSc7d1	Anderssonův
(	(	kIx(	(
<g/>
Krefftichthys	Krefftichthys	k1gInSc1	Krefftichthys
andressoni	andressoň	k1gFnSc3	andressoň
<g/>
)	)	kIx)	)
či	či	k8xC	či
lampovníkem	lampovník	k1gInSc7	lampovník
Tenisonovým	Tenisonův	k2eAgInSc7d1	Tenisonův
(	(	kIx(	(
<g/>
Protomyctophum	Protomyctophum	k1gInSc1	Protomyctophum
tenisoni	tenisoň	k1gFnSc3	tenisoň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
(	(	kIx(	(
<g/>
červenec-srpen	červenecrpen	k2eAgMnSc1d1	červenec-srpen
<g/>
)	)	kIx)	)
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
výhradně	výhradně	k6eAd1	výhradně
hlavonožce	hlavonožec	k1gMnPc4	hlavonožec
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
krakatici	krakatice	k1gFnSc4	krakatice
Kondakovu	Kondakův	k2eAgFnSc4d1	Kondakova
(	(	kIx(	(
<g/>
Kondakovia	Kondakovius	k1gMnSc2	Kondakovius
longimana	longiman	k1gMnSc2	longiman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
druhy	druh	k1gInPc1	druh
Psychroteutis	Psychroteutis	k1gFnSc2	Psychroteutis
glacialis	glacialis	k1gFnSc2	glacialis
či	či	k8xC	či
Martialia	Martialium	k1gNnSc2	Martialium
hyadesii	hyadesie	k1gFnSc3	hyadesie
<g/>
.	.	kIx.	.
</s>
<s>
Usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
takováto	takovýto	k3xDgFnSc1	takovýto
kořist	kořist	k1gFnSc1	kořist
výživnější	výživný	k2eAgFnSc1d2	výživnější
nežli	nežli	k8xS	nežli
ryze	ryze	k6eAd1	ryze
rybí	rybí	k2eAgNnSc1d1	rybí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
se	se	k3xPyFc4	se
ale	ale	k9	ale
tučňák	tučňák	k1gMnSc1	tučňák
patagonský	patagonský	k2eAgMnSc1d1	patagonský
živí	živit	k5eAaImIp3nS	živit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
období	období	k1gNnSc6	období
dostupné	dostupný	k2eAgInPc1d1	dostupný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kořist	kořist	k1gFnSc1	kořist
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
cm	cm	kA	cm
velká	velká	k1gFnSc1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
loví	lovit	k5eAaImIp3nP	lovit
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
180	[number]	k4	180
km	km	kA	km
od	od	k7c2	od
hnízdiště	hnízdiště	k1gNnSc2	hnízdiště
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
mimo	mimo	k6eAd1	mimo
hnízdním	hnízdní	k2eAgNnSc6d1	hnízdní
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
potulovat	potulovat	k5eAaImF	potulovat
až	až	k9	až
500	[number]	k4	500
km	km	kA	km
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
domoviny	domovina	k1gFnSc2	domovina
<g/>
.	.	kIx.	.
</s>
<s>
Kořist	kořist	k1gFnSc1	kořist
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
rychlostí	rychlost	k1gFnSc7	rychlost
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
(	(	kIx(	(
<g/>
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
právě	právě	k6eAd1	právě
probíhající	probíhající	k2eAgFnSc3d1	probíhající
akci	akce	k1gFnSc3	akce
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
potápí	potápět	k5eAaImIp3nS	potápět
se	se	k3xPyFc4	se
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
průměrně	průměrně	k6eAd1	průměrně
do	do	k7c2	do
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnPc1	studie
na	na	k7c6	na
Falklandských	Falklandský	k2eAgInPc6d1	Falklandský
ostrovech	ostrov	k1gInPc6	ostrov
provedena	provést	k5eAaPmNgFnS	provést
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
doposud	doposud	k6eAd1	doposud
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
ponor	ponor	k1gInSc4	ponor
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
343	[number]	k4	343
metrů	metr	k1gInPc2	metr
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
000	[number]	k4	000
měřených	měřený	k2eAgNnPc2d1	měřené
potápění	potápění	k1gNnPc2	potápění
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
denním	denní	k2eAgNnSc6d1	denní
světle	světlo	k1gNnSc6	světlo
se	se	k3xPyFc4	se
potápí	potápět	k5eAaImIp3nS	potápět
hlouběji	hluboko	k6eAd2	hluboko
nežli	nežli	k8xS	nežli
při	při	k7c6	při
setmění	setmění	k1gNnSc6	setmění
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zpravidla	zpravidla	k6eAd1	zpravidla
nepřekročí	překročit	k5eNaPmIp3nP	překročit
30	[number]	k4	30
metrovou	metrový	k2eAgFnSc4d1	metrová
hloubku	hloubka	k1gFnSc4	hloubka
–	–	k?	–
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
natolik	natolik	k6eAd1	natolik
přizpůsobený	přizpůsobený	k2eAgInSc4d1	přizpůsobený
zrak	zrak	k1gInSc4	zrak
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
predátorům	predátor	k1gMnPc3	predátor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
setmění	setmění	k1gNnSc6	setmění
aktivnější	aktivní	k2eAgFnSc4d2	aktivnější
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
vydrží	vydržet	k5eAaPmIp3nS	vydržet
asi	asi	k9	asi
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
ponořování	ponořování	k1gNnSc1	ponořování
je	být	k5eAaImIp3nS	být
uskutečňováno	uskutečňovat	k5eAaImNgNnS	uskutečňovat
až	až	k9	až
150	[number]	k4	150
krát	krát	k6eAd1	krát
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
tučňák	tučňák	k1gMnSc1	tučňák
patagonský	patagonský	k2eAgMnSc1d1	patagonský
projevuje	projevovat	k5eAaImIp3nS	projevovat
při	při	k7c6	při
potápění	potápění	k1gNnSc6	potápění
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
písmenu	písmeno	k1gNnSc6	písmeno
"	"	kIx"	"
<g/>
U	u	k7c2	u
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
pták	pták	k1gMnSc1	pták
ponoří	ponořit	k5eAaPmIp3nS	ponořit
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
hloubky	hloubka	k1gFnSc2	hloubka
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
setrvá	setrvat	k5eAaPmIp3nS	setrvat
polovinu	polovina	k1gFnSc4	polovina
celého	celý	k2eAgInSc2d1	celý
času	čas	k1gInSc2	čas
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
vynoří	vynořit	k5eAaPmIp3nS	vynořit
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
písmenu	písmeno	k1gNnSc3	písmeno
"	"	kIx"	"
<g/>
W	W	kA	W
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
V	v	k7c4	v
<g/>
"	"	kIx"	"
značící	značící	k2eAgNnSc4d1	značící
ponoření	ponoření	k1gNnSc4	ponoření
pod	pod	k7c7	pod
určitým	určitý	k2eAgInSc7d1	určitý
úhlem	úhel	k1gInSc7	úhel
a	a	k8xC	a
rychlé	rychlý	k2eAgNnSc1d1	rychlé
vynoření	vynoření	k1gNnSc1	vynoření
nad	nad	k7c4	nad
hladinu	hladina	k1gFnSc4	hladina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přirození	přirozený	k2eAgMnPc1d1	přirozený
predátoři	predátor	k1gMnPc1	predátor
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
mořském	mořský	k2eAgNnSc6d1	mořské
působení	působení	k1gNnSc6	působení
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
tučňák	tučňák	k1gMnSc1	tučňák
patagonského	patagonský	k2eAgInSc2d1	patagonský
především	především	k9	především
tuleň	tuleň	k1gMnSc1	tuleň
leopardí	leopardí	k2eAgMnSc1d1	leopardí
(	(	kIx(	(
<g/>
Hydrurga	Hydrurga	k1gFnSc1	Hydrurga
leptonyx	leptonyx	k1gInSc1	leptonyx
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kosatka	kosatka	k1gFnSc1	kosatka
dravá	dravý	k2eAgFnSc1d1	dravá
(	(	kIx(	(
<g/>
Orcinus	Orcinus	k1gMnSc1	Orcinus
orca	orca	k1gMnSc1	orca
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
lachtani	lachtan	k1gMnPc1	lachtan
(	(	kIx(	(
<g/>
Otariidae	Otariidae	k1gInSc1	Otariidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
pak	pak	k6eAd1	pak
lachtan	lachtan	k1gMnSc1	lachtan
antarktický	antarktický	k2eAgMnSc1d1	antarktický
(	(	kIx(	(
<g/>
Arctocephalus	Arctocephalus	k1gMnSc1	Arctocephalus
gazella	gazella	k1gMnSc1	gazella
<g/>
)	)	kIx)	)
Na	na	k7c6	na
souši	souš	k1gFnSc6	souš
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
a	a	k8xC	a
mláďatech	mládě	k1gNnPc6	mládě
přiživují	přiživovat	k5eAaImIp3nP	přiživovat
draví	dravý	k2eAgMnPc1d1	dravý
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
buřňák	buřňák	k1gMnSc1	buřňák
obrovský	obrovský	k2eAgMnSc1d1	obrovský
(	(	kIx(	(
<g/>
Macronectes	Macronectes	k1gMnSc1	Macronectes
giganteus	giganteus	k1gMnSc1	giganteus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
štítonos	štítonos	k1gInSc4	štítonos
světlezobý	světlezobý	k2eAgInSc4d1	světlezobý
(	(	kIx(	(
<g/>
Chionis	Chionis	k1gFnSc1	Chionis
albus	albus	k1gMnSc1	albus
<g/>
)	)	kIx)	)
racek	racek	k1gMnSc1	racek
jižní	jižní	k2eAgMnSc1d1	jižní
(	(	kIx(	(
<g/>
Larus	Larus	k1gMnSc1	Larus
dominicanus	dominicanus	k1gMnSc1	dominicanus
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
chaluha	chaluha	k1gFnSc1	chaluha
subantarktická	subantarktický	k2eAgFnSc1d1	subantarktický
(	(	kIx(	(
<g/>
Catharacta	Catharacta	k1gFnSc1	Catharacta
antarcticus	antarcticus	k1gInSc4	antarcticus
lonnbergi	lonnberg	k1gFnSc2	lonnberg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
masožraví	masožravý	k2eAgMnPc1d1	masožravý
ptáci	pták	k1gMnPc1	pták
využívají	využívat	k5eAaPmIp3nP	využívat
těžkého	těžký	k2eAgNnSc2d1	těžké
období	období	k1gNnSc2	období
pro	pro	k7c4	pro
mláďata	mládě	k1gNnPc4	mládě
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
osamocena	osamocen	k2eAgNnPc1d1	osamoceno
<g/>
,	,	kIx,	,
zesláblá	zesláblý	k2eAgNnPc4d1	zesláblé
nízkým	nízký	k2eAgInSc7d1	nízký
příjmem	příjem	k1gInSc7	příjem
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
snadná	snadný	k2eAgFnSc1d1	snadná
k	k	k7c3	k
uštvání	uštvání	k1gNnSc3	uštvání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnPc1	rozmnožování
a	a	k8xC	a
chov	chov	k1gInSc1	chov
mláďat	mládě	k1gNnPc2	mládě
===	===	k?	===
</s>
</p>
<p>
<s>
Hnízdit	hnízdit	k5eAaImF	hnízdit
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
období	období	k1gNnSc6	období
antarktického	antarktický	k2eAgNnSc2d1	antarktické
léta	léto	k1gNnSc2	léto
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
září	září	k1gNnSc6	září
až	až	k8xS	až
listopadu	listopad	k1gInSc6	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
námluvách	námluva	k1gFnPc6	námluva
láká	lákat	k5eAaImIp3nS	lákat
samec	samec	k1gMnSc1	samec
potenciální	potenciální	k2eAgFnSc4d1	potenciální
družku	družka	k1gFnSc4	družka
troubením	troubení	k1gNnSc7	troubení
se	se	k3xPyFc4	se
zdviženu	zdvižen	k2eAgFnSc4d1	zdvižena
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ta	ten	k3xDgFnSc1	ten
má	mít	k5eAaImIp3nS	mít
zájem	zájem	k1gInSc4	zájem
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
samec	samec	k1gMnSc1	samec
provádí	provádět	k5eAaImIp3nS	provádět
tzv.	tzv.	kA	tzv.
reklamní	reklamní	k2eAgInSc4d1	reklamní
pochod	pochod	k1gInSc4	pochod
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
kývá	kývat	k5eAaImIp3nS	kývat
hlavou	hlavý	k2eAgFnSc4d1	hlavá
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
<g/>
Kdykoli	kdykoli	k6eAd1	kdykoli
mezi	mezi	k7c7	mezi
listopadem	listopad	k1gInSc7	listopad
až	až	k8xS	až
březnem	březen	k1gInSc7	březen
snese	snést	k5eAaPmIp3nS	snést
samice	samice	k1gFnSc1	samice
jedno	jeden	k4xCgNnSc1	jeden
velké	velký	k2eAgNnSc1d1	velké
vejce	vejce	k1gNnSc1	vejce
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
cm	cm	kA	cm
×	×	k?	×
7	[number]	k4	7
cm	cm	kA	cm
a	a	k8xC	a
váze	váha	k1gFnSc6	váha
zhruba	zhruba	k6eAd1	zhruba
300	[number]	k4	300
gramů	gram	k1gInPc2	gram
<g/>
,	,	kIx,	,
a	a	k8xC	a
předá	předat	k5eAaPmIp3nS	předat
jej	on	k3xPp3gNnSc4	on
samci	samec	k1gMnPc1	samec
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
vejce	vejce	k1gNnSc1	vejce
překutálí	překutálet	k5eAaPmIp3nS	překutálet
na	na	k7c4	na
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
jej	on	k3xPp3gMnSc4	on
izoluje	izolovat	k5eAaBmIp3nS	izolovat
od	od	k7c2	od
chladné	chladný	k2eAgFnSc2d1	chladná
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
takto	takto	k6eAd1	takto
jej	on	k3xPp3gMnSc4	on
překryje	překrýt	k5eAaPmIp3nS	překrýt
kožním	kožní	k2eAgInSc7d1	kožní
záhybem	záhyb	k1gInSc7	záhyb
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
vejcem	vejce	k1gNnSc7	vejce
neopeřený	opeřený	k2eNgMnSc1d1	neopeřený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezení	sezení	k1gNnSc6	sezení
na	na	k7c6	na
vejci	vejce	k1gNnSc6	vejce
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
střídají	střídat	k5eAaImIp3nP	střídat
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
pětidenních	pětidenní	k2eAgInPc6d1	pětidenní
intervalech	interval	k1gInPc6	interval
<g/>
.	.	kIx.	.
</s>
<s>
Mládě	mládě	k1gNnSc1	mládě
se	se	k3xPyFc4	se
líhne	líhnout	k5eAaImIp3nS	líhnout
po	po	k7c6	po
52	[number]	k4	52
<g/>
–	–	k?	–
<g/>
56	[number]	k4	56
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyklubání	vyklubání	k1gNnSc1	vyklubání
ze	z	k7c2	z
skořápky	skořápka	k1gFnSc2	skořápka
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
až	až	k9	až
72	[number]	k4	72
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
mládě	mládě	k1gNnSc1	mládě
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
váhy	váha	k1gFnPc4	váha
asi	asi	k9	asi
430	[number]	k4	430
gramů	gram	k1gInPc2	gram
a	a	k8xC	a
skrývá	skrývat	k5eAaImIp3nS	skrývat
se	se	k3xPyFc4	se
prvních	první	k4xOgInPc2	první
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
dnů	den	k1gInPc2	den
pod	pod	k7c7	pod
kožním	kožní	k2eAgInSc7d1	kožní
záhybem	záhyb	k1gInSc7	záhyb
na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
není	být	k5eNaImIp3nS	být
schopné	schopný	k2eAgNnSc1d1	schopné
regulovat	regulovat	k5eAaImF	regulovat
teplotu	teplota	k1gFnSc4	teplota
svého	svůj	k3xOyFgNnSc2	svůj
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvních	první	k4xOgNnPc2	první
asi	asi	k9	asi
pěti	pět	k4xCc2	pět
týdnů	týden	k1gInPc2	týden
naslouchá	naslouchat	k5eAaImIp3nS	naslouchat
potomek	potomek	k1gMnSc1	potomek
svým	svůj	k3xOyFgMnPc3	svůj
rodičům	rodič	k1gMnPc3	rodič
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ho	on	k3xPp3gMnSc4	on
učí	učit	k5eAaImIp3nS	učit
společným	společný	k2eAgInPc3d1	společný
rozpoznávacím	rozpoznávací	k2eAgInPc3d1	rozpoznávací
tónům	tón	k1gInPc3	tón
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
budou	být	k5eAaImBp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
v	v	k7c6	v
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
období	období	k1gNnSc6	období
chovu	chov	k1gInSc2	chov
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
mládě	mládě	k1gNnSc1	mládě
odhodlává	odhodlávat	k5eAaImIp3nS	odhodlávat
prozkoumávat	prozkoumávat	k5eAaImF	prozkoumávat
blízké	blízký	k2eAgNnSc4d1	blízké
okolí	okolí	k1gNnSc4	okolí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
na	na	k7c4	na
nohy	noha	k1gFnPc4	noha
rodičů	rodič	k1gMnPc2	rodič
vrací	vracet	k5eAaImIp3nS	vracet
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
krmení	krmení	k1gNnSc2	krmení
a	a	k8xC	a
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
péči	péče	k1gFnSc6	péče
o	o	k7c4	o
potomka	potomek	k1gMnSc4	potomek
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
obě	dva	k4xCgNnPc4	dva
pohlaví	pohlaví	k1gNnPc4	pohlaví
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
vždy	vždy	k6eAd1	vždy
hlídá	hlídat	k5eAaImIp3nS	hlídat
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
loví	lovit	k5eAaImIp3nS	lovit
potravu	potrava	k1gFnSc4	potrava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mládě	mládě	k1gNnSc1	mládě
do	do	k7c2	do
konce	konec	k1gInSc2	konec
dubna	duben	k1gInSc2	duben
přepelichá	přepelichat	k5eAaPmIp3nS	přepelichat
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
prachového	prachový	k2eAgNnSc2d1	prachové
peří	peří	k1gNnSc2	peří
a	a	k8xC	a
přibere	přibrat	k5eAaPmIp3nS	přibrat
na	na	k7c4	na
hmotnost	hmotnost	k1gFnSc4	hmotnost
kolem	kolem	k7c2	kolem
11	[number]	k4	11
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nástupu	nástup	k1gInSc3	nástup
zimy	zima	k1gFnSc2	zima
<g/>
,	,	kIx,	,
k	k	k7c3	k
sníženému	snížený	k2eAgNnSc3d1	snížené
množství	množství	k1gNnSc3	množství
potravy	potrava	k1gFnSc2	potrava
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
odchodu	odchod	k1gInSc6	odchod
obou	dva	k4xCgInPc6	dva
rodičů	rodič	k1gMnPc2	rodič
z	z	k7c2	z
hnízdní	hnízdní	k2eAgFnSc2d1	hnízdní
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
do	do	k7c2	do
takzvaných	takzvaný	k2eAgFnPc2d1	takzvaná
školek	školka	k1gFnPc2	školka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jejich	jejich	k3xOp3gInSc1	jejich
těsný	těsný	k2eAgInSc1d1	těsný
kontakt	kontakt	k1gInSc1	kontakt
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
výdej	výdej	k1gInSc4	výdej
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
je	být	k5eAaImIp3nS	být
rodiče	rodič	k1gMnPc4	rodič
krmí	krmě	k1gFnSc7	krmě
jen	jen	k9	jen
každý	každý	k3xTgInSc4	každý
druhý	druhý	k4xOgInSc4	druhý
až	až	k8xS	až
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
proto	proto	k8xC	proto
zhubnou	zhubnout	k5eAaPmIp3nP	zhubnout
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
původní	původní	k2eAgFnSc2d1	původní
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
hmotnosti	hmotnost	k1gFnPc4	hmotnost
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
10	[number]	k4	10
kg	kg	kA	kg
<g/>
,	,	kIx,	,
zimu	zima	k1gFnSc4	zima
zpravidla	zpravidla	k6eAd1	zpravidla
nepřečkají	přečkat	k5eNaPmIp3nP	přečkat
a	a	k8xC	a
uhynou	uhynout	k5eAaPmIp3nP	uhynout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
opětovnému	opětovný	k2eAgNnSc3d1	opětovné
zvýšení	zvýšení	k1gNnSc3	zvýšení
množství	množství	k1gNnSc2	množství
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
oteplení	oteplení	k1gNnSc3	oteplení
povrchové	povrchový	k2eAgFnSc2d1	povrchová
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
250	[number]	k4	250
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
a	a	k8xC	a
množícímu	množící	k2eAgInSc3d1	množící
se	se	k3xPyFc4	se
planktonu	plankton	k1gInSc6	plankton
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přiláká	přilákat	k5eAaPmIp3nS	přilákat
především	především	k6eAd1	především
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
tak	tak	k9	tak
přicházejí	přicházet	k5eAaImIp3nP	přicházet
nakrmit	nakrmit	k5eAaPmF	nakrmit
své	svůj	k3xOyFgMnPc4	svůj
potomky	potomek	k1gMnPc4	potomek
podstatně	podstatně	k6eAd1	podstatně
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
prosince	prosinec	k1gInSc2	prosinec
je	být	k5eAaImIp3nS	být
prachové	prachový	k2eAgNnSc1d1	prachové
peří	peří	k1gNnSc1	peří
mláďat	mládě	k1gNnPc2	mládě
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
juvenilním	juvenilní	k2eAgMnPc3d1	juvenilní
(	(	kIx(	(
<g/>
plně	plně	k6eAd1	plně
funkčním	funkční	k2eAgMnSc7d1	funkční
a	a	k8xC	a
nepromokavým	promokavý	k2eNgInSc7d1	nepromokavý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
vydávají	vydávat	k5eAaImIp3nP	vydávat
se	se	k3xPyFc4	se
samostatně	samostatně	k6eAd1	samostatně
lovit	lovit	k5eAaImF	lovit
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Schopni	schopen	k2eAgMnPc1d1	schopen
reprodukce	reprodukce	k1gFnSc2	reprodukce
jsou	být	k5eAaImIp3nP	být
asi	asi	k9	asi
ve	v	k7c6	v
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Populace	populace	k1gFnSc2	populace
a	a	k8xC	a
ohrožení	ohrožení	k1gNnSc2	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
stabilně	stabilně	k6eAd1	stabilně
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
asi	asi	k9	asi
dva	dva	k4xCgInPc4	dva
miliony	milion	k4xCgInPc4	milion
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
zaznamenáván	zaznamenávat	k5eAaImNgInS	zaznamenávat
globální	globální	k2eAgInSc1d1	globální
populační	populační	k2eAgInSc1d1	populační
růst	růst	k1gInSc1	růst
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgInSc1d1	červený
seznam	seznam	k1gInSc1	seznam
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
IUCN	IUCN	kA	IUCN
<g/>
)	)	kIx)	)
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
tučňák	tučňák	k1gMnSc1	tučňák
patagonský	patagonský	k2eAgMnSc1d1	patagonský
neohrožený	ohrožený	k2eNgMnSc1d1	neohrožený
(	(	kIx(	(
<g/>
málo	málo	k6eAd1	málo
dotčený	dotčený	k2eAgInSc1d1	dotčený
<g/>
)	)	kIx)	)
na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgNnPc2	tento
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
zeměpisný	zeměpisný	k2eAgInSc1d1	zeměpisný
rozsah	rozsah	k1gInSc1	rozsah
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
20	[number]	k4	20
000	[number]	k4	000
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
</s>
</p>
<p>
<s>
populace	populace	k1gFnSc1	populace
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
nad	nad	k7c7	nad
10	[number]	k4	10
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
</s>
</p>
<p>
<s>
populace	populace	k1gFnPc1	populace
rosteV	rostva	k1gFnPc2	rostva
současnosti	současnost	k1gFnSc2	současnost
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
nečelí	čelit	k5eNaImIp3nS	čelit
tučňák	tučňák	k1gMnSc1	tučňák
patagonský	patagonský	k2eAgMnSc1d1	patagonský
žádnému	žádný	k3yNgNnSc3	žádný
přímému	přímý	k2eAgNnSc3d1	přímé
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Případnou	případný	k2eAgFnSc7d1	případná
hrozbou	hrozba	k1gFnSc7	hrozba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kolísává	kolísávat	k5eAaImIp3nS	kolísávat
potravní	potravní	k2eAgFnSc1d1	potravní
dostupnost	dostupnost	k1gFnSc1	dostupnost
vlivem	vliv	k1gInSc7	vliv
komerčních	komerční	k2eAgInPc2d1	komerční
rybolovů	rybolov	k1gInPc2	rybolov
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byli	být	k5eAaImAgMnP	být
tučňáci	tučňák	k1gMnPc1	tučňák
patagonští	patagonský	k2eAgMnPc1d1	patagonský
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc2	jejich
domovině	domovina	k1gFnSc6	domovina
odchyceni	odchycen	k2eAgMnPc1d1	odchycen
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgFnPc4d1	jižní
Georgie	Georgie	k1gFnPc4	Georgie
<g/>
)	)	kIx)	)
a	a	k8xC	a
odvlečeni	odvléct	k5eAaPmNgMnP	odvléct
na	na	k7c4	na
opačný	opačný	k2eAgInSc4d1	opačný
konec	konec	k1gInSc4	konec
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
do	do	k7c2	do
severního	severní	k2eAgNnSc2d1	severní
Norska	Norsko	k1gNnSc2	Norsko
(	(	kIx(	(
<g/>
Lofoty	Lofota	k1gFnSc2	Lofota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
experiment	experiment	k1gInSc4	experiment
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
Iniciátorem	iniciátor	k1gMnSc7	iniciátor
byl	být	k5eAaImAgMnS	být
lovec	lovec	k1gMnSc1	lovec
velryb	velryba	k1gFnPc2	velryba
Helge	Helge	k1gFnPc2	Helge
Helgesen	Helgesen	k2eAgInSc1d1	Helgesen
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
odpovědět	odpovědět	k5eAaPmF	odpovědět
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
tito	tento	k3xDgMnPc1	tento
ptáci	pták	k1gMnPc1	pták
dokážou	dokázat	k5eAaPmIp3nP	dokázat
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
taktéž	taktéž	k?	taktéž
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
stejných	stejný	k2eAgFnPc6d1	stejná
podmínkách	podmínka	k1gFnPc6	podmínka
jako	jako	k8xS	jako
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
polokouli	polokoule	k1gFnSc6	polokoule
jižní	jižní	k2eAgFnSc6d1	jižní
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
se	se	k3xPyFc4	se
však	však	k9	však
pouze	pouze	k6eAd1	pouze
rozprchli	rozprchnout	k5eAaPmAgMnP	rozprchnout
<g/>
,	,	kIx,	,
k	k	k7c3	k
odchovu	odchov	k1gInSc3	odchov
zjevně	zjevně	k6eAd1	zjevně
nikdy	nikdy	k6eAd1	nikdy
nedošlo	dojít	k5eNaPmAgNnS	dojít
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
již	již	k6eAd1	již
nebyli	být	k5eNaImAgMnP	být
v	v	k7c4	v
dané	daný	k2eAgFnPc4d1	daná
oblasti	oblast	k1gFnPc4	oblast
spatřeni	spatřen	k2eAgMnPc1d1	spatřen
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
největší	veliký	k2eAgFnSc4d3	veliký
příčinu	příčina	k1gFnSc4	příčina
neúspěchu	neúspěch	k1gInSc2	neúspěch
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zde	zde	k6eAd1	zde
zkusila	zkusit	k5eAaPmAgFnS	zkusit
výprava	výprava	k1gFnSc1	výprava
vypustit	vypustit	k5eAaPmF	vypustit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
druh	druh	k1gInSc4	druh
hnízdící	hnízdící	k2eAgInSc4d1	hnízdící
v	v	k7c6	v
obrovských	obrovský	k2eAgFnPc6d1	obrovská
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
<g/>
Tučňák	tučňák	k1gMnSc1	tučňák
patagonský	patagonský	k2eAgMnSc1d1	patagonský
ztvárňuje	ztvárňovat	k5eAaImIp3nS	ztvárňovat
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Batman	Batman	k1gMnSc1	Batman
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Batman	Batman	k1gMnSc1	Batman
Returns	Returns	k1gInSc1	Returns
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
natočeném	natočený	k2eAgInSc6d1	natočený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Mykiska	Mykiska	k1gFnSc1	Mykiska
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
Byl	být	k5eAaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
rok	rok	k1gInSc4	rok
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Mykiska	Mykiska	k1gFnSc1	Mykiska
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
Veselovský	Veselovský	k1gMnSc1	Veselovský
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
,	,	kIx,	,
Tučňáci	tučňák	k1gMnPc1	tučňák
<g/>
,	,	kIx,	,
Státní	státní	k2eAgNnSc1d1	státní
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Williams	Williams	k1gInSc1	Williams
<g/>
,	,	kIx,	,
Tony	Tony	k1gMnSc1	Tony
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Penguins	Penguins	k1gInSc1	Penguins
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
England	England	k1gInSc1	England
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
019854667X	[number]	k4	019854667X
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
tučňák	tučňák	k1gMnSc1	tučňák
patagonský	patagonský	k2eAgMnSc1d1	patagonský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
tučňák	tučňák	k1gMnSc1	tučňák
patagonský	patagonský	k2eAgMnSc1d1	patagonský
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
http://www.penguinsworld.cz	[url]	k1gInSc1	http://www.penguinsworld.cz
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
