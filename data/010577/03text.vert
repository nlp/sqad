<p>
<s>
Rowan	Rowan	k1gMnSc1	Rowan
Sebastian	Sebastian	k1gMnSc1	Sebastian
Atkinson	Atkinson	k1gMnSc1	Atkinson
CBE	CBE	kA	CBE
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
Consett	Consett	k1gInSc1	Consett
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgMnSc1d1	britský
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
komik	komik	k1gMnSc1	komik
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
rolím	role	k1gFnPc3	role
patří	patřit	k5eAaImIp3nS	patřit
postava	postava	k1gFnSc1	postava
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
Beana	Beana	k6eAd1	Beana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
do	do	k7c2	do
selské	selský	k2eAgFnSc2d1	selská
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
už	už	k6eAd1	už
odmala	odmala	k6eAd1	odmala
projevoval	projevovat	k5eAaImAgMnS	projevovat
nadání	nadání	k1gNnSc4	nadání
rozesmívat	rozesmívat	k5eAaImF	rozesmívat
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
nechtěl	chtít	k5eNaImAgMnS	chtít
stát	stát	k5eAaImF	stát
komikem	komik	k1gMnSc7	komik
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
povolání	povolání	k1gNnSc4	povolání
se	s	k7c7	s
stálým	stálý	k2eAgMnSc7d1	stálý
a	a	k8xC	a
jistým	jistý	k2eAgInSc7d1	jistý
platem	plat	k1gInSc7	plat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studie	k1gFnPc6	studie
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
v	v	k7c6	v
Newcastlu	Newcastl	k1gInSc6	Newcastl
a	a	k8xC	a
Oxfordu	Oxford	k1gInSc2	Oxford
se	se	k3xPyFc4	se
však	však	k9	však
komikem	komik	k1gMnSc7	komik
stal	stát	k5eAaPmAgMnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
pro	pro	k7c4	pro
divadlo	divadlo	k1gNnSc4	divadlo
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
Playhouse	Playhouse	k1gFnSc2	Playhouse
a	a	k8xC	a
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
se	se	k3xPyFc4	se
i	i	k9	i
jako	jako	k9	jako
divadelní	divadelní	k2eAgMnSc1d1	divadelní
komik	komik	k1gMnSc1	komik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
pronikl	proniknout	k5eAaPmAgInS	proniknout
do	do	k7c2	do
televize	televize	k1gFnSc2	televize
jako	jako	k8xS	jako
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
BBC	BBC	kA	BBC
Nezprávy	Nezpráva	k1gFnSc2	Nezpráva
v	v	k7c4	v
devět	devět	k4xCc4	devět
<g/>
,	,	kIx,	,
obdržel	obdržet	k5eAaPmAgMnS	obdržet
ocenění	ocenění	k1gNnSc4	ocenění
British	British	k1gMnSc1	British
Academy	Academa	k1gFnSc2	Academa
Award	Award	k1gMnSc1	Award
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
osobností	osobnost	k1gFnSc7	osobnost
BBC	BBC	kA	BBC
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
úspěchy	úspěch	k1gInPc1	úspěch
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
a	a	k8xC	a
zajišťovaly	zajišťovat	k5eAaImAgInP	zajišťovat
mu	on	k3xPp3gMnSc3	on
růst	růst	k1gInSc4	růst
popularity	popularita	k1gFnSc2	popularita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
vedle	vedle	k7c2	vedle
Seana	Sean	k1gMnSc2	Sean
Conneryho	Connery	k1gMnSc2	Connery
v	v	k7c6	v
bondovce	bondovce	k?	bondovce
Nikdy	nikdy	k6eAd1	nikdy
neříkej	říkat	k5eNaImRp2nS	říkat
nikdy	nikdy	k6eAd1	nikdy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgMnS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
se	s	k7c7	s
Sunetrou	Sunetra	k1gFnSc7	Sunetra
Sastry	Sastr	k1gInPc7	Sastr
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
Lily	lít	k5eAaImAgInP	lít
a	a	k8xC	a
Benjamina	Benjamin	k1gMnSc2	Benjamin
<g/>
.	.	kIx.	.
</s>
<s>
Rozvedl	rozvést	k5eAaPmAgInS	rozvést
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnPc1d1	vlastní
dvě	dva	k4xCgFnPc1	dva
filmové	filmový	k2eAgFnPc1d1	filmová
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
Tiger	Tiger	k1gInSc1	Tiger
Television	Television	k1gInSc1	Television
a	a	k8xC	a
Tiger	Tiger	k1gInSc1	Tiger
Aspect	Aspect	k2eAgInSc4d1	Aspect
Films	Films	k1gInSc4	Films
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
milovník	milovník	k1gMnSc1	milovník
rychlých	rychlý	k2eAgInPc2d1	rychlý
aut	aut	k1gInSc4	aut
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
televizní	televizní	k2eAgFnSc7d1	televizní
show	show	k1gFnSc7	show
Top	topit	k5eAaImRp2nS	topit
Gear	Gear	k1gMnSc1	Gear
jako	jako	k8xC	jako
host	host	k1gMnSc1	host
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Herecká	herecký	k2eAgFnSc1d1	herecká
dráha	dráha	k1gFnSc1	dráha
==	==	k?	==
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
Rowan	Rowan	k1gMnSc1	Rowan
Atkinson	Atkinson	k1gMnSc1	Atkinson
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
lidí	člověk	k1gMnPc2	člověk
svým	svůj	k3xOyFgInSc7	svůj
pořadem	pořad	k1gInSc7	pořad
The	The	k1gFnPc2	The
Atkinson	Atkinsona	k1gFnPc2	Atkinsona
Family	Famila	k1gFnSc2	Famila
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vysílal	vysílat	k5eAaImAgInS	vysílat
v	v	k7c6	v
rádiu	rádius	k1gInSc6	rádius
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
slavil	slavit	k5eAaImAgInS	slavit
úspěch	úspěch	k1gInSc4	úspěch
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
skečovým	skečův	k2eAgInSc7d1	skečův
pořadem	pořad	k1gInSc7	pořad
Not	nota	k1gFnPc2	nota
the	the	k?	the
Nine	Nine	k1gNnSc4	Nine
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Clock	Clock	k1gInSc4	Clock
News	Newsa	k1gFnPc2	Newsa
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgInS	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Černá	černý	k2eAgFnSc1d1	černá
zmije	zmije	k1gFnSc1	zmije
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gMnSc4	on
proslavil	proslavit	k5eAaPmAgMnS	proslavit
<g/>
.	.	kIx.	.
</s>
<s>
Zlom	zlom	k1gInSc1	zlom
jeho	jeho	k3xOp3gFnSc2	jeho
kariéry	kariéra	k1gFnSc2	kariéra
přišel	přijít	k5eAaPmAgInS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
s	s	k7c7	s
vysíláním	vysílání	k1gNnSc7	vysílání
seriálu	seriál	k1gInSc2	seriál
Mr	Mr	k1gFnSc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gInSc1	Bean
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
Atkinson	Atkinson	k1gInSc1	Atkinson
zahrál	zahrát	k5eAaPmAgInS	zahrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
-	-	kIx~	-
Mr	Mr	k1gFnSc4	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Beana	Beana	k6eAd1	Beana
<g/>
.	.	kIx.	.
</s>
<s>
Vysílal	vysílat	k5eAaImAgInS	vysílat
se	se	k3xPyFc4	se
v	v	k7c6	v
245	[number]	k4	245
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
celkem	celkem	k6eAd1	celkem
15	[number]	k4	15
45	[number]	k4	45
<g/>
minutových	minutový	k2eAgFnPc2d1	minutová
epizod	epizoda	k1gFnPc2	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
se	se	k3xPyFc4	se
vysílal	vysílat	k5eAaImAgInS	vysílat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnSc2	některý
televize	televize	k1gFnSc2	televize
ho	on	k3xPp3gMnSc4	on
reprízují	reprízovat	k5eAaImIp3nP	reprízovat
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
Beana	Bean	k1gInSc2	Bean
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Mr	Mr	k1gFnSc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gNnSc1	Bean
<g/>
:	:	kIx,	:
Největší	veliký	k2eAgFnSc1d3	veliký
fimová	fimový	k2eAgFnSc1d1	fimový
katastrofa	katastrofa	k1gFnSc1	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
jako	jako	k9	jako
neschopný	schopný	k2eNgMnSc1d1	neschopný
agent	agent	k1gMnSc1	agent
Johnny	Johnna	k1gFnSc2	Johnna
English	English	k1gMnSc1	English
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Johny	John	k1gMnPc4	John
English	English	k1gInSc4	English
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
znovu	znovu	k6eAd1	znovu
jako	jako	k8xC	jako
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gInSc1	Bean
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Prázdniny	prázdniny	k1gFnPc1	prázdniny
pana	pan	k1gMnSc2	pan
Beana	Bean	k1gMnSc2	Bean
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
jako	jako	k8xC	jako
Johnny	Johnn	k1gInPc4	Johnn
English	Englisha	k1gFnPc2	Englisha
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Johnny	Johnna	k1gFnSc2	Johnna
English	Englisha	k1gFnPc2	Englisha
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
rolí	role	k1gFnPc2	role
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
poslední	poslední	k2eAgNnSc4d1	poslední
patří	patřit	k5eAaImIp3nS	patřit
komisař	komisař	k1gMnSc1	komisař
Mairgaret	Mairgaret	k1gMnSc1	Mairgaret
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
trilogii	trilogie	k1gFnSc6	trilogie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
jako	jako	k8xC	jako
Johnny	Johnn	k1gInPc4	Johnn
English	Englisha	k1gFnPc2	Englisha
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Johnny	Johnna	k1gFnSc2	Johnna
English	English	k1gMnSc1	English
strikes	strikes	k1gMnSc1	strikes
again	again	k1gMnSc1	again
(	(	kIx(	(
<g/>
Johnny	Johnna	k1gMnSc2	Johnna
English	English	k1gInSc1	English
znovu	znovu	k6eAd1	znovu
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc1	filmografie
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Instantní	instantní	k2eAgInSc1d1	instantní
smích	smích	k1gInSc1	smích
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Černá	černý	k2eAgFnSc1d1	černá
zmije	zmije	k1gFnSc1	zmije
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Přesný	přesný	k2eAgMnSc1d1	přesný
jako	jako	k8xS	jako
smrt	smrt	k1gFnSc1	smrt
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nikdy	nikdy	k6eAd1	nikdy
neříkej	říkat	k5eNaImRp2nS	říkat
nikdy	nikdy	k6eAd1	nikdy
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mr	Mr	k?	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gNnSc1	Bean
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Žhavé	žhavý	k2eAgInPc1d1	žhavý
výstřely	výstřel	k1gInPc1	výstřel
2	[number]	k4	2
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
svatby	svatba	k1gFnPc1	svatba
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
pohřeb	pohřeb	k1gInSc1	pohřeb
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mr	Mr	k?	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bean	Bean	k1gNnSc1	Bean
<g/>
:	:	kIx,	:
Největší	veliký	k2eAgFnSc1d3	veliký
filmová	filmový	k2eAgFnSc1d1	filmová
katastrofa	katastrofa	k1gFnSc1	katastrofa
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bestián	Bestián	k1gMnSc1	Bestián
<g/>
:	:	kIx,	:
Kupředu	kupředu	k6eAd1	kupředu
<g/>
,	,	kIx,	,
zpátky	zpátky	k6eAd1	zpátky
jen	jen	k9	jen
krok	krok	k1gInSc1	krok
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Milionový	milionový	k2eAgInSc1d1	milionový
závod	závod	k1gInSc1	závod
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Scooby-Doo	Scooby-Doo	k1gNnSc1	Scooby-Doo
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Johnny	Johnn	k1gInPc1	Johnn
English	Englisha	k1gFnPc2	Englisha
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Johnny	Johnna	k1gFnPc1	Johnna
English	Englisha	k1gFnPc2	Englisha
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Láska	láska	k1gFnSc1	láska
nebeská	nebeský	k2eAgFnSc1d1	nebeská
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Univerzální	univerzální	k2eAgFnSc1d1	univerzální
uklízečka	uklízečka	k1gFnSc1	uklízečka
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Prázdniny	prázdniny	k1gFnPc1	prázdniny
pana	pan	k1gMnSc2	pan
Beana	Bean	k1gMnSc2	Bean
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Maigret	Maigret	k1gInSc1	Maigret
(	(	kIx(	(
<g/>
4	[number]	k4	4
TV	TV	kA	TV
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Johnny	Johnna	k1gFnPc1	Johnna
English	Englisha	k1gFnPc2	Englisha
znovu	znovu	k6eAd1	znovu
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rowan	Rowany	k1gInPc2	Rowany
Atkinson	Atkinsona	k1gFnPc2	Atkinsona
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Rowan	Rowana	k1gFnPc2	Rowana
Atkinson	Atkinsona	k1gFnPc2	Atkinsona
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
