<s>
Královská	královský	k2eAgFnSc1d1	královská
linie	linie	k1gFnSc1	linie
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
se	se	k3xPyFc4	se
váží	vážit	k5eAaImIp3nP	vážit
počátky	počátek	k1gInPc1	počátek
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
<g/>
,	,	kIx,	,
skončila	skončit	k5eAaPmAgFnS	skončit
nečekaně	nečekaně	k6eAd1	nečekaně
zavražděním	zavraždění	k1gNnSc7	zavraždění
mladého	mladý	k2eAgMnSc2d1	mladý
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1306	[number]	k4	1306
<g/>
.	.	kIx.	.
</s>
