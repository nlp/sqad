<s>
Michal	Michal	k1gMnSc1	Michal
Hašek	Hašek	k1gMnSc1	Hašek
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1976	[number]	k4	1976
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
sociálně-demokratický	sociálněemokratický	k2eAgMnSc1d1	sociálně-demokratický
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
zastupitel	zastupitel	k1gMnSc1	zastupitel
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
statutární	statutární	k2eAgMnSc1d1	statutární
místopředseda	místopředseda	k1gMnSc1	místopředseda
České	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnPc1d1	demokratická
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2008	[number]	k4	2008
a	a	k8xC	a
2016	[number]	k4	2016
byl	být	k5eAaImAgMnS	být
hejtmanem	hejtman	k1gMnSc7	hejtman
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
také	také	k9	také
předsedou	předseda	k1gMnSc7	předseda
Asociace	asociace	k1gFnSc2	asociace
krajů	kraj	k1gInPc2	kraj
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
a	a	k8xC	a
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
poslancem	poslanec	k1gMnSc7	poslanec
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
byl	být	k5eAaImAgInS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
oblíbeného	oblíbený	k2eAgMnSc4d1	oblíbený
politika	politik	k1gMnSc4	politik
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
hejtmany	hejtman	k1gMnPc7	hejtman
patřil	patřit	k5eAaImAgMnS	patřit
nejpopulárnějším	populární	k2eAgMnSc7d3	nejpopulárnější
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celé	celá	k1gFnSc2	celá
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
do	do	k7c2	do
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
trvale	trvale	k6eAd1	trvale
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
byl	být	k5eAaImAgInS	být
však	však	k9	však
hlavním	hlavní	k2eAgMnSc7d1	hlavní
aktérem	aktér	k1gMnSc7	aktér
několika	několik	k4yIc2	několik
kauz	kauza	k1gFnPc2	kauza
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Lánského	Lánského	k2eAgInPc4d1	Lánského
puče	puč	k1gInPc4	puč
části	část	k1gFnSc2	část
vedení	vedení	k1gNnSc2	vedení
ČSSD	ČSSD	kA	ČSSD
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Milošem	Miloš	k1gMnSc7	Miloš
Zemanem	Zeman	k1gMnSc7	Zeman
po	po	k7c6	po
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgNnSc6	který
lhal	lhát	k5eAaImAgMnS	lhát
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
televizním	televizní	k2eAgInSc6d1	televizní
přenosu	přenos	k1gInSc6	přenos
<g/>
,	,	kIx,	,
či	či	k8xC	či
kauzy	kauza	k1gFnPc1	kauza
s	s	k7c7	s
fiktivní	fiktivní	k2eAgFnSc7d1	fiktivní
tiskovou	tiskový	k2eAgFnSc7d1	tisková
mluvčí	mluvčí	k1gFnSc7	mluvčí
a	a	k8xC	a
placením	placení	k1gNnSc7	placení
propagačních	propagační	k2eAgInPc2d1	propagační
článků	článek	k1gInPc2	článek
v	v	k7c6	v
Parlamentních	parlamentní	k2eAgInPc6d1	parlamentní
listech	list	k1gInPc6	list
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
rozhořela	rozhořet	k5eAaPmAgFnS	rozhořet
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2016	[number]	k4	2016
jakožto	jakožto	k8xS	jakožto
lídr	lídr	k1gMnSc1	lídr
kandidátky	kandidátka	k1gFnSc2	kandidátka
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
znovu	znovu	k6eAd1	znovu
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
křeslo	křeslo	k1gNnSc4	křeslo
hejtmana	hejtman	k1gMnSc2	hejtman
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jako	jako	k9	jako
zastupitel	zastupitel	k1gMnSc1	zastupitel
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Maturoval	maturovat	k5eAaBmAgMnS	maturovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Tišnově	Tišnov	k1gInSc6	Tišnov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
dokončil	dokončit	k5eAaPmAgMnS	dokončit
Právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
diplomová	diplomový	k2eAgFnSc1d1	Diplomová
práce	práce	k1gFnSc1	práce
Přeměna	přeměna	k1gFnSc1	přeměna
družstva	družstvo	k1gNnSc2	družstvo
v	v	k7c4	v
jiný	jiný	k2eAgInSc4d1	jiný
podnikatelský	podnikatelský	k2eAgInSc4d1	podnikatelský
subjekt	subjekt	k1gInSc4	subjekt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
magistr	magistr	k1gMnSc1	magistr
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
pak	pak	k6eAd1	pak
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
doktora	doktor	k1gMnSc2	doktor
práv	právo	k1gNnPc2	právo
(	(	kIx(	(
<g/>
JUDr.	JUDr.	kA	JUDr.
<g/>
)	)	kIx)	)
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
soukromé	soukromý	k2eAgFnSc6d1	soukromá
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Sládkovičove	Sládkovičov	k1gInSc5	Sládkovičov
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vedl	vést	k5eAaImAgMnS	vést
slovenský	slovenský	k2eAgMnSc1d1	slovenský
sociálnědemokratický	sociálnědemokratický	k2eAgMnSc1d1	sociálnědemokratický
poslanec	poslanec	k1gMnSc1	poslanec
Mojmír	Mojmír	k1gMnSc1	Mojmír
Mamojka	Mamojka	k1gFnSc1	Mamojka
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
také	také	k9	také
konzultantem	konzultant	k1gMnSc7	konzultant
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Michal	Michal	k1gMnSc1	Michal
Hašek	Hašek	k1gMnSc1	Hašek
na	na	k7c6	na
základě	základ	k1gInSc6	základ
veřejného	veřejný	k2eAgInSc2d1	veřejný
zájmu	zájem	k1gInSc2	zájem
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zveřejní	zveřejnit	k5eAaPmIp3nS	zveřejnit
jak	jak	k6eAd1	jak
svoji	svůj	k3xOyFgFnSc4	svůj
rigorózní	rigorózní	k2eAgFnSc4d1	rigorózní
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
její	její	k3xOp3gInPc4	její
oponentské	oponentský	k2eAgInPc4d1	oponentský
posudky	posudek	k1gInPc4	posudek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
tak	tak	k6eAd1	tak
skutečně	skutečně	k6eAd1	skutečně
neučinil	učinit	k5eNaPmAgMnS	učinit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Veronikou	Veronika	k1gFnSc7	Veronika
má	mít	k5eAaImIp3nS	mít
dceru	dcera	k1gFnSc4	dcera
Justýnu	Justýna	k1gFnSc4	Justýna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
až	až	k9	až
1998	[number]	k4	1998
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
asistent	asistent	k1gMnSc1	asistent
starosty	starosta	k1gMnSc2	starosta
obce	obec	k1gFnSc2	obec
Drásov	Drásovo	k1gNnPc2	Drásovo
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
až	až	k9	až
2000	[number]	k4	2000
byl	být	k5eAaImAgInS	být
asistentem	asistent	k1gMnSc7	asistent
poslance	poslanec	k1gMnSc2	poslanec
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Členem	člen	k1gInSc7	člen
ČSSD	ČSSD	kA	ČSSD
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
a	a	k8xC	a
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
obce	obec	k1gFnSc2	obec
Drásov	Drásovo	k1gNnPc2	Drásovo
za	za	k7c4	za
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Profesně	profesně	k6eAd1	profesně
se	se	k3xPyFc4	se
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1998	[number]	k4	1998
uváděl	uvádět	k5eAaImAgMnS	uvádět
jako	jako	k9	jako
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
student	student	k1gMnSc1	student
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
starostou	starosta	k1gMnSc7	starosta
obce	obec	k1gFnSc2	obec
Drásov	Drásovo	k1gNnPc2	Drásovo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
později	pozdě	k6eAd2	pozdě
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
funkci	funkce	k1gFnSc4	funkce
místostarosty	místostarosta	k1gMnSc2	místostarosta
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
za	za	k7c4	za
Českou	český	k2eAgFnSc4d1	Česká
stranu	strana	k1gFnSc4	strana
sociálně	sociálně	k6eAd1	sociálně
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
(	(	kIx(	(
<g/>
volební	volební	k2eAgInSc1d1	volební
obvod	obvod	k1gInSc1	obvod
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
sněmovního	sněmovní	k2eAgInSc2d1	sněmovní
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
správu	správa	k1gFnSc4	správa
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgInSc4d1	regionální
rozvoj	rozvoj	k1gInSc4	rozvoj
a	a	k8xC	a
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC	a
členem	člen	k1gInSc7	člen
zemědělského	zemědělský	k2eAgInSc2d1	zemědělský
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
rovněž	rovněž	k9	rovněž
zasedal	zasedat	k5eAaImAgInS	zasedat
v	v	k7c6	v
organizačním	organizační	k2eAgInSc6d1	organizační
výboru	výbor	k1gInSc6	výbor
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
zastával	zastávat	k5eAaImAgMnS	zastávat
post	post	k1gInSc4	post
místopředsedy	místopředseda	k1gMnSc2	místopředseda
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
do	do	k7c2	do
konce	konec	k1gInSc2	konec
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
poslance	poslanec	k1gMnSc2	poslanec
obhájil	obhájit	k5eAaPmAgInS	obhájit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
místopředseda	místopředseda	k1gMnSc1	místopředseda
sněmovního	sněmovní	k2eAgInSc2d1	sněmovní
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
coby	coby	k?	coby
místopředseda	místopředseda	k1gMnSc1	místopředseda
zemědělského	zemědělský	k2eAgInSc2d1	zemědělský
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
navíc	navíc	k6eAd1	navíc
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
organizačního	organizační	k2eAgInSc2d1	organizační
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
červen	červeno	k1gNnPc2	červeno
2006	[number]	k4	2006
-	-	kIx~	-
prosinec	prosinec	k1gInSc4	prosinec
2008	[number]	k4	2008
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
byl	být	k5eAaImAgMnS	být
navíc	navíc	k6eAd1	navíc
stínovým	stínový	k2eAgMnSc7d1	stínový
ministrem	ministr	k1gMnSc7	ministr
zemědělství	zemědělství	k1gNnSc2	zemědělství
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sněmovně	sněmovna	k1gFnSc6	sněmovna
setrval	setrvat	k5eAaPmAgMnS	setrvat
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c4	na
poslanecký	poslanecký	k2eAgInSc4d1	poslanecký
mandát	mandát	k1gInSc4	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	s	k7c7	s
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2009	[number]	k4	2009
Michal	Michal	k1gMnSc1	Michal
Hašek	Hašek	k1gMnSc1	Hašek
vzdal	vzdát	k5eAaPmAgMnS	vzdát
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
mandátu	mandát	k1gInSc2	mandát
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
zdůvodnil	zdůvodnit	k5eAaPmAgMnS	zdůvodnit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
možné	možný	k2eAgNnSc1d1	možné
kvalitně	kvalitně	k6eAd1	kvalitně
zvládat	zvládat	k5eAaImF	zvládat
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
jihomoravský	jihomoravský	k2eAgMnSc1d1	jihomoravský
hejtman	hejtman	k1gMnSc1	hejtman
a	a	k8xC	a
poslanec	poslanec	k1gMnSc1	poslanec
českého	český	k2eAgInSc2d1	český
Parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2008	[number]	k4	2008
v	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
totiž	totiž	k9	totiž
kandidoval	kandidovat	k5eAaImAgInS	kandidovat
na	na	k7c4	na
hejtmana	hejtman	k1gMnSc4	hejtman
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
jeho	jeho	k3xOp3gNnSc4	jeho
vedením	vedení	k1gNnSc7	vedení
ČSSD	ČSSD	kA	ČSSD
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
a	a	k8xC	a
Michal	Michal	k1gMnSc1	Michal
Hašek	Hašek	k1gMnSc1	Hašek
se	s	k7c7	s
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
stal	stát	k5eAaPmAgMnS	stát
hejtmanem	hejtman	k1gMnSc7	hejtman
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Hejtmanský	hejtmanský	k2eAgInSc1d1	hejtmanský
post	post	k1gInSc1	post
obhájil	obhájit	k5eAaPmAgInS	obhájit
i	i	k9	i
v	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
skončila	skončit	k5eAaPmAgFnS	skončit
ČSSD	ČSSD	kA	ČSSD
s	s	k7c7	s
Michalem	Michal	k1gMnSc7	Michal
Haškem	Hašek	k1gMnSc7	Hašek
jako	jako	k8xS	jako
lídrem	lídr	k1gMnSc7	lídr
kandidátky	kandidátka	k1gFnSc2	kandidátka
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
funkci	funkce	k1gFnSc4	funkce
předsedy	předseda	k1gMnSc2	předseda
Asociace	asociace	k1gFnSc2	asociace
krajů	kraj	k1gInPc2	kraj
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
zemědělského	zemědělský	k2eAgInSc2d1	zemědělský
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
volebního	volební	k2eAgInSc2d1	volební
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Poslancem	poslanec	k1gMnSc7	poslanec
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
z	z	k7c2	z
nevolitelného	volitelný	k2eNgMnSc2d1	nevolitelný
11	[number]	k4	11
<g/>
.	.	kIx.	.
místa	místo	k1gNnSc2	místo
kandidátky	kandidátka	k1gFnSc2	kandidátka
ČSSD	ČSSD	kA	ČSSD
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
preferenčními	preferenční	k2eAgInPc7d1	preferenční
hlasy	hlas	k1gInPc7	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
přitom	přitom	k6eAd1	přitom
nejvíce	nejvíce	k6eAd1	nejvíce
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
voličů	volič	k1gMnPc2	volič
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
počet	počet	k1gInSc1	počet
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
zvolených	zvolený	k2eAgMnPc2d1	zvolený
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Jiřího	Jiří	k1gMnSc2	Jiří
Paroubka	Paroubek	k1gMnSc2	Paroubek
z	z	k7c2	z
čela	čelo	k1gNnSc2	čelo
ČSSD	ČSSD	kA	ČSSD
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Michal	Michal	k1gMnSc1	Michal
Hašek	Hašek	k1gMnSc1	Hašek
svoji	svůj	k3xOyFgFnSc4	svůj
kandidaturu	kandidatura	k1gFnSc4	kandidatura
na	na	k7c4	na
předsedu	předseda	k1gMnSc4	předseda
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
36	[number]	k4	36
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc2	sjezd
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
vyrovnaném	vyrovnaný	k2eAgInSc6d1	vyrovnaný
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
předsedou	předseda	k1gMnSc7	předseda
ČSSD	ČSSD	kA	ČSSD
Bohuslavem	Bohuslav	k1gMnSc7	Bohuslav
Sobotkou	Sobotka	k1gMnSc7	Sobotka
o	o	k7c6	o
funkci	funkce	k1gFnSc6	funkce
předsedy	předseda	k1gMnSc2	předseda
ČSSD	ČSSD	kA	ČSSD
zvolen	zvolit	k5eAaPmNgMnS	zvolit
statutárním	statutární	k2eAgMnSc7d1	statutární
místopředsedou	místopředseda	k1gMnSc7	místopředseda
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČSSD	ČSSD	kA	ČSSD
pak	pak	k6eAd1	pak
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
KSČM	KSČM	kA	KSČM
a	a	k8xC	a
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
KDU-ČSL	KDU-ČSL	k1gFnSc7	KDU-ČSL
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
i	i	k8xC	i
s	s	k7c7	s
SPOZ	SPOZ	kA	SPOZ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2009	[number]	k4	2009
podala	podat	k5eAaPmAgFnS	podat
společnost	společnost	k1gFnSc1	společnost
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnPc4	Agenca
na	na	k7c4	na
Haška	Hašek	k1gMnSc4	Hašek
trestní	trestní	k2eAgNnSc1d1	trestní
oznámení	oznámení	k1gNnSc1	oznámení
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gInPc3	jeho
výrokům	výrok	k1gInPc3	výrok
o	o	k7c6	o
"	"	kIx"	"
<g/>
vyzobávání	vyzobávání	k1gNnPc1	vyzobávání
rozinek	rozinka	k1gFnPc2	rozinka
<g/>
"	"	kIx"	"
-	-	kIx~	-
lukrativních	lukrativní	k2eAgFnPc2d1	lukrativní
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
(	(	kIx(	(
<g/>
M.	M.	kA	M.
Hašek	Hašek	k1gMnSc1	Hašek
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Opakuji	opakovat	k5eAaImIp1nS	opakovat
<g/>
,	,	kIx,	,
pan	pan	k1gMnSc1	pan
Jančura	Jančur	k1gMnSc2	Jančur
vyzobal	vyzobat	k5eAaPmAgInS	vyzobat
rozinky	rozinka	k1gFnPc4	rozinka
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
podal	podat	k5eAaPmAgMnS	podat
nabídku	nabídka	k1gFnSc4	nabídka
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
některé	některý	k3yIgFnSc6	některý
trati	trať	k1gFnSc6	trať
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zápisu	zápis	k1gInSc2	zápis
z	z	k7c2	z
jednání	jednání	k1gNnSc2	jednání
její	její	k3xOp3gFnSc2	její
dceřiné	dceřiný	k2eAgFnSc2d1	dceřiná
společnosti	společnost	k1gFnSc2	společnost
RegioJet	RegioJeta	k1gFnPc2	RegioJeta
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
kraje	kraj	k1gInSc2	kraj
vybral	vybrat	k5eAaPmAgInS	vybrat
tratě	trať	k1gFnSc2	trať
sám	sám	k3xTgMnSc1	sám
objednatel	objednatel	k1gMnSc1	objednatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Jana	Jan	k1gMnSc2	Jan
Paroubka	Paroubek	k1gMnSc2	Paroubek
navíc	navíc	k6eAd1	navíc
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tratě	trať	k1gFnPc1	trať
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
hrazení	hrazení	k1gNnSc4	hrazení
prokazatelné	prokazatelný	k2eAgFnSc2d1	prokazatelná
ztráty	ztráta	k1gFnSc2	ztráta
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
"	"	kIx"	"
<g/>
rozinkami	rozinka	k1gFnPc7	rozinka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vytvoření	vytvoření	k1gNnSc6	vytvoření
nové	nový	k2eAgFnSc2d1	nová
krajské	krajský	k2eAgFnSc2d1	krajská
koalice	koalice	k1gFnSc2	koalice
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
(	(	kIx(	(
<g/>
srpen	srpen	k1gInSc1	srpen
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
avizoval	avizovat	k5eAaBmAgInS	avizovat
záměr	záměr	k1gInSc4	záměr
otevření	otevření	k1gNnSc2	otevření
regionální	regionální	k2eAgFnSc2d1	regionální
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Kraj	kraj	k1gInSc1	kraj
plánuje	plánovat	k5eAaImIp3nS	plánovat
vypsat	vypsat	k5eAaPmF	vypsat
tendr	tendr	k1gInSc4	tendr
na	na	k7c4	na
55	[number]	k4	55
procent	procento	k1gNnPc2	procento
tratí	trať	k1gFnPc2	trať
na	na	k7c6	na
území	území	k1gNnSc6	území
jižní	jižní	k2eAgFnSc2d1	jižní
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Cestujícím	cestující	k1gMnPc3	cestující
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
tendr	tendr	k1gInSc1	tendr
přinést	přinést	k5eAaPmF	přinést
nové	nový	k2eAgFnPc4d1	nová
soupravy	souprava	k1gFnPc4	souprava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
budou	být	k5eAaImBp3nP	být
podmínkou	podmínka	k1gFnSc7	podmínka
účasti	účast	k1gFnSc3	účast
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2016	[number]	k4	2016
přinesl	přinést	k5eAaPmAgInS	přinést
deník	deník	k1gInSc1	deník
MF	MF	kA	MF
Dnes	dnes	k6eAd1	dnes
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hašek	Hašek	k1gMnSc1	Hašek
nezná	znát	k5eNaImIp3nS	znát
Lucii	Lucie	k1gFnSc4	Lucie
Proutníkovou	proutníkův	k2eAgFnSc7d1	proutníkův
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
několik	několik	k4yIc4	několik
předchozích	předchozí	k2eAgNnPc2d1	předchozí
let	léto	k1gNnPc2	léto
zasílala	zasílat	k5eAaImAgFnS	zasílat
jeho	jeho	k3xOp3gFnSc1	jeho
výroky	výrok	k1gInPc4	výrok
médiím	médium	k1gNnPc3	médium
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
deníku	deník	k1gInSc2	deník
se	se	k3xPyFc4	se
za	za	k7c7	za
fiktivním	fiktivní	k2eAgNnSc7d1	fiktivní
jménem	jméno	k1gNnSc7	jméno
údajně	údajně	k6eAd1	údajně
skrývá	skrývat	k5eAaImIp3nS	skrývat
kontroverzní	kontroverzní	k2eAgFnSc1d1	kontroverzní
lobbistka	lobbistka	k1gFnSc1	lobbistka
Jana	Jana	k1gFnSc1	Jana
Mrencová	Mrencový	k2eAgFnSc1d1	Mrencová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
nepravomocně	pravomocně	k6eNd1	pravomocně
odsouzena	odsouzet	k5eAaImNgFnS	odsouzet
za	za	k7c4	za
nabízení	nabízení	k1gNnSc4	nabízení
úplatku	úplatek	k1gInSc2	úplatek
k	k	k7c3	k
ovlivnění	ovlivnění	k1gNnSc3	ovlivnění
novinářů	novinář	k1gMnPc2	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Deník	deník	k1gInSc1	deník
dále	daleko	k6eAd2	daleko
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hašek	Hašek	k1gMnSc1	Hašek
se	se	k3xPyFc4	se
u	u	k7c2	u
partnera	partner	k1gMnSc2	partner
Mrencové	Mrencový	k2eAgFnSc2d1	Mrencová
z	z	k7c2	z
veřejných	veřejný	k2eAgInPc2d1	veřejný
peněz	peníze	k1gInPc2	peníze
objednal	objednat	k5eAaPmAgInS	objednat
před	před	k7c7	před
krajskými	krajský	k2eAgFnPc7d1	krajská
volbami	volba	k1gFnPc7	volba
marketingové	marketingový	k2eAgFnSc2d1	marketingová
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
400	[number]	k4	400
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Mrencové	Mrencový	k2eAgFnSc2d1	Mrencová
následně	následně	k6eAd1	následně
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
bývalá	bývalý	k2eAgFnSc1d1	bývalá
krajská	krajský	k2eAgFnSc1d1	krajská
tisková	tiskový	k2eAgFnSc1d1	tisková
mluvčí	mluvčí	k1gFnSc1	mluvčí
Denisa	Denisa	k1gFnSc1	Denisa
Kapitančiková	Kapitančikový	k2eAgFnSc1d1	Kapitančiková
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dodala	dodat	k5eAaPmAgFnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lobbistka	lobbistka	k1gFnSc1	lobbistka
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
s	s	k7c7	s
úřadem	úřad	k1gInSc7	úřad
opakovaně	opakovaně	k6eAd1	opakovaně
uzavíranou	uzavíraný	k2eAgFnSc4d1	uzavíraná
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
částku	částka	k1gFnSc4	částka
kolem	kolem	k7c2	kolem
200	[number]	k4	200
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
na	na	k7c4	na
poskytování	poskytování	k1gNnSc4	poskytování
mediálních	mediální	k2eAgFnPc2d1	mediální
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
propagace	propagace	k1gFnSc2	propagace
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pátrání	pátrání	k1gNnSc6	pátrání
po	po	k7c6	po
platbách	platba	k1gFnPc6	platba
pro	pro	k7c4	pro
Mrencovou	Mrencový	k2eAgFnSc4d1	Mrencová
novináři	novinář	k1gMnSc3	novinář
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
zaplatil	zaplatit	k5eAaPmAgInS	zaplatit
minimálně	minimálně	k6eAd1	minimálně
650	[number]	k4	650
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
i	i	k8xC	i
firmě	firma	k1gFnSc3	firma
JT	JT	kA	JT
Media	medium	k1gNnSc2	medium
za	za	k7c4	za
publikaci	publikace	k1gFnSc4	publikace
pozitivních	pozitivní	k2eAgInPc2d1	pozitivní
článků	článek	k1gInPc2	článek
během	během	k7c2	během
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
na	na	k7c6	na
zpravodajském	zpravodajský	k2eAgInSc6d1	zpravodajský
serveru	server	k1gInSc6	server
ParlamentníListy	ParlamentníLista	k1gMnSc2	ParlamentníLista
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
firem	firma	k1gFnPc2	firma
Mrencové	Mrencový	k2eAgFnSc2d1	Mrencová
či	či	k8xC	či
jejího	její	k3xOp3gMnSc2	její
druha	druh	k1gMnSc2	druh
Miloše	Miloš	k1gMnSc2	Miloš
Skácela	Skácel	k1gMnSc2	Skácel
si	se	k3xPyFc3	se
kraj	kraj	k7c2	kraj
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
objednával	objednávat	k5eAaImAgMnS	objednávat
fotografické	fotografický	k2eAgFnPc4d1	fotografická
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
ale	ale	k8xC	ale
podle	podle	k7c2	podle
idnes	idnesa	k1gFnPc2	idnesa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
práci	práce	k1gFnSc4	práce
vykonali	vykonat	k5eAaPmAgMnP	vykonat
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
tiskového	tiskový	k2eAgInSc2d1	tiskový
odboru	odbor	k1gInSc2	odbor
krajského	krajský	k2eAgInSc2d1	krajský
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
přikázáno	přikázán	k2eAgNnSc4d1	přikázáno
výsledek	výsledek	k1gInSc4	výsledek
práce	práce	k1gFnSc2	práce
poslat	poslat	k5eAaPmF	poslat
Mrencové	Mrencový	k2eAgInPc4d1	Mrencový
<g/>
;	;	kIx,	;
ta	ten	k3xDgFnSc1	ten
jej	on	k3xPp3gMnSc4	on
pak	pak	k6eAd1	pak
krajskému	krajský	k2eAgInSc3d1	krajský
úřadu	úřad	k1gInSc3	úřad
posílala	posílat	k5eAaImAgFnS	posílat
zpátky	zpátky	k6eAd1	zpátky
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
prověřili	prověřit	k5eAaPmAgMnP	prověřit
smlouvy	smlouva	k1gFnSc2	smlouva
uzavřené	uzavřený	k2eAgFnPc4d1	uzavřená
mezi	mezi	k7c7	mezi
krajským	krajský	k2eAgInSc7d1	krajský
úřadem	úřad	k1gInSc7	úřad
a	a	k8xC	a
firmami	firma	k1gFnPc7	firma
propojenými	propojený	k2eAgInPc7d1	propojený
s	s	k7c7	s
Janou	Jana	k1gFnSc7	Jana
Mrencovou	Mrencův	k2eAgFnSc7d1	Mrencův
členové	člen	k1gMnPc1	člen
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
výboru	výbor	k1gInSc2	výbor
jihomoravského	jihomoravský	k2eAgNnSc2d1	Jihomoravské
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Dospěli	dochvít	k5eAaPmAgMnP	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyl	být	k5eNaImAgInS	být
porušen	porušen	k2eAgInSc1d1	porušen
zákon	zákon	k1gInSc1	zákon
ani	ani	k8xC	ani
interní	interní	k2eAgInPc1d1	interní
předpisy	předpis	k1gInPc1	předpis
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
závěr	závěr	k1gInSc4	závěr
podpořili	podpořit	k5eAaPmAgMnP	podpořit
ve	v	k7c6	v
výboru	výbor	k1gInSc6	výbor
i	i	k8xC	i
jeho	jeho	k3xOp3gMnPc1	jeho
členové	člen	k1gMnPc1	člen
z	z	k7c2	z
opozičních	opoziční	k2eAgFnPc2d1	opoziční
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
krajské	krajský	k2eAgFnSc6d1	krajská
konferenci	konference	k1gFnSc6	konference
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
Tišnově	Tišnov	k1gInSc6	Tišnov
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2015	[number]	k4	2015
získal	získat	k5eAaPmAgMnS	získat
nominaci	nominace	k1gFnSc4	nominace
lídra	lídr	k1gMnSc2	lídr
kandidátky	kandidátka	k1gFnSc2	kandidátka
do	do	k7c2	do
krajských	krajský	k2eAgFnPc2d1	krajská
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
Haškovu	Haškův	k2eAgFnSc4d1	Haškova
nominaci	nominace	k1gFnSc4	nominace
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
vedení	vedení	k1gNnSc1	vedení
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
schválilo	schválit	k5eAaPmAgNnS	schválit
také	také	k9	také
stranické	stranický	k2eAgNnSc1d1	stranické
referendum	referendum	k1gNnSc1	referendum
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgInS	získat
65	[number]	k4	65
<g/>
procentní	procentní	k2eAgFnSc4d1	procentní
podporu	podpora	k1gFnSc4	podpora
spolustraníků	spolustraník	k1gMnPc2	spolustraník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
zvolen	zvolit	k5eAaPmNgInS	zvolit
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
lídra	lídr	k1gMnSc2	lídr
kandidátky	kandidátka	k1gFnSc2	kandidátka
ČSSD	ČSSD	kA	ČSSD
zastupitelem	zastupitel	k1gMnSc7	zastupitel
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
sice	sice	k8xC	sice
8	[number]	k4	8
103	[number]	k4	103
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
strana	strana	k1gFnSc1	strana
skončila	skončit	k5eAaPmAgFnS	skončit
až	až	k9	až
třetí	třetí	k4xOgFnSc1	třetí
za	za	k7c2	za
hnutím	hnutí	k1gNnSc7	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
a	a	k8xC	a
KDU-ČSL	KDU-ČSL	k1gMnSc1	KDU-ČSL
<g/>
.	.	kIx.	.
</s>
<s>
ČSSD	ČSSD	kA	ČSSD
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
sice	sice	k8xC	sice
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
krajské	krajský	k2eAgFnSc2d1	krajská
koalice	koalice	k1gFnSc2	koalice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samotný	samotný	k2eAgMnSc1d1	samotný
Hašek	Hašek	k1gMnSc1	Hašek
jako	jako	k8xC	jako
hejtman	hejtman	k1gMnSc1	hejtman
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2016	[number]	k4	2016
skončil	skončit	k5eAaPmAgMnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
řadový	řadový	k2eAgMnSc1d1	řadový
zastupitel	zastupitel	k1gMnSc1	zastupitel
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
advokátního	advokátní	k2eAgMnSc2d1	advokátní
koncipienta	koncipient	k1gMnSc2	koncipient
v	v	k7c6	v
Advokátní	advokátní	k2eAgFnSc6d1	advokátní
kanceláři	kancelář	k1gFnSc6	kancelář
Jansta	Jansta	k1gMnSc1	Jansta
<g/>
,	,	kIx,	,
Kostka	Kostka	k1gMnSc1	Kostka
(	(	kIx(	(
<g/>
spoluvlastněné	spoluvlastněný	k2eAgFnSc2d1	spoluvlastněná
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Janstou	Jansta	k1gMnSc7	Jansta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
vyjádření	vyjádření	k1gNnSc2	vyjádření
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
působil	působit	k5eAaImAgMnS	působit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
srpna	srpen	k1gInSc2	srpen
2016	[number]	k4	2016
předestřela	předestřít	k5eAaPmAgFnS	předestřít
některá	některý	k3yIgNnPc4	některý
media	medium	k1gNnPc4	medium
podezření	podezření	k1gNnSc2	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
Michal	Michal	k1gMnSc1	Michal
Hašek	Hašek	k1gMnSc1	Hašek
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
z	z	k7c2	z
antikampaní	antikampaný	k2eAgMnPc1d1	antikampaný
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
spolek	spolek	k1gInSc1	spolek
Brno	Brno	k1gNnSc1	Brno
<g/>
+	+	kIx~	+
navádí	navádět	k5eAaImIp3nP	navádět
občany	občan	k1gMnPc7	občan
Brna	Brno	k1gNnSc2	Brno
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vzdali	vzdát	k5eAaPmAgMnP	vzdát
účasti	účast	k1gFnSc2	účast
v	v	k7c6	v
místním	místní	k2eAgNnSc6d1	místní
referendu	referendum	k1gNnSc6	referendum
o	o	k7c6	o
poloze	poloha	k1gFnSc6	poloha
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
úvahu	úvaha	k1gFnSc4	úvaha
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
svá	svůj	k3xOyFgNnPc4	svůj
pátraní	pátraný	k2eAgMnPc1d1	pátraný
v	v	k7c6	v
registru	registr	k1gInSc6	registr
smluv	smlouva	k1gFnPc2	smlouva
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolek	spolek	k1gInSc1	spolek
získal	získat	k5eAaPmAgInS	získat
od	od	k7c2	od
kraje	kraj	k1gInSc2	kraj
dotaci	dotace	k1gFnSc4	dotace
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
197	[number]	k4	197
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
</s>
<s>
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Dotaci	dotace	k1gFnSc4	dotace
schválila	schválit	k5eAaPmAgFnS	schválit
rada	rada	k1gFnSc1	rada
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
Michal	Michal	k1gMnSc1	Michal
Hašek	Hašek	k1gMnSc1	Hašek
předsedá	předsedat	k5eAaImIp3nS	předsedat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
září	září	k1gNnSc2	září
se	se	k3xPyFc4	se
kraj	kraj	k1gInSc1	kraj
za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
jednostranné	jednostranný	k2eAgNnSc4d1	jednostranné
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
omluvil	omluvit	k5eAaPmAgMnS	omluvit
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c6	o
poskytnutí	poskytnutí	k1gNnSc6	poskytnutí
stejné	stejný	k2eAgFnSc2d1	stejná
částky	částka	k1gFnSc2	částka
i	i	k8xC	i
přípravnému	přípravný	k2eAgInSc3d1	přípravný
výboru	výbor	k1gInSc3	výbor
referenda	referendum	k1gNnSc2	referendum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
hejtman	hejtman	k1gMnSc1	hejtman
Hašek	Hašek	k1gMnSc1	Hašek
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
pro	pro	k7c4	pro
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
krajského	krajský	k2eAgNnSc2d1	krajské
referenda	referendum	k1gNnSc2	referendum
k	k	k7c3	k
otázce	otázka	k1gFnSc3	otázka
polohy	poloha	k1gFnSc2	poloha
nádraží	nádraží	k1gNnSc2	nádraží
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
předsevzetí	předsevzetí	k1gNnSc3	předsevzetí
nebyla	být	k5eNaImAgFnS	být
ale	ale	k9	ale
převedena	převést	k5eAaPmNgFnS	převést
ve	v	k7c4	v
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
jako	jako	k9	jako
druhý	druhý	k4xOgMnSc1	druhý
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
za	za	k7c7	za
Bohuslavem	Bohuslav	k1gMnSc7	Bohuslav
Sobotkou	Sobotka	k1gMnSc7	Sobotka
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
však	však	k9	však
25	[number]	k4	25
531	[number]	k4	531
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
Sobotka	Sobotka	k1gMnSc1	Sobotka
jen	jen	k9	jen
22	[number]	k4	22
175	[number]	k4	175
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
tak	tak	k6eAd1	tak
zvolen	zvolit	k5eAaPmNgMnS	zvolit
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
poté	poté	k6eAd1	poté
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
brněnským	brněnský	k2eAgMnSc7d1	brněnský
primátorem	primátor	k1gMnSc7	primátor
Romanem	Roman	k1gMnSc7	Roman
Onderkou	Onderka	k1gMnSc7	Onderka
na	na	k7c4	na
jednání	jednání	k1gNnSc4	jednání
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
Lidovém	lidový	k2eAgInSc6d1	lidový
domě	dům	k1gInSc6	dům
vyloučení	vyloučení	k1gNnSc2	vyloučení
předsedy	předseda	k1gMnSc2	předseda
strany	strana	k1gFnSc2	strana
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Sobotky	Sobotka	k1gMnSc2	Sobotka
z	z	k7c2	z
vyjednávacího	vyjednávací	k2eAgInSc2d1	vyjednávací
týmu	tým	k1gInSc2	tým
o	o	k7c6	o
nové	nový	k2eAgFnSc6d1	nová
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Ústředním	ústřední	k2eAgInSc7d1	ústřední
výkonným	výkonný	k2eAgInSc7d1	výkonný
výborem	výbor	k1gInSc7	výbor
ČSSD	ČSSD	kA	ČSSD
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
budoucího	budoucí	k2eAgMnSc4d1	budoucí
premiéra	premiér	k1gMnSc4	premiér
jako	jako	k8xS	jako
hlava	hlava	k1gFnSc1	hlava
vyjednávacího	vyjednávací	k2eAgInSc2d1	vyjednávací
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
také	také	k9	také
Bohuslava	Bohuslav	k1gMnSc4	Bohuslav
Sobotku	Sobotka	k1gMnSc4	Sobotka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
postu	post	k1gInSc2	post
předsedy	předseda	k1gMnSc2	předseda
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odůvodnil	odůvodnit	k5eAaPmAgInS	odůvodnit
nižším	nízký	k2eAgInSc7d2	nižší
ziskem	zisk	k1gInSc7	zisk
získaných	získaný	k2eAgInPc2d1	získaný
hlasů	hlas	k1gInPc2	hlas
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
čekalo	čekat	k5eAaImAgNnS	čekat
<g/>
.	.	kIx.	.
</s>
<s>
Sobotka	Sobotka	k1gMnSc1	Sobotka
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc3	tento
výzvě	výzva	k1gFnSc3	výzva
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
podřídit	podřídit	k5eAaPmF	podřídit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vzápětí	vzápětí	k6eAd1	vzápětí
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
mladých	mladý	k2eAgMnPc2d1	mladý
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
začala	začít	k5eAaPmAgFnS	začít
formovat	formovat	k5eAaImF	formovat
podpora	podpora	k1gFnSc1	podpora
předsedy	předseda	k1gMnSc2	předseda
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
vývoj	vývoj	k1gInSc1	vývoj
dostal	dostat	k5eAaPmAgInS	dostat
živelný	živelný	k2eAgInSc1d1	živelný
spád	spád	k1gInSc1	spád
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
spíše	spíše	k9	spíše
ukázal	ukázat	k5eAaPmAgInS	ukázat
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
podporu	podpora	k1gFnSc4	podpora
předsedy	předseda	k1gMnSc2	předseda
Sobotky	Sobotka	k1gMnSc2	Sobotka
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
Michal	Michal	k1gMnSc1	Michal
Hašek	Hašek	k1gMnSc1	Hašek
původně	původně	k6eAd1	původně
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
.	.	kIx.	.
</s>
<s>
Budoucí	budoucí	k2eAgMnPc1d1	budoucí
koaliční	koaliční	k2eAgMnPc1d1	koaliční
partneři	partner	k1gMnPc1	partner
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Babiš	Babiš	k1gInSc1	Babiš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
veřejnost	veřejnost	k1gFnSc1	veřejnost
samotná	samotný	k2eAgFnSc1d1	samotná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
také	také	k9	také
přiklonili	přiklonit	k5eAaPmAgMnP	přiklonit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
řádně	řádně	k6eAd1	řádně
zvoleného	zvolený	k2eAgMnSc2d1	zvolený
předsedy	předseda	k1gMnSc2	předseda
B.	B.	kA	B.
Sobotky	Sobotka	k1gMnSc2	Sobotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
nedělní	nedělní	k2eAgFnSc7d1	nedělní
výzvou	výzva	k1gFnSc7	výzva
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
probírána	probírán	k2eAgFnSc1d1	probírána
tajná	tajný	k2eAgFnSc1d1	tajná
schůzka	schůzka	k1gFnSc1	schůzka
u	u	k7c2	u
prezidenta	prezident	k1gMnSc2	prezident
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgInPc4	který
měli	mít	k5eAaImAgMnP	mít
dostat	dostat	k5eAaPmF	dostat
instrukce	instrukce	k1gFnPc4	instrukce
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
postupu	postup	k1gInSc3	postup
<g/>
.	.	kIx.	.
</s>
<s>
Michal	Michal	k1gMnSc1	Michal
Hašek	Hašek	k1gMnSc1	Hašek
tuto	tento	k3xDgFnSc4	tento
zmíněnou	zmíněný	k2eAgFnSc4d1	zmíněná
schůzku	schůzka	k1gFnSc4	schůzka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nepřímo	přímo	k6eNd1	přímo
monitorovala	monitorovat	k5eAaImAgFnS	monitorovat
některá	některý	k3yIgNnPc4	některý
česká	český	k2eAgNnPc4d1	české
media	medium	k1gNnPc4	medium
<g/>
,	,	kIx,	,
v	v	k7c6	v
živě	živě	k6eAd1	živě
vysílaném	vysílaný	k2eAgInSc6d1	vysílaný
pořadu	pořad	k1gInSc6	pořad
na	na	k7c6	na
ČT24	ČT24	k1gFnSc6	ČT24
Interview	interview	k1gNnSc1	interview
Daniely	Daniela	k1gFnSc2	Daniela
Drtinové	drtinový	k2eAgFnSc2d1	Drtinová
popřel	popřít	k5eAaPmAgMnS	popřít
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Chovanec	Chovanec	k1gMnSc1	Chovanec
<g/>
,	,	kIx,	,
její	její	k3xOp3gMnSc1	její
další	další	k2eAgMnSc1d1	další
účastník	účastník	k1gMnSc1	účastník
ji	on	k3xPp3gFnSc4	on
naopak	naopak	k6eAd1	naopak
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Michal	Michal	k1gMnSc1	Michal
Hašek	Hašek	k1gMnSc1	Hašek
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
postupném	postupný	k2eAgInSc6d1	postupný
vývoji	vývoj	k1gInSc6	vývoj
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
Sobotkových	Sobotkových	k2eAgMnPc2d1	Sobotkových
sympatizantů	sympatizant	k1gMnPc2	sympatizant
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
obecně	obecně	k6eAd1	obecně
stigma	stigma	k1gNnSc1	stigma
"	"	kIx"	"
<g/>
pučisty	pučista	k1gMnPc7	pučista
<g/>
"	"	kIx"	"
a	a	k8xC	a
zrádce	zrádce	k1gMnSc1	zrádce
a	a	k8xC	a
dočkal	dočkat	k5eAaPmAgMnS	dočkat
se	se	k3xPyFc4	se
množících	množící	k2eAgFnPc2d1	množící
se	se	k3xPyFc4	se
výzev	výzva	k1gFnPc2	výzva
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
z	z	k7c2	z
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Hašek	Hašek	k1gMnSc1	Hašek
o	o	k7c6	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
dané	daný	k2eAgFnSc6d1	daná
schůzce	schůzka	k1gFnSc6	schůzka
skutečně	skutečně	k6eAd1	skutečně
byl	být	k5eAaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
médiích	médium	k1gNnPc6	médium
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgMnS	označit
za	za	k7c4	za
sběratele	sběratel	k1gMnPc4	sběratel
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
politického	politický	k2eAgMnSc2d1	politický
lháře	lhář	k1gMnSc2	lhář
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Škromach	Škromach	k1gMnSc1	Škromach
oznámil	oznámit	k5eAaPmAgMnS	oznámit
svou	svůj	k3xOyFgFnSc4	svůj
rezignaci	rezignace	k1gFnSc4	rezignace
na	na	k7c4	na
místopředsedu	místopředseda	k1gMnSc4	místopředseda
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
stranických	stranický	k2eAgFnPc2d1	stranická
funkcí	funkce	k1gFnPc2	funkce
i	i	k9	i
Hašek	Hašek	k1gMnSc1	Hašek
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
učinil	učinit	k5eAaImAgInS	učinit
stejný	stejný	k2eAgInSc4d1	stejný
krok	krok	k1gInSc4	krok
také	také	k6eAd1	také
další	další	k2eAgMnSc1d1	další
účastník	účastník	k1gMnSc1	účastník
tajné	tajný	k2eAgFnSc2d1	tajná
schůzky	schůzka	k1gFnSc2	schůzka
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Jeroným	Jeroným	k1gMnSc1	Jeroným
Tejc	Tejc	k1gFnSc1	Tejc
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
Hašek	Hašek	k1gMnSc1	Hašek
zdůvodnil	zdůvodnit	k5eAaPmAgInS	zdůvodnit
reflexí	reflexe	k1gFnSc7	reflexe
špatného	špatný	k2eAgInSc2d1	špatný
volebního	volební	k2eAgInSc2d1	volební
výsledku	výsledek	k1gInSc2	výsledek
<g/>
,	,	kIx,	,
nutností	nutnost	k1gFnSc7	nutnost
zklidnit	zklidnit	k5eAaPmF	zklidnit
vnitrostranickou	vnitrostranický	k2eAgFnSc4d1	vnitrostranická
situaci	situace	k1gFnSc4	situace
a	a	k8xC	a
negativní	negativní	k2eAgFnSc7d1	negativní
mediální	mediální	k2eAgFnSc7d1	mediální
kampaní	kampaň	k1gFnSc7	kampaň
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
osobě	osoba	k1gFnSc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Zimolou	Zimola	k1gMnSc7	Zimola
<g/>
,	,	kIx,	,
Josefem	Josef	k1gMnSc7	Josef
Novotným	Novotný	k1gMnSc7	Novotný
a	a	k8xC	a
Jiřím	Jiří	k1gMnSc7	Jiří
Běhounkem	běhounek	k1gMnSc7	běhounek
veřejně	veřejně	k6eAd1	veřejně
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
nejbližší	blízký	k2eAgFnSc6d3	nejbližší
době	doba	k1gFnSc6	doba
definitivně	definitivně	k6eAd1	definitivně
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
si	se	k3xPyFc3	se
ponechají	ponechat	k5eAaPmIp3nP	ponechat
svůj	svůj	k3xOyFgInSc4	svůj
mandát	mandát	k1gInSc4	mandát
hejtmana	hejtman	k1gMnSc2	hejtman
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
poslance	poslanec	k1gMnPc4	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Dodali	dodat	k5eAaPmAgMnP	dodat
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
předloží	předložit	k5eAaPmIp3nS	předložit
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
návrh	návrh	k1gInSc4	návrh
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yRgNnSc6	který
budou	být	k5eAaImBp3nP	být
dané	daný	k2eAgFnPc1d1	daná
legislativní	legislativní	k2eAgFnPc1d1	legislativní
změny	změna	k1gFnPc1	změna
reflektovat	reflektovat	k5eAaImF	reflektovat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
funkce	funkce	k1gFnSc2	funkce
poslance	poslanec	k1gMnPc4	poslanec
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
hejtmanem	hejtman	k1gMnSc7	hejtman
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Splnil	splnit	k5eAaPmAgMnS	splnit
tak	tak	k9	tak
vnitrostranické	vnitrostranický	k2eAgNnSc4d1	vnitrostranické
usnesení	usnesení	k1gNnSc4	usnesení
zakazující	zakazující	k2eAgInSc1d1	zakazující
souběh	souběh	k1gInSc1	souběh
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
odevzdal	odevzdat	k5eAaPmAgInS	odevzdat
notářský	notářský	k2eAgInSc1d1	notářský
zápis	zápis	k1gInSc1	zápis
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
rezignaci	rezignace	k1gFnSc6	rezignace
na	na	k7c4	na
poslanecký	poslanecký	k2eAgInSc4d1	poslanecký
mandát	mandát	k1gInSc4	mandát
předsedovi	předseda	k1gMnSc3	předseda
sněmovny	sněmovna	k1gFnSc2	sněmovna
Janu	Jan	k1gMnSc3	Jan
Hamáčkovi	Hamáček	k1gMnSc3	Hamáček
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
jej	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Gabrhel	Gabrhel	k1gMnSc1	Gabrhel
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
Michala	Michal	k1gMnSc4	Michal
Haška	Hašek	k1gMnSc4	Hašek
k	k	k7c3	k
prosinci	prosinec	k1gInSc3	prosinec
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
člen	člen	k1gMnSc1	člen
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
člen	člen	k1gMnSc1	člen
politického	politický	k2eAgNnSc2d1	politické
grémia	grémium	k1gNnSc2	grémium
ČSSD	ČSSD	kA	ČSSD
člen	člen	k1gInSc1	člen
okresního	okresní	k2eAgInSc2d1	okresní
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
ČSSD	ČSSD	kA	ČSSD
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
poradce	poradce	k1gMnSc2	poradce
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
regionálního	regionální	k2eAgInSc2d1	regionální
rozvoje	rozvoj	k1gInSc2	rozvoj
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
zastával	zastávat	k5eAaImAgMnS	zastávat
další	další	k2eAgFnPc4d1	další
funkce	funkce	k1gFnPc4	funkce
(	(	kIx(	(
<g/>
až	až	k9	až
kolem	kolem	k7c2	kolem
30	[number]	k4	30
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
podle	podle	k7c2	podle
jeho	on	k3xPp3gNnSc2	on
vyjádření	vyjádření	k1gNnSc2	vyjádření
vyplývaly	vyplývat	k5eAaImAgInP	vyplývat
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
hejtmana	hejtman	k1gMnSc2	hejtman
a	a	k8xC	a
poslance	poslanec	k1gMnSc2	poslanec
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
nehorované	horovaný	k2eNgInPc1d1	horovaný
(	(	kIx(	(
<g/>
poslanecký	poslanecký	k2eAgInSc1d1	poslanecký
plat	plat	k1gInSc1	plat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010-2013	[number]	k4	2010-2013
věnoval	věnovat	k5eAaImAgMnS	věnovat
na	na	k7c4	na
charitativní	charitativní	k2eAgInPc4d1	charitativní
účely	účel	k1gInPc4	účel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
souběhu	souběh	k1gInSc3	souběh
mnoha	mnoho	k4c2	mnoho
funkcí	funkce	k1gFnSc7	funkce
byl	být	k5eAaImAgMnS	být
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
častou	častý	k2eAgFnSc4d1	častá
neúčast	neúčast	k1gFnSc4	neúčast
na	na	k7c6	na
hlasováních	hlasování	k1gNnPc6	hlasování
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
jako	jako	k9	jako
poslanec	poslanec	k1gMnSc1	poslanec
účastnil	účastnit	k5eAaImAgMnS	účastnit
pouze	pouze	k6eAd1	pouze
23	[number]	k4	23
%	%	kIx~	%
hlasování	hlasování	k1gNnSc6	hlasování
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rezignaci	rezignace	k1gFnSc6	rezignace
na	na	k7c6	na
funkci	funkce	k1gFnSc6	funkce
poslance	poslanec	k1gMnSc2	poslanec
omezil	omezit	k5eAaPmAgInS	omezit
počet	počet	k1gInSc1	počet
svých	svůj	k3xOyFgFnPc2	svůj
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2015	[number]	k4	2015
jich	on	k3xPp3gMnPc2	on
zastával	zastávat	k5eAaImAgMnS	zastávat
už	už	k6eAd1	už
méně	málo	k6eAd2	málo
než	než	k8xS	než
deset	deset	k4xCc1	deset
<g/>
,	,	kIx,	,
k	k	k7c3	k
prosinci	prosinec	k1gInSc3	prosinec
2016	[number]	k4	2016
pět	pět	k4xCc4	pět
<g/>
.	.	kIx.	.
</s>
