<s>
Zemská	zemský	k2eAgFnSc1d1	zemská
kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
litosférickými	litosférický	k2eAgFnPc7d1	litosférická
deskami	deska	k1gFnPc7	deska
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
neustálém	neustálý	k2eAgInSc6d1	neustálý
pohybu	pohyb	k1gInSc6	pohyb
vlivem	vliv	k1gInSc7	vliv
procesu	proces	k1gInSc2	proces
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
<g/>
.	.	kIx.	.
</s>
