<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaImIp3nS
chemický	chemický	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
tvořící	tvořící	k2eAgInSc1d1
základní	základní	k2eAgInSc1d1
stavební	stavební	k2eAgInSc1d1
kámen	kámen	k1gInSc1
všech	všecek	k3xTgFnPc2
organických	organický	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k8xC
všech	všecek	k3xTgMnPc2
živých	živý	k2eAgMnPc2d1
organismů	organismus	k1gInPc2
na	na	k7c6
této	tento	k3xDgFnSc6
planetě	planeta	k1gFnSc6
a	a	k8xC
má	mít	k5eAaImIp3nS
značku	značka	k1gFnSc4
C	C	kA
<g/>
?	?	kIx.
</s>