<s>
Curium	curium	k1gNnSc1
</s>
<s>
Curium	curium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
7	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
1	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
247	#num#	k4
</s>
<s>
Cm	cm	kA
</s>
<s>
96	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Curium	curium	k1gNnSc1
<g/>
,	,	kIx,
Cm	cm	kA
<g/>
,	,	kIx,
96	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Curium	curium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
f	f	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
stříbřitý	stříbřitý	k2eAgInSc1d1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-51-9	7440-51-9	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
247,070	247,070	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
174	#num#	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
169	#num#	k4
±	±	k?
3	#num#	k4
<g/>
)	)	kIx)
pm	pm	k?
</s>
<s>
Iontový	iontový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
Cm	cm	kA
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
:	:	kIx,
119	#num#	k4
pmCm	pmCm	k6eAd1
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
:	:	kIx,
100	#num#	k4
pmCm	pmCm	k6eAd1
<g/>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
:	:	kIx,
90	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
7	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
1	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
II	II	kA
<g/>
,	,	kIx,
III	III	kA
<g/>
,	,	kIx,
IV	IV	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,3	1,3	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
6,1	6,1	k4
eV	eV	k?
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
11,9	11,9	k4
eV	eV	k?
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
21,0	21,0	k4
eV	eV	k?
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
nejtěsnější	těsný	k2eAgFnSc1d3
hexagonální	hexagonální	k2eAgFnSc1d1
</s>
<s>
Molární	molární	k2eAgInSc1d1
objem	objem	k1gInSc1
</s>
<s>
18,05	18,05	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
3	#num#	k4
dm	dm	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
mol	mol	k1gInSc1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
0	#num#	k4
°	°	k?
<g/>
C	C	kA
13,68	13,68	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
320	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
:	:	kIx,
13,55	13,55	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Tlak	tlak	k1gInSc1
syté	sytý	k2eAgFnSc2d1
páry	pára	k1gFnSc2
</s>
<s>
1	#num#	k4
515	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
:	:	kIx,
1	#num#	k4
Pa	Pa	kA
<g/>
1	#num#	k4
709	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
:	:	kIx,
10	#num#	k4
Pa	Pa	kA
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
1	#num#	k4
340	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
613,15	613,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
3	#num#	k4
110	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
3	#num#	k4
383,15	383,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Specifické	specifický	k2eAgNnSc4d1
teplo	teplo	k1gNnSc4
tání	tání	k1gNnSc2
</s>
<s>
1	#num#	k4
508	#num#	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
K	k	k7c3
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
1,25	1,25	k4
μ	μ	k1gInSc1
</s>
<s>
Standardní	standardní	k2eAgInSc1d1
elektrodový	elektrodový	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
</s>
<s>
Cm	cm	kA
<g/>
3	#num#	k4
<g/>
+	+	kIx~
+	+	kIx~
3	#num#	k4
e	e	k0
<g/>
−	−	k?
→	→	k?
Cm	cm	kA
<g/>
:	:	kIx,
−	−	k?
V	v	k7c6
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
antiferomagnetický	antiferomagnetický	k2eAgInSc1d1
→	→	k?
paramagnetický	paramagnetický	k2eAgInSc1d1
(	(	kIx(
<g/>
přechod	přechod	k1gInSc1
při	při	k7c6
52	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Gd	Gd	k?
<g/>
⋏	⋏	k?
</s>
<s>
Americium	americium	k1gNnSc1
≺	≺	k?
<g/>
Cm	cm	kA
<g/>
≻	≻	k?
Berkelium	Berkelium	k1gNnSc1
</s>
<s>
Curium	curium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Cm	cm	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
osmý	osmý	k4xOgMnSc1
člen	člen	k1gMnSc1
z	z	k7c2
řady	řada	k1gFnSc2
aktinoidů	aktinoid	k1gInPc2
<g/>
,	,	kIx,
čtvrtý	čtvrtý	k4xOgInSc4
transuran	transuran	k1gInSc4
<g/>
,	,	kIx,
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
připravovaný	připravovaný	k2eAgInSc1d1
uměle	uměle	k6eAd1
v	v	k7c6
jaderných	jaderný	k2eAgInPc6d1
reaktorech	reaktor	k1gInPc6
především	především	k9
z	z	k7c2
plutonia	plutonium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Curium	curium	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
přírodě	příroda	k1gFnSc6
nevyskytuje	vyskytovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
uměle	uměle	k6eAd1
připravený	připravený	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
z	z	k7c2
řady	řada	k1gFnSc2
transuranů	transuran	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Fyzikálně-chemické	Fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Curium	curium	k1gNnSc1
je	být	k5eAaImIp3nS
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
stříbřitě	stříbřitě	k6eAd1
bílé	bílý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
působením	působení	k1gNnSc7
vzdušného	vzdušný	k2eAgInSc2d1
kyslíku	kyslík	k1gInSc2
mění	měnit	k5eAaImIp3nS
na	na	k7c4
šedavou	šedavý	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyzařuje	vyzařovat	k5eAaImIp3nS
α	α	k?
a	a	k8xC
γ	γ	k?
záření	záření	k1gNnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
proto	proto	k8xC
nutno	nutno	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
manipulovat	manipulovat	k5eAaImF
za	za	k7c4
dodržování	dodržování	k1gNnSc4
bezpečnostních	bezpečnostní	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
pro	pro	k7c4
práci	práce	k1gFnSc4
s	s	k7c7
radioaktivními	radioaktivní	k2eAgInPc7d1
materiály	materiál	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
sloučeninách	sloučenina	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
pouze	pouze	k6eAd1
v	v	k7c6
mocenství	mocenství	k1gNnSc6
Cm	cm	kA
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Curium	curium	k1gNnSc1
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
připraveno	připravit	k5eAaPmNgNnS
roku	rok	k1gInSc2
1944	#num#	k4
bombardováním	bombardování	k1gNnSc7
239	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
částicemi	částice	k1gFnPc7
α	α	k?
v	v	k7c6
cyklotronu	cyklotron	k1gInSc6
jaderné	jaderný	k2eAgFnSc2d1
laboratoře	laboratoř	k1gFnSc2
kalifornské	kalifornský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c4
Berkeley	Berkele	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
jeho	jeho	k3xOp3gMnPc4
objevitele	objevitel	k1gMnPc4
jsou	být	k5eAaImIp3nP
označováni	označovat	k5eAaImNgMnP
Glenn	Glenna	k1gFnPc2
T.	T.	kA
Seaborg	Seaborg	k1gMnSc1
<g/>
,	,	kIx,
Ralph	Ralph	k1gMnSc1
A.	A.	kA
James	James	k1gMnSc1
a	a	k8xC
Albert	Albert	k1gMnSc1
Ghiorso	Ghiorsa	k1gFnSc5
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
jej	on	k3xPp3gMnSc4
pojmenovali	pojmenovat	k5eAaPmAgMnP
po	po	k7c6
objevitelích	objevitel	k1gMnPc6
radia	radio	k1gNnSc2
<g/>
,	,	kIx,
manželích	manžel	k1gMnPc6
Marií	Maria	k1gFnPc2
a	a	k8xC
Pierru	Pierra	k1gFnSc4
Curieových	Curieův	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s>
23994	#num#	k4
Pu	Pu	k1gFnSc1
+	+	kIx~
42	#num#	k4
He	he	k0
→	→	k?
24296	#num#	k4
Cm	cm	kA
+	+	kIx~
10	#num#	k4
n	n	k0
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
identifikace	identifikace	k1gFnSc1
nového	nový	k2eAgInSc2d1
prvku	prvek	k1gInSc2
(	(	kIx(
<g/>
izotopu	izotop	k1gInSc2
242	#num#	k4
<g/>
Cm	cm	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
provedena	provést	k5eAaPmNgFnS
metalurgickou	metalurgický	k2eAgFnSc7d1
laboratoří	laboratoř	k1gFnSc7
Argonne	Argonn	k1gInSc5
chicagské	chicagský	k2eAgFnPc4d1
university	universita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
formě	forma	k1gFnSc6
elementárního	elementární	k2eAgInSc2d1
kovu	kov	k1gInSc2
bylo	být	k5eAaImAgNnS
curium	curium	k1gNnSc1
poprvé	poprvé	k6eAd1
připraveno	připravit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
Je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
20	#num#	k4
izotopů	izotop	k1gInPc2
curia	curium	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgNnPc2
jsou	být	k5eAaImIp3nP
nejstabilnější	stabilní	k2eAgFnPc1d3
247	#num#	k4
<g/>
Cm	cm	kA
s	s	k7c7
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
15,6	15,6	k4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
a	a	k8xC
248	#num#	k4
<g/>
Cm	cm	kA
s	s	k7c7
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
348	#num#	k4
tisíc	tisíc	k4xCgInPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc4
zbývající	zbývající	k2eAgInPc1d1
radioaktivní	radioaktivní	k2eAgInPc1d1
izotopy	izotop	k1gInPc1
mají	mít	k5eAaImIp3nP
poločas	poločas	k1gInSc4
rozpadu	rozpad	k1gInSc2
méně	málo	k6eAd2
než	než	k8xS
9	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
dokonce	dokonce	k9
méně	málo	k6eAd2
než	než	k8xS
33	#num#	k4
dní	den	k1gInPc2
<g/>
;	;	kIx,
viz	vidět	k5eAaImRp2nS
izotopy	izotop	k1gInPc1
curia	curium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Všechny	všechen	k3xTgInPc1
izotopy	izotop	k1gInPc1
curia	curium	k1gNnSc2
jsou	být	k5eAaImIp3nP
radioaktivní	radioaktivní	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
biologického	biologický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
představuje	představovat	k5eAaImIp3nS
největší	veliký	k2eAgNnSc4d3
riziko	riziko	k1gNnSc4
schopnost	schopnost	k1gFnSc1
curia	curium	k1gNnSc2
akumulovat	akumulovat	k5eAaBmF
se	se	k3xPyFc4
v	v	k7c6
kostní	kostní	k2eAgFnSc6d1
tkáni	tkáň	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
jeho	jeho	k3xOp3gFnSc1
radioaktivita	radioaktivita	k1gFnSc1
působí	působit	k5eAaImIp3nS
poruchy	porucha	k1gFnPc4
krvetvorby	krvetvorba	k1gFnSc2
–	–	k?
brání	bránit	k5eAaImIp3nS
vytváření	vytváření	k1gNnSc4
červených	červený	k2eAgFnPc2d1
krvinek	krvinka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
energie	energie	k1gFnSc1
uvolňovaná	uvolňovaný	k2eAgFnSc1d1
samovolným	samovolný	k2eAgInSc7d1
rozpadem	rozpad	k1gInSc7
jader	jádro	k1gNnPc2
242	#num#	k4
<g/>
Cm	cm	kA
může	moct	k5eAaImIp3nS
dosahovat	dosahovat	k5eAaImF
až	až	k9
120	#num#	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
g	g	kA
a	a	k8xC
činí	činit	k5eAaImIp3nS
tak	tak	k9
z	z	k7c2
tohoto	tento	k3xDgInSc2
izotopu	izotop	k1gInSc2
potenciální	potenciální	k2eAgInSc4d1
energetický	energetický	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
v	v	k7c6
radioizotopovém	radioizotopový	k2eAgInSc6d1
termoelektrickém	termoelektrický	k2eAgInSc6d1
generátoru	generátor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
izotop	izotop	k1gInSc1
má	mít	k5eAaImIp3nS
poločas	poločas	k1gInSc4
rozpadu	rozpad	k1gInSc2
pouze	pouze	k6eAd1
160	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znemožňuje	znemožňovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc1
dlouhodobější	dlouhodobý	k2eAgNnSc1d2
využití	využití	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izotop	izotop	k1gInSc1
243	#num#	k4
<g/>
Cm	cm	kA
vykazuje	vykazovat	k5eAaImIp3nS
poločas	poločas	k1gInSc1
rozpadu	rozpad	k1gInSc6
30	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
244	#num#	k4
<g/>
Cm	cm	kA
přibližně	přibližně	k6eAd1
18	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
jejich	jejich	k3xOp3gInSc4
energetický	energetický	k2eAgInSc4d1
výkon	výkon	k1gInSc4
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
kolem	kolem	k7c2
1	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
g.	g.	k?
Navíc	navíc	k6eAd1
jsou	být	k5eAaImIp3nP
všechny	všechen	k3xTgInPc1
uvedené	uvedený	k2eAgInPc1d1
izotopy	izotop	k1gInPc1
silným	silný	k2eAgInSc7d1
γ	γ	k1gMnPc1
s	s	k7c7
vysokým	vysoký	k2eAgNnSc7d1
zdravotním	zdravotní	k2eAgNnSc7d1
rizikem	riziko	k1gNnSc7
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
jejich	jejich	k3xOp3gNnSc1
praktické	praktický	k2eAgNnSc1d1
uplatnění	uplatnění	k1gNnSc1
prakticky	prakticky	k6eAd1
neprosadilo	prosadit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Curie	Curie	k1gMnPc2
<g/>
,	,	kIx,
1911	#num#	k4
</s>
<s>
Pierre	Pierr	k1gMnSc5
Curie	Curie	k1gMnSc5
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://radchem.nevada.edu/classes/rdch710/files/curium.pdf	http://radchem.nevada.edu/classes/rdch710/files/curium.pdf	k1gInSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
II	II	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
VOHLÍDAL	VOHLÍDAL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
ŠTULÍK	štulík	k1gInSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
;	;	kIx,
JULÁK	JULÁK	kA
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemické	chemický	k2eAgFnPc4d1
a	a	k8xC
analytické	analytický	k2eAgFnPc4d1
tabulky	tabulka	k1gFnPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7169	#num#	k4
<g/>
-	-	kIx~
<g/>
855	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Jaderná	jaderný	k2eAgFnSc1d1
fyzika	fyzika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
curium	curium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
curium	curium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4148404-6	4148404-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85034863	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85034863	#num#	k4
</s>
