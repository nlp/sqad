<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
připravovaný	připravovaný	k2eAgInSc1d1
uměle	uměle	k6eAd1
v	v	k7c6
jaderných	jaderný	k2eAgInPc6d1
reaktorech	reaktor	k1gInPc6
především	především	k6eAd1
z	z	k7c2
plutonia	plutonium	k1gNnSc2
a	a	k8xC
má	mít	k5eAaImIp3nS
chemickou	chemický	k2eAgFnSc4d1
značku	značka	k1gFnSc4
Cm	cm	kA
<g/>
?	?	kIx.
</s>