<s>
Čingischán	Čingischán	k1gMnSc1	Čingischán
(	(	kIx(	(
<g/>
též	též	k9	též
Čingizchán	Čingizchán	k2eAgMnSc1d1	Čingizchán
<g/>
,	,	kIx,	,
Džingischán	Džingischán	k2eAgMnSc1d1	Džingischán
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Džingis	Džingis	k1gFnSc1	Džingis
-	-	kIx~	-
chán	chán	k1gMnSc1	chán
(	(	kIx(	(
<g/>
lze	lze	k6eAd1	lze
volně	volně	k6eAd1	volně
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k9	jako
velký	velký	k2eAgMnSc1d1	velký
vládce	vládce	k1gMnSc1	vládce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Temüdžin	Temüdžina	k1gFnPc2	Temüdžina
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
sjednotitelem	sjednotitel	k1gMnSc7	sjednotitel
mongolských	mongolský	k2eAgInPc2d1	mongolský
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
prvním	první	k4xOgMnSc7	první
Velkým	velký	k2eAgMnSc7d1	velký
chánem	chán	k1gMnSc7	chán
Mongolů	Mongol	k1gMnPc2	Mongol
a	a	k8xC	a
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgMnPc2d3	nejslavnější
vojevůdců	vojevůdce	k1gMnPc2	vojevůdce
a	a	k8xC	a
dobyvatelů	dobyvatel	k1gMnPc2	dobyvatel
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
