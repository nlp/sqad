<s>
Čingischán	Čingischán	k1gMnSc1	Čingischán
(	(	kIx(	(
<g/>
též	též	k9	též
Čingizchán	Čingizchán	k2eAgMnSc1d1	Čingizchán
<g/>
,	,	kIx,	,
Džingischán	Džingischán	k2eAgMnSc1d1	Džingischán
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Džingis	Džingis	k1gFnSc1	Džingis
-	-	kIx~	-
chán	chán	k1gMnSc1	chán
(	(	kIx(	(
<g/>
lze	lze	k6eAd1	lze
volně	volně	k6eAd1	volně
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k9	jako
velký	velký	k2eAgMnSc1d1	velký
vládce	vládce	k1gMnSc1	vládce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Temüdžin	Temüdžina	k1gFnPc2	Temüdžina
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
sjednotitelem	sjednotitel	k1gMnSc7	sjednotitel
mongolských	mongolský	k2eAgInPc2d1	mongolský
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
prvním	první	k4xOgMnSc7	první
Velkým	velký	k2eAgMnSc7d1	velký
chánem	chán	k1gMnSc7	chán
Mongolů	Mongol	k1gMnPc2	Mongol
a	a	k8xC	a
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgMnPc2d3	nejslavnější
vojevůdců	vojevůdce	k1gMnPc2	vojevůdce
a	a	k8xC	a
dobyvatelů	dobyvatel	k1gMnPc2	dobyvatel
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Narozen	narozen	k2eAgMnSc1d1	narozen
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1162	[number]	k4	1162
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
1155	[number]	k4	1155
<g/>
,	,	kIx,	,
1161	[number]	k4	1161
či	či	k8xC	či
1167	[number]	k4	1167
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1227	[number]	k4	1227
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
klanu	klan	k1gInSc2	klan
Bordžiginů	Bordžigin	k1gInPc2	Bordžigin
<g/>
.	.	kIx.	.
</s>
<s>
Čingischánovým	Čingischánův	k2eAgMnSc7d1	Čingischánův
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
Jesühej	Jesühej	k1gMnSc1	Jesühej
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
mongolských	mongolský	k2eAgInPc2d1	mongolský
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1175	[number]	k4	1175
byl	být	k5eAaImAgInS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
Tatary	Tatar	k1gMnPc7	Tatar
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Temüdžin	Temüdžin	k1gMnSc1	Temüdžin
byl	být	k5eAaImAgMnS	být
posléze	posléze	k6eAd1	posléze
svým	svůj	k3xOyFgInSc7	svůj
vlastním	vlastní	k2eAgInSc7d1	vlastní
kmenem	kmen	k1gInSc7	kmen
vyhnán	vyhnat	k5eAaPmNgMnS	vyhnat
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
kočovníkem	kočovník	k1gMnSc7	kočovník
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Tajné	tajný	k2eAgFnSc2d1	tajná
kroniky	kronika	k1gFnSc2	kronika
Mongolů	Mongol	k1gMnPc2	Mongol
byl	být	k5eAaImAgInS	být
také	také	k9	také
načas	načas	k6eAd1	načas
uvrhnut	uvrhnout	k5eAaPmNgInS	uvrhnout
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
20	[number]	k4	20
letech	léto	k1gNnPc6	léto
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Börte	Bört	k1gMnSc5	Bört
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
postavenou	postavený	k2eAgFnSc7d1	postavená
ženou	žena	k1gFnSc7	žena
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Kereitů	Kereita	k1gMnPc2	Kereita
<g/>
,	,	kIx,	,
Temüdžinova	Temüdžinův	k2eAgFnSc1d1	Temüdžinův
společenská	společenský	k2eAgFnSc1d1	společenská
pozice	pozice	k1gFnSc1	pozice
tím	ten	k3xDgNnSc7	ten
značně	značně	k6eAd1	značně
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
silným	silný	k2eAgMnSc7d1	silný
kmenovým	kmenový	k2eAgMnSc7d1	kmenový
vůdcem	vůdce	k1gMnSc7	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
Temüdžinově	Temüdžinův	k2eAgNnSc6d1	Temüdžinův
spojenectví	spojenectví	k1gNnSc6	spojenectví
s	s	k7c7	s
částečně	částečně	k6eAd1	částečně
christianizovanými	christianizovaný	k2eAgInPc7d1	christianizovaný
Kereity	Kereit	k1gInPc7	Kereit
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Nestoriánství	Nestoriánství	k1gNnSc4	Nestoriánství
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
až	až	k9	až
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
šířila	šířit	k5eAaImAgFnS	šířit
fáma	fáma	k1gFnSc1	fáma
<g/>
,	,	kIx,	,
že	že	k8xS	že
Temüdžin	Temüdžin	k1gMnSc1	Temüdžin
je	být	k5eAaImIp3nS	být
bájný	bájný	k2eAgMnSc1d1	bájný
kněz	kněz	k1gMnSc1	kněz
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
Temüdžinův	Temüdžinův	k2eAgInSc1d1	Temüdžinův
kmen	kmen	k1gInSc1	kmen
přepaden	přepaden	k2eAgInSc1d1	přepaden
sousedními	sousední	k2eAgInPc7d1	sousední
Merkity	Merkit	k1gInPc7	Merkit
a	a	k8xC	a
Börte	Bört	k1gInSc5	Bört
unesena	unesen	k2eAgFnSc1d1	unesena
<g/>
;	;	kIx,	;
spojil	spojit	k5eAaPmAgMnS	spojit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
s	s	k7c7	s
mongolskými	mongolský	k2eAgMnPc7d1	mongolský
vůdci	vůdce	k1gMnPc7	vůdce
Tooril-chánem	Toorilhán	k1gMnSc7	Tooril-chán
a	a	k8xC	a
Džamuchou	Džamucha	k1gFnSc7	Džamucha
a	a	k8xC	a
nepřátele	nepřítel	k1gMnPc4	nepřítel
porazil	porazit	k5eAaPmAgInS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
žárlivým	žárlivý	k2eAgInSc7d1	žárlivý
Džamuchou	Džamucha	k1gFnSc7	Džamucha
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
mocenského	mocenský	k2eAgInSc2d1	mocenský
sporu	spor	k1gInSc2	spor
o	o	k7c4	o
výsadní	výsadní	k2eAgNnSc4d1	výsadní
politické	politický	k2eAgNnSc4d1	politické
postavení	postavení	k1gNnSc4	postavení
mezi	mezi	k7c7	mezi
Mongoly	Mongol	k1gMnPc7	Mongol
<g/>
.	.	kIx.	.
</s>
<s>
Temüdžin	Temüdžin	k1gMnSc1	Temüdžin
společně	společně	k6eAd1	společně
s	s	k7c7	s
Tooril-chánem	Toorilhán	k1gMnSc7	Tooril-chán
nakonec	nakonec	k6eAd1	nakonec
nad	nad	k7c7	nad
Džamuchou	Džamucha	k1gFnSc7	Džamucha
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
dominantní	dominantní	k2eAgFnSc4d1	dominantní
pozici	pozice	k1gFnSc4	pozice
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgInPc7	všecek
mongolskými	mongolský	k2eAgInPc7d1	mongolský
kmeny	kmen	k1gInPc7	kmen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1198	[number]	k4	1198
se	se	k3xPyFc4	se
Temüdžinovi	Temüdžin	k1gMnSc3	Temüdžin
podařilo	podařit	k5eAaPmAgNnS	podařit
porazit	porazit	k5eAaPmF	porazit
sousední	sousední	k2eAgInPc4d1	sousední
kmeny	kmen	k1gInPc4	kmen
Tatarů	Tatar	k1gMnPc2	Tatar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1202	[number]	k4	1202
se	se	k3xPyFc4	se
vypořádal	vypořádat	k5eAaPmAgMnS	vypořádat
i	i	k9	i
s	s	k7c7	s
odpůrci	odpůrce	k1gMnPc7	odpůrce
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
Mongolů	Mongol	k1gMnPc2	Mongol
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Džamuchy	Džamucha	k1gFnSc2	Džamucha
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
poté	poté	k6eAd1	poté
nechal	nechat	k5eAaPmAgMnS	nechat
popravit	popravit	k5eAaPmF	popravit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
zcela	zcela	k6eAd1	zcela
rozdrtil	rozdrtit	k5eAaPmAgMnS	rozdrtit
Tatary	tatar	k1gInPc7	tatar
a	a	k8xC	a
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
vyhladil	vyhladit	k5eAaPmAgInS	vyhladit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1203	[number]	k4	1203
porážkou	porážka	k1gFnSc7	porážka
svého	svůj	k3xOyFgMnSc2	svůj
bývalého	bývalý	k2eAgMnSc2d1	bývalý
přítele	přítel	k1gMnSc2	přítel
Tooril-chána	Toorilhán	k2eAgFnSc1d1	Tooril-chán
(	(	kIx(	(
<g/>
jindy	jindy	k6eAd1	jindy
titulovaného	titulovaný	k2eAgInSc2d1	titulovaný
Ong-chán	Onghán	k2eAgInSc4d1	Ong-chán
<g/>
)	)	kIx)	)
odstranil	odstranit	k5eAaPmAgMnS	odstranit
zbytky	zbytek	k1gInPc4	zbytek
vojenské	vojenský	k2eAgFnSc2d1	vojenská
opozice	opozice	k1gFnSc2	opozice
mezi	mezi	k7c7	mezi
kmeny	kmen	k1gInPc7	kmen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1206	[number]	k4	1206
byl	být	k5eAaImAgInS	být
všemi	všecek	k3xTgFnPc7	všecek
vůdci	vůdce	k1gMnPc1	vůdce
mongolských	mongolský	k2eAgInPc2d1	mongolský
kmenů	kmen	k1gInPc2	kmen
na	na	k7c6	na
Velkém	velký	k2eAgInSc6d1	velký
mongolském	mongolský	k2eAgInSc6d1	mongolský
sněmu	sněm	k1gInSc6	sněm
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Kurultaj	Kurultaj	k1gInSc1	Kurultaj
<g/>
)	)	kIx)	)
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
Čingischánem	Čingischán	k1gMnSc7	Čingischán
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc2	překlad
velkým	velký	k2eAgNnPc3d1	velké
(	(	kIx(	(
<g/>
světovým	světový	k2eAgMnSc7d1	světový
<g/>
)	)	kIx)	)
vladařem	vladař	k1gMnSc7	vladař
<g/>
,	,	kIx,	,
doslovný	doslovný	k2eAgInSc4d1	doslovný
překlad	překlad	k1gInSc4	překlad
neomezený	omezený	k2eNgMnSc1d1	neomezený
vládce	vládce	k1gMnSc1	vládce
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středoasijských	středoasijský	k2eAgInPc6d1	středoasijský
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
turkických	turkický	k2eAgInPc6d1	turkický
jazycích	jazyk	k1gInPc6	jazyk
znamená	znamenat	k5eAaImIp3nS	znamenat
slovo	slovo	k1gNnSc1	slovo
čingiz	čingiza	k1gFnPc2	čingiza
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
nekonečno	nekonečno	k1gNnSc1	nekonečno
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
mj.	mj.	kA	mj.
ruská	ruský	k2eAgFnSc1d1	ruská
bájná	bájný	k2eAgFnSc1d1	bájná
postava	postava	k1gFnSc1	postava
mořského	mořský	k2eAgMnSc2d1	mořský
cara	car	k1gMnSc2	car
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
chán	chán	k1gMnSc1	chán
neboli	neboli	k8xC	neboli
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
Velkým	velký	k2eAgMnSc7d1	velký
chánem	chán	k1gMnSc7	chán
všech	všecek	k3xTgMnPc2	všecek
Mongolů	Mongol	k1gMnPc2	Mongol
(	(	kIx(	(
<g/>
Cha-han	Chaan	k1gMnSc1	Cha-han
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svoje	svůj	k3xOyFgNnSc4	svůj
sídelní	sídelní	k2eAgNnSc4d1	sídelní
město	město	k1gNnSc4	město
ustanovil	ustanovit	k5eAaPmAgInS	ustanovit
Karakorum	Karakorum	k1gInSc1	Karakorum
(	(	kIx(	(
<g/>
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
východním	východní	k2eAgNnSc6d1	východní
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
díky	díky	k7c3	díky
vnitřním	vnitřní	k2eAgInPc3d1	vnitřní
sociálním	sociální	k2eAgInPc3d1	sociální
problémům	problém	k1gInPc3	problém
zahájil	zahájit	k5eAaPmAgMnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1211	[number]	k4	1211
Čingischán	Čingischán	k1gMnSc1	Čingischán
mohutnou	mohutný	k2eAgFnSc4d1	mohutná
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
expanzi	expanze	k1gFnSc4	expanze
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
znamenala	znamenat	k5eAaImAgFnS	znamenat
dobytí	dobytí	k1gNnSc4	dobytí
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Persie	Persie	k1gFnSc2	Persie
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Čingischánova	Čingischánův	k2eAgNnSc2d1	Čingischánovo
vojska	vojsko	k1gNnSc2	vojsko
pronikla	proniknout	k5eAaPmAgFnS	proniknout
roku	rok	k1gInSc2	rok
1213	[number]	k4	1213
na	na	k7c6	na
pěti	pět	k4xCc2	pět
místech	místo	k1gNnPc6	místo
Velkou	velký	k2eAgFnSc7d1	velká
čínskou	čínský	k2eAgFnSc7d1	čínská
zdí	zeď	k1gFnSc7	zeď
a	a	k8xC	a
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
na	na	k7c6	na
říši	říš	k1gFnSc6	říš
Ťin	Ťin	k1gFnSc2	Ťin
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Mongolové	Mongol	k1gMnPc1	Mongol
porazili	porazit	k5eAaPmAgMnP	porazit
hlavní	hlavní	k2eAgFnPc4d1	hlavní
síly	síla	k1gFnPc4	síla
nepřítele	nepřítel	k1gMnSc2	nepřítel
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1215	[number]	k4	1215
se	se	k3xPyFc4	se
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
dnešního	dnešní	k2eAgInSc2d1	dnešní
Pekingu	Peking	k1gInSc2	Peking
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nechali	nechat	k5eAaPmAgMnP	nechat
vyplenit	vyplenit	k5eAaPmF	vyplenit
<g/>
.	.	kIx.	.
</s>
<s>
Ťinský	Ťinský	k1gMnSc1	Ťinský
císař	císař	k1gMnSc1	císař
<g/>
,	,	kIx,	,
Süan-cung	Süanung	k1gMnSc1	Süan-cung
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
s	s	k7c7	s
dvorem	dvůr	k1gInSc7	dvůr
do	do	k7c2	do
Kchaj-fengu	Kchajeng	k1gInSc2	Kchaj-feng
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Ťinové	Ťinové	k2eAgMnPc3d1	Ťinové
Mongolům	Mongol	k1gMnPc3	Mongol
vzdorovali	vzdorovat	k5eAaImAgMnP	vzdorovat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1234	[number]	k4	1234
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
rychlé	rychlý	k2eAgFnSc3d1	rychlá
mongolské	mongolský	k2eAgFnSc3d1	mongolská
ofenzivě	ofenziva	k1gFnSc3	ofenziva
se	se	k3xPyFc4	se
postavila	postavit	k5eAaPmAgFnS	postavit
karakitanská	karakitanský	k2eAgFnSc1d1	karakitanský
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
Západní	západní	k2eAgFnSc2d1	západní
říše	říš	k1gFnSc2	říš
Liao	Liao	k1gNnSc1	Liao
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
však	však	k9	však
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1217	[number]	k4	1217
smetena	smeten	k2eAgFnSc1d1	smetena
vedlejšími	vedlejší	k2eAgInPc7d1	vedlejší
oddíly	oddíl	k1gInPc1	oddíl
Čingischánových	Čingischánův	k2eAgNnPc2d1	Čingischánovo
vojsk	vojsko	k1gNnPc2	vojsko
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
poslední	poslední	k2eAgMnSc1d1	poslední
chán	chán	k1gMnSc1	chán
Kučlug	Kučlug	k1gMnSc1	Kučlug
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1218	[number]	k4	1218
popraven	popraven	k2eAgInSc1d1	popraven
<g/>
.	.	kIx.	.
</s>
<s>
Čingischán	Čingischán	k1gMnSc1	Čingischán
dále	daleko	k6eAd2	daleko
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
tažení	tažení	k1gNnSc6	tažení
na	na	k7c4	na
západ	západ	k1gInSc1	západ
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
armádami	armáda	k1gFnPc7	armáda
<g/>
,	,	kIx,	,
třem	tři	k4xCgFnPc3	tři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
veleli	velet	k5eAaImAgMnP	velet
jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
Ögödej	Ögödej	k1gFnSc2	Ögödej
<g/>
,	,	kIx,	,
Džuči	Džuč	k1gFnSc3	Džuč
a	a	k8xC	a
Čaadaj	Čaadaj	k1gFnSc1	Čaadaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1218-1224	[number]	k4	1218-1224
se	se	k3xPyFc4	se
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
Chorezmské	Chorezmský	k2eAgFnPc4d1	Chorezmský
říše	říš	k1gFnPc4	říš
a	a	k8xC	a
jejích	její	k3xOp3gNnPc2	její
center	centrum	k1gNnPc2	centrum
Samarkandu	Samarkanda	k1gFnSc4	Samarkanda
<g/>
,	,	kIx,	,
Buchary	buchar	k1gInPc4	buchar
a	a	k8xC	a
Chorezmu	Chorezm	k1gInSc3	Chorezm
(	(	kIx(	(
<g/>
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
Uzbekistánu	Uzbekistán	k1gInSc6	Uzbekistán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zažívala	zažívat	k5eAaImAgFnS	zažívat
velký	velký	k2eAgInSc4d1	velký
politický	politický	k2eAgInSc4d1	politický
i	i	k8xC	i
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
rozkvět	rozkvět	k1gInSc4	rozkvět
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
pozabíjena	pozabíjen	k2eAgFnSc1d1	pozabíjena
<g/>
.	.	kIx.	.
</s>
<s>
Chorezmský	Chorezmský	k2eAgMnSc1d1	Chorezmský
šáh	šáh	k1gMnSc1	šáh
Aláuddín	Aláuddín	k1gMnSc1	Aláuddín
Muhammad	Muhammad	k1gInSc4	Muhammad
prchal	prchat	k5eAaImAgMnS	prchat
před	před	k7c7	před
Čingischánovými	Čingischánův	k2eAgMnPc7d1	Čingischánův
válečníky	válečník	k1gMnPc7	válečník
Sübeetejem	Sübeetej	k1gMnSc7	Sübeetej
a	a	k8xC	a
Džebem	Džeb	k1gInSc7	Džeb
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
Kaspickému	kaspický	k2eAgNnSc3d1	Kaspické
moři	moře	k1gNnSc3	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1221	[number]	k4	1221
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
Mohammedův	Mohammedův	k2eAgMnSc1d1	Mohammedův
syn	syn	k1gMnSc1	syn
šáh	šáh	k1gMnSc1	šáh
Džellaluddín	Džellaluddín	k1gMnSc1	Džellaluddín
(	(	kIx(	(
<g/>
Jallaluddín	Jallaluddín	k1gMnSc1	Jallaluddín
<g/>
,	,	kIx,	,
Jalal	Jalal	k1gMnSc1	Jalal
<g/>
)	)	kIx)	)
při	při	k7c6	při
řece	řeka	k1gFnSc6	řeka
Indus	Indus	k1gInSc1	Indus
pokusil	pokusit	k5eAaPmAgInS	pokusit
Mongolům	Mongol	k1gMnPc3	Mongol
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1221	[number]	k4	1221
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
novou	nový	k2eAgFnSc4d1	nová
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zprvu	zprvu	k6eAd1	zprvu
podařilo	podařit	k5eAaPmAgNnS	podařit
vyhrát	vyhrát	k5eAaPmF	vyhrát
nad	nad	k7c7	nad
nepočetnými	početný	k2eNgInPc7d1	nepočetný
vedlejšími	vedlejší	k2eAgInPc7d1	vedlejší
mongolskými	mongolský	k2eAgInPc7d1	mongolský
oddíly	oddíl	k1gInPc7	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
síly	síla	k1gFnPc1	síla
vedené	vedený	k2eAgFnSc2d1	vedená
Čingischánem	Čingischán	k1gMnSc7	Čingischán
však	však	k8xC	však
brzy	brzy	k6eAd1	brzy
přinutily	přinutit	k5eAaPmAgFnP	přinutit
Džellaluddínovo	Džellaluddínův	k2eAgNnSc4d1	Džellaluddínův
vojsko	vojsko	k1gNnSc4	vojsko
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Indu	Indus	k1gInSc2	Indus
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
<g/>
,	,	kIx,	,
sám	sám	k3xTgInSc1	sám
Džellaluddín	Džellaluddín	k1gInSc1	Džellaluddín
uprchl	uprchnout	k5eAaPmAgInS	uprchnout
skokem	skok	k1gInSc7	skok
ze	z	k7c2	z
skály	skála	k1gFnSc2	skála
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Čingischán	Čingischán	k1gMnSc1	Čingischán
poté	poté	k6eAd1	poté
rozkázal	rozkázat	k5eAaPmAgMnS	rozkázat
prohledat	prohledat	k5eAaPmF	prohledat
celou	celý	k2eAgFnSc4d1	celá
severozápadní	severozápadní	k2eAgFnSc4d1	severozápadní
Indii	Indie	k1gFnSc4	Indie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Džellaludín	Džellaludín	k1gMnSc1	Džellaludín
nalezen	nalezen	k2eAgMnSc1d1	nalezen
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
utekl	utéct	k5eAaPmAgMnS	utéct
do	do	k7c2	do
Persie	Persie	k1gFnSc2	Persie
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Čingischán	Čingischán	k1gMnSc1	Čingischán
setrvával	setrvávat	k5eAaImAgMnS	setrvávat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
mongolští	mongolský	k2eAgMnPc1d1	mongolský
vojevůdci	vojevůdce	k1gMnPc1	vojevůdce
Sübeetej	Sübeetej	k1gMnSc1	Sübeetej
a	a	k8xC	a
Džebe	Džeb	k1gInSc5	Džeb
si	se	k3xPyFc3	se
mezitím	mezitím	k6eAd1	mezitím
razili	razit	k5eAaImAgMnP	razit
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězných	vítězný	k2eAgFnPc6d1	vítězná
bitvách	bitva	k1gFnPc6	bitva
roku	rok	k1gInSc2	rok
1221	[number]	k4	1221
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
Ázerbájdžánu	Ázerbájdžán	k1gInSc6	Ázerbájdžán
a	a	k8xC	a
Arménii	Arménie	k1gFnSc6	Arménie
zamířili	zamířit	k5eAaPmAgMnP	zamířit
přes	přes	k7c4	přes
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
na	na	k7c4	na
sever	sever	k1gInSc4	sever
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Kubáň	Kubáň	k1gFnSc1	Kubáň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
porazili	porazit	k5eAaPmAgMnP	porazit
Kumány	Kumán	k2eAgInPc4d1	Kumán
(	(	kIx(	(
<g/>
Polovce	Polovec	k1gInPc4	Polovec
<g/>
,	,	kIx,	,
Kipčáky	Kipčák	k1gInPc4	Kipčák
<g/>
)	)	kIx)	)
a	a	k8xC	a
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
se	se	k3xPyFc4	se
okolního	okolní	k2eAgNnSc2d1	okolní
území	území	k1gNnSc2	území
až	až	k9	až
k	k	k7c3	k
severnímu	severní	k2eAgInSc3d1	severní
břehu	břeh	k1gInSc3	břeh
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Přemístili	přemístit	k5eAaPmAgMnP	přemístit
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Azovskému	azovský	k2eAgNnSc3d1	Azovské
moři	moře	k1gNnSc3	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Kalce	Kalce	k?	Kalce
roku	rok	k1gInSc2	rok
1223	[number]	k4	1223
zcela	zcela	k6eAd1	zcela
rozdrtili	rozdrtit	k5eAaPmAgMnP	rozdrtit
spojená	spojený	k2eAgNnPc4d1	spojené
vojska	vojsko	k1gNnPc4	vojsko
Kumánů	Kumán	k1gMnPc2	Kumán
a	a	k8xC	a
ruských	ruský	k2eAgMnPc2d1	ruský
knížat	kníže	k1gMnPc2wR	kníže
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
kyjevským	kyjevský	k2eAgMnSc7d1	kyjevský
velkoknížetem	velkokníže	k1gMnSc7	velkokníže
Mstislavem	Mstislav	k1gMnSc7	Mstislav
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Mstislav	Mstislava	k1gFnPc2	Mstislava
Mstislavič	Mstislavič	k1gInSc1	Mstislavič
Haličský	haličský	k2eAgInSc1d1	haličský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zajatá	zajatý	k2eAgNnPc1d1	zajaté
knížata	kníže	k1gNnPc1	kníže
Mongolové	Mongol	k1gMnPc1	Mongol
údajně	údajně	k6eAd1	údajně
umučili	umučit	k5eAaPmAgMnP	umučit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
hostiny	hostina	k1gFnSc2	hostina
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Vítězná	vítězný	k2eAgNnPc1d1	vítězné
vojska	vojsko	k1gNnPc1	vojsko
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgNnP	přesunout
na	na	k7c4	na
Krym	Krym	k1gInSc4	Krym
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
na	na	k7c4	na
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
,	,	kIx,	,
záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
ale	ale	k9	ale
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
a	a	k8xC	a
zamířila	zamířit	k5eAaPmAgFnS	zamířit
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
hlavním	hlavní	k2eAgFnPc3d1	hlavní
silám	síla	k1gFnPc3	síla
Čingischánovým	Čingischánův	k2eAgFnPc3d1	Čingischánova
<g/>
.	.	kIx.	.
</s>
<s>
Džebe	Džebat	k5eAaPmIp3nS	Džebat
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
zpátky	zpátky	k6eAd1	zpátky
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Čingischán	Čingischán	k1gMnSc1	Čingischán
se	se	k3xPyFc4	se
po	po	k7c6	po
definitivní	definitivní	k2eAgFnSc6d1	definitivní
porážce	porážka	k1gFnSc6	porážka
nepřítele	nepřítel	k1gMnSc2	nepřítel
na	na	k7c6	na
západě	západ	k1gInSc6	západ
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
zničení	zničení	k1gNnSc4	zničení
dynastie	dynastie	k1gFnSc2	dynastie
Západní	západní	k2eAgFnSc2d1	západní
Sia	Sia	k1gFnSc2	Sia
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zamrzlé	zamrzlý	k2eAgFnSc6d1	zamrzlá
řece	řeka	k1gFnSc6	řeka
Jang-c	Jang	k1gInSc1	Jang-c
<g/>
'	'	kIx"	'
Mongolové	Mongol	k1gMnPc1	Mongol
čínské	čínský	k2eAgFnSc2d1	čínská
vojsko	vojsko	k1gNnSc4	vojsko
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
porazili	porazit	k5eAaPmAgMnP	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Čingischán	Čingischán	k1gMnSc1	Čingischán
pak	pak	k6eAd1	pak
opět	opět	k6eAd1	opět
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
proti	proti	k7c3	proti
severočínské	severočínský	k2eAgFnSc3d1	Severočínská
dynastii	dynastie	k1gFnSc3	dynastie
Ťin	Ťin	k1gFnSc2	Ťin
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1227	[number]	k4	1227
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
tohoto	tento	k3xDgNnSc2	tento
tažení	tažení	k1gNnSc2	tažení
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
místě	místo	k1gNnSc6	místo
Čingischánova	Čingischánův	k2eAgInSc2d1	Čingischánův
hrobu	hrob	k1gInSc2	hrob
existují	existovat	k5eAaImIp3nP	existovat
četné	četný	k2eAgFnPc1d1	četná
legendy	legenda	k1gFnPc1	legenda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
lokalita	lokalita	k1gFnSc1	lokalita
dosud	dosud	k6eAd1	dosud
zcela	zcela	k6eAd1	zcela
neznámá	známý	k2eNgFnSc1d1	neznámá
<g/>
.	.	kIx.	.
</s>
<s>
Vlády	vláda	k1gFnPc1	vláda
obrovské	obrovský	k2eAgFnSc2d1	obrovská
Mongolské	mongolský	k2eAgFnSc2d1	mongolská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
vytvořené	vytvořený	k2eAgNnSc1d1	vytvořené
Čingischánem	Čingischán	k1gMnSc7	Čingischán
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Ögödej	Ögödej	k1gMnSc1	Ögödej
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšná	úspěšný	k2eAgNnPc4d1	úspěšné
překonání	překonání	k1gNnPc4	překonání
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
Čingischán	Čingischán	k1gMnSc1	Čingischán
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
vojevůdci	vojevůdce	k1gMnPc1	vojevůdce
porazili	porazit	k5eAaPmAgMnP	porazit
<g/>
,	,	kIx,	,
záleželo	záležet	k5eAaImAgNnS	záležet
na	na	k7c6	na
perfektní	perfektní	k2eAgFnSc6d1	perfektní
organizovanosti	organizovanost	k1gFnSc6	organizovanost
mongolské	mongolský	k2eAgFnSc2d1	mongolská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc3	její
nezvykle	zvykle	k6eNd1	zvykle
velké	velký	k2eAgFnSc3d1	velká
mobilitě	mobilita	k1gFnSc3	mobilita
<g/>
,	,	kIx,	,
okamžité	okamžitý	k2eAgFnSc6d1	okamžitá
připravenosti	připravenost	k1gFnSc6	připravenost
mongolského	mongolský	k2eAgMnSc4d1	mongolský
vojáka	voják	k1gMnSc4	voják
k	k	k7c3	k
boji	boj	k1gInSc3	boj
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
nevylučovala	vylučovat	k5eNaImAgFnS	vylučovat
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
kočováním	kočování	k1gNnSc7	kočování
v	v	k7c6	v
době	doba	k1gFnSc6	doba
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
nepřekonatelném	překonatelný	k2eNgNnSc6d1	nepřekonatelné
umění	umění	k1gNnSc6	umění
mongolských	mongolský	k2eAgMnPc2d1	mongolský
jezdců	jezdec	k1gMnPc2	jezdec
a	a	k8xC	a
lučištníků	lučištník	k1gMnPc2	lučištník
<g/>
,	,	kIx,	,
konečně	konečně	k6eAd1	konečně
také	také	k9	také
na	na	k7c4	na
znalosti	znalost	k1gFnPc4	znalost
taktických	taktický	k2eAgInPc2d1	taktický
a	a	k8xC	a
strategických	strategický	k2eAgInPc2d1	strategický
postupů	postup	k1gInPc2	postup
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
propracované	propracovaný	k2eAgFnSc2d1	propracovaná
vojenské	vojenský	k2eAgFnSc2d1	vojenská
špionáže	špionáž	k1gFnSc2	špionáž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
Mongolové	Mongol	k1gMnPc1	Mongol
postupně	postupně	k6eAd1	postupně
získávali	získávat	k5eAaImAgMnP	získávat
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
dobývání	dobývání	k1gNnSc6	dobývání
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
výbojích	výboj	k1gInPc6	výboj
Čingischána	Čingischán	k1gMnSc2	Čingischán
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
potomků	potomek	k1gMnPc2	potomek
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
při	při	k7c6	při
bojích	boj	k1gInPc6	boj
<g/>
,	,	kIx,	,
masakrech	masakr	k1gInPc6	masakr
a	a	k8xC	a
hladomorech	hladomor	k1gInPc6	hladomor
30	[number]	k4	30
-	-	kIx~	-
60	[number]	k4	60
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
populace	populace	k1gFnSc2	populace
Číny	Čína	k1gFnSc2	Čína
klesla	klesnout	k5eAaPmAgFnS	klesnout
během	během	k7c2	během
50	[number]	k4	50
let	léto	k1gNnPc2	léto
mongolské	mongolský	k2eAgFnSc2d1	mongolská
vlády	vláda	k1gFnSc2	vláda
až	až	k9	až
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Úplnému	úplný	k2eAgNnSc3d1	úplné
ovládnutí	ovládnutí	k1gNnSc3	ovládnutí
Evropy	Evropa	k1gFnSc2	Evropa
zřejmě	zřejmě	k6eAd1	zřejmě
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
období	období	k1gNnSc1	období
silných	silný	k2eAgInPc2d1	silný
dešťů	dešť	k1gInPc2	dešť
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
proměnilo	proměnit	k5eAaPmAgNnS	proměnit
podunajské	podunajský	k2eAgFnPc4d1	Podunajská
a	a	k8xC	a
polské	polský	k2eAgFnPc4d1	polská
nížiny	nížina	k1gFnPc4	nížina
v	v	k7c4	v
bahnitý	bahnitý	k2eAgInSc4d1	bahnitý
terén	terén	k1gInSc4	terén
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
jízdní	jízdní	k2eAgInPc4d1	jízdní
oddíly	oddíl	k1gInPc4	oddíl
neschůdný	schůdný	k2eNgInSc1d1	neschůdný
<g/>
.	.	kIx.	.
</s>
<s>
Mongolská	mongolský	k2eAgFnSc1d1	mongolská
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
po	po	k7c6	po
sobě	se	k3xPyFc3	se
Čingischán	Čingischán	k1gMnSc1	Čingischán
zanechal	zanechat	k5eAaPmAgMnS	zanechat
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
propracovaný	propracovaný	k2eAgInSc4d1	propracovaný
a	a	k8xC	a
pevně	pevně	k6eAd1	pevně
zavedený	zavedený	k2eAgInSc1d1	zavedený
správní	správní	k2eAgInSc1d1	správní
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
Jasa	Jasa	k1gFnSc1	Jasa
-	-	kIx~	-
ustanovení	ustanovení	k1gNnSc1	ustanovení
zákonů	zákon	k1gInPc2	zákon
<g/>
)	)	kIx)	)
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vnitřně	vnitřně	k6eAd1	vnitřně
dostatečně	dostatečně	k6eAd1	dostatečně
jednotná	jednotný	k2eAgFnSc1d1	jednotná
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
následně	následně	k6eAd1	následně
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
rozpadu	rozpad	k1gInSc3	rozpad
<g/>
,	,	kIx,	,
nástupnické	nástupnický	k2eAgInPc1d1	nástupnický
(	(	kIx(	(
<g/>
veskrze	veskrze	k6eAd1	veskrze
dynastické	dynastický	k2eAgInPc1d1	dynastický
<g/>
)	)	kIx)	)
státy	stát	k1gInPc1	stát
ovládané	ovládaný	k2eAgFnSc2d1	ovládaná
potomky	potomek	k1gMnPc4	potomek
Čingischána	Čingischán	k1gMnSc2	Čingischán
přetrvaly	přetrvat	k5eAaPmAgFnP	přetrvat
několik	několik	k4yIc4	několik
staletí	staletí	k1gNnPc2	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Krymský	krymský	k2eAgInSc4d1	krymský
chanát	chanát	k1gInSc4	chanát
<g/>
,	,	kIx,	,
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
až	až	k9	až
koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pchaj-ca	Pchaja	k1gFnSc1	Pchaj-ca
-	-	kIx~	-
kovová	kovový	k2eAgFnSc1d1	kovová
destička	destička	k1gFnSc1	destička
s	s	k7c7	s
vyrytým	vyrytý	k2eAgInSc7d1	vyrytý
Temudžinovým	Temudžinův	k2eAgInSc7d1	Temudžinův
příkazem	příkaz	k1gInSc7	příkaz
k	k	k7c3	k
volnému	volný	k2eAgInSc3d1	volný
průjezdu	průjezd	k1gInSc3	průjezd
<g/>
,	,	kIx,	,
vládcové	vládce	k1gMnPc1	vládce
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
území	území	k1gNnPc2	území
byli	být	k5eAaImAgMnP	být
povinni	povinen	k2eAgMnPc1d1	povinen
majiteli	majitel	k1gMnSc3	majitel
poskytnout	poskytnout	k5eAaPmF	poskytnout
útočiště	útočiště	k1gNnSc4	útočiště
a	a	k8xC	a
vybavení	vybavení	k1gNnSc4	vybavení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
čerstvé	čerstvý	k2eAgNnSc1d1	čerstvé
koně	kůň	k1gMnSc4	kůň
<g/>
)	)	kIx)	)
Billig	Billig	k1gMnSc1	Billig
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
Temudžinových	Temudžinův	k2eAgInPc2d1	Temudžinův
výroků	výrok	k1gInPc2	výrok
Džoči	Džoč	k1gFnSc3	Džoč
(	(	kIx(	(
<g/>
Jochi	Jochi	k1gNnSc2	Jochi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
synové	syn	k1gMnPc1	syn
byli	být	k5eAaImAgMnP	být
Udur	Udur	k1gInSc4	Udur
<g/>
,	,	kIx,	,
Šejbají	Šejbají	k1gFnSc4	Šejbají
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
Batú	Batú	k1gMnSc1	Batú
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
založil	založit	k5eAaPmAgMnS	založit
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
hordu	horda	k1gFnSc4	horda
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
Asii	Asie	k1gFnSc6	Asie
Čaadaj	Čaadaj	k1gInSc1	Čaadaj
(	(	kIx(	(
<g/>
Jagathai	Jagathai	k1gNnSc1	Jagathai
<g/>
,	,	kIx,	,
Čagataj	Čagataj	k1gFnSc1	Čagataj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
středoasijského	středoasijský	k2eAgInSc2d1	středoasijský
čagatajského	čagatajský	k2eAgInSc2d1	čagatajský
chanátu	chanát	k1gInSc2	chanát
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Büri	Bür	k1gFnSc2	Bür
Ögödej	Ögödej	k1gMnSc1	Ögödej
(	(	kIx(	(
<g/>
Ögedej	Ögedej	k1gMnSc1	Ögedej
<g/>
,	,	kIx,	,
Otkaj	Otkaj	k1gMnSc1	Otkaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1227	[number]	k4	1227
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1241	[number]	k4	1241
Velkým	velký	k2eAgMnSc7d1	velký
chánem	chán	k1gMnSc7	chán
<g/>
,	,	kIx,	,
vládce	vládce	k1gMnSc1	vládce
v	v	k7c6	v
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
synové	syn	k1gMnPc1	syn
byli	být	k5eAaImAgMnP	být
Güjük	Güjük	k1gInSc4	Güjük
<g/>
,	,	kIx,	,
Velkým	velký	k2eAgMnSc7d1	velký
chánem	chán	k1gMnSc7	chán
1246	[number]	k4	1246
<g/>
-	-	kIx~	-
<g/>
1248	[number]	k4	1248
<g/>
,	,	kIx,	,
a	a	k8xC	a
Kadan	Kadan	k1gInSc1	Kadan
Tolujchán	Tolujchán	k2eAgInSc1d1	Tolujchán
(	(	kIx(	(
<g/>
Tolui	Tolu	k1gFnPc1	Tolu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vládce	vládce	k1gMnSc1	vládce
východního	východní	k2eAgNnSc2d1	východní
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
synové	syn	k1gMnPc1	syn
byli	být	k5eAaImAgMnP	být
Kublaj	Kublaj	k1gInSc4	Kublaj
(	(	kIx(	(
<g/>
Burgidžin	Burgidžin	k2eAgInSc4d1	Burgidžin
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Chubilaj	Chubilaj	k1gInSc1	Chubilaj
<g/>
,	,	kIx,	,
Budžek	Budžek	k1gInSc1	Budžek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velký	velký	k2eAgMnSc1d1	velký
chán	chán	k1gMnSc1	chán
1259	[number]	k4	1259
<g/>
-	-	kIx~	-
<g/>
1294	[number]	k4	1294
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
dynastie	dynastie	k1gFnSc2	dynastie
Jüan	jüan	k1gInSc1	jüan
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Möngke	Möngke	k1gFnSc7	Möngke
<g/>
,	,	kIx,	,
Velkým	velký	k2eAgMnSc7d1	velký
chánem	chán	k1gMnSc7	chán
1251	[number]	k4	1251
<g/>
-	-	kIx~	-
<g/>
1259	[number]	k4	1259
<g/>
,	,	kIx,	,
a	a	k8xC	a
Hulagu	Hulaga	k1gFnSc4	Hulaga
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
říše	říš	k1gFnSc2	říš
ílchánů	ílchán	k1gMnPc2	ílchán
v	v	k7c6	v
Persii	Persie	k1gFnSc6	Persie
Díky	díky	k7c3	díky
moderním	moderní	k2eAgFnPc3d1	moderní
metodám	metoda	k1gFnPc3	metoda
genetického	genetický	k2eAgInSc2d1	genetický
výzkumu	výzkum	k1gInSc2	výzkum
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
16	[number]	k4	16
milionů	milion	k4xCgInPc2	milion
nositelů	nositel	k1gMnPc2	nositel
jeho	jeho	k3xOp3gInPc2	jeho
genů	gen	k1gInPc2	gen
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jeho	jeho	k3xOp3gMnPc2	jeho
přímých	přímý	k2eAgMnPc2d1	přímý
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
objektivity	objektivita	k1gFnSc2	objektivita
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
původcem	původce	k1gMnSc7	původce
genetického	genetický	k2eAgInSc2d1	genetický
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
hvězdicové	hvězdicový	k2eAgFnSc2d1	hvězdicová
aeoly	aeola	k1gFnSc2	aeola
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
děd	děd	k1gMnSc1	děd
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
definitivně	definitivně	k6eAd1	definitivně
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
genetického	genetický	k2eAgInSc2d1	genetický
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
prokazatelně	prokazatelně	k6eAd1	prokazatelně
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
ostatků	ostatek	k1gInPc2	ostatek
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
životopisu	životopis	k1gInSc3	životopis
je	být	k5eAaImIp3nS	být
však	však	k9	však
rozšíření	rozšíření	k1gNnSc4	rozšíření
znaku	znak	k1gInSc2	znak
Temüdžinem	Temüdžino	k1gNnSc7	Temüdžino
téměř	téměř	k6eAd1	téměř
jisté	jistý	k2eAgNnSc4d1	jisté
(	(	kIx(	(
<g/>
měl	mít	k5eAaImAgMnS	mít
asi	asi	k9	asi
600	[number]	k4	600
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
konkubín	konkubína	k1gFnPc2	konkubína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
hrob	hrob	k1gInSc1	hrob
však	však	k9	však
dosud	dosud	k6eAd1	dosud
nalezen	nalezen	k2eAgMnSc1d1	nalezen
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Tajná	tajný	k2eAgFnSc1d1	tajná
kronika	kronika	k1gFnSc1	kronika
Mongolů	mongol	k1gInPc2	mongol
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
uváděna	uváděn	k2eAgFnSc1d1	uváděna
jako	jako	k8xS	jako
Tajná	tajný	k2eAgFnSc1d1	tajná
historie	historie	k1gFnSc1	historie
Mongolů	Mongol	k1gMnPc2	Mongol
či	či	k8xC	či
Tajná	tajný	k2eAgFnSc1d1	tajná
zpráva	zpráva	k1gFnSc1	zpráva
Mongolů	Mongol	k1gMnPc2	Mongol
<g/>
)	)	kIx)	)
-	-	kIx~	-
historie	historie	k1gFnSc1	historie
Čingischánovy	Čingischánův	k2eAgFnSc2d1	Čingischánova
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1228	[number]	k4	1228
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejcennějším	cenný	k2eAgInSc7d3	nejcennější
autentickým	autentický	k2eAgInSc7d1	autentický
spisem	spis	k1gInSc7	spis
o	o	k7c6	o
Čingischánovi	Čingischán	k1gMnSc6	Čingischán
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
době	doba	k1gFnSc3	doba
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
vydaná	vydaný	k2eAgFnSc1d1	vydaná
u	u	k7c2	u
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
SNK	SNK	kA	SNK
Praha	Praha	k1gFnSc1	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
přeložená	přeložený	k2eAgFnSc1d1	přeložená
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Pavlem	Pavel	k1gMnSc7	Pavel
Pouchou	Poucha	k1gMnSc7	Poucha
<g/>
.	.	kIx.	.
</s>
<s>
Čingischán	Čingischán	k1gMnSc1	Čingischán
(	(	kIx(	(
<g/>
Genghis	Genghis	k1gFnSc1	Genghis
Khan	Khan	k1gMnSc1	Khan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Henry	Henry	k1gMnSc1	Henry
Levin	Levin	k1gMnSc1	Levin
<g/>
;	;	kIx,	;
Hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Omar	Omar	k1gMnSc1	Omar
Sharif	Sharif	k1gMnSc1	Sharif
(	(	kIx(	(
<g/>
Temüdžin	Temüdžin	k1gMnSc1	Temüdžin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Françoise	Françoise	k1gFnSc1	Françoise
Dorléac	Dorléac	k1gFnSc1	Dorléac
(	(	kIx(	(
<g/>
Bortei	Bortei	k1gNnSc1	Bortei
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stephen	Stephen	k2eAgMnSc1d1	Stephen
Boyd	Boyd	k1gMnSc1	Boyd
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Mason	mason	k1gMnSc1	mason
Mongol	Mongol	k1gMnSc1	Mongol
(	(	kIx(	(
<g/>
М	М	k?	М
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Sergei	Sergei	k1gNnSc1	Sergei
Bodrov	Bodrovo	k1gNnPc2	Bodrovo
<g/>
;	;	kIx,	;
Hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Tadanobu	Tadanoba	k1gFnSc4	Tadanoba
Asano	Asano	k6eAd1	Asano
(	(	kIx(	(
<g/>
Temüdžin	Temüdžin	k2eAgMnSc1d1	Temüdžin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Amadu	Amada	k1gFnSc4	Amada
Mamadakov	Mamadakov	k1gInSc4	Mamadakov
<g/>
,	,	kIx,	,
Honglei	Honglee	k1gFnSc4	Honglee
Sun	Sun	kA	Sun
<g/>
,	,	kIx,	,
Ying	Ying	k1gInSc1	Ying
Bai	Bai	k1gFnSc4	Bai
Mongolská	mongolský	k2eAgFnSc1d1	mongolská
říše	říše	k1gFnSc1	říše
Kublaj	Kublaj	k1gFnSc1	Kublaj
Batú	Batú	k1gFnSc1	Batú
Tamerlán	Tamerlán	k1gInSc1	Tamerlán
Chán	chán	k1gMnSc1	chán
Kagan	Kagan	k1gMnSc1	Kagan
Černá	černat	k5eAaImIp3nS	černat
smrt	smrt	k1gFnSc4	smrt
</s>
