<s>
Nový	nový	k2eAgMnSc1d1	nový
správce	správce	k1gMnSc1	správce
tratí	trať	k1gFnPc2	trať
přebudoval	přebudovat	k5eAaPmAgMnS	přebudovat
tratě	trať	k1gFnPc4	trať
na	na	k7c4	na
elektrické	elektrický	k2eAgMnPc4d1	elektrický
(	(	kIx(	(
<g/>
provoz	provoz	k1gInSc1	provoz
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
třetí	třetí	k4xOgFnSc1	třetí
trať	trať	k1gFnSc1	trať
z	z	k7c2	z
náměstí	náměstí	k1gNnSc2	náměstí
Svobody	Svoboda	k1gMnSc2	Svoboda
do	do	k7c2	do
Zábrdovic	Zábrdovice	k1gFnPc2	Zábrdovice
<g/>
,	,	kIx,	,
linky	linka	k1gFnPc1	linka
byly	být	k5eAaImAgFnP	být
tehdy	tehdy	k6eAd1	tehdy
označeny	označen	k2eAgFnPc1d1	označena
barvami	barva	k1gFnPc7	barva
<g/>
.	.	kIx.	.
</s>
