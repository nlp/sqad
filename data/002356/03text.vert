<s>
Estrapáda	Estrapáda	k1gFnSc1	Estrapáda
je	být	k5eAaImIp3nS	být
historický	historický	k2eAgInSc4d1	historický
způsob	způsob	k1gInSc4	způsob
popravy	poprava	k1gFnSc2	poprava
či	či	k8xC	či
mučení	mučení	k1gNnSc2	mučení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vykonání	vykonání	k1gNnSc6	vykonání
rozsudku	rozsudek	k1gInSc2	rozsudek
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Odsouzenec	odsouzenec	k1gMnSc1	odsouzenec
byl	být	k5eAaImAgMnS	být
vytažen	vytáhnout	k5eAaPmNgMnS	vytáhnout
provazem	provaz	k1gInSc7	provaz
procházejícím	procházející	k2eAgFnPc3d1	procházející
pod	pod	k7c7	pod
rukama	ruka	k1gFnPc7	ruka
svázanýma	svázaný	k2eAgFnPc7d1	svázaná
za	za	k7c4	za
zády	záda	k1gNnPc7	záda
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
stožáru	stožár	k1gInSc2	stožár
nebo	nebo	k8xC	nebo
šibenice	šibenice	k1gFnSc2	šibenice
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
byl	být	k5eAaImAgMnS	být
nahoře	nahoře	k6eAd1	nahoře
<g/>
,	,	kIx,	,
nechali	nechat	k5eAaPmAgMnP	nechat
ho	on	k3xPp3gNnSc4	on
spadnout	spadnout	k5eAaPmF	spadnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
až	až	k9	až
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nárazem	náraz	k1gInSc7	náraz
při	při	k7c6	při
zastavení	zastavení	k1gNnSc6	zastavení
pádu	pád	k1gInSc2	pád
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
odsouzenci	odsouzenec	k1gMnPc1	odsouzenec
vykloubily	vykloubit	k5eAaPmAgInP	vykloubit
ruce	ruka	k1gFnPc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Estrapádu	Estrapáda	k1gFnSc4	Estrapáda
patrně	patrně	k6eAd1	patrně
vynalezli	vynaleznout	k5eAaPmAgMnP	vynaleznout
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Používala	používat	k5eAaImAgFnS	používat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Sardinii	Sardinie	k1gFnSc6	Sardinie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
německých	německý	k2eAgInPc6d1	německý
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
touto	tento	k3xDgFnSc7	tento
metodou	metoda	k1gFnSc7	metoda
oficiálně	oficiálně	k6eAd1	oficiálně
již	již	k6eAd1	již
nepopravuje	popravovat	k5eNaImIp3nS	popravovat
<g/>
;	;	kIx,	;
obdobná	obdobný	k2eAgFnSc1d1	obdobná
technika	technika	k1gFnSc1	technika
se	se	k3xPyFc4	se
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
BDSM	BDSM	kA	BDSM
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
kati	kat	k1gMnPc1	kat
vymýšleli	vymýšlet	k5eAaImAgMnP	vymýšlet
zostření	zostření	k1gNnSc4	zostření
trestu	trest	k1gInSc2	trest
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgNnSc1d3	nejjednodušší
bylo	být	k5eAaImAgNnS	být
odsouzenci	odsouzenec	k1gMnPc1	odsouzenec
na	na	k7c4	na
nohy	noha	k1gFnPc4	noha
přivázat	přivázat	k5eAaPmF	přivázat
závaží	závaží	k1gNnSc4	závaží
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
náraz	náraz	k1gInSc1	náraz
ještě	ještě	k6eAd1	ještě
prudší	prudký	k2eAgInSc1d2	prudší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
vykloubily	vykloubit	k5eAaPmAgFnP	vykloubit
nejen	nejen	k6eAd1	nejen
ruce	ruka	k1gFnPc4	ruka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nejtěžších	těžký	k2eAgNnPc2d3	nejtěžší
závaží	závaží	k1gNnSc2	závaží
se	se	k3xPyFc4	se
odsouzenci	odsouzenec	k1gMnPc1	odsouzenec
často	často	k6eAd1	často
roztrhlo	roztrhnout	k5eAaPmAgNnS	roztrhnout
břicho	břicho	k1gNnSc1	břicho
a	a	k8xC	a
vnitřnosti	vnitřnost	k1gFnPc1	vnitřnost
vyhřezly	vyhřeznout	k5eAaPmAgFnP	vyhřeznout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nahradila	nahradit	k5eAaPmAgFnS	nahradit
estrapáda	estrapáda	k1gFnSc1	estrapáda
dočasně	dočasně	k6eAd1	dočasně
zrušený	zrušený	k2eAgInSc4d1	zrušený
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
asi	asi	k9	asi
šedesát	šedesát	k4xCc1	šedesát
procent	procento	k1gNnPc2	procento
odsouzených	odsouzený	k2eAgMnPc2d1	odsouzený
<g/>
.	.	kIx.	.
</s>
