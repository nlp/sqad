<s>
Cassini	Cassin	k2eAgMnPc1d1	Cassin
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
planetární	planetární	k2eAgFnSc1d1	planetární
sonda	sonda	k1gFnSc1	sonda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
navedena	naveden	k2eAgFnSc1d1	navedena
na	na	k7c4	na
orbitu	orbita	k1gFnSc4	orbita
Saturnu	Saturn	k1gInSc2	Saturn
pro	pro	k7c4	pro
jeho	on	k3xPp3gInSc4	on
průzkum	průzkum	k1gInSc4	průzkum
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc2	jeho
prstenců	prstenec	k1gInPc2	prstenec
a	a	k8xC	a
systému	systém	k1gInSc2	systém
jeho	jeho	k3xOp3gInPc2	jeho
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
