<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
americká	americký	k2eAgFnSc1d1	americká
planetární	planetární	k2eAgFnSc1d1	planetární
sonda	sonda	k1gFnSc1	sonda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
navedena	naveden	k2eAgFnSc1d1	navedena
na	na	k7c4	na
orbitu	orbita	k1gFnSc4	orbita
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
?	?	kIx.	?
</s>
