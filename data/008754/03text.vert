<p>
<s>
Cassini	Cassin	k2eAgMnPc1d1	Cassin
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
planetární	planetární	k2eAgFnSc1d1	planetární
sonda	sonda	k1gFnSc1	sonda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
navedena	naveden	k2eAgFnSc1d1	navedena
na	na	k7c4	na
orbitu	orbita	k1gFnSc4	orbita
Saturnu	Saturn	k1gInSc2	Saturn
pro	pro	k7c4	pro
jeho	on	k3xPp3gInSc4	on
průzkum	průzkum	k1gInSc4	průzkum
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc2	jeho
prstenců	prstenec	k1gInPc2	prstenec
a	a	k8xC	a
systému	systém	k1gInSc2	systém
jeho	jeho	k3xOp3gInPc2	jeho
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Vypuštěna	vypuštěn	k2eAgFnSc1d1	vypuštěna
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
a	a	k8xC	a
po	po	k7c6	po
dvacetileté	dvacetiletý	k2eAgFnSc6d1	dvacetiletá
výzkumné	výzkumný	k2eAgFnSc6d1	výzkumná
misi	mise	k1gFnSc6	mise
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
plánovaně	plánovaně	k6eAd1	plánovaně
shořela	shořet	k5eAaPmAgFnS	shořet
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
přístrojovém	přístrojový	k2eAgNnSc6d1	přístrojové
vybavení	vybavení	k1gNnSc6	vybavení
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgFnP	podílet
také	také	k6eAd1	také
evropská	evropský	k2eAgFnSc1d1	Evropská
organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
vesmíru	vesmír	k1gInSc2	vesmír
ESA	eso	k1gNnSc2	eso
a	a	k8xC	a
italská	italský	k2eAgFnSc1d1	italská
národní	národní	k2eAgFnSc1d1	národní
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
ASI	asi	k9	asi
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
měla	mít	k5eAaImAgFnS	mít
dvě	dva	k4xCgFnPc4	dva
samostatné	samostatný	k2eAgFnPc4d1	samostatná
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
sondu	sonda	k1gFnSc4	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
(	(	kIx(	(
<g/>
pojmenovanou	pojmenovaný	k2eAgFnSc4d1	pojmenovaná
po	po	k7c6	po
italském	italský	k2eAgInSc6d1	italský
astronomovi	astronomův	k2eAgMnPc1d1	astronomův
Giovanni	Giovanen	k2eAgMnPc1d1	Giovanen
Domenico	Domenico	k1gMnSc1	Domenico
Cassinim	Cassinim	k1gMnSc1	Cassinim
<g/>
)	)	kIx)	)
a	a	k8xC	a
atmosférickou	atmosférický	k2eAgFnSc4d1	atmosférická
sondu	sonda	k1gFnSc4	sonda
Huygens	Huygensa	k1gFnPc2	Huygensa
(	(	kIx(	(
<g/>
pojmenovanou	pojmenovaný	k2eAgFnSc7d1	pojmenovaná
po	po	k7c6	po
holandském	holandský	k2eAgMnSc6d1	holandský
vědci	vědec	k1gMnSc6	vědec
Christiaanu	Christiaan	k1gMnSc6	Christiaan
Huygensovi	Huygens	k1gMnSc6	Huygens
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Středisko	středisko	k1gNnSc1	středisko
NASA	NASA	kA	NASA
v	v	k7c6	v
Pasadeně	Pasadena	k1gFnSc6	Pasadena
bylo	být	k5eAaImAgNnS	být
pověřeno	pověřit	k5eAaPmNgNnS	pověřit
pozemním	pozemní	k2eAgNnSc7d1	pozemní
sledováním	sledování	k1gNnSc7	sledování
rádiového	rádiový	k2eAgInSc2d1	rádiový
signálu	signál	k1gInSc2	signál
vysílaného	vysílaný	k2eAgInSc2d1	vysílaný
sondou	sonda	k1gFnSc7	sonda
jednak	jednak	k8xC	jednak
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
vysoké	vysoký	k2eAgFnSc2d1	vysoká
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
ionosféry	ionosféra	k1gFnSc2	ionosféra
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
ke	k	k7c3	k
zjišťování	zjišťování	k1gNnSc3	zjišťování
vlastností	vlastnost	k1gFnPc2	vlastnost
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
Saturnu	Saturn	k1gInSc2	Saturn
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Modul	modul	k1gInSc1	modul
Huygens	Huygens	k1gInSc1	Huygens
<g/>
,	,	kIx,	,
určený	určený	k2eAgInSc1d1	určený
k	k	k7c3	k
průzkumu	průzkum	k1gInSc3	průzkum
měsíce	měsíc	k1gInSc2	měsíc
Titan	titan	k1gInSc4	titan
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
prvního	první	k4xOgNnSc2	první
přistání	přistání	k1gNnSc4	přistání
dosaženého	dosažený	k2eAgMnSc4d1	dosažený
ve	v	k7c6	v
vnější	vnější	k2eAgFnSc6d1	vnější
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Jezera	jezero	k1gNnPc1	jezero
z	z	k7c2	z
kapalného	kapalný	k2eAgInSc2d1	kapalný
methanu	methan	k1gInSc2	methan
zpozorované	zpozorovaný	k2eAgNnSc4d1	zpozorované
sondou	sonda	k1gFnSc7	sonda
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jediná	jediný	k2eAgFnSc1d1	jediná
známá	známá	k1gFnSc1	známá
jezera	jezero	k1gNnSc2	jezero
mimo	mimo	k7c4	mimo
planetu	planeta	k1gFnSc4	planeta
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
také	také	k9	také
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ledový	ledový	k2eAgInSc4d1	ledový
Saturnův	Saturnův	k2eAgInSc4d1	Saturnův
měsíc	měsíc	k1gInSc4	měsíc
Enceladus	Enceladus	k1gInSc1	Enceladus
skrývá	skrývat	k5eAaImIp3nS	skrývat
podpovrchový	podpovrchový	k2eAgInSc1d1	podpovrchový
<g/>
,	,	kIx,	,
slaný	slaný	k2eAgInSc1d1	slaný
oceán	oceán	k1gInSc1	oceán
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gFnSc7	jeho
kůrou	kůra	k1gFnSc7	kůra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přístrojové	přístrojový	k2eAgNnSc4d1	přístrojové
vybavení	vybavení	k1gNnSc4	vybavení
==	==	k?	==
</s>
</p>
<p>
<s>
Sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
nesla	nést	k5eAaImAgFnS	nést
soubor	soubor	k1gInSc4	soubor
vědeckých	vědecký	k2eAgInPc2d1	vědecký
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
kamerový	kamerový	k2eAgInSc1d1	kamerový
systém	systém	k1gInSc1	systém
se	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
CCD	CCD	kA	CCD
kamerami	kamera	k1gFnPc7	kamera
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
ultrafialový	ultrafialový	k2eAgInSc4d1	ultrafialový
zobrazující	zobrazující	k2eAgInSc4d1	zobrazující
spektrograf	spektrograf	k1gInSc4	spektrograf
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
mapující	mapující	k2eAgInSc1d1	mapující
spektrograf	spektrograf	k1gInSc1	spektrograf
pro	pro	k7c4	pro
viditelnou	viditelný	k2eAgFnSc4d1	viditelná
a	a	k8xC	a
infračervenou	infračervený	k2eAgFnSc4d1	infračervená
oblast	oblast	k1gFnSc4	oblast
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
kombinovaný	kombinovaný	k2eAgInSc1d1	kombinovaný
infračervený	infračervený	k2eAgInSc1d1	infračervený
spektrometr	spektrometr	k1gInSc1	spektrometr
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
radiolokátor	radiolokátor	k1gInSc1	radiolokátor
a	a	k8xC	a
radarový	radarový	k2eAgInSc1d1	radarový
výškoměr	výškoměr	k1gInSc1	výškoměr
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
analyzátor	analyzátor	k1gInSc1	analyzátor
kosmického	kosmický	k2eAgInSc2d1	kosmický
prachu	prach	k1gInSc2	prach
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
plazmový	plazmový	k2eAgInSc1d1	plazmový
spektrometr	spektrometr	k1gInSc1	spektrometr
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
hmotnostní	hmotnostní	k2eAgInSc1d1	hmotnostní
spektrometr	spektrometr	k1gInSc1	spektrometr
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
magnetometr	magnetometr	k1gInSc1	magnetometr
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
magnetosférické	magnetosférický	k2eAgNnSc4d1	magnetosférický
zobrazovací	zobrazovací	k2eAgNnSc4d1	zobrazovací
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
soubor	soubor	k1gInSc1	soubor
přístrojů	přístroj	k1gInPc2	přístroj
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
vlastností	vlastnost	k1gFnPc2	vlastnost
plazmatu	plazma	k1gNnSc2	plazma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
letu	let	k1gInSc2	let
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
plánovaný	plánovaný	k2eAgInSc1d1	plánovaný
datum	datum	k1gInSc1	datum
startu	start	k1gInSc2	start
byl	být	k5eAaImAgInS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
ale	ale	k9	ale
nezdařilo	zdařit	k5eNaPmAgNnS	zdařit
dodržet	dodržet	k5eAaPmF	dodržet
pro	pro	k7c4	pro
poruchu	porucha	k1gFnSc4	porucha
klimatizačního	klimatizační	k2eAgNnSc2d1	klimatizační
zařízení	zařízení	k1gNnSc2	zařízení
na	na	k7c6	na
modulu	modul	k1gInSc6	modul
Huygens	Huygens	k1gInSc1	Huygens
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgNnSc1d1	následné
poškození	poškození	k1gNnSc1	poškození
ochranné	ochranný	k2eAgFnSc2d1	ochranná
fólie	fólie	k1gFnSc2	fólie
pouzdra	pouzdro	k1gNnSc2	pouzdro
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
týdenní	týdenní	k2eAgInSc4d1	týdenní
odklad	odklad	k1gInSc4	odklad
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
plánovaný	plánovaný	k2eAgInSc1d1	plánovaný
datum	datum	k1gInSc1	datum
startu	start	k1gInSc2	start
byl	být	k5eAaImAgInS	být
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tentokrát	tentokrát	k6eAd1	tentokrát
startu	start	k1gInSc2	start
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
nepříznivé	příznivý	k2eNgNnSc1d1	nepříznivé
počasí	počasí	k1gNnSc1	počasí
na	na	k7c6	na
mysu	mys	k1gInSc6	mys
Canaveral	Canaveral	k1gFnSc2	Canaveral
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
startu	start	k1gInSc6	start
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1997	[number]	k4	1997
z	z	k7c2	z
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
na	na	k7c6	na
mysu	mys	k1gInSc6	mys
Canaveral	Canaveral	k1gFnSc2	Canaveral
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
značné	značný	k2eAgFnSc3d1	značná
hmotnosti	hmotnost	k1gFnSc3	hmotnost
sondy	sonda	k1gFnSc2	sonda
během	během	k7c2	během
letu	let	k1gInSc2	let
k	k	k7c3	k
Saturnu	Saturn	k1gMnSc3	Saturn
využito	využít	k5eAaPmNgNnS	využít
gravitačních	gravitační	k2eAgInPc2d1	gravitační
manévrů	manévr	k1gInPc2	manévr
při	při	k7c6	při
dvou	dva	k4xCgInPc6	dva
průletech	průlet	k1gInPc6	průlet
kolem	kolem	k7c2	kolem
Venuše	Venuše	k1gFnSc2	Venuše
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1998	[number]	k4	1998
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
234	[number]	k4	234
km	km	kA	km
a	a	k8xC	a
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1999	[number]	k4	1999
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
600	[number]	k4	600
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1999	[number]	k4	1999
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
1171	[number]	k4	1171
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
kolem	kolem	k7c2	kolem
Jupiteru	Jupiter	k1gInSc2	Jupiter
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2000	[number]	k4	2000
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
gravitačních	gravitační	k2eAgInPc2d1	gravitační
manévrů	manévr	k1gInPc2	manévr
sondu	sonda	k1gFnSc4	sonda
urychlil	urychlit	k5eAaPmAgMnS	urychlit
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
podstatně	podstatně	k6eAd1	podstatně
zkrátila	zkrátit	k5eAaPmAgFnS	zkrátit
doba	doba	k1gFnSc1	doba
přeletu	přelet	k1gInSc2	přelet
k	k	k7c3	k
Saturnu	Saturn	k1gInSc3	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Průletů	průlet	k1gInPc2	průlet
kolem	kolem	k7c2	kolem
Venuše	Venuše	k1gFnSc2	Venuše
a	a	k8xC	a
Země	zem	k1gFnSc2	zem
bylo	být	k5eAaImAgNnS	být
využito	využít	k5eAaPmNgNnS	využít
ke	k	k7c3	k
kalibraci	kalibrace	k1gFnSc3	kalibrace
vědeckých	vědecký	k2eAgInPc2d1	vědecký
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
planety	planeta	k1gFnSc2	planeta
Jupiter	Jupiter	k1gInSc4	Jupiter
sonda	sonda	k1gFnSc1	sonda
prováděla	provádět	k5eAaImAgFnS	provádět
v	v	k7c6	v
omezeném	omezený	k2eAgInSc6d1	omezený
rozsahu	rozsah	k1gInSc6	rozsah
vědecká	vědecký	k2eAgNnPc4d1	vědecké
měření	měření	k1gNnSc4	měření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Na	na	k7c6	na
orbitě	orbita	k1gFnSc6	orbita
Saturnu	Saturn	k1gInSc2	Saturn
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Saturnovy	Saturnův	k2eAgFnSc2d1	Saturnova
sféry	sféra	k1gFnSc2	sféra
gravitačního	gravitační	k2eAgInSc2d1	gravitační
vlivu	vliv	k1gInSc2	vliv
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
sonda	sonda	k1gFnSc1	sonda
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Zážehem	zážeh	k1gInSc7	zážeh
hlavního	hlavní	k2eAgInSc2d1	hlavní
korekčního	korekční	k2eAgInSc2d1	korekční
motoru	motor	k1gInSc2	motor
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
96	[number]	k4	96
minut	minuta	k1gFnPc2	minuta
byla	být	k5eAaImAgFnS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2004	[number]	k4	2004
sonda	sonda	k1gFnSc1	sonda
navedena	navést	k5eAaPmNgFnS	navést
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
se	se	k3xPyFc4	se
od	od	k7c2	od
sondy	sonda	k1gFnSc2	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
oddělila	oddělit	k5eAaPmAgFnS	oddělit
sonda	sonda	k1gFnSc1	sonda
Huygens	Huygensa	k1gFnPc2	Huygensa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
úspěšně	úspěšně	k6eAd1	úspěšně
přistála	přistát	k5eAaImAgFnS	přistát
na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
Titan	titan	k1gInSc1	titan
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jejího	její	k3xOp3gNnSc2	její
přistávání	přistávání	k1gNnSc2	přistávání
sloužila	sloužit	k5eAaImAgFnS	sloužit
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
jako	jako	k8xC	jako
retranslační	retranslační	k2eAgFnPc1d1	retranslační
stanice	stanice	k1gFnPc1	stanice
pro	pro	k7c4	pro
předávání	předávání	k1gNnSc4	předávání
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
a	a	k8xC	a
technických	technický	k2eAgNnPc2d1	technické
dat	datum	k1gNnPc2	datum
ze	z	k7c2	z
sondy	sonda	k1gFnSc2	sonda
Huygens	Huygensa	k1gFnPc2	Huygensa
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sonda	sonda	k1gFnSc1	sonda
Huygens	Huygensa	k1gFnPc2	Huygensa
přistála	přistát	k5eAaPmAgFnS	přistát
na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
Titanu	titan	k1gInSc2	titan
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
a	a	k8xC	a
během	během	k7c2	během
sestupu	sestup	k1gInSc6	sestup
atmosférou	atmosféra	k1gFnSc7	atmosféra
a	a	k8xC	a
po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
shromáždila	shromáždit	k5eAaPmAgFnS	shromáždit
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
vědeckých	vědecký	k2eAgInPc2d1	vědecký
poznatků	poznatek	k1gInPc2	poznatek
o	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
tělese	těleso	k1gNnSc6	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
vydržela	vydržet	k5eAaPmAgFnS	vydržet
pracovat	pracovat	k5eAaImF	pracovat
4	[number]	k4	4
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Cassini	Cassin	k2eAgMnPc1d1	Cassin
<g/>
,	,	kIx,	,
zprostředkovávající	zprostředkovávající	k2eAgMnPc1d1	zprostředkovávající
informace	informace	k1gFnSc2	informace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
hodinách	hodina	k1gFnPc6	hodina
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
zákrytu	zákryt	k1gInSc2	zákryt
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
přenos	přenos	k1gInSc1	přenos
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
přerušen	přerušit	k5eAaPmNgInS	přerušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sonda	sonda	k1gFnSc1	sonda
provedla	provést	k5eAaPmAgFnS	provést
výzkum	výzkum	k1gInSc4	výzkum
měsíců	měsíc	k1gInPc2	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2007	[number]	k4	2007
odeslala	odeslat	k5eAaPmAgFnS	odeslat
sonda	sonda	k1gFnSc1	sonda
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
fotografie	fotografia	k1gFnSc2	fotografia
svědčící	svědčící	k2eAgFnSc2d1	svědčící
o	o	k7c6	o
existenci	existence	k1gFnSc6	existence
tekutých	tekutý	k2eAgInPc2d1	tekutý
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
Titan	titan	k1gInSc1	titan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
jej	on	k3xPp3gNnSc2	on
minula	minulo	k1gNnSc2	minulo
po	po	k7c6	po
čtyřicáté	čtyřicátý	k4xOgFnSc6	čtyřicátý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konec	konec	k1gInSc1	konec
mise	mise	k1gFnSc2	mise
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
plánován	plánovat	k5eAaImNgInS	plánovat
na	na	k7c4	na
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2008	[number]	k4	2008
však	však	k9	však
NASA	NASA	kA	NASA
oznámila	oznámit	k5eAaPmAgFnS	oznámit
prodloužení	prodloužení	k1gNnSc4	prodloužení
mise	mise	k1gFnSc2	mise
nejméně	málo	k6eAd3	málo
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
prodloužení	prodloužení	k1gNnSc3	prodloužení
mise	mise	k1gFnSc2	mise
sondy	sonda	k1gFnSc2	sonda
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
až	až	k9	až
2012	[number]	k4	2012
sonda	sonda	k1gFnSc1	sonda
proletěla	proletět	k5eAaPmAgFnS	proletět
třikrát	třikrát	k6eAd1	třikrát
poblíž	poblíž	k7c2	poblíž
měsíce	měsíc	k1gInSc2	měsíc
Enceladus	Enceladus	k1gInSc4	Enceladus
u	u	k7c2	u
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2014	[number]	k4	2014
byly	být	k5eAaImAgInP	být
oznámeny	oznámen	k2eAgInPc1d1	oznámen
výsledky	výsledek	k1gInPc1	výsledek
přijatých	přijatý	k2eAgNnPc2d1	přijaté
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
svědčící	svědčící	k2eAgInSc1d1	svědčící
o	o	k7c6	o
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
slané	slaný	k2eAgFnSc2d1	slaná
vody	voda	k1gFnSc2	voda
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2013	[number]	k4	2013
NASA	NASA	kA	NASA
oznámila	oznámit	k5eAaPmAgFnS	oznámit
příjem	příjem	k1gInSc4	příjem
signálů	signál	k1gInPc2	signál
ze	z	k7c2	z
sondy	sonda	k1gFnSc2	sonda
<g/>
,	,	kIx,	,
pocházejících	pocházející	k2eAgFnPc2d1	pocházející
z	z	k7c2	z
měření	měření	k1gNnSc2	měření
infračerveným	infračervený	k2eAgInSc7d1	infračervený
spektrometrem	spektrometr	k1gInSc7	spektrometr
u	u	k7c2	u
měsíce	měsíc	k1gInSc2	měsíc
Titan	titan	k1gInSc1	titan
<g/>
,	,	kIx,	,
dosvědčujícím	dosvědčující	k2eAgInSc7d1	dosvědčující
existenci	existence	k1gFnSc6	existence
molekul	molekula	k1gFnPc2	molekula
propanu	propan	k1gInSc2	propan
či	či	k8xC	či
propylenu	propylen	k1gInSc2	propylen
<g/>
.	.	kIx.	.
<g/>
Sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
byla	být	k5eAaImAgNnP	být
plánovaně	plánovaně	k6eAd1	plánovaně
zničena	zničit	k5eAaPmNgNnP	zničit
ponořením	ponoření	k1gNnSc7	ponoření
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
Saturnu	Saturn	k1gInSc2	Saturn
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyslala	vyslat	k5eAaPmAgFnS	vyslat
poslední	poslední	k2eAgFnSc4d1	poslední
sérii	série	k1gFnSc4	série
fotek	fotka	k1gFnPc2	fotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Snímky	snímek	k1gInPc1	snímek
největších	veliký	k2eAgInPc2d3	veliký
měsíců	měsíc	k1gInPc2	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
vyfocené	vyfocený	k2eAgNnSc4d1	vyfocené
sondou	sonda	k1gFnSc7	sonda
Cassini	Cassin	k2eAgMnPc5d1	Cassin
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
jména	jméno	k1gNnSc2	jméno
==	==	k?	==
</s>
</p>
<p>
<s>
Sonda	sonda	k1gFnSc1	sonda
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
italsko-francouzského	italskorancouzský	k2eAgMnSc2d1	italsko-francouzský
astronoma	astronom	k1gMnSc2	astronom
G.	G.	kA	G.
D.	D.	kA	D.
Cassiniho	Cassini	k1gMnSc2	Cassini
(	(	kIx(	(
<g/>
1625	[number]	k4	1625
<g/>
–	–	k?	–
<g/>
1712	[number]	k4	1712
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
nebývalou	nebývalý	k2eAgFnSc7d1	nebývalá
měrou	míra	k1gFnSc7wR	míra
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
výzkum	výzkum	k1gInSc4	výzkum
Saturnu	Saturn	k1gInSc2	Saturn
a	a	k8xC	a
objevil	objevit	k5eAaPmAgMnS	objevit
čtyři	čtyři	k4xCgInPc4	čtyři
jeho	jeho	k3xOp3gInPc4	jeho
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kosmická	kosmický	k2eAgFnSc1d1	kosmická
sonda	sonda	k1gFnSc1	sonda
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Cassini-Huygens	Cassini-Huygens	k1gInSc1	Cassini-Huygens
Home	Hom	k1gMnSc2	Hom
Page	Pag	k1gMnSc2	Pag
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
SPACE	SPACE	kA	SPACE
40	[number]	k4	40
-	-	kIx~	-
Cassini	Cassin	k2eAgMnPc1d1	Cassin
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Home	Home	k6eAd1	Home
Page	Page	k1gInSc1	Page
ESA	eso	k1gNnSc2	eso
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cassini-Huygens	Cassini-Huygens	k1gInSc1	Cassini-Huygens
Home	Home	k1gNnSc2	Home
Page	Page	k1gFnPc2	Page
JPL	JPL	kA	JPL
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
SPACE	SPACE	kA	SPACE
40	[number]	k4	40
-	-	kIx~	-
Cassini	Cassin	k2eAgMnPc1d1	Cassin
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
SPACE	SPACE	kA	SPACE
40	[number]	k4	40
-	-	kIx~	-
Huygens	Huygens	k1gInSc1	Huygens
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jezero	jezero	k1gNnSc1	jezero
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
