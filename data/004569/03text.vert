<s>
Alchymista	alchymista	k1gMnSc1	alchymista
(	(	kIx(	(
<g/>
v	v	k7c6	v
portugalském	portugalský	k2eAgInSc6d1	portugalský
originále	originál	k1gInSc6	originál
O	o	k7c6	o
Alquimista	Alquimista	k1gMnSc1	Alquimista
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
brazilského	brazilský	k2eAgMnSc2d1	brazilský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Paula	Paul	k1gMnSc2	Paul
Coelha	Coelh	k1gMnSc2	Coelh
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
Rio	Rio	k1gFnSc7	Rio
de	de	k?	de
Janeiru	Janeir	k1gInSc2	Janeir
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
cestě	cesta	k1gFnSc6	cesta
mladého	mladý	k2eAgMnSc2d1	mladý
pastýře	pastýř	k1gMnSc2	pastýř
za	za	k7c7	za
svým	svůj	k3xOyFgInSc7	svůj
snem	sen	k1gInSc7	sen
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
čtenáře	čtenář	k1gMnSc2	čtenář
přesvědčuje	přesvědčovat	k5eAaImIp3nS	přesvědčovat
o	o	k7c6	o
smysluplnosti	smysluplnost	k1gFnSc6	smysluplnost
cesty	cesta	k1gFnSc2	cesta
za	za	k7c7	za
osobním	osobní	k2eAgNnSc7d1	osobní
štěstím	štěstí	k1gNnSc7	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
Osobní	osobní	k2eAgInSc4d1	osobní
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k6eAd1	jen
málokdo	málokdo	k3yInSc1	málokdo
ho	on	k3xPp3gMnSc4	on
dokáže	dokázat	k5eAaPmIp3nS	dokázat
prožít	prožít	k5eAaPmF	prožít
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
volí	volit	k5eAaImIp3nP	volit
raději	rád	k6eAd2	rád
pohodlí	pohodlí	k1gNnSc4	pohodlí
a	a	k8xC	a
jistotu	jistota	k1gFnSc4	jistota
místo	místo	k7c2	místo
rizika	riziko	k1gNnSc2	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
příběh	příběh	k1gInSc1	příběh
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jasně	jasně	k6eAd1	jasně
vymezena	vymezit	k5eAaPmNgFnS	vymezit
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
jen	jen	k9	jen
usuzovat	usuzovat	k5eAaImF	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
nejdříve	dříve	k6eAd3	dříve
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
esperanto	esperanto	k1gNnSc1	esperanto
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
Angličan	Angličan	k1gMnSc1	Angličan
putující	putující	k2eAgMnSc1d1	putující
s	s	k7c7	s
pastýřem	pastýř	k1gMnSc7	pastýř
v	v	k7c6	v
karavaně	karavana	k1gFnSc6	karavana
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
Andalusii	Andalusie	k1gFnSc6	Andalusie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Tarifa	tarifa	k1gFnSc1	tarifa
<g/>
,	,	kIx,	,
Tanger	Tanger	k1gInSc1	Tanger
<g/>
,	,	kIx,	,
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
Sahara	Sahara	k1gFnSc1	Sahara
nebo	nebo	k8xC	nebo
u	u	k7c2	u
egyptských	egyptský	k2eAgFnPc2d1	egyptská
pyramid	pyramida	k1gFnPc2	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
Tangerem	Tangero	k1gNnSc7	Tangero
a	a	k8xC	a
pyramidami	pyramida	k1gFnPc7	pyramida
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
musel	muset	k5eAaImAgMnS	muset
pastýř	pastýř	k1gMnSc1	pastýř
Santiago	Santiago	k1gNnSc4	Santiago
s	s	k7c7	s
karavanou	karavana	k1gFnSc7	karavana
urazit	urazit	k5eAaPmF	urazit
<g/>
,	,	kIx,	,
měří	měřit	k5eAaImIp3nS	měřit
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
čarou	čára	k1gFnSc7	čára
přes	přes	k7c4	přes
3	[number]	k4	3
500	[number]	k4	500
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
andaluský	andaluský	k2eAgMnSc1d1	andaluský
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
pastýř	pastýř	k1gMnSc1	pastýř
Santiago	Santiago	k1gNnSc1	Santiago
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
již	již	k6eAd1	již
dvakrát	dvakrát	k6eAd1	dvakrát
zdá	zdát	k5eAaImIp3nS	zdát
stejný	stejný	k2eAgInSc4d1	stejný
sen	sen	k1gInSc4	sen
o	o	k7c6	o
pokladu	poklad	k1gInSc6	poklad
ukrytém	ukrytý	k2eAgInSc6d1	ukrytý
u	u	k7c2	u
pyramid	pyramida	k1gFnPc2	pyramida
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
za	za	k7c7	za
vědmou	vědma	k1gFnSc7	vědma
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
ho	on	k3xPp3gNnSc4	on
vyložila	vyložit	k5eAaPmAgFnS	vyložit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
narazí	narazit	k5eAaPmIp3nS	narazit
Santiago	Santiago	k1gNnSc1	Santiago
na	na	k7c4	na
starce	stařec	k1gMnSc4	stařec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
začne	začít	k5eAaPmIp3nS	začít
vyprávět	vyprávět	k5eAaImF	vyprávět
o	o	k7c6	o
Osobním	osobní	k2eAgInSc6d1	osobní
příběhu	příběh	k1gInSc6	příběh
<g/>
,	,	kIx,	,
o	o	k7c6	o
znameních	znamení	k1gNnPc6	znamení
<g/>
,	,	kIx,	,
o	o	k7c6	o
Řeči	řeč	k1gFnSc6	řeč
a	a	k8xC	a
Duši	duše	k1gFnSc6	duše
světa	svět	k1gInSc2	svět
a	a	k8xC	a
o	o	k7c6	o
lidech	člověk	k1gMnPc6	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
jít	jít	k5eAaImF	jít
za	za	k7c7	za
svým	svůj	k3xOyFgInSc7	svůj
Osobním	osobní	k2eAgInSc7d1	osobní
příběhem	příběh	k1gInSc7	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Chlapec	chlapec	k1gMnSc1	chlapec
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vydá	vydat	k5eAaPmIp3nS	vydat
za	za	k7c7	za
oním	onen	k3xDgInSc7	onen
snem	sen	k1gInSc7	sen
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
už	už	k6eAd1	už
se	se	k3xPyFc4	se
málem	málem	k6eAd1	málem
vzdá	vzdát	k5eAaPmIp3nS	vzdát
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
jít	jít	k5eAaImF	jít
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
chvíli	chvíle	k1gFnSc6	chvíle
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
opět	opět	k6eAd1	opět
ukáže	ukázat	k5eAaPmIp3nS	ukázat
nějaké	nějaký	k3yIgNnSc4	nějaký
znamení	znamení	k1gNnSc4	znamení
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
cesta	cesta	k1gFnSc1	cesta
je	být	k5eAaImIp3nS	být
plná	plný	k2eAgFnSc1d1	plná
nástrah	nástraha	k1gFnPc2	nástraha
a	a	k8xC	a
překvapení	překvapení	k1gNnPc2	překvapení
<g/>
,	,	kIx,	,
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
objeví	objevit	k5eAaPmIp3nS	objevit
svou	svůj	k3xOyFgFnSc4	svůj
opravdovou	opravdový	k2eAgFnSc4d1	opravdová
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
svůj	svůj	k3xOyFgInSc4	svůj
poklad	poklad	k1gInSc4	poklad
najde	najít	k5eAaPmIp3nS	najít
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgNnSc1d2	podrobnější
vyprávění	vyprávění	k1gNnSc1	vyprávění
<g/>
:	:	kIx,	:
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
nás	my	k3xPp1nPc4	my
příběh	příběh	k1gInSc1	příběh
zavede	zavést	k5eAaPmIp3nS	zavést
do	do	k7c2	do
starého	starý	k2eAgInSc2d1	starý
opuštěného	opuštěný	k2eAgInSc2d1	opuštěný
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
sakristie	sakristie	k1gFnSc2	sakristie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyrostl	vyrůst	k5eAaPmAgInS	vyrůst
fíkovník	fíkovník	k1gInSc1	fíkovník
<g/>
,	,	kIx,	,
spí	spát	k5eAaImIp3nS	spát
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
Santiago	Santiago	k1gNnSc4	Santiago
a	a	k8xC	a
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
sen	sen	k1gInSc4	sen
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
se	se	k3xPyFc4	se
probudí	probudit	k5eAaPmIp3nS	probudit
a	a	k8xC	a
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
sen	sen	k1gInSc1	sen
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
už	už	k6eAd1	už
jednou	jednou	k6eAd1	jednou
zdál	zdát	k5eAaImAgInS	zdát
<g/>
.	.	kIx.	.
</s>
<s>
Chvíli	chvíle	k1gFnSc4	chvíle
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
hloubá	hloubat	k5eAaImIp3nS	hloubat
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
zajde	zajít	k5eAaPmIp3nS	zajít
do	do	k7c2	do
města	město	k1gNnSc2	město
a	a	k8xC	a
nechá	nechat	k5eAaPmIp3nS	nechat
si	se	k3xPyFc3	se
sen	sen	k1gInSc4	sen
vyložit	vyložit	k5eAaPmF	vyložit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tarife	tarif	k1gInSc5	tarif
vyhledá	vyhledat	k5eAaPmIp3nS	vyhledat
starou	starý	k2eAgFnSc4d1	stará
cikánku	cikánka	k1gFnSc4	cikánka
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
jí	jíst	k5eAaImIp3nS	jíst
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snu	sen	k1gInSc6	sen
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
přišlo	přijít	k5eAaPmAgNnS	přijít
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
vzalo	vzít	k5eAaPmAgNnS	vzít
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
vedlo	vést	k5eAaImAgNnS	vést
ho	on	k3xPp3gMnSc4	on
k	k	k7c3	k
Egyptským	egyptský	k2eAgFnPc3d1	egyptská
pyramidám	pyramida	k1gFnPc3	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
mu	on	k3xPp3gNnSc3	on
řeklo	říct	k5eAaPmAgNnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
sem	sem	k6eAd1	sem
přijdeš	přijít	k5eAaPmIp2nS	přijít
<g/>
,	,	kIx,	,
najdeš	najít	k5eAaPmIp2nS	najít
zakopaný	zakopaný	k2eAgInSc1d1	zakopaný
poklad	poklad	k1gInSc1	poklad
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
chlapec	chlapec	k1gMnSc1	chlapec
probudil	probudit	k5eAaPmAgMnS	probudit
<g/>
.	.	kIx.	.
</s>
<s>
Cikánka	cikánka	k1gFnSc1	cikánka
mu	on	k3xPp3gMnSc3	on
slíbila	slíbit	k5eAaPmAgFnS	slíbit
sen	sen	k1gInSc4	sen
vyložit	vyložit	k5eAaPmF	vyložit
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
dá	dát	k5eAaPmIp3nS	dát
desetinu	desetina	k1gFnSc4	desetina
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
nalezne	naleznout	k5eAaPmIp3nS	naleznout
<g/>
.	.	kIx.	.
</s>
<s>
Mladík	mladík	k1gMnSc1	mladík
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
pravila	pravit	k5eAaBmAgFnS	pravit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jdi	jít	k5eAaImRp2nS	jít
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
k	k	k7c3	k
pyramidám	pyramida	k1gFnPc3	pyramida
a	a	k8xC	a
najdi	najít	k5eAaPmRp2nS	najít
tam	tam	k6eAd1	tam
ten	ten	k3xDgInSc4	ten
poklad	poklad	k1gInSc4	poklad
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Santiago	Santiago	k1gNnSc4	Santiago
si	se	k3xPyFc3	se
pomyslil	pomyslit	k5eAaPmAgMnS	pomyslit
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
jsem	být	k5eAaImIp1nS	být
sem	sem	k6eAd1	sem
nemusel	muset	k5eNaImAgMnS	muset
chodit	chodit	k5eAaImF	chodit
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
co	co	k3yInSc1	co
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zadarmo	zadarmo	k6eAd1	zadarmo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nepůjdu	jít	k5eNaImIp1nS	jít
poklad	poklad	k1gInSc4	poklad
hledat	hledat	k5eAaImF	hledat
<g/>
,	,	kIx,	,
cikánka	cikánka	k1gFnSc1	cikánka
také	také	k9	také
nic	nic	k3yNnSc1	nic
nedostane	dostat	k5eNaPmIp3nS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgMnS	být
pasáček	pasáček	k1gMnSc1	pasáček
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
do	do	k7c2	do
dalšího	další	k2eAgNnSc2d1	další
města	město	k1gNnSc2	město
prodávat	prodávat	k5eAaImF	prodávat
vlnu	vlna	k1gFnSc4	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
sedí	sedit	k5eAaImIp3nS	sedit
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
čte	číst	k5eAaImIp3nS	číst
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc4	svůj
jedinou	jediný	k2eAgFnSc4d1	jediná
knihu	kniha	k1gFnSc4	kniha
a	a	k8xC	a
tu	ten	k3xDgFnSc4	ten
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
promluví	promluvit	k5eAaPmIp3nS	promluvit
starý	starý	k2eAgMnSc1d1	starý
král	král	k1gMnSc1	král
a	a	k8xC	a
nabádá	nabádat	k5eAaBmIp3nS	nabádat
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
splnil	splnit	k5eAaPmAgInS	splnit
svůj	svůj	k3xOyFgInSc4	svůj
"	"	kIx"	"
<g/>
Osobní	osobní	k2eAgInSc4d1	osobní
příběh	příběh	k1gInSc4	příběh
<g/>
"	"	kIx"	"
–	–	k?	–
šel	jít	k5eAaImAgMnS	jít
hledat	hledat	k5eAaImF	hledat
poklad	poklad	k1gInSc4	poklad
<g/>
.	.	kIx.	.
</s>
<s>
Mladík	mladík	k1gInSc1	mladík
nakonec	nakonec	k6eAd1	nakonec
svolí	svolit	k5eAaPmIp3nS	svolit
<g/>
,	,	kIx,	,
prodá	prodat	k5eAaPmIp3nS	prodat
všechny	všechen	k3xTgFnPc4	všechen
svoje	svůj	k3xOyFgFnPc4	svůj
ovečky	ovečka	k1gFnPc4	ovečka
a	a	k8xC	a
za	za	k7c4	za
utržené	utržený	k2eAgInPc4d1	utržený
peníze	peníz	k1gInPc4	peníz
jede	jet	k5eAaImIp3nS	jet
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Sice	sice	k8xC	sice
se	se	k3xPyFc4	se
neumí	umět	k5eNaImIp3nS	umět
domluvit	domluvit	k5eAaPmF	domluvit
arabsky	arabsky	k6eAd1	arabsky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
putování	putování	k1gNnSc2	putování
s	s	k7c7	s
ovcemi	ovce	k1gFnPc7	ovce
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejen	nejen	k6eAd1	nejen
slovy	slovo	k1gNnPc7	slovo
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
domluví	domluvit	k5eAaPmIp3nS	domluvit
<g/>
.	.	kIx.	.
</s>
<s>
Zajde	zajít	k5eAaPmIp3nS	zajít
do	do	k7c2	do
první	první	k4xOgMnSc1	první
hospody	hospody	k?	hospody
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
najde	najít	k5eAaPmIp3nS	najít
<g/>
,	,	kIx,	,
a	a	k8xC	a
rád	rád	k6eAd1	rád
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
někoho	někdo	k3yInSc4	někdo
zeptal	zeptat	k5eAaPmAgMnS	zeptat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
si	se	k3xPyFc3	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
jeden	jeden	k4xCgMnSc1	jeden
Arab	Arab	k1gMnSc1	Arab
přisedne	přisednout	k5eAaPmIp3nS	přisednout
a	a	k8xC	a
promluví	promluvit	k5eAaPmIp3nS	promluvit
španělsky	španělsky	k6eAd1	španělsky
<g/>
.	.	kIx.	.
</s>
<s>
Vyjedná	vyjednat	k5eAaPmIp3nS	vyjednat
si	se	k3xPyFc3	se
od	od	k7c2	od
Santiaga	Santiago	k1gNnSc2	Santiago
všechny	všechen	k3xTgInPc4	všechen
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
dvou	dva	k4xCgMnPc2	dva
velbloudů	velbloud	k1gMnPc2	velbloud
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
je	on	k3xPp3gInPc4	on
koupit	koupit	k5eAaPmF	koupit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hospody	hospody	k?	hospody
ještě	ještě	k6eAd1	ještě
vyšli	vyjít	k5eAaPmAgMnP	vyjít
spolu	spolu	k6eAd1	spolu
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
najednou	najednou	k6eAd1	najednou
po	po	k7c6	po
průvodci	průvodce	k1gMnSc6	průvodce
ani	ani	k8xC	ani
stopy	stop	k1gInPc4	stop
a	a	k8xC	a
mladík	mladík	k1gMnSc1	mladík
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
úspory	úspora	k1gFnPc4	úspora
<g/>
.	.	kIx.	.
</s>
<s>
Smutný	Smutný	k1gMnSc1	Smutný
se	se	k3xPyFc4	se
toulal	toulat	k5eAaImAgMnS	toulat
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
narazil	narazit	k5eAaPmAgMnS	narazit
na	na	k7c6	na
sklenářství	sklenářství	k1gNnSc6	sklenářství
<g/>
.	.	kIx.	.
</s>
<s>
Majiteli	majitel	k1gMnSc3	majitel
krámku	krámek	k1gInSc3	krámek
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
vyleštit	vyleštit	k5eAaPmF	vyleštit
všechny	všechen	k3xTgFnPc4	všechen
sklenice	sklenice	k1gFnPc4	sklenice
ve	v	k7c6	v
výloze	výloha	k1gFnSc6	výloha
za	za	k7c4	za
trochu	trocha	k1gFnSc4	trocha
jídla	jídlo	k1gNnSc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
domluvili	domluvit	k5eAaPmAgMnP	domluvit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Santiago	Santiago	k1gNnSc1	Santiago
bude	být	k5eAaImBp3nS	být
u	u	k7c2	u
sklenáře	sklenář	k1gMnSc2	sklenář
pracovat	pracovat	k5eAaImF	pracovat
za	za	k7c4	za
podíl	podíl	k1gInSc4	podíl
ze	z	k7c2	z
zisku	zisk	k1gInSc2	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Majitel	majitel	k1gMnSc1	majitel
si	se	k3xPyFc3	se
pomyslel	pomyslet	k5eAaPmAgMnS	pomyslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
moc	moc	k6eAd1	moc
peněz	peníze	k1gInPc2	peníze
nebude	být	k5eNaImBp3nS	být
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
poloha	poloha	k1gFnSc1	poloha
krámku	krámek	k1gInSc2	krámek
je	být	k5eAaImIp3nS	být
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
a	a	k8xC	a
už	už	k6eAd1	už
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
nic	nic	k6eAd1	nic
neprodal	prodat	k5eNaPmAgMnS	prodat
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
Santiago	Santiago	k1gNnSc4	Santiago
měl	mít	k5eAaImAgMnS	mít
plno	plno	k6eAd1	plno
dobrých	dobrý	k2eAgInPc2d1	dobrý
nápadů	nápad	k1gInPc2	nápad
na	na	k7c4	na
zlepšení	zlepšení	k1gNnSc4	zlepšení
prodeje	prodej	k1gInSc2	prodej
a	a	k8xC	a
zisk	zisk	k1gInSc1	zisk
se	se	k3xPyFc4	se
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
viděl	vidět	k5eAaImAgMnS	vidět
svůj	svůj	k3xOyFgInSc4	svůj
cíl	cíl	k1gInSc4	cíl
–	–	k?	–
nashromáždit	nashromáždit	k5eAaPmF	nashromáždit
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
měl	mít	k5eAaImAgMnS	mít
mladík	mladík	k1gMnSc1	mladík
dost	dost	k6eAd1	dost
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
uskutečnění	uskutečnění	k1gNnSc4	uskutečnění
svého	svůj	k3xOyFgInSc2	svůj
snu	sen	k1gInSc2	sen
<g/>
.	.	kIx.	.
</s>
<s>
Domluvil	domluvit	k5eAaPmAgMnS	domluvit
se	se	k3xPyFc4	se
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
Karavanou	karavana	k1gFnSc7	karavana
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
vydá	vydat	k5eAaPmIp3nS	vydat
do	do	k7c2	do
pouště	poušť	k1gFnSc2	poušť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oáze	oáza	k1gFnSc6	oáza
Al-Fajjúm	Al-Fajjúma	k1gFnPc2	Al-Fajjúma
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
ženy	žena	k1gFnSc2	žena
pouště	poušť	k1gFnSc2	poušť
Fátimy	Fátima	k1gFnSc2	Fátima
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
si	se	k3xPyFc3	se
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
našel	najít	k5eAaPmAgMnS	najít
svůj	svůj	k3xOyFgInSc4	svůj
poklad	poklad	k1gInSc4	poklad
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
potkal	potkat	k5eAaPmAgInS	potkat
Alchymistu	alchymista	k1gMnSc4	alchymista
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
ho	on	k3xPp3gInSc4	on
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
poklad	poklad	k1gInSc1	poklad
přece	přece	k9	přece
leží	ležet	k5eAaImIp3nS	ležet
u	u	k7c2	u
Egyptských	egyptský	k2eAgFnPc2d1	egyptská
pyramid	pyramida	k1gFnPc2	pyramida
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
v	v	k7c6	v
oáze	oáza	k1gFnSc6	oáza
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
tentokrát	tentokrát	k6eAd1	tentokrát
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
Alchymisty	alchymista	k1gMnSc2	alchymista
vydali	vydat	k5eAaPmAgMnP	vydat
k	k	k7c3	k
pyramidám	pyramida	k1gFnPc3	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Cestu	cesta	k1gFnSc4	cesta
jim	on	k3xPp3gMnPc3	on
znepříjemňovala	znepříjemňovat	k5eAaImAgFnS	znepříjemňovat
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
kmeny	kmen	k1gInPc7	kmen
pouště	poušť	k1gFnSc2	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
dorazili	dorazit	k5eAaPmAgMnP	dorazit
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
nedaleko	nedaleko	k7c2	nedaleko
pyramid	pyramida	k1gFnPc2	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
Alchymista	alchymista	k1gMnSc1	alchymista
se	s	k7c7	s
Santiagem	Santiago	k1gNnSc7	Santiago
rozloučil	rozloučit	k5eAaPmAgInS	rozloučit
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
mu	on	k3xPp3gNnSc3	on
dal	dát	k5eAaPmAgInS	dát
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
kousek	kousek	k1gInSc1	kousek
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
kousek	kousek	k1gInSc4	kousek
mu	on	k3xPp3gMnSc3	on
nechal	nechat	k5eAaPmAgMnS	nechat
u	u	k7c2	u
mnichů	mnich	k1gMnPc2	mnich
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
ho	on	k3xPp3gMnSc4	on
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
vracet	vracet	k5eAaImF	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Santiago	Santiago	k1gNnSc4	Santiago
nazítří	nazítří	k6eAd1	nazítří
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
k	k	k7c3	k
pyramidám	pyramida	k1gFnPc3	pyramida
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
srdce	srdce	k1gNnSc2	srdce
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
promluvilo	promluvit	k5eAaPmAgNnS	promluvit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Až	až	k6eAd1	až
se	se	k3xPyFc4	se
rozpláčeš	rozplakat	k5eAaPmIp2nS	rozplakat
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
budeš	být	k5eAaImBp2nS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
pokladu	poklad	k1gInSc2	poklad
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mladík	mladík	k1gMnSc1	mladík
začal	začít	k5eAaPmAgMnS	začít
kopat	kopat	k5eAaImF	kopat
nedaleko	nedaleko	k7c2	nedaleko
pyramid	pyramida	k1gFnPc2	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
kopal	kopat	k5eAaImAgMnS	kopat
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
najednou	najednou	k6eAd1	najednou
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
přišli	přijít	k5eAaPmAgMnP	přijít
dva	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
a	a	k8xC	a
chtěli	chtít	k5eAaImAgMnP	chtít
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Mysleli	myslet	k5eAaImAgMnP	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
něco	něco	k3yInSc4	něco
schovává	schovávat	k5eAaImIp3nS	schovávat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
Santiaga	Santiago	k1gNnPc4	Santiago
prohledali	prohledat	k5eAaPmAgMnP	prohledat
a	a	k8xC	a
našli	najít	k5eAaPmAgMnP	najít
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
mu	on	k3xPp3gMnSc3	on
sebrali	sebrat	k5eAaPmAgMnP	sebrat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ho	on	k3xPp3gMnSc4	on
tloukli	tlouct	k5eAaImAgMnP	tlouct
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
vymlátili	vymlátit	k5eAaPmAgMnP	vymlátit
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
,	,	kIx,	,
že	že	k8xS	že
hledá	hledat	k5eAaImIp3nS	hledat
poklad	poklad	k1gInSc4	poklad
<g/>
.	.	kIx.	.
</s>
<s>
Lupiči	lupič	k1gMnPc1	lupič
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vysmáli	vysmát	k5eAaPmAgMnP	vysmát
a	a	k8xC	a
nechali	nechat	k5eAaPmAgMnP	nechat
ho	on	k3xPp3gNnSc4	on
jít	jít	k5eAaImF	jít
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
než	než	k8xS	než
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
ten	ten	k3xDgMnSc1	ten
větší	veliký	k2eAgMnSc1d2	veliký
z	z	k7c2	z
lupičů	lupič	k1gMnPc2	lupič
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zrovna	zrovna	k6eAd1	zrovna
před	před	k7c7	před
dvěma	dva	k4xCgInPc7	dva
roky	rok	k1gInPc7	rok
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
zdál	zdát	k5eAaImAgInS	zdát
dvakrát	dvakrát	k6eAd1	dvakrát
po	po	k7c6	po
sobě	se	k3xPyFc3	se
sen	sen	k1gInSc4	sen
<g/>
.	.	kIx.	.
</s>
<s>
Zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
že	že	k8xS	že
mám	mít	k5eAaImIp1nS	mít
jet	jet	k5eAaImF	jet
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
vyhledat	vyhledat	k5eAaPmF	vyhledat
polo	polo	k6eAd1	polo
zbořený	zbořený	k2eAgInSc4d1	zbořený
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
uprostřed	uprostřed	k6eAd1	uprostřed
roste	růst	k5eAaImIp3nS	růst
fíkovník	fíkovník	k1gInSc4	fíkovník
a	a	k8xC	a
kde	kde	k6eAd1	kde
pastýři	pastýř	k1gMnPc1	pastýř
často	často	k6eAd1	často
přespávají	přespávat	k5eAaImIp3nP	přespávat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snu	sen	k1gInSc6	sen
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
pod	pod	k7c7	pod
fíkovníkem	fíkovník	k1gInSc7	fíkovník
budu	být	k5eAaImBp1nS	být
kopat	kopat	k5eAaImF	kopat
<g/>
,	,	kIx,	,
naleznu	nalézt	k5eAaBmIp1nS	nalézt
poklad	poklad	k1gInSc1	poklad
<g/>
.	.	kIx.	.
</s>
<s>
Nejsem	být	k5eNaImIp1nS	být
takový	takový	k3xDgMnSc1	takový
blázen	blázen	k1gMnSc1	blázen
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
putoval	putovat	k5eAaImAgInS	putovat
přes	přes	k7c4	přes
celou	celý	k2eAgFnSc4d1	celá
poušť	poušť	k1gFnSc4	poušť
jenom	jenom	k9	jenom
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
zdál	zdát	k5eAaImAgInS	zdát
sen	sen	k1gInSc1	sen
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c6	po
těchto	tento	k3xDgNnPc6	tento
slovech	slovo	k1gNnPc6	slovo
Santiago	Santiago	k1gNnSc4	Santiago
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nalezl	nalézt	k5eAaBmAgInS	nalézt
poklad	poklad	k1gInSc1	poklad
<g/>
.	.	kIx.	.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
s	s	k7c7	s
posledním	poslední	k2eAgInSc7d1	poslední
kusem	kus	k1gInSc7	kus
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mu	on	k3xPp3gMnSc3	on
Alchymista	alchymista	k1gMnSc1	alchymista
nechal	nechat	k5eAaPmAgMnS	nechat
u	u	k7c2	u
mnicha	mnich	k1gMnSc2	mnich
<g/>
,	,	kIx,	,
a	a	k8xC	a
našel	najít	k5eAaPmAgMnS	najít
poklad	poklad	k1gInSc4	poklad
<g/>
.	.	kIx.	.
</s>
<s>
Náhle	náhle	k6eAd1	náhle
na	na	k7c6	na
tváři	tvář	k1gFnSc6	tvář
ucítil	ucítit	k5eAaPmAgInS	ucítit
závan	závan	k1gInSc4	závan
větru	vítr	k1gInSc2	vítr
a	a	k8xC	a
povídá	povídat	k5eAaImIp3nS	povídat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Fátimo	Fátima	k1gFnSc5	Fátima
<g/>
,	,	kIx,	,
ženo	žena	k1gFnSc5	žena
mého	můj	k3xOp1gNnSc2	můj
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
jdu	jít	k5eAaImIp1nS	jít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Alchymista	alchymista	k1gMnSc1	alchymista
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Pavla	Pavla	k1gFnSc1	Pavla
Lidmilová	Lidmilová	k1gFnSc1	Lidmilová
<g/>
,	,	kIx,	,
Jota	jota	k1gNnSc1	jota
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
Alchymista	alchymista	k1gMnSc1	alchymista
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Pavla	Pavla	k1gFnSc1	Pavla
Lidmilová	Lidmilová	k1gFnSc1	Lidmilová
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vydán	vydat	k5eAaPmNgInS	vydat
i	i	k9	i
jako	jako	k8xS	jako
audiokniha	audiokniha	k1gFnSc1	audiokniha
na	na	k7c6	na
CD	CD	kA	CD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
zkrácenou	zkrácený	k2eAgFnSc4d1	zkrácená
verzi	verze	k1gFnSc4	verze
načetl	načíst	k5eAaBmAgMnS	načíst
Marek	Marek	k1gMnSc1	Marek
Eben	eben	k1gInSc4	eben
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
českými	český	k2eAgMnPc7d1	český
herci	herec	k1gMnPc7	herec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
pak	pak	k6eAd1	pak
celou	celý	k2eAgFnSc4d1	celá
knihu	kniha	k1gFnSc4	kniha
načetl	načíst	k5eAaBmAgMnS	načíst
Lukáš	Lukáš	k1gMnSc1	Lukáš
Hlavica	Hlavica	k1gMnSc1	Hlavica
<g/>
.	.	kIx.	.
</s>
