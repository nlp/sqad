<s>
Alchymista	alchymista	k1gMnSc1	alchymista
(	(	kIx(	(
<g/>
v	v	k7c6	v
portugalském	portugalský	k2eAgInSc6d1	portugalský
originále	originál	k1gInSc6	originál
O	o	k7c6	o
Alquimista	Alquimista	k1gMnSc1	Alquimista
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
brazilského	brazilský	k2eAgMnSc2d1	brazilský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Paula	Paul	k1gMnSc2	Paul
Coelha	Coelh	k1gMnSc2	Coelh
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
Rio	Rio	k1gFnSc7	Rio
de	de	k?	de
Janeiru	Janeir	k1gInSc2	Janeir
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
