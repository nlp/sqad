<s>
Dana	Dana	k1gFnSc1	Dana
Zátopková	Zátopková	k1gFnSc1	Zátopková
(	(	kIx(	(
<g/>
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Ingrová	Ingrová	k1gFnSc1	Ingrová
<g/>
,	,	kIx,	,
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1922	[number]	k4	1922
Fryštát	Fryštát	k1gInSc1	Fryštát
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
česká	český	k2eAgFnSc1d1	Česká
atletka	atletka	k1gFnSc1	atletka
–	–	k?	–
oštěpařka	oštěpařka	k1gFnSc1	oštěpařka
<g/>
,	,	kIx,	,
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vítězka	vítězka	k1gFnSc1	vítězka
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
manželkou	manželka	k1gFnSc7	manželka
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgMnPc2d3	nejslavnější
českých	český	k2eAgMnPc2d1	český
sportovců	sportovec	k1gMnPc2	sportovec
Emila	Emil	k1gMnSc2	Emil
Zátopka	Zátopek	k1gMnSc2	Zátopek
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
a	a	k8xC	a
rok	rok	k1gInSc4	rok
jako	jako	k8xS	jako
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Daně	Dana	k1gFnSc6	Dana
Ingrové	Ingrové	k2eAgNnPc2d1	Ingrové
šest	šest	k4xCc1	šest
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
z	z	k7c2	z
Karviné	Karviná	k1gFnSc2	Karviná
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Vacenovic	Vacenovice	k1gFnPc2	Vacenovice
u	u	k7c2	u
Hodonína	Hodonín	k1gInSc2	Hodonín
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
atletikou	atletika	k1gFnSc7	atletika
se	se	k3xPyFc4	se
Dana	Dana	k1gFnSc1	Dana
poprvé	poprvé	k6eAd1	poprvé
seznámila	seznámit	k5eAaPmAgFnS	seznámit
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
kroky	krok	k1gInPc1	krok
ovšem	ovšem	k9	ovšem
nevedly	vést	k5eNaImAgInP	vést
automaticky	automaticky	k6eAd1	automaticky
k	k	k7c3	k
hodu	hod	k1gInSc3	hod
oštěpem	oštěp	k1gInSc7	oštěp
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ji	on	k3xPp3gFnSc4	on
o	o	k7c6	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
proslavil	proslavit	k5eAaPmAgInS	proslavit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
se	se	k3xPyFc4	se
Dana	Dana	k1gFnSc1	Dana
Ingrová	Ingrová	k1gFnSc1	Ingrová
aktivně	aktivně	k6eAd1	aktivně
věnovala	věnovat	k5eAaImAgFnS	věnovat
házené	házená	k1gFnPc4	házená
<g/>
.	.	kIx.	.
</s>
<s>
Všesportovní	všesportovní	k2eAgNnSc4d1	všesportovní
nadání	nadání	k1gNnSc4	nadání
dokázala	dokázat	k5eAaPmAgFnS	dokázat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
týmu	tým	k1gInSc2	tým
Slovácká	slovácký	k2eAgFnSc1d1	Slovácká
Slavie	slavie	k1gFnSc1	slavie
mistryní	mistryně	k1gFnPc2	mistryně
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
házené	házená	k1gFnSc6	házená
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
házenou	házená	k1gFnSc7	házená
začala	začít	k5eAaPmAgFnS	začít
Dana	Dana	k1gFnSc1	Dana
Ingrová	Ingrová	k1gFnSc1	Ingrová
opět	opět	k6eAd1	opět
věnovat	věnovat	k5eAaImF	věnovat
i	i	k8xC	i
atletice	atletika	k1gFnSc3	atletika
a	a	k8xC	a
shodou	shoda	k1gFnSc7	shoda
náhod	náhoda	k1gFnPc2	náhoda
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
k	k	k7c3	k
náčiní	náčiní	k1gNnSc3	náčiní
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ji	on	k3xPp3gFnSc4	on
později	pozdě	k6eAd2	pozdě
proslavilo	proslavit	k5eAaPmAgNnS	proslavit
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
k	k	k7c3	k
oštěpu	oštěp	k1gInSc3	oštěp
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
jako	jako	k8xC	jako
mistryně	mistryně	k1gFnSc1	mistryně
republiky	republika	k1gFnSc2	republika
odjela	odjet	k5eAaPmAgFnS	odjet
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
Československo	Československo	k1gNnSc4	Československo
na	na	k7c6	na
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
sedmém	sedmý	k4xOgInSc6	sedmý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
olympiádě	olympiáda	k1gFnSc6	olympiáda
se	se	k3xPyFc4	se
Dana	Dana	k1gFnSc1	Dana
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
vytrvalostního	vytrvalostní	k2eAgMnSc4d1	vytrvalostní
běžce	běžec	k1gMnSc4	běžec
Emila	Emil	k1gMnSc2	Emil
Zátopka	Zátopek	k1gMnSc2	Zátopek
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
svazek	svazek	k1gInSc1	svazek
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgInS	stát
svazkem	svazek	k1gInSc7	svazek
dvou	dva	k4xCgMnPc2	dva
výjimečných	výjimečný	k2eAgMnPc2d1	výjimečný
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
,	,	kIx,	,
olympijských	olympijský	k2eAgMnPc2d1	olympijský
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
byly	být	k5eAaImAgInP	být
velmi	velmi	k6eAd1	velmi
blízké	blízký	k2eAgFnSc2d1	blízká
základní	základní	k2eAgFnSc2d1	základní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
myšlenky	myšlenka	k1gFnSc2	myšlenka
a	a	k8xC	a
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgInSc2	svůj
největšího	veliký	k2eAgInSc2d3	veliký
úspěchu	úspěch	k1gInSc2	úspěch
se	se	k3xPyFc4	se
Dana	Dana	k1gFnSc1	Dana
Zátopková	Zátopkový	k2eAgFnSc1d1	Zátopková
dočkala	dočkat	k5eAaPmAgFnS	dočkat
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
na	na	k7c6	na
XV	XV	kA	XV
<g/>
.	.	kIx.	.
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
vítězství	vítězství	k1gNnSc2	vítězství
svého	svůj	k3xOyFgMnSc2	svůj
manžela	manžel	k1gMnSc2	manžel
Emila	Emil	k1gMnSc2	Emil
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
5	[number]	k4	5
000	[number]	k4	000
metrů	metr	k1gInPc2	metr
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
disciplíně	disciplína	k1gFnSc6	disciplína
i	i	k8xC	i
ona	onen	k3xDgFnSc1	onen
hodem	hod	k1gInSc7	hod
za	za	k7c4	za
hranici	hranice	k1gFnSc4	hranice
padesáti	padesát	k4xCc2	padesát
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
50,47	[number]	k4	50,47
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
úspěchy	úspěch	k1gInPc1	úspěch
pak	pak	k6eAd1	pak
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nenechaly	nechat	k5eNaPmAgFnP	nechat
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
<g/>
.	.	kIx.	.
</s>
<s>
Mnohonásobný	mnohonásobný	k2eAgInSc4d1	mnohonásobný
zisk	zisk	k1gInSc4	zisk
titulu	titul	k1gInSc2	titul
mistryně	mistryně	k1gFnSc1	mistryně
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
mistryně	mistryně	k1gFnSc2	mistryně
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1954	[number]	k4	1954
a	a	k8xC	a
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
překonání	překonání	k1gNnSc1	překonání
světového	světový	k2eAgInSc2d1	světový
rekordu	rekord	k1gInSc2	rekord
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
(	(	kIx(	(
<g/>
55,73	[number]	k4	55,73
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
zisk	zisk	k1gInSc1	zisk
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
medaile	medaile	k1gFnSc2	medaile
ze	z	k7c2	z
XVII	XVII	kA	XVII
<g/>
.	.	kIx.	.
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
úspěch	úspěch	k1gInSc1	úspěch
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
nebyl	být	k5eNaImAgInS	být
náhodným	náhodný	k2eAgNnSc7d1	náhodné
vítězstvím	vítězství	k1gNnSc7	vítězství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vrcholem	vrchol	k1gInSc7	vrchol
kariéry	kariéra	k1gFnSc2	kariéra
vynikající	vynikající	k2eAgFnSc2d1	vynikající
sportovkyně	sportovkyně	k1gFnSc2	sportovkyně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
aktivní	aktivní	k2eAgFnSc2d1	aktivní
sportovní	sportovní	k2eAgFnSc2d1	sportovní
kariéry	kariéra	k1gFnSc2	kariéra
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
se	se	k3xPyFc4	se
Dana	Dana	k1gFnSc1	Dana
Zátopková	Zátopková	k1gFnSc1	Zátopková
věnovala	věnovat	k5eAaPmAgFnS	věnovat
trenérské	trenérský	k2eAgFnPc4d1	trenérská
činnosti	činnost	k1gFnPc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
členkou	členka	k1gFnSc7	členka
ženské	ženský	k2eAgFnSc2d1	ženská
komise	komise	k1gFnSc2	komise
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
atletické	atletický	k2eAgFnSc2d1	atletická
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
Dana	Dana	k1gFnSc1	Dana
Zátopková	Zátopková	k1gFnSc1	Zátopková
aktivně	aktivně	k6eAd1	aktivně
účastní	účastnit	k5eAaImIp3nS	účastnit
českého	český	k2eAgNnSc2d1	české
olympijského	olympijský	k2eAgNnSc2d1	Olympijské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
