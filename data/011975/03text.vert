<p>
<s>
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
Řásnovka	Řásnovka	k1gFnSc1	Řásnovka
je	být	k5eAaImIp3nS	být
dobrodružná	dobrodružný	k2eAgFnSc1d1	dobrodružná
kniha	kniha	k1gFnSc1	kniha
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
peripetie	peripetie	k1gFnSc1	peripetie
formování	formování	k1gNnSc2	formování
chlapeckého	chlapecký	k2eAgInSc2d1	chlapecký
klubu	klub	k1gInSc2	klub
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Řásnovka	Řásnovka	k1gFnSc1	Řásnovka
(	(	kIx(	(
<g/>
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
pražskou	pražský	k2eAgFnSc7d1	Pražská
Řásnovkou	Řásnovka	k1gFnSc7	Řásnovka
jen	jen	k9	jen
inspirované	inspirovaný	k2eAgFnPc1d1	inspirovaná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
z	z	k7c2	z
pětice	pětice	k1gFnSc2	pětice
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Foglarovi	Foglar	k1gMnSc3	Foglar
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
dvacetileté	dvacetiletý	k2eAgFnSc6d1	dvacetiletá
pauze	pauza	k1gFnSc6	pauza
vynucené	vynucený	k2eAgNnSc1d1	vynucené
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
cenzurou	cenzura	k1gFnSc7	cenzura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Jindra	Jindra	k1gMnSc1	Jindra
Sochor	Sochor	k1gMnSc1	Sochor
je	být	k5eAaImIp3nS	být
čestný	čestný	k2eAgMnSc1d1	čestný
a	a	k8xC	a
citlivý	citlivý	k2eAgMnSc1d1	citlivý
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
trpí	trpět	k5eAaImIp3nS	trpět
hádkami	hádka	k1gFnPc7	hádka
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
se	se	k3xPyFc4	se
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c4	na
učení	učení	k1gNnSc4	učení
<g/>
.	.	kIx.	.
</s>
<s>
Učitel	učitel	k1gMnSc1	učitel
chemie	chemie	k1gFnSc2	chemie
mu	on	k3xPp3gMnSc3	on
hrozí	hrozit	k5eAaImIp3nP	hrozit
propadnutím	propadnutí	k1gNnSc7	propadnutí
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
v	v	k7c6	v
zoufalství	zoufalství	k1gNnSc6	zoufalství
uteče	utéct	k5eAaPmIp3nS	utéct
z	z	k7c2	z
domova	domov	k1gInSc2	domov
<g/>
.	.	kIx.	.
</s>
<s>
Dostane	dostat	k5eAaPmIp3nS	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
jemu	on	k3xPp3gNnSc3	on
neznámé	známý	k2eNgFnPc1d1	neznámá
čtvrti	čtvrt	k1gFnPc1	čtvrt
zvané	zvaný	k2eAgFnPc1d1	zvaná
Řásnovka	Řásnovka	k1gFnSc1	Řásnovka
a	a	k8xC	a
při	při	k7c6	při
přespání	přespání	k1gNnSc6	přespání
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
dvoře	dvůr	k1gInSc6	dvůr
natrefí	natrefit	k5eAaPmIp3nS	natrefit
na	na	k7c4	na
dvojici	dvojice	k1gFnSc4	dvojice
výrostků	výrostek	k1gMnPc2	výrostek
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zde	zde	k6eAd1	zde
něco	něco	k3yInSc4	něco
schovávají	schovávat	k5eAaImIp3nP	schovávat
<g/>
.	.	kIx.	.
</s>
<s>
Jindrovi	Jindrův	k2eAgMnPc1d1	Jindrův
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
dojde	dojít	k5eAaPmIp3nS	dojít
neudržitelnost	neudržitelnost	k1gFnSc1	neudržitelnost
jeho	jeho	k3xOp3gFnSc2	jeho
situace	situace	k1gFnSc2	situace
a	a	k8xC	a
podpořen	podpořit	k5eAaPmNgMnS	podpořit
svým	svůj	k3xOyFgMnSc7	svůj
chápajícím	chápající	k2eAgMnSc7d1	chápající
učitelem	učitel	k1gMnSc7	učitel
Holasem	Holas	k1gMnSc7	Holas
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
okouzlující	okouzlující	k2eAgFnSc4d1	okouzlující
Řásnovku	Řásnovka	k1gFnSc4	Řásnovka
ale	ale	k8xC	ale
nemůže	moct	k5eNaImIp3nS	moct
zapomenout	zapomenout	k5eAaPmF	zapomenout
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jindra	Jindra	k1gFnSc1	Jindra
musí	muset	k5eAaImIp3nS	muset
školy	škola	k1gFnPc4	škola
nechat	nechat	k5eAaPmF	nechat
a	a	k8xC	a
jít	jít	k5eAaImF	jít
do	do	k7c2	do
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
vybere	vybrat	k5eAaPmIp3nS	vybrat
si	se	k3xPyFc3	se
Jindra	Jindra	k1gFnSc1	Jindra
truhlářskou	truhlářský	k2eAgFnSc4d1	truhlářská
dílnu	dílna	k1gFnSc4	dílna
právě	právě	k9	právě
v	v	k7c6	v
Řásnovce	Řásnovka	k1gFnSc6	Řásnovka
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
zděšení	zděšení	k1gNnSc3	zděšení
však	však	k9	však
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgMnSc1	jeden
tamní	tamní	k2eAgMnSc1d1	tamní
učeň	učeň	k1gMnSc1	učeň
Finťula	Finťul	k1gMnSc2	Finťul
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
podezřelé	podezřelý	k2eAgFnSc2d1	podezřelá
dvojice	dvojice	k1gFnSc2	dvojice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
tehdy	tehdy	k6eAd1	tehdy
přistihl	přistihnout	k5eAaPmAgMnS	přistihnout
při	při	k7c6	při
ukrývání	ukrývání	k1gNnSc6	ukrývání
balíku	balík	k1gInSc2	balík
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
mezitím	mezitím	k6eAd1	mezitím
z	z	k7c2	z
úkrytu	úkryt	k1gInSc2	úkryt
zmizel	zmizet	k5eAaPmAgMnS	zmizet
a	a	k8xC	a
Finťula	Finťula	k1gFnSc1	Finťula
podezřívá	podezřívat	k5eAaImIp3nS	podezřívat
Jindru	Jindra	k1gFnSc4	Jindra
<g/>
.	.	kIx.	.
</s>
<s>
Nemůže	moct	k5eNaImIp3nS	moct
mu	on	k3xPp3gMnSc3	on
nic	nic	k6eAd1	nic
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
chvíle	chvíle	k1gFnSc2	chvíle
mu	on	k3xPp3gMnSc3	on
dělá	dělat	k5eAaImIp3nS	dělat
samé	samý	k3xTgFnPc4	samý
nepříjemnosti	nepříjemnost	k1gFnPc4	nepříjemnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jindra	Jindra	k1gFnSc1	Jindra
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
Řásnovce	Řásnovka	k1gFnSc6	Řásnovka
nějaké	nějaký	k3yIgMnPc4	nějaký
kamarády	kamarád	k1gMnPc4	kamarád
a	a	k8xC	a
dostane	dostat	k5eAaPmIp3nS	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
party	parta	k1gFnSc2	parta
čtyř	čtyři	k4xCgMnPc2	čtyři
hochů	hoch	k1gMnPc2	hoch
<g/>
,	,	kIx,	,
Míry	Míra	k1gMnSc2	Míra
<g/>
,	,	kIx,	,
Ríši	Ríša	k1gMnSc2	Ríša
<g/>
,	,	kIx,	,
Otíka	Otík	k1gMnSc2	Otík
a	a	k8xC	a
vedoucího	vedoucí	k1gMnSc2	vedoucí
Vildy	Vilda	k1gMnSc2	Vilda
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
ještě	ještě	k6eAd1	ještě
nemají	mít	k5eNaImIp3nP	mít
název	název	k1gInSc4	název
ani	ani	k8xC	ani
klubovnu	klubovna	k1gFnSc4	klubovna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechno	všechen	k3xTgNnSc4	všechen
úsilí	úsilí	k1gNnSc4	úsilí
věnují	věnovat	k5eAaPmIp3nP	věnovat
jejímu	její	k3xOp3gNnSc3	její
hledání	hledání	k1gNnSc3	hledání
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
různých	různý	k2eAgFnPc2d1	různá
situací	situace	k1gFnPc2	situace
se	se	k3xPyFc4	se
Vilda	Vilda	k1gMnSc1	Vilda
projevuje	projevovat	k5eAaImIp3nS	projevovat
jako	jako	k9	jako
sobecký	sobecký	k2eAgMnSc1d1	sobecký
a	a	k8xC	a
vznětlivý	vznětlivý	k2eAgMnSc1d1	vznětlivý
autoritář	autoritář	k1gMnSc1	autoritář
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
charismatickém	charismatický	k2eAgNnSc6d1	charismatické
a	a	k8xC	a
uvážlivém	uvážlivý	k2eAgMnSc6d1	uvážlivý
Jindrovi	Jindra	k1gMnSc6	Jindra
vidí	vidět	k5eAaImIp3nS	vidět
ohrožení	ohrožení	k1gNnSc3	ohrožení
své	svůj	k3xOyFgFnSc2	svůj
vůdcovské	vůdcovský	k2eAgFnSc2d1	vůdcovská
pozice	pozice	k1gFnSc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
ten	ten	k3xDgMnSc1	ten
nic	nic	k3yNnSc4	nic
takového	takový	k3xDgNnSc2	takový
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc7	svůj
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
autoritou	autorita	k1gFnSc7	autorita
si	se	k3xPyFc3	se
získá	získat	k5eAaPmIp3nS	získat
ostatní	ostatní	k2eAgMnPc4d1	ostatní
hochy	hoch	k1gMnPc4	hoch
a	a	k8xC	a
Vilda	Vild	k1gMnSc4	Vild
nakonec	nakonec	k6eAd1	nakonec
po	po	k7c6	po
prudké	prudký	k2eAgFnSc6d1	prudká
hádce	hádka	k1gFnSc6	hádka
z	z	k7c2	z
party	parta	k1gFnSc2	parta
uraženě	uraženě	k6eAd1	uraženě
odejde	odejít	k5eAaPmIp3nS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Zbylá	zbylý	k2eAgFnSc1d1	zbylá
čtveřice	čtveřice	k1gFnSc1	čtveřice
si	se	k3xPyFc3	se
dá	dát	k5eAaPmIp3nS	dát
název	název	k1gInSc1	název
Stráž	stráž	k1gFnSc4	stráž
Řásnovky	Řásnovka	k1gFnPc1	Řásnovka
a	a	k8xC	a
vytkne	vytknout	k5eAaPmIp3nS	vytknout
si	se	k3xPyFc3	se
cíl	cíl	k1gInSc4	cíl
bránit	bránit	k5eAaImF	bránit
mezi	mezi	k7c7	mezi
dětmi	dítě	k1gFnPc7	dítě
v	v	k7c6	v
Řásnovce	Řásnovka	k1gFnSc6	Řásnovka
pořádek	pořádek	k1gInSc4	pořádek
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
i	i	k9	i
najít	najít	k5eAaPmF	najít
klubovnu	klubovna	k1gFnSc4	klubovna
<g/>
,	,	kIx,	,
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
domu	dům	k1gInSc2	dům
kde	kde	k6eAd1	kde
bydlí	bydlet	k5eAaImIp3nS	bydlet
Otík	Otík	k1gMnSc1	Otík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klub	klub	k1gInSc1	klub
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
prosperuje	prosperovat	k5eAaImIp3nS	prosperovat
<g/>
,	,	kIx,	,
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
se	se	k3xPyFc4	se
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
členy	člen	k1gInPc4	člen
a	a	k8xC	a
uspořádá	uspořádat	k5eAaPmIp3nS	uspořádat
závody	závod	k1gInPc4	závod
na	na	k7c6	na
vozíčcích	vozíček	k1gInPc6	vozíček
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
přinesou	přinést	k5eAaPmIp3nP	přinést
velkou	velký	k2eAgFnSc4d1	velká
popularitu	popularita	k1gFnSc4	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ale	ale	k9	ale
musí	muset	k5eAaImIp3nS	muset
neustále	neustále	k6eAd1	neustále
čelit	čelit	k5eAaImF	čelit
nepřátelství	nepřátelství	k1gNnSc4	nepřátelství
<g/>
,	,	kIx,	,
pomluvám	pomluva	k1gFnPc3	pomluva
a	a	k8xC	a
sabotážím	sabotáž	k1gFnPc3	sabotáž
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Finťuly	Finťula	k1gFnSc2	Finťula
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
kumpána	kumpána	k?	kumpána
Lipka	lipka	k1gFnSc1	lipka
a	a	k8xC	a
také	také	k6eAd1	také
zhrzeného	zhrzený	k2eAgMnSc2d1	zhrzený
Vildy	Vilda	k1gMnSc2	Vilda
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Jindru	Jindra	k1gFnSc4	Jindra
nenávidí	návidět	k5eNaImIp3nS	návidět
a	a	k8xC	a
přidá	přidat	k5eAaPmIp3nS	přidat
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
trojici	trojice	k1gFnSc3	trojice
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
poštvat	poštvat	k5eAaPmF	poštvat
proti	proti	k7c3	proti
klubu	klub	k1gInSc3	klub
obyvatele	obyvatel	k1gMnPc4	obyvatel
Otíkova	Otíkův	k2eAgInSc2d1	Otíkův
domu	dům	k1gInSc2	dům
a	a	k8xC	a
Stráž	stráž	k1gFnSc1	stráž
Řásnovky	Řásnovka	k1gFnSc2	Řásnovka
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
klubovny	klubovna	k1gFnSc2	klubovna
nakonec	nakonec	k6eAd1	nakonec
vyhozena	vyhozen	k2eAgFnSc1d1	vyhozena
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dalším	další	k2eAgNnSc6d1	další
zoufalém	zoufalý	k2eAgNnSc6d1	zoufalé
hledání	hledání	k1gNnSc6	hledání
se	se	k3xPyFc4	se
seznámí	seznámit	k5eAaPmIp3nS	seznámit
s	s	k7c7	s
chlapcem	chlapec	k1gMnSc7	chlapec
Vendou	Venda	k1gMnSc7	Venda
zvaným	zvaný	k2eAgInSc7d1	zvaný
Koníčkář	koníčkář	k1gMnSc1	koníčkář
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jim	on	k3xPp3gMnPc3	on
ukáže	ukázat	k5eAaPmIp3nS	ukázat
místnost	místnost	k1gFnSc1	místnost
u	u	k7c2	u
stájí	stáj	k1gFnPc2	stáj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
scházet	scházet	k5eAaImF	scházet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
Stráž	stráž	k1gFnSc4	stráž
osloví	oslovit	k5eAaPmIp3nS	oslovit
Tonda	Tonda	k1gMnSc1	Tonda
Menčík	Menčík	k1gMnSc1	Menčík
-	-	kIx~	-
vynálezce	vynálezce	k1gMnSc1	vynálezce
stolní	stolní	k2eAgFnSc2d1	stolní
hry	hra	k1gFnSc2	hra
Fan-tan	Fanan	k1gInSc1	Fan-tan
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Řásnovce	Řásnovka	k1gFnSc6	Řásnovka
všeobecně	všeobecně	k6eAd1	všeobecně
oblíbena	oblíben	k2eAgFnSc1d1	oblíbena
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
podnik	podnik	k1gInSc1	podnik
ji	on	k3xPp3gFnSc4	on
nyní	nyní	k6eAd1	nyní
chce	chtít	k5eAaImIp3nS	chtít
začít	začít	k5eAaPmF	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
a	a	k8xC	a
prodávat	prodávat	k5eAaImF	prodávat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
autorství	autorství	k1gNnSc6	autorství
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
honorář	honorář	k1gInSc4	honorář
se	se	k3xPyFc4	se
však	však	k9	však
hlásí	hlásit	k5eAaImIp3nS	hlásit
Menčíkův	Menčíkův	k2eAgMnSc1d1	Menčíkův
spolužák	spolužák	k1gMnSc1	spolužák
Hřivnas	Hřivnas	k1gMnSc1	Hřivnas
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
příspěvek	příspěvek	k1gInSc1	příspěvek
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
zanedbatelný	zanedbatelný	k2eAgInSc1d1	zanedbatelný
<g/>
.	.	kIx.	.
</s>
<s>
Zakřiknutý	zakřiknutý	k2eAgInSc1d1	zakřiknutý
Menčík	Menčík	k1gInSc1	Menčík
prosí	prosit	k5eAaImIp3nS	prosit
Stráž	stráž	k1gFnSc4	stráž
Řásnovky	Řásnovka	k1gFnSc2	Řásnovka
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
proti	proti	k7c3	proti
této	tento	k3xDgFnSc3	tento
nespravedlnosti	nespravedlnost	k1gFnSc3	nespravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Ukáže	ukázat	k5eAaPmIp3nS	ukázat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
sešit	sešit	k1gInSc1	sešit
s	s	k7c7	s
poznámkami	poznámka	k1gFnPc7	poznámka
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgInSc1d1	jediný
důkaz	důkaz	k1gInSc1	důkaz
jeho	jeho	k3xOp3gNnSc2	jeho
autorství	autorství	k1gNnSc2	autorství
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
někdo	někdo	k3yInSc1	někdo
stihl	stihnout	k5eAaPmAgMnS	stihnout
odnést	odnést	k5eAaPmF	odnést
z	z	k7c2	z
bytu	byt	k1gInSc2	byt
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
prsty	prst	k1gInPc1	prst
parta	parta	k1gFnSc1	parta
Arny	Arne	k1gMnPc4	Arne
Bidmanna	Bidmann	k1gInSc2	Bidmann
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
rival	rival	k1gMnSc1	rival
Stráže	stráž	k1gFnSc2	stráž
Řásnovky	Řásnovka	k1gFnSc2	Řásnovka
<g/>
.	.	kIx.	.
</s>
<s>
Nastává	nastávat	k5eAaImIp3nS	nastávat
dramatický	dramatický	k2eAgInSc4d1	dramatický
závod	závod	k1gInSc4	závod
o	o	k7c4	o
nalezení	nalezení	k1gNnSc4	nalezení
sešitu	sešit	k1gInSc2	sešit
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
doručení	doručení	k1gNnSc4	doručení
do	do	k7c2	do
redakce	redakce	k1gFnSc2	redakce
novin	novina	k1gFnPc2	novina
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
bude	být	k5eAaImBp3nS	být
autorství	autorství	k1gNnSc1	autorství
definitivně	definitivně	k6eAd1	definitivně
přiznáno	přiznat	k5eAaPmNgNnS	přiznat
Hřivnasovi	Hřivnasův	k2eAgMnPc5d1	Hřivnasův
<g/>
.	.	kIx.	.
</s>
<s>
Jindrovi	Jindrův	k2eAgMnPc1d1	Jindrův
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
velmi	velmi	k6eAd1	velmi
pomůže	pomoct	k5eAaPmIp3nS	pomoct
Ríšova	Ríšův	k2eAgFnSc1d1	Ríšova
sestra	sestra	k1gFnSc1	sestra
Blanka	Blanka	k1gFnSc1	Blanka
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
dívčí	dívčí	k2eAgInSc1d1	dívčí
klub	klub	k1gInSc1	klub
Pomněnky	pomněnka	k1gFnSc2	pomněnka
a	a	k8xC	a
věc	věc	k1gFnSc1	věc
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Hřivnas	Hřivnas	k1gInSc1	Hřivnas
je	být	k5eAaImIp3nS	být
znemožněn	znemožnit	k5eAaPmNgInS	znemožnit
a	a	k8xC	a
Bidmannova	Bidmannův	k2eAgFnSc1d1	Bidmannův
parta	parta	k1gFnSc1	parta
také	také	k9	také
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Stráži	stráž	k1gFnSc3	stráž
Řásnovky	Řásnovka	k1gFnSc2	Řásnovka
se	se	k3xPyFc4	se
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
dostane	dostat	k5eAaPmIp3nS	dostat
velké	velký	k2eAgFnPc4d1	velká
pochvaly	pochvala	k1gFnPc4	pochvala
<g/>
.	.	kIx.	.
</s>
<s>
Menčík	Menčík	k1gInSc1	Menčík
obdrží	obdržet	k5eAaPmIp3nS	obdržet
na	na	k7c6	na
honoráři	honorář	k1gInSc6	honorář
značnou	značný	k2eAgFnSc4d1	značná
sumu	suma	k1gFnSc4	suma
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vděčně	vděčně	k6eAd1	vděčně
věnuje	věnovat	k5eAaImIp3nS	věnovat
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
byl	být	k5eAaImAgMnS	být
sám	sám	k3xTgMnSc1	sám
mezitím	mezitím	k6eAd1	mezitím
přijat	přijmout	k5eAaPmNgMnS	přijmout
<g/>
,	,	kIx,	,
na	na	k7c4	na
pořízení	pořízení	k1gNnSc4	pořízení
konečně	konečně	k6eAd1	konečně
pořádné	pořádný	k2eAgFnSc2d1	pořádná
klubovny	klubovna	k1gFnSc2	klubovna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bude	být	k5eAaImBp3nS	být
jen	jen	k6eAd1	jen
jejich	jejich	k3xOp3gNnSc1	jejich
<g/>
.	.	kIx.	.
</s>
</p>
