<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Litovelské	litovelský	k2eAgFnSc2d1
PomoravíIUCN	PomoravíIUCN	k1gFnSc2
kategorie	kategorie	k1gFnSc2
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Mokřadní	mokřadní	k2eAgInPc1d1
biotopy	biotop	k1gInPc1
v	v	k7c6
PR	pr	k0
Plané	Planá	k1gFnPc1
loučkyZákladní	loučkyZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
1990	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
96	#num#	k4
km	km	kA
<g/>
2	#num#	k4
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Olomoucký	olomoucký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Umístění	umístění	k1gNnSc1
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
41	#num#	k4
<g/>
′	′	k?
<g/>
52,5	52,5	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
<g/>
29,5	29,5	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc1
</s>
<s>
84	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.litovelskepomoravi.ochranaprirody.cz	www.litovelskepomoravi.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
CHKO	CHKO	kA
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
severu	sever	k1gInSc6
střední	střední	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
mezi	mezi	k7c7
městy	město	k1gNnPc7
Olomouc	Olomouc	k1gFnSc1
a	a	k8xC
Mohelnice	Mohelnice	k1gFnSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
přímo	přímo	k6eAd1
prochází	procházet	k5eAaImIp3nS
městem	město	k1gNnSc7
Litovel	Litovel	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Litovli	Litovel	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhlášena	vyhlášen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1990	#num#	k4
zejména	zejména	k9
z	z	k7c2
důvodu	důvod	k1gInSc2
ochrany	ochrana	k1gFnSc2
přirozeného	přirozený	k2eAgInSc2d1
meandrujícího	meandrující	k2eAgInSc2d1
toku	tok	k1gInSc2
řeky	řeka	k1gFnSc2
Moravy	Morava	k1gFnSc2
a	a	k8xC
něj	on	k3xPp3gInSc2
navazujícího	navazující	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
lužních	lužní	k2eAgInPc2d1
lesů	les	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnPc4
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
chráněno	chránit	k5eAaImNgNnS
jako	jako	k8xS,k8xC
ptačí	ptačí	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Satelitní	satelitní	k2eAgInSc1d1
snímek	snímek	k1gInSc1
Litovelského	litovelský	k2eAgNnSc2d1
Pomoraví	Pomoraví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zřetelné	zřetelný	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
lužní	lužní	k2eAgInPc1d1
lesy	les	k1gInPc1
mezi	mezi	k7c7
Olomoucí	Olomouc	k1gFnSc7
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
dole	dole	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
Litovlí	Litovel	k1gFnSc7
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
uprostřed	uprostřed	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
lužní	lužní	k2eAgInPc1d1
lesy	les	k1gInPc1
a	a	k8xC
doubravy	doubrava	k1gFnPc1
severozápadně	severozápadně	k6eAd1
od	od	k7c2
Litovle	Litovel	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
</s>
<s>
Poloha	poloha	k1gFnSc1
a	a	k8xC
rozloha	rozloha	k1gFnSc1
</s>
<s>
CHKO	CHKO	kA
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
severu	sever	k1gInSc6
střední	střední	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
mezi	mezi	k7c7
městy	město	k1gNnPc7
Olomouc	Olomouc	k1gFnSc1
a	a	k8xC
Mohelnice	Mohelnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
jeho	jeho	k3xOp3gNnPc2
území	území	k1gNnPc2
leží	ležet	k5eAaImIp3nS
v	v	k7c6
okrese	okres	k1gInSc6
Olomouc	Olomouc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
SZ	SZ	kA
část	část	k1gFnSc1
CHKO	CHKO	kA
zasahuje	zasahovat	k5eAaImIp3nS
do	do	k7c2
okresu	okres	k1gInSc2
Šumperk	Šumperk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
celková	celkový	k2eAgFnSc1d1
rozloha	rozloha	k1gFnSc1
je	být	k5eAaImIp3nS
9600,86	9600,86	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s>
Geologie	geologie	k1gFnSc1
</s>
<s>
Geologický	geologický	k2eAgInSc1d1
podklad	podklad	k1gInSc1
Doubravy	Doubrava	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nS
spodnokarbonské	spodnokarbonský	k2eAgInPc4d1
droby	drob	k1gInPc4
a	a	k8xC
břidlice	břidlice	k1gFnPc4
<g/>
;	;	kIx,
pouze	pouze	k6eAd1
v	v	k7c6
prostoru	prostor	k1gInSc6
J	J	kA
od	od	k7c2
obce	obec	k1gFnSc2
Králová	Králová	k1gFnSc1
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
fylity	fylit	k1gInPc1
devonského	devonský	k2eAgNnSc2d1
stáří	stáří	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vápence	vápenec	k1gInSc2
devonského	devonský	k2eAgNnSc2d1
stáří	stáří	k1gNnSc2
tvoří	tvořit	k5eAaImIp3nS
vrch	vrch	k1gInSc1
Třesín	Třesína	k1gFnPc2
<g/>
,	,	kIx,
skalní	skalní	k2eAgNnSc1d1
dno	dno	k1gNnSc1
údolí	údolí	k1gNnSc2
Moravy	Morava	k1gFnSc2
mezi	mezi	k7c7
Řimicemi	Řimice	k1gInPc7
<g/>
,	,	kIx,
Novými	nový	k2eAgInPc7d1
Zámky	zámek	k1gInPc7
a	a	k8xC
Mladčí	Mladč	k1gFnSc7
a	a	k8xC
v	v	k7c6
ojedinělých	ojedinělý	k2eAgInPc6d1
výchozech	výchoz	k1gInPc6
vystupují	vystupovat	k5eAaImIp3nP
i	i	k9
podél	podél	k6eAd1
J	J	kA
úpatí	úpatí	k1gNnSc1
hřbetu	hřbet	k1gInSc2
Doubravy	Doubrava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třesínské	Třesínský	k2eAgInPc1d1
vápence	vápenec	k1gInPc1
mají	mít	k5eAaImIp3nP
složitou	složitý	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
a	a	k8xC
je	on	k3xPp3gFnPc4
do	do	k7c2
nich	on	k3xPp3gFnPc2
zavrásněno	zavrásnit	k5eAaPmNgNnS
několik	několik	k4yIc1
pruhů	pruh	k1gInPc2
spodnokarbonských	spodnokarbonský	k2eAgFnPc2d1
břidlic	břidlice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nivu	niva	k1gFnSc4
Moravy	Morava	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nP
kvartérní	kvartérní	k2eAgInPc1d1
sedimenty	sediment	k1gInPc1
<g/>
:	:	kIx,
štěrkopísky	štěrkopísek	k1gInPc1
<g/>
,	,	kIx,
písky	písek	k1gInPc1
a	a	k8xC
fluviální	fluviální	k2eAgFnSc2d1
hlíny	hlína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Geologické	geologický	k2eAgNnSc1d1
členění	členění	k1gNnSc1
</s>
<s>
Neogenní	neogenní	k2eAgInSc1d1
pokyv	pokyv	k1gInSc1
reprezentují	reprezentovat	k5eAaImIp3nP
neogén	neogén	k1gInSc4
Hornomoravského	hornomoravský	k2eAgInSc2d1
úvalu	úval	k1gInSc2
(	(	kIx(
<g/>
území	území	k1gNnPc4
od	od	k7c2
Olomouce	Olomouc	k1gFnSc2
po	po	k7c4
Mladeč	Mladeč	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
neogén	neogén	k1gInSc1
Mohelnické	mohelnický	k2eAgFnSc2d1
brázdy	brázda	k1gFnSc2
(	(	kIx(
<g/>
PR	pr	k0
Moravičanské	Moravičanský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
severně	severně	k6eAd1
od	od	k7c2
Moravičan	Moravičan	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravskoslezský	moravskoslezský	k2eAgInSc4d1
a	a	k8xC
spodní	spodní	k2eAgInSc4d1
devon	devon	k1gInSc4
reprezentují	reprezentovat	k5eAaImIp3nP
spodní	spodní	k2eAgInSc4d1
karbon	karbon	k1gInSc4
Drahanské	Drahanský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
(	(	kIx(
<g/>
lesní	lesní	k2eAgInSc4d1
komplex	komplex	k1gInSc4
Doubrava	Doubrava	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
devon	devon	k1gInSc4
konicko-mladečský	konicko-mladečský	k2eAgInSc4d1
(	(	kIx(
<g/>
Třesín	Třesín	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Geomorfologie	geomorfologie	k1gFnSc1
</s>
<s>
Reliéfem	reliéf	k1gInSc7
je	být	k5eAaImIp3nS
většinou	většinou	k6eAd1
rovina	rovina	k1gFnSc1
s	s	k7c7
výškovou	výškový	k2eAgFnSc7d1
členitostí	členitost	k1gFnSc7
do	do	k7c2
30	#num#	k4
m	m	kA
<g/>
,	,	kIx,
postupně	postupně	k6eAd1
k	k	k7c3
severu	sever	k1gInSc3
plochá	plochý	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
se	s	k7c7
členitostí	členitost	k1gFnSc7
30	#num#	k4
<g/>
–	–	k?
<g/>
75	#num#	k4
m	m	kA
až	až	k6eAd1
členitá	členitý	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
se	s	k7c7
členitostí	členitost	k1gFnSc7
75	#num#	k4
<g/>
–	–	k?
<g/>
100	#num#	k4
m	m	kA
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
Úsovské	Úsovský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
geomorfologického	geomorfologický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
zaujímá	zaujímat	k5eAaImIp3nS
střední	střední	k2eAgFnSc1d1
část	část	k1gFnSc1
Olomoucko-litovelské	olomoucko-litovelský	k2eAgFnSc2d1
sníženiny	sníženina	k1gFnSc2
Hornomoravského	hornomoravský	k2eAgInSc2d1
úvalu	úval	k1gInSc2
<g/>
,	,	kIx,
jižní	jižní	k2eAgFnSc1d1
část	část	k1gFnSc1
Mohelnické	mohelnický	k2eAgFnSc2d1
brázdy	brázda	k1gFnSc2
a	a	k8xC
střední	střední	k2eAgFnSc1d1
část	část	k1gFnSc1
Třesínského	Třesínský	k2eAgInSc2d1
prahu	práh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejnižším	nízký	k2eAgInSc7d3
bodem	bod	k1gInSc7
je	být	k5eAaImIp3nS
koryto	koryto	k1gNnSc1
řeky	řeka	k1gFnSc2
Moravy	Morava	k1gFnSc2
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
CHKO	CHKO	kA
(	(	kIx(
<g/>
210	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
a	a	k8xC
nejvyššími	vysoký	k2eAgInPc7d3
body	bod	k1gInPc7
jsou	být	k5eAaImIp3nP
Jelení	jelení	k2eAgFnSc1d1
vrch	vrch	k1gInSc1
(	(	kIx(
<g/>
344,9	344,9	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
a	a	k8xC
Třesín	Třesín	k1gMnSc1
(	(	kIx(
<g/>
344,9	344,9	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
geomorfologických	geomorfologický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Provincie	provincie	k1gFnSc1
<g/>
:	:	kIx,
Západní	západní	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
</s>
<s>
Soustava	soustava	k1gFnSc1
<g/>
:	:	kIx,
Vněkarpatské	Vněkarpatský	k2eAgFnPc1d1
sníženiny	sníženina	k1gFnPc1
</s>
<s>
Podsoustava	Podsoustava	k1gFnSc1
<g/>
:	:	kIx,
Západní	západní	k2eAgFnPc1d1
Vněkarpatské	Vněkarpatský	k2eAgFnPc1d1
sníženiny	sníženina	k1gFnPc1
</s>
<s>
Celek	celek	k1gInSc1
<g/>
:	:	kIx,
Hornomoravský	hornomoravský	k2eAgInSc1d1
úval	úval	k1gInSc1
</s>
<s>
Podcelek	Podcelek	k1gInSc1
<g/>
:	:	kIx,
Středomoravská	středomoravský	k2eAgFnSc1d1
niva	niva	k1gFnSc1
</s>
<s>
Podcelek	Podcelek	k1gInSc1
<g/>
:	:	kIx,
Uničovská	Uničovský	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
</s>
<s>
Okrsek	okrsek	k1gInSc1
<g/>
:	:	kIx,
Červenecká	Červenecký	k2eAgFnSc1d1
rovina	rovina	k1gFnSc1
</s>
<s>
Provincie	provincie	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
vysočina	vysočina	k1gFnSc1
</s>
<s>
Soustava	soustava	k1gFnSc1
<g/>
:	:	kIx,
Krkonošsko-jesenická	krkonošsko-jesenický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Podsoustava	Podsoustava	k1gFnSc1
<g/>
:	:	kIx,
Jesenická	jesenický	k2eAgFnSc1d1
podsoustava	podsoustava	k1gFnSc1
</s>
<s>
Celek	celek	k1gInSc1
<g/>
:	:	kIx,
Mohelnická	mohelnický	k2eAgFnSc1d1
brázda	brázda	k1gFnSc1
</s>
<s>
Celek	celek	k1gInSc1
<g/>
:	:	kIx,
Hanušovická	hanušovický	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
</s>
<s>
Podcelek	Podcelek	k1gInSc1
<g/>
:	:	kIx,
Úsovská	Úsovský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
</s>
<s>
Okrsek	okrsek	k1gInSc1
<g/>
:	:	kIx,
Medlovská	Medlovský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
</s>
<s>
Celek	celek	k1gInSc1
<g/>
:	:	kIx,
Zábřežská	zábřežský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
</s>
<s>
Podcelek	Podcelek	k1gInSc1
<g/>
:	:	kIx,
Bouzovská	Bouzovský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
</s>
<s>
Okrsek	okrsek	k1gInSc1
<g/>
:	:	kIx,
Ludmírovská	Ludmírovský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
</s>
<s>
Klimatické	klimatický	k2eAgInPc1d1
poměry	poměr	k1gInPc1
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
teplou	teplý	k2eAgFnSc4d1
klimatickou	klimatický	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
T	T	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
část	část	k1gFnSc4
Třesína	Třesín	k1gInSc2
patří	patřit	k5eAaImIp3nS
do	do	k7c2
mírně	mírně	k6eAd1
teplé	teplý	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
MT-	MT-	k1gFnSc2
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roční	roční	k2eAgInSc1d1
chod	chod	k1gInSc1
relativní	relativní	k2eAgFnSc2d1
vlhkosti	vlhkost	k1gFnSc2
vzduchu	vzduch	k1gInSc2
Olomouckého	olomoucký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
má	mít	k5eAaImIp3nS
kontinentální	kontinentální	k2eAgInSc4d1
ráz	ráz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrná	průměrný	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
<g/>
:	:	kIx,
8,4	8,4	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
Olomouc	Olomouc	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
celoročním	celoroční	k2eAgInSc6d1
průměru	průměr	k1gInSc6
má	mít	k5eAaImIp3nS
území	území	k1gNnSc1
poměrně	poměrně	k6eAd1
málo	málo	k6eAd1
srážek	srážka	k1gFnPc2
(	(	kIx(
<g/>
600	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
srážkový	srážkový	k2eAgInSc1d1
stín	stín	k1gInSc1
v	v	k7c6
závětří	závětří	k1gNnSc6
Zábřežské	zábřežský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
a	a	k8xC
Úsovské	Úsovský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrný	průměrný	k2eAgInSc1d1
roční	roční	k2eAgInSc1d1
úhrn	úhrn	k1gInSc1
srážek	srážka	k1gFnPc2
<g/>
:	:	kIx,
Olomouc	Olomouc	k1gFnSc1
612	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
Litovel	Litovel	k1gFnSc1
566	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
Mohelnice	Mohelnice	k1gFnSc1
619	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1975-1990	1975-1990	k4
poklesla	poklesnout	k5eAaPmAgFnS
srážková	srážkový	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
v	v	k7c6
lesním	lesní	k2eAgInSc6d1
hospodářském	hospodářský	k2eAgInSc6d1
celku	celek	k1gInSc6
Březová	březový	k2eAgFnSc1d1
o	o	k7c6
12	#num#	k4
%	%	kIx~
z	z	k7c2
612	#num#	k4
mm	mm	kA
na	na	k7c4
544	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s>
Půdy	půda	k1gFnPc1
</s>
<s>
Půdami	půda	k1gFnPc7
jsou	být	k5eAaImIp3nP
glejové	glej	k1gMnPc1
fluvizemě	fluvizemě	k6eAd1
v	v	k7c6
širokém	široký	k2eAgInSc6d1
pásu	pás	k1gInSc6
táhnoucím	táhnoucí	k2eAgInSc6d1
se	se	k3xPyFc4
podél	podél	k7c2
řeky	řeka	k1gFnSc2
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modální	modální	k2eAgFnPc4d1
hnědozemě	hnědozem	k1gFnPc4
jsou	být	k5eAaImIp3nP
v	v	k7c6
oblasti	oblast	k1gFnSc6
Střeně	Střena	k1gFnSc6
<g/>
,	,	kIx,
Měníka	Měník	k1gMnSc2
a	a	k8xC
u	u	k7c2
obce	obec	k1gFnSc2
Králová	Králová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
komplexu	komplex	k1gInSc6
Doubravy	Doubrava	k1gFnSc2
převažují	převažovat	k5eAaImIp3nP
mezotrofní	mezotrofní	k2eAgFnPc1d1
až	až	k8xS
eutrofní	eutrofní	k2eAgFnPc1d1
hnědozemní	hnědozemní	k2eAgFnPc1d1
půdy	půda	k1gFnPc1
(	(	kIx(
<g/>
obecně	obecně	k6eAd1
modální	modální	k2eAgFnSc3d1
kambizemě	kambizema	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
Nové	Nové	k2eAgInPc1d1
Zámky	zámek	k1gInPc1
až	až	k8xS
Nový	nový	k2eAgInSc1d1
Dvůr	Dvůr	k1gInSc1
jsou	být	k5eAaImIp3nP
výrazné	výrazný	k2eAgFnPc4d1
stopy	stopa	k1gFnPc4
oglejení	oglejení	k1gNnSc2
či	či	k8xC
pseudoglejení	pseudoglejení	k1gNnSc2
(	(	kIx(
<g/>
obecně	obecně	k6eAd1
modální	modální	k2eAgMnSc1d1
pseudogleje	pseudoglít	k5eAaPmIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
velmi	velmi	k6eAd1
malou	malý	k2eAgFnSc4d1
část	část	k1gFnSc4
území	území	k1gNnSc6
CHKO	CHKO	kA
zasahují	zasahovat	k5eAaImIp3nP
také	také	k9
luvické	luvický	k2eAgFnPc1d1
černozemě	černozem	k1gFnPc1
ze	z	k7c2
spraší	spraš	k1gFnPc2
Z	Z	kA
od	od	k7c2
obce	obec	k1gFnSc2
Pňovice	Pňovice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vápencích	vápenec	k1gInPc6
Třesína	Třesín	k1gInSc2
se	se	k3xPyFc4
tvoří	tvořit	k5eAaImIp3nS
hnědá	hnědý	k2eAgFnSc1d1
rendzina	rendzina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Hydrologie	hydrologie	k1gFnSc1
</s>
<s>
Selská	selský	k2eAgFnSc1d1
hráz	hráz	k1gFnSc1
severně	severně	k6eAd1
od	od	k7c2
obce	obec	k1gFnSc2
Horka	horko	k1gNnSc2
nad	nad	k7c7
Moravou	Morava	k1gFnSc7
umožňuje	umožňovat	k5eAaImIp3nS
rozliv	rozliv	k1gInSc1
povodní	povodeň	k1gFnPc2
v	v	k7c6
lužním	lužní	k2eAgInSc6d1
lese	les	k1gInSc6
a	a	k8xC
chrání	chránit	k5eAaImIp3nS
pole	pole	k1gNnSc1
a	a	k8xC
obce	obec	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
protipovodňová	protipovodňový	k2eAgFnSc1d1
hráz	hráz	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stav	stav	k1gInSc1
těsně	těsně	k6eAd1
po	po	k7c6
stavbě	stavba	k1gFnSc6
<g/>
/	/	kIx~
<g/>
rekonstrukci	rekonstrukce	k1gFnSc6
r.	r.	kA
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Stav	stav	k1gInSc1
lokality	lokalita	k1gFnSc2
před	před	k7c7
výstavbou	výstavba	k1gFnSc7
</s>
<s>
Unikátní	unikátní	k2eAgFnSc1d1
říční	říční	k2eAgFnSc1d1
síť	síť	k1gFnSc1
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
mezi	mezi	k7c4
anastomózní	anastomózní	k2eAgInPc4d1
říční	říční	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lužních	lužní	k2eAgInPc6d1
lesích	les	k1gInPc6
se	se	k3xPyFc4
tok	tok	k1gInSc1
větví	větev	k1gFnPc2
na	na	k7c4
boční	boční	k2eAgNnPc4d1
stálá	stálý	k2eAgNnPc4d1
a	a	k8xC
periodická	periodický	k2eAgNnPc4d1
říční	říční	k2eAgNnPc4d1
ramena	rameno	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
smuhy	smuha	k1gFnPc1
(	(	kIx(
<g/>
hanácky	hanácky	k6eAd1
<g/>
:	:	kIx,
smohe	smohe	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smuhy	Smuh	k1gInPc7
během	během	k7c2
jara	jaro	k1gNnSc2
postupně	postupně	k6eAd1
vysychají	vysychat	k5eAaImIp3nP
a	a	k8xC
mění	měnit	k5eAaImIp3nP
se	se	k3xPyFc4
na	na	k7c6
periodické	periodický	k2eAgFnSc6d1
tůně	tůna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
léta	léto	k1gNnSc2
většinou	většinou	k6eAd1
vysychají	vysychat	k5eAaImIp3nP
úplně	úplně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záplavový	záplavový	k2eAgInSc4d1
(	(	kIx(
<g/>
inundační	inundační	k2eAgInSc4d1
<g/>
)	)	kIx)
režim	režim	k1gInSc4
je	být	k5eAaImIp3nS
dosud	dosud	k6eAd1
nenarušen	narušen	k2eNgMnSc1d1
a	a	k8xC
umožňuje	umožňovat	k5eAaImIp3nS
tak	tak	k6eAd1
přirozené	přirozený	k2eAgInPc1d1
pedogenetické	pedogenetický	k2eAgInPc1d1
fluviální	fluviální	k2eAgInPc1d1
procesy	proces	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
2	#num#	k4
významné	významný	k2eAgInPc1d1
jezy	jez	k1gInPc1
<g/>
:	:	kIx,
řimický	řimický	k2eAgInSc1d1
jez	jez	k1gInSc1
a	a	k8xC
hynkovský	hynkovský	k2eAgInSc1d1
jez	jez	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ochranu	ochrana	k1gFnSc4
sídel	sídlo	k1gNnPc2
zajišťují	zajišťovat	k5eAaImIp3nP
selské	selský	k2eAgFnPc4d1
hráze	hráz	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvartérní	kvartérní	k2eAgInPc1d1
štěrkopísky	štěrkopísek	k1gInPc1
mají	mít	k5eAaImIp3nP
vysoké	vysoký	k2eAgNnSc4d1
zvodnění	zvodnění	k1gNnSc4
a	a	k8xC
slouží	sloužit	k5eAaImIp3nS
jako	jako	k8xS,k8xC
zdroje	zdroj	k1gInPc4
pitné	pitný	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodárenské	vodárenský	k2eAgNnSc1d1
jímání	jímání	k1gNnSc1
podzemní	podzemní	k2eAgFnSc2d1
vody	voda	k1gFnSc2
ohrožuje	ohrožovat	k5eAaImIp3nS
vodní	vodní	k2eAgInSc4d1
režim	režim	k1gInSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
a	a	k8xC
v	v	k7c6
minulosti	minulost	k1gFnSc6
např.	např.	kA
vedlo	vést	k5eAaImAgNnS
ke	k	k7c3
zničení	zničení	k1gNnSc3
vápnitého	vápnitý	k2eAgNnSc2d1
slatiniště	slatiniště	k1gNnSc2
a	a	k8xC
prameniště	prameniště	k1gNnSc2
Čerlinka	Čerlinka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1
toky	toka	k1gFnPc1
—	—	k?
Morava	Morava	k1gFnSc1
<g/>
,	,	kIx,
Mlýnský	mlýnský	k2eAgInSc1d1
potok	potok	k1gInSc1
/	/	kIx~
Střední	střední	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
/	/	kIx~
Malá	malý	k2eAgFnSc1d1
voda	voda	k1gFnSc1
<g/>
,	,	kIx,
Častava	Častava	k1gFnSc1
<g/>
,	,	kIx,
Cholinka	Cholinka	k1gFnSc1
<g/>
,	,	kIx,
Benkovský	Benkovský	k2eAgInSc1d1
potok	potok	k1gInSc1
<g/>
,	,	kIx,
Kobylník	kobylník	k1gMnSc1
<g/>
,	,	kIx,
Třídvorka	Třídvorka	k1gFnSc1
<g/>
,	,	kIx,
Odrážka	odrážka	k1gFnSc1
(	(	kIx(
<g/>
Bahenka	bahenka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pacovka	Pacovka	k1gFnSc1
<g/>
,	,	kIx,
Čerlinka	Čerlinka	k1gFnSc1
<g/>
,	,	kIx,
Hradečka	Hradečka	k1gFnSc1
<g/>
,	,	kIx,
Doubravka	Doubravka	k1gFnSc1
<g/>
,	,	kIx,
Mírovka	Mírovka	k1gFnSc1
<g/>
,	,	kIx,
Újezdka	Újezdka	k1gFnSc1
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1
plochy	plocha	k1gFnPc1
—	—	k?
Moravičanské	Moravičanský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
,	,	kIx,
Chomoutovské	Chomoutovský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
,	,	kIx,
Podhradní	podhradní	k2eAgInSc1d1
rybník	rybník	k1gInSc1
<g/>
,	,	kIx,
Rozvišť	Rozviště	k1gNnPc2
<g/>
,	,	kIx,
Bázlerova	Bázlerův	k2eAgFnSc1d1
pískovna	pískovna	k1gFnSc1
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1
—	—	k?
Řimické	Řimický	k2eAgFnPc1d1
vyvěračky	vyvěračka	k1gFnPc1
</s>
<s>
Potenciální	potenciální	k2eAgFnSc1d1
přirozená	přirozený	k2eAgFnSc1d1
vegetace	vegetace	k1gFnSc1
</s>
<s>
Bezkolencová	Bezkolencový	k2eAgFnSc1d1
lipová	lipový	k2eAgFnSc1d1
dubohabřina	dubohabřin	k2eAgFnSc1d1
Tilio-Carpinetum	Tilio-Carpinetum	k1gNnSc4
molinietosum	molinietosum	k1gInSc1
v	v	k7c6
PR	pr	k0
U	u	k7c2
spálené	spálený	k2eAgInPc5d1
</s>
<s>
Přirozený	přirozený	k2eAgInSc4d1
lesní	lesní	k2eAgInSc4d1
kryt	kryt	k1gInSc4
tvoří	tvořit	k5eAaImIp3nP
na	na	k7c6
většině	většina	k1gFnSc6
území	území	k1gNnSc2
CHKO	CHKO	kA
různé	různý	k2eAgInPc4d1
typy	typ	k1gInPc4
lužních	lužní	k2eAgInPc2d1
a	a	k8xC
bažinných	bažinný	k2eAgInPc2d1
lesů	les	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
zejména	zejména	k9
jilmové	jilmový	k2eAgFnPc1d1
doubravy	doubrava	k1gFnPc1
společenstva	společenstvo	k1gNnSc2
Ficario-Ulmetum	Ficario-Ulmetum	k1gNnSc1
campestris	campestris	k1gFnSc2
patřící	patřící	k2eAgFnSc2d1
mezi	mezi	k7c4
tvrdé	tvrdý	k2eAgInPc4d1
luhy	luh	k1gInPc4
nížinných	nížinný	k2eAgFnPc2d1
řek	řeka	k1gFnPc2
L	L	kA
<g/>
2.3	2.3	k4
a	a	k8xC
na	na	k7c6
místech	místo	k1gNnPc6
se	s	k7c7
stagnující	stagnující	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
mokřadní	mokřadní	k2eAgFnSc2d1
olšiny	olšina	k1gFnSc2
L1	L1	k1gFnSc2
společenstva	společenstvo	k1gNnSc2
Carici	Carice	k1gFnSc4
elongatae-Alnetum	elongatae-Alnetum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menší	malý	k2eAgFnSc1d2
část	část	k1gFnSc1
pokrývají	pokrývat	k5eAaImIp3nP
dubohabřiny	dubohabřin	k2eAgFnPc4d1
a	a	k8xC
lipové	lipový	k2eAgFnPc4d1
doubravy	doubrava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černýšová	Černýšový	k2eAgFnSc1d1
dubohabřina	dubohabřina	k1gFnSc1
Melampyro	Melampyro	k1gNnSc1
nemorosi-Carpinetum	nemorosi-Carpinetum	k1gNnSc4
v	v	k7c6
okolí	okolí	k1gNnSc6
Střeně	Střena	k1gFnSc3
a	a	k8xC
Jeleního	jelení	k2eAgInSc2d1
kopce	kopec	k1gInSc2
Z	Z	kA
od	od	k7c2
Střelic	Střelice	k1gFnPc2
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
hercynské	hercynský	k2eAgFnPc4d1
dubohabřiny	dubohabřina	k1gFnPc4
L	L	kA
<g/>
3.1	3.1	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lipová	lipový	k2eAgFnSc1d1
dubohabřina	dubohabřina	k1gFnSc1
Tilio-Carpinetum	Tilio-Carpinetum	k1gNnSc1
severozápadněji	severozápadně	k6eAd2
od	od	k7c2
Jeleního	jelení	k2eAgInSc2d1
kopce	kopec	k1gInSc2
přes	přes	k7c4
vrch	vrch	k1gInSc4
Kukačka	kukačka	k1gFnSc1
směrem	směr	k1gInSc7
k	k	k7c3
obci	obec	k1gFnSc3
Stavenice	Stavenice	k1gFnSc2
a	a	k8xC
území	území	k1gNnSc2
mezi	mezi	k7c7
Sobáčovem	Sobáčovo	k1gNnSc7
a	a	k8xC
Řimicemi	Řimice	k1gFnPc7
patří	patřit	k5eAaImIp3nP
mezi	mezi	k7c4
polonské	polonský	k2eAgFnPc4d1
dubohabřiny	dubohabřina	k1gFnPc4
L	L	kA
<g/>
3.2	3.2	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimečně	výjimečně	k6eAd1
na	na	k7c6
jižních	jižní	k2eAgFnPc6d1
expozicích	expozice	k1gFnPc6
doznívají	doznívat	k5eAaImIp3nP
z	z	k7c2
jihu	jih	k1gInSc2
fragmenty	fragment	k1gInPc1
acidofilních	acidofilní	k2eAgFnPc2d1
teplomilných	teplomilný	k2eAgFnPc2d1
doubrav	doubrava	k1gFnPc2
L	L	kA
<g/>
6.5	6.5	k4
společenstva	společenstvo	k1gNnSc2
Sorbo	Sorba	k1gFnSc5
torminalis	torminalis	k1gFnPc6
Quercetum	Quercetum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Současná	současný	k2eAgFnSc1d1
vegetace	vegetace	k1gFnSc1
</s>
<s>
Lužní	lužní	k2eAgInSc1d1
les	les	k1gInSc1
poblíž	poblíž	k6eAd1
Mladče	Mladec	k1gMnSc5
</s>
<s>
Většina	většina	k1gFnSc1
území	území	k1gNnSc2
CHKO	CHKO	kA
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
v	v	k7c6
údolní	údolní	k2eAgFnSc6d1
nivě	niva	k1gFnSc6
řeky	řeka	k1gFnSc2
Moravy	Morava	k1gFnSc2
s	s	k7c7
lužními	lužní	k2eAgInPc7d1
lesy	les	k1gInPc7
<g/>
,	,	kIx,
loukami	louka	k1gFnPc7
<g/>
,	,	kIx,
mokřady	mokřad	k1gInPc7
<g/>
,	,	kIx,
tůněmi	tůně	k1gFnPc7
a	a	k8xC
zatopenými	zatopený	k2eAgFnPc7d1
pískovnami	pískovna	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgInPc7d1
biotopy	biotop	k1gInPc7
na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
jsou	být	k5eAaImIp3nP
teplomilné	teplomilný	k2eAgFnPc1d1
chlumní	chlumní	k2eAgFnPc1d1
doubravy	doubrava	k1gFnPc1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
a	a	k8xC
dubohabrové	dubohabrový	k2eAgInPc1d1
a	a	k8xC
bukové	bukový	k2eAgInPc1d1
lesy	les	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lesích	les	k1gInPc6
převažuje	převažovat	k5eAaImIp3nS
přirozená	přirozený	k2eAgFnSc1d1
druhová	druhový	k2eAgFnSc1d1
skladba	skladba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadregionální	nadregionální	k2eAgInSc1d1
význam	význam	k1gInSc1
má	mít	k5eAaImIp3nS
hlavně	hlavně	k9
nezregulovaný	zregulovaný	k2eNgInSc1d1
přírodní	přírodní	k2eAgInSc1d1
meandrující	meandrující	k2eAgInSc1d1
tok	tok	k1gInSc1
řeky	řeka	k1gFnSc2
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
trvale	trvale	k6eAd1
zamokřených	zamokřený	k2eAgNnPc6d1
místech	místo	k1gNnPc6
rostou	růst	k5eAaImIp3nP
olšové	olšový	k2eAgFnPc1d1
vrbiny	vrbina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
vzácná	vzácný	k2eAgNnPc1d1
společenstva	společenstvo	k1gNnPc1
lze	lze	k6eAd1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
v	v	k7c6
PR	pr	k0
Kačení	kačení	k2eAgFnSc1d1
louka	louka	k1gFnSc1
<g/>
,	,	kIx,
PR	pr	k0
Plané	planý	k2eAgMnPc4d1
loučky	loučka	k1gFnSc2
a	a	k8xC
fragmentálně	fragmentálně	k6eAd1
také	také	k9
v	v	k7c6
PR	pr	k0
Litovelské	litovelský	k2eAgFnPc1d1
luhy	luh	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejcennější	cenný	k2eAgInSc1d3
typ	typ	k1gInSc1
lužních	lužní	k2eAgInPc2d1
lesů	les	k1gInPc2
jsou	být	k5eAaImIp3nP
vrbiny	vrbina	k1gFnPc1
jako	jako	k8xC,k8xS
tzv.	tzv.	kA
měkký	měkký	k2eAgInSc1d1
luh	luh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rostou	růst	k5eAaImIp3nP
na	na	k7c6
náplavech	náplav	k1gInPc6
jako	jako	k9
iniciální	iniciální	k2eAgNnSc4d1
stadium	stadium	k1gNnSc4
zejména	zejména	k9
v	v	k7c6
NPR	NPR	kA
Ramena	rameno	k1gNnPc1
řeky	řeka	k1gFnSc2
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
PR	pr	k0
Litovelské	litovelský	k2eAgInPc4d1
luhy	luh	k1gInPc1
a	a	k8xC
v	v	k7c6
PP	PP	kA
Pod	pod	k7c7
Templem	templ	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
vrbiny	vrbina	k1gFnPc4
na	na	k7c6
pravidelně	pravidelně	k6eAd1
zaplavovaných	zaplavovaný	k2eAgNnPc6d1
místech	místo	k1gNnPc6
navazují	navazovat	k5eAaImIp3nP
topolojilmové	topolojilmový	k2eAgFnPc1d1
jaseniny	jasenina	k1gFnPc1
a	a	k8xC
dále	daleko	k6eAd2
od	od	k7c2
řeky	řeka	k1gFnSc2
dubové	dubový	k2eAgFnSc2d1
jaseniny	jasenina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
jsou	být	k5eAaImIp3nP
zaplavována	zaplavovat	k5eAaImNgNnP
jen	jen	k6eAd1
výjimečně	výjimečně	k6eAd1
<g/>
,	,	kIx,
rostou	růst	k5eAaImIp3nP
habrojilmové	habrojilmový	k2eAgFnPc4d1
jaseniny	jasenina	k1gFnPc4
jako	jako	k8xS,k8xC
tzv.	tzv.	kA
tvrdý	tvrdý	k2eAgInSc4d1
luh	luh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdý	tvrdý	k2eAgInSc1d1
luh	luh	k1gInSc1
je	být	k5eAaImIp3nS
chráněn	chránit	k5eAaImNgInS
v	v	k7c6
těchto	tento	k3xDgFnPc6
rezervacích	rezervace	k1gFnPc6
<g/>
:	:	kIx,
NPR	NPR	kA
Vrapač	Vrapač	k1gInSc1
<g/>
,	,	kIx,
PR	pr	k0
Hejtmanka	hejtmanka	k1gFnSc1
<g/>
,	,	kIx,
PR	pr	k0
Litovelské	litovelský	k2eAgInPc4d1
luhy	luh	k1gInPc7
<g/>
,	,	kIx,
PR	pr	k0
Kenický	Kenický	k2eAgInSc1d1
<g/>
,	,	kIx,
PR	pr	k0
Panenský	panenský	k2eAgInSc4d1
les	les	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Dalšími	další	k2eAgInPc7d1
cennými	cenný	k2eAgInPc7d1
rostlinnými	rostlinný	k2eAgInPc7d1
společenstvy	společenstvo	k1gNnPc7
jsou	být	k5eAaImIp3nP
vlhké	vlhký	k2eAgFnPc4d1
aluviální	aluviální	k2eAgFnPc4d1
louky	louka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nich	on	k3xPp3gMnPc2
je	být	k5eAaImIp3nS
nejvýznamnější	významný	k2eAgMnPc4d3
PR	pr	k0
Plané	planý	k2eAgMnPc4d1
loučky	loučka	k1gFnSc2
a	a	k8xC
dále	daleko	k6eAd2
PP	PP	kA
Daliboř	Daliboř	k1gFnSc4
<g/>
,	,	kIx,
PP	PP	kA
Hvězda	Hvězda	k1gMnSc1
<g/>
,	,	kIx,
PP	PP	kA
Za	za	k7c7
mlýnem	mlýn	k1gInSc7
a	a	k8xC
PR	pr	k0
Novozámecké	Novozámecká	k1gFnPc4
louky	louka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PR	pr	k0
Kačení	kačení	k2eAgFnSc1d1
louka	louka	k1gFnSc1
je	být	k5eAaImIp3nS
významná	významný	k2eAgFnSc1d1
společenstvy	společenstvo	k1gNnPc7
ostřic	ostřice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
přibližně	přibližně	k6eAd1
od	od	k7c2
250	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
nastupují	nastupovat	k5eAaImIp3nP
dubohabrové	dubohabrový	k2eAgInPc4d1
lesy	les	k1gInPc4
cenné	cenný	k2eAgInPc4d1
zejména	zejména	k9
na	na	k7c6
exponovaných	exponovaný	k2eAgInPc6d1
svazích	svah	k1gInPc6
s	s	k7c7
jižní	jižní	k2eAgFnSc7d1
a	a	k8xC
jihovýchodní	jihovýchodní	k2eAgFnSc7d1
expozicí	expozice	k1gFnSc7
(	(	kIx(
<g/>
NPP	NPP	kA
Třesín	Třesín	k1gInSc1
<g/>
,	,	kIx,
PR	pr	k0
Doubrava	Doubrava	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starý	starý	k2eAgInSc1d1
bukový	bukový	k2eAgInSc1d1
les	les	k1gInSc1
je	být	k5eAaImIp3nS
chráněn	chránit	k5eAaImNgInS
v	v	k7c6
PR	pr	k0
Bradlec	Bradlec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
zatopených	zatopený	k2eAgFnPc6d1
pískovnách	pískovna	k1gFnPc6
(	(	kIx(
<g/>
PP	PP	kA
Bázlerova	Bázlerův	k2eAgFnSc1d1
pískovna	pískovna	k1gFnSc1
<g/>
,	,	kIx,
jezero	jezero	k1gNnSc1
Poděbrady	Poděbrady	k1gInPc1
<g/>
,	,	kIx,
PP	PP	kA
Chomoutovské	Chomoutovský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
,	,	kIx,
PR	pr	k0
Moravičanské	Moravičanský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
)	)	kIx)
rostou	růst	k5eAaImIp3nP
nejrůznější	různý	k2eAgNnPc4d3
společenstva	společenstvo	k1gNnPc4
vodních	vodní	k2eAgFnPc2d1
a	a	k8xC
mokřadních	mokřadní	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
v	v	k7c6
různém	různý	k2eAgNnSc6d1
stadiu	stadion	k1gNnSc6
sukcese	sukcese	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
písčitých	písčitý	k2eAgInPc6d1
náplavech	náplav	k1gInPc6
PR	pr	k0
Moravičanské	Moravičanský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
roste	růst	k5eAaImIp3nS
kriticky	kriticky	k6eAd1
ohrožená	ohrožený	k2eAgFnSc1d1
přeslička	přeslička	k1gFnSc1
různobarvá	různobarvý	k2eAgFnSc1d1
Hippochaete	Hippochae	k1gNnSc2
variegata	variegata	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
CHKO	CHKO	kA
rostou	růst	k5eAaImIp3nP
také	také	k9
některé	některý	k3yIgInPc1
nepůvodní	původní	k2eNgInPc1d1
druhy	druh	k1gInPc1
a	a	k8xC
neofyty	neofyt	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místy	místo	k1gNnPc7
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
v	v	k7c6
lužním	lužní	k2eAgInSc6d1
lese	les	k1gInSc6
nepůvodní	původní	k2eNgFnSc2d1
smrkové	smrkový	k2eAgFnSc2d1
monokultury	monokultura	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
vysazovány	vysazovat	k5eAaImNgInP
jako	jako	k8xS,k8xC
úkryty	úkryt	k1gInPc1
pro	pro	k7c4
bažanty	bažant	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsadby	výsadba	k1gFnPc4
hybridních	hybridní	k2eAgInPc2d1
topolů	topol	k1gInPc2
a	a	k8xC
šíření	šíření	k1gNnSc3
bolševníku	bolševník	k1gInSc2
velkolepého	velkolepý	k2eAgNnSc2d1
(	(	kIx(
<g/>
Heracleum	Heracleum	k1gNnSc1
mantegazzianum	mantegazzianum	k1gNnSc1
<g/>
)	)	kIx)
správa	správa	k1gFnSc1
CHKO	CHKO	kA
zastavila	zastavit	k5eAaPmAgFnS
a	a	k8xC
zlikvidovala	zlikvidovat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
PP	PP	kA
Častava	Častava	k1gFnSc1
a	a	k8xC
PP	PP	kA
Malá	Malá	k1gFnSc1
voda	voda	k1gFnSc1
jsou	být	k5eAaImIp3nP
zaznamenány	zaznamenán	k2eAgInPc1d1
výskytykřídlatky	výskytykřídlatek	k1gInPc1
(	(	kIx(
<g/>
Reynoutria	Reynoutrium	k1gNnPc1
sp	sp	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Břehové	břehový	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
všech	všecek	k3xTgInPc2
toků	tok	k1gInPc2
zasáhla	zasáhnout	k5eAaPmAgFnS
netýkavka	netýkavka	k1gFnSc1
žláznatá	žláznatý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Impatiens	Impatiens	k1gInSc1
glandulifera	glandulifero	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
místy	místy	k6eAd1
je	být	k5eAaImIp3nS
hojná	hojný	k2eAgFnSc1d1
také	také	k9
netýkavka	netýkavka	k1gFnSc1
malokvětá	malokvětý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Impatiens	Impatiens	k1gInSc1
parviflora	parviflor	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vrbovka	vrbovka	k1gFnSc1
žláznatá	žláznatý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Epilobium	Epilobium	k1gNnSc1
ciliatum	ciliatum	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
slunečnice	slunečnice	k1gFnSc1
topinambur	topinambur	k1gInSc1
(	(	kIx(
<g/>
Helianthus	Helianthus	k1gMnSc1
tuberosus	tuberosus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šíření	šíření	k1gNnSc1
neofytů	neofyt	k1gInPc2
na	na	k7c6
říčních	říční	k2eAgInPc6d1
náplavech	náplav	k1gInPc6
je	být	k5eAaImIp3nS
správou	správa	k1gFnSc7
CHKO	CHKO	kA
redukováno	redukovat	k5eAaBmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
PR	pr	k0
Chomoutovské	Chomoutovský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
celík	celík	k1gInSc1
kanadský	kanadský	k2eAgInSc1d1
(	(	kIx(
<g/>
Solidago	Solidago	k1gNnSc1
canadensis	canadensis	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
javor	javor	k1gInSc1
jasanolistý	jasanolistý	k2eAgInSc1d1
(	(	kIx(
<g/>
Acer	Acer	k1gInSc1
negundo	gundo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Biogeografie	biogeografie	k1gFnSc1
</s>
<s>
Provincie	provincie	k1gFnSc1
středoevropských	středoevropský	k2eAgInPc2d1
listnatých	listnatý	k2eAgInPc2d1
lesů	les	k1gInPc2
<g/>
,	,	kIx,
podprovincie	podprovincie	k1gFnSc1
hercynská	hercynský	k2eAgFnSc1d1
<g/>
,	,	kIx,
biogeografický	biogeografický	k2eAgInSc1d1
region	region	k1gInSc1
litovelský	litovelský	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Fytogeografické	fytogeografický	k2eAgNnSc1d1
členění	členění	k1gNnSc1
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
rozhraní	rozhraní	k1gNnSc6
termofytika	termofytikum	k1gNnSc2
a	a	k8xC
mezofytika	mezofytikum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fytogeografickou	fytogeografický	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
termofytikum	termofytikum	k1gNnSc1
(	(	kIx(
<g/>
Thermophyticum	Thermophyticum	k1gInSc1
<g/>
)	)	kIx)
zastupuje	zastupovat	k5eAaImIp3nS
území	území	k1gNnSc4
Hornomoravského	hornomoravský	k2eAgInSc2d1
úvalu	úval	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
fytogeografický	fytogeografický	k2eAgInSc4d1
obvod	obvod	k1gInSc4
panonské	panonský	k2eAgNnSc1d1
termofytikum	termofytikum	k1gNnSc1
(	(	kIx(
<g/>
Pannonicum	Pannonicum	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezofytikum	Mezofytikum	k1gNnSc1
(	(	kIx(
<g/>
Mesophyticum	Mesophyticum	k1gInSc1
<g/>
)	)	kIx)
zastupuje	zastupovat	k5eAaImIp3nS
Zábřežsko-uničovský	Zábřežsko-uničovský	k2eAgInSc1d1
úval	úval	k1gInSc1
a	a	k8xC
Bouzovská	Bouzovský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
fytogeografický	fytogeografický	k2eAgInSc4d1
obvod	obvod	k1gInSc4
českomoravské	českomoravský	k2eAgNnSc1d1
mezofytikum	mezofytikum	k1gNnSc1
(	(	kIx(
<g/>
Mesophyticum	Mesophyticum	k1gNnSc1
Massivi	Massiev	k1gFnSc3
bohemici	bohemik	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
fytogeografického	fytogeografický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
jsou	být	k5eAaImIp3nP
významné	významný	k2eAgFnPc1d1
lokality	lokalita	k1gFnPc1
hradisko	hradisko	k1gNnSc4
v	v	k7c4
PR	pr	k0
Doubrava	Doubrava	k1gMnSc1
a	a	k8xC
Třesín	Třesín	k1gMnSc1
s	s	k7c7
výskytem	výskyt	k1gInSc7
náročnějších	náročný	k2eAgInPc2d2
rostlinných	rostlinný	k2eAgInPc2d1
termofytů	termofyt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vegetační	vegetační	k2eAgInPc1d1
stupně	stupeň	k1gInPc1
</s>
<s>
Lesní	lesní	k2eAgFnSc1d1
vegetace	vegetace	k1gFnSc1
odpovídá	odpovídat	k5eAaImIp3nS
1	#num#	k4
<g/>
.	.	kIx.
až	až	k9
4	#num#	k4
<g/>
.	.	kIx.
vegetačnímu	vegetační	k2eAgInSc3d1
stupni	stupeň	k1gInSc3
(	(	kIx(
<g/>
dubový	dubový	k2eAgInSc1d1
<g/>
,	,	kIx,
buko-dubový	buko-dubový	k2eAgInSc1d1
<g/>
,	,	kIx,
dubobukový	dubobukový	k2eAgInSc1d1
<g/>
,	,	kIx,
bukový	bukový	k2eAgInSc1d1
<g/>
)	)	kIx)
ve	v	k7c6
smyslu	smysl	k1gInSc6
geobiocenologickém	geobiocenologický	k2eAgInSc6d1
podle	podle	k7c2
Zlatníka	zlatník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inverzní	inverzní	k2eAgFnPc4d1
rostlinná	rostlinný	k2eAgNnPc4d1
společenstva	společenstvo	k1gNnPc4
4	#num#	k4
<g/>
.	.	kIx.
vegetačního	vegetační	k2eAgInSc2d1
stupně	stupeň	k1gInSc2
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
na	na	k7c6
strmých	strmý	k2eAgFnPc6d1
SV	sv	kA
svazích	svah	k1gInPc6
Třesína	Třesín	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Krajinný	krajinný	k2eAgInSc1d1
pokryv	pokryv	k1gInSc1
</s>
<s>
56	#num#	k4
%	%	kIx~
lesy	les	k1gInPc1
<g/>
,	,	kIx,
zemědělská	zemědělský	k2eAgFnSc1d1
půda	půda	k1gFnSc1
27	#num#	k4
%	%	kIx~
(	(	kIx(
<g/>
louky	louka	k1gFnSc2
9,5	9,5	k4
%	%	kIx~
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgFnSc1d1
zemědělská	zemědělský	k2eAgFnSc1d1
půda	půda	k1gFnSc1
17,5	17,5	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vodní	vodní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
8	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
zastavěné	zastavěný	k2eAgInPc1d1
a	a	k8xC
ostatní	ostatní	k2eAgInPc1d1
pozemky	pozemek	k1gInPc1
9	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podíl	podíl	k1gInSc4
mokřadů	mokřad	k1gInPc2
je	být	k5eAaImIp3nS
43	#num#	k4
%	%	kIx~
(	(	kIx(
<g/>
stálé	stálý	k2eAgFnPc1d1
vodní	vodní	k2eAgFnPc1d1
plochy	plocha	k1gFnPc1
7	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
periodické	periodický	k2eAgFnSc2d1
vodní	vodní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
36	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Status	status	k1gInSc1
území	území	k1gNnSc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
PLA	PLA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhlášena	vyhlášen	k2eAgFnSc1d1
29	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1990	#num#	k4
vyhláškou	vyhláška	k1gFnSc7
MŽP	MŽP	kA
ČR	ČR	kA
č.	č.	k?
464	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozloha	rozloha	k1gFnSc1
<g/>
:	:	kIx,
9600	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
přirozené	přirozený	k2eAgFnSc2d1
akumulace	akumulace	k1gFnSc2
vod	voda	k1gFnPc2
Kvartér	kvartér	k1gInSc1
řeky	řeka	k1gFnSc2
Moravy	Morava	k1gFnSc2
</s>
<s>
Mezinárodně	mezinárodně	k6eAd1
významný	významný	k2eAgInSc1d1
mokřad	mokřad	k1gInSc1
(	(	kIx(
<g/>
Ramsar	Ramsar	k1gInSc1
site	sit	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zapsáno	zapsán	k2eAgNnSc4d1
2	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozloha	rozloha	k1gFnSc1
<g/>
:	:	kIx,
5	#num#	k4
122	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Splňuje	splňovat	k5eAaImIp3nS
kritéria	kritérion	k1gNnPc4
1	#num#	k4
(	(	kIx(
<g/>
mezinárodně	mezinárodně	k6eAd1
významný	významný	k2eAgInSc1d1
<g/>
,	,	kIx,
jedinečný	jedinečný	k2eAgInSc1d1
příklad	příklad	k1gInSc1
v	v	k7c6
biogeografickém	biogeografický	k2eAgInSc6d1
regionu	region	k1gInSc6
<g/>
)	)	kIx)
a	a	k8xC
kritérium	kritérium	k1gNnSc1
3	#num#	k4
(	(	kIx(
<g/>
význam	význam	k1gInSc4
pro	pro	k7c4
biodiverzitu	biodiverzita	k1gFnSc4
rostlin	rostlina	k1gFnPc2
a	a	k8xC
<g/>
/	/	kIx~
<g/>
nebo	nebo	k8xC
živočichů	živočich	k1gMnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1
splňuje	splňovat	k5eAaImIp3nS
i	i	k8xC
kritérium	kritérium	k1gNnSc1
2	#num#	k4
(	(	kIx(
<g/>
podporuje	podporovat	k5eAaImIp3nS
vzácné	vzácný	k2eAgNnSc1d1
<g/>
,	,	kIx,
ohrožené	ohrožený	k2eAgNnSc1d1
a	a	k8xC
kriticky	kriticky	k6eAd1
ohrožené	ohrožený	k2eAgInPc1d1
druhy	druh	k1gInPc1
nebo	nebo	k8xC
ohrožené	ohrožený	k2eAgInPc1d1
ekosystémy	ekosystém	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
ale	ale	k8xC
zatím	zatím	k6eAd1
nebylo	být	k5eNaImAgNnS
oficiálně	oficiálně	k6eAd1
schváleno	schválit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s>
Natura	Natura	k1gFnSc1
2000	#num#	k4
</s>
<s>
Ptačí	ptačí	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
nařízení	nařízení	k1gNnSc2
vlády	vláda	k1gFnSc2
23	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
41	#num#	k4
ptačích	ptačí	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
ochrany	ochrana	k1gFnSc2
ptáků	pták	k1gMnPc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozloha	rozloha	k1gFnSc1
<g/>
:	:	kIx,
9318,57	9318,57	k4
ha	ha	kA
<g/>
,	,	kIx,
kód	kód	k1gInSc4
<g/>
:	:	kIx,
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
711018	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
je	být	k5eAaImIp3nS
navrženo	navrhnout	k5eAaPmNgNnS
jako	jako	k8xC,k8xS
evropsky	evropsky	k6eAd1
významná	významný	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
(	(	kIx(
<g/>
kód	kód	k1gInSc1
<g/>
:	:	kIx,
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
714073	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
je	být	k5eAaImIp3nS
zapsáno	zapsat	k5eAaPmNgNnS
na	na	k7c6
návrhu	návrh	k1gInSc6
národního	národní	k2eAgInSc2d1
seznamu	seznam	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
schválen	schválit	k5eAaPmNgInS
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2004	#num#	k4
a	a	k8xC
poslán	poslat	k5eAaPmNgMnS
do	do	k7c2
Bruselu	Brusel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Biocentra	biocentrum	k1gNnPc4
a	a	k8xC
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
</s>
<s>
vápnomilná	vápnomilný	k2eAgFnSc1d1
bučina	bučina	k1gFnSc1
va	va	k0wR
východní	východní	k2eAgFnSc6d1
části	část	k1gFnSc6
NPP	NPP	kA
Třesín	Třesín	k1gInSc1
</s>
<s>
Nadregionální	nadregionální	k2eAgNnSc1d1
biocentrum	biocentrum	k1gNnSc1
„	„	k?
<g/>
Vrapač	Vrapač	k1gMnSc1
–	–	k?
Doubrava	Doubrava	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
1100	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
</s>
<s>
Nadregionální	nadregionální	k2eAgNnSc4d1
biocentrum	biocentrum	k1gNnSc4
„	„	k?
<g/>
Ramena	rameno	k1gNnSc2
řeky	řeka	k1gFnSc2
Moravy	Morava	k1gFnSc2
<g/>
“	“	k?
nazývané	nazývaný	k2eAgFnPc4d1
také	také	k9
„	„	k?
<g/>
Litovelské	litovelský	k2eAgNnSc4d1
Pomoraví	Pomoraví	k1gNnSc4
–	–	k?
luh	luh	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
1600	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Regionální	regionální	k2eAgNnPc1d1
biocentra	biocentrum	k1gNnPc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Pňovický	Pňovický	k2eAgInSc4d1
luh	luh	k1gInSc4
<g/>
“	“	k?
(	(	kIx(
<g/>
80	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Třesín	Třesín	k1gMnSc1
(	(	kIx(
<g/>
120	#num#	k4
ha	ha	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Celkem	celkem	k6eAd1
25	#num#	k4
maloplošných	maloplošný	k2eAgNnPc2d1
zvláště	zvláště	k6eAd1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
(	(	kIx(
<g/>
ZCHÚ	ZCHÚ	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
2	#num#	k4
NPR	NPR	kA
<g/>
,	,	kIx,
1	#num#	k4
NPP	NPP	kA
<g/>
,	,	kIx,
13	#num#	k4
PR	pr	k0
<g/>
,	,	kIx,
11	#num#	k4
PP	PP	kA
<g/>
.	.	kIx.
</s>
<s>
NPR	NPR	kA
</s>
<s>
NPR	NPR	kA
Ramena	rameno	k1gNnPc1
řeky	řeka	k1gFnSc2
Moravy	Morava	k1gFnSc2
</s>
<s>
NPR	NPR	kA
Vrapač	Vrapač	k1gMnSc1
</s>
<s>
NPP	NPP	kA
</s>
<s>
NPP	NPP	kA
Třesín	Třesín	k1gMnSc1
(	(	kIx(
<g/>
NPP	NPP	kA
143,08	143,08	k4
ha	ha	kA
<g/>
,	,	kIx,
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
-	-	kIx~
má	mít	k5eAaImIp3nS
5,5	5,5	k4
km	km	kA
a	a	k8xC
9	#num#	k4
zastavení	zastavení	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
NPP	NPP	kA
Na	na	k7c6
skále	skála	k1gFnSc6
</s>
<s>
NPP	NPP	kA
Park	park	k1gInSc1
v	v	k7c6
Bílé	bílý	k2eAgFnSc6d1
Lhotě	Lhota	k1gFnSc6
</s>
<s>
PR	pr	k0
</s>
<s>
PR	pr	k0
Doubrava	Doubrava	k1gMnSc1
</s>
<s>
PR	pr	k0
Hejtmanka	hejtmanka	k1gFnSc1
</s>
<s>
PR	pr	k0
Kačení	kačení	k2eAgFnSc1d1
louka	louka	k1gFnSc1
</s>
<s>
PR	pr	k0
Kenický	Kenický	k2eAgMnSc1d1
</s>
<s>
PR	pr	k0
Litovelské	litovelský	k2eAgFnSc2d1
luhy	luh	k1gInPc4
</s>
<s>
PR	pr	k0
Moravičanské	Moravičanský	k2eAgNnSc5d1
jezero	jezero	k1gNnSc1
</s>
<s>
PR	pr	k0
Panenský	panenský	k2eAgInSc1d1
les	les	k1gInSc1
</s>
<s>
PR	pr	k0
Plané	planý	k2eAgMnPc4d1
loučky	loučka	k1gFnPc1
</s>
<s>
PP	PP	kA
</s>
<s>
PP	PP	kA
Chomoutovské	Chomoutovský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
</s>
<s>
PP	PP	kA
Bázlerova	Bázlerův	k2eAgFnSc1d1
pískovna	pískovna	k1gFnSc1
</s>
<s>
PP	PP	kA
Častava	Častava	k1gFnSc1
</s>
<s>
PP	PP	kA
Daliboř	Daliboř	k1gFnSc1
</s>
<s>
PP	PP	kA
Hvězda	Hvězda	k1gMnSc1
</s>
<s>
PP	PP	kA
Kurfürstovo	Kurfürstův	k2eAgNnSc1d1
rameno	rameno	k1gNnSc1
</s>
<s>
PP	PP	kA
Malá	malý	k2eAgFnSc1d1
Voda	voda	k1gFnSc1
</s>
<s>
PP	PP	kA
U	u	k7c2
přejezdu	přejezd	k1gInSc2
</s>
<s>
PP	PP	kA
U	u	k7c2
Senné	senný	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
</s>
<s>
PP	PP	kA
V	v	k7c6
Boukalovém	Boukalový	k2eAgInSc6d1
</s>
<s>
PP	PP	kA
Za	za	k7c7
mlýnem	mlýn	k1gInSc7
</s>
<s>
Antropogenní	antropogenní	k2eAgInPc1d1
vlivy	vliv	k1gInPc1
</s>
<s>
Škodlivé	škodlivý	k2eAgInPc1d1
vlivy	vliv	k1gInPc1
</s>
<s>
Za	za	k7c4
posledních	poslední	k2eAgNnPc2d1
50	#num#	k4
let	léto	k1gNnPc2
nastaly	nastat	k5eAaPmAgInP
velmi	velmi	k6eAd1
výrazné	výrazný	k2eAgFnSc2d1
změny	změna	k1gFnSc2
spíše	spíše	k9
k	k	k7c3
horšímu	horší	k1gNnSc3
<g/>
:	:	kIx,
</s>
<s>
Úpravy	úprava	k1gFnPc1
vodního	vodní	k2eAgInSc2d1
režimu	režim	k1gInSc2
toků	tok	k1gInPc2
<g/>
,	,	kIx,
např.	např.	kA
regulace	regulace	k1gFnSc2
Třídvorky	Třídvorka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vodárenská	vodárenský	k2eAgFnSc1d1
exploatace	exploatace	k1gFnSc1
jímáním	jímání	k1gNnSc7
podzemní	podzemní	k2eAgFnSc2d1
vody	voda	k1gFnSc2
v	v	k7c6
jímacím	jímací	k2eAgNnSc6d1
území	území	k1gNnSc6
Litovel	Litovel	k1gFnSc1
–	–	k?
Čerlinka	Čerlinka	k1gFnSc1
a	a	k8xC
Olomouc	Olomouc	k1gFnSc1
–	–	k?
Černovír	Černovír	k1gInSc4
pravděpodobně	pravděpodobně	k6eAd1
vedla	vést	k5eAaImAgFnS
k	k	k7c3
výraznějšímu	výrazný	k2eAgInSc3d2
poklesu	pokles	k1gInSc3
hladiny	hladina	k1gFnSc2
podzemní	podzemní	k2eAgFnSc2d1
vody	voda	k1gFnSc2
v	v	k7c6
území	území	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porovnání	porovnání	k1gNnSc1
úrovně	úroveň	k1gFnSc2
hladiny	hladina	k1gFnSc2
podzemní	podzemní	k2eAgFnSc2d1
vody	voda	k1gFnSc2
s	s	k7c7
jejím	její	k3xOp3gNnSc7
čerpáním	čerpání	k1gNnSc7
a	a	k8xC
srážkovými	srážkový	k2eAgInPc7d1
úhrny	úhrn	k1gInPc7
však	však	k9
dosud	dosud	k6eAd1
nebylo	být	k5eNaImAgNnS
provedeno	provést	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s>
Výstavba	výstavba	k1gFnSc1
dálnice	dálnice	k1gFnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
Třesínského	Třesínský	k2eAgInSc2d1
prahu	práh	k1gInSc2
zlikvidovala	zlikvidovat	k5eAaPmAgFnS
za	za	k7c2
hlubokého	hluboký	k2eAgInSc2d1
komunismu	komunismus	k1gInSc2
(	(	kIx(
<g/>
r.	r.	kA
1972	#num#	k4
<g/>
)	)	kIx)
z	z	k7c2
evropského	evropský	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
unikátní	unikátní	k2eAgNnSc1d1
sufózní	sufózní	k2eAgInPc1d1
prameny	pramen	k1gInPc1
a	a	k8xC
z	z	k7c2
krajinářského	krajinářský	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
přivodila	přivodit	k5eAaBmAgFnS,k5eAaPmAgFnS
nevratnou	vratný	k2eNgFnSc4d1
destrukci	destrukce	k1gFnSc4
tohoto	tento	k3xDgNnSc2
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavbou	stavba	k1gFnSc7
dálnic	dálnice	k1gFnPc2
jsou	být	k5eAaImIp3nP
však	však	k9
podobným	podobný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
i	i	k9
v	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
ohrožována	ohrožován	k2eAgNnPc1d1
a	a	k8xC
likvidována	likvidován	k2eAgNnPc1d1
jiná	jiné	k1gNnPc1
CHKO	CHKO	kA
<g/>
:	:	kIx,
mokřadní	mokřadní	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
v	v	k7c6
Poodří	Poodří	k1gNnSc6
a	a	k8xC
evropsky	evropsky	k6eAd1
unikátní	unikátní	k2eAgNnSc4d1
sopečné	sopečný	k2eAgNnSc4d1
pohoří	pohoří	k1gNnSc4
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Chov	chov	k1gInSc1
nepůvodního	původní	k2eNgInSc2d1
druhu	druh	k1gInSc2
bažanta	bažant	k1gMnSc2
obecného	obecný	k2eAgMnSc2d1
v	v	k7c6
CHKO	CHKO	kA
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1962-1991	1962-1991	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
provozu	provoz	k1gInSc6
velkobažantnice	velkobažantnice	k1gFnSc2
Střeň	Střeň	k1gFnSc1
–	–	k?
Březová	březový	k2eAgFnSc1d1
a	a	k8xC
dosud	dosud	k6eAd1
je	být	k5eAaImIp3nS
v	v	k7c6
omezené	omezený	k2eAgFnSc6d1
míře	míra	k1gFnSc6
v	v	k7c6
provozu	provoz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkové	celkový	k2eAgInPc1d1
vyhodnocení	vyhodnocení	k1gNnSc4
vlivu	vliv	k1gInSc2
bažantnice	bažantnice	k1gFnSc2
na	na	k7c6
biotu	biot	k1gInSc6
není	být	k5eNaImIp3nS
provedeno	provést	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s>
Zejména	zejména	k9
v	v	k7c6
minulosti	minulost	k1gFnSc6
byly	být	k5eAaImAgFnP
půdy	půda	k1gFnPc1
silněji	silně	k6eAd2
zatěžovány	zatěžován	k2eAgFnPc1d1
imisemi	imise	k1gFnPc7
a	a	k8xC
úlety	úlet	k1gInPc7
popílku	popílek	k1gInSc2
s	s	k7c7
těžkými	těžký	k2eAgInPc7d1
kovy	kov	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
r.	r.	kA
1970	#num#	k4
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
na	na	k7c6
většině	většina	k1gFnSc6
území	území	k1gNnSc2
Doubravy	Doubrava	k1gFnSc2
systémové	systémový	k2eAgNnSc1d1
virové	virový	k2eAgNnSc1d1
onemocnění	onemocnění	k1gNnSc1
zvané	zvaný	k2eAgNnSc1d1
malolistost	malolistost	k1gFnSc4
lip	lípa	k1gFnPc2
a	a	k8xC
od	od	k7c2
r.	r.	kA
1974	#num#	k4
nabírá	nabírat	k5eAaImIp3nS
již	již	k6eAd1
jejich	jejich	k3xOp3gNnSc4
odumírání	odumírání	k1gNnSc4
kalamitního	kalamitní	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dochází	docházet	k5eAaImIp3nS
k	k	k7c3
nahodilé	nahodilý	k2eAgFnSc3d1
těžbě	těžba	k1gFnSc3
lip	lípa	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnPc2
souší	souš	k1gFnPc2
v	v	k7c6
takové	takový	k3xDgFnSc6
míře	míra	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
porosty	porost	k1gInPc7
značně	značně	k6eAd1
prosvětlují	prosvětlovat	k5eAaImIp3nP
a	a	k8xC
v	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgInSc2
se	se	k3xPyFc4
půda	půda	k1gFnSc1
rychle	rychle	k6eAd1
zabuřeňuje	zabuřeňovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průměru	průměr	k1gInSc6
vznikla	vzniknout	k5eAaPmAgFnS
mezi	mezi	k7c7
léty	léto	k1gNnPc7
1980-1989	1980-1989	k4
každým	každý	k3xTgInSc7
rokem	rok	k1gInSc7
na	na	k7c4
polesí	polesí	k1gNnPc4
Úsov	Úsova	k1gFnPc2
holina	holina	k1gFnSc1
o	o	k7c4
velikosti	velikost	k1gFnPc4
12	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
místech	místo	k1gNnPc6
výskytu	výskyt	k1gInSc2
onemocnění	onemocnění	k1gNnSc2
byla	být	k5eAaImAgFnS
zastavena	zastavit	k5eAaPmNgFnS
výsadba	výsadba	k1gFnSc1
lípy	lípa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
se	se	k3xPyFc4
zastoupení	zastoupení	k1gNnSc1
lip	lípa	k1gFnPc2
na	na	k7c6
bývalém	bývalý	k2eAgNnSc6d1
polesí	polesí	k1gNnSc6
Mladeč	Mladeč	k1gInSc4
a	a	k8xC
Úsov	Úsov	k1gInSc4
v	v	k7c6
letech	léto	k1gNnPc6
1970-1989	1970-1989	k4
snížilo	snížit	k5eAaPmAgNnS
o	o	k7c4
9	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
tzn.	tzn.	kA
o	o	k7c4
287	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
zjištěno	zjistit	k5eAaPmNgNnS
pH	ph	kA
půdy	půda	k1gFnPc4
mezi	mezi	k7c7
3,3	3,3	k4
až	až	k9
3,9	3,9	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
protiopatření	protiopatření	k1gNnSc1
bylo	být	k5eAaImAgNnS
provedeno	proveden	k2eAgNnSc1d1
vápnění	vápnění	k1gNnSc1
500	#num#	k4
ha	ha	kA
lesa	les	k1gInSc2
v	v	k7c6
dávce	dávka	k1gFnSc6
1,1	1,1	k4
t	t	k?
•	•	k?
ha	ha	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
a	a	k8xC
ještě	ještě	k6eAd1
třikrát	třikrát	k6eAd1
v	v	k7c6
dávce	dávka	k1gFnSc6
2	#num#	k4
t	t	k?
•	•	k?
ha	ha	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Živočichové	živočich	k1gMnPc1
</s>
<s>
Bohaté	bohatý	k2eAgFnPc1d1
populace	populace	k1gFnPc1
ryb	ryba	k1gFnPc2
a	a	k8xC
obojživelníků	obojživelník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravidelné	pravidelný	k2eAgFnPc4d1
hnízdění	hnízdění	k1gNnSc3
asi	asi	k9
100	#num#	k4
druhů	druh	k1gInPc2
ptáků	pták	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Příklady	příklad	k1gInPc1
výskytu	výskyt	k1gInSc2
živočichů	živočich	k1gMnPc2
<g/>
:	:	kIx,
</s>
<s>
Obojživelníci	obojživelník	k1gMnPc1
a	a	k8xC
plazi	plaz	k1gMnPc1
<g/>
:	:	kIx,
skokan	skokan	k1gMnSc1
štíhlý	štíhlý	k2eAgMnSc1d1
<g/>
,	,	kIx,
čolek	čolek	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
<g/>
,	,	kIx,
užovka	užovka	k1gFnSc1
obojková	obojkový	k2eAgFnSc1d1
</s>
<s>
Šelmy	šelma	k1gFnPc1
<g/>
:	:	kIx,
vydra	vydra	k1gFnSc1
říční	říční	k2eAgFnSc2d1
</s>
<s>
Hlodavci	hlodavec	k1gMnPc1
<g/>
:	:	kIx,
bobr	bobr	k1gMnSc1
evropský	evropský	k2eAgMnSc1d1
</s>
<s>
Měkkýši	měkkýš	k1gMnPc1
<g/>
:	:	kIx,
svinutec	svinutec	k1gMnSc1
zploštělý	zploštělý	k2eAgMnSc1d1
<g/>
,	,	kIx,
Anisus	Anisus	k1gMnSc1
vorticulus	vorticulus	k1gMnSc1
<g/>
,	,	kIx,
Gyraulus	Gyraulus	k1gMnSc1
rossmaessleri	rossmaessler	k1gFnSc2
<g/>
,	,	kIx,
Pisidium	Pisidium	k1gNnSc4
pseudosphaerium	pseudosphaerium	k1gNnSc1
<g/>
,	,	kIx,
Cochlicopa	Cochlicopa	k1gFnSc1
nitens	nitens	k6eAd1
</s>
<s>
Korýši	korýš	k1gMnPc1
<g/>
:	:	kIx,
rak	rak	k1gMnSc1
říční	říční	k2eAgMnSc1d1
<g/>
,	,	kIx,
listonoh	listonoh	k1gMnSc1
jarní	jarní	k2eAgMnSc1d1
<g/>
,	,	kIx,
žábronožka	žábronožka	k1gFnSc1
sněžní	sněžní	k2eAgFnSc2d1
</s>
<s>
Hmyz	hmyz	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
krascovití	krascovitý	k2eAgMnPc1d1
-	-	kIx~
Lampra	Lampra	k1gMnSc1
mirifica	mirifica	k1gMnSc1
<g/>
,	,	kIx,
Anthaxia	Anthaxia	k1gFnSc1
deaurata	deaurata	k1gFnSc1
</s>
<s>
kovaříkovití	kovaříkovitý	k2eAgMnPc1d1
-	-	kIx~
Selatosomus	Selatosomus	k1gMnSc1
cruciatus	cruciatus	k1gMnSc1
</s>
<s>
tesaříkovití	tesaříkovitý	k2eAgMnPc1d1
-	-	kIx~
Saphanus	Saphanus	k1gMnSc1
piceus	piceus	k1gMnSc1
<g/>
,	,	kIx,
Saperda	Saperda	k1gMnSc1
octopunctata	octopunctat	k1gMnSc2
<g/>
,	,	kIx,
Oplosia	Oplosius	k1gMnSc2
fennica	fennicum	k1gNnSc2
aj.	aj.	kA
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Maňas	maňas	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
Měkkýši	měkkýš	k1gMnPc1
(	(	kIx(
<g/>
Mollusca	Mollusca	k1gMnSc1
<g/>
)	)	kIx)
chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Litovelské	litovelský	k2eAgNnSc4d1
Pomoraví	Pomoraví	k1gNnSc4
<g/>
.	.	kIx.
–	–	k?
Dipl	Dipl	k1gInSc1
<g/>
.	.	kIx.
práce	práce	k1gFnSc1
<g/>
,	,	kIx,
Ms.	Ms.	k1gFnSc1
depon	depon	k1gMnSc1
<g/>
.	.	kIx.
in	in	k?
<g/>
:	:	kIx,
PřF	PřF	k1gFnSc2
UP	UP	kA
<g/>
,	,	kIx,
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
80	#num#	k4
pp	pp	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
66	#num#	k4
pp	pp	k?
<g/>
.	.	kIx.
příloh	příloha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
http://www.litovelskepomoravi.ochranaprirody.cz/	http://www.litovelskepomoravi.ochranaprirody.cz/	k?
-	-	kIx~
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
</s>
<s>
http://www.strednimorava-tourism.cz/cil/chranena-krajinna-oblast-litovelske-pomoravi	http://www.strednimorava-tourism.cz/cil/chranena-krajinna-oblast-litovelske-pomoravit	k5eAaPmRp2nS
-	-	kIx~
turistické	turistický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
na	na	k7c6
oficiálním	oficiální	k2eAgInSc6d1
turistickém	turistický	k2eAgInSc6d1
portále	portál	k1gInSc6
Olomouckého	olomoucký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Klenoty	klenot	k1gInPc1
naší	náš	k3xOp1gFnSc2
krajiny	krajina	k1gFnSc2
<g/>
:	:	kIx,
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
3	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
České	český	k2eAgInPc1d1
ramsarské	ramsarský	k2eAgInPc1d1
mokřady	mokřad	k1gInPc1
(	(	kIx(
<g/>
„	„	k?
<g/>
Ramsar	Ramsar	k1gMnSc1
sites	sites	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
Krkonošská	krkonošský	k2eAgNnPc1d1
rašeliniště	rašeliniště	k1gNnPc1
•	•	k?
Lednické	lednický	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc4d1
Pomoraví	Pomoraví	k1gNnSc4
•	•	k?
Mokřady	mokřad	k1gInPc4
dolního	dolní	k2eAgNnSc2d1
Podyjí	Podyjí	k1gNnSc2
•	•	k?
Mokřady	mokřad	k1gInPc7
Liběchovky	Liběchovka	k1gFnSc2
a	a	k8xC
Pšovky	Pšovka	k1gFnSc2
•	•	k?
Novozámecký	Novozámecký	k2eAgInSc1d1
a	a	k8xC
Břehyňský	Břehyňský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Podzemní	podzemní	k2eAgFnSc1d1
Punkva	Punkva	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Třeboňská	třeboňský	k2eAgNnPc4d1
rašeliniště	rašeliniště	k1gNnSc4
•	•	k?
Třeboňské	třeboňský	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Šumavské	šumavský	k2eAgFnSc2d1
slatě	slať	k1gFnSc2
•	•	k?
Pramenné	pramenný	k2eAgInPc4d1
vývěry	vývěr	k1gInPc4
a	a	k8xC
rašeliniště	rašeliniště	k1gNnSc4
Slavkovského	slavkovský	k2eAgInSc2d1
lesa	les	k1gInSc2
•	•	k?
Horní	horní	k2eAgFnSc1d1
Jizera	Jizera	k1gFnSc1
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Morava	Morava	k1gFnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
|	|	kIx~
Životní	životní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
</s>
