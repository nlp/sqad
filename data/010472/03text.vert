<p>
<s>
Helmut	Helmut	k1gMnSc1	Helmut
Dohnálek	Dohnálek	k1gMnSc1	Dohnálek
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
až	až	k9	až
2009	[number]	k4	2009
předseda	předseda	k1gMnSc1	předseda
SNK-ED	SNK-ED	k1gMnSc1	SNK-ED
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
zastupitel	zastupitel	k1gMnSc1	zastupitel
Královéhradeckého	královéhradecký	k2eAgInSc2d1	královéhradecký
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
starosta	starosta	k1gMnSc1	starosta
a	a	k8xC	a
zastupitel	zastupitel	k1gMnSc1	zastupitel
obce	obec	k1gFnSc2	obec
Orlické	orlický	k2eAgNnSc1d1	Orlické
Záhoří	Záhoří	k1gNnSc1	Záhoří
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
SNK-ED	SNK-ED	k1gMnSc1	SNK-ED
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
soukromě	soukromě	k6eAd1	soukromě
podniká	podnikat	k5eAaImIp3nS	podnikat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jako	jako	k8xS	jako
jednatel	jednatel	k1gMnSc1	jednatel
a	a	k8xC	a
společník	společník	k1gMnSc1	společník
firmy	firma	k1gFnSc2	firma
KRONTOUR	KRONTOUR	kA	KRONTOUR
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
či	či	k8xC	či
jednatel	jednatel	k1gMnSc1	jednatel
společnosti	společnost	k1gFnSc2	společnost
S.	S.	kA	S.
<g/>
O.	O.	kA	O.
<g/>
M.	M.	kA	M.
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
až	až	k9	až
2001	[number]	k4	2001
působil	působit	k5eAaImAgInS	působit
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
vedoucího	vedoucí	k1gMnSc2	vedoucí
Odboru	odbor	k1gInSc2	odbor
regionálního	regionální	k2eAgInSc2d1	regionální
rozvoje	rozvoj	k1gInSc2	rozvoj
Okresního	okresní	k2eAgInSc2d1	okresní
úřadu	úřad	k1gInSc2	úřad
v	v	k7c6	v
Rychnově	Rychnov	k1gInSc6	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
sekretář	sekretář	k1gMnSc1	sekretář
Euroregionu	euroregion	k1gInSc2	euroregion
Glacensis	Glacensis	k1gFnSc2	Glacensis
a	a	k8xC	a
ředitel	ředitel	k1gMnSc1	ředitel
regionální	regionální	k2eAgFnSc2d1	regionální
rozvojové	rozvojový	k2eAgFnSc2d1	rozvojová
agentury	agentura	k1gFnSc2	agentura
RDA	RDA	kA	RDA
(	(	kIx(	(
<g/>
Regional	Regional	k1gMnSc2	Regional
Development	Development	k1gMnSc1	Development
Agency	Agenca	k1gMnSc2	Agenca
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
členem	člen	k1gMnSc7	člen
Komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
reformu	reforma	k1gFnSc4	reforma
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
působila	působit	k5eAaImAgFnS	působit
při	při	k7c6	při
Ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
pro	pro	k7c4	pro
místní	místní	k2eAgInSc4d1	místní
rozvoj	rozvoj	k1gInSc4	rozvoj
ČR	ČR	kA	ČR
nebo	nebo	k8xC	nebo
členem	člen	k1gMnSc7	člen
Výboru	výbor	k1gInSc2	výbor
regionální	regionální	k2eAgFnSc2d1	regionální
rady	rada	k1gFnSc2	rada
NUTS	NUTS	kA	NUTS
2	[number]	k4	2
Severovýchod	severovýchod	k1gInSc4	severovýchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Helmut	Helmut	k1gMnSc1	Helmut
Dohnálek	Dohnálek	k1gMnSc1	Dohnálek
je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
jednoho	jeden	k4xCgMnSc4	jeden
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politické	politický	k2eAgNnSc1d1	politické
působení	působení	k1gNnSc1	působení
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
v	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
zastupitelem	zastupitel	k1gMnSc7	zastupitel
obce	obec	k1gFnSc2	obec
Orlické	orlický	k2eAgNnSc4d1	Orlické
Záhoří	Záhoří	k1gNnSc4	Záhoří
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
starostou	starosta	k1gMnSc7	starosta
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
zastupitele	zastupitel	k1gMnSc2	zastupitel
(	(	kIx(	(
<g/>
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
post	post	k1gInSc4	post
starosty	starosta	k1gMnSc2	starosta
<g/>
)	)	kIx)	)
obhájil	obhájit	k5eAaPmAgMnS	obhájit
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
ODS	ODS	kA	ODS
v	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
dalších	další	k2eAgFnPc6d1	další
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
už	už	k9	už
nekandidoval	kandidovat	k5eNaImAgInS	kandidovat
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vedl	vést	k5eAaImAgMnS	vést
kandidátku	kandidátka	k1gFnSc4	kandidátka
"	"	kIx"	"
<g/>
SNK	SNK	kA	SNK
Evropští	evropský	k2eAgMnPc1d1	evropský
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
"	"	kIx"	"
v	v	k7c6	v
Rychnově	Rychnov	k1gInSc6	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
ale	ale	k9	ale
získala	získat	k5eAaPmAgFnS	získat
jen	jen	k9	jen
2,60	[number]	k4	2,60
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
do	do	k7c2	do
městského	městský	k2eAgNnSc2d1	Městské
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
se	se	k3xPyFc4	se
nedostala	dostat	k5eNaPmAgFnS	dostat
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
vyšší	vysoký	k2eAgFnSc2d2	vyšší
politiky	politika	k1gFnSc2	politika
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
úspěšně	úspěšně	k6eAd1	úspěšně
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
jakožto	jakožto	k8xS	jakožto
člen	člen	k1gMnSc1	člen
SNK	SNK	kA	SNK
za	za	k7c2	za
"	"	kIx"	"
<g/>
SNK	SNK	kA	SNK
sdružení	sdružení	k1gNnSc1	sdružení
nezávislých	závislý	k2eNgInPc2d1	nezávislý
<g/>
"	"	kIx"	"
do	do	k7c2	do
Zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
Královéhradeckého	královéhradecký	k2eAgInSc2d1	královéhradecký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc4	mandát
zastupitele	zastupitel	k1gMnSc2	zastupitel
kraje	kraj	k1gInSc2	kraj
obhájil	obhájit	k5eAaPmAgInS	obhájit
i	i	k9	i
v	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
jakožto	jakožto	k8xS	jakožto
člen	člen	k1gMnSc1	člen
SNK-ED	SNK-ED	k1gMnSc1	SNK-ED
na	na	k7c6	na
kandidátce	kandidátka	k1gFnSc6	kandidátka
"	"	kIx"	"
<g/>
SNK	SNK	kA	SNK
Evropští	evropský	k2eAgMnPc1d1	evropský
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
lídrem	lídr	k1gMnSc7	lídr
daných	daný	k2eAgNnPc2d1	dané
uskupení	uskupení	k1gNnPc2	uskupení
a	a	k8xC	a
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c4	na
hejtmana	hejtman	k1gMnSc4	hejtman
Královéhradeckého	královéhradecký	k2eAgInSc2d1	královéhradecký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
volebním	volební	k2eAgInPc3d1	volební
ziskům	zisk	k1gInPc3	zisk
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
6,09	[number]	k4	6,09
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
pak	pak	k6eAd1	pak
6,02	[number]	k4	6,02
%	%	kIx~	%
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
stal	stát	k5eAaPmAgInS	stát
náměstkem	náměstek	k1gMnSc7	náměstek
hejtmana	hejtman	k1gMnSc2	hejtman
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
jako	jako	k9	jako
lídr	lídr	k1gMnSc1	lídr
subjektu	subjekt	k1gInSc2	subjekt
"	"	kIx"	"
<g/>
SNK	SNK	kA	SNK
Evropští	evropský	k2eAgMnPc1d1	evropský
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
2,23	[number]	k4	2,23
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
do	do	k7c2	do
Zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
Královéhradeckého	královéhradecký	k2eAgInSc2d1	královéhradecký
kraje	kraj	k1gInSc2	kraj
vůbec	vůbec	k9	vůbec
nedostala	dostat	k5eNaPmAgFnS	dostat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
republikovým	republikový	k2eAgInSc7d1	republikový
sněmem	sněm	k1gInSc7	sněm
SNK-ED	SNK-ED	k1gMnSc2	SNK-ED
předsedou	předseda	k1gMnSc7	předseda
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
když	když	k8xS	když
porazil	porazit	k5eAaPmAgMnS	porazit
tehdejšího	tehdejší	k2eAgMnSc4d1	tehdejší
senátora	senátor	k1gMnSc4	senátor
Josefa	Josef	k1gMnSc4	Josef
Novotného	Novotný	k1gMnSc4	Novotný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgMnPc6	jenž
SNK-ED	SNK-ED	k1gFnSc6	SNK-ED
neuspěla	uspět	k5eNaPmAgFnS	uspět
(	(	kIx(	(
<g/>
samostatně	samostatně	k6eAd1	samostatně
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
Zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
v	v	k7c6	v
Královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
v	v	k7c6	v
koalici	koalice	k1gFnSc6	koalice
uspěla	uspět	k5eAaPmAgFnS	uspět
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
dvou	dva	k4xCgInPc6	dva
regionech	region	k1gInPc6	region
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
na	na	k7c6	na
nejbližším	blízký	k2eAgInSc6d3	Nejbližší
sněmu	sněm	k1gInSc6	sněm
strany	strana	k1gFnSc2	strana
vzdát	vzdát	k5eAaPmF	vzdát
své	svůj	k3xOyFgFnPc4	svůj
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Zůstal	zůstat	k5eAaPmAgMnS	zůstat
tak	tak	k9	tak
předsedou	předseda	k1gMnSc7	předseda
až	až	k8xS	až
do	do	k7c2	do
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
volební	volební	k2eAgInSc1d1	volební
sněm	sněm	k1gInSc1	sněm
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
o	o	k7c6	o
funkci	funkce	k1gFnSc6	funkce
předsedy	předseda	k1gMnSc2	předseda
SNK-ED	SNK-ED	k1gMnSc2	SNK-ED
už	už	k6eAd1	už
neuchází	ucházet	k5eNaImIp3nS	ucházet
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
na	na	k7c6	na
28	[number]	k4	28
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
kandidátky	kandidátka	k1gFnSc2	kandidátka
SNK-ED	SNK-ED	k1gFnSc2	SNK-ED
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
původně	původně	k6eAd1	původně
figuroval	figurovat	k5eAaImAgMnS	figurovat
jako	jako	k9	jako
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
SPO	SPO	kA	SPO
na	na	k7c6	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
kandidátky	kandidátka	k1gFnSc2	kandidátka
subjektu	subjekt	k1gInSc2	subjekt
"	"	kIx"	"
<g/>
Koalice	koalice	k1gFnSc1	koalice
Svoboda	svoboda	k1gFnSc1	svoboda
a	a	k8xC	a
přímá	přímý	k2eAgFnSc1d1	přímá
demokracie	demokracie	k1gFnSc1	demokracie
-	-	kIx~	-
Tomio	Tomio	k1gNnSc1	Tomio
Okamura	Okamura	k1gFnSc1	Okamura
(	(	kIx(	(
<g/>
SPD	SPD	kA	SPD
<g/>
)	)	kIx)	)
a	a	k8xC	a
Strana	strana	k1gFnSc1	strana
Práv	právo	k1gNnPc2	právo
Občanů	občan	k1gMnPc2	občan
a	a	k8xC	a
SNK	SNK	kA	SNK
<g/>
"	"	kIx"	"
do	do	k7c2	do
Zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
Královéhradeckého	královéhradecký	k2eAgInSc2d1	královéhradecký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
voleb	volba	k1gFnPc2	volba
byl	být	k5eAaImAgMnS	být
však	však	k9	však
z	z	k7c2	z
kandidátní	kandidátní	k2eAgFnSc2d1	kandidátní
listiny	listina	k1gFnSc2	listina
vyškrtnut	vyškrtnout	k5eAaPmNgMnS	vyškrtnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
