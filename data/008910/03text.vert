<p>
<s>
Raoul	Raoout	k5eAaPmAgMnS	Raoout
Gustav	Gustav	k1gMnSc1	Gustav
Wallenberg	Wallenberg	k1gMnSc1	Wallenberg
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1912	[number]	k4	1912
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1947	[number]	k4	1947
<g/>
/	/	kIx~	/
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1952	[number]	k4	1952
<g/>
?	?	kIx.	?
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švédský	švédský	k2eAgMnSc1d1	švédský
diplomat	diplomat	k1gMnSc1	diplomat
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
vlivné	vlivný	k2eAgFnSc2d1	vlivná
rodiny	rodina	k1gFnSc2	rodina
Wallenbergů	Wallenberg	k1gMnPc2	Wallenberg
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
osobním	osobní	k2eAgNnSc7d1	osobní
nasazením	nasazení	k1gNnSc7	nasazení
a	a	k8xC	a
také	také	k9	také
za	za	k7c2	za
velkého	velký	k2eAgNnSc2d1	velké
rizika	riziko	k1gNnSc2	riziko
zachránil	zachránit	k5eAaPmAgInS	zachránit
až	až	k6eAd1	až
100	[number]	k4	100
000	[number]	k4	000
maďarských	maďarský	k2eAgMnPc2d1	maďarský
Židů	Žid	k1gMnPc2	Žid
před	před	k7c7	před
holokaustem	holokaust	k1gInSc7	holokaust
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgInPc1d1	jiný
vydáváním	vydávání	k1gNnSc7	vydávání
švédských	švédský	k2eAgInPc2d1	švédský
ochranných	ochranný	k2eAgInPc2d1	ochranný
pasů	pas	k1gInPc2	pas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Nacisté	nacista	k1gMnPc1	nacista
začali	začít	k5eAaPmAgMnP	začít
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1944	[number]	k4	1944
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Adolfa	Adolf	k1gMnSc2	Adolf
Eichmanna	Eichmann	k1gMnSc4	Eichmann
uskutečňovat	uskutečňovat	k5eAaImF	uskutečňovat
"	"	kIx"	"
<g/>
konečné	konečný	k2eAgNnSc4d1	konečné
řešení	řešení	k1gNnSc4	řešení
<g/>
"	"	kIx"	"
židovské	židovský	k2eAgFnPc1d1	židovská
otázky	otázka	k1gFnPc1	otázka
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Raoul	Raoul	k1gInSc1	Raoul
Wallenberg	Wallenberg	k1gMnSc1	Wallenberg
postrádal	postrádat	k5eAaImAgMnS	postrádat
zkušenosti	zkušenost	k1gFnPc4	zkušenost
z	z	k7c2	z
diplomatické	diplomatický	k2eAgFnSc2d1	diplomatická
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
však	však	k9	však
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
maďarským	maďarský	k2eAgMnSc7d1	maďarský
obchodníkem	obchodník	k1gMnSc7	obchodník
Kalmanem	Kalman	k1gMnSc7	Kalman
Lauerem	Lauer	k1gMnSc7	Lauer
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
několik	několik	k4yIc4	několik
obchodních	obchodní	k2eAgFnPc2d1	obchodní
cest	cesta	k1gFnPc2	cesta
do	do	k7c2	do
Budapešti	Budapešť	k1gFnSc2	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
organizace	organizace	k1gFnSc1	organizace
War	War	k1gFnSc2	War
Refugee	Refugee	k1gFnSc1	Refugee
Board	Board	k1gInSc1	Board
hledala	hledat	k5eAaImAgFnS	hledat
vhodného	vhodný	k2eAgMnSc4d1	vhodný
vyslance	vyslanec	k1gMnSc4	vyslanec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
pokusit	pokusit	k5eAaPmF	pokusit
pomoci	pomoct	k5eAaPmF	pomoct
ohroženým	ohrožený	k2eAgMnPc3d1	ohrožený
Židům	Žid	k1gMnPc3	Žid
<g/>
,	,	kIx,	,
volba	volba	k1gFnSc1	volba
padla	padnout	k5eAaImAgFnS	padnout
právě	právě	k9	právě
na	na	k7c4	na
Wallenberga	Wallenberg	k1gMnSc4	Wallenberg
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gMnSc3	on
udělen	udělit	k5eAaPmNgInS	udělit
status	status	k1gInSc1	status
diplomata	diplomat	k1gMnSc2	diplomat
a	a	k8xC	a
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
na	na	k7c4	na
švédské	švédský	k2eAgNnSc4d1	švédské
velvyslanectví	velvyslanectví	k1gNnSc4	velvyslanectví
do	do	k7c2	do
Budapešti	Budapešť	k1gFnSc2	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
efektivní	efektivní	k2eAgFnSc4d1	efektivní
kancelář	kancelář	k1gFnSc4	kancelář
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hledala	hledat	k5eAaImAgFnS	hledat
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
cesty	cesta	k1gFnPc4	cesta
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
zachránit	zachránit	k5eAaPmF	zachránit
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Wallenberg	Wallenberg	k1gMnSc1	Wallenberg
dokázal	dokázat	k5eAaPmAgMnS	dokázat
najít	najít	k5eAaPmF	najít
odvahu	odvaha	k1gFnSc4	odvaha
vylézt	vylézt	k5eAaPmF	vylézt
na	na	k7c4	na
železniční	železniční	k2eAgInSc4d1	železniční
vagón	vagón	k1gInSc4	vagón
a	a	k8xC	a
rozdávat	rozdávat	k5eAaImF	rozdávat
ochranné	ochranný	k2eAgInPc4d1	ochranný
pasy	pas	k1gInPc4	pas
lidem	člověk	k1gMnPc3	člověk
směřujícím	směřující	k2eAgMnSc7d1	směřující
do	do	k7c2	do
vyhlazovacích	vyhlazovací	k2eAgMnPc2d1	vyhlazovací
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Jednal	jednat	k5eAaImAgMnS	jednat
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
iniciativy	iniciativa	k1gFnSc2	iniciativa
<g/>
,	,	kIx,	,
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemůže	moct	k5eNaImIp3nS	moct
čekat	čekat	k5eAaImF	čekat
na	na	k7c4	na
schválení	schválení	k1gNnSc4	schválení
seshora	seshora	k6eAd1	seshora
<g/>
,	,	kIx,	,
diplomatické	diplomatický	k2eAgFnPc1d1	diplomatická
cesty	cesta	k1gFnPc1	cesta
byly	být	k5eAaImAgFnP	být
neúčinné	účinný	k2eNgFnPc1d1	neúčinná
<g/>
.	.	kIx.	.
</s>
<s>
Wallenberg	Wallenberg	k1gInSc1	Wallenberg
začal	začít	k5eAaPmAgInS	začít
autem	aut	k1gInSc7	aut
stíhat	stíhat	k5eAaImF	stíhat
pochody	pochod	k1gInPc4	pochod
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mířily	mířit	k5eAaImAgInP	mířit
k	k	k7c3	k
rakouským	rakouský	k2eAgFnPc3d1	rakouská
hranicím	hranice	k1gFnPc3	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Rozdával	rozdávat	k5eAaImAgInS	rozdávat
lidem	člověk	k1gMnPc3	člověk
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
,	,	kIx,	,
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
,	,	kIx,	,
léky	lék	k1gInPc1	lék
a	a	k8xC	a
pasy	pas	k1gInPc1	pas
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jejich	jejich	k3xOp3gMnPc3	jejich
majitelům	majitel	k1gMnPc3	majitel
zaručovaly	zaručovat	k5eAaImAgFnP	zaručovat
nedotknutelnost	nedotknutelnost	k1gFnSc1	nedotknutelnost
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1945	[number]	k4	1945
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
o	o	k7c6	o
nacistickém	nacistický	k2eAgInSc6d1	nacistický
plánu	plán	k1gInSc6	plán
na	na	k7c4	na
vyhlazení	vyhlazení	k1gNnSc4	vyhlazení
obyvatel	obyvatel	k1gMnPc2	obyvatel
budapešťského	budapešťský	k2eAgNnSc2d1	budapešťské
ghetta	ghetto	k1gNnSc2	ghetto
<g/>
,	,	kIx,	,
písemně	písemně	k6eAd1	písemně
kontaktoval	kontaktovat	k5eAaImAgMnS	kontaktovat
vrchního	vrchní	k2eAgMnSc4d1	vrchní
velitele	velitel	k1gMnSc4	velitel
wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
a	a	k8xC	a
pohrozil	pohrozit	k5eAaPmAgMnS	pohrozit
mu	on	k3xPp3gMnSc3	on
trestním	trestní	k2eAgNnSc7d1	trestní
stíháním	stíhání	k1gNnSc7	stíhání
za	za	k7c4	za
válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
akce	akce	k1gFnSc1	akce
slavila	slavit	k5eAaImAgFnS	slavit
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
plán	plán	k1gInSc4	plán
na	na	k7c4	na
vyhlazení	vyhlazení	k1gNnSc4	vyhlazení
se	se	k3xPyFc4	se
nerealizoval	realizovat	k5eNaBmAgMnS	realizovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zmizení	zmizení	k1gNnSc2	zmizení
==	==	k?	==
</s>
</p>
<p>
<s>
Válku	válka	k1gFnSc4	válka
bez	bez	k7c2	bez
úhony	úhona	k1gFnSc2	úhona
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
chaotické	chaotický	k2eAgFnSc6d1	chaotická
situaci	situace	k1gFnSc6	situace
během	během	k7c2	během
obsazování	obsazování	k1gNnSc2	obsazování
Budapešti	Budapešť	k1gFnSc2	Budapešť
pokusil	pokusit	k5eAaPmAgMnS	pokusit
kontaktovat	kontaktovat	k5eAaImF	kontaktovat
Rudou	rudý	k2eAgFnSc4d1	rudá
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
měl	mít	k5eAaImAgInS	mít
zajistit	zajistit	k5eAaPmF	zajistit
u	u	k7c2	u
domnělých	domnělý	k2eAgMnPc2d1	domnělý
spojenců	spojenec	k1gMnPc2	spojenec
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
ochranu	ochrana	k1gFnSc4	ochrana
pro	pro	k7c4	pro
ghetto	ghetto	k1gNnSc4	ghetto
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
také	také	k9	také
navrhnout	navrhnout	k5eAaPmF	navrhnout
svůj	svůj	k3xOyFgInSc4	svůj
plán	plán	k1gInSc4	plán
pro	pro	k7c4	pro
poválečnou	poválečný	k2eAgFnSc4d1	poválečná
obnovu	obnova	k1gFnSc4	obnova
Budapešti	Budapešť	k1gFnSc2	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
ho	on	k3xPp3gNnSc4	on
jeho	jeho	k3xOp3gMnPc1	jeho
spolupracovníci	spolupracovník	k1gMnPc1	spolupracovník
viděli	vidět	k5eAaImAgMnP	vidět
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
odvážen	odvážit	k5eAaPmNgInS	odvážit
sovětskými	sovětský	k2eAgFnPc7d1	sovětská
vojáky	voják	k1gMnPc4	voják
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
vojenské	vojenský	k2eAgNnSc1d1	vojenské
velitelství	velitelství	k1gNnSc1	velitelství
v	v	k7c6	v
Debrecínu	Debrecín	k1gInSc6	Debrecín
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
však	však	k9	však
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
jako	jako	k9	jako
možný	možný	k2eAgMnSc1d1	možný
americký	americký	k2eAgMnSc1d1	americký
špión	špión	k1gMnSc1	špión
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gNnSc3	jeho
studiu	studio	k1gNnSc3	studio
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
Michiganu	Michigan	k1gInSc2	Michigan
<g/>
)	)	kIx)	)
odvezen	odvézt	k5eAaPmNgMnS	odvézt
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zřejmě	zřejmě	k6eAd1	zřejmě
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1947	[number]	k4	1947
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc2	jeho
úmrtí	úmrtí	k1gNnSc2	úmrtí
nicméně	nicméně	k8xC	nicméně
nebylo	být	k5eNaImAgNnS	být
nikdy	nikdy	k6eAd1	nikdy
oficiálně	oficiálně	k6eAd1	oficiálně
potvrzeno	potvrzen	k2eAgNnSc1d1	potvrzeno
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
vězni	vězeň	k1gMnPc1	vězeň
propuštění	propuštěný	k2eAgMnPc1d1	propuštěný
ze	z	k7c2	z
sibiřských	sibiřský	k2eAgInPc2d1	sibiřský
gulagů	gulag	k1gInPc2	gulag
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
muže	muž	k1gMnSc4	muž
odpovídajícího	odpovídající	k2eAgInSc2d1	odpovídající
jeho	jeho	k3xOp3gMnPc1	jeho
popisu	popis	k1gInSc3	popis
viděli	vidět	k5eAaImAgMnP	vidět
ještě	ještě	k9	ještě
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
komise	komise	k1gFnSc1	komise
sestavená	sestavený	k2eAgFnSc1d1	sestavená
ke	k	k7c3	k
zjištění	zjištění	k1gNnSc3	zjištění
Wallenbergova	Wallenbergův	k2eAgInSc2d1	Wallenbergův
osudu	osud	k1gInSc2	osud
nedospěla	dochvít	k5eNaPmAgNnP	dochvít
k	k	k7c3	k
žádným	žádný	k3yNgInPc3	žádný
výsledkům	výsledek	k1gInPc3	výsledek
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgFnPc2d1	jiná
informací	informace	k1gFnPc2	informace
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yQnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
uprostřed	uprostřed	k7c2	uprostřed
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
Wallenberg	Wallenberg	k1gMnSc1	Wallenberg
vydal	vydat	k5eAaPmAgMnS	vydat
za	za	k7c7	za
sovětským	sovětský	k2eAgMnSc7d1	sovětský
maršálem	maršál	k1gMnSc7	maršál
Rodionem	Rodion	k1gInSc7	Rodion
Malinovskim	Malinovskima	k1gFnPc2	Malinovskima
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
ledna	leden	k1gInSc2	leden
vydalo	vydat	k5eAaPmAgNnS	vydat
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
prohlášení	prohlášení	k1gNnSc2	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Wallenberg	Wallenberg	k1gInSc1	Wallenberg
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
oficiálně	oficiálně	k6eAd1	oficiálně
nic	nic	k3yNnSc1	nic
neví	vědět	k5eNaImIp3nS	vědět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
rodina	rodina	k1gFnSc1	rodina
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pátrala	pátrat	k5eAaImAgNnP	pátrat
<g/>
,	,	kIx,	,
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
náměstek	náměstka	k1gFnPc2	náměstka
ministra	ministr	k1gMnSc2	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Andrej	Andrej	k1gMnSc1	Andrej
Višinskij	Višinskij	k1gMnSc1	Višinskij
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Wallenberg	Wallenberg	k1gMnSc1	Wallenberg
nenachází	nacházet	k5eNaImIp3nS	nacházet
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
známý	známý	k1gMnSc1	známý
jeho	jeho	k3xOp3gInSc4	jeho
současný	současný	k2eAgInSc4d1	současný
pobyt	pobyt	k1gInSc4	pobyt
<g/>
.	.	kIx.	.
</s>
<s>
Našli	najít	k5eAaPmAgMnP	najít
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
svědci	svědek	k1gMnPc1	svědek
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yQnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Wallenberga	Wallenberga	k1gFnSc1	Wallenberga
viděli	vidět	k5eAaImAgMnP	vidět
i	i	k9	i
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
moskevském	moskevský	k2eAgNnSc6d1	moskevské
vězení	vězení	k1gNnSc6	vězení
či	či	k8xC	či
na	na	k7c6	na
severu	sever	k1gInSc6	sever
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
ve	v	k7c6	v
Vorkutě	Vorkutě	k1gFnSc6	Vorkutě
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byla	být	k5eAaImAgFnS	být
proto	proto	k6eAd1	proto
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
rusko-švédská	rusko-švédský	k2eAgFnSc1d1	rusko-švédská
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ujasnila	ujasnit	k5eAaPmAgFnS	ujasnit
záhadu	záhada	k1gFnSc4	záhada
kolem	kolem	k7c2	kolem
jeho	jeho	k3xOp3gNnPc2	jeho
zmizení	zmizení	k1gNnPc2	zmizení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
vyšetřovacích	vyšetřovací	k2eAgFnPc2d1	vyšetřovací
verzí	verze	k1gFnPc2	verze
komise	komise	k1gFnSc2	komise
jej	on	k3xPp3gMnSc4	on
chtěl	chtít	k5eAaImAgMnS	chtít
Stalin	Stalin	k1gMnSc1	Stalin
vyměnit	vyměnit	k5eAaPmF	vyměnit
za	za	k7c4	za
sovětské	sovětský	k2eAgMnPc4d1	sovětský
špiony	špion	k1gMnPc4	špion
<g/>
,	,	kIx,	,
chycené	chycený	k2eAgMnPc4d1	chycený
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Stockholm	Stockholm	k1gInSc1	Stockholm
ovšem	ovšem	k9	ovšem
na	na	k7c4	na
výměnu	výměna	k1gFnSc4	výměna
nepřistoupil	přistoupit	k5eNaPmAgMnS	přistoupit
<g/>
.	.	kIx.	.
</s>
<s>
Vyvstává	vyvstávat	k5eAaImIp3nS	vyvstávat
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
Wallenberg	Wallenberg	k1gMnSc1	Wallenberg
stal	stát	k5eAaPmAgMnS	stát
"	"	kIx"	"
<g/>
výměnným	výměnný	k2eAgNnSc7d1	výměnné
zbožím	zboží	k1gNnSc7	zboží
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
před	před	k7c7	před
zatčením	zatčení	k1gNnSc7	zatčení
či	či	k8xC	či
po	po	k7c6	po
něm.	něm.	k?	něm.
Švédská	švédský	k2eAgFnSc1d1	švédská
vláda	vláda	k1gFnSc1	vláda
později	pozdě	k6eAd2	pozdě
údajně	údajně	k6eAd1	údajně
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
politování	politování	k1gNnSc4	politování
za	za	k7c4	za
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
neuskutečnit	uskutečnit	k5eNaPmF	uskutečnit
tuto	tento	k3xDgFnSc4	tento
výměnu	výměna	k1gFnSc4	výměna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
členů	člen	k1gInPc2	člen
komise	komise	k1gFnSc2	komise
byl	být	k5eAaImAgInS	být
člen	člen	k1gInSc1	člen
izraelského	izraelský	k2eAgInSc2d1	izraelský
MOSSADu	MOSSADus	k1gInSc2	MOSSADus
Peter	Peter	k1gMnSc1	Peter
Cvi	Cvi	k1gMnSc1	Cvi
Malkin	Malkin	k1gMnSc1	Malkin
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
pracoval	pracovat	k5eAaImAgMnS	pracovat
nezávisle	závisle	k6eNd1	závisle
na	na	k7c4	na
komisi	komise	k1gFnSc4	komise
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
švédského	švédský	k2eAgNnSc2d1	švédské
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
indicie	indicie	k1gFnPc4	indicie
dokazující	dokazující	k2eAgFnPc4d1	dokazující
<g/>
,	,	kIx,	,
že	že	k8xS	že
Wallenberg	Wallenberg	k1gMnSc1	Wallenberg
zemřel	zemřít	k5eAaPmAgMnS	zemřít
později	pozdě	k6eAd2	pozdě
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
nepřímé	přímý	k2eNgFnPc4d1	nepřímá
stopy	stopa	k1gFnPc4	stopa
<g/>
,	,	kIx,	,
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
vězně	vězně	k6eAd1	vězně
Van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Berga	Berg	k1gMnSc4	Berg
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tímto	tento	k3xDgNnSc7	tento
jménem	jméno	k1gNnSc7	jméno
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
veden	veden	k2eAgInSc1d1	veden
Wallenberg	Wallenberg	k1gInSc1	Wallenberg
ve	v	k7c6	v
moskevských	moskevský	k2eAgFnPc6d1	Moskevská
věznicích	věznice	k1gFnPc6	věznice
a	a	k8xC	a
ve	v	k7c6	v
věznici	věznice	k1gFnSc6	věznice
Vorkuta	Vorkut	k1gMnSc2	Vorkut
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Stopu	stopa	k1gFnSc4	stopa
Malkin	Malkin	k1gInSc1	Malkin
ztratil	ztratit	k5eAaPmAgInS	ztratit
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ještě	ještě	k6eAd1	ještě
pozdější	pozdní	k2eAgNnSc4d2	pozdější
úmrtí	úmrtí	k1gNnSc4	úmrtí
pak	pak	k6eAd1	pak
svědčí	svědčit	k5eAaImIp3nS	svědčit
podle	podle	k7c2	podle
předsedy	předseda	k1gMnSc2	předseda
rusko-švédské	rusko-švédský	k2eAgFnSc2d1	rusko-švédská
komise	komise	k1gFnSc2	komise
Hanse	Hans	k1gMnSc2	Hans
Magnunssona	Magnunsson	k1gMnSc2	Magnunsson
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
údajně	údajně	k6eAd1	údajně
Moskva	Moskva	k1gFnSc1	Moskva
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
výměnu	výměna	k1gFnSc4	výměna
Wallenberga	Wallenberga	k1gFnSc1	Wallenberga
za	za	k7c4	za
Stiga	Stig	k1gMnSc4	Stig
Wennerstrőma	Wennerstrőm	k1gMnSc4	Wennerstrőm
<g/>
,	,	kIx,	,
top	topit	k5eAaImRp2nS	topit
špiona	špion	k1gMnSc4	špion
pracujícího	pracující	k2eAgMnSc2d1	pracující
pro	pro	k7c4	pro
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
podle	podle	k7c2	podle
původního	původní	k2eAgNnSc2d1	původní
prohlášení	prohlášení	k1gNnSc2	prohlášení
ruských	ruský	k2eAgInPc2d1	ruský
úřadů	úřad	k1gInPc2	úřad
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Wallenberg	Wallenberg	k1gMnSc1	Wallenberg
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
na	na	k7c4	na
infarkt	infarkt	k1gInSc4	infarkt
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
sovětská	sovětský	k2eAgFnSc1d1	sovětská
strana	strana	k1gFnSc1	strana
jeho	jeho	k3xOp3gFnSc4	jeho
výměnu	výměna	k1gFnSc4	výměna
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Malkina	Malkin	k1gMnSc2	Malkin
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
očití	očitý	k2eAgMnPc1d1	očitý
svědkové	svědek	k1gMnPc1	svědek
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
prohlašují	prohlašovat	k5eAaImIp3nP	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
Wallenbergem	Wallenberg	k1gInSc7	Wallenberg
mluvili	mluvit	k5eAaImAgMnP	mluvit
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
relevanci	relevance	k1gFnSc4	relevance
této	tento	k3xDgFnSc2	tento
informace	informace	k1gFnSc2	informace
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
dostala	dostat	k5eAaPmAgFnS	dostat
jeho	jeho	k3xOp3gFnSc1	jeho
nevlastní	vlastní	k2eNgFnSc1d1	nevlastní
sestra	sestra	k1gFnSc1	sestra
Wallenbergův	Wallenbergův	k2eAgInSc1d1	Wallenbergův
pas	pas	k1gInSc4	pas
<g/>
,	,	kIx,	,
řidičský	řidičský	k2eAgInSc4d1	řidičský
průkaz	průkaz	k1gInSc4	průkaz
<g/>
,	,	kIx,	,
pouzdro	pouzdro	k1gNnSc4	pouzdro
na	na	k7c4	na
cigarety	cigareta	k1gFnPc4	cigareta
a	a	k8xC	a
poznámkový	poznámkový	k2eAgInSc4d1	poznámkový
kalendář	kalendář	k1gInSc4	kalendář
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
švédským	švédský	k2eAgInSc7d1	švédský
daňovým	daňový	k2eAgInSc7d1	daňový
úřadem	úřad	k1gInSc7	úřad
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
úmrtí	úmrtí	k1gNnSc2	úmrtí
bylo	být	k5eAaImAgNnS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
na	na	k7c4	na
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
<g/>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Wallenberg	Wallenberg	k1gInSc1	Wallenberg
zachránil	zachránit	k5eAaPmAgInS	zachránit
asi	asi	k9	asi
100	[number]	k4	100
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
Malkin	Malkin	k1gMnSc1	Malkin
jako	jako	k8xS	jako
člen	člen	k1gMnSc1	člen
tajné	tajný	k2eAgFnSc2d1	tajná
služby	služba	k1gFnSc2	služba
Mossad	Mossad	k1gInSc1	Mossad
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
operaci	operace	k1gFnSc4	operace
únosu	únos	k1gInSc2	únos
Adolfa	Adolf	k1gMnSc2	Adolf
Eichmanna	Eichmann	k1gMnSc2	Eichmann
z	z	k7c2	z
Buenos	Buenosa	k1gFnPc2	Buenosa
Aires	Airesa	k1gFnPc2	Airesa
<g/>
,	,	kIx,	,
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Eichmanna	Eichmanno	k1gNnPc1	Eichmanno
jsou	být	k5eAaImIp3nP	být
plné	plný	k2eAgFnPc4d1	plná
noviny	novina	k1gFnPc4	novina
a	a	k8xC	a
o	o	k7c6	o
Wallenberovi	Wallenber	k1gMnSc6	Wallenber
ani	ani	k9	ani
slovo	slovo	k1gNnSc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Stránky	stránka	k1gFnPc1	stránka
novin	novina	k1gFnPc2	novina
plní	plnit	k5eAaImIp3nP	plnit
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
posílal	posílat	k5eAaImAgInS	posílat
lidi	člověk	k1gMnPc4	člověk
do	do	k7c2	do
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
a	a	k8xC	a
svět	svět	k1gInSc1	svět
zapomíná	zapomínat	k5eAaImIp3nS	zapomínat
na	na	k7c4	na
toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
před	před	k7c7	před
plynem	plyn	k1gInSc7	plyn
zachraňoval	zachraňovat	k5eAaImAgMnS	zachraňovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
let	léto	k1gNnPc2	léto
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
Raoul	Raoul	k1gInSc1	Raoul
Wallenberg	Wallenberg	k1gMnSc1	Wallenberg
oceněn	ocenit	k5eAaPmNgMnS	ocenit
několika	několik	k4yIc7	několik
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
jako	jako	k9	jako
Spravedlivý	spravedlivý	k2eAgMnSc1d1	spravedlivý
mezi	mezi	k7c7	mezi
národy	národ	k1gInPc7	národ
či	či	k8xC	či
čestným	čestný	k2eAgNnSc7d1	čestné
občanstvím	občanství	k1gNnSc7	občanství
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Docela	docela	k6eAd1	docela
podrobně	podrobně	k6eAd1	podrobně
se	s	k7c7	s
případem	případ	k1gInSc7	případ
Raoula	Raoul	k1gMnSc2	Raoul
Wallenberga	Wallenberg	k1gMnSc2	Wallenberg
zabýval	zabývat	k5eAaImAgInS	zabývat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Alana	Alan	k1gMnSc2	Alan
Levyho	Levyho	k?	Levyho
Simon	Simon	k1gMnSc1	Simon
Wiesenthal	Wiesenthal	k1gMnSc1	Wiesenthal
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc7	jeho
případy	případ	k1gInPc7	případ
samotný	samotný	k2eAgMnSc1d1	samotný
Wiesenthal	Wiesenthal	k1gMnSc1	Wiesenthal
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
po	po	k7c6	po
Raoulovi	Raoul	k1gMnSc6	Raoul
pátral	pátrat	k5eAaImAgMnS	pátrat
na	na	k7c4	na
prosbu	prosba	k1gFnSc4	prosba
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
LEVY	LEVY	k?	LEVY
<g/>
,	,	kIx,	,
Alan	Alan	k1gMnSc1	Alan
<g/>
.	.	kIx.	.
</s>
<s>
Simon	Simon	k1gMnSc1	Simon
Wiesenthal	Wiesenthal	k1gMnSc1	Wiesenthal
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc7	jeho
případy	případ	k1gInPc7	případ
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
450	[number]	k4	450
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Raoul	Raoul	k1gInSc1	Raoul
Wallenberg	Wallenberg	k1gInSc1	Wallenberg
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Raoul	Raoul	k1gInSc1	Raoul
Wallenberg	Wallenberg	k1gInSc4	Wallenberg
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
