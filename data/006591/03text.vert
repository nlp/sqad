<s>
Chromozom	chromozom	k1gInSc1	chromozom
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
χ	χ	k?	χ
chroma	chroma	k1gFnSc1	chroma
–	–	k?	–
barva	barva	k1gFnSc1	barva
a	a	k8xC	a
σ	σ	k?	σ
soma	soma	k1gFnSc1	soma
–	–	k?	–
tělo	tělo	k1gNnSc1	tělo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
specifická	specifický	k2eAgFnSc1d1	specifická
barvitelná	barvitelný	k2eAgFnSc1d1	barvitelná
buněčná	buněčný	k2eAgFnSc1d1	buněčná
struktura	struktura	k1gFnSc1	struktura
eukaryot	eukaryota	k1gFnPc2	eukaryota
přítomná	přítomný	k2eAgFnSc1d1	přítomná
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
DNA	dno	k1gNnSc2	dno
a	a	k8xC	a
histonů	histon	k1gInPc2	histon
<g/>
.	.	kIx.	.
</s>
<s>
Účelem	účel	k1gInSc7	účel
jeho	jeho	k3xOp3gFnSc2	jeho
existence	existence	k1gFnSc2	existence
je	být	k5eAaImIp3nS	být
usnadnit	usnadnit	k5eAaPmF	usnadnit
rovnoměrné	rovnoměrný	k2eAgNnSc1d1	rovnoměrné
rozdělení	rozdělení	k1gNnSc1	rozdělení
genetické	genetický	k2eAgNnSc1d1	genetické
informace	informace	k1gFnPc4	informace
do	do	k7c2	do
dceřiných	dceřin	k2eAgFnPc2d1	dceřina
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
všech	všecek	k3xTgInPc2	všecek
chromozomů	chromozom	k1gInPc2	chromozom
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
karyotyp	karyotyp	k1gInSc1	karyotyp
<g/>
.	.	kIx.	.
</s>
<s>
Chromozomy	chromozom	k1gInPc1	chromozom
jsou	být	k5eAaImIp3nP	být
pozorovatelné	pozorovatelný	k2eAgMnPc4d1	pozorovatelný
světelným	světelný	k2eAgInSc7d1	světelný
mikroskopem	mikroskop	k1gInSc7	mikroskop
především	především	k9	především
při	při	k7c6	při
buněčném	buněčný	k2eAgNnSc6d1	buněčné
dělení	dělení	k1gNnSc6	dělení
<g/>
.	.	kIx.	.
</s>
<s>
Chromozomy	chromozom	k1gInPc4	chromozom
poprvé	poprvé	k6eAd1	poprvé
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
a	a	k8xC	a
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
biolog	biolog	k1gMnSc1	biolog
Karl	Karl	k1gMnSc1	Karl
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
von	von	k1gInSc1	von
Nägeli	Nägel	k1gInSc6	Nägel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
detailní	detailní	k2eAgInSc1d1	detailní
popis	popis	k1gInSc1	popis
chování	chování	k1gNnSc2	chování
chromozomů	chromozom	k1gInPc2	chromozom
při	při	k7c6	při
jaderném	jaderný	k2eAgInSc6d1	jaderný
a	a	k8xC	a
buněčném	buněčný	k2eAgNnSc6d1	buněčné
dělení	dělení	k1gNnSc6	dělení
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
německého	německý	k2eAgMnSc2d1	německý
lékaře	lékař	k1gMnSc2	lékař
Walthera	Walther	k1gMnSc2	Walther
Flemminga	Flemming	k1gMnSc2	Flemming
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k8xS	že
chromozomy	chromozom	k1gInPc1	chromozom
nesou	nést	k5eAaImIp3nP	nést
genetickou	genetický	k2eAgFnSc4d1	genetická
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
dokázal	dokázat	k5eAaPmAgInS	dokázat
svými	svůj	k3xOyFgInPc7	svůj
pokusy	pokus	k1gInPc7	pokus
souvisejícími	související	k2eAgInPc7d1	související
s	s	k7c7	s
vazbou	vazba	k1gFnSc7	vazba
genů	gen	k1gInPc2	gen
u	u	k7c2	u
octomilky	octomilka	k1gFnSc2	octomilka
americký	americký	k2eAgMnSc1d1	americký
genetik	genetik	k1gMnSc1	genetik
Thomas	Thomas	k1gMnSc1	Thomas
Morgan	morgan	k1gMnSc1	morgan
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chromozom	chromozom	k1gInSc1	chromozom
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
histonových	histonový	k2eAgFnPc2d1	histonový
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
jakousi	jakýsi	k3yIgFnSc4	jakýsi
kostru	kostra	k1gFnSc4	kostra
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
namotává	namotávat	k5eAaImIp3nS	namotávat
molekula	molekula	k1gFnSc1	molekula
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
1,6	[number]	k4	1,6
<g/>
–	–	k?	–
<g/>
8,2	[number]	k4	8,2
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
dalších	další	k2eAgInPc6d1	další
úkolech	úkol	k1gInPc6	úkol
(	(	kIx(	(
<g/>
replikace	replikace	k1gFnPc1	replikace
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
ochrana	ochrana	k1gFnSc1	ochrana
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
regulace	regulace	k1gFnSc2	regulace
replikace	replikace	k1gFnSc2	replikace
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
komplex	komplex	k1gInSc1	komplex
DNA	dno	k1gNnSc2	dno
a	a	k8xC	a
bílkovin	bílkovina	k1gFnPc2	bílkovina
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
chromatin	chromatin	k1gInSc1	chromatin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
chromozomu	chromozom	k1gInSc2	chromozom
se	s	k7c7	s
strukturní	strukturní	k2eAgFnSc7d1	strukturní
funkcí	funkce	k1gFnSc7	funkce
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
chromatinu	chromatin	k1gInSc2	chromatin
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc1	několik
úrovní	úroveň	k1gFnPc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
nukleozom	nukleozom	k1gInSc1	nukleozom
<g/>
,	,	kIx,	,
struktura	struktura	k1gFnSc1	struktura
tvořená	tvořený	k2eAgFnSc1d1	tvořená
histonovými	histonový	k2eAgFnPc7d1	histonový
molekulami	molekula	k1gFnPc7	molekula
omotanými	omotaný	k2eAgFnPc7d1	omotaná
vláknem	vlákno	k1gNnSc7	vlákno
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
asi	asi	k9	asi
80	[number]	k4	80
párů	pár	k1gInPc2	pár
bází	báze	k1gFnPc2	báze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc7d2	vyšší
strukturou	struktura	k1gFnSc7	struktura
je	být	k5eAaImIp3nS	být
solenoid	solenoid	k1gInSc1	solenoid
<g/>
,	,	kIx,	,
spiralizované	spiralizovaný	k2eAgNnSc1d1	spiralizovaný
uspořádání	uspořádání	k1gNnSc1	uspořádání
nukleozomů	nukleozom	k1gInPc2	nukleozom
(	(	kIx(	(
<g/>
1	[number]	k4	1
závit	závit	k1gInSc1	závit
tvoří	tvořit	k5eAaImIp3nP	tvořit
asi	asi	k9	asi
6	[number]	k4	6
nukleozomů	nukleozom	k1gInPc2	nukleozom
a	a	k8xC	a
nese	nést	k5eAaImIp3nS	nést
1	[number]	k4	1
200	[number]	k4	200
párů	pár	k1gInPc2	pár
bází	báze	k1gFnPc2	báze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Solenoidy	solenoid	k1gInPc1	solenoid
se	se	k3xPyFc4	se
uspořádávají	uspořádávat	k5eAaImIp3nP	uspořádávat
do	do	k7c2	do
smyček	smyčka	k1gFnPc2	smyčka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
okolo	okolo	k7c2	okolo
50	[number]	k4	50
otoček	otočka	k1gFnPc2	otočka
solenoidu	solenoid	k1gInSc2	solenoid
a	a	k8xC	a
nese	nést	k5eAaImIp3nS	nést
stovky	stovka	k1gFnPc1	stovka
tisíc	tisíc	k4xCgInPc2	tisíc
párů	pár	k1gInPc2	pár
bází	báze	k1gFnPc2	báze
<g/>
.	.	kIx.	.
18	[number]	k4	18
smyček	smyčka	k1gFnPc2	smyčka
pravidelně	pravidelně	k6eAd1	pravidelně
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
okolo	okolo	k7c2	okolo
základní	základní	k2eAgFnSc2d1	základní
proteinové	proteinový	k2eAgFnSc2d1	proteinová
matrice	matrice	k1gFnSc2	matrice
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
základní	základní	k2eAgInSc1d1	základní
segment	segment	k1gInSc1	segment
chromozomu	chromozom	k1gInSc2	chromozom
<g/>
.	.	kIx.	.
</s>
<s>
Morfologie	morfologie	k1gFnSc1	morfologie
(	(	kIx(	(
<g/>
tvarové	tvarový	k2eAgNnSc1d1	tvarové
uspořádání	uspořádání	k1gNnSc1	uspořádání
<g/>
)	)	kIx)	)
těla	tělo	k1gNnSc2	tělo
řádně	řádně	k6eAd1	řádně
spiralizovaného	spiralizovaný	k2eAgInSc2d1	spiralizovaný
chromozomu	chromozom	k1gInSc2	chromozom
je	být	k5eAaImIp3nS	být
nejlépe	dobře	k6eAd3	dobře
pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
ve	v	k7c6	v
stadiu	stadion	k1gNnSc6	stadion
metafáze	metafáze	k1gFnSc2	metafáze
nebo	nebo	k8xC	nebo
rané	raný	k2eAgFnSc2d1	raná
anafáze	anafáze	k1gFnSc2	anafáze
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
fázích	fág	k1gInPc6	fág
jaderného	jaderný	k2eAgNnSc2d1	jaderné
dělení	dělení	k1gNnSc2	dělení
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
zkreslován	zkreslovat	k5eAaImNgMnS	zkreslovat
despiralizací	despiralizace	k1gFnSc7	despiralizace
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
též	též	k9	též
dekondezací	dekondezací	k1gFnSc2	dekondezací
<g/>
)	)	kIx)	)
chromozomu	chromozom	k1gInSc2	chromozom
<g/>
.	.	kIx.	.
</s>
<s>
Chromozom	chromozom	k1gInSc1	chromozom
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
pentlicovitý	pentlicovitý	k2eAgInSc1d1	pentlicovitý
útvar	útvar	k1gInSc1	útvar
tvořený	tvořený	k2eAgInSc1d1	tvořený
zpravidla	zpravidla	k6eAd1	zpravidla
dvěma	dva	k4xCgFnPc7	dva
podélně	podélně	k6eAd1	podélně
orientovanými	orientovaný	k2eAgFnPc7d1	orientovaná
chromatidami	chromatida	k1gFnPc7	chromatida
(	(	kIx(	(
<g/>
výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
polyténní	polyténní	k2eAgInPc1d1	polyténní
chromozomy	chromozom	k1gInPc1	chromozom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
propojeny	propojit	k5eAaPmNgInP	propojit
centromerou	centromera	k1gFnSc7	centromera
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
tzv.	tzv.	kA	tzv.
primární	primární	k2eAgFnSc1d1	primární
konstrikce	konstrikce	k1gFnSc1	konstrikce
<g/>
.	.	kIx.	.
</s>
<s>
Primární	primární	k2eAgFnSc1d1	primární
konstrikce	konstrikce	k1gFnSc1	konstrikce
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
chromatidy	chromatid	k1gInPc4	chromatid
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
ramena	rameno	k1gNnPc4	rameno
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
chromozomů	chromozom	k1gInPc2	chromozom
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ještě	ještě	k9	ještě
tzv.	tzv.	kA	tzv.
sekundární	sekundární	k2eAgFnSc1d1	sekundární
konstrikce	konstrikce	k1gFnSc1	konstrikce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
z	z	k7c2	z
jedněch	jeden	k4xCgNnPc2	jeden
ramen	rameno	k1gNnPc2	rameno
tzv.	tzv.	kA	tzv.
satelit	satelit	k1gInSc4	satelit
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnSc3d1	vnější
koncové	koncový	k2eAgFnSc3d1	koncová
oblasti	oblast	k1gFnSc3	oblast
ramen	rameno	k1gNnPc2	rameno
chromatid	chromatid	k1gInSc1	chromatid
(	(	kIx(	(
<g/>
ty	ten	k3xDgFnPc4	ten
odvrácené	odvrácený	k2eAgFnPc4d1	odvrácená
od	od	k7c2	od
centromery	centromera	k1gFnSc2	centromera
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
telomery	telomera	k1gFnPc1	telomera
a	a	k8xC	a
u	u	k7c2	u
živočichů	živočich	k1gMnPc2	živočich
hrají	hrát	k5eAaImIp3nP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
stárnutí	stárnutí	k1gNnSc1	stárnutí
<g/>
.	.	kIx.	.
</s>
<s>
Telomery	telomera	k1gFnPc1	telomera
<g/>
,	,	kIx,	,
centromera	centromero	k1gNnPc1	centromero
a	a	k8xC	a
oblasti	oblast	k1gFnPc1	oblast
konstrikce	konstrikce	k1gFnSc2	konstrikce
mají	mít	k5eAaImIp3nP	mít
strukturní	strukturní	k2eAgFnSc4d1	strukturní
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
genetickou	genetický	k2eAgFnSc4d1	genetická
informaci	informace	k1gFnSc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
umístění	umístění	k1gNnSc2	umístění
centromery	centromera	k1gFnSc2	centromera
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
základní	základní	k2eAgInPc1d1	základní
morfologické	morfologický	k2eAgInPc1d1	morfologický
typy	typ	k1gInPc1	typ
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
:	:	kIx,	:
Metacentrický	metacentrický	k2eAgInSc1d1	metacentrický
chromozom	chromozom	k1gInSc1	chromozom
–	–	k?	–
centromera	centromera	k1gFnSc1	centromera
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
zhruba	zhruba	k6eAd1	zhruba
uprostřed	uprostřed	k7c2	uprostřed
chromozomu	chromozom	k1gInSc2	chromozom
a	a	k8xC	a
dělí	dělit	k5eAaImIp3nP	dělit
chromatidy	chromatida	k1gFnPc4	chromatida
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
zhruba	zhruba	k6eAd1	zhruba
stejně	stejně	k6eAd1	stejně
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
ramena	rameno	k1gNnPc4	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Chromozom	chromozom	k1gInSc1	chromozom
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
replikaci	replikace	k1gFnSc6	replikace
DNA	dno	k1gNnSc2	dno
tvar	tvar	k1gInSc1	tvar
blížící	blížící	k2eAgFnSc2d1	blížící
se	se	k3xPyFc4	se
písmenu	písmeno	k1gNnSc3	písmeno
X	X	kA	X
<g/>
.	.	kIx.	.
</s>
<s>
Submetacentrický	Submetacentrický	k2eAgInSc1d1	Submetacentrický
chromozom	chromozom	k1gInSc1	chromozom
–	–	k?	–
centromera	centromera	k1gFnSc1	centromera
je	být	k5eAaImIp3nS	být
výrazněji	výrazně	k6eAd2	výrazně
posunuta	posunout	k5eAaPmNgFnS	posunout
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
jedněch	jeden	k4xCgInPc2	jeden
ramen	rameno	k1gNnPc2	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Akrocentrický	Akrocentrický	k2eAgInSc1d1	Akrocentrický
chromozom	chromozom	k1gInSc1	chromozom
–	–	k?	–
centromera	centromera	k1gFnSc1	centromera
dělí	dělit	k5eAaImIp3nS	dělit
chromatidy	chromatida	k1gFnSc2	chromatida
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
rameno	rameno	k1gNnSc4	rameno
velké	velká	k1gFnSc2	velká
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
malé	malý	k2eAgNnSc1d1	malé
uzlovité	uzlovitý	k2eAgInPc1d1	uzlovitý
<g/>
.	.	kIx.	.
</s>
<s>
Telocentrický	Telocentrický	k2eAgInSc1d1	Telocentrický
chromozom	chromozom	k1gInSc1	chromozom
–	–	k?	–
centromera	centromera	k1gFnSc1	centromera
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
u	u	k7c2	u
oblasti	oblast	k1gFnSc2	oblast
telomer	telomera	k1gFnPc2	telomera
a	a	k8xC	a
chromozom	chromozom	k1gInSc1	chromozom
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
G2	G2	k1gFnSc6	G2
fázi	fáze	k1gFnSc6	fáze
opticky	opticky	k6eAd1	opticky
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
jednoramenný	jednoramenný	k2eAgMnSc1d1	jednoramenný
a	a	k8xC	a
připomíná	připomínat	k5eAaImIp3nS	připomínat
písmeno	písmeno	k1gNnSc4	písmeno
V.	V.	kA	V.
Tento	tento	k3xDgInSc4	tento
typ	typ	k1gInSc4	typ
chromozomu	chromozom	k1gInSc2	chromozom
se	se	k3xPyFc4	se
v	v	k7c6	v
karyotypu	karyotyp	k1gInSc6	karyotyp
člověka	člověk	k1gMnSc2	člověk
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
chromozomů	chromozom	k1gInPc2	chromozom
s	s	k7c7	s
různě	různě	k6eAd1	různě
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
rameny	rameno	k1gNnPc7	rameno
se	se	k3xPyFc4	se
kratší	krátký	k2eAgNnSc1d2	kratší
rameno	rameno	k1gNnSc1	rameno
označuje	označovat	k5eAaImIp3nS	označovat
p	p	k?	p
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
z	z	k7c2	z
francouzského	francouzský	k2eAgMnSc2d1	francouzský
petit	petit	k1gInSc4	petit
<g/>
)	)	kIx)	)
a	a	k8xC	a
delší	dlouhý	k2eAgFnSc4d2	delší
q	q	k?	q
(	(	kIx(	(
<g/>
písmeno	písmeno	k1gNnSc4	písmeno
následující	následující	k2eAgNnSc4d1	následující
v	v	k7c6	v
abecedě	abeceda	k1gFnSc6	abeceda
po	po	k7c6	po
p	p	k?	p
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ramena	rameno	k1gNnPc1	rameno
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
oblasti	oblast	k1gFnPc4	oblast
a	a	k8xC	a
pruhy	pruh	k1gInPc4	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
konkrétního	konkrétní	k2eAgNnSc2d1	konkrétní
místa	místo	k1gNnSc2	místo
v	v	k7c6	v
karyotypu	karyotyp	k1gInSc6	karyotyp
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
čtyřmístný	čtyřmístný	k2eAgInSc4d1	čtyřmístný
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
první	první	k4xOgInSc4	první
znak	znak	k1gInSc4	znak
určuje	určovat	k5eAaImIp3nS	určovat
chromozom	chromozom	k1gInSc1	chromozom
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
rameno	rameno	k1gNnSc4	rameno
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc4	třetí
oblast	oblast	k1gFnSc4	oblast
(	(	kIx(	(
<g/>
číslují	číslovat	k5eAaImIp3nP	číslovat
se	se	k3xPyFc4	se
vzestupně	vzestupně	k6eAd1	vzestupně
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
centromery	centromera	k1gFnSc2	centromera
k	k	k7c3	k
telomerám	telomera	k1gFnPc3	telomera
<g/>
)	)	kIx)	)
a	a	k8xC	a
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
pruh	pruh	k1gInSc1	pruh
(	(	kIx(	(
<g/>
čísluje	číslovat	k5eAaImIp3nS	číslovat
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
kód	kód	k1gInSc1	kód
4	[number]	k4	4
<g/>
q	q	k?	q
<g/>
12	[number]	k4	12
–	–	k?	–
označuje	označovat	k5eAaImIp3nS	označovat
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
rameni	rameno	k1gNnSc3	rameno
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
chromozomu	chromozom	k1gInSc2	chromozom
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
1	[number]	k4	1
<g/>
,	,	kIx,	,
pruh	pruh	k1gInSc1	pruh
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
karyotyp	karyotyp	k1gInSc1	karyotyp
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
chromozomů	chromozom	k1gInPc2	chromozom
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
buňce	buňka	k1gFnSc6	buňka
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
karyotyp	karyotyp	k1gInSc1	karyotyp
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
velikosti	velikost	k1gFnSc2	velikost
i	i	k8xC	i
tvaru	tvar	k1gInSc2	tvar
chromozomů	chromozom	k1gInPc2	chromozom
v	v	k7c6	v
buněčných	buněčný	k2eAgNnPc6d1	buněčné
jádrech	jádro	k1gNnPc6	jádro
konstantní	konstantní	k2eAgMnSc1d1	konstantní
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
karotyp	karotyp	k1gInSc1	karotyp
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
druhových	druhový	k2eAgInPc2d1	druhový
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Odchylky	odchylka	k1gFnPc1	odchylka
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
aneuploidie	aneuploidie	k1gFnPc1	aneuploidie
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
jsou	být	k5eAaImIp3nP	být
patologické	patologický	k2eAgFnPc1d1	patologická
<g/>
.	.	kIx.	.
</s>
<s>
Účelné	účelný	k2eAgNnSc1d1	účelné
využití	využití	k1gNnSc1	využití
aneuploidie	aneuploidie	k1gFnSc2	aneuploidie
bylo	být	k5eAaImAgNnS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
kvasinek	kvasinka	k1gFnPc2	kvasinka
Saccharomyces	Saccharomycesa	k1gFnPc2	Saccharomycesa
cerevisiae	cerevisia	k1gFnSc2	cerevisia
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dokážou	dokázat	k5eAaPmIp3nP	dokázat
šest	šest	k4xCc1	šest
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
16	[number]	k4	16
chromozomů	chromozom	k1gInPc2	chromozom
jednotlivě	jednotlivě	k6eAd1	jednotlivě
nezávisle	závisle	k6eNd1	závisle
a	a	k8xC	a
vratně	vratně	k6eAd1	vratně
zmnožit	zmnožit	k5eAaPmF	zmnožit
a	a	k8xC	a
využít	využít	k5eAaPmF	využít
toho	ten	k3xDgMnSc4	ten
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
podoby	podoba	k1gFnSc2	podoba
a	a	k8xC	a
flexibilnější	flexibilní	k2eAgFnSc4d2	flexibilnější
adaptaci	adaptace	k1gFnSc4	adaptace
na	na	k7c4	na
okolní	okolní	k2eAgFnPc4d1	okolní
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
ploidie	ploidie	k1gFnSc2	ploidie
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
běžné	běžný	k2eAgFnPc1d1	běžná
buňky	buňka	k1gFnPc1	buňka
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
organismu	organismus	k1gInSc6	organismus
jsou	být	k5eAaImIp3nP	být
diploidní	diploidní	k2eAgInPc1d1	diploidní
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
2	[number]	k4	2
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
chromozomy	chromozom	k1gInPc1	chromozom
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
párech	pár	k1gInPc6	pár
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
chromozom	chromozom	k1gInSc1	chromozom
se	se	k3xPyFc4	se
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c4	o
autozómy	autozóma	k1gFnPc4	autozóma
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
oba	dva	k4xCgInPc1	dva
chromozómy	chromozóm	k1gInPc1	chromozóm
v	v	k7c6	v
páru	pár	k1gInSc6	pár
alely	alela	k1gFnSc2	alela
stejných	stejný	k2eAgInPc2d1	stejný
genů	gen	k1gInPc2	gen
ve	v	k7c6	v
stejných	stejný	k2eAgInPc6d1	stejný
lokusech	lokus	k1gInPc6	lokus
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
alely	alela	k1gFnPc1	alela
ovšem	ovšem	k9	ovšem
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
různé	různý	k2eAgInPc1d1	různý
(	(	kIx(	(
<g/>
každá	každý	k3xTgFnSc1	každý
alela	alela	k1gFnSc1	alela
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dominantní	dominantní	k2eAgFnSc1d1	dominantní
nebo	nebo	k8xC	nebo
recesivní	recesivní	k2eAgFnSc1d1	recesivní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c4	o
heterozomy	heterozom	k1gInPc4	heterozom
(	(	kIx(	(
<g/>
pohlavní	pohlavní	k2eAgInPc4d1	pohlavní
chromozomy	chromozom	k1gInPc4	chromozom
<g/>
,	,	kIx,	,
u	u	k7c2	u
organismů	organismus	k1gInPc2	organismus
typu	typ	k1gInSc2	typ
Drosophila	Drosophil	k1gMnSc2	Drosophil
X	X	kA	X
a	a	k8xC	a
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
totéž	týž	k3xTgNnSc1	týž
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
homonymní	homonymní	k2eAgFnSc4d1	homonymní
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
heteronymní	heteronymní	k2eAgFnPc1d1	heteronymní
části	část	k1gFnPc1	část
heterozomů	heterozom	k1gInPc2	heterozom
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
různé	různý	k2eAgInPc1d1	různý
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
morfologií	morfologie	k1gFnSc7	morfologie
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
geny	gen	k1gInPc1	gen
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
alely	alela	k1gFnPc1	alela
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
jsou	být	k5eAaImIp3nP	být
zakódovány	zakódován	k2eAgInPc1d1	zakódován
znaky	znak	k1gInPc1	znak
vázané	vázaný	k2eAgInPc1d1	vázaný
na	na	k7c4	na
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
diploidních	diploidní	k2eAgFnPc2d1	diploidní
buněk	buňka	k1gFnPc2	buňka
existují	existovat	k5eAaImIp3nP	existovat
běžně	běžně	k6eAd1	běžně
i	i	k9	i
buňky	buňka	k1gFnPc1	buňka
haploidní	haploidní	k2eAgFnPc1d1	haploidní
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
chromozom	chromozom	k1gInSc4	chromozom
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
během	během	k7c2	během
dělení	dělení	k1gNnSc2	dělení
buněk	buňka	k1gFnPc2	buňka
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
výsledek	výsledek	k1gInSc1	výsledek
redukčního	redukční	k2eAgNnSc2d1	redukční
dělení	dělení	k1gNnSc2	dělení
<g/>
.	.	kIx.	.
</s>
<s>
Umělou	umělý	k2eAgFnSc7d1	umělá
manipulací	manipulace	k1gFnSc7	manipulace
s	s	k7c7	s
genetickou	genetický	k2eAgFnSc7d1	genetická
informací	informace	k1gFnSc7	informace
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
i	i	k9	i
buňky	buňka	k1gFnPc4	buňka
či	či	k8xC	či
celé	celý	k2eAgInPc4d1	celý
organismy	organismus	k1gInPc4	organismus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
polyploidní	polyploidní	k2eAgNnSc1d1	polyploidní
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
více	hodně	k6eAd2	hodně
než	než	k8xS	než
diploidní	diploidní	k2eAgFnSc1d1	diploidní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
vysazuje	vysazovat	k5eAaImIp3nS	vysazovat
například	například	k6eAd1	například
tetraploidní	tetraploidní	k2eAgFnSc1d1	tetraploidní
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
genů	gen	k1gInPc2	gen
chromozomy	chromozom	k1gInPc4	chromozom
studuje	studovat	k5eAaImIp3nS	studovat
genetika	genetika	k1gFnSc1	genetika
<g/>
,	,	kIx,	,
manipulací	manipulace	k1gFnPc2	manipulace
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
genetické	genetický	k2eAgNnSc1d1	genetické
inženýrství	inženýrství	k1gNnSc1	inženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Autozom	Autozom	k1gInSc1	Autozom
Chromozomová	Chromozomový	k2eAgFnSc1d1	Chromozomový
mutace	mutace	k1gFnSc1	mutace
Genomová	Genomový	k2eAgFnSc1d1	Genomová
mutace	mutace	k1gFnSc2	mutace
Genová	genový	k2eAgFnSc1d1	genová
vazebná	vazebný	k2eAgFnSc1d1	vazebná
skupina	skupina	k1gFnSc1	skupina
Genom	genom	k1gInSc4	genom
Lidský	lidský	k2eAgInSc1d1	lidský
genom	genom	k1gInSc1	genom
Polyténní	Polyténní	k2eAgInSc4d1	Polyténní
chromozom	chromozom	k1gInSc4	chromozom
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
chromozom	chromozom	k1gInSc1	chromozom
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
