<s>
Růže	růže	k1gFnSc1	růže
(	(	kIx(	(
<g/>
Rosa	Rosa	k1gFnSc1	Rosa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
keřovitých	keřovitý	k2eAgFnPc2d1	keřovitá
rostlin	rostlina	k1gFnPc2	rostlina
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
druhy	druh	k1gInPc7	druh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
