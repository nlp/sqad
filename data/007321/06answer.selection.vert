<s>
Deoxyribonukleová	deoxyribonukleový	k2eAgFnSc1d1	deoxyribonukleová
kyselina	kyselina	k1gFnSc1	kyselina
byla	být	k5eAaImAgFnS	být
popsána	popsán	k2eAgFnSc1d1	popsána
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
lékař	lékař	k1gMnSc1	lékař
Friedrich	Friedrich	k1gMnSc1	Friedrich
Miescher	Mieschra	k1gFnPc2	Mieschra
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
složení	složení	k1gNnSc3	složení
hnisu	hnis	k1gInSc2	hnis
z	z	k7c2	z
nemocničních	nemocniční	k2eAgInPc2d1	nemocniční
obvazů	obvaz	k1gInPc2	obvaz
<g/>
.	.	kIx.	.
</s>
