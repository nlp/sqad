<s>
Jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
napájena	napájet	k5eAaImNgFnS	napájet
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
<g/>
,	,	kIx,	,
srážkovou	srážkový	k2eAgFnSc7d1	srážková
popř.	popř.	kA	popř.
podzemní	podzemní	k2eAgFnSc7d1	podzemní
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětově	celosvětově	k6eAd1	celosvětově
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
jezera	jezero	k1gNnPc4	jezero
1,8	[number]	k4	1,8
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
objem	objem	k1gInSc1	objem
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
180	[number]	k4	180
tisíc	tisíc	k4xCgInSc4	tisíc
kubických	kubický	k2eAgInPc2d1	kubický
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
velká	velký	k2eAgNnPc1d1	velké
bezodtoká	bezodtoký	k2eAgNnPc1d1	bezodtoké
jezera	jezero	k1gNnPc1	jezero
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
vnitrozemskými	vnitrozemský	k2eAgNnPc7d1	vnitrozemské
moři	moře	k1gNnPc7	moře
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
slanou	slaný	k2eAgFnSc4d1	slaná
vodu	voda	k1gFnSc4	voda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
Saltonské	Saltonský	k2eAgNnSc1d1	Saltonské
moře	moře	k1gNnSc1	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumáním	zkoumání	k1gNnSc7	zkoumání
jezer	jezero	k1gNnPc2	jezero
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
věda	věda	k1gFnSc1	věda
zvaná	zvaný	k2eAgFnSc1d1	zvaná
limnologie	limnologie	k1gFnSc1	limnologie
<g/>
.	.	kIx.	.
</s>
<s>
Jezera	jezero	k1gNnPc1	jezero
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přirozeného	přirozený	k2eAgInSc2d1	přirozený
nebo	nebo	k8xC	nebo
umělého	umělý	k2eAgInSc2d1	umělý
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
jezero	jezero	k1gNnSc1	jezero
součástí	součást	k1gFnSc7	součást
pojmenování	pojmenování	k1gNnSc2	pojmenování
jen	jen	k9	jen
několika	několik	k4yIc2	několik
přehradních	přehradní	k2eAgFnPc2d1	přehradní
nádrží	nádrž	k1gFnPc2	nádrž
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Ivanské	Ivanský	k2eAgNnSc1d1	Ivanský
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
a	a	k8xC	a
rybníků	rybník	k1gInPc2	rybník
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Máchovo	Máchův	k2eAgNnSc1d1	Máchovo
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nádrže	nádrž	k1gFnPc1	nádrž
napodobující	napodobující	k2eAgFnPc1d1	napodobující
jezera	jezero	k1gNnPc1	jezero
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
používány	používat	k5eAaImNgFnP	používat
jako	jako	k8xC	jako
prvek	prvek	k1gInSc1	prvek
zahradní	zahradní	k2eAgFnSc2d1	zahradní
architektury	architektura	k1gFnSc2	architektura
v	v	k7c6	v
sadovnické	sadovnický	k2eAgFnSc6d1	Sadovnická
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
sladkovodní	sladkovodní	k2eAgNnPc1d1	sladkovodní
jezera	jezero	k1gNnPc1	jezero
(	(	kIx(	(
<g/>
Ladožské	ladožský	k2eAgNnSc1d1	Ladožské
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
)	)	kIx)	)
slaná	slaný	k2eAgNnPc1d1	slané
jezera	jezero	k1gNnPc1	jezero
(	(	kIx(	(
<g/>
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
<g/>
)	)	kIx)	)
smíšená	smíšený	k2eAgNnPc1d1	smíšené
jezera	jezero	k1gNnPc1	jezero
–	–	k?	–
jedna	jeden	k4xCgFnSc1	jeden
část	část	k1gFnSc1	část
jezera	jezero	k1gNnSc2	jezero
<g />
.	.	kIx.	.
</s>
<s>
má	mít	k5eAaImIp3nS	mít
slanou	slaný	k2eAgFnSc4d1	slaná
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
sladkou	sladký	k2eAgFnSc7d1	sladká
(	(	kIx(	(
<g/>
Neziderské	neziderský	k2eAgNnSc1d1	Neziderské
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
Balchaš	Balchaš	k1gInSc1	Balchaš
<g/>
)	)	kIx)	)
hořká	hořká	k1gFnSc1	hořká
jezera	jezero	k1gNnSc2	jezero
(	(	kIx(	(
<g/>
Velké	velký	k2eAgNnSc1d1	velké
Hořké	hořký	k2eAgNnSc1d1	hořké
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
tektonická	tektonický	k2eAgFnSc1d1	tektonická
sopečná	sopečný	k2eAgFnSc1d1	sopečná
(	(	kIx(	(
<g/>
vulkanická	vulkanický	k2eAgFnSc1d1	vulkanická
<g/>
)	)	kIx)	)
ledovcová	ledovcový	k2eAgFnSc1d1	ledovcová
sesuvová	sesuvový	k2eAgFnSc1d1	sesuvový
termokrasová	termokrasová	k1gFnSc1	termokrasová
fluviální	fluviální	k2eAgFnSc1d1	fluviální
limanová	limanový	k2eAgFnSc1d1	limanový
eolická	eolický	k2eAgFnSc1d1	eolická
meteoritická	meteoritický	k2eAgFnSc1d1	meteoritická
krasová	krasový	k2eAgFnSc1d1	krasová
organogenní	organogenní	k2eAgFnSc1d1	organogenní
antropogenní	antropogenní	k2eAgFnSc1d1	antropogenní
(	(	kIx(	(
<g/>
např.	např.	kA	např.
písníky	písník	k1gInPc1	písník
<g/>
,	,	kIx,	,
zatopené	zatopený	k2eAgInPc1d1	zatopený
lomy	lom	k1gInPc1	lom
<g />
.	.	kIx.	.
</s>
<s>
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Jezera	jezero	k1gNnPc1	jezero
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
kombinovaný	kombinovaný	k2eAgInSc4d1	kombinovaný
původ	původ	k1gInSc4	původ
např.	např.	kA	např.
tektonicko-ledovcový	tektonickoedovcový	k2eAgMnSc1d1	tektonicko-ledovcový
(	(	kIx(	(
<g/>
Ladožské	ladožský	k2eAgNnSc1d1	Ladožské
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tektonicko-krasový	tektonickorasový	k2eAgMnSc1d1	tektonicko-krasový
(	(	kIx(	(
<g/>
Skadarské	skadarský	k2eAgNnSc1d1	Skadarské
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
a	a	k8xC	a
tvarování	tvarování	k1gNnSc6	tvarování
jezerní	jezerní	k2eAgFnSc2d1	jezerní
pánve	pánev	k1gFnSc2	pánev
podílí	podílet	k5eAaImIp3nS	podílet
více	hodně	k6eAd2	hodně
různých	různý	k2eAgInPc2d1	různý
geomorfologických	geomorfologický	k2eAgInPc2d1	geomorfologický
pochodů	pochod	k1gInPc2	pochod
<g/>
.	.	kIx.	.
průtočná	průtočný	k2eAgNnPc1d1	průtočné
jezera	jezero	k1gNnPc1	jezero
tj.	tj.	kA	tj.
jezera	jezero	k1gNnSc2	jezero
s	s	k7c7	s
přítoky	přítok	k1gInPc7	přítok
a	a	k8xC	a
alespoň	alespoň	k9	alespoň
<g />
.	.	kIx.	.
</s>
<s>
jedním	jeden	k4xCgInSc7	jeden
odtokem	odtok	k1gInSc7	odtok
–	–	k?	–
např.	např.	kA	např.
Ladožské	ladožský	k2eAgNnSc1d1	Ladožské
jezero	jezero	k1gNnSc1	jezero
bezpřítoková	bezpřítokový	k2eAgNnPc1d1	bezpřítokový
jezera	jezero	k1gNnPc1	jezero
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
napájena	napájen	k2eAgNnPc1d1	napájeno
pouze	pouze	k6eAd1	pouze
podzemní	podzemní	k2eAgFnSc7d1	podzemní
a	a	k8xC	a
dešťovou	dešťový	k2eAgFnSc7d1	dešťová
vodou	voda	k1gFnSc7	voda
<g/>
)	)	kIx)	)
bezodtoková	bezodtokový	k2eAgNnPc1d1	bezodtokové
jezera	jezero	k1gNnPc1	jezero
(	(	kIx(	(
<g/>
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
přítoky	přítok	k1gInPc1	přítok
<g/>
)	)	kIx)	)
–	–	k?	–
např.	např.	kA	např.
Aralské	aralský	k2eAgNnSc1d1	Aralské
jezero	jezero	k1gNnSc1	jezero
jezera	jezero	k1gNnSc2	jezero
bez	bez	k7c2	bez
přítoků	přítok	k1gInPc2	přítok
a	a	k8xC	a
bez	bez	k7c2	bez
odtoků	odtok	k1gInPc2	odtok
–	–	k?	–
např.	např.	kA	např.
Jezírko	jezírko	k1gNnSc4	jezírko
v	v	k7c6	v
Praze-Hlubočepích	Praze-Hlubočepí	k1gNnPc6	Praze-Hlubočepí
nejhlubší	hluboký	k2eAgNnSc1d3	nejhlubší
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
Bajkal	Bajkal	k1gInSc1	Bajkal
největším	veliký	k2eAgNnSc7d3	veliký
jezerem	jezero	k1gNnSc7	jezero
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
reliktní	reliktní	k2eAgNnSc1d1	reliktní
jezero	jezero	k1gNnSc1	jezero
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
jak	jak	k6eAd1	jak
podle	podle	k7c2	podle
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
podle	podle	k7c2	podle
objemu	objem	k1gInSc2	objem
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
vlny	vlna	k1gFnSc2	vlna
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
na	na	k7c6	na
Velkých	velký	k2eAgNnPc6d1	velké
jezerech	jezero	k1gNnPc6	jezero
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
výšky	výška	k1gFnSc2	výška
6	[number]	k4	6
metrů	metr	k1gInPc2	metr
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
slapových	slapový	k2eAgFnPc2d1	slapová
sil	síla	k1gFnPc2	síla
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
u	u	k7c2	u
Michiganského	michiganský	k2eAgNnSc2d1	Michiganské
jezera	jezero	k1gNnSc2	jezero
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
<g />
.	.	kIx.	.
</s>
<s>
vzdutí	vzdutí	k1gNnSc1	vzdutí
hladiny	hladina	k1gFnSc2	hladina
až	až	k9	až
o	o	k7c4	o
7	[number]	k4	7
cm	cm	kA	cm
Bohumír	Bohumír	k1gMnSc1	Bohumír
Janský	janský	k2eAgMnSc1d1	janský
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Šobr	Šobr	k1gMnSc1	Šobr
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Jezera	jezero	k1gNnSc2	jezero
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Katedra	katedra	k1gFnSc1	katedra
fyzické	fyzický	k2eAgFnSc2d1	fyzická
geografie	geografie	k1gFnSc2	geografie
a	a	k8xC	a
geoekologie	geoekologie	k1gFnSc2	geoekologie
Přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86561-05-4	[number]	k4	80-86561-05-4
Seznam	seznam	k1gInSc4	seznam
českých	český	k2eAgNnPc2d1	české
jezer	jezero	k1gNnPc2	jezero
Největší	veliký	k2eAgInSc4d3	veliký
jezera	jezero	k1gNnSc2	jezero
světa	svět	k1gInSc2	svět
podle	podle	k7c2	podle
rozlohy	rozloha	k1gFnSc2	rozloha
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
jezero	jezero	k1gNnSc1	jezero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
jezero	jezero	k1gNnSc4	jezero
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
