<s>
Jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
napájena	napájet	k5eAaImNgFnS	napájet
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
<g/>
,	,	kIx,	,
srážkovou	srážkový	k2eAgFnSc7d1	srážková
popř.	popř.	kA	popř.
podzemní	podzemní	k2eAgFnSc7d1	podzemní
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
