<s>
Trnuchovití	Trnuchovitý	k2eAgMnPc1d1
</s>
<s>
Trnucha	trnucha	k1gFnSc1
americká	americký	k2eAgFnSc1d1
Trnucha	trnucha	k1gFnSc1
americká	americký	k2eAgFnSc1d1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
paryby	paryba	k1gFnPc1
(	(	kIx(
<g/>
Chondrichthyes	Chondrichthyes	k1gInSc1
<g/>
)	)	kIx)
Nadřád	nadřád	k1gInSc1
</s>
<s>
rejnoci	rejnok	k1gMnPc1
(	(	kIx(
<g/>
Batoidea	Batoidea	k1gMnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
praví	pravý	k2eAgMnPc1d1
rejnoci	rejnok	k1gMnPc1
(	(	kIx(
<g/>
Rajiformes	Rajiformes	k1gMnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
trnuchovití	trnuchovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Dasyatidae	Dasyatidae	k1gNnSc7
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Trnuchovití	Trnuchovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Dasyatidae	Dasyatidae	k1gNnSc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
čeleď	čeleď	k1gFnSc1
paryb	paryba	k1gFnPc2
z	z	k7c2
řádu	řád	k1gInSc2
pravých	pravý	k2eAgMnPc2d1
rejnoků	rejnok	k1gMnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
typická	typický	k2eAgFnSc1d1
tvarem	tvar	k1gInSc7
svého	svůj	k3xOyFgNnSc2
těla	tělo	k1gNnSc2
a	a	k8xC
pohybem	pohyb	k1gInSc7
<g/>
,	,	kIx,
zdánlivě	zdánlivě	k6eAd1
připomínajícím	připomínající	k2eAgInSc6d1
ptačí	ptačit	k5eAaImIp3nS
let	let	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc4d1
název	název	k1gInSc4
získala	získat	k5eAaPmAgFnS
podle	podle	k7c2
jedovatého	jedovatý	k2eAgInSc2d1
trnu	trn	k1gInSc2
na	na	k7c6
konci	konec	k1gInSc6
ocasu	ocas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čeleď	čeleď	k1gFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
7	#num#	k4
rodů	rod	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
obsahují	obsahovat	k5eAaImIp3nP
okolo	okolo	k7c2
70	#num#	k4
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Rody	rod	k1gInPc1
</s>
<s>
rod	rod	k1gInSc1
trnucha	trnucha	k1gFnSc1
Dasyatis	Dasyatis	k1gFnSc1
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
acutirostra	acutirostra	k1gFnSc1
(	(	kIx(
<g/>
Nishida	Nishida	k1gFnSc1
&	&	k?
Nakaya	Nakaya	k1gMnSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
trnucha	trnucha	k1gFnSc1
východní	východní	k2eAgFnSc2d1
<g/>
,	,	kIx,
Dasyatis	Dasyatis	k1gFnSc2
akajei	akaje	k1gFnSc2
(	(	kIx(
<g/>
Müller	Müller	k1gMnSc1
&	&	k?
Henle	Henle	k1gInSc1
<g/>
,	,	kIx,
1841	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
trnucha	trnucha	k1gFnSc1
americká	americký	k2eAgFnSc1d1
<g/>
,	,	kIx,
Dasyatis	Dasyatis	k1gFnSc1
americana	americana	k1gFnSc1
(	(	kIx(
<g/>
Hildebrand	Hildebrand	k1gInSc1
&	&	k?
Schroeder	Schroeder	k1gInSc1
<g/>
,	,	kIx,
1928	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
annotata	annotata	k1gFnSc1
(	(	kIx(
<g/>
Last	Last	k1gMnSc1
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
bennetti	bennetť	k1gFnSc2
(	(	kIx(
<g/>
Müller	Müller	k1gMnSc1
&	&	k?
Henle	Henle	k1gInSc1
<g/>
,	,	kIx,
1841	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
brevicaudata	brevicaudata	k1gFnSc1
(	(	kIx(
<g/>
Hutton	Hutton	k1gInSc1
<g/>
,	,	kIx,
1875	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
brevis	brevis	k1gFnSc1
(	(	kIx(
<g/>
Garman	Garman	k1gMnSc1
<g/>
,	,	kIx,
1880	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
centroura	centroura	k1gFnSc1
(	(	kIx(
<g/>
Mitchill	Mitchill	k1gMnSc1
<g/>
,	,	kIx,
1815	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
chrysonota	chrysonota	k1gFnSc1
(	(	kIx(
<g/>
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
1828	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
dipterura	dipterura	k1gFnSc1
(	(	kIx(
<g/>
Jordan	Jordan	k1gMnSc1
&	&	k?
Gilbert	Gilbert	k1gMnSc1
<g/>
,	,	kIx,
1880	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gInSc1
fluviorum	fluviorum	k1gInSc1
(	(	kIx(
<g/>
Ogilby	Ogilba	k1gFnSc2
<g/>
,	,	kIx,
1908	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
garouaensis	garouaensis	k1gFnSc1
(	(	kIx(
<g/>
Stauch	Stauch	k1gMnSc1
&	&	k?
Blanc	Blanc	k1gMnSc1
<g/>
,	,	kIx,
1962	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc4
geijskesi	geijskese	k1gFnSc4
(	(	kIx(
<g/>
Boeseman	Boeseman	k1gMnSc1
<g/>
,	,	kIx,
1948	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
gigantea	gigantea	k1gMnSc1
(	(	kIx(
<g/>
Lindberg	Lindberg	k1gMnSc1
<g/>
,	,	kIx,
1930	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
guttata	guttata	k1gFnSc1
(	(	kIx(
<g/>
Bloch	Bloch	k1gMnSc1
&	&	k?
Schneider	Schneider	k1gMnSc1
<g/>
,	,	kIx,
1801	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
hastata	hastat	k1gMnSc2
(	(	kIx(
<g/>
DeKay	DeKaa	k1gMnSc2
<g/>
,	,	kIx,
1842	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
izuensis	izuensis	k1gFnSc1
(	(	kIx(
<g/>
Nishida	Nishida	k1gFnSc1
&	&	k?
Nakaya	Nakaya	k1gMnSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
trnucha	trnucha	k1gFnSc1
vanicorská	vanicorský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Dasyatis	Dasyatis	k1gInSc1
kuhlii	kuhlie	k1gFnSc4
(	(	kIx(
<g/>
Müller	Müller	k1gInSc1
&	&	k?
Henle	Henle	k1gInSc1
<g/>
,	,	kIx,
1841	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
laevigata	laevigata	k1gFnSc1
(	(	kIx(
<g/>
Chu	Chu	k1gMnSc1
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
laosensis	laosensis	k1gFnSc1
(	(	kIx(
<g/>
Roberts	Roberts	k1gInSc1
&	&	k?
Karnasuta	Karnasut	k1gMnSc2
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
latus	latus	k1gMnSc1
(	(	kIx(
<g/>
Garman	Garman	k1gMnSc1
<g/>
,	,	kIx,
1880	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gInSc4
leylandi	leyland	k1gMnPc1
(	(	kIx(
<g/>
Last	Last	k1gMnSc1
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
longa	longa	k1gFnSc1
(	(	kIx(
<g/>
Garman	Garman	k1gMnSc1
<g/>
,	,	kIx,
1880	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
margarita	margarita	k1gFnSc1
(	(	kIx(
<g/>
Günther	Günthra	k1gFnPc2
<g/>
,	,	kIx,
1870	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
margaritella	margaritella	k1gFnSc1
(	(	kIx(
<g/>
Compagno	Compagno	k1gNnSc1
&	&	k?
Roberts	Robertsa	k1gFnPc2
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
marianae	marianae	k1gFnSc1
(	(	kIx(
<g/>
Gomes	Gomes	k1gMnSc1
<g/>
,	,	kIx,
Rosa	Rosa	k1gMnSc1
&	&	k?
Gadig	Gadig	k1gMnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
marmorata	marmorata	k1gFnSc1
(	(	kIx(
<g/>
Steindachner	Steindachner	k1gMnSc1
<g/>
,	,	kIx,
1892	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
matsubarai	matsubarai	k1gNnPc7
(	(	kIx(
<g/>
Miyosi	Miyos	k1gMnPc7
<g/>
,	,	kIx,
1939	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
microps	micropsa	k1gFnPc2
(	(	kIx(
<g/>
Annandale	Annandala	k1gFnSc6
<g/>
,	,	kIx,
1908	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
multispinosa	multispinosa	k1gFnSc1
(	(	kIx(
<g/>
Tokarev	Tokarev	k1gFnSc1
<g/>
,	,	kIx,
1959	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
navarrae	navarrae	k1gFnSc1
(	(	kIx(
<g/>
Steindachner	Steindachner	k1gMnSc1
<g/>
,	,	kIx,
1892	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
trnucha	trnucha	k1gFnSc1
obecná	obecná	k1gFnSc1
<g/>
,	,	kIx,
Dasyatis	Dasyatis	k1gFnSc1
pastinaca	pastinaca	k1gMnSc1
(	(	kIx(
<g/>
Linnaeus	Linnaeus	k1gMnSc1
<g/>
,	,	kIx,
1758	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
rudis	rudis	k1gFnPc2
(	(	kIx(
<g/>
Günther	Günthra	k1gFnPc2
<g/>
,	,	kIx,
1870	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
trnucha	trnucha	k1gFnSc1
atlantská	atlantský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Dasyatis	Dasyatis	k1gFnSc1
sabina	sabina	k1gMnSc1
(	(	kIx(
<g/>
Lesueur	Lesueur	k1gMnSc1
<g/>
,	,	kIx,
1824	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
say	say	k?
(	(	kIx(
<g/>
Lesueur	Lesueur	k1gMnSc1
<g/>
,	,	kIx,
1817	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
sinensis	sinensis	k1gFnSc1
(	(	kIx(
<g/>
Steindachner	Steindachner	k1gMnSc1
<g/>
,	,	kIx,
1892	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
thetidis	thetidis	k1gFnSc2
(	(	kIx(
<g/>
Ogilby	Ogilba	k1gFnSc2
<g/>
,	,	kIx,
1899	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
tortonesei	tortonese	k1gFnSc2
(	(	kIx(
<g/>
Capapé	Capapý	k2eAgInPc1d1
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
trnucha	trnucha	k1gFnSc1
mikawská	mikawská	k1gFnSc1
<g/>
,	,	kIx,
Dasyatis	Dasyatis	k1gFnSc1
ushiei	ushie	k1gFnSc2
(	(	kIx(
<g/>
Jordan	Jordan	k1gMnSc1
&	&	k?
Hubbs	Hubbs	k1gInSc1
<g/>
,	,	kIx,
1925	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dasyatis	Dasyatis	k1gFnSc1
zugei	zuge	k1gFnSc2
(	(	kIx(
<g/>
Müller	Müller	k1gMnSc1
&	&	k?
Henle	Henle	k1gInSc1
<g/>
,	,	kIx,
1841	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
rod	rod	k1gInSc1
Himantura	Himantura	k1gFnSc1
</s>
<s>
Himantura	Himantura	k1gFnSc1
alcockii	alcockium	k1gNnPc7
(	(	kIx(
<g/>
Annandale	Annandala	k1gFnSc6
<g/>
,	,	kIx,
1909	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
bleekeri	bleeker	k1gFnSc2
(	(	kIx(
<g/>
Blyth	Blyth	k1gMnSc1
<g/>
,	,	kIx,
1860	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
trnucha	trnucha	k1gFnSc1
říční	říční	k2eAgFnSc1d1
Himantura	Himantura	k1gFnSc1
chaophraya	chaophray	k2eAgFnSc1d1
(	(	kIx(
<g/>
Monkolprasit	Monkolprasit	k1gFnSc1
&	&	k?
Roberts	Robertsa	k1gFnPc2
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
draco	draco	k6eAd1
(	(	kIx(
<g/>
Compagno	Compagno	k6eAd1
&	&	k?
Heemstra	Heemstra	k1gFnSc1
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
fai	fai	k?
(	(	kIx(
<g/>
Jordan	Jordan	k1gMnSc1
&	&	k?
Seale	Seale	k1gInSc1
<g/>
,	,	kIx,
1906	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
fluviatilis	fluviatilis	k1gFnSc1
(	(	kIx(
<g/>
Hamilton	Hamilton	k1gInSc1
<g/>
,	,	kIx,
1822	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
gerrardi	gerrard	k1gMnPc1
(	(	kIx(
<g/>
Gray	Gray	k1gInPc1
<g/>
,	,	kIx,
1851	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
granulata	granule	k1gNnPc1
(	(	kIx(
<g/>
Macleay	Macleay	k1gInPc1
<g/>
,	,	kIx,
1883	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
hortlei	hortle	k1gFnSc2
Last	Lasta	k1gFnPc2
<g/>
,	,	kIx,
Manjaji-Matsumoto	Manjaji-Matsumota	k1gFnSc5
&	&	k?
Kailola	Kailola	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Himantura	Himantura	k1gFnSc1
imbricata	imbricata	k1gFnSc1
(	(	kIx(
<g/>
Bloch	Bloch	k1gMnSc1
&	&	k?
Schneider	Schneider	k1gMnSc1
<g/>
,	,	kIx,
1801	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
jenkinsii	jenkinsie	k1gFnSc6
(	(	kIx(
<g/>
Annandale	Annandala	k1gFnSc6
<g/>
,	,	kIx,
1909	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
kittipongi	kittipong	k1gFnSc2
</s>
<s>
Himantura	Himantura	k1gFnSc1
krempfi	krempf	k1gFnSc2
(	(	kIx(
<g/>
Chabanaud	Chabanaud	k1gMnSc1
<g/>
,	,	kIx,
1923	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
lobistoma	lobistoma	k1gFnSc1
Manjaji-Matsumoto	Manjaji-Matsumota	k1gFnSc5
&	&	k?
Last	Lastum	k1gNnPc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Himantura	Himantura	k1gFnSc1
marginatus	marginatus	k1gMnSc1
(	(	kIx(
<g/>
Blyth	Blyth	k1gMnSc1
<g/>
,	,	kIx,
1860	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
microphthalma	microphthalma	k1gFnSc1
(	(	kIx(
<g/>
Chen	Chen	k1gNnSc1
<g/>
,	,	kIx,
1948	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
oxyrhyncha	oxyrhyncha	k1gFnSc1
(	(	kIx(
<g/>
Sauvage	Sauvage	k1gNnSc1
<g/>
,	,	kIx,
1878	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
pacifica	pacifica	k1gFnSc1
(	(	kIx(
<g/>
Beebe	Beeb	k1gInSc5
&	&	k?
Tee-Van	Tee-Vany	k1gInPc2
<g/>
,	,	kIx,
1941	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
pareh	pareh	k1gMnSc1
(	(	kIx(
<g/>
Bleeker	Bleeker	k1gMnSc1
<g/>
,	,	kIx,
1852	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
pastinacoides	pastinacoides	k1gMnSc1
(	(	kIx(
<g/>
Bleeker	Bleeker	k1gMnSc1
<g/>
,	,	kIx,
1852	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
schmardae	schmardae	k1gFnSc1
(	(	kIx(
<g/>
Werner	Werner	k1gMnSc1
<g/>
,	,	kIx,
1904	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
signifer	signifra	k1gFnPc2
(	(	kIx(
<g/>
Compagno	Compagno	k6eAd1
&	&	k?
Roberts	Robertsa	k1gFnPc2
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
toshi	tosh	k1gFnSc2
(	(	kIx(
<g/>
Whitley	Whitlea	k1gMnSc2
<g/>
,	,	kIx,
1939	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
uarnacoides	uarnacoides	k1gMnSc1
(	(	kIx(
<g/>
Bleeker	Bleeker	k1gMnSc1
<g/>
,	,	kIx,
1852	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
trnucha	trnucha	k1gFnSc1
leopardí	leopardí	k2eAgFnSc1d1
,	,	kIx,
Himantura	Himantura	k1gFnSc1
uarnak	uarnak	k1gMnSc1
(	(	kIx(
<g/>
Forsskå	Forsskå	k1gMnSc1
<g/>
,	,	kIx,
1775	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
undulata	undule	k1gNnPc1
(	(	kIx(
<g/>
Bleeker	Bleeker	k1gInSc1
<g/>
,	,	kIx,
1852	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Himantura	Himantura	k1gFnSc1
walga	walga	k1gFnSc1
(	(	kIx(
<g/>
Müller	Müller	k1gInSc1
&	&	k?
Henle	Henle	k1gInSc1
<g/>
,	,	kIx,
1841	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
rod	rod	k1gInSc1
Makararaja	Makararaj	k1gInSc2
</s>
<s>
Makararaja	Makararaj	k2eAgFnSc1d1
chindwinensis	chindwinensis	k1gFnSc1
Roberts	Robertsa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
rod	rod	k1gInSc1
Pastinachus	Pastinachus	k1gInSc1
</s>
<s>
trnucha	trnucha	k1gFnSc1
jemenská	jemenský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Pastinachus	Pastinachus	k1gInSc1
sephen	sephen	k2eAgInSc1d1
(	(	kIx(
<g/>
Forsskå	Forsskå	k1gInSc1
<g/>
,	,	kIx,
1775	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pastinachus	Pastinachus	k1gInSc1
solocirostris	solocirostris	k1gInSc1
(	(	kIx(
<g/>
Last	Last	k1gInSc1
<g/>
,	,	kIx,
Manjaji	Manjaje	k1gFnSc4
&	&	k?
Yearsley	Yearslea	k1gFnSc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
rod	rod	k1gInSc1
Pteroplatytrygon	Pteroplatytrygona	k1gFnPc2
</s>
<s>
trnucha	trnucha	k1gFnSc1
fialová	fialový	k2eAgFnSc1d1
,	,	kIx,
Pteroplatytrygon	Pteroplatytrygon	k1gMnSc1
violacea	violacea	k1gMnSc1
(	(	kIx(
<g/>
Bonaparte	bonapart	k1gInSc5
<g/>
,	,	kIx,
1832	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
rod	rod	k1gInSc1
Taeniura	Taeniur	k1gMnSc2
</s>
<s>
trnucha	trnucha	k1gFnSc1
středomořská	středomořský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Taeniura	Taeniura	k1gFnSc1
grabata	grabata	k1gFnSc1
(	(	kIx(
<g/>
É.	É.	kA
Geoffroy	Geoffroa	k1gFnSc2
Saint-Hilaire	Saint-Hilair	k1gInSc5
<g/>
,	,	kIx,
1817	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
trnucha	trnucha	k1gFnSc1
modroskvrnná	modroskvrnný	k2eAgFnSc1d1
<g/>
,	,	kIx,
Taeniura	Taeniura	k1gFnSc1
lymma	lymma	k1gFnSc1
(	(	kIx(
<g/>
Forsskå	Forsskå	k1gMnSc1
<g/>
,	,	kIx,
1775	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
trnucha	trnucha	k1gFnSc1
indonéská	indonéský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Taeniura	Taeniura	k1gFnSc1
meyeni	meyit	k5eAaBmNgMnP,k5eAaImNgMnP,k5eAaPmNgMnP
(	(	kIx(
<g/>
Müller	Müller	k1gMnSc1
&	&	k?
Henle	Henle	k1gInSc1
<g/>
,	,	kIx,
1841	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
rod	rod	k1gInSc1
Urogymnus	Urogymnus	k1gInSc1
</s>
<s>
trnucha	trnucha	k1gFnSc1
indická	indický	k2eAgFnSc1d1
<g/>
,	,	kIx,
Urogymnus	Urogymnus	k1gMnSc1
asperrimus	asperrimus	k1gMnSc1
(	(	kIx(
<g/>
Bloch	Bloch	k1gMnSc1
&	&	k?
Schneider	Schneider	k1gMnSc1
<g/>
,	,	kIx,
1801	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Urogymnus	Urogymnus	k1gInSc1
ukpam	ukpam	k1gInSc1
(	(	kIx(
<g/>
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
1863	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Stingray	Stingraa	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Last	Last	k1gInSc1
<g/>
,	,	kIx,
Manjaji-Matsumoto	Manjaji-Matsumota	k1gFnSc5
&	&	k?
Kailola	Kailola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Himantura	Himantura	k1gFnSc1
hortlei	hortle	k1gFnSc2
n.	n.	k?
sp	sp	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
a	a	k8xC
new	new	k?
species	species	k1gFnSc2
of	of	k?
whipray	whipraa	k1gFnSc2
(	(	kIx(
<g/>
Myliobatiformes	Myliobatiformes	k1gInSc1
<g/>
:	:	kIx,
Dasyatidae	Dasyatidae	k1gFnSc1
<g/>
)	)	kIx)
from	from	k1gMnSc1
Irian	Irian	k1gMnSc1
Jaya	Jaya	k1gMnSc1
<g/>
,	,	kIx,
Indonesia	Indonesia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zootaxa	Zootaxum	k1gNnPc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
1239	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
19	#num#	k4
<g/>
–	–	k?
<g/>
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Manjaji-Matsumoto	Manjaji-Matsumota	k1gFnSc5
&	&	k?
Last	Lasta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Himantura	Himantura	k1gFnSc1
lobistoma	lobistoma	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
new	new	k?
whipray	whipraa	k1gFnSc2
(	(	kIx(
<g/>
Rajiformes	Rajiformes	k1gInSc1
<g/>
:	:	kIx,
Dasyatidae	Dasyatidae	k1gInSc1
<g/>
)	)	kIx)
from	from	k6eAd1
Borneo	Borneo	k1gNnSc1
<g/>
,	,	kIx,
with	with	k1gMnSc1
comments	comments	k6eAd1
on	on	k3xPp3gMnSc1
the	the	k?
status	status	k1gInSc4
of	of	k?
Dasyatis	Dasyatis	k1gInSc1
microphthalmus	microphthalmus	k1gMnSc1
<g/>
..	..	k?
Ichthyological	Ichthyological	k1gMnSc1
Research	Research	k1gMnSc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
53	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
291	#num#	k4
<g/>
ff	ff	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
10228	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
350	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Roberts	Roberts	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Makararaja	Makararaja	k1gFnSc1
chindwinensis	chindwinensis	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
new	new	k?
genus	genus	k1gInSc1
and	and	k?
species	species	k1gFnSc2
of	of	k?
freshwater	freshwater	k1gInSc1
dasyatidid	dasyatidida	k1gFnPc2
stingray	stingraa	k1gFnSc2
from	from	k1gMnSc1
upper	upper	k1gMnSc1
Myanmar	Myanmar	k1gMnSc1
<g/>
..	..	k?
The	The	k1gMnSc1
Natural	Natural	k?
History	Histor	k1gInPc1
Bulletin	bulletin	k1gInSc1
of	of	k?
the	the	k?
Siam	Siam	k1gInSc4
Society	societa	k1gFnSc2
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
54	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
285	#num#	k4
<g/>
–	–	k?
<g/>
293	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.sfu.ca/biology/faculty/dill/publications/417.pdf	http://www.sfu.ca/biology/faculty/dill/publications/417.pdf	k1gInSc1
<g/>
↑	↑	k?
Last	Last	k1gInSc4
<g/>
,	,	kIx,
Manjaji	Manjaje	k1gFnSc4
&	&	k?
Yearsley	Yearslea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pastinachus	Pastinachus	k1gInSc1
solocirostris	solocirostris	k1gFnSc2
sp	sp	k?
<g/>
.	.	kIx.
nov	nov	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
a	a	k8xC
new	new	k?
species	species	k1gFnSc2
of	of	k?
Stingray	Stingraa	k1gFnSc2
(	(	kIx(
<g/>
Elasmobranchii	Elasmobranchie	k1gFnSc4
<g/>
:	:	kIx,
Myliobatiformes	Myliobatiformes	k1gInSc4
<g/>
)	)	kIx)
from	from	k6eAd1
the	the	k?
Indo-Malay	Indo-Malay	k1gInPc4
Archipelago	Archipelago	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zootaxa	Zootaxum	k1gNnPc1
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
1040	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
–	–	k?
<g/>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
trnuchovití	trnuchovitý	k2eAgMnPc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Dasyatidae	Dasyatidae	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Živočichové	živočich	k1gMnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4508877-9	4508877-9	k4
</s>
