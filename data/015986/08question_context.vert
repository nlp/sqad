<s>
Trnuchovití	Trnuchovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Dasyatidae	Dasyatidae	k1gNnSc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
čeleď	čeleď	k1gFnSc1
paryb	paryba	k1gFnPc2
z	z	k7c2
řádu	řád	k1gInSc2
pravých	pravý	k2eAgMnPc2d1
rejnoků	rejnok	k1gMnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
typická	typický	k2eAgFnSc1d1
tvarem	tvar	k1gInSc7
svého	svůj	k3xOyFgNnSc2
těla	tělo	k1gNnSc2
a	a	k8xC
pohybem	pohyb	k1gInSc7
<g/>
,	,	kIx,
zdánlivě	zdánlivě	k6eAd1
připomínajícím	připomínající	k2eAgInSc6d1
ptačí	ptačit	k5eAaImIp3nS
let	let	k1gInSc1
<g/>
.	.	kIx.
</s>