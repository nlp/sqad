<s>
Nejstarší	starý	k2eAgInPc4d3	nejstarší
doklady	doklad	k1gInPc4	doklad
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
pocházejí	pocházet	k5eAaImIp3nP	pocházet
už	už	k6eAd1	už
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnPc4	tisíciletí
př.n.l.	př.n.l.	k?	př.n.l.
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
cca	cca	kA	cca
1200	[number]	k4	1200
př.n.l.	př.n.l.	k?	př.n.l.
</s>
