<p>
<s>
Madeira	Madeira	k1gFnSc1	Madeira
(	(	kIx(	(
<g/>
portugalsky	portugalsky	k6eAd1	portugalsky
Ilha	Ilh	k2eAgFnSc1d1	Ilha
da	da	k?	da
Madeira	Madeira	k1gFnSc1	Madeira
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
portugalský	portugalský	k2eAgInSc4d1	portugalský
ostrov	ostrov	k1gInSc4	ostrov
ležící	ležící	k2eAgInSc4d1	ležící
v	v	k7c6	v
Atlantickém	atlantický	k2eAgInSc6d1	atlantický
oceánu	oceán	k1gInSc6	oceán
asi	asi	k9	asi
580	[number]	k4	580
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Maroka	Maroko	k1gNnSc2	Maroko
a	a	k8xC	a
980	[number]	k4	980
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Lisabonu	Lisabon	k1gInSc2	Lisabon
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Madeirského	madeirský	k2eAgNnSc2d1	Madeirské
souostroví	souostroví	k1gNnSc2	souostroví
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
tvoří	tvořit	k5eAaImIp3nS	tvořit
součást	součást	k1gFnSc4	součást
Makaronézie	Makaronézie	k1gFnSc2	Makaronézie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rozloze	rozloha	k1gFnSc6	rozloha
750,7	[number]	k4	750,7
km2	km2	k4	km2
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
Národního	národní	k2eAgInSc2d1	národní
svazu	svaz	k1gInSc2	svaz
obcí	obec	k1gFnPc2	obec
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
244	[number]	k4	244
286	[number]	k4	286
stálých	stálý	k2eAgMnPc2d1	stálý
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
100	[number]	k4	100
847	[number]	k4	847
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Funchal	Funchal	k1gFnSc2	Funchal
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
souostroví	souostroví	k1gNnSc1	souostroví
má	mít	k5eAaImIp3nS	mít
status	status	k1gInSc4	status
portugalské	portugalský	k2eAgFnSc2d1	portugalská
autonomní	autonomní	k2eAgFnSc2d1	autonomní
oblasti	oblast	k1gFnSc2	oblast
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
a	a	k8xC	a
jako	jako	k8xS	jako
takové	takový	k3xDgInPc4	takový
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Administrativně	administrativně	k6eAd1	administrativně
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc1	ostrov
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
10	[number]	k4	10
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
municipios	municipios	k1gInSc1	municipios
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Starověk	starověk	k1gInSc1	starověk
a	a	k8xC	a
středověk	středověk	k1gInSc1	středověk
===	===	k?	===
</s>
</p>
<p>
<s>
O	o	k7c6	o
existenci	existence	k1gFnSc6	existence
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Atlantiku	Atlantik	k1gInSc6	Atlantik
západně	západně	k6eAd1	západně
od	od	k7c2	od
Gibraltaru	Gibraltar	k1gInSc2	Gibraltar
se	se	k3xPyFc4	se
vědělo	vědět	k5eAaImAgNnS	vědět
již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
(	(	kIx(	(
<g/>
Féničané	Féničan	k1gMnPc1	Féničan
<g/>
,	,	kIx,	,
Řekové	Řek	k1gMnPc1	Řek
<g/>
,	,	kIx,	,
Římané	Říman	k1gMnPc1	Říman
-	-	kIx~	-
Plinius	Plinius	k1gMnSc1	Plinius
Starší	starší	k1gMnSc1	starší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byla	být	k5eAaImAgFnS	být
Madeira	Madeira	k1gFnSc1	Madeira
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1351	[number]	k4	1351
uváděna	uvádět	k5eAaImNgFnS	uvádět
na	na	k7c6	na
italských	italský	k2eAgFnPc6d1	italská
mapách	mapa	k1gFnPc6	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgNnSc4d1	Malé
Portugalsko	Portugalsko	k1gNnSc4	Portugalsko
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
hledalo	hledat	k5eAaImAgNnS	hledat
zdroje	zdroj	k1gInPc1	zdroj
svého	svůj	k3xOyFgInSc2	svůj
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Nezastupitelnou	zastupitelný	k2eNgFnSc4d1	nezastupitelná
roli	role	k1gFnSc4	role
sehrály	sehrát	k5eAaPmAgFnP	sehrát
portugalské	portugalský	k2eAgInPc4d1	portugalský
zámořské	zámořský	k2eAgInPc4d1	zámořský
objevy	objev	k1gInPc4	objev
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
osobností	osobnost	k1gFnSc7	osobnost
byl	být	k5eAaImAgMnS	být
princ	princ	k1gMnSc1	princ
Jindřich	Jindřich	k1gMnSc1	Jindřich
(	(	kIx(	(
<g/>
1394	[number]	k4	1394
<g/>
–	–	k?	–
<g/>
1460	[number]	k4	1460
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
Jindřich	Jindřich	k1gMnSc1	Jindřich
Mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
mladý	mladý	k2eAgMnSc1d1	mladý
<g/>
,	,	kIx,	,
shromáždil	shromáždit	k5eAaPmAgInS	shromáždit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
portugalské	portugalský	k2eAgMnPc4d1	portugalský
kartografy	kartograf	k1gMnPc4	kartograf
a	a	k8xC	a
mořeplavce	mořeplavec	k1gMnPc4	mořeplavec
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
rozšířit	rozšířit	k5eAaPmF	rozšířit
znalosti	znalost	k1gFnSc3	znalost
o	o	k7c6	o
západoafrickém	západoafrický	k2eAgNnSc6d1	západoafrické
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
ovládnout	ovládnout	k5eAaPmF	ovládnout
obchod	obchod	k1gInSc4	obchod
a	a	k8xC	a
najít	najít	k5eAaPmF	najít
námořní	námořní	k2eAgFnSc4d1	námořní
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
první	první	k4xOgFnSc6	první
cestě	cesta	k1gFnSc6	cesta
více	hodně	k6eAd2	hodně
zapracovala	zapracovat	k5eAaPmAgFnS	zapracovat
náhoda	náhoda	k1gFnSc1	náhoda
a	a	k8xC	a
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
veleli	velet	k5eAaImAgMnP	velet
kapitáni	kapitán	k1gMnPc1	kapitán
Joã	Joã	k1gMnSc1	Joã
Gonçalves	Gonçalves	k1gMnSc1	Gonçalves
Zarco	Zarco	k1gMnSc1	Zarco
a	a	k8xC	a
Tristã	Tristã	k1gMnSc1	Tristã
Vaz	vaz	k1gInSc4	vaz
Teixeira	Teixeiro	k1gNnSc2	Teixeiro
<g/>
,	,	kIx,	,
zanesla	zanést	k5eAaPmAgFnS	zanést
bouře	bouře	k1gFnSc1	bouře
až	až	k9	až
k	k	k7c3	k
neobydlenému	obydlený	k2eNgInSc3d1	neobydlený
ostrovu	ostrov	k1gInSc3	ostrov
Porto	porto	k1gNnSc4	porto
Santo	Santo	k1gNnSc4	Santo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
úplně	úplně	k6eAd1	úplně
první	první	k4xOgInSc1	první
výsledek	výsledek	k1gInSc1	výsledek
cest	cesta	k1gFnPc2	cesta
organizovaných	organizovaný	k2eAgFnPc2d1	organizovaná
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Mořeplavcem	mořeplavec	k1gMnSc7	mořeplavec
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgInS	psát
se	se	k3xPyFc4	se
rok	rok	k1gInSc1	rok
1418	[number]	k4	1418
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
Jindřich	Jindřich	k1gMnSc1	Jindřich
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
vyslal	vyslat	k5eAaPmAgMnS	vyslat
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
lodě	loď	k1gFnSc2	loď
znovu	znovu	k6eAd1	znovu
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
ostrov	ostrov	k1gInSc4	ostrov
osídlit	osídlit	k5eAaPmF	osídlit
a	a	k8xC	a
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1419	[number]	k4	1419
Portugalci	Portugalec	k1gMnPc1	Portugalec
vylodili	vylodit	k5eAaPmAgMnP	vylodit
na	na	k7c6	na
Madeiře	Madeira	k1gFnSc6	Madeira
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
u	u	k7c2	u
dnešního	dnešní	k2eAgInSc2d1	dnešní
Machica	Machic	k1gInSc2	Machic
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
název	název	k1gInSc1	název
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
portugalského	portugalský	k2eAgInSc2d1	portugalský
"	"	kIx"	"
<g/>
ilha	ilh	k1gInSc2	ilh
da	da	k?	da
madeira	madeira	k1gFnSc1	madeira
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
ostrov	ostrov	k1gInSc1	ostrov
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Madeira	Madeira	k1gFnSc1	Madeira
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
neobydlená	obydlený	k2eNgFnSc1d1	neobydlená
<g/>
,	,	kIx,	,
hustě	hustě	k6eAd1	hustě
zalesněná	zalesněný	k2eAgFnSc1d1	zalesněná
(	(	kIx(	(
<g/>
převládaly	převládat	k5eAaImAgFnP	převládat
vavřínové	vavřínový	k2eAgInPc4d1	vavřínový
lesy	les	k1gInPc4	les
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
pobřeží	pobřeží	k1gNnSc1	pobřeží
bylo	být	k5eAaImAgNnS	být
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
nepřístupné	přístupný	k2eNgFnSc2d1	nepřístupná
kvůli	kvůli	k7c3	kvůli
strmým	strmý	k2eAgInPc3d1	strmý
útesům	útes	k1gInPc3	útes
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1420	[number]	k4	1420
začíná	začínat	k5eAaImIp3nS	začínat
vypalování	vypalování	k1gNnSc4	vypalování
lesů	les	k1gInPc2	les
a	a	k8xC	a
osidlování	osidlování	k1gNnSc2	osidlování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
skončilo	skončit	k5eAaPmAgNnS	skončit
roku	rok	k1gInSc3	rok
1433	[number]	k4	1433
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
Machico	Machico	k6eAd1	Machico
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1425	[number]	k4	1425
byl	být	k5eAaImAgInS	být
ostrov	ostrov	k1gInSc1	ostrov
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
tak	tak	k6eAd1	tak
zvané	zvaný	k2eAgFnPc4d1	zvaná
kapitánie	kapitánie	k1gFnPc4	kapitánie
Machico	Machico	k6eAd1	Machico
a	a	k8xC	a
Funchal	Funchal	k1gFnPc1	Funchal
(	(	kIx(	(
<g/>
třetí	třetí	k4xOgFnSc1	třetí
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
Porto	porto	k1gNnSc4	porto
Santu	Sant	k1gInSc2	Sant
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
pěstování	pěstování	k1gNnSc4	pěstování
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
.	.	kIx.	.
</s>
<s>
Osidlování	osidlování	k1gNnSc1	osidlování
a	a	k8xC	a
zavádění	zavádění	k1gNnSc1	zavádění
zemědělství	zemědělství	k1gNnSc2	zemědělství
neprobíhá	probíhat	k5eNaImIp3nS	probíhat
živelně	živelně	k6eAd1	živelně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
organizováno	organizovat	k5eAaBmNgNnS	organizovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zmíněném	zmíněný	k2eAgInSc6d1	zmíněný
roce	rok	k1gInSc6	rok
1425	[number]	k4	1425
již	již	k6eAd1	již
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
existuje	existovat	k5eAaImIp3nS	existovat
150	[number]	k4	150
zemědělských	zemědělský	k2eAgNnPc2d1	zemědělské
hospodářství	hospodářství	k1gNnPc2	hospodářství
a	a	k8xC	a
produkce	produkce	k1gFnSc1	produkce
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
vyvážena	vyvážit	k5eAaPmNgFnS	vyvážit
do	do	k7c2	do
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Novověk	novověk	k1gInSc4	novověk
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1497	[number]	k4	1497
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
5000	[number]	k4	5000
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
vyhlášen	vyhlášen	k2eAgMnSc1d1	vyhlášen
Funchal	Funchal	k1gMnSc1	Funchal
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
na	na	k7c4	na
město	město	k1gNnSc4	město
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1508	[number]	k4	1508
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1514	[number]	k4	1514
je	být	k5eAaImIp3nS	být
Funchal	Funchal	k1gFnSc1	Funchal
sídlem	sídlo	k1gNnSc7	sídlo
biskupa	biskup	k1gInSc2	biskup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1452	[number]	k4	1452
se	se	k3xPyFc4	se
na	na	k7c4	na
Madeiru	Madeira	k1gFnSc4	Madeira
začali	začít	k5eAaPmAgMnP	začít
dovážet	dovážet	k5eAaImF	dovážet
otroci	otrok	k1gMnPc1	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Cukrovarnictví	cukrovarnictví	k1gNnSc1	cukrovarnictví
prosperovalo	prosperovat	k5eAaImAgNnS	prosperovat
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
třtinové	třtinový	k2eAgFnPc1d1	třtinová
plantáže	plantáž	k1gFnPc1	plantáž
na	na	k7c6	na
Svatém	svatý	k1gMnSc6	svatý
Tomáši	Tomáš	k1gMnSc6	Tomáš
a	a	k8xC	a
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
řada	řada	k1gFnSc1	řada
pěstitelů	pěstitel	k1gMnPc2	pěstitel
z	z	k7c2	z
Madeiry	Madeira	k1gFnSc2	Madeira
emigrovala	emigrovat	k5eAaBmAgFnS	emigrovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1580	[number]	k4	1580
až	až	k9	až
1640	[number]	k4	1640
vládli	vládnout	k5eAaImAgMnP	vládnout
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
španělští	španělský	k2eAgMnPc1d1	španělský
Habsburkové	Habsburk	k1gMnPc1	Habsburk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
do	do	k7c2	do
hospodářství	hospodářství	k1gNnSc2	hospodářství
zavedli	zavést	k5eAaPmAgMnP	zavést
tvrdá	tvrdý	k2eAgNnPc4d1	tvrdé
koloniální	koloniální	k2eAgNnPc4d1	koloniální
opatření	opatření	k1gNnPc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
trpěla	trpět	k5eAaImAgFnS	trpět
i	i	k9	i
Madeira	Madeira	k1gFnSc1	Madeira
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1640	[number]	k4	1640
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
30	[number]	k4	30
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
rokem	rok	k1gInSc7	rok
nadvláda	nadvláda	k1gFnSc1	nadvláda
Španělska	Španělsko	k1gNnSc2	Španělsko
končí	končit	k5eAaImIp3nS	končit
a	a	k8xC	a
rýsují	rýsovat	k5eAaImIp3nP	rýsovat
se	se	k3xPyFc4	se
také	také	k9	také
dobré	dobrý	k2eAgInPc1d1	dobrý
vztahy	vztah	k1gInPc1	vztah
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
vývoz	vývoz	k1gInSc1	vývoz
vína	víno	k1gNnSc2	víno
z	z	k7c2	z
Madeiry	Madeira	k1gFnSc2	Madeira
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
45	[number]	k4	45
000	[number]	k4	000
sudů	sud	k1gInPc2	sud
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
Madeira	Madeira	k1gFnSc1	Madeira
není	být	k5eNaImIp3nS	být
potravinově	potravinově	k6eAd1	potravinově
soběstačná	soběstačný	k2eAgFnSc1d1	soběstačná
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
obyvatel	obyvatel	k1gMnPc2	obyvatel
emigruje	emigrovat	k5eAaBmIp3nS	emigrovat
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
do	do	k7c2	do
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
pohyb	pohyb	k1gInSc1	pohyb
obyvatel	obyvatel	k1gMnPc2	obyvatel
začal	začít	k5eAaPmAgInS	začít
po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
otroctví	otroctví	k1gNnSc2	otroctví
v	v	k7c6	v
anglických	anglický	k2eAgFnPc6d1	anglická
a	a	k8xC	a
francouzských	francouzský	k2eAgFnPc6d1	francouzská
koloniích	kolonie	k1gFnPc6	kolonie
(	(	kIx(	(
<g/>
1834	[number]	k4	1834
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
přicházejí	přicházet	k5eAaImIp3nP	přicházet
dělníci	dělník	k1gMnPc1	dělník
z	z	k7c2	z
plantáží	plantáž	k1gFnPc2	plantáž
v	v	k7c6	v
Karibiku	Karibikum	k1gNnSc6	Karibikum
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Madeiry	Madeira	k1gFnSc2	Madeira
však	však	k9	však
začínají	začínat	k5eAaImIp3nP	začínat
emigrovat	emigrovat	k5eAaBmF	emigrovat
do	do	k7c2	do
neportugalských	portugalský	k2eNgFnPc2d1	portugalský
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
se	se	k3xPyFc4	se
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
datuje	datovat	k5eAaImIp3nS	datovat
krajkářství	krajkářství	k1gNnSc1	krajkářství
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
zdrojů	zdroj	k1gInPc2	zdroj
příjmu	příjem	k1gInSc2	příjem
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začínají	začínat	k5eAaImIp3nP	začínat
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
přicházet	přicházet	k5eAaImF	přicházet
první	první	k4xOgMnPc1	první
turisté	turist	k1gMnPc1	turist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
měla	mít	k5eAaImAgFnS	mít
Madeira	Madeira	k1gFnSc1	Madeira
132	[number]	k4	132
223	[number]	k4	223
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
pěstování	pěstování	k1gNnSc1	pěstování
banánů	banán	k1gInPc2	banán
jako	jako	k8xS	jako
doplňkové	doplňkový	k2eAgFnPc4d1	doplňková
plodiny	plodina	k1gFnPc4	plodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
začíná	začínat	k5eAaImIp3nS	začínat
elektrifikace	elektrifikace	k1gFnSc1	elektrifikace
Madeiry	Madeira	k1gFnSc2	Madeira
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Madeiry	Madeira	k1gFnPc1	Madeira
se	se	k3xPyFc4	se
však	však	k9	však
válka	válka	k1gFnSc1	válka
příliš	příliš	k6eAd1	příliš
nedotkla	dotknout	k5eNaPmAgFnS	dotknout
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgFnPc1d1	německá
ponorky	ponorka	k1gFnPc1	ponorka
jednou	jednou	k6eAd1	jednou
ostřelovaly	ostřelovat	k5eAaImAgFnP	ostřelovat
funchalský	funchalský	k2eAgInSc4d1	funchalský
přístav	přístav	k1gInSc4	přístav
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bez	bez	k7c2	bez
větších	veliký	k2eAgFnPc2d2	veliký
škod	škoda	k1gFnPc2	škoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
sice	sice	k8xC	sice
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
neutrální	neutrální	k2eAgNnSc1d1	neutrální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nabídlo	nabídnout	k5eAaPmAgNnS	nabídnout
Velké	velký	k2eAgFnSc3d1	velká
Británii	Británie	k1gFnSc3	Británie
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
své	svůj	k3xOyFgInPc4	svůj
přístavy	přístav	k1gInPc4	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
na	na	k7c6	na
Madeiře	Madeira	k1gFnSc6	Madeira
nebojovalo	bojovat	k5eNaImAgNnS	bojovat
<g/>
,	,	kIx,	,
portugalská	portugalský	k2eAgFnSc1d1	portugalská
armáda	armáda	k1gFnSc1	armáda
prováděla	provádět	k5eAaImAgFnS	provádět
opevňovací	opevňovací	k2eAgFnSc1d1	opevňovací
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
obranných	obranný	k2eAgNnPc2d1	obranné
zařízení	zařízení	k1gNnPc2	zařízení
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
dnes	dnes	k6eAd1	dnes
viditelné	viditelný	k2eAgFnPc1d1	viditelná
ve	v	k7c6	v
skalních	skalní	k2eAgFnPc6d1	skalní
dutinách	dutina	k1gFnPc6	dutina
na	na	k7c6	na
konci	konec	k1gInSc6	konec
pláže	pláž	k1gFnSc2	pláž
Formosa	Formosa	k1gFnSc1	Formosa
u	u	k7c2	u
Funchalu	Funchal	k1gInSc2	Funchal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
obživy	obživa	k1gFnSc2	obživa
obyvatel	obyvatel	k1gMnPc2	obyvatel
Madeiry	Madeira	k1gFnSc2	Madeira
jsou	být	k5eAaImIp3nP	být
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
krajkářství	krajkářství	k1gNnSc2	krajkářství
a	a	k8xC	a
košíkářství	košíkářství	k1gNnSc2	košíkářství
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
jezdili	jezdit	k5eAaImAgMnP	jezdit
na	na	k7c4	na
Madeiru	Madeira	k1gFnSc4	Madeira
hlavně	hlavně	k9	hlavně
Angličané	Angličan	k1gMnPc1	Angličan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
začali	začít	k5eAaPmAgMnP	začít
kupovat	kupovat	k5eAaImF	kupovat
pozemky	pozemek	k1gInPc4	pozemek
a	a	k8xC	a
podnikat	podnikat	k5eAaImF	podnikat
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
vybudován	vybudován	k2eAgInSc1d1	vybudován
honosný	honosný	k2eAgInSc1d1	honosný
hotel	hotel	k1gInSc1	hotel
Reid	Reid	k1gInSc1	Reid
<g/>
́	́	k?	́
<g/>
s.	s.	k?	s.
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
řada	řada	k1gFnSc1	řada
hotelů	hotel	k1gInPc2	hotel
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc2d2	vyšší
cenové	cenový	k2eAgFnSc2d1	cenová
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
Funchalu	Funchal	k1gInSc6	Funchal
<g/>
.	.	kIx.	.
</s>
<s>
Madeira	Madeira	k1gFnSc1	Madeira
je	být	k5eAaImIp3nS	být
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
mj.	mj.	kA	mj.
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
bujnou	bujný	k2eAgFnSc4d1	bujná
vegetaci	vegetace	k1gFnSc4	vegetace
(	(	kIx(	(
<g/>
říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
ostrov	ostrov	k1gInSc4	ostrov
květin	květina	k1gFnPc2	květina
<g/>
)	)	kIx)	)
a	a	k8xC	a
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
ji	on	k3xPp3gFnSc4	on
hojně	hojně	k6eAd1	hojně
turisté	turist	k1gMnPc1	turist
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
EU	EU	kA	EU
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
Angličané	Angličan	k1gMnPc1	Angličan
<g/>
,	,	kIx,	,
Holanďané	Holanďan	k1gMnPc1	Holanďan
a	a	k8xC	a
Skandinávci	Skandinávec	k1gMnPc1	Skandinávec
včetně	včetně	k7c2	včetně
Norů	Nor	k1gMnPc2	Nor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
produkuje	produkovat	k5eAaImIp3nS	produkovat
díky	díky	k7c3	díky
úrodné	úrodný	k2eAgFnSc3d1	úrodná
půdě	půda	k1gFnSc3	půda
<g/>
,	,	kIx,	,
dostatku	dostatek	k1gInSc3	dostatek
vody	voda	k1gFnSc2	voda
dodávané	dodávaný	k2eAgFnPc1d1	dodávaná
levádami	leváda	k1gFnPc7	leváda
a	a	k8xC	a
příznivému	příznivý	k2eAgNnSc3d1	příznivé
klimatu	klima	k1gNnSc3	klima
kromě	kromě	k7c2	kromě
vína	víno	k1gNnSc2	víno
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgFnPc4	všechen
potřebné	potřebný	k2eAgFnPc4d1	potřebná
potraviny	potravina	k1gFnPc4	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
založeno	založit	k5eAaPmNgNnS	založit
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
ruční	ruční	k2eAgFnSc4d1	ruční
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
nízkou	nízký	k2eAgFnSc7d1	nízká
produktivitou	produktivita	k1gFnSc7	produktivita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náhorní	náhorní	k2eAgFnSc6d1	náhorní
plošině	plošina	k1gFnSc6	plošina
se	se	k3xPyFc4	se
volně	volně	k6eAd1	volně
pasou	pást	k5eAaImIp3nP	pást
krávy	kráva	k1gFnPc1	kráva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výrobu	výroba	k1gFnSc4	výroba
krajek	krajka	k1gFnPc2	krajka
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
domácí	domácí	k2eAgFnSc1d1	domácí
<g/>
,	,	kIx,	,
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
krajkářský	krajkářský	k2eAgInSc1d1	krajkářský
ústav	ústav	k1gInSc1	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
ručně	ručně	k6eAd1	ručně
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
krajka	krajka	k1gFnSc1	krajka
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
opatřena	opatřit	k5eAaPmNgFnS	opatřit
značkou	značka	k1gFnSc7	značka
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
košíkářství	košíkářství	k1gNnSc2	košíkářství
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Camacha	Camach	k1gMnSc2	Camach
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Elektrifikace	elektrifikace	k1gFnSc2	elektrifikace
==	==	k?	==
</s>
</p>
<p>
<s>
Madeira	Madeira	k1gFnSc1	Madeira
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
elektrifikovaná	elektrifikovaný	k2eAgFnSc1d1	elektrifikovaná
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
energie	energie	k1gFnSc2	energie
je	být	k5eAaImIp3nS	být
tepelná	tepelný	k2eAgFnSc1d1	tepelná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Vitória	Vitórium	k1gNnSc2	Vitórium
u	u	k7c2	u
Câmara	Câmara	k1gFnSc1	Câmara
de	de	k?	de
Lobos	Lobos	k1gInSc1	Lobos
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
výkon	výkon	k1gInSc1	výkon
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
MW	MW	kA	MW
<g/>
)	)	kIx)	)
stabilizuje	stabilizovat	k5eAaBmIp3nS	stabilizovat
frekvenci	frekvence	k1gFnSc4	frekvence
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
měla	mít	k5eAaImAgFnS	mít
elektrárna	elektrárna	k1gFnSc1	elektrárna
postupně	postupně	k6eAd1	postupně
přejít	přejít	k5eAaPmF	přejít
od	od	k7c2	od
nafty	nafta	k1gFnSc2	nafta
ke	k	k7c3	k
spalování	spalování	k1gNnSc3	spalování
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
řekách	řeka	k1gFnPc6	řeka
a	a	k8xC	a
levádách	leváda	k1gFnPc6	leváda
řada	řada	k1gFnSc1	řada
hydroelektráren	hydroelektrárna	k1gFnPc2	hydroelektrárna
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
zachytává	zachytávat	k5eAaImIp3nS	zachytávat
v	v	k7c6	v
pomocné	pomocný	k2eAgFnSc6d1	pomocná
nádrži	nádrž	k1gFnSc6	nádrž
a	a	k8xC	a
elektrárna	elektrárna	k1gFnSc1	elektrárna
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
špičková	špičkový	k2eAgFnSc1d1	špičková
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náhorní	náhorní	k2eAgFnSc6d1	náhorní
planině	planina	k1gFnSc6	planina
a	a	k8xC	a
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
jsou	být	k5eAaImIp3nP	být
instalovány	instalovat	k5eAaBmNgFnP	instalovat
větrné	větrný	k2eAgFnPc1d1	větrná
elektrárny	elektrárna	k1gFnPc1	elektrárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
sítě	síť	k1gFnSc2	síť
silnic	silnice	k1gFnPc2	silnice
budovaných	budovaný	k2eAgFnPc2d1	budovaná
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dnes	dnes	k6eAd1	dnes
na	na	k7c6	na
Madeiře	Madeira	k1gFnSc6	Madeira
existuje	existovat	k5eAaImIp3nS	existovat
moderní	moderní	k2eAgFnSc4d1	moderní
dálniční	dálniční	k2eAgFnSc4d1	dálniční
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dálnicích	dálnice	k1gFnPc6	dálnice
jsou	být	k5eAaImIp3nP	být
desítky	desítka	k1gFnPc1	desítka
kilometrů	kilometr	k1gInPc2	kilometr
tunelů	tunel	k1gInPc2	tunel
a	a	k8xC	a
odvážné	odvážný	k2eAgInPc1d1	odvážný
mosty	most	k1gInPc1	most
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Funchalu	Funchal	k1gInSc6	Funchal
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
kabinové	kabinový	k2eAgFnPc4d1	kabinová
lanovky	lanovka	k1gFnPc4	lanovka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
delší	dlouhý	k2eAgFnSc1d2	delší
dopraví	dopravit	k5eAaPmIp3nS	dopravit
cestující	cestující	k1gFnSc1	cestující
od	od	k7c2	od
nábřeží	nábřeží	k1gNnSc2	nábřeží
do	do	k7c2	do
čtvrti	čtvrt	k1gFnSc2	čtvrt
Monte	Mont	k1gInSc5	Mont
(	(	kIx(	(
<g/>
konečná	konečný	k2eAgFnSc1d1	konečná
stanice	stanice	k1gFnSc1	stanice
lanovky	lanovka	k1gFnSc2	lanovka
550	[number]	k4	550
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
kratší	krátký	k2eAgFnSc1d2	kratší
lanovka	lanovka	k1gFnSc1	lanovka
z	z	k7c2	z
Monte	Mont	k1gInSc5	Mont
překračuje	překračovat	k5eAaImIp3nS	překračovat
hluboké	hluboký	k2eAgNnSc4d1	hluboké
údolí	údolí	k1gNnSc4	údolí
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Botanické	botanický	k2eAgFnSc2d1	botanická
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
Funchalu	Funchal	k1gInSc6	Funchal
jediný	jediný	k2eAgInSc1d1	jediný
kolejový	kolejový	k2eAgInSc1d1	kolejový
spoj	spoj	k1gInSc1	spoj
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
přístavu	přístav	k1gInSc2	přístav
na	na	k7c4	na
Monte	Mont	k1gMnSc5	Mont
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
již	již	k9	již
tato	tento	k3xDgFnSc1	tento
trať	trať	k1gFnSc1	trať
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvním	první	k4xOgNnSc7	první
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
přiletělo	přiletět	k5eAaPmAgNnS	přiletět
na	na	k7c4	na
Madeiru	Madeira	k1gFnSc4	Madeira
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1921	[number]	k4	1921
hydroplán	hydroplán	k1gInSc1	hydroplán
Rolls	Rolls	k1gInSc4	Rolls
Royce	Royce	k1gFnSc2	Royce
typu	typ	k1gInSc2	typ
F-3	F-3	k1gFnSc2	F-3
se	s	k7c7	s
čtyřčlennou	čtyřčlenný	k2eAgFnSc7d1	čtyřčlenná
posádkou	posádka	k1gFnSc7	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
z	z	k7c2	z
Lisabonu	Lisabon	k1gInSc2	Lisabon
trvala	trvat	k5eAaImAgFnS	trvat
asi	asi	k9	asi
20	[number]	k4	20
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Hydroplán	hydroplán	k1gInSc1	hydroplán
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
přistál	přistát	k5eAaImAgInS	přistát
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
u	u	k7c2	u
přístavu	přístav	k1gInSc2	přístav
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
řízen	řídit	k5eAaImNgInS	řídit
kapitánem	kapitán	k1gMnSc7	kapitán
jménem	jméno	k1gNnSc7	jméno
Gago	Gago	k1gMnSc1	Gago
Coutinho	Coutin	k1gMnSc4	Coutin
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pevné	pevný	k2eAgFnSc6d1	pevná
zemi	zem	k1gFnSc6	zem
přistálo	přistát	k5eAaImAgNnS	přistát
první	první	k4xOgNnSc1	první
letadlo	letadlo	k1gNnSc1	letadlo
až	až	k8xS	až
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
pokusné	pokusný	k2eAgFnSc6d1	pokusná
dráze	dráha	k1gFnSc6	dráha
Santa	Sant	k1gMnSc2	Sant
Catarina	Catarin	k1gMnSc2	Catarin
a	a	k8xC	a
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
malé	malý	k2eAgNnSc4d1	malé
sportovní	sportovní	k2eAgNnSc4d1	sportovní
letadlo	letadlo	k1gNnSc4	letadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Madeirské	madeirský	k2eAgNnSc1d1	Madeirské
letiště	letiště	k1gNnSc1	letiště
Santa	Sant	k1gMnSc2	Sant
Catarina	Catarin	k1gMnSc2	Catarin
bylo	být	k5eAaImAgNnS	být
modernizováno	modernizovat	k5eAaBmNgNnS	modernizovat
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
přistávací	přistávací	k2eAgFnSc1d1	přistávací
dráha	dráha	k1gFnSc1	dráha
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
na	na	k7c4	na
2781	[number]	k4	2781
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
místa	místo	k1gNnSc2	místo
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
180	[number]	k4	180
sloupech	sloup	k1gInPc6	sloup
protažena	protáhnout	k5eAaPmNgFnS	protáhnout
nad	nad	k7c4	nad
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
letiště	letiště	k1gNnSc1	letiště
moderně	moderně	k6eAd1	moderně
vybavené	vybavený	k2eAgNnSc1d1	vybavené
<g/>
,	,	kIx,	,
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
přistání	přistání	k1gNnSc4	přistání
značnou	značný	k2eAgFnSc4d1	značná
zručnost	zručnost	k1gFnSc4	zručnost
pilotů	pilot	k1gMnPc2	pilot
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
pevné	pevný	k2eAgInPc4d1	pevný
nervy	nerv	k1gInPc4	nerv
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
letiště	letiště	k1gNnSc1	letiště
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
nedalekém	daleký	k2eNgInSc6d1	nedaleký
ostrově	ostrov	k1gInSc6	ostrov
Porto	porto	k1gNnSc1	porto
Santo	Santo	k1gNnSc4	Santo
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přistání	přistání	k1gNnSc4	přistání
vrtulníků	vrtulník	k1gInPc2	vrtulník
slouží	sloužit	k5eAaImIp3nS	sloužit
na	na	k7c6	na
Madeiře	Madeira	k1gFnSc6	Madeira
heliporty	heliport	k1gInPc4	heliport
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
cestujících	cestující	k1gMnPc2	cestující
a	a	k8xC	a
vozidel	vozidlo	k1gNnPc2	vozidlo
mezi	mezi	k7c7	mezi
Madeirou	Madeira	k1gFnSc7	Madeira
a	a	k8xC	a
Porto	porto	k1gNnSc1	porto
Santo	Sant	k2eAgNnSc1d1	Santo
slouží	sloužit	k5eAaImIp3nS	sloužit
moderní	moderní	k2eAgInSc4d1	moderní
přívoz	přívoz	k1gInSc4	přívoz
<g/>
,	,	kIx,	,
provozovaný	provozovaný	k2eAgInSc4d1	provozovaný
jedenkrát	jedenkrát	k6eAd1	jedenkrát
denně	denně	k6eAd1	denně
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
směrech	směr	k1gInPc6	směr
(	(	kIx(	(
<g/>
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
i	i	k8xC	i
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
objíždí	objíždět	k5eAaImIp3nS	objíždět
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
délka	délka	k1gFnSc1	délka
trasy	trasa	k1gFnSc2	trasa
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
směru	směr	k1gInSc6	směr
75	[number]	k4	75
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podnebí	podnebí	k1gNnSc2	podnebí
a	a	k8xC	a
vegetace	vegetace	k1gFnSc2	vegetace
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
podnebí	podnebí	k1gNnSc6	podnebí
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Madeira	Madeira	k1gFnSc1	Madeira
se	se	k3xPyFc4	se
silně	silně	k6eAd1	silně
projevuje	projevovat	k5eAaImIp3nS	projevovat
tzv.	tzv.	kA	tzv.
výšková	výškový	k2eAgFnSc1d1	výšková
pásmovitost	pásmovitost	k1gFnSc1	pásmovitost
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
kolem	kolem	k7c2	kolem
ostrova	ostrov	k1gInSc2	ostrov
Madeira	Madeira	k1gFnSc1	Madeira
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
ani	ani	k9	ani
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
neklesá	klesat	k5eNaImIp3nS	klesat
pod	pod	k7c4	pod
18	[number]	k4	18
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
poloha	poloha	k1gFnSc1	poloha
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Evropy	Evropa	k1gFnSc2	Evropa
zde	zde	k6eAd1	zde
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
do	do	k7c2	do
200	[number]	k4	200
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
pěstovat	pěstovat	k5eAaImF	pěstovat
tropické	tropický	k2eAgFnPc1d1	tropická
plodiny	plodina	k1gFnPc1	plodina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
narůstající	narůstající	k2eAgFnSc7d1	narůstající
výškou	výška	k1gFnSc7	výška
se	se	k3xPyFc4	se
struktura	struktura	k1gFnSc1	struktura
plodin	plodina	k1gFnPc2	plodina
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
200	[number]	k4	200
do	do	k7c2	do
400	[number]	k4	400
m	m	kA	m
je	být	k5eAaImIp3nS	být
klima	klima	k1gNnSc1	klima
subtropické	subtropický	k2eAgNnSc1d1	subtropické
a	a	k8xC	a
od	od	k7c2	od
400	[number]	k4	400
do	do	k7c2	do
700	[number]	k4	700
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
,	,	kIx,	,
jablka	jablko	k1gNnPc1	jablko
<g/>
,	,	kIx,	,
třešně	třešeň	k1gFnPc1	třešeň
a	a	k8xC	a
obilí	obilí	k1gNnSc1	obilí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
až	až	k6eAd1	až
do	do	k7c2	do
1000	[number]	k4	1000
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
jsou	být	k5eAaImIp3nP	být
vřesoviště	vřesoviště	k1gNnPc1	vřesoviště
<g/>
,	,	kIx,	,
rostou	růst	k5eAaImIp3nP	růst
zde	zde	k6eAd1	zde
eukalypty	eukalypt	k1gInPc4	eukalypt
a	a	k8xC	a
azorské	azorský	k2eAgInPc4d1	azorský
vavříny	vavřín	k1gInPc4	vavřín
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
1000	[number]	k4	1000
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
horské	horský	k2eAgNnSc4d1	horské
pásmo	pásmo	k1gNnSc4	pásmo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
pobyt	pobyt	k1gInSc4	pobyt
je	být	k5eAaImIp3nS	být
nejpříjemnější	příjemný	k2eAgNnSc1d3	nejpříjemnější
podnebí	podnebí	k1gNnSc1	podnebí
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Funchalu	Funchal	k1gInSc6	Funchal
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
téměř	téměř	k6eAd1	téměř
neprší	pršet	k5eNaImIp3nS	pršet
<g/>
.	.	kIx.	.
</s>
<s>
Denní	denní	k2eAgFnSc1d1	denní
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
nestoupne	stoupnout	k5eNaPmIp3nS	stoupnout
nad	nad	k7c7	nad
25	[number]	k4	25
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
noční	noční	k2eAgFnSc1d1	noční
teplota	teplota	k1gFnSc1	teplota
neklesne	klesnout	k5eNaPmIp3nS	klesnout
pod	pod	k7c4	pod
11	[number]	k4	11
°	°	k?	°
<g/>
C.	C.	kA	C.
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
denní	denní	k2eAgFnSc7d1	denní
a	a	k8xC	a
noční	noční	k2eAgFnSc7d1	noční
teplotou	teplota	k1gFnSc7	teplota
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
4	[number]	k4	4
°	°	k?	°
<g/>
C	C	kA	C
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
suchém	suchý	k2eAgInSc6d1	suchý
východě	východ	k1gInSc6	východ
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
na	na	k7c6	na
náhorní	náhorní	k2eAgFnSc6d1	náhorní
plošině	plošina	k1gFnSc6	plošina
většinou	většinou	k6eAd1	většinou
fouká	foukat	k5eAaImIp3nS	foukat
silný	silný	k2eAgInSc1d1	silný
vítr	vítr	k1gInSc1	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
často	často	k6eAd1	často
prší	pršet	k5eAaImIp3nS	pršet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Krajina	Krajina	k1gFnSc1	Krajina
a	a	k8xC	a
osídlení	osídlení	k1gNnSc1	osídlení
==	==	k?	==
</s>
</p>
<p>
<s>
Celý	celý	k2eAgInSc1d1	celý
povrch	povrch	k1gInSc1	povrch
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
hornatý	hornatý	k2eAgInSc1d1	hornatý
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
vnitrozemí	vnitrozemí	k1gNnSc4	vnitrozemí
jen	jen	k9	jen
málo	málo	k6eAd1	málo
osídlené	osídlený	k2eAgNnSc1d1	osídlené
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
hluboké	hluboký	k2eAgNnSc4d1	hluboké
uzavřené	uzavřený	k2eAgNnSc4d1	uzavřené
Údolí	údolí	k1gNnSc4	údolí
jeptišek	jeptiška	k1gFnPc2	jeptiška
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
údolí	údolí	k1gNnPc1	údolí
procházející	procházející	k2eAgNnPc1d1	procházející
od	od	k7c2	od
Ribeira	Ribeir	k1gInSc2	Ribeir
Brava	bravo	k1gMnSc2	bravo
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
k	k	k7c3	k
Sã	Sã	k1gFnSc3	Sã
Vicente	Vicent	k1gInSc5	Vicent
na	na	k7c6	na
severu	sever	k1gInSc6	sever
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
sídel	sídlo	k1gNnPc2	sídlo
však	však	k9	však
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
úzkém	úzký	k2eAgInSc6d1	úzký
pruhu	pruh	k1gInSc6	pruh
mezi	mezi	k7c7	mezi
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
skalnatým	skalnatý	k2eAgNnSc7d1	skalnaté
vnitrozemím	vnitrozemí	k1gNnSc7	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Cesty	cesta	k1gFnPc4	cesta
vnitrozemím	vnitrozemí	k1gNnPc3	vnitrozemí
dříve	dříve	k6eAd2	dříve
neexistovaly	existovat	k5eNaImAgFnP	existovat
<g/>
,	,	kIx,	,
spojení	spojení	k1gNnSc1	spojení
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
hustá	hustý	k2eAgFnSc1d1	hustá
síť	síť	k1gFnSc1	síť
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
osidlovány	osidlován	k2eAgFnPc1d1	osidlována
i	i	k8xC	i
vyšší	vysoký	k2eAgFnPc1d2	vyšší
polohy	poloha	k1gFnPc1	poloha
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hory	hora	k1gFnSc2	hora
a	a	k8xC	a
moře	moře	k1gNnSc2	moře
==	==	k?	==
</s>
</p>
<p>
<s>
Náhorní	náhorní	k2eAgFnSc1d1	náhorní
plošina	plošina	k1gFnSc1	plošina
Paul	Paul	k1gMnSc1	Paul
da	da	k?	da
Serra	Serra	k1gMnSc1	Serra
(	(	kIx(	(
<g/>
1500	[number]	k4	1500
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
)	)	kIx)	)
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
polovině	polovina	k1gFnSc6	polovina
ostrova	ostrov	k1gInSc2	ostrov
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
zvaném	zvaný	k2eAgNnSc6d1	zvané
Rabaçal	Rabaçal	k1gInSc1	Rabaçal
do	do	k7c2	do
hlubokého	hluboký	k2eAgNnSc2d1	hluboké
údolí	údolí	k1gNnSc2	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Řeky	řeka	k1gFnPc1	řeka
tekoucí	tekoucí	k2eAgFnPc1d1	tekoucí
po	po	k7c6	po
plošině	plošina	k1gFnSc6	plošina
padají	padat	k5eAaImIp3nP	padat
dolů	dolů	k6eAd1	dolů
v	v	k7c6	v
několika	několik	k4yIc6	několik
vodopádech	vodopád	k1gInPc6	vodopád
(	(	kIx(	(
<g/>
Risco	Risco	k1gMnSc1	Risco
<g/>
,	,	kIx,	,
25	[number]	k4	25
fontes	fontesa	k1gFnPc2	fontesa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgInPc1d3	Nejvyšší
vrcholy	vrchol	k1gInPc1	vrchol
ostrova	ostrov	k1gInSc2	ostrov
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
východní	východní	k2eAgFnSc6d1	východní
polovině	polovina	k1gFnSc6	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
Pico	Pico	k6eAd1	Pico
Ruivo	Ruivo	k1gNnSc1	Ruivo
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pico	Pico	k6eAd1	Pico
do	do	k7c2	do
Arieiro	Arieiro	k1gNnSc4	Arieiro
(	(	kIx(	(
<g/>
1818	[number]	k4	1818
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
vzdáleny	vzdálit	k5eAaPmNgFnP	vzdálit
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
čarou	čára	k1gFnSc7	čára
asi	asi	k9	asi
3	[number]	k4	3
km	km	kA	km
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
namáhavý	namáhavý	k2eAgInSc1d1	namáhavý
přechod	přechod	k1gInSc1	přechod
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
trvá	trvat	k5eAaImIp3nS	trvat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
směru	směr	k1gInSc6	směr
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
hodiny	hodina	k1gFnPc4	hodina
kvůli	kvůli	k7c3	kvůli
stoupání	stoupání	k1gNnSc3	stoupání
a	a	k8xC	a
klesání	klesání	k1gNnSc4	klesání
po	po	k7c6	po
schodech	schod	k1gInPc6	schod
vytesaných	vytesaný	k2eAgInPc6d1	vytesaný
do	do	k7c2	do
skal	skála	k1gFnPc2	skála
a	a	k8xC	a
průchodům	průchod	k1gInPc3	průchod
tunelem	tunel	k1gInSc7	tunel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odpoledních	odpolední	k2eAgFnPc6d1	odpolední
hodinách	hodina	k1gFnPc6	hodina
je	být	k5eAaImIp3nS	být
cesta	cesta	k1gFnSc1	cesta
obvykle	obvykle	k6eAd1	obvykle
zahalena	zahalit	k5eAaPmNgFnS	zahalit
v	v	k7c6	v
mlze	mlha	k1gFnSc6	mlha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
koupání	koupání	k1gNnSc4	koupání
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
reklamní	reklamní	k2eAgInSc1d1	reklamní
slogan	slogan	k1gInSc1	slogan
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
spíše	spíše	k9	spíše
než	než	k8xS	než
na	na	k7c6	na
koupání	koupání	k1gNnSc6	koupání
je	být	k5eAaImIp3nS	být
Madeira	Madeira	k1gFnSc1	Madeira
ke	k	k7c3	k
koukání	koukání	k?	koukání
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc4	dva
malé	malý	k2eAgFnPc4d1	malá
přírodní	přírodní	k2eAgFnPc4d1	přírodní
písčité	písčitý	k2eAgFnPc4d1	písčitá
pláže	pláž	k1gFnPc4	pláž
s	s	k7c7	s
černým	černý	k2eAgInSc7d1	černý
pískem	písek	k1gInSc7	písek
a	a	k8xC	a
kilometrová	kilometrový	k2eAgFnSc1d1	kilometrová
kamenitá	kamenitý	k2eAgFnSc1d1	kamenitá
pláž	pláž	k1gFnSc1	pláž
Formosa	Formosa	k1gFnSc1	Formosa
ve	v	k7c6	v
Funchalu	Funchal	k1gInSc6	Funchal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
městečku	městečko	k1gNnSc6	městečko
Calheta	Calheto	k1gNnSc2	Calheto
je	být	k5eAaImIp3nS	být
koupaliště	koupaliště	k1gNnPc4	koupaliště
s	s	k7c7	s
umělými	umělý	k2eAgFnPc7d1	umělá
plážemi	pláž	k1gFnPc7	pláž
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgNnPc6	jenž
je	být	k5eAaImIp3nS	být
dovezený	dovezený	k2eAgInSc1d1	dovezený
jemný	jemný	k2eAgInSc1d1	jemný
žlutý	žlutý	k2eAgInSc1d1	žlutý
písek	písek	k1gInSc1	písek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
míst	místo	k1gNnPc2	místo
jsou	být	k5eAaImIp3nP	být
mořské	mořský	k2eAgFnPc1d1	mořská
lázně	lázeň	k1gFnPc1	lázeň
tvořené	tvořený	k2eAgInPc4d1	tvořený
bazénky	bazének	k1gInPc4	bazének
mezi	mezi	k7c7	mezi
lávovými	lávový	k2eAgFnPc7d1	lávová
skalami	skála	k1gFnPc7	skála
a	a	k8xC	a
doplněné	doplněný	k2eAgFnPc4d1	doplněná
o	o	k7c4	o
betonové	betonový	k2eAgFnPc4d1	betonová
stěny	stěna	k1gFnPc4	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tyto	tento	k3xDgFnPc4	tento
stěny	stěna	k1gFnPc4	stěna
se	se	k3xPyFc4	se
při	při	k7c6	při
přílivu	příliv	k1gInSc6	příliv
převalují	převalovat	k5eAaImIp3nP	převalovat
vlny	vlna	k1gFnPc1	vlna
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
tak	tak	k6eAd1	tak
k	k	k7c3	k
výměně	výměna	k1gFnSc3	výměna
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Madeira	Madeira	k1gFnSc1	Madeira
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Fotoreportáže	fotoreportáž	k1gFnPc1	fotoreportáž
z	z	k7c2	z
Madeiry	Madeira	k1gFnSc2	Madeira
</s>
</p>
