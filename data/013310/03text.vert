<p>
<s>
Čeněk	Čeněk	k1gMnSc1	Čeněk
Kalandra	Kalandr	k1gMnSc2	Kalandr
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
Mýto	mýto	k1gNnSc1	mýto
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1928	[number]	k4	1928
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgMnS	být
křtěn	křtít	k5eAaImNgMnS	křtít
jako	jako	k8xC	jako
Vincenc	Vincenc	k1gMnSc1	Vincenc
<g/>
,	,	kIx,	,
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
zásadně	zásadně	k6eAd1	zásadně
používal	používat	k5eAaImAgMnS	používat
počeštělou	počeštělý	k2eAgFnSc4d1	počeštělá
obdobu	obdoba	k1gFnSc4	obdoba
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
Čeněk	Čeněk	k1gMnSc1	Čeněk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
pekařského	pekařský	k2eAgMnSc2d1	pekařský
mistra	mistr	k1gMnSc2	mistr
ve	v	k7c6	v
Vysokém	vysoký	k2eAgNnSc6d1	vysoké
Mýtě	mýto	k1gNnSc6	mýto
na	na	k7c6	na
Pražském	pražský	k2eAgNnSc6d1	Pražské
předměstí	předměstí	k1gNnSc6	předměstí
č.	č.	k?	č.
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
řemeslem	řemeslo	k1gNnSc7	řemeslo
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
jak	jak	k6eAd1	jak
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
děd	děd	k1gMnSc1	děd
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Maminka	maminka	k1gFnSc1	maminka
Veronika	Veronika	k1gFnSc1	Veronika
byla	být	k5eAaImAgFnS	být
dcera	dcera	k1gFnSc1	dcera
koželužského	koželužský	k2eAgMnSc2d1	koželužský
mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Lichtenberka	Lichtenberka	k1gFnSc1	Lichtenberka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
ukončil	ukončit	k5eAaPmAgInS	ukončit
tříleté	tříletý	k2eAgNnSc4d1	tříleté
studium	studium	k1gNnSc4	studium
reálné	reálný	k2eAgFnSc2d1	reálná
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
našel	najít	k5eAaPmAgInS	najít
si	se	k3xPyFc3	se
práci	práce	k1gFnSc4	práce
jako	jako	k8xS	jako
advokátní	advokátní	k2eAgMnSc1d1	advokátní
písař	písař	k1gMnSc1	písař
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
úředník	úředník	k1gMnSc1	úředník
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
a	a	k8xC	a
Bohumíně	Bohumín	k1gInSc6	Bohumín
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
však	však	k9	však
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
věnovat	věnovat	k5eAaImF	věnovat
učitelskému	učitelský	k2eAgNnSc3d1	učitelské
povolání	povolání	k1gNnSc3	povolání
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1875	[number]	k4	1875
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
svoje	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
pedagogické	pedagogický	k2eAgNnSc4d1	pedagogické
působiště	působiště	k1gNnSc4	působiště
v	v	k7c6	v
Úžici	Úžice	k1gFnSc6	Úžice
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
bez	bez	k7c2	bez
patřičných	patřičný	k2eAgFnPc2d1	patřičná
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
1879	[number]	k4	1879
získal	získat	k5eAaPmAgMnS	získat
místo	místo	k1gNnSc4	místo
prozatímního	prozatímní	k2eAgMnSc2d1	prozatímní
učitele	učitel	k1gMnSc2	učitel
v	v	k7c6	v
Čisté	čistá	k1gFnSc6	čistá
u	u	k7c2	u
Nové	Nové	k2eAgFnSc2d1	Nové
Paky	Paka	k1gFnSc2	Paka
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
přivedl	přivést	k5eAaPmAgMnS	přivést
i	i	k9	i
svoji	svůj	k3xOyFgFnSc4	svůj
budoucí	budoucí	k2eAgFnSc4d1	budoucí
manželkou	manželka	k1gFnSc7	manželka
Julii	Julie	k1gFnSc4	Julie
Morávkovou	Morávková	k1gFnSc4	Morávková
(	(	kIx(	(
<g/>
*	*	kIx~	*
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
Kostelce	Kostelec	k1gInSc2	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
složil	složit	k5eAaPmAgMnS	složit
učitelské	učitelský	k2eAgFnPc4d1	učitelská
zkoušky	zkouška	k1gFnPc4	zkouška
a	a	k8xC	a
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
lepší	dobrý	k2eAgNnSc4d2	lepší
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
1883	[number]	k4	1883
zastával	zastávat	k5eAaImAgMnS	zastávat
sice	sice	k8xC	sice
místo	místo	k1gNnSc4	místo
správce	správce	k1gMnSc2	správce
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
nedalekých	daleký	k2eNgMnPc6d1	nedaleký
Uhlířích	Uhlíř	k1gMnPc6	Uhlíř
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
součástí	součást	k1gFnSc7	součást
obce	obec	k1gFnSc2	obec
Lázně	lázeň	k1gFnSc2	lázeň
Bělohrad	Bělohrad	k1gInSc1	Bělohrad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
jen	jen	k9	jen
o	o	k7c4	o
méně	málo	k6eAd2	málo
významnou	významný	k2eAgFnSc4d1	významná
obec	obec	k1gFnSc4	obec
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
přibližně	přibližně	k6eAd1	přibližně
250	[number]	k4	250
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
správcem	správce	k1gMnSc7	správce
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Roveň	roveň	k1gMnSc1	roveň
u	u	k7c2	u
Mladějova	Mladějův	k2eAgNnSc2d1	Mladějův
<g/>
.	.	kIx.	.
</s>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
nedalekých	daleký	k2eNgFnPc2d1	nedaleká
Prachovských	prachovský	k2eAgFnPc2d1	Prachovská
skal	skála	k1gFnPc2	skála
či	či	k8xC	či
hradů	hrad	k1gInPc2	hrad
Trosky	troska	k1gFnSc2	troska
a	a	k8xC	a
Kost	kost	k1gFnSc4	kost
mu	on	k3xPp3gMnSc3	on
sice	sice	k8xC	sice
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
literární	literární	k2eAgFnSc7d1	literární
inspirací	inspirace	k1gFnSc7	inspirace
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vysněného	vysněný	k2eAgNnSc2d1	vysněné
místa	místo	k1gNnSc2	místo
řídícího	řídící	k2eAgMnSc2d1	řídící
učitele	učitel	k1gMnSc2	učitel
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
až	až	k9	až
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
v	v	k7c6	v
Žereticích	Žeretik	k1gMnPc6	Žeretik
(	(	kIx(	(
<g/>
občas	občas	k6eAd1	občas
uváděn	uvádět	k5eAaImNgInS	uvádět
název	název	k1gInSc1	název
Žerotice	Žerotice	k1gFnSc2	Žerotice
<g/>
,	,	kIx,	,
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
obce	obec	k1gFnSc2	obec
u	u	k7c2	u
Znojma	Znojmo	k1gNnSc2	Znojmo
<g/>
)	)	kIx)	)
na	na	k7c6	na
Jičínsku	Jičínsko	k1gNnSc6	Jičínsko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
i	i	k9	i
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
platem	plat	k1gInSc7	plat
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
zažil	zažít	k5eAaPmAgMnS	zažít
i	i	k9	i
nejplodnější	plodný	k2eAgNnSc4d3	nejplodnější
literární	literární	k2eAgNnSc4d1	literární
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
opět	opět	k6eAd1	opět
změnil	změnit	k5eAaPmAgInS	změnit
pedagogické	pedagogický	k2eAgNnSc4d1	pedagogické
působiště	působiště	k1gNnSc4	působiště
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
žádost	žádost	k1gFnSc4	žádost
převeden	převést	k5eAaPmNgMnS	převést
do	do	k7c2	do
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
chlapecké	chlapecký	k2eAgFnSc2d1	chlapecká
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Libáni	Libán	k2eAgMnPc1d1	Libán
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
až	až	k9	až
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
tak	tak	k6eAd1	tak
umožněno	umožnit	k5eAaPmNgNnS	umožnit
pouze	pouze	k6eAd1	pouze
za	za	k7c4	za
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
roce	rok	k1gInSc6	rok
odejde	odejít	k5eAaPmIp3nS	odejít
do	do	k7c2	do
penze	penze	k1gFnSc2	penze
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
neudělal	udělat	k5eNaPmAgMnS	udělat
<g/>
.	.	kIx.	.
</s>
<s>
Přilepšil	přilepšit	k5eAaPmAgMnS	přilepšit
si	se	k3xPyFc3	se
na	na	k7c6	na
služném	služné	k1gNnSc6	služné
(	(	kIx(	(
<g/>
pobíral	pobírat	k5eAaImAgInS	pobírat
270	[number]	k4	270
korun	koruna	k1gFnPc2	koruna
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
svoji	svůj	k3xOyFgFnSc4	svůj
literární	literární	k2eAgFnSc4d1	literární
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
o	o	k7c4	o
poznání	poznání	k1gNnSc4	poznání
rušnější	rušný	k2eAgNnSc1d2	rušnější
působiště	působiště	k1gNnSc1	působiště
než	než	k8xS	než
ta	ten	k3xDgFnSc1	ten
předešlá	předešlý	k2eAgFnSc1d1	předešlá
<g/>
,	,	kIx,	,
nepřineslo	přinést	k5eNaPmAgNnS	přinést
Kalandrovi	Kalandr	k1gMnSc3	Kalandr
kýžený	kýžený	k2eAgInSc4d1	kýžený
klid	klid	k1gInSc4	klid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maloměšťácká	maloměšťácký	k2eAgFnSc1d1	maloměšťácká
společnost	společnost	k1gFnSc1	společnost
bývalého	bývalý	k2eAgMnSc2d1	bývalý
venkovského	venkovský	k2eAgMnSc2d1	venkovský
učitele	učitel	k1gMnSc2	učitel
<g/>
,	,	kIx,	,
nedbale	dbale	k6eNd1	dbale
oblečeného	oblečený	k2eAgNnSc2d1	oblečené
a	a	k8xC	a
s	s	k7c7	s
poněkud	poněkud	k6eAd1	poněkud
bohémským	bohémský	k2eAgInSc7d1	bohémský
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
,	,	kIx,	,
odmítala	odmítat	k5eAaImAgFnS	odmítat
přijmout	přijmout	k5eAaPmF	přijmout
mezi	mezi	k7c4	mezi
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
ji	on	k3xPp3gFnSc4	on
oplátkou	oplátka	k1gFnSc7	oplátka
sarkasticky	sarkasticky	k6eAd1	sarkasticky
vykresloval	vykreslovat	k5eAaImAgMnS	vykreslovat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
článcích	článek	k1gInPc6	článek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
jako	jako	k8xS	jako
bedlivému	bedlivý	k2eAgMnSc3d1	bedlivý
pozorovateli	pozorovatel	k1gMnSc3	pozorovatel
dařilo	dařit	k5eAaImAgNnS	dařit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
utišení	utišení	k1gNnSc3	utišení
napjatých	napjatý	k2eAgInPc2d1	napjatý
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
libáňskými	libáňský	k2eAgMnPc7d1	libáňský
spoluobčany	spoluobčan	k1gMnPc7	spoluobčan
nepřispěla	přispět	k5eNaPmAgFnS	přispět
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gNnSc4	jeho
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
tíživější	tíživý	k2eAgFnPc4d2	tíživější
finanční	finanční	k2eAgFnPc4d1	finanční
situace	situace	k1gFnPc4	situace
a	a	k8xC	a
nezdary	nezdar	k1gInPc4	nezdar
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
pokusech	pokus	k1gInPc6	pokus
o	o	k7c4	o
vlastní	vlastní	k2eAgNnSc4d1	vlastní
podnikání	podnikání	k1gNnSc4	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
řemeslníků	řemeslník	k1gMnPc2	řemeslník
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zadlužoval	zadlužovat	k5eAaImAgMnS	zadlužovat
a	a	k8xC	a
při	při	k7c6	při
snaze	snaha	k1gFnSc6	snaha
o	o	k7c6	o
zprostředkování	zprostředkování	k1gNnSc6	zprostředkování
zboží	zboží	k1gNnSc2	zboží
místním	místní	k2eAgMnPc3d1	místní
rolníkům	rolník	k1gMnPc3	rolník
se	se	k3xPyFc4	se
stával	stávat	k5eAaImAgInS	stávat
nespolehlivým	spolehlivý	k2eNgInSc7d1	nespolehlivý
<g/>
.	.	kIx.	.
</s>
<s>
Exekuční	exekuční	k2eAgFnPc1d1	exekuční
události	událost	k1gFnPc1	událost
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
domácnosti	domácnost	k1gFnSc6	domácnost
nebyly	být	k5eNaImAgInP	být
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
i	i	k9	i
k	k	k7c3	k
nelibosti	nelibost	k1gFnSc3	nelibost
u	u	k7c2	u
místního	místní	k2eAgMnSc2d1	místní
řídícího	řídící	k2eAgMnSc2d1	řídící
učitele	učitel	k1gMnSc2	učitel
chlapecké	chlapecký	k2eAgFnSc2d1	chlapecká
měšťanky	měšťanka	k1gFnSc2	měšťanka
Josefa	Josef	k1gMnSc2	Josef
Straky	Straka	k1gMnSc2	Straka
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
nepohodlného	pohodlný	k2eNgMnSc4d1	nepohodlný
podřízeného	podřízený	k1gMnSc4	podřízený
zbavit	zbavit	k5eAaPmF	zbavit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1911	[number]	k4	1911
Čeněk	Čeněk	k1gMnSc1	Čeněk
Kalandra	Kalandr	k1gMnSc4	Kalandr
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
dovolenou	dovolená	k1gFnSc4	dovolená
a	a	k8xC	a
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
,	,	kIx,	,
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
zemské	zemský	k2eAgFnSc2d1	zemská
školní	školní	k2eAgFnSc2d1	školní
rady	rada	k1gFnSc2	rada
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1912	[number]	k4	1912
penzionován	penzionován	k2eAgInSc1d1	penzionován
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
už	už	k6eAd1	už
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1911	[number]	k4	1911
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ani	ani	k8xC	ani
zde	zde	k6eAd1	zde
nepřestal	přestat	k5eNaPmAgInS	přestat
psát	psát	k5eAaImF	psát
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
přispěvatelem	přispěvatel	k1gMnSc7	přispěvatel
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
časopisů	časopis	k1gInPc2	časopis
(	(	kIx(	(
<g/>
Nových	Nových	k2eAgInPc6d1	Nových
ilustrovaných	ilustrovaný	k2eAgInPc6d1	ilustrovaný
listech	list	k1gInPc6	list
<g/>
,	,	kIx,	,
Naší	náš	k3xOp1gFnSc3	náš
zahrádce	zahrádka	k1gFnSc3	zahrádka
<g/>
,	,	kIx,	,
Mlynářských	mlynářský	k2eAgFnPc6d1	Mlynářská
novinách	novina	k1gFnPc6	novina
<g/>
,	,	kIx,	,
Zemědělských	zemědělský	k2eAgInPc6d1	zemědělský
listech	list	k1gInPc6	list
<g/>
,	,	kIx,	,
Dobré	dobrý	k2eAgFnSc6d1	dobrá
kopě	kopa	k1gFnSc6	kopa
<g/>
,	,	kIx,	,
v	v	k7c6	v
Mladých	mladý	k2eAgInPc6d1	mladý
proudech	proud	k1gInPc6	proud
<g/>
,	,	kIx,	,
Nivě	niva	k1gFnSc6	niva
<g/>
,	,	kIx,	,
Českém	český	k2eAgNnSc6d1	české
slově	slovo	k1gNnSc6	slovo
<g/>
,	,	kIx,	,
Ostravském	ostravský	k2eAgInSc6d1	ostravský
deníku	deník	k1gInSc6	deník
<g/>
,	,	kIx,	,
Samostatnosti	samostatnost	k1gFnSc3	samostatnost
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgMnS	napsat
i	i	k9	i
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
jeho	jeho	k3xOp3gMnPc1	jeho
rádci	rádce	k1gMnPc1	rádce
(	(	kIx(	(
<g/>
Vzorný	vzorný	k2eAgMnSc1d1	vzorný
rádce	rádce	k1gMnSc1	rádce
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
Co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
když	když	k8xS	když
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Druhá	druhý	k4xOgFnSc1	druhý
knihovna	knihovna	k1gFnSc1	knihovna
žádostí	žádost	k1gFnSc7	žádost
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
slabší	slabý	k2eAgNnPc4d2	slabší
autorova	autorův	k2eAgNnPc4d1	autorovo
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
spisovatelské	spisovatelský	k2eAgFnPc1d1	spisovatelská
se	se	k3xPyFc4	se
činnosti	činnost	k1gFnSc2	činnost
věnoval	věnovat	k5eAaPmAgInS	věnovat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
29	[number]	k4	29
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
života	život	k1gInSc2	život
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
padesát	padesát	k4xCc4	padesát
titulů	titul	k1gInPc2	titul
a	a	k8xC	a
přispěl	přispět	k5eAaPmAgMnS	přispět
četnými	četný	k2eAgInPc7d1	četný
články	článek	k1gInPc7	článek
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
časopisů	časopis	k1gInPc2	časopis
<g/>
,	,	kIx,	,
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
i	i	k9	i
překladatelské	překladatelský	k2eAgFnPc4d1	překladatelská
činnosti	činnost	k1gFnPc4	činnost
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
část	část	k1gFnSc4	část
romanopisnou	romanopisný	k2eAgFnSc4d1	romanopisný
a	a	k8xC	a
populárně-naučnou	populárněaučný	k2eAgFnSc4d1	populárně-naučná
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
především	především	k9	především
botaniky	botanika	k1gFnSc2	botanika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
prvotina	prvotina	k1gFnSc1	prvotina
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
působení	působení	k1gNnSc4	působení
drážního	drážní	k2eAgMnSc2d1	drážní
úředníka	úředník	k1gMnSc2	úředník
v	v	k7c6	v
Bohumíně	Bohumín	k1gInSc6	Bohumín
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
německé	německý	k2eAgNnSc4d1	německé
prostředí	prostředí	k1gNnSc4	prostředí
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
ho	on	k3xPp3gMnSc4	on
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
snaze	snaha	k1gFnSc3	snaha
sepsat	sepsat	k5eAaPmF	sepsat
česky	česky	k6eAd1	česky
psané	psaný	k2eAgNnSc1d1	psané
dílo	dílo	k1gNnSc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
pak	pak	k6eAd1	pak
několik	několik	k4yIc1	několik
humoresek	humoreska	k1gFnPc2	humoreska
do	do	k7c2	do
časopisu	časopis	k1gInSc2	časopis
Paleček	paleček	k1gInSc1	paleček
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaImAgInS	věnovat
se	se	k3xPyFc4	se
i	i	k9	i
psaní	psaní	k1gNnSc3	psaní
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
při	při	k7c6	při
vydání	vydání	k1gNnSc6	vydání
svých	svůj	k3xOyFgFnPc2	svůj
literárních	literární	k2eAgFnPc2d1	literární
prací	práce	k1gFnPc2	práce
používal	používat	k5eAaImAgMnS	používat
pseudonymy	pseudonym	k1gInPc4	pseudonym
(	(	kIx(	(
<g/>
X.	X.	kA	X.
Čekal	čekat	k5eAaImAgMnS	čekat
<g/>
,	,	kIx,	,
Čeněk	Čeněk	k1gMnSc1	Čeněk
Orlický	orlický	k2eAgMnSc1d1	orlický
<g/>
,	,	kIx,	,
L.	L.	kA	L.
Báňský	báňský	k2eAgMnSc1d1	báňský
<g/>
,	,	kIx,	,
X.	X.	kA	X.
Neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
,	,	kIx,	,
X.	X.	kA	X.
Přímý	přímý	k2eAgMnSc1d1	přímý
a	a	k8xC	a
X.	X.	kA	X.
Zapomenutý	zapomenutý	k2eAgMnSc1d1	zapomenutý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
výběrNa	výběrNa	k6eAd1	výběrNa
kraji	kraj	k1gInSc3	kraj
propasti	propast	k1gFnSc2	propast
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
pseudonym	pseudonym	k1gInSc1	pseudonym
X.	X.	kA	X.
Čekal	čekat	k5eAaImAgInS	čekat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1885	[number]	k4	1885
</s>
</p>
<p>
<s>
Od	od	k7c2	od
Sázavy	Sázava	k1gFnSc2	Sázava
ku	k	k7c3	k
Labi	Labe	k1gNnSc3	Labe
<g/>
,	,	kIx,	,
cestopisné	cestopisný	k2eAgInPc4d1	cestopisný
obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
Velké	velký	k2eAgNnSc1d1	velké
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
1887	[number]	k4	1887
</s>
</p>
<p>
<s>
Z	z	k7c2	z
výletů	výlet	k1gInPc2	výlet
po	po	k7c6	po
Jičínsku	Jičínsko	k1gNnSc6	Jičínsko
<g/>
,	,	kIx,	,
cestopisné	cestopisný	k2eAgInPc1d1	cestopisný
obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
Velké	velký	k2eAgNnSc1d1	velké
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
1887	[number]	k4	1887
</s>
</p>
<p>
<s>
Škůdcové	škůdce	k1gMnPc1	škůdce
našich	náš	k3xOp1gInPc2	náš
lesů	les	k1gInPc2	les
a	a	k8xC	a
sadů	sad	k1gInPc2	sad
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
n.	n.	k?	n.
M.	M.	kA	M.
(	(	kIx(	(
<g/>
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
)	)	kIx)	)
1889	[number]	k4	1889
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
světa	svět	k1gInSc2	svět
zvířecího	zvířecí	k2eAgInSc2d1	zvířecí
<g/>
,	,	kIx,	,
humoresky	humoreska	k1gFnPc1	humoreska
<g/>
,	,	kIx,	,
Velké	velký	k2eAgNnSc1d1	velké
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
1889	[number]	k4	1889
</s>
</p>
<p>
<s>
Jiskry	jiskra	k1gFnPc1	jiskra
<g/>
,	,	kIx,	,
humoresky	humoreska	k1gFnPc1	humoreska
<g/>
,	,	kIx,	,
Polička	Polička	k1gFnSc1	Polička
1890	[number]	k4	1890
</s>
</p>
<p>
<s>
Na	na	k7c4	na
praotcův	praotcův	k2eAgInSc4d1	praotcův
statku	statek	k1gInSc2	statek
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
(	(	kIx(	(
<g/>
X.	X.	kA	X.
Čekal	čekat	k5eAaImAgInS	čekat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1890	[number]	k4	1890
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Krátký	Krátký	k1gMnSc1	Krátký
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
(	(	kIx(	(
<g/>
X.	X.	kA	X.
Čekal	čekat	k5eAaImAgInS	čekat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Modrá	modrý	k2eAgFnSc1d1	modrá
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1892	[number]	k4	1892
</s>
</p>
<p>
<s>
Ať	ať	k9	ať
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
zdraví	zdraví	k1gNnSc3	zdraví
<g/>
!	!	kIx.	!
</s>
<s>
humoresky	humoreska	k1gFnPc1	humoreska
<g/>
,	,	kIx,	,
Velké	velký	k2eAgNnSc1d1	velké
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
1893	[number]	k4	1893
</s>
</p>
<p>
<s>
Na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
lodi	loď	k1gFnSc2	loď
otrokářské	otrokářský	k2eAgFnSc2d1	otrokářská
1894	[number]	k4	1894
</s>
</p>
<p>
<s>
Na	na	k7c6	na
prahu	práh	k1gInSc6	práh
Českého	český	k2eAgInSc2d1	český
ráje	ráj	k1gInSc2	ráj
<g/>
,	,	kIx,	,
cestopisné	cestopisný	k2eAgInPc1d1	cestopisný
obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
Besedy	beseda	k1gFnPc1	beseda
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
316	[number]	k4	316
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1894	[number]	k4	1894
</s>
</p>
<p>
<s>
Pěstování	pěstování	k1gNnSc1	pěstování
květin	květina	k1gFnPc2	květina
v	v	k7c6	v
pokoji	pokoj	k1gInSc6	pokoj
<g/>
,	,	kIx,	,
Milotice	Milotice	k1gFnPc1	Milotice
n.	n.	k?	n.
Bečvou	Bečva	k1gFnSc7	Bečva
1894	[number]	k4	1894
Dostupné	dostupný	k2eAgInPc1d1	dostupný
online	onlin	k1gMnSc5	onlin
</s>
</p>
<p>
<s>
Pokání	pokání	k1gNnSc1	pokání
<g/>
,	,	kIx,	,
historická	historický	k2eAgFnSc1d1	historická
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1894	[number]	k4	1894
</s>
</p>
<p>
<s>
Putování	putování	k1gNnSc1	putování
za	za	k7c7	za
chlebíčkem	chlebíček	k1gInSc7	chlebíček
<g/>
,	,	kIx,	,
veselohra	veselohra	k1gFnSc1	veselohra
(	(	kIx(	(
<g/>
X.	X.	kA	X.
Neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Žižkov	Žižkov	k1gInSc1	Žižkov
1894	[number]	k4	1894
</s>
</p>
<p>
<s>
V	v	k7c6	v
proudu	proud	k1gInSc6	proud
<g/>
,	,	kIx,	,
humoresky	humoreska	k1gFnPc4	humoreska
a	a	k8xC	a
črty	črta	k1gFnPc4	črta
<g/>
,	,	kIx,	,
Havránkova	Havránkův	k2eAgFnSc1d1	Havránkova
Moravská	moravský	k2eAgFnSc1d1	Moravská
bibliotéka	bibliotéka	k1gFnSc1	bibliotéka
rodinná	rodinný	k2eAgFnSc1d1	rodinná
<g/>
,	,	kIx,	,
r.	r.	kA	r.
II	II	kA	II
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
1	[number]	k4	1
<g/>
,	,	kIx,	,
Velké	velký	k2eAgNnSc1d1	velké
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
1894	[number]	k4	1894
</s>
</p>
<p>
<s>
Domácí	domácí	k2eAgFnSc1d1	domácí
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
Knihovna	knihovna	k1gFnSc1	knihovna
milotického	milotický	k2eAgMnSc2d1	milotický
hospodáře	hospodář	k1gMnSc2	hospodář
<g/>
,	,	kIx,	,
Milotice	Milotice	k1gFnPc1	Milotice
n.	n.	k?	n.
Bečvou	Bečva	k1gFnSc7	Bečva
1895	[number]	k4	1895
Dostupné	dostupný	k2eAgInPc1d1	dostupný
online	onlin	k1gInSc5	onlin
</s>
</p>
<p>
<s>
Z	z	k7c2	z
upomínek	upomínka	k1gFnPc2	upomínka
ruského	ruský	k2eAgMnSc2d1	ruský
žalářníka	žalářník	k1gMnSc2	žalářník
(	(	kIx(	(
<g/>
Xaver	Xaver	k1gMnSc1	Xaver
Čekal	čekat	k5eAaImAgMnS	čekat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karlín	Karlín	k1gInSc1	Karlín
1895	[number]	k4	1895
</s>
</p>
<p>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
orlická	orlický	k2eAgFnSc1d1	Orlická
<g/>
,	,	kIx,	,
cestopisné	cestopisný	k2eAgInPc4d1	cestopisný
obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
Českým	český	k2eAgInSc7d1	český
krajem	kraj	k1gInSc7	kraj
sv.	sv.	kA	sv.
7	[number]	k4	7
<g/>
,	,	kIx,	,
KČT	KČT	kA	KČT
Praha	Praha	k1gFnSc1	Praha
1895	[number]	k4	1895
</s>
</p>
<p>
<s>
V	v	k7c6	v
dýmu	dým	k1gInSc6	dým
a	a	k8xC	a
plamenech	plamen	k1gInPc6	plamen
I.	I.	kA	I.
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
Žeretice	Žeretika	k1gFnSc3	Žeretika
1897	[number]	k4	1897
</s>
</p>
<p>
<s>
Dobrá	dobrá	k1gFnSc1	dobrá
zažití	zažití	k1gNnSc2	zažití
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
humoresky	humoreska	k1gFnSc2	humoreska
<g/>
,	,	kIx,	,
Žeretice	Žeretika	k1gFnSc3	Žeretika
1897	[number]	k4	1897
</s>
</p>
<p>
<s>
V	v	k7c6	v
dýmu	dým	k1gInSc6	dým
a	a	k8xC	a
plamenech	plamen	k1gInPc6	plamen
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
1898	[number]	k4	1898
</s>
</p>
<p>
<s>
V	v	k7c6	v
dýmu	dým	k1gInSc6	dým
a	a	k8xC	a
plamenech	plamen	k1gInPc6	plamen
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
1901	[number]	k4	1901
</s>
</p>
<p>
<s>
Vpřed	vpřed	k6eAd1	vpřed
–	–	k?	–
k	k	k7c3	k
cíli	cíl	k1gInSc3	cíl
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
přednášky	přednáška	k1gFnSc2	přednáška
a	a	k8xC	a
rozpravy	rozprava	k1gFnSc2	rozprava
</s>
</p>
<p>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zlata	zlato	k1gNnSc2	zlato
(	(	kIx(	(
<g/>
=	=	kIx~	=
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
povídky	povídka	k1gFnSc2	povídka
Jan	Jan	k1gMnSc1	Jan
Krátký	Krátký	k1gMnSc1	Krátký
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
z	z	k7c2	z
r.	r.	kA	r.
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Věštba	věštba	k1gFnSc1	věštba
<g/>
,	,	kIx,	,
Jičín	Jičín	k1gInSc1	Jičín
1902	[number]	k4	1902
</s>
</p>
<p>
<s>
Drůbež	drůbež	k1gFnSc1	drůbež
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
<g/>
Velké	velký	k2eAgNnSc1d1	velké
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
1903	[number]	k4	1903
</s>
</p>
<p>
<s>
V	v	k7c6	v
dýmu	dým	k1gInSc6	dým
a	a	k8xC	a
plamenech	plamen	k1gInPc6	plamen
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
<g/>
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
1904	[number]	k4	1904
</s>
</p>
<p>
<s>
Odplata	odplata	k1gFnSc1	odplata
(	(	kIx(	(
<g/>
=	=	kIx~	=
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
historické	historický	k2eAgFnSc2d1	historická
povídky	povídka	k1gFnSc2	povídka
Pokání	pokání	k1gNnSc2	pokání
z	z	k7c2	z
r.	r.	kA	r.
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
1904	[number]	k4	1904
</s>
</p>
<p>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
se	s	k7c7	s
živly	živel	k1gInPc7	živel
<g/>
,	,	kIx,	,
Kostelec	Kostelec	k1gInSc1	Kostelec
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
1905	[number]	k4	1905
</s>
</p>
<p>
<s>
Zradou	zrada	k1gFnSc7	zrada
i	i	k8xC	i
násilím	násilí	k1gNnSc7	násilí
<g/>
,	,	kIx,	,
historické	historický	k2eAgFnPc1d1	historická
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Velké	velký	k2eAgNnSc1d1	velké
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
1906	[number]	k4	1906
</s>
</p>
<p>
<s>
Bude	být	k5eAaImBp3nS	být
vojna	vojna	k1gFnSc1	vojna
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
<g/>
!	!	kIx.	!
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
1907	[number]	k4	1907
</s>
</p>
<p>
<s>
Pěstování	pěstování	k1gNnSc1	pěstování
květin	květina	k1gFnPc2	květina
v	v	k7c6	v
pokoji	pokoj	k1gInSc6	pokoj
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
rozmnožené	rozmnožený	k2eAgNnSc1d1	rozmnožené
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Knihovna	knihovna	k1gFnSc1	knihovna
milotického	milotický	k2eAgMnSc2d1	milotický
hospodáře	hospodář	k1gMnSc2	hospodář
<g/>
,	,	kIx,	,
Milotice	Milotice	k1gFnPc1	Milotice
n.	n.	k?	n.
Bečvou	Bečva	k1gFnSc7	Bečva
1907	[number]	k4	1907
</s>
</p>
<p>
<s>
Radosti	radost	k1gFnPc1	radost
venkova	venkov	k1gInSc2	venkov
<g/>
,	,	kIx,	,
humoreska	humoreska	k1gFnSc1	humoreska
<g/>
,	,	kIx,	,
Velké	velký	k2eAgNnSc1d1	velké
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
1910	[number]	k4	1910
</s>
</p>
<p>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
Svatojanské	svatojanský	k2eAgFnSc6d1	Svatojanská
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1910	[number]	k4	1910
</s>
</p>
<p>
<s>
Odpřísahal	odpřísahat	k5eAaPmAgMnS	odpřísahat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1911	[number]	k4	1911
</s>
</p>
<p>
<s>
Nejspolehlivější	spolehlivý	k2eAgMnSc1d3	nejspolehlivější
tajemník	tajemník	k1gMnSc1	tajemník
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
1912	[number]	k4	1912
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
,	,	kIx,	,
Rolníkova	rolníkův	k2eAgFnSc1d1	rolníkova
knihovna	knihovna	k1gFnSc1	knihovna
sv.	sv.	kA	sv.
4	[number]	k4	4
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1912	[number]	k4	1912
</s>
</p>
<p>
<s>
Do	do	k7c2	do
zlaté	zlatý	k2eAgFnSc2d1	zlatá
Matičky	matička	k1gFnSc2	matička
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
knihovna	knihovna	k1gFnSc1	knihovna
mládeže	mládež	k1gFnSc2	mládež
II	II	kA	II
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
135	[number]	k4	135
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1913	[number]	k4	1913
</s>
</p>
<p>
<s>
Květiny	květina	k1gFnPc1	květina
cibulovité	cibulovitý	k2eAgFnPc1d1	cibulovitá
a	a	k8xC	a
hliznaté	hliznatý	k2eAgFnPc1d1	hliznatá
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1913	[number]	k4	1913
</s>
</p>
<p>
<s>
Naše	náš	k3xOp1gFnSc1	náš
Káča	Káča	k1gFnSc1	Káča
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
knihovna	knihovna	k1gFnSc1	knihovna
mládeže	mládež	k1gFnSc2	mládež
II	II	kA	II
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
134	[number]	k4	134
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1913	[number]	k4	1913
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
z	z	k7c2	z
naší	náš	k3xOp1gFnSc2	náš
vsi	ves	k1gFnSc2	ves
I.	I.	kA	I.
<g/>
,	,	kIx,	,
<g/>
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1913	[number]	k4	1913
</s>
</p>
<p>
<s>
Stromy	strom	k1gInPc1	strom
a	a	k8xC	a
křoviny	křovina	k1gFnPc1	křovina
okrasné	okrasný	k2eAgFnPc1d1	okrasná
<g/>
,	,	kIx,	,
Knihovna	knihovna	k1gFnSc1	knihovna
Českých	český	k2eAgInPc2d1	český
zahrádkářských	zahrádkářský	k2eAgInPc2d1	zahrádkářský
listů	list	k1gInPc2	list
sv.	sv.	kA	sv.
10	[number]	k4	10
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1913	[number]	k4	1913
</s>
</p>
<p>
<s>
Sladké	Sladké	k2eAgInPc1d1	Sladké
hříchy	hřích	k1gInPc1	hřích
(	(	kIx(	(
<g/>
L.	L.	kA	L.
Báňský	báňský	k2eAgInSc1d1	báňský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Smíchov	Smíchov	k1gInSc1	Smíchov
1915	[number]	k4	1915
</s>
</p>
<p>
<s>
Vzorný	vzorný	k2eAgMnSc1d1	vzorný
rádce	rádce	k1gMnSc1	rádce
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
Švejdův	Švejdův	k2eAgInSc1d1	Švejdův
sborník	sborník	k1gInSc1	sborník
praktických	praktický	k2eAgFnPc2d1	praktická
příruček	příručka	k1gFnPc2	příručka
č.	č.	k?	č.
19	[number]	k4	19
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1915	[number]	k4	1915
</s>
</p>
<p>
<s>
Co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
když	když	k8xS	když
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
?	?	kIx.	?
</s>
<s>
Názory	názor	k1gInPc1	názor
a	a	k8xC	a
úvahy	úvaha	k1gFnPc1	úvaha
dle	dle	k7c2	dle
životních	životní	k2eAgFnPc2d1	životní
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
,	,	kIx,	,
Melicharova	Melicharův	k2eAgFnSc1d1	Melicharova
bibliotéka	bibliotéka	k1gFnSc1	bibliotéka
16	[number]	k4	16
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
(	(	kIx(	(
<g/>
K.	K.	kA	K.
Přímý	přímý	k2eAgMnSc1d1	přímý
<g/>
)	)	kIx)	)
1916	[number]	k4	1916
</s>
</p>
<p>
<s>
Na	na	k7c6	na
horké	horký	k2eAgFnSc6d1	horká
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
(	(	kIx(	(
<g/>
=	=	kIx~	=
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
románu	román	k1gInSc2	román
Nad	nad	k7c7	nad
sopkou	sopka	k1gFnSc7	sopka
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
in	in	k?	in
<g/>
:	:	kIx,	:
V	v	k7c6	v
dýmu	dým	k1gInSc6	dým
a	a	k8xC	a
plamenech	plamen	k1gInPc6	plamen
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1916	[number]	k4	1916
</s>
</p>
<p>
<s>
V	v	k7c6	v
zásobárně	zásobárna	k1gFnSc6	zásobárna
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
humoresky	humoreska	k1gFnPc1	humoreska
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Knihovna	knihovna	k1gFnSc1	knihovna
ilustrovaných	ilustrovaný	k2eAgFnPc2d1	ilustrovaná
humoresek	humoreska	k1gFnPc2	humoreska
sv.	sv.	kA	sv.
5	[number]	k4	5
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1919	[number]	k4	1919
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
Kujebáky	Kujebák	k1gMnPc7	Kujebák
<g/>
,	,	kIx,	,
humoresky	humoreska	k1gFnPc4	humoreska
<g/>
,	,	kIx,	,
Vysoké	vysoký	k2eAgNnSc4d1	vysoké
Mýto	mýto	k1gNnSc4	mýto
(	(	kIx(	(
<g/>
b.	b.	k?	b.
d.	d.	k?	d.
<g/>
,	,	kIx,	,
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
uvádí	uvádět	k5eAaImIp3nS	uvádět
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
katalog	katalog	k1gInSc1	katalog
Nosovského	Nosovského	k2eAgInSc1d1	Nosovského
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
1920	[number]	k4	1920
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
1920	[number]	k4	1920
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
knihovna	knihovna	k1gFnSc1	knihovna
žádostí	žádost	k1gFnSc7	žádost
<g/>
,	,	kIx,	,
Švejdův	Švejdův	k2eAgInSc1d1	Švejdův
sborník	sborník	k1gInSc1	sborník
praktických	praktický	k2eAgFnPc2d1	praktická
příruček	příručka	k1gFnPc2	příručka
č.	č.	k?	č.
19	[number]	k4	19
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1921	[number]	k4	1921
</s>
</p>
<p>
<s>
Na	na	k7c6	na
tváři	tvář	k1gFnSc6	tvář
lehký	lehký	k2eAgInSc4d1	lehký
smích	smích	k1gInSc4	smích
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
1921	[number]	k4	1921
</s>
</p>
<p>
<s>
Sňatek	sňatek	k1gInSc1	sňatek
ve	v	k7c6	v
sluji	sluj	k1gFnSc6	sluj
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1923	[number]	k4	1923
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
i	i	k9	i
za	za	k7c7	za
kulisami	kulisa	k1gFnPc7	kulisa
<g/>
,	,	kIx,	,
románek	románek	k1gInSc1	románek
z	z	k7c2	z
podhoří	podhoří	k1gNnSc2	podhoří
<g/>
,	,	kIx,	,
Sebrané	sebraný	k2eAgInPc4d1	sebraný
spisy	spis	k1gInPc4	spis
Čeňka	Čeněk	k1gMnSc2	Čeněk
Kalandry	kalandr	k1gInPc1	kalandr
sv.	sv.	kA	sv.
1	[number]	k4	1
(	(	kIx(	(
<g/>
svazek	svazek	k1gInSc1	svazek
jediný	jediný	k2eAgInSc1d1	jediný
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
–	–	k?	–
pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
A.	A.	kA	A.
F.	F.	kA	F.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1924	[number]	k4	1924
</s>
</p>
<p>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
léčivé	léčivý	k2eAgFnSc2d1	léčivá
a	a	k8xC	a
jedovaté	jedovatý	k2eAgFnSc2d1	jedovatá
<g/>
,	,	kIx,	,
Knížky	knížka	k1gFnSc2	knížka
13	[number]	k4	13
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1924	[number]	k4	1924
<g/>
loutkové	loutkový	k2eAgFnSc2d1	loutková
hry	hra	k1gFnSc2	hra
</s>
</p>
<p>
<s>
Cestou	cesta	k1gFnSc7	cesta
trnitou	trnitý	k2eAgFnSc7d1	trnitá
1893	[number]	k4	1893
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgMnPc1d1	poslední
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Zdeborských	Zdeborský	k2eAgFnPc2d1	Zdeborský
1893	[number]	k4	1893
</s>
</p>
<p>
<s>
Pytláci	pytlák	k1gMnPc1	pytlák
</s>
</p>
<p>
<s>
Štědrý	štědrý	k2eAgInSc4d1	štědrý
příspěvekpřekladyDr	příspěvekpřekladyDr	k1gInSc4	příspěvekpřekladyDr
<g/>
.	.	kIx.	.
</s>
<s>
V.	V.	kA	V.
z	z	k7c2	z
Kepplerů	Keppler	k1gInPc2	Keppler
<g/>
:	:	kIx,	:
Radosti	radost	k1gFnSc6	radost
více	hodně	k6eAd2	hodně
<g/>
!	!	kIx.	!
</s>
<s>
Melicharův	Melicharův	k2eAgInSc1d1	Melicharův
Výkvět	výkvět	k1gInSc1	výkvět
literatur	literatura	k1gFnPc2	literatura
světových	světový	k2eAgFnPc2d1	světová
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
Führer	Führer	k1gInSc1	Führer
durch	durch	k1gInSc1	durch
Königgrätz	Königgrätz	k1gInSc1	Königgrätz
(	(	kIx(	(
<g/>
Průvodce	průvodce	k1gMnSc1	průvodce
po	po	k7c6	po
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
z	z	k7c2	z
češtiny	čeština	k1gFnSc2	čeština
do	do	k7c2	do
němčiny	němčina	k1gFnSc2	němčina
<g/>
)	)	kIx)	)
1910	[number]	k4	1910
</s>
</p>
<p>
<s>
K.	K.	kA	K.
Sauvain	Sauvain	k1gMnSc1	Sauvain
<g/>
:	:	kIx,	:
Tragedie	tragedie	k1gFnSc1	tragedie
katolického	katolický	k2eAgMnSc2d1	katolický
faráře	farář	k1gMnSc2	farář
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
1912	[number]	k4	1912
</s>
</p>
<p>
<s>
Guy	Guy	k?	Guy
de	de	k?	de
Maupassant	Maupassant	k1gInSc1	Maupassant
<g/>
:	:	kIx,	:
Před	před	k7c7	před
spaním	spaní	k1gNnSc7	spaní
1912	[number]	k4	1912
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Čeněk	Čeněk	k1gMnSc1	Čeněk
Kalandra	Kalandr	k1gMnSc2	Kalandr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Čeněk	Čeněk	k1gMnSc1	Čeněk
Kalandra	Kalandr	k1gMnSc2	Kalandr
</s>
</p>
<p>
<s>
Listy	list	k1gInPc1	list
starohradské	starohradský	k2eAgFnSc2d1	starohradský
kroniky	kronika	k1gFnSc2	kronika
<g/>
,	,	kIx,	,
článek	článek	k1gInSc1	článek
Čeněk	Čeněk	k1gMnSc1	Čeněk
Kalandra	Kalandra	k1gFnSc1	Kalandra
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
Aleš	Aleš	k1gMnSc1	Aleš
Fetters	Fetters	k1gInSc4	Fetters
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
na	na	k7c6	na
serveru	server	k1gInSc6	server
archive	archiv	k1gInSc5	archiv
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
