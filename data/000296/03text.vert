<s>
Zoo	zoo	k1gFnSc3	zoo
Brno	Brno	k1gNnSc1	Brno
je	být	k5eAaImIp3nS	být
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Brno-Bystrc	Brno-Bystrc	k1gFnSc1	Brno-Bystrc
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
Mniší	mniší	k2eAgFnSc2d1	mniší
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c4	na
kopytníky	kopytník	k1gInPc4	kopytník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chová	chovat	k5eAaImIp3nS	chovat
i	i	k9	i
další	další	k2eAgNnPc4d1	další
atraktivní	atraktivní	k2eAgNnPc4d1	atraktivní
zvířata	zvíře	k1gNnPc4	zvíře
jako	jako	k8xC	jako
lední	lední	k2eAgMnPc4d1	lední
medvědy	medvěd	k1gMnPc4	medvěd
<g/>
,	,	kIx,	,
tygry	tygr	k1gMnPc4	tygr
<g/>
,	,	kIx,	,
opice	opice	k1gFnPc4	opice
apod.	apod.	kA	apod.
</s>
<s>
Provozuje	provozovat	k5eAaImIp3nS	provozovat
ji	on	k3xPp3gFnSc4	on
příspěvková	příspěvkový	k2eAgFnSc1d1	příspěvková
organizace	organizace	k1gFnSc1	organizace
Zoo	zoo	k1gFnSc2	zoo
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
stanice	stanice	k1gFnSc1	stanice
zájmových	zájmový	k2eAgFnPc2d1	zájmová
činností	činnost	k1gFnPc2	činnost
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
ředitelem	ředitel	k1gMnSc7	ředitel
je	být	k5eAaImIp3nS	být
Martin	Martin	k1gMnSc1	Martin
Hovorka	Hovorka	k1gMnSc1	Hovorka
<g/>
.	.	kIx.	.
</s>
<s>
Zoo	zoo	k1gFnSc1	zoo
Brno	Brno	k1gNnSc1	Brno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
přehrady	přehrada	k1gFnSc2	přehrada
na	na	k7c6	na
Mniší	mniší	k2eAgFnSc6d1	mniší
hoře	hora	k1gFnSc6	hora
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
65	[number]	k4	65
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
zde	zde	k6eAd1	zde
mohou	moct	k5eAaImIp3nP	moct
vidět	vidět	k5eAaImF	vidět
téměř	téměř	k6eAd1	téměř
800	[number]	k4	800
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
210	[number]	k4	210
druzích	druh	k1gInPc6	druh
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
chovatelské	chovatelský	k2eAgInPc4d1	chovatelský
úspěchy	úspěch	k1gInPc4	úspěch
zoo	zoo	k1gFnPc2	zoo
patří	patřit	k5eAaImIp3nS	patřit
první	první	k4xOgInSc4	první
odchov	odchov	k1gInSc4	odchov
mláděte	mládě	k1gNnSc2	mládě
medvěda	medvěd	k1gMnSc2	medvěd
ledního	lední	k2eAgMnSc2d1	lední
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
nebo	nebo	k8xC	nebo
obdobně	obdobně	k6eAd1	obdobně
první	první	k4xOgInSc1	první
odchov	odchov	k1gInSc1	odchov
šimpanze	šimpanz	k1gMnSc2	šimpanz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Modernizace	modernizace	k1gFnSc1	modernizace
zoo	zoo	k1gFnSc2	zoo
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
započala	započnout	k5eAaPmAgFnS	započnout
výstavbou	výstavba	k1gFnSc7	výstavba
pavilonu	pavilon	k1gInSc2	pavilon
Tygří	tygří	k2eAgFnSc2d1	tygří
skály	skála	k1gFnSc2	skála
pro	pro	k7c4	pro
tygry	tygr	k1gMnPc4	tygr
a	a	k8xC	a
levharty	levhart	k1gMnPc4	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
výběhy	výběh	k1gInPc1	výběh
jsou	být	k5eAaImIp3nP	být
koncipovány	koncipovat	k5eAaBmNgInP	koncipovat
jako	jako	k8xC	jako
džungle	džungle	k1gFnPc1	džungle
<g/>
,	,	kIx,	,
interiér	interiér	k1gInSc1	interiér
pavilonu	pavilon	k1gInSc2	pavilon
napodobuje	napodobovat	k5eAaImIp3nS	napodobovat
skalní	skalní	k2eAgFnSc4d1	skalní
jeskyni	jeskyně	k1gFnSc4	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
je	být	k5eAaImIp3nS	být
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
spojení	spojení	k1gNnSc4	spojení
expozic	expozice	k1gFnPc2	expozice
s	s	k7c7	s
domorodými	domorodý	k2eAgFnPc7d1	domorodá
stavbami	stavba	k1gFnPc7	stavba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
výběh	výběh	k1gInSc1	výběh
vlků	vlk	k1gMnPc2	vlk
kanadských	kanadský	k2eAgMnPc2d1	kanadský
je	být	k5eAaImIp3nS	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
srubem	srub	k1gInSc7	srub
kanadských	kanadský	k2eAgMnPc2d1	kanadský
indiánů	indián	k1gMnPc2	indián
kmene	kmen	k1gInSc2	kmen
Haida	Haida	k1gMnSc1	Haida
<g/>
,	,	kIx,	,
výběh	výběh	k1gInSc1	výběh
bizonů	bizon	k1gMnPc2	bizon
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
několik	několik	k4yIc1	několik
týpí	týpí	k1gNnPc2	týpí
prérijních	prérijní	k2eAgMnPc2d1	prérijní
indiánů	indián	k1gMnPc2	indián
a	a	k8xC	a
výběh	výběh	k1gInSc1	výběh
koní	kůň	k1gMnPc2	kůň
Převalského	převalský	k2eAgNnSc2d1	převalský
mongolská	mongolský	k2eAgFnSc1d1	mongolská
jurta	jurta	k1gFnSc1	jurta
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
počinem	počin	k1gInSc7	počin
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
bylo	být	k5eAaImAgNnS	být
zatím	zatím	k6eAd1	zatím
otevření	otevření	k1gNnSc1	otevření
expozice	expozice	k1gFnSc2	expozice
Beringie	Beringie	k1gFnSc2	Beringie
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
komplex	komplex	k1gInSc4	komplex
výběhů	výběh	k1gInPc2	výběh
medvědů	medvěd	k1gMnPc2	medvěd
kamčatských	kamčatský	k2eAgInPc2d1	kamčatský
<g/>
,	,	kIx,	,
rosomáků	rosomák	k1gMnPc2	rosomák
a	a	k8xC	a
sibiřských	sibiřský	k2eAgMnPc2d1	sibiřský
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
doplněný	doplněný	k2eAgInSc1d1	doplněný
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
ruské	ruský	k2eAgFnSc2d1	ruská
vesničky	vesnička	k1gFnSc2	vesnička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
africká	africký	k2eAgFnSc1d1	africká
vesnice	vesnice	k1gFnSc1	vesnice
s	s	k7c7	s
expozicí	expozice	k1gFnSc7	expozice
žiraf	žirafa	k1gFnPc2	žirafa
<g/>
,	,	kIx,	,
lemurů	lemur	k1gMnPc2	lemur
a	a	k8xC	a
plameňáků	plameňák	k1gMnPc2	plameňák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
zoo	zoo	k1gFnSc1	zoo
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
chov	chov	k1gInSc4	chov
zvířat	zvíře	k1gNnPc2	zvíře
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc2d1	severní
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
brněnskou	brněnský	k2eAgFnSc7d1	brněnská
zoo	zoo	k1gFnSc7	zoo
donedávna	donedávna	k6eAd1	donedávna
spadala	spadat	k5eAaPmAgFnS	spadat
i	i	k9	i
stálá	stálý	k2eAgFnSc1d1	stálá
akvarijní	akvarijní	k2eAgFnSc1d1	akvarijní
výstava	výstava	k1gFnSc1	výstava
umístěná	umístěný	k2eAgFnSc1d1	umístěná
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Brna	Brno	k1gNnSc2	Brno
na	na	k7c6	na
Radnické	radnický	k2eAgFnSc6d1	Radnická
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
omezené	omezený	k2eAgFnPc4d1	omezená
prostory	prostora	k1gFnPc4	prostora
v	v	k7c6	v
historickém	historický	k2eAgInSc6d1	historický
domě	dům	k1gInSc6	dům
nabízela	nabízet	k5eAaImAgFnS	nabízet
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
akvárií	akvárium	k1gNnPc2	akvárium
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
bylo	být	k5eAaImAgNnS	být
chováno	chovat	k5eAaImNgNnS	chovat
kolem	kolem	k7c2	kolem
tisíce	tisíc	k4xCgInSc2	tisíc
ryb	ryba	k1gFnPc2	ryba
v	v	k7c4	v
přibližně	přibližně	k6eAd1	přibližně
120	[number]	k4	120
druzích	druh	k1gInPc6	druh
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
výstavy	výstava	k1gFnSc2	výstava
byla	být	k5eAaImAgNnP	být
i	i	k8xC	i
mořská	mořský	k2eAgNnPc1d1	mořské
akvária	akvárium	k1gNnPc1	akvárium
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ryb	ryba	k1gFnPc2	ryba
zde	zde	k6eAd1	zde
byli	být	k5eAaImAgMnP	být
chováni	chovat	k5eAaImNgMnP	chovat
také	také	k9	také
někteří	některý	k3yIgMnPc1	některý
bezobratlí	bezobratlý	k2eAgMnPc1d1	bezobratlý
živočichové	živočich	k1gMnPc1	živočich
(	(	kIx(	(
<g/>
krabi	krab	k1gMnPc1	krab
<g/>
,	,	kIx,	,
krevety	kreveta	k1gFnPc1	kreveta
<g/>
,	,	kIx,	,
koráli	korále	k1gInPc7	korále
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obojživelníci	obojživelník	k1gMnPc1	obojživelník
a	a	k8xC	a
plazi	plaz	k1gMnPc1	plaz
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
želvy	želva	k1gFnSc2	želva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc3	červenec
2011	[number]	k4	2011
však	však	k9	však
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
výstava	výstava	k1gFnSc1	výstava
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nevyhovující	vyhovující	k2eNgInSc4d1	nevyhovující
technický	technický	k2eAgInSc4d1	technický
stav	stav	k1gInSc4	stav
výstavních	výstavní	k2eAgFnPc2d1	výstavní
prostor	prostora	k1gFnPc2	prostora
a	a	k8xC	a
také	také	k9	také
z	z	k7c2	z
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
důvodů	důvod	k1gInPc2	důvod
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
a	a	k8xC	a
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
živočichů	živočich	k1gMnPc2	živočich
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
chovatelském	chovatelský	k2eAgNnSc6d1	chovatelské
zázemí	zázemí	k1gNnSc6	zázemí
brněnské	brněnský	k2eAgFnSc2d1	brněnská
zoologické	zoologický	k2eAgFnSc2d1	zoologická
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
jen	jen	k9	jen
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
v	v	k7c6	v
pavilonu	pavilon	k1gInSc6	pavilon
Tropické	tropický	k2eAgNnSc1d1	tropické
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
