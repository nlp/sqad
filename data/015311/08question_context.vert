<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc4d1
kraj	kraj	k1gInSc4
byla	být	k5eAaImAgFnS
vyhlášena	vyhlášen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1976	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
jako	jako	k8xC,k8xS
CHKO	CHKO	kA
Kokořínsko	Kokořínsko	k1gNnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
v	v	k7c6
minulosti	minulost	k1gFnSc6
nazývala	nazývat	k5eAaImAgFnS
Polomené	polomený	k2eAgFnPc4d1
hory	hora	k1gFnPc4
<g/>
,	,	kIx,
Dubské	Dubské	k2eAgFnPc1d1
skály	skála	k1gFnPc1
či	či	k8xC
Dubské	Dubské	k2eAgNnSc1d1
Švýcarsko	Švýcarsko	k1gNnSc1
(	(	kIx(
<g/>
Daubaer	Daubaer	k1gMnSc1
Schweiz	Schweiz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2014	#num#	k4
byla	být	k5eAaImAgFnS
CHKO	CHKO	kA
rozšířena	rozšířit	k5eAaPmNgFnS
o	o	k7c4
svoji	svůj	k3xOyFgFnSc4
druhou	druhý	k4xOgFnSc4
<g/>
,	,	kIx,
nenavazující	navazující	k2eNgFnSc4d1
část	část	k1gFnSc4
o	o	k7c6
rozloze	rozloha	k1gFnSc6
136	#num#	k4
km²	km²	k?
jménem	jméno	k1gNnSc7
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
rozkládající	rozkládající	k2eAgMnSc1d1
se	se	k3xPyFc4
v	v	k7c6
severovýchodním	severovýchodní	k2eAgNnSc6d1
okolí	okolí	k1gNnSc6
Máchova	Máchův	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
<g/>
.	.	kIx.
</s>