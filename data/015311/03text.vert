<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
krajIUCN	krajIUCN	k?
kategorie	kategorie	k1gFnSc1
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Mšenské	Mšenské	k2eAgFnSc1d1
pokličkyZákladní	pokličkyZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
1976	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
410	#num#	k4
km	km	kA
<g/>
2	#num#	k4
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Kokořínsko	Kokořínsko	k1gNnSc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraje	kraj	k1gInSc2
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Liberecký	liberecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
Ústecký	ústecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
29	#num#	k4
<g/>
′	′	k?
<g/>
41,29	41,29	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
32	#num#	k4
<g/>
′	′	k?
<g/>
2,68	2,68	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc1
</s>
<s>
23	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.kokorinsko.ochranaprirody.cz	www.kokorinsko.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc4d1
kraj	kraj	k1gInSc4
byla	být	k5eAaImAgFnS
vyhlášena	vyhlášen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1976	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
jako	jako	k8xC,k8xS
CHKO	CHKO	kA
Kokořínsko	Kokořínsko	k1gNnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
v	v	k7c6
minulosti	minulost	k1gFnSc6
nazývala	nazývat	k5eAaImAgFnS
Polomené	polomený	k2eAgFnPc4d1
hory	hora	k1gFnPc4
<g/>
,	,	kIx,
Dubské	Dubské	k2eAgFnPc1d1
skály	skála	k1gFnPc1
či	či	k8xC
Dubské	Dubské	k2eAgNnSc1d1
Švýcarsko	Švýcarsko	k1gNnSc1
(	(	kIx(
<g/>
Daubaer	Daubaer	k1gMnSc1
Schweiz	Schweiz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2014	#num#	k4
byla	být	k5eAaImAgFnS
CHKO	CHKO	kA
rozšířena	rozšířit	k5eAaPmNgFnS
o	o	k7c4
svoji	svůj	k3xOyFgFnSc4
druhou	druhý	k4xOgFnSc4
<g/>
,	,	kIx,
nenavazující	navazující	k2eNgFnSc4d1
část	část	k1gFnSc4
o	o	k7c6
rozloze	rozloha	k1gFnSc6
136	#num#	k4
km²	km²	k?
jménem	jméno	k1gNnSc7
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
rozkládající	rozkládající	k2eAgMnSc1d1
se	se	k3xPyFc4
v	v	k7c6
severovýchodním	severovýchodní	k2eAgNnSc6d1
okolí	okolí	k1gNnSc6
Máchova	Máchův	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
zaujímá	zaujímat	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
cca	cca	kA
410	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejkrásnějším	krásný	k2eAgFnPc3d3
oblastem	oblast	k1gFnPc3
Mělnicka	Mělnicko	k1gNnSc2
<g/>
,	,	kIx,
Českolipska	Českolipsko	k1gNnSc2
<g/>
,	,	kIx,
Mladoboleslavska	Mladoboleslavsko	k1gNnSc2
i	i	k8xC
celých	celý	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krajina	Krajina	k1gFnSc1
má	mít	k5eAaImIp3nS
kaňonovitý	kaňonovitý	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
přechází	přecházet	k5eAaImIp3nS
v	v	k7c4
pahorkatinu	pahorkatina	k1gFnSc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
typické	typický	k2eAgFnPc1d1
pískovcové	pískovcový	k2eAgFnPc1d1
skály	skála	k1gFnPc1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
mnohé	mnohý	k2eAgInPc1d1
vytvářejí	vytvářet	k5eAaImIp3nP
rozličné	rozličný	k2eAgInPc4d1
tvary	tvar	k1gInPc4
-	-	kIx~
skalní	skalní	k2eAgInPc1d1
převisy	převis	k1gInPc1
<g/>
,	,	kIx,
drobné	drobný	k2eAgFnPc1d1
jeskyně	jeskyně	k1gFnPc1
<g/>
,	,	kIx,
výklenky	výklenek	k1gInPc1
a	a	k8xC
římsy	říms	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polomené	polomený	k2eAgFnPc4d1
hory	hora	k1gFnPc4
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
vznikly	vzniknout	k5eAaPmAgInP
koncem	koncem	k7c2
třetihor	třetihory	k1gFnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozlomení	rozlomení	k1gNnSc3
reliéfu	reliéf	k1gInSc2
a	a	k8xC
proniknutí	proniknutí	k1gNnSc2
čedičového	čedičový	k2eAgNnSc2d1
a	a	k8xC
znělcového	znělcový	k2eAgNnSc2d1
magmatu	magma	k1gNnSc2
k	k	k7c3
zemskému	zemský	k2eAgInSc3d1
povrchu	povrch	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Územní	územní	k2eAgNnSc1d1
vymezení	vymezení	k1gNnSc1
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1
vstupními	vstupní	k2eAgFnPc7d1
branami	brána	k1gFnPc7
CHKO	CHKO	kA
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc4d1
kraj	kraj	k1gInSc4
jsou	být	k5eAaImIp3nP
města	město	k1gNnSc2
Dubá	Dubá	k1gFnSc1
a	a	k8xC
Doksy	Doksy	k1gInPc1
<g/>
,	,	kIx,
ležící	ležící	k2eAgFnSc1d1
mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
částmi	část	k1gFnPc7
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
pak	pak	k6eAd1
Mšeno	Mšeno	k6eAd1
<g/>
,	,	kIx,
ležící	ležící	k2eAgFnSc1d1
při	při	k7c6
jejím	její	k3xOp3gInSc6
jihovýchodním	jihovýchodní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
Liběchov	Liběchov	k1gInSc1
<g/>
,	,	kIx,
ležící	ležící	k2eAgFnSc1d1
nedaleko	nedaleko	k7c2
CHKO	CHKO	kA
směrem	směr	k1gInSc7
k	k	k7c3
jihozápadu	jihozápad	k1gInSc3
<g/>
,	,	kIx,
k	k	k7c3
nedalekému	daleký	k2eNgInSc3d1
Mělníku	Mělník	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
starší	starý	k2eAgFnSc2d2
části	část	k1gFnSc2
CHKO	CHKO	kA
(	(	kIx(
<g/>
Kokořínska	Kokořínsko	k1gNnSc2
<g/>
)	)	kIx)
týče	týkat	k5eAaImIp3nS
<g/>
,	,	kIx,
nejnavštěvovanější	navštěvovaný	k2eAgFnSc1d3
je	být	k5eAaImIp3nS
její	její	k3xOp3gFnSc1
jižní	jižní	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
kaňonovitém	kaňonovitý	k2eAgNnSc6d1
údolí	údolí	k1gNnSc6
a	a	k8xC
povodí	povodí	k1gNnSc6
potoka	potok	k1gInSc2
Pšovky	Pšovka	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc2
nejcennější	cenný	k2eAgFnSc2d3
části	část	k1gFnSc2
včetně	včetně	k7c2
postranních	postranní	k2eAgFnPc2d1
roklí	rokle	k1gFnPc2
a	a	k8xC
některých	některý	k3yIgInPc2
skalních	skalní	k2eAgInPc2d1
komplexů	komplex	k1gInPc2
a	a	k8xC
hřebenů	hřeben	k1gInPc2
v	v	k7c6
okolí	okolí	k1gNnSc6
Kokořína	Kokořín	k1gInSc2
a	a	k8xC
západně	západně	k6eAd1
a	a	k8xC
severně	severně	k6eAd1
od	od	k7c2
Mšena	Mšen	k1gInSc2
jsou	být	k5eAaImIp3nP
zvláště	zvláště	k6eAd1
chráněny	chráněn	k2eAgFnPc4d1
jako	jako	k8xS,k8xC
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
Kokořínský	Kokořínský	k2eAgInSc4d1
důl	důl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osou	osa	k1gFnSc7
jihozápadní	jihozápadní	k2eAgFnSc2d1
části	část	k1gFnSc2
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
potok	potok	k1gInSc4
Liběchovka	Liběchovka	k1gFnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc7
údolím	údolí	k1gNnSc7
po	po	k7c6
většině	většina	k1gFnSc6
délky	délka	k1gFnSc2
prochází	procházet	k5eAaImIp3nS
silnice	silnice	k1gFnSc1
I	I	kA
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
,	,	kIx,
turistickými	turistický	k2eAgInPc7d1
východisky	východisko	k1gNnPc7
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
například	například	k6eAd1
Želízy	Želízy	k1gInPc7
<g/>
,	,	kIx,
Tupadly	Tupadlo	k1gNnPc7
<g/>
,	,	kIx,
Chudolazy	Chudolaz	k1gInPc4
nebo	nebo	k8xC
Medonosy	Medonosa	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prostoru	prostor	k1gInSc6
mezi	mezi	k7c7
údolími	údolí	k1gNnPc7
Liběchovky	Liběchovka	k1gFnSc2
a	a	k8xC
Pšovky	Pšovka	k1gFnSc2
je	být	k5eAaImIp3nS
několik	několik	k4yIc4
dolů	dol	k1gInPc2
(	(	kIx(
<g/>
kaňonovitých	kaňonovitý	k2eAgNnPc2d1
údolí	údolí	k1gNnPc2
obklopených	obklopený	k2eAgNnPc2d1
bloky	blok	k1gInPc4
pískovcových	pískovcový	k2eAgFnPc2d1
skal	skála	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
například	například	k6eAd1
Zimořský	Zimořský	k2eAgInSc4d1
důl	důl	k1gInSc4
<g/>
,	,	kIx,
Truskavenský	Truskavenský	k2eAgInSc4d1
důl	důl	k1gInSc4
<g/>
,	,	kIx,
Šemanovický	Šemanovický	k2eAgInSc4d1
důl	důl	k1gInSc4
<g/>
,	,	kIx,
Sitenský	Sitenský	k2eAgInSc4d1
důl	důl	k1gInSc4
<g/>
,	,	kIx,
Vidimský	Vidimský	k2eAgInSc4d1
důl	důl	k1gInSc4
<g/>
,	,	kIx,
Hluboký	hluboký	k2eAgInSc4d1
důl	důl	k1gInSc4
<g/>
,	,	kIx,
Střezivojický	Střezivojický	k2eAgInSc4d1
důl	důl	k1gInSc4
a	a	k8xC
další	další	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
východní	východní	k2eAgFnSc6d1
částí	část	k1gFnPc2
CHKO	CHKO	kA
jsou	být	k5eAaImIp3nP
nejznámějšími	známý	k2eAgFnPc7d3
dominantami	dominanta	k1gFnPc7
Houska	houska	k1gFnSc1
(	(	kIx(
<g/>
poblíž	poblíž	k7c2
pramenů	pramen	k1gInPc2
Pšovky	Pšovka	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
Vrátenská	vrátenský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
<g/>
,	,	kIx,
blíže	blízce	k6eAd2
Dubé	Dubá	k1gFnSc2
pak	pak	k6eAd1
Vysoký	vysoký	k2eAgInSc1d1
vrch	vrch	k1gInSc1
a	a	k8xC
Korecký	Korecký	k2eAgInSc1d1
vrch	vrch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výběžek	výběžek	k1gInSc1
CHKO	CHKO	kA
směřuje	směřovat	k5eAaImIp3nS
na	na	k7c4
severozápad	severozápad	k1gInSc4
od	od	k7c2
Dubé	Dubá	k1gFnSc2
<g/>
,	,	kIx,
až	až	k9
téměř	téměř	k6eAd1
k	k	k7c3
hranicím	hranice	k1gFnPc3
CHKO	CHKO	kA
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
části	část	k1gFnSc6
Kokořínska	Kokořínsko	k1gNnSc2
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
nejvzdálenější	vzdálený	k2eAgFnSc6d3
části	část	k1gFnSc6
dominanty	dominanta	k1gFnSc2
Ronov	Ronov	k1gInSc1
(	(	kIx(
<g/>
553	#num#	k4
m	m	kA
<g/>
)	)	kIx)
či	či	k8xC
Vlhošť	Vlhošť	k1gFnSc1
<g/>
,	,	kIx,
Stříbrný	stříbrný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
<g/>
,	,	kIx,
Husa	husa	k1gFnSc1
<g/>
,	,	kIx,
blíže	blízce	k6eAd2
Dubé	Dubý	k2eAgInPc1d1
pak	pak	k6eAd1
Kostelecké	Kostelecké	k2eAgInPc1d1
bory	bor	k1gInPc1
<g/>
,	,	kIx,
Martinské	martinský	k2eAgInPc1d1
stěny	stěna	k1gFnPc4
a	a	k8xC
Čap	čap	k1gInSc4
a	a	k8xC
západně	západně	k6eAd1
od	od	k7c2
Dubé	Dubá	k1gFnSc2
Dubová	Dubová	k1gFnSc1
hora	hora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
novější	nový	k2eAgFnSc4d2
část	část	k1gFnSc4
chráněné	chráněný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
(	(	kIx(
<g/>
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
zde	zde	k6eAd1
mezi	mezi	k7c4
hlavní	hlavní	k2eAgInPc4d1
turistické	turistický	k2eAgInPc4d1
cíle	cíl	k1gInPc4
bezesporu	bezesporu	k9
okolí	okolí	k1gNnSc6
Máchova	Máchův	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
<g/>
,	,	kIx,
využívané	využívaný	k2eAgFnPc1d1
zejména	zejména	k9
k	k	k7c3
rekreačním	rekreační	k2eAgInPc3d1
účelům	účel	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neméně	málo	k6eNd2
oblíbená	oblíbený	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
zřícenina	zřícenina	k1gFnSc1
hradu	hrad	k1gInSc2
Bezděz	Bezděz	k1gInSc1
na	na	k7c6
stejnojmenném	stejnojmenný	k2eAgInSc6d1
kopci	kopec	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
svými	svůj	k3xOyFgInPc7
606	#num#	k4
metry	metr	k1gInPc7
po	po	k7c6
Vlhošti	Vlhošti	k1gFnSc6
(	(	kIx(
<g/>
614	#num#	k4
m	m	kA
<g/>
)	)	kIx)
druhým	druhý	k4xOgInSc7
nejvyšším	vysoký	k2eAgInSc7d3
vrchem	vrch	k1gInSc7
celé	celý	k2eAgFnSc2d1
CHKO	CHKO	kA
a	a	k8xC
představuje	představovat	k5eAaImIp3nS
tak	tak	k6eAd1
spolu	spolu	k6eAd1
se	se	k3xPyFc4
svým	svůj	k3xOyFgInPc3
menším	menšit	k5eAaImIp1nS
„	„	k?
<g/>
dvojčetem	dvojče	k1gNnSc7
<g/>
“	“	k?
Malým	malý	k2eAgInSc7d1
Bezdězem	Bezděz	k1gInSc7
výraznou	výrazný	k2eAgFnSc7d1
a	a	k8xC
nepřehlédnutelnou	přehlédnutelný	k2eNgFnSc7d1
krajinou	krajina	k1gFnSc7
dominatu	dominat	k1gInSc2
<g/>
,	,	kIx,
již	již	k6eAd1
lze	lze	k6eAd1
za	za	k7c2
vhodných	vhodný	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
zahlédnout	zahlédnout	k5eAaPmF
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
další	další	k2eAgNnPc4d1
pozoruhodná	pozoruhodný	k2eAgNnPc4d1
místa	místo	k1gNnPc4
patří	patřit	k5eAaImIp3nP
Hradčanské	hradčanský	k2eAgInPc1d1
stěny	stěn	k1gInPc1
v	v	k7c6
centrální	centrální	k2eAgFnSc6d1
části	část	k1gFnSc6
oblasti	oblast	k1gFnSc2
-	-	kIx~
rozsáhlé	rozsáhlý	k2eAgNnSc1d1
neosídlené	osídlený	k2eNgNnSc1d1
území	území	k1gNnSc1
<g/>
,	,	kIx,
jemuž	jenž	k3xRgNnSc3
dominují	dominovat	k5eAaImIp3nP
pískovcové	pískovcový	k2eAgFnPc1d1
skály	skála	k1gFnPc1
a	a	k8xC
rokle	rokle	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
návštěvu	návštěva	k1gFnSc4
také	také	k6eAd1
stojí	stát	k5eAaImIp3nP
PP	PP	kA
Provodínské	Provodínský	k2eAgInPc1d1
kameny	kámen	k1gInPc1
s	s	k7c7
nejvyšším	vysoký	k2eAgInSc7d3
vrcholem	vrchol	k1gInSc7
Lysou	lysý	k2eAgFnSc7d1
skálou	skála	k1gFnSc7
(	(	kIx(
<g/>
419	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
zřícenina	zřícenina	k1gFnSc1
hradu	hrad	k1gInSc2
Jestřebí	Jestřebí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
se	se	k3xPyFc4
také	také	k9
nachází	nacházet	k5eAaImIp3nS
velmi	velmi	k6eAd1
významná	významný	k2eAgFnSc1d1
ornitologická	ornitologický	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
Novozámecký	Novozámecký	k2eAgInSc4d1
rybník	rybník	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
v	v	k7c6
bezprostředním	bezprostřední	k2eAgNnSc6d1
okolí	okolí	k1gNnSc6
CHKO	CHKO	kA
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
nespočet	nespočet	k1gInSc1
atraktivních	atraktivní	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
:	:	kIx,
zříceniny	zřícenina	k1gFnSc2
Helfenburk	Helfenburk	k1gInSc1
<g/>
,	,	kIx,
Chudý	chudý	k2eAgInSc1d1
hrádek	hrádek	k1gInSc1
a	a	k8xC
Starý	starý	k2eAgInSc1d1
Berštejn	Berštejn	k1gInSc1
<g/>
,	,	kIx,
malebný	malebný	k2eAgInSc1d1
kaňon	kaňon	k1gInSc1
Peklo	péct	k5eAaImAgNnS
u	u	k7c2
České	český	k2eAgFnSc2d1
Lípy	lípa	k1gFnSc2
<g/>
,	,	kIx,
Čertovy	čertův	k2eAgFnSc2d1
hlavy	hlava	k1gFnSc2
u	u	k7c2
Želíz	Želízy	k1gInPc2
<g/>
,	,	kIx,
hora	hora	k1gFnSc1
Ralsko	Ralsko	k1gNnSc1
a	a	k8xC
mnohé	mnohé	k1gNnSc1
další	další	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Území	území	k1gNnSc1
CHKO	CHKO	kA
patří	patřit	k5eAaImIp3nS
též	též	k9
mezi	mezi	k7c4
významné	významný	k2eAgFnPc4d1
horolezecké	horolezecký	k2eAgFnPc4d1
lokality	lokalita	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
jen	jen	k9
v	v	k7c6
oblasti	oblast	k1gFnSc6
Dubských	Dubských	k2eAgFnPc2d1
skal	skála	k1gFnPc2
bylo	být	k5eAaImAgNnS
popsáno	popsat	k5eAaPmNgNnS
1221	#num#	k4
pískovcových	pískovcový	k2eAgInPc2d1
skalních	skalní	k2eAgInPc2d1
masívů	masív	k1gInPc2
a	a	k8xC
věží	věž	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vznik	vznik	k1gInSc1
a	a	k8xC
popis	popis	k1gInSc1
krajiny	krajina	k1gFnSc2
</s>
<s>
V	v	k7c6
třetihorách	třetihory	k1gFnPc6
došlo	dojít	k5eAaPmAgNnS
na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
k	k	k7c3
průnikům	průnik	k1gInPc3
sopečných	sopečný	k2eAgFnPc2d1
vyvřelin	vyvřelina	k1gFnPc2
(	(	kIx(
<g/>
čedič	čedič	k1gInSc1
<g/>
,	,	kIx,
znělce	znělec	k1gInPc1
<g/>
,	,	kIx,
trachyty	trachyt	k1gInPc1
a	a	k8xC
jiné	jiný	k2eAgFnPc1d1
<g/>
)	)	kIx)
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
tvoří	tvořit	k5eAaImIp3nP
vrcholky	vrcholek	k1gInPc1
v	v	k7c6
krajině	krajina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působením	působení	k1gNnSc7
erozních	erozní	k2eAgInPc2d1
vlivů	vliv	k1gInPc2
byla	být	k5eAaImAgFnS
pískovcová	pískovcový	k2eAgFnSc1d1
tabule	tabule	k1gFnSc1
rozčleněna	rozčlenit	k5eAaPmNgFnS
nejprve	nejprve	k6eAd1
erozními	erozní	k2eAgFnPc7d1
rýhami	rýha	k1gFnPc7
a	a	k8xC
postupně	postupně	k6eAd1
hlubokými	hluboký	k2eAgFnPc7d1
roklemi	rokle	k1gFnPc7
a	a	k8xC
údolími	údolí	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obnažené	obnažený	k2eAgFnPc1d1
skalní	skalní	k2eAgFnPc1d1
stěny	stěna	k1gFnPc1
podléhají	podléhat	k5eAaImIp3nP
selektivnímu	selektivní	k2eAgNnSc3d1
větrání	větrání	k1gNnSc3
za	za	k7c2
vzniku	vznik	k1gInSc2
drobných	drobný	k2eAgInPc2d1
reliéfních	reliéfní	k2eAgInPc2d1
tvarů	tvar	k1gInPc2
jako	jako	k8xC,k8xS
například	například	k6eAd1
voštiny	voština	k1gFnPc1
<g/>
,	,	kIx,
skalní	skalní	k2eAgFnPc1d1
římsy	římsa	k1gFnPc1
a	a	k8xC
lišty	lišta	k1gFnPc1
<g/>
,	,	kIx,
skalní	skalní	k2eAgNnPc1d1
okna	okno	k1gNnPc1
<g/>
,	,	kIx,
výklenky	výklenek	k1gInPc1
a	a	k8xC
jeskyně	jeskyně	k1gFnPc1
<g/>
,	,	kIx,
vzácněji	vzácně	k6eAd2
škrapy	škrapy	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pískovcích	pískovec	k1gInPc6
o	o	k7c6
nestejné	stejný	k2eNgFnSc6d1
odolnosti	odolnost	k1gFnSc6
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
známé	známý	k2eAgFnPc1d1
pokličky	poklička	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údolí	údolí	k1gNnSc1
jsou	být	k5eAaImIp3nP
většinou	většina	k1gFnSc7
bezvodá	bezvodý	k2eAgFnSc1d1
<g/>
,	,	kIx,
jedinými	jediný	k2eAgInPc7d1
většími	veliký	k2eAgInPc7d2
stálými	stálý	k2eAgInPc7d1
toky	tok	k1gInPc7
jsou	být	k5eAaImIp3nP
potoky	potok	k1gInPc1
Pšovka	Pšovka	k1gFnSc1
<g/>
,	,	kIx,
Liběchovka	Liběchovka	k1gFnSc1
<g/>
,	,	kIx,
Dolský	dolský	k2eAgInSc1d1
potok	potok	k1gInSc1
a	a	k8xC
řeka	řeka	k1gFnSc1
Ploučnice	Ploučnice	k1gFnSc1
protékající	protékající	k2eAgFnSc4d1
severovýchodním	severovýchodní	k2eAgInSc7d1
okrajem	okraj	k1gInSc7
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Botanicky	botanicky	k6eAd1
je	být	k5eAaImIp3nS
CHKO	CHKO	kA
zajímavá	zajímavý	k2eAgFnSc1d1
především	především	k9
díky	díky	k7c3
inverzním	inverzní	k2eAgFnPc3d1
polohám	poloha	k1gFnPc3
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
údolích	údolí	k1gNnPc6
rostou	růst	k5eAaImIp3nP
vlhkomilné	vlhkomilný	k2eAgFnPc1d1
a	a	k8xC
podhorské	podhorský	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
a	a	k8xC
na	na	k7c6
výše	vysoce	k6eAd2
položených	položený	k2eAgFnPc6d1
polohách	poloha	k1gFnPc6
potkáme	potkat	k5eAaPmIp1nP
spíše	spíše	k9
druhy	druh	k1gInPc1
teplomilné	teplomilný	k2eAgInPc1d1
a	a	k8xC
suchomilné	suchomilný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
ptáků	pták	k1gMnPc2
v	v	k7c6
údolích	údolí	k1gNnPc6
hnízdí	hnízdit	k5eAaImIp3nP
některé	některý	k3yIgInPc4
druhy	druh	k1gInPc4
sov	sova	k1gFnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
pak	pak	k6eAd1
výr	výr	k1gMnSc1
velký	velký	k2eAgMnSc1d1
a	a	k8xC
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
rozšířil	rozšířit	k5eAaPmAgMnS
krkavec	krkavec	k1gMnSc1
velký	velký	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
2014	#num#	k4
zachytily	zachytit	k5eAaPmAgFnP
fotokamery	fotokamera	k1gFnPc1
na	na	k7c4
území	území	k1gNnSc4
CHKO	CHKO	kA
mládě	mládě	k1gNnSc4
vlka	vlk	k1gMnSc2
obecného	obecný	k2eAgMnSc2d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
dokládá	dokládat	k5eAaImIp3nS
opětovné	opětovný	k2eAgNnSc1d1
rozmnožování	rozmnožování	k1gNnSc1
vlka	vlk	k1gMnSc2
obecného	obecný	k2eAgMnSc2d1
na	na	k7c6
území	území	k1gNnSc6
Česka	Česko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Jen	jen	k9
v	v	k7c6
průběhu	průběh	k1gInSc6
října	říjen	k1gInSc2
2015	#num#	k4
zachytily	zachytit	k5eAaPmAgFnP
fotopasti	fotopast	k1gFnPc1
vlky	vlk	k1gMnPc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
Břehyně	Břehyně	k1gFnSc2
–	–	k?
Pecopala	Pecopal	k1gMnSc2
severně	severně	k6eAd1
od	od	k7c2
Máchova	Máchův	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
a	a	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
Břehyňského	Břehyňský	k2eAgInSc2d1
rybníka	rybník	k1gInSc2
více	hodně	k6eAd2
než	než	k8xS
dvěstěkrát	dvěstěkrát	k6eAd1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
na	na	k7c6
několika	několik	k4yIc6
záznamech	záznam	k1gInPc6
bylo	být	k5eAaImAgNnS
najednou	najednou	k6eAd1
vyfotografováno	vyfotografovat	k5eAaPmNgNnS
pět	pět	k4xCc1
zvířat	zvíře	k1gNnPc2
najednou	najednou	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
březnu	březen	k1gInSc6
roku	rok	k1gInSc2
2018	#num#	k4
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
vyfotografovat	vyfotografovat	k5eAaPmF
vlka	vlk	k1gMnSc4
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
CHKO	CHKO	kA
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
větší	veliký	k2eAgFnSc6d2
vzdálenosti	vzdálenost	k1gFnSc6
od	od	k7c2
dosavadního	dosavadní	k2eAgNnSc2d1
místa	místo	k1gNnSc2
výskytu	výskyt	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
lidové	lidový	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
zachována	zachován	k2eAgFnSc1d1
řada	řada	k1gFnSc1
malebných	malebný	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
s	s	k7c7
roubenými	roubený	k2eAgInPc7d1
a	a	k8xC
hrázděnými	hrázděný	k2eAgInPc7d1
domy	dům	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohé	mnohý	k2eAgInPc1d1
výklenky	výklenek	k1gInPc1
v	v	k7c6
poměrně	poměrně	k6eAd1
měkkém	měkký	k2eAgInSc6d1
pískovci	pískovec	k1gInSc6
byly	být	k5eAaImAgFnP
v	v	k7c6
minulých	minulý	k2eAgFnPc6d1
dobách	doba	k1gFnPc6
upraveny	upraven	k2eAgFnPc1d1
ve	v	k7c4
skalní	skalní	k2eAgInPc4d1
byty	byt	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
však	však	k9
již	již	k6eAd1
byly	být	k5eAaImAgInP
opuštěny	opustit	k5eAaPmNgInP
a	a	k8xC
chátrají	chátrat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
tohoto	tento	k3xDgNnSc2
obydlí	obydlí	k1gNnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
skalní	skalní	k2eAgInSc4d1
byt	byt	k1gInSc4
ve	v	k7c6
Lhotce	Lhotka	k1gFnSc6
u	u	k7c2
Mělníka	Mělník	k1gInSc2
(	(	kIx(
<g/>
mimo	mimo	k7c4
hranice	hranice	k1gFnPc4
CHKO	CHKO	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
správě	správa	k1gFnSc6
Regionálního	regionální	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
Mělník	Mělník	k1gInSc1
a	a	k8xC
přístupný	přístupný	k2eAgInSc1d1
turistům	turist	k1gMnPc3
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
je	být	k5eAaImIp3nS
především	především	k9
využívána	využívat	k5eAaImNgFnS,k5eAaPmNgFnS
k	k	k7c3
chalupářské	chalupářský	k2eAgFnSc3d1
rekreaci	rekreace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dominantou	dominanta	k1gFnSc7
Kokořínska	Kokořínsko	k1gNnSc2
je	být	k5eAaImIp3nS
hrad	hrad	k1gInSc4
Kokořín	Kokořína	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
a	a	k8xC
sídlo	sídlo	k1gNnSc1
CHKO	CHKO	kA
</s>
<s>
Tabule	tabule	k1gFnSc1
<g/>
,	,	kIx,
označující	označující	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
CHKO	CHKO	kA
</s>
<s>
Oblast	oblast	k1gFnSc1
je	být	k5eAaImIp3nS
vyhledávaná	vyhledávaný	k2eAgFnSc1d1
českými	český	k2eAgMnPc7d1
i	i	k8xC
německými	německý	k2eAgMnPc7d1
turisty	turist	k1gMnPc7
(	(	kIx(
<g/>
do	do	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byla	být	k5eAaImAgFnS
oblast	oblast	k1gFnSc1
německojazyčná	německojazyčný	k2eAgFnSc1d1
<g/>
)	)	kIx)
již	již	k9
nejméně	málo	k6eAd3
od	od	k7c2
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
zřízena	zřízen	k2eAgFnSc1d1
výnosem	výnos	k1gInSc7
Ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
ČSR	ČSR	kA
č.	č.	k?
<g/>
j.	j.	k?
6.070	6.070	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1976	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spravované	spravovaný	k2eAgFnPc4d1
území	území	k1gNnSc6
se	se	k3xPyFc4
rozprostíralo	rozprostírat	k5eAaImAgNnS
na	na	k7c6
území	území	k1gNnSc6
tří	tři	k4xCgInPc2
krajů	kraj	k1gInPc2
a	a	k8xC
několika	několik	k4yIc2
okresů	okres	k1gInPc2
<g/>
,	,	kIx,
zejména	zejména	k9
Mělník	Mělník	k1gInSc1
a	a	k8xC
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
CHKO	CHKO	kA
Kokořínsko	Kokořínsko	k1gNnSc4
se	se	k3xPyFc4
skládala	skládat	k5eAaImAgFnS
ze	z	k7c2
dvou	dva	k4xCgFnPc2
oblastí	oblast	k1gFnPc2
-	-	kIx~
kokořínské	kokořínský	k2eAgFnPc1d1
a	a	k8xC
vlhošťské	vlhošťský	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Správa	správa	k1gFnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Kokořínsko	Kokořínsko	k1gNnSc1
při	při	k7c6
svém	svůj	k3xOyFgNnSc6
založení	založení	k1gNnSc6
sídlila	sídlit	k5eAaImAgFnS
na	na	k7c6
zámku	zámek	k1gInSc6
Mělník	Mělník	k1gInSc1
a	a	k8xC
dislokované	dislokovaný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
měla	mít	k5eAaImAgFnS
v	v	k7c6
Holanech	Holan	k1gMnPc6
u	u	k7c2
Zahrádek	zahrádka	k1gFnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Nyní	nyní	k6eAd1
sídlí	sídlet	k5eAaImIp3nS
ve	v	k7c6
městě	město	k1gNnSc6
Mělník	Mělník	k1gInSc1
<g/>
,	,	kIx,
adresa	adresa	k1gFnSc1
je	být	k5eAaImIp3nS
Česká	český	k2eAgFnSc1d1
149	#num#	k4
<g/>
,	,	kIx,
276	#num#	k4
01	#num#	k4
Mělník	Mělník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
Mělník	Mělník	k1gInSc1
samotné	samotný	k2eAgFnSc3d1
ovšem	ovšem	k9
leží	ležet	k5eAaImIp3nP
již	již	k6eAd1
vně	vně	k7c2
území	území	k1gNnSc2
CHKO	CHKO	kA
<g/>
,	,	kIx,
asi	asi	k9
7	#num#	k4
km	km	kA
od	od	k7c2
její	její	k3xOp3gFnSc2
hranice	hranice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
</s>
<s>
Od	od	k7c2
listopadu	listopad	k1gInSc2
2009	#num#	k4
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Kokořínsko	Kokořínsko	k1gNnSc1
a	a	k8xC
Ministerstvo	ministerstvo	k1gNnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
ČR	ČR	kA
předjednávaly	předjednávat	k5eAaImAgFnP,k5eAaPmAgFnP
s	s	k7c7
obcemi	obec	k1gFnPc7
záměr	záměr	k1gInSc1
rozšíření	rozšíření	k1gNnSc4
CHKO	CHKO	kA
z	z	k7c2
272	#num#	k4
km²	km²	k?
na	na	k7c4
cca	cca	kA
420	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
o	o	k7c4
jihozápadní	jihozápadní	k2eAgFnSc4d1
část	část	k1gFnSc4
Českolipska	Českolipsko	k1gNnSc2
od	od	k7c2
Novozámeckého	Novozámecký	k2eAgInSc2d1
rybníka	rybník	k1gInSc2
u	u	k7c2
obce	obec	k1gFnSc2
Zahrádky	zahrádka	k1gFnSc2
až	až	k9
k	k	k7c3
Bělé	Bělá	k1gFnSc3
pod	pod	k7c7
Bezdězem	Bezděz	k1gInSc7
<g/>
,	,	kIx,
včetně	včetně	k7c2
části	část	k1gFnSc2
bývalého	bývalý	k2eAgInSc2d1
vojenského	vojenský	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
Ralsko	Ralsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
hlavní	hlavní	k2eAgInSc1d1
důvod	důvod	k1gInSc1
je	být	k5eAaImIp3nS
uváděn	uváděn	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
mnoha	mnoho	k4c2
vzácných	vzácný	k2eAgInPc2d1
druhů	druh	k1gInPc2
živočichů	živočich	k1gMnPc2
a	a	k8xC
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
záměru	záměr	k1gInSc3
se	se	k3xPyFc4
na	na	k7c4
podzim	podzim	k1gInSc4
2009	#num#	k4
kvůli	kvůli	k7c3
komplikacím	komplikace	k1gFnPc3
a	a	k8xC
omezením	omezení	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
by	by	kYmCp3nP
to	ten	k3xDgNnSc1
znamenalo	znamenat	k5eAaImAgNnS
pro	pro	k7c4
rozvoj	rozvoj	k1gInSc4
území	území	k1gNnSc2
<g/>
,	,	kIx,
postavily	postavit	k5eAaPmAgInP
Zákupy	zákup	k1gInPc1
a	a	k8xC
Doksy	Doksy	k1gInPc1
<g/>
,	,	kIx,
starosta	starosta	k1gMnSc1
Ralska	Ralsko	k1gNnSc2
se	se	k3xPyFc4
vyjádřil	vyjádřit	k5eAaPmAgMnS
vstřícněji	vstřícně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
2009	#num#	k4
ministerstvo	ministerstvo	k1gNnSc1
předpokládalo	předpokládat	k5eAaImAgNnS
vyhlášení	vyhlášení	k1gNnSc4
změny	změna	k1gFnSc2
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Zastupitelstva	zastupitelstvo	k1gNnSc2
obcí	obec	k1gFnSc7
Doksy	Doksy	k1gInPc4
<g/>
,	,	kIx,
Mimoň	Mimoň	k1gFnSc4
a	a	k8xC
Zákupy	zákup	k1gInPc4
přijala	přijmout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
zamítavá	zamítavý	k2eAgNnPc1d1
stanoviska	stanovisko	k1gNnPc1
<g/>
,	,	kIx,
Ralsko	Ralsko	k1gNnSc1
mělo	mít	k5eAaImAgNnS
dílčí	dílčí	k2eAgFnPc4d1
připomínky	připomínka	k1gFnPc4
<g/>
,	,	kIx,
například	například	k6eAd1
vyloučení	vyloučení	k1gNnSc1
sídliště	sídliště	k1gNnSc2
z	z	k7c2
CHKO	CHKO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starosta	Starosta	k1gMnSc1
Doks	Doksy	k1gInPc2
nevyloučil	vyloučit	k5eNaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
zastupitelstvo	zastupitelstvo	k1gNnSc1
s	s	k7c7
rozšířením	rozšíření	k1gNnSc7
souhlasilo	souhlasit	k5eAaImAgNnS
<g/>
,	,	kIx,
pokud	pokud	k8xS
by	by	kYmCp3nS
dostalo	dostat	k5eAaPmAgNnS
záruky	záruka	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
rozšíření	rozšíření	k1gNnSc1
neuškodí	uškodit	k5eNaPmIp3nS
rekreačnímu	rekreační	k2eAgInSc3d1
provozu	provoz	k1gInSc3
na	na	k7c6
Máchově	Máchův	k2eAgNnSc6d1
jezeře	jezero	k1gNnSc6
a	a	k8xC
místním	místní	k2eAgMnPc3d1
podnikatelům	podnikatel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starostka	starostka	k1gFnSc1
Zákup	zákup	k1gInSc1
mezi	mezi	k7c7
hlavními	hlavní	k2eAgFnPc7d1
námitkami	námitka	k1gFnPc7
uvedla	uvést	k5eAaPmAgFnS
zvýšení	zvýšení	k1gNnSc3
byrokratické	byrokratický	k2eAgFnSc2d1
zátěže	zátěž	k1gFnSc2
pro	pro	k7c4
obyvatele	obyvatel	k1gMnPc4
například	například	k6eAd1
při	při	k7c6
stavebních	stavební	k2eAgFnPc6d1
úpravách	úprava	k1gFnPc6
domů	domů	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Negativní	negativní	k2eAgNnSc1d1
stanovisko	stanovisko	k1gNnSc1
vyjádřily	vyjádřit	k5eAaPmAgFnP
i	i	k9
obce	obec	k1gFnPc1
Jestřebí	Jestřebí	k1gNnSc2
a	a	k8xC
Bělá	bělat	k5eAaImIp3nS
pod	pod	k7c7
Bezdězem	Bezděz	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Zahrádky	zahrádka	k1gFnPc1
nesouhlasily	souhlasit	k5eNaImAgFnP
proto	proto	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
části	část	k1gFnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
Karasy	karas	k1gMnPc7
je	být	k5eAaImIp3nS
uvažováno	uvažován	k2eAgNnSc1d1
s	s	k7c7
výstavbou	výstavba	k1gFnSc7
rodinných	rodinný	k2eAgInPc2d1
domů	dům	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Jednání	jednání	k1gNnSc4
s	s	k7c7
nesouhlasícími	souhlasící	k2eNgFnPc7d1
obcemi	obec	k1gFnPc7
většinou	většinou	k6eAd1
skončila	skončit	k5eAaPmAgFnS
kompromisem	kompromis	k1gInSc7
<g/>
,	,	kIx,
například	například	k6eAd1
přeřazením	přeřazení	k1gNnSc7
některých	některý	k3yIgNnPc2
území	území	k1gNnPc2
(	(	kIx(
<g/>
např.	např.	kA
Máchova	Máchův	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
s	s	k7c7
plážemi	pláž	k1gFnPc7
<g/>
)	)	kIx)
do	do	k7c2
nižšího	nízký	k2eAgNnSc2d2
pásma	pásmo	k1gNnSc2
ochrany	ochrana	k1gFnSc2
nebo	nebo	k8xC
vyjmutím	vyjmutí	k1gNnSc7
z	z	k7c2
navrhovaného	navrhovaný	k2eAgInSc2d1
CHKO	CHKO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Jestřebí	Jestřebí	k1gNnSc1
mělo	mít	k5eAaImAgNnS
podmínku	podmínka	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
ochranáři	ochranář	k1gMnPc1
vyčistili	vyčistit	k5eAaPmAgMnP
Robečský	Robečský	k2eAgInSc4d1
potok	potok	k1gInSc4
a	a	k8xC
vyjmuli	vyjmout	k5eAaPmAgMnP
z	z	k7c2
ochrany	ochrana	k1gFnSc2
území	území	k1gNnSc2
pro	pro	k7c4
silniční	silniční	k2eAgInSc4d1
obchvat	obchvat	k1gInSc4
do	do	k7c2
Provodína	Provodín	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
červnu	červen	k1gInSc6
2011	#num#	k4
ministerstvo	ministerstvo	k1gNnSc1
psalo	psát	k5eAaImAgNnS
o	o	k7c6
záměru	záměr	k1gInSc6
rozšíření	rozšíření	k1gNnSc2
o	o	k7c6
části	část	k1gFnSc6
Středočeského	středočeský	k2eAgInSc2d1
a	a	k8xC
Libereckého	liberecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
o	o	k7c6
rozloze	rozloha	k1gFnSc6
139	#num#	k4
km²	km²	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
ministr	ministr	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Chalupa	Chalupa	k1gMnSc1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
záměr	záměr	k1gInSc1
má	mít	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc4
podporu	podpora	k1gFnSc4
i	i	k8xC
podporu	podpora	k1gFnSc4
většiny	většina	k1gFnSc2
ze	z	k7c2
starostů	starosta	k1gMnPc2
obcí	obec	k1gFnPc2
Ralsko	Ralsko	k1gNnSc1
<g/>
,	,	kIx,
Doksy	Doksy	k1gInPc1
<g/>
,	,	kIx,
Mimoň	Mimoň	k1gFnSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
<g/>
,	,	kIx,
Bělá	bělat	k5eAaImIp3nS
pod	pod	k7c7
Bezdězem	Bezděz	k1gInSc7
<g/>
,	,	kIx,
Bezděz	Bezděz	k1gInSc1
<g/>
,	,	kIx,
Provodín	Provodín	k1gInSc1
<g/>
,	,	kIx,
Jestřebí	Jestřebí	k1gNnPc1
a	a	k8xC
Zahrádky	zahrádka	k1gFnPc1
<g/>
,	,	kIx,
s	s	k7c7
nimiž	jenž	k3xRgInPc7
se	se	k3xPyFc4
ministr	ministr	k1gMnSc1
setkal	setkat	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
bylo	být	k5eAaImAgNnS
schválenou	schválený	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
9	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2014	#num#	k4
s	s	k7c7
účinností	účinnost	k1gFnSc7
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2014	#num#	k4
<g/>
,	,	kIx,
nová	nový	k2eAgFnSc1d1
chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
„	„	k?
<g/>
CHKO	CHKO	kA
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
svého	svůj	k3xOyFgNnSc2
založení	založení	k1gNnSc2
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
měla	mít	k5eAaImAgFnS
správa	správa	k1gFnSc1
CHKO	CHKO	kA
na	na	k7c6
starosti	starost	k1gFnSc6
dvě	dva	k4xCgNnPc4
maloplošná	maloplošný	k2eAgNnPc4d1
chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnPc4
-	-	kIx~
Státní	státní	k2eAgFnSc3d1
přírodní	přírodní	k2eAgFnSc3d1
rezervaci	rezervace	k1gFnSc3
Kokořínský	Kokořínský	k2eAgInSc4d1
důl	důl	k1gInSc4
a	a	k8xC
Chráněný	chráněný	k2eAgInSc4d1
přírodní	přírodní	k2eAgInSc4d1
výtvor	výtvor	k1gInSc4
Špičák	Špičák	k1gMnSc1
u	u	k7c2
Střezivojic	Střezivojice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
také	také	k9
jednu	jeden	k4xCgFnSc4
naučnou	naučný	k2eAgFnSc4d1
stezku	stezka	k1gFnSc4
-	-	kIx~
Kokořínská	Kokořínský	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nyní	nyní	k6eAd1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
přírodně	přírodně	k6eAd1
cenných	cenný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
chráněna	chráněn	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
maloplošná	maloplošný	k2eAgNnPc1d1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
(	(	kIx(
<g/>
MCHU	MCHU	kA
<g/>
)	)	kIx)
-	-	kIx~
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
(	(	kIx(
<g/>
PR	pr	k0
<g/>
)	)	kIx)
a	a	k8xC
přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
(	(	kIx(
<g/>
PP	PP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
území	území	k1gNnPc1
v	v	k7c6
péči	péče	k1gFnSc6
CHKO	CHKO	kA
jsou	být	k5eAaImIp3nP
mimo	mimo	k7c4
Kokořínsko	Kokořínsko	k1gNnSc4
<g/>
,	,	kIx,
např.	např.	kA
v	v	k7c6
okresech	okres	k1gInPc6
Nymburk	Nymburk	k1gInSc1
a	a	k8xC
Kolín	Kolín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
(	(	kIx(
<g/>
NPR	NPR	kA
<g/>
)	)	kIx)
</s>
<s>
NPR	NPR	kA
Novozámecký	Novozámecký	k2eAgInSc4d1
rybník	rybník	k1gInSc4
<g/>
,	,	kIx,
okres	okres	k1gInSc4
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
NPR	NPR	kA
Břehyně	Břehyně	k1gFnSc1
–	–	k?
Pecopala	Pecopal	k1gMnSc2
–	–	k?
Břehyňský	Břehyňský	k2eAgInSc4d1
rybník	rybník	k1gInSc4
s	s	k7c7
mokřady	mokřad	k1gInPc7
a	a	k8xC
kopec	kopec	k1gInSc1
Pecopala	Pecopal	k1gMnSc2
s	s	k7c7
Popelovým	Popelův	k2eAgInSc7d1
hřebenem	hřeben	k1gInSc7
a	a	k8xC
blízkými	blízký	k2eAgFnPc7d1
roklemi	rokle	k1gFnPc7
poblíž	poblíž	k7c2
Ralska	Ralsko	k1gNnSc2
<g/>
,	,	kIx,
okres	okres	k1gInSc4
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
NPR	NPR	kA
Velký	velký	k2eAgMnSc1d1
a	a	k8xC
Malý	malý	k2eAgInSc1d1
Bezděz	Bezděz	k1gInSc1
<g/>
,	,	kIx,
kopce	kopec	k1gInPc1
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
NPR	NPR	kA
Čtvrtě	čtvrt	k1gFnPc1
–	–	k?
prameniště	prameniště	k1gNnSc1
Křinecké	Křinecký	k2eAgFnPc1d1
Blatnice	Blatnice	k1gFnPc1
u	u	k7c2
obce	obec	k1gFnSc2
Mcely	Mcela	k1gFnSc2
v	v	k7c6
okrese	okres	k1gInSc6
Nymburk	Nymburk	k1gInSc1
</s>
<s>
NPR	NPR	kA
Kněžičky	kněžička	k1gFnPc1
<g/>
,	,	kIx,
stráň	stráň	k1gFnSc1
údolí	údolí	k1gNnSc2
Cidliny	Cidlina	k1gFnSc2
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Nymburk	Nymburk	k1gInSc1
</s>
<s>
NPR	NPR	kA
Libický	Libický	k2eAgInSc1d1
luh	luh	k1gInSc1
<g/>
,	,	kIx,
lužní	lužní	k2eAgInSc1d1
les	les	k1gInSc1
Labe	Labe	k1gNnSc2
<g/>
,	,	kIx,
okresy	okres	k1gInPc1
Kolín	Kolín	k1gInSc1
a	a	k8xC
Nymburk	Nymburk	k1gInSc1
</s>
<s>
Národní	národní	k2eAgFnPc1d1
přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
(	(	kIx(
<g/>
NPP	NPP	kA
<g/>
)	)	kIx)
</s>
<s>
NPP	NPP	kA
Peklo	péct	k5eAaImAgNnS
–	–	k?
údolí	údolí	k1gNnSc1
Robečského	Robečský	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
,	,	kIx,
okres	okres	k1gInSc4
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
NPP	NPP	kA
Swamp	Swamp	k1gMnSc1
–	–	k?
rašeliniště	rašeliniště	k1gNnSc4
u	u	k7c2
Máchova	Máchův	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
<g/>
,	,	kIx,
okres	okres	k1gInSc4
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
NPP	NPP	kA
Holý	holý	k2eAgInSc1d1
vrch	vrch	k1gInSc1
–	–	k?
skalnatý	skalnatý	k2eAgInSc1d1
hřeben	hřeben	k1gInSc1
a	a	k8xC
les	les	k1gInSc1
u	u	k7c2
Lhotky	Lhotka	k1gFnSc2
u	u	k7c2
Mělníka	Mělník	k1gInSc2
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
CHKO	CHKO	kA
Kokořínsko	Kokořínsko	k1gNnSc1
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Mělník	Mělník	k1gInSc1
</s>
<s>
NPP	NPP	kA
Jestřebské	Jestřebský	k2eAgFnPc4d1
slatiny	slatina	k1gFnPc4
-	-	kIx~
slatinné	slatinný	k2eAgInPc4d1
biotopy	biotop	k1gInPc4
v	v	k7c6
nivě	niva	k1gFnSc6
Robečského	Robečský	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
,	,	kIx,
okres	okres	k1gInSc4
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
NPP	NPP	kA
Klokočka	Klokočka	k1gFnSc1
–	–	k?
les	les	k1gInSc1
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
</s>
<s>
NPR	NPR	kA
Polabská	polabský	k2eAgFnSc1d1
černava	černava	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
údolí	údolí	k1gNnSc6
Pšovky	Pšovka	k1gFnSc2
u	u	k7c2
Mělnické	mělnický	k2eAgFnSc2d1
Vrutice	Vrutice	k1gFnSc2
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Mělník	Mělník	k1gInSc1
</s>
<s>
NPP	NPP	kA
Radouč	Radouč	k1gMnSc1
–	–	k?
skály	skála	k1gFnSc2
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
</s>
<s>
NPP	NPP	kA
Rečkov	Rečkov	k1gInSc1
–	–	k?
bažina	bažina	k1gFnSc1
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
</s>
<s>
NPP	NPP	kA
Slatinná	slatinný	k2eAgFnSc1d1
louka	louka	k1gFnSc1
u	u	k7c2
Velenky	Velenka	k1gFnSc2
–	–	k?
okres	okres	k1gInSc1
Nymburk	Nymburk	k1gInSc1
</s>
<s>
NPP	NPP	kA
Dlouhopolsko	Dlouhopolsko	k1gNnSc1
–	–	k?
louky	louka	k1gFnSc2
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Nymburk	Nymburk	k1gInSc1
</s>
<s>
NPP	NPP	kA
Kopičácký	Kopičácký	k2eAgInSc4d1
rybník	rybník	k1gInSc4
–	–	k?
rybník	rybník	k1gInSc1
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Nymburk	Nymburk	k1gInSc1
</s>
<s>
NPP	NPP	kA
V	v	k7c6
jezírkách	jezírko	k1gNnPc6
–	–	k?
louky	louka	k1gFnPc1
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Kolín	Kolín	k1gInSc1
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
(	(	kIx(
<g/>
PR	pr	k0
<g/>
)	)	kIx)
</s>
<s>
PR	pr	k0
Hradčanské	hradčanský	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
-	-	kIx~
čtyři	čtyři	k4xCgInPc1
rybníky	rybník	k1gInPc1
s	s	k7c7
přilehlými	přilehlý	k2eAgInPc7d1
mokřady	mokřad	k1gInPc7
<g/>
,	,	kIx,
loukami	louka	k1gFnPc7
a	a	k8xC
lesy	les	k1gInPc7
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
PR	pr	k0
Kokořínský	Kokořínský	k2eAgInSc4d1
důl	důl	k1gInSc4
-	-	kIx~
jedno	jeden	k4xCgNnSc1
z	z	k7c2
největších	veliký	k2eAgNnPc2d3
maloplošných	maloplošný	k2eAgNnPc2d1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
v	v	k7c6
ČR	ČR	kA
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
tvoří	tvořit	k5eAaImIp3nS
jádro	jádro	k1gNnSc4
CHKO	CHKO	kA
<g/>
,	,	kIx,
okresy	okres	k1gInPc1
Mělník	Mělník	k1gInSc1
a	a	k8xC
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
PR	pr	k0
Mokřady	mokřad	k1gInPc7
dolní	dolní	k2eAgFnSc2d1
Liběchovky	Liběchovka	k1gFnSc2
-	-	kIx~
okresy	okres	k1gInPc1
Mělník	Mělník	k1gInSc1
a	a	k8xC
Litoměřice	Litoměřice	k1gInPc1
</s>
<s>
PR	pr	k0
Mokřady	mokřad	k1gInPc7
horní	horní	k2eAgFnSc2d1
Liběchovky	Liběchovka	k1gFnSc2
-	-	kIx~
okresy	okres	k1gInPc1
Mělník	Mělník	k1gInSc1
a	a	k8xC
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
PR	pr	k0
Kostelecké	Kostelecké	k2eAgInPc1d1
bory	bor	k1gInPc7
-	-	kIx~
okres	okres	k1gInSc1
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
PR	pr	k0
Vlhošť	Vlhošť	k1gMnPc6
-	-	kIx~
okres	okres	k1gInSc1
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
Mokřady	mokřad	k1gInPc1
Liběchovky	Liběchovka	k1gFnSc2
a	a	k8xC
Pšovky	Pšovek	k1gInPc1
jsou	být	k5eAaImIp3nP
zapsány	zapsat	k5eAaPmNgInP
na	na	k7c4
seznam	seznam	k1gInSc4
mezinárodně	mezinárodně	k6eAd1
chráněných	chráněný	k2eAgInPc2d1
mokřadů	mokřad	k1gInPc2
podle	podle	k7c2
Ramsarské	Ramsarský	k2eAgFnSc2d1
úmluvy	úmluva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
(	(	kIx(
<g/>
PP	PP	kA
<g/>
)	)	kIx)
</s>
<s>
PP	PP	kA
Mrzínov	Mrzínov	k1gInSc1
-	-	kIx~
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
CHKO	CHKO	kA
<g/>
,	,	kIx,
nedaleko	daleko	k6eNd1
od	od	k7c2
vodní	vodní	k2eAgFnSc2d1
nádrže	nádrž	k1gFnSc2
Lhotka	Lhotka	k1gFnSc1
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Mělník	Mělník	k1gInSc1
</s>
<s>
PP	PP	kA
Na	na	k7c6
oboře	obora	k1gFnSc6
-	-	kIx~
mezi	mezi	k7c7
Dolní	dolní	k2eAgFnSc7d1
Zimoří	Zimoř	k1gFnSc7
a	a	k8xC
Truskavnou	Truskavna	k1gFnSc7
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Mělník	Mělník	k1gInSc1
</s>
<s>
PP	PP	kA
Želízky	želízko	k1gNnPc7
-	-	kIx~
mezi	mezi	k7c7
Dolní	dolní	k2eAgFnSc7d1
Zimoří	Zimoř	k1gFnSc7
a	a	k8xC
Truskavnou	Truskavna	k1gFnSc7
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Mělník	Mělník	k1gInSc1
</s>
<s>
PP	PP	kA
Stráně	stráň	k1gFnPc1
Truskavenského	Truskavenský	k2eAgInSc2d1
dolu	dol	k1gInSc2
-	-	kIx~
mezi	mezi	k7c7
Dolní	dolní	k2eAgFnSc7d1
Zimoří	Zimoř	k1gFnSc7
a	a	k8xC
Truskavnou	Truskavna	k1gFnSc7
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Mělník	Mělník	k1gInSc1
</s>
<s>
PP	PP	kA
Stráně	stráň	k1gFnPc1
Hlubokého	hluboký	k2eAgInSc2d1
dolu	dol	k1gInSc2
-	-	kIx~
u	u	k7c2
Tupadel	Tupadlo	k1gNnPc2
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Mělník	Mělník	k1gInSc1
</s>
<s>
PP	PP	kA
Osinalické	Osinalický	k2eAgFnPc4d1
bučiny	bučina	k1gFnPc4
-	-	kIx~
poblíž	poblíž	k7c2
Medonos	Medonosa	k1gFnPc2
a	a	k8xC
údolí	údolí	k1gNnSc2
Liběchovky	Liběchovka	k1gFnSc2
<g/>
,	,	kIx,
okres	okres	k1gInSc4
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
PP	PP	kA
Deštenské	Deštenský	k2eAgFnPc4d1
pastviny	pastvina	k1gFnPc4
-	-	kIx~
poblíž	poblíž	k7c2
mokřadů	mokřad	k1gInPc2
horní	horní	k2eAgFnSc2d1
Liběchovky	Liběchovka	k1gFnSc2
<g/>
,	,	kIx,
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
Dubé	Dubá	k1gFnSc2
<g/>
,	,	kIx,
okres	okres	k1gInSc4
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
PP	PP	kA
Kamenný	kamenný	k2eAgInSc4d1
vrch	vrch	k1gInSc4
u	u	k7c2
Křenova	Křenov	k1gInSc2
-	-	kIx~
poblíž	poblíž	k7c2
mokřadů	mokřad	k1gInPc2
horní	horní	k2eAgFnSc2d1
Liběchovky	Liběchovka	k1gFnSc2
<g/>
,	,	kIx,
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
Dubé	Dubá	k1gFnSc2
<g/>
,	,	kIx,
poblíž	poblíž	k7c2
Velkého	velký	k2eAgInSc2d1
Beškovského	Beškovský	k2eAgInSc2d1
kopce	kopec	k1gInSc2
<g/>
,	,	kIx,
okres	okres	k1gInSc4
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
PP	PP	kA
Černý	černý	k2eAgInSc4d1
důl	důl	k1gInSc4
-	-	kIx~
u	u	k7c2
Housky	houska	k1gFnSc2
<g/>
,	,	kIx,
okres	okres	k1gInSc4
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
PP	PP	kA
Prameny	pramen	k1gInPc1
Pšovky	Pšovka	k1gFnSc2
-	-	kIx~
u	u	k7c2
Tubože	Tubož	k1gFnSc2
<g/>
,	,	kIx,
okres	okres	k1gInSc4
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
PP	PP	kA
Špičák	špičák	k1gInSc1
u	u	k7c2
Střezivojic	Střezivojice	k1gInPc2
-	-	kIx~
asi	asi	k9
4	#num#	k4
km	km	kA
západně	západně	k6eAd1
od	od	k7c2
Housky	houska	k1gFnSc2
a	a	k8xC
6	#num#	k4
km	km	kA
jihojihozápadně	jihojihozápadně	k6eAd1
od	od	k7c2
Dubé	Dubá	k1gFnSc2
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Mělník	Mělník	k1gInSc1
</s>
<s>
PP	PP	kA
Martinské	martinský	k2eAgFnPc4d1
stěny	stěna	k1gFnPc4
-	-	kIx~
severně	severně	k6eAd1
od	od	k7c2
Kosteleckých	Kosteleckých	k2eAgInPc2d1
borů	bor	k1gInPc2
<g/>
,	,	kIx,
okres	okres	k1gInSc4
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
PP	PP	kA
Husa	husa	k1gFnSc1
-	-	kIx~
severně	severně	k6eAd1
od	od	k7c2
Kosteleckých	Kosteleckých	k2eAgInPc2d1
borů	bor	k1gInPc2
<g/>
,	,	kIx,
okres	okres	k1gInSc4
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
PP	PP	kA
Stříbrný	stříbrný	k2eAgInSc4d1
vrch	vrch	k1gInSc4
-	-	kIx~
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
Vlhoště	Vlhošť	k1gFnSc2
<g/>
,	,	kIx,
okres	okres	k1gInSc4
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
PP	PP	kA
Pod	pod	k7c7
Hvězdou	hvězda	k1gFnSc7
-	-	kIx~
západně	západně	k6eAd1
od	od	k7c2
Vlhoště	Vlhošť	k1gFnSc2
<g/>
,	,	kIx,
okres	okres	k1gInSc4
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
PP	PP	kA
Ronov	Ronov	k1gInSc1
-	-	kIx~
vrch	vrch	k1gInSc1
v	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
CHKO	CHKO	kA
<g/>
,	,	kIx,
okres	okres	k1gInSc4
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
PP	PP	kA
Okřešické	Okřešický	k2eAgFnPc4d1
louky	louka	k1gFnPc4
-	-	kIx~
louky	louka	k1gFnPc4
na	na	k7c6
území	území	k1gNnSc6
města	město	k1gNnSc2
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
PP	PP	kA
Provodínské	Provodínský	k2eAgInPc1d1
kameny	kámen	k1gInPc1
-	-	kIx~
čedičový	čedičový	k2eAgInSc1d1
suk	suk	k1gInSc1
Lysá	Lysá	k1gFnSc1
skála	skála	k1gFnSc1
u	u	k7c2
Provodína	Provodín	k1gInSc2
<g/>
,	,	kIx,
okres	okres	k1gInSc4
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
</s>
<s>
Kulturní	kulturní	k2eAgNnSc1d1
dědictví	dědictví	k1gNnSc1
</s>
<s>
Díky	díky	k7c3
zachovalé	zachovalý	k2eAgFnSc3d1
německé	německý	k2eAgFnSc3d1
lidové	lidový	k2eAgFnSc3d1
architektuře	architektura	k1gFnSc3
získaly	získat	k5eAaPmAgFnP
některé	některý	k3yIgFnPc1
vesnice	vesnice	k1gFnPc1
status	status	k1gInSc4
vesnická	vesnický	k2eAgFnSc1d1
památková	památkový	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
(	(	kIx(
<g/>
Dobřeň	Dobřeň	k1gFnSc1
<g/>
,	,	kIx,
Olešno	Olešno	k1gNnSc1
<g/>
,	,	kIx,
Nosálov	Nosálov	k1gInSc1
<g/>
,	,	kIx,
Nové	Nové	k2eAgFnPc1d1
Osinalice	Osinalice	k1gFnPc1
<g/>
,	,	kIx,
Lhota	Lhota	k1gFnSc1
u	u	k7c2
Zátyní	zátyň	k1gFnPc2
a	a	k8xC
Žďár	Žďár	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jiné	jiná	k1gFnSc2
vesnická	vesnický	k2eAgFnSc1d1
památková	památkový	k2eAgFnSc1d1
zóna	zóna	k1gFnSc1
(	(	kIx(
<g/>
Brocno	Brocno	k1gNnSc1
<g/>
,	,	kIx,
Tubož	Tubož	k1gFnSc1
<g/>
,	,	kIx,
Bukovec	Bukovec	k1gInSc1
<g/>
,	,	kIx,
Střezivojice	Střezivojice	k1gFnSc1
<g/>
,	,	kIx,
Jestřebice	Jestřebice	k1gFnSc1
<g/>
,	,	kIx,
Vidim	Vidim	k?
<g/>
,	,	kIx,
Lobeč	Lobeč	k1gInSc1
<g/>
,	,	kIx,
Sitné	Sitné	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
městech	město	k1gNnPc6
Dubá	Dubá	k1gFnSc1
na	na	k7c6
Českolipsku	Českolipsko	k1gNnSc6
a	a	k8xC
Mšeno	Mšeno	k6eAd1
na	na	k7c6
Kokořínsku	Kokořínsko	k1gNnSc6
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
městská	městský	k2eAgFnSc1d1
památková	památkový	k2eAgFnSc1d1
zóna	zóna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Dopravní	dopravní	k2eAgFnSc1d1
obslužnost	obslužnost	k1gFnSc1
</s>
<s>
Železniční	železniční	k2eAgFnSc2d1
tratě	trať	k1gFnSc2
centrální	centrální	k2eAgFnSc2d1
částí	část	k1gFnSc7
Kokořínska	Kokořínsko	k1gNnSc2
neprocházejí	procházet	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jižním	jižní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
CHKO	CHKO	kA
vedou	vést	k5eAaImIp3nP
Trať	trať	k1gFnSc4
076	#num#	k4
z	z	k7c2
Mělníka	Mělník	k1gInSc2
do	do	k7c2
Mladé	mladý	k2eAgFnSc6d1
Boleslavi	Boleslaev	k1gFnSc6
(	(	kIx(
<g/>
turisticky	turisticky	k6eAd1
zajímavé	zajímavý	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
stanice	stanice	k1gFnSc1
Lhotka	Lhotka	k1gFnSc1
u	u	k7c2
Mělníka	Mělník	k1gInSc2
<g/>
,	,	kIx,
Nebužely	Nebužely	k1gFnSc2
a	a	k8xC
Mšeno	Mšeno	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
trať	trať	k1gFnSc1
072	#num#	k4
z	z	k7c2
Lysé	Lysá	k1gFnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
do	do	k7c2
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
přes	přes	k7c4
Liběchov	Liběchov	k1gInSc4
a	a	k8xC
Štětí	štětit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severním	severní	k2eAgInSc7d1
okrajem	okraj	k1gInSc7
prochází	procházet	k5eAaImIp3nS
Trať	trať	k1gFnSc1
087	#num#	k4
z	z	k7c2
České	český	k2eAgFnSc2d1
Lípy	lípa	k1gFnSc2
do	do	k7c2
Lovosic	Lovosice	k1gInPc2
(	(	kIx(
<g/>
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
CHKO	CHKO	kA
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
stanice	stanice	k1gFnPc1
Blíževedly	Blíževedlo	k1gNnPc7
<g/>
,	,	kIx,
Kravaře	Kravaře	k1gInPc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
a	a	k8xC
Stvolínky	Stvolínka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
částmi	část	k1gFnPc7
CHKO	CHKO	kA
pak	pak	k8xC
prochází	procházet	k5eAaImIp3nS
Trať	trať	k1gFnSc4
080	#num#	k4
mezi	mezi	k7c7
Českou	český	k2eAgFnSc7d1
Lípou	lípa	k1gFnSc7
a	a	k8xC
Mladou	mladý	k2eAgFnSc7d1
Boleslaví	Boleslavý	k2eAgMnPc5d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
části	část	k1gFnSc6
severní	severní	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
oblasti	oblast	k1gFnSc2
pak	pak	k6eAd1
vede	vést	k5eAaImIp3nS
Trať	trať	k1gFnSc4
086	#num#	k4
z	z	k7c2
České	český	k2eAgFnSc2d1
Lípy	lípa	k1gFnSc2
do	do	k7c2
Liberce	Liberec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kokořínskem	Kokořínsko	k1gNnSc7
prochází	procházet	k5eAaImIp3nS
silnice	silnice	k1gFnSc1
I	I	kA
<g/>
/	/	kIx~
<g/>
9	#num#	k4
z	z	k7c2
Prahy	Praha	k1gFnSc2
na	na	k7c4
sever	sever	k1gInSc4
přes	přes	k7c4
Mělník	Mělník	k1gInSc4
a	a	k8xC
Dubou	Duba	k1gMnSc7
na	na	k7c4
Českou	český	k2eAgFnSc4d1
Lípu	lípa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
oběma	dva	k4xCgInPc7
části	část	k1gFnSc6
CHKO	CHKO	kA
probíhá	probíhat	k5eAaImIp3nS
silnice	silnice	k1gFnPc4
I	i	k9
<g/>
/	/	kIx~
<g/>
38	#num#	k4
mezi	mezi	k7c7
Mladou	mladá	k1gFnSc7
Boleslaví	Boleslavý	k2eAgMnPc1d1
a	a	k8xC
Jestřebím	Jestřebí	k1gNnPc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
napojuje	napojovat	k5eAaImIp3nS
na	na	k7c4
výše	vysoce	k6eAd2
zmíněnou	zmíněný	k2eAgFnSc4d1
silnici	silnice	k1gFnSc4
I	I	kA
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
východním	východní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
CHKO	CHKO	kA
vede	vést	k5eAaImIp3nS
silnice	silnice	k1gFnPc4
II	II	kA
<g/>
/	/	kIx~
<g/>
273	#num#	k4
ve	v	k7c6
směru	směr	k1gInSc6
Mělník	Mělník	k1gInSc1
–	–	k?
Mšeno	Mšeno	k6eAd1
–	–	k?
Doksy	Doksy	k1gInPc1
<g/>
,	,	kIx,
ve	v	k7c6
Mšeně	Mšen	k1gInSc6
z	z	k7c2
ní	on	k3xPp3gFnSc2
odbočuje	odbočovat	k5eAaImIp3nS
silnice	silnice	k1gFnPc4
II	II	kA
<g/>
/	/	kIx~
<g/>
259	#num#	k4
na	na	k7c6
Dubou	Duba	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
Dubé	Dubá	k1gFnSc2
je	být	k5eAaImIp3nS
CHKO	CHKO	kA
přeťata	přetít	k5eAaPmNgFnS
západovýchodním	západovýchodní	k2eAgInSc7d1
směrem	směr	k1gInSc7
silnicí	silnice	k1gFnSc7
II	II	kA
<g/>
/	/	kIx~
<g/>
260	#num#	k4
od	od	k7c2
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
(	(	kIx(
<g/>
Úštěk	Úštěk	k1gMnSc1
–	–	k?
Tuhaň	Tuhaň	k1gMnSc1
–	–	k?
Dubá	Dubá	k1gFnSc1
<g/>
)	)	kIx)
Z	z	k7c2
dubé	dubá	k1gFnSc2
pak	pak	k6eAd1
v	v	k7c6
témž	týž	k3xTgInSc6
směru	směr	k1gInSc6
pokračuje	pokračovat	k5eAaImIp3nS
silnice	silnice	k1gFnSc1
II	II	kA
<g/>
/	/	kIx~
<g/>
270	#num#	k4
na	na	k7c4
Doksy	Doksy	k1gInPc4
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
dále	daleko	k6eAd2
vede	vést	k5eAaImIp3nS
na	na	k7c4
Mimoň	Mimoň	k1gFnSc4
a	a	k8xC
tím	ten	k3xDgNnSc7
protíná	protínat	k5eAaImIp3nS
část	část	k1gFnSc1
CHKO	CHKO	kA
Máchův	Máchův	k2eAgInSc1d1
Kraj	kraj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silnice	silnice	k1gFnSc1
260	#num#	k4
z	z	k7c2
jihozápadní	jihozápadní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
lemuje	lemovat	k5eAaImIp3nS
i	i	k9
severní	severní	k2eAgFnSc1d1
část	část	k1gFnSc1
CHKO	CHKO	kA
a	a	k8xC
v	v	k7c6
Tuhani	Tuhaň	k1gFnSc6
se	se	k3xPyFc4
k	k	k7c3
ní	on	k3xPp3gFnSc3
připojuje	připojovat	k5eAaImIp3nS
silnice	silnice	k1gFnSc1
II	II	kA
<g/>
/	/	kIx~
<g/>
269	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
oblast	oblast	k1gFnSc1
napojuje	napojovat	k5eAaImIp3nS
na	na	k7c4
Litoměřice	Litoměřice	k1gInPc4
a	a	k8xC
Lovosice	Lovosice	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
mezi	mezi	k7c7
Kokořínském	Kokořínský	k2eAgInSc6d1
a	a	k8xC
Českým	český	k2eAgNnPc3d1
středohořím	středohoří	k1gNnPc3
souběžně	souběžně	k6eAd1
s	s	k7c7
železniční	železniční	k2eAgFnSc7d1
tratí	trať	k1gFnSc7
087	#num#	k4
prochází	procházet	k5eAaImIp3nS
i	i	k9
silnice	silnice	k1gFnSc1
I	I	kA
<g/>
/	/	kIx~
<g/>
15	#num#	k4
Litoměřice	Litoměřice	k1gInPc4
–	–	k?
Zahrádky	zahrádka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
silnic	silnice	k1gFnPc2
III	III	kA
<g/>
.	.	kIx.
třídy	třída	k1gFnPc1
stojí	stát	k5eAaImIp3nP
za	za	k7c4
zmínku	zmínka	k1gFnSc4
především	především	k9
silnice	silnice	k1gFnPc4
III	III	kA
<g/>
/	/	kIx~
<g/>
25931	#num#	k4
ze	z	k7c2
Lhotky	Lhotka	k1gFnSc2
u	u	k7c2
Mělníka	Mělník	k1gInSc2
Kokořínským	Kokořínský	k2eAgInSc7d1
dolem	dol	k1gInSc7
kolem	kolem	k7c2
hradu	hrad	k1gInSc2
Kokořín	Kokořína	k1gFnPc2
a	a	k8xC
přes	přes	k7c4
Vojtěchov	Vojtěchov	k1gInSc4
do	do	k7c2
Mšena-Ráje	Mšena-Ráj	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Meziměstské	meziměstský	k2eAgInPc1d1
autobusy	autobus	k1gInPc1
na	na	k7c6
trase	trasa	k1gFnSc6
silnice	silnice	k1gFnSc2
č.	č.	k?
9	#num#	k4
(	(	kIx(
<g/>
Liběchov	Liběchov	k1gInSc1
<g/>
,	,	kIx,
Želízy	Želízy	k1gInPc1
<g/>
,	,	kIx,
Tupadly	Tupadlo	k1gNnPc7
<g/>
,	,	kIx,
Medonosy	Medonosa	k1gFnPc1
<g/>
,	,	kIx,
Dubá	Dubá	k1gFnSc1
<g/>
)	)	kIx)
projíždějí	projíždět	k5eAaImIp3nP
poměrně	poměrně	k6eAd1
často	často	k6eAd1
<g/>
,	,	kIx,
ne	ne	k9
všechny	všechen	k3xTgFnPc1
však	však	k9
v	v	k7c6
menších	malý	k2eAgFnPc6d2
obcích	obec	k1gFnPc6
po	po	k7c6
trase	trasa	k1gFnSc6
zastavují	zastavovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
dobré	dobrý	k2eAgNnSc4d1
meziměstské	meziměstský	k2eAgNnSc4d1
autobusové	autobusový	k2eAgNnSc4d1
i	i	k8xC
železniční	železniční	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
s	s	k7c7
Mělníkem	Mělník	k1gInSc7
a	a	k8xC
Mladou	mladý	k2eAgFnSc7d1
Boleslaví	Boleslavý	k2eAgMnPc1d1
má	mít	k5eAaImIp3nS
město	město	k1gNnSc1
Mšeno	Mšeen	k2eAgNnSc1d1
<g/>
,	,	kIx,
přes	přes	k7c4
krajskou	krajský	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
mezi	mezi	k7c7
Mšenem	Mšen	k1gMnSc7
a	a	k8xC
Dubou	Duba	k1gMnSc7
(	(	kIx(
<g/>
silnice	silnice	k1gFnSc1
II	II	kA
<g/>
/	/	kIx~
<g/>
259	#num#	k4
<g/>
)	)	kIx)
však	však	k9
veřejná	veřejný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
prakticky	prakticky	k6eAd1
neexistuje	existovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místní	místní	k2eAgInPc1d1
spoje	spoj	k1gInPc1
do	do	k7c2
větších	veliký	k2eAgFnPc2d2
obcí	obec	k1gFnPc2
na	na	k7c6
Kokořínsku	Kokořínsko	k1gNnSc6
fungují	fungovat	k5eAaImIp3nP
dobře	dobře	k6eAd1
ve	v	k7c4
všední	všední	k2eAgInPc4d1
dny	den	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
oblasti	oblast	k1gFnSc6
Mšenska	Mšensko	k1gNnSc2
a	a	k8xC
Kokořínska	Kokořínsko	k1gNnSc2
zajišťuje	zajišťovat	k5eAaImIp3nS
místní	místní	k2eAgFnSc1d1
obslužnost	obslužnost	k1gFnSc1
převážně	převážně	k6eAd1
v	v	k7c6
pracovních	pracovní	k2eAgInPc6d1
dnech	den	k1gInPc6
minibusové	minibusový	k2eAgFnSc2d1
linky	linka	k1gFnSc2
dopravce	dopravce	k1gMnSc1
Kokořínský	Kokořínský	k2eAgMnSc1d1
SOK	sok	k1gMnSc1
<g/>
,	,	kIx,
vlastněného	vlastněný	k2eAgInSc2d1
svazkem	svazek	k1gInSc7
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Pískovcové	pískovcový	k2eAgFnPc1d1
skály	skála	k1gFnPc1
okolo	okolo	k7c2
Vlhoště	Vlhošť	k1gFnSc2
</s>
<s>
Cesta	cesta	k1gFnSc1
na	na	k7c4
Ronov	Ronov	k1gInSc4
</s>
<s>
Skalní	skalní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
Husa	Hus	k1gMnSc2
</s>
<s>
Les	les	k1gInSc4
v	v	k7c6
CHKO	CHKO	kA
Kokořínsko	Kokořínsko	k1gNnSc1
</s>
<s>
Úvozová	úvozový	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
na	na	k7c4
Uhelný	uhelný	k2eAgInSc4d1
vrch	vrch	k1gInSc4
</s>
<s>
Okraj	okraj	k1gInSc1
pískovcových	pískovcový	k2eAgFnPc2d1
skal	skála	k1gFnPc2
nedaleko	daleko	k6eNd1
od	od	k7c2
Zakšína	Zakšín	k1gInSc2
</s>
<s>
Letecký	letecký	k2eAgInSc1d1
snímek	snímek	k1gInSc1
hradu	hrad	k1gInSc2
Kokořín	Kokořína	k1gFnPc2
</s>
<s>
Cinibulkova	Cinibulkův	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
</s>
<s>
Pískovcový	pískovcový	k2eAgInSc1d1
převis	převis	k1gInSc1
Krápník	krápník	k1gInSc1
</s>
<s>
Máchovo	Máchův	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
pozadí	pozadí	k1gNnSc6
Bezděz	Bezděz	k1gInSc1
</s>
<s>
Bezděz	Bezděz	k1gInSc1
</s>
<s>
Skalní	skalní	k2eAgFnSc1d1
brána	brána	k1gFnSc1
v	v	k7c6
Hradčanských	hradčanský	k2eAgFnPc6d1
stěnách	stěna	k1gFnPc6
</s>
<s>
Lysá	lysý	k2eAgFnSc1d1
skála	skála	k1gFnSc1
</s>
<s>
hrad	hrad	k1gInSc1
Jestřebí	Jestřebí	k1gNnSc2
</s>
<s>
Novozámecký	Novozámecký	k2eAgInSc1d1
rybník	rybník	k1gInSc1
</s>
<s>
Helfenburk	Helfenburk	k1gInSc1
u	u	k7c2
Úštěka	Úštěek	k1gInSc2
</s>
<s>
Čertovy	čertův	k2eAgFnPc1d1
hlavy	hlava	k1gFnPc1
</s>
<s>
Chudý	chudý	k2eAgInSc1d1
hrádek	hrádek	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
–	–	k?
Stručný	stručný	k2eAgMnSc1d1
turistický	turistický	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cheb	Cheb	k1gInSc1
<g/>
:	:	kIx,
Music	Music	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85925	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Kokořínsko	Kokořínsko	k1gNnSc1
<g/>
,	,	kIx,
s.	s.	k?
221	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Richard	Richard	k1gMnSc1
Grégr	Grégr	k1gMnSc1
<g/>
:	:	kIx,
Kokořínsko	Kokořínsko	k1gNnSc1
aneb	aneb	k?
Polomené	polomený	k2eAgFnSc2d1
hory	hora	k1gFnSc2
<g/>
,	,	kIx,
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
.	.	kIx.
„	„	k?
<g/>
Území	území	k1gNnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
Polomené	polomený	k2eAgFnSc2d1
hory	hora	k1gFnSc2
<g/>
,	,	kIx,
Dubská	dubský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
či	či	k8xC
Dubské	Dubské	k2eAgNnSc1d1
Švýcarsko	Švýcarsko	k1gNnSc1
–	–	k?
podle	podle	k7c2
městečka	městečko	k1gNnSc2
Dubá	Dubá	k1gFnSc1
<g/>
,	,	kIx,
ležícího	ležící	k2eAgInSc2d1
téměř	téměř	k6eAd1
ve	v	k7c6
středu	střed	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Kokořínsko	Kokořínsko	k1gNnSc4
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
pro	pro	k7c4
ochránce	ochránce	k1gMnSc4
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
↑	↑	k?
AUTORSKÝ	autorský	k2eAgInSc4d1
KOLEKTIV	kolektiv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Horopisné	horopisný	k2eAgNnSc4d1
členění	členění	k1gNnSc4
<g/>
,	,	kIx,
s.	s.	k?
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KOLEKTIV	kolektivum	k1gNnPc2
AUTORŮ	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dubské	Dubské	k2eAgFnPc4d1
skály	skála	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kokořínsko	Kokořínsko	k1gNnSc1
a	a	k8xC
přilehlé	přilehlý	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Litoměřice	Litoměřice	k1gInPc4
<g/>
:	:	kIx,
Vod-ka	Vod-ka	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
422	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
239	#num#	k4
<g/>
-	-	kIx~
<g/>
5407	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Vlci	vlk	k1gMnPc1
jsou	být	k5eAaImIp3nP
zpět	zpět	k6eAd1
<g/>
:	:	kIx,
fotopast	fotopast	k1gInSc1
u	u	k7c2
Máchova	Máchův	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
zachytila	zachytit	k5eAaPmAgFnS
mládě	mládě	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Smečka	smečka	k1gFnSc1
vlků	vlk	k1gMnPc2
se	se	k3xPyFc4
stále	stále	k6eAd1
pohybuje	pohybovat	k5eAaImIp3nS
na	na	k7c4
Dokesku	Dokeska	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AOPK	AOPK	kA
ČR	ČR	kA
-	-	kIx~
RP	RP	kA
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Kokořínsko-Máchův	Kokořínsko-Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
2015-11-09	2015-11-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nové	Nové	k2eAgInPc4d1
údaje	údaj	k1gInPc4
o	o	k7c6
výskytu	výskyt	k1gInSc6
vlka	vlk	k1gMnSc2
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
Kokořínska	Kokořínsko	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AOPK	AOPK	kA
ČR	ČR	kA
_	_	kIx~
RP	RP	kA
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Kokořínsko-Máchův	Kokořínsko-Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
2018-03-23	2018-03-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MĚLNÍK	Mělník	k1gInSc1
<g/>
,	,	kIx,
Regionální	regionální	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Regionální	regionální	k2eAgFnSc1d1
muzeum	muzeum	k1gNnSc4
Mělník	Mělník	k1gInSc1
<g/>
.	.	kIx.
www.muzeum-melnik.cz	www.muzeum-melnik.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
AUTORSKÝ	autorský	k2eAgInSc4d1
KOLEKTIV	kolektiv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnSc4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
3	#num#	k4
<g/>
:	:	kIx,
Informatorium	Informatorium	k1gNnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85368	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
CHKO	CHKO	kA
Kokořínsko	Kokořínsko	k1gNnSc1
<g/>
,	,	kIx,
s.	s.	k?
57	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Radmila	Radmila	k1gFnSc1
Pokorná	Pokorná	k1gFnSc1
<g/>
:	:	kIx,
Chráněná	chráněný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Kokořínsko	Kokořínsko	k1gNnSc1
se	se	k3xPyFc4
rozšíří	rozšířit	k5eAaPmIp3nS
o	o	k7c4
polovinu	polovina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obce	obec	k1gFnPc4
proti	proti	k7c3
tomu	ten	k3xDgNnSc3
protestují	protestovat	k5eAaBmIp3nP
<g/>
,	,	kIx,
Českolipský	českolipský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
↑	↑	k?
Obce	obec	k1gFnPc1
z	z	k7c2
okolí	okolí	k1gNnSc2
Máchova	Máchův	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
nechtějí	chtít	k5eNaImIp3nP
rozšíření	rozšíření	k1gNnSc4
CHKO	CHKO	kA
Kokořínsko	Kokořínsko	k1gNnSc4
<g/>
,	,	kIx,
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
20101	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Radmila	Radmila	k1gFnSc1
Pokorná	Pokorná	k1gFnSc1
<g/>
:	:	kIx,
Nebudeme	být	k5eNaImBp1nP
<g />
.	.	kIx.
</s>
<s hack="1">
rozhodovat	rozhodovat	k5eAaImF
od	od	k7c2
stolu	stol	k1gInSc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Českolipský	českolipský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
,	,	kIx,
opis	opis	k1gInSc1
na	na	k7c6
webu	web	k1gInSc6
Ministerstva	ministerstvo	k1gNnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
↑	↑	k?
Ministr	ministr	k1gMnSc1
Chalupa	Chalupa	k1gMnSc1
<g/>
:	:	kIx,
Rozšíření	rozšíření	k1gNnSc1
CHKO	CHKO	kA
Kokořínsko	Kokořínsko	k1gNnSc1
má	mít	k5eAaImIp3nS
moji	můj	k3xOp1gFnSc4
podporu	podpora	k1gFnSc4
<g/>
,	,	kIx,
Ministerstvo	ministerstvo	k1gNnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
Michaela	Michaela	k1gFnSc1
Jendeková	Jendeková	k1gFnSc1
<g/>
,	,	kIx,
tiskové	tiskový	k2eAgNnSc1d1
oddělení	oddělení	k1gNnSc1
MŽP	MŽP	kA
<g/>
,	,	kIx,
Parlamentní	parlamentní	k2eAgInPc4d1
listy	list	k1gInPc4
<g/>
,	,	kIx,
22	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
↑	↑	k?
Tiskové	tiskový	k2eAgNnSc4d1
oddělení	oddělení	k1gNnSc4
MŽP	MŽP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Máchova	Máchův	k2eAgInSc2d1
kraje	kraj	k1gInSc2
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
vlk	vlk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
dnes	dnes	k6eAd1
schválila	schválit	k5eAaPmAgFnS
rozšíření	rozšíření	k1gNnSc4
CHKO	CHKO	kA
Kokořínsko	Kokořínsko	k1gNnSc4
o	o	k7c4
Dokesko	Dokesko	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
2014-04-09	2014-04-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnSc4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
58	#num#	k4
<g/>
↑	↑	k?
NOVOTNÁ	Novotná	k1gFnSc1
<g/>
,	,	kIx,
Mgr	Mgr	kA
Daniela	Daniela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kokořínsko	Kokořínsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
a.s.	a.s.	k?
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7033	#num#	k4
<g/>
-	-	kIx~
<g/>
843	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Turistická	turistický	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Českolipsko	Českolipsko	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Kokořínsko	Kokořínsko	k1gNnSc1
-	-	kIx~
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
na	na	k7c4
Mapy	mapa	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
Správy	správa	k1gFnSc2
CHKO	CHKO	kA
</s>
<s>
Kokořínsko	Kokořínsko	k1gNnSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
</s>
<s>
Nařízení	nařízení	k1gNnPc1
vlády	vláda	k1gFnSc2
č.	č.	k?
176	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
kterým	který	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
se	se	k3xPyFc4
vyhlašuje	vyhlašovat	k5eAaImIp3nS
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgMnSc1d1
kraj	kraj	k7c2
•	•	k?
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Břehyně	Břehyně	k1gFnSc1
–	–	k?
Pecopala	Pecopal	k1gMnSc2
•	•	k?
Jezevčí	jezevčí	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Novozámecký	Novozámecký	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
a	a	k8xC
Malý	malý	k2eAgInSc1d1
Bezděz	Bezděz	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Jestřebské	Jestřebský	k2eAgFnPc1d1
slatiny	slatina	k1gFnPc1
•	•	k?
Panská	panská	k1gFnSc1
skála	skála	k1gFnSc1
•	•	k?
Peklo	peklo	k1gNnSc1
•	•	k?
Swamp	Swamp	k1gMnSc1
Přírodní	přírodní	k2eAgMnSc1d1
rezervace	rezervace	k1gFnPc4
</s>
<s>
Hradčanské	hradčanský	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Jílovka	jílovka	k1gFnSc1
•	•	k?
Klíč	klíč	k1gInSc1
•	•	k?
Kokořínský	Kokořínský	k2eAgInSc4d1
důl	důl	k1gInSc4
•	•	k?
Kostelecké	Kostelecké	k2eAgInPc4d1
bory	bor	k1gInPc4
•	•	k?
Luž	Luž	k1gFnSc1
•	•	k?
Mokřady	mokřad	k1gInPc1
horní	horní	k2eAgFnSc2d1
Liběchovky	Liběchovka	k1gFnSc2
•	•	k?
Ralsko	Ralsko	k1gNnSc1
•	•	k?
Vlhošť	Vlhošť	k1gFnSc1
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Bobří	bobří	k2eAgFnSc1d1
soutěska	soutěska	k1gFnSc1
•	•	k?
Brazilka	Brazilka	k1gFnSc1
•	•	k?
Cihelenské	Cihelenský	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Černý	černý	k2eAgInSc1d1
důl	důl	k1gInSc1
•	•	k?
Červený	červený	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
–	–	k?
mokřad	mokřad	k1gInSc4
v	v	k7c6
nivě	niva	k1gFnSc6
Šporky	Šporka	k1gMnSc2
•	•	k?
Deštenské	Deštenský	k2eAgFnPc1d1
pastviny	pastvina	k1gFnPc1
•	•	k?
Děvín	Děvín	k1gInSc1
a	a	k8xC
Ostrý	ostrý	k2eAgMnSc1d1
•	•	k?
Divadlo	divadlo	k1gNnSc1
•	•	k?
Dutý	dutý	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Farská	farský	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Husa	Hus	k1gMnSc2
•	•	k?
Jelení	jelení	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
•	•	k?
Kamenný	kamenný	k2eAgInSc4d1
vrch	vrch	k1gInSc4
u	u	k7c2
Křenova	Křenov	k1gInSc2
•	•	k?
Kaňon	kaňon	k1gInSc1
potoka	potok	k1gInSc2
Kolné	Kolná	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Manušické	Manušický	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Martinské	martinský	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
•	•	k?
Meandry	meandr	k1gInPc1
Ploučnice	Ploučnice	k1gFnSc2
u	u	k7c2
Mimoně	Mimoň	k1gFnSc2
•	•	k?
Naděje	naděje	k1gFnSc1
•	•	k?
Niva	niva	k1gFnSc1
Ploučnice	Ploučnice	k1gFnSc1
u	u	k7c2
Žizníkova	Žizníkův	k2eAgInSc2d1
•	•	k?
Okřešické	Okřešický	k2eAgFnPc1d1
louky	louka	k1gFnPc1
•	•	k?
Osinalické	Osinalický	k2eAgFnPc1d1
bučiny	bučina	k1gFnPc1
•	•	k?
Pískovna	pískovna	k1gFnSc1
Žizníkov	Žizníkov	k1gInSc1
•	•	k?
Pod	pod	k7c7
Hvězdou	hvězda	k1gFnSc7
•	•	k?
Prameny	pramen	k1gInPc1
Pšovky	Pšovka	k1gFnSc2
•	•	k?
Provodínské	Provodínský	k2eAgInPc1d1
kameny	kámen	k1gInPc1
•	•	k?
Pustý	pustý	k2eAgInSc1d1
zámek	zámek	k1gInSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc2
Černého	Černý	k1gMnSc2
rybníka	rybník	k1gInSc2
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
Mařeničky	Mařenička	k1gFnSc2
•	•	k?
Ronov	Ronov	k1gInSc1
•	•	k?
Skalice	Skalice	k1gFnSc2
u	u	k7c2
České	český	k2eAgFnSc2d1
Lípy	lípa	k1gFnSc2
•	•	k?
Stohánek	Stohánek	k1gMnSc1
•	•	k?
Stružnické	Stružnický	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Stříbrný	stříbrný	k1gInSc1
vrch	vrch	k1gInSc1
•	•	k?
Široký	široký	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
U	u	k7c2
Rozmoklé	rozmoklý	k2eAgFnSc2d1
žáby	žába	k1gFnSc2
•	•	k?
Vranovské	vranovský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Zahrádky	zahrádka	k1gFnSc2
u	u	k7c2
České	český	k2eAgFnSc2d1
Lípy	lípa	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Litoměřice	Litoměřice	k1gInPc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Dolní	dolní	k2eAgNnSc1d1
Poohří	Poohří	k1gNnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Lovoš	Lovoš	k1gMnSc1
•	•	k?
Milešovka	Milešovka	k1gFnSc1
•	•	k?
Sedlo	sedlo	k1gNnSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bílé	bílý	k2eAgFnSc2d1
stráně	stráň	k1gFnSc2
•	•	k?
Borečský	Borečský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Dubí	dubí	k1gNnSc2
hora	hora	k1gFnSc1
•	•	k?
Kleneč	Kleneč	k1gFnSc1
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Březina	Březina	k1gMnSc1
•	•	k?
Holý	Holý	k1gMnSc1
vrch	vrch	k1gInSc1
•	•	k?
Holý	holý	k2eAgInSc1d1
vrch	vrch	k1gInSc1
u	u	k7c2
Hlinné	hlinný	k2eAgFnSc2d1
•	•	k?
Kalvárie	Kalvárie	k1gFnSc2
•	•	k?
Lipská	lipský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Loužek	loužka	k1gFnPc2
•	•	k?
Mokřady	mokřad	k1gInPc1
dolní	dolní	k2eAgFnSc2d1
Liběchovky	Liběchovka	k1gFnSc2
•	•	k?
Myslivna	myslivna	k1gFnSc1
•	•	k?
Na	na	k7c6
Černčí	Černčí	k2eAgFnSc6d1
•	•	k?
Pístecký	Pístecký	k2eAgInSc1d1
les	les	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1
stráně	stráň	k1gFnPc1
u	u	k7c2
Štětí	štětit	k5eAaImIp3nP
•	•	k?
Dobříňský	Dobříňský	k2eAgInSc1d1
háj	háj	k1gInSc1
•	•	k?
Evaňská	Evaňský	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
•	•	k?
Říp	Říp	k1gInSc1
•	•	k?
Hradiště	Hradiště	k1gNnSc1
•	•	k?
Košťálov	Košťálovo	k1gNnPc2
•	•	k?
Koštice	Koštice	k1gFnSc2
•	•	k?
Kuzov	Kuzov	k1gInSc1
•	•	k?
Mokřad	mokřad	k1gInSc1
pod	pod	k7c7
Terezínskou	terezínský	k2eAgFnSc7d1
pevností	pevnost	k1gFnSc7
•	•	k?
Na	na	k7c6
Dlouhé	Dlouhé	k2eAgFnSc6d1
stráni	stráň	k1gFnSc6
•	•	k?
Písčiny	písčina	k1gFnSc2
u	u	k7c2
Oleška	Olešek	k1gInSc2
•	•	k?
Plešivec	plešivec	k1gMnSc1
•	•	k?
Radobýl	Radobýl	k1gMnSc1
•	•	k?
Radouň	Radouň	k1gFnSc1
•	•	k?
Skalky	skalka	k1gFnSc2
u	u	k7c2
Třebutiček	Třebutička	k1gFnPc2
•	•	k?
Slatiniště	slatiniště	k1gNnSc4
u	u	k7c2
Vrbky	vrbka	k1gFnSc2
•	•	k?
Sovice	sovice	k1gFnSc2
u	u	k7c2
Brzánek	Brzánka	k1gFnPc2
•	•	k?
Stráně	stráň	k1gFnSc2
nad	nad	k7c7
Suchým	suchý	k2eAgInSc7d1
potokem	potok	k1gInSc7
•	•	k?
Stráně	stráň	k1gFnSc2
u	u	k7c2
Drahobuzi	Drahobuze	k1gFnSc4
•	•	k?
Stráně	stráň	k1gFnPc1
u	u	k7c2
Velkého	velký	k2eAgInSc2d1
Újezdu	Újezd	k1gInSc2
•	•	k?
Údolí	údolí	k1gNnSc1
Podbradeckého	Podbradecký	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
V	v	k7c6
kuksu	kuks	k1gInSc6
•	•	k?
Vrch	vrch	k1gInSc1
Hazmburk	Hazmburk	k1gInSc1
•	•	k?
Vrbka	vrbka	k1gFnSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Mělník	Mělník	k1gInSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Kokořínsko	Kokořínsko	k6eAd1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Holý	holý	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Polabská	polabský	k2eAgFnSc1d1
černava	černava	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Dřínovská	Dřínovský	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Kokořínský	Kokořínský	k2eAgInSc1d1
důl	důl	k1gInSc1
•	•	k?
Kopeč	Kopeč	k1gInSc4
•	•	k?
Mokřady	mokřad	k1gInPc4
dolní	dolní	k2eAgFnSc2d1
Liběchovky	Liběchovka	k1gFnSc2
•	•	k?
Mokřady	mokřad	k1gInPc7
horní	horní	k2eAgFnSc2d1
Liběchovky	Liběchovka	k1gFnSc2
•	•	k?
Slatinná	slatinný	k2eAgFnSc1d1
louka	louka	k1gFnSc1
u	u	k7c2
Liblic	Liblice	k1gFnPc2
•	•	k?
Úpor-Černínovsko	Úpor-Černínovsko	k1gNnSc4
•	•	k?
Všetatská	Všetatský	k2eAgFnSc1d1
černava	černava	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1
Pšovka	Pšovka	k1gFnSc1
•	•	k?
Hlaváčková	Hlaváčková	k1gFnSc1
stráň	stráň	k1gFnSc1
•	•	k?
Hostibejk	Hostibejk	k1gInSc1
•	•	k?
Jiřina	Jiřina	k1gFnSc1
•	•	k?
Minická	Minický	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Mrzínov	Mrzínov	k1gInSc1
•	•	k?
Na	na	k7c6
oboře	obora	k1gFnSc6
•	•	k?
Netřebská	Netřebský	k2eAgFnSc1d1
slaniska	slanisko	k1gNnSc2
•	•	k?
Pahorek	pahorek	k1gInSc1
u	u	k7c2
Ledčic	Ledčice	k1gFnPc2
•	•	k?
Písčina	písčina	k1gFnSc1
u	u	k7c2
Tišic	Tišice	k1gInPc2
•	•	k?
Písčina	písčina	k1gFnSc1
u	u	k7c2
Tuhaně	Tuhaň	k1gFnSc2
•	•	k?
Polabí	Polabí	k1gNnSc1
u	u	k7c2
Kostelce	Kostelec	k1gInSc2
•	•	k?
Slaná	slaný	k2eAgFnSc1d1
louka	louka	k1gFnSc1
u	u	k7c2
Újezdce	Újezdec	k1gMnSc2
•	•	k?
Sprašová	sprašový	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
u	u	k7c2
Zeměch	Zeměcha	k1gFnPc2
•	•	k?
Stráně	stráň	k1gFnPc4
Hlubokého	hluboký	k2eAgInSc2d1
dolu	dol	k1gInSc2
•	•	k?
Stráně	stráň	k1gFnPc4
Truskavenského	Truskavenský	k2eAgInSc2d1
dolu	dol	k1gInSc2
•	•	k?
Špičák	Špičák	k1gMnSc1
u	u	k7c2
Střezivojic	Střezivojice	k1gFnPc2
•	•	k?
Vehlovické	Vehlovický	k2eAgFnSc2d1
opuky	opuka	k1gFnSc2
•	•	k?
Veltrusy	Veltrusy	k1gInPc1
•	•	k?
Zámecký	zámecký	k2eAgInSc1d1
park	park	k1gInSc1
Liblice	Liblice	k1gFnSc2
•	•	k?
Želízky	želízko	k1gNnPc7
•	•	k?
Žerka	Žerka	k1gMnSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Mladá	mladý	k2eAgFnSc1d1
Boleslav	Boleslav	k1gMnSc1
Přírodní	přírodní	k2eAgInPc4d1
parky	park	k1gInPc4
</s>
<s>
Čížovky	Čížovka	k1gFnPc1
•	•	k?
Chlum	chlum	k1gInSc1
•	•	k?
Jabkenicko	Jabkenicko	k1gNnSc4
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Klokočka	Klokočka	k1gFnSc1
•	•	k?
Radouč	Radouč	k1gFnSc1
•	•	k?
Rečkov	Rečkov	k1gInSc4
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Bažantnice	bažantnice	k1gFnSc1
u	u	k7c2
Loukova	Loukov	k1gInSc2
•	•	k?
Podtrosecká	Podtrosecká	k1gFnSc1
údolí	údolí	k1gNnSc2
•	•	k?
Příhrazské	Příhrazský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Údolí	údolí	k1gNnSc1
Plakánek	Plakánek	k1gMnSc1
•	•	k?
Vrch	vrch	k1gInSc1
Baba	baba	k1gFnSc1
u	u	k7c2
Kosmonos	Kosmonosy	k1gInPc2
•	•	k?
Žabakor	Žabakor	k1gInSc1
Přírodní	přírodní	k2eAgInSc1d1
památky	památka	k1gFnSc2
</s>
<s>
Bělá	bělat	k5eAaImIp3nS
pod	pod	k7c7
Bezdězem	Bezděz	k1gInSc7
–	–	k?
zámek	zámek	k1gInSc1
•	•	k?
Bezděčín	Bezděčín	k1gMnSc1
•	•	k?
Černý	Černý	k1gMnSc1
Orel	Orel	k1gMnSc1
•	•	k?
Dymokursko	Dymokursko	k1gNnSc4
•	•	k?
Chlum	chlum	k1gInSc1
u	u	k7c2
Nepřevázky	Nepřevázka	k1gFnSc2
•	•	k?
Ledce	Ledce	k1gMnSc1
–	–	k?
hájovna	hájovna	k1gFnSc1
•	•	k?
Lom	lom	k1gInSc1
u	u	k7c2
Chrástu	chrást	k1gInSc2
•	•	k?
Niva	niva	k1gFnSc1
Bělé	Bělá	k1gFnSc2
u	u	k7c2
Klokočky	klokoček	k1gInPc1
•	•	k?
Paterovské	Paterovský	k2eAgFnSc2d1
stráně	stráň	k1gFnSc2
•	•	k?
Podhradská	podhradský	k2eAgFnSc1d1
tůň	tůň	k1gFnSc1
•	•	k?
Skalní	skalní	k2eAgInPc4d1
sruby	srub	k1gInPc4
Jizery	Jizera	k1gFnSc2
•	•	k?
Slepeč	Slepeč	k1gInSc1
•	•	k?
Stará	starat	k5eAaImIp3nS
Jizera	Jizera	k1gFnSc1
•	•	k?
Stráně	stráň	k1gFnSc2
u	u	k7c2
Kochánek	kochánek	k1gMnSc1
•	•	k?
V	v	k7c6
Dubech	dub	k1gInPc6
•	•	k?
Valcha	valcha	k1gFnSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
Radechov	Radechov	k1gInSc1
•	•	k?
Zadní	zadní	k2eAgInSc1d1
Hrádek	hrádek	k1gInSc1
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
</s>
