<p>
<s>
Nordkalottleden	Nordkalottleden	k2eAgMnSc1d1	Nordkalottleden
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
<g/>
;	;	kIx,	;
česky	česky	k6eAd1	česky
Cesta	cesta	k1gFnSc1	cesta
Severní	severní	k2eAgFnSc1d1	severní
Kalotou	kalota	k1gFnSc7	kalota
<g/>
,	,	kIx,	,
švédsky	švédsky	k6eAd1	švédsky
Nordkalottleden	Nordkalottleden	k2eAgMnSc1d1	Nordkalottleden
<g/>
,	,	kIx,	,
norsky	norsky	k6eAd1	norsky
<g/>
:	:	kIx,	:
Nordkalottruta	Nordkalottrut	k2eAgFnSc1d1	Nordkalottrut
<g/>
,	,	kIx,	,
finsky	finsky	k6eAd1	finsky
<g/>
:	:	kIx,	:
Kalottireitti	Kalottireitti	k1gNnSc7	Kalottireitti
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
značená	značený	k2eAgFnSc1d1	značená
turistická	turistický	k2eAgFnSc1d1	turistická
stezka	stezka	k1gFnSc1	stezka
v	v	k7c6	v
polární	polární	k2eAgFnSc6d1	polární
oblasti	oblast	k1gFnSc6	oblast
v	v	k7c6	v
severských	severský	k2eAgFnPc6d1	severská
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Severní	severní	k2eAgFnSc1d1	severní
Kalota	kalota	k1gFnSc1	kalota
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
má	mít	k5eAaImIp3nS	mít
celkovou	celkový	k2eAgFnSc4d1	celková
délku	délka	k1gFnSc4	délka
800	[number]	k4	800
km	km	kA	km
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
podél	podél	k7c2	podél
hranice	hranice	k1gFnSc2	hranice
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c4	v
Kautokeino	Kautokeino	k1gNnSc4	Kautokeino
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Norsku	Norsko	k1gNnSc6	Norsko
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Sulitjelma	Sulitjelma	k1gFnSc1	Sulitjelma
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
případně	případně	k6eAd1	případně
v	v	k7c6	v
osadě	osada	k1gFnSc6	osada
Kvikkjokk	Kvikkjokk	k1gInSc1	Kvikkjokk
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
800	[number]	k4	800
km	km	kA	km
cesty	cesta	k1gFnSc2	cesta
leží	ležet	k5eAaImIp3nS	ležet
380	[number]	k4	380
km	km	kA	km
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
,	,	kIx,	,
350	[number]	k4	350
km	km	kA	km
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
70	[number]	k4	70
km	km	kA	km
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Stezka	stezka	k1gFnSc1	stezka
celkem	celkem	k6eAd1	celkem
patnáctkrát	patnáctkrát	k6eAd1	patnáctkrát
překračuje	překračovat	k5eAaImIp3nS	překračovat
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
s	s	k7c7	s
plánováním	plánování	k1gNnSc7	plánování
turistické	turistický	k2eAgFnSc2d1	turistická
stezky	stezka	k1gFnSc2	stezka
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
zahrnuty	zahrnut	k2eAgFnPc4d1	zahrnuta
stávající	stávající	k2eAgFnSc4d1	stávající
cesty	cesta	k1gFnPc4	cesta
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
novými	nový	k2eAgFnPc7d1	nová
cestami	cesta	k1gFnPc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
cesty	cesta	k1gFnSc2	cesta
byly	být	k5eAaImAgFnP	být
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
části	část	k1gFnPc1	část
cesty	cesta	k1gFnSc2	cesta
švédských	švédský	k2eAgFnPc2d1	švédská
stezek	stezka	k1gFnPc2	stezka
Padjelantaleden	Padjelantaleden	k2eAgMnSc1d1	Padjelantaleden
a	a	k8xC	a
Kungsleden	Kungsleden	k2eAgMnSc1d1	Kungsleden
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
norské	norský	k2eAgFnPc1d1	norská
stezky	stezka	k1gFnPc1	stezka
Nordlandsruta	Nordlandsrut	k1gMnSc2	Nordlandsrut
a	a	k8xC	a
cesty	cesta	k1gFnSc2	cesta
staré	starý	k2eAgFnSc2d1	stará
hranice	hranice	k1gFnSc2	hranice
u	u	k7c2	u
Tromso	Tromsa	k1gFnSc5	Tromsa
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
plány	plán	k1gInPc1	plán
byly	být	k5eAaImAgInP	být
dokončeny	dokončit	k5eAaPmNgInP	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
a	a	k8xC	a
Nordkalottleden	Nordkalottleden	k2eAgInSc1d1	Nordkalottleden
byla	být	k5eAaImAgNnP	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřít	k5eAaPmNgNnP	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
zvaném	zvaný	k2eAgNnSc6d1	zvané
švédsky	švédsky	k6eAd1	švédsky
Treriksröset	Treriksröseta	k1gFnPc2	Treriksröseta
(	(	kIx(	(
<g/>
Mohyla	mohyla	k1gFnSc1	mohyla
třech	tři	k4xCgFnPc2	tři
zemí	zem	k1gFnPc2	zem
<g/>
)	)	kIx)	)
aneb	aneb	k?	aneb
na	na	k7c4	na
trojmezí	trojmezí	k1gNnSc4	trojmezí
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cesta	cesta	k1gFnSc1	cesta
prochází	procházet	k5eAaImIp3nS	procházet
národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
Ø	Ø	k?	Ø
Dividal	Dividal	k1gFnSc2	Dividal
<g/>
,	,	kIx,	,
národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
Reisa	Reis	k1gMnSc2	Reis
<g/>
,	,	kIx,	,
národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
Rohkunborri	Rohkunborr	k1gFnSc2	Rohkunborr
<g/>
,	,	kIx,	,
národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
Abisko	Abisko	k1gNnSc4	Abisko
a	a	k8xC	a
národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
Padjelanta	Padjelant	k1gMnSc2	Padjelant
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
cesta	cesta	k1gFnSc1	cesta
Nordkalottleden	Nordkalottleden	k2eAgInSc4d1	Nordkalottleden
nově	nově	k6eAd1	nově
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
řídce	řídce	k6eAd1	řídce
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
stále	stále	k6eAd1	stále
viditelně	viditelně	k6eAd1	viditelně
značena	značen	k2eAgFnSc1d1	značena
kamennými	kamenný	k2eAgMnPc7d1	kamenný
mužiky	mužik	k1gMnPc7	mužik
a	a	k8xC	a
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
značky	značka	k1gFnPc4	značka
řídké	řídký	k2eAgFnPc4d1	řídká
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
zásadní	zásadní	k2eAgNnSc1d1	zásadní
mít	mít	k5eAaImF	mít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
kvalitní	kvalitní	k2eAgFnSc4d1	kvalitní
mapu	mapa	k1gFnSc4	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
léta	léto	k1gNnSc2	léto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
cesta	cesta	k1gFnSc1	cesta
zaplavena	zaplaven	k2eAgFnSc1d1	zaplavena
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
dělat	dělat	k5eAaImF	dělat
zacházky	zacházka	k1gFnPc4	zacházka
od	od	k7c2	od
původní	původní	k2eAgFnSc2d1	původní
trasy	trasa	k1gFnSc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
brodit	brodit	k5eAaImF	brodit
řeky	řeka	k1gFnPc4	řeka
a	a	k8xC	a
potoky	potok	k1gInPc4	potok
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
částečně	částečně	k6eAd1	částečně
chybí	chybit	k5eAaPmIp3nS	chybit
mosty	most	k1gInPc4	most
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
úseků	úsek	k1gInPc2	úsek
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vzdálenostech	vzdálenost	k1gFnPc6	vzdálenost
10-20	[number]	k4	10-20
kilometrů	kilometr	k1gInPc2	kilometr
postaveny	postaven	k2eAgFnPc1d1	postavena
chaty	chata	k1gFnPc1	chata
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
přístřešky	přístřešek	k1gInPc1	přístřešek
na	na	k7c4	na
přenocování	přenocování	k1gNnSc4	přenocování
<g/>
.	.	kIx.	.
</s>
<s>
Chaty	chata	k1gFnPc1	chata
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
vybavené	vybavený	k2eAgFnPc1d1	vybavená
zařízením	zařízení	k1gNnSc7	zařízení
na	na	k7c6	na
vaření	vaření	k1gNnSc6	vaření
<g/>
,	,	kIx,	,
postelemi	postel	k1gFnPc7	postel
a	a	k8xC	a
palivovým	palivový	k2eAgNnSc7d1	palivové
dřívím	dříví	k1gNnSc7	dříví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chatách	chata	k1gFnPc6	chata
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
poplatek	poplatek	k1gInSc1	poplatek
za	za	k7c4	za
přespání	přespání	k1gNnSc4	přespání
<g/>
.	.	kIx.	.
</s>
<s>
Očekává	očekávat	k5eAaImIp3nS	očekávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
turisté	turist	k1gMnPc1	turist
berou	brát	k5eAaImIp3nP	brát
na	na	k7c4	na
vědomí	vědomí	k1gNnSc4	vědomí
právo	právo	k1gNnSc4	právo
veřejné	veřejný	k2eAgFnSc2d1	veřejná
přístupnosti	přístupnost	k1gFnSc2	přístupnost
krajiny	krajina	k1gFnSc2	krajina
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
budou	být	k5eAaImBp3nP	být
šetrně	šetrně	k6eAd1	šetrně
chovat	chovat	k5eAaImF	chovat
k	k	k7c3	k
přírodě	příroda	k1gFnSc3	příroda
a	a	k8xC	a
životnímu	životní	k2eAgNnSc3d1	životní
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgNnPc4d1	další
zajímavá	zajímavý	k2eAgNnPc4d1	zajímavé
místa	místo	k1gNnPc4	místo
na	na	k7c6	na
stezce	stezka	k1gFnSc6	stezka
==	==	k?	==
</s>
</p>
<p>
<s>
Kautokeino	Kautokeino	k1gNnSc1	Kautokeino
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vodopád	vodopád	k1gInSc1	vodopád
Pihtsusköngäs	Pihtsusköngäs	k1gInSc1	Pihtsusköngäs
(	(	kIx(	(
<g/>
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vesnice	vesnice	k1gFnSc1	vesnice
Kilpisjärvi	Kilpisjärev	k1gFnSc6	Kilpisjärev
(	(	kIx(	(
<g/>
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
rezervace	rezervace	k1gFnPc1	rezervace
Malla	Mallo	k1gNnSc2	Mallo
(	(	kIx(	(
<g/>
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Innset	Innset	k1gInSc1	Innset
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Abisko	Abisko	k1gNnSc1	Abisko
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Skjomdalen	Skjomdalen	k2eAgMnSc1d1	Skjomdalen
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nikkaluokta	Nikkaluokta	k1gFnSc1	Nikkaluokta
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ritsem	Rits	k1gInSc7	Rits
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sulitjelma	Sulitjelma	k1gNnSc1	Sulitjelma
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kvikkjokk	Kvikkjokk	k1gInSc1	Kvikkjokk
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Nordkalottruta	Nordkalottrut	k1gMnSc2	Nordkalottrut
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Nordkalottleden	Nordkalottleden	k2eAgMnSc1d1	Nordkalottleden
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nordkalottleden	Nordkalottleden	k2eAgMnSc1d1	Nordkalottleden
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Cestopis	cestopis	k1gInSc1	cestopis
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
části	část	k1gFnSc2	část
stezky	stezka	k1gFnSc2	stezka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Cestopis	cestopis	k1gInSc1	cestopis
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
stezky	stezka	k1gFnSc2	stezka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
<g/>
)	)	kIx)	)
Popis	popis	k1gInSc1	popis
stezky	stezka	k1gFnSc2	stezka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Podrobný	podrobný	k2eAgInSc1d1	podrobný
popis	popis	k1gInSc1	popis
severní	severní	k2eAgFnSc2d1	severní
části	část	k1gFnSc2	část
stezky	stezka	k1gFnSc2	stezka
</s>
</p>
