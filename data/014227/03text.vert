<s>
James	James	k1gMnSc1
Harris	Harris	k1gFnSc2
Simons	Simons	k1gInSc1
</s>
<s>
James	James	k1gMnSc1
Harris	Harris	k1gFnSc2
Simons	Simons	k1gInSc1
James	James	k1gMnSc1
Harris	Harris	k1gFnPc2
Simons	Simonsa	k1gFnPc2
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
Narození	narození	k1gNnSc2
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1938	#num#	k4
(	(	kIx(
<g/>
82	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Newton	newton	k1gInSc1
Bydliště	bydliště	k1gNnSc2
</s>
<s>
BrooklineSetauket-East	BrooklineSetauket-East	k1gMnSc1
Setauket	Setauket	k1gMnSc1
Národnost	národnost	k1gFnSc4
</s>
<s>
Židé	Žid	k1gMnPc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Massachusettský	massachusettský	k2eAgInSc1d1
technologický	technologický	k2eAgInSc1d1
institutKalifornská	institutKalifornský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
BerkeleyHarvardova	BerkeleyHarvardův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
matematik	matematik	k1gMnSc1
<g/>
,	,	kIx,
finančník	finančník	k1gMnSc1
<g/>
,	,	kIx,
obchodník	obchodník	k1gMnSc1
<g/>
,	,	kIx,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
a	a	k8xC
kryptograf	kryptograf	k1gMnSc1
Zaměstnavatelé	zaměstnavatel	k1gMnPc1
</s>
<s>
Harvardova	Harvardův	k2eAgMnSc2d1
univerzitaMassachusettský	univerzitaMassachusettský	k2eAgInSc4d1
technologický	technologický	k2eAgInSc4d1
institutLancaster	institutLancaster	k1gInSc4
University	universita	k1gFnSc2
Majetek	majetek	k1gInSc1
</s>
<s>
23,5	23,5	k4
mld.	mld.	k?
US	US	kA
<g/>
$	$	kIx~
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
21,5	21,5	k4
mld.	mld.	k?
US	US	kA
<g/>
$	$	kIx~
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
Oswalda	Oswalda	k1gFnSc1
Veblena	Veblena	k1gFnSc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
<g/>
medaile	medaile	k1gFnPc4
Giuseppe	Giusepp	k1gInSc5
Motty	motto	k1gNnPc7
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Barbara	Barbara	k1gFnSc1
Simons	Simonsa	k1gFnPc2
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Richard	Richard	k1gMnSc1
Lourie	Lourie	k1gFnSc2
(	(	kIx(
<g/>
bratranec	bratranec	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
James	James	k1gMnSc1
Harris	Harris	k1gFnSc2
Simons	Simons	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
James	James	k1gMnSc1
Harris	Harris	k1gFnSc2
Simons	Simons	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
1938	#num#	k4
<g/>
,	,	kIx,
Newton	Newton	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americký	americký	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
<g/>
,	,	kIx,
manažer	manažer	k1gMnSc1
hedgeových	hedgeův	k2eAgMnPc2d1
fondů	fond	k1gInPc2
a	a	k8xC
filantrop	filantrop	k1gMnSc1
židovského	židovský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
oblasti	oblast	k1gFnSc6
vědy	věda	k1gFnSc2
přispěl	přispět	k5eAaPmAgMnS
ke	k	k7c3
studiu	studio	k1gNnSc3
strojového	strojový	k2eAgNnSc2d1
rozpoznávání	rozpoznávání	k1gNnSc2
tvarů	tvar	k1gInPc2
<g/>
:	:	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
Shiing-Shen	Shiing-Shen	k1gInSc1
Chernem	Chern	k1gInSc7
formuloval	formulovat	k5eAaImAgInS
Chern-Simonsovu	Chern-Simonsův	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přispěl	přispět	k5eAaPmAgMnS
k	k	k7c3
vývoji	vývoj	k1gInSc3
teorie	teorie	k1gFnSc2
strun	struna	k1gFnPc2
poskytnutím	poskytnutí	k1gNnSc7
teoretického	teoretický	k2eAgInSc2d1
rámce	rámec	k1gInSc2
pro	pro	k7c4
kombinaci	kombinace	k1gFnSc4
geometrie	geometrie	k1gFnSc2
<g/>
,	,	kIx,
topologie	topologie	k1gFnSc2
a	a	k8xC
kvantové	kvantový	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
pole	pole	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1968-1978	1968-1978	k4
byl	být	k5eAaImAgMnS
profesorem	profesor	k1gMnSc7
matematiky	matematika	k1gFnSc2
a	a	k8xC
následně	následně	k6eAd1
vedoucí	vedoucí	k1gMnSc1
katedry	katedra	k1gFnSc2
matematiky	matematika	k1gFnSc2
na	na	k7c6
Newyorské	newyorský	k2eAgFnSc6d1
státní	státní	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
ve	v	k7c4
Stony	ston	k1gInPc4
Brooku	Brook	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proslul	proslout	k5eAaPmAgMnS
také	také	k9
jako	jako	k9
tzv.	tzv.	kA
kvantitativní	kvantitativní	k2eAgMnSc1d1
investor	investor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
založil	založit	k5eAaPmAgInS
Renaissance	Renaissance	k1gFnSc2
Technologies	Technologies	k1gInSc1
<g/>
,	,	kIx,
soukromý	soukromý	k2eAgInSc1d1
hedgeový	hedgeový	k2eAgInSc1d1
fond	fond	k1gInSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
Simons	Simons	k1gInSc1
odešel	odejít	k5eAaPmAgInS
z	z	k7c2
fondu	fond	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
<g/>
,	,	kIx,
zůstává	zůstávat	k5eAaImIp3nS
jeho	jeho	k3xOp3gMnSc7
nevýkonným	výkonný	k2eNgMnSc7d1
předsedou	předseda	k1gMnSc7
a	a	k8xC
poradcem	poradce	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
časopisu	časopis	k1gInSc2
Forbes	forbes	k1gInSc1
disponoval	disponovat	k5eAaBmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
majetkem	majetek	k1gInSc7
ve	v	k7c6
výši	výše	k1gFnSc6
18	#num#	k4
miliard	miliarda	k4xCgFnPc2
dolarů	dolar	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1
z	z	k7c2
něj	on	k3xPp3gNnSc2
činilo	činit	k5eAaImAgNnS
24	#num#	k4
<g/>
.	.	kIx.
nejbohatšího	bohatý	k2eAgMnSc2d3
Američana	Američan	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
James	James	k1gInSc4
Harris	Harris	k1gInSc1
Simons	Simons	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Seznam	seznam	k1gInSc4
nejbohatších	bohatý	k2eAgMnPc2d3
lidí	člověk	k1gMnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
James	James	k1gMnSc1
Harris	Harris	k1gFnSc2
Simons	Simons	k1gInSc1
-	-	kIx~
Biography	Biographa	k1gFnPc1
—	—	k?
JewAge	JewAge	k1gFnSc1
<g/>
.	.	kIx.
www.jewage.org	www.jewage.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Portrét	portrét	k1gInSc1
v	v	k7c6
New	New	k1gFnSc6
York	York	k1gInSc1
Times	Timesa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jo	jo	k9
<g/>
20201063765	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1196832579	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2017167908	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
226151302929248661346	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2017167908	#num#	k4
</s>
