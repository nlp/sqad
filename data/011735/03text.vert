<p>
<s>
Rum	rum	k1gInSc1	rum
je	být	k5eAaImIp3nS	být
alkoholický	alkoholický	k2eAgInSc4d1	alkoholický
nápoj	nápoj	k1gInSc4	nápoj
destilovaný	destilovaný	k2eAgInSc4d1	destilovaný
z	z	k7c2	z
melasy	melasa	k1gFnSc2	melasa
nebo	nebo	k8xC	nebo
ze	z	k7c2	z
šťávy	šťáva	k1gFnSc2	šťáva
získané	získaný	k2eAgFnSc2d1	získaná
z	z	k7c2	z
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
slova	slovo	k1gNnSc2	slovo
rum	rum	k1gInSc1	rum
není	být	k5eNaImIp3nS	být
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
.	.	kIx.	.
</s>
<s>
Odborníci	odborník	k1gMnPc1	odborník
uvádějí	uvádět	k5eAaImIp3nP	uvádět
dvě	dva	k4xCgFnPc4	dva
varianty	varianta	k1gFnPc4	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
první	první	k4xOgFnSc2	první
teorie	teorie	k1gFnSc2	teorie
je	on	k3xPp3gNnSc4	on
slovo	slovo	k1gNnSc4	slovo
rum	rum	k1gInSc4	rum
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
rumbellion	rumbellion	k1gInSc1	rumbellion
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
velký	velký	k2eAgInSc1d1	velký
ruch	ruch	k1gInSc1	ruch
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
druhého	druhý	k4xOgInSc2	druhý
tábora	tábor	k1gInSc2	tábor
pochází	pocházet	k5eAaImIp3nS	pocházet
pojmenování	pojmenování	k1gNnSc4	pojmenování
tohoto	tento	k3xDgInSc2	tento
nápoje	nápoj	k1gInSc2	nápoj
od	od	k7c2	od
latinského	latinský	k2eAgInSc2d1	latinský
názvu	název	k1gInSc2	název
"	"	kIx"	"
<g/>
sacharum	sacharum	k1gInSc1	sacharum
officinarium	officinarium	k1gNnSc1	officinarium
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Většina	většina	k1gFnSc1	většina
producentů	producent	k1gMnPc2	producent
rumu	rum	k1gInSc2	rum
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
rozvinuto	rozvinut	k2eAgNnSc4d1	rozvinuto
zpracování	zpracování	k1gNnSc4	zpracování
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
ostrov	ostrov	k1gInSc1	ostrov
Réunion	Réunion	k1gInSc1	Réunion
či	či	k8xC	či
oblast	oblast	k1gFnSc1	oblast
Karibiku	Karibik	k1gInSc2	Karibik
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
zejména	zejména	k9	zejména
Jamajka	Jamajka	k1gFnSc1	Jamajka
a	a	k8xC	a
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
vlastí	vlast	k1gFnSc7	vlast
jsou	být	k5eAaImIp3nP	být
Antily	Antily	k1gFnPc1	Antily
(	(	kIx(	(
<g/>
souostroví	souostroví	k1gNnSc1	souostroví
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
dominantní	dominantní	k2eAgFnSc1d1	dominantní
část	část	k1gFnSc1	část
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
ostrově	ostrov	k1gInSc6	ostrov
specifická	specifický	k2eAgFnSc1d1	specifická
varianta	varianta	k1gFnSc1	varianta
tohoto	tento	k3xDgInSc2	tento
destilátu	destilát	k1gInSc2	destilát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
rumu	rum	k1gInSc2	rum
<g/>
.	.	kIx.	.
</s>
<s>
Světlý	světlý	k2eAgInSc1d1	světlý
rum	rum	k1gInSc1	rum
(	(	kIx(	(
<g/>
Blanco	blanco	k2eAgInPc2d1	blanco
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
často	často	k6eAd1	často
využívá	využívat	k5eAaImIp3nS	využívat
na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
míchaných	míchaný	k2eAgInPc2d1	míchaný
nápojů	nápoj	k1gInPc2	nápoj
<g/>
,	,	kIx,	,
zlatý	zlatý	k2eAgInSc4d1	zlatý
a	a	k8xC	a
tmavý	tmavý	k2eAgInSc4d1	tmavý
rum	rum	k1gInSc4	rum
(	(	kIx(	(
<g/>
tmavnoucí	tmavnoucí	k2eAgFnSc4d1	tmavnoucí
postupným	postupný	k2eAgNnSc7d1	postupné
zráním	zrání	k1gNnSc7	zrání
v	v	k7c6	v
sudech	sud	k1gInPc6	sud
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
konzumován	konzumován	k2eAgMnSc1d1	konzumován
samostatně	samostatně	k6eAd1	samostatně
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
sipper	sipper	k1gInSc1	sipper
či	či	k8xC	či
využíván	využívat	k5eAaImNgInS	využívat
pro	pro	k7c4	pro
vaření	vaření	k1gNnSc4	vaření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
alkoholický	alkoholický	k2eAgInSc4d1	alkoholický
nápoj	nápoj	k1gInSc4	nápoj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k9	jako
"	"	kIx"	"
<g/>
tuzemský	tuzemský	k2eAgInSc4d1	tuzemský
rum	rum	k1gInSc4	rum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
však	však	k9	však
uměle	uměle	k6eAd1	uměle
<g/>
,	,	kIx,	,
dochucením	dochucení	k1gNnSc7	dochucení
zředěného	zředěný	k2eAgInSc2d1	zředěný
lihu	líh	k1gInSc2	líh
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
vyráběného	vyráběný	k2eAgNnSc2d1	vyráběné
kolonovou	kolonový	k2eAgFnSc7d1	Kolonová
destilací	destilace	k1gFnSc7	destilace
z	z	k7c2	z
cukrové	cukrový	k2eAgFnSc2d1	cukrová
řepy	řepa	k1gFnSc2	řepa
<g/>
)	)	kIx)	)
rumovou	rumový	k2eAgFnSc7d1	rumová
esencí	esence	k1gFnSc7	esence
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
však	však	k9	však
nevyhovuje	vyhovovat	k5eNaImIp3nS	vyhovovat
předpisům	předpis	k1gInPc3	předpis
EU	EU	kA	EU
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
názvem	název	k1gInSc7	název
rum	rum	k1gInSc1	rum
smí	smět	k5eAaImIp3nS	smět
označovat	označovat	k5eAaImF	označovat
jen	jen	k9	jen
třtinový	třtinový	k2eAgInSc4d1	třtinový
rum	rum	k1gInSc4	rum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
produkt	produkt	k1gInSc1	produkt
od	od	k7c2	od
vstupu	vstup	k1gInSc2	vstup
Česka	Česko	k1gNnSc2	Česko
do	do	k7c2	do
EU	EU	kA	EU
musí	muset	k5eAaImIp3nP	muset
prodávat	prodávat	k5eAaImF	prodávat
pod	pod	k7c7	pod
jinými	jiný	k2eAgInPc7d1	jiný
názvy	název	k1gInPc7	název
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
Tuzemák	Tuzemák	k1gMnSc1	Tuzemák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kategorizace	kategorizace	k1gFnSc1	kategorizace
==	==	k?	==
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
neexistuje	existovat	k5eNaImIp3nS	existovat
jednotná	jednotný	k2eAgFnSc1d1	jednotná
definice	definice	k1gFnSc1	definice
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
rum	rum	k1gInSc1	rum
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
dělení	dělení	k1gNnSc4	dělení
do	do	k7c2	do
smysluplných	smysluplný	k2eAgFnPc2d1	smysluplná
skupin	skupina	k1gFnPc2	skupina
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
komplikované	komplikovaný	k2eAgNnSc1d1	komplikované
<g/>
.	.	kIx.	.
</s>
<s>
Rum	rum	k1gInSc1	rum
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
většinou	většinou	k6eAd1	většinou
definován	definovat	k5eAaBmNgInS	definovat
podle	podle	k7c2	podle
různých	různý	k2eAgNnPc2d1	různé
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
zákonů	zákon	k1gInPc2	zákon
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgNnPc6	který
je	být	k5eAaImIp3nS	být
vyráběn	vyráběn	k2eAgInSc1d1	vyráběn
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
spočívají	spočívat	k5eAaImIp3nP	spočívat
například	například	k6eAd1	například
v	v	k7c6	v
obsahu	obsah	k1gInSc6	obsah
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
době	doba	k1gFnSc6	doba
zrání	zrání	k1gNnSc2	zrání
i	i	k9	i
ve	v	k7c6	v
zvyklostech	zvyklost	k1gFnPc6	zvyklost
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
pojmenovávání	pojmenovávání	k1gNnSc6	pojmenovávání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obsah	obsah	k1gInSc1	obsah
alkoholu	alkohol	k1gInSc2	alkohol
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
například	například	k6eAd1	například
v	v	k7c6	v
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
musí	muset	k5eAaImIp3nS	muset
rum	rum	k1gInSc4	rum
obsahovat	obsahovat	k5eAaImF	obsahovat
minimálně	minimálně	k6eAd1	minimálně
50	[number]	k4	50
%	%	kIx~	%
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
či	či	k8xC	či
Venezuele	Venezuela	k1gFnSc6	Venezuela
je	být	k5eAaImIp3nS	být
vyžadováno	vyžadovat	k5eAaImNgNnS	vyžadovat
minimálně	minimálně	k6eAd1	minimálně
40	[number]	k4	40
%	%	kIx~	%
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
musí	muset	k5eAaImIp3nS	muset
rum	rum	k1gInSc1	rum
zrát	zrát	k5eAaImF	zrát
alespoň	alespoň	k9	alespoň
osm	osm	k4xCc4	osm
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dominikánské	dominikánský	k2eAgFnSc6d1	Dominikánská
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
Panamě	Panama	k1gFnSc6	Panama
a	a	k8xC	a
Venezuele	Venezuela	k1gFnSc6	Venezuela
pak	pak	k6eAd1	pak
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
v	v	k7c6	v
pojmenovávání	pojmenovávání	k1gNnSc6	pojmenovávání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
se	se	k3xPyFc4	se
rumy	rum	k1gInPc7	rum
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
bílé	bílý	k2eAgFnPc4d1	bílá
<g/>
,	,	kIx,	,
zlaté	zlatý	k2eAgFnPc4d1	zlatá
<g/>
,	,	kIx,	,
světlé	světlý	k2eAgFnPc4d1	světlá
a	a	k8xC	a
extra	extra	k2eAgFnPc4d1	extra
světlé	světlý	k2eAgFnPc4d1	světlá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Grenadě	Grenada	k1gFnSc6	Grenada
a	a	k8xC	a
Barbadosu	Barbadosa	k1gFnSc4	Barbadosa
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
termíny	termín	k1gInPc1	termín
bílý	bílý	k2eAgInSc4d1	bílý
<g/>
,	,	kIx,	,
vyzrálý	vyzrálý	k2eAgInSc4d1	vyzrálý
a	a	k8xC	a
"	"	kIx"	"
<g/>
overproof	overproof	k1gInSc1	overproof
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc1d1	silný
rum	rum	k1gInSc1	rum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
dělením	dělení	k1gNnSc7	dělení
na	na	k7c4	na
rumy	rum	k1gInPc4	rum
<g/>
,	,	kIx,	,
rumové	rumový	k2eAgInPc4d1	rumový
likéry	likér	k1gInPc4	likér
a	a	k8xC	a
ochucené	ochucený	k2eAgInPc4d1	ochucený
rumy	rum	k1gInPc4	rum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Navzdory	navzdory	k7c3	navzdory
těmto	tento	k3xDgInPc3	tento
rozdílům	rozdíl	k1gInPc3	rozdíl
ve	v	k7c6	v
standardech	standard	k1gInPc6	standard
a	a	k8xC	a
v	v	k7c6	v
názvosloví	názvosloví	k1gNnSc6	názvosloví
jsou	být	k5eAaImIp3nP	být
zahrnuta	zahrnut	k2eAgNnPc1d1	zahrnuto
následující	následující	k2eAgNnPc1d1	následující
rozdělení	rozdělení	k1gNnPc1	rozdělení
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
rozsah	rozsah	k1gInSc4	rozsah
škály	škála	k1gFnSc2	škála
vyráběných	vyráběný	k2eAgInPc2d1	vyráběný
rumů	rum	k1gInPc2	rum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Regionální	regionální	k2eAgFnPc4d1	regionální
varianty	varianta	k1gFnPc4	varianta
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Karibiku	Karibik	k1gInSc6	Karibik
má	mít	k5eAaImIp3nS	mít
každý	každý	k3xTgInSc1	každý
ostrov	ostrov	k1gInSc1	ostrov
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
výrobní	výrobní	k2eAgFnSc1d1	výrobní
oblast	oblast	k1gFnSc1	oblast
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
unikátní	unikátní	k2eAgInSc4d1	unikátní
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
styly	styl	k1gInPc1	styl
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
dají	dát	k5eAaPmIp3nP	dát
seskupit	seskupit	k5eAaPmF	seskupit
podle	podle	k7c2	podle
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
na	na	k7c6	na
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
hovoří	hovořit	k5eAaImIp3nS	hovořit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
rumu	rum	k1gInSc2	rum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zkonzumuje	zkonzumovat	k5eAaPmIp3nS	zkonzumovat
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
zásadnímu	zásadní	k2eAgInSc3d1	zásadní
vlivu	vliv	k1gInSc3	vliv
portorikánských	portorikánský	k2eAgInPc2d1	portorikánský
rumů	rum	k1gInPc2	rum
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
ve	v	k7c6	v
"	"	kIx"	"
<g/>
španělském	španělský	k2eAgInSc6d1	španělský
stylu	styl	k1gInSc6	styl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Země	zem	k1gFnPc1	zem
a	a	k8xC	a
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
zpravidla	zpravidla	k6eAd1	zpravidla
tmavší	tmavý	k2eAgInPc1d2	tmavší
<g/>
,	,	kIx,	,
plnější	plný	k2eAgInPc1d2	plnější
rumy	rum	k1gInPc1	rum
a	a	k8xC	a
výraznější	výrazný	k2eAgFnPc1d2	výraznější
chutí	chuť	k1gFnSc7	chuť
melasy	melasa	k1gFnSc2	melasa
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
Antigua	Antigua	k1gFnSc1	Antigua
<g/>
,	,	kIx,	,
Trinidad	Trinidad	k1gInSc1	Trinidad
a	a	k8xC	a
Tobago	Tobago	k1gNnSc1	Tobago
<g/>
,	,	kIx,	,
Grenada	Grenada	k1gFnSc1	Grenada
<g/>
,	,	kIx,	,
Barbados	Barbados	k1gInSc1	Barbados
<g/>
,	,	kIx,	,
Svatá	svatý	k2eAgFnSc1d1	svatá
Lucie	Lucie	k1gFnSc1	Lucie
<g/>
,	,	kIx,	,
Belize	Belize	k1gFnSc1	Belize
<g/>
,	,	kIx,	,
Bermudy	Bermudy	k1gFnPc1	Bermudy
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Kryštof	Kryštof	k1gMnSc1	Kryštof
a	a	k8xC	a
Jamajka	Jamajka	k1gFnSc1	Jamajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
frankofonních	frankofonní	k2eAgFnPc6d1	frankofonní
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
zase	zase	k9	zase
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
rumy	rum	k1gInPc4	rum
typu	typ	k1gInSc2	typ
"	"	kIx"	"
<g/>
rhum	rhum	k1gInSc1	rhum
agricole	agricole	k1gFnSc2	agricole
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
pouze	pouze	k6eAd1	pouze
ze	z	k7c2	z
šťávy	šťáva	k1gFnSc2	šťáva
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
si	se	k3xPyFc3	se
zachovávají	zachovávat	k5eAaImIp3nP	zachovávat
více	hodně	k6eAd2	hodně
její	její	k3xOp3gFnSc2	její
chuti	chuť	k1gFnSc2	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
dražší	drahý	k2eAgInPc1d2	dražší
než	než	k8xS	než
rumy	rum	k1gInPc1	rum
vyráběné	vyráběný	k2eAgInPc1d1	vyráběný
z	z	k7c2	z
melasy	melasa	k1gFnSc2	melasa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
rumu	rum	k1gInSc2	rum
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
například	například	k6eAd1	například
na	na	k7c6	na
Haiti	Haiti	k1gNnSc6	Haiti
<g/>
,	,	kIx,	,
Guadeloupe	Guadeloupe	k1gFnSc6	Guadeloupe
nebo	nebo	k8xC	nebo
Martiniku	Martinik	k1gInSc6	Martinik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
španělsky	španělsky	k6eAd1	španělsky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
poměrně	poměrně	k6eAd1	poměrně
jemné	jemný	k2eAgInPc4d1	jemný
rumy	rum	k1gInPc4	rum
añ	añ	k?	añ
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
příklad	příklad	k1gInSc4	příklad
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
,	,	kIx,	,
Guatemalu	Guatemala	k1gFnSc4	Guatemala
<g/>
,	,	kIx,	,
Panamu	Panama	k1gFnSc4	Panama
<g/>
,	,	kIx,	,
Dominikánskou	dominikánský	k2eAgFnSc4d1	Dominikánská
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
Portoriko	Portoriko	k1gNnSc4	Portoriko
<g/>
,	,	kIx,	,
Kolumbii	Kolumbie	k1gFnSc4	Kolumbie
a	a	k8xC	a
Venezuelu	Venezuela	k1gFnSc4	Venezuela
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Kanárských	kanárský	k2eAgInPc6d1	kanárský
ostrovech	ostrov	k1gInPc6	ostrov
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
medový	medový	k2eAgInSc1d1	medový
rum	rum	k1gInSc1	rum
ron	ron	k1gInSc1	ron
miel	mienout	k5eAaPmAgMnS	mienout
de	de	k?	de
Canarias	Canarias	k1gMnSc1	Canarias
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
cachaça	cachaça	k1gFnSc1	cachaça
<g/>
,	,	kIx,	,
alkoholický	alkoholický	k2eAgInSc1d1	alkoholický
nápoj	nápoj	k1gInSc4	nápoj
podobný	podobný	k2eAgInSc4d1	podobný
rumu	rum	k1gInSc3	rum
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
USA	USA	kA	USA
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
nápoj	nápoj	k1gInSc4	nápoj
klasifikují	klasifikovat	k5eAaImIp3nP	klasifikovat
jako	jako	k9	jako
druh	druh	k1gInSc4	druh
rumu	rum	k1gInSc2	rum
<g/>
.	.	kIx.	.
</s>
<s>
Seco	Seco	k1gNnSc1	Seco
<g/>
,	,	kIx,	,
destilát	destilát	k1gInSc1	destilát
z	z	k7c2	z
Panamy	Panama	k1gFnSc2	Panama
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
podobný	podobný	k2eAgInSc1d1	podobný
rumu	rum	k1gInSc3	rum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
vodce	vodka	k1gFnSc3	vodka
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
destiluje	destilovat	k5eAaImIp3nS	destilovat
třikrát	třikrát	k6eAd1	třikrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
nejen	nejen	k6eAd1	nejen
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
světlých	světlý	k2eAgInPc2d1	světlý
i	i	k8xC	i
tmavých	tmavý	k2eAgInPc2d1	tmavý
rumů	rum	k1gInPc2	rum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
levnější	levný	k2eAgInPc1d2	levnější
druhy	druh	k1gInPc1	druh
ochuceného	ochucený	k2eAgMnSc2d1	ochucený
i	i	k8xC	i
neochuceného	ochucený	k2eNgInSc2d1	neochucený
alkoholu	alkohol	k1gInSc2	alkohol
z	z	k7c2	z
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
aguardiente	aguardient	k1gInSc5	aguardient
de	de	k?	de
cañ	cañ	k?	cañ
nebo	nebo	k8xC	nebo
charanda	charanda	k1gFnSc1	charanda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
nápoj	nápoj	k1gInSc1	nápoj
aguardiente	aguardient	k1gInSc5	aguardient
destilovaný	destilovaný	k2eAgMnSc1d1	destilovaný
z	z	k7c2	z
melasy	melasa	k1gFnSc2	melasa
a	a	k8xC	a
ochucený	ochucený	k2eAgInSc1d1	ochucený
anýzem	anýz	k1gInSc7	anýz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
destilaci	destilace	k1gFnSc6	destilace
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
často	často	k6eAd1	často
přidává	přidávat	k5eAaImIp3nS	přidávat
další	další	k2eAgFnSc1d1	další
šťáva	šťáva	k1gFnSc1	šťáva
z	z	k7c2	z
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Libérii	Libérie	k1gFnSc6	Libérie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
"	"	kIx"	"
<g/>
třtinová	třtinový	k2eAgFnSc1d1	třtinová
šťáva	šťáva	k1gFnSc1	šťáva
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
známá	známá	k1gFnSc1	známá
také	také	k9	také
jako	jako	k8xS	jako
Liberijský	liberijský	k2eAgInSc4d1	liberijský
rum	rum	k1gInSc4	rum
nebo	nebo	k8xC	nebo
CJ	CJ	kA	CJ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
levný	levný	k2eAgInSc4d1	levný
a	a	k8xC	a
silný	silný	k2eAgInSc4d1	silný
alkoholický	alkoholický	k2eAgInSc4d1	alkoholický
nápoj	nápoj	k1gInSc4	nápoj
destilovaný	destilovaný	k2eAgInSc4d1	destilovaný
z	z	k7c2	z
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
až	až	k9	až
43	[number]	k4	43
%	%	kIx~	%
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc6d1	slovenská
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
z	z	k7c2	z
cukrové	cukrový	k2eAgFnSc2d1	cukrová
řepy	řepa	k1gFnSc2	řepa
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
destilát	destilát	k1gInSc1	destilát
Tuzemák	Tuzemák	k1gInSc1	Tuzemák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
Rum-Verschnitt	Rum-Verschnitt	k1gInSc1	Rum-Verschnitt
<g/>
,	,	kIx,	,
levná	levný	k2eAgFnSc1d1	levná
náhražka	náhražka	k1gFnSc1	náhražka
opravdového	opravdový	k2eAgInSc2d1	opravdový
tmavého	tmavý	k2eAgInSc2d1	tmavý
rumu	rum	k1gInSc2	rum
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
tmavého	tmavý	k2eAgInSc2d1	tmavý
rumu	rum	k1gInSc2	rum
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
z	z	k7c2	z
Jamajky	Jamajka	k1gFnSc2	Jamajka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rektifikovaného	rektifikovaný	k2eAgInSc2d1	rektifikovaný
lihu	líh	k1gInSc2	líh
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
často	často	k6eAd1	často
se	se	k3xPyFc4	se
dobarvuje	dobarvovat	k5eAaImIp3nS	dobarvovat
karamelem	karamel	k1gInSc7	karamel
Může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
i	i	k9	i
velice	velice	k6eAd1	velice
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
opravdového	opravdový	k2eAgInSc2d1	opravdový
rumu	rum	k1gInSc2	rum
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
minimum	minimum	k1gNnSc1	minimum
je	být	k5eAaImIp3nS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
na	na	k7c4	na
5	[number]	k4	5
%	%	kIx~	%
objemu	objem	k1gInSc2	objem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
podobný	podobný	k2eAgInSc4d1	podobný
rum	rum	k1gInSc4	rum
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Inländerrum	Inländerrum	k1gInSc1	Inländerrum
či	či	k8xC	či
také	také	k9	také
domácí	domácí	k2eAgInSc4d1	domácí
rum	rum	k1gInSc4	rum
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
vždycky	vždycky	k6eAd1	vždycky
kořeněný	kořeněný	k2eAgInSc1d1	kořeněný
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
německé	německý	k2eAgFnSc2d1	německá
náhražky	náhražka	k1gFnSc2	náhražka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nikdy	nikdy	k6eAd1	nikdy
není	být	k5eNaImIp3nS	být
kořeněná	kořeněný	k2eAgFnSc1d1	kořeněná
ani	ani	k8xC	ani
ochucená	ochucený	k2eAgFnSc1d1	ochucená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Metody	metoda	k1gFnPc1	metoda
výroby	výroba	k1gFnSc2	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
alkoholu	alkohol	k1gInSc2	alkohol
neexistuje	existovat	k5eNaImIp3nS	existovat
u	u	k7c2	u
rumu	rum	k1gInSc2	rum
jednotná	jednotný	k2eAgFnSc1d1	jednotná
metoda	metoda	k1gFnSc1	metoda
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
výroba	výroba	k1gFnSc1	výroba
se	se	k3xPyFc4	se
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c6	na
tradičních	tradiční	k2eAgInPc6d1	tradiční
stylech	styl	k1gInPc6	styl
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
palíren	palírna	k1gFnPc2	palírna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fermentace	fermentace	k1gFnSc2	fermentace
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
rumu	rum	k1gInSc2	rum
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
melasy	melasa	k1gFnSc2	melasa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
.	.	kIx.	.
</s>
<s>
Kvalita	kvalita	k1gFnSc1	kvalita
rumu	rum	k1gInSc2	rum
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
kvalitě	kvalita	k1gFnSc6	kvalita
a	a	k8xC	a
druhu	druh	k1gInSc2	druh
použité	použitý	k2eAgFnSc2d1	použitá
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
kvalita	kvalita	k1gFnSc1	kvalita
pak	pak	k6eAd1	pak
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
na	na	k7c6	na
podnebí	podnebí	k1gNnSc6	podnebí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
byla	být	k5eAaImAgFnS	být
vypěstována	vypěstován	k2eAgFnSc1d1	vypěstována
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
melasy	melasa	k1gFnSc2	melasa
používané	používaný	k2eAgFnSc2d1	používaná
v	v	k7c6	v
Karibiku	Karibik	k1gInSc6	Karibik
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
frankofonní	frankofonní	k2eAgInPc1d1	frankofonní
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rum	rum	k1gInSc1	rum
místo	místo	k1gNnSc1	místo
z	z	k7c2	z
melasy	melasa	k1gFnSc2	melasa
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
převážně	převážně	k6eAd1	převážně
ze	z	k7c2	z
šťávy	šťáva	k1gFnSc2	šťáva
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
destilovaný	destilovaný	k2eAgInSc4d1	destilovaný
alkoholický	alkoholický	k2eAgInSc4d1	alkoholický
nápoj	nápoj	k1gInSc4	nápoj
ze	z	k7c2	z
šťávy	šťáva	k1gFnSc2	šťáva
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
nazývá	nazývat	k5eAaImIp3nS	nazývat
cachaça	cachaça	k6eAd1	cachaça
a	a	k8xC	a
od	od	k7c2	od
rumu	rum	k1gInSc2	rum
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
fermentace	fermentace	k1gFnSc2	fermentace
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zahájí	zahájit	k5eAaPmIp3nS	zahájit
přidáním	přidání	k1gNnSc7	přidání
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
kvasnic	kvasnice	k1gFnPc2	kvasnice
k	k	k7c3	k
základní	základní	k2eAgFnSc3d1	základní
ingredienci	ingredience	k1gFnSc3	ingredience
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Destilace	destilace	k1gFnSc2	destilace
===	===	k?	===
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
aspektů	aspekt	k1gInPc2	aspekt
výroby	výroba	k1gFnSc2	výroba
rumu	rum	k1gInSc2	rum
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
při	při	k7c6	při
destilaci	destilace	k1gFnSc6	destilace
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
žádná	žádný	k3yNgFnSc1	žádný
jednotná	jednotný	k2eAgFnSc1d1	jednotná
metoda	metoda	k1gFnSc1	metoda
destilace	destilace	k1gFnSc1	destilace
<g/>
.	.	kIx.	.
</s>
<s>
Použitá	použitý	k2eAgFnSc1d1	použitá
metoda	metoda	k1gFnSc1	metoda
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
odrazit	odrazit	k5eAaPmF	odrazit
například	například	k6eAd1	například
na	na	k7c6	na
plnosti	plnost	k1gFnSc6	plnost
chuti	chuť	k1gFnSc2	chuť
rumu	rum	k1gInSc2	rum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zrání	zrání	k1gNnPc4	zrání
a	a	k8xC	a
míchání	míchání	k1gNnPc4	míchání
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
musí	muset	k5eAaImIp3nS	muset
rum	rum	k1gInSc4	rum
zrát	zrát	k5eAaImF	zrát
alespoň	alespoň	k9	alespoň
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Rum	rum	k1gInSc1	rum
zraje	zrát	k5eAaImIp3nS	zrát
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
dřevěných	dřevěný	k2eAgInPc6d1	dřevěný
sudech	sud	k1gInPc6	sud
<g/>
,	,	kIx,	,
zrání	zrání	k1gNnSc1	zrání
může	moct	k5eAaImIp3nS	moct
však	však	k9	však
probíhat	probíhat	k5eAaImF	probíhat
i	i	k9	i
v	v	k7c6	v
nádržích	nádrž	k1gFnPc6	nádrž
z	z	k7c2	z
nerezové	rezový	k2eNgFnSc2d1	nerezová
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
procesu	proces	k1gInSc2	proces
zrání	zrání	k1gNnSc2	zrání
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
barva	barva	k1gFnSc1	barva
rumu	rum	k1gInSc2	rum
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zrání	zrání	k1gNnSc6	zrání
v	v	k7c6	v
dubových	dubový	k2eAgInPc6d1	dubový
sudech	sud	k1gInPc6	sud
rum	rum	k1gInSc4	rum
tmavne	tmavnout	k5eAaImIp3nS	tmavnout
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
nádržích	nádrž	k1gFnPc6	nádrž
z	z	k7c2	z
nerezové	rezový	k2eNgFnSc2d1	nerezová
oceli	ocel	k1gFnSc2	ocel
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
prakticky	prakticky	k6eAd1	prakticky
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rum	rum	k1gInSc1	rum
typicky	typicky	k6eAd1	typicky
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
tropickým	tropický	k2eAgNnSc7d1	tropické
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
,	,	kIx,	,
zraje	zrát	k5eAaImIp3nS	zrát
daleko	daleko	k6eAd1	daleko
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
whisky	whisky	k1gFnSc1	whisky
či	či	k8xC	či
brandy	brandy	k1gFnSc1	brandy
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
v	v	k7c6	v
andělském	andělský	k2eAgInSc6d1	andělský
podílu	podíl	k1gInSc6	podíl
neboli	neboli	k8xC	neboli
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
produktu	produkt	k1gInSc2	produkt
ztraceného	ztracený	k2eAgInSc2d1	ztracený
odpařením	odpaření	k1gNnSc7	odpaření
<g/>
.	.	kIx.	.
</s>
<s>
Produkty	produkt	k1gInPc1	produkt
zrající	zrající	k2eAgInPc1d1	zrající
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
či	či	k8xC	či
Skotsku	Skotsko	k1gNnSc6	Skotsko
ztratí	ztratit	k5eAaPmIp3nS	ztratit
ročně	ročně	k6eAd1	ročně
asi	asi	k9	asi
2	[number]	k4	2
%	%	kIx~	%
objemu	objem	k1gInSc2	objem
<g/>
,	,	kIx,	,
produkty	produkt	k1gInPc1	produkt
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
pak	pak	k6eAd1	pak
až	až	k9	až
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
uzrání	uzrání	k1gNnSc6	uzrání
se	se	k3xPyFc4	se
rum	rum	k1gInSc1	rum
většinou	většinou	k6eAd1	většinou
míchá	míchat	k5eAaImIp3nS	míchat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
výrobce	výrobce	k1gMnSc1	výrobce
zajistit	zajistit	k5eAaPmF	zajistit
konzistentní	konzistentní	k2eAgFnSc4d1	konzistentní
chuť	chuť	k1gFnSc4	chuť
produktu	produkt	k1gInSc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
poslední	poslední	k2eAgInSc4d1	poslední
krok	krok	k1gInSc4	krok
výroby	výroba	k1gFnSc2	výroba
rumu	rum	k1gInSc2	rum
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
něj	on	k3xPp3gInSc2	on
jsou	být	k5eAaImIp3nP	být
světlé	světlý	k2eAgInPc1d1	světlý
rumy	rum	k1gInPc1	rum
občas	občas	k6eAd1	občas
filtrovány	filtrován	k2eAgInPc1d1	filtrován
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
odstranění	odstranění	k1gNnSc2	odstranění
zabarvení	zabarvení	k1gNnSc2	zabarvení
vzniklého	vzniklý	k2eAgNnSc2d1	vzniklé
během	během	k7c2	během
zrání	zrání	k1gNnSc2	zrání
<g/>
.	.	kIx.	.
</s>
<s>
Tmavší	tmavý	k2eAgInPc1d2	tmavší
rumy	rum	k1gInPc1	rum
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
dobarvovány	dobarvovat	k5eAaImNgInP	dobarvovat
karamelem	karamel	k1gInSc7	karamel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nápoje	nápoj	k1gInPc4	nápoj
a	a	k8xC	a
pokrmy	pokrm	k1gInPc4	pokrm
==	==	k?	==
</s>
</p>
<p>
<s>
Rum	rum	k1gInSc1	rum
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
nejen	nejen	k6eAd1	nejen
do	do	k7c2	do
punčů	punč	k1gInPc2	punč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
do	do	k7c2	do
známých	známý	k2eAgInPc2d1	známý
koktejlů	koktejl	k1gInPc2	koktejl
pocházejících	pocházející	k2eAgInPc2d1	pocházející
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Cuba	Cuba	k1gFnSc1	Cuba
libre	libr	k1gMnSc5	libr
nebo	nebo	k8xC	nebo
Daiquiri	Daiquir	k1gMnSc5	Daiquir
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
do	do	k7c2	do
koktejlů	koktejl	k1gInPc2	koktejl
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Piñ	Piñ	k1gFnSc1	Piñ
colada	colada	k1gFnSc1	colada
nebo	nebo	k8xC	nebo
Mojito	Mojit	k2eAgNnSc1d1	Mojito
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
k	k	k7c3	k
dochucování	dochucování	k1gNnSc3	dochucování
různých	různý	k2eAgInPc2d1	různý
pokrmů	pokrm	k1gInPc2	pokrm
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
rumové	rumový	k2eAgFnPc1d1	rumová
kuličky	kulička	k1gFnPc1	kulička
či	či	k8xC	či
koláče	koláč	k1gInPc1	koláč
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
nakládá	nakládat	k5eAaImIp3nS	nakládat
ovoce	ovoce	k1gNnSc4	ovoce
používané	používaný	k2eAgNnSc4d1	používané
do	do	k7c2	do
koláčů	koláč	k1gInPc2	koláč
<g/>
.	.	kIx.	.
</s>
<s>
Přidává	přidávat	k5eAaImIp3nS	přidávat
se	se	k3xPyFc4	se
také	také	k9	také
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
marinád	marináda	k1gFnPc2	marináda
používaných	používaný	k2eAgFnPc2d1	používaná
v	v	k7c6	v
karibské	karibský	k2eAgFnSc6d1	karibská
kuchyni	kuchyně	k1gFnSc6	kuchyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rumy	rum	k1gInPc4	rum
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Rum	rum	k1gInSc1	rum
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
COULOMBE	coulomb	k1gInSc5	coulomb
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
A.	A.	kA	A.
Rum	rum	k1gInSc1	rum
<g/>
:	:	kIx,	:
výpravný	výpravný	k2eAgInSc1d1	výpravný
příběh	příběh	k1gInSc1	příběh
nápoje	nápoj	k1gInSc2	nápoj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dobyl	dobýt	k5eAaPmAgInS	dobýt
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
BB	BB	kA	BB
art	art	k?	art
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
287	[number]	k4	287
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7341	[number]	k4	7341
<g/>
-	-	kIx~	-
<g/>
766	[number]	k4	766
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
čeština	čeština	k1gFnSc1	čeština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rum	rum	k1gInSc1	rum
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Magdalena	Magdalena	k1gFnSc1	Magdalena
Wagnerová	Wagnerová	k1gFnSc1	Wagnerová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Plot	plot	k1gInSc1	plot
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
110	[number]	k4	110
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86523	[number]	k4	86523
<g/>
-	-	kIx~	-
<g/>
68	[number]	k4	68
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
čeština	čeština	k1gFnSc1	čeština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Cachaça	Cachaça	k1gFnSc1	Cachaça
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
kašasa	kašasa	k1gFnSc1	kašasa
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tuzemský	tuzemský	k2eAgInSc4d1	tuzemský
rum	rum	k1gInSc4	rum
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rum	rum	k1gInSc4	rum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
rum	rum	k1gInSc1	rum
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Rum	rum	k1gInSc1	rum
už	už	k9	už
dávno	dávno	k6eAd1	dávno
není	být	k5eNaImIp3nS	být
jen	jen	k9	jen
pro	pro	k7c4	pro
ošlehané	ošlehaný	k2eAgMnPc4d1	ošlehaný
námořníky	námořník	k1gMnPc4	námořník
</s>
</p>
<p>
<s>
Petrovy	Petrův	k2eAgFnPc1d1	Petrova
rumové	rumový	k2eAgFnPc1d1	rumová
stránky	stránka	k1gFnPc1	stránka
–	–	k?	–
Online	Onlin	k1gInSc5	Onlin
sbírka	sbírka	k1gFnSc1	sbírka
rumových	rumový	k2eAgFnPc2d1	rumová
etiket	etiketa	k1gFnPc2	etiketa
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
svět	svět	k1gInSc1	svět
</s>
</p>
