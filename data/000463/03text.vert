<s>
Vatikán	Vatikán	k1gInSc1	Vatikán
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
Městský	městský	k2eAgInSc1d1	městský
stát	stát	k1gInSc1	stát
Vatikán	Vatikán	k1gInSc1	Vatikán
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
suverénní	suverénní	k2eAgInSc1d1	suverénní
městský	městský	k2eAgInSc1d1	městský
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
enkláva	enkláva	k1gFnSc1	enkláva
uvnitř	uvnitř	k7c2	uvnitř
města	město	k1gNnSc2	město
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
přibližně	přibližně	k6eAd1	přibližně
44	[number]	k4	44
hektarů	hektar	k1gInPc2	hektar
a	a	k8xC	a
méně	málo	k6eAd2	málo
než	než	k8xS	než
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejmenší	malý	k2eAgInSc1d3	nejmenší
stát	stát	k1gInSc1	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
rozlohu	rozloha	k1gFnSc4	rozloha
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
území	území	k1gNnSc1	území
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
obehnáno	obehnat	k5eAaPmNgNnS	obehnat
historickou	historický	k2eAgFnSc7d1	historická
hradbou	hradba	k1gFnSc7	hradba
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
,	,	kIx,	,
kostely	kostel	k1gInPc4	kostel
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
budovy	budova	k1gFnPc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
Vatikán	Vatikán	k1gInSc1	Vatikán
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Lateránských	lateránský	k2eAgFnPc2d1	Lateránská
smluv	smlouva	k1gFnPc2	smlouva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
podepsal	podepsat	k5eAaPmAgMnS	podepsat
kardinál	kardinál	k1gMnSc1	kardinál
Pietro	Pietro	k1gNnSc4	Pietro
Gasparri	Gasparr	k1gFnSc2	Gasparr
jménem	jméno	k1gNnSc7	jméno
Svatého	svatý	k2eAgInSc2d1	svatý
stolce	stolec	k1gInSc2	stolec
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Benito	Benita	k1gMnSc5	Benita
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
jménem	jméno	k1gNnSc7	jméno
království	království	k1gNnSc2	království
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Smlouvy	smlouva	k1gFnPc1	smlouva
o	o	k7c6	o
Vatikánu	Vatikán	k1gInSc6	Vatikán
hovoří	hovořit	k5eAaImIp3nS	hovořit
jako	jako	k9	jako
o	o	k7c6	o
novém	nový	k2eAgInSc6d1	nový
útvaru	útvar	k1gInSc6	útvar
(	(	kIx(	(
<g/>
preambule	preambule	k1gFnSc1	preambule
a	a	k8xC	a
článek	článek	k1gInSc1	článek
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
jako	jako	k9	jako
o	o	k7c6	o
pozůstatku	pozůstatek	k1gInSc6	pozůstatek
mnohem	mnohem	k6eAd1	mnohem
většího	veliký	k2eAgInSc2d2	veliký
papežského	papežský	k2eAgInSc2d1	papežský
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
756	[number]	k4	756
<g/>
-	-	kIx~	-
<g/>
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dříve	dříve	k6eAd2	dříve
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
střední	střední	k2eAgFnSc2d1	střední
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
byla	být	k5eAaImAgFnS	být
včleněna	včlenit	k5eAaPmNgFnS	včlenit
do	do	k7c2	do
Italského	italský	k2eAgNnSc2d1	italské
království	království	k1gNnSc2	království
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
a	a	k8xC	a
poslední	poslední	k2eAgFnSc4d1	poslední
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
město	město	k1gNnSc1	město
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Vatikán	Vatikán	k1gInSc1	Vatikán
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
církevní	církevní	k2eAgMnSc1d1	církevní
nebo	nebo	k8xC	nebo
duchovně	duchovně	k6eAd1	duchovně
monarchický	monarchický	k2eAgMnSc1d1	monarchický
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
vládne	vládnout	k5eAaImIp3nS	vládnout
doživotně	doživotně	k6eAd1	doživotně
volený	volený	k2eAgMnSc1d1	volený
římský	římský	k2eAgMnSc1d1	římský
biskup	biskup	k1gMnSc1	biskup
-	-	kIx~	-
papež	papež	k1gMnSc1	papež
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
státní	státní	k2eAgMnPc1d1	státní
činitelé	činitel	k1gMnPc1	činitel
jsou	být	k5eAaImIp3nP	být
všichni	všechen	k3xTgMnPc1	všechen
katoličtí	katolický	k2eAgMnPc1d1	katolický
duchovní	duchovní	k1gMnPc1	duchovní
různého	různý	k2eAgInSc2d1	různý
národního	národní	k2eAgInSc2d1	národní
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
návratu	návrat	k1gInSc2	návrat
z	z	k7c2	z
Avignonského	avignonský	k2eAgNnSc2d1	avignonské
zajetí	zajetí	k1gNnSc2	zajetí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1377	[number]	k4	1377
papežové	papež	k1gMnPc1	papež
sídlí	sídlet	k5eAaImIp3nP	sídlet
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Apoštolském	apoštolský	k2eAgInSc6d1	apoštolský
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
<g/>
.	.	kIx.	.
</s>
<s>
Vatikánský	vatikánský	k2eAgInSc1d1	vatikánský
městský	městský	k2eAgInSc1d1	městský
stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
odlišovat	odlišovat	k5eAaImF	odlišovat
od	od	k7c2	od
Svatého	svatý	k2eAgInSc2d1	svatý
stolce	stolec	k1gInSc2	stolec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
raného	raný	k2eAgNnSc2d1	rané
křesťanství	křesťanství	k1gNnSc2	křesťanství
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
biskupským	biskupský	k2eAgInSc7d1	biskupský
stolcem	stolec	k1gInSc7	stolec
pro	pro	k7c4	pro
1,2	[number]	k4	1,2
miliardy	miliarda	k4xCgFnSc2	miliarda
katolických	katolický	k2eAgMnPc2d1	katolický
věřících	věřící	k1gMnPc2	věřící
latinského	latinský	k2eAgInSc2d1	latinský
i	i	k8xC	i
východního	východní	k2eAgInSc2d1	východní
obřadu	obřad	k1gInSc2	obřad
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislý	závislý	k2eNgInSc1d1	nezávislý
městský	městský	k2eAgInSc1d1	městský
stát	stát	k1gInSc1	stát
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Lateránské	lateránský	k2eAgFnSc2d1	Lateránská
smlouvy	smlouva	k1gFnSc2	smlouva
mezi	mezi	k7c7	mezi
Svatým	svatý	k2eAgInSc7d1	svatý
stolcem	stolec	k1gInSc7	stolec
a	a	k8xC	a
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Dokumenty	dokument	k1gInPc1	dokument
Vatikánu	Vatikán	k1gInSc2	Vatikán
se	se	k3xPyFc4	se
vydávají	vydávat	k5eAaImIp3nP	vydávat
v	v	k7c6	v
italštině	italština	k1gFnSc6	italština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
i	i	k9	i
komunikačním	komunikační	k2eAgInSc7d1	komunikační
jazykem	jazyk	k1gInSc7	jazyk
Vatikánu	Vatikán	k1gInSc2	Vatikán
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
oficiální	oficiální	k2eAgInPc1d1	oficiální
dokumenty	dokument	k1gInPc1	dokument
Svatého	svatý	k2eAgInSc2d1	svatý
stolce	stolec	k1gInSc2	stolec
jsou	být	k5eAaImIp3nP	být
vydány	vydat	k5eAaPmNgInP	vydat
hlavně	hlavně	k9	hlavně
latinsky	latinsky	k6eAd1	latinsky
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgNnSc1d1	vlastní
území	území	k1gNnSc1	území
Vatikánu	Vatikán	k1gInSc2	Vatikán
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
baziliku	bazilika	k1gFnSc4	bazilika
sv.	sv.	kA	sv.
Petra	Petra	k1gFnSc1	Petra
a	a	k8xC	a
Svatopetrské	svatopetrský	k2eAgNnSc1d1	Svatopetrské
náměstí	náměstí	k1gNnSc1	náměstí
s	s	k7c7	s
Apoštolským	apoštolský	k2eAgInSc7d1	apoštolský
palácem	palác	k1gInSc7	palác
<g/>
,	,	kIx,	,
přiléhajícími	přiléhající	k2eAgFnPc7d1	přiléhající
budovami	budova	k1gFnPc7	budova
a	a	k8xC	a
Vatikánskými	vatikánský	k2eAgFnPc7d1	Vatikánská
zahradami	zahrada	k1gFnPc7	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
významné	významný	k2eAgFnPc1d1	významná
kulturní	kulturní	k2eAgFnPc1d1	kulturní
památky	památka	k1gFnPc1	památka
a	a	k8xC	a
instituce	instituce	k1gFnPc1	instituce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Sixtinská	sixtinský	k2eAgFnSc1d1	Sixtinská
kaple	kaple	k1gFnSc1	kaple
<g/>
,	,	kIx,	,
Vatikánská	vatikánský	k2eAgNnPc1d1	Vatikánské
muzea	muzeum	k1gNnPc1	muzeum
nebo	nebo	k8xC	nebo
Vatikánská	vatikánský	k2eAgFnSc1d1	Vatikánská
apoštolská	apoštolský	k2eAgFnSc1d1	apoštolská
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
k	k	k7c3	k
Vatikánu	Vatikán	k1gInSc3	Vatikán
náleží	náležet	k5eAaImIp3nS	náležet
i	i	k9	i
exteritoriální	exteritoriální	k2eAgNnSc4d1	exteritoriální
území	území	k1gNnSc4	území
se	s	k7c7	s
zvláštním	zvláštní	k2eAgNnSc7d1	zvláštní
právním	právní	k2eAgNnSc7d1	právní
postavením	postavení	k1gNnSc7	postavení
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nP	patřit
papežské	papežský	k2eAgFnPc4d1	Papežská
baziliky	bazilika	k1gFnPc4	bazilika
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
"	"	kIx"	"
<g/>
patriarchální	patriarchální	k2eAgMnSc1d1	patriarchální
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
v	v	k7c6	v
Lateránu	Laterán	k1gInSc6	Laterán
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Pavla	Pavla	k1gFnSc1	Pavla
za	za	k7c7	za
hradbami	hradba	k1gFnPc7	hradba
<g/>
,	,	kIx,	,
bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
za	za	k7c7	za
hradbami	hradba	k1gFnPc7	hradba
<g/>
,	,	kIx,	,
bazilika	bazilika	k1gFnSc1	bazilika
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Sněžné	sněžný	k2eAgNnSc4d1	Sněžné
a	a	k8xC	a
papežské	papežský	k2eAgNnSc4d1	papežské
letní	letní	k2eAgNnSc4d1	letní
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c4	v
Castel	Castel	k1gInSc4	Castel
Gandolfo	Gandolfo	k6eAd1	Gandolfo
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Vatikán	Vatikán	k1gInSc1	Vatikán
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
pahorku	pahorek	k1gInSc2	pahorek
Vaticanus	Vaticanus	k1gInSc1	Vaticanus
Mons	Mons	k1gInSc1	Mons
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
ze	z	k7c2	z
starolatinského	starolatinský	k2eAgInSc2d1	starolatinský
vates	vates	k1gMnSc1	vates
(	(	kIx(	(
<g/>
věštec	věštec	k1gMnSc1	věštec
<g/>
)	)	kIx)	)
a	a	k8xC	a
doslova	doslova	k6eAd1	doslova
znamená	znamenat	k5eAaImIp3nS	znamenat
Pahorek	pahorek	k1gInSc1	pahorek
věštců	věštec	k1gMnPc2	věštec
<g/>
.	.	kIx.	.
</s>
<s>
Odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
do	do	k7c2	do
dob	doba	k1gFnPc2	doba
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
stávala	stávat	k5eAaImAgFnS	stávat
věštírna	věštírna	k1gFnSc1	věštírna
<g/>
.	.	kIx.	.
</s>
<s>
Bažinatá	bažinatý	k2eAgFnSc1d1	bažinatá
část	část	k1gFnSc1	část
pod	pod	k7c7	pod
pahorkem	pahorek	k1gInSc7	pahorek
se	se	k3xPyFc4	se
nazývala	nazývat	k5eAaImAgFnS	nazývat
Campus	Campus	k1gMnSc1	Campus
Vaticanus	Vaticanus	k1gInSc1	Vaticanus
(	(	kIx(	(
<g/>
Vatikánské	vatikánský	k2eAgNnSc1d1	Vatikánské
pole	pole	k1gNnSc1	pole
<g/>
)	)	kIx)	)
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
byly	být	k5eAaImAgFnP	být
ještě	ještě	k9	ještě
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
pozdního	pozdní	k2eAgInSc2d1	pozdní
císařství	císařství	k1gNnSc3	císařství
vně	vně	k6eAd1	vně
Aurelianových	Aurelianův	k2eAgFnPc2d1	Aurelianův
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
tohoto	tento	k3xDgInSc2	tento
státu	stát	k1gInSc2	stát
tvoří	tvořit	k5eAaImIp3nS	tvořit
bazilika	bazilika	k1gFnSc1	bazilika
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
<g/>
,	,	kIx,	,
Apoštolský	apoštolský	k2eAgInSc1d1	apoštolský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
Sixtinská	sixtinský	k2eAgFnSc1d1	Sixtinská
kaple	kaple	k1gFnSc1	kaple
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
přilehlé	přilehlý	k2eAgFnPc1d1	přilehlá
stavby	stavba	k1gFnPc1	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vatikánské	vatikánský	k2eAgFnSc2d1	Vatikánská
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
část	část	k1gFnSc4	část
území	území	k1gNnPc2	území
tvoří	tvořit	k5eAaImIp3nP	tvořit
Vatikánské	vatikánský	k2eAgFnPc4d1	Vatikánská
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
23	[number]	k4	23
ha	ha	kA	ha
celkové	celkový	k2eAgFnSc2d1	celková
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
prostory	prostor	k1gInPc1	prostor
byly	být	k5eAaImAgInP	být
vytvářeny	vytvářit	k5eAaPmNgInP	vytvářit
během	během	k7c2	během
období	období	k1gNnSc2	období
renesance	renesance	k1gFnSc2	renesance
a	a	k8xC	a
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
zde	zde	k6eAd1	zde
najít	najít	k5eAaPmF	najít
spoustu	spousta	k1gFnSc4	spousta
kašen	kašna	k1gFnPc2	kašna
a	a	k8xC	a
soch	socha	k1gFnPc2	socha
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
60	[number]	k4	60
m.	m.	k?	m.
n.	n.	k?	n.
m.	m.	k?	m.
Zahrady	zahrada	k1gFnPc1	zahrada
jsou	být	k5eAaImIp3nP	být
ohraničeny	ohraničit	k5eAaPmNgFnP	ohraničit
kamennou	kamenný	k2eAgFnSc7d1	kamenná
zdí	zeď	k1gFnSc7	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Vatikán	Vatikán	k1gInSc1	Vatikán
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mírného	mírný	k2eAgNnSc2d1	mírné
středozemního	středozemní	k2eAgNnSc2d1	středozemní
podnebí	podnebí	k1gNnSc2	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Zimy	zima	k1gFnPc1	zima
jsou	být	k5eAaImIp3nP	být
mírné	mírný	k2eAgFnPc1d1	mírná
<g/>
,	,	kIx,	,
deštivé	deštivý	k2eAgFnPc1d1	deštivá
a	a	k8xC	a
probíhají	probíhat	k5eAaImIp3nP	probíhat
od	od	k7c2	od
září	září	k1gNnSc2	září
do	do	k7c2	do
zhruba	zhruba	k6eAd1	zhruba
poloviny	polovina	k1gFnSc2	polovina
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgNnSc1d1	letní
<g/>
,	,	kIx,	,
suché	suchý	k2eAgNnSc1d1	suché
a	a	k8xC	a
horké	horký	k2eAgNnSc1d1	horké
období	období	k1gNnSc1	období
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
května	květen	k1gInSc2	květen
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Papežský	papežský	k2eAgInSc1d1	papežský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
vatikánském	vatikánský	k2eAgNnSc6d1	Vatikánské
území	území	k1gNnSc6	území
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
svatyně	svatyně	k1gFnSc1	svatyně
u	u	k7c2	u
hrobu	hrob	k1gInSc2	hrob
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
v	v	k7c6	v
sousedním	sousední	k2eAgInSc6d1	sousední
Neronově	Neronův	k2eAgInSc6d1	Neronův
cirku	cirk	k1gInSc6	cirk
umučen	umučit	k5eAaPmNgMnS	umučit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
hrob	hrob	k1gInSc1	hrob
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
nacházet	nacházet	k5eAaImF	nacházet
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Vatikánské	vatikánský	k2eAgFnSc6d1	Vatikánská
nekropoli	nekropole	k1gFnSc6	nekropole
pod	pod	k7c7	pod
kryptami	krypta	k1gFnPc7	krypta
chrámu	chrám	k1gInSc2	chrám
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
starověkého	starověký	k2eAgNnSc2d1	starověké
pohřebiště	pohřebiště	k1gNnSc2	pohřebiště
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
u	u	k7c2	u
Vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
pahorku	pahorek	k1gInSc2	pahorek
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
za	za	k7c2	za
časů	čas	k1gInPc2	čas
císaře	císař	k1gMnSc2	císař
Caliguly	Caligula	k1gFnSc2	Caligula
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
zbudován	zbudován	k2eAgInSc4d1	zbudován
vatikánský	vatikánský	k2eAgInSc4d1	vatikánský
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1308	[number]	k4	1308
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
odstěhovali	odstěhovat	k5eAaPmAgMnP	odstěhovat
do	do	k7c2	do
Avignonu	Avignon	k1gInSc2	Avignon
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
papežským	papežský	k2eAgNnSc7d1	papežské
sídlem	sídlo	k1gNnSc7	sídlo
většinou	většinou	k6eAd1	většinou
Laterán	Laterán	k1gInSc4	Laterán
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
od	od	k7c2	od
návratu	návrat	k1gInSc2	návrat
roku	rok	k1gInSc2	rok
1377	[number]	k4	1377
je	být	k5eAaImIp3nS	být
Vatikán	Vatikán	k1gInSc1	Vatikán
stálým	stálý	k2eAgNnSc7d1	stálé
sídlem	sídlo	k1gNnSc7	sídlo
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
vatikánský	vatikánský	k2eAgInSc1d1	vatikánský
stát	stát	k1gInSc1	stát
nahradil	nahradit	k5eAaPmAgInS	nahradit
Papežský	papežský	k2eAgInSc1d1	papežský
stát	stát	k1gInSc1	stát
(	(	kIx(	(
<g/>
Patrimonium	patrimonium	k1gNnSc1	patrimonium
sancti	sancť	k1gFnSc2	sancť
Petri	Petri	k1gNnSc2	Petri
<g/>
,	,	kIx,	,
Stati	stať	k1gFnSc3	stať
pontifici	pontifice	k1gFnSc3	pontifice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
zformoval	zformovat	k5eAaPmAgMnS	zformovat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tzv.	tzv.	kA	tzv.
Pipinovy	Pipinův	k2eAgFnPc4d1	Pipinova
donace	donace	k1gFnPc4	donace
jako	jako	k8xS	jako
území	území	k1gNnPc4	území
na	na	k7c6	na
Apeninském	apeninský	k2eAgInSc6d1	apeninský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
přímou	přímý	k2eAgFnSc7d1	přímá
světskou	světský	k2eAgFnSc7d1	světská
vládou	vláda	k1gFnSc7	vláda
římského	římský	k2eAgMnSc2d1	římský
biskupa	biskup	k1gMnSc2	biskup
(	(	kIx(	(
<g/>
papeže	papež	k1gMnSc2	papež
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
územní	územní	k2eAgInSc1d1	územní
rozsah	rozsah	k1gInSc1	rozsah
se	se	k3xPyFc4	se
během	během	k7c2	během
století	století	k1gNnSc2	století
měnil	měnit	k5eAaImAgMnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
papežského	papežský	k2eAgInSc2d1	papežský
státu	stát	k1gInSc2	stát
připojena	připojen	k2eAgFnSc1d1	připojena
k	k	k7c3	k
Italskému	italský	k2eAgNnSc3d1	italské
království	království	k1gNnSc3	království
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
Řím	Řím	k1gInSc1	Řím
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Papežský	papežský	k2eAgInSc1d1	papežský
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
snažil	snažit	k5eAaImAgMnS	snažit
uchovat	uchovat	k5eAaPmF	uchovat
si	se	k3xPyFc3	se
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
styky	styk	k1gInPc4	styk
a	a	k8xC	a
postavení	postavení	k1gNnSc4	postavení
subjektu	subjekt	k1gInSc2	subjekt
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
států	stát	k1gInPc2	stát
mu	on	k3xPp3gMnSc3	on
toto	tento	k3xDgNnSc4	tento
postavení	postavení	k1gNnSc4	postavení
nikdy	nikdy	k6eAd1	nikdy
neodepřela	odepřít	k5eNaPmAgFnS	odepřít
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
Vatikán	Vatikán	k1gInSc1	Vatikán
začleněn	začleněn	k2eAgInSc1d1	začleněn
do	do	k7c2	do
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
několikrát	několikrát	k6eAd1	několikrát
obnoveny	obnovit	k5eAaPmNgInP	obnovit
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pahorek	pahorek	k1gInSc1	pahorek
Vatikán	Vatikán	k1gInSc1	Vatikán
a	a	k8xC	a
některá	některý	k3yIgNnPc1	některý
exteritorální	exteritorální	k2eAgNnPc1d1	exteritorální
území	území	k1gNnPc1	území
byla	být	k5eAaImAgNnP	být
navrácena	navrátit	k5eAaPmNgNnP	navrátit
pod	pod	k7c4	pod
papežskou	papežský	k2eAgFnSc4d1	Papežská
správu	správa	k1gFnSc4	správa
smlouvami	smlouva	k1gFnPc7	smlouva
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1929	[number]	k4	1929
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
Lateránské	lateránský	k2eAgFnPc4d1	Lateránská
smlouvy	smlouva	k1gFnPc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
toto	tento	k3xDgNnSc4	tento
narovnání	narovnání	k1gNnSc4	narovnání
Vatikánu	Vatikán	k1gInSc2	Vatikán
nabízela	nabízet	k5eAaImAgFnS	nabízet
již	již	k6eAd1	již
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
papežové	papež	k1gMnPc1	papež
však	však	k9	však
ještě	ještě	k6eAd1	ještě
doufali	doufat	k5eAaImAgMnP	doufat
v	v	k7c6	v
obnovení	obnovení	k1gNnSc6	obnovení
Papežského	papežský	k2eAgInSc2d1	papežský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
na	na	k7c4	na
podobné	podobný	k2eAgFnPc4d1	podobná
dohody	dohoda	k1gFnPc4	dohoda
nechtěli	chtít	k5eNaImAgMnP	chtít
dlouho	dlouho	k6eAd1	dlouho
přistoupit	přistoupit	k5eAaPmF	přistoupit
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
Vatikánem	Vatikán	k1gInSc7	Vatikán
a	a	k8xC	a
Itálií	Itálie	k1gFnSc7	Itálie
byl	být	k5eAaImAgMnS	být
pak	pak	k6eAd1	pak
ještě	ještě	k6eAd1	ještě
upraven	upravit	k5eAaPmNgInS	upravit
konkordátem	konkordát	k1gInSc7	konkordát
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Vatikán	Vatikán	k1gInSc1	Vatikán
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
800	[number]	k4	800
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
většinu	většina	k1gFnSc4	většina
tvoří	tvořit	k5eAaImIp3nP	tvořit
diplomaté	diplomat	k1gMnPc1	diplomat
(	(	kIx(	(
<g/>
nunciové	nuncius	k1gMnPc1	nuncius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc4	zbytek
tvoří	tvořit	k5eAaImIp3nP	tvořit
jednak	jednak	k8xC	jednak
kardinálové	kardinál	k1gMnPc1	kardinál
a	a	k8xC	a
hodnostáři	hodnostář	k1gMnPc1	hodnostář
papežských	papežský	k2eAgNnPc2d1	papežské
grémií	grémium	k1gNnPc2	grémium
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
úředníci	úředník	k1gMnPc1	úředník
a	a	k8xC	a
101	[number]	k4	101
členů	člen	k1gMnPc2	člen
Švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
gardy	garda	k1gFnSc2	garda
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
asi	asi	k9	asi
2	[number]	k4	2
500	[number]	k4	500
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zde	zde	k6eAd1	zde
pracují	pracovat	k5eAaImIp3nP	pracovat
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Vatikán	Vatikán	k1gInSc1	Vatikán
nemá	mít	k5eNaImIp3nS	mít
stanovený	stanovený	k2eAgInSc4d1	stanovený
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Svatého	svatý	k2eAgInSc2d1	svatý
stolce	stolec	k1gInSc2	stolec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
využívá	využívat	k5eAaPmIp3nS	využívat
latinu	latina	k1gFnSc4	latina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
k	k	k7c3	k
běžné	běžný	k2eAgFnSc3d1	běžná
komunikaci	komunikace	k1gFnSc3	komunikace
používá	používat	k5eAaImIp3nS	používat
italština	italština	k1gFnSc1	italština
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
garda	garda	k1gFnSc1	garda
dostává	dostávat	k5eAaImIp3nS	dostávat
povely	povel	k1gInPc7	povel
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
příslušník	příslušník	k1gMnSc1	příslušník
této	tento	k3xDgFnSc2	tento
gardy	garda	k1gFnSc2	garda
pak	pak	k6eAd1	pak
skládá	skládat	k5eAaImIp3nS	skládat
svou	svůj	k3xOyFgFnSc4	svůj
přísahu	přísaha	k1gFnSc4	přísaha
v	v	k7c6	v
mateřském	mateřský	k2eAgInSc6d1	mateřský
jazyce	jazyk	k1gInSc6	jazyk
např.	např.	kA	např.
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
italštině	italština	k1gFnSc6	italština
<g/>
,	,	kIx,	,
francouzštině	francouzština	k1gFnSc3	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
Vatikánu	Vatikán	k1gInSc2	Vatikán
je	být	k5eAaImIp3nS	být
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
garda	garda	k1gFnSc1	garda
<g/>
,	,	kIx,	,
vojenská	vojenský	k2eAgFnSc1d1	vojenská
jednotka	jednotka	k1gFnSc1	jednotka
Svatého	svatý	k2eAgInSc2d1	svatý
stolce	stolec	k1gInSc2	stolec
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
zodpovědností	zodpovědnost	k1gFnSc7	zodpovědnost
je	být	k5eAaImIp3nS	být
osobní	osobní	k2eAgNnSc4d1	osobní
bezpečí	bezpečí	k1gNnSc4	bezpečí
papeže	papež	k1gMnSc2	papež
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
pobývajících	pobývající	k2eAgMnPc2d1	pobývající
na	na	k7c6	na
území	území	k1gNnSc6	území
městského	městský	k2eAgInSc2d1	městský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Papežská	papežský	k2eAgFnSc1d1	Papežská
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
garda	garda	k1gFnSc1	garda
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
katolickými	katolický	k2eAgInPc7d1	katolický
kantony	kanton	k1gInPc7	kanton
Švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
konfederace	konfederace	k1gFnSc2	konfederace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1506	[number]	k4	1506
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
příslušníci	příslušník	k1gMnPc1	příslušník
se	se	k3xPyFc4	se
vyznamenali	vyznamenat	k5eAaPmAgMnP	vyznamenat
břehem	břeh	k1gInSc7	břeh
Sacco	Sacco	k1gMnSc1	Sacco
di	di	k?	di
Roma	Rom	k1gMnSc4	Rom
roku	rok	k1gInSc2	rok
1527	[number]	k4	1527
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
rekruti	rekrut	k1gMnPc1	rekrut
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
katolického	katolický	k2eAgNnSc2d1	katolické
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
nesezdaní	sezdaný	k2eNgMnPc1d1	nesezdaný
muži	muž	k1gMnPc1	muž
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
národnosti	národnost	k1gFnSc2	národnost
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
dokončili	dokončit	k5eAaPmAgMnP	dokončit
základní	základní	k2eAgFnSc4d1	základní
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
ve	v	k7c6	v
Švýcarské	švýcarský	k2eAgFnSc6d1	švýcarská
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
19	[number]	k4	19
a	a	k8xC	a
30	[number]	k4	30
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
174	[number]	k4	174
cm	cm	kA	cm
vysocí	vysoký	k2eAgMnPc1d1	vysoký
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
gardy	garda	k1gFnSc2	garda
jsou	být	k5eAaImIp3nP	být
vybaveni	vybavit	k5eAaPmNgMnP	vybavit
střelnými	střelný	k2eAgFnPc7d1	střelná
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
také	také	k9	také
halapartnami	halapartna	k1gFnPc7	halapartna
<g/>
,	,	kIx,	,
a	a	k8xC	a
vycvičení	vycvičený	k2eAgMnPc1d1	vycvičený
jako	jako	k8xS	jako
bodyguardi	bodyguard	k1gMnPc1	bodyguard
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
k	k	k7c3	k
ozbrojeným	ozbrojený	k2eAgFnPc3d1	ozbrojená
složkám	složka	k1gFnPc3	složka
Papežského	papežský	k2eAgInSc2d1	papežský
státu	stát	k1gInSc2	stát
patřili	patřit	k5eAaImAgMnP	patřit
také	také	k9	také
Papežští	papežský	k2eAgMnPc1d1	papežský
zuávové	zuáv	k1gMnPc1	zuáv
<g/>
,	,	kIx,	,
Palatini	Palatin	k2eAgMnPc1d1	Palatin
<g/>
,	,	kIx,	,
Jízdní	jízdní	k2eAgFnSc1d1	jízdní
garda	garda	k1gFnSc1	garda
a	a	k8xC	a
Korsická	korsický	k2eAgFnSc1d1	Korsická
garda	garda	k1gFnSc1	garda
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
vojenské	vojenský	k2eAgFnPc1d1	vojenská
složky	složka	k1gFnPc1	složka
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
gardy	garda	k1gFnSc2	garda
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
papežem	papež	k1gMnSc7	papež
Pavlem	Pavel	k1gMnSc7	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
rozpuštěny	rozpuštěn	k2eAgFnPc1d1	rozpuštěna
a	a	k8xC	a
policie	policie	k1gFnSc1	policie
byla	být	k5eAaImAgFnS	být
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
z	z	k7c2	z
vojenské	vojenský	k2eAgFnSc2d1	vojenská
do	do	k7c2	do
civilní	civilní	k2eAgFnSc2d1	civilní
sféry	sféra	k1gFnSc2	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Corpo	Corpa	k1gFnSc5	Corpa
della	della	k1gFnSc1	della
Gendarmeria	Gendarmerium	k1gNnSc2	Gendarmerium
je	být	k5eAaImIp3nS	být
četnictvo	četnictvo	k1gNnSc1	četnictvo
<g/>
,	,	kIx,	,
policie	policie	k1gFnSc1	policie
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
jednotka	jednotka	k1gFnSc1	jednotka
Města	město	k1gNnSc2	město
Vatikán	Vatikán	k1gInSc1	Vatikán
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
exteritoriálních	exteritoriální	k2eAgInPc2d1	exteritoriální
majetků	majetek	k1gInPc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
dohled	dohled	k1gInSc1	dohled
nad	nad	k7c7	nad
veřejným	veřejný	k2eAgInSc7d1	veřejný
pořádkem	pořádek	k1gInSc7	pořádek
<g/>
,	,	kIx,	,
kontrola	kontrola	k1gFnSc1	kontrola
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
dodržování	dodržování	k1gNnSc1	dodržování
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgFnSc1d1	dopravní
kontrola	kontrola	k1gFnSc1	kontrola
a	a	k8xC	a
kriminální	kriminální	k2eAgNnSc1d1	kriminální
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
v	v	k7c6	v
Městském	městský	k2eAgInSc6d1	městský
státu	stát	k1gInSc6	stát
Vatikán	Vatikán	k1gInSc4	Vatikán
včetně	včetně	k7c2	včetně
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
papeže	papež	k1gMnSc2	papež
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
Vatikánu	Vatikán	k1gInSc2	Vatikán
<g/>
.	.	kIx.	.
</s>
<s>
Sbor	sbor	k1gInSc1	sbor
má	mít	k5eAaImIp3nS	mít
130	[number]	k4	130
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
gardy	garda	k1gFnSc2	garda
podřízen	podřízen	k2eAgInSc4d1	podřízen
vedení	vedení	k1gNnSc4	vedení
Městského	městský	k2eAgInSc2d1	městský
státu	stát	k1gInSc2	stát
Vatikán	Vatikán	k1gInSc1	Vatikán
<g/>
.	.	kIx.	.
</s>
<s>
Vatikán	Vatikán	k1gInSc1	Vatikán
představuje	představovat	k5eAaImIp3nS	představovat
stát	stát	k5eAaImF	stát
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
vysokou	vysoký	k2eAgFnSc7d1	vysoká
kriminalitou	kriminalita	k1gFnSc7	kriminalita
(	(	kIx(	(
<g/>
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
dvacetkrát	dvacetkrát	k6eAd1	dvacetkrát
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
to	ten	k3xDgNnSc1	ten
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
kapsáři	kapsář	k1gMnPc1	kapsář
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
okrádají	okrádat	k5eAaImIp3nP	okrádat
poutníky	poutník	k1gMnPc4	poutník
a	a	k8xC	a
turisty	turist	k1gMnPc4	turist
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
před	před	k7c4	před
soud	soud	k1gInSc4	soud
nikdy	nikdy	k6eAd1	nikdy
nedostane	dostat	k5eNaPmIp3nS	dostat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
obtížně	obtížně	k6eAd1	obtížně
odhalitelní	odhalitelný	k2eAgMnPc1d1	odhalitelný
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
hranice	hranice	k1gFnSc1	hranice
Vatikánu	Vatikán	k1gInSc2	Vatikán
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
jsou	být	k5eAaImIp3nP	být
blízko	blízko	k6eAd1	blízko
a	a	k8xC	a
nestřežené	střežený	k2eNgNnSc1d1	nestřežené
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
spolehlivou	spolehlivý	k2eAgFnSc4d1	spolehlivá
možnost	možnost	k1gFnSc4	možnost
utéct	utéct	k5eAaPmF	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
zločinci	zločinec	k1gMnPc1	zločinec
přistiženi	přistihnout	k5eAaPmNgMnP	přistihnout
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
Vatikán	Vatikán	k1gInSc4	Vatikán
svého	svůj	k3xOyFgMnSc2	svůj
generálního	generální	k2eAgMnSc2d1	generální
prokurátora	prokurátor	k1gMnSc2	prokurátor
(	(	kIx(	(
<g/>
aktuálně	aktuálně	k6eAd1	aktuálně
Nicola	Nicola	k1gFnSc1	Nicola
Picardi	Picard	k1gMnPc5	Picard
<g/>
)	)	kIx)	)
a	a	k8xC	a
soudce	soudce	k1gMnSc1	soudce
(	(	kIx(	(
<g/>
Gianluigi	Gianluig	k1gMnSc5	Gianluig
Marrone	Marron	k1gMnSc5	Marron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
jejich	jejich	k3xOp3gFnSc2	jejich
práce	práce	k1gFnSc2	práce
představují	představovat	k5eAaImIp3nP	představovat
kauzy	kauza	k1gFnPc1	kauza
kapesních	kapesní	k2eAgFnPc2d1	kapesní
krádeží	krádež	k1gFnPc2	krádež
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
případy	případ	k1gInPc4	případ
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těch	ten	k3xDgMnPc2	ten
často	často	k6eAd1	často
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
problém	problém	k1gInSc4	problém
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vatikán	Vatikán	k1gInSc1	Vatikán
nemá	mít	k5eNaImIp3nS	mít
komplexní	komplexní	k2eAgInPc4d1	komplexní
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
kauzy	kauza	k1gFnPc4	kauza
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
řešeny	řešit	k5eAaImNgInP	řešit
často	často	k6eAd1	často
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
a	a	k8xC	a
některých	některý	k3yIgNnPc2	některý
velmi	velmi	k6eAd1	velmi
vágních	vágní	k2eAgNnPc2d1	vágní
ustanovení	ustanovení	k1gNnPc2	ustanovení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vatikánský	vatikánský	k2eAgInSc1d1	vatikánský
trestní	trestní	k2eAgInSc1d1	trestní
zákon	zákon	k1gInSc1	zákon
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
Vatikánu	Vatikán	k1gInSc2	Vatikán
(	(	kIx(	(
<g/>
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
svým	svůj	k3xOyFgNnSc7	svůj
zadržením	zadržení	k1gNnSc7	zadržení
propuštěný	propuštěný	k2eAgInSc4d1	propuštěný
pro	pro	k7c4	pro
trestní	trestní	k2eAgNnSc4d1	trestní
stíhání	stíhání	k1gNnSc4	stíhání
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
přistižen	přistihnout	k5eAaPmNgInS	přistihnout
při	při	k7c6	při
držení	držení	k1gNnSc6	držení
drog	droga	k1gFnPc2	droga
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zákony	zákon	k1gInPc4	zákon
Vatikánu	Vatikán	k1gInSc2	Vatikán
neřeší	řešit	k5eNaImIp3nP	řešit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
na	na	k7c4	na
4	[number]	k4	4
měsíce	měsíc	k1gInSc2	měsíc
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
na	na	k7c6	na
základě	základ	k1gInSc6	základ
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
protidrogové	protidrogový	k2eAgFnSc2d1	protidrogová
konvence	konvence	k1gFnSc2	konvence
a	a	k8xC	a
trestního	trestní	k2eAgInSc2d1	trestní
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
udělit	udělit	k5eAaPmF	udělit
tento	tento	k3xDgInSc4	tento
trest	trest	k1gInSc4	trest
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jednání	jednání	k1gNnSc1	jednání
zákonem	zákon	k1gInSc7	zákon
přímo	přímo	k6eAd1	přímo
nejmenovaného	jmenovaný	k2eNgInSc2d1	nejmenovaný
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
"	"	kIx"	"
<g/>
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
morálku	morálka	k1gFnSc4	morálka
a	a	k8xC	a
víru	víra	k1gFnSc4	víra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Vatikánu	Vatikán	k1gInSc2	Vatikán
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Vatikánu	Vatikán	k1gInSc2	Vatikán
má	mít	k5eAaImIp3nS	mít
unikátní	unikátní	k2eAgInSc4d1	unikátní
nekomerční	komerční	k2eNgInSc4d1	nekomerční
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc7d1	oficiální
měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
euro	euro	k1gNnSc4	euro
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
vatikánskou	vatikánský	k2eAgFnSc4d1	Vatikánská
liru	lira	k1gFnSc4	lira
<g/>
.	.	kIx.	.
</s>
<s>
Vatikán	Vatikán	k1gInSc1	Vatikán
razí	razit	k5eAaImIp3nS	razit
vlastní	vlastní	k2eAgInSc1d1	vlastní
typ	typ	k1gInSc1	typ
euromincí	eurominký	k2eAgMnPc1d1	eurominký
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc4	jejich
produkce	produkce	k1gFnSc1	produkce
je	být	k5eAaImIp3nS	být
však	však	k9	však
přísně	přísně	k6eAd1	přísně
limitována	limitovat	k5eAaBmNgFnS	limitovat
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
jsou	být	k5eAaImIp3nP	být
mince	mince	k1gFnSc2	mince
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
vatikánské	vatikánský	k2eAgFnPc4d1	Vatikánská
známky	známka	k1gFnPc4	známka
<g/>
,	,	kIx,	,
vyhledávány	vyhledáván	k2eAgFnPc4d1	vyhledávána
sběrateli	sběratel	k1gMnPc7	sběratel
<g/>
.	.	kIx.	.
</s>
<s>
Vatikán	Vatikán	k1gInSc1	Vatikán
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
banku	banka	k1gFnSc4	banka
IOR	IOR	kA	IOR
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
bankomat	bankomat	k1gInSc1	bankomat
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jediný	jediný	k2eAgInSc1d1	jediný
s	s	k7c7	s
instrukcemi	instrukce	k1gFnPc7	instrukce
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
příjmy	příjem	k1gInPc1	příjem
tvoří	tvořit	k5eAaImIp3nP	tvořit
hlavně	hlavně	k9	hlavně
turistický	turistický	k2eAgInSc4d1	turistický
ruch	ruch	k1gInSc4	ruch
<g/>
,	,	kIx,	,
pronájem	pronájem	k1gInSc4	pronájem
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
,	,	kIx,	,
dary	dar	k1gInPc1	dar
a	a	k8xC	a
příspěvky	příspěvek	k1gInPc1	příspěvek
národních	národní	k2eAgFnPc2d1	národní
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Vatikán	Vatikán	k1gInSc1	Vatikán
chce	chtít	k5eAaImIp3nS	chtít
"	"	kIx"	"
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
pokrývat	pokrývat	k5eAaImF	pokrývat
z	z	k7c2	z
20	[number]	k4	20
procent	procento	k1gNnPc2	procento
svou	svůj	k3xOyFgFnSc4	svůj
energetickou	energetický	k2eAgFnSc4d1	energetická
potřebu	potřeba	k1gFnSc4	potřeba
z	z	k7c2	z
obnovitelných	obnovitelný	k2eAgInPc2d1	obnovitelný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
vytyčila	vytyčit	k5eAaPmAgFnS	vytyčit
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
krokem	krok	k1gInSc7	krok
byla	být	k5eAaImAgFnS	být
instalace	instalace	k1gFnSc1	instalace
solárních	solární	k2eAgInPc2d1	solární
panelů	panel	k1gInPc2	panel
na	na	k7c4	na
střechu	střecha	k1gFnSc4	střecha
auly	aula	k1gFnSc2	aula
Pavla	Pavel	k1gMnSc2	Pavel
VI	VI	kA	VI
<g/>
..	..	k?	..
Dále	daleko	k6eAd2	daleko
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
omezení	omezení	k1gNnSc3	omezení
počtu	počet	k1gInSc2	počet
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
vjíždět	vjíždět	k5eAaImF	vjíždět
na	na	k7c6	na
území	území	k1gNnSc6	území
Vatikánu	Vatikán	k1gInSc2	Vatikán
a	a	k8xC	a
parkovat	parkovat	k5eAaImF	parkovat
na	na	k7c6	na
něm.	něm.	k?	něm.
V	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
i	i	k9	i
o	o	k7c4	o
zabudování	zabudování	k1gNnSc4	zabudování
dalších	další	k2eAgInPc2d1	další
solárních	solární	k2eAgInPc2d1	solární
panelů	panel	k1gInPc2	panel
na	na	k7c4	na
jiné	jiný	k2eAgFnPc4d1	jiná
budovy	budova	k1gFnPc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Vatikán	Vatikán	k1gInSc1	Vatikán
není	být	k5eNaImIp3nS	být
členem	člen	k1gInSc7	člen
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
států	stát	k1gInPc2	stát
<g/>
)	)	kIx)	)
a	a	k8xC	a
nepodepsal	podepsat	k5eNaPmAgMnS	podepsat
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
deklaraci	deklarace	k1gFnSc4	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Konvenci	konvence	k1gFnSc4	konvence
o	o	k7c6	o
právech	právo	k1gNnPc6	právo
handicapovaných	handicapovaný	k2eAgFnPc2d1	handicapovaná
osob	osoba	k1gFnPc2	osoba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vatikán	Vatikán	k1gInSc1	Vatikán
není	být	k5eNaImIp3nS	být
členem	člen	k1gInSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
ani	ani	k8xC	ani
Evropské	evropský	k2eAgFnSc2d1	Evropská
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
měnové	měnový	k2eAgFnSc3d1	měnová
unii	unie	k1gFnSc3	unie
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
smí	smět	k5eAaImIp3nS	smět
razit	razit	k5eAaImF	razit
své	svůj	k3xOyFgInPc4	svůj
originální	originální	k2eAgInPc4d1	originální
euromince	eurominec	k1gInPc4	eurominec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
má	mít	k5eAaImIp3nS	mít
Vatikán	Vatikán	k1gInSc4	Vatikán
vlastní	vlastní	k2eAgNnSc1d1	vlastní
malé	malý	k2eAgNnSc1d1	malé
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
však	však	k9	však
slouží	sloužit	k5eAaImIp3nS	sloužit
hlavně	hlavně	k9	hlavně
k	k	k7c3	k
přepravě	přeprava	k1gFnSc3	přeprava
nákladů	náklad	k1gInPc2	náklad
a	a	k8xC	a
jen	jen	k9	jen
zcela	zcela	k6eAd1	zcela
výjimečně	výjimečně	k6eAd1	výjimečně
k	k	k7c3	k
osobní	osobní	k2eAgFnSc3d1	osobní
dopravě	doprava	k1gFnSc3	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
<g/>
.	.	kIx.	.
</s>
<s>
Městu	město	k1gNnSc3	město
slouží	sloužit	k5eAaImIp3nP	sloužit
nezávislý	závislý	k2eNgInSc1d1	nezávislý
moderní	moderní	k2eAgInSc1d1	moderní
telefonní	telefonní	k2eAgInSc1d1	telefonní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
vatikánská	vatikánský	k2eAgFnSc1d1	Vatikánská
lékárna	lékárna	k1gFnSc1	lékárna
a	a	k8xC	a
pošta	pošta	k1gFnSc1	pošta
<g/>
.	.	kIx.	.
</s>
<s>
Poštovní	poštovní	k2eAgInSc1d1	poštovní
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vatikán	Vatikán	k1gInSc1	Vatikán
také	také	k9	také
disponuje	disponovat	k5eAaBmIp3nS	disponovat
vlastní	vlastní	k2eAgFnSc7d1	vlastní
internetovou	internetový	k2eAgFnSc7d1	internetová
doménou	doména	k1gFnSc7	doména
1	[number]	k4	1
<g/>
.	.	kIx.	.
řádu	řád	k1gInSc2	řád
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
va	va	k0wR	va
<g/>
"	"	kIx"	"
a	a	k8xC	a
předponou	předpona	k1gFnSc7	předpona
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
telekomunikační	telekomunikační	k2eAgFnSc2d1	telekomunikační
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
využívána	využívat	k5eAaImNgFnS	využívat
radioamatéry	radioamatér	k1gMnPc7	radioamatér
<g/>
.	.	kIx.	.
</s>
<s>
Radio	radio	k1gNnSc1	radio
Vatikán	Vatikán	k1gInSc1	Vatikán
vysílá	vysílat	k5eAaImIp3nS	vysílat
na	na	k7c6	na
krátkých	krátká	k1gFnPc6	krátká
<g/>
,	,	kIx,	,
středních	střední	k2eAgFnPc6d1	střední
i	i	k8xC	i
velmi	velmi	k6eAd1	velmi
krátkých	krátký	k2eAgFnPc6d1	krátká
vlnách	vlna	k1gFnPc6	vlna
a	a	k8xC	a
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
vysílače	vysílač	k1gInPc1	vysílač
jsou	být	k5eAaImIp3nP	být
umístěny	umístěn	k2eAgMnPc4d1	umístěn
na	na	k7c6	na
italském	italský	k2eAgNnSc6d1	italské
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Televizní	televizní	k2eAgNnSc1d1	televizní
vysílání	vysílání	k1gNnSc1	vysílání
připravuje	připravovat	k5eAaImIp3nS	připravovat
Vatikánské	vatikánský	k2eAgNnSc1d1	Vatikánské
televizní	televizní	k2eAgNnSc1d1	televizní
centrum	centrum	k1gNnSc1	centrum
Telepace	Telepace	k1gFnSc2	Telepace
a	a	k8xC	a
italská	italský	k2eAgFnSc1d1	italská
veřejnoprávní	veřejnoprávní	k2eAgFnSc1d1	veřejnoprávní
televize	televize	k1gFnSc1	televize
RAI	RAI	kA	RAI
která	který	k3yIgFnSc1	který
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Rai	Rai	k1gFnSc2	Rai
Vaticano	Vaticana	k1gFnSc5	Vaticana
<g/>
.	.	kIx.	.
</s>
