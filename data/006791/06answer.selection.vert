<s>
Za	za	k7c2	za
krále	král	k1gMnSc2	král
instantních	instantní	k2eAgFnPc2d1	instantní
polévek	polévka	k1gFnPc2	polévka
bývá	bývat	k5eAaImIp3nS	bývat
označován	označován	k2eAgMnSc1d1	označován
Carl	Carl	k1gMnSc1	Carl
Heinrich	Heinrich	k1gMnSc1	Heinrich
Knorr	Knorr	k1gMnSc1	Knorr
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
sušených	sušený	k2eAgFnPc2d1	sušená
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
syny	syn	k1gMnPc7	syn
uvedl	uvést	k5eAaPmAgMnS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
řadu	řad	k1gInSc2	řad
sušených	sušený	k2eAgFnPc2d1	sušená
zelenin	zelenina	k1gFnPc2	zelenina
a	a	k8xC	a
koření	koření	k1gNnPc2	koření
do	do	k7c2	do
polévek	polévka	k1gFnPc2	polévka
<g/>
.	.	kIx.	.
</s>
