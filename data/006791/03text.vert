<s>
Instantní	instantní	k2eAgFnSc1d1	instantní
polévka	polévka	k1gFnSc1	polévka
je	být	k5eAaImIp3nS	být
dehydrovaná	dehydrovaný	k2eAgFnSc1d1	dehydrovaná
polévka	polévka	k1gFnSc1	polévka
dodávaná	dodávaný	k2eAgFnSc1d1	dodávaná
na	na	k7c4	na
trh	trh	k1gInSc4	trh
v	v	k7c6	v
sáčku	sáček	k1gInSc6	sáček
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
před	před	k7c7	před
spotřebou	spotřeba	k1gFnSc7	spotřeba
doplní	doplnit	k5eAaPmIp3nS	doplnit
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
zalije	zalít	k5eAaPmIp3nS	zalít
vřící	vřící	k2eAgFnSc7d1	vřící
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
rozmíchané	rozmíchaný	k2eAgFnPc1d1	rozmíchaná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
uvaří	uvařit	k5eAaPmIp3nS	uvařit
<g/>
.	.	kIx.	.
</s>
<s>
Návod	návod	k1gInSc1	návod
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
obalu	obal	k1gInSc6	obal
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
polévek	polévka	k1gFnPc2	polévka
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
obalu	obal	k1gInSc6	obal
zatavené	zatavený	k2eAgNnSc1d1	zatavené
v	v	k7c6	v
minisáčcích	minisáček	k1gInPc6	minisáček
i	i	k8xC	i
přílohy	příloha	k1gFnPc1	příloha
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
provozovny	provozovna	k1gFnPc4	provozovna
rychlého	rychlý	k2eAgNnSc2d1	rychlé
občerstvení	občerstvení	k1gNnSc2	občerstvení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pro	pro	k7c4	pro
restaurace	restaurace	k1gFnPc4	restaurace
a	a	k8xC	a
provozovny	provozovna	k1gFnPc4	provozovna
závodního	závodní	k2eAgMnSc2d1	závodní
či	či	k8xC	či
školního	školní	k2eAgNnSc2d1	školní
stravování	stravování	k1gNnSc2	stravování
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
instantní	instantní	k2eAgFnPc4d1	instantní
polévky	polévka	k1gFnPc4	polévka
v	v	k7c4	v
gastro	gastro	k1gNnSc4	gastro
balení	balení	k1gNnSc2	balení
<g/>
.	.	kIx.	.
</s>
<s>
Popudem	popud	k1gInSc7	popud
k	k	k7c3	k
výzkumu	výzkum	k1gInSc3	výzkum
byl	být	k5eAaImAgMnS	být
požadavek	požadavek	k1gInSc4	požadavek
generála	generál	k1gMnSc2	generál
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
císaře	císař	k1gMnSc4	císař
Napoleona	Napoleon	k1gMnSc4	Napoleon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vypsal	vypsat	k5eAaPmAgMnS	vypsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1795	[number]	k4	1795
dotovanou	dotovaný	k2eAgFnSc4d1	dotovaná
soutěž	soutěž	k1gFnSc4	soutěž
na	na	k7c4	na
dodavatele	dodavatel	k1gMnSc4	dodavatel
zásob	zásoba	k1gFnPc2	zásoba
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
14	[number]	k4	14
let	léto	k1gNnPc2	léto
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
první	první	k4xOgFnSc1	první
sklenice	sklenice	k1gFnSc1	sklenice
se	s	k7c7	s
sterilizovaným	sterilizovaný	k2eAgNnSc7d1	sterilizované
jídlem	jídlo	k1gNnSc7	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Sklenice	sklenice	k1gFnPc1	sklenice
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1810	[number]	k4	1810
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
kovovou	kovový	k2eAgFnSc7d1	kovová
konzervou	konzerva	k1gFnSc7	konzerva
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
krále	král	k1gMnSc2	král
instantních	instantní	k2eAgFnPc2d1	instantní
polévek	polévka	k1gFnPc2	polévka
bývá	bývat	k5eAaImIp3nS	bývat
označován	označován	k2eAgMnSc1d1	označován
Carl	Carl	k1gMnSc1	Carl
Heinrich	Heinrich	k1gMnSc1	Heinrich
Knorr	Knorr	k1gMnSc1	Knorr
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
sušených	sušený	k2eAgFnPc2d1	sušená
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
syny	syn	k1gMnPc7	syn
uvedl	uvést	k5eAaPmAgMnS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
řadu	řad	k1gInSc2	řad
sušených	sušený	k2eAgFnPc2d1	sušená
zelenin	zelenina	k1gFnPc2	zelenina
a	a	k8xC	a
koření	koření	k1gNnPc2	koření
do	do	k7c2	do
polévek	polévka	k1gFnPc2	polévka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgFnSc4	první
instantní	instantní	k2eAgFnSc4d1	instantní
polévku	polévka	k1gFnSc4	polévka
bývá	bývat	k5eAaImIp3nS	bývat
označena	označen	k2eAgFnSc1d1	označena
Hrachová	hrachový	k2eAgFnSc1d1	hrachová
klobása	klobása	k1gFnSc1	klobása
<g/>
,	,	kIx,	,
dodávána	dodávat	k5eAaImNgFnS	dodávat
na	na	k7c4	na
trh	trh	k1gInSc4	trh
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
dehydrace	dehydrace	k1gFnSc2	dehydrace
polévek	polévka	k1gFnPc2	polévka
byla	být	k5eAaImAgFnS	být
vynalezena	vynaleznout	k5eAaPmNgFnS	vynaleznout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
Juliem	Julius	k1gMnSc7	Julius
Maggim	Maggim	k1gInSc4	Maggim
<g/>
,	,	kIx,	,
švýcarským	švýcarský	k2eAgMnSc7d1	švýcarský
mlynářem	mlynář	k1gMnSc7	mlynář
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
dodal	dodat	k5eAaPmAgMnS	dodat
na	na	k7c4	na
trh	trh	k1gInSc4	trh
první	první	k4xOgFnSc4	první
dehydrovanou	dehydrovaný	k2eAgFnSc4d1	dehydrovaná
polévku	polévka	k1gFnSc4	polévka
z	z	k7c2	z
hrachu	hrách	k1gInSc2	hrách
a	a	k8xC	a
fazolí	fazole	k1gFnPc2	fazole
<g/>
.	.	kIx.	.
</s>
<s>
Vynálezem	vynález	k1gInSc7	vynález
řešil	řešit	k5eAaImAgInS	řešit
urychlení	urychlení	k1gNnSc4	urychlení
přípravy	příprava	k1gFnSc2	příprava
hotového	hotový	k2eAgNnSc2d1	hotové
jídla	jídlo	k1gNnSc2	jídlo
s	s	k7c7	s
dostatečnou	dostatečný	k2eAgFnSc7d1	dostatečná
výživovou	výživový	k2eAgFnSc7d1	výživová
hodnotou	hodnota	k1gFnSc7	hodnota
a	a	k8xC	a
za	za	k7c4	za
nízkou	nízký	k2eAgFnSc4d1	nízká
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
dalším	další	k2eAgInSc7d1	další
vynálezem	vynález	k1gInSc7	vynález
<g/>
,	,	kIx,	,
kořením	kořenit	k5eAaImIp1nS	kořenit
pro	pro	k7c4	pro
ochucení	ochucení	k1gNnSc4	ochucení
polévek	polévka	k1gFnPc2	polévka
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgInP	být
vynalezeny	vynalezen	k2eAgInPc1d1	vynalezen
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
instantní	instantní	k2eAgFnSc2d1	instantní
nudle	nudle	k1gFnSc2	nudle
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
instantní	instantní	k2eAgFnSc2d1	instantní
polévky	polévka	k1gFnSc2	polévka
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
snadno	snadno	k6eAd1	snadno
dostupných	dostupný	k2eAgInPc2d1	dostupný
convenience	convenienec	k1gMnPc4	convenienec
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
oblíbených	oblíbený	k2eAgMnPc2d1	oblíbený
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
rychlou	rychlý	k2eAgFnSc4d1	rychlá
přípravu	příprava	k1gFnSc4	příprava
<g/>
.	.	kIx.	.
</s>
<s>
Kvalita	kvalita	k1gFnSc1	kvalita
polévek	polévka	k1gFnPc2	polévka
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
výrobků	výrobek	k1gInPc2	výrobek
je	být	k5eAaImIp3nS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
konzervanty	konzervant	k1gInPc7	konzervant
<g/>
,	,	kIx,	,
barvícími	barvící	k2eAgFnPc7d1	barvící
a	a	k8xC	a
aromatickými	aromatický	k2eAgFnPc7d1	aromatická
látkami	látka	k1gFnPc7	látka
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
mnohdy	mnohdy	k6eAd1	mnohdy
nevhodné	vhodný	k2eNgFnPc1d1	nevhodná
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
těhotné	těhotný	k2eAgFnPc4d1	těhotná
ženy	žena	k1gFnPc4	žena
a	a	k8xC	a
lidi	člověk	k1gMnPc4	člověk
s	s	k7c7	s
alergiemi	alergie	k1gFnPc7	alergie
či	či	k8xC	či
nemocemi	nemoc	k1gFnPc7	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Polévky	polévka	k1gFnPc4	polévka
bez	bez	k7c2	bez
těchto	tento	k3xDgFnPc2	tento
příměsí	příměs	k1gFnPc2	příměs
mívají	mívat	k5eAaImIp3nP	mívat
označení	označení	k1gNnPc1	označení
BIO	BIO	k?	BIO
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
jiné	jiný	k2eAgFnPc4d1	jiná
upravené	upravený	k2eAgFnPc4d1	upravená
jako	jako	k8xS	jako
vhodné	vhodný	k2eAgFnPc4d1	vhodná
pro	pro	k7c4	pro
strávníky	strávník	k1gMnPc4	strávník
s	s	k7c7	s
dietou	dieta	k1gFnSc7	dieta
<g/>
.	.	kIx.	.
</s>
<s>
Výrobců	výrobce	k1gMnPc2	výrobce
i	i	k8xC	i
dovozců	dovozce	k1gMnPc2	dovozce
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
<g/>
.	.	kIx.	.
</s>
