<p>
<s>
Hobitín	Hobitín	k1gInSc4	Hobitín
je	být	k5eAaImIp3nS	být
fiktivní	fiktivní	k2eAgNnSc1d1	fiktivní
městečko	městečko	k1gNnSc1	městečko
vystupující	vystupující	k2eAgNnSc1d1	vystupující
v	v	k7c6	v
románu	román	k1gInSc6	román
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
od	od	k7c2	od
spisovatele	spisovatel	k1gMnSc2	spisovatel
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
<g/>
.	.	kIx.	.
</s>
<s>
Hobitín	Hobitín	k1gInSc1	Hobitín
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejhlavnějších	hlavní	k2eAgNnPc2d3	nejhlavnější
a	a	k8xC	a
největších	veliký	k2eAgNnPc2d3	veliký
sídel	sídlo	k1gNnPc2	sídlo
této	tento	k3xDgFnSc6	tento
hobití	hobití	k1gNnSc2	hobití
zemičky	zemička	k1gFnPc1	zemička
<g/>
.	.	kIx.	.
</s>
<s>
Přesně	přesně	k6eAd1	přesně
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
čtvrtce	čtvrtka	k1gFnSc6	čtvrtka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
městečka	městečko	k1gNnSc2	městečko
stojí	stát	k5eAaImIp3nS	stát
starosta	starosta	k1gMnSc1	starosta
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Hobitíně	Hobitína	k1gFnSc6	Hobitína
žijí	žít	k5eAaImIp3nP	žít
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
název	název	k1gInSc1	název
vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
<g/>
,	,	kIx,	,
hobité	hobitý	k2eAgInPc1d1	hobitý
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
norách	nora	k1gFnPc6	nora
zabudovaných	zabudovaný	k2eAgFnPc6d1	zabudovaná
ve	v	k7c6	v
svazích	svah	k1gInPc6	svah
kopců	kopec	k1gInPc2	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gMnPc2	jeho
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
a	a	k8xC	a
řemesly	řemeslo	k1gNnPc7	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
provaznické	provaznický	k2eAgFnPc1d1	Provaznická
<g/>
,	,	kIx,	,
zahradnické	zahradnický	k2eAgFnPc1d1	zahradnická
i	i	k8xC	i
mlynářské	mlynářský	k2eAgFnPc1d1	Mlynářská
rodiny	rodina	k1gFnPc1	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
pohostinství	pohostinství	k1gNnSc2	pohostinství
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
dvě	dva	k4xCgFnPc1	dva
hospody	hospody	k?	hospody
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Zeleného	Zelený	k1gMnSc2	Zelený
draka	drak	k1gMnSc2	drak
a	a	k8xC	a
Na	na	k7c6	na
Povodí	povodí	k1gNnSc6	povodí
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgMnPc1d1	zdejší
hobiti	hobit	k1gMnPc1	hobit
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
hobitů	hobit	k1gMnPc2	hobit
z	z	k7c2	z
Kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
na	na	k7c6	na
okolním	okolní	k2eAgInSc6d1	okolní
světě	svět	k1gInSc6	svět
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
nezaplétat	zaplétat	k5eNaImF	zaplétat
do	do	k7c2	do
odvážných	odvážný	k2eAgNnPc2d1	odvážné
či	či	k8xC	či
nebezpečných	bezpečný	k2eNgNnPc2d1	nebezpečné
dobrodružství	dobrodružství	k1gNnPc2	dobrodružství
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
zkrátka	zkrátka	k6eAd1	zkrátka
venkované	venkovan	k1gMnPc1	venkovan
<g/>
.	.	kIx.	.
</s>
<s>
Nemají	mít	k5eNaImIp3nP	mít
moc	moc	k6eAd1	moc
rádi	rád	k2eAgMnPc1d1	rád
své	svůj	k3xOyFgMnPc4	svůj
sousedy	soused	k1gMnPc4	soused
z	z	k7c2	z
Východnějších	východní	k2eAgFnPc2d2	východnější
částí	část	k1gFnPc2	část
Kraje	kraj	k1gInSc2	kraj
nebo	nebo	k8xC	nebo
Z	z	k7c2	z
Rádovska	Rádovsko	k1gNnSc2	Rádovsko
za	za	k7c7	za
Brandyvínou	Brandyvína	k1gFnSc7	Brandyvína
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jim	on	k3xPp3gMnPc3	on
připadají	připadat	k5eAaImIp3nP	připadat
unáhlení	unáhlení	k1gNnSc4	unáhlení
a	a	k8xC	a
divní	divný	k2eAgMnPc1d1	divný
<g/>
,	,	kIx,	,
když	když	k8xS	když
žijí	žít	k5eAaImIp3nP	žít
tak	tak	k6eAd1	tak
blízko	blízko	k6eAd1	blízko
u	u	k7c2	u
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejbohatší	bohatý	k2eAgMnPc4d3	nejbohatší
a	a	k8xC	a
nejvlivnější	vlivný	k2eAgMnPc4d3	nejvlivnější
obyvatele	obyvatel	k1gMnPc4	obyvatel
patřila	patřit	k5eAaImAgFnS	patřit
rodina	rodina	k1gFnSc1	rodina
Pytlíků	pytlík	k1gMnPc2	pytlík
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
velikou	veliký	k2eAgFnSc4d1	veliká
a	a	k8xC	a
honosnou	honosný	k2eAgFnSc4d1	honosná
noru	nora	k1gFnSc4	nora
<g/>
.	.	kIx.	.
</s>
<s>
Pytlíci	pytlík	k1gMnPc1	pytlík
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
bráni	brát	k5eAaImNgMnP	brát
jako	jako	k8xC	jako
panstvo	panstvo	k1gNnSc1	panstvo
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc4	sídlo
je	být	k5eAaImIp3nS	být
významné	významný	k2eAgNnSc4d1	významné
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
něho	on	k3xPp3gInSc2	on
pochází	pocházet	k5eAaImIp3nS	pocházet
několik	několik	k4yIc4	několik
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
z	z	k7c2	z
Pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
a	a	k8xC	a
Hobita	hobit	k1gMnSc2	hobit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
Frodo	Frodo	k1gNnSc4	Frodo
Pytlík	pytlík	k1gMnSc1	pytlík
a	a	k8xC	a
Bilbo	Bilba	k1gFnSc5	Bilba
Pytlík	pytlík	k1gInSc4	pytlík
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Samvědem	Samvěd	k1gInSc7	Samvěd
Křepelkou	křepelka	k1gFnSc7	křepelka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Skutečný	skutečný	k2eAgInSc1d1	skutečný
Hobitín	Hobitín	k1gInSc1	Hobitín
==	==	k?	==
</s>
</p>
<p>
<s>
Hobitín	Hobitín	k1gInSc1	Hobitín
neexistuje	existovat	k5eNaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
trilogii	trilogie	k1gFnSc6	trilogie
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
byly	být	k5eAaImAgFnP	být
kulisy	kulisa	k1gFnPc1	kulisa
vyrobeny	vyrobit	k5eAaPmNgFnP	vyrobit
z	z	k7c2	z
méně	málo	k6eAd2	málo
odolných	odolný	k2eAgInPc2d1	odolný
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
novou	nový	k2eAgFnSc4d1	nová
trilogii	trilogie	k1gFnSc4	trilogie
Hobit	hobit	k1gMnSc1	hobit
byl	být	k5eAaImAgInS	být
Hobitín	Hobitín	k1gInSc4	Hobitín
postaven	postavit	k5eAaPmNgInS	postavit
znovu	znovu	k6eAd1	znovu
a	a	k8xC	a
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
natáčení	natáčení	k1gNnSc2	natáčení
zde	zde	k6eAd1	zde
zůstal	zůstat	k5eAaPmAgMnS	zůstat
jako	jako	k8xS	jako
atrakce	atrakce	k1gFnSc1	atrakce
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
Hobbiton	Hobbiton	k1gInSc4	Hobbiton
Movie	Movie	k1gFnSc1	Movie
Set	set	k1gInSc1	set
Tours	Tours	k1gInSc1	Tours
<g/>
.	.	kIx.	.
</s>
<s>
Hobitín	Hobitín	k1gMnSc1	Hobitín
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
také	také	k9	také
řadu	řada	k1gFnSc4	řada
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
můžete	moct	k5eAaImIp2nP	moct
navštívit	navštívit	k5eAaPmF	navštívit
Czech	Czech	k1gInSc4	Czech
Hobbiton	Hobbiton	k1gInSc1	Hobbiton
<g/>
.	.	kIx.	.
</s>
</p>
