<p>
<s>
Krajnice	krajnice	k1gFnSc1	krajnice
je	být	k5eAaImIp3nS	být
krajní	krajní	k2eAgFnSc4d1	krajní
část	část	k1gFnSc4	část
koruny	koruna	k1gFnSc2	koruna
pozemní	pozemní	k2eAgFnSc2d1	pozemní
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
již	již	k6eAd1	již
nepatří	patřit	k5eNaImIp3nS	patřit
k	k	k7c3	k
vozovce	vozovka	k1gFnSc3	vozovka
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
zpevněné	zpevněný	k2eAgFnSc2d1	zpevněná
a	a	k8xC	a
nezpevněné	zpevněný	k2eNgFnSc2d1	nezpevněná
části	část	k1gFnSc2	část
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
od	od	k7c2	od
vozovky	vozovka	k1gFnSc2	vozovka
oddělena	oddělit	k5eAaPmNgFnS	oddělit
vodicí	vodicí	k2eAgFnSc7d1	vodicí
čarou	čára	k1gFnSc7	čára
<g/>
;	;	kIx,	;
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
čára	čára	k1gFnSc1	čára
ani	ani	k8xC	ani
chodník	chodník	k1gInSc1	chodník
<g/>
,	,	kIx,	,
považuje	považovat	k5eAaImIp3nS	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
vozovkou	vozovka	k1gFnSc7	vozovka
a	a	k8xC	a
krajnicí	krajnice	k1gFnSc7	krajnice
obvykle	obvykle	k6eAd1	obvykle
hranice	hranice	k1gFnSc2	hranice
zpevněné	zpevněný	k2eAgFnSc2d1	zpevněná
části	část	k1gFnSc2	část
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
svodidlo	svodidlo	k1gNnSc1	svodidlo
nebo	nebo	k8xC	nebo
obdobná	obdobný	k2eAgFnSc1d1	obdobná
zábrana	zábrana	k1gFnSc1	zábrana
<g/>
.	.	kIx.	.
</s>
<s>
Komunikace	komunikace	k1gFnSc1	komunikace
uličního	uliční	k2eAgInSc2d1	uliční
typu	typ	k1gInSc2	typ
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
místo	místo	k7c2	místo
krajnice	krajnice	k1gFnSc2	krajnice
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
chodník	chodník	k1gInSc1	chodník
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
však	však	k9	však
krajnice	krajnice	k1gFnSc1	krajnice
bývá	bývat	k5eAaImIp3nS	bývat
i	i	k9	i
mezi	mezi	k7c7	mezi
vozovkou	vozovka	k1gFnSc7	vozovka
a	a	k8xC	a
chodníkem	chodník	k1gInSc7	chodník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dálnici	dálnice	k1gFnSc6	dálnice
je	být	k5eAaImIp3nS	být
krajnice	krajnice	k1gFnSc1	krajnice
širší	široký	k2eAgFnSc1d2	širší
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
odstavný	odstavný	k2eAgInSc1d1	odstavný
pruh	pruh	k1gInSc1	pruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Provoz	provoz	k1gInSc1	provoz
na	na	k7c4	na
krajnici	krajnice	k1gFnSc4	krajnice
podle	podle	k7c2	podle
českých	český	k2eAgInPc2d1	český
předpisů	předpis	k1gInPc2	předpis
==	==	k?	==
</s>
</p>
<p>
<s>
Krajnice	krajnice	k1gFnSc1	krajnice
není	být	k5eNaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
k	k	k7c3	k
jízdě	jízda	k1gFnSc3	jízda
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
§	§	k?	§
11	[number]	k4	11
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
361	[number]	k4	361
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
smí	smět	k5eAaImIp3nS	smět
řidič	řidič	k1gMnSc1	řidič
motorového	motorový	k2eAgNnSc2d1	motorové
vozidla	vozidlo	k1gNnSc2	vozidlo
vjet	vjet	k5eAaPmF	vjet
na	na	k7c6	na
krajnici	krajnice	k1gFnSc6	krajnice
jen	jen	k9	jen
při	při	k7c6	při
zastavení	zastavení	k1gNnSc6	zastavení
a	a	k8xC	a
stání	stání	k1gNnSc6	stání
nebo	nebo	k8xC	nebo
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
při	při	k7c6	při
objíždění	objíždění	k1gNnSc6	objíždění
<g/>
,	,	kIx,	,
vyhýbání	vyhýbání	k1gNnSc6	vyhýbání
<g/>
,	,	kIx,	,
odbočování	odbočování	k1gNnSc6	odbočování
nebo	nebo	k8xC	nebo
otáčení	otáčení	k1gNnSc6	otáčení
<g/>
;	;	kIx,	;
přitom	přitom	k6eAd1	přitom
musí	muset	k5eAaImIp3nS	muset
dbát	dbát	k5eAaImF	dbát
zvýšené	zvýšený	k2eAgFnSc3d1	zvýšená
opatrnosti	opatrnost	k1gFnSc3	opatrnost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
kolony	kolona	k1gFnSc2	kolona
na	na	k7c6	na
dálnici	dálnice	k1gFnSc6	dálnice
nebo	nebo	k8xC	nebo
silnici	silnice	k1gFnSc6	silnice
pro	pro	k7c4	pro
motorová	motorový	k2eAgNnPc4d1	motorové
vozidla	vozidlo	k1gNnPc4	vozidlo
mohou	moct	k5eAaImIp3nP	moct
řidiči	řidič	k1gMnPc1	řidič
z	z	k7c2	z
krajního	krajní	k2eAgInSc2d1	krajní
jízdního	jízdní	k2eAgInSc2d1	jízdní
pruhu	pruh	k1gInSc2	pruh
vjet	vjet	k5eAaPmF	vjet
na	na	k7c4	na
krajnici	krajnice	k1gFnSc4	krajnice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
to	ten	k3xDgNnSc1	ten
nutné	nutný	k2eAgNnSc1d1	nutné
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
průjezdného	průjezdný	k2eAgInSc2d1	průjezdný
pruhu	pruh	k1gInSc2	pruh
pro	pro	k7c4	pro
vozidla	vozidlo	k1gNnPc4	vozidlo
s	s	k7c7	s
právem	právo	k1gNnSc7	právo
přednostní	přednostní	k2eAgFnSc2d1	přednostní
jízdy	jízda	k1gFnSc2	jízda
(	(	kIx(	(
<g/>
§	§	k?	§
41	[number]	k4	41
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
8	[number]	k4	8
zák	zák	k?	zák
<g/>
.	.	kIx.	.
361	[number]	k4	361
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
pozemní	pozemní	k2eAgFnSc1d1	pozemní
komunikace	komunikace	k1gFnSc1	komunikace
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
chodník	chodník	k1gInSc4	chodník
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
chodník	chodník	k1gInSc1	chodník
neschůdný	schůdný	k2eNgInSc1d1	neschůdný
<g/>
,	,	kIx,	,
smí	smět	k5eAaImIp3nS	smět
chodec	chodec	k1gMnSc1	chodec
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
chůzi	chůze	k1gFnSc3	chůze
levé	levý	k2eAgFnSc2d1	levá
krajnice	krajnice	k1gFnSc2	krajnice
(	(	kIx(	(
<g/>
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
možné	možný	k2eAgNnSc4d1	možné
ani	ani	k8xC	ani
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
levého	levý	k2eAgInSc2d1	levý
okraje	okraj	k1gInSc2	okraj
vozovky	vozovka	k1gFnSc2	vozovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
chodce	chodec	k1gMnSc4	chodec
táhnoucího	táhnoucí	k2eAgMnSc4d1	táhnoucí
nebo	nebo	k8xC	nebo
tlačícího	tlačící	k2eAgMnSc2d1	tlačící
dětský	dětský	k2eAgInSc4d1	dětský
kočárek	kočárek	k1gInSc4	kočárek
<g/>
,	,	kIx,	,
sáňky	sáňky	k1gFnPc4	sáňky
<g/>
,	,	kIx,	,
vedoucího	vedoucí	k2eAgMnSc4d1	vedoucí
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
osobu	osoba	k1gFnSc4	osoba
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
nebo	nebo	k8xC	nebo
kolečkových	kolečkový	k2eAgFnPc6d1	kolečková
bruslích	brusle	k1gFnPc6	brusle
atd.	atd.	kA	atd.
Chodec	chodec	k1gMnSc1	chodec
nesoucí	nesoucí	k2eAgMnSc1d1	nesoucí
větší	veliký	k2eAgFnSc7d2	veliký
předmět	předmět	k1gInSc4	předmět
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
na	na	k7c6	na
chodníku	chodník	k1gInSc6	chodník
ohrožovat	ohrožovat	k5eAaImF	ohrožovat
ostatní	ostatní	k2eAgMnPc4d1	ostatní
chodce	chodec	k1gMnPc4	chodec
<g/>
,	,	kIx,	,
a	a	k8xC	a
kde	kde	k6eAd1	kde
chybí	chybět	k5eAaImIp3nS	chybět
chodník	chodník	k1gInSc1	chodník
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
osoba	osoba	k1gFnSc1	osoba
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
jízdní	jízdní	k2eAgNnSc4d1	jízdní
kolo	kolo	k1gNnSc4	kolo
<g/>
,	,	kIx,	,
koloběžku	koloběžka	k1gFnSc4	koloběžka
<g/>
,	,	kIx,	,
moped	moped	k1gInSc4	moped
<g/>
,	,	kIx,	,
ruční	ruční	k2eAgInSc1d1	ruční
vozík	vozík	k1gInSc1	vozík
<g/>
,	,	kIx,	,
osoba	osoba	k1gFnSc1	osoba
na	na	k7c6	na
ručním	ruční	k2eAgInSc6d1	ruční
nebo	nebo	k8xC	nebo
motorovém	motorový	k2eAgInSc6d1	motorový
vozíku	vozík	k1gInSc6	vozík
pro	pro	k7c4	pro
invalidy	invalid	k1gMnPc4	invalid
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
jít	jít	k5eAaImF	jít
po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
krajnici	krajnice	k1gFnSc6	krajnice
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ručním	ruční	k2eAgInSc7d1	ruční
vozíkem	vozík	k1gInSc7	vozík
do	do	k7c2	do
60	[number]	k4	60
cm	cm	kA	cm
šířky	šířka	k1gFnSc2	šířka
se	se	k3xPyFc4	se
v	v	k7c6	v
takových	takový	k3xDgInPc6	takový
případech	případ	k1gInPc6	případ
chodí	chodit	k5eAaImIp3nP	chodit
po	po	k7c6	po
levé	levý	k2eAgFnSc6d1	levá
krajnici	krajnice	k1gFnSc6	krajnice
<g/>
,	,	kIx,	,
s	s	k7c7	s
vozíkem	vozík	k1gInSc7	vozík
nad	nad	k7c4	nad
60	[number]	k4	60
cm	cm	kA	cm
šířky	šířka	k1gFnSc2	šířka
po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
krajnici	krajnice	k1gFnSc6	krajnice
<g/>
.	.	kIx.	.
</s>
<s>
Krajnici	krajnice	k1gFnSc4	krajnice
však	však	k9	však
k	k	k7c3	k
chůzi	chůze	k1gFnSc3	chůze
nesmí	smět	k5eNaImIp3nS	smět
použít	použít	k5eAaPmF	použít
tzv.	tzv.	kA	tzv.
organizovaný	organizovaný	k2eAgInSc4d1	organizovaný
útvar	útvar	k1gInSc4	útvar
chodců	chodec	k1gMnPc2	chodec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k6eAd1	pokud
tím	ten	k3xDgNnSc7	ten
nejsou	být	k5eNaImIp3nP	být
ohrožováni	ohrožován	k2eAgMnPc1d1	ohrožován
ani	ani	k8xC	ani
omezováni	omezován	k2eAgMnPc1d1	omezován
chodci	chodec	k1gMnPc1	chodec
<g/>
,	,	kIx,	,
smí	smět	k5eAaImIp3nS	smět
po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
krajnici	krajnice	k1gFnSc6	krajnice
jet	jet	k2eAgMnSc1d1	jet
cyklisté	cyklista	k1gMnPc1	cyklista
a	a	k8xC	a
jezdci	jezdec	k1gMnPc1	jezdec
na	na	k7c6	na
zvířeti	zvíře	k1gNnSc6	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krajnici	krajnice	k1gFnSc6	krajnice
mohou	moct	k5eAaImIp3nP	moct
cyklisté	cyklista	k1gMnPc1	cyklista
též	též	k9	též
zprava	zprava	k6eAd1	zprava
předjíždět	předjíždět	k5eAaImF	předjíždět
nebo	nebo	k8xC	nebo
objíždět	objíždět	k5eAaImF	objíždět
kolonu	kolona	k1gFnSc4	kolona
stojících	stojící	k2eAgFnPc2d1	stojící
nebo	nebo	k8xC	nebo
pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
pohybujících	pohybující	k2eAgNnPc2d1	pohybující
vozidel	vozidlo	k1gNnPc2	vozidlo
při	při	k7c6	při
pravém	pravý	k2eAgInSc6d1	pravý
okraji	okraj	k1gInSc6	okraj
vozovky	vozovka	k1gFnSc2	vozovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
