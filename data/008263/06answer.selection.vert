<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1789	[number]	k4	1789
začal	začít	k5eAaPmAgInS	začít
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
Ženský	ženský	k2eAgInSc1d1	ženský
pochod	pochod	k1gInSc1	pochod
na	na	k7c6	na
Versailles	Versailles	k1gFnSc6	Versailles
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
během	během	k7c2	během
dne	den	k1gInSc2	den
propukaly	propukat	k5eAaImAgInP	propukat
nepokoje	nepokoj	k1gInPc1	nepokoj
mezi	mezi	k7c7	mezi
trhovkyněmi	trhovkyně	k1gFnPc7	trhovkyně
na	na	k7c6	na
tržištích	tržiště	k1gNnPc6	tržiště
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
kvůli	kvůli	k7c3	kvůli
vysokým	vysoký	k2eAgFnPc3d1	vysoká
cenám	cena	k1gFnPc3	cena
a	a	k8xC	a
nedostatku	nedostatek	k1gInSc3	nedostatek
chleba	chléb	k1gInSc2	chléb
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
náladě	nálada	k1gFnSc6	nálada
jedna	jeden	k4xCgFnSc1	jeden
mladá	mladý	k2eAgFnSc1d1	mladá
žena	žena	k1gFnSc1	žena
na	na	k7c6	na
tržišti	tržiště	k1gNnSc6	tržiště
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Paříži	Paříž	k1gFnSc6	Paříž
začala	začít	k5eAaPmAgFnS	začít
bubnovat	bubnovat	k5eAaImF	bubnovat
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
si	se	k3xPyFc3	se
dav	dav	k1gInSc1	dav
žen	žena	k1gFnPc2	žena
vynutil	vynutit	k5eAaPmAgInS	vynutit
rozeznění	rozeznění	k1gNnSc4	rozeznění
zvonů	zvon	k1gInPc2	zvon
v	v	k7c6	v
nejbližším	blízký	k2eAgInSc6d3	Nejbližší
kostele	kostel	k1gInSc6	kostel
<g/>
.	.	kIx.	.
</s>
