<s>
Abarth	Abarth	k1gInSc1	Abarth
&	&	k?	&
Co	co	k3yQnSc4	co
je	být	k5eAaImIp3nS	být
italský	italský	k2eAgMnSc1d1	italský
tvůrce	tvůrce	k1gMnSc1	tvůrce
závodních	závodní	k2eAgInPc2d1	závodní
vozů	vůz	k1gInPc2	vůz
založený	založený	k2eAgInSc4d1	založený
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
žijícím	žijící	k2eAgMnSc7d1	žijící
rakušanem	rakušan	k1gMnSc7	rakušan
Carlem	Carl	k1gMnSc7	Carl
Abarthem	Abarth	k1gInSc7	Abarth
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
firemním	firemní	k2eAgInSc6d1	firemní
znaku	znak	k1gInSc6	znak
byl	být	k5eAaImAgMnS	být
štír	štír	k1gMnSc1	štír
<g/>
,	,	kIx,	,
znamení	znamení	k1gNnSc1	znamení
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
Carlo	Carlo	k1gNnSc1	Carlo
Abarth	Abartha	k1gFnPc2	Abartha
narodil	narodit	k5eAaPmAgInS	narodit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
Abarth	Abarth	k1gMnSc1	Abarth
velmi	velmi	k6eAd1	velmi
úspěšným	úspěšný	k2eAgInPc3d1	úspěšný
v	v	k7c6	v
závodech	závod	k1gInPc6	závod
do	do	k7c2	do
vrchu	vrch	k1gInSc2	vrch
a	a	k8xC	a
závodech	závod	k1gInPc6	závod
sportovních	sportovní	k2eAgInPc2d1	sportovní
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
třídách	třída	k1gFnPc6	třída
od	od	k7c2	od
850	[number]	k4	850
cm3	cm3	k4	cm3
do	do	k7c2	do
2	[number]	k4	2
000	[number]	k4	000
cm3	cm3	k4	cm3
<g/>
,	,	kIx,	,
soupeřící	soupeřící	k2eAgInPc4d1	soupeřící
s	s	k7c7	s
vozy	vůz	k1gInPc7	vůz
Porsche	Porsche	k1gNnSc2	Porsche
904	[number]	k4	904
a	a	k8xC	a
Ferrari	Ferrari	k1gMnSc1	Ferrari
Dino	Dino	k1gMnSc1	Dino
<g/>
.	.	kIx.	.
</s>
<s>
Hans	Hans	k1gMnSc1	Hans
Herrmann	Herrmann	k1gMnSc1	Herrmann
byl	být	k5eAaImAgMnS	být
továrním	tovární	k2eAgMnSc7d1	tovární
jezdcem	jezdec	k1gMnSc7	jezdec
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
do	do	k7c2	do
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
500	[number]	k4	500
km	km	kA	km
na	na	k7c6	na
Nürburgringu	Nürburgring	k1gInSc6	Nürburgring
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
s	s	k7c7	s
Teddym	Teddym	k1gInSc1	Teddym
Pilettem	Pilett	k1gInSc7	Pilett
<g/>
.	.	kIx.	.
</s>
<s>
Johann	Johann	k1gMnSc1	Johann
Abt	Abt	k1gMnSc1	Abt
měl	mít	k5eAaImAgMnS	mít
později	pozdě	k6eAd2	pozdě
slíbeno	slíbit	k5eAaPmNgNnS	slíbit
od	od	k7c2	od
Carla	Carlo	k1gNnSc2	Carlo
Abbartha	Abbarth	k1gMnSc2	Abbarth
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
řídit	řídit	k5eAaImF	řídit
tovární	tovární	k2eAgNnPc4d1	tovární
auta	auto	k1gNnPc4	auto
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
všechny	všechen	k3xTgInPc4	všechen
závody	závod	k1gInPc4	závod
do	do	k7c2	do
kterých	který	k3yRgFnPc2	který
odstartuje	odstartovat	k5eAaPmIp3nS	odstartovat
–	–	k?	–
což	což	k9	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
téměř	téměř	k6eAd1	téměř
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
29	[number]	k4	29
ze	z	k7c2	z
30	[number]	k4	30
závodů	závod	k1gInPc2	závod
<g/>
,	,	kIx,	,
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
třicátém	třicátý	k4xOgNnSc6	třicátý
byl	být	k5eAaImAgInS	být
druhý	druhý	k4xOgMnSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
závodních	závodní	k2eAgInPc2d1	závodní
vozů	vůz	k1gInPc2	vůz
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
Abarth	Abartha	k1gFnPc2	Abartha
výfuky	výfuk	k1gInPc1	výfuk
pro	pro	k7c4	pro
vysoce	vysoce	k6eAd1	vysoce
výkonná	výkonný	k2eAgNnPc4d1	výkonné
auta	auto	k1gNnPc4	auto
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
různorodé	různorodý	k2eAgInPc4d1	různorodý
tuningové	tuningový	k2eAgInPc4d1	tuningový
kity	kit	k1gInPc4	kit
pro	pro	k7c4	pro
silniční	silniční	k2eAgNnPc4d1	silniční
vozidla	vozidlo	k1gNnPc4	vozidlo
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
Fiat	fiat	k1gInSc4	fiat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
byl	být	k5eAaImAgInS	být
zapojen	zapojit	k5eAaPmNgMnS	zapojit
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
sportovních	sportovní	k2eAgInPc2d1	sportovní
či	či	k8xC	či
závodních	závodní	k2eAgInPc2d1	závodní
vozů	vůz	k1gInPc2	vůz
pro	pro	k7c4	pro
Porsche	Porsche	k1gNnSc4	Porsche
a	a	k8xC	a
Simcu	Simca	k1gFnSc4	Simca
<g/>
.	.	kIx.	.
</s>
<s>
Výrobní	výrobní	k2eAgFnSc1d1	výrobní
část	část	k1gFnSc1	část
Abarthu	Abarth	k1gInSc2	Abarth
byla	být	k5eAaImAgFnS	být
prodána	prodán	k2eAgFnSc1d1	prodána
Fiatu	fiat	k1gInSc2	fiat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
a	a	k8xC	a
závodní	závodní	k2eAgInSc1d1	závodní
tým	tým	k1gInSc1	tým
byl	být	k5eAaImAgInS	být
prodán	prodat	k5eAaPmNgInS	prodat
Enzovi	Enza	k1gMnSc3	Enza
Osselovi	Ossel	k1gMnSc3	Ossel
<g/>
,	,	kIx,	,
zakladateli	zakladatel	k1gMnSc3	zakladatel
týmu	tým	k1gInSc3	tým
Ossela	Ossel	k1gMnSc2	Ossel
<g/>
.	.	kIx.	.
</s>
<s>
Abarth	Abarth	k1gInSc1	Abarth
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
závodním	závodní	k2eAgNnSc7d1	závodní
oddělením	oddělení	k1gNnSc7	oddělení
Fiatu	fiat	k1gInSc2	fiat
<g/>
,	,	kIx,	,
vedeným	vedený	k2eAgMnSc7d1	vedený
slavným	slavný	k2eAgMnSc7d1	slavný
návrhářem	návrhář	k1gMnSc7	návrhář
motorů	motor	k1gInPc2	motor
Aureliem	Aurelium	k1gNnSc7	Aurelium
Lampredi	Lampred	k1gMnPc1	Lampred
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
modely	model	k1gInPc1	model
Fiatu	fiat	k1gInSc2	fiat
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnPc2	jeho
dceřiných	dceřin	k2eAgFnPc2d1	dceřina
společností	společnost	k1gFnPc2	společnost
Lancie	Lancia	k1gFnSc2	Lancia
a	a	k8xC	a
Autobianchi	Autobianchi	k1gNnSc2	Autobianchi
byly	být	k5eAaImAgInP	být
přeoznačeny	přeoznačen	k2eAgInPc1d1	přeoznačen
na	na	k7c4	na
Abarth	Abarth	k1gInSc4	Abarth
<g/>
,	,	kIx,	,
nejpopulárnějším	populární	k2eAgMnPc3d3	nejpopulárnější
byl	být	k5eAaImAgMnS	být
Autobianchi	Autobianch	k1gFnPc4	Autobianch
A112	A112	k1gFnSc2	A112
Abarth	Abartha	k1gFnPc2	Abartha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
Fiat	fiat	k1gInSc1	fiat
Automobiles	Automobiles	k1gInSc4	Automobiles
SpA	SpA	k1gFnSc2	SpA
oživil	oživit	k5eAaPmAgMnS	oživit
značku	značka	k1gFnSc4	značka
Abarth	Abarth	k1gMnSc1	Abarth
Grandem	grand	k1gMnSc7	grand
Punto	punto	k1gNnSc1	punto
Abarth	Abartha	k1gFnPc2	Abartha
a	a	k8xC	a
Grandem	grand	k1gMnSc7	grand
Punto	punto	k1gNnSc4	punto
Abarth	Abarth	k1gInSc1	Abarth
S	s	k7c7	s
<g/>
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
značka	značka	k1gFnSc1	značka
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
oddělenou	oddělený	k2eAgFnSc7d1	oddělená
divizí	divize	k1gFnSc7	divize
hlavní	hlavní	k2eAgFnSc2d1	hlavní
značky	značka	k1gFnSc2	značka
Fiat	fiat	k1gInSc1	fiat
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
jít	jít	k5eAaImF	jít
svou	svůj	k3xOyFgFnSc7	svůj
vlastní	vlastní	k2eAgFnSc7d1	vlastní
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Renaultsport	Renaultsport	k1gInSc1	Renaultsport
vedle	vedle	k7c2	vedle
tradičního	tradiční	k2eAgInSc2d1	tradiční
Renaultu	renault	k1gInSc2	renault
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Abarth	Abartha	k1gFnPc2	Abartha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
</s>
