<s>
Duševní	duševní	k2eAgFnSc1d1	duševní
porucha	porucha	k1gFnSc1	porucha
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
psychická	psychický	k2eAgFnSc1d1	psychická
porucha	porucha	k1gFnSc1	porucha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
pojetí	pojetí	k1gNnSc6	pojetí
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
psychické	psychický	k2eAgInPc4d1	psychický
procesy	proces	k1gInPc4	proces
<g/>
,	,	kIx,	,
projevující	projevující	k2eAgInPc4d1	projevující
se	se	k3xPyFc4	se
v	v	k7c6	v
myšlení	myšlení	k1gNnSc6	myšlení
<g/>
,	,	kIx,	,
prožívání	prožívání	k1gNnSc6	prožívání
a	a	k8xC	a
chování	chování	k1gNnSc6	chování
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
znesnadňující	znesnadňující	k2eAgFnSc1d1	znesnadňující
jeho	jeho	k3xOp3gNnSc4	jeho
fungování	fungování	k1gNnSc4	fungování
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Definic	definice	k1gFnPc2	definice
duševní	duševní	k2eAgFnSc2d1	duševní
poruchy	porucha	k1gFnSc2	porucha
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
duševní	duševní	k2eAgFnPc4d1	duševní
poruchy	porucha	k1gFnPc4	porucha
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
ty	ten	k3xDgFnPc4	ten
z	z	k7c2	z
poruch	porucha	k1gFnPc2	porucha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
prvotně	prvotně	k6eAd1	prvotně
týkají	týkat	k5eAaImIp3nP	týkat
myšlení	myšlení	k1gNnSc3	myšlení
<g/>
,	,	kIx,	,
prožívání	prožívání	k1gNnSc3	prožívání
nebo	nebo	k8xC	nebo
vztahů	vztah	k1gInPc2	vztah
k	k	k7c3	k
ostatním	ostatní	k2eAgMnPc3d1	ostatní
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
v	v	k7c6	v
obdobném	obdobný	k2eAgInSc6d1	obdobný
významu	význam	k1gInSc6	význam
užíval	užívat	k5eAaImAgInS	užívat
například	například	k6eAd1	například
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
šílenství	šílenství	k1gNnSc1	šílenství
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Duševní	duševní	k2eAgNnSc1d1	duševní
zdraví	zdraví	k1gNnSc1	zdraví
a	a	k8xC	a
duševní	duševní	k2eAgFnSc1d1	duševní
porucha	porucha	k1gFnSc1	porucha
jsou	být	k5eAaImIp3nP	být
pojmy	pojem	k1gInPc1	pojem
značně	značně	k6eAd1	značně
kulturně	kulturně	k6eAd1	kulturně
i	i	k9	i
názorově	názorově	k6eAd1	názorově
podmíněné	podmíněný	k2eAgNnSc1d1	podmíněné
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
obsah	obsah	k1gInSc1	obsah
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nepřesný	přesný	k2eNgInSc1d1	nepřesný
<g/>
,	,	kIx,	,
proměnlivý	proměnlivý	k2eAgInSc1d1	proměnlivý
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
obecné	obecný	k2eAgFnSc2d1	obecná
shody	shoda	k1gFnSc2	shoda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
duševní	duševní	k2eAgFnPc4d1	duševní
poruchy	porucha	k1gFnPc4	porucha
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jak	jak	k6eAd1	jak
stavy	stav	k1gInPc4	stav
podmíněné	podmíněný	k2eAgInPc4d1	podmíněný
zejména	zejména	k9	zejména
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
(	(	kIx(	(
<g/>
vrozenou	vrozený	k2eAgFnSc4d1	vrozená
<g/>
)	)	kIx)	)
organickou	organický	k2eAgFnSc4d1	organická
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
neurologickou	neurologický	k2eAgFnSc4d1	neurologická
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
též	též	k9	též
genetickou	genetický	k2eAgFnSc7d1	genetická
<g/>
)	)	kIx)	)
specifickou	specifický	k2eAgFnSc7d1	specifická
výbavou	výbava	k1gFnSc7	výbava
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
tak	tak	k9	tak
stavy	stav	k1gInPc7	stav
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
za	za	k7c4	za
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
příčinu	příčina	k1gFnSc4	příčina
vzniku	vznik	k1gInSc2	vznik
pokládá	pokládat	k5eAaImIp3nS	pokládat
vliv	vliv	k1gInSc1	vliv
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
životních	životní	k2eAgFnPc2d1	životní
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
a	a	k8xC	a
vztah	vztah	k1gInSc1	vztah
vrozených	vrozený	k2eAgFnPc2d1	vrozená
dispozic	dispozice	k1gFnPc2	dispozice
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
vlivů	vliv	k1gInPc2	vliv
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
typů	typ	k1gInPc2	typ
poruch	porucha	k1gFnPc2	porucha
jsou	být	k5eAaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
zkoumání	zkoumání	k1gNnSc2	zkoumání
a	a	k8xC	a
diskusí	diskuse	k1gFnPc2	diskuse
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
může	moct	k5eAaImIp3nS	moct
spočívat	spočívat	k5eAaImF	spočívat
buď	buď	k8xC	buď
v	v	k7c6	v
odstraňování	odstraňování	k1gNnSc6	odstraňování
příčin	příčina	k1gFnPc2	příčina
poruchy	porucha	k1gFnSc2	porucha
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
úpravě	úprava	k1gFnSc6	úprava
jejích	její	k3xOp3gInPc2	její
následků	následek	k1gInPc2	následek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
medikamenty	medikament	k1gInPc1	medikament
<g/>
,	,	kIx,	,
psychoterapie	psychoterapie	k1gFnPc1	psychoterapie
včetně	včetně	k7c2	včetně
úpravy	úprava	k1gFnSc2	úprava
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
způsoby	způsob	k1gInPc4	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
duševní	duševní	k2eAgFnSc2d1	duševní
poruchy	porucha	k1gFnSc2	porucha
a	a	k8xC	a
obdobné	obdobný	k2eAgInPc1d1	obdobný
pojmy	pojem	k1gInPc1	pojem
z	z	k7c2	z
dřívějška	dřívějšek	k1gInSc2	dřívějšek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
duševní	duševní	k2eAgFnSc4d1	duševní
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
podmíněny	podmínit	k5eAaPmNgFnP	podmínit
dobovou	dobový	k2eAgFnSc7d1	dobová
a	a	k8xC	a
místní	místní	k2eAgFnSc7d1	místní
kulturou	kultura	k1gFnSc7	kultura
<g/>
,	,	kIx,	,
osobními	osobní	k2eAgInPc7d1	osobní
názory	názor	k1gInPc7	názor
i	i	k8xC	i
sociální	sociální	k2eAgFnSc7d1	sociální
rolí	role	k1gFnSc7	role
zdravotnického	zdravotnický	k2eAgInSc2d1	zdravotnický
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
homosexualita	homosexualita	k1gFnSc1	homosexualita
nebo	nebo	k8xC	nebo
leváctví	leváctví	k1gNnSc1	leváctví
byly	být	k5eAaImAgFnP	být
ještě	ještě	k6eAd1	ještě
nedávno	nedávno	k6eAd1	nedávno
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
poruchy	porucha	k1gFnPc4	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
způsobu	způsob	k1gInSc3	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
hlavní	hlavní	k2eAgInSc1d1	hlavní
proud	proud	k1gInSc1	proud
psychiatrie	psychiatrie	k1gFnSc2	psychiatrie
pojímá	pojímat	k5eAaImIp3nS	pojímat
duševní	duševní	k2eAgFnSc2d1	duševní
poruchy	porucha	k1gFnSc2	porucha
<g/>
,	,	kIx,	,
argumentovali	argumentovat	k5eAaImAgMnP	argumentovat
zejména	zejména	k9	zejména
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc6	století
lidé	člověk	k1gMnPc1	člověk
řazení	řazení	k1gNnSc2	řazení
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
antipsychiatrického	antipsychiatrický	k2eAgNnSc2d1	antipsychiatrické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
jejich	jejich	k3xOp3gInPc2	jejich
pohledů	pohled	k1gInPc2	pohled
na	na	k7c4	na
psychologický	psychologický	k2eAgInSc4d1	psychologický
a	a	k8xC	a
sociologický	sociologický	k2eAgInSc4d1	sociologický
rámec	rámec	k1gInSc4	rámec
duševních	duševní	k2eAgFnPc2d1	duševní
poruch	porucha	k1gFnPc2	porucha
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
významnou	významný	k2eAgFnSc7d1	významná
částí	část	k1gFnSc7	část
psychiatrické	psychiatrický	k2eAgFnPc1d1	psychiatrická
obce	obec	k1gFnPc1	obec
přijímány	přijímán	k2eAgFnPc1d1	přijímána
a	a	k8xC	a
odrazily	odrazit	k5eAaPmAgFnP	odrazit
se	se	k3xPyFc4	se
v	v	k7c6	v
novějších	nový	k2eAgInPc6d2	novější
klasifikačních	klasifikační	k2eAgInPc6d1	klasifikační
manuálech	manuál	k1gInPc6	manuál
<g/>
,	,	kIx,	,
léčebné	léčebný	k2eAgFnSc6d1	léčebná
praxi	praxe	k1gFnSc6	praxe
i	i	k8xC	i
odborných	odborný	k2eAgFnPc6d1	odborná
diskusích	diskuse	k1gFnPc6	diskuse
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
klasifikacích	klasifikace	k1gFnPc6	klasifikace
rozlišováno	rozlišován	k2eAgNnSc4d1	rozlišováno
jen	jen	k6eAd1	jen
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
duševních	duševní	k2eAgFnPc2d1	duševní
poruch	porucha	k1gFnPc2	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
192	[number]	k4	192
a	a	k8xC	a
Diagnostický	diagnostický	k2eAgInSc1d1	diagnostický
a	a	k8xC	a
statistický	statistický	k2eAgInSc1d1	statistický
manuál	manuál	k1gInSc1	manuál
duševních	duševní	k2eAgFnPc2d1	duševní
poruch	porucha	k1gFnPc2	porucha
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
edice	edice	k1gFnSc1	edice
(	(	kIx(	(
<g/>
DSM-IV	DSM-IV	k1gMnPc2	DSM-IV
<g/>
)	)	kIx)	)
jich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
uvádí	uvádět	k5eAaImIp3nS	uvádět
374	[number]	k4	374
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nárůst	nárůst	k1gInSc1	nárůst
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsoben	způsoben	k2eAgMnSc1d1	způsoben
<g/>
:	:	kIx,	:
efektivnější	efektivní	k2eAgFnSc1d2	efektivnější
diagnostikou	diagnostika	k1gFnSc7	diagnostika
a	a	k8xC	a
lepší	dobrý	k2eAgFnSc7d2	lepší
charakteristikou	charakteristika	k1gFnSc7	charakteristika
duševních	duševní	k2eAgFnPc2d1	duševní
poruch	porucha	k1gFnPc2	porucha
<g/>
,	,	kIx,	,
díky	dík	k1gInPc7	dík
více	hodně	k6eAd2	hodně
než	než	k8xS	než
století	století	k1gNnSc1	století
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
;	;	kIx,	;
skutečně	skutečně	k6eAd1	skutečně
se	s	k7c7	s
zvyšujícím	zvyšující	k2eAgInSc7d1	zvyšující
počtem	počet	k1gInSc7	počet
duševních	duševní	k2eAgFnPc2d1	duševní
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
způsobeným	způsobený	k2eAgInSc7d1	způsobený
hnacími	hnací	k2eAgInPc7d1	hnací
faktory	faktor	k1gInPc7	faktor
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
dieta	dieta	k1gFnSc1	dieta
nebo	nebo	k8xC	nebo
stále	stále	k6eAd1	stále
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
stres	stres	k1gInSc4	stres
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
<g/>
;	;	kIx,	;
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
tendencí	tendence	k1gFnSc7	tendence
některých	některý	k3yIgMnPc2	některý
psychiatrů	psychiatr	k1gMnPc2	psychiatr
označovat	označovat	k5eAaImF	označovat
individuální	individuální	k2eAgFnPc4d1	individuální
zvláštnosti	zvláštnost	k1gFnPc4	zvláštnost
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
slabůstky	slabůstka	k1gFnSc2	slabůstka
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
jako	jako	k9	jako
nemoc	nemoc	k1gFnSc1	nemoc
Diagnóza	diagnóza	k1gFnSc1	diagnóza
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
na	na	k7c6	na
základě	základ	k1gInSc6	základ
příznaků	příznak	k1gInPc2	příznak
duševních	duševní	k2eAgFnPc2d1	duševní
poruch	porucha	k1gFnPc2	porucha
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
slovních	slovní	k2eAgInPc2d1	slovní
projevů	projev	k1gInPc2	projev
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
někdy	někdy	k6eAd1	někdy
i	i	k9	i
na	na	k7c6	na
základě	základ	k1gInSc6	základ
fyzického	fyzický	k2eAgNnSc2d1	fyzické
vyšetření	vyšetření	k1gNnSc2	vyšetření
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
drogových	drogový	k2eAgFnPc2d1	drogová
závislostí	závislost	k1gFnPc2	závislost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
byly	být	k5eAaImAgFnP	být
duševní	duševní	k2eAgFnPc1d1	duševní
poruchy	porucha	k1gFnPc1	porucha
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
podle	podle	k7c2	podle
společných	společný	k2eAgInPc2d1	společný
symptomů	symptom	k1gInPc2	symptom
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Diagnostickém	diagnostický	k2eAgInSc6d1	diagnostický
a	a	k8xC	a
statistickém	statistický	k2eAgInSc6d1	statistický
manuálu	manuál	k1gInSc6	manuál
duševních	duševní	k2eAgFnPc2d1	duševní
poruch	porucha	k1gFnPc2	porucha
(	(	kIx(	(
<g/>
DSM	DSM	kA	DSM
<g/>
)	)	kIx)	)
vytvořeném	vytvořený	k2eAgInSc6d1	vytvořený
Americkou	americký	k2eAgFnSc7d1	americká
psychiatrickou	psychiatrický	k2eAgFnSc7d1	psychiatrická
asociací	asociace	k1gFnSc7	asociace
<g/>
.	.	kIx.	.
</s>
<s>
Manuál	manuál	k1gInSc1	manuál
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
13	[number]	k4	13
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Americkým	americký	k2eAgInSc7d1	americký
manuálem	manuál	k1gInSc7	manuál
DSM	DSM	kA	DSM
byl	být	k5eAaImAgInS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
i	i	k9	i
V.	V.	kA	V.
díl	díl	k1gInSc1	díl
(	(	kIx(	(
<g/>
diagnózy	diagnóza	k1gFnSc2	diagnóza
řady	řada	k1gFnSc2	řada
F	F	kA	F
<g/>
)	)	kIx)	)
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
klasifikace	klasifikace	k1gFnSc1	klasifikace
nemocí	nemoc	k1gFnPc2	nemoc
(	(	kIx(	(
<g/>
MKN	MKN	kA	MKN
<g/>
,	,	kIx,	,
ICD	ICD	kA	ICD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vydává	vydávat	k5eAaImIp3nS	vydávat
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
DSM	DSM	kA	DSM
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
kategorizovaných	kategorizovaný	k2eAgFnPc2d1	kategorizovaná
duševních	duševní	k2eAgFnPc2d1	duševní
poruch	porucha	k1gFnPc2	porucha
definováno	definovat	k5eAaBmNgNnS	definovat
i	i	k8xC	i
mnoho	mnoho	k4c4	mnoho
jejich	jejich	k3xOp3gInPc2	jejich
příznaků	příznak	k1gInPc2	příznak
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
paranoia	paranoia	k1gFnSc1	paranoia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
nejsou	být	k5eNaImIp3nP	být
samy	sám	k3xTgFnPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
za	za	k7c4	za
indikátory	indikátor	k1gInPc1	indikátor
některé	některý	k3yIgMnPc4	některý
z	z	k7c2	z
výše	výše	k1gFnSc2	výše
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
poruch	porucha	k1gFnPc2	porucha
<g/>
.	.	kIx.	.
</s>
<s>
MKN	MKN	kA	MKN
mezi	mezi	k7c7	mezi
poruchami	porucha	k1gFnPc7	porucha
a	a	k8xC	a
příznaky	příznak	k1gInPc7	příznak
takto	takto	k6eAd1	takto
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
většina	většina	k1gFnSc1	většina
diagnostických	diagnostický	k2eAgFnPc2d1	diagnostická
položek	položka	k1gFnPc2	položka
je	být	k5eAaImIp3nS	být
vytvářena	vytvářet	k5eAaImNgFnS	vytvářet
především	především	k9	především
na	na	k7c6	na
základě	základ	k1gInSc6	základ
příznaků	příznak	k1gInPc2	příznak
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
organické	organický	k2eAgFnPc4d1	organická
příčiny	příčina	k1gFnPc4	příčina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
platná	platný	k2eAgFnSc1d1	platná
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
klasifikace	klasifikace	k1gFnSc1	klasifikace
nemocí	nemoc	k1gFnPc2	nemoc
MKN	MKN	kA	MKN
10	[number]	k4	10
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Prochází	procházet	k5eAaImIp3nS	procházet
průběžnými	průběžný	k2eAgFnPc7d1	průběžná
aktualizacemi	aktualizace	k1gFnPc7	aktualizace
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc1d1	poslední
aktualizace	aktualizace	k1gFnSc1	aktualizace
je	být	k5eAaImIp3nS	být
platná	platný	k2eAgFnSc1d1	platná
od	od	k7c2	od
1.1	[number]	k4	1.1
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapitole	kapitola	k1gFnSc6	kapitola
Poruchy	porucha	k1gFnSc2	porucha
duševní	duševní	k2eAgFnSc2d1	duševní
a	a	k8xC	a
poruchy	porucha	k1gFnSc2	porucha
chování	chování	k1gNnSc2	chování
rozpoznává	rozpoznávat	k5eAaImIp3nS	rozpoznávat
tyto	tento	k3xDgFnPc4	tento
kategorie	kategorie	k1gFnPc4	kategorie
zvané	zvaný	k2eAgFnPc4d1	zvaná
oddíly	oddíl	k1gInPc4	oddíl
<g/>
:	:	kIx,	:
F	F	kA	F
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
F	F	kA	F
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
Organické	organický	k2eAgFnSc2d1	organická
duševní	duševní	k2eAgFnSc2d1	duševní
poruchy	porucha	k1gFnSc2	porucha
včetně	včetně	k7c2	včetně
symptomatických	symptomatický	k2eAgInPc2d1	symptomatický
F	F	kA	F
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
F	F	kA	F
<g/>
19	[number]	k4	19
Poruchy	porucha	k1gFnSc2	porucha
duševní	duševní	k2eAgFnSc2d1	duševní
a	a	k8xC	a
poruchy	porucha	k1gFnSc2	porucha
chování	chování	k1gNnSc2	chování
způsobené	způsobený	k2eAgNnSc4d1	způsobené
užíváním	užívání	k1gNnSc7	užívání
psychoaktivních	psychoaktivní	k2eAgFnPc2d1	psychoaktivní
látek	látka	k1gFnPc2	látka
<g />
.	.	kIx.	.
</s>
<s>
F	F	kA	F
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
F	F	kA	F
<g/>
29	[number]	k4	29
Schizofrenie	schizofrenie	k1gFnPc1	schizofrenie
<g/>
'	'	kIx"	'
poruchy	porucha	k1gFnPc1	porucha
schizotypální	schizotypálnit	k5eAaPmIp3nP	schizotypálnit
a	a	k8xC	a
poruchy	poruch	k1gInPc1	poruch
s	s	k7c7	s
bludy	blud	k1gInPc7	blud
F	F	kA	F
<g/>
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
F	F	kA	F
<g/>
39	[number]	k4	39
Afektivní	afektivní	k2eAgFnSc2d1	afektivní
poruchy	porucha	k1gFnSc2	porucha
(	(	kIx(	(
<g/>
poruchy	porucha	k1gFnSc2	porucha
nálady	nálada	k1gFnSc2	nálada
<g/>
)	)	kIx)	)
F	F	kA	F
<g/>
40	[number]	k4	40
<g/>
-	-	kIx~	-
<g/>
F	F	kA	F
<g/>
48	[number]	k4	48
Neurotické	neurotický	k2eAgFnSc2d1	neurotická
<g/>
'	'	kIx"	'
stresové	stresový	k2eAgFnSc2d1	stresová
a	a	k8xC	a
somatoformní	somatoformní	k2eAgFnSc2d1	somatoformní
<g />
.	.	kIx.	.
</s>
<s>
poruchy	poruch	k1gInPc1	poruch
F	F	kA	F
<g/>
50	[number]	k4	50
<g/>
-	-	kIx~	-
<g/>
F	F	kA	F
<g/>
59	[number]	k4	59
Syndromy	syndrom	k1gInPc1	syndrom
poruch	porucha	k1gFnPc2	porucha
chování	chování	k1gNnSc2	chování
<g/>
'	'	kIx"	'
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
fyziologickými	fyziologický	k2eAgFnPc7d1	fyziologická
poruchami	porucha	k1gFnPc7	porucha
a	a	k8xC	a
somatickými	somatický	k2eAgInPc7d1	somatický
faktory	faktor	k1gInPc7	faktor
F	F	kA	F
<g/>
60	[number]	k4	60
<g/>
-	-	kIx~	-
<g/>
F	F	kA	F
<g/>
69	[number]	k4	69
Poruchy	porucha	k1gFnSc2	porucha
osobnosti	osobnost	k1gFnSc2	osobnost
a	a	k8xC	a
chování	chování	k1gNnSc2	chování
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
F	F	kA	F
<g/>
70	[number]	k4	70
<g/>
-	-	kIx~	-
<g/>
F	F	kA	F
<g/>
79	[number]	k4	79
Mentální	mentální	k2eAgFnSc2d1	mentální
retardace	retardace	k1gFnSc2	retardace
F	F	kA	F
<g/>
<g />
.	.	kIx.	.
</s>
<s>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
F	F	kA	F
<g/>
89	[number]	k4	89
Poruchy	porucha	k1gFnPc4	porucha
psychického	psychický	k2eAgInSc2d1	psychický
vývoje	vývoj	k1gInSc2	vývoj
F	F	kA	F
<g/>
90	[number]	k4	90
<g/>
-	-	kIx~	-
<g/>
F	F	kA	F
<g/>
98	[number]	k4	98
Poruchy	porucha	k1gFnSc2	porucha
chování	chování	k1gNnPc2	chování
a	a	k8xC	a
emocí	emoce	k1gFnPc2	emoce
se	s	k7c7	s
začátkem	začátek	k1gInSc7	začátek
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
a	a	k8xC	a
v	v	k7c6	v
dospívání	dospívání	k1gNnSc6	dospívání
F99	F99	k1gFnSc2	F99
Neurčená	určený	k2eNgFnSc1d1	neurčená
duševní	duševní	k2eAgFnSc1d1	duševní
porucha	porucha	k1gFnSc1	porucha
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
kapitolu	kapitola	k1gFnSc4	kapitola
jsou	být	k5eAaImIp3nP	být
stanoveny	stanovit	k5eAaPmNgFnP	stanovit
následující	následující	k2eAgFnPc1d1	následující
položky	položka	k1gFnPc1	položka
s	s	k7c7	s
hvězdičkou	hvězdička	k1gFnSc7	hvězdička
<g/>
:	:	kIx,	:
F	F	kA	F
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
*	*	kIx~	*
Demence	demence	k1gFnSc2	demence
u	u	k7c2	u
Alzheimerovy	Alzheimerův	k2eAgFnSc2d1	Alzheimerova
nemoci	nemoc	k1gFnSc2	nemoc
F	F	kA	F
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
*	*	kIx~	*
Demence	demence	k1gFnPc1	demence
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
nemocí	nemoc	k1gFnPc2	nemoc
zařazených	zařazený	k2eAgFnPc2d1	zařazená
jinde	jinde	k6eAd1	jinde
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
duševní	duševní	k2eAgFnSc1d1	duševní
porucha	porucha	k1gFnSc1	porucha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
ÚZIS	ÚZIS	kA	ÚZIS
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
klasifikace	klasifikace	k1gFnSc1	klasifikace
nemocí	nemoc	k1gFnPc2	nemoc
MNK	MNK	kA	MNK
</s>
