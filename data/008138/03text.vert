<s>
Psycho	psycha	k1gFnSc5	psycha
je	on	k3xPp3gMnPc4	on
americký	americký	k2eAgInSc1d1	americký
hororový	hororový	k2eAgInSc1d1	hororový
snímek	snímek	k1gInSc1	snímek
<g/>
,	,	kIx,	,
natočený	natočený	k2eAgInSc1d1	natočený
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
Alfredem	Alfred	k1gMnSc7	Alfred
Hitchcockem	Hitchcock	k1gInSc7	Hitchcock
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
Roberta	Robert	k1gMnSc2	Robert
Blocha	Bloch	k1gMnSc2	Bloch
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
bývá	bývat	k5eAaImIp3nS	bývat
konstatováno	konstatovat	k5eAaBmNgNnS	konstatovat
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
Truman	Truman	k1gMnSc1	Truman
Capote	capot	k1gInSc5	capot
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
rozhovorů	rozhovor	k1gInPc2	rozhovor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
literární	literární	k2eAgFnSc1d1	literární
předloha	předloha	k1gFnSc1	předloha
je	být	k5eAaImIp3nS	být
slabší	slabý	k2eAgFnSc1d2	slabší
než	než	k8xS	než
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
detektivky	detektivka	k1gFnSc2	detektivka
kniha	kniha	k1gFnSc1	kniha
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
pointě	pointa	k1gFnSc3	pointa
neobstojí	obstát	k5eNaPmIp3nP	obstát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
autor	autor	k1gMnSc1	autor
užívá	užívat	k5eAaImIp3nS	užívat
zavádějící	zavádějící	k2eAgFnPc4d1	zavádějící
věty	věta	k1gFnPc4	věta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
znemožňují	znemožňovat	k5eAaImIp3nP	znemožňovat
uhádnout	uhádnout	k5eAaPmF	uhádnout
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Hitchcock	Hitchcock	k6eAd1	Hitchcock
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
tohoto	tento	k3xDgInSc2	tento
filmu	film	k1gInSc2	film
odchýlil	odchýlit	k5eAaPmAgMnS	odchýlit
od	od	k7c2	od
běžné	běžný	k2eAgFnSc2d1	běžná
praxe	praxe	k1gFnSc2	praxe
a	a	k8xC	a
natočil	natočit	k5eAaBmAgMnS	natočit
ho	on	k3xPp3gInSc4	on
s	s	k7c7	s
nižšími	nízký	k2eAgInPc7d2	nižší
náklady	náklad	k1gInPc7	náklad
a	a	k8xC	a
štábem	štáb	k1gInSc7	štáb
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
svého	svůj	k3xOyFgInSc2	svůj
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
(	(	kIx(	(
<g/>
jím	jíst	k5eAaImIp1nS	jíst
ovšem	ovšem	k9	ovšem
režírovaného	režírovaný	k2eAgInSc2d1	režírovaný
jen	jen	k6eAd1	jen
v	v	k7c6	v
případě	případ	k1gInSc6	případ
asi	asi	k9	asi
deseti	deset	k4xCc2	deset
epizod	epizoda	k1gFnPc2	epizoda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
popularity	popularita	k1gFnSc2	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byl	být	k5eAaImAgInS	být
i	i	k9	i
výrazný	výrazný	k2eAgInSc1d1	výrazný
finanční	finanční	k2eAgInSc1d1	finanční
úspěch	úspěch	k1gInSc1	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
žena	žena	k1gFnSc1	žena
Marion	Marion	k1gInSc1	Marion
odcizí	odcizit	k5eAaPmIp3nS	odcizit
zaměstnavateli	zaměstnavatel	k1gMnSc3	zaměstnavatel
větší	veliký	k2eAgFnSc4d2	veliký
částku	částka	k1gFnSc4	částka
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
uprchne	uprchnout	k5eAaPmIp3nS	uprchnout
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
útěku	útěk	k1gInSc6	útěk
se	se	k3xPyFc4	se
na	na	k7c4	na
noc	noc	k1gFnSc4	noc
zastaví	zastavit	k5eAaPmIp3nS	zastavit
v	v	k7c6	v
motelu	motel	k1gInSc6	motel
<g/>
.	.	kIx.	.
</s>
<s>
Majitel	majitel	k1gMnSc1	majitel
motelu	motel	k1gInSc2	motel
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
svěří	svěřit	k5eAaPmIp3nS	svěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
domě	dům	k1gInSc6	dům
nad	nad	k7c7	nad
motelem	motel	k1gInSc7	motel
se	s	k7c7	s
starou	starý	k2eAgFnSc7d1	stará
matkou	matka	k1gFnSc7	matka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sprchování	sprchování	k1gNnSc6	sprchování
je	být	k5eAaImIp3nS	být
později	pozdě	k6eAd2	pozdě
Marion	Marion	k1gInSc1	Marion
zavražděna	zavraždit	k5eAaPmNgFnS	zavraždit
nožem	nůž	k1gInSc7	nůž
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
chvíli	chvíle	k1gFnSc6	chvíle
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
objevuje	objevovat	k5eAaImIp3nS	objevovat
majitel	majitel	k1gMnSc1	majitel
motelu	motel	k1gInSc2	motel
a	a	k8xC	a
nepřímo	přímo	k6eNd1	přímo
obviňuje	obviňovat	k5eAaImIp3nS	obviňovat
z	z	k7c2	z
vraždy	vražda	k1gFnSc2	vražda
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
koupelnu	koupelna	k1gFnSc4	koupelna
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
k	k	k7c3	k
vraždě	vražda	k1gFnSc3	vražda
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
uklidí	uklidit	k5eAaPmIp3nP	uklidit
a	a	k8xC	a
tělo	tělo	k1gNnSc4	tělo
Marion	Marion	k1gInSc1	Marion
včetně	včetně	k7c2	včetně
jejího	její	k3xOp3gNnSc2	její
auta	auto	k1gNnSc2	auto
zaveze	zavézt	k5eAaPmIp3nS	zavézt
do	do	k7c2	do
bažiny	bažina	k1gFnSc2	bažina
<g/>
.	.	kIx.	.
</s>
<s>
Zmizelou	zmizelý	k2eAgFnSc4d1	Zmizelá
Marion	Marion	k1gInSc4	Marion
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
brzo	brzo	k6eAd1	brzo
začne	začít	k5eAaPmIp3nS	začít
hledat	hledat	k5eAaImF	hledat
její	její	k3xOp3gFnSc1	její
sestra	sestra	k1gFnSc1	sestra
Lila	lít	k5eAaImAgFnS	lít
a	a	k8xC	a
milenec	milenec	k1gMnSc1	milenec
Marion	Marion	k1gInSc4	Marion
Sam	Sam	k1gMnSc1	Sam
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
úkol	úkol	k1gInSc4	úkol
najmou	najmout	k5eAaPmIp3nP	najmout
soukromého	soukromý	k2eAgMnSc4d1	soukromý
detektiva	detektiv	k1gMnSc4	detektiv
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
najde	najít	k5eAaPmIp3nS	najít
stopu	stopa	k1gFnSc4	stopa
Marion	Marion	k1gInSc1	Marion
až	až	k6eAd1	až
do	do	k7c2	do
motelu	motel	k1gInSc2	motel
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
tam	tam	k6eAd1	tam
pátrat	pátrat	k5eAaImF	pátrat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
také	také	k9	také
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
Lila	lila	k2eAgMnSc1d1	lila
a	a	k8xC	a
Sam	Sam	k1gMnSc1	Sam
dozvídají	dozvídat	k5eAaImIp3nP	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
matka	matka	k1gFnSc1	matka
majitele	majitel	k1gMnSc2	majitel
motelu	motel	k1gInSc2	motel
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
<g/>
.	.	kIx.	.
</s>
<s>
Vypraví	vypravit	k5eAaPmIp3nS	vypravit
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
do	do	k7c2	do
domu	dům	k1gInSc2	dům
u	u	k7c2	u
motelu	motel	k1gInSc2	motel
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
Lila	lila	k6eAd1	lila
napadena	napadnout	k5eAaPmNgFnS	napadnout
postavou	postava	k1gFnSc7	postava
v	v	k7c6	v
ženských	ženský	k2eAgInPc6d1	ženský
šatech	šat	k1gInPc6	šat
<g/>
,	,	kIx,	,
při	při	k7c6	při
potyčce	potyčka	k1gFnSc6	potyčka
však	však	k9	však
vyjde	vyjít	k5eAaPmIp3nS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
majitel	majitel	k1gMnSc1	majitel
motelu	motel	k1gInSc2	motel
převlečený	převlečený	k2eAgMnSc1d1	převlečený
za	za	k7c4	za
mrtvou	mrtvý	k2eAgFnSc4d1	mrtvá
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
scéně	scéna	k1gFnSc6	scéna
je	být	k5eAaImIp3nS	být
vysvětleno	vysvětlen	k2eAgNnSc1d1	vysvětleno
<g/>
,	,	kIx,	,
že	že	k8xS	že
majitel	majitel	k1gMnSc1	majitel
motelu	motel	k1gInSc2	motel
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
zabil	zabít	k5eAaPmAgMnS	zabít
a	a	k8xC	a
propadl	propadnout	k5eAaPmAgMnS	propadnout
duševní	duševní	k2eAgFnSc3d1	duševní
chorobě	choroba	k1gFnSc3	choroba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
vydával	vydávat	k5eAaPmAgInS	vydávat
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejznámější	známý	k2eAgFnSc7d3	nejznámější
scénou	scéna	k1gFnSc7	scéna
je	být	k5eAaImIp3nS	být
zavraždění	zavraždění	k1gNnSc1	zavraždění
Marion	Marion	k1gInSc1	Marion
ve	v	k7c6	v
sprše	sprcha	k1gFnSc6	sprcha
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Alfred	Alfred	k1gMnSc1	Alfred
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
o	o	k7c6	o
natáčení	natáčení	k1gNnSc6	natáčení
této	tento	k3xDgFnSc2	tento
scény	scéna	k1gFnSc2	scéna
(	(	kIx(	(
<g/>
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Rozhovory	rozhovor	k1gInPc4	rozhovor
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
Truffaut	Truffaut	k1gMnSc1	Truffaut
<g/>
,	,	kIx,	,
Československý	československý	k2eAgInSc1d1	československý
filmový	filmový	k2eAgInSc1d1	filmový
ústav	ústav	k1gInSc1	ústav
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Lubomír	Lubomír	k1gMnSc1	Lubomír
Oliva	Oliva	k1gMnSc1	Oliva
<g/>
)	)	kIx)	)
říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Natáčení	natáčení	k1gNnSc3	natáčení
této	tento	k3xDgFnSc2	tento
scény	scéna	k1gFnSc2	scéna
trvalo	trvat	k5eAaImAgNnS	trvat
sedm	sedm	k4xCc1	sedm
dní	den	k1gInPc2	den
a	a	k8xC	a
pětačtyřicet	pětačtyřicet	k4xCc1	pětačtyřicet
sekund	sekunda	k1gFnPc2	sekunda
filmu	film	k1gInSc2	film
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
sedmdesát	sedmdesát	k4xCc1	sedmdesát
různých	různý	k2eAgNnPc2d1	různé
postavení	postavení	k1gNnPc2	postavení
kamery	kamera	k1gFnSc2	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
scénu	scéna	k1gFnSc4	scéna
mně	já	k3xPp1nSc6	já
vyrobili	vyrobit	k5eAaPmAgMnP	vyrobit
skvělou	skvělý	k2eAgFnSc4d1	skvělá
figurínu	figurína	k1gFnSc4	figurína
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
měla	mít	k5eAaImAgFnS	mít
pod	pod	k7c7	pod
ranami	rána	k1gFnPc7	rána
nože	nůž	k1gInSc2	nůž
prýštit	prýštit	k5eAaImF	prýštit
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepoužil	použít	k5eNaPmAgMnS	použít
jsem	být	k5eAaImIp1nS	být
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Raději	rád	k6eAd2	rád
jsem	být	k5eAaImIp1nS	být
natáčel	natáčet	k5eAaImAgMnS	natáčet
s	s	k7c7	s
dívkou	dívka	k1gFnSc7	dívka
<g/>
,	,	kIx,	,
nahým	nahý	k2eAgMnSc7d1	nahý
dvojníkem	dvojník	k1gMnSc7	dvojník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
zastupoval	zastupovat	k5eAaImAgInS	zastupovat
Janet	Janet	k1gInSc1	Janet
Leighovou	Leighová	k1gFnSc4	Leighová
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Janet	Janeta	k1gFnPc2	Janeta
vidíme	vidět	k5eAaImIp1nP	vidět
jen	jen	k9	jen
ruce	ruka	k1gFnPc4	ruka
<g/>
,	,	kIx,	,
ramena	rameno	k1gNnPc4	rameno
a	a	k8xC	a
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Všechno	všechen	k3xTgNnSc4	všechen
ostatní	ostatní	k1gNnSc4	ostatní
byl	být	k5eAaImAgMnS	být
dvojník	dvojník	k1gMnSc1	dvojník
<g/>
.	.	kIx.	.
</s>
<s>
Nůž	nůž	k1gInSc1	nůž
se	se	k3xPyFc4	se
přirozeně	přirozeně	k6eAd1	přirozeně
ani	ani	k8xC	ani
jednou	jeden	k4xCgFnSc7	jeden
nedotkl	dotknout	k5eNaPmAgMnS	dotknout
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
všechno	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
dělalo	dělat	k5eAaImAgNnS	dělat
montáží	montáž	k1gFnSc7	montáž
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
scéně	scéna	k1gFnSc6	scéna
je	být	k5eAaImIp3nS	být
nejvíc	hodně	k6eAd3	hodně
násilí	násilí	k1gNnSc4	násilí
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
děj	děj	k1gInSc1	děj
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
násilí	násilí	k1gNnSc4	násilí
stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
tu	ten	k3xDgFnSc4	ten
první	první	k4xOgFnSc4	první
vraždu	vražda	k1gFnSc4	vražda
stačí	stačit	k5eAaBmIp3nS	stačit
vyvolat	vyvolat	k5eAaPmF	vyvolat
úzkost	úzkost	k1gFnSc4	úzkost
v	v	k7c6	v
okamžicích	okamžik	k1gInPc6	okamžik
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
následují	následovat	k5eAaImIp3nP	následovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgInS	dočkat
tří	tři	k4xCgNnPc2	tři
pokračování	pokračování	k1gNnPc2	pokračování
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
třetí	třetí	k4xOgFnSc1	třetí
režíroval	režírovat	k5eAaImAgMnS	režírovat
sám	sám	k3xTgMnSc1	sám
představitel	představitel	k1gMnSc1	představitel
hlavní	hlavní	k2eAgFnSc2d1	hlavní
role	role	k1gFnSc2	role
Anthony	Anthona	k1gFnSc2	Anthona
Perkins	Perkinsa	k1gFnPc2	Perkinsa
(	(	kIx(	(
<g/>
mimochodem	mimochodem	k9	mimochodem
<g/>
,	,	kIx,	,
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgMnSc1	tento
hrdina	hrdina	k1gMnSc1	hrdina
tlustý	tlustý	k2eAgMnSc1d1	tlustý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrté	čtvrtý	k4xOgNnSc1	čtvrtý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
prequel	prequel	k1gInSc1	prequel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sice	sice	k8xC	sice
Perkins	Perkins	k1gInSc1	Perkins
opět	opět	k6eAd1	opět
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc4	jeho
Normana	Norman	k1gMnSc4	Norman
Batese	Batese	k1gFnSc2	Batese
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
a	a	k8xC	a
při	při	k7c6	při
utváření	utváření	k1gNnSc6	utváření
úchylky	úchylka	k1gFnSc2	úchylka
však	však	k9	však
představuje	představovat	k5eAaImIp3nS	představovat
jiný	jiný	k2eAgMnSc1d1	jiný
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
Perkins	Perkins	k1gInSc1	Perkins
zemřel	zemřít	k5eAaPmAgInS	zemřít
během	během	k7c2	během
příprav	příprava	k1gFnPc2	příprava
pátého	pátý	k4xOgInSc2	pátý
dílu	díl	k1gInSc2	díl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavost	zajímavost	k1gFnSc1	zajímavost
<g/>
:	:	kIx,	:
Série	série	k1gFnSc1	série
tudíž	tudíž	k8xC	tudíž
momentálně	momentálně	k6eAd1	momentálně
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
podobnou	podobný	k2eAgFnSc4d1	podobná
strukturu	struktura	k1gFnSc4	struktura
jako	jako	k9	jako
čtyři	čtyři	k4xCgInPc1	čtyři
filmy	film	k1gInPc1	film
o	o	k7c6	o
Hannibalu	Hannibal	k1gMnSc6	Hannibal
Lecterovi	Lecter	k1gMnSc6	Lecter
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
poslední	poslední	k2eAgFnSc4d1	poslední
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
stal	stát	k5eAaPmAgMnS	stát
prequelem	prequel	k1gInSc7	prequel
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
k	k	k7c3	k
filmům	film	k1gInPc3	film
s	s	k7c7	s
Hannibalem	Hannibal	k1gInSc7	Hannibal
však	však	k9	však
existují	existovat	k5eAaImIp3nP	existovat
jako	jako	k9	jako
předlohy	předloha	k1gFnPc1	předloha
Harrisovy	Harrisův	k2eAgFnPc1d1	Harrisova
romány	román	k1gInPc4	román
<g/>
,	,	kIx,	,
Bloch	Bloch	k1gMnSc1	Bloch
napsal	napsat	k5eAaPmAgMnS	napsat
romány	román	k1gInPc4	román
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgNnPc1	tři
a	a	k8xC	a
otázkou	otázka	k1gFnSc7	otázka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
nakolik	nakolik	k6eAd1	nakolik
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
drží	držet	k5eAaImIp3nS	držet
druhý	druhý	k4xOgInSc4	druhý
a	a	k8xC	a
třetí	třetí	k4xOgInSc4	třetí
díl	díl	k1gInSc4	díl
filmového	filmový	k2eAgInSc2d1	filmový
Psycha	psycha	k1gFnSc1	psycha
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc4	dva
série	série	k1gFnPc4	série
spojuje	spojovat	k5eAaImIp3nS	spojovat
i	i	k9	i
tatáž	týž	k3xTgFnSc1	týž
postava	postava	k1gFnSc1	postava
reálného	reálný	k2eAgMnSc2d1	reálný
vraha	vrah	k1gMnSc2	vrah
Eda	Eda	k1gMnSc1	Eda
Gaina	Gaina	k1gFnSc1	Gaina
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pozor	pozor	k1gInSc1	pozor
<g/>
:	:	kIx,	:
Zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
Blocha	Bloch	k1gMnSc2	Bloch
byl	být	k5eAaImAgMnS	být
předlohou	předloha	k1gFnSc7	předloha
Normana	Norman	k1gMnSc2	Norman
Batese	Batese	k1gFnSc2	Batese
<g/>
,	,	kIx,	,
u	u	k7c2	u
Harrise	Harrise	k1gFnSc2	Harrise
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
vraha	vrah	k1gMnSc4	vrah
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yIgInSc6	který
pátrá	pátrat	k5eAaImIp3nS	pátrat
Hannibal	Hannibal	k1gInSc4	Hannibal
v	v	k7c6	v
románu	román	k1gInSc6	román
a	a	k8xC	a
filmu	film	k1gInSc6	film
Mlčení	mlčení	k1gNnSc4	mlčení
jehňátek	jehňátko	k1gNnPc2	jehňátko
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Psycho	psycha	k1gFnSc5	psycha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Psycho	psycha	k1gFnSc5	psycha
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc3d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
Psycho	psycha	k1gFnSc5	psycha
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Psycho	psycha	k1gFnSc5	psycha
v	v	k7c6	v
Internet	Internet	k1gInSc1	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
</s>
