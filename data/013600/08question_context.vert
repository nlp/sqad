<s>
Dámský	dámský	k2eAgInSc1d1
házenkářský	házenkářský	k2eAgInSc1d1
klub	klub	k1gInSc1
Zora	Zora	k1gFnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
z.	z.	kA
s.	s.	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
spolkem	spolek	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
provozuje	provozovat	k5eAaImIp3nS
házenou	házená	k1gFnSc4
na	na	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
,	,	kIx,
s	s	k7c7
návazností	návaznost	k1gFnSc7
na	na	k7c4
státní	státní	k2eAgFnSc4d1
reprezentaci	reprezentace	k1gFnSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>