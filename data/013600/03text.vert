<s>
DHK	DHK	kA
Zora	Zora	k1gFnSc1
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
DHK	DHK	kA
Zora	Zora	k1gFnSc1
OlomoucNázev	OlomoucNázev	k1gFnSc1
</s>
<s>
Dámský	dámský	k2eAgInSc1d1
házenkářský	házenkářský	k2eAgInSc1d1
klub	klub	k1gInSc1
Zora	Zora	k1gFnSc1
Olomouc	Olomouc	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
U	u	k7c2
stadionu	stadion	k1gInSc2
1357	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
a	a	k8xC
<g/>
772	#num#	k4
00	#num#	k4
Olomouc	Olomouc	k1gFnSc1
Založen	založen	k2eAgInSc4d1
</s>
<s>
1949	#num#	k4
Klubové	klubový	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
</s>
<s>
modrá	modrý	k2eAgFnSc1d1
a	a	k8xC
bílá	bílý	k2eAgFnSc1d1
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Extraliga	extraliga	k1gFnSc1
házené	házená	k1gFnSc2
Web	web	k1gInSc1
</s>
<s>
http://www.dhkzoraolomouc.cz	http://www.dhkzoraolomouc.cz	k1gMnSc1
</s>
<s>
Dámský	dámský	k2eAgInSc1d1
házenkářský	házenkářský	k2eAgInSc1d1
klub	klub	k1gInSc1
Zora	Zora	k1gFnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
z.	z.	kA
s.	s.	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
spolkem	spolek	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
provozuje	provozovat	k5eAaImIp3nS
házenou	házená	k1gFnSc4
na	na	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
,	,	kIx,
s	s	k7c7
návazností	návaznost	k1gFnSc7
na	na	k7c4
státní	státní	k2eAgFnSc4d1
reprezentaci	reprezentace	k1gFnSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
je	být	k5eAaImIp3nS
pokračovatelem	pokračovatel	k1gMnSc7
tradic	tradice	k1gFnPc2
ženské	ženský	k2eAgFnSc2d1
české	český	k2eAgFnSc2d1
házené	házená	k1gFnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
zde	zde	k6eAd1
počíná	počínat	k5eAaImIp3nS
rokem	rok	k1gInSc7
1919	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klubové	klubový	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
jsou	být	k5eAaImIp3nP
modrá	modrý	k2eAgNnPc1d1
a	a	k8xC
bílá	bílý	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Sportovní	sportovní	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
je	být	k5eAaImIp3nS
provozována	provozovat	k5eAaImNgFnS
v	v	k7c6
areálu	areál	k1gInSc6
házené	házená	k1gFnSc2
na	na	k7c6
Andrově	Andrův	k2eAgInSc6d1
stadionu	stadion	k1gInSc6
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
jsou	být	k5eAaImIp3nP
dvě	dva	k4xCgNnPc4
venkovní	venkovní	k2eAgNnPc4d1
hřiště	hřiště	k1gNnPc4
(	(	kIx(
<g/>
jedno	jeden	k4xCgNnSc1
s	s	k7c7
asfaltovým	asfaltový	k2eAgMnSc7d1
a	a	k8xC
jedno	jeden	k4xCgNnSc1
s	s	k7c7
antukovým	antukový	k2eAgInSc7d1
povrchem	povrch	k1gInSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sportovní	sportovní	k2eAgFnSc1d1
hala	hala	k1gFnSc1
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
600	#num#	k4
diváků	divák	k1gMnPc2
<g/>
,	,	kIx,
s	s	k7c7
turistickou	turistický	k2eAgFnSc7d1
ubytovnou	ubytovna	k1gFnSc7
s	s	k7c7
osmi	osm	k4xCc7
třílůžkovými	třílůžkový	k2eAgInPc7d1
pokoji	pokoj	k1gInPc7
a	a	k8xC
rehabilitačním	rehabilitační	k2eAgNnSc7d1
zařízením	zařízení	k1gNnSc7
se	s	k7c7
saunou	sauna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
areálu	areál	k1gInSc2
je	být	k5eAaImIp3nS
budova	budova	k1gFnSc1
se	s	k7c7
sociálním	sociální	k2eAgNnSc7d1
zázemím	zázemí	k1gNnSc7
<g/>
,	,	kIx,
šatnami	šatna	k1gFnPc7
<g/>
,	,	kIx,
skladovými	skladový	k2eAgFnPc7d1
prostorami	prostora	k1gFnPc7
a	a	k8xC
restaurací	restaurace	k1gFnPc2
APETIT	apetit	k1gInSc1
–	–	k?
ZORA	Zora	k1gFnSc1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
pronajata	pronajmout	k5eAaPmNgFnS
k	k	k7c3
provozování	provozování	k1gNnSc3
restaurační	restaurační	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Majetkově	majetkově	k6eAd1
jsou	být	k5eAaImIp3nP
všechna	všechen	k3xTgNnPc4
tato	tento	k3xDgNnPc1
sportovní	sportovní	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
a	a	k8xC
související	související	k2eAgFnPc4d1
budovy	budova	k1gFnPc4
v	v	k7c6
majetku	majetek	k1gInSc6
DHK	DHK	kA
Zora	Zora	k1gFnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
venkovních	venkovní	k2eAgNnPc2d1
hřišť	hřiště	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
získána	získat	k5eAaPmNgFnS
jako	jako	k8xC,k8xS
dar	dar	k1gInSc1
od	od	k7c2
SK	Sk	kA
Sigma	sigma	k1gNnPc6
Olomouc	Olomouc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Spolek	spolek	k1gInSc1
měl	mít	k5eAaImAgInS
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc6
2005	#num#	k4
celkem	celkem	k6eAd1
511	#num#	k4
členů	člen	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
bylo	být	k5eAaImAgNnS
346	#num#	k4
hráček	hráčka	k1gFnPc2
všech	všecek	k3xTgFnPc2
věkových	věkový	k2eAgFnPc2d1
kategorií	kategorie	k1gFnPc2
od	od	k7c2
přípravky	přípravka	k1gFnSc2
a	a	k8xC
miniházené	miniházený	k2eAgFnSc2d1
až	až	k9
po	po	k7c4
družstvo	družstvo	k1gNnSc4
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
58	#num#	k4
funkcionářů	funkcionář	k1gMnPc2
<g/>
,	,	kIx,
trenérů	trenér	k1gMnPc2
a	a	k8xC
rozhodčích	rozhodčí	k1gMnPc2
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgMnPc4d1
byli	být	k5eAaImAgMnP
členové	člen	k1gMnPc1
a	a	k8xC
příznivci	příznivec	k1gMnPc1
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organizačně	organizačně	k6eAd1
řídí	řídit	k5eAaImIp3nS
dočasně	dočasně	k6eAd1
klub	klub	k1gInSc1
řídící	řídící	k2eAgInSc1d1
výbor	výbor	k1gInSc1
(	(	kIx(
<g/>
ŘV	ŘV	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
organizuje	organizovat	k5eAaBmIp3nS
činnost	činnost	k1gFnSc4
klubu	klub	k1gInSc2
zejména	zejména	k9
po	po	k7c6
stránce	stránka	k1gFnSc6
finanční	finanční	k2eAgFnSc6d1
a	a	k8xC
marketingové	marketingový	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracuje	pracovat	k5eAaImIp3nS
ve	v	k7c6
složení	složení	k1gNnSc6
<g/>
:	:	kIx,
Ing.	ing.	kA
Vítězslav	Vítězslav	k1gMnSc1
Růžička	Růžička	k1gMnSc1
–	–	k?
místopředseda	místopředseda	k1gMnSc1
klubu	klub	k1gInSc2
(	(	kIx(
<g/>
statutární	statutární	k2eAgMnSc1d1
zástupce	zástupce	k1gMnSc1
klubu	klub	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
Maráček	Maráček	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Juráš	Juráš	k1gMnSc1
<g/>
,	,	kIx,
Vítězslav	Vítězslav	k1gMnSc1
Hejtmánek	Hejtmánek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sportovní	sportovní	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
organizuje	organizovat	k5eAaBmIp3nS
a	a	k8xC
řídí	řídit	k5eAaImIp3nS
Sportovní	sportovní	k2eAgFnSc1d1
komise	komise	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
pracuje	pracovat	k5eAaImIp3nS
ve	v	k7c6
složení	složení	k1gNnSc6
<g/>
:	:	kIx,
Ing.	ing.	kA
Václav	Václav	k1gMnSc1
Dobeš	Dobeš	k1gMnSc1
–	–	k?
předseda	předseda	k1gMnSc1
komise	komise	k1gFnSc2
a	a	k8xC
organizační	organizační	k2eAgMnSc1d1
pracovník	pracovník	k1gMnSc1
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Juráš	Juráš	k1gMnSc1
<g/>
,	,	kIx,
Vítězslav	Vítězslav	k1gMnSc1
Hejtmánek	Hejtmánek	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
Lubomír	Lubomír	k1gMnSc1
Krejčíř	Krejčíř	k1gMnSc1
<g/>
,	,	kIx,
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Bělka	bělka	k1gFnSc1
<g/>
,	,	kIx,
Valdemar	Valdemara	k1gFnPc2
Macharáček	Macharáček	k1gInSc1
<g/>
,	,	kIx,
Iveta	Iveta	k1gFnSc1
Hofmanová	Hofmanová	k1gFnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Novotný	Novotný	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Mimo	mimo	k7c4
sportovní	sportovní	k2eAgFnPc4d1
činnosti	činnost	k1gFnPc4
družstev	družstvo	k1gNnPc2
všech	všecek	k3xTgFnPc2
věkových	věkový	k2eAgFnPc2d1
kategorií	kategorie	k1gFnPc2
(	(	kIx(
<g/>
ženy	žena	k1gFnPc1
<g/>
,	,	kIx,
starší	starý	k2eAgFnPc1d2
dorostenky	dorostenka	k1gFnPc1
<g/>
,	,	kIx,
mladší	mladý	k2eAgFnPc1d2
dorostenky	dorostenka	k1gFnPc1
<g/>
,	,	kIx,
starší	starý	k2eAgFnPc1d2
a	a	k8xC
mladší	mladý	k2eAgFnPc1d2
žákyně	žákyně	k1gFnPc1
<g/>
,	,	kIx,
miniházená	miniházený	k2eAgFnSc1d1
4	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
a	a	k8xC
přípravka	přípravka	k1gFnSc1
<g/>
)	)	kIx)
zabezpečuje	zabezpečovat	k5eAaImIp3nS
klub	klub	k1gInSc4
organizačně	organizačně	k6eAd1
<g/>
,	,	kIx,
personálně	personálně	k6eAd1
i	i	k9
technicky	technicky	k6eAd1
činnost	činnost	k1gFnSc4
Sportovního	sportovní	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
mládeže	mládež	k1gFnSc2
(	(	kIx(
<g/>
SCM	SCM	kA
<g/>
)	)	kIx)
a	a	k8xC
sportovních	sportovní	k2eAgFnPc2d1
tříd	třída	k1gFnPc2
(	(	kIx(
<g/>
ST	St	kA
<g/>
)	)	kIx)
na	na	k7c6
ZŠ	ZŠ	kA
Holečkova	Holečkův	k2eAgFnSc1d1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucím	vedoucí	k1gMnSc7
trenérem	trenér	k1gMnSc7
SCM	SCM	kA
je	být	k5eAaImIp3nS
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Bělka	bělka	k1gFnSc1
a	a	k8xC
ST	St	kA
Iveta	Iveta	k1gFnSc1
Hofmanová	Hofmanová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
www.dhkzoraolomouc.cz	www.dhkzoraolomouc.cz	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Morava	Morava	k1gFnSc1
|	|	kIx~
Sport	sport	k1gInSc1
</s>
