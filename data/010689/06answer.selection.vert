<s>
Březnová	březnový	k2eAgFnSc1d1	březnová
rovnodennost	rovnodennost	k1gFnSc1	rovnodennost
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
přechod	přechod	k1gInSc4	přechod
z	z	k7c2	z
astronomické	astronomický	k2eAgFnSc2d1	astronomická
zimy	zima	k1gFnSc2	zima
do	do	k7c2	do
jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
mluvíme	mluvit	k5eAaImIp1nP	mluvit
tedy	tedy	k9	tedy
o	o	k7c6	o
jarní	jarní	k2eAgFnSc6d1	jarní
rovnodennosti	rovnodennost	k1gFnSc6	rovnodennost
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
v	v	k7c6	v
září	září	k1gNnSc6	září
přechází	přecházet	k5eAaImIp3nS	přecházet
léto	léto	k1gNnSc1	léto
v	v	k7c4	v
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
mluvme	mluvit	k5eAaImRp1nP	mluvit
o	o	k7c4	o
rovnodennosti	rovnodennost	k1gFnPc4	rovnodennost
podzimní	podzimní	k2eAgFnPc4d1	podzimní
<g/>
.	.	kIx.	.
</s>
