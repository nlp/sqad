<s>
Čára	čára	k1gFnSc1	čára
ponoru	ponor	k1gInSc2	ponor
je	být	k5eAaImIp3nS	být
ukazatel	ukazatel	k1gInSc1	ukazatel
optimálního	optimální	k2eAgInSc2d1	optimální
ponoru	ponor	k1gInSc2	ponor
lodi	loď	k1gFnSc2	loď
stanovený	stanovený	k2eAgInSc4d1	stanovený
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
loď	loď	k1gFnSc1	loď
kladla	klást	k5eAaImAgFnS	klást
nejmenší	malý	k2eAgInSc4d3	nejmenší
odpor	odpor	k1gInSc4	odpor
při	při	k7c6	při
plavbě	plavba	k1gFnSc6	plavba
<g/>
.	.	kIx.	.
</s>
<s>
Ponor	ponor	k1gInSc1	ponor
je	být	k5eAaImIp3nS	být
měřen	měřit	k5eAaImNgInS	měřit
jako	jako	k8xS	jako
svislá	svislý	k2eAgFnSc1d1	svislá
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
nejnižšího	nízký	k2eAgInSc2d3	nejnižší
bodu	bod	k1gInSc2	bod
lodi	loď	k1gFnSc2	loď
od	od	k7c2	od
hladiny	hladina	k1gFnSc2	hladina
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c4	na
zatížení	zatížení	k1gNnSc4	zatížení
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
hustotě	hustota	k1gFnSc6	hustota
vody	voda	k1gFnSc2	voda
resp.	resp.	kA	resp.
činitelích	činitel	k1gMnPc6	činitel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ji	on	k3xPp3gFnSc4	on
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
tj.	tj.	kA	tj.
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
slanosti	slanost	k1gFnSc6	slanost
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
barevně	barevně	k6eAd1	barevně
označená	označený	k2eAgFnSc1d1	označená
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
odpor	odpor	k1gInSc1	odpor
vody	voda	k1gFnSc2	voda
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
vpřed	vpřed	k6eAd1	vpřed
je	být	k5eAaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
délce	délka	k1gFnSc6	délka
čáry	čára	k1gFnSc2	čára
ponoru	ponor	k1gInSc2	ponor
(	(	kIx(	(
<g/>
vlnový	vlnový	k2eAgInSc1d1	vlnový
odpor	odpor	k1gInSc1	odpor
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
a	a	k8xC	a
délce	délka	k1gFnSc6	délka
plavidla	plavidlo	k1gNnSc2	plavidlo
<g/>
)	)	kIx)	)
a	a	k8xC	a
šířce	šířka	k1gFnSc6	šířka
lodi	loď	k1gFnSc2	loď
(	(	kIx(	(
<g/>
frikční	frikční	k2eAgInSc4d1	frikční
odpor	odpor	k1gInSc4	odpor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čím	čí	k3xOyRgNnSc7	čí
delší	dlouhý	k2eAgFnSc1d2	delší
je	být	k5eAaImIp3nS	být
čára	čára	k1gFnSc1	čára
ponoru	ponor	k1gInSc2	ponor
a	a	k8xC	a
čím	co	k3yInSc7	co
užší	úzký	k2eAgFnSc1d2	užší
je	být	k5eAaImIp3nS	být
ponořená	ponořený	k2eAgFnSc1d1	ponořená
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
rychleji	rychle	k6eAd2	rychle
loď	loď	k1gFnSc1	loď
pojede	pojet	k5eAaPmIp3nS	pojet
a	a	k8xC	a
snáze	snadno	k6eAd2	snadno
bude	být	k5eAaImBp3nS	být
udržovat	udržovat	k5eAaImF	udržovat
přímý	přímý	k2eAgInSc4d1	přímý
směr	směr	k1gInSc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stejné	stejný	k2eAgFnSc6d1	stejná
rychlosti	rychlost	k1gFnSc6	rychlost
by	by	kYmCp3nS	by
delší	dlouhý	k2eAgFnSc1d2	delší
loď	loď	k1gFnSc1	loď
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
menší	malý	k2eAgInSc4d2	menší
vlnový	vlnový	k2eAgInSc4d1	vlnový
odpor	odpor	k1gInSc4	odpor
než	než	k8xS	než
loď	loď	k1gFnSc1	loď
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
boku	bok	k1gInSc6	bok
námořních	námořní	k2eAgFnPc2d1	námořní
lodí	loď	k1gFnPc2	loď
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
značka	značka	k1gFnSc1	značka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
maximální	maximální	k2eAgFnSc4d1	maximální
čáru	čára	k1gFnSc4	čára
ponoru	ponor	k1gInSc2	ponor
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
různé	různý	k2eAgFnSc2d1	různá
hustoty	hustota	k1gFnSc2	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
čáry	čára	k1gFnPc1	čára
jsou	být	k5eAaImIp3nP	být
označeny	označit	k5eAaPmNgFnP	označit
zkratkami	zkratka	k1gFnPc7	zkratka
FW	FW	kA	FW
–	–	k?	–
ve	v	k7c6	v
sladké	sladký	k2eAgFnSc6d1	sladká
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Fresh	Fresh	k1gMnSc1	Fresh
Water	Water	k1gMnSc1	Water
<g/>
)	)	kIx)	)
TF	tf	k0wR	tf
–	–	k?	–
v	v	k7c6	v
tropické	tropický	k2eAgFnSc6d1	tropická
sladké	sladký	k2eAgFnSc6d1	sladká
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
Tropical	Tropical	k1gMnSc1	Tropical
Fresh	Fresh	k1gMnSc1	Fresh
Water	Water	k1gMnSc1	Water
<g/>
)	)	kIx)	)
IS	IS	kA	IS
–	–	k?	–
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
(	(	kIx(	(
<g/>
India	indium	k1gNnPc1	indium
Summer	Summra	k1gFnPc2	Summra
<g/>
)	)	kIx)	)
S	s	k7c7	s
–	–	k?	–
ve	v	k7c6	v
slané	slaný	k2eAgFnSc6d1	slaná
vodě	voda	k1gFnSc6	voda
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Summer	Summer	k1gMnSc1	Summer
<g/>
)	)	kIx)	)
W	W	kA	W
–	–	k?	–
ve	v	k7c6	v
slané	slaný	k2eAgFnSc6d1	slaná
vodě	voda	k1gFnSc6	voda
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
(	(	kIx(	(
<g/>
Winter	Winter	k1gMnSc1	Winter
<g/>
)	)	kIx)	)
WNA	WNA	kA	WNA
–	–	k?	–
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Atlantiku	Atlantik	k1gInSc6	Atlantik
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
(	(	kIx(	(
<g/>
Winter	Winter	k1gMnSc1	Winter
North	North	k1gMnSc1	North
Atlantic	Atlantice	k1gFnPc2	Atlantice
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Čára	čára	k1gFnSc1	čára
ponoru	ponor	k1gInSc6	ponor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Lodě	loď	k1gFnSc2	loď
od	od	k7c2	od
A	A	kA	A
až	až	k9	až
po	po	k7c6	po
Ž	Ž	kA	Ž
TURISTICKÉ	turistický	k2eAgFnPc4d1	turistická
LODĚ	loď	k1gFnPc4	loď
Směrnice	směrnice	k1gFnSc2	směrnice
Rady	rada	k1gFnSc2	rada
ze	z	k7c2	z
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
stanoví	stanovit	k5eAaPmIp3nS	stanovit
technické	technický	k2eAgInPc4d1	technický
požadavky	požadavek	k1gInPc4	požadavek
pro	pro	k7c4	pro
plavidla	plavidlo	k1gNnPc4	plavidlo
vnitrozemské	vnitrozemský	k2eAgFnSc2d1	vnitrozemská
plavby	plavba	k1gFnSc2	plavba
(	(	kIx(	(
<g/>
82	[number]	k4	82
<g/>
/	/	kIx~	/
<g/>
714	[number]	k4	714
<g/>
/	/	kIx~	/
<g/>
EHS	EHS	kA	EHS
<g/>
)	)	kIx)	)
</s>
