<s>
Čára	čára	k1gFnSc1	čára
ponoru	ponor	k1gInSc2	ponor
je	být	k5eAaImIp3nS	být
ukazatel	ukazatel	k1gInSc1	ukazatel
optimálního	optimální	k2eAgInSc2d1	optimální
ponoru	ponor	k1gInSc2	ponor
lodi	loď	k1gFnSc2	loď
stanovený	stanovený	k2eAgInSc4d1	stanovený
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
loď	loď	k1gFnSc1	loď
kladla	klást	k5eAaImAgFnS	klást
nejmenší	malý	k2eAgInSc4d3	nejmenší
odpor	odpor	k1gInSc4	odpor
při	při	k7c6	při
plavbě	plavba	k1gFnSc6	plavba
<g/>
.	.	kIx.	.
</s>
