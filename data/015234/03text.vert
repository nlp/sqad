<s>
Geysir	Geysir	k1gMnSc1
</s>
<s>
GeysirErupce	GeysirErupce	k1gFnSc1
Geysiru	Geysir	k1gInSc2
během	během	k7c2
léta	léto	k1gNnSc2
2009	#num#	k4
</s>
<s>
Geysir	Geysir	k1gInSc1
(	(	kIx(
<g/>
islandsky	islandsky	k6eAd1
Stóri	Stóri	k1gNnSc1
Geysir	Geysira	k1gFnPc2
<g/>
,	,	kIx,
česky	česky	k6eAd1
Gejzír	gejzír	k1gInSc1
nebo	nebo	k8xC
Velký	velký	k2eAgInSc1d1
gejzír	gejzír	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
gejzír	gejzír	k1gInSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
prolomového	prolomový	k2eAgInSc2d1
riftu	rift	k1gInSc2
Haukadular	Haukadulara	k1gFnPc2
<g/>
,	,	kIx,
80	#num#	k4
kilometrů	kilometr	k1gInPc2
východně	východně	k6eAd1
od	od	k7c2
Reykjavíku	Reykjavík	k1gInSc2
na	na	k7c6
jihozápadním	jihozápadní	k2eAgInSc6d1
Islandu	Island	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
od	od	k7c2
islandského	islandský	k2eAgInSc2d1
názvu	název	k1gInSc2
tohoto	tento	k3xDgInSc2
útvaru	útvar	k1gInSc2
se	se	k3xPyFc4
odvozuje	odvozovat	k5eAaImIp3nS
geologický	geologický	k2eAgInSc1d1
termín	termín	k1gInSc1
gejzír	gejzír	k1gInSc1
(	(	kIx(
<g/>
z	z	k7c2
islandského	islandský	k2eAgInSc2d1
výrazu	výraz	k1gInSc2
„	„	k?
<g/>
geysa	geys	k1gMnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
proudit	proudit	k5eAaPmF,k5eAaImF
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Velký	velký	k2eAgInSc1d1
gejzír	gejzír	k1gInSc1
je	být	k5eAaImIp3nS
přitom	přitom	k6eAd1
také	také	k6eAd1
nejstarším	starý	k2eAgMnSc7d3
známým	známý	k2eAgMnSc7d1
zástupcem	zástupce	k1gMnSc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
aktivní	aktivní	k2eAgInSc1d1
byl	být	k5eAaImAgInS
popsán	popsán	k2eAgInSc1d1
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1294	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Okolní	okolní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1846	#num#	k4
prozkoumal	prozkoumat	k5eAaPmAgMnS
Robert	Robert	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
Bunsen	Bunsen	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
jako	jako	k9
první	první	k4xOgInSc4
vědecky	vědecky	k6eAd1
vysvětlil	vysvětlit	k5eAaPmAgMnS
příčiny	příčina	k1gFnPc4
tohoto	tento	k3xDgInSc2
úkazu	úkaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Aktivita	aktivita	k1gFnSc1
gejzíru	gejzír	k1gInSc2
je	být	k5eAaImIp3nS
nepravidelná	pravidelný	k2eNgFnSc1d1
a	a	k8xC
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
sopečné	sopečný	k2eAgFnSc6d1
aktivitě	aktivita	k1gFnSc6
celé	celý	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaznamenána	zaznamenán	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc1
aktivita	aktivita	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1630	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
probudil	probudit	k5eAaPmAgInS
k	k	k7c3
činnosti	činnost	k1gFnSc3
po	po	k7c6
téměř	téměř	k6eAd1
40	#num#	k4
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1896	#num#	k4
začal	začít	k5eAaPmAgMnS
chrlit	chrlit	k5eAaImF
vodu	voda	k1gFnSc4
a	a	k8xC
páru	pára	k1gFnSc4
po	po	k7c6
dlouhých	dlouhý	k2eAgNnPc6d1
letech	léto	k1gNnPc6
klidu	klid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1910	#num#	k4
<g/>
–	–	k?
<g/>
1915	#num#	k4
byla	být	k5eAaImAgFnS
naopak	naopak	k6eAd1
jeho	jeho	k3xOp3gFnSc1
frekvence	frekvence	k1gFnSc1
jen	jen	k9
asi	asi	k9
30	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1915	#num#	k4
činnost	činnost	k1gFnSc1
gejzíru	gejzír	k1gInSc2
ustala	ustat	k5eAaPmAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
následná	následný	k2eAgFnSc1d1
zemětřesná	zemětřesný	k2eAgFnSc1d1
aktivita	aktivita	k1gFnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
ho	on	k3xPp3gNnSc2
opět	opět	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
probudila	probudit	k5eAaPmAgFnS
k	k	k7c3
životu	život	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
sérii	série	k1gFnSc6
zemětřesení	zemětřesení	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zasáhla	zasáhnout	k5eAaPmAgFnS
oblast	oblast	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
<g/>
,	,	kIx,
Geysir	Geysir	k1gInSc1
opět	opět	k6eAd1
chrlí	chrlit	k5eAaImIp3nS
vodu	voda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Frekvence	frekvence	k1gFnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
5	#num#	k4
až	až	k9
8	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vařící	vařící	k2eAgFnSc1d1
voda	voda	k1gFnSc1
je	být	k5eAaImIp3nS
vyvrhována	vyvrhovat	k5eAaImNgFnS
až	až	k6eAd1
do	do	k7c2
výšky	výška	k1gFnSc2
kolem	kolem	k7c2
80	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1845	#num#	k4
dosáhla	dosáhnout	k5eAaPmAgFnS
až	až	k9
170	#num#	k4
m.	m.	k?
Mezi	mezi	k7c7
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
červnem	červen	k1gInSc7
2000	#num#	k4
<g/>
,	,	kIx,
během	během	k7c2
zemětřesení	zemětřesení	k1gNnSc2
<g/>
,	,	kIx,
dosahoval	dosahovat	k5eAaImAgInS
do	do	k7c2
výšky	výška	k1gFnSc2
122	#num#	k4
m	m	kA
po	po	k7c4
dva	dva	k4xCgInPc4
dny	den	k1gInPc4
a	a	k8xC
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
nejvyšší	vysoký	k2eAgInSc4d3
gejzír	gejzír	k1gInSc4
v	v	k7c6
činnosti	činnost	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
dočasně	dočasně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Návštěva	návštěva	k1gFnSc1
Geysiru	Geysir	k1gInSc2
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
nejznámější	známý	k2eAgFnSc2d3
turistické	turistický	k2eAgFnSc2d1
trasy	trasa	k1gFnSc2
na	na	k7c6
Islandu	Island	k1gInSc6
<g/>
,	,	kIx,
zvané	zvaný	k2eAgNnSc4d1
Zlatý	zlatý	k2eAgInSc4d1
kruh	kruh	k1gInSc4
(	(	kIx(
<g/>
Gullni	Gullni	k1gMnSc1
hringurinn	hringurinn	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
National	National	k1gMnSc1
Geographic	Geographic	k1gMnSc1
Česko	Česko	k1gNnSc4
Dostupné	dostupný	k2eAgFnSc2d1
online	onlin	k1gInSc5
<g/>
↑	↑	k?
Islandský	islandský	k2eAgInSc1d1
gejzír	gejzír	k1gInSc1
neúnavně	únavně	k6eNd1
vybuchuje	vybuchovat	k5eAaImIp3nS
každých	každý	k3xTgNnPc2
pět	pět	k4xCc4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
k	k	k7c3
těm	ten	k3xDgNnPc3
nejčinnějším	činný	k2eAgNnPc3d3
na	na	k7c6
světě	svět	k1gInSc6
Dostupné	dostupný	k2eAgFnSc2d1
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
Island	Island	k1gInSc1
zpoplatní	zpoplatnit	k5eAaPmIp3nS
návštěvníkům	návštěvník	k1gMnPc3
proslulý	proslulý	k2eAgInSc4d1
gejzír	gejzír	k1gInSc4
Dostupné	dostupný	k2eAgFnSc2d1
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
Novinky	novinka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Geysir	Geysira	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7517833-3	7517833-3	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
246989034	#num#	k4
</s>
