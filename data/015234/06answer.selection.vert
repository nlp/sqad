<s>
Okolní	okolní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1846	#num#	k4
prozkoumal	prozkoumat	k5eAaPmAgMnS
Robert	Robert	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
Bunsen	Bunsen	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
jako	jako	k9
první	první	k4xOgInSc4
vědecky	vědecky	k6eAd1
vysvětlil	vysvětlit	k5eAaPmAgMnS
příčiny	příčina	k1gFnPc4
tohoto	tento	k3xDgInSc2
úkazu	úkaz	k1gInSc2
<g/>
.	.	kIx.
</s>