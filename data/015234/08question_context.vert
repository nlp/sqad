<s desamb="1">
Právě	právě	k6eAd1
od	od	k7c2
islandského	islandský	k2eAgInSc2d1
názvu	název	k1gInSc2
tohoto	tento	k3xDgInSc2
útvaru	útvar	k1gInSc2
se	se	k3xPyFc4
odvozuje	odvozovat	k5eAaImIp3nS
geologický	geologický	k2eAgInSc1d1
termín	termín	k1gInSc1
gejzír	gejzír	k1gInSc1
(	(	kIx(
<g/>
z	z	k7c2
islandského	islandský	k2eAgInSc2d1
výrazu	výraz	k1gInSc2
„	„	k?
<g/>
geysa	geys	k1gMnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
proudit	proudit	k5eAaPmF,k5eAaImF
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Velký	velký	k2eAgInSc1d1
gejzír	gejzír	k1gInSc1
je	být	k5eAaImIp3nS
přitom	přitom	k6eAd1
také	také	k6eAd1
nejstarším	starý	k2eAgMnSc7d3
známým	známý	k2eAgMnSc7d1
zástupcem	zástupce	k1gMnSc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
aktivní	aktivní	k2eAgInSc1d1
byl	být	k5eAaImAgInS
popsán	popsán	k2eAgInSc1d1
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1294	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>