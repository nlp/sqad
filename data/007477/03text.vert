<s>
Červený	červený	k2eAgInSc1d1	červený
kopec	kopec	k1gInSc1	kopec
(	(	kIx(	(
<g/>
311	[number]	k4	311
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vrch	vrch	k1gInSc4	vrch
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
<g/>
.	.	kIx.	.
</s>
<s>
Kopec	kopec	k1gInSc1	kopec
se	se	k3xPyFc4	se
zvedá	zvedat	k5eAaImIp3nS	zvedat
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
středu	střed	k1gInSc2	střed
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
řekou	řeka	k1gFnSc7	řeka
Svratkou	Svratka	k1gFnSc7	Svratka
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
čtvrtí	čtvrtit	k5eAaImIp3nS	čtvrtit
Štýřice	Štýřice	k1gFnSc1	Štýřice
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
Bohunicemi	Bohunice	k1gFnPc7	Bohunice
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
Svratky	Svratka	k1gFnSc2	Svratka
spadá	spadat	k5eAaPmIp3nS	spadat
velmi	velmi	k6eAd1	velmi
příkře	příkro	k6eAd1	příkro
<g/>
,	,	kIx,	,
s	s	k7c7	s
převýšením	převýšení	k1gNnSc7	převýšení
přes	přes	k7c4	přes
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
vrcholy	vrchol	k1gInPc4	vrchol
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc2d1	jižní
je	být	k5eAaImIp3nS	být
311	[number]	k4	311
a	a	k8xC	a
severní	severní	k2eAgFnSc1d1	severní
309	[number]	k4	309
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Kopec	kopec	k1gInSc1	kopec
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
převážně	převážně	k6eAd1	převážně
zahrádkami	zahrádka	k1gFnPc7	zahrádka
<g/>
,	,	kIx,	,
příkrý	příkrý	k2eAgInSc1d1	příkrý
severní	severní	k2eAgInSc1d1	severní
svah	svah	k1gInSc1	svah
je	být	k5eAaImIp3nS	být
zalesněný	zalesněný	k2eAgInSc1d1	zalesněný
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
skalnatý	skalnatý	k2eAgInSc1d1	skalnatý
(	(	kIx(	(
<g/>
Mahenova	Mahenův	k2eAgFnSc1d1	Mahenova
stráň	stráň	k1gFnSc1	stráň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jihovýchodní	jihovýchodní	k2eAgNnSc1d1	jihovýchodní
úbočí	úbočí	k1gNnSc1	úbočí
má	mít	k5eAaImIp3nS	mít
stepní	stepní	k2eAgInSc4d1	stepní
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
zástavba	zástavba	k1gFnSc1	zástavba
brněnské	brněnský	k2eAgFnSc2d1	brněnská
čtvrti	čtvrt	k1gFnSc2	čtvrt
Štýřice	Štýřice	k1gFnSc2	Štýřice
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Kamenné	kamenný	k2eAgFnSc2d1	kamenná
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
přiléhá	přiléhat	k5eAaImIp3nS	přiléhat
pobočný	pobočný	k2eAgInSc4d1	pobočný
vrchol	vrchol	k1gInSc4	vrchol
Červená	červený	k2eAgFnSc1d1	červená
skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
vysoký	vysoký	k2eAgInSc1d1	vysoký
asi	asi	k9	asi
260	[number]	k4	260
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
ní	on	k3xPp3gFnSc7	on
a	a	k8xC	a
vlastním	vlastní	k2eAgInSc7d1	vlastní
Červeným	červený	k2eAgInSc7d1	červený
kopcem	kopec	k1gInSc7	kopec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ulice	ulice	k1gFnSc2	ulice
Červený	červený	k2eAgInSc1d1	červený
kopec	kopec	k1gInSc1	kopec
se	s	k7c7	s
stejnojmennou	stejnojmenný	k2eAgFnSc7d1	stejnojmenná
LDN	LDN	kA	LDN
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
sem	sem	k6eAd1	sem
zajišťována	zajišťovat	k5eAaImNgFnS	zajišťovat
pouze	pouze	k6eAd1	pouze
speciální	speciální	k2eAgFnSc1d1	speciální
autobusovou	autobusový	k2eAgFnSc7d1	autobusová
linkou	linka	k1gFnSc7	linka
82	[number]	k4	82
<g/>
,	,	kIx,	,
s	s	k7c7	s
dvouhodinovým	dvouhodinový	k2eAgInSc7d1	dvouhodinový
intervalem	interval	k1gInSc7	interval
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
Červeného	Červeného	k2eAgInSc2d1	Červeného
kopce	kopec	k1gInSc2	kopec
vede	vést	k5eAaImIp3nS	vést
zelená	zelený	k2eAgFnSc1d1	zelená
turistická	turistický	k2eAgFnSc1d1	turistická
značka	značka	k1gFnSc1	značka
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgInSc1d1	červený
kopec	kopec	k1gInSc1	kopec
<g/>
,	,	kIx,	,
ev.	ev.	k?	ev.
č.	č.	k?	č.
66	[number]	k4	66
<g/>
,	,	kIx,	,
lokalita	lokalita	k1gFnSc1	lokalita
Brno-Štýřice	Brno-Štýřice	k1gFnSc1	Brno-Štýřice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Brno-město	Brnoěsta	k1gMnSc5	Brno-města
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
severozápadní	severozápadní	k2eAgFnSc4d1	severozápadní
část	část	k1gFnSc4	část
těžebního	těžební	k2eAgInSc2d1	těžební
prostoru	prostor	k1gInSc2	prostor
tzv.	tzv.	kA	tzv.
Kohnovy	Kohnův	k2eAgFnPc4d1	Kohnova
cihelny	cihelna	k1gFnPc4	cihelna
<g/>
,	,	kIx,	,
asi	asi	k9	asi
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
severně	severně	k6eAd1	severně
od	od	k7c2	od
ulice	ulice	k1gFnSc2	ulice
Jihlavská	jihlavský	k2eAgFnSc1d1	Jihlavská
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Spravuje	spravovat	k5eAaImIp3nS	spravovat
CHKO	CHKO	kA	CHKO
Moravský	moravský	k2eAgInSc1d1	moravský
kras	kras	k1gInSc1	kras
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
profil	profil	k1gInSc4	profil
s	s	k7c7	s
kvartérními	kvartérní	k2eAgInPc7d1	kvartérní
sedimenty	sediment	k1gInPc7	sediment
<g/>
,	,	kIx,	,
paleontologické	paleontologický	k2eAgNnSc1d1	paleontologické
naleziště	naleziště	k1gNnSc1	naleziště
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ojedinělý	ojedinělý	k2eAgInSc4d1	ojedinělý
čtvrtohorní	čtvrtohorní	k2eAgInSc4d1	čtvrtohorní
profil	profil	k1gInSc4	profil
se	s	k7c7	s
sprašemi	spraš	k1gFnPc7	spraš
a	a	k8xC	a
pohřbenými	pohřbený	k2eAgFnPc7d1	pohřbená
půdami	půda	k1gFnPc7	půda
<g/>
.	.	kIx.	.
</s>
<s>
Průběžná	průběžný	k2eAgFnSc1d1	průběžná
sedimentace	sedimentace	k1gFnSc1	sedimentace
spraší	spraš	k1gFnPc2	spraš
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
období	období	k1gNnSc4	období
čtvrtohor	čtvrtohory	k1gFnPc2	čtvrtohory
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zhruba	zhruba	k6eAd1	zhruba
dva	dva	k4xCgInPc4	dva
miliony	milion	k4xCgInPc4	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
památka	památka	k1gFnSc1	památka
cílem	cíl	k1gInSc7	cíl
řady	řada	k1gFnSc2	řada
exkurzí	exkurze	k1gFnPc2	exkurze
a	a	k8xC	a
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
konferencí	konference	k1gFnPc2	konference
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
přírodní	přírodní	k2eAgFnSc2d1	přírodní
památky	památka	k1gFnSc2	památka
náležel	náležet	k5eAaImAgInS	náležet
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ke	k	k7c3	k
katastrálnímu	katastrální	k2eAgInSc3d1	katastrální
území	území	k1gNnSc2	území
Bohunice	Bohunice	k1gFnPc1	Bohunice
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Červený	červený	k2eAgInSc4d1	červený
kopec	kopec	k1gInSc4	kopec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Fotografie	fotografie	k1gFnSc1	fotografie
kopce	kopec	k1gInSc2	kopec
MACKOVČIN	MACKOVČIN	kA	MACKOVČIN
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
;	;	kIx,	;
JATIOVÁ	JATIOVÁ	kA	JATIOVÁ
<g/>
,	,	kIx,	,
Matilda	Matilda	k1gFnSc1	Matilda
<g/>
;	;	kIx,	;
DEMEK	DEMEK	kA	DEMEK
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
,	,	kIx,	,
Slavík	Slavík	k1gMnSc1	Slavík
P.	P.	kA	P.
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Chráněná	chráněný	k2eAgNnPc4d1	chráněné
území	území	k1gNnPc4	území
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc4	svazek
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s>
Brněnsko	Brněnsko	k1gNnSc1	Brněnsko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Agentura	agentura	k1gFnSc1	agentura
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86064	[number]	k4	86064
<g/>
-	-	kIx~	-
<g/>
66	[number]	k4	66
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
238	[number]	k4	238
-	-	kIx~	-
239	[number]	k4	239
<g/>
.	.	kIx.	.
</s>
