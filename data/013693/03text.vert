<s>
Bez	bez	k7c2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
rodu	rod	k1gInSc6
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
tanečníkovi	tanečník	k1gMnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Bez	bez	k7c2
(	(	kIx(
<g/>
tanečník	tanečník	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Bez	bez	k7c2
Kvetoucí	kvetoucí	k2eAgFnSc2d1
bez	bez	k7c2
černý	černý	k1gMnSc1
(	(	kIx(
<g/>
Sambucus	Sambucus	k1gMnSc1
nigra	nigr	k1gMnSc2
<g/>
)	)	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
krytosemenné	krytosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Magnoliophyta	Magnoliophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
vyšší	vysoký	k2eAgFnPc1d2
dvouděložné	dvouděložná	k1gFnPc1
(	(	kIx(
<g/>
Rosopsida	Rosopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
štětkotvaré	štětkotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Dipsacales	Dipsacales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
kalinovité	kalinovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Viburnaceae	Viburnacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
bez	bez	k7c2
(	(	kIx(
<g/>
Sambucus	Sambucus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
L.	L.	kA
<g/>
,	,	kIx,
1753	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bez	bez	k1gInSc1
(	(	kIx(
<g/>
Sambucus	Sambucus	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
rod	rod	k1gInSc1
vyšších	vysoký	k2eAgFnPc2d2
dvouděložných	dvouděložný	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
patřící	patřící	k2eAgFnSc1d1
do	do	k7c2
čeledi	čeleď	k1gFnSc2
kalinovité	kalinovitý	k2eAgFnSc2d1
(	(	kIx(
<g/>
Viburnaceae	Viburnacea	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrnuje	zahrnovat	k5eAaImIp3nS
asi	asi	k9
40	#num#	k4
druhů	druh	k1gInPc2
dřevin	dřevina	k1gFnPc2
i	i	k8xC
bylin	bylina	k1gFnPc2
<g/>
,	,	kIx,
rozšířených	rozšířený	k2eAgFnPc2d1
zejména	zejména	k9
v	v	k7c6
mírném	mírný	k2eAgInSc6d1
pásu	pás	k1gInSc6
a	a	k8xC
subtropech	subtropy	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
rostou	růst	k5eAaImIp3nP
3	#num#	k4
druhy	druh	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
druhy	druh	k1gInPc4
a	a	k8xC
kultivary	kultivar	k1gInPc4
bezu	bez	k1gInSc2
jsou	být	k5eAaImIp3nP
pěstovány	pěstován	k2eAgFnPc4d1
jako	jako	k8xS,k8xC
okrasné	okrasný	k2eAgFnPc4d1
dřeviny	dřevina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Bezy	bez	k1gInPc1
jsou	být	k5eAaImIp3nP
opadavé	opadavý	k2eAgInPc1d1
keře	keř	k1gInPc1
<g/>
,	,	kIx,
stromy	strom	k1gInPc1
nebo	nebo	k8xC
i	i	k9
vytrvalé	vytrvalý	k2eAgFnPc1d1
byliny	bylina	k1gFnPc1
se	s	k7c7
vstřícnými	vstřícný	k2eAgInPc7d1
zpeřenými	zpeřený	k2eAgInPc7d1
listy	list	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kořenový	kořenový	k2eAgInSc1d1
systém	systém	k1gInSc1
je	být	k5eAaImIp3nS
bohatě	bohatě	k6eAd1
větvený	větvený	k2eAgMnSc1d1
a	a	k8xC
mělký	mělký	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listy	lista	k1gFnPc1
jsou	být	k5eAaImIp3nP
na	na	k7c6
okraji	okraj	k1gInSc6
pilovité	pilovitý	k2eAgNnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
palisty	palist	k1gInPc7
nebo	nebo	k8xC
s	s	k7c7
nektárii	nektárium	k1gNnPc7
vzniklými	vzniklý	k2eAgNnPc7d1
přeměnou	přeměna	k1gFnSc7
palistů	palist	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větve	větev	k1gFnPc1
jsou	být	k5eAaImIp3nP
tlusté	tlustý	k2eAgFnPc1d1
<g/>
,	,	kIx,
s	s	k7c7
nápadnými	nápadný	k2eAgFnPc7d1
lenticelami	lenticela	k1gFnPc7
a	a	k8xC
tlustou	tlustý	k2eAgFnSc7d1
dření	dřeň	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květenství	květenství	k1gNnSc1
jsou	být	k5eAaImIp3nP
koncové	koncový	k2eAgInPc1d1
vrcholičnaté	vrcholičnatý	k2eAgInPc1d1
laty	lat	k1gInPc1
nebo	nebo	k8xC
ploché	plochý	k2eAgInPc1d1
vrcholíky	vrcholík	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květy	Květa	k1gFnPc1
jsou	být	k5eAaImIp3nP
pravidelné	pravidelný	k2eAgFnPc1d1
<g/>
,	,	kIx,
drobné	drobný	k2eAgFnPc1d1
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
pětičetné	pětičetný	k2eAgFnPc1d1
<g/>
,	,	kIx,
bílé	bílý	k2eAgFnPc1d1
<g/>
,	,	kIx,
zelenavé	zelenavý	k2eAgFnPc1d1
nebo	nebo	k8xC
žlutavé	žlutavý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kališní	kališní	k2eAgInPc1d1
i	i	k8xC
korunní	korunní	k2eAgInPc1d1
lístky	lístek	k1gInPc1
jsou	být	k5eAaImIp3nP
srostlé	srostlý	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyčinek	tyčinka	k1gFnPc2
je	být	k5eAaImIp3nS
5	#num#	k4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
přirostlé	přirostlý	k2eAgFnPc1d1
při	při	k7c6
bázi	báze	k1gFnSc6
koruny	koruna	k1gFnSc2
a	a	k8xC
vyčnívají	vyčnívat	k5eAaImIp3nP
ven	ven	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Semeník	semeník	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
3	#num#	k4
až	až	k9
5	#num#	k4
komůrek	komůrka	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c4
každé	každý	k3xTgInPc4
je	být	k5eAaImIp3nS
jediné	jediný	k2eAgNnSc1d1
vajíčko	vajíčko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plodem	plod	k1gInSc7
je	být	k5eAaImIp3nS
dužnatá	dužnatý	k2eAgFnSc1d1
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
černá	černý	k2eAgFnSc1d1
nebo	nebo	k8xC
červená	červený	k2eAgFnSc1d1
peckovice	peckovice	k1gFnSc1
připomínající	připomínající	k2eAgFnSc4d1
bobuli	bobule	k1gFnSc4
a	a	k8xC
obsahující	obsahující	k2eAgInSc1d1
3	#num#	k4
až	až	k8xS
5	#num#	k4
semen	semeno	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Rod	rod	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
asi	asi	k9
40	#num#	k4
druhů	druh	k1gInPc2
rozšířených	rozšířený	k2eAgInPc2d1
v	v	k7c6
mírném	mírný	k2eAgInSc6d1
a	a	k8xC
subtropickém	subtropický	k2eAgInSc6d1
pásu	pás	k1gInSc6
obou	dva	k4xCgFnPc2
polokoulí	polokoule	k1gFnPc2
a	a	k8xC
v	v	k7c6
některých	některý	k3yIgFnPc6
horských	horský	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
tropů	trop	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
zastoupen	zastoupit	k5eAaPmNgMnS
v	v	k7c6
subsaharské	subsaharský	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ČR	ČR	kA
je	být	k5eAaImIp3nS
zastoupen	zastoupit	k5eAaPmNgInS
3	#num#	k4
původními	původní	k2eAgInPc7d1
druhy	druh	k1gInPc7
<g/>
:	:	kIx,
bez	bez	k1gInSc1
černý	černý	k2eAgInSc1d1
(	(	kIx(
<g/>
Sambucus	Sambucus	k1gInSc1
nigra	nigr	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bez	bez	k7c2
chebdí	chebdí	k1gNnSc2
(	(	kIx(
<g/>
S.	S.	kA
ebulus	ebulus	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
bez	bez	k1gInSc4
červený	červený	k2eAgInSc4d1
(	(	kIx(
<g/>
S.	S.	kA
racemosa	racemosa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Mimo	mimo	k7c4
tyto	tento	k3xDgFnPc4
3	#num#	k4
druhy	druh	k1gInPc4
roste	růst	k5eAaImIp3nS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
ještě	ještě	k9
bez	bez	k1gInSc1
sibiřský	sibiřský	k2eAgInSc1d1
(	(	kIx(
<g/>
S.	S.	kA
sibirica	sibirica	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rozšířený	rozšířený	k2eAgInSc1d1
ve	v	k7c6
středním	střední	k2eAgNnSc6d1
a	a	k8xC
východním	východní	k2eAgNnSc6d1
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Detail	detail	k1gInSc1
květů	květ	k1gInPc2
bezu	bez	k1gInSc2
černého	černý	k2eAgInSc2d1
</s>
<s>
Kvetoucí	kvetoucí	k2eAgMnSc1d1
bez	bez	k7c2
chebdí	chebdí	k1gNnSc2
</s>
<s>
Zralé	zralý	k2eAgInPc1d1
plody	plod	k1gInPc1
bezu	bez	k1gInSc2
červeného	červený	k2eAgInSc2d1
</s>
<s>
Zimní	zimní	k2eAgFnSc1d1
větévka	větévka	k1gFnSc1
bezu	bez	k1gInSc2
červeného	červený	k2eAgInSc2d1
</s>
<s>
Australský	australský	k2eAgInSc4d1
bez	bez	k1gInSc4
Sambucus	Sambucus	k1gMnSc1
gaudichaudiana	gaudichaudian	k1gMnSc2
</s>
<s>
Zástupci	zástupce	k1gMnPc1
</s>
<s>
bez	bez	k7c2
chebdí	chebdí	k1gNnSc2
(	(	kIx(
<g/>
'	'	kIx"
<g/>
Sambucus	Sambucus	k1gMnSc1
ebulus	ebulus	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
bez	bez	k1gInSc1
černoplodý	černoplodý	k2eAgInSc1d1
(	(	kIx(
<g/>
Sambucus	Sambucus	k1gInSc1
melanocarpa	melanocarp	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
bez	bez	k1gInSc1
černý	černý	k2eAgInSc1d1
(	(	kIx(
<g/>
Sambucus	Sambucus	k1gInSc1
nigra	nigr	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
bez	bez	k1gInSc1
červený	červený	k2eAgInSc1d1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
<g/>
.	.	kIx.
bez	bez	k1gInSc1
hroznatý	hroznatý	k2eAgInSc1d1
(	(	kIx(
<g/>
Sambucus	Sambucus	k1gInSc1
racemosa	racemosa	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
bez	bez	k1gInSc1
hladkoplodý	hladkoplodý	k2eAgInSc1d1
(	(	kIx(
<g/>
Sambucus	Sambucus	k1gInSc1
callicarpa	callicarp	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
bez	bez	k1gInSc1
kamčatský	kamčatský	k2eAgInSc1d1
(	(	kIx(
<g/>
Sambucus	Sambucus	k1gInSc1
kamtschatica	kamtschatic	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
bez	bez	k1gInSc1
kanadský	kanadský	k2eAgInSc1d1
(	(	kIx(
<g/>
Sambucus	Sambucus	k1gInSc1
canadensis	canadensis	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
bez	bez	k1gInSc1
pýřitý	pýřitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Sambucus	Sambucus	k1gInSc1
pubens	pubens	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Jako	jako	k9
bez	bez	k7c2
se	se	k3xPyFc4
někdy	někdy	k6eAd1
lidově	lidově	k6eAd1
označuje	označovat	k5eAaImIp3nS
i	i	k9
šeřík	šeřík	k1gInSc1
obecný	obecný	k2eAgInSc1d1
(	(	kIx(
<g/>
Syringa	syringa	k1gFnSc1
vulgaris	vulgaris	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
však	však	k9
s	s	k7c7
bezem	bez	k1gInSc7
není	být	k5eNaImIp3nS
nijak	nijak	k6eAd1
příbuzný	příbuzný	k2eAgMnSc1d1
(	(	kIx(
<g/>
patří	patřit	k5eAaImIp3nS
do	do	k7c2
řádu	řád	k1gInSc2
hluchavkotvarých	hluchavkotvarý	k2eAgFnPc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Jako	jako	k8xC,k8xS
okrasné	okrasný	k2eAgFnPc1d1
dřeviny	dřevina	k1gFnPc1
jsou	být	k5eAaImIp3nP
u	u	k7c2
nás	my	k3xPp1nPc2
nejčastěji	často	k6eAd3
pěstovány	pěstován	k2eAgInPc1d1
různé	různý	k2eAgInPc1d1
kultivary	kultivar	k1gInPc1
bezu	bez	k1gInSc2
černého	černý	k2eAgInSc2d1
<g/>
,	,	kIx,
lišící	lišící	k2eAgFnSc2d1
se	se	k3xPyFc4
především	především	k6eAd1
tvarem	tvar	k1gInSc7
a	a	k8xC
zbarvením	zbarvení	k1gNnSc7
listů	list	k1gInPc2
a	a	k8xC
celkovým	celkový	k2eAgInSc7d1
vzrůstem	vzrůst	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bez	bez	k1gInSc1
černý	černý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
využíván	využívat	k5eAaPmNgInS,k5eAaImNgInS
v	v	k7c6
lidovém	lidový	k2eAgNnSc6d1
léčitelství	léčitelství	k1gNnSc6
a	a	k8xC
plody	plod	k1gInPc4
jsou	být	k5eAaImIp3nP
různým	různý	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
zpracovávány	zpracováván	k2eAgInPc4d1
na	na	k7c4
šťávy	šťáva	k1gFnPc4
<g/>
,	,	kIx,
džemy	džem	k1gInPc1
apod.	apod.	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
KOBLÍŽEK	koblížek	k1gInSc1
<g/>
,	,	kIx,
J.	J.	kA
Jehličnaté	jehličnatý	k2eAgFnPc4d1
a	a	k8xC
listnaté	listnatý	k2eAgFnPc4d1
dřeviny	dřevina	k1gFnPc4
našich	náš	k3xOp1gFnPc2
zahrad	zahrada	k1gFnPc2
a	a	k8xC
parků	park	k1gInPc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tišnov	Tišnov	k1gInSc1
<g/>
:	:	kIx,
Sursum	Sursum	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7323	#num#	k4
<g/>
-	-	kIx~
<g/>
117	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
SLAVÍK	Slavík	k1gMnSc1
<g/>
,	,	kIx,
Bohumil	Bohumil	k1gMnSc1
(	(	kIx(
<g/>
editor	editor	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květena	květena	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
590	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Flora	Flora	k1gFnSc1
of	of	k?
China	China	k1gFnSc1
<g/>
:	:	kIx,
Sambucus	Sambucus	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Flora	Flora	k1gFnSc1
Europaea	Europaea	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Royal	Royal	k1gInSc1
Botanic	Botanice	k1gFnPc2
Garden	Gardno	k1gNnPc2
Edinburgh	Edinburgh	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Dendrologie	dendrologie	k1gFnSc1
online	onlinout	k5eAaPmIp3nS
<g/>
:	:	kIx,
Sambucus	Sambucus	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
bez	bez	k7c2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
bez	bez	k7c2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Taxon	taxon	k1gInSc1
Sambucus	Sambucus	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Biologie	biologie	k1gFnPc1
|	|	kIx~
Rostliny	rostlina	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4136385-1	4136385-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85117016	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85117016	#num#	k4
</s>
