<s>
Marianne	Mariannout	k5eAaPmIp3nS	Mariannout
je	on	k3xPp3gFnPc4	on
alegorická	alegorický	k2eAgFnSc1d1	alegorická
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
zpodobení	zpodobení	k1gNnPc1	zpodobení
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Marianne	Mariannout	k5eAaPmIp3nS	Mariannout
ztělesňuje	ztělesňovat	k5eAaImIp3nS	ztělesňovat
Francouzskou	francouzský	k2eAgFnSc4d1	francouzská
Republiku	republika	k1gFnSc4	republika
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
ženské	ženský	k2eAgFnSc6d1	ženská
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
hlavu	hlava	k1gFnSc4	hlava
ozdobenou	ozdobený	k2eAgFnSc7d1	ozdobená
frygickou	frygický	k2eAgFnSc7d1	frygická
čapkou	čapka	k1gFnSc7	čapka
<g/>
.	.	kIx.	.
</s>
<s>
Dává	dávat	k5eAaImIp3nS	dávat
tak	tak	k9	tak
současně	současně	k6eAd1	současně
podobu	podoba	k1gFnSc4	podoba
i	i	k9	i
třem	tři	k4xCgFnPc3	tři
základním	základní	k2eAgFnPc3d1	základní
hodnotám	hodnota	k1gFnPc3	hodnota
republikánské	republikánský	k2eAgFnSc2d1	republikánská
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vyjádřeny	vyjádřen	k2eAgFnPc1d1	vyjádřena
heslem	heslo	k1gNnSc7	heslo
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
Volnost	volnost	k1gFnSc1	volnost
<g/>
,	,	kIx,	,
rovnost	rovnost	k1gFnSc1	rovnost
<g/>
,	,	kIx,	,
bratrství	bratrství	k1gNnSc1	bratrství
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Liberté	Libertý	k2eAgFnPc1d1	Libertý
<g/>
,	,	kIx,	,
Égalité	Égalitý	k2eAgFnPc1d1	Égalitý
<g/>
,	,	kIx,	,
Fraternité	Fraternitý	k2eAgFnPc1d1	Fraternitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řadě	řada	k1gFnSc3	řada
slavných	slavný	k2eAgFnPc2d1	slavná
Francouzek	Francouzka	k1gFnPc2	Francouzka
byla	být	k5eAaImAgFnS	být
prokázána	prokázán	k2eAgFnSc1d1	prokázána
čest	čest	k1gFnSc1	čest
a	a	k8xC	a
vysloveno	vysloven	k2eAgNnSc4d1	vysloveno
uznání	uznání	k1gNnSc4	uznání
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
vybrány	vybrán	k2eAgFnPc1d1	vybrána
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
propůjčily	propůjčit	k5eAaPmAgFnP	propůjčit
svou	svůj	k3xOyFgFnSc4	svůj
tvář	tvář	k1gFnSc4	tvář
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
nové	nový	k2eAgFnSc2d1	nová
podoby	podoba	k1gFnSc2	podoba
Marianne	Mariann	k1gInSc5	Mariann
<g/>
:	:	kIx,	:
1968	[number]	k4	1968
:	:	kIx,	:
Brigitte	Brigitte	k1gFnSc1	Brigitte
Bardot	Bardot	k1gInSc1	Bardot
1978	[number]	k4	1978
:	:	kIx,	:
Mireille	Mireillo	k1gNnSc6	Mireillo
Mathieu	Mathieus	k1gInSc2	Mathieus
1985	[number]	k4	1985
:	:	kIx,	:
Catherine	Catherin	k1gInSc5	Catherin
Deneuve	Deneuev	k1gFnPc4	Deneuev
1989	[number]	k4	1989
:	:	kIx,	:
Inè	Inè	k1gFnPc2	Inè
de	de	k?	de
la	la	k1gNnSc6	la
Fressange	Fressang	k1gInSc2	Fressang
2000	[number]	k4	2000
:	:	kIx,	:
Laetitia	Laetitia	k1gFnSc1	Laetitia
Casta	Casta	k1gFnSc1	Casta
2003	[number]	k4	2003
:	:	kIx,	:
Évelyne	Évelyn	k1gInSc5	Évelyn
Thomas	Thomas	k1gMnSc1	Thomas
Samira	Samir	k1gMnSc4	Samir
Bellil	Bellil	k1gMnSc4	Bellil
</s>
<s>
Tvář	tvář	k1gFnSc1	tvář
Marianne	Mariann	k1gInSc5	Mariann
je	být	k5eAaImIp3nS	být
zobrazena	zobrazen	k2eAgFnSc1d1	zobrazena
na	na	k7c6	na
francouzských	francouzský	k2eAgInPc6d1	francouzský
euromincích	eurominec	k1gInPc6	eurominec
<g/>
.	.	kIx.	.
</s>
<s>
Symboly	symbol	k1gInPc1	symbol
Marianne	Mariann	k1gInSc5	Mariann
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
antiky	antika	k1gFnSc2	antika
<g/>
.	.	kIx.	.
</s>
