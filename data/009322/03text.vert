<p>
<s>
Doložka	doložka	k1gFnSc1	doložka
národního	národní	k2eAgNnSc2d1	národní
zacházení	zacházení	k1gNnSc2	zacházení
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obsažena	obsáhnout	k5eAaPmNgFnS	obsáhnout
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
smlouvě	smlouva	k1gFnSc6	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
ustanovení	ustanovení	k1gNnSc6	ustanovení
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
si	se	k3xPyFc3	se
smluvní	smluvní	k2eAgFnPc1d1	smluvní
strany	strana	k1gFnPc1	strana
dohodnou	dohodnout	k5eAaPmIp3nP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
poskytnout	poskytnout	k5eAaPmF	poskytnout
takové	takový	k3xDgNnSc4	takový
zacházení	zacházení	k1gNnSc4	zacházení
cizímu	cizí	k2eAgMnSc3d1	cizí
obchodnímu	obchodní	k2eAgMnSc3d1	obchodní
partnerovi	partner	k1gMnSc3	partner
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
zaručeno	zaručit	k5eAaPmNgNnS	zaručit
domácímu	domácí	k2eAgMnSc3d1	domácí
obchodnímu	obchodní	k2eAgMnSc3d1	obchodní
partnerovi	partner	k1gMnSc3	partner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Princip	princip	k1gInSc1	princip
nediskriminace	nediskriminace	k1gFnSc2	nediskriminace
ve	v	k7c6	v
WTO	WTO	kA	WTO
==	==	k?	==
</s>
</p>
<p>
<s>
Pomocí	pomocí	k7c2	pomocí
doložky	doložka	k1gFnSc2	doložka
národního	národní	k2eAgNnSc2d1	národní
zacházení	zacházení	k1gNnSc2	zacházení
je	být	k5eAaImIp3nS	být
uplatňován	uplatňován	k2eAgInSc1d1	uplatňován
nediskriminační	diskriminační	k2eNgInSc1d1	nediskriminační
princip	princip	k1gInSc1	princip
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
vyžadován	vyžadovat	k5eAaImNgInS	vyžadovat
Světovou	světový	k2eAgFnSc7d1	světová
obchodní	obchodní	k2eAgFnSc7d1	obchodní
organizací	organizace	k1gFnSc7	organizace
(	(	kIx(	(
<g/>
WTO	WTO	kA	WTO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
tak	tak	k9	tak
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
doložkou	doložka	k1gFnSc7	doložka
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
výhod	výhoda	k1gFnPc2	výhoda
k	k	k7c3	k
odstraňování	odstraňování	k1gNnSc3	odstraňování
bariér	bariéra	k1gFnPc2	bariéra
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
obchodu	obchod	k1gInSc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
princip	princip	k1gInSc1	princip
je	být	k5eAaImIp3nS	být
zakotven	zakotvit	k5eAaPmNgInS	zakotvit
v	v	k7c6	v
GATT	GATT	kA	GATT
<g/>
,	,	kIx,	,
GATS	GATS	kA	GATS
i	i	k8xC	i
TRIPS	TRIPS	kA	TRIPS
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
dohodě	dohoda	k1gFnSc6	dohoda
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
trochu	trochu	k6eAd1	trochu
jiné	jiný	k2eAgFnSc6d1	jiná
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
doložka	doložka	k1gFnSc1	doložka
národního	národní	k2eAgNnSc2d1	národní
zacházení	zacházení	k1gNnSc2	zacházení
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
na	na	k7c4	na
zboží	zboží	k1gNnSc4	zboží
<g/>
,	,	kIx,	,
služby	služba	k1gFnPc4	služba
i	i	k8xC	i
duševní	duševní	k2eAgNnSc4d1	duševní
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Doložka	doložka	k1gFnSc1	doložka
národního	národní	k2eAgNnSc2d1	národní
zacházení	zacházení	k1gNnSc2	zacházení
se	se	k3xPyFc4	se
použije	použít	k5eAaPmIp3nS	použít
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
až	až	k9	až
zboží	zboží	k1gNnSc1	zboží
<g/>
,	,	kIx,	,
služba	služba	k1gFnSc1	služba
či	či	k8xC	či
položka	položka	k1gFnSc1	položka
duševního	duševní	k2eAgNnSc2d1	duševní
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
na	na	k7c4	na
daný	daný	k2eAgInSc4d1	daný
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
uvalení	uvalení	k1gNnPc1	uvalení
cla	clo	k1gNnSc2	clo
na	na	k7c4	na
dovoz	dovoz	k1gInSc4	dovoz
není	být	k5eNaImIp3nS	být
porušením	porušení	k1gNnSc7	porušení
doložky	doložka	k1gFnSc2	doložka
národního	národní	k2eAgNnSc2d1	národní
zacházení	zacházení	k1gNnSc2	zacházení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Národní	národní	k2eAgInSc1d1	národní
režim	režim	k1gInSc1	režim
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
doložka	doložka	k1gFnSc1	doložka
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
výhod	výhoda	k1gFnPc2	výhoda
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
relativní	relativní	k2eAgInPc4d1	relativní
režimy	režim	k1gInPc4	režim
zacházení	zacházení	k1gNnSc2	zacházení
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1	relativní
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
výhody	výhoda	k1gFnPc1	výhoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
budou	být	k5eAaImBp3nP	být
poskytnuty	poskytnout	k5eAaPmNgFnP	poskytnout
členským	členský	k2eAgFnPc3d1	členská
státům	stát	k1gInPc3	stát
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
rozsahu	rozsah	k1gInSc6	rozsah
poskytování	poskytování	k1gNnSc4	poskytování
výhod	výhoda	k1gFnPc2	výhoda
v	v	k7c6	v
domácím	domácí	k2eAgInSc6d1	domácí
státu	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
národního	národní	k2eAgNnSc2d1	národní
zacházení	zacházení	k1gNnSc2	zacházení
rozsah	rozsah	k1gInSc1	rozsah
poskytnutých	poskytnutý	k2eAgFnPc2d1	poskytnutá
výhod	výhoda	k1gFnPc2	výhoda
pro	pro	k7c4	pro
vstupující	vstupující	k2eAgInSc4d1	vstupující
stát	stát	k1gInSc4	stát
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
zacházení	zacházení	k1gNnSc2	zacházení
v	v	k7c6	v
domácím	domácí	k2eAgInSc6d1	domácí
státu	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cílem	cíl	k1gInSc7	cíl
doložky	doložka	k1gFnSc2	doložka
národního	národní	k2eAgNnSc2d1	národní
zacházení	zacházení	k1gNnSc2	zacházení
je	být	k5eAaImIp3nS	být
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
daně	daň	k1gFnPc1	daň
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgInPc1d1	jiný
předpisy	předpis	k1gInPc1	předpis
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
jako	jako	k8xC	jako
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
celní	celní	k2eAgNnPc4d1	celní
opatření	opatření	k1gNnPc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
doložky	doložka	k1gFnSc2	doložka
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k9	jako
doložky	doložka	k1gFnSc2	doložka
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc1	pojem
obdobný	obdobný	k2eAgInSc1d1	obdobný
výrobek	výrobek	k1gInSc1	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Doložky	doložka	k1gFnPc1	doložka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
diskriminaci	diskriminace	k1gFnSc4	diskriminace
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
obchodě	obchod	k1gInSc6	obchod
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
použijí	použít	k5eAaPmIp3nP	použít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
stejné	stejný	k2eAgInPc4d1	stejný
nebo	nebo	k8xC	nebo
obdobné	obdobný	k2eAgInPc4d1	obdobný
výrobky	výrobek	k1gInPc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
problematický	problematický	k2eAgInSc1d1	problematický
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
několika	několik	k4yIc2	několik
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
aplikaci	aplikace	k1gFnSc6	aplikace
národního	národní	k2eAgNnSc2d1	národní
zacházení	zacházení	k1gNnSc2	zacházení
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
identifikovat	identifikovat	k5eAaBmF	identifikovat
3	[number]	k4	3
základní	základní	k2eAgInPc4d1	základní
kroky	krok	k1gInPc4	krok
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
o	o	k7c4	o
použití	použití	k1gNnSc4	použití
doložky	doložka	k1gFnSc2	doložka
národního	národní	k2eAgNnSc2d1	národní
zacházení	zacházení	k1gNnSc2	zacházení
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
je	být	k5eAaImIp3nS	být
teda	teda	k?	teda
nutné	nutný	k2eAgNnSc1d1	nutné
najít	najít	k5eAaPmF	najít
obdobný	obdobný	k2eAgInSc4d1	obdobný
výrobek	výrobek	k1gInSc4	výrobek
<g/>
,	,	kIx,	,
službu	služba	k1gFnSc4	služba
či	či	k8xC	či
položku	položka	k1gFnSc4	položka
duševního	duševní	k2eAgNnSc2d1	duševní
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgMnSc7	který
bude	být	k5eAaImBp3nS	být
srovnáváno	srovnáván	k2eAgNnSc1d1	srovnáváno
přiznané	přiznaný	k2eAgNnSc1d1	přiznané
zacházení	zacházení	k1gNnSc1	zacházení
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
krokem	krok	k1gInSc7	krok
je	být	k5eAaImIp3nS	být
identifikace	identifikace	k1gFnSc1	identifikace
méně	málo	k6eAd2	málo
výhodného	výhodný	k2eAgNnSc2d1	výhodné
zacházení	zacházení	k1gNnSc2	zacházení
uděleného	udělený	k2eAgInSc2d1	udělený
zahraničnímu	zahraniční	k2eAgInSc3d1	zahraniční
státu	stát	k1gInSc3	stát
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bude	být	k5eAaImBp3nS	být
srovnáno	srovnat	k5eAaPmNgNnS	srovnat
se	s	k7c7	s
zacházením	zacházení	k1gNnSc7	zacházení
v	v	k7c6	v
hostitelském	hostitelský	k2eAgInSc6d1	hostitelský
státu	stát	k1gInSc6	stát
u	u	k7c2	u
obdobného	obdobný	k2eAgInSc2d1	obdobný
výrobku	výrobek	k1gInSc2	výrobek
<g/>
,	,	kIx,	,
služby	služba	k1gFnSc2	služba
či	či	k8xC	či
položky	položka	k1gFnPc4	položka
duševního	duševní	k2eAgNnSc2d1	duševní
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
krokem	krok	k1gInSc7	krok
je	být	k5eAaImIp3nS	být
zvážení	zvážení	k1gNnSc1	zvážení
výjimek	výjimka	k1gFnPc2	výjimka
z	z	k7c2	z
národního	národní	k2eAgNnSc2d1	národní
zacházení	zacházení	k1gNnSc2	zacházení
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
GATT	GATT	kA	GATT
<g/>
,	,	kIx,	,
GATS	GATS	kA	GATS
a	a	k8xC	a
TRIPS	TRIPS	kA	TRIPS
a	a	k8xC	a
uznání	uznání	k1gNnSc4	uznání
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
měl	mít	k5eAaImAgMnS	mít
či	či	k8xC	či
neměl	mít	k5eNaImAgMnS	mít
hostitelský	hostitelský	k2eAgMnSc1d1	hostitelský
stát	stát	k5eAaPmF	stát
oprávněné	oprávněný	k2eAgInPc4d1	oprávněný
důvody	důvod	k1gInPc4	důvod
k	k	k7c3	k
přiznání	přiznání	k1gNnSc3	přiznání
rozdílného	rozdílný	k2eAgNnSc2d1	rozdílné
zacházení	zacházení	k1gNnSc2	zacházení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
GATT	GATT	kA	GATT
===	===	k?	===
</s>
</p>
<p>
<s>
Princip	princip	k1gInSc1	princip
národního	národní	k2eAgNnSc2d1	národní
zacházení	zacházení	k1gNnSc2	zacházení
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
GATT	GATT	kA	GATT
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
článku	článek	k1gInSc6	článek
III	III	kA	III
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zrovnoprávňuje	zrovnoprávňovat	k5eAaImIp3nS	zrovnoprávňovat
zboží	zboží	k1gNnSc4	zboží
pocházející	pocházející	k2eAgNnSc4d1	pocházející
z	z	k7c2	z
členského	členský	k2eAgInSc2d1	členský
státu	stát	k1gInSc2	stát
se	s	k7c7	s
zbožím	zboží	k1gNnSc7	zboží
vyrobeným	vyrobený	k2eAgNnSc7d1	vyrobené
na	na	k7c6	na
domácím	domácí	k2eAgNnSc6d1	domácí
území	území	k1gNnSc6	území
a	a	k8xC	a
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
mu	on	k3xPp3gNnSc3	on
tak	tak	k6eAd1	tak
stejné	stejný	k2eAgNnSc4d1	stejné
zacházení	zacházení	k1gNnSc4	zacházení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
článek	článek	k1gInSc1	článek
III	III	kA	III
odst	odst	k1gInSc1	odst
<g/>
.	.	kIx.	.
1	[number]	k4	1
GATT	GATT	kA	GATT
1994	[number]	k4	1994
<g/>
"	"	kIx"	"
<g/>
Smluvní	smluvní	k2eAgFnSc2d1	smluvní
strany	strana	k1gFnSc2	strana
uznávají	uznávat	k5eAaImIp3nP	uznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
daně	daň	k1gFnPc1	daň
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc1d1	ostatní
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
dávky	dávka	k1gFnPc1	dávka
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
zákony	zákon	k1gInPc4	zákon
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
předpisy	předpis	k1gInPc4	předpis
<g/>
,	,	kIx,	,
ovlivňující	ovlivňující	k2eAgInSc4d1	ovlivňující
prodej	prodej	k1gInSc4	prodej
<g/>
,	,	kIx,	,
nabízení	nabízení	k1gNnSc4	nabízení
na	na	k7c4	na
prodej	prodej	k1gInSc4	prodej
<g/>
,	,	kIx,	,
nákup	nákup	k1gInSc4	nákup
<g/>
,	,	kIx,	,
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
distribuci	distribuce	k1gFnSc4	distribuce
nebo	nebo	k8xC	nebo
používání	používání	k1gNnSc4	používání
výrobků	výrobek	k1gInPc2	výrobek
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
množstevní	množstevní	k2eAgFnSc2d1	množstevní
úpravy	úprava	k1gFnSc2	úprava
<g/>
,	,	kIx,	,
předpisující	předpisující	k2eAgNnSc1d1	předpisující
míchání	míchání	k1gNnSc1	míchání
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc1	zpracování
nebo	nebo	k8xC	nebo
používání	používání	k1gNnSc1	používání
výrobků	výrobek	k1gInPc2	výrobek
v	v	k7c6	v
určitých	určitý	k2eAgNnPc6d1	určité
množstvích	množství	k1gNnPc6	množství
nebo	nebo	k8xC	nebo
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
poměrech	poměr	k1gInPc6	poměr
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
být	být	k5eAaImF	být
uplatňovány	uplatňován	k2eAgFnPc1d1	uplatňována
na	na	k7c4	na
dovážené	dovážený	k2eAgInPc4d1	dovážený
či	či	k8xC	či
domácí	domácí	k2eAgInPc4d1	domácí
výrobky	výrobek	k1gInPc4	výrobek
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
ochrana	ochrana	k1gFnSc1	ochrana
domácí	domácí	k2eAgFnSc1d1	domácí
výrobě	výroba	k1gFnSc3	výroba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
odstavci	odstavec	k1gInSc6	odstavec
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádná	žádný	k3yNgFnSc1	žádný
smluvní	smluvní	k2eAgFnSc1d1	smluvní
strana	strana	k1gFnSc1	strana
neuloží	uložit	k5eNaPmIp3nS	uložit
přímo	přímo	k6eAd1	přímo
či	či	k8xC	či
nepřímo	přímo	k6eNd1	přímo
vyšší	vysoký	k2eAgFnPc1d2	vyšší
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
daně	daň	k1gFnPc1	daň
ani	ani	k8xC	ani
jiné	jiný	k2eAgFnPc1d1	jiná
dávky	dávka	k1gFnPc1	dávka
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
podléhá	podléhat	k5eAaImIp3nS	podléhat
obdobné	obdobný	k2eAgNnSc4d1	obdobné
domácí	domácí	k2eAgNnSc4d1	domácí
zboží	zboží	k1gNnSc4	zboží
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
odstavci	odstavec	k1gInSc3	odstavec
2	[number]	k4	2
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
odstavec	odstavec	k1gInSc1	odstavec
3	[number]	k4	3
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stanoví	stanovit	k5eAaPmIp3nS	stanovit
výjimku	výjimka	k1gFnSc4	výjimka
z	z	k7c2	z
použití	použití	k1gNnSc2	použití
odstavce	odstavec	k1gInSc2	odstavec
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
článek	článek	k1gInSc1	článek
III	III	kA	III
odst	odst	k1gInSc1	odst
<g/>
.	.	kIx.	.
3	[number]	k4	3
GATT	GATT	kA	GATT
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
kterékoli	kterýkoli	k3yIgFnSc2	kterýkoli
existující	existující	k2eAgFnSc2d1	existující
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
daně	daň	k1gFnSc2	daň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
ustanoveními	ustanovení	k1gNnPc7	ustanovení
odstavce	odstavec	k1gInSc2	odstavec
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
je	být	k5eAaImIp3nS	být
výslovně	výslovně	k6eAd1	výslovně
povolena	povolit	k5eAaPmNgFnS	povolit
v	v	k7c6	v
obchodní	obchodní	k2eAgFnSc6d1	obchodní
dohodě	dohoda	k1gFnSc6	dohoda
<g/>
,	,	kIx,	,
platné	platný	k2eAgFnSc6d1	platná
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
dovozní	dovozní	k2eAgFnSc2d1	dovozní
clo	clo	k1gNnSc4	clo
na	na	k7c4	na
zdaněný	zdaněný	k2eAgInSc4d1	zdaněný
výrobek	výrobek	k1gInSc4	výrobek
je	být	k5eAaImIp3nS	být
vázáno	vázat	k5eAaImNgNnS	vázat
proti	proti	k7c3	proti
zvýšení	zvýšení	k1gNnSc3	zvýšení
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
smluvní	smluvní	k2eAgFnSc1d1	smluvní
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
daň	daň	k1gFnSc1	daň
ukládá	ukládat	k5eAaImIp3nS	ukládat
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nS	muset
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
daň	daň	k1gFnSc4	daň
použít	použít	k5eAaPmF	použít
ustanovení	ustanovení	k1gNnSc4	ustanovení
odstavce	odstavec	k1gInSc2	odstavec
2	[number]	k4	2
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebude	být	k5eNaImBp3nS	být
vyvázána	vyvázat	k5eAaPmNgFnS	vyvázat
ze	z	k7c2	z
závazků	závazek	k1gInPc2	závazek
plynoucích	plynoucí	k2eAgInPc2d1	plynoucí
z	z	k7c2	z
takové	takový	k3xDgFnSc2	takový
dohody	dohoda	k1gFnSc2	dohoda
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc2	on
bylo	být	k5eAaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
zvýšit	zvýšit	k5eAaPmF	zvýšit
dovozní	dovozní	k2eAgNnSc4d1	dovozní
clo	clo	k1gNnSc4	clo
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
potřebném	potřebné	k1gNnSc6	potřebné
k	k	k7c3	k
vyrovnání	vyrovnání	k1gNnSc3	vyrovnání
odstraněné	odstraněný	k2eAgFnSc2d1	odstraněná
ochranné	ochranný	k2eAgFnSc2d1	ochranná
části	část	k1gFnSc2	část
této	tento	k3xDgFnSc2	tento
daně	daň	k1gFnSc2	daň
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
odstavci	odstavec	k1gInSc6	odstavec
4	[number]	k4	4
článku	článek	k1gInSc2	článek
III	III	kA	III
je	být	k5eAaImIp3nS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
zacházeno	zacházen	k2eAgNnSc1d1	zacházeno
s	s	k7c7	s
výrobky	výrobek	k1gInPc7	výrobek
pocházejícími	pocházející	k2eAgInPc7d1	pocházející
z	z	k7c2	z
území	území	k1gNnSc2	území
kterékoli	kterýkoli	k3yIgFnSc2	kterýkoli
smluvní	smluvní	k2eAgFnSc2d1	smluvní
strany	strana	k1gFnSc2	strana
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xS	jako
s	s	k7c7	s
domácími	domácí	k2eAgInPc7d1	domácí
výrobky	výrobek	k1gInPc7	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
je	být	k5eAaImIp3nS	být
stejné	stejný	k2eAgNnSc1d1	stejné
zacházení	zacházení	k1gNnSc1	zacházení
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
při	při	k7c6	při
prodeji	prodej	k1gInSc6	prodej
<g/>
,	,	kIx,	,
nabízení	nabízení	k1gNnSc6	nabízení
na	na	k7c4	na
prodej	prodej	k1gInSc4	prodej
<g/>
,	,	kIx,	,
nákup	nákup	k1gInSc4	nákup
<g/>
,	,	kIx,	,
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
distribuci	distribuce	k1gFnSc4	distribuce
nebo	nebo	k8xC	nebo
používání	používání	k1gNnSc4	používání
na	na	k7c6	na
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odstavec	odstavec	k1gInSc1	odstavec
5	[number]	k4	5
článku	článek	k1gInSc2	článek
III	III	kA	III
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
míchání	míchání	k1gNnSc1	míchání
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc1	zpracování
a	a	k8xC	a
používání	používání	k1gNnSc1	používání
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Výslovně	výslovně	k6eAd1	výslovně
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
stanovování	stanovování	k1gNnSc4	stanovování
jakýchkoli	jakýkoli	k3yIgInPc2	jakýkoli
poměrů	poměr	k1gInPc2	poměr
či	či	k8xC	či
množstevních	množstevní	k2eAgInPc2d1	množstevní
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
nařizovaly	nařizovat	k5eAaImAgInP	nařizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
určitý	určitý	k2eAgInSc1d1	určitý
poměr	poměr	k1gInSc1	poměr
výrobku	výrobek	k1gInSc2	výrobek
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dodáván	dodávat	k5eAaImNgInS	dodávat
z	z	k7c2	z
domácích	domácí	k2eAgInPc2d1	domácí
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Výjimky	výjimek	k1gInPc1	výjimek
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
odstavci	odstavec	k1gInSc3	odstavec
jsou	být	k5eAaImIp3nP	být
uvedeny	uveden	k2eAgInPc1d1	uveden
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
odstavci	odstavec	k1gInSc6	odstavec
<g/>
.	.	kIx.	.
</s>
<s>
Výjimky	výjimek	k1gInPc1	výjimek
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
na	na	k7c4	na
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
množstevní	množstevní	k2eAgInSc4d1	množstevní
předpis	předpis	k1gInSc4	předpis
platný	platný	k2eAgInSc4d1	platný
na	na	k7c6	na
území	území	k1gNnSc6	území
kterékoli	kterýkoli	k3yIgFnSc2	kterýkoli
smluvní	smluvní	k2eAgFnSc2d1	smluvní
strany	strana	k1gFnSc2	strana
ze	z	k7c2	z
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1947	[number]	k4	1947
nebo	nebo	k8xC	nebo
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1948	[number]	k4	1948
podle	podle	k7c2	podle
volby	volba	k1gFnSc2	volba
dotyčné	dotyčný	k2eAgFnSc2d1	dotyčná
smluvní	smluvní	k2eAgFnSc2d1	smluvní
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
problematice	problematika	k1gFnSc3	problematika
míchání	míchání	k1gNnSc2	míchání
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc2	zpracování
a	a	k8xC	a
používání	používání	k1gNnSc2	používání
výrobků	výrobek	k1gInPc2	výrobek
v	v	k7c6	v
určitých	určitý	k2eAgNnPc6d1	určité
množstvích	množství	k1gNnPc6	množství
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
odstavec	odstavec	k1gInSc1	odstavec
7	[number]	k4	7
článku	článek	k1gInSc2	článek
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
stanovení	stanovení	k1gNnSc4	stanovení
takového	takový	k3xDgInSc2	takový
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
množstevního	množstevní	k2eAgInSc2d1	množstevní
předpisu	předpis	k1gInSc2	předpis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nP	by
toto	tento	k3xDgNnSc4	tento
požadované	požadovaný	k2eAgNnSc4d1	požadované
množství	množství	k1gNnSc4	množství
přiděloval	přidělovat	k5eAaImAgMnS	přidělovat
mezi	mezi	k7c4	mezi
různé	různý	k2eAgMnPc4d1	různý
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
dodavatele	dodavatel	k1gMnPc4	dodavatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odstavec	odstavec	k1gInSc1	odstavec
8	[number]	k4	8
článku	článek	k1gInSc2	článek
III	III	kA	III
uvádí	uvádět	k5eAaImIp3nS	uvádět
výjimku	výjimka	k1gFnSc4	výjimka
z	z	k7c2	z
použití	použití	k1gNnSc2	použití
doložky	doložka	k1gFnSc2	doložka
národního	národní	k2eAgNnSc2d1	národní
zacházení	zacházení	k1gNnSc2	zacházení
<g/>
.	.	kIx.	.
</s>
<s>
Doložka	doložka	k1gFnSc1	doložka
národního	národní	k2eAgNnSc2d1	národní
zacházení	zacházení	k1gNnSc2	zacházení
nebude	být	k5eNaImBp3nS	být
použita	použít	k5eAaPmNgFnS	použít
na	na	k7c4	na
opatřování	opatřování	k1gNnSc4	opatřování
těch	ten	k3xDgInPc2	ten
výrobků	výrobek	k1gInPc2	výrobek
veřejnoprávními	veřejnoprávní	k2eAgInPc7d1	veřejnoprávní
subjekty	subjekt	k1gInPc7	subjekt
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
nakupovány	nakupovat	k5eAaBmNgInP	nakupovat
pro	pro	k7c4	pro
vládní	vládní	k2eAgInPc4d1	vládní
účely	účel	k1gInPc4	účel
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
prodeji	prodej	k1gInSc3	prodej
nebo	nebo	k8xC	nebo
použití	použití	k1gNnSc3	použití
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
zboží	zboží	k1gNnSc2	zboží
k	k	k7c3	k
prodeji	prodej	k1gInSc3	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
nezabraňuje	zabraňovat	k5eNaImIp3nS	zabraňovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
vypláceny	vyplácet	k5eAaImNgFnP	vyplácet
subvence	subvence	k1gFnPc1	subvence
výhradně	výhradně	k6eAd1	výhradně
domácím	domácí	k2eAgMnPc3d1	domácí
výrobcům	výrobce	k1gMnPc3	výrobce
a	a	k8xC	a
subvence	subvence	k1gFnSc2	subvence
poskytované	poskytovaný	k2eAgFnSc2d1	poskytovaná
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
vládních	vládní	k2eAgInPc2d1	vládní
nákupů	nákup	k1gInPc2	nákup
domácích	domácí	k2eAgInPc2d1	domácí
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odstavec	odstavec	k1gInSc1	odstavec
10	[number]	k4	10
článku	článek	k1gInSc2	článek
III	III	kA	III
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
smluvní	smluvní	k2eAgFnPc1d1	smluvní
strany	strana	k1gFnPc1	strana
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
zavedeny	zaveden	k2eAgInPc4d1	zaveden
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
množstevní	množstevní	k2eAgInPc4d1	množstevní
předpisy	předpis	k1gInPc4	předpis
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
osvětlených	osvětlený	k2eAgInPc2d1	osvětlený
kinematografických	kinematografický	k2eAgInPc2d1	kinematografický
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
tyto	tento	k3xDgInPc1	tento
předpisy	předpis	k1gInPc1	předpis
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
požadavkům	požadavek	k1gInPc3	požadavek
článku	článek	k1gInSc6	článek
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
výjimky	výjimka	k1gFnPc1	výjimka
z	z	k7c2	z
použití	použití	k1gNnSc2	použití
doložky	doložka	k1gFnSc2	doložka
národního	národní	k2eAgNnSc2d1	národní
zacházení	zacházení	k1gNnSc2	zacházení
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
v	v	k7c6	v
článku	článek	k1gInSc6	článek
XVI	XVI	kA	XVI
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
podrobněji	podrobně	k6eAd2	podrobně
zabývá	zabývat	k5eAaImIp3nS	zabývat
problematikou	problematika	k1gFnSc7	problematika
veřejných	veřejný	k2eAgFnPc2d1	veřejná
podpor	podpora	k1gFnPc2	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
uveden	uvést	k5eAaPmNgInS	uvést
požadavek	požadavek	k1gInSc1	požadavek
na	na	k7c4	na
omezení	omezení	k1gNnSc4	omezení
subvencí	subvence	k1gFnPc2	subvence
na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
základních	základní	k2eAgInPc2d1	základní
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
XIX	XIX	kA	XIX
připouští	připouštět	k5eAaImIp3nS	připouštět
výjimky	výjimka	k1gFnPc4	výjimka
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nouzových	nouzový	k2eAgNnPc2d1	nouzové
opatření	opatření	k1gNnPc2	opatření
při	při	k7c6	při
dovozu	dovoz	k1gInSc6	dovoz
některých	některý	k3yIgMnPc2	některý
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
v	v	k7c6	v
článku	článek	k1gInSc6	článek
XX	XX	kA	XX
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
všeobecné	všeobecný	k2eAgFnPc1d1	všeobecná
výjimky	výjimka	k1gFnPc1	výjimka
a	a	k8xC	a
článek	článek	k1gInSc1	článek
XXI	XXI	kA	XXI
připouští	připouštět	k5eAaImIp3nS	připouštět
bezpečnostní	bezpečnostní	k2eAgFnPc4d1	bezpečnostní
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
výjimky	výjimka	k1gFnPc1	výjimka
jsou	být	k5eAaImIp3nP	být
obdobné	obdobný	k2eAgInPc1d1	obdobný
jako	jako	k8xS	jako
výjimky	výjimka	k1gFnPc4	výjimka
uvedené	uvedený	k2eAgFnPc4d1	uvedená
u	u	k7c2	u
doložky	doložka	k1gFnSc2	doložka
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
výhod	výhoda	k1gFnPc2	výhoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
ROZEHNALOVÁ	Rozehnalová	k1gFnSc1	Rozehnalová
<g/>
,	,	kIx,	,
Naděžda	Naděžda	k1gFnSc1	Naděžda
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Wolters	Wolters	k1gInSc1	Wolters
Kluwer	Kluwra	k1gFnPc2	Kluwra
<g/>
,	,	kIx,	,
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
552	[number]	k4	552
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7357	[number]	k4	7357
<g/>
-	-	kIx~	-
<g/>
562	[number]	k4	562
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
Understanding	Understanding	k1gInSc1	Understanding
the	the	k?	the
WTO	WTO	kA	WTO
<g/>
:	:	kIx,	:
Basics	Basics	k1gInSc1	Basics
<g/>
.	.	kIx.	.
</s>
<s>
Principles	Principles	k1gInSc1	Principles
of	of	k?	of
the	the	k?	the
trading	trading	k1gInSc1	trading
system	systo	k1gNnSc7	systo
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
na	na	k7c6	na
WWW	WWW	kA	WWW
<g/>
:	:	kIx,	:
<	<	kIx(	<
<g/>
http://www.wto.org/english/thewto_e/whatis_e/tif_e/fact2_e.htm	[url]	k6eAd1	http://www.wto.org/english/thewto_e/whatis_e/tif_e/fact2_e.htm
<g/>
>	>	kIx)	>
<g/>
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
clech	clo	k1gNnPc6	clo
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
ASPI	ASPI	kA	ASPI
[	[	kIx(	[
<g/>
právní	právní	k2eAgInSc1d1	právní
informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Wolters	Wolters	k6eAd1	Wolters
Kluwer	Kluwer	k1gInSc1	Kluwer
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
<g/>
MÜLLEROVÁ	MÜLLEROVÁ	kA	MÜLLEROVÁ
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
<g/>
.	.	kIx.	.
</s>
<s>
Doložka	doložka	k1gFnSc1	doložka
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
výhod	výhoda	k1gFnPc2	výhoda
a	a	k8xC	a
doložka	doložka	k1gFnSc1	doložka
národního	národní	k2eAgNnSc2d1	národní
zacházení	zacházení	k1gNnSc2	zacházení
v	v	k7c6	v
právu	právo	k1gNnSc6	právo
WTO	WTO	kA	WTO
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
na	na	k7c6	na
WWW	WWW	kA	WWW
<g/>
:	:	kIx,	:
<	<	kIx(	<
<g/>
http://is.muni.cz/th/348720/pravf_b/	[url]	k4	http://is.muni.cz/th/348720/pravf_b/
<g/>
>	>	kIx)	>
</s>
</p>
