<s>
Brucelóza	brucelóza	k1gFnSc1	brucelóza
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
též	též	k9	též
vlnitá	vlnitý	k2eAgFnSc1d1	vlnitá
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
maltská	maltský	k2eAgFnSc1d1	Maltská
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
Bangova	Bangův	k2eAgFnSc1d1	Bangova
choroba	choroba	k1gFnSc1	choroba
<g/>
,	,	kIx,	,
infekční	infekční	k2eAgNnSc1d1	infekční
zmetání	zmetání	k1gNnSc1	zmetání
skotu	skot	k1gInSc2	skot
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
celosvětově	celosvětově	k6eAd1	celosvětově
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
nakažlivé	nakažlivý	k2eAgNnSc4d1	nakažlivé
bakteriální	bakteriální	k2eAgNnSc4d1	bakteriální
onemocnění	onemocnění	k1gNnSc4	onemocnění
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
jsou	být	k5eAaImIp3nP	být
gramnegativní	gramnegativní	k2eAgFnSc1d1	gramnegativní
bakterie	bakterie	k1gFnSc1	bakterie
rodu	rod	k1gInSc2	rod
Brucella	Brucello	k1gNnSc2	Brucello
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
choroby	choroba	k1gFnSc2	choroba
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
jejího	její	k3xOp3gMnSc4	její
původce	původce	k1gMnSc4	původce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
Sira	sir	k1gMnSc2	sir
Davida	David	k1gMnSc2	David
Bruce	Bruce	k1gMnSc2	Bruce
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
poprvé	poprvé	k6eAd1	poprvé
onemocnění	onemocnění	k1gNnPc4	onemocnění
popsal	popsat	k5eAaPmAgMnS	popsat
u	u	k7c2	u
britských	britský	k2eAgMnPc2d1	britský
vojáků	voják	k1gMnPc2	voják
na	na	k7c6	na
Maltě	Malta	k1gFnSc6	Malta
<g/>
.	.	kIx.	.
</s>
<s>
Primárně	primárně	k6eAd1	primárně
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
onemocnění	onemocnění	k1gNnPc4	onemocnění
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
<g/>
,	,	kIx,	,
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
psů	pes	k1gMnPc2	pes
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
nakazit	nakazit	k5eAaPmF	nakazit
od	od	k7c2	od
infikovaných	infikovaný	k2eAgNnPc2d1	infikované
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
nemoc	nemoc	k1gFnSc1	nemoc
se	se	k3xPyFc4	se
však	však	k9	však
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
nepřenáší	přenášet	k5eNaImIp3nS	přenášet
<g/>
.	.	kIx.	.
</s>
<s>
Brucelóza	brucelóza	k1gFnSc1	brucelóza
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
způsobena	způsobit	k5eAaPmNgFnS	způsobit
druhem	druh	k1gInSc7	druh
B.	B.	kA	B.
melitensis	melitensis	k1gInSc1	melitensis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Blízkého	blízký	k2eAgInSc2d1	blízký
a	a	k8xC	a
Středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
přímého	přímý	k2eAgInSc2d1	přímý
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
nemocným	nemocný	k2eAgNnSc7d1	nemocný
zvířetem	zvíře	k1gNnSc7	zvíře
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgInSc7d1	častý
zdrojem	zdroj	k1gInSc7	zdroj
infekce	infekce	k1gFnSc2	infekce
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
též	též	k9	též
tepelně	tepelně	k6eAd1	tepelně
neošetřené	ošetřený	k2eNgNnSc1d1	neošetřené
mléko	mléko	k1gNnSc1	mléko
a	a	k8xC	a
výrobky	výrobek	k1gInPc1	výrobek
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
se	se	k3xPyFc4	se
brucelóza	brucelóza	k1gFnSc1	brucelóza
projevuje	projevovat	k5eAaImIp3nS	projevovat
především	především	k9	především
potraty	potrat	k1gInPc4	potrat
u	u	k7c2	u
gravidních	gravidní	k2eAgFnPc2d1	gravidní
samic	samice	k1gFnPc2	samice
a	a	k8xC	a
záněty	zánět	k1gInPc4	zánět
varlat	varle	k1gNnPc2	varle
<g/>
,	,	kIx,	,
nadvarlat	nadvarle	k1gNnPc2	nadvarle
a	a	k8xC	a
ztrátou	ztráta	k1gFnSc7	ztráta
plodnosti	plodnost	k1gFnSc2	plodnost
samců	samec	k1gMnPc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
multisystémové	multisystémový	k2eAgNnSc4d1	multisystémové
onemocnění	onemocnění	k1gNnSc4	onemocnění
(	(	kIx(	(
<g/>
postihuje	postihovat	k5eAaImIp3nS	postihovat
více	hodně	k6eAd2	hodně
orgánových	orgánový	k2eAgFnPc2d1	orgánová
soustav	soustava	k1gFnPc2	soustava
<g/>
)	)	kIx)	)
charakterizované	charakterizovaný	k2eAgNnSc1d1	charakterizované
horečkou	horečka	k1gFnSc7	horečka
<g/>
,	,	kIx,	,
chronickým	chronický	k2eAgInSc7d1	chronický
průběhem	průběh	k1gInSc7	průběh
a	a	k8xC	a
postižením	postižení	k1gNnSc7	postižení
různých	různý	k2eAgInPc2d1	různý
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Diagnostika	diagnostika	k1gFnSc1	diagnostika
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
především	především	k9	především
na	na	k7c6	na
sérologických	sérologický	k2eAgFnPc6d1	sérologická
metodách	metoda	k1gFnPc6	metoda
(	(	kIx(	(
<g/>
Rose	Rose	k1gMnSc1	Rose
Bengal	Bengal	k1gMnSc1	Bengal
test	test	k1gMnSc1	test
<g/>
,	,	kIx,	,
ELISA	ELISA	kA	ELISA
<g/>
,	,	kIx,	,
komplement	komplement	k1gInSc1	komplement
fixační	fixační	k2eAgFnSc2d1	fixační
reakce	reakce	k1gFnSc2	reakce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
na	na	k7c4	na
kultivaci	kultivace	k1gFnSc4	kultivace
původce	původce	k1gMnSc2	původce
<g/>
.	.	kIx.	.
</s>
<s>
Lékem	lék	k1gInSc7	lék
první	první	k4xOgFnSc2	první
volby	volba	k1gFnSc2	volba
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
jsou	být	k5eAaImIp3nP	být
antibiotika	antibiotikum	k1gNnSc2	antibiotikum
rifampicin	rifampicin	k2eAgMnSc1d1	rifampicin
a	a	k8xC	a
doxycyklin	doxycyklin	k2eAgMnSc1d1	doxycyklin
<g/>
.	.	kIx.	.
</s>
<s>
Preventivní	preventivní	k2eAgNnPc1d1	preventivní
opatření	opatření	k1gNnPc1	opatření
spočívají	spočívat	k5eAaImIp3nP	spočívat
v	v	k7c6	v
důsledném	důsledný	k2eAgNnSc6d1	důsledné
sérologickém	sérologický	k2eAgNnSc6d1	sérologické
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
domácích	domácí	k2eAgNnPc2d1	domácí
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
zamezení	zamezení	k1gNnSc1	zamezení
konzumace	konzumace	k1gFnSc2	konzumace
nepasterizovaného	pasterizovaný	k2eNgNnSc2d1	nepasterizované
mléka	mléko	k1gNnSc2	mléko
a	a	k8xC	a
výrobků	výrobek	k1gInPc2	výrobek
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
endemickým	endemický	k2eAgInSc7d1	endemický
výskytem	výskyt	k1gInSc7	výskyt
brucelózy	brucelóza	k1gFnSc2	brucelóza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
patřila	patřit	k5eAaImAgFnS	patřit
brucelóza	brucelóza	k1gFnSc1	brucelóza
mezi	mezi	k7c7	mezi
kandidáty	kandidát	k1gMnPc7	kandidát
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
biologických	biologický	k2eAgFnPc2d1	biologická
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Minulost	minulost	k1gFnSc1	minulost
brucelózy	brucelóza	k1gFnSc2	brucelóza
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
spjata	spjat	k2eAgFnSc1d1	spjata
s	s	k7c7	s
oblastí	oblast	k1gFnSc7	oblast
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1751	[number]	k4	1751
popsal	popsat	k5eAaPmAgMnS	popsat
britský	britský	k2eAgMnSc1d1	britský
armádní	armádní	k2eAgMnSc1d1	armádní
lékař	lékař	k1gMnSc1	lékař
případy	případ	k1gInPc4	případ
chronické	chronický	k2eAgFnSc2d1	chronická
choroby	choroba	k1gFnSc2	choroba
s	s	k7c7	s
opakující	opakující	k2eAgFnSc7d1	opakující
se	se	k3xPyFc4	se
horečkou	horečka	k1gFnSc7	horečka
u	u	k7c2	u
vojáků	voják	k1gMnPc2	voják
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Menorca	Menorc	k1gInSc2	Menorc
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
se	se	k3xPyFc4	se
nápadně	nápadně	k6eAd1	nápadně
podobaly	podobat	k5eAaImAgFnP	podobat
nemoci	nemoc	k1gFnPc1	nemoc
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
již	již	k6eAd1	již
o	o	k7c4	o
2000	[number]	k4	2000
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
popsal	popsat	k5eAaPmAgInS	popsat
Hippokratés	Hippokratés	k1gInSc1	Hippokratés
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
objasnění	objasnění	k1gNnSc6	objasnění
původu	původ	k1gInSc2	původ
nemoci	nemoc	k1gFnSc2	nemoc
a	a	k8xC	a
jejím	její	k3xOp3gInSc6	její
popisu	popis	k1gInSc6	popis
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
opět	opět	k6eAd1	opět
tři	tři	k4xCgMnPc1	tři
jiní	jiný	k2eAgMnPc1d1	jiný
britští	britský	k2eAgMnPc1d1	britský
vojenští	vojenský	k2eAgMnPc1d1	vojenský
lékaři	lékař	k1gMnPc1	lékař
působící	působící	k2eAgMnPc1d1	působící
na	na	k7c6	na
Maltě	Malta	k1gFnSc6	Malta
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
maltská	maltský	k2eAgFnSc1d1	Maltská
horečka	horečka	k1gFnSc1	horečka
<g/>
)	)	kIx)	)
během	během	k7c2	během
Krymské	krymský	k2eAgFnSc2d1	Krymská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgMnS	být
J.	J.	kA	J.
A.	A.	kA	A.
Marston	Marston	k1gInSc1	Marston
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
charakterizoval	charakterizovat	k5eAaBmAgInS	charakterizovat
příznaky	příznak	k1gInPc4	příznak
onemocnění	onemocnění	k1gNnPc2	onemocnění
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vlastní	vlastní	k2eAgFnSc2d1	vlastní
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšího	významný	k2eAgInSc2d3	nejvýznamnější
objevu	objev	k1gInSc2	objev
však	však	k9	však
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
David	David	k1gMnSc1	David
Bruce	Bruce	k1gMnSc1	Bruce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
izoloval	izolovat	k5eAaBmAgInS	izolovat
a	a	k8xC	a
popsal	popsat	k5eAaPmAgInS	popsat
původce	původce	k1gMnSc4	původce
ze	z	k7c2	z
sleziny	slezina	k1gFnSc2	slezina
vojáků	voják	k1gMnPc2	voják
zemřelých	zemřelý	k1gMnPc2	zemřelý
na	na	k7c4	na
následky	následek	k1gInPc4	následek
toho	ten	k3xDgNnSc2	ten
horečnatého	horečnatý	k2eAgNnSc2d1	horečnaté
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Bakterii	bakterie	k1gFnSc4	bakterie
zařadil	zařadit	k5eAaPmAgMnS	zařadit
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Micrococcus	Micrococcus	k1gMnSc1	Micrococcus
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
jako	jako	k9	jako
Micrococcus	Micrococcus	k1gMnSc1	Micrococcus
melitensis	melitensis	k1gFnSc2	melitensis
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
bakterie	bakterie	k1gFnSc1	bakterie
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
podle	podle	k7c2	podle
jejího	její	k3xOp3gMnSc4	její
objevitele	objevitel	k1gMnSc4	objevitel
na	na	k7c4	na
Brucella	Brucell	k1gMnSc4	Brucell
melitensis	melitensis	k1gFnSc2	melitensis
<g/>
.	.	kIx.	.
</s>
<s>
Deset	deset	k4xCc1	deset
let	léto	k1gNnPc2	léto
po	po	k7c6	po
objevu	objev	k1gInSc6	objev
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Bruce	Bruce	k1gMnSc1	Bruce
publikoval	publikovat	k5eAaBmAgMnS	publikovat
další	další	k2eAgMnSc1d1	další
britský	britský	k2eAgMnSc1d1	britský
lékař	lékař	k1gMnSc1	lékař
M.	M.	kA	M.
L.	L.	kA	L.
Hughes	Hughes	k1gInSc4	Hughes
detailní	detailní	k2eAgInSc1d1	detailní
popis	popis	k1gInSc1	popis
klinických	klinický	k2eAgInPc2d1	klinický
případů	případ	k1gInPc2	případ
a	a	k8xC	a
patologických	patologický	k2eAgInPc2d1	patologický
nálezů	nález	k1gInPc2	nález
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
u	u	k7c2	u
celkem	celkem	k6eAd1	celkem
844	[number]	k4	844
pacientů	pacient	k1gMnPc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc4	onemocnění
pojmenoval	pojmenovat	k5eAaPmAgInS	pojmenovat
jako	jako	k8xC	jako
vlnitá	vlnitý	k2eAgFnSc1d1	vlnitá
horečka	horečka	k1gFnSc1	horečka
dle	dle	k7c2	dle
charakteristického	charakteristický	k2eAgInSc2d1	charakteristický
projevu	projev	k1gInSc2	projev
nemoci	nemoc	k1gFnSc2	nemoc
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
izoloval	izolovat	k5eAaBmAgMnS	izolovat
dánský	dánský	k2eAgMnSc1d1	dánský
vědec	vědec	k1gMnSc1	vědec
Bernard	Bernard	k1gMnSc1	Bernard
Bang	Bang	k1gMnSc1	Bang
bakterii	bakterie	k1gFnSc4	bakterie
(	(	kIx(	(
<g/>
sám	sám	k3xTgMnSc1	sám
původce	původce	k1gMnSc1	původce
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
jako	jako	k9	jako
Bacillus	Bacillus	k1gMnSc1	Bacillus
abortus	abortus	k1gInSc1	abortus
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
Brucella	Brucello	k1gNnSc2	Brucello
abortus	abortus	k1gInSc1	abortus
<g/>
)	)	kIx)	)
z	z	k7c2	z
placenty	placenta	k1gFnSc2	placenta
a	a	k8xC	a
potracených	potracený	k2eAgInPc2d1	potracený
plodů	plod	k1gInPc2	plod
u	u	k7c2	u
skotu	skot	k1gInSc2	skot
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
označení	označení	k1gNnSc4	označení
Bangova	Bangův	k2eAgFnSc1d1	Bangova
nemoc	nemoc	k1gFnSc1	nemoc
u	u	k7c2	u
skotu	skot	k1gInSc2	skot
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
bakterioložka	bakterioložka	k1gFnSc1	bakterioložka
Alice	Alice	k1gFnSc1	Alice
Evans	Evans	k1gInSc1	Evans
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
původce	původce	k1gMnSc4	původce
abortů	abort	k1gInPc2	abort
u	u	k7c2	u
skotu	skot	k1gInSc2	skot
objevený	objevený	k2eAgInSc4d1	objevený
Bangem	Bang	k1gInSc7	Bang
a	a	k8xC	a
bakterie	bakterie	k1gFnSc1	bakterie
popsaná	popsaný	k2eAgFnSc1d1	popsaná
Brucem	Bruec	k1gInSc7	Bruec
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
identické	identický	k2eAgFnPc1d1	identická
a	a	k8xC	a
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
některými	některý	k3yIgFnPc7	některý
antigenními	antigenní	k2eAgFnPc7d1	antigenní
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgInP	být
postupně	postupně	k6eAd1	postupně
popsány	popsat	k5eAaPmNgInP	popsat
další	další	k2eAgInPc1d1	další
druhy	druh	k1gInPc1	druh
brucel	brucet	k5eAaImAgMnS	brucet
u	u	k7c2	u
jiných	jiný	k2eAgNnPc2d1	jiné
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
druh	druh	k1gInSc1	druh
B.	B.	kA	B.
ovis	ovis	k1gInSc1	ovis
jako	jako	k9	jako
původce	původce	k1gMnSc4	původce
infekčního	infekční	k2eAgInSc2d1	infekční
zánětu	zánět	k1gInSc2	zánět
varlat	varle	k1gNnPc2	varle
u	u	k7c2	u
beranů	beran	k1gInPc2	beran
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
izolována	izolovat	k5eAaBmNgFnS	izolovat
B.	B.	kA	B.
neotomae	neotomae	k1gFnSc1	neotomae
z	z	k7c2	z
potkanů	potkan	k1gMnPc2	potkan
v	v	k7c6	v
pouštích	poušť	k1gFnPc6	poušť
Utahu	Utah	k1gInSc2	Utah
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
byl	být	k5eAaImAgInS	být
odhalen	odhalit	k5eAaPmNgInS	odhalit
druh	druh	k1gInSc1	druh
B.	B.	kA	B.
canis	canis	k1gInSc4	canis
jako	jako	k8xC	jako
původce	původce	k1gMnPc4	původce
hromadných	hromadný	k2eAgInPc2d1	hromadný
infekčních	infekční	k2eAgInPc2d1	infekční
potratů	potrat	k1gInPc2	potrat
u	u	k7c2	u
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Brucella	Brucello	k1gNnSc2	Brucello
<g/>
.	.	kIx.	.
</s>
<s>
Brucelózu	brucelóza	k1gFnSc4	brucelóza
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
bakterie	bakterie	k1gFnPc1	bakterie
rodu	rod	k1gInSc2	rod
Brucella	Brucello	k1gNnSc2	Brucello
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
gramnegativní	gramnegativní	k2eAgFnPc4d1	gramnegativní
<g/>
,	,	kIx,	,
tyčinkovité	tyčinkovitý	k2eAgFnPc4d1	tyčinkovitá
<g/>
,	,	kIx,	,
aerobní	aerobní	k2eAgFnPc4d1	aerobní
<g/>
,	,	kIx,	,
nesporulující	sporulující	k2eNgFnPc4d1	nesporulující
bakterie	bakterie	k1gFnPc4	bakterie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
hostitelském	hostitelský	k2eAgInSc6d1	hostitelský
organismu	organismus	k1gInSc6	organismus
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
intracelulárně	intracelulárně	k6eAd1	intracelulárně
(	(	kIx(	(
<g/>
uvnitř	uvnitř	k7c2	uvnitř
buněk	buňka	k1gFnPc2	buňka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
celkem	celkem	k6eAd1	celkem
osm	osm	k4xCc4	osm
druhů	druh	k1gInPc2	druh
brucel	brucet	k5eAaPmAgMnS	brucet
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
především	především	k9	především
primárním	primární	k2eAgMnSc7d1	primární
hostitelem	hostitel	k1gMnSc7	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Taxonomie	taxonomie	k1gFnPc4	taxonomie
brucel	brucet	k5eAaPmAgInS	brucet
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
stále	stále	k6eAd1	stále
nejasná	jasný	k2eNgFnSc1d1	nejasná
a	a	k8xC	a
nevyřešená	vyřešený	k2eNgFnSc1d1	nevyřešená
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
sekvenace	sekvenace	k1gFnSc2	sekvenace
genů	gen	k1gInPc2	gen
ribozomální	ribozomální	k2eAgFnSc2d1	ribozomální
RNA	RNA	kA	RNA
se	se	k3xPyFc4	se
brucely	brucet	k5eAaImAgInP	brucet
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
alfaproteobakterie	alfaproteobakterie	k1gFnPc4	alfaproteobakterie
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
blízký	blízký	k2eAgInSc4d1	blízký
fylogenetický	fylogenetický	k2eAgInSc4d1	fylogenetický
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
rodům	rod	k1gInPc3	rod
Agrobacterium	Agrobacterium	k1gNnSc1	Agrobacterium
<g/>
,	,	kIx,	,
Rickettsia	Rickettsia	k1gFnSc1	Rickettsia
<g/>
,	,	kIx,	,
Rhizobium	Rhizobium	k1gNnSc1	Rhizobium
a	a	k8xC	a
Rhodobacter	Rhodobacter	k1gInSc1	Rhodobacter
<g/>
.	.	kIx.	.
</s>
<s>
Grimont	Grimont	k1gMnSc1	Grimont
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
provedli	provést	k5eAaPmAgMnP	provést
analýzu	analýza	k1gFnSc4	analýza
DNA	dno	k1gNnSc2	dno
více	hodně	k6eAd2	hodně
než	než	k8xS	než
51	[number]	k4	51
kmenů	kmen	k1gInPc2	kmen
brucel	brucet	k5eAaPmAgMnS	brucet
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
druhy	druh	k1gInPc4	druh
brucel	brucet	k5eAaImAgMnS	brucet
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
natolik	natolik	k6eAd1	natolik
fylogeneticky	fylogeneticky	k6eAd1	fylogeneticky
blízké	blízký	k2eAgFnPc1d1	blízká
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
druh	druh	k1gInSc4	druh
označovaný	označovaný	k2eAgInSc4d1	označovaný
jako	jako	k8xS	jako
Brucella	Brucell	k1gMnSc4	Brucell
melitensis	melitensis	k1gFnSc4	melitensis
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
různých	různý	k2eAgInPc2d1	různý
biovarů	biovar	k1gInPc2	biovar
a	a	k8xC	a
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
rozdílům	rozdíl	k1gInPc3	rozdíl
v	v	k7c6	v
hostitelském	hostitelský	k2eAgNnSc6d1	hostitelské
spektru	spektrum	k1gNnSc6	spektrum
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
rozdílům	rozdíl	k1gInPc3	rozdíl
v	v	k7c6	v
klinických	klinický	k2eAgInPc6d1	klinický
projevech	projev	k1gInPc6	projev
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
tento	tento	k3xDgInSc1	tento
koncept	koncept	k1gInSc1	koncept
všeobecně	všeobecně	k6eAd1	všeobecně
přijat	přijmout	k5eAaPmNgInS	přijmout
a	a	k8xC	a
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
používá	používat	k5eAaImIp3nS	používat
původní	původní	k2eAgNnSc4d1	původní
druhové	druhový	k2eAgNnSc4d1	druhové
označení	označení	k1gNnSc4	označení
<g/>
.	.	kIx.	.
</s>
<s>
Brucelóza	brucelóza	k1gFnSc1	brucelóza
zvířat	zvíře	k1gNnPc2	zvíře
i	i	k8xC	i
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
lokality	lokalita	k1gFnPc1	lokalita
výskytu	výskyt	k1gInSc2	výskyt
nemoci	nemoc	k1gFnPc1	nemoc
jsou	být	k5eAaImIp3nP	být
omezeny	omezit	k5eAaPmNgFnP	omezit
v	v	k7c6	v
několika	několik	k4yIc6	několik
endemických	endemický	k2eAgFnPc6d1	endemická
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
oblast	oblast	k1gFnSc4	oblast
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Blízký	blízký	k2eAgInSc4d1	blízký
a	a	k8xC	a
Střední	střední	k2eAgInSc4d1	střední
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Incidence	incidence	k1gFnPc1	incidence
brucelózy	brucelóza	k1gFnSc2	brucelóza
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
patogenních	patogenní	k2eAgInPc2d1	patogenní
druhů	druh	k1gInPc2	druh
brucel	brucet	k5eAaImAgMnS	brucet
u	u	k7c2	u
zvířecí	zvířecí	k2eAgFnSc2d1	zvířecí
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgInPc1d1	vysoký
počty	počet	k1gInPc1	počet
lidí	člověk	k1gMnPc2	člověk
nemocných	nemocný	k2eAgMnPc2d1	nemocný
brucelózou	brucelóza	k1gFnSc7	brucelóza
najdeme	najít	k5eAaPmIp1nP	najít
především	především	k9	především
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc2d1	rozvojová
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
zejména	zejména	k9	zejména
v	v	k7c6	v
zemědělských	zemědělský	k2eAgFnPc6d1	zemědělská
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
život	život	k1gInSc1	život
lidí	člověk	k1gMnPc2	člověk
úzce	úzko	k6eAd1	úzko
spjat	spjat	k2eAgMnSc1d1	spjat
s	s	k7c7	s
hospodářskými	hospodářský	k2eAgNnPc7d1	hospodářské
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Zjištěné	zjištěný	k2eAgFnPc1d1	zjištěná
incidence	incidence	k1gFnPc1	incidence
brucelózy	brucelóza	k1gFnSc2	brucelóza
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
endemických	endemický	k2eAgFnPc6d1	endemická
oblastech	oblast	k1gFnPc6	oblast
kolísají	kolísat	k5eAaImIp3nP	kolísat
od	od	k7c2	od
méně	málo	k6eAd2	málo
než	než	k8xS	než
0,01	[number]	k4	0,01
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
případů	případ	k1gInPc2	případ
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
Jordánsku	Jordánsko	k1gNnSc6	Jordánsko
je	být	k5eAaImIp3nS	být
hlášeno	hlásit	k5eAaImNgNnS	hlásit
33	[number]	k4	33
případů	případ	k1gInPc2	případ
brucelózy	brucelóza	k1gFnSc2	brucelóza
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kuvajtu	Kuvajt	k1gInSc6	Kuvajt
dokonce	dokonce	k9	dokonce
88	[number]	k4	88
případů	případ	k1gInPc2	případ
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
v	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
incidence	incidence	k1gFnSc1	incidence
kolem	kolem	k7c2	kolem
0,04	[number]	k4	0,04
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
zhruba	zhruba	k6eAd1	zhruba
200	[number]	k4	200
případů	případ	k1gInPc2	případ
ročně	ročně	k6eAd1	ročně
po	po	k7c6	po
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
EU	EU	kA	EU
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
hlášeno	hlásit	k5eAaImNgNnS	hlásit
1337	[number]	k4	1337
případů	případ	k1gInPc2	případ
z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
21	[number]	k4	21
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Incidence	incidence	k1gFnSc1	incidence
brucelózy	brucelóza	k1gFnSc2	brucelóza
tedy	tedy	k9	tedy
činí	činit	k5eAaImIp3nS	činit
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
Unie	unie	k1gFnSc2	unie
kolem	kolem	k7c2	kolem
0,4	[number]	k4	0,4
případů	případ	k1gInPc2	případ
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
incidence	incidence	k1gFnSc1	incidence
brucelózy	brucelóza	k1gFnSc2	brucelóza
u	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
hlášeny	hlásit	k5eAaImNgInP	hlásit
z	z	k7c2	z
Balkánského	balkánský	k2eAgInSc2d1	balkánský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
ze	z	k7c2	z
států	stát	k1gInPc2	stát
kolem	kolem	k7c2	kolem
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
dle	dle	k7c2	dle
informací	informace	k1gFnPc2	informace
Světové	světový	k2eAgFnSc2d1	světová
organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
zdraví	zdraví	k1gNnSc4	zdraví
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
zkratka	zkratka	k1gFnSc1	zkratka
OIE	OIE	kA	OIE
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
incidence	incidence	k1gFnSc1	incidence
B.	B.	kA	B.
melitensis	melitensis	k1gFnSc1	melitensis
u	u	k7c2	u
domácích	domácí	k2eAgMnPc2d1	domácí
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2006	[number]	k4	2006
a	a	k8xC	a
2007	[number]	k4	2007
hlášena	hlásit	k5eAaImNgFnS	hlásit
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
Albánie	Albánie	k1gFnSc1	Albánie
<g/>
,	,	kIx,	,
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
zjištěna	zjištěn	k2eAgFnSc1d1	zjištěna
přítomnost	přítomnost	k1gFnSc1	přítomnost
Brucella	Brucell	k1gMnSc2	Brucell
spp	spp	k?	spp
<g/>
.	.	kIx.	.
v	v	k7c6	v
nezpracovaném	zpracovaný	k2eNgNnSc6d1	nezpracované
kravském	kravský	k2eAgNnSc6d1	kravské
mléce	mléko	k1gNnSc6	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
třeba	třeba	k6eAd1	třeba
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ne	ne	k9	ne
všechny	všechen	k3xTgFnPc1	všechen
členské	členský	k2eAgFnPc1d1	členská
země	zem	k1gFnPc1	zem
EU	EU	kA	EU
provádějí	provádět	k5eAaImIp3nP	provádět
monitoring	monitoring	k1gInSc4	monitoring
brucelózy	brucelóza	k1gFnSc2	brucelóza
v	v	k7c6	v
mléčných	mléčný	k2eAgFnPc6d1	mléčná
potravinách	potravina	k1gFnPc6	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozsáhlým	rozsáhlý	k2eAgInPc3d1	rozsáhlý
eradikačním	eradikační	k2eAgInPc3d1	eradikační
programům	program	k1gInPc3	program
probíhajících	probíhající	k2eAgFnPc2d1	probíhající
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
států	stát	k1gInPc2	stát
byla	být	k5eAaImAgFnS	být
brucelóza	brucelóza	k1gFnSc1	brucelóza
skotu	skot	k1gInSc2	skot
(	(	kIx(	(
<g/>
B.	B.	kA	B.
abortus	abortus	k1gInSc1	abortus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
koz	koza	k1gFnPc2	koza
(	(	kIx(	(
<g/>
B.	B.	kA	B.
melitensis	melitensis	k1gFnSc2	melitensis
<g/>
)	)	kIx)	)
či	či	k8xC	či
brucelóza	brucelóza	k1gFnSc1	brucelóza
prasat	prase	k1gNnPc2	prase
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
již	již	k6eAd1	již
zcela	zcela	k6eAd1	zcela
vymýcena	vymýcen	k2eAgFnSc1d1	vymýcena
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
země	zem	k1gFnPc1	zem
si	se	k3xPyFc3	se
drží	držet	k5eAaImIp3nP	držet
statut	statut	k1gInSc4	statut
zemí	zem	k1gFnPc2	zem
prostých	prostý	k2eAgFnPc2d1	prostá
nákazy	nákaza	k1gFnPc4	nákaza
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
přísná	přísný	k2eAgNnPc4d1	přísné
veterinární	veterinární	k2eAgNnPc4d1	veterinární
opatření	opatření	k1gNnPc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Brucelóza	brucelóza	k1gFnSc1	brucelóza
skotu	skot	k1gInSc2	skot
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
eradikována	eradikovat	k5eAaImNgFnS	eradikovat
např.	např.	kA	např.
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
,	,	kIx,	,
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
Severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
Střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
prostá	prostý	k2eAgFnSc1d1	prostá
bovinní	bovinní	k2eAgFnSc1d1	bovinní
brucelózy	brucelóza	k1gFnPc1	brucelóza
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
si	se	k3xPyFc3	se
drží	držet	k5eAaImIp3nS	držet
statut	statut	k1gInSc4	statut
země	zem	k1gFnSc2	zem
prosté	prostý	k2eAgFnSc2d1	prostá
B.	B.	kA	B.
ovis	ovis	k6eAd1	ovis
a	a	k8xC	a
B.	B.	kA	B.
melitensis	melitensis	k1gInSc1	melitensis
u	u	k7c2	u
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
koz	koza	k1gFnPc2	koza
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
formou	forma	k1gFnSc7	forma
brucelózy	brucelóza	k1gFnSc2	brucelóza
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
brucelóza	brucelóza	k1gFnSc1	brucelóza
zajíců	zajíc	k1gMnPc2	zajíc
způsobená	způsobený	k2eAgFnSc1d1	způsobená
B.	B.	kA	B.
suis	suis	k1gInSc4	suis
biovarem	biovar	k1gInSc7	biovar
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Brucelóza	brucelóza	k1gFnSc1	brucelóza
zajíců	zajíc	k1gMnPc2	zajíc
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
určitých	určitý	k2eAgNnPc6d1	určité
přírodních	přírodní	k2eAgNnPc6d1	přírodní
endemických	endemický	k2eAgNnPc6d1	endemické
ohniscích	ohnisko	k1gNnPc6	ohnisko
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c6	o
oblasti	oblast	k1gFnSc6	oblast
Královehradeckého	královehradecký	k2eAgInSc2d1	královehradecký
kraje	kraj	k1gInSc2	kraj
(	(	kIx(	(
<g/>
Hradecko	Hradecko	k1gNnSc1	Hradecko
<g/>
,	,	kIx,	,
Náchodsko	Náchodsko	k1gNnSc1	Náchodsko
<g/>
,	,	kIx,	,
Rychnovsko	Rychnovsko	k1gNnSc1	Rychnovsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ústeckého	ústecký	k2eAgMnSc4d1	ústecký
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Litoměřice	Litoměřice	k1gInPc1	Litoměřice
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
(	(	kIx(	(
<g/>
okresy	okres	k1gInPc4	okres
Břeclav	Břeclav	k1gFnSc1	Břeclav
a	a	k8xC	a
Znojmo	Znojmo	k1gNnSc1	Znojmo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
další	další	k2eAgFnPc4d1	další
lokality	lokalita	k1gFnPc4	lokalita
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
jsou	být	k5eAaImIp3nP	být
nařízena	nařízen	k2eAgNnPc4d1	nařízeno
mimořádná	mimořádný	k2eAgNnPc4d1	mimořádné
veterinární	veterinární	k2eAgNnPc4d1	veterinární
opatření	opatření	k1gNnPc4	opatření
k	k	k7c3	k
zamezení	zamezení	k1gNnSc3	zamezení
šíření	šíření	k1gNnSc2	šíření
infekce	infekce	k1gFnSc2	infekce
a	a	k8xC	a
zabránění	zabránění	k1gNnSc4	zabránění
přenosu	přenos	k1gInSc2	přenos
brucelózy	brucelóza	k1gFnSc2	brucelóza
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
či	či	k8xC	či
prasata	prase	k1gNnPc4	prase
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
těchto	tento	k3xDgNnPc2	tento
opatření	opatření	k1gNnPc2	opatření
je	být	k5eAaImIp3nS	být
výzva	výzva	k1gFnSc1	výzva
pro	pro	k7c4	pro
myslivce	myslivec	k1gMnSc4	myslivec
k	k	k7c3	k
opatrnému	opatrný	k2eAgNnSc3d1	opatrné
zacházení	zacházení	k1gNnSc3	zacházení
s	s	k7c7	s
uhynulými	uhynulý	k2eAgMnPc7d1	uhynulý
či	či	k8xC	či
ulovenými	ulovený	k2eAgMnPc7d1	ulovený
zajíci	zajíc	k1gMnPc7	zajíc
(	(	kIx(	(
<g/>
používání	používání	k1gNnSc1	používání
rukavic	rukavice	k1gFnPc2	rukavice
a	a	k8xC	a
dezinfekčních	dezinfekční	k2eAgInPc2d1	dezinfekční
prostředků	prostředek	k1gInPc2	prostředek
<g/>
)	)	kIx)	)
a	a	k8xC	a
zejména	zejména	k9	zejména
povinné	povinný	k2eAgNnSc1d1	povinné
sérologické	sérologický	k2eAgNnSc1d1	sérologické
vyšetření	vyšetření	k1gNnSc1	vyšetření
všech	všecek	k3xTgMnPc2	všecek
odlovených	odlovený	k2eAgMnPc2d1	odlovený
zajíců	zajíc	k1gMnPc2	zajíc
rychlou	rychlý	k2eAgFnSc7d1	rychlá
sklíčkovou	sklíčkový	k2eAgFnSc7d1	sklíčkový
aglutinací	aglutinace	k1gFnSc7	aglutinace
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
skutečností	skutečnost	k1gFnSc7	skutečnost
ve	v	k7c6	v
výskytu	výskyt	k1gInSc6	výskyt
brucelózy	brucelóza	k1gFnSc2	brucelóza
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
přestože	přestože	k8xS	přestože
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
vyspělých	vyspělý	k2eAgInPc2d1	vyspělý
států	stát	k1gInPc2	stát
byla	být	k5eAaImAgFnS	být
brucelóza	brucelóza	k1gFnSc1	brucelóza
skotu	skot	k1gInSc2	skot
(	(	kIx(	(
<g/>
B.	B.	kA	B.
abortus	abortus	k1gInSc1	abortus
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
eradikována	eradikován	k2eAgFnSc1d1	eradikován
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
USA	USA	kA	USA
se	se	k3xPyFc4	se
brucelóza	brucelóza	k1gFnSc1	brucelóza
skotu	skot	k1gInSc2	skot
stále	stále	k6eAd1	stále
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
dlouhodobé	dlouhodobý	k2eAgInPc4d1	dlouhodobý
a	a	k8xC	a
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
eradikační	eradikační	k2eAgInPc4d1	eradikační
programy	program	k1gInPc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
příčin	příčina	k1gFnPc2	příčina
je	být	k5eAaImIp3nS	být
endemický	endemický	k2eAgInSc1d1	endemický
výskyt	výskyt	k1gInSc1	výskyt
B.	B.	kA	B.
abortus	abortus	k1gInSc1	abortus
u	u	k7c2	u
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgNnPc2d1	žijící
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
bizonů	bizon	k1gMnPc2	bizon
(	(	kIx(	(
<g/>
Bison	Bison	k1gMnSc1	Bison
bison	bison	k1gMnSc1	bison
<g/>
)	)	kIx)	)
a	a	k8xC	a
jelenů	jelen	k1gMnPc2	jelen
wapiti	wapít	k5eAaPmNgMnP	wapít
(	(	kIx(	(
<g/>
Cervus	Cervus	k1gMnSc1	Cervus
elaphus	elaphus	k1gMnSc1	elaphus
canadensis	canadensis	k1gFnSc1	canadensis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
funguje	fungovat	k5eAaImIp3nS	fungovat
již	již	k6eAd1	již
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
program	program	k1gInSc4	program
na	na	k7c6	na
eliminaci	eliminace	k1gFnSc6	eliminace
brucelózy	brucelóza	k1gFnSc2	brucelóza
u	u	k7c2	u
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
prasat	prase	k1gNnPc2	prase
s	s	k7c7	s
názvem	název	k1gInSc7	název
Cooperative	Cooperativ	k1gInSc5	Cooperativ
State	status	k1gInSc5	status
Federal	Federal	k1gFnPc7	Federal
Brucellosis	Brucellosis	k1gFnSc1	Brucellosis
Eradication	Eradication	k1gInSc1	Eradication
Program	program	k1gInSc1	program
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
programu	program	k1gInSc3	program
je	být	k5eAaImIp3nS	být
výskyt	výskyt	k1gInSc1	výskyt
bovinní	bovinní	k2eAgFnSc2d1	bovinní
brucelózy	brucelóza	k1gFnSc2	brucelóza
hlášen	hlásit	k5eAaImNgInS	hlásit
už	už	k6eAd1	už
jen	jen	k6eAd1	jen
ze	z	k7c2	z
6	[number]	k4	6
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
prevalence	prevalence	k1gFnPc1	prevalence
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
státech	stát	k1gInPc6	stát
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
0,25	[number]	k4	0,25
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Názorným	názorný	k2eAgInSc7d1	názorný
příkladem	příklad	k1gInSc7	příklad
efektivity	efektivita	k1gFnSc2	efektivita
tohoto	tento	k3xDgInSc2	tento
ozdravovacího	ozdravovací	k2eAgInSc2d1	ozdravovací
programu	program	k1gInSc2	program
jsou	být	k5eAaImIp3nP	být
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgFnPc4d1	roční
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
ztráty	ztráta	k1gFnPc4	ztráta
způsobené	způsobený	k2eAgFnPc4d1	způsobená
brucelózou	brucelóza	k1gFnSc7	brucelóza
(	(	kIx(	(
<g/>
pokles	pokles	k1gInSc1	pokles
mléčné	mléčný	k2eAgFnSc2d1	mléčná
produkce	produkce	k1gFnSc2	produkce
<g/>
,	,	kIx,	,
potracená	potracený	k2eAgNnPc4d1	potracené
telata	tele	k1gNnPc4	tele
a	a	k8xC	a
selata	sele	k1gNnPc4	sele
<g/>
)	)	kIx)	)
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
činily	činit	k5eAaImAgInP	činit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
milión	milión	k4xCgInSc4	milión
USD	USD	kA	USD
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
2007	[number]	k4	2007
a	a	k8xC	a
počátek	počátek	k1gInSc4	počátek
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
B.	B.	kA	B.
abortus	abortus	k1gInSc1	abortus
u	u	k7c2	u
domácího	domácí	k2eAgInSc2d1	domácí
skotu	skot	k1gInSc2	skot
hlášena	hlásit	k5eAaImNgFnS	hlásit
pouze	pouze	k6eAd1	pouze
ze	z	k7c2	z
států	stát	k1gInPc2	stát
Montana	Montana	k1gFnSc1	Montana
a	a	k8xC	a
Louisiana	Louisiana	k1gFnSc1	Louisiana
<g/>
.	.	kIx.	.
</s>
<s>
Eradikační	Eradikační	k2eAgInSc1d1	Eradikační
program	program	k1gInSc1	program
je	být	k5eAaImIp3nS	být
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
krom	krom	k7c2	krom
sérologického	sérologický	k2eAgInSc2d1	sérologický
monitoringu	monitoring	k1gInSc2	monitoring
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
mléka	mléko	k1gNnSc2	mléko
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
na	na	k7c4	na
volně	volně	k6eAd1	volně
žijící	žijící	k2eAgNnPc4d1	žijící
stáda	stádo	k1gNnPc4	stádo
bizonů	bizon	k1gMnPc2	bizon
a	a	k8xC	a
jelenů	jelen	k1gMnPc2	jelen
wapiti	wapit	k1gMnPc1	wapit
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
brucelózy	brucelóza	k1gFnSc2	brucelóza
pro	pro	k7c4	pro
domácí	domácí	k2eAgInSc4d1	domácí
dobytek	dobytek	k1gInSc4	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
oblastí	oblast	k1gFnSc7	oblast
s	s	k7c7	s
endemickým	endemický	k2eAgInSc7d1	endemický
výskytem	výskyt	k1gInSc7	výskyt
B.	B.	kA	B.
abortus	abortus	k1gInSc1	abortus
v	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc4	oblast
Yellowstonského	Yellowstonský	k2eAgInSc2d1	Yellowstonský
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
(	(	kIx(	(
<g/>
YNP	YNP	kA	YNP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
území	území	k1gNnSc6	území
států	stát	k1gInPc2	stát
Wyoming	Wyoming	k1gInSc1	Wyoming
<g/>
,	,	kIx,	,
Montana	Montana	k1gFnSc1	Montana
a	a	k8xC	a
Idaho	Ida	k1gMnSc4	Ida
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
%	%	kIx~	%
populace	populace	k1gFnPc1	populace
bizonů	bizon	k1gMnPc2	bizon
z	z	k7c2	z
YNP	YNP	kA	YNP
je	být	k5eAaImIp3nS	být
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
na	na	k7c4	na
brucelózu	brucelóza	k1gFnSc4	brucelóza
<g/>
.	.	kIx.	.
</s>
<s>
Brucelóza	brucelóza	k1gFnSc1	brucelóza
bizonů	bizon	k1gMnPc2	bizon
představuje	představovat	k5eAaImIp3nS	představovat
krom	krom	k7c2	krom
samotného	samotný	k2eAgNnSc2d1	samotné
ovlivnění	ovlivnění	k1gNnSc2	ovlivnění
zdraví	zdraví	k1gNnSc2	zdraví
bizoní	bizoní	k2eAgFnSc2d1	bizoní
populace	populace	k1gFnSc2	populace
také	také	k9	také
zdroj	zdroj	k1gInSc4	zdroj
infekce	infekce	k1gFnSc2	infekce
pro	pro	k7c4	pro
domácí	domácí	k2eAgInSc4d1	domácí
dobytek	dobytek	k1gInSc4	dobytek
a	a	k8xC	a
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
infekce	infekce	k1gFnSc2	infekce
pro	pro	k7c4	pro
lovce	lovec	k1gMnSc4	lovec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ulovená	ulovený	k2eAgNnPc1d1	ulovené
zvířata	zvíře	k1gNnPc1	zvíře
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
koncem	konec	k1gInSc7	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
přistoupilo	přistoupit	k5eAaPmAgNnS	přistoupit
k	k	k7c3	k
plošné	plošný	k2eAgFnSc3d1	plošná
vakcinaci	vakcinace	k1gFnSc3	vakcinace
bizonů	bizon	k1gMnPc2	bizon
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
YNP	YNP	kA	YNP
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
vakcíny	vakcína	k1gFnSc2	vakcína
původně	původně	k6eAd1	původně
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
pro	pro	k7c4	pro
skot	skot	k1gInSc4	skot
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
druhy	druh	k1gInPc4	druh
brucel	brucet	k5eAaImAgMnS	brucet
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
u	u	k7c2	u
svých	svůj	k3xOyFgMnPc2	svůj
typických	typický	k2eAgMnPc2d1	typický
rezervoárových	rezervoárův	k2eAgMnPc2d1	rezervoárův
hostitelů	hostitel	k1gMnPc2	hostitel
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
tabulka	tabulka	k1gFnSc1	tabulka
výše	výše	k1gFnSc1	výše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
je	být	k5eAaImIp3nS	být
vnímavé	vnímavý	k2eAgNnSc1d1	vnímavé
daleko	daleko	k6eAd1	daleko
širší	široký	k2eAgNnSc1d2	širší
spektrum	spektrum	k1gNnSc1	spektrum
hostitelů	hostitel	k1gMnPc2	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
melitensis	melitensis	k1gFnSc1	melitensis
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
koz	koza	k1gFnPc2	koza
a	a	k8xC	a
velbloudů	velbloud	k1gMnPc2	velbloud
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přenosná	přenosný	k2eAgFnSc1d1	přenosná
na	na	k7c4	na
skot	skot	k1gInSc4	skot
<g/>
,	,	kIx,	,
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
psa	pes	k1gMnSc4	pes
ale	ale	k8xC	ale
i	i	k9	i
další	další	k2eAgInPc4d1	další
druhy	druh	k1gInPc4	druh
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
také	také	k9	také
hlavní	hlavní	k2eAgMnSc1d1	hlavní
původce	původce	k1gMnSc1	původce
brucelózy	brucelóza	k1gFnSc2	brucelóza
skotu	skot	k1gInSc2	skot
-	-	kIx~	-
B.	B.	kA	B.
abortus	abortus	k1gInSc1	abortus
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
přenosný	přenosný	k2eAgInSc1d1	přenosný
ze	z	k7c2	z
skotu	skot	k1gInSc2	skot
na	na	k7c4	na
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
ze	z	k7c2	z
skotu	skot	k1gInSc2	skot
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokonce	dokonce	k9	dokonce
i	i	k9	i
ze	z	k7c2	z
psa	pes	k1gMnSc2	pes
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Bovinní	bovinní	k2eAgFnSc7d1	bovinní
brucelózou	brucelóza	k1gFnSc7	brucelóza
se	se	k3xPyFc4	se
psi	pes	k1gMnPc1	pes
infikují	infikovat	k5eAaBmIp3nP	infikovat
pojídáním	pojídání	k1gNnSc7	pojídání
plodových	plodový	k2eAgInPc2d1	plodový
obalů	obal	k1gInPc2	obal
krav	kráva	k1gFnPc2	kráva
a	a	k8xC	a
mumifikovaných	mumifikovaný	k2eAgNnPc2d1	mumifikované
telat	tele	k1gNnPc2	tele
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
najdou	najít	k5eAaPmIp3nP	najít
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
ve	v	k7c6	v
stájích	stáj	k1gFnPc6	stáj
skotu	skot	k1gInSc2	skot
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
z	z	k7c2	z
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
vnímavý	vnímavý	k2eAgMnSc1d1	vnímavý
k	k	k7c3	k
celkem	celek	k1gInSc7	celek
čtyřem	čtyři	k4xCgInPc3	čtyři
druhům	druh	k1gInPc3	druh
brucel	brucet	k5eAaImAgMnS	brucet
<g/>
:	:	kIx,	:
B.	B.	kA	B.
melitensis	melitensis	k1gInSc1	melitensis
<g/>
,	,	kIx,	,
B.	B.	kA	B.
abortus	abortus	k1gInSc1	abortus
<g/>
,	,	kIx,	,
B.	B.	kA	B.
suis	suis	k1gInSc1	suis
a	a	k8xC	a
B.	B.	kA	B.
canis	canis	k1gInSc1	canis
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
virulentní	virulentní	k2eAgMnSc1d1	virulentní
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
jsou	být	k5eAaImIp3nP	být
druhy	druh	k1gInPc7	druh
B.	B.	kA	B.
melitensis	melitensis	k1gFnPc2	melitensis
(	(	kIx(	(
<g/>
biovary	biovar	k1gInPc7	biovar
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
B.	B.	kA	B.
suis	suis	k1gInSc1	suis
(	(	kIx(	(
<g/>
biovar	biovar	k1gInSc1	biovar
1	[number]	k4	1
a	a	k8xC	a
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
abortus	abortus	k1gInSc1	abortus
a	a	k8xC	a
B.	B.	kA	B.
canis	canis	k1gInSc4	canis
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
obecně	obecně	k6eAd1	obecně
lehčí	lehký	k2eAgFnSc4d2	lehčí
formu	forma	k1gFnSc4	forma
brucelózy	brucelóza	k1gFnSc2	brucelóza
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
i	i	k9	i
tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
mohou	moct	k5eAaImIp3nP	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
život	život	k1gInSc4	život
ohrožující	ohrožující	k2eAgFnSc2d1	ohrožující
komplikace	komplikace	k1gFnSc2	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejčastější	častý	k2eAgFnSc7d3	nejčastější
příčinou	příčina	k1gFnSc7	příčina
brucelózy	brucelóza	k1gFnSc2	brucelóza
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
B.	B.	kA	B.
melitensis	melitensis	k1gInSc1	melitensis
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
státech	stát	k1gInPc6	stát
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
případy	případ	k1gInPc4	případ
brucelózy	brucelóza	k1gFnSc2	brucelóza
lidí	člověk	k1gMnPc2	člověk
způsobeny	způsoben	k2eAgInPc1d1	způsoben
právě	právě	k6eAd1	právě
druhem	druh	k1gInSc7	druh
B.	B.	kA	B.
melitensis	melitensis	k1gInSc1	melitensis
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
studii	studie	k1gFnSc6	studie
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
celkem	celkem	k6eAd1	celkem
2107	[number]	k4	2107
pacientů	pacient	k1gMnPc2	pacient
s	s	k7c7	s
diagnostikovanou	diagnostikovaný	k2eAgFnSc7d1	diagnostikovaná
brucelózou	brucelóza	k1gFnSc7	brucelóza
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
98	[number]	k4	98
%	%	kIx~	%
původcem	původce	k1gMnSc7	původce
B.	B.	kA	B.
melitensis	melitensis	k1gFnPc4	melitensis
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
B.	B.	kA	B.
abortus	abortus	k1gInSc1	abortus
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
daleko	daleko	k6eAd1	daleko
méně	málo	k6eAd2	málo
častějším	častý	k2eAgMnSc7d2	častější
původcem	původce	k1gMnSc7	původce
onemocnění	onemocnění	k1gNnSc2	onemocnění
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
nakazit	nakazit	k5eAaPmF	nakazit
pouze	pouze	k6eAd1	pouze
od	od	k7c2	od
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
se	se	k3xPyFc4	se
nemoc	nemoc	k1gFnSc1	nemoc
prakticky	prakticky	k6eAd1	prakticky
nepřenáší	přenášet	k5eNaImIp3nS	přenášet
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
přenos	přenos	k1gInSc1	přenos
z	z	k7c2	z
člověka	člověk	k1gMnSc2	člověk
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
extrémně	extrémně	k6eAd1	extrémně
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
blízkém	blízký	k2eAgInSc6d1	blízký
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
nemocnou	nemocný	k2eAgFnSc7d1	nemocná
osobou	osoba	k1gFnSc7	osoba
nebo	nebo	k8xC	nebo
pohlavním	pohlavní	k2eAgInSc7d1	pohlavní
stykem	styk	k1gInSc7	styk
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
způsob	způsob	k1gInSc1	způsob
přenosu	přenos	k1gInSc2	přenos
infekce	infekce	k1gFnSc2	infekce
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
přímý	přímý	k2eAgInSc4d1	přímý
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
infikovanými	infikovaný	k2eAgNnPc7d1	infikované
zvířaty	zvíře	k1gNnPc7	zvíře
a	a	k8xC	a
požitím	požití	k1gNnSc7	požití
kontaminované	kontaminovaný	k2eAgFnSc2d1	kontaminovaná
potravy	potrava	k1gFnSc2	potrava
z	z	k7c2	z
nemocných	mocný	k2eNgNnPc2d1	nemocné
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Největšímu	veliký	k2eAgNnSc3d3	veliký
riziku	riziko	k1gNnSc3	riziko
infekce	infekce	k1gFnSc2	infekce
jsou	být	k5eAaImIp3nP	být
vystaveni	vystaven	k2eAgMnPc1d1	vystaven
lidé	člověk	k1gMnPc1	člověk
pracující	pracující	k1gMnSc1	pracující
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
-	-	kIx~	-
chovatelé	chovatel	k1gMnPc1	chovatel
<g/>
,	,	kIx,	,
veterináři	veterinář	k1gMnPc1	veterinář
<g/>
,	,	kIx,	,
pracovníci	pracovník	k1gMnPc1	pracovník
jatek	jatka	k1gFnPc2	jatka
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
infekce	infekce	k1gFnSc2	infekce
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc4	všechen
sekrety	sekret	k1gInPc4	sekret
(	(	kIx(	(
<g/>
mléko	mléko	k1gNnSc1	mléko
<g/>
)	)	kIx)	)
a	a	k8xC	a
exkrety	exkret	k1gInPc1	exkret
(	(	kIx(	(
<g/>
moč	moč	k1gFnSc1	moč
<g/>
,	,	kIx,	,
trus	trus	k1gInSc1	trus
<g/>
)	)	kIx)	)
infikovaných	infikovaný	k2eAgNnPc2d1	infikované
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
jejich	jejich	k3xOp3gInPc1	jejich
orgány	orgán	k1gInPc1	orgán
a	a	k8xC	a
svalovina	svalovina	k1gFnSc1	svalovina
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
bakterií	bakterie	k1gFnSc7	bakterie
Brucella	Brucello	k1gNnSc2	Brucello
spp	spp	k?	spp
<g/>
.	.	kIx.	.
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
plodová	plodový	k2eAgFnSc1d1	plodová
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
plodové	plodový	k2eAgInPc1d1	plodový
obaly	obal	k1gInPc1	obal
infikovaných	infikovaný	k2eAgFnPc2d1	infikovaná
samic	samice	k1gFnPc2	samice
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Brucelóza	brucelóza	k1gFnSc1	brucelóza
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
rovněž	rovněž	k9	rovněž
i	i	k9	i
vzduchem	vzduch	k1gInSc7	vzduch
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
aerosolu	aerosol	k1gInSc2	aerosol
<g/>
,	,	kIx,	,
poraněnou	poraněný	k2eAgFnSc7d1	poraněná
kůží	kůže	k1gFnSc7	kůže
a	a	k8xC	a
všemi	všecek	k3xTgFnPc7	všecek
sliznicemi	sliznice	k1gFnPc7	sliznice
(	(	kIx(	(
<g/>
spojivka	spojivka	k1gFnSc1	spojivka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
s	s	k7c7	s
brucelami	brucela	k1gFnPc7	brucela
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
mikrobiologických	mikrobiologický	k2eAgFnPc6d1	mikrobiologická
laboratořích	laboratoř	k1gFnPc6	laboratoř
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
značně	značně	k6eAd1	značně
riziková	rizikový	k2eAgFnSc1d1	riziková
a	a	k8xC	a
podíl	podíl	k1gInSc4	podíl
infekcí	infekce	k1gFnPc2	infekce
laboratorních	laboratorní	k2eAgMnPc2d1	laboratorní
pracovníků	pracovník	k1gMnPc2	pracovník
představuje	představovat	k5eAaImIp3nS	představovat
zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
%	%	kIx~	%
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
případů	případ	k1gInPc2	případ
onemocnění	onemocnění	k1gNnSc2	onemocnění
brucelózou	brucelóza	k1gFnSc7	brucelóza
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Brucelóza	brucelóza	k1gFnSc1	brucelóza
představuje	představovat	k5eAaImIp3nS	představovat
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nebezpečných	bezpečný	k2eNgNnPc2d1	nebezpečné
alimentárních	alimentární	k2eAgNnPc2d1	alimentární
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
dochází	docházet	k5eAaImIp3nS	docházet
konzumací	konzumace	k1gFnSc7	konzumace
mléka	mléko	k1gNnSc2	mléko
a	a	k8xC	a
mléčných	mléčný	k2eAgInPc2d1	mléčný
výrobků	výrobek	k1gInPc2	výrobek
z	z	k7c2	z
nepasterizovaného	pasterizovaný	k2eNgNnSc2d1	nepasterizované
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rizikovými	rizikový	k2eAgInPc7d1	rizikový
výrobky	výrobek	k1gInPc7	výrobek
je	být	k5eAaImIp3nS	být
máslo	máslo	k1gNnSc1	máslo
<g/>
,	,	kIx,	,
zmrzlina	zmrzlina	k1gFnSc1	zmrzlina
a	a	k8xC	a
měkké	měkký	k2eAgInPc1d1	měkký
sýry	sýr	k1gInPc1	sýr
připravené	připravený	k2eAgInPc1d1	připravený
z	z	k7c2	z
tepelně	tepelně	k6eAd1	tepelně
neošetřeného	ošetřený	k2eNgNnSc2d1	neošetřené
mléka	mléko	k1gNnSc2	mléko
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
koz	koza	k1gFnPc2	koza
<g/>
,	,	kIx,	,
skotu	skot	k1gInSc2	skot
nebo	nebo	k8xC	nebo
velbloudů	velbloud	k1gMnPc2	velbloud
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
měkké	měkký	k2eAgInPc1d1	měkký
sýry	sýr	k1gInPc1	sýr
z	z	k7c2	z
nepasterizovaného	pasterizovaný	k2eNgNnSc2d1	nepasterizované
mléka	mléko	k1gNnSc2	mléko
jsou	být	k5eAaImIp3nP	být
častým	častý	k2eAgInSc7d1	častý
zdrojem	zdroj	k1gInSc7	zdroj
infekce	infekce	k1gFnSc2	infekce
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
a	a	k8xC	a
na	na	k7c6	na
Středním	střední	k2eAgInSc6d1	střední
Východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
technologickém	technologický	k2eAgNnSc6d1	Technologické
zpracování	zpracování	k1gNnSc6	zpracování
měkkých	měkký	k2eAgInPc2d1	měkký
sýrů	sýr	k1gInPc2	sýr
(	(	kIx(	(
<g/>
např.	např.	kA	např.
balkánský	balkánský	k2eAgInSc1d1	balkánský
sýr	sýr	k1gInSc1	sýr
<g/>
)	)	kIx)	)
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zkoncentrování	zkoncentrování	k1gNnSc3	zkoncentrování
brucel	brucet	k5eAaPmAgInS	brucet
v	v	k7c6	v
sýru	sýr	k1gInSc6	sýr
a	a	k8xC	a
ty	ten	k3xDgFnPc1	ten
pak	pak	k6eAd1	pak
vydrží	vydržet	k5eAaPmIp3nP	vydržet
uvnitř	uvnitř	k7c2	uvnitř
sýru	sýr	k1gInSc2	sýr
infekceschopné	infekceschopný	k2eAgFnPc4d1	infekceschopný
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
mléčné	mléčný	k2eAgInPc1d1	mléčný
výrobky	výrobek	k1gInPc1	výrobek
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
sýry	sýr	k1gInPc1	sýr
<g/>
,	,	kIx,	,
kysané	kysaný	k2eAgInPc1d1	kysaný
mléčné	mléčný	k2eAgInPc1d1	mléčný
výrobky	výrobek	k1gInPc1	výrobek
představují	představovat	k5eAaImIp3nP	představovat
daleko	daleko	k6eAd1	daleko
nižší	nízký	k2eAgNnSc4d2	nižší
riziko	riziko	k1gNnSc4	riziko
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
bakterie	bakterie	k1gFnPc1	bakterie
nepřežívají	přežívat	k5eNaImIp3nP	přežívat
proces	proces	k1gInSc4	proces
fermentace	fermentace	k1gFnSc2	fermentace
<g/>
.	.	kIx.	.
</s>
<s>
Svalovina	svalovina	k1gFnSc1	svalovina
nemocných	nemocný	k2eAgNnPc2d1	nemocný
zvířat	zvíře	k1gNnPc2	zvíře
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
nízké	nízký	k2eAgInPc4d1	nízký
počty	počet	k1gInPc4	počet
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
nepředstavuje	představovat	k5eNaImIp3nS	představovat
riziko	riziko	k1gNnSc1	riziko
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
málokteré	málokterý	k3yIgInPc1	málokterý
masné	masný	k2eAgInPc1d1	masný
výrobky	výrobek	k1gInPc1	výrobek
se	se	k3xPyFc4	se
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
syrové	syrový	k2eAgFnPc1d1	syrová
<g/>
.	.	kIx.	.
</s>
<s>
Určité	určitý	k2eAgNnSc4d1	určité
riziko	riziko	k1gNnSc4	riziko
mohou	moct	k5eAaImIp3nP	moct
představovat	představovat	k5eAaImF	představovat
orgány	orgán	k1gInPc1	orgán
a	a	k8xC	a
krev	krev	k1gFnSc1	krev
z	z	k7c2	z
nemocných	nemocný	k1gMnPc2	nemocný
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
připravovat	připravovat	k5eAaImF	připravovat
pokrmy	pokrm	k1gInPc4	pokrm
ze	z	k7c2	z
syrových	syrový	k2eAgNnPc2d1	syrové
jater	játra	k1gNnPc2	játra
nebo	nebo	k8xC	nebo
přimíchávat	přimíchávat	k5eAaImF	přimíchávat
krev	krev	k1gFnSc4	krev
do	do	k7c2	do
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
přímým	přímý	k2eAgInSc7d1	přímý
kontaktem	kontakt	k1gInSc7	kontakt
s	s	k7c7	s
infikovaným	infikovaný	k2eAgNnSc7d1	infikované
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
je	být	k5eAaImIp3nS	být
přenosná	přenosný	k2eAgFnSc1d1	přenosná
z	z	k7c2	z
matky	matka	k1gFnSc2	matka
na	na	k7c4	na
plod	plod	k1gInSc4	plod
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
epizootologickým	epizootologický	k2eAgInSc7d1	epizootologický
faktorem	faktor	k1gInSc7	faktor
brucelózy	brucelóza	k1gFnSc2	brucelóza
zvířat	zvíře	k1gNnPc2	zvíře
je	být	k5eAaImIp3nS	být
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pohlavně	pohlavně	k6eAd1	pohlavně
přenosná	přenosný	k2eAgFnSc1d1	přenosná
<g/>
.	.	kIx.	.
</s>
<s>
Plemenní	plemenný	k2eAgMnPc1d1	plemenný
býci	býk	k1gMnPc1	býk
<g/>
,	,	kIx,	,
kanci	kanec	k1gMnPc1	kanec
<g/>
,	,	kIx,	,
berani	beran	k1gMnPc1	beran
ale	ale	k8xC	ale
i	i	k8xC	i
psi	pes	k1gMnPc1	pes
mohou	moct	k5eAaImIp3nP	moct
vylučovat	vylučovat	k5eAaImF	vylučovat
brucely	brucet	k5eAaPmAgFnP	brucet
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
semeni	semeno	k1gNnSc6	semeno
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
nebezpečným	bezpečný	k2eNgInSc7d1	nebezpečný
zdrojem	zdroj	k1gInSc7	zdroj
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
pečlivé	pečlivý	k2eAgNnSc4d1	pečlivé
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
zvířecích	zvířecí	k2eAgMnPc2d1	zvířecí
samců	samec	k1gMnPc2	samec
určených	určený	k2eAgMnPc2d1	určený
k	k	k7c3	k
plemenitbě	plemenitba	k1gFnSc3	plemenitba
<g/>
.	.	kIx.	.
</s>
<s>
Brucely	Brucet	k5eAaImAgFnP	Brucet
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
poraněnou	poraněný	k2eAgFnSc7d1	poraněná
kůží	kůže	k1gFnSc7	kůže
<g/>
,	,	kIx,	,
spojivkou	spojivka	k1gFnSc7	spojivka
<g/>
,	,	kIx,	,
respiračním	respirační	k2eAgInSc7d1	respirační
traktem	trakt	k1gInSc7	trakt
či	či	k8xC	či
alimentární	alimentární	k2eAgFnSc7d1	alimentární
cestou	cesta	k1gFnSc7	cesta
(	(	kIx(	(
<g/>
ústy	ústa	k1gNnPc7	ústa
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
sliznice	sliznice	k1gFnPc4	sliznice
trávicího	trávicí	k2eAgNnSc2d1	trávicí
ústrojí	ústrojí	k1gNnSc2	ústrojí
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
Brucella	Brucella	k1gFnSc1	Brucella
spp	spp	k?	spp
<g/>
.	.	kIx.	.
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
proniknutí	proniknutí	k1gNnSc6	proniknutí
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
pohlceny	pohltit	k5eAaPmNgInP	pohltit
fagocytujícími	fagocytující	k2eAgFnPc7d1	fagocytující
buňkami	buňka	k1gFnPc7	buňka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
makrofágy	makrofág	k1gInPc4	makrofág
a	a	k8xC	a
neutrofily	neutrofil	k1gMnPc4	neutrofil
<g/>
.	.	kIx.	.
</s>
<s>
Lymfatickými	lymfatický	k2eAgFnPc7d1	lymfatická
cestami	cesta	k1gFnPc7	cesta
jsou	být	k5eAaImIp3nP	být
zaneseny	zanést	k5eAaPmNgFnP	zanést
do	do	k7c2	do
mízních	mízní	k2eAgFnPc2d1	mízní
uzlin	uzlina	k1gFnPc2	uzlina
či	či	k8xC	či
lymfatických	lymfatický	k2eAgFnPc2d1	lymfatická
buněk	buňka	k1gFnPc2	buňka
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
sleziny	slezina	k1gFnSc2	slezina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
primárnímu	primární	k2eAgNnSc3d1	primární
pomnožení	pomnožení	k1gNnSc3	pomnožení
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
šíření	šíření	k1gNnSc3	šíření
bakterií	bakterie	k1gFnPc2	bakterie
po	po	k7c6	po
těle	tělo	k1gNnSc6	tělo
pomocí	pomocí	k7c2	pomocí
krve	krev	k1gFnSc2	krev
-	-	kIx~	-
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
bakteriémii	bakteriémie	k1gFnSc6	bakteriémie
<g/>
.	.	kIx.	.
</s>
<s>
Brucely	Brucet	k5eAaPmAgFnP	Brucet
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgMnPc4d1	schopný
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
přežívat	přežívat	k5eAaImF	přežívat
a	a	k8xC	a
množit	množit	k5eAaImF	množit
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
monocytů	monocyt	k1gInPc2	monocyt
<g/>
,	,	kIx,	,
makrofágů	makrofág	k1gInPc2	makrofág
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
orgánových	orgánový	k2eAgInPc6d1	orgánový
systémech	systém	k1gInPc6	systém
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
schopnost	schopnost	k1gFnSc4	schopnost
intracelulárního	intracelulární	k2eAgNnSc2d1	intracelulární
přežívání	přežívání	k1gNnSc2	přežívání
je	být	k5eAaImIp3nS	být
umožněna	umožnit	k5eAaPmNgFnS	umožnit
řadou	řada	k1gFnSc7	řada
faktorů	faktor	k1gInPc2	faktor
virulence	virulence	k1gFnSc2	virulence
<g/>
.	.	kIx.	.
</s>
<s>
Brucely	Brucet	k5eAaPmAgFnP	Brucet
zabraňují	zabraňovat	k5eAaImIp3nP	zabraňovat
uvnitř	uvnitř	k7c2	uvnitř
makrofágu	makrofág	k1gInSc2	makrofág
fúzi	fúze	k1gFnSc4	fúze
fagozómu	fagozóm	k1gInSc2	fagozóm
s	s	k7c7	s
lyzozomem	lyzozom	k1gInSc7	lyzozom
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgMnPc4d1	schopný
ve	v	k7c6	v
fagozómu	fagozómo	k1gNnSc6	fagozómo
přežívat	přežívat	k5eAaImF	přežívat
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
replikovat	replikovat	k5eAaImF	replikovat
<g/>
.	.	kIx.	.
</s>
<s>
Predilekčním	predilekční	k2eAgNnSc7d1	predilekční
místem	místo	k1gNnSc7	místo
dalšího	další	k2eAgNnSc2d1	další
množení	množení	k1gNnSc2	množení
bývá	bývat	k5eAaImIp3nS	bývat
placenta	placenta	k1gFnSc1	placenta
a	a	k8xC	a
plodové	plodový	k2eAgInPc1d1	plodový
obaly	obal	k1gInPc1	obal
březích	březí	k2eAgFnPc2d1	březí
samic	samice	k1gFnPc2	samice
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
tkáň	tkáň	k1gFnSc4	tkáň
varlat	varle	k1gNnPc2	varle
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
imunitních	imunitní	k2eAgInPc2d1	imunitní
obranných	obranný	k2eAgInPc2d1	obranný
mechanismů	mechanismus	k1gInPc2	mechanismus
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
buňkami	buňka	k1gFnPc7	buňka
zprostředkovaná	zprostředkovaný	k2eAgFnSc1d1	zprostředkovaná
imunita	imunita	k1gFnSc1	imunita
typu	typ	k1gInSc2	typ
Th-	Th-	k1gFnSc2	Th-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
faktorem	faktor	k1gInSc7	faktor
virulence	virulence	k1gFnSc2	virulence
brucel	brucet	k5eAaImAgInS	brucet
je	on	k3xPp3gMnPc4	on
lipopolysacharidový	lipopolysacharidový	k2eAgInSc1d1	lipopolysacharidový
antigen	antigen	k1gInSc1	antigen
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k9	jako
S-LPS	S-LPS	k1gMnPc2	S-LPS
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
smooth	smooth	k1gInSc1	smooth
lipopolysacharide	lipopolysacharid	k1gInSc5	lipopolysacharid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
invazi	invaze	k1gFnSc4	invaze
bakterií	bakterie	k1gFnPc2	bakterie
do	do	k7c2	do
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc4	jejich
přežívání	přežívání	k1gNnSc4	přežívání
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jsou	být	k5eAaImIp3nP	být
proti	proti	k7c3	proti
tomuto	tento	k3xDgInSc3	tento
antigenu	antigen	k1gInSc3	antigen
imunitním	imunitní	k2eAgInSc7d1	imunitní
systémem	systém	k1gInSc7	systém
hostitele	hostitel	k1gMnSc2	hostitel
produkovány	produkován	k2eAgFnPc4d1	produkována
specifické	specifický	k2eAgFnPc4d1	specifická
protilátky	protilátka	k1gFnPc4	protilátka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
také	také	k9	také
S-LPS	S-LPS	k1gMnSc1	S-LPS
izolovaný	izolovaný	k2eAgMnSc1d1	izolovaný
z	z	k7c2	z
buněk	buňka	k1gFnPc2	buňka
brucel	brucet	k5eAaImAgInS	brucet
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
imunodiagnostice	imunodiagnostika	k1gFnSc6	imunodiagnostika
<g/>
.	.	kIx.	.
</s>
<s>
Brucelóza	brucelóza	k1gFnSc1	brucelóza
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
multifunkčním	multifunkční	k2eAgNnSc7d1	multifunkční
onemocněním	onemocnění	k1gNnSc7	onemocnění
zasahujícím	zasahující	k2eAgNnSc7d1	zasahující
více	hodně	k6eAd2	hodně
orgánových	orgánový	k2eAgInPc2d1	orgánový
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
klinická	klinický	k2eAgFnSc1d1	klinická
manifestace	manifestace	k1gFnSc1	manifestace
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
.	.	kIx.	.
</s>
<s>
Příznakem	příznak	k1gInSc7	příznak
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
četností	četnost	k1gFnSc7	četnost
u	u	k7c2	u
nemocných	nemocný	k1gMnPc2	nemocný
brucelózou	brucelóza	k1gFnSc7	brucelóza
je	být	k5eAaImIp3nS	být
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zjišťována	zjišťovat	k5eAaImNgNnP	zjišťovat
u	u	k7c2	u
více	hodně	k6eAd2	hodně
než	než	k8xS	než
93	[number]	k4	93
%	%	kIx~	%
pacientů	pacient	k1gMnPc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejčetnější	četný	k2eAgInPc4d3	nejčetnější
projevy	projev	k1gInPc4	projev
brucelózy	brucelóza	k1gFnSc2	brucelóza
aborty	abort	k1gInPc4	abort
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
a	a	k8xC	a
záněty	zánět	k1gInPc4	zánět
varlat	varle	k1gNnPc2	varle
a	a	k8xC	a
nadvarlat	nadvarle	k1gNnPc2	nadvarle
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
obyčejně	obyčejně	k6eAd1	obyčejně
od	od	k7c2	od
1	[number]	k4	1
až	až	k9	až
do	do	k7c2	do
8	[number]	k4	8
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
je	být	k5eAaImIp3nS	být
multisystémové	multisystémový	k2eAgNnSc1d1	multisystémové
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
postihnout	postihnout	k5eAaPmF	postihnout
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgInPc4	všechen
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Brucelóza	brucelóza	k1gFnSc1	brucelóza
mívá	mívat	k5eAaImIp3nS	mívat
zpravidla	zpravidla	k6eAd1	zpravidla
chronický	chronický	k2eAgInSc1d1	chronický
průběh	průběh	k1gInSc1	průběh
<g/>
,	,	kIx,	,
mortalita	mortalita	k1gFnSc1	mortalita
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
do	do	k7c2	do
5	[number]	k4	5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Fatální	fatální	k2eAgFnPc1d1	fatální
infekce	infekce	k1gFnPc1	infekce
jsou	být	k5eAaImIp3nP	být
důsledkem	důsledek	k1gInSc7	důsledek
sekundárních	sekundární	k2eAgFnPc2d1	sekundární
komplikací	komplikace	k1gFnPc2	komplikace
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
endokarditidy	endokarditida	k1gFnSc2	endokarditida
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
virulentní	virulentní	k2eAgMnSc1d1	virulentní
B.	B.	kA	B.
melitensis	melitensis	k1gInSc4	melitensis
<g/>
,	,	kIx,	,
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
pak	pak	k6eAd1	pak
B.	B.	kA	B.
suis	suisa	k1gFnPc2	suisa
biotyp	biotyp	k1gMnSc1	biotyp
3	[number]	k4	3
a	a	k8xC	a
B.	B.	kA	B.
abortus	abortus	k1gInSc1	abortus
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
symptomy	symptom	k1gInPc4	symptom
brucelózy	brucelóza	k1gFnSc2	brucelóza
patří	patřit	k5eAaImIp3nP	patřit
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
slabost	slabost	k1gFnSc1	slabost
<g/>
,	,	kIx,	,
malátnost	malátnost	k1gFnSc1	malátnost
<g/>
,	,	kIx,	,
nevolnost	nevolnost	k1gFnSc4	nevolnost
<g/>
,	,	kIx,	,
nadměrné	nadměrný	k2eAgNnSc4d1	nadměrné
pocení	pocení	k1gNnSc4	pocení
či	či	k8xC	či
nechutenství	nechutenství	k1gNnSc4	nechutenství
<g/>
.	.	kIx.	.
</s>
<s>
Horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
mezi	mezi	k7c4	mezi
nejfrekventovanější	frekventovaný	k2eAgInPc4d3	nejfrekventovanější
klinické	klinický	k2eAgInPc4d1	klinický
příznaky	příznak	k1gInPc4	příznak
brucelózy	brucelóza	k1gFnSc2	brucelóza
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
kolísavá	kolísavý	k2eAgFnSc1d1	kolísavá
a	a	k8xC	a
přerušovaná	přerušovaný	k2eAgFnSc1d1	přerušovaná
(	(	kIx(	(
<g/>
ve	v	k7c6	v
vlnách	vlna	k1gFnPc6	vlna
-	-	kIx~	-
odtud	odtud	k6eAd1	odtud
vlnitá	vlnitý	k2eAgFnSc1d1	vlnitá
horečka	horečka	k1gFnSc1	horečka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
postižení	postižení	k1gNnSc6	postižení
pohybového	pohybový	k2eAgInSc2d1	pohybový
aparátu	aparát	k1gInSc2	aparát
pacienti	pacient	k1gMnPc1	pacient
pociťují	pociťovat	k5eAaImIp3nP	pociťovat
bolesti	bolest	k1gFnPc4	bolest
svalů	sval	k1gInPc2	sval
a	a	k8xC	a
kloubů	kloub	k1gInPc2	kloub
příznačné	příznačný	k2eAgNnSc1d1	příznačné
pro	pro	k7c4	pro
artritidy	artritida	k1gFnPc4	artritida
<g/>
,	,	kIx,	,
záněty	zánět	k1gInPc4	zánět
šlach	šlacha	k1gFnPc2	šlacha
a	a	k8xC	a
šlachových	šlachový	k2eAgFnPc2d1	šlachová
pochev	pochva	k1gFnPc2	pochva
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
neuropsychických	neuropsychický	k2eAgInPc2d1	neuropsychický
symptomů	symptom	k1gInPc2	symptom
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
bolesti	bolest	k1gFnPc4	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
únava	únava	k1gFnSc1	únava
a	a	k8xC	a
deprese	deprese	k1gFnSc1	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
příznaků	příznak	k1gInPc2	příznak
jmenujme	jmenovat	k5eAaImRp1nP	jmenovat
bolesti	bolest	k1gFnPc4	bolest
břicha	břich	k1gInSc2	břich
<g/>
,	,	kIx,	,
průjem	průjem	k1gInSc1	průjem
či	či	k8xC	či
zvracení	zvracení	k1gNnSc1	zvracení
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
postižení	postižení	k1gNnSc2	postižení
nervového	nervový	k2eAgInSc2d1	nervový
aparátu	aparát	k1gInSc2	aparát
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
závrať	závrať	k1gFnSc4	závrať
<g/>
,	,	kIx,	,
retence	retence	k1gFnSc1	retence
(	(	kIx(	(
<g/>
zadržení	zadržení	k1gNnSc1	zadržení
<g/>
)	)	kIx)	)
moči	moč	k1gFnPc1	moč
<g/>
,	,	kIx,	,
přechodné	přechodný	k2eAgNnSc1d1	přechodné
ochrnutí	ochrnutí	k1gNnSc1	ochrnutí
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
klinickém	klinický	k2eAgNnSc6d1	klinické
vyšetření	vyšetření	k1gNnSc6	vyšetření
bývá	bývat	k5eAaImIp3nS	bývat
nejčastěji	často	k6eAd3	často
diagnostikována	diagnostikován	k2eAgFnSc1d1	diagnostikována
hepatosplenomegalie	hepatosplenomegalie	k1gFnSc1	hepatosplenomegalie
(	(	kIx(	(
<g/>
patologické	patologický	k2eAgNnSc1d1	patologické
zvětšení	zvětšení	k1gNnSc1	zvětšení
jater	játra	k1gNnPc2	játra
a	a	k8xC	a
sleziny	slezina	k1gFnSc2	slezina
<g/>
)	)	kIx)	)
či	či	k8xC	či
bradykardie	bradykardie	k1gFnSc1	bradykardie
(	(	kIx(	(
<g/>
zpomalená	zpomalený	k2eAgFnSc1d1	zpomalená
srdeční	srdeční	k2eAgFnSc1d1	srdeční
činnost	činnost	k1gFnSc1	činnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
ustrojí	ustrojit	k5eAaPmIp3nS	ustrojit
vede	vést	k5eAaImIp3nS	vést
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
k	k	k7c3	k
zánětům	zánět	k1gInPc3	zánět
varlat	varle	k1gNnPc2	varle
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
trvalou	trvalý	k2eAgFnSc4d1	trvalá
neplodnost	neplodnost	k1gFnSc4	neplodnost
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
těhotných	těhotný	k2eAgFnPc2d1	těhotná
žen	žena	k1gFnPc2	žena
mohou	moct	k5eAaImIp3nP	moct
zapříčinit	zapříčinit	k5eAaPmF	zapříčinit
potraty	potrat	k1gInPc4	potrat
či	či	k8xC	či
postižení	postižení	k1gNnSc4	postižení
narozených	narozený	k2eAgFnPc2d1	narozená
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
brucelóza	brucelóza	k1gFnSc1	brucelóza
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
potraty	potrat	k1gInPc4	potrat
častěji	často	k6eAd2	často
než	než	k8xS	než
jiné	jiný	k2eAgFnPc1d1	jiná
patogenní	patogenní	k2eAgFnPc1d1	patogenní
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
u	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
má	mít	k5eAaImIp3nS	mít
brucelóza	brucelóza	k1gFnSc1	brucelóza
chronický	chronický	k2eAgInSc1d1	chronický
průběh	průběh	k1gInSc4	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnPc1	infekce
mohou	moct	k5eAaImIp3nP	moct
probíhat	probíhat	k5eAaImF	probíhat
často	často	k6eAd1	často
subklinicky	subklinicky	k6eAd1	subklinicky
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
příznaků	příznak	k1gInPc2	příznak
<g/>
)	)	kIx)	)
a	a	k8xC	a
aborty	abort	k1gInPc1	abort
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
jediným	jediný	k2eAgInSc7d1	jediný
příznakem	příznak	k1gInSc7	příznak
onemocnění	onemocnění	k1gNnSc2	onemocnění
ve	v	k7c6	v
stádě	stádo	k1gNnSc6	stádo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
skotu	skot	k1gInSc2	skot
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
potratům	potrat	k1gInPc3	potrat
v	v	k7c6	v
kterémkoliv	kterýkoliv	k3yIgNnSc6	kterýkoliv
stádiu	stádium	k1gNnSc6	stádium
gravidity	gravidita	k1gFnSc2	gravidita
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
býků	býk	k1gMnPc2	býk
se	se	k3xPyFc4	se
onemocnění	onemocnění	k1gNnSc1	onemocnění
manifestuje	manifestovat	k5eAaBmIp3nS	manifestovat
sporadicky	sporadicky	k6eAd1	sporadicky
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
zánětu	zánět	k1gInSc2	zánět
varlat	varle	k1gNnPc2	varle
a	a	k8xC	a
nadvarlat	nadvarle	k1gNnPc2	nadvarle
<g/>
.	.	kIx.	.
</s>
<s>
Infikovaní	infikovaný	k2eAgMnPc1d1	infikovaný
býci	býk	k1gMnPc1	býk
jsou	být	k5eAaImIp3nP	být
zdrojem	zdroj	k1gInSc7	zdroj
infekce	infekce	k1gFnSc2	infekce
pro	pro	k7c4	pro
připouštěné	připouštěný	k2eAgFnPc4d1	připouštěná
krávy	kráva	k1gFnPc4	kráva
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
neplodní	plodní	k2eNgMnPc1d1	neplodní
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
epizootologického	epizootologický	k2eAgNnSc2d1	epizootologické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
bezpříznakové	bezpříznakový	k2eAgNnSc1d1	bezpříznakové
nosičství	nosičství	k1gNnSc1	nosičství
infekce	infekce	k1gFnSc2	infekce
u	u	k7c2	u
býků	býk	k1gMnPc2	býk
tou	ten	k3xDgFnSc7	ten
nejnebezpečnější	bezpečný	k2eNgFnSc7d3	nejnebezpečnější
formou	forma	k1gFnSc7	forma
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
obou	dva	k4xCgNnPc2	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
artritidy	artritida	k1gFnPc4	artritida
<g/>
,	,	kIx,	,
záněty	zánět	k1gInPc4	zánět
šlachových	šlachový	k2eAgFnPc2d1	šlachová
pochev	pochva	k1gFnPc2	pochva
a	a	k8xC	a
podkožní	podkožní	k2eAgInPc4d1	podkožní
záněty	zánět	k1gInPc4	zánět
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
koz	koza	k1gFnPc2	koza
se	se	k3xPyFc4	se
brucelóza	brucelóza	k1gFnSc1	brucelóza
projevuje	projevovat	k5eAaImIp3nS	projevovat
potraty	potrat	k1gInPc4	potrat
mezi	mezi	k7c7	mezi
3	[number]	k4	3
<g/>
.	.	kIx.	.
až	až	k9	až
5	[number]	k4	5
<g/>
.	.	kIx.	.
měsícem	měsíc	k1gInSc7	měsíc
gravidity	gravidita	k1gFnSc2	gravidita
<g/>
.	.	kIx.	.
</s>
<s>
Berany	Beran	k1gMnPc4	Beran
postihuje	postihovat	k5eAaImIp3nS	postihovat
zánět	zánět	k1gInSc1	zánět
varlat	varle	k1gNnPc2	varle
a	a	k8xC	a
nadvarlat	nadvarle	k1gNnPc2	nadvarle
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
prasnic	prasnice	k1gFnPc2	prasnice
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
abortům	abort	k1gInPc3	abort
mezi	mezi	k7c4	mezi
4	[number]	k4	4
<g/>
.	.	kIx.	.
až	až	k9	až
12	[number]	k4	12
<g/>
.	.	kIx.	.
týdnu	týden	k1gInSc6	týden
gravidity	gravidita	k1gFnSc2	gravidita
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kanců	kanec	k1gMnPc2	kanec
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
zánět	zánět	k1gInSc4	zánět
varlat	varle	k1gNnPc2	varle
a	a	k8xC	a
nadvarlat	nadvarle	k1gNnPc2	nadvarle
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zajíců	zajíc	k1gMnPc2	zajíc
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
jsou	být	k5eAaImIp3nP	být
vnímaví	vnímavý	k2eAgMnPc1d1	vnímavý
k	k	k7c3	k
prasečí	prasečí	k2eAgMnSc1d1	prasečí
B.	B.	kA	B.
suis	suis	k1gInSc4	suis
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
brucelóza	brucelóza	k1gFnSc1	brucelóza
manifestuje	manifestovat	k5eAaBmIp3nS	manifestovat
krom	krom	k7c2	krom
abortů	abort	k1gInPc2	abort
také	také	k9	také
poševním	poševní	k2eAgInSc7d1	poševní
výtokem	výtok	k1gInSc7	výtok
<g/>
,	,	kIx,	,
zánětem	zánět	k1gInSc7	zánět
varlat	varle	k1gNnPc2	varle
a	a	k8xC	a
nekrózami	nekróza	k1gFnPc7	nekróza
na	na	k7c6	na
pohlavních	pohlavní	k2eAgInPc6d1	pohlavní
orgánech	orgán	k1gInPc6	orgán
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
psů	pes	k1gMnPc2	pes
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
počáteční	počáteční	k2eAgFnSc6d1	počáteční
fázi	fáze	k1gFnSc6	fáze
infekce	infekce	k1gFnSc2	infekce
B.	B.	kA	B.
canis	canis	k1gFnSc4	canis
k	k	k7c3	k
přechodnému	přechodný	k2eAgNnSc3d1	přechodné
zvětšení	zvětšení	k1gNnSc3	zvětšení
mízních	mízní	k2eAgFnPc2d1	mízní
uzlin	uzlina	k1gFnPc2	uzlina
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc1	zvýšení
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgInSc1d1	typický
je	být	k5eAaImIp3nS	být
abort	abort	k1gInSc1	abort
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
45	[number]	k4	45
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
49	[number]	k4	49
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
březosti	březost	k1gFnSc2	březost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
možný	možný	k2eAgInSc4d1	možný
během	během	k7c2	během
celé	celý	k2eAgFnSc2d1	celá
gravidity	gravidita	k1gFnSc2	gravidita
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
cyklus	cyklus	k1gInSc1	cyklus
nebývá	bývat	k5eNaImIp3nS	bývat
u	u	k7c2	u
fen	fena	k1gFnPc2	fena
narušen	narušit	k5eAaPmNgMnS	narušit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
psů	pes	k1gMnPc2	pes
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
otoky	otok	k1gInPc4	otok
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k9	i
bolestivé	bolestivý	k2eAgNnSc1d1	bolestivé
<g/>
)	)	kIx)	)
varlat	varle	k1gNnPc2	varle
<g/>
,	,	kIx,	,
kůže	kůže	k1gFnSc1	kůže
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
šourku	šourek	k1gInSc2	šourek
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
příznaky	příznak	k1gInPc4	příznak
dermatitidy	dermatitida	k1gFnSc2	dermatitida
<g/>
.	.	kIx.	.
</s>
<s>
Kloubní	kloubní	k2eAgNnSc1d1	kloubní
postižení	postižení	k1gNnSc1	postižení
či	či	k8xC	či
postižení	postižení	k1gNnSc1	postižení
nervového	nervový	k2eAgInSc2d1	nervový
aparátu	aparát	k1gInSc2	aparát
bývá	bývat	k5eAaImIp3nS	bývat
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
jen	jen	k6eAd1	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
chorob	choroba	k1gFnPc2	choroba
diagnostika	diagnostika	k1gFnSc1	diagnostika
brucelózy	brucelóza	k1gFnSc2	brucelóza
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
kombinaci	kombinace	k1gFnSc4	kombinace
anamnézy	anamnéza	k1gFnSc2	anamnéza
<g/>
,	,	kIx,	,
klinického	klinický	k2eAgNnSc2d1	klinické
vyšetření	vyšetření	k1gNnSc2	vyšetření
a	a	k8xC	a
laboratorního	laboratorní	k2eAgNnSc2d1	laboratorní
vyšetření	vyšetření	k1gNnSc2	vyšetření
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
laboratorních	laboratorní	k2eAgFnPc2d1	laboratorní
metod	metoda	k1gFnPc2	metoda
dominuje	dominovat	k5eAaImIp3nS	dominovat
jak	jak	k6eAd1	jak
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ve	v	k7c6	v
veterinární	veterinární	k2eAgFnSc6d1	veterinární
oblasti	oblast	k1gFnSc6	oblast
sérologická	sérologický	k2eAgFnSc1d1	sérologická
diagnostika	diagnostika	k1gFnSc1	diagnostika
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
průkazu	průkaz	k1gInSc6	průkaz
specifických	specifický	k2eAgFnPc2d1	specifická
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pozitivním	pozitivní	k2eAgNnSc6d1	pozitivní
sérologickém	sérologický	k2eAgNnSc6d1	sérologické
vyšetření	vyšetření	k1gNnSc6	vyšetření
se	se	k3xPyFc4	se
infekce	infekce	k1gFnSc1	infekce
konfirmuje	konfirmovat	k5eAaBmIp3nS	konfirmovat
kultivací	kultivace	k1gFnSc7	kultivace
původce	původce	k1gMnSc2	původce
nebo	nebo	k8xC	nebo
PCR	PCR	kA	PCR
<g/>
.	.	kIx.	.
</s>
<s>
Diagnostika	diagnostika	k1gFnSc1	diagnostika
brucelózy	brucelóza	k1gFnSc2	brucelóza
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
detailním	detailní	k2eAgNnSc6d1	detailní
klinickém	klinický	k2eAgNnSc6d1	klinické
vyšetření	vyšetření	k1gNnSc6	vyšetření
(	(	kIx(	(
<g/>
hematologické	hematologický	k2eAgNnSc4d1	hematologické
<g/>
,	,	kIx,	,
biochemické	biochemický	k2eAgNnSc4d1	biochemické
<g/>
,	,	kIx,	,
ultrasonografické	ultrasonografický	k2eAgNnSc4d1	ultrasonografické
<g/>
,	,	kIx,	,
rentgenologické	rentgenologický	k2eAgNnSc4d1	rentgenologické
vyšetření	vyšetření	k1gNnSc4	vyšetření
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
laboratorní	laboratorní	k2eAgFnSc7d1	laboratorní
diagnostikou	diagnostika	k1gFnSc7	diagnostika
původce	původce	k1gMnSc2	původce
<g/>
.	.	kIx.	.
</s>
<s>
Podezření	podezření	k1gNnSc4	podezření
na	na	k7c4	na
brucelózu	brucelóza	k1gFnSc4	brucelóza
vychází	vycházet	k5eAaImIp3nS	vycházet
prvně	prvně	k?	prvně
z	z	k7c2	z
anamnestických	anamnestický	k2eAgInPc2d1	anamnestický
údajů	údaj	k1gInPc2	údaj
od	od	k7c2	od
pacienta	pacient	k1gMnSc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
příznaky	příznak	k1gInPc7	příznak
jsou	být	k5eAaImIp3nP	být
dlouhotrvající	dlouhotrvající	k2eAgFnSc1d1	dlouhotrvající
opakující	opakující	k2eAgFnSc1d1	opakující
se	se	k3xPyFc4	se
horečka	horečka	k1gFnSc1	horečka
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
slabost	slabost	k1gFnSc1	slabost
<g/>
,	,	kIx,	,
bolesti	bolest	k1gFnPc1	bolest
svalů	sval	k1gInPc2	sval
a	a	k8xC	a
kloubů	kloub	k1gInPc2	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
prvkem	prvek	k1gInSc7	prvek
v	v	k7c6	v
anamnéze	anamnéza	k1gFnSc6	anamnéza
je	být	k5eAaImIp3nS	být
i	i	k9	i
zda	zda	k8xS	zda
pacient	pacient	k1gMnSc1	pacient
navštívil	navštívit	k5eAaPmAgMnS	navštívit
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
zemi	zem	k1gFnSc4	zem
s	s	k7c7	s
endemickým	endemický	k2eAgInSc7d1	endemický
výskytem	výskyt	k1gInSc7	výskyt
brucelózy	brucelóza	k1gFnSc2	brucelóza
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohl	moct	k5eAaImAgInS	moct
přijít	přijít	k5eAaPmF	přijít
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
nemocnými	mocný	k2eNgNnPc7d1	nemocné
zvířaty	zvíře	k1gNnPc7	zvíře
či	či	k8xC	či
konzumovat	konzumovat	k5eAaBmF	konzumovat
mléko	mléko	k1gNnSc4	mléko
a	a	k8xC	a
mléčné	mléčný	k2eAgInPc4d1	mléčný
výrobky	výrobek	k1gInPc4	výrobek
z	z	k7c2	z
nepasterizovaného	pasterizovaný	k2eNgNnSc2d1	nepasterizované
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
brucelóza	brucelóza	k1gFnSc1	brucelóza
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
zohlednit	zohlednit	k5eAaPmF	zohlednit
i	i	k9	i
povolání	povolání	k1gNnSc1	povolání
vyšetřovaného	vyšetřovaný	k2eAgMnSc2d1	vyšetřovaný
pacienta	pacient	k1gMnSc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělci	zemědělec	k1gMnPc1	zemědělec
<g/>
,	,	kIx,	,
chovatelé	chovatel	k1gMnPc1	chovatel
dobytka	dobytek	k1gInSc2	dobytek
a	a	k8xC	a
pracovníci	pracovník	k1gMnPc1	pracovník
jatek	jatka	k1gFnPc2	jatka
jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
mnohem	mnohem	k6eAd1	mnohem
vyššímu	vysoký	k2eAgNnSc3d2	vyšší
riziku	riziko	k1gNnSc3	riziko
infekce	infekce	k1gFnSc2	infekce
než	než	k8xS	než
ostatní	ostatní	k2eAgFnSc2d1	ostatní
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
klinickém	klinický	k2eAgNnSc6d1	klinické
vyšetření	vyšetření	k1gNnSc6	vyšetření
pacienta	pacient	k1gMnSc2	pacient
s	s	k7c7	s
brucelózou	brucelóza	k1gFnSc7	brucelóza
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
zjištěny	zjistit	k5eAaPmNgFnP	zjistit
<g/>
:	:	kIx,	:
hepatosplenomegalie	hepatosplenomegalie	k1gFnPc1	hepatosplenomegalie
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
zvětšení	zvětšení	k1gNnSc1	zvětšení
sleziny	slezina	k1gFnSc2	slezina
či	či	k8xC	či
zvětšení	zvětšení	k1gNnSc1	zvětšení
jater	játra	k1gNnPc2	játra
samostatně	samostatně	k6eAd1	samostatně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anémie	anémie	k1gFnSc1	anémie
<g/>
,	,	kIx,	,
zvětšení	zvětšení	k1gNnSc1	zvětšení
mízních	mízní	k2eAgFnPc2d1	mízní
uzlin	uzlina	k1gFnPc2	uzlina
<g/>
,	,	kIx,	,
endokarditida	endokarditida	k1gFnSc1	endokarditida
<g/>
,	,	kIx,	,
spondylitida	spondylitida	k1gFnSc1	spondylitida
<g/>
,	,	kIx,	,
abscesy	absces	k1gInPc1	absces
na	na	k7c6	na
játrech	játra	k1gNnPc6	játra
či	či	k8xC	či
slezině	slezina	k1gFnSc6	slezina
<g/>
,	,	kIx,	,
pneumonie	pneumonie	k1gFnSc1	pneumonie
<g/>
,	,	kIx,	,
bronchitida	bronchitida	k1gFnSc1	bronchitida
<g/>
,	,	kIx,	,
meningitida	meningitida	k1gFnSc1	meningitida
<g/>
,	,	kIx,	,
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
zánět	zánět	k1gInSc4	zánět
varlat	varle	k1gNnPc2	varle
a	a	k8xC	a
nadvarlat	nadvarle	k1gNnPc2	nadvarle
<g/>
,	,	kIx,	,
záněty	zánět	k1gInPc4	zánět
močových	močový	k2eAgFnPc2d1	močová
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
kožní	kožní	k2eAgFnSc2d1	kožní
změny	změna	k1gFnSc2	změna
atd.	atd.	kA	atd.
Rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
faktorem	faktor	k1gInSc7	faktor
při	při	k7c6	při
stanovení	stanovení	k1gNnSc6	stanovení
definitivní	definitivní	k2eAgFnSc2d1	definitivní
diagnózy	diagnóza	k1gFnSc2	diagnóza
je	být	k5eAaImIp3nS	být
však	však	k9	však
laboratorní	laboratorní	k2eAgFnSc2d1	laboratorní
izolace	izolace	k1gFnSc2	izolace
a	a	k8xC	a
určení	určení	k1gNnSc2	určení
původce	původce	k1gMnSc2	původce
<g/>
.	.	kIx.	.
</s>
<s>
Brucely	Brucet	k5eAaPmAgFnP	Brucet
se	se	k3xPyFc4	se
diagnostikují	diagnostikovat	k5eAaBmIp3nP	diagnostikovat
klasickou	klasický	k2eAgFnSc7d1	klasická
kultivací	kultivace	k1gFnSc7	kultivace
bakterií	bakterie	k1gFnPc2	bakterie
na	na	k7c6	na
agarech	agar	k1gInPc6	agar
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
barvením	barvení	k1gNnSc7	barvení
<g/>
,	,	kIx,	,
sérologicky	sérologicky	k6eAd1	sérologicky
a	a	k8xC	a
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
i	i	k9	i
molekulárními	molekulární	k2eAgFnPc7d1	molekulární
metodami	metoda	k1gFnPc7	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
Brucella	Brucella	k1gFnSc1	Brucella
spp	spp	k?	spp
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
izolují	izolovat	k5eAaBmIp3nP	izolovat
a	a	k8xC	a
kultivují	kultivovat	k5eAaImIp3nP	kultivovat
nejčastěji	často	k6eAd3	často
ze	z	k7c2	z
vzorků	vzorek	k1gInPc2	vzorek
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
běžných	běžný	k2eAgInPc2d1	běžný
krevních	krevní	k2eAgInPc2d1	krevní
agarů	agar	k1gInPc2	agar
lze	lze	k6eAd1	lze
brucely	brucet	k5eAaPmAgFnP	brucet
kultivovat	kultivovat	k5eAaImF	kultivovat
i	i	k9	i
na	na	k7c6	na
specifických	specifický	k2eAgFnPc6d1	specifická
půdách	půda	k1gFnPc6	půda
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Farrellovo	Farrellův	k2eAgNnSc4d1	Farrellův
médium	médium	k1gNnSc4	médium
<g/>
.	.	kIx.	.
</s>
<s>
Kultivace	kultivace	k1gFnSc1	kultivace
probíhá	probíhat	k5eAaImIp3nS	probíhat
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
37	[number]	k4	37
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
5	[number]	k4	5
%	%	kIx~	%
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
dominantní	dominantní	k2eAgFnSc4d1	dominantní
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
laboratorní	laboratorní	k2eAgFnSc6d1	laboratorní
diagnostice	diagnostika	k1gFnSc6	diagnostika
hrají	hrát	k5eAaImIp3nP	hrát
sérologické	sérologický	k2eAgFnPc1d1	sérologická
metody	metoda	k1gFnPc1	metoda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
přítomnost	přítomnost	k1gFnSc1	přítomnost
specifických	specifický	k2eAgFnPc2d1	specifická
protilátek	protilátka	k1gFnPc2	protilátka
proti	proti	k7c3	proti
brucelovému	brucelový	k2eAgInSc3d1	brucelový
antigenu	antigen	k1gInSc3	antigen
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejpoužívanější	používaný	k2eAgNnSc4d3	nejpoužívanější
patří	patřit	k5eAaImIp3nS	patřit
metody	metoda	k1gFnPc4	metoda
založené	založený	k2eAgFnPc4d1	založená
na	na	k7c4	na
aglutinaci	aglutinace	k1gFnSc4	aglutinace
(	(	kIx(	(
<g/>
Rose	Rose	k1gMnSc1	Rose
Bengal	Bengal	k1gMnSc1	Bengal
test	test	k1gMnSc1	test
<g/>
,	,	kIx,	,
pomalá	pomalý	k2eAgFnSc1d1	pomalá
zkumavková	zkumavkový	k2eAgFnSc1d1	zkumavkový
aglutinace	aglutinace	k1gFnSc1	aglutinace
<g/>
,	,	kIx,	,
Coombsův	Coombsův	k2eAgInSc1d1	Coombsův
test	test	k1gInSc1	test
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
reakci	reakce	k1gFnSc4	reakce
vazby	vazba	k1gFnSc2	vazba
komplementu	komplement	k1gInSc2	komplement
(	(	kIx(	(
<g/>
komplement	komplement	k1gInSc1	komplement
fixační	fixační	k2eAgInSc1d1	fixační
test	test	k1gInSc1	test
<g/>
)	)	kIx)	)
či	či	k8xC	či
enzymatické	enzymatický	k2eAgFnSc3d1	enzymatická
reakci	reakce	k1gFnSc3	reakce
(	(	kIx(	(
<g/>
ELISA	ELISA	kA	ELISA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
těchto	tento	k3xDgFnPc6	tento
metodách	metoda	k1gFnPc6	metoda
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
lipopolysacharidové	lipopolysacharid	k1gMnPc1	lipopolysacharid
(	(	kIx(	(
<g/>
S-LPS	S-LPS	k1gMnSc1	S-LPS
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Kapitola	kapitola	k1gFnSc1	kapitola
Patogeneze	patogeneze	k1gFnSc1	patogeneze
<g/>
)	)	kIx)	)
antigeny	antigen	k1gInPc1	antigen
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
se	se	k3xPyFc4	se
detekují	detekovat	k5eAaImIp3nP	detekovat
protilátky	protilátka	k1gFnPc1	protilátka
z	z	k7c2	z
vyšetřovaného	vyšetřovaný	k2eAgNnSc2d1	vyšetřované
lidského	lidský	k2eAgNnSc2d1	lidské
séra	sérum	k1gNnSc2	sérum
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
takovýto	takovýto	k3xDgInSc4	takovýto
S-LPS	S-LPS	k1gFnSc1	S-LPS
antigen	antigen	k1gInSc4	antigen
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
epitopy	epitop	k1gInPc4	epitop
společné	společný	k2eAgInPc4d1	společný
i	i	k9	i
pro	pro	k7c4	pro
jiné	jiný	k2eAgFnPc4d1	jiná
bakterie	bakterie	k1gFnPc4	bakterie
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
tedy	tedy	k9	tedy
docházet	docházet	k5eAaImF	docházet
ke	k	k7c3	k
zkříženým	zkřížený	k2eAgFnPc3d1	zkřížená
reakcím	reakce	k1gFnPc3	reakce
např.	např.	kA	např.
s	s	k7c7	s
bakteriemi	bakterie	k1gFnPc7	bakterie
Yersinia	Yersinium	k1gNnSc2	Yersinium
enterocolitica	enterocolitica	k6eAd1	enterocolitica
O	o	k7c6	o
<g/>
:	:	kIx,	:
<g/>
9	[number]	k4	9
nebo	nebo	k8xC	nebo
Escherichia	Escherichia	k1gFnSc1	Escherichia
coli	coli	k1gNnSc2	coli
O	o	k7c6	o
<g/>
:	:	kIx,	:
<g/>
159	[number]	k4	159
<g/>
.	.	kIx.	.
</s>
<s>
Obezřetná	obezřetný	k2eAgFnSc1d1	obezřetná
interpretace	interpretace	k1gFnSc1	interpretace
pozitivních	pozitivní	k2eAgInPc2d1	pozitivní
výsledků	výsledek	k1gInPc2	výsledek
testu	test	k1gInSc2	test
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
pro	pro	k7c4	pro
potvrzení	potvrzení	k1gNnSc4	potvrzení
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
nutné	nutný	k2eAgNnSc1d1	nutné
provést	provést	k5eAaPmF	provést
jiný	jiný	k2eAgInSc4d1	jiný
sérologický	sérologický	k2eAgInSc4d1	sérologický
test	test	k1gInSc4	test
nebo	nebo	k8xC	nebo
kultivaci	kultivace	k1gFnSc4	kultivace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
byla	být	k5eAaImAgFnS	být
publikována	publikován	k2eAgFnSc1d1	publikována
řada	řada	k1gFnSc1	řada
studií	studie	k1gFnPc2	studie
zabývajících	zabývající	k2eAgFnPc2d1	zabývající
se	se	k3xPyFc4	se
diagnostikou	diagnostika	k1gFnSc7	diagnostika
brucelózy	brucelóza	k1gFnPc1	brucelóza
pomocí	pomocí	k7c2	pomocí
metod	metoda	k1gFnPc2	metoda
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
především	především	k9	především
o	o	k7c4	o
detekci	detekce	k1gFnSc4	detekce
brucelové	brucelové	k2eAgNnSc2d1	brucelové
DNA	dno	k1gNnSc2	dno
z	z	k7c2	z
krve	krev	k1gFnSc2	krev
pacienta	pacient	k1gMnSc2	pacient
pomocí	pomocí	k7c2	pomocí
klasické	klasický	k2eAgFnSc2d1	klasická
"	"	kIx"	"
<g/>
end	end	k?	end
point	pointa	k1gFnPc2	pointa
PCR	PCR	kA	PCR
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgFnPc1d3	nejnovější
práce	práce	k1gFnPc1	práce
referují	referovat	k5eAaBmIp3nP	referovat
i	i	k9	i
o	o	k7c4	o
použití	použití	k1gNnSc4	použití
Real-time	Realim	k1gInSc5	Real-tim
PCR	PCR	kA	PCR
k	k	k7c3	k
detekci	detekce	k1gFnSc3	detekce
Brucella	Brucello	k1gNnSc2	Brucello
spp	spp	k?	spp
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
těchto	tento	k3xDgFnPc2	tento
metod	metoda	k1gFnPc2	metoda
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
citlivost	citlivost	k1gFnSc1	citlivost
<g/>
,	,	kIx,	,
přesnost	přesnost	k1gFnSc1	přesnost
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
i	i	k9	i
možnost	možnost	k1gFnSc4	možnost
druhového	druhový	k2eAgNnSc2d1	druhové
rozlišení	rozlišení	k1gNnSc2	rozlišení
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
B.	B.	kA	B.
melitensis	melitensis	k1gInSc4	melitensis
či	či	k8xC	či
B.	B.	kA	B.
abortus	abortus	k1gInSc1	abortus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zvířat	zvíře	k1gNnPc2	zvíře
se	se	k3xPyFc4	se
diagnostika	diagnostika	k1gFnSc1	diagnostika
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
na	na	k7c4	na
laboratorní	laboratorní	k2eAgNnSc4d1	laboratorní
určení	určení	k1gNnSc4	určení
původce	původce	k1gMnSc2	původce
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
vždy	vždy	k6eAd1	vždy
zjevné	zjevný	k2eAgNnSc1d1	zjevné
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
humánním	humánní	k2eAgNnSc6d1	humánní
lékařství	lékařství	k1gNnSc6	lékařství
je	být	k5eAaImIp3nS	být
i	i	k9	i
u	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
nejdůležitější	důležitý	k2eAgNnSc1d3	nejdůležitější
sérologické	sérologický	k2eAgNnSc1d1	sérologické
vyšetření	vyšetření	k1gNnSc1	vyšetření
<g/>
.	.	kIx.	.
</s>
<s>
OIE	OIE	kA	OIE
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
k	k	k7c3	k
diagnostice	diagnostika	k1gFnSc3	diagnostika
brucelózy	brucelóza	k1gFnSc2	brucelóza
tyto	tento	k3xDgFnPc4	tento
sérologické	sérologický	k2eAgFnPc4d1	sérologická
metody	metoda	k1gFnPc4	metoda
<g/>
:	:	kIx,	:
Rose	Rose	k1gMnSc1	Rose
Bengal	Bengal	k1gMnSc1	Bengal
test	test	k1gMnSc1	test
<g/>
,	,	kIx,	,
komplement	komplement	k1gInSc1	komplement
fixační	fixační	k2eAgFnSc2d1	fixační
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
ELISA	ELISA	kA	ELISA
(	(	kIx(	(
<g/>
nepřímá	přímý	k2eNgFnSc1d1	nepřímá
nebo	nebo	k8xC	nebo
kompetitivní	kompetitivní	k2eAgFnSc1d1	kompetitivní
<g/>
)	)	kIx)	)
a	a	k8xC	a
test	test	k1gInSc1	test
fluorescenční	fluorescenční	k2eAgFnSc2d1	fluorescenční
polarizace	polarizace	k1gFnSc2	polarizace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
detekci	detekce	k1gFnSc4	detekce
brucelózy	brucelóza	k1gFnSc2	brucelóza
ve	v	k7c6	v
vyšetřovaném	vyšetřovaný	k2eAgNnSc6d1	vyšetřované
mléce	mléko	k1gNnSc6	mléko
OIE	OIE	kA	OIE
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
kroužkový	kroužkový	k2eAgInSc1d1	kroužkový
test	test	k1gInSc1	test
mléka	mléko	k1gNnSc2	mléko
(	(	kIx(	(
<g/>
aglutinace	aglutinace	k1gFnSc1	aglutinace
ve	v	k7c6	v
zkumavce	zkumavka	k1gFnSc6	zkumavka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nepřímou	přímý	k2eNgFnSc4d1	nepřímá
ELISA	ELISA	kA	ELISA
metodu	metoda	k1gFnSc4	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
ze	z	k7c2	z
sérologických	sérologický	k2eAgFnPc2d1	sérologická
metod	metoda	k1gFnPc2	metoda
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
i	i	k8xC	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
testy	testa	k1gFnSc2	testa
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
principu	princip	k1gInSc6	princip
aglutinace	aglutinace	k1gFnSc2	aglutinace
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
jejich	jejich	k3xOp3gFnSc3	jejich
nízké	nízký	k2eAgFnSc3d1	nízká
ceně	cena	k1gFnSc3	cena
a	a	k8xC	a
rychlosti	rychlost	k1gFnSc3	rychlost
provedení	provedení	k1gNnSc2	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
-li	i	k?	-li
testované	testovaný	k2eAgNnSc4d1	testované
sérum	sérum	k1gNnSc4	sérum
specifické	specifický	k2eAgFnSc2d1	specifická
protilátky	protilátka	k1gFnSc2	protilátka
proti	proti	k7c3	proti
Brucella	Brucello	k1gNnPc1	Brucello
spp	spp	k?	spp
<g/>
.	.	kIx.	.
dojde	dojít	k5eAaPmIp3nS	dojít
po	po	k7c4	po
přidání	přidání	k1gNnSc4	přidání
antigenu	antigen	k1gInSc2	antigen
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
aglutinace	aglutinace	k1gFnSc2	aglutinace
(	(	kIx(	(
<g/>
okem	oke	k1gNnSc7	oke
pozorovatelný	pozorovatelný	k2eAgInSc4d1	pozorovatelný
zákal	zákal	k1gInSc4	zákal
či	či	k8xC	či
vločky	vločka	k1gFnPc4	vločka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
povinně	povinně	k6eAd1	povinně
sérologicky	sérologicky	k6eAd1	sérologicky
vyšetřovat	vyšetřovat	k5eAaImF	vyšetřovat
všichni	všechen	k3xTgMnPc1	všechen
býci	býk	k1gMnPc1	býk
<g/>
,	,	kIx,	,
berani	beran	k1gMnPc1	beran
<g/>
,	,	kIx,	,
kozli	kozel	k1gMnPc1	kozel
a	a	k8xC	a
kanci	kanec	k1gMnPc1	kanec
určení	určení	k1gNnSc2	určení
k	k	k7c3	k
plemenitbě	plemenitba	k1gFnSc3	plemenitba
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
vyšetřit	vyšetřit	k5eAaPmF	vyšetřit
každá	každý	k3xTgFnSc1	každý
samice	samice	k1gFnSc1	samice
u	u	k7c2	u
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
koz	koza	k1gFnPc2	koza
<g/>
,	,	kIx,	,
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
potratila	potratit	k5eAaPmAgNnP	potratit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyšetření	vyšetření	k1gNnSc3	vyšetření
se	se	k3xPyFc4	se
odebírají	odebírat	k5eAaImIp3nP	odebírat
vzorky	vzorek	k1gInPc1	vzorek
plodové	plodový	k2eAgFnSc2d1	plodová
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
plodové	plodový	k2eAgInPc4d1	plodový
obaly	obal	k1gInPc4	obal
nebo	nebo	k8xC	nebo
mrtvý	mrtvý	k2eAgInSc4d1	mrtvý
plod	plod	k1gInSc4	plod
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřují	vyšetřovat	k5eAaImIp3nP	vyšetřovat
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
odlovení	odlovený	k2eAgMnPc1d1	odlovený
zajíci	zajíc	k1gMnPc1	zajíc
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
rychlou	rychlý	k2eAgFnSc7d1	rychlá
sklíčkovou	sklíčkový	k2eAgFnSc7d1	sklíčkový
aglutinací	aglutinace	k1gFnSc7	aglutinace
<g/>
.	.	kIx.	.
</s>
<s>
Molekulární	molekulární	k2eAgFnPc1d1	molekulární
metody	metoda	k1gFnPc1	metoda
ve	v	k7c6	v
veterinární	veterinární	k2eAgFnSc6d1	veterinární
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
brucelózy	brucelóza	k1gFnSc2	brucelóza
z	z	k7c2	z
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
důvodů	důvod	k1gInPc2	důvod
zatím	zatím	k6eAd1	zatím
intenzivně	intenzivně	k6eAd1	intenzivně
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
brucelózy	brucelóza	k1gFnSc2	brucelóza
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
širokému	široký	k2eAgNnSc3d1	široké
spektru	spektrum	k1gNnSc3	spektrum
projevů	projev	k1gInPc2	projev
nemoci	nemoc	k1gFnSc2	nemoc
a	a	k8xC	a
častému	častý	k2eAgInSc3d1	častý
rozvoji	rozvoj	k1gInSc3	rozvoj
komplikací	komplikace	k1gFnSc7	komplikace
poměrně	poměrně	k6eAd1	poměrně
složitá	složitý	k2eAgFnSc1d1	složitá
a	a	k8xC	a
názory	názor	k1gInPc4	názor
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nP	různit
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
terapie	terapie	k1gFnSc2	terapie
představuje	představovat	k5eAaImIp3nS	představovat
podávání	podávání	k1gNnSc2	podávání
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
podporované	podporovaný	k2eAgFnSc2d1	podporovaná
dle	dle	k7c2	dle
potřeby	potřeba	k1gFnSc2	potřeba
symptomatologickou	symptomatologický	k2eAgFnSc7d1	symptomatologická
léčbou	léčba	k1gFnSc7	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
k	k	k7c3	k
terapii	terapie	k1gFnSc3	terapie
brucelózy	brucelóza	k1gFnSc2	brucelóza
dospělých	dospělí	k1gMnPc2	dospělí
antibiotikum	antibiotikum	k1gNnSc1	antibiotikum
rifampicin	rifampicin	k2eAgMnSc1d1	rifampicin
(	(	kIx(	(
<g/>
600	[number]	k4	600
až	až	k9	až
900	[number]	k4	900
mg	mg	kA	mg
<g/>
)	)	kIx)	)
a	a	k8xC	a
doxycyklin	doxycyklin	k1gInSc1	doxycyklin
(	(	kIx(	(
<g/>
100	[number]	k4	100
mg	mg	kA	mg
<g/>
)	)	kIx)	)
dvakrát	dvakrát	k6eAd1	dvakrát
denně	denně	k6eAd1	denně
minimálně	minimálně	k6eAd1	minimálně
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
šesti	šest	k4xCc2	šest
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
prevenci	prevence	k1gFnSc4	prevence
znovuvzplanutí	znovuvzplanutí	k1gNnSc2	znovuvzplanutí
nemoci	nemoc	k1gFnSc2	nemoc
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
100	[number]	k4	100
mg	mg	kA	mg
doxycyklinu	doxycyklin	k1gInSc2	doxycyklin
perorálně	perorálně	k6eAd1	perorálně
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
6	[number]	k4	6
týdnů	týden	k1gInPc2	týden
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
intramuskulární	intramuskulární	k2eAgFnSc7d1	intramuskulární
aplikací	aplikace	k1gFnSc7	aplikace
streptomycinu	streptomycin	k1gInSc2	streptomycin
(	(	kIx(	(
<g/>
1	[number]	k4	1
gram	gram	k1gInSc1	gram
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
a	a	k8xC	a
den	den	k1gInSc4	den
<g/>
)	)	kIx)	)
denně	denně	k6eAd1	denně
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
streptomycinu	streptomycin	k1gInSc2	streptomycin
lze	lze	k6eAd1	lze
rovněž	rovněž	k9	rovněž
použít	použít	k5eAaPmF	použít
gentamicin	gentamicin	k2eAgMnSc1d1	gentamicin
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
alternativní	alternativní	k2eAgFnSc4d1	alternativní
antibiotickou	antibiotický	k2eAgFnSc4d1	antibiotická
terapii	terapie	k1gFnSc4	terapie
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
chinolony	chinolon	k1gInPc4	chinolon
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kombinaci	kombinace	k1gFnSc4	kombinace
ciprofloxacinu	ciprofloxacin	k2eAgFnSc4d1	ciprofloxacin
s	s	k7c7	s
ofloxacinem	ofloxacino	k1gNnSc7	ofloxacino
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
8	[number]	k4	8
let	léto	k1gNnPc2	léto
jsou	být	k5eAaImIp3nP	být
lékem	lék	k1gInSc7	lék
první	první	k4xOgFnSc2	první
volby	volba	k1gFnSc2	volba
rifampicin	rifampicin	k2eAgInSc4d1	rifampicin
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
trimetoprim-sulfametoxazolem	trimetoprimulfametoxazol	k1gInSc7	trimetoprim-sulfametoxazol
podávané	podávaný	k2eAgInPc4d1	podávaný
perorálně	perorálně	k6eAd1	perorálně
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
6	[number]	k4	6
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
vzniku	vznik	k1gInSc2	vznik
cévních	cévní	k2eAgFnPc2d1	cévní
komplikací	komplikace	k1gFnPc2	komplikace
(	(	kIx(	(
<g/>
endokarditidy	endokarditida	k1gFnPc1	endokarditida
<g/>
,	,	kIx,	,
chlopňové	chlopňový	k2eAgFnPc1d1	chlopňová
vady	vada	k1gFnPc1	vada
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
provést	provést	k5eAaPmF	provést
chirurgické	chirurgický	k2eAgNnSc4d1	chirurgické
ošetření	ošetření	k1gNnSc4	ošetření
na	na	k7c6	na
specializovaném	specializovaný	k2eAgNnSc6d1	specializované
kardiovaskulárním	kardiovaskulární	k2eAgNnSc6d1	kardiovaskulární
pracovišti	pracoviště	k1gNnSc6	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Terapie	terapie	k1gFnSc1	terapie
všech	všecek	k3xTgNnPc2	všecek
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgNnPc2d1	žijící
zvířat	zvíře	k1gNnPc2	zvíře
se	se	k3xPyFc4	se
neprovádí	provádět	k5eNaImIp3nS	provádět
<g/>
,	,	kIx,	,
ba	ba	k9	ba
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
států	stát	k1gInPc2	stát
zakázána	zakázán	k2eAgFnSc1d1	zakázána
<g/>
.	.	kIx.	.
</s>
<s>
Nemocná	mocný	k2eNgNnPc1d1	nemocné
zvířata	zvíře	k1gNnPc1	zvíře
jsou	být	k5eAaImIp3nP	být
utracena	utracen	k2eAgNnPc1d1	utraceno
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
psů	pes	k1gMnPc2	pes
je	být	k5eAaImIp3nS	být
terapie	terapie	k1gFnSc1	terapie
povolena	povolit	k5eAaPmNgFnS	povolit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
neúspěšná	úspěšný	k2eNgFnSc1d1	neúspěšná
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
klinických	klinický	k2eAgInPc2d1	klinický
příznaků	příznak	k1gInPc2	příznak
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
doporučována	doporučován	k2eAgFnSc1d1	doporučována
kastrace	kastrace	k1gFnSc1	kastrace
nemocných	mocný	k2eNgFnPc2d1	mocný
fen	fena	k1gFnPc2	fena
i	i	k9	i
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
sérologicky	sérologicky	k6eAd1	sérologicky
pozitivní	pozitivní	k2eAgMnPc1d1	pozitivní
psi	pes	k1gMnPc1	pes
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
vyřazeni	vyřadit	k5eAaPmNgMnP	vyřadit
z	z	k7c2	z
chovu	chov	k1gInSc2	chov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
terapii	terapie	k1gFnSc6	terapie
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
kombinace	kombinace	k1gFnPc1	kombinace
vysokých	vysoký	k2eAgFnPc2d1	vysoká
dávek	dávka	k1gFnPc2	dávka
aminoglykosidů	aminoglykosid	k1gInPc2	aminoglykosid
a	a	k8xC	a
tetracyklinů	tetracyklin	k1gInPc2	tetracyklin
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
autoři	autor	k1gMnPc1	autor
uvádějí	uvádět	k5eAaImIp3nP	uvádět
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
léčbu	léčba	k1gFnSc4	léčba
brucelózy	brucelóza	k1gFnSc2	brucelóza
psů	pes	k1gMnPc2	pes
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
antibiotika	antibiotikum	k1gNnSc2	antibiotikum
enrofloxacinu	enrofloxacin	k1gInSc6	enrofloxacin
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
doporučují	doporučovat	k5eAaImIp3nP	doporučovat
eutanázii	eutanázie	k1gFnSc4	eutanázie
nemocných	nemocný	k1gMnPc2	nemocný
psů	pes	k1gMnPc2	pes
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
možnosti	možnost	k1gFnSc3	možnost
přenosu	přenos	k1gInSc2	přenos
nákazy	nákaza	k1gFnSc2	nákaza
na	na	k7c4	na
další	další	k2eAgMnPc4d1	další
psy	pes	k1gMnPc4	pes
nebo	nebo	k8xC	nebo
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
canis	canis	k1gFnSc1	canis
byla	být	k5eAaImAgFnS	být
totiž	totiž	k9	totiž
prokázána	prokázat	k5eAaPmNgFnS	prokázat
v	v	k7c6	v
tkáních	tkáň	k1gFnPc6	tkáň
psů	pes	k1gMnPc2	pes
týdny	týden	k1gInPc7	týden
<g/>
,	,	kIx,	,
ba	ba	k9	ba
dokonce	dokonce	k9	dokonce
i	i	k9	i
měsíce	měsíc	k1gInPc1	měsíc
po	po	k7c6	po
léčbě	léčba	k1gFnSc6	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Prevence	prevence	k1gFnSc1	prevence
brucelózy	brucelóza	k1gFnSc2	brucelóza
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
klíčově	klíčově	k6eAd1	klíčově
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
eradikačních	eradikační	k2eAgInPc6d1	eradikační
programech	program	k1gInPc6	program
a	a	k8xC	a
tlumení	tlumení	k1gNnSc6	tlumení
nemoci	nemoc	k1gFnSc2	nemoc
u	u	k7c2	u
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
na	na	k7c4	na
dodržování	dodržování	k1gNnSc4	dodržování
veterinárně-hygienických	veterinárněygienický	k2eAgFnPc2d1	veterinárně-hygienický
zásad	zásada	k1gFnPc2	zásada
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
komerční	komerční	k2eAgFnSc1d1	komerční
bezpečná	bezpečný	k2eAgFnSc1d1	bezpečná
vakcína	vakcína	k1gFnSc1	vakcína
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
ochranou	ochrana	k1gFnSc7	ochrana
před	před	k7c7	před
infekcí	infekce	k1gFnSc7	infekce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
oslabené	oslabený	k2eAgFnPc1d1	oslabená
živé	živá	k1gFnPc1	živá
vakcíny	vakcína	k1gFnSc2	vakcína
proti	proti	k7c3	proti
B.	B.	kA	B.
abortus	abortus	k1gInSc1	abortus
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
Svazu	svaz	k1gInSc6	svaz
a	a	k8xC	a
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
účinnost	účinnost	k1gFnSc1	účinnost
byla	být	k5eAaImAgFnS	být
však	však	k9	však
omezená	omezený	k2eAgFnSc1d1	omezená
a	a	k8xC	a
existovala	existovat	k5eAaImAgFnS	existovat
možnost	možnost	k1gFnSc4	možnost
propuknutí	propuknutí	k1gNnSc2	propuknutí
onemocnění	onemocnění	k1gNnSc2	onemocnění
u	u	k7c2	u
vakcinovaného	vakcinovaný	k2eAgMnSc2d1	vakcinovaný
člověka	člověk	k1gMnSc2	člověk
z	z	k7c2	z
vakcinovaného	vakcinovaný	k2eAgInSc2d1	vakcinovaný
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
s	s	k7c7	s
očkováním	očkování	k1gNnSc7	očkování
lepší	dobrý	k2eAgFnSc2d2	lepší
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
států	stát	k1gInPc2	stát
je	být	k5eAaImIp3nS	být
vakcinace	vakcinace	k1gFnSc1	vakcinace
proti	proti	k7c3	proti
brucelóze	brucelóza	k1gFnSc3	brucelóza
zakázána	zakázán	k2eAgFnSc1d1	zakázána
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
komplikace	komplikace	k1gFnSc2	komplikace
sérologické	sérologický	k2eAgFnSc2d1	sérologická
diagnostiky	diagnostika	k1gFnSc2	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
skotu	skot	k1gInSc2	skot
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
používají	používat	k5eAaImIp3nP	používat
živé	živá	k1gFnPc1	živá
oslabené	oslabený	k2eAgFnSc2d1	oslabená
vakcíny	vakcína	k1gFnSc2	vakcína
B.	B.	kA	B.
abortus	abortus	k1gInSc1	abortus
kmen	kmen	k1gInSc1	kmen
19	[number]	k4	19
nebo	nebo	k8xC	nebo
kmen	kmen	k1gInSc1	kmen
RB	RB	kA	RB
<g/>
51	[number]	k4	51
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
B.	B.	kA	B.
melitensis	melitensis	k1gFnPc1	melitensis
existují	existovat	k5eAaImIp3nP	existovat
vakcíny	vakcína	k1gFnPc4	vakcína
Rev	Rev	k1gFnSc2	Rev
1	[number]	k4	1
nebo	nebo	k8xC	nebo
mutantní	mutantní	k2eAgMnSc1d1	mutantní
pur	pur	k?	pur
E.	E.	kA	E.
Preventivní	preventivní	k2eAgNnSc1d1	preventivní
opatření	opatření	k1gNnSc1	opatření
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
tkví	tkvět	k5eAaImIp3nS	tkvět
především	především	k9	především
v	v	k7c4	v
zamezení	zamezení	k1gNnSc4	zamezení
zavlečení	zavlečení	k1gNnSc2	zavlečení
původce	původce	k1gMnSc2	původce
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
důsledném	důsledný	k2eAgNnSc6d1	důsledné
dodržování	dodržování	k1gNnSc6	dodržování
veterinárně-hygienických	veterinárněygienický	k2eAgInPc2d1	veterinárně-hygienický
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
sérologickém	sérologický	k2eAgInSc6d1	sérologický
monitoringu	monitoring	k1gInSc6	monitoring
u	u	k7c2	u
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
koz	koza	k1gFnPc2	koza
a	a	k8xC	a
zajíců	zajíc	k1gMnPc2	zajíc
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k8xC	i
preventivním	preventivní	k2eAgNnSc6d1	preventivní
sérologickém	sérologický	k2eAgNnSc6d1	sérologické
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
vzorků	vzorek	k1gInPc2	vzorek
mléka	mléko	k1gNnSc2	mléko
dodávaných	dodávaný	k2eAgInPc2d1	dodávaný
do	do	k7c2	do
mlékáren	mlékárna	k1gFnPc2	mlékárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mlékárenských	mlékárenský	k2eAgInPc2d1	mlékárenský
podniků	podnik	k1gInPc2	podnik
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
kontrola	kontrola	k1gFnSc1	kontrola
pasterizace	pasterizace	k1gFnSc2	pasterizace
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
prevenci	prevence	k1gFnSc4	prevence
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
nekonzumovat	konzumovat	k5eNaBmF	konzumovat
nepasterizované	pasterizovaný	k2eNgNnSc4d1	nepasterizované
mléko	mléko	k1gNnSc4	mléko
ani	ani	k8xC	ani
výrobky	výrobek	k1gInPc4	výrobek
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
s	s	k7c7	s
endemickým	endemický	k2eAgInSc7d1	endemický
výskytem	výskyt	k1gInSc7	výskyt
brucelózy	brucelóza	k1gFnSc2	brucelóza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
turisticky	turisticky	k6eAd1	turisticky
preferovaných	preferovaný	k2eAgFnPc2d1	preferovaná
zemí	zem	k1gFnPc2	zem
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Malta	Malta	k1gFnSc1	Malta
či	či	k8xC	či
Tunisko	Tunisko	k1gNnSc1	Tunisko
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
rizikové	rizikový	k2eAgNnSc1d1	rizikové
konzumovat	konzumovat	k5eAaBmF	konzumovat
nepasterizované	pasterizovaný	k2eNgNnSc4d1	nepasterizované
či	či	k8xC	či
nepřevařené	převařený	k2eNgNnSc4d1	nepřevařené
ovčí	ovčí	k2eAgNnSc4d1	ovčí
<g/>
,	,	kIx,	,
kozí	kozí	k2eAgNnSc4d1	kozí
<g/>
,	,	kIx,	,
velbloudí	velbloudí	k2eAgNnSc4d1	velbloudí
mléko	mléko	k1gNnSc4	mléko
a	a	k8xC	a
mléčné	mléčný	k2eAgInPc4d1	mléčný
výrobky	výrobek	k1gInPc4	výrobek
z	z	k7c2	z
tamních	tamní	k2eAgInPc2d1	tamní
venkovských	venkovský	k2eAgInPc2d1	venkovský
domácích	domácí	k2eAgInPc2d1	domácí
chovů	chov	k1gInPc2	chov
a	a	k8xC	a
farem	farma	k1gFnPc2	farma
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
snadnému	snadný	k2eAgInSc3d1	snadný
přenosu	přenos	k1gInSc3	přenos
brucel	brucet	k5eAaImAgInS	brucet
vzduchem	vzduch	k1gInSc7	vzduch
pomocí	pomocí	k7c2	pomocí
aerosolu	aerosol	k1gInSc2	aerosol
patřila	patřit	k5eAaImAgFnS	patřit
brucelóza	brucelóza	k1gFnSc1	brucelóza
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
samotní	samotný	k2eAgMnPc1d1	samotný
původci	původce	k1gMnPc1	původce
<g/>
,	,	kIx,	,
a	a	k8xC	a
hypoteticky	hypoteticky	k6eAd1	hypoteticky
stále	stále	k6eAd1	stále
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
kandidáty	kandidát	k1gMnPc7	kandidát
na	na	k7c4	na
biologickou	biologický	k2eAgFnSc4d1	biologická
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
však	však	k9	však
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
začal	začít	k5eAaPmAgInS	začít
vývoj	vývoj	k1gInSc4	vývoj
biologické	biologický	k2eAgFnSc2d1	biologická
zbraně	zbraň	k1gFnSc2	zbraň
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
kmenů	kmen	k1gInPc2	kmen
B.	B.	kA	B.
suis	suis	k6eAd1	suis
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
1944	[number]	k4	1944
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
biologická	biologický	k2eAgFnSc1d1	biologická
zbraň	zbraň	k1gFnSc1	zbraň
poprvé	poprvé	k6eAd1	poprvé
testována	testovat	k5eAaImNgFnS	testovat
na	na	k7c6	na
zvířatech	zvíře	k1gNnPc6	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byly	být	k5eAaImAgFnP	být
americkou	americký	k2eAgFnSc7d1	americká
armádou	armáda	k1gFnSc7	armáda
vyráběny	vyráběn	k2eAgFnPc1d1	vyráběna
bomby	bomba	k1gFnPc1	bomba
obsahující	obsahující	k2eAgFnSc2d1	obsahující
B.	B.	kA	B.
suis	suis	k6eAd1	suis
pro	pro	k7c4	pro
americké	americký	k2eAgNnSc4d1	americké
letectvo	letectvo	k1gNnSc4	letectvo
v	v	k7c6	v
Arkansasu	Arkansas	k1gInSc6	Arkansas
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
zastavila	zastavit	k5eAaPmAgFnS	zastavit
americká	americký	k2eAgFnSc1d1	americká
vláda	vláda	k1gFnSc1	vláda
vývoj	vývoj	k1gInSc1	vývoj
a	a	k8xC	a
používání	používání	k1gNnSc1	používání
těchto	tento	k3xDgFnPc2	tento
biologických	biologický	k2eAgFnPc2d1	biologická
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
bomby	bomba	k1gFnPc4	bomba
s	s	k7c7	s
brucelami	brucela	k1gFnPc7	brucela
nebyly	být	k5eNaImAgFnP	být
nikdy	nikdy	k6eAd1	nikdy
použity	použít	k5eAaPmNgFnP	použít
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
patogenní	patogenní	k2eAgInPc4d1	patogenní
kmeny	kmen	k1gInPc4	kmen
Brucella	Brucella	k1gMnSc1	Brucella
spp	spp	k?	spp
<g/>
.	.	kIx.	.
stále	stále	k6eAd1	stále
mezi	mezi	k7c7	mezi
kandidáty	kandidát	k1gMnPc7	kandidát
na	na	k7c4	na
biologickou	biologický	k2eAgFnSc4d1	biologická
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
.	.	kIx.	.
</s>
