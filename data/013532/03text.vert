<s>
Brainwashed	Brainwashed	k1gMnSc1
</s>
<s>
BrainwashedInterpretGeorge	BrainwashedInterpretGeorge	k1gFnSc1
HarrisonPruh	HarrisonPruha	k1gFnPc2
albastudiové	albastudiový	k2eAgNnSc4d1
albumVydáno	albumVydán	k2eAgNnSc4d1
<g/>
18	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2002	#num#	k4
<g/>
Nahráno	nahrát	k5eAaPmNgNnS,k5eAaBmNgNnS
<g/>
1988	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
ŽánrrockDélka	ŽánrrockDélka	k1gFnSc1
<g/>
47	#num#	k4
<g/>
:	:	kIx,
<g/>
41	#num#	k4
<g/>
JazykangličtinaVydavatelstvíDark	JazykangličtinaVydavatelstvíDark	k1gInSc1
Horse	Horse	k1gFnSc2
RecordsProducentiGeorge	RecordsProducentiGeorge	k1gFnPc2
Harrison	Harrison	k1gMnSc1
<g/>
,	,	kIx,
Jeff	Jeff	k1gMnSc1
Lynne	Lynn	k1gInSc5
<g/>
,	,	kIx,
Dhani	Dhaeň	k1gFnSc6
HarrisonProfesionální	HarrisonProfesionální	k2eAgFnSc1d1
kritika	kritika	k1gFnSc1
</s>
<s>
Allmusic	Allmusic	k1gMnSc1
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
George	George	k6eAd1
Harrison	Harrison	k1gMnSc1
chronologicky	chronologicky	k6eAd1
</s>
<s>
Live	Live	k1gFnSc1
in	in	k?
Japan	japan	k1gInSc1
<g/>
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Brainwashed	Brainwashed	k1gInSc1
<g/>
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Dark	Dark	k1gMnSc1
Horse	Horse	k1gFnSc2
Years	Years	k1gInSc1
1976	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Brainwashed	Brainwashed	k1gInSc1
je	být	k5eAaImIp3nS
dvanácté	dvanáctý	k4xOgNnSc1
a	a	k8xC
poslední	poslední	k2eAgFnPc1d1
studiové	studiový	k2eAgFnPc1d1
album	album	k1gNnSc4
anglického	anglický	k2eAgMnSc2d1
hudebníka	hudebník	k1gMnSc2
George	George	k1gMnSc2
Harrisona	Harrison	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšlo	vyjít	k5eAaPmAgNnS
téměř	téměř	k6eAd1
rok	rok	k1gInSc4
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
listopadu	listopad	k1gInSc6
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
produkci	produkce	k1gFnSc6
se	se	k3xPyFc4
kromě	kromě	k7c2
Harrisona	Harrison	k1gMnSc2
podíleli	podílet	k5eAaImAgMnP
také	také	k9
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
syn	syn	k1gMnSc1
Dhani	Dhaen	k2eAgMnPc1d1
a	a	k8xC
Jeff	Jeff	k1gMnSc1
Lynne	Lynn	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deska	deska	k1gFnSc1
se	se	k3xPyFc4
umístila	umístit	k5eAaPmAgFnS
na	na	k7c6
osmnácté	osmnáctý	k4xOgFnSc6
příčce	příčka	k1gFnSc6
americké	americký	k2eAgFnSc2d1
hitparády	hitparáda	k1gFnSc2
Billboard	billboard	k1gInSc1
200	#num#	k4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
britské	britský	k2eAgFnSc6d1
UK	UK	kA
Albums	Albumsa	k1gFnPc2
Chart	charta	k1gFnPc2
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
na	na	k7c4
devětadvacátou	devětadvacátý	k4xOgFnSc4
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
skladeb	skladba	k1gFnPc2
</s>
<s>
„	„	k?
<g/>
Any	Any	k1gMnSc1
Road	Road	k1gMnSc1
<g/>
“	“	k?
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
52	#num#	k4
</s>
<s>
„	„	k?
<g/>
P	P	kA
<g/>
2	#num#	k4
Vatican	Vatican	k1gInSc4
Blues	blues	k1gFnSc2
(	(	kIx(
<g/>
Last	Last	k1gMnSc1
Saturday	Saturdaa	k1gFnSc2
Night	Night	k1gMnSc1
<g/>
)	)	kIx)
<g/>
“	“	k?
–	–	k?
2	#num#	k4
<g/>
:	:	kIx,
<g/>
38	#num#	k4
</s>
<s>
„	„	k?
<g/>
Pisces	Pisces	k1gMnSc1
Fish	Fish	k1gMnSc1
<g/>
“	“	k?
–	–	k?
4	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
</s>
<s>
„	„	k?
<g/>
Looking	Looking	k1gInSc1
for	forum	k1gNnPc2
My	my	k3xPp1nPc1
Life	Life	k1gNnSc2
<g/>
“	“	k?
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
49	#num#	k4
</s>
<s>
„	„	k?
<g/>
Rising	Rising	k1gInSc1
Sun	Sun	kA
<g/>
“	“	k?
–	–	k?
5	#num#	k4
<g/>
:	:	kIx,
<g/>
27	#num#	k4
</s>
<s>
„	„	k?
<g/>
Marwa	Marwa	k1gMnSc1
Blues	blues	k1gNnSc1
<g/>
“	“	k?
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
</s>
<s>
„	„	k?
<g/>
Stuck	Stuck	k1gInSc1
Inside	Insid	k1gInSc5
a	a	k8xC
Cloud	Cloudo	k1gNnPc2
<g/>
“	“	k?
–	–	k?
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
„	„	k?
<g/>
Run	run	k1gInSc1
So	So	kA
Far	fara	k1gFnPc2
<g/>
“	“	k?
–	–	k?
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
„	„	k?
<g/>
Never	Never	k1gMnSc1
Get	Get	k1gMnSc1
Over	Over	k1gMnSc1
You	You	k1gMnSc1
<g/>
“	“	k?
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
26	#num#	k4
</s>
<s>
„	„	k?
<g/>
Between	Between	k1gInSc1
the	the	k?
Devil	Devil	k1gInSc1
and	and	k?
the	the	k?
Deep	Deep	k1gInSc1
Blue	Blu	k1gMnSc2
Sea	Sea	k1gMnSc2
<g/>
“	“	k?
–	–	k?
2	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
</s>
<s>
„	„	k?
<g/>
Rocking	Rocking	k1gInSc1
Chair	Chair	k1gMnSc1
in	in	k?
Hawaii	Hawaie	k1gFnSc4
<g/>
“	“	k?
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
„	„	k?
<g/>
Brainwashed	Brainwashed	k1gInSc1
<g/>
“	“	k?
–	–	k?
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Obsazení	obsazení	k1gNnSc1
</s>
<s>
George	George	k6eAd1
Harrison	Harrison	k1gMnSc1
–	–	k?
zpěv	zpěv	k1gInSc1
<g/>
,	,	kIx,
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
dobro	dobro	k1gNnSc1
<g/>
,	,	kIx,
ukulele	ukulele	k1gNnSc2
<g/>
,	,	kIx,
klávesy	klávesa	k1gFnSc2
<g/>
,	,	kIx,
baskytara	baskytara	k1gFnSc1
<g/>
,	,	kIx,
perkuse	perkuse	k1gFnPc1
</s>
<s>
Jeff	Jeff	k1gMnSc1
Lynne	Lynn	k1gInSc5
–	–	k?
baskytara	baskytara	k1gFnSc1
<g/>
,	,	kIx,
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
klavír	klavír	k1gInSc1
<g/>
,	,	kIx,
klávesy	kláves	k1gInPc1
<g/>
,	,	kIx,
perkuse	perkuse	k1gFnPc1
<g/>
,	,	kIx,
doprovodné	doprovodný	k2eAgInPc1d1
vokály	vokál	k1gInPc1
</s>
<s>
Dhani	Dhaeň	k1gFnSc3
Harrison	Harrison	k1gMnSc1
–	–	k?
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
elektrické	elektrický	k2eAgNnSc1d1
piano	piano	k1gNnSc1
<g/>
,	,	kIx,
doprovodné	doprovodný	k2eAgInPc1d1
vokály	vokál	k1gInPc1
</s>
<s>
Jim	on	k3xPp3gFnPc3
Keltner	Keltner	k1gInSc1
–	–	k?
bicí	bicí	k2eAgInSc1d1
</s>
<s>
Mike	Mike	k1gFnSc1
Moran	Morana	k1gFnPc2
–	–	k?
klávesy	klávesa	k1gFnSc2
</s>
<s>
Marc	Marc	k6eAd1
Mann	Mann	k1gMnSc1
–	–	k?
klávesy	klávesa	k1gFnSc2
</s>
<s>
Ray	Ray	k?
Cooper	Cooper	k1gInSc1
–	–	k?
perkuse	perkuse	k1gFnPc1
<g/>
,	,	kIx,
bicí	bicí	k2eAgFnPc1d1
</s>
<s>
Jools	Jools	k6eAd1
Holland	Holland	k1gInSc1
–	–	k?
klavír	klavír	k1gInSc4
</s>
<s>
Mark	Mark	k1gMnSc1
Flannagan	Flannagan	k1gMnSc1
–	–	k?
kytara	kytara	k1gFnSc1
</s>
<s>
Joe	Joe	k?
Brown	Brown	k1gMnSc1
–	–	k?
kytara	kytara	k1gFnSc1
</s>
<s>
Herbie	Herbie	k1gFnSc1
Flowers	Flowers	k1gInSc1
–	–	k?
baskytara	baskytara	k1gFnSc1
<g/>
,	,	kIx,
tuba	tuba	k1gFnSc1
</s>
<s>
Bickram	Bickram	k1gInSc1
Ghosh	Ghosh	k1gInSc1
–	–	k?
tabla	tablo	k1gNnSc2
</s>
<s>
Jon	Jon	k?
Lord	lord	k1gMnSc1
–	–	k?
klavír	klavír	k1gInSc1
</s>
<s>
Sam	Sam	k1gMnSc1
Brown	Brown	k1gMnSc1
–	–	k?
doprovodné	doprovodný	k2eAgInPc1d1
vokály	vokál	k1gInPc1
</s>
<s>
Jane	Jan	k1gMnSc5
Lister	Lister	k1gMnSc1
–	–	k?
harfa	harfa	k1gFnSc1
</s>
<s>
Isabela	Isabela	k1gFnSc1
Borzymowska	Borzymowsk	k1gInSc2
–	–	k?
hlas	hlas	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ERLEWINE	ERLEWINE	kA
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgMnSc1d1
Thomas	Thomas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
George	Georg	k1gInSc2
Harrison	Harrison	k1gMnSc1
<g/>
:	:	kIx,
Brainwashed	Brainwashed	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Allmusic	Allmusic	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
George	Georg	k1gMnSc2
Harrison	Harrison	k1gMnSc1
Studiová	studiový	k2eAgFnSc1d1
alba	album	k1gNnPc4
</s>
<s>
Wonderwall	Wonderwall	k1gMnSc1
Music	Musice	k1gFnPc2
•	•	k?
Electronic	Electronice	k1gFnPc2
Sound	Sound	k1gMnSc1
•	•	k?
All	All	k1gFnSc1
Things	Thingsa	k1gFnPc2
Must	Must	k2eAgInSc1d1
Pass	Pass	k1gInSc1
•	•	k?
Living	Living	k1gInSc1
in	in	k?
the	the	k?
Material	Material	k1gMnSc1
World	World	k1gMnSc1
•	•	k?
Dark	Dark	k1gMnSc1
Horse	Horse	k1gFnSc2
•	•	k?
Extra	extra	k6eAd1
Texture	Textur	k1gMnSc5
(	(	kIx(
<g/>
Read	Read	k1gMnSc1
All	All	k1gMnSc1
About	About	k1gMnSc1
It	It	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Thirty	Thirta	k1gFnPc4
Three	Thre	k1gInSc2
&	&	k?
1	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
•	•	k?
George	George	k1gNnSc1
Harrison	Harrison	k1gMnSc1
•	•	k?
Somewhere	Somewher	k1gInSc5
in	in	k?
England	Englando	k1gNnPc2
•	•	k?
Gone	Gon	k1gFnSc2
Troppo	Troppa	k1gFnSc5
•	•	k?
Cloud	Cloudo	k1gNnPc2
Nine	Nine	k1gNnPc2
•	•	k?
Brainwashed	Brainwashed	k1gMnSc1
Koncertní	koncertní	k2eAgMnSc1d1
alba	album	k1gNnSc2
</s>
<s>
The	The	k?
Concert	Concert	k1gInSc1
for	forum	k1gNnPc2
Bangladesh	Bangladesh	k1gInSc1
•	•	k?
Live	Live	k1gInSc1
in	in	k?
Japan	japan	k1gInSc4
Kompilační	kompilační	k2eAgNnPc4d1
alba	album	k1gNnPc4
</s>
<s>
The	The	k?
Best	Best	k1gMnSc1
of	of	k?
George	George	k1gInSc1
Harrison	Harrison	k1gMnSc1
•	•	k?
Best	Best	k1gMnSc1
of	of	k?
Dark	Dark	k1gInSc1
Horse	Horse	k1gFnSc1
1976	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
•	•	k?
Let	let	k1gInSc1
It	It	k1gMnSc1
Roll	Roll	k1gMnSc1
<g/>
:	:	kIx,
Songs	Songs	k1gInSc1
by	by	kYmCp3nS
George	George	k1gInSc4
Harrison	Harrison	k1gMnSc1
•	•	k?
Early	earl	k1gMnPc4
Takes	Takesa	k1gFnPc2
<g/>
:	:	kIx,
Volume	volum	k1gInSc5
1	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
