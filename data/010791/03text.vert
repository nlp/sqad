<p>
<s>
Gaweinstal	Gaweinstat	k5eAaImAgMnS	Gaweinstat
je	být	k5eAaImIp3nS	být
městys	městys	k1gInSc4	městys
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Mistelbach	Mistelbacha	k1gFnPc2	Mistelbacha
v	v	k7c6	v
Dolních	dolní	k2eAgInPc6d1	dolní
Rakousích	Rakousy	k1gInPc6	Rakousy
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
900	[number]	k4	900
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Gaweinstal	Gaweinstat	k5eAaPmAgMnS	Gaweinstat
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
pahorkovité	pahorkovitý	k2eAgFnSc6d1	pahorkovitá
krajině	krajina	k1gFnSc6	krajina
Weinviertelu	Weinviertel	k1gInSc2	Weinviertel
v	v	k7c6	v
Dolních	dolní	k2eAgInPc6d1	dolní
Rakousích	Rakousy	k1gInPc6	Rakousy
u	u	k7c2	u
brněnské	brněnský	k2eAgFnSc2d1	brněnská
silnice	silnice	k1gFnSc2	silnice
B7	B7	k1gFnSc2	B7
asi	asi	k9	asi
25	[number]	k4	25
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Vidně	vidně	k6eAd1	vidně
<g/>
,	,	kIx,	,
Plocha	plocha	k1gFnSc1	plocha
území	území	k1gNnSc2	území
obce	obec	k1gFnSc2	obec
činí	činit	k5eAaImIp3nS	činit
51,6	[number]	k4	51,6
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
9,19	[number]	k4	9,19
%	%	kIx~	%
plochy	plocha	k1gFnPc4	plocha
je	být	k5eAaImIp3nS	být
zalesněno	zalesněn	k2eAgNnSc1d1	zalesněno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
<g/>
:	:	kIx,	:
Atzelsdorf	Atzelsdorf	k1gMnSc1	Atzelsdorf
<g/>
,	,	kIx,	,
Gaweinstal	Gaweinstal	k1gMnSc1	Gaweinstal
<g/>
,	,	kIx,	,
Höbersbrunn	Höbersbrunn	k1gMnSc1	Höbersbrunn
<g/>
,	,	kIx,	,
Martinsdorf	Martinsdorf	k1gMnSc1	Martinsdorf
<g/>
,	,	kIx,	,
Pellendorf	Pellendorf	k1gMnSc1	Pellendorf
a	a	k8xC	a
Schrick	Schrick	k1gMnSc1	Schrick
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Původ	původ	k1gInSc1	původ
jména	jméno	k1gNnSc2	jméno
===	===	k?	===
</s>
</p>
<p>
<s>
Gaweinstal	Gaweinstat	k5eAaImAgMnS	Gaweinstat
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
míst	místo	k1gNnPc2	místo
ve	v	k7c6	v
Weinviertelu	Weinviertel	k1gInSc6	Weinviertel
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1050	[number]	k4	1050
<g/>
,	,	kIx,	,
prokazatelně	prokazatelně	k6eAd1	prokazatelně
však	však	k9	však
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
dokumentech	dokument	k1gInPc6	dokument
uvedený	uvedený	k2eAgInSc4d1	uvedený
jako	jako	k9	jako
Gunesdorf	Gunesdorf	k1gInSc4	Gunesdorf
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1236	[number]	k4	1236
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
osobního	osobní	k2eAgNnSc2d1	osobní
jména	jméno	k1gNnSc2	jméno
"	"	kIx"	"
<g/>
Guni	Guni	k1gNnSc2	Guni
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
skrývá	skrývat	k5eAaImIp3nS	skrývat
jiné	jiný	k2eAgNnSc4d1	jiné
místní	místní	k2eAgNnSc4d1	místní
jméno	jméno	k1gNnSc4	jméno
"	"	kIx"	"
<g/>
bajuwarského	bajuwarský	k2eAgInSc2d1	bajuwarský
<g/>
"	"	kIx"	"
původu	původ	k1gInSc2	původ
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
užívané	užívaný	k2eAgNnSc1d1	užívané
v	v	k7c6	v
bavorském	bavorský	k2eAgNnSc6d1	bavorské
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Guni	Guni	k1gNnSc1	Guni
<g/>
"	"	kIx"	"
mohl	moct	k5eAaImAgInS	moct
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1050	[number]	k4	1050
založit	založit	k5eAaPmF	založit
místo	místo	k6eAd1	místo
jako	jako	k9	jako
leník	leník	k1gMnSc1	leník
babenberského	babenberský	k2eAgMnSc2d1	babenberský
markraběte	markrabě	k1gMnSc2	markrabě
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgInS	zůstat
správcem	správce	k1gMnSc7	správce
nebo	nebo	k8xC	nebo
pověřencem	pověřenec	k1gMnSc7	pověřenec
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
ohrožení	ohrožení	k1gNnSc2	ohrožení
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
jména	jméno	k1gNnPc1	jméno
Gaunestorf	Gaunestorf	k1gMnSc1	Gaunestorf
a	a	k8xC	a
Gawnestorf	Gawnestorf	k1gMnSc1	Gawnestorf
<g/>
,	,	kIx,	,
během	během	k7c2	během
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
vyvinuje	vyvinovat	k5eAaImIp3nS	vyvinovat
až	až	k9	až
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nelichotivý	lichotivý	k2eNgInSc4d1	nelichotivý
název	název	k1gInSc4	název
Gaunersdorf	Gaunersdorf	k1gInSc4	Gaunersdorf
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
šejdířská	šejdířský	k2eAgFnSc1d1	šejdířský
ves	ves	k1gFnSc1	ves
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
po	po	k7c4	po
čtyři	čtyři	k4xCgNnPc4	čtyři
staletí	staletí	k1gNnPc4	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
obyvatele	obyvatel	k1gMnPc4	obyvatel
posměšný	posměšný	k2eAgInSc1d1	posměšný
název	název	k1gInSc1	název
stal	stát	k5eAaPmAgInS	stát
neúnosný	únosný	k2eNgInSc1d1	neúnosný
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1914	[number]	k4	1914
obec	obec	k1gFnSc1	obec
požádala	požádat	k5eAaPmAgFnS	požádat
dolnorakouské	dolnorakouský	k2eAgNnSc4d1	dolnorakouské
místodržitelství	místodržitelství	k1gNnSc4	místodržitelství
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
místo	místo	k1gNnSc1	místo
přejmenovalo	přejmenovat	k5eAaPmAgNnS	přejmenovat
na	na	k7c4	na
Schottenkirchen	Schottenkirchen	k1gInSc4	Schottenkirchen
a	a	k8xC	a
jako	jako	k9	jako
alternativu	alternativa	k1gFnSc4	alternativa
předložili	předložit	k5eAaPmAgMnP	předložit
název	název	k1gInSc4	název
Rudolfsthal	Rudolfsthal	k1gMnSc1	Rudolfsthal
<g/>
.	.	kIx.	.
</s>
<s>
Nabídka	nabídka	k1gFnSc1	nabídka
byla	být	k5eAaImAgFnS	být
zamítnuta	zamítnout	k5eAaPmNgFnS	zamítnout
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
výběrem	výběr	k1gInSc7	výběr
jména	jméno	k1gNnPc4	jméno
jazykozpytec	jazykozpytec	k1gMnSc1	jazykozpytec
Richard	Richard	k1gMnSc1	Richard
Müller	Müller	k1gMnSc1	Müller
<g/>
.	.	kIx.	.
</s>
<s>
Domníval	domnívat	k5eAaImAgMnS	domnívat
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
k	k	k7c3	k
nejstaršímu	starý	k2eAgInSc3d3	nejstarší
tvaru	tvar	k1gInSc3	tvar
místního	místní	k2eAgInSc2d1	místní
názvu	název	k1gInSc2	název
vsi	ves	k1gFnSc2	ves
Gouwini	Gouwin	k2eAgMnPc1d1	Gouwin
(	(	kIx(	(
<g/>
Gaufreund	Gaufreund	k1gMnSc1	Gaufreund
=	=	kIx~	=
přítel	přítel	k1gMnSc1	přítel
župy	župa	k1gFnSc2	župa
<g/>
)	)	kIx)	)
a	a	k8xC	a
tak	tak	k6eAd1	tak
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
název	název	k1gInSc4	název
Gaweinstal	Gaweinstal	k1gFnSc2	Gaweinstal
<g/>
.	.	kIx.	.
</s>
<s>
Výnosem	výnos	k1gInSc7	výnos
c.	c.	k?	c.
a	a	k8xC	a
k.	k.	k?	k.
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
záležitostí	záležitost	k1gFnPc2	záležitost
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1917	[number]	k4	1917
byla	být	k5eAaImAgFnS	být
změna	změna	k1gFnSc1	změna
místního	místní	k2eAgInSc2d1	místní
názvu	název	k1gInSc2	název
Gaunersdorf	Gaunersdorf	k1gInSc1	Gaunersdorf
schválena	schválen	k2eAgFnSc1d1	schválena
na	na	k7c4	na
název	název	k1gInSc4	název
Gaweinstal	Gaweinstal	k1gFnSc2	Gaweinstal
<g/>
.	.	kIx.	.
</s>
<s>
Vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
bývalého	bývalý	k2eAgInSc2d1	bývalý
tvaru	tvar	k1gInSc2	tvar
názvu	název	k1gInSc2	název
Müllerem	Müller	k1gInSc7	Müller
bylo	být	k5eAaImAgNnS	být
sice	sice	k8xC	sice
stručné	stručný	k2eAgNnSc1d1	stručné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
na	na	k7c6	na
Gaweinstal	Gaweinstal	k1gFnSc6	Gaweinstal
byla	být	k5eAaImAgFnS	být
nesprávná	správný	k2eNgFnSc1d1	nesprávná
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
bezvýznamné	bezvýznamný	k2eAgNnSc1d1	bezvýznamné
<g/>
.	.	kIx.	.
</s>
<s>
Posměšné	posměšný	k2eAgNnSc1d1	posměšné
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
názvem	název	k1gInSc7	název
novým	nový	k2eAgInSc7d1	nový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Starověk	starověk	k1gInSc4	starověk
===	===	k?	===
</s>
</p>
<p>
<s>
Před	před	k7c7	před
výstavbou	výstavba	k1gFnSc7	výstavba
severní	severní	k2eAgFnSc2d1	severní
dálnice	dálnice	k1gFnSc2	dálnice
A5	A5	k1gFnSc7	A5
prováděl	provádět	k5eAaImAgMnS	provádět
spolkový	spolkový	k2eAgInSc4d1	spolkový
památkový	památkový	k2eAgInSc4d1	památkový
ústav	ústav	k1gInSc4	ústav
v	v	k7c6	v
dosahu	dosah	k1gInSc6	dosah
obcí	obec	k1gFnPc2	obec
Gaweinstal	Gaweinstal	k1gFnSc4	Gaweinstal
a	a	k8xC	a
Pellendorf	Pellendorf	k1gInSc4	Pellendorf
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
archeologické	archeologický	k2eAgInPc4d1	archeologický
výzkumy	výzkum	k1gInPc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
června	červen	k1gInSc2	červen
2003	[number]	k4	2003
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
prozkoumána	prozkoumán	k2eAgFnSc1d1	prozkoumána
plocha	plocha	k1gFnSc1	plocha
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
metrů	metr	k1gInPc2	metr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
,	,	kIx,	,
objevilo	objevit	k5eAaPmAgNnS	objevit
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
osídlených	osídlený	k2eAgNnPc2d1	osídlené
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
dokonce	dokonce	k9	dokonce
z	z	k7c2	z
období	období	k1gNnSc2	období
před	před	k7c7	před
1400	[number]	k4	1400
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgNnSc1	první
osídlení	osídlení	k1gNnSc1	osídlení
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
již	již	k6eAd1	již
v	v	k7c6	v
mladší	mladý	k2eAgFnSc3d2	mladší
době	doba	k1gFnSc3	doba
kamenné	kamenný	k2eAgFnSc3d1	kamenná
(	(	kIx(	(
<g/>
neolitu	neolit	k1gInSc6	neolit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
badenské	badenský	k2eAgFnSc2d1	badenská
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
asi	asi	k9	asi
4000	[number]	k4	4000
let	léto	k1gNnPc2	léto
před	před	k7c7	před
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
mladší	mladý	k2eAgFnSc3d2	mladší
době	doba	k1gFnSc3	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
(	(	kIx(	(
<g/>
od	od	k7c2	od
asi	asi	k9	asi
2000	[number]	k4	2000
před	před	k7c4	před
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pahorek	pahorek	k1gInSc1	pahorek
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1600	[number]	k4	1600
před	před	k7c4	před
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
také	také	k9	také
prozkoumán	prozkoumat	k5eAaPmNgInS	prozkoumat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
větší	veliký	k2eAgNnSc1d2	veliký
osídlení	osídlení	k1gNnSc1	osídlení
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
mladší	mladý	k2eAgFnSc6d2	mladší
době	doba	k1gFnSc6	doba
železné	železný	k2eAgFnSc6d1	železná
a	a	k8xC	a
pozdní	pozdní	k2eAgFnSc6d1	pozdní
době	doba	k1gFnSc6	doba
železné	železný	k2eAgFnSc2d1	železná
v	v	k7c6	v
době	doba	k1gFnSc6	doba
450	[number]	k4	450
let	léto	k1gNnPc2	léto
před	před	k7c7	před
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
jen	jen	k9	jen
málo	málo	k1gNnSc1	málo
nálezu	nález	k1gInSc2	nález
našlo	najít	k5eAaPmAgNnS	najít
neporušených	porušený	k2eNgMnPc2d1	neporušený
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
rozptýleny	rozptýlit	k5eAaPmNgFnP	rozptýlit
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
prozkoumané	prozkoumaný	k2eAgFnSc6d1	prozkoumaná
ploše	plocha	k1gFnSc6	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
nálezů	nález	k1gInPc2	nález
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
germánského	germánský	k2eAgNnSc2d1	germánské
osídlení	osídlení	k1gNnSc2	osídlení
–	–	k?	–
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Markomanů	Markoman	k1gMnPc2	Markoman
a	a	k8xC	a
Quadenů	Quaden	k1gMnPc2	Quaden
–	–	k?	–
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
přidružují	přidružovat	k5eAaImIp3nP	přidružovat
hlavně	hlavně	k9	hlavně
hloubené	hloubený	k2eAgFnPc1d1	hloubená
boudy	bouda	k1gFnPc1	bouda
<g/>
,	,	kIx,	,
sloupky	sloupek	k1gInPc1	sloupek
od	od	k7c2	od
vícelodních	vícelodní	k2eAgInPc2d1	vícelodní
obytných	obytný	k2eAgInPc2d1	obytný
domů	dům	k1gInPc2	dům
a	a	k8xC	a
popisují	popisovat	k5eAaImIp3nP	popisovat
hluboké	hluboký	k2eAgFnPc4d1	hluboká
jámy	jáma	k1gFnPc4	jáma
pro	pro	k7c4	pro
uskladnění	uskladnění	k1gNnSc4	uskladnění
zásob	zásoba	k1gFnPc2	zásoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Středověk	středověk	k1gInSc1	středověk
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c2	za
raného	raný	k2eAgInSc2d1	raný
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
i	i	k9	i
ze	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
až	až	k9	až
po	po	k7c4	po
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
mohl	moct	k5eAaImAgMnS	moct
spolkový	spolkový	k2eAgInSc4d1	spolkový
památkový	památkový	k2eAgInSc4d1	památkový
úřad	úřad	k1gInSc4	úřad
doložit	doložit	k5eAaPmF	doložit
hroby	hrob	k1gInPc4	hrob
další	další	k2eAgInPc1d1	další
význačná	význačný	k2eAgNnPc4d1	význačné
osídlení	osídlení	k1gNnSc4	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
byly	být	k5eAaImAgFnP	být
odkryty	odkryt	k2eAgFnPc4d1	odkryta
hloubené	hloubený	k2eAgFnPc4d1	hloubená
spížní	spížní	k2eAgFnPc4d1	spížní
jámy	jáma	k1gFnPc4	jáma
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
boudy	bouda	k1gFnPc4	bouda
či	či	k8xC	či
vinné	vinný	k2eAgInPc4d1	vinný
sklepy	sklep	k1gInPc4	sklep
s	s	k7c7	s
pravoúhlým	pravoúhlý	k2eAgInSc7d1	pravoúhlý
půdorysem	půdorys	k1gInSc7	půdorys
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
zdokumentovány	zdokumentován	k2eAgFnPc1d1	zdokumentována
rohové	rohový	k2eAgFnPc1d1	rohová
pece	pec	k1gFnPc1	pec
vybudované	vybudovaný	k2eAgFnPc1d1	vybudovaná
z	z	k7c2	z
lomového	lomový	k2eAgInSc2d1	lomový
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
byly	být	k5eAaImAgFnP	být
zjištěny	zjistit	k5eAaPmNgFnP	zjistit
kupolovité	kupolovitý	k2eAgFnPc1d1	kupolovitá
pece	pec	k1gFnPc1	pec
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
vykopány	vykopat	k5eAaPmNgFnP	vykopat
ve	v	k7c6	v
spraších	spraš	k1gFnPc6	spraš
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
archeologickému	archeologický	k2eAgInSc3d1	archeologický
výzkumu	výzkum	k1gInSc3	výzkum
často	často	k6eAd1	často
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hladina	hladina	k1gFnSc1	hladina
spodní	spodní	k2eAgFnSc2d1	spodní
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Závěr	závěr	k1gInSc1	závěr
====	====	k?	====
</s>
</p>
<p>
<s>
Gaunersdorf	Gaunersdorf	k1gInSc1	Gaunersdorf
byl	být	k5eAaImAgInS	být
založený	založený	k2eAgInSc1d1	založený
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1050	[number]	k4	1050
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
fázi	fáze	k1gFnSc6	fáze
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Babenberkové	Babenberkové	k2eAgFnSc1d1	Babenberkové
–	–	k?	–
bajuwarišská	bajuwarišský	k2eAgFnSc1d1	bajuwarišský
vyšší	vysoký	k2eAgFnSc1d2	vyšší
šlechta	šlechta	k1gFnSc1	šlechta
pocházející	pocházející	k2eAgFnSc1d1	pocházející
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
–	–	k?	–
jejich	jejich	k3xOp3gNnSc2	jejich
panství	panství	k1gNnSc2	panství
se	se	k3xPyFc4	se
rozpínala	rozpínat	k5eAaImAgFnS	rozpínat
přes	přes	k7c4	přes
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
Ostarrîchi	Ostarrîche	k1gFnSc4	Ostarrîche
<g/>
.	.	kIx.	.
</s>
<s>
Gaunersdorf	Gaunersdorf	k1gMnSc1	Gaunersdorf
sestával	sestávat	k5eAaImAgMnS	sestávat
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
samostatných	samostatný	k2eAgFnPc2d1	samostatná
osad	osada	k1gFnPc2	osada
Markt	Markt	k1gInSc1	Markt
<g/>
,	,	kIx,	,
Aigen	Aigen	k1gInSc1	Aigen
a	a	k8xC	a
Wieden-Gaunersdorf	Wieden-Gaunersdorf	k1gInSc1	Wieden-Gaunersdorf
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
tato	tento	k3xDgFnSc1	tento
osada	osada	k1gFnSc1	osada
byla	být	k5eAaImAgFnS	být
samostatně	samostatně	k6eAd1	samostatně
zpracovaná	zpracovaný	k2eAgFnSc1d1	zpracovaná
<g/>
.	.	kIx.	.
</s>
<s>
Poloha	poloha	k1gFnSc1	poloha
na	na	k7c6	na
"	"	kIx"	"
<g/>
Moravsko-Slezské	moravskolezský	k2eAgFnSc6d1	moravsko-slezská
cestě	cesta	k1gFnSc6	cesta
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
spolková	spolkový	k2eAgFnSc1d1	spolková
silnice	silnice	k1gFnSc1	silnice
<g/>
)	)	kIx)	)
sehrála	sehrát	k5eAaPmAgFnS	sehrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
životní	životní	k2eAgFnSc4d1	životní
roli	role	k1gFnSc4	role
při	při	k7c6	při
obchodu	obchod	k1gInSc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Markt-Gaunersdorf	Markt-Gaunersdorf	k1gInSc1	Markt-Gaunersdorf
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
markraběte	markrabě	k1gMnSc2	markrabě
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
již	již	k6eAd1	již
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1250	[number]	k4	1250
právo	právo	k1gNnSc1	právo
ročních	roční	k2eAgInPc2d1	roční
trhů	trh	k1gInPc2	trh
<g/>
,	,	kIx,	,
týdenních	týdenní	k2eAgInPc2d1	týdenní
trhů	trh	k1gInPc2	trh
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
výsady	výsada	k1gFnPc4	výsada
a	a	k8xC	a
soudní	soudní	k2eAgFnSc4d1	soudní
pravomoc	pravomoc	k1gFnSc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Wieden-Gaunersdorf	Wieden-Gaunersdorf	k1gMnSc1	Wieden-Gaunersdorf
byl	být	k5eAaImAgMnS	být
sídlem	sídlo	k1gNnSc7	sídlo
farního	farní	k2eAgInSc2d1	farní
úřadu	úřad	k1gInSc2	úřad
a	a	k8xC	a
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
v	v	k7c6	v
dokumentech	dokument	k1gInPc6	dokument
o	o	k7c4	o
místu	místo	k1gNnSc3	místo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1236	[number]	k4	1236
<g/>
,	,	kIx,	,
když	když	k8xS	když
vévoda	vévoda	k1gMnSc1	vévoda
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Bojovný	bojovný	k2eAgMnSc1d1	bojovný
(	(	kIx(	(
<g/>
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
1246	[number]	k4	1246
<g/>
)	)	kIx)	)
převzal	převzít	k5eAaPmAgInS	převzít
panství	panství	k1gNnSc4	panství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1360	[number]	k4	1360
vévoda	vévoda	k1gMnSc1	vévoda
Rudolf	Rudolf	k1gMnSc1	Rudolf
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
(	(	kIx(	(
<g/>
1339	[number]	k4	1339
<g/>
-	-	kIx~	-
<g/>
1365	[number]	k4	1365
<g/>
)	)	kIx)	)
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
měšťanům	měšťan	k1gMnPc3	měšťan
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
měli	mít	k5eAaImAgMnP	mít
od	od	k7c2	od
jeho	on	k3xPp3gMnSc2	on
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
i	i	k8xC	i
konání	konání	k1gNnSc2	konání
dvou	dva	k4xCgInPc2	dva
týdenních	týdenní	k2eAgInPc2d1	týdenní
trhů	trh	k1gInPc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
další	další	k2eAgNnSc4d1	další
potvrzení	potvrzení	k1gNnSc4	potvrzení
práv	právo	k1gNnPc2	právo
provedl	provést	k5eAaPmAgMnS	provést
také	také	k9	také
vévoda	vévoda	k1gMnSc1	vévoda
Albrecht	Albrecht	k1gMnSc1	Albrecht
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Habsburský	habsburský	k2eAgInSc1d1	habsburský
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1349	[number]	k4	1349
<g/>
-	-	kIx~	-
<g/>
1395	[number]	k4	1395
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1369	[number]	k4	1369
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
loupeživý	loupeživý	k2eAgMnSc1d1	loupeživý
rytíř	rytíř	k1gMnSc1	rytíř
"	"	kIx"	"
<g/>
Gamareth	Gamareth	k1gMnSc1	Gamareth
Fronauer	Fronauer	k1gMnSc1	Fronauer
<g/>
"	"	kIx"	"
z	z	k7c2	z
císařského	císařský	k2eAgNnSc2d1	císařské
vojska	vojsko	k1gNnSc2	vojsko
na	na	k7c4	na
rozkaz	rozkaz	k1gInSc4	rozkaz
císaře	císař	k1gMnSc2	císař
Fridricha	Fridrich	k1gMnSc2	Fridrich
III	III	kA	III
<g/>
.	.	kIx.	.
z	z	k7c2	z
Orth	Ortha	k1gFnPc2	Ortha
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Donau	donau	k1gNnPc6	donau
,	,	kIx,	,
pevně	pevně	k6eAd1	pevně
držel	držet	k5eAaImAgInS	držet
Groß-Schweinbarth	Groß-Schweinbarth	k1gInSc1	Groß-Schweinbarth
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1460	[number]	k4	1460
přepadl	přepadnout	k5eAaPmAgMnS	přepadnout
Gaunersdorf	Gaunersdorf	k1gMnSc1	Gaunersdorf
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
zdroje	zdroj	k1gInPc1	zdroj
udávají	udávat	k5eAaImIp3nP	udávat
<g/>
,	,	kIx,	,
pořádal	pořádat	k5eAaImAgMnS	pořádat
řeže	řezat	k5eAaImIp3nS	řezat
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yQgNnPc6	který
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k6eAd1	mnoho
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
Fronauer	Fronaura	k1gFnPc2	Fronaura
se	se	k3xPyFc4	se
opakovaně	opakovaně	k6eAd1	opakovaně
vracel	vracet	k5eAaImAgMnS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
nato	nato	k6eAd1	nato
<g/>
,	,	kIx,	,
1462	[number]	k4	1462
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
opět	opět	k6eAd1	opět
utlačována	utlačován	k2eAgFnSc1d1	utlačována
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
Podenski	Podenske	k1gFnSc4	Podenske
<g/>
,	,	kIx,	,
loupežník	loupežník	k1gMnSc1	loupežník
ze	z	k7c2	z
Slavonie	Slavonie	k1gFnSc2	Slavonie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
"	"	kIx"	"
<g/>
žádné	žádný	k3yNgFnSc3	žádný
starce	starka	k1gFnSc3	starka
a	a	k8xC	a
ženy	žena	k1gFnSc2	žena
nešetřil	šetřit	k5eNaImAgMnS	šetřit
<g/>
,	,	kIx,	,
duchovní	duchovní	k2eAgInPc4d1	duchovní
a	a	k8xC	a
světské	světský	k2eAgInPc4d1	světský
do	do	k7c2	do
řetězů	řetěz	k1gInPc2	řetěz
poutal	poutat	k5eAaImAgInS	poutat
a	a	k8xC	a
kostel	kostel	k1gInSc1	kostel
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
rolníky	rolník	k1gMnPc4	rolník
-	-	kIx~	-
i	i	k8xC	i
občany	občan	k1gMnPc4	občan
olupoval	olupovat	k5eAaImAgMnS	olupovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
Heinrich	Heinrich	k1gMnSc1	Heinrich
von	von	k1gInSc4	von
Lichtenstein	Lichtenstein	k1gInSc1	Lichtenstein
1483	[number]	k4	1483
zřídil	zřídit	k5eAaPmAgInS	zřídit
řemesla	řemeslo	k1gNnPc4	řemeslo
<g/>
,	,	kIx,	,
a	a	k8xC	a
žádnou	žádný	k3yNgFnSc4	žádný
šanci	šance	k1gFnSc4	šance
loupežníkům	loupežník	k1gMnPc3	loupežník
neumožnil	umožnit	k5eNaPmAgInS	umožnit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Novověk	novověk	k1gInSc4	novověk
===	===	k?	===
</s>
</p>
<p>
<s>
Rokem	rok	k1gInSc7	rok
1522	[number]	k4	1522
přišla	přijít	k5eAaPmAgFnS	přijít
reformace	reformace	k1gFnSc1	reformace
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
jenom	jenom	k9	jenom
Wieden-Gaunersdorfu	Wieden-Gaunersdorf	k1gMnSc3	Wieden-Gaunersdorf
<g/>
,	,	kIx,	,
Aigen-Gaunersdorfu	Aigen-Gaunersdorf	k1gMnSc3	Aigen-Gaunersdorf
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zůstali	zůstat	k5eAaPmAgMnP	zůstat
u	u	k7c2	u
staré	starý	k2eAgFnSc2d1	stará
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Učení	učení	k1gNnSc1	učení
Martina	Martin	k2eAgNnSc2d1	Martino
Luthera	Luthero	k1gNnSc2	Luthero
vydrželo	vydržet	k5eAaPmAgNnS	vydržet
sice	sice	k8xC	sice
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
na	na	k7c6	na
tahu	tah	k1gInSc6	tah
byl	být	k5eAaImAgMnS	být
ale	ale	k8xC	ale
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
biskup	biskup	k1gMnSc1	biskup
Melchior	Melchior	k1gMnSc1	Melchior
Khlesl	Khlesl	k1gMnSc1	Khlesl
a	a	k8xC	a
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
protireformaci	protireformace	k1gFnSc4	protireformace
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1604	[number]	k4	1604
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
opět	opět	k6eAd1	opět
katolická	katolický	k2eAgFnSc1d1	katolická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
první	první	k4xOgNnSc1	první
turecké	turecký	k2eAgNnSc1d1	turecké
obléhání	obléhání	k1gNnSc1	obléhání
bylo	být	k5eAaImAgNnS	být
záležitostí	záležitost	k1gFnSc7	záležitost
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
vojáků	voják	k1gMnPc2	voják
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
Osmanů	Osman	k1gMnPc2	Osman
však	však	k9	však
těžce	těžce	k6eAd1	těžce
zasahovalo	zasahovat	k5eAaImAgNnS	zasahovat
<g/>
.	.	kIx.	.
</s>
<s>
Gaunersdorf	Gaunersdorf	k1gInSc1	Gaunersdorf
měl	mít	k5eAaImAgInS	mít
ale	ale	k9	ale
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
lese	les	k1gInSc6	les
na	na	k7c6	na
"	"	kIx"	"
<g/>
Hochleiten	Hochleitno	k1gNnPc2	Hochleitno
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInPc2	tisíc
mužů	muž	k1gMnPc2	muž
císařského	císařský	k2eAgNnSc2d1	císařské
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
Turci	Turek	k1gMnPc1	Turek
místo	místo	k1gNnSc4	místo
pustošili	pustošit	k5eAaImAgMnP	pustošit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nastalém	nastalý	k2eAgInSc6d1	nastalý
boji	boj	k1gInSc6	boj
bylo	být	k5eAaImAgNnS	být
zajato	zajmout	k5eAaPmNgNnS	zajmout
mnoho	mnoho	k4c1	mnoho
Turků	Turek	k1gMnPc2	Turek
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
drženi	držet	k5eAaImNgMnP	držet
v	v	k7c6	v
Gaunersdorferském	Gaunersdorferský	k2eAgNnSc6d1	Gaunersdorferský
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
<g/>
Vesnice	vesnice	k1gFnSc1	vesnice
Gaunersdorf	Gaunersdorf	k1gInSc4	Gaunersdorf
trpěla	trpět	k5eAaImAgFnS	trpět
také	také	k9	také
za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
<g/>
,	,	kIx,	,
když	když	k8xS	když
hrabě	hrabě	k1gMnSc1	hrabě
Jindřich	Jindřich	k1gMnSc1	Jindřich
Matyáš	Matyáš	k1gMnSc1	Matyáš
z	z	k7c2	z
Thurnu	Thurn	k1gInSc2	Thurn
(	(	kIx(	(
<g/>
1567	[number]	k4	1567
<g/>
-	-	kIx~	-
<g/>
1640	[number]	k4	1640
<g/>
)	)	kIx)	)
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
povstaleckých	povstalecký	k2eAgMnPc2d1	povstalecký
Čechů	Čech	k1gMnPc2	Čech
táhl	táhnout	k5eAaImAgInS	táhnout
na	na	k7c4	na
Vídeň	Vídeň	k1gFnSc4	Vídeň
<g/>
,	,	kIx,	,
vojáci	voják	k1gMnPc1	voják
ves	ves	k1gFnSc4	ves
zase	zase	k9	zase
drancovali	drancovat	k5eAaImAgMnP	drancovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
došlo	dojít	k5eAaPmAgNnS	dojít
však	však	k9	však
k	k	k7c3	k
nejhoršímu	zlý	k2eAgMnSc3d3	nejhorší
<g/>
:	:	kIx,	:
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Jankau	Jankaus	k1gInSc2	Jankaus
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Jankov	Jankov	k1gInSc4	Jankov
<g/>
)	)	kIx)	)
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
asi	asi	k9	asi
60	[number]	k4	60
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poslední	poslední	k2eAgFnSc3d1	poslední
velké	velký	k2eAgFnSc3d1	velká
bitvě	bitva	k1gFnSc3	bitva
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1645	[number]	k4	1645
švédské	švédský	k2eAgNnSc1d1	švédské
protestantské	protestantský	k2eAgNnSc1d1	protestantské
vojsko	vojsko	k1gNnSc1	vojsko
pod	pod	k7c7	pod
polním	polní	k2eAgMnSc7d1	polní
maršálem	maršál	k1gMnSc7	maršál
Lennartem	Lennart	k1gMnSc7	Lennart
Torstensonem	Torstenson	k1gMnSc7	Torstenson
porazilo	porazit	k5eAaPmAgNnS	porazit
habsburská	habsburský	k2eAgFnSc1d1	habsburská
vojska	vojsko	k1gNnSc2	vojsko
polního	polní	k2eAgMnSc2d1	polní
maršála	maršál	k1gMnSc2	maršál
von	von	k1gInSc4	von
Hatzfeldta	Hatzfeldto	k1gNnSc2	Hatzfeldto
(	(	kIx(	(
<g/>
1593	[number]	k4	1593
<g/>
-	-	kIx~	-
<g/>
1658	[number]	k4	1658
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
otevřela	otevřít	k5eAaPmAgFnS	otevřít
cesta	cesta	k1gFnSc1	cesta
pro	pro	k7c4	pro
švédské	švédský	k2eAgNnSc4d1	švédské
vojsko	vojsko	k1gNnSc4	vojsko
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
Torstensson	Torstensson	k1gMnSc1	Torstensson
táhl	táhnout	k5eAaImAgMnS	táhnout
do	do	k7c2	do
Dolních	dolní	k2eAgInPc2d1	dolní
Rakous	Rakousy	k1gInPc2	Rakousy
<g/>
,	,	kIx,	,
zanechával	zanechávat	k5eAaImAgMnS	zanechávat
za	za	k7c7	za
sebou	se	k3xPyFc7	se
stopy	stopa	k1gFnPc4	stopa
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
až	až	k9	až
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
a	a	k8xC	a
dal	dát	k5eAaPmAgInS	dát
celou	celý	k2eAgFnSc4d1	celá
ves	ves	k1gFnSc4	ves
Gaunersdorf	Gaunersdorf	k1gMnSc1	Gaunersdorf
vypálit	vypálit	k5eAaPmF	vypálit
a	a	k8xC	a
srovnat	srovnat	k5eAaPmF	srovnat
se	se	k3xPyFc4	se
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnSc1d1	místní
kronikář	kronikář	k1gMnSc1	kronikář
Martin	Martin	k1gMnSc1	Martin
Merkh	Merkh	k1gMnSc1	Merkh
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Roku	rok	k1gInSc2	rok
1645	[number]	k4	1645
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc4d1	celý
městys	městys	k1gInSc4	městys
Gaunersdorf	Gaunersdorf	k1gMnSc1	Gaunersdorf
i	i	k9	i
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
a	a	k8xC	a
okolními	okolní	k2eAgFnPc7d1	okolní
osadami	osada	k1gFnPc7	osada
nepřítelem	nepřítel	k1gMnSc7	nepřítel
vypálený	vypálený	k2eAgInSc1d1	vypálený
a	a	k8xC	a
lehl	lehnout	k5eAaPmAgInS	lehnout
popelem	popel	k1gInSc7	popel
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
ztrátu	ztráta	k1gFnSc4	ztráta
utrpěl	utrpět	k5eAaPmAgInS	utrpět
Gaunersdorf	Gaunersdorf	k1gInSc1	Gaunersdorf
hrubým	hrubý	k2eAgNnSc7d1	hrubé
drancováním	drancování	k1gNnSc7	drancování
<g/>
,	,	kIx,	,
požáry	požár	k1gInPc4	požár
a	a	k8xC	a
ztrátu	ztráta	k1gFnSc4	ztráta
stovek	stovka	k1gFnPc2	stovka
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
celé	celý	k2eAgInPc1d1	celý
domy	dům	k1gInPc1	dům
i	i	k9	i
s	s	k7c7	s
palivovým	palivový	k2eAgNnSc7d1	palivové
dřívím	dříví	k1gNnSc7	dříví
změnily	změnit	k5eAaPmAgFnP	změnit
v	v	k7c6	v
požářiště	požářiště	k1gNnSc4	požářiště
<g/>
,	,	kIx,	,
neštěstí	neštěstí	k1gNnSc1	neštěstí
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
nepěknou	pěkný	k2eNgFnSc7d1	nepěkná
podívanou	podívaná	k1gFnSc7	podívaná
na	na	k7c4	na
Gaunersdorf	Gaunersdorf	k1gInSc4	Gaunersdorf
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
nebo	nebo	k8xC	nebo
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
válkou	válka	k1gFnSc7	válka
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
nouzi	nouze	k1gFnSc4	nouze
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
:	:	kIx,	:
z	z	k7c2	z
tolika	tolik	k4xDc2	tolik
stovek	stovka	k1gFnPc2	stovka
zámožných	zámožný	k2eAgFnPc2d1	zámožná
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
ubožáci	ubožák	k1gMnPc1	ubožák
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Bída	bída	k1gFnSc1	bída
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
i	i	k8xC	i
vlasti	vlast	k1gFnSc2	vlast
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
stěží	stěží	k6eAd1	stěží
popsat	popsat	k5eAaPmF	popsat
<g/>
.	.	kIx.	.
</s>
<s>
Gaunersdorf	Gaunersdorf	k1gMnSc1	Gaunersdorf
musí	muset	k5eAaImIp3nS	muset
všechnu	všechen	k3xTgFnSc4	všechen
myslitelnou	myslitelný	k2eAgFnSc4d1	myslitelná
bídu	bída	k1gFnSc4	bída
a	a	k8xC	a
potíže	potíž	k1gFnPc4	potíž
způsobené	způsobený	k2eAgFnPc4d1	způsobená
touto	tento	k3xDgFnSc7	tento
válkou	válka	k1gFnSc7	válka
snášet	snášet	k5eAaImF	snášet
<g/>
;	;	kIx,	;
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
uprchlo	uprchnout	k5eAaPmAgNnS	uprchnout
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
ukryla	ukrýt	k5eAaPmAgFnS	ukrýt
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
do	do	k7c2	do
podzemí	podzemí	k1gNnSc2	podzemí
stájí	stáj	k1gFnPc2	stáj
<g/>
.	.	kIx.	.
</s>
<s>
Lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
,	,	kIx,	,
svlékli	svléknout	k5eAaPmAgMnP	svléknout
a	a	k8xC	a
ubohé	ubohý	k2eAgMnPc4d1	ubohý
tloukli	tlouct	k5eAaImAgMnP	tlouct
<g/>
,	,	kIx,	,
potom	potom	k8xC	potom
většinu	většina	k1gFnSc4	většina
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
umučili	umučit	k5eAaPmAgMnP	umučit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
uprchlíky	uprchlík	k1gMnPc4	uprchlík
nepřítel	nepřítel	k1gMnSc1	nepřítel
zadržel	zadržet	k5eAaPmAgMnS	zadržet
<g/>
,	,	kIx,	,
setnul	setnout	k5eAaPmAgMnS	setnout
je	být	k5eAaImIp3nS	být
šavlí	šavle	k1gFnPc2	šavle
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
lidí	člověk	k1gMnPc2	člověk
ukrývajících	ukrývající	k2eAgMnPc2d1	ukrývající
se	se	k3xPyFc4	se
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
před	před	k7c7	před
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
,	,	kIx,	,
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
udušením	udušení	k1gNnSc7	udušení
od	od	k7c2	od
zápachu	zápach	k1gInSc2	zápach
a	a	k8xC	a
kouře	kouř	k1gInSc2	kouř
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
tisíc	tisíc	k4xCgInSc4	tisíc
lidí	člověk	k1gMnPc2	člověk
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
z	z	k7c2	z
nouze	nouze	k1gFnSc2	nouze
a	a	k8xC	a
hladu	hlad	k1gInSc2	hlad
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
žalostný	žalostný	k2eAgInSc1d1	žalostný
stav	stav	k1gInSc1	stav
trval	trvat	k5eAaImAgInS	trvat
celých	celý	k2eAgInPc2d1	celý
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Takové	takový	k3xDgInPc1	takový
podzemní	podzemní	k2eAgInPc1d1	podzemní
chlévy	chlév	k1gInPc1	chlév
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	on	k3xPp3gNnPc4	on
popisuje	popisovat	k5eAaImIp3nS	popisovat
kronikář	kronikář	k1gMnSc1	kronikář
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Gaweinstalu	Gaweinstal	k1gMnSc6	Gaweinstal
objeveny	objevit	k5eAaPmNgInP	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
také	také	k9	také
funkce	funkce	k1gFnSc1	funkce
podzemních	podzemní	k2eAgFnPc2d1	podzemní
stájí	stáj	k1gFnPc2	stáj
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
osvětlena	osvětlit	k5eAaPmNgFnS	osvětlit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
druhém	druhý	k4xOgInSc6	druhý
obléhání	obléhání	k1gNnSc6	obléhání
Vídně	Vídeň	k1gFnSc2	Vídeň
Turky	Turek	k1gMnPc4	Turek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1683	[number]	k4	1683
bylo	být	k5eAaImAgNnS	být
zase	zase	k9	zase
pustošeno	pustošen	k2eAgNnSc1d1	pustošen
zájmové	zájmový	k2eAgNnSc1d1	zájmové
území	území	k1gNnSc1	území
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
také	také	k9	také
Gaunersdorf	Gaunersdorf	k1gInSc1	Gaunersdorf
byl	být	k5eAaImAgInS	být
zatížený	zatížený	k2eAgInSc1d1	zatížený
ubytováním	ubytování	k1gNnSc7	ubytování
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
.	.	kIx.	.
</s>
<s>
Polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Sobieski	Sobieski	k1gNnSc1	Sobieski
(	(	kIx(	(
<g/>
1629	[number]	k4	1629
<g/>
-	-	kIx~	-
<g/>
1696	[number]	k4	1696
<g/>
)	)	kIx)	)
táhl	táhnout	k5eAaImAgMnS	táhnout
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
vojskem	vojsko	k1gNnSc7	vojsko
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
také	také	k9	také
přes	přes	k7c4	přes
Gaunersdorf	Gaunersdorf	k1gInSc4	Gaunersdorf
<g/>
..	..	k?	..
<g/>
Během	během	k7c2	během
krátkého	krátký	k2eAgNnSc2d1	krátké
povstání	povstání	k1gNnSc2	povstání
1703-1706	[number]	k4	1703-1706
byla	být	k5eAaImAgFnS	být
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
Dolních	dolní	k2eAgInPc2d1	dolní
Rakous	Rakousy	k1gInPc2	Rakousy
vtažena	vtáhnout	k5eAaPmNgFnS	vtáhnout
také	také	k6eAd1	také
do	do	k7c2	do
strádání	strádání	k1gNnSc2	strádání
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1704	[number]	k4	1704
byl	být	k5eAaImAgMnS	být
také	také	k9	také
Gaunersdorf	Gaunersdorf	k1gMnSc1	Gaunersdorf
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
<g/>
,	,	kIx,	,
občané	občan	k1gMnPc1	občan
své	svůj	k3xOyFgFnPc4	svůj
zásoby	zásoba	k1gFnPc4	zásoba
ukrývali	ukrývat	k5eAaImAgMnP	ukrývat
do	do	k7c2	do
podzemních	podzemní	k2eAgFnPc2d1	podzemní
stájí	stáj	k1gFnPc2	stáj
<g/>
,	,	kIx,	,
dobytek	dobytek	k1gInSc4	dobytek
soustřeďovali	soustřeďovat	k5eAaImAgMnP	soustřeďovat
do	do	k7c2	do
bezpečí	bezpečí	k1gNnSc2	bezpečí
ve	v	k7c6	v
"	"	kIx"	"
<g/>
Wachtbergu	Wachtberg	k1gInSc6	Wachtberg
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
opevnili	opevnit	k5eAaPmAgMnP	opevnit
a	a	k8xC	a
proti	proti	k7c3	proti
přibližujícím	přibližující	k2eAgInPc3d1	přibližující
se	se	k3xPyFc4	se
Kurucům	kuruc	k1gMnPc3	kuruc
se	se	k3xPyFc4	se
bránili	bránit	k5eAaImAgMnP	bránit
ohněm	oheň	k1gInSc7	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
neočekávaný	očekávaný	k2eNgInSc1d1	neočekávaný
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
neorganizovaný	organizovaný	k2eNgInSc4d1	neorganizovaný
odpor	odpor	k1gInSc4	odpor
zahnal	zahnat	k5eAaPmAgMnS	zahnat
útočníky	útočník	k1gMnPc4	útočník
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1704	[number]	k4	1704
propukl	propuknout	k5eAaPmAgInS	propuknout
v	v	k7c6	v
"	"	kIx"	"
<g/>
Gaunersdorfu	Gaunersdorf	k1gInSc6	Gaunersdorf
<g/>
"	"	kIx"	"
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
celé	celý	k2eAgNnSc4d1	celé
místo	místo	k1gNnSc4	místo
i	i	k9	i
kostel	kostel	k1gInSc1	kostel
poškodil	poškodit	k5eAaPmAgInS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Bída	bída	k1gFnSc1	bída
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
oheň	oheň	k1gInSc1	oheň
také	také	k6eAd1	také
zničil	zničit	k5eAaPmAgInS	zničit
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
budovy	budova	k1gFnPc4	budova
a	a	k8xC	a
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
uskladněnou	uskladněný	k2eAgFnSc4d1	uskladněná
sklizeň	sklizeň	k1gFnSc1	sklizeň
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zdrojů	zdroj	k1gInPc2	zdroj
nevyplývá	vyplývat	k5eNaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
k	k	k7c3	k
požárům	požár	k1gInPc3	požár
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
náhoda	náhoda	k1gFnSc1	náhoda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
týkalo	týkat	k5eAaImAgNnS	týkat
Kuruců	kuruc	k1gMnPc2	kuruc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
zřízen	zřízen	k2eAgInSc1d1	zřízen
špitál	špitál	k1gInSc1	špitál
s	s	k7c7	s
"	"	kIx"	"
<g/>
chirurgií	chirurgie	k1gFnSc7	chirurgie
<g/>
"	"	kIx"	"
k	k	k7c3	k
výchově	výchova	k1gFnSc3	výchova
lékařů	lékař	k1gMnPc2	lékař
na	na	k7c4	na
ošetřování	ošetřování	k1gNnPc4	ošetřování
raněných	raněný	k1gMnPc2	raněný
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1753	[number]	k4	1753
byl	být	k5eAaImAgInS	být
Gaunersdorf	Gaunersdorf	k1gInSc1	Gaunersdorf
za	za	k7c2	za
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
ustanoven	ustanoven	k2eAgInSc4d1	ustanoven
okresním	okresní	k2eAgInSc7d1	okresní
úřadem	úřad	k1gInSc7	úřad
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
pod	pod	k7c7	pod
Manhartsbergem	Manhartsberg	k1gInSc7	Manhartsberg
(	(	kIx(	(
až	až	k6eAd1	až
do	do	k7c2	do
1764	[number]	k4	1764
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
"	"	kIx"	"
<g/>
Josefínského	josefínský	k2eAgNnSc2d1	josefínské
jmenování	jmenování	k1gNnSc2	jmenování
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1787	[number]	k4	1787
se	se	k3xPyFc4	se
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Městys	městys	k1gInSc1	městys
Gaunersdorf	Gaunersdorf	k1gInSc1	Gaunersdorf
<g/>
"	"	kIx"	"
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dobrý	dobrý	k2eAgInSc4d1	dobrý
kostel	kostel	k1gInSc4	kostel
se	s	k7c7	s
hřbitovní	hřbitovní	k2eAgFnSc7d1	hřbitovní
zdí	zeď	k1gFnSc7	zeď
<g/>
,	,	kIx,	,
faru	fara	k1gFnSc4	fara
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
hostince	hostinec	k1gInPc4	hostinec
<g/>
,	,	kIx,	,
poštu	pošta	k1gFnSc4	pošta
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
pevné	pevný	k2eAgInPc1d1	pevný
měšťanské	měšťanský	k2eAgInPc1d1	měšťanský
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
od	od	k7c2	od
ostatních	ostatní	k1gNnPc2	ostatní
mají	mít	k5eAaImIp3nP	mít
středně	středně	k6eAd1	středně
odolnou	odolný	k2eAgFnSc4d1	odolná
konstrukci	konstrukce	k1gFnSc4	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Městys	městys	k1gInSc1	městys
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dominují	dominovat	k5eAaImIp3nP	dominovat
"	"	kIx"	"
<g/>
Sulz	Sulz	k1gInSc1	Sulz
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Birken	Birken	k1gInSc1	Birken
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Schrickerberg	Schrickerberg	k1gInSc1	Schrickerberg
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
císaře	císař	k1gMnSc2	císař
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
"	"	kIx"	"
<g/>
Josefínských	josefínský	k2eAgInPc2d1	josefínský
reformních	reformní	k2eAgInPc2d1	reformní
zákonů	zákon	k1gInPc2	zákon
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1787	[number]	k4	1787
také	také	k6eAd1	také
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
pronásledování	pronásledování	k1gNnSc4	pronásledování
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
a	a	k8xC	a
čarodějů	čaroděj	k1gMnPc2	čaroděj
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Gaunersdorfu	Gaunersdorf	k1gInSc6	Gaunersdorf
známy	znám	k2eAgInPc4d1	znám
čarodějnické	čarodějnický	k2eAgInPc4d1	čarodějnický
procesy	proces	k1gInPc4	proces
<g/>
.	.	kIx.	.
<g/>
Mimo	mimo	k6eAd1	mimo
Kuruců	kuruc	k1gMnPc2	kuruc
a	a	k8xC	a
požárů	požár	k1gInPc2	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1704	[number]	k4	1704
neuplynulo	uplynout	k5eNaPmAgNnS	uplynout
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pro	pro	k7c4	pro
obec	obec	k1gFnSc4	obec
nikterak	nikterak	k6eAd1	nikterak
pokojně	pokojně	k6eAd1	pokojně
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
zatažena	zatáhnout	k5eAaPmNgFnS	zatáhnout
do	do	k7c2	do
koaliční	koaliční	k2eAgFnSc2d1	koaliční
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
Napoleonu	napoleon	k1gInSc3	napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
a	a	k8xC	a
do	do	k7c2	do
válečného	válečný	k2eAgNnSc2d1	válečné
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1797	[number]	k4	1797
c.	c.	k?	c.
a	a	k8xC	a
k.	k.	k?	k.
armádě	armáda	k1gFnSc3	armáda
obyvatelé	obyvatel	k1gMnPc1	obyvatel
odevzdali	odevzdat	k5eAaPmAgMnP	odevzdat
25	[number]	k4	25
pušek	puška	k1gFnPc2	puška
a	a	k8xC	a
polovina	polovina	k1gFnSc1	polovina
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
byla	být	k5eAaImAgFnS	být
nasazena	nasazen	k2eAgFnSc1d1	nasazena
k	k	k7c3	k
obranným	obranný	k2eAgFnPc3d1	obranná
pracím	práce	k1gFnPc3	práce
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
a	a	k8xC	a
zaplacení	zaplacení	k1gNnSc4	zaplacení
150	[number]	k4	150
zlatých	zlatá	k1gFnPc2	zlatá
na	na	k7c4	na
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
několik	několik	k4yIc1	několik
z	z	k7c2	z
gaunersdorferských	gaunersdorferský	k2eAgFnPc2d1	gaunersdorferský
se	se	k3xPyFc4	se
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
k	k	k7c3	k
vojenské	vojenský	k2eAgFnSc3d1	vojenská
službě	služba	k1gFnSc3	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
roce	rok	k1gInSc6	rok
c.	c.	k?	c.
a	a	k8xC	a
k.	k.	k?	k.
vojsko	vojsko	k1gNnSc1	vojsko
oloupilo	oloupit	k5eAaPmAgNnS	oloupit
občany	občan	k1gMnPc4	občan
o	o	k7c4	o
všechny	všechen	k3xTgFnPc4	všechen
zásoby	zásoba	k1gFnPc4	zásoba
palivového	palivový	k2eAgNnSc2d1	palivové
dříví	dříví	k1gNnSc2	dříví
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
ubytováno	ubytovat	k5eAaPmNgNnS	ubytovat
500	[number]	k4	500
francouzských	francouzský	k2eAgMnPc2d1	francouzský
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1805	[number]	k4	1805
ubytovali	ubytovat	k5eAaPmAgMnP	ubytovat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
francouzští	francouzský	k2eAgMnPc1d1	francouzský
vojáci	voják	k1gMnPc1	voják
a	a	k8xC	a
obec	obec	k1gFnSc1	obec
musela	muset	k5eAaImAgFnS	muset
napoleonským	napoleonský	k2eAgMnPc3d1	napoleonský
vojákům	voják	k1gMnPc3	voják
dodávat	dodávat	k5eAaImF	dodávat
zásoby	zásoba	k1gFnPc4	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Nepozornosti	nepozornost	k1gFnPc1	nepozornost
vojáků	voják	k1gMnPc2	voják
bylo	být	k5eAaImAgNnS	být
vypáleno	vypálit	k5eAaPmNgNnS	vypálit
11	[number]	k4	11
domů	dům	k1gInPc2	dům
v	v	k7c4	v
Aigen-Gaunersdorf	Aigen-Gaunersdorf	k1gInSc4	Aigen-Gaunersdorf
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
Francouzi	Francouz	k1gMnPc1	Francouz
smažili	smažit	k5eAaImAgMnP	smažit
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jinak	jinak	k6eAd1	jinak
vydala	vydat	k5eAaPmAgFnS	vydat
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
<g/>
:	:	kIx,	:
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Wagramu	Wagram	k1gInSc2	Wagram
dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1809	[number]	k4	1809
pochodovala	pochodovat	k5eAaImAgFnS	pochodovat
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
armáda	armáda	k1gFnSc1	armáda
<g/>
"	"	kIx"	"
na	na	k7c4	na
Gaunersdorf	Gaunersdorf	k1gInSc4	Gaunersdorf
a	a	k8xC	a
drancovala	drancovat	k5eAaImAgFnS	drancovat
obec	obec	k1gFnSc1	obec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
obecní	obecní	k2eAgFnSc2d1	obecní
kroniky	kronika	k1gFnSc2	kronika
byly	být	k5eAaImAgInP	být
domy	dům	k1gInPc1	dům
vydrancovány	vydrancován	k2eAgInPc1d1	vydrancován
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
také	také	k9	také
farář	farář	k1gMnSc1	farář
Paul	Paul	k1gMnSc1	Paul
Schmid	Schmid	k1gInSc4	Schmid
<g/>
,	,	kIx,	,
sedmdesátiletý	sedmdesátiletý	k2eAgMnSc1d1	sedmdesátiletý
stařec	stařec	k1gMnSc1	stařec
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
zpustošené	zpustošený	k2eAgFnSc2d1	zpustošená
<g/>
,	,	kIx,	,
samotný	samotný	k2eAgMnSc1d1	samotný
"	"	kIx"	"
<g/>
tabernakel	tabernakel	k1gMnSc1	tabernakel
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
baldachýn	baldachýn	k1gInSc1	baldachýn
<g/>
,	,	kIx,	,
nebesa	nebesa	k1gNnPc1	nebesa
<g/>
)	)	kIx)	)
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
i	i	k9	i
farní	farní	k2eAgInSc1d1	farní
dvůr	dvůr	k1gInSc1	dvůr
a	a	k8xC	a
ani	ani	k8xC	ani
škola	škola	k1gFnSc1	škola
nebyla	být	k5eNaImAgFnS	být
ušetřena	ušetřit	k5eAaPmNgFnS	ušetřit
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
domů	dům	k1gInPc2	dům
bylo	být	k5eAaImAgNnS	být
vypáleno	vypálen	k2eAgNnSc1d1	vypáleno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dívka	dívka	k1gFnSc1	dívka
"	"	kIx"	"
<g/>
vyvedená	vyvedený	k2eAgNnPc4d1	vyvedené
aprílovým	aprílový	k2eAgInSc7d1	aprílový
žertem	žert	k1gInSc7	žert
<g/>
"	"	kIx"	"
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1822	[number]	k4	1822
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
jedenáctiletým	jedenáctiletý	k2eAgMnSc7d1	jedenáctiletý
bratrem	bratr	k1gMnSc7	bratr
způsobili	způsobit	k5eAaPmAgMnP	způsobit
požár	požár	k1gInSc4	požár
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
vlastním	vlastní	k2eAgInSc6d1	vlastní
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
prudkou	prudký	k2eAgFnSc4d1	prudká
větrnou	větrný	k2eAgFnSc4d1	větrná
bouři	bouře	k1gFnSc4	bouře
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
obec	obec	k1gFnSc4	obec
<g/>
.	.	kIx.	.
</s>
<s>
Markt	Markt	k1gInSc1	Markt
<g/>
,	,	kIx,	,
Aigen	Aigen	k1gInSc1	Aigen
a	a	k8xC	a
Wieden-Gaunersdorf	Wieden-Gaunersdorf	k1gMnSc1	Wieden-Gaunersdorf
byl	být	k5eAaImAgMnS	být
během	během	k7c2	během
tří	tři	k4xCgFnPc2	tři
hodin	hodina	k1gFnPc2	hodina
uchvácen	uchvátit	k5eAaPmNgInS	uchvátit
plameny	plamen	k1gInPc7	plamen
a	a	k8xC	a
jen	jen	k9	jen
málokterý	málokterý	k3yIgInSc1	málokterý
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
i	i	k9	i
kostel	kostel	k1gInSc1	kostel
nebyl	být	k5eNaImAgInS	být
ušetřen	ušetřit	k5eAaPmNgInS	ušetřit
<g/>
.	.	kIx.	.
</s>
<s>
Požár	požár	k1gInSc1	požár
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
že	že	k8xS	že
ohrožoval	ohrožovat	k5eAaImAgMnS	ohrožovat
i	i	k9	i
sousední	sousední	k2eAgFnPc4d1	sousední
obce	obec	k1gFnPc4	obec
Bad	Bad	k1gMnSc1	Bad
Pirawarth	Pirawarth	k1gMnSc1	Pirawarth
a	a	k8xC	a
Kollnbrunn	Kollnbrunn	k1gMnSc1	Kollnbrunn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
katastrofálních	katastrofální	k2eAgInPc6d1	katastrofální
požárech	požár	k1gInPc6	požár
1645	[number]	k4	1645
a	a	k8xC	a
1704	[number]	k4	1704
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
třetí	třetí	k4xOgFnSc7	třetí
jizvou	jizva	k1gFnSc7	jizva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obec	obec	k1gFnSc1	obec
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
požáry	požár	k1gInPc4	požár
zničila	zničit	k5eAaPmAgFnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
nato	nato	k6eAd1	nato
1823	[number]	k4	1823
jedenáctiletý	jedenáctiletý	k2eAgInSc1d1	jedenáctiletý
"	"	kIx"	"
<g/>
pyroman	pyroman	k1gMnSc1	pyroman
<g/>
"	"	kIx"	"
opět	opět	k6eAd1	opět
založil	založit	k5eAaPmAgMnS	založit
oheň	oheň	k1gInSc4	oheň
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
přistižen	přistižen	k2eAgMnSc1d1	přistižen
a	a	k8xC	a
přiznal	přiznat	k5eAaPmAgMnS	přiznat
se	se	k3xPyFc4	se
také	také	k9	také
k	k	k7c3	k
požáru	požár	k1gInSc3	požár
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1822	[number]	k4	1822
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
neznámému	známý	k2eNgInSc3d1	neznámý
trestu	trest	k1gInSc3	trest
<g/>
,	,	kIx,	,
převezen	převezen	k2eAgInSc1d1	převezen
do	do	k7c2	do
vídeňského	vídeňský	k2eAgNnSc2d1	Vídeňské
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
při	při	k7c6	při
povodni	povodeň	k1gFnSc6	povodeň
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
utonul	utonout	k5eAaPmAgMnS	utonout
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
cele	cela	k1gFnSc6	cela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
revoluce	revoluce	k1gFnSc2	revoluce
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1848	[number]	k4	1848
<g/>
/	/	kIx~	/
<g/>
49	[number]	k4	49
v	v	k7c6	v
rakouském	rakouský	k2eAgNnSc6d1	rakouské
císařství	císařství	k1gNnSc6	císařství
byly	být	k5eAaImAgFnP	být
zakládány	zakládán	k2eAgFnPc1d1	zakládána
"	"	kIx"	"
<g/>
Národní	národní	k2eAgFnPc1d1	národní
gardy	garda	k1gFnPc1	garda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
"	"	kIx"	"
<g/>
Gaunersdorfu	Gaunersdorf	k1gInSc6	Gaunersdorf
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
kronikář	kronikář	k1gMnSc1	kronikář
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
hodně	hodně	k6eAd1	hodně
času	čas	k1gInSc2	čas
při	při	k7c6	při
neúčelném	účelný	k2eNgNnSc6d1	neúčelné
cvičení	cvičení	k1gNnSc6	cvičení
promarnila	promarnit	k5eAaPmAgFnS	promarnit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tažení	tažení	k1gNnSc6	tažení
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
obsadil	obsadit	k5eAaPmAgMnS	obsadit
také	také	k9	také
Gaunersdorf	Gaunersdorf	k1gMnSc1	Gaunersdorf
polní	polní	k2eAgMnSc1d1	polní
maršál	maršál	k1gMnSc1	maršál
kníže	kníže	k1gMnSc1	kníže
Alfred	Alfred	k1gMnSc1	Alfred
Windischgrätz	Windischgrätz	k1gMnSc1	Windischgrätz
(	(	kIx(	(
<g/>
1787	[number]	k4	1787
<g/>
-	-	kIx~	-
<g/>
1862	[number]	k4	1862
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
si	se	k3xPyFc3	se
pospíšilo	pospíšit	k5eAaPmAgNnS	pospíšit
a	a	k8xC	a
věrnost	věrnost	k1gFnSc4	věrnost
císaři	císař	k1gMnSc3	císař
obec	obec	k1gFnSc1	obec
přislíbila	přislíbit	k5eAaPmAgFnS	přislíbit
<g/>
.	.	kIx.	.
</s>
<s>
Windischgrätz	Windischgrätz	k1gInSc1	Windischgrätz
nařídil	nařídit	k5eAaPmAgInS	nařídit
obci	obec	k1gFnSc3	obec
další	další	k2eAgFnSc2d1	další
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
určil	určit	k5eAaPmAgInS	určit
věnovat	věnovat	k5eAaPmF	věnovat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
pozornost	pozornost	k1gFnSc4	pozornost
revolučnímu	revoluční	k2eAgNnSc3d1	revoluční
hlavnímu	hlavní	k2eAgNnSc3d1	hlavní
městu	město	k1gNnSc3	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
německé	německý	k2eAgFnPc1d1	německá
války	válka	k1gFnPc1	válka
měly	mít	k5eAaImAgFnP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
Gaunersdorf	Gaunersdorf	k1gInSc4	Gaunersdorf
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
opět	opět	k6eAd1	opět
nucena	nutit	k5eAaImNgFnS	nutit
ubytováním	ubytování	k1gNnSc7	ubytování
a	a	k8xC	a
dodávkami	dodávka	k1gFnPc7	dodávka
pomáhat	pomáhat	k5eAaImF	pomáhat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1866	[number]	k4	1866
táhla	táhnout	k5eAaImAgFnS	táhnout
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
rakouského	rakouský	k2eAgNnSc2d1	rakouské
vojska	vojsko	k1gNnSc2	vojsko
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
následovala	následovat	k5eAaImAgFnS	následovat
zase	zase	k9	zase
Pruská	pruský	k2eAgFnSc1d1	pruská
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Újma	újma	k1gFnSc1	újma
pro	pro	k7c4	pro
obec	obec	k1gFnSc4	obec
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dělostřelectvo	dělostřelectvo	k1gNnSc1	dělostřelectvo
odstřelovalo	odstřelovat	k5eAaImAgNnS	odstřelovat
pouze	pouze	k6eAd1	pouze
vepřín	vepřín	k1gInSc4	vepřín
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyletěl	vyletět	k5eAaPmAgInS	vyletět
do	do	k7c2	do
povětří	povětří	k1gNnSc2	povětří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výrazně	výrazně	k6eAd1	výrazně
hůře	zle	k6eAd2	zle
bylo	být	k5eAaImAgNnS	být
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
postiženo	postihnout	k5eAaPmNgNnS	postihnout
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
při	při	k7c6	při
zabírání	zabírání	k1gNnSc6	zabírání
dobytka	dobytek	k1gInSc2	dobytek
<g/>
,	,	kIx,	,
krmiva	krmivo	k1gNnSc2	krmivo
a	a	k8xC	a
vína	víno	k1gNnSc2	víno
pruskou	pruský	k2eAgFnSc7d1	pruská
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
neúrodou	neúroda	k1gFnSc7	neúroda
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
mírovou	mírový	k2eAgFnSc7d1	mírová
smlouvou	smlouva	k1gFnSc7	smlouva
mezi	mezi	k7c7	mezi
německou	německý	k2eAgFnSc7d1	německá
válkou	válka	k1gFnSc7	válka
1866	[number]	k4	1866
a	a	k8xC	a
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
1914-1918	[number]	k4	1914-1918
obec	obec	k1gFnSc1	obec
prospívala	prospívat	k5eAaImAgFnS	prospívat
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
starosty	starosta	k1gMnSc2	starosta
Ignaze	Ignaha	k1gFnSc3	Ignaha
Withalma	Withalma	k1gNnSc1	Withalma
v	v	k7c6	v
obecní	obecní	k2eAgFnSc6d1	obecní
radě	rada	k1gFnSc6	rada
sloučit	sloučit	k5eAaPmF	sloučit
osady	osada	k1gFnPc4	osada
Markt	Markt	k1gInSc4	Markt
<g/>
,	,	kIx,	,
Aigen	Aigen	k1gInSc4	Aigen
a	a	k8xC	a
Wieden-Gaunersdorf	Wieden-Gaunersdorf	k1gInSc4	Wieden-Gaunersdorf
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
obec	obec	k1gFnSc4	obec
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
jednoho	jeden	k4xCgInSc2	jeden
hlasu	hlas	k1gInSc2	hlas
bylo	být	k5eAaImAgNnS	být
usneseno	usnést	k5eAaPmNgNnS	usnést
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
městys	městys	k1gInSc4	městys
"	"	kIx"	"
<g/>
Gaunersdorf	Gaunersdorf	k1gMnSc1	Gaunersdorf
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
byl	být	k5eAaImAgInS	být
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
městys	městys	k1gInSc4	městys
"	"	kIx"	"
<g/>
Gaweinstal	Gaweinstal	k1gFnSc4	Gaweinstal
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Místům	místo	k1gNnPc3	místo
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
velmi	velmi	k6eAd1	velmi
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
musela	muset	k5eAaImAgFnS	muset
oplakávat	oplakávat	k5eAaImF	oplakávat
54	[number]	k4	54
padlých	padlý	k2eAgMnPc2d1	padlý
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
celé	celý	k2eAgNnSc4d1	celé
Rakousko	Rakousko	k1gNnSc4	Rakousko
<g/>
,	,	kIx,	,
trpěla	trpět	k5eAaImAgFnS	trpět
především	především	k6eAd1	především
mezi	mezi	k7c7	mezi
válkami	válka	k1gFnPc7	válka
nezaměstnaností	nezaměstnanost	k1gFnSc7	nezaměstnanost
a	a	k8xC	a
podvýživou	podvýživa	k1gFnSc7	podvýživa
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
konec	konec	k1gInSc4	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
obec	obec	k1gFnSc4	obec
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
region	region	k1gInSc4	region
velmi	velmi	k6eAd1	velmi
tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
operace	operace	k1gFnSc1	operace
–	–	k?	–
město	město	k1gNnSc1	město
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
okolí	okolí	k1gNnSc2	okolí
bránila	bránit	k5eAaImAgFnS	bránit
z	z	k7c2	z
největší	veliký	k2eAgFnSc2d3	veliký
části	část	k1gFnSc2	část
6	[number]	k4	6
<g/>
.	.	kIx.	.
tanková	tankový	k2eAgFnSc1d1	tanková
divize	divize	k1gFnSc1	divize
vojsk	vojsko	k1gNnPc2	vojsko
SS	SS	kA	SS
–	–	k?	–
dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
překročila	překročit	k5eAaPmAgFnS	překročit
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
řeku	řeka	k1gFnSc4	řeka
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
šla	jít	k5eAaImAgFnS	jít
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Sovětským	sovětský	k2eAgInSc7d1	sovětský
prvotním	prvotní	k2eAgInSc7d1	prvotní
cílem	cíl	k1gInSc7	cíl
byl	být	k5eAaImAgMnS	být
Zistersdorf	Zistersdorf	k1gMnSc1	Zistersdorf
<g/>
,	,	kIx,	,
Prottes	Prottes	k1gMnSc1	Prottes
<g/>
,	,	kIx,	,
Matzen-Raggendorf	Matzen-Raggendorf	k1gMnSc1	Matzen-Raggendorf
a	a	k8xC	a
Neusiedl	Neusiedl	k1gMnSc1	Neusiedl
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Zaya	Zaya	k1gFnSc1	Zaya
<g/>
;	;	kIx,	;
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
nacházela	nacházet	k5eAaImAgFnS	nacházet
poslední	poslední	k2eAgFnSc1d1	poslední
ještě	ještě	k6eAd1	ještě
fungující	fungující	k2eAgFnSc1d1	fungující
naftová	naftový	k2eAgFnSc1d1	naftová
pole	pole	k1gFnSc1	pole
třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
tlačil	tlačit	k5eAaImAgMnS	tlačit
čas	čas	k1gInSc4	čas
Josefa	Josef	k1gMnSc2	Josef
Stalina	Stalin	k1gMnSc2	Stalin
<g/>
,	,	kIx,	,
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
se	se	k3xPyFc4	se
vojska	vojsko	k1gNnSc2	vojsko
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
tlačila	tlačit	k5eAaImAgFnS	tlačit
od	od	k7c2	od
západu	západ	k1gInSc2	západ
na	na	k7c4	na
Československo	Československo	k1gNnSc4	Československo
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
získal	získat	k5eAaPmAgInS	získat
oblast	oblast	k1gFnSc4	oblast
svého	svůj	k3xOyFgInSc2	svůj
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postavení	postavení	k1gNnSc1	postavení
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
části	část	k1gFnPc1	část
zbraní	zbraň	k1gFnPc2	zbraň
–	–	k?	–
SS	SS	kA	SS
když	když	k8xS	když
i	i	k9	i
Wehrmacht	wehrmacht	k1gFnSc1	wehrmacht
byla	být	k5eAaImAgFnS	být
naproti	naproti	k6eAd1	naproti
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
37	[number]	k4	37
<g/>
.	.	kIx.	.
</s>
<s>
SS	SS	kA	SS
–	–	k?	–
jezdecká	jezdecký	k2eAgFnSc1d1	jezdecká
divize	divize	k1gFnSc1	divize
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
,	,	kIx,	,
96	[number]	k4	96
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc2	divize
pěchoty	pěchota	k1gFnSc2	pěchota
"	"	kIx"	"
<g/>
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
101	[number]	k4	101
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgFnSc2d1	stíhací
divize	divize	k1gFnSc2	divize
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
<g/>
,	,	kIx,	,
211	[number]	k4	211
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc2	divize
domoobrany	domoobrana	k1gFnSc2	domoobrana
granátníků	granátník	k1gMnPc2	granátník
a	a	k8xC	a
357	[number]	k4	357
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc2	divize
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
;	;	kIx,	;
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
sledu	sled	k1gInSc6	sled
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
divize	divize	k1gFnSc2	divize
granátníků	granátník	k1gMnPc2	granátník
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
tanková	tankový	k2eAgFnSc1d1	tanková
divize	divize	k1gFnSc1	divize
(	(	kIx(	(
<g/>
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
SS	SS	kA	SS
–	–	k?	–
brigády	brigáda	k1gFnSc2	brigáda
bojových	bojový	k2eAgFnPc2d1	bojová
skupin	skupina	k1gFnPc2	skupina
"	"	kIx"	"
<g/>
Trabant	trabant	k1gInSc1	trabant
1	[number]	k4	1
a	a	k8xC	a
Trabant	trabant	k1gInSc1	trabant
2	[number]	k4	2
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
přičemž	přičemž	k6eAd1	přičemž
těmto	tento	k3xDgFnPc3	tento
jednotkám	jednotka	k1gFnPc3	jednotka
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vyšších	vysoký	k2eAgFnPc2d2	vyšší
ztrát	ztráta	k1gFnPc2	ztráta
a	a	k8xC	a
závad	závada	k1gFnPc2	závada
zbyl	zbýt	k5eAaPmAgMnS	zbýt
ještě	ještě	k6eAd1	ještě
nějaký	nějaký	k3yIgInSc4	nějaký
zlomek	zlomek	k1gInSc4	zlomek
jejich	jejich	k3xOp3gFnPc2	jejich
povinností	povinnost	k1gFnPc2	povinnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zrovna	zrovna	k6eAd1	zrovna
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
padl	padnout	k5eAaPmAgMnS	padnout
Gänserndorf	Gänserndorf	k1gMnSc1	Gänserndorf
a	a	k8xC	a
"	"	kIx"	"
<g/>
Strasshof	Strasshof	k1gInSc1	Strasshof
<g/>
"	"	kIx"	"
nad	nad	k7c7	nad
nádražím	nádraží	k1gNnSc7	nádraží
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Sovětů	Sovět	k1gMnPc2	Sovět
a	a	k8xC	a
zatlačili	zatlačit	k5eAaPmAgMnP	zatlačit
německé	německý	k2eAgNnSc4d1	německé
vojsko	vojsko	k1gNnSc4	vojsko
na	na	k7c4	na
Prottes	Prottes	k1gInSc4	Prottes
<g/>
,	,	kIx,	,
Groß-Schweinbarth	Groß-Schweinbarth	k1gInSc4	Groß-Schweinbarth
a	a	k8xC	a
Hohenruppersdorf	Hohenruppersdorf	k1gInSc4	Hohenruppersdorf
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
byli	být	k5eAaImAgMnP	být
Rusové	Rus	k1gMnPc1	Rus
podporováni	podporovat	k5eAaImNgMnP	podporovat
leteckými	letecký	k2eAgMnPc7d1	letecký
gardisty	gardista	k1gMnPc7	gardista
a	a	k8xC	a
tanky	tank	k1gInPc1	tank
před	před	k7c7	před
Gaweinstalem	Gaweinstal	k1gMnSc7	Gaweinstal
a	a	k8xC	a
blokují	blokovat	k5eAaImIp3nP	blokovat
říšskou	říšský	k2eAgFnSc4d1	říšská
silnici	silnice	k1gFnSc4	silnice
Vídeň	Vídeň	k1gFnSc1	Vídeň
–	–	k?	–
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Příštího	příští	k2eAgInSc2d1	příští
dne	den	k1gInSc2	den
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
Gaweinstalu	Gaweinstal	k1gMnSc3	Gaweinstal
proveden	provést	k5eAaPmNgInS	provést
protiútok	protiútok	k1gInSc1	protiútok
bojovým	bojový	k2eAgNnSc7d1	bojové
seskupením	seskupení	k1gNnSc7	seskupení
"	"	kIx"	"
<g/>
Witte	Witt	k1gInSc5	Witt
<g/>
"	"	kIx"	"
s	s	k7c7	s
částmi	část	k1gFnPc7	část
tankového	tankový	k2eAgInSc2d1	tankový
pluku	pluk	k1gInSc2	pluk
146	[number]	k4	146
<g/>
.	.	kIx.	.
a	a	k8xC	a
tankoborníky	tankoborník	k1gMnPc7	tankoborník
útvaru	útvar	k1gInSc2	útvar
87	[number]	k4	87
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
tankové	tankový	k2eAgFnSc2d1	tanková
divize	divize	k1gFnSc2	divize
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rudoarmějci	rudoarmějec	k1gMnSc3	rudoarmějec
prolomeno	prolomen	k2eAgNnSc1d1	prolomeno
místo	místo	k1gNnSc1	místo
a	a	k8xC	a
uzavřeno	uzavřen	k2eAgNnSc1d1	uzavřeno
i	i	k8xC	i
s	s	k7c7	s
děly	dělo	k1gNnPc7	dělo
a	a	k8xC	a
zatlačeno	zatlačit	k5eAaPmNgNnS	zatlačit
na	na	k7c4	na
Martinsdorf	Martinsdorf	k1gInSc4	Martinsdorf
a	a	k8xC	a
Hohenruppersdorf	Hohenruppersdorf	k1gInSc4	Hohenruppersdorf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
protitahu	protitah	k1gInSc6	protitah
Sověti	Sovět	k1gMnPc1	Sovět
dobyli	dobýt	k5eAaPmAgMnP	dobýt
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
stejného	stejný	k2eAgInSc2d1	stejný
dne	den	k1gInSc2	den
Wolkersdorf	Wolkersdorf	k1gInSc1	Wolkersdorf
<g/>
,	,	kIx,	,
když	když	k8xS	když
tam	tam	k6eAd1	tam
bylo	být	k5eAaImAgNnS	být
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Němců	Němec	k1gMnPc2	Němec
všechno	všechen	k3xTgNnSc1	všechen
opuštěno	opuštěn	k2eAgNnSc1d1	opuštěno
a	a	k8xC	a
v	v	k7c6	v
průniku	průnik	k1gInSc6	průnik
na	na	k7c6	na
Mistelbach	Mistelba	k1gFnPc6	Mistelba
zabráněno	zabráněn	k2eAgNnSc1d1	zabráněno
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
byla	být	k5eAaImAgNnP	být
místa	místo	k1gNnPc1	místo
Hohenruppersdorf	Hohenruppersdorf	k1gMnSc1	Hohenruppersdorf
<g/>
,	,	kIx,	,
Schrick	Schrick	k1gMnSc1	Schrick
a	a	k8xC	a
Obersulz	Obersulz	k1gMnSc1	Obersulz
těžce	těžce	k6eAd1	těžce
dobyta	dobyt	k2eAgFnSc1d1	dobyta
<g/>
.	.	kIx.	.
</s>
<s>
Poněvadž	poněvadž	k8xS	poněvadž
Schrick	Schrick	k1gMnSc1	Schrick
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
a	a	k8xC	a
pro	pro	k7c4	pro
obranu	obrana	k1gFnSc4	obrana
je	být	k5eAaImIp3nS	být
nevýhodný	výhodný	k2eNgInSc1d1	nevýhodný
<g/>
,	,	kIx,	,
opevnila	opevnit	k5eAaPmAgFnS	opevnit
se	s	k7c7	s
25	[number]	k4	25
<g/>
.	.	kIx.	.
tanková	tankový	k2eAgFnSc1d1	tanková
divize	divize	k1gFnSc1	divize
na	na	k7c4	na
Schricker	Schricker	k1gInSc4	Schricker
Bergu	Berg	k1gInSc2	Berg
mezi	mezi	k7c7	mezi
Schrickem	Schrick	k1gInSc7	Schrick
a	a	k8xC	a
Gaweinstalem	Gaweinstal	k1gMnSc7	Gaweinstal
a	a	k8xC	a
také	také	k9	také
přes	přes	k7c4	přes
císařskou	císařský	k2eAgFnSc4d1	císařská
silnici	silnice	k1gFnSc4	silnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gaweinstal	Gaweinstat	k5eAaImAgMnS	Gaweinstat
byl	být	k5eAaImAgInS	být
101	[number]	k4	101
<g/>
.	.	kIx.	.
stíhacím	stíhací	k2eAgInSc7d1	stíhací
oddílem	oddíl	k1gInSc7	oddíl
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
generálporučíka	generálporučík	k1gMnSc2	generálporučík
Walter	Walter	k1gMnSc1	Walter
Assmanna	Assmann	k1gMnSc2	Assmann
ubráněn	ubráněn	k2eAgMnSc1d1	ubráněn
<g/>
,	,	kIx,	,
Neubau	Neubaa	k1gFnSc4	Neubaa
(	(	kIx(	(
<g/>
Gemeinde	Gemeind	k1gMnSc5	Gemeind
Ladendorf	Ladendorf	k1gInSc4	Ladendorf
<g/>
)	)	kIx)	)
mělo	mít	k5eAaImAgNnS	mít
celou	celý	k2eAgFnSc4d1	celá
linii	linie	k1gFnSc4	linie
Wolfpassing	Wolfpassing	k1gInSc1	Wolfpassing
–	–	k?	–
Bogenneusiedl	Bogenneusiedl	k1gFnSc2	Bogenneusiedl
–	–	k?	–
Gaweinstal	Gaweinstal	k1gMnSc1	Gaweinstal
-	-	kIx~	-
Pellendorf	Pellendorf	k1gMnSc1	Pellendorf
–	–	k?	–
Atzelsdorf	Atzelsdorf	k1gMnSc1	Atzelsdorf
–	–	k?	–
a	a	k8xC	a
Höbersbrunn	Höbersbrunn	k1gInSc1	Höbersbrunn
ležení	ležení	k1gNnSc2	ležení
u	u	k7c2	u
Schrickerského	Schrickerský	k2eAgInSc2d1	Schrickerský
kopce	kopec	k1gInSc2	kopec
25	[number]	k4	25
<g/>
.	.	kIx.	.
tankovou	tankový	k2eAgFnSc4d1	tanková
divizi	divize	k1gFnSc4	divize
neuzavřeli	uzavřít	k5eNaPmAgMnP	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
se	se	k3xPyFc4	se
blížily	blížit	k5eAaImAgFnP	blížit
od	od	k7c2	od
Bad	Bad	k1gFnSc2	Bad
Pirawarth	Pirawartha	k1gFnPc2	Pirawartha
těžké	těžký	k2eAgFnSc2d1	těžká
mohutné	mohutný	k2eAgFnSc2d1	mohutná
síly	síla	k1gFnSc2	síla
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
na	na	k7c6	na
Gaweinstal	Gaweinstal	k1gFnSc6	Gaweinstal
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
protiútoku	protiútok	k1gInSc3	protiútok
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
a	a	k8xC	a
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
hodinou	hodina	k1gFnSc7	hodina
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
ruským	ruský	k2eAgMnPc3d1	ruský
vojákům	voják	k1gMnPc3	voják
od	od	k7c2	od
východu	východ	k1gInSc2	východ
sem	sem	k6eAd1	sem
přes	přes	k7c4	přes
železniční	železniční	k2eAgFnSc4d1	železniční
trať	trať	k1gFnSc4	trať
<g/>
,	,	kIx,	,
a	a	k8xC	a
Gaweinstal	Gaweinstal	k1gFnSc1	Gaweinstal
po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
boji	boj	k1gInSc6	boj
byl	být	k5eAaImAgInS	být
dobyt	dobýt	k5eAaPmNgInS	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Kronikář	kronikář	k1gMnSc1	kronikář
bojové	bojový	k2eAgFnSc2d1	bojová
roty	rota	k1gFnSc2	rota
situaci	situace	k1gFnSc4	situace
zde	zde	k6eAd1	zde
zachytil	zachytit	k5eAaPmAgMnS	zachytit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dopoledne	dopoledne	k6eAd1	dopoledne
jde	jít	k5eAaImIp3nS	jít
nalevo	nalevo	k6eAd1	nalevo
od	od	k7c2	od
nás	my	k3xPp1nPc2	my
všechno	všechen	k3xTgNnSc1	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Odvoláváme	odvolávat	k5eAaImIp1nP	odvolávat
se	se	k3xPyFc4	se
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
Rusové	Rus	k1gMnPc1	Rus
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
štěstí	štěstit	k5eAaImIp3nS	štěstit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Brání	bránit	k5eAaImIp3nS	bránit
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
Kellergasse	Kellergass	k1gMnPc4	Kellergass
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgMnPc4d1	dnešní
Scheicherstraße	Scheicherstraß	k1gMnPc4	Scheicherstraß
<g/>
)	)	kIx)	)
do	do	k7c2	do
Bogenneusiedl	Bogenneusiedl	k1gFnSc2	Bogenneusiedl
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
"	"	kIx"	"
<g/>
Pellendorf	Pellendorf	k1gInSc1	Pellendorf
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Atzelsdorf	Atzelsdorf	k1gInSc1	Atzelsdorf
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Velící	velící	k2eAgMnSc1d1	velící
důstojník	důstojník	k1gMnSc1	důstojník
Heinz	Heinz	k1gMnSc1	Heinz
od	od	k7c2	od
85	[number]	k4	85
<g/>
.	.	kIx.	.
pluku	pluk	k1gInSc2	pluk
podává	podávat	k5eAaImIp3nS	podávat
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
byla	být	k5eAaImAgFnS	být
jednotka	jednotka	k1gFnSc1	jednotka
rozmístěna	rozmístit	k5eAaPmNgFnS	rozmístit
u	u	k7c2	u
Bad	Bad	k1gFnPc2	Bad
Pirwarth	Pirwartha	k1gFnPc2	Pirwartha
<g/>
,	,	kIx,	,
Kollnbrunn	Kollnbrunna	k1gFnPc2	Kollnbrunna
a	a	k8xC	a
Gaweinstalu	Gaweinstal	k1gMnSc6	Gaweinstal
<g/>
.	.	kIx.	.
</s>
<s>
Odtamtud	odtamtud	k6eAd1	odtamtud
jsme	být	k5eAaImIp1nP	být
museli	muset	k5eAaImAgMnP	muset
na	na	k7c6	na
vyšší	vysoký	k2eAgFnSc6d2	vyšší
západní	západní	k2eAgFnSc6d1	západní
Gaweinstal	Gaweinstal	k1gFnSc6	Gaweinstal
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
stáhnout	stáhnout	k5eAaPmF	stáhnout
do	do	k7c2	do
Atzelsdorfu	Atzelsdorf	k1gInSc2	Atzelsdorf
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
ruský	ruský	k2eAgMnSc1d1	ruský
pilot	pilot	k1gMnSc1	pilot
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nás	my	k3xPp1nPc4	my
ostřeloval	ostřelovat	k5eAaImAgInS	ostřelovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přerušování	přerušování	k1gNnSc6	přerušování
pohybu	pohyb	k1gInSc2	pohyb
z	z	k7c2	z
Gaweinstalu	Gaweinstal	k1gMnSc3	Gaweinstal
bylo	být	k5eAaImAgNnS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
jít	jít	k5eAaImF	jít
vlevo	vlevo	k6eAd1	vlevo
i	i	k8xC	i
vpravo	vpravo	k6eAd1	vpravo
po	po	k7c6	po
štěrkové	štěrkový	k2eAgFnSc6d1	štěrková
vozovce	vozovka	k1gFnSc6	vozovka
ve	v	k7c6	v
skrytu	skryt	k1gInSc6	skryt
pod	pod	k7c7	pod
stromy	strom	k1gInPc7	strom
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nádraží	nádraží	k1gNnSc4	nádraží
Höbersbrunn-Atzelsdorf	Höbersbrunn-Atzelsdorf	k1gInSc4	Höbersbrunn-Atzelsdorf
opatrně	opatrně	k6eAd1	opatrně
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
před	před	k7c7	před
Rusem	Rus	k1gMnSc7	Rus
ukryli	ukrýt	k5eAaPmAgMnP	ukrýt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nádraží	nádraží	k1gNnSc6	nádraží
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
nezdržovali	zdržovat	k5eNaImAgMnP	zdržovat
a	a	k8xC	a
šli	jít	k5eAaImAgMnP	jít
mimo	mimo	k7c4	mimo
železniční	železniční	k2eAgNnSc4d1	železniční
těleso	těleso	k1gNnSc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hodin	hodina	k1gFnPc2	hodina
jsem	být	k5eAaImIp1nS	být
usuzoval	usuzovat	k5eAaImAgMnS	usuzovat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
musíme	muset	k5eAaImIp1nP	muset
být	být	k5eAaImF	být
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
jsem	být	k5eAaImIp1nS	být
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nepřijde	přijít	k5eNaPmIp3nS	přijít
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
přijde	přijít	k5eAaPmIp3nS	přijít
teď	teď	k6eAd1	teď
Rus	Rus	k1gFnSc1	Rus
<g/>
""	""	k?	""
<g/>
Případ	případ	k1gInSc1	případ
Gaweinstalu	Gaweinstal	k1gMnSc3	Gaweinstal
měl	mít	k5eAaImAgInS	mít
pro	pro	k7c4	pro
25	[number]	k4	25
<g/>
.	.	kIx.	.
tankovou	tankový	k2eAgFnSc4d1	tanková
divizi	divize	k1gFnSc4	divize
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ještě	ještě	k9	ještě
stále	stále	k6eAd1	stále
držela	držet	k5eAaImAgFnS	držet
"	"	kIx"	"
<g/>
Schricker	Schricker	k1gMnSc1	Schricker
Berg	Berg	k1gMnSc1	Berg
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nepříjemné	příjemný	k2eNgNnSc1d1	nepříjemné
pokračování	pokračování	k1gNnSc1	pokračování
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
měla	mít	k5eAaImAgFnS	mít
otevřený	otevřený	k2eAgInSc4d1	otevřený
svůj	svůj	k3xOyFgInSc4	svůj
pravý	pravý	k2eAgInSc4d1	pravý
bok	bok	k1gInSc4	bok
<g/>
.	.	kIx.	.
101	[number]	k4	101
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgFnSc1d1	stíhací
divize	divize	k1gFnSc1	divize
dostala	dostat	k5eAaPmAgFnS	dostat
příkaz	příkaz	k1gInSc4	příkaz
k	k	k7c3	k
protiútoku	protiútok	k1gInSc3	protiútok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vedl	vést	k5eAaImAgInS	vést
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
tanků	tank	k1gInPc2	tank
od	od	k7c2	od
Atzelsdorfu	Atzelsdorf	k1gInSc2	Atzelsdorf
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
domů	dům	k1gInPc2	dům
zničeno	zničit	k5eAaPmNgNnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Velitel	velitel	k1gMnSc1	velitel
roty	rota	k1gFnSc2	rota
(	(	kIx(	(
<g/>
nadporučík	nadporučík	k1gMnSc1	nadporučík
<g/>
)	)	kIx)	)
Steubing	Steubing	k1gInSc1	Steubing
zapsal	zapsat	k5eAaPmAgInS	zapsat
do	do	k7c2	do
jeho	on	k3xPp3gInSc2	on
deníku	deník	k1gInSc2	deník
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nahoře	nahoře	k6eAd1	nahoře
na	na	k7c6	na
výšině	výšina	k1gFnSc6	výšina
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
velkými	velký	k2eAgInPc7d1	velký
stromy	strom	k1gInPc7	strom
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vinný	vinný	k2eAgInSc1d1	vinný
sklep	sklep	k1gInSc1	sklep
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Steubing	Steubing	k1gInSc1	Steubing
je	být	k5eAaImIp3nS	být
zamyšlen	zamyslet	k5eAaPmNgInS	zamyslet
tu	tu	k6eAd1	tu
na	na	k7c4	na
Scheicherstraße	Scheicherstraße	k1gInSc4	Scheicherstraße
do	do	k7c2	do
Bogenneusiedlu	Bogenneusiedlo	k1gNnSc3	Bogenneusiedlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nalézá	nalézat	k5eAaImIp3nS	nalézat
sportovní	sportovní	k2eAgNnSc4d1	sportovní
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Steubing	Steubing	k1gInSc1	Steubing
dále	daleko	k6eAd2	daleko
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
tanku	tank	k1gInSc2	tank
vyšly	vyjít	k5eAaPmAgFnP	vyjít
všechny	všechen	k3xTgFnPc1	všechen
ženy	žena	k1gFnPc1	žena
ze	z	k7c2	z
sklepů	sklep	k1gInPc2	sklep
a	a	k8xC	a
říkaly	říkat	k5eAaImAgFnP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gFnPc4	on
Rusové	Rusová	k1gFnPc4	Rusová
budou	být	k5eAaImBp3nP	být
znásilňovat	znásilňovat	k5eAaImF	znásilňovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
zprávy	zpráva	k1gFnPc1	zpráva
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
z	z	k7c2	z
Pürstendorfu	Pürstendorf	k1gInSc2	Pürstendorf
<g/>
,	,	kIx,	,
Niederleisu	Niederleis	k1gInSc2	Niederleis
<g/>
,	,	kIx,	,
Helfensu	Helfens	k1gInSc2	Helfens
<g/>
,	,	kIx,	,
Schrick	Schricka	k1gFnPc2	Schricka
<g/>
,	,	kIx,	,
<g/>
u	u	k7c2	u
Niederkreuzstettenu	Niederkreuzstetten	k1gInSc2	Niederkreuzstetten
<g/>
,	,	kIx,	,
Poysdorfu	Poysdorf	k1gInSc2	Poysdorf
<g/>
,	,	kIx,	,
Kleinhadersdorfu	Kleinhadersdorf	k1gInSc2	Kleinhadersdorf
<g/>
,	,	kIx,	,
Hörersdorf	Hörersdorf	k1gInSc1	Hörersdorf
...	...	k?	...
<g/>
Prakticky	prakticky	k6eAd1	prakticky
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
očitých	očitý	k2eAgMnPc2d1	očitý
svědků	svědek	k1gMnPc2	svědek
při	při	k7c6	při
opětovném	opětovný	k2eAgNnSc6d1	opětovné
dobytí	dobytí	k1gNnSc6	dobytí
Gaweinstalu	Gaweinstal	k1gMnSc3	Gaweinstal
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
mnoho	mnoho	k6eAd1	mnoho
sovětských	sovětský	k2eAgMnPc2d1	sovětský
vojáků	voják	k1gMnPc2	voják
padlo	padnout	k5eAaImAgNnS	padnout
<g/>
.	.	kIx.	.
</s>
<s>
Steubingova	Steubingův	k2eAgFnSc1d1	Steubingův
jednotka	jednotka	k1gFnSc1	jednotka
mohla	moct	k5eAaImAgFnS	moct
proniknout	proniknout	k5eAaPmF	proniknout
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
obce	obec	k1gFnSc2	obec
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnPc4d1	severozápadní
části	část	k1gFnPc4	část
Gaweinstalu	Gaweinstal	k1gMnSc3	Gaweinstal
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
na	na	k7c4	na
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
obrana	obrana	k1gFnSc1	obrana
"	"	kIx"	"
<g/>
Schricker	Schricker	k1gInSc1	Schricker
Bergu	Berg	k1gInSc2	Berg
<g/>
"	"	kIx"	"
zakrátko	zakrátko	k6eAd1	zakrátko
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
vyklizené	vyklizený	k2eAgFnSc2d1	vyklizená
obce	obec	k1gFnSc2	obec
Pellendorf	Pellendorf	k1gMnSc1	Pellendorf
<g/>
,	,	kIx,	,
Atzelsdorfund	Atzelsdorfund	k1gMnSc1	Atzelsdorfund
Höbersbrunn	Höbersbrunn	k1gMnSc1	Höbersbrunn
<g/>
.	.	kIx.	.
</s>
<s>
Odpoután	odpoután	k2eAgMnSc1d1	odpoután
od	od	k7c2	od
nepřítele	nepřítel	k1gMnSc2	nepřítel
z	z	k7c2	z
Höbersbrunnu	Höbersbrunn	k1gInSc2	Höbersbrunn
mohl	moct	k5eAaImAgMnS	moct
velitel	velitel	k1gMnSc1	velitel
Heinz	Heinz	k1gInSc4	Heinz
pozorovat	pozorovat	k5eAaImF	pozorovat
Rusy	Rus	k1gMnPc4	Rus
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
šel	jít	k5eAaImAgMnS	jít
z	z	k7c2	z
Gaweinstalu	Gaweinstal	k1gMnSc3	Gaweinstal
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
Höbersbrunnu	Höbersbrunn	k1gInSc2	Höbersbrunn
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
se	se	k3xPyFc4	se
přibližují	přibližovat	k5eAaImIp3nP	přibližovat
také	také	k9	také
již	již	k9	již
první	první	k4xOgMnPc1	první
Sověti	Sovět	k1gMnPc1	Sovět
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Gaweinstalu	Gaweinstal	k1gMnSc3	Gaweinstal
přicházející	přicházející	k2eAgFnSc2d1	přicházející
Rusové	Rusová	k1gFnSc2	Rusová
<g/>
,	,	kIx,	,
odbočili	odbočit	k5eAaPmAgMnP	odbočit
u	u	k7c2	u
nádraží	nádraží	k1gNnSc2	nádraží
napravo	napravo	k6eAd1	napravo
a	a	k8xC	a
šli	jít	k5eAaImAgMnP	jít
do	do	k7c2	do
Höbersbrunnu	Höbersbrunn	k1gInSc2	Höbersbrunn
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
osadou	osada	k1gFnSc7	osada
odbočili	odbočit	k5eAaPmAgMnP	odbočit
vlevo	vlevo	k6eAd1	vlevo
<g/>
,	,	kIx,	,
zdolali	zdolat	k5eAaPmAgMnP	zdolat
malé	malý	k2eAgNnSc4d1	malé
převýšení	převýšení	k1gNnSc4	převýšení
(	(	kIx(	(
<g/>
v	v	k7c4	v
Greuten	Greuten	k2eAgInSc4d1	Greuten
<g/>
)	)	kIx)	)
a	a	k8xC	a
táhli	táhnout	k5eAaImAgMnP	táhnout
potom	potom	k6eAd1	potom
200	[number]	k4	200
až	až	k9	až
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
před	před	k7c7	před
námi	my	k3xPp1nPc7	my
přes	přes	k7c4	přes
kotlinu	kotlina	k1gFnSc4	kotlina
severozápadním	severozápadní	k2eAgInSc7d1	severozápadní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
železniční	železniční	k2eAgFnSc4d1	železniční
trať	trať	k1gFnSc4	trať
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgInSc1d1	silný
"	"	kIx"	"
<g/>
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
"	"	kIx"	"
vedl	vést	k5eAaImAgInS	vést
zhruba	zhruba	k6eAd1	zhruba
1100	[number]	k4	1100
mužů	muž	k1gMnPc2	muž
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
oni	onen	k3xDgMnPc1	onen
vezli	vézt	k5eAaImAgMnP	vézt
mimo	mimo	k6eAd1	mimo
pěchotních	pěchotní	k2eAgFnPc2d1	pěchotní
zbraní	zbraň	k1gFnPc2	zbraň
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
střeliva	střelivo	k1gNnSc2	střelivo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
odpoledni	odpoledne	k1gNnSc6	odpoledne
objevil	objevit	k5eAaPmAgMnS	objevit
další	další	k2eAgFnSc2d1	další
lidové	lidový	k2eAgFnSc2d1	lidová
milice	milice	k1gFnSc2	milice
–	–	k?	–
požívali	požívat	k5eAaImAgMnP	požívat
zřejmě	zřejmě	k6eAd1	zřejmě
také	také	k9	také
hojně	hojně	k6eAd1	hojně
víno	víno	k1gNnSc4	víno
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
motali	motat	k5eAaImAgMnP	motat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
byli	být	k5eAaImAgMnP	být
totálně	totálně	k6eAd1	totálně
opilí	opilý	k2eAgMnPc1d1	opilý
<g/>
.	.	kIx.	.
</s>
<s>
Přinášeli	přinášet	k5eAaImAgMnP	přinášet
kulomety	kulomet	k1gInPc4	kulomet
a	a	k8xC	a
děla	dělo	k1gNnPc4	dělo
do	do	k7c2	do
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c4	na
nás	my	k3xPp1nPc4	my
namířili	namířit	k5eAaPmAgMnP	namířit
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
jsme	být	k5eAaImIp1nP	být
seděli	sedět	k5eAaImAgMnP	sedět
v	v	k7c6	v
úkrytu	úkryt	k1gInSc6	úkryt
a	a	k8xC	a
tísnili	tísnit	k5eAaImAgMnP	tísnit
se	se	k3xPyFc4	se
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
oni	onen	k3xDgMnPc1	onen
uspořádávali	uspořádávat	k5eAaImAgMnP	uspořádávat
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
usazovali	usazovat	k5eAaImAgMnP	usazovat
je	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Přece	přece	k9	přece
velitel	velitel	k1gMnSc1	velitel
zakázal	zakázat	k5eAaPmAgMnS	zakázat
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
vyšli	vyjít	k5eAaPmAgMnP	vyjít
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
když	když	k8xS	když
pozice	pozice	k1gFnPc1	pozice
ještě	ještě	k6eAd1	ještě
držíme	držet	k5eAaImIp1nP	držet
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
jsme	být	k5eAaImIp1nP	být
museli	muset	k5eAaImAgMnP	muset
v	v	k7c6	v
ohni	oheň	k1gInSc6	oheň
kulek	kulka	k1gFnPc2	kulka
bez	bez	k7c2	bez
krytí	krytí	k1gNnSc2	krytí
se	s	k7c7	s
vším	všecek	k3xTgNnSc7	všecek
nářadím	nářadí	k1gNnSc7	nářadí
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
<s>
Dým	dým	k1gInSc1	dým
z	z	k7c2	z
prachu	prach	k1gInSc2	prach
detonace	detonace	k1gFnSc2	detonace
granátu	granát	k1gInSc6	granát
mi	já	k3xPp1nSc3	já
dělal	dělat	k5eAaImAgMnS	dělat
potíže	potíž	k1gFnPc4	potíž
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
skoro	skoro	k6eAd1	skoro
nemohl	moct	k5eNaImAgMnS	moct
dýchat	dýchat	k5eAaImF	dýchat
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nás	my	k3xPp1nPc2	my
dostal	dostat	k5eAaPmAgMnS	dostat
trhavou	trhavý	k2eAgFnSc4d1	trhavá
střelu	střela	k1gFnSc4	střela
do	do	k7c2	do
paže	paže	k1gFnSc2	paže
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
pro	pro	k7c4	pro
zásah	zásah	k1gInSc4	zásah
šíje	šíj	k1gFnSc2	šíj
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
odnesen	odnesen	k2eAgMnSc1d1	odnesen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Těmito	tento	k3xDgFnPc7	tento
událostmi	událost	k1gFnPc7	událost
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
ke	k	k7c3	k
zvratu	zvrat	k1gInSc3	zvrat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dobyt	dobyt	k2eAgInSc1d1	dobyt
Gaweinstal	Gaweinstal	k1gMnPc7	Gaweinstal
a	a	k8xC	a
okolní	okolní	k2eAgNnPc1d1	okolní
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
den	den	k1gInSc1	den
nato	nato	k6eAd1	nato
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
okresní	okresní	k2eAgNnSc1d1	okresní
město	město	k1gNnSc1	město
Mistelbach	Mistelbacha	k1gFnPc2	Mistelbacha
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
Poysdorf	Poysdorf	k1gInSc1	Poysdorf
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
skoro	skoro	k6eAd1	skoro
celý	celý	k2eAgInSc1d1	celý
region	region	k1gInSc1	region
obsazen	obsadit	k5eAaPmNgInS	obsadit
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
bezpodmínečné	bezpodmínečný	k2eAgFnSc6d1	bezpodmínečná
kapitulaci	kapitulace	k1gFnSc6	kapitulace
německé	německý	k2eAgFnSc2d1	německá
branné	branný	k2eAgFnSc2d1	Branná
moci	moc	k1gFnSc2	moc
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
uvolnilo	uvolnit	k5eAaPmAgNnS	uvolnit
se	se	k3xPyFc4	se
odvetné	odvetný	k2eAgNnSc1d1	odvetné
opatření	opatření	k1gNnSc1	opatření
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poválečná	poválečný	k2eAgFnSc1d1	poválečná
historie	historie	k1gFnSc1	historie
===	===	k?	===
</s>
</p>
<p>
<s>
Poválečná	poválečný	k2eAgFnSc1d1	poválečná
doba	doba	k1gFnSc1	doba
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
Gaweinstal	Gaweinstal	k1gFnSc4	Gaweinstal
hospodářským	hospodářský	k2eAgInSc7d1	hospodářský
rozmachem	rozmach	k1gInSc7	rozmach
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
ovlivněná	ovlivněný	k2eAgNnPc4d1	ovlivněné
čilým	čilý	k2eAgInSc7d1	čilý
stavebním	stavební	k2eAgInSc7d1	stavební
ruchem	ruch	k1gInSc7	ruch
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současném	současný	k2eAgInSc6d1	současný
vzhledu	vzhled	k1gInSc6	vzhled
krajinářsky	krajinářsky	k6eAd1	krajinářsky
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
území	území	k1gNnSc1	území
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
již	již	k6eAd1	již
uvedené	uvedený	k2eAgInPc1d1	uvedený
"	"	kIx"	"
<g/>
Ganslwiesn	Ganslwiesn	k1gInSc1	Ganslwiesn
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
umělecky	umělecky	k6eAd1	umělecky
cenné	cenný	k2eAgFnPc4d1	cenná
stavby	stavba	k1gFnPc4	stavba
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
asi	asi	k9	asi
v	v	k7c6	v
době	doba	k1gFnSc6	doba
baroka	baroko	k1gNnSc2	baroko
zářily	zářit	k5eAaImAgFnP	zářit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
takových	takový	k3xDgFnPc2	takový
budov	budova	k1gFnPc2	budova
byl	být	k5eAaImAgInS	být
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1958	[number]	k4	1958
a	a	k8xC	a
1960	[number]	k4	1960
postaven	postaven	k2eAgInSc4d1	postaven
obecní	obecní	k2eAgInSc4d1	obecní
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
opravené	opravený	k2eAgFnSc2d1	opravená
budovy	budova	k1gFnSc2	budova
dřívější	dřívější	k2eAgFnSc2d1	dřívější
obecné	obecný	k2eAgFnSc2d1	obecná
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
obilní	obilní	k2eAgNnSc1d1	obilní
silo	silo	k1gNnSc1	silo
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
skladovací	skladovací	k2eAgFnSc2d1	skladovací
společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
Gaweinstal	Gaweinstal	k1gFnSc4	Gaweinstal
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
byla	být	k5eAaImAgFnS	být
brněnská	brněnský	k2eAgFnSc1d1	brněnská
silnice	silnice	k1gFnSc1	silnice
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
a	a	k8xC	a
napřímena	napřímit	k5eAaPmNgFnS	napřímit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
několik	několik	k4yIc1	několik
domů	dům	k1gInPc2	dům
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
zbouráno	zbourat	k5eAaPmNgNnS	zbourat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
hlavní	hlavní	k2eAgFnSc2d1	hlavní
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
obec	obec	k1gFnSc1	obec
Gaweinstal	Gaweinstal	k1gFnSc2	Gaweinstal
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
sloučením	sloučení	k1gNnSc7	sloučení
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
Atzelsdorf	Atzelsdorf	k1gMnSc1	Atzelsdorf
<g/>
,	,	kIx,	,
Gaweinstal	Gaweinstal	k1gMnSc1	Gaweinstal
<g/>
,	,	kIx,	,
Höbersbrunn	Höbersbrunn	k1gMnSc1	Höbersbrunn
<g/>
,	,	kIx,	,
Martinsdorf	Martinsdorf	k1gMnSc1	Martinsdorf
<g/>
,	,	kIx,	,
Pellendorf	Pellendorf	k1gMnSc1	Pellendorf
a	a	k8xC	a
Schrick	Schrick	k1gMnSc1	Schrick
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
čerpají	čerpat	k5eAaImIp3nP	čerpat
prostředky	prostředek	k1gInPc7	prostředek
a	a	k8xC	a
investuje	investovat	k5eAaBmIp3nS	investovat
se	se	k3xPyFc4	se
do	do	k7c2	do
bytové	bytový	k2eAgFnSc2d1	bytová
výstavby	výstavba	k1gFnSc2	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
spolek	spolek	k1gInSc1	spolek
Dobrovolných	Dobrovolná	k1gFnPc2	Dobrovolná
hasičů	hasič	k1gMnPc2	hasič
Gaweinstal	Gaweinstal	k1gMnPc2	Gaweinstal
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
dostal	dostat	k5eAaPmAgInS	dostat
nový	nový	k2eAgInSc1d1	nový
Hasičský	hasičský	k2eAgInSc1d1	hasičský
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
otevřel	otevřít	k5eAaPmAgMnS	otevřít
zemský	zemský	k2eAgMnSc1d1	zemský
hejtman	hejtman	k1gMnSc1	hejtman
Erwin	Erwin	k1gMnSc1	Erwin
Pröll	Pröll	k1gMnSc1	Pröll
novou	nový	k2eAgFnSc4d1	nová
obecnou	obecný	k2eAgFnSc4d1	obecná
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
podnětu	podnět	k1gInSc2	podnět
byla	být	k5eAaImAgFnS	být
také	také	k6eAd1	také
obci	obec	k1gFnSc3	obec
předána	předán	k2eAgFnSc1d1	předána
listina	listina	k1gFnSc1	listina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
propůjčuje	propůjčovat	k5eAaImIp3nS	propůjčovat
užívání	užívání	k1gNnSc3	užívání
obecního	obecní	k2eAgInSc2d1	obecní
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Gaweinstalský	Gaweinstalský	k2eAgInSc1d1	Gaweinstalský
obecní	obecní	k2eAgInSc1d1	obecní
znak	znak	k1gInSc1	znak
<g/>
:	:	kIx,	:
na	na	k7c6	na
zlatém	zlatý	k2eAgNnSc6d1	Zlaté
poli	pole	k1gNnSc6	pole
je	být	k5eAaImIp3nS	být
položen	položen	k2eAgInSc1d1	položen
zelený	zelený	k2eAgInSc1d1	zelený
trojlist	trojlist	k1gInSc1	trojlist
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
červeno-bílo-červený	červenoílo-červený	k2eAgInSc4d1	červeno-bílo-červený
štít	štít	k1gInSc4	štít
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
štítu	štít	k1gInSc2	štít
vyplynuly	vyplynout	k5eAaPmAgFnP	vyplynout
barvy	barva	k1gFnPc1	barva
praporu	prapor	k1gInSc2	prapor
městyse	městys	k1gInSc2	městys
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
je	být	k5eAaImIp3nS	být
žluto-zelená	žlutoelený	k2eAgFnSc1d1	žluto-zelená
<g/>
.	.	kIx.	.
</s>
<s>
Zapůjčení	zapůjčení	k1gNnSc1	zapůjčení
znaku	znak	k1gInSc2	znak
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
po	po	k7c4	po
uznání	uznání	k1gNnSc4	uznání
historického	historický	k2eAgInSc2d1	historický
významu	význam	k1gInSc2	význam
městyse	městys	k1gInSc2	městys
a	a	k8xC	a
ocenění	ocenění	k1gNnSc2	ocenění
za	za	k7c4	za
stálé	stálý	k2eAgNnSc4d1	stálé
zdokonalování	zdokonalování	k1gNnSc4	zdokonalování
obecních	obecní	k2eAgNnPc2d1	obecní
zařízení	zařízení	k1gNnPc2	zařízení
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Masivní	masivní	k2eAgInSc1d1	masivní
vzestup	vzestup	k1gInSc1	vzestup
kvality	kvalita	k1gFnSc2	kvalita
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
v	v	k7c6	v
městysu	městys	k1gInSc6	městys
od	od	k7c2	od
nové	nový	k2eAgFnSc2d1	nová
výstavby	výstavba	k1gFnSc2	výstavba
"	"	kIx"	"
<g/>
Severní	severní	k2eAgFnSc2d1	severní
dálnice	dálnice	k1gFnSc2	dálnice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
brněnskou	brněnský	k2eAgFnSc4d1	brněnská
silnici	silnice	k1gFnSc4	silnice
<g/>
,	,	kIx,	,
po	po	k7c4	po
které	který	k3yRgFnPc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
projelo	projet	k5eAaPmAgNnS	projet
40	[number]	k4	40
tisíc	tisíc	k4xCgInPc2	tisíc
vozidel	vozidlo	k1gNnPc2	vozidlo
tranzitní	tranzitní	k2eAgFnSc2d1	tranzitní
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
tu	tu	k6eAd1	tu
nahradí	nahradit	k5eAaPmIp3nS	nahradit
nová	nový	k2eAgFnSc1d1	nová
dálnice	dálnice	k1gFnSc1	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
na	na	k7c4	na
dálnici	dálnice	k1gFnSc4	dálnice
A5	A5	k1gFnSc2	A5
mezi	mezi	k7c4	mezi
Schrick	Schrick	k1gInSc4	Schrick
a	a	k8xC	a
Eibesbrunn	Eibesbrunn	k1gInSc1	Eibesbrunn
se	se	k3xPyFc4	se
napojí	napojit	k5eAaPmIp3nS	napojit
na	na	k7c4	na
Vídeňský	vídeňský	k2eAgInSc4d1	vídeňský
rychlostní	rychlostní	k2eAgInSc4d1	rychlostní
okruh	okruh	k1gInSc4	okruh
S1	S1	k1gFnPc2	S1
od	od	k7c2	od
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
Gaweinstal	Gaweinstal	k1gFnSc6	Gaweinstal
prováděna	provádět	k5eAaImNgFnS	provádět
demontáž	demontáž	k1gFnSc1	demontáž
brněnské	brněnský	k2eAgFnSc2d1	brněnská
silnice	silnice	k1gFnSc2	silnice
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
plánují	plánovat	k5eAaImIp3nP	plánovat
nové	nový	k2eAgFnPc1d1	nová
cyklistické	cyklistický	k2eAgFnPc1d1	cyklistická
stezky	stezka	k1gFnPc1	stezka
<g/>
,	,	kIx,	,
zeleň	zeleň	k1gFnSc1	zeleň
<g/>
,	,	kIx,	,
hostinské	hostinský	k2eAgFnPc4d1	hostinská
zahrádky	zahrádka	k1gFnPc4	zahrádka
a	a	k8xC	a
parkovací	parkovací	k2eAgNnPc4d1	parkovací
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
fary	fara	k1gFnSc2	fara
Gaweinstal	Gaweinstal	k1gFnSc2	Gaweinstal
==	==	k?	==
</s>
</p>
<p>
<s>
Přesné	přesný	k2eAgNnSc1d1	přesné
datum	datum	k1gNnSc1	datum
založení	založení	k1gNnSc2	založení
fary	fara	k1gFnSc2	fara
nebylo	být	k5eNaImAgNnS	být
dosud	dosud	k6eAd1	dosud
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
patřilo	patřit	k5eAaImAgNnS	patřit
místo	místo	k1gNnSc1	místo
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
založení	založení	k1gNnSc6	založení
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1050	[number]	k4	1050
k	k	k7c3	k
obvodu	obvod	k1gInSc3	obvod
mateřské	mateřský	k2eAgFnSc2d1	mateřská
fary	fara	k1gFnSc2	fara
Großrußbach	Großrußbacha	k1gFnPc2	Großrußbacha
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
jen	jen	k9	jen
způsob	způsob	k1gInSc4	způsob
starosti	starost	k1gFnSc2	starost
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
vlastní	vlastní	k2eAgInSc4d1	vlastní
kostel	kostel	k1gInSc4	kostel
během	během	k7c2	během
doby	doba	k1gFnSc2	doba
následoval	následovat	k5eAaImAgInS	následovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vlastní	vlastní	k2eAgInSc1d1	vlastní
kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
obvodu	obvod	k1gInSc2	obvod
vyčleněn	vyčleněn	k2eAgMnSc1d1	vyčleněn
a	a	k8xC	a
osamostatněn	osamostatněn	k2eAgMnSc1d1	osamostatněn
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
dokumentovaná	dokumentovaný	k2eAgFnSc1d1	dokumentovaná
zmínka	zmínka	k1gFnSc1	zmínka
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1280	[number]	k4	1280
<g/>
,	,	kIx,	,
když	když	k8xS	když
král	král	k1gMnSc1	král
Rudolf	Rudolf	k1gMnSc1	Rudolf
I.	I.	kA	I.
Habsburský	habsburský	k2eAgInSc1d1	habsburský
předal	předat	k5eAaPmAgMnS	předat
Skotskému	skotský	k2eAgInSc3d1	skotský
klášteru	klášter	k1gInSc3	klášter
patronátní	patronátní	k2eAgInSc1d1	patronátní
práva	právo	k1gNnSc2	právo
nad	nad	k7c7	nad
zemskou	zemský	k2eAgFnSc7d1	zemská
knížecí	knížecí	k2eAgFnSc7d1	knížecí
farností	farnost	k1gFnSc7	farnost
Gaunenstorf	Gaunenstorf	k1gMnSc1	Gaunenstorf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
zasvěcený	zasvěcený	k2eAgInSc1d1	zasvěcený
svatému	svatý	k2eAgInSc3d1	svatý
Georgu	Georg	k1gInSc3	Georg
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1278	[number]	k4	1278
písemně	písemně	k6eAd1	písemně
zmíněn	zmínit	k5eAaPmNgInS	zmínit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
kostel	kostel	k1gInSc1	kostel
stál	stát	k5eAaImAgInS	stát
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
na	na	k7c6	na
témže	týž	k3xTgNnSc6	týž
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
dnes	dnes	k6eAd1	dnes
stojí	stát	k5eAaImIp3nS	stát
"	"	kIx"	"
<g/>
Bílý	bílý	k2eAgInSc1d1	bílý
kříž	kříž	k1gInSc1	kříž
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
ukřižování	ukřižování	k1gNnSc2	ukřižování
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
ji	on	k3xPp3gFnSc4	on
poštmistr	poštmistr	k1gMnSc1	poštmistr
Lettner	Lettner	k1gMnSc1	Lettner
1718	[number]	k4	1718
a	a	k8xC	a
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
kamenický	kamenický	k2eAgMnSc1d1	kamenický
mistr	mistr	k1gMnSc1	mistr
Oxner	Oxner	k1gMnSc1	Oxner
ji	on	k3xPp3gFnSc4	on
postavil	postavit	k5eAaPmAgMnS	postavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1541	[number]	k4	1541
zvolený	zvolený	k2eAgMnSc1d1	zvolený
opat	opat	k1gMnSc1	opat
Skotského	skotský	k2eAgInSc2d1	skotský
kláštera	klášter	k1gInSc2	klášter
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Traunsteiner	Traunsteiner	k1gMnSc1	Traunsteiner
navštívil	navštívit	k5eAaPmAgMnS	navštívit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1544	[number]	k4	1544
farní	farní	k2eAgInSc1d1	farní
úřad	úřad	k1gInSc1	úřad
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
obnovu	obnova	k1gFnSc4	obnova
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prohlídce	prohlídka	k1gFnSc6	prohlídka
opat	opat	k1gMnSc1	opat
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
shledal	shledat	k5eAaPmAgMnS	shledat
kostel	kostel	k1gInSc4	kostel
již	již	k6eAd1	již
značně	značně	k6eAd1	značně
chatrný	chatrný	k2eAgInSc1d1	chatrný
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
nacházela	nacházet	k5eAaImAgFnS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
farního	farní	k2eAgInSc2d1	farní
kostela	kostel	k1gInSc2	kostel
"	"	kIx"	"
<g/>
násypka	násypka	k1gFnSc1	násypka
<g/>
"	"	kIx"	"
a	a	k8xC	a
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
,	,	kIx,	,
obklopený	obklopený	k2eAgInSc1d1	obklopený
zdí	zeď	k1gFnSc7	zeď
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k9	jako
útulek	útulek	k1gInSc1	útulek
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
obhajován	obhajovat	k5eAaImNgInS	obhajovat
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
opevněnému	opevněný	k2eAgNnSc3d1	opevněné
místu	místo	k1gNnSc3	místo
byl	být	k5eAaImAgInS	být
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
věží	věž	k1gFnSc7	věž
a	a	k8xC	a
obklopen	obklopit	k5eAaPmNgInS	obklopit
hradebním	hradební	k2eAgInSc7d1	hradební
příkopem	příkop	k1gInSc7	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Násypka	násypka	k1gFnSc1	násypka
byla	být	k5eAaImAgFnS	být
ke	k	k7c3	k
kostelu	kostel	k1gInSc3	kostel
přestavěna	přestavěn	k2eAgMnSc4d1	přestavěn
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nazývali	nazývat	k5eAaImAgMnP	nazývat
"	"	kIx"	"
<g/>
naší	náš	k3xOp1gFnSc2	náš
milované	milovaný	k2eAgFnSc2d1	milovaná
paní	paní	k1gFnSc2	paní
z	z	k7c2	z
hor	hora	k1gFnPc2	hora
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
kostel	kostel	k1gInSc1	kostel
pustl	pustnout	k5eAaImAgInS	pustnout
stále	stále	k6eAd1	stále
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
až	až	k9	až
konečně	konečně	k6eAd1	konečně
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
zbouraný	zbouraný	k2eAgMnSc1d1	zbouraný
nebo	nebo	k8xC	nebo
1645	[number]	k4	1645
díky	díky	k7c3	díky
švédskému	švédský	k2eAgNnSc3d1	švédské
vojsku	vojsko	k1gNnSc3	vojsko
byl	být	k5eAaImAgInS	být
zcela	zcela	k6eAd1	zcela
zničený	zničený	k2eAgInSc1d1	zničený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
1645	[number]	k4	1645
také	také	k9	také
zničený	zničený	k2eAgInSc1d1	zničený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kolem	kolem	k7c2	kolem
1650	[number]	k4	1650
byl	být	k5eAaImAgInS	být
obnovený	obnovený	k2eAgInSc1d1	obnovený
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgInSc1d1	stavební
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
desetiletích	desetiletí	k1gNnPc6	desetiletí
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
1688	[number]	k4	1688
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
zřícení	zřícení	k1gNnSc2	zřícení
přestál	přestát	k5eAaPmAgMnS	přestát
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
1692	[number]	k4	1692
až	až	k6eAd1	až
na	na	k7c4	na
chór	chór	k1gInSc4	chór
nově	nově	k6eAd1	nově
postavena	postaven	k2eAgFnSc1d1	postavena
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1702	[number]	k4	1702
byl	být	k5eAaImAgInS	být
dnešní	dnešní	k2eAgInSc1d1	dnešní
chór	chór	k1gInSc1	chór
přistavěn	přistavěn	k2eAgInSc1d1	přistavěn
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
hřbitovní	hřbitovní	k2eAgFnSc2d1	hřbitovní
zdi	zeď	k1gFnSc2	zeď
se	se	k3xPyFc4	se
rozebrali	rozebrat	k5eAaPmAgMnP	rozebrat
na	na	k7c4	na
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
přístavbu	přístavba	k1gFnSc4	přístavba
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
věž	věž	k1gFnSc1	věž
byla	být	k5eAaImAgFnS	být
nově	nově	k6eAd1	nově
postavena	postaven	k2eAgFnSc1d1	postavena
a	a	k8xC	a
hradební	hradební	k2eAgInSc1d1	hradební
příkop	příkop	k1gInSc1	příkop
zasypán	zasypat	k5eAaPmNgInS	zasypat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1704	[number]	k4	1704
kostel	kostel	k1gInSc1	kostel
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
zase	zase	k9	zase
až	až	k9	až
do	do	k7c2	do
základů	základ	k1gInPc2	základ
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nakonec	nakonec	k6eAd1	nakonec
postavený	postavený	k2eAgMnSc1d1	postavený
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
tu	tu	k6eAd1	tu
přitom	přitom	k6eAd1	přitom
o	o	k7c4	o
stavbu	stavba	k1gFnSc4	stavba
vrcholného	vrcholný	k2eAgNnSc2d1	vrcholné
baroka	baroko	k1gNnSc2	baroko
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc4	kostel
halový	halový	k2eAgInSc4d1	halový
<g/>
,	,	kIx,	,
podélný	podélný	k2eAgInSc4d1	podélný
s	s	k7c7	s
valenou	valený	k2eAgFnSc7d1	valená
klenbou	klenba	k1gFnSc7	klenba
<g/>
;	;	kIx,	;
dvě	dva	k4xCgNnPc4	dva
kněžiště	kněžiště	k1gNnPc4	kněžiště
s	s	k7c7	s
rovným	rovný	k2eAgInSc7d1	rovný
chórem	chór	k1gInSc7	chór
s	s	k7c7	s
výstřední	výstřední	k2eAgFnSc7d1	výstřední
připojenou	připojený	k2eAgFnSc7d1	připojená
křížovou	křížový	k2eAgFnSc7d1	křížová
ostrohrannou	ostrohranný	k2eAgFnSc7d1	ostrohranná
klenbou	klenba	k1gFnSc7	klenba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naproti	naproti	k6eAd1	naproti
farního	farní	k2eAgInSc2d1	farní
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
budova	budova	k1gFnSc1	budova
vysoce	vysoce	k6eAd1	vysoce
barokní	barokní	k2eAgFnSc2d1	barokní
fary	fara	k1gFnSc2	fara
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dvoupodlažní	dvoupodlažní	k2eAgFnSc1d1	dvoupodlažní
budova	budova	k1gFnSc1	budova
se	s	k7c7	s
čtyřmi	čtyři	k4xCgNnPc7	čtyři
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
působí	působit	k5eAaImIp3nS	působit
dojmem	dojem	k1gInSc7	dojem
místností	místnost	k1gFnPc2	místnost
s	s	k7c7	s
významnými	významný	k2eAgInPc7d1	významný
štukovými	štukový	k2eAgInPc7d1	štukový
povrchy	povrch	k1gInPc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Prelátova	prelátův	k2eAgFnSc1d1	prelátův
síň	síň	k1gFnSc1	síň
je	být	k5eAaImIp3nS	být
obložená	obložený	k2eAgFnSc1d1	obložená
a	a	k8xC	a
vyzdobená	vyzdobený	k2eAgFnSc1d1	vyzdobená
barokní	barokní	k2eAgFnSc7d1	barokní
olejomalbou	olejomalba	k1gFnSc7	olejomalba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jako	jako	k9	jako
čtyři	čtyři	k4xCgInPc4	čtyři
světadíly	světadíl	k1gInPc4	světadíl
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
kardinální	kardinální	k2eAgFnSc1d1	kardinální
ctnosti	ctnost	k1gFnPc1	ctnost
<g/>
:	:	kIx,	:
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
lásku	láska	k1gFnSc4	láska
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
a	a	k8xC	a
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
výsledku	výsledek	k1gInSc2	výsledek
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
3.485	[number]	k4	3.485
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
měl	mít	k5eAaImAgInS	mít
městys	městys	k1gInSc1	městys
3.024	[number]	k4	3.024
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
2.781	[number]	k4	2.781
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
2.736	[number]	k4	2.736
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
počty	počet	k1gInPc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
:	:	kIx,	:
1951	[number]	k4	1951
1.117	[number]	k4	1.117
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
1.380	[number]	k4	1.380
<g/>
,	,	kIx,	,
1869	[number]	k4	1869
1.245	[number]	k4	1.245
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Aigen	Aigna	k1gFnPc2	Aigna
<g/>
,	,	kIx,	,
Wieden	Wiedna	k1gFnPc2	Wiedna
und	und	k?	und
Markt-Gaunersdorf	Markt-Gaunersdorf	k1gMnSc1	Markt-Gaunersdorf
dohromady	dohromady	k6eAd1	dohromady
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
měl	mít	k5eAaImAgInS	mít
Markt-Gaunersdorf	Markt-Gaunersdorf	k1gInSc1	Markt-Gaunersdorf
676	[number]	k4	676
<g/>
,	,	kIx,	,
Aigen-Gaunersdorf	Aigen-Gaunersdorf	k1gInSc1	Aigen-Gaunersdorf
323	[number]	k4	323
a	a	k8xC	a
Wieden-Gaunersdorf	Wieden-Gaunersdorf	k1gInSc1	Wieden-Gaunersdorf
168	[number]	k4	168
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Starostou	Starosta	k1gMnSc7	Starosta
je	být	k5eAaImIp3nS	být
Richard	Richard	k1gMnSc1	Richard
Schober	Schober	k1gMnSc1	Schober
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
kanceláře	kancelář	k1gFnSc2	kancelář
Gerald	Gerald	k1gMnSc1	Gerald
Schalkhammer	Schalkhammer	k1gMnSc1	Schalkhammer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
obecní	obecní	k2eAgFnSc6d1	obecní
radě	rada	k1gFnSc6	rada
23	[number]	k4	23
křesel	křeslo	k1gNnPc2	křeslo
podle	podle	k7c2	podle
výsledku	výsledek	k1gInSc2	výsledek
voleb	volba	k1gFnPc2	volba
z	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2005	[number]	k4	2005
s	s	k7c7	s
následujícími	následující	k2eAgInPc7d1	následující
mandáty	mandát	k1gInPc7	mandát
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
ÖVP	ÖVP	kA	ÖVP
<g/>
)	)	kIx)	)
13	[number]	k4	13
a	a	k8xC	a
(	(	kIx(	(
<g/>
SPÖ	SPÖ	kA	SPÖ
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obecní	obecní	k2eAgInSc1d1	obecní
znak	znak	k1gInSc1	znak
===	===	k?	===
</s>
</p>
<p>
<s>
Popis	popis	k1gInSc1	popis
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
zlatém	zlatý	k2eAgNnSc6d1	Zlaté
poli	pole	k1gNnSc6	pole
zelený	zelený	k2eAgInSc4d1	zelený
trojlístek	trojlístek	k1gInSc4	trojlístek
<g/>
,	,	kIx,	,
uvnitř	uvnitř	k6eAd1	uvnitř
štítek	štítek	k1gInSc1	štítek
s	s	k7c7	s
červeno-bílo-červeným	červenoílo-červený	k2eAgNnSc7d1	červeno-bílo-červený
břevnem	břevno	k1gNnSc7	břevno
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
protokolu	protokol	k1gInSc2	protokol
obecní	obecní	k2eAgFnSc2d1	obecní
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
internetové	internetový	k2eAgFnPc4d1	internetová
stránky	stránka	k1gFnPc4	stránka
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
aktuální	aktuální	k2eAgNnSc1d1	aktuální
propůjčení	propůjčení	k1gNnSc1	propůjčení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
a	a	k8xC	a
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
==	==	k?	==
</s>
</p>
<p>
<s>
Nezemědělských	zemědělský	k2eNgNnPc2d1	nezemědělské
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
111	[number]	k4	111
<g/>
,	,	kIx,	,
zemědělsko	zemědělsko	k6eAd1	zemědělsko
a	a	k8xC	a
lesnických	lesnický	k2eAgNnPc2d1	lesnické
pracovišť	pracoviště	k1gNnPc2	pracoviště
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
bylo	být	k5eAaImAgNnS	být
137	[number]	k4	137
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
výdělečně	výdělečně	k6eAd1	výdělečně
činných	činný	k2eAgFnPc2d1	činná
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
bydlišti	bydliště	k1gNnSc6	bydliště
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
1.605	[number]	k4	1.605
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
47,25	[number]	k4	47,25
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významní	významný	k2eAgMnPc1d1	významný
rodáci	rodák	k1gMnPc1	rodák
==	==	k?	==
</s>
</p>
<p>
<s>
Ignaz	Ignaz	k1gInSc1	Ignaz
Withalm	Withalm	k1gInSc1	Withalm
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1851	[number]	k4	1851
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1910	[number]	k4	1910
v	v	k7c6	v
Gaweinstalu	Gaweinstal	k1gMnSc6	Gaweinstal
<g/>
)	)	kIx)	)
–	–	k?	–
mlynář	mlynář	k1gMnSc1	mlynář
a	a	k8xC	a
starosta	starosta	k1gMnSc1	starosta
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
zemského	zemský	k2eAgInSc2d1	zemský
dolnorakouského	dolnorakouský	k2eAgInSc2d1	dolnorakouský
sněmu	sněm	k1gInSc2	sněm
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
–	–	k?	–
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hermann	Hermann	k1gMnSc1	Hermann
Withalm	Withalm	k1gMnSc1	Withalm
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1912	[number]	k4	1912
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2003	[number]	k4	2003
ve	v	k7c6	v
Wolkersdorfu	Wolkersdorf	k1gInSc6	Wolkersdorf
ve	v	k7c6	v
Weinviertelu	Weinviertel	k1gInSc6	Weinviertel
<g/>
)	)	kIx)	)
–	–	k?	–
rakouský	rakouský	k2eAgMnSc1d1	rakouský
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
ÖVP	ÖVP	kA	ÖVP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
náměstek	náměstek	k1gMnSc1	náměstek
kancléře	kancléř	k1gMnSc2	kancléř
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reinhard	Reinhard	k1gMnSc1	Reinhard
Führer	Führer	k1gMnSc1	Führer
(	(	kIx(	(
<g/>
*	*	kIx~	*
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
–	–	k?	–
německý	německý	k2eAgMnSc1d1	německý
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
CDU	CDU	kA	CDU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Německého	německý	k2eAgInSc2d1	německý
svazu	svaz	k1gInSc2	svaz
o	o	k7c4	o
válečné	válečný	k2eAgInPc4d1	válečný
hroby	hrob	k1gInPc4	hrob
<g/>
,	,	kIx,	,
někdejší	někdejší	k2eAgFnSc4d1	někdejší
předseda	předseda	k1gMnSc1	předseda
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Gaweinstal	Gaweinstal	k1gFnSc2	Gaweinstal
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Walter	Walter	k1gMnSc1	Walter
F.	F.	kA	F.
Kalina	Kalina	k1gMnSc1	Kalina
<g/>
,	,	kIx,	,
<g/>
Kaiser	Kaiser	k1gMnSc1	Kaiser
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
und	und	k?	und
die	die	k?	die
bildende	bildend	k1gInSc5	bildend
Kunst	Kunst	k1gMnSc1	Kunst
<g/>
.	.	kIx.	.
</s>
<s>
Ein	Ein	k?	Ein
Beitrag	Beitrag	k1gInSc1	Beitrag
zur	zur	k?	zur
Kulturgeschichte	Kulturgeschicht	k1gInSc5	Kulturgeschicht
des	des	k1gNnPc2	des
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Jahrhunderts	Jahrhunderts	k6eAd1	Jahrhunderts
<g/>
.	.	kIx.	.
</s>
<s>
Dissertation	Dissertation	k1gInSc1	Dissertation
<g/>
,	,	kIx,	,
Universität	Universität	k2eAgInSc1d1	Universität
Wien	Wien	k1gInSc1	Wien
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Walter	Walter	k1gMnSc1	Walter
F.	F.	kA	F.
Kalina	Kalina	k1gMnSc1	Kalina
<g/>
,	,	kIx,	,
<g/>
Der	drát	k5eAaImRp2nS	drát
Dreißigjährige	Dreißigjährige	k1gInSc4	Dreißigjährige
Krieg	Krieg	k1gInSc1	Krieg
in	in	k?	in
der	drát	k5eAaImRp2nS	drát
bildenden	bildendna	k1gFnPc2	bildendna
Kunst	Kunst	k1gMnSc1	Kunst
<g/>
.	.	kIx.	.
</s>
<s>
Diplomarbeit	Diplomarbeit	k1gInSc1	Diplomarbeit
<g/>
,	,	kIx,	,
UniversitätWien	UniversitätWien	k1gInSc1	UniversitätWien
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elfriede	Elfriíst	k5eAaPmIp3nS	Elfriíst
Popp	Popp	k1gMnSc1	Popp
<g/>
,	,	kIx,	,
<g/>
Gaweinstal	Gaweinstal	k1gMnSc1	Gaweinstal
in	in	k?	in
historischen	historischen	k1gInSc1	historischen
Ansichten	Ansichten	k2eAgInSc1d1	Ansichten
<g/>
.	.	kIx.	.
</s>
<s>
Budapest	Budapest	k1gMnSc1	Budapest
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elfriede	Elfriíst	k5eAaPmIp3nS	Elfriíst
Popp	Popp	k1gInSc1	Popp
<g/>
,	,	kIx,	,
<g/>
Historische	Historische	k1gNnSc1	Historische
Entwicklung	Entwicklunga	k1gFnPc2	Entwicklunga
der	drát	k5eAaImRp2nS	drát
Marktgemeinde	Marktgemeind	k1gMnSc5	Marktgemeind
Gaweinstal	Gaweinstal	k1gMnSc5	Gaweinstal
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Heimat	Heimat	k1gInSc1	Heimat
im	im	k?	im
Weinland	Weinland	k1gInSc1	Weinland
<g/>
.	.	kIx.	.
</s>
<s>
Heimatkundliches	Heimatkundliches	k1gMnSc1	Heimatkundliches
Beiblatt	Beiblatt	k1gMnSc1	Beiblatt
zum	zum	k?	zum
Amtsblatt	Amtsblatt	k1gMnSc1	Amtsblatt
der	drát	k5eAaImRp2nS	drát
Bezirkshauptmannschaft	Bezirkshauptmannschaft	k1gMnSc1	Bezirkshauptmannschaft
Mistelbach	Mistelbach	k1gMnSc1	Mistelbach
<g/>
,	,	kIx,	,
Jahrgang	Jahrgang	k1gMnSc1	Jahrgang
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Mistelbach	Mistelbach	k1gInSc1	Mistelbach
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Christian	Christian	k1gMnSc1	Christian
Jostmann	Jostmann	k1gMnSc1	Jostmann
<g/>
:	:	kIx,	:
<g/>
Die	Die	k1gMnSc1	Die
Brünner	Brünner	k1gMnSc1	Brünner
Straße	Straß	k1gFnSc2	Straß
–	–	k?	–
Eine	Einus	k1gMnSc5	Einus
Geschichte	Geschicht	k1gMnSc5	Geschicht
des	des	k1gNnSc7	des
Verkehrsweges	Verkehrsweges	k1gInSc1	Verkehrsweges
von	von	k1gInSc1	von
Wien	Wiena	k1gFnPc2	Wiena
nach	nach	k1gInSc1	nach
Brünn	Brünn	k1gMnSc1	Brünn
in	in	k?	in
Bildern	Bildern	k1gMnSc1	Bildern
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
Edition	Edition	k1gInSc1	Edition
Winkler-Hermaden	Winkler-Hermaden	k2eAgInSc1d1	Winkler-Hermaden
<g/>
,	,	kIx,	,
<g/>
ISBN	ISBN	kA	ISBN
978-3-9502688-6-7	[number]	k4	978-3-9502688-6-7
</s>
</p>
<p>
<s>
Hans	Hans	k1gMnSc1	Hans
Spreitzer	Spreitzer	k1gMnSc1	Spreitzer
<g/>
,	,	kIx,	,
<g/>
Gaweinstal	Gaweinstal	k1gMnSc1	Gaweinstal
<g/>
.	.	kIx.	.
</s>
<s>
Aus	Aus	k?	Aus
der	drát	k5eAaImRp2nS	drát
Vergangenheit	Vergangenheit	k1gInSc1	Vergangenheit
des	des	k1gNnSc2	des
ersten	ersten	k2eAgMnSc1d1	ersten
Weinviertler	Weinviertler	k1gMnSc1	Weinviertler
Kreisvorortes	Kreisvorortes	k1gMnSc1	Kreisvorortes
<g/>
.	.	kIx.	.
<g/>
Mistelbach	Mistelbach	k1gMnSc1	Mistelbach
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
</s>
</p>
<p>
<s>
Bundesdenkmalamt	Bundesdenkmalamt	k1gMnSc1	Bundesdenkmalamt
(	(	kIx(	(
<g/>
Hg	Hg	k1gMnSc1	Hg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Trassenarchäologie	Trassenarchäologie	k1gFnSc1	Trassenarchäologie
<g/>
.	.	kIx.	.
</s>
<s>
Neue	Neue	k6eAd1	Neue
Straßen	Straßen	k2eAgInSc1d1	Straßen
im	im	k?	im
Weinviertel	Weinviertel	k1gInSc1	Weinviertel
<g/>
.	.	kIx.	.
</s>
<s>
Wien	Wien	k1gMnSc1	Wien
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
Evelyn	Evelyn	k1gMnSc1	Evelyn
Benesch	Benesch	k1gMnSc1	Benesch
<g/>
,	,	kIx,	,
Bernd	Bernd	k1gMnSc1	Bernd
Euler-Rolle	Euler-Rolle	k1gFnSc1	Euler-Rolle
<g/>
,	,	kIx,	,
Claudia	Claudia	k1gFnSc1	Claudia
Haas	Haas	k1gInSc1	Haas
<g/>
,	,	kIx,	,
Renate	Renat	k1gInSc5	Renat
Holzschuh-Hofer	Holzschuh-Hofer	k1gMnSc1	Holzschuh-Hofer
<g/>
,	,	kIx,	,
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Huber	Huber	k1gMnSc1	Huber
<g/>
,	,	kIx,	,
Katharina	Katharina	k1gMnSc1	Katharina
Packpfeifer	Packpfeifer	k1gMnSc1	Packpfeifer
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Maria	Maria	k1gFnSc1	Maria
Vancsa-Tironiek	Vancsa-Tironiek	k1gInSc1	Vancsa-Tironiek
<g/>
,	,	kIx,	,
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Vogg	Vogg	k1gMnSc1	Vogg
<g/>
:	:	kIx,	:
Niederösterreich	Niederösterreich	k1gMnSc1	Niederösterreich
nördlich	nördlich	k1gMnSc1	nördlich
der	drát	k5eAaImRp2nS	drát
Donau	donau	k1gInSc1	donau
<g/>
,	,	kIx,	,
Bundesdenkmalamt	Bundesdenkmalamt	k1gMnSc1	Bundesdenkmalamt
(	(	kIx(	(
<g/>
Hrsg	Hrsg	k1gMnSc1	Hrsg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Dehio-Handbuch	Dehio-Handbuch	k1gMnSc1	Dehio-Handbuch
Die	Die	k1gFnSc2	Die
Kunstdenkmäler	Kunstdenkmäler	k1gMnSc1	Kunstdenkmäler
Österreichs	Österreichs	k1gInSc1	Österreichs
<g/>
.	.	kIx.	.
</s>
<s>
Anton	Anton	k1gMnSc1	Anton
Schroll	Schroll	k1gMnSc1	Schroll
&	&	k?	&
Co	co	k9	co
<g/>
,	,	kIx,	,
Wien	Wien	k1gInSc1	Wien
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
7031	[number]	k4	7031
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
652	[number]	k4	652
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
S.	S.	kA	S.
247	[number]	k4	247
<g/>
–	–	k?	–
<g/>
250	[number]	k4	250
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karl	Karl	k1gMnSc1	Karl
Mattes	Mattes	k1gMnSc1	Mattes
<g/>
,	,	kIx,	,
<g/>
Heimatbuch	Heimatbuch	k1gMnSc1	Heimatbuch
des	des	k1gNnSc2	des
Verwaltungsbezirkes	Verwaltungsbezirkes	k1gMnSc1	Verwaltungsbezirkes
Mistelbach	Mistelbach	k1gMnSc1	Mistelbach
<g/>
.	.	kIx.	.
</s>
<s>
Wien	Wien	k1gMnSc1	Wien
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Jordan	Jordan	k1gMnSc1	Jordan
<g/>
,	,	kIx,	,
<g/>
April	April	k1gMnSc1	April
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Kämpfe	Kämpf	k1gMnSc5	Kämpf
im	im	k?	im
nördlichen	nördlichen	k2eAgInSc4d1	nördlichen
Niederösterreich	Niederösterreich	k1gInSc4	Niederösterreich
<g/>
.	.	kIx.	.
</s>
<s>
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hans	Hans	k1gMnSc1	Hans
Egger	Egger	k1gMnSc1	Egger
/	/	kIx~	/
Franz	Franz	k1gMnSc1	Franz
Jordan	Jordan	k1gMnSc1	Jordan
<g/>
,	,	kIx,	,
Brände	Bränd	k1gInSc5	Bränd
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Donau	donau	k1gInSc2	donau
<g/>
.	.	kIx.	.
</s>
<s>
Das	Das	k?	Das
Finale	Final	k1gMnSc5	Final
des	des	k1gNnSc7	des
Zweiten	Zweiten	k2eAgInSc1d1	Zweiten
Weltkriegs	Weltkriegs	k1gInSc1	Weltkriegs
in	in	k?	in
Wien	Wien	k1gInSc1	Wien
<g/>
,	,	kIx,	,
Niederösterreich	Niederösterreich	k1gInSc1	Niederösterreich
und	und	k?	und
Nordburgenland	Nordburgenland	k1gInSc1	Nordburgenland
<g/>
.	.	kIx.	.
</s>
<s>
Graz	Graz	k1gMnSc1	Graz
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gaweinstal	Gaweinstal	k1gFnSc2	Gaweinstal
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
