<s>
Želva	želva	k1gFnSc1	želva
třípásá	třípásý	k2eAgFnSc1d1	třípásý
(	(	kIx(	(
<g/>
Cuora	Cuora	k1gFnSc1	Cuora
trifasciata	trifasciata	k1gFnSc1	trifasciata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
želva	želva	k1gFnSc1	želva
zlatá	zlatý	k2eAgFnSc1d1	zlatá
mínce	mínce	k1gFnSc1	mínce
je	být	k5eAaImIp3nS	být
kriticky	kriticky	k6eAd1	kriticky
ohrožená	ohrožený	k2eAgFnSc1d1	ohrožená
asijská	asijský	k2eAgFnSc1d1	asijská
sladkovodní	sladkovodní	k2eAgFnSc1d1	sladkovodní
želva	želva	k1gFnSc1	želva
rodu	rod	k1gInSc2	rod
Cuora	Cuor	k1gInSc2	Cuor
<g/>
.	.	kIx.	.
</s>
<s>
Krunýř	krunýř	k1gInSc1	krunýř
má	mít	k5eAaImIp3nS	mít
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
asi	asi	k9	asi
15	[number]	k4	15
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Karapax	Karapax	k1gInSc1	Karapax
má	mít	k5eAaImIp3nS	mít
mírně	mírně	k6eAd1	mírně
klenutý	klenutý	k2eAgInSc4d1	klenutý
<g/>
,	,	kIx,	,
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
šedě	šedě	k6eAd1	šedě
až	až	k9	až
červenohnědě	červenohnědě	k6eAd1	červenohnědě
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
fialovým	fialový	k2eAgInSc7d1	fialový
nádechem	nádech	k1gInSc7	nádech
a	a	k8xC	a
třemi	tři	k4xCgInPc7	tři
podélnými	podélný	k2eAgInPc7d1	podélný
černými	černý	k2eAgInPc7d1	černý
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Plastron	plastron	k1gInSc1	plastron
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgInSc1d1	černý
<g/>
,	,	kIx,	,
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
žlutě	žlutě	k6eAd1	žlutě
lemovaný	lemovaný	k2eAgMnSc1d1	lemovaný
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
roku	rok	k1gInSc2	rok
Cuora	Cuor	k1gInSc2	Cuor
je	být	k5eAaImIp3nS	být
i	i	k9	i
plastron	plastron	k1gInSc1	plastron
želvy	želva	k1gFnSc2	želva
třípásé	třípásý	k2eAgFnSc2d1	třípásý
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
pružným	pružný	k2eAgInSc7d1	pružný
vazivovým	vazivový	k2eAgInSc7d1	vazivový
kloubem	kloub	k1gInSc7	kloub
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
želva	želva	k1gFnSc1	želva
může	moct	k5eAaImIp3nS	moct
pevně	pevně	k6eAd1	pevně
uzavřít	uzavřít	k5eAaPmF	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
úzká	úzký	k2eAgFnSc1d1	úzká
hlava	hlava	k1gFnSc1	hlava
se	s	k7c7	s
zašpičatělým	zašpičatělý	k2eAgInSc7d1	zašpičatělý
čenichem	čenich	k1gInSc7	čenich
je	být	k5eAaImIp3nS	být
shora	shora	k6eAd1	shora
zlatožlutá	zlatožlutý	k2eAgFnSc1d1	zlatožlutá
<g/>
,	,	kIx,	,
zboku	zboku	k6eAd1	zboku
černá	černý	k2eAgFnSc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
želva	želva	k1gFnSc1	želva
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Kantonu	Kanton	k1gInSc2	Kanton
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
žije	žít	k5eAaImIp3nS	žít
i	i	k9	i
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
severního	severní	k2eAgInSc2d1	severní
Vietnamu	Vietnam	k1gInSc2	Vietnam
a	a	k8xC	a
Laosu	Laos	k1gInSc2	Laos
<g/>
,	,	kIx,	,
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
samosttný	samosttný	k2eAgInSc4d1	samosttný
druh	druh	k1gInSc4	druh
Cuora	Cuor	k1gMnSc2	Cuor
cyclornata	cyclornat	k1gMnSc2	cyclornat
<g/>
.	.	kIx.	.
</s>
<s>
Želva	želva	k1gFnSc1	želva
třípásá	třípásý	k2eAgFnSc1d1	třípásý
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
zarostlých	zarostlý	k2eAgFnPc2d1	zarostlá
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
a	a	k8xC	a
bažin	bažina	k1gFnPc2	bažina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
všežravá	všežravý	k2eAgFnSc1d1	všežravá
<g/>
,	,	kIx,	,
živé	živý	k2eAgFnPc1d1	živá
se	se	k3xPyFc4	se
především	především	k9	především
vodními	vodní	k2eAgMnPc7d1	vodní
bezobratlými	bezobratlý	k2eAgMnPc7d1	bezobratlý
živočichy	živočich	k1gMnPc7	živočich
<g/>
,	,	kIx,	,
červy	červ	k1gMnPc7	červ
<g/>
,	,	kIx,	,
drobnými	drobný	k2eAgFnPc7d1	drobná
rybkami	rybka	k1gFnPc7	rybka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepohrdne	pohrdnout	k5eNaPmIp3nS	pohrdnout
ani	ani	k8xC	ani
rostlinnou	rostlinný	k2eAgFnSc7d1	rostlinná
potravou	potrava	k1gFnSc7	potrava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
Čínu	Čína	k1gFnSc4	Čína
chová	chovat	k5eAaImIp3nS	chovat
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Želva	želva	k1gFnSc1	želva
třípásá	třípásat	k5eAaPmIp3nS	třípásat
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejohroženější	ohrožený	k2eAgInPc4d3	nejohroženější
druhy	druh	k1gInPc4	druh
asijských	asijský	k2eAgFnPc2d1	asijská
želv	želva	k1gFnPc2	želva
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ztráty	ztráta	k1gFnSc2	ztráta
prostředí	prostředí	k1gNnSc2	prostředí
vlivem	vlivem	k7c2	vlivem
stavební	stavební	k2eAgFnSc2d1	stavební
a	a	k8xC	a
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
činnosti	činnost	k1gFnSc2	činnost
či	či	k8xC	či
regulace	regulace	k1gFnSc2	regulace
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
želva	želva	k1gFnSc1	želva
ohrožena	ohrozit	k5eAaPmNgFnS	ohrozit
i	i	k9	i
lovem	lov	k1gInSc7	lov
<g/>
.	.	kIx.	.
</s>
<s>
Rosolovitá	rosolovitý	k2eAgFnSc1d1	rosolovitá
polévka	polévka	k1gFnSc1	polévka
guilingao	guilingao	k6eAd1	guilingao
<g/>
,	,	kIx,	,
připravená	připravený	k2eAgFnSc1d1	připravená
z	z	k7c2	z
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
plastronu	plastron	k1gInSc2	plastron
této	tento	k3xDgFnSc2	tento
želvy	želva	k1gFnSc2	želva
<g/>
,	,	kIx,	,
kořene	kořen	k1gInSc2	kořen
přestupu	přestup	k1gInSc2	přestup
<g/>
,	,	kIx,	,
houby	houba	k1gFnSc2	houba
lesklokorky	lesklokorka	k1gFnSc2	lesklokorka
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
dalších	další	k2eAgFnPc2d1	další
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Číně	Čína	k1gFnSc6	Čína
pokládána	pokládán	k2eAgMnSc4d1	pokládán
za	za	k7c4	za
pochoutku	pochoutka	k1gFnSc4	pochoutka
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
účinný	účinný	k2eAgInSc1d1	účinný
lék	lék	k1gInSc1	lék
proti	proti	k7c3	proti
rozmanitým	rozmanitý	k2eAgFnPc3d1	rozmanitá
chorobám	choroba	k1gFnPc3	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
želvu	želva	k1gFnSc4	želva
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
se	se	k3xPyFc4	se
na	na	k7c6	na
černém	černý	k2eAgInSc6d1	černý
trhu	trh	k1gInSc6	trh
platí	platit	k5eAaImIp3nS	platit
až	až	k9	až
1000	[number]	k4	1000
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Jistým	jistý	k2eAgNnSc7d1	jisté
řešením	řešení	k1gNnSc7	řešení
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
chov	chov	k1gInSc4	chov
těchto	tento	k3xDgFnPc2	tento
želv	želva	k1gFnPc2	želva
pro	pro	k7c4	pro
kuchyňské	kuchyňský	k2eAgInPc4d1	kuchyňský
účely	účel	k1gInPc4	účel
na	na	k7c6	na
specializovaných	specializovaný	k2eAgFnPc6d1	specializovaná
farmách	farma	k1gFnPc6	farma
<g/>
,	,	kIx,	,
i	i	k8xC	i
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
však	však	k9	však
pytlákům	pytlák	k1gMnPc3	pytlák
stále	stále	k6eAd1	stále
vyplácí	vyplácet	k5eAaImIp3nS	vyplácet
jejich	jejich	k3xOp3gNnPc4	jejich
chytání	chytání	k1gNnPc4	chytání
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgInPc1d1	ostatní
druhy	druh	k1gInPc1	druh
želv	želva	k1gFnPc2	želva
rodu	rod	k1gInSc2	rod
Cuora	Cuor	k1gInSc2	Cuor
je	být	k5eAaImIp3nS	být
všežravá	všežravý	k2eAgFnSc1d1	všežravá
<g/>
,	,	kIx,	,
živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
vodními	vodní	k2eAgFnPc7d1	vodní
a	a	k8xC	a
pobřežními	pobřežní	k2eAgFnPc7d1	pobřežní
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
také	také	k9	také
živočišnou	živočišný	k2eAgFnSc7d1	živočišná
potravou	potrava	k1gFnSc7	potrava
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
larvami	larva	k1gFnPc7	larva
vodního	vodní	k2eAgInSc2d1	vodní
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
rybkami	rybka	k1gFnPc7	rybka
a	a	k8xC	a
měkkýši	měkkýš	k1gMnPc1	měkkýš
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
si	se	k3xPyFc3	se
hledá	hledat	k5eAaImIp3nS	hledat
převážně	převážně	k6eAd1	převážně
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Golden	Goldna	k1gFnPc2	Goldna
coin	coina	k1gFnPc2	coina
turtle	turtle	k6eAd1	turtle
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Želva	želva	k1gFnSc1	želva
třípásá	třípásat	k5eAaPmIp3nS	třípásat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Taxon	taxon	k1gInSc1	taxon
Cuora	Cuora	k1gMnSc1	Cuora
trifasciata	trifasciata	k1gFnSc1	trifasciata
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
želva	želva	k1gFnSc1	želva
třípásá	třípásat	k5eAaPmIp3nS	třípásat
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
