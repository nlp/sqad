<s>
Želva	želva	k1gFnSc1	želva
třípásá	třípásý	k2eAgFnSc1d1	třípásý
(	(	kIx(	(
<g/>
Cuora	Cuora	k1gFnSc1	Cuora
trifasciata	trifasciata	k1gFnSc1	trifasciata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
želva	želva	k1gFnSc1	želva
zlatá	zlatý	k2eAgFnSc1d1	zlatá
mínce	mínce	k1gFnSc1	mínce
je	být	k5eAaImIp3nS	být
kriticky	kriticky	k6eAd1	kriticky
ohrožená	ohrožený	k2eAgFnSc1d1	ohrožená
asijská	asijský	k2eAgFnSc1d1	asijská
sladkovodní	sladkovodní	k2eAgFnSc1d1	sladkovodní
želva	želva	k1gFnSc1	želva
rodu	rod	k1gInSc2	rod
Cuora	Cuor	k1gInSc2	Cuor
<g/>
.	.	kIx.	.
</s>
