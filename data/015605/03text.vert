<s>
Kajagum	Kajagum	k1gInSc1
</s>
<s>
Sanjo	Sanjo	k6eAd1
kajagum	kajagum	k1gInSc1
</s>
<s>
Hráčka	hráčka	k1gFnSc1
na	na	k7c6
dvanácti	dvanáct	k4xCc6
strunný	strunný	k2eAgMnSc1d1
sanjo	sanjo	k6eAd1
kajagum	kajagum	k1gInSc1
</s>
<s>
Kajagum	Kajagum	k1gNnSc1
(	(	kIx(
<g/>
korejsky	korejsky	k6eAd1
<g/>
:	:	kIx,
가	가	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
tradiční	tradiční	k2eAgInSc4d1
korejský	korejský	k2eAgInSc4d1
strunný	strunný	k2eAgInSc4d1
hudební	hudební	k2eAgInSc4d1
nástroj	nástroj	k1gInSc4
patřící	patřící	k2eAgInSc4d1
mezi	mezi	k7c4
citery	citera	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
dvanáct	dvanáct	k4xCc4
strun	struna	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
existují	existovat	k5eAaImIp3nP
také	také	k9
verze	verze	k1gFnPc1
s	s	k7c7
21	#num#	k4
a	a	k8xC
více	hodně	k6eAd2
strunami	struna	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyvinul	vyvinout	k5eAaPmAgMnS
se	se	k3xPyFc4
z	z	k7c2
čínského	čínský	k2eAgInSc2d1
gužengu	guženg	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
příbuzný	příbuzný	k1gMnSc1
dalším	další	k2eAgInPc3d1
tradičním	tradiční	k2eAgInPc3d1
asijským	asijský	k2eAgInPc3d1
nástrojům	nástroj	k1gInPc3
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
japonské	japonský	k2eAgNnSc4d1
koto	koto	k1gNnSc4
<g/>
,	,	kIx,
mongolská	mongolský	k2eAgFnSc1d1
jatga	jatga	k1gFnSc1
či	či	k8xC
vietnamský	vietnamský	k2eAgInSc1d1
dantranh	dantranh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Podle	podle	k7c2
Samguk	Samguka	k1gFnPc2
Sagi	Sag	k1gFnSc2
(	(	kIx(
<g/>
삼	삼	k?
<g/>
,	,	kIx,
<g/>
三	三	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1145	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kroniky	kronika	k1gFnPc4
Tří	tři	k4xCgInPc2
království	království	k1gNnPc2
Koreje	Korea	k1gFnSc2
<g/>
,	,	kIx,
vymyslel	vymyslet	k5eAaPmAgMnS
kajagum	kajagum	k1gInSc4
v	v	k7c6
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
král	král	k1gMnSc1
Gasil	Gasil	k1gMnSc1
z	z	k7c2
kmenového	kmenový	k2eAgInSc2d1
svazu	svaz	k1gInSc2
Kaja	Kajum	k1gNnSc2
<g/>
,	,	kIx,
když	když	k8xS
si	se	k3xPyFc3
prohlížel	prohlížet	k5eAaImAgMnS
staré	starý	k2eAgInPc4d1
čínské	čínský	k2eAgInPc4d1
hudební	hudební	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nařídil	nařídit	k5eAaPmAgMnS
hudebníkovi	hudebník	k1gMnSc3
jménem	jméno	k1gNnSc7
Wu	Wu	k1gFnSc2
Ruk	ruka	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
složil	složit	k5eAaPmAgMnS
hudbu	hudba	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
šla	jít	k5eAaImAgFnS
na	na	k7c4
kajagum	kajagum	k1gInSc4
zahrát	zahrát	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wu	Wu	k1gFnSc1
Ruk	ruka	k1gFnPc2
pak	pak	k6eAd1
nástroj	nástroj	k1gInSc1
dále	daleko	k6eAd2
rozvíjel	rozvíjet	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Existují	existovat	k5eAaImIp3nP
dva	dva	k4xCgInPc1
základní	základní	k2eAgInPc1d1
typy	typ	k1gInPc1
kajagumu	kajagum	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
Typ	typ	k1gInSc1
beobgeum	beobgeum	k1gNnSc1
kajagum	kajagum	k1gInSc1
je	být	k5eAaImIp3nS
160	#num#	k4
<g/>
cm	cm	kA
dlouhý	dlouhý	k2eAgMnSc1d1
<g/>
,	,	kIx,
30	#num#	k4
<g/>
cm	cm	kA
široký	široký	k2eAgMnSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
10	#num#	k4
<g/>
cm	cm	kA
na	na	k7c4
výšku	výška	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tělo	tělo	k1gNnSc1
je	být	k5eAaImIp3nS
vyrobeno	vyrobit	k5eAaPmNgNnS
z	z	k7c2
jednoho	jeden	k4xCgInSc2
kusu	kus	k1gInSc2
paulovnie	paulovnie	k1gFnSc2
do	do	k7c2
něhož	jenž	k3xRgInSc2
je	být	k5eAaImIp3nS
vydlabaná	vydlabaný	k2eAgFnSc1d1
rezonanční	rezonanční	k2eAgFnSc1d1
komora	komora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Typ	typ	k1gInSc1
sanjo	sanjo	k6eAd1
kajagum	kajagum	k1gInSc1
je	být	k5eAaImIp3nS
okolo	okolo	k7c2
142	#num#	k4
<g/>
cm	cm	kA
dlouhý	dlouhý	k2eAgMnSc1d1
<g/>
,	,	kIx,
23	#num#	k4
<g/>
cm	cm	kA
široký	široký	k2eAgMnSc1d1
a	a	k8xC
na	na	k7c4
výšku	výška	k1gFnSc4
má	mít	k5eAaImIp3nS
10	#num#	k4
<g/>
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rezonanční	rezonanční	k2eAgFnSc1d1
deska	deska	k1gFnSc1
je	být	k5eAaImIp3nS
vytvořená	vytvořený	k2eAgFnSc1d1
z	z	k7c2
paulovnie	paulovnie	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
boky	bok	k1gInPc1
a	a	k8xC
zadní	zadní	k2eAgFnSc1d1
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
vyrobena	vyrobit	k5eAaPmNgFnS
z	z	k7c2
tvrdšího	tvrdý	k2eAgNnSc2d2
dřeva	dřevo	k1gNnSc2
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
kaštan	kaštan	k1gInSc1
nebo	nebo	k8xC
ořech	ořech	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Všechny	všechen	k3xTgInPc1
tradiční	tradiční	k2eAgInPc1d1
kajagumy	kajagum	k1gInPc1
používají	používat	k5eAaImIp3nP
hedvábné	hedvábný	k2eAgFnSc2d1
struny	struna	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
konce	konec	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
však	však	k9
někteří	některý	k3yIgMnPc1
hudebníci	hudebník	k1gMnPc1
začali	začít	k5eAaPmAgMnP
používat	používat	k5eAaImF
struny	struna	k1gFnPc4
nylonové	nylonový	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Gayageum	Gayageum	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Yoonjah	Yoonjah	k1gInSc1
Choi	Cho	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kayagŭ	Kayagŭ	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
