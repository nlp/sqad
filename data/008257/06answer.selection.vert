<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
(	(	kIx(	(
<g/>
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
<g/>
,	,	kIx,	,
Dolnorakouská	dolnorakouský	k2eAgFnSc1d1	Dolnorakouská
<g/>
)	)	kIx)	)
měrná	měrný	k2eAgFnSc1d1	měrná
soustava	soustava	k1gFnSc1	soustava
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
zavedena	zavést	k5eAaPmNgFnS	zavést
od	od	k7c2	od
1.1	[number]	k4	1.1
<g/>
.1765	.1765	k4	.1765
za	za	k7c4	za
panování	panování	k1gNnSc4	panování
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
a	a	k8xC	a
platila	platit	k5eAaImAgFnS	platit
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
zákonná	zákonný	k2eAgFnSc1d1	zákonná
soustava	soustava	k1gFnSc1	soustava
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Rakousku	Rakousko	k1gNnSc6	Rakousko
až	až	k9	až
do	do	k7c2	do
zavedení	zavedení	k1gNnSc2	zavedení
metrické	metrický	k2eAgFnSc2d1	metrická
soustavy	soustava	k1gFnSc2	soustava
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
<g/>
.	.	kIx.	.
</s>
