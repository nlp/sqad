<p>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
(	(	kIx(	(
<g/>
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
<g/>
,	,	kIx,	,
Dolnorakouská	dolnorakouský	k2eAgFnSc1d1	Dolnorakouská
<g/>
)	)	kIx)	)
měrná	měrný	k2eAgFnSc1d1	měrná
soustava	soustava	k1gFnSc1	soustava
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
zavedena	zavést	k5eAaPmNgFnS	zavést
od	od	k7c2	od
1.1	[number]	k4	1.1
<g/>
.1765	.1765	k4	.1765
za	za	k7c4	za
panování	panování	k1gNnSc4	panování
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
a	a	k8xC	a
platila	platit	k5eAaImAgFnS	platit
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
zákonná	zákonný	k2eAgFnSc1d1	zákonná
soustava	soustava	k1gFnSc1	soustava
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Rakousku	Rakousko	k1gNnSc6	Rakousko
až	až	k9	až
do	do	k7c2	do
zavedení	zavedení	k1gNnSc2	zavedení
metrické	metrický	k2eAgFnSc2d1	metrická
soustavy	soustava	k1gFnSc2	soustava
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jednotky	jednotka	k1gFnSc2	jednotka
délky	délka	k1gFnSc2	délka
==	==	k?	==
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
soustavy	soustava	k1gFnSc2	soustava
délek	délka	k1gFnPc2	délka
byl	být	k5eAaImAgInS	být
vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
sáh	sáh	k1gInSc1	sáh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
bod	bod	k1gInSc1	bod
=	=	kIx~	=
0,1829	[number]	k4	0,1829
mm	mm	kA	mm
</s>
</p>
<p>
<s>
1	[number]	k4	1
čárka	čárka	k1gFnSc1	čárka
=	=	kIx~	=
12	[number]	k4	12
bodů	bod	k1gInPc2	bod
=	=	kIx~	=
2,195	[number]	k4	2,195
mm	mm	kA	mm
</s>
</p>
<p>
<s>
1	[number]	k4	1
palec	palec	k1gInSc1	palec
=	=	kIx~	=
12	[number]	k4	12
čárek	čárka	k1gFnPc2	čárka
=	=	kIx~	=
2,634	[number]	k4	2,634
cm	cm	kA	cm
</s>
</p>
<p>
<s>
1	[number]	k4	1
střevíc	střevíc	k1gInSc1	střevíc
(	(	kIx(	(
<g/>
stopa	stopa	k1gFnSc1	stopa
<g/>
)	)	kIx)	)
=	=	kIx~	=
12	[number]	k4	12
palců	palec	k1gInPc2	palec
=	=	kIx~	=
31,608	[number]	k4	31,608
<g/>
1	[number]	k4	1
cm	cm	kA	cm
</s>
</p>
<p>
<s>
1	[number]	k4	1
vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
sáh	sáh	k1gInSc1	sáh
=	=	kIx~	=
6	[number]	k4	6
střevíců	střevíc	k1gInPc2	střevíc
=	=	kIx~	=
189,648	[number]	k4	189,648
<g/>
4	[number]	k4	4
cm	cm	kA	cm
</s>
</p>
<p>
<s>
1	[number]	k4	1
německá	německý	k2eAgFnSc1d1	německá
míle	míle	k1gFnSc1	míle
=	=	kIx~	=
2912	[number]	k4	2912
vídeňských	vídeňský	k2eAgInPc2d1	vídeňský
sáhů	sáh	k1gInPc2	sáh
=	=	kIx~	=
5	[number]	k4	5
522,561	[number]	k4	522,561
<g/>
408	[number]	k4	408
m	m	kA	m
</s>
</p>
<p>
<s>
1	[number]	k4	1
rakouská	rakouský	k2eAgFnSc1d1	rakouská
míle	míle	k1gFnSc1	míle
=	=	kIx~	=
4000	[number]	k4	4000
vídeňských	vídeňský	k2eAgInPc2d1	vídeňský
sáhů	sáh	k1gInPc2	sáh
=	=	kIx~	=
7	[number]	k4	7
585,936	[number]	k4	585,936
m	m	kA	m
</s>
</p>
<p>
<s>
==	==	k?	==
Jednotky	jednotka	k1gFnPc4	jednotka
plošné	plošný	k2eAgFnSc2d1	plošná
míry	míra	k1gFnSc2	míra
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
čtverečná	čtverečný	k2eAgFnSc1d1	čtverečná
stopa	stopa	k1gFnSc1	stopa
=	=	kIx~	=
0,099907	[number]	k4	0,099907
m2	m2	k4	m2
</s>
</p>
<p>
<s>
1	[number]	k4	1
čtverečný	čtverečný	k2eAgInSc1d1	čtverečný
sáh	sáh	k1gInSc1	sáh
=	=	kIx~	=
3,596	[number]	k4	3,596
<g/>
652	[number]	k4	652
m2	m2	k4	m2
</s>
</p>
<p>
<s>
1	[number]	k4	1
měřice	měřice	k1gFnSc1	měřice
=	=	kIx~	=
1	[number]	k4	1
918,21	[number]	k4	918,21
m2	m2	k4	m2
</s>
</p>
<p>
<s>
1	[number]	k4	1
korec	korec	k1gInSc1	korec
=	=	kIx~	=
2	[number]	k4	2
877,32	[number]	k4	877,32
m2	m2	k4	m2
</s>
</p>
<p>
<s>
1	[number]	k4	1
jitro	jitro	k1gNnSc1	jitro
=	=	kIx~	=
5	[number]	k4	5
754,64	[number]	k4	754,64
m2	m2	k4	m2
</s>
</p>
<p>
<s>
1	[number]	k4	1
řemenová	řemenový	k2eAgFnSc1d1	řemenová
tečka	tečka	k1gFnSc1	tečka
=	=	kIx~	=
0,000345233	[number]	k4	0,000345233
m2	m2	k4	m2
</s>
</p>
<p>
<s>
1	[number]	k4	1
řemenová	řemenový	k2eAgFnSc1d1	řemenová
čárka	čárka	k1gFnSc1	čárka
=	=	kIx~	=
0,00416279	[number]	k4	0,00416279
m2	m2	k4	m2
</s>
</p>
<p>
<s>
1	[number]	k4	1
řemenový	řemenový	k2eAgInSc1d1	řemenový
palec	palec	k1gInSc1	palec
=	=	kIx~	=
0,0499535	[number]	k4	0,0499535
m2	m2	k4	m2
</s>
</p>
<p>
<s>
1	[number]	k4	1
řemenová	řemenový	k2eAgFnSc1d1	řemenová
stopa	stopa	k1gFnSc1	stopa
=	=	kIx~	=
0,599	[number]	k4	0,599
442	[number]	k4	442
m2	m2	k4	m2
</s>
</p>
<p>
<s>
1	[number]	k4	1
řemenový	řemenový	k2eAgInSc1d1	řemenový
sáh	sáh	k1gInSc1	sáh
=	=	kIx~	=
3,596	[number]	k4	3,596
<g/>
652	[number]	k4	652
m2	m2	k4	m2
</s>
</p>
<p>
<s>
==	==	k?	==
Jednotky	jednotka	k1gFnSc2	jednotka
objemu	objem	k1gInSc2	objem
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
žejdlík	žejdlík	k1gInSc1	žejdlík
=	=	kIx~	=
0,000358	[number]	k4	0,000358
m3	m3	k4	m3
</s>
</p>
<p>
<s>
1	[number]	k4	1
holba	holba	k1gFnSc1	holba
=	=	kIx~	=
0,000707	[number]	k4	0,000707
m3	m3	k4	m3
</s>
</p>
<p>
<s>
1	[number]	k4	1
máz	máz	k1gInSc1	máz
=	=	kIx~	=
0,001415	[number]	k4	0,001415
m3	m3	k4	m3
</s>
</p>
<p>
<s>
1	[number]	k4	1
vědro	vědro	k1gNnSc1	vědro
=	=	kIx~	=
0,056589	[number]	k4	0,056589
m3	m3	k4	m3
</s>
</p>
<p>
<s>
1	[number]	k4	1
krychlová	krychlový	k2eAgFnSc1d1	krychlová
stopa	stopa	k1gFnSc1	stopa
=	=	kIx~	=
0,03157867	[number]	k4	0,03157867
m3	m3	k4	m3
</s>
</p>
<p>
<s>
1	[number]	k4	1
krychlový	krychlový	k2eAgInSc1d1	krychlový
sáh	sáh	k1gInSc1	sáh
=	=	kIx~	=
6,820	[number]	k4	6,820
<g/>
992	[number]	k4	992
m3	m3	k4	m3
</s>
</p>
<p>
<s>
==	==	k?	==
Jednotky	jednotka	k1gFnSc2	jednotka
hmotnosti	hmotnost	k1gFnSc2	hmotnost
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
karát	karát	k1gInSc1	karát
=	=	kIx~	=
0,000206	[number]	k4	0,000206
kg	kg	kA	kg
</s>
</p>
<p>
<s>
1	[number]	k4	1
denár	denár	k1gInSc1	denár
=	=	kIx~	=
0,001096	[number]	k4	0,001096
kg	kg	kA	kg
</s>
</p>
<p>
<s>
1	[number]	k4	1
kvintlík	kvintlík	k1gInSc1	kvintlík
=	=	kIx~	=
0,004375	[number]	k4	0,004375
kg	kg	kA	kg
</s>
</p>
<p>
<s>
1	[number]	k4	1
lot	lot	k1gInSc1	lot
=	=	kIx~	=
0,017501	[number]	k4	0,017501
kg	kg	kA	kg
</s>
</p>
<p>
<s>
1	[number]	k4	1
marka	marka	k1gFnSc1	marka
(	(	kIx(	(
<g/>
hřivna	hřivna	k1gFnSc1	hřivna
<g/>
)	)	kIx)	)
=	=	kIx~	=
0,280668	[number]	k4	0,280668
kg	kg	kA	kg
</s>
</p>
<p>
<s>
1	[number]	k4	1
lékárnická	lékárnický	k2eAgFnSc1d1	lékárnická
libra	libra	k1gFnSc1	libra
=	=	kIx~	=
0,420045	[number]	k4	0,420045
kg	kg	kA	kg
</s>
</p>
<p>
<s>
1	[number]	k4	1
celní	celní	k2eAgFnSc1d1	celní
libra	libra	k1gFnSc1	libra
=	=	kIx~	=
0,5	[number]	k4	0,5
kg	kg	kA	kg
</s>
</p>
<p>
<s>
1	[number]	k4	1
libra	libra	k1gFnSc1	libra
(	(	kIx(	(
<g/>
funt	funt	k1gInSc1	funt
<g/>
)	)	kIx)	)
=	=	kIx~	=
0,56006	[number]	k4	0,56006
kg	kg	kA	kg
</s>
</p>
<p>
<s>
1	[number]	k4	1
celní	celní	k2eAgInSc1d1	celní
cent	cent	k1gInSc1	cent
=	=	kIx~	=
50	[number]	k4	50
kg	kg	kA	kg
</s>
</p>
<p>
<s>
1	[number]	k4	1
cent	cent	k1gInSc1	cent
=	=	kIx~	=
56,006	[number]	k4	56,006
kg	kg	kA	kg
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
měrná	měrný	k2eAgFnSc1d1	měrná
soustava	soustava	k1gFnSc1	soustava
</s>
</p>
