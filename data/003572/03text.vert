<s>
Steven	Steven	k2eAgMnSc1d1	Steven
Paul	Paul	k1gMnSc1	Paul
Jobs	Jobsa	k1gFnPc2	Jobsa
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
pouze	pouze	k6eAd1	pouze
Steve	Steve	k1gMnSc1	Steve
Jobs	Jobsa	k1gFnPc2	Jobsa
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1955	[number]	k4	1955
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
Palo	Pala	k1gMnSc5	Pala
Alto	Alta	k1gMnSc5	Alta
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
zakladatel	zakladatel	k1gMnSc1	zakladatel
<g/>
,	,	kIx,	,
výkonný	výkonný	k2eAgMnSc1d1	výkonný
ředitel	ředitel	k1gMnSc1	ředitel
(	(	kIx(	(
<g/>
CEO	CEO	kA	CEO
<g/>
)	)	kIx)	)
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
firmy	firma	k1gFnSc2	firma
Apple	Apple	kA	Apple
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýraznějších	výrazný	k2eAgFnPc2d3	nejvýraznější
osobností	osobnost	k1gFnPc2	osobnost
počítačového	počítačový	k2eAgInSc2d1	počítačový
průmyslu	průmysl	k1gInSc2	průmysl
posledních	poslední	k2eAgNnPc2d1	poslední
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Stevem	Steve	k1gMnSc7	Steve
Wozniakem	Wozniak	k1gMnSc7	Wozniak
založil	založit	k5eAaPmAgMnS	založit
firmu	firma	k1gFnSc4	firma
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
osobních	osobní	k2eAgInPc2d1	osobní
počítačů	počítač	k1gInPc2	počítač
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgInS	založit
také	také	k9	také
firmu	firma	k1gFnSc4	firma
NeXT	NeXT	k1gFnSc2	NeXT
a	a	k8xC	a
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
i	i	k9	i
filmová	filmový	k2eAgNnPc4d1	filmové
studia	studio	k1gNnPc4	studio
Pixar	Pixara	k1gFnPc2	Pixara
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
americké	americký	k2eAgFnSc3d1	americká
vysokoškolačce	vysokoškolačka	k1gFnSc3	vysokoškolačka
a	a	k8xC	a
syrskému	syrský	k2eAgMnSc3d1	syrský
studentovi	student	k1gMnSc3	student
politologie	politologie	k1gFnSc2	politologie
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
adoptován	adoptovat	k5eAaPmNgInS	adoptovat
Paulem	Paul	k1gMnSc7	Paul
a	a	k8xC	a
Clarou	Clara	k1gFnSc7	Clara
Jobsovými	Jobsovi	k1gRnPc7	Jobsovi
<g/>
.	.	kIx.	.
</s>
<s>
Studia	studio	k1gNnPc1	studio
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
zanechal	zanechat	k5eAaPmAgMnS	zanechat
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
semestru	semestr	k1gInSc6	semestr
<g/>
;	;	kIx,	;
přednášky	přednáška	k1gFnPc4	přednáška
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
už	už	k6eAd1	už
si	se	k3xPyFc3	se
ale	ale	k8xC	ale
vybíral	vybírat	k5eAaImAgMnS	vybírat
čistě	čistě	k6eAd1	čistě
podle	podle	k7c2	podle
vlastní	vlastní	k2eAgFnSc2d1	vlastní
volby	volba	k1gFnSc2	volba
<g/>
,	,	kIx,	,
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
a	a	k8xC	a
půl	půl	k1xP	půl
<g/>
.	.	kIx.	.
</s>
<s>
Spával	spávat	k5eAaImAgMnS	spávat
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
po	po	k7c6	po
pokojích	pokoj	k1gInPc6	pokoj
svých	svůj	k3xOyFgMnPc2	svůj
kamarádů	kamarád	k1gMnPc2	kamarád
a	a	k8xC	a
na	na	k7c4	na
jídlo	jídlo	k1gNnSc4	jídlo
si	se	k3xPyFc3	se
vydělával	vydělávat	k5eAaImAgInS	vydělávat
vracením	vracení	k1gNnSc7	vracení
lahví	lahev	k1gFnPc2	lahev
od	od	k7c2	od
coly	cola	k1gFnSc2	cola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
bylo	být	k5eAaImAgNnS	být
jídlo	jídlo	k1gNnSc1	jídlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
každou	každý	k3xTgFnSc4	každý
neděli	neděle	k1gFnSc4	neděle
rozdávalo	rozdávat	k5eAaImAgNnS	rozdávat
hnutí	hnutí	k1gNnSc4	hnutí
Hare	Hare	k1gFnPc2	Hare
Krišna	Krišna	k1gMnSc1	Krišna
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
jediným	jediný	k2eAgNnSc7d1	jediné
pořádným	pořádný	k2eAgNnSc7d1	pořádné
jídlem	jídlo	k1gNnSc7	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
jeho	jeho	k3xOp3gFnPc2	jeho
přítelkyní	přítelkyně	k1gFnPc2	přítelkyně
písničkářka	písničkářka	k1gFnSc1	písničkářka
Joan	Joan	k1gInSc1	Joan
Baez	Baez	k1gInSc1	Baez
<g/>
.	.	kIx.	.
</s>
<s>
Oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
Laurene	Lauren	k1gInSc5	Lauren
Powellovou	Powellův	k2eAgFnSc7d1	Powellova
(	(	kIx(	(
<g/>
tři	tři	k4xCgFnPc1	tři
děti	dítě	k1gFnPc1	dítě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
měl	mít	k5eAaImAgMnS	mít
dceru	dcera	k1gFnSc4	dcera
z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
vztahu	vztah	k1gInSc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
typickým	typický	k2eAgNnSc7d1	typické
oblečením	oblečení	k1gNnSc7	oblečení
byl	být	k5eAaImAgInS	být
černý	černý	k2eAgInSc1d1	černý
rolák	rolák	k1gInSc1	rolák
St.	st.	kA	st.
Croix	Croix	k1gInSc1	Croix
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
rukávem	rukáv	k1gInSc7	rukáv
<g/>
,	,	kIx,	,
modré	modrý	k2eAgInPc4d1	modrý
džíny	džíny	k1gInPc4	džíny
Levi	Lev	k1gFnSc2	Lev
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
501	[number]	k4	501
a	a	k8xC	a
tenisky	teniska	k1gFnPc1	teniska
New	New	k1gFnSc2	New
Balance	balanc	k1gInSc2	balanc
992	[number]	k4	992
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
rozpoznali	rozpoznat	k5eAaPmAgMnP	rozpoznat
potenciál	potenciál	k1gInSc4	potenciál
myší	myš	k1gFnPc2	myš
ovládaného	ovládaný	k2eAgNnSc2d1	ovládané
grafického	grafický	k2eAgNnSc2d1	grafické
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
rozhraní	rozhraní	k1gNnSc2	rozhraní
vyvíjeného	vyvíjený	k2eAgNnSc2d1	vyvíjené
v	v	k7c6	v
laboratořích	laboratoř	k1gFnPc6	laboratoř
Palo	Pala	k1gMnSc5	Pala
Alto	Alta	k1gFnSc5	Alta
Research	Research	k1gInSc1	Research
Center	centrum	k1gNnPc2	centrum
firmy	firma	k1gFnSc2	firma
Xerox	Xerox	kA	Xerox
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
divize	divize	k1gFnSc2	divize
Macintosh	Macintosh	kA	Macintosh
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
uvedl	uvést	k5eAaPmAgMnS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
počítač	počítač	k1gInSc1	počítač
ovládaný	ovládaný	k2eAgInSc1d1	ovládaný
pomoci	pomoc	k1gFnSc3	pomoc
myši	myš	k1gFnSc2	myš
a	a	k8xC	a
menu	menu	k1gNnSc2	menu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
byl	být	k5eAaImAgMnS	být
vyhozen	vyhodit	k5eAaPmNgMnS	vyhodit
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
a	a	k8xC	a
založil	založit	k5eAaPmAgInS	založit
technologicky	technologicky	k6eAd1	technologicky
vyspělou	vyspělý	k2eAgFnSc7d1	vyspělá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
komerčně	komerčně	k6eAd1	komerčně
nepříliš	příliš	k6eNd1	příliš
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
společnost	společnost	k1gFnSc4	společnost
NeXT	NeXT	k1gFnSc2	NeXT
Computer	computer	k1gInSc1	computer
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
odkoupil	odkoupit	k5eAaPmAgInS	odkoupit
stagnující	stagnující	k2eAgInSc1d1	stagnující
Apple	Apple	kA	Apple
Computer	computer	k1gInSc1	computer
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
jeho	on	k3xPp3gNnSc2	on
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
NeXTSTEP	NeXTSTEP	k1gFnSc2	NeXTSTEP
firmy	firma	k1gFnSc2	firma
NeXT	NeXT	k1gFnSc1	NeXT
Computer	computer	k1gInSc1	computer
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
základem	základ	k1gInSc7	základ
nového	nový	k2eAgInSc2d1	nový
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
Mac	Mac	kA	Mac
OS	OS	kA	OS
X.	X.	kA	X.
Byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
většinovým	většinový	k2eAgMnSc7d1	většinový
majitelem	majitel	k1gMnSc7	majitel
animačního	animační	k2eAgNnSc2d1	animační
studia	studio	k1gNnSc2	studio
Pixar	Pixara	k1gFnPc2	Pixara
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
koupil	koupit	k5eAaPmAgMnS	koupit
od	od	k7c2	od
George	George	k1gFnPc2	George
Lucase	Lucasa	k1gFnSc3	Lucasa
za	za	k7c2	za
necelých	celý	k2eNgInPc2d1	necelý
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
po	po	k7c4	po
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
dvorním	dvorní	k2eAgMnSc7d1	dvorní
dodavatelem	dodavatel	k1gMnSc7	dodavatel
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
Disney	Disnea	k1gFnSc2	Disnea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2006	[number]	k4	2006
Disney	Disnea	k1gFnSc2	Disnea
odkoupil	odkoupit	k5eAaPmAgInS	odkoupit
Pixar	Pixar	k1gInSc1	Pixar
za	za	k7c4	za
7,4	[number]	k4	7,4
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Steve	Steve	k1gMnSc1	Steve
Jobs	Jobs	k1gInSc1	Jobs
tím	ten	k3xDgNnSc7	ten
získal	získat	k5eAaPmAgInS	získat
7	[number]	k4	7
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
Disney	Disnea	k1gFnSc2	Disnea
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
představenstvu	představenstvo	k1gNnSc6	představenstvo
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
známý	známý	k2eAgInSc1d1	známý
svým	svůj	k3xOyFgInSc7	svůj
manažerským	manažerský	k2eAgInSc7d1	manažerský
stylem	styl	k1gInSc7	styl
–	–	k?	–
často	často	k6eAd1	často
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
říkalo	říkat	k5eAaImAgNnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
terorizuje	terorizovat	k5eAaImIp3nS	terorizovat
své	svůj	k3xOyFgMnPc4	svůj
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
<g/>
,	,	kIx,	,
vyhazuje	vyhazovat	k5eAaImIp3nS	vyhazovat
z	z	k7c2	z
firmy	firma	k1gFnSc2	firma
za	za	k7c4	za
drobnosti	drobnost	k1gFnPc4	drobnost
a	a	k8xC	a
přenáší	přenášet	k5eAaImIp3nP	přenášet
příliš	příliš	k6eAd1	příliš
pozornosti	pozornost	k1gFnPc1	pozornost
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
byl	být	k5eAaImAgMnS	být
také	také	k9	také
znám	znám	k2eAgMnSc1d1	znám
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
řečnické	řečnický	k2eAgFnPc4d1	řečnická
schopnosti	schopnost	k1gFnPc4	schopnost
a	a	k8xC	a
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokázal	dokázat	k5eAaPmAgMnS	dokázat
mít	mít	k5eAaImF	mít
vizi	vize	k1gFnSc4	vize
a	a	k8xC	a
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
obchodní	obchodní	k2eAgFnSc2d1	obchodní
strategie	strategie	k1gFnSc2	strategie
Steva	Steve	k1gMnSc2	Steve
Jobse	Jobs	k1gMnSc2	Jobs
stál	stát	k5eAaImAgInS	stát
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokázal	dokázat	k5eAaPmAgInS	dokázat
vycítit	vycítit	k5eAaPmF	vycítit
aktuální	aktuální	k2eAgFnPc4d1	aktuální
potřeby	potřeba	k1gFnPc4	potřeba
trhu	trh	k1gInSc2	trh
<g/>
,	,	kIx,	,
klást	klást	k5eAaImF	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
jednoduchost	jednoduchost	k1gFnSc4	jednoduchost
použití	použití	k1gNnSc2	použití
a	a	k8xC	a
nepodceňovat	podceňovat	k5eNaImF	podceňovat
design	design	k1gInSc4	design
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yRgInSc4	který
si	se	k3xPyFc3	se
zájemci	zájemce	k1gMnPc1	zájemce
rádi	rád	k2eAgMnPc1d1	rád
byli	být	k5eAaImAgMnP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
připlatit	připlatit	k5eAaPmF	připlatit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jim	on	k3xPp3gMnPc3	on
dával	dávat	k5eAaImAgInS	dávat
pocit	pocit	k1gInSc4	pocit
výjimečnosti	výjimečnost	k1gFnSc2	výjimečnost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gNnSc2	jeho
vedení	vedení	k1gNnSc2	vedení
získala	získat	k5eAaPmAgFnS	získat
struktura	struktura	k1gFnSc1	struktura
Applu	Appl	k1gInSc2	Appl
podobu	podoba	k1gFnSc4	podoba
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Jobs	Jobs	k1gInSc4	Jobs
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gInSc7	jeho
centrem	centr	k1gInSc7	centr
a	a	k8xC	a
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
oddělení	oddělení	k1gNnPc1	oddělení
a	a	k8xC	a
manažeři	manažer	k1gMnPc1	manažer
paprsky	paprsek	k1gInPc4	paprsek
tohoto	tento	k3xDgNnSc2	tento
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Steve	Steve	k1gMnSc1	Steve
Jobs	Jobsa	k1gFnPc2	Jobsa
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgInS	chtít
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
všem	všecek	k3xTgNnSc6	všecek
<g/>
,	,	kIx,	,
mít	mít	k5eAaImF	mít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
každým	každý	k3xTgInSc7	každý
procesem	proces	k1gInSc7	proces
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
měl	mít	k5eAaImAgMnS	mít
roli	role	k1gFnSc4	role
nesrovnatelnou	srovnatelný	k2eNgFnSc4d1	nesrovnatelná
s	s	k7c7	s
jinými	jiná	k1gFnPc7	jiná
CEO	CEO	kA	CEO
<g/>
,	,	kIx,	,
a	a	k8xC	a
analytici	analytik	k1gMnPc1	analytik
proto	proto	k8xC	proto
poukazovali	poukazovat	k5eAaImAgMnP	poukazovat
na	na	k7c4	na
provázanost	provázanost	k1gFnSc4	provázanost
jeho	jeho	k3xOp3gFnSc2	jeho
osobnosti	osobnost	k1gFnSc2	osobnost
s	s	k7c7	s
Applem	Applo	k1gNnSc7	Applo
samotným	samotný	k2eAgNnSc7d1	samotné
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
plat	plat	k1gInSc1	plat
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
činil	činit	k5eAaImAgInS	činit
pouhý	pouhý	k2eAgInSc4d1	pouhý
jeden	jeden	k4xCgInSc4	jeden
dolar	dolar	k1gInSc4	dolar
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
akciím	akcie	k1gFnPc3	akcie
společností	společnost	k1gFnPc2	společnost
Apple	Apple	kA	Apple
a	a	k8xC	a
Disney	Disney	k1gInPc1	Disney
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
<g/>
,	,	kIx,	,
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
nejbohatší	bohatý	k2eAgMnPc4d3	nejbohatší
Američany	Američan	k1gMnPc4	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Forbes	forbes	k1gInSc1	forbes
odhadoval	odhadovat	k5eAaImAgInS	odhadovat
jeho	jeho	k3xOp3gNnSc4	jeho
jmění	jmění	k1gNnSc4	jmění
na	na	k7c4	na
8,3	[number]	k4	8,3
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
úspěchu	úspěch	k1gInSc3	úspěch
modelů	model	k1gInPc2	model
počítače	počítač	k1gInSc2	počítač
iMac	iMac	k1gFnSc4	iMac
<g/>
,	,	kIx,	,
přehrávače	přehrávač	k1gInPc4	přehrávač
iPod	iPoda	k1gFnPc2	iPoda
<g/>
,	,	kIx,	,
mobilního	mobilní	k2eAgInSc2d1	mobilní
telefonu	telefon	k1gInSc2	telefon
iPhone	iPhon	k1gInSc5	iPhon
a	a	k8xC	a
tabletu	tablet	k1gInSc6	tablet
iPad	iPada	k1gFnPc2	iPada
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
Apple	Apple	kA	Apple
stala	stát	k5eAaPmAgFnS	stát
největší	veliký	k2eAgFnSc7d3	veliký
světovou	světový	k2eAgFnSc7d1	světová
firmou	firma	k1gFnSc7	firma
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
oznámil	oznámit	k5eAaPmAgMnS	oznámit
rezignaci	rezignace	k1gFnSc4	rezignace
na	na	k7c6	na
postu	post	k1gInSc6	post
šéfa	šéf	k1gMnSc2	šéf
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gInSc4	on
Tim	Tim	k?	Tim
Cook	Cook	k1gInSc1	Cook
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
Jobsovi	Jobsův	k2eAgMnPc1d1	Jobsův
diagnostikována	diagnostikovat	k5eAaBmNgFnS	diagnostikovat
rakovina	rakovina	k1gFnSc1	rakovina
slinivky	slinivka	k1gFnSc2	slinivka
břišní	břišní	k2eAgFnSc2d1	břišní
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
její	její	k3xOp3gFnSc1	její
vzácná	vzácný	k2eAgFnSc1d1	vzácná
forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
chirurgicky	chirurgicky	k6eAd1	chirurgicky
léčitelná	léčitelný	k2eAgFnSc1d1	léčitelná
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
řekl	říct	k5eAaPmAgMnS	říct
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
slavném	slavný	k2eAgInSc6d1	slavný
Stanfordském	Stanfordský	k2eAgInSc6d1	Stanfordský
projevu	projev	k1gInSc6	projev
<g/>
.	.	kIx.	.
</s>
<s>
Jobs	Jobs	k6eAd1	Jobs
nicméně	nicméně	k8xC	nicméně
po	po	k7c4	po
9	[number]	k4	9
měsíců	měsíc	k1gInPc2	měsíc
chirurgický	chirurgický	k2eAgInSc1d1	chirurgický
zákrok	zákrok	k1gInSc1	zákrok
odkládal	odkládat	k5eAaImAgInS	odkládat
a	a	k8xC	a
s	s	k7c7	s
rakovinou	rakovina	k1gFnSc7	rakovina
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
bojovat	bojovat	k5eAaImF	bojovat
pomocí	pomocí	k7c2	pomocí
postupů	postup	k1gInPc2	postup
alternativní	alternativní	k2eAgFnSc2d1	alternativní
medicíny	medicína	k1gFnSc2	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc4	jeho
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgNnSc2	svůj
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
litoval	litovat	k5eAaImAgMnS	litovat
<g/>
,	,	kIx,	,
uvedl	uvést	k5eAaPmAgMnS	uvést
jeho	jeho	k3xOp3gMnSc1	jeho
životopisec	životopisec	k1gMnSc1	životopisec
Walter	Walter	k1gMnSc1	Walter
Isaacson	Isaacson	k1gMnSc1	Isaacson
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
společnost	společnost	k1gFnSc1	společnost
Bloomberg	Bloomberg	k1gInSc4	Bloomberg
omylem	omylem	k6eAd1	omylem
publikovala	publikovat	k5eAaBmAgFnS	publikovat
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
Jobsově	Jobsův	k2eAgFnSc6d1	Jobsova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
chybu	chyba	k1gFnSc4	chyba
rychle	rychle	k6eAd1	rychle
napravila	napravit	k5eAaPmAgFnS	napravit
<g/>
,	,	kIx,	,
zpráva	zpráva	k1gFnSc1	zpráva
se	se	k3xPyFc4	se
rozběhla	rozběhnout	k5eAaPmAgFnS	rozběhnout
do	do	k7c2	do
světa	svět	k1gInSc2	svět
a	a	k8xC	a
posílila	posílit	k5eAaPmAgFnS	posílit
spekulace	spekulace	k1gFnPc4	spekulace
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
chatrném	chatrný	k2eAgNnSc6d1	chatrné
zdraví	zdraví	k1gNnSc6	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Jobs	Jobs	k6eAd1	Jobs
si	se	k3xPyFc3	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
situaci	situace	k1gFnSc6	situace
vypůjčil	vypůjčit	k5eAaPmAgMnS	vypůjčit
citát	citát	k1gInSc4	citát
Marka	Marek	k1gMnSc2	Marek
Twaina	Twain	k1gMnSc2	Twain
a	a	k8xC	a
na	na	k7c4	na
akci	akce	k1gFnSc4	akce
Applu	Appl	k1gInSc2	Appl
publiku	publikum	k1gNnSc6	publikum
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
mé	můj	k3xOp1gFnSc6	můj
smrti	smrt	k1gFnSc6	smrt
nesmírně	smírně	k6eNd1	smírně
přehánějí	přehánět	k5eAaImIp3nP	přehánět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
pozastavil	pozastavit	k5eAaPmAgInS	pozastavit
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
výkon	výkon	k1gInSc4	výkon
funkce	funkce	k1gFnSc2	funkce
ředitele	ředitel	k1gMnSc2	ředitel
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
podstoupil	podstoupit	k5eAaPmAgInS	podstoupit
transplantaci	transplantace	k1gFnSc4	transplantace
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Pankreatický	pankreatický	k2eAgInSc1d1	pankreatický
nádor	nádor	k1gInSc1	nádor
nicméně	nicméně	k8xC	nicméně
metastazoval	metastazovat	k5eAaImAgInS	metastazovat
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostřední	bezprostřední	k2eAgFnSc7d1	bezprostřední
příčinou	příčina	k1gFnSc7	příčina
smrti	smrt	k1gFnSc2	smrt
byla	být	k5eAaImAgFnS	být
zástava	zástava	k1gFnSc1	zástava
dechu	dech	k1gInSc2	dech
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
právě	právě	k9	právě
rozšíření	rozšíření	k1gNnSc1	rozšíření
rakoviny	rakovina	k1gFnSc2	rakovina
ze	z	k7c2	z
slinivky	slinivka	k1gFnSc2	slinivka
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Jobs	Jobs	k1gInSc1	Jobs
své	svůj	k3xOyFgFnSc2	svůj
nemoci	nemoc	k1gFnSc2	nemoc
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Jobs	Jobs	k6eAd1	Jobs
a	a	k8xC	a
Steve	Steve	k1gMnSc1	Steve
Wozniak	Wozniak	k1gMnSc1	Wozniak
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
setkali	setkat	k5eAaPmAgMnP	setkat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
představil	představit	k5eAaPmAgMnS	představit
jejich	jejich	k3xOp3gNnSc4	jejich
společný	společný	k2eAgMnSc1d1	společný
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
Bill	Bill	k1gMnSc1	Bill
Fernandez	Fernandez	k1gMnSc1	Fernandez
<g/>
.	.	kIx.	.
</s>
<s>
Wozniakovi	Wozniak	k1gMnSc3	Wozniak
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Jobsovi	Jobsův	k2eAgMnPc1d1	Jobsův
bylo	být	k5eAaImAgNnS	být
pouhých	pouhý	k2eAgInPc2d1	pouhý
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
Wozniak	Wozniak	k1gMnSc1	Wozniak
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
počítač	počítač	k1gInSc4	počítač
Apple	Apple	kA	Apple
I.	I.	kA	I.
Jobs	Jobs	k1gInSc1	Jobs
<g/>
,	,	kIx,	,
Wozniak	Wozniak	k1gMnSc1	Wozniak
a	a	k8xC	a
Ronald	Ronald	k1gMnSc1	Ronald
Wayne	Wayn	k1gInSc5	Wayn
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
garáži	garáž	k1gFnSc6	garáž
Jobsových	Jobsův	k2eAgMnPc2d1	Jobsův
rodičů	rodič	k1gMnPc2	rodič
společnost	společnost	k1gFnSc1	společnost
Apple	Apple	kA	Apple
Computer	computer	k1gInSc1	computer
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
svůj	svůj	k3xOyFgInSc4	svůj
počítač	počítač	k1gInSc4	počítač
mohli	moct	k5eAaImAgMnP	moct
prodávat	prodávat	k5eAaImF	prodávat
<g/>
.	.	kIx.	.
</s>
<s>
Financoval	financovat	k5eAaBmAgMnS	financovat
je	být	k5eAaImIp3nS	být
tehdy	tehdy	k6eAd1	tehdy
napůl	napůl	k6eAd1	napůl
vysloužilý	vysloužilý	k2eAgMnSc1d1	vysloužilý
manažer	manažer	k1gMnSc1	manažer
produktového	produktový	k2eAgInSc2d1	produktový
marketingu	marketing	k1gInSc2	marketing
a	a	k8xC	a
inženýr	inženýr	k1gMnSc1	inženýr
Intelu	Intel	k1gInSc2	Intel
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
osobě	osoba	k1gFnSc6	osoba
<g/>
,	,	kIx,	,
Mike	Mike	k1gFnSc6	Mike
Markkula	Markkulum	k1gNnSc2	Markkulum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
Apple	Apple	kA	Apple
rekrutoval	rekrutovat	k5eAaImAgMnS	rekrutovat
Michaela	Michael	k1gMnSc2	Michael
Scotta	Scotta	k1gMnSc1	Scotta
z	z	k7c2	z
National	National	k1gFnSc2	National
Semiconductor	Semiconductor	k1gInSc4	Semiconductor
na	na	k7c4	na
post	post	k1gInSc4	post
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
několik	několik	k4yIc4	několik
divokých	divoký	k2eAgNnPc2d1	divoké
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
se	se	k3xPyFc4	se
Jobs	Jobs	k1gInSc1	Jobs
snažil	snažit	k5eAaImAgInS	snažit
získat	získat	k5eAaPmF	získat
Johna	John	k1gMnSc4	John
Sculleyho	Sculley	k1gMnSc4	Sculley
z	z	k7c2	z
firmy	firma	k1gFnSc2	firma
Pepsi-Cola	Pepsi-Cola	k1gFnSc1	Pepsi-Cola
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
u	u	k7c2	u
Applu	Appl	k1gInSc2	Appl
pracoval	pracovat	k5eAaImAgInS	pracovat
jako	jako	k8xC	jako
CEO	CEO	kA	CEO
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
lanaření	lanaření	k1gNnSc2	lanaření
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc4	on
Jobs	Jobs	k1gInSc4	Jobs
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
zeptal	zeptat	k5eAaPmAgMnS	zeptat
známou	známý	k2eAgFnSc7d1	známá
otázkou	otázka	k1gFnSc7	otázka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chcete	chtít	k5eAaImIp2nP	chtít
prodávat	prodávat	k5eAaImF	prodávat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
sladkou	sladký	k2eAgFnSc4d1	sladká
vodu	voda	k1gFnSc4	voda
<g/>
?	?	kIx.	?
</s>
<s>
Nebo	nebo	k8xC	nebo
chcete	chtít	k5eAaImIp2nP	chtít
pracovat	pracovat	k5eAaImF	pracovat
se	s	k7c7	s
mnou	já	k3xPp1nSc7	já
a	a	k8xC	a
pomoct	pomoct	k5eAaPmF	pomoct
mi	já	k3xPp1nSc3	já
změnit	změnit	k5eAaPmF	změnit
svět	svět	k1gInSc1	svět
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
spatřil	spatřit	k5eAaPmAgMnS	spatřit
Jobs	Jobs	k1gInSc4	Jobs
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
komerční	komerční	k2eAgInSc4d1	komerční
potenciál	potenciál	k1gInSc4	potenciál
v	v	k7c6	v
myší	myší	k2eAgFnSc6d1	myší
ovládaném	ovládaný	k2eAgNnSc6d1	ovládané
grafickém	grafický	k2eAgNnSc6d1	grafické
uživatelském	uživatelský	k2eAgNnSc6d1	Uživatelské
rozhraní	rozhraní	k1gNnSc6	rozhraní
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyvíjelo	vyvíjet	k5eAaImAgNnS	vyvíjet
Palo	Pala	k1gMnSc5	Pala
Alto	Alta	k1gFnSc5	Alta
Research	Research	k1gInSc1	Research
Center	centrum	k1gNnPc2	centrum
firmy	firma	k1gFnSc2	firma
Xerox	Xerox	kA	Xerox
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
počítače	počítač	k1gInSc2	počítač
Apple	Apple	kA	Apple
Lisa	Lisa	k1gFnSc1	Lisa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
Applu	Appl	k1gInSc2	Appl
Jef	Jef	k1gMnSc1	Jef
Raskin	Raskin	k1gMnSc1	Raskin
přivedl	přivést	k5eAaPmAgMnS	přivést
na	na	k7c4	na
svět	svět	k1gInSc4	svět
Macintosh	Macintosh	kA	Macintosh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
Apple	Apple	kA	Apple
během	během	k7c2	během
vysílání	vysílání	k1gNnSc2	vysílání
Super	super	k2eAgInSc2d1	super
Bowlu	Bowl	k1gInSc2	Bowl
odvysílal	odvysílat	k5eAaPmAgMnS	odvysílat
televizní	televizní	k2eAgFnSc4d1	televizní
reklamu	reklama	k1gFnSc4	reklama
evokující	evokující	k2eAgInSc4d1	evokující
román	román	k1gInSc4	román
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1984	[number]	k4	1984
na	na	k7c6	na
výroční	výroční	k2eAgFnSc6d1	výroční
valné	valný	k2eAgFnSc6d1	valná
hromadě	hromada	k1gFnSc6	hromada
představil	představit	k5eAaPmAgInS	představit
Jobs	Jobs	k1gInSc1	Jobs
velice	velice	k6eAd1	velice
emocionálním	emocionální	k2eAgInSc7d1	emocionální
způsobem	způsob	k1gInSc7	způsob
počítač	počítač	k1gInSc1	počítač
Macintosh	Macintosh	kA	Macintosh
<g/>
.	.	kIx.	.
</s>
<s>
Andy	Anda	k1gFnPc1	Anda
Hertzfeld	Hertzfelda	k1gFnPc2	Hertzfelda
popsal	popsat	k5eAaPmAgMnS	popsat
tuto	tento	k3xDgFnSc4	tento
scénu	scéna	k1gFnSc4	scéna
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
vřavu	vřava	k1gFnSc4	vřava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Jobs	Jobs	k1gInSc1	Jobs
byl	být	k5eAaImAgInS	být
přesvědčivý	přesvědčivý	k2eAgInSc1d1	přesvědčivý
a	a	k8xC	a
charismatický	charismatický	k2eAgMnSc1d1	charismatický
ředitel	ředitel	k1gMnSc1	ředitel
Applu	Appl	k1gInSc2	Appl
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
jeho	jeho	k3xOp3gMnPc1	jeho
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
ho	on	k3xPp3gNnSc4	on
měli	mít	k5eAaImAgMnP	mít
za	za	k7c4	za
excentrického	excentrický	k2eAgMnSc4d1	excentrický
náladového	náladový	k2eAgMnSc4d1	náladový
manažera	manažer	k1gMnSc4	manažer
<g/>
.	.	kIx.	.
</s>
<s>
Neuspokojivé	uspokojivý	k2eNgInPc1d1	neuspokojivý
prodeje	prodej	k1gInPc1	prodej
způsobily	způsobit	k5eAaPmAgInP	způsobit
zhoršení	zhoršení	k1gNnSc4	zhoršení
pracovního	pracovní	k2eAgInSc2d1	pracovní
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
Jobsem	Jobs	k1gInSc7	Jobs
a	a	k8xC	a
Sculleym	Sculleym	k1gInSc4	Sculleym
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
stal	stát	k5eAaPmAgInS	stát
boj	boj	k1gInSc1	boj
o	o	k7c4	o
pracovní	pracovní	k2eAgNnSc4d1	pracovní
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
Jobsem	Jobs	k1gInSc7	Jobs
a	a	k8xC	a
Sculleym	Sculleym	k1gInSc4	Sculleym
<g/>
.	.	kIx.	.
</s>
<s>
Sculley	Sculle	k1gMnPc4	Sculle
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolupráce	spolupráce	k1gFnSc1	spolupráce
Jobs	Jobs	k1gInSc1	Jobs
–	–	k?	–
Sculley	Scullea	k1gFnSc2	Scullea
není	být	k5eNaImIp3nS	být
již	již	k6eAd1	již
nadále	nadále	k6eAd1	nadále
pro	pro	k7c4	pro
Apple	Apple	kA	Apple
vhodná	vhodný	k2eAgFnSc1d1	vhodná
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
v	v	k7c6	v
zasedací	zasedací	k2eAgFnSc6d1	zasedací
místnosti	místnost	k1gFnSc6	místnost
o	o	k7c4	o
jakýsi	jakýsi	k3yIgInSc4	jakýsi
převrat	převrat	k1gInSc4	převrat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1985	[number]	k4	1985
svolal	svolat	k5eAaPmAgMnS	svolat
schůzi	schůze	k1gFnSc4	schůze
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
situace	situace	k1gFnSc1	situace
vyřešit	vyřešit	k5eAaPmF	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Představenstvo	představenstvo	k1gNnSc1	představenstvo
Applu	Appl	k1gInSc2	Appl
sdílelo	sdílet	k5eAaImAgNnS	sdílet
názor	názor	k1gInSc4	názor
se	se	k3xPyFc4	se
Sculleym	Sculleym	k1gInSc1	Sculleym
a	a	k8xC	a
zakázalo	zakázat	k5eAaPmAgNnS	zakázat
Jobsovi	Jobs	k1gMnSc3	Jobs
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
manažerských	manažerský	k2eAgInPc6d1	manažerský
úkolech	úkol	k1gInPc6	úkol
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
divize	divize	k1gFnSc2	divize
Macintosh	Macintosh	kA	Macintosh
<g/>
.	.	kIx.	.
</s>
<s>
Jobs	Jobs	k1gInSc1	Jobs
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
pět	pět	k4xCc4	pět
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
vyhozen	vyhodit	k5eAaPmNgInS	vyhodit
z	z	k7c2	z
Applu	Appl	k1gInSc2	Appl
a	a	k8xC	a
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
založil	založit	k5eAaPmAgMnS	založit
NeXT	NeXT	k1gMnSc1	NeXT
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
Jobs	Jobsa	k1gFnPc2	Jobsa
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
na	na	k7c6	na
Stanfordově	Stanfordův	k2eAgFnSc6d1	Stanfordova
univerzitě	univerzita	k1gFnSc6	univerzita
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
odchod	odchod	k1gInSc1	odchod
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
mohlo	moct	k5eAaImAgNnS	moct
stát	stát	k5eAaImF	stát
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Tíhu	tíha	k1gFnSc4	tíha
úspěchu	úspěch	k1gInSc2	úspěch
nahradila	nahradit	k5eAaPmAgFnS	nahradit
lehkost	lehkost	k1gFnSc1	lehkost
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
znovu	znovu	k6eAd1	znovu
začátečník	začátečník	k1gMnSc1	začátečník
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
si	se	k3xPyFc3	se
vším	všecek	k3xTgNnSc7	všecek
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
.	.	kIx.	.
</s>
<s>
Dala	dát	k5eAaPmAgFnS	dát
mi	já	k3xPp1nSc3	já
svobodu	svoboda	k1gFnSc4	svoboda
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nejkreativnějších	kreativní	k2eAgFnPc2d3	nejkreativnější
etap	etapa	k1gFnPc2	etapa
mého	můj	k3xOp1gInSc2	můj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
A	a	k9	a
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
,	,	kIx,	,
že	že	k8xS	že
nic	nic	k3yNnSc1	nic
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
,	,	kIx,	,
kdybych	kdyby	kYmCp1nS	kdyby
nebyl	být	k5eNaImAgInS	být
vyhozen	vyhodit	k5eAaPmNgInS	vyhodit
z	z	k7c2	z
Applu	Appl	k1gInSc2	Appl
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
odporně	odporně	k6eAd1	odporně
hořká	hořký	k2eAgFnSc1d1	hořká
pilulka	pilulka	k1gFnSc1	pilulka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
řekl	říct	k5eAaPmAgMnS	říct
bych	by	kYmCp1nS	by
<g/>
,	,	kIx,	,
že	že	k8xS	že
pacient	pacient	k1gMnSc1	pacient
ji	on	k3xPp3gFnSc4	on
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
<s>
Po	po	k7c6	po
vyhazovu	vyhazov	k1gInSc6	vyhazov
z	z	k7c2	z
Applu	Appl	k1gInSc2	Appl
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
založil	založit	k5eAaPmAgInS	založit
Jobs	Jobs	k1gInSc1	Jobs
společnost	společnost	k1gFnSc1	společnost
NeXT	NeXT	k1gFnSc1	NeXT
Computer	computer	k1gInSc1	computer
s	s	k7c7	s
kapitálem	kapitál	k1gInSc7	kapitál
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
Jobsovi	Jobsův	k2eAgMnPc1d1	Jobsův
došly	dojít	k5eAaPmAgInP	dojít
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
bez	bez	k7c2	bez
jediného	jediný	k2eAgInSc2d1	jediný
produktu	produkt	k1gInSc2	produkt
na	na	k7c6	na
obzoru	obzor	k1gInSc6	obzor
hrozil	hrozit	k5eAaImAgInS	hrozit
společnosti	společnost	k1gFnSc2	společnost
bankrot	bankrot	k1gInSc1	bankrot
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
ale	ale	k9	ale
upoutal	upoutat	k5eAaPmAgInS	upoutat
pozornost	pozornost	k1gFnSc4	pozornost
miliardáře	miliardář	k1gMnSc2	miliardář
Rosse	Ross	k1gMnSc2	Ross
Perota	Perot	k1gMnSc2	Perot
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
investuje	investovat	k5eAaBmIp3nS	investovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
pracovní	pracovní	k2eAgFnPc1d1	pracovní
stanice	stanice	k1gFnPc1	stanice
NeXT	NeXT	k1gFnSc3	NeXT
byly	být	k5eAaImAgFnP	být
uvedeny	uveden	k2eAgInPc1d1	uveden
na	na	k7c4	na
trh	trh	k1gInSc4	trh
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
a	a	k8xC	a
stály	stát	k5eAaImAgFnP	stát
9	[number]	k4	9
999	[number]	k4	999
$	$	kIx~	$
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Apple	Apple	kA	Apple
Lisa	Lisa	k1gFnSc1	Lisa
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
i	i	k9	i
NeXT	NeXT	k1gMnSc1	NeXT
workstation	workstation	k1gInSc2	workstation
technologicky	technologicky	k6eAd1	technologicky
velmi	velmi	k6eAd1	velmi
pokročilý	pokročilý	k2eAgMnSc1d1	pokročilý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
zamítnut	zamítnout	k5eAaPmNgInS	zamítnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
vzdělávací	vzdělávací	k2eAgInSc4d1	vzdělávací
sektor	sektor	k1gInSc4	sektor
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
který	který	k3yRgInSc4	který
byl	být	k5eAaImAgInS	být
navrhován	navrhovat	k5eAaImNgInS	navrhovat
<g/>
)	)	kIx)	)
poněkud	poněkud	k6eAd1	poněkud
drahý	drahý	k2eAgInSc1d1	drahý
<g/>
.	.	kIx.	.
</s>
<s>
NeXT	NeXT	k?	NeXT
workstation	workstation	k1gInSc1	workstation
byl	být	k5eAaImAgMnS	být
známý	známý	k2eAgMnSc1d1	známý
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
technické	technický	k2eAgFnPc4d1	technická
přednosti	přednost	k1gFnPc4	přednost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
objektově	objektově	k6eAd1	objektově
orientovaný	orientovaný	k2eAgInSc4d1	orientovaný
vývoj	vývoj	k1gInSc4	vývoj
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Jobs	Jobs	k6eAd1	Jobs
uvedl	uvést	k5eAaPmAgMnS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
produkty	produkt	k1gInPc4	produkt
NeXT	NeXT	k1gFnPc1	NeXT
určené	určený	k2eAgFnPc1d1	určená
do	do	k7c2	do
finančních	finanční	k2eAgFnPc2d1	finanční
<g/>
,	,	kIx,	,
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
a	a	k8xC	a
akademických	akademický	k2eAgFnPc2d1	akademická
komunit	komunita	k1gFnPc2	komunita
<g/>
,	,	kIx,	,
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
inovativní	inovativní	k2eAgFnPc4d1	inovativní
<g/>
,	,	kIx,	,
experimentální	experimentální	k2eAgFnPc4d1	experimentální
nové	nový	k2eAgFnPc4d1	nová
technologie	technologie	k1gFnPc4	technologie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Mach	macha	k1gFnPc2	macha
kernel	kernel	k1gInSc1	kernel
<g/>
,	,	kIx,	,
čip	čip	k1gInSc1	čip
digitálního	digitální	k2eAgInSc2d1	digitální
signálového	signálový	k2eAgInSc2d1	signálový
procesoru	procesor	k1gInSc2	procesor
a	a	k8xC	a
vestavěný	vestavěný	k2eAgInSc4d1	vestavěný
ethernetový	ethernetový	k2eAgInSc4d1	ethernetový
port	port	k1gInSc4	port
<g/>
.	.	kIx.	.
</s>
<s>
Tim	Tim	k?	Tim
Berners-Lee	Berners-Lee	k1gInSc1	Berners-Lee
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
v	v	k7c6	v
CERNu	CERNus	k1gInSc6	CERNus
na	na	k7c6	na
počítači	počítač	k1gInSc6	počítač
NeXT	NeXT	k1gFnSc2	NeXT
World	Worlda	k1gFnPc2	Worlda
Wide	Wid	k1gFnSc2	Wid
Web	web	k1gInSc1	web
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
revidovaná	revidovaný	k2eAgFnSc1d1	revidovaná
druhá	druhý	k4xOgFnSc1	druhý
generace	generace	k1gFnSc1	generace
NeXTcube	NeXTcub	k1gInSc5	NeXTcub
byla	být	k5eAaImAgNnP	být
do	do	k7c2	do
světa	svět	k1gInSc2	svět
vypuštěna	vypustit	k5eAaPmNgFnS	vypustit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
inovativnímu	inovativní	k2eAgInSc3d1	inovativní
multimediálnímu	multimediální	k2eAgInSc3d1	multimediální
e-mailovému	eailový	k2eAgInSc3d1	e-mailový
systému	systém	k1gInSc3	systém
NeXTMail	NeXTMaila	k1gFnPc2	NeXTMaila
mohl	moct	k5eAaImAgInS	moct
NeXTcube	NeXTcub	k1gInSc5	NeXTcub
vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
sdílet	sdílet	k5eAaImF	sdílet
pomocí	pomocí	k7c2	pomocí
e-mailu	eail	k1gInSc2	e-mail
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
grafiku	grafika	k1gFnSc4	grafika
a	a	k8xC	a
video	video	k1gNnSc4	video
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zaměření	zaměření	k1gNnSc3	zaměření
na	na	k7c4	na
komunikaci	komunikace	k1gFnSc4	komunikace
Jobs	Jobsa	k1gFnPc2	Jobsa
pro	pro	k7c4	pro
NeXTcube	NeXTcub	k1gInSc5	NeXTcub
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
meziosobní	meziosobní	k2eAgInSc4d1	meziosobní
počítač	počítač	k1gInSc4	počítač
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
interpersonal	interpersonat	k5eAaImAgMnS	interpersonat
computer	computer	k1gInSc4	computer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Meziosobní	Meziosobní	k2eAgInPc1d1	Meziosobní
počítače	počítač	k1gInPc1	počítač
budou	být	k5eAaImBp3nP	být
jistojistě	jistojistě	k6eAd1	jistojistě
revolucí	revoluce	k1gFnSc7	revoluce
v	v	k7c6	v
komunikaci	komunikace	k1gFnSc6	komunikace
a	a	k8xC	a
skupinové	skupinový	k2eAgFnSc3d1	skupinová
práci	práce	k1gFnSc3	práce
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
Jobs	Jobs	k1gInSc4	Jobs
novinářům	novinář	k1gMnPc3	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Jobs	Jobs	k1gInSc1	Jobs
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
NeXTu	NeXTa	k1gMnSc4	NeXTa
posedlý	posedlý	k2eAgInSc1d1	posedlý
estetickým	estetický	k2eAgInSc7d1	estetický
perfekcionismem	perfekcionismus	k1gInSc7	perfekcionismus
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
vývoj	vývoj	k1gInSc1	vývoj
hořčíkové	hořčíkový	k2eAgFnSc2d1	hořčíková
skříně	skříň	k1gFnSc2	skříň
pro	pro	k7c4	pro
NeXTcube	NeXTcub	k1gInSc5	NeXTcub
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
značný	značný	k2eAgInSc4d1	značný
tlak	tlak	k1gInSc4	tlak
na	na	k7c4	na
hardwarovou	hardwarový	k2eAgFnSc4d1	hardwarová
divizi	divize	k1gFnSc4	divize
NeXTu	NeXTus	k1gInSc2	NeXTus
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
prodali	prodat	k5eAaPmAgMnP	prodat
jen	jen	k9	jen
50	[number]	k4	50
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
přešla	přejít	k5eAaPmAgFnS	přejít
NeXT	NeXT	k1gFnSc1	NeXT
plně	plně	k6eAd1	plně
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
software	software	k1gInSc4	software
NeXTSTEP	NeXTSTEP	k1gFnPc2	NeXTSTEP
pro	pro	k7c4	pro
platformu	platforma	k1gFnSc4	platforma
Intel	Intel	kA	Intel
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
poprvé	poprvé	k6eAd1	poprvé
vykázala	vykázat	k5eAaPmAgFnS	vykázat
zisk	zisk	k1gInSc4	zisk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
1,03	[number]	k4	1,03
milionu	milion	k4xCgInSc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
vydala	vydat	k5eAaPmAgFnS	vydat
NeXT	NeXT	k1gFnSc1	NeXT
Software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
WebObjects	WebObjects	k1gInSc4	WebObjects
<g/>
,	,	kIx,	,
framework	framework	k1gInSc4	framework
určený	určený	k2eAgInSc4d1	určený
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
webových	webový	k2eAgFnPc2d1	webová
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
byla	být	k5eAaImAgFnS	být
NeXT	NeXT	k1gFnSc1	NeXT
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
odkoupena	odkoupen	k2eAgFnSc1d1	odkoupena
společností	společnost	k1gFnSc7	společnost
Apple	Apple	kA	Apple
Inc	Inc	k1gFnSc7	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
WebObjects	WebObjects	k1gInSc4	WebObjects
použity	použít	k5eAaPmNgFnP	použít
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
obchodu	obchod	k1gInSc2	obchod
Apple	Apple	kA	Apple
Online	Onlin	k1gMnSc5	Onlin
Store	Stor	k1gMnSc5	Stor
<g/>
,	,	kIx,	,
služby	služba	k1gFnPc4	služba
MobileMe	MobileM	k1gFnSc2	MobileM
a	a	k8xC	a
hudebního	hudební	k2eAgInSc2d1	hudební
obchodu	obchod	k1gInSc2	obchod
iTunes	iTunes	k1gMnSc1	iTunes
Store	Stor	k1gMnSc5	Stor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
Jobs	Jobsa	k1gFnPc2	Jobsa
za	za	k7c4	za
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
koupil	koupit	k5eAaPmAgInS	koupit
od	od	k7c2	od
divize	divize	k1gFnSc2	divize
počítačové	počítačový	k2eAgFnSc2d1	počítačová
grafiky	grafika	k1gFnSc2	grafika
studia	studio	k1gNnSc2	studio
Lucasfilm	Lucasfilm	k1gMnSc1	Lucasfilm
The	The	k1gMnSc1	The
Graphics	Graphicsa	k1gFnPc2	Graphicsa
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přejmenovaný	přejmenovaný	k2eAgInSc1d1	přejmenovaný
na	na	k7c4	na
Pixar	Pixar	k1gInSc4	Pixar
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
vyrobeným	vyrobený	k2eAgInSc7d1	vyrobený
filmem	film	k1gInSc7	film
byl	být	k5eAaImAgInS	být
Toy	Toy	k1gFnSc4	Toy
Story	story	k1gFnPc2	story
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgNnSc2	jenž
Jobs	Jobs	k1gInSc1	Jobs
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
výkonný	výkonný	k2eAgMnSc1d1	výkonný
producent	producent	k1gMnSc1	producent
<g/>
.	.	kIx.	.
</s>
<s>
Studiu	studio	k1gNnSc3	studio
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
filmu	film	k1gInSc2	film
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
přineslo	přinést	k5eAaPmAgNnS	přinést
slávu	sláva	k1gFnSc4	sláva
a	a	k8xC	a
přijetí	přijetí	k1gNnSc4	přijetí
u	u	k7c2	u
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
15	[number]	k4	15
let	léto	k1gNnPc2	léto
vydalo	vydat	k5eAaPmAgNnS	vydat
studio	studio	k1gNnSc1	studio
Pixar	Pixara	k1gFnPc2	Pixara
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
tvůrčího	tvůrčí	k2eAgMnSc2d1	tvůrčí
ředitele	ředitel	k1gMnSc2	ředitel
Johna	John	k1gMnSc2	John
Lassetera	Lasseter	k1gMnSc2	Lasseter
tyto	tento	k3xDgInPc1	tento
kasovní	kasovní	k2eAgInPc1d1	kasovní
trháky	trhák	k1gInPc1	trhák
<g/>
:	:	kIx,	:
Život	život	k1gInSc1	život
Brouka	brouk	k1gMnSc2	brouk
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Toy	Toy	k1gFnSc1	Toy
Story	story	k1gFnSc1	story
2	[number]	k4	2
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
hraček	hračka	k1gFnPc2	hračka
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Příšerky	příšerka	k1gFnSc2	příšerka
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
Nemo	Nemo	k1gNnSc1	Nemo
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Úžasňákovi	Úžasňák	k1gMnSc6	Úžasňák
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Auta	auto	k1gNnSc2	auto
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Ratatouille	Ratatouillo	k1gNnSc6	Ratatouillo
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
WALL-E	WALL-E	k1gFnSc2	WALL-E
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Vzhůru	vzhůru	k6eAd1	vzhůru
do	do	k7c2	do
oblak	oblaka	k1gNnPc2	oblaka
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Toy	Toy	k1gFnSc1	Toy
Story	story	k1gFnSc1	story
3	[number]	k4	3
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
hraček	hračka	k1gFnPc2	hračka
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Auta	auto	k1gNnPc4	auto
2	[number]	k4	2
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Rebelka	rebelka	k1gFnSc1	rebelka
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
Dory	Dora	k1gFnSc2	Dora
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Auta	auto	k1gNnSc2	auto
3	[number]	k4	3
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
Filmy	film	k1gInPc4	film
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
Nemo	Nemo	k6eAd1	Nemo
<g/>
,	,	kIx,	,
Úžasňákovi	Úžasňák	k1gMnSc6	Úžasňák
<g/>
,	,	kIx,	,
Ratattouille	Ratattouilla	k1gFnSc6	Ratattouilla
<g/>
,	,	kIx,	,
WALL-E	WALL-E	k1gFnSc6	WALL-E
<g/>
,	,	kIx,	,
Vzhůru	vzhůru	k6eAd1	vzhůru
do	do	k7c2	do
oblak	oblaka	k1gNnPc2	oblaka
a	a	k8xC	a
Toy	Toy	k1gFnSc1	Toy
Story	story	k1gFnSc1	story
3	[number]	k4	3
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
hraček	hračka	k1gFnPc2	hračka
získaly	získat	k5eAaPmAgFnP	získat
Oscara	Oscar	k1gMnSc2	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
animovaný	animovaný	k2eAgInSc4d1	animovaný
film	film	k1gInSc4	film
(	(	kIx(	(
<g/>
ocenění	ocenění	k1gNnSc1	ocenění
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
Jobs	Jobs	k1gInSc1	Jobs
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pixar	Pixar	k1gMnSc1	Pixar
hledá	hledat	k5eAaImIp3nS	hledat
nového	nový	k2eAgMnSc4d1	nový
partnera	partner	k1gMnSc4	partner
pro	pro	k7c4	pro
distribuci	distribuce	k1gFnSc4	distribuce
svých	svůj	k3xOyFgInPc2	svůj
filmů	film	k1gInPc2	film
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
vypršela	vypršet	k5eAaPmAgFnS	vypršet
smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Disney	Disnea	k1gFnSc2	Disnea
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byly	být	k5eAaImAgFnP	být
neshody	neshoda	k1gFnPc4	neshoda
s	s	k7c7	s
generálním	generální	k2eAgMnSc7d1	generální
ředitelem	ředitel	k1gMnSc7	ředitel
Disney	Disnea	k1gFnSc2	Disnea
Michaelem	Michael	k1gMnSc7	Michael
Eisnerem	Eisner	k1gMnSc7	Eisner
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2005	[number]	k4	2005
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Eisnera	Eisner	k1gMnSc4	Eisner
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
Disney	Disnea	k1gMnSc2	Disnea
Bob	Bob	k1gMnSc1	Bob
Iger	Iger	k1gMnSc1	Iger
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
se	se	k3xPyFc4	se
Iger	Iger	k1gMnSc1	Iger
snažil	snažit	k5eAaImAgMnS	snažit
urovnat	urovnat	k5eAaPmF	urovnat
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Jobsem	Jobs	k1gInSc7	Jobs
a	a	k8xC	a
Pixarem	Pixar	k1gInSc7	Pixar
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Disney	Disney	k1gInPc4	Disney
odkoupil	odkoupit	k5eAaPmAgMnS	odkoupit
akcie	akcie	k1gFnPc4	akcie
Pixaru	Pixar	k1gInSc2	Pixar
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
7,4	[number]	k4	7,4
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
dohoda	dohoda	k1gFnSc1	dohoda
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
Jobs	Jobs	k1gInSc1	Jobs
největším	veliký	k2eAgMnSc7d3	veliký
akcionářem	akcionář	k1gMnSc7	akcionář
The	The	k1gMnSc7	The
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
Company	Compana	k1gFnSc2	Compana
s	s	k7c7	s
vlastnictvím	vlastnictví	k1gNnSc7	vlastnictví
přibližně	přibližně	k6eAd1	přibližně
sedmi	sedm	k4xCc2	sedm
procent	procento	k1gNnPc2	procento
akcií	akcie	k1gFnPc2	akcie
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jobs	Jobs	k6eAd1	Jobs
tak	tak	k6eAd1	tak
předstihl	předstihnout	k5eAaPmAgMnS	předstihnout
jak	jak	k6eAd1	jak
Eisnera	Eisner	k1gMnSc4	Eisner
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc4	ten
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
cca	cca	kA	cca
1,7	[number]	k4	1,7
procenta	procento	k1gNnSc2	procento
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
člena	člen	k1gMnSc4	člen
rodiny	rodina	k1gFnSc2	rodina
Disneyů	Disney	k1gInPc2	Disney
Roye	Roy	k1gMnSc2	Roy
E.	E.	kA	E.
Disneyho	Disney	k1gMnSc2	Disney
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
jedno	jeden	k4xCgNnSc4	jeden
procento	procento	k1gNnSc4	procento
akcií	akcie	k1gFnPc2	akcie
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
Eisnerovým	Eisnerův	k2eAgMnSc7d1	Eisnerův
kritikem	kritik	k1gMnSc7	kritik
–	–	k?	–
zejména	zejména	k9	zejména
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
komplikoval	komplikovat	k5eAaBmAgMnS	komplikovat
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Pixarem	Pixar	k1gInSc7	Pixar
–	–	k?	–
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
Eisnerovu	Eisnerův	k2eAgNnSc3d1	Eisnerovo
odvolání	odvolání	k1gNnSc3	odvolání
<g/>
.	.	kIx.	.
</s>
<s>
Jobs	Jobs	k6eAd1	Jobs
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
po	po	k7c4	po
dokončení	dokončení	k1gNnSc4	dokončení
fúze	fúze	k1gFnSc2	fúze
členem	člen	k1gMnSc7	člen
představenstva	představenstvo	k1gNnSc2	představenstvo
Disney	Disnea	k1gFnSc2	Disnea
a	a	k8xC	a
v	v	k7c4	v
šestičlenného	šestičlenný	k2eAgMnSc4d1	šestičlenný
řídícím	řídící	k2eAgInSc7d1	řídící
výboru	výbor	k1gInSc2	výbor
také	také	k9	také
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
dohlížet	dohlížet	k5eAaImF	dohlížet
na	na	k7c4	na
společné	společný	k2eAgNnSc4d1	společné
podnikání	podnikání	k1gNnSc4	podnikání
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
animací	animace	k1gFnPc2	animace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Jobsově	Jobsův	k2eAgFnSc6d1	Jobsova
smrti	smrt	k1gFnSc6	smrt
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gFnPc4	jeho
akcie	akcie	k1gFnPc4	akcie
v	v	k7c4	v
Disney	Disne	k1gMnPc4	Disne
převedeny	převeden	k2eAgMnPc4d1	převeden
na	na	k7c4	na
účet	účet	k1gInSc4	účet
společnosti	společnost	k1gFnSc2	společnost
Steven	Stevna	k1gFnPc2	Stevna
P.	P.	kA	P.
Jobs	Jobs	k1gInSc1	Jobs
Trust	trust	k1gInSc1	trust
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
a	a	k8xC	a
řídí	řídit	k5eAaImIp3nS	řídit
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Laurene	Lauren	k1gMnSc5	Lauren
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
Apple	Apple	kA	Apple
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
chtěl	chtít	k5eAaImAgMnS	chtít
odkoupit	odkoupit	k5eAaPmF	odkoupit
firmu	firma	k1gFnSc4	firma
NeXT	NeXT	k1gFnSc2	NeXT
za	za	k7c4	za
427	[number]	k4	427
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
Jobs	Jobsa	k1gFnPc2	Jobsa
oznámil	oznámit	k5eAaPmAgMnS	oznámit
svůj	svůj	k3xOyFgInSc4	svůj
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
Applu	Appl	k1gInSc2	Appl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1997	[number]	k4	1997
byl	být	k5eAaImAgMnS	být
Gil	Gil	k1gMnSc1	Gil
Amelio	Amelio	k1gMnSc1	Amelio
sesazen	sesadit	k5eAaPmNgMnS	sesadit
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
tentýž	týž	k3xTgInSc4	týž
měsíc	měsíc	k1gInSc4	měsíc
byl	být	k5eAaImAgInS	být
Jobs	Jobs	k1gInSc1	Jobs
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
prozatímního	prozatímní	k2eAgMnSc2d1	prozatímní
ředitele	ředitel	k1gMnSc2	ředitel
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1998	[number]	k4	1998
Jobs	Jobs	k1gInSc1	Jobs
učinil	učinit	k5eAaPmAgInS	učinit
riskantní	riskantní	k2eAgInSc4d1	riskantní
krok	krok	k1gInSc4	krok
a	a	k8xC	a
ukončil	ukončit	k5eAaPmAgInS	ukončit
řadu	řada	k1gFnSc4	řada
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Newton	Newton	k1gMnSc1	Newton
<g/>
,	,	kIx,	,
Cyberdog	Cyberdog	k1gMnSc1	Cyberdog
a	a	k8xC	a
OpenPoc	OpenPoc	k1gFnSc1	OpenPoc
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
to	ten	k3xDgNnSc1	ten
zajistit	zajistit	k5eAaPmF	zajistit
návrat	návrat	k1gInSc4	návrat
Applu	Appl	k1gInSc2	Appl
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
ziskovosti	ziskovost	k1gFnSc2	ziskovost
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
měsících	měsíc	k1gInPc6	měsíc
projevilo	projevit	k5eAaPmAgNnS	projevit
mnoho	mnoho	k4c1	mnoho
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
přímo	přímo	k6eAd1	přímo
panický	panický	k2eAgInSc1d1	panický
strach	strach	k1gInSc1	strach
z	z	k7c2	z
ježdění	ježdění	k1gNnSc2	ježdění
s	s	k7c7	s
Jobsem	Jobso	k1gNnSc7	Jobso
ve	v	k7c6	v
výtahu	výtah	k1gInSc6	výtah
<g/>
.	.	kIx.	.
</s>
<s>
Kdykoliv	kdykoliv	k6eAd1	kdykoliv
Jobse	Jobse	k1gFnPc4	Jobse
uviděli	uvidět	k5eAaPmAgMnP	uvidět
<g/>
,	,	kIx,	,
báli	bát	k5eAaImAgMnP	bát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
přijdou	přijít	k5eAaPmIp3nP	přijít
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnost	skutečnost	k1gFnSc1	skutečnost
byla	být	k5eAaImAgFnS	být
ale	ale	k9	ale
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jobsovy	Jobsův	k2eAgFnPc1d1	Jobsova
"	"	kIx"	"
<g/>
popravy	poprava	k1gFnPc1	poprava
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
"	"	kIx"	"
byly	být	k5eAaImAgInP	být
poměrně	poměrně	k6eAd1	poměrně
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
to	ten	k3xDgNnSc1	ten
stačilo	stačit	k5eAaBmAgNnS	stačit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
hrstka	hrstka	k1gFnSc1	hrstka
propuštěných	propuštěný	k2eAgMnPc2d1	propuštěný
stala	stát	k5eAaPmAgFnS	stát
odstrašujícím	odstrašující	k2eAgInSc7d1	odstrašující
příkladem	příklad	k1gInSc7	příklad
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
koupi	koupě	k1gFnSc6	koupě
NeXT	NeXT	k1gFnSc4	NeXT
si	se	k3xPyFc3	se
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
produktů	produkt	k1gInPc2	produkt
této	tento	k3xDgFnSc2	tento
společnosti	společnost	k1gFnSc2	společnost
našla	najít	k5eAaPmAgFnS	najít
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
produktů	produkt	k1gInPc2	produkt
Apple	Apple	kA	Apple
<g/>
.	.	kIx.	.
</s>
<s>
Nejzajímavějším	zajímavý	k2eAgMnSc7d3	nejzajímavější
byl	být	k5eAaImAgMnS	být
asi	asi	k9	asi
NeXTSTEP	NeXTSTEP	k1gMnSc1	NeXTSTEP
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
pozdější	pozdní	k2eAgFnSc6d2	pozdější
Mac	Mac	kA	Mac
OS	OS	kA	OS
X.	X.	kA	X.
Pod	pod	k7c7	pod
Jobsovým	Jobsův	k2eAgNnSc7d1	Jobsovo
vedením	vedení	k1gNnSc7	vedení
společnost	společnost	k1gFnSc1	společnost
významně	významně	k6eAd1	významně
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
prodeje	prodej	k1gFnPc4	prodej
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
krom	krom	k7c2	krom
jiných	jiný	k2eAgInPc2d1	jiný
produktů	produkt	k1gInPc2	produkt
zapříčiněno	zapříčinit	k5eAaPmNgNnS	zapříčinit
i	i	k9	i
spuštěním	spuštění	k1gNnSc7	spuštění
výroby	výroba	k1gFnSc2	výroba
tzv.	tzv.	kA	tzv.
iMaců	iMaec	k1gInPc2	iMaec
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
hlavním	hlavní	k2eAgInSc7d1	hlavní
plusem	plus	k1gInSc7	plus
byl	být	k5eAaImAgInS	být
lákavý	lákavý	k2eAgInSc1d1	lákavý
design	design	k1gInSc1	design
<g/>
.	.	kIx.	.
</s>
<s>
Branding	Branding	k1gInSc1	Branding
hrál	hrát	k5eAaImAgInS	hrát
samo	sám	k3xTgNnSc4	sám
sebou	se	k3xPyFc7	se
také	také	k9	také
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Applu	Appl	k1gInSc2	Appl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
na	na	k7c4	na
Macworld	Macworld	k1gInSc4	Macworld
Expo	Expo	k1gNnSc4	Expo
Jobs	Jobsa	k1gFnPc2	Jobsa
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
dosadit	dosadit	k5eAaPmF	dosadit
sebe	sebe	k3xPyFc4	sebe
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
Applu	Appl	k1gInSc2	Appl
<g/>
.	.	kIx.	.
</s>
<s>
Apple	Apple	kA	Apple
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
vylepšování	vylepšování	k1gNnSc4	vylepšování
jiných	jiný	k2eAgNnPc2d1	jiné
digitálních	digitální	k2eAgNnPc2d1	digitální
zařízení	zařízení	k1gNnPc2	zařízení
než	než	k8xS	než
počítačů	počítač	k1gInPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
<g/>
:	:	kIx,	:
iPod	iPod	k6eAd1	iPod
–	–	k?	–
přenosný	přenosný	k2eAgInSc1d1	přenosný
hudební	hudební	k2eAgInSc1d1	hudební
přehrávač	přehrávač	k1gInSc1	přehrávač
iTunes	iTunes	k1gInSc1	iTunes
–	–	k?	–
digitální	digitální	k2eAgInSc1d1	digitální
hudební	hudební	k2eAgInSc1d1	hudební
software	software	k1gInSc1	software
iTunes	iTunesa	k1gFnPc2	iTunesa
Store	Stor	k1gInSc5	Stor
–	–	k?	–
společnost	společnost	k1gFnSc4	společnost
určenou	určený	k2eAgFnSc4d1	určená
k	k	k7c3	k
prodeji	prodej	k1gInSc3	prodej
Apple	Apple	kA	Apple
spotřební	spotřební	k2eAgFnSc2d1	spotřební
elektroniky	elektronika	k1gFnSc2	elektronika
a	a	k8xC	a
distribuci	distribuce	k1gFnSc4	distribuce
hudby	hudba	k1gFnSc2	hudba
iPhone	iPhon	k1gInSc5	iPhon
–	–	k?	–
dotykový	dotykový	k2eAgInSc4d1	dotykový
mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
a	a	k8xC	a
mobilním	mobilní	k2eAgInSc7d1	mobilní
webovým	webový	k2eAgInSc7d1	webový
prohlížečem	prohlížeč	k1gInSc7	prohlížeč
iPad	iPad	k1gInSc1	iPad
–	–	k?	–
první	první	k4xOgInSc4	první
masově	masově	k6eAd1	masově
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
tablet	tablet	k1gInSc4	tablet
Díky	díky	k7c3	díky
těmto	tento	k3xDgInPc3	tento
výrobkům	výrobek	k1gInPc3	výrobek
označil	označit	k5eAaPmAgMnS	označit
Jobs	Jobs	k1gInSc4	Jobs
své	svůj	k3xOyFgMnPc4	svůj
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
za	za	k7c4	za
"	"	kIx"	"
<g/>
skutečné	skutečný	k2eAgMnPc4d1	skutečný
umělce	umělec	k1gMnPc4	umělec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
recyklační	recyklační	k2eAgInPc4d1	recyklační
programy	program	k1gInPc4	program
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
zákazník	zákazník	k1gMnSc1	zákazník
v	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
povinován	povinován	k2eAgInSc4d1	povinován
svůj	svůj	k3xOyFgInSc4	svůj
starý	starý	k2eAgInSc4d1	starý
výrobek	výrobek	k1gInSc4	výrobek
recyklovat	recyklovat	k5eAaBmF	recyklovat
dle	dle	k7c2	dle
norem	norma	k1gFnPc2	norma
stanovených	stanovený	k2eAgFnPc2d1	stanovená
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
Jobs	Jobs	k1gInSc1	Jobs
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
na	na	k7c4	na
funkci	funkce	k1gFnSc4	funkce
výkonného	výkonný	k2eAgMnSc2d1	výkonný
ředitele	ředitel	k1gMnSc2	ředitel
(	(	kIx(	(
<g/>
CEO	CEO	kA	CEO
<g/>
)	)	kIx)	)
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
zůstal	zůstat	k5eAaPmAgMnS	zůstat
jako	jako	k8xC	jako
předseda	předseda	k1gMnSc1	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Odchod	odchod	k1gInSc1	odchod
z	z	k7c2	z
vrcholné	vrcholný	k2eAgFnSc2d1	vrcholná
pozice	pozice	k1gFnSc2	pozice
byl	být	k5eAaImAgInS	být
zapříčiněn	zapříčinit	k5eAaPmNgInS	zapříčinit
kritickým	kritický	k2eAgInSc7d1	kritický
zdravotním	zdravotní	k2eAgInSc7d1	zdravotní
stavem	stav	k1gInSc7	stav
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
138	[number]	k4	138
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
akcií	akcie	k1gFnPc2	akcie
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
(	(	kIx(	(
<g/>
hodnota	hodnota	k1gFnSc1	hodnota
4,7	[number]	k4	4,7
mld.	mld.	k?	mld.
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
a	a	k8xC	a
Apple	Apple	kA	Apple
(	(	kIx(	(
<g/>
hodnota	hodnota	k1gFnSc1	hodnota
2	[number]	k4	2
mld.	mld.	k?	mld.
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
připadly	připadnout	k5eAaPmAgFnP	připadnout
do	do	k7c2	do
rodinného	rodinný	k2eAgInSc2d1	rodinný
fondu	fond	k1gInSc2	fond
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
je	on	k3xPp3gInPc4	on
spravuje	spravovat	k5eAaImIp3nS	spravovat
Laurene	Lauren	k1gInSc5	Lauren
Powell	Powell	k1gMnSc1	Powell
Jobsová	Jobsová	k1gFnSc1	Jobsová
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Buďme	budit	k5eAaImRp1nP	budit
měřítkem	měřítko	k1gNnSc7	měřítko
kvality	kvalita	k1gFnSc2	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
nejsou	být	k5eNaImIp3nP	být
zvyklí	zvyklý	k2eAgMnPc1d1	zvyklý
na	na	k7c6	na
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dokonalost	dokonalost	k1gFnSc1	dokonalost
očekává	očekávat	k5eAaImIp3nS	očekávat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Tlačítka	tlačítko	k1gNnPc4	tlačítko
na	na	k7c6	na
obrazovce	obrazovka	k1gFnSc6	obrazovka
jsme	být	k5eAaImIp1nP	být
udělali	udělat	k5eAaPmAgMnP	udělat
tak	tak	k6eAd1	tak
lákavá	lákavý	k2eAgNnPc4d1	lákavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
máte	mít	k5eAaImIp2nP	mít
chuť	chuť	k1gFnSc4	chuť
je	on	k3xPp3gFnPc4	on
olíznout	olíznout	k5eAaPmF	olíznout
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Vždycky	vždycky	k6eAd1	vždycky
jsem	být	k5eAaImIp1nS	být
chtěl	chtít	k5eAaImAgMnS	chtít
u	u	k7c2	u
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
děláme	dělat	k5eAaImIp1nP	dělat
<g/>
,	,	kIx,	,
vlastnit	vlastnit	k5eAaImF	vlastnit
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
primární	primární	k2eAgFnSc4d1	primární
technologii	technologie	k1gFnSc4	technologie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Chci	chtít	k5eAaImIp1nS	chtít
rozezvučet	rozezvučet	k5eAaPmF	rozezvučet
vesmír	vesmír	k1gInSc4	vesmír
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Nechci	chtít	k5eNaImIp1nS	chtít
průzkum	průzkum	k1gInSc4	průzkum
trhu	trh	k1gInSc2	trh
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
lidé	člověk	k1gMnPc1	člověk
nevědí	vědět	k5eNaImIp3nP	vědět
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
chtějí	chtít	k5eAaImIp3nP	chtít
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
jim	on	k3xPp3gMnPc3	on
to	ten	k3xDgNnSc4	ten
neukážeme	ukázat	k5eNaPmIp1nP	ukázat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Musíte	muset	k5eAaImIp2nP	muset
v	v	k7c4	v
něco	něco	k3yInSc4	něco
věřit	věřit	k5eAaImF	věřit
<g/>
:	:	kIx,	:
instinkt	instinkt	k1gInSc4	instinkt
<g/>
,	,	kIx,	,
osud	osud	k1gInSc4	osud
<g/>
,	,	kIx,	,
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
karmu	karma	k1gFnSc4	karma
<g/>
,	,	kIx,	,
cokoliv	cokoliv	k3yInSc4	cokoliv
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
nebudete	být	k5eNaImBp2nP	být
v	v	k7c6	v
sobotu	sobota	k1gFnSc4	sobota
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
už	už	k6eAd1	už
nechoďte	chodit	k5eNaImRp2nP	chodit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Pracujte	pracovat	k5eAaImRp2nP	pracovat
jen	jen	k9	jen
s	s	k7c7	s
génii	génius	k1gMnPc7	génius
<g/>
,	,	kIx,	,
idioty	idiot	k1gMnPc7	idiot
vyhoďte	vyhodit	k5eAaPmRp2nP	vyhodit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Váš	váš	k3xOp2gInSc1	váš
čas	čas	k1gInSc1	čas
je	být	k5eAaImIp3nS	být
omezený	omezený	k2eAgInSc1d1	omezený
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jím	on	k3xPp3gMnSc7	on
neplýtvejte	plýtvat	k5eNaImRp2nP	plýtvat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
žili	žít	k5eAaImAgMnP	žít
život	život	k1gInSc4	život
někoho	někdo	k3yInSc2	někdo
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Nepodléhejte	podléhat	k5eNaImRp2nP	podléhat
dogmatu	dogma	k1gNnSc3	dogma
<g/>
,	,	kIx,	,
že	že	k8xS	že
máte	mít	k5eAaImIp2nP	mít
žít	žít	k5eAaImF	žít
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nP	myslet
druzí	druhý	k4xOgMnPc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Nedopusťte	dopustit	k5eNaPmRp2nP	dopustit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
hluku	hluk	k1gInSc6	hluk
cizích	cizí	k2eAgInPc2d1	cizí
názorů	názor	k1gInPc2	názor
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
váš	váš	k3xOp2gInSc4	váš
vlastní	vlastní	k2eAgInSc4d1	vlastní
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
nejdůležitější	důležitý	k2eAgInSc1d3	nejdůležitější
<g/>
:	:	kIx,	:
mějte	mít	k5eAaImRp2nP	mít
odvahu	odvaha	k1gFnSc4	odvaha
jít	jít	k5eAaImF	jít
za	za	k7c7	za
svým	svůj	k3xOyFgNnSc7	svůj
srdcem	srdce	k1gNnSc7	srdce
a	a	k8xC	a
intuicí	intuice	k1gFnSc7	intuice
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ty	ten	k3xDgMnPc4	ten
už	už	k6eAd1	už
nějak	nějak	k6eAd1	nějak
vědí	vědět	k5eAaImIp3nP	vědět
<g/>
,	,	kIx,	,
kým	kdo	k3yInSc7	kdo
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
chcete	chtít	k5eAaImIp2nP	chtít
stát	stát	k5eAaImF	stát
<g/>
.	.	kIx.	.
</s>
<s>
Všechno	všechen	k3xTgNnSc1	všechen
ostatní	ostatní	k2eAgMnSc1d1	ostatní
je	být	k5eAaImIp3nS	být
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zdroje	zdroj	k1gInPc1	zdroj
<g/>
:	:	kIx,	:
Leander	Leander	k1gInSc1	Leander
Kahney	Kahnea	k1gFnSc2	Kahnea
<g/>
:	:	kIx,	:
Jak	jak	k6eAd1	jak
myslí	myslet	k5eAaImIp3nS	myslet
Steve	Steve	k1gMnSc1	Steve
Jobs	Jobs	k1gInSc1	Jobs
<g/>
,	,	kIx,	,
Walter	Walter	k1gMnSc1	Walter
Isaacson	Isaacson	k1gMnSc1	Isaacson
<g/>
:	:	kIx,	:
Steve	Steve	k1gMnSc1	Steve
Jobs	Jobsa	k1gFnPc2	Jobsa
</s>
