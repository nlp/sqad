<s>
Zatímco	zatímco	k8xS	zatímco
Jobs	Jobs	k1gInSc1	Jobs
byl	být	k5eAaImAgInS	být
přesvědčivý	přesvědčivý	k2eAgInSc1d1	přesvědčivý
a	a	k8xC	a
charismatický	charismatický	k2eAgMnSc1d1	charismatický
ředitel	ředitel	k1gMnSc1	ředitel
Applu	Appl	k1gInSc2	Appl
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
jeho	jeho	k3xOp3gMnPc1	jeho
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
ho	on	k3xPp3gNnSc4	on
měli	mít	k5eAaImAgMnP	mít
za	za	k7c4	za
excentrického	excentrický	k2eAgMnSc4d1	excentrický
náladového	náladový	k2eAgMnSc4d1	náladový
manažera	manažer	k1gMnSc4	manažer
<g/>
.	.	kIx.	.
</s>
