<p>
<s>
Mark	Mark	k1gMnSc1	Mark
Andrew	Andrew	k1gMnSc1	Andrew
Spitz	Spitz	k1gMnSc1	Spitz
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
Modesto	Modesta	k1gFnSc5	Modesta
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc5	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
plavec	plavec	k1gMnSc1	plavec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1968	[number]	k4	1968
a	a	k8xC	a
1972	[number]	k4	1972
získal	získat	k5eAaPmAgInS	získat
celkem	celkem	k6eAd1	celkem
11	[number]	k4	11
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
devítinásobným	devítinásobný	k2eAgMnSc7d1	devítinásobný
olympijským	olympijský	k2eAgMnSc7d1	olympijský
vítězem	vítěz	k1gMnSc7	vítěz
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
získal	získat	k5eAaPmAgMnS	získat
jednu	jeden	k4xCgFnSc4	jeden
stříbrnou	stříbrná	k1gFnSc4	stříbrná
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1972	[number]	k4	1972
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
účastníkem	účastník	k1gMnSc7	účastník
těchto	tento	k3xDgFnPc2	tento
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
když	když	k8xS	když
získal	získat	k5eAaPmAgMnS	získat
7	[number]	k4	7
zlatých	zlatý	k2eAgFnPc2d1	zlatá
medailí	medaile	k1gFnPc2	medaile
na	na	k7c6	na
jedněch	jeden	k4xCgFnPc6	jeden
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
33	[number]	k4	33
světových	světový	k2eAgInPc2d1	světový
rekordů	rekord	k1gInPc2	rekord
v	v	k7c6	v
plavání	plavání	k1gNnSc6	plavání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
a	a	k8xC	a
1972	[number]	k4	1972
byl	být	k5eAaImAgMnS	být
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
Světovým	světový	k2eAgMnSc7d1	světový
plavcem	plavec	k1gMnSc7	plavec
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
Letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
2008	[number]	k4	2008
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nejúspěšnějšího	úspěšný	k2eAgMnSc4d3	nejúspěšnější
plavce	plavec	k1gMnPc4	plavec
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
36	[number]	k4	36
let	léto	k1gNnPc2	léto
nikým	nikdo	k3yNnSc7	nikdo
nepřekonanou	překonaný	k2eNgFnSc7d1	nepřekonaná
legendou	legenda	k1gFnSc7	legenda
tohoto	tento	k3xDgNnSc2	tento
sportovního	sportovní	k2eAgNnSc2d1	sportovní
odvětví	odvětví	k1gNnSc2	odvětví
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
ho	on	k3xPp3gMnSc4	on
nepřekonal	překonat	k5eNaPmAgMnS	překonat
Michael	Michael	k1gMnSc1	Michael
Phelps	Phelpsa	k1gFnPc2	Phelpsa
ziskem	zisk	k1gInSc7	zisk
8	[number]	k4	8
zlatých	zlatý	k2eAgFnPc2d1	zlatá
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Phelps	Phelpsa	k1gFnPc2	Phelpsa
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mark	Mark	k1gMnSc1	Mark
Spitz	Spitza	k1gFnPc2	Spitza
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Mark	Mark	k1gMnSc1	Mark
Spitz	Spitz	k1gMnSc1	Spitz
oficiální	oficiální	k2eAgFnSc4d1	oficiální
stránka	stránka	k1gFnSc1	stránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Spitz	Spitz	k1gMnSc1	Spitz
lived	lived	k1gMnSc1	lived
up	up	k?	up
to	ten	k3xDgNnSc4	ten
enormous	enormous	k1gInSc1	enormous
expectations	expectations	k6eAd1	expectations
životopis	životopis	k1gInSc4	životopis
na	na	k7c4	na
Mark	Mark	k1gMnSc1	Mark
Spitz	Spitz	k1gInSc1	Spitz
na	na	k7c6	na
ESPN	ESPN	kA	ESPN
</s>
</p>
