<s>
Arsen	arsen	k1gInSc1	arsen
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
As	as	k1gNnSc2	as
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Arsenicum	Arsenicum	k1gInSc1	Arsenicum
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
al-zarnī	alarnī	k?	al-zarnī
-	-	kIx~	-
zlatavá	zlatavý	k2eAgFnSc1d1	zlatavá
barva	barva	k1gFnSc1	barva
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
též	též	k9	též
název	název	k1gInSc1	název
Arzén	arzén	k1gInSc1	arzén
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
toxický	toxický	k2eAgInSc1d1	toxický
polokovový	polokovový	k2eAgInSc1d1	polokovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
