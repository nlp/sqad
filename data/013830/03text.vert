<s>
Moneron	Moneron	k1gMnSc1
</s>
<s>
MoneronМ	MoneronМ	k?
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
Turistické	turistický	k2eAgFnPc1d1
chaty	chata	k1gFnPc1
na	na	k7c6
ostrově	ostrov	k1gInSc6
</s>
<s>
Moneron	Moneron	k1gMnSc1
</s>
<s>
Lokalizace	lokalizace	k1gFnSc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
Stát	stát	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
Rusko	Rusko	k1gNnSc1
Topografie	topografie	k1gFnSc2
Rozloha	rozloha	k1gFnSc1
</s>
<s>
30	#num#	k4
km²	km²	k?
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
46	#num#	k4
<g/>
°	°	k?
<g/>
14	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
141	#num#	k4
<g/>
°	°	k?
<g/>
13	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
</s>
<s>
bezejmenná	bezejmenný	k2eAgFnSc1d1
kóta	kóta	k1gFnSc1
(	(	kIx(
<g/>
439	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
Osídlení	osídlení	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
neobydlen	obydlen	k2eNgInSc1d1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Moneron	Moneron	k1gInSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
М	М	k?
<g/>
,	,	kIx,
japonsky	japonsky	k6eAd1
海	海	k?
<g/>
,	,	kIx,
Kaibato	Kaibat	k2eAgNnSc1d1
<g/>
,	,	kIx,
ainu	ainu	k5eAaPmIp1nS
<g/>
:	:	kIx,
Todomoshiri	Todomoshiri	k1gNnSc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ostrov	ostrov	k1gInSc4
sopečného	sopečný	k2eAgInSc2d1
původu	původ	k1gInSc2
ležící	ležící	k2eAgFnSc1d1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
Japonského	japonský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
43	#num#	k4
km	km	kA
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
Sachalinu	Sachalin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrov	ostrov	k1gInSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Ruské	ruský	k2eAgFnSc2d1
federace	federace	k1gFnSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
30	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
v	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
neobydlený	obydlený	k2eNgInSc1d1
<g/>
;	;	kIx,
byl	být	k5eAaImAgInS
na	na	k7c6
něm	on	k3xPp3gInSc6
zřízen	zřídit	k5eAaPmNgInS
mořský	mořský	k2eAgInSc1d1
park	park	k1gInSc1
a	a	k8xC
vstup	vstup	k1gInSc1
je	být	k5eAaImIp3nS
možný	možný	k2eAgInSc1d1
jen	jen	k9
na	na	k7c4
povolení	povolení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Ainuové	Ainuové	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
nazývali	nazývat	k5eAaImAgMnP
Totomoširi	Totomošir	k1gMnPc1
(	(	kIx(
<g/>
Ostrov	ostrov	k1gInSc1
lvounů	lvoun	k1gMnPc2
<g/>
)	)	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1787	#num#	k4
ho	on	k3xPp3gMnSc4
jako	jako	k9
první	první	k4xOgMnSc1
Evropan	Evropan	k1gMnSc1
navštívil	navštívit	k5eAaPmAgMnS
Jean-François	Jean-François	k1gFnSc3
de	de	k?
La	la	k1gNnSc4
Pérouse	Pérouse	k1gFnSc2
a	a	k8xC
pojmenoval	pojmenovat	k5eAaPmAgMnS
ho	on	k3xPp3gNnSc4
po	po	k7c6
členu	člen	k1gInSc6
své	svůj	k3xOyFgFnSc2
výpravy	výprava	k1gFnSc2
Paulu	Paul	k1gMnSc3
Méraultu	Mérault	k1gInSc2
Monneronovi	Monneron	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1905	#num#	k4
až	až	k9
1945	#num#	k4
patřil	patřit	k5eAaImAgInS
ostrov	ostrov	k1gInSc4
Japonsku	Japonsko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
byl	být	k5eAaImAgMnS
nedaleko	nedaleko	k7c2
Moneronu	Moneron	k1gInSc2
sestřelen	sestřelen	k2eAgInSc4d1
let	let	k1gInSc4
Korean	Korean	k1gInSc1
Air	Air	k1gFnSc1
0	#num#	k4
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1
poměry	poměr	k1gInPc1
</s>
<s>
Ostrov	ostrov	k1gInSc1
je	být	k5eAaImIp3nS
skalnatý	skalnatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
původních	původní	k2eAgInPc2d1
lesů	les	k1gInPc2
byla	být	k5eAaImAgFnS
vykácena	vykácen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roste	růst	k5eAaImIp3nS
zde	zde	k6eAd1
divoká	divoký	k2eAgFnSc1d1
réva	réva	k1gFnSc1
<g/>
,	,	kIx,
růže	růže	k1gFnSc1
svraskalá	svraskalý	k2eAgFnSc1d1
a	a	k8xC
morušovník	morušovník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ostrově	ostrov	k1gInSc6
jsou	být	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
řeky	řeka	k1gFnPc1
<g/>
,	,	kIx,
Uso	Uso	k1gFnPc1
a	a	k8xC
Moneron	Moneron	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
vytvářejí	vytvářet	k5eAaImIp3nP
četné	četný	k2eAgInPc1d1
vodopády	vodopád	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podnebí	podnebí	k1gNnPc4
je	být	k5eAaImIp3nS
chladné	chladný	k2eAgNnSc1d1
<g/>
,	,	kIx,
větrné	větrný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teplý	teplý	k2eAgInSc1d1
Cušimský	cušimský	k2eAgInSc1d1
proud	proud	k1gInSc1
umožňuje	umožňovat	k5eAaImIp3nS
bohatý	bohatý	k2eAgInSc4d1
život	život	k1gInSc4
v	v	k7c6
okolním	okolní	k2eAgNnSc6d1
moři	moře	k1gNnSc6
—	—	k?
typickým	typický	k2eAgMnSc7d1
představitelem	představitel	k1gMnSc7
je	být	k5eAaImIp3nS
lachtan	lachtan	k1gMnSc1
ušatý	ušatý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Moneronu	Moneron	k1gInSc6
hnízdí	hnízdit	k5eAaImIp3nS
množství	množství	k1gNnSc1
mořských	mořský	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
především	především	k9
buřňáčků	buřňáček	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Moneron	Moneron	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Stránky	stránka	k1gFnPc1
mořského	mořský	k2eAgInSc2d1
parku	park	k1gInSc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Expedice	expedice	k1gFnSc1
na	na	k7c4
ostrov	ostrov	k1gInSc4
</s>
<s>
Bohatá	bohatý	k2eAgFnSc1d1
kolekce	kolekce	k1gFnSc1
fotografií	fotografia	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
</s>
