<s>
Technologicky	technologicky	k6eAd1	technologicky
výrobu	výroba	k1gFnSc4	výroba
žárovky	žárovka	k1gFnSc2	žárovka
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
a	a	k8xC	a
patentoval	patentovat	k5eAaBmAgMnS	patentovat
Thomas	Thomas	k1gMnSc1	Thomas
Alva	Alva	k1gMnSc1	Alva
Edison	Edison	k1gMnSc1	Edison
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
-	-	kIx~	-
první	první	k4xOgFnSc1	první
žárovka	žárovka	k1gFnSc1	žárovka
byla	být	k5eAaImAgFnS	být
rozsvícena	rozsvícen	k2eAgFnSc1d1	rozsvícena
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1879	[number]	k4	1879
a	a	k8xC	a
svítila	svítit	k5eAaImAgFnS	svítit
40	[number]	k4	40
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
