<p>
<s>
Haumea	Haumea	k6eAd1	Haumea
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgNnSc4d1	oficiální
označení	označení	k1gNnSc4	označení
(	(	kIx(	(
<g/>
136108	[number]	k4	136108
<g/>
)	)	kIx)	)
Haumea	Haume	k1gInSc2	Haume
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
plutoid	plutoid	k1gInSc1	plutoid
v	v	k7c6	v
Kuiperově	Kuiperův	k2eAgInSc6d1	Kuiperův
pásu	pás	k1gInSc6	pás
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
asi	asi	k9	asi
jedné	jeden	k4xCgFnSc2	jeden
třetiny	třetina	k1gFnSc2	třetina
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Pluta	Pluto	k1gNnSc2	Pluto
a	a	k8xC	a
0,07	[number]	k4	0,07
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Objeven	objevit	k5eAaPmNgInS	objevit
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
týmem	tým	k1gInSc7	tým
vedeným	vedený	k2eAgMnSc7d1	vedený
Michaelem	Michael	k1gMnSc7	Michael
Brownem	Brown	k1gMnSc7	Brown
z	z	k7c2	z
Kalifornského	kalifornský	k2eAgInSc2d1	kalifornský
technologického	technologický	k2eAgInSc2d1	technologický
institutu	institut	k1gInSc2	institut
na	na	k7c6	na
fotografiích	fotografia	k1gFnPc6	fotografia
pořízených	pořízený	k2eAgFnPc2d1	pořízená
na	na	k7c6	na
observatoři	observatoř	k1gFnSc6	observatoř
Palomar	Palomara	k1gFnPc2	Palomara
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
týmem	tým	k1gInSc7	tým
vedeným	vedený	k2eAgNnSc7d1	vedené
José	José	k1gNnSc1	José
Ortizem	Ortiz	k1gInSc7	Ortiz
na	na	k7c6	na
Observatoři	observatoř	k1gFnSc6	observatoř
Sierra	Sierra	k1gFnSc1	Sierra
Nevada	Nevada	k1gFnSc1	Nevada
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc4	objev
provázely	provázet	k5eAaImAgInP	provázet
spory	spor	k1gInPc1	spor
o	o	k7c6	o
prvenství	prvenství	k1gNnSc6	prvenství
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
týmy	tým	k1gInPc7	tým
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
astronomická	astronomický	k2eAgFnSc1d1	astronomická
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
IAU	IAU	kA	IAU
<g/>
)	)	kIx)	)
tělesu	těleso	k1gNnSc6	těleso
přiznala	přiznat	k5eAaPmAgFnS	přiznat
status	status	k1gInSc4	status
trpasličí	trpasličí	k2eAgFnSc2d1	trpasličí
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
ho	on	k3xPp3gMnSc4	on
po	po	k7c6	po
havajské	havajský	k2eAgFnSc6d1	Havajská
bohyni	bohyně	k1gFnSc6	bohyně
zrození	zrození	k1gNnSc2	zrození
Haumea	Haume	k1gInSc2	Haume
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Extrémně	extrémně	k6eAd1	extrémně
protáhlý	protáhlý	k2eAgInSc1d1	protáhlý
tvar	tvar	k1gInSc1	tvar
této	tento	k3xDgFnSc2	tento
trpasličí	trpasličí	k2eAgFnSc2d1	trpasličí
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
transneptunickými	transneptunický	k2eAgNnPc7d1	transneptunické
tělesy	těleso	k1gNnPc7	těleso
(	(	kIx(	(
<g/>
TNO	TNO	kA	TNO
<g/>
)	)	kIx)	)
jedinečný	jedinečný	k2eAgMnSc1d1	jedinečný
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
její	její	k3xOp3gInSc1	její
tvar	tvar	k1gInSc1	tvar
nebyl	být	k5eNaImAgInS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
výpočty	výpočet	k1gInPc1	výpočet
z	z	k7c2	z
její	její	k3xOp3gFnSc2	její
světelné	světelný	k2eAgFnSc2d1	světelná
křivky	křivka	k1gFnSc2	křivka
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
elipsoid	elipsoid	k1gInSc4	elipsoid
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
osa	osa	k1gFnSc1	osa
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
nejkratší	krátký	k2eAgFnSc3d3	nejkratší
ose	osa	k1gFnSc3	osa
dvojnásobná	dvojnásobný	k2eAgFnSc1d1	dvojnásobná
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
se	se	k3xPyFc4	se
přesto	přesto	k8xC	přesto
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
gravitace	gravitace	k1gFnSc1	gravitace
tělesa	těleso	k1gNnSc2	těleso
je	být	k5eAaImIp3nS	být
dostatečná	dostatečný	k2eAgFnSc1d1	dostatečná
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
spočinulo	spočinout	k5eAaPmAgNnS	spočinout
v	v	k7c6	v
hydrostatické	hydrostatický	k2eAgFnSc6d1	hydrostatická
rovnováze	rovnováha	k1gFnSc6	rovnováha
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
splňuje	splňovat	k5eAaImIp3nS	splňovat
podmínky	podmínka	k1gFnPc4	podmínka
definice	definice	k1gFnSc2	definice
trpasličí	trpasličí	k2eAgFnSc2d1	trpasličí
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Prodloužený	prodloužený	k2eAgInSc1d1	prodloužený
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
rychlost	rychlost	k1gFnSc1	rychlost
rotace	rotace	k1gFnSc1	rotace
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hustota	hustota	k1gFnSc1	hustota
i	i	k8xC	i
velké	velký	k2eAgNnSc1d1	velké
albedo	albedo	k1gNnSc1	albedo
(	(	kIx(	(
<g/>
způsobené	způsobený	k2eAgFnSc2d1	způsobená
přítomností	přítomnost	k1gFnSc7	přítomnost
krystalického	krystalický	k2eAgInSc2d1	krystalický
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
zřejmě	zřejmě	k6eAd1	zřejmě
výsledkem	výsledek	k1gInSc7	výsledek
mohutné	mohutný	k2eAgFnSc2d1	mohutná
kolize	kolize	k1gFnSc2	kolize
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
navíc	navíc	k6eAd1	navíc
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
celá	celý	k2eAgFnSc1d1	celá
skupina	skupina	k1gFnSc1	skupina
příbuzných	příbuzný	k2eAgNnPc2d1	příbuzné
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInSc1d1	zahrnující
také	také	k6eAd1	také
dva	dva	k4xCgInPc4	dva
známé	známý	k2eAgInPc4d1	známý
měsíce	měsíc	k1gInPc4	měsíc
Haumey	Haume	k2eAgInPc4d1	Haume
a	a	k8xC	a
několik	několik	k4yIc4	několik
dalších	další	k2eAgNnPc2d1	další
TNO	TNO	kA	TNO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasifikace	klasifikace	k1gFnSc2	klasifikace
==	==	k?	==
</s>
</p>
<p>
<s>
Haumea	Haumea	k6eAd1	Haumea
je	být	k5eAaImIp3nS	být
plutoid	plutoid	k1gInSc1	plutoid
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
trpasličí	trpasličí	k2eAgFnPc4d1	trpasličí
planety	planeta	k1gFnPc4	planeta
obíhající	obíhající	k2eAgFnPc4d1	obíhající
za	za	k7c7	za
drahou	draha	k1gFnSc7	draha
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
definice	definice	k1gFnSc2	definice
trpasličí	trpasličí	k2eAgFnSc2d1	trpasličí
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
takové	takový	k3xDgNnSc4	takový
těleso	těleso	k1gNnSc4	těleso
dostatečně	dostatečně	k6eAd1	dostatečně
hmotné	hmotný	k2eAgFnPc1d1	hmotná
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vlivem	vlivem	k7c2	vlivem
vlastní	vlastní	k2eAgFnSc2d1	vlastní
gravitace	gravitace	k1gFnSc2	gravitace
získalo	získat	k5eAaPmAgNnS	získat
kulový	kulový	k2eAgInSc4d1	kulový
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nevyčistilo	vyčistit	k5eNaPmAgNnS	vyčistit
své	svůj	k3xOyFgNnSc1	svůj
sousedství	sousedství	k1gNnSc1	sousedství
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
,	,	kIx,	,
podobných	podobný	k2eAgInPc2d1	podobný
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Haumea	Haumea	k1gFnSc1	Haumea
zdaleka	zdaleka	k6eAd1	zdaleka
nepřipomíná	připomínat	k5eNaImIp3nS	připomínat
kouli	koule	k1gFnSc4	koule
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
elipsoidní	elipsoidní	k2eAgInSc4d1	elipsoidní
tvar	tvar	k1gInSc4	tvar
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
výsledkem	výsledek	k1gInSc7	výsledek
rychlé	rychlý	k2eAgFnSc2d1	rychlá
rotace	rotace	k1gFnSc2	rotace
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
nedostatečné	dostatečný	k2eNgFnPc1d1	nedostatečná
gravitace	gravitace	k1gFnPc1	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Haumea	Haumea	k1gFnSc1	Haumea
byla	být	k5eAaImAgFnS	být
také	také	k9	také
původně	původně	k6eAd1	původně
zařazena	zařadit	k5eAaPmNgFnS	zařadit
mezi	mezi	k7c4	mezi
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
klasické	klasický	k2eAgInPc4d1	klasický
objekty	objekt	k1gInPc4	objekt
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
též	též	k9	též
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k9	jako
kubewana	kubewana	k1gFnSc1	kubewana
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
skupinou	skupina	k1gFnSc7	skupina
známých	známý	k2eAgNnPc2d1	známé
transneptunických	transneptunický	k2eAgNnPc2d1	transneptunické
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Trajektorie	trajektorie	k1gFnSc1	trajektorie
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
Haumea	Haumea	k1gFnSc1	Haumea
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
však	však	k9	však
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rezonuje	rezonovat	k5eAaImIp3nS	rezonovat
s	s	k7c7	s
Neptunem	Neptun	k1gInSc7	Neptun
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ji	on	k3xPp3gFnSc4	on
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
rezonanční	rezonanční	k2eAgFnPc4d1	rezonanční
transneptunická	transneptunický	k2eAgNnPc4d1	transneptunické
tělesa	těleso	k1gNnPc4	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jméno	jméno	k1gNnSc1	jméno
==	==	k?	==
</s>
</p>
<p>
<s>
Kalifornský	kalifornský	k2eAgInSc1d1	kalifornský
tým	tým	k1gInSc1	tým
původně	původně	k6eAd1	původně
těleso	těleso	k1gNnSc4	těleso
nazýval	nazývat	k5eAaImAgMnS	nazývat
familiárně	familiárně	k6eAd1	familiárně
"	"	kIx"	"
<g/>
Santa	Santa	k1gFnSc1	Santa
<g/>
"	"	kIx"	"
kvůli	kvůli	k7c3	kvůli
době	doba	k1gFnSc3	doba
objevu	objev	k1gInSc2	objev
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
Vánocích	Vánoce	k1gFnPc6	Vánoce
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
dostalo	dostat	k5eAaPmAgNnS	dostat
oficiální	oficiální	k2eAgNnSc1d1	oficiální
předběžné	předběžný	k2eAgNnSc1d1	předběžné
označení	označení	k1gNnSc1	označení
2003	[number]	k4	2003
EL	Ela	k1gFnPc2	Ela
<g/>
61	[number]	k4	61
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
však	však	k9	však
vztahovalo	vztahovat	k5eAaImAgNnS	vztahovat
k	k	k7c3	k
datu	datum	k1gNnSc3	datum
španělského	španělský	k2eAgInSc2d1	španělský
objevového	objevový	k2eAgInSc2d1	objevový
snímku	snímek	k1gInSc2	snímek
<g/>
,	,	kIx,	,
pořízeného	pořízený	k2eAgInSc2d1	pořízený
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2005	[number]	k4	2005
pak	pak	k8xC	pak
těleso	těleso	k1gNnSc1	těleso
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
katalogové	katalogový	k2eAgNnSc1d1	Katalogové
číslo	číslo	k1gNnSc1	číslo
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
katalogu	katalog	k1gInSc2	katalog
Minor	minor	k2eAgFnPc2d1	minor
Planet	planeta	k1gFnPc2	planeta
Center	centrum	k1gNnPc2	centrum
jako	jako	k9	jako
(	(	kIx(	(
<g/>
136108	[number]	k4	136108
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
EL	Ela	k1gFnPc2	Ela
<g/>
61	[number]	k4	61
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
Komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
nomenklaturu	nomenklatura	k1gFnSc4	nomenklatura
malých	malý	k2eAgNnPc2d1	malé
těles	těleso	k1gNnPc2	těleso
stanovují	stanovovat	k5eAaImIp3nP	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
klasická	klasický	k2eAgNnPc1d1	klasické
tělesa	těleso	k1gNnPc1	těleso
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
pojmenovávána	pojmenováván	k2eAgFnSc1d1	pojmenovávána
podle	podle	k7c2	podle
mytologických	mytologický	k2eAgFnPc2d1	mytologická
bytostí	bytost	k1gFnPc2	bytost
souvisejících	související	k2eAgFnPc2d1	související
se	s	k7c7	s
stvořením	stvoření	k1gNnSc7	stvoření
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
konvencí	konvence	k1gFnSc7	konvence
David	David	k1gMnSc1	David
Rabinowitz	Rabinowitz	k1gMnSc1	Rabinowitz
z	z	k7c2	z
kalifornského	kalifornský	k2eAgInSc2d1	kalifornský
týmu	tým	k1gInSc2	tým
v	v	k7c6	v
září	září	k1gNnSc6	září
2006	[number]	k4	2006
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
pro	pro	k7c4	pro
těleso	těleso	k1gNnSc4	těleso
i	i	k8xC	i
jeho	jeho	k3xOp3gInPc4	jeho
měsíce	měsíc	k1gInPc4	měsíc
jména	jméno	k1gNnSc2	jméno
z	z	k7c2	z
havajské	havajský	k2eAgFnSc2d1	Havajská
mytologie	mytologie	k1gFnSc2	mytologie
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
tak	tak	k6eAd1	tak
bude	být	k5eAaImBp3nS	být
vzdána	vzdán	k2eAgFnSc1d1	vzdána
pocta	pocta	k1gFnSc1	pocta
místu	místo	k1gNnSc3	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgInPc1	tento
měsíce	měsíc	k1gInPc1	měsíc
objeveny	objeven	k2eAgInPc1d1	objeven
<g/>
.	.	kIx.	.
<g/>
Bohyně	bohyně	k1gFnSc1	bohyně
Haumea	Haumea	k1gFnSc1	Haumea
je	být	k5eAaImIp3nS	být
patronkou	patronka	k1gFnSc7	patronka
ostrova	ostrov	k1gInSc2	ostrov
Havaj	Havaj	k1gFnSc4	Havaj
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
leží	ležet	k5eAaImIp3nS	ležet
Observatoř	observatoř	k1gFnSc1	observatoř
Mauna	Mauna	k1gFnSc1	Mauna
Kea	Kea	k1gFnSc1	Kea
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
bývá	bývat	k5eAaImIp3nS	bývat
ztotožňována	ztotožňován	k2eAgFnSc1d1	ztotožňována
s	s	k7c7	s
Pā	Pā	k1gFnSc7	Pā
<g/>
,	,	kIx,	,
bohyní	bohyně	k1gFnSc7	bohyně
země	zem	k1gFnSc2	zem
reprezentující	reprezentující	k2eAgInSc4d1	reprezentující
element	element	k1gInSc4	element
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
současně	současně	k6eAd1	současně
podporuje	podporovat	k5eAaImIp3nS	podporovat
vhodnost	vhodnost	k1gFnSc4	vhodnost
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tělesu	těleso	k1gNnSc6	těleso
se	se	k3xPyFc4	se
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
z	z	k7c2	z
pevného	pevný	k2eAgInSc2d1	pevný
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
známých	známý	k2eAgInPc2d1	známý
objektů	objekt	k1gInPc2	objekt
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
jen	jen	k9	jen
tenkým	tenký	k2eAgInSc7d1	tenký
ledovým	ledový	k2eAgInSc7d1	ledový
pláštěm	plášť	k1gInSc7	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důvodem	důvod	k1gInSc7	důvod
návrhu	návrh	k1gInSc2	návrh
jména	jméno	k1gNnSc2	jméno
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Haumea	Haumea	k1gFnSc1	Haumea
je	být	k5eAaImIp3nS	být
také	také	k9	také
bohyní	bohyně	k1gFnSc7	bohyně
plodnosti	plodnost	k1gFnSc2	plodnost
a	a	k8xC	a
zrození	zrození	k1gNnSc2	zrození
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c4	mnoho
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
vyrašily	vyrašit	k5eAaPmAgInP	vyrašit
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
částí	část	k1gFnSc7	část
jejího	její	k3xOp3gNnSc2	její
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
shluku	shluk	k1gInSc2	shluk
ledových	ledový	k2eAgNnPc2d1	ledové
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
od	od	k7c2	od
mateřského	mateřský	k2eAgNnSc2d1	mateřské
tělesa	těleso	k1gNnSc2	těleso
odtrhl	odtrhnout	k5eAaPmAgInS	odtrhnout
při	při	k7c6	při
nějaké	nějaký	k3yIgFnSc6	nějaký
dávné	dávný	k2eAgFnSc6d1	dávná
kolizi	kolize	k1gFnSc6	kolize
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
oba	dva	k4xCgInPc1	dva
známé	známý	k2eAgInPc1d1	známý
měsíce	měsíc	k1gInPc1	měsíc
se	se	k3xPyFc4	se
zrodily	zrodit	k5eAaPmAgInP	zrodit
právě	právě	k9	právě
touto	tento	k3xDgFnSc7	tento
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byly	být	k5eAaImAgInP	být
pojmenovány	pojmenovat	k5eAaPmNgInP	pojmenovat
podle	podle	k7c2	podle
dvou	dva	k4xCgFnPc2	dva
z	z	k7c2	z
mýtických	mýtický	k2eAgFnPc2d1	mýtická
dcer	dcera	k1gFnPc2	dcera
Haumey	Haumea	k1gMnSc2	Haumea
<g/>
,	,	kIx,	,
Hiʻ	Hiʻ	k1gMnSc2	Hiʻ
a	a	k8xC	a
Namaka	Namak	k1gMnSc2	Namak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Spory	spor	k1gInPc4	spor
kolem	kolem	k7c2	kolem
objevu	objev	k1gInSc2	objev
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
objevu	objev	k1gInSc3	objev
Haumey	Haumea	k1gFnSc2	Haumea
se	se	k3xPyFc4	se
přihlásily	přihlásit	k5eAaPmAgInP	přihlásit
dva	dva	k4xCgInPc1	dva
týmy	tým	k1gInPc1	tým
<g/>
.	.	kIx.	.
</s>
<s>
Kalifornský	kalifornský	k2eAgInSc1d1	kalifornský
tým	tým	k1gInSc1	tým
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Michaela	Michael	k1gMnSc2	Michael
Browna	Brown	k1gMnSc2	Brown
těleso	těleso	k1gNnSc4	těleso
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2004	[number]	k4	2004
na	na	k7c6	na
fotografiích	fotografia	k1gFnPc6	fotografia
pořízených	pořízený	k2eAgFnPc2d1	pořízená
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
publikovali	publikovat	k5eAaBmAgMnP	publikovat
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
abstrakt	abstrakt	k1gInSc1	abstrakt
své	svůj	k3xOyFgFnSc2	svůj
zprávy	zpráva	k1gFnSc2	zpráva
pro	pro	k7c4	pro
konferenci	konference	k1gFnSc4	konference
konanou	konaný	k2eAgFnSc4d1	konaná
v	v	k7c6	v
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
zamýšleli	zamýšlet	k5eAaImAgMnP	zamýšlet
objev	objev	k1gInSc4	objev
oficiálně	oficiálně	k6eAd1	oficiálně
oznámit	oznámit	k5eAaPmF	oznámit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
Haumeu	Haumeus	k1gInSc2	Haumeus
nalezl	naleznout	k5eAaPmAgInS	naleznout
také	také	k9	také
José-Luis	José-Luis	k1gInSc1	José-Luis
Ortiz	Ortiza	k1gFnPc2	Ortiza
Moreno	Moren	k2eAgNnSc1d1	Moreno
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
týmem	tým	k1gInSc7	tým
v	v	k7c6	v
Instituto	Institut	k2eAgNnSc4d1	Instituto
de	de	k?	de
Astrofísica	Astrofísic	k2eAgFnSc1d1	Astrofísic
de	de	k?	de
Andalucía	Andalucía	k1gFnSc1	Andalucía
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
na	na	k7c6	na
snímcích	snímek	k1gInPc6	snímek
pořízených	pořízený	k2eAgInPc6d1	pořízený
již	již	k6eAd1	již
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Ortiz	Ortiz	k1gInSc1	Ortiz
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
zaslal	zaslat	k5eAaPmAgMnS	zaslat
zprávu	zpráva	k1gFnSc4	zpráva
e-mailem	eail	k1gInSc7	e-mail
do	do	k7c2	do
Minor	minor	k2eAgFnPc2d1	minor
Planet	planeta	k1gFnPc2	planeta
Center	centrum	k1gNnPc2	centrum
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
objev	objev	k1gInSc1	objev
byl	být	k5eAaImAgInS	být
učiněn	učinit	k5eAaPmNgInS	učinit
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
<g/>
Michael	Michael	k1gMnSc1	Michael
Brown	Brown	k1gMnSc1	Brown
se	se	k3xPyFc4	se
však	však	k9	však
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
někdo	někdo	k3yInSc1	někdo
ze	z	k7c2	z
španělské	španělský	k2eAgFnSc2d1	španělská
observatoře	observatoř	k1gFnSc2	observatoř
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
prohlížel	prohlížet	k5eAaImAgMnS	prohlížet
jejich	jejich	k3xOp3gInPc4	jejich
soubory	soubor	k1gInPc4	soubor
obsahující	obsahující	k2eAgInPc4d1	obsahující
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byly	být	k5eAaImAgInP	být
namířeny	namířit	k5eAaPmNgInP	namířit
jejich	jejich	k3xOp3gInPc1	jejich
teleskopy	teleskop	k1gInPc1	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
soubory	soubor	k1gInPc1	soubor
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
dost	dost	k6eAd1	dost
informací	informace	k1gFnSc7	informace
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
umožnily	umožnit	k5eAaPmAgFnP	umožnit
nalézt	nalézt	k5eAaBmF	nalézt
těleso	těleso	k1gNnSc4	těleso
na	na	k7c6	na
předobjevových	předobjevův	k2eAgInPc6d1	předobjevův
snímcích	snímek	k1gInPc6	snímek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Soubory	soubor	k1gInPc1	soubor
někdo	někdo	k3yInSc1	někdo
prohlížel	prohlížet	k5eAaImAgInS	prohlížet
právě	právě	k6eAd1	právě
den	den	k1gInSc4	den
před	před	k7c7	před
Ortizovým	Ortizův	k2eAgNnSc7d1	Ortizův
oznámením	oznámení	k1gNnSc7	oznámení
objevu	objev	k1gInSc2	objev
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Ortiz	Ortiz	k1gMnSc1	Ortiz
později	pozdě	k6eAd2	pozdě
připustil	připustit	k5eAaPmAgMnS	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
popřel	popřít	k5eAaPmAgMnS	popřít
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
zlý	zlý	k2eAgInSc4d1	zlý
úmysl	úmysl	k1gInSc4	úmysl
<g/>
.	.	kIx.	.
</s>
<s>
Argumentoval	argumentovat	k5eAaImAgMnS	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
pouze	pouze	k6eAd1	pouze
ověřoval	ověřovat	k5eAaImAgMnS	ověřovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
objevili	objevit	k5eAaPmAgMnP	objevit
nový	nový	k2eAgInSc4d1	nový
objekt	objekt	k1gInSc4	objekt
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
astronomické	astronomický	k2eAgFnSc2d1	astronomická
unie	unie	k1gFnSc2	unie
je	být	k5eAaImIp3nS	být
objevitelem	objevitel	k1gMnSc7	objevitel
planetky	planetka	k1gFnSc2	planetka
vždy	vždy	k6eAd1	vždy
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
první	první	k4xOgMnSc1	první
podá	podat	k5eAaPmIp3nS	podat
do	do	k7c2	do
Minor	minor	k2eAgFnPc2d1	minor
Planet	planeta	k1gFnPc2	planeta
Center	centrum	k1gNnPc2	centrum
zprávu	zpráva	k1gFnSc4	zpráva
s	s	k7c7	s
dostatkem	dostatek	k1gInSc7	dostatek
pozičních	poziční	k2eAgInPc2d1	poziční
údajů	údaj	k1gInPc2	údaj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
umožní	umožnit	k5eAaPmIp3nP	umožnit
určit	určit	k5eAaPmF	určit
její	její	k3xOp3gFnSc4	její
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
pojmenovávání	pojmenovávání	k1gNnSc6	pojmenovávání
pak	pak	k6eAd1	pak
má	mít	k5eAaImIp3nS	mít
přednost	přednost	k1gFnSc4	přednost
návrh	návrh	k1gInSc1	návrh
objevitele	objevitel	k1gMnSc2	objevitel
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
v	v	k7c4	v
oznámení	oznámení	k1gNnSc4	oznámení
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
že	že	k8xS	že
Haumea	Haumea	k1gFnSc1	Haumea
byla	být	k5eAaImAgFnS	být
uznána	uznán	k2eAgFnSc1d1	uznána
jako	jako	k8xC	jako
trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
žádný	žádný	k3yNgMnSc1	žádný
objevitel	objevitel	k1gMnSc1	objevitel
zmíněn	zmíněn	k2eAgMnSc1d1	zmíněn
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
místo	místo	k1gNnSc1	místo
objevu	objev	k1gInSc2	objev
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
Observatoř	observatoř	k1gFnSc1	observatoř
Sierra	Sierra	k1gFnSc1	Sierra
Nevada	Nevada	k1gFnSc1	Nevada
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
španělský	španělský	k2eAgInSc4d1	španělský
návrh	návrh	k1gInSc4	návrh
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
těleso	těleso	k1gNnSc4	těleso
po	po	k7c6	po
iberské	iberský	k2eAgFnSc6d1	iberská
bohyni	bohyně	k1gFnSc6	bohyně
Ataecina	Ataecin	k1gMnSc2	Ataecin
nebyl	být	k5eNaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
a	a	k8xC	a
těleso	těleso	k1gNnSc1	těleso
dostalo	dostat	k5eAaPmAgNnS	dostat
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
kalifornského	kalifornský	k2eAgInSc2d1	kalifornský
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
==	==	k?	==
</s>
</p>
<p>
<s>
Haumea	Haumea	k1gFnSc1	Haumea
má	mít	k5eAaImIp3nS	mít
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
typickou	typický	k2eAgFnSc4d1	typická
pro	pro	k7c4	pro
klasické	klasický	k2eAgInPc4d1	klasický
objekty	objekt	k1gInPc4	objekt
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
s	s	k7c7	s
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dobou	doba	k1gFnSc7	doba
282	[number]	k4	282
pozemských	pozemský	k2eAgNnPc2d1	pozemské
let	léto	k1gNnPc2	léto
a	a	k8xC	a
sklonem	sklon	k1gInSc7	sklon
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
k	k	k7c3	k
rovině	rovina	k1gFnSc3	rovina
ekliptiky	ekliptika	k1gFnSc2	ekliptika
28	[number]	k4	28
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Perihéliem	perihélium	k1gNnSc7	perihélium
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
34,5	[number]	k4	34,5
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
projde	projít	k5eAaPmIp3nS	projít
v	v	k7c6	v
září	září	k1gNnSc6	září
2132	[number]	k4	2132
<g/>
.	.	kIx.	.
</s>
<s>
Aféliem	afélium	k1gNnSc7	afélium
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
51,5	[number]	k4	51,5
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
naposledy	naposledy	k6eAd1	naposledy
prošla	projít	k5eAaPmAgFnS	projít
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stane	stanout	k5eAaPmIp3nS	stanout
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2277	[number]	k4	2277
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
opět	opět	k6eAd1	opět
pomalu	pomalu	k6eAd1	pomalu
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
51	[number]	k4	51
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
Haumey	Haumea	k1gFnSc2	Haumea
má	mít	k5eAaImIp3nS	mít
o	o	k7c4	o
něco	něco	k3yInSc4	něco
výstřednější	výstřední	k2eAgInSc1d2	výstřednější
charakter	charakter	k1gInSc1	charakter
než	než	k8xS	než
dráhy	dráha	k1gFnPc1	dráha
ostatních	ostatní	k2eAgInPc2d1	ostatní
členů	člen	k1gInPc2	člen
její	její	k3xOp3gFnSc2	její
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
slabá	slabý	k2eAgFnSc1d1	slabá
dráhová	dráhový	k2eAgFnSc1d1	dráhová
rezonance	rezonance	k1gFnSc1	rezonance
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
s	s	k7c7	s
Neptunem	Neptun	k1gInSc7	Neptun
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
vlivem	vliv	k1gInSc7	vliv
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
Haumey	Haumea	k1gFnSc2	Haumea
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
poslední	poslední	k2eAgFnSc2d1	poslední
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
mírně	mírně	k6eAd1	mírně
pozměnit	pozměnit	k5eAaPmF	pozměnit
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
tzv.	tzv.	kA	tzv.
Kozaiův	Kozaiův	k2eAgInSc4d1	Kozaiův
efekt	efekt	k1gInSc4	efekt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zvětšování	zvětšování	k1gNnSc4	zvětšování
výstřednosti	výstřednost	k1gFnSc2	výstřednost
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
jejího	její	k3xOp3gInSc2	její
sklonu	sklon	k1gInSc2	sklon
<g/>
.	.	kIx.	.
<g/>
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
Haumey	Haumea	k1gFnSc2	Haumea
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
a	a	k8xC	a
zejména	zejména	k9	zejména
v	v	k7c6	v
perihelu	perihel	k1gInSc6	perihel
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
pod	pod	k7c4	pod
vliv	vliv	k1gInSc4	vliv
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
těleso	těleso	k1gNnSc1	těleso
v	v	k7c6	v
daleké	daleký	k2eAgFnSc6d1	daleká
budoucnosti	budoucnost	k1gFnSc6	budoucnost
(	(	kIx(	(
<g/>
až	až	k9	až
miliarda	miliarda	k4xCgFnSc1	miliarda
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
zamíří	zamířit	k5eAaPmIp3nS	zamířit
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
částí	část	k1gFnPc2	část
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
tenkou	tenký	k2eAgFnSc7d1	tenká
vrstvou	vrstva	k1gFnSc7	vrstva
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
změnilo	změnit	k5eAaPmAgNnS	změnit
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c4	v
kometu	kometa	k1gFnSc4	kometa
desettisíckrát	desettisíckrát	k6eAd1	desettisíckrát
jasnější	jasný	k2eAgFnSc1d2	jasnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
kometa	kometa	k1gFnSc1	kometa
Hale-Bopp	Hale-Bopp	k1gMnSc1	Hale-Bopp
<g/>
.	.	kIx.	.
<g/>
Haumea	Haumea	k1gFnSc1	Haumea
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
zdánlivé	zdánlivý	k2eAgFnSc2d1	zdánlivá
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
velikosti	velikost	k1gFnSc2	velikost
17,5	[number]	k4	17,5
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
snadno	snadno	k6eAd1	snadno
pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
i	i	k8xC	i
velkým	velký	k2eAgInSc7d1	velký
amatérským	amatérský	k2eAgInSc7d1	amatérský
dalekohledem	dalekohled	k1gInSc7	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třetím	třetí	k4xOgInSc7	třetí
nejjasnějším	jasný	k2eAgInSc7d3	nejjasnější
objektem	objekt	k1gInSc7	objekt
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
po	po	k7c6	po
Plutu	Pluto	k1gNnSc6	Pluto
a	a	k8xC	a
Makemake	Makemake	k1gNnSc6	Makemake
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
planety	planeta	k1gFnPc1	planeta
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
planetek	planetka	k1gFnPc2	planetka
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
v	v	k7c6	v
protoplanetárním	protoplanetární	k2eAgInSc6d1	protoplanetární
disku	disk	k1gInSc6	disk
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
dráhách	dráha	k1gFnPc6	dráha
ve	v	k7c6	v
společné	společný	k2eAgFnSc6d1	společná
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
i	i	k9	i
většina	většina	k1gFnSc1	většina
raných	raný	k2eAgInPc2d1	raný
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
nalezení	nalezení	k1gNnSc4	nalezení
vzdálených	vzdálený	k2eAgInPc2d1	vzdálený
objektů	objekt	k1gInPc2	objekt
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
tu	ten	k3xDgFnSc4	ten
část	část	k1gFnSc4	část
oblohy	obloha	k1gFnSc2	obloha
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
společná	společný	k2eAgFnSc1d1	společná
rovina	rovina	k1gFnSc1	rovina
promítá	promítat	k5eAaImIp3nS	promítat
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
ekliptiku	ekliptika	k1gFnSc4	ekliptika
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
však	však	k9	však
začali	začít	k5eAaPmAgMnP	začít
astronomové	astronom	k1gMnPc1	astronom
hledat	hledat	k5eAaImF	hledat
i	i	k9	i
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
byla	být	k5eAaImAgNnP	být
odkloněna	odklonit	k5eAaPmNgNnP	odklonit
na	na	k7c4	na
dráhy	dráha	k1gFnPc4	dráha
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
sklonem	sklon	k1gInSc7	sklon
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
vzdálená	vzdálený	k2eAgNnPc1d1	vzdálené
tělesa	těleso	k1gNnPc1	těleso
s	s	k7c7	s
pomalejším	pomalý	k2eAgInSc7d2	pomalejší
středním	střední	k2eAgInSc7d1	střední
denním	denní	k2eAgInSc7d1	denní
pohybem	pohyb	k1gInSc7	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
těchto	tento	k3xDgInPc2	tento
průzkumů	průzkum	k1gInPc2	průzkum
oblohy	obloha	k1gFnSc2	obloha
byl	být	k5eAaImAgInS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
objev	objev	k1gInSc4	objev
Haumey	Haumea	k1gFnSc2	Haumea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
charakteristika	charakteristika	k1gFnSc1	charakteristika
==	==	k?	==
</s>
</p>
<p>
<s>
Měsíce	měsíc	k1gInPc1	měsíc
obíhající	obíhající	k2eAgInPc1d1	obíhající
kolem	kolem	k7c2	kolem
Haumey	Haumea	k1gFnSc2	Haumea
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
astronomům	astronom	k1gMnPc3	astronom
pomocí	pomoc	k1gFnSc7	pomoc
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Keplerova	Keplerův	k2eAgInSc2d1	Keplerův
zákona	zákon	k1gInSc2	zákon
vypočítat	vypočítat	k5eAaPmF	vypočítat
hmotnost	hmotnost	k1gFnSc4	hmotnost
celého	celý	k2eAgInSc2d1	celý
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
výpočtu	výpočet	k1gInSc2	výpočet
<g/>
,	,	kIx,	,
4,006	[number]	k4	4,006
±	±	k?	±
0,04	[number]	k4	0,04
<g/>
×	×	k?	×
<g/>
1021	[number]	k4	1021
kg	kg	kA	kg
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
28	[number]	k4	28
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc6	hmotnost
systému	systém	k1gInSc2	systém
Pluto	Pluto	k1gMnSc1	Pluto
<g/>
–	–	k?	–
<g/>
Charon	Charon	k1gMnSc1	Charon
a	a	k8xC	a
5	[number]	k4	5
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
veškerá	veškerý	k3xTgFnSc1	veškerý
tato	tento	k3xDgFnSc1	tento
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
mateřském	mateřský	k2eAgNnSc6d1	mateřské
tělese	těleso	k1gNnSc6	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jasnost	jasnost	k1gFnSc1	jasnost
Haumey	Haumea	k1gFnSc2	Haumea
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
velké	velký	k2eAgFnPc4d1	velká
fluktuace	fluktuace	k1gFnPc4	fluktuace
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
4	[number]	k4	4
<g/>
hodinových	hodinový	k2eAgFnPc2d1	hodinová
period	perioda	k1gFnPc2	perioda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
lze	lze	k6eAd1	lze
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
jedině	jedině	k6eAd1	jedině
stejně	stejně	k6eAd1	stejně
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
dobou	doba	k1gFnSc7	doba
rotace	rotace	k1gFnSc2	rotace
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
nejen	nejen	k6eAd1	nejen
o	o	k7c4	o
nejrychlejší	rychlý	k2eAgFnSc4d3	nejrychlejší
rotaci	rotace	k1gFnSc4	rotace
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
známých	známý	k2eAgNnPc2d1	známé
hydrostaticky	hydrostaticky	k6eAd1	hydrostaticky
rovnovážných	rovnovážný	k2eAgNnPc2d1	rovnovážné
těles	těleso	k1gNnPc2	těleso
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
o	o	k7c4	o
nejrychlejší	rychlý	k2eAgFnSc4d3	nejrychlejší
rotaci	rotace	k1gFnSc4	rotace
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
známých	známý	k2eAgNnPc2d1	známé
těles	těleso	k1gNnPc2	těleso
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
větším	veliký	k2eAgInSc7d2	veliký
než	než	k8xS	než
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rychlá	rychlý	k2eAgFnSc1d1	rychlá
rotace	rotace	k1gFnSc1	rotace
tělesa	těleso	k1gNnSc2	těleso
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
důsledkem	důsledek	k1gInSc7	důsledek
srážky	srážka	k1gFnSc2	srážka
s	s	k7c7	s
jiným	jiný	k2eAgNnSc7d1	jiné
tělesem	těleso	k1gNnSc7	těleso
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
měsíce	měsíc	k1gInPc1	měsíc
Haumey	Haume	k2eAgInPc1d1	Haume
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
tělesa	těleso	k1gNnSc2	těleso
její	její	k3xOp3gFnSc2	její
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozměry	rozměr	k1gInPc4	rozměr
<g/>
,	,	kIx,	,
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
složení	složení	k1gNnSc4	složení
===	===	k?	===
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
jakéhokoliv	jakýkoliv	k3yIgNnSc2	jakýkoliv
tělesa	těleso	k1gNnSc2	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
zdánlivé	zdánlivý	k2eAgFnSc2d1	zdánlivá
jasnosti	jasnost	k1gFnSc2	jasnost
<g/>
,	,	kIx,	,
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
a	a	k8xC	a
albeda	albed	k1gMnSc2	albed
<g/>
.	.	kIx.	.
</s>
<s>
Tělesa	těleso	k1gNnPc1	těleso
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
pozemským	pozemský	k2eAgMnPc3d1	pozemský
pozorovatelům	pozorovatel	k1gMnPc3	pozorovatel
zdát	zdát	k5eAaImF	zdát
jasná	jasný	k2eAgFnSc1d1	jasná
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
velká	velký	k2eAgNnPc1d1	velké
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
protože	protože	k8xS	protože
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
odrazivý	odrazivý	k2eAgInSc4d1	odrazivý
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
astronomové	astronom	k1gMnPc1	astronom
znají	znát	k5eAaImIp3nP	znát
míru	míra	k1gFnSc4	míra
jejich	jejich	k3xOp3gFnSc2	jejich
odrazivosti	odrazivost	k1gFnSc2	odrazivost
(	(	kIx(	(
<g/>
albedo	albedo	k1gNnSc1	albedo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
mohou	moct	k5eAaImIp3nP	moct
zhruba	zhruba	k6eAd1	zhruba
odhadnout	odhadnout	k5eAaPmF	odhadnout
i	i	k9	i
jejich	jejich	k3xOp3gFnSc4	jejich
velikost	velikost	k1gFnSc4	velikost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
vzdálených	vzdálený	k2eAgNnPc2d1	vzdálené
těles	těleso	k1gNnPc2	těleso
není	být	k5eNaImIp3nS	být
albedo	albedo	k1gNnSc1	albedo
známé	známý	k2eAgNnSc1d1	známé
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Haumea	Haumea	k1gMnSc1	Haumea
je	být	k5eAaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
jasná	jasný	k2eAgFnSc1d1	jasná
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
změřit	změřit	k5eAaPmF	změřit
její	její	k3xOp3gNnSc4	její
tepelné	tepelný	k2eAgNnSc4d1	tepelné
vyzařování	vyzařování	k1gNnSc4	vyzařování
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
astronomové	astronom	k1gMnPc1	astronom
odvodili	odvodit	k5eAaPmAgMnP	odvodit
i	i	k9	i
hodnotu	hodnota	k1gFnSc4	hodnota
jejího	její	k3xOp3gMnSc2	její
albeda	albed	k1gMnSc2	albed
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
velikost	velikost	k1gFnSc1	velikost
<g/>
.	.	kIx.	.
<g/>
Tvar	tvar	k1gInSc1	tvar
Haumey	Haumea	k1gFnSc2	Haumea
byl	být	k5eAaImAgInS	být
zjištěn	zjistit	k5eAaPmNgInS	zjistit
z	z	k7c2	z
fotometrie	fotometrie	k1gFnSc2	fotometrie
(	(	kIx(	(
<g/>
sledování	sledování	k1gNnSc1	sledování
jasnosti	jasnost	k1gFnSc2	jasnost
a	a	k8xC	a
jejích	její	k3xOp3gFnPc2	její
změn	změna	k1gFnPc2	změna
<g/>
)	)	kIx)	)
po	po	k7c6	po
podrobné	podrobný	k2eAgFnSc6d1	podrobná
interpretaci	interpretace	k1gFnSc6	interpretace
světelné	světelný	k2eAgFnSc2d1	světelná
křivky	křivka	k1gFnSc2	křivka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výpočty	výpočet	k1gInPc1	výpočet
přesných	přesný	k2eAgInPc2d1	přesný
rozměrů	rozměr	k1gInPc2	rozměr
však	však	k9	však
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
její	její	k3xOp3gFnSc1	její
rychlá	rychlý	k2eAgFnSc1d1	rychlá
rotace	rotace	k1gFnSc1	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
zákony	zákon	k1gInPc1	zákon
platné	platný	k2eAgInPc1d1	platný
pro	pro	k7c4	pro
rotaci	rotace	k1gFnSc4	rotace
tvárných	tvárný	k2eAgNnPc2d1	tvárné
těles	těleso	k1gNnPc2	těleso
předpovídají	předpovídat	k5eAaImIp3nP	předpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tělesu	těleso	k1gNnSc3	těleso
jako	jako	k8xC	jako
Haumea	Haumea	k1gFnSc1	Haumea
stačí	stačit	k5eAaBmIp3nS	stačit
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
rychlosti	rychlost	k1gFnSc6	rychlost
otáčení	otáčení	k1gNnSc2	otáčení
k	k	k7c3	k
nabytí	nabytí	k1gNnSc3	nabytí
rovnovážného	rovnovážný	k2eAgInSc2d1	rovnovážný
tvaru	tvar	k1gInSc2	tvar
trojosého	trojosý	k2eAgInSc2d1	trojosý
elipsoidu	elipsoid	k1gInSc2	elipsoid
méně	málo	k6eAd2	málo
než	než	k8xS	než
100	[number]	k4	100
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
změn	změna	k1gFnPc2	změna
jasnosti	jasnost	k1gFnSc2	jasnost
Haumey	Haume	k1gMnPc4	Haume
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
není	být	k5eNaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
místními	místní	k2eAgInPc7d1	místní
rozdíly	rozdíl	k1gInPc7	rozdíl
v	v	k7c6	v
albedu	albed	k1gMnSc6	albed
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozorovatelům	pozorovatel	k1gMnPc3	pozorovatel
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
rotující	rotující	k2eAgNnSc1d1	rotující
těleso	těleso	k1gNnSc1	těleso
nastavuje	nastavovat	k5eAaImIp3nS	nastavovat
k	k	k7c3	k
pohledu	pohled	k1gInSc2	pohled
střídavě	střídavě	k6eAd1	střídavě
zboku	zboku	k6eAd1	zboku
a	a	k8xC	a
shora	shora	k6eAd1	shora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rotace	rotace	k1gFnSc1	rotace
a	a	k8xC	a
amplituda	amplituda	k1gFnSc1	amplituda
světelné	světelný	k2eAgFnSc2d1	světelná
křivky	křivka	k1gFnSc2	křivka
Haumey	Haumea	k1gFnSc2	Haumea
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
odvodit	odvodit	k5eAaPmF	odvodit
její	její	k3xOp3gNnSc4	její
složení	složení	k1gNnSc4	složení
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
Haumea	Haumea	k1gFnSc1	Haumea
měla	mít	k5eAaImAgFnS	mít
nízkou	nízký	k2eAgFnSc4d1	nízká
hustotu	hustota	k1gFnSc4	hustota
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Pluto	Pluto	k1gMnSc1	Pluto
<g/>
)	)	kIx)	)
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
ledovým	ledový	k2eAgInSc7d1	ledový
pláštěm	plášť	k1gInSc7	plášť
pokrývajícím	pokrývající	k2eAgInSc7d1	pokrývající
malé	malý	k2eAgNnSc1d1	malé
kamenné	kamenný	k2eAgNnSc1d1	kamenné
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
rychlá	rychlý	k2eAgFnSc1d1	rychlá
rotace	rotace	k1gFnSc1	rotace
protáhla	protáhnout	k5eAaPmAgFnS	protáhnout
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
jak	jak	k8xS	jak
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
jasnosti	jasnost	k1gFnSc6	jasnost
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
tedy	tedy	k9	tedy
došli	dojít	k5eAaPmAgMnP	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
hustota	hustota	k1gFnSc1	hustota
tělesa	těleso	k1gNnSc2	těleso
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
2,6	[number]	k4	2,6
<g/>
–	–	k?	–
<g/>
3,3	[number]	k4	3,3
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Haumea	Haumea	k1gFnSc1	Haumea
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
mnoho	mnoho	k4c4	mnoho
jiných	jiný	k2eAgNnPc2d1	jiné
těles	těleso	k1gNnPc2	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
)	)	kIx)	)
z	z	k7c2	z
křemičitých	křemičitý	k2eAgInPc2d1	křemičitý
minerálů	minerál	k1gInPc2	minerál
jako	jako	k8xS	jako
olivín	olivín	k1gInSc4	olivín
či	či	k8xC	či
pyroxen	pyroxen	k1gInSc4	pyroxen
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hornina	hornina	k1gFnSc1	hornina
je	být	k5eAaImIp3nS	být
pokryta	pokryt	k2eAgNnPc4d1	pokryto
relativně	relativně	k6eAd1	relativně
tenkou	tenký	k2eAgFnSc7d1	tenká
vrstvou	vrstva	k1gFnSc7	vrstva
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgInSc1d1	silný
ledový	ledový	k2eAgInSc1d1	ledový
plášť	plášť	k1gInSc1	plášť
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
typičtější	typický	k2eAgInSc1d2	typičtější
pro	pro	k7c4	pro
objekty	objekt	k1gInPc4	objekt
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
odmrštěn	odmrštit	k5eAaPmNgInS	odmrštit
při	při	k7c6	při
impaktu	impakt	k1gInSc6	impakt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
všechna	všechen	k3xTgNnPc4	všechen
tělesa	těleso	k1gNnSc2	těleso
její	její	k3xOp3gFnSc2	její
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
<g/>
Čím	co	k3yRnSc7	co
je	být	k5eAaImIp3nS	být
těleso	těleso	k1gNnSc4	těleso
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
hydrostatické	hydrostatický	k2eAgFnSc6d1	hydrostatická
rovnováze	rovnováha	k1gFnSc6	rovnováha
hustší	hustý	k2eAgFnSc6d2	hustší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
kulovější	kulový	k2eAgInPc4d2	kulový
tvar	tvar	k1gInSc4	tvar
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
danou	daný	k2eAgFnSc4d1	daná
periodu	perioda	k1gFnSc4	perioda
rotace	rotace	k1gFnSc2	rotace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
rovněž	rovněž	k9	rovněž
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vymezit	vymezit	k5eAaPmF	vymezit
rozměry	rozměr	k1gInPc4	rozměr
Haumey	Haumea	k1gFnSc2	Haumea
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
přesně	přesně	k6eAd1	přesně
známé	známý	k2eAgFnSc3d1	známá
hmotnosti	hmotnost	k1gFnSc3	hmotnost
a	a	k8xC	a
rotaci	rotace	k1gFnSc3	rotace
a	a	k8xC	a
k	k	k7c3	k
odvozené	odvozený	k2eAgFnSc3d1	odvozená
hustotě	hustota	k1gFnSc3	hustota
lze	lze	k6eAd1	lze
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
Haumea	Haumea	k1gFnSc1	Haumea
má	mít	k5eAaImIp3nS	mít
podél	podél	k7c2	podél
své	svůj	k3xOyFgFnSc2	svůj
nejdelší	dlouhý	k2eAgFnSc2d3	nejdelší
osy	osa	k1gFnSc2	osa
průměr	průměr	k1gInSc1	průměr
přibližně	přibližně	k6eAd1	přibližně
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xS	jako
Pluto	plut	k2eAgNnSc1d1	Pluto
<g/>
,	,	kIx,	,
a	a	k8xC	a
mezi	mezi	k7c4	mezi
póly	pól	k1gInPc4	pól
pak	pak	k6eAd1	pak
asi	asi	k9	asi
poloviční	poloviční	k2eAgMnSc1d1	poloviční
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
zatím	zatím	k6eAd1	zatím
u	u	k7c2	u
tohoto	tento	k3xDgNnSc2	tento
tělesa	těleso	k1gNnSc2	těleso
nebyly	být	k5eNaImAgInP	být
pozorovány	pozorován	k2eAgInPc1d1	pozorován
žádné	žádný	k3yNgInPc1	žádný
zákryty	zákryt	k1gInPc1	zákryt
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
již	již	k6eAd1	již
hvězd	hvězda	k1gFnPc2	hvězda
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gInPc2	jeho
vlastních	vlastní	k2eAgInPc2d1	vlastní
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
neexistují	existovat	k5eNaImIp3nP	existovat
zatím	zatím	k6eAd1	zatím
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
)	)	kIx)	)
ani	ani	k8xC	ani
žádná	žádný	k3yNgNnPc4	žádný
přímá	přímý	k2eAgNnPc4d1	přímé
měření	měření	k1gNnPc4	měření
těchto	tento	k3xDgMnPc2	tento
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Astronomové	astronom	k1gMnPc1	astronom
sestavili	sestavit	k5eAaPmAgMnP	sestavit
několik	několik	k4yIc4	několik
modelů	model	k1gInPc2	model
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
různé	různý	k2eAgInPc4d1	různý
rozměry	rozměr	k1gInPc4	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
sestavený	sestavený	k2eAgInSc1d1	sestavený
Rabinowitzem	Rabinowitz	k1gInSc7	Rabinowitz
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
objevu	objev	k1gInSc6	objev
Haumey	Haumea	k1gFnSc2	Haumea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
pozorování	pozorování	k1gNnSc2	pozorování
světelné	světelný	k2eAgFnSc2d1	světelná
křivky	křivka	k1gFnSc2	křivka
viditelného	viditelný	k2eAgNnSc2d1	viditelné
spektra	spektrum	k1gNnSc2	spektrum
pozemními	pozemní	k2eAgInPc7d1	pozemní
teleskopy	teleskop	k1gInPc7	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
měření	měření	k1gNnSc2	měření
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jakým	jaký	k3yIgInSc7	jaký
úhlem	úhel	k1gInSc7	úhel
vidíme	vidět	k5eAaImIp1nP	vidět
osu	osa	k1gFnSc4	osa
rotace	rotace	k1gFnSc2	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
úhlu	úhel	k1gInSc6	úhel
90	[number]	k4	90
<g/>
°	°	k?	°
a	a	k8xC	a
uvažovaném	uvažovaný	k2eAgMnSc6d1	uvažovaný
albedu	albed	k1gMnSc6	albed
0,73	[number]	k4	0,73
by	by	kYmCp3nP	by
rozměry	rozměra	k1gFnPc1	rozměra
tělesa	těleso	k1gNnSc2	těleso
byly	být	k5eAaImAgFnP	být
1960	[number]	k4	1960
×	×	k?	×
1518	[number]	k4	1518
×	×	k?	×
996	[number]	k4	996
km	km	kA	km
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
osa	osa	k1gFnSc1	osa
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
osou	osa	k1gFnSc7	osa
rotace	rotace	k1gFnSc2	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
studie	studie	k1gFnSc2	studie
pokládají	pokládat	k5eAaImIp3nP	pokládat
za	za	k7c4	za
pravděpodobné	pravděpodobný	k2eAgNnSc4d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
osa	osa	k1gFnSc1	osa
rotace	rotace	k1gFnSc1	rotace
se	se	k3xPyFc4	se
kryje	krýt	k5eAaImIp3nS	krýt
s	s	k7c7	s
rovinou	rovina	k1gFnSc7	rovina
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
satelitu	satelit	k1gInSc2	satelit
Hiʻ	Hiʻ	k1gMnPc2	Hiʻ
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
úhel	úhel	k1gInSc1	úhel
činí	činit	k5eAaImIp3nS	činit
86	[number]	k4	86
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
a	a	k8xC	a
skutečné	skutečný	k2eAgInPc1d1	skutečný
rozměry	rozměr	k1gInPc1	rozměr
Haumey	Haume	k2eAgInPc1d1	Haume
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
velmi	velmi	k6eAd1	velmi
blízké	blízký	k2eAgNnSc4d1	blízké
uvedeným	uvedený	k2eAgInPc3d1	uvedený
údajům	údaj	k1gInPc3	údaj
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
však	však	k9	však
pól	pól	k1gInSc1	pól
osy	osa	k1gFnSc2	osa
rotace	rotace	k1gFnSc2	rotace
byl	být	k5eAaImAgInS	být
orientován	orientovat	k5eAaBmNgInS	orientovat
více	hodně	k6eAd2	hodně
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
potom	potom	k8xC	potom
by	by	kYmCp3nS	by
tvar	tvar	k1gInSc1	tvar
tělesa	těleso	k1gNnSc2	těleso
byl	být	k5eAaImAgInS	být
ještě	ještě	k6eAd1	ještě
protaženější	protažený	k2eAgInSc1d2	protažený
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
současně	současně	k6eAd1	současně
bylo	být	k5eAaImAgNnS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
nižší	nízký	k2eAgFnSc1d2	nižší
i	i	k8xC	i
albedo	albedo	k1gNnSc1	albedo
(	(	kIx(	(
<g/>
0,6	[number]	k4	0,6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
při	při	k7c6	při
úhlu	úhel	k1gInSc6	úhel
47	[number]	k4	47
<g/>
°	°	k?	°
by	by	kYmCp3nP	by
rozměry	rozměr	k1gInPc1	rozměr
Haumey	Haumea	k1gFnSc2	Haumea
byly	být	k5eAaImAgFnP	být
2500	[number]	k4	2500
×	×	k?	×
1080	[number]	k4	1080
×	×	k?	×
860	[number]	k4	860
km	km	kA	km
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
analýzy	analýza	k1gFnSc2	analýza
světelné	světelný	k2eAgFnSc2d1	světelná
křivky	křivka	k1gFnSc2	křivka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
zveřejnili	zveřejnit	k5eAaPmAgMnP	zveřejnit
Pedro	Pedro	k1gNnSc4	Pedro
Lacerda	Lacerda	k1gMnSc1	Lacerda
a	a	k8xC	a
David	David	k1gMnSc1	David
C.	C.	kA	C.
Hewitt	Hewitt	k1gMnSc1	Hewitt
z	z	k7c2	z
havajského	havajský	k2eAgInSc2d1	havajský
Institute	institut	k1gInSc5	institut
for	forum	k1gNnPc2	forum
Astronomy	astronom	k1gMnPc7	astronom
<g/>
,	,	kIx,	,
vyšel	vyjít	k5eAaPmAgInS	vyjít
průměr	průměr	k1gInSc4	průměr
ekvivalentního	ekvivalentní	k2eAgNnSc2d1	ekvivalentní
kulatého	kulatý	k2eAgNnSc2d1	kulaté
tělesa	těleso	k1gNnSc2	těleso
1450	[number]	k4	1450
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
fotometrických	fotometrický	k2eAgNnPc2d1	fotometrické
pozorování	pozorování	k1gNnPc2	pozorování
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
světla	světlo	k1gNnSc2	světlo
o	o	k7c6	o
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
70	[number]	k4	70
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
,	,	kIx,	,
provedených	provedený	k2eAgInPc2d1	provedený
pomocí	pomocí	k7c2	pomocí
Spitzerova	Spitzerův	k2eAgInSc2d1	Spitzerův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zase	zase	k9	zase
určen	určen	k2eAgInSc1d1	určen
průměr	průměr	k1gInSc1	průměr
1150	[number]	k4	1150
+250	+250	k4	+250
<g/>
−	−	k?	−
<g/>
100	[number]	k4	100
km	km	kA	km
a	a	k8xC	a
albedo	albedo	k1gNnSc4	albedo
0,84	[number]	k4	0,84
+0,1	+0,1	k4	+0,1
<g/>
−	−	k?	−
<g/>
0,2	[number]	k4	0,2
.	.	kIx.	.
<g/>
Tyto	tento	k3xDgInPc1	tento
nezávislé	závislý	k2eNgInPc1d1	nezávislý
odhady	odhad	k1gInPc1	odhad
rozměrů	rozměr	k1gInPc2	rozměr
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
kolem	kolem	k7c2	kolem
středního	střední	k2eAgInSc2d1	střední
geometrického	geometrický	k2eAgInSc2d1	geometrický
průměru	průměr	k1gInSc2	průměr
1400	[number]	k4	1400
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
třetí	třetí	k4xOgNnSc4	třetí
či	či	k8xC	či
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
největší	veliký	k2eAgNnSc4d3	veliký
transneptunické	transneptunický	k2eAgNnSc4d1	transneptunické
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgNnSc1	jaký
bylo	být	k5eAaImAgNnS	být
dosud	dosud	k6eAd1	dosud
objeveno	objevit	k5eAaPmNgNnS	objevit
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
Eris	Eris	k1gFnSc1	Eris
<g/>
,	,	kIx,	,
Plutu	plut	k2eAgFnSc4d1	Pluta
a	a	k8xC	a
snad	snad	k9	snad
také	také	k9	také
Makemake	Makemake	k1gFnSc1	Makemake
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Sedna	Seden	k2eAgFnSc1d1	Sedna
<g/>
,	,	kIx,	,
Orcus	Orcus	k1gMnSc1	Orcus
či	či	k8xC	či
Quaoar	Quaoar	k1gMnSc1	Quaoar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Povrch	povrch	k1gInSc1	povrch
===	===	k?	===
</s>
</p>
<p>
<s>
Kolísání	kolísání	k1gNnSc1	kolísání
světelné	světelný	k2eAgFnSc2d1	světelná
křivky	křivka	k1gFnSc2	křivka
Haumey	Haumea	k1gFnSc2	Haumea
zapříčiněné	zapříčiněný	k2eAgFnSc2d1	zapříčiněná
jejím	její	k3xOp3gInSc7	její
tvarem	tvar	k1gInSc7	tvar
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
všechny	všechen	k3xTgFnPc4	všechen
její	její	k3xOp3gFnPc4	její
barevné	barevný	k2eAgFnPc4d1	barevná
složky	složka	k1gFnPc4	složka
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Pedro	Pedro	k1gNnSc4	Pedro
Lacerda	Lacerdo	k1gNnSc2	Lacerdo
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
také	také	k9	také
jisté	jistý	k2eAgFnPc4d1	jistá
barevné	barevný	k2eAgFnPc4d1	barevná
odlišnosti	odlišnost	k1gFnPc4	odlišnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
viditelném	viditelný	k2eAgNnSc6d1	viditelné
spektru	spektrum	k1gNnSc6	spektrum
i	i	k8xC	i
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
blízké	blízký	k2eAgFnSc6d1	blízká
infračervenému	infračervený	k2eAgNnSc3d1	infračervené
spektru	spektrum	k1gNnSc3	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
tělesa	těleso	k1gNnSc2	těleso
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
barvou	barva	k1gFnSc7	barva
a	a	k8xC	a
albedem	albed	k1gInSc7	albed
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
jeho	on	k3xPp3gInSc2	on
zbytku	zbytek	k1gInSc2	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
Haumea	Haumea	k6eAd1	Haumea
tedy	tedy	k9	tedy
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
skvrnitý	skvrnitý	k2eAgInSc4d1	skvrnitý
povrch	povrch	k1gInSc4	povrch
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
Pluto	plut	k2eAgNnSc1d1	Pluto
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ne	ne	k9	ne
tak	tak	k9	tak
znatelně	znatelně	k6eAd1	znatelně
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
dalekohledy	dalekohled	k1gInPc7	dalekohled
Keck	Keck	k1gMnSc1	Keck
a	a	k8xC	a
Gemini	Gemin	k2eAgMnPc1d1	Gemin
získaly	získat	k5eAaPmAgFnP	získat
světelná	světelný	k2eAgNnPc4d1	světelné
spektra	spektrum	k1gNnPc4	spektrum
Haumey	Haumea	k1gFnSc2	Haumea
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
svědčila	svědčit	k5eAaImAgFnS	svědčit
o	o	k7c6	o
přítomnosti	přítomnost	k1gFnSc6	přítomnost
krystalického	krystalický	k2eAgInSc2d1	krystalický
ledu	led	k1gInSc2	led
podobného	podobný	k2eAgNnSc2d1	podobné
tomu	ten	k3xDgNnSc3	ten
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Plutova	Plutův	k2eAgInSc2d1	Plutův
měsíce	měsíc	k1gInSc2	měsíc
Charon	Charon	k1gMnSc1	Charon
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
překvapivé	překvapivý	k2eAgNnSc1d1	překvapivé
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
krystalický	krystalický	k2eAgInSc1d1	krystalický
led	led	k1gInSc1	led
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
převyšujících	převyšující	k2eAgFnPc2d1	převyšující
110	[number]	k4	110
K	K	kA	K
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
160	[number]	k4	160
<g/>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
Haumey	Haumea	k1gFnSc2	Haumea
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
50	[number]	k4	50
K	k	k7c3	k
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
220	[number]	k4	220
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
amorfní	amorfní	k2eAgInSc1d1	amorfní
led	led	k1gInSc1	led
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
struktura	struktura	k1gFnSc1	struktura
krystalického	krystalický	k2eAgInSc2d1	krystalický
ledu	led	k1gInSc2	led
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
neustálém	neustálý	k2eAgNnSc6d1	neustálé
bombardování	bombardování	k1gNnSc1	bombardování
kosmickým	kosmický	k2eAgNnSc7d1	kosmické
zářením	záření	k1gNnSc7	záření
a	a	k8xC	a
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
<g/>
,	,	kIx,	,
kterému	který	k3yIgNnSc3	který
jsou	být	k5eAaImIp3nP	být
povrchy	povrch	k1gInPc1	povrch
transneptunických	transneptunický	k2eAgNnPc2d1	transneptunické
těles	těleso	k1gNnPc2	těleso
vystaveny	vystaven	k2eAgInPc1d1	vystaven
<g/>
,	,	kIx,	,
nestabilní	stabilní	k2eNgInPc1d1	nestabilní
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
těchto	tento	k3xDgFnPc2	tento
podmínek	podmínka	k1gFnPc2	podmínka
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
krystalický	krystalický	k2eAgInSc1d1	krystalický
led	led	k1gInSc1	led
měl	mít	k5eAaImAgInS	mít
přeměnit	přeměnit	k5eAaPmF	přeměnit
v	v	k7c4	v
amorfní	amorfní	k2eAgInSc4d1	amorfní
do	do	k7c2	do
deseti	deset	k4xCc2	deset
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
transneptunické	transneptunický	k2eAgInPc1d1	transneptunický
objekty	objekt	k1gInPc1	objekt
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
drahách	draha	k1gFnPc6	draha
v	v	k7c6	v
chladných	chladný	k2eAgFnPc6d1	chladná
oblastech	oblast	k1gFnPc6	oblast
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
už	už	k6eAd1	už
miliardy	miliarda	k4xCgFnPc4	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
transneptunických	transneptunický	k2eAgNnPc6d1	transneptunické
tělesech	těleso	k1gNnPc6	těleso
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
led	led	k1gInSc1	led
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
organické	organický	k2eAgFnPc4d1	organická
sloučeniny	sloučenina	k1gFnPc4	sloučenina
jako	jako	k8xS	jako
tholin	tholin	k1gInSc4	tholin
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Pluto	Pluto	k1gNnSc1	Pluto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
radiace	radiace	k1gFnSc1	radiace
také	také	k9	také
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc4	povrch
zčervená	zčervenat	k5eAaPmIp3nS	zčervenat
a	a	k8xC	a
ztmavne	ztmavnout	k5eAaPmIp3nS	ztmavnout
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
lze	lze	k6eAd1	lze
ze	z	k7c2	z
spektra	spektrum	k1gNnSc2	spektrum
a	a	k8xC	a
barvy	barva	k1gFnSc2	barva
Haumey	Haumea	k1gFnSc2	Haumea
usoudit	usoudit	k5eAaPmF	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ona	onen	k3xDgFnSc1	onen
i	i	k9	i
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
její	její	k3xOp3gFnSc2	její
skupiny	skupina	k1gFnSc2	skupina
prošly	projít	k5eAaPmAgFnP	projít
událostí	událost	k1gFnSc7	událost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
změnila	změnit	k5eAaPmAgFnS	změnit
jejich	jejich	k3xOp3gInSc4	jejich
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
čerstvý	čerstvý	k2eAgInSc1d1	čerstvý
led	led	k1gInSc1	led
<g/>
.	.	kIx.	.
<g/>
Haumea	Haumea	k1gFnSc1	Haumea
je	být	k5eAaImIp3nS	být
podobně	podobně	k6eAd1	podobně
jasná	jasný	k2eAgFnSc1d1	jasná
jako	jako	k8xC	jako
sníh	sníh	k1gInSc1	sníh
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
albedo	albedo	k1gNnSc1	albedo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
0,6	[number]	k4	0,6
<g/>
–	–	k?	–
<g/>
0,8	[number]	k4	0,8
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
krystalickému	krystalický	k2eAgInSc3d1	krystalický
ledu	led	k1gInSc3	led
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgNnPc1d1	jiné
velká	velký	k2eAgNnPc1d1	velké
transneptunická	transneptunický	k2eAgNnPc1d1	transneptunické
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Eris	Eris	k1gFnSc1	Eris
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
albedo	albedo	k1gNnSc4	albedo
zřejmě	zřejmě	k6eAd1	zřejmě
také	také	k9	také
tak	tak	k6eAd1	tak
vysoké	vysoký	k2eAgNnSc1d1	vysoké
nebo	nebo	k8xC	nebo
i	i	k9	i
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
66	[number]	k4	66
až	až	k9	až
80	[number]	k4	80
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
Haumey	Haumea	k1gFnSc2	Haumea
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
čistého	čistý	k2eAgInSc2d1	čistý
krystalického	krystalický	k2eAgInSc2d1	krystalický
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
k	k	k7c3	k
jehož	jehož	k3xOyRp3gMnSc3	jehož
vysokému	vysoký	k2eAgMnSc3d1	vysoký
albedu	albed	k1gMnSc3	albed
snad	snad	k9	snad
přispívá	přispívat	k5eAaImIp3nS	přispívat
také	také	k9	také
kyanovodík	kyanovodík	k1gInSc1	kyanovodík
nebo	nebo	k8xC	nebo
jíly	jíl	k1gInPc1	jíl
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
fylosilikátů	fylosilikát	k1gInPc2	fylosilikát
<g/>
.	.	kIx.	.
</s>
<s>
Přítomné	přítomný	k1gMnPc4	přítomný
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k9	také
anorganické	anorganický	k2eAgFnPc4d1	anorganická
soli	sůl	k1gFnPc4	sůl
kyanovodíku	kyanovodík	k1gInSc2	kyanovodík
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
kyanid	kyanid	k1gInSc1	kyanid
měďnato-draselný	měďnatoraselný	k2eAgInSc1d1	měďnato-draselný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
např.	např.	kA	např.
od	od	k7c2	od
tělesa	těleso	k1gNnSc2	těleso
Makemake	Makemak	k1gFnSc2	Makemak
se	se	k3xPyFc4	se
ve	v	k7c6	v
spektru	spektrum	k1gNnSc6	spektrum
Haumey	Haumea	k1gFnSc2	Haumea
nenalézá	nalézat	k5eNaImIp3nS	nalézat
žádné	žádný	k3yNgNnSc1	žádný
měřitelné	měřitelný	k2eAgNnSc1d1	měřitelné
množství	množství	k1gNnSc1	množství
methanu	methan	k1gInSc2	methan
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
methanový	methanový	k2eAgInSc1d1	methanový
led	led	k1gInSc1	led
zde	zde	k6eAd1	zde
netvoří	tvořit	k5eNaImIp3nS	tvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
dočasnému	dočasný	k2eAgInSc3d1	dočasný
oteplení	oteplení	k1gNnSc3	oteplení
způsobenému	způsobený	k2eAgInSc3d1	způsobený
kolizí	kolize	k1gFnPc2	kolize
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
podobné	podobný	k2eAgFnPc1d1	podobná
těkavé	těkavý	k2eAgFnPc1d1	těkavá
látky	látka	k1gFnPc1	látka
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
odstranilo	odstranit	k5eAaPmAgNnS	odstranit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Prstenec	prstenec	k1gInSc4	prstenec
===	===	k?	===
</s>
</p>
<p>
<s>
Pozorováním	pozorování	k1gNnSc7	pozorování
hvězdného	hvězdný	k2eAgInSc2d1	hvězdný
zákrytu	zákryt	k1gInSc2	zákryt
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Haumea	Haumea	k1gFnSc1	Haumea
má	mít	k5eAaImIp3nS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
prstenec	prstenec	k1gInSc1	prstenec
o	o	k7c4	o
průměr	průměr	k1gInSc4	průměr
zhruba	zhruba	k6eAd1	zhruba
2287	[number]	k4	2287
km	km	kA	km
a	a	k8xC	a
šířce	šířka	k1gFnSc6	šířka
ca	ca	kA	ca
70	[number]	k4	70
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tak	tak	k9	tak
o	o	k7c4	o
první	první	k4xOgInSc4	první
prokázaný	prokázaný	k2eAgInSc4d1	prokázaný
prstenec	prstenec	k1gInSc4	prstenec
u	u	k7c2	u
transneptunického	transneptunický	k2eAgNnSc2d1	transneptunické
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Měsíce	měsíc	k1gInSc2	měsíc
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
Brownův	Brownův	k2eAgInSc1d1	Brownův
tým	tým	k1gInSc1	tým
na	na	k7c6	na
Keckově	Keckův	k2eAgFnSc6d1	Keckova
observatoři	observatoř	k1gFnSc6	observatoř
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolem	kolem	k7c2	kolem
Haumey	Haumea	k1gFnSc2	Haumea
obíhají	obíhat	k5eAaImIp3nP	obíhat
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
136108	[number]	k4	136108
<g/>
)	)	kIx)	)
Haumea	Haume	k1gInSc2	Haume
I	i	k9	i
Hiʻ	Hiʻ	k1gFnSc4	Hiʻ
a	a	k8xC	a
(	(	kIx(	(
<g/>
136108	[number]	k4	136108
<g/>
)	)	kIx)	)
Haumea	Haume	k1gInSc2	Haume
II	II	kA	II
Namaka	Namak	k1gMnSc2	Namak
<g/>
.	.	kIx.	.
<g/>
Satelit	satelit	k1gMnSc1	satelit
Hiʻ	Hiʻ	k1gMnSc1	Hiʻ
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tým	tým	k1gInSc1	tým
původně	původně	k6eAd1	původně
neoficiálně	neoficiálně	k6eAd1	neoficiálně
přezdíval	přezdívat	k5eAaImAgMnS	přezdívat
Rudolph	Rudolph	k1gInSc4	Rudolph
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jednoho	jeden	k4xCgMnSc2	jeden
ze	z	k7c2	z
sobů	sob	k1gMnPc2	sob
Santa	Santa	k1gFnSc1	Santa
Clause	Clause	k1gFnSc1	Clause
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
měsíců	měsíc	k1gInPc2	měsíc
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ten	ten	k3xDgInSc1	ten
větší	veliký	k2eAgInSc1d2	veliký
<g/>
,	,	kIx,	,
jasnější	jasný	k2eAgFnSc1d2	jasnější
a	a	k8xC	a
od	od	k7c2	od
mateřského	mateřský	k2eAgNnSc2d1	mateřské
tělesa	těleso	k1gNnSc2	těleso
vzdálenější	vzdálený	k2eAgFnSc1d2	vzdálenější
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
asi	asi	k9	asi
310	[number]	k4	310
km	km	kA	km
a	a	k8xC	a
Haumeu	Haumeus	k1gInSc2	Haumeus
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
km	km	kA	km
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
kruhové	kruhový	k2eAgFnSc6d1	kruhová
dráze	dráha	k1gFnSc6	dráha
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
49	[number]	k4	49
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Absorpční	absorpční	k2eAgFnPc1d1	absorpční
čáry	čára	k1gFnPc1	čára
v	v	k7c6	v
infračerveném	infračervený	k2eAgNnSc6d1	infračervené
spektru	spektrum	k1gNnSc6	spektrum
na	na	k7c6	na
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
1,5	[number]	k4	1,5
a	a	k8xC	a
2	[number]	k4	2
mikrometry	mikrometr	k1gInPc7	mikrometr
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
povrchu	povrch	k1gInSc2	povrch
je	být	k5eAaImIp3nS	být
pokryta	pokryt	k2eAgFnSc1d1	pokryta
téměř	téměř	k6eAd1	téměř
čistým	čistý	k2eAgInSc7d1	čistý
krystalickým	krystalický	k2eAgInSc7d1	krystalický
vodním	vodní	k2eAgInSc7d1	vodní
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
Neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
spektrum	spektrum	k1gNnSc1	spektrum
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
podobnými	podobný	k2eAgFnPc7d1	podobná
absorpčními	absorpční	k2eAgFnPc7d1	absorpční
čarami	čára	k1gFnPc7	čára
ve	v	k7c6	v
spektru	spektrum	k1gNnSc6	spektrum
Haumey	Haumea	k1gFnSc2	Haumea
vedlo	vést	k5eAaImAgNnS	vést
Browna	Browno	k1gNnPc1	Browno
s	s	k7c7	s
kolegy	kolega	k1gMnPc7	kolega
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
zřejmě	zřejmě	k6eAd1	zřejmě
nevznikl	vzniknout	k5eNaPmAgInS	vzniknout
gravitačním	gravitační	k2eAgNnSc7d1	gravitační
zachycením	zachycení	k1gNnSc7	zachycení
menších	malý	k2eAgNnPc2d2	menší
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
měsíce	měsíc	k1gInPc1	měsíc
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
fragmenty	fragment	k1gInPc1	fragment
samotné	samotný	k2eAgInPc1d1	samotný
Haumey	Haumey	k1gInPc7	Haumey
<g/>
.	.	kIx.	.
<g/>
Namaka	Namak	k1gMnSc2	Namak
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc4d2	menší
a	a	k8xC	a
blíže	blízce	k6eAd2	blízce
obíhající	obíhající	k2eAgInSc4d1	obíhající
satelit	satelit	k1gInSc4	satelit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2005	[number]	k4	2005
a	a	k8xC	a
zpočátku	zpočátku	k6eAd1	zpočátku
byl	být	k5eAaImAgInS	být
neoficiálně	oficiálně	k6eNd1	oficiálně
nazýván	nazývat	k5eAaImNgInS	nazývat
po	po	k7c6	po
dalším	další	k2eAgMnSc6d1	další
sobu	sob	k1gMnSc6	sob
Blitzen	Blitzna	k1gFnPc2	Blitzna
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hmotnost	hmotnost	k1gFnSc1	hmotnost
činí	činit	k5eAaImIp3nS	činit
jednu	jeden	k4xCgFnSc4	jeden
desetinu	desetina	k1gFnSc4	desetina
hmotnosti	hmotnost	k1gFnSc2	hmotnost
satelitu	satelit	k1gInSc2	satelit
Hiʻ	Hiʻ	k1gFnSc2	Hiʻ
a	a	k8xC	a
Haumeu	Haumeus	k1gInSc2	Haumeus
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
po	po	k7c6	po
velmi	velmi	k6eAd1	velmi
eliptické	eliptický	k2eAgFnSc6d1	eliptická
dráze	dráha	k1gFnSc6	dráha
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
18	[number]	k4	18
dní	den	k1gInPc2	den
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
39	[number]	k4	39
tisíc	tisíc	k4xCgInPc2	tisíc
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
oběh	oběh	k1gInSc1	oběh
je	být	k5eAaImIp3nS	být
větším	veliký	k2eAgInSc7d2	veliký
měsícem	měsíc	k1gInSc7	měsíc
gravitačně	gravitačně	k6eAd1	gravitačně
rušen	rušit	k5eAaImNgInS	rušit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
je	být	k5eAaImIp3nS	být
jeho	on	k3xPp3gInSc4	on
sklon	sklon	k1gInSc4	sklon
vůči	vůči	k7c3	vůči
dráze	dráha	k1gFnSc3	dráha
většího	veliký	k2eAgInSc2d2	veliký
měsíce	měsíc	k1gInSc2	měsíc
13	[number]	k4	13
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
překvapivé	překvapivý	k2eAgNnSc1d1	překvapivé
<g/>
,	,	kIx,	,
že	že	k8xS	že
relativně	relativně	k6eAd1	relativně
velké	velký	k2eAgFnSc6d1	velká
výstřednosti	výstřednost	k1gFnSc6	výstřednost
drah	draha	k1gFnPc2	draha
obou	dva	k4xCgInPc2	dva
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
vzájemný	vzájemný	k2eAgInSc4d1	vzájemný
sklon	sklon	k1gInSc4	sklon
nebyly	být	k5eNaImAgFnP	být
zmenšeny	zmenšen	k2eAgFnPc1d1	zmenšena
jejich	jejich	k3xOp3gNnSc7	jejich
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
slapovým	slapový	k2eAgNnSc7d1	slapové
působením	působení	k1gNnSc7	působení
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
by	by	kYmCp3nS	by
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
složité	složitý	k2eAgInPc4d1	složitý
rezonanční	rezonanční	k2eAgInPc4d1	rezonanční
vztahy	vztah	k1gInPc4	vztah
jejich	jejich	k3xOp3gFnPc2	jejich
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
oběžné	oběžný	k2eAgFnPc1d1	oběžná
dráhy	dráha	k1gFnPc1	dráha
obou	dva	k4xCgInPc2	dva
měsíců	měsíc	k1gInPc2	měsíc
vůči	vůči	k7c3	vůči
Zemi	zem	k1gFnSc6	zem
orientovány	orientovat	k5eAaBmNgInP	orientovat
téměř	téměř	k6eAd1	téměř
bočně	bočně	k6eAd1	bočně
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
občas	občas	k6eAd1	občas
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
zákrytu	zákryt	k1gInSc3	zákryt
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
takových	takový	k3xDgInPc2	takový
přechodů	přechod	k1gInPc2	přechod
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
upřesnit	upřesnit	k5eAaPmF	upřesnit
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
a	a	k8xC	a
tvaru	tvar	k1gInSc6	tvar
Haumey	Haumea	k1gMnSc2	Haumea
i	i	k8xC	i
těchto	tento	k3xDgInPc2	tento
satelitů	satelit	k1gInPc2	satelit
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
Pluta	Pluto	k1gNnSc2	Pluto
a	a	k8xC	a
Charonu	Charon	k1gMnSc6	Charon
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
jasnosti	jasnost	k1gFnSc2	jasnost
systému	systém	k1gInSc2	systém
během	během	k7c2	během
zákrytů	zákryt	k1gInPc2	zákryt
budou	být	k5eAaImBp3nP	být
nepatrné	nepatrný	k2eAgInPc1d1	nepatrný
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
klade	klást	k5eAaImIp3nS	klást
poměrně	poměrně	k6eAd1	poměrně
vysoké	vysoký	k2eAgInPc4d1	vysoký
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
pozorovacího	pozorovací	k2eAgNnSc2d1	pozorovací
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zákrytu	zákryt	k1gInSc3	zákryt
Hiʻ	Hiʻ	k1gFnSc4	Hiʻ
došlo	dojít	k5eAaPmAgNnS	dojít
naposledy	naposledy	k6eAd1	naposledy
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jen	jen	k9	jen
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
před	před	k7c7	před
objevem	objev	k1gInSc7	objev
Haumey	Haumea	k1gFnSc2	Haumea
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
nastane	nastat	k5eAaPmIp3nS	nastat
až	až	k9	až
za	za	k7c4	za
přibližně	přibližně	k6eAd1	přibližně
130	[number]	k4	130
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
však	však	k9	však
u	u	k7c2	u
Namaky	Namak	k1gInPc5	Namak
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
pravidelně	pravidelně	k6eAd1	pravidelně
obíhajících	obíhající	k2eAgInPc2d1	obíhající
měsíců	měsíc	k1gInPc2	měsíc
jsou	být	k5eAaImIp3nP	být
zákryty	zákryt	k1gInPc1	zákryt
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
,	,	kIx,	,
díky	dík	k1gInPc1	dík
vlivu	vliv	k1gInSc2	vliv
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc1	jaký
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
oběh	oběh	k1gInSc4	oběh
Namaky	Namak	k1gInPc4	Namak
Hiʻ	Hiʻ	k1gMnPc3	Hiʻ
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
nyní	nyní	k6eAd1	nyní
její	její	k3xOp3gFnSc1	její
dráha	dráha	k1gFnSc1	dráha
v	v	k7c6	v
příznivém	příznivý	k2eAgInSc6d1	příznivý
úhlu	úhel	k1gInSc6	úhel
po	po	k7c4	po
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
Haumey	Haumea	k1gFnSc2	Haumea
==	==	k?	==
</s>
</p>
<p>
<s>
Haumea	Haumea	k1gMnSc1	Haumea
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
členem	člen	k1gMnSc7	člen
rodiny	rodina	k1gFnSc2	rodina
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
byl	být	k5eAaImAgMnS	být
jejich	jejich	k3xOp3gMnSc1	jejich
větší	veliký	k2eAgMnSc1d2	veliký
předchůdce	předchůdce	k1gMnSc1	předchůdce
zničen	zničit	k5eAaPmNgInS	zničit
srážkou	srážka	k1gFnSc7	srážka
s	s	k7c7	s
jiným	jiný	k2eAgNnSc7d1	jiné
tělesem	těleso	k1gNnSc7	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
prvotní	prvotní	k2eAgNnSc1d1	prvotní
těleso	těleso	k1gNnSc1	těleso
bylo	být	k5eAaImAgNnS	být
velké	velký	k2eAgNnSc1d1	velké
přibližně	přibližně	k6eAd1	přibližně
jako	jako	k9	jako
Pluto	Pluto	k1gMnSc1	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
první	první	k4xOgFnSc4	první
skupinu	skupina	k1gFnSc4	skupina
těles	těleso	k1gNnPc2	těleso
identifikovanou	identifikovaný	k2eAgFnSc4d1	identifikovaná
mezi	mezi	k7c7	mezi
transneptunickými	transneptunický	k2eAgInPc7d1	transneptunický
objekty	objekt	k1gInPc7	objekt
a	a	k8xC	a
kromě	kromě	k7c2	kromě
Haumey	Haumea	k1gFnSc2	Haumea
a	a	k8xC	a
jejích	její	k3xOp3gInPc2	její
měsíců	měsíc	k1gInPc2	měsíc
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
následující	následující	k2eAgFnPc4d1	následující
planetky	planetka	k1gFnPc4	planetka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
24835	[number]	k4	24835
<g/>
)	)	kIx)	)
1995	[number]	k4	1995
SM55	SM55	k1gFnPc2	SM55
(	(	kIx(	(
<g/>
odhadovaný	odhadovaný	k2eAgInSc1d1	odhadovaný
průměr	průměr	k1gInSc1	průměr
700	[number]	k4	700
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
19308	[number]	k4	19308
<g/>
)	)	kIx)	)
1996	[number]	k4	1996
TO66	TO66	k1gFnPc2	TO66
(	(	kIx(	(
<g/>
500	[number]	k4	500
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
55636	[number]	k4	55636
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
TX300	TX300	k1gFnPc2	TX300
(	(	kIx(	(
<g/>
600	[number]	k4	600
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
120178	[number]	k4	120178
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
OP32	OP32	k1gFnPc2	OP32
(	(	kIx(	(
<g/>
700	[number]	k4	700
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
145453	[number]	k4	145453
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
RR43	RR43	k1gFnPc2	RR43
(	(	kIx(	(
<g/>
700	[number]	k4	700
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
Tato	tento	k3xDgNnPc1	tento
tělesa	těleso	k1gNnPc1	těleso
mají	mít	k5eAaImIp3nP	mít
podobný	podobný	k2eAgInSc4d1	podobný
charakter	charakter	k1gInSc4	charakter
spektra	spektrum	k1gNnSc2	spektrum
blízko	blízko	k7c2	blízko
infračervené	infračervený	k2eAgFnSc2d1	infračervená
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
podobného	podobný	k2eAgNnSc2d1	podobné
spektra	spektrum	k1gNnSc2	spektrum
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
podobné	podobný	k2eAgNnSc4d1	podobné
složení	složení	k1gNnSc4	složení
a	a	k8xC	a
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
je	být	k5eAaImIp3nS	být
zase	zase	k9	zase
možné	možný	k2eAgNnSc1d1	možné
usuzovat	usuzovat	k5eAaImF	usuzovat
na	na	k7c4	na
stejný	stejný	k2eAgInSc4d1	stejný
původ	původ	k1gInSc4	původ
těchto	tento	k3xDgNnPc2	tento
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existence	existence	k1gFnSc1	existence
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Haumea	Haumea	k1gFnSc1	Haumea
i	i	k8xC	i
její	její	k3xOp3gMnPc1	její
potomci	potomek	k1gMnPc1	potomek
mají	mít	k5eAaImIp3nP	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
rozptýleném	rozptýlený	k2eAgInSc6d1	rozptýlený
disku	disk	k1gInSc6	disk
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
kdyby	kdyby	kYmCp3nS	kdyby
byla	být	k5eAaImAgFnS	být
vzata	vzat	k2eAgFnSc1d1	vzata
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
celá	celý	k2eAgFnSc1d1	celá
doba	doba	k1gFnSc1	doba
existence	existence	k1gFnSc2	existence
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
by	by	kYmCp3nS	by
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
velmi	velmi	k6eAd1	velmi
řídkém	řídký	k2eAgInSc6d1	řídký
Kuiperově	Kuiperův	k2eAgInSc6d1	Kuiperův
pásu	pás	k1gInSc6	pás
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
podobné	podobný	k2eAgFnSc2d1	podobná
kolize	kolize	k1gFnSc2	kolize
0,1	[number]	k4	0,1
procenta	procento	k1gNnSc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
nemohla	moct	k5eNaImAgFnS	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
v	v	k7c6	v
Kuiperově	Kuiperův	k2eAgInSc6d1	Kuiperův
pásu	pás	k1gInSc6	pás
ani	ani	k8xC	ani
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
mnohem	mnohem	k6eAd1	mnohem
hustší	hustý	k2eAgInSc1d2	hustší
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
zase	zase	k9	zase
svou	svůj	k3xOyFgFnSc7	svůj
gravitací	gravitace	k1gFnSc7	gravitace
roztrhal	roztrhat	k5eAaPmAgInS	roztrhat
Neptun	Neptun	k1gInSc1	Neptun
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
své	svůj	k3xOyFgFnSc2	svůj
migrace	migrace	k1gFnSc2	migrace
na	na	k7c4	na
dnešní	dnešní	k2eAgFnSc4d1	dnešní
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dávná	dávný	k2eAgFnSc1d1	dávná
migrace	migrace	k1gFnSc1	migrace
Neptunu	Neptun	k1gInSc2	Neptun
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
také	také	k9	také
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
Kuiperově	Kuiperův	k2eAgInSc6d1	Kuiperův
pásu	pás	k1gInSc6	pás
tak	tak	k9	tak
málo	málo	k4c4	málo
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
skupina	skupina	k1gFnSc1	skupina
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
dynamického	dynamický	k2eAgInSc2d1	dynamický
rozptýleného	rozptýlený	k2eAgInSc2d1	rozptýlený
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
podobné	podobný	k2eAgFnSc2d1	podobná
srážky	srážka	k1gFnSc2	srážka
daleko	daleko	k6eAd1	daleko
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
<g/>
Skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
poměrně	poměrně	k6eAd1	poměrně
rozptýlená	rozptýlený	k2eAgFnSc1d1	rozptýlená
<g/>
,	,	kIx,	,
a	a	k8xC	a
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
stavu	stav	k1gInSc3	stav
muselo	muset	k5eAaImAgNnS	muset
trvat	trvat	k5eAaImF	trvat
miliardy	miliarda	k4xCgFnPc4	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kolize	kolize	k1gFnSc1	kolize
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ji	on	k3xPp3gFnSc4	on
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
odehrála	odehrát	k5eAaPmAgFnS	odehrát
již	již	k6eAd1	již
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
historii	historie	k1gFnSc6	historie
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Impakt	Impakt	k1gInSc1	Impakt
však	však	k9	však
nemusel	muset	k5eNaImAgInS	muset
být	být	k5eAaImF	být
příliš	příliš	k6eAd1	příliš
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
i	i	k8xC	i
mírná	mírný	k2eAgFnSc1d1	mírná
kolize	kolize	k1gFnSc1	kolize
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
velkým	velký	k2eAgFnPc3d1	velká
změnám	změna	k1gFnPc3	změna
drah	draha	k1gFnPc2	draha
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Haumea	Haume	k1gInSc2	Haume
(	(	kIx(	(
<g/>
dwarf	dwarf	k1gInSc1	dwarf
planet	planeta	k1gFnPc2	planeta
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
RABINOWITZ	RABINOWITZ	kA	RABINOWITZ
<g/>
,	,	kIx,	,
D.	D.	kA	D.
L.	L.	kA	L.
<g/>
;	;	kIx,	;
BARKUME	BARKUME	kA	BARKUME
<g/>
,	,	kIx,	,
K.	K.	kA	K.
M.	M.	kA	M.
<g/>
;	;	kIx,	;
BROWN	BROWN	kA	BROWN
<g/>
,	,	kIx,	,
M.	M.	kA	M.
E.	E.	kA	E.
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Photometric	Photometric	k1gMnSc1	Photometric
Observations	Observationsa	k1gFnPc2	Observationsa
Constraining	Constraining	k1gInSc4	Constraining
the	the	k?	the
Size	Sizus	k1gMnSc5	Sizus
<g/>
,	,	kIx,	,
Shape	Shap	k1gMnSc5	Shap
<g/>
,	,	kIx,	,
and	and	k?	and
Albedo	Albedo	k1gNnSc4	Albedo
of	of	k?	of
2003	[number]	k4	2003
EL	Ela	k1gFnPc2	Ela
<g/>
61	[number]	k4	61
<g/>
,	,	kIx,	,
a	a	k8xC	a
Rapidly	Rapidlo	k1gNnPc7	Rapidlo
Rotating	Rotating	k1gInSc1	Rotating
<g/>
,	,	kIx,	,
Pluto-Sized	Pluto-Sized	k1gInSc1	Pluto-Sized
Object	Object	k1gInSc1	Object
in	in	k?	in
the	the	k?	the
Kuiper	Kuiper	k1gMnSc1	Kuiper
Belt	Belt	k1gMnSc1	Belt
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Astrophysical	Astrophysical	k1gMnSc1	Astrophysical
Journal	Journal	k1gMnSc1	Journal
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
639	[number]	k4	639
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
1238	[number]	k4	1238
<g/>
–	–	k?	–
<g/>
1251	[number]	k4	1251
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
637	[number]	k4	637
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BROWN	BROWN	kA	BROWN
<g/>
,	,	kIx,	,
M.	M.	kA	M.
E.	E.	kA	E.
<g/>
;	;	kIx,	;
BOUCHEZ	BOUCHEZ	kA	BOUCHEZ
<g/>
,	,	kIx,	,
A.	A.	kA	A.
H.	H.	kA	H.
<g/>
;	;	kIx,	;
RABINOWITZ	RABINOWITZ	kA	RABINOWITZ
<g/>
,	,	kIx,	,
D.	D.	kA	D.
L.	L.	kA	L.
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Keck	Keck	k6eAd1	Keck
Observatory	Observator	k1gInPc7	Observator
laser	laser	k1gInSc1	laser
guide	guide	k6eAd1	guide
star	star	k1gInSc1	star
adaptive	adaptiv	k1gInSc5	adaptiv
optics	optics	k1gInSc1	optics
discovery	discover	k1gInPc1	discover
and	and	k?	and
characterization	characterization	k1gInSc1	characterization
of	of	k?	of
a	a	k8xC	a
satellite	satellit	k1gInSc5	satellit
to	ten	k3xDgNnSc4	ten
large	large	k1gFnSc7	large
Kuiper	Kuiper	k1gMnSc1	Kuiper
belt	belt	k1gMnSc1	belt
object	object	k1gMnSc1	object
2003	[number]	k4	2003
EL	Ela	k1gFnPc2	Ela
<g/>
61	[number]	k4	61
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Astrophysical	Astrophysical	k1gFnSc1	Astrophysical
Journal	Journal	k1gFnSc2	Journal
Letters	Lettersa	k1gFnPc2	Lettersa
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
632	[number]	k4	632
<g/>
,	,	kIx,	,
s.	s.	k?	s.
L	L	kA	L
<g/>
45	[number]	k4	45
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
637	[number]	k4	637
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BROWN	BROWN	kA	BROWN
<g/>
,	,	kIx,	,
M.	M.	kA	M.
E.	E.	kA	E.
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
collisional	collisionat	k5eAaPmAgInS	collisionat
family	famila	k1gFnPc4	famila
of	of	k?	of
icy	icy	k?	icy
objects	objects	k1gInSc1	objects
in	in	k?	in
the	the	k?	the
Kuiper	Kuiper	k1gMnSc1	Kuiper
belt	belt	k1gMnSc1	belt
<g/>
.	.	kIx.	.
</s>
<s>
Nature	Natur	k1gMnSc5	Natur
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
446	[number]	k4	446
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
7133	[number]	k4	7133
<g/>
,	,	kIx,	,
s.	s.	k?	s.
294	[number]	k4	294
<g/>
–	–	k?	–
<g/>
296	[number]	k4	296
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Plutoid	Plutoid	k1gInSc1	Plutoid
</s>
</p>
<p>
<s>
Trpasličí	trpasličí	k2eAgFnSc1d1	trpasličí
planeta	planeta	k1gFnSc1	planeta
</s>
</p>
<p>
<s>
Kuiperův	Kuiperův	k2eAgInSc1d1	Kuiperův
pás	pás	k1gInSc1	pás
</s>
</p>
<p>
<s>
Pluto	Pluto	k1gMnSc1	Pluto
</s>
</p>
<p>
<s>
Makemake	Makemake	k6eAd1	Makemake
</s>
</p>
<p>
<s>
Eris	Eris	k1gFnSc1	Eris
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Haumea	Haume	k1gInSc2	Haume
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc2	Wikidat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgInSc1d1	lokální
odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
Haumea	Haumea	k1gFnSc1	Haumea
</s>
</p>
<p>
<s>
Wikidata	Wikidata	k1gFnSc1	Wikidata
<g/>
:	:	kIx,	:
136108	[number]	k4	136108
Haumea	Haume	k1gInSc2	Haume
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Haumea	Haume	k1gInSc2	Haume
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
en	en	k?	en
<g/>
:	:	kIx,	:
<g/>
Kuiper	Kuiper	k1gMnSc1	Kuiper
Belt	Belt	k1gMnSc1	Belt
object	object	k1gMnSc1	object
to	ten	k3xDgNnSc4	ten
become	becom	k1gInSc5	becom
comet	comet	k1gInSc4	comet
in	in	k?	in
approx	approx	k1gInSc1	approx
<g/>
.	.	kIx.	.
2	[number]	k4	2
million	million	k1gInSc1	million
years	years	k1gInSc4	years
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Znázornění	znázornění	k1gNnSc4	znázornění
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Haumey	Haumea	k1gFnSc2	Haumea
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Animace	animace	k1gFnSc1	animace
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
satelitů	satelit	k1gMnPc2	satelit
Haumey	Haumea	k1gFnSc2	Haumea
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Robert	Robert	k1gMnSc1	Robert
Johnston	Johnston	k1gInSc1	Johnston
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
136108	[number]	k4	136108
<g/>
)	)	kIx)	)
Haumea	Haume	k1gInSc2	Haume
<g/>
,	,	kIx,	,
Hi	hi	k0	hi
<g/>
'	'	kIx"	'
<g/>
iaka	iakus	k1gMnSc2	iakus
<g/>
,	,	kIx,	,
and	and	k?	and
Namaka	Namak	k1gMnSc2	Namak
</s>
</p>
