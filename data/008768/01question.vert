<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
plutoid	plutoid	k1gInSc1	plutoid
v	v	k7c6	v
Kuiperově	Kuiperův	k2eAgInSc6d1	Kuiperův
pásu	pás	k1gInSc6	pás
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
asi	asi	k9	asi
jedné	jeden	k4xCgFnSc2	jeden
třetiny	třetina	k1gFnSc2	třetina
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Pluta	Pluto	k1gNnSc2	Pluto
a	a	k8xC	a
0,07	[number]	k4	0,07
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
?	?	kIx.	?
</s>
