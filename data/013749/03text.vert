<s>
Koutníkovití	Koutníkovitý	k2eAgMnPc1d1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Pavouci	pavouk	k1gMnPc1
Koutník	Koutník	k1gMnSc1
jedovatý	jedovatý	k2eAgMnSc1d1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc2
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
členovci	členovec	k1gMnPc1
(	(	kIx(
<g/>
Arthropoda	Arthropoda	k1gMnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
klepítkatci	klepítkatec	k1gMnPc1
(	(	kIx(
<g/>
Chelicerata	Chelicerat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
pavoukovci	pavoukovec	k1gMnPc1
(	(	kIx(
<g/>
Arachnida	Arachnida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
pavouci	pavouk	k1gMnPc1
(	(	kIx(
<g/>
Araneae	Araneae	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
koutníkovití	koutníkovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Sicariidae	Sicariidae	k1gNnSc7
<g/>
)	)	kIx)
<g/>
Keyserling	Keyserling	k1gInSc1
<g/>
,	,	kIx,
1880	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
rody	rod	k1gInPc1
</s>
<s>
Loxosceles	Loxosceles	k1gMnSc1
</s>
<s>
Sicarius	Sicarius	k1gMnSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Koutníkovití	Koutníkovitý	k2eAgMnPc1d1
je	on	k3xPp3gNnSc4
čeleď	čeleď	k1gFnSc1
pavouků	pavouk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
do	do	k7c2
ní	on	k3xPp3gFnSc2
122	#num#	k4
druhů	druh	k1gInPc2
pavouků	pavouk	k1gMnPc2
rozdělených	rozdělená	k1gFnPc2
do	do	k7c2
rodů	rod	k1gInPc2
Loxosceles	Loxosceles	k1gInSc1
a	a	k8xC
Sicarius	Sicarius	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
specificky	specificky	k6eAd1
uspořádaných	uspořádaný	k2eAgNnPc2d1
šesti	šest	k4xCc2
očí	oko	k1gNnPc2
je	být	k5eAaImIp3nS
jejich	jejich	k3xOp3gNnSc7
hlavním	hlavní	k2eAgNnSc7d1
poznávacím	poznávací	k2eAgNnSc7d1
znamením	znamení	k1gNnSc7
skvrna	skvrna	k1gFnSc1
ve	v	k7c6
tvaru	tvar	k1gInSc6
houslí	housle	k1gFnPc2
na	na	k7c6
hlavohrudi	hlavohruď	k1gFnSc6
a	a	k8xC
silný	silný	k2eAgInSc1d1
jed	jed	k1gInSc1
způsobující	způsobující	k2eAgInSc1d1
nekrózu	nekróza	k1gFnSc4
tkáně	tkáň	k1gFnSc2
<g/>
,	,	kIx,
loxoscelismus	loxoscelismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Pavouci	pavouk	k1gMnPc1
rodu	rod	k1gInSc2
Loxosceles	Loxosceles	k1gInSc4
jsou	být	k5eAaImIp3nP
rozšířeni	rozšířit	k5eAaPmNgMnP
téměř	téměř	k6eAd1
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
,	,	kIx,
především	především	k6eAd1
pak	pak	k6eAd1
v	v	k7c6
teplejších	teplý	k2eAgFnPc6d2
oblastech	oblast	k1gFnPc6
(	(	kIx(
<g/>
jih	jih	k1gInSc4
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
oblast	oblast	k1gFnSc4
Středozemního	středozemní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zástupci	zástupce	k1gMnPc1
rodu	rod	k1gInSc2
Sicarius	Sicarius	k1gInSc4
jsou	být	k5eAaImIp3nP
pouštní	pouštní	k2eAgMnPc1d1
pavouci	pavouk	k1gMnPc1
rozšířeni	rozšířit	k5eAaPmNgMnP
především	především	k6eAd1
v	v	k7c6
Africe	Afrika	k1gFnSc6
a	a	k8xC
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
Sicariidae	Sicariidae	k1gInSc1
(	(	kIx(
<g/>
koutníkovití	koutníkovitý	k2eAgMnPc1d1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
