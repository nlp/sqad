<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
záložník	záložník	k1gMnSc1	záložník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
lize	liga	k1gFnSc6	liga
hrál	hrát	k5eAaImAgMnS	hrát
za	za	k7c7	za
SONP	SONP	kA	SONP
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
11	[number]	k4	11
ligových	ligový	k2eAgInPc6d1	ligový
utkáních	utkání	k1gNnPc6	utkání
<g/>
,	,	kIx,	,
gól	gól	k1gInSc4	gól
v	v	k7c6	v
lize	liga	k1gFnSc6	liga
nedal	dát	k5eNaPmAgMnS	dát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ligová	ligový	k2eAgFnSc1d1	ligová
bilance	bilance	k1gFnSc1	bilance
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Horák	Horák	k1gMnSc1	Horák
<g/>
,	,	kIx,	,
Lubomír	Lubomír	k1gMnSc1	Lubomír
Král	Král	k1gMnSc1	Král
<g/>
:	:	kIx,	:
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
našeho	náš	k3xOp1gInSc2	náš
fotbalu	fotbal	k1gInSc2	fotbal
−	−	k?	−
Libri	Libri	k1gNnSc1	Libri
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
Radovan	Radovan	k1gMnSc1	Radovan
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Jenšík	Jenšík	k1gMnSc1	Jenšík
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
českého	český	k2eAgInSc2d1	český
fotbalu	fotbal	k1gInSc2	fotbal
−	−	k?	−
Radovan	Radovan	k1gMnSc1	Radovan
Jelínek	Jelínek	k1gMnSc1	Jelínek
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
Luboš	Luboš	k1gMnSc1	Luboš
Jeřábek	Jeřábek	k1gMnSc1	Jeřábek
<g/>
:	:	kIx,	:
Československý	československý	k2eAgInSc1d1	československý
fotbal	fotbal	k1gInSc1	fotbal
v	v	k7c6	v
číslech	číslo	k1gNnPc6	číslo
a	a	k8xC	a
faktech	fakt	k1gInPc6	fakt
-	-	kIx~	-
Olympia	Olympia	k1gFnSc1	Olympia
1991	[number]	k4	1991
</s>
</p>
