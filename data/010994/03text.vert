<p>
<s>
Dopravní	dopravní	k2eAgFnPc1d1	dopravní
značky	značka	k1gFnPc1	značka
jsou	být	k5eAaImIp3nP	být
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
piktogramy	piktogram	k1gInPc1	piktogram
určené	určený	k2eAgInPc1d1	určený
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
a	a	k8xC	a
regulaci	regulace	k1gFnSc4	regulace
silničního	silniční	k2eAgInSc2d1	silniční
provozu	provoz	k1gInSc2	provoz
na	na	k7c6	na
pozemních	pozemní	k2eAgFnPc6d1	pozemní
komunikacích	komunikace	k1gFnPc6	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zařízení	zařízení	k1gNnSc4	zařízení
upozorňující	upozorňující	k2eAgNnSc4d1	upozorňující
účastníky	účastník	k1gMnPc7	účastník
silničního	silniční	k2eAgInSc2d1	silniční
provozu	provoz	k1gInSc2	provoz
na	na	k7c4	na
nebezpečná	bezpečný	k2eNgNnPc4d1	nebezpečné
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
ukládají	ukládat	k5eAaImIp3nP	ukládat
jim	on	k3xPp3gMnPc3	on
zákazy	zákaz	k1gInPc7	zákaz
<g/>
,	,	kIx,	,
příkazy	příkaz	k1gInPc7	příkaz
nebo	nebo	k8xC	nebo
omezení	omezení	k1gNnPc2	omezení
<g/>
,	,	kIx,	,
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
jim	on	k3xPp3gMnPc3	on
informace	informace	k1gFnSc1	informace
nebo	nebo	k8xC	nebo
zpřesňují	zpřesňovat	k5eAaImIp3nP	zpřesňovat
<g/>
,	,	kIx,	,
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
nebo	nebo	k8xC	nebo
omezují	omezovat	k5eAaImIp3nP	omezovat
význam	význam	k1gInSc4	význam
jiné	jiný	k2eAgFnSc2d1	jiná
dopravní	dopravní	k2eAgFnSc2d1	dopravní
značky	značka	k1gFnSc2	značka
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
dopravních	dopravní	k2eAgFnPc2d1	dopravní
značek	značka	k1gFnPc2	značka
zpravidla	zpravidla	k6eAd1	zpravidla
stanoví	stanovit	k5eAaPmIp3nS	stanovit
Pravidla	pravidlo	k1gNnPc1	pravidlo
silničního	silniční	k2eAgInSc2d1	silniční
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
dopravních	dopravní	k2eAgFnPc2d1	dopravní
značek	značka	k1gFnPc2	značka
je	být	k5eAaImIp3nS	být
provoz	provoz	k1gInSc4	provoz
na	na	k7c6	na
pozemních	pozemní	k2eAgFnPc6d1	pozemní
komunikacích	komunikace	k1gFnPc6	komunikace
také	také	k9	také
řízen	řídit	k5eAaImNgMnS	řídit
<g/>
,	,	kIx,	,
zabezpečován	zabezpečovat	k5eAaImNgMnS	zabezpečovat
a	a	k8xC	a
usměrňován	usměrňovat	k5eAaImNgInS	usměrňovat
ještě	ještě	k9	ještě
dalšími	další	k2eAgFnPc7d1	další
dopravními	dopravní	k2eAgFnPc7d1	dopravní
zařízeními	zařízení	k1gNnPc7	zařízení
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
také	také	k9	také
světelnými	světelný	k2eAgInPc7d1	světelný
a	a	k8xC	a
akustickými	akustický	k2eAgInPc7d1	akustický
signály	signál	k1gInPc7	signál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
dopravního	dopravní	k2eAgNnSc2d1	dopravní
značení	značení	k1gNnSc2	značení
==	==	k?	==
</s>
</p>
<p>
<s>
Funkci	funkce	k1gFnSc4	funkce
směrového	směrový	k2eAgNnSc2d1	směrové
dopravního	dopravní	k2eAgNnSc2d1	dopravní
značení	značení	k1gNnSc2	značení
v	v	k7c6	v
dávných	dávný	k2eAgFnPc6d1	dávná
dobách	doba	k1gFnPc6	doba
plnily	plnit	k5eAaImAgFnP	plnit
jen	jen	k6eAd1	jen
stopy	stopa	k1gFnPc1	stopa
a	a	k8xC	a
vyšlapané	vyšlapaný	k2eAgFnPc1d1	vyšlapaná
pěšiny	pěšina	k1gFnPc1	pěšina
a	a	k8xC	a
stezky	stezka	k1gFnPc1	stezka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Již	již	k9	již
v	v	k7c6	v
antických	antický	k2eAgFnPc6d1	antická
Pompejích	Pompeje	k1gFnPc6	Pompeje
byly	být	k5eAaImAgFnP	být
nalezeny	nalezen	k2eAgInPc4d1	nalezen
patníky	patník	k1gInPc4	patník
oddělující	oddělující	k2eAgInSc4d1	oddělující
prostor	prostor	k1gInSc4	prostor
náměstí	náměstí	k1gNnSc2	náměstí
od	od	k7c2	od
pásu	pás	k1gInSc2	pás
pro	pro	k7c4	pro
jízdu	jízda	k1gFnSc4	jízda
<g/>
,	,	kIx,	,
zvýšené	zvýšený	k2eAgInPc4d1	zvýšený
chodníky	chodník	k1gInPc4	chodník
a	a	k8xC	a
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
přechod	přechod	k1gInSc4	přechod
přes	přes	k7c4	přes
jízdní	jízdní	k2eAgInSc4d1	jízdní
pás	pás	k1gInSc4	pás
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgNnSc4d1	připomínající
nejmodernější	moderní	k2eAgNnSc4d3	nejmodernější
provedení	provedení	k1gNnSc4	provedení
přechodu	přechod	k1gInSc2	přechod
pro	pro	k7c4	pro
chodce	chodec	k1gMnPc4	chodec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Římané	Říman	k1gMnPc1	Říman
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
120	[number]	k4	120
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
začali	začít	k5eAaPmAgMnP	začít
kolem	kolem	k7c2	kolem
cest	cesta	k1gFnPc2	cesta
osazovat	osazovat	k5eAaImF	osazovat
milníky	milník	k1gInPc4	milník
se	s	k7c7	s
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
od	od	k7c2	od
Říma	Řím	k1gInSc2	Řím
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
úsloví	úsloví	k1gNnSc1	úsloví
"	"	kIx"	"
<g/>
Všechny	všechen	k3xTgFnPc1	všechen
cesty	cesta	k1gFnPc1	cesta
vedou	vést	k5eAaImIp3nP	vést
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Milníky	milník	k1gInPc1	milník
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
zaváděl	zavádět	k5eAaImAgInS	zavádět
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
panovník	panovník	k1gMnSc1	panovník
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Saska	Saska	k1gMnSc1	Saska
August	August	k1gMnSc1	August
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgInSc1d1	silný
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
zaváděl	zavádět	k5eAaImAgInS	zavádět
verstníky	verstník	k1gMnPc4	verstník
(	(	kIx(	(
<g/>
versta	versta	k1gFnSc1	versta
=	=	kIx~	=
jednotka	jednotka	k1gFnSc1	jednotka
délky	délka	k1gFnSc2	délka
<g/>
)	)	kIx)	)
car	car	k1gMnSc1	car
Petr	Petr	k1gMnSc1	Petr
Veliký	veliký	k2eAgMnSc1d1	veliký
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1750	[number]	k4	1750
se	se	k3xPyFc4	se
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
na	na	k7c6	na
křižovatkách	křižovatka	k1gFnPc6	křižovatka
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
"	"	kIx"	"
<g/>
křížové	křížový	k2eAgFnPc1d1	křížová
značky	značka	k1gFnPc1	značka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
je	on	k3xPp3gNnPc4	on
začali	začít	k5eAaPmAgMnP	začít
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
"	"	kIx"	"
<g/>
hodinovými	hodinový	k2eAgInPc7d1	hodinový
kameny	kámen	k1gInPc7	kámen
<g/>
"	"	kIx"	"
-	-	kIx~	-
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
se	se	k3xPyFc4	se
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
udávala	udávat	k5eAaImAgFnS	udávat
v	v	k7c6	v
hodinách	hodina	k1gFnPc6	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstaršími	starý	k2eAgFnPc7d3	nejstarší
příkazovými	příkazový	k2eAgFnPc7d1	příkazová
značkami	značka	k1gFnPc7	značka
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
brzdové	brzdový	k2eAgInPc1d1	brzdový
kameny	kámen	k1gInPc1	kámen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Signály	signál	k1gInPc1	signál
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
železniční	železniční	k2eAgFnSc2d1	železniční
a	a	k8xC	a
silniční	silniční	k2eAgFnSc2d1	silniční
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgInP	být
inspirovány	inspirován	k2eAgInPc1d1	inspirován
námořními	námořní	k2eAgInPc7d1	námořní
signály	signál	k1gInPc7	signál
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
je	být	k5eAaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
použití	použití	k1gNnSc1	použití
primitivního	primitivní	k2eAgInSc2d1	primitivní
mechanického	mechanický	k2eAgInSc2d1	mechanický
semaforu	semafor	k1gInSc2	semafor
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
londýnských	londýnský	k2eAgFnPc2d1	londýnská
křižovatek	křižovatka	k1gFnPc2	křižovatka
-	-	kIx~	-
semafor	semafor	k1gInSc1	semafor
měl	mít	k5eAaImAgInS	mít
tvar	tvar	k1gInSc4	tvar
kříže	kříž	k1gInSc2	kříž
napodobujícího	napodobující	k2eAgInSc2d1	napodobující
lidskou	lidský	k2eAgFnSc4d1	lidská
postavu	postava	k1gFnSc4	postava
s	s	k7c7	s
rozpaženýma	rozpažený	k2eAgFnPc7d1	rozpažená
rukama	ruka	k1gFnPc7	ruka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
stůj	stát	k5eAaImRp2nS	stát
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
svěsit	svěsit	k5eAaPmF	svěsit
obě	dva	k4xCgFnPc4	dva
paže	paže	k1gFnPc4	paže
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
volno	volno	k6eAd1	volno
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
The	The	k1gFnPc2	The
Motor	motor	k1gInSc1	motor
Car	car	k1gMnSc1	car
Act	Act	k1gMnSc1	Act
(	(	kIx(	(
<g/>
3	[number]	k4	3
Edw	Edw	k1gFnPc2	Edw
<g/>
.	.	kIx.	.
</s>
<s>
VII	VII	kA	VII
<g/>
,	,	kIx,	,
c.	c.	k?	c.
30	[number]	k4	30
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
automobilový	automobilový	k2eAgInSc1d1	automobilový
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
zavedl	zavést	k5eAaPmAgInS	zavést
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
světě	svět	k1gInSc6	svět
dopravní	dopravní	k2eAgFnSc2d1	dopravní
značky	značka	k1gFnSc2	značka
podobné	podobný	k2eAgFnSc2d1	podobná
dnešním	dnešní	k2eAgNnSc7d1	dnešní
<g/>
,	,	kIx,	,
již	již	k9	již
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
kruhů	kruh	k1gInPc2	kruh
a	a	k8xC	a
trojúhelníků	trojúhelník	k1gInPc2	trojúhelník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
dopravní	dopravní	k2eAgFnPc1d1	dopravní
značky	značka	k1gFnPc1	značka
zavedly	zavést	k5eAaPmAgFnP	zavést
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
zemích	zem	k1gFnPc6	zem
už	už	k6eAd1	už
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
desetiletí	desetiletí	k1gNnSc6	desetiletí
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
národní	národní	k2eAgInPc4d1	národní
automobilové	automobilový	k2eAgInPc4d1	automobilový
kluby	klub	k1gInPc4	klub
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Touring	Touring	k1gInSc1	Touring
Club	club	k1gInSc1	club
Italiano	Italiana	k1gFnSc5	Italiana
nebo	nebo	k8xC	nebo
Kaiserlicher	Kaiserlichra	k1gFnPc2	Kaiserlichra
Automobil	automobil	k1gInSc1	automobil
Club	club	k1gInSc1	club
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
silniční	silniční	k2eAgInSc1d1	silniční
kongres	kongres	k1gInSc1	kongres
v	v	k7c6	v
Holandsku	Holandsko	k1gNnSc6	Holandsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgNnSc6	jenž
se	se	k3xPyFc4	se
místní	místní	k2eAgFnSc1d1	místní
turistická	turistický	k2eAgFnSc1d1	turistická
organizace	organizace	k1gFnSc1	organizace
ANWB	ANWB	kA	ANWB
pochlubila	pochlubit	k5eAaPmAgFnS	pochlubit
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
umístila	umístit	k5eAaPmAgFnS	umístit
400	[number]	k4	400
výstražných	výstražný	k2eAgFnPc2d1	výstražná
značek	značka	k1gFnPc2	značka
odpovídajících	odpovídající	k2eAgFnPc6d1	odpovídající
mezinárodně	mezinárodně	k6eAd1	mezinárodně
přijatým	přijatý	k2eAgInPc3d1	přijatý
tvarům	tvar	k1gInPc3	tvar
a	a	k8xC	a
barvám	barva	k1gFnPc3	barva
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
byla	být	k5eAaImAgFnS	být
podobná	podobný	k2eAgFnSc1d1	podobná
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
značení	značení	k1gNnSc1	značení
turistických	turistický	k2eAgFnPc2d1	turistická
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
cyklotras	cyklotrasa	k1gFnPc2	cyklotrasa
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dopravní	dopravní	k2eAgNnSc1d1	dopravní
značení	značení	k1gNnSc1	značení
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
bylo	být	k5eAaImAgNnS	být
zaváděno	zavádět	k5eAaImNgNnS	zavádět
později	pozdě	k6eAd2	pozdě
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
prvních	první	k4xOgInPc2	první
pět	pět	k4xCc1	pět
druhů	druh	k1gInPc2	druh
výstražných	výstražný	k2eAgFnPc2d1	výstražná
značek	značka	k1gFnPc2	značka
a	a	k8xC	a
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
již	již	k6eAd1	již
celá	celý	k2eAgFnSc1d1	celá
škála	škála	k1gFnSc1	škála
značek	značka	k1gFnPc2	značka
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
počítalo	počítat	k5eAaImAgNnS	počítat
i	i	k9	i
se	s	k7c7	s
značkami	značka	k1gFnPc7	značka
prosvětlovanými	prosvětlovaný	k2eAgFnPc7d1	prosvětlovaná
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
reflexní	reflexní	k2eAgFnSc1d1	reflexní
fólie	fólie	k1gFnSc1	fólie
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
nebyly	být	k5eNaImAgFnP	být
<g/>
,	,	kIx,	,
připevňovaly	připevňovat	k5eAaImAgFnP	připevňovat
se	se	k3xPyFc4	se
na	na	k7c4	na
některé	některý	k3yIgFnPc4	některý
značky	značka	k1gFnPc4	značka
malé	malý	k2eAgFnSc2d1	malá
kulaté	kulatý	k2eAgFnSc2d1	kulatá
odrazky	odrazka	k1gFnSc2	odrazka
–	–	k?	–
traťová	traťový	k2eAgNnPc1d1	traťové
návěstidla	návěstidlo	k1gNnPc1	návěstidlo
podobného	podobný	k2eAgNnSc2d1	podobné
provedení	provedení	k1gNnSc2	provedení
můžeme	moct	k5eAaImIp1nP	moct
ještě	ještě	k9	ještě
dodnes	dodnes	k6eAd1	dodnes
najít	najít	k5eAaPmF	najít
na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
železnicích	železnice	k1gFnPc6	železnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
konvence	konvence	k1gFnSc1	konvence
a	a	k8xC	a
návrhy	návrh	k1gInPc4	návrh
ke	k	k7c3	k
sjednocení	sjednocení	k1gNnSc3	sjednocení
dopravního	dopravní	k2eAgNnSc2d1	dopravní
značení	značení	k1gNnSc2	značení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1908	[number]	k4	1908
Paříž	Paříž	k1gFnSc1	Paříž
-	-	kIx~	-
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
Silniční	silniční	k2eAgInSc1d1	silniční
kongres	kongres	k1gInSc1	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
konvence	konvence	k1gFnSc1	konvence
(	(	kIx(	(
<g/>
Úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
jízdě	jízda	k1gFnSc6	jízda
motorovými	motorový	k2eAgNnPc7d1	motorové
vozidly	vozidlo	k1gNnPc7	vozidlo
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
kongres	kongres	k1gInSc1	kongres
1910	[number]	k4	1910
-	-	kIx~	-
navržené	navržený	k2eAgFnSc2d1	navržená
značky	značka	k1gFnSc2	značka
většinou	většinou	k6eAd1	většinou
černobílé	černobílý	k2eAgFnSc2d1	černobílá
<g/>
,	,	kIx,	,
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
obrázky	obrázek	k1gInPc4	obrázek
i	i	k8xC	i
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
kongres	kongres	k1gInSc1	kongres
v	v	k7c6	v
Seville	Sevilla	k1gFnSc6	Sevilla
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
doporučeny	doporučen	k2eAgFnPc1d1	doporučena
výstražné	výstražný	k2eAgFnPc1d1	výstražná
značky	značka	k1gFnPc1	značka
trojúhelníkového	trojúhelníkový	k2eAgInSc2d1	trojúhelníkový
tvaru	tvar	k1gInSc2	tvar
vrcholem	vrchol	k1gInSc7	vrchol
nahoru	nahoru	k6eAd1	nahoru
</s>
</p>
<p>
<s>
1926	[number]	k4	1926
Pařížský	pařížský	k2eAgInSc4d1	pařížský
kongres	kongres	k1gInSc4	kongres
</s>
</p>
<p>
<s>
Návrhy	návrh	k1gInPc1	návrh
Ligy	liga	k1gFnSc2	liga
Spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
změna	změna	k1gFnSc1	změna
barevného	barevný	k2eAgNnSc2d1	barevné
provedení	provedení	k1gNnSc2	provedení
výstražných	výstražný	k2eAgFnPc2d1	výstražná
značek	značka	k1gFnPc2	značka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c4	o
sjednocení	sjednocení	k1gNnSc4	sjednocení
silničních	silniční	k2eAgFnPc2d1	silniční
značek	značka	k1gFnPc2	značka
-	-	kIx~	-
Ženeva	Ženeva	k1gFnSc1	Ženeva
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1931	[number]	k4	1931
</s>
</p>
<p>
<s>
Ženevský	ženevský	k2eAgInSc1d1	ženevský
protokol	protokol	k1gInSc1	protokol
(	(	kIx(	(
<g/>
Světová	světový	k2eAgFnSc1d1	světová
úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
silniční	silniční	k2eAgFnSc6d1	silniční
a	a	k8xC	a
automobilové	automobilový	k2eAgFnSc6d1	automobilová
dopravě	doprava	k1gFnSc6	doprava
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
silničním	silniční	k2eAgInSc6d1	silniční
provozu	provoz	k1gInSc6	provoz
-	-	kIx~	-
Ženeva	Ženeva	k1gFnSc1	Ženeva
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1949	[number]	k4	1949
</s>
</p>
<p>
<s>
Protokol	protokol	k1gInSc1	protokol
o	o	k7c6	o
silničních	silniční	k2eAgFnPc6d1	silniční
značkách	značka	k1gFnPc6	značka
a	a	k8xC	a
signálech	signál	k1gInPc6	signál
-	-	kIx~	-
Ženeva	Ženeva	k1gFnSc1	Ženeva
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1949	[number]	k4	1949
</s>
</p>
<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
silničních	silniční	k2eAgFnPc6d1	silniční
značkách	značka	k1gFnPc6	značka
-	-	kIx~	-
Ženeva	Ženeva	k1gFnSc1	Ženeva
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1957	[number]	k4	1957
(	(	kIx(	(
<g/>
vyhl	vyhla	k1gFnPc2	vyhla
<g/>
.	.	kIx.	.
č.	č.	k?	č.
175	[number]	k4	175
<g/>
/	/	kIx~	/
<g/>
1960	[number]	k4	1960
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
konvence	konvence	k1gFnSc1	konvence
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
-	-	kIx~	-
několik	několik	k4yIc4	několik
nových	nový	k2eAgFnPc2d1	nová
informativních	informativní	k2eAgFnPc2d1	informativní
značek	značka	k1gFnPc2	značka
</s>
</p>
<p>
<s>
Úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
silničním	silniční	k2eAgInSc6d1	silniční
provozu	provoz	k1gInSc6	provoz
-	-	kIx~	-
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1968	[number]	k4	1968
(	(	kIx(	(
<g/>
částka	částka	k1gFnSc1	částka
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
1980	[number]	k4	1980
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
silničních	silniční	k2eAgFnPc6d1	silniční
značkách	značka	k1gFnPc6	značka
a	a	k8xC	a
signálech	signál	k1gInPc6	signál
-	-	kIx~	-
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1968	[number]	k4	1968
(	(	kIx(	(
<g/>
částka	částka	k1gFnSc1	částka
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
1980	[number]	k4	1980
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
dohoda	dohoda	k1gFnSc1	dohoda
doplňující	doplňující	k2eAgFnSc4d1	doplňující
Úmluvu	úmluva	k1gFnSc4	úmluva
o	o	k7c6	o
silničním	silniční	k2eAgInSc6d1	silniční
provozu	provoz	k1gInSc6	provoz
-	-	kIx~	-
Ženeva	Ženeva	k1gFnSc1	Ženeva
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1971	[number]	k4	1971
(	(	kIx(	(
<g/>
částka	částka	k1gFnSc1	částka
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
1980	[number]	k4	1980
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
dohoda	dohoda	k1gFnSc1	dohoda
doplňující	doplňující	k2eAgFnSc4d1	doplňující
Úmluvu	úmluva	k1gFnSc4	úmluva
o	o	k7c6	o
silničních	silniční	k2eAgFnPc6d1	silniční
značkách	značka	k1gFnPc6	značka
a	a	k8xC	a
signálech	signál	k1gInPc6	signál
-	-	kIx~	-
Ženeva	Ženeva	k1gFnSc1	Ženeva
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1971	[number]	k4	1971
(	(	kIx(	(
<g/>
částka	částka	k1gFnSc1	částka
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
1980	[number]	k4	1980
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Protokol	protokol	k1gInSc1	protokol
o	o	k7c6	o
silničních	silniční	k2eAgNnPc6d1	silniční
označeních	označení	k1gNnPc6	označení
k	k	k7c3	k
Evropské	evropský	k2eAgFnSc3d1	Evropská
dohodě	dohoda	k1gFnSc3	dohoda
doplňující	doplňující	k2eAgFnSc4d1	doplňující
Úmluvu	úmluva	k1gFnSc4	úmluva
o	o	k7c6	o
silničních	silniční	k2eAgFnPc6d1	silniční
značkách	značka	k1gFnPc6	značka
a	a	k8xC	a
signálech	signál	k1gInPc6	signál
-	-	kIx~	-
Ženeva	Ženeva	k1gFnSc1	Ženeva
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
V	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
systém	systém	k1gInSc1	systém
značení	značení	k1gNnSc2	značení
vyvíjel	vyvíjet	k5eAaImAgInS	vyvíjet
odlišně	odlišně	k6eAd1	odlišně
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
značky	značka	k1gFnPc1	značka
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
evropským	evropský	k2eAgMnPc3d1	evropský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
výstražné	výstražný	k2eAgFnPc1d1	výstražná
značky	značka	k1gFnPc1	značka
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
žlutého	žlutý	k2eAgInSc2d1	žlutý
čtverce	čtverec	k1gInSc2	čtverec
postaveného	postavený	k2eAgInSc2d1	postavený
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejnovější	nový	k2eAgInSc1d3	nejnovější
vývoj	vývoj	k1gInSc1	vývoj
znamená	znamenat	k5eAaImIp3nS	znamenat
vstup	vstup	k1gInSc4	vstup
proměnných	proměnná	k1gFnPc2	proměnná
a	a	k8xC	a
elektronických	elektronický	k2eAgFnPc2d1	elektronická
dopravních	dopravní	k2eAgFnPc2d1	dopravní
značek	značka	k1gFnPc2	značka
<g/>
,	,	kIx,	,
osvětlení	osvětlení	k1gNnSc1	osvětlení
značek	značka	k1gFnPc2	značka
s	s	k7c7	s
časovým	časový	k2eAgNnSc7d1	časové
ovládáním	ovládání	k1gNnSc7	ovládání
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
u	u	k7c2	u
škol	škola	k1gFnPc2	škola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvýrazňující	zvýrazňující	k2eAgFnSc4d1	zvýrazňující
reflexní	reflexní	k2eAgNnSc4d1	reflexní
žlutozelené	žlutozelený	k2eAgNnSc4d1	žlutozelené
orámování	orámování	k1gNnSc4	orámování
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Černý	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Holeš	Holeš	k1gMnSc1	Holeš
<g/>
:	:	kIx,	:
Sémiotika	sémiotika	k1gFnSc1	sémiotika
<g/>
,	,	kIx,	,
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Šafránek	Šafránek	k1gMnSc1	Šafránek
<g/>
:	:	kIx,	:
Legislativa	legislativa	k1gFnSc1	legislativa
a	a	k8xC	a
její	její	k3xOp3gInPc4	její
trendy	trend	k1gInPc4	trend
II	II	kA	II
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Brzdový	brzdový	k2eAgInSc1d1	brzdový
kámen	kámen	k1gInSc1	kámen
</s>
</p>
<p>
<s>
Dopravní	dopravní	k2eAgNnSc1d1	dopravní
značení	značení	k1gNnSc1	značení
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
Dopravní	dopravní	k2eAgNnSc1d1	dopravní
značení	značení	k1gNnSc1	značení
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
dopravních	dopravní	k2eAgFnPc2d1	dopravní
značek	značka	k1gFnPc2	značka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
silničního	silniční	k2eAgInSc2d1	silniční
provozu	provoz	k1gInSc2	provoz
</s>
</p>
<p>
<s>
Návěstidlo	návěstidlo	k1gNnSc1	návěstidlo
</s>
</p>
<p>
<s>
Signál	signál	k1gInSc4	signál
</s>
</p>
<p>
<s>
Světelné	světelný	k2eAgNnSc4d1	světelné
signalizační	signalizační	k2eAgNnSc4d1	signalizační
zařízení	zařízení	k1gNnSc4	zařízení
</s>
</p>
<p>
<s>
Traťová	traťový	k2eAgFnSc1d1	traťová
značka	značka	k1gFnSc1	značka
</s>
</p>
<p>
<s>
Turistická	turistický	k2eAgFnSc1d1	turistická
značka	značka	k1gFnSc1	značka
</s>
</p>
<p>
<s>
Dopravní	dopravní	k2eAgNnSc1d1	dopravní
zařízení	zařízení	k1gNnSc1	zařízení
</s>
</p>
<p>
<s>
Cyklistická	cyklistický	k2eAgFnSc1d1	cyklistická
trasa	trasa	k1gFnSc1	trasa
</s>
</p>
<p>
<s>
Stezka	stezka	k1gFnSc1	stezka
pro	pro	k7c4	pro
cyklisty	cyklista	k1gMnPc4	cyklista
</s>
</p>
<p>
<s>
Pěší	pěší	k2eAgFnSc1d1	pěší
zóna	zóna	k1gFnSc1	zóna
</s>
</p>
<p>
<s>
Plavební	plavební	k2eAgInSc1d1	plavební
znak	znak	k1gInSc1	znak
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgNnSc1d1	železniční
traťové	traťový	k2eAgNnSc1d1	traťové
návěstidlo	návěstidlo	k1gNnSc1	návěstidlo
</s>
</p>
<p>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
a	a	k8xC	a
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
traťová	traťový	k2eAgFnSc1d1	traťová
návěst	návěst	k1gFnSc1	návěst
</s>
</p>
<p>
<s>
Sdílený	sdílený	k2eAgInSc1d1	sdílený
prostor	prostor	k1gInSc1	prostor
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dopravní	dopravní	k2eAgFnSc1d1	dopravní
značka	značka	k1gFnSc1	značka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Přílohy	příloh	k1gInPc1	příloh
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
294	[number]	k4	294
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
obsahující	obsahující	k2eAgFnSc1d1	obsahující
vyobrazení	vyobrazení	k1gNnSc4	vyobrazení
dopravních	dopravní	k2eAgFnPc2d1	dopravní
značek	značka	k1gFnPc2	značka
</s>
</p>
<p>
<s>
České	český	k2eAgFnSc2d1	Česká
dopravní	dopravní	k2eAgFnSc2d1	dopravní
značky	značka	k1gFnSc2	značka
</s>
</p>
<p>
<s>
Skutečné	skutečný	k2eAgInPc1d1	skutečný
rozměry	rozměr	k1gInPc1	rozměr
českých	český	k2eAgFnPc2d1	Česká
dopravních	dopravní	k2eAgFnPc2d1	dopravní
značek	značka	k1gFnPc2	značka
v	v	k7c6	v
mm	mm	kA	mm
–	–	k?	–
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
*	*	kIx~	*
<g/>
.	.	kIx.	.
<g/>
doc	doc	kA	doc
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bartolomeova	Bartolomeův	k2eAgFnSc1d1	Bartolomeův
stránka	stránka	k1gFnSc1	stránka
–	–	k?	–
porovnání	porovnání	k1gNnSc1	porovnání
značek	značka	k1gFnPc2	značka
Děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
Práce	práce	k1gFnPc1	práce
na	na	k7c4	na
silnici	silnice	k1gFnSc4	silnice
a	a	k8xC	a
Padající	padající	k2eAgNnSc4d1	padající
kamení	kamení	k1gNnSc4	kamení
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
;	;	kIx,	;
odkazy	odkaz	k1gInPc1	odkaz
na	na	k7c4	na
stránky	stránka	k1gFnPc4	stránka
o	o	k7c6	o
dopravních	dopravní	k2eAgFnPc6d1	dopravní
značkách	značka	k1gFnPc6	značka
</s>
</p>
