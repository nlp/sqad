<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Labské	labský	k2eAgFnSc2d1
pískovce	pískovec	k1gInPc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgNnPc1d1
oblastLabské	oblastLabský	k2eAgFnPc4d1
pískovceIUCN	pískovceIUCN	k?
kategorie	kategorie	k1gFnPc4
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
Tiských	Tiská	k1gFnPc2
stěnZákladní	stěnZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1972	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
243	#num#	k4
km	km	kA
<g/>
2	#num#	k4
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
NP	NP	kA
České	český	k2eAgNnSc1d1
Švýcarsko	Švýcarsko	k1gNnSc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Ústecký	ústecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Okres	okres	k1gInSc1
</s>
<s>
Děčín	Děčín	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
56	#num#	k4
<g/>
′	′	k?
<g/>
54,9	54,9	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
<g/>
14,2	14,2	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Web	web	k1gInSc4
</s>
<s>
www.labskepiskovce.ochranaprirody.cz	www.labskepiskovce.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Labské	labský	k2eAgFnSc6d1
pískovce	pískovka	k1gFnSc6
byla	být	k5eAaImAgFnS
vyhlášena	vyhlášen	k2eAgFnSc1d1
dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1972	#num#	k4
výnosem	výnos	k1gInSc7
ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
č.	č.	k?
<g/>
j.	j.	k?
4.946	4.946	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
-	-	kIx~
II	II	kA
<g/>
/	/	kIx~
<g/>
2	#num#	k4
o	o	k7c6
zřízení	zřízení	k1gNnSc6
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Děčín	Děčín	k1gInSc1
a	a	k8xC
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
na	na	k7c6
rozloze	rozloha	k1gFnSc6
32	#num#	k4
302	#num#	k4
ha	ha	kA
<g/>
,	,	kIx,
po	po	k7c6
vyčlenění	vyčlenění	k1gNnSc6
její	její	k3xOp3gFnSc2
nejzachovalejší	zachovalý	k2eAgFnSc2d3
severní	severní	k2eAgFnSc2d1
části	část	k1gFnSc2
vyhlášené	vyhlášený	k2eAgInPc1d1
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2000	#num#	k4
jako	jako	k8xC,k8xS
Národní	národní	k2eAgInSc4d1
park	park	k1gInSc4
České	český	k2eAgNnSc1d1
Švýcarsko	Švýcarsko	k1gNnSc1
je	být	k5eAaImIp3nS
současná	současný	k2eAgFnSc1d1
rozloha	rozloha	k1gFnSc1
CHKO	CHKO	kA
24	#num#	k4
300	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
vykonává	vykonávat	k5eAaImIp3nS
státní	státní	k2eAgFnSc4d1
správu	správa	k1gFnSc4
na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
Správa	správa	k1gFnSc1
Národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
České	český	k2eAgNnSc1d1
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Geograficky	geograficky	k6eAd1
zaujímá	zaujímat	k5eAaImIp3nS
spolu	spolu	k6eAd1
s	s	k7c7
Národním	národní	k2eAgInSc7d1
parkem	park	k1gInSc7
České	český	k2eAgNnSc4d1
Švýcarsko	Švýcarsko	k1gNnSc4
prakticky	prakticky	k6eAd1
celou	celý	k2eAgFnSc4d1
geografickou	geografický	k2eAgFnSc4d1
jednotku	jednotka	k1gFnSc4
Děčínská	děčínský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
s	s	k7c7
nejvyšším	vysoký	k2eAgInSc7d3
vrcholem	vrchol	k1gInSc7
Děčínský	děčínský	k2eAgInSc1d1
Sněžník	Sněžník	k1gInSc1
(	(	kIx(
<g/>
723	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozprostírá	rozprostírat	k5eAaImIp3nS
se	se	k3xPyFc4
severně	severně	k6eAd1
od	od	k7c2
Českého	český	k2eAgNnSc2d1
středohoří	středohoří	k1gNnSc2
–	–	k?
zhruba	zhruba	k6eAd1
od	od	k7c2
obce	obec	k1gFnSc2
Petrovice	Petrovice	k1gFnSc2
a	a	k8xC
Tiských	Tiský	k2eAgFnPc2d1
stěn	stěna	k1gFnPc2
na	na	k7c6
západě	západ	k1gInSc6
<g/>
,	,	kIx,
dále	daleko	k6eAd2
přes	přes	k7c4
Děčín	Děčín	k1gInSc4
až	až	k9
po	po	k7c4
Chřibskou	chřibský	k2eAgFnSc4d1
na	na	k7c6
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1
poměry	poměr	k1gInPc1
jsou	být	k5eAaImIp3nP
stejné	stejný	k2eAgInPc1d1
jako	jako	k8xS,k8xC
v	v	k7c6
Národním	národní	k2eAgInSc6d1
parku	park	k1gInSc6
České	český	k2eAgNnSc1d1
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
,	,	kIx,
avšak	avšak	k8xC
hustší	hustý	k2eAgNnSc1d2
osídlení	osídlení	k1gNnSc1
zanechalo	zanechat	k5eAaPmAgNnS
svůj	svůj	k3xOyFgInSc4
vliv	vliv	k1gInSc4
v	v	k7c6
menší	malý	k2eAgFnSc6d2
rozmanitosti	rozmanitost	k1gFnSc6
fauny	fauna	k1gFnSc2
a	a	k8xC
flóry	flóra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
cenná	cenný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
však	však	k9
zachovaná	zachovaný	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
roubených	roubený	k2eAgInPc2d1
domů	dům	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Povrch	povrch	k6eAd1wR
tvoří	tvořit	k5eAaImIp3nP
pískovce	pískovec	k1gInPc1
křídového	křídový	k2eAgNnSc2d1
stáří	stáří	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
skalních	skalní	k2eAgInPc2d1
útvarů	útvar	k1gInPc2
v	v	k7c6
krajině	krajina	k1gFnSc6
je	být	k5eAaImIp3nS
srovnatelný	srovnatelný	k2eAgInSc1d1
s	s	k7c7
národními	národní	k2eAgInPc7d1
parky	park	k1gInPc7
v	v	k7c6
USA	USA	kA
v	v	k7c6
Arizoně	Arizona	k1gFnSc6
a	a	k8xC
Utahu	Utah	k1gInSc6
<g/>
,	,	kIx,
avšak	avšak	k8xC
na	na	k7c6
západě	západ	k1gInSc6
USA	USA	kA
jsou	být	k5eAaImIp3nP
skalní	skalní	k2eAgInPc1d1
útvary	útvar	k1gInPc1
vlivem	vlivem	k7c2
suchého	suchý	k2eAgNnSc2d1
klimatu	klima	k1gNnSc2
obnažené	obnažený	k2eAgNnSc1d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
Labských	labský	k2eAgInPc6d1
pískovcích	pískovec	k1gInPc6
jsou	být	k5eAaImIp3nP
pokryté	pokrytý	k2eAgInPc1d1
lesy	les	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Maloplošná	Maloplošný	k2eAgNnPc1d1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Pískovcová	pískovcový	k2eAgFnSc1d1
skála	skála	k1gFnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
Tiských	Tiský	k2eAgFnPc2d1
stěn	stěna	k1gFnPc2
</s>
<s>
PR	pr	k0
Arba	arba	k1gFnSc1
</s>
<s>
PR	pr	k0
Čabel	Čablo	k1gNnPc2
</s>
<s>
PP	PP	kA
Hofberg	Hofberg	k1gMnSc1
</s>
<s>
PP	PP	kA
Jeskyně	jeskyně	k1gFnSc1
pod	pod	k7c7
Sněžníkem	Sněžník	k1gInSc7
</s>
<s>
NPR	NPR	kA
Kaňon	kaňon	k1gInSc1
Labe	Labe	k1gNnSc2
</s>
<s>
PR	pr	k0
Libouchecké	Libouchecká	k1gFnPc5
rybníčky	rybníček	k1gInPc1
</s>
<s>
PP	PP	kA
Meandry	meandr	k1gInPc4
Chřibské	chřibský	k2eAgFnSc2d1
Kamenice	Kamenice	k1gFnSc2
</s>
<s>
PR	pr	k0
Niva	niva	k1gFnSc1
Olšového	olšový	k2eAgInSc2d1
potoka	potok	k1gInSc2
</s>
<s>
PR	pr	k0
Pavlino	Pavlin	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
u	u	k7c2
Jetřichovic	Jetřichovice	k1gFnPc2
</s>
<s>
PR	pr	k0
Pekelský	pekelský	k2eAgInSc4d1
důl	důl	k1gInSc4
</s>
<s>
PR	pr	k0
Pod	pod	k7c7
lesem	les	k1gInSc7
</s>
<s>
PR	pr	k0
Rájecká	Rájecký	k2eAgFnSc1d1
rašeliniště	rašeliniště	k1gNnSc5
</s>
<s>
PP	PP	kA
Rybník	rybník	k1gInSc1
u	u	k7c2
Králova	Králův	k2eAgInSc2d1
mlýna	mlýn	k1gInSc2
</s>
<s>
PR	pr	k0
Stará	starý	k2eAgFnSc1d1
Oleška	Oleška	k1gFnSc1
</s>
<s>
PP	PP	kA
Tiské	Tiský	k2eAgFnPc4d1
stěny	stěna	k1gFnPc4
</s>
<s>
PR	pr	k0
Za	za	k7c4
pilou	pila	k1gFnSc7
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AOPK	AOPK	kA
ČR	ČR	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
stránky	stránka	k1gFnPc4
Správy	správa	k1gFnSc2
CHKO	CHKO	kA
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
z	z	k7c2
CHKO	CHKO	kA
Labské	labský	k2eAgFnSc6d1
pískovce	pískovka	k1gFnSc6
Archivováno	archivovat	k5eAaBmNgNnS
24	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
Krajinou	Krajina	k1gFnSc7
domova	domov	k1gInSc2
II	II	kA
—	—	k?
Evropská	evropský	k2eAgFnSc1d1
Arizona	Arizona	k1gFnSc1
dokumentární	dokumentární	k2eAgFnSc1d1
pořad	pořad	k1gInSc4
ČT	ČT	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Děčín	Děčín	k1gInSc1
Národní	národní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
České	český	k2eAgNnSc1d1
Švýcarsko	Švýcarsko	k1gNnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Kaňon	kaňon	k1gInSc1
Labe	Labe	k1gNnSc2
•	•	k?
Růžovský	Růžovský	k2eAgInSc4d1
vrch	vrch	k1gInSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Březinské	Březinský	k2eAgInPc4d1
tisy	tis	k1gInPc4
•	•	k?
Pravčická	Pravčická	k1gFnSc1
brána	brána	k1gFnSc1
•	•	k?
Zlatý	zlatý	k2eAgInSc4d1
vrch	vrch	k1gInSc4
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Arba	arba	k1gFnSc1
•	•	k?
Bohyňská	Bohyňský	k2eAgFnSc1d1
lada	lado	k1gNnSc2
•	•	k?
Čabel	Čabel	k1gInSc1
•	•	k?
Holý	holý	k2eAgInSc1d1
vrch	vrch	k1gInSc1
u	u	k7c2
Jílového	Jílové	k1gNnSc2
•	•	k?
Kamenná	kamenný	k2eAgFnSc1d1
hůra	hůra	k1gFnSc1
•	•	k?
Maiberg	Maiberg	k1gInSc1
•	•	k?
Marschnerova	Marschnerův	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Pavlínino	Pavlínin	k2eAgNnSc4d1
údolí	údolí	k1gNnSc4
•	•	k?
Pekelský	pekelský	k2eAgInSc1d1
důl	důl	k1gInSc1
•	•	k?
Spravedlnost	spravedlnost	k1gFnSc1
•	•	k?
Studený	studený	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Světlík	světlík	k1gInSc1
•	•	k?
Vápenka	vápenka	k1gFnSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Vrabinec	Vrabinec	k1gInSc1
•	•	k?
Za	za	k7c7
pilou	pila	k1gFnSc7
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bobří	bobří	k2eAgFnSc1d1
soutěska	soutěska	k1gFnSc1
•	•	k?
Hofberg	Hofberg	k1gInSc1
•	•	k?
Jeskyně	jeskyně	k1gFnSc1
pod	pod	k7c7
Sněžníkem	Sněžník	k1gInSc7
•	•	k?
Jílovské	jílovský	k2eAgInPc4d1
tisy	tis	k1gInPc4
•	•	k?
Kytlice	kytlice	k1gFnSc2
•	•	k?
Líska	líska	k1gFnSc1
•	•	k?
Louka	louka	k1gFnSc1
u	u	k7c2
Brodských	Brodská	k1gFnPc2
•	•	k?
Meandry	meandr	k1gInPc1
Chřibské	chřibský	k2eAgFnSc2d1
Kamenice	Kamenice	k1gFnSc2
•	•	k?
Nebočadský	Nebočadský	k2eAgInSc1d1
luh	luh	k1gInSc1
•	•	k?
Noldenteich	Noldenteich	k1gInSc1
•	•	k?
Pod	pod	k7c7
lesem	les	k1gInSc7
•	•	k?
Rybník	rybník	k1gInSc1
u	u	k7c2
Králova	Králův	k2eAgInSc2d1
mlýna	mlýn	k1gInSc2
•	•	k?
Sojčí	sojčí	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
•	•	k?
Stará	starat	k5eAaImIp3nS
Oleška	Olešek	k1gMnSc2
•	•	k?
Stříbrný	stříbrný	k2eAgInSc4d1
roh	roh	k1gInSc4
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
Přírodní	přírodní	k2eAgFnSc2d1
parky	park	k1gInPc1
</s>
<s>
Východní	východní	k2eAgFnPc1d1
Krušné	krušný	k2eAgFnPc1d1
hory	hora	k1gFnPc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Vrkoč	vrkoč	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Kozí	kozí	k2eAgInSc4d1
vrch	vrch	k1gInSc4
•	•	k?
Libouchecké	Libouchecký	k2eAgInPc4d1
rybníčky	rybníček	k1gInPc4
•	•	k?
Niva	niva	k1gFnSc1
Olšového	olšový	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Rač	ráčit	k5eAaImRp2nS
•	•	k?
Rájecká	Rájecká	k1gFnSc1
rašeliniště	rašeliniště	k1gNnSc2
•	•	k?
Sluneční	sluneční	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Špičák	špičák	k1gInSc1
u	u	k7c2
Krásného	krásný	k2eAgInSc2d1
Lesa	les	k1gInSc2
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Babinské	Babinský	k2eAgFnPc1d1
louky	louka	k1gFnPc1
•	•	k?
Divoká	divoký	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
•	•	k?
Eiland	Eiland	k1gInSc1
•	•	k?
Loupežnická	loupežnický	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
•	•	k?
Magnetovec	magnetovec	k1gInSc1
–	–	k?
Skalní	skalní	k2eAgInSc1d1
hřib	hřib	k1gInSc1
•	•	k?
Tiské	Tiská	k1gFnSc2
stěny	stěna	k1gFnSc2
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Šluknovský	šluknovský	k2eAgInSc1d1
výběžek	výběžek	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
128448	#num#	k4
</s>
