<s>
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1972	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Labské	labský	k2eAgFnSc6d1
pískovce	pískovka	k1gFnSc6
byla	být	k5eAaImAgFnS
vyhlášena	vyhlášen	k2eAgFnSc1d1
dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1972	#num#	k4
výnosem	výnos	k1gInSc7
ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
č.	č.	k?
<g/>
j.	j.	k?
4.946	4.946	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
-	-	kIx~
II	II	kA
<g/>
/	/	kIx~
<g/>
2	#num#	k4
o	o	k7c6
zřízení	zřízení	k1gNnSc6
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Děčín	Děčín	k1gInSc1
a	a	k8xC
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
na	na	k7c6
rozloze	rozloha	k1gFnSc6
32	#num#	k4
302	#num#	k4
ha	ha	kA
<g/>
,	,	kIx,
po	po	k7c6
vyčlenění	vyčlenění	k1gNnSc6
její	její	k3xOp3gFnSc2
nejzachovalejší	zachovalý	k2eAgFnSc2d3
severní	severní	k2eAgFnSc2d1
části	část	k1gFnSc2
vyhlášené	vyhlášený	k2eAgInPc1d1
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2000	#num#	k4
jako	jako	k8xC,k8xS
Národní	národní	k2eAgInSc4d1
park	park	k1gInSc4
České	český	k2eAgNnSc1d1
Švýcarsko	Švýcarsko	k1gNnSc1
je	být	k5eAaImIp3nS
současná	současný	k2eAgFnSc1d1
rozloha	rozloha	k1gFnSc1
CHKO	CHKO	kA
24	#num#	k4
300	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>