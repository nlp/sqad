<s>
Značka	značka	k1gFnSc1	značka
pro	pro	k7c4	pro
peso	peso	k1gNnSc4	peso
je	být	k5eAaImIp3nS	být
$	$	kIx~	$
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
rozlišení	rozlišení	k1gNnSc4	rozlišení
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
měn	měna	k1gFnPc2	měna
se	se	k3xPyFc4	se
před	před	k7c4	před
tento	tento	k3xDgInSc4	tento
znak	znak	k1gInSc4	znak
dávají	dávat	k5eAaImIp3nP	dávat
písmena	písmeno	k1gNnPc1	písmeno
Mex	Mex	k1gFnSc2	Mex
(	(	kIx(	(
<g/>
Mex	Mex	k1gMnSc1	Mex
<g/>
$	$	kIx~	$
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISO	ISO	kA	ISO
4217	[number]	k4	4217
kód	kód	k1gInSc4	kód
mexické	mexický	k2eAgFnSc2d1	mexická
měny	měna	k1gFnSc2	měna
je	být	k5eAaImIp3nS	být
MXN	MXN	kA	MXN
<g/>
.	.	kIx.	.
</s>
