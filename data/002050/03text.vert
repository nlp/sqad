<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
of	of	k?	of
America	America	k1gMnSc1	America
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
také	také	k9	také
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
US	US	kA	US
nebo	nebo	k8xC	nebo
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
také	také	k9	také
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
demokratická	demokratický	k2eAgFnSc1d1	demokratická
federativní	federativní	k2eAgFnSc1d1	federativní
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
republika	republika	k1gFnSc1	republika
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
rozkládající	rozkládající	k2eAgMnSc1d1	rozkládající
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Atlantským	atlantský	k2eAgInSc7d1	atlantský
oceánem	oceán	k1gInSc7	oceán
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
Tichým	tichý	k2eAgInSc7d1	tichý
oceánem	oceán	k1gInSc7	oceán
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Kanadou	Kanada	k1gFnSc7	Kanada
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
státu	stát	k1gInSc3	stát
Aljaška	Aljaška	k1gFnSc1	Aljaška
sahá	sahat	k5eAaImIp3nS	sahat
území	území	k1gNnSc4	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
i	i	k8xC	i
k	k	k7c3	k
břehům	břeh	k1gInPc3	břeh
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
oceánu	oceán	k1gInSc2	oceán
(	(	kIx(	(
<g/>
Beringova	Beringův	k2eAgFnSc1d1	Beringova
úžina	úžina	k1gFnSc1	úžina
je	on	k3xPp3gInPc4	on
dělí	dělit	k5eAaImIp3nS	dělit
od	od	k7c2	od
asijského	asijský	k2eAgNnSc2d1	asijské
území	území	k1gNnSc2	území
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
také	také	k9	také
některé	některý	k3yIgInPc1	některý
tichomořské	tichomořský	k2eAgInPc1d1	tichomořský
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
Havaj	Havaj	k1gFnSc1	Havaj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
50	[number]	k4	50
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgNnSc2	jeden
federálního	federální	k2eAgNnSc2d1	federální
území	území	k1gNnSc2	území
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
a	a	k8xC	a
sídlem	sídlo	k1gNnSc7	sídlo
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
Kongresu	kongres	k1gInSc2	kongres
a	a	k8xC	a
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
(	(	kIx(	(
<g/>
District	District	k1gInSc1	District
of	of	k?	of
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šesti	šest	k4xCc2	šest
závislých	závislý	k2eAgNnPc2d1	závislé
území	území	k1gNnPc2	území
(	(	kIx(	(
<g/>
Portoriko	Portoriko	k1gNnSc1	Portoriko
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnPc1d1	severní
Mariany	Mariana	k1gFnPc1	Mariana
<g/>
,	,	kIx,	,
Guam	Guam	k1gInSc1	Guam
<g/>
,	,	kIx,	,
Americké	americký	k2eAgInPc1d1	americký
Panenské	panenský	k2eAgInPc1d1	panenský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Americká	americký	k2eAgFnSc1d1	americká
Samoa	Samoa	k1gFnSc1	Samoa
a	a	k8xC	a
atol	atol	k1gInSc1	atol
Palmyra	Palmyra	k1gFnSc1	Palmyra
<g/>
)	)	kIx)	)
a	a	k8xC	a
deseti	deset	k4xCc2	deset
malých	malý	k2eAgInPc2d1	malý
ostrovů	ostrov	k1gInPc2	ostrov
či	či	k8xC	či
útesů	útes	k1gInPc2	útes
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
9,8	[number]	k4	9,8
milionu	milion	k4xCgInSc2	milion
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
jsou	být	k5eAaImIp3nP	být
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
třetí	třetí	k4xOgFnSc7	třetí
největší	veliký	k2eAgFnSc7d3	veliký
zemí	zem	k1gFnSc7	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
po	po	k7c6	po
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
327	[number]	k4	327
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
třetí	třetí	k4xOgFnSc7	třetí
nejlidnatější	lidnatý	k2eAgFnSc7d3	nejlidnatější
zemí	zem	k1gFnSc7	zem
planety	planeta	k1gFnSc2	planeta
(	(	kIx(	(
<g/>
po	po	k7c6	po
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
Indii	Indie	k1gFnSc6	Indie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
koncentrována	koncentrován	k2eAgFnSc1d1	koncentrována
na	na	k7c6	na
obou	dva	k4xCgNnPc6	dva
pobřežích	pobřeží	k1gNnPc6	pobřeží
<g/>
,	,	kIx,	,
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
leží	ležet	k5eAaImIp3nS	ležet
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
hustěji	husto	k6eAd2	husto
osídlena	osídlen	k2eAgFnSc1d1	osídlena
než	než	k8xS	než
západní	západní	k2eAgFnSc1d1	západní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
nominálního	nominální	k2eAgMnSc2d1	nominální
HDP	HDP	kA	HDP
jsou	být	k5eAaImIp3nP	být
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
největší	veliký	k2eAgInPc1d3	veliký
světovou	světový	k2eAgFnSc7d1	světová
ekonomikou	ekonomika	k1gFnSc7	ekonomika
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
HDP	HDP	kA	HDP
v	v	k7c6	v
paritě	parita	k1gFnSc6	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc1d3	veliký
(	(	kIx(	(
<g/>
po	po	k7c6	po
Číně	Čína	k1gFnSc6	Čína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
výkon	výkon	k1gInSc1	výkon
představuje	představovat	k5eAaImIp3nS	představovat
přibližně	přibližně	k6eAd1	přibližně
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
největším	veliký	k2eAgMnSc7d3	veliký
světovým	světový	k2eAgMnSc7d1	světový
dovozcem	dovozce	k1gMnSc7	dovozce
a	a	k8xC	a
druhým	druhý	k4xOgMnPc3	druhý
největším	veliký	k2eAgMnPc3d3	veliký
vývozcem	vývozce	k1gMnSc7	vývozce
<g/>
.	.	kIx.	.
</s>
<s>
Staly	stát	k5eAaPmAgInP	stát
se	s	k7c7	s
symbolem	symbol	k1gInSc7	symbol
a	a	k8xC	a
průkopníkem	průkopník	k1gMnSc7	průkopník
ekonomiky	ekonomika	k1gFnSc2	ekonomika
založené	založený	k2eAgInPc4d1	založený
na	na	k7c6	na
volném	volný	k2eAgInSc6d1	volný
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
inspirovaly	inspirovat	k5eAaBmAgInP	inspirovat
Evropu	Evropa	k1gFnSc4	Evropa
nebo	nebo	k8xC	nebo
Čínu	Čína	k1gFnSc4	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
Evropě	Evropa	k1gFnSc3	Evropa
ovšem	ovšem	k9	ovšem
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
menší	malý	k2eAgFnSc4d2	menší
míru	míra	k1gFnSc4	míra
přerozdělování	přerozdělování	k1gNnSc2	přerozdělování
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
větší	veliký	k2eAgInPc4d2	veliký
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nominálním	nominální	k2eAgNnSc6d1	nominální
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnSc4	obyvatel
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
osmém	osmý	k4xOgInSc6	osmý
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
HDP	HDP	kA	HDP
v	v	k7c6	v
paritě	parita	k1gFnSc6	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
na	na	k7c6	na
dvanáctém	dvanáctý	k4xOgInSc6	dvanáctý
<g/>
,	,	kIx,	,
v	v	k7c6	v
indexu	index	k1gInSc6	index
lidského	lidský	k2eAgInSc2d1	lidský
rozvoje	rozvoj	k1gInSc2	rozvoj
na	na	k7c6	na
třináctém	třináctý	k4xOgInSc6	třináctý
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
měna	měna	k1gFnSc1	měna
<g/>
,	,	kIx,	,
dolar	dolar	k1gInSc1	dolar
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
rezervní	rezervní	k2eAgFnSc7d1	rezervní
měnou	měna	k1gFnSc7	měna
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
obchod	obchod	k1gInSc1	obchod
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
vynikajících	vynikající	k2eAgInPc2d1	vynikající
výsledků	výsledek	k1gInPc2	výsledek
a	a	k8xC	a
dobyl	dobýt	k5eAaPmAgMnS	dobýt
trhy	trh	k1gInPc4	trh
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
Ford	ford	k1gInSc1	ford
<g/>
,	,	kIx,	,
Coca-Cola	cocaola	k1gFnSc1	coca-cola
<g/>
,	,	kIx,	,
Boeing	boeing	k1gInSc1	boeing
<g/>
,	,	kIx,	,
JP	JP	kA	JP
Morgan	morgan	k1gMnSc1	morgan
<g/>
,	,	kIx,	,
Exxon	Exxon	k1gMnSc1	Exxon
<g/>
,	,	kIx,	,
Johnson	Johnson	k1gMnSc1	Johnson
&	&	k?	&
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
,	,	kIx,	,
Walmart	Walmart	k1gInSc1	Walmart
<g/>
,	,	kIx,	,
Amazon	amazona	k1gFnPc2	amazona
<g/>
,	,	kIx,	,
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
<g/>
,	,	kIx,	,
Google	Google	k1gFnSc1	Google
<g/>
,	,	kIx,	,
Facebook	Facebook	k1gInSc1	Facebook
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
většiny	většina	k1gFnSc2	většina
kritérií	kritérion	k1gNnPc2	kritérion
jsou	být	k5eAaImIp3nP	být
vojensky	vojensky	k6eAd1	vojensky
nejmocnější	mocný	k2eAgFnSc7d3	nejmocnější
zemí	zem	k1gFnSc7	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřicet	čtyřicet	k4xCc1	čtyřicet
procent	procento	k1gNnPc2	procento
vojenských	vojenský	k2eAgInPc2d1	vojenský
výdajů	výdaj	k1gInPc2	výdaj
na	na	k7c6	na
světě	svět	k1gInSc6	svět
připadá	připadat	k5eAaPmIp3nS	připadat
právě	právě	k9	právě
na	na	k7c6	na
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jadernou	jaderný	k2eAgFnSc7d1	jaderná
velmocí	velmoc	k1gFnSc7	velmoc
a	a	k8xC	a
jedinou	jediný	k2eAgFnSc7d1	jediná
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jadernou	jaderný	k2eAgFnSc4d1	jaderná
zbraň	zbraň	k1gFnSc4	zbraň
použila	použít	k5eAaPmAgFnS	použít
ve	v	k7c6	v
vojenské	vojenský	k2eAgFnSc6d1	vojenská
operaci	operace	k1gFnSc6	operace
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
stálých	stálý	k2eAgMnPc2d1	stálý
členů	člen	k1gMnPc2	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Geopoliticky	geopoliticky	k6eAd1	geopoliticky
jsou	být	k5eAaImIp3nP	být
hodnoceny	hodnotit	k5eAaImNgFnP	hodnotit
jako	jako	k8xS	jako
supervelmoc	supervelmoc	k1gFnSc1	supervelmoc
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tento	tento	k3xDgInSc4	tento
statut	statut	k1gInSc4	statut
získali	získat	k5eAaPmAgMnP	získat
především	především	k9	především
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
vítězství	vítězství	k1gNnSc6	vítězství
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konci	konec	k1gInSc6	konec
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
hovořilo	hovořit	k5eAaImAgNnS	hovořit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
supervelmocí	supervelmoc	k1gFnSc7	supervelmoc
jedinou	jediný	k2eAgFnSc7d1	jediná
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
snaží	snažit	k5eAaImIp3nP	snažit
změnit	změnit	k5eAaPmF	změnit
zejména	zejména	k9	zejména
Čína	Čína	k1gFnSc1	Čína
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Specifické	specifický	k2eAgInPc1d1	specifický
jsou	být	k5eAaImIp3nP	být
vztahy	vztah	k1gInPc1	vztah
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
s	s	k7c7	s
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
zejména	zejména	k9	zejména
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
evropské	evropský	k2eAgFnPc1d1	Evropská
velmoci	velmoc	k1gFnPc1	velmoc
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
severní	severní	k2eAgFnPc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
kolonizovaly	kolonizovat	k5eAaBmAgFnP	kolonizovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pak	pak	k6eAd1	pak
čelily	čelit	k5eAaImAgInP	čelit
vzpouře	vzpoura	k1gFnSc3	vzpoura
místních	místní	k2eAgMnPc2d1	místní
kolonistů	kolonista	k1gMnPc2	kolonista
a	a	k8xC	a
hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
střet	střet	k1gInSc1	střet
s	s	k7c7	s
Brity	Brit	k1gMnPc7	Brit
byl	být	k5eAaImAgMnS	být
rozhodující	rozhodující	k2eAgMnSc1d1	rozhodující
(	(	kIx(	(
<g/>
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c4	na
Británii	Británie	k1gFnSc4	Británie
Američané	Američan	k1gMnPc1	Američan
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
roku	rok	k1gInSc2	rok
1776	[number]	k4	1776
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
prosazení	prosazení	k1gNnSc3	prosazení
se	se	k3xPyFc4	se
angličtiny	angličtina	k1gFnSc2	angličtina
jako	jako	k8xC	jako
hlavního	hlavní	k2eAgInSc2d1	hlavní
a	a	k8xC	a
úředního	úřední	k2eAgInSc2d1	úřední
jazyka	jazyk	k1gInSc2	jazyk
USA	USA	kA	USA
však	však	k8xC	však
specifická	specifický	k2eAgFnSc1d1	specifická
vazba	vazba	k1gFnSc1	vazba
mezi	mezi	k7c7	mezi
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
Británií	Británie	k1gFnSc7	Británie
(	(	kIx(	(
<g/>
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
dalšími	další	k2eAgFnPc7d1	další
anglicky	anglicky	k6eAd1	anglicky
mluvícími	mluvící	k2eAgFnPc7d1	mluvící
zeměmi	zem	k1gFnPc7	zem
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Kanada	Kanada	k1gFnSc1	Kanada
či	či	k8xC	či
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
)	)	kIx)	)
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
<g/>
.	.	kIx.	.
</s>
<s>
Vazba	vazba	k1gFnSc1	vazba
se	s	k7c7	s
západní	západní	k2eAgFnSc7d1	západní
kontinentální	kontinentální	k2eAgFnSc7d1	kontinentální
Evropou	Evropa	k1gFnSc7	Evropa
byla	být	k5eAaImAgFnS	být
dána	dát	k5eAaPmNgFnS	dát
imigračními	imigrační	k2eAgFnPc7d1	imigrační
vlnami	vlna	k1gFnPc7	vlna
v	v	k7c6	v
19.	[number]	k4	19.
století	století	k1gNnSc6	století
(	(	kIx(	(
<g/>
zejm	zejm	k?	zejm
<g/>
.	.	kIx.	.
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posílena	posílen	k2eAgFnSc1d1	posílena
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
americkém	americký	k2eAgNnSc6d1	americké
osvobození	osvobození	k1gNnSc6	osvobození
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
Evropy	Evropa	k1gFnSc2	Evropa
od	od	k7c2	od
nacismu	nacismus	k1gInSc2	nacismus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
vojenské	vojenský	k2eAgFnSc2d1	vojenská
organizace	organizace	k1gFnSc2	organizace
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
staly	stát	k5eAaPmAgFnP	stát
garantem	garant	k1gMnSc7	garant
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunismu	komunismus	k1gInSc2	komunismus
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
podobně	podobně	k6eAd1	podobně
úzké	úzký	k2eAgFnPc1d1	úzká
spojenecké	spojenecký	k2eAgFnPc1d1	spojenecká
vazby	vazba	k1gFnPc1	vazba
i	i	k9	i
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
klíčovým	klíčový	k2eAgMnSc7d1	klíčový
spojencem	spojenec	k1gMnSc7	spojenec
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Američany	Američan	k1gMnPc4	Američan
zejména	zejména	k9	zejména
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
supervelmoc	supervelmoc	k1gFnSc1	supervelmoc
se	se	k3xPyFc4	se
však	však	k9	však
USA	USA	kA	USA
snaží	snažit	k5eAaImIp3nS	snažit
budovat	budovat	k5eAaImF	budovat
spojenecké	spojenecký	k2eAgFnSc2d1	spojenecká
vazby	vazba	k1gFnSc2	vazba
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
regionech	region	k1gInPc6	region
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
klíčovým	klíčový	k2eAgMnPc3d1	klíčový
spojencům	spojenec	k1gMnPc3	spojenec
Izrael	Izrael	k1gInSc4	Izrael
a	a	k8xC	a
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
takovým	takový	k3xDgMnSc7	takový
spojencem	spojenec	k1gMnSc7	spojenec
stává	stávat	k5eAaImIp3nS	stávat
i	i	k9	i
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americká	americký	k2eAgFnSc1d1	americká
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
pop-kultura	popultura	k1gFnSc1	pop-kultura
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
globální	globální	k2eAgInSc4d1	globální
vliv	vliv	k1gInSc4	vliv
(	(	kIx(	(
<g/>
Hollywood	Hollywood	k1gInSc1	Hollywood
<g/>
,	,	kIx,	,
Disney	Disney	k1gInPc1	Disney
<g/>
,	,	kIx,	,
jazz	jazz	k1gInSc1	jazz
<g/>
,	,	kIx,	,
rockenrol	rockenrol	k1gInSc1	rockenrol
<g/>
,	,	kIx,	,
rap	rap	k1gMnSc1	rap
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInPc4d1	televizní
seriály	seriál	k1gInPc4	seriál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
americký	americký	k2eAgInSc1d1	americký
životní	životní	k2eAgInSc1d1	životní
styl	styl	k1gInSc1	styl
(	(	kIx(	(
<g/>
automobilismus	automobilismus	k1gInSc1	automobilismus
<g/>
,	,	kIx,	,
fast	fast	k2eAgInSc1d1	fast
food	food	k1gInSc1	food
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k6eAd1	až
takový	takový	k3xDgInSc4	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
USA	USA	kA	USA
začalo	začít	k5eAaPmAgNnS	začít
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
americkém	americký	k2eAgInSc6d1	americký
kulturním	kulturní	k2eAgInSc6d1	kulturní
imperialismu	imperialismus	k1gInSc6	imperialismus
či	či	k8xC	či
amerikanizaci	amerikanizace	k1gFnSc3	amerikanizace
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Výborných	výborný	k2eAgInPc2d1	výborný
výsledků	výsledek	k1gInPc2	výsledek
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
americká	americký	k2eAgFnSc1d1	americká
věda	věda	k1gFnSc1	věda
a	a	k8xC	a
školství	školství	k1gNnSc1	školství
(	(	kIx(	(
<g/>
systém	systém	k1gInSc1	systém
univerzit	univerzita	k1gFnPc2	univerzita
s	s	k7c7	s
Harvardem	Harvard	k1gInSc7	Harvard
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
<g/>
,	,	kIx,	,
vynález	vynález	k1gInSc4	vynález
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
,	,	kIx,	,
internetu	internet	k1gInSc2	internet
<g/>
,	,	kIx,	,
přistání	přistání	k1gNnSc4	přistání
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Původ	původ	k1gMnSc1	původ
pojmu	pojem	k1gInSc2	pojem
Amerika	Amerika	k1gFnSc1	Amerika
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
<g/>
:	:	kIx,	:
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1507	[number]	k4	1507
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
německý	německý	k2eAgMnSc1d1	německý
kartograf	kartograf	k1gMnSc1	kartograf
Martin	Martin	k1gMnSc1	Martin
Waldseemüller	Waldseemüller	k1gMnSc1	Waldseemüller
mapu	mapa	k1gFnSc4	mapa
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
na	na	k7c6	na
počest	počest	k1gFnSc6	počest
italského	italský	k2eAgMnSc2d1	italský
objevitele	objevitel	k1gMnSc2	objevitel
a	a	k8xC	a
kartografa	kartograf	k1gMnSc2	kartograf
Ameriga	Amerig	k1gMnSc2	Amerig
Vespucciho	Vespucci	k1gMnSc2	Vespucci
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Americus	Americus	k1gMnSc1	Americus
Vespucius	Vespucius	k1gMnSc1	Vespucius
<g/>
)	)	kIx)	)
označil	označit	k5eAaPmAgMnS	označit
země	zem	k1gFnPc4	zem
západní	západní	k2eAgFnSc2d1	západní
polokoule	polokoule	k1gFnSc2	polokoule
"	"	kIx"	"
<g/>
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zvolil	zvolit	k5eAaPmAgMnS	zvolit
Vespucciho	Vespucciha	k1gMnSc5	Vespucciha
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
Kryštofa	Kryštof	k1gMnSc4	Kryštof
Kolumba	Kolumbus	k1gMnSc4	Kolumbus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
doplul	doplout	k5eAaPmAgInS	doplout
k	k	k7c3	k
americkým	americký	k2eAgInPc3d1	americký
břehům	břeh	k1gInPc3	břeh
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Vespucci	Vespucce	k1gMnPc1	Vespucce
si	se	k3xPyFc3	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Kolumba	Kolumbus	k1gMnSc2	Kolumbus
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
kontinentě	kontinent	k1gInSc6	kontinent
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
Vespucci	Vespucce	k1gFnSc4	Vespucce
nazýval	nazývat	k5eAaImAgInS	nazývat
nový	nový	k2eAgInSc1d1	nový
kontinent	kontinent	k1gInSc1	kontinent
"	"	kIx"	"
<g/>
Nový	nový	k2eAgInSc1d1	nový
svět	svět	k1gInSc1	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgNnPc1	první
užití	užití	k1gNnSc1	užití
pojmu	pojem	k1gInSc2	pojem
"	"	kIx"	"
<g/>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
doloženo	doložit	k5eAaPmNgNnS	doložit
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
z	z	k7c2	z
2.	[number]	k4	2.
ledna	leden	k1gInSc2	leden
1776	[number]	k4	1776
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
napsal	napsat	k5eAaPmAgMnS	napsat
irský	irský	k2eAgMnSc1d1	irský
generál	generál	k1gMnSc1	generál
americké	americký	k2eAgFnSc2d1	americká
revoluční	revoluční	k2eAgFnSc2d1	revoluční
armády	armáda	k1gFnSc2	armáda
Stephen	Stephen	k2eAgMnSc1d1	Stephen
Moylan	Moylan	k1gMnSc1	Moylan
asistentovi	asistent	k1gMnSc3	asistent
George	Georg	k1gMnPc4	Georg
Washingtona	Washington	k1gMnSc2	Washington
Josephu	Joseph	k1gInSc2	Joseph
Reedovi	Reeda	k1gMnSc3	Reeda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
veřejném	veřejný	k2eAgInSc6d1	veřejný
textu	text	k1gInSc6	text
se	se	k3xPyFc4	se
pojem	pojem	k1gInSc1	pojem
prvně	prvně	k?	prvně
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
anonymní	anonymní	k2eAgFnSc6d1	anonymní
eseji	esej	k1gFnSc6	esej
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
The	The	k1gFnSc2	The
Virginia	Virginium	k1gNnSc2	Virginium
Gazette	Gazett	k1gMnSc5	Gazett
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
vycházely	vycházet	k5eAaImAgFnP	vycházet
ve	v	k7c6	v
Williamsburgu	Williamsburg	k1gInSc6	Williamsburg
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
vyšel	vyjít	k5eAaPmAgInS	vyjít
6.	[number]	k4	6.
dubna	duben	k1gInSc2	duben
1776	[number]	k4	1776
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
právně	právně	k6eAd1	právně
závazném	závazný	k2eAgInSc6d1	závazný
textu	text	k1gInSc6	text
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
návrhu	návrh	k1gInSc6	návrh
Článků	článek	k1gInPc2	článek
konfederace	konfederace	k1gFnSc2	konfederace
a	a	k8xC	a
trvalé	trvalý	k2eAgFnSc2d1	trvalá
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
připravil	připravit	k5eAaPmAgMnS	připravit
John	John	k1gMnSc1	John
Dickinson	Dickinson	k1gMnSc1	Dickinson
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
vyhotovil	vyhotovit	k5eAaPmAgInS	vyhotovit
k	k	k7c3	k
17.	[number]	k4	17.
červnu	červen	k1gInSc3	červen
1776	[number]	k4	1776
a	a	k8xC	a
uvádělo	uvádět	k5eAaImAgNnS	uvádět
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jméno	jméno	k1gNnSc1	jméno
této	tento	k3xDgFnSc2	tento
konfederace	konfederace	k1gFnSc2	konfederace
je	být	k5eAaImIp3nS	být
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1776	[number]	k4	1776
také	také	k9	také
Thomas	Thomas	k1gMnSc1	Thomas
Jefferson	Jefferson	k1gMnSc1	Jefferson
napsal	napsat	k5eAaPmAgMnS	napsat
pojem	pojem	k1gInSc4	pojem
"	"	kIx"	"
<g/>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
"	"	kIx"	"
velkými	velký	k2eAgNnPc7d1	velké
písmeny	písmeno	k1gNnPc7	písmeno
do	do	k7c2	do
nadpisu	nadpis	k1gInSc2	nadpis
originálního	originální	k2eAgInSc2d1	originální
hrubého	hrubý	k2eAgInSc2d1	hrubý
návrhu	návrh	k1gInSc2	návrh
Deklarace	deklarace	k1gFnSc1	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
<g/>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
též	též	k9	též
krátká	krátký	k2eAgFnSc1d1	krátká
verze	verze	k1gFnSc1	verze
názvu	název	k1gInSc2	název
"	"	kIx"	"
<g/>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
anglická	anglický	k2eAgFnSc1d1	anglická
zkratka	zkratka	k1gFnSc1	zkratka
"	"	kIx"	"
<g/>
USA	USA	kA	USA
<g/>
"	"	kIx"	"
v	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
i	i	k8xC	i
české	český	k2eAgFnSc3d1	Česká
výslovnosti	výslovnost	k1gFnSc3	výslovnost
(	(	kIx(	(
<g/>
logický	logický	k2eAgInSc4d1	logický
český	český	k2eAgInSc4d1	český
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
SSA	SSA	kA	SSA
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
zavededen	zavededen	k2eAgMnSc1d1	zavededen
není	být	k5eNaImIp3nS	být
<g/>
)	)	kIx)	)
či	či	k8xC	či
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18.	[number]	k4	18.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
užívalo	užívat	k5eAaImAgNnS	užívat
též	též	k9	též
pojmu	pojem	k1gInSc3	pojem
"	"	kIx"	"
<g/>
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
odkazoval	odkazovat	k5eAaImAgMnS	odkazovat
k	k	k7c3	k
objeviteli	objevitel	k1gMnSc3	objevitel
kontinentu	kontinent	k1gInSc2	kontinent
Kryštofu	Kryštof	k1gMnSc3	Kryštof
Kolumbovi	Kolumbus	k1gMnSc3	Kolumbus
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
literární	literární	k2eAgMnSc1d1	literární
<g/>
,	,	kIx,	,
časem	čas	k1gInSc7	čas
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
zachován	zachovat	k5eAaPmNgInS	zachovat
v	v	k7c6	v
označení	označení	k1gNnSc6	označení
federálního	federální	k2eAgNnSc2d1	federální
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
de	de	k?	de
facto	facto	k1gNnSc1	facto
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
District	District	k1gInSc1	District
of	of	k?	of
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
pojem	pojem	k1gInSc1	pojem
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
nejprve	nejprve	k6eAd1	nejprve
užíval	užívat	k5eAaImAgMnS	užívat
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
are	ar	k1gInSc5	ar
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
jednotném	jednotný	k2eAgNnSc6d1	jednotné
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
is	is	k?	is
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
zdůrazněno	zdůraznit	k5eAaPmNgNnS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nerozbornou	rozborný	k2eNgFnSc4d1	nerozborná
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
ale	ale	k9	ale
tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
neprosadila	prosadit	k5eNaPmAgFnS	prosadit
a	a	k8xC	a
užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
plurál	plurál	k1gInSc1	plurál
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
jsou	být	k5eAaImIp3nP	být
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
"	"	kIx"	"
<g/>
americký	americký	k2eAgInSc1d1	americký
<g/>
"	"	kIx"	"
převážil	převážit	k5eAaPmAgInS	převážit
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
význam	význam	k1gInSc1	význam
"	"	kIx"	"
<g/>
vztahující	vztahující	k2eAgMnSc1d1	vztahující
se	se	k3xPyFc4	se
ke	k	k7c3	k
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
pojem	pojem	k1gInSc1	pojem
může	moct	k5eAaImIp3nS	moct
značit	značit	k5eAaImF	značit
i	i	k8xC	i
"	"	kIx"	"
<g/>
vztahující	vztahující	k2eAgMnSc1d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
celému	celý	k2eAgInSc3d1	celý
americkému	americký	k2eAgInSc3d1	americký
kontinentu	kontinent	k1gInSc3	kontinent
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
severní	severní	k2eAgFnSc6d1	severní
i	i	k8xC	i
jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
význam	význam	k1gInSc1	význam
je	být	k5eAaImIp3nS	být
však	však	k9	však
typický	typický	k2eAgInSc1d1	typický
jen	jen	k9	jen
pro	pro	k7c4	pro
odbornější	odborný	k2eAgFnPc4d2	odbornější
geografické	geografický	k2eAgFnPc4d1	geografická
<g/>
,	,	kIx,	,
geologické	geologický	k2eAgFnPc4d1	geologická
či	či	k8xC	či
jiné	jiný	k2eAgFnPc4d1	jiná
přírodovědné	přírodovědný	k2eAgFnPc4d1	přírodovědná
publikace	publikace	k1gFnPc4	publikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
řeči	řeč	k1gFnSc6	řeč
a	a	k8xC	a
publicistice	publicistika	k1gFnSc6	publicistika
převažuje	převažovat	k5eAaImIp3nS	převažovat
význam	význam	k1gInSc1	význam
prvý	prvý	k4xOgInSc1	prvý
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
pak	pak	k6eAd1	pak
u	u	k7c2	u
pojmu	pojem	k1gInSc2	pojem
Američan	Američan	k1gMnSc1	Američan
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
vždy	vždy	k6eAd1	vždy
míněn	míněn	k2eAgMnSc1d1	míněn
"	"	kIx"	"
<g/>
občan	občan	k1gMnSc1	občan
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Stejné	stejný	k2eAgNnSc1d1	stejné
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
u	u	k7c2	u
pojmu	pojem	k1gInSc2	pojem
"	"	kIx"	"
<g/>
American	American	k1gInSc1	American
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
jím	on	k3xPp3gInSc7	on
označují	označovat	k5eAaImIp3nP	označovat
jak	jak	k6eAd1	jak
"	"	kIx"	"
<g/>
občana	občan	k1gMnSc4	občan
USA	USA	kA	USA
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tak	tak	k9	tak
"	"	kIx"	"
<g/>
příslušníka	příslušník	k1gMnSc4	příslušník
amerického	americký	k2eAgInSc2d1	americký
národa	národ	k1gInSc2	národ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
prakticky	prakticky	k6eAd1	prakticky
není	být	k5eNaImIp3nS	být
rozdíl	rozdíl	k1gInSc4	rozdíl
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
americké	americký	k2eAgFnSc3d1	americká
politické	politický	k2eAgFnSc3d1	politická
(	(	kIx(	(
<g/>
státní	státní	k2eAgFnSc3d1	státní
<g/>
)	)	kIx)	)
koncepci	koncepce	k1gFnSc3	koncepce
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
převažuje	převažovat	k5eAaImIp3nS	převažovat
pojetí	pojetí	k1gNnSc1	pojetí
národa	národ	k1gInSc2	národ
kulturní	kulturní	k2eAgInSc1d1	kulturní
(	(	kIx(	(
<g/>
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kultura	kultura	k1gFnSc1	kultura
<g/>
)	)	kIx)	)
či	či	k8xC	či
etnické	etnický	k2eAgFnPc4d1	etnická
(	(	kIx(	(
<g/>
společný	společný	k2eAgInSc4d1	společný
původ	původ	k1gInSc4	původ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
také	také	k9	také
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
nation	nation	k1gInSc1	nation
<g/>
"	"	kIx"	"
prakticky	prakticky	k6eAd1	prakticky
splývá	splývat	k5eAaImIp3nS	splývat
s	s	k7c7	s
pojmem	pojem	k1gInSc7	pojem
"	"	kIx"	"
<g/>
stát	stát	k1gInSc1	stát
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
lid	lid	k1gInSc1	lid
státu	stát	k1gInSc2	stát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pojem	pojem	k1gInSc4	pojem
národ	národ	k1gInSc4	národ
v	v	k7c6	v
evropském	evropský	k2eAgInSc6d1	evropský
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
spíše	spíše	k9	spíše
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
ethnicity	ethnicita	k1gFnPc1	ethnicita
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Původní	původní	k2eAgNnSc1d1	původní
osídlení	osídlení	k1gNnSc1	osídlení
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Původní	původní	k2eAgNnSc1d1	původní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Indiáni	Indián	k1gMnPc1	Indián
a	a	k8xC	a
Eskymáci	Eskymák	k1gMnPc1	Eskymák
<g/>
,	,	kIx,	,
osídlilo	osídlit	k5eAaPmAgNnS	osídlit
americký	americký	k2eAgInSc4d1	americký
kontinent	kontinent	k1gInSc4	kontinent
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgInSc1d1	tradiční
odhad	odhad	k1gInSc1	odhad
příchodu	příchod	k1gInSc2	příchod
tzv.	tzv.	kA	tzv.
paleoindiánů	paleoindián	k1gInPc2	paleoindián
přes	přes	k7c4	přes
zamrzlou	zamrzlý	k2eAgFnSc4d1	zamrzlá
Beringovu	Beringův	k2eAgFnSc4d1	Beringova
úžinu	úžina	k1gFnSc4	úžina
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
12 000-11 000	[number]	k4	12 000-11 000
let	léto	k1gNnPc2	léto
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
novější	nový	k2eAgInPc1d2	novější
výzkumy	výzkum	k1gInPc1	výzkum
počítají	počítat	k5eAaImIp3nP	počítat
i	i	k9	i
s	s	k7c7	s
časnějším	časný	k2eAgNnSc7d2	časnější
osídlením	osídlení	k1gNnSc7	osídlení
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Kolonisté	kolonista	k1gMnPc1	kolonista
ze	z	k7c2	z
Sibiře	Sibiř	k1gFnSc2	Sibiř
postupovali	postupovat	k5eAaImAgMnP	postupovat
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
patrně	patrně	k6eAd1	patrně
takto	takto	k6eAd1	takto
osídlili	osídlit	k5eAaPmAgMnP	osídlit
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
jižní	jižní	k2eAgFnSc4d1	jižní
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
kultura	kultura	k1gFnSc1	kultura
je	být	k5eAaImIp3nS	být
nazývána	nazývat	k5eAaImNgFnS	nazývat
kultura	kultura	k1gFnSc1	kultura
Clovis	Clovis	k1gFnSc1	Clovis
<g/>
,	,	kIx,	,
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
byli	být	k5eAaImAgMnP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
zejména	zejména	k9	zejména
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Novém	nový	k2eAgNnSc6d1	nové
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
Texasu	Texas	k1gInSc6	Texas
<g/>
,	,	kIx,	,
Virginii	Virginie	k1gFnSc6	Virginie
či	či	k8xC	či
Pensylvánii	Pensylvánie	k1gFnSc6	Pensylvánie
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Clovis	Clovis	k1gFnSc1	Clovis
byla	být	k5eAaImAgFnS	být
dlouho	dlouho	k6eAd1	dlouho
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejstarší	starý	k2eAgFnSc4d3	nejstarší
domorodou	domorodý	k2eAgFnSc4d1	domorodá
kulturu	kultura	k1gFnSc4	kultura
obou	dva	k4xCgFnPc2	dva
Amerik	Amerika	k1gFnPc2	Amerika
a	a	k8xC	a
prapředka	prapředek	k1gMnSc2	prapředek
všech	všecek	k3xTgFnPc2	všecek
kultur	kultura	k1gFnPc2	kultura
pozdějších	pozdní	k2eAgMnPc2d2	pozdější
<g/>
,	,	kIx,	,
množí	množit	k5eAaImIp3nP	množit
se	se	k3xPyFc4	se
ale	ale	k9	ale
důkazy	důkaz	k1gInPc1	důkaz
i	i	k8xC	i
o	o	k7c6	o
některých	některý	k3yIgFnPc6	některý
starších	starý	k2eAgFnPc6d2	starší
kulturách	kultura	k1gFnPc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pozdějších	pozdní	k2eAgFnPc2d2	pozdější
předkolumbovských	předkolumbovský	k2eAgFnPc2d1	předkolumbovská
komunit	komunita	k1gFnPc2	komunita
byla	být	k5eAaImAgFnS	být
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
Mississippská	mississippský	k2eAgFnSc1d1	Mississippská
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
kulminovala	kulminovat	k5eAaImAgFnS	kulminovat
v	v	k7c4	v
období	období	k1gNnSc4	období
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
800-1600	[number]	k4	800-1600
<g/>
,	,	kIx,	,
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
Cahokii	Cahokie	k1gFnSc6	Cahokie
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
dnešního	dnešní	k2eAgNnSc2d1	dnešní
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
osmdesát	osmdesát	k4xCc1	osmdesát
mohyl	mohyla	k1gFnPc2	mohyla
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
největší	veliký	k2eAgInSc1d3	veliký
je	být	k5eAaImIp3nS	být
Monks	Monks	k1gInSc1	Monks
Mound	Mounda	k1gFnPc2	Mounda
<g/>
,	,	kIx,	,
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
pět	pět	k4xCc4	pět
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
30 000	[number]	k4	30 000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Doloženo	doložen	k2eAgNnSc4d1	doloženo
bylo	být	k5eAaImAgNnS	být
vyspělé	vyspělý	k2eAgNnSc1d1	vyspělé
zemědělství	zemědělství	k1gNnSc1	zemědělství
se	s	k7c7	s
zásadní	zásadní	k2eAgFnSc7d1	zásadní
rolí	role	k1gFnSc7	role
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
,	,	kIx,	,
ptačí	ptačí	k2eAgInSc4d1	ptačí
kult	kult	k1gInSc4	kult
a	a	k8xC	a
lidské	lidský	k2eAgFnPc4d1	lidská
oběti	oběť	k1gFnPc4	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc4	třetí
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
předkolumbovskou	předkolumbovský	k2eAgFnSc4d1	předkolumbovská
kulturu	kultura	k1gFnSc4	kultura
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Anasaziové	Anasazius	k1gMnPc1	Anasazius
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Nového	Nového	k2eAgNnSc2d1	Nového
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
Arizony	Arizona	k1gFnSc2	Arizona
<g/>
,	,	kIx,	,
Colorada	Colorado	k1gNnSc2	Colorado
a	a	k8xC	a
Utahu	Utah	k1gInSc2	Utah
<g/>
.	.	kIx.	.
</s>
<s>
Stavěli	stavět	k5eAaImAgMnP	stavět
sídla	sídlo	k1gNnPc4	sídlo
zvaná	zvaný	k2eAgNnPc1d1	zvané
pueblo	puebnout	k5eAaPmAgNnS	puebnout
<g/>
,	,	kIx,	,
nejslavnější	slavný	k2eAgFnPc1d3	nejslavnější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c4	v
Mesa	Mesus	k1gMnSc4	Mesus
Verde	Verd	k1gInSc5	Verd
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodná	pozoruhodný	k2eAgFnSc1d1	pozoruhodná
kultura	kultura	k1gFnSc1	kultura
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Louisianě	Louisiana	k1gFnSc6	Louisiana
<g/>
,	,	kIx,	,
v	v	k7c6	v
archeologické	archeologický	k2eAgFnSc6d1	archeologická
lokalitě	lokalita	k1gFnSc6	lokalita
Poverty	Povert	k1gMnPc4	Povert
Point	pointa	k1gFnPc2	pointa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
byla	být	k5eAaImAgFnS	být
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
útvarem	útvar	k1gInSc7	útvar
Irokézká	Irokézký	k2eAgFnSc1d1	Irokézký
liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
zažívala	zažívat	k5eAaImAgFnS	zažívat
svůj	svůj	k3xOyFgInSc4	svůj
vrchol	vrchol	k1gInSc4	vrchol
až	až	k9	až
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
Evropanů	Evropan	k1gMnPc2	Evropan
a	a	k8xC	a
sehrávala	sehrávat	k5eAaImAgFnS	sehrávat
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
ještě	ještě	k9	ještě
v	v	k7c6	v
britsko-amerických	britskomerický	k2eAgInPc6d1	britsko-americký
konfliktech	konflikt	k1gInPc6	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
americké	americký	k2eAgFnSc2d1	americká
populace	populace	k1gFnSc2	populace
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Evropanů	Evropan	k1gMnPc2	Evropan
se	se	k3xPyFc4	se
vedou	vést	k5eAaImIp3nP	vést
spory	spor	k1gInPc1	spor
a	a	k8xC	a
odhady	odhad	k1gInPc1	odhad
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
<g/>
,	,	kIx,	,
od	od	k7c2	od
500 000	[number]	k4	500 000
lidí	člověk	k1gMnPc2	člověk
až	až	k6eAd1	až
k	k	k7c3	k
9	[number]	k4	9
milionům	milion	k4xCgInPc3	milion
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
současnou	současný	k2eAgFnSc4d1	současná
evropskou	evropský	k2eAgFnSc4d1	Evropská
civilizaci	civilizace	k1gFnSc4	civilizace
byl	být	k5eAaImAgInS	být
tzv.	tzv.	kA	tzv.
Nový	nový	k2eAgInSc1d1	nový
svět	svět	k1gInSc1	svět
objeven	objevit	k5eAaPmNgInS	objevit
výpravou	výprava	k1gFnSc7	výprava
Kryštofa	Kryštof	k1gMnSc2	Kryštof
Kolumba	Kolumbus	k1gMnSc2	Kolumbus
roku	rok	k1gInSc2	rok
1492	[number]	k4	1492
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
<g/>
,	,	kIx,	,
přistáli	přistát	k5eAaPmAgMnP	přistát
u	u	k7c2	u
břehů	břeh	k1gInPc2	břeh
Ameriky	Amerika	k1gFnSc2	Amerika
Vikingové	Viking	k1gMnPc1	Viking
vedeni	vést	k5eAaImNgMnP	vést
Leifem	Leif	k1gMnSc7	Leif
Erikssonem	Eriksson	k1gMnSc7	Eriksson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Koloniální	koloniální	k2eAgNnSc4d1	koloniální
období	období	k1gNnSc4	období
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
stoletích	století	k1gNnPc6	století
se	se	k3xPyFc4	se
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
stala	stát	k5eAaPmAgFnS	stát
cílem	cíl	k1gInSc7	cíl
kolonizačních	kolonizační	k2eAgFnPc2d1	kolonizační
snah	snaha	k1gFnPc2	snaha
Španělska	Španělsko	k1gNnSc2	Španělsko
(	(	kIx(	(
<g/>
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
území	území	k1gNnSc1	území
západně	západně	k6eAd1	západně
od	od	k7c2	od
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
povodí	povodí	k1gNnSc1	povodí
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
i	i	k8xC	i
Švédska	Švédsko	k1gNnSc2	Švédsko
(	(	kIx(	(
<g/>
Nové	Nové	k2eAgNnSc1d1	Nové
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
historii	historie	k1gFnSc4	historie
budoucích	budoucí	k2eAgInPc2d1	budoucí
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
měla	mít	k5eAaImAgFnS	mít
největší	veliký	k2eAgInSc4d3	veliký
význam	význam	k1gInSc4	význam
anglická	anglický	k2eAgFnSc1d1	anglická
kolonizace	kolonizace	k1gFnSc1	kolonizace
atlantského	atlantský	k2eAgNnSc2d1	Atlantské
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1664	[number]	k4	1664
se	se	k3xPyFc4	se
Anglie	Anglie	k1gFnSc1	Anglie
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Království	království	k1gNnSc1	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
postupně	postupně	k6eAd1	postupně
zmocnily	zmocnit	k5eAaPmAgFnP	zmocnit
nizozemských	nizozemský	k2eAgFnPc2d1	nizozemská
a	a	k8xC	a
části	část	k1gFnSc6	část
francouzských	francouzský	k2eAgFnPc2d1	francouzská
osad	osada	k1gFnPc2	osada
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
13	[number]	k4	13
kolonií	kolonie	k1gFnPc2	kolonie
(	(	kIx(	(
<g/>
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
Jersey	Jersea	k1gFnPc1	Jersea
<g/>
,	,	kIx,	,
New	New	k1gFnPc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
Connecticut	Connecticut	k1gInSc1	Connecticut
<g/>
,	,	kIx,	,
New	New	k1gMnSc5	New
Hampshire	Hampshir	k1gMnSc5	Hampshir
<g/>
,	,	kIx,	,
Pennsylvania	Pennsylvanium	k1gNnPc1	Pennsylvanium
<g/>
,	,	kIx,	,
Delaware	Delawar	k1gMnSc5	Delawar
<g/>
,	,	kIx,	,
Virginia	Virginium	k1gNnPc1	Virginium
<g/>
,	,	kIx,	,
Maryland	Maryland	k1gInSc1	Maryland
<g/>
,	,	kIx,	,
North	North	k1gInSc1	North
Carolina	Carolina	k1gFnSc1	Carolina
<g/>
,	,	kIx,	,
South	South	k1gInSc1	South
Carolina	Carolina	k1gFnSc1	Carolina
<g/>
,	,	kIx,	,
Georgia	Georgia	k1gFnSc1	Georgia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
základem	základ	k1gInSc7	základ
budoucích	budoucí	k2eAgMnPc2d1	budoucí
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
evropské	evropský	k2eAgFnPc1d1	Evropská
mocnosti	mocnost	k1gFnPc1	mocnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
kolonizovaly	kolonizovat	k5eAaBmAgFnP	kolonizovat
sever	sever	k1gInSc4	sever
amerického	americký	k2eAgInSc2d1	americký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
zdůrazňovaly	zdůrazňovat	k5eAaImAgFnP	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
postup	postup	k1gInSc1	postup
není	být	k5eNaImIp3nS	být
kořistnický	kořistnický	k2eAgInSc1d1	kořistnický
jako	jako	k8xC	jako
postup	postup	k1gInSc1	postup
Španělů	Španěl	k1gMnPc2	Španěl
a	a	k8xC	a
Portugalců	Portugalec	k1gMnPc2	Portugalec
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
evropská	evropský	k2eAgFnSc1d1	Evropská
kolonizace	kolonizace	k1gFnSc1	kolonizace
znamenala	znamenat	k5eAaImAgFnS	znamenat
i	i	k9	i
zde	zde	k6eAd1	zde
pro	pro	k7c4	pro
původní	původní	k2eAgMnPc4d1	původní
obyvatele	obyvatel	k1gMnPc4	obyvatel
katastrofu	katastrofa	k1gFnSc4	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
zavlečené	zavlečený	k2eAgFnPc1d1	zavlečená
nemoci	nemoc	k1gFnPc1	nemoc
jako	jako	k8xS	jako
neštovice	neštovice	k1gFnPc1	neštovice
<g/>
,	,	kIx,	,
chřipka	chřipka	k1gFnSc1	chřipka
a	a	k8xC	a
spalničky	spalničky	k1gFnPc1	spalničky
zredukovaly	zredukovat	k5eAaPmAgFnP	zredukovat
jejich	jejich	k3xOp3gFnSc4	jejich
populaci	populace	k1gFnSc4	populace
místy	místy	k6eAd1	místy
až	až	k9	až
o	o	k7c4	o
90	[number]	k4	90
%	%	kIx~	%
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
zničily	zničit	k5eAaPmAgFnP	zničit
jejich	jejich	k3xOp3gFnPc1	jejich
sociální	sociální	k2eAgFnPc1d1	sociální
struktury	struktura	k1gFnPc1	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
již	již	k6eAd1	již
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
kolonizaci	kolonizace	k1gFnSc6	kolonizace
se	se	k3xPyFc4	se
rodil	rodit	k5eAaImAgMnS	rodit
i	i	k9	i
nový	nový	k2eAgMnSc1d1	nový
svobodný	svobodný	k2eAgMnSc1d1	svobodný
duch	duch	k1gMnSc1	duch
budoucího	budoucí	k2eAgInSc2d1	budoucí
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
řada	řada	k1gFnSc1	řada
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
osadníků	osadník	k1gMnPc2	osadník
byli	být	k5eAaImAgMnP	být
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
odcházeli	odcházet	k5eAaImAgMnP	odcházet
ze	z	k7c2	z
starého	starý	k2eAgInSc2d1	starý
kontinentu	kontinent	k1gInSc2	kontinent
kvůli	kvůli	k7c3	kvůli
náboženskému	náboženský	k2eAgInSc3d1	náboženský
útlaku	útlak	k1gInSc3	útlak
a	a	k8xC	a
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
světě	svět	k1gInSc6	svět
naleznou	naleznout	k5eAaPmIp3nP	naleznout
útočiště	útočiště	k1gNnSc4	útočiště
<g/>
.	.	kIx.	.
</s>
<s>
Náboženská	náboženský	k2eAgFnSc1d1	náboženská
svoboda	svoboda	k1gFnSc1	svoboda
a	a	k8xC	a
tolerance	tolerance	k1gFnSc1	tolerance
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgNnPc2d1	klíčové
témat	téma	k1gNnPc2	téma
při	při	k7c6	při
formování	formování	k1gNnSc6	formování
novodobé	novodobý	k2eAgFnSc2d1	novodobá
severoamerické	severoamerický	k2eAgFnSc2d1	severoamerická
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Amerika	Amerika	k1gFnSc1	Amerika
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
též	též	k9	též
nekopírovala	kopírovat	k5eNaImAgFnS	kopírovat
evropské	evropský	k2eAgInPc4d1	evropský
sekularizační	sekularizační	k2eAgInPc4d1	sekularizační
trendy	trend	k1gInPc4	trend
a	a	k8xC	a
modernismus	modernismus	k1gInSc1	modernismus
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
prosazován	prosazovat	k5eAaImNgInS	prosazovat
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
součinnosti	součinnost	k1gFnSc6	součinnost
s	s	k7c7	s
vírou	víra	k1gFnSc7	víra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
specifický	specifický	k2eAgInSc4d1	specifický
<g/>
,	,	kIx,	,
naléhavý	naléhavý	k2eAgInSc4d1	naléhavý
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgInSc4d1	osobní
charakter	charakter	k1gInSc4	charakter
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
První	první	k4xOgNnSc1	první
velké	velký	k2eAgNnSc1d1	velké
probuzení	probuzení	k1gNnSc1	probuzení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
mezi	mezi	k7c7	mezi
protestanty	protestant	k1gMnPc7	protestant
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nakonec	nakonec	k6eAd1	nakonec
mezi	mezi	k7c7	mezi
osadníky	osadník	k1gMnPc7	osadník
převážili	převážit	k5eAaPmAgMnP	převážit
<g/>
.	.	kIx.	.
</s>
<s>
Specifický	specifický	k2eAgInSc1d1	specifický
americký	americký	k2eAgInSc1d1	americký
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
náboženskému	náboženský	k2eAgInSc3d1	náboženský
prožitku	prožitek	k1gInSc3	prožitek
se	se	k3xPyFc4	se
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
politice	politika	k1gFnSc6	politika
USA	USA	kA	USA
projevuje	projevovat	k5eAaImIp3nS	projevovat
dodnes	dodnes	k6eAd1	dodnes
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
americký	americký	k2eAgInSc1d1	americký
evangelikalismus	evangelikalismus	k1gInSc1	evangelikalismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
rysem	rys	k1gInSc7	rys
prvních	první	k4xOgFnPc2	první
osadnických	osadnický	k2eAgFnPc2d1	osadnická
kultur	kultura	k1gFnPc2	kultura
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
samosprávu	samospráva	k1gFnSc4	samospráva
a	a	k8xC	a
konstitucionalismus	konstitucionalismus	k1gInSc4	konstitucionalismus
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
práva	právo	k1gNnPc1	právo
zaručená	zaručený	k2eAgNnPc1d1	zaručené
základními	základní	k2eAgInPc7d1	základní
zákony	zákon	k1gInPc7	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1770	[number]	k4	1770
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
populace	populace	k1gFnSc1	populace
ve	v	k7c6	v
třinácti	třináct	k4xCc6	třináct
britských	britský	k2eAgFnPc6d1	britská
koloniích	kolonie	k1gFnPc6	kolonie
2,1	[number]	k4	2,1
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představovalo	představovat	k5eAaImAgNnS	představovat
třetinu	třetina	k1gFnSc4	třetina
populace	populace	k1gFnSc2	populace
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgInSc1d1	tradiční
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
samosprávnost	samosprávnost	k1gFnSc4	samosprávnost
<g/>
,	,	kIx,	,
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
síla	síla	k1gFnSc1	síla
i	i	k8xC	i
populace	populace	k1gFnSc1	populace
a	a	k8xC	a
koroze	koroze	k1gFnSc1	koroze
idejí	idea	k1gFnPc2	idea
monarchismu	monarchismus	k1gInSc2	monarchismus
u	u	k7c2	u
klíčových	klíčový	k2eAgMnPc2d1	klíčový
myslitelů	myslitel	k1gMnPc2	myslitel
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Adams	Adamsa	k1gFnPc2	Adamsa
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Paine	Pain	k1gInSc5	Pain
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
<g/>
)	)	kIx)	)
předurčily	předurčit	k5eAaPmAgFnP	předurčit
střet	střet	k1gInSc4	střet
mezi	mezi	k7c7	mezi
kolonií	kolonie	k1gFnSc7	kolonie
a	a	k8xC	a
kolonizačním	kolonizační	k2eAgNnSc7d1	kolonizační
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vznik	vznik	k1gInSc1	vznik
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgFnPc2d1	americká
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
expanze	expanze	k1gFnSc1	expanze
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Bezohledné	bezohledný	k2eAgInPc1d1	bezohledný
zásahy	zásah	k1gInPc1	zásah
mateřské	mateřský	k2eAgFnSc2d1	mateřská
země	zem	k1gFnSc2	zem
do	do	k7c2	do
poměrů	poměr	k1gInPc2	poměr
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
protibritskou	protibritský	k2eAgFnSc4d1	protibritská
opozici	opozice	k1gFnSc4	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Pověstným	pověstný	k2eAgInSc7d1	pověstný
incidentem	incident	k1gInSc7	incident
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
zvláště	zvláště	k9	zvláště
tzv.	tzv.	kA	tzv.
Bostonské	bostonský	k2eAgNnSc1d1	Bostonské
pití	pití	k1gNnSc1	pití
čaje	čaj	k1gInSc2	čaj
roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
<g/>
.	.	kIx.	.
</s>
<s>
Napětí	napětí	k1gNnSc1	napětí
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1775	[number]	k4	1775
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
otevřené	otevřený	k2eAgFnSc2d1	otevřená
války	válka	k1gFnSc2	válka
mezi	mezi	k7c7	mezi
koloniemi	kolonie	k1gFnPc7	kolonie
a	a	k8xC	a
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
.	.	kIx.	.
4.	[number]	k4	4.
července	červenec	k1gInSc2	červenec
1776	[number]	k4	1776
vydal	vydat	k5eAaPmAgInS	vydat
druhý	druhý	k4xOgInSc4	druhý
Kontinentální	kontinentální	k2eAgInSc4d1	kontinentální
kongres	kongres	k1gInSc4	kongres
Deklaraci	deklarace	k1gFnSc4	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyhlašovala	vyhlašovat	k5eAaImAgFnS	vyhlašovat
vznik	vznik	k1gInSc4	vznik
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Článků	článek	k1gInPc2	článek
Konfederace	konfederace	k1gFnSc2	konfederace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1781	[number]	k4	1781
si	se	k3xPyFc3	se
každý	každý	k3xTgInSc1	každý
ze	z	k7c2	z
států	stát	k1gInPc2	stát
Unie	unie	k1gFnSc2	unie
zachoval	zachovat	k5eAaPmAgInS	zachovat
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
a	a	k8xC	a
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
skončila	skončit	k5eAaPmAgFnS	skončit
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
britským	britský	k2eAgNnSc7d1	Britské
uznáním	uznání	k1gNnSc7	uznání
nového	nový	k2eAgInSc2d1	nový
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1787	[number]	k4	1787
byl	být	k5eAaImAgInS	být
konfederativní	konfederativní	k2eAgInSc1d1	konfederativní
charakter	charakter	k1gInSc1	charakter
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
nahrazen	nahradit	k5eAaPmNgInS	nahradit
systémem	systém	k1gInSc7	systém
federativním	federativní	k2eAgInSc7d1	federativní
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
tzv.	tzv.	kA	tzv.
Bill	Bill	k1gMnSc1	Bill
of	of	k?	of
Rights	Rights	k1gInSc1	Rights
–	–	k?	–
listina	listina	k1gFnSc1	listina
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
prvních	první	k4xOgInPc2	první
10	[number]	k4	10
dodatků	dodatek	k1gInPc2	dodatek
ústavy	ústava	k1gFnSc2	ústava
-	-	kIx~	-
jehož	jehož	k3xOyRp3gFnSc1	jehož
ratifikace	ratifikace	k1gFnSc1	ratifikace
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1791	[number]	k4	1791
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
zvolen	zvolit	k5eAaPmNgInS	zvolit
George	George	k1gInSc1	George
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dovedl	dovést	k5eAaPmAgInS	dovést
povstaleckou	povstalecký	k2eAgFnSc4d1	povstalecká
armádu	armáda	k1gFnSc4	armáda
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
nad	nad	k7c4	nad
Brity	Brit	k1gMnPc4	Brit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
18.	[number]	k4	18.
století	století	k1gNnSc2	století
pak	pak	k6eAd1	pak
začala	začít	k5eAaPmAgFnS	začít
územní	územní	k2eAgFnSc1d1	územní
expanze	expanze	k1gFnSc1	expanze
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
a	a	k8xC	a
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
Unie	unie	k1gFnSc2	unie
přijaty	přijmout	k5eAaPmNgInP	přijmout
další	další	k2eAgInPc1d1	další
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
Vermont	Vermont	k1gInSc1	Vermont
(	(	kIx(	(
<g/>
1791	[number]	k4	1791
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kentucky	Kentuck	k1gInPc1	Kentuck
(	(	kIx(	(
<g/>
1792	[number]	k4	1792
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tennessee	Tennessee	k1gFnSc1	Tennessee
(	(	kIx(	(
<g/>
1796	[number]	k4	1796
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ohio	Ohio	k1gNnSc1	Ohio
(	(	kIx(	(
<g/>
1803	[number]	k4	1803
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1803	[number]	k4	1803
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
Francie	Francie	k1gFnSc2	Francie
odkoupena	odkoupen	k2eAgMnSc4d1	odkoupen
Louisiana	Louisian	k1gMnSc4	Louisian
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Koupě	koupě	k1gFnSc1	koupě
Louisiany	Louisian	k1gInPc1	Louisian
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
do	do	k7c2	do
Unie	unie	k1gFnSc2	unie
jako	jako	k8xC	jako
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
stát	stát	k1gInSc1	stát
roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
<g/>
.	.	kIx.	.
</s>
<s>
Pokračující	pokračující	k2eAgInPc1d1	pokračující
spory	spor	k1gInPc1	spor
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
námořního	námořní	k2eAgInSc2d1	námořní
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
rozdělení	rozdělení	k1gNnSc2	rozdělení
sfér	sféra	k1gFnPc2	sféra
vlivu	vliv	k1gInSc2	vliv
na	na	k7c6	na
severoamerickém	severoamerický	k2eAgInSc6d1	severoamerický
kontinentu	kontinent	k1gInSc6	kontinent
mezi	mezi	k7c7	mezi
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
britsko-americké	britskomerický	k2eAgFnSc3d1	britsko-americká
válce	válka	k1gFnSc3	válka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
nazývána	nazývat	k5eAaImNgFnS	nazývat
druhou	druhý	k4xOgFnSc7	druhý
válkou	válka	k1gFnSc7	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
trvala	trvat	k5eAaImAgFnS	trvat
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1812	[number]	k4	1812
a	a	k8xC	a
1814	[number]	k4	1814
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
válku	válka	k1gFnSc4	válka
vyhlásily	vyhlásit	k5eAaPmAgInP	vyhlásit
v	v	k7c6	v
přesvědčení	přesvědčení	k1gNnSc6	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
moci	moct	k5eAaImF	moct
využít	využít	k5eAaPmF	využít
zaneprázdněnosti	zaneprázdněnost	k1gFnPc4	zaneprázdněnost
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zaměstnávala	zaměstnávat	k5eAaImAgFnS	zaměstnávat
válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Napoleonem	napoleon	k1gInSc7	napoleon
<g/>
,	,	kIx,	,
a	a	k8xC	a
zabrat	zabrat	k5eAaPmF	zabrat
zbytek	zbytek	k1gInSc4	zbytek
britského	britský	k2eAgNnSc2d1	Britské
panství	panství	k1gNnSc2	panství
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Neuspěly	uspět	k5eNaPmAgInP	uspět
však	však	k9	však
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gInSc1	jejich
vpád	vpád	k1gInSc1	vpád
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
skončil	skončit	k5eAaPmAgMnS	skončit
debaklem	debakl	k1gInSc7	debakl
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
postupně	postupně	k6eAd1	postupně
začalo	začít	k5eAaPmAgNnS	začít
na	na	k7c4	na
americký	americký	k2eAgInSc4d1	americký
kontinent	kontinent	k1gInSc4	kontinent
přesouvat	přesouvat	k5eAaImF	přesouvat
další	další	k2eAgFnPc4d1	další
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
ovšem	ovšem	k9	ovšem
o	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
válku	válka	k1gFnSc4	válka
nestáli	stát	k5eNaImAgMnP	stát
a	a	k8xC	a
nehodlali	hodlat	k5eNaImAgMnP	hodlat
investovat	investovat	k5eAaBmF	investovat
prostředky	prostředek	k1gInPc4	prostředek
do	do	k7c2	do
tak	tak	k6eAd1	tak
nejistého	jistý	k2eNgInSc2d1	nejistý
podniku	podnik	k1gInSc2	podnik
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
znovudobytí	znovudobytí	k1gNnSc4	znovudobytí
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
dohodly	dohodnout	k5eAaPmAgInP	dohodnout
na	na	k7c6	na
návratu	návrat	k1gInSc6	návrat
ke	k	k7c3	k
statu	status	k1gInSc3	status
quo	quo	k?	quo
ante	ante	k6eAd1	ante
doplněném	doplněný	k2eAgInSc6d1	doplněný
dohodami	dohoda	k1gFnPc7	dohoda
řešícími	řešící	k2eAgFnPc7d1	řešící
největší	veliký	k2eAgFnSc4d3	veliký
kontroverze	kontroverze	k1gFnSc1	kontroverze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obnovení	obnovení	k1gNnSc1	obnovení
předválečného	předválečný	k2eAgInSc2d1	předválečný
stavu	stav	k1gInSc2	stav
fakticky	fakticky	k6eAd1	fakticky
posílilo	posílit	k5eAaPmAgNnS	posílit
postaveni	postavit	k5eAaPmNgMnP	postavit
USA	USA	kA	USA
a	a	k8xC	a
nakrátko	nakrátko	k6eAd1	nakrátko
zapříčinilo	zapříčinit	k5eAaPmAgNnS	zapříčinit
faktickou	faktický	k2eAgFnSc4d1	faktická
vládu	vláda	k1gFnSc4	vláda
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xS	jako
demokraté-republikáni	demokratéepublikán	k1gMnPc1	demokraté-republikán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
nazýváno	nazýván	k2eAgNnSc1d1	nazýváno
era	era	k?	era
of	of	k?	of
good	good	k6eAd1	good
feelings	feelings	k6eAd1	feelings
neboli	neboli	k8xC	neboli
érou	éra	k1gFnSc7	éra
dobré	dobrý	k2eAgFnSc2d1	dobrá
shody	shoda	k1gFnSc2	shoda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
teritorií	teritorium	k1gNnPc2	teritorium
byly	být	k5eAaImAgInP	být
postupně	postupně	k6eAd1	postupně
vytvářeny	vytvářet	k5eAaImNgInP	vytvářet
další	další	k2eAgInPc1d1	další
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
Indiana	Indiana	k1gFnSc1	Indiana
(	(	kIx(	(
<g/>
1816	[number]	k4	1816
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mississippi	Mississippi	k1gFnSc1	Mississippi
(	(	kIx(	(
<g/>
1817	[number]	k4	1817
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Illinois	Illinois	k1gFnSc1	Illinois
(	(	kIx(	(
<g/>
1818	[number]	k4	1818
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alabama	Alabama	k1gFnSc1	Alabama
(	(	kIx(	(
<g/>
1819	[number]	k4	1819
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Maine	Main	k1gMnSc5	Main
(	(	kIx(	(
<g/>
1820	[number]	k4	1820
<g/>
)	)	kIx)	)
a	a	k8xC	a
Missouri	Missouri	k1gFnSc1	Missouri
(	(	kIx(	(
<g/>
1821	[number]	k4	1821
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1819	[number]	k4	1819
získaly	získat	k5eAaPmAgFnP	získat
USA	USA	kA	USA
od	od	k7c2	od
Španělska	Španělsko	k1gNnSc2	Španělsko
Floridu	Florida	k1gFnSc4	Florida
(	(	kIx(	(
<g/>
stát	stát	k1gInSc1	stát
od	od	k7c2	od
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1845	[number]	k4	1845
byl	být	k5eAaImAgInS	být
anektován	anektován	k2eAgInSc1d1	anektován
Texas	Texas	k1gInSc1	Texas
a	a	k8xC	a
po	po	k7c6	po
americko-mexické	americkoexický	k2eAgFnSc6d1	americko-mexický
válce	válka	k1gFnSc6	válka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1846	[number]	k4	1846
<g/>
–	–	k?	–
<g/>
1848	[number]	k4	1848
Alta	Altum	k1gNnSc2	Altum
California	Californium	k1gNnSc2	Californium
a	a	k8xC	a
Santa	Santo	k1gNnSc2	Santo
Fe	Fe	k1gFnPc2	Fe
de	de	k?	de
Nuevo	Nuevo	k1gNnSc1	Nuevo
México	México	k1gMnSc1	México
(	(	kIx(	(
<g/>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
jako	jako	k8xC	jako
stát	stát	k1gInSc1	stát
od	od	k7c2	od
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
60.	[number]	k4	60.
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
Unie	unie	k1gFnSc2	unie
přijaty	přijmout	k5eAaPmNgInP	přijmout
další	další	k2eAgInPc1d1	další
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
Arkansas	Arkansas	k1gInSc1	Arkansas
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Michigan	Michigan	k1gInSc1	Michigan
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Iowa	Iowa	k1gMnSc1	Iowa
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Wisconsin	Wisconsin	k1gMnSc1	Wisconsin
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Minnesota	Minnesota	k1gFnSc1	Minnesota
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
<g/>
)	)	kIx)	)
a	a	k8xC	a
Oregon	Oregon	k1gMnSc1	Oregon
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Americká	americký	k2eAgFnSc1d1	americká
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
19.	[number]	k4	19.
století	století	k1gNnSc6	století
začaly	začít	k5eAaPmAgInP	začít
narůstat	narůstat	k5eAaImF	narůstat
rozpory	rozpor	k1gInPc4	rozpor
mezi	mezi	k7c7	mezi
americkým	americký	k2eAgInSc7d1	americký
severem	sever	k1gInSc7	sever
a	a	k8xC	a
jihem	jih	k1gInSc7	jih
<g/>
.	.	kIx.	.
</s>
<s>
Sever	sever	k1gInSc1	sever
byl	být	k5eAaImAgInS	být
lidnatý	lidnatý	k2eAgMnSc1d1	lidnatý
(	(	kIx(	(
<g/>
19	[number]	k4	19
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
industriální	industriální	k2eAgMnSc1d1	industriální
a	a	k8xC	a
odmítal	odmítat	k5eAaImAgMnS	odmítat
otroctví	otroctví	k1gNnSc4	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Jih	jih	k1gInSc1	jih
byl	být	k5eAaImAgInS	být
méně	málo	k6eAd2	málo
osídlený	osídlený	k2eAgMnSc1d1	osídlený
(	(	kIx(	(
<g/>
8	[number]	k4	8
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
polovina	polovina	k1gFnSc1	polovina
byli	být	k5eAaImAgMnP	být
černí	černit	k5eAaImIp3nP	černit
otroci	otrok	k1gMnPc1	otrok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
(	(	kIx(	(
<g/>
zejm	zejm	k?	zejm
<g/>
.	.	kIx.	.
plantáže	plantáž	k1gFnSc2	plantáž
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
)	)	kIx)	)
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
z	z	k7c2	z
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
založené	založený	k2eAgNnSc1d1	založené
na	na	k7c6	na
protestantské	protestantský	k2eAgFnSc6d1	protestantská
teologii	teologie	k1gFnSc6	teologie
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
otroctví	otroctví	k1gNnSc1	otroctví
odmítalo	odmítat	k5eAaImAgNnS	odmítat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývalo	nazývat	k5eAaImAgNnS	nazývat
abolicionismus	abolicionismus	k1gInSc4	abolicionismus
<g/>
.	.	kIx.	.
</s>
<s>
Pozvolna	pozvolna	k6eAd1	pozvolna
se	se	k3xPyFc4	se
na	na	k7c6	na
severu	sever	k1gInSc6	sever
otázka	otázka	k1gFnSc1	otázka
otroctví	otroctví	k1gNnPc4	otroctví
ovšem	ovšem	k9	ovšem
začala	začít	k5eAaPmAgFnS	začít
politizovat	politizovat	k5eAaImF	politizovat
<g/>
.	.	kIx.	.
</s>
<s>
Odpůrci	odpůrce	k1gMnPc1	odpůrce
otroctví	otroctví	k1gNnSc2	otroctví
založili	založit	k5eAaPmAgMnP	založit
i	i	k9	i
novou	nový	k2eAgFnSc4d1	nová
Republikánskou	republikánský	k2eAgFnSc4d1	republikánská
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
ústřední	ústřední	k2eAgFnSc7d1	ústřední
osobností	osobnost	k1gFnSc7	osobnost
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
zvolen	zvolit	k5eAaPmNgInS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
secesi	secese	k1gFnSc3	secese
(	(	kIx(	(
<g/>
odtržení	odtržení	k1gNnSc1	odtržení
<g/>
)	)	kIx)	)
jedenácti	jedenáct	k4xCc2	jedenáct
jižních	jižní	k2eAgInPc2d1	jižní
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgFnSc1d1	jižní
Karolíny	Karolína	k1gFnPc1	Karolína
<g/>
,	,	kIx,	,
Mississippi	Mississippi	k1gFnPc1	Mississippi
<g/>
,	,	kIx,	,
Floridy	Florida	k1gFnPc1	Florida
<g/>
,	,	kIx,	,
Alabamy	Alabam	k1gInPc1	Alabam
<g/>
,	,	kIx,	,
Georgie	Georgie	k1gFnPc1	Georgie
<g/>
,	,	kIx,	,
Louisiany	Louisiana	k1gFnPc1	Louisiana
<g/>
,	,	kIx,	,
Texasu	Texas	k1gInSc6	Texas
<g/>
,	,	kIx,	,
Virginie	Virginie	k1gFnSc5	Virginie
<g/>
,	,	kIx,	,
Arkansasu	Arkansas	k1gInSc2	Arkansas
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc2d1	severní
Karolíny	Karolína	k1gFnSc2	Karolína
a	a	k8xC	a
Tennessee	Tennesse	k1gFnSc2	Tennesse
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
vyhlásily	vyhlásit	k5eAaPmAgInP	vyhlásit
Konfederované	konfederovaný	k2eAgInPc1d1	konfederovaný
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nakonec	nakonec	k6eAd1	nakonec
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
občanské	občanský	k2eAgFnSc3d1	občanská
válce	válka	k1gFnSc3	válka
(	(	kIx(	(
<g/>
známé	známá	k1gFnSc2	známá
též	též	k9	též
jako	jako	k8xS	jako
válka	válka	k1gFnSc1	válka
Severu	sever	k1gInSc2	sever
proti	proti	k7c3	proti
Jihu	jih	k1gInSc3	jih
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
začala	začít	k5eAaPmAgFnS	začít
12.	[number]	k4	12.
dubna	duben	k1gInSc2	duben
1861	[number]	k4	1861
útokem	útok	k1gInSc7	útok
konfederačních	konfederační	k2eAgFnPc2d1	konfederační
jednotek	jednotka	k1gFnPc2	jednotka
na	na	k7c4	na
americkou	americký	k2eAgFnSc4d1	americká
pevnost	pevnost	k1gFnSc4	pevnost
Fort	Fort	k?	Fort
Sumter	Sumtra	k1gFnPc2	Sumtra
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Karolíně	Karolína	k1gFnSc6	Karolína
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
jižní	jižní	k2eAgFnPc1d1	jižní
jednotky	jednotka	k1gFnPc1	jednotka
solidních	solidní	k2eAgInPc2d1	solidní
úspěchů	úspěch	k1gInPc2	úspěch
díky	díky	k7c3	díky
zkušenosti	zkušenost	k1gFnSc3	zkušenost
svého	svůj	k3xOyFgMnSc2	svůj
velitele	velitel	k1gMnSc2	velitel
Roberta	Robert	k1gMnSc2	Robert
Edwarda	Edward	k1gMnSc2	Edward
Leea	Leeus	k1gMnSc2	Leeus
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gNnSc2	on
vojska	vojsko	k1gNnSc2	vojsko
však	však	k9	však
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
poražena	poražen	k2eAgFnSc1d1	poražena
v	v	k7c6	v
klíčové	klíčový	k2eAgFnSc6d1	klíčová
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Gettysburgu	Gettysburg	k1gInSc2	Gettysburg
<g/>
.	.	kIx.	.
</s>
<s>
Armádu	armáda	k1gFnSc4	armáda
Unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
Severu	sever	k1gInSc2	sever
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
přivedl	přivést	k5eAaPmAgMnS	přivést
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
Ulysses	Ulyssesa	k1gFnPc2	Ulyssesa
S.	S.	kA	S.
Grant	grant	k1gInSc1	grant
(	(	kIx(	(
<g/>
budoucí	budoucí	k2eAgMnSc1d1	budoucí
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1869	[number]	k4	1869
<g/>
–	–	k?	–
<g/>
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
trvala	trvat	k5eAaImAgFnS	trvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
Severu	sever	k1gInSc2	sever
musely	muset	k5eAaImAgInP	muset
všechny	všechen	k3xTgInPc1	všechen
jižní	jižní	k2eAgInPc1d1	jižní
státy	stát	k1gInPc1	stát
ratifikovat	ratifikovat	k5eAaBmF	ratifikovat
třináctý	třináctý	k4xOgInSc1	třináctý
dodatek	dodatek	k1gInSc1	dodatek
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
otroctví	otroctví	k1gNnSc4	otroctví
zakazoval	zakazovat	k5eAaImAgMnS	zakazovat
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc4	vítězství
to	ten	k3xDgNnSc1	ten
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
zdaleka	zdaleka	k6eAd1	zdaleka
definitivní	definitivní	k2eAgNnSc1d1	definitivní
<g/>
.	.	kIx.	.
</s>
<s>
Nejenže	nejenže	k6eAd1	nejenže
byl	být	k5eAaImAgMnS	být
Lincoln	Lincoln	k1gMnSc1	Lincoln
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
se	se	k3xPyFc4	se
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
dostala	dostat	k5eAaPmAgFnS	dostat
frakce	frakce	k1gFnSc1	frakce
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Redeemers	Redeemers	k1gInSc1	Redeemers
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
hlásala	hlásat	k5eAaImAgFnS	hlásat
nadřazenost	nadřazenost	k1gFnSc4	nadřazenost
bělochů	běloch	k1gMnPc2	běloch
a	a	k8xC	a
zavedla	zavést	k5eAaPmAgFnS	zavést
rasovou	rasový	k2eAgFnSc4d1	rasová
segregaci	segregace	k1gFnSc4	segregace
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Zákony	zákon	k1gInPc1	zákon
Jima	Jimus	k1gMnSc2	Jimus
Crowa	Crowus	k1gMnSc2	Crowus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
O	o	k7c6	o
odstranění	odstranění	k1gNnSc6	odstranění
jejích	její	k3xOp3gInPc2	její
posledních	poslední	k2eAgInPc2d1	poslední
zbytků	zbytek	k1gInPc2	zbytek
se	se	k3xPyFc4	se
vedl	vést	k5eAaImAgInS	vést
tuhý	tuhý	k2eAgInSc1d1	tuhý
politický	politický	k2eAgInSc1d1	politický
boj	boj	k1gInSc1	boj
až	až	k9	až
do	do	k7c2	do
60.	[number]	k4	60.
let	léto	k1gNnPc2	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
nicméně	nicméně	k8xC	nicméně
znatelně	znatelně	k6eAd1	znatelně
posílila	posílit	k5eAaPmAgFnS	posílit
federální	federální	k2eAgFnSc1d1	federální
moc	moc	k1gFnSc1	moc
a	a	k8xC	a
umožnila	umožnit	k5eAaPmAgFnS	umožnit
další	další	k2eAgFnSc4d1	další
expanzi	expanze	k1gFnSc4	expanze
<g/>
..	..	k?	..
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Posilování	posilování	k1gNnSc1	posilování
velmocenského	velmocenský	k2eAgNnSc2d1	velmocenské
postavení	postavení	k1gNnSc2	postavení
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Mohutný	mohutný	k2eAgInSc1d1	mohutný
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
rozvoj	rozvoj	k1gInSc1	rozvoj
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
způsobil	způsobit	k5eAaPmAgMnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
USA	USA	kA	USA
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
staly	stát	k5eAaPmAgFnP	stát
hospodářsky	hospodářsky	k6eAd1	hospodářsky
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
zemí	zem	k1gFnSc7	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
rozmachem	rozmach	k1gInSc7	rozmach
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19.	[number]	k4	19.
a	a	k8xC	a
začátku	začátek	k1gInSc2	začátek
20.	[number]	k4	20.
století	století	k1gNnSc2	století
stáli	stát	k5eAaImAgMnP	stát
průmyslníci	průmyslník	k1gMnPc1	průmyslník
jako	jako	k8xS	jako
Cornelius	Cornelius	k1gMnSc1	Cornelius
Vanderbilt	Vanderbilt	k1gMnSc1	Vanderbilt
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
D.	D.	kA	D.
Rockefeller	Rockefeller	k1gMnSc1	Rockefeller
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
Ford	ford	k1gInSc1	ford
a	a	k8xC	a
Andrew	Andrew	k1gFnSc1	Andrew
Carnegie	Carnegie	k1gFnSc1	Carnegie
<g/>
,	,	kIx,	,
či	či	k8xC	či
bankéř	bankéř	k1gMnSc1	bankéř
John	John	k1gMnSc1	John
Pierpont	Pierpont	k1gMnSc1	Pierpont
Morgan	morgan	k1gMnSc1	morgan
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
boom	boom	k1gInSc1	boom
byl	být	k5eAaImAgInS	být
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
další	další	k2eAgFnSc7d1	další
expanzí	expanze	k1gFnSc7	expanze
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
poté	poté	k6eAd1	poté
až	až	k9	až
do	do	k7c2	do
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zde	zde	k6eAd1	zde
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
15	[number]	k4	15
dalších	další	k2eAgInPc2d1	další
unijních	unijní	k2eAgInPc2d1	unijní
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
Kansas	Kansas	k1gInSc1	Kansas
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Západní	západní	k2eAgFnPc1d1	západní
Virginie	Virginie	k1gFnPc1	Virginie
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nevada	Nevada	k1gFnSc1	Nevada
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nebraska	Nebraska	k1gFnSc1	Nebraska
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Colorado	Colorado	k1gNnSc1	Colorado
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Dakota	Dakota	k1gFnSc1	Dakota
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Dakota	Dakota	k1gFnSc1	Dakota
<g/>
,	,	kIx,	,
Montana	Montana	k1gFnSc1	Montana
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Idaho	Ida	k1gMnSc4	Ida
<g/>
,	,	kIx,	,
Wyoming	Wyoming	k1gInSc1	Wyoming
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Utah	Utah	k1gInSc1	Utah
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Oklahoma	Oklahoma	k1gFnSc1	Oklahoma
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Arizona	Arizona	k1gFnSc1	Arizona
a	a	k8xC	a
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
americká	americký	k2eAgFnSc1d1	americká
vláda	vláda	k1gFnSc1	vláda
od	od	k7c2	od
Ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
území	území	k1gNnSc4	území
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
80.	[number]	k4	80.
let	léto	k1gNnPc2	léto
19.	[number]	k4	19.
století	století	k1gNnSc2	století
pak	pak	k6eAd1	pak
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
expandovaly	expandovat	k5eAaImAgInP	expandovat
i	i	k9	i
mimo	mimo	k7c4	mimo
vlastní	vlastní	k2eAgFnSc4d1	vlastní
americkou	americký	k2eAgFnSc4d1	americká
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
do	do	k7c2	do
Karibiku	Karibik	k1gInSc2	Karibik
a	a	k8xC	a
do	do	k7c2	do
Tichomoří	Tichomoří	k1gNnSc2	Tichomoří
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
ve	v	k7c6	v
Španělsko-americké	španělskomerický	k2eAgFnSc6d1	španělsko-americká
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
intervence	intervence	k1gFnSc2	intervence
měly	mít	k5eAaImAgFnP	mít
několik	několik	k4yIc4	několik
důsledků	důsledek	k1gInPc2	důsledek
<g/>
;	;	kIx,	;
protektorát	protektorát	k1gInSc1	protektorát
nad	nad	k7c7	nad
Portorikem	Portorico	k1gNnSc7	Portorico
a	a	k8xC	a
Kubou	Kuba	k1gFnSc7	Kuba
<g/>
,	,	kIx,	,
ostrov	ostrov	k1gInSc1	ostrov
Guam	Guama	k1gFnPc2	Guama
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
,	,	kIx,	,
anexe	anexe	k1gFnPc4	anexe
Havajských	havajský	k2eAgInPc2d1	havajský
ostrovů	ostrov	k1gInPc2	ostrov
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
rozdělení	rozdělení	k1gNnSc1	rozdělení
ostrovů	ostrov	k1gInPc2	ostrov
Samoa	Samoa	k1gFnSc1	Samoa
s	s	k7c7	s
Německou	německý	k2eAgFnSc7d1	německá
říší	říš	k1gFnSc7	říš
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
a	a	k8xC	a
Průplavové	průplavový	k2eAgNnSc4d1	průplavový
pásmo	pásmo	k1gNnSc4	pásmo
v	v	k7c6	v
Panamě	Panama	k1gFnSc6	Panama
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20.	[number]	k4	20.
století	století	k1gNnPc2	století
se	se	k3xPyFc4	se
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
staly	stát	k5eAaPmAgInP	stát
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
světových	světový	k2eAgFnPc2d1	světová
velmocí	velmoc	k1gFnPc2	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
svého	svůj	k3xOyFgInSc2	svůj
již	již	k6eAd1	již
velkého	velký	k2eAgInSc2d1	velký
vlivu	vliv	k1gInSc2	vliv
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
kontinentech	kontinent	k1gInPc6	kontinent
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
USA	USA	kA	USA
ve	v	k7c6	v
světové	světový	k2eAgFnSc6d1	světová
politice	politika	k1gFnSc6	politika
až	až	k9	až
do	do	k7c2	do
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
angažovaly	angažovat	k5eAaBmAgFnP	angažovat
jen	jen	k9	jen
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
politika	politika	k1gFnSc1	politika
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
izolacionistická	izolacionistický	k2eAgFnSc1d1	izolacionistická
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Monroeova	Monroeův	k2eAgFnSc1d1	Monroeova
doktrína	doktrína	k1gFnSc1	doktrína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnSc4d1	zásadní
roli	role	k1gFnSc4	role
při	při	k7c6	při
překonávání	překonávání	k1gNnSc6	překonávání
amerického	americký	k2eAgInSc2d1	americký
izolacionismu	izolacionismus	k1gInSc2	izolacionismus
sehrál	sehrát	k5eAaPmAgMnS	sehrát
prezident	prezident	k1gMnSc1	prezident
Woodrow	Woodrow	k1gMnSc1	Woodrow
Wilson	Wilson	k1gMnSc1	Wilson
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
němu	on	k3xPp3gNnSc3	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
zapojili	zapojit	k5eAaPmAgMnP	zapojit
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Dohody	dohoda	k1gFnSc2	dohoda
do	do	k7c2	do
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Wilson	Wilson	k1gMnSc1	Wilson
svou	svůj	k3xOyFgFnSc7	svůj
koncepcí	koncepce	k1gFnSc7	koncepce
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
národů	národ	k1gInPc2	národ
též	též	k9	též
významně	významně	k6eAd1	významně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
podobu	podoba	k1gFnSc4	podoba
Evropy	Evropa	k1gFnSc2	Evropa
po	po	k7c6	po
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
přinesla	přinést	k5eAaPmAgFnS	přinést
další	další	k2eAgInSc4d1	další
mohutný	mohutný	k2eAgInSc4d1	mohutný
rozmach	rozmach	k1gInSc4	rozmach
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Americkým	americký	k2eAgNnSc7d1	americké
sebevědomím	sebevědomí	k1gNnSc7	sebevědomí
zacloumal	zacloumat	k5eAaPmAgInS	zacloumat
až	až	k6eAd1	až
krach	krach	k1gInSc1	krach
na	na	k7c6	na
newyorské	newyorský	k2eAgFnSc6d1	newyorská
burze	burza	k1gFnSc6	burza
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
způsobil	způsobit	k5eAaPmAgInS	způsobit
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
krizi	krize	k1gFnSc4	krize
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
katalyzovala	katalyzovat	k5eAaBmAgFnS	katalyzovat
zejména	zejména	k9	zejména
politický	politický	k2eAgInSc4d1	politický
vývoj	vývoj	k1gInSc4	vývoj
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
pomohla	pomoct	k5eAaPmAgFnS	pomoct
zvláště	zvláště	k6eAd1	zvláště
vzestupu	vzestup	k1gInSc2	vzestup
fašismu	fašismus	k1gInSc2	fašismus
a	a	k8xC	a
nacismu	nacismus	k1gInSc2	nacismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotných	samotný	k2eAgInPc6d1	samotný
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
opuštění	opuštění	k1gNnSc3	opuštění
čisté	čistá	k1gFnSc2	čistá
"	"	kIx"	"
<g/>
laissez	laissez	k1gInSc1	laissez
faire	fair	k1gInSc5	fair
<g/>
"	"	kIx"	"
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
větším	veliký	k2eAgInPc3d2	veliký
státním	státní	k2eAgInPc3d1	státní
zásahům	zásah	k1gInPc3	zásah
do	do	k7c2	do
ekonomiky	ekonomika	k1gFnSc2	ekonomika
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
plánu	plán	k1gInSc2	plán
zvaného	zvaný	k2eAgInSc2d1	zvaný
New	New	k1gFnSc7	New
Deal	Dealum	k1gNnPc2	Dealum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
připravil	připravit	k5eAaPmAgMnS	připravit
Demokrat	demokrat	k1gMnSc1	demokrat
Franklin	Franklina	k1gFnPc2	Franklina
Delano	Delana	k1gFnSc5	Delana
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
měl	mít	k5eAaImAgInS	mít
své	svůj	k3xOyFgFnPc4	svůj
kritiky	kritika	k1gFnPc4	kritika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obzvláště	obzvláště	k6eAd1	obzvláště
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
hodil	hodit	k5eAaPmAgInS	hodit
pro	pro	k7c4	pro
válečný	válečný	k2eAgInSc4d1	válečný
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
USA	USA	kA	USA
dostaly	dostat	k5eAaPmAgInP	dostat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Japonsko	Japonsko	k1gNnSc1	Japonsko
napadlo	napadnout	k5eAaPmAgNnS	napadnout
jejich	jejich	k3xOp3gFnSc4	jejich
základnu	základna	k1gFnSc4	základna
Pearl	Pearl	k1gMnSc1	Pearl
Harbor	Harbor	k1gMnSc1	Harbor
v	v	k7c6	v
Pacifiku	Pacifik	k1gInSc6	Pacifik
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
následně	následně	k6eAd1	následně
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
válku	válka	k1gFnSc4	válka
nejen	nejen	k6eAd1	nejen
Japonsku	Japonsko	k1gNnSc3	Japonsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jeho	jeho	k3xOp3gMnPc1	jeho
spojenci	spojenec	k1gMnPc1	spojenec
<g/>
,	,	kIx,	,
nacistickému	nacistický	k2eAgNnSc3d1	nacistické
Německu	Německo	k1gNnSc3	Německo
<g/>
,	,	kIx,	,
a	a	k8xC	a
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
tak	tak	k9	tak
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
ovšem	ovšem	k9	ovšem
hospodářsky	hospodářsky	k6eAd1	hospodářsky
podporovaly	podporovat	k5eAaImAgInP	podporovat
Spojence	spojenka	k1gFnSc3	spojenka
podle	podle	k7c2	podle
Zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
půjčce	půjčka	k1gFnSc6	půjčka
a	a	k8xC	a
pronájmu	pronájem	k1gInSc6	pronájem
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgInP	podílet
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
poražení	poražení	k1gNnSc3	poražení
Německa	Německo	k1gNnSc2	Německo
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentě	kontinent	k1gInSc6	kontinent
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Den	den	k1gInSc1	den
D	D	kA	D
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Japonce	Japonka	k1gFnSc3	Japonka
pak	pak	k6eAd1	pak
porazily	porazit	k5eAaPmAgFnP	porazit
i	i	k9	i
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgFnSc2d1	nová
zbraně	zbraň	k1gFnSc2	zbraň
–	–	k?	–
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
–	–	k?	–
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
svrhly	svrhnout	k5eAaPmAgFnP	svrhnout
na	na	k7c4	na
japonská	japonský	k2eAgNnPc4d1	Japonské
města	město	k1gNnPc4	město
Hirošima	Hirošima	k1gFnSc1	Hirošima
a	a	k8xC	a
Nagasaki	Nagasaki	k1gNnSc1	Nagasaki
<g/>
.	.	kIx.	.
</s>
<s>
Vítězné	vítězný	k2eAgFnPc1d1	vítězná
mocnosti	mocnost	k1gFnPc1	mocnost
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
dohodami	dohoda	k1gFnPc7	dohoda
ekonomickými	ekonomický	k2eAgFnPc7d1	ekonomická
(	(	kIx(	(
<g/>
konference	konference	k1gFnSc2	konference
v	v	k7c4	v
Bretton	Bretton	k1gInSc4	Bretton
Woods	Woods	k1gInSc4	Woods
<g/>
)	)	kIx)	)
i	i	k9	i
politickými	politický	k2eAgFnPc7d1	politická
(	(	kIx(	(
<g/>
Jaltská	jaltský	k2eAgFnSc1d1	Jaltská
konference	konference	k1gFnSc1	konference
<g/>
)	)	kIx)	)
narýsovaly	narýsovat	k5eAaPmAgFnP	narýsovat
podobu	podoba	k1gFnSc4	podoba
poválečného	poválečný	k2eAgInSc2d1	poválečný
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Za	za	k7c2	za
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přímo	přímo	k6eAd1	přímo
nezasáhla	zasáhnout	k5eNaPmAgFnS	zasáhnout
území	území	k1gNnSc2	území
kontinentálních	kontinentální	k2eAgInPc2d1	kontinentální
USA	USA	kA	USA
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
americká	americký	k2eAgFnSc1d1	americká
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Evropa	Evropa	k1gFnSc1	Evropa
byla	být	k5eAaImAgFnS	být
hospodářsky	hospodářsky	k6eAd1	hospodářsky
na	na	k7c6	na
kolenou	koleno	k1gNnPc6	koleno
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
klíčovou	klíčový	k2eAgFnSc7d1	klíčová
mocností	mocnost	k1gFnSc7	mocnost
Západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
pomohly	pomoct	k5eAaPmAgInP	pomoct
těžce	těžce	k6eAd1	těžce
postižené	postižený	k2eAgFnSc6d1	postižená
Západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
včetně	včetně	k7c2	včetně
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Marshallova	Marshallův	k2eAgInSc2d1	Marshallův
plánu	plán	k1gInSc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Západoevropské	západoevropský	k2eAgInPc1d1	západoevropský
státy	stát	k1gInPc1	stát
díky	díky	k7c3	díky
němu	on	k3xPp3gNnSc3	on
nejen	nejen	k6eAd1	nejen
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
překonaly	překonat	k5eAaPmAgFnP	překonat
nejhorší	zlý	k2eAgInPc4d3	Nejhorší
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navíc	navíc	k6eAd1	navíc
nastoupily	nastoupit	k5eAaPmAgFnP	nastoupit
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
výraznému	výrazný	k2eAgInSc3d1	výrazný
hospodářskému	hospodářský	k2eAgInSc3d1	hospodářský
růstu	růst	k1gInSc3	růst
a	a	k8xC	a
k	k	k7c3	k
doposud	doposud	k6eAd1	doposud
nebývalému	nebývalý	k2eAgInSc3d1	nebývalý
blahobytu	blahobyt	k1gInSc3	blahobyt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
dostaly	dostat	k5eAaPmAgFnP	dostat
USA	USA	kA	USA
též	též	k9	též
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
poručenská	poručenský	k2eAgNnPc1d1	poručenský
území	území	k1gNnPc1	území
OSN	OSN	kA	OSN
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceáně	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
předtím	předtím	k6eAd1	předtím
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
volně	volně	k6eAd1	volně
přidruženými	přidružený	k2eAgInPc7d1	přidružený
územími	území	k1gNnPc7	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
Marshallovy	Marshallův	k2eAgInPc1d1	Marshallův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnPc1d1	severní
Mariany	Mariana	k1gFnPc1	Mariana
<g/>
,	,	kIx,	,
Mikronésie	Mikronésie	k1gFnPc1	Mikronésie
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
byly	být	k5eAaImAgInP	být
vytvořeny	vytvořen	k2eAgInPc1d1	vytvořen
dva	dva	k4xCgInPc1	dva
–	–	k?	–
dosud	dosud	k6eAd1	dosud
poslední	poslední	k2eAgInPc1d1	poslední
–	–	k?	–
státy	stát	k1gInPc1	stát
Unie	unie	k1gFnSc1	unie
<g/>
:	:	kIx,	:
Aljaška	Aljaška	k1gFnSc1	Aljaška
a	a	k8xC	a
Havaj	Havaj	k1gFnSc1	Havaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
sfér	sféra	k1gFnPc2	sféra
vlivu	vliv	k1gInSc2	vliv
mezi	mezi	k7c7	mezi
vítěznými	vítězný	k2eAgFnPc7d1	vítězná
mocnostmi	mocnost	k1gFnPc7	mocnost
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
poklidně	poklidně	k6eAd1	poklidně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
klid	klid	k1gInSc4	klid
nevydržel	vydržet	k5eNaPmAgMnS	vydržet
dlouho	dlouho	k6eAd1	dlouho
–	–	k?	–
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
mezi	mezi	k7c7	mezi
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
(	(	kIx(	(
<g/>
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
západními	západní	k2eAgMnPc7d1	západní
spojenci	spojenec	k1gMnPc7	spojenec
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
pak	pak	k8xC	pak
říkalo	říkat	k5eAaImAgNnS	říkat
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
napětí	napětí	k1gNnSc1	napětí
se	se	k3xPyFc4	se
projevovalo	projevovat	k5eAaImAgNnS	projevovat
jak	jak	k6eAd1	jak
pozitivně	pozitivně	k6eAd1	pozitivně
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
snahou	snaha	k1gFnSc7	snaha
o	o	k7c4	o
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
předběhnutí	předběhnutí	k1gNnSc4	předběhnutí
soupeřícího	soupeřící	k2eAgInSc2d1	soupeřící
tábora	tábor	k1gInSc2	tábor
či	či	k8xC	či
proslulým	proslulý	k2eAgNnSc7d1	proslulé
"	"	kIx"	"
<g/>
vesmírným	vesmírný	k2eAgInSc7d1	vesmírný
závodem	závod	k1gInSc7	závod
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
Sověti	Sovět	k1gMnPc1	Sovět
vyslali	vyslat	k5eAaPmAgMnP	vyslat
prvního	první	k4xOgMnSc4	první
člověka	člověk	k1gMnSc4	člověk
do	do	k7c2	do
kosmu	kosmos	k1gInSc2	kosmos
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
a	a	k8xC	a
Američané	Američan	k1gMnPc1	Američan
přistáli	přistát	k5eAaImAgMnP	přistát
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
občas	občas	k6eAd1	občas
i	i	k9	i
různými	různý	k2eAgInPc7d1	různý
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
krvavými	krvavý	k2eAgFnPc7d1	krvavá
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
proxy	prox	k1gInPc4	prox
wars	wars	k1gInSc1	wars
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zástupnými	zástupný	k2eAgInPc7d1	zástupný
konflikty	konflikt	k1gInPc7	konflikt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
tak	tak	k6eAd1	tak
či	či	k8xC	či
onak	onak	k6eAd1	onak
přímo	přímo	k6eAd1	přímo
podílely	podílet	k5eAaImAgInP	podílet
–	–	k?	–
Korejská	korejský	k2eAgFnSc1d1	Korejská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
53	[number]	k4	53
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karibská	karibský	k2eAgFnSc1d1	karibská
krize	krize	k1gFnSc1	krize
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1959-1975	[number]	k4	1959-1975
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
vietnamský	vietnamský	k2eAgInSc1d1	vietnamský
konflikt	konflikt	k1gInSc1	konflikt
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
pnutí	pnutí	k1gNnSc3	pnutí
i	i	k9	i
uvnitř	uvnitř	k7c2	uvnitř
americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
odpor	odpor	k1gInSc1	odpor
proti	proti	k7c3	proti
této	tento	k3xDgFnSc3	tento
válce	válka	k1gFnSc3	válka
probíhal	probíhat	k5eAaImAgInS	probíhat
v	v	k7c6	v
součinnosti	součinnost	k1gFnSc6	součinnost
s	s	k7c7	s
hnutím	hnutí	k1gNnSc7	hnutí
za	za	k7c2	za
práva	právo	k1gNnSc2	právo
Afroameričanů	Afroameričan	k1gMnPc2	Afroameričan
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
vedl	vést	k5eAaImAgMnS	vést
především	především	k6eAd1	především
reverend	reverend	k1gMnSc1	reverend
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
King	King	k1gMnSc1	King
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
boj	boj	k1gInSc1	boj
<g/>
,	,	kIx,	,
podpořený	podpořený	k2eAgInSc1d1	podpořený
zejména	zejména	k9	zejména
demokratickým	demokratický	k2eAgMnSc7d1	demokratický
prezidentem	prezident	k1gMnSc7	prezident
Johnem	John	k1gMnSc7	John
Fitzgeraldem	Fitzgerald	k1gMnSc7	Fitzgerald
Kennedym	Kennedym	k1gInSc4	Kennedym
(	(	kIx(	(
<g/>
zavražděn	zavražděn	k2eAgInSc4d1	zavražděn
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
<g/>
:	:	kIx,	:
Zákony	zákon	k1gInPc1	zákon
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1964	[number]	k4	1964
(	(	kIx(	(
<g/>
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
a	a	k8xC	a
ubytování	ubytování	k1gNnSc2	ubytování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
(	(	kIx(	(
<g/>
volby	volba	k1gFnSc2	volba
<g/>
)	)	kIx)	)
a	a	k8xC	a
1968	[number]	k4	1968
(	(	kIx(	(
<g/>
trh	trh	k1gInSc1	trh
s	s	k7c7	s
bydlením	bydlení	k1gNnSc7	bydlení
<g/>
)	)	kIx)	)
zakázaly	zakázat	k5eAaPmAgInP	zakázat
diskriminaci	diskriminace	k1gFnSc3	diskriminace
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rasy	rasa	k1gFnSc2	rasa
nebo	nebo	k8xC	nebo
barvy	barva	k1gFnSc2	barva
pleti	pleť	k1gFnSc2	pleť
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
60.	[number]	k4	60.
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
kontrakultuře	kontrakultura	k1gFnSc6	kontrakultura
<g/>
)	)	kIx)	)
přinesla	přinést	k5eAaPmAgFnS	přinést
však	však	k9	však
i	i	k9	i
některé	některý	k3yIgInPc4	některý
sporné	sporný	k2eAgInPc4d1	sporný
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
adorace	adorace	k1gFnSc1	adorace
drog	droga	k1gFnPc2	droga
a	a	k8xC	a
promiskuity	promiskuita	k1gFnSc2	promiskuita
<g/>
,	,	kIx,	,
černošský	černošský	k2eAgInSc1d1	černošský
radikalismus	radikalismus	k1gInSc1	radikalismus
(	(	kIx(	(
<g/>
Černí	černý	k2eAgMnPc1d1	černý
panteři	panter	k1gMnPc1	panter
<g/>
,	,	kIx,	,
Malcolm	Malcolm	k1gMnSc1	Malcolm
X	X	kA	X
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
levicový	levicový	k2eAgInSc1d1	levicový
extremismus	extremismus	k1gInSc1	extremismus
(	(	kIx(	(
<g/>
obdiv	obdiv	k1gInSc1	obdiv
k	k	k7c3	k
násilnické	násilnický	k2eAgFnSc3d1	násilnická
čínské	čínský	k2eAgFnSc3d1	čínská
kulturní	kulturní	k2eAgFnSc3d1	kulturní
revoluci	revoluce	k1gFnSc3	revoluce
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c4	v
70.	[number]	k4	70.
a	a	k8xC	a
80.	[number]	k4	80.
letech	let	k1gInPc6	let
přineslo	přinést	k5eAaPmAgNnS	přinést
protipohyb	protipohyb	k1gInSc4	protipohyb
–	–	k?	–
neokonzervatismus	neokonzervatismus	k1gInSc1	neokonzervatismus
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
reprezentovaný	reprezentovaný	k2eAgInSc1d1	reprezentovaný
především	především	k9	především
republikánským	republikánský	k2eAgMnSc7d1	republikánský
prezidentem	prezident	k1gMnSc7	prezident
Ronaldem	Ronald	k1gMnSc7	Ronald
Reaganem	Reagan	k1gMnSc7	Reagan
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
zvolil	zvolit	k5eAaPmAgMnS	zvolit
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
postup	postup	k1gInSc4	postup
i	i	k9	i
vůči	vůči	k7c3	vůči
Sovětům	Sověty	k1gInPc3	Sověty
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc4	ten
právě	právě	k6eAd1	právě
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nakonec	nakonec	k6eAd1	nakonec
reagoval	reagovat	k5eAaBmAgMnS	reagovat
vstřícně	vstřícně	k6eAd1	vstřícně
na	na	k7c4	na
reformátorského	reformátorský	k2eAgMnSc4d1	reformátorský
vůdce	vůdce	k1gMnSc4	vůdce
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
Michaila	Michail	k1gMnSc2	Michail
Gorbačova	Gorbačův	k2eAgMnSc2d1	Gorbačův
(	(	kIx(	(
<g/>
1985-1991	[number]	k4	1985-1991
<g/>
)	)	kIx)	)
a	a	k8xC	a
sérií	série	k1gFnSc7	série
dohod	dohoda	k1gFnPc2	dohoda
o	o	k7c6	o
odzbrojení	odzbrojení	k1gNnSc6	odzbrojení
de	de	k?	de
facto	facto	k1gNnSc4	facto
Studenou	studený	k2eAgFnSc4d1	studená
válku	válka	k1gFnSc4	válka
ukončil	ukončit	k5eAaPmAgInS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
definitivně	definitivně	k6eAd1	definitivně
skončila	skončit	k5eAaPmAgFnS	skončit
pádem	pád	k1gInSc7	pád
komunismu	komunismus	k1gInSc2	komunismus
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozpadem	rozpad	k1gInSc7	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
hovořit	hovořit	k5eAaImF	hovořit
jako	jako	k9	jako
o	o	k7c6	o
jediné	jediný	k2eAgFnSc6d1	jediná
světové	světový	k2eAgFnSc6d1	světová
supervelmoci	supervelmoc	k1gFnSc6	supervelmoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
===	===	k?	===
</s>
</p>
<p>
<s>
Euforie	euforie	k1gFnSc1	euforie
z	z	k7c2	z
konce	konec	k1gInSc2	konec
Studené	Studené	k2eAgFnSc2d1	Studené
války	válka	k1gFnSc2	válka
a	a	k8xC	a
triumfu	triumf	k1gInSc2	triumf
západního	západní	k2eAgInSc2d1	západní
modelu	model	k1gInSc2	model
společnosti	společnost	k1gFnSc2	společnost
trvala	trvat	k5eAaImAgFnS	trvat
takřka	takřka	k6eAd1	takřka
po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
90.	[number]	k4	90.
léta	léto	k1gNnSc2	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnSc3d1	americká
ekonomice	ekonomika	k1gFnSc3	ekonomika
<g/>
,	,	kIx,	,
opojené	opojený	k2eAgFnSc6d1	opojená
průnikem	průnik	k1gInSc7	průnik
na	na	k7c4	na
nové	nový	k2eAgInPc4d1	nový
trhy	trh	k1gInPc4	trh
a	a	k8xC	a
možnostmi	možnost	k1gFnPc7	možnost
internetu	internet	k1gInSc2	internet
<g/>
,	,	kIx,	,
digitalizace	digitalizace	k1gFnSc2	digitalizace
a	a	k8xC	a
nových	nový	k2eAgFnPc2d1	nová
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dařilo	dařit	k5eAaImAgNnS	dařit
(	(	kIx(	(
<g/>
90	[number]	k4	90
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
jsou	být	k5eAaImIp3nP	být
nejdelším	dlouhý	k2eAgNnPc3d3	nejdelší
obdobím	období	k1gNnPc3	období
setrvalého	setrvalý	k2eAgInSc2d1	setrvalý
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
růstu	růst	k1gInSc2	růst
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
historii	historie	k1gFnSc6	historie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
akceptoval	akceptovat	k5eAaBmAgMnS	akceptovat
i	i	k9	i
roli	role	k1gFnSc4	role
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
světového	světový	k2eAgMnSc4d1	světový
četníka	četník	k1gMnSc4	četník
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zejm	zejm	k?	zejm
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
přišel	přijít	k5eAaPmAgInS	přijít
však	však	k9	však
zlom	zlom	k1gInSc1	zlom
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prasknutí	prasknutí	k1gNnSc3	prasknutí
"	"	kIx"	"
<g/>
dot-com	dotom	k1gInSc4	dot-com
bubliny	bublina	k1gFnSc2	bublina
<g/>
"	"	kIx"	"
na	na	k7c6	na
trzích	trh	k1gInPc6	trh
a	a	k8xC	a
poté	poté	k6eAd1	poté
éru	éra	k1gFnSc4	éra
poklidného	poklidný	k2eAgInSc2d1	poklidný
vývoje	vývoj	k1gInSc2	vývoj
narušil	narušit	k5eAaPmAgInS	narušit
teroristický	teroristický	k2eAgInSc1d1	teroristický
útok	útok	k1gInSc1	útok
z	z	k7c2	z
11.	[number]	k4	11.
září	zářit	k5eAaImIp3nS	zářit
2001	[number]	k4	2001
na	na	k7c4	na
newyorská	newyorský	k2eAgNnPc4d1	newyorské
Dvojčata	dvojče	k1gNnPc4	dvojče
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
provedli	provést	k5eAaPmAgMnP	provést
islámští	islámský	k2eAgMnPc1d1	islámský
teroristé	terorista	k1gMnPc1	terorista
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Al-Káida	Al-Káid	k1gMnSc2	Al-Káid
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
demokratický	demokratický	k2eAgInSc1d1	demokratický
svět	svět	k1gInSc1	svět
s	s	k7c7	s
Američany	Američan	k1gMnPc7	Američan
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc7	jejich
prezidentem	prezident	k1gMnSc7	prezident
George	George	k1gNnSc2	George
W.	W.	kA	W.
Bushem	Bush	k1gMnSc7	Bush
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
chvíli	chvíle	k1gFnSc6	chvíle
solidarizoval	solidarizovat	k5eAaImAgMnS	solidarizovat
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc2	jejich
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Válka	válka	k1gFnSc1	válka
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
především	především	k6eAd1	především
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
invazi	invaze	k1gFnSc6	invaze
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
tolik	tolik	k6eAd1	tolik
respektována	respektovat	k5eAaImNgFnS	respektovat
nebyla	být	k5eNaImAgFnS	být
a	a	k8xC	a
vzrostlo	vzrůst	k5eAaPmAgNnS	vzrůst
i	i	k9	i
napětí	napětí	k1gNnSc4	napětí
uvnitř	uvnitř	k7c2	uvnitř
americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jistotami	jistota	k1gFnPc7	jistota
zacloumala	zacloumat	k5eAaPmAgFnS	zacloumat
i	i	k9	i
finanční	finanční	k2eAgFnSc1d1	finanční
krize	krize	k1gFnSc1	krize
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
jako	jako	k9	jako
americká	americký	k2eAgFnSc1d1	americká
hypoteční	hypoteční	k2eAgFnSc1d1	hypoteční
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
přerostla	přerůst	k5eAaPmAgFnS	přerůst
v	v	k7c4	v
krizi	krize	k1gFnSc4	krize
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vzrostlo	vzrůst	k5eAaPmAgNnS	vzrůst
rovněž	rovněž	k9	rovněž
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
velmocemi	velmoc	k1gFnPc7	velmoc
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
s	s	k7c7	s
nelibostí	nelibost	k1gFnSc7	nelibost
nesou	nést	k5eAaImIp3nP	nést
americkou	americký	k2eAgFnSc4d1	americká
hegemonii	hegemonie	k1gFnSc4	hegemonie
-	-	kIx~	-
především	především	k9	především
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Oslabeny	oslaben	k2eAgInPc1d1	oslaben
byly	být	k5eAaImAgInP	být
vztahy	vztah	k1gInPc1	vztah
i	i	k9	i
se	s	k7c7	s
západní	západní	k2eAgFnSc7d1	západní
Evropou	Evropa	k1gFnSc7	Evropa
(	(	kIx(	(
<g/>
krom	krom	k7c2	krom
Británie	Británie	k1gFnSc2	Británie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
trendy	trend	k1gInPc1	trend
vyvrcholily	vyvrcholit	k5eAaPmAgInP	vyvrcholit
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Donalda	Donald	k1gMnSc2	Donald
Trumpa	Trump	k1gMnSc2	Trump
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
roku	rok	k1gInSc2	rok
2017.	[number]	k4	2017.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Poloha	poloha	k1gFnSc1	poloha
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc7	jejich
břehy	břeh	k1gInPc7	břeh
omývá	omývat	k5eAaImIp3nS	omývat
z	z	k7c2	z
východu	východ	k1gInSc2	východ
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
a	a	k8xC	a
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
Aljaška	Aljaška	k1gFnSc1	Aljaška
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
kontinentu	kontinent	k1gInSc2	kontinent
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
omýván	omývat	k5eAaImNgInS	omývat
Severním	severní	k2eAgInSc7d1	severní
ledovým	ledový	k2eAgInSc7d1	ledový
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aljašku	Aljaška	k1gFnSc4	Aljaška
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
Euroasijského	euroasijský	k2eAgInSc2d1	euroasijský
kontinentu	kontinent	k1gInSc2	kontinent
Beringův	Beringův	k2eAgInSc4d1	Beringův
průliv	průliv	k1gInSc4	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
poloostrovy	poloostrov	k1gInPc4	poloostrov
patří	patřit	k5eAaImIp3nS	patřit
Florida	Florida	k1gFnSc1	Florida
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
USA	USA	kA	USA
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Mexický	mexický	k2eAgInSc4d1	mexický
záliv	záliv	k1gInSc4	záliv
od	od	k7c2	od
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
severu	sever	k1gInSc2	sever
mají	mít	k5eAaImIp3nP	mít
USA	USA	kA	USA
společnou	společný	k2eAgFnSc4d1	společná
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Kanadou	Kanada	k1gFnSc7	Kanada
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
pak	pak	k6eAd1	pak
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Floridský	floridský	k2eAgInSc1d1	floridský
průliv	průliv	k1gInSc1	průliv
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
USA	USA	kA	USA
od	od	k7c2	od
souostroví	souostroví	k1gNnSc2	souostroví
Bahamy	Bahamy	k1gFnPc4	Bahamy
a	a	k8xC	a
od	od	k7c2	od
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Horopis	horopis	k1gInSc4	horopis
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
USA	USA	kA	USA
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
tohoto	tento	k3xDgInSc2	tento
kontinentu	kontinent	k1gInSc2	kontinent
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgInSc1d1	starý
kanadský	kanadský	k2eAgInSc1d1	kanadský
štít	štít	k1gInSc1	štít
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
kontinentu	kontinent	k1gInSc2	kontinent
a	a	k8xC	a
jehož	jehož	k3xOyRp3gFnSc1	jehož
střední	střední	k2eAgFnSc1d1	střední
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
protlačena	protlačen	k2eAgFnSc1d1	protlačena
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
dno	dno	k1gNnSc4	dno
Hudsonova	Hudsonův	k2eAgInSc2d1	Hudsonův
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
velmi	velmi	k6eAd1	velmi
starými	stará	k1gFnPc7	stará
přeměněnými	přeměněný	k2eAgFnPc7d1	přeměněná
a	a	k8xC	a
vyvřelými	vyvřelý	k2eAgFnPc7d1	vyvřelá
horninami	hornina	k1gFnPc7	hornina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
pod	pod	k7c4	pod
západní	západní	k2eAgInSc4d1	západní
okraj	okraj	k1gInSc4	okraj
desek	deska	k1gFnPc2	deska
amerických	americký	k2eAgInPc2d1	americký
kontinentů	kontinent	k1gInPc2	kontinent
podsouvá	podsouvat	k5eAaImIp3nS	podsouvat
tichooceánská	tichooceánský	k2eAgFnSc1d1	Tichooceánská
litosférická	litosférický	k2eAgFnSc1d1	litosférická
deska	deska	k1gFnSc1	deska
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Severní	severní	k2eAgFnSc3d1	severní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
pohybů	pohyb	k1gInPc2	pohyb
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
vysoká	vysoký	k2eAgFnSc1d1	vysoká
horská	horský	k2eAgFnSc1d1	horská
hradba	hradba	k1gFnSc1	hradba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
od	od	k7c2	od
Aleutských	aleutský	k2eAgInPc2d1	aleutský
ostrovů	ostrov	k1gInPc2	ostrov
přes	přes	k7c4	přes
Aljašský	aljašský	k2eAgInSc4d1	aljašský
poloostrov	poloostrov	k1gInSc4	poloostrov
až	až	k9	až
po	po	k7c4	po
Ohňovou	ohňový	k2eAgFnSc4d1	ohňová
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kordillery	Kordillery	k1gFnPc1	Kordillery
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
výškou	výška	k1gFnSc7	výška
6168	[number]	k4	6168
metrů	metr	k1gInPc2	metr
Denali	Denali	k1gFnPc2	Denali
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Mount	Mount	k1gMnSc1	Mount
McKinley	McKinlea	k1gFnSc2	McKinlea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
začínají	začínat	k5eAaImIp3nP	začínat
Aljašským	aljašský	k2eAgInSc7d1	aljašský
hřbetem	hřbet	k1gInSc7	hřbet
a	a	k8xC	a
táhnou	táhnout	k5eAaImIp3nP	táhnout
se	se	k3xPyFc4	se
na	na	k7c4	na
jih	jih	k1gInSc4	jih
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
výrazných	výrazný	k2eAgNnPc6d1	výrazné
pásmech	pásmo	k1gNnPc6	pásmo
<g/>
:	:	kIx,	:
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
leží	ležet	k5eAaImIp3nS	ležet
Pacifické	pacifický	k2eAgNnSc1d1	pacifické
pobřežní	pobřežní	k2eAgNnSc1d1	pobřežní
pásmo	pásmo	k1gNnSc1	pásmo
a	a	k8xC	a
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
Skalnaté	skalnatý	k2eAgFnSc2d1	skalnatá
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
pásy	pás	k1gInPc7	pás
Kordiller	Kordillery	k1gFnPc2	Kordillery
se	se	k3xPyFc4	se
rozprostírají	rozprostírat	k5eAaImIp3nP	rozprostírat
sníženiny	sníženina	k1gFnPc1	sníženina
a	a	k8xC	a
plošiny	plošina	k1gFnPc1	plošina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Koloradská	Koloradský	k2eAgFnSc1d1	Koloradský
plošina	plošina	k1gFnSc1	plošina
či	či	k8xC	či
Velká	velký	k2eAgFnSc1d1	velká
pánev	pánev	k1gFnSc1	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
bod	bod	k1gInSc4	bod
tzv.	tzv.	kA	tzv.
sousedících	sousedící	k2eAgInPc2d1	sousedící
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
bez	bez	k7c2	bez
Aljašky	Aljaška	k1gFnSc2	Aljaška
a	a	k8xC	a
Havaje	Havaj	k1gFnSc2	Havaj
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc4	vrchol
Mount	Mounto	k1gNnPc2	Mounto
Whitney	Whitnea	k1gMnSc2	Whitnea
(	(	kIx(	(
<g/>
4417	[number]	k4	4417
m	m	kA	m
<g/>
)	)	kIx)	)
v	v	k7c6	v
kalifornském	kalifornský	k2eAgNnSc6d1	kalifornské
pohoří	pohoří	k1gNnSc6	pohoří
Sierra	Sierra	k1gFnSc1	Sierra
Nevada	Nevada	k1gFnSc1	Nevada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jen	jen	k9	jen
pár	pár	k4xCyI	pár
desítek	desítka	k1gFnPc2	desítka
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Sierry	Sierra	k1gFnSc2	Sierra
Nevady	Nevada	k1gFnSc2	Nevada
leží	ležet	k5eAaImIp3nS	ležet
nejníže	nízce	k6eAd3	nízce
položené	položený	k2eAgNnSc4d1	položené
místo	místo	k1gNnSc4	místo
USA	USA	kA	USA
i	i	k8xC	i
Ameriky	Amerika	k1gFnSc2	Amerika
v	v	k7c6	v
Údolí	údolí	k1gNnSc6	údolí
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Death	Death	k1gMnSc1	Death
Valley	Vallea	k1gFnSc2	Vallea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
proláklina	proláklina	k1gFnSc1	proláklina
je	být	k5eAaImIp3nS	být
86	[number]	k4	86
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
hladiny	hladina	k1gFnSc2	hladina
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
dostala	dostat	k5eAaPmAgFnS	dostat
podle	podle	k7c2	podle
nehostinných	hostinný	k2eNgFnPc2d1	nehostinná
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
život	život	k1gInSc4	život
<g/>
:	:	kIx,	:
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
poušť	poušť	k1gFnSc4	poušť
bez	bez	k7c2	bez
vodních	vodní	k2eAgInPc2d1	vodní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
naměřena	naměřit	k5eAaBmNgFnS	naměřit
dosud	dosud	k6eAd1	dosud
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
56,7	[number]	k4	56,7
°	°	k?	°
<g/>
C	C	kA	C
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poblíž	poblíž	k7c2	poblíž
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
staré	starý	k2eAgNnSc4d1	staré
pohoří	pohoří	k1gNnSc4	pohoří
Apalačské	Apalačský	k2eAgFnSc2d1	Apalačský
hory	hora	k1gFnSc2	hora
a	a	k8xC	a
mezi	mezi	k7c7	mezi
Apalačskými	Apalačský	k2eAgFnPc7d1	Apalačský
horami	hora	k1gFnPc7	hora
a	a	k8xC	a
Atlantským	atlantský	k2eAgInSc7d1	atlantský
oceánem	oceán	k1gInSc7	oceán
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
pak	pak	k6eAd1	pak
Pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
nížina	nížina	k1gFnSc1	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Mexický	mexický	k2eAgInSc1d1	mexický
záliv	záliv	k1gInSc1	záliv
lemuje	lemovat	k5eAaImIp3nS	lemovat
Mississippská	mississippský	k2eAgFnSc1d1	Mississippská
nížina	nížina	k1gFnSc1	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Apalačskými	Apalačský	k2eAgFnPc7d1	Apalačský
horami	hora	k1gFnPc7	hora
a	a	k8xC	a
Kordillerami	Kordillery	k1gFnPc7	Kordillery
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
od	od	k7c2	od
východu	východ	k1gInSc2	východ
rozprostírají	rozprostírat	k5eAaImIp3nP	rozprostírat
Vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
roviny	rovina	k1gFnPc1	rovina
a	a	k8xC	a
Velké	velký	k2eAgFnPc1d1	velká
planiny	planina	k1gFnPc1	planina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vodstvo	vodstvo	k1gNnSc1	vodstvo
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
USA	USA	kA	USA
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
úmoří	úmoří	k1gNnSc2	úmoří
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Vlévají	vlévat	k5eAaImIp3nP	vlévat
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
veletoky	veletok	k1gInPc1	veletok
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Mississippi	Mississippi	k1gNnSc1	Mississippi
s	s	k7c7	s
přítokem	přítok	k1gInSc7	přítok
Missouri	Missouri	k1gFnSc2	Missouri
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
říční	říční	k2eAgFnSc1d1	říční
soustava	soustava	k1gFnSc1	soustava
USA	USA	kA	USA
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
6212	[number]	k4	6212
km	km	kA	km
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
také	také	k9	také
Řeka	řeka	k1gFnSc1	řeka
Svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
či	či	k8xC	či
Rio	Rio	k1gMnSc2	Rio
Grande	grand	k1gMnSc5	grand
<g/>
.	.	kIx.	.
</s>
<s>
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
zásobují	zásobovat	k5eAaImIp3nP	zásobovat
vesměs	vesměs	k6eAd1	vesměs
kratší	krátký	k2eAgInPc1d2	kratší
toky	tok	k1gInPc1	tok
tekoucí	tekoucí	k2eAgInPc1d1	tekoucí
z	z	k7c2	z
Kordiller	Kordillery	k1gFnPc2	Kordillery
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
řeky	řeka	k1gFnPc1	řeka
Columbia	Columbia	k1gFnSc1	Columbia
a	a	k8xC	a
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
USA	USA	kA	USA
najdeme	najít	k5eAaPmIp1nP	najít
i	i	k9	i
mnoho	mnoho	k4c4	mnoho
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Ledovcového	ledovcový	k2eAgInSc2d1	ledovcový
původu	původ	k1gInSc2	původ
jsou	být	k5eAaImIp3nP	být
Velká	velký	k2eAgNnPc1d1	velké
jezera	jezero	k1gNnPc1	jezero
(	(	kIx(	(
<g/>
Hořejší	Hořejší	k1gFnSc1	Hořejší
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
Michiganské	michiganský	k2eAgNnSc1d1	Michiganské
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
Huronské	Huronský	k2eAgNnSc1d1	Huronské
jezero	jezero	k1gNnSc1	jezero
a	a	k8xC	a
jezero	jezero	k1gNnSc1	jezero
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
největší	veliký	k2eAgFnSc4d3	veliký
zásobárnu	zásobárna	k1gFnSc4	zásobárna
sladké	sladký	k2eAgFnSc2d1	sladká
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
přírodní	přírodní	k2eAgFnSc6d1	přírodní
hranici	hranice	k1gFnSc6	hranice
mezi	mezi	k7c7	mezi
USA	USA	kA	USA
a	a	k8xC	a
Kanadou	Kanada	k1gFnSc7	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
Solné	solný	k2eAgNnSc1d1	solné
jezero	jezero	k1gNnSc1	jezero
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
v	v	k7c6	v
bezodtoké	bezodtoký	k2eAgFnSc6d1	bezodtoká
oblasti	oblast	k1gFnSc6	oblast
mezi	mezi	k7c7	mezi
Skalnatými	skalnatý	k2eAgFnPc7d1	skalnatá
horami	hora	k1gFnPc7	hora
a	a	k8xC	a
Coastal	Coastal	k1gFnPc7	Coastal
Range	Rang	k1gFnSc2	Rang
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
vodopádů	vodopád	k1gInPc2	vodopád
<g/>
;	;	kIx,	;
nesporně	sporně	k6eNd1	sporně
nejznámější	známý	k2eAgInPc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
Niagarské	niagarský	k2eAgInPc1d1	niagarský
vodopády	vodopád	k1gInPc1	vodopád
mezi	mezi	k7c7	mezi
Erijským	Erijský	k2eAgNnSc7d1	Erijské
a	a	k8xC	a
Ontarijským	ontarijský	k2eAgNnSc7d1	Ontarijské
jezerem	jezero	k1gNnSc7	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Díky	dík	k1gInPc1	dík
své	svůj	k3xOyFgFnSc2	svůj
velikosti	velikost	k1gFnSc2	velikost
leží	ležet	k5eAaImIp3nS	ležet
USA	USA	kA	USA
v	v	k7c6	v
několika	několik	k4yIc6	několik
významných	významný	k2eAgInPc6d1	významný
podnebních	podnební	k2eAgInPc6d1	podnební
pásech	pás	k1gInPc6	pás
<g/>
.	.	kIx.	.
</s>
<s>
Počasí	počasí	k1gNnSc1	počasí
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
nestálé	stálý	k2eNgInPc4d1	nestálý
a	a	k8xC	a
teplotní	teplotní	k2eAgInPc4d1	teplotní
výkyvy	výkyv	k1gInPc4	výkyv
mohou	moct	k5eAaImIp3nP	moct
nastávat	nastávat	k5eAaImF	nastávat
dokonce	dokonce	k9	dokonce
i	i	k9	i
několikrát	několikrát	k6eAd1	několikrát
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
mírném	mírný	k2eAgInSc6d1	mírný
pásu	pás	k1gInSc6	pás
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
chladnější	chladný	k2eAgInSc1d2	chladnější
a	a	k8xC	a
vlhčí	vlhký	k2eAgInSc1d2	vlhčí
než	než	k8xS	než
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
to	ten	k3xDgNnSc1	ten
studený	studený	k2eAgInSc1d1	studený
Labradorský	labradorský	k2eAgInSc1d1	labradorský
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
omývá	omývat	k5eAaImIp3nS	omývat
severovýchodní	severovýchodní	k2eAgNnSc4d1	severovýchodní
a	a	k8xC	a
východní	východní	k2eAgNnSc4d1	východní
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vnitřních	vnitřní	k2eAgFnPc6d1	vnitřní
rovinách	rovina	k1gFnPc6	rovina
a	a	k8xC	a
Velkých	velký	k2eAgFnPc6d1	velká
planinách	planina	k1gFnPc6	planina
padá	padat	k5eAaImIp3nS	padat
úměrně	úměrně	k6eAd1	úměrně
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
od	od	k7c2	od
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
čím	co	k3yRnSc7	co
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
méně	málo	k6eAd2	málo
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
jevem	jev	k1gInSc7	jev
je	být	k5eAaImIp3nS	být
sněhový	sněhový	k2eAgInSc4d1	sněhový
efekt	efekt	k1gInSc4	efekt
Velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
způsobující	způsobující	k2eAgFnPc1d1	způsobující
sněhové	sněhový	k2eAgFnPc1d1	sněhová
kalamity	kalamita	k1gFnPc1	kalamita
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
území	území	k1gNnSc6	území
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Chladnější	chladný	k2eAgNnSc1d2	chladnější
podnebí	podnebí	k1gNnSc1	podnebí
panuje	panovat	k5eAaImIp3nS	panovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
proudů	proud	k1gInPc2	proud
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
udržuje	udržovat	k5eAaImIp3nS	udržovat
teplota	teplota	k1gFnSc1	teplota
vesměs	vesměs	k6eAd1	vesměs
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
.	.	kIx.	.
</s>
<s>
Nejchladnější	chladný	k2eAgFnPc1d3	nejchladnější
zimy	zima	k1gFnPc1	zima
najdeme	najít	k5eAaPmIp1nP	najít
ve	v	k7c6	v
Skalistých	skalistý	k2eAgFnPc6d1	skalistý
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
Sierra	Sierra	k1gFnSc1	Sierra
Nevadě	Nevada	k1gFnSc6	Nevada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
můžeme	moct	k5eAaImIp1nP	moct
naměřit	naměřit	k5eAaBmF	naměřit
jedny	jeden	k4xCgInPc4	jeden
z	z	k7c2	z
nejsilnějších	silný	k2eAgInPc2d3	nejsilnější
mrazů	mráz	k1gInPc2	mráz
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Velkých	velký	k2eAgFnPc2d1	velká
plání	pláň	k1gFnPc2	pláň
<g/>
,	,	kIx,	,
jižních	jižní	k2eAgFnPc2d1	jižní
částí	část	k1gFnPc2	část
atlantického	atlantický	k2eAgNnSc2d1	atlantické
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
Floridě	Florida	k1gFnSc6	Florida
a	a	k8xC	a
státech	stát	k1gInPc6	stát
u	u	k7c2	u
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
panují	panovat	k5eAaImIp3nP	panovat
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
až	až	k9	až
tropická	tropický	k2eAgNnPc1d1	tropické
vedra	vedro	k1gNnPc1	vedro
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
může	moct	k5eAaImIp3nS	moct
doprovázet	doprovázet	k5eAaImF	doprovázet
i	i	k9	i
nesnesitelná	snesitelný	k2eNgFnSc1d1	nesnesitelná
vlhkost	vlhkost	k1gFnSc1	vlhkost
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
USA	USA	kA	USA
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
příjemném	příjemný	k2eAgInSc6d1	příjemný
subtropickém	subtropický	k2eAgInSc6d1	subtropický
podnebném	podnebný	k2eAgInSc6d1	podnebný
pásu	pás	k1gInSc6	pás
a	a	k8xC	a
Mexický	mexický	k2eAgInSc1d1	mexický
záliv	záliv	k1gInSc1	záliv
s	s	k7c7	s
poloostrovem	poloostrov	k1gInSc7	poloostrov
Florida	Florida	k1gFnSc1	Florida
v	v	k7c6	v
tropickém	tropický	k2eAgInSc6d1	tropický
pásu	pás	k1gInSc6	pás
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
vysoce	vysoce	k6eAd1	vysoce
položené	položený	k2eAgNnSc4d1	položené
pohoří	pohoří	k1gNnSc4	pohoří
a	a	k8xC	a
plošiny	plošina	k1gFnPc1	plošina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
podstatně	podstatně	k6eAd1	podstatně
chladněji	chladně	k6eAd2	chladně
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
projevuje	projevovat	k5eAaImIp3nS	projevovat
výšková	výškový	k2eAgFnSc1d1	výšková
stupňovitost	stupňovitost	k1gFnSc1	stupňovitost
<g/>
.	.	kIx.	.
</s>
<s>
Aljaška	Aljaška	k1gFnSc1	Aljaška
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
subarktickém	subarktický	k2eAgInSc6d1	subarktický
pásu	pás	k1gInSc6	pás
a	a	k8xC	a
Havajské	havajský	k2eAgInPc1d1	havajský
ostrovy	ostrov	k1gInPc1	ostrov
v	v	k7c6	v
tropickém	tropický	k2eAgInSc6d1	tropický
pásu	pás	k1gInSc6	pás
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
povrchu	povrch	k1gInSc2	povrch
Ameriky	Amerika	k1gFnSc2	Amerika
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
takřka	takřka	k6eAd1	takřka
bezproblémový	bezproblémový	k2eAgInSc4d1	bezproblémový
přesun	přesun	k1gInSc4	přesun
vzdušných	vzdušný	k2eAgFnPc2d1	vzdušná
mas	masa	k1gFnPc2	masa
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
časté	častý	k2eAgMnPc4d1	častý
hlavně	hlavně	k6eAd1	hlavně
vpády	vpád	k1gInPc4	vpád
arktického	arktický	k2eAgInSc2d1	arktický
vzduchu	vzduch	k1gInSc2	vzduch
z	z	k7c2	z
vyšších	vysoký	k2eAgFnPc2d2	vyšší
zeměpisných	zeměpisný	k2eAgFnPc2d1	zeměpisná
šířek	šířka	k1gFnPc2	šířka
až	až	k9	až
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Proudění	proudění	k1gNnSc1	proudění
v	v	k7c6	v
rovnoběžkovém	rovnoběžkový	k2eAgInSc6d1	rovnoběžkový
směru	směr	k1gInSc6	směr
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
horské	horský	k2eAgFnPc4d1	horská
bariéry	bariéra	k1gFnPc4	bariéra
Kordiller	Kordillery	k1gFnPc2	Kordillery
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
vláha	vláha	k1gFnSc1	vláha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
s	s	k7c7	s
proudícím	proudící	k2eAgInSc7d1	proudící
vzduchem	vzduch	k1gInSc7	vzduch
dostávala	dostávat	k5eAaImAgFnS	dostávat
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
Ameriky	Amerika	k1gFnSc2	Amerika
z	z	k7c2	z
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
vyprší	vypršet	k5eAaPmIp3nS	vypršet
nad	nad	k7c7	nad
oceánem	oceán	k1gInSc7	oceán
<g/>
,	,	kIx,	,
úzkou	úzký	k2eAgFnSc7d1	úzká
pobřežní	pobřežní	k2eAgFnSc7d1	pobřežní
nížinou	nížina	k1gFnSc7	nížina
a	a	k8xC	a
návětrnými	návětrný	k2eAgInPc7d1	návětrný
svahy	svah	k1gInPc7	svah
tichooceánského	tichooceánský	k2eAgNnSc2d1	tichooceánský
pásma	pásmo	k1gNnSc2	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Texas	Texas	k1gInSc1	Texas
je	být	k5eAaImIp3nS	být
jinak	jinak	k6eAd1	jinak
známý	známý	k2eAgMnSc1d1	známý
pro	pro	k7c4	pro
svá	svůj	k3xOyFgNnPc4	svůj
tornáda	tornádo	k1gNnPc4	tornádo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
otevřených	otevřený	k2eAgFnPc6d1	otevřená
plochách	plocha	k1gFnPc6	plocha
v	v	k7c6	v
období	období	k1gNnSc6	období
května	květen	k1gInSc2	květen
a	a	k8xC	a
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
jsou	být	k5eAaImIp3nP	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
federací	federace	k1gFnSc7	federace
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
zastupitelskou	zastupitelský	k2eAgFnSc4d1	zastupitelská
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
,	,	kIx,	,
fungující	fungující	k2eAgInSc4d1	fungující
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
kongresového	kongresový	k2eAgInSc2d1	kongresový
systému	systém	k1gInSc2	systém
definovaného	definovaný	k2eAgInSc2d1	definovaný
ústavou	ústava	k1gFnSc7	ústava
nahrazující	nahrazující	k2eAgFnSc4d1	nahrazující
původní	původní	k2eAgFnSc4d1	původní
ústavu	ústava	k1gFnSc4	ústava
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Články	článek	k1gInPc1	článek
Konfederace	konfederace	k1gFnSc2	konfederace
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Articles	Articles	k1gInSc1	Articles
of	of	k?	of
Confederation	Confederation	k1gInSc1	Confederation
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
platné	platný	k2eAgInPc4d1	platný
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1781	[number]	k4	1781
a	a	k8xC	a
1788	[number]	k4	1788
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
tak	tak	k9	tak
spadá	spadat	k5eAaPmIp3nS	spadat
pod	pod	k7c4	pod
troje	troje	k4xRgFnPc4	troje
orgány	orgány	k1gFnPc4	orgány
–	–	k?	–
na	na	k7c6	na
federální	federální	k2eAgFnSc6d1	federální
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc6d1	státní
a	a	k8xC	a
místní	místní	k2eAgFnSc6d1	místní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
mnohé	mnohý	k2eAgFnPc1d1	mnohá
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
spravovány	spravován	k2eAgInPc1d1	spravován
více	hodně	k6eAd2	hodně
místními	místní	k2eAgFnPc7d1	místní
správami	správa	k1gFnPc7	správa
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
i	i	k9	i
okresními	okresní	k2eAgInPc7d1	okresní
nebo	nebo	k8xC	nebo
metropolitními	metropolitní	k2eAgInPc7d1	metropolitní
orgány	orgán	k1gInPc7	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
správní	správní	k2eAgInPc1d1	správní
orgány	orgán	k1gInPc1	orgán
jsou	být	k5eAaImIp3nP	být
voliči	volič	k1gMnPc7	volič
voleny	volen	k2eAgMnPc4d1	volen
v	v	k7c6	v
tajných	tajný	k2eAgFnPc6d1	tajná
volbách	volba	k1gFnPc6	volba
nebo	nebo	k8xC	nebo
jmenovány	jmenovat	k5eAaBmNgFnP	jmenovat
voliči	volič	k1gMnPc7	volič
volenými	volený	k2eAgMnPc7d1	volený
zástupci	zástupce	k1gMnPc7	zástupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Státní	státní	k2eAgFnSc1d1	státní
správa	správa	k1gFnSc1	správa
se	se	k3xPyFc4	se
na	na	k7c6	na
federální	federální	k2eAgFnSc7d1	federální
úrovní	úroveň	k1gFnPc2	úroveň
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
složek	složka	k1gFnPc2	složka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
zákonodárné	zákonodárný	k2eAgNnSc1d1	zákonodárné
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
Kongres	kongres	k1gInSc1	kongres
skládající	skládající	k2eAgFnSc2d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
Senátu	senát	k1gInSc2	senát
a	a	k8xC	a
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
House	house	k1gNnSc1	house
of	of	k?	of
Representatives	Representatives	k1gMnSc1	Representatives
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
určující	určující	k2eAgInPc1d1	určující
federální	federální	k2eAgInPc1d1	federální
zákony	zákon	k1gInPc1	zákon
<g/>
,	,	kIx,	,
vyhlašující	vyhlašující	k2eAgFnPc1d1	vyhlašující
války	válka	k1gFnPc1	válka
<g/>
,	,	kIx,	,
schvalující	schvalující	k2eAgFnPc1d1	schvalující
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
dohody	dohoda	k1gFnPc1	dohoda
a	a	k8xC	a
federální	federální	k2eAgInSc1d1	federální
rozpočet	rozpočet	k1gInSc1	rozpočet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
výkonné	výkonný	k2eAgFnPc4d1	výkonná
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
prezident	prezident	k1gMnSc1	prezident
se	s	k7c7	s
schválením	schválení	k1gNnSc7	schválení
senátu	senát	k1gInSc2	senát
jmenující	jmenující	k2eAgFnSc4d1	jmenující
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
úředníky	úředník	k1gMnPc4	úředník
<g/>
,	,	kIx,	,
spravující	spravující	k2eAgNnSc4d1	spravující
federální	federální	k2eAgNnSc4d1	federální
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
vetující	vetující	k2eAgInPc4d1	vetující
návrhy	návrh	k1gInPc4	návrh
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
velící	velící	k2eAgFnSc6d1	velící
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
soudní	soudní	k2eAgInSc1d1	soudní
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
soud	soud	k1gInSc4	soud
a	a	k8xC	a
níže	nízce	k6eAd2	nízce
postavené	postavený	k2eAgInPc1d1	postavený
federální	federální	k2eAgInPc1d1	federální
soudy	soud	k1gInPc1	soud
jmenované	jmenovaný	k2eAgInPc1d1	jmenovaný
prezidentem	prezident	k1gMnSc7	prezident
se	s	k7c7	s
svolením	svolení	k1gNnSc7	svolení
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
právo	právo	k1gNnSc4	právo
vykládají	vykládat	k5eAaImIp3nP	vykládat
a	a	k8xC	a
určují	určovat	k5eAaImIp3nP	určovat
platnost	platnost	k1gFnSc4	platnost
zákonů	zákon	k1gInPc2	zákon
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
je	být	k5eAaImIp3nS	být
parlamentem	parlament	k1gInSc7	parlament
dvoukomorovým	dvoukomorový	k2eAgInSc7d1	dvoukomorový
<g/>
.	.	kIx.	.
</s>
<s>
Sněmovna	sněmovna	k1gFnSc1	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
má	mít	k5eAaImIp3nS	mít
435	[number]	k4	435
členů	člen	k1gMnPc2	člen
po	po	k7c4	po
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
reprezentujících	reprezentující	k2eAgInPc2d1	reprezentující
takzvané	takzvaný	k2eAgInPc4d1	takzvaný
"	"	kIx"	"
<g/>
kongresové	kongresový	k2eAgInPc4d1	kongresový
okresy	okres	k1gInPc4	okres
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
je	být	k5eAaImIp3nS	být
problematický	problematický	k2eAgInSc1d1	problematický
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
počtu	počet	k1gInSc2	počet
zástupců	zástupce	k1gMnPc2	zástupce
z	z	k7c2	z
každého	každý	k3xTgInSc2	každý
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc4	každý
desátý	desátý	k4xOgInSc4	desátý
rok	rok	k1gInSc4	rok
mění	měnit	k5eAaImIp3nS	měnit
podle	podle	k7c2	podle
vývoje	vývoj	k1gInSc2	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Každému	každý	k3xTgInSc3	každý
státu	stát	k1gInSc3	stát
je	být	k5eAaImIp3nS	být
však	však	k9	však
zajištěn	zajistit	k5eAaPmNgInS	zajistit
nejméně	málo	k6eAd3	málo
jeden	jeden	k4xCgMnSc1	jeden
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
má	mít	k5eAaImIp3nS	mít
sedm	sedm	k4xCc1	sedm
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nelidnatější	lidnatý	k2eNgInSc1d2	lidnatý
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
jich	on	k3xPp3gMnPc2	on
má	mít	k5eAaImIp3nS	mít
53	[number]	k4	53
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
stát	stát	k1gInSc1	stát
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
senátory	senátor	k1gMnPc4	senátor
volené	volený	k2eAgMnPc4d1	volený
na	na	k7c4	na
šestileté	šestiletý	k2eAgNnSc4d1	šestileté
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
druhý	druhý	k4xOgInSc4	druhý
rok	rok	k1gInSc4	rok
je	být	k5eAaImIp3nS	být
volena	volit	k5eAaImNgFnS	volit
třetina	třetina	k1gFnSc1	třetina
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
sněmovna	sněmovna	k1gFnSc1	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc4	každý
druhý	druhý	k4xOgInSc4	druhý
rok	rok	k1gInSc4	rok
volena	volen	k2eAgFnSc1d1	volena
celá	celá	k1gFnSc1	celá
<g/>
.	.	kIx.	.
</s>
<s>
Senátoři	senátor	k1gMnPc1	senátor
tedy	tedy	k9	tedy
mají	mít	k5eAaImIp3nP	mít
šestileté	šestiletý	k2eAgNnSc4d1	šestileté
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
a	a	k8xC	a
kongresmani	kongresman	k1gMnPc1	kongresman
dvouleté	dvouletý	k2eAgFnSc2d1	dvouletá
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
kongresmanů	kongresman	k1gMnPc2	kongresman
i	i	k8xC	i
senátorů	senátor	k1gMnPc2	senátor
není	být	k5eNaImIp3nS	být
ústavou	ústava	k1gFnSc7	ústava
specifikována	specifikován	k2eAgFnSc1d1	specifikována
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
ponechána	ponechat	k5eAaPmNgFnS	ponechat
na	na	k7c6	na
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
do	do	k7c2	do
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
volí	volit	k5eAaImIp3nS	volit
pomocí	pomocí	k7c2	pomocí
Systému	systém	k1gInSc2	systém
prvního	první	k4xOgNnSc2	první
v	v	k7c6	v
cíli	cíl	k1gInSc6	cíl
(	(	kIx(	(
<g/>
First-past-the-post	Firstastheost	k1gFnSc1	First-past-the-post
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ústava	ústava	k1gFnSc1	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
zákonným	zákonný	k2eAgInSc7d1	zákonný
dokumentem	dokument	k1gInSc7	dokument
amerického	americký	k2eAgInSc2d1	americký
právního	právní	k2eAgInSc2d1	právní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
společenskou	společenský	k2eAgFnSc7d1	společenská
smlouvou	smlouva	k1gFnSc7	smlouva
upravující	upravující	k2eAgNnSc1d1	upravující
fungování	fungování	k1gNnSc1	fungování
společnosti	společnost	k1gFnSc2	společnost
pomocí	pomocí	k7c2	pomocí
zvolené	zvolený	k2eAgFnSc2d1	zvolená
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgInPc1	veškerý
zákony	zákon	k1gInPc1	zákon
i	i	k8xC	i
postupy	postup	k1gInPc1	postup
států	stát	k1gInPc2	stát
a	a	k8xC	a
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
předmětem	předmět	k1gInSc7	předmět
zkoumání	zkoumání	k1gNnSc2	zkoumání
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
shledáno	shledat	k5eAaPmNgNnS	shledat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
ústavou	ústava	k1gFnSc7	ústava
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zneplatněny	zneplatněn	k2eAgFnPc1d1	zneplatněn
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
je	být	k5eAaImIp3nS	být
živým	živý	k2eAgInSc7d1	živý
dokumentem	dokument	k1gInSc7	dokument
jež	jenž	k3xRgNnSc4	jenž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
několika	několik	k4yIc7	několik
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
však	však	k9	však
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
svolení	svolení	k1gNnSc4	svolení
alespoň	alespoň	k9	alespoň
převážné	převážný	k2eAgFnSc2d1	převážná
většiny	většina	k1gFnSc2	většina
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
změněna	změněn	k2eAgFnSc1d1	změněna
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
doplněna	doplněn	k2eAgFnSc1d1	doplněna
<g/>
,	,	kIx,	,
27	[number]	k4	27
krát	krát	k6eAd1	krát
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992.	[number]	k4	1992.
</s>
</p>
<p>
<s>
Ústava	ústava	k1gFnSc1	ústava
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
ustanovení	ustanovení	k1gNnSc4	ustanovení
"	"	kIx"	"
<g/>
zachovat	zachovat	k5eAaPmF	zachovat
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
"	"	kIx"	"
a	a	k8xC	a
základní	základní	k2eAgNnPc4d1	základní
práva	právo	k1gNnPc4	právo
podle	podle	k7c2	podle
Listiny	listina	k1gFnSc2	listina
práv	právo	k1gNnPc2	právo
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Bill	Bill	k1gMnSc1	Bill
of	of	k?	of
Rights	Rights	k1gInSc1	Rights
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
dodatků	dodatek	k1gInPc2	dodatek
ústavy	ústava	k1gFnSc2	ústava
jež	jenž	k3xRgInPc1	jenž
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
svobodu	svoboda	k1gFnSc4	svoboda
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
svobodu	svoboda	k1gFnSc4	svoboda
vyznání	vyznání	k1gNnSc2	vyznání
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
spravedlivý	spravedlivý	k2eAgInSc4d1	spravedlivý
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
držet	držet	k5eAaImF	držet
a	a	k8xC	a
nosit	nosit	k5eAaImF	nosit
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
a	a	k8xC	a
právo	právo	k1gNnSc1	právo
vlastnické	vlastnický	k2eAgNnSc1d1	vlastnické
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
všem	všecek	k3xTgInPc3	všecek
státům	stát	k1gInPc3	stát
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
republikánské	republikánský	k2eAgNnSc4d1	republikánské
zřízení	zřízení	k1gNnSc4	zřízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
jsou	být	k5eAaImIp3nP	být
zastupitelskou	zastupitelský	k2eAgFnSc7d1	zastupitelská
demokracií	demokracie	k1gFnSc7	demokracie
a	a	k8xC	a
prezidentskou	prezidentský	k2eAgFnSc7d1	prezidentská
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
většinový	většinový	k2eAgInSc1d1	většinový
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
americké	americký	k2eAgFnSc3d1	americká
politické	politický	k2eAgFnSc3d1	politická
kultuře	kultura	k1gFnSc3	kultura
patří	patřit	k5eAaImIp3nS	patřit
velká	velký	k2eAgFnSc1d1	velká
soupeřivost	soupeřivost	k1gFnSc1	soupeřivost
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
největšími	veliký	k2eAgFnPc7d3	veliký
stranami	strana	k1gFnPc7	strana
a	a	k8xC	a
vysoký	vysoký	k2eAgInSc1d1	vysoký
podíl	podíl	k1gInSc1	podíl
veřejnosti	veřejnost	k1gFnSc2	veřejnost
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americké	americký	k2eAgFnSc3d1	americká
politice	politika	k1gFnSc3	politika
dominují	dominovat	k5eAaImIp3nP	dominovat
dvě	dva	k4xCgFnPc4	dva
největší	veliký	k2eAgFnPc4d3	veliký
strany	strana	k1gFnPc4	strana
–	–	k?	–
Republikánská	republikánský	k2eAgFnSc1d1	republikánská
strana	strana	k1gFnSc1	strana
a	a	k8xC	a
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
drží	držet	k5eAaImIp3nS	držet
převážnou	převážný	k2eAgFnSc4d1	převážná
většinu	většina	k1gFnSc4	většina
volených	volený	k2eAgInPc2d1	volený
úřadů	úřad	k1gInPc2	úřad
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
na	na	k7c6	na
federální	federální	k2eAgFnSc6d1	federální
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc6d1	státní
i	i	k8xC	i
místní	místní	k2eAgFnSc6d1	místní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislí	závislý	k2eNgMnPc1d1	nezávislý
nebo	nebo	k8xC	nebo
kandidáti	kandidát	k1gMnPc1	kandidát
menších	malý	k2eAgFnPc2d2	menší
stran	strana	k1gFnPc2	strana
bývají	bývat	k5eAaImIp3nP	bývat
nejúspěšnější	úspěšný	k2eAgMnPc1d3	nejúspěšnější
převážně	převážně	k6eAd1	převážně
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
místních	místní	k2eAgNnPc2d1	místní
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
několik	několik	k4yIc1	několik
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
senátorů	senátor	k1gMnPc2	senátor
má	mít	k5eAaImIp3nS	mít
svá	svůj	k3xOyFgNnPc4	svůj
křesla	křeslo	k1gNnPc4	křeslo
i	i	k8xC	i
v	v	k7c6	v
senátu	senát	k1gInSc6	senát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
politické	politický	k2eAgFnSc6d1	politická
kultuře	kultura	k1gFnSc6	kultura
je	být	k5eAaImIp3nS	být
Republikánská	republikánský	k2eAgFnSc1d1	republikánská
strana	strana	k1gFnSc1	strana
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
pravostředová	pravostředový	k2eAgNnPc4d1	pravostředový
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
<g/>
,	,	kIx,	,
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
pak	pak	k6eAd1	pak
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
levostředová	levostředový	k2eAgFnSc1d1	levostředová
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
liberální	liberální	k2eAgInSc1d1	liberální
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
ovšem	ovšem	k9	ovšem
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
značné	značný	k2eAgInPc4d1	značný
rozdíly	rozdíl	k1gInPc4	rozdíl
i	i	k9	i
uvnitř	uvnitř	k7c2	uvnitř
jich	on	k3xPp3gMnPc2	on
samotných	samotný	k2eAgMnPc2d1	samotný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
drží	držet	k5eAaImIp3nS	držet
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
většinu	většina	k1gFnSc4	většina
ve	v	k7c6	v
sněmovně	sněmovna	k1gFnSc6	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
i	i	k8xC	i
v	v	k7c6	v
senátu	senát	k1gInSc6	senát
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgInSc4d2	veliký
náskok	náskok	k1gInSc4	náskok
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
úspěchu	úspěch	k1gInSc2	úspěch
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
je	být	k5eAaImIp3nS	být
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
republikán	republikán	k1gMnSc1	republikán
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Přední	přední	k2eAgFnSc2d1	přední
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Republikánská	republikánský	k2eAgFnSc1d1	republikánská
strana	strana	k1gFnSc1	strana
–	–	k?	–
strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
ke	k	k7c3	k
konzervatismu	konzervatismus	k1gInSc3	konzervatismus
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
např.	např.	kA	např.
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
status	status	k1gInSc4	status
pravice	pravice	k1gFnSc2	pravice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
–	–	k?	–
oficiálně	oficiálně	k6eAd1	oficiálně
liberální	liberální	k2eAgFnSc1d1	liberální
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
soustředí	soustředit	k5eAaPmIp3nS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
sociální	sociální	k2eAgFnSc2d1	sociální
a	a	k8xC	a
státní	státní	k2eAgFnSc2d1	státní
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
má	mít	k5eAaImIp3nS	mít
status	status	k1gInSc4	status
levice	levice	k1gFnSc2	levice
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
vnímána	vnímán	k2eAgFnSc1d1	vnímána
spíše	spíše	k9	spíše
jako	jako	k8xC	jako
středová	středový	k2eAgFnSc1d1	středová
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
ekonomicky	ekonomicky	k6eAd1	ekonomicky
konzervativnější	konzervativní	k2eAgFnPc4d2	konzervativnější
než	než	k8xS	než
evropské	evropský	k2eAgFnPc4d1	Evropská
levicové	levicový	k2eAgFnPc4d1	levicová
strany	strana	k1gFnPc4	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Vývojové	vývojový	k2eAgFnPc1d1	vývojová
tendence	tendence	k1gFnPc1	tendence
====	====	k?	====
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
měřítku	měřítko	k1gNnSc6	měřítko
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
a	a	k8xC	a
vojenský	vojenský	k2eAgInSc1d1	vojenský
vývoj	vývoj	k1gInSc1	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vliv	vliv	k1gInSc1	vliv
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
četné	četný	k2eAgFnSc2d1	četná
diskuze	diskuze	k1gFnSc2	diskuze
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc1	všechen
státy	stát	k1gInPc1	stát
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
svá	svůj	k3xOyFgNnPc4	svůj
velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
konzuláty	konzulát	k1gInPc1	konzulát
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
Bhútán	Bhútán	k1gInSc1	Bhútán
a	a	k8xC	a
Súdán	Súdán	k1gInSc1	Súdán
diplomatické	diplomatický	k2eAgInPc1d1	diplomatický
styky	styk	k1gInPc1	styk
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
neudržují	udržovat	k5eNaImIp3nP	udržovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
jsou	být	k5eAaImIp3nP	být
zakládajícím	zakládající	k2eAgInSc7d1	zakládající
členem	člen	k1gInSc7	člen
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
se	s	k7c7	s
stálým	stálý	k2eAgNnSc7d1	stálé
zastoupením	zastoupení	k1gNnSc7	zastoupení
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
a	a	k8xC	a
právem	právo	k1gNnSc7	právo
veta	veto	k1gNnSc2	veto
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
členem	člen	k1gInSc7	člen
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Současnými	současný	k2eAgMnPc7d1	současný
spojenci	spojenec	k1gMnPc7	spojenec
jsou	být	k5eAaImIp3nP	být
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
a	a	k8xC	a
státy	stát	k1gInPc1	stát
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
USA	USA	kA	USA
další	další	k2eAgInPc4d1	další
nadstandardní	nadstandardní	k2eAgInPc4d1	nadstandardní
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
mají	mít	k5eAaImIp3nP	mít
USA	USA	kA	USA
úzké	úzký	k2eAgInPc4d1	úzký
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
a	a	k8xC	a
kulturní	kulturní	k2eAgInPc4d1	kulturní
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
sousedí	sousedit	k5eAaImIp3nS	sousedit
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
s	s	k7c7	s
Kanadou	Kanada	k1gFnSc7	Kanada
a	a	k8xC	a
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
celek	celek	k1gInSc4	celek
prošla	projít	k5eAaPmAgFnS	projít
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
USA	USA	kA	USA
především	především	k9	především
ve	v	k7c6	v
20.	[number]	k4	20.
století	století	k1gNnSc6	století
rozmanitým	rozmanitý	k2eAgInSc7d1	rozmanitý
vývojem	vývoj	k1gInSc7	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Monroeova	Monroeův	k2eAgFnSc1d1	Monroeova
doktrína	doktrína	k1gFnSc1	doktrína
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1823	[number]	k4	1823
definovala	definovat	k5eAaBmAgFnS	definovat
celou	celý	k2eAgFnSc4d1	celá
západní	západní	k2eAgFnSc4d1	západní
polokouli	polokoule	k1gFnSc4	polokoule
za	za	k7c4	za
sféru	sféra	k1gFnSc4	sféra
vlivu	vliv	k1gInSc2	vliv
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězné	vítězný	k2eAgFnSc6d1	vítězná
španělsko-americké	španělskomerický	k2eAgFnSc6d1	španělsko-americká
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
anektovaly	anektovat	k5eAaBmAgInP	anektovat
Havaj	Havaj	k1gFnSc4	Havaj
<g/>
,	,	kIx,	,
Portoriko	Portoriko	k1gNnSc4	Portoriko
a	a	k8xC	a
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tzv.	tzv.	kA	tzv.
banánových	banánový	k2eAgFnPc6d1	banánová
válkách	válka	k1gFnPc6	válka
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20.	[number]	k4	20.
století	století	k1gNnSc2	století
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
intervenovaly	intervenovat	k5eAaImAgFnP	intervenovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
zemích	zem	k1gFnPc6	zem
Karibiku	Karibik	k1gInSc2	Karibik
a	a	k8xC	a
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
století	století	k1gNnSc2	století
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
izolacionismus	izolacionismus	k1gInSc4	izolacionismus
a	a	k8xC	a
neangažování	neangažování	k1gNnSc1	neangažování
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
západní	západní	k2eAgFnSc4d1	západní
polokouli	polokoule	k1gFnSc4	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
čas	čas	k1gInSc4	čas
přerušen	přerušen	k2eAgInSc4d1	přerušen
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
USA	USA	kA	USA
zapojily	zapojit	k5eAaPmAgInP	zapojit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
po	po	k7c6	po
potopení	potopení	k1gNnSc6	potopení
britské	britský	k2eAgFnSc2d1	britská
lodě	loď	k1gFnSc2	loď
Lusitania	Lusitanium	k1gNnSc2	Lusitanium
s	s	k7c7	s
Američany	Američan	k1gMnPc7	Američan
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
odmítly	odmítnout	k5eAaPmAgInP	odmítnout
zapojit	zapojit	k5eAaPmF	zapojit
do	do	k7c2	do
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
předchůdkyně	předchůdkyně	k1gFnSc2	předchůdkyně
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
a	a	k8xC	a
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
politice	politika	k1gFnSc6	politika
z	z	k7c2	z
předválečného	předválečný	k2eAgNnSc2d1	předválečné
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
definitivně	definitivně	k6eAd1	definitivně
opuštěna	opustit	k5eAaPmNgFnS	opustit
po	po	k7c6	po
japonském	japonský	k2eAgInSc6d1	japonský
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
Pearl	Pearl	k1gInSc4	Pearl
Harbor	Harbora	k1gFnPc2	Harbora
a	a	k8xC	a
vstupem	vstup	k1gInSc7	vstup
USA	USA	kA	USA
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
konci	konec	k1gInSc6	konec
největšího	veliký	k2eAgInSc2d3	veliký
konfliktu	konflikt	k1gInSc2	konflikt
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
pomáhaly	pomáhat	k5eAaImAgFnP	pomáhat
USA	USA	kA	USA
obnovit	obnovit	k5eAaPmF	obnovit
západní	západní	k2eAgFnSc4d1	západní
Evropu	Evropa	k1gFnSc4	Evropa
(	(	kIx(	(
<g/>
Marshallův	Marshallův	k2eAgInSc1d1	Marshallův
plán	plán	k1gInSc1	plán
<g/>
)	)	kIx)	)
a	a	k8xC	a
snažily	snažit	k5eAaImAgFnP	snažit
se	se	k3xPyFc4	se
zabránit	zabránit	k5eAaPmF	zabránit
celosvětovému	celosvětový	k2eAgInSc3d1	celosvětový
rozmachu	rozmach	k1gInSc3	rozmach
komunismu	komunismus	k1gInSc2	komunismus
(	(	kIx(	(
<g/>
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
Korejská	korejský	k2eAgFnSc1d1	Korejská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
Vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgInSc2	tento
boje	boj	k1gInSc2	boj
podporovaly	podporovat	k5eAaImAgFnP	podporovat
USA	USA	kA	USA
také	také	k9	také
různé	různý	k2eAgFnPc1d1	různá
nedemokratické	demokratický	k2eNgFnPc1d1	nedemokratická
vlády	vláda	k1gFnPc1	vláda
(	(	kIx(	(
<g/>
Pinochet	Pinochet	k1gInSc1	Pinochet
<g/>
,	,	kIx,	,
Suharto	Suharta	k1gFnSc5	Suharta
<g/>
,	,	kIx,	,
Čon	Čon	k1gMnSc1	Čon
Tu-hwan	Tuwan	k1gMnSc1	Tu-hwan
<g/>
,	,	kIx,	,
Perón	perón	k1gInSc1	perón
<g/>
,	,	kIx,	,
Husajn	Husajn	k1gMnSc1	Husajn
<g/>
,	,	kIx,	,
Duvalier	Duvalier	k1gMnSc1	Duvalier
<g/>
,	,	kIx,	,
podpora	podpora	k1gFnSc1	podpora
operace	operace	k1gFnSc2	operace
Kondor	kondor	k1gMnSc1	kondor
<g/>
)	)	kIx)	)
či	či	k8xC	či
se	se	k3xPyFc4	se
nepřímo	přímo	k6eNd1	přímo
podílely	podílet	k5eAaImAgInP	podílet
na	na	k7c4	na
svržení	svržení	k1gNnSc4	svržení
vlád	vláda	k1gFnPc2	vláda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
odporovaly	odporovat	k5eAaImAgInP	odporovat
jejich	jejich	k3xOp3gNnPc3	jejich
zájmům	zájem	k1gInPc3	zájem
(	(	kIx(	(
<g/>
Mosaddek	Mosaddek	k1gMnSc1	Mosaddek
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
<g/>
,	,	kIx,	,
Árbenz	Árbenz	k1gInSc1	Árbenz
v	v	k7c6	v
Guatemale	Guatemala	k1gFnSc6	Guatemala
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
o	o	k7c6	o
jejich	jejich	k3xOp3gNnSc6	jejich
svržení	svržení	k1gNnSc6	svržení
snažily	snažit	k5eAaImAgFnP	snažit
(	(	kIx(	(
<g/>
podpora	podpora	k1gFnSc1	podpora
contras	contrasa	k1gFnPc2	contrasa
v	v	k7c6	v
Nikaragui	Nikaragua	k1gFnSc6	Nikaragua
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
ve	v	k7c6	v
studené	studený	k2eAgFnSc6d1	studená
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
USA	USA	kA	USA
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jedinou	jediný	k2eAgFnSc7d1	jediná
supervelmocí	supervelmoc	k1gFnSc7	supervelmoc
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
jejich	jejich	k3xOp3gFnSc1	jejich
pozice	pozice	k1gFnSc1	pozice
však	však	k9	však
doznává	doznávat	k5eAaImIp3nS	doznávat
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
určitou	určitý	k2eAgFnSc4d1	určitá
proměnu	proměna	k1gFnSc4	proměna
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
komplikovanou	komplikovaný	k2eAgFnSc7d1	komplikovaná
situací	situace	k1gFnSc7	situace
na	na	k7c6	na
Středním	střední	k2eAgInSc6d1	střední
Východě	východ	k1gInSc6	východ
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
s	s	k7c7	s
mocenským	mocenský	k2eAgInSc7d1	mocenský
vzestupem	vzestup	k1gInSc7	vzestup
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
podporovaným	podporovaný	k2eAgInSc7d1	podporovaný
jejím	její	k3xOp3gInSc7	její
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
potenciálem	potenciál	k1gInSc7	potenciál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Některé	některý	k3yIgInPc1	některý
problémy	problém	k1gInPc1	problém
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21.	[number]	k4	21.
století	století	k1gNnSc2	století
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Vlády	vláda	k1gFnPc1	vláda
USA	USA	kA	USA
čelily	čelit	k5eAaImAgFnP	čelit
po	po	k7c6	po
světě	svět	k1gInSc6	svět
antipatiím	antipatie	k1gFnPc3	antipatie
mnoha	mnoho	k4c2	mnoho
občanů	občan	k1gMnPc2	občan
i	i	k8xC	i
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
protestům	protest	k1gInPc3	protest
v	v	k7c4	v
samotných	samotný	k2eAgInPc2d1	samotný
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
metodám	metoda	k1gFnPc3	metoda
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
teroristům	terorista	k1gMnPc3	terorista
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yRgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
zacházení	zacházení	k1gNnSc1	zacházení
s	s	k7c7	s
vězni	vězeň	k1gMnPc7	vězeň
ve	v	k7c6	v
věznicích	věznice	k1gFnPc6	věznice
Guantánamo	Guantánama	k1gFnSc5	Guantánama
a	a	k8xC	a
Abu-Ghrajb	Abu-Ghrajb	k1gInSc4	Abu-Ghrajb
<g/>
,	,	kIx,	,
provozování	provozování	k1gNnSc4	provozování
tajných	tajný	k2eAgFnPc2d1	tajná
věznic	věznice	k1gFnPc2	věznice
CIA	CIA	kA	CIA
mimo	mimo	k7c4	mimo
americké	americký	k2eAgNnSc4d1	americké
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
<g />
.	.	kIx.	.
</s>
<s>
bylo	být	k5eAaImAgNnS	být
povoleno	povolit	k5eAaPmNgNnS	povolit
mučení	mučení	k1gNnSc1	mučení
vězňů	vězeň	k1gMnPc2	vězeň
(	(	kIx(	(
<g/>
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
nebo	nebo	k8xC	nebo
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
masové	masový	k2eAgNnSc1d1	masové
odposlouchávání	odposlouchávání	k1gNnSc1	odposlouchávání
svých	svůj	k3xOyFgMnPc2	svůj
občanů	občan	k1gMnPc2	občan
i	i	k8xC	i
spojenců	spojenec	k1gMnPc2	spojenec
(	(	kIx(	(
<g/>
PATRIOT	patriot	k1gMnSc1	patriot
Act	Act	k1gMnSc1	Act
<g/>
,	,	kIx,	,
Echelon	Echelon	k1gInSc1	Echelon
a	a	k8xC	a
odhalení	odhalení	k1gNnSc1	odhalení
Edwarda	Edward	k1gMnSc2	Edward
Snowdena	Snowden	k1gMnSc2	Snowden
o	o	k7c6	o
činnosti	činnost	k1gFnSc6	činnost
NSA	NSA	kA	NSA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
válce	válka	k1gFnSc3	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
destabilizovala	destabilizovat	k5eAaBmAgFnS	destabilizovat
region	region	k1gInSc4	region
a	a	k8xC	a
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
si	se	k3xPyFc3	se
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
obětí	oběť	k1gFnPc2	oběť
(	(	kIx(	(
<g/>
odhady	odhad	k1gInPc1	odhad
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
115 000	[number]	k4	115 000
do	do	k7c2	do
až	až	k6eAd1	až
461 000	[number]	k4	461 000
mrtvých	mrtvý	k1gMnPc2	mrtvý
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vojenské	vojenský	k2eAgFnSc2d1	vojenská
intervence	intervence	k1gFnSc2	intervence
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
svržení	svržení	k1gNnSc6	svržení
libyjského	libyjský	k2eAgInSc2d1	libyjský
režimu	režim	k1gInSc2	režim
propadla	propadnout	k5eAaPmAgFnS	propadnout
do	do	k7c2	do
chaosu	chaos	k1gInSc2	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Kritizována	kritizován	k2eAgFnSc1d1	kritizována
je	být	k5eAaImIp3nS	být
také	také	k9	také
úzká	úzký	k2eAgFnSc1d1	úzká
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
nedemokratickými	demokratický	k2eNgInPc7d1	nedemokratický
režimy	režim	k1gInPc7	režim
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
a	a	k8xC	a
podpora	podpora	k1gFnSc1	podpora
vojenské	vojenský	k2eAgFnSc2d1	vojenská
intervence	intervence	k1gFnSc2	intervence
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
Íránem	Írán	k1gInSc7	Írán
se	se	k3xPyFc4	se
prudce	prudko	k6eAd1	prudko
zhoršily	zhoršit	k5eAaPmAgInP	zhoršit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
USA	USA	kA	USA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
odstoupily	odstoupit	k5eAaPmAgFnP	odstoupit
od	od	k7c2	od
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
jaderné	jaderný	k2eAgFnSc2d1	jaderná
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
Íránem	Írán	k1gInSc7	Írán
a	a	k8xC	a
zpřísnily	zpřísnit	k5eAaPmAgFnP	zpřísnit
protiíránské	protiíránský	k2eAgFnPc4d1	protiíránská
sankce	sankce	k1gFnPc4	sankce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Americko-čínské	americko-čínský	k2eAgInPc1d1	americko-čínský
vztahy	vztah	k1gInPc1	vztah
====	====	k?	====
</s>
</p>
<p>
<s>
Stav	stav	k1gInSc1	stav
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
USA	USA	kA	USA
a	a	k8xC	a
Čínou	Čína	k1gFnSc7	Čína
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
parametrů	parametr	k1gInPc2	parametr
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
vztahy	vztah	k1gInPc1	vztah
se	se	k3xPyFc4	se
zlepšily	zlepšit	k5eAaPmAgInP	zlepšit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
prezidentství	prezidentství	k1gNnPc2	prezidentství
Richarda	Richard	k1gMnSc2	Richard
Nixona	Nixon	k1gMnSc2	Nixon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
provedl	provést	k5eAaPmAgMnS	provést
zásadní	zásadní	k2eAgInSc4d1	zásadní
obrat	obrat	k1gInSc4	obrat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
uznal	uznat	k5eAaPmAgMnS	uznat
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
jako	jako	k8xC	jako
legitimní	legitimní	k2eAgFnSc4d1	legitimní
vládu	vláda	k1gFnSc4	vláda
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
uznání	uznání	k1gNnSc3	uznání
tzv.	tzv.	kA	tzv.
politiky	politika	k1gFnSc2	politika
jedné	jeden	k4xCgFnSc2	jeden
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
byl	být	k5eAaImAgInS	být
nucen	nutit	k5eAaImNgMnS	nutit
postoupit	postoupit	k5eAaPmF	postoupit
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
OSN	OSN	kA	OSN
Pekingu	Peking	k1gInSc6	Peking
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
Čína	Čína	k1gFnSc1	Čína
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
americké	americký	k2eAgFnPc4d1	americká
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
zasíláním	zasílání	k1gNnSc7	zasílání
finančních	finanční	k2eAgInPc2d1	finanční
příspěvků	příspěvek	k1gInPc2	příspěvek
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
kampaně	kampaň	k1gFnSc2	kampaň
Billa	Bill	k1gMnSc2	Bill
Clintona	Clinton	k1gMnSc2	Clinton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
Čína	Čína	k1gFnSc1	Čína
jako	jako	k8xS	jako
vlastník	vlastník	k1gMnSc1	vlastník
amerických	americký	k2eAgInPc2d1	americký
státních	státní	k2eAgInPc2d1	státní
dluhopisů	dluhopis	k1gInPc2	dluhopis
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
1,244	[number]	k4	1,244
bilionu	bilion	k4xCgInSc2	bilion
USD	USD	kA	USD
největším	veliký	k2eAgMnSc7d3	veliký
zahraničním	zahraniční	k2eAgMnSc7d1	zahraniční
věřitelem	věřitel	k1gMnSc7	věřitel
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
prezidentů	prezident	k1gMnPc2	prezident
George	Georg	k1gFnSc2	Georg
W.	W.	kA	W.
Bushe	Bush	k1gMnSc4	Bush
a	a	k8xC	a
Baracka	Baracka	k1gFnSc1	Baracka
Obamy	Obama	k1gFnSc2	Obama
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
projevovat	projevovat	k5eAaImF	projevovat
rozpory	rozpor	k1gInPc4	rozpor
zapříčiněné	zapříčiněný	k2eAgNnSc1d1	zapříčiněné
mocenským	mocenský	k2eAgInSc7d1	mocenský
vzestupem	vzestup	k1gInSc7	vzestup
Číny	Čína	k1gFnSc2	Čína
jako	jako	k8xS	jako
vážného	vážný	k1gMnSc2	vážný
hráče	hráč	k1gMnSc2	hráč
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
však	však	k9	však
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
Čínou	Čína	k1gFnSc7	Čína
a	a	k8xC	a
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
vystupňovala	vystupňovat	k5eAaPmAgFnS	vystupňovat
řada	řada	k1gFnSc1	řada
incidentů	incident	k1gInPc2	incident
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
hackerské	hackerský	k2eAgInPc4d1	hackerský
útoky	útok	k1gInPc4	útok
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
krádeže	krádež	k1gFnSc2	krádež
osobních	osobní	k2eAgInPc2d1	osobní
údajů	údaj	k1gInPc2	údaj
21,5	[number]	k4	21,5
milionu	milion	k4xCgInSc2	milion
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
amerických	americký	k2eAgInPc2d1	americký
federálních	federální	k2eAgInPc2d1	federální
úřadů	úřad	k1gInPc2	úřad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
budování	budování	k1gNnSc1	budování
umělých	umělý	k2eAgInPc2d1	umělý
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Jihočínském	jihočínský	k2eAgNnSc6d1	Jihočínské
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Ambice	ambice	k1gFnPc1	ambice
Číny	Čína	k1gFnSc2	Čína
jsou	být	k5eAaImIp3nP	být
umocněny	umocněn	k2eAgInPc1d1	umocněn
její	její	k3xOp3gFnSc7	její
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
sílou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
mj.	mj.	kA	mj.
v	v	k7c6	v
obrovských	obrovský	k2eAgInPc6d1	obrovský
každoročních	každoroční	k2eAgInPc6d1	každoroční
přebytcích	přebytek	k1gInPc6	přebytek
Číny	Čína	k1gFnSc2	Čína
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
obchodu	obchod	k1gInSc6	obchod
s	s	k7c7	s
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
názoru	názor	k1gInSc2	názor
amerických	americký	k2eAgMnPc2d1	americký
analytiků	analytik	k1gMnPc2	analytik
provádí	provádět	k5eAaImIp3nS	provádět
Čína	Čína	k1gFnSc1	Čína
protekcionistickou	protekcionistický	k2eAgFnSc4d1	protekcionistická
obchodní	obchodní	k2eAgFnSc4d1	obchodní
politiku	politika	k1gFnSc4	politika
pomocí	pomocí	k7c2	pomocí
uměle	uměle	k6eAd1	uměle
udržovaného	udržovaný	k2eAgInSc2d1	udržovaný
nízkého	nízký	k2eAgInSc2d1	nízký
směnného	směnný	k2eAgInSc2d1	směnný
kurzu	kurz	k1gInSc2	kurz
čínské	čínský	k2eAgFnSc2d1	čínská
měny	měna	k1gFnSc2	měna
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
mnohem	mnohem	k6eAd1	mnohem
nížších	nížší	k2eAgFnPc2d1	nížší
mezd	mzda	k1gFnPc2	mzda
pracovníků	pracovník	k1gMnPc2	pracovník
v	v	k7c6	v
čínském	čínský	k2eAgInSc6d1	čínský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
může	moct	k5eAaImIp3nS	moct
nabízet	nabízet	k5eAaImF	nabízet
své	svůj	k3xOyFgNnSc4	svůj
zboží	zboží	k1gNnSc4	zboží
v	v	k7c6	v
USA	USA	kA	USA
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnPc4d1	nízká
ceny	cena	k1gFnPc4	cena
a	a	k8xC	a
vytlačovat	vytlačovat	k5eAaImF	vytlačovat
tak	tak	k9	tak
domácí	domácí	k2eAgMnPc4d1	domácí
výrobce	výrobce	k1gMnPc4	výrobce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
čínský	čínský	k2eAgInSc1d1	čínský
přebytek	přebytek	k1gInSc1	přebytek
v	v	k7c6	v
obchodu	obchod	k1gInSc6	obchod
s	s	k7c7	s
USA	USA	kA	USA
375	[number]	k4	375
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Donalda	Donald	k1gMnSc2	Donald
Trumpa	Trump	k1gMnSc2	Trump
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2017	[number]	k4	2017
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
přeměnila	přeměnit	k5eAaPmAgFnS	přeměnit
v	v	k7c4	v
konflikt	konflikt	k1gInSc4	konflikt
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
USA	USA	kA	USA
již	již	k9	již
zavedly	zavést	k5eAaPmAgFnP	zavést
zvýšené	zvýšený	k2eAgFnPc1d1	zvýšená
celní	celní	k2eAgFnPc1d1	celní
sazby	sazba	k1gFnPc1	sazba
na	na	k7c4	na
dovoz	dovoz	k1gInSc4	dovoz
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
hliníku	hliník	k1gInSc2	hliník
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nedojde	dojít	k5eNaPmIp3nS	dojít
ke	k	k7c3	k
kompromisu	kompromis	k1gInSc3	kompromis
<g/>
,	,	kIx,	,
hrozí	hrozit	k5eAaImIp3nS	hrozit
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
stupňující	stupňující	k2eAgFnSc2d1	stupňující
se	se	k3xPyFc4	se
obchodní	obchodní	k2eAgFnSc2d1	obchodní
války	válka	k1gFnSc2	válka
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
velmocemi	velmoc	k1gFnPc7	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Ohlášené	ohlášený	k2eAgInPc1d1	ohlášený
eventuální	eventuální	k2eAgInPc1d1	eventuální
odvetné	odvetný	k2eAgInPc1d1	odvetný
kroky	krok	k1gInPc1	krok
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
by	by	kYmCp3nS	by
zavedla	zavést	k5eAaPmAgFnS	zavést
vysoká	vysoká	k1gFnSc1	vysoká
cla	clo	k1gNnSc2	clo
na	na	k7c4	na
128	[number]	k4	128
druhů	druh	k1gInPc2	druh
zboží	zboží	k1gNnSc2	zboží
dováženého	dovážený	k2eAgInSc2d1	dovážený
z	z	k7c2	z
USA	USA	kA	USA
<g/>
,	,	kIx,	,
neudělaly	udělat	k5eNaPmAgInP	udělat
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
velký	velký	k2eAgInSc4d1	velký
dojem	dojem	k1gInSc4	dojem
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pozorovatelů	pozorovatel	k1gMnPc2	pozorovatel
nejde	jít	k5eNaImIp3nS	jít
prezidentu	prezident	k1gMnSc3	prezident
Trumpovi	Trump	k1gMnSc3	Trump
primárně	primárně	k6eAd1	primárně
o	o	k7c4	o
přístup	přístup	k1gInSc4	přístup
na	na	k7c4	na
čínské	čínský	k2eAgInPc4d1	čínský
trhy	trh	k1gInPc4	trh
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
platební	platební	k2eAgFnSc2d1	platební
bilance	bilance	k1gFnSc2	bilance
USA	USA	kA	USA
a	a	k8xC	a
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
celkově	celkově	k6eAd1	celkově
zbrzděn	zbrzdit	k5eAaPmNgInS	zbrzdit
mocenský	mocenský	k2eAgInSc1d1	mocenský
vzestup	vzestup	k1gInSc1	vzestup
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Amerika	Amerika	k1gFnSc1	Amerika
hrozí	hrozit	k5eAaImIp3nS	hrozit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
zavedla	zavést	k5eAaPmAgNnP	zavést
trestná	trestný	k2eAgNnPc1d1	trestné
cla	clo	k1gNnPc1	clo
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
čínské	čínský	k2eAgInPc4d1	čínský
dovozy	dovoz	k1gInPc4	dovoz
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
150	[number]	k4	150
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
a	a	k8xC	a
Čína	Čína	k1gFnSc1	Čína
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
takové	takový	k3xDgInPc4	takový
obchodní	obchodní	k2eAgInPc4d1	obchodní
válce	válec	k1gInPc4	válec
zabránila	zabránit	k5eAaPmAgFnS	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
čínských	čínský	k2eAgMnPc2d1	čínský
odborníků	odborník	k1gMnPc2	odborník
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
jejich	jejich	k3xOp3gFnSc3	jejich
zemi	zem	k1gFnSc3	zem
silněji	silně	k6eAd2	silně
než	než	k8xS	než
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vedou	vést	k5eAaImIp3nP	vést
intenzivní	intenzivní	k2eAgNnPc1d1	intenzivní
jednání	jednání	k1gNnPc1	jednání
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Pekingu	Peking	k1gInSc2	Peking
přiletěla	přiletět	k5eAaPmAgFnS	přiletět
2.	[number]	k4	2.
května	květen	k1gInSc2	květen
2018	[number]	k4	2018
americká	americký	k2eAgFnSc1d1	americká
delegace	delegace	k1gFnSc1	delegace
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
patřili	patřit	k5eAaImAgMnP	patřit
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Steven	Stevna	k1gFnPc2	Stevna
Mnuchin	Mnuchin	k1gInSc1	Mnuchin
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
obchodu	obchod	k1gInSc2	obchod
Wilbur	Wilbura	k1gFnPc2	Wilbura
Ross	Ross	k1gInSc1	Ross
a	a	k8xC	a
pověřenec	pověřenec	k1gMnSc1	pověřenec
pro	pro	k7c4	pro
obchodní	obchodní	k2eAgFnPc4d1	obchodní
záležitosti	záležitost	k1gFnPc4	záležitost
Robert	Robert	k1gMnSc1	Robert
Lighthizer	Lighthizer	k1gMnSc1	Lighthizer
<g/>
.	.	kIx.	.
</s>
<s>
Informované	informovaný	k2eAgInPc1d1	informovaný
kruhy	kruh	k1gInPc1	kruh
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
byly	být	k5eAaImAgInP	být
toho	ten	k3xDgInSc2	ten
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
cílem	cíl	k1gInSc7	cíl
této	tento	k3xDgFnSc2	tento
delegace	delegace	k1gFnSc2	delegace
nebylo	být	k5eNaImAgNnS	být
ihned	ihned	k6eAd1	ihned
ukončit	ukončit	k5eAaPmF	ukončit
spor	spor	k1gInSc4	spor
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
o	o	k7c4	o
obchodní	obchodní	k2eAgFnPc4d1	obchodní
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
organizace	organizace	k1gFnSc1	organizace
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Vládní	vládní	k2eAgFnSc1d1	vládní
organizace	organizace	k1gFnSc1	organizace
====	====	k?	====
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
–	–	k?	–
Central	Central	k1gMnSc1	Central
Intelligence	Intelligenec	k1gMnSc2	Intelligenec
Agency	Agenca	k1gFnSc2	Agenca
–	–	k?	–
rozvědka	rozvědka	k1gFnSc1	rozvědka
pro	pro	k7c4	pro
zahraničí	zahraničí	k1gNnSc4	zahraničí
</s>
</p>
<p>
<s>
FBI	FBI	kA	FBI
–	–	k?	–
Federal	Federal	k1gMnSc1	Federal
Bureau	Bureaus	k1gInSc2	Bureaus
of	of	k?	of
Investigation	Investigation	k1gInSc1	Investigation
–	–	k?	–
kontrarozvědka	kontrarozvědka	k1gFnSc1	kontrarozvědka
a	a	k8xC	a
federální	federální	k2eAgInSc1d1	federální
vyšetřovací	vyšetřovací	k2eAgInSc1d1	vyšetřovací
úřad	úřad	k1gInSc1	úřad
</s>
</p>
<p>
<s>
NASA	NASA	kA	NASA
–	–	k?	–
National	National	k1gFnSc2	National
Aeronautic	Aeronautice	k1gFnPc2	Aeronautice
and	and	k?	and
Space	Space	k1gMnSc1	Space
Agency	Agenca	k1gFnSc2	Agenca
–	–	k?	–
vládní	vládní	k2eAgFnSc1d1	vládní
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
kosmonautiku	kosmonautika	k1gFnSc4	kosmonautika
</s>
</p>
<p>
<s>
NSA	NSA	kA	NSA
–	–	k?	–
National	National	k1gFnSc2	National
Security	Securita	k1gFnSc2	Securita
Agency	Agenca	k1gFnSc2	Agenca
–	–	k?	–
Národní	národní	k2eAgFnSc1d1	národní
bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
agentura	agentura	k1gFnSc1	agentura
</s>
</p>
<p>
<s>
NCIS	NCIS	kA	NCIS
–	–	k?	–
Naval	navalit	k5eAaPmRp2nS	navalit
Criminal	Criminal	k1gMnSc1	Criminal
Investigative	Investigativ	k1gInSc5	Investigativ
Service	Service	k1gFnSc1	Service
–	–	k?	–
Námořní	námořní	k2eAgFnSc1d1	námořní
kriminální	kriminální	k2eAgFnSc1d1	kriminální
vyšetřovací	vyšetřovací	k2eAgFnSc1d1	vyšetřovací
služba	služba	k1gFnSc1	služba
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Veřejné	veřejný	k2eAgFnPc1d1	veřejná
organizace	organizace	k1gFnPc1	organizace
====	====	k?	====
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
střelecká	střelecký	k2eAgFnSc1d1	střelecká
asociace	asociace	k1gFnSc1	asociace
–	–	k?	–
National	National	k1gFnSc1	National
Rifle	rifle	k1gFnPc4	rifle
Association	Association	k1gInSc1	Association
(	(	kIx(	(
<g/>
NRA	NRA	kA	NRA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
pěti	pět	k4xCc2	pět
složek	složka	k1gFnPc2	složka
<g/>
:	:	kIx,	:
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
letectva	letectvo	k1gNnSc2	letectvo
<g/>
,	,	kIx,	,
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
,	,	kIx,	,
námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
a	a	k8xC	a
pobřežní	pobřežní	k2eAgFnSc2d1	pobřežní
stráže	stráž	k1gFnSc2	stráž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aktivní	aktivní	k2eAgFnSc6d1	aktivní
službě	služba	k1gFnSc6	služba
je	být	k5eAaImIp3nS	být
1 426 026	[number]	k4	1 426 026
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
výzbroj	výzbroj	k1gFnSc4	výzbroj
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
letadlové	letadlový	k2eAgFnPc4d1	letadlová
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
jaderné	jaderný	k2eAgFnPc4d1	jaderná
ponorky	ponorka	k1gFnPc4	ponorka
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
neviditelná	viditelný	k2eNgFnSc1d1	neviditelná
<g/>
"	"	kIx"	"
letadla	letadlo	k1gNnPc1	letadlo
či	či	k8xC	či
tanky	tank	k1gInPc1	tank
Abrams	Abramsa	k1gFnPc2	Abramsa
<g/>
.	.	kIx.	.
</s>
<s>
Pokročilými	pokročilý	k2eAgFnPc7d1	pokročilá
technologiemi	technologie	k1gFnPc7	technologie
disponují	disponovat	k5eAaBmIp3nP	disponovat
i	i	k8xC	i
výstroje	výstroj	k1gFnSc2	výstroj
a	a	k8xC	a
vybavení	vybavení	k1gNnSc2	vybavení
pěších	pěší	k2eAgFnPc2d1	pěší
a	a	k8xC	a
speciálních	speciální	k2eAgFnPc2d1	speciální
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zapojila	zapojit	k5eAaPmAgFnS	zapojit
se	se	k3xPyFc4	se
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
hlavních	hlavní	k2eAgInPc2d1	hlavní
konfliktů	konflikt	k1gInPc2	konflikt
20.	[number]	k4	20.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
pomohla	pomoct	k5eAaPmAgFnS	pomoct
porazit	porazit	k5eAaPmF	porazit
císařské	císařský	k2eAgInPc4d1	císařský
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
nacistické	nacistický	k2eAgNnSc4d1	nacistické
Německo	Německo	k1gNnSc4	Německo
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
světových	světový	k2eAgFnPc6d1	světová
válkách	válka	k1gFnPc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
mají	mít	k5eAaImIp3nP	mít
USA	USA	kA	USA
nejlépe	dobře	k6eAd3	dobře
vybavenou	vybavený	k2eAgFnSc4d1	vybavená
a	a	k8xC	a
nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
armádu	armáda	k1gFnSc4	armáda
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
vysokými	vysoký	k2eAgInPc7d1	vysoký
výdaji	výdaj	k1gInPc7	výdaj
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
3/4	[number]	k4	3/4
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
zbytek	zbytek	k1gInSc1	zbytek
světa	svět	k1gInSc2	svět
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2011	[number]	k4	2011
–	–	k?	–
698	[number]	k4	698
mld	mld	k?	mld
<g/>
.	.	kIx.	.
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozvojem	rozvoj	k1gInSc7	rozvoj
moderních	moderní	k2eAgFnPc2d1	moderní
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
jakými	jaký	k3yQgFnPc7	jaký
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
technologie	technologie	k1gFnPc1	technologie
stealth	stealtha	k1gFnPc2	stealtha
nebo	nebo	k8xC	nebo
mikrovlnné	mikrovlnný	k2eAgFnSc2d1	mikrovlnná
<g/>
,	,	kIx,	,
sonické	sonický	k2eAgFnSc2d1	sonická
a	a	k8xC	a
ULF	ULF	kA	ULF
(	(	kIx(	(
<g/>
Ultra-Low	Ultra-Low	k1gMnSc2	Ultra-Low
Frequency	Frequenca	k1gMnSc2	Frequenca
<g/>
)	)	kIx)	)
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
úspěšně	úspěšně	k6eAd1	úspěšně
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
přední	přední	k2eAgFnPc4d1	přední
americké	americký	k2eAgFnPc4d1	americká
korporace	korporace	k1gFnPc4	korporace
<g/>
,	,	kIx,	,
a	a	k8xC	a
též	též	k9	též
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
munice	munice	k1gFnSc2	munice
z	z	k7c2	z
ochuzeného	ochuzený	k2eAgInSc2d1	ochuzený
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
má	mít	k5eAaImIp3nS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
bojeschopnost	bojeschopnost	k1gFnSc4	bojeschopnost
díky	díky	k7c3	díky
dlouholetým	dlouholetý	k2eAgFnPc3d1	dlouholetá
zkušenostem	zkušenost	k1gFnPc3	zkušenost
<g/>
;	;	kIx,	;
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
20.	[number]	k4	20.
století	století	k1gNnPc2	století
se	se	k3xPyFc4	se
účastnila	účastnit	k5eAaImAgFnS	účastnit
přibližně	přibližně	k6eAd1	přibližně
stovky	stovka	k1gFnPc4	stovka
ozbrojených	ozbrojený	k2eAgMnPc2d1	ozbrojený
konfliktů	konflikt	k1gInPc2	konflikt
nebo	nebo	k8xC	nebo
intervencí	intervence	k1gFnPc2	intervence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
operuje	operovat	k5eAaImIp3nS	operovat
například	například	k6eAd1	například
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
operace	operace	k1gFnSc2	operace
Operace	operace	k1gFnSc1	operace
Trvalá	trvalá	k1gFnSc1	trvalá
svoboda	svoboda	k1gFnSc1	svoboda
a	a	k8xC	a
obecnějšího	obecní	k2eAgInSc2d2	obecní
konceptu	koncept	k1gInSc2	koncept
tzv.	tzv.	kA	tzv.
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
mají	mít	k5eAaImIp3nP	mít
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
823	[number]	k4	823
vojenských	vojenský	k2eAgFnPc2d1	vojenská
základen	základna	k1gFnPc2	základna
<g/>
,	,	kIx,	,
rozmístěných	rozmístěný	k2eAgFnPc2d1	rozmístěná
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
130	[number]	k4	130
ze	z	k7c2	z
195	[number]	k4	195
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
2.	[number]	k4	2.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
od	od	k7c2	od
Korejské	korejský	k2eAgFnSc2d1	Korejská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
vojenských	vojenský	k2eAgFnPc2d1	vojenská
aliancí	aliance	k1gFnPc2	aliance
patří	patřit	k5eAaImIp3nS	patřit
USA	USA	kA	USA
k	k	k7c3	k
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
jsou	být	k5eAaImIp3nP	být
nejsilnějším	silný	k2eAgInSc7d3	nejsilnější
členem	člen	k1gInSc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
USA	USA	kA	USA
jsou	být	k5eAaImIp3nP	být
subjektem	subjekt	k1gInSc7	subjekt
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
dopadem	dopad	k1gInSc7	dopad
na	na	k7c4	na
znečištění	znečištění	k1gNnSc4	znečištění
ovzduší	ovzduší	k1gNnSc2	ovzduší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
též	též	k9	též
největší	veliký	k2eAgFnSc7d3	veliký
spotřebou	spotřeba	k1gFnSc7	spotřeba
ropy	ropa	k1gFnSc2	ropa
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
CIA	CIA	kA	CIA
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
320 000	[number]	k4	320 000
barelů	barel	k1gInPc2	barel
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
<g/>
×	×	k?	×
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
době	doba	k1gFnSc6	doba
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
emise	emise	k1gFnPc1	emise
navíc	navíc	k6eAd1	navíc
nejsou	být	k5eNaImIp3nP	být
připočítávány	připočítáván	k2eAgFnPc1d1	připočítávána
k	k	k7c3	k
emisím	emise	k1gFnPc3	emise
USA	USA	kA	USA
ani	ani	k8xC	ani
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
území	území	k1gNnSc6	území
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc4	síla
operují	operovat	k5eAaImIp3nP	operovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
operují	operovat	k5eAaImIp3nP	operovat
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
vojensky	vojensky	k6eAd1	vojensky
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
stovce	stovka	k1gFnSc6	stovka
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc4d1	administrativní
členění	členění	k1gNnSc4	členění
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
50	[number]	k4	50
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
state	status	k1gInSc5	status
/	/	kIx~	/
mn	mn	k?	mn
<g/>
.	.	kIx.	.
č	č	k0	č
<g/>
.	.	kIx.	.
states	statesa	k1gFnPc2	statesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgInSc2	jeden
federálního	federální	k2eAgInSc2d1	federální
distriktu	distrikt	k1gInSc2	distrikt
–	–	k?	–
District	District	k1gMnSc1	District
of	of	k?	of
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
leží	ležet	k5eAaImIp3nS	ležet
federální	federální	k2eAgNnSc1d1	federální
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc1	D.C.
<g/>
,	,	kIx,	,
spadá	spadat	k5eAaImIp3nS	spadat
přímo	přímo	k6eAd1	přímo
pod	pod	k7c4	pod
jurisdikci	jurisdikce	k1gFnSc4	jurisdikce
Kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
nespadá	spadat	k5eNaPmIp3nS	spadat
pod	pod	k7c4	pod
žádný	žádný	k3yNgInSc4	žádný
stát	stát	k1gInSc4	stát
a	a	k8xC	a
oficiálně	oficiálně	k6eAd1	oficiálně
není	být	k5eNaImIp3nS	být
státem	stát	k1gInSc7	stát
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
uváděn	uváděn	k2eAgInSc1d1	uváděn
<g/>
)	)	kIx)	)
–	–	k?	–
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
nezačleněných	začleněný	k2eNgNnPc2d1	začleněný
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ostrovních	ostrovní	k2eAgFnPc2d1	ostrovní
teritorií	teritorium	k1gNnPc2	teritorium
Portoriko	Portoriko	k1gNnSc4	Portoriko
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Mariany	Mariana	k1gFnSc2	Mariana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
podepsání	podepsání	k1gNnSc6	podepsání
Deklarace	deklarace	k1gFnSc2	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
Unie	unie	k1gFnSc1	unie
skládala	skládat	k5eAaImAgFnS	skládat
ze	z	k7c2	z
13	[number]	k4	13
zakládajících	zakládající	k2eAgInPc2d1	zakládající
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
britskými	britský	k2eAgFnPc7d1	britská
koloniemi	kolonie	k1gFnPc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
rozrostl	rozrůst	k5eAaPmAgInS	rozrůst
při	při	k7c6	při
expanzi	expanze	k1gFnSc6	expanze
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
dobytím	dobytí	k1gNnSc7	dobytí
či	či	k8xC	či
nákupem	nákup	k1gInSc7	nákup
nových	nový	k2eAgNnPc2d1	nové
území	území	k1gNnPc2	území
americkou	americký	k2eAgFnSc7d1	americká
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
dělením	dělení	k1gNnSc7	dělení
existujících	existující	k2eAgInPc2d1	existující
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
Západní	západní	k2eAgFnPc1d1	západní
Virginie	Virginie	k1gFnPc1	Virginie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Města	město	k1gNnSc2	město
a	a	k8xC	a
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
83	[number]	k4	83
%	%	kIx~	%
Američanů	Američan	k1gMnPc2	Američan
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
361	[number]	k4	361
metropolitních	metropolitní	k2eAgFnPc2d1	metropolitní
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
254	[number]	k4	254
měst	město	k1gNnPc2	město
s	s	k7c7	s
populací	populace	k1gFnSc7	populace
větší	veliký	k2eAgFnSc7d2	veliký
než	než	k8xS	než
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
,	,	kIx,	,
25	[number]	k4	25
měst	město	k1gNnPc2	město
mělo	mít	k5eAaImAgNnS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
milion	milion	k4xCgInSc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
4	[number]	k4	4
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
<g/>
,	,	kIx,	,
Chicago	Chicago	k1gNnSc1	Chicago
a	a	k8xC	a
Houston	Houston	k1gInSc1	Houston
<g/>
)	)	kIx)	)
s	s	k7c7	s
populací	populace	k1gFnSc7	populace
větší	veliký	k2eAgFnSc7d2	veliký
než	než	k8xS	než
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
50	[number]	k4	50
metropolitních	metropolitní	k2eAgFnPc2d1	metropolitní
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
přes	přes	k7c4	přes
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejrychleji	rychle	k6eAd3	rychle
rostoucí	rostoucí	k2eAgFnPc4d1	rostoucí
aglomerace	aglomerace	k1gFnPc4	aglomerace
patří	patřit	k5eAaImIp3nS	patřit
Dallas	Dallas	k1gInSc1	Dallas
<g/>
,	,	kIx,	,
Houston	Houston	k1gInSc1	Houston
<g/>
,	,	kIx,	,
Atlanta	Atlanta	k1gFnSc1	Atlanta
a	a	k8xC	a
Phoenix	Phoenix	k1gInSc1	Phoenix
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
jsou	být	k5eAaImIp3nP	být
třetí	třetí	k4xOgFnSc7	třetí
nejlidnatější	lidnatý	k2eAgFnSc7d3	nejlidnatější
zemí	zem	k1gFnSc7	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17.	[number]	k4	17.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
překročily	překročit	k5eAaPmAgFnP	překročit
hranici	hranice	k1gFnSc4	hranice
300	[number]	k4	300
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
ilegálních	ilegální	k2eAgMnPc2d1	ilegální
imigrantů	imigrant	k1gMnPc2	imigrant
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
12	[number]	k4	12
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Populační	populační	k2eAgInSc1d1	populační
růst	růst	k1gInSc1	růst
je	být	k5eAaImIp3nS	být
0,97	[number]	k4	0,97
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
vysoké	vysoký	k2eAgNnSc1d1	vysoké
číslo	číslo	k1gNnSc1	číslo
např.	např.	kA	např.
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
přírůstek	přírůstek	k1gInSc1	přírůstek
pouze	pouze	k6eAd1	pouze
0,16	[number]	k4	0,16
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
USA	USA	kA	USA
zemí	zem	k1gFnPc2	zem
prvního	první	k4xOgInSc2	první
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
relativně	relativně	k6eAd1	relativně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
i	i	k8xC	i
dětská	dětský	k2eAgFnSc1d1	dětská
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
<g/>
14	[number]	k4	14
ppm	ppm	k?	ppm
<g/>
)	)	kIx)	)
i	i	k9	i
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
(	(	kIx(	(
<g/>
47	[number]	k4	47
milionů	milion	k4xCgInPc2	milion
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
15,15	[number]	k4	15,15
%	%	kIx~	%
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
<g/>
245	[number]	k4	245
milionu	milion	k4xCgInSc2	milion
neboli	neboli	k8xC	neboli
751	[number]	k4	751
na	na	k7c4	na
100 000	[number]	k4	100 000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
tj.	tj.	kA	tj.
asi	asi	k9	asi
7,5	[number]	k4	7,5
<g/>
×	×	k?	×
více	hodně	k6eAd2	hodně
než	než	k8xS	než
průměr	průměr	k1gInSc1	průměr
EU	EU	kA	EU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
USA	USA	kA	USA
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
rozmanitou	rozmanitý	k2eAgFnSc4d1	rozmanitá
populaci	populace	k1gFnSc4	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
31	[number]	k4	31
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
větším	většit	k5eAaImIp1nS	většit
než	než	k8xS	než
jeden	jeden	k4xCgMnSc1	jeden
milion	milion	k4xCgInSc1	milion
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
imigrací	imigrace	k1gFnSc7	imigrace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
provázela	provázet	k5eAaImAgFnS	provázet
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
po	po	k7c4	po
celou	celá	k1gFnSc4	celá
jejich	jejich	k3xOp3gFnSc4	jejich
historii	historie	k1gFnSc4	historie
–	–	k?	–
a	a	k8xC	a
provází	provázet	k5eAaImIp3nS	provázet
je	on	k3xPp3gInPc4	on
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
17.	[number]	k4	17.
století	století	k1gNnPc2	století
se	se	k3xPyFc4	se
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
stěhovali	stěhovat	k5eAaImAgMnP	stěhovat
hlavně	hlavně	k9	hlavně
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
několika	několik	k4yIc6	několik
dekádách	dekáda	k1gFnPc6	dekáda
imigranty	imigrant	k1gMnPc7	imigrant
představují	představovat	k5eAaImIp3nP	představovat
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
z	z	k7c2	z
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
Kuby	Kuba	k1gFnSc2	Kuba
a	a	k8xC	a
Portorika	Portorico	k1gNnSc2	Portorico
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
do	do	k7c2	do
USA	USA	kA	USA
přistěhuje	přistěhovat	k5eAaPmIp3nS	přistěhovat
okolo	okolo	k7c2	okolo
jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
legálních	legální	k2eAgMnPc2d1	legální
imigrantů	imigrant	k1gMnPc2	imigrant
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Etnické	etnický	k2eAgNnSc4d1	etnické
složení	složení	k1gNnSc4	složení
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
tvoří	tvořit	k5eAaImIp3nS	tvořit
74,7	[number]	k4	74,7
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
běloši	běloch	k1gMnPc1	běloch
<g/>
,	,	kIx,	,
12,1	[number]	k4	12,1
%	%	kIx~	%
černoši	černoch	k1gMnPc1	černoch
<g/>
/	/	kIx~	/
<g/>
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
<g/>
,	,	kIx,	,
4,3	[number]	k4	4,3
%	%	kIx~	%
Asiaté	Asiat	k1gMnPc1	Asiat
<g/>
,	,	kIx,	,
Indiáni	Indián	k1gMnPc1	Indián
a	a	k8xC	a
Eskymáci	Eskymák	k1gMnPc1	Eskymák
pak	pak	k6eAd1	pak
0,8	[number]	k4	0,8
%	%	kIx~	%
a	a	k8xC	a
jiné	jiné	k1gNnSc4	jiné
ras	ras	k1gMnSc1	ras
celkem	celek	k1gInSc7	celek
7,9	[number]	k4	7,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
číslech	číslo	k1gNnPc6	číslo
je	být	k5eAaImIp3nS	být
zahrnuto	zahrnut	k2eAgNnSc1d1	zahrnuto
14,5	[number]	k4	14,5
%	%	kIx~	%
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
za	za	k7c4	za
Hispánce	Hispánec	k1gMnPc4	Hispánec
nebo	nebo	k8xC	nebo
Latinoameričany	Latinoameričan	k1gMnPc4	Latinoameričan
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
otázka	otázka	k1gFnSc1	otázka
rasy	rasa	k1gFnSc2	rasa
jedna	jeden	k4xCgFnSc1	jeden
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
otázka	otázka	k1gFnSc1	otázka
hispánského	hispánský	k2eAgInSc2d1	hispánský
původu	původ	k1gInSc2	původ
věc	věc	k1gFnSc1	věc
druhá	druhý	k4xOgFnSc1	druhý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejrychleji	rychle	k6eAd3	rychle
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgInSc2d1	hispánský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
tvořili	tvořit	k5eAaImAgMnP	tvořit
10	[number]	k4	10
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
12,5	[number]	k4	12,5
%	%	kIx~	%
a	a	k8xC	a
podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005 14,5	[number]	k4	2005 14,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
růst	růst	k1gInSc1	růst
je	být	k5eAaImIp3nS	být
daný	daný	k2eAgInSc1d1	daný
jednak	jednak	k8xC	jednak
imigrací	imigrace	k1gFnPc2	imigrace
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
vysokou	vysoký	k2eAgFnSc7d1	vysoká
porodností	porodnost	k1gFnSc7	porodnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
Arizoně	Arizona	k1gFnSc6	Arizona
<g/>
,	,	kIx,	,
Novém	nový	k2eAgNnSc6d1	nové
Mexiku	Mexiko	k1gNnSc6	Mexiko
a	a	k8xC	a
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
brzy	brzy	k6eAd1	brzy
stanou	stanout	k5eAaPmIp3nP	stanout
většinou	většina	k1gFnSc7	většina
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
podíl	podíl	k1gInSc1	podíl
Hispánců	Hispánec	k1gMnPc2	Hispánec
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Mexiku	Mexiko	k1gNnSc6	Mexiko
–	–	k?	–
43,6	[number]	k4	43,6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Mexičané	Mexičan	k1gMnPc1	Mexičan
obývají	obývat	k5eAaImIp3nP	obývat
především	především	k9	především
jihozápad	jihozápad	k1gInSc4	jihozápad
a	a	k8xC	a
západ	západ	k1gInSc4	západ
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Portoričané	Portoričan	k1gMnPc1	Portoričan
severovýchod	severovýchod	k1gInSc4	severovýchod
a	a	k8xC	a
Kubánci	Kubánec	k1gMnPc1	Kubánec
Floridu	Florida	k1gFnSc4	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Nehispánští	hispánský	k2eNgMnPc1d1	hispánský
běloši	běloch	k1gMnPc1	běloch
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
evropského	evropský	k2eAgInSc2d1	evropský
původu	původ	k1gInSc2	původ
<g/>
)	)	kIx)	)
tvořili	tvořit	k5eAaImAgMnP	tvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960 85	[number]	k4	1960 85
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
představují	představovat	k5eAaImIp3nP	představovat
asi	asi	k9	asi
65	[number]	k4	65
%	%	kIx~	%
a	a	k8xC	a
podle	podle	k7c2	podle
prognóz	prognóza	k1gFnPc2	prognóza
klesne	klesnout	k5eAaPmIp3nS	klesnout
jejich	jejich	k3xOp3gInSc1	jejich
podíl	podíl	k1gInSc1	podíl
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2042	[number]	k4	2042
pod	pod	k7c4	pod
50	[number]	k4	50
%	%	kIx~	%
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2050	[number]	k4	2050
mají	mít	k5eAaImIp3nP	mít
tvořit	tvořit	k5eAaImF	tvořit
46,3	[number]	k4	46,3
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Nehispánští	hispánský	k2eNgMnPc1d1	hispánský
běloši	běloch	k1gMnPc1	běloch
tvořili	tvořit	k5eAaImAgMnP	tvořit
v	v	k7c6	v
letech	let	k1gInPc6	let
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
51,4	[number]	k4	51,4
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
jejich	jejich	k3xOp3gFnSc4	jejich
podíl	podíl	k1gInSc1	podíl
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
pod	pod	k7c4	pod
50	[number]	k4	50
%	%	kIx~	%
a	a	k8xC	a
menšiny	menšina	k1gFnPc1	menšina
tak	tak	k9	tak
již	již	k6eAd1	již
tvoří	tvořit	k5eAaImIp3nP	tvořit
většinu	většina	k1gFnSc4	většina
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
.	.	kIx.	.
<g/>
Muži	muž	k1gMnPc1	muž
tvoří	tvořit	k5eAaImIp3nP	tvořit
48,5	[number]	k4	48,5
%	%	kIx~	%
populace	populace	k1gFnPc1	populace
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
51,5	[number]	k4	51,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Mužů	muž	k1gMnPc2	muž
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
více	hodně	k6eAd2	hodně
ve	v	k7c6	v
věkových	věkový	k2eAgFnPc6d1	věková
skupinách	skupina	k1gFnPc6	skupina
do	do	k7c2	do
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
nad	nad	k7c4	nad
65	[number]	k4	65
let	léto	k1gNnPc2	léto
již	již	k6eAd1	již
převažují	převažovat	k5eAaImIp3nP	převažovat
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
o	o	k7c4	o
celých	celý	k2eAgNnPc2d1	celé
12,5	[number]	k4	12,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Věkové	věkový	k2eAgNnSc1d1	věkové
složení	složení	k1gNnSc1	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgFnSc1d1	následující
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Imigrace	imigrace	k1gFnSc2	imigrace
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
kolonizace	kolonizace	k1gFnSc1	kolonizace
v	v	k7c6	v
17.	[number]	k4	17.
století	století	k1gNnSc6	století
až	až	k9	až
do	do	k7c2	do
20.	[number]	k4	20.
století	století	k1gNnSc2	století
mezi	mezi	k7c7	mezi
imigranty	imigrant	k1gMnPc7	imigrant
převládali	převládat	k5eAaImAgMnP	převládat
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
,	,	kIx,	,
především	především	k9	především
Angličané	Angličan	k1gMnPc1	Angličan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Irové	Ir	k1gMnPc1	Ir
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
prchali	prchat	k5eAaImAgMnP	prchat
před	před	k7c7	před
hladomorem	hladomor	k1gInSc7	hladomor
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
ve	v	k7c6	v
větších	veliký	k2eAgInPc6d2	veliký
počtech	počet	k1gInPc6	počet
stěhovat	stěhovat	k5eAaImF	stěhovat
po	po	k7c6	po
potlačení	potlačení	k1gNnSc6	potlačení
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
imigrantů	imigrant	k1gMnPc2	imigrant
v	v	k7c4	v
19.	[number]	k4	19.
století	století	k1gNnPc2	století
prošla	projít	k5eAaPmAgFnS	projít
imigračním	imigrační	k2eAgNnSc7d1	imigrační
střediskem	středisko	k1gNnSc7	středisko
na	na	k7c6	na
Ellis	Ellis	k1gFnSc6	Ellis
Islandu	Island	k1gInSc2	Island
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19.	[number]	k4	19.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
stěhovalo	stěhovat	k5eAaImAgNnS	stěhovat
mnoho	mnoho	k4c1	mnoho
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
usazovali	usazovat	k5eAaImAgMnP	usazovat
především	především	k9	především
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
a	a	k8xC	a
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
středozápadu	středozápad	k1gInSc2	středozápad
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Nebrasce	Nebraska	k1gFnSc6	Nebraska
tvoří	tvořit	k5eAaImIp3nP	tvořit
Čechoameričané	Čechoameričan	k1gMnPc1	Čechoameričan
okolo	okolo	k7c2	okolo
5	[number]	k4	5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
jako	jako	k8xC	jako
Chicago	Chicago	k1gNnSc4	Chicago
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
<g />
.	.	kIx.	.
</s>
<s>
starostou	starosta	k1gMnSc7	starosta
český	český	k2eAgMnSc1d1	český
rodák	rodák	k1gMnSc1	rodák
Antonín	Antonín	k1gMnSc1	Antonín
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
.	.	kIx.	.
<g/>
Imigrační	imigrační	k2eAgInSc1d1	imigrační
zákon	zákon	k1gInSc1	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
(	(	kIx(	(
<g/>
Emergency	Emergenca	k1gFnSc2	Emergenca
Quota	Quota	k1gMnSc1	Quota
Act	Act	k1gMnSc1	Act
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
ještě	ještě	k6eAd1	ještě
zpřísněn	zpřísnit	k5eAaPmNgInS	zpřísnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
omezil	omezit	k5eAaPmAgInS	omezit
přistěhovalectví	přistěhovalectví	k1gNnSc4	přistěhovalectví
do	do	k7c2	do
USA	USA	kA	USA
kvótami	kvóta	k1gFnPc7	kvóta
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nejvíce	hodně	k6eAd3	hodně
postihlo	postihnout	k5eAaPmAgNnS	postihnout
emigranty	emigrant	k1gMnPc4	emigrant
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tvořili	tvořit	k5eAaImAgMnP	tvořit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20.	[number]	k4	20.
století	století	k1gNnSc2	století
přes	přes	k7c4	přes
polovinu	polovina	k1gFnSc4	polovina
nově	nově	k6eAd1	nově
příchozích	příchozí	k1gMnPc2	příchozí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30.	[number]	k4	30.
letech	léto	k1gNnPc6	léto
tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
bránil	bránit	k5eAaImAgInS	bránit
příchodu	příchod	k1gInSc3	příchod
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
prchali	prchat	k5eAaImAgMnP	prchat
před	před	k7c7	před
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50.	[number]	k4	50.
a	a	k8xC	a
60.	[number]	k4	60.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
stěhovalo	stěhovat	k5eAaImAgNnS	stěhovat
ročně	ročně	k6eAd1	ročně
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
Prezident	prezident	k1gMnSc1	prezident
Lyndon	Lyndon	k1gMnSc1	Lyndon
B.	B.	kA	B.
Johnson	Johnson	k1gMnSc1	Johnson
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
podepsal	podepsat	k5eAaPmAgInS	podepsat
zákon	zákon	k1gInSc1	zákon
Immigration	Immigration	k1gInSc1	Immigration
and	and	k?	and
Nationality	Nationalita	k1gFnSc2	Nationalita
Act	Act	k1gFnSc2	Act
of	of	k?	of
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zrušil	zrušit	k5eAaPmAgMnS	zrušit
jakákoliv	jakýkoliv	k3yIgNnPc4	jakýkoliv
rasová	rasový	k2eAgNnPc4d1	rasové
omezení	omezení	k1gNnPc4	omezení
pro	pro	k7c4	pro
imigraci	imigrace	k1gFnSc4	imigrace
a	a	k8xC	a
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
otevřel	otevřít	k5eAaPmAgMnS	otevřít
imigrantům	imigrant	k1gMnPc3	imigrant
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
třetího	třetí	k4xOgInSc2	třetí
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Karibiku	Karibik	k1gInSc2	Karibik
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
postupně	postupně	k6eAd1	postupně
mezi	mezi	k7c7	mezi
imigranty	imigrant	k1gMnPc7	imigrant
převládli	převládnout	k5eAaPmAgMnP	převládnout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21.	[number]	k4	21.
století	století	k1gNnSc2	století
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
přijímaly	přijímat	k5eAaImAgInP	přijímat
okolo	okolo	k7c2	okolo
1	[number]	k4	1
milionu	milion	k4xCgInSc2	milion
imigrantů	imigrant	k1gMnPc2	imigrant
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
přistěhovalo	přistěhovat	k5eAaPmAgNnS	přistěhovat
legálně	legálně	k6eAd1	legálně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
milion	milion	k4xCgInSc1	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jenom	jenom	k9	jenom
85	[number]	k4	85
tisíc	tisíc	k4xCgInSc4	tisíc
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
přišlo	přijít	k5eAaPmAgNnS	přijít
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
<g/>
Takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
"	"	kIx"	"
<g/>
sanctuary	sanctuar	k1gInPc1	sanctuar
cities	citiesa	k1gFnPc2	citiesa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
ochranitelská	ochranitelský	k2eAgNnPc4d1	ochranitelské
či	či	k8xC	či
azylová	azylový	k2eAgNnPc4d1	azylové
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
než	než	k8xS	než
patří	patřit	k5eAaImIp3nS	patřit
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
<g/>
,	,	kIx,	,
Denver	Denver	k1gInSc1	Denver
<g/>
,	,	kIx,	,
Chicago	Chicago	k1gNnSc4	Chicago
<g/>
,	,	kIx,	,
Seattle	Seattle	k1gFnPc4	Seattle
a	a	k8xC	a
stovky	stovka	k1gFnPc4	stovka
dalších	další	k2eAgNnPc2d1	další
amerických	americký	k2eAgNnPc2d1	americké
měst	město	k1gNnPc2	město
a	a	k8xC	a
okrsků	okrsek	k1gInPc2	okrsek
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
demokraty	demokrat	k1gMnPc7	demokrat
<g/>
,	,	kIx,	,
odmítají	odmítat	k5eAaImIp3nP	odmítat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
území	území	k1gNnSc6	území
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
federální	federální	k2eAgInPc4d1	federální
zákony	zákon	k1gInPc4	zákon
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
imigrace	imigrace	k1gFnSc2	imigrace
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nS	chránit
nelegální	legální	k2eNgMnPc4d1	nelegální
imigranty	imigrant	k1gMnPc4	imigrant
před	před	k7c7	před
federální	federální	k2eAgFnSc7d1	federální
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Jazyky	jazyk	k1gInPc1	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Angličtina	angličtina	k1gFnSc1	angličtina
je	být	k5eAaImIp3nS	být
výlučným	výlučný	k2eAgInSc7d1	výlučný
mateřským	mateřský	k2eAgInSc7d1	mateřský
jazykem	jazyk	k1gInSc7	jazyk
pro	pro	k7c4	pro
asi	asi	k9	asi
82	[number]	k4	82
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
španělština	španělština	k1gFnSc1	španělština
pro	pro	k7c4	pro
10	[number]	k4	10
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
nemají	mít	k5eNaImIp3nP	mít
na	na	k7c6	na
federální	federální	k2eAgFnSc6d1	federální
úrovni	úroveň	k1gFnSc6	úroveň
zákonem	zákon	k1gInSc7	zákon
kodifikovaný	kodifikovaný	k2eAgInSc4d1	kodifikovaný
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
angličtina	angličtina	k1gFnSc1	angličtina
jím	on	k3xPp3gNnSc7	on
fakticky	fakticky	k6eAd1	fakticky
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
zákony	zákon	k1gInPc1	zákon
a	a	k8xC	a
předpisy	předpis	k1gInPc1	předpis
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
oblastech	oblast	k1gFnPc6	oblast
její	její	k3xOp3gNnSc1	její
znalost	znalost	k1gFnSc1	znalost
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
požadavky	požadavek	k1gInPc4	požadavek
pro	pro	k7c4	pro
naturalizaci	naturalizace	k1gFnSc4	naturalizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
státy	stát	k1gInPc1	stát
Unie	unie	k1gFnSc2	unie
úřední	úřední	k2eAgInPc1d1	úřední
jazyky	jazyk	k1gInPc1	jazyk
kodifikovány	kodifikován	k2eAgInPc1d1	kodifikován
mají	mít	k5eAaImIp3nP	mít
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buďto	buďto	k8xC	buďto
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vícero	vícero	k1gNnSc1	vícero
jazyků	jazyk	k1gInPc2	jazyk
včetně	včetně	k7c2	včetně
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Vícejazyčné	vícejazyčný	k2eAgInPc1d1	vícejazyčný
státy	stát	k1gInPc1	stát
jsou	být	k5eAaImIp3nP	být
Havaj	Havaj	k1gFnSc4	Havaj
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
havajština	havajština	k1gFnSc1	havajština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
španělština	španělština	k1gFnSc1	španělština
<g/>
)	)	kIx)	)
a	a	k8xC	a
Louisiana	Louisiana	k1gFnSc1	Louisiana
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
dalších	další	k2eAgNnPc6d1	další
závislých	závislý	k2eAgNnPc6d1	závislé
územích	území	k1gNnPc6	území
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
ještě	ještě	k6eAd1	ještě
pestřejší	pestrý	k2eAgFnSc1d2	pestřejší
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
zvláštními	zvláštní	k2eAgInPc7d1	zvláštní
předpisy	předpis	k1gInPc7	předpis
chráněn	chránit	k5eAaImNgInS	chránit
jazyk	jazyk	k1gInSc1	jazyk
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgMnPc2d1	americký
hrají	hrát	k5eAaImIp3nP	hrát
náboženství	náboženství	k1gNnSc4	náboženství
mnohem	mnohem	k6eAd1	mnohem
významnější	významný	k2eAgFnSc4d2	významnější
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20.	[number]	k4	20.
století	století	k1gNnSc2	století
k	k	k7c3	k
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
sekularizaci	sekularizace	k1gFnSc3	sekularizace
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
kupř.	kupř.	kA	kupř.
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
až	až	k8xS	až
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
dodat	dodat	k5eAaPmF	dodat
pro	pro	k7c4	pro
USA	USA	kA	USA
přesná	přesný	k2eAgNnPc4d1	přesné
čísla	číslo	k1gNnPc4	číslo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
neexistují	existovat	k5eNaImIp3nP	existovat
žádné	žádný	k3yNgFnPc1	žádný
ucelené	ucelený	k2eAgFnPc1d1	ucelená
statistiky	statistika	k1gFnPc1	statistika
typu	typ	k1gInSc2	typ
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
(	(	kIx(	(
<g/>
americké	americký	k2eAgNnSc1d1	americké
pojetí	pojetí	k1gNnSc1	pojetí
odluky	odluka	k1gFnSc2	odluka
církve	církev	k1gFnSc2	církev
od	od	k7c2	od
státu	stát	k1gInSc2	stát
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
státní	státní	k2eAgInPc1d1	státní
úřady	úřad	k1gInPc1	úřad
na	na	k7c4	na
věci	věc	k1gFnPc4	věc
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
víry	víra	k1gFnSc2	víra
neptají	ptat	k5eNaImIp3nP	ptat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
počty	počet	k1gInPc1	počet
věřících	věřící	k1gMnPc2	věřící
se	se	k3xPyFc4	se
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
buďto	buďto	k8xC	buďto
ze	z	k7c2	z
soukromých	soukromý	k2eAgInPc2d1	soukromý
průzkumů	průzkum	k1gInPc2	průzkum
či	či	k8xC	či
údajů	údaj	k1gInPc2	údaj
samotných	samotný	k2eAgFnPc2d1	samotná
církví	církev	k1gFnPc2	církev
a	a	k8xC	a
představují	představovat	k5eAaImIp3nP	představovat
spíše	spíše	k9	spíše
hrubé	hrubý	k2eAgInPc1d1	hrubý
odhady	odhad	k1gInPc1	odhad
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatel	obyvatel	k1gMnSc1	obyvatel
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
ateistů	ateista	k1gMnPc2	ateista
a	a	k8xC	a
agnostiků	agnostik	k1gMnPc2	agnostik
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
asi	asi	k9	asi
14	[number]	k4	14
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejrozšířenějším	rozšířený	k2eAgNnSc7d3	nejrozšířenější
náboženstvím	náboženství	k1gNnSc7	náboženství
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
křesťanství	křesťanství	k1gNnSc1	křesťanství
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
asi	asi	k9	asi
76	[number]	k4	76
%	%	kIx~	%
až	až	k9	až
81	[number]	k4	81
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
2/3	[number]	k4	2/3
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
protestanti	protestant	k1gMnPc1	protestant
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
roztříštěni	roztříštěn	k2eAgMnPc1d1	roztříštěn
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
dílčích	dílčí	k2eAgNnPc2d1	dílčí
seskupení	seskupení	k1gNnPc2	seskupení
a	a	k8xC	a
denominací	denominace	k1gFnPc2	denominace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
církví	církev	k1gFnSc7	církev
je	být	k5eAaImIp3nS	být
církev	církev	k1gFnSc1	církev
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
náleží	náležet	k5eAaImIp3nS	náležet
zbylá	zbylý	k2eAgFnSc1d1	zbylá
třetina	třetina	k1gFnSc1	třetina
křesťanů	křesťan	k1gMnPc2	křesťan
(	(	kIx(	(
<g/>
asi	asi	k9	asi
25	[number]	k4	25
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
tam	tam	k6eAd1	tam
i	i	k9	i
početné	početný	k2eAgFnSc2d1	početná
diecéze	diecéze	k1gFnSc2	diecéze
východních	východní	k2eAgMnPc2d1	východní
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
10	[number]	k4	10
církví	církev	k1gFnPc2	církev
<g/>
:	:	kIx,	:
arménští	arménský	k2eAgMnPc1d1	arménský
katolíci	katolík	k1gMnPc1	katolík
<g/>
,	,	kIx,	,
syrští	syrský	k2eAgMnPc1d1	syrský
katolíci	katolík	k1gMnPc1	katolík
<g/>
,	,	kIx,	,
syromalankarští	syromalankarský	k2eAgMnPc1d1	syromalankarský
katolíci	katolík	k1gMnPc1	katolík
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
syromalabarští	syromalabarský	k2eAgMnPc1d1	syromalabarský
katolíci	katolík	k1gMnPc1	katolík
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
chaldejští	chaldejský	k2eAgMnPc1d1	chaldejský
katolíci	katolík	k1gMnPc1	katolík
z	z	k7c2	z
Iráku	Irák	k1gInSc2	Irák
<g/>
,	,	kIx,	,
maronité	maronitý	k2eAgFnPc1d1	maronitý
<g/>
,	,	kIx,	,
řeckokatolíci-melchité	řeckokatolícielchita	k1gMnPc1	řeckokatolíci-melchita
<g/>
,	,	kIx,	,
ukrajinští	ukrajinský	k2eAgMnPc1d1	ukrajinský
řeckokatolíci	řeckokatolík	k1gMnPc1	řeckokatolík
<g/>
,	,	kIx,	,
rusínští	rusínský	k2eAgMnPc1d1	rusínský
řeckokatolíci	řeckokatolík	k1gMnPc1	řeckokatolík
a	a	k8xC	a
rumunští	rumunský	k2eAgMnPc1d1	rumunský
řeckokatolíci	řeckokatolík	k1gMnPc1	řeckokatolík
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
mají	mít	k5eAaImIp3nP	mít
tyto	tento	k3xDgFnPc1	tento
církve	církev	k1gFnPc1	církev
468 330	[number]	k4	468 330
věřících	věřící	k1gFnPc2	věřící
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
také	také	k9	také
část	část	k1gFnSc4	část
z	z	k7c2	z
36 000	[number]	k4	36 000
arménských	arménský	k2eAgMnPc2d1	arménský
katolíků	katolík	k1gMnPc2	katolík
a	a	k8xC	a
část	část	k1gFnSc4	část
z	z	k7c2	z
6 200	[number]	k4	6 200
rumunských	rumunský	k2eAgMnPc2d1	rumunský
řeckokatolíků	řeckokatolík	k1gMnPc2	řeckokatolík
z	z	k7c2	z
diecézí	diecéze	k1gFnPc2	diecéze
pro	pro	k7c4	pro
USA	USA	kA	USA
a	a	k8xC	a
Kanadu	Kanada	k1gFnSc4	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
též	též	k9	též
personální	personální	k2eAgInSc1d1	personální
Ordinariát	ordinariát	k1gInSc1	ordinariát
katedry	katedra	k1gFnSc2	katedra
sv.	sv.	kA	sv.
Petra	Petra	k1gFnSc1	Petra
pro	pro	k7c4	pro
římskokatolíky	římskokatolík	k1gMnPc4	římskokatolík
anglikánského	anglikánský	k2eAgInSc2d1	anglikánský
ritu	rit	k1gInSc2	rit
(	(	kIx(	(
<g/>
bývalé	bývalý	k2eAgFnSc2d1	bývalá
episkopály	episkopála	k1gFnSc2	episkopála
z	z	k7c2	z
ECUSA-Episkopální	ECUSA-Episkopální	k2eAgFnSc2d1	ECUSA-Episkopální
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
1.	[number]	k4	1.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
měl	mít	k5eAaImAgInS	mít
2 550	[number]	k4	2 550
věřících	věřící	k1gMnPc2	věřící
a	a	k8xC	a
23	[number]	k4	23
kněží	kněz	k1gMnPc2	kněz
ve	v	k7c6	v
12	[number]	k4	12
farnostech	farnost	k1gFnPc6	farnost
<g/>
;	;	kIx,	;
počet	počet	k1gInSc4	počet
věřících	věřící	k1gMnPc2	věřící
narůstá	narůstat	k5eAaImIp3nS	narůstat
-	-	kIx~	-
2013	[number]	k4	2013
již	již	k9	již
4 550	[number]	k4	4 550
<g/>
,	,	kIx,	,
23	[number]	k4	23
kněží	kněz	k1gMnPc2	kněz
ve	v	k7c6	v
12	[number]	k4	12
farnostech	farnost	k1gFnPc6	farnost
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
-	-	kIx~	-
už	už	k6eAd1	už
6 000	[number]	k4	6 000
věřících	věřící	k1gMnPc2	věřící
a	a	k8xC	a
40	[number]	k4	40
kněží	kněz	k1gMnPc2	kněz
ve	v	k7c6	v
25	[number]	k4	25
farnostech	farnost	k1gFnPc6	farnost
<g/>
.	.	kIx.	.
</s>
<s>
Ordinářem	ordinář	k1gMnSc7	ordinář
je	být	k5eAaImIp3nS	být
Jeffrey	Jeffre	k1gMnPc4	Jeffre
Neil	Neil	k1gMnSc1	Neil
Steenson	Steenson	k1gMnSc1	Steenson
<g/>
,	,	kIx,	,
do	do	k7c2	do
září	září	k1gNnSc2	září
2007	[number]	k4	2007
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
biskupů	biskup	k1gMnPc2	biskup
ECUSA	ECUSA	kA	ECUSA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
konvertoval	konvertovat	k5eAaBmAgMnS	konvertovat
(	(	kIx(	(
<g/>
i	i	k8xC	i
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
<g/>
)	)	kIx)	)
1.	[number]	k4	1.
prosince	prosinec	k1gInSc2	prosinec
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ordinářem	ordinář	k1gMnSc7	ordinář
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
1.	[number]	k4	1.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
uveden	uveden	k2eAgInSc1d1	uveden
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
12.	[number]	k4	12.
února	únor	k1gInSc2	únor
2012.	[number]	k4	2012.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
nejpočetnější	početní	k2eAgFnSc1d3	nejpočetnější
a	a	k8xC	a
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
skupina	skupina	k1gFnSc1	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
z	z	k7c2	z
náboženského	náboženský	k2eAgNnSc2d1	náboženské
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
bráni	brán	k2eAgMnPc1d1	brán
evangelikálové	evangelikál	k1gMnPc1	evangelikál
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
následovaní	následovaný	k2eAgMnPc1d1	následovaný
katolíky	katolík	k1gMnPc7	katolík
(	(	kIx(	(
<g/>
obojí	oboj	k1gFnSc7	oboj
zhruba	zhruba	k6eAd1	zhruba
25	[number]	k4	25
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
pořadí	pořadí	k1gNnSc2	pořadí
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
statistikách	statistika	k1gFnPc6	statistika
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
koncentrace	koncentrace	k1gFnSc1	koncentrace
konzervativních	konzervativní	k2eAgMnPc2d1	konzervativní
protestantů	protestant	k1gMnPc2	protestant
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
představují	představovat	k5eAaImIp3nP	představovat
dominantní	dominantní	k2eAgFnSc4d1	dominantní
kulturní	kulturní	k2eAgFnSc4d1	kulturní
a	a	k8xC	a
politickou	politický	k2eAgFnSc4d1	politická
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
dala	dát	k5eAaPmAgFnS	dát
této	tento	k3xDgFnSc3	tento
oblasti	oblast	k1gFnSc3	oblast
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
biblický	biblický	k2eAgInSc1d1	biblický
pás	pás	k1gInSc1	pás
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Bible	bible	k1gFnSc1	bible
Belt	Belt	k1gMnSc1	Belt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Církev	církev	k1gFnSc1	církev
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
Svatých	svatý	k1gMnPc2	svatý
posledních	poslední	k2eAgInPc2d1	poslední
dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
mormoni	mormon	k1gMnPc1	mormon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
stoupenci	stoupenec	k1gMnPc1	stoupenec
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19.	[number]	k4	19.
století	století	k1gNnSc2	století
osídli	osídlit	k5eAaPmRp2nS	osídlit
území	území	k1gNnSc6	území
budoucího	budoucí	k2eAgInSc2d1	budoucí
státu	stát	k1gInSc2	stát
Utah	Utah	k1gInSc1	Utah
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
dnes	dnes	k6eAd1	dnes
většinovou	většinový	k2eAgFnSc7d1	většinová
církví	církev	k1gFnSc7	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nekřesťanská	křesťanský	k2eNgNnPc1d1	nekřesťanské
náboženství	náboženství	k1gNnPc1	náboženství
představují	představovat	k5eAaImIp3nP	představovat
asi	asi	k9	asi
3,5	[number]	k4	3,5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
USA	USA	kA	USA
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
judaismus	judaismus	k1gInSc4	judaismus
<g/>
(	(	kIx(	(
<g/>
židé	žid	k1gMnPc1	žid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
islám	islám	k1gInSc1	islám
<g/>
,	,	kIx,	,
buddhismus	buddhismus	k1gInSc1	buddhismus
a	a	k8xC	a
hinduismus	hinduismus	k1gInSc1	hinduismus
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
počet	počet	k1gInSc1	počet
vyznavačů	vyznavač	k1gMnPc2	vyznavač
islámu	islám	k1gInSc2	islám
a	a	k8xC	a
buddhismu	buddhismus	k1gInSc2	buddhismus
rychle	rychle	k6eAd1	rychle
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
křesťané	křesťan	k1gMnPc1	křesťan
76,7	[number]	k4	76,7
%	%	kIx~	%
</s>
</p>
<p>
<s>
protestanti	protestant	k1gMnPc1	protestant
52	[number]	k4	52
%	%	kIx~	%
</s>
</p>
<p>
<s>
baptisté	baptista	k1gMnPc1	baptista
16,3	[number]	k4	16,3
%	%	kIx~	%
</s>
</p>
<p>
<s>
metodisté	metodista	k1gMnPc1	metodista
6,8	[number]	k4	6,8
%	%	kIx~	%
</s>
</p>
<p>
<s>
luteráni	luterán	k1gMnPc1	luterán
4,6	[number]	k4	4,6
%	%	kIx~	%
</s>
</p>
<p>
<s>
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
24,5	[number]	k4	24,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
plus	plus	k6eAd1	plus
východní	východní	k2eAgMnPc1d1	východní
katolíci	katolík	k1gMnPc1	katolík
</s>
</p>
<p>
<s>
agnostici	agnostik	k1gMnPc1	agnostik
<g/>
,	,	kIx,	,
ateisté	ateista	k1gMnPc1	ateista
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
celkem	celkem	k6eAd1	celkem
14,2	[number]	k4	14,2
%	%	kIx~	%
</s>
</p>
<p>
<s>
židé	žid	k1gMnPc1	žid
1,6	[number]	k4	1,6
%	%	kIx~	%
</s>
</p>
<p>
<s>
muslimové	muslim	k1gMnPc1	muslim
0,6	[number]	k4	0,6
%	%	kIx~	%
</s>
</p>
<p>
<s>
buddhisté	buddhista	k1gMnPc1	buddhista
0,5	[number]	k4	0,5
%	%	kIx~	%
</s>
</p>
<p>
<s>
hinduisté	hinduista	k1gMnPc1	hinduista
0,4	[number]	k4	0,4
%	%	kIx~	%
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Systém	systém	k1gInSc1	systém
vzdělávaní	vzdělávaný	k2eAgMnPc1d1	vzdělávaný
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
USA	USA	kA	USA
podobný	podobný	k2eAgInSc1d1	podobný
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
z	z	k7c2	z
50	[number]	k4	50
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Školní	školní	k2eAgInSc1d1	školní
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kompetenci	kompetence	k1gFnSc6	kompetence
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Výrazné	výrazný	k2eAgInPc1d1	výrazný
rozdíly	rozdíl	k1gInPc1	rozdíl
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
financování	financování	k1gNnSc6	financování
<g/>
.	.	kIx.	.
</s>
<s>
Veřejné	veřejný	k2eAgNnSc1d1	veřejné
školství	školství	k1gNnSc1	školství
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
decentralizované	decentralizovaný	k2eAgNnSc1d1	decentralizované
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
každého	každý	k3xTgInSc2	každý
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
např.	např.	kA	např.
od	od	k7c2	od
státního	státní	k2eAgInSc2d1	státní
výboru	výbor	k1gInSc2	výbor
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
až	až	k9	až
po	po	k7c4	po
místní	místní	k2eAgInPc4d1	místní
školské	školský	k2eAgInPc4d1	školský
obvody	obvod	k1gInPc4	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Obecné	obecný	k2eAgInPc4d1	obecný
požadavky	požadavek	k1gInPc4	požadavek
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
realizují	realizovat	k5eAaBmIp3nP	realizovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
těchto	tento	k3xDgInPc2	tento
místních	místní	k2eAgInPc2d1	místní
školních	školní	k2eAgInPc2d1	školní
obvodů	obvod	k1gInPc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
spravovány	spravovat	k5eAaImNgFnP	spravovat
školní	školní	k2eAgFnSc7d1	školní
radou	rada	k1gFnSc7	rada
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
volí	volit	k5eAaImIp3nP	volit
občané	občan	k1gMnPc1	občan
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
formě	forma	k1gFnSc6	forma
výuky	výuka	k1gFnSc2	výuka
<g/>
,	,	kIx,	,
osnov	osnova	k1gFnPc2	osnova
<g/>
,	,	kIx,	,
použití	použití	k1gNnSc1	použití
učebnic	učebnice	k1gFnPc2	učebnice
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
školní	školní	k2eAgFnSc1d1	školní
rada	rada	k1gFnSc1	rada
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kompetentním	kompetentní	k2eAgMnSc7d1	kompetentní
pedagogickým	pedagogický	k2eAgMnSc7d1	pedagogický
pracovníkem	pracovník	k1gMnSc7	pracovník
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Financování	financování	k1gNnSc1	financování
škol	škola	k1gFnPc2	škola
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
peněžních	peněžní	k2eAgInPc6d1	peněžní
prostředcích	prostředek	k1gInPc6	prostředek
plynoucích	plynoucí	k2eAgFnPc2d1	plynoucí
z	z	k7c2	z
místních	místní	k2eAgFnPc2d1	místní
daní	daň	k1gFnPc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Aktuálně	aktuálně	k6eAd1	aktuálně
se	se	k3xPyFc4	se
školy	škola	k1gFnPc1	škola
financují	financovat	k5eAaBmIp3nP	financovat
hlavně	hlavně	k9	hlavně
ze	z	k7c2	z
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
a	a	k8xC	a
o	o	k7c4	o
použití	použití	k1gNnSc4	použití
prostředků	prostředek	k1gInPc2	prostředek
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
školní	školní	k2eAgFnSc1d1	školní
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
<g/>
Veřejné	veřejný	k2eAgNnSc1d1	veřejné
školství	školství	k1gNnSc1	školství
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
dostupné	dostupný	k2eAgNnSc1d1	dostupné
<g/>
.	.	kIx.	.
</s>
<s>
Věk	věk	k1gInSc1	věk
vzdělávací	vzdělávací	k2eAgFnSc2d1	vzdělávací
povinnosti	povinnost	k1gFnSc2	povinnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
státech	stát	k1gInPc6	stát
různý	různý	k2eAgInSc1d1	různý
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
pěti	pět	k4xCc6	pět
až	až	k8xS	až
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
ve	v	k7c6	v
čtrnácti	čtrnáct	k4xCc6	čtrnáct
až	až	k8xS	až
osmnácti	osmnáct	k4xCc6	osmnáct
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělávací	vzdělávací	k2eAgFnSc1d1	vzdělávací
povinnost	povinnost	k1gFnSc1	povinnost
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
plní	plnit	k5eAaImIp3nS	plnit
školní	školní	k2eAgFnSc7d1	školní
docházkou	docházka	k1gFnSc7	docházka
do	do	k7c2	do
veřejných	veřejný	k2eAgFnPc2d1	veřejná
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
do	do	k7c2	do
státem	stát	k1gInSc7	stát
akreditovaných	akreditovaný	k2eAgFnPc2d1	akreditovaná
soukromých	soukromý	k2eAgFnPc2d1	soukromá
škol	škola	k1gFnPc2	škola
nebo	nebo	k8xC	nebo
schválenými	schválený	k2eAgInPc7d1	schválený
programy	program	k1gInPc7	program
domácího	domácí	k2eAgNnSc2d1	domácí
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
veřejných	veřejný	k2eAgFnPc2d1	veřejná
a	a	k8xC	a
soukromých	soukromý	k2eAgFnPc2d1	soukromá
škol	škola	k1gFnPc2	škola
je	být	k5eAaImIp3nS	být
výuka	výuka	k1gFnSc1	výuka
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
stupňů	stupeň	k1gInPc2	stupeň
(	(	kIx(	(
<g/>
úrovní	úroveň	k1gFnPc2	úroveň
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
základní	základní	k2eAgFnSc1d1	základní
<g/>
"	"	kIx"	"
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
elementary	elementar	k1gInPc1	elementar
school	schoola	k1gFnPc2	schoola
<g/>
;	;	kIx,	;
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
první	první	k4xOgInSc4	první
stupeň	stupeň	k1gInSc4	stupeň
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
"	"	kIx"	"
<g/>
obecná	obecná	k1gFnSc1	obecná
<g/>
"	"	kIx"	"
škola	škola	k1gFnSc1	škola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
střední	střední	k2eAgMnPc4d1	střední
<g/>
"	"	kIx"	"
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
druhému	druhý	k4xOgInSc3	druhý
stupni	stupeň	k1gInSc3	stupeň
české	český	k2eAgFnSc2d1	Česká
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
nebo	nebo	k8xC	nebo
nižším	nízký	k2eAgInPc3d2	nižší
ročníkům	ročník	k1gInPc3	ročník
víceletých	víceletý	k2eAgNnPc2d1	víceleté
gymnázií	gymnázium	k1gNnPc2	gymnázium
(	(	kIx(	(
<g/>
middle	middle	k6eAd1	middle
school	school	k1gInSc1	school
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
junior	junior	k1gMnSc1	junior
high	higha	k1gFnPc2	higha
school	schoola	k1gFnPc2	schoola
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
high	high	k1gInSc1	high
school	schoola	k1gFnPc2	schoola
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
secondary	secondara	k1gFnPc1	secondara
education	education	k1gInSc1	education
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgMnSc1d1	odpovídající
českým	český	k2eAgFnPc3d1	Česká
středním	střední	k2eAgFnPc3d1	střední
školám	škola	k1gFnPc3	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
převážné	převážný	k2eAgFnSc6d1	převážná
většině	většina	k1gFnSc6	většina
škol	škola	k1gFnPc2	škola
těchto	tento	k3xDgInPc2	tento
stupňů	stupeň	k1gInPc2	stupeň
jsou	být	k5eAaImIp3nP	být
děti	dítě	k1gFnPc1	dítě
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
do	do	k7c2	do
věkových	věkový	k2eAgFnPc2d1	věková
skupin	skupina	k1gFnPc2	skupina
na	na	k7c4	na
ročníky	ročník	k1gInPc4	ročník
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
třídy	třída	k1gFnSc2	třída
(	(	kIx(	(
<g/>
grade	grad	k1gInSc5	grad
<g/>
)	)	kIx)	)
od	od	k7c2	od
školky	školka	k1gFnSc2	školka
(	(	kIx(	(
<g/>
Kindergarden	Kindergardna	k1gFnPc2	Kindergardna
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
nejmladší	mladý	k2eAgMnPc4d3	nejmladší
žáky	žák	k1gMnPc4	žák
přes	přes	k7c4	přes
první	první	k4xOgFnSc4	první
třídu	třída	k1gFnSc4	třída
až	až	k9	až
po	po	k7c4	po
dvanáctý	dvanáctý	k4xOgInSc4	dvanáctý
ročník	ročník	k1gInSc4	ročník
(	(	kIx(	(
<g/>
třídu	třída	k1gFnSc4	třída
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc4d1	poslední
rok	rok	k1gInSc4	rok
"	"	kIx"	"
<g/>
vysoké	vysoký	k2eAgFnPc4d1	vysoká
<g/>
"	"	kIx"	"
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
věkové	věkový	k2eAgNnSc4d1	věkové
přiřazení	přiřazení	k1gNnSc4	přiřazení
žáků	žák	k1gMnPc2	žák
a	a	k8xC	a
studentů	student	k1gMnPc2	student
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
jen	jen	k9	jen
student	student	k1gMnSc1	student
<g/>
)	)	kIx)	)
v	v	k7c6	v
ročnících	ročník	k1gInPc6	ročník
se	se	k3xPyFc4	se
trochu	trochu	k6eAd1	trochu
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
terciární	terciární	k2eAgNnSc1d1	terciární
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
<g/>
,	,	kIx,	,
známější	známý	k2eAgFnSc1d2	známější
jako	jako	k9	jako
college	college	k1gFnPc7	college
univerzitní	univerzitní	k2eAgFnSc2d1	univerzitní
úrovně	úroveň	k1gFnSc2	úroveň
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
vedeno	vést	k5eAaImNgNnS	vést
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
základním	základní	k2eAgMnSc6d1	základní
a	a	k8xC	a
středním	střední	k2eAgNnSc6d1	střední
školství	školství	k1gNnSc6	školství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
rasová	rasový	k2eAgFnSc1d1	rasová
segregace	segregace	k1gFnSc1	segregace
na	na	k7c6	na
amerických	americký	k2eAgFnPc6d1	americká
veřejných	veřejný	k2eAgFnPc6d1	veřejná
školách	škola	k1gFnPc6	škola
je	být	k5eAaImIp3nS	být
protiústavní	protiústavní	k2eAgNnSc1d1	protiústavní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
za	za	k7c4	za
desegregaci	desegregace	k1gFnSc4	desegregace
začaly	začít	k5eAaPmAgInP	začít
americké	americký	k2eAgInPc1d1	americký
soudy	soud	k1gInPc1	soud
v	v	k7c4	v
60.	[number]	k4	60.
a	a	k8xC	a
70.	[number]	k4	70.
letech	léto	k1gNnPc6	léto
nařizovat	nařizovat	k5eAaImF	nařizovat
povinné	povinný	k2eAgNnSc4d1	povinné
rozvážení	rozvážení	k1gNnSc4	rozvážení
dětí	dítě	k1gFnPc2	dítě
odlišných	odlišný	k2eAgFnPc2d1	odlišná
ras	rasa	k1gFnPc2	rasa
do	do	k7c2	do
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
čtvrtích	čtvrt	k1gFnPc6	čtvrt
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
desegregation	desegregation	k1gInSc1	desegregation
busing	busing	k1gInSc1	busing
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
rasová	rasový	k2eAgFnSc1d1	rasová
rovnováha	rovnováha	k1gFnSc1	rovnováha
na	na	k7c6	na
amerických	americký	k2eAgFnPc6d1	americká
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
odporem	odpor	k1gInSc7	odpor
mnoha	mnoho	k4c2	mnoho
bílých	bílý	k2eAgMnPc2d1	bílý
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
nastal	nastat	k5eAaPmAgInS	nastat
tzv.	tzv.	kA	tzv.
bílý	bílý	k2eAgInSc4d1	bílý
útěk	útěk	k1gInSc4	útěk
z	z	k7c2	z
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
3.	[number]	k4	3.
července	červenec	k1gInSc2	červenec
2018	[number]	k4	2018
vydala	vydat	k5eAaPmAgFnS	vydat
Trumpova	Trumpův	k2eAgFnSc1d1	Trumpova
administrativa	administrativa	k1gFnSc1	administrativa
nařízení	nařízení	k1gNnSc2	nařízení
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
zrušila	zrušit	k5eAaPmAgFnS	zrušit
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
praxi	praxe	k1gFnSc4	praxe
pozitivní	pozitivní	k2eAgFnSc2d1	pozitivní
diskriminace	diskriminace	k1gFnSc2	diskriminace
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
affirmative	affirmativ	k1gInSc5	affirmativ
action	action	k1gInSc4	action
<g/>
)	)	kIx)	)
na	na	k7c6	na
amerických	americký	k2eAgFnPc6d1	americká
středních	střední	k2eAgFnPc6d1	střední
a	a	k8xC	a
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
při	při	k7c6	při
přijímání	přijímání	k1gNnSc6	přijímání
studentů	student	k1gMnPc2	student
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
podle	podle	k7c2	podle
rasového	rasový	k2eAgInSc2d1	rasový
klíče	klíč	k1gInSc2	klíč
a	a	k8xC	a
směly	smět	k5eAaImAgInP	smět
zvýhodňovat	zvýhodňovat	k5eAaImF	zvýhodňovat
afroamerické	afroamerický	k2eAgMnPc4d1	afroamerický
a	a	k8xC	a
hispánské	hispánský	k2eAgMnPc4d1	hispánský
uchazeče	uchazeč	k1gMnPc4	uchazeč
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
na	na	k7c4	na
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Harvardova	Harvardův	k2eAgFnSc1d1	Harvardova
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
americké	americký	k2eAgFnPc1d1	americká
univerzity	univerzita	k1gFnPc1	univerzita
se	se	k3xPyFc4	se
rozhodly	rozhodnout	k5eAaPmAgFnP	rozhodnout
nařízení	nařízení	k1gNnSc4	nařízení
vlády	vláda	k1gFnSc2	vláda
nerespektovat	respektovat	k5eNaImF	respektovat
a	a	k8xC	a
chtějí	chtít	k5eAaImIp3nP	chtít
dál	daleko	k6eAd2	daleko
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
rasové	rasový	k2eAgNnSc4d1	rasové
hledisko	hledisko	k1gNnSc4	hledisko
při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
budoucích	budoucí	k2eAgMnPc2d1	budoucí
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
péče	péče	k1gFnSc1	péče
===	===	k?	===
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
země	zem	k1gFnPc4	zem
s	s	k7c7	s
nejvyššími	vysoký	k2eAgInPc7d3	Nejvyšší
výdaji	výdaj	k1gInPc7	výdaj
na	na	k7c6	na
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezinárodních	mezinárodní	k2eAgInPc6d1	mezinárodní
žebříčcích	žebříček	k1gInPc6	žebříček
kvality	kvalita	k1gFnSc2	kvalita
zdravotnických	zdravotnický	k2eAgInPc2d1	zdravotnický
systémů	systém	k1gInPc2	systém
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
BMJ	BMJ	kA	BMJ
<g/>
)	)	kIx)	)
však	však	k9	však
nepatří	patřit	k5eNaImIp3nS	patřit
ke	k	k7c3	k
špičce	špička	k1gFnSc3	špička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
USA	USA	kA	USA
podle	podle	k7c2	podle
OECD	OECD	kA	OECD
při	při	k7c6	při
srovnání	srovnání	k1gNnSc6	srovnání
délky	délka	k1gFnSc2	délka
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
kojeneckou	kojenecký	k2eAgFnSc7d1	kojenecká
úmrtností	úmrtnost	k1gFnSc7	úmrtnost
nebo	nebo	k8xC	nebo
potenciálně	potenciálně	k6eAd1	potenciálně
ztracených	ztracený	k2eAgNnPc2d1	ztracené
let	léto	k1gNnPc2	léto
života	život	k1gInSc2	život
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zdravotním	zdravotní	k2eAgInSc7d1	zdravotní
stavem	stav	k1gInSc7	stav
umístili	umístit	k5eAaPmAgMnP	umístit
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
třetině	třetina	k1gFnSc6	třetina
států	stát	k1gInPc2	stát
OECD	OECD	kA	OECD
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
USA	USA	kA	USA
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
nejrozšířenějším	rozšířený	k2eAgNnSc7d3	nejrozšířenější
využíváním	využívání	k1gNnSc7	využívání
moderních	moderní	k2eAgFnPc2d1	moderní
medicínských	medicínský	k2eAgFnPc2d1	medicínská
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
USA	USA	kA	USA
neexistuje	existovat	k5eNaImIp3nS	existovat
univerzální	univerzální	k2eAgInSc4d1	univerzální
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
systém	systém	k1gInSc4	systém
zajišťující	zajišťující	k2eAgInSc4d1	zajišťující
péči	péče	k1gFnSc4	péče
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
však	však	k9	však
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vyvíjely	vyvíjet	k5eAaImAgInP	vyvíjet
iniciativu	iniciativa	k1gFnSc4	iniciativa
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
univerzálnímu	univerzální	k2eAgInSc3d1	univerzální
systému	systém	k1gInSc3	systém
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zavedením	zavedení	k1gNnSc7	zavedení
povinného	povinný	k2eAgNnSc2d1	povinné
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
pojištění	pojištění	k1gNnSc2	pojištění
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
chudým	chudý	k2eAgInSc7d1	chudý
dotuje	dotovat	k5eAaBmIp3nS	dotovat
stát	stát	k5eAaPmF	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
státy	stát	k1gInPc4	stát
Minnesota	Minnesota	k1gFnSc1	Minnesota
a	a	k8xC	a
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
USA	USA	kA	USA
financuje	financovat	k5eAaBmIp3nS	financovat
z	z	k7c2	z
daní	daň	k1gFnPc2	daň
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
péči	péče	k1gFnSc4	péče
pro	pro	k7c4	pro
přibližně	přibližně	k6eAd1	přibližně
28	[number]	k4	28
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
nad	nad	k7c4	nad
65	[number]	k4	65
let	léto	k1gNnPc2	léto
a	a	k8xC	a
zdravotně	zdravotně	k6eAd1	zdravotně
postižených	postižený	k2eAgFnPc2d1	postižená
(	(	kIx(	(
<g/>
Medicare	Medicar	k1gMnSc5	Medicar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chudých	chudý	k2eAgFnPc2d1	chudá
(	(	kIx(	(
Medicaid	Medicaida	k1gFnPc2	Medicaida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
z	z	k7c2	z
rodin	rodina	k1gFnPc2	rodina
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
příjmem	příjem	k1gInSc7	příjem
(	(	kIx(	(
<g/>
Children	Childrna	k1gFnPc2	Childrna
<g/>
'	'	kIx"	'
s	s	k7c7	s
Health	Healtha	k1gFnPc2	Healtha
Insurance	Insurance	k1gFnSc1	Insurance
Program	program	k1gInSc1	program
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgMnPc4d1	vojenský
veterány	veterán	k1gMnPc4	veterán
(	(	kIx(	(
<g/>
Veterans	Veterans	k1gInSc1	Veterans
Health	Health	k1gMnSc1	Health
Administration	Administration	k1gInSc1	Administration
<g/>
)	)	kIx)	)
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
aktivní	aktivní	k2eAgFnSc6d1	aktivní
vojenské	vojenský	k2eAgFnSc6d1	vojenská
službě	služba	k1gFnSc6	služba
(	(	kIx(	(
<g/>
Tricare	Tricar	k1gMnSc5	Tricar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
také	také	k9	také
financuje	financovat	k5eAaBmIp3nS	financovat
kliniky	klinika	k1gFnPc4	klinika
poskytující	poskytující	k2eAgFnSc3d1	poskytující
předporodní	předporodní	k2eAgFnSc3d1	předporodní
péči	péče	k1gFnSc3	péče
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
charitativními	charitativní	k2eAgFnPc7d1	charitativní
organizacemi	organizace	k1gFnPc7	organizace
i	i	k8xC	i
většinu	většina	k1gFnSc4	většina
hospiců	hospic	k1gInPc2	hospic
pečujících	pečující	k2eAgInPc2d1	pečující
o	o	k7c4	o
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
očekávaná	očekávaný	k2eAgFnSc1d1	očekávaná
délka	délka	k1gFnSc1	délka
dožití	dožití	k1gNnSc4	dožití
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
šesti	šest	k4xCc2	šest
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
veřejných	veřejný	k2eAgInPc2d1	veřejný
výdajů	výdaj	k1gInPc2	výdaj
na	na	k7c6	na
celkových	celkový	k2eAgInPc6d1	celkový
výdajích	výdaj	k1gInPc6	výdaj
na	na	k7c6	na
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
jiných	jiný	k2eAgInPc2d1	jiný
států	stát	k1gInPc2	stát
OECD	OECD	kA	OECD
vláda	vláda	k1gFnSc1	vláda
USA	USA	kA	USA
nereguluje	regulovat	k5eNaImIp3nS	regulovat
ceny	cena	k1gFnPc4	cena
léků	lék	k1gInPc2	lék
<g/>
,	,	kIx,	,
důsledkem	důsledek	k1gInSc7	důsledek
čehož	což	k3yRnSc2	což
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
ceny	cena	k1gFnPc1	cena
originálních	originální	k2eAgInPc2d1	originální
léků	lék	k1gInPc2	lék
podstatně	podstatně	k6eAd1	podstatně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
tak	tak	k9	tak
příznivé	příznivý	k2eAgFnPc1d1	příznivá
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
nových	nový	k2eAgInPc2d1	nový
léků	lék	k1gInPc2	lék
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
profitují	profitovat	k5eAaBmIp3nP	profitovat
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
výdajů	výdaj	k1gInPc2	výdaj
na	na	k7c4	na
zdravotnictví	zdravotnictví	k1gNnSc4	zdravotnictví
v	v	k7c6	v
USA	USA	kA	USA
směřuje	směřovat	k5eAaImIp3nS	směřovat
do	do	k7c2	do
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
(	(	kIx(	(
<g/>
hlavně	hlavně	k6eAd1	hlavně
aplikovaného	aplikovaný	k2eAgInSc2d1	aplikovaný
<g/>
)	)	kIx)	)
výzkumu	výzkum	k1gInSc2	výzkum
financují	financovat	k5eAaBmIp3nP	financovat
soukromé	soukromý	k2eAgFnPc1d1	soukromá
ziskové	ziskový	k2eAgFnPc1d1	zisková
firmy	firma	k1gFnPc1	firma
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
orientuje	orientovat	k5eAaBmIp3nS	orientovat
zejména	zejména	k9	zejména
na	na	k7c4	na
základní	základní	k2eAgInSc4d1	základní
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
diskuse	diskuse	k1gFnSc1	diskuse
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
hlavně	hlavně	k6eAd1	hlavně
problému	problém	k1gInSc3	problém
rychle	rychle	k6eAd1	rychle
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
nákladů	náklad	k1gInPc2	náklad
<g/>
,	,	kIx,	,
nerovného	rovný	k2eNgInSc2d1	nerovný
přístupu	přístup	k1gInSc2	přístup
ke	k	k7c3	k
zdravotní	zdravotní	k2eAgFnSc3d1	zdravotní
péči	péče	k1gFnSc3	péče
a	a	k8xC	a
nárůstu	nárůst	k1gInSc3	nárůst
počtu	počet	k1gInSc2	počet
nepojištěných	pojištěný	k2eNgFnPc2d1	nepojištěná
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
nedostatečně	dostatečně	k6eNd1	dostatečně
pojištěných	pojištěný	k2eAgMnPc2d1	pojištěný
Američanů	Američan	k1gMnPc2	Američan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
bez	bez	k7c2	bez
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
pojištění	pojištění	k1gNnSc2	pojištění
28,6	[number]	k4	28,6
milionu	milion	k4xCgInSc2	milion
Američanů	Američan	k1gMnPc2	Američan
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvíce	nejvíce	k6eAd1	nejvíce
diskutovaným	diskutovaný	k2eAgNnPc3d1	diskutované
řešením	řešení	k1gNnPc3	řešení
patří	patřit	k5eAaImIp3nP	patřit
různé	různý	k2eAgFnPc1d1	různá
možnosti	možnost	k1gFnPc1	možnost
zavedení	zavedení	k1gNnSc2	zavedení
univerzálního	univerzální	k2eAgInSc2d1	univerzální
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Společnost	společnost	k1gFnSc1	společnost
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Americký	americký	k2eAgInSc1d1	americký
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
USA	USA	kA	USA
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
americký	americký	k2eAgInSc1d1	americký
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
American	American	k1gInSc1	American
way	way	k?	way
of	of	k?	of
life	life	k1gInSc1	life
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Široké	Široké	k2eAgFnPc1d1	Široké
vrstvy	vrstva	k1gFnPc1	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
jsou	být	k5eAaImIp3nP	být
nábožensky	nábožensky	k6eAd1	nábožensky
založené	založený	k2eAgInPc1d1	založený
<g/>
.	.	kIx.	.
</s>
<s>
Společenské	společenský	k2eAgFnPc1d1	společenská
tradice	tradice	k1gFnPc1	tradice
se	se	k3xPyFc4	se
historicky	historicky	k6eAd1	historicky
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
od	od	k7c2	od
puritánství	puritánství	k1gNnSc2	puritánství
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
například	například	k6eAd1	například
prudérní	prudérní	k2eAgInSc1d1	prudérní
filmový	filmový	k2eAgInSc1d1	filmový
rating	rating	k1gInSc1	rating
MPAA	MPAA	kA	MPAA
či	či	k8xC	či
citlivost	citlivost	k1gFnSc1	citlivost
na	na	k7c4	na
sexismus	sexismus	k1gInSc4	sexismus
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
americká	americký	k2eAgFnSc1d1	americká
výjimečnost	výjimečnost	k1gFnSc1	výjimečnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
představa	představa	k1gFnSc1	představa
tzv.	tzv.	kA	tzv.
amerického	americký	k2eAgInSc2d1	americký
snu	sen	k1gInSc2	sen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
ústavě	ústava	k1gFnSc6	ústava
USA	USA	kA	USA
zakotvené	zakotvený	k2eAgNnSc1d1	zakotvené
právo	právo	k1gNnSc1	právo
na	na	k7c4	na
vlastnění	vlastnění	k1gNnSc4	vlastnění
ručních	ruční	k2eAgFnPc2d1	ruční
palných	palný	k2eAgFnPc2d1	palná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dovoleno	dovolen	k2eAgNnSc1d1	dovoleno
nosit	nosit	k5eAaImF	nosit
ruční	ruční	k2eAgFnPc4d1	ruční
zbraně	zbraň	k1gFnPc4	zbraň
viditelně	viditelně	k6eAd1	viditelně
u	u	k7c2	u
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
v	v	k7c6	v
městě	město	k1gNnSc6	město
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
naopak	naopak	k6eAd1	naopak
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Oligarchie	oligarchie	k1gFnSc2	oligarchie
===	===	k?	===
</s>
</p>
<p>
<s>
Dvě	dva	k4xCgFnPc1	dva
americké	americký	k2eAgFnPc1d1	americká
univerzity	univerzita	k1gFnPc1	univerzita
<g/>
,	,	kIx,	,
Northwestern	Northwestern	k1gInSc1	Northwestern
University	universita	k1gFnSc2	universita
a	a	k8xC	a
Princeton	Princeton	k1gInSc4	Princeton
<g/>
,	,	kIx,	,
vypracovaly	vypracovat	k5eAaPmAgInP	vypracovat
společnou	společný	k2eAgFnSc4d1	společná
studii	studie	k1gFnSc4	studie
uveřejněnou	uveřejněný	k2eAgFnSc4d1	uveřejněná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
zkoumaly	zkoumat	k5eAaImAgFnP	zkoumat
1 779	[number]	k4	1 779
politických	politický	k2eAgFnPc2d1	politická
změn	změna	k1gFnPc2	změna
či	či	k8xC	či
návrhů	návrh	k1gInPc2	návrh
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1981	[number]	k4	1981
až	až	k6eAd1	až
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřily	zaměřit	k5eAaPmAgFnP	zaměřit
se	se	k3xPyFc4	se
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
shody	shoda	k1gFnSc2	shoda
těchto	tento	k3xDgFnPc2	tento
změn	změna	k1gFnPc2	změna
s	s	k7c7	s
veřejným	veřejný	k2eAgNnSc7d1	veřejné
míněním	mínění	k1gNnSc7	mínění
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
je	on	k3xPp3gNnSc4	on
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
studií	studie	k1gFnSc7	studie
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatímco	zatímco	k8xS	zatímco
míra	míra	k1gFnSc1	míra
prosazení	prosazení	k1gNnSc2	prosazení
legislativních	legislativní	k2eAgInPc2d1	legislativní
návrhů	návrh	k1gInPc2	návrh
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
nejbohatších	bohatý	k2eAgInPc2d3	nejbohatší
10	[number]	k4	10
%	%	kIx~	%
Američanů	Američan	k1gMnPc2	Američan
měla	mít	k5eAaImAgFnS	mít
lineární	lineární	k2eAgFnSc1d1	lineární
závislost	závislost	k1gFnSc1	závislost
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
preferenci	preference	k1gFnSc4	preference
<g/>
,	,	kIx,	,
prosazení	prosazení	k1gNnSc4	prosazení
legislativních	legislativní	k2eAgFnPc2d1	legislativní
změn	změna	k1gFnPc2	změna
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
průměrných	průměrný	k2eAgMnPc2d1	průměrný
Američanů	Američan	k1gMnPc2	Američan
bylo	být	k5eAaImAgNnS	být
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
moc	moc	k6eAd1	moc
si	se	k3xPyFc3	se
Američané	Američan	k1gMnPc1	Američan
dané	daný	k2eAgFnSc2d1	daná
změny	změna	k1gFnSc2	změna
přejí	přát	k5eAaImIp3nP	přát
nebo	nebo	k8xC	nebo
je	on	k3xPp3gMnPc4	on
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
tak	tak	k9	tak
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
uplatňována	uplatňován	k2eAgFnSc1d1	uplatňována
"	"	kIx"	"
<g/>
vůle	vůle	k1gFnSc1	vůle
lidu	lid	k1gInSc2	lid
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vůle	vůle	k1gFnSc1	vůle
několika	několik	k4yIc2	několik
málo	málo	k6eAd1	málo
elit	elita	k1gFnPc2	elita
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
tak	tak	k9	tak
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
oligarchie	oligarchie	k1gFnSc2	oligarchie
a	a	k8xC	a
neexistenci	neexistence	k1gFnSc4	neexistence
demokracie	demokracie	k1gFnSc2	demokracie
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
elity	elita	k1gFnPc1	elita
a	a	k8xC	a
organizované	organizovaný	k2eAgFnPc1d1	organizovaná
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
zastupující	zastupující	k2eAgInPc1d1	zastupující
velké	velký	k2eAgInPc1d1	velký
zájmy	zájem	k1gInPc1	zájem
byznysu	byznys	k1gInSc2	byznys
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
podstatný	podstatný	k2eAgInSc4d1	podstatný
nezávislý	závislý	k2eNgInSc4d1	nezávislý
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
politiku	politika	k1gFnSc4	politika
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zájmy	zájem	k1gInPc4	zájem
mas	masa	k1gFnPc2	masa
a	a	k8xC	a
průměrných	průměrný	k2eAgMnPc2d1	průměrný
občanů	občan	k1gMnPc2	občan
mají	mít	k5eAaImIp3nP	mít
[	[	kIx(	[
<g/>
na	na	k7c4	na
prosazení	prosazení	k1gNnSc4	prosazení
politických	politický	k2eAgFnPc2d1	politická
změn	změna	k1gFnPc2	změna
<g/>
]	]	kIx)	]
nepatrný	patrný	k2eNgInSc4d1	patrný
nebo	nebo	k8xC	nebo
žádný	žádný	k3yNgInSc4	žádný
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
<g/>
|	|	kIx~	|
<g/>
Studie	studie	k1gFnSc1	studie
Nothwestern	Nothwesterna	k1gFnPc2	Nothwesterna
University	universita	k1gFnSc2	universita
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
PrincetonuNa	PrincetonuNa	k1gFnSc1	PrincetonuNa
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
tzv.	tzv.	kA	tzv.
index	index	k1gInSc4	index
demokracie	demokracie	k1gFnSc2	demokracie
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
ohodnocen	ohodnotit	k5eAaPmNgInS	ohodnotit
hodnotou	hodnota	k1gFnSc7	hodnota
pod	pod	k7c7	pod
8,0	[number]	k4	8,0
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
Ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
jednotky	jednotka	k1gFnSc2	jednotka
(	(	kIx(	(
<g/>
Economist	Economist	k1gInSc1	Economist
Intelligence	Intelligence	k1gFnSc2	Intelligence
Unit	Unita	k1gFnPc2	Unita
<g/>
,	,	kIx,	,
EIU	EIU	kA	EIU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hodnocení	hodnocení	k1gNnSc3	hodnocení
prováděla	provádět	k5eAaImAgFnS	provádět
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
přesunula	přesunout	k5eAaPmAgFnS	přesunout
z	z	k7c2	z
kategorie	kategorie	k1gFnSc2	kategorie
"	"	kIx"	"
<g/>
plná	plný	k2eAgFnSc1d1	plná
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
"	"	kIx"	"
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
"	"	kIx"	"
<g/>
vadná	vadný	k2eAgFnSc1d1	vadná
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
"	"	kIx"	"
–	–	k?	–
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Chudoba	Chudoba	k1gMnSc1	Chudoba
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Studie	studie	k1gFnSc1	studie
OSN	OSN	kA	OSN
z	z	k7c2	z
května	květen	k1gInSc2	květen
2018	[number]	k4	2018
konstatovala	konstatovat	k5eAaBmAgFnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
Američanů	Američan	k1gMnPc2	Američan
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
chudobě	chudoba	k1gFnSc6	chudoba
a	a	k8xC	a
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
třetího	třetí	k4xOgInSc2	třetí
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
americké	americký	k2eAgFnSc2d1	americká
národní	národní	k2eAgFnSc2d1	národní
koalice	koalice	k1gFnSc2	koalice
proti	proti	k7c3	proti
bezdomovectví	bezdomovectví	k1gNnSc3	bezdomovectví
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
má	mít	k5eAaImIp3nS	mít
44	[number]	k4	44
%	%	kIx~	%
bezdomovců	bezdomovec	k1gMnPc2	bezdomovec
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
přesto	přesto	k8xC	přesto
nedokáží	dokázat	k5eNaPmIp3nP	dokázat
bezdomovectví	bezdomovectví	k1gNnSc4	bezdomovectví
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Kriminalita	kriminalita	k1gFnSc1	kriminalita
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
kriminalita	kriminalita	k1gFnSc1	kriminalita
relativně	relativně	k6eAd1	relativně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
4,3	[number]	k4	4,3
vraždy	vražda	k1gFnSc2	vražda
na	na	k7c4	na
100 000	[number]	k4	100 000
obyvatel	obyvatel	k1gMnPc2	obyvatel
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
ale	ale	k9	ale
liší	lišit	k5eAaImIp3nP	lišit
dle	dle	k7c2	dle
státu	stát	k1gInSc2	stát
a	a	k8xC	a
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Průřezová	průřezový	k2eAgFnSc1d1	průřezová
analýza	analýza	k1gFnSc1	analýza
Světové	světový	k2eAgFnSc2d1	světová
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
organizace	organizace	k1gFnSc2	organizace
Mortality	mortalita	k1gFnSc2	mortalita
Database	Databasa	k1gFnSc3	Databasa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
míra	míra	k1gFnSc1	míra
vražd	vražda	k1gFnPc2	vražda
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
sedmkrát	sedmkrát	k6eAd1	sedmkrát
vyšší	vysoký	k2eAgFnPc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
rozvinutých	rozvinutý	k2eAgFnPc6d1	rozvinutá
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
nicméně	nicméně	k8xC	nicméně
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c6	na
celosvětovém	celosvětový	k2eAgInSc6d1	celosvětový
průměru	průměr	k1gInSc6	průměr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
vražd	vražda	k1gFnPc2	vražda
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
střelné	střelný	k2eAgFnSc2d1	střelná
zbraně	zbraň	k1gFnSc2	zbraň
pak	pak	k6eAd1	pak
25	[number]	k4	25
<g/>
,	,	kIx,	,
<g/>
2krát	2krát	k1gInSc1	2krát
vyšší	vysoký	k2eAgInSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečná	bezpečný	k2eNgNnPc1d1	nebezpečné
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
ghetta	ghetto	k1gNnPc1	ghetto
obývaná	obývaný	k2eAgNnPc1d1	obývané
městkou	městka	k1gFnSc7	městka
chudinou	chudina	k1gFnSc7	chudina
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
mírou	míra	k1gFnSc7	míra
kriminality	kriminalita	k1gFnSc2	kriminalita
je	být	k5eAaImIp3nS	být
Detroit	Detroit	k1gInSc1	Detroit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
ukazatel	ukazatel	k1gInSc4	ukazatel
vražd	vražda	k1gFnPc2	vražda
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
desetinásobku	desetinásobek	k1gInSc3	desetinásobek
amerického	americký	k2eAgInSc2d1	americký
průměru	průměr	k1gInSc2	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
států	stát	k1gInPc2	stát
měla	mít	k5eAaImAgFnS	mít
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2012	[number]	k4	2012
největší	veliký	k2eAgFnSc1d3	veliký
vražednost	vražednost	k1gFnSc1	vražednost
Louisiana	Louisiana	k1gFnSc1	Louisiana
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
pak	pak	k6eAd1	pak
New	New	k1gMnSc5	New
Hampshire	Hampshir	k1gMnSc5	Hampshir
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
nosit	nosit	k5eAaImF	nosit
zbraň	zbraň	k1gFnSc4	zbraň
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
Američanům	Američan	k1gMnPc3	Američan
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
dodatků	dodatek	k1gInPc2	dodatek
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
se	se	k3xPyFc4	se
o	o	k7c6	o
věci	věc	k1gFnSc6	věc
vede	vést	k5eAaImIp3nS	vést
široká	široký	k2eAgFnSc1d1	široká
společenská	společenský	k2eAgFnSc1d1	společenská
diskuse	diskuse	k1gFnSc1	diskuse
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
FBI	FBI	kA	FBI
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1980-2008	[number]	k4	1980-2008
spáchali	spáchat	k5eAaPmAgMnP	spáchat
90	[number]	k4	90
<g/>
%	%	kIx~	%
vražd	vražda	k1gFnPc2	vražda
muži	muž	k1gMnPc1	muž
a	a	k8xC	a
52,5	[number]	k4	52,5
<g/>
%	%	kIx~	%
černoši	černoch	k1gMnPc1	černoch
<g/>
.	.	kIx.	.
</s>
<s>
Černoši	černoch	k1gMnPc1	černoch
tak	tak	k9	tak
v	v	k7c6	v
USA	USA	kA	USA
páchají	páchat	k5eAaImIp3nP	páchat
vraždy	vražda	k1gFnSc2	vražda
osmkrát	osmkrát	k6eAd1	osmkrát
častěji	často	k6eAd2	často
než	než	k8xS	než
bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
(	(	kIx(	(
<g/>
počítáno	počítat	k5eAaImNgNnS	počítat
v	v	k7c4	v
to	ten	k3xDgNnSc4	ten
i	i	k9	i
Hispánce	Hispánec	k1gMnPc4	Hispánec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vražd	vražda	k1gFnPc2	vražda
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
"	"	kIx"	"
<g/>
uvnitř	uvnitř	k7c2	uvnitř
jedné	jeden	k4xCgFnSc2	jeden
rasy	rasa	k1gFnSc2	rasa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
93	[number]	k4	93
<g/>
%	%	kIx~	%
zavražděných	zavražděný	k2eAgMnPc2d1	zavražděný
černochů	černoch	k1gMnPc2	černoch
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
černošské	černošský	k2eAgMnPc4d1	černošský
vrahy	vrah	k1gMnPc4	vrah
<g/>
,	,	kIx,	,
84	[number]	k4	84
<g/>
%	%	kIx~	%
bílých	bílý	k2eAgFnPc2d1	bílá
obětí	oběť	k1gFnPc2	oběť
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
bílými	bílý	k2eAgFnPc7d1	bílá
vrahy	vrah	k1gMnPc4	vrah
<g/>
.	.	kIx.	.
<g/>
Třicet	třicet	k4xCc1	třicet
z	z	k7c2	z
padesáti	padesát	k4xCc2	padesát
států	stát	k1gInPc2	stát
federace	federace	k1gFnSc2	federace
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
zákonodárství	zákonodárství	k1gNnSc1	zákonodárství
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
USA	USA	kA	USA
popravy	poprava	k1gFnPc1	poprava
nevykonávaly	vykonávat	k5eNaImAgFnP	vykonávat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
po	po	k7c6	po
průlomovém	průlomový	k2eAgNnSc6d1	průlomové
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soudu	soud	k1gInSc2	soud
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
byla	být	k5eAaImAgFnS	být
praxe	praxe	k1gFnSc1	praxe
obnovena	obnoven	k2eAgFnSc1d1	obnovena
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
vykonáno	vykonat	k5eAaPmNgNnS	vykonat
zhruba	zhruba	k6eAd1	zhruba
1300	[number]	k4	1300
proprav	proprava	k1gFnPc2	proprava
<g/>
,	,	kIx,	,
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
státech	stát	k1gInPc6	stát
<g/>
:	:	kIx,	:
Texas	Texas	k1gInSc1	Texas
<g/>
,	,	kIx,	,
Virginie	Virginie	k1gFnSc1	Virginie
a	a	k8xC	a
Oklahoma	Oklahoma	k1gFnSc1	Oklahoma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
měly	mít	k5eAaImAgInP	mít
díky	díky	k7c3	díky
tomu	ten	k3xDgMnSc3	ten
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
pátý	pátý	k4xOgInSc1	pátý
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
počet	počet	k1gInSc1	počet
poprav	poprava	k1gFnPc2	poprava
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
po	po	k7c6	po
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Íránu	Írán	k1gInSc6	Írán
<g/>
,	,	kIx,	,
Pákistánu	Pákistán	k1gInSc6	Pákistán
a	a	k8xC	a
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
mají	mít	k5eAaImIp3nP	mít
rovněž	rovněž	k9	rovněž
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
počet	počet	k1gInSc4	počet
uvězněných	uvězněný	k2eAgFnPc2d1	uvězněná
osob	osoba	k1gFnPc2	osoba
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
vězňů	vězeň	k1gMnPc2	vězeň
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
zečtyřnásobil	zečtyřnásobit	k5eAaPmAgMnS	zečtyřnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kritiků	kritik	k1gMnPc2	kritik
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
trend	trend	k1gInSc4	trend
vliv	vliv	k1gInSc1	vliv
privatizace	privatizace	k1gFnSc2	privatizace
vězeňství	vězeňství	k1gNnSc2	vězeňství
v	v	k7c6	v
80.	[number]	k4	80.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
však	však	k9	však
byla	být	k5eAaImAgFnS	být
patrně	patrně	k6eAd1	patrně
změna	změna	k1gFnSc1	změna
postoje	postoj	k1gInSc2	postoj
státu	stát	k1gInSc2	stát
k	k	k7c3	k
drogám	droga	k1gFnPc3	droga
od	od	k7c2	od
80.	[number]	k4	80.
let	léto	k1gNnPc2	léto
–	–	k?	–
podle	podle	k7c2	podle
Federal	Federal	k1gFnSc2	Federal
Bureau	Bureaus	k1gInSc2	Bureaus
of	of	k?	of
Prisons	Prisonsa	k1gFnPc2	Prisonsa
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
vězňů	vězeň	k1gMnPc2	vězeň
ve	v	k7c6	v
federálních	federální	k2eAgFnPc6d1	federální
věznicích	věznice	k1gFnPc6	věznice
odsouzena	odsoudit	k5eAaPmNgFnS	odsoudit
za	za	k7c2	za
trestné	trestný	k2eAgFnSc2d1	trestná
činy	čina	k1gFnSc2	čina
spjaté	spjatý	k2eAgFnPc4d1	spjatá
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
míra	míra	k1gFnSc1	míra
uvězněnosti	uvězněnost	k1gFnSc2	uvězněnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Oklahomě	Oklahomě	k1gFnSc6	Oklahomě
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
v	v	k7c6	v
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
je	být	k5eAaImIp3nS	být
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
národní	národní	k2eAgFnSc7d1	národní
ekonomikou	ekonomika	k1gFnSc7	ekonomika
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
největší	veliký	k2eAgInSc4d3	veliký
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
celek	celek	k1gInSc4	celek
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
za	za	k7c7	za
ekonomikou	ekonomika	k1gFnSc7	ekonomika
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
)	)	kIx)	)
a	a	k8xC	a
země	země	k1gFnSc1	země
s	s	k7c7	s
druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
objemem	objem	k1gInSc7	objem
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
(	(	kIx(	(
<g/>
za	za	k7c7	za
Čínou	Čína	k1gFnSc7	Čína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
hrubý	hrubý	k2eAgInSc1d1	hrubý
národní	národní	k2eAgInSc1d1	národní
produkt	produkt	k1gInSc1	produkt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
14,991	[number]	k4	14,991
bilionu	bilion	k4xCgInSc2	bilion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
s	s	k7c7	s
48 350	[number]	k4	48 350
dolary	dolar	k1gInPc7	dolar
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomiku	ekonomika	k1gFnSc4	ekonomika
USA	USA	kA	USA
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
služby	služba	k1gFnPc4	služba
a	a	k8xC	a
výzkum	výzkum	k1gInSc4	výzkum
<g/>
;	;	kIx,	;
primární	primární	k2eAgInSc4d1	primární
(	(	kIx(	(
<g/>
suroviny-zpracovávající	surovinypracovávající	k2eAgInSc4d1	suroviny-zpracovávající
<g/>
)	)	kIx)	)
průmysl	průmysl	k1gInSc4	průmysl
tvoří	tvořit	k5eAaImIp3nP	tvořit
jen	jen	k9	jen
nepatrnou	nepatrný	k2eAgFnSc4d1	nepatrná
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
tvoří	tvořit	k5eAaImIp3nS	tvořit
pouhé	pouhý	k2eAgNnSc4d1	pouhé
1	[number]	k4	1
%	%	kIx~	%
hrubého	hrubý	k2eAgInSc2d1	hrubý
národního	národní	k2eAgInSc2d1	národní
produktu	produkt	k1gInSc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
oblasti	oblast	k1gFnSc2	oblast
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Silicon	Silicon	kA	Silicon
Valley	Vallea	k1gFnSc2	Vallea
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
také	také	k9	také
řada	řada	k1gFnSc1	řada
technologických	technologický	k2eAgFnPc2d1	technologická
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
známými	známý	k2eAgFnPc7d1	známá
firmami	firma	k1gFnPc7	firma
najdeme	najít	k5eAaPmIp1nP	najít
společnosti	společnost	k1gFnSc2	společnost
Google	Google	k1gFnSc2	Google
<g/>
,	,	kIx,	,
Amazon.com	Amazon.com	k1gInSc4	Amazon.com
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
<g/>
,	,	kIx,	,
Facebook	Facebook	k1gInSc4	Facebook
<g/>
,	,	kIx,	,
Apple	Apple	kA	Apple
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
problémy	problém	k1gInPc7	problém
americké	americký	k2eAgFnSc2d1	americká
ekonomiky	ekonomika	k1gFnSc2	ekonomika
patří	patřit	k5eAaImIp3nP	patřit
státní	státní	k2eAgInSc4d1	státní
a	a	k8xC	a
veřejný	veřejný	k2eAgInSc4d1	veřejný
dluh	dluh	k1gInSc4	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
měly	mít	k5eAaImAgInP	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
státní	státní	k2eAgInSc4d1	státní
dluh	dluh	k1gInSc4	dluh
zhruba	zhruba	k6eAd1	zhruba
18 151	[number]	k4	18 151
miliard	miliarda	k4xCgFnPc2	miliarda
$	$	kIx~	$
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
102	[number]	k4	102
%	%	kIx~	%
jeho	jeho	k3xOp3gFnSc1	jeho
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
největší	veliký	k2eAgInSc1d3	veliký
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
dluh	dluh	k1gInSc1	dluh
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
11.	[number]	k4	11.
největší	veliký	k2eAgInSc4d3	veliký
vládní	vládní	k2eAgInSc4d1	vládní
dluh	dluh	k1gInSc4	dluh
na	na	k7c4	na
%	%	kIx~	%
HDP	HDP	kA	HDP
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
problémy	problém	k1gInPc4	problém
patří	patřit	k5eAaImIp3nP	patřit
korporátní	korporátní	k2eAgInPc1d1	korporátní
dluhopisy	dluhopis	k1gInPc1	dluhopis
<g/>
,	,	kIx,	,
hypoteční	hypoteční	k2eAgFnSc1d1	hypoteční
krize	krize	k1gFnSc1	krize
vyvolaná	vyvolaný	k2eAgFnSc1d1	vyvolaná
pádem	pád	k1gInSc7	pád
cen	cena	k1gFnPc2	cena
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
,	,	kIx,	,
nízké	nízký	k2eAgInPc4d1	nízký
úroky	úrok	k1gInPc4	úrok
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
negativní	negativní	k2eAgNnSc4d1	negativní
saldo	saldo	k1gNnSc4	saldo
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
vzrůstající	vzrůstající	k2eAgFnSc2d1	vzrůstající
inflace	inflace	k1gFnSc2	inflace
a	a	k8xC	a
uspokojení	uspokojení	k1gNnSc2	uspokojení
nároků	nárok	k1gInPc2	nárok
tzv.	tzv.	kA	tzv.
baby	baby	k1gNnSc1	baby
boom	boom	k1gInSc1	boom
generace	generace	k1gFnSc1	generace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
začíná	začínat	k5eAaImIp3nS	začínat
čerpat	čerpat	k5eAaImF	čerpat
sociální	sociální	k2eAgInPc4d1	sociální
důchody	důchod	k1gInPc4	důchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgNnSc4d3	veliký
obchodní	obchodní	k2eAgNnSc4d1	obchodní
odvětví	odvětví	k1gNnSc4	odvětví
patří	patřit	k5eAaImIp3nS	patřit
maloobchodní	maloobchodní	k2eAgInSc1d1	maloobchodní
a	a	k8xC	a
velkoobchodní	velkoobchodní	k2eAgInSc1d1	velkoobchodní
prodej	prodej	k1gInSc1	prodej
<g/>
,	,	kIx,	,
sektor	sektor	k1gInSc1	sektor
(	(	kIx(	(
<g/>
finančních	finanční	k2eAgFnPc2d1	finanční
<g/>
,	,	kIx,	,
obchodních	obchodní	k2eAgFnPc2d1	obchodní
<g/>
,	,	kIx,	,
zdravotnických	zdravotnický	k2eAgFnPc2d1	zdravotnická
<g/>
,	,	kIx,	,
sociálních	sociální	k2eAgFnPc2d1	sociální
<g/>
)	)	kIx)	)
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
proporcionálně	proporcionálně	k6eAd1	proporcionálně
vysoký	vysoký	k2eAgInSc1d1	vysoký
podíl	podíl	k1gInSc1	podíl
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
věda	věda	k1gFnSc1	věda
a	a	k8xC	a
výzkum	výzkum	k1gInSc1	výzkum
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
zábavní	zábavní	k2eAgInSc1d1	zábavní
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
mají	mít	k5eAaImIp3nP	mít
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
spotřebu	spotřeba	k1gFnSc4	spotřeba
na	na	k7c6	na
světě	svět	k1gInSc6	svět
co	co	k8xS	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
elektřiny	elektřina	k1gFnSc2	elektřina
a	a	k8xC	a
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgFnPc2d1	další
komodit	komodita	k1gFnPc2	komodita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
obchodními	obchodní	k2eAgMnPc7d1	obchodní
partnery	partner	k1gMnPc7	partner
jsou	být	k5eAaImIp3nP	být
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
včetně	včetně	k7c2	včetně
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
obchod	obchod	k1gInSc1	obchod
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
schodku	schodek	k1gInSc6	schodek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
začal	začít	k5eAaPmAgMnS	začít
prohlubovat	prohlubovat	k5eAaImF	prohlubovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
negativní	negativní	k2eAgFnSc1d1	negativní
bilance	bilance	k1gFnSc1	bilance
(	(	kIx(	(
<g/>
s	s	k7c7	s
jedinou	jediný	k2eAgFnSc7d1	jediná
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
)	)	kIx)	)
hranici	hranice	k1gFnSc6	hranice
400	[number]	k4	400
miliard	miliarda	k4xCgFnPc2	miliarda
$	$	kIx~	$
ročně	ročně	k6eAd1	ročně
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
informační	informační	k2eAgFnSc1d1	informační
grafika	grafika	k1gFnSc1	grafika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
dluh	dluh	k1gInSc1	dluh
USA	USA	kA	USA
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
koncem	koncem	k7c2	koncem
října	říjen	k1gInSc2	říjen
2018	[number]	k4	2018
částku	částka	k1gFnSc4	částka
6 200	[number]	k4	6 200
miliard	miliarda	k4xCgFnPc2	miliarda
$	$	kIx~	$
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
největšími	veliký	k2eAgMnPc7d3	veliký
zahraničními	zahraniční	k2eAgMnPc7d1	zahraniční
věřiteli	věřitel	k1gMnPc7	věřitel
jsou	být	k5eAaImIp3nP	být
Čína	Čína	k1gFnSc1	Čína
(	(	kIx(	(
<g/>
1139	[number]	k4	1139
miliard	miliarda	k4xCgFnPc2	miliarda
$	$	kIx~	$
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
(	(	kIx(	(
<g/>
1019	[number]	k4	1019
miliard	miliarda	k4xCgFnPc2	miliarda
$	$	kIx~	$
<g/>
)	)	kIx)	)
a	a	k8xC	a
Brazílie	Brazílie	k1gFnSc1	Brazílie
(	(	kIx(	(
<g/>
314	[number]	k4	314
miliard	miliarda	k4xCgFnPc2	miliarda
$	$	kIx~	$
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Sociální	sociální	k2eAgFnSc1d1	sociální
reformy	reforma	k1gFnPc1	reforma
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Během	během	k7c2	během
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
období	období	k1gNnPc4	období
Baracka	Baracko	k1gNnSc2	Baracko
Obamy	Obama	k1gFnSc2	Obama
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
posunu	posun	k1gInSc3	posun
v	v	k7c6	v
schvalování	schvalování	k1gNnSc6	schvalování
dvou	dva	k4xCgFnPc2	dva
zásadních	zásadní	k2eAgFnPc2d1	zásadní
reforem	reforma	k1gFnPc2	reforma
–	–	k?	–
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
přistěhovalectví	přistěhovalectví	k1gNnSc1	přistěhovalectví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reforma	reforma	k1gFnSc1	reforma
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
pojištění	pojištění	k1gNnSc2	pojištění
–	–	k?	–
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
pojištění	pojištění	k1gNnSc4	pojištění
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
záležitostí	záležitost	k1gFnPc2	záležitost
každého	každý	k3xTgMnSc2	každý
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
povinné	povinný	k2eAgNnSc1d1	povinné
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
jsou	být	k5eAaImIp3nP	být
pojišťovny	pojišťovna	k1gFnPc1	pojišťovna
soukromými	soukromý	k2eAgFnPc7d1	soukromá
firmami	firma	k1gFnPc7	firma
<g/>
,	,	kIx,	,
výše	vysoce	k6eAd2	vysoce
pojištění	pojištění	k1gNnSc2	pojištění
se	se	k3xPyFc4	se
neodvíjí	odvíjet	k5eNaImIp3nS	odvíjet
od	od	k7c2	od
výše	výše	k1gFnSc2	výše
příjmu	příjem	k1gInSc2	příjem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
dle	dle	k7c2	dle
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
prodělaných	prodělaný	k2eAgFnPc2d1	prodělaná
nemocí	nemoc	k1gFnPc2	nemoc
atd.	atd.	kA	atd.
To	to	k9	to
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
rodin	rodina	k1gFnPc2	rodina
s	s	k7c7	s
nízkými	nízký	k2eAgInPc7d1	nízký
příjmy	příjem	k1gInPc7	příjem
si	se	k3xPyFc3	se
nemůže	moct	k5eNaImIp3nS	moct
dovolit	dovolit	k5eAaPmF	dovolit
pojištění	pojištění	k1gNnSc4	pojištění
platit	platit	k5eAaImF	platit
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
statistiky	statistika	k1gFnSc2	statistika
National	National	k1gMnSc2	National
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
Health	Healtha	k1gFnPc2	Healtha
Statistics	Statisticsa	k1gFnPc2	Statisticsa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
45,5	[number]	k4	45,5
milionů	milion	k4xCgInPc2	milion
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
14,7	[number]	k4	14,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
osob	osoba	k1gFnPc2	osoba
bylo	být	k5eAaImAgNnS	být
nepojištěno	pojistit	k5eNaPmNgNnS	pojistit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
rozhovoru	rozhovor	k1gInSc2	rozhovor
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
57,7	[number]	k4	57,7
milionů	milion	k4xCgInPc2	milion
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
6	[number]	k4	6
%	%	kIx~	%
<g/>
)	)	kIx)	)
nebylo	být	k5eNaImAgNnS	být
pojištěno	pojistit	k5eAaPmNgNnS	pojistit
alespoň	alespoň	k9	alespoň
část	část	k1gFnSc1	část
roku	rok	k1gInSc2	rok
předcházejícímu	předcházející	k2eAgNnSc3d1	předcházející
interview	interview	k1gNnSc3	interview
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
34,1	[number]	k4	34,1
milionů	milion	k4xCgInPc2	milion
nebylo	být	k5eNaImAgNnS	být
pojištěno	pojistit	k5eAaPmNgNnS	pojistit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
bylo	být	k5eAaImAgNnS	být
nepojištěno	pojištěn	k2eNgNnSc4d1	nepojištěno
4 900 000	[number]	k4	4 900 000
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
6	[number]	k4	6
%	%	kIx~	%
<g/>
)	)	kIx)	)
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
18let	18leta	k1gFnPc2	18leta
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
statistiky	statistika	k1gFnSc2	statistika
také	také	k9	také
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
neplatí	platit	k5eNaImIp3nP	platit
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
pojištění	pojištění	k1gNnSc4	pojištění
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
klesá	klesat	k5eAaImIp3nS	klesat
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
%	%	kIx~	%
neplatičů	neplatič	k1gMnPc2	neplatič
nepochází	pocházet	k5eNaImIp3nS	pocházet
z	z	k7c2	z
rodin	rodina	k1gFnPc2	rodina
s	s	k7c7	s
nízkými	nízký	k2eAgInPc7d1	nízký
příjmy	příjem	k1gInPc7	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Reforma	reforma	k1gFnSc1	reforma
zvaná	zvaný	k2eAgFnSc1d1	zvaná
"	"	kIx"	"
<g/>
Obamacare	Obamacar	k1gMnSc5	Obamacar
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Patient	Patient	k1gInSc1	Patient
Protection	Protection	k1gInSc1	Protection
and	and	k?	and
Affordable	Affordable	k1gMnSc5	Affordable
Care	car	k1gMnSc5	car
Act	Act	k1gMnSc5	Act
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
začít	začít	k5eAaPmF	začít
platit	platit	k5eAaImF	platit
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
občany	občan	k1gMnPc4	občan
USA	USA	kA	USA
zajistit	zajistit	k5eAaPmF	zajistit
podstatně	podstatně	k6eAd1	podstatně
větší	veliký	k2eAgFnSc4d2	veliký
dostupnost	dostupnost	k1gFnSc4	dostupnost
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
pojištění	pojištění	k1gNnSc2	pojištění
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
například	například	k6eAd1	například
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
firmám	firma	k1gFnPc3	firma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
pojištění	pojištění	k1gNnSc4	pojištění
<g/>
,	,	kIx,	,
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
diskriminaci	diskriminace	k1gFnSc4	diskriminace
–	–	k?	–
tedy	tedy	k8xC	tedy
posuzovat	posuzovat	k5eAaImF	posuzovat
výši	výše	k1gFnSc4	výše
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
pojistky	pojistka	k1gFnSc2	pojistka
klienta	klient	k1gMnSc2	klient
podle	podle	k7c2	podle
jeho	on	k3xPp3gInSc2	on
předchozího	předchozí	k2eAgInSc2d1	předchozí
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
stavu	stav	k1gInSc2	stav
či	či	k8xC	či
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bylo	být	k5eAaImAgNnS	být
doposud	doposud	k6eAd1	doposud
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
pojištění	pojištění	k1gNnSc4	pojištění
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
nedostupné	dostupný	k2eNgFnPc1d1	nedostupná
pro	pro	k7c4	pro
vážně	vážně	k6eAd1	vážně
nemocné	mocný	k2eNgMnPc4d1	nemocný
a	a	k8xC	a
sociálně	sociálně	k6eAd1	sociálně
slabé	slabý	k2eAgMnPc4d1	slabý
občany	občan	k1gMnPc4	občan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Imigrační	imigrační	k2eAgFnSc1d1	imigrační
reforma	reforma	k1gFnSc1	reforma
–	–	k?	–
v	v	k7c6	v
USA	USA	kA	USA
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
11	[number]	k4	11
milionů	milion	k4xCgInPc2	milion
nelegálních	legální	k2eNgMnPc2d1	nelegální
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Názory	názor	k1gInPc4	názor
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
ekonomiku	ekonomika	k1gFnSc4	ekonomika
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nP	různit
<g/>
,	,	kIx,	,
od	od	k7c2	od
pozitivního	pozitivní	k2eAgInSc2d1	pozitivní
přínosu	přínos	k1gInSc2	přínos
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
poměru	poměr	k1gInSc3	poměr
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
vyjádřeno	vyjádřit	k5eAaPmNgNnS	vyjádřit
práce	práce	k1gFnSc1	práce
<g/>
/	/	kIx~	/
<g/>
mzda	mzda	k1gFnSc1	mzda
<g/>
/	/	kIx~	/
<g/>
útrata	útrata	k1gFnSc1	útrata
–	–	k?	–
nekvalifikovaná	kvalifikovaný	k2eNgFnSc1d1	nekvalifikovaná
práce	práce	k1gFnSc1	práce
za	za	k7c4	za
co	co	k9	co
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
mzdu	mzda	k1gFnSc4	mzda
<g/>
(	(	kIx(	(
<g/>
neakceptovatelnými	akceptovatelný	k2eNgInPc7d1	neakceptovatelný
pro	pro	k7c4	pro
Američany	Američan	k1gMnPc4	Američan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
utracena	utracen	k2eAgFnSc1d1	utracena
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
k	k	k7c3	k
negativní	negativní	k2eAgFnSc3d1	negativní
dopadům	dopad	k1gInPc3	dopad
souvisejícím	související	k2eAgInPc3d1	související
s	s	k7c7	s
šedou	šedý	k2eAgFnSc7d1	šedá
ekonomikou	ekonomika	k1gFnSc7	ekonomika
<g/>
,	,	kIx,	,
vzděláváním	vzdělávání	k1gNnSc7	vzdělávání
<g/>
,	,	kIx,	,
zdravotní	zdravotní	k2eAgFnSc7d1	zdravotní
péčí	péče	k1gFnSc7	péče
<g/>
,	,	kIx,	,
<g/>
občanskými	občanský	k2eAgNnPc7d1	občanské
právy	právo	k1gNnPc7	právo
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Dne	den	k1gInSc2	den
27.	[number]	k4	27.
6.	[number]	k4	6.
2013	[number]	k4	2013
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
USA	USA	kA	USA
k	k	k7c3	k
většinovému	většinový	k2eAgNnSc3d1	většinové
schválení	schválení	k1gNnSc3	schválení
"	"	kIx"	"
<g/>
Border	Border	k1gInSc4	Border
Security	Securita	k1gFnSc2	Securita
<g/>
,	,	kIx,	,
Economic	Economic	k1gMnSc1	Economic
Opportunity	Opportunita	k1gFnSc2	Opportunita
<g/>
,	,	kIx,	,
and	and	k?	and
Immigration	Immigration	k1gInSc1	Immigration
Modernization	Modernization	k1gInSc1	Modernization
Act	Act	k1gFnSc1	Act
of	of	k?	of
2013	[number]	k4	2013
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reforma	reforma	k1gFnSc1	reforma
by	by	kYmCp3nS	by
umožnila	umožnit	k5eAaPmAgFnS	umožnit
zlegalizování	zlegalizování	k1gNnSc3	zlegalizování
pobytu	pobyt	k1gInSc2	pobyt
imigrantů	imigrant	k1gMnPc2	imigrant
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
získání	získání	k1gNnSc1	získání
občanství	občanství	k1gNnSc2	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
systém	systém	k1gInSc1	systém
vydávání	vydávání	k1gNnSc2	vydávání
legálních	legální	k2eAgNnPc2d1	legální
víz	vízo	k1gNnPc2	vízo
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
ekonomických	ekonomický	k2eAgFnPc6d1	ekonomická
potřebách	potřeba	k1gFnPc6	potřeba
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
kontroly	kontrola	k1gFnPc1	kontrola
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reforma	reforma	k1gFnSc1	reforma
musí	muset	k5eAaImIp3nS	muset
ještě	ještě	k6eAd1	ještě
projít	projít	k5eAaPmF	projít
schválením	schválení	k1gNnSc7	schválení
v	v	k7c6	v
Kongresu	kongres	k1gInSc6	kongres
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
Obama	Obama	k?	Obama
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
snažil	snažit	k5eAaImAgMnS	snažit
imigrační	imigrační	k2eAgFnSc4d1	imigrační
reformu	reforma	k1gFnSc4	reforma
prosadit	prosadit	k5eAaPmF	prosadit
pomocí	pomocí	k7c2	pomocí
exekutivních	exekutivní	k2eAgNnPc2d1	exekutivní
nařízení	nařízení	k1gNnPc2	nařízení
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
obcházejí	obcházet	k5eAaImIp3nP	obcházet
Kongres	kongres	k1gInSc4	kongres
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
podle	podle	k7c2	podle
kritiků	kritik	k1gMnPc2	kritik
porušil	porušit	k5eAaPmAgMnS	porušit
americkou	americký	k2eAgFnSc4d1	americká
Ústavu	ústava	k1gFnSc4	ústava
a	a	k8xC	a
proto	proto	k8xC	proto
celkem	celkem	k6eAd1	celkem
26	[number]	k4	26
amerických	americký	k2eAgInPc2d1	americký
států	stát	k1gInPc2	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Texasem	Texas	k1gInSc7	Texas
podalo	podat	k5eAaPmAgNnS	podat
na	na	k7c4	na
Obamovu	Obamův	k2eAgFnSc4d1	Obamova
administrativu	administrativa	k1gFnSc4	administrativa
žalobu	žaloba	k1gFnSc4	žaloba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Turismus	turismus	k1gInSc4	turismus
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgNnPc4d3	nejvýznamnější
centra	centrum	k1gNnPc4	centrum
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
patří	patřit	k5eAaImIp3nP	patřit
velkoměsta	velkoměsto	k1gNnPc4	velkoměsto
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
turistů	turist	k1gMnPc2	turist
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
národní	národní	k2eAgInPc4d1	národní
parky	park	k1gInPc4	park
(	(	kIx(	(
<g/>
Yellowstonský	Yellowstonský	k2eAgInSc4d1	Yellowstonský
<g/>
,	,	kIx,	,
Yosemitský	Yosemitský	k2eAgInSc4d1	Yosemitský
<g/>
,	,	kIx,	,
Sequoia	Sequoia	k1gFnSc1	Sequoia
<g/>
,	,	kIx,	,
Grand	grand	k1gMnSc1	grand
Canyon	Canyon	k1gMnSc1	Canyon
<g/>
,	,	kIx,	,
Everglades	Everglades	k1gMnSc1	Everglades
<g/>
,	,	kIx,	,
Redwood	Redwood	k1gInSc1	Redwood
<g/>
,	,	kIx,	,
Mamutí	mamutí	k2eAgFnSc1d1	mamutí
jeskyně	jeskyně	k1gFnSc1	jeskyně
<g/>
,	,	kIx,	,
Olympic	Olympic	k1gMnSc1	Olympic
<g/>
,	,	kIx,	,
Great	Great	k2eAgInSc1d1	Great
Smoky	smok	k1gMnPc7	smok
Mountains	Mountainsa	k1gFnPc2	Mountainsa
<g/>
,	,	kIx,	,
Glacier	Glacira	k1gFnPc2	Glacira
<g/>
,	,	kIx,	,
Carlsbadské	Carlsbadský	k2eAgFnPc1d1	Carlsbadský
jeskyně	jeskyně	k1gFnPc1	jeskyně
<g/>
,	,	kIx,	,
Skalnaté	skalnatý	k2eAgFnPc1d1	skalnatá
hory	hora	k1gFnPc1	hora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejnavštěvovanější	navštěvovaný	k2eAgFnSc7d3	nejnavštěvovanější
přírodní	přírodní	k2eAgFnSc7d1	přírodní
památkou	památka	k1gFnSc7	památka
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
Niagarské	niagarský	k2eAgInPc1d1	niagarský
vodopády	vodopád	k1gInPc1	vodopád
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Kanadou	Kanada	k1gFnSc7	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
amerického	americký	k2eAgNnSc2d1	americké
města	město	k1gNnSc2	město
Keyston	Keyston	k1gInSc1	Keyston
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Dakotě	Dakota	k1gFnSc6	Dakota
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
národní	národní	k2eAgInSc1d1	národní
památník	památník	k1gInSc1	památník
Mount	Mounta	k1gFnPc2	Mounta
Rushmore	Rushmor	k1gInSc5	Rushmor
<g/>
,	,	kIx,	,
sousoší	sousoší	k1gNnSc4	sousoší
čtyř	čtyři	k4xCgMnPc2	čtyři
amerických	americký	k2eAgMnPc2d1	americký
prezidentů	prezident	k1gMnPc2	prezident
vytesané	vytesaný	k2eAgFnSc2d1	vytesaná
do	do	k7c2	do
skály	skála	k1gFnSc2	skála
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
ho	on	k3xPp3gMnSc4	on
navštíví	navštívit	k5eAaPmIp3nS	navštívit
okolo	okolo	k7c2	okolo
dvou	dva	k4xCgInPc2	dva
milionů	milion	k4xCgInPc2	milion
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohé	k1gNnSc1	mnohé
atrakce	atrakce	k1gFnSc2	atrakce
nabízí	nabízet	k5eAaImIp3nS	nabízet
proslulý	proslulý	k2eAgInSc1d1	proslulý
zábavní	zábavní	k2eAgInSc1d1	zábavní
park	park	k1gInSc1	park
Disneyland	Disneyland	k1gInSc1	Disneyland
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
turistů	turist	k1gMnPc2	turist
chce	chtít	k5eAaImIp3nS	chtít
vidět	vidět	k5eAaImF	vidět
i	i	k9	i
centrum	centrum	k1gNnSc4	centrum
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
Hollywood	Hollywood	k1gInSc1	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejcenějším	cený	k2eAgFnPc3d3	nejcenější
památkám	památka	k1gFnPc3	památka
z	z	k7c2	z
předkolumbovské	předkolumbovský	k2eAgFnSc2d1	předkolumbovská
historie	historie	k1gFnSc2	historie
patří	patřit	k5eAaImIp3nS	patřit
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Mesa	Mes	k1gInSc2	Mes
Verde	Verd	k1gInSc5	Verd
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
i	i	k9	i
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nepřírodních	přírodní	k2eNgFnPc2d1	nepřírodní
památek	památka	k1gFnPc2	památka
se	se	k3xPyFc4	se
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k9	i
budova	budova	k1gFnSc1	budova
Independence	Independence	k1gFnSc2	Independence
Hall	Halla	k1gFnPc2	Halla
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
Deklarace	deklarace	k1gFnSc1	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
Socha	socha	k1gFnSc1	socha
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejen	nejen	k6eAd1	nejen
symbolem	symbol	k1gInSc7	symbol
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
celých	celá	k1gFnPc2	celá
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
známým	známá	k1gFnPc3	známá
<g/>
,	,	kIx,	,
ba	ba	k9	ba
ikonickým	ikonický	k2eAgMnSc7d1	ikonický
<g/>
,	,	kIx,	,
stavbám	stavba	k1gFnPc3	stavba
patří	patřit	k5eAaImIp3nP	patřit
Gateway	Gatewaa	k1gFnPc1	Gatewaa
Arch	archa	k1gFnPc2	archa
v	v	k7c6	v
St.	st.	kA	st.
Louis	louis	k1gInSc2	louis
<g/>
,	,	kIx,	,
Golden	Goldna	k1gFnPc2	Goldna
Gate	Gat	k1gInSc2	Gat
Bridge	Bridg	k1gInSc2	Bridg
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
<g/>
,	,	kIx,	,
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc1	Building
a	a	k8xC	a
nádraží	nádraží	k1gNnSc1	nádraží
Grand	grand	k1gMnSc1	grand
Central	Central	k1gMnSc1	Central
Terminal	Terminal	k1gMnSc1	Terminal
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
Lincolnův	Lincolnův	k2eAgInSc1d1	Lincolnův
památník	památník	k1gInSc1	památník
<g/>
,	,	kIx,	,
Bílý	bílý	k2eAgInSc1d1	bílý
dům	dům	k1gInSc1	dům
a	a	k8xC	a
Kapitol	Kapitol	k1gInSc1	Kapitol
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
Millennium	millennium	k1gNnSc1	millennium
Park	park	k1gInSc1	park
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Metropolitní	metropolitní	k2eAgNnSc1d1	metropolitní
muzeum	muzeum	k1gNnSc1	muzeum
umění	umění	k1gNnSc1	umění
či	či	k8xC	či
Carnegie	Carnegie	k1gFnSc1	Carnegie
Hall	Halla	k1gFnPc2	Halla
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejnavštěvovanějším	navštěvovaný	k2eAgFnPc3d3	nejnavštěvovanější
kulturním	kulturní	k2eAgFnPc3d1	kulturní
institucím	instituce	k1gFnPc3	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Řadu	řada	k1gFnSc4	řada
turistů	turist	k1gMnPc2	turist
láká	lákat	k5eAaImIp3nS	lákat
i	i	k9	i
město	město	k1gNnSc4	město
hazardu	hazard	k1gInSc2	hazard
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc1	Vegas
(	(	kIx(	(
<g/>
zejm	zejm	k?	zejm
<g/>
.	.	kIx.	.
slavná	slavný	k2eAgFnSc1d1	slavná
Fontána	fontána	k1gFnSc1	fontána
Bellagio	Bellagio	k1gMnSc1	Bellagio
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přímořská	přímořský	k2eAgNnPc1d1	přímořské
letoviska	letovisko	k1gNnPc1	letovisko
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Miami	Miami	k1gNnSc1	Miami
Beach	Beach	k1gInSc1	Beach
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Santa	Santa	k1gFnSc1	Santa
Monica	Monica	k1gFnSc1	Monica
se	s	k7c7	s
slavným	slavný	k2eAgInSc7d1	slavný
molem	mol	k1gInSc7	mol
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
centrem	centr	k1gMnSc7	centr
americké	americký	k2eAgFnSc2d1	americká
taneční	taneční	k2eAgFnSc2d1	taneční
kultury	kultura	k1gFnSc2	kultura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Automobilismus	automobilismus	k1gInSc1	automobilismus
hraje	hrát	k5eAaImIp3nS	hrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
osobní	osobní	k2eAgFnSc6d1	osobní
dopravě	doprava	k1gFnSc6	doprava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
americké	americký	k2eAgFnSc6d1	americká
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
mají	mít	k5eAaImIp3nP	mít
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
míru	míra	k1gFnSc4	míra
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
automobilů	automobil	k1gInPc2	automobil
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
255 009 283	[number]	k4	255 009 283
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
–	–	k?	–
to	ten	k3xDgNnSc1	ten
značí	značit	k5eAaImIp3nS	značit
poměr	poměr	k1gInSc1	poměr
910	[number]	k4	910
vozidel	vozidlo	k1gNnPc2	vozidlo
na	na	k7c4	na
1000	[number]	k4	1000
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgMnSc1d1	průměrný
dospělý	dospělý	k2eAgMnSc1d1	dospělý
Američan	Američan	k1gMnSc1	Američan
stráví	strávit	k5eAaPmIp3nS	strávit
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
v	v	k7c6	v
autě	auto	k1gNnSc6	auto
55	[number]	k4	55
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gInSc7	centr
automobilového	automobilový	k2eAgInSc2d1	automobilový
průmyslu	průmysl	k1gInSc2	průmysl
byl	být	k5eAaImAgInS	být
tradičně	tradičně	k6eAd1	tradičně
Detroit	Detroit	k1gInSc1	Detroit
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
tzv.	tzv.	kA	tzv.
rezavý	rezavý	k2eAgInSc1d1	rezavý
pás	pás	k1gInSc1	pás
kolem	kolem	k7c2	kolem
Velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
do	do	k7c2	do
krize	krize	k1gFnSc2	krize
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Tradičními	tradiční	k2eAgFnPc7d1	tradiční
automobilovými	automobilový	k2eAgFnPc7d1	automobilová
značkami	značka	k1gFnPc7	značka
byli	být	k5eAaImAgMnP	být
Ford	ford	k1gInSc4	ford
<g/>
,	,	kIx,	,
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
,	,	kIx,	,
Chrysler	Chrysler	k1gMnSc1	Chrysler
<g/>
,	,	kIx,	,
Chevrolet	chevrolet	k1gInSc1	chevrolet
<g/>
,	,	kIx,	,
Jeep	jeep	k1gInSc1	jeep
<g/>
,	,	kIx,	,
Cadillac	cadillac	k1gInSc1	cadillac
<g/>
,	,	kIx,	,
Buick	Buick	k1gInSc1	Buick
nebo	nebo	k8xC	nebo
Dodge	Dodge	k1gInSc1	Dodge
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
značky	značka	k1gFnPc1	značka
už	už	k9	už
ovšem	ovšem	k9	ovšem
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
motorkami	motorka	k1gFnPc7	motorka
měly	mít	k5eAaImAgFnP	mít
velkou	velký	k2eAgFnSc4d1	velká
tradici	tradice	k1gFnSc4	tradice
značky	značka	k1gFnSc2	značka
Indian	Indiana	k1gFnPc2	Indiana
a	a	k8xC	a
Harley-Davidson	Harley-Davidsona	k1gFnPc2	Harley-Davidsona
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
specifickou	specifický	k2eAgFnSc4d1	specifická
uživatelskou	uživatelský	k2eAgFnSc4d1	Uživatelská
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Automobilisté	automobilista	k1gMnPc1	automobilista
i	i	k8xC	i
motocyklisté	motocyklista	k1gMnPc1	motocyklista
mohou	moct	k5eAaImIp3nP	moct
využívat	využívat	k5eAaPmF	využívat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
sítí	síť	k1gFnPc2	síť
veřejných	veřejný	k2eAgFnPc2d1	veřejná
silnic	silnice	k1gFnPc2	silnice
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
6,4	[number]	k4	6,4
miliónu	milión	k4xCgInSc2	milión
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
91 700	[number]	k4	91 700
kilometrů	kilometr	k1gInPc2	kilometr
jsou	být	k5eAaImIp3nP	být
dálnice	dálnice	k1gFnPc4	dálnice
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
mnohaproudové	mnohaproudový	k2eAgFnPc1d1	mnohaproudový
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
silnice	silnice	k1gFnPc1	silnice
mají	mít	k5eAaImIp3nP	mít
přímo	přímo	k6eAd1	přímo
ikonický	ikonický	k2eAgInSc4d1	ikonický
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
slavná	slavný	k2eAgFnSc1d1	slavná
U.S.	U.S.	k1gFnSc1	U.S.
Route	Rout	k1gInSc5	Rout
66	[number]	k4	66
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sice	sice	k8xC	sice
již	již	k6eAd1	již
v	v	k7c6	v
dálničním	dálniční	k2eAgInSc6d1	dálniční
systému	systém	k1gInSc6	systém
není	být	k5eNaImIp3nS	být
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gFnPc1	její
části	část	k1gFnPc1	část
fungují	fungovat	k5eAaImIp3nP	fungovat
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
jako	jako	k8xC	jako
Historic	Historic	k1gMnSc1	Historic
Route	Rout	k1gMnSc5	Rout
66.	[number]	k4	66.
</s>
</p>
<p>
</p>
<p>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
tradici	tradice	k1gFnSc4	tradice
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
americká	americký	k2eAgFnSc1d1	americká
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
počet	počet	k1gInSc1	počet
přepravených	přepravený	k2eAgFnPc2d1	přepravená
osob	osoba	k1gFnPc2	osoba
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
Evropě	Evropa	k1gFnSc3	Evropa
nízký	nízký	k2eAgInSc1d1	nízký
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
31	[number]	k4	31
miliónů	milión	k4xCgInPc2	milión
osob	osoba	k1gFnPc2	osoba
ročně	ročně	k6eAd1	ročně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
ale	ale	k9	ale
zájem	zájem	k1gInSc1	zájem
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
–	–	k?	–
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
2000	[number]	k4	2000
a	a	k8xC	a
2010	[number]	k4	2010
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
uživatelů	uživatel	k1gMnPc2	uživatel
vnitrostátní	vnitrostátní	k2eAgFnSc2d1	vnitrostátní
meziměstské	meziměstská	k1gFnSc2	meziměstská
osobní	osobní	k2eAgFnSc2d1	osobní
železnice	železnice	k1gFnSc2	železnice
o	o	k7c4	o
37	[number]	k4	37
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
hraje	hrát	k5eAaImIp3nS	hrát
velkou	velký	k2eAgFnSc4d1	velká
úlohu	úloha	k1gFnSc4	úloha
státní	státní	k2eAgInSc4d1	státní
sektor	sektor	k1gInSc4	sektor
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
společností	společnost	k1gFnSc7	společnost
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgMnSc1d1	státní
dopravce	dopravce	k1gMnSc1	dopravce
Amtrak	Amtrak	k1gMnSc1	Amtrak
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
když	když	k8xS	když
převzal	převzít	k5eAaPmAgMnS	převzít
26	[number]	k4	26
menších	malý	k2eAgFnPc2d2	menší
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejrušnější	rušný	k2eAgFnSc7d3	nejrušnější
železniční	železniční	k2eAgFnSc7d1	železniční
trasou	trasa	k1gFnSc7	trasa
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
elektrifikovaný	elektrifikovaný	k2eAgInSc1d1	elektrifikovaný
Severovýchodní	severovýchodní	k2eAgInSc1d1	severovýchodní
koridor	koridor	k1gInSc1	koridor
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgInSc1d1	spojující
Washington	Washington	k1gInSc1	Washington
a	a	k8xC	a
Boston	Boston	k1gInSc1	Boston
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
přepravováno	přepravovat	k5eAaImNgNnS	přepravovat
asi	asi	k9	asi
10	[number]	k4	10
milionu	milion	k4xCgInSc2	milion
cestujících	cestující	k1gMnPc2	cestující
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
třetina	třetina	k1gFnSc1	třetina
výkonů	výkon	k1gInPc2	výkon
celého	celý	k2eAgInSc2d1	celý
Amtraku	Amtrak	k1gInSc2	Amtrak
<g/>
)	)	kIx)	)
a	a	k8xC	a
jezdí	jezdit	k5eAaImIp3nP	jezdit
zde	zde	k6eAd1	zde
i	i	k9	i
rychlovlak	rychlovlak	k1gInSc1	rychlovlak
Acela	Acelo	k1gNnSc2	Acelo
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
kanadskou	kanadský	k2eAgFnSc7d1	kanadská
společností	společnost	k1gFnSc7	společnost
Bombardier	Bombardira	k1gFnPc2	Bombardira
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Civilní	civilní	k2eAgFnSc1d1	civilní
letecká	letecký	k2eAgFnSc1d1	letecká
přeprava	přeprava	k1gFnSc1	přeprava
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
i	i	k9	i
ve	v	k7c6	v
vnitrostátní	vnitrostátní	k2eAgFnSc6d1	vnitrostátní
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Letecké	letecký	k2eAgFnPc1d1	letecká
společnosti	společnost	k1gFnPc1	společnost
jsou	být	k5eAaImIp3nP	být
výhradně	výhradně	k6eAd1	výhradně
soukromé	soukromý	k2eAgFnPc1d1	soukromá
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
letiště	letiště	k1gNnPc1	letiště
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
vlastněna	vlastnit	k5eAaImNgFnS	vlastnit
veřejnými	veřejný	k2eAgFnPc7d1	veřejná
korporacemi	korporace	k1gFnPc7	korporace
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
městy	město	k1gNnPc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
největší	veliký	k2eAgFnPc1d3	veliký
letecké	letecký	k2eAgFnPc1d1	letecká
společnosti	společnost	k1gFnPc1	společnost
na	na	k7c6	na
světě	svět	k1gInSc6	svět
jsou	být	k5eAaImIp3nP	být
americké	americký	k2eAgFnPc1d1	americká
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
dominuje	dominovat	k5eAaImIp3nS	dominovat
American	American	k1gMnSc1	American
Airlines	Airlines	k1gMnSc1	Airlines
<g/>
.	.	kIx.	.
</s>
<s>
Hartsfield-Jackson	Hartsfield-Jackson	k1gMnSc1	Hartsfield-Jackson
International	International	k1gFnPc2	International
Airport	Airport	k1gInSc4	Airport
u	u	k7c2	u
Atlanty	Atlanta	k1gFnSc2	Atlanta
je	být	k5eAaImIp3nS	být
nejvytíženějším	vytížený	k2eAgNnSc7d3	nejvytíženější
letištěm	letiště	k1gNnSc7	letiště
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
International	International	k1gMnSc1	International
Airport	Airport	k1gInSc4	Airport
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
a	a	k8xC	a
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Hare	Hare	k1gFnSc4	Hare
International	International	k1gFnPc2	International
Airport	Airport	k1gInSc4	Airport
u	u	k7c2	u
Chicaga	Chicago	k1gNnSc2	Chicago
šestým	šestý	k4xOgMnSc7	šestý
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
společnost	společnost	k1gFnSc1	společnost
Boeing	boeing	k1gInSc4	boeing
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
evropským	evropský	k2eAgInSc7d1	evropský
Airbusem	airbus	k1gInSc7	airbus
největším	veliký	k2eAgInSc7d3	veliký
výrobcem	výrobce	k1gMnSc7	výrobce
letadel	letadlo	k1gNnPc2	letadlo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
také	také	k6eAd1	také
největším	veliký	k2eAgMnSc7d3	veliký
americkým	americký	k2eAgMnSc7d1	americký
exportérem	exportér	k1gMnSc7	exportér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Sport	sport	k1gInSc1	sport
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Nejoblíbenější	oblíbený	k2eAgInPc1d3	nejoblíbenější
sporty	sport	k1gInPc1	sport
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
americký	americký	k2eAgInSc4d1	americký
fotbal	fotbal	k1gInSc4	fotbal
<g/>
,	,	kIx,	,
baseball	baseball	k1gInSc4	baseball
<g/>
,	,	kIx,	,
basketbal	basketbal	k1gInSc4	basketbal
<g/>
,	,	kIx,	,
lední	lední	k2eAgInSc4d1	lední
hokej	hokej	k1gInSc4	hokej
<g/>
,	,	kIx,	,
fotbal	fotbal	k1gInSc4	fotbal
<g/>
,	,	kIx,	,
tenis	tenis	k1gInSc4	tenis
<g/>
,	,	kIx,	,
golf	golf	k1gInSc4	golf
a	a	k8xC	a
wrestling	wrestling	k1gInSc4	wrestling
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
hlavní	hlavní	k2eAgFnPc1d1	hlavní
profesionální	profesionální	k2eAgFnPc1d1	profesionální
sportovní	sportovní	k2eAgFnPc1d1	sportovní
ligy	liga	k1gFnPc1	liga
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
jsou	být	k5eAaImIp3nP	být
Major	major	k1gMnSc1	major
League	Leagu	k1gFnSc2	Leagu
Baseball	baseball	k1gInSc1	baseball
(	(	kIx(	(
<g/>
MLB	MLB	kA	MLB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
National	National	k1gFnSc1	National
Basketball	Basketball	k1gMnSc1	Basketball
Association	Association	k1gInSc1	Association
(	(	kIx(	(
<g/>
NBA	NBA	kA	NBA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
National	National	k1gFnSc1	National
Football	Football	k1gMnSc1	Football
League	League	k1gInSc1	League
(	(	kIx(	(
<g/>
NFL	NFL	kA	NFL
<g/>
)	)	kIx)	)
a	a	k8xC	a
National	National	k1gMnSc2	National
Hockey	Hockea	k1gMnSc2	Hockea
League	Leagu	k1gMnSc2	Leagu
(	(	kIx(	(
<g/>
NHL	NHL	kA	NHL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
čtyři	čtyři	k4xCgFnPc1	čtyři
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejlukrativnější	lukrativní	k2eAgFnPc4d3	nejlukrativnější
sportovní	sportovní	k2eAgFnPc4d1	sportovní
ligy	liga	k1gFnPc4	liga
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Profesionální	profesionální	k2eAgFnSc1d1	profesionální
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
liga	liga	k1gFnSc1	liga
Major	major	k1gMnSc1	major
League	Leagu	k1gInSc2	Leagu
Soccer	Soccer	k1gMnSc1	Soccer
(	(	kIx(	(
<g/>
MLS	mls	k1gInSc1	mls
<g/>
)	)	kIx)	)
zatím	zatím	k6eAd1	zatím
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
úrovně	úroveň	k1gFnPc4	úroveň
popularity	popularita	k1gFnSc2	popularita
čtyř	čtyři	k4xCgFnPc2	čtyři
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
soutěží	soutěž	k1gFnPc2	soutěž
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gInPc2	jejich
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
protějšků	protějšek	k1gInPc2	protějšek
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
její	její	k3xOp3gFnSc1	její
průměrná	průměrný	k2eAgFnSc1d1	průměrná
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
původ	původ	k1gInSc1	původ
má	mít	k5eAaImIp3nS	mít
basketbal	basketbal	k1gInSc4	basketbal
<g/>
,	,	kIx,	,
volejbal	volejbal	k1gInSc4	volejbal
<g/>
,	,	kIx,	,
skateboarding	skateboarding	k1gInSc4	skateboarding
a	a	k8xC	a
snowboarding	snowboarding	k1gInSc4	snowboarding
<g/>
.	.	kIx.	.
</s>
<s>
Lakros	lakros	k1gInSc4	lakros
vymysleli	vymyslet	k5eAaPmAgMnP	vymyslet
ve	v	k7c6	v
12.	[number]	k4	12.
století	století	k1gNnSc6	století
severoameričtí	severoamerický	k2eAgMnPc1d1	severoamerický
indiáni	indián	k1gMnPc1	indián
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
americkým	americký	k2eAgFnPc3d1	americká
sportovním	sportovní	k2eAgFnPc3d1	sportovní
legendám	legenda	k1gFnPc3	legenda
patří	patřit	k5eAaImIp3nP	patřit
boxeři	boxer	k1gMnPc1	boxer
Muhammad	Muhammad	k1gInSc4	Muhammad
Ali	Ali	k1gMnSc1	Ali
<g/>
,	,	kIx,	,
Joe	Joe	k1gMnSc1	Joe
Frazier	Frazier	k1gMnSc1	Frazier
a	a	k8xC	a
Mike	Mike	k1gFnSc1	Mike
Tyson	Tysona	k1gFnPc2	Tysona
<g/>
,	,	kIx,	,
basketbalisté	basketbalista	k1gMnPc1	basketbalista
Michael	Michael	k1gMnSc1	Michael
Jordan	Jordan	k1gMnSc1	Jordan
<g/>
,	,	kIx,	,
Kobe	Kobe	k1gFnSc1	Kobe
Bryant	Bryant	k1gMnSc1	Bryant
<g/>
,	,	kIx,	,
LeBron	LeBron	k1gMnSc1	LeBron
James	James	k1gMnSc1	James
<g/>
,	,	kIx,	,
Magic	Magic	k1gMnSc1	Magic
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
,	,	kIx,	,
Shaquille	Shaquille	k1gFnSc1	Shaquille
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neal	Neal	k1gInSc4	Neal
<g/>
,	,	kIx,	,
Kareem	Kareum	k1gNnSc7	Kareum
Abdul-Jabbar	Abdul-Jabbara	k1gFnPc2	Abdul-Jabbara
<g/>
,	,	kIx,	,
Wilt	Wilt	k1gMnSc1	Wilt
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
<g />
.	.	kIx.	.
</s>
<s>
Malone	Malon	k1gMnSc5	Malon
<g/>
,	,	kIx,	,
Tim	Tim	k?	Tim
Duncan	Duncan	k1gInSc1	Duncan
a	a	k8xC	a
Larry	Larra	k1gFnPc1	Larra
Bird	Birdo	k1gNnPc2	Birdo
<g/>
,	,	kIx,	,
baseballisté	baseballista	k1gMnPc1	baseballista
Babe	babit	k5eAaImSgInS	babit
Ruth	Ruth	k1gFnSc1	Ruth
<g/>
,	,	kIx,	,
Jackie	Jackie	k1gFnSc1	Jackie
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
,	,	kIx,	,
Joe	Joe	k1gMnSc1	Joe
DiMaggio	DiMaggio	k1gMnSc1	DiMaggio
<g/>
,	,	kIx,	,
Barry	Barr	k1gInPc1	Barr
Bonds	Bonds	k1gInSc1	Bonds
a	a	k8xC	a
Hank	Hank	k1gInSc1	Hank
Aaron	Aarona	k1gFnPc2	Aarona
<g/>
,	,	kIx,	,
tenistky	tenistka	k1gFnSc2	tenistka
Serena	Seren	k2eAgFnSc1d1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
<g/>
,	,	kIx,	,
Chris	Chris	k1gFnSc1	Chris
Evertová	Evertová	k1gFnSc1	Evertová
a	a	k8xC	a
Billie	Billie	k1gFnSc1	Billie
Jean	Jean	k1gMnSc1	Jean
Kingová	Kingový	k2eAgFnSc1d1	Kingová
<g/>
,	,	kIx,	,
tenisté	tenista	k1gMnPc1	tenista
Andre	Andr	k1gMnSc5	Andr
Agassi	Agass	k1gMnSc5	Agass
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
McEnroe	McEnro	k1gFnSc2	McEnro
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Jimmy	Jimmo	k1gNnPc7	Jimmo
Connors	Connors	k1gInSc1	Connors
a	a	k8xC	a
Pete	Pete	k1gNnSc1	Pete
Sampras	Sampras	k1gMnSc1	Sampras
<g/>
,	,	kIx,	,
golfisté	golfista	k1gMnPc1	golfista
Tiger	Tigero	k1gNnPc2	Tigero
Woods	Woodsa	k1gFnPc2	Woodsa
a	a	k8xC	a
Jack	Jack	k1gMnSc1	Jack
Nicklaus	Nicklaus	k1gMnSc1	Nicklaus
<g/>
,	,	kIx,	,
cyklista	cyklista	k1gMnSc1	cyklista
Lance	lance	k1gNnSc2	lance
Armstrong	Armstrong	k1gMnSc1	Armstrong
<g/>
,	,	kIx,	,
plavci	plavec	k1gMnPc1	plavec
Michael	Michael	k1gMnSc1	Michael
Phelps	Phelps	k1gInSc1	Phelps
(	(	kIx(	(
<g/>
23	[number]	k4	23
zlatých	zlatý	k2eAgFnPc2d1	zlatá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
Spitz	Spitz	k1gMnSc1	Spitz
(	(	kIx(	(
<g/>
9	[number]	k4	9
zlatých	zlatý	k2eAgFnPc2d1	zlatá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Matt	Matt	k2eAgMnSc1d1	Matt
Biondi	Biond	k1gMnPc1	Biond
(	(	kIx(	(
<g/>
8	[number]	k4	8
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jenny	Jenna	k1gFnSc2	Jenna
Thompsonová	Thompsonová	k1gFnSc1	Thompsonová
(	(	kIx(	(
<g/>
8	[number]	k4	8
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atleti	atlet	k1gMnPc1	atlet
Jesse	Jesse	k1gFnSc2	Jesse
Owens	Owens	k1gInSc1	Owens
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Lewis	Lewis	k1gFnSc2	Lewis
<g/>
,	,	kIx,	,
Florence	Florenc	k1gFnSc2	Florenc
Griffith-Joynerová	Griffith-Joynerový	k2eAgFnSc1d1	Griffith-Joynerový
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Johnson	Johnson	k1gMnSc1	Johnson
a	a	k8xC	a
Ray	Ray	k1gMnSc1	Ray
Ewry	Ewra	k1gFnSc2	Ewra
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
americkým	americký	k2eAgMnSc7d1	americký
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
byl	být	k5eAaImAgMnS	být
Bobby	Bobba	k1gFnPc4	Bobba
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
hvězdám	hvězda	k1gFnPc3	hvězda
amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
patří	patřit	k5eAaImIp3nS	patřit
Jerry	Jerra	k1gFnPc4	Jerra
Rice	Ric	k1gFnSc2	Ric
nebo	nebo	k8xC	nebo
Adam	Adam	k1gMnSc1	Adam
Vinatieri	Vinatier	k1gFnSc2	Vinatier
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
medailově	medailově	k6eAd1	medailově
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
na	na	k7c6	na
zimních	zimní	k2eAgInPc6d1	zimní
jsou	být	k5eAaImIp3nP	být
druhé	druhý	k4xOgFnSc6	druhý
za	za	k7c7	za
Norskem	Norsko	k1gNnSc7	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největším	veliký	k2eAgMnPc3d3	veliký
"	"	kIx"	"
<g/>
zimním	zimní	k2eAgFnPc3d1	zimní
hvězdám	hvězda	k1gFnPc3	hvězda
<g/>
"	"	kIx"	"
amerického	americký	k2eAgInSc2d1	americký
sportu	sport	k1gInSc2	sport
patří	patřit	k5eAaImIp3nS	patřit
sjezdařka	sjezdařka	k1gFnSc1	sjezdařka
Lindsey	Lindsea	k1gFnSc2	Lindsea
Vonnová	Vonnová	k1gFnSc1	Vonnová
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
americkým	americký	k2eAgMnSc7d1	americký
hokejistou	hokejista	k1gMnSc7	hokejista
v	v	k7c6	v
NHL	NHL	kA	NHL
byl	být	k5eAaImAgInS	být
Mike	Mike	k1gInSc1	Mike
Modano	Modana	k1gFnSc5	Modana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
osmkrát	osmkrát	k6eAd1	osmkrát
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
pořádaly	pořádat	k5eAaImAgFnP	pořádat
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
hry	hra	k1gFnPc4	hra
letní	letní	k2eAgFnPc4d1	letní
(	(	kIx(	(
<g/>
v	v	k7c6	v
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1932	[number]	k4	1932
a	a	k8xC	a
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
a	a	k8xC	a
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
o	o	k7c4	o
hry	hra	k1gFnPc4	hra
zimní	zimní	k2eAgFnPc4d1	zimní
(	(	kIx(	(
<g/>
v	v	k7c6	v
Lake	Lake	k1gNnSc6	Lake
Placid	Placid	k1gInSc1	Placid
1932	[number]	k4	1932
a	a	k8xC	a
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Squaw	squaw	k1gFnPc6	squaw
Valley	Vallea	k1gFnSc2	Vallea
1960	[number]	k4	1960
a	a	k8xC	a
v	v	k7c6	v
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gFnSc2	Lak
City	city	k1gNnSc1	city
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2028	[number]	k4	2028
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
konat	konat	k5eAaImF	konat
letní	letní	k2eAgFnPc1d1	letní
hry	hra	k1gFnPc1	hra
již	již	k6eAd1	již
potřetí	potřetí	k4xO	potřetí
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
pořádaly	pořádat	k5eAaImAgInP	pořádat
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Americkými	americký	k2eAgMnPc7d1	americký
nositeli	nositel	k1gMnPc7	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
jsou	být	k5eAaImIp3nP	být
Ernest	Ernest	k1gMnSc1	Ernest
Hemingway	Hemingwaa	k1gFnSc2	Hemingwaa
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Steinbeck	Steinbeck	k1gMnSc1	Steinbeck
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Stearns	Stearnsa	k1gFnPc2	Stearnsa
Eliot	Eliot	k1gMnSc1	Eliot
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Faulkner	Faulkner	k1gInSc1	Faulkner
<g/>
,	,	kIx,	,
Pearl	Pearl	k1gInSc1	Pearl
S.	S.	kA	S.
Bucková	Bucková	k1gFnSc1	Bucková
<g/>
,	,	kIx,	,
Toni	Toni	k1gFnSc1	Toni
Morrisonová	Morrisonová	k1gFnSc1	Morrisonová
<g/>
,	,	kIx,	,
Saul	Saul	k1gMnSc1	Saul
Bellow	Bellow	k1gMnSc1	Bellow
<g/>
,	,	kIx,	,
Isaac	Isaac	k1gFnSc1	Isaac
Bashevis	Bashevis	k1gFnPc2	Bashevis
Singer	Singer	k1gMnSc1	Singer
<g/>
,	,	kIx,	,
Sinclair	Sinclair	k1gMnSc1	Sinclair
Lewis	Lewis	k1gFnSc2	Lewis
a	a	k8xC	a
Eugene	Eugen	k1gInSc5	Eugen
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neill	Neill	k1gInSc4	Neill
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
klasikům	klasik	k1gMnPc3	klasik
americké	americký	k2eAgFnSc2d1	americká
literatury	literatura	k1gFnSc2	literatura
patří	patřit	k5eAaImIp3nS	patřit
též	též	k9	též
Mark	Mark	k1gMnSc1	Mark
Twain	Twain	k1gMnSc1	Twain
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
Miller	Miller	k1gMnSc1	Miller
<g/>
,	,	kIx,	,
Jerome	Jerom	k1gInSc5	Jerom
David	David	k1gMnSc1	David
Salinger	Salinger	k1gMnSc1	Salinger
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gMnSc1	Vladimir
Nabokov	Nabokov	k1gInSc1	Nabokov
<g/>
,	,	kIx,	,
Francis	Francis	k1gFnSc1	Francis
Scott	Scott	k1gMnSc1	Scott
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
<g/>
,	,	kIx,	,
Ayn	Ayn	k1gMnSc1	Ayn
Randová	Randová	k1gFnSc1	Randová
<g/>
,	,	kIx,	,
Truman	Truman	k1gMnSc1	Truman
Capote	capot	k1gInSc5	capot
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
James	James	k1gMnSc1	James
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Updike	Updik	k1gFnSc2	Updik
<g/>
,	,	kIx,	,
Norman	Norman	k1gMnSc1	Norman
Mailer	Mailer	k1gMnSc1	Mailer
<g/>
,	,	kIx,	,
Philip	Philip	k1gMnSc1	Philip
Roth	Roth	k1gMnSc1	Roth
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
O.	O.	kA	O.
Henry	henry	k1gInSc1	henry
<g/>
,	,	kIx,	,
Gore	Gore	k1gFnSc1	Gore
Vidal	Vidal	k1gMnSc1	Vidal
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Bukowski	Bukowsk	k1gFnSc2	Bukowsk
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
Miller	Miller	k1gMnSc1	Miller
či	či	k8xC	či
Thomas	Thomas	k1gMnSc1	Thomas
Pynchon	Pynchon	k1gMnSc1	Pynchon
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
<g/>
,	,	kIx,	,
Walt	Walt	k1gMnSc1	Walt
Whitman	Whitman	k1gMnSc1	Whitman
<g/>
,	,	kIx,	,
Emily	Emil	k1gMnPc4	Emil
Dickinsonová	Dickinsonový	k2eAgFnSc1d1	Dickinsonová
<g/>
,	,	kIx,	,
Ralph	Ralph	k1gInSc1	Ralph
Waldo	Waldo	k1gNnSc1	Waldo
Emerson	Emerson	k1gInSc1	Emerson
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
Kerouac	Kerouac	k1gFnSc1	Kerouac
<g/>
,	,	kIx,	,
Allen	Allen	k1gMnSc1	Allen
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
<g/>
,	,	kIx,	,
Ezra	Ezra	k1gMnSc1	Ezra
Pound	pound	k1gInSc1	pound
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Frost	Frost	k1gMnSc1	Frost
<g/>
,	,	kIx,	,
Sylvia	Sylvia	k1gFnSc1	Sylvia
Plathová	Plathová	k1gFnSc1	Plathová
<g/>
,	,	kIx,	,
Langston	Langston	k1gInSc1	Langston
Hughes	Hughes	k1gMnSc1	Hughes
či	či	k8xC	či
Henry	Henry	k1gMnSc1	Henry
Wadsworth	Wadsworth	k1gMnSc1	Wadsworth
Longfellow	Longfellow	k1gMnSc1	Longfellow
v	v	k7c4	v
poezii	poezie	k1gFnSc4	poezie
a	a	k8xC	a
Tennessee	Tennessee	k1gFnSc4	Tennessee
Williams	Williamsa	k1gFnPc2	Williamsa
v	v	k7c6	v
dramatu	drama	k1gNnSc3	drama
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velké	velký	k2eAgFnPc1d1	velká
popularity	popularita	k1gFnPc1	popularita
lze	lze	k6eAd1	lze
v	v	k7c6	v
USA	USA	kA	USA
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
i	i	k9	i
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
jediné	jediný	k2eAgFnSc2d1	jediná
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
stalo	stát	k5eAaPmAgNnS	stát
Harper	Harper	k1gInSc4	Harper
Leeové	Leeová	k1gFnSc2	Leeová
(	(	kIx(	(
<g/>
Jako	jako	k8xS	jako
zabít	zabít	k5eAaPmF	zabít
ptáčka	ptáček	k1gMnSc4	ptáček
<g/>
)	)	kIx)	)
Margaret	Margareta	k1gFnPc2	Margareta
Mitchellové	Mitchellové	k2eAgFnPc2d1	Mitchellové
(	(	kIx(	(
<g/>
Jih	jih	k1gInSc1	jih
proti	proti	k7c3	proti
severu	sever	k1gInSc3	sever
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Harriet	Harriet	k1gInSc1	Harriet
Beecher	Beechra	k1gFnPc2	Beechra
Stoweové	Stoweová	k1gFnSc2	Stoweová
(	(	kIx(	(
<g/>
Chaloupka	chaloupka	k1gFnSc1	chaloupka
strýčka	strýček	k1gMnSc2	strýček
Toma	Tom	k1gMnSc2	Tom
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Maye	May	k1gMnSc2	May
Angelou	Angela	k1gFnSc7	Angela
(	(	kIx(	(
<g/>
I	i	k9	i
Know	Know	k1gMnSc1	Know
Why	Why	k1gMnSc1	Why
the	the	k?	the
Caged	Caged	k1gInSc1	Caged
Bird	Bird	k1gMnSc1	Bird
Sings	Sings	k1gInSc1	Sings
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
byly	být	k5eAaImAgInP	být
vždy	vždy	k6eAd1	vždy
především	především	k6eAd1	především
velmocí	velmoc	k1gFnPc2	velmoc
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
žánrové	žánrový	k2eAgFnSc2d1	žánrová
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Králem	Král	k1gMnSc7	Král
žánrů	žánr	k1gInPc2	žánr
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
Edgar	Edgar	k1gMnSc1	Edgar
Allan	Allan	k1gMnSc1	Allan
Poe	Poe	k1gMnSc1	Poe
<g/>
.	.	kIx.	.
</s>
<s>
Dobrodružný	dobrodružný	k2eAgInSc1d1	dobrodružný
román	román	k1gInSc1	román
přivedl	přivést	k5eAaPmAgInS	přivést
k	k	k7c3	k
dokonalosti	dokonalost	k1gFnSc3	dokonalost
Jack	Jack	k1gMnSc1	Jack
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
Herman	Herman	k1gMnSc1	Herman
Melville	Melville	k1gFnSc2	Melville
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
vnesl	vnést	k5eAaPmAgMnS	vnést
slavnou	slavný	k2eAgFnSc4d1	slavná
postavu	postava	k1gFnSc4	postava
a	a	k8xC	a
symbol	symbol	k1gInSc4	symbol
touhy	touha	k1gFnSc2	touha
Mobydicka	Mobydicko	k1gNnSc2	Mobydicko
<g/>
,	,	kIx,	,
Edgar	Edgar	k1gMnSc1	Edgar
Rice	Ric	k1gFnSc2	Ric
Burroughs	Burroughs	k1gInSc1	Burroughs
Tarzana	Tarzan	k1gMnSc4	Tarzan
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Fenimore	Fenimor	k1gInSc5	Fenimor
Cooper	Coopra	k1gFnPc2	Coopra
svého	svůj	k3xOyFgMnSc2	svůj
Posledního	poslední	k2eAgMnSc2d1	poslední
mohykána	mohykán	k1gMnSc2	mohykán
<g/>
.	.	kIx.	.
</s>
<s>
Isaac	Isaac	k6eAd1	Isaac
Asimov	Asimov	k1gInSc1	Asimov
<g/>
,	,	kIx,	,
Ray	Ray	k1gMnSc1	Ray
Bradbury	Bradbura	k1gFnSc2	Bradbura
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
A.	A.	kA	A.
Heinlein	Heinlein	k1gMnSc1	Heinlein
a	a	k8xC	a
Philip	Philip	k1gMnSc1	Philip
K.	K.	kA	K.
Dick	Dick	k1gMnSc1	Dick
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
klasikům	klasik	k1gMnPc3	klasik
sci-fi	scii	k1gFnSc2	sci-fi
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
pomezí	pomezí	k1gNnSc6	pomezí
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
rafinovaně	rafinovaně	k6eAd1	rafinovaně
pohyboval	pohybovat	k5eAaImAgMnS	pohybovat
i	i	k9	i
Kurt	Kurt	k1gMnSc1	Kurt
Vonnegut	Vonnegut	k1gMnSc1	Vonnegut
<g/>
.	.	kIx.	.
</s>
<s>
Stephen	Stephen	k2eAgMnSc1d1	Stephen
King	King	k1gMnSc1	King
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
králem	král	k1gMnSc7	král
literárního	literární	k2eAgInSc2d1	literární
hororu	horor	k1gInSc2	horor
a	a	k8xC	a
thrilleru	thriller	k1gInSc2	thriller
<g/>
,	,	kIx,	,
když	když	k8xS	když
tak	tak	k6eAd1	tak
navázal	navázat	k5eAaPmAgMnS	navázat
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
velkého	velký	k2eAgMnSc4d1	velký
předchůdce	předchůdce	k1gMnSc4	předchůdce
Howarda	Howard	k1gMnSc2	Howard
Phillipse	Phillips	k1gMnSc2	Phillips
Lovecrafta	Lovecraft	k1gMnSc2	Lovecraft
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgMnSc7d1	známý
autorem	autor	k1gMnSc7	autor
thrillerů	thriller	k1gInPc2	thriller
je	být	k5eAaImIp3nS	být
také	také	k9	také
John	John	k1gMnSc1	John
Grisham	Grisham	k1gInSc1	Grisham
<g/>
.	.	kIx.	.
</s>
<s>
Mario	Mario	k1gMnSc1	Mario
Puzo	Puzo	k1gMnSc1	Puzo
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
svým	svůj	k3xOyFgInSc7	svůj
mafiánským	mafiánský	k2eAgInSc7d1	mafiánský
eposem	epos	k1gInSc7	epos
Kmotr	kmotr	k1gMnSc1	kmotr
<g/>
.	.	kIx.	.
</s>
<s>
Drsnou	drsný	k2eAgFnSc4d1	drsná
detektivní	detektivní	k2eAgFnSc4d1	detektivní
školu	škola	k1gFnSc4	škola
zakládal	zakládat	k5eAaImAgInS	zakládat
Dashiell	Dashiell	k1gMnSc1	Dashiell
Hammett	Hammett	k1gMnSc1	Hammett
<g/>
.	.	kIx.	.
</s>
<s>
Revoluci	revoluce	k1gFnSc4	revoluce
v	v	k7c6	v
žánru	žánr	k1gInSc6	žánr
fantasy	fantas	k1gInPc4	fantas
svou	svůj	k3xOyFgFnSc7	svůj
Hrou	hra	k1gFnSc7	hra
o	o	k7c4	o
trůny	trůn	k1gInPc4	trůn
způsobil	způsobit	k5eAaPmAgInS	způsobit
George	George	k1gInSc1	George
R.	R.	kA	R.
R.	R.	kA	R.
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Skulinu	skulina	k1gFnSc4	skulina
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
objevila	objevit	k5eAaPmAgFnS	objevit
i	i	k9	i
Stephenie	Stephenie	k1gFnSc1	Stephenie
Meyerová	Meyerový	k2eAgFnSc1d1	Meyerová
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ságou	sága	k1gFnSc7	sága
Stmívání	stmívání	k1gNnSc1	stmívání
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Ursula	Ursula	k1gFnSc1	Ursula
K.	K.	kA	K.
Le	Le	k1gFnSc1	Le
Guinová	Guinová	k1gFnSc1	Guinová
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
průkopníkům	průkopník	k1gMnPc3	průkopník
americké	americký	k2eAgFnSc2d1	americká
fantasy	fantas	k1gInPc4	fantas
patřil	patřit	k5eAaImAgInS	patřit
již	již	k6eAd1	již
"	"	kIx"	"
<g/>
temný	temný	k2eAgMnSc1d1	temný
romantik	romantik	k1gMnSc1	romantik
<g/>
"	"	kIx"	"
Nathaniel	Nathaniel	k1gMnSc1	Nathaniel
Hawthorne	Hawthorn	k1gInSc5	Hawthorn
<g/>
.	.	kIx.	.
</s>
<s>
Špiónské	špiónský	k2eAgInPc1d1	špiónský
romány	román	k1gInPc1	román
proslavily	proslavit	k5eAaPmAgInP	proslavit
Toma	Tom	k1gMnSc4	Tom
Clancyho	Clancy	k1gMnSc4	Clancy
<g/>
.	.	kIx.	.
</s>
<s>
Kouzlo	kouzlo	k1gNnSc1	kouzlo
konspiračních	konspirační	k2eAgFnPc2d1	konspirační
teorií	teorie	k1gFnPc2	teorie
zužitkoval	zužitkovat	k5eAaPmAgInS	zužitkovat
beze	beze	k7c2	beze
zbytku	zbytek	k1gInSc2	zbytek
Dan	Dan	k1gMnSc1	Dan
Brown	Brown	k1gMnSc1	Brown
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
humoristickém	humoristický	k2eAgInSc6d1	humoristický
žánru	žánr	k1gInSc6	žánr
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgInS	prosadit
Washington	Washington	k1gInSc1	Washington
Irving	Irving	k1gInSc1	Irving
<g/>
.	.	kIx.	.
</s>
<s>
Biografie	biografie	k1gFnPc1	biografie
proslavily	proslavit	k5eAaPmAgFnP	proslavit
Carla	Carla	k1gFnSc1	Carla
Sandburga	Sandburga	k1gFnSc1	Sandburga
<g/>
.	.	kIx.	.
</s>
<s>
Řadu	řada	k1gFnSc4	řada
klasických	klasický	k2eAgFnPc2d1	klasická
komiksových	komiksový	k2eAgFnPc2d1	komiksová
postav	postava	k1gFnPc2	postava
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Stan	stan	k1gInSc4	stan
Lee	Lea	k1gFnSc3	Lea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
amerického	americký	k2eAgNnSc2d1	americké
divadla	divadlo	k1gNnSc2	divadlo
sehrály	sehrát	k5eAaPmAgFnP	sehrát
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
"	"	kIx"	"
<g/>
lehčí	lehký	k2eAgInPc4d2	lehčí
<g/>
"	"	kIx"	"
žánry	žánr	k1gInPc4	žánr
jako	jako	k9	jako
burleska	burleska	k1gFnSc1	burleska
<g/>
,	,	kIx,	,
vaudeville	vaudeville	k1gInSc1	vaudeville
či	či	k8xC	či
minstrel	minstrel	k1gMnSc1	minstrel
show	show	k1gFnPc2	show
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
předpokladem	předpoklad	k1gInSc7	předpoklad
rozvoje	rozvoj	k1gInSc2	rozvoj
muzikálu	muzikál	k1gInSc2	muzikál
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
Američané	Američan	k1gMnPc1	Američan
stali	stát	k5eAaPmAgMnP	stát
velmocí	velmoc	k1gFnSc7	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Proslulou	proslulý	k2eAgFnSc4d1	proslulá
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
zejména	zejména	k9	zejména
čtyřicítka	čtyřicítka	k1gFnSc1	čtyřicítka
scén	scéna	k1gFnPc2	scéna
na	na	k7c6	na
newyorské	newyorský	k2eAgFnSc6d1	newyorská
ulici	ulice	k1gFnSc6	ulice
Broadway	Broadwaa	k1gFnSc2	Broadwaa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měly	mít	k5eAaImAgFnP	mít
premiéru	premiéra	k1gFnSc4	premiéra
jedny	jeden	k4xCgInPc1	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgInPc2d3	nejslavnější
muzikálů	muzikál	k1gInPc2	muzikál
20.	[number]	k4	20.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
Brita	Brit	k1gMnSc2	Brit
Andrew	Andrew	k1gMnSc2	Andrew
Lloyd	Lloyd	k1gMnSc1	Lloyd
Webbera	Webber	k1gMnSc2	Webber
(	(	kIx(	(
<g/>
Fantom	fantom	k1gInSc1	fantom
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
Cats	Cats	k1gInSc1	Cats
<g/>
,	,	kIx,	,
Evita	Evita	k1gMnSc1	Evita
<g/>
,	,	kIx,	,
Jesus	Jesus	k1gMnSc1	Jesus
Christ	Christ	k1gMnSc1	Christ
Superstar	superstar	k1gFnSc1	superstar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgMnPc2d1	další
autorů	autor	k1gMnPc2	autor
(	(	kIx(	(
<g/>
West	West	k2eAgInSc1d1	West
Side	Side	k1gInSc1	Side
Story	story	k1gFnSc1	story
<g/>
,	,	kIx,	,
Oklahoma	Oklahoma	k1gFnSc1	Oklahoma
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Chicago	Chicago	k1gNnSc4	Chicago
<g/>
,	,	kIx,	,
My	my	k3xPp1nPc1	my
Fair	fair	k6eAd1	fair
Lady	lady	k1gFnPc2	lady
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
je	být	k5eAaImIp3nS	být
udílena	udílen	k2eAgFnSc1d1	udílena
prestižní	prestižní	k2eAgFnSc1d1	prestižní
divadelní	divadelní	k2eAgFnSc1d1	divadelní
cena	cena	k1gFnSc1	cena
Tony	Tony	k1gFnSc2	Tony
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Americké	americký	k2eAgNnSc1d1	americké
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
bylo	být	k5eAaImAgNnS	být
dlouho	dlouho	k6eAd1	dlouho
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
evropského	evropský	k2eAgInSc2d1	evropský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
19.	[number]	k4	19.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
emancipovalo	emancipovat	k5eAaBmAgNnS	emancipovat
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
svébytným	svébytný	k2eAgMnSc7d1	svébytný
<g/>
.	.	kIx.	.
</s>
<s>
Mary	Mary	k1gFnSc1	Mary
Cassattová	Cassattová	k1gFnSc1	Cassattová
byla	být	k5eAaImAgFnS	být
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
reprezentantkou	reprezentantka	k1gFnSc7	reprezentantka
impresionismu	impresionismus	k1gInSc2	impresionismus
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
James	James	k1gMnSc1	James
Abbott	Abbott	k1gMnSc1	Abbott
McNeill	McNeill	k1gMnSc1	McNeill
Whistler	Whistler	k1gMnSc1	Whistler
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
realistickou	realistický	k2eAgFnSc4d1	realistická
malbu	malba	k1gFnSc4	malba
vsadil	vsadit	k5eAaPmAgMnS	vsadit
Edward	Edward	k1gMnSc1	Edward
Hopper	Hopper	k1gMnSc1	Hopper
<g/>
,	,	kIx,	,
regionalismus	regionalismus	k1gInSc1	regionalismus
zakládal	zakládat	k5eAaImAgInS	zakládat
Grant	grant	k1gInSc4	grant
Wood	Wooda	k1gFnPc2	Wooda
<g/>
.	.	kIx.	.
</s>
<s>
Průkopnicí	průkopnice	k1gFnSc7	průkopnice
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
USA	USA	kA	USA
byla	být	k5eAaImAgFnS	být
Georgia	Georgia	k1gFnSc1	Georgia
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Keeffe	Keeff	k1gInSc5	Keeff
<g/>
.	.	kIx.	.
</s>
<s>
Modernismus	modernismus	k1gInSc1	modernismus
došel	dojít	k5eAaPmAgInS	dojít
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
vrcholu	vrchol	k1gInSc3	vrchol
v	v	k7c6	v
díle	díl	k1gInSc6	díl
Jacksona	Jackson	k1gMnSc2	Jackson
Pollocka	Pollocko	k1gNnSc2	Pollocko
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
světových	světový	k2eAgMnPc2d1	světový
představitelů	představitel	k1gMnPc2	představitel
abstraktního	abstraktní	k2eAgInSc2d1	abstraktní
expresionismu	expresionismus	k1gInSc2	expresionismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
60.	[number]	k4	60.
letech	léto	k1gNnPc6	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
i	i	k9	i
zcela	zcela	k6eAd1	zcela
osobitý	osobitý	k2eAgInSc4d1	osobitý
výtvarný	výtvarný	k2eAgInSc4d1	výtvarný
směr	směr	k1gInSc4	směr
zvaný	zvaný	k2eAgInSc1d1	zvaný
pop-art	poprt	k1gInSc1	pop-art
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
hlavním	hlavní	k2eAgMnSc7d1	hlavní
představitelem	představitel	k1gMnSc7	představitel
byl	být	k5eAaImAgInS	být
Andy	Anda	k1gFnSc2	Anda
Warhol	Warhol	k1gInSc1	Warhol
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgMnSc1d1	narozen
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
reprezentantům	reprezentant	k1gMnPc3	reprezentant
patřil	patřit	k5eAaImAgInS	patřit
Roy	Roy	k1gMnSc1	Roy
Lichtenstein	Lichtenstein	k1gMnSc1	Lichtenstein
<g/>
.	.	kIx.	.
</s>
<s>
Jean-Michel	Jean-Michel	k1gMnSc1	Jean-Michel
Basquiat	Basquiat	k1gInSc4	Basquiat
založil	založit	k5eAaPmAgMnS	založit
pouliční	pouliční	k2eAgNnSc4d1	pouliční
graffiti	graffiti	k1gNnSc4	graffiti
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uměleckou	umělecký	k2eAgFnSc4d1	umělecká
fotografii	fotografia	k1gFnSc4	fotografia
formoval	formovat	k5eAaImAgMnS	formovat
zvláště	zvláště	k6eAd1	zvláště
Man	Man	k1gMnSc1	Man
Ray	Ray	k1gMnSc1	Ray
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Silná	silný	k2eAgFnSc1d1	silná
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Frank	Frank	k1gMnSc1	Frank
Lloyd	Lloyd	k1gMnSc1	Lloyd
Wright	Wright	k1gMnSc1	Wright
zavedl	zavést	k5eAaPmAgMnS	zavést
koncept	koncept	k1gInSc4	koncept
"	"	kIx"	"
<g/>
organické	organický	k2eAgFnSc2d1	organická
architektury	architektura	k1gFnSc2	architektura
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgMnSc7d1	podobný
průkopníkem	průkopník	k1gMnSc7	průkopník
byl	být	k5eAaImAgMnS	být
Buckminster	Buckminster	k1gMnSc1	Buckminster
Fuller	Fuller	k1gMnSc1	Fuller
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
geodetických	geodetický	k2eAgFnPc2d1	geodetická
kopulí	kopule	k1gFnPc2	kopule
<g/>
.	.	kIx.	.
</s>
<s>
Louis	Louis	k1gMnSc1	Louis
Sullivan	Sullivan	k1gMnSc1	Sullivan
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
otce	otec	k1gMnSc2	otec
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
americkou	americký	k2eAgFnSc4d1	americká
kulturu	kultura	k1gFnSc4	kultura
přímo	přímo	k6eAd1	přímo
ikonické	ikonický	k2eAgNnSc1d1	ikonické
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgInPc7d1	typický
bílými	bílý	k2eAgInPc7d1	bílý
domy	dům	k1gInPc7	dům
proslul	proslout	k5eAaPmAgMnS	proslout
modernista	modernista	k1gMnSc1	modernista
Richard	Richard	k1gMnSc1	Richard
Meier	Meier	k1gMnSc1	Meier
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
modernismem	modernismus	k1gInSc7	modernismus
a	a	k8xC	a
postmodernismem	postmodernismus	k1gInSc7	postmodernismus
stál	stát	k5eAaImAgMnS	stát
Philip	Philip	k1gMnSc1	Philip
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
.	.	kIx.	.
</s>
<s>
Klasikem	klasik	k1gMnSc7	klasik
postmodernismu	postmodernismus	k1gInSc2	postmodernismus
je	být	k5eAaImIp3nS	být
Robert	Robert	k1gMnSc1	Robert
Venturi	Ventur	k1gFnSc2	Ventur
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
narozený	narozený	k2eAgMnSc1d1	narozený
I.	I.	kA	I.
M.	M.	kA	M.
Pei	Pei	k1gMnSc1	Pei
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
svými	svůj	k3xOyFgInPc7	svůj
moderními	moderní	k2eAgInPc7d1	moderní
zásahy	zásah	k1gInPc7	zásah
do	do	k7c2	do
galerie	galerie	k1gFnSc2	galerie
Louvre	Louvre	k1gInSc1	Louvre
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Hudba	hudba	k1gFnSc1	hudba
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
nejslavnějším	slavný	k2eAgMnPc3d3	nejslavnější
americkým	americký	k2eAgMnPc3d1	americký
skladatelům	skladatel	k1gMnPc3	skladatel
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
patří	patřit	k5eAaImIp3nP	patřit
George	George	k1gNnPc1	George
Gershwin	Gershwin	k1gMnSc1	Gershwin
<g/>
,	,	kIx,	,
Leonard	Leonard	k1gMnSc1	Leonard
Bernstein	Bernstein	k1gMnSc1	Bernstein
<g/>
,	,	kIx,	,
Aaron	Aaron	k1gMnSc1	Aaron
Copland	Copland	k1gInSc1	Copland
a	a	k8xC	a
Philip	Philip	k1gInSc1	Philip
Glass	Glassa	k1gFnPc2	Glassa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
avantgardních	avantgardní	k2eAgInPc2d1	avantgardní
pokusů	pokus	k1gInPc2	pokus
stál	stát	k5eAaImAgMnS	stát
John	John	k1gMnSc1	John
Cage	Cag	k1gInSc2	Cag
<g/>
.	.	kIx.	.
</s>
<s>
Legendou	legenda	k1gFnSc7	legenda
filmové	filmový	k2eAgFnSc2d1	filmová
hudby	hudba	k1gFnSc2	hudba
je	být	k5eAaImIp3nS	být
John	John	k1gMnSc1	John
Williams	Williams	k1gInSc1	Williams
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
žánru	žánr	k1gInSc6	žánr
prosluli	proslout	k5eAaPmAgMnP	proslout
též	též	k9	též
James	James	k1gMnSc1	James
Horner	Horner	k1gMnSc1	Horner
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
Mancini	Mancin	k2eAgMnPc1d1	Mancin
či	či	k8xC	či
Quincy	Quinca	k1gFnPc1	Quinca
Jones	Jonesa	k1gFnPc2	Jonesa
<g/>
.	.	kIx.	.
</s>
<s>
Králem	Král	k1gMnSc7	Král
muzikálů	muzikál	k1gInPc2	muzikál
byl	být	k5eAaImAgInS	být
Cole	cola	k1gFnSc3	cola
Porter	porter	k1gInSc4	porter
<g/>
.	.	kIx.	.
</s>
<s>
Scott	Scott	k2eAgInSc1d1	Scott
Joplin	Joplin	k1gInSc1	Joplin
založil	založit	k5eAaPmAgInS	založit
hudební	hudební	k2eAgInSc1d1	hudební
styl	styl	k1gInSc1	styl
ragtime	ragtime	k1gInSc1	ragtime
<g/>
.	.	kIx.	.
</s>
<s>
Jazzovými	jazzový	k2eAgFnPc7d1	jazzová
legendami	legenda	k1gFnPc7	legenda
jsou	být	k5eAaImIp3nP	být
Louis	Louis	k1gMnSc1	Louis
Armstrong	Armstrong	k1gMnSc1	Armstrong
<g/>
,	,	kIx,	,
Ella	Ella	k1gMnSc1	Ella
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
<g/>
,	,	kIx,	,
Miles	Miles	k1gMnSc1	Miles
Davis	Davis	k1gFnSc2	Davis
<g/>
,	,	kIx,	,
Glenn	Glenn	k1gMnSc1	Glenn
Miller	Miller	k1gMnSc1	Miller
<g/>
,	,	kIx,	,
Nat	Nat	k1gMnSc1	Nat
King	King	k1gMnSc1	King
Cole	cola	k1gFnSc6	cola
<g/>
,	,	kIx,	,
Charlie	Charlie	k1gMnSc1	Charlie
Parker	Parker	k1gMnSc1	Parker
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Coltrane	Coltran	k1gInSc5	Coltran
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejslavnější	slavný	k2eAgMnPc1d3	nejslavnější
sóloví	sólový	k2eAgMnPc1d1	sólový
umělci	umělec	k1gMnPc1	umělec
populární	populární	k2eAgFnSc2d1	populární
a	a	k8xC	a
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
pocházejí	pocházet	k5eAaImIp3nP	pocházet
často	často	k6eAd1	často
právě	právě	k9	právě
z	z	k7c2	z
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Klasiky	klasika	k1gFnPc1	klasika
40.	[number]	k4	40.
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
otci	otec	k1gMnSc3	otec
zakladateli	zakladatel	k1gMnSc3	zakladatel
žánru	žánr	k1gInSc2	žánr
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
Elvis	Elvis	k1gMnSc1	Elvis
Presley	Preslea	k1gFnSc2	Preslea
<g/>
,	,	kIx,	,
Bob	Bob	k1gMnSc1	Bob
Dylan	Dylan	k1gMnSc1	Dylan
<g/>
,	,	kIx,	,
Johnny	Johnen	k2eAgFnPc1d1	Johnna
Cash	cash	k1gFnPc1	cash
<g/>
,	,	kIx,	,
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
<g/>
,	,	kIx,	,
Frank	Frank	k1gMnSc1	Frank
Sinatra	Sinatra	k1gFnSc1	Sinatra
<g/>
,	,	kIx,	,
Pete	Pete	k1gFnSc1	Pete
Seeger	Seeger	k1gMnSc1	Seeger
<g/>
,	,	kIx,	,
B.B.	B.B.	k1gMnSc1	B.B.
King	King	k1gMnSc1	King
<g/>
,	,	kIx,	,
Ray	Ray	k1gMnSc1	Ray
Charles	Charles	k1gMnSc1	Charles
<g/>
,	,	kIx,	,
Bruce	Bruce	k1gMnSc1	Bruce
<g />
.	.	kIx.	.
</s>
<s>
Springsteen	Springsteen	k1gInSc1	Springsteen
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Brown	Brown	k1gMnSc1	Brown
<g/>
,	,	kIx,	,
Janis	Janis	k1gFnSc1	Janis
Joplin	Joplin	k1gInSc1	Joplin
<g/>
,	,	kIx,	,
Tina	Tina	k1gFnSc1	Tina
Turner	turner	k1gMnSc1	turner
<g/>
,	,	kIx,	,
Aretha	Aretha	k1gMnSc1	Aretha
Franklin	Franklin	k1gInSc1	Franklin
<g/>
,	,	kIx,	,
Frank	Frank	k1gMnSc1	Frank
Zappa	Zappa	k1gFnSc1	Zappa
<g/>
,	,	kIx,	,
Stevie	Stevie	k1gFnSc1	Stevie
Wonder	Wondra	k1gFnPc2	Wondra
<g/>
,	,	kIx,	,
Bing	bingo	k1gNnPc2	bingo
Crosby	Crosb	k1gInPc7	Crosb
<g/>
,	,	kIx,	,
Chuck	Chuck	k1gMnSc1	Chuck
Berry	Berra	k1gFnSc2	Berra
<g/>
,	,	kIx,	,
Eartha	Eartha	k1gFnSc1	Eartha
Kittová	Kittová	k1gFnSc1	Kittová
<g/>
,	,	kIx,	,
Liza	Liza	k1gFnSc1	Liza
Minnelli	Minnelle	k1gFnSc4	Minnelle
<g/>
,	,	kIx,	,
Duke	Duke	k1gFnSc1	Duke
Ellington	Ellington	k1gInSc1	Ellington
<g/>
,	,	kIx,	,
Joan	Joan	k1gInSc1	Joan
Baezová	Baezová	k1gFnSc1	Baezová
<g/>
,	,	kIx,	,
Buddy	Budda	k1gFnPc1	Budda
Holly	Holla	k1gFnSc2	Holla
<g/>
,	,	kIx,	,
Lou	Lou	k1gMnSc1	Lou
Reed	Reed	k1gMnSc1	Reed
<g/>
,	,	kIx,	,
Billie	Billie	k1gFnSc1	Billie
Holiday	Holidaa	k1gFnSc2	Holidaa
<g/>
,	,	kIx,	,
Diana	Diana	k1gFnSc1	Diana
Rossová	Rossová	k1gFnSc1	Rossová
<g/>
,	,	kIx,	,
Donna	donna	k1gFnSc1	donna
Summer	Summer	k1gInSc1	Summer
<g/>
,	,	kIx,	,
Nina	Nina	k1gFnSc1	Nina
Simone	Simon	k1gMnSc5	Simon
<g/>
,	,	kIx,	,
Gene	gen	k1gInSc5	gen
Kelly	Kell	k1gInPc7	Kell
<g/>
,	,	kIx,	,
Etta	Etta	k1gMnSc1	Etta
James	James	k1gMnSc1	James
<g/>
,	,	kIx,	,
Alice	Alice	k1gFnSc1	Alice
Cooper	Coopra	k1gFnPc2	Coopra
či	či	k8xC	či
Iggy	Igga	k1gFnSc2	Igga
Pop	pop	k1gInSc1	pop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc1d1	další
silná	silný	k2eAgFnSc1d1	silná
vlna	vlna	k1gFnSc1	vlna
přišla	přijít	k5eAaPmAgFnS	přijít
v	v	k7c6	v
80.	[number]	k4	80.
letech	let	k1gInPc6	let
<g/>
:	:	kIx,	:
Michael	Michael	k1gMnSc1	Michael
Jackson	Jackson	k1gMnSc1	Jackson
<g/>
,	,	kIx,	,
Madonna	Madonna	k1gFnSc1	Madonna
<g/>
,	,	kIx,	,
Whitney	Whitneum	k1gNnPc7	Whitneum
Houston	Houston	k1gInSc1	Houston
<g/>
,	,	kIx,	,
Cher	Cher	k1gInSc1	Cher
<g/>
,	,	kIx,	,
Prince	princ	k1gMnPc4	princ
<g/>
,	,	kIx,	,
Mariah	Mariah	k1gInSc4	Mariah
Carey	Carea	k1gFnSc2	Carea
<g/>
,	,	kIx,	,
Janet	Janet	k1gMnSc1	Janet
Jackson	Jackson	k1gMnSc1	Jackson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
90.	[number]	k4	90.
letech	léto	k1gNnPc6	léto
nastal	nastat	k5eAaPmAgInS	nastat
mohutný	mohutný	k2eAgInSc1d1	mohutný
nástup	nástup	k1gInSc1	nástup
rapu	rapa	k1gFnSc4	rapa
a	a	k8xC	a
hip-hopu	hipopa	k1gFnSc4	hip-hopa
<g/>
,	,	kIx,	,
nejslavnějšími	slavný	k2eAgInPc7d3	nejslavnější
rappery	rapper	k1gInPc7	rapper
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Eminem	Emino	k1gNnSc7	Emino
<g/>
,	,	kIx,	,
Tupac	Tupac	k1gInSc1	Tupac
Shakur	Shakura	k1gFnPc2	Shakura
<g/>
,	,	kIx,	,
50	[number]	k4	50
Cent	cent	k1gInSc1	cent
<g/>
,	,	kIx,	,
Kanye	Kanye	k1gFnSc1	Kanye
West	West	k1gMnSc1	West
<g/>
,	,	kIx,	,
Akon	Akon	k1gMnSc1	Akon
<g/>
,	,	kIx,	,
Jay	Jay	k1gMnSc1	Jay
Z	Z	kA	Z
<g/>
,	,	kIx,	,
Snoop	Snoop	k1gInSc4	Snoop
Dogg	Dogga	k1gFnPc2	Dogga
<g/>
,	,	kIx,	,
Dr.	dr.	kA	dr.
Dre	Dre	k1gFnSc4	Dre
<g/>
,	,	kIx,	,
Lil	lít	k5eAaImAgMnS	lít
Wayne	Wayn	k1gInSc5	Wayn
<g/>
.	.	kIx.	.
</s>
<s>
Králem	Král	k1gMnSc7	Král
latino-popu	latinoop	k1gInSc2	latino-pop
je	být	k5eAaImIp3nS	být
Ricky	Ricek	k1gMnPc4	Ricek
Martin	Martin	k1gInSc1	Martin
<g/>
,	,	kIx,	,
americké	americký	k2eAgFnSc2d1	americká
country	country	k2eAgFnSc2d1	country
pak	pak	k8xC	pak
Dolly	Doll	k1gMnPc4	Doll
Parton	Parton	k1gInSc4	Parton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
kralují	kralovat	k5eAaImIp3nP	kralovat
hitparádám	hitparáda	k1gFnPc3	hitparáda
Lady	Lada	k1gFnSc2	Lada
Gaga	Gaga	k1gMnSc1	Gaga
<g/>
,	,	kIx,	,
Beyoncé	Beyoncá	k1gFnPc1	Beyoncá
<g/>
,	,	kIx,	,
Katy	Kata	k1gFnPc1	Kata
Perry	Perra	k1gFnPc1	Perra
<g/>
,	,	kIx,	,
Miley	Milea	k1gFnPc1	Milea
Cyrus	Cyrus	k1gInSc1	Cyrus
<g/>
,	,	kIx,	,
Britney	Britney	k1gInPc1	Britney
Spears	Spearsa	k1gFnPc2	Spearsa
<g/>
,	,	kIx,	,
Justin	Justina	k1gFnPc2	Justina
Timberlake	Timberlak	k1gInSc2	Timberlak
<g/>
,	,	kIx,	,
Bruno	Bruno	k1gMnSc1	Bruno
Mars	Mars	k1gMnSc1	Mars
<g/>
,	,	kIx,	,
Anastacia	Anastacia	k1gFnSc1	Anastacia
<g/>
,	,	kIx,	,
Taylor	Taylor	k1gMnSc1	Taylor
Swift	Swift	k1gMnSc1	Swift
<g/>
,	,	kIx,	,
Christina	Christin	k1gMnSc2	Christin
Aguilera	Aguiler	k1gMnSc2	Aguiler
<g/>
,	,	kIx,	,
Pink	pink	k0	pink
<g/>
,	,	kIx,	,
Gwen	Gwen	k1gMnSc1	Gwen
Stefani	Stefan	k1gMnPc1	Stefan
<g/>
,	,	kIx,	,
Usher	Usher	k1gMnSc1	Usher
<g/>
,	,	kIx,	,
Lana	lano	k1gNnSc2	lano
Del	Del	k1gFnSc2	Del
Rey	Rea	k1gFnSc2	Rea
<g/>
,	,	kIx,	,
Norah	Norah	k1gMnSc1	Norah
Jones	Jones	k1gMnSc1	Jones
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
pochází	pocházet	k5eAaImIp3nS	pocházet
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nejslavnějších	slavný	k2eAgFnPc2d3	nejslavnější
rockových	rockový	k2eAgFnPc2d1	rocková
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Povětšinou	povětšinou	k6eAd1	povětšinou
z	z	k7c2	z
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Doors	Doorsa	k1gFnPc2	Doorsa
(	(	kIx(	(
<g/>
Jim	on	k3xPp3gMnPc3	on
Morrison	Morrison	k1gNnSc4	Morrison
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Metallica	Metallica	k1gMnSc1	Metallica
(	(	kIx(	(
<g/>
James	James	k1gMnSc1	James
Hetfield	Hetfield	k1gMnSc1	Hetfield
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Linkin	Linkin	k2eAgInSc1d1	Linkin
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
Chester	Chester	k1gInSc1	Chester
Bennington	Bennington	k1gInSc1	Bennington
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Red	Red	k1gMnSc1	Red
Hot	hot	k0	hot
Chili	Chile	k1gFnSc3	Chile
Peppers	Peppers	k1gInSc1	Peppers
(	(	kIx(	(
<g/>
Anthony	Anthon	k1gInPc1	Anthon
Kiedis	Kiedis	k1gFnSc2	Kiedis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Eagles	Eagles	k1gMnSc1	Eagles
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
<g />
.	.	kIx.	.
</s>
<s>
Beach	Beach	k1gInSc1	Beach
Boys	boy	k1gMnPc2	boy
<g/>
,	,	kIx,	,
Thirty	Thirta	k1gFnPc4	Thirta
Seconds	Secondsa	k1gFnPc2	Secondsa
to	ten	k3xDgNnSc4	ten
Mars	Mars	k1gMnSc1	Mars
(	(	kIx(	(
<g/>
Jared	Jared	k1gMnSc1	Jared
Leto	Leto	k1gMnSc1	Leto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Green	Green	k2eAgMnSc1d1	Green
Day	Day	k1gMnSc1	Day
(	(	kIx(	(
<g/>
Billie	Billie	k1gFnSc1	Billie
Joe	Joe	k1gMnSc1	Joe
Armstrong	Armstrong	k1gMnSc1	Armstrong
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Guns	Guns	k1gInSc1	Guns
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
(	(	kIx(	(
<g/>
Axl	Axl	k1gMnSc1	Axl
Rose	Rose	k1gMnSc1	Rose
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Megadeth	Megadeth	k1gMnSc1	Megadeth
(	(	kIx(	(
<g/>
Dave	Dav	k1gInSc5	Dav
Mustaine	Mustain	k1gMnSc5	Mustain
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Korn	Korn	k1gMnSc1	Korn
<g/>
,	,	kIx,	,
Blink-182	Blink-182	k1gMnSc1	Blink-182
<g/>
,	,	kIx,	,
Slayer	Slayer	k1gMnSc1	Slayer
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Black	Black	k1gMnSc1	Black
Eyed	Eyed	k1gMnSc1	Eyed
Peas	Peas	k1gInSc4	Peas
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
narušením	narušení	k1gNnSc7	narušení
kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
kulturní	kulturní	k2eAgFnSc2d1	kulturní
hegemonie	hegemonie	k1gFnSc2	hegemonie
byla	být	k5eAaImAgFnS	být
vlna	vlna	k1gFnSc1	vlna
grunge	grunge	k1gFnSc1	grunge
z	z	k7c2	z
90.	[number]	k4	90.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zdvihla	zdvihnout	k5eAaPmAgFnS	zdvihnout
v	v	k7c4	v
Seattlu	Seattla	k1gMnSc4	Seattla
<g/>
:	:	kIx,	:
Nirvana	Nirvan	k1gMnSc4	Nirvan
(	(	kIx(	(
<g/>
Kurt	Kurt	k1gMnSc1	Kurt
Cobain	Cobain	k1gMnSc1	Cobain
<g/>
,	,	kIx,	,
Dave	Dav	k1gInSc5	Dav
Grohl	Grohl	k1gMnSc5	Grohl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alice	Alice	k1gFnSc1	Alice
in	in	k?	in
Chains	Chains	k1gInSc4	Chains
<g/>
,	,	kIx,	,
Pearl	Pearl	k1gInSc4	Pearl
Jam	jáma	k1gFnPc2	jáma
či	či	k8xC	či
Soundgarden	Soundgardna	k1gFnPc2	Soundgardna
<g/>
.	.	kIx.	.
</s>
<s>
Silná	silný	k2eAgFnSc1d1	silná
hudební	hudební	k2eAgFnSc1d1	hudební
scéna	scéna	k1gFnSc1	scéna
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
:	:	kIx,	:
Bon	bon	k1gInSc1	bon
Jovi	Jovi	k1gNnSc1	Jovi
(	(	kIx(	(
<g/>
Jon	Jon	k1gFnPc1	Jon
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kiss	Kiss	k1gInSc1	Kiss
<g/>
,	,	kIx,	,
Aerosmith	Aerosmith	k1gInSc1	Aerosmith
(	(	kIx(	(
<g/>
Steven	Steven	k2eAgInSc1d1	Steven
Tyler	Tyler	k1gInSc1	Tyler
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
R.E.M.	R.E.M.	k1gMnSc1	R.E.M.
<g/>
,	,	kIx,	,
Ramones	Ramones	k1gMnSc1	Ramones
<g/>
,	,	kIx,	,
My	my	k3xPp1nPc1	my
Chemical	Chemical	k1gMnPc1	Chemical
Romance	romance	k1gFnSc2	romance
<g/>
,	,	kIx,	,
The	The	k1gFnSc2	The
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc1	underground
<g/>
,	,	kIx,	,
Simon	Simon	k1gMnSc1	Simon
&	&	k?	&
Garfunkel	Garfunkel	k1gMnSc1	Garfunkel
(	(	kIx(	(
<g/>
Paul	Paul	k1gMnSc1	Paul
Simon	Simon	k1gMnSc1	Simon
<g/>
,	,	kIx,	,
Art	Art	k1gMnSc1	Art
Garfunkel	Garfunkel	k1gMnSc1	Garfunkel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pixies	Pixies	k1gInSc1	Pixies
<g/>
,	,	kIx,	,
Backstreet	Backstreet	k1gInSc1	Backstreet
Boys	boy	k1gMnPc2	boy
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
amerického	americký	k2eAgNnSc2d1	americké
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
je	být	k5eAaImIp3nS	být
cesta	cesta	k1gFnSc1	cesta
ke	k	k7c3	k
světové	světový	k2eAgFnSc3d1	světová
slávě	sláva	k1gFnSc3	sláva
o	o	k7c4	o
dost	dost	k6eAd1	dost
těžší	těžký	k2eAgNnSc4d2	těžší
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
našly	najít	k5eAaPmAgFnP	najít
se	se	k3xPyFc4	se
i	i	k9	i
takové	takový	k3xDgInPc1	takový
případy	případ	k1gInPc1	případ
<g/>
:	:	kIx,	:
skupina	skupina	k1gFnSc1	skupina
Evanescence	Evanescence	k1gFnSc1	Evanescence
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Arkansasu	Arkansas	k1gInSc6	Arkansas
<g/>
,	,	kIx,	,
metaloví	metalový	k2eAgMnPc1d1	metalový
Nine	Nine	k1gNnSc7	Nine
Inch	Inch	k1gInSc1	Inch
Nails	Nails	k1gInSc1	Nails
v	v	k7c6	v
Ohiu	Ohio	k1gNnSc6	Ohio
<g/>
,	,	kIx,	,
Slipknot	Slipknota	k1gFnPc2	Slipknota
v	v	k7c6	v
Iowě	Iowa	k1gFnSc6	Iowa
či	či	k8xC	či
Pantera	panter	k1gMnSc2	panter
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejslavnější	slavný	k2eAgFnSc7d3	nejslavnější
americkou	americký	k2eAgFnSc7d1	americká
tanečnicí	tanečnice	k1gFnSc7	tanečnice
byla	být	k5eAaImAgFnS	být
Isadora	Isadora	k1gFnSc1	Isadora
Duncanová	Duncanová	k1gFnSc1	Duncanová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Kinematografie	kinematografie	k1gFnSc1	kinematografie
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Americký	americký	k2eAgInSc1d1	americký
filmový	filmový	k2eAgInSc1d1	filmový
průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
nejrozvinutější	rozvinutý	k2eAgInSc1d3	nejrozvinutější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
synonymem	synonymum	k1gNnSc7	synonymum
je	být	k5eAaImIp3nS	být
Hollywood	Hollywood	k1gInSc1	Hollywood
<g/>
,	,	kIx,	,
komplex	komplex	k1gInSc1	komplex
filmových	filmový	k2eAgInPc2d1	filmový
koncernů	koncern	k1gInPc2	koncern
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
–	–	k?	–
byť	byť	k8xS	byť
existují	existovat	k5eAaImIp3nP	existovat
studia	studio	k1gNnPc1	studio
i	i	k8xC	i
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zcela	zcela	k6eAd1	zcela
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
i	i	k8xC	i
globální	globální	k2eAgFnSc6d1	globální
kultuře	kultura	k1gFnSc6	kultura
si	se	k3xPyFc3	se
vydobyl	vydobýt	k5eAaPmAgMnS	vydobýt
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gMnSc2	Disnea
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
způsobil	způsobit	k5eAaPmAgInS	způsobit
revoluci	revoluce	k1gFnSc4	revoluce
v	v	k7c6	v
dětském	dětský	k2eAgInSc6d1	dětský
a	a	k8xC	a
kresleném	kreslený	k2eAgInSc6d1	kreslený
filmu	film	k1gInSc6	film
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
i	i	k9	i
vlastní	vlastní	k2eAgNnSc4d1	vlastní
obchodní	obchodní	k2eAgNnSc4d1	obchodní
impérium	impérium	k1gNnSc4	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Režiséry	režisér	k1gMnPc4	režisér
nejslavnějších	slavný	k2eAgInPc2d3	nejslavnější
hollywoodských	hollywoodský	k2eAgInPc2d1	hollywoodský
blockbusterů	blockbuster	k1gInPc2	blockbuster
byli	být	k5eAaImAgMnP	být
Steven	Steven	k2eAgMnSc1d1	Steven
Spielberg	Spielberg	k1gMnSc1	Spielberg
(	(	kIx(	(
<g/>
E.T.	E.T.	k1gMnSc1	E.T.
<g/>
,	,	kIx,	,
Čelisti	čelist	k1gFnPc1	čelist
<g/>
)	)	kIx)	)
a	a	k8xC	a
George	Georg	k1gMnSc2	Georg
Lucas	Lucasa	k1gFnPc2	Lucasa
(	(	kIx(	(
<g/>
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ron	Ron	k1gMnSc1	Ron
Howard	Howard	k1gMnSc1	Howard
proslul	proslout	k5eAaPmAgMnS	proslout
blockbustery	blockbuster	k1gMnPc4	blockbuster
podle	podle	k7c2	podle
knih	kniha	k1gFnPc2	kniha
Dana	Dana	k1gFnSc1	Dana
Browna	Browna	k1gFnSc1	Browna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ceněným	ceněný	k2eAgInSc7d1	ceněný
životopisným	životopisný	k2eAgInSc7d1	životopisný
snímkem	snímek	k1gInSc7	snímek
Čistá	čistý	k2eAgFnSc1d1	čistá
duše	duše	k1gFnSc1	duše
<g/>
.	.	kIx.	.
</s>
<s>
Westerny	western	k1gInPc1	western
proslul	proslout	k5eAaPmAgMnS	proslout
John	John	k1gMnSc1	John
Ford	ford	k1gInSc4	ford
<g/>
.	.	kIx.	.
</s>
<s>
Specialistou	specialista	k1gMnSc7	specialista
na	na	k7c6	na
sci-fi	scii	k1gFnSc6	sci-fi
byl	být	k5eAaImAgMnS	být
Robert	Robert	k1gMnSc1	Robert
Zemeckis	Zemeckis	k1gFnSc2	Zemeckis
<g/>
,	,	kIx,	,
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
kontě	konto	k1gNnSc6	konto
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
i	i	k9	i
ceněnou	ceněný	k2eAgFnSc4d1	ceněná
"	"	kIx"	"
<g/>
kroniku	kronika	k1gFnSc4	kronika
Ameriky	Amerika	k1gFnSc2	Amerika
2.	[number]	k4	2.
poloviny	polovina	k1gFnSc2	polovina
20.	[number]	k4	20.
století	století	k1gNnSc2	století
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
snímek	snímek	k1gInSc1	snímek
Forrest	Forrest	k1gMnSc1	Forrest
Gump	Gump	k1gMnSc1	Gump
<g/>
.	.	kIx.	.
</s>
<s>
Komedie	komedie	k1gFnPc4	komedie
byli	být	k5eAaImAgMnP	být
doménou	doména	k1gFnSc7	doména
George	Georg	k1gMnSc2	Georg
Cukora	Cukor	k1gMnSc2	Cukor
(	(	kIx(	(
<g/>
Adamovo	Adamův	k2eAgNnSc1d1	Adamovo
žebro	žebro	k1gNnSc1	žebro
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Billyho	Billy	k1gMnSc2	Billy
Wildera	Wilder	k1gMnSc2	Wilder
(	(	kIx(	(
<g/>
Někdo	někdo	k3yInSc1	někdo
to	ten	k3xDgNnSc4	ten
rád	rád	k6eAd1	rád
horké	horký	k2eAgNnSc1d1	horké
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komediální	komediální	k2eAgFnPc1d1	komediální
klasiky	klasika	k1gFnPc1	klasika
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
kontě	konto	k1gNnSc6	konto
i	i	k8xC	i
Woody	Wood	k1gInPc4	Wood
Allen	allen	k1gInSc1	allen
(	(	kIx(	(
<g/>
Annie	Annie	k1gFnSc1	Annie
Hallová	Hallová	k1gFnSc1	Hallová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sydney	Sydney	k1gNnSc4	Sydney
Pollack	Pollacka	k1gFnPc2	Pollacka
(	(	kIx(	(
<g/>
Tootsie	Tootsie	k1gFnSc1	Tootsie
<g/>
)	)	kIx)	)
či	či	k8xC	či
William	William	k1gInSc1	William
Wyler	Wylra	k1gFnPc2	Wylra
(	(	kIx(	(
<g/>
Prázdniny	prázdniny	k1gFnPc1	prázdniny
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnSc3d1	velká
mafiánské	mafiánský	k2eAgFnSc3d1	mafiánská
podívané	podívaná	k1gFnSc3	podívaná
proslavili	proslavit	k5eAaPmAgMnP	proslavit
Martina	Martin	k1gMnSc4	Martin
Scorseseho	Scorsese	k1gMnSc4	Scorsese
(	(	kIx(	(
<g/>
Gangy	Ganga	k1gFnSc2	Ganga
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
)	)	kIx)	)
a	a	k8xC	a
Francise	Francise	k1gFnSc1	Francise
Forda	ford	k1gMnSc2	ford
Coppolu	Coppol	k1gInSc2	Coppol
(	(	kIx(	(
<g/>
Kmotr	kmotr	k1gMnSc1	kmotr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Akční	akční	k2eAgInSc1d1	akční
film	film	k1gInSc1	film
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgInS	pokusit
na	na	k7c4	na
umění	umění	k1gNnSc4	umění
povýšit	povýšit	k5eAaPmF	povýšit
Quentin	Quentin	k1gInSc4	Quentin
Tarantino	Tarantin	k2eAgNnSc1d1	Tarantino
(	(	kIx(	(
<g/>
Pulp	pulpa	k1gFnPc2	pulpa
Fiction	Fiction	k1gInSc1	Fiction
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Surrealistické	surrealistický	k2eAgFnPc1d1	surrealistická
inspirace	inspirace	k1gFnPc1	inspirace
ani	ani	k8xC	ani
v	v	k7c6	v
komerční	komerční	k2eAgFnSc6d1	komerční
produkci	produkce	k1gFnSc6	produkce
nezapřel	zapřít	k5eNaPmAgMnS	zapřít
originální	originální	k2eAgNnSc4d1	originální
Tim	Tim	k?	Tim
Burton	Burton	k1gInSc4	Burton
(	(	kIx(	(
<g/>
Batman	Batman	k1gMnSc1	Batman
<g/>
,	,	kIx,	,
Karlík	Karlík	k1gMnSc1	Karlík
a	a	k8xC	a
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
čokoládu	čokoláda	k1gFnSc4	čokoláda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Symbolem	symbol	k1gInSc7	symbol
uměleckého	umělecký	k2eAgInSc2d1	umělecký
filmu	film	k1gInSc2	film
jsou	být	k5eAaImIp3nP	být
naopak	naopak	k6eAd1	naopak
tvůrci	tvůrce	k1gMnPc1	tvůrce
jako	jako	k9	jako
Orson	Orson	k1gMnSc1	Orson
Welles	Welles	k1gMnSc1	Welles
(	(	kIx(	(
<g/>
Občan	občan	k1gMnSc1	občan
Kane	kanout	k5eAaImIp3nS	kanout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stanley	Stanley	k1gInPc1	Stanley
Kubrick	Kubrick	k1gInSc1	Kubrick
(	(	kIx(	(
<g/>
Mechanický	mechanický	k2eAgInSc1d1	mechanický
pomeranč	pomeranč	k1gInSc1	pomeranč
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
David	David	k1gMnSc1	David
Lynch	Lynch	k1gMnSc1	Lynch
(	(	kIx(	(
<g/>
Modrý	modrý	k2eAgInSc1d1	modrý
samet	samet	k1gInSc1	samet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Umělecké	umělecký	k2eAgFnPc4d1	umělecká
ambice	ambice	k1gFnPc4	ambice
měl	mít	k5eAaImAgMnS	mít
i	i	k9	i
Elia	Elia	k1gMnSc1	Elia
Kazan	Kazan	k1gMnSc1	Kazan
(	(	kIx(	(
<g/>
Na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
ráje	ráj	k1gInSc2	ráj
<g/>
)	)	kIx)	)
či	či	k8xC	či
Sidney	Sidnea	k1gMnSc2	Sidnea
Lumet	Lumeta	k1gFnPc2	Lumeta
(	(	kIx(	(
<g/>
Dvanáct	dvanáct	k4xCc1	dvanáct
rozhněvaných	rozhněvaný	k2eAgMnPc2d1	rozhněvaný
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Politický	politický	k2eAgInSc1d1	politický
aktivismus	aktivismus	k1gInSc1	aktivismus
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
motivem	motiv	k1gInSc7	motiv
Olivera	Oliver	k1gMnSc2	Oliver
Stonea	Stoneus	k1gMnSc2	Stoneus
(	(	kIx(	(
<g/>
</s>
<s>
Četa	četa	k1gFnSc1	četa
<g/>
,	,	kIx,	,
JFK	JFK	kA	JFK
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
dokumentaristy	dokumentarista	k1gMnSc2	dokumentarista
Michaela	Michael	k1gMnSc2	Michael
Moorea	Mooreus	k1gMnSc2	Mooreus
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgInSc4d1	silný
politický	politický	k2eAgInSc4d1	politický
akcent	akcent	k1gInSc4	akcent
nesly	nést	k5eAaImAgInP	nést
již	již	k9	již
snímky	snímek	k1gInPc1	snímek
průkopníka	průkopník	k1gMnSc2	průkopník
amerického	americký	k2eAgInSc2d1	americký
filmu	film	k1gInSc2	film
D.	D.	kA	D.
W.	W.	kA	W.
Griffitha	Griffitha	k1gFnSc1	Griffitha
(	(	kIx(	(
<g/>
Zrození	zrození	k1gNnSc1	zrození
národa	národ	k1gInSc2	národ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Hollywood	Hollywood	k1gInSc1	Hollywood
produkuje	produkovat	k5eAaImIp3nS	produkovat
i	i	k9	i
nesčetně	sčetně	k6eNd1	sčetně
hereckých	herecký	k2eAgFnPc2d1	herecká
filmových	filmový	k2eAgFnPc2d1	filmová
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
globálními	globální	k2eAgFnPc7d1	globální
celebritami	celebrita	k1gFnPc7	celebrita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
éře	éra	k1gFnSc6	éra
klasického	klasický	k2eAgInSc2d1	klasický
hollywoodu	hollywood	k1gInSc2	hollywood
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
mezi	mezi	k7c7	mezi
ženami	žena	k1gFnPc7	žena
Mary	Mary	k1gFnSc1	Mary
Pickfordová	Pickfordová	k1gFnSc1	Pickfordová
<g/>
,	,	kIx,	,
Lillian	Lillian	k1gInSc1	Lillian
Gishová	Gishová	k1gFnSc1	Gishová
<g/>
,	,	kIx,	,
Němka	Němka	k1gFnSc1	Němka
Marlene	Marlen	k1gInSc5	Marlen
Dietrichová	Dietrichová	k1gFnSc5	Dietrichová
<g/>
,	,	kIx,	,
Joan	Joan	k1gMnSc1	Joan
Crawfordová	Crawfordová	k1gFnSc1	Crawfordová
<g/>
,	,	kIx,	,
Grace	Grace	k1gFnSc1	Grace
Kellyová	Kellyová	k1gFnSc1	Kellyová
<g/>
,	,	kIx,	,
Judy	judo	k1gNnPc7	judo
Garlandová	Garlandový	k2eAgFnSc1d1	Garlandová
<g/>
,	,	kIx,	,
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
<g/>
,	,	kIx,	,
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Taylorová	Taylorová	k1gFnSc1	Taylorová
<g/>
,	,	kIx,	,
Katharine	Katharin	k1gInSc5	Katharin
Hepburnová	Hepburnová	k1gFnSc1	Hepburnová
<g/>
,	,	kIx,	,
Rita	Rita	k1gFnSc1	Rita
Hayworthová	Hayworthová	k1gFnSc1	Hayworthová
<g/>
,	,	kIx,	,
Lauren	Laurna	k1gFnPc2	Laurna
Bacallová	Bacallový	k2eAgFnSc1d1	Bacallová
<g/>
,	,	kIx,	,
belgicko-britská	belgickoritský	k2eAgFnSc1d1	belgicko-britský
herečka	herečka	k1gFnSc1	herečka
Audrey	Audrea	k1gFnSc2	Audrea
Hepburnová	Hepburnový	k2eAgFnSc1d1	Hepburnová
či	či	k8xC	či
dětská	dětský	k2eAgFnSc1d1	dětská
hvězda	hvězda	k1gFnSc1	hvězda
Shirley	Shirlea	k1gFnSc2	Shirlea
Temple	templ	k1gInSc5	templ
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mužů	muž	k1gMnPc2	muž
pak	pak	k6eAd1	pak
Buster	Buster	k1gMnSc1	Buster
Keaton	Keaton	k1gInSc1	Keaton
<g/>
,	,	kIx,	,
Angličan	Angličan	k1gMnSc1	Angličan
Charlie	Charlie	k1gMnSc1	Charlie
Chaplin	Chaplin	k2eAgMnSc1d1	Chaplin
<g/>
,	,	kIx,	,
Harold	Harold	k1gMnSc1	Harold
Lloyd	Lloyd	k1gMnSc1	Lloyd
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Wayne	Wayn	k1gInSc5	Wayn
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnPc4	Henry
Fonda	Fond	k1gMnSc4	Fond
<g/>
,	,	kIx,	,
Gregory	Gregor	k1gMnPc4	Gregor
Peck	Pecka	k1gFnPc2	Pecka
<g/>
,	,	kIx,	,
Fred	Fred	k1gMnSc1	Fred
Astaire	Astair	k1gInSc5	Astair
<g/>
,	,	kIx,	,
Cary	car	k1gMnPc4	car
Grant	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
Humphrey	Humphrea	k1gFnPc1	Humphrea
Bogart	Bogart	k1gInSc1	Bogart
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Dean	Dean	k1gMnSc1	Dean
<g/>
,	,	kIx,	,
Marlon	Marlon	k1gInSc4	Marlon
Brando	Brando	k1gNnSc4	Brando
<g/>
,	,	kIx,	,
Clark	Clark	k1gInSc4	Clark
Gable	Gable	k1gFnSc2	Gable
<g/>
,	,	kIx,	,
Bratři	bratr	k1gMnPc1	bratr
Marxové	Marxová	k1gFnSc2	Marxová
<g/>
,	,	kIx,	,
Gary	Gara	k1gFnPc1	Gara
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
,	,	kIx,	,
Spencer	Spencer	k1gMnSc1	Spencer
Tracy	Traca	k1gFnSc2	Traca
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Stewart	Stewart	k1gMnSc1	Stewart
nebo	nebo	k8xC	nebo
Charlton	Charlton	k1gInSc1	Charlton
Heston	Heston	k1gInSc1	Heston
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
hvězdy	hvězda	k1gFnPc1	hvězda
jako	jako	k8xS	jako
Julia	Julius	k1gMnSc2	Julius
Robertsová	Robertsová	k1gFnSc1	Robertsová
<g/>
,	,	kIx,	,
Sandra	Sandra	k1gFnSc1	Sandra
Bullocková	Bullocková	k1gFnSc1	Bullocková
<g/>
,	,	kIx,	,
Meryl	Meryl	k1gInSc1	Meryl
Streepová	Streepová	k1gFnSc1	Streepová
<g/>
,	,	kIx,	,
Barbra	Barbra	k1gFnSc1	Barbra
Streisandová	Streisandový	k2eAgFnSc1d1	Streisandová
<g/>
,	,	kIx,	,
Sigourney	Sigournea	k1gFnPc1	Sigournea
Weaver	Weaver	k1gMnSc1	Weaver
<g/>
,	,	kIx,	,
Gwyneth	Gwyneth	k1gMnSc1	Gwyneth
Paltrowová	Paltrowová	k1gFnSc1	Paltrowová
<g/>
,	,	kIx,	,
Julianne	Juliann	k1gMnSc5	Juliann
Moore	Moor	k1gMnSc5	Moor
<g/>
,	,	kIx,	,
Whoopi	Whoop	k1gFnSc2	Whoop
Goldbergová	Goldbergový	k2eAgFnSc1d1	Goldbergová
<g/>
,	,	kIx,	,
Cameron	Cameron	k1gInSc1	Cameron
Diazová	Diazová	k1gFnSc1	Diazová
<g/>
,	,	kIx,	,
Jennifer	Jennifra	k1gFnPc2	Jennifra
Anistonová	Anistonový	k2eAgFnSc1d1	Anistonová
<g/>
,	,	kIx,	,
Jodie	Jodie	k1gFnSc1	Jodie
Fosterová	Fosterová	k1gFnSc1	Fosterová
<g/>
,	,	kIx,	,
Angelina	Angelin	k2eAgFnSc1d1	Angelina
Jolie	Jolie	k1gFnSc1	Jolie
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Jennifer	Jennifer	k1gInSc1	Jennifer
Lopezová	Lopezová	k1gFnSc1	Lopezová
<g/>
,	,	kIx,	,
Robin	robin	k2eAgInSc1d1	robin
Williams	Williams	k1gInSc1	Williams
<g/>
,	,	kIx,	,
Brad	brada	k1gFnPc2	brada
Pitt	Pitta	k1gFnPc2	Pitta
<g/>
,	,	kIx,	,
Leonardo	Leonardo	k1gMnSc1	Leonardo
DiCaprio	DiCaprio	k1gMnSc1	DiCaprio
<g/>
,	,	kIx,	,
Arnold	Arnold	k1gMnSc1	Arnold
Schwarzenegger	Schwarzenegger	k1gMnSc1	Schwarzenegger
<g/>
,	,	kIx,	,
Bruce	Bruce	k1gFnPc1	Bruce
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
Johnny	Johnna	k1gFnPc1	Johnna
Depp	Depp	k1gMnSc1	Depp
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Redford	Redford	k1gMnSc1	Redford
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
De	De	k?	De
Niro	Niro	k1gMnSc1	Niro
<g/>
,	,	kIx,	,
Jim	on	k3xPp3gMnPc3	on
Carrey	Carrea	k1gFnPc1	Carrea
<g/>
,	,	kIx,	,
Tom	Tom	k1gMnSc1	Tom
Cruise	Cruise	k1gFnSc1	Cruise
<g/>
,	,	kIx,	,
Sylvester	Sylvester	k1gInSc1	Sylvester
Stallone	Stallon	k1gInSc5	Stallon
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Bruce	Bruce	k1gMnSc1	Bruce
Willis	Willis	k1gFnSc2	Willis
<g/>
,	,	kIx,	,
Kevin	Kevin	k1gMnSc1	Kevin
Costner	Costner	k1gMnSc1	Costner
<g/>
,	,	kIx,	,
Tom	Tom	k1gMnSc1	Tom
Hanks	Hanks	k1gInSc1	Hanks
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
Nicholson	Nicholson	k1gMnSc1	Nicholson
<g/>
,	,	kIx,	,
Clint	Clint	k1gMnSc1	Clint
Eastwood	Eastwood	k1gInSc1	Eastwood
<g/>
,	,	kIx,	,
Morgan	morgan	k1gMnSc1	morgan
Freeman	Freeman	k1gMnSc1	Freeman
<g/>
,	,	kIx,	,
Will	Will	k1gMnSc1	Will
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
Clooney	Cloonea	k1gMnSc2	Cloonea
<g/>
,	,	kIx,	,
Eddie	Eddie	k1gFnSc2	Eddie
Murphy	Murpha	k1gFnSc2	Murpha
<g/>
,	,	kIx,	,
Mel	mlít	k5eAaImRp2nS	mlít
Gibson	Gibson	k1gMnSc1	Gibson
<g/>
,	,	kIx,	,
Nicolas	Nicolas	k1gMnSc1	Nicolas
Cage	Cag	k1gFnSc2	Cag
<g/>
,	,	kIx,	,
Harrison	Harrison	k1gMnSc1	Harrison
Ford	ford	k1gInSc1	ford
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Newman	Newman	k1gMnSc1	Newman
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Travolta	Travolta	k1gMnSc1	Travolta
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Douglas	Douglas	k1gMnSc1	Douglas
<g/>
,	,	kIx,	,
Patrick	Patrick	k1gMnSc1	Patrick
Swayze	Swayze	k1gFnSc2	Swayze
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Gere	Ger	k1gFnSc2	Ger
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
jiní	jiný	k1gMnPc1	jiný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americká	americký	k2eAgFnSc1d1	americká
Akademie	akademie	k1gFnSc1	akademie
filmového	filmový	k2eAgNnSc2d1	filmové
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
každoročně	každoročně	k6eAd1	každoročně
uděluje	udělovat	k5eAaImIp3nS	udělovat
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
všeobecně	všeobecně	k6eAd1	všeobecně
říká	říkat	k5eAaImIp3nS	říkat
Oscar	Oscar	k1gInSc1	Oscar
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
Cena	cena	k1gFnSc1	cena
Akademie	akademie	k1gFnSc2	akademie
–	–	k?	–
Academy	Academa	k1gFnSc2	Academa
Award	Awarda	k1gFnPc2	Awarda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
prestiž	prestiž	k1gFnSc4	prestiž
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
(	(	kIx(	(
<g/>
Golden	Goldna	k1gFnPc2	Goldna
Globe	globus	k1gInSc5	globus
Award	Award	k1gInSc1	Award
<g/>
)	)	kIx)	)
udělovaný	udělovaný	k2eAgInSc1d1	udělovaný
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
Asociací	asociace	k1gFnSc7	asociace
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
novinářů	novinář	k1gMnPc2	novinář
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
iluzionista	iluzionista	k1gMnSc1	iluzionista
celosvětově	celosvětově	k6eAd1	celosvětově
proslul	proslout	k5eAaPmAgMnS	proslout
David	David	k1gMnSc1	David
Copperfield	Copperfield	k1gMnSc1	Copperfield
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Media	medium	k1gNnPc4	medium
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgNnPc4d3	nejvýznamnější
americká	americký	k2eAgNnPc4d1	americké
média	médium	k1gNnPc4	médium
patří	patřit	k5eAaImIp3nS	patřit
tzv.	tzv.	kA	tzv.
velká	velký	k2eAgFnSc1d1	velká
trojka	trojka	k1gFnSc1	trojka
televizních	televizní	k2eAgFnPc2d1	televizní
společností	společnost	k1gFnPc2	společnost
–	–	k?	–
NBC	NBC	kA	NBC
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
a	a	k8xC	a
CBS	CBS	kA	CBS
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
televizní	televizní	k2eAgFnSc1d1	televizní
síť	síť	k1gFnSc1	síť
Fox	fox	k1gInSc1	fox
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
kabelová	kabelový	k2eAgFnSc1d1	kabelová
televizní	televizní	k2eAgFnSc1d1	televizní
společnost	společnost	k1gFnSc1	společnost
CNN	CNN	kA	CNN
a	a	k8xC	a
na	na	k7c4	na
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
Bloomberg	Bloomberg	k1gInSc1	Bloomberg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
novinám	novina	k1gFnPc3	novina
–	–	k?	–
i	i	k8xC	i
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
–	–	k?	–
patří	patřit	k5eAaImIp3nS	patřit
deník	deník	k1gInSc1	deník
The	The	k1gFnSc2	The
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Times	Timesa	k1gFnPc2	Timesa
mediálního	mediální	k2eAgInSc2d1	mediální
konglomerátu	konglomerát	k1gInSc2	konglomerát
Time	Tim	k1gFnSc2	Tim
Warner	Warnra	k1gFnPc2	Warnra
a	a	k8xC	a
také	také	k9	také
The	The	k1gMnSc1	The
Wall	Wall	k1gMnSc1	Wall
Street	Street	k1gMnSc1	Street
Journal	Journal	k1gMnSc1	Journal
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
vydávaný	vydávaný	k2eAgInSc1d1	vydávaný
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaImF	jmenovat
mj.	mj.	kA	mj.
The	The	k1gFnSc1	The
Washington	Washington	k1gInSc1	Washington
Post	post	k1gInSc1	post
<g/>
,	,	kIx,	,
Chicago	Chicago	k1gNnSc1	Chicago
Tribune	tribun	k1gMnSc5	tribun
a	a	k8xC	a
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
Times	Times	k1gMnSc1	Times
<g/>
.	.	kIx.	.
</s>
<s>
Značný	značný	k2eAgInSc1d1	značný
význam	význam	k1gInSc1	význam
je	být	k5eAaImIp3nS	být
přisuzován	přisuzován	k2eAgInSc1d1	přisuzován
také	také	k9	také
internetovým	internetový	k2eAgFnPc3d1	internetová
novinám	novina	k1gFnPc3	novina
The	The	k1gMnPc2	The
Huffington	Huffington	k1gInSc4	Huffington
Post	posta	k1gFnPc2	posta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
periodickými	periodický	k2eAgInPc7d1	periodický
časopisy	časopis	k1gInPc7	časopis
vynikají	vynikat	k5eAaImIp3nP	vynikat
týdeníky	týdeník	k1gInPc7	týdeník
Time	Time	k1gNnSc2	Time
a	a	k8xC	a
Newsweek	Newsweky	k1gFnPc2	Newsweky
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
Latinskou	latinský	k2eAgFnSc4d1	Latinská
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
,	,	kIx,	,
Afriku	Afrika	k1gFnSc4	Afrika
a	a	k8xC	a
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
vychází	vycházet	k5eAaImIp3nS	vycházet
Time	Timus	k1gMnSc5	Timus
Europe	Europ	k1gMnSc5	Europ
<g/>
.	.	kIx.	.
</s>
<s>
Time	Time	k1gFnSc1	Time
Asia	Asi	k1gInSc2	Asi
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
Hongkongu	Hongkong	k1gInSc6	Hongkong
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Newsweek	Newsweek	k6eAd1	Newsweek
vychází	vycházet	k5eAaImIp3nS	vycházet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
vědeckých	vědecký	k2eAgInPc2d1	vědecký
a	a	k8xC	a
populárně-vědeckých	populárněědecký	k2eAgInPc2d1	populárně-vědecký
časopisů	časopis	k1gInPc2	časopis
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
National	National	k1gMnSc1	National
Geographic	Geographic	k1gMnSc1	Geographic
Magazine	Magazin	k1gMnSc5	Magazin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Kuchyně	kuchyně	k1gFnSc2	kuchyně
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Na	na	k7c4	na
Den	den	k1gInSc4	den
díkůvzdání	díkůvzdání	k1gNnSc2	díkůvzdání
většina	většina	k1gFnSc1	většina
Američanů	Američan	k1gMnPc2	Američan
jí	jíst	k5eAaImIp3nS	jíst
tradiční	tradiční	k2eAgNnPc4d1	tradiční
jídla	jídlo	k1gNnPc4	jídlo
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
osidlování	osidlování	k1gNnSc2	osidlování
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
patří	patřit	k5eAaImIp3nS	patřit
zejména	zejména	k9	zejména
pečený	pečený	k2eAgMnSc1d1	pečený
krocan	krocan	k1gMnSc1	krocan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
sladké	sladký	k2eAgFnPc1d1	sladká
brambory	brambora	k1gFnPc1	brambora
či	či	k8xC	či
javorový	javorový	k2eAgInSc1d1	javorový
sirup	sirup	k1gInSc1	sirup
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
kultuře	kultura	k1gFnSc6	kultura
také	také	k9	také
hraje	hrát	k5eAaImIp3nS	hrát
jablečný	jablečný	k2eAgInSc1d1	jablečný
koláč	koláč	k1gInSc1	koláč
(	(	kIx(	(
<g/>
zvaný	zvaný	k2eAgInSc1d1	zvaný
často	často	k6eAd1	často
americký	americký	k2eAgInSc1d1	americký
koláč	koláč	k1gInSc1	koláč
–	–	k?	–
american	american	k1gInSc1	american
pie	pie	k?	pie
<g/>
)	)	kIx)	)
či	či	k8xC	či
hovězí	hovězí	k2eAgInSc4d1	hovězí
steak	steak	k1gInSc4	steak
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20.	[number]	k4	20.
století	století	k1gNnSc6	století
si	se	k3xPyFc3	se
Američané	Američan	k1gMnPc1	Američan
oblíbili	oblíbit	k5eAaPmAgMnP	oblíbit
hamburgery	hamburger	k1gInPc4	hamburger
a	a	k8xC	a
hot	hot	k0	hot
dogy	doga	k1gFnPc1	doga
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20.	[number]	k4	20.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
americké	americký	k2eAgFnSc2d1	americká
kultury	kultura	k1gFnSc2	kultura
staly	stát	k5eAaPmAgFnP	stát
fast	fast	k5eAaPmF	fast
food	food	k1gInSc4	food
restaurace	restaurace	k1gFnSc2	restaurace
jako	jako	k8xC	jako
McDonald	McDonald	k1gMnSc1	McDonald
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
KFC	KFC	kA	KFC
či	či	k8xC	či
Burger	Burger	k1gMnSc1	Burger
King	King	k1gMnSc1	King
<g/>
.	.	kIx.	.
</s>
<s>
Obliba	obliba	k1gFnSc1	obliba
rychlého	rychlý	k2eAgNnSc2d1	rychlé
občerstvení	občerstvení	k1gNnSc2	občerstvení
je	být	k5eAaImIp3nS	být
však	však	k9	však
i	i	k9	i
předmětem	předmět	k1gInSc7	předmět
veřejné	veřejný	k2eAgFnSc2d1	veřejná
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Podílela	podílet	k5eAaImAgFnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
a	a	k8xC	a
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
příjem	příjem	k1gInSc1	příjem
kalorií	kalorie	k1gFnPc2	kalorie
Američanů	Američan	k1gMnPc2	Američan
zvýšil	zvýšit	k5eAaPmAgMnS	zvýšit
o	o	k7c4	o
24	[number]	k4	24
<g/>
%	%	kIx~	%
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
epidemií	epidemie	k1gFnSc7	epidemie
obezity	obezita	k1gFnSc2	obezita
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
USA	USA	kA	USA
spotřeba	spotřeba	k1gFnSc1	spotřeba
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
kavárenský	kavárenský	k2eAgInSc1d1	kavárenský
řetězec	řetězec	k1gInSc1	řetězec
Starbucks	Starbucksa	k1gFnPc2	Starbucksa
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
slavnější	slavný	k2eAgFnSc1d2	slavnější
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
kofeinová	kofeinový	k2eAgFnSc1d1	kofeinová
limonáda	limonáda	k1gFnSc1	limonáda
Coca-Cola	cocaola	k1gFnSc1	coca-cola
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sladkostí	sladkost	k1gFnPc2	sladkost
jsou	být	k5eAaImIp3nP	být
oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
lívance	lívanec	k1gInPc1	lívanec
polévané	polévaný	k2eAgInPc1d1	polévaný
sirupem	sirup	k1gInSc7	sirup
<g/>
,	,	kIx,	,
donut	donut	k1gInSc1	donut
(	(	kIx(	(
<g/>
americká	americký	k2eAgFnSc1d1	americká
verze	verze	k1gFnSc1	verze
koblihy	kobliha	k1gFnSc2	kobliha
s	s	k7c7	s
dírou	díra	k1gFnSc7	díra
uprostřed	uprostřed	k7c2	uprostřed
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sušenky	sušenka	k1gFnPc1	sušenka
cookies	cookies	k1gInSc1	cookies
<g/>
,	,	kIx,	,
dortíky	dortík	k1gInPc1	dortík
muffiny	muffin	k2eAgInPc1d1	muffin
<g/>
,	,	kIx,	,
moučník	moučník	k1gInSc1	moučník
brownie	brownie	k1gFnSc2	brownie
či	či	k8xC	či
tvarohový	tvarohový	k2eAgInSc4d1	tvarohový
dort	dort	k1gInSc4	dort
cheesecake	cheesecak	k1gFnSc2	cheesecak
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
americkou	americký	k2eAgFnSc4d1	americká
gastronomii	gastronomie	k1gFnSc4	gastronomie
měly	mít	k5eAaImAgFnP	mít
pochopitelně	pochopitelně	k6eAd1	pochopitelně
i	i	k8xC	i
vlny	vlna	k1gFnSc2	vlna
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
,	,	kIx,	,
Italové	Ital	k1gMnPc1	Ital
sebou	se	k3xPyFc7	se
přivezli	přivézt	k5eAaPmAgMnP	přivézt
oblibu	obliba	k1gFnSc4	obliba
pizzy	pizza	k1gFnSc2	pizza
<g/>
,	,	kIx,	,
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
pak	pak	k6eAd1	pak
jídel	jídlo	k1gNnPc2	jídlo
jako	jako	k8xC	jako
burrito	burrita	k1gFnSc5	burrita
či	či	k8xC	či
tacos	tacos	k1gInSc4	tacos
a	a	k8xC	a
velkou	velký	k2eAgFnSc4d1	velká
oblibu	obliba	k1gFnSc4	obliba
si	se	k3xPyFc3	se
získalo	získat	k5eAaPmAgNnS	získat
i	i	k9	i
jídlo	jídlo	k1gNnSc1	jídlo
čínské	čínský	k2eAgFnSc2d1	čínská
či	či	k8xC	či
thajské	thajský	k2eAgFnSc2d1	thajská
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
regionálním	regionální	k2eAgFnPc3d1	regionální
variantám	varianta	k1gFnPc3	varianta
americké	americký	k2eAgFnSc2d1	americká
kuchyně	kuchyně	k1gFnSc2	kuchyně
patří	patřit	k5eAaImIp3nS	patřit
jižanský	jižanský	k2eAgInSc1d1	jižanský
Tex-Mex	Tex-Mex	k1gInSc1	Tex-Mex
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejtypičtějším	typický	k2eAgInSc7d3	nejtypičtější
pokrmem	pokrm	k1gInSc7	pokrm
je	být	k5eAaImIp3nS	být
chili	chile	k1gFnSc3	chile
con	con	k?	con
carne	carnout	k5eAaPmIp3nS	carnout
<g/>
.	.	kIx.	.
</s>
<s>
Klasickými	klasický	k2eAgFnPc7d1	klasická
surovinami	surovina	k1gFnPc7	surovina
jsou	být	k5eAaImIp3nP	být
fazole	fazole	k1gFnSc1	fazole
<g/>
,	,	kIx,	,
čedar	čedar	k1gInSc1	čedar
<g/>
,	,	kIx,	,
hovězí	hovězí	k2eAgFnSc1d1	hovězí
a	a	k8xC	a
kukuřičná	kukuřičný	k2eAgFnSc1d1	kukuřičná
mouka	mouka	k1gFnSc1	mouka
na	na	k7c4	na
tortilly	tortilla	k1gFnPc4	tortilla
<g/>
.	.	kIx.	.
<g/>
Specifickou	specifický	k2eAgFnSc4d1	specifická
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
mladou	mladá	k1gFnSc7	mladá
<g/>
,	,	kIx,	,
tradicí	tradice	k1gFnSc7	tradice
amerického	americký	k2eAgNnSc2d1	americké
stravování	stravování	k1gNnSc2	stravování
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
brunch	brunch	k1gInSc1	brunch
<g/>
,	,	kIx,	,
dopolední	dopolední	k2eAgNnSc1d1	dopolední
jídlo	jídlo	k1gNnSc1	jídlo
mezi	mezi	k7c7	mezi
časem	čas	k1gInSc7	čas
snídaně	snídaně	k1gFnSc2	snídaně
a	a	k8xC	a
oběda	oběd	k1gInSc2	oběd
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
brunch	brun	k1gFnPc6	brun
chodí	chodit	k5eAaImIp3nP	chodit
Američané	Američan	k1gMnPc1	Američan
zejména	zejména	k9	zejména
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
restaurací	restaurace	k1gFnPc2	restaurace
není	být	k5eNaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
slušné	slušný	k2eAgMnPc4d1	slušný
sednout	sednout	k5eAaPmF	sednout
si	se	k3xPyFc3	se
rovnou	rovnou	k6eAd1	rovnou
ke	k	k7c3	k
stolu	stol	k1gInSc3	stol
<g/>
,	,	kIx,	,
na	na	k7c6	na
uvedení	uvedení	k1gNnSc6	uvedení
ke	k	k7c3	k
stolu	stol	k1gInSc3	stol
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
počkat	počkat	k5eAaPmF	počkat
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgFnPc1d1	typická
pro	pro	k7c4	pro
americké	americký	k2eAgFnPc4d1	americká
restaurace	restaurace	k1gFnPc4	restaurace
rovněž	rovněž	k9	rovněž
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsluha	obsluha	k1gFnSc1	obsluha
je	být	k5eAaImIp3nS	být
placena	platit	k5eAaImNgFnS	platit
výhradně	výhradně	k6eAd1	výhradně
ze	z	k7c2	z
spropitného	spropitné	k1gNnSc2	spropitné
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
obzvláště	obzvláště	k6eAd1	obzvláště
neslušné	slušný	k2eNgNnSc4d1	neslušné
dýško	dýško	k1gNnSc4	dýško
nedat	dat	k5eNaImF	dat
<g/>
.	.	kIx.	.
</s>
<s>
Očekávanou	očekávaný	k2eAgFnSc7d1	očekávaná
jeho	jeho	k3xOp3gFnSc7	jeho
výší	výše	k1gFnSc7	výše
je	být	k5eAaImIp3nS	být
15-20	[number]	k4	15-20
procent	procento	k1gNnPc2	procento
ceny	cena	k1gFnSc2	cena
jídla	jídlo	k1gNnSc2	jídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Věda	věda	k1gFnSc1	věda
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Jakýmsi	jakýsi	k3yIgMnSc7	jakýsi
praotcem	praotec	k1gMnSc7	praotec
americké	americký	k2eAgFnSc2d1	americká
vědy	věda	k1gFnSc2	věda
byl	být	k5eAaImAgMnS	být
všestranný	všestranný	k2eAgMnSc1d1	všestranný
učenec	učenec	k1gMnSc1	učenec
Benjamin	Benjamin	k1gMnSc1	Benjamin
Franklin	Franklin	k2eAgMnSc1d1	Franklin
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
věda	věda	k1gFnSc1	věda
byla	být	k5eAaImAgFnS	být
dlouho	dlouho	k6eAd1	dlouho
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
evropské	evropský	k2eAgNnSc1d1	Evropské
<g/>
,	,	kIx,	,
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19.	[number]	k4	19.
století	století	k1gNnPc2	století
se	se	k3xPyFc4	se
však	však	k9	však
začala	začít	k5eAaPmAgFnS	začít
prudce	prudko	k6eAd1	prudko
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
této	tento	k3xDgFnSc2	tento
éry	éra	k1gFnSc2	éra
byli	být	k5eAaImAgMnP	být
vynálezci	vynálezce	k1gMnPc1	vynálezce
jako	jako	k9	jako
Samuel	Samuel	k1gMnSc1	Samuel
F.	F.	kA	F.
B.	B.	kA	B.
Morse	Morse	k1gMnSc1	Morse
(	(	kIx(	(
<g/>
elektrický	elektrický	k2eAgInSc1d1	elektrický
telegraf	telegraf	k1gInSc1	telegraf
<g/>
)	)	kIx)	)
a	a	k8xC	a
Thomas	Thomas	k1gMnSc1	Thomas
Alva	Alva	k1gMnSc1	Alva
Edison	Edison	k1gMnSc1	Edison
(	(	kIx(	(
<g/>
fonograf	fonograf	k1gInSc1	fonograf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Bratři	bratr	k1gMnPc1	bratr
Wrightové	Wrightová	k1gFnSc2	Wrightová
<g/>
,	,	kIx,	,
tvůrci	tvůrce	k1gMnPc1	tvůrce
prvního	první	k4xOgNnSc2	první
letadla	letadlo	k1gNnSc2	letadlo
těžšího	těžký	k2eAgNnSc2d2	těžší
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Obrovskou	obrovský	k2eAgFnSc7d1	obrovská
posilou	posila	k1gFnSc7	posila
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vlna	vlna	k1gFnSc1	vlna
vědeckých	vědecký	k2eAgMnPc2d1	vědecký
emigrantů	emigrant	k1gMnPc2	emigrant
ve	v	k7c6	v
30.	[number]	k4	30.
letech	léto	k1gNnPc6	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
prchali	prchat	k5eAaImAgMnP	prchat
před	před	k7c7	před
nacismem	nacismus	k1gInSc7	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
i	i	k9	i
lidé	člověk	k1gMnPc1	člověk
jako	jako	k8xC	jako
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Abraham	Abraham	k1gMnSc1	Abraham
Michelson	Michelson	k1gMnSc1	Michelson
<g/>
,	,	kIx,	,
Enrico	Enrico	k6eAd1	Enrico
Fermi	Fer	k1gFnPc7	Fer
nebo	nebo	k8xC	nebo
John	John	k1gMnSc1	John
von	von	k1gInSc4	von
Neumann	Neumann	k1gMnSc1	Neumann
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
americkou	americký	k2eAgFnSc4d1	americká
vědu	věda	k1gFnSc4	věda
rychle	rychle	k6eAd1	rychle
vytáhli	vytáhnout	k5eAaPmAgMnP	vytáhnout
na	na	k7c4	na
špičkovou	špičkový	k2eAgFnSc4d1	špičková
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
podporováni	podporovat	k5eAaImNgMnP	podporovat
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
silou	síla	k1gFnSc7	síla
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
této	tento	k3xDgFnSc2	tento
nové	nový	k2eAgFnSc2d1	nová
síly	síla	k1gFnSc2	síla
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Projekt	projekt	k1gInSc1	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
(	(	kIx(	(
<g/>
řízený	řízený	k2eAgInSc1d1	řízený
Robertem	Robert	k1gMnSc7	Robert
Oppenheimerem	Oppenheimer	k1gMnSc7	Oppenheimer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dal	dát	k5eAaPmAgMnS	dát
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
jako	jako	k8xC	jako
první	první	k4xOgFnSc3	první
zemi	zem	k1gFnSc3	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
atomovou	atomový	k2eAgFnSc4d1	atomová
bombu	bomba	k1gFnSc4	bomba
<g/>
,	,	kIx,	,
Program	program	k1gInSc4	program
Apollo	Apollo	k1gNnSc4	Apollo
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Američany	Američan	k1gMnPc7	Američan
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
přivedl	přivést	k5eAaPmAgInS	přivést
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
(	(	kIx(	(
<g/>
v	v	k7c6	v
osobě	osoba	k1gFnSc6	osoba
Neila	Neil	k1gMnSc2	Neil
Armstronga	Armstrong	k1gMnSc2	Armstrong
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
masivní	masivní	k2eAgInSc4d1	masivní
rozvoj	rozvoj	k1gInSc4	rozvoj
elektroniky	elektronika	k1gFnSc2	elektronika
<g/>
,	,	kIx,	,
počítačů	počítač	k1gInPc2	počítač
a	a	k8xC	a
digitálních	digitální	k2eAgFnPc2d1	digitální
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
symbolem	symbol	k1gInSc7	symbol
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Silicon	Silicon	kA	Silicon
Valley	Vallea	k1gMnSc2	Vallea
<g/>
,	,	kIx,	,
proslulé	proslulý	k2eAgNnSc4d1	proslulé
kalifornské	kalifornský	k2eAgNnSc4d1	kalifornské
centrum	centrum	k1gNnSc4	centrum
inovací	inovace	k1gFnPc2	inovace
<g/>
,	,	kIx,	,
či	či	k8xC	či
Arpanet	Arpanet	k1gMnSc1	Arpanet
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc1	předchůdce
internetu	internet	k1gInSc2	internet
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
pro	pro	k7c4	pro
americkou	americký	k2eAgFnSc4d1	americká
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
pilířů	pilíř	k1gInPc2	pilíř
internetu	internet	k1gInSc2	internet
–	–	k?	–
vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
Google	Google	k1gNnSc1	Google
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
sociální	sociální	k2eAgFnSc1d1	sociální
síť	síť	k1gFnSc1	síť
Facebook	Facebook	k1gInSc1	Facebook
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
internetová	internetový	k2eAgFnSc1d1	internetová
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
–	–	k?	–
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Pýchou	pýcha	k1gFnSc7	pýcha
americké	americký	k2eAgFnSc2d1	americká
vědy	věda	k1gFnSc2	věda
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
síť	síť	k1gFnSc4	síť
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejprestižnějším	prestižní	k2eAgNnPc3d3	nejprestižnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
podle	podle	k7c2	podle
žebříčků	žebříček	k1gInPc2	žebříček
kvality	kvalita	k1gFnSc2	kvalita
univerzit	univerzita	k1gFnPc2	univerzita
QS	QS	kA	QS
World	World	k1gMnSc1	World
University	universita	k1gFnSc2	universita
Ranking	Ranking	k1gInSc1	Ranking
2019	[number]	k4	2019
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
dvaceti	dvacet	k4xCc7	dvacet
nejlepšími	dobrý	k2eAgFnPc7d3	nejlepší
univerzitami	univerzita	k1gFnPc7	univerzita
světa	svět	k1gInSc2	svět
jedenáct	jedenáct	k4xCc4	jedenáct
amerických	americký	k2eAgInPc2d1	americký
<g/>
:	:	kIx,	:
Massachusettský	massachusettský	k2eAgInSc1d1	massachusettský
technologický	technologický	k2eAgInSc1d1	technologický
institut	institut	k1gInSc1	institut
<g/>
,	,	kIx,	,
Stanfordova	Stanfordův	k2eAgFnSc1d1	Stanfordova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Harvardova	Harvardův	k2eAgFnSc1d1	Harvardova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Kalifornský	kalifornský	k2eAgInSc1d1	kalifornský
technologický	technologický	k2eAgInSc1d1	technologický
institut	institut	k1gInSc1	institut
<g/>
,	,	kIx,	,
Chicagská	chicagský	k2eAgFnSc1d1	Chicagská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Princetonská	Princetonský	k2eAgFnSc1d1	Princetonská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Cornellova	Cornellův	k2eAgFnSc1d1	Cornellova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Yaleova	Yaleův	k2eAgFnSc1d1	Yaleova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Kolumbijská	kolumbijský	k2eAgFnSc1d1	kolumbijská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Pensylvánská	pensylvánský	k2eAgFnSc1d1	Pensylvánská
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
Michiganská	michiganský	k2eAgFnSc1d1	Michiganská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Harvard	Harvard	k1gInSc1	Harvard
drží	držet	k5eAaImIp3nS	držet
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
laureátů	laureát	k1gMnPc2	laureát
Nobelových	Nobelových	k2eAgFnPc2d1	Nobelových
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
náskokem	náskok	k1gInSc7	náskok
před	před	k7c4	před
britskou	britský	k2eAgFnSc4d1	britská
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
rodilým	rodilý	k2eAgMnSc7d1	rodilý
Američanem	Američan	k1gMnSc7	Američan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
vědu	věda	k1gFnSc4	věda
byl	být	k5eAaImAgMnS	být
chemik	chemik	k1gMnSc1	chemik
Theodore	Theodor	k1gMnSc5	Theodor
William	William	k1gInSc4	William
Richards	Richards	k1gInSc1	Richards
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Linus	Linus	k1gInSc1	Linus
Pauling	Pauling	k1gInSc1	Pauling
obdržel	obdržet	k5eAaPmAgInS	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
i	i	k8xC	i
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Bardeen	Bardena	k1gFnPc2	Bardena
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
vědcem	vědec	k1gMnSc7	vědec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
oboru	obor	k1gInSc6	obor
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
za	za	k7c4	za
půlomový	půlomový	k2eAgInSc4d1	půlomový
objev	objev	k1gInSc4	objev
transistoru	transistor	k1gInSc2	transistor
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgInPc2d1	klíčový
objevů	objev	k1gInPc2	objev
fyziky	fyzika	k1gFnSc2	fyzika
20.	[number]	k4	20.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
že	že	k8xS	že
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
má	mít	k5eAaImIp3nS	mít
jak	jak	k6eAd1	jak
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
částicovou	částicový	k2eAgFnSc4d1	částicová
povahu	povaha	k1gFnSc4	povaha
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
kontě	konto	k1gNnSc6	konto
Arthur	Arthura	k1gFnPc2	Arthura
Holly	Holla	k1gFnSc2	Holla
Compton	Compton	k1gInSc1	Compton
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ikonám	ikona	k1gFnPc3	ikona
americké	americký	k2eAgFnSc2d1	americká
vědy	věda	k1gFnSc2	věda
patří	patřit	k5eAaImIp3nS	patřit
též	též	k9	též
vynálezce	vynálezce	k1gMnSc1	vynálezce
cyklotronu	cyklotron	k1gInSc2	cyklotron
Ernest	Ernest	k1gMnSc1	Ernest
Lawrence	Lawrence	k1gFnSc1	Lawrence
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
vakcíny	vakcína	k1gFnSc2	vakcína
proti	proti	k7c3	proti
dětské	dětský	k2eAgFnSc3d1	dětská
obrně	obrna	k1gFnSc3	obrna
Jonas	Jonas	k1gMnSc1	Jonas
Salk	Salk	k1gMnSc1	Salk
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
popularizátor	popularizátor	k1gMnSc1	popularizátor
vědy	věda	k1gFnSc2	věda
Richard	Richard	k1gMnSc1	Richard
Feynman	Feynman	k1gMnSc1	Feynman
<g/>
,	,	kIx,	,
spoluobjevitel	spoluobjevitel	k1gMnSc1	spoluobjevitel
DNA	dno	k1gNnSc2	dno
James	James	k1gMnSc1	James
Dewey	Dewea	k1gFnSc2	Dewea
Watson	Watson	k1gMnSc1	Watson
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
počítačové	počítačový	k2eAgFnSc2d1	počítačová
myši	myš	k1gFnSc2	myš
Douglas	Douglas	k1gMnSc1	Douglas
Engelbart	Engelbart	k1gInSc1	Engelbart
či	či	k8xC	či
astronomové	astronom	k1gMnPc1	astronom
Edwin	Edwin	k1gMnSc1	Edwin
Hubble	Hubble	k1gMnSc1	Hubble
a	a	k8xC	a
Carl	Carl	k1gMnSc1	Carl
Sagan	Sagan	k1gMnSc1	Sagan
<g/>
.	.	kIx.	.
</s>
<s>
Matematik	matematik	k1gMnSc1	matematik
Norbert	Norbert	k1gMnSc1	Norbert
Wiener	Wiener	k1gMnSc1	Wiener
založil	založit	k5eAaPmAgMnS	založit
kybernetiku	kybernetika	k1gFnSc4	kybernetika
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejslavnějším	slavný	k2eAgMnPc3d3	nejslavnější
americkým	americký	k2eAgMnPc3d1	americký
informatikům	informatik	k1gMnPc3	informatik
patří	patřit	k5eAaImIp3nP	patřit
Donald	Donald	k1gMnSc1	Donald
Knuth	Knuth	k1gMnSc1	Knuth
<g/>
,	,	kIx,	,
tvůrci	tvůrce	k1gMnPc1	tvůrce
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
Ken	Ken	k1gMnSc1	Ken
Thompson	Thompson	k1gMnSc1	Thompson
a	a	k8xC	a
Dennis	Dennis	k1gFnSc1	Dennis
Ritchie	Ritchie	k1gFnSc1	Ritchie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
otec	otec	k1gMnSc1	otec
internetu	internet	k1gInSc2	internet
<g/>
"	"	kIx"	"
Vint	vint	k1gInSc1	vint
Cerf	Cerf	k1gInSc1	Cerf
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
komunikační	komunikační	k2eAgInSc4d1	komunikační
protokol	protokol	k1gInSc4	protokol
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
síť	síť	k1gFnSc1	síť
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pilíře	pilíř	k1gInPc1	pilíř
americké	americký	k2eAgFnSc2d1	americká
filozofie	filozofie	k1gFnSc2	filozofie
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věd	věda	k1gFnPc2	věda
stavěli	stavět	k5eAaImAgMnP	stavět
Henry	Henry	k1gMnSc1	Henry
David	David	k1gMnSc1	David
Thoreau	Thoreaus	k1gInSc2	Thoreaus
či	či	k8xC	či
William	William	k1gInSc4	William
James	James	k1gMnSc1	James
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Sanders	Sandersa	k1gFnPc2	Sandersa
Peirce	Peirce	k1gMnSc1	Peirce
a	a	k8xC	a
John	John	k1gMnSc1	John
Dewey	Dewea	k1gFnSc2	Dewea
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
představitelé	představitel	k1gMnPc1	představitel
amerického	americký	k2eAgInSc2d1	americký
pragmatismu	pragmatismus	k1gInSc2	pragmatismus
<g/>
,	,	kIx,	,
prvního	první	k4xOgMnSc2	první
ryze	ryze	k6eAd1	ryze
amerického	americký	k2eAgInSc2d1	americký
filozofického	filozofický	k2eAgInSc2d1	filozofický
směru	směr	k1gInSc2	směr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
hluboce	hluboko	k6eAd1	hluboko
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
celou	celý	k2eAgFnSc4d1	celá
americkou	americký	k2eAgFnSc4d1	americká
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
vlivnými	vlivný	k2eAgMnPc7d1	vlivný
autory	autor	k1gMnPc7	autor
20.	[number]	k4	20.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
psychologové	psycholog	k1gMnPc1	psycholog
Erich	Erich	k1gMnSc1	Erich
Fromm	Fromm	k1gMnSc1	Fromm
<g/>
,	,	kIx,	,
Abraham	Abraham	k1gMnSc1	Abraham
Maslow	Maslow	k1gMnSc1	Maslow
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Rogers	Rogers	k1gInSc1	Rogers
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Watson	Watson	k1gMnSc1	Watson
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
Thorndike	Thorndik	k1gFnSc2	Thorndik
či	či	k8xC	či
nejcitovanější	citovaný	k2eAgMnSc1d3	nejcitovanější
psycholog	psycholog	k1gMnSc1	psycholog
20.	[number]	k4	20.
století	století	k1gNnPc2	století
Burrhus	Burrhus	k1gMnSc1	Burrhus
Frederic	Frederic	k1gMnSc1	Frederic
Skinner	Skinner	k1gMnSc1	Skinner
<g/>
,	,	kIx,	,
sociologové	sociolog	k1gMnPc1	sociolog
Erving	Erving	k1gInSc4	Erving
Goffman	Goffman	k1gMnSc1	Goffman
a	a	k8xC	a
Talcott	Talcott	k2eAgInSc1d1	Talcott
Parsons	Parsons	k1gInSc1	Parsons
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
vědy	věda	k1gFnSc2	věda
Thomas	Thomas	k1gMnSc1	Thomas
Kuhn	Kuhn	k1gMnSc1	Kuhn
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
genderová	genderový	k2eAgFnSc1d1	genderová
teoretička	teoretička	k1gFnSc1	teoretička
Judith	Judith	k1gInSc1	Judith
Butlerová	Butlerová	k1gFnSc1	Butlerová
<g/>
,	,	kIx,	,
političtí	politický	k2eAgMnPc1d1	politický
filozofové	filozof	k1gMnPc1	filozof
John	John	k1gMnSc1	John
Rawls	Rawls	k1gInSc1	Rawls
<g/>
,	,	kIx,	,
Samuel	Samuel	k1gMnSc1	Samuel
Huntington	Huntington	k1gInSc1	Huntington
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
slavného	slavný	k2eAgInSc2d1	slavný
Střetu	střet	k1gInSc2	střet
civilizací	civilizace	k1gFnSc7	civilizace
<g/>
)	)	kIx)	)
a	a	k8xC	a
Francis	Francis	k1gFnSc1	Francis
Fukuyama	Fukuyama	k1gFnSc1	Fukuyama
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
neméně	málo	k6eNd2	málo
slavného	slavný	k2eAgInSc2d1	slavný
eseje	esej	k1gInSc2	esej
Konec	konec	k1gInSc1	konec
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
člověk	člověk	k1gMnSc1	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
jazyka	jazyk	k1gInSc2	jazyk
Hilary	Hilara	k1gFnSc2	Hilara
Putnam	Putnam	k1gInSc1	Putnam
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
sexuologie	sexuologie	k1gFnSc2	sexuologie
Alfred	Alfred	k1gMnSc1	Alfred
Kinsey	Kinsea	k1gFnSc2	Kinsea
<g/>
,	,	kIx,	,
antropologové	antropolog	k1gMnPc1	antropolog
Margaret	Margareta	k1gFnPc2	Margareta
Meadová	Meadová	k1gFnSc1	Meadová
a	a	k8xC	a
Clifford	Clifford	k1gMnSc1	Clifford
Geertz	Geertz	k1gMnSc1	Geertz
či	či	k8xC	či
lingvista	lingvista	k1gMnSc1	lingvista
Noam	Noam	k1gMnSc1	Noam
Chomsky	Chomsky	k1gMnSc1	Chomsky
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
mimořádné	mimořádný	k2eAgNnSc4d1	mimořádné
postavení	postavení	k1gNnSc4	postavení
mají	mít	k5eAaImIp3nP	mít
Američané	Američan	k1gMnPc1	Američan
v	v	k7c6	v
ekonomii	ekonomie	k1gFnSc6	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
udílení	udílení	k1gNnSc2	udílení
Nobelových	Nobelových	k2eAgFnPc2d1	Nobelových
cen	cena	k1gFnPc2	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
jen	jen	k6eAd1	jen
třináct	třináct	k4xCc1	třináct
ročníků	ročník	k1gInPc2	ročník
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
cenu	cena	k1gFnSc4	cena
nepřebíral	přebírat	k5eNaImAgMnS	přebírat
některý	některý	k3yIgMnSc1	některý
z	z	k7c2	z
Američanů	Američan	k1gMnPc2	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
měli	mít	k5eAaImAgMnP	mít
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
laureátů	laureát	k1gMnPc2	laureát
otec	otec	k1gMnSc1	otec
neoliberalismu	neoliberalismus	k1gInSc2	neoliberalismus
Milton	Milton	k1gInSc1	Milton
Friedman	Friedman	k1gMnSc1	Friedman
<g/>
,	,	kIx,	,
klíčoví	klíčový	k2eAgMnPc1d1	klíčový
představitelé	představitel	k1gMnPc1	představitel
neokeynesyánského	okeynesyánský	k2eNgInSc2d1	okeynesyánský
směru	směr	k1gInSc2	směr
Joseph	Joseph	k1gMnSc1	Joseph
Stiglitz	Stiglitz	k1gMnSc1	Stiglitz
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
Krugman	Krugman	k1gMnSc1	Krugman
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
teorie	teorie	k1gFnSc2	teorie
her	hra	k1gFnPc2	hra
John	John	k1gMnSc1	John
Forbes	forbes	k1gInSc4	forbes
Nash	Nash	k1gMnSc1	Nash
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
proslavil	proslavit	k5eAaPmAgMnS	proslavit
film	film	k1gInSc4	film
Čistá	čistý	k2eAgFnSc1d1	čistá
duše	duše	k1gFnSc1	duše
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yQgMnSc1	který
nesl	nést	k5eAaImAgMnS	nést
při	při	k7c6	při
bádání	bádání	k1gNnSc6	bádání
břímě	břímě	k1gNnSc4	břímě
paranoidní	paranoidní	k2eAgFnSc2d1	paranoidní
psychózy	psychóza	k1gFnSc2	psychóza
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Paul	Paul	k1gMnSc1	Paul
Samuelson	Samuelson	k1gMnSc1	Samuelson
<g/>
,	,	kIx,	,
z	z	k7c2	z
jehož	jehož	k3xOyRp3gFnSc2	jehož
učebnice	učebnice	k1gFnSc2	učebnice
se	se	k3xPyFc4	se
již	již	k6eAd1	již
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
učí	učit	k5eAaImIp3nP	učit
tisíce	tisíc	k4xCgInPc1	tisíc
studentů	student	k1gMnPc2	student
ekonomie	ekonomie	k1gFnSc2	ekonomie
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
obohacení	obohacení	k1gNnSc6	obohacení
ekonomie	ekonomie	k1gFnSc2	ekonomie
analýzou	analýza	k1gFnSc7	analýza
netržního	tržní	k2eNgNnSc2d1	netržní
chování	chování	k1gNnSc2	chování
usiloval	usilovat	k5eAaImAgInS	usilovat
Gary	Gar	k2eAgFnPc4d1	Gara
Stanley	Stanlea	k1gFnPc4	Stanlea
Becker	Becker	k1gMnSc1	Becker
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
psychologií	psychologie	k1gFnSc7	psychologie
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
ekonomii	ekonomie	k1gFnSc3	ekonomie
propojit	propojit	k5eAaPmF	propojit
Herbert	Herbert	k1gMnSc1	Herbert
A.	A.	kA	A.
Simon	Simon	k1gMnSc1	Simon
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jediná	jediný	k2eAgFnSc1d1	jediná
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
získala	získat	k5eAaPmAgFnS	získat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Američanka	Američanka	k1gFnSc1	Američanka
<g/>
:	:	kIx,	:
Elinor	Elinor	k1gInSc1	Elinor
Ostromová	Ostromový	k2eAgFnSc1d1	Ostromová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Státní	státní	k2eAgInPc1d1	státní
svátky	svátek	k1gInPc1	svátek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Americký	americký	k2eAgInSc1d1	americký
sen	sen	k1gInSc1	sen
</s>
</p>
<p>
<s>
Slib	slib	k1gInSc1	slib
věrnosti	věrnost	k1gFnSc2	věrnost
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
amerických	americký	k2eAgMnPc2d1	americký
prezidentů	prezident	k1gMnPc2	prezident
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
pečetí	pečeť	k1gFnSc7	pečeť
států	stát	k1gInPc2	stát
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
vlajek	vlajka	k1gFnPc2	vlajka
států	stát	k1gInPc2	stát
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnPc1	kategorie
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Cestovní	cestovní	k2eAgFnSc1d1	cestovní
příručka	příručka	k1gFnSc1	příručka
na	na	k7c6	na
Wikivoyage	Wikivoyage	k1gFnSc6	Wikivoyage
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Infoamerika.cz	Infoamerika.cz	k1gInSc1	Infoamerika.cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
USA	USA	kA	USA
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Americké	americký	k2eAgNnSc1d1	americké
centrum	centrum	k1gNnSc1	centrum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
–	–	k?	–
USA	USA	kA	USA
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Sborník	sborník	k1gInSc1	sborník
z	z	k7c2	z
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
k	k	k7c3	k
volnému	volný	k2eAgNnSc3d1	volné
použití	použití	k1gNnSc3	použití
pod	pod	k7c7	pod
licencí	licence	k1gFnSc7	licence
Creative	Creativ	k1gInSc5	Creativ
Commons	Commons	k1gInSc4	Commons
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
/	/	kIx~	/
Podnikání	podnikání	k1gNnPc1	podnikání
a	a	k8xC	a
firmy	firma	k1gFnPc1	firma
v	v	k7c6	v
USA	USA	kA	USA
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CDC	CDC	kA	CDC
<g/>
.	.	kIx.	.
<g/>
gov	gov	k?	gov
–	–	k?	–
data	datum	k1gNnSc2	datum
k	k	k7c3	k
zdravotnímu	zdravotní	k2eAgNnSc3d1	zdravotní
pojištění	pojištění	k1gNnSc3	pojištění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
HHS	HHS	kA	HHS
<g/>
.	.	kIx.	.
<g/>
gov	gov	k?	gov
–	–	k?	–
průběh	průběh	k1gInSc4	průběh
a	a	k8xC	a
fakta	faktum	k1gNnPc4	faktum
k	k	k7c3	k
reformě	reforma	k1gFnSc3	reforma
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
pojištění	pojištění	k1gNnSc2	pojištění
dle	dle	k7c2	dle
let	léto	k1gNnPc2	léto
<g/>
/	/	kIx~	/
<g/>
CDC	CDC	kA	CDC
data	datum	k1gNnSc2	datum
k	k	k7c3	k
zdravotnímu	zdravotní	k2eAgNnSc3d1	zdravotní
pojištění	pojištění	k1gNnSc3	pojištění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
]	]	kIx)	]
</s>
</p>
<p>
</p>
<p>
<s>
http://www.nytimes.com/2013/02/17/magazine/do-illegal-immigrants-actually-hurt-the-us-economy.html?pagewanted=all&	[url]	k?	http://www.nytimes.com/2013/02/17/magazine/do-illegal-immigrants-actually-hurt-the-us-economy.html?pagewanted=all&
</s>
</p>
<p>
<s>
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
of	of	k?	of
America	America	k1gMnSc1	America
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
of	of	k?	of
America	America	k1gMnSc1	America
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
USA	USA	kA	USA
-	-	kIx~	-
o	o	k7c6	o
českých	český	k2eAgMnPc6d1	český
krajanech	krajan	k1gMnPc6	krajan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
2002-06-20	[number]	k4	2002-06-20
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-12-14	[number]	k4	2011-12-14
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-07-26	[number]	k4	2011-07-26
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo.cz	Businessinfo.cz	k1gMnSc1	Businessinfo.cz
<g/>
,	,	kIx,	,
2011-04-01	[number]	k4	2011-04-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012-01-11	[number]	k4	2012-01-11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BEEMAN	BEEMAN	kA	BEEMAN
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
R	R	kA	R
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
