<s>
Nekřesťanská	křesťanský	k2eNgNnPc1d1	nekřesťanské
náboženství	náboženství	k1gNnPc1	náboženství
představují	představovat	k5eAaImIp3nP	představovat
asi	asi	k9	asi
3,5	[number]	k4	3,5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
USA	USA	kA	USA
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
judaismus	judaismus	k1gInSc4	judaismus
<g/>
(	(	kIx(	(
<g/>
židé	žid	k1gMnPc1	žid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
islám	islám	k1gInSc1	islám
<g/>
,	,	kIx,	,
buddhismus	buddhismus	k1gInSc1	buddhismus
a	a	k8xC	a
hinduismus	hinduismus	k1gInSc1	hinduismus
<g/>
.	.	kIx.	.
</s>
