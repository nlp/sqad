<s>
První	první	k4xOgFnSc1	první
dáma	dáma	k1gFnSc1	dáma
představuje	představovat	k5eAaImIp3nS	představovat
v	v	k7c6	v
republikách	republika	k1gFnPc6	republika
neoficiální	oficiální	k2eNgNnSc1d1	neoficiální
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
manželku	manželka	k1gFnSc4	manželka
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zúčastňuje	zúčastňovat	k5eAaImIp3nS	zúčastňovat
společenských	společenský	k2eAgFnPc2d1	společenská
<g/>
,	,	kIx,	,
politických	politický	k2eAgFnPc2d1	politická
či	či	k8xC	či
diplomatických	diplomatický	k2eAgFnPc2d1	diplomatická
akcí	akce	k1gFnPc2	akce
(	(	kIx(	(
<g/>
jako	jako	k9	jako
například	například	k6eAd1	například
tradičního	tradiční	k2eAgInSc2d1	tradiční
novoročního	novoroční	k2eAgInSc2d1	novoroční
oběda	oběd	k1gInSc2	oběd
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
a	a	k8xC	a
plní	plnit	k5eAaImIp3nS	plnit
reprezentativní	reprezentativní	k2eAgNnSc1d1	reprezentativní
<g/>
,	,	kIx,	,
charitativní	charitativní	k2eAgFnPc1d1	charitativní
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povinnosti	povinnost	k1gFnPc1	povinnost
<g/>
.	.	kIx.	.
</s>
