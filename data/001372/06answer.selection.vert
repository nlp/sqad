<s>
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc1	jméno
podle	podle	k7c2	podle
křestního	křestní	k2eAgInSc2d1	křestní
záznamu	záznam	k1gInSc2	záznam
Joannes	Joannes	k1gMnSc1	Joannes
Chrysostomus	Chrysostomus	k1gMnSc1	Chrysostomus
Wolfgangus	Wolfgangus	k1gMnSc1	Wolfgangus
Theophilus	Theophilus	k1gMnSc1	Theophilus
Mozart	Mozart	k1gMnSc1	Mozart
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1756	[number]	k4	1756
<g/>
,	,	kIx,	,
Salzburg	Salzburg	k1gInSc1	Salzburg
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1791	[number]	k4	1791
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
klasicistní	klasicistní	k2eAgMnSc1d1	klasicistní
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
klavírní	klavírní	k2eAgMnSc1d1	klavírní
virtuos	virtuos	k1gMnSc1	virtuos
<g/>
;	;	kIx,	;
geniální	geniální	k2eAgMnSc1d1	geniální
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
složil	složit	k5eAaPmAgInS	složit
626	[number]	k4	626
děl	dělo	k1gNnPc2	dělo
světského	světský	k2eAgInSc2d1	světský
i	i	k8xC	i
duchovního	duchovní	k2eAgInSc2d1	duchovní
charakteru	charakter	k1gInSc2	charakter
-	-	kIx~	-
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
symfonie	symfonie	k1gFnSc2	symfonie
<g/>
,	,	kIx,	,
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
komorní	komorní	k2eAgFnSc4d1	komorní
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
mše	mše	k1gFnPc4	mše
a	a	k8xC	a
chorály	chorál	k1gInPc4	chorál
<g/>
.	.	kIx.	.
</s>
