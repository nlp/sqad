<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
México	México	k6eAd1	México
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Mexico	Mexico	k6eAd1	Mexico
City	city	k1gNnSc1	city
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
,	,	kIx,	,
vysoko	vysoko	k6eAd1	vysoko
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
Mexická	mexický	k2eAgFnSc1d1	mexická
náhorní	náhorní	k2eAgFnSc1d1	náhorní
plošina	plošina	k1gFnSc1	plošina
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
měst	město	k1gNnPc2	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
31	[number]	k4	31
států	stát	k1gInPc2	stát
je	být	k5eAaImIp3nS	být
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
32	[number]	k4	32
státotvorných	státotvorný	k2eAgFnPc2d1	státotvorná
územně-správních	územněprávní	k2eAgFnPc2d1	územně-správní
jednotek	jednotka	k1gFnPc2	jednotka
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
mexických	mexický	k2eAgInPc2d1	mexický
<g/>
.	.	kIx.	.
</s>
<s>
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
México	México	k6eAd1	México
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
města	město	k1gNnPc4	město
s	s	k7c7	s
největšími	veliký	k2eAgInPc7d3	veliký
sociálními	sociální	k2eAgInPc7d1	sociální
rozdíly	rozdíl	k1gInPc7	rozdíl
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
potýkají	potýkat	k5eAaImIp3nP	potýkat
s	s	k7c7	s
chudobou	chudoba	k1gFnSc7	chudoba
<g/>
,	,	kIx,	,
nezaměstnaností	nezaměstnanost	k1gFnSc7	nezaměstnanost
a	a	k8xC	a
nedostatkem	nedostatek	k1gInSc7	nedostatek
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
chudinské	chudinský	k2eAgFnPc4d1	chudinská
čtvrtě	čtvrt	k1gFnPc4	čtvrt
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Nezahualcóyotl	Nezahualcóyotl	k1gFnSc1	Nezahualcóyotl
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyv	obyv	k1gInSc1	obyv
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
oplývající	oplývající	k2eAgFnSc1d1	oplývající
blahobytem	blahobyt	k1gInSc7	blahobyt
je	být	k5eAaImIp3nS	být
rájem	ráj	k1gInSc7	ráj
boháčů	boháč	k1gMnPc2	boháč
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gInSc7	centr
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
náměstí	náměstí	k1gNnSc4	náměstí
Zócalo	Zócala	k1gFnSc5	Zócala
a	a	k8xC	a
náměstí	náměstí	k1gNnSc1	náměstí
Tří	tři	k4xCgFnPc2	tři
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
figuruje	figurovat	k5eAaImIp3nS	figurovat
historické	historický	k2eAgNnSc4d1	historické
centrum	centrum	k1gNnSc4	centrum
města	město	k1gNnSc2	město
společně	společně	k6eAd1	společně
s	s	k7c7	s
kulturní	kulturní	k2eAgFnSc7d1	kulturní
krajinou	krajina	k1gFnSc7	krajina
Xochimilca	Xochimilc	k1gInSc2	Xochimilc
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
další	další	k2eAgFnPc1d1	další
2	[number]	k4	2
kulturní	kulturní	k2eAgFnPc1d1	kulturní
památky	památka	k1gFnPc1	památka
UNESCO	UNESCO	kA	UNESCO
-	-	kIx~	-
dům	dům	k1gInSc1	dům
a	a	k8xC	a
ateliér	ateliér	k1gInSc1	ateliér
Luise	Luisa	k1gFnSc6	Luisa
Barragána	Barragán	k1gMnSc4	Barragán
a	a	k8xC	a
univerzitní	univerzitní	k2eAgInSc4d1	univerzitní
kampus	kampus	k1gInSc4	kampus
Mexické	mexický	k2eAgFnSc2d1	mexická
národní	národní	k2eAgFnSc2d1	národní
autonomní	autonomní	k2eAgFnSc2d1	autonomní
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
72	[number]	k4	72
km	km	kA	km
východně	východně	k6eAd1	východně
směrem	směr	k1gInSc7	směr
na	na	k7c6	na
Pueblu	Puebl	k1gInSc6	Puebl
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
aktivní	aktivní	k2eAgFnSc1d1	aktivní
sopka	sopka	k1gFnSc1	sopka
Popocatépetl	Popocatépetl	k1gFnSc1	Popocatépetl
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
druhým	druhý	k4xOgInSc7	druhý
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobré	dobrý	k2eAgFnPc4d1	dobrá
viditelnosti	viditelnost	k1gFnPc4	viditelnost
ji	on	k3xPp3gFnSc4	on
můžete	moct	k5eAaImIp2nP	moct
spatřit	spatřit	k5eAaPmF	spatřit
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Kdysi	kdysi	k6eAd1	kdysi
bylo	být	k5eAaImAgNnS	být
Mexico	Mexico	k6eAd1	Mexico
City	city	k1gNnSc1	city
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
novém	nový	k2eAgNnSc6d1	nové
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
překonáno	překonat	k5eAaPmNgNnS	překonat
jinými	jiný	k2eAgFnPc7d1	jiná
světovými	světový	k2eAgNnPc7d1	světové
velkoměsty	velkoměsto	k1gNnPc7	velkoměsto
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
aglomeraci	aglomerace	k1gFnSc6	aglomerace
žije	žít	k5eAaImIp3nS	žít
přes	přes	k7c4	přes
21,2	[number]	k4	21,2
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1521	[number]	k4	1521
na	na	k7c6	na
ruinách	ruina	k1gFnPc6	ruina
bývalého	bývalý	k2eAgNnSc2d1	bývalé
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Aztéků	Azték	k1gMnPc2	Azték
<g/>
,	,	kIx,	,
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
dobyli	dobýt	k5eAaPmAgMnP	dobýt
španělští	španělský	k2eAgMnPc1d1	španělský
conquistadoři	conquistador	k1gMnPc1	conquistador
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Hernánem	Hernán	k1gMnSc7	Hernán
Cortésem	Cortés	k1gMnSc7	Cortés
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1525	[number]	k4	1525
do	do	k7c2	do
vypuknutí	vypuknutí	k1gNnSc2	vypuknutí
války	válka	k1gFnSc2	válka
o	o	k7c4	o
nezávislost	nezávislost	k1gFnSc4	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1810	[number]	k4	1810
bylo	být	k5eAaImAgNnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
městem	město	k1gNnSc7	město
Nového	Nového	k2eAgNnSc2d1	Nového
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
tu	tu	k6eAd1	tu
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
první	první	k4xOgInSc1	první
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
Torre	torr	k1gInSc5	torr
Latinoamericana	Latinoamerican	k1gMnSc2	Latinoamerican
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
poničilo	poničit	k5eAaPmAgNnS	poničit
město	město	k1gNnSc1	město
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pořádaly	pořádat	k5eAaImAgFnP	pořádat
letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
Tří	tři	k4xCgFnPc2	tři
kultur	kultura	k1gFnPc2	kultura
k	k	k7c3	k
masakru	masakr	k1gInSc3	masakr
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
200-300	[number]	k4	200-300
demonstrantů	demonstrant	k1gMnPc2	demonstrant
mexickou	mexický	k2eAgFnSc7d1	mexická
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
Ciudad	Ciudad	k1gInSc4	Ciudad
de	de	k?	de
México	México	k6eAd1	México
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
8,1	[number]	k4	8,1
Richterovy	Richterův	k2eAgFnSc2d1	Richterova
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
;	;	kIx,	;
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
si	se	k3xPyFc3	se
5	[number]	k4	5
000	[number]	k4	000
až	až	k9	až
20	[number]	k4	20
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
a	a	k8xC	a
1986	[number]	k4	1986
zde	zde	k6eAd1	zde
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
sčítání	sčítání	k1gNnSc2	sčítání
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c4	na
území	území	k1gNnSc4	území
města	město	k1gNnSc2	město
8	[number]	k4	8
851	[number]	k4	851
080	[number]	k4	080
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Souvislá	souvislý	k2eAgFnSc1d1	souvislá
městská	městský	k2eAgFnSc1d1	městská
zástavba	zástavba	k1gFnSc1	zástavba
ale	ale	k8xC	ale
fyzicky	fyzicky	k6eAd1	fyzicky
překračuje	překračovat	k5eAaImIp3nS	překračovat
administrativní	administrativní	k2eAgFnSc1d1	administrativní
hranice	hranice	k1gFnSc1	hranice
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
México	México	k6eAd1	México
a	a	k8xC	a
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
sousedního	sousední	k2eAgInSc2d1	sousední
státu	stát	k1gInSc2	stát
México	México	k1gMnSc1	México
<g/>
.	.	kIx.	.
</s>
<s>
Mexický	mexický	k2eAgInSc1d1	mexický
statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
potřeby	potřeba	k1gFnPc4	potřeba
definoval	definovat	k5eAaBmAgMnS	definovat
tzv.	tzv.	kA	tzv.
Metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
Valle	Valle	k1gFnSc2	Valle
de	de	k?	de
México	México	k1gMnSc1	México
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
kromě	kromě	k7c2	kromě
samotného	samotný	k2eAgMnSc2d1	samotný
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
México	México	k6eAd1	México
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
60	[number]	k4	60
okolních	okolní	k2eAgInPc2d1	okolní
urbanistických	urbanistický	k2eAgInPc2d1	urbanistický
celků	celek	k1gInPc2	celek
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
México	México	k1gMnSc1	México
a	a	k8xC	a
Hidalgo	Hidalgo	k1gMnSc1	Hidalgo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
žilo	žít	k5eAaImAgNnS	žít
20	[number]	k4	20
137	[number]	k4	137
152	[number]	k4	152
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
México	México	k6eAd1	México
jako	jako	k8xS	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
měst	město	k1gNnPc2	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
trpí	trpět	k5eAaImIp3nS	trpět
stejnými	stejný	k2eAgInPc7d1	stejný
problémy	problém	k1gInPc7	problém
jako	jako	k8xC	jako
jiná	jiný	k2eAgNnPc4d1	jiné
světová	světový	k2eAgNnPc4d1	světové
velkoměsta	velkoměsto	k1gNnPc4	velkoměsto
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
kvůli	kvůli	k7c3	kvůli
nižší	nízký	k2eAgFnSc6d2	nižší
životní	životní	k2eAgFnSc6d1	životní
úrovni	úroveň	k1gFnSc6	úroveň
jeho	jeho	k3xOp3gMnPc2	jeho
obyvatel	obyvatel	k1gMnPc2	obyvatel
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
zvětšenými	zvětšený	k2eAgFnPc7d1	zvětšená
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
znečištění	znečištění	k1gNnSc1	znečištění
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc4	nedostatek
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
chudoba	chudoba	k1gFnSc1	chudoba
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
růst	růst	k1gInSc1	růst
předměstských	předměstský	k2eAgFnPc2d1	předměstská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
se	s	k7c7	s
slumy	slum	k1gInPc7	slum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgFnSc1d1	dopravní
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
poddimenzovaná	poddimenzovaný	k2eAgFnSc1d1	poddimenzovaná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Ciudad	Ciudad	k1gInSc4	Ciudad
de	de	k?	de
México	México	k6eAd1	México
žije	žít	k5eAaImIp3nS	žít
mnoho	mnoho	k4c1	mnoho
dětí	dítě	k1gFnPc2	dítě
na	na	k7c6	na
ulicích	ulice	k1gFnPc6	ulice
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tisíce	tisíc	k4xCgInPc4	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
velkých	velký	k2eAgInPc2d1	velký
problémů	problém	k1gInPc2	problém
je	být	k5eAaImIp3nS	být
také	také	k9	také
kriminalita	kriminalita	k1gFnSc1	kriminalita
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
bylo	být	k5eAaImAgNnS	být
právě	právě	k6eAd1	právě
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
uneseno	unést	k5eAaPmNgNnS	unést
zhruba	zhruba	k6eAd1	zhruba
3	[number]	k4	3
000	[number]	k4	000
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
městu	město	k1gNnSc3	město
přineslo	přinést	k5eAaPmAgNnS	přinést
ve	v	k7c6	v
světovém	světový	k2eAgNnSc6d1	světové
srovnání	srovnání	k1gNnSc6	srovnání
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
i	i	k9	i
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
případům	případ	k1gInPc3	případ
únosů	únos	k1gInPc2	únos
černými	černý	k2eAgFnPc7d1	černá
taxikáři	taxikář	k1gMnPc1	taxikář
<g/>
,	,	kIx,	,
v	v	k7c6	v
metru	metro	k1gNnSc6	metro
i	i	k8xC	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
dopravních	dopravní	k2eAgInPc6d1	dopravní
prostředcích	prostředek	k1gInPc6	prostředek
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
kapsářům	kapsář	k1gInPc3	kapsář
<g/>
.	.	kIx.	.
</s>
<s>
Městskou	městský	k2eAgFnSc4d1	městská
dopravu	doprava	k1gFnSc4	doprava
zajišťujií	zajišťujium	k1gNnPc2	zajišťujium
autobusy	autobus	k1gInPc7	autobus
<g/>
,	,	kIx,	,
trolejbusy	trolejbus	k1gInPc1	trolejbus
a	a	k8xC	a
metro	metro	k1gNnSc1	metro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
města	město	k1gNnSc2	město
a	a	k8xC	a
blízkého	blízký	k2eAgNnSc2d1	blízké
okolí	okolí	k1gNnSc2	okolí
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
osobní	osobní	k2eAgFnSc4d1	osobní
železniční	železniční	k2eAgFnSc4d1	železniční
přepravu	přeprava	k1gFnSc4	přeprava
příměstské	příměstský	k2eAgInPc1d1	příměstský
vlaky	vlak	k1gInPc1	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Meziměstská	meziměstský	k2eAgFnSc1d1	meziměstská
přeprava	přeprava	k1gFnSc1	přeprava
osob	osoba	k1gFnPc2	osoba
po	po	k7c6	po
železnici	železnice	k1gFnSc6	železnice
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
privatizaci	privatizace	k1gFnSc6	privatizace
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
státě	stát	k1gInSc6	stát
prakticky	prakticky	k6eAd1	prakticky
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
se	se	k3xPyFc4	se
již	již	k6eAd1	již
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
jejím	její	k3xOp3gNnSc7	její
obnovením	obnovení	k1gNnSc7	obnovení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
letiště	letiště	k1gNnSc1	letiště
Beníta	Benít	k1gInSc2	Benít
Juaréze	Juaréza	k1gFnSc3	Juaréza
a	a	k8xC	a
železnici	železnice	k1gFnSc6	železnice
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
meziměstské	meziměstský	k2eAgInPc4d1	meziměstský
autobusy	autobus	k1gInPc4	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
Chicago	Chicago	k1gNnSc4	Chicago
<g/>
,	,	kIx,	,
USA	USA	kA	USA
Cuzco	Cuzco	k1gNnSc1	Cuzco
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
Dolores	Dolores	k1gMnSc1	Dolores
Hidalgo	Hidalgo	k1gMnSc1	Hidalgo
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
Havana	havana	k1gNnSc2	havana
<g/>
,	,	kIx,	,
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc4	Angeles
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
Malmö	Malmö	k1gFnPc2	Malmö
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Nagoja	Nagojum	k1gNnSc2	Nagojum
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
San	San	k1gMnSc1	San
Salvador	Salvador	k1gMnSc1	Salvador
<g/>
,	,	kIx,	,
Salvador	Salvador	k1gMnSc1	Salvador
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
Soul	Soul	k1gInSc1	Soul
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
</s>
