<s>
Etika	etika	k1gFnSc1	etika
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
ethos	ethos	k1gNnSc2	ethos
–	–	k?	–
mrav	mrav	k1gInSc1	mrav
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
též	též	k9	též
teorie	teorie	k1gFnSc1	teorie
morálky	morálka	k1gFnSc2	morálka
je	být	k5eAaImIp3nS	být
filozofickou	filozofický	k2eAgFnSc7d1	filozofická
disciplínou	disciplína	k1gFnSc7	disciplína
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
zkoumáním	zkoumání	k1gNnSc7	zkoumání
mravní	mravní	k2eAgFnSc2d1	mravní
dimenze	dimenze	k1gFnSc2	dimenze
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Etika	etika	k1gFnSc1	etika
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
morálku	morálka	k1gFnSc4	morálka
nebo	nebo	k8xC	nebo
morálně	morálně	k6eAd1	morálně
relevantní	relevantní	k2eAgNnSc4d1	relevantní
jednání	jednání	k1gNnSc4	jednání
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
normy	norma	k1gFnPc4	norma
<g/>
.	.	kIx.	.
</s>
<s>
Etika	etika	k1gFnSc1	etika
je	být	k5eAaImIp3nS	být
disciplínou	disciplína	k1gFnSc7	disciplína
praktické	praktický	k2eAgFnSc2d1	praktická
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Etika	etika	k1gFnSc1	etika
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
teoretickým	teoretický	k2eAgNnSc7d1	teoretické
zkoumáním	zkoumání	k1gNnSc7	zkoumání
hodnot	hodnota	k1gFnPc2	hodnota
a	a	k8xC	a
principů	princip	k1gInPc2	princip
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
usměrňují	usměrňovat	k5eAaImIp3nP	usměrňovat
lidské	lidský	k2eAgNnSc4d1	lidské
jednání	jednání	k1gNnSc4	jednání
v	v	k7c6	v
situacích	situace	k1gFnPc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
existuje	existovat	k5eAaImIp3nS	existovat
možnost	možnost	k1gFnSc4	možnost
volby	volba	k1gFnSc2	volba
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
svobodné	svobodný	k2eAgFnSc2d1	svobodná
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
činnost	činnost	k1gFnSc4	činnost
člověka	člověk	k1gMnSc2	člověk
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
dobra	dobro	k1gNnSc2	dobro
a	a	k8xC	a
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
morálky	morálka	k1gFnSc2	morálka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
blíže	blízce	k6eAd2	blízce
konkrétním	konkrétní	k2eAgNnPc3d1	konkrétní
pravidlům	pravidlo	k1gNnPc3	pravidlo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
etika	etika	k1gFnSc1	etika
snaží	snažit	k5eAaImIp3nS	snažit
najít	najít	k5eAaPmF	najít
společné	společný	k2eAgInPc4d1	společný
a	a	k8xC	a
obecné	obecný	k2eAgInPc4d1	obecný
základy	základ	k1gInPc4	základ
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgFnPc6	jenž
morálka	morálka	k1gFnSc1	morálka
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
usiluje	usilovat	k5eAaImIp3nS	usilovat
morálku	morálka	k1gFnSc4	morálka
zdůvodnit	zdůvodnit	k5eAaPmF	zdůvodnit
<g/>
.	.	kIx.	.
</s>
<s>
Etika	etika	k1gFnSc1	etika
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
disciplíny	disciplína	k1gFnPc4	disciplína
<g/>
:	:	kIx,	:
e.	e.	k?	e.
analytická	analytický	k2eAgFnSc1d1	analytická
<g/>
,	,	kIx,	,
e.	e.	k?	e.
autonomní	autonomní	k2eAgInSc1d1	autonomní
<g/>
,	,	kIx,	,
e.	e.	k?	e.
deontologická	deontologický	k2eAgFnSc1d1	deontologická
<g/>
,	,	kIx,	,
e.	e.	k?	e.
environmentální	environmentální	k2eAgInSc1d1	environmentální
<g/>
,	,	kIx,	,
e.	e.	k?	e.
evoluční	evoluční	k2eAgInSc1d1	evoluční
<g/>
,	,	kIx,	,
e.	e.	k?	e.
feministická	feministický	k2eAgFnSc1d1	feministická
<g/>
,	,	kIx,	,
e.	e.	k?	e.
heteronomní	heteronomní	k2eAgInSc1d1	heteronomní
<g/>
,	,	kIx,	,
e.	e.	k?	e.
individuální	individuální	k2eAgFnSc1d1	individuální
<g/>
,	,	kIx,	,
žurnalistická	žurnalistický	k2eAgFnSc1d1	žurnalistická
etika	etika	k1gFnSc1	etika
<g/>
,	,	kIx,	,
bioetika	bioetika	k1gFnSc1	bioetika
atd.	atd.	kA	atd.
Deskriptivní	deskriptivní	k2eAgFnSc1d1	deskriptivní
etika	etika	k1gFnSc1	etika
popisuje	popisovat	k5eAaImIp3nS	popisovat
mravní	mravní	k2eAgFnPc4d1	mravní
hodnoty	hodnota	k1gFnPc4	hodnota
a	a	k8xC	a
soudy	soud	k1gInPc4	soud
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
platí	platit	k5eAaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Neříká	říkat	k5eNaImIp3nS	říkat
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
či	či	k8xC	či
ono	onen	k3xDgNnSc1	onen
dobré	dobrý	k2eAgNnSc1d1	dobré
nebo	nebo	k8xC	nebo
špatné	špatný	k2eAgNnSc1d1	špatné
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc4	jaký
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc4	stav
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
překrývá	překrývat	k5eAaImIp3nS	překrývat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sociologií	sociologie	k1gFnSc7	sociologie
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc7d1	sociální
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc7d1	kulturní
antropologií	antropologie	k1gFnSc7	antropologie
<g/>
,	,	kIx,	,
religionistikou	religionistika	k1gFnSc7	religionistika
<g/>
,	,	kIx,	,
etnologií	etnologie	k1gFnSc7	etnologie
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
společenskými	společenský	k2eAgFnPc7d1	společenská
vědami	věda	k1gFnPc7	věda
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Metaetika	Metaetika	k1gFnSc1	Metaetika
<g/>
.	.	kIx.	.
</s>
<s>
Metaetika	Metaetika	k1gFnSc1	Metaetika
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
pronášíme	pronášet	k5eAaImIp1nP	pronášet
etické	etický	k2eAgInPc4d1	etický
výroky	výrok	k1gInPc4	výrok
a	a	k8xC	a
morální	morální	k2eAgInPc4d1	morální
soudy	soud	k1gInPc4	soud
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
analytickou	analytický	k2eAgFnSc7d1	analytická
filosofií	filosofie	k1gFnSc7	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
otázkami	otázka	k1gFnPc7	otázka
povinností	povinnost	k1gFnSc7	povinnost
(	(	kIx(	(
<g/>
deontologické	deontologický	k2eAgFnPc4d1	deontologická
otázky	otázka	k1gFnPc4	otázka
–	–	k?	–
co	co	k9	co
by	by	kYmCp3nS	by
člověk	člověk	k1gMnSc1	člověk
dělat	dělat	k5eAaImF	dělat
měl	mít	k5eAaImAgInS	mít
<g/>
)	)	kIx)	)
a	a	k8xC	a
otázkami	otázka	k1gFnPc7	otázka
hodnot	hodnota	k1gFnPc2	hodnota
(	(	kIx(	(
<g/>
axiologické	axiologický	k2eAgFnSc2d1	axiologická
otázky	otázka	k1gFnSc2	otázka
–	–	k?	–
co	co	k3yRnSc1	co
utváří	utvářit	k5eAaPmIp3nS	utvářit
dobrý	dobrý	k2eAgInSc1d1	dobrý
život	život	k1gInSc1	život
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
etiky	etika	k1gFnSc2	etika
deskriptivní	deskriptivní	k2eAgFnSc2d1	deskriptivní
hledá	hledat	k5eAaImIp3nS	hledat
odpovědi	odpověď	k1gFnPc1	odpověď
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
něco	něco	k3yInSc4	něco
morálně	morálně	k6eAd1	morálně
správné	správný	k2eAgNnSc1d1	správné
či	či	k8xC	či
nikoli	nikoli	k9	nikoli
<g/>
.	.	kIx.	.
</s>
<s>
Vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
se	se	k3xPyFc4	se
ke	k	k7c3	k
konkrétním	konkrétní	k2eAgFnPc3d1	konkrétní
<g/>
,	,	kIx,	,
praktickým	praktický	k2eAgFnPc3d1	praktická
otázkám	otázka	k1gFnPc3	otázka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
etických	etický	k2eAgFnPc2d1	etická
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
bioetika	bioetika	k1gFnSc1	bioetika
nebo	nebo	k8xC	nebo
žurnalistická	žurnalistický	k2eAgFnSc1d1	žurnalistická
etika	etika	k1gFnSc1	etika
<g/>
.	.	kIx.	.
</s>
<s>
Vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
autonomie	autonomie	k1gFnSc2	autonomie
svědomí	svědomí	k1gNnSc2	svědomí
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
jako	jako	k8xS	jako
základní	základní	k2eAgFnSc2d1	základní
mravní	mravní	k2eAgFnSc2d1	mravní
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Etika	etika	k1gFnSc1	etika
autenticity	autenticita	k1gFnSc2	autenticita
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
morální	morální	k2eAgNnSc1d1	morální
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
čistého	čistý	k2eAgNnSc2d1	čisté
upřímného	upřímný	k2eAgNnSc2d1	upřímné
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
můžeme	moct	k5eAaImIp1nP	moct
zařadit	zařadit	k5eAaPmF	zařadit
etiku	etika	k1gFnSc4	etika
existencialismu	existencialismus	k1gInSc2	existencialismus
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
příkazem	příkaz	k1gInSc7	příkaz
mravním	mravní	k2eAgInSc7d1	mravní
je	být	k5eAaImIp3nS	být
jednat	jednat	k5eAaImF	jednat
svobodně	svobodně	k6eAd1	svobodně
a	a	k8xC	a
autenticky	autenticky	k6eAd1	autenticky
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vše	všechen	k3xTgNnSc4	všechen
jsme	být	k5eAaImIp1nP	být
ale	ale	k8xC	ale
také	také	k9	také
absolutně	absolutně	k6eAd1	absolutně
zodpovědní	zodpovědný	k2eAgMnPc1d1	zodpovědný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
etice	etika	k1gFnSc3	etika
autonomie	autonomie	k1gFnSc2	autonomie
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
etika	etika	k1gFnSc1	etika
Kantova	Kantův	k2eAgFnSc1d1	Kantova
<g/>
.	.	kIx.	.
</s>
<s>
Autonomie	autonomie	k1gFnSc1	autonomie
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mravní	mravní	k2eAgFnSc1d1	mravní
autorita	autorita	k1gFnSc1	autorita
není	být	k5eNaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
zvnějšku	zvnějšku	k6eAd1	zvnějšku
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Kanta	Kant	k1gMnSc2	Kant
je	být	k5eAaImIp3nS	být
touto	tento	k3xDgFnSc7	tento
sebeurčující	sebeurčující	k2eAgFnSc7d1	sebeurčující
autoritou	autorita	k1gFnSc7	autorita
praktický	praktický	k2eAgInSc4d1	praktický
rozum	rozum	k1gInSc4	rozum
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
formuluje	formulovat	k5eAaImIp3nS	formulovat
mravní	mravní	k2eAgInPc4d1	mravní
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
platící	platící	k2eAgNnPc4d1	platící
nepodmíněně	podmíněně	k6eNd1	podmíněně
<g/>
.	.	kIx.	.
</s>
<s>
Takovým	takový	k3xDgInSc7	takový
zákonem	zákon	k1gInSc7	zákon
je	být	k5eAaImIp3nS	být
kategorický	kategorický	k2eAgInSc1d1	kategorický
imperativ	imperativ	k1gInSc1	imperativ
<g/>
.	.	kIx.	.
</s>
<s>
Jednej	jednat	k5eAaImRp2nS	jednat
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zákony	zákon	k1gInPc1	zákon
tvého	tvůj	k3xOp2gNnSc2	tvůj
jednání	jednání	k1gNnSc2	jednání
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgInP	moct
stát	stát	k5eAaPmF	stát
principem	princip	k1gInSc7	princip
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
zákonodárství	zákonodárství	k1gNnSc2	zákonodárství
<g/>
.	.	kIx.	.
</s>
<s>
Kategorický	kategorický	k2eAgInSc4d1	kategorický
imperativ	imperativ	k1gInSc4	imperativ
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgMnS	dát
sám	sám	k3xTgMnSc1	sám
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
ho	on	k3xPp3gInSc4	on
dokazovat	dokazovat	k5eAaImF	dokazovat
<g/>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c4	o
mravní	mravní	k2eAgFnSc4d1	mravní
apriori	apriori	k6eAd1	apriori
<g/>
.	.	kIx.	.
</s>
<s>
Mravní	mravní	k2eAgInSc1d1	mravní
zákon	zákon	k1gInSc1	zákon
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
také	také	k9	také
jako	jako	k9	jako
povinnost	povinnost	k1gFnSc1	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
podle	podle	k7c2	podle
Kanta	Kant	k1gMnSc2	Kant
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
morálně	morálně	k6eAd1	morálně
dobrém	dobrý	k2eAgNnSc6d1	dobré
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
vždy	vždy	k6eAd1	vždy
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
povinností	povinnost	k1gFnSc7	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
sklony	sklona	k1gFnPc4	sklona
a	a	k8xC	a
smyslové	smyslový	k2eAgInPc4d1	smyslový
impulsy	impuls	k1gInPc4	impuls
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
<g/>
-li	i	k?	-li
proti	proti	k7c3	proti
povinnosti	povinnost	k1gFnSc3	povinnost
<g/>
,	,	kIx,	,
zapříčiňují	zapříčiňovat	k5eAaImIp3nP	zapříčiňovat
jednání	jednání	k1gNnPc1	jednání
nemorální	morální	k2eNgNnPc1d1	nemorální
<g/>
.	.	kIx.	.
</s>
<s>
Autonomní	autonomní	k2eAgFnPc1d1	autonomní
etiky	etika	k1gFnPc1	etika
se	se	k3xPyFc4	se
soustředily	soustředit	k5eAaPmAgFnP	soustředit
na	na	k7c4	na
jednajícího	jednající	k2eAgMnSc4d1	jednající
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc4	jeho
rozum	rozum	k1gInSc4	rozum
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
diskuzi	diskuze	k1gFnSc6	diskuze
se	se	k3xPyFc4	se
otevírají	otevírat	k5eAaImIp3nP	otevírat
otázky	otázka	k1gFnPc1	otázka
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
tento	tento	k3xDgInSc4	tento
rozum	rozum	k1gInSc4	rozum
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
orientuje	orientovat	k5eAaBmIp3nS	orientovat
<g/>
.	.	kIx.	.
</s>
<s>
Uvědomujeme	uvědomovat	k5eAaImIp1nP	uvědomovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
naplnění	naplnění	k1gNnSc3	naplnění
života	život	k1gInSc2	život
nestačí	stačit	k5eNaBmIp3nS	stačit
pouze	pouze	k6eAd1	pouze
naše	náš	k3xOp1gFnSc1	náš
autonomie	autonomie	k1gFnSc1	autonomie
a	a	k8xC	a
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
nejranějšího	raný	k2eAgNnSc2d3	nejranější
dětství	dětství	k1gNnSc2	dětství
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
tu	tu	k6eAd1	tu
díky	díky	k7c3	díky
druhým	druhý	k4xOgNnSc7	druhý
a	a	k8xC	a
naše	náš	k3xOp1gFnSc1	náš
svoboda	svoboda	k1gFnSc1	svoboda
má	mít	k5eAaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
ti	ten	k3xDgMnPc1	ten
druzí	druhý	k4xOgMnPc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Židovský	židovský	k2eAgMnSc1d1	židovský
filozof	filozof	k1gMnSc1	filozof
Martin	Martin	k1gMnSc1	Martin
Buber	Buber	k1gMnSc1	Buber
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
naše	náš	k3xOp1gFnSc1	náš
"	"	kIx"	"
<g/>
já	já	k3xPp1nSc1	já
<g/>
"	"	kIx"	"
zrodilo	zrodit	k5eAaPmAgNnS	zrodit
a	a	k8xC	a
stále	stále	k6eAd1	stále
dotváří	dotvářet	k5eAaImIp3nS	dotvářet
díky	díky	k7c3	díky
"	"	kIx"	"
<g/>
ty	ty	k3xPp2nSc1	ty
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tomu	ten	k3xDgMnSc3	ten
druhému	druhý	k4xOgMnSc3	druhý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nás	my	k3xPp1nPc4	my
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
druhý	druhý	k4xOgMnSc1	druhý
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
konkrétní	konkrétní	k2eAgMnSc1d1	konkrétní
člověk	člověk	k1gMnSc1	člověk
nebo	nebo	k8xC	nebo
Bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
etiku	etika	k1gFnSc4	etika
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
se	se	k3xPyFc4	se
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
mravní	mravní	k2eAgFnSc7d1	mravní
hodnotou	hodnota	k1gFnSc7	hodnota
stává	stávat	k5eAaImIp3nS	stávat
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
věrnosti	věrnost	k1gFnSc2	věrnost
svému	svůj	k3xOyFgNnSc3	svůj
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
či	či	k8xC	či
rozumu	rozum	k1gInSc3	rozum
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
věrnost	věrnost	k1gFnSc1	věrnost
tomu	ten	k3xDgNnSc3	ten
druhému	druhý	k4xOgInSc3	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Etikou	etika	k1gFnSc7	etika
toho	ten	k3xDgNnSc2	ten
druhého	druhý	k4xOgMnSc4	druhý
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
zejména	zejména	k9	zejména
Emmanuel	Emmanuel	k1gMnSc1	Emmanuel
Lévinas	Lévinas	k1gMnSc1	Lévinas
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgInSc1	ten
druhý	druhý	k4xOgInSc1	druhý
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
moje	můj	k3xOp1gFnSc1	můj
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Svobodný	svobodný	k2eAgMnSc1d1	svobodný
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
zaslíben	zaslíben	k2eAgMnSc1d1	zaslíben
druhému	druhý	k4xOgInSc3	druhý
<g/>
,	,	kIx,	,
je	on	k3xPp3gInPc4	on
za	za	k7c4	za
něj	on	k3xPp3gInSc4	on
zodpovědný	zodpovědný	k2eAgInSc4d1	zodpovědný
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
ho	on	k3xPp3gMnSc4	on
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
o	o	k7c4	o
druhého	druhý	k4xOgMnSc4	druhý
staral	starat	k5eAaImAgMnS	starat
dřív	dříve	k6eAd2	dříve
než	než	k8xS	než
o	o	k7c4	o
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
druhého	druhý	k4xOgMnSc4	druhý
v	v	k7c6	v
bezmoci	bezmoc	k1gFnSc6	bezmoc
nelze	lze	k6eNd1	lze
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
je	on	k3xPp3gMnPc4	on
nesouměrný	souměrný	k2eNgInSc1d1	nesouměrný
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vůbec	vůbec	k9	vůbec
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
pomoc	pomoc	k1gFnSc1	pomoc
opětována	opětován	k2eAgFnSc1d1	opětována
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnSc1d1	tradiční
etika	etika	k1gFnSc1	etika
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
zaujata	zaujat	k2eAgFnSc1d1	zaujata
člověkem	člověk	k1gMnSc7	člověk
a	a	k8xC	a
lidskými	lidský	k2eAgInPc7d1	lidský
vztahy	vztah	k1gInPc7	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Opomíjí	opomíjet	k5eAaImIp3nS	opomíjet
vztahy	vztah	k1gInPc4	vztah
k	k	k7c3	k
přírodě	příroda	k1gFnSc3	příroda
a	a	k8xC	a
budoucnosti	budoucnost	k1gFnSc3	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
technika	technika	k1gFnSc1	technika
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
utvořit	utvořit	k5eAaPmF	utvořit
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
jde	jít	k5eAaImIp3nS	jít
strach	strach	k1gInSc1	strach
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
strach	strach	k1gInSc1	strach
se	se	k3xPyFc4	se
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaPmF	stát
pozitivní	pozitivní	k2eAgFnSc7d1	pozitivní
etickou	etický	k2eAgFnSc7d1	etická
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
,	,	kIx,	,
když	když	k8xS	když
probudí	probudit	k5eAaPmIp3nS	probudit
naši	náš	k3xOp1gFnSc4	náš
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
za	za	k7c4	za
budoucí	budoucí	k2eAgFnPc4d1	budoucí
generace	generace	k1gFnPc4	generace
<g/>
.	.	kIx.	.
</s>
<s>
Modelem	model	k1gInSc7	model
veškeré	veškerý	k3xTgFnSc2	veškerý
opravdové	opravdový	k2eAgFnSc2d1	opravdová
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
je	být	k5eAaImIp3nS	být
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
rodičovská	rodičovský	k2eAgFnSc1d1	rodičovská
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
být	být	k5eAaImF	být
vnímavý	vnímavý	k2eAgMnSc1d1	vnímavý
a	a	k8xC	a
zbaběle	zbaběle	k6eAd1	zbaběle
neunikat	unikat	k5eNaImF	unikat
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
etiky	etika	k1gFnSc2	etika
podle	podle	k7c2	podle
tohoto	tento	k3xDgMnSc2	tento
pojetí	pojetí	k1gNnPc2	pojetí
neříká	říkat	k5eNaImIp3nS	říkat
nic	nic	k3yNnSc1	nic
objektivního	objektivní	k2eAgNnSc2d1	objektivní
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
osobní	osobní	k2eAgFnSc1d1	osobní
preference	preference	k1gFnSc1	preference
mluvčího	mluvčí	k1gMnSc2	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
etika	etika	k1gFnSc1	etika
neměla	mít	k5eNaImAgFnS	mít
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
pojetí	pojetí	k1gNnSc6	pojetí
nelze	lze	k6eNd1	lze
srovnávat	srovnávat	k5eAaImF	srovnávat
dvě	dva	k4xCgFnPc4	dva
situace	situace	k1gFnPc4	situace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
žádné	žádný	k3yNgFnPc1	žádný
dvě	dva	k4xCgFnPc1	dva
situace	situace	k1gFnPc1	situace
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
absolutní	absolutní	k2eAgInSc4d1	absolutní
základ	základ	k1gInSc4	základ
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgNnSc3	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
různé	různý	k2eAgFnPc1d1	různá
situace	situace	k1gFnPc1	situace
mohly	moct	k5eAaImAgFnP	moct
vztahovat	vztahovat	k5eAaImF	vztahovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
vše	všechen	k3xTgNnSc1	všechen
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
relativní	relativní	k2eAgFnSc1d1	relativní
<g/>
.	.	kIx.	.
</s>
<s>
Snaha	snaha	k1gFnSc1	snaha
vytvořit	vytvořit	k5eAaPmF	vytvořit
myšlenkový	myšlenkový	k2eAgInSc4d1	myšlenkový
rámec	rámec	k1gInSc4	rámec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
něco	něco	k3yInSc1	něco
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
správné	správný	k2eAgNnSc1d1	správné
a	a	k8xC	a
něco	něco	k3yInSc4	něco
vždy	vždy	k6eAd1	vždy
nesprávné	správný	k2eNgNnSc1d1	nesprávné
<g/>
.	.	kIx.	.
</s>
<s>
Nemusí	muset	k5eNaImIp3nP	muset
to	ten	k3xDgNnSc4	ten
nutně	nutně	k6eAd1	nutně
znamenat	znamenat	k5eAaImF	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
neměnná	neměnný	k2eAgNnPc1d1	neměnné
<g/>
,	,	kIx,	,
pevně	pevně	k6eAd1	pevně
daná	daný	k2eAgNnPc4d1	dané
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
,	,	kIx,	,
toliko	toliko	k6eAd1	toliko
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mravní	mravní	k2eAgNnSc1d1	mravní
chování	chování	k1gNnSc1	chování
lze	lze	k6eAd1	lze
vztahovat	vztahovat	k5eAaImF	vztahovat
k	k	k7c3	k
univerzálně	univerzálně	k6eAd1	univerzálně
použitelným	použitelný	k2eAgInPc3d1	použitelný
principům	princip	k1gInPc3	princip
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Antice	antika	k1gFnSc6	antika
se	se	k3xPyFc4	se
etikou	etika	k1gFnSc7	etika
zabývalo	zabývat	k5eAaImAgNnS	zabývat
několik	několik	k4yIc1	několik
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
dochovaly	dochovat	k5eAaPmAgInP	dochovat
texty	text	k1gInPc1	text
<g/>
,	,	kIx,	,
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
však	však	k9	však
nikoliv	nikoliv	k9	nikoliv
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Hésiodos	Hésiodos	k1gMnSc1	Hésiodos
<g/>
,	,	kIx,	,
Epicharmos	Epicharmos	k1gMnSc1	Epicharmos
<g/>
,	,	kIx,	,
Theognis	Theognis	k1gFnSc1	Theognis
<g/>
,	,	kIx,	,
Sókratés	Sókratés	k1gInSc1	Sókratés
<g/>
,	,	kIx,	,
Platón	platón	k1gInSc1	platón
<g/>
,	,	kIx,	,
Xenofón	Xenofón	k1gInSc1	Xenofón
<g/>
,	,	kIx,	,
Antisthenés	Antisthenés	k1gInSc1	Antisthenés
<g/>
,	,	kIx,	,
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
<g/>
.	.	kIx.	.
</s>
<s>
Antická	antický	k2eAgFnSc1d1	antická
etika	etika	k1gFnSc1	etika
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
otázky	otázka	k1gFnPc4	otázka
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
a	a	k8xC	a
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
špatné	špatný	k2eAgNnSc1d1	špatné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
řešila	řešit	k5eAaImAgFnS	řešit
teorie	teorie	k1gFnSc1	teorie
způsobu	způsob	k1gInSc2	způsob
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Sókratés	Sókratés	k6eAd1	Sókratés
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
vede	vést	k5eAaImIp3nS	vést
daimonion	daimonion	k1gInSc1	daimonion
(	(	kIx(	(
<g/>
svědomí	svědomí	k1gNnSc1	svědomí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
poznání	poznání	k1gNnSc1	poznání
je	být	k5eAaImIp3nS	být
pravá	pravý	k2eAgFnSc1d1	pravá
ctnost	ctnost	k1gFnSc1	ctnost
a	a	k8xC	a
neřest	neřest	k1gFnSc1	neřest
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
neznalosti	neznalost	k1gFnSc2	neznalost
<g/>
.	.	kIx.	.
</s>
<s>
Platón	Platón	k1gMnSc1	Platón
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
známém	známý	k2eAgNnSc6d1	známé
podobenství	podobenství	k1gNnSc6	podobenství
o	o	k7c6	o
jeskyni	jeskyně	k1gFnSc6	jeskyně
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
idea	idea	k1gFnSc1	idea
Dobra	dobro	k1gNnSc2	dobro
je	být	k5eAaImIp3nS	být
transcendentní	transcendentní	k2eAgFnSc1d1	transcendentní
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
Platóna	Platón	k1gMnSc2	Platón
tři	tři	k4xCgFnPc4	tři
stránky	stránka	k1gFnPc4	stránka
<g/>
:	:	kIx,	:
žádostivost	žádostivost	k1gFnSc4	žádostivost
<g/>
,	,	kIx,	,
vůli	vůle	k1gFnSc4	vůle
a	a	k8xC	a
rozum	rozum	k1gInSc4	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Rozum	rozum	k1gInSc1	rozum
má	mít	k5eAaImIp3nS	mít
ovládat	ovládat	k5eAaImF	ovládat
žádosti	žádost	k1gFnPc4	žádost
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgNnSc1d2	lepší
být	být	k5eAaImF	být
spravedlivý	spravedlivý	k2eAgInSc1d1	spravedlivý
<g/>
,	,	kIx,	,
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
rozum	rozum	k1gInSc1	rozum
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
věčnými	věčný	k2eAgFnPc7d1	věčná
hodnotami	hodnota	k1gFnPc7	hodnota
<g/>
,	,	kIx,	,
žádostivost	žádostivost	k1gFnSc4	žádostivost
dočasnými	dočasný	k2eAgInPc7d1	dočasný
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
rozum	rozum	k1gInSc4	rozum
přednost	přednost	k1gFnSc4	přednost
<g/>
.	.	kIx.	.
</s>
<s>
Dobrý	dobrý	k2eAgInSc1d1	dobrý
a	a	k8xC	a
spravedlivý	spravedlivý	k2eAgInSc1d1	spravedlivý
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Platóna	Platón	k1gMnSc4	Platón
totéž	týž	k3xTgNnSc1	týž
<g/>
.	.	kIx.	.
</s>
<s>
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
je	být	k5eAaImIp3nS	být
zastáncem	zastánce	k1gMnSc7	zastánce
teorie	teorie	k1gFnSc2	teorie
středu	střed	k1gInSc2	střed
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zlatá	zlatý	k2eAgFnSc1d1	zlatá
střední	střední	k2eAgFnSc1d1	střední
cesta	cesta	k1gFnSc1	cesta
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Špatné	špatná	k1gFnPc1	špatná
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
extrémy	extrém	k1gInPc4	extrém
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
hněv	hněv	k1gInSc1	hněv
je	být	k5eAaImIp3nS	být
dobrý	dobrý	k2eAgInSc1d1	dobrý
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
člověka	člověk	k1gMnSc4	člověk
podněcuje	podněcovat	k5eAaImIp3nS	podněcovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hněv	hněv	k1gInSc1	hněv
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
extrému	extrém	k1gInSc6	extrém
(	(	kIx(	(
<g/>
zášť	zášť	k1gFnSc4	zášť
<g/>
,	,	kIx,	,
nenávist	nenávist	k1gFnSc4	nenávist
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
špatný	špatný	k2eAgInSc1d1	špatný
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
extrému	extrém	k1gInSc6	extrém
(	(	kIx(	(
<g/>
netečnost	netečnost	k1gFnSc1	netečnost
<g/>
,	,	kIx,	,
apatie	apatie	k1gFnSc1	apatie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
extrémy	extrém	k1gInPc7	extrém
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
harmonie	harmonie	k1gFnSc2	harmonie
<g/>
.	.	kIx.	.
</s>
<s>
Etika	etika	k1gFnSc1	etika
znamenala	znamenat	k5eAaImAgFnS	znamenat
nejen	nejen	k6eAd1	nejen
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
a	a	k8xC	a
co	co	k3yInSc4	co
špatné	špatný	k2eAgFnPc4d1	špatná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
</s>
<s>
Eudaimonia	Eudaimonium	k1gNnSc2	Eudaimonium
–	–	k?	–
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
,	,	kIx,	,
blaženost	blaženost	k1gFnSc1	blaženost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
nás	my	k3xPp1nPc4	my
dovede	dovést	k5eAaPmIp3nS	dovést
náš	náš	k3xOp1gInSc4	náš
rozum	rozum	k1gInSc4	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
směry	směr	k1gInPc1	směr
pak	pak	k6eAd1	pak
také	také	k9	také
řeší	řešit	k5eAaImIp3nP	řešit
etické	etický	k2eAgFnPc4d1	etická
otázky	otázka	k1gFnPc4	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Epikurejci	epikurejec	k1gMnPc1	epikurejec
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediným	jediný	k2eAgInSc7d1	jediný
cílem	cíl	k1gInSc7	cíl
a	a	k8xC	a
ctností	ctnost	k1gFnSc7	ctnost
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
blaho	blaho	k1gNnSc4	blaho
<g/>
.	.	kIx.	.
</s>
<s>
Dosahování	dosahování	k1gNnSc1	dosahování
blaha	blaho	k1gNnSc2	blaho
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
podle	podle	k7c2	podle
nich	on	k3xPp3gFnPc2	on
člověk	člověk	k1gMnSc1	člověk
omezit	omezit	k5eAaPmF	omezit
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
nevhodné	vhodný	k2eNgFnPc1d1	nevhodná
(	(	kIx(	(
<g/>
sebedestrukce	sebedestrukce	k1gFnPc1	sebedestrukce
alkoholem	alkohol	k1gInSc7	alkohol
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stoikové	stoik	k1gMnPc1	stoik
se	se	k3xPyFc4	se
zase	zase	k9	zase
shodně	shodně	k6eAd1	shodně
s	s	k7c7	s
Aristotelem	Aristoteles	k1gMnSc7	Aristoteles
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
cílem	cíl	k1gInSc7	cíl
člověka	člověk	k1gMnSc2	člověk
je	on	k3xPp3gNnPc4	on
eudaimonia	eudaimonium	k1gNnPc4	eudaimonium
<g/>
.	.	kIx.	.
</s>
<s>
Věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc4	vesmír
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
inteligentní	inteligentní	k2eAgFnSc4d1	inteligentní
bytost	bytost	k1gFnSc4	bytost
a	a	k8xC	a
že	že	k8xS	že
tedy	tedy	k9	tedy
má	mít	k5eAaImIp3nS	mít
smysl	smysl	k1gInSc1	smysl
(	(	kIx(	(
<g/>
logos	logos	k1gInSc1	logos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stoicismus	stoicismus	k1gInSc1	stoicismus
uznává	uznávat	k5eAaImIp3nS	uznávat
realitu	realita	k1gFnSc4	realita
takovou	takový	k3xDgFnSc4	takový
<g/>
,	,	kIx,	,
jaká	jaký	k3yRgFnSc1	jaký
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc4	některý
věci	věc	k1gFnPc4	věc
může	moct	k5eAaImIp3nS	moct
změnit	změnit	k5eAaPmF	změnit
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
může	moct	k5eAaImIp3nS	moct
změnit	změnit	k5eAaPmF	změnit
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
přijmout	přijmout	k5eAaPmF	přijmout
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
stoiků	stoik	k1gMnPc2	stoik
nezáleží	záležet	k5eNaImIp3nS	záležet
na	na	k7c6	na
výsledcích	výsledek	k1gInPc6	výsledek
jednání	jednání	k1gNnSc2	jednání
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
např.	např.	kA	např.
utilitarismu	utilitarismus	k1gInSc2	utilitarismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
dobré	dobrý	k2eAgFnSc6d1	dobrá
vůli	vůle	k1gFnSc6	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Akvinský	Akvinský	k2eAgMnSc1d1	Akvinský
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
etiku	etika	k1gFnSc4	etika
na	na	k7c6	na
základě	základ	k1gInSc6	základ
přirozeného	přirozený	k2eAgInSc2d1	přirozený
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Vycházel	vycházet	k5eAaImAgInS	vycházet
z	z	k7c2	z
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
tradice	tradice	k1gFnSc2	tradice
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podložit	podložit	k5eAaPmF	podložit
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
etiku	etika	k1gFnSc4	etika
racionálními	racionální	k2eAgInPc7d1	racionální
základy	základ	k1gInPc7	základ
<g/>
.	.	kIx.	.
</s>
<s>
Nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
přirozeném	přirozený	k2eAgInSc6d1	přirozený
zákoně	zákon	k1gInSc6	zákon
(	(	kIx(	(
<g/>
lex	lex	k?	lex
naturalis	naturalis	k1gInSc1	naturalis
<g/>
)	)	kIx)	)
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
empirického	empirický	k2eAgNnSc2d1	empirické
pozorování	pozorování	k1gNnSc2	pozorování
a	a	k8xC	a
logických	logický	k2eAgFnPc2d1	logická
dedukcí	dedukce	k1gFnPc2	dedukce
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
věc	věc	k1gFnSc1	věc
sleduje	sledovat	k5eAaImIp3nS	sledovat
účel	účel	k1gInSc4	účel
<g/>
,	,	kIx,	,
a	a	k8xC	a
naplnění	naplnění	k1gNnSc1	naplnění
tohoto	tento	k3xDgInSc2	tento
účelu	účel	k1gInSc2	účel
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
<g/>
.	.	kIx.	.
</s>
<s>
Rozum	rozum	k1gInSc1	rozum
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
člověk	člověk	k1gMnSc1	člověk
od	od	k7c2	od
Boha	bůh	k1gMnSc2	bůh
dostal	dostat	k5eAaPmAgMnS	dostat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
východiskem	východisko	k1gNnSc7	východisko
morálky	morálka	k1gFnSc2	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Tomáš	Tomáš	k1gMnSc1	Tomáš
Akvinský	Akvinský	k2eAgMnSc1d1	Akvinský
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mravní	mravní	k2eAgNnSc1d1	mravní
posouzení	posouzení	k1gNnSc1	posouzení
jednání	jednání	k1gNnSc2	jednání
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
výsledku	výsledek	k1gInSc6	výsledek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
účelu	účel	k1gInSc3	účel
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgInSc3	který
směřuje	směřovat	k5eAaImIp3nS	směřovat
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
přirozené	přirozený	k2eAgNnSc1d1	přirozené
právo	právo	k1gNnSc1	právo
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
rozumu	rozum	k1gInSc6	rozum
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
kulturně	kulturně	k6eAd1	kulturně
podmíněné	podmíněný	k2eAgNnSc1d1	podmíněné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Summa	Summ	k1gMnSc4	Summ
Theologiae	Theologia	k1gMnSc4	Theologia
popsal	popsat	k5eAaPmAgMnS	popsat
Tomáš	Tomáš	k1gMnSc1	Tomáš
čtyři	čtyři	k4xCgFnPc4	čtyři
základní	základní	k2eAgFnPc4d1	základní
ctnosti	ctnost	k1gFnPc4	ctnost
<g/>
:	:	kIx,	:
rozumnost	rozumnost	k1gFnSc1	rozumnost
<g/>
,	,	kIx,	,
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
<g/>
,	,	kIx,	,
statečnost	statečnost	k1gFnSc1	statečnost
a	a	k8xC	a
uměřenost	uměřenost	k1gFnSc1	uměřenost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
se	se	k3xPyFc4	se
etika	etika	k1gFnSc1	etika
vypořádávala	vypořádávat	k5eAaImAgFnS	vypořádávat
s	s	k7c7	s
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
tvoří	tvořit	k5eAaImIp3nS	tvořit
základ	základ	k1gInSc4	základ
morálky	morálka	k1gFnSc2	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Hobbes	Hobbes	k1gMnSc1	Hobbes
míní	mínit	k5eAaImIp3nS	mínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
cestou	cesta	k1gFnSc7	cesta
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
norem	norma	k1gFnPc2	norma
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
vládcem	vládce	k1gMnSc7	vládce
a	a	k8xC	a
jedinci	jedinec	k1gMnPc7	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Vládce	vládce	k1gMnSc1	vládce
by	by	kYmCp3nS	by
neměl	mít	k5eNaImAgMnS	mít
obavu	obava	k1gFnSc4	obava
z	z	k7c2	z
anarchie	anarchie	k1gFnSc2	anarchie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Leviathan	Leviathana	k1gFnPc2	Leviathana
píše	psát	k5eAaImIp3nS	psát
o	o	k7c6	o
absolutistickém	absolutistický	k2eAgInSc6d1	absolutistický
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
prosazována	prosazovat	k5eAaImNgFnS	prosazovat
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
omezení	omezení	k1gNnSc2	omezení
sobeckých	sobecký	k2eAgFnPc2d1	sobecká
lidských	lidský	k2eAgFnPc2d1	lidská
emocí	emoce	k1gFnPc2	emoce
<g/>
,	,	kIx,	,
k	k	k7c3	k
vzájemnému	vzájemný	k2eAgInSc3d1	vzájemný
prospěchu	prospěch	k1gInSc3	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
John	John	k1gMnSc1	John
Locke	Lock	k1gInSc2	Lock
uznává	uznávat	k5eAaImIp3nS	uznávat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
Pojednání	pojednání	k1gNnSc6	pojednání
o	o	k7c6	o
vládě	vláda	k1gFnSc6	vláda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1690	[number]	k4	1690
potřebu	potřeba	k1gFnSc4	potřeba
společenské	společenský	k2eAgFnSc2d1	společenská
smlouvy	smlouva	k1gFnSc2	smlouva
–	–	k?	–
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
nutný	nutný	k2eAgMnSc1d1	nutný
arbitr	arbitr	k1gMnSc1	arbitr
pro	pro	k7c4	pro
nestranné	nestranný	k2eAgNnSc4d1	nestranné
řešení	řešení	k1gNnSc4	řešení
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Autorita	autorita	k1gFnSc1	autorita
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
vládce	vládce	k1gMnSc1	vládce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
náleží	náležet	k5eAaImIp3nS	náležet
spíše	spíše	k9	spíše
státním	státní	k2eAgFnPc3d1	státní
institucím	instituce	k1gFnPc3	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
základu	základ	k1gInSc6	základ
stojí	stát	k5eAaImIp3nS	stát
i	i	k9	i
dnešní	dnešní	k2eAgFnSc1d1	dnešní
moderní	moderní	k2eAgFnSc1d1	moderní
demokracie	demokracie	k1gFnSc1	demokracie
a	a	k8xC	a
oddělení	oddělení	k1gNnSc1	oddělení
mocí	moc	k1gFnSc7	moc
zákonodárné	zákonodárný	k2eAgInPc1d1	zákonodárný
<g/>
,	,	kIx,	,
výkonné	výkonný	k2eAgInPc1d1	výkonný
a	a	k8xC	a
soudní	soudní	k2eAgInSc1d1	soudní
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
utilitarismu	utilitarismus	k1gInSc2	utilitarismus
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
etických	etický	k2eAgFnPc2d1	etická
teorií	teorie	k1gFnPc2	teorie
<g/>
,	,	kIx,	,
stál	stát	k5eAaImAgInS	stát
Jeremy	Jerema	k1gFnSc2	Jerema
Bentham	Bentham	k1gInSc1	Bentham
<g/>
.	.	kIx.	.
</s>
<s>
Hledání	hledání	k1gNnSc1	hledání
teorie	teorie	k1gFnSc2	teorie
morálky	morálka	k1gFnSc2	morálka
ku	k	k7c3	k
prospěchu	prospěch	k1gInSc3	prospěch
občanům	občan	k1gMnPc3	občan
jej	on	k3xPp3gMnSc4	on
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
principu	princip	k1gInSc3	princip
užitečnosti	užitečnost	k1gFnSc2	užitečnost
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
je	být	k5eAaImIp3nS	být
užitečná	užitečný	k2eAgFnSc1d1	užitečná
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
co	co	k9	co
největšímu	veliký	k2eAgNnSc3d3	veliký
a	a	k8xC	a
nejtrvalejšímu	trvalý	k2eAgNnSc3d3	nejtrvalejší
štěstí	štěstí	k1gNnSc3	štěstí
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Záleží	záležet	k5eAaImIp3nS	záležet
tedy	tedy	k9	tedy
především	především	k9	především
na	na	k7c6	na
následcích	následek	k1gInPc6	následek
každého	každý	k3xTgInSc2	každý
činu	čin	k1gInSc2	čin
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
čin	čin	k1gInSc4	čin
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
normami	norma	k1gFnPc7	norma
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
druhořadé	druhořadý	k2eAgNnSc1d1	druhořadé
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Stuart	Stuarta	k1gFnPc2	Stuarta
Mill	Mill	k1gMnSc1	Mill
upozornil	upozornit	k5eAaPmAgMnS	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
nejsou	být	k5eNaImIp3nP	být
příliš	příliš	k6eAd1	příliš
předvídaví	předvídavý	k2eAgMnPc1d1	předvídavý
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Utilitarismus	utilitarismus	k1gInSc1	utilitarismus
představil	představit	k5eAaPmAgInS	představit
tzv.	tzv.	kA	tzv.
utilitarismus	utilitarismus	k1gInSc4	utilitarismus
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
neobejde	obejde	k6eNd1	obejde
bez	bez	k7c2	bez
zásad	zásada	k1gFnPc2	zásada
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zásada	zásada	k1gFnSc1	zásada
pravdomluvnosti	pravdomluvnost	k1gFnSc2	pravdomluvnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Immanuel	Immanuel	k1gMnSc1	Immanuel
Kant	Kant	k1gMnSc1	Kant
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepoznáváme	poznávat	k5eNaImIp1nP	poznávat
věci	věc	k1gFnPc4	věc
"	"	kIx"	"
<g/>
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
věci	věc	k1gFnPc1	věc
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgInPc1	jaký
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
jeví	jevit	k5eAaImIp3nS	jevit
<g/>
.	.	kIx.	.
</s>
<s>
Morálku	morálka	k1gFnSc4	morálka
proto	proto	k8xC	proto
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c6	na
dobré	dobrý	k2eAgFnSc6d1	dobrá
vůli	vůle	k1gFnSc6	vůle
–	–	k?	–
to	ten	k3xDgNnSc4	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jediné	jediný	k2eAgNnSc1d1	jediné
správné	správný	k2eAgNnSc1d1	správné
východisko	východisko	k1gNnSc1	východisko
dobrého	dobrý	k2eAgNnSc2d1	dobré
jednání	jednání	k1gNnSc2	jednání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kritice	kritika	k1gFnSc6	kritika
praktického	praktický	k2eAgInSc2d1	praktický
rozumu	rozum	k1gInSc2	rozum
(	(	kIx(	(
<g/>
1788	[number]	k4	1788
<g/>
)	)	kIx)	)
představil	představit	k5eAaPmAgInS	představit
kategorický	kategorický	k2eAgInSc1d1	kategorický
imperativ	imperativ	k1gInSc1	imperativ
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jednejte	jednat	k5eAaImRp2nP	jednat
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
maxima	maximum	k1gNnSc2	maximum
vaší	váš	k3xOp2gFnSc2	váš
vůle	vůle	k1gFnSc2	vůle
mohla	moct	k5eAaImAgFnS	moct
vždy	vždy	k6eAd1	vždy
být	být	k5eAaImF	být
současně	současně	k6eAd1	současně
uplatněna	uplatnit	k5eAaPmNgFnS	uplatnit
jako	jako	k8xC	jako
princip	princip	k1gInSc1	princip
vytvářející	vytvářející	k2eAgInSc1d1	vytvářející
univerzální	univerzální	k2eAgInSc4d1	univerzální
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Druhý	druhý	k4xOgMnSc1	druhý
člověk	člověk	k1gMnSc1	člověk
nemá	mít	k5eNaImIp3nS	mít
být	být	k5eAaImF	být
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
prostředek	prostředek	k1gInSc1	prostředek
našeho	náš	k3xOp1gInSc2	náš
prospěchu	prospěch	k1gInSc2	prospěch
–	–	k?	–
lidskost	lidskost	k1gFnSc1	lidskost
je	být	k5eAaImIp3nS	být
cílem	cíl	k1gInSc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Kantova	Kantův	k2eAgFnSc1d1	Kantova
morálka	morálka	k1gFnSc1	morálka
je	být	k5eAaImIp3nS	být
apriorní	apriorní	k2eAgFnSc1d1	apriorní
–	–	k?	–
platí	platit	k5eAaImIp3nS	platit
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
důsledky	důsledek	k1gInPc4	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Nietzsche	Nietzsch	k1gFnSc2	Nietzsch
se	s	k7c7	s
Zarathustrovým	Zarathustrový	k2eAgInSc7d1	Zarathustrový
výrokem	výrok	k1gInSc7	výrok
"	"	kIx"	"
<g/>
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
mrtev	mrtev	k2eAgMnSc1d1	mrtev
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
loučí	louč	k1gFnSc7	louč
s	s	k7c7	s
představou	představa	k1gFnSc7	představa
absolutní	absolutní	k2eAgFnSc2d1	absolutní
morálky	morálka	k1gFnSc2	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
smyslem	smysl	k1gInSc7	smysl
všeho	všecek	k3xTgNnSc2	všecek
nadčlověk	nadčlověk	k1gMnSc1	nadčlověk
–	–	k?	–
směřování	směřování	k1gNnSc4	směřování
ke	k	k7c3	k
zdokonalení	zdokonalení	k1gNnSc3	zdokonalení
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
schopen	schopen	k2eAgMnSc1d1	schopen
"	"	kIx"	"
<g/>
přitakání	přitakání	k1gNnSc1	přitakání
životu	život	k1gInSc2	život
takový	takový	k3xDgMnSc1	takový
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgMnSc1	jaký
je	být	k5eAaImIp3nS	být
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Existencialismus	existencialismus	k1gInSc1	existencialismus
<g/>
.	.	kIx.	.
</s>
<s>
Sø	Sø	k?	Sø
Kierkegaard	Kierkegaard	k1gInSc1	Kierkegaard
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
stěžejní	stěžejní	k2eAgInSc4d1	stěžejní
vztah	vztah	k1gInSc4	vztah
člověka	člověk	k1gMnSc2	člověk
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
dal	dát	k5eAaPmAgMnS	dát
člověku	člověk	k1gMnSc3	člověk
svobodnou	svobodný	k2eAgFnSc4d1	svobodná
vůli	vůle	k1gFnSc4	vůle
a	a	k8xC	a
s	s	k7c7	s
tou	ten	k3xDgFnSc7	ten
je	být	k5eAaImIp3nS	být
svázaná	svázaný	k2eAgFnSc1d1	svázaná
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
<g/>
.	.	kIx.	.
</s>
<s>
Lidská	lidský	k2eAgFnSc1d1	lidská
existence	existence	k1gFnSc1	existence
je	být	k5eAaImIp3nS	být
něčím	něco	k3yInSc7	něco
zcela	zcela	k6eAd1	zcela
jedinečným	jedinečný	k2eAgInSc7d1	jedinečný
<g/>
,	,	kIx,	,
nezáleží	záležet	k5eNaImIp3nS	záležet
jen	jen	k9	jen
na	na	k7c6	na
výsledku	výsledek	k1gInSc6	výsledek
naší	náš	k3xOp1gFnSc2	náš
volby	volba	k1gFnSc2	volba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
upřímnosti	upřímnost	k1gFnSc6	upřímnost
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Heidegger	Heidegger	k1gMnSc1	Heidegger
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
vrženosti	vrženost	k1gFnSc6	vrženost
do	do	k7c2	do
světa	svět	k1gInSc2	svět
a	a	k8xC	a
o	o	k7c6	o
maskách	maska	k1gFnPc6	maska
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
si	se	k3xPyFc3	se
oblékáme	oblékat	k5eAaImIp1nP	oblékat
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
jenom	jenom	k9	jenom
povrchně	povrchně	k6eAd1	povrchně
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
převzetí	převzetí	k1gNnSc3	převzetí
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
vede	vést	k5eAaImIp3nS	vést
skrze	skrze	k?	skrze
uvědomění	uvědomění	k1gNnSc1	uvědomění
si	se	k3xPyFc3	se
omezenosti	omezenost	k1gFnSc3	omezenost
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
–	–	k?	–
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
Sartre	Sartr	k1gInSc5	Sartr
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistuje	existovat	k5eNaImIp3nS	existovat
žádný	žádný	k3yNgInSc4	žádný
apriorní	apriorní	k2eAgInSc4d1	apriorní
účel	účel	k1gInSc4	účel
nebo	nebo	k8xC	nebo
smysl	smysl	k1gInSc4	smysl
lidské	lidský	k2eAgFnSc2d1	lidská
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
předchází	předcházet	k5eAaImIp3nS	předcházet
esenci	esence	k1gFnSc4	esence
–	–	k?	–
tedy	tedy	k9	tedy
napřed	napřed	k6eAd1	napřed
člověk	člověk	k1gMnSc1	člověk
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
pak	pak	k6eAd1	pak
teprve	teprve	k6eAd1	teprve
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
bude	být	k5eAaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
"	"	kIx"	"
<g/>
být	být	k5eAaImF	být
sám	sám	k3xTgMnSc1	sám
sebou	se	k3xPyFc7	se
<g/>
"	"	kIx"	"
a	a	k8xC	a
ne	ne	k9	ne
jenom	jenom	k9	jenom
přijímat	přijímat	k5eAaImF	přijímat
role	role	k1gFnPc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Sartre	Sartr	k1gMnSc5	Sartr
domyslel	domyslet	k5eAaPmAgInS	domyslet
svět	svět	k1gInSc1	svět
bez	bez	k7c2	bez
Boha	bůh	k1gMnSc2	bůh
–	–	k?	–
"	"	kIx"	"
<g/>
není	být	k5eNaImIp3nS	být
na	na	k7c4	na
co	co	k3yInSc4	co
se	se	k3xPyFc4	se
upnout	upnout	k5eAaPmF	upnout
<g/>
,	,	kIx,	,
jsme	být	k5eAaImIp1nP	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
ze	z	k7c2	z
sebe	se	k3xPyFc2	se
uděláme	udělat	k5eAaPmIp1nP	udělat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
Josepha	Joseph	k1gMnSc2	Joseph
Fletchera	Fletcher	k1gMnSc2	Fletcher
není	být	k5eNaImIp3nS	být
moudré	moudrý	k2eAgNnSc1d1	moudré
postupovat	postupovat	k5eAaImF	postupovat
od	od	k7c2	od
pravidel	pravidlo	k1gNnPc2	pravidlo
k	k	k7c3	k
situacím	situace	k1gFnPc3	situace
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
nastoluje	nastolovat	k5eAaImIp3nS	nastolovat
"	"	kIx"	"
<g/>
princip	princip	k1gInSc1	princip
lásky	láska	k1gFnSc2	láska
<g/>
"	"	kIx"	"
–	–	k?	–
jediné	jediný	k2eAgNnSc1d1	jediné
a	a	k8xC	a
absolutní	absolutní	k2eAgNnSc1d1	absolutní
pravidlo	pravidlo	k1gNnSc1	pravidlo
je	být	k5eAaImIp3nS	být
pravidlo	pravidlo	k1gNnSc4	pravidlo
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
tak	tak	k6eAd1	tak
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
morálku	morálka	k1gFnSc4	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Směru	směr	k1gInSc2	směr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
staví	stavit	k5eAaPmIp3nS	stavit
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
princip	princip	k1gInSc1	princip
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
agapéismus	agapéismus	k1gInSc1	agapéismus
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
řeckého	řecký	k2eAgInSc2d1	řecký
výrazu	výraz	k1gInSc2	výraz
pro	pro	k7c4	pro
lásku	láska	k1gFnSc4	láska
pečující	pečující	k2eAgFnPc4d1	pečující
<g/>
,	,	kIx,	,
agapé	agapý	k2eAgFnPc4d1	agapý
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
Tillich	Tillich	k1gMnSc1	Tillich
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravidla	pravidlo	k1gNnPc1	pravidlo
jsou	být	k5eAaImIp3nP	být
přesto	přesto	k8xC	přesto
důležitá	důležitý	k2eAgFnSc1d1	důležitá
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
totiž	totiž	k9	totiž
poskytnout	poskytnout	k5eAaPmF	poskytnout
určité	určitý	k2eAgNnSc4d1	určité
vodítko	vodítko	k1gNnSc4	vodítko
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc4	tři
přístupy	přístup	k1gInPc4	přístup
<g/>
:	:	kIx,	:
autonomní	autonomní	k2eAgFnSc1d1	autonomní
–	–	k?	–
etika	etika	k1gFnSc1	etika
zcela	zcela	k6eAd1	zcela
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c4	na
náboženství	náboženství	k1gNnSc4	náboženství
heteronomní	heteronomní	k2eAgFnSc1d1	heteronomní
–	–	k?	–
morálka	morálka	k1gFnSc1	morálka
staví	stavit	k5eAaImIp3nS	stavit
na	na	k7c6	na
náboženských	náboženský	k2eAgNnPc6d1	náboženské
přesvědčeních	přesvědčení	k1gNnPc6	přesvědčení
teonomní	teonomní	k2eAgFnSc4d1	teonomní
–	–	k?	–
morálka	morálka	k1gFnSc1	morálka
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
zdroje	zdroj	k1gInSc2	zdroj
jako	jako	k8xS	jako
náboženství	náboženství	k1gNnSc2	náboženství
Podle	podle	k7c2	podle
křesťanství	křesťanství	k1gNnSc2	křesťanství
je	být	k5eAaImIp3nS	být
Bůh	bůh	k1gMnSc1	bůh
absolutní	absolutní	k2eAgMnSc1d1	absolutní
autoritou	autorita	k1gFnSc7	autorita
morálky	morálka	k1gFnSc2	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
mohou	moct	k5eAaImIp3nP	moct
morální	morální	k2eAgInSc4d1	morální
zákon	zákon	k1gInSc4	zákon
poznávat	poznávat	k5eAaImF	poznávat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
církevní	církevní	k2eAgFnPc4d1	církevní
tradice	tradice	k1gFnPc4	tradice
<g/>
,	,	kIx,	,
Božského	božský	k2eAgNnSc2d1	božské
vnuknutí	vnuknutí	k1gNnSc2	vnuknutí
(	(	kIx(	(
<g/>
Duch	duch	k1gMnSc1	duch
svatý	svatý	k2eAgMnSc1d1	svatý
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
skrze	skrze	k?	skrze
svědomí	svědomí	k1gNnSc2	svědomí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
judaismu	judaismus	k1gInSc6	judaismus
je	být	k5eAaImIp3nS	být
autoritou	autorita	k1gFnSc7	autorita
např.	např.	kA	např.
Tóra	tóra	k1gFnSc1	tóra
<g/>
,	,	kIx,	,
v	v	k7c6	v
islámu	islám	k1gInSc6	islám
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
Korán	korán	k1gInSc4	korán
<g/>
,	,	kIx,	,
hadísy	hadísa	k1gFnPc4	hadísa
a	a	k8xC	a
zákon	zákon	k1gInSc4	zákon
šaría	šaríum	k1gNnSc2	šaríum
<g/>
,	,	kIx,	,
hinduismus	hinduismus	k1gInSc1	hinduismus
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
spisů	spis	k1gInPc2	spis
a	a	k8xC	a
živé	živý	k2eAgFnPc1d1	živá
zkušenosti	zkušenost	k1gFnPc1	zkušenost
<g/>
,	,	kIx,	,
buddhismus	buddhismus	k1gInSc1	buddhismus
kromě	kromě	k7c2	kromě
spisů	spis	k1gInPc2	spis
klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
rozum	rozum	k1gInSc4	rozum
a	a	k8xC	a
osobního	osobní	k2eAgMnSc2d1	osobní
učitele	učitel	k1gMnSc2	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Svědomí	svědomí	k1gNnSc2	svědomí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
etice	etika	k1gFnSc6	etika
hraje	hrát	k5eAaImIp3nS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
svědomí	svědomí	k1gNnSc2	svědomí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
křesťanském	křesťanský	k2eAgNnSc6d1	křesťanské
pojetí	pojetí	k1gNnSc6	pojetí
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zákon	zákon	k1gInSc1	zákon
vepsaný	vepsaný	k2eAgInSc1d1	vepsaný
Bohem	bůh	k1gMnSc7	bůh
do	do	k7c2	do
lidských	lidský	k2eAgNnPc2d1	lidské
srdcí	srdce	k1gNnPc2	srdce
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
hlas	hlas	k1gInSc1	hlas
Boha	bůh	k1gMnSc2	bůh
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
duši	duše	k1gFnSc6	duše
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
niterné	niterný	k2eAgNnSc4d1	niterné
zjevení	zjevení	k1gNnSc4	zjevení
Boha	bůh	k1gMnSc2	bůh
v	v	k7c4	v
nás	my	k3xPp1nPc4	my
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
má	mít	k5eAaImIp3nS	mít
svědomí	svědomí	k1gNnSc4	svědomí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
cestou	cesta	k1gFnSc7	cesta
k	k	k7c3	k
poznání	poznání	k1gNnSc3	poznání
přirozeného	přirozený	k2eAgInSc2d1	přirozený
zákona	zákon	k1gInSc2	zákon
a	a	k8xC	a
také	také	k9	také
Boží	boží	k2eAgFnSc2d1	boží
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sekulárním	sekulární	k2eAgNnSc6d1	sekulární
pojetí	pojetí	k1gNnSc6	pojetí
je	být	k5eAaImIp3nS	být
svědomí	svědomí	k1gNnSc1	svědomí
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
svobodě	svoboda	k1gFnSc6	svoboda
a	a	k8xC	a
poznání	poznání	k1gNnSc6	poznání
dobra	dobro	k1gNnSc2	dobro
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
nás	my	k3xPp1nPc4	my
naučila	naučit	k5eAaPmAgFnS	naučit
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
z	z	k7c2	z
pudů	pud	k1gInPc2	pud
apod.	apod.	kA	apod.
Svědomí	svědomí	k1gNnSc1	svědomí
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
univerzálním	univerzální	k2eAgInSc7d1	univerzální
činitelem	činitel	k1gInSc7	činitel
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
základ	základ	k1gInSc4	základ
mravní	mravní	k2eAgFnSc2d1	mravní
argumentace	argumentace	k1gFnSc2	argumentace
napříč	napříč	k7c7	napříč
kulturami	kultura	k1gFnPc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Hume	Hume	k1gNnSc2	Hume
započal	započnout	k5eAaPmAgMnS	započnout
éru	éra	k1gFnSc4	éra
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dobro	dobro	k1gNnSc1	dobro
a	a	k8xC	a
zlo	zlo	k1gNnSc1	zlo
nerozlišovalo	rozlišovat	k5eNaImAgNnS	rozlišovat
rozumem	rozum	k1gInSc7	rozum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
citem	cit	k1gInSc7	cit
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
lze	lze	k6eAd1	lze
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
afekty	afekt	k1gInPc1	afekt
–	–	k?	–
přímé	přímý	k2eAgFnSc2d1	přímá
a	a	k8xC	a
nepřímé	přímý	k2eNgFnSc2d1	nepřímá
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
ale	ale	k9	ale
řídí	řídit	k5eAaImIp3nS	řídit
jen	jen	k9	jen
citem	cit	k1gInSc7	cit
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
mimo	mimo	k6eAd1	mimo
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
se	se	k3xPyFc4	se
vcítím	vcítit	k5eAaPmIp1nS	vcítit
do	do	k7c2	do
druhých	druhý	k4xOgFnPc2	druhý
<g/>
,	,	kIx,	,
budu	být	k5eAaImBp1nS	být
jednat	jednat	k5eAaImF	jednat
jako	jako	k9	jako
oni	onen	k3xDgMnPc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
třeba	třeba	k6eAd1	třeba
i	i	k9	i
špatně	špatně	k6eAd1	špatně
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP	mít
strach	strach	k1gInSc4	strach
jít	jít	k5eAaImF	jít
proti	proti	k7c3	proti
davu	dav	k1gInSc3	dav
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
rádi	rád	k2eAgMnPc1d1	rád
v	v	k7c6	v
davu	dav	k1gInSc6	dav
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
potom	potom	k6eAd1	potom
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
lidé	člověk	k1gMnPc1	člověk
poznat	poznat	k5eAaPmF	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
něco	něco	k3yInSc4	něco
špatně	špatně	k6eAd1	špatně
<g/>
?	?	kIx.	?
</s>
<s>
Immanuel	Immanuel	k1gMnSc1	Immanuel
Kant	Kant	k1gMnSc1	Kant
naopak	naopak	k6eAd1	naopak
city	city	k1gFnSc2	city
zavrhl	zavrhnout	k5eAaPmAgInS	zavrhnout
a	a	k8xC	a
kladl	klást	k5eAaImAgInS	klást
důraz	důraz	k1gInSc4	důraz
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
rozum	rozum	k1gInSc4	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
rozumu	rozum	k1gInSc3	rozum
mohu	moct	k5eAaImIp1nS	moct
lépe	dobře	k6eAd2	dobře
poznat	poznat	k5eAaPmF	poznat
své	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Abych	aby	kYmCp1nS	aby
poznal	poznat	k5eAaPmAgMnS	poznat
svého	svůj	k3xOyFgMnSc4	svůj
přítele	přítel	k1gMnSc4	přítel
<g/>
,	,	kIx,	,
musím	muset	k5eAaImIp1nS	muset
jej	on	k3xPp3gMnSc4	on
nejprve	nejprve	k6eAd1	nejprve
nenávidět	návidět	k5eNaImF	návidět
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
jej	on	k3xPp3gMnSc4	on
nezávisle	závisle	k6eNd1	závisle
posoudil	posoudit	k5eAaPmAgMnS	posoudit
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
můžu	můžu	k?	můžu
cítit	cítit	k5eAaImF	cítit
něco	něco	k6eAd1	něco
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
třeba	třeba	k6eAd1	třeba
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
<g/>
?	?	kIx.	?
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
mě	já	k3xPp1nSc4	já
může	moct	k5eAaImIp3nS	moct
dotýkat	dotýkat	k5eAaImF	dotýkat
dění	dění	k1gNnSc4	dění
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
<g/>
?	?	kIx.	?
</s>
<s>
Zde	zde	k6eAd1	zde
funguje	fungovat	k5eAaImIp3nS	fungovat
soucit	soucit	k1gInSc4	soucit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
situacích	situace	k1gFnPc6	situace
ale	ale	k8xC	ale
naše	náš	k3xOp1gNnSc1	náš
morální	morální	k2eAgNnSc1d1	morální
souzení	souzení	k1gNnSc1	souzení
nenásleduje	následovat	k5eNaImIp3nS	následovat
cítění	cítění	k1gNnSc4	cítění
<g/>
.	.	kIx.	.
</s>
<s>
Někomu	někdo	k3yInSc3	někdo
přeji	přát	k5eAaImIp1nS	přát
vše	všechen	k3xTgNnSc1	všechen
špatné	špatný	k2eAgNnSc1d1	špatné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
soucítím	soucítit	k5eAaImIp1nS	soucítit
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
bolestí	bolest	k1gFnSc7	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
také	také	k9	také
možno	možno	k6eAd1	možno
motivovat	motivovat	k5eAaBmF	motivovat
rozumem	rozum	k1gInSc7	rozum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
motivace	motivace	k1gFnSc1	motivace
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
silná	silný	k2eAgFnSc1d1	silná
jako	jako	k8xC	jako
citová	citový	k2eAgFnSc1d1	citová
<g/>
.	.	kIx.	.
</s>
<s>
Morální	morální	k2eAgInPc1d1	morální
soudy	soud	k1gInPc1	soud
mají	mít	k5eAaImIp3nP	mít
povahu	povaha	k1gFnSc4	povaha
složitou	složitý	k2eAgFnSc4d1	složitá
<g/>
.	.	kIx.	.
</s>
<s>
Morální	morální	k2eAgInSc1d1	morální
vývoj	vývoj	k1gInSc1	vývoj
je	být	k5eAaImIp3nS	být
podmíněn	podmínit	k5eAaPmNgInS	podmínit
jak	jak	k6eAd1	jak
citovým	citový	k2eAgInSc7d1	citový
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
rozumovým	rozumový	k2eAgInSc7d1	rozumový
vývojem	vývoj	k1gInSc7	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
zakrnělý	zakrnělý	k2eAgMnSc1d1	zakrnělý
v	v	k7c6	v
citech	cit	k1gInPc6	cit
nedosáhne	dosáhnout	k5eNaPmIp3nS	dosáhnout
plného	plný	k2eAgInSc2d1	plný
morálního	morální	k2eAgInSc2d1	morální
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
třeba	třeba	k6eAd1	třeba
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dítě	dítě	k1gNnSc1	dítě
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
v	v	k7c6	v
deprivaci	deprivace	k1gFnSc6	deprivace
<g/>
.	.	kIx.	.
</s>
<s>
Svědomí	svědomí	k1gNnSc1	svědomí
Projev	projev	k1gInSc1	projev
svědomí	svědomí	k1gNnSc3	svědomí
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
náš	náš	k3xOp1gInSc1	náš
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
éthos	éthos	k1gInSc1	éthos
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
postavit	postavit	k5eAaPmF	postavit
třeba	třeba	k6eAd1	třeba
proti	proti	k7c3	proti
celospolečenskému	celospolečenský	k2eAgInSc3d1	celospolečenský
étosu	étos	k1gInSc3	étos
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
bylo	být	k5eAaImAgNnS	být
pojímáno	pojímat	k5eAaImNgNnS	pojímat
jako	jako	k8xS	jako
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
hlas	hlas	k1gInSc1	hlas
boží	boží	k2eAgInSc1d1	boží
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
poslechnout	poslechnout	k5eAaPmF	poslechnout
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
pohled	pohled	k1gInSc1	pohled
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
psychologický	psychologický	k2eAgInSc1d1	psychologický
<g/>
.	.	kIx.	.
́	́	k?	́
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
jej	on	k3xPp3gInSc4	on
mají	mít	k5eAaImIp3nP	mít
tak	tak	k9	tak
silné	silný	k2eAgFnPc1d1	silná
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
sami	sám	k3xTgMnPc1	sám
se	s	k7c7	s
sebou	se	k3xPyFc7	se
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
</s>
<s>
Nesou	nést	k5eAaImIp3nP	nést
na	na	k7c6	na
všem	všecek	k3xTgNnSc6	všecek
takovou	takový	k3xDgFnSc4	takový
vinu	vina	k1gFnSc4	vina
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
ji	on	k3xPp3gFnSc4	on
nikdo	nikdo	k3yNnSc1	nikdo
nemůže	moct	k5eNaImIp3nS	moct
sejmout	sejmout	k5eAaPmF	sejmout
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
citovém	citový	k2eAgInSc6d1	citový
a	a	k8xC	a
morálním	morální	k2eAgInSc6d1	morální
vývoji	vývoj	k1gInSc6	vývoj
naopak	naopak	k6eAd1	naopak
zakrnět	zakrnět	k5eAaPmF	zakrnět
a	a	k8xC	a
netrápí	trápit	k5eNaImIp3nS	trápit
jej	on	k3xPp3gMnSc4	on
třeba	třeba	k6eAd1	třeba
ani	ani	k8xC	ani
vražda	vražda	k1gFnSc1	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
vnímavé	vnímavý	k2eAgFnPc1d1	vnímavá
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
vývoji	vývoj	k1gInSc6	vývoj
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Neměly	mít	k5eNaImAgFnP	mít
by	by	kYmCp3nP	by
být	být	k5eAaImF	být
brány	brát	k5eAaImNgInP	brát
na	na	k7c4	na
lehkou	lehký	k2eAgFnSc4d1	lehká
váhu	váha	k1gFnSc4	váha
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gInSc4	jejich
nevinný	vinný	k2eNgInSc4d1	nevinný
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
hlas	hlas	k1gInSc4	hlas
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
ten	ten	k3xDgInSc1	ten
správný	správný	k2eAgInSc1d1	správný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
my	my	k3xPp1nPc1	my
je	on	k3xPp3gMnPc4	on
výchovou	výchova	k1gFnSc7	výchova
zkazíme	zkazit	k5eAaPmIp1nP	zkazit
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Akvinský	Akvinský	k2eAgMnSc1d1	Akvinský
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
svědomí	svědomí	k1gNnSc1	svědomí
je	být	k5eAaImIp3nS	být
aplikací	aplikace	k1gFnSc7	aplikace
znalostí	znalost	k1gFnPc2	znalost
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
svědomí	svědomí	k1gNnSc4	svědomí
následné	následný	k2eAgNnSc4d1	následné
a	a	k8xC	a
předchozí	předchozí	k2eAgNnSc4d1	předchozí
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgFnSc1d1	předchozí
je	být	k5eAaImIp3nS	být
důležitější	důležitý	k2eAgFnSc1d2	důležitější
a	a	k8xC	a
určuje	určovat	k5eAaImIp3nS	určovat
naše	náš	k3xOp1gNnSc4	náš
morální	morální	k2eAgNnSc4d1	morální
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gNnSc1	náš
svědomí	svědomí	k1gNnSc1	svědomí
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
tři	tři	k4xCgFnPc4	tři
roviny	rovina	k1gFnPc4	rovina
<g/>
:	:	kIx,	:
synderesis	synderesis	k1gFnSc1	synderesis
–	–	k?	–
základní	základní	k2eAgFnSc1d1	základní
rovina	rovina	k1gFnSc1	rovina
člověku	člověk	k1gMnSc3	člověk
vrozená	vrozený	k2eAgFnSc1d1	vrozená
<g/>
,	,	kIx,	,
nejobecnější	obecní	k2eAgFnSc1d3	obecní
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
dobro	dobro	k1gNnSc4	dobro
konat	konat	k5eAaImF	konat
a	a	k8xC	a
neubližovat	ubližovat	k5eNaImF	ubližovat
druhým	druhý	k4xOgInSc7	druhý
lidem	lid	k1gInSc7	lid
sapientia	sapientia	k1gFnSc1	sapientia
–	–	k?	–
rovina	rovina	k1gFnSc1	rovina
moudrosti	moudrost	k1gFnSc2	moudrost
scientia	scientia	k1gFnSc1	scientia
–	–	k?	–
rovina	rovina	k1gFnSc1	rovina
konkrétního	konkrétní	k2eAgNnSc2d1	konkrétní
vědění	vědění	k1gNnSc2	vědění
Všichni	všechen	k3xTgMnPc1	všechen
lidé	člověk	k1gMnPc1	člověk
to	ten	k3xDgNnSc4	ten
mají	mít	k5eAaImIp3nP	mít
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
stejně	stejně	k6eAd1	stejně
nastaveno	nastavit	k5eAaPmNgNnS	nastavit
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
u	u	k7c2	u
každého	každý	k3xTgInSc2	každý
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
podmínky	podmínka	k1gFnSc2	podmínka
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
tak	tak	k6eAd1	tak
není	být	k5eNaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
libovůle	libovůle	k1gFnSc1	libovůle
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
podřízeno	podřízen	k2eAgNnSc4d1	podřízeno
všeobecnému	všeobecný	k2eAgInSc3d1	všeobecný
zákonu	zákon	k1gInSc3	zákon
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
základě	základ	k1gInSc6	základ
přirozeného	přirozený	k2eAgInSc2d1	přirozený
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
čemu	co	k3yInSc3	co
má	mít	k5eAaImIp3nS	mít
pak	pak	k6eAd1	pak
člověk	člověk	k1gMnSc1	člověk
sklon	sklona	k1gFnPc2	sklona
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
dobré	dobrý	k2eAgNnSc4d1	dobré
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
povinnost	povinnost	k1gFnSc4	povinnost
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
<g/>
.	.	kIx.	.
nejzákladnější	základní	k2eAgFnSc1d3	nejzákladnější
je	on	k3xPp3gNnSc4	on
zachování	zachování	k1gNnSc4	zachování
si	se	k3xPyFc3	se
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
podobné	podobný	k2eAgNnSc1d1	podobné
vše	všechen	k3xTgNnSc1	všechen
živé	živý	k2eAgNnSc1d1	živé
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
lidé	člověk	k1gMnPc1	člověk
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
je	být	k5eAaImIp3nS	být
společné	společný	k2eAgNnSc1d1	společné
také	také	k9	také
<g/>
,	,	kIx,	,
výchova	výchova	k1gFnSc1	výchova
potomků	potomek	k1gMnPc2	potomek
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
třetí	třetí	k4xOgNnSc4	třetí
je	být	k5eAaImIp3nS	být
čistě	čistě	k6eAd1	čistě
lidské	lidský	k2eAgNnSc4d1	lidské
–	–	k?	–
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
sklon	sklon	k1gInSc1	sklon
žít	žít	k5eAaImF	žít
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
poznat	poznat	k5eAaPmF	poznat
pravdu	pravda	k1gFnSc4	pravda
o	o	k7c6	o
bohu	bůh	k1gMnSc6	bůh
Tento	tento	k3xDgInSc1	tento
přirozený	přirozený	k2eAgInSc1d1	přirozený
zákon	zákon	k1gInSc1	zákon
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
všech	všecek	k3xTgMnPc2	všecek
lidí	člověk	k1gMnPc2	člověk
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
víc	hodně	k6eAd2	hodně
ale	ale	k8xC	ale
jdeme	jít	k5eAaImIp1nP	jít
k	k	k7c3	k
jednotlivostem	jednotlivost	k1gFnPc3	jednotlivost
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
víc	hodně	k6eAd2	hodně
můžeme	moct	k5eAaImIp1nP	moct
selhat	selhat	k5eAaPmF	selhat
–	–	k?	–
každý	každý	k3xTgMnSc1	každý
tedy	tedy	k9	tedy
přikázání	přikázání	k1gNnSc1	přikázání
zákona	zákon	k1gInSc2	zákon
vykoná	vykonat	k5eAaPmIp3nS	vykonat
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
někdo	někdo	k3yInSc1	někdo
špatné	špatný	k2eAgInPc1d1	špatný
návyky	návyk	k1gInPc1	návyk
<g/>
,	,	kIx,	,
tak	tak	k9	tak
už	už	k6eAd1	už
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
nepřijde	přijít	k5eNaPmIp3nS	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Prapůvodně	prapůvodně	k6eAd1	prapůvodně
byl	být	k5eAaImAgInS	být
dobrý	dobrý	k2eAgInSc1d1	dobrý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
se	se	k3xPyFc4	se
někdo	někdo	k3yInSc1	někdo
naučí	naučit	k5eAaPmIp3nS	naučit
žít	žít	k5eAaImF	žít
špatně	špatně	k6eAd1	špatně
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
už	už	k6eAd1	už
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nenapraví	napravit	k5eNaPmIp3nS	napravit
<g/>
.	.	kIx.	.
</s>
<s>
Svědomí	svědomí	k1gNnSc1	svědomí
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
niternou	niterný	k2eAgFnSc7d1	niterná
záležitostí	záležitost	k1gFnSc7	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
při	při	k7c6	při
společenské	společenský	k2eAgFnSc6d1	společenská
praxi	praxe	k1gFnSc6	praxe
není	být	k5eNaImIp3nS	být
do	do	k7c2	do
člověka	člověk	k1gMnSc2	člověk
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
daleko	daleko	k6eAd1	daleko
hůře	zle	k6eAd2	zle
posuzován	posuzovat	k5eAaImNgInS	posuzovat
čin	čin	k1gInSc1	čin
vykonaný	vykonaný	k2eAgInSc1d1	vykonaný
<g/>
,	,	kIx,	,
než	než	k8xS	než
pouhé	pouhý	k2eAgNnSc1d1	pouhé
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
právě	právě	k9	právě
ta	ten	k3xDgFnSc1	ten
myšlenka	myšlenka	k1gFnSc1	myšlenka
je	být	k5eAaImIp3nS	být
základ	základ	k1gInSc4	základ
špatného	špatný	k2eAgInSc2d1	špatný
činu	čin	k1gInSc2	čin
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
horší	zlý	k2eAgFnSc1d2	horší
než	než	k8xS	než
čin	čin	k1gInSc1	čin
samotný	samotný	k2eAgInSc1d1	samotný
<g/>
.	.	kIx.	.
</s>
<s>
Thompson	Thompson	k1gMnSc1	Thompson
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Přehled	přehled	k1gInSc4	přehled
etiky	etika	k1gFnSc2	etika
<g/>
,	,	kIx,	,
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
Moravec	Moravec	k1gMnSc1	Moravec
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Etika	etika	k1gFnSc1	etika
a	a	k8xC	a
česká	český	k2eAgNnPc1d1	české
média	médium	k1gNnPc1	médium
(	(	kIx(	(
<g/>
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
ve	v	k7c6	v
sborníku	sborník	k1gInSc6	sborník
10	[number]	k4	10
let	léto	k1gNnPc2	léto
v	v	k7c6	v
českých	český	k2eAgNnPc6d1	české
médiích	médium	k1gNnPc6	médium
<g/>
,	,	kIx,	,
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Anzenbacher	Anzenbachra	k1gFnPc2	Anzenbachra
<g/>
,	,	kIx,	,
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
etiky	etika	k1gFnSc2	etika
<g/>
,	,	kIx,	,
Zvon	zvon	k1gInSc1	zvon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1994	[number]	k4	1994
Fletcher	Fletchra	k1gFnPc2	Fletchra
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Situační	situační	k2eAgFnSc1d1	situační
etika	etika	k1gFnSc1	etika
<g/>
,	,	kIx,	,
Kalich	kalich	k1gInSc1	kalich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2009	[number]	k4	2009
HABÁŇ	HABÁŇ	kA	HABÁŇ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Metoděj	Metoděj	k1gMnSc1	Metoděj
<g/>
.	.	kIx.	.
</s>
<s>
Přirozená	přirozený	k2eAgFnSc1d1	přirozená
ethika	ethika	k1gFnSc1	ethika
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
:	:	kIx,	:
Krystal	krystal	k1gInSc1	krystal
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
160	[number]	k4	160
s.	s.	k?	s.
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
etika	etik	k1gMnSc2	etik
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
etika	etika	k1gFnSc1	etika
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gFnSc1	téma
Etika	etika	k1gFnSc1	etika
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Sirovátka	Sirovátka	k1gFnSc1	Sirovátka
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Etika	etika	k1gFnSc1	etika
<g/>
.	.	kIx.	.
</s>
<s>
Video	video	k1gNnSc1	video
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
Filosofie	filosofie	k1gFnSc2	filosofie
(	(	kIx(	(
<g/>
asi	asi	k9	asi
<g/>
)	)	kIx)	)
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
Viva	Viv	k2eAgFnSc1d1	Viva
Etika	etika	k1gFnSc1	etika
<g/>
,	,	kIx,	,
projekt	projekt	k1gInSc1	projekt
Transparency	Transparenca	k1gFnSc2	Transparenca
International	International	k1gFnSc2	International
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
pozastavený	pozastavený	k2eAgInSc1d1	pozastavený
<g/>
)	)	kIx)	)
Fairness	Fairness	k1gInSc1	Fairness
Judgments	Judgments	k1gInSc1	Judgments
<g/>
:	:	kIx,	:
Genuine	Genuin	k1gInSc5	Genuin
Morality	moralita	k1gFnPc4	moralita
or	or	k?	or
Disguised	Disguised	k1gMnSc1	Disguised
Egoism	Egoism	k1gMnSc1	Egoism
<g/>
?	?	kIx.	?
</s>
<s>
Psychological	Psychologicat	k5eAaPmAgMnS	Psychologicat
Article	Article	k1gInSc4	Article
on	on	k3xPp3gMnSc1	on
Fairness	Fairness	k1gInSc1	Fairness
(	(	kIx(	(
<g/>
registration	registration	k1gInSc1	registration
required	required	k1gInSc1	required
<g/>
)	)	kIx)	)
</s>
