<s desamb="1">
Protože	protože	k8xS
měl	mít	k5eAaImAgInS
ale	ale	k9
málo	málo	k4c4
administračních	administrační	k2eAgFnPc2d1
zkušeností	zkušenost	k1gFnPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
pod	pod	k7c7
silným	silný	k2eAgInSc7d1
tlakem	tlak	k1gInSc7
kritiky	kritika	k1gFnSc2
ostatních	ostatní	k2eAgMnPc2d1
zaměstnanců	zaměstnanec	k1gMnPc2
kanceláře	kancelář	k1gFnSc2
i	i	k8xC
členů	člen	k1gMnPc2
asociace	asociace	k1gFnSc2
obecně	obecně	k6eAd1
<g/>
.	.	kIx.
</s>