<s>
Trevor	Trevor	k1gMnSc1
Steele	Steel	k1gInSc2
</s>
<s>
Trevor	Trevor	k1gMnSc1
Steele	Steel	k1gInSc2
Narození	narození	k1gNnSc2
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1940	#num#	k4
(	(	kIx(
<g/>
80	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Povolání	povolání	k1gNnPc2
</s>
<s>
esperantista	esperantista	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
překladatel	překladatel	k1gMnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
OSIEK	OSIEK	kA
award	award	k1gMnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
Funkce	funkce	k1gFnSc1
</s>
<s>
Director	Director	k1gInSc1
of	of	k?
UEA	UEA	kA
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Trevor	Trevor	k1gInSc1
Steele	Steel	k1gInSc2
(	(	kIx(
<g/>
*	*	kIx~
1940	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
australský	australský	k2eAgMnSc1d1
esperantista	esperantista	k1gMnSc1
<g/>
,	,	kIx,
učitel	učitel	k1gMnSc1
a	a	k8xC
esperantský	esperantský	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
píšící	píšící	k2eAgFnSc2d1
povídky	povídka	k1gFnSc2
<g/>
,	,	kIx,
novely	novela	k1gFnSc2
a	a	k8xC
romány	román	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
dílo	dílo	k1gNnSc1
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
ovlivněno	ovlivnit	k5eAaPmNgNnS
jeho	jeho	k3xOp3gFnPc7
zkušenostmi	zkušenost	k1gFnPc7
z	z	k7c2
cest	cesta	k1gFnPc2
mj.	mj.	kA
po	po	k7c6
Německu	Německo	k1gNnSc6
a	a	k8xC
východní	východní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
počátku	počátek	k1gInSc2
roku	rok	k1gInSc2
2002	#num#	k4
do	do	k7c2
dubna	duben	k1gInSc2
2004	#num#	k4
pracoval	pracovat	k5eAaImAgMnS
Trevor	Trevor	k1gMnSc1
Steele	Steel	k1gInSc2
jako	jako	k8xS,k8xC
generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
ústřední	ústřední	k2eAgFnSc2d1
kanceláře	kancelář	k1gFnSc2
Světového	světový	k2eAgInSc2d1
esperantského	esperantský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
v	v	k7c6
Rotterdamu	Rotterdam	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
měl	mít	k5eAaImAgInS
ale	ale	k9
málo	málo	k4c4
administračních	administrační	k2eAgFnPc2d1
zkušeností	zkušenost	k1gFnPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
pod	pod	k7c7
silným	silný	k2eAgInSc7d1
tlakem	tlak	k1gInSc7
kritiky	kritika	k1gFnSc2
ostatních	ostatní	k2eAgMnPc2d1
zaměstnanců	zaměstnanec	k1gMnPc2
kanceláře	kancelář	k1gFnSc2
i	i	k8xC
členů	člen	k1gMnPc2
asociace	asociace	k1gFnSc2
obecně	obecně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
2003	#num#	k4
podal	podat	k5eAaPmAgMnS
"	"	kIx"
<g/>
z	z	k7c2
osobních	osobní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
"	"	kIx"
demisi	demise	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Trevor	Trevor	k1gInSc1
Steele	Steel	k1gInSc2
je	být	k5eAaImIp3nS
také	také	k9
členem	člen	k1gMnSc7
Esperantské	esperantský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
</s>
<s>
Sed	sed	k1gInSc1
nur	nur	k?
fragmento	fragmento	k1gNnSc1
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Memori	Memori	k6eAd1
kaj	kát	k5eAaImRp2nS
forgesi	forgese	k1gFnSc3
(	(	kIx(
<g/>
noveloj	noveloj	k1gInSc1
el	ela	k1gFnPc2
la	la	k1gNnSc1
Norda	Norda	k1gMnSc1
Montaro	Montara	k1gFnSc5
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Apenaux	Apenaux	k1gInSc1
papilioj	papilioj	k1gInSc1
en	en	k?
Bergen-Belsen	Bergen-Belsen	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Remember	Remember	k1gMnSc1
and	and	k?
forget	forget	k1gMnSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Falantaj	Falantaj	k1gInSc1
muroj	muroj	k1gInSc1
kaj	kát	k5eAaImRp2nS
aliaj	aliaj	k1gMnSc1
rakontoj	rakontoj	k1gInSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
No	no	k9
Butterflies	Butterflies	k1gMnSc1
in	in	k?
Bergen-Belsen	Bergen-Belsen	k1gInSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Australia	Australia	k1gFnSc1
Felix	Felix	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Neniu	Nenius	k1gMnSc3
ajn	ajn	k?
papilio	papilio	k1gMnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
La	la	k1gNnSc4
fotoalbumo	fotoalbumo	k6eAd1
<g/>
:	:	kIx,
unua	unua	k6eAd1
volumo	volumo	k6eAd1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Diverskolore	Diverskolor	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prozajxoj	Prozajxoj	k1gInSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
La	la	k1gNnSc4
fotoalbumo	fotoalbumo	k6eAd1
<g/>
:	:	kIx,
dua	duo	k1gNnSc2
volumo	volumo	k6eAd1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kaj	kát	k5eAaImRp2nS
staros	starosa	k1gFnPc2
tre	tre	k?
alte	alt	k1gInSc5
<g/>
...	...	k?
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Trevor	Trevor	k1gInSc1
Steele	Steel	k1gInSc2
<g/>
:	:	kIx,
Patrick	Patrick	k1gMnSc1
White	Whit	k1gInSc5
-	-	kIx~
Život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
<g/>
,	,	kIx,
Mezinárodní	mezinárodní	k2eAgFnSc1d1
kongresová	kongresový	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
82	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světový	světový	k2eAgInSc1d1
esperantský	esperantský	k2eAgInSc1d1
kongres	kongres	k1gInSc1
1997	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
esperantu	esperanto	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
Díla	dílo	k1gNnPc1
týkající	týkající	k2eAgNnPc1d1
se	se	k3xPyFc4
autora	autor	k1gMnSc2
Trevor	Trevor	k1gMnSc1
Steele	Steel	k1gInSc2
v	v	k7c6
katalogu	katalog	k1gInSc6
Sbírky	sbírka	k1gFnSc2
plánových	plánový	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
a	a	k8xC
esperantského	esperantský	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
mzk	mzk	k?
<g/>
2013766060	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
130534722	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
4398	#num#	k4
7897	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nb	nb	k?
<g/>
99013259	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
47871656	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nb	lccn-nb	k1gInSc1
<g/>
99013259	#num#	k4
</s>
