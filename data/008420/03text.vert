<p>
<s>
Dominik	Dominik	k1gMnSc1	Dominik
Dán	Dán	k1gMnSc1	Dán
(	(	kIx(	(
<g/>
*	*	kIx~	*
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
spisovatel	spisovatel	k1gMnSc1	spisovatel
detektivních	detektivní	k2eAgInPc2d1	detektivní
románů	román	k1gInPc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Píše	psát	k5eAaImIp3nS	psát
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
autorsky	autorsky	k6eAd1	autorsky
podílel	podílet	k5eAaImAgMnS	podílet
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
seriálu	seriál	k1gInSc2	seriál
Kriminálka	kriminálka	k1gFnSc1	kriminálka
Staré	Stará	k1gFnSc2	Stará
Město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Identita	identita	k1gFnSc1	identita
autora	autor	k1gMnSc2	autor
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
autorově	autorův	k2eAgFnSc6d1	autorova
identitě	identita	k1gFnSc6	identita
se	se	k3xPyFc4	se
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
i	i	k9	i
mezi	mezi	k7c7	mezi
příznivci	příznivec	k1gMnPc7	příznivec
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
vedou	vést	k5eAaImIp3nP	vést
spory	spor	k1gInPc1	spor
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
konspirační	konspirační	k2eAgFnPc1d1	konspirační
teorie	teorie	k1gFnPc1	teorie
předpokládající	předpokládající	k2eAgFnPc1d1	předpokládající
nikoli	nikoli	k9	nikoli
autora	autor	k1gMnSc2	autor
jednoho	jeden	k4xCgMnSc4	jeden
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
několik	několik	k4yIc1	několik
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
vydal	vydat	k5eAaPmAgMnS	vydat
první	první	k4xOgFnSc4	první
desítku	desítka	k1gFnSc4	desítka
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
o	o	k7c4	o
něho	on	k3xPp3gMnSc4	on
média	médium	k1gNnPc1	médium
začala	začít	k5eAaPmAgNnP	začít
zajímat	zajímat	k5eAaImF	zajímat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
o	o	k7c4	o
úspěšného	úspěšný	k2eAgMnSc4d1	úspěšný
autora	autor	k1gMnSc4	autor
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
komunikovat	komunikovat	k5eAaImF	komunikovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
elektronických	elektronický	k2eAgNnPc2d1	elektronické
médií	médium	k1gNnPc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Neodhalil	odhalit	k5eNaPmAgMnS	odhalit
však	však	k9	však
svou	svůj	k3xOyFgFnSc4	svůj
totožnost	totožnost	k1gFnSc4	totožnost
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediné	k1gNnSc1	jediné
<g/>
,	,	kIx,	,
co	co	k9	co
takto	takto	k6eAd1	takto
vedenými	vedený	k2eAgInPc7d1	vedený
rozhovory	rozhovor	k1gInPc7	rozhovor
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
kriminalisty	kriminalista	k1gMnPc4	kriminalista
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
z	z	k7c2	z
profesionálního	profesionální	k2eAgInSc2d1	profesionální
pohledu	pohled	k1gInSc2	pohled
<g/>
,	,	kIx,	,
odhalit	odhalit	k5eAaPmF	odhalit
nehodlá	hodlat	k5eNaImIp3nS	hodlat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
identitu	identita	k1gFnSc4	identita
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
nejbližších	blízký	k2eAgMnPc2d3	nejbližší
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
<g/>
,	,	kIx,	,
zná	znát	k5eAaImIp3nS	znát
i	i	k9	i
několik	několik	k4yIc1	několik
novinářů	novinář	k1gMnPc2	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Nesprávný	správný	k2eNgInSc1d1	nesprávný
je	být	k5eAaImIp3nS	být
i	i	k9	i
rok	rok	k1gInSc4	rok
jeho	jeho	k3xOp3gNnSc2	jeho
narození	narození	k1gNnSc2	narození
uváděný	uváděný	k2eAgMnSc1d1	uváděný
na	na	k7c6	na
přebalech	přebal	k1gInPc6	přebal
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
knihách	kniha	k1gFnPc6	kniha
řeší	řešit	k5eAaImIp3nP	řešit
kriminální	kriminální	k2eAgInPc1d1	kriminální
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Našem	náš	k3xOp1gInSc6	náš
městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc1	tento
město	město	k1gNnSc1	město
není	být	k5eNaImIp3nS	být
nijak	nijak	k6eAd1	nijak
konkrétně	konkrétně	k6eAd1	konkrétně
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
knih	kniha	k1gFnPc2	kniha
je	být	k5eAaImIp3nS	být
zjevné	zjevný	k2eAgNnSc1d1	zjevné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
Bratislavu	Bratislava	k1gFnSc4	Bratislava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc7	jeho
dílo	dílo	k1gNnSc4	dílo
monotematické	monotematický	k2eAgFnSc2d1	Monotematická
a	a	k8xC	a
tyto	tento	k3xDgInPc1	tento
případy	případ	k1gInPc1	případ
vždy	vždy	k6eAd1	vždy
řeší	řešit	k5eAaImIp3nP	řešit
detektivové	detektiv	k1gMnPc1	detektiv
z	z	k7c2	z
oddělení	oddělení	k1gNnSc2	oddělení
vražd	vražda	k1gFnPc2	vražda
policejního	policejní	k2eAgInSc2d1	policejní
sboru	sbor	k1gInSc2	sbor
Našeho	náš	k3xOp1gNnSc2	náš
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
jeho	jeho	k3xOp3gFnSc1	jeho
kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
specifická	specifický	k2eAgFnSc1d1	specifická
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
psána	psát	k5eAaImNgFnS	psát
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
úhlu	úhel	k1gInSc2	úhel
pohledu	pohled	k1gInSc2	pohled
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
nepoužívá	používat	k5eNaImIp3nS	používat
stejnou	stejný	k2eAgFnSc4d1	stejná
šablonu	šablona	k1gFnSc4	šablona
a	a	k8xC	a
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
jeho	jeho	k3xOp3gFnPc4	jeho
knihy	kniha	k1gFnPc4	kniha
překvapivé	překvapivý	k2eAgFnPc4d1	překvapivá
a	a	k8xC	a
nedá	dát	k5eNaPmIp3nS	dát
se	se	k3xPyFc4	se
dopředu	dopředu	k6eAd1	dopředu
očekávat	očekávat	k5eAaImF	očekávat
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
bude	být	k5eAaImBp3nS	být
jejich	jejich	k3xOp3gInSc1	jejich
příběh	příběh	k1gInSc1	příběh
směřovat	směřovat	k5eAaImF	směřovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Knihy	kniha	k1gFnPc1	kniha
podle	podle	k7c2	podle
data	datum	k1gNnSc2	datum
vydání	vydání	k1gNnSc2	vydání
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
===	===	k?	===
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
Popol	Popol	k1gInSc4	Popol
všetkých	všetký	k1gMnPc2	všetký
zarovná	zarovnat	k5eAaPmIp3nS	zarovnat
</s>
</p>
<p>
<s>
kauza	kauza	k1gFnSc1	kauza
zavlečení	zavlečení	k1gNnSc2	zavlečení
Michala	Michal	k1gMnSc2	Michal
Kováče	Kováč	k1gMnSc2	Kováč
ml.	ml.	kA	ml.
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
</s>
</p>
<p>
<s>
vražda	vražda	k1gFnSc1	vražda
Roberta	Robert	k1gMnSc2	Robert
Remiáše-	Remiáše-	k1gMnSc2	Remiáše-
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Popel	popel	k1gInSc1	popel
všechny	všechen	k3xTgMnPc4	všechen
zarovná	zarovnat	k5eAaPmIp3nS	zarovnat
</s>
</p>
<p>
<s>
-	-	kIx~	-
vydáno	vydat	k5eAaPmNgNnS	vydat
slovenským	slovenský	k2eAgNnSc7d1	slovenské
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Slovart	Slovarta	k1gFnPc2	Slovarta
CZ	CZ	kA	CZ
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
elektronické	elektronický	k2eAgFnSc2d1	elektronická
knihy	kniha	k1gFnSc2	kniha
pod	pod	k7c4	pod
názven	názven	k2eAgInSc4d1	názven
Popel	popel	k1gInSc4	popel
všechny	všechen	k3xTgMnPc4	všechen
zarovná	zarovnat	k5eAaPmIp3nS	zarovnat
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
Nehanebné	hanebný	k2eNgFnPc4d1	hanebný
neviniatko	neviniatka	k1gFnSc5	neviniatka
</s>
</p>
<p>
<s>
případ	případ	k1gInSc1	případ
smrti	smrt	k1gFnSc2	smrt
mladé	mladý	k2eAgFnSc2d1	mladá
studentky-	studentky-	k?	studentky-
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Nestydaté	nestydatý	k2eAgNnSc1d1	nestydaté
neviňátko	neviňátko	k1gNnSc1	neviňátko
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
Beštia	Beštia	k1gFnSc1	Beštia
</s>
</p>
<p>
<s>
případ	případ	k1gInSc1	případ
sériového	sériový	k2eAgMnSc2d1	sériový
vraha	vrah	k1gMnSc2	vrah
Ondreje	Ondrej	k1gMnSc2	Ondrej
Riga-	Riga-	k1gMnSc2	Riga-
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
a	a	k8xC	a
2015	[number]	k4	2015
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Bestie	bestie	k1gFnSc2	bestie
</s>
</p>
<p>
<s>
-	-	kIx~	-
vydáno	vydat	k5eAaPmNgNnS	vydat
slovenským	slovenský	k2eAgNnSc7d1	slovenské
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Slovart	Slovarta	k1gFnPc2	Slovarta
CZ	CZ	kA	CZ
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
elektronické	elektronický	k2eAgFnSc2d1	elektronická
knihy	kniha	k1gFnSc2	kniha
pod	pod	k7c4	pod
názven	názven	k2eAgInSc4d1	názven
Bestie	bestie	k1gFnPc4	bestie
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
Sára	Sára	k1gFnSc1	Sára
</s>
</p>
<p>
<s>
případ	případ	k1gInSc1	případ
vraždy	vražda	k1gFnSc2	vražda
prominentního	prominentní	k2eAgMnSc2d1	prominentní
bankéře	bankéř	k1gMnSc2	bankéř
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
Cela	cela	k1gFnSc1	cela
číslo	číslo	k1gNnSc1	číslo
17	[number]	k4	17
</s>
</p>
<p>
<s>
případ	případ	k1gInSc1	případ
nespravedlivě	spravedlivě	k6eNd1	spravedlivě
odsouzeného-	odsouzeného-	k?	odsouzeného-
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
a	a	k8xC	a
2016	[number]	k4	2016
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Cela	cela	k1gFnSc1	cela
17	[number]	k4	17
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
Červený	Červený	k1gMnSc1	Červený
kapitán	kapitán	k1gMnSc1	kapitán
</s>
</p>
<p>
<s>
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
případu	případ	k1gInSc2	případ
otevřeného	otevřený	k2eAgInSc2d1	otevřený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
zavádějící	zavádějící	k2eAgFnSc1d1	zavádějící
však	však	k9	však
kriminalisty	kriminalista	k1gMnPc4	kriminalista
do	do	k7c2	do
událostí	událost	k1gFnPc2	událost
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1989-	[number]	k4	1989-
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Kapitán	kapitán	k1gMnSc1	kapitán
Smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jako	jako	k8xC	jako
Rudý	rudý	k1gMnSc1	rudý
kapitán	kapitán	k1gMnSc1	kapitán
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
-	-	kIx~	-
vydáno	vydat	k5eAaPmNgNnS	vydat
slovenským	slovenský	k2eAgNnSc7d1	slovenské
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Slovart	Slovarta	k1gFnPc2	Slovarta
CZ	CZ	kA	CZ
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
elektronické	elektronický	k2eAgFnSc2d1	elektronická
knihy	kniha	k1gFnSc2	kniha
pod	pod	k7c4	pod
názven	názven	k2eAgInSc4d1	názven
Rudý	rudý	k2eAgInSc4d1	rudý
kapitán	kapitán	k1gMnSc1	kapitán
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
Hriech	Hriech	k1gInSc1	Hriech
náš	náš	k3xOp1gInSc4	náš
každodenný	každodenný	k2eAgInSc4d1	každodenný
</s>
</p>
<p>
<s>
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
případu	případ	k1gInSc2	případ
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
utýraného	utýraný	k2eAgMnSc2d1	utýraný
chlapce	chlapec	k1gMnSc2	chlapec
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
Knieža	Kniež	k1gInSc2	Kniež
Smrť	smrtit	k5eAaImRp2nS	smrtit
</s>
</p>
<p>
<s>
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
smrti	smrt	k1gFnSc2	smrt
stavebního	stavební	k2eAgMnSc2d1	stavební
podnikatele	podnikatel	k1gMnSc2	podnikatel
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
Noc	noc	k1gFnSc1	noc
temných	temný	k2eAgFnPc2d1	temná
klamstiev	klamstiva	k1gFnPc2	klamstiva
</s>
</p>
<p>
<s>
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
zavede	zavést	k5eAaPmIp3nS	zavést
kriminalisty	kriminalista	k1gMnPc4	kriminalista
do	do	k7c2	do
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
politických	politický	k2eAgFnPc2d1	politická
kruhů-	kruhů-	k?	kruhů-
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Noc	noc	k1gFnSc1	noc
plná	plný	k2eAgFnSc1d1	plná
lží	lež	k1gFnSc7	lež
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
Mucha	Mucha	k1gMnSc1	Mucha
</s>
</p>
<p>
<s>
případ	případ	k1gInSc1	případ
nezvěstného	zvěstný	k2eNgNnSc2d1	nezvěstné
mladého	mladý	k2eAgNnSc2d1	mladé
děvčete	děvče	k1gNnSc2	děvče
</s>
</p>
<p>
<s>
začátek	začátek	k1gInSc1	začátek
příběhu	příběh	k1gInSc2	příběh
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
vražd	vražda	k1gFnPc2	vražda
masového	masový	k2eAgMnSc2d1	masový
vraha	vrah	k1gMnSc2	vrah
Jozefa	Jozef	k1gMnSc2	Jozef
Slováka-	Slováka-	k1gMnSc2	Slováka-
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Moucha	Moucha	k1gMnSc1	Moucha
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
Studňa	Studň	k1gInSc2	Studň
</s>
</p>
<p>
<s>
kauza	kauza	k1gFnSc1	kauza
podvodu	podvod	k1gInSc2	podvod
s	s	k7c7	s
pozemky	pozemek	k1gInPc7	pozemek
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
Mucholapka	mucholapka	k1gFnSc1	mucholapka
</s>
</p>
<p>
<s>
případ	případ	k1gInSc1	případ
masového	masový	k2eAgMnSc2d1	masový
vraha	vrah	k1gMnSc2	vrah
Jozefa	Jozef	k1gMnSc2	Jozef
Slováka	Slovák	k1gMnSc2	Slovák
<g/>
,	,	kIx,	,
přímé	přímý	k2eAgNnSc4d1	přímé
pokračování	pokračování	k1gNnSc4	pokračování
knihy	kniha	k1gFnSc2	kniha
Mucha-	Mucha-	k1gFnSc2	Mucha-
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Mucholapka	mucholapka	k1gFnSc1	mucholapka
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
Na	na	k7c4	na
podpätkoch	podpätkoch	k1gInSc4	podpätkoch
</s>
</p>
<p>
<s>
vyrovnávání	vyrovnávání	k1gNnSc1	vyrovnávání
účtů	účet	k1gInPc2	účet
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
mafie	mafie	k1gFnSc2	mafie
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
Žiješ	žít	k5eAaImIp2nS	žít
iba	iba	k6eAd1	iba
dvakrát	dvakrát	k6eAd1	dvakrát
</s>
</p>
<p>
<s>
volné	volný	k2eAgNnSc4d1	volné
pokračování	pokračování	k1gNnSc4	pokračování
knihy	kniha	k1gFnSc2	kniha
Cela	cela	k1gFnSc1	cela
číslo	číslo	k1gNnSc1	číslo
17	[number]	k4	17
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
Kožené	kožený	k2eAgFnPc4d1	kožená
srdce	srdce	k1gNnPc4	srdce
</s>
</p>
<p>
<s>
případ	případ	k1gInSc1	případ
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
mající	mající	k2eAgInSc1d1	mající
přesah	přesah	k1gInSc1	přesah
zpět	zpět	k6eAd1	zpět
až	až	k9	až
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
Uzol	Uzol	k1gInSc1	Uzol
</s>
</p>
<p>
<s>
případ	případ	k1gInSc1	případ
vražd	vražda	k1gFnPc2	vražda
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
konce	konec	k1gInSc2	konec
socialismu-	socialismu-	k?	socialismu-
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Uzel	uzel	k1gInSc1	uzel
</s>
</p>
<p>
<s>
-	-	kIx~	-
vydáno	vydat	k5eAaPmNgNnS	vydat
slovenským	slovenský	k2eAgNnSc7d1	slovenské
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Slovart	Slovarta	k1gFnPc2	Slovarta
CZ	CZ	kA	CZ
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
elektronické	elektronický	k2eAgFnSc2d1	elektronická
knihy	kniha	k1gFnSc2	kniha
pod	pod	k7c4	pod
názven	názven	k2eAgInSc4d1	názven
Uzel	uzel	k1gInSc4	uzel
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
Kráska	kráska	k1gFnSc1	kráska
a	a	k8xC	a
netvor	netvor	k1gMnSc1	netvor
</s>
</p>
<p>
<s>
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
brutální	brutální	k2eAgFnSc2d1	brutální
vraždy	vražda	k1gFnSc2	vražda
studentky	studentka	k1gFnSc2	studentka
a	a	k8xC	a
pokusu	pokus	k1gInSc2	pokus
o	o	k7c4	o
vraždu	vražda	k1gFnSc4	vražda
batolete-	batolete-	k?	batolete-
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Kráska	kráska	k1gFnSc1	kráska
a	a	k8xC	a
zvíře	zvíře	k1gNnSc1	zvíře
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
Básnik	Básnik	k1gInSc1	Básnik
</s>
</p>
<p>
<s>
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
vražd	vražda	k1gFnPc2	vražda
mladých	mladý	k2eAgFnPc2d1	mladá
dívek-	dívek-	k?	dívek-
vydáno	vydat	k5eAaPmNgNnS	vydat
slovenským	slovenský	k2eAgNnSc7d1	slovenské
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Slovart	Slovarta	k1gFnPc2	Slovarta
CZ	CZ	kA	CZ
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
elektronické	elektronický	k2eAgFnSc2d1	elektronická
knihy	kniha	k1gFnSc2	kniha
pod	pod	k7c4	pod
názven	názven	k2eAgInSc4d1	názven
Básník	básník	k1gMnSc1	básník
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
Nevinným	vinný	k2eNgMnSc7d1	nevinný
sa	sa	k?	sa
neodpúšťa	odpúšťa	k6eNd1	odpúšťa
</s>
</p>
<p>
<s>
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
přirozená	přirozený	k2eAgFnSc1d1	přirozená
smrt	smrt	k1gFnSc1	smrt
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
vraždou	vražda	k1gFnSc7	vražda
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
Jednou	jeden	k4xCgFnSc7	jeden
nohou	noha	k1gFnSc7	noha
v	v	k7c6	v
hrobe	hrob	k1gInSc5	hrob
</s>
</p>
<p>
<s>
pátrání	pátrání	k1gNnSc1	pátrání
po	po	k7c6	po
identitě	identita	k1gFnSc6	identita
a	a	k8xC	a
osudu	osud	k1gInSc6	osud
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
noha	noha	k1gFnSc1	noha
je	být	k5eAaImIp3nS	být
nalezena	naleznout	k5eAaPmNgFnS	naleznout
na	na	k7c6	na
skládce	skládka	k1gFnSc6	skládka
odpadu	odpad	k1gInSc2	odpad
</s>
</p>
<p>
<s>
pokračování	pokračování	k1gNnSc1	pokračování
knihy	kniha	k1gFnSc2	kniha
Na	na	k7c4	na
podpätkoch	podpätkoch	k1gInSc4	podpätkoch
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
Krv	krvit	k5eAaImRp2nS	krvit
nie	nie	k?	nie
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
</s>
</p>
<p>
<s>
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
upálení	upálení	k1gNnSc2	upálení
mladé	mladý	k2eAgFnSc2d1	mladá
ženy	žena	k1gFnSc2	žena
a	a	k8xC	a
jejího	její	k3xOp3gInSc2	její
dítěte-	dítěte-	k?	dítěte-
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Krev	krev	k1gFnSc4	krev
není	být	k5eNaImIp3nS	být
voda	voda	k1gFnSc1	voda
</s>
</p>
<p>
<s>
-	-	kIx~	-
vydáno	vydat	k5eAaPmNgNnS	vydat
slovenským	slovenský	k2eAgNnSc7d1	slovenské
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Slovart	Slovarta	k1gFnPc2	Slovarta
CZ	CZ	kA	CZ
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
elektronické	elektronický	k2eAgFnSc2d1	elektronická
knihy	kniha	k1gFnSc2	kniha
pod	pod	k7c7	pod
názven	názven	k2eAgInSc1d1	názven
Krev	krev	k1gFnSc4	krev
není	být	k5eNaImIp3nS	být
voda	voda	k1gFnSc1	voda
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
Nežná	Nežný	k2eAgFnSc1d1	Nežný
fatamorgána	fatamorgána	k1gFnSc1	fatamorgána
</s>
</p>
<p>
<s>
případ	případ	k1gInSc1	případ
znásilněni	znásilnit	k5eAaPmNgMnP	znásilnit
a	a	k8xC	a
zardoušení	zardoušení	k1gNnSc1	zardoušení
mladých	mladý	k2eAgFnPc2d1	mladá
dívek	dívka	k1gFnPc2	dívka
</s>
</p>
<p>
<s>
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
knihy	kniha	k1gFnPc4	kniha
Uzol	Uzola	k1gFnPc2	Uzola
a	a	k8xC	a
Básnik-	Básnik-	k1gFnPc2	Básnik-
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Něžná	něžný	k2eAgNnPc4d1	něžné
fata	fatum	k1gNnPc4	fatum
morgána	morgána	k1gFnSc1	morgána
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
Smrť	smrtit	k5eAaImRp2nS	smrtit
na	na	k7c4	na
druhom	druhom	k1gInSc4	druhom
brehu	breh	k1gInSc2	breh
</s>
</p>
<p>
<s>
pátrání	pátrání	k1gNnSc1	pátrání
po	po	k7c6	po
vrahovi	vrah	k1gMnSc6	vrah
muže	muž	k1gMnSc4	muž
nalezeného	nalezený	k2eAgMnSc4d1	nalezený
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
po	po	k7c6	po
ztracené	ztracený	k2eAgFnSc6d1	ztracená
mladé	mladý	k2eAgFnSc3d1	mladá
ženě	žena	k1gFnSc3	žena
a	a	k8xC	a
donašeči	donašeč	k1gMnSc3	donašeč
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
policie-	policie-	k?	policie-
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Smrt	smrt	k1gFnSc1	smrt
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
břehu	břeh	k1gInSc6	břeh
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
Korene	Koren	k1gInSc5	Koren
zla	zlo	k1gNnSc2	zlo
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
Cigaretka	cigaretka	k1gFnSc1	cigaretka
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
ťahy	ťahy	k1gInPc4	ťahy
</s>
</p>
<p>
<s>
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
vraždy	vražda	k1gFnSc2	vražda
řidiče	řidič	k1gMnSc2	řidič
dodávky	dodávka	k1gFnSc2	dodávka
<g/>
.	.	kIx.	.
</s>
<s>
Trestná	trestný	k2eAgFnSc1d1	trestná
činnost	činnost	k1gFnSc1	činnost
příslušníků	příslušník	k1gMnPc2	příslušník
policie	policie	k1gFnSc2	policie
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
List	list	k1gInSc1	list
zo	zo	k?	zo
záhrobia	záhrobia	k1gFnSc1	záhrobia
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
Venuša	Venuša	k1gFnSc1	Venuša
zo	zo	k?	zo
zátoky	zátoka	k1gFnSc2	zátoka
</s>
</p>
<p>
<s>
nález	nález	k1gInSc1	nález
torz	torzo	k1gNnPc2	torzo
dvou	dva	k4xCgNnPc2	dva
ženský	ženský	k2eAgInSc4d1	ženský
těl	tělo	k1gNnPc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
jejich	jejich	k3xOp3gFnSc2	jejich
smrti	smrt	k1gFnSc2	smrt
na	na	k7c4	na
pozadí	pozadí	k1gNnSc4	pozadí
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
a	a	k8xC	a
prostitucí	prostituce	k1gFnSc7	prostituce
</s>
</p>
<p>
<s>
===	===	k?	===
Knihy	kniha	k1gFnPc4	kniha
seřazené	seřazený	k2eAgFnPc4d1	seřazená
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
===	===	k?	===
</s>
</p>
<p>
<s>
Uzol	Uzol	k1gInSc1	Uzol
-	-	kIx~	-
březen	březen	k1gInSc1	březen
1988	[number]	k4	1988
</s>
</p>
<p>
<s>
Básnik	Básnik	k1gInSc1	Básnik
-	-	kIx~	-
březen	březen	k1gInSc1	březen
a	a	k8xC	a
duben	duben	k1gInSc1	duben
1988	[number]	k4	1988
</s>
</p>
<p>
<s>
Nežná	Nežný	k2eAgFnSc1d1	Nežný
fatamorgána	fatamorgána	k1gFnSc1	fatamorgána
–	–	k?	–
duben	duben	k1gInSc1	duben
1987	[number]	k4	1987
až	až	k9	až
listopad	listopad	k1gInSc1	listopad
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
List	list	k1gInSc1	list
zo	zo	k?	zo
záhrobia	záhrobia	k1gFnSc1	záhrobia
-	-	kIx~	-
jaro	jaro	k1gNnSc1	jaro
1990	[number]	k4	1990
</s>
</p>
<p>
<s>
Mucha	Mucha	k1gMnSc1	Mucha
-	-	kIx~	-
červenec	červenec	k1gInSc1	červenec
1990	[number]	k4	1990
</s>
</p>
<p>
<s>
Beštia	Beštia	k1gFnSc1	Beštia
-	-	kIx~	-
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
1990	[number]	k4	1990
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
Mucholapka	mucholapka	k1gFnSc1	mucholapka
-	-	kIx~	-
červenec	červenec	k1gInSc1	červenec
až	až	k8xS	až
prosinec	prosinec	k1gInSc1	prosinec
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
Červený	Červený	k1gMnSc1	Červený
kapitán	kapitán	k1gMnSc1	kapitán
-	-	kIx~	-
červen	červen	k1gInSc1	červen
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
Krv	krvit	k5eAaImRp2nS	krvit
nie	nie	k?	nie
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
-	-	kIx~	-
léto	léto	k1gNnSc1	léto
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
Smrť	smrtit	k5eAaImRp2nS	smrtit
na	na	k7c4	na
druhom	druhom	k1gInSc4	druhom
brehu	breh	k1gInSc2	breh
-	-	kIx~	-
jaro	jaro	k1gNnSc4	jaro
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
Korene	Koren	k1gMnSc5	Koren
zla	zlo	k1gNnSc2	zlo
-	-	kIx~	-
jaro	jaro	k1gNnSc4	jaro
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
Cigaretka	cigaretka	k1gFnSc1	cigaretka
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
ťahy	ťahy	k1gInPc4	ťahy
-	-	kIx~	-
jaro	jaro	k6eAd1	jaro
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
Popol	Popol	k1gInSc1	Popol
všetkých	všetký	k1gMnPc2	všetký
zarovná	zarovnat	k5eAaPmIp3nS	zarovnat
-	-	kIx~	-
podzim	podzim	k1gInSc4	podzim
1996	[number]	k4	1996
</s>
</p>
<p>
<s>
Nehanebné	hanebný	k2eNgNnSc1d1	hanebný
neviniatko	neviniatka	k1gFnSc5	neviniatka
-	-	kIx~	-
leden	leden	k1gInSc4	leden
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
Na	na	k7c4	na
podpätkoch	podpätkoch	k1gInSc4	podpätkoch
-	-	kIx~	-
únor	únor	k1gInSc4	únor
až	až	k9	až
květen	květen	k1gInSc4	květen
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
nohou	noha	k1gFnSc7	noha
v	v	k7c6	v
hrobě	hrob	k1gInSc6	hrob
-	-	kIx~	-
srpen	srpen	k1gInSc1	srpen
až	až	k8xS	až
listopad	listopad	k1gInSc1	listopad
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
Cela	cela	k1gFnSc1	cela
číslo	číslo	k1gNnSc1	číslo
17	[number]	k4	17
-	-	kIx~	-
prosinec	prosinec	k1gInSc1	prosinec
1998	[number]	k4	1998
až	až	k9	až
únor	únor	k1gInSc1	únor
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
Žiješ	žít	k5eAaImIp2nS	žít
iba	iba	k6eAd1	iba
dvakrát	dvakrát	k6eAd1	dvakrát
-	-	kIx~	-
únor	únor	k1gInSc1	únor
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
Nevinným	vinný	k2eNgMnSc7d1	nevinný
sa	sa	k?	sa
neodpúšťa	odpúšťa	k6eNd1	odpúšťa
–	–	k?	–
červenec	červenec	k1gInSc4	červenec
a	a	k8xC	a
srpen	srpen	k1gInSc4	srpen
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
Sára	Sára	k1gFnSc1	Sára
-	-	kIx~	-
únor	únor	k1gInSc1	únor
až	až	k8xS	až
duben	duben	k1gInSc1	duben
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
Hriech	Hriech	k1gInSc1	Hriech
náš	náš	k3xOp1gInSc4	náš
každodenný	každodenný	k2eAgInSc4d1	každodenný
-	-	kIx~	-
listopad	listopad	k1gInSc4	listopad
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
Studňa	Studňa	k1gFnSc1	Studňa
-	-	kIx~	-
srpen	srpen	k1gInSc1	srpen
2002	[number]	k4	2002
a	a	k8xC	a
květen	květen	k1gInSc4	květen
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
Knieža	Knieža	k1gMnSc1	Knieža
Smrť	smrtit	k5eAaImRp2nS	smrtit
-	-	kIx~	-
duben	duben	k1gInSc1	duben
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
Kožené	kožený	k2eAgNnSc1d1	kožené
srdce	srdce	k1gNnSc1	srdce
-	-	kIx~	-
červen	červen	k1gInSc1	červen
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
Kráska	kráska	k1gFnSc1	kráska
a	a	k8xC	a
netvor	netvora	k1gFnPc2	netvora
-	-	kIx~	-
září	září	k1gNnSc2	září
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Noc	noc	k1gFnSc1	noc
temných	temný	k2eAgFnPc2d1	temná
klamstiev	klamstiva	k1gFnPc2	klamstiva
-	-	kIx~	-
březen	březen	k1gInSc1	březen
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
Venuša	Venuša	k1gFnSc1	Venuša
zo	zo	k?	zo
zátoky	zátoka	k1gFnSc2	zátoka
-	-	kIx~	-
léto	léto	k1gNnSc1	léto
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
===	===	k?	===
Audioknihy	Audioknih	k1gInPc4	Audioknih
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Seriál	seriál	k1gInSc1	seriál
Kriminálka	kriminálka	k1gFnSc1	kriminálka
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Dominik	Dominik	k1gMnSc1	Dominik
Dán	Dán	k1gMnSc1	Dán
byl	být	k5eAaImAgMnS	být
autorem	autor	k1gMnSc7	autor
námětu	námět	k1gInSc2	námět
seriálu	seriál	k1gInSc2	seriál
Kriminálka	kriminálka	k1gFnSc1	kriminálka
Staré	Stará	k1gFnSc2	Stará
Město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Česko-slovenský	českolovenský	k2eAgInSc1d1	česko-slovenský
koprodukční	koprodukční	k2eAgInSc1d1	koprodukční
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
skutečnými	skutečný	k2eAgInPc7d1	skutečný
případy	případ	k1gInPc7	případ
nedávné	dávný	k2eNgFnSc2d1	nedávná
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Trigon	trigon	k1gInSc1	trigon
production	production	k1gInSc1	production
<g/>
,	,	kIx,	,
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
a	a	k8xC	a
Rozhlas	rozhlas	k1gInSc1	rozhlas
a	a	k8xC	a
televízia	televízia	k1gFnSc1	televízia
Slovenska	Slovensko	k1gNnSc2	Slovensko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
celkem	celkem	k6eAd1	celkem
o	o	k7c4	o
sedm	sedm	k4xCc4	sedm
dílů	díl	k1gInPc2	díl
detektivek	detektivka	k1gFnPc2	detektivka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
hráli	hrát	k5eAaImAgMnP	hrát
Zuzana	Zuzana	k1gFnSc1	Zuzana
Fialová	Fialová	k1gFnSc1	Fialová
a	a	k8xC	a
Matěj	Matěj	k1gMnSc1	Matěj
Hádek	hádek	k1gMnSc1	hádek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Film	film	k1gInSc1	film
Rudý	rudý	k1gMnSc1	rudý
kapitán	kapitán	k1gMnSc1	kapitán
==	==	k?	==
</s>
</p>
<p>
<s>
Detektivní	detektivní	k2eAgInSc1d1	detektivní
thriller	thriller	k1gInSc1	thriller
režiséra	režisér	k1gMnSc2	režisér
Michala	Michal	k1gMnSc2	Michal
Kollára	Kollár	k1gMnSc2	Kollár
odehrávající	odehrávající	k2eAgInSc4d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
v	v	k7c6	v
rozpadajícím	rozpadající	k2eAgInSc6d1	rozpadající
se	se	k3xPyFc4	se
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Předlohou	předloha	k1gFnSc7	předloha
filmu	film	k1gInSc2	film
byla	být	k5eAaImAgFnS	být
kniha	kniha	k1gFnSc1	kniha
Červený	Červený	k1gMnSc1	Červený
kapitán	kapitán	k1gMnSc1	kapitán
<g/>
.	.	kIx.	.
</s>
<s>
Natočen	natočen	k2eAgMnSc1d1	natočen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
v	v	k7c6	v
česko-polsko-slovenské	českoolskolovenský	k2eAgFnSc6d1	česko-polsko-slovenská
koprodukci	koprodukce	k1gFnSc6	koprodukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Dominik	Dominik	k1gMnSc1	Dominik
Dán	Dán	k1gMnSc1	Dán
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Dominik	Dominik	k1gMnSc1	Dominik
Dán	Dán	k1gMnSc1	Dán
</s>
</p>
<p>
<s>
Bojím	bát	k5eAaImIp1nS	bát
sa	sa	k?	sa
<g/>
,	,	kIx,	,
že	že	k8xS	že
niektorí	niektorí	k2eAgNnPc1d1	niektorí
neuveria	neuverium	k1gNnPc1	neuverium
<g/>
,	,	kIx,	,
Knižná	knižný	k2eAgFnSc1d1	Knižná
revue	revue	k1gFnSc1	revue
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
Kritický	kritický	k2eAgInSc1d1	kritický
rub	rub	k1gInSc1	rub
<g/>
(	(	kIx(	(
<g/>
r	r	kA	r
<g/>
)	)	kIx)	)
<g/>
ikon	ikona	k1gFnPc2	ikona
<g/>
.	.	kIx.	.
</s>
<s>
Textový	textový	k2eAgInSc1d1	textový
agregátor	agregátor	k1gInSc1	agregátor
Petra	Petr	k1gMnSc2	Petr
F.	F.	kA	F.
'	'	kIx"	'
<g/>
Ria	Ria	k1gMnSc2	Ria
Jílka	Jílek	k1gMnSc2	Jílek
-	-	kIx~	-
"	"	kIx"	"
<g/>
Popularita	popularita	k1gFnSc1	popularita
mi	já	k3xPp1nSc3	já
naozaj	naozaj	k1gInSc4	naozaj
nechýba	nechýb	k1gMnSc4	nechýb
<g/>
"	"	kIx"	"
-	-	kIx~	-
Rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Dominikom	Dominikom	k1gInSc4	Dominikom
Dánom	Dánom	k1gInSc4	Dánom
</s>
</p>
<p>
<s>
Dominik	Dominik	k1gMnSc1	Dominik
Dán	Dán	k1gMnSc1	Dán
o	o	k7c6	o
seriálu	seriál	k1gInSc6	seriál
Krinálka	Krinálek	k1gMnSc2	Krinálek
Staré	Staré	k2eAgInPc1d1	Staré
město	město	k1gNnSc4	město
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
</s>
</p>
<p>
<s>
Novinky	novinka	k1gFnPc1	novinka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
o	o	k7c6	o
Dominiku	Dominik	k1gMnSc6	Dominik
Dánovi	Dán	k1gMnSc6	Dán
a	a	k8xC	a
Rudém	rudý	k1gMnSc6	rudý
kapitánu	kapitán	k1gMnSc6	kapitán
</s>
</p>
