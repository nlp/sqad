<s>
Émile	Émile	k1gFnSc1	Émile
Zola	Zola	k1gFnSc1	Zola
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1840	[number]	k4	1840
Paříž	Paříž	k1gFnSc1	Paříž
-	-	kIx~	-
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1902	[number]	k4	1902
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
naturalismu	naturalismus	k1gInSc2	naturalismus
<g/>
.	.	kIx.	.
</s>
<s>
Émile	Émile	k1gNnSc1	Émile
Zola	Zola	k1gMnSc1	Zola
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
François	François	k1gFnSc1	François
Zola	Zola	k1gFnSc1	Zola
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Francesco	Francesco	k1gMnSc1	Francesco
Zola	Zola	k1gMnSc1	Zola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
narozen	narozen	k2eAgMnSc1d1	narozen
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
roku	rok	k1gInSc2	rok
1795	[number]	k4	1795
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
Émilie	Émilie	k1gFnSc1	Émilie
Aubert	Aubert	k1gMnSc1	Aubert
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
francouzka	francouzka	k1gFnSc1	francouzka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Émilovy	Émilův	k2eAgFnPc1d1	Émilův
byly	být	k5eAaImAgFnP	být
3	[number]	k4	3
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
se	se	k3xPyFc4	se
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
Zolův	Zolův	k2eAgMnSc1d1	Zolův
otec	otec	k1gMnSc1	otec
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
zavodňovacího	zavodňovací	k2eAgInSc2d1	zavodňovací
kanálu	kanál	k1gInSc2	kanál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
svést	svést	k5eAaPmF	svést
říční	říční	k2eAgFnSc4d1	říční
vodu	voda	k1gFnSc4	voda
k	k	k7c3	k
městečku	městečko	k1gNnSc3	městečko
Aix-en-Provence	Aixn-Provence	k1gFnSc2	Aix-en-Provence
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
<g/>
,	,	kIx,	,
náhle	náhle	k6eAd1	náhle
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
se	se	k3xPyFc4	se
Zola	Zola	k1gMnSc1	Zola
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
kamarádem	kamarád	k1gMnSc7	kamarád
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
Paulem	Paul	k1gMnSc7	Paul
Cézannem	Cézann	k1gMnSc7	Cézann
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěšném	úspěšný	k2eNgNnSc6d1	neúspěšné
studiu	studio	k1gNnSc6	studio
odešel	odejít	k5eAaPmAgMnS	odejít
pracovat	pracovat	k5eAaImF	pracovat
do	do	k7c2	do
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Hachette	Hachett	k1gMnSc5	Hachett
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
okolo	okolo	k6eAd1	okolo
sebe	sebe	k3xPyFc4	sebe
soustředil	soustředit	k5eAaPmAgInS	soustředit
skupinu	skupina	k1gFnSc4	skupina
mladých	mladý	k2eAgMnPc2d1	mladý
romanopisců	romanopisec	k1gMnPc2	romanopisec
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
médanská	médanský	k2eAgFnSc1d1	médanský
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
pokrokovými	pokrokový	k2eAgInPc7d1	pokrokový
názory	názor	k1gInPc7	názor
měl	mít	k5eAaImAgInS	mít
velkou	velký	k2eAgFnSc4d1	velká
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c6	na
liberalizaci	liberalizace	k1gFnSc6	liberalizace
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgInS	angažovat
zejména	zejména	k9	zejména
v	v	k7c6	v
Dreyfusově	Dreyfusův	k2eAgFnSc6d1	Dreyfusova
aféře	aféra	k1gFnSc6	aféra
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
otevřeným	otevřený	k2eAgInSc7d1	otevřený
dopisem	dopis	k1gInSc7	dopis
prezidentu	prezident	k1gMnSc6	prezident
republiky	republika	k1gFnSc2	republika
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
J	J	kA	J
<g/>
'	'	kIx"	'
<g/>
accuse	accuse	k1gFnSc1	accuse
(	(	kIx(	(
<g/>
Žaluji	žalovat	k5eAaImIp1nS	žalovat
<g/>
)	)	kIx)	)
hájil	hájit	k5eAaImAgMnS	hájit
nevinu	nevina	k1gFnSc4	nevina
kapitána	kapitán	k1gMnSc2	kapitán
Alfreda	Alfred	k1gMnSc2	Alfred
Dreyfuse	Dreyfuse	k1gFnSc2	Dreyfuse
<g/>
,	,	kIx,	,
neprávem	neprávo	k1gNnSc7	neprávo
odsouzeného	odsouzený	k1gMnSc2	odsouzený
za	za	k7c4	za
špionáž	špionáž	k1gFnSc4	špionáž
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
údajně	údajně	k6eAd1	údajně
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
náhodou	náhoda	k1gFnSc7	náhoda
na	na	k7c4	na
otravu	otrava	k1gFnSc4	otrava
oxidem	oxid	k1gInSc7	oxid
uhelnatým	uhelnatý	k2eAgMnSc7d1	uhelnatý
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ucpal	ucpat	k5eAaPmAgInS	ucpat
komín	komín	k1gInSc1	komín
u	u	k7c2	u
kamen	kamna	k1gNnPc2	kamna
(	(	kIx(	(
<g/>
záměrné	záměrný	k2eAgNnSc1d1	záměrné
ucpání	ucpání	k1gNnSc1	ucpání
komínu	komín	k1gInSc2	komín
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
předmětem	předmět	k1gInSc7	předmět
hypotéz	hypotéza	k1gFnPc2	hypotéza
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
nikdy	nikdy	k6eAd1	nikdy
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
hřbitově	hřbitov	k1gInSc6	hřbitov
Montmartre	Montmartr	k1gMnSc5	Montmartr
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
zařadila	zařadit	k5eAaPmAgFnS	zařadit
veškeré	veškerý	k3xTgInPc4	veškerý
spisy	spis	k1gInPc4	spis
Émila	Émil	k1gMnSc4	Émil
Zoly	Zola	k1gMnSc2	Zola
(	(	kIx(	(
<g/>
opera	opera	k1gFnSc1	opera
omnia	omnium	k1gNnSc2	omnium
<g/>
)	)	kIx)	)
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
psal	psát	k5eAaImAgMnS	psát
Zola	Zola	k1gMnSc1	Zola
kratší	krátký	k2eAgFnSc2d2	kratší
prózy	próza	k1gFnSc2	próza
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
ovlivněné	ovlivněný	k2eAgNnSc4d1	ovlivněné
romantismem	romantismus	k1gInSc7	romantismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c4	po
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
řídil	řídit	k5eAaImAgMnS	řídit
literární	literární	k2eAgFnSc4d1	literární
kroniku	kronika	k1gFnSc4	kronika
Evénement	Evénement	k1gMnSc1	Evénement
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
v	v	k7c6	v
ostrých	ostrý	k2eAgFnPc6d1	ostrá
kritikách	kritika	k1gFnPc6	kritika
napadal	napadat	k5eAaBmAgInS	napadat
řadu	řada	k1gFnSc4	řada
významných	významný	k2eAgFnPc2d1	významná
postav	postava	k1gFnPc2	postava
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
literárního	literární	k2eAgInSc2d1	literární
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
"	"	kIx"	"
<g/>
naturalistický	naturalistický	k2eAgInSc4d1	naturalistický
<g/>
"	"	kIx"	"
román	román	k1gInSc4	román
označil	označit	k5eAaPmAgMnS	označit
Zola	Zola	k1gMnSc1	Zola
již	jenž	k3xRgFnSc4	jenž
Paní	paní	k1gFnSc4	paní
Bovaryovou	Bovaryová	k1gFnSc4	Bovaryová
a	a	k8xC	a
za	za	k7c4	za
román	román	k1gInSc4	román
zásadního	zásadní	k2eAgInSc2d1	zásadní
významu	význam	k1gInSc2	význam
pak	pak	k6eAd1	pak
Germinii	Germinie	k1gFnSc4	Germinie
Lacerteuxovou	Lacerteuxový	k2eAgFnSc4d1	Lacerteuxový
bratří	bratr	k1gMnPc2	bratr
Goncourtů	Goncourt	k1gInPc2	Goncourt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
neúspěšných	úspěšný	k2eNgInPc6d1	neúspěšný
pokusech	pokus	k1gInPc6	pokus
se	se	k3xPyFc4	se
Zola	Zola	k1gMnSc1	Zola
prosadil	prosadit	k5eAaPmAgMnS	prosadit
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
románem	román	k1gInSc7	román
Tereza	Tereza	k1gFnSc1	Tereza
Raquinová	Raquinová	k1gFnSc1	Raquinová
-	-	kIx~	-
tragický	tragický	k2eAgInSc1d1	tragický
příběh	příběh	k1gInSc1	příběh
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
spojí	spojit	k5eAaPmIp3nS	spojit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
milencem	milenec	k1gMnSc7	milenec
proti	proti	k7c3	proti
manželovi	manžel	k1gMnSc3	manžel
a	a	k8xC	a
neváhá	váhat	k5eNaImIp3nS	váhat
jej	on	k3xPp3gMnSc4	on
zavraždit	zavraždit	k5eAaPmF	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Kruté	krutý	k2eAgFnPc4d1	krutá
výčitky	výčitka	k1gFnPc4	výčitka
svědomí	svědomí	k1gNnSc2	svědomí
a	a	k8xC	a
neutěšené	utěšený	k2eNgNnSc4d1	neutěšené
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
žijí	žít	k5eAaImIp3nP	žít
<g/>
,	,	kIx,	,
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
učiní	učinit	k5eAaPmIp3nS	učinit
život	život	k1gInSc4	život
nesnesitelným	snesitelný	k2eNgInPc3d1	nesnesitelný
<g/>
.	.	kIx.	.
</s>
<s>
Zola	Zola	k6eAd1	Zola
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
definitivně	definitivně	k6eAd1	definitivně
opouští	opouštět	k5eAaImIp3nS	opouštět
romantismus	romantismus	k1gInSc1	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Nalezneme	naleznout	k5eAaPmIp1nP	naleznout
zde	zde	k6eAd1	zde
i	i	k9	i
všechny	všechen	k3xTgInPc1	všechen
prvky	prvek	k1gInPc1	prvek
jeho	jeho	k3xOp3gFnSc2	jeho
naturalistické	naturalistický	k2eAgFnSc2d1	naturalistická
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
později	pozdě	k6eAd2	pozdě
sepsal	sepsat	k5eAaPmAgMnS	sepsat
v	v	k7c6	v
teoretické	teoretický	k2eAgFnSc6d1	teoretická
práci	práce	k1gFnSc6	práce
Experimentální	experimentální	k2eAgInSc1d1	experimentální
román	román	k1gInSc1	román
Pravda	pravda	k1gFnSc1	pravda
-	-	kIx~	-
umělecké	umělecký	k2eAgNnSc1d1	umělecké
zpracování	zpracování	k1gNnSc1	zpracování
Dreyfusovy	Dreyfusův	k2eAgFnSc2d1	Dreyfusova
aféry	aféra	k1gFnSc2	aféra
Románový	románový	k2eAgInSc1d1	románový
cyklus	cyklus	k1gInSc1	cyklus
s	s	k7c7	s
proticírkevním	proticírkevní	k2eAgNnSc7d1	proticírkevní
zaměřením	zaměření	k1gNnSc7	zaměření
<g/>
:	:	kIx,	:
Lurdy	Lurda	k1gFnSc2	Lurda
Řím	Řím	k1gInSc1	Řím
Paříž	Paříž	k1gFnSc4	Paříž
Povídkový	povídkový	k2eAgInSc1d1	povídkový
soubor	soubor	k1gInSc1	soubor
<g/>
:	:	kIx,	:
Povídky	povídka	k1gFnPc1	povídka
o	o	k7c6	o
Ninoně	Ninon	k1gInSc6	Ninon
Nové	Nové	k2eAgFnSc2d1	Nové
povídky	povídka	k1gFnSc2	povídka
o	o	k7c6	o
Ninoně	Ninona	k1gFnSc6	Ninona
Cyklus	cyklus	k1gInSc1	cyklus
"	"	kIx"	"
<g/>
Evangelia	evangelium	k1gNnSc2	evangelium
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
:	:	kIx,	:
tato	tento	k3xDgNnPc1	tento
díla	dílo	k1gNnPc1	dílo
ovlivněna	ovlivněn	k2eAgFnSc1d1	ovlivněna
idejemi	idea	k1gFnPc7	idea
utopického	utopický	k2eAgInSc2d1	utopický
socialismu	socialismus	k1gInSc2	socialismus
Plodnost	plodnost	k1gFnSc1	plodnost
<g/>
,	,	kIx,	,
chvalozpěv	chvalozpěv	k1gInSc1	chvalozpěv
manželské	manželský	k2eAgFnSc2d1	manželská
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
rodiny	rodina	k1gFnSc2	rodina
Práce	práce	k1gFnSc1	práce
Pravda	pravda	k1gFnSc1	pravda
Spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
(	(	kIx(	(
<g/>
nedokončeno	dokončen	k2eNgNnSc1d1	nedokončeno
<g/>
)	)	kIx)	)
Zola	Zola	k1gFnSc1	Zola
je	být	k5eAaImIp3nS	být
také	také	k9	také
autorem	autor	k1gMnSc7	autor
sborníků	sborník	k1gInPc2	sborník
kritických	kritický	k2eAgFnPc2d1	kritická
statí	stať	k1gFnPc2	stať
o	o	k7c6	o
divadle	divadlo	k1gNnSc6	divadlo
a	a	k8xC	a
literatuře	literatura	k1gFnSc6	literatura
<g/>
:	:	kIx,	:
Naturalismus	naturalismus	k1gInSc1	naturalismus
a	a	k8xC	a
divadlo	divadlo	k1gNnSc1	divadlo
Naši	náš	k3xOp1gFnSc4	náš
dramatičtí	dramatický	k2eAgMnPc1d1	dramatický
autoři	autor	k1gMnPc1	autor
Republika	republika	k1gFnSc1	republika
a	a	k8xC	a
literatura	literatura	k1gFnSc1	literatura
Rougon-Macquartů	Rougon-Macquart	k1gMnPc2	Rougon-Macquart
[	[	kIx(	[
<g/>
rugon-makartů	rugonakart	k1gMnPc2	rugon-makart
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
cyklus	cyklus	k1gInSc1	cyklus
20	[number]	k4	20
románů	román	k1gInPc2	román
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
"	"	kIx"	"
<g/>
přírodopisná	přírodopisný	k2eAgFnSc1d1	přírodopisná
a	a	k8xC	a
sociální	sociální	k2eAgFnSc1d1	sociální
studie	studie	k1gFnSc1	studie
jedné	jeden	k4xCgFnSc2	jeden
rodiny	rodina	k1gFnSc2	rodina
za	za	k7c2	za
druhého	druhý	k4xOgNnSc2	druhý
císařství	císařství	k1gNnSc2	císařství
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Les	les	k1gInSc4	les
Rougon-Macquart	Rougon-Macquarta	k1gFnPc2	Rougon-Macquarta
<g/>
.	.	kIx.	.
</s>
<s>
Histoire	Histoir	k1gMnSc5	Histoir
naturelle	naturell	k1gMnSc5	naturell
et	et	k?	et
sociale	sociala	k1gFnSc6	sociala
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
une	une	k?	une
famille	famille	k1gInSc1	famille
sous	sous	k1gInSc1	sous
le	le	k?	le
Second	Second	k1gInSc1	Second
Empire	empir	k1gInSc5	empir
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
tímto	tento	k3xDgInSc7	tento
cyklem	cyklus	k1gInSc7	cyklus
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
jakousi	jakýsi	k3yIgFnSc4	jakýsi
obdobu	obdoba	k1gFnSc4	obdoba
Balzacovy	Balzacův	k2eAgFnSc2d1	Balzacova
Lidské	lidský	k2eAgFnSc2d1	lidská
komedie	komedie	k1gFnSc2	komedie
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
zde	zde	k6eAd1	zde
vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgFnPc1d1	hlavní
postavy	postava	k1gFnPc1	postava
obyčejné	obyčejný	k2eAgMnPc4d1	obyčejný
dělníky	dělník	k1gMnPc4	dělník
a	a	k8xC	a
lidi	člověk	k1gMnPc4	člověk
z	z	k7c2	z
periférie	periférie	k1gFnSc2	periférie
<g/>
.	.	kIx.	.
</s>
<s>
Potomci	potomek	k1gMnPc1	potomek
rodiny	rodina	k1gFnSc2	rodina
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
dělníky	dělník	k1gMnPc7	dělník
<g/>
,	,	kIx,	,
horníky	horník	k1gMnPc7	horník
<g/>
,	,	kIx,	,
kněžími	kněz	k1gMnPc7	kněz
<g/>
,	,	kIx,	,
vědci	vědec	k1gMnPc7	vědec
<g/>
,	,	kIx,	,
zemědělci	zemědělec	k1gMnPc7	zemědělec
<g/>
,	,	kIx,	,
vojáky	voják	k1gMnPc7	voják
<g/>
,	,	kIx,	,
podnikateli	podnikatel	k1gMnPc7	podnikatel
<g/>
,	,	kIx,	,
spekulanty	spekulant	k1gMnPc7	spekulant
<g/>
,	,	kIx,	,
živnostníky	živnostník	k1gMnPc7	živnostník
<g/>
,	,	kIx,	,
prostitutkami	prostitutka	k1gFnPc7	prostitutka
i	i	k8xC	i
politiky	politik	k1gMnPc7	politik
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnPc1	jejich
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
autor	autor	k1gMnSc1	autor
bez	bez	k7c2	bez
příkras	příkrasa	k1gFnPc2	příkrasa
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Určujícím	určující	k2eAgInSc7d1	určující
znakem	znak	k1gInSc7	znak
celého	celý	k2eAgInSc2d1	celý
cyklu	cyklus	k1gInSc2	cyklus
jsou	být	k5eAaImIp3nP	být
přechody	přechod	k1gInPc1	přechod
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
postav	postava	k1gFnPc2	postava
více	hodně	k6eAd2	hodně
díly	dílo	k1gNnPc7	dílo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
rodokmen	rodokmen	k1gInSc4	rodokmen
rodiny	rodina	k1gFnPc1	rodina
v	v	k7c6	v
externích	externí	k2eAgInPc6d1	externí
odkazech	odkaz	k1gInPc6	odkaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Štěstí	štěstí	k1gNnSc1	štěstí
Rougonů	Rougon	k1gInPc2	Rougon
(	(	kIx(	(
<g/>
La	la	k0	la
Fortune	Fortun	k1gMnSc5	Fortun
des	des	k1gNnPc1	des
Rougon	Rougon	k1gMnSc1	Rougon
<g/>
,	,	kIx,	,
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
román	román	k1gInSc1	román
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
Rougon-Macquartů	Rougon-Macquart	k1gInPc2	Rougon-Macquart
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
autorův	autorův	k2eAgInSc4d1	autorův
úvod	úvod	k1gInSc4	úvod
k	k	k7c3	k
celému	celý	k2eAgInSc3d1	celý
cyklu	cyklus	k1gInSc3	cyklus
Štvanice	Štvanice	k1gFnSc2	Štvanice
<g/>
/	/	kIx~	/
<g/>
Kořist	kořist	k1gFnSc1	kořist
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Curée	Curée	k1gFnPc2	Curée
<g/>
,	,	kIx,	,
1871	[number]	k4	1871
<g/>
-	-	kIx~	-
<g/>
72	[number]	k4	72
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
rozkoše	rozkoš	k1gFnPc4	rozkoš
a	a	k8xC	a
lesk	lesk	k1gInSc4	lesk
Paříže	Paříž	k1gFnSc2	Paříž
Břicho	břicho	k1gNnSc1	břicho
Paříže	Paříž	k1gFnSc2	Paříž
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc5	Le
Ventre	Ventr	k1gMnSc5	Ventr
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
tržnice	tržnice	k1gFnSc2	tržnice
Dobytí	dobytí	k1gNnSc1	dobytí
Plassansu	Plassans	k1gInSc2	Plassans
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Conquê	Conquê	k1gFnPc2	Conquê
de	de	k?	de
Plassans	Plassansa	k1gFnPc2	Plassansa
<g/>
,	,	kIx,	,
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
křivolaké	křivolaký	k2eAgFnSc2d1	křivolaká
cesty	cesta	k1gFnSc2	cesta
ctižádostivého	ctižádostivý	k2eAgMnSc2d1	ctižádostivý
kněze	kněz	k1gMnSc2	kněz
Hřích	hřích	k1gInSc4	hřích
abbého	abbé	k1gMnSc2	abbé
Moureta	Mouret	k1gMnSc2	Mouret
<g/>
/	/	kIx~	/
<g/>
Poklesek	poklesek	k1gInSc1	poklesek
<g />
.	.	kIx.	.
</s>
<s>
abbého	abbé	k1gMnSc4	abbé
Moureta	Mouret	k1gMnSc4	Mouret
(	(	kIx(	(
<g/>
La	la	k0	la
Faute	Faut	k1gMnSc5	Faut
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Abbé	abbé	k1gMnSc1	abbé
Mouret	Mouret	k1gMnSc1	Mouret
<g/>
,	,	kIx,	,
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
láska	láska	k1gFnSc1	láska
mladého	mladý	k2eAgMnSc2d1	mladý
kněze	kněz	k1gMnSc2	kněz
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
trpí	trpět	k5eAaImIp3nS	trpět
ztrátou	ztráta	k1gFnSc7	ztráta
paměti	paměť	k1gFnSc2	paměť
Jeho	jeho	k3xOp3gNnSc4	jeho
veličenstvo	veličenstvo	k1gNnSc4	veličenstvo
Eugè	Eugè	k1gMnSc1	Eugè
Rougon	Rougon	k1gMnSc1	Rougon
<g/>
/	/	kIx~	/
<g/>
Jeho	jeho	k3xOp3gFnSc1	jeho
excelence	excelence	k1gFnSc1	excelence
Eugene	Eugen	k1gInSc5	Eugen
Rougon	Rougon	k1gInSc1	Rougon
(	(	kIx(	(
<g/>
Son	son	k1gInSc1	son
Excellence	Excellence	k1gFnSc2	Excellence
Eugè	Eugè	k1gMnSc1	Eugè
Rougon	Rougon	k1gMnSc1	Rougon
<g/>
,	,	kIx,	,
1876	[number]	k4	1876
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Zabiják	zabiják	k1gMnSc1	zabiják
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Assommoir	Assommoir	k1gMnSc1	Assommoir
<g/>
,	,	kIx,	,
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
Lístek	lístek	k1gInSc1	lístek
lásky	láska	k1gFnSc2	láska
(	(	kIx(	(
<g/>
Une	Une	k1gMnSc1	Une
Page	Pag	k1gFnSc2	Pag
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
amour	amour	k1gMnSc1	amour
<g/>
,	,	kIx,	,
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
smyslná	smyslný	k2eAgFnSc1d1	smyslná
láska	láska	k1gFnSc1	láska
mladé	mladý	k2eAgFnSc2d1	mladá
dívky	dívka	k1gFnSc2	dívka
v	v	k7c6	v
příkrém	příkrý	k2eAgInSc6d1	příkrý
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
láskou	láska	k1gFnSc7	láska
k	k	k7c3	k
matce	matka	k1gFnSc3	matka
Nana	Nana	k1gMnSc1	Nana
(	(	kIx(	(
<g/>
Nana	Nana	k1gMnSc1	Nana
<g/>
,	,	kIx,	,
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
pokličkou	poklička	k1gFnSc7	poklička
<g/>
/	/	kIx~	/
<g/>
U	u	k7c2	u
rodinného	rodinný	k2eAgInSc2d1	rodinný
krbu	krb	k1gInSc2	krb
(	(	kIx(	(
<g/>
Pot-Bouille	Pot-Bouille	k1gInSc1	Pot-Bouille
<g/>
,	,	kIx,	,
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
téměř	téměř	k6eAd1	téměř
všude	všude	k6eAd1	všude
chybně	chybně	k6eAd1	chybně
uváděno	uvádět	k5eAaImNgNnS	uvádět
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Kořist	kořist	k1gFnSc1	kořist
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
"	"	kIx"	"
<g/>
La	la	k1gNnSc1	la
Curée	Curé	k1gInSc2	Curé
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
život	život	k1gInSc4	život
v	v	k7c6	v
hlučném	hlučný	k2eAgInSc6d1	hlučný
pařížském	pařížský	k2eAgInSc6d1	pařížský
<g />
.	.	kIx.	.
</s>
<s>
činžáku	činžák	k1gInSc3	činžák
U	u	k7c2	u
štěstí	štěstí	k1gNnSc2	štěstí
dam	dáma	k1gFnPc2	dáma
(	(	kIx(	(
<g/>
Au	au	k0	au
Bonheur	Bonheur	k1gMnSc1	Bonheur
des	des	k1gNnSc1	des
Dames	Dames	k1gMnSc1	Dames
<g/>
,	,	kIx,	,
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
životní	životní	k2eAgFnSc1d1	životní
dráha	dráha	k1gFnSc1	dráha
mladého	mladý	k2eAgNnSc2d1	mladé
a	a	k8xC	a
ctižádostivého	ctižádostivý	k2eAgMnSc2d1	ctižádostivý
obchodníka	obchodník	k1gMnSc2	obchodník
Radost	radost	k6eAd1	radost
ze	z	k7c2	z
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Joie	Joi	k1gInSc2	Joi
de	de	k?	de
vivre	vivr	k1gMnSc5	vivr
<g/>
,	,	kIx,	,
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
nikdy	nikdy	k6eAd1	nikdy
nevyšlo	vyjít	k5eNaPmAgNnS	vyjít
Germinal	Germinal	k1gFnSc2	Germinal
(	(	kIx(	(
<g/>
Germinal	Germinal	k1gMnSc1	Germinal
<g/>
,	,	kIx,	,
1885	[number]	k4	1885
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
horníků	horník	k1gMnPc2	horník
zobrazující	zobrazující	k2eAgInSc4d1	zobrazující
vyhrocený	vyhrocený	k2eAgInSc4d1	vyhrocený
konflikt	konflikt	k1gInSc4	konflikt
mezi	mezi	k7c7	mezi
dělníky	dělník	k1gMnPc7	dělník
a	a	k8xC	a
majiteli	majitel	k1gMnPc7	majitel
dolů	dolů	k6eAd1	dolů
Mistrovské	mistrovský	k2eAgNnSc1d1	mistrovské
dílo	dílo	k1gNnSc1	dílo
<g/>
/	/	kIx~	/
<g/>
Dílo	dílo	k1gNnSc1	dílo
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Œ	Œ	k?	Œ
<g/>
,	,	kIx,	,
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obraz	obraz	k1gInSc1	obraz
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
snažení	snažení	k1gNnSc2	snažení
Paříže	Paříž	k1gFnSc2	Paříž
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
La	la	k0	la
Terre	Terr	k1gMnSc5	Terr
<g/>
,	,	kIx,	,
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
velkolepá	velkolepý	k2eAgFnSc1d1	velkolepá
oslava	oslava	k1gFnSc1	oslava
země-dárkyně	zeměárkyně	k1gFnSc2	země-dárkyně
Růžové	růžový	k2eAgNnSc4d1	růžové
poupě	poupě	k1gNnSc4	poupě
<g/>
/	/	kIx~	/
<g/>
Sen	sen	k1gInSc1	sen
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
Rê	Rê	k1gMnSc1	Rê
<g/>
,	,	kIx,	,
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
Lidská	lidský	k2eAgFnSc1d1	lidská
bestie	bestie	k1gFnSc1	bestie
<g/>
/	/	kIx~	/
<g/>
Člověk	člověk	k1gMnSc1	člověk
bestie	bestie	k1gFnSc2	bestie
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Bê	Bê	k1gFnSc2	Bê
humaine	humainout	k5eAaPmIp3nS	humainout
<g/>
,	,	kIx,	,
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naturalistický	naturalistický	k2eAgInSc1d1	naturalistický
román	román	k1gInSc1	román
z	z	k7c2	z
železničního	železniční	k2eAgNnSc2d1	železniční
prostředí	prostředí	k1gNnSc2	prostředí
Peníze	peníz	k1gInSc2	peníz
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Argent	argentum	k1gNnPc2	argentum
<g/>
,	,	kIx,	,
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hrůza	hrůza	k1gFnSc1	hrůza
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
peníze	peníz	k1gInPc4	peníz
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
Rozvrat	rozvrat	k1gInSc4	rozvrat
(	(	kIx(	(
<g/>
La	la	k1gNnSc4	la
Débâcle	Débâcle	k1gFnSc2	Débâcle
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
scény	scéna	k1gFnSc2	scéna
z	z	k7c2	z
Prusko-Francouzské	pruskorancouzský	k2eAgFnSc2d1	prusko-francouzská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1870-71	[number]	k4	1870-71
Doktor	doktor	k1gMnSc1	doktor
Pascal	Pascal	k1gMnSc1	Pascal
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
Docteur	Docteur	k1gMnSc1	Docteur
Pascal	Pascal	k1gMnSc1	Pascal
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc4d1	poslední
román	román	k1gInSc4	román
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
Rougon-Macquartové	Rougon-Macquartová	k1gFnSc2	Rougon-Macquartová
<g/>
.	.	kIx.	.
</s>
