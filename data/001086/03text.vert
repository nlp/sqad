<s>
Franck	Franck	k1gMnSc1	Franck
Henry	Henry	k1gMnSc1	Henry
Pierre	Pierr	k1gInSc5	Pierr
Ribéry	Ribér	k1gMnPc4	Ribér
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1983	[number]	k4	1983
v	v	k7c4	v
Boulogne-sur-Mer	Boulogneur-Mer	k1gInSc4	Boulogne-sur-Mer
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
konverze	konverze	k1gFnSc2	konverze
k	k	k7c3	k
islámu	islám	k1gInSc3	islám
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
známý	známý	k1gMnSc1	známý
také	také	k9	také
jako	jako	k9	jako
Bilal	Bilal	k1gMnSc1	Bilal
Yusuf	Yusuf	k1gMnSc1	Yusuf
Mohammed	Mohammed	k1gMnSc1	Mohammed
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
záložník	záložník	k1gMnSc1	záložník
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
reprezentant	reprezentant	k1gMnSc1	reprezentant
<g/>
,	,	kIx,	,
hráč	hráč	k1gMnSc1	hráč
FC	FC	kA	FC
Bayern	Bayern	k1gInSc1	Bayern
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Novináři	novinář	k1gMnPc1	novinář
jej	on	k3xPp3gMnSc4	on
občas	občas	k6eAd1	občas
označovali	označovat	k5eAaImAgMnP	označovat
za	za	k7c4	za
nástupce	nástupce	k1gMnSc4	nástupce
Zinedina	Zinedin	k2eAgMnSc2d1	Zinedin
Zidana	Zidan	k1gMnSc2	Zidan
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
Franckovi	Francek	k1gMnSc3	Francek
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
vracel	vracet	k5eAaImAgMnS	vracet
se	se	k3xPyFc4	se
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
autem	auto	k1gNnSc7	auto
domů	dům	k1gInPc2	dům
z	z	k7c2	z
výletu	výlet	k1gInSc2	výlet
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
ztratil	ztratit	k5eAaPmAgMnS	ztratit
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
vozem	vůz	k1gInSc7	vůz
a	a	k8xC	a
havaroval	havarovat	k5eAaPmAgMnS	havarovat
<g/>
.	.	kIx.	.
</s>
<s>
Malý	Malý	k1gMnSc1	Malý
chlapec	chlapec	k1gMnSc1	chlapec
pak	pak	k6eAd1	pak
proletěl	proletět	k5eAaPmAgInS	proletět
předním	přední	k2eAgNnSc7d1	přední
sklem	sklo	k1gNnSc7	sklo
auta	auto	k1gNnSc2	auto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nosí	nosit	k5eAaImIp3nS	nosit
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
tváři	tvář	k1gFnSc6	tvář
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
jizvu	jizva	k1gFnSc4	jizva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gNnSc7	jeho
poznávacím	poznávací	k2eAgNnSc7d1	poznávací
znamením	znamení	k1gNnSc7	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jiný	jiný	k2eAgMnSc1d1	jiný
fotbalista	fotbalista	k1gMnSc1	fotbalista
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
vizáží	vizáž	k1gFnSc7	vizáž
<g/>
,	,	kIx,	,
argentinský	argentinský	k2eAgMnSc1d1	argentinský
útočník	útočník	k1gMnSc1	útočník
Carlos	Carlos	k1gMnSc1	Carlos
Tévez	Tévez	k1gMnSc1	Tévez
<g/>
,	,	kIx,	,
i	i	k8xC	i
Ribéry	Ribéra	k1gFnSc2	Ribéra
plastickou	plastický	k2eAgFnSc4d1	plastická
operaci	operace	k1gFnSc4	operace
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
za	za	k7c2	za
co	co	k3yRnSc1	co
bych	by	kYmCp1nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
stydět	stydět	k5eAaImF	stydět
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
stojí	stát	k5eAaImIp3nS	stát
si	se	k3xPyFc3	se
za	za	k7c7	za
svým	svůj	k3xOyFgMnSc7	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
byl	být	k5eAaImAgInS	být
vyšetřován	vyšetřovat	k5eAaImNgInS	vyšetřovat
za	za	k7c4	za
sex	sex	k1gInSc4	sex
s	s	k7c7	s
nezletilou	zletilý	k2eNgFnSc7d1	nezletilá
prostitutkou	prostitutka	k1gFnSc7	prostitutka
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
zproštěn	zproštěn	k2eAgInSc1d1	zproštěn
obvinění	obvinění	k1gNnSc3	obvinění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
podepsal	podepsat	k5eAaPmAgInS	podepsat
kontrakt	kontrakt	k1gInSc1	kontrakt
s	s	k7c7	s
tureckým	turecký	k2eAgNnSc7d1	turecké
Galatasarayem	Galatasarayum	k1gNnSc7	Galatasarayum
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
Pomohl	pomoct	k5eAaPmAgMnS	pomoct
mu	on	k3xPp3gMnSc3	on
k	k	k7c3	k
zisku	zisk	k1gInSc3	zisk
tureckého	turecký	k2eAgInSc2d1	turecký
poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c4	v
semifinále	semifinále	k1gNnSc4	semifinále
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
jedinou	jediný	k2eAgFnSc4d1	jediná
branku	branka	k1gFnSc4	branka
a	a	k8xC	a
pak	pak	k6eAd1	pak
dalším	další	k2eAgInSc7d1	další
gólem	gól	k1gInSc7	gól
přispěl	přispět	k5eAaPmAgInS	přispět
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
nad	nad	k7c7	nad
rivalem	rival	k1gMnSc7	rival
Fenerbahçe	Fenerbahç	k1gFnSc2	Fenerbahç
<g/>
.	.	kIx.	.
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1	Fanoušek
Galatasaraye	Galatasaray	k1gFnSc2	Galatasaray
byli	být	k5eAaImAgMnP	být
uchváceni	uchvátit	k5eAaPmNgMnP	uchvátit
jeho	jeho	k3xOp3gFnSc7	jeho
hrou	hra	k1gFnSc7	hra
a	a	k8xC	a
dali	dát	k5eAaPmAgMnP	dát
mu	on	k3xPp3gMnSc3	on
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
FerraRibery	FerraRibera	k1gFnSc2	FerraRibera
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
největší	veliký	k2eAgInSc4d3	veliký
objev	objev	k1gInSc4	objev
MS	MS	kA	MS
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
ale	ale	k8xC	ale
dlouho	dlouho	k6eAd1	dlouho
nevydržel	vydržet	k5eNaPmAgMnS	vydržet
jeho	jeho	k3xOp3gNnSc7	jeho
dalším	další	k2eAgNnSc7d1	další
působištěm	působiště	k1gNnSc7	působiště
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc1	jeho
rodná	rodný	k2eAgFnSc1d1	rodná
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Marseille	Marseille	k1gFnSc2	Marseille
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
neobešlo	obešnout	k5eNaPmAgNnS	obešnout
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Galatasaray	Galatasaray	k1gInPc4	Galatasaray
nakonec	nakonec	k6eAd1	nakonec
vysoudil	vysoudit	k5eAaPmAgInS	vysoudit
náhradu	náhrada	k1gFnSc4	náhrada
10	[number]	k4	10
miliónů	milión	k4xCgInPc2	milión
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
Olympique	Olympique	k1gNnSc6	Olympique
Marseille	Marseille	k1gFnSc2	Marseille
měl	mít	k5eAaImAgInS	mít
dvě	dva	k4xCgFnPc4	dva
úspěšné	úspěšný	k2eAgFnPc4d1	úspěšná
sezóny	sezóna	k1gFnPc4	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
s	s	k7c7	s
Marseille	Marseille	k1gFnPc7	Marseille
dokráčel	dokráčet	k5eAaPmAgMnS	dokráčet
až	až	k9	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
Poháru	pohár	k1gInSc2	pohár
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
tým	tým	k1gInSc1	tým
však	však	k9	však
ztroskotal	ztroskotat	k5eAaPmAgInS	ztroskotat
na	na	k7c6	na
Valencii	Valencie	k1gFnSc6	Valencie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ročníku	ročník	k1gInSc6	ročník
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
se	se	k3xPyFc4	se
probojoval	probojovat	k5eAaPmAgMnS	probojovat
s	s	k7c7	s
Bayernem	Bayern	k1gInSc7	Bayern
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
DFB	DFB	kA	DFB
Pokalu	Pokala	k1gFnSc4	Pokala
proti	proti	k7c3	proti
Borussii	Borussie	k1gFnSc3	Borussie
Dortmund	Dortmund	k1gInSc1	Dortmund
<g/>
,	,	kIx,	,
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
jeden	jeden	k4xCgInSc4	jeden
gól	gól	k1gInSc4	gól
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vítězství	vítězství	k1gNnSc4	vítězství
to	ten	k3xDgNnSc1	ten
nestačilo	stačit	k5eNaBmAgNnS	stačit
bavorský	bavorský	k2eAgInSc4d1	bavorský
klub	klub	k1gInSc4	klub
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
soupeři	soupeř	k1gMnSc3	soupeř
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
vsítil	vsítit	k5eAaPmAgMnS	vsítit
gól	gól	k1gInSc4	gól
proti	proti	k7c3	proti
Hannoveru	Hannover	k1gInSc3	Hannover
96	[number]	k4	96
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
vítězství	vítězství	k1gNnSc3	vítězství
Bayernu	Bayern	k1gInSc2	Bayern
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
při	při	k7c6	při
drtivé	drtivý	k2eAgFnSc6d1	drtivá
domácí	domácí	k2eAgFnSc6d1	domácí
výhře	výhra	k1gFnSc6	výhra
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
nad	nad	k7c7	nad
Werderem	Werder	k1gInSc7	Werder
Brémy	Brémy	k1gFnPc1	Brémy
<g/>
,	,	kIx,	,
v	v	k7c6	v
ligovém	ligový	k2eAgInSc6d1	ligový
zápase	zápas	k1gInSc6	zápas
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
jeden	jeden	k4xCgInSc4	jeden
gól	gól	k1gInSc4	gól
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
27	[number]	k4	27
<g/>
.	.	kIx.	.
bundesligovém	bundesligový	k2eAgNnSc6d1	bundesligové
kole	kolo	k1gNnSc6	kolo
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
gól	gól	k1gInSc4	gól
při	při	k7c6	při
kanonádě	kanonáda	k1gFnSc6	kanonáda
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
nad	nad	k7c7	nad
Hamburkem	Hamburk	k1gInSc7	Hamburk
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
gólem	gól	k1gInSc7	gól
na	na	k7c6	na
vysokém	vysoký	k2eAgNnSc6d1	vysoké
vítězství	vítězství	k1gNnSc6	vítězství
Bayernu	Bayern	k1gInSc2	Bayern
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
nad	nad	k7c7	nad
domácím	domácí	k2eAgInSc7d1	domácí
Hannoverem	Hannover	k1gInSc7	Hannover
96	[number]	k4	96
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
klubem	klub	k1gInSc7	klub
slavil	slavit	k5eAaImAgInS	slavit
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
zisk	zisk	k1gInSc1	zisk
ligového	ligový	k2eAgInSc2d1	ligový
titulu	titul	k1gInSc2	titul
již	již	k6eAd1	již
6	[number]	k4	6
kol	kolo	k1gNnPc2	kolo
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
ve	v	k7c6	v
28	[number]	k4	28
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
německé	německý	k2eAgFnSc2d1	německá
Bundesligy	bundesliga	k1gFnSc2	bundesliga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
kole	kolo	k1gNnSc6	kolo
Bundesligy	bundesliga	k1gFnSc2	bundesliga
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
dva	dva	k4xCgInPc4	dva
góly	gól	k1gInPc4	gól
proti	proti	k7c3	proti
Borussii	Borussie	k1gFnSc3	Borussie
Mönchengladbach	Mönchengladbach	k1gInSc1	Mönchengladbach
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
F	F	kA	F
Ligy	liga	k1gFnPc1	liga
mistrů	mistr	k1gMnPc2	mistr
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
mírnil	mírnit	k5eAaImAgMnS	mírnit
svým	svůj	k3xOyFgInSc7	svůj
gólem	gól	k1gInSc7	gól
překvapivou	překvapivý	k2eAgFnSc4d1	překvapivá
prohru	prohra	k1gFnSc4	prohra
Bayernu	Bayern	k1gInSc2	Bayern
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
s	s	k7c7	s
běloruským	běloruský	k2eAgNnSc7d1	běloruské
mužstvem	mužstvo	k1gNnSc7	mužstvo
BATE	BATE	kA	BATE
Borisov	Borisov	k1gInSc1	Borisov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
semifinále	semifinále	k1gNnSc2	semifinále
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
výhry	výhra	k1gFnSc2	výhra
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
nad	nad	k7c7	nad
Barcelonou	Barcelona	k1gFnSc7	Barcelona
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
dosud	dosud	k6eAd1	dosud
velmi	velmi	k6eAd1	velmi
suverénní	suverénní	k2eAgFnSc1d1	suverénní
<g/>
.	.	kIx.	.
</s>
<s>
Franck	Franck	k1gMnSc1	Franck
odehrál	odehrát	k5eAaPmAgMnS	odehrát
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnPc1	jeho
spoluhráči	spoluhráč	k1gMnPc1	spoluhráč
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgNnSc4d1	dobré
utkání	utkání	k1gNnSc4	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Bayern	Bayern	k1gInSc4	Bayern
si	se	k3xPyFc3	se
zajistil	zajistit	k5eAaPmAgMnS	zajistit
výbornou	výborný	k2eAgFnSc4d1	výborná
pozici	pozice	k1gFnSc4	pozice
do	do	k7c2	do
odvety	odveta	k1gFnSc2	odveta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odvetě	odveta	k1gFnSc6	odveta
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
zařídil	zařídit	k5eAaPmAgMnS	zařídit
dva	dva	k4xCgInPc4	dva
góly	gól	k1gInPc4	gól
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
si	se	k3xPyFc3	se
v	v	k7c6	v
72	[number]	k4	72
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
centru	centrum	k1gNnSc6	centrum
srazil	srazit	k5eAaPmAgInS	srazit
míč	míč	k1gInSc4	míč
do	do	k7c2	do
vlastní	vlastní	k2eAgFnSc2d1	vlastní
sítě	síť	k1gFnSc2	síť
obránce	obránce	k1gMnSc1	obránce
Barcelony	Barcelona	k1gFnSc2	Barcelona
Gerard	Gerard	k1gMnSc1	Gerard
Piqué	Piquá	k1gFnSc2	Piquá
a	a	k8xC	a
o	o	k7c4	o
čtyři	čtyři	k4xCgFnPc4	čtyři
minuty	minuta	k1gFnPc4	minuta
později	pozdě	k6eAd2	pozdě
přetavil	přetavit	k5eAaPmAgMnS	přetavit
jeho	jeho	k3xOp3gMnSc1	jeho
další	další	k2eAgMnSc1d1	další
centr	centr	k1gMnSc1	centr
hlavou	hlava	k1gFnSc7	hlava
v	v	k7c4	v
gól	gól	k1gInSc4	gól
Thomas	Thomas	k1gMnSc1	Thomas
Müller	Müller	k1gMnSc1	Müller
<g/>
.	.	kIx.	.
</s>
<s>
Bayern	Bayern	k1gInSc1	Bayern
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
na	na	k7c4	na
Camp	camp	k1gInSc4	camp
Nou	Nou	k1gFnPc4	Nou
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
a	a	k8xC	a
suverénním	suverénní	k2eAgInSc7d1	suverénní
způsobem	způsob	k1gInSc7	způsob
postoupil	postoupit	k5eAaPmAgMnS	postoupit
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
ve	v	k7c4	v
Wembley	Wemblea	k1gFnPc4	Wemblea
proti	proti	k7c3	proti
Borussii	Borussie	k1gFnSc3	Borussie
Dortmund	Dortmund	k1gInSc1	Dortmund
přihrál	přihrát	k5eAaPmAgInS	přihrát
v	v	k7c6	v
89	[number]	k4	89
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
patičkou	patička	k1gFnSc7	patička
mezi	mezi	k7c7	mezi
obránci	obránce	k1gMnPc7	obránce
na	na	k7c4	na
vítězný	vítězný	k2eAgInSc4d1	vítězný
gól	gól	k1gInSc4	gól
Arjenu	Arjen	k1gMnSc3	Arjen
Robbenovi	Robben	k1gMnSc3	Robben
<g/>
,	,	kIx,	,
Bayern	Bayern	k1gInSc1	Bayern
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
nejprestižnější	prestižní	k2eAgInSc4d3	nejprestižnější
pohár	pohár	k1gInSc4	pohár
v	v	k7c6	v
evropském	evropský	k2eAgInSc6d1	evropský
fotbale	fotbal	k1gInSc6	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
DFB-Pokalu	DFB-Pokal	k1gMnSc3	DFB-Pokal
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
porazil	porazit	k5eAaPmAgInS	porazit
Bayern	Bayern	k1gInSc1	Bayern
s	s	k7c7	s
Ribérym	Ribérym	k1gInSc4	Ribérym
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
VfB	VfB	k1gFnSc2	VfB
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
tak	tak	k6eAd1	tak
treble	treble	k6eAd1	treble
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
domácí	domácí	k2eAgFnPc4d1	domácí
soutěže	soutěž	k1gFnPc4	soutěž
plus	plus	k6eAd1	plus
titul	titul	k1gInSc1	titul
v	v	k7c6	v
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
resp.	resp.	kA	resp.
PMEZ	PMEZ	kA	PMEZ
<g/>
)	)	kIx)	)
jako	jako	k9	jako
sedmý	sedmý	k4xOgInSc1	sedmý
evropský	evropský	k2eAgInSc1d1	evropský
klub	klub	k1gInSc1	klub
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
dva	dva	k4xCgInPc4	dva
góly	gól	k1gInPc4	gól
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
utkání	utkání	k1gNnSc6	utkání
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
Viktorii	Viktoria	k1gFnSc4	Viktoria
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
souboj	souboj	k1gInSc1	souboj
německého	německý	k2eAgMnSc2d1	německý
mistra	mistr	k1gMnSc2	mistr
s	s	k7c7	s
českým	český	k2eAgMnSc7d1	český
vyzněl	vyznít	k5eAaPmAgInS	vyznít
jednoznačně	jednoznačně	k6eAd1	jednoznačně
pro	pro	k7c4	pro
Bayern	Bayern	k1gInSc4	Bayern
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
branku	branka	k1gFnSc4	branka
dal	dát	k5eAaPmAgMnS	dát
z	z	k7c2	z
pokutového	pokutový	k2eAgInSc2d1	pokutový
kopu	kop	k1gInSc2	kop
<g/>
,	,	kIx,	,
při	při	k7c6	při
druhé	druhý	k4xOgFnSc6	druhý
se	se	k3xPyFc4	se
zasekávačkou	zasekávačka	k1gFnSc7	zasekávačka
zbavil	zbavit	k5eAaPmAgInS	zbavit
dvou	dva	k4xCgMnPc2	dva
bránících	bránící	k2eAgMnPc2d1	bránící
hráčů	hráč	k1gMnPc2	hráč
a	a	k8xC	a
obloučkem	oblouček	k1gInSc7	oblouček
překonal	překonat	k5eAaPmAgMnS	překonat
brankáře	brankář	k1gMnSc4	brankář
Matúše	Matúš	k1gMnSc4	Matúš
Kozáčika	Kozáčik	k1gMnSc4	Kozáčik
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Bayernem	Bayern	k1gInSc7	Bayern
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2013	[number]	k4	2013
i	i	k8xC	i
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
klubů	klub	k1gInPc2	klub
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Bayern	Bayern	k1gInSc1	Bayern
porazil	porazit	k5eAaPmAgInS	porazit
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
domácí	domácí	k2eAgInSc4d1	domácí
tým	tým	k1gInSc4	tým
Raja	Raja	k1gFnSc1	Raja
Casablanca	Casablanca	k1gFnSc1	Casablanca
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
skončila	skončit	k5eAaPmAgFnS	skončit
pouť	pouť	k1gFnSc1	pouť
Bayernu	Bayern	k1gInSc2	Bayern
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
proti	proti	k7c3	proti
Realu	Real	k1gInSc3	Real
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
Ribéry	Ribér	k1gInPc1	Ribér
ale	ale	k8xC	ale
mohl	moct	k5eAaImAgInS	moct
na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezony	sezona	k1gFnSc2	sezona
slavit	slavit	k5eAaImF	slavit
obhajobu	obhajoba	k1gFnSc4	obhajoba
ligového	ligový	k2eAgInSc2d1	ligový
titulu	titul	k1gInSc2	titul
a	a	k8xC	a
vítězství	vítězství	k1gNnSc2	vítězství
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
poháru	pohár	k1gInSc6	pohár
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
E	E	kA	E
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
gól	gól	k1gInSc1	gól
italskému	italský	k2eAgInSc3d1	italský
klubu	klub	k1gInSc3	klub
AS	as	k1gInSc4	as
Roma	Rom	k1gMnSc2	Rom
<g/>
,	,	kIx,	,
Bayern	Bayern	k1gInSc1	Bayern
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
vysoko	vysoko	k6eAd1	vysoko
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Franck	Franck	k1gMnSc1	Franck
nastupoval	nastupovat	k5eAaImAgMnS	nastupovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
za	za	k7c4	za
francouzský	francouzský	k2eAgInSc4d1	francouzský
reprezentační	reprezentační	k2eAgInSc4d1	reprezentační
výběr	výběr	k1gInSc4	výběr
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
francouzský	francouzský	k2eAgInSc4d1	francouzský
národní	národní	k2eAgInSc4d1	národní
A-tým	Aé	k1gNnSc7	A-té
debutoval	debutovat	k5eAaBmAgInS	debutovat
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
v	v	k7c6	v
přátelském	přátelský	k2eAgNnSc6d1	přátelské
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
hostujícímu	hostující	k2eAgNnSc3d1	hostující
Mexiku	Mexiko	k1gNnSc3	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Šel	jít	k5eAaImAgMnS	jít
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
v	v	k7c6	v
75	[number]	k4	75
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
pro	pro	k7c4	pro
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
tímto	tento	k3xDgInSc7	tento
výsledkem	výsledek	k1gInSc7	výsledek
střetnutí	střetnutí	k1gNnSc2	střetnutí
skončilo	skončit	k5eAaPmAgNnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Ukončil	ukončit	k5eAaPmAgMnS	ukončit
ji	on	k3xPp3gFnSc4	on
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
odehrál	odehrát	k5eAaPmAgInS	odehrát
81	[number]	k4	81
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
šestnáctkrát	šestnáctkrát	k6eAd1	šestnáctkrát
skóroval	skórovat	k5eAaBmAgMnS	skórovat
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
kvalifikačním	kvalifikační	k2eAgInSc6d1	kvalifikační
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
Gruzii	Gruzie	k1gFnSc3	Gruzie
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
skončil	skončit	k5eAaPmAgMnS	skončit
vítězstvím	vítězství	k1gNnSc7	vítězství
domácí	domácí	k2eAgFnSc2d1	domácí
Francie	Francie	k1gFnSc2	Francie
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ribéry	Ribér	k1gMnPc4	Ribér
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
jeden	jeden	k4xCgInSc4	jeden
gól	gól	k1gInSc4	gól
svého	svůj	k3xOyFgInSc2	svůj
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
góly	gól	k1gInPc7	gól
v	v	k7c6	v
kvalifikačním	kvalifikační	k2eAgNnSc6d1	kvalifikační
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
domácímu	domácí	k2eAgInSc3d1	domácí
Bělorusku	Běloruska	k1gFnSc4	Běloruska
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
výhře	výhra	k1gFnSc6	výhra
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
už	už	k6eAd1	už
měla	mít	k5eAaImAgFnS	mít
jistou	jistý	k2eAgFnSc4d1	jistá
účast	účast	k1gFnSc4	účast
minimálně	minimálně	k6eAd1	minimálně
v	v	k7c6	v
baráži	baráž	k1gFnSc6	baráž
o	o	k7c4	o
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
2014	[number]	k4	2014
a	a	k8xC	a
bojovala	bojovat	k5eAaImAgFnS	bojovat
se	se	k3xPyFc4	se
Španělskem	Španělsko	k1gNnSc7	Španělsko
o	o	k7c4	o
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
zaručující	zaručující	k2eAgFnSc4d1	zaručující
přímou	přímý	k2eAgFnSc4d1	přímá
účast	účast	k1gFnSc4	účast
na	na	k7c4	na
mundialu	mundiala	k1gFnSc4	mundiala
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
nezapomenutelný	zapomenutelný	k2eNgInSc4d1	nezapomenutelný
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentační	reprezentační	k2eAgMnSc1d1	reprezentační
trenér	trenér	k1gMnSc1	trenér
Francie	Francie	k1gFnSc2	Francie
Raymond	Raymond	k1gMnSc1	Raymond
Domenech	Domen	k1gInPc6	Domen
jej	on	k3xPp3gNnSc4	on
postavil	postavit	k5eAaPmAgInS	postavit
hned	hned	k6eAd1	hned
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
na	na	k7c6	na
Mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
2006	[number]	k4	2006
proti	proti	k7c3	proti
Švýcarsku	Švýcarsko	k1gNnSc3	Švýcarsko
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
šel	jít	k5eAaImAgMnS	jít
na	na	k7c4	na
hřiště	hřiště	k1gNnSc4	hřiště
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
zápasu	zápas	k1gInSc2	zápas
s	s	k7c7	s
Jižní	jižní	k2eAgFnSc7d1	jižní
Koreou	Korea	k1gFnSc7	Korea
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
skončil	skončit	k5eAaPmAgInS	skončit
remízou	remíza	k1gFnSc7	remíza
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
výhře	výhra	k1gFnSc6	výhra
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
nad	nad	k7c7	nad
Togem	Togo	k1gNnSc7	Togo
<g/>
,	,	kIx,	,
když	když	k8xS	když
přihrával	přihrávat	k5eAaImAgMnS	přihrávat
na	na	k7c4	na
první	první	k4xOgInSc4	první
gól	gól	k1gInSc4	gól
Patricku	Patrick	k1gInSc2	Patrick
Vieirovi	Vieir	k1gMnSc3	Vieir
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
skončila	skončit	k5eAaPmAgFnS	skončit
s	s	k7c7	s
5	[number]	k4	5
body	bod	k1gInPc7	bod
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
příčce	příčka	k1gFnSc6	příčka
tabulky	tabulka	k1gFnSc2	tabulka
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
G	G	kA	G
za	za	k7c7	za
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
a	a	k8xC	a
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
osmifinále	osmifinále	k1gNnSc2	osmifinále
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
vsítil	vsítit	k5eAaPmAgMnS	vsítit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
reprezentační	reprezentační	k2eAgInSc4d1	reprezentační
gól	gól	k1gInSc4	gól
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
důležitý	důležitý	k2eAgMnSc1d1	důležitý
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
ve	v	k7c6	v
41	[number]	k4	41
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
pro	pro	k7c4	pro
Španělsko	Španělsko	k1gNnSc4	Španělsko
překonal	překonat	k5eAaPmAgMnS	překonat
gólmana	gólman	k1gMnSc4	gólman
Ikera	Ikero	k1gNnSc2	Ikero
Casillase	Casillas	k1gInSc6	Casillas
a	a	k8xC	a
načal	načít	k5eAaPmAgMnS	načít
francouzský	francouzský	k2eAgInSc4d1	francouzský
obrat	obrat	k1gInSc4	obrat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Španělé	Španěl	k1gMnPc1	Španěl
byli	být	k5eAaImAgMnP	být
favorité	favorit	k1gMnPc1	favorit
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
šla	jít	k5eAaImAgFnS	jít
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
proti	proti	k7c3	proti
Brazílii	Brazílie	k1gFnSc3	Brazílie
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
proti	proti	k7c3	proti
Brazílii	Brazílie	k1gFnSc3	Brazílie
Franck	Francka	k1gFnPc2	Francka
střídal	střídat	k5eAaImAgMnS	střídat
v	v	k7c6	v
77	[number]	k4	77
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
výsledek	výsledek	k1gInSc1	výsledek
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
se	se	k3xPyFc4	se
zrodil	zrodit	k5eAaPmAgInS	zrodit
i	i	k9	i
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
s	s	k7c7	s
Portugalskem	Portugalsko	k1gNnSc7	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Ribéry	Ribéra	k1gFnPc1	Ribéra
si	se	k3xPyFc3	se
tak	tak	k9	tak
zahrál	zahrát	k5eAaPmAgMnS	zahrát
památné	památný	k2eAgNnSc4d1	památné
finále	finále	k1gNnSc4	finále
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
proti	proti	k7c3	proti
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
ale	ale	k8xC	ale
vítězné	vítězný	k2eAgNnSc1d1	vítězné
tažení	tažení	k1gNnSc1	tažení
Francie	Francie	k1gFnSc2	Francie
skončilo	skončit	k5eAaPmAgNnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výsledku	výsledek	k1gInSc6	výsledek
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
po	po	k7c6	po
prodloužení	prodloužení	k1gNnSc6	prodloužení
následoval	následovat	k5eAaImAgInS	následovat
penaltový	penaltový	k2eAgInSc1d1	penaltový
rozstřel	rozstřel	k1gInSc1	rozstřel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
Francie	Francie	k1gFnSc1	Francie
prohrála	prohrát	k5eAaPmAgFnS	prohrát
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
EURU	euro	k1gNnSc6	euro
2008	[number]	k4	2008
konaném	konaný	k2eAgNnSc6d1	konané
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc6	Rakousko
Francie	Francie	k1gFnSc2	Francie
nebyla	být	k5eNaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
již	již	k6eAd1	již
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
C	C	kA	C
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
skupina	skupina	k1gFnSc1	skupina
smrti	smrt	k1gFnSc2	smrt
<g/>
"	"	kIx"	"
-	-	kIx~	-
nejtěžší	těžký	k2eAgFnSc1d3	nejtěžší
základní	základní	k2eAgFnSc1d1	základní
skupina	skupina	k1gFnSc1	skupina
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
<g/>
)	)	kIx)	)
po	po	k7c6	po
remíze	remíza	k1gFnSc6	remíza
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
s	s	k7c7	s
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
a	a	k8xC	a
porážkách	porážka	k1gFnPc6	porážka
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
s	s	k7c7	s
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
a	a	k8xC	a
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Franck	Franck	k1gMnSc1	Franck
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
sestavě	sestava	k1gFnSc6	sestava
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
utkáních	utkání	k1gNnPc6	utkání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
proti	proti	k7c3	proti
Itálii	Itálie	k1gFnSc3	Itálie
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
zranil	zranit	k5eAaPmAgMnS	zranit
a	a	k8xC	a
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
vystřídán	vystřídán	k2eAgMnSc1d1	vystřídán
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
2010	[number]	k4	2010
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
Francii	Francie	k1gFnSc3	Francie
nevyvedlo	vyvést	k5eNaPmAgNnS	vyvést
<g/>
.	.	kIx.	.
</s>
<s>
Postoupila	postoupit	k5eAaPmAgFnS	postoupit
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
po	po	k7c6	po
sporné	sporný	k2eAgFnSc6d1	sporná
baráži	baráž	k1gFnSc6	baráž
s	s	k7c7	s
Irskem	Irsko	k1gNnSc7	Irsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Thierry	Thierra	k1gFnPc4	Thierra
Henry	henry	k1gInSc2	henry
nahrával	nahrávat	k5eAaImAgMnS	nahrávat
na	na	k7c4	na
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
gól	gól	k1gInSc4	gól
Williamu	William	k1gInSc6	William
Gallasovi	Gallasův	k2eAgMnPc1d1	Gallasův
po	po	k7c4	po
zpracování	zpracování	k1gNnSc4	zpracování
míče	míč	k1gInSc2	míč
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
zápas	zápas	k1gInSc1	zápas
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
s	s	k7c7	s
Uruguayí	Uruguay	k1gFnSc7	Uruguay
skončil	skončit	k5eAaPmAgInS	skončit
bezbrankovou	bezbrankový	k2eAgFnSc7d1	bezbranková
remízou	remíza	k1gFnSc7	remíza
<g/>
,	,	kIx,	,
Franck	Franck	k1gInSc1	Franck
dostal	dostat	k5eAaPmAgInS	dostat
žlutou	žlutý	k2eAgFnSc4d1	žlutá
kartu	karta	k1gFnSc4	karta
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
přišla	přijít	k5eAaPmAgFnS	přijít
porážka	porážka	k1gFnSc1	porážka
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
prohra	prohra	k1gFnSc1	prohra
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
s	s	k7c7	s
domácí	domácí	k2eAgFnSc7d1	domácí
Jižní	jižní	k2eAgFnSc7d1	jižní
Afrikou	Afrika	k1gFnSc7	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výsledky	výsledek	k1gInPc1	výsledek
znamenaly	znamenat	k5eAaImAgInP	znamenat
umístění	umístění	k1gNnSc4	umístění
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
příčce	příčka	k1gFnSc6	příčka
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
A	a	k9	a
a	a	k8xC	a
brzké	brzký	k2eAgNnSc1d1	brzké
vyřazení	vyřazení	k1gNnSc1	vyřazení
francouzské	francouzský	k2eAgFnSc2d1	francouzská
reprezentace	reprezentace	k1gFnSc2	reprezentace
<g/>
.	.	kIx.	.
</s>
<s>
Franck	Franck	k1gMnSc1	Franck
odehrál	odehrát	k5eAaPmAgMnS	odehrát
kompletní	kompletní	k2eAgInSc4d1	kompletní
počet	počet	k1gInSc4	počet
minut	minuta	k1gFnPc2	minuta
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
třech	tři	k4xCgInPc6	tři
zápasech	zápas	k1gInPc6	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
EURU	euro	k1gNnSc6	euro
2012	[number]	k4	2012
konaném	konaný	k2eAgNnSc6d1	konané
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
postoupila	postoupit	k5eAaPmAgFnS	postoupit
Francie	Francie	k1gFnSc1	Francie
se	s	k7c7	s
4	[number]	k4	4
body	bod	k1gInPc7	bod
ze	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
místa	místo	k1gNnSc2	místo
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
utkání	utkání	k1gNnSc6	utkání
Ribéry	Ribéra	k1gFnSc2	Ribéra
nahrával	nahrávat	k5eAaImAgMnS	nahrávat
na	na	k7c4	na
gól	gól	k1gInSc4	gól
Samiru	Samir	k1gInSc2	Samir
Nasri	Nasr	k1gFnSc2	Nasr
proti	proti	k7c3	proti
Anglii	Anglie	k1gFnSc3	Anglie
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
Francie	Francie	k1gFnSc1	Francie
porazila	porazit	k5eAaPmAgFnS	porazit
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
a	a	k8xC	a
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
nestačil	stačit	k5eNaBmAgMnS	stačit
Ribéry	Ribéra	k1gFnPc4	Ribéra
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spoluhráči	spoluhráč	k1gMnPc7	spoluhráč
na	na	k7c4	na
největšího	veliký	k2eAgMnSc4d3	veliký
favorita	favorit	k1gMnSc4	favorit
turnaje	turnaj	k1gInSc2	turnaj
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
Francie	Francie	k1gFnSc1	Francie
prohrála	prohrát	k5eAaPmAgFnS	prohrát
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
×	×	k?	×
Fotbalista	fotbalista	k1gMnSc1	fotbalista
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
-	-	kIx~	-
2008	[number]	k4	2008
FIFA	FIFA	kA	FIFA
fotbalista	fotbalista	k1gMnSc1	fotbalista
roku	rok	k1gInSc2	rok
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
místo	místo	k1gNnSc4	místo
-	-	kIx~	-
2013	[number]	k4	2013
</s>
