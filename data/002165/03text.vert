<s>
Honolulu	Honolulu	k1gNnSc1	Honolulu
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
Havaje	Havaj	k1gFnSc2	Havaj
<g/>
,	,	kIx,	,
státu	stát	k1gInSc2	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
domorodého	domorodý	k2eAgInSc2d1	domorodý
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
honolulu	honolul	k1gMnSc3	honolul
znamená	znamenat	k5eAaImIp3nS	znamenat
chráněná	chráněný	k2eAgFnSc1d1	chráněná
zátoka	zátoka	k1gFnSc1	zátoka
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
názvem	název	k1gInSc7	název
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Křižovatka	křižovatka	k1gFnSc1	křižovatka
Pacifiku	Pacifik	k1gInSc2	Pacifik
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Oahu	Oahus	k1gInSc2	Oahus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
390	[number]	k4	390
738	[number]	k4	738
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
asijského	asijský	k2eAgNnSc2d1	asijské
(	(	kIx(	(
<g/>
především	především	k9	především
japonského	japonský	k2eAgMnSc2d1	japonský
<g/>
,	,	kIx,	,
filipínského	filipínský	k2eAgMnSc2d1	filipínský
nebo	nebo	k8xC	nebo
čínského	čínský	k2eAgMnSc2d1	čínský
<g/>
)	)	kIx)	)
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
17,9	[number]	k4	17,9
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
1,5	[number]	k4	1,5
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,2	[number]	k4	0,2
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
54,8	[number]	k4	54,8
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
8,4	[number]	k4	8,4
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
0,8	[number]	k4	0,8
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
16,3	[number]	k4	16,3
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
5,4	[number]	k4	5,4
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1816	[number]	k4	1816
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Havajského	havajský	k2eAgNnSc2d1	havajské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Barack	Barack	k1gInSc1	Barack
Obama	Obama	k?	Obama
(	(	kIx(	(
<g/>
*	*	kIx~	*
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
Arthur	Arthur	k1gMnSc1	Arthur
Leigh	Leigh	k1gMnSc1	Leigh
Allen	Allen	k1gMnSc1	Allen
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Američan	Američan	k1gMnSc1	Američan
<g/>
,	,	kIx,	,
považovaný	považovaný	k2eAgMnSc1d1	považovaný
za	za	k7c4	za
Zodiaka	Zodiak	k1gMnSc4	Zodiak
Israel	Israel	k1gMnSc1	Israel
"	"	kIx"	"
<g/>
IZ	IZ	kA	IZ
<g/>
"	"	kIx"	"
Kamakawiwoʻ	Kamakawiwoʻ	k1gMnSc1	Kamakawiwoʻ
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
ukulele	ukulele	k1gNnSc4	ukulele
Soul	Soul	k1gInSc1	Soul
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
</s>
