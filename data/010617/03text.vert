<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
Airbus	airbus	k1gInSc1	airbus
S.	S.	kA	S.
<g/>
A.S.	A.S.	k1gMnPc2	A.S.
<g/>
,	,	kIx,	,
známější	známý	k2eAgFnSc1d2	známější
jen	jen	k9	jen
jako	jako	k8xC	jako
Airbus	airbus	k1gInSc4	airbus
<g/>
,	,	kIx,	,
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Toulouse	Toulouse	k1gInSc6	Toulouse
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
největších	veliký	k2eAgMnPc2d3	veliký
světových	světový	k2eAgMnPc2d1	světový
výrobců	výrobce	k1gMnPc2	výrobce
civilních	civilní	k2eAgNnPc2d1	civilní
dopravních	dopravní	k2eAgNnPc2d1	dopravní
letadel	letadlo	k1gNnPc2	letadlo
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Boeing	boeing	k1gInSc4	boeing
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1970	[number]	k4	1970
ve	v	k7c6	v
francouzsko-německo-britské	francouzskoěmeckoritský	k2eAgFnSc6d1	francouzsko-německo-britský
spolupráci	spolupráce	k1gFnSc6	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Vlastníky	vlastník	k1gMnPc7	vlastník
společnosti	společnost	k1gFnSc2	společnost
Airbus	airbus	k1gInSc1	airbus
jsou	být	k5eAaImIp3nP	být
EADS	EADS	kA	EADS
(	(	kIx(	(
<g/>
80	[number]	k4	80
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
BAE	BAE	kA	BAE
Systems	Systems	k1gInSc1	Systems
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
největší	veliký	k2eAgFnPc1d3	veliký
evropské	evropský	k2eAgFnPc1d1	Evropská
zbrojařské	zbrojařský	k2eAgFnPc1d1	zbrojařská
společnosti	společnost	k1gFnPc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Airbus	airbus	k1gInSc1	airbus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
zaměstnával	zaměstnávat	k5eAaImAgInS	zaměstnávat
přes	přes	k7c4	přes
72	[number]	k4	72
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Konečná	konečný	k2eAgFnSc1d1	konečná
montáž	montáž	k1gFnSc1	montáž
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
v	v	k7c6	v
Toulouse	Toulouse	k1gInSc6	Toulouse
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hamburku	Hamburk	k1gInSc2	Hamburk
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
(	(	kIx(	(
<g/>
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jediným	jediný	k2eAgMnSc7d1	jediný
<g/>
)	)	kIx)	)
konkurentem	konkurent	k1gMnSc7	konkurent
Airbusu	airbus	k1gInSc2	airbus
je	být	k5eAaImIp3nS	být
Boeing	boeing	k1gInSc4	boeing
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterým	který	k3yQgMnPc3	který
Airbus	airbus	k1gInSc1	airbus
vede	vést	k5eAaImIp3nS	vést
intenzivní	intenzivní	k2eAgFnSc4d1	intenzivní
obchodní	obchodní	k2eAgFnSc4d1	obchodní
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
vzlétl	vzlétnout	k5eAaPmAgMnS	vzlétnout
první	první	k4xOgInSc4	první
model	model	k1gInSc4	model
Airbus	airbus	k1gInSc4	airbus
A	a	k9	a
<g/>
300	[number]	k4	300
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgInSc1d1	rozhodující
úspěch	úspěch	k1gInSc1	úspěch
přinesl	přinést	k5eAaPmAgInS	přinést
model	model	k1gInSc4	model
A	a	k9	a
<g/>
320	[number]	k4	320
<g/>
:	:	kIx,	:
když	když	k8xS	když
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
poprvé	poprvé	k6eAd1	poprvé
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
společnost	společnost	k1gFnSc1	společnost
už	už	k9	už
400	[number]	k4	400
objednávek	objednávka	k1gFnPc2	objednávka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
modernizovanou	modernizovaný	k2eAgFnSc4d1	modernizovaná
verzi	verze	k1gFnSc4	verze
A	a	k9	a
<g/>
320	[number]	k4	320
<g/>
neo	neo	k?	neo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
poprvé	poprvé	k6eAd1	poprvé
vzlétla	vzlétnout	k5eAaPmAgFnS	vzlétnout
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2015	[number]	k4	2015
přes	přes	k7c4	přes
5	[number]	k4	5
000	[number]	k4	000
objednávek	objednávka	k1gFnPc2	objednávka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byl	být	k5eAaImAgInS	být
Airbus	airbus	k1gInSc1	airbus
zapsán	zapsat	k5eAaPmNgInS	zapsat
do	do	k7c2	do
francouzského	francouzský	k2eAgInSc2d1	francouzský
obchodního	obchodní	k2eAgInSc2d1	obchodní
rejstříku	rejstřík	k1gInSc2	rejstřík
jako	jako	k8xC	jako
zjednodušená	zjednodušený	k2eAgFnSc1d1	zjednodušená
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
neboli	neboli	k8xC	neboli
S.	S.	kA	S.
<g/>
A.S.	A.S.	k1gMnPc1	A.S.
(	(	kIx(	(
<g/>
Société	Sociéta	k1gMnPc1	Sociéta
par	para	k1gFnPc2	para
Actions	Actions	k1gInSc1	Actions
Simplifiée	Simplifié	k1gInSc2	Simplifié
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Airbus	airbus	k1gInSc1	airbus
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
znám	znám	k2eAgInSc1d1	znám
jako	jako	k8xS	jako
Airbus	airbus	k1gInSc1	airbus
Industrie	industrie	k1gFnSc2	industrie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
označován	označovat	k5eAaImNgInS	označovat
jen	jen	k9	jen
jako	jako	k8xS	jako
Airbus	airbus	k1gInSc4	airbus
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
krokem	krok	k1gInSc7	krok
byl	být	k5eAaImAgInS	být
vzlet	vzlet	k1gInSc1	vzlet
modelu	model	k1gInSc2	model
A380	A380	k1gFnSc2	A380
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
tohoto	tento	k3xDgNnSc2	tento
největšího	veliký	k2eAgNnSc2d3	veliký
civilního	civilní	k2eAgNnSc2d1	civilní
dopravního	dopravní	k2eAgNnSc2d1	dopravní
letadla	letadlo	k1gNnSc2	letadlo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
však	však	k9	však
narazila	narazit	k5eAaPmAgFnS	narazit
na	na	k7c4	na
organizační	organizační	k2eAgFnPc4d1	organizační
potíže	potíž	k1gFnPc4	potíž
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
první	první	k4xOgInSc1	první
stroj	stroj	k1gInSc1	stroj
byl	být	k5eAaImAgInS	být
předán	předat	k5eAaPmNgInS	předat
zákazníkovi	zákazník	k1gMnSc3	zákazník
až	až	k8xS	až
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
přivedlo	přivést	k5eAaPmAgNnS	přivést
společnost	společnost	k1gFnSc4	společnost
do	do	k7c2	do
finančních	finanční	k2eAgFnPc2d1	finanční
potíží	potíž	k1gFnPc2	potíž
<g/>
,	,	kIx,	,
po	po	k7c6	po
reorganizaci	reorganizace	k1gFnSc6	reorganizace
se	se	k3xPyFc4	se
ale	ale	k9	ale
rychle	rychle	k6eAd1	rychle
vzchopila	vzchopit	k5eAaPmAgFnS	vzchopit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
slavil	slavit	k5eAaImAgInS	slavit
Airbus	airbus	k1gInSc1	airbus
výročí	výročí	k1gNnSc1	výročí
10	[number]	k4	10
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
vyrobeného	vyrobený	k2eAgNnSc2d1	vyrobené
letadla	letadlo	k1gNnSc2	letadlo
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
byl	být	k5eAaImAgMnS	být
A350-900	A350-900	k1gMnSc1	A350-900
pro	pro	k7c4	pro
Singapore	Singapor	k1gInSc5	Singapor
Airlines	Airlinesa	k1gFnPc2	Airlinesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Začátky	začátek	k1gInPc1	začátek
===	===	k?	===
</s>
</p>
<p>
<s>
Airbus	airbus	k1gInSc1	airbus
Industrie	industrie	k1gFnSc2	industrie
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
konsorcium	konsorcium	k1gNnSc1	konsorcium
evropských	evropský	k2eAgFnPc2d1	Evropská
leteckých	letecký	k2eAgFnPc2d1	letecká
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
chtěly	chtít	k5eAaImAgFnP	chtít
konkurovat	konkurovat	k5eAaImF	konkurovat
velkým	velký	k2eAgFnPc3d1	velká
americkým	americký	k2eAgFnPc3d1	americká
společnostem	společnost	k1gFnPc3	společnost
jako	jako	k9	jako
Boeing	boeing	k1gInSc1	boeing
a	a	k8xC	a
McDonnell	McDonnell	k1gMnSc1	McDonnell
Douglas	Douglas	k1gMnSc1	Douglas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
si	se	k3xPyFc3	se
evropští	evropský	k2eAgMnPc1d1	evropský
výrobci	výrobce	k1gMnPc1	výrobce
letadel	letadlo	k1gNnPc2	letadlo
konkurovali	konkurovat	k5eAaImAgMnP	konkurovat
nejen	nejen	k6eAd1	nejen
s	s	k7c7	s
americkými	americký	k2eAgMnPc7d1	americký
giganty	gigant	k1gMnPc7	gigant
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
začala	začít	k5eAaPmAgFnS	začít
první	první	k4xOgNnPc4	první
váhavá	váhavý	k2eAgNnPc4d1	váhavé
jednání	jednání	k1gNnPc4	jednání
o	o	k7c4	o
spojení	spojení	k1gNnSc4	spojení
evropských	evropský	k2eAgFnPc2d1	Evropská
kapacit	kapacita	k1gFnPc2	kapacita
při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
<g/>
,	,	kIx,	,
výrobě	výroba	k1gFnSc6	výroba
a	a	k8xC	a
prodeji	prodej	k1gInSc6	prodej
dopravních	dopravní	k2eAgInPc2d1	dopravní
letounů	letoun	k1gInPc2	letoun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1967	[number]	k4	1967
britská	britský	k2eAgFnSc1d1	britská
<g/>
,	,	kIx,	,
francouzská	francouzský	k2eAgFnSc1d1	francouzská
a	a	k8xC	a
německá	německý	k2eAgFnSc1d1	německá
vláda	vláda	k1gFnSc1	vláda
podepsaly	podepsat	k5eAaPmAgFnP	podepsat
Memorandum	memorandum	k1gNnSc4	memorandum
o	o	k7c4	o
porozumění	porozumění	k1gNnSc4	porozumění
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
začít	začít	k5eAaPmF	začít
vývoj	vývoj	k1gInSc1	vývoj
300	[number]	k4	300
<g/>
místného	místný	k2eAgInSc2d1	místný
Airbusu	airbus	k1gInSc2	airbus
A	a	k9	a
<g/>
300	[number]	k4	300
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
druhý	druhý	k4xOgInSc1	druhý
velký	velký	k2eAgInSc1d1	velký
společný	společný	k2eAgInSc1d1	společný
letecký	letecký	k2eAgInSc1d1	letecký
projekt	projekt	k1gInSc1	projekt
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
po	po	k7c6	po
Concordu	Concordo	k1gNnSc6	Concordo
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
který	který	k3yQgInSc4	který
nebylo	být	k5eNaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
žádné	žádný	k3yNgNnSc1	žádný
trvalé	trvalý	k2eAgNnSc1d1	trvalé
konsorcium	konsorcium	k1gNnSc1	konsorcium
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
oznámení	oznámení	k1gNnSc1	oznámení
o	o	k7c6	o
projektu	projekt	k1gInSc6	projekt
byla	být	k5eAaImAgNnP	být
učiněna	učinit	k5eAaPmNgNnP	učinit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
situaci	situace	k1gFnSc4	situace
zkomplikovala	zkomplikovat	k5eAaPmAgFnS	zkomplikovat
společnost	společnost	k1gFnSc1	společnost
British	British	k1gInSc1	British
Aircraft	Aircraft	k1gInSc1	Aircraft
Corporation	Corporation	k1gInSc4	Corporation
(	(	kIx(	(
<g/>
BAC	bacit	k5eAaPmRp2nS	bacit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
chtěla	chtít	k5eAaImAgFnS	chtít
sama	sám	k3xTgFnSc1	sám
vyvinout	vyvinout	k5eAaPmF	vyvinout
podobný	podobný	k2eAgInSc4d1	podobný
letoun	letoun	k1gInSc4	letoun
(	(	kIx(	(
<g/>
BAC	bacit	k5eAaPmRp2nS	bacit
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
založený	založený	k2eAgMnSc1d1	založený
na	na	k7c6	na
již	již	k6eAd1	již
existujícím	existující	k2eAgNnSc6d1	existující
BAC	bac	k0	bac
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
ale	ale	k9	ale
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
postavit	postavit	k5eAaPmF	postavit
za	za	k7c4	za
vývoj	vývoj	k1gInSc4	vývoj
BAC	bac	k0	bac
3-11	[number]	k4	3-11
a	a	k8xC	a
místo	místo	k7c2	místo
něj	on	k3xPp3gInSc2	on
podpořila	podpořit	k5eAaPmAgFnS	podpořit
Airbus	airbus	k1gInSc4	airbus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
měsících	měsíc	k1gInPc6	měsíc
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
následovaly	následovat	k5eAaImAgInP	následovat
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
francouzská	francouzský	k2eAgFnSc1d1	francouzská
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
britská	britský	k2eAgFnSc1d1	britská
vláda	vláda	k1gFnSc1	vláda
vyjadřovaly	vyjadřovat	k5eAaImAgInP	vyjadřovat
své	svůj	k3xOyFgFnPc4	svůj
pochybnosti	pochybnost	k1gFnPc4	pochybnost
o	o	k7c6	o
budoucnosti	budoucnost	k1gFnSc6	budoucnost
celého	celý	k2eAgInSc2d1	celý
projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
problémem	problém	k1gInSc7	problém
byla	být	k5eAaImAgFnS	být
nutnost	nutnost	k1gFnSc1	nutnost
vývoje	vývoj	k1gInSc2	vývoj
nového	nový	k2eAgInSc2d1	nový
motoru	motor	k1gInSc2	motor
společností	společnost	k1gFnPc2	společnost
Rolls-Royce	Rolls-Royce	k1gFnSc2	Rolls-Royce
<g/>
,	,	kIx,	,
RB	RB	kA	RB
<g/>
207	[number]	k4	207
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1968	[number]	k4	1968
partnerské	partnerský	k2eAgFnSc2d1	partnerská
společnosti	společnost	k1gFnSc2	společnost
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Sud	sud	k1gInSc1	sud
Aviation	Aviation	k1gInSc1	Aviation
a	a	k8xC	a
Hawker	Hawker	k1gInSc1	Hawker
Siddeley	Siddelea	k1gFnSc2	Siddelea
<g/>
,	,	kIx,	,
navrhly	navrhnout	k5eAaPmAgFnP	navrhnout
pozměnit	pozměnit	k5eAaPmF	pozměnit
konfiguraci	konfigurace	k1gFnSc4	konfigurace
letounu	letoun	k1gInSc2	letoun
na	na	k7c4	na
250	[number]	k4	250
<g/>
místný	místný	k2eAgInSc4d1	místný
Airbus	airbus	k1gInSc4	airbus
A	a	k9	a
<g/>
250	[number]	k4	250
<g/>
.	.	kIx.	.
</s>
<s>
Letoun	letoun	k1gInSc1	letoun
<g/>
,	,	kIx,	,
vzápětí	vzápětí	k6eAd1	vzápětí
přejmenovaný	přejmenovaný	k2eAgInSc4d1	přejmenovaný
na	na	k7c4	na
A	A	kA	A
<g/>
300	[number]	k4	300
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
nevyžadoval	vyžadovat	k5eNaImAgInS	vyžadovat
nové	nový	k2eAgInPc4d1	nový
motory	motor	k1gInPc4	motor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
významně	významně	k6eAd1	významně
snížilo	snížit	k5eAaPmAgNnS	snížit
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
britská	britský	k2eAgFnSc1d1	britská
vláda	vláda	k1gFnSc1	vláda
šokovala	šokovat	k5eAaBmAgFnS	šokovat
své	svůj	k3xOyFgMnPc4	svůj
partnery	partner	k1gMnPc4	partner
odstoupením	odstoupení	k1gNnSc7	odstoupení
od	od	k7c2	od
projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
dosavadní	dosavadní	k2eAgFnSc3d1	dosavadní
plodné	plodný	k2eAgFnSc3d1	plodná
účasti	účast	k1gFnSc3	účast
firmy	firma	k1gFnSc2	firma
Hawker	Hawkra	k1gFnPc2	Hawkra
Siddeley	Siddelea	k1gFnSc2	Siddelea
se	se	k3xPyFc4	se
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Německo	Německo	k1gNnSc4	Německo
zdráhaly	zdráhat	k5eAaImAgFnP	zdráhat
převzít	převzít	k5eAaPmF	převzít
vývoj	vývoj	k1gInSc4	vývoj
křídla	křídlo	k1gNnSc2	křídlo
letounu	letoun	k1gInSc2	letoun
a	a	k8xC	a
britská	britský	k2eAgFnSc1d1	britská
společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
dále	daleko	k6eAd2	daleko
účastnit	účastnit	k5eAaImF	účastnit
jako	jako	k9	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
subdodavatel	subdodavatel	k1gMnSc1	subdodavatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
se	se	k3xPyFc4	se
Británie	Británie	k1gFnSc1	Británie
znovu	znovu	k6eAd1	znovu
připojila	připojit	k5eAaPmAgFnS	připojit
ke	k	k7c3	k
konsorciu	konsorcium	k1gNnSc3	konsorcium
<g/>
,	,	kIx,	,
když	když	k8xS	když
British	British	k1gInSc1	British
Aerospace	Aerospace	k1gFnSc2	Aerospace
(	(	kIx(	(
<g/>
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
spojením	spojení	k1gNnSc7	spojení
Hawker	Hawkero	k1gNnPc2	Hawkero
Siddeley	Siddelea	k1gFnSc2	Siddelea
a	a	k8xC	a
BAC	bacit	k5eAaPmRp2nS	bacit
<g/>
)	)	kIx)	)
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
20	[number]	k4	20
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc4	podíl
v	v	k7c6	v
Airbusu	airbus	k1gInSc6	airbus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Založení	založení	k1gNnSc1	založení
===	===	k?	===
</s>
</p>
<p>
<s>
Airbus	airbus	k1gInSc1	airbus
Industrie	industrie	k1gFnSc2	industrie
byl	být	k5eAaImAgInS	být
formálně	formálně	k6eAd1	formálně
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
dohody	dohoda	k1gFnSc2	dohoda
mezi	mezi	k7c7	mezi
Aérospatiale	Aérospatiala	k1gFnSc3	Aérospatiala
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Deutsche	Deutsche	k1gFnSc1	Deutsche
Aerospace	Aerospace	k1gFnSc1	Aerospace
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
firma	firma	k1gFnSc1	firma
Construcciones	Construcciones	k1gMnSc1	Construcciones
Aeronáuticas	Aeronáuticas	k1gMnSc1	Aeronáuticas
SA	SA	kA	SA
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
účastníků	účastník	k1gMnPc2	účastník
měl	mít	k5eAaImAgMnS	mít
dodávat	dodávat	k5eAaImF	dodávat
"	"	kIx"	"
<g/>
svou	svůj	k3xOyFgFnSc4	svůj
<g/>
"	"	kIx"	"
část	část	k1gFnSc4	část
letounu	letoun	k1gInSc2	letoun
plně	plně	k6eAd1	plně
vybavenou	vybavený	k2eAgFnSc4d1	vybavená
(	(	kIx(	(
<g/>
připravenou	připravený	k2eAgFnSc4d1	připravená
ke	k	k7c3	k
konečné	konečná	k1gFnSc3	konečná
montáži	montáž	k1gFnSc3	montáž
bez	bez	k7c2	bez
jakýchkoliv	jakýkoliv	k3yIgFnPc2	jakýkoliv
dalších	další	k2eAgFnPc2d1	další
úprav	úprava	k1gFnPc2	úprava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Airbus	airbus	k1gInSc1	airbus
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
podle	podle	k7c2	podle
výrazu	výraz	k1gInSc2	výraz
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
leteckém	letecký	k2eAgInSc6d1	letecký
průmyslu	průmysl	k1gInSc6	průmysl
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
označovala	označovat	k5eAaImAgFnS	označovat
civilní	civilní	k2eAgNnPc1d1	civilní
letadla	letadlo	k1gNnPc4	letadlo
dané	daný	k2eAgFnSc2d1	daná
velikosti	velikost	k1gFnSc2	velikost
a	a	k8xC	a
doletu	dolet	k1gInSc2	dolet
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
Francii	Francie	k1gFnSc4	Francie
přijatelný	přijatelný	k2eAgInSc1d1	přijatelný
z	z	k7c2	z
lingvistického	lingvistický	k2eAgNnSc2d1	lingvistické
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
A300	A300	k1gMnSc1	A300
provedl	provést	k5eAaPmAgMnS	provést
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
vzlet	vzlet	k1gInSc4	vzlet
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
byl	být	k5eAaImAgInS	být
dodán	dodat	k5eAaPmNgInS	dodat
první	první	k4xOgInSc1	první
sériový	sériový	k2eAgInSc1d1	sériový
model	model	k1gInSc1	model
A300B2	A300B2	k1gMnPc2	A300B2
(	(	kIx(	(
<g/>
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
u	u	k7c2	u
Air	Air	k1gFnSc2	Air
France	Franc	k1gMnSc2	Franc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byl	být	k5eAaImAgInS	být
úspěch	úspěch	k1gInSc1	úspěch
Airbusu	airbus	k1gInSc2	airbus
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
už	už	k6eAd1	už
oblohu	obloha	k1gFnSc4	obloha
křižovalo	křižovat	k5eAaImAgNnS	křižovat
81	[number]	k4	81
letounů	letoun	k1gInPc2	letoun
z	z	k7c2	z
produkce	produkce	k1gFnSc2	produkce
evropského	evropský	k2eAgNnSc2d1	Evropské
konsorcia	konsorcium	k1gNnSc2	konsorcium
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
až	až	k9	až
vzlet	vzlet	k1gInSc4	vzlet
modelu	model	k1gInSc2	model
A320	A320	k1gFnSc2	A320
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zajistil	zajistit	k5eAaPmAgInS	zajistit
Airbusu	airbus	k1gInSc2	airbus
místo	místo	k7c2	místo
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
hráčů	hráč	k1gMnPc2	hráč
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
civilních	civilní	k2eAgNnPc2d1	civilní
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
A320	A320	k4	A320
měl	mít	k5eAaImAgMnS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
objednávek	objednávka	k1gFnPc2	objednávka
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
než	než	k8xS	než
poprvé	poprvé	k6eAd1	poprvé
vzlétl	vzlétnout	k5eAaPmAgMnS	vzlétnout
(	(	kIx(	(
<g/>
u	u	k7c2	u
A300	A300	k1gMnSc2	A300
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
15	[number]	k4	15
objednaných	objednaný	k2eAgInPc2d1	objednaný
kusů	kus	k1gInPc2	kus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
EADS	EADS	kA	EADS
===	===	k?	===
</s>
</p>
<p>
<s>
Airbus	airbus	k1gInSc1	airbus
byl	být	k5eAaImAgInS	být
poměrně	poměrně	k6eAd1	poměrně
volnou	volný	k2eAgFnSc7d1	volná
aliancí	aliance	k1gFnSc7	aliance
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
výrobců	výrobce	k1gMnPc2	výrobce
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
DASA	DASA	kA	DASA
<g/>
,	,	kIx,	,
Aerospatiale	Aerospatiala	k1gFnSc6	Aerospatiala
a	a	k8xC	a
CASA	CASA	kA	CASA
sloučily	sloučit	k5eAaPmAgFnP	sloučit
do	do	k7c2	do
EADS	EADS	kA	EADS
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
BAE	BAE	kA	BAE
Systems	Systemsa	k1gFnPc2	Systemsa
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
British	British	k1gMnSc1	British
Aerospace	Aerospace	k1gFnSc2	Aerospace
<g/>
)	)	kIx)	)
a	a	k8xC	a
EADS	EADS	kA	EADS
zformovaly	zformovat	k5eAaPmAgFnP	zformovat
Airbus	airbus	k1gInSc4	airbus
Integrated	Integrated	k1gInSc4	Integrated
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
časově	časově	k6eAd1	časově
shodovalo	shodovat	k5eAaImAgNnS	shodovat
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
nového	nový	k2eAgInSc2d1	nový
Airbusu	airbus	k1gInSc2	airbus
A	a	k9	a
<g/>
380	[number]	k4	380
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
dokáže	dokázat	k5eAaPmIp3nS	dokázat
pojmout	pojmout	k5eAaPmF	pojmout
555	[number]	k4	555
cestujících	cestující	k1gMnPc2	cestující
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
civilním	civilní	k2eAgNnSc7d1	civilní
dopravním	dopravní	k2eAgNnSc7d1	dopravní
letadlem	letadlo	k1gNnSc7	letadlo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Airbus	airbus	k1gInSc1	airbus
A380	A380	k1gFnSc2	A380
===	===	k?	===
</s>
</p>
<p>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
Airbus	airbus	k1gInSc4	airbus
A380	A380	k1gFnSc2	A380
úspěšně	úspěšně	k6eAd1	úspěšně
provedl	provést	k5eAaPmAgInS	provést
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
let	let	k1gInSc4	let
z	z	k7c2	z
francouzského	francouzský	k2eAgInSc2d1	francouzský
Toulouse	Toulouse	k1gInSc2	Toulouse
<g/>
.	.	kIx.	.
</s>
<s>
Let	let	k1gInSc1	let
trval	trvat	k5eAaImAgInS	trvat
skoro	skoro	k6eAd1	skoro
čtyři	čtyři	k4xCgFnPc1	čtyři
hodiny	hodina	k1gFnPc1	hodina
<g/>
,	,	kIx,	,
letoun	letoun	k1gInSc1	letoun
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
z	z	k7c2	z
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
letiště	letiště	k1gNnSc2	letiště
Toulouse	Toulouse	k1gInSc1	Toulouse
Blagnac	Blagnac	k1gFnSc4	Blagnac
v	v	k7c6	v
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
29	[number]	k4	29
UTC	UTC	kA	UTC
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
29	[number]	k4	29
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
letěl	letět	k5eAaImAgMnS	letět
západně	západně	k6eAd1	západně
k	k	k7c3	k
Atlantskému	atlantský	k2eAgInSc3d1	atlantský
oceánu	oceán	k1gInSc3	oceán
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
oceánem	oceán	k1gInSc7	oceán
se	se	k3xPyFc4	se
obrátil	obrátit	k5eAaPmAgMnS	obrátit
<g/>
,	,	kIx,	,
přeletěl	přeletět	k5eAaPmAgMnS	přeletět
Pyreneje	Pyreneje	k1gFnPc4	Pyreneje
a	a	k8xC	a
přistál	přistát	k5eAaPmAgInS	přistát
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Toulouse	Toulouse	k1gInSc2	Toulouse
Blagnac	Blagnac	k1gInSc1	Blagnac
v	v	k7c6	v
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
UTC	UTC	kA	UTC
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posádku	posádka	k1gFnSc4	posádka
tvořili	tvořit	k5eAaImAgMnP	tvořit
francouzští	francouzský	k2eAgMnPc1d1	francouzský
testovací	testovací	k2eAgMnPc1d1	testovací
piloti	pilot	k1gMnPc1	pilot
Jacques	Jacques	k1gMnSc1	Jacques
Rosay	Rosaa	k1gMnSc2	Rosaa
(	(	kIx(	(
<g/>
kapitán	kapitán	k1gMnSc1	kapitán
při	při	k7c6	při
vzletu	vzlet	k1gInSc6	vzlet
a	a	k8xC	a
první	první	k4xOgFnSc3	první
části	část	k1gFnSc3	část
letu	let	k1gInSc2	let
<g/>
)	)	kIx)	)
a	a	k8xC	a
Claude	Claud	k1gMnSc5	Claud
Lelaie	Lelaius	k1gMnSc5	Lelaius
(	(	kIx(	(
<g/>
kapitán	kapitán	k1gMnSc1	kapitán
při	při	k7c6	při
druhé	druhý	k4xOgFnSc6	druhý
části	část	k1gFnSc6	část
letu	let	k1gInSc2	let
a	a	k8xC	a
přistání	přistání	k1gNnSc2	přistání
<g/>
)	)	kIx)	)
a	a	k8xC	a
tři	tři	k4xCgMnPc1	tři
zkušební	zkušební	k2eAgMnPc1d1	zkušební
technici	technik	k1gMnPc1	technik
(	(	kIx(	(
<g/>
Španěl	Španěl	k1gMnSc1	Španěl
<g/>
,	,	kIx,	,
Francouz	Francouz	k1gMnSc1	Francouz
a	a	k8xC	a
Němec	Němec	k1gMnSc1	Němec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nedávnému	dávný	k2eNgInSc3d1	nedávný
francouzsko-německému	francouzskoěmecký	k2eAgInSc3d1	francouzsko-německý
sporu	spor	k1gInSc3	spor
o	o	k7c4	o
vedení	vedení	k1gNnSc4	vedení
EADS	EADS	kA	EADS
<g/>
,	,	kIx,	,
Airbus	airbus	k1gInSc1	airbus
vydal	vydat	k5eAaPmAgInS	vydat
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
členové	člen	k1gMnPc1	člen
posádky	posádka	k1gFnSc2	posádka
byli	být	k5eAaImAgMnP	být
vybráni	vybrat	k5eAaPmNgMnP	vybrat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
svých	svůj	k3xOyFgFnPc2	svůj
schopností	schopnost	k1gFnPc2	schopnost
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
podle	podle	k7c2	podle
svých	svůj	k3xOyFgFnPc2	svůj
národností	národnost	k1gFnPc2	národnost
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Toulouse	Toulouse	k1gInSc6	Toulouse
pro	pro	k7c4	pro
Singapore	Singapor	k1gInSc5	Singapor
Airlines	Airlinesa	k1gFnPc2	Airlinesa
předán	předat	k5eAaPmNgInS	předat
první	první	k4xOgInSc1	první
A	A	kA	A
<g/>
380	[number]	k4	380
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
komerční	komerční	k2eAgInSc1d1	komerční
let	let	k1gInSc1	let
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
odkoupil	odkoupit	k5eAaPmAgInS	odkoupit
Airbus	airbus	k1gInSc1	airbus
50	[number]	k4	50
%	%	kIx~	%
v	v	k7c6	v
programu	program	k1gInSc6	program
CSeries	CSeriesa	k1gFnPc2	CSeriesa
kanadské	kanadský	k2eAgFnSc2d1	kanadská
společnosti	společnost	k1gFnSc2	společnost
Bombardier	Bombardira	k1gFnPc2	Bombardira
Aerospace	Aerospace	k1gFnSc2	Aerospace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2018	[number]	k4	2018
dokoupil	dokoupit	k5eAaPmAgInS	dokoupit
zbytek	zbytek	k1gInSc4	zbytek
a	a	k8xC	a
sérii	série	k1gFnSc4	série
malých	malý	k2eAgNnPc2d1	malé
letadel	letadlo	k1gNnPc2	letadlo
CS100	CS100	k1gMnSc1	CS100
a	a	k8xC	a
CS300	CS300	k1gMnSc1	CS300
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
na	na	k7c4	na
Airbus	airbus	k1gInSc4	airbus
A220-100	A220-100	k1gFnSc2	A220-100
a	a	k8xC	a
A	a	k9	a
<g/>
220	[number]	k4	220
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Civilní	civilní	k2eAgFnSc1d1	civilní
výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
výrobkem	výrobek	k1gInSc7	výrobek
Airbusu	airbus	k1gInSc2	airbus
byl	být	k5eAaImAgInS	být
typ	typ	k1gInSc1	typ
A	A	kA	A
<g/>
300	[number]	k4	300
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
letoun	letoun	k1gInSc4	letoun
na	na	k7c6	na
světě	svět	k1gInSc6	svět
s	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
uličkami	ulička	k1gFnPc7	ulička
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
motory	motor	k1gInPc7	motor
<g/>
.	.	kIx.	.
</s>
<s>
Kratší	krátký	k2eAgFnSc1d2	kratší
varianta	varianta	k1gFnSc1	varianta
A300	A300	k1gFnSc2	A300
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
A	a	k9	a
<g/>
310	[number]	k4	310
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
svých	svůj	k3xOyFgInPc2	svůj
prvních	první	k4xOgInPc2	první
typů	typ	k1gInPc2	typ
představil	představit	k5eAaPmAgInS	představit
Airbus	airbus	k1gInSc1	airbus
typ	typ	k1gInSc4	typ
A320	A320	k1gFnSc2	A320
s	s	k7c7	s
inovativním	inovativní	k2eAgInSc7d1	inovativní
systémem	systém	k1gInSc7	systém
řízení	řízení	k1gNnSc2	řízení
fly-by-wire	flyyir	k1gInSc5	fly-by-wir
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
z	z	k7c2	z
kokpitu	kokpit	k1gInSc2	kokpit
se	se	k3xPyFc4	se
přenášejí	přenášet	k5eAaImIp3nP	přenášet
pouze	pouze	k6eAd1	pouze
elektrické	elektrický	k2eAgInPc1d1	elektrický
signály	signál	k1gInPc1	signál
k	k	k7c3	k
servomotorům	servomotor	k1gInPc3	servomotor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A320	A320	k4	A320
byl	být	k5eAaImAgInS	být
velkým	velký	k2eAgInSc7d1	velký
komerčním	komerční	k2eAgInSc7d1	komerční
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
A318	A318	k4	A318
a	a	k8xC	a
A319	A319	k1gFnPc1	A319
jsou	být	k5eAaImIp3nP	být
kratší	krátký	k2eAgFnPc1d2	kratší
verze	verze	k1gFnPc1	verze
A	a	k9	a
<g/>
320	[number]	k4	320
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
A319	A319	k1gFnSc2	A319
je	být	k5eAaImIp3nS	být
dodáván	dodávat	k5eAaImNgInS	dodávat
také	také	k9	také
v	v	k7c6	v
úpravě	úprava	k1gFnSc6	úprava
pro	pro	k7c4	pro
podnikovou	podnikový	k2eAgFnSc4d1	podniková
sféru	sféra	k1gFnSc4	sféra
jako	jako	k8xS	jako
biz-jet	bizet	k1gInSc4	biz-jet
(	(	kIx(	(
<g/>
Airbus	airbus	k1gInSc1	airbus
Corporate	Corporat	k1gInSc5	Corporat
Jet	jet	k5eAaImNgMnS	jet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A321	A321	k4	A321
je	být	k5eAaImIp3nS	být
prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
verze	verze	k1gFnSc1	verze
A320	A320	k1gFnSc2	A320
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přímým	přímý	k2eAgMnSc7d1	přímý
konkurentem	konkurent	k1gMnSc7	konkurent
nejnovějších	nový	k2eAgInPc2d3	nejnovější
modelů	model	k1gInPc2	model
Boeingu	boeing	k1gInSc2	boeing
737	[number]	k4	737
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
letadla	letadlo	k1gNnPc4	letadlo
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
doletem	dolet	k1gInSc7	dolet
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
Airbusu	airbus	k1gInSc2	airbus
<g/>
,	,	kIx,	,
dvoumotorový	dvoumotorový	k2eAgMnSc1d1	dvoumotorový
A330	A330	k1gMnSc1	A330
a	a	k8xC	a
čtyřmotorový	čtyřmotorový	k2eAgMnSc1d1	čtyřmotorový
A	a	k8xC	a
<g/>
340	[number]	k4	340
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
mají	mít	k5eAaImIp3nP	mít
moderní	moderní	k2eAgInSc4d1	moderní
design	design	k1gInSc4	design
křídel	křídlo	k1gNnPc2	křídlo
s	s	k7c7	s
winglety	winglet	k1gInPc7	winglet
<g/>
.	.	kIx.	.
</s>
<s>
Airbus	airbus	k1gInSc1	airbus
A340-500	A340-500	k1gFnSc2	A340-500
má	mít	k5eAaImIp3nS	mít
operační	operační	k2eAgInSc4d1	operační
dolet	dolet	k1gInSc4	dolet
16	[number]	k4	16
700	[number]	k4	700
kilometrů	kilometr	k1gInPc2	kilometr
(	(	kIx(	(
<g/>
9	[number]	k4	9
000	[number]	k4	000
námořních	námořní	k2eAgFnPc2d1	námořní
mil	míle	k1gFnPc2	míle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
u	u	k7c2	u
civilních	civilní	k2eAgNnPc2d1	civilní
letadel	letadlo	k1gNnPc2	letadlo
po	po	k7c6	po
Boeingu	boeing	k1gInSc6	boeing
777-200LR	[number]	k4	777-200LR
(	(	kIx(	(
<g/>
dolet	dolet	k1gInSc1	dolet
17	[number]	k4	17
446	[number]	k4	446
km	km	kA	km
neboli	neboli	k8xC	neboli
9	[number]	k4	9
420	[number]	k4	420
námořních	námořní	k2eAgFnPc2d1	námořní
mil	míle	k1gFnPc2	míle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
typy	typ	k1gInPc1	typ
si	se	k3xPyFc3	se
tvrdě	tvrdě	k6eAd1	tvrdě
konkurují	konkurovat	k5eAaImIp3nP	konkurovat
s	s	k7c7	s
podobnými	podobný	k2eAgInPc7d1	podobný
výrobky	výrobek	k1gInPc7	výrobek
Boeingu	boeing	k1gInSc2	boeing
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zčásti	zčásti	k6eAd1	zčásti
odpovědné	odpovědný	k2eAgFnPc1d1	odpovědná
za	za	k7c4	za
zastavení	zastavení	k1gNnSc4	zastavení
výroby	výroba	k1gFnSc2	výroba
civilních	civilní	k2eAgNnPc2d1	civilní
letadel	letadlo	k1gNnPc2	letadlo
u	u	k7c2	u
Lockheedu	Lockheed	k1gMnSc6	Lockheed
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
a	a	k8xC	a
převzetí	převzetí	k1gNnSc4	převzetí
firmy	firma	k1gFnSc2	firma
McDonnell	McDonnell	k1gMnSc1	McDonnell
Douglas	Douglas	k1gMnSc1	Douglas
dnes	dnes	k6eAd1	dnes
jediným	jediný	k2eAgMnSc7d1	jediný
americkým	americký	k2eAgMnSc7d1	americký
výrobcem	výrobce	k1gMnSc7	výrobce
civilních	civilní	k2eAgNnPc2d1	civilní
dopravních	dopravní	k2eAgNnPc2d1	dopravní
letadel	letadlo	k1gNnPc2	letadlo
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
doletem	dolet	k1gInSc7	dolet
Boeingem	boeing	k1gInSc7	boeing
v	v	k7c6	v
letech	let	k1gInPc6	let
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Airbus	airbus	k1gInSc1	airbus
je	být	k5eAaImIp3nS	být
obzvláště	obzvláště	k6eAd1	obzvláště
hrdá	hrdý	k2eAgFnSc1d1	hrdá
na	na	k7c4	na
své	svůj	k3xOyFgMnPc4	svůj
technologie	technologie	k1gFnSc1	technologie
fly-by-wire	flyyir	k1gInSc5	fly-by-wir
a	a	k8xC	a
na	na	k7c4	na
použití	použití	k1gNnSc4	použití
společného	společný	k2eAgInSc2d1	společný
kokpitu	kokpit	k1gInSc2	kokpit
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
typech	typ	k1gInPc6	typ
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zjednodušuje	zjednodušovat	k5eAaImIp3nS	zjednodušovat
výcvik	výcvik	k1gInSc4	výcvik
jejich	jejich	k3xOp3gFnPc2	jejich
posádek	posádka	k1gFnPc2	posádka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
*	*	kIx~	*
výroba	výroba	k1gFnSc1	výroba
ukončena	ukončen	k2eAgFnSc1d1	ukončena
</s>
</p>
<p>
<s>
*	*	kIx~	*
všechny	všechen	k3xTgInPc4	všechen
modely	model	k1gInPc4	model
</s>
</p>
<p>
<s>
==	==	k?	==
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1999	[number]	k4	1999
Airbus	airbus	k1gInSc1	airbus
založil	založit	k5eAaPmAgInS	založit
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
Airbus	airbus	k1gInSc4	airbus
Military	Militara	k1gFnSc2	Militara
S.	S.	kA	S.
<g/>
A.S.	A.S.	k1gFnSc2	A.S.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
vývojem	vývoj	k1gInSc7	vývoj
a	a	k8xC	a
výrobou	výroba	k1gFnSc7	výroba
turbovrtulového	turbovrtulový	k2eAgNnSc2d1	turbovrtulové
vojenského	vojenský	k2eAgNnSc2d1	vojenské
transportního	transportní	k2eAgNnSc2d1	transportní
letadla	letadlo	k1gNnSc2	letadlo
Airbus	airbus	k1gInSc4	airbus
A	a	k9	a
<g/>
400	[number]	k4	400
<g/>
M.	M.	kA	M.
A400M	A400M	k1gFnPc2	A400M
je	být	k5eAaImIp3nS	být
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
několika	několik	k4yIc7	několik
členy	člen	k1gInPc7	člen
NATO	NATO	kA	NATO
-	-	kIx~	-
Belgií	Belgie	k1gFnSc7	Belgie
<g/>
,	,	kIx,	,
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
Lucemburskem	Lucembursko	k1gNnSc7	Lucembursko
<g/>
,	,	kIx,	,
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
,	,	kIx,	,
Tureckem	Turecko	k1gNnSc7	Turecko
a	a	k8xC	a
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
-	-	kIx~	-
jako	jako	k8xS	jako
alternativa	alternativa	k1gFnSc1	alternativa
k	k	k7c3	k
C-130	C-130	k1gFnSc3	C-130
Hercules	Herculesa	k1gFnPc2	Herculesa
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
na	na	k7c4	na
trh	trh	k1gInSc4	trh
s	s	k7c7	s
vojenskými	vojenský	k2eAgNnPc7d1	vojenské
letadly	letadlo	k1gNnPc7	letadlo
sníží	snížit	k5eAaPmIp3nS	snížit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
úplně	úplně	k6eAd1	úplně
neodstraní	odstranit	k5eNaPmIp3nS	odstranit
<g/>
,	,	kIx,	,
zranitelnost	zranitelnost	k1gFnSc4	zranitelnost
Airbusu	airbus	k1gInSc2	airbus
cyklickými	cyklický	k2eAgInPc7d1	cyklický
poklesy	pokles	k1gInPc7	pokles
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
civilních	civilní	k2eAgNnPc2d1	civilní
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Airbus	airbus	k1gInSc1	airbus
A400M	A400M	k1gFnSc2	A400M
</s>
</p>
<p>
<s>
A310	A310	k4	A310
MRTT	MRTT	kA	MRTT
(	(	kIx(	(
<g/>
Multi	Mulť	k1gFnSc2	Mulť
Role	role	k1gFnSc2	role
Tanker	tanker	k1gInSc1	tanker
Transport	transport	k1gInSc1	transport
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
A330	A330	k4	A330
MRTT	MRTT	kA	MRTT
</s>
</p>
<p>
<s>
==	==	k?	==
Konkurenční	konkurenční	k2eAgInSc1d1	konkurenční
boj	boj	k1gInSc1	boj
s	s	k7c7	s
Boeingem	boeing	k1gInSc7	boeing
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
33	[number]	k4	33
<g/>
leté	letý	k2eAgFnSc6d1	letá
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
dodal	dodat	k5eAaPmAgMnS	dodat
Airbus	airbus	k1gInSc4	airbus
zákazníkům	zákazník	k1gMnPc3	zákazník
více	hodně	k6eAd2	hodně
proudových	proudový	k2eAgNnPc2d1	proudové
dopravních	dopravní	k2eAgNnPc2d1	dopravní
letadel	letadlo	k1gNnPc2	letadlo
než	než	k8xS	než
Boeing	boeing	k1gInSc4	boeing
<g/>
.	.	kIx.	.
</s>
<s>
Boeing	boeing	k1gInSc1	boeing
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
typu	typa	k1gFnSc4	typa
Boeing	boeing	k1gInSc1	boeing
777	[number]	k4	777
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeho	jeho	k3xOp3gInPc2	jeho
protějšků	protějšek	k1gInPc2	protějšek
A340	A340	k1gFnSc2	A340
a	a	k8xC	a
A	a	k9	a
<g/>
330	[number]	k4	330
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
menšího	malý	k2eAgMnSc2d2	menší
A	A	kA	A
<g/>
330	[number]	k4	330
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
konkuruje	konkurovat	k5eAaImIp3nS	konkurovat
typu	typa	k1gFnSc4	typa
Boeing	boeing	k1gInSc1	boeing
767	[number]	k4	767
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
Boeingova	Boeingův	k2eAgInSc2d1	Boeingův
protějšku	protějšek	k1gInSc2	protějšek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
očekávat	očekávat	k5eAaImF	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
Boeingu	boeing	k1gInSc2	boeing
787	[number]	k4	787
na	na	k7c4	na
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pozice	pozice	k1gFnSc1	pozice
americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
zlepší	zlepšit	k5eAaPmIp3nS	zlepšit
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
trumfnout	trumfnout	k5eAaPmF	trumfnout
prodeje	prodej	k1gInSc2	prodej
A	A	kA	A
<g/>
330	[number]	k4	330
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
A380	A380	k1gFnSc2	A380
lze	lze	k6eAd1	lze
očekávat	očekávat	k5eAaImF	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dále	daleko	k6eAd2	daleko
sníží	snížit	k5eAaPmIp3nS	snížit
odbyt	odbyt	k1gInSc1	odbyt
Boeingu	boeing	k1gInSc2	boeing
747	[number]	k4	747
a	a	k8xC	a
získá	získat	k5eAaPmIp3nS	získat
Airbusu	airbus	k1gInSc2	airbus
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
velmi	velmi	k6eAd1	velmi
velkých	velký	k2eAgNnPc2d1	velké
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Boeing	boeing	k1gInSc1	boeing
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
prodlouženou	prodloužený	k2eAgFnSc4d1	prodloužená
verzi	verze	k1gFnSc4	verze
svého	svůj	k3xOyFgMnSc2	svůj
Jumba	Jumb	k1gMnSc2	Jumb
<g/>
,	,	kIx,	,
747	[number]	k4	747
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bude	být	k5eAaImBp3nS	být
konkurovat	konkurovat	k5eAaImF	konkurovat
A	a	k9	a
<g/>
380	[number]	k4	380
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
8	[number]	k4	8
593	[number]	k4	593
letadel	letadlo	k1gNnPc2	letadlo
Boeing	boeing	k1gInSc4	boeing
proti	proti	k7c3	proti
4092	[number]	k4	4092
letadlům	letadlo	k1gNnPc3	letadlo
Airbus	airbus	k1gInSc4	airbus
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
poměru	poměra	k1gFnSc4	poměra
asi	asi	k9	asi
2,1	[number]	k4	2,1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Údaj	údaj	k1gInSc1	údaj
za	za	k7c4	za
firmu	firma	k1gFnSc4	firma
Boeing	boeing	k1gInSc1	boeing
však	však	k9	však
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
období	období	k1gNnSc4	období
1974	[number]	k4	1974
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
Airbus	airbus	k1gInSc1	airbus
začal	začít	k5eAaPmAgInS	začít
dodávat	dodávat	k5eAaImF	dodávat
letadla	letadlo	k1gNnPc4	letadlo
až	až	k6eAd1	až
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
konkurence	konkurence	k1gFnSc1	konkurence
obou	dva	k4xCgFnPc2	dva
firem	firma	k1gFnPc2	firma
přiostřuje	přiostřovat	k5eAaImIp3nS	přiostřovat
<g/>
,	,	kIx,	,
získává	získávat	k5eAaImIp3nS	získávat
Airbus	airbus	k1gInSc1	airbus
častěji	často	k6eAd2	často
víc	hodně	k6eAd2	hodně
objednávek	objednávka	k1gFnPc2	objednávka
na	na	k7c4	na
nová	nový	k2eAgNnPc4d1	nové
letadla	letadlo	k1gNnPc4	letadlo
než	než	k8xS	než
Boeing	boeing	k1gInSc4	boeing
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
i	i	k9	i
poměr	poměr	k1gInSc4	poměr
všech	všecek	k3xTgNnPc2	všecek
provozovaných	provozovaný	k2eAgNnPc2d1	provozované
dopravních	dopravní	k2eAgNnPc2d1	dopravní
letadel	letadlo	k1gNnPc2	letadlo
pozvolna	pozvolna	k6eAd1	pozvolna
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
činil	činit	k5eAaImAgInS	činit
9965	[number]	k4	9965
<g/>
:	:	kIx,	:
<g/>
7813	[number]	k4	7813
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Boeingu	boeing	k1gInSc2	boeing
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
poměr	poměr	k1gInSc1	poměr
1,28	[number]	k4	1,28
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
dodal	dodat	k5eAaPmAgMnS	dodat
Airbus	airbus	k1gInSc4	airbus
svým	svůj	k3xOyFgMnPc3	svůj
zákazníkům	zákazník	k1gMnPc3	zákazník
635	[number]	k4	635
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
Boeing	boeing	k1gInSc4	boeing
752	[number]	k4	752
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
období	období	k1gNnSc6	období
získal	získat	k5eAaPmAgInS	získat
Airbus	airbus	k1gInSc1	airbus
1036	[number]	k4	1036
nových	nový	k2eAgFnPc2d1	nová
objednávek	objednávka	k1gFnPc2	objednávka
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
Boeing	boeing	k1gInSc1	boeing
869	[number]	k4	869
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
měl	mít	k5eAaImAgInS	mít
Airbus	airbus	k1gInSc1	airbus
6787	[number]	k4	6787
nevyřízených	vyřízený	k2eNgFnPc2d1	nevyřízená
objednávek	objednávka	k1gFnPc2	objednávka
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
Boeing	boeing	k1gInSc4	boeing
5813	[number]	k4	5813
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vládní	vládní	k2eAgFnSc1d1	vládní
podpora	podpora	k1gFnSc1	podpora
===	===	k?	===
</s>
</p>
<p>
<s>
Boeing	boeing	k1gInSc4	boeing
si	se	k3xPyFc3	se
již	již	k6eAd1	již
mnoho	mnoho	k4c1	mnoho
let	léto	k1gNnPc2	léto
stěžuje	stěžovat	k5eAaImIp3nS	stěžovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Airbus	airbus	k1gInSc1	airbus
dostává	dostávat	k5eAaImIp3nS	dostávat
od	od	k7c2	od
evropských	evropský	k2eAgFnPc2d1	Evropská
vlád	vláda	k1gFnPc2	vláda
finanční	finanční	k2eAgFnSc4d1	finanční
a	a	k8xC	a
jinou	jiný	k2eAgFnSc4d1	jiná
pomoc	pomoc	k1gFnSc4	pomoc
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
launch	launch	k1gMnSc1	launch
aid	aid	k?	aid
<g/>
)	)	kIx)	)
při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
svých	svůj	k3xOyFgNnPc2	svůj
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Airbus	airbus	k1gInSc1	airbus
kontruje	kontrovat	k5eAaImIp3nS	kontrovat
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Boeing	boeing	k1gInSc1	boeing
dostává	dostávat	k5eAaImIp3nS	dostávat
zakázanou	zakázaný	k2eAgFnSc4d1	zakázaná
pomoc	pomoc	k1gFnSc4	pomoc
přes	přes	k7c4	přes
vojenské	vojenský	k2eAgInPc4d1	vojenský
a	a	k8xC	a
výzkumné	výzkumný	k2eAgInPc4d1	výzkumný
kontrakty	kontrakt	k1gInPc4	kontrakt
a	a	k8xC	a
daňové	daňový	k2eAgFnPc4d1	daňová
úlevy	úleva	k1gFnPc4	úleva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2004	[number]	k4	2004
Harry	Harra	k1gFnSc2	Harra
Stonecipher	Stoneciphra	k1gFnPc2	Stoneciphra
(	(	kIx(	(
<g/>
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Boeingu	boeing	k1gInSc2	boeing
<g/>
)	)	kIx)	)
obvinil	obvinit	k5eAaPmAgInS	obvinit
Airbus	airbus	k1gInSc1	airbus
z	z	k7c2	z
porušování	porušování	k1gNnSc2	porušování
nezávazné	závazný	k2eNgFnSc2d1	nezávazná
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
vládní	vládní	k2eAgFnSc6d1	vládní
pomoci	pomoc	k1gFnSc6	pomoc
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Airbus	airbus	k1gInSc1	airbus
dostává	dostávat	k5eAaImIp3nS	dostávat
finanční	finanční	k2eAgFnPc4d1	finanční
půjčky	půjčka	k1gFnPc4	půjčka
od	od	k7c2	od
evropských	evropský	k2eAgFnPc2d1	Evropská
vlád	vláda	k1gFnPc2	vláda
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
nových	nový	k2eAgNnPc2d1	nové
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
musí	muset	k5eAaImIp3nP	muset
později	pozdě	k6eAd2	pozdě
vrátit	vrátit	k5eAaPmF	vrátit
i	i	k9	i
s	s	k7c7	s
jistým	jistý	k2eAgInSc7d1	jistý
úrokem	úrok	k1gInSc7	úrok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
daný	daný	k2eAgInSc4d1	daný
typ	typ	k1gInSc4	typ
letadla	letadlo	k1gNnSc2	letadlo
komerčně	komerčně	k6eAd1	komerčně
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Airbus	airbus	k1gInSc1	airbus
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
takový	takový	k3xDgInSc1	takový
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
dohodou	dohoda	k1gFnSc7	dohoda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
a	a	k8xC	a
pravidly	pravidlo	k1gNnPc7	pravidlo
Světové	světový	k2eAgFnSc2d1	světová
obchodní	obchodní	k2eAgFnSc2d1	obchodní
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
WTO	WTO	kA	WTO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
až	až	k9	až
33	[number]	k4	33
<g/>
%	%	kIx~	%
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
program	program	k1gInSc4	program
bylo	být	k5eAaImAgNnS	být
hrazeno	hradit	k5eAaImNgNnS	hradit
vládními	vládní	k2eAgFnPc7d1	vládní
půjčkami	půjčka	k1gFnPc7	půjčka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
plně	plně	k6eAd1	plně
splaceny	splatit	k5eAaPmNgInP	splatit
během	během	k7c2	během
17	[number]	k4	17
let	léto	k1gNnPc2	léto
včetně	včetně	k7c2	včetně
všech	všecek	k3xTgInPc2	všecek
úroků	úrok	k1gInPc2	úrok
a	a	k8xC	a
poplatků	poplatek	k1gInPc2	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1	minimální
úroková	úrokový	k2eAgFnSc1d1	úroková
míra	míra	k1gFnSc1	míra
těchto	tento	k3xDgFnPc2	tento
půjček	půjčka	k1gFnPc2	půjčka
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
nákladům	náklad	k1gInPc3	náklad
vlád	vláda	k1gFnPc2	vláda
na	na	k7c4	na
půjčky	půjčka	k1gFnPc4	půjčka
plus	plus	k1gNnSc1	plus
0,25	[number]	k4	0,25
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
úrokovou	úrokový	k2eAgFnSc7d1	úroková
mírou	míra	k1gFnSc7	míra
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
by	by	kYmCp3nS	by
Airbus	airbus	k1gInSc1	airbus
musel	muset	k5eAaImAgInS	muset
zaplatit	zaplatit	k5eAaPmF	zaplatit
na	na	k7c6	na
volném	volný	k2eAgInSc6d1	volný
trhu	trh	k1gInSc6	trh
bez	bez	k7c2	bez
podpory	podpora	k1gFnSc2	podpora
vlád	vláda	k1gFnPc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Airbus	airbus	k1gInSc1	airbus
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
podpisu	podpis	k1gInSc2	podpis
evropsko-americké	evropskomerický	k2eAgFnSc2d1	evropsko-americká
dohody	dohoda	k1gFnSc2	dohoda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
splatil	splatit	k5eAaPmAgMnS	splatit
evropským	evropský	k2eAgFnPc3d1	Evropská
vládám	vláda	k1gFnPc3	vláda
více	hodně	k6eAd2	hodně
než	než	k8xS	než
6,7	[number]	k4	6,7
miliardy	miliarda	k4xCgFnPc4	miliarda
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
40	[number]	k4	40
<g/>
%	%	kIx~	%
více	hodně	k6eAd2	hodně
než	než	k8xS	než
kolik	kolik	k9	kolik
dostal	dostat	k5eAaPmAgMnS	dostat
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
Airbus	airbus	k1gInSc4	airbus
argumentuje	argumentovat	k5eAaImIp3nS	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tučné	tučný	k2eAgInPc4d1	tučný
vojenské	vojenský	k2eAgInPc4d1	vojenský
kontrakty	kontrakt	k1gInPc4	kontrakt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
dostává	dostávat	k5eAaImIp3nS	dostávat
Boeing	boeing	k1gInSc1	boeing
(	(	kIx(	(
<g/>
druhý	druhý	k4xOgMnSc1	druhý
největší	veliký	k2eAgMnSc1d3	veliký
americký	americký	k2eAgMnSc1d1	americký
dodavatel	dodavatel	k1gMnSc1	dodavatel
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
formou	forma	k1gFnSc7	forma
vládní	vládní	k2eAgFnSc2d1	vládní
pomoci	pomoc	k1gFnSc2	pomoc
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
skandál	skandál	k1gInSc1	skandál
kolem	kolem	k7c2	kolem
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
tankerů	tanker	k1gInPc2	tanker
Boeing	boeing	k1gInSc4	boeing
KC-	KC-	k1gFnSc2	KC-
<g/>
767	[number]	k4	767
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
i	i	k9	i
Airbus	airbus	k1gInSc1	airbus
dostává	dostávat	k5eAaImIp3nS	dostávat
vojenské	vojenský	k2eAgFnPc4d1	vojenská
zakázky	zakázka	k1gFnPc4	zakázka
od	od	k7c2	od
různých	různý	k2eAgFnPc2d1	různá
vlád	vláda	k1gFnPc2	vláda
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nS	by
chybou	chyba	k1gFnSc7	chyba
se	se	k3xPyFc4	se
domnívat	domnívat	k5eAaImF	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
strana	strana	k1gFnSc1	strana
dostává	dostávat	k5eAaImIp3nS	dostávat
státní	státní	k2eAgFnSc4d1	státní
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Podpora	podpora	k1gFnSc1	podpora
americké	americký	k2eAgFnSc2d1	americká
vlády	vláda	k1gFnSc2	vláda
vývoji	vývoj	k1gInSc3	vývoj
technologií	technologie	k1gFnPc2	technologie
v	v	k7c6	v
NASA	NASA	kA	NASA
je	být	k5eAaImIp3nS	být
také	také	k9	také
významnou	významný	k2eAgFnSc7d1	významná
podporou	podpora	k1gFnSc7	podpora
pro	pro	k7c4	pro
Boeing	boeing	k1gInSc4	boeing
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
daňové	daňový	k2eAgFnPc1d1	daňová
úlevy	úleva	k1gFnPc1	úleva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
dohody	dohoda	k1gFnSc2	dohoda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
a	a	k8xC	a
pravidel	pravidlo	k1gNnPc2	pravidlo
WTO	WTO	kA	WTO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
svých	svůj	k3xOyFgMnPc2	svůj
nejnovějších	nový	k2eAgMnPc2d3	Nejnovější
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
787	[number]	k4	787
<g/>
,	,	kIx,	,
Boeing	boeing	k1gInSc4	boeing
dostal	dostat	k5eAaPmAgMnS	dostat
nabídky	nabídka	k1gFnPc4	nabídka
podpory	podpora	k1gFnSc2	podpora
od	od	k7c2	od
vlád	vláda	k1gFnPc2	vláda
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
z	z	k7c2	z
výsledků	výsledek	k1gInPc2	výsledek
podpory	podpora	k1gFnSc2	podpora
americké	americký	k2eAgFnSc2d1	americká
vlády	vláda	k1gFnSc2	vláda
vývoji	vývoj	k1gInSc6	vývoj
technologií	technologie	k1gFnPc2	technologie
může	moct	k5eAaImIp3nS	moct
těžit	těžit	k5eAaImF	těžit
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
<g/>
,	,	kIx,	,
i	i	k9	i
Airbus	airbus	k1gInSc1	airbus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2005	[number]	k4	2005
zástupci	zástupce	k1gMnPc7	zástupce
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Mandelson	Mandelson	k1gMnSc1	Mandelson
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
Zoellick	Zoellick	k1gMnSc1	Zoellick
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
vystřídaný	vystřídaný	k2eAgMnSc1d1	vystřídaný
Robem	Robem	k?	Robem
Portmanem	Portman	k1gInSc7	Portman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
se	s	k7c7	s
zahájením	zahájení	k1gNnSc7	zahájení
rozhovorů	rozhovor	k1gInPc2	rozhovor
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
vyřešily	vyřešit	k5eAaPmAgFnP	vyřešit
rostoucí	rostoucí	k2eAgNnSc4d1	rostoucí
napětí	napětí	k1gNnSc4	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Rozhovory	rozhovor	k1gInPc1	rozhovor
byly	být	k5eAaImAgInP	být
neúspěšné	úspěšný	k2eNgInPc1d1	neúspěšný
a	a	k8xC	a
spor	spor	k1gInSc1	spor
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
vyostřil	vyostřit	k5eAaPmAgMnS	vyostřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spor	spor	k1gInSc1	spor
u	u	k7c2	u
Světové	světový	k2eAgFnSc2d1	světová
obchodní	obchodní	k2eAgFnSc2d1	obchodní
organizace	organizace	k1gFnSc2	organizace
===	===	k?	===
</s>
</p>
<p>
<s>
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
podaly	podat	k5eAaPmAgInP	podat
žalobu	žaloba	k1gFnSc4	žaloba
u	u	k7c2	u
Světové	světový	k2eAgFnSc2d1	světová
obchodní	obchodní	k2eAgFnSc2d1	obchodní
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
WTO	WTO	kA	WTO
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
kvůli	kvůli	k7c3	kvůli
poskytování	poskytování	k1gNnSc3	poskytování
nezákonných	zákonný	k2eNgFnPc2d1	nezákonná
dotací	dotace	k1gFnPc2	dotace
Airbusu	airbus	k1gInSc2	airbus
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
později	pozdě	k6eAd2	pozdě
EU	EU	kA	EU
podala	podat	k5eAaPmAgFnS	podat
žalobu	žaloba	k1gFnSc4	žaloba
na	na	k7c4	na
USA	USA	kA	USA
za	za	k7c4	za
skrytou	skrytý	k2eAgFnSc4d1	skrytá
podporu	podpora	k1gFnSc4	podpora
Boeingu	boeing	k1gInSc2	boeing
<g/>
.	.	kIx.	.
<g/>
Portman	Portman	k1gMnSc1	Portman
(	(	kIx(	(
<g/>
za	za	k7c4	za
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mandelson	Mandelson	k1gInSc1	Mandelson
(	(	kIx(	(
<g/>
za	za	k7c4	za
EU	EU	kA	EU
<g/>
)	)	kIx)	)
následně	následně	k6eAd1	následně
vydali	vydat	k5eAaPmAgMnP	vydat
společné	společný	k2eAgNnSc4d1	společné
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zůstáváme	zůstávat	k5eAaImIp1nP	zůstávat
jednotni	jednotnit	k5eAaImRp2nS	jednotnit
v	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
odhodlání	odhodlání	k1gNnSc6	odhodlání
nepřipustit	připustit	k5eNaPmF	připustit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tento	tento	k3xDgInSc1	tento
spor	spor	k1gInSc1	spor
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
naši	náš	k3xOp1gFnSc4	náš
spolupráci	spolupráce	k1gFnSc4	spolupráce
v	v	k7c6	v
širších	široký	k2eAgFnPc6d2	širší
bilaterálních	bilaterální	k2eAgFnPc6d1	bilaterální
a	a	k8xC	a
multilaterálních	multilaterální	k2eAgFnPc6d1	multilaterální
obchodních	obchodní	k2eAgFnPc6d1	obchodní
otázkách	otázka	k1gFnPc6	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Doposud	doposud	k6eAd1	doposud
jsme	být	k5eAaImIp1nP	být
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
úrovni	úroveň	k1gFnSc6	úroveň
a	a	k8xC	a
míníme	mínit	k5eAaImIp1nP	mínit
v	v	k7c6	v
tom	ten	k3xDgMnSc6	ten
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Napětí	napětí	k1gNnSc1	napětí
způsobené	způsobený	k2eAgFnPc1d1	způsobená
podporou	podpora	k1gFnSc7	podpora
Airbusu	airbus	k1gInSc2	airbus
A380	A380	k1gMnPc2	A380
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
pokraj	pokraj	k1gInSc4	pokraj
obchodní	obchodní	k2eAgFnSc2d1	obchodní
války	válka	k1gFnSc2	válka
s	s	k7c7	s
ohlášením	ohlášení	k1gNnSc7	ohlášení
podpory	podpora	k1gFnSc2	podpora
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgInPc4d1	budoucí
A	a	k9	a
<g/>
350	[number]	k4	350
<g/>
.	.	kIx.	.
</s>
<s>
Airbus	airbus	k1gInSc1	airbus
by	by	kYmCp3nS	by
rád	rád	k6eAd1	rád
pokryl	pokrýt	k5eAaPmAgInS	pokrýt
třetinu	třetina	k1gFnSc4	třetina
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
A350	A350	k1gFnPc2	A350
z	z	k7c2	z
vládních	vládní	k2eAgInPc2d1	vládní
úvěrů	úvěr	k1gInPc2	úvěr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nutnosti	nutnost	k1gFnSc2	nutnost
je	být	k5eAaImIp3nS	být
připraven	připraven	k2eAgMnSc1d1	připraven
se	se	k3xPyFc4	se
obejít	obejít	k5eAaPmF	obejít
i	i	k9	i
bez	bez	k7c2	bez
těchto	tento	k3xDgFnPc2	tento
půjček	půjčka	k1gFnPc2	půjčka
<g/>
.	.	kIx.	.
</s>
<s>
A350	A350	k4	A350
bude	být	k5eAaImBp3nS	být
přímým	přímý	k2eAgMnSc7d1	přímý
konkurentem	konkurent	k1gMnSc7	konkurent
nejúspěšnějšího	úspěšný	k2eAgInSc2d3	nejúspěšnější
projektu	projekt	k1gInSc2	projekt
Boeingu	boeing	k1gInSc2	boeing
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
-	-	kIx~	-
787	[number]	k4	787
Dreamliner	Dreamlinra	k1gFnPc2	Dreamlinra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vysocí	vysoký	k2eAgMnPc1d1	vysoký
úředníci	úředník	k1gMnPc1	úředník
EU	EU	kA	EU
pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
pochybnosti	pochybnost	k1gFnPc1	pochybnost
o	o	k7c4	o
financování	financování	k1gNnSc4	financování
vývoje	vývoj	k1gInSc2	vývoj
Boeingu	boeing	k1gInSc2	boeing
787	[number]	k4	787
japonskou	japonský	k2eAgFnSc7d1	japonská
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
japonskými	japonský	k2eAgFnPc7d1	japonská
společnostmi	společnost	k1gFnPc7	společnost
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
některými	některý	k3yIgInPc7	některý
státy	stát	k1gInPc7	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
konkurence	konkurence	k1gFnSc1	konkurence
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
===	===	k?	===
</s>
</p>
<p>
<s>
Dvě	dva	k4xCgFnPc1	dva
velké	velký	k2eAgFnPc1d1	velká
japonské	japonský	k2eAgFnPc1d1	japonská
letecké	letecký	k2eAgFnPc1d1	letecká
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
Japan	japan	k1gInSc1	japan
Airlines	Airlinesa	k1gFnPc2	Airlinesa
a	a	k8xC	a
All	All	k1gFnPc2	All
Nippon	Nippona	k1gFnPc2	Nippona
Airways	Airways	k1gInSc1	Airways
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
velmi	velmi	k6eAd1	velmi
loajální	loajální	k2eAgMnPc1d1	loajální
zákazníci	zákazník	k1gMnPc1	zákazník
Boeingu	boeing	k1gInSc2	boeing
<g/>
,	,	kIx,	,
stroje	stroj	k1gInSc2	stroj
tohoto	tento	k3xDgMnSc2	tento
amerického	americký	k2eAgMnSc2d1	americký
výrobce	výrobce	k1gMnSc2	výrobce
tvoří	tvořit	k5eAaImIp3nS	tvořit
asi	asi	k9	asi
90	[number]	k4	90
<g/>
%	%	kIx~	%
flotily	flotila	k1gFnPc4	flotila
obou	dva	k4xCgMnPc2	dva
dopravců	dopravce	k1gMnPc2	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
aerolinky	aerolinka	k1gFnPc1	aerolinka
provozují	provozovat	k5eAaImIp3nP	provozovat
i	i	k9	i
několik	několik	k4yIc4	několik
strojů	stroj	k1gInPc2	stroj
Airbus	airbus	k1gInSc4	airbus
(	(	kIx(	(
<g/>
JAL	jmout	k5eAaPmAgInS	jmout
získal	získat	k5eAaPmAgMnS	získat
několik	několik	k4yIc4	několik
A300	A300	k1gFnPc2	A300
po	po	k7c4	po
převzetí	převzetí	k1gNnSc4	převzetí
Japan	japan	k1gInSc4	japan
Air	Air	k1gMnSc7	Air
System	Syst	k1gMnSc7	Syst
a	a	k8xC	a
All	All	k1gMnSc7	All
Nippon	Nippona	k1gFnPc2	Nippona
Airways	Airways	k1gInSc1	Airways
si	se	k3xPyFc3	se
objednaly	objednat	k5eAaPmAgFnP	objednat
několik	několik	k4yIc1	několik
A	a	k8xC	a
<g/>
320	[number]	k4	320
<g/>
/	/	kIx~	/
<g/>
A	a	k9	a
<g/>
321	[number]	k4	321
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ANA	ANA	kA	ANA
plánovala	plánovat	k5eAaImAgFnS	plánovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
737	[number]	k4	737
NG	NG	kA	NG
a	a	k8xC	a
787	[number]	k4	787
na	na	k7c4	na
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
flotile	flotila	k1gFnSc6	flotila
pouze	pouze	k6eAd1	pouze
letouny	letoun	k1gMnPc4	letoun
Boeing	boeing	k1gInSc4	boeing
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
úspěchem	úspěch	k1gInSc7	úspěch
pro	pro	k7c4	pro
Airbus	airbus	k1gInSc4	airbus
závazná	závazný	k2eAgFnSc1d1	závazná
objednávka	objednávka	k1gFnSc1	objednávka
od	od	k7c2	od
Sagawa	Sagaw	k1gInSc2	Sagaw
Express	express	k1gInSc1	express
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
A300-600F	A300-600F	k1gFnPc2	A300-600F
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
opci	opce	k1gFnSc4	opce
na	na	k7c4	na
stejný	stejný	k2eAgInSc4d1	stejný
typ	typ	k1gInSc4	typ
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
nové	nový	k2eAgFnPc1d1	nová
japonské	japonský	k2eAgFnPc1d1	japonská
aerolinky	aerolinka	k1gFnPc1	aerolinka
Star	star	k1gFnSc2	star
Flyer	Flyra	k1gFnPc2	Flyra
budou	být	k5eAaImBp3nP	být
používat	používat	k5eAaImF	používat
A320-200	A320-200	k1gMnPc1	A320-200
na	na	k7c6	na
linkách	linka	k1gFnPc6	linka
z	z	k7c2	z
nového	nový	k2eAgNnSc2d1	nové
letiště	letiště	k1gNnSc2	letiště
v	v	k7c6	v
Kitakjúšú	Kitakjúšú	k1gFnSc6	Kitakjúšú
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2005	[number]	k4	2005
si	se	k3xPyFc3	se
ANA	ANA	kA	ANA
objednaly	objednat	k5eAaPmAgFnP	objednat
pět	pět	k4xCc1	pět
A	a	k8xC	a
<g/>
320	[number]	k4	320
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
jako	jako	k9	jako
dočasné	dočasný	k2eAgNnSc4d1	dočasné
řešení	řešení	k1gNnSc4	řešení
než	než	k8xS	než
budou	být	k5eAaImBp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
Boeingy	boeing	k1gInPc1	boeing
737NG	[number]	k4	737NG
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
Generation	Generation	k1gInSc1	Generation
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ANA	ANA	kA	ANA
nezměnily	změnit	k5eNaPmAgFnP	změnit
svůj	svůj	k3xOyFgInSc4	svůj
úmysl	úmysl	k1gInSc4	úmysl
prodat	prodat	k5eAaPmF	prodat
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
všechny	všechen	k3xTgInPc4	všechen
své	svůj	k3xOyFgInPc4	svůj
Airbusy	airbus	k1gInPc4	airbus
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
však	však	k9	však
situace	situace	k1gFnSc1	situace
také	také	k9	také
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2014	[number]	k4	2014
a	a	k8xC	a
2015	[number]	k4	2015
objednala	objednat	k5eAaPmAgFnS	objednat
společnost	společnost	k1gFnSc1	společnost
ANA	ANA	kA	ANA
32	[number]	k4	32
letadel	letadlo	k1gNnPc2	letadlo
Airbus	airbus	k1gInSc4	airbus
A320	A320	k1gFnPc4	A320
a	a	k8xC	a
tři	tři	k4xCgNnPc4	tři
letadla	letadlo	k1gNnPc4	letadlo
A	a	k8xC	a
380	[number]	k4	380
<g/>
,	,	kIx,	,
Japan	japan	k1gInSc1	japan
Airlines	Airlinesa	k1gFnPc2	Airlinesa
objednaly	objednat	k5eAaPmAgFnP	objednat
31	[number]	k4	31
kusů	kus	k1gInPc2	kus
letadla	letadlo	k1gNnSc2	letadlo
A	a	k9	a
<g/>
350	[number]	k4	350
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
faktorem	faktor	k1gInSc7	faktor
ovlivňujícím	ovlivňující	k2eAgInSc7d1	ovlivňující
loajalitu	loajalita	k1gFnSc4	loajalita
japonských	japonský	k2eAgMnPc2d1	japonský
dopravců	dopravce	k1gMnPc2	dopravce
k	k	k7c3	k
Boeingu	boeing	k1gInSc3	boeing
je	být	k5eAaImIp3nS	být
partnerství	partnerství	k1gNnSc1	partnerství
Boeingu	boeing	k1gInSc2	boeing
a	a	k8xC	a
několika	několik	k4yIc2	několik
japonských	japonský	k2eAgNnPc2d1	Japonské
konsorcií	konsorcium	k1gNnPc2	konsorcium
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
částí	část	k1gFnPc2	část
letadel	letadlo	k1gNnPc2	letadlo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
křídel	křídlo	k1gNnPc2	křídlo
typu	typ	k1gInSc2	typ
787	[number]	k4	787
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
byly	být	k5eAaImAgFnP	být
japonské	japonský	k2eAgFnPc1d1	japonská
letecké	letecký	k2eAgFnPc1d1	letecká
společnosti	společnost	k1gFnPc1	společnost
konzultovány	konzultovat	k5eAaImNgFnP	konzultovat
při	při	k7c6	při
návrhu	návrh	k1gInSc6	návrh
typu	typ	k1gInSc2	typ
777	[number]	k4	777
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
výrobci	výrobce	k1gMnPc1	výrobce
a	a	k8xC	a
aerolinky	aerolinka	k1gFnPc1	aerolinka
odlišné	odlišný	k2eAgFnSc2d1	odlišná
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
japonská	japonský	k2eAgFnSc1d1	japonská
vláda	vláda	k1gFnSc1	vláda
velmi	velmi	k6eAd1	velmi
podporuje	podporovat	k5eAaImIp3nS	podporovat
spolupráci	spolupráce	k1gFnSc4	spolupráce
leteckého	letecký	k2eAgInSc2d1	letecký
průmyslu	průmysl	k1gInSc2	průmysl
s	s	k7c7	s
Boeingem	boeing	k1gInSc7	boeing
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výrobní	výrobní	k2eAgInPc1d1	výrobní
závody	závod	k1gInPc1	závod
==	==	k?	==
</s>
</p>
<p>
<s>
Pět	pět	k4xCc1	pět
konečných	konečný	k2eAgFnPc2d1	konečná
montážních	montážní	k2eAgFnPc2d1	montážní
linek	linka	k1gFnPc2	linka
Airbusu	airbus	k1gInSc2	airbus
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
na	na	k7c6	na
následujících	následující	k2eAgNnPc6d1	následující
místech	místo	k1gNnPc6	místo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Toulouse	Toulouse	k1gInSc1	Toulouse
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
dvě	dva	k4xCgFnPc1	dva
linky	linka	k1gFnPc1	linka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hamburk	Hamburk	k1gInSc1	Hamburk
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
linka	linka	k1gFnSc1	linka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sevilla	Sevilla	k1gFnSc1	Sevilla
(	(	kIx(	(
<g/>
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
Airbus	airbus	k1gInSc4	airbus
A	a	k9	a
<g/>
400	[number]	k4	400
<g/>
M	M	kA	M
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tchien-ťin	Tchien-ťin	k1gMnSc1	Tchien-ťin
(	(	kIx(	(
<g/>
ČLR	ČLR	kA	ČLR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mobile	mobile	k1gNnSc1	mobile
(	(	kIx(	(
<g/>
Alabama	Alabama	k1gFnSc1	Alabama
<g/>
)	)	kIx)	)
<g/>
Airbus	airbus	k1gInSc1	airbus
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
množství	množství	k1gNnSc1	množství
dalších	další	k2eAgInPc2d1	další
výrobních	výrobní	k2eAgInPc2d1	výrobní
závodů	závod	k1gInPc2	závod
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odráží	odrážet	k5eAaImIp3nS	odrážet
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
založen	založit	k5eAaPmNgInS	založit
jako	jako	k8xS	jako
konsorcium	konsorcium	k1gNnSc1	konsorcium
firem	firma	k1gFnPc2	firma
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Originálním	originální	k2eAgNnSc7d1	originální
řešením	řešení	k1gNnSc7	řešení
problému	problém	k1gInSc2	problém
přesouvání	přesouvání	k1gNnSc2	přesouvání
částí	část	k1gFnPc2	část
konstrukce	konstrukce	k1gFnSc2	konstrukce
letadel	letadlo	k1gNnPc2	letadlo
mezi	mezi	k7c7	mezi
výrobními	výrobní	k2eAgInPc7d1	výrobní
a	a	k8xC	a
montážními	montážní	k2eAgInPc7d1	montážní
závody	závod	k1gInPc7	závod
je	být	k5eAaImIp3nS	být
používání	používání	k1gNnSc1	používání
"	"	kIx"	"
<g/>
Belugy	Beluga	k1gFnPc1	Beluga
<g/>
"	"	kIx"	"
–	–	k?	–
speciálně	speciálně	k6eAd1	speciálně
zvětšeného	zvětšený	k2eAgInSc2d1	zvětšený
letounu	letoun	k1gInSc2	letoun
A	A	kA	A
<g/>
300	[number]	k4	300
<g/>
,	,	kIx,	,
schopného	schopný	k2eAgMnSc2d1	schopný
převážet	převážet	k5eAaImF	převážet
rozměrné	rozměrný	k2eAgFnSc2d1	rozměrná
části	část	k1gFnSc2	část
trupů	trup	k1gMnPc2	trup
letadel	letadlo	k1gNnPc2	letadlo
Airbus	airbus	k1gInSc1	airbus
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
řešením	řešení	k1gNnSc7	řešení
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
také	také	k9	také
Boeing	boeing	k1gInSc4	boeing
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
svůj	svůj	k3xOyFgInSc4	svůj
Boeing	boeing	k1gInSc4	boeing
747	[number]	k4	747
přizpůsobil	přizpůsobit	k5eAaPmAgMnS	přizpůsobit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pojmul	pojmout	k5eAaPmAgMnS	pojmout
trup	trup	k1gMnSc1	trup
Boeingu	boeing	k1gInSc2	boeing
787	[number]	k4	787
a	a	k8xC	a
nazval	nazvat	k5eAaBmAgInS	nazvat
ho	on	k3xPp3gInSc4	on
"	"	kIx"	"
<g/>
Dreamlifter	Dreamlifter	k1gInSc4	Dreamlifter
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
A	a	k8xC	a
<g/>
380	[number]	k4	380
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
trup	trup	k1gInSc1	trup
a	a	k8xC	a
křídla	křídlo	k1gNnSc2	křídlo
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
rozměrné	rozměrný	k2eAgFnPc1d1	rozměrná
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nP	aby
jejich	jejich	k3xOp3gFnPc1	jejich
části	část	k1gFnPc1	část
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
přepravovány	přepravovat	k5eAaImNgInP	přepravovat
Belugou	Beluga	k1gFnSc7	Beluga
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
části	část	k1gFnPc1	část
A380	A380	k1gFnSc2	A380
jsou	být	k5eAaImIp3nP	být
dopravovány	dopravován	k2eAgFnPc1d1	dopravována
lodí	loď	k1gFnSc7	loď
do	do	k7c2	do
Bordeaux	Bordeaux	k1gNnSc2	Bordeaux
a	a	k8xC	a
poté	poté	k6eAd1	poté
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
Toulouse	Toulouse	k1gInSc2	Toulouse
po	po	k7c6	po
speciálně	speciálně	k6eAd1	speciálně
rozšířené	rozšířený	k2eAgFnSc6d1	rozšířená
silnici	silnice	k1gFnSc6	silnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Airbus	airbus	k1gInSc4	airbus
nejen	nejen	k6eAd1	nejen
důležitým	důležitý	k2eAgInSc7d1	důležitý
trhem	trh	k1gInSc7	trh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
sídlem	sídlo	k1gNnSc7	sídlo
mnoha	mnoho	k4c2	mnoho
jeho	jeho	k3xOp3gMnPc2	jeho
dodavatelů	dodavatel	k1gMnPc2	dodavatel
<g/>
,	,	kIx,	,
ve	v	k7c4	v
zdejší	zdejší	k2eAgNnSc4d1	zdejší
Mobile	mobile	k1gNnSc4	mobile
je	být	k5eAaImIp3nS	být
výrobní	výrobní	k2eAgFnSc1d1	výrobní
linka	linka	k1gFnSc1	linka
<g/>
.	.	kIx.	.
</s>
<s>
Zákazníci	zákazník	k1gMnPc1	zákazník
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
odebrali	odebrat	k5eAaPmAgMnP	odebrat
2	[number]	k4	2
000	[number]	k4	000
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
5	[number]	k4	5
300	[number]	k4	300
letadel	letadlo	k1gNnPc2	letadlo
prodaných	prodaný	k2eAgInPc2d1	prodaný
Airbusem	airbus	k1gInSc7	airbus
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
všech	všecek	k3xTgFnPc2	všecek
výrobních	výrobní	k2eAgFnPc2d1	výrobní
řad	řada	k1gFnPc2	řada
od	od	k7c2	od
107	[number]	k4	107
<g/>
místného	místný	k2eAgInSc2d1	místný
A318	A318	k1gMnSc7	A318
až	až	k9	až
po	po	k7c4	po
555	[number]	k4	555
<g/>
místný	místný	k2eAgInSc4d1	místný
A	a	k8xC	a
<g/>
380	[number]	k4	380
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Američtí	americký	k2eAgMnPc1d1	americký
dodavatelé	dodavatel	k1gMnPc1	dodavatel
díky	díky	k7c3	díky
zakázkám	zakázka	k1gFnPc3	zakázka
Airbusu	airbus	k1gInSc2	airbus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
zaměstnávaly	zaměstnávat	k5eAaImAgFnP	zaměstnávat
asi	asi	k9	asi
120	[number]	k4	120
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
obchod	obchod	k1gInSc1	obchod
měl	mít	k5eAaImAgInS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
5,5	[number]	k4	5,5
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
A380	A380	k1gFnSc1	A380
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
51	[number]	k4	51
<g/>
%	%	kIx~	%
americký	americký	k2eAgInSc1d1	americký
podle	podle	k7c2	podle
podílu	podíl	k1gInSc2	podíl
hodnoty	hodnota	k1gFnSc2	hodnota
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
má	mít	k5eAaImIp3nS	mít
Airbus	airbus	k1gInSc1	airbus
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2017	[number]	k4	2017
přibližně	přibližně	k6eAd1	přibližně
150	[number]	k4	150
dodavatelů	dodavatel	k1gMnPc2	dodavatel
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
komponenty	komponenta	k1gFnPc1	komponenta
tvoří	tvořit	k5eAaImIp3nP	tvořit
součást	součást	k1gFnSc4	součást
většiny	většina	k1gFnSc2	většina
jeho	jeho	k3xOp3gNnPc2	jeho
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
partnery	partner	k1gMnPc7	partner
Airbusu	airbus	k1gInSc2	airbus
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jsou	být	k5eAaImIp3nP	být
plzeňská	plzeňská	k1gFnSc1	plzeňská
firma	firma	k1gFnSc1	firma
Zodiac-Driessen	Zodiac-Driessen	k1gInSc1	Zodiac-Driessen
Aerospace	Aerospace	k1gFnSc1	Aerospace
CZ	CZ	kA	CZ
vyrábějící	vyrábějící	k2eAgInPc4d1	vyrábějící
kuchyňské	kuchyňský	k2eAgInPc4d1	kuchyňský
moduly	modul	k1gInPc4	modul
do	do	k7c2	do
letadel	letadlo	k1gNnPc2	letadlo
a	a	k8xC	a
odpočinkové	odpočinkový	k2eAgFnPc4d1	odpočinková
místnosti	místnost	k1gFnPc4	místnost
pro	pro	k7c4	pro
posádky	posádka	k1gFnPc4	posádka
<g/>
,	,	kIx,	,
Latecoere	Latecoer	k1gInSc5	Latecoer
CZ	CZ	kA	CZ
produkující	produkující	k2eAgFnPc4d1	produkující
dveře	dveře	k1gFnPc4	dveře
<g/>
,	,	kIx,	,
brněnský	brněnský	k2eAgInSc4d1	brněnský
Frentech	Frent	k1gInPc6	Frent
Aerospace	Aerospace	k1gFnPc1	Aerospace
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
mikroelektroniku	mikroelektronika	k1gFnSc4	mikroelektronika
a	a	k8xC	a
Aero	aero	k1gNnSc4	aero
Vodochody	Vodochod	k1gInPc1	Vodochod
s	s	k7c7	s
kompozitovými	kompozitový	k2eAgInPc7d1	kompozitový
díly	díl	k1gInPc7	díl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
podle	podle	k7c2	podle
země	zem	k1gFnSc2	zem
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Údaje	údaj	k1gInSc2	údaj
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
podle	podle	k7c2	podle
poboček	pobočka	k1gFnPc2	pobočka
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Údaje	údaj	k1gInPc4	údaj
z	z	k7c2	z
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1	[number]	k4	1
V	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
jsou	být	k5eAaImIp3nP	být
přesné	přesný	k2eAgInPc1d1	přesný
názvy	název	k1gInPc1	název
míst	místo	k1gNnPc2	místo
poboček	pobočka	k1gFnPc2	pobočka
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dodávky	dodávka	k1gFnPc1	dodávka
letadel	letadlo	k1gNnPc2	letadlo
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
objednaných	objednaný	k2eAgNnPc2d1	objednané
a	a	k8xC	a
dodaných	dodaný	k2eAgNnPc2d1	dodané
letadel	letadlo	k1gNnPc2	letadlo
Airbus	airbus	k1gInSc1	airbus
A380	A380	k1gFnPc2	A380
</s>
</p>
<p>
<s>
Airbus	airbus	k1gInSc1	airbus
Military	Militara	k1gFnSc2	Militara
</s>
</p>
<p>
<s>
Boeing	boeing	k1gInSc4	boeing
</s>
</p>
<p>
<s>
Lockheed	Lockheed	k1gInSc1	Lockheed
Corporation	Corporation	k1gInSc1	Corporation
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Airbus	airbus	k1gInSc1	airbus
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Airbus	airbus	k1gInSc4	airbus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Airbus	airbus	k1gInSc1	airbus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
