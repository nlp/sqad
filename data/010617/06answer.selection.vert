<s>
Společnost	společnost	k1gFnSc1	společnost
Airbus	airbus	k1gInSc1	airbus
S.	S.	kA	S.
<g/>
A.S.	A.S.	k1gMnPc2	A.S.
<g/>
,	,	kIx,	,
známější	známý	k2eAgFnSc1d2	známější
jen	jen	k9	jen
jako	jako	k8xS	jako
Airbus	airbus	k1gInSc4	airbus
<g/>
,	,	kIx,	,
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Toulouse	Toulouse	k1gInSc6	Toulouse
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
největších	veliký	k2eAgMnPc2d3	veliký
světových	světový	k2eAgMnPc2d1	světový
výrobců	výrobce	k1gMnPc2	výrobce
civilních	civilní	k2eAgNnPc2d1	civilní
dopravních	dopravní	k2eAgNnPc2d1	dopravní
letadel	letadlo	k1gNnPc2	letadlo
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Boeing	boeing	k1gInSc4	boeing
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
