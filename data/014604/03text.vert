<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Salvátora	Salvátor	k1gMnSc2
(	(	kIx(
<g/>
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
nad	nad	k7c7
Metují	Metuje	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Salvátora	Salvátor	k1gMnSc2
Místo	místo	k7c2
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Obec	obec	k1gFnSc1
</s>
<s>
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
nad	nad	k7c7
Metují	Metuje	k1gFnSc7
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
20	#num#	k4
<g/>
′	′	k?
<g/>
44,08	44,08	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
9	#num#	k4
<g/>
′	′	k?
<g/>
0,36	0,36	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Církev	církev	k1gFnSc1
</s>
<s>
římskokatolická	římskokatolický	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
česká	český	k2eAgFnSc1d1
Diecéze	diecéze	k1gFnSc1
</s>
<s>
královéhradecká	královéhradecký	k2eAgFnSc1d1
Vikariát	vikariát	k1gInSc1
</s>
<s>
náchodský	náchodský	k2eAgInSc1d1
Farnost	farnost	k1gFnSc4
</s>
<s>
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
nad	nad	k7c7
Metují	Metuje	k1gFnSc7
Zánik	zánik	k1gInSc1
</s>
<s>
1877	#num#	k4
Architektonický	architektonický	k2eAgInSc1d1
popis	popis	k1gInSc1
Stavební	stavební	k2eAgInSc1d1
sloh	sloh	k1gInSc4
</s>
<s>
pozdní	pozdní	k2eAgFnSc1d1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1729	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Salvátora	Salvátor	k1gMnSc2
byl	být	k5eAaImAgInS
římskokatolický	římskokatolický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
na	na	k7c6
Plácku	plácek	k1gInSc6
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
náměstí	náměstí	k1gNnSc1
Republiky	republika	k1gFnSc2
<g/>
)	)	kIx)
v	v	k7c6
Novém	nový	k2eAgNnSc6d1
Městě	město	k1gNnSc6
nad	nad	k7c7
Metují	Metuje	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
podoba	podoba	k1gFnSc1
je	být	k5eAaImIp3nS
zachycena	zachytit	k5eAaPmNgFnS
na	na	k7c6
starých	starý	k2eAgFnPc6d1
grafikách	grafika	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
kostela	kostel	k1gInSc2
</s>
<s>
Kostel	kostel	k1gInSc1
byl	být	k5eAaImAgInS
založen	založen	k2eAgInSc1d1
roku	rok	k1gInSc2
1729	#num#	k4
Jakubem	Jakub	k1gMnSc7
Arnoštem	Arnošt	k1gMnSc7
z	z	k7c2
Leslie	Leslie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Měl	mít	k5eAaImAgMnS
křížovou	křížový	k2eAgFnSc4d1
loď	loď	k1gFnSc4
a	a	k8xC
věž	věž	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Zánik	zánik	k1gInSc1
kostela	kostel	k1gInSc2
</s>
<s>
Zanikl	zaniknout	k5eAaPmAgInS
za	za	k7c2
josefínských	josefínský	k2eAgFnPc2d1
reforem	reforma	k1gFnPc2
roku	rok	k1gInSc2
1787	#num#	k4
<g/>
,	,	kIx,
budova	budova	k1gFnSc1
dále	daleko	k6eAd2
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
mandl	mandl	k1gInSc4
a	a	k8xC
sklad	sklad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
byla	být	k5eAaImAgFnS
zlikvidována	zlikvidovat	k5eAaPmNgFnS
obcí	obec	k1gFnSc7
roku	rok	k1gInSc2
1877	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Socha	socha	k1gFnSc1
sv.	sv.	kA
Jakuba	Jakub	k1gMnSc2
<g/>
,	,	kIx,
původně	původně	k6eAd1
umístěná	umístěný	k2eAgNnPc1d1
v	v	k7c6
kostele	kostel	k1gInSc6
sv.	sv.	kA
Salvátora	Salvátor	k1gMnSc2
v	v	k7c6
nice	nika	k1gFnSc6
nad	nad	k7c7
portálem	portál	k1gInSc7
směrem	směr	k1gInSc7
ke	k	k7c3
Krajské	krajský	k2eAgFnSc3d1
bráně	brána	k1gFnSc3
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1922	#num#	k4
umístěna	umístit	k5eAaPmNgFnS
ve	v	k7c6
výklenku	výklenek	k1gInSc6
kostelní	kostelní	k2eAgFnPc4d1
zdi	zeď	k1gFnPc4
kostela	kostel	k1gInSc2
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
vlevo	vlevo	k6eAd1
před	před	k7c7
vchodem	vchod	k1gInSc7
a	a	k8xC
mobiliář	mobiliář	k1gInSc1
byl	být	k5eAaImAgInS
rozebrán	rozebrat	k5eAaPmNgInS
ve	v	k7c4
prospěch	prospěch	k1gInSc4
jiných	jiný	k2eAgInPc2d1
kostelů	kostel	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
(	(	kIx(
<g/>
zvony	zvon	k1gInPc1
byly	být	k5eAaImAgInP
prodány	prodat	k5eAaPmNgInP
do	do	k7c2
Zdobnice	Zdobnice	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
varhany	varhany	k1gFnPc4
přemístěny	přemístěn	k2eAgFnPc4d1
do	do	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Ducha	duch	k1gMnSc2
v	v	k7c6
nedalekém	daleký	k2eNgInSc6d1
Krčíně	Krčín	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Socha	socha	k1gFnSc1
sv.	sv.	kA
Jakuba	Jakub	k1gMnSc2
</s>
<s>
Socha	socha	k1gFnSc1
sv.	sv.	kA
Jakuba	Jakub	k1gMnSc2
před	před	k7c7
kostelem	kostel	k1gInSc7
Nejsv	Nejsva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trojice	trojice	k1gFnSc1
</s>
<s>
Pomník	pomník	k1gInSc1
obětem	oběť	k1gFnPc3
světových	světový	k2eAgFnPc2d1
válek	válka	k1gFnPc2
-	-	kIx~
místo	místo	k1gNnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
stával	stávat	k5eAaImAgInS
kostel	kostel	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
NÝVLT	Nývlt	k1gMnSc1
<g/>
,	,	kIx,
K.	K.	kA
V.	V.	kA
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Salvátora	Salvátor	k1gMnSc2
-	-	kIx~
Farnost	farnost	k1gFnSc1
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
nad	nad	k7c7
Metují	Metuje	k1gFnSc7
<g/>
.	.	kIx.
www.farnostnm.cz	www.farnostnm.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KLOS	KLOS	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paměti	paměť	k1gFnSc2
města	město	k1gNnSc2
a	a	k8xC
zámku	zámek	k1gInSc2
Nového	Nového	k2eAgNnSc2d1
Města	město	k1gNnSc2
nad	nad	k7c7
Metují	Metuje	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInSc4d1
Město	město	k1gNnSc1
nad	nad	k7c7
Metují	Metuje	k1gFnSc7
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1922	#num#	k4
<g/>
.	.	kIx.
135	#num#	k4
s.	s.	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
</s>
