<s>
Hřib	hřib	k1gInSc1
olivovožlutý	olivovožlutý	k2eAgInSc1d1
</s>
<s>
Hřib	hřib	k1gInSc1
olivovožlutý	olivovožlutý	k2eAgInSc1d1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc2
Říše	říš	k1gFnSc2
</s>
<s>
houby	houba	k1gFnPc1
(	(	kIx(
<g/>
Fungi	Fungi	k1gNnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
houby	houby	k6eAd1
stopkovýtrusné	stopkovýtrusný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Basidiomycota	Basidiomycota	k1gFnSc1
<g/>
)	)	kIx)
Pododdělení	pododdělení	k1gNnSc1
</s>
<s>
Agaricomycotina	Agaricomycotina	k1gFnSc1
Třída	třída	k1gFnSc1
</s>
<s>
Agaricomycetes	Agaricomycetes	k1gInSc1
Řád	řád	k1gInSc1
</s>
<s>
hřibotvaré	hřibotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Boletales	Boletales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
hřibovité	hřibovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Boletaceae	Boletacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Boletus	Boletus	k1gInSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Boletus	Boletus	k1gMnSc1
pachypusFr	pachypusFr	k1gMnSc1
<g/>
.	.	kIx.
sensu	sens	k1gInSc2
Quél	Quél	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hřib	hřib	k1gInSc1
olivovožlutý	olivovožlutý	k2eAgInSc1d1
(	(	kIx(
<g/>
Boletus	Boletus	k1gInSc1
pachypus	pachypus	k1gMnSc1
Fr.	Fr.	k1gMnSc1
sensu	sensa	k1gFnSc4
Quél	Quéla	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejedlá	jedlý	k2eNgFnSc1d1
houba	houba	k1gFnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
hřibovitých	hřibovitý	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řadí	řadit	k5eAaImIp3nS
se	se	k3xPyFc4
do	do	k7c2
sekce	sekce	k1gFnSc2
Calopodes	Calopodesa	k1gFnPc2
a	a	k8xC
je	být	k5eAaImIp3nS
blízce	blízce	k6eAd1
příbuzný	příbuzný	k1gMnSc1
hřibu	hřib	k1gInSc2
kříšti	kříšť	k1gInSc3
(	(	kIx(
<g/>
Boletus	Boletus	k1gMnSc1
calopus	calopus	k1gMnSc1
Pers.	Pers.	k1gMnSc1
<g/>
:	:	kIx,
Fr.	Fr.	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klasifikace	klasifikace	k1gFnSc1
tohoto	tento	k3xDgInSc2
druhu	druh	k1gInSc2
není	být	k5eNaImIp3nS
jednotná	jednotný	k2eAgFnSc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
není	být	k5eNaImIp3nS
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
v	v	k7c6
rámci	rámec	k1gInSc6
tohoto	tento	k3xDgInSc2
článku	článek	k1gInSc2
prezentována	prezentovat	k5eAaBmNgFnS
podle	podle	k7c2
pojetí	pojetí	k1gNnSc2
mykologa	mykolog	k1gMnSc2
Aurela	Aurel	k1gMnSc2
Dermeka	Dermeek	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Synonyma	synonymum	k1gNnPc1
</s>
<s>
Boletus	Boletus	k1gMnSc1
olivaceo-flavus	olivaceo-flavus	k1gMnSc1
Dermek	Dermek	k1gMnSc1
(	(	kIx(
<g/>
nom.	nom.	k?
nud	nuda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Boletus	Boletus	k1gMnSc1
olivaceoflavus	olivaceoflavus	k1gMnSc1
Dermek	Dermek	k1gMnSc1
1973	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Boletus	Boletus	k1gMnSc1
olivaceus	olivaceus	k1gMnSc1
Sch	Sch	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
1774	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Boletus	Boletus	k1gMnSc1
pachypus	pachypus	k1gMnSc1
Fr.	Fr.	k1gMnSc1
sensu	sensa	k1gFnSc4
Quél	Quéla	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Boletus	Boletus	k1gMnSc1
pachypus	pachypus	k1gMnSc1
Fr.	Fr.	k1gMnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
sensu	sens	k1gInSc2
Smotl	Smotl	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Boletus	Boletus	k1gMnSc1
pachypus	pachypus	k1gMnSc1
var.	var.	k?
olivaceus	olivaceus	k1gMnSc1
(	(	kIx(
<g/>
Schaeff	Schaeff	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Pers.	Pers.	k1gFnSc1
1825	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Boletus	Boletus	k1gMnSc1
tesselatus	tesselatus	k1gMnSc1
Rostk	Rostk	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
hřib	hřib	k1gInSc1
hořký	hořký	k2eAgInSc1d1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
hřib	hřib	k1gInSc4
olivový	olivový	k2eAgInSc4d1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
hřib	hřib	k1gInSc1
olivovožlutý	olivovožlutý	k2eAgInSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
modrák	modrák	k1gMnSc1
hořký	hořký	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
modrák	modrák	k1gMnSc1
olivový	olivový	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Taxonomie	taxonomie	k1gFnSc1
</s>
<s>
Různí	různý	k2eAgMnPc1d1
mykologové	mykolog	k1gMnPc1
klasifikují	klasifikovat	k5eAaImIp3nP
druh	druh	k1gInSc4
Boletus	Boletus	k1gMnSc1
pachypus	pachypus	k1gMnSc1
odlišně	odlišně	k6eAd1
<g/>
,	,	kIx,
mnozí	mnohý	k2eAgMnPc1d1
jej	on	k3xPp3gMnSc4
považují	považovat	k5eAaImIp3nP
za	za	k7c4
totožný	totožný	k2eAgInSc4d1
s	s	k7c7
hřibem	hřib	k1gInSc7
kříštěm	kříšť	k1gInSc7
(	(	kIx(
<g/>
Boletus	Boletus	k1gMnSc1
calopus	calopus	k1gMnSc1
<g/>
)	)	kIx)
v	v	k7c6
aktuálně	aktuálně	k6eAd1
rozšířeném	rozšířený	k2eAgNnSc6d1
pojetí	pojetí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojetí	pojetí	k1gNnPc4
autorů	autor	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
jej	on	k3xPp3gMnSc4
považují	považovat	k5eAaImIp3nP
za	za	k7c4
samostatný	samostatný	k2eAgInSc4d1
druh	druh	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
často	často	k6eAd1
liší	lišit	k5eAaImIp3nP
a	a	k8xC
popisy	popis	k1gInPc1
se	se	k3xPyFc4
do	do	k7c2
různé	různý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
překrývají	překrývat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Velenovský	Velenovský	k2eAgInSc1d1
</s>
<s>
Josef	Josef	k1gMnSc1
Velenovský	Velenovský	k2eAgMnSc1d1
rozlišoval	rozlišovat	k5eAaImAgMnS
tři	tři	k4xCgInPc4
blízce	blízce	k6eAd1
příbuzné	příbuzný	k2eAgInPc4d1
druhy	druh	k1gInPc4
<g/>
:	:	kIx,
hřib	hřib	k1gInSc1
kříšť	kříšť	k1gInSc1
(	(	kIx(
<g/>
Boletus	Boletus	k1gMnSc1
pachypus	pachypus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hřib	hřib	k1gInSc1
červenka	červenka	k1gFnSc1
(	(	kIx(
<g/>
Boletus	Boletus	k1gMnSc1
calopus	calopus	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
hřib	hřib	k1gInSc1
olivový	olivový	k2eAgInSc1d1
(	(	kIx(
<g/>
Boletus	Boletus	k1gInSc1
olivaceus	olivaceus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Jejich	jejich	k3xOp3gInSc1
popis	popis	k1gInSc1
se	se	k3xPyFc4
odlišuje	odlišovat	k5eAaImIp3nS
především	především	k9
vybarvením	vybarvení	k1gNnSc7
třeně	třeň	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
chutí	chuť	k1gFnSc7
dužniny	dužnina	k1gFnSc2
<g/>
:	:	kIx,
kříšť	kříšť	k1gInSc1
(	(	kIx(
<g/>
Boletus	Boletus	k1gMnSc1
pachypus	pachypus	k1gMnSc1
<g/>
)	)	kIx)
podle	podle	k7c2
Velenovského	Velenovský	k2eAgNnSc2d1
má	mít	k5eAaImIp3nS
třeň	třeň	k1gInSc1
červený	červený	k2eAgInSc1d1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
nahoře	nahoře	k6eAd1
žlutý	žlutý	k2eAgInSc1d1
a	a	k8xC
velmi	velmi	k6eAd1
hořkou	hořký	k2eAgFnSc4d1
chuť	chuť	k1gFnSc4
<g/>
,	,	kIx,
červenka	červenka	k1gFnSc1
(	(	kIx(
<g/>
Boletus	Boletus	k1gMnSc1
calopus	calopus	k1gMnSc1
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
třeň	třeň	k1gInSc1
celý	celý	k2eAgInSc1d1
nachově	nachově	k6eAd1
červený	červený	k2eAgInSc1d1
<g/>
,	,	kIx,
chuť	chuť	k1gFnSc4
sladkou	sladký	k2eAgFnSc4d1
a	a	k8xC
hřib	hřib	k1gInSc4
olivový	olivový	k2eAgInSc4d1
<g/>
,	,	kIx,
neboli	neboli	k8xC
hořký	hořký	k2eAgMnSc1d1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Boletus	Boletus	k1gMnSc1
olivaceus	olivaceus	k1gMnSc1
syn	syn	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boletus	Boletus	k1gMnSc1
tesselatus	tesselatus	k1gMnSc1
Rostk	Rostk	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
třeň	třeň	k1gInSc4
nahoře	nahoře	k6eAd1
žlutý	žlutý	k2eAgInSc4d1
<g/>
,	,	kIx,
dole	dole	k6eAd1
(	(	kIx(
<g/>
resp.	resp.	kA
uprostřed	uprostřed	k6eAd1
<g/>
)	)	kIx)
červený	červený	k2eAgInSc1d1
<g/>
,	,	kIx,
chuť	chuť	k1gFnSc4
hořkou	hořký	k2eAgFnSc4d1
a	a	k8xC
je	být	k5eAaImIp3nS
drobnější	drobný	k2eAgInSc1d2
(	(	kIx(
<g/>
klobouk	klobouk	k1gInSc1
do	do	k7c2
60	#num#	k4
milimetrů	milimetr	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Macků	Macků	k1gFnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Macků	Macků	k1gMnSc1
rozlišoval	rozlišovat	k5eAaImAgMnS
dva	dva	k4xCgInPc4
druhy	druh	k1gInPc4
<g/>
:	:	kIx,
hřib	hřib	k1gInSc1
kříšť	kříšť	k1gInSc1
(	(	kIx(
<g/>
Boletus	Boletus	k1gMnSc1
pachypus	pachypus	k1gMnSc1
Fr.	Fr.	k1gMnSc1
<g/>
)	)	kIx)
se	s	k7c7
třeněm	třeň	k1gInSc7
nahoře	nahoře	k6eAd1
žlutým	žlutý	k2eAgInSc7d1
<g/>
,	,	kIx,
dole	dole	k6eAd1
červeným	červené	k1gNnSc7
a	a	k8xC
hořkou	hořký	k2eAgFnSc7d1
chutí	chuť	k1gFnSc7
a	a	k8xC
hřib	hřib	k1gInSc1
červenka	červenka	k1gFnSc1
(	(	kIx(
<g/>
Boletus	Boletus	k1gMnSc1
calopus	calopus	k1gMnSc1
Pers.	Pers.	k1gMnSc1
<g/>
)	)	kIx)
s	s	k7c7
červeným	červený	k2eAgInSc7d1
třeněm	třeň	k1gInSc7
(	(	kIx(
<g/>
chuť	chuť	k1gFnSc4
neuváděl	uvádět	k5eNaImAgMnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Smotlacha	smotlacha	k1gFnSc1
</s>
<s>
František	František	k1gMnSc1
Smotlacha	smotlacha	k1gMnSc1
rozlišoval	rozlišovat	k5eAaImAgMnS
červenku	červenka	k1gFnSc4
(	(	kIx(
<g/>
Boletus	Boletus	k1gMnSc1
calopus	calopus	k1gMnSc1
Sm	Sm	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
formu	forma	k1gFnSc4
hřibu	hřib	k1gInSc2
satanovitého	satanovitý	k2eAgInSc2d1
se	s	k7c7
sytě	sytě	k6eAd1
červeným	červený	k2eAgInSc7d1
třeněm	třeň	k1gInSc7
a	a	k8xC
chutnou	chutný	k2eAgFnSc7d1
dužninou	dužnina	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k8xS,k8xC
podobný	podobný	k2eAgInSc1d1
(	(	kIx(
<g/>
ale	ale	k8xC
samostatný	samostatný	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
)	)	kIx)
uváděl	uvádět	k5eAaImAgInS
kříšť	kříšť	k1gInSc1
(	(	kIx(
<g/>
Boletus	Boletus	k1gMnSc1
pachypus	pachypus	k1gMnSc1
Fr.	Fr.	k1gMnSc1
<g/>
)	)	kIx)
s	s	k7c7
třeněm	třeň	k1gInSc7
červeným	červený	k2eAgInSc7d1
jen	jen	k6eAd1
uprostřed	uprostřed	k6eAd1
<g/>
,	,	kIx,
nahoře	nahoře	k6eAd1
žlutým	žlutý	k2eAgInSc7d1
<g/>
,	,	kIx,
dole	dole	k6eAd1
hnědým	hnědý	k2eAgMnSc7d1
-	-	kIx~
a	a	k8xC
bronzovou	bronzový	k2eAgFnSc4d1
formu	forma	k1gFnSc4
kříště	kříšť	k1gInSc2
-	-	kIx~
hřib	hřib	k1gInSc1
olivový	olivový	k2eAgInSc1d1
(	(	kIx(
<g/>
Boletus	Boletus	k1gInSc1
olivaceus	olivaceus	k1gMnSc1
Sch	Sch	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dermek	Dermek	k1gMnSc1
</s>
<s>
Aurel	Aurel	k1gMnSc1
Dermek	Dermek	k1gMnSc1
uváděl	uvádět	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
mnozí	mnohý	k2eAgMnPc1d1
současníci	současník	k1gMnPc1
považují	považovat	k5eAaImIp3nP
hřib	hřib	k1gInSc4
olivovožlutý	olivovožlutý	k2eAgInSc4d1
(	(	kIx(
<g/>
Boletus	Boletus	k1gInSc1
pachypus	pachypus	k1gMnSc1
Fr.	Fr.	k1gMnSc1
<g/>
)	)	kIx)
za	za	k7c4
barevnou	barevný	k2eAgFnSc4d1
odchylku	odchylka	k1gFnSc4
hřibu	hřib	k1gInSc2
kříště	kříšť	k1gInSc2
(	(	kIx(
<g/>
Boletus	Boletus	k1gMnSc1
calopus	calopus	k1gMnSc1
Fr.	Fr.	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
odlišných	odlišný	k2eAgInPc2d1
znaků	znak	k1gInPc2
ho	on	k3xPp3gMnSc4
ale	ale	k8xC
klasifikoval	klasifikovat	k5eAaImAgInS
jako	jako	k9
samostatný	samostatný	k2eAgInSc1d1
druh	druh	k1gInSc1
(	(	kIx(
<g/>
popis	popis	k1gInSc1
níže	níže	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
Makroskopický	makroskopický	k2eAgInSc1d1
</s>
<s>
Klobouk	klobouk	k1gInSc1
dosahuje	dosahovat	k5eAaImIp3nS
60	#num#	k4
-	-	kIx~
100	#num#	k4
milimetrů	milimetr	k1gInPc2
<g/>
,	,	kIx,
nejdříve	dříve	k6eAd3
polokulovitý	polokulovitý	k2eAgMnSc1d1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
klenutý	klenutý	k2eAgInSc1d1
<g/>
,	,	kIx,
v	v	k7c6
mládí	mládí	k1gNnSc6
je	být	k5eAaImIp3nS
okraj	okraj	k1gInSc1
podvinutý	podvinutý	k2eAgInSc1d1
<g/>
,	,	kIx,
v	v	k7c6
dospělosti	dospělost	k1gFnSc6
zvlněný	zvlněný	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Povrch	povrch	k1gInSc1
je	být	k5eAaImIp3nS
suchý	suchý	k2eAgInSc1d1
<g/>
,	,	kIx,
jemně	jemně	k6eAd1
plstnatý	plstnatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
olivově	olivově	k6eAd1
plavý	plavý	k2eAgInSc1d1
až	až	k9
olivově	olivově	k6eAd1
hnědý	hnědý	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rourky	rourka	k1gFnPc1
jsou	být	k5eAaImIp3nP
5	#num#	k4
-	-	kIx~
8	#num#	k4
milimetrů	milimetr	k1gInPc2
dlouhé	dlouhý	k2eAgNnSc1d1
<g/>
,	,	kIx,
nejprve	nejprve	k6eAd1
bledě	bledě	k6eAd1
žluté	žlutý	k2eAgFnPc1d1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
zelenožluté	zelenožlutý	k2eAgFnPc1d1
<g/>
,	,	kIx,
na	na	k7c6
řezu	řez	k1gInSc6
modrající	modrající	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Póry	pór	k1gInPc1
mají	mít	k5eAaImIp3nP
okrouhlý	okrouhlý	k2eAgInSc4d1
tvar	tvar	k1gInSc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
drobné	drobný	k2eAgFnPc1d1
<g/>
,	,	kIx,
v	v	k7c6
mládí	mládí	k1gNnSc6
bleděžluté	bleděžlutý	k2eAgInPc1d1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
sytě	sytě	k6eAd1
žluté	žlutý	k2eAgFnPc4d1
až	až	k8xS
chromové	chromový	k2eAgFnPc4d1
<g/>
,	,	kIx,
vždy	vždy	k6eAd1
bez	bez	k7c2
zeleného	zelený	k2eAgInSc2d1
odstínu	odstín	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
otlacích	otlak	k1gInPc6
modrozelenají	modrozelenat	k5eAaImIp3nP,k5eAaPmIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Třeň	třeň	k1gInSc1
dosahuje	dosahovat	k5eAaImIp3nS
60	#num#	k4
-	-	kIx~
120	#num#	k4
×	×	k?
20	#num#	k4
-	-	kIx~
35	#num#	k4
milimetrů	milimetr	k1gInPc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
válcovitý	válcovitý	k2eAgInSc4d1
až	až	k8xS
kyjovitý	kyjovitý	k2eAgInSc4d1
tvar	tvar	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c4
bázi	báze	k1gFnSc4
zaoblený	zaoblený	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zbarvení	zbarvení	k1gNnSc4
pod	pod	k7c7
kloboukem	klobouk	k1gInSc7
je	být	k5eAaImIp3nS
sytě	sytě	k6eAd1
žluté	žlutý	k2eAgFnPc4d1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
bledě	bledě	k6eAd1
žlutý	žlutý	k2eAgInSc1d1
a	a	k8xC
přechází	přecházet	k5eAaImIp3nS
do	do	k7c2
špinavě	špinavě	k6eAd1
hnědé	hnědý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žlutou	žlutý	k2eAgFnSc4d1
zónu	zóna	k1gFnSc4
od	od	k7c2
bledě	bledě	k6eAd1
žluté	žlutý	k2eAgFnSc6d1
odděluje	oddělovat	k5eAaImIp3nS
červený	červený	k2eAgInSc1d1
pásek	pásek	k1gInSc1
o	o	k7c6
šířce	šířka	k1gFnSc6
3	#num#	k4
-	-	kIx~
10	#num#	k4
milimetrů	milimetr	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Povrch	povrch	k1gInSc1
třeně	třeň	k1gInSc2
kryje	krýt	k5eAaImIp3nS
síťka	síťka	k1gFnSc1
ohrověhnědé	ohrověhnědý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dužnina	dužnina	k1gFnSc1
má	mít	k5eAaImIp3nS
bělavé	bělavý	k2eAgNnSc4d1
zbarvení	zbarvení	k1gNnSc4
<g/>
,	,	kIx,
na	na	k7c6
řezu	řez	k1gInSc6
lehce	lehko	k6eAd1
modrá	modrý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Konzistence	konzistence	k1gFnSc1
je	být	k5eAaImIp3nS
tvrdá	tvrdý	k2eAgFnSc1d1
<g/>
,	,	kIx,
vůně	vůně	k1gFnSc1
nenápadná	nápadný	k2eNgFnSc1d1
<g/>
,	,	kIx,
chuť	chuť	k1gFnSc1
odporně	odporně	k6eAd1
hořká	hořký	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mikroskopický	mikroskopický	k2eAgInSc1d1
</s>
<s>
Výtrusy	výtrus	k1gInPc1
dosahují	dosahovat	k5eAaImIp3nP
11	#num#	k4
-	-	kIx~
15	#num#	k4
×	×	k?
4,5	4,5	k4
-	-	kIx~
5,5	5,5	k4
μ	μ	k1gNnPc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
hladké	hladký	k2eAgFnPc1d1
<g/>
,	,	kIx,
elipsoidně	elipsoidně	k6eAd1
vřetenovité	vřetenovitý	k2eAgInPc1d1
<g/>
,	,	kIx,
okrově	okrově	k6eAd1
olivové	olivový	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Výtrusný	výtrusný	k2eAgInSc1d1
prach	prach	k1gInSc1
je	být	k5eAaImIp3nS
hnědavě	hnědavě	k6eAd1
olivový	olivový	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Roste	růst	k5eAaImIp3nS
v	v	k7c6
jehličnatých	jehličnatý	k2eAgInPc6d1
i	i	k8xC
listnatých	listnatý	k2eAgInPc6d1
lesích	les	k1gInPc6
<g/>
,	,	kIx,
ale	ale	k8xC
výrazně	výrazně	k6eAd1
vzácněji	vzácně	k6eAd2
než	než	k8xS
hřib	hřib	k1gInSc1
kříšť	kříšť	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fruktifikuje	fruktifikovat	k5eAaBmIp3nS
od	od	k7c2
července	červenec	k1gInSc2
do	do	k7c2
září	září	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozšíření	rozšíření	k1gNnSc1
není	být	k5eNaImIp3nS
zatím	zatím	k6eAd1
blíže	blízce	k6eAd2
prozkoumané	prozkoumaný	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Velenovský	Velenovský	k2eAgMnSc1d1
uváděl	uvádět	k5eAaImAgMnS
výskyt	výskyt	k1gInSc4
hřibu	hřib	k1gInSc2
olivového	olivový	k2eAgInSc2d1
v	v	k7c6
jedlinách	jedlina	k1gFnPc6
<g/>
,	,	kIx,
smíšených	smíšený	k2eAgInPc6d1
lesích	les	k1gInPc6
a	a	k8xC
bučinách	bučina	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Boletus	Boletus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
15	#num#	k4
16	#num#	k4
17	#num#	k4
DERMEK	DERMEK	kA
<g/>
,	,	kIx,
Aurel	Aurel	k1gMnSc1
<g/>
;	;	kIx,
LIZOŇ	LIZOŇ	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malý	malý	k2eAgInSc1d1
atlas	atlas	k1gInSc1
húb	húb	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
spn	spn	k?
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
.	.	kIx.
548	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Hríb	Hríb	k1gMnSc1
olivovožltý	olivovožltý	k2eAgMnSc1d1
<g/>
,	,	kIx,
s.	s.	k?
392	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Index	index	k1gInSc1
Fungorum	Fungorum	k1gInSc4
<g/>
.	.	kIx.
databáze	databáze	k1gFnSc1
Index	index	k1gInSc1
Fungorum	Fungorum	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Index	index	k1gInSc1
Fungorum	Fungorum	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
SMOTLACHA	smotlacha	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlas	Atlas	k1gInSc1
hub	houba	k1gFnPc2
jedlých	jedlý	k2eAgInPc2d1
a	a	k8xC
nejedlých	jedlý	k2eNgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Melantrich	Melantrich	k1gMnSc1
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
.	.	kIx.
297	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Hřib	hřib	k1gInSc1
neboli	neboli	k8xC
modrák	modrák	k1gInSc1
satanovitý	satanovitý	k2eAgInSc1d1
<g/>
,	,	kIx,
s.	s.	k?
186	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
VELENOVSKÝ	VELENOVSKÝ	kA
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
houby	houba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
botanická	botanický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
1920	#num#	k4
<g/>
.	.	kIx.
950	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
709	#num#	k4
<g/>
-	-	kIx~
<g/>
710	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
MACKŮ	Macků	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgMnSc1d1
houbař	houbař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
R.	R.	kA
Promberger	Promberger	k1gMnSc1
<g/>
,	,	kIx,
1924	#num#	k4
<g/>
.	.	kIx.
257	#num#	k4
s.	s.	k?
S.	S.	kA
164	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Taxon	taxon	k1gInSc1
Boletus	Boletus	k1gInSc1
pachypus	pachypus	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Hřibovité	hřibovitý	k2eAgInPc4d1
Hřib	hřib	k1gInSc4
(	(	kIx(
<g/>
Aureoboletus	Aureoboletus	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc4
moravský	moravský	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
pružný	pružný	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
štíhlotřenný	štíhlotřenný	k2eAgInSc4d1
Hřib	hřib	k1gInSc4
(	(	kIx(
<g/>
Boletus	Boletus	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
borový	borový	k2eAgInSc1d1
(	(	kIx(
<g/>
hnědofialový	hnědofialový	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Hřib	hřib	k1gInSc4
bronzový	bronzový	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
dubový	dubový	k2eAgInSc4d1
(	(	kIx(
<g/>
habrový	habrový	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Hřib	hřib	k1gInSc4
Melzerův	Melzerův	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
nádherný	nádherný	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
smrkový	smrkový	k2eAgInSc4d1
(	(	kIx(
<g/>
březový	březový	k2eAgInSc4d1
<g/>
,	,	kIx,
citronový	citronový	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Hřib	hřib	k1gInSc1
šumavský	šumavský	k2eAgInSc1d1
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Buchwaldoboletus	Buchwaldoboletus	k1gInSc1
<g/>
)	)	kIx)
<g/>
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Chalciporus	Chalciporus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
dřevožijný	dřevožijný	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
sírovýHřib	sírovýHřib	k1gInSc1
maličký	maličký	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
malinový	malinový	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
peprný	peprný	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
rubínový	rubínový	k2eAgInSc1d1
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Butyriboletus	Butyriboletus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc4
Fechtnerův	Fechtnerův	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
hnědorůžový	hnědorůžový	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
horský	horský	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
královský	královský	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
přívěskatý	přívěskatý	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc1
růžovník	růžovník	k1gInSc1
•	•	k?
Hřib	hřib	k1gInSc4
šedorůžový	šedorůžový	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
Wichanského	Wichanský	k2eAgInSc2d1
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Caloboletus	Caloboletus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
Kluzákův	Kluzákův	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
kříšť	kříšť	k1gInSc1
•	•	k?
Hřib	hřib	k1gInSc4
medotrpký	medotrpký	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
olivovožlutý	olivovožlutý	k2eAgInSc4d1
Hřib	hřib	k1gInSc4
(	(	kIx(
<g/>
Cyanoboletus	Cyanoboletus	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
modračka	modračka	k1gFnSc1
•	•	k?
Hřib	hřib	k1gInSc4
žlutokrvavý	žlutokrvavý	k2eAgInSc4d1
Hřib	hřib	k1gInSc4
(	(	kIx(
<g/>
Hemileccinum	Hemileccinum	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
plavý	plavý	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
skvrnitý	skvrnitý	k2eAgInSc1d1
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Hortiboletus	Hortiboletus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc4
červený	červený	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
Engelův	Engelův	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
lindový	lindový	k2eAgInSc4d1
Hřib	hřib	k1gInSc4
(	(	kIx(
<g/>
Imleria	Imlerium	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
hnědý	hnědý	k2eAgInSc1d1
(	(	kIx(
<g/>
hnědočervený	hnědočervený	k2eAgInSc1d1
<g/>
,	,	kIx,
naduřelý	naduřelý	k2eAgInSc1d1
<g/>
)	)	kIx)
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Imperator	Imperator	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc4
měďový	měďový	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
rudonachový	rudonachový	k2eAgInSc4d1
(	(	kIx(
<g/>
žlutonachový	žlutonachový	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Hřib	hřib	k1gInSc1
zavalitý	zavalitý	k2eAgInSc1d1
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Neoboletus	Neoboletus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
kovář	kovář	k1gMnSc1
(	(	kIx(
<g/>
neměnný	neměnný	k2eAgInSc1d1,k2eNgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Hřib	hřib	k1gInSc1
žlutý	žlutý	k2eAgInSc1d1
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Pseudoboletus	Pseudoboletus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
hvězdákožijný	hvězdákožijný	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
příživný	příživný	k2eAgInSc1d1
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Rheubarbariboletus	Rheubarbariboletus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
broskvový	broskvový	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
meruňkový	meruňkový	k2eAgInSc1d1
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Rubroboletus	Rubroboletus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
Dupainův	Dupainův	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
Le	Le	k1gFnSc2
Galové	Galová	k1gFnSc2
(	(	kIx(
<g/>
Špinarův	Špinarův	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Hřib	hřib	k1gInSc4
Moserův	Moserův	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
nachový	nachový	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
narůžovělý	narůžovělý	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc1
satan	satan	k1gInSc1
(	(	kIx(
<g/>
hlohový	hlohový	k2eAgInSc1d1
<g/>
,	,	kIx,
satanovitý	satanovitý	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Hřib	hřib	k1gInSc4
středomořský	středomořský	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
vlčí	vlčí	k2eAgInSc4d1
Hřib	hřib	k1gInSc4
(	(	kIx(
<g/>
Suillellus	Suillellus	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc4
Adalgisin	Adalgisin	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
atlantický	atlantický	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc1
koloděj	koloděj	k1gMnSc1
(	(	kIx(
<g/>
kavkazský	kavkazský	k2eAgMnSc1d1
<g/>
,	,	kIx,
rudomasý	rudomasý	k2eAgMnSc1d1
<g/>
)	)	kIx)
•	•	k?
Hřib	hřib	k1gInSc4
kolodějovitý	kolodějovitý	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
polosíťkovaný	polosíťkovaný	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
Quéletův	Quéletův	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
tajemný	tajemný	k2eAgInSc4d1
Hřib	hřib	k1gInSc4
(	(	kIx(
<g/>
Tylopilus	Tylopilus	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc1
nachovýtrusý	nachovýtrusý	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
žlučník	žlučník	k1gInSc1
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Xerocomus	Xerocomus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc4
osmahlý	osmahlý	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
plstnatý	plstnatý	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
topolový	topolový	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
zlatokořenný	zlatokořenný	k2eAgInSc4d1
Hřib	hřib	k1gInSc4
(	(	kIx(
<g/>
Xerocomellus	Xerocomellus	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Hřib	hřib	k1gInSc4
Markův	Markův	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
mokřadní	mokřadní	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
políčkatý	políčkatý	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
Redeuilhův	Redeuilhův	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
sametový	sametový	k2eAgInSc4d1
•	•	k?
Hřib	hřib	k1gInSc4
Sarnariho	Sarnari	k1gMnSc2
•	•	k?
Hřib	hřib	k1gInSc1
suchomilný	suchomilný	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
uťatovýtrusý	uťatovýtrusý	k2eAgInSc1d1
•	•	k?
Hřib	hřib	k1gInSc1
žlutomasý	žlutomasý	k2eAgMnSc1d1
Kozák	Kozák	k1gMnSc1
(	(	kIx(
<g/>
Leccinellum	Leccinellum	k1gInSc1
<g/>
)	)	kIx)
<g/>
Kozák	kozák	k1gInSc1
a	a	k8xC
křemenáč	křemenáč	k1gInSc1
<g/>
(	(	kIx(
<g/>
Leccinum	Leccinum	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kozák	Kozák	k1gMnSc1
dubový	dubový	k2eAgMnSc1d1
•	•	k?
Kozák	Kozák	k1gMnSc1
habrový	habrový	k2eAgMnSc1d1
•	•	k?
Kozák	Kozák	k1gMnSc1
půvabný	půvabný	k2eAgMnSc1d1
•	•	k?
Kozák	Kozák	k1gMnSc1
korsickýKozák	korsickýKozák	k1gMnSc1
bílý	bílý	k1gMnSc1
•	•	k?
Kozák	Kozák	k1gMnSc1
březový	březový	k2eAgMnSc1d1
•	•	k?
Kozák	Kozák	k1gMnSc1
černohnědý	černohnědý	k2eAgMnSc1d1
•	•	k?
Kozák	Kozák	k1gMnSc1
maličký	maličký	k1gMnSc1
•	•	k?
Kozák	Kozák	k1gMnSc1
pískomilný	pískomilný	k2eAgMnSc1d1
•	•	k?
Kozák	Kozák	k1gMnSc1
šedohnědý	šedohnědý	k2eAgMnSc1d1
•	•	k?
Kozák	Kozák	k1gMnSc1
šedozelený	šedozelený	k2eAgMnSc1d1
•	•	k?
Kozák	Kozák	k1gMnSc1
topolový	topolový	k2eAgMnSc1d1
•	•	k?
Kozák	Kozák	k1gMnSc1
zelenající	zelenající	k2eAgMnSc1d1
•	•	k?
Křemenáč	křemenáč	k1gInSc4
bělostný	bělostný	k2eAgInSc4d1
•	•	k?
Křemenáč	křemenáč	k1gInSc4
bělotřeňový	bělotřeňový	k2eAgInSc4d1
•	•	k?
Křemenáč	křemenáč	k1gInSc4
borový	borový	k2eAgInSc4d1
•	•	k?
Křemenáč	křemenáč	k1gInSc4
březový	březový	k2eAgInSc4d1
•	•	k?
Křemenáč	křemenáč	k1gInSc4
černotřeňový	černotřeňový	k2eAgInSc4d1
•	•	k?
Křemenáč	křemenáč	k1gInSc4
dubový	dubový	k2eAgInSc4d1
•	•	k?
Křemenáč	křemenáč	k1gInSc4
hnědý	hnědý	k2eAgInSc4d1
•	•	k?
Křemenáč	křemenáč	k1gInSc4
osikový	osikový	k2eAgInSc4d1
•	•	k?
Křemenáč	křemenáč	k1gInSc4
smrkový	smrkový	k2eAgInSc4d1
Další	další	k2eAgFnSc7d1
rody	rod	k1gInPc7
</s>
<s>
Lupenopórka	Lupenopórka	k1gFnSc1
(	(	kIx(
<g/>
Lupenopórka	Lupenopórka	k1gFnSc1
červenožlutá	červenožlutý	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Šiškovec	Šiškovec	k1gInSc1
(	(	kIx(
<g/>
Šiškovec	Šiškovec	k1gInSc1
černý	černý	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
Lanmoa	Lanmoa	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Hřib	hřib	k1gInSc4
vonný	vonný	k2eAgInSc4d1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Houby	houba	k1gFnPc5
</s>
