<s>
Decca	Decca	k1gFnSc1	Decca
Records	Recordsa	k1gFnPc2	Recordsa
je	být	k5eAaImIp3nS	být
britské	britský	k2eAgNnSc4d1	Britské
hudební	hudební	k2eAgNnSc4d1	hudební
vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
založené	založený	k2eAgNnSc4d1	založené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
Edwardem	Edward	k1gMnSc7	Edward
Lewisem	Lewis	k1gInSc7	Lewis
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
vlastníkem	vlastník	k1gMnSc7	vlastník
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
Universal	Universal	k1gFnSc2	Universal
Music	Music	k1gMnSc1	Music
Group	Group	k1gMnSc1	Group
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
umělce	umělec	k1gMnPc4	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
u	u	k7c2	u
tohoto	tento	k3xDgNnSc2	tento
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
vydávali	vydávat	k5eAaImAgMnP	vydávat
své	svůj	k3xOyFgFnPc4	svůj
nahrávky	nahrávka	k1gFnPc4	nahrávka
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Thin	Thin	k1gMnSc1	Thin
Lizzy	Lizza	k1gFnSc2	Lizza
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gInSc1	Stones
<g/>
,	,	kIx,	,
Annie	Annie	k1gFnSc1	Annie
Lennox	Lennox	k1gInSc1	Lennox
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Who	Who	k1gFnSc1	Who
<g/>
,	,	kIx,	,
Billie	Billie	k1gFnPc1	Billie
Holiday	Holida	k2eAgFnPc1d1	Holida
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
