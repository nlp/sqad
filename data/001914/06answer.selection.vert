<s>
Transferová	transferový	k2eAgFnSc1d1	transferová
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
tRNA	trnout	k5eAaImSgMnS	trnout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
svým	svůj	k3xOyFgMnSc7	svůj
antikodonem	antikodon	k1gMnSc7	antikodon
na	na	k7c4	na
kodon	kodon	k1gInSc4	kodon
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
přesně	přesně	k6eAd1	přesně
rozeznána	rozeznán	k2eAgFnSc1d1	rozeznána
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nP	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
záměnám	záměna	k1gFnPc3	záměna
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
a	a	k8xC	a
chybnému	chybný	k2eAgNnSc3d1	chybné
čtení	čtení	k1gNnSc3	čtení
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
