<p>
<s>
Kazimierz	Kazimierz	k1gMnSc1	Kazimierz
Dejmek	Dejmek	k1gMnSc1	Dejmek
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1924	[number]	k4	1924
Kovel	Kovel	k1gInSc1	Kovel
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2002	[number]	k4	2002
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
polský	polský	k2eAgMnSc1d1	polský
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Kovelu	Kovel	k1gInSc6	Kovel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
mezi	mezi	k7c7	mezi
světovými	světový	k2eAgFnPc7d1	světová
válkami	válka	k1gFnPc7	válka
součástí	součást	k1gFnSc7	součást
polských	polský	k2eAgMnPc2d1	polský
Kresů	Kres	k1gMnPc2	Kres
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
bojoval	bojovat	k5eAaImAgInS	bojovat
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
Zemské	zemský	k2eAgFnSc2d1	zemská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hercem	herec	k1gMnSc7	herec
vojenského	vojenský	k2eAgNnSc2d1	vojenské
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Lodži	Lodž	k1gFnSc6	Lodž
a	a	k8xC	a
studoval	studovat	k5eAaImAgMnS	studovat
divadelní	divadelní	k2eAgFnSc4d1	divadelní
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgInS	hrát
také	také	k9	také
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Eugeniusze	Eugeniusze	k1gFnSc2	Eugeniusze
Cękalskeho	Cękalske	k1gMnSc2	Cękalske
Jasná	jasný	k2eAgFnSc1d1	jasná
pole	pole	k1gFnSc1	pole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Januszem	Janusz	k1gInSc7	Janusz
Warmińskim	Warmińskima	k1gFnPc2	Warmińskima
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
Lodži	Lodž	k1gFnSc6	Lodž
Teatr	Teatr	k1gInSc4	Teatr
Nowy	Nowa	k1gMnSc2	Nowa
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
zprvu	zprvu	k6eAd1	zprvu
vedli	vést	k5eAaImAgMnP	vést
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
socialistického	socialistický	k2eAgInSc2d1	socialistický
realismu	realismus	k1gInSc2	realismus
(	(	kIx(	(
<g/>
prvním	první	k4xOgMnSc6	první
představením	představení	k1gNnSc7	představení
byla	být	k5eAaImAgFnS	být
Parta	parta	k1gFnSc1	parta
brusiče	brusič	k1gInSc2	brusič
Karhana	Karhana	k?	Karhana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Leona	Leo	k1gMnSc2	Leo
Schillera	Schiller	k1gMnSc2	Schiller
přiklonil	přiklonit	k5eAaPmAgMnS	přiklonit
Dejmek	Dejmek	k1gMnSc1	Dejmek
jako	jako	k8xS	jako
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
dramaturg	dramaturg	k1gMnSc1	dramaturg
k	k	k7c3	k
antropologickému	antropologický	k2eAgNnSc3d1	antropologické
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
,	,	kIx,	,
zaměřoval	zaměřovat	k5eAaImAgMnS	zaměřovat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
polskou	polský	k2eAgFnSc4d1	polská
klasiku	klasika	k1gFnSc4	klasika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
byl	být	k5eAaImAgInS	být
uměleckým	umělecký	k2eAgMnSc7d1	umělecký
vedoucím	vedoucí	k1gMnSc7	vedoucí
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
připravil	připravit	k5eAaPmAgMnS	připravit
inscenaci	inscenace	k1gFnSc4	inscenace
Dziadů	Dziad	k1gInPc2	Dziad
Adama	Adam	k1gMnSc2	Adam
Mickiewicze	Mickiewicze	k1gFnSc2	Mickiewicze
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
záhy	záhy	k6eAd1	záhy
zakázána	zakázat	k5eAaPmNgFnS	zakázat
pro	pro	k7c4	pro
údajně	údajně	k6eAd1	údajně
podvratný	podvratný	k2eAgInSc4d1	podvratný
a	a	k8xC	a
protisovětský	protisovětský	k2eAgInSc4d1	protisovětský
obsah	obsah	k1gInSc4	obsah
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
rozsáhlým	rozsáhlý	k2eAgInPc3d1	rozsáhlý
studentským	studentský	k2eAgInPc3d1	studentský
protestům	protest	k1gInPc3	protest
<g/>
,	,	kIx,	,
známým	známý	k1gMnSc7	známý
jako	jako	k8xC	jako
polská	polský	k2eAgFnSc1d1	polská
politická	politický	k2eAgFnSc1d1	politická
krize	krize	k1gFnSc1	krize
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Dejmek	Dejmek	k1gMnSc1	Dejmek
byl	být	k5eAaImAgMnS	být
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
z	z	k7c2	z
Polské	polský	k2eAgFnSc2d1	polská
sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
mu	on	k3xPp3gMnSc3	on
znemožňována	znemožňován	k2eAgFnSc1d1	znemožňována
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
proto	proto	k8xC	proto
odešel	odejít	k5eAaPmAgMnS	odejít
režírovat	režírovat	k5eAaImF	režírovat
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
působil	působit	k5eAaImAgMnS	působit
mj.	mj.	kA	mj.
v	v	k7c6	v
Burgtheateru	Burgtheater	k1gInSc6	Burgtheater
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgInS	vrátit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
Teatru	teatrum	k1gNnSc6	teatrum
Noweho	Nowe	k1gMnSc4	Nowe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
udělena	udělen	k2eAgFnSc1d1	udělena
Herderova	Herderův	k2eAgFnSc1d1	Herderova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
přínos	přínos	k1gInSc4	přínos
evropské	evropský	k2eAgFnSc3d1	Evropská
kultuře	kultura	k1gFnSc3	kultura
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
obdržel	obdržet	k5eAaPmAgInS	obdržet
titul	titul	k1gInSc4	titul
zasloužilého	zasloužilý	k2eAgMnSc2d1	zasloužilý
umělce	umělec	k1gMnSc2	umělec
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
velkokříž	velkokříž	k1gInSc4	velkokříž
Řádu	řád	k1gInSc2	řád
znovuzrozeného	znovuzrozený	k2eAgNnSc2d1	znovuzrozené
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
Polského	polský	k2eAgInSc2d1	polský
svazu	svaz	k1gInSc2	svaz
dramatických	dramatický	k2eAgMnPc2d1	dramatický
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
poslancem	poslanec	k1gMnSc7	poslanec
za	za	k7c4	za
Polskou	polský	k2eAgFnSc4d1	polská
lidovou	lidový	k2eAgFnSc4d1	lidová
stranu	strana	k1gFnSc4	strana
a	a	k8xC	a
premiér	premiér	k1gMnSc1	premiér
Waldemar	Waldemar	k1gMnSc1	Waldemar
Pawlak	Pawlak	k1gMnSc1	Pawlak
ho	on	k3xPp3gMnSc4	on
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
ministrem	ministr	k1gMnSc7	ministr
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
zastával	zastávat	k5eAaImAgMnS	zastávat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
čestným	čestný	k2eAgMnSc7d1	čestný
občanem	občan	k1gMnSc7	občan
města	město	k1gNnSc2	město
Lodž	Lodž	k1gFnSc4	Lodž
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
dostal	dostat	k5eAaPmAgInS	dostat
Teatr	Teatr	k1gInSc1	Teatr
Nowy	Nowa	k1gFnSc2	Nowa
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
Divadlo	divadlo	k1gNnSc1	divadlo
Kazimierze	Kazimierze	k1gFnSc2	Kazimierze
Dejmka	Dejmek	k1gMnSc2	Dejmek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kazimierz	Kazimierz	k1gMnSc1	Kazimierz
Dejmek	Dejmek	k1gMnSc1	Dejmek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Kazimierz	Kazimierz	k1gMnSc1	Kazimierz
Dejmek	Dejmek	k1gMnSc1	Dejmek
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
na	na	k7c6	na
webu	web	k1gInSc6	web
"	"	kIx"	"
<g/>
Teatr	Teatr	k1gInSc1	Teatr
w	w	k?	w
Polsce	Polska	k1gFnSc6	Polska
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
na	na	k7c6	na
webu	web	k1gInSc6	web
Mickiewiczova	Mickiewiczův	k2eAgInSc2d1	Mickiewiczův
ústavu	ústav	k1gInSc2	ústav
</s>
</p>
