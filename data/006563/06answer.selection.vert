<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
je	být	k5eAaImIp3nS	být
transkripce	transkripce	k1gFnSc1	transkripce
poměrně	poměrně	k6eAd1	poměrně
nekompromisně	kompromisně	k6eNd1	kompromisně
regulována	regulován	k2eAgFnSc1d1	regulována
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
nadměrné	nadměrný	k2eAgFnSc3d1	nadměrná
genové	genový	k2eAgFnSc3d1	genová
expresi	exprese	k1gFnSc3	exprese
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
mRNA	mRNA	k?	mRNA
<g/>
.	.	kIx.	.
</s>
