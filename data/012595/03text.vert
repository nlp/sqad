<p>
<s>
Šanta	šanta	k1gFnSc1	šanta
kočičí	kočičí	k2eAgFnSc1d1	kočičí
(	(	kIx(	(
<g/>
Nepeta	Nepeta	k1gFnSc1	Nepeta
cataria	catarium	k1gNnSc2	catarium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rostlina	rostlina	k1gFnSc1	rostlina
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
bylinkářství	bylinkářství	k1gNnSc6	bylinkářství
a	a	k8xC	a
chovatelství	chovatelství	k1gNnSc6	chovatelství
koček	kočka	k1gFnPc2	kočka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
až	až	k9	až
60	[number]	k4	60
<g/>
cm	cm	kA	cm
vysokou	vysoký	k2eAgFnSc4d1	vysoká
trvalku	trvalka	k1gFnSc4	trvalka
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
v	v	k7c6	v
trsech	trs	k1gInPc6	trs
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
však	však	k9	však
za	za	k7c2	za
dobrých	dobrý	k2eAgFnPc2d1	dobrá
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
vhodné	vhodný	k2eAgFnSc6d1	vhodná
půdě	půda	k1gFnSc6	půda
může	moct	k5eAaImIp3nS	moct
přesáhnou	přesáhnout	k5eAaPmIp3nP	přesáhnout
délku	délka	k1gFnSc4	délka
1	[number]	k4	1
<g/>
m.	m.	k?	m.
Lodyha	lodyha	k1gFnSc1	lodyha
je	být	k5eAaImIp3nS	být
přímá	přímý	k2eAgFnSc1d1	přímá
čtyřhranná	čtyřhranný	k2eAgFnSc1d1	čtyřhranná
větvená	větvený	k2eAgFnSc1d1	větvená
s	s	k7c7	s
hustou	hustý	k2eAgFnSc7d1	hustá
krátkou	krátký	k2eAgFnSc7d1	krátká
plstí	plst	k1gFnSc7	plst
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
srdčitého	srdčitý	k2eAgInSc2d1	srdčitý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
vroubkovaně	vroubkovaně	k6eAd1	vroubkovaně
pilovité	pilovitý	k2eAgFnPc1d1	pilovitá
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
rozkvétající	rozkvétající	k2eAgInPc1d1	rozkvétající
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
září	září	k1gNnSc2	září
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgNnSc1d1	malé
<g/>
;	;	kIx,	;
původní	původní	k2eAgFnSc1d1	původní
rostlina	rostlina	k1gFnSc1	rostlina
je	on	k3xPp3gInPc4	on
má	mít	k5eAaImIp3nS	mít
bílé	bílý	k2eAgNnSc1d1	bílé
nebo	nebo	k8xC	nebo
slabě	slabě	k6eAd1	slabě
růžové	růžový	k2eAgInPc4d1	růžový
<g/>
,	,	kIx,	,
zahradní	zahradní	k2eAgInPc4d1	zahradní
kultivary	kultivar	k1gInPc4	kultivar
modré	modrý	k2eAgFnSc2d1	modrá
nebo	nebo	k8xC	nebo
fialové	fialový	k2eAgFnSc2d1	fialová
<g/>
.	.	kIx.	.
</s>
<s>
Rostlina	rostlina	k1gFnSc1	rostlina
je	být	k5eAaImIp3nS	být
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
částí	část	k1gFnPc2	část
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
blízko	blízko	k7c2	blízko
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
na	na	k7c6	na
mezích	mez	k1gFnPc6	mez
a	a	k8xC	a
v	v	k7c6	v
křovinách	křovina	k1gFnPc6	křovina
<g/>
.	.	kIx.	.
</s>
<s>
Rodové	rodový	k2eAgNnSc4d1	rodové
jméno	jméno	k1gNnSc4	jméno
rostliny	rostlina	k1gFnPc1	rostlina
(	(	kIx(	(
<g/>
nepeta	pet	k2eNgFnSc1d1	pet
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
italského	italský	k2eAgNnSc2d1	italské
města	město	k1gNnSc2	město
Nepete	Nepe	k1gNnSc2	Nepe
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
šanta	šanta	k1gFnSc1	šanta
kdysi	kdysi	k6eAd1	kdysi
hojně	hojně	k6eAd1	hojně
pěstovala	pěstovat	k5eAaImAgFnS	pěstovat
<g/>
.	.	kIx.	.
</s>
<s>
Druhové	druhový	k2eAgNnSc4d1	druhové
jméno	jméno	k1gNnSc4	jméno
(	(	kIx(	(
<g/>
cataria	catarium	k1gNnSc2	catarium
<g/>
)	)	kIx)	)
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
přitažlivostí	přitažlivost	k1gFnSc7	přitažlivost
pro	pro	k7c4	pro
kočky	kočka	k1gFnPc4	kočka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Šanta	šanta	k1gFnSc1	šanta
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
==	==	k?	==
</s>
</p>
<p>
<s>
Šanta	šanta	k1gFnSc1	šanta
se	se	k3xPyFc4	se
již	již	k6eAd1	již
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
staletí	staletí	k1gNnPc4	staletí
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
léčivá	léčivý	k2eAgFnSc1d1	léčivá
rostlina	rostlina	k1gFnSc1	rostlina
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
ochucovadlo	ochucovadlo	k1gNnSc1	ochucovadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
halucinogenní	halucinogenní	k2eAgFnPc4d1	halucinogenní
látky	látka	k1gFnPc4	látka
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc4	její
sušené	sušený	k2eAgInPc4d1	sušený
listy	list	k1gInPc4	list
kdysi	kdysi	k6eAd1	kdysi
kouřili	kouřit	k5eAaImAgMnP	kouřit
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
alespoň	alespoň	k9	alespoň
načas	načas	k6eAd1	načas
zahnali	zahnat	k5eAaPmAgMnP	zahnat
strasti	strast	k1gFnPc4	strast
běžného	běžný	k2eAgInSc2d1	běžný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Žvýkání	žvýkání	k1gNnSc1	žvýkání
kořene	kořen	k1gInSc2	kořen
se	se	k3xPyFc4	se
zase	zase	k9	zase
doporučovalo	doporučovat	k5eAaImAgNnS	doporučovat
osobám	osoba	k1gFnPc3	osoba
plachým	plachý	k2eAgFnPc3d1	plachá
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gFnPc3	on
dodalo	dodat	k5eAaPmAgNnS	dodat
více	hodně	k6eAd2	hodně
odvahy	odvaha	k1gFnSc2	odvaha
a	a	k8xC	a
průbojnosti	průbojnost	k1gFnSc2	průbojnost
<g/>
.	.	kIx.	.
</s>
<s>
Traduje	tradovat	k5eAaImIp3nS	tradovat
se	se	k3xPyFc4	se
prohlášení	prohlášení	k1gNnSc2	prohlášení
jednoho	jeden	k4xCgMnSc2	jeden
kata	kat	k1gMnSc2	kat
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nP	by
prý	prý	k9	prý
nikdy	nikdy	k6eAd1	nikdy
nemohl	moct	k5eNaImAgMnS	moct
vykonat	vykonat	k5eAaPmF	vykonat
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
předtím	předtím	k6eAd1	předtím
nesnědl	snědnout	k5eNaImAgInS	snědnout
kousek	kousek	k1gInSc1	kousek
kořene	kořen	k1gInSc2	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
odvar	odvar	k1gInSc1	odvar
z	z	k7c2	z
šanty	šanta	k1gFnSc2	šanta
používal	používat	k5eAaImAgMnS	používat
jako	jako	k9	jako
uklidňující	uklidňující	k2eAgInSc4d1	uklidňující
čaj	čaj	k1gInSc4	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Odvaru	odvar	k1gInSc2	odvar
byly	být	k5eAaImAgFnP	být
připisovány	připisovat	k5eAaImNgInP	připisovat
také	také	k9	také
pozitivní	pozitivní	k2eAgInPc1d1	pozitivní
účinky	účinek	k1gInPc1	účinek
na	na	k7c4	na
plodnost	plodnost	k1gFnSc4	plodnost
<g/>
;	;	kIx,	;
ženy	žena	k1gFnPc1	žena
si	se	k3xPyFc3	se
jej	on	k3xPp3gInSc2	on
měly	mít	k5eAaImAgInP	mít
přidávat	přidávat	k5eAaImF	přidávat
do	do	k7c2	do
koupele	koupel	k1gFnSc2	koupel
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
šanta	šanta	k1gFnSc1	šanta
doporučována	doporučován	k2eAgFnSc1d1	doporučována
pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
nachlazení	nachlazení	k1gNnSc2	nachlazení
a	a	k8xC	a
horečky	horečka	k1gFnSc2	horečka
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
podporuje	podporovat	k5eAaImIp3nS	podporovat
pocení	pocení	k1gNnSc4	pocení
a	a	k8xC	a
snižuje	snižovat	k5eAaImIp3nS	snižovat
teplotu	teplota	k1gFnSc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
od	od	k7c2	od
bolestí	bolest	k1gFnPc2	bolest
hlavy	hlava	k1gFnSc2	hlava
nervového	nervový	k2eAgInSc2d1	nervový
původu	původ	k1gInSc2	původ
a	a	k8xC	a
při	při	k7c6	při
poruchách	porucha	k1gFnPc6	porucha
trávení	trávení	k1gNnSc2	trávení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
obkladu	obklad	k1gInSc2	obklad
zklidňuje	zklidňovat	k5eAaImIp3nS	zklidňovat
podrážděnou	podrážděný	k2eAgFnSc4d1	podrážděná
pokožku	pokožka	k1gFnSc4	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Čerstvě	čerstvě	k6eAd1	čerstvě
nasekaná	nasekaný	k2eAgFnSc1d1	nasekaná
rostlina	rostlina	k1gFnSc1	rostlina
má	mít	k5eAaImIp3nS	mít
hojivé	hojivý	k2eAgInPc4d1	hojivý
účinky	účinek	k1gInPc4	účinek
na	na	k7c4	na
pohmožděniny	pohmožděnina	k1gFnPc4	pohmožděnina
<g/>
.	.	kIx.	.
<g/>
Látky	látka	k1gFnPc4	látka
obsažené	obsažený	k2eAgFnPc4d1	obsažená
v	v	k7c6	v
šantě	šanta	k1gFnSc6	šanta
odpuzují	odpuzovat	k5eAaImIp3nP	odpuzovat
komáry	komár	k1gMnPc4	komár
a	a	k8xC	a
šváby	šváb	k1gMnPc4	šváb
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
také	také	k9	také
krysy	krysa	k1gFnSc2	krysa
a	a	k8xC	a
myši	myš	k1gFnSc2	myš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Šanta	šanta	k1gFnSc1	šanta
a	a	k8xC	a
kočky	kočka	k1gFnPc1	kočka
==	==	k?	==
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
již	již	k9	již
sám	sám	k3xTgInSc1	sám
název	název	k1gInSc1	název
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
šanta	šanta	k1gFnSc1	šanta
silně	silně	k6eAd1	silně
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
kočkami	kočka	k1gFnPc7	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
savce	savec	k1gMnPc4	savec
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
minimální	minimální	k2eAgInPc1d1	minimální
účinky	účinek	k1gInPc1	účinek
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
působení	působení	k1gNnSc4	působení
na	na	k7c4	na
kočky	kočka	k1gFnPc4	kočka
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
někdy	někdy	k6eAd1	někdy
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
"	"	kIx"	"
<g/>
kočičí	kočičí	k2eAgInSc4d1	kočičí
kokain	kokain	k1gInSc4	kokain
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
kočky	kočka	k1gFnPc1	kočka
v	v	k7c6	v
porostu	porost	k1gInSc6	porost
šanty	šanta	k1gFnSc2	šanta
válejí	válet	k5eAaImIp3nP	válet
<g/>
,	,	kIx,	,
trhají	trhat	k5eAaImIp3nP	trhat
a	a	k8xC	a
kousají	kousat	k5eAaImIp3nP	kousat
listy	list	k1gInPc4	list
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
svinou	svinout	k5eAaPmIp3nP	svinout
do	do	k7c2	do
klubíčka	klubíčko	k1gNnSc2	klubíčko
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
chování	chování	k1gNnSc1	chování
je	být	k5eAaImIp3nS	být
vyvoláno	vyvolat	k5eAaPmNgNnS	vyvolat
zejména	zejména	k9	zejména
nepetalaktonem	nepetalakton	k1gInSc7	nepetalakton
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
který	který	k3yQgInSc1	který
kočky	kočka	k1gFnPc1	kočka
vdechují	vdechovat	k5eAaImIp3nP	vdechovat
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc4d1	stejný
efekt	efekt	k1gInSc4	efekt
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
výtažky	výtažek	k1gInPc1	výtažek
z	z	k7c2	z
čerstvých	čerstvý	k2eAgFnPc2d1	čerstvá
rostlin	rostlina	k1gFnPc2	rostlina
nebo	nebo	k8xC	nebo
sušené	sušený	k2eAgFnPc1d1	sušená
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
např.	např.	kA	např.
přidávají	přidávat	k5eAaImIp3nP	přidávat
do	do	k7c2	do
hraček	hračka	k1gFnPc2	hračka
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
postřiku	postřik	k1gInSc2	postřik
aplikují	aplikovat	k5eAaBmIp3nP	aplikovat
na	na	k7c4	na
škrabadla	škrabadlo	k1gNnPc4	škrabadlo
<g/>
.	.	kIx.	.
</s>
<s>
Efekt	efekt	k1gInSc1	efekt
vyvolaný	vyvolaný	k2eAgInSc1d1	vyvolaný
šantou	šanta	k1gFnSc7	šanta
trvá	trvat	k5eAaImIp3nS	trvat
pět	pět	k4xCc4	pět
až	až	k9	až
deset	deset	k4xCc4	deset
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
útlumu	útlum	k1gInSc3	útlum
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
kočky	kočka	k1gFnPc1	kočka
části	část	k1gFnSc2	část
šanty	šanta	k1gFnSc2	šanta
obvykle	obvykle	k6eAd1	obvykle
snědí	sníst	k5eAaPmIp3nP	sníst
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ta	ten	k3xDgFnSc1	ten
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
posléze	posléze	k6eAd1	posléze
působí	působit	k5eAaImIp3nP	působit
jako	jako	k8xC	jako
sedativum	sedativum	k1gNnSc4	sedativum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
šanta	šanta	k1gFnSc1	šanta
působí	působit	k5eAaImIp3nS	působit
jen	jen	k9	jen
asi	asi	k9	asi
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
koček	kočka	k1gFnPc2	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Kočky	kočka	k1gFnPc1	kočka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
jsou	být	k5eAaImIp3nP	být
vnímavé	vnímavý	k2eAgInPc4d1	vnímavý
<g/>
,	,	kIx,	,
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
reagují	reagovat	k5eAaBmIp3nP	reagovat
s	s	k7c7	s
různě	různě	k6eAd1	různě
silnou	silný	k2eAgFnSc7d1	silná
intenzitou	intenzita	k1gFnSc7	intenzita
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
zpozorován	zpozorován	k2eAgInSc1d1	zpozorován
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
kočkami	kočka	k1gFnPc7	kočka
a	a	k8xC	a
kocoury	kocour	k1gInPc7	kocour
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
koťata	kotě	k1gNnPc1	kotě
mladší	mladý	k2eAgFnSc2d2	mladší
osmi	osm	k4xCc2	osm
týdnů	týden	k1gInPc2	týden
nejenže	nejenže	k6eAd1	nejenže
na	na	k7c4	na
šantu	šanta	k1gFnSc4	šanta
nereagují	reagovat	k5eNaBmIp3nP	reagovat
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
přímo	přímo	k6eAd1	přímo
projevují	projevovat	k5eAaImIp3nP	projevovat
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
senzitivita	senzitivita	k1gFnSc1	senzitivita
je	být	k5eAaImIp3nS	být
dědičná	dědičný	k2eAgFnSc1d1	dědičná
<g/>
.	.	kIx.	.
</s>
<s>
Podrobný	podrobný	k2eAgInSc1d1	podrobný
výzkum	výzkum	k1gInSc1	výzkum
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
<g/>
.	.	kIx.	.
</s>
<s>
Odborníci	odborník	k1gMnPc1	odborník
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yQnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rostlina	rostlina	k1gFnSc1	rostlina
láká	lákat	k5eAaImIp3nS	lákat
kočky	kočka	k1gFnSc2	kočka
svou	svůj	k3xOyFgFnSc7	svůj
vůní	vůně	k1gFnSc7	vůně
záměrně	záměrně	k6eAd1	záměrně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jim	on	k3xPp3gMnPc3	on
při	při	k7c6	při
válení	válení	k1gNnSc6	válení
se	se	k3xPyFc4	se
v	v	k7c6	v
porostu	porost	k1gInSc6	porost
ulpí	ulpět	k5eAaPmIp3nS	ulpět
na	na	k7c6	na
kožichu	kožich	k1gInSc6	kožich
šantová	šantová	k1gFnSc1	šantová
semínka	semínko	k1gNnSc2	semínko
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
pak	pak	k6eAd1	pak
kočky	kočka	k1gFnPc4	kočka
při	při	k7c6	při
svých	svůj	k3xOyFgFnPc6	svůj
toulkách	toulka	k1gFnPc6	toulka
roznesou	roznést	k5eAaPmIp3nP	roznést
po	po	k7c6	po
okolí	okolí	k1gNnSc6	okolí
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zajistí	zajistit	k5eAaPmIp3nS	zajistit
další	další	k2eAgNnSc1d1	další
rozšíření	rozšíření	k1gNnSc1	rozšíření
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šanta	šanta	k1gFnSc1	šanta
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
kočky	kočka	k1gFnPc4	kočka
neškodná	škodný	k2eNgFnSc1d1	neškodná
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
účinky	účinek	k1gInPc4	účinek
a	a	k8xC	a
dle	dle	k7c2	dle
známých	známý	k2eAgFnPc2d1	známá
skutečností	skutečnost	k1gFnPc2	skutečnost
není	být	k5eNaImIp3nS	být
návyková	návykový	k2eAgFnSc1d1	návyková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
s	s	k7c7	s
chovatelskými	chovatelský	k2eAgFnPc7d1	chovatelská
potřebami	potřeba	k1gFnPc7	potřeba
se	se	k3xPyFc4	se
prodává	prodávat	k5eAaImIp3nS	prodávat
buď	buď	k8xC	buď
pod	pod	k7c7	pod
svým	svůj	k3xOyFgInSc7	svůj
českým	český	k2eAgInSc7d1	český
názvem	název	k1gInSc7	název
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
anglickými	anglický	k2eAgInPc7d1	anglický
názvy	název	k1gInPc7	název
catnip	catnip	k1gMnSc1	catnip
či	či	k8xC	či
catmint	catmint	k1gMnSc1	catmint
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
šanta	šanta	k1gFnSc1	šanta
kočičí	kočičí	k2eAgFnSc1d1	kočičí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
šanta	šanta	k1gFnSc1	šanta
kočičí	kočičí	k2eAgFnSc4d1	kočičí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
šanta	šanta	k1gFnSc1	šanta
kočičí	kočičí	k2eAgNnSc4d1	kočičí
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Nepeta	Nepeto	k1gNnSc2	Nepeto
cataria	catarium	k1gNnSc2	catarium
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
<p>
<s>
BioLib	BioLib	k1gMnSc1	BioLib
</s>
</p>
<p>
<s>
Modrý	modrý	k2eAgInSc1d1	modrý
kocouř	kocouř	k1gInSc1	kocouř
</s>
</p>
<p>
<s>
Zmatení	zmatení	k1gNnSc1	zmatení
vůní	vůně	k1gFnPc2	vůně
</s>
</p>
