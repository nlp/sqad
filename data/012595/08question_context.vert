<s>
Šanta	šanta	k1gFnSc1	šanta
kočičí	kočičí	k2eAgFnSc1d1	kočičí
(	(	kIx(	(
<g/>
Nepeta	Nepeta	k1gFnSc1	Nepeta
cataria	catarium	k1gNnSc2	catarium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rostlina	rostlina	k1gFnSc1	rostlina
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
bylinkářství	bylinkářství	k1gNnSc6	bylinkářství
a	a	k8xC	a
chovatelství	chovatelství	k1gNnSc6	chovatelství
koček	kočka	k1gFnPc2	kočka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
až	až	k9	až
60	[number]	k4	60
<g/>
cm	cm	kA	cm
vysokou	vysoký	k2eAgFnSc4d1	vysoká
trvalku	trvalka	k1gFnSc4	trvalka
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
v	v	k7c6	v
trsech	trs	k1gInPc6	trs
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
však	však	k9	však
za	za	k7c2	za
dobrých	dobrý	k2eAgFnPc2d1	dobrá
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
vhodné	vhodný	k2eAgFnSc6d1	vhodná
půdě	půda	k1gFnSc6	půda
může	moct	k5eAaImIp3nS	moct
přesáhnou	přesáhnout	k5eAaPmIp3nP	přesáhnout
délku	délka	k1gFnSc4	délka
1	[number]	k4	1
<g/>
m.	m.	k?	m.
Lodyha	lodyha	k1gFnSc1	lodyha
je	být	k5eAaImIp3nS	být
přímá	přímý	k2eAgFnSc1d1	přímá
čtyřhranná	čtyřhranný	k2eAgFnSc1d1	čtyřhranná
větvená	větvený	k2eAgFnSc1d1	větvená
s	s	k7c7	s
hustou	hustý	k2eAgFnSc7d1	hustá
krátkou	krátký	k2eAgFnSc7d1	krátká
plstí	plst	k1gFnSc7	plst
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
srdčitého	srdčitý	k2eAgInSc2d1	srdčitý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
vroubkovaně	vroubkovaně	k6eAd1	vroubkovaně
pilovité	pilovitý	k2eAgFnPc1d1	pilovitá
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
rozkvétající	rozkvétající	k2eAgInPc1d1	rozkvétající
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
září	září	k1gNnSc2	září
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgNnSc1d1	malé
<g/>
;	;	kIx,	;
původní	původní	k2eAgFnSc1d1	původní
rostlina	rostlina	k1gFnSc1	rostlina
je	on	k3xPp3gInPc4	on
má	mít	k5eAaImIp3nS	mít
bílé	bílý	k2eAgNnSc1d1	bílé
nebo	nebo	k8xC	nebo
slabě	slabě	k6eAd1	slabě
růžové	růžový	k2eAgInPc4d1	růžový
<g/>
,	,	kIx,	,
zahradní	zahradní	k2eAgInPc4d1	zahradní
kultivary	kultivar	k1gInPc4	kultivar
modré	modrý	k2eAgFnSc2d1	modrá
nebo	nebo	k8xC	nebo
fialové	fialový	k2eAgFnSc2d1	fialová
<g/>
.	.	kIx.	.
</s>
<s>
Rostlina	rostlina	k1gFnSc1	rostlina
je	být	k5eAaImIp3nS	být
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
částí	část	k1gFnPc2	část
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
blízko	blízko	k7c2	blízko
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
na	na	k7c6	na
mezích	mez	k1gFnPc6	mez
a	a	k8xC	a
v	v	k7c6	v
křovinách	křovina	k1gFnPc6	křovina
<g/>
.	.	kIx.	.
</s>
<s>
Rodové	rodový	k2eAgNnSc4d1	rodové
jméno	jméno	k1gNnSc4	jméno
rostliny	rostlina	k1gFnPc1	rostlina
(	(	kIx(	(
<g/>
nepeta	pet	k2eNgFnSc1d1	pet
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
italského	italský	k2eAgNnSc2d1	italské
města	město	k1gNnSc2	město
Nepete	Nepe	k1gNnSc2	Nepe
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
šanta	šanta	k1gFnSc1	šanta
kdysi	kdysi	k6eAd1	kdysi
hojně	hojně	k6eAd1	hojně
pěstovala	pěstovat	k5eAaImAgFnS	pěstovat
<g/>
.	.	kIx.	.
</s>
<s>
Druhové	druhový	k2eAgNnSc4d1	druhové
jméno	jméno	k1gNnSc4	jméno
(	(	kIx(	(
<g/>
cataria	catarium	k1gNnSc2	catarium
<g/>
)	)	kIx)	)
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
přitažlivostí	přitažlivost	k1gFnSc7	přitažlivost
pro	pro	k7c4	pro
kočky	kočka	k1gFnPc4	kočka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Šanta	šanta	k1gFnSc1	šanta
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
==	==	k?	==
</s>
</p>
<p>
<s>
Šanta	šanta	k1gFnSc1	šanta
se	se	k3xPyFc4	se
již	již	k6eAd1	již
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
staletí	staletí	k1gNnPc4	staletí
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
léčivá	léčivý	k2eAgFnSc1d1	léčivá
rostlina	rostlina	k1gFnSc1	rostlina
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
ochucovadlo	ochucovadlo	k1gNnSc1	ochucovadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
halucinogenní	halucinogenní	k2eAgFnPc4d1	halucinogenní
látky	látka	k1gFnPc4	látka
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc4	její
sušené	sušený	k2eAgInPc4d1	sušený
listy	list	k1gInPc4	list
kdysi	kdysi	k6eAd1	kdysi
kouřili	kouřit	k5eAaImAgMnP	kouřit
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
alespoň	alespoň	k9	alespoň
načas	načas	k6eAd1	načas
zahnali	zahnat	k5eAaPmAgMnP	zahnat
strasti	strast	k1gFnPc4	strast
běžného	běžný	k2eAgInSc2d1	běžný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Žvýkání	žvýkání	k1gNnSc1	žvýkání
kořene	kořen	k1gInSc2	kořen
se	se	k3xPyFc4	se
zase	zase	k9	zase
doporučovalo	doporučovat	k5eAaImAgNnS	doporučovat
osobám	osoba	k1gFnPc3	osoba
plachým	plachý	k2eAgFnPc3d1	plachá
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gFnPc3	on
dodalo	dodat	k5eAaPmAgNnS	dodat
více	hodně	k6eAd2	hodně
odvahy	odvaha	k1gFnSc2	odvaha
a	a	k8xC	a
průbojnosti	průbojnost	k1gFnSc2	průbojnost
<g/>
.	.	kIx.	.
</s>
<s>
Traduje	tradovat	k5eAaImIp3nS	tradovat
se	se	k3xPyFc4	se
prohlášení	prohlášení	k1gNnSc2	prohlášení
jednoho	jeden	k4xCgMnSc2	jeden
kata	kat	k1gMnSc2	kat
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nP	by
prý	prý	k9	prý
nikdy	nikdy	k6eAd1	nikdy
nemohl	moct	k5eNaImAgMnS	moct
vykonat	vykonat	k5eAaPmF	vykonat
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
předtím	předtím	k6eAd1	předtím
nesnědl	snědnout	k5eNaImAgInS	snědnout
kousek	kousek	k1gInSc1	kousek
kořene	kořen	k1gInSc2	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
odvar	odvar	k1gInSc1	odvar
z	z	k7c2	z
šanty	šanta	k1gFnSc2	šanta
používal	používat	k5eAaImAgMnS	používat
jako	jako	k9	jako
uklidňující	uklidňující	k2eAgInSc4d1	uklidňující
čaj	čaj	k1gInSc4	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Odvaru	odvar	k1gInSc2	odvar
byly	být	k5eAaImAgFnP	být
připisovány	připisovat	k5eAaImNgInP	připisovat
také	také	k9	také
pozitivní	pozitivní	k2eAgInPc1d1	pozitivní
účinky	účinek	k1gInPc1	účinek
na	na	k7c4	na
plodnost	plodnost	k1gFnSc4	plodnost
<g/>
;	;	kIx,	;
ženy	žena	k1gFnPc1	žena
si	se	k3xPyFc3	se
jej	on	k3xPp3gInSc2	on
měly	mít	k5eAaImAgInP	mít
přidávat	přidávat	k5eAaImF	přidávat
do	do	k7c2	do
koupele	koupel	k1gFnSc2	koupel
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
šanta	šanta	k1gFnSc1	šanta
doporučována	doporučován	k2eAgFnSc1d1	doporučována
pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
nachlazení	nachlazení	k1gNnSc2	nachlazení
a	a	k8xC	a
horečky	horečka	k1gFnSc2	horečka
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
podporuje	podporovat	k5eAaImIp3nS	podporovat
pocení	pocení	k1gNnSc4	pocení
a	a	k8xC	a
snižuje	snižovat	k5eAaImIp3nS	snižovat
teplotu	teplota	k1gFnSc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
od	od	k7c2	od
bolestí	bolest	k1gFnPc2	bolest
hlavy	hlava	k1gFnSc2	hlava
nervového	nervový	k2eAgInSc2d1	nervový
původu	původ	k1gInSc2	původ
a	a	k8xC	a
při	při	k7c6	při
poruchách	porucha	k1gFnPc6	porucha
trávení	trávení	k1gNnSc2	trávení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
obkladu	obklad	k1gInSc2	obklad
zklidňuje	zklidňovat	k5eAaImIp3nS	zklidňovat
podrážděnou	podrážděný	k2eAgFnSc4d1	podrážděná
pokožku	pokožka	k1gFnSc4	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Čerstvě	čerstvě	k6eAd1	čerstvě
nasekaná	nasekaný	k2eAgFnSc1d1	nasekaná
rostlina	rostlina	k1gFnSc1	rostlina
má	mít	k5eAaImIp3nS	mít
hojivé	hojivý	k2eAgInPc4d1	hojivý
účinky	účinek	k1gInPc4	účinek
na	na	k7c4	na
pohmožděniny	pohmožděnina	k1gFnPc4	pohmožděnina
<g/>
.	.	kIx.	.
<g/>
Látky	látka	k1gFnPc4	látka
obsažené	obsažený	k2eAgFnPc4d1	obsažená
v	v	k7c6	v
šantě	šanta	k1gFnSc6	šanta
odpuzují	odpuzovat	k5eAaImIp3nP	odpuzovat
komáry	komár	k1gMnPc4	komár
a	a	k8xC	a
šváby	šváb	k1gMnPc4	šváb
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
také	také	k9	také
krysy	krysa	k1gFnSc2	krysa
a	a	k8xC	a
myši	myš	k1gFnSc2	myš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Šanta	šanta	k1gFnSc1	šanta
a	a	k8xC	a
kočky	kočka	k1gFnPc1	kočka
==	==	k?	==
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
již	již	k9	již
sám	sám	k3xTgInSc1	sám
název	název	k1gInSc1	název
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
šanta	šanta	k1gFnSc1	šanta
silně	silně	k6eAd1	silně
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
kočkami	kočka	k1gFnPc7	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
savce	savec	k1gMnPc4	savec
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
minimální	minimální	k2eAgInPc1d1	minimální
účinky	účinek	k1gInPc1	účinek
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
působení	působení	k1gNnSc4	působení
na	na	k7c4	na
kočky	kočka	k1gFnPc4	kočka
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
někdy	někdy	k6eAd1	někdy
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
"	"	kIx"	"
<g/>
kočičí	kočičí	k2eAgInSc4d1	kočičí
kokain	kokain	k1gInSc4	kokain
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>

