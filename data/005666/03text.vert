<s>
Kapybara	kapybara	k1gFnSc1	kapybara
(	(	kIx(	(
<g/>
Hydrochoerus	Hydrochoerus	k1gInSc1	Hydrochoerus
hydrochaeris	hydrochaeris	k1gInSc1	hydrochaeris
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
žijící	žijící	k2eAgMnSc1d1	žijící
hlodavec	hlodavec	k1gMnSc1	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc7	jejich
přirozeným	přirozený	k2eAgNnSc7d1	přirozené
prostředím	prostředí	k1gNnSc7	prostředí
jsou	být	k5eAaImIp3nP	být
tropické	tropický	k2eAgInPc1d1	tropický
a	a	k8xC	a
teplé	teplý	k2eAgInPc1d1	teplý
kraje	kraj	k1gInPc1	kraj
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
východně	východně	k6eAd1	východně
od	od	k7c2	od
And	Anda	k1gFnPc2	Anda
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
blízko	blízko	k6eAd1	blízko
u	u	k7c2	u
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Panamy	Panama	k1gFnSc2	Panama
až	až	k9	až
po	po	k7c6	po
ústí	ústí	k1gNnSc6	ústí
La	la	k1gNnSc6	la
Platy	plat	k1gInPc4	plat
u	u	k7c2	u
Buenos	Buenosa	k1gFnPc2	Buenosa
Aires	Airesa	k1gFnPc2	Airesa
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
,	,	kIx,	,
východně	východně	k6eAd1	východně
od	od	k7c2	od
And	Anda	k1gFnPc2	Anda
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
vodním	vodní	k2eAgNnSc6d1	vodní
prostředí	prostředí	k1gNnSc6	prostředí
všude	všude	k6eAd1	všude
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ještě	ještě	k6eAd1	ještě
nebyla	být	k5eNaImAgFnS	být
vyhubena	vyhubit	k5eAaPmNgFnS	vyhubit
<g/>
.	.	kIx.	.
</s>
<s>
Západně	západně	k6eAd1	západně
od	od	k7c2	od
And	Anda	k1gFnPc2	Anda
je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
až	až	k9	až
po	po	k7c6	po
zalesněné	zalesněný	k2eAgFnSc6d1	zalesněná
oblasti	oblast	k1gFnSc6	oblast
Chocó	Chocó	k1gFnSc2	Chocó
v	v	k7c6	v
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Obrovský	obrovský	k2eAgInSc1d1	obrovský
areál	areál	k1gInSc1	areál
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
však	však	k9	však
není	být	k5eNaImIp3nS	být
osídlen	osídlen	k2eAgMnSc1d1	osídlen
stejnoměrně	stejnoměrně	k6eAd1	stejnoměrně
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
výskytu	výskyt	k1gInSc2	výskyt
kapybary	kapybara	k1gFnSc2	kapybara
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
na	na	k7c6	na
záplavových	záplavový	k2eAgNnPc6d1	záplavové
územích	území	k1gNnPc6	území
velkých	velký	k2eAgFnPc2d1	velká
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Orinoka	Orinoko	k1gNnSc2	Orinoko
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Llanos	Llanos	k1gInPc2	Llanos
<g/>
,	,	kIx,	,
na	na	k7c6	na
Amazonce	Amazonka	k1gFnSc6	Amazonka
<g/>
,	,	kIx,	,
v	v	k7c6	v
Pantanalu	Pantanal	k1gInSc6	Pantanal
plošiny	plošina	k1gFnSc2	plošina
Mato	Mato	k6eAd1	Mato
Groso	Grosa	k1gFnSc5	Grosa
<g/>
,	,	kIx,	,
na	na	k7c6	na
záplavových	záplavový	k2eAgNnPc6d1	záplavové
územích	území	k1gNnPc6	území
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Gran	Grana	k1gFnPc2	Grana
Chaco	Chaco	k6eAd1	Chaco
a	a	k8xC	a
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Paraná	Paraná	k1gFnSc1	Paraná
až	až	k6eAd1	až
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
La	la	k1gNnSc2	la
Platy	plat	k1gInPc1	plat
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgNnPc4d2	veliký
ohniska	ohnisko	k1gNnPc4	ohnisko
výskytu	výskyt	k1gInSc2	výskyt
kapybary	kapybara	k1gFnSc2	kapybara
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
i	i	k9	i
na	na	k7c6	na
savanách	savana	k1gFnPc6	savana
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
Bolívie	Bolívie	k1gFnSc2	Bolívie
<g/>
.	.	kIx.	.
</s>
<s>
Pobřežní	pobřežní	k2eAgNnSc1d1	pobřežní
pásmo	pásmo	k1gNnSc1	pásmo
řek	řeka	k1gFnPc2	řeka
(	(	kIx(	(
<g/>
vodní	vodní	k2eAgNnSc1d1	vodní
prostředí	prostředí	k1gNnSc1	prostředí
jsou	být	k5eAaImIp3nP	být
kapybary	kapybara	k1gFnPc1	kapybara
nuceny	nucen	k2eAgFnPc1d1	nucena
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
i	i	k9	i
z	z	k7c2	z
termoregulačních	termoregulační	k2eAgInPc2d1	termoregulační
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnSc1	jejich
mohutné	mohutný	k2eAgNnSc1d1	mohutné
tělo	tělo	k1gNnSc1	tělo
rychle	rychle	k6eAd1	rychle
přehřívá	přehřívat	k5eAaImIp3nS	přehřívat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
záplavová	záplavový	k2eAgNnPc1d1	záplavové
území	území	k1gNnPc1	území
s	s	k7c7	s
bohatým	bohatý	k2eAgInSc7d1	bohatý
travním	travní	k2eAgInSc7d1	travní
porostem	porost	k1gInSc7	porost
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
také	také	k9	také
pochází	pocházet	k5eAaImIp3nS	pocházet
jméno	jméno	k1gNnSc4	jméno
tohoto	tento	k3xDgNnSc2	tento
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
kapybara	kapybara	k1gFnSc1	kapybara
nebo	nebo	k8xC	nebo
přesněji	přesně	k6eAd2	přesně
kaapiiúara	kaapiiúara	k1gFnSc1	kaapiiúara
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
indiánském	indiánský	k2eAgInSc6d1	indiánský
jazyku	jazyk	k1gInSc6	jazyk
Guaraní	Guaraní	k1gNnSc2	Guaraní
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
pán	pán	k1gMnSc1	pán
tenkého	tenký	k2eAgNnSc2d1	tenké
listí	listí	k1gNnSc2	listí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
trávožrout	trávožrout	k1gMnSc1	trávožrout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
kaá	kaá	k?	kaá
(	(	kIx(	(
<g/>
list	list	k1gInSc1	list
<g/>
)	)	kIx)	)
+	+	kIx~	+
píi	píi	k?	píi
(	(	kIx(	(
<g/>
tenký	tenký	k2eAgInSc4d1	tenký
<g/>
)	)	kIx)	)
+	+	kIx~	+
ú	ú	k0	ú
(	(	kIx(	(
<g/>
jíst	jíst	k5eAaImF	jíst
<g/>
)	)	kIx)	)
+	+	kIx~	+
ara	ara	k1gMnSc1	ara
(	(	kIx(	(
<g/>
činitelský	činitelský	k2eAgInSc1d1	činitelský
nebo	nebo	k8xC	nebo
vlastnický	vlastnický	k2eAgInSc1d1	vlastnický
sufix	sufix	k1gInSc1	sufix
substantiv	substantivum	k1gNnPc2	substantivum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapybara	kapybara	k1gFnSc1	kapybara
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
žijící	žijící	k2eAgInSc4d1	žijící
druh	druh	k1gInSc4	druh
hlodavce	hlodavec	k1gMnSc2	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Dospělá	dospělý	k2eAgNnPc1d1	dospělé
zvířata	zvíře	k1gNnPc1	zvíře
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
hmotnosti	hmotnost	k1gFnPc1	hmotnost
až	až	k9	až
80	[number]	k4	80
kg	kg	kA	kg
<g/>
,	,	kIx,	,
délky	délka	k1gFnSc2	délka
těla	tělo	k1gNnSc2	tělo
1,3	[number]	k4	1,3
m	m	kA	m
a	a	k8xC	a
výšky	výška	k1gFnSc2	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
55	[number]	k4	55
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
hnědé	hnědý	k2eAgNnSc1d1	hnědé
až	až	k6eAd1	až
rezavé	rezavý	k2eAgNnSc1d1	rezavé
<g/>
,	,	kIx,	,
srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
překryta	překrýt	k5eAaPmNgFnS	překrýt
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
a	a	k8xC	a
hrubými	hrubý	k2eAgInPc7d1	hrubý
pesíky	pesík	k1gInPc7	pesík
<g/>
,	,	kIx,	,
podsada	podsada	k1gFnSc1	podsada
je	být	k5eAaImIp3nS	být
hustá	hustý	k2eAgFnSc1d1	hustá
a	a	k8xC	a
měkká	měkký	k2eAgFnSc1d1	měkká
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
hřbetu	hřbet	k1gInSc2	hřbet
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgMnSc1d2	delší
a	a	k8xC	a
tmavší	tmavý	k2eAgMnSc1d2	tmavší
než	než	k8xS	než
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
pravoúhlého	pravoúhlý	k2eAgInSc2d1	pravoúhlý
hranolu	hranol	k1gInSc2	hranol
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
podtrhuje	podtrhovat	k5eAaImIp3nS	podtrhovat
nápadně	nápadně	k6eAd1	nápadně
ploché	plochý	k2eAgNnSc1d1	ploché
čelo	čelo	k1gNnSc1	čelo
<g/>
.	.	kIx.	.
</s>
<s>
Nozdry	nozdra	k1gFnPc1	nozdra
leží	ležet	k5eAaImIp3nP	ležet
vysoko	vysoko	k6eAd1	vysoko
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
kapybarám	kapybara	k1gFnPc3	kapybara
dýchat	dýchat	k5eAaImF	dýchat
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
vysunovaly	vysunovat	k5eAaImAgFnP	vysunovat
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
ušní	ušní	k2eAgInPc4d1	ušní
boltce	boltec	k1gInPc4	boltec
nad	nad	k7c4	nad
vodní	vodní	k2eAgFnSc4d1	vodní
hladinu	hladina	k1gFnSc4	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Relativně	relativně	k6eAd1	relativně
malé	malý	k2eAgInPc4d1	malý
ušní	ušní	k2eAgInPc4d1	ušní
boltce	boltec	k1gInPc4	boltec
zřetelně	zřetelně	k6eAd1	zřetelně
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
ze	z	k7c2	z
srsti	srst	k1gFnSc2	srst
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgFnPc1d1	malá
a	a	k8xC	a
tmavé	tmavý	k2eAgFnPc1d1	tmavá
<g/>
,	,	kIx,	,
nažloutlé	nažloutlý	k2eAgFnPc1d1	nažloutlá
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
načervenalé	načervenalý	k2eAgFnPc1d1	načervenalá
a	a	k8xC	a
umístěné	umístěný	k2eAgFnPc1d1	umístěná
hodně	hodně	k6eAd1	hodně
vysoko	vysoko	k6eAd1	vysoko
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
hlavy	hlava	k1gFnSc2	hlava
zabírají	zabírat	k5eAaImIp3nP	zabírat
čelisti	čelist	k1gFnPc1	čelist
s	s	k7c7	s
mohutnými	mohutný	k2eAgInPc7d1	mohutný
obloukovitými	obloukovitý	k2eAgInPc7d1	obloukovitý
řezáky	řezák	k1gInPc7	řezák
<g/>
,	,	kIx,	,
hluboko	hluboko	k6eAd1	hluboko
zasazenými	zasazený	k2eAgInPc7d1	zasazený
až	až	k6eAd1	až
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
stoliček	stolička	k1gFnPc2	stolička
<g/>
.	.	kIx.	.
</s>
<s>
Řezáky	řezák	k1gInPc1	řezák
s	s	k7c7	s
neukončeným	ukončený	k2eNgInSc7d1	neukončený
růstem	růst	k1gInSc7	růst
jsou	být	k5eAaImIp3nP	být
zabrušovány	zabrušovat	k5eAaImNgInP	zabrušovat
stálým	stálý	k2eAgNnSc7d1	stálé
hlodáním	hlodání	k1gNnSc7	hlodání
tvrdé	tvrdý	k2eAgFnSc2d1	tvrdá
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Amazonští	amazonský	k2eAgMnPc1d1	amazonský
indiáni	indián	k1gMnPc1	indián
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
dříve	dříve	k6eAd2	dříve
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
nožíky	nožík	k1gInPc4	nožík
a	a	k8xC	a
dláta	dláto	k1gNnPc4	dláto
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnPc1d3	veliký
<g/>
,	,	kIx,	,
dominantní	dominantní	k2eAgMnPc1d1	dominantní
samečci	sameček	k1gMnPc1	sameček
mají	mít	k5eAaImIp3nP	mít
u	u	k7c2	u
kořene	kořen	k1gInSc2	kořen
nosu	nos	k1gInSc2	nos
černě	černě	k6eAd1	černě
se	se	k3xPyFc4	se
lesknoucí	lesknoucí	k2eAgFnSc4d1	lesknoucí
pachovou	pachový	k2eAgFnSc4d1	pachová
kožní	kožní	k2eAgFnSc4d1	kožní
žlázu	žláza	k1gFnSc4	žláza
zvanou	zvaný	k2eAgFnSc4d1	zvaná
"	"	kIx"	"
<g/>
morillo	morillo	k1gNnSc4	morillo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
produkuje	produkovat	k5eAaImIp3nS	produkovat
bělavý	bělavý	k2eAgInSc4d1	bělavý
sekret	sekret	k1gInSc4	sekret
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
kapybary	kapybara	k1gFnPc1	kapybara
využívají	využívat	k5eAaImIp3nP	využívat
ke	k	k7c3	k
značkování	značkování	k1gNnSc3	značkování
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
žlázy	žláza	k1gFnPc1	žláza
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
podstatně	podstatně	k6eAd1	podstatně
menší	malý	k2eAgFnPc1d2	menší
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
samičky	samička	k1gFnPc4	samička
<g/>
.	.	kIx.	.
</s>
<s>
Mohutné	mohutný	k2eAgNnSc1d1	mohutné
tělo	tělo	k1gNnSc1	tělo
kapybary	kapybara	k1gFnSc2	kapybara
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
na	na	k7c6	na
krátkých	krátký	k2eAgFnPc6d1	krátká
končetinách	končetina	k1gFnPc6	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
není	být	k5eNaImIp3nS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
prsty	prst	k1gInPc7	prst
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
krátká	krátký	k2eAgFnSc1d1	krátká
plovací	plovací	k2eAgFnSc1d1	plovací
blána	blána	k1gFnSc1	blána
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
končetinách	končetina	k1gFnPc6	končetina
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgNnPc4	čtyři
,	,	kIx,	,
na	na	k7c6	na
zadních	zadní	k2eAgInPc6d1	zadní
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgInPc4	tři
prsty	prst	k1gInPc4	prst
<g/>
,	,	kIx,	,
zakončené	zakončený	k2eAgInPc4d1	zakončený
drápy	dráp	k1gInPc4	dráp
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
kopýtka	kopýtko	k1gNnSc2	kopýtko
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
bývají	bývat	k5eAaImIp3nP	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
větší	veliký	k2eAgMnPc1d2	veliký
než	než	k8xS	než
samci	samec	k1gMnPc1	samec
<g/>
.	.	kIx.	.
</s>
<s>
Pohlaví	pohlaví	k1gNnSc1	pohlaví
je	být	k5eAaImIp3nS	být
však	však	k9	však
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
nerozlišitelné	rozlišitelný	k2eNgNnSc1d1	nerozlišitelné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
orgány	orgán	k1gInPc1	orgán
jsou	být	k5eAaImIp3nP	být
ukryty	ukrýt	k5eAaPmNgInP	ukrýt
ve	v	k7c6	v
zvláštním	zvláštní	k2eAgInSc6d1	zvláštní
kožním	kožní	k2eAgInSc6d1	kožní
záhybu	záhyb	k1gInSc6	záhyb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
vždy	vždy	k6eAd1	vždy
utíkají	utíkat	k5eAaImIp3nP	utíkat
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
ozývají	ozývat	k5eAaImIp3nP	ozývat
se	se	k3xPyFc4	se
ječivým	ječivý	k2eAgInSc7d1	ječivý
alarmujícím	alarmující	k2eAgInSc7d1	alarmující
hlasem	hlas	k1gInSc7	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Individuální	individuální	k2eAgFnSc1d1	individuální
hlasová	hlasový	k2eAgFnSc1d1	hlasová
komunikace	komunikace	k1gFnSc1	komunikace
probíhá	probíhat	k5eAaImIp3nS	probíhat
pomocí	pomocí	k7c2	pomocí
chrochtání	chrochtání	k1gNnSc2	chrochtání
a	a	k8xC	a
švitoření	švitoření	k1gNnSc2	švitoření
<g/>
,	,	kIx,	,
slyšitelného	slyšitelný	k2eAgInSc2d1	slyšitelný
většinou	většinou	k6eAd1	většinou
jen	jen	k9	jen
zblízka	zblízka	k6eAd1	zblízka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
výborný	výborný	k2eAgMnSc1d1	výborný
plavec	plavec	k1gMnSc1	plavec
a	a	k8xC	a
dovede	dovést	k5eAaPmIp3nS	dovést
se	se	k3xPyFc4	se
i	i	k9	i
potápět	potápět	k5eAaImF	potápět
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
vydrží	vydržet	k5eAaPmIp3nS	vydržet
8	[number]	k4	8
až	až	k9	až
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
samec	samec	k1gInSc1	samec
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
harém	harém	k1gInSc1	harém
samic	samice	k1gFnPc2	samice
různého	různý	k2eAgNnSc2d1	různé
stáří	stáří	k1gNnSc2	stáří
a	a	k8xC	a
páří	pářit	k5eAaImIp3nS	pářit
se	se	k3xPyFc4	se
s	s	k7c7	s
každou	každý	k3xTgFnSc7	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
příhodných	příhodný	k2eAgNnPc6d1	příhodné
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
v	v	k7c6	v
bažinách	bažina	k1gFnPc6	bažina
Ilha	Ilh	k1gInSc2	Ilh
de	de	k?	de
Marajó	Marajó	k1gMnSc1	Marajó
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kapybary	kapybara	k1gFnPc1	kapybara
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
do	do	k7c2	do
velkých	velký	k2eAgFnPc2d1	velká
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
můžeme	moct	k5eAaImIp1nP	moct
tu	tu	k6eAd1	tu
potkat	potkat	k5eAaPmF	potkat
i	i	k9	i
stočlenná	stočlenný	k2eAgNnPc4d1	stočlenné
stáda	stádo	k1gNnPc4	stádo
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
kapybary	kapybara	k1gFnPc1	kapybara
ukrývají	ukrývat	k5eAaImIp3nP	ukrývat
<g/>
,	,	kIx,	,
za	za	k7c2	za
šera	šero	k1gNnSc2	šero
se	se	k3xPyFc4	se
vydávají	vydávat	k5eAaPmIp3nP	vydávat
za	za	k7c7	za
potravou	potrava	k1gFnSc7	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
sociální	sociální	k2eAgNnPc1d1	sociální
chování	chování	k1gNnPc1	chování
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
sucha	sucho	k1gNnSc2	sucho
se	se	k3xPyFc4	se
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
ve	v	k7c6	v
značném	značný	k2eAgInSc6d1	značný
počtu	počet	k1gInSc6	počet
u	u	k7c2	u
zbylých	zbylý	k2eAgFnPc2d1	zbylá
vodních	vodní	k2eAgFnPc2d1	vodní
nádrží	nádrž	k1gFnPc2	nádrž
a	a	k8xC	a
chovají	chovat	k5eAaImIp3nP	chovat
se	se	k3xPyFc4	se
k	k	k7c3	k
sobě	se	k3xPyFc3	se
tolerantně	tolerantně	k6eAd1	tolerantně
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
stoupající	stoupající	k2eAgFnSc7d1	stoupající
vodní	vodní	k2eAgFnSc7d1	vodní
hladinou	hladina	k1gFnSc7	hladina
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
skupiny	skupina	k1gFnPc1	skupina
migrují	migrovat	k5eAaImIp3nP	migrovat
do	do	k7c2	do
okolní	okolní	k2eAgFnSc2d1	okolní
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
dominantní	dominantní	k2eAgMnPc1d1	dominantní
samci	samec	k1gMnPc1	samec
značí	značit	k5eAaImIp3nP	značit
svá	svůj	k3xOyFgNnPc4	svůj
teritoria	teritorium	k1gNnPc4	teritorium
sekretem	sekret	k1gInSc7	sekret
pachových	pachový	k2eAgFnPc2d1	pachová
žláz	žláza	k1gFnPc2	žláza
uložených	uložený	k2eAgFnPc2d1	uložená
u	u	k7c2	u
kořene	kořen	k1gInSc2	kořen
nosu	nos	k1gInSc2	nos
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
plaché	plachý	k2eAgFnPc1d1	plachá
a	a	k8xC	a
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Kapybary	kapybara	k1gFnPc1	kapybara
při	při	k7c6	při
pastvě	pastva	k1gFnSc6	pastva
často	často	k6eAd1	často
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
drobný	drobný	k2eAgMnSc1d1	drobný
pěvec	pěvec	k1gMnSc1	pěvec
tyran	tyran	k1gMnSc1	tyran
dobytčí	dobytčí	k2eAgMnSc1d1	dobytčí
(	(	kIx(	(
<g/>
Machetornis	Machetornis	k1gInSc1	Machetornis
rixosa	rixosa	k1gFnSc1	rixosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
chytá	chytat	k5eAaImIp3nS	chytat
jimi	on	k3xPp3gInPc7	on
vyplašený	vyplašený	k2eAgInSc4d1	vyplašený
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
jim	on	k3xPp3gMnPc3	on
obírá	obírat	k5eAaImIp3nS	obírat
klíšťata	klíště	k1gNnPc4	klíště
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
cizopasníky	cizopasník	k1gMnPc7	cizopasník
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
afričtí	africký	k2eAgMnPc1d1	africký
klubáci	klubák	k1gMnPc1	klubák
tamním	tamní	k2eAgMnPc3d1	tamní
kopytníkům	kopytník	k1gInPc3	kopytník
<g/>
.	.	kIx.	.
</s>
<s>
Páření	páření	k1gNnSc1	páření
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
březí	březí	k1gNnSc1	březí
pět	pět	k4xCc1	pět
až	až	k9	až
pět	pět	k4xCc1	pět
a	a	k8xC	a
půl	půl	k1xP	půl
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
jednou	jednou	k6eAd1	jednou
či	či	k8xC	či
dvakrát	dvakrát	k6eAd1	dvakrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
rodí	rodit	k5eAaImIp3nP	rodit
2-8	[number]	k4	2-8
mláďat	mládě	k1gNnPc2	mládě
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
mláďat	mládě	k1gNnPc2	mládě
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
1,5	[number]	k4	1,5
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
dokonale	dokonale	k6eAd1	dokonale
vyvinutá	vyvinutý	k2eAgNnPc1d1	vyvinuté
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
přijímají	přijímat	k5eAaImIp3nP	přijímat
pevnou	pevný	k2eAgFnSc4d1	pevná
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	on	k3xPp3gFnPc4	on
matka	matka	k1gFnSc1	matka
kojí	kojit	k5eAaImIp3nS	kojit
ještě	ještě	k9	ještě
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
v	v	k7c6	v
15	[number]	k4	15
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Kapybary	kapybara	k1gFnPc1	kapybara
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
8-	[number]	k4	8-
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
rostlinnou	rostlinný	k2eAgFnSc7d1	rostlinná
potravou	potrava	k1gFnSc7	potrava
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
trávami	tráva	k1gFnPc7	tráva
<g/>
,	,	kIx,	,
vodními	vodní	k2eAgFnPc7d1	vodní
rostlinami	rostlina	k1gFnPc7	rostlina
a	a	k8xC	a
kůrou	kůra	k1gFnSc7	kůra
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
sbírají	sbírat	k5eAaImIp3nP	sbírat
spadané	spadaný	k2eAgInPc4d1	spadaný
plody	plod	k1gInPc4	plod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
honbě	honba	k1gFnSc6	honba
za	za	k7c7	za
potravou	potrava	k1gFnSc7	potrava
prohledávají	prohledávat	k5eAaImIp3nP	prohledávat
listí	listý	k2eAgMnPc1d1	listý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
zemědělských	zemědělský	k2eAgFnPc6d1	zemědělská
oblastech	oblast	k1gFnPc6	oblast
občas	občas	k6eAd1	občas
kradou	krást	k5eAaImIp3nP	krást
melouny	meloun	k1gInPc1	meloun
a	a	k8xC	a
zrní	zrní	k1gNnSc1	zrní
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
přirozené	přirozený	k2eAgMnPc4d1	přirozený
nepřátele	nepřítel	k1gMnPc4	nepřítel
kapybar	kapybara	k1gFnPc2	kapybara
patří	patřit	k5eAaImIp3nP	patřit
šelmy	šelma	k1gFnSc2	šelma
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc7d2	menší
kapybarou	kapybara	k1gFnSc7	kapybara
nepohrdne	pohrdnout	k5eNaPmIp3nS	pohrdnout
ani	ani	k8xC	ani
anakonda	anakonda	k1gFnSc1	anakonda
nebo	nebo	k8xC	nebo
kajman	kajman	k1gMnSc1	kajman
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
se	se	k3xPyFc4	se
na	na	k7c6	na
populaci	populace	k1gFnSc6	populace
tohoto	tento	k3xDgNnSc2	tento
zvířete	zvíře	k1gNnSc2	zvíře
podepisuje	podepisovat	k5eAaImIp3nS	podepisovat
lov	lov	k1gInSc4	lov
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
a	a	k8xC	a
pro	pro	k7c4	pro
kůži	kůže	k1gFnSc4	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
lovu	lov	k1gInSc3	lov
je	být	k5eAaImIp3nS	být
však	však	k9	však
počet	počet	k1gInSc1	počet
kapybar	kapybara	k1gFnPc2	kapybara
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
oblastí	oblast	k1gFnPc2	oblast
dosud	dosud	k6eAd1	dosud
relativně	relativně	k6eAd1	relativně
velký	velký	k2eAgInSc1d1	velký
<g/>
.	.	kIx.	.
</s>
<s>
Honáci	honák	k1gMnPc1	honák
dobytka	dobytek	k1gInSc2	dobytek
ve	v	k7c6	v
Venezuele	Venezuela	k1gFnSc6	Venezuela
často	často	k6eAd1	často
kapybary	kapybara	k1gFnSc2	kapybara
loví	lovit	k5eAaImIp3nP	lovit
jen	jen	k9	jen
pro	pro	k7c4	pro
zábavu	zábava	k1gFnSc4	zábava
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vyzkoušeli	vyzkoušet	k5eAaPmAgMnP	vyzkoušet
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
obratnost	obratnost	k1gFnSc4	obratnost
svých	svůj	k3xOyFgMnPc2	svůj
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Venezuele	Venezuela	k1gFnSc6	Venezuela
se	se	k3xPyFc4	se
experimentuje	experimentovat	k5eAaImIp3nS	experimentovat
s	s	k7c7	s
farmovým	farmův	k2eAgInSc7d1	farmův
chovem	chov	k1gInSc7	chov
kapybar	kapybara	k1gFnPc2	kapybara
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
<s>
Maso	maso	k1gNnSc1	maso
kapybar	kapybara	k1gFnPc2	kapybara
je	být	k5eAaImIp3nS	být
chutné	chutný	k2eAgNnSc1d1	chutné
a	a	k8xC	a
bohaté	bohatý	k2eAgNnSc1d1	bohaté
na	na	k7c4	na
proteiny	protein	k1gInPc4	protein
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jiźní	Jiźní	k2eAgFnSc6d1	Jiźní
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
pokládáno	pokládat	k5eAaImNgNnS	pokládat
za	za	k7c4	za
postní	postní	k2eAgInSc4d1	postní
pokrm	pokrm	k1gInSc4	pokrm
<g/>
.	.	kIx.	.
</s>
<s>
Spásáním	spásání	k1gNnSc7	spásání
vegetace	vegetace	k1gFnSc2	vegetace
se	se	k3xPyFc4	se
kapybary	kapybara	k1gFnPc1	kapybara
staly	stát	k5eAaPmAgFnP	stát
vážnými	vážný	k2eAgMnPc7d1	vážný
konkurenty	konkurent	k1gMnPc7	konkurent
dobytka	dobytek	k1gInSc2	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
se	se	k3xPyFc4	se
také	také	k9	také
pro	pro	k7c4	pro
chutné	chutný	k2eAgNnSc4d1	chutné
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
kvalitní	kvalitní	k2eAgFnSc4d1	kvalitní
kůži	kůže	k1gFnSc4	kůže
s	s	k7c7	s
mimořádně	mimořádně	k6eAd1	mimořádně
hebkou	hebký	k2eAgFnSc7d1	hebká
srstí	srst	k1gFnSc7	srst
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
rančeři	rančer	k1gMnPc1	rančer
již	již	k6eAd1	již
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
podložky	podložka	k1gFnPc4	podložka
pod	pod	k7c4	pod
koňská	koňský	k2eAgNnPc4d1	koňské
sedla	sedlo	k1gNnPc4	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zubů	zub	k1gInPc2	zub
si	se	k3xPyFc3	se
domorodí	domorodý	k2eAgMnPc1d1	domorodý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
vyrábí	vyrábět	k5eAaImIp3nP	vyrábět
ozdobné	ozdobný	k2eAgInPc4d1	ozdobný
předměty	předmět	k1gInPc4	předmět
a	a	k8xC	a
příslušníci	příslušník	k1gMnPc1	příslušník
kmene	kmen	k1gInSc2	kmen
Bororo	Borora	k1gFnSc5	Borora
dokonce	dokonce	k9	dokonce
nože	nůž	k1gInSc2	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Indiáni	Indián	k1gMnPc1	Indián
kmene	kmen	k1gInSc2	kmen
Mundurukú	Mundurukú	k1gFnSc2	Mundurukú
chovají	chovat	k5eAaImIp3nP	chovat
kapybary	kapybara	k1gFnPc1	kapybara
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
osadách	osada	k1gFnPc6	osada
<g/>
.	.	kIx.	.
</s>
<s>
Počty	počet	k1gInPc1	počet
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
<g/>
:	:	kIx,	:
vinou	vinou	k7c2	vinou
intenzivního	intenzivní	k2eAgInSc2d1	intenzivní
lovu	lov	k1gInSc2	lov
stavy	stav	k1gInPc1	stav
klesají	klesat	k5eAaImIp3nP	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Počty	počet	k1gInPc1	počet
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
péči	péče	k1gFnSc6	péče
<g/>
:	:	kIx,	:
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
počty	počet	k1gInPc4	počet
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
stoupají	stoupat	k5eAaImIp3nP	stoupat
<g/>
,	,	kIx,	,
především	především	k9	především
díky	díky	k7c3	díky
úspěšným	úspěšný	k2eAgInPc3d1	úspěšný
odchovům	odchov	k1gInPc3	odchov
<g/>
.	.	kIx.	.
</s>
<s>
Chycená	chycený	k2eAgNnPc4d1	chycené
mláďata	mládě	k1gNnPc4	mládě
kapybar	kapybara	k1gFnPc2	kapybara
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
snadno	snadno	k6eAd1	snadno
ochočit	ochočit	k5eAaPmF	ochočit
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
přítulná	přítulný	k2eAgNnPc1d1	přítulné
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
venezuelských	venezuelský	k2eAgFnPc6d1	venezuelská
i	i	k8xC	i
brazilských	brazilský	k2eAgFnPc6d1	brazilská
vesnicích	vesnice	k1gFnPc6	vesnice
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
kapybary	kapybara	k1gFnPc1	kapybara
chovány	chován	k2eAgFnPc1d1	chována
zejména	zejména	k9	zejména
indiány	indián	k1gMnPc4	indián
jako	jako	k8xC	jako
domácí	domácí	k2eAgMnPc1d1	domácí
mazlíčci	mazlíček	k1gMnPc1	mazlíček
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
naučit	naučit	k5eAaPmF	naučit
spoustu	spousta	k1gFnSc4	spousta
povelů	povel	k1gInPc2	povel
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
pes	pes	k1gMnSc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
zatím	zatím	k6eAd1	zatím
oficiálně	oficiálně	k6eAd1	oficiálně
žádná	žádný	k3yNgFnSc1	žádný
chovná	chovný	k2eAgFnSc1d1	chovná
stanice	stanice	k1gFnSc1	stanice
kapybar	kapybara	k1gFnPc2	kapybara
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
chovány	chován	k2eAgFnPc1d1	chována
v	v	k7c6	v
několika	několik	k4yIc6	několik
českých	český	k2eAgFnPc6d1	Česká
ZOO	zoo	k1gFnPc6	zoo
(	(	kIx(	(
<g/>
např	např	kA	např
ZOO	zoo	k1gNnSc1	zoo
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
ZOO	zoo	k1gFnSc1	zoo
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
ZOO	zoo	k1gFnSc1	zoo
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
ZOO	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
ZOO	zoo	k1gFnSc1	zoo
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
ZOO	zoo	k1gFnSc1	zoo
Tábor	Tábor	k1gInSc1	Tábor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapybary	kapybara	k1gFnPc1	kapybara
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
v	v	k7c6	v
Llanos	Llanos	k1gInPc6	Llanos
ve	v	k7c6	v
Venezuele	Venezuela	k1gFnSc6	Venezuela
chovány	chovat	k5eAaImNgInP	chovat
na	na	k7c6	na
farmách	farma	k1gFnPc6	farma
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
postní	postní	k2eAgInSc4d1	postní
pokrm	pokrm	k1gInSc4	pokrm
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kapybara	kapybara	k1gFnSc1	kapybara
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Taxon	taxon	k1gInSc1	taxon
Hydrochoerus	Hydrochoerus	k1gMnSc1	Hydrochoerus
hydrochaeris	hydrochaeris	k1gInSc1	hydrochaeris
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
Galerie	galerie	k1gFnSc1	galerie
Kapybara	kapybara	k1gFnSc1	kapybara
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Kapybara	kapybara	k1gFnSc1	kapybara
v	v	k7c6	v
lexikonu	lexikon	k1gInSc6	lexikon
zvířat	zvíře	k1gNnPc2	zvíře
ZOO	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
Kapybara	kapybara	k1gFnSc1	kapybara
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Lešná	Lešná	k1gFnSc1	Lešná
(	(	kIx(	(
<g/>
Zlín	Zlín	k1gInSc1	Zlín
<g/>
)	)	kIx)	)
</s>
