<s>
Josef	Josef	k1gMnSc1	Josef
Rosipal	Rosipal	k1gMnSc1	Rosipal
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
Žižkov	Žižkov	k1gInSc1	Žižkov
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
Hermanovo	Hermanův	k2eAgNnSc4d1	Hermanovo
<g/>
,	,	kIx,	,
Halič	halič	k1gMnSc1	halič
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
designér	designér	k1gMnSc1	designér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
architekturu	architektura	k1gFnSc4	architektura
na	na	k7c6	na
Císařské	císařský	k2eAgFnSc6d1	císařská
a	a	k8xC	a
královské	královský	k2eAgFnSc6d1	královská
české	český	k2eAgFnSc6d1	Česká
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
technické	technický	k2eAgFnSc6d1	technická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
absolvování	absolvování	k1gNnSc6	absolvování
byl	být	k5eAaImAgInS	být
inženýrem	inženýr	k1gMnSc7	inženýr
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
dráhy	dráha	k1gFnSc2	dráha
v	v	k7c6	v
Ražicích	Ražice	k1gFnPc6	Ražice
u	u	k7c2	u
Písku	Písek	k1gInSc2	Písek
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
Magistrát	magistrát	k1gInSc4	magistrát
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Slibnou	slibný	k2eAgFnSc4d1	slibná
kariéru	kariéra	k1gFnSc4	kariéra
nadaného	nadaný	k2eAgMnSc2d1	nadaný
architekta	architekt	k1gMnSc2	architekt
však	však	k9	však
záhy	záhy	k6eAd1	záhy
ukončila	ukončit	k5eAaPmAgFnS	ukončit
I.	I.	kA	I.
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
musel	muset	k5eAaImAgMnS	muset
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
narukoval	narukovat	k5eAaPmAgMnS	narukovat
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
poručíka	poručík	k1gMnSc2	poručík
a	a	k8xC	a
padl	padnout	k5eAaImAgInS	padnout
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
samotném	samotný	k2eAgInSc6d1	samotný
počátku	počátek	k1gInSc6	počátek
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Halič	Halič	k1gFnSc4	Halič
<g/>
.	.	kIx.	.
</s>
<s>
Náhrobní	náhrobní	k2eAgFnSc1d1	náhrobní
deska	deska	k1gFnSc1	deska
je	být	k5eAaImIp3nS	být
vsazena	vsadit	k5eAaPmNgFnS	vsadit
do	do	k7c2	do
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
poškozeného	poškozený	k2eAgInSc2d1	poškozený
náhrobku	náhrobek	k1gInSc2	náhrobek
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Portrét	portrét	k1gInSc4	portrét
architekta	architekt	k1gMnSc2	architekt
pocházel	pocházet	k5eAaImAgMnS	pocházet
od	od	k7c2	od
sochaře	sochař	k1gMnSc2	sochař
Jana	Jan	k1gMnSc2	Jan
Štursy	Štursa	k1gFnSc2	Štursa
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
byl	být	k5eAaImAgInS	být
pozdějšími	pozdní	k2eAgInPc7d2	pozdější
amatérskými	amatérský	k2eAgInPc7d1	amatérský
pokusy	pokus	k1gInPc7	pokus
o	o	k7c6	o
restaurování	restaurování	k1gNnSc6	restaurování
zcela	zcela	k6eAd1	zcela
deformován	deformovat	k5eAaImNgInS	deformovat
a	a	k8xC	a
poničen	poničit	k5eAaPmNgInS	poničit
<g/>
.	.	kIx.	.
</s>
<s>
Bronzový	bronzový	k2eAgInSc1d1	bronzový
reliéf	reliéf	k1gInSc1	reliéf
od	od	k7c2	od
Josefa	Josef	k1gMnSc2	Josef
Jílka	Jílek	k1gMnSc2	Jílek
<g/>
,	,	kIx,	,
vsazený	vsazený	k2eAgInSc4d1	vsazený
do	do	k7c2	do
dodatečně	dodatečně	k6eAd1	dodatečně
postaveného	postavený	k2eAgInSc2d1	postavený
žulového	žulový	k2eAgInSc2d1	žulový
nástavce	nástavec	k1gInSc2	nástavec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
vylomen	vylomen	k2eAgMnSc1d1	vylomen
a	a	k8xC	a
odcizen	odcizen	k2eAgMnSc1d1	odcizen
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
nejznámější	známý	k2eAgFnSc7d3	nejznámější
stavbou	stavba	k1gFnSc7	stavba
je	být	k5eAaImIp3nS	být
Městský	městský	k2eAgInSc1d1	městský
sirotčinec	sirotčinec	k1gInSc1	sirotčinec
na	na	k7c6	na
Hradčanech	Hradčany	k1gInPc6	Hradčany
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kultury	kultura	k1gFnSc2	kultura
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
ulici	ulice	k1gFnSc6	ulice
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
a	a	k8xC	a
také	také	k9	také
portál	portál	k1gInSc1	portál
proslulé	proslulý	k2eAgFnSc2d1	proslulá
Myšákovy	myšákův	k2eAgFnSc2d1	Myšákova
cukrárny	cukrárna	k1gFnSc2	cukrárna
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
pražském	pražský	k2eAgNnSc6d1	Pražské
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
členem	člen	k1gInSc7	člen
družstva	družstvo	k1gNnSc2	družstvo
Artěl	artěl	k1gInSc1	artěl
<g/>
.	.	kIx.	.
</s>
<s>
Městský	městský	k2eAgInSc1d1	městský
sirotčinec	sirotčinec	k1gInSc1	sirotčinec
na	na	k7c6	na
Hradčanech	Hradčany	k1gInPc6	Hradčany
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
6	[number]	k4	6
<g/>
-Hradčany	-Hradčan	k1gMnPc7	-Hradčan
<g/>
,	,	kIx,	,
č.	č.	k?	č.
p.	p.	k?	p.
220	[number]	k4	220
<g/>
,	,	kIx,	,
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
139	[number]	k4	139
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
geometrické	geometrický	k2eAgFnSc2d1	geometrická
secese	secese	k1gFnSc2	secese
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgFnSc4	první
stavbu	stavba	k1gFnSc4	stavba
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
s	s	k7c7	s
plochou	plocha	k1gFnSc7	plocha
střechou	střecha	k1gFnSc7	střecha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kultury	kultura	k1gFnSc2	kultura
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Nástavba	nástavba	k1gFnSc1	nástavba
horních	horní	k2eAgNnPc2d1	horní
pater	patro	k1gNnPc2	patro
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Josefské	josefský	k2eAgFnSc6d1	Josefská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
<g/>
-Malá	-Malat	k5eAaPmIp3nS	-Malat
Strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
č.	č.	k?	č.
p.	p.	k?	p.
626	[number]	k4	626
Interiér	interiér	k1gInSc1	interiér
cukrárny	cukrárna	k1gFnSc2	cukrárna
U	u	k7c2	u
Myšáka	myšák	k1gMnSc2	myšák
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
<g/>
-Nové	-Nová	k1gFnSc6	-Nová
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
č.	č.	k?	č.
p.	p.	k?	p.
710	[number]	k4	710
<g/>
,	,	kIx,	,
Vodičkova	Vodičkův	k2eAgFnSc1d1	Vodičkova
31	[number]	k4	31
Jako	jako	k8xC	jako
člen	člen	k1gMnSc1	člen
družstva	družstvo	k1gNnSc2	družstvo
Artěl	artěl	k1gInSc1	artěl
navrhoval	navrhovat	k5eAaImAgInS	navrhovat
skleněné	skleněný	k2eAgInPc4d1	skleněný
soubory	soubor	k1gInPc4	soubor
a	a	k8xC	a
nábytek	nábytek	k1gInSc4	nábytek
<g/>
.	.	kIx.	.
</s>
<s>
Skleněné	skleněný	k2eAgInPc1d1	skleněný
soubory	soubor	k1gInPc1	soubor
(	(	kIx(	(
<g/>
nápojové	nápojový	k2eAgFnPc1d1	nápojová
sady	sada	k1gFnPc1	sada
<g/>
,	,	kIx,	,
vázy	váza	k1gFnPc1	váza
<g/>
,	,	kIx,	,
podnosy	podnos	k1gInPc1	podnos
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
Uměleckoprůmyslového	uměleckoprůmyslový	k2eAgNnSc2d1	Uměleckoprůmyslové
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
