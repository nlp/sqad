<s>
Utrpení	utrpení	k1gNnSc1	utrpení
knížete	kníže	k1gMnSc2	kníže
Sternenhocha	Sternenhoch	k1gMnSc2	Sternenhoch
je	být	k5eAaImIp3nS	být
expresionistický	expresionistický	k2eAgInSc4d1	expresionistický
román	román	k1gInSc4	román
českého	český	k2eAgMnSc2d1	český
spisovatele	spisovatel	k1gMnSc2	spisovatel
Ladislava	Ladislav	k1gMnSc2	Ladislav
Klímy	Klíma	k1gMnSc2	Klíma
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
měsíc	měsíc	k1gInSc4	měsíc
po	po	k7c6	po
autorově	autorův	k2eAgFnSc6d1	autorova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Klíma	Klíma	k1gMnSc1	Klíma
jej	on	k3xPp3gMnSc4	on
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
groteskní	groteskní	k2eAgNnSc4d1	groteskní
romaneto	romaneto	k1gNnSc4	romaneto
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Německém	německý	k2eAgNnSc6d1	německé
císařství	císařství	k1gNnSc6	císařství
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
období	období	k1gNnSc6	období
vlády	vláda	k1gFnSc2	vláda
kancléře	kancléř	k1gMnSc2	kancléř
Bismarcka	Bismarcko	k1gNnSc2	Bismarcko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
jasně	jasně	k6eAd1	jasně
patrná	patrný	k2eAgFnSc1d1	patrná
filosofie	filosofie	k1gFnSc1	filosofie
Friedricha	Friedrich	k1gMnSc2	Friedrich
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
a	a	k8xC	a
některých	některý	k3yIgMnPc2	některý
dalších	další	k2eAgMnPc2d1	další
soudobých	soudobý	k2eAgMnPc2d1	soudobý
filosofů	filosof	k1gMnPc2	filosof
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
zdegenerovaný	zdegenerovaný	k2eAgMnSc1d1	zdegenerovaný
a	a	k8xC	a
bohatý	bohatý	k2eAgMnSc1d1	bohatý
šlechtic	šlechtic	k1gMnSc1	šlechtic
Sternenhoch	Sternenhoch	k1gMnSc1	Sternenhoch
(	(	kIx(	(
<g/>
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
za	za	k7c2	za
krásného	krásný	k2eAgNnSc2d1	krásné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc4d1	malý
<g/>
,	,	kIx,	,
bezzubý	bezzubý	k2eAgInSc4d1	bezzubý
<g/>
,	,	kIx,	,
bezvlasý	bezvlasý	k2eAgInSc4d1	bezvlasý
<g/>
,	,	kIx,	,
šilhavý	šilhavý	k2eAgInSc4d1	šilhavý
a	a	k8xC	a
bezmezně	bezmezně	k6eAd1	bezmezně
ošklivý	ošklivý	k2eAgInSc1d1	ošklivý
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
styky	styk	k1gInPc4	styk
i	i	k8xC	i
s	s	k7c7	s
německým	německý	k2eAgMnSc7d1	německý
císařem	císař	k1gMnSc7	císař
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
na	na	k7c6	na
jakémsi	jakýsi	k3yIgInSc6	jakýsi
plese	ples	k1gInSc6	ples
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
mrtvolně	mrtvolně	k6eAd1	mrtvolně
bledá	bledý	k2eAgFnSc1d1	bledá
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
ošklivá	ošklivý	k2eAgFnSc1d1	ošklivá
dívka	dívka	k1gFnSc1	dívka
Helga	Helga	k1gFnSc1	Helga
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
si	se	k3xPyFc3	se
přes	přes	k7c4	přes
obrovský	obrovský	k2eAgInSc4d1	obrovský
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
(	(	kIx(	(
<g/>
i	i	k9	i
varování	varování	k1gNnSc1	varování
budoucího	budoucí	k2eAgMnSc2d1	budoucí
tchána	tchán	k1gMnSc2	tchán
<g/>
)	)	kIx)	)
bere	brát	k5eAaImIp3nS	brát
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Helga	Helga	k1gFnSc1	Helga
však	však	k9	však
během	během	k7c2	během
těhotenství	těhotenství	k1gNnSc2	těhotenství
zkrásní	zkrásnit	k5eAaPmIp3nS	zkrásnit
<g/>
,	,	kIx,	,
také	také	k9	také
její	její	k3xOp3gFnSc1	její
povaha	povaha	k1gFnSc1	povaha
se	se	k3xPyFc4	se
však	však	k9	však
výrazně	výrazně	k6eAd1	výrazně
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
nesnášenlivá	snášenlivý	k2eNgFnSc1d1	nesnášenlivá
zrůda	zrůda	k1gFnSc1	zrůda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
svého	svůj	k3xOyFgMnSc4	svůj
manžela	manžel	k1gMnSc4	manžel
nenávidí	návidět	k5eNaImIp3nS	návidět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
narození	narození	k1gNnSc6	narození
syna	syn	k1gMnSc2	syn
je	být	k5eAaImIp3nS	být
kníže	kníže	k1gMnSc1	kníže
velice	velice	k6eAd1	velice
šťastný	šťastný	k2eAgMnSc1d1	šťastný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Helga	Helga	k1gFnSc1	Helga
jej	on	k3xPp3gMnSc4	on
zavraždí	zavraždit	k5eAaPmIp3nS	zavraždit
(	(	kIx(	(
<g/>
vinu	vina	k1gFnSc4	vina
svede	svést	k5eAaPmIp3nS	svést
na	na	k7c4	na
chůvu	chůva	k1gFnSc4	chůva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
cítí	cítit	k5eAaImIp3nS	cítit
stejný	stejný	k2eAgInSc1d1	stejný
odpor	odpor	k1gInSc1	odpor
jako	jako	k8xS	jako
ke	k	k7c3	k
Sternhochovi	Sternhoch	k1gMnSc3	Sternhoch
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
začíná	začínat	k5eAaImIp3nS	začínat
uvědomovat	uvědomovat	k5eAaImF	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
nevzal	vzít	k5eNaPmAgMnS	vzít
Helgu	Helga	k1gFnSc4	Helga
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
Démonu	démon	k1gMnSc3	démon
<g/>
.	.	kIx.	.
</s>
<s>
Helga	Helga	k1gFnSc1	Helga
dále	daleko	k6eAd2	daleko
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
často	často	k6eAd1	často
bil	bít	k5eAaImAgMnS	bít
a	a	k8xC	a
jí	jíst	k5eAaImIp3nS	jíst
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
téměř	téměř	k6eAd1	téměř
ani	ani	k8xC	ani
nepřišlo	přijít	k5eNaPmAgNnS	přijít
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
(	(	kIx(	(
<g/>
nechtěl	chtít	k5eNaImAgMnS	chtít
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
tichá	tichý	k2eAgFnSc1d1	tichá
a	a	k8xC	a
nečinná	činný	k2eNgFnSc1d1	nečinná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
její	její	k3xOp3gNnSc4	její
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
chování	chování	k1gNnSc4	chování
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
však	však	k9	však
kníže	kníže	k1gMnSc1	kníže
nepřestává	přestávat	k5eNaImIp3nS	přestávat
toužit	toužit	k5eAaImF	toužit
a	a	k8xC	a
poté	poté	k6eAd1	poté
co	co	k3yRnSc4	co
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
milence	milenec	k1gMnSc4	milenec
<g/>
,	,	kIx,	,
probouzí	probouzet	k5eAaImIp3nS	probouzet
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
obrovská	obrovský	k2eAgFnSc1d1	obrovská
vlna	vlna	k1gFnSc1	vlna
žárlivosti	žárlivost	k1gFnSc2	žárlivost
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
s	s	k7c7	s
jejím	její	k3xOp3gMnSc7	její
milencem	milenec	k1gMnSc7	milenec
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
,	,	kIx,	,
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
provozují	provozovat	k5eAaImIp3nP	provozovat
masochistické	masochistický	k2eAgFnPc1d1	masochistická
hrátky	hrátky	k1gFnPc1	hrátky
a	a	k8xC	a
že	že	k8xS	že
její	její	k3xOp3gMnSc1	její
milenec	milenec	k1gMnSc1	milenec
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
chová	chovat	k5eAaImIp3nS	chovat
povýšeně	povýšeně	k6eAd1	povýšeně
-	-	kIx~	-
nadává	nadávat	k5eAaImIp3nS	nadávat
jí	on	k3xPp3gFnSc7	on
a	a	k8xC	a
bije	bít	k5eAaImIp3nS	bít
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
Helga	Helga	k1gFnSc1	Helga
plánuje	plánovat	k5eAaImIp3nS	plánovat
útěk	útěk	k1gInSc4	útěk
a	a	k8xC	a
o	o	k7c6	o
svém	svůj	k3xOyFgMnSc6	svůj
manželovi	manžel	k1gMnSc6	manžel
mluví	mluvit	k5eAaImIp3nS	mluvit
jako	jako	k9	jako
o	o	k7c6	o
Hnusu	hnus	k1gInSc6	hnus
<g/>
.	.	kIx.	.
</s>
<s>
Sternenchoch	Sternenchoch	k1gMnSc1	Sternenchoch
se	se	k3xPyFc4	se
jejímu	její	k3xOp3gInSc3	její
odjezdu	odjezd	k1gInSc3	odjezd
se	se	k3xPyFc4	se
záhadným	záhadný	k2eAgMnSc7d1	záhadný
mužem	muž	k1gMnSc7	muž
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
zabránit	zabránit	k5eAaPmF	zabránit
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
útěku	útěk	k1gInSc2	útěk
tak	tak	k8xS	tak
Helgu	Helg	k1gInSc2	Helg
udeří	udeřit	k5eAaPmIp3nS	udeřit
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
vhodí	vhodit	k5eAaPmIp3nP	vhodit
do	do	k7c2	do
hladomorny	hladomorna	k1gFnSc2	hladomorna
(	(	kIx(	(
<g/>
zabije	zabít	k5eAaPmIp3nS	zabít
i	i	k9	i
jejího	její	k3xOp3gMnSc4	její
milence	milenec	k1gMnSc4	milenec
a	a	k8xC	a
své	svůj	k3xOyFgMnPc4	svůj
milované	milovaný	k2eAgMnPc4d1	milovaný
psy	pes	k1gMnPc4	pes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
ji	on	k3xPp3gFnSc4	on
surově	surově	k6eAd1	surově
zbije	zbít	k5eAaPmIp3nS	zbít
<g/>
,	,	kIx,	,
poníží	ponížit	k5eAaPmIp3nS	ponížit
a	a	k8xC	a
nechá	nechat	k5eAaPmIp3nS	nechat
zemřít	zemřít	k5eAaPmF	zemřít
hlady	hlad	k1gInPc7	hlad
a	a	k8xC	a
žízní	žízeň	k1gFnSc7	žízeň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ho	on	k3xPp3gInSc4	on
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
její	její	k3xOp3gNnSc1	její
vidina	vidina	k1gFnSc1	vidina
a	a	k8xC	a
nejistota	nejistota	k1gFnSc1	nejistota
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
halucinace	halucinace	k1gFnPc4	halucinace
či	či	k8xC	či
skutečně	skutečně	k6eAd1	skutečně
jeho	jeho	k3xOp3gFnSc4	jeho
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
Helga	Helga	k1gFnSc1	Helga
nemohla	moct	k5eNaImAgFnS	moct
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
zachránit	zachránit	k5eAaPmF	zachránit
a	a	k8xC	a
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nemstí	mstít	k5eNaImIp3nP	mstít
(	(	kIx(	(
<g/>
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
léčit	léčit	k5eAaImF	léčit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
každou	každý	k3xTgFnSc4	každý
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vidí	vidět	k5eAaImIp3nP	vidět
<g/>
,	,	kIx,	,
křičí	křičet	k5eAaImIp3nP	křičet
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Strašidlo	strašidlo	k1gNnSc1	strašidlo
<g/>
,	,	kIx,	,
do	do	k7c2	do
prdele	prdel	k1gFnSc2	prdel
mi	já	k3xPp1nSc3	já
vskoč	vskočit	k5eAaPmRp2nS	vskočit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostavují	dostavovat	k5eAaImIp3nP	dostavovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
výčitky	výčitka	k1gFnPc1	výčitka
svědomí	svědomí	k1gNnSc2	svědomí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Helgy-přeludu	Helgyřelud	k1gInSc2	Helgy-přelud
se	se	k3xPyFc4	se
dovídá	dovídat	k5eAaImIp3nS	dovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
pekla	peklo	k1gNnSc2	peklo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
muka	muka	k1gFnSc1	muka
mění	měnit	k5eAaImIp3nP	měnit
v	v	k7c4	v
rozkoš	rozkoš	k1gFnSc4	rozkoš
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
všeidioctvím	všeidioctví	k1gNnPc3	všeidioctví
<g/>
,	,	kIx,	,
cítí	cítit	k5eAaImIp3nS	cítit
ke	k	k7c3	k
všemu	všecek	k3xTgNnSc3	všecek
jen	jen	k9	jen
nenávist	nenávist	k1gFnSc4	nenávist
a	a	k8xC	a
hnus	hnus	k1gInSc4	hnus
<g/>
.	.	kIx.	.
</s>
<s>
Stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
Bohyně	bohyně	k1gFnSc2	bohyně
věčnosti	věčnost	k1gFnSc2	věčnost
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
peklo	peklo	k1gNnSc4	peklo
zničit	zničit	k5eAaPmF	zničit
svou	svůj	k3xOyFgFnSc7	svůj
vůlí	vůle	k1gFnSc7	vůle
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
však	však	k9	však
nedaří	dařit	k5eNaImIp3nS	dařit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nikdy	nikdy	k6eAd1	nikdy
nepoznala	poznat	k5eNaPmAgFnS	poznat
hlavní	hlavní	k2eAgNnSc4d1	hlavní
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
<g/>
.	.	kIx.	.
</s>
<s>
Helga-přelud	Helgařelud	k6eAd1	Helga-přelud
hledá	hledat	k5eAaImIp3nS	hledat
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
ji	on	k3xPp3gFnSc4	on
vhodil	vhodit	k5eAaPmAgMnS	vhodit
do	do	k7c2	do
hladomorny	hladomorna	k1gFnSc2	hladomorna
<g/>
.	.	kIx.	.
</s>
<s>
Kníže	kníže	k1gMnSc1	kníže
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
přiznat	přiznat	k5eAaPmF	přiznat
a	a	k8xC	a
dlouho	dlouho	k6eAd1	dlouho
svůj	svůj	k3xOyFgInSc4	svůj
čin	čin	k1gInSc4	čin
zapírá	zapírat	k5eAaImIp3nS	zapírat
<g/>
,	,	kIx,	,
trápení	trápení	k1gNnSc1	trápení
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
ukončit	ukončit	k5eAaPmF	ukončit
až	až	k9	až
v	v	k7c4	v
den	den	k1gInSc4	den
odjezdu	odjezd	k1gInSc2	odjezd
do	do	k7c2	do
léčebny	léčebna	k1gFnSc2	léčebna
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
duševně	duševně	k6eAd1	duševně
choré	chorý	k2eAgInPc4d1	chorý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
hladomorny	hladomorna	k1gFnSc2	hladomorna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
mrtvolou	mrtvola	k1gFnSc7	mrtvola
Helgy	Helga	k1gFnSc2	Helga
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
chce	chtít	k5eAaImIp3nS	chtít
odpustit	odpustit	k5eAaPmF	odpustit
a	a	k8xC	a
začít	začít	k5eAaPmF	začít
jej	on	k3xPp3gMnSc4	on
milovat	milovat	k5eAaImF	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Kníže	kníže	k1gMnSc1	kníže
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
a	a	k8xC	a
ráno	ráno	k6eAd1	ráno
jej	on	k3xPp3gMnSc4	on
najdou	najít	k5eAaPmIp3nP	najít
šíleného	šílený	k2eAgMnSc4d1	šílený
<g/>
,	,	kIx,	,
kterak	kterak	k8xS	kterak
souloží	souložit	k5eAaImIp3nS	souložit
se	s	k7c7	s
zahnívající	zahnívající	k2eAgFnSc7d1	zahnívající
kostrou	kostra	k1gFnSc7	kostra
<g/>
.	.	kIx.	.
</s>
<s>
Sterenenhoch	Sterenenhoch	k1gInSc1	Sterenenhoch
umírá	umírat	k5eAaImIp3nS	umírat
pouhé	pouhý	k2eAgInPc4d1	pouhý
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
nalezení	nalezení	k1gNnSc6	nalezení
na	na	k7c4	na
otravu	otrava	k1gFnSc4	otrava
krve	krev	k1gFnSc2	krev
od	od	k7c2	od
kosti	kost	k1gFnSc2	kost
mrtvé	mrtvý	k2eAgFnSc2d1	mrtvá
Helgy	Helga	k1gFnSc2	Helga
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
úvodní	úvodní	k2eAgFnSc4d1	úvodní
část	část	k1gFnSc4	část
vyprávěnou	vyprávěný	k2eAgFnSc4d1	vyprávěná
Sternenhochem	Sternenhoch	k1gInSc7	Sternenhoch
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
část	část	k1gFnSc4	část
-	-	kIx~	-
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
dny	den	k1gInPc4	den
(	(	kIx(	(
<g/>
psanou	psaný	k2eAgFnSc7d1	psaná
formou	forma	k1gFnSc7	forma
Sternenhochových	Sternenhochův	k2eAgInPc2d1	Sternenhochův
deníkových	deníkový	k2eAgInPc2d1	deníkový
zápisů	zápis	k1gInPc2	zápis
<g/>
)	)	kIx)	)
a	a	k8xC	a
závěrečný	závěrečný	k2eAgInSc1d1	závěrečný
autorem	autor	k1gMnSc7	autor
psaný	psaný	k2eAgInSc4d1	psaný
epilog	epilog	k1gInSc4	epilog
<g/>
.	.	kIx.	.
</s>
<s>
Klíma	Klíma	k1gMnSc1	Klíma
se	se	k3xPyFc4	se
v	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
snaží	snažit	k5eAaImIp3nS	snažit
čtenáře	čtenář	k1gMnPc4	čtenář
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
odehrál	odehrát	k5eAaPmAgInS	odehrát
-	-	kIx~	-
redakce	redakce	k1gFnSc1	redakce
měla	mít	k5eAaImAgFnS	mít
získat	získat	k5eAaPmF	získat
Sternenhochův	Sternenhochův	k2eAgInSc4d1	Sternenhochův
deník	deník	k1gInSc4	deník
a	a	k8xC	a
závěr	závěr	k1gInSc4	závěr
měla	mít	k5eAaImAgFnS	mít
dopsat	dopsat	k5eAaPmF	dopsat
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Sternenhoch	Sternenhoch	k1gMnSc1	Sternenhoch
v	v	k7c6	v
deníkových	deníkový	k2eAgInPc6d1	deníkový
zápisech	zápis	k1gInPc6	zápis
předvídá	předvídat	k5eAaImIp3nS	předvídat
svou	svůj	k3xOyFgFnSc4	svůj
budoucnost	budoucnost	k1gFnSc4	budoucnost
a	a	k8xC	a
píše	psát	k5eAaImIp3nS	psát
retrospektivně	retrospektivně	k6eAd1	retrospektivně
(	(	kIx(	(
<g/>
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
halucinacích	halucinace	k1gFnPc6	halucinace
a	a	k8xC	a
až	až	k9	až
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
zavraždění	zavraždění	k1gNnSc3	zavraždění
Helgy	Helga	k1gFnSc2	Helga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
používá	používat	k5eAaImIp3nS	používat
spisovný	spisovný	k2eAgInSc1d1	spisovný
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uchyluje	uchylovat	k5eAaImIp3nS	uchylovat
se	se	k3xPyFc4	se
i	i	k9	i
k	k	k7c3	k
hovorovým	hovorový	k2eAgInPc3d1	hovorový
výrazům	výraz	k1gInPc3	výraz
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
slova	slovo	k1gNnPc1	slovo
vyloženě	vyloženě	k6eAd1	vyloženě
zhrubělá	zhrubělý	k2eAgNnPc1d1	zhrubělé
a	a	k8xC	a
sprostá	sprostý	k2eAgNnPc1d1	sprosté
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
natočil	natočit	k5eAaBmAgMnS	natočit
na	na	k7c4	na
námět	námět	k1gInSc4	námět
této	tento	k3xDgFnSc2	tento
knihy	kniha	k1gFnSc2	kniha
režisér	režisér	k1gMnSc1	režisér
Jan	Jan	k1gMnSc1	Jan
Němec	Němec	k1gMnSc1	Němec
film	film	k1gInSc4	film
V	v	k7c6	v
žáru	žár	k1gInSc6	žár
královské	královský	k2eAgFnSc2d1	královská
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
