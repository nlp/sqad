<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Ghany	Ghana	k1gFnSc2	Ghana
je	být	k5eAaImIp3nS	být
list	list	k1gInSc1	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
vodorovných	vodorovný	k2eAgInPc2d1	vodorovný
pruhů	pruh	k1gInPc2	pruh
<g/>
:	:	kIx,	:
červeného	červený	k2eAgMnSc2d1	červený
<g/>
,	,	kIx,	,
zlatého	zlatý	k2eAgInSc2d1	zlatý
a	a	k8xC	a
zeleného	zelený	k2eAgInSc2d1	zelený
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
zlatého	zlatý	k2eAgInSc2d1	zlatý
pruhu	pruh	k1gInSc2	pruh
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
pěticípá	pěticípý	k2eAgFnSc1d1	pěticípá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
dotýkající	dotýkající	k2eAgInPc1d1	dotýkající
se	se	k3xPyFc4	se
cípy	cíp	k1gInPc7	cíp
krajních	krajní	k2eAgInPc2d1	krajní
pruhů	pruh	k1gInPc2	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgInSc1d1	červený
pruh	pruh	k1gInSc1	pruh
značí	značit	k5eAaImIp3nS	značit
krev	krev	k1gFnSc1	krev
bojovníků	bojovník	k1gMnPc2	bojovník
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
zlatý	zlatý	k2eAgInSc4d1	zlatý
pruh	pruh	k1gInSc4	pruh
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
nerostné	nerostný	k2eAgNnSc4d1	nerostné
bohatství	bohatství	k1gNnSc4	bohatství
země	zem	k1gFnSc2	zem
a	a	k8xC	a
zelený	zelený	k2eAgInSc4d1	zelený
přírodní	přírodní	k2eAgFnSc4d1	přírodní
krásu	krása	k1gFnSc4	krása
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
africkou	africký	k2eAgFnSc4d1	africká
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
připomíná	připomínat	k5eAaImIp3nS	připomínat
etiopskou	etiopský	k2eAgFnSc4d1	etiopská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
barvy	barva	k1gFnPc1	barva
pruhů	pruh	k1gInPc2	pruh
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
v	v	k7c6	v
obráceném	obrácený	k2eAgInSc6d1	obrácený
sledu	sled	k1gInSc6	sled
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
tzv.	tzv.	kA	tzv.
panafrické	panafrický	k2eAgFnPc4d1	panafrická
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
etiopské	etiopský	k2eAgNnSc4d1	etiopské
je	být	k5eAaImIp3nS	být
ghanská	ghanský	k2eAgFnSc1d1	ghanská
vlajka	vlajka	k1gFnSc1	vlajka
druhou	druhý	k4xOgFnSc7	druhý
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
barvami	barva	k1gFnPc7	barva
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c4	za
tvůrce	tvůrce	k1gMnPc4	tvůrce
ghanské	ghanský	k2eAgFnSc2d1	ghanská
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
paní	paní	k1gFnSc1	paní
Theodosia	Theodosium	k1gNnSc2	Theodosium
Salome	Salom	k1gInSc5	Salom
Okohová	Okohový	k2eAgNnPc4d1	Okohový
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
užívána	užívat	k5eAaImNgFnS	užívat
od	od	k7c2	od
získání	získání	k1gNnSc2	získání
samostatnosti	samostatnost	k1gFnSc2	samostatnost
na	na	k7c6	na
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
nakrátko	nakrátko	k6eAd1	nakrátko
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
užívána	užívat	k5eAaImNgFnS	užívat
od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ghanská	ghanský	k2eAgFnSc1d1	ghanská
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
~	~	kIx~	~
<g/>
300	[number]	k4	300
<g/>
–	–	k?	–
<g/>
1235	[number]	k4	1235
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Mali	Mali	k1gNnSc2	Mali
a	a	k8xC	a
Mauritánie	Mauritánie	k1gFnSc2	Mauritánie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Ghany	Ghana	k1gFnSc2	Ghana
ale	ale	k8xC	ale
již	již	k6eAd1	již
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
existovala	existovat	k5eAaImAgFnS	existovat
důležitá	důležitý	k2eAgFnSc1d1	důležitá
království	království	k1gNnSc6	království
Dogomba	Dogomb	k1gMnSc4	Dogomb
a	a	k8xC	a
Mamprusi	Mampruse	k1gFnSc4	Mampruse
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jih	jih	k1gInSc4	jih
pak	pak	k6eAd1	pak
migrovali	migrovat	k5eAaImAgMnP	migrovat
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Akanové	Akanová	k1gFnSc2	Akanová
(	(	kIx(	(
<g/>
především	především	k9	především
Ašantové	Ašantové	k?	Ašantové
a	a	k8xC	a
Fantové	Fantová	k1gFnSc2	Fantová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
několik	několik	k4yIc4	několik
malých	malý	k2eAgInPc2d1	malý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1470	[number]	k4	1470
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vylodili	vylodit	k5eAaPmAgMnP	vylodit
a	a	k8xC	a
usídlili	usídlit	k5eAaPmAgMnP	usídlit
Portugalci	Portugalec	k1gMnPc1	Portugalec
a	a	k8xC	a
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
ji	on	k3xPp3gFnSc4	on
Zlatonosné	zlatonosný	k2eAgNnSc1d1	zlatonosné
pobřeží	pobřeží	k1gNnSc1	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pronikaly	pronikat	k5eAaImAgFnP	pronikat
do	do	k7c2	do
země	zem	k1gFnSc2	zem
i	i	k8xC	i
další	další	k2eAgInPc4d1	další
evropské	evropský	k2eAgInPc4d1	evropský
národy	národ	k1gInPc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
existovala	existovat	k5eAaImAgFnS	existovat
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Ašantská	Ašantský	k2eAgFnSc1d1	Ašantská
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
britsko-ašantských	britskošantský	k2eAgFnPc6d1	britsko-ašantský
válkách	válka	k1gFnPc6	válka
bylo	být	k5eAaImAgNnS	být
celé	celý	k2eAgNnSc1d1	celé
území	území	k1gNnSc1	území
postupně	postupně	k6eAd1	postupně
připojováno	připojovat	k5eAaImNgNnS	připojovat
ke	k	k7c3	k
Spojenému	spojený	k2eAgNnSc3d1	spojené
království	království	k1gNnSc3	království
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
se	se	k3xPyFc4	se
vyvěšovaly	vyvěšovat	k5eAaImAgFnP	vyvěšovat
vlajky	vlajka	k1gFnPc1	vlajka
britské	britský	k2eAgFnPc1d1	britská
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
bylo	být	k5eAaImAgNnS	být
Britské	britský	k2eAgNnSc1d1	Britské
Zlatonosné	zlatonosný	k2eAgNnSc1d1	zlatonosné
pobřeží	pobřeží	k1gNnSc1	pobřeží
začleněno	začleněn	k2eAgNnSc1d1	začleněno
do	do	k7c2	do
území	území	k1gNnSc2	území
tzv.	tzv.	kA	tzv.
Britských	britský	k2eAgFnPc2d1	britská
západoafrických	západoafrický	k2eAgFnPc2d1	západoafrická
osad	osada	k1gFnPc2	osada
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
územní	územní	k2eAgInSc1d1	územní
celek	celek	k1gInSc1	celek
užíval	užívat	k5eAaImAgInS	užívat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
vlajku	vlajka	k1gFnSc4	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
odvozenou	odvozený	k2eAgFnSc4d1	odvozená
z	z	k7c2	z
britská	britský	k2eAgFnSc1d1	britská
státní	státní	k2eAgFnSc2d1	státní
námořní	námořní	k2eAgInSc4d1	námořní
(	(	kIx(	(
<g/>
služební	služební	k2eAgInSc4d1	služební
<g/>
)	)	kIx)	)
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
měla	mít	k5eAaImAgFnS	mít
ve	v	k7c6	v
vlající	vlající	k2eAgFnSc6d1	vlající
části	část	k1gFnSc6	část
místní	místní	k2eAgInSc1d1	místní
vlajkový	vlajkový	k2eAgInSc1d1	vlajkový
emblém	emblém	k1gInSc1	emblém
(	(	kIx(	(
<g/>
badge	badge	k1gInSc1	badge
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvořený	tvořený	k2eAgInSc1d1	tvořený
kruhovým	kruhový	k2eAgNnSc7d1	kruhové
polem	pole	k1gNnSc7	pole
převážně	převážně	k6eAd1	převážně
žluté	žlutý	k2eAgFnSc2d1	žlutá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
přirozených	přirozený	k2eAgFnPc6d1	přirozená
barvách	barva	k1gFnPc6	barva
vyobrazen	vyobrazen	k2eAgMnSc1d1	vyobrazen
slon	slon	k1gMnSc1	slon
se	s	k7c7	s
zdviženým	zdvižený	k2eAgInSc7d1	zdvižený
chobotem	chobot	k1gInSc7	chobot
<g/>
,	,	kIx,	,
palma	palma	k1gFnSc1	palma
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
trsy	trs	k1gInPc4	trs
trávy	tráva	k1gFnSc2	tráva
<g/>
,	,	kIx,	,
kopcovitá	kopcovitý	k2eAgFnSc1d1	kopcovitá
krajina	krajina	k1gFnSc1	krajina
a	a	k8xC	a
západ	západ	k1gInSc1	západ
slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
slonem	slon	k1gMnSc7	slon
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
oblouku	oblouk	k1gInSc2	oblouk
(	(	kIx(	(
<g/>
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
řádcích	řádek	k1gInPc6	řádek
<g/>
)	)	kIx)	)
nápis	nápis	k1gInSc1	nápis
WEST	WEST	kA	WEST
AFRICA	AFRICA	kA	AFRICA
a	a	k8xC	a
SETTLEMENTS	SETTLEMENTS	kA	SETTLEMENTS
<g/>
.	.	kIx.	.
</s>
<s>
Sloni	slon	k1gMnPc1	slon
z	z	k7c2	z
volné	volný	k2eAgFnSc2d1	volná
přírody	příroda	k1gFnSc2	příroda
v	v	k7c6	v
Ghaně	Ghana	k1gFnSc6	Ghana
však	však	k9	však
již	již	k6eAd1	již
vymizeli	vymizet	k5eAaPmAgMnP	vymizet
<g/>
.24	.24	k4	.24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1874	[number]	k4	1874
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
britská	britský	k2eAgFnSc1d1	britská
kolonie	kolonie	k1gFnSc1	kolonie
Zlatonosné	zlatonosný	k2eAgNnSc1d1	zlatonosné
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Užívala	užívat	k5eAaImAgFnS	užívat
téměř	téměř	k6eAd1	téměř
totožnou	totožný	k2eAgFnSc4d1	totožná
vlajku	vlajka	k1gFnSc4	vlajka
s	s	k7c7	s
předchozí	předchozí	k2eAgFnSc7d1	předchozí
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
nápis	nápis	k1gInSc1	nápis
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
iniciálami	iniciála	k1gFnPc7	iniciála
G.C.	G.C.	k1gMnPc2	G.C.
(	(	kIx(	(
<g/>
Gold	Gold	k1gMnSc1	Gold
Coast	Coast	k1gMnSc1	Coast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Ghany	Ghana	k1gFnSc2	Ghana
byla	být	k5eAaImAgNnP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
součástí	součást	k1gFnSc7	součást
německé	německý	k2eAgFnSc2d1	německá
kolonie	kolonie	k1gFnSc2	kolonie
Togoland	Togolanda	k1gFnPc2	Togolanda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
kolonii	kolonie	k1gFnSc4	kolonie
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
několik	několik	k4yIc4	několik
variant	varianta	k1gFnPc2	varianta
vlastních	vlastní	k2eAgFnPc2d1	vlastní
vlajek	vlajka	k1gFnPc2	vlajka
a	a	k8xC	a
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
první	první	k4xOgFnSc3	první
světové	světový	k2eAgFnSc3d1	světová
válce	válka	k1gFnSc3	válka
ale	ale	k8xC	ale
nebyla	být	k5eNaImAgFnS	být
žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nikdy	nikdy	k6eAd1	nikdy
zavedena	zavést	k5eAaPmNgNnP	zavést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1901	[number]	k4	1901
bylo	být	k5eAaImAgNnS	být
Zlatonosné	zlatonosný	k2eAgNnSc1d1	zlatonosné
pobřeží	pobřeží	k1gNnSc1	pobřeží
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ašantským	Ašantský	k2eAgInSc7d1	Ašantský
protektorátem	protektorát	k1gInSc7	protektorát
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
korunní	korunní	k2eAgFnSc7d1	korunní
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
připojena	připojen	k2eAgFnSc1d1	připojena
tzv.	tzv.	kA	tzv.
Severní	severní	k2eAgNnSc1d1	severní
území	území	k1gNnSc1	území
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
pak	pak	k6eAd1	pak
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
bývalého	bývalý	k2eAgInSc2d1	bývalý
německého	německý	k2eAgInSc2d1	německý
Togolandu	Togoland	k1gInSc2	Togoland
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
mandátní	mandátní	k2eAgNnSc4d1	mandátní
území	území	k1gNnSc4	území
Britské	britský	k2eAgNnSc1d1	Britské
Togo	Togo	k1gNnSc1	Togo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
zůstala	zůstat	k5eAaPmAgFnS	zůstat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
zachována	zachován	k2eAgFnSc1d1	zachována
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
získalo	získat	k5eAaPmAgNnS	získat
Zlatonosné	zlatonosný	k2eAgNnSc4d1	zlatonosné
pobřeží	pobřeží	k1gNnSc4	pobřeží
samosprávu	samospráva	k1gFnSc4	samospráva
a	a	k8xC	a
vlastní	vlastní	k2eAgFnSc4d1	vlastní
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1956	[number]	k4	1956
se	se	k3xPyFc4	se
Britské	britský	k2eAgNnSc1d1	Britské
Togo	Togo	k1gNnSc1	Togo
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
ve	v	k7c6	v
všelidovém	všelidový	k2eAgNnSc6d1	všelidové
hlasování	hlasování	k1gNnSc6	hlasování
o	o	k7c4	o
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
budoucímu	budoucí	k2eAgInSc3d1	budoucí
samostatnému	samostatný	k2eAgInSc3d1	samostatný
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1957	[number]	k4	1957
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc1	nezávislost
nového	nový	k2eAgInSc2d1	nový
státu	stát	k1gInSc2	stát
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Ghana	Ghana	k1gFnSc1	Ghana
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
zavlála	zavlát	k5eAaPmAgFnS	zavlát
poprvé	poprvé	k6eAd1	poprvé
ghanská	ghanský	k2eAgFnSc1d1	ghanská
vlajka	vlajka	k1gFnSc1	vlajka
(	(	kIx(	(
<g/>
kterou	který	k3yRgFnSc4	který
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
paní	paní	k1gFnSc1	paní
Theodosia	Theodosia	k1gFnSc1	Theodosia
Salome	Salom	k1gInSc5	Salom
Okohová	Okohová	k1gFnSc1	Okohová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
shodnou	shodný	k2eAgFnSc4d1	shodná
se	se	k3xPyFc4	se
současnou	současný	k2eAgFnSc4d1	současná
<g/>
.11	.11	k4	.11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1958	[number]	k4	1958
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Federace	federace	k1gFnSc1	federace
spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
nazývána	nazývat	k5eAaImNgFnS	nazývat
též	též	k9	též
Unie	unie	k1gFnSc1	unie
Ghana-Guinea	Ghana-Guinea	k1gFnSc1	Ghana-Guinea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
<g />
.	.	kIx.	.
</s>
<s>
tento	tento	k3xDgInSc4	tento
útvar	útvar	k1gInSc4	útvar
zavedena	zaveden	k2eAgFnSc1d1	zavedena
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ghanské	ghanský	k2eAgFnPc1d1	ghanská
dvě	dva	k4xCgFnPc1	dva
černé	černý	k2eAgFnPc1d1	černá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
symbolizující	symbolizující	k2eAgInPc1d1	symbolizující
oba	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
federace	federace	k1gFnSc2	federace
<g/>
:	:	kIx,	:
Ghanu	Ghana	k1gFnSc4	Ghana
a	a	k8xC	a
Guineu	Guinea	k1gFnSc4	Guinea
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1959	[number]	k4	1959
byla	být	k5eAaImAgFnS	být
federace	federace	k1gFnSc1	federace
<g/>
/	/	kIx~	/
<g/>
unie	unie	k1gFnSc1	unie
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
na	na	k7c4	na
Svaz	svaz	k1gInSc4	svaz
afrických	africký	k2eAgInPc2d1	africký
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Unie	unie	k1gFnSc1	unie
afrických	africký	k2eAgInPc2d1	africký
států	stát	k1gInPc2	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ghana	Ghana	k1gFnSc1	Ghana
i	i	k8xC	i
Guinea	Guinea	k1gFnSc1	Guinea
užívaly	užívat	k5eAaImAgFnP	užívat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
i	i	k8xC	i
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1960	[number]	k4	1960
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Ghanská	ghanský	k2eAgFnSc1d1	ghanská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachována	zachován	k2eAgFnSc1d1	zachována
<g/>
.24	.24	k4	.24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1960	[number]	k4	1960
byla	být	k5eAaImAgFnS	být
přidána	přidat	k5eAaPmNgFnS	přidat
hvězda	hvězda	k1gFnSc1	hvězda
na	na	k7c4	na
vlajku	vlajka	k1gFnSc4	vlajka
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
po	po	k7c6	po
přistoupení	přistoupení	k1gNnSc6	přistoupení
Mali	Mali	k1gNnSc2	Mali
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
užívala	užívat	k5eAaImAgFnS	užívat
do	do	k7c2	do
května	květen	k1gInSc2	květen
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
Organizace	organizace	k1gFnSc2	organizace
africké	africký	k2eAgFnSc2d1	africká
jednoty	jednota	k1gFnSc2	jednota
(	(	kIx(	(
<g/>
předchůdce	předchůdce	k1gMnSc2	předchůdce
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Africké	africký	k2eAgFnSc2d1	africká
unie	unie	k1gFnSc2	unie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1964	[number]	k4	1964
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
prostřední	prostřední	k2eAgInSc1d1	prostřední
<g/>
,	,	kIx,	,
žlutý	žlutý	k2eAgInSc1d1	žlutý
pruh	pruh	k1gInSc1	pruh
na	na	k7c6	na
ghanské	ghanský	k2eAgFnSc6d1	ghanská
vlajce	vlajka	k1gFnSc6	vlajka
na	na	k7c4	na
bílý	bílý	k2eAgInSc4d1	bílý
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
vlajka	vlajka	k1gFnSc1	vlajka
shodná	shodný	k2eAgFnSc1d1	shodná
s	s	k7c7	s
vlajkou	vlajka	k1gFnSc7	vlajka
Dohodové	dohodový	k2eAgFnSc2d1	dohodová
lidové	lidový	k2eAgFnSc2d1	lidová
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
jediné	jediný	k2eAgFnSc2d1	jediná
legální	legální	k2eAgFnSc2d1	legální
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vojenském	vojenský	k2eAgInSc6d1	vojenský
puči	puč	k1gInSc6	puč
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1966	[number]	k4	1966
byl	být	k5eAaImAgInS	být
svržen	svržen	k2eAgInSc1d1	svržen
autoritářský	autoritářský	k2eAgInSc1d1	autoritářský
režim	režim	k1gInSc1	režim
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
Kofi	Kof	k1gFnSc2	Kof
Kwane	Kwan	k1gInSc5	Kwan
Nkrumah	Nkrumah	k1gInSc1	Nkrumah
byl	být	k5eAaImAgMnS	být
přinucen	přinutit	k5eAaPmNgMnS	přinutit
emigrovat	emigrovat	k5eAaBmF	emigrovat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1966	[number]	k4	1966
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
užívání	užívání	k1gNnSc1	užívání
vlajky	vlajka	k1gFnSc2	vlajka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Ghany	Ghana	k1gFnSc2	Ghana
</s>
</p>
<p>
<s>
Ghanská	ghanský	k2eAgFnSc1d1	ghanská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ghanská	ghanský	k2eAgFnSc1d1	ghanská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
