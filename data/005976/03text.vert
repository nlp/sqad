<s>
Bráhmí	Bráhmí	k1gNnSc1	Bráhmí
je	být	k5eAaImIp3nS	být
písmo	písmo	k1gNnSc4	písmo
používané	používaný	k2eAgNnSc4d1	používané
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
nejméně	málo	k6eAd3	málo
od	od	k7c2	od
vlády	vláda	k1gFnSc2	vláda
krále	král	k1gMnSc2	král
Ašóky	Ašóka	k1gMnSc2	Ašóka
(	(	kIx(	(
<g/>
273	[number]	k4	273
až	až	k9	až
232	[number]	k4	232
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
nápisy	nápis	k1gInPc1	nápis
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
vytesány	vytesán	k2eAgFnPc1d1	vytesána
právě	právě	k6eAd1	právě
v	v	k7c6	v
bráhmí	bráhmí	k1gNnSc6	bráhmí
<g/>
,	,	kIx,	,
z	z	k7c2	z
menší	malý	k2eAgFnSc2d2	menší
pak	pak	k6eAd1	pak
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
kháróšthí	kháróšthí	k1gFnSc2	kháróšthí
<g/>
.	.	kIx.	.
</s>
<s>
Písmo	písmo	k1gNnSc1	písmo
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
vodorovně	vodorovně	k6eAd1	vodorovně
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
bráhmí	bráhmí	k1gNnSc1	bráhmí
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ještě	ještě	k6eAd1	ještě
pár	pár	k4xCyI	pár
set	set	k1gInSc4	set
let	léto	k1gNnPc2	léto
před	před	k7c7	před
Ašókou	Ašóka	k1gFnSc7	Ašóka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
písemné	písemný	k2eAgFnPc1d1	písemná
památky	památka	k1gFnPc1	památka
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
nedochovaly	dochovat	k5eNaPmAgInP	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bráhmí	bráhmí	k1gNnSc2	bráhmí
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
většina	většina	k1gFnSc1	většina
dnes	dnes	k6eAd1	dnes
používaných	používaný	k2eAgNnPc2d1	používané
indických	indický	k2eAgNnPc2d1	indické
písem	písmo	k1gNnPc2	písmo
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
písem	písmo	k1gNnPc2	písmo
založených	založený	k2eAgNnPc2d1	založené
na	na	k7c6	na
persko-arabské	perskorabský	k2eAgFnSc6d1	persko-arabský
abecedě	abeceda	k1gFnSc6	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Znalost	znalost	k1gFnSc1	znalost
samotného	samotný	k2eAgNnSc2d1	samotné
bráhmí	bráhmí	k1gNnSc2	bráhmí
postupně	postupně	k6eAd1	postupně
upadala	upadat	k5eAaPmAgFnS	upadat
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dillíský	dillíský	k2eAgMnSc1d1	dillíský
sultán	sultán	k1gMnSc1	sultán
objevil	objevit	k5eAaPmAgMnS	objevit
dva	dva	k4xCgInPc4	dva
Ašókovy	Ašókův	k2eAgInPc4d1	Ašókův
sloupy	sloup	k1gInPc4	sloup
<g/>
,	,	kIx,	,
nenašel	najít	k5eNaPmAgInS	najít
mezi	mezi	k7c7	mezi
indickými	indický	k2eAgMnPc7d1	indický
učenci	učenec	k1gMnPc7	učenec
nikoho	nikdo	k3yNnSc2	nikdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
by	by	kYmCp3nS	by
dokázal	dokázat	k5eAaPmAgMnS	dokázat
nápisy	nápis	k1gInPc4	nápis
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
přečíst	přečíst	k5eAaPmF	přečíst
<g/>
.	.	kIx.	.
</s>
<s>
Rozluštil	rozluštit	k5eAaPmAgMnS	rozluštit
ho	on	k3xPp3gNnSc4	on
znovu	znovu	k6eAd1	znovu
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1837	[number]	k4	1837
Angličan	Angličan	k1gMnSc1	Angličan
James	James	k1gMnSc1	James
Prinsep	Prinsep	k1gMnSc1	Prinsep
<g/>
.	.	kIx.	.
</s>
<s>
Ašókovy	Ašókův	k2eAgInPc1d1	Ašókův
nápisy	nápis	k1gInPc1	nápis
byly	být	k5eAaImAgInP	být
psány	psát	k5eAaImNgInP	psát
většinou	většina	k1gFnSc7	většina
středoindickými	středoindický	k2eAgInPc7d1	středoindický
hovorovými	hovorový	k2eAgInPc7d1	hovorový
dialekty	dialekt	k1gInPc7	dialekt
<g/>
,	,	kIx,	,
nazývanými	nazývaný	k2eAgInPc7d1	nazývaný
prákrty	prákrt	k1gInPc7	prákrt
<g/>
.	.	kIx.	.
</s>
<s>
Písmo	písmo	k1gNnSc1	písmo
původně	původně	k6eAd1	původně
zachycovalo	zachycovat	k5eAaImAgNnS	zachycovat
32	[number]	k4	32
souhlásek	souhláska	k1gFnPc2	souhláska
a	a	k8xC	a
5	[number]	k4	5
samohlásek	samohláska	k1gFnPc2	samohláska
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
sanskrtu	sanskrt	k1gInSc6	sanskrt
přidáním	přidání	k1gNnSc7	přidání
znaků	znak	k1gInPc2	znak
pro	pro	k7c4	pro
dvojhlásky	dvojhláska	k1gFnPc4	dvojhláska
"	"	kIx"	"
<g/>
ai	ai	k?	ai
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
au	au	k0	au
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
od	od	k7c2	od
písma	písmo	k1gNnSc2	písmo
bráhmí	bráhmit	k5eAaPmIp3nS	bráhmit
k	k	k7c3	k
dnešním	dnešní	k2eAgNnPc3d1	dnešní
písmům	písmo	k1gNnPc3	písmo
postupoval	postupovat	k5eAaImAgMnS	postupovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
větvích	větev	k1gFnPc6	větev
<g/>
,	,	kIx,	,
severoindické	severoindický	k2eAgNnSc4d1	severoindické
(	(	kIx(	(
<g/>
kušánské	kušánský	k2eAgNnSc4d1	kušánský
bráhmí	bráhmí	k1gNnSc4	bráhmí
<g/>
)	)	kIx)	)
a	a	k8xC	a
jihoindické	jihoindický	k2eAgFnPc1d1	jihoindická
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgNnPc1d1	jižní
písma	písmo	k1gNnPc1	písmo
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgNnP	začít
oddělovat	oddělovat	k5eAaImF	oddělovat
už	už	k6eAd1	už
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
i	i	k9	i
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
nepoužívané	používaný	k2eNgNnSc4d1	nepoužívané
písmo	písmo	k1gNnSc4	písmo
grantha	granth	k1gMnSc2	granth
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
sloužilo	sloužit	k5eAaImAgNnS	sloužit
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
především	především	k9	především
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
sanskrtu	sanskrt	k1gInSc2	sanskrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
variantě	varianta	k1gFnSc6	varianta
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
většina	většina	k1gFnSc1	většina
tvarů	tvar	k1gInPc2	tvar
zakulacuje	zakulacovat	k5eAaImIp3nS	zakulacovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
vidět	vidět	k5eAaImF	vidět
např.	např.	kA	např.
na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
telugském	telugské	k1gNnSc6	telugské
<g/>
,	,	kIx,	,
kannadském	kannadský	k2eAgNnSc6d1	kannadský
nebo	nebo	k8xC	nebo
sinhálském	sinhálský	k2eAgNnSc6d1	sinhálské
písmu	písmo	k1gNnSc6	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Jakási	jakýsi	k3yIgFnSc1	jakýsi
patka	patka	k1gFnSc1	patka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
zakončuje	zakončovat	k5eAaImIp3nS	zakončovat
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
většina	většina	k1gFnSc1	většina
telugských	telugská	k1gFnPc2	telugská
nebo	nebo	k8xC	nebo
kannadských	kannadský	k2eAgNnPc2d1	kannadský
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
přerostla	přerůst	k5eAaPmAgFnS	přerůst
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
větvi	větev	k1gFnSc6	větev
do	do	k7c2	do
souvislé	souvislý	k2eAgFnSc2d1	souvislá
čáry	čára	k1gFnSc2	čára
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nP	by
všechna	všechen	k3xTgNnPc1	všechen
písmena	písmeno	k1gNnPc1	písmeno
byla	být	k5eAaImAgNnP	být
zavěšena	zavěsit	k5eAaPmNgNnP	zavěsit
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
lince	linka	k1gFnSc6	linka
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vypadá	vypadat	k5eAaPmIp3nS	vypadat
písmo	písmo	k1gNnSc4	písmo
dévanágarí	dévanágarí	k1gFnSc2	dévanágarí
i	i	k8xC	i
příbuzná	příbuzný	k2eAgNnPc4d1	příbuzné
písma	písmo	k1gNnPc4	písmo
gurmukhí	gurmukhí	k2eAgNnPc4d1	gurmukhí
a	a	k8xC	a
bengálské	bengálský	k2eAgNnSc4d1	bengálské
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Kód	kód	k1gInSc1	kód
písma	písmo	k1gNnSc2	písmo
bráhmí	bráhmit	k5eAaPmIp3nS	bráhmit
podle	podle	k7c2	podle
normy	norma	k1gFnSc2	norma
ISO	ISO	kA	ISO
15924	[number]	k4	15924
je	být	k5eAaImIp3nS	být
Brah	Brah	k1gMnSc1	Brah
<g/>
.	.	kIx.	.
</s>
<s>
Semitská	semitský	k2eAgFnSc1d1	semitská
(	(	kIx(	(
<g/>
fénická	fénický	k2eAgFnSc1d1	fénická
nebo	nebo	k8xC	nebo
aramejská	aramejský	k2eAgFnSc1d1	aramejská
<g/>
)	)	kIx)	)
teorie	teorie	k1gFnSc1	teorie
původu	původ	k1gInSc2	původ
bráhmí	bráhmí	k1gNnSc2	bráhmí
se	se	k3xPyFc4	se
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
dostupná	dostupný	k2eAgNnPc4d1	dostupné
fakta	faktum	k1gNnPc4	faktum
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
nejstarší	starý	k2eAgInPc4d3	nejstarší
nápisy	nápis	k1gInPc4	nápis
bráhmí	bráhmí	k1gNnPc2	bráhmí
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
nápadnou	nápadný	k2eAgFnSc4d1	nápadná
souběžnost	souběžnost	k1gFnSc4	souběžnost
s	s	k7c7	s
tehdejším	tehdejší	k2eAgNnSc7d1	tehdejší
aramejským	aramejský	k2eAgNnSc7d1	aramejské
písmem	písmo	k1gNnSc7	písmo
pro	pro	k7c4	pro
hlásky	hláska	k1gFnPc4	hláska
blízké	blízký	k2eAgFnSc2d1	blízká
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
jazycích	jazyk	k1gInPc6	jazyk
zejména	zejména	k9	zejména
pokud	pokud	k6eAd1	pokud
se	se	k3xPyFc4	se
zohlední	zohlednit	k5eAaPmIp3nP	zohlednit
odchylky	odchylka	k1gFnPc1	odchylka
způsobené	způsobený	k2eAgFnPc1d1	způsobená
změnou	změna	k1gFnSc7	změna
směru	směr	k1gInSc2	směr
psaní	psaní	k1gNnSc4	psaní
–	–	k?	–
aramejsky	aramejsky	k6eAd1	aramejsky
se	se	k3xPyFc4	se
psalo	psát	k5eAaImAgNnS	psát
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
g	g	kA	g
značí	značit	k5eAaImIp3nS	značit
shodně	shodně	k6eAd1	shodně
Λ	Λ	k?	Λ
a	a	k8xC	a
t	t	k?	t
píší	psát	k5eAaImIp3nP	psát
ʎ	ʎ	k?	ʎ
Bráhmí	Bráhmí	k1gNnSc1	Bráhmí
má	mít	k5eAaImIp3nS	mít
řadu	řada	k1gFnSc4	řada
prvků	prvek	k1gInPc2	prvek
rozšířenou	rozšířený	k2eAgFnSc4d1	rozšířená
oproti	oproti	k7c3	oproti
aramejské	aramejský	k2eAgFnSc3d1	aramejská
abecedě	abeceda	k1gFnSc3	abeceda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
musela	muset	k5eAaImAgFnS	muset
postihovat	postihovat	k5eAaImF	postihovat
více	hodně	k6eAd2	hodně
hlásek	hlásek	k1gInSc4	hlásek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
aramejština	aramejština	k1gFnSc1	aramejština
nepodchycovala	podchycovat	k5eNaImAgFnS	podchycovat
dentální	dentální	k2eAgFnPc4d1	dentální
závěrové	závěrový	k2eAgFnPc4d1	závěrová
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
d	d	k?	d
z	z	k7c2	z
retroflexní	retroflexní	k2eAgFnSc2d1	retroflexní
souhlásky	souhláska	k1gFnSc2	souhláska
jako	jako	k8xS	jako
ḍ	ḍ	k?	ḍ
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
bráhmí	bráhmí	k1gNnSc6	bráhmí
jsou	být	k5eAaImIp3nP	být
dentální	dentální	k2eAgFnPc1d1	dentální
a	a	k8xC	a
retroflexní	retroflexní	k2eAgFnPc1d1	retroflexní
souhlásky	souhláska	k1gFnPc1	souhláska
graficky	graficky	k6eAd1	graficky
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc1d1	podobné
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
kdyby	kdyby	kYmCp3nP	kdyby
byly	být	k5eAaImAgInP	být
odvozené	odvozený	k2eAgInPc1d1	odvozený
ze	z	k7c2	z
společného	společný	k2eAgInSc2d1	společný
aramejského	aramejský	k2eAgInSc2d1	aramejský
prototypu	prototyp	k1gInSc2	prototyp
<g/>
.	.	kIx.	.
</s>
<s>
Srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
vývoj	vývoj	k1gInSc1	vývoj
nastal	nastat	k5eAaPmAgInS	nastat
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
tibetské	tibetský	k2eAgFnSc6d1	tibetská
abecedě	abeceda	k1gFnSc6	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Aramejština	aramejština	k1gFnSc1	aramejština
neměla	mít	k5eNaImAgFnS	mít
bráhmíské	bráhmíské	k1gNnSc4	bráhmíské
aspirované	aspirovaný	k2eAgFnSc2d1	aspirovaná
(	(	kIx(	(
<g/>
přídechové	přídechový	k2eAgFnSc2d1	přídechový
<g/>
)	)	kIx)	)
souhlásky	souhláska	k1gFnSc2	souhláska
kh	kh	k0	kh
<g/>
,	,	kIx,	,
th	th	k?	th
apod.	apod.	kA	apod.
A	a	k8xC	a
bráhmí	bráhmí	k1gNnSc1	bráhmí
nemá	mít	k5eNaImIp3nS	mít
aramejské	aramejský	k2eAgFnPc4d1	aramejská
rázové	rázový	k2eAgFnPc4d1	rázová
souhlásky	souhláska	k1gFnPc4	souhláska
q	q	k?	q
<g/>
,	,	kIx,	,
ṭ	ṭ	k?	ṭ
<g/>
,	,	kIx,	,
ṣ	ṣ	k?	ṣ
a	a	k8xC	a
tyto	tento	k3xDgInPc1	tento
znaky	znak	k1gInPc1	znak
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
bráhmí	bráhmí	k1gNnSc6	bráhmí
jako	jako	k8xC	jako
aspiranty	aspirant	k1gMnPc7	aspirant
:	:	kIx,	:
aramejské	aramejský	k2eAgFnSc2d1	aramejská
q	q	k?	q
v	v	k7c6	v
bráhmí	bráhmí	k1gNnSc6	bráhmí
jako	jako	k8xC	jako
kh	kh	k0	kh
aramejské	aramejský	k2eAgFnPc4d1	aramejská
ṭ	ṭ	k?	ṭ
(	(	kIx(	(
<g/>
Θ	Θ	k?	Θ
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
v	v	k7c6	v
brā	brā	k?	brā
th	th	k?	th
(	(	kIx(	(
<g/>
ʘ	ʘ	k?	ʘ
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
Právě	právě	k6eAd1	právě
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
aramejština	aramejština	k1gFnSc1	aramejština
nemá	mít	k5eNaImIp3nS	mít
rázovou	rázový	k2eAgFnSc4d1	rázová
souhlásku	souhláska	k1gFnSc4	souhláska
p	p	k?	p
<g/>
,	,	kIx,	,
bráhmí	bráhmí	k1gNnSc4	bráhmí
má	mít	k5eAaImIp3nS	mít
dvojí	dvojit	k5eAaImIp3nS	dvojit
pro	pro	k7c4	pro
příslušné	příslušný	k2eAgMnPc4d1	příslušný
aspiranty	aspirant	k1gMnPc4	aspirant
<g/>
:	:	kIx,	:
bráhmíské	bráhmíský	k1gMnPc4	bráhmíský
p	p	k?	p
a	a	k8xC	a
ph	ph	kA	ph
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
podobné	podobný	k2eAgInPc1d1	podobný
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nP	by
pocházely	pocházet	k5eAaImAgFnP	pocházet
společně	společně	k6eAd1	společně
z	z	k7c2	z
aramejského	aramejský	k2eAgInSc2d1	aramejský
p.	p.	k?	p.
První	první	k4xOgFnSc1	první
písmeno	písmeno	k1gNnSc1	písmeno
obou	dva	k4xCgFnPc2	dva
abeced	abeceda	k1gFnPc2	abeceda
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
také	také	k9	také
shoduje	shodovat	k5eAaImIp3nS	shodovat
<g/>
:	:	kIx,	:
a	a	k8xC	a
z	z	k7c2	z
bráhmí	bráhmí	k1gNnSc2	bráhmí
připomíná	připomínat	k5eAaImIp3nS	připomínat
obrácené	obrácený	k2eAgNnSc1d1	obrácené
κ	κ	k?	κ
<g/>
,	,	kIx,	,
vypadá	vypadat	k5eAaPmIp3nS	vypadat
úplně	úplně	k6eAd1	úplně
jako	jako	k8xS	jako
aramejské	aramejský	k2eAgInPc1d1	aramejský
alef	alef	k1gInSc1	alef
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
hebrejskému	hebrejský	k2eAgInSc3d1	hebrejský
א	א	k?	א
Tabulka	tabulka	k1gFnSc1	tabulka
porovnání	porovnání	k1gNnSc2	porovnání
bráhmí	bráhmí	k1gNnSc2	bráhmí
a	a	k8xC	a
fénické	fénický	k2eAgFnSc2d1	fénická
a	a	k8xC	a
aramejské	aramejský	k2eAgFnSc2d1	aramejská
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
:	:	kIx,	:
*	*	kIx~	*
Jak	jak	k8xS	jak
féničtina	féničtina	k1gFnSc1	féničtina
a	a	k8xC	a
aramejština	aramejština	k1gFnSc1	aramejština
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
bráhmí	bráhmí	k1gNnSc1	bráhmí
měly	mít	k5eAaImAgInP	mít
tři	tři	k4xCgFnPc4	tři
slabikotvorné	slabikotvorný	k2eAgFnPc4d1	slabikotvorný
souhlásky	souhláska	k1gFnPc4	souhláska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
příslušnost	příslušnost	k1gFnSc1	příslušnost
není	být	k5eNaImIp3nS	být
jasná	jasný	k2eAgFnSc1d1	jasná
<g/>
.	.	kIx.	.
</s>
<s>
Neuvádí	uvádět	k5eNaImIp3nS	uvádět
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
šest	šest	k4xCc4	šest
souhlásek	souhláska	k1gFnPc2	souhláska
bráhmí	bráhmí	k1gNnSc2	bráhmí
<g/>
:	:	kIx,	:
bh	bh	k?	bh
<g/>
,	,	kIx,	,
gh	gh	k?	gh
<g/>
,	,	kIx,	,
h	h	k?	h
<g/>
,	,	kIx,	,
j	j	k?	j
<g/>
,	,	kIx,	,
jh	jh	k?	jh
<g/>
,	,	kIx,	,
ny	ny	k?	ny
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
odvozené	odvozený	k2eAgNnSc1d1	odvozené
s	s	k7c7	s
nejasnou	jasný	k2eNgFnSc7d1	nejasná
návazností	návaznost	k1gFnSc7	návaznost
<g/>
:	:	kIx,	:
he	he	k0	he
<g/>
,	,	kIx,	,
heth	heth	k1gMnSc1	heth
<g/>
,	,	kIx,	,
and	and	k?	and
ayin	ayin	k1gInSc1	ayin
<g/>
.	.	kIx.	.
</s>
<s>
Bráhmínské	Bráhmínský	k2eAgNnSc1d1	Bráhmínský
ng	ng	k?	ng
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
dodatečně	dodatečně	k6eAd1	dodatečně
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
gh	gh	k?	gh
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
vyvozovat	vyvozovat	k5eAaImF	vyvozovat
z	z	k7c2	z
heth	hetha	k1gFnPc2	hetha
<g/>
.	.	kIx.	.
</s>
