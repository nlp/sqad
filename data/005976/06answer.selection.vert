<s>
Bráhmí	Bráhmí	k1gNnSc1	Bráhmí
je	být	k5eAaImIp3nS	být
písmo	písmo	k1gNnSc4	písmo
používané	používaný	k2eAgNnSc4d1	používané
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
nejméně	málo	k6eAd3	málo
od	od	k7c2	od
vlády	vláda	k1gFnSc2	vláda
krále	král	k1gMnSc2	král
Ašóky	Ašóka	k1gMnSc2	Ašóka
(	(	kIx(	(
<g/>
273	[number]	k4	273
až	až	k9	až
232	[number]	k4	232
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
nápisy	nápis	k1gInPc1	nápis
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
vytesány	vytesán	k2eAgFnPc1d1	vytesána
právě	právě	k6eAd1	právě
v	v	k7c6	v
bráhmí	bráhmí	k1gNnSc6	bráhmí
<g/>
,	,	kIx,	,
z	z	k7c2	z
menší	malý	k2eAgFnSc2d2	menší
pak	pak	k6eAd1	pak
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
kháróšthí	kháróšthí	k1gFnSc2	kháróšthí
<g/>
.	.	kIx.	.
</s>
