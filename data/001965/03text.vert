<s>
Ottawa	Ottawa	k1gFnSc1	Ottawa
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
šestým	šestý	k4xOgMnSc7	šestý
největším	veliký	k2eAgNnSc7d3	veliký
kanadským	kanadský	k2eAgNnSc7d1	kanadské
městem	město	k1gNnSc7	město
a	a	k8xC	a
druhým	druhý	k4xOgMnPc3	druhý
největším	veliký	k2eAgMnPc3d3	veliký
městem	město	k1gNnSc7	město
provincie	provincie	k1gFnSc2	provincie
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
řeky	řeka	k1gFnSc2	řeka
Ottawa	Ottawa	k1gFnSc1	Ottawa
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
provincie	provincie	k1gFnSc2	provincie
Ontario	Ontario	k1gNnSc1	Ontario
v	v	k7c6	v
Ottawském	ottawský	k2eAgNnSc6d1	ottawský
údolí	údolí	k1gNnSc6	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Ottawa	Ottawa	k1gFnSc1	Ottawa
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
celkem	celkem	k6eAd1	celkem
883391	[number]	k4	883391
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ottawa	Ottawa	k1gFnSc1	Ottawa
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
jen	jen	k9	jen
malou	malý	k2eAgFnSc7d1	malá
vesnicí	vesnice	k1gFnSc7	vesnice
jménem	jméno	k1gNnSc7	jméno
Bytown	Bytown	k1gNnSc1	Bytown
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1827	[number]	k4	1827
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
získala	získat	k5eAaPmAgFnS	získat
statut	statut	k1gInSc4	statut
města	město	k1gNnSc2	město
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
královnou	královna	k1gFnSc7	královna
Viktorií	Viktoria	k1gFnSc7	Viktoria
jako	jako	k8xC	jako
město	město	k1gNnSc1	město
předsedající	předsedající	k1gMnSc1	předsedající
kanadské	kanadský	k2eAgFnSc3d1	kanadská
vládě	vláda	k1gFnSc3	vláda
(	(	kIx(	(
<g/>
Canadian	Canadian	k1gMnSc1	Canadian
Government	Government	k1gMnSc1	Government
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
přecházel	přecházet	k5eAaImAgInS	přecházet
spor	spor	k1gInSc1	spor
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
anglickou	anglický	k2eAgFnSc7d1	anglická
<g/>
"	"	kIx"	"
částí	část	k1gFnSc7	část
kanadského	kanadský	k2eAgNnSc2d1	kanadské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Ottawa	Ottawa	k1gFnSc1	Ottawa
byla	být	k5eAaImAgFnS	být
kompromis	kompromis	k1gInSc4	kompromis
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
anglicky	anglicky	k6eAd1	anglicky
mluvícího	mluvící	k2eAgNnSc2d1	mluvící
Ontaria	Ontario	k1gNnSc2	Ontario
a	a	k8xC	a
francouzské	francouzský	k2eAgFnSc2d1	francouzská
provincie	provincie	k1gFnSc2	provincie
Québec	Québec	k1gMnSc1	Québec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
existence	existence	k1gFnSc2	existence
množství	množství	k1gNnSc2	množství
vládních	vládní	k2eAgFnPc2d1	vládní
budov	budova	k1gFnPc2	budova
je	být	k5eAaImIp3nS	být
architektura	architektura	k1gFnSc1	architektura
centra	centrum	k1gNnSc2	centrum
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
a	a	k8xC	a
formalistická	formalistický	k2eAgFnSc1d1	formalistická
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
budov	budova	k1gFnPc2	budova
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
romantickém	romantický	k2eAgInSc6d1	romantický
<g/>
,	,	kIx,	,
či	či	k8xC	či
jiném	jiný	k2eAgInSc6d1	jiný
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
parlamentu	parlament	k1gInSc2	parlament
nese	nést	k5eAaImIp3nS	nést
znaky	znak	k1gInPc4	znak
viktoriánské	viktoriánský	k2eAgFnSc2d1	viktoriánská
gotiky	gotika	k1gFnSc2	gotika
<g/>
.	.	kIx.	.
</s>
<s>
Architektura	architektura	k1gFnSc1	architektura
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
rodinnými	rodinný	k2eAgInPc7d1	rodinný
domky	domek	k1gInPc7	domek
<g/>
,	,	kIx,	,
z	z	k7c2	z
menší	malý	k2eAgFnSc2d2	menší
části	část	k1gFnSc2	část
apartmánovými	apartmánův	k2eAgInPc7d1	apartmánův
domky	domek	k1gInPc7	domek
a	a	k8xC	a
obytnými	obytný	k2eAgInPc7d1	obytný
bloky	blok	k1gInPc7	blok
<g/>
.	.	kIx.	.
</s>
<s>
Panorama	panorama	k1gNnSc1	panorama
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
zákazu	zákaz	k1gInSc2	zákaz
výstavby	výstavba	k1gFnSc2	výstavba
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
<g/>
.	.	kIx.	.
</s>
<s>
Omezení	omezení	k1gNnPc1	omezení
byla	být	k5eAaImAgNnP	být
zavedena	zavést	k5eAaPmNgNnP	zavést
proto	proto	k6eAd1	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
budova	budova	k1gFnSc1	budova
kanadského	kanadský	k2eAgInSc2d1	kanadský
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jeho	jeho	k3xOp3gFnSc4	jeho
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
věž	věž	k1gFnSc4	věž
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
92	[number]	k4	92
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Ottawa	Ottawa	k1gFnSc1	Ottawa
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
šesti	šest	k4xCc2	šest
územních	územní	k2eAgFnPc2d1	územní
částí	část	k1gFnPc2	část
-	-	kIx~	-
Kanata	Kanata	k1gFnSc1	Kanata
(	(	kIx(	(
<g/>
západ	západ	k1gInSc1	západ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nepean	Nepean	k1gInSc1	Nepean
(	(	kIx(	(
<g/>
jihozápad	jihozápad	k1gInSc1	jihozápad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Downtown	Downtown	k1gNnSc1	Downtown
(	(	kIx(	(
<g/>
centrum	centrum	k1gNnSc1	centrum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gloucester	Gloucester	k1gInSc1	Gloucester
(	(	kIx(	(
<g/>
jihovýchod	jihovýchod	k1gInSc1	jihovýchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Orléans	Orléans	k1gInSc1	Orléans
(	(	kIx(	(
<g/>
východ	východ	k1gInSc1	východ
<g/>
)	)	kIx)	)
a	a	k8xC	a
Cumberland	Cumberland	k1gInSc1	Cumberland
(	(	kIx(	(
<g/>
východ	východ	k1gInSc1	východ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Rideau	Rideaus	k1gInSc2	Rideaus
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
konci	konec	k1gInSc6	konec
města	město	k1gNnSc2	město
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Ottawy	Ottawa	k1gFnSc2	Ottawa
<g/>
,	,	kIx,	,
dělící	dělící	k2eAgNnPc1d1	dělící
města	město	k1gNnPc1	město
Ottawu	Ottawa	k1gFnSc4	Ottawa
a	a	k8xC	a
sousedící	sousedící	k2eAgNnSc4d1	sousedící
město	město	k1gNnSc4	město
Gatineau	Gatineaus	k1gInSc2	Gatineaus
na	na	k7c6	na
Qubecké	Qubecký	k2eAgFnSc6d1	Qubecký
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Ottawa	Ottawa	k1gFnSc1	Ottawa
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
kanadské	kanadský	k2eAgFnPc4d1	kanadská
provincie	provincie	k1gFnPc4	provincie
Ontario	Ontario	k1gNnSc1	Ontario
a	a	k8xC	a
Québec	Québec	k1gInSc1	Québec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
městě	město	k1gNnSc6	město
jsou	být	k5eAaImIp3nP	být
početné	početný	k2eAgInPc1d1	početný
chodníky	chodník	k1gInPc1	chodník
pro	pro	k7c4	pro
cyklisty	cyklista	k1gMnPc4	cyklista
a	a	k8xC	a
chodce	chodec	k1gMnPc4	chodec
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
probíhá	probíhat	k5eAaImIp3nS	probíhat
podél	podél	k7c2	podél
řek	řeka	k1gFnPc2	řeka
Ottawa	Ottawa	k1gFnSc1	Ottawa
<g/>
,	,	kIx,	,
Rideau	Ridea	k2eAgFnSc4d1	Ridea
a	a	k8xC	a
vodní	vodní	k2eAgFnSc4d1	vodní
cesty	cesta	k1gFnPc4	cesta
Rideau	Rideaus	k1gInSc2	Rideaus
Canal	Canal	k1gInSc1	Canal
<g/>
.	.	kIx.	.
</s>
<s>
Cyklistika	cyklistika	k1gFnSc1	cyklistika
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgInSc4d1	populární
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
aglomeraci	aglomerace	k1gFnSc6	aglomerace
Ottawa-Gatineau	Ottawa-Gatineaus	k1gInSc2	Ottawa-Gatineaus
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
220	[number]	k4	220
km	km	kA	km
tratí	tratit	k5eAaImIp3nS	tratit
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
uvedeny	uvést	k5eAaPmNgInP	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
cyklistické	cyklistický	k2eAgFnPc4d1	cyklistická
tratě	trať	k1gFnPc4	trať
oddělené	oddělený	k2eAgFnPc4d1	oddělená
od	od	k7c2	od
silnic	silnice	k1gFnPc2	silnice
betonovými	betonový	k2eAgFnPc7d1	betonová
zábranami	zábrana	k1gFnPc7	zábrana
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečný	bezpečný	k2eAgInSc1d1	bezpečný
provoz	provoz	k1gInSc1	provoz
cyklistů	cyklista	k1gMnPc2	cyklista
podporuje	podporovat	k5eAaImIp3nS	podporovat
dorovolnická	dorovolnický	k2eAgFnSc1d1	dorovolnický
organizace	organizace	k1gFnSc1	organizace
Citizens	Citizensa	k1gFnPc2	Citizensa
for	forum	k1gNnPc2	forum
Safe	safe	k1gInSc1	safe
Cycling	Cycling	k1gInSc1	Cycling
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
jsou	být	k5eAaImIp3nP	být
ulice	ulice	k1gFnPc1	ulice
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Ottawy	Ottawa	k1gFnSc2	Ottawa
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
rezervovány	rezervovat	k5eAaBmNgInP	rezervovat
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
chodce	chodec	k1gMnPc4	chodec
a	a	k8xC	a
cyklisty	cyklista	k1gMnPc4	cyklista
<g/>
.	.	kIx.	.
</s>
<s>
Ottawu	Ottawa	k1gFnSc4	Ottawa
protínají	protínat	k5eAaImIp3nP	protínat
kanadské	kanadský	k2eAgFnPc1d1	kanadská
dálnice	dálnice	k1gFnPc1	dálnice
417	[number]	k4	417
a	a	k8xC	a
416	[number]	k4	416
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nachází	nacházet	k5eAaImIp3nS	nacházet
mezinárodní	mezinárodní	k2eAgNnPc4d1	mezinárodní
letiště	letiště	k1gNnPc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
vcelku	vcelku	k6eAd1	vcelku
fungující	fungující	k2eAgNnSc1d1	fungující
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
systému	systém	k1gInSc6	systém
autobusových	autobusový	k2eAgInPc2d1	autobusový
koridorů	koridor	k1gInPc2	koridor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
časový	časový	k2eAgInSc4d1	časový
rozestup	rozestup	k1gInSc4	rozestup
mezi	mezi	k7c7	mezi
spoji	spoj	k1gInPc7	spoj
nižší	nízký	k2eAgFnSc2d2	nižší
než	než	k8xS	než
pět	pět	k4xCc4	pět
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zaměstnavateli	zaměstnavatel	k1gMnSc3	zaměstnavatel
v	v	k7c6	v
Ottawě	Ottawa	k1gFnSc6	Ottawa
jsou	být	k5eAaImIp3nP	být
vládní	vládní	k2eAgInSc4d1	vládní
sektor	sektor	k1gInSc4	sektor
a	a	k8xC	a
odvětví	odvětví	k1gNnSc4	odvětví
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
třetí	třetí	k4xOgInSc4	třetí
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
příjem	příjem	k1gInSc4	příjem
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
třetí	třetí	k4xOgFnSc4	třetí
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
nezaměstnanost	nezaměstnanost	k1gFnSc4	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
Ottawa	Ottawa	k1gFnSc1	Ottawa
Senators	Senatorsa	k1gFnPc2	Senatorsa
<g/>
,	,	kIx,	,
jediného	jediný	k2eAgInSc2d1	jediný
profesionálního	profesionální	k2eAgInSc2d1	profesionální
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
hrajícího	hrající	k2eAgMnSc2d1	hrající
v	v	k7c6	v
National	National	k1gFnSc6	National
Hockey	Hockea	k1gMnSc2	Hockea
League	Leagu	k1gMnSc2	Leagu
<g/>
.	.	kIx.	.
</s>
<s>
Domovským	domovský	k2eAgInSc7d1	domovský
stadionem	stadion	k1gInSc7	stadion
týmu	tým	k1gInSc2	tým
je	být	k5eAaImIp3nS	být
Canadian	Canadian	k1gInSc1	Canadian
Tire	Tir	k1gFnSc2	Tir
Centre	centr	k1gInSc5	centr
<g/>
.	.	kIx.	.
</s>
<s>
Lední	lední	k2eAgInSc4d1	lední
hokej	hokej	k1gInSc4	hokej
provozují	provozovat	k5eAaImIp3nP	provozovat
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc1	všechen
generace	generace	k1gFnPc1	generace
<g/>
,	,	kIx,	,
muži	muž	k1gMnPc1	muž
i	i	k8xC	i
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
tým	tým	k1gInSc1	tým
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
plánuje	plánovat	k5eAaImIp3nS	plánovat
provoz	provoz	k1gInSc4	provoz
druhý	druhý	k4xOgInSc4	druhý
profesionální	profesionální	k2eAgInSc4d1	profesionální
sportový	sportový	k2eAgInSc4d1	sportový
tým	tým	k1gInSc4	tým
<g/>
,	,	kIx,	,
v	v	k7c6	v
kanadském	kanadský	k2eAgInSc6d1	kanadský
fotbalu	fotbal	k1gInSc6	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ottawě	Ottawa	k1gFnSc6	Ottawa
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
klubové	klubový	k2eAgFnSc6d1	klubová
úrovni	úroveň	k1gFnSc6	úroveň
provozován	provozovat	k5eAaImNgInS	provozovat
též	též	k9	též
basketbal	basketbal	k1gInSc1	basketbal
<g/>
,	,	kIx,	,
curling	curling	k1gInSc1	curling
<g/>
,	,	kIx,	,
baseball	baseball	k1gInSc1	baseball
<g/>
,	,	kIx,	,
fotbal	fotbal	k1gInSc1	fotbal
<g/>
,	,	kIx,	,
dostihy	dostih	k1gInPc1	dostih
<g/>
,	,	kIx,	,
gaelic	gaelice	k1gFnPc2	gaelice
footbal	footbal	k1gInSc1	footbal
a	a	k8xC	a
australský	australský	k2eAgInSc1d1	australský
fotbal	fotbal	k1gInSc1	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ottawa	Ottawa	k1gFnSc1	Ottawa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Ottawa	Ottawa	k1gFnSc1	Ottawa
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
