<s>
Ottawa	Ottawa	k1gFnSc1	Ottawa
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
šesti	šest	k4xCc2	šest
územních	územní	k2eAgFnPc2d1	územní
částí	část	k1gFnPc2	část
-	-	kIx~	-
Kanata	Kanata	k1gFnSc1	Kanata
(	(	kIx(	(
<g/>
západ	západ	k1gInSc1	západ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nepean	Nepean	k1gInSc1	Nepean
(	(	kIx(	(
<g/>
jihozápad	jihozápad	k1gInSc1	jihozápad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Downtown	Downtown	k1gNnSc1	Downtown
(	(	kIx(	(
<g/>
centrum	centrum	k1gNnSc1	centrum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gloucester	Gloucester	k1gInSc1	Gloucester
(	(	kIx(	(
<g/>
jihovýchod	jihovýchod	k1gInSc1	jihovýchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Orléans	Orléans	k1gInSc1	Orléans
(	(	kIx(	(
<g/>
východ	východ	k1gInSc1	východ
<g/>
)	)	kIx)	)
a	a	k8xC	a
Cumberland	Cumberland	k1gInSc1	Cumberland
(	(	kIx(	(
<g/>
východ	východ	k1gInSc1	východ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
