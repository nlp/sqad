<s>
Mongolština	mongolština	k1gFnSc1	mongolština
je	být	k5eAaImIp3nS	být
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
jazykem	jazyk	k1gInSc7	jazyk
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
mongolských	mongolský	k2eAgInPc2d1	mongolský
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
hlavním	hlavní	k2eAgInSc7d1	hlavní
jazykem	jazyk	k1gInSc7	jazyk
ve	v	k7c6	v
Státě	stát	k1gInSc6	stát
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
jím	on	k3xPp3gNnSc7	on
kolem	kolo	k1gNnSc7	kolo
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
,	,	kIx,	,
žijících	žijící	k2eAgMnPc2d1	žijící
ve	v	k7c6	v
Státě	stát	k1gInSc6	stát
Mongolsko	Mongolsko	k1gNnSc4	Mongolsko
a	a	k8xC	a
v	v	k7c6	v
přilehlých	přilehlý	k2eAgFnPc6d1	přilehlá
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c6	na
území	území	k1gNnSc6	území
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
zejména	zejména	k9	zejména
ve	v	k7c6	v
Vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
spisovnou	spisovný	k2eAgFnSc4d1	spisovná
variantu	varianta	k1gFnSc4	varianta
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
považováno	považován	k2eAgNnSc1d1	považováno
nejrozšířenější	rozšířený	k2eAgNnSc1d3	nejrozšířenější
chalchské	chalchský	k2eAgNnSc1d1	chalchský
nářečí	nářečí	k1gNnSc1	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
Mongolština	mongolština	k1gFnSc1	mongolština
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svého	svůj	k3xOyFgInSc2	svůj
vývoje	vývoj	k1gInSc2	vývoj
používala	používat	k5eAaImAgFnS	používat
hned	hned	k6eAd1	hned
několik	několik	k4yIc4	několik
písem	písmo	k1gNnPc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgNnSc1d1	tradiční
mongolské	mongolský	k2eAgNnSc1d1	mongolské
písmo	písmo	k1gNnSc1	písmo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
při	při	k7c6	při
formování	formování	k1gNnSc6	formování
Čingischánovy	Čingischánův	k2eAgFnSc2d1	Čingischánova
Mongolské	mongolský	k2eAgFnSc2d1	mongolská
říše	říš	k1gFnSc2	říš
<g/>
;	;	kIx,	;
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gInSc4	jeho
základ	základ	k1gInSc4	základ
posloužilo	posloužit	k5eAaPmAgNnS	posloužit
ujgurské	ujgurský	k2eAgNnSc1d1	ujgurské
písmo	písmo	k1gNnSc1	písmo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
znaková	znakový	k2eAgFnSc1d1	znaková
sada	sada	k1gFnSc1	sada
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
s	s	k7c7	s
drobnými	drobný	k2eAgFnPc7d1	drobná
obměnami	obměna	k1gFnPc7	obměna
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
Vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
záhy	záhy	k6eAd1	záhy
po	po	k7c6	po
vytvoření	vytvoření	k1gNnSc6	vytvoření
tradičního	tradiční	k2eAgNnSc2d1	tradiční
mongolského	mongolský	k2eAgNnSc2d1	mongolské
písma	písmo	k1gNnSc2	písmo
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
pokusům	pokus	k1gInPc3	pokus
vytvořit	vytvořit	k5eAaPmF	vytvořit
pro	pro	k7c4	pro
mongolštinu	mongolština	k1gFnSc4	mongolština
lépe	dobře	k6eAd2	dobře
se	se	k3xPyFc4	se
hodící	hodící	k2eAgNnSc1d1	hodící
písmo	písmo	k1gNnSc1	písmo
(	(	kIx(	(
<g/>
Phagpovo	Phagpův	k2eAgNnSc1d1	Phagpův
kvadrátní	kvadrátní	k2eAgNnSc1d1	kvadrátní
písmo	písmo	k1gNnSc1	písmo
z	z	k7c2	z
tibetského	tibetský	k2eAgNnSc2d1	tibetské
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
sojombo	sojomba	k1gFnSc5	sojomba
z	z	k7c2	z
indických	indický	k2eAgNnPc2d1	indické
písem	písmo	k1gNnPc2	písmo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ho	on	k3xPp3gInSc4	on
aktualizovat	aktualizovat	k5eAaBmF	aktualizovat
(	(	kIx(	(
<g/>
todo-bičig	todoičig	k1gInSc1	todo-bičig
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
galik	galik	k1gInSc1	galik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
se	se	k3xPyFc4	se
tradiční	tradiční	k2eAgNnSc1d1	tradiční
písmo	písmo	k1gNnSc1	písmo
používalo	používat	k5eAaImAgNnS	používat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
latinkou	latinka	k1gFnSc7	latinka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
změně	změna	k1gFnSc3	změna
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
upravená	upravený	k2eAgFnSc1d1	upravená
cyrilice	cyrilice	k1gFnSc1	cyrilice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
návratu	návrat	k1gInSc2	návrat
ke	k	k7c3	k
kulturním	kulturní	k2eAgFnPc3d1	kulturní
tradicím	tradice	k1gFnPc3	tradice
k	k	k7c3	k
pokusům	pokus	k1gInPc3	pokus
o	o	k7c4	o
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
tradičnímu	tradiční	k2eAgNnSc3d1	tradiční
písmu	písmo	k1gNnSc3	písmo
–	–	k?	–
to	ten	k3xDgNnSc1	ten
si	se	k3xPyFc3	se
sice	sice	k8xC	sice
nezískalo	získat	k5eNaPmAgNnS	získat
takovou	takový	k3xDgFnSc4	takový
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
písmo	písmo	k1gNnSc1	písmo
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
cyrilici	cyrilice	k1gFnSc4	cyrilice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
jsou	být	k5eAaImIp3nP	být
obě	dva	k4xCgNnPc1	dva
písma	písmo	k1gNnPc1	písmo
vyučována	vyučován	k2eAgFnSc1d1	vyučována
<g/>
.	.	kIx.	.
</s>
<s>
Užívání	užívání	k1gNnSc1	užívání
latinky	latinka	k1gFnSc2	latinka
v	v	k7c6	v
elektronických	elektronický	k2eAgInPc6d1	elektronický
textech	text	k1gInPc6	text
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c4	mezi
Mongoly	Mongol	k1gMnPc4	Mongol
také	také	k6eAd1	také
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
.	.	kIx.	.
</s>
<s>
Mongolská	mongolský	k2eAgFnSc1d1	mongolská
znaková	znakový	k2eAgFnSc1d1	znaková
sada	sada	k1gFnSc1	sada
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
klasické	klasický	k2eAgFnSc2d1	klasická
cyrilice	cyrilice	k1gFnSc2	cyrilice
(	(	kIx(	(
<g/>
ruské	ruský	k2eAgFnSc2d1	ruská
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
navíc	navíc	k6eAd1	navíc
znaky	znak	k1gInPc1	znak
"	"	kIx"	"
<g/>
Ө	Ө	k?	Ө
ө	ө	k?	ө
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Ү	Ү	k?	Ү
ү	ү	k?	ү
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
písmena	písmeno	k1gNnPc1	písmeno
klasické	klasický	k2eAgFnSc2d1	klasická
cyrilice	cyrilice	k1gFnSc2	cyrilice
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jen	jen	k9	jen
v	v	k7c6	v
přejatých	přejatý	k2eAgNnPc6d1	přejaté
slovech	slovo	k1gNnPc6	slovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mongolštině	mongolština	k1gFnSc6	mongolština
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
vokálová	vokálový	k2eAgFnSc1d1	vokálová
harmonie	harmonie	k1gFnSc1	harmonie
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
má	mít	k5eAaImIp3nS	mít
sedm	sedm	k4xCc4	sedm
pádů	pád	k1gInPc2	pád
–	–	k?	–
nominativ	nominativ	k1gInSc1	nominativ
<g/>
,	,	kIx,	,
akuzativ	akuzativ	k1gInSc1	akuzativ
<g/>
,	,	kIx,	,
genitiv	genitiv	k1gInSc1	genitiv
<g/>
,	,	kIx,	,
dativ-lokativ	dativokativ	k1gInSc1	dativ-lokativ
<g/>
,	,	kIx,	,
ablativ	ablativ	k1gInSc1	ablativ
<g/>
,	,	kIx,	,
komitativ	komitativ	k1gInSc1	komitativ
a	a	k8xC	a
instrumentál	instrumentál	k1gInSc1	instrumentál
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
Všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
deklarace	deklarace	k1gFnSc2	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
:	:	kIx,	:
Х	Х	k?	Х
б	б	k?	б
т	т	k?	т
м	м	k?	м
э	э	k?	э
ч	ч	k?	ч
<g/>
,	,	kIx,	,
а	а	k?	а
н	н	k?	н
т	т	k?	т
<g/>
,	,	kIx,	,
и	и	k?	и
э	э	k?	э
б	б	k?	б
<g/>
.	.	kIx.	.
О	О	k?	О
у	у	k?	у
н	н	k?	н
ч	ч	k?	ч
з	з	k?	з
х	х	k?	х
г	г	k?	г
ө	ө	k?	ө
х	х	k?	х
а	а	k?	а
д	д	k?	д
ү	ү	k?	ү
с	с	k?	с
х	х	k?	х
у	у	k?	у
<g/>
.	.	kIx.	.
transkripce	transkripce	k1gFnSc1	transkripce
<g/>
:	:	kIx,	:
Khün	Khün	k1gInSc1	Khün
bür	bür	k?	bür
törzh	törzh	k1gInSc1	törzh
mendlekhee	mendlekheat	k5eAaPmIp3nS	mendlekheat
erkh	erkh	k1gInSc4	erkh
čölöötei	čölööte	k1gFnSc2	čölööte
<g/>
,	,	kIx,	,
adilkhan	adilkhan	k1gMnSc1	adilkhan
ner	ner	k?	ner
törtei	törte	k1gFnSc2	törte
<g/>
,	,	kIx,	,
izhil	izhit	k5eAaImAgMnS	izhit
erkhtei	erkhtei	k6eAd1	erkhtei
baidag	baidag	k1gMnSc1	baidag
<g/>
.	.	kIx.	.
</s>
<s>
Oyuun	Oyuun	k1gMnSc1	Oyuun
ukhaan	ukhaan	k1gMnSc1	ukhaan
nandin	nandina	k1gFnPc2	nandina
čanar	čanar	k1gMnSc1	čanar
zayaasan	zayaasan	k1gMnSc1	zayaasan
khün	khün	k1gMnSc1	khün
gegč	gegč	k1gMnSc1	gegč
öör	öör	k?	öör
khoorondoo	khoorondoo	k1gMnSc1	khoorondoo
akhan	akhan	k1gMnSc1	akhan
düügiin	düügiin	k2eAgMnSc1d1	düügiin
üzel	üzel	k1gMnSc1	üzel
sanaagaar	sanaagaar	k1gMnSc1	sanaagaar
khar	khar	k1gMnSc1	khar
<g/>
'	'	kIx"	'
<g/>
tsakh	tsakh	k1gMnSc1	tsakh
učirtai	učirta	k1gFnSc2	učirta
<g/>
.	.	kIx.	.
překlad	překlad	k1gInSc4	překlad
<g/>
:	:	kIx,	:
Všichni	všechen	k3xTgMnPc1	všechen
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
svobodní	svobodný	k2eAgMnPc1d1	svobodný
a	a	k8xC	a
sobě	se	k3xPyFc3	se
rovní	rovný	k2eAgMnPc1d1	rovný
co	co	k9	co
do	do	k7c2	do
důstojnosti	důstojnost	k1gFnSc2	důstojnost
a	a	k8xC	a
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
nadáni	nadat	k5eAaPmNgMnP	nadat
rozumem	rozum	k1gInSc7	rozum
a	a	k8xC	a
svědomím	svědomí	k1gNnSc7	svědomí
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
spolu	spolu	k6eAd1	spolu
jednat	jednat	k5eAaImF	jednat
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
bratrství	bratrství	k1gNnPc4	bratrství
<g/>
.	.	kIx.	.
</s>
