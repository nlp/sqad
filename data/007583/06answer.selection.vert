<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Olomouc	Olomouc	k1gFnSc1	Olomouc
–	–	k?	–
Opava	Opava	k1gFnSc1	Opava
východ	východ	k1gInSc1	východ
(	(	kIx(	(
<g/>
v	v	k7c6	v
jízdním	jízdní	k2eAgInSc6d1	jízdní
řádu	řád	k1gInSc6	řád
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
označená	označený	k2eAgFnSc1d1	označená
číslem	číslo	k1gNnSc7	číslo
310	[number]	k4	310
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednokolejná	jednokolejný	k2eAgFnSc1d1	jednokolejná
trať	trať	k1gFnSc1	trať
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
celostátní	celostátní	k2eAgFnSc2d1	celostátní
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
