<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Olomouc	Olomouc	k1gFnSc1	Olomouc
–	–	k?	–
Opava	Opava	k1gFnSc1	Opava
východ	východ	k1gInSc1	východ
(	(	kIx(	(
<g/>
v	v	k7c6	v
jízdním	jízdní	k2eAgInSc6d1	jízdní
řádu	řád	k1gInSc6	řád
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
označená	označený	k2eAgFnSc1d1	označená
číslem	číslo	k1gNnSc7	číslo
310	[number]	k4	310
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednokolejná	jednokolejný	k2eAgFnSc1d1	jednokolejná
trať	trať	k1gFnSc1	trať
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
celostátní	celostátní	k2eAgFnSc2d1	celostátní
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Trať	trať	k1gFnSc1	trať
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Olomouce	Olomouc	k1gFnSc2	Olomouc
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
přes	přes	k7c4	přes
Valšov	Valšov	k1gInSc4	Valšov
<g/>
,	,	kIx,	,
Bruntál	Bruntál	k1gInSc1	Bruntál
<g/>
,	,	kIx,	,
Milotice	Milotice	k1gFnSc1	Milotice
nad	nad	k7c7	nad
Opavou	Opava	k1gFnSc7	Opava
a	a	k8xC	a
Krnov	Krnov	k1gInSc1	Krnov
do	do	k7c2	do
Opavy	Opava	k1gFnSc2	Opava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trati	trať	k1gFnSc6	trať
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pět	pět	k4xCc1	pět
tunelů	tunel	k1gInPc2	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1872	[number]	k4	1872
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Krnova	Krnov	k1gInSc2	Krnov
vedla	vést	k5eAaImAgFnS	vést
odbočná	odbočný	k2eAgFnSc1d1	odbočná
trať	trať	k1gFnSc1	trať
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
nynější	nynější	k2eAgNnSc1d1	nynější
území	území	k1gNnSc1	území
Polska	Polsko	k1gNnSc2	Polsko
<g/>
)	)	kIx)	)
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Głubczyce	Głubczyce	k1gFnSc2	Głubczyce
<g/>
;	;	kIx,	;
doprava	doprava	k1gFnSc1	doprava
byla	být	k5eAaImAgFnS	být
zastavena	zastavit	k5eAaPmNgFnS	zastavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
uspořádání	uspořádání	k1gNnSc2	uspořádání
Evropy	Evropa	k1gFnSc2	Evropa
po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
;	;	kIx,	;
zčásti	zčásti	k6eAd1	zčásti
zůstala	zůstat	k5eAaPmAgFnS	zůstat
jako	jako	k9	jako
vlečka	vlečka	k1gFnSc1	vlečka
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
byl	být	k5eAaImAgInS	být
snesen	snést	k5eAaPmNgInS	snést
<g/>
.	.	kIx.	.
</s>
<s>
Trať	trať	k1gFnSc1	trať
z	z	k7c2	z
Opavy	Opava	k1gFnSc2	Opava
k	k	k7c3	k
státní	státní	k2eAgFnSc3d1	státní
hranici	hranice	k1gFnSc3	hranice
a	a	k8xC	a
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
území	území	k1gNnSc1	území
Polska	Polsko	k1gNnSc2	Polsko
<g/>
)	)	kIx)	)
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Pilszcz	Pilszcz	k1gInSc1	Pilszcz
zahájila	zahájit	k5eAaPmAgFnS	zahájit
provoz	provoz	k1gInSc4	provoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
a	a	k8xC	a
ukončila	ukončit	k5eAaPmAgFnS	ukončit
provoz	provoz	k1gInSc4	provoz
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
důvodu	důvod	k1gInSc2	důvod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
0	[number]	k4	0
–	–	k?	–
Olomouc	Olomouc	k1gFnSc1	Olomouc
hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
(	(	kIx(	(
<g/>
stanice	stanice	k1gFnSc2	stanice
<g/>
)	)	kIx)	)
215	[number]	k4	215
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
4	[number]	k4	4
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Bystrovany	Bystrovan	k1gMnPc7	Bystrovan
(	(	kIx(	(
<g/>
zastávka	zastávka	k1gFnSc1	zastávka
<g/>
)	)	kIx)	)
230	[number]	k4	230
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
6	[number]	k4	6
–	–	k?	–
Velká	velký	k2eAgFnSc1d1	velká
Bystřice	Bystřice	k1gFnSc1	Bystřice
(	(	kIx(	(
<g/>
stanice	stanice	k1gFnSc1	stanice
<g/>
)	)	kIx)	)
240	[number]	k4	240
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
8	[number]	k4	8
–	–	k?	–
Velká	velký	k2eAgFnSc1d1	velká
Bystřice	Bystřice	k1gFnSc1	Bystřice
(	(	kIx(	(
<g/>
zastávka	zastávka	k1gFnSc1	zastávka
<g/>
)	)	kIx)	)
245	[number]	k4	245
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
11	[number]	k4	11
–	–	k?	–
Hlubočky-Mariánské	Hlubočky-Mariánský	k2eAgNnSc1d1	Hlubočky-Mariánské
Údolí	údolí	k1gNnSc1	údolí
žst	žst	k?	žst
<g/>
.	.	kIx.	.
265	[number]	k4	265
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
13	[number]	k4	13
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Hlubočky	Hlubočka	k1gFnPc1	Hlubočka
zastávka	zastávka	k1gFnSc1	zastávka
275	[number]	k4	275
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
15	[number]	k4	15
–	–	k?	–
Hlubočky	Hlubočka	k1gFnSc2	Hlubočka
(	(	kIx(	(
<g/>
stanice	stanice	k1gFnSc2	stanice
<g/>
)	)	kIx)	)
295	[number]	k4	295
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
18	[number]	k4	18
–	–	k?	–
Hrubá	hrubý	k2eAgFnSc1d1	hrubá
Voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
zastávka	zastávka	k1gFnSc1	zastávka
<g/>
)	)	kIx)	)
330	[number]	k4	330
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
19	[number]	k4	19
–	–	k?	–
Hrubá	hrubý	k2eAgFnSc1d1	hrubá
Voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
stanice	stanice	k1gFnSc1	stanice
<g/>
)	)	kIx)	)
345	[number]	k4	345
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
22	[number]	k4	22
–	–	k?	–
Hrubá	Hrubá	k1gFnSc1	Hrubá
Voda-Smilov	Voda-Smilov	k1gInSc1	Voda-Smilov
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
zastávka	zastávka	k1gFnSc1	zastávka
<g/>
)	)	kIx)	)
385	[number]	k4	385
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
25	[number]	k4	25
–	–	k?	–
Hl.	Hl.	k1gFnPc2	Hl.
Jívová	jívový	k2eAgFnSc1d1	Jívová
(	(	kIx(	(
<g/>
zastávka	zastávka	k1gFnSc1	zastávka
<g/>
)	)	kIx)	)
430	[number]	k4	430
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
29	[number]	k4	29
–	–	k?	–
Domašov	Domašov	k1gInSc1	Domašov
nad	nad	k7c7	nad
Bystřicí	Bystřice	k1gFnSc7	Bystřice
(	(	kIx(	(
<g/>
stanice	stanice	k1gFnSc1	stanice
<g/>
)	)	kIx)	)
495	[number]	k4	495
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
36	[number]	k4	36
–	–	k?	–
Moravský	moravský	k2eAgInSc1d1	moravský
Beroun	Beroun	k1gInSc1	Beroun
(	(	kIx(	(
<g/>
stanice	stanice	k1gFnSc1	stanice
<g/>
)	)	kIx)	)
535	[number]	k4	535
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
45	[number]	k4	45
–	–	k?	–
Dětřichov	Dětřichov	k1gInSc1	Dětřichov
<g />
.	.	kIx.	.
</s>
<s>
nad	nad	k7c7	nad
Bystřicí	Bystřice	k1gFnSc7	Bystřice
(	(	kIx(	(
<g/>
stanice	stanice	k1gFnSc1	stanice
<g/>
)	)	kIx)	)
620	[number]	k4	620
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
49	[number]	k4	49
–	–	k?	–
Lomnice	Lomnice	k1gFnSc2	Lomnice
u	u	k7c2	u
Rýmařova	rýmařův	k2eAgInSc2d1	rýmařův
(	(	kIx(	(
<g/>
zastávka	zastávka	k1gFnSc1	zastávka
<g/>
)	)	kIx)	)
595	[number]	k4	595
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
56	[number]	k4	56
–	–	k?	–
Valšov	Valšov	k1gInSc1	Valšov
(	(	kIx(	(
<g/>
stanice	stanice	k1gFnSc1	stanice
<g/>
)	)	kIx)	)
525	[number]	k4	525
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
64	[number]	k4	64
–	–	k?	–
Bruntál	Bruntál	k1gInSc1	Bruntál
(	(	kIx(	(
<g/>
stanice	stanice	k1gFnSc1	stanice
<g/>
)	)	kIx)	)
555	[number]	k4	555
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
73	[number]	k4	73
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Milotice	Milotice	k1gFnSc1	Milotice
nad	nad	k7c7	nad
Opavou	Opava	k1gFnSc7	Opava
(	(	kIx(	(
<g/>
stanice	stanice	k1gFnSc1	stanice
<g/>
)	)	kIx)	)
445	[number]	k4	445
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
78	[number]	k4	78
–	–	k?	–
Zátor	Zátor	k1gMnSc1	Zátor
(	(	kIx(	(
<g/>
zastávka	zastávka	k1gFnSc1	zastávka
<g/>
)	)	kIx)	)
400	[number]	k4	400
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
80	[number]	k4	80
–	–	k?	–
Brantice	Brantice	k1gFnSc1	Brantice
(	(	kIx(	(
<g/>
stanice	stanice	k1gFnSc1	stanice
<g/>
)	)	kIx)	)
385	[number]	k4	385
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
87	[number]	k4	87
–	–	k?	–
Krnov	Krnov	k1gInSc1	Krnov
(	(	kIx(	(
<g/>
stanice	stanice	k1gFnSc1	stanice
<g/>
)	)	kIx)	)
325	[number]	k4	325
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
90	[number]	k4	90
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Krnov-Cvilín	Krnov-Cvilín	k1gInSc1	Krnov-Cvilín
(	(	kIx(	(
<g/>
zastávka	zastávka	k1gFnSc1	zastávka
<g/>
)	)	kIx)	)
315	[number]	k4	315
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
96	[number]	k4	96
–	–	k?	–
Úvalno	Úvalno	k1gNnSc1	Úvalno
(	(	kIx(	(
<g/>
zastávka	zastávka	k1gFnSc1	zastávka
<g/>
)	)	kIx)	)
295	[number]	k4	295
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
100	[number]	k4	100
–	–	k?	–
Skrochovice	Skrochovice	k1gFnSc1	Skrochovice
(	(	kIx(	(
<g/>
stanice	stanice	k1gFnSc1	stanice
<g/>
)	)	kIx)	)
285	[number]	k4	285
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
103	[number]	k4	103
–	–	k?	–
Hl.	Hl.	k1gFnSc1	Hl.
Holasovice	Holasovice	k1gFnSc1	Holasovice
(	(	kIx(	(
<g/>
zastávka	zastávka	k1gFnSc1	zastávka
<g/>
)	)	kIx)	)
290	[number]	k4	290
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
108	[number]	k4	108
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Vávrovice	Vávrovice	k1gFnSc1	Vávrovice
(	(	kIx(	(
<g/>
zastávka	zastávka	k1gFnSc1	zastávka
<g/>
)	)	kIx)	)
265	[number]	k4	265
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
109	[number]	k4	109
–	–	k?	–
Palhanec	Palhanec	k1gInSc1	Palhanec
(	(	kIx(	(
<g/>
nákladní	nákladní	k2eAgFnSc1d1	nákladní
<g/>
)	)	kIx)	)
112	[number]	k4	112
–	–	k?	–
Opava	Opava	k1gFnSc1	Opava
západ	západ	k1gInSc1	západ
(	(	kIx(	(
<g/>
stanice	stanice	k1gFnSc1	stanice
<g/>
)	)	kIx)	)
270	[number]	k4	270
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
116	[number]	k4	116
–	–	k?	–
Opava	Opava	k1gFnSc1	Opava
východ	východ	k1gInSc1	východ
(	(	kIx(	(
<g/>
stanice	stanice	k1gFnSc1	stanice
<g/>
)	)	kIx)	)
255	[number]	k4	255
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Sudet	Sudety	k1gFnPc2	Sudety
Německem	Německo	k1gNnSc7	Německo
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
postavena	postaven	k2eAgFnSc1d1	postavena
propojovací	propojovací	k2eAgFnSc1d1	propojovací
kolej	kolej	k1gFnSc1	kolej
mezi	mezi	k7c7	mezi
železniční	železniční	k2eAgFnSc7d1	železniční
tratí	trať	k1gFnSc7	trať
Mährisch	Mährisch	k1gMnSc1	Mährisch
Schönberg	Schönberg	k1gMnSc1	Schönberg
-	-	kIx~	-
Jägerndorf	Jägerndorf	k1gMnSc1	Jägerndorf
a	a	k8xC	a
železniční	železniční	k2eAgFnSc1d1	železniční
tratí	tratit	k5eAaImIp3nS	tratit
Olmütz	Olmütz	k1gInSc4	Olmütz
-	-	kIx~	-
Troppau	Troppaa	k1gFnSc4	Troppaa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
podél	podél	k7c2	podél
dnešní	dnešní	k2eAgFnSc2d1	dnešní
krnovské	krnovský	k2eAgFnSc2d1	Krnovská
zahrádkářské	zahrádkářský	k2eAgFnSc2d1	zahrádkářská
osady	osada	k1gFnSc2	osada
Úsvit	úsvit	k1gInSc1	úsvit
a	a	k8xC	a
která	který	k3yQgFnSc1	který
křižovala	křižovat	k5eAaImAgFnS	křižovat
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Jägerndorf	Jägerndorf	k1gMnSc1	Jägerndorf
-	-	kIx~	-
Leobschütz	Leobschütz	k1gMnSc1	Leobschütz
<g/>
.	.	kIx.	.
</s>
<s>
Propojovací	propojovací	k2eAgFnSc1d1	propojovací
kolej	kolej	k1gFnSc1	kolej
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vytrhána	vytrhán	k2eAgFnSc1d1	vytrhána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
ČR	ČR	kA	ČR
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Výběrové	výběrový	k2eAgNnSc1d1	výběrové
řízení	řízení	k1gNnSc1	řízení
na	na	k7c4	na
dopravce	dopravce	k1gMnPc4	dopravce
na	na	k7c6	na
rychlíkové	rychlíkový	k2eAgFnSc6d1	rychlíková
lince	linka	k1gFnSc6	linka
R27	R27	k1gFnPc2	R27
Olomouc	Olomouc	k1gFnSc1	Olomouc
–	–	k?	–
Krnov	Krnov	k1gInSc1	Krnov
–	–	k?	–
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Přihlásili	přihlásit	k5eAaPmAgMnP	přihlásit
se	se	k3xPyFc4	se
dva	dva	k4xCgMnPc1	dva
zájemci	zájemce	k1gMnPc1	zájemce
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
RegioJet	RegioJet	k1gInSc4	RegioJet
a	a	k8xC	a
Arriva	Arriva	k1gFnSc1	Arriva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2013	[number]	k4	2013
z	z	k7c2	z
řízení	řízení	k1gNnSc2	řízení
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
pro	pro	k7c4	pro
nesplnění	nesplnění	k1gNnSc4	nesplnění
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
ČR	ČR	kA	ČR
bez	bez	k7c2	bez
dalšího	další	k2eAgNnSc2d1	další
řízení	řízení	k1gNnSc2	řízení
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
dopravcem	dopravce	k1gMnSc7	dopravce
RegioJet	RegioJet	k1gInSc4	RegioJet
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
provoz	provoz	k1gInSc1	provoz
zahájí	zahájit	k5eAaPmIp3nS	zahájit
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2013	[number]	k4	2013
ovšem	ovšem	k9	ovšem
ministr	ministr	k1gMnSc1	ministr
dopravy	doprava	k1gFnSc2	doprava
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Stanjura	Stanjur	k1gMnSc2	Stanjur
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chystanou	chystaný	k2eAgFnSc4d1	chystaná
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
smlouvě	smlouva	k1gFnSc6	smlouva
budoucí	budoucí	k2eAgMnPc1d1	budoucí
s	s	k7c7	s
Jančurou	Jančura	k1gFnSc7	Jančura
nepodepíše	podepsat	k5eNaPmIp3nS	podepsat
<g/>
.	.	kIx.	.
</s>
