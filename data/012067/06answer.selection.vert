<s>
Plazmatická	plazmatický	k2eAgFnSc1d1	plazmatická
buňka	buňka	k1gFnSc1	buňka
(	(	kIx(	(
<g/>
či	či	k8xC	či
plazmocyt	plazmocyt	k1gInSc1	plazmocyt
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krvinka	krvinka	k1gFnSc1	krvinka
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
na	na	k7c4	na
produkci	produkce	k1gFnSc4	produkce
protilátek	protilátka	k1gFnPc2	protilátka
(	(	kIx(	(
<g/>
imunoglobulinů	imunoglobulin	k1gInPc2	imunoglobulin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
