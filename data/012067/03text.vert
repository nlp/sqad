<p>
<s>
Plazmatická	plazmatický	k2eAgFnSc1d1	plazmatická
buňka	buňka	k1gFnSc1	buňka
(	(	kIx(	(
<g/>
či	či	k8xC	či
plazmocyt	plazmocyt	k1gInSc1	plazmocyt
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krvinka	krvinka	k1gFnSc1	krvinka
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
na	na	k7c4	na
produkci	produkce	k1gFnSc4	produkce
protilátek	protilátka	k1gFnPc2	protilátka
(	(	kIx(	(
<g/>
imunoglobulinů	imunoglobulin	k1gInPc2	imunoglobulin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
přeměnou	přeměna	k1gFnSc7	přeměna
(	(	kIx(	(
<g/>
diferenciací	diferenciace	k1gFnSc7	diferenciace
<g/>
)	)	kIx)	)
B-lymfocytů	Bymfocyt	k1gInPc2	B-lymfocyt
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
poměrně	poměrně	k6eAd1	poměrně
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
povrchu	povrch	k1gInSc6	povrch
mají	mít	k5eAaImIp3nP	mít
minimální	minimální	k2eAgNnSc4d1	minimální
množství	množství	k1gNnSc4	množství
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zato	zato	k6eAd1	zato
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
(	(	kIx(	(
<g/>
sekretují	sekretovat	k5eAaImIp3nP	sekretovat
<g/>
)	)	kIx)	)
obrovské	obrovský	k2eAgNnSc1d1	obrovské
množství	množství	k1gNnSc1	množství
těchto	tento	k3xDgFnPc2	tento
bílkovin	bílkovina	k1gFnPc2	bílkovina
(	(	kIx(	(
<g/>
asi	asi	k9	asi
2000	[number]	k4	2000
molekul	molekula	k1gFnPc2	molekula
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protilátky	protilátka	k1gFnPc1	protilátka
čili	čili	k8xC	čili
imunoglobuliny	imunoglobulin	k2eAgInPc1d1	imunoglobulin
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
přichytávají	přichytávat	k5eAaImIp3nP	přichytávat
na	na	k7c4	na
cizorodé	cizorodý	k2eAgFnPc4d1	cizorodá
částice	částice	k1gFnPc4	částice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
)	)	kIx)	)
a	a	k8xC	a
představují	představovat	k5eAaImIp3nP	představovat
vlastně	vlastně	k9	vlastně
hlavní	hlavní	k2eAgInSc1d1	hlavní
princip	princip	k1gInSc1	princip
fungování	fungování	k1gNnSc2	fungování
tzv.	tzv.	kA	tzv.
látkové	látkový	k2eAgFnSc2d1	látková
imunity	imunita	k1gFnSc2	imunita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Plazmocyty	Plazmocyt	k1gInPc1	Plazmocyt
mají	mít	k5eAaImIp3nP	mít
excentrické	excentrický	k2eAgNnSc4d1	excentrické
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
jádro	jádro	k1gNnSc1	jádro
není	být	k5eNaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
uprostřed	uprostřed	k7c2	uprostřed
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
rozvinuté	rozvinutý	k2eAgNnSc1d1	rozvinuté
drsné	drsný	k2eAgNnSc1d1	drsné
endoplazmatické	endoplazmatický	k2eAgNnSc1d1	endoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
se	se	k3xPyFc4	se
nedělí	dělit	k5eNaImIp3nS	dělit
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
vůbec	vůbec	k9	vůbec
<g/>
;	;	kIx,	;
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
zřejmě	zřejmě	k6eAd1	zřejmě
uvězněné	uvězněný	k2eAgFnPc1d1	uvězněná
v	v	k7c6	v
G1	G1	k1gFnSc6	G1
fázi	fáze	k1gFnSc6	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
především	především	k6eAd1	především
v	v	k7c6	v
kostní	kostní	k2eAgFnSc6d1	kostní
dřeni	dřeň	k1gFnSc6	dřeň
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
v	v	k7c6	v
peri-mukózní	periukózní	k2eAgFnSc6d1	peri-mukózní
lymfatické	lymfatický	k2eAgFnSc6d1	lymfatická
tkáni	tkáň	k1gFnSc6	tkáň
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Peyerových	Peyerův	k2eAgInPc6d1	Peyerův
plátech	plát	k1gInPc6	plát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Vzniku	vznik	k1gInSc2	vznik
plazmatické	plazmatický	k2eAgFnSc2d1	plazmatická
buňky	buňka	k1gFnSc2	buňka
předchází	předcházet	k5eAaImIp3nS	předcházet
složitý	složitý	k2eAgInSc1d1	složitý
mechanismus	mechanismus	k1gInSc1	mechanismus
v	v	k7c6	v
buněčné	buněčný	k2eAgFnSc6d1	buněčná
populaci	populace	k1gFnSc6	populace
B-lymfocytů	Bymfocyt	k1gInPc2	B-lymfocyt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krvi	krev	k1gFnSc6	krev
koluje	kolovat	k5eAaImIp3nS	kolovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
B-lymfocytů	Bymfocyt	k1gInPc2	B-lymfocyt
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
každý	každý	k3xTgMnSc1	každý
je	být	k5eAaImIp3nS	být
specializovaný	specializovaný	k2eAgMnSc1d1	specializovaný
na	na	k7c4	na
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
cizorodou	cizorodý	k2eAgFnSc4d1	cizorodá
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
B-lymfocytů	Bymfocyt	k1gInPc2	B-lymfocyt
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
nedojde	dojít	k5eNaPmIp3nS	dojít
uplatnění	uplatnění	k1gNnSc4	uplatnění
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
však	však	k9	však
se	se	k3xPyFc4	se
nějaký	nějaký	k3yIgInSc1	nějaký
B-lymfocyt	Bymfocyt	k1gInSc1	B-lymfocyt
naváže	navázat	k5eAaPmIp3nS	navázat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
"	"	kIx"	"
<g/>
substrát	substrát	k1gInSc4	substrát
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
typizovaném	typizovaný	k2eAgInSc6d1	typizovaný
případě	případ	k1gInSc6	případ
např.	např.	kA	např.
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
buňka	buňka	k1gFnSc1	buňka
klonálně	klonálně	k6eAd1	klonálně
dělit	dělit	k5eAaImF	dělit
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
asistují	asistovat	k5eAaImIp3nP	asistovat
i	i	k9	i
tzv.	tzv.	kA	tzv.
pomocné	pomocný	k2eAgInPc1d1	pomocný
T-lymfocyty	Tymfocyt	k1gInPc1	T-lymfocyt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
části	část	k1gFnSc2	část
potomků	potomek	k1gMnPc2	potomek
tohoto	tento	k3xDgInSc2	tento
tzv.	tzv.	kA	tzv.
aktivovaného	aktivovaný	k2eAgInSc2d1	aktivovaný
B-lymfocytu	Bymfocyt	k1gInSc2	B-lymfocyt
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
paměťové	paměťový	k2eAgInPc1d1	paměťový
B-lymfocyty	Bymfocyt	k1gInPc1	B-lymfocyt
<g/>
,	,	kIx,	,
ze	z	k7c2	z
zbytku	zbytek	k1gInSc2	zbytek
pak	pak	k6eAd1	pak
právě	právě	k6eAd1	právě
plazmatické	plazmatický	k2eAgFnSc2d1	plazmatická
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Zrání	zrání	k1gNnSc1	zrání
B-lymfocytů	Bymfocyt	k1gInPc2	B-lymfocyt
na	na	k7c4	na
plazmatické	plazmatický	k2eAgFnPc4d1	plazmatická
buňky	buňka	k1gFnPc4	buňka
podporují	podporovat	k5eAaImIp3nP	podporovat
různé	různý	k2eAgFnPc4d1	různá
interleukiny	interleukina	k1gFnPc4	interleukina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
IL-4	IL-4	k1gMnSc1	IL-4
(	(	kIx(	(
<g/>
vylučovaný	vylučovaný	k2eAgMnSc1d1	vylučovaný
Th	Th	k1gMnSc1	Th
<g/>
2	[number]	k4	2
<g/>
-lymfocyty	ymfocyt	k1gInPc4	-lymfocyt
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
IL-6	IL-6	k1gMnPc2	IL-6
a	a	k8xC	a
IL-14	IL-14	k1gMnPc2	IL-14
(	(	kIx(	(
<g/>
vylučované	vylučovaný	k2eAgInPc4d1	vylučovaný
T-lymfocyty	Tymfocyt	k1gInPc4	T-lymfocyt
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
