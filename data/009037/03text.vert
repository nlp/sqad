<p>
<s>
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
monarchie	monarchie	k1gFnSc1	monarchie
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Habsburgermonarchie	Habsburgermonarchie	k1gFnSc1	Habsburgermonarchie
<g/>
)	)	kIx)	)
či	či	k8xC	či
hovorově	hovorově	k6eAd1	hovorově
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
monarchie	monarchie	k1gFnSc1	monarchie
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Österreichische	Österreichische	k1gFnSc1	Österreichische
Monarchie	monarchie	k1gFnSc2	monarchie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
Podunajská	podunajský	k2eAgFnSc1d1	Podunajská
monarchie	monarchie	k1gFnSc1	monarchie
(	(	kIx(	(
<g/>
Donaumonarchie	Donaumonarchie	k1gFnSc1	Donaumonarchie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zkráceně	zkráceně	k6eAd1	zkráceně
jen	jen	k9	jen
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
neoficiální	neoficiální	k2eAgInPc1d1	neoficiální
názvy	název	k1gInPc1	název
historického	historický	k2eAgNnSc2d1	historické
soustátí	soustátí	k1gNnSc2	soustátí
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
vládla	vládnout	k5eAaImAgFnS	vládnout
rakouská	rakouský	k2eAgFnSc1d1	rakouská
větev	větev	k1gFnSc1	větev
habsburské	habsburský	k2eAgFnSc2d1	habsburská
dynastie	dynastie	k1gFnSc2	dynastie
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
následnická	následnický	k2eAgFnSc1d1	následnická
habsbursko-lotrinská	habsburskootrinský	k2eAgFnSc1d1	habsbursko-lotrinská
dynastie	dynastie	k1gFnSc1	dynastie
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1526	[number]	k4	1526
<g/>
-	-	kIx~	-
<g/>
1804	[number]	k4	1804
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
habsburského	habsburský	k2eAgNnSc2d1	habsburské
soustátí	soustátí	k1gNnSc2	soustátí
oficiálně	oficiálně	k6eAd1	oficiálně
náležela	náležet	k5eAaImAgFnS	náležet
ke	k	k7c3	k
Svaté	svatý	k2eAgFnSc3d1	svatá
říši	říš	k1gFnSc3	říš
římské	římský	k2eAgFnSc3d1	římská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
monarchie	monarchie	k1gFnSc1	monarchie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
personální	personální	k2eAgFnSc1d1	personální
unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
když	když	k8xS	když
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
<g/>
,	,	kIx,	,
ovládající	ovládající	k2eAgInSc1d1	ovládající
doposud	doposud	k6eAd1	doposud
jen	jen	k9	jen
dědičné	dědičný	k2eAgFnSc2d1	dědičná
habsburské	habsburský	k2eAgFnSc2d1	habsburská
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
získal	získat	k5eAaPmAgInS	získat
českou	český	k2eAgFnSc4d1	Česká
a	a	k8xC	a
uherskou	uherský	k2eAgFnSc4d1	uherská
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
dočasné	dočasný	k2eAgInPc4d1	dočasný
neúspěchy	neúspěch	k1gInPc4	neúspěch
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
Habsburkům	Habsburk	k1gInPc3	Habsburk
podařilo	podařit	k5eAaPmAgNnS	podařit
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
monarchie	monarchie	k1gFnPc4	monarchie
integrovat	integrovat	k5eAaBmF	integrovat
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
centralizovanou	centralizovaný	k2eAgFnSc4d1	centralizovaná
správu	správa	k1gFnSc4	správa
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
roku	rok	k1gInSc2	rok
1804	[number]	k4	1804
přijal	přijmout	k5eAaPmAgMnS	přijmout
František	František	k1gMnSc1	František
I.	I.	kA	I.
císařský	císařský	k2eAgInSc4d1	císařský
titul	titul	k1gInSc4	titul
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
bezejmenný	bezejmenný	k2eAgInSc1d1	bezejmenný
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
přeměnil	přeměnit	k5eAaPmAgInS	přeměnit
na	na	k7c4	na
Rakouské	rakouský	k2eAgNnSc4d1	rakouské
císařství	císařství	k1gNnSc4	císařství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zrod	zrod	k1gInSc1	zrod
monarchie	monarchie	k1gFnSc2	monarchie
(	(	kIx(	(
<g/>
1526	[number]	k4	1526
<g/>
-	-	kIx~	-
<g/>
1597	[number]	k4	1597
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Ferdinandova	Ferdinandův	k2eAgFnSc1d1	Ferdinandova
unie	unie	k1gFnSc1	unie
a	a	k8xC	a
vyprovokování	vyprovokování	k1gNnSc1	vyprovokování
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
====	====	k?	====
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1515	[number]	k4	1515
byly	být	k5eAaImAgInP	být
mezi	mezi	k7c7	mezi
Habsburky	Habsburk	k1gInPc7	Habsburk
a	a	k8xC	a
Jagellonci	Jagellonec	k1gInSc6	Jagellonec
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
vídeňské	vídeňský	k2eAgFnPc1d1	Vídeňská
smlouvy	smlouva	k1gFnPc1	smlouva
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
bylo	být	k5eAaImAgNnS	být
dojednání	dojednání	k1gNnSc1	dojednání
sňatku	sňatek	k1gInSc2	sňatek
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Jagellonskou	jagellonský	k2eAgFnSc7d1	Jagellonská
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
roku	rok	k1gInSc2	rok
1521	[number]	k4	1521
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byly	být	k5eAaImAgFnP	být
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
bruselskými	bruselský	k2eAgMnPc7d1	bruselský
smlouvami	smlouva	k1gFnPc7	smlouva
přiřčeny	přiřčen	k2eAgFnPc1d1	přiřčena
dědičné	dědičný	k2eAgFnPc1d1	dědičná
habsburské	habsburský	k2eAgFnPc1d1	habsburská
země	zem	k1gFnPc1	zem
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgInPc1d1	dolní
a	a	k8xC	a
Horní	horní	k2eAgInPc1d1	horní
Rakousy	Rakousy	k1gInPc1	Rakousy
<g/>
,	,	kIx,	,
Štýrsko	Štýrsko	k1gNnSc1	Štýrsko
<g/>
,	,	kIx,	,
Korutany	Korutany	k1gInPc1	Korutany
<g/>
,	,	kIx,	,
Kraňsko	Kraňsko	k1gNnSc1	Kraňsko
a	a	k8xC	a
Tyrolsko	Tyrolsko	k1gNnSc1	Tyrolsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alsasko	Alsasko	k1gNnSc1	Alsasko
a	a	k8xC	a
Württembersko	Württembersko	k1gNnSc1	Württembersko
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
náhle	náhle	k6eAd1	náhle
zahynul	zahynout	k5eAaPmAgMnS	zahynout
Ludvík	Ludvík	k1gMnSc1	Ludvík
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
uplatnit	uplatnit	k5eAaPmF	uplatnit
dědické	dědický	k2eAgNnSc4d1	dědické
právo	právo	k1gNnSc4	právo
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
na	na	k7c4	na
Korunu	koruna	k1gFnSc4	koruna
českou	český	k2eAgFnSc4d1	Česká
a	a	k8xC	a
Uherské	uherský	k2eAgNnSc1d1	Uherské
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
byly	být	k5eAaImAgInP	být
Ferdinandovy	Ferdinandův	k2eAgInPc1d1	Ferdinandův
dědické	dědický	k2eAgInPc1d1	dědický
nároky	nárok	k1gInPc1	nárok
odmítnuty	odmítnut	k2eAgInPc1d1	odmítnut
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
přivtělených	přivtělený	k2eAgFnPc6d1	přivtělená
zemích	zem	k1gFnPc6	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
dědický	dědický	k2eAgInSc4d1	dědický
nárok	nárok	k1gInSc4	nárok
uznán	uznán	k2eAgMnSc1d1	uznán
byl	být	k5eAaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Obdobný	obdobný	k2eAgInSc4d1	obdobný
spor	spor	k1gInSc4	spor
o	o	k7c4	o
uznání	uznání	k1gNnSc4	uznání
Ferdinandova	Ferdinandův	k2eAgInSc2d1	Ferdinandův
nároku	nárok	k1gInSc2	nárok
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
i	i	k9	i
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
názorové	názorový	k2eAgNnSc1d1	názorové
rozštěpení	rozštěpení	k1gNnSc1	rozštěpení
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
ustavení	ustavení	k1gNnSc3	ustavení
dvojvládí	dvojvládí	k1gNnSc2	dvojvládí
(	(	kIx(	(
<g/>
druhým	druhý	k4xOgMnSc7	druhý
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Jan	Jan	k1gMnSc1	Jan
Zápolský	Zápolský	k2eAgMnSc1d1	Zápolský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Ferdinandova	Ferdinandův	k2eAgFnSc1d1	Ferdinandova
snaha	snaha	k1gFnSc1	snaha
získat	získat	k5eAaPmF	získat
moc	moc	k1gFnSc4	moc
nad	nad	k7c7	nad
celými	celý	k2eAgFnPc7d1	celá
Uhrami	Uhry	k1gFnPc7	Uhry
vojensky	vojensky	k6eAd1	vojensky
vedla	vést	k5eAaImAgFnS	vést
nejen	nejen	k6eAd1	nejen
k	k	k7c3	k
rozštěpení	rozštěpení	k1gNnSc3	rozštěpení
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
k	k	k7c3	k
vyprovokování	vyprovokování	k1gNnSc3	vyprovokování
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
armáda	armáda	k1gFnSc1	armáda
v	v	k7c6	v
nastalé	nastalý	k2eAgFnSc6d1	nastalá
válce	válka	k1gFnSc6	válka
nejen	nejen	k6eAd1	nejen
pomohla	pomoct	k5eAaPmAgFnS	pomoct
Zápolskému	Zápolský	k2eAgInSc3d1	Zápolský
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
tureckým	turecký	k2eAgMnSc7d1	turecký
vazalem	vazal	k1gMnSc7	vazal
<g/>
)	)	kIx)	)
ovládnout	ovládnout	k5eAaPmF	ovládnout
Sedmihradsko	Sedmihradsko	k1gNnSc4	Sedmihradsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokonce	dokonce	k9	dokonce
roku	rok	k1gInSc2	rok
1529	[number]	k4	1529
oblehla	oblehnout	k5eAaPmAgFnS	oblehnout
Vídeň	Vídeň	k1gFnSc1	Vídeň
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1532	[number]	k4	1532
plenila	plenit	k5eAaImAgFnS	plenit
rakouské	rakouský	k2eAgFnPc4d1	rakouská
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Válečný	válečný	k2eAgInSc1d1	válečný
konflikt	konflikt	k1gInSc1	konflikt
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
po	po	k7c4	po
Zápolského	Zápolský	k2eAgMnSc4d1	Zápolský
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
nakonec	nakonec	k6eAd1	nakonec
Turci	Turek	k1gMnPc1	Turek
roku	rok	k1gInSc2	rok
1541	[number]	k4	1541
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Budín	Budín	k1gInSc4	Budín
a	a	k8xC	a
zřídili	zřídit	k5eAaPmAgMnP	zřídit
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
Uher	Uher	k1gMnSc1	Uher
Budínský	Budínský	k1gMnSc1	Budínský
vilájet	vilájet	k5eAaImF	vilájet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sedmihradsku	Sedmihradsko	k1gNnSc6	Sedmihradsko
vládl	vládnout	k5eAaImAgMnS	vládnout
turecký	turecký	k2eAgMnSc1d1	turecký
vazal	vazal	k1gMnSc1	vazal
Jan	Jan	k1gMnSc1	Jan
Zikmund	Zikmund	k1gMnSc1	Zikmund
Zápolský	Zápolský	k2eAgMnSc1d1	Zápolský
a	a	k8xC	a
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
nakonec	nakonec	k6eAd1	nakonec
zbyly	zbýt	k5eAaPmAgInP	zbýt
pouze	pouze	k6eAd1	pouze
Královské	královský	k2eAgNnSc1d1	královské
Uhersko	Uhersko	k1gNnSc1	Uhersko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Myšlenkový	myšlenkový	k2eAgInSc4d1	myšlenkový
svět	svět	k1gInSc4	svět
za	za	k7c2	za
prvních	první	k4xOgInPc2	první
Habsburků	Habsburk	k1gInPc2	Habsburk
====	====	k?	====
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
ovšem	ovšem	k9	ovšem
výrazně	výrazně	k6eAd1	výrazně
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
Ferdinandovy	Ferdinandův	k2eAgInPc4d1	Ferdinandův
finanční	finanční	k2eAgInPc4d1	finanční
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
stavy	stav	k1gInPc4	stav
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
nelíbilo	líbit	k5eNaImAgNnS	líbit
zejména	zejména	k9	zejména
českým	český	k2eAgMnPc3d1	český
stavům	stav	k1gInPc3	stav
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
představovaly	představovat	k5eAaImAgInP	představovat
největší	veliký	k2eAgFnSc4d3	veliký
opozici	opozice	k1gFnSc4	opozice
krále	král	k1gMnSc2	král
v	v	k7c6	v
monarchii	monarchie	k1gFnSc6	monarchie
<g/>
,	,	kIx,	,
obviňovaly	obviňovat	k5eAaImAgFnP	obviňovat
krále	král	k1gMnSc4	král
z	z	k7c2	z
vyprovokování	vyprovokování	k1gNnSc2	vyprovokování
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
a	a	k8xC	a
nebyly	být	k5eNaImAgFnP	být
příliš	příliš	k6eAd1	příliš
ochotny	ochoten	k2eAgFnPc1d1	ochotna
tuto	tento	k3xDgFnSc4	tento
"	"	kIx"	"
<g/>
soukromou	soukromý	k2eAgFnSc4d1	soukromá
záležitost	záležitost	k1gFnSc4	záležitost
krále	král	k1gMnSc2	král
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
"	"	kIx"	"
vojensky	vojensky	k6eAd1	vojensky
či	či	k8xC	či
finančně	finančně	k6eAd1	finančně
podporovat	podporovat	k5eAaImF	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Českým	český	k2eAgInPc3d1	český
stavům	stav	k1gInPc3	stav
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
také	také	k9	také
příčily	příčit	k5eAaImAgFnP	příčit
centralizační	centralizační	k2eAgFnPc1d1	centralizační
a	a	k8xC	a
integrační	integrační	k2eAgFnPc1d1	integrační
snahy	snaha	k1gFnPc1	snaha
a	a	k8xC	a
úsilí	úsilí	k1gNnSc1	úsilí
o	o	k7c4	o
posílení	posílení	k1gNnSc4	posílení
moci	moc	k1gFnSc2	moc
krále	král	k1gMnSc2	král
či	či	k8xC	či
rakouských	rakouský	k2eAgMnPc2d1	rakouský
stavů	stav	k1gInPc2	stav
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
<g/>
Avšak	avšak	k8xC	avšak
ani	ani	k8xC	ani
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
částech	část	k1gFnPc6	část
monarchie	monarchie	k1gFnSc2	monarchie
nebylo	být	k5eNaImAgNnS	být
postavení	postavení	k1gNnSc1	postavení
katolického	katolický	k2eAgMnSc2d1	katolický
krále	král	k1gMnSc2	král
jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Ferdinandova	Ferdinandův	k2eAgNnSc2d1	Ferdinandovo
panství	panství	k1gNnSc2	panství
se	se	k3xPyFc4	se
šířila	šířit	k5eAaImAgFnS	šířit
reformace	reformace	k1gFnSc1	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Luteránství	luteránství	k1gNnSc1	luteránství
bylo	být	k5eAaImAgNnS	být
přijímáno	přijímat	k5eAaImNgNnS	přijímat
zejména	zejména	k9	zejména
v	v	k7c6	v
Rakousích	Rakousy	k1gInPc6	Rakousy
<g/>
,	,	kIx,	,
Slezsku	Slezsko	k1gNnSc6	Slezsko
a	a	k8xC	a
mezi	mezi	k7c7	mezi
českými	český	k2eAgMnPc7d1	český
a	a	k8xC	a
uherskými	uherský	k2eAgMnPc7d1	uherský
Němci	Němec	k1gMnPc7	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Kalvinismus	kalvinismus	k1gInSc1	kalvinismus
získával	získávat	k5eAaImAgInS	získávat
své	svůj	k3xOyFgMnPc4	svůj
stoupence	stoupenec	k1gMnPc4	stoupenec
zejména	zejména	k9	zejména
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
a	a	k8xC	a
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
Jednota	jednota	k1gFnSc1	jednota
bratrská	bratrský	k2eAgFnSc1d1	bratrská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
převládal	převládat	k5eAaImAgInS	převládat
utrakvismus	utrakvismus	k1gInSc1	utrakvismus
<g/>
,	,	kIx,	,
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
zas	zas	k6eAd1	zas
proudili	proudit	k5eAaPmAgMnP	proudit
novokřtěnci	novokřtěnec	k1gMnPc1	novokřtěnec
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
těchto	tento	k3xDgInPc2	tento
hlavních	hlavní	k2eAgInPc2d1	hlavní
proudů	proud	k1gInPc2	proud
vznikaly	vznikat	k5eAaImAgInP	vznikat
v	v	k7c4	v
monarchii	monarchie	k1gFnSc4	monarchie
četné	četný	k2eAgFnSc2d1	četná
sekty	sekta	k1gFnSc2	sekta
unitářů	unitář	k1gMnPc2	unitář
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
Sedmihradsku	Sedmihradsko	k1gNnSc6	Sedmihradsko
<g/>
)	)	kIx)	)
či	či	k8xC	či
nonkonformistů	nonkonformista	k1gMnPc2	nonkonformista
<g/>
.	.	kIx.	.
<g/>
Vzestup	vzestup	k1gInSc1	vzestup
protestantství	protestantství	k1gNnSc1	protestantství
doprovázel	doprovázet	k5eAaImAgInS	doprovázet
úpadek	úpadek	k1gInSc4	úpadek
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
katolické	katolický	k2eAgFnSc2d1	katolická
instituce	instituce	k1gFnSc2	instituce
se	se	k3xPyFc4	se
uchovaly	uchovat	k5eAaPmAgInP	uchovat
jen	jen	k9	jen
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nejednotné	jednotný	k2eNgFnSc2d1	nejednotná
evangelické	evangelický	k2eAgFnSc2d1	evangelická
církve	církev	k1gFnSc2	církev
nebyly	být	k5eNaImAgFnP	být
většinou	většinou	k6eAd1	většinou
schopny	schopen	k2eAgFnPc1d1	schopna
nahradit	nahradit	k5eAaPmF	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
cenzura	cenzura	k1gFnSc1	cenzura
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
značně	značně	k6eAd1	značně
neúčinná	účinný	k2eNgFnSc1d1	neúčinná
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
evangelická	evangelický	k2eAgFnSc1d1	evangelická
náboženská	náboženský	k2eAgFnSc1d1	náboženská
literatura	literatura	k1gFnSc1	literatura
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
Bible	bible	k1gFnSc1	bible
kralická	kralický	k2eAgFnSc1d1	Kralická
a	a	k8xC	a
maďarské	maďarský	k2eAgInPc1d1	maďarský
překlady	překlad	k1gInPc1	překlad
Bible	bible	k1gFnSc2	bible
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soustátí	soustátí	k1gNnSc6	soustátí
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
humanismus	humanismus	k1gInSc4	humanismus
a	a	k8xC	a
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
místními	místní	k2eAgMnPc7d1	místní
katolickými	katolický	k2eAgMnPc7d1	katolický
biskupy	biskup	k1gMnPc7	biskup
a	a	k8xC	a
papežem	papež	k1gMnSc7	papež
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozvolňoval	rozvolňovat	k5eAaImAgMnS	rozvolňovat
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
potlačení	potlačení	k1gNnSc4	potlačení
odboje	odboj	k1gInSc2	odboj
českých	český	k2eAgInPc2d1	český
stavů	stav	k1gInPc2	stav
roku	rok	k1gInSc2	rok
1547	[number]	k4	1547
(	(	kIx(	(
<g/>
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
náboženství	náboženství	k1gNnSc1	náboženství
ostatně	ostatně	k6eAd1	ostatně
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
habsburská	habsburský	k2eAgFnSc1d1	habsburská
monarchie	monarchie	k1gFnSc1	monarchie
relativně	relativně	k6eAd1	relativně
nábožensky	nábožensky	k6eAd1	nábožensky
tolerantní	tolerantní	k2eAgMnSc1d1	tolerantní
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
ostatně	ostatně	k6eAd1	ostatně
ani	ani	k8xC	ani
neměli	mít	k5eNaImAgMnP	mít
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c4	mnoho
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
reformaci	reformace	k1gFnSc4	reformace
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
mnohdy	mnohdy	k6eAd1	mnohdy
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
panovník	panovník	k1gMnSc1	panovník
ani	ani	k8xC	ani
neměl	mít	k5eNaImAgMnS	mít
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
vůli	vůle	k1gFnSc4	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
vůči	vůči	k7c3	vůči
protestantismu	protestantismus	k1gInSc3	protestantismus
velmi	velmi	k6eAd1	velmi
tolerantní	tolerantní	k2eAgMnSc1d1	tolerantní
a	a	k8xC	a
pravověrným	pravověrný	k2eAgMnSc7d1	pravověrný
katolíkem	katolík	k1gMnSc7	katolík
nebyl	být	k5eNaImAgMnS	být
rozhodně	rozhodně	k6eAd1	rozhodně
ani	ani	k8xC	ani
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
než	než	k8xS	než
k	k	k7c3	k
teologii	teologie	k1gFnSc3	teologie
<g/>
,	,	kIx,	,
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
přírodní	přírodní	k2eAgFnPc4d1	přírodní
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc3	umění
<g/>
,	,	kIx,	,
astrologii	astrologie	k1gFnSc3	astrologie
a	a	k8xC	a
alchymii	alchymie	k1gFnSc3	alchymie
<g/>
.	.	kIx.	.
</s>
<s>
Humanismus	humanismus	k1gInSc1	humanismus
Habsburků	Habsburk	k1gMnPc2	Habsburk
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
smíšený	smíšený	k2eAgMnSc1d1	smíšený
s	s	k7c7	s
magií	magie	k1gFnSc7	magie
zatlačoval	zatlačovat	k5eAaImAgMnS	zatlačovat
náboženství	náboženství	k1gNnSc4	náboženství
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Duchovnímu	duchovní	k2eAgInSc3d1	duchovní
světu	svět	k1gInSc3	svět
vládl	vládnout	k5eAaImAgInS	vládnout
manýrismus	manýrismus	k1gInSc1	manýrismus
<g/>
,	,	kIx,	,
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
spojené	spojený	k2eAgNnSc1d1	spojené
s	s	k7c7	s
nervozitou	nervozita	k1gFnSc7	nervozita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Krize	krize	k1gFnSc1	krize
monarchie	monarchie	k1gFnSc2	monarchie
(	(	kIx(	(
<g/>
1597	[number]	k4	1597
<g/>
-	-	kIx~	-
<g/>
1648	[number]	k4	1648
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Eskalace	eskalace	k1gFnSc1	eskalace
napětí	napětí	k1gNnSc2	napětí
====	====	k?	====
</s>
</p>
<p>
<s>
Rudolfův	Rudolfův	k2eAgInSc1d1	Rudolfův
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
esoterismus	esoterismus	k1gInSc4	esoterismus
se	se	k3xPyFc4	se
však	však	k9	však
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
duševní	duševní	k2eAgFnSc7d1	duševní
chorobou	choroba	k1gFnSc7	choroba
a	a	k8xC	a
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c4	v
neschopnost	neschopnost	k1gFnSc4	neschopnost
vládnout	vládnout	k5eAaImF	vládnout
a	a	k8xC	a
následnou	následný	k2eAgFnSc4d1	následná
politickou	politický	k2eAgFnSc4d1	politická
krizi	krize	k1gFnSc4	krize
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Válku	válka	k1gFnSc4	válka
s	s	k7c7	s
Turky	turek	k1gInPc7	turek
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zuřila	zuřit	k5eAaImAgFnS	zuřit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1593	[number]	k4	1593
<g/>
-	-	kIx~	-
<g/>
1606	[number]	k4	1606
<g/>
,	,	kIx,	,
a	a	k8xC	a
zvládnutí	zvládnutí	k1gNnSc1	zvládnutí
řady	řada	k1gFnSc2	řada
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
(	(	kIx(	(
<g/>
Zikmund	Zikmund	k1gMnSc1	Zikmund
Báthory	Báthora	k1gFnSc2	Báthora
<g/>
,	,	kIx,	,
Štěpán	Štěpán	k1gMnSc1	Štěpán
Bočkaj	Bočkaj	k1gMnSc1	Bočkaj
<g/>
)	)	kIx)	)
přenechával	přenechávat	k5eAaImAgMnS	přenechávat
jiným	jiná	k1gFnPc3	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Rudolfův	Rudolfův	k2eAgMnSc1d1	Rudolfův
bratr	bratr	k1gMnSc1	bratr
Matyáš	Matyáš	k1gMnSc1	Matyáš
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
situaci	situace	k1gFnSc4	situace
vzít	vzít	k5eAaPmF	vzít
do	do	k7c2	do
vlastních	vlastní	k2eAgFnPc2d1	vlastní
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
podporu	podpora	k1gFnSc4	podpora
všech	všecek	k3xTgMnPc2	všecek
stavů	stav	k1gInPc2	stav
krom	krom	k7c2	krom
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1608	[number]	k4	1608
donutil	donutit	k5eAaPmAgInS	donutit
Rudolfa	Rudolf	k1gMnSc4	Rudolf
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
přenechal	přenechat	k5eAaPmAgMnS	přenechat
vládu	vláda	k1gFnSc4	vláda
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
nakonec	nakonec	k6eAd1	nakonec
ztratil	ztratit	k5eAaPmAgMnS	ztratit
podporu	podpora	k1gFnSc4	podpora
i	i	k9	i
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1611	[number]	k4	1611
přinucen	přinucen	k2eAgInSc1d1	přinucen
k	k	k7c3	k
abdikaci	abdikace	k1gFnSc3	abdikace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
monarchii	monarchie	k1gFnSc4	monarchie
vládl	vládnout	k5eAaImAgMnS	vládnout
zase	zase	k9	zase
jediný	jediný	k2eAgMnSc1d1	jediný
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
Matyáš	Matyáš	k1gMnSc1	Matyáš
(	(	kIx(	(
<g/>
jen	jen	k9	jen
<g/>
.	.	kIx.	.
<g/>
Tím	ten	k3xDgNnSc7	ten
však	však	k8xC	však
krize	krize	k1gFnSc1	krize
neskončila	skončit	k5eNaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
uvedl	uvést	k5eAaPmAgMnS	uvést
do	do	k7c2	do
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
jezuity	jezuita	k1gMnPc4	jezuita
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
obnovit	obnovit	k5eAaPmF	obnovit
ztracené	ztracený	k2eAgFnPc4d1	ztracená
pozice	pozice	k1gFnPc4	pozice
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Odpor	odpor	k1gInSc1	odpor
vůči	vůči	k7c3	vůči
jezuitům	jezuita	k1gMnPc3	jezuita
panoval	panovat	k5eAaImAgInS	panovat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
vrstvách	vrstva	k1gFnPc6	vrstva
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
Tovaryšstvo	tovaryšstvo	k1gNnSc1	tovaryšstvo
však	však	k9	však
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
obratně	obratně	k6eAd1	obratně
využívat	využívat	k5eAaImF	využívat
sporů	spor	k1gInPc2	spor
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
mocenskými	mocenský	k2eAgFnPc7d1	mocenská
skupinami	skupina	k1gFnPc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
trpělivému	trpělivý	k2eAgNnSc3d1	trpělivé
obratnému	obratný	k2eAgNnSc3d1	obratné
manévrování	manévrování	k1gNnSc3	manévrování
a	a	k8xC	a
propracovanému	propracovaný	k2eAgInSc3d1	propracovaný
systému	systém	k1gInSc3	systém
jezuitského	jezuitský	k2eAgNnSc2d1	jezuitské
školství	školství	k1gNnSc2	školství
si	se	k3xPyFc3	se
nakonec	nakonec	k6eAd1	nakonec
jezuité	jezuita	k1gMnPc1	jezuita
vychovali	vychovat	k5eAaPmAgMnP	vychovat
skupinu	skupina	k1gFnSc4	skupina
vlivných	vlivný	k2eAgMnPc2d1	vlivný
katolických	katolický	k2eAgMnPc2d1	katolický
radikálů	radikál	k1gMnPc2	radikál
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
znovu	znovu	k6eAd1	znovu
vynořil	vynořit	k5eAaPmAgInS	vynořit
mocenský	mocenský	k2eAgInSc1d1	mocenský
spor	spor	k1gInSc1	spor
mezi	mezi	k7c7	mezi
králem	král	k1gMnSc7	král
a	a	k8xC	a
stavy	stav	k1gInPc7	stav
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nakonec	nakonec	k6eAd1	nakonec
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
náboženským	náboženský	k2eAgNnSc7d1	náboženské
pnutím	pnutí	k1gNnSc7	pnutí
propukl	propuknout	k5eAaPmAgInS	propuknout
roku	rok	k1gInSc2	rok
1618	[number]	k4	1618
v	v	k7c6	v
povstání	povstání	k1gNnSc6	povstání
českých	český	k2eAgInPc2d1	český
stavů	stav	k1gInPc2	stav
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
přidaly	přidat	k5eAaPmAgInP	přidat
také	také	k9	také
stavy	stav	k1gInPc1	stav
slezské	slezský	k2eAgInPc1d1	slezský
<g/>
,	,	kIx,	,
moravské	moravský	k2eAgInPc1d1	moravský
<g/>
,	,	kIx,	,
rakouské	rakouský	k2eAgInPc1d1	rakouský
i	i	k8xC	i
uherské	uherský	k2eAgInPc1d1	uherský
<g/>
.	.	kIx.	.
</s>
<s>
Novému	nový	k2eAgMnSc3d1	nový
králi	král	k1gMnSc3	král
Ferdinandu	Ferdinand	k1gMnSc3	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
povstání	povstání	k1gNnSc1	povstání
podařilo	podařit	k5eAaPmAgNnS	podařit
zpacifikovat	zpacifikovat	k5eAaPmF	zpacifikovat
<g/>
,	,	kIx,	,
konflikt	konflikt	k1gInSc1	konflikt
však	však	k9	však
přerostl	přerůst	k5eAaPmAgInS	přerůst
do	do	k7c2	do
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vítězství	vítězství	k1gNnSc1	vítězství
"	"	kIx"	"
<g/>
pietas	pietas	k1gMnSc1	pietas
Austriaca	Austriaca	k1gMnSc1	Austriaca
<g/>
"	"	kIx"	"
====	====	k?	====
</s>
</p>
<p>
<s>
Válečné	válečný	k2eAgNnSc1d1	válečné
řešení	řešení	k1gNnSc1	řešení
náboženského	náboženský	k2eAgInSc2d1	náboženský
konfliktu	konflikt	k1gInSc2	konflikt
vložilo	vložit	k5eAaPmAgNnS	vložit
mezi	mezi	k7c4	mezi
katolickou	katolický	k2eAgFnSc4d1	katolická
víru	víra	k1gFnSc4	víra
a	a	k8xC	a
loajalitu	loajalita	k1gFnSc4	loajalita
panovníkovi	panovník	k1gMnSc3	panovník
rovnítko	rovnítko	k1gNnSc4	rovnítko
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znemožnilo	znemožnit	k5eAaPmAgNnS	znemožnit
další	další	k2eAgFnSc4d1	další
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
toleranci	tolerance	k1gFnSc4	tolerance
<g/>
.	.	kIx.	.
</s>
<s>
Kalvinisté	kalvinista	k1gMnPc1	kalvinista
i	i	k8xC	i
luteráni	luterán	k1gMnPc1	luterán
byli	být	k5eAaImAgMnP	být
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
vyháněni	vyháněn	k2eAgMnPc1d1	vyháněn
a	a	k8xC	a
provinilá	provinilý	k2eAgFnSc1d1	provinilá
evangelická	evangelický	k2eAgFnSc1d1	evangelická
šlechta	šlechta	k1gFnSc1	šlechta
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
vítěznému	vítězný	k2eAgInSc3d1	vítězný
katolicismu	katolicismus	k1gInSc3	katolicismus
podřídit	podřídit	k5eAaPmF	podřídit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
opustit	opustit	k5eAaPmF	opustit
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
ideologie	ideologie	k1gFnSc1	ideologie
zdůrazňující	zdůrazňující	k2eAgFnSc1d1	zdůrazňující
zbožnost	zbožnost	k1gFnSc1	zbožnost
habsburského	habsburský	k2eAgInSc2d1	habsburský
rodu	rod	k1gInSc2	rod
(	(	kIx(	(
<g/>
pietas	pietas	k1gMnSc1	pietas
Austriaca	Austriaca	k1gMnSc1	Austriaca
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
soulad	soulad	k1gInSc4	soulad
mezi	mezi	k7c7	mezi
dynastií	dynastie	k1gFnSc7	dynastie
a	a	k8xC	a
církví	církev	k1gFnPc2	církev
byl	být	k5eAaImAgInS	být
jen	jen	k9	jen
klam	klam	k1gInSc1	klam
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Habsburkové	Habsburk	k1gMnPc1	Habsburk
nebyli	být	k5eNaImAgMnP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
posílit	posílit	k5eAaPmF	posílit
moc	moc	k1gFnSc4	moc
církve	církev	k1gFnSc2	církev
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
oslabení	oslabení	k1gNnSc4	oslabení
moci	moct	k5eAaImF	moct
své	své	k1gNnSc4	své
<g/>
.	.	kIx.	.
<g/>
Porážka	porážka	k1gFnSc1	porážka
protestantismu	protestantismus	k1gInSc2	protestantismus
mnohé	mnohý	k2eAgMnPc4d1	mnohý
evangelíky	evangelík	k1gMnPc4	evangelík
zviklala	zviklat	k5eAaPmAgFnS	zviklat
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
přesvědčení	přesvědčení	k1gNnSc6	přesvědčení
o	o	k7c6	o
správnosti	správnost	k1gFnSc6	správnost
jejich	jejich	k3xOp3gFnSc2	jejich
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
také	také	k9	také
opětovné	opětovný	k2eAgInPc4d1	opětovný
útoky	útok	k1gInPc4	útok
Turků	turek	k1gInPc2	turek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
vykládány	vykládat	k5eAaImNgInP	vykládat
jako	jako	k8xC	jako
Boží	boží	k2eAgInSc1d1	boží
trest	trest	k1gInSc1	trest
za	za	k7c4	za
protestantismus	protestantismus	k1gInSc4	protestantismus
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgInS	vést
mnohé	mnohé	k1gNnSc4	mnohé
ke	k	k7c3	k
konverzi	konverze	k1gFnSc3	konverze
ke	k	k7c3	k
katolicismu	katolicismus	k1gInSc3	katolicismus
<g/>
,	,	kIx,	,
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
k	k	k7c3	k
horlivé	horlivý	k2eAgFnSc3d1	horlivá
protireformaci	protireformace	k1gFnSc3	protireformace
<g/>
.	.	kIx.	.
</s>
<s>
Katolicismus	katolicismus	k1gInSc1	katolicismus
navíc	navíc	k6eAd1	navíc
nyní	nyní	k6eAd1	nyní
představoval	představovat	k5eAaImAgMnS	představovat
jednotu	jednota	k1gFnSc4	jednota
a	a	k8xC	a
řád	řád	k1gInSc4	řád
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
"	"	kIx"	"
<g/>
novátorskému	novátorský	k2eAgMnSc3d1	novátorský
a	a	k8xC	a
destruktivnímu	destruktivní	k2eAgMnSc3d1	destruktivní
<g/>
"	"	kIx"	"
protestantismu	protestantismus	k1gInSc3	protestantismus
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
však	však	k9	však
stále	stále	k6eAd1	stále
zuřila	zuřit	k5eAaImAgFnS	zuřit
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
podepsala	podepsat	k5eAaPmAgFnS	podepsat
i	i	k9	i
na	na	k7c6	na
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
situaci	situace	k1gFnSc6	situace
v	v	k7c6	v
habsburské	habsburský	k2eAgFnSc6d1	habsburská
monarchii	monarchie	k1gFnSc6	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Poddaní	poddaný	k1gMnPc1	poddaný
byli	být	k5eAaImAgMnP	být
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
znevolňováni	znevolňován	k2eAgMnPc1d1	znevolňován
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
nevolnickým	nevolnický	k2eAgNnPc3d1	nevolnické
povstáním	povstání	k1gNnPc3	povstání
a	a	k8xC	a
zbojnictví	zbojnictví	k1gNnPc2	zbojnictví
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
a	a	k8xC	a
náboženská	náboženský	k2eAgFnSc1d1	náboženská
intolerance	intolerance	k1gFnSc1	intolerance
vedla	vést	k5eAaImAgFnS	vést
také	také	k9	také
k	k	k7c3	k
úpadku	úpadek	k1gInSc3	úpadek
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
,	,	kIx,	,
cenzuře	cenzura	k1gFnSc3	cenzura
<g/>
,	,	kIx,	,
pálení	pálení	k1gNnSc6	pálení
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konsolidace	konsolidace	k1gFnSc2	konsolidace
(	(	kIx(	(
<g/>
1648	[number]	k4	1648
<g/>
-	-	kIx~	-
<g/>
1700	[number]	k4	1700
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
Leopold	Leopold	k1gMnSc1	Leopold
I.	I.	kA	I.
ideálu	ideál	k1gInSc2	ideál
zbožnosti	zbožnost	k1gFnSc2	zbožnost
odpovídali	odpovídat	k5eAaImAgMnP	odpovídat
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
intolerancí	intolerance	k1gFnSc7	intolerance
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
však	však	k9	však
rekatolizace	rekatolizace	k1gFnSc1	rekatolizace
neobešla	obešnout	k5eNaPmAgFnS	obešnout
ani	ani	k8xC	ani
po	po	k7c6	po
třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
bez	bez	k7c2	bez
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
Habsburkové	Habsburk	k1gMnPc1	Habsburk
přivést	přivést	k5eAaPmF	přivést
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
ke	k	k7c3	k
konverzi	konverze	k1gFnSc3	konverze
bez	bez	k7c2	bez
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
škod	škoda	k1gFnPc2	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Rekatolizace	rekatolizace	k1gFnSc1	rekatolizace
proto	proto	k8xC	proto
probíhala	probíhat	k5eAaImAgFnS	probíhat
pomocí	pomocí	k7c2	pomocí
směsi	směs	k1gFnSc2	směs
zákazů	zákaz	k1gInPc2	zákaz
a	a	k8xC	a
lákadel	lákadlo	k1gNnPc2	lákadlo
<g/>
.	.	kIx.	.
</s>
<s>
Panovníci	panovník	k1gMnPc1	panovník
využívali	využívat	k5eAaPmAgMnP	využívat
katolické	katolický	k2eAgFnPc4d1	katolická
církve	církev	k1gFnPc4	církev
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
a	a	k8xC	a
posílení	posílení	k1gNnSc3	posílení
svých	svůj	k3xOyFgFnPc2	svůj
pozic	pozice	k1gFnPc2	pozice
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
ovšem	ovšem	k9	ovšem
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
chtěli	chtít	k5eAaImAgMnP	chtít
mít	mít	k5eAaImF	mít
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
vstupováním	vstupování	k1gNnSc7	vstupování
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
loajální	loajální	k2eAgFnSc2d1	loajální
šlechty	šlechta	k1gFnSc2	šlechta
mezi	mezi	k7c4	mezi
duchovenstvo	duchovenstvo	k1gNnSc4	duchovenstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Panovnická	panovnický	k2eAgFnSc1d1	panovnická
dynastie	dynastie	k1gFnSc1	dynastie
postupně	postupně	k6eAd1	postupně
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
státní	státní	k2eAgInSc4d1	státní
byrokratický	byrokratický	k2eAgInSc4d1	byrokratický
aparát	aparát	k1gInSc4	aparát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
však	však	k9	však
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
ještě	ještě	k9	ještě
nebyly	být	k5eNaImAgFnP	být
zcela	zcela	k6eAd1	zcela
vyjasněny	vyjasněn	k2eAgFnPc1d1	vyjasněna
kompetence	kompetence	k1gFnPc1	kompetence
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
úřady	úřad	k1gInPc7	úřad
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
častým	častý	k2eAgFnPc3d1	častá
neshodám	neshoda	k1gFnPc3	neshoda
<g/>
.	.	kIx.	.
<g/>
Pozice	pozice	k1gFnSc1	pozice
Habsburků	Habsburk	k1gMnPc2	Habsburk
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
upevnila	upevnit	k5eAaPmAgFnS	upevnit
také	také	k9	také
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1683	[number]	k4	1683
zahájila	zahájit	k5eAaPmAgFnS	zahájit
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
další	další	k2eAgFnSc4d1	další
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
byla	být	k5eAaImAgFnS	být
dokonce	dokonce	k9	dokonce
znovu	znovu	k6eAd1	znovu
obležena	obležen	k2eAgFnSc1d1	obležen
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
díky	díky	k7c3	díky
výrazné	výrazný	k2eAgFnSc3d1	výrazná
vojenské	vojenský	k2eAgFnSc3d1	vojenská
pomoci	pomoc	k1gFnSc3	pomoc
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
polského	polský	k2eAgMnSc2d1	polský
krále	král	k1gMnSc2	král
Jana	Jan	k1gMnSc2	Jan
Sobieského	Sobieský	k2eAgMnSc2d1	Sobieský
byli	být	k5eAaImAgMnP	být
Turci	Turek	k1gMnPc1	Turek
zahnáni	zahnat	k5eAaPmNgMnP	zahnat
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc2	tento
situace	situace	k1gFnSc2	situace
využili	využít	k5eAaPmAgMnP	využít
Habsburkové	Habsburk	k1gMnPc1	Habsburk
a	a	k8xC	a
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
s	s	k7c7	s
Benátkami	Benátky	k1gFnPc7	Benátky
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
Svatou	svatý	k2eAgFnSc7d1	svatá
ligu	liga	k1gFnSc4	liga
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
vytlačit	vytlačit	k5eAaPmF	vytlačit
Turky	Turek	k1gMnPc4	Turek
z	z	k7c2	z
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1686	[number]	k4	1686
dobyl	dobýt	k5eAaPmAgMnS	dobýt
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
Lotrinský	lotrinský	k2eAgInSc4d1	lotrinský
Budín	Budín	k1gInSc4	Budín
a	a	k8xC	a
po	po	k7c4	po
vítězství	vítězství	k1gNnSc4	vítězství
u	u	k7c2	u
Nagyharsány	Nagyharsán	k2eAgFnPc1d1	Nagyharsán
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
Habsburkové	Habsburk	k1gMnPc1	Habsburk
získali	získat	k5eAaPmAgMnP	získat
část	část	k1gFnSc4	část
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
a	a	k8xC	a
Sedmihradska	Sedmihradsko	k1gNnSc2	Sedmihradsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
projevovat	projevovat	k5eAaImF	projevovat
krize	krize	k1gFnSc2	krize
a	a	k8xC	a
díky	díky	k7c3	díky
úspěchům	úspěch	k1gInPc3	úspěch
Evžena	Evžen	k1gMnSc2	Evžen
Savojského	savojský	k2eAgInSc2d1	savojský
byly	být	k5eAaImAgInP	být
k	k	k7c3	k
habsburské	habsburský	k2eAgFnSc3d1	habsburská
monarchii	monarchie	k1gFnSc3	monarchie
připojeny	připojen	k2eAgFnPc4d1	připojena
celé	celý	k2eAgFnPc4d1	celá
Uhry	Uhry	k1gFnPc4	Uhry
a	a	k8xC	a
Sedmihradsko	Sedmihradsko	k1gNnSc4	Sedmihradsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Španělské	španělský	k2eAgNnSc1d1	španělské
dědictví	dědictví	k1gNnSc1	dědictví
(	(	kIx(	(
<g/>
1700	[number]	k4	1700
<g/>
-	-	kIx~	-
<g/>
1740	[number]	k4	1740
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
tureckými	turecký	k2eAgFnPc7d1	turecká
válkami	válka	k1gFnPc7	válka
podporou	podpora	k1gFnSc7	podpora
protifrancouzských	protifrancouzský	k2eAgFnPc2d1	protifrancouzská
koalic	koalice	k1gFnPc2	koalice
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
<g/>
:	:	kIx,	:
účast	účast	k1gFnSc1	účast
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
o	o	k7c4	o
dědictví	dědictví	k1gNnSc4	dědictví
španělské	španělský	k2eAgNnSc4d1	španělské
po	po	k7c6	po
vymření	vymření	k1gNnSc6	vymření
habsburské	habsburský	k2eAgFnSc2d1	habsburská
španělské	španělský	k2eAgFnSc2d1	španělská
větve	větev	k1gFnSc2	větev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
císařským	císařský	k2eAgMnSc7d1	císařský
vojevůdcem	vojevůdce	k1gMnSc7	vojevůdce
přelomu	přelom	k1gInSc2	přelom
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
princ	princ	k1gMnSc1	princ
Evžen	Evžen	k1gMnSc1	Evžen
Savojský	savojský	k2eAgMnSc1d1	savojský
<g/>
.	.	kIx.	.
1705	[number]	k4	1705
-	-	kIx~	-
1711	[number]	k4	1711
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
</s>
</p>
<p>
<s>
Stavovské	stavovský	k2eAgInPc1d1	stavovský
zemské	zemský	k2eAgInPc1d1	zemský
sněmy	sněm	k1gInPc1	sněm
ztrácely	ztrácet	k5eAaImAgInP	ztrácet
pravomoce	pravomoc	k1gFnPc4	pravomoc
<g/>
,	,	kIx,	,
rostl	růst	k5eAaImAgInS	růst
význam	význam	k1gInSc1	význam
ústředních	ústřední	k2eAgInPc2d1	ústřední
orgánů	orgán	k1gInPc2	orgán
<g/>
:	:	kIx,	:
státní	státní	k2eAgFnSc1d1	státní
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
dvorská	dvorský	k2eAgFnSc1d1	dvorská
kancelář	kancelář	k1gFnSc1	kancelář
<g/>
,	,	kIx,	,
dvorská	dvorský	k2eAgFnSc1d1	dvorská
vojenská	vojenský	k2eAgFnSc1d1	vojenská
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
probíhaly	probíhat	k5eAaImAgFnP	probíhat
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
situace	situace	k1gFnSc2	situace
(	(	kIx(	(
<g/>
obava	obava	k1gFnSc1	obava
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Václav	Václav	k1gMnSc1	Václav
Vratislav	Vratislav	k1gMnSc1	Vratislav
z	z	k7c2	z
Mitrovic	Mitrovice	k1gFnPc2	Mitrovice
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Norbert	Norbert	k1gMnSc1	Norbert
Kinský	Kinský	k1gMnSc1	Kinský
</s>
</p>
<p>
<s>
===	===	k?	===
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
(	(	kIx(	(
<g/>
1740	[number]	k4	1740
<g/>
-	-	kIx~	-
<g/>
1780	[number]	k4	1780
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1756	[number]	k4	1756
bylo	být	k5eAaImAgNnS	být
Sasko	Sasko	k1gNnSc1	Sasko
napadeno	napadnout	k5eAaPmNgNnS	napadnout
Pruskem	Prusko	k1gNnSc7	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
habsburská	habsburský	k2eAgFnSc1d1	habsburská
panovnice	panovnice	k1gFnSc1	panovnice
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
v	v	k7c6	v
obavách	obava	k1gFnPc6	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
ohroženo	ohrozit	k5eAaPmNgNnS	ohrozit
i	i	k9	i
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Prusům	Prus	k1gMnPc3	Prus
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Osvícenství	osvícenství	k1gNnSc3	osvícenství
(	(	kIx(	(
<g/>
1780	[number]	k4	1780
<g/>
-	-	kIx~	-
<g/>
1792	[number]	k4	1792
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
Josef	Josefa	k1gFnPc2	Josefa
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
vášnivý	vášnivý	k2eAgMnSc1d1	vášnivý
reformátor	reformátor	k1gMnSc1	reformátor
<g/>
.	.	kIx.	.
</s>
<s>
Zrušil	zrušit	k5eAaPmAgMnS	zrušit
robotu	robota	k1gFnSc4	robota
<g/>
,	,	kIx,	,
zavedl	zavést	k5eAaPmAgInS	zavést
toleranční	toleranční	k2eAgInSc1d1	toleranční
patent	patent	k1gInSc1	patent
atd.	atd.	kA	atd.
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
vydal	vydat	k5eAaPmAgInS	vydat
přes	přes	k7c4	přes
6000	[number]	k4	6000
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
Leopold	Leopolda	k1gFnPc2	Leopolda
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
také	také	k9	také
reformovat	reformovat	k5eAaBmF	reformovat
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
dáno	dát	k5eAaPmNgNnS	dát
moc	moc	k6eAd1	moc
málo	málo	k1gNnSc1	málo
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Umírá	umírat	k5eAaImIp3nS	umírat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1792	[number]	k4	1792
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
jeho	jeho	k3xOp3gFnSc4	jeho
konzervativní	konzervativní	k2eAgMnSc1d1	konzervativní
syn	syn	k1gMnSc1	syn
František	František	k1gMnSc1	František
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
</s>
</p>
<p>
<s>
===	===	k?	===
Proměna	proměna	k1gFnSc1	proměna
v	v	k7c4	v
Rakouské	rakouský	k2eAgNnSc4d1	rakouské
císařství	císařství	k1gNnSc4	císařství
(	(	kIx(	(
<g/>
1792	[number]	k4	1792
<g/>
–	–	k?	–
<g/>
1804	[number]	k4	1804
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1792	[number]	k4	1792
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
revoluční	revoluční	k2eAgFnSc1d1	revoluční
Francie	Francie	k1gFnSc1	Francie
Františkovi	František	k1gMnSc3	František
II	II	kA	II
<g/>
.	.	kIx.	.
válku	válka	k1gFnSc4	válka
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
podporu	podpora	k1gFnSc4	podpora
royalistům	royalista	k1gMnPc3	royalista
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
spojilo	spojit	k5eAaPmAgNnS	spojit
s	s	k7c7	s
Pruskem	Prusko	k1gNnSc7	Prusko
<g/>
,	,	kIx,	,
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Sardinií	Sardinie	k1gFnSc7	Sardinie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
počátečních	počáteční	k2eAgInPc6d1	počáteční
úspěších	úspěch	k1gInPc6	úspěch
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
koalice	koalice	k1gFnSc1	koalice
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
prohry	prohra	k1gFnPc4	prohra
–	–	k?	–
Rakousko	Rakousko	k1gNnSc1	Rakousko
definitivně	definitivně	k6eAd1	definitivně
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
Rakouské	rakouský	k2eAgNnSc4d1	rakouské
Nizozemí	Nizozemí	k1gNnSc4	Nizozemí
a	a	k8xC	a
Lombardii	Lombardie	k1gFnSc4	Lombardie
<g/>
,	,	kIx,	,
podepsáním	podepsání	k1gNnSc7	podepsání
míru	mír	k1gInSc2	mír
roku	rok	k1gInSc2	rok
1797	[number]	k4	1797
však	však	k9	však
jako	jako	k9	jako
kompenzaci	kompenzace	k1gFnSc4	kompenzace
získalo	získat	k5eAaPmAgNnS	získat
část	část	k1gFnSc4	část
Benátska	Benátsk	k1gInSc2	Benátsk
s	s	k7c7	s
Istrií	Istrie	k1gFnSc7	Istrie
a	a	k8xC	a
Dalmácií	Dalmácie	k1gFnSc7	Dalmácie
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1799	[number]	k4	1799
se	se	k3xPyFc4	se
však	však	k9	však
rozpoutala	rozpoutat	k5eAaPmAgFnS	rozpoutat
další	další	k2eAgFnSc1d1	další
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Svatá	svatat	k5eAaImIp3nS	svatat
říše	říše	k1gFnSc1	říše
římská	římský	k2eAgFnSc1d1	římská
ztratila	ztratit	k5eAaPmAgFnS	ztratit
levobřežní	levobřežní	k2eAgNnSc4d1	levobřežní
Porýní	Porýní	k1gNnSc4	Porýní
i	i	k9	i
s	s	k7c7	s
katolickými	katolický	k2eAgNnPc7d1	katolické
knížectvími	knížectví	k1gNnPc7	knížectví
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
v	v	k7c6	v
Říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
získali	získat	k5eAaPmAgMnP	získat
navrch	navrch	k6eAd1	navrch
protestanští	protestanský	k2eAgMnPc1d1	protestanský
kurfiřti	kurfiřt	k1gMnPc1	kurfiřt
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
obavě	obava	k1gFnSc3	obava
ze	z	k7c2	z
ztráty	ztráta	k1gFnSc2	ztráta
císařského	císařský	k2eAgInSc2d1	císařský
titulu	titul	k1gInSc2	titul
František	František	k1gMnSc1	František
II	II	kA	II
<g/>
.	.	kIx.	.
ustavil	ustavit	k5eAaPmAgMnS	ustavit
roku	rok	k1gInSc2	rok
1804	[number]	k4	1804
nový	nový	k2eAgInSc1d1	nový
dědičný	dědičný	k2eAgInSc1d1	dědičný
titul	titul	k1gInSc1	titul
rakouského	rakouský	k2eAgMnSc2d1	rakouský
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
habsburská	habsburský	k2eAgFnSc1d1	habsburská
monarchie	monarchie	k1gFnSc1	monarchie
proměnila	proměnit	k5eAaPmAgFnS	proměnit
na	na	k7c4	na
Rakouské	rakouský	k2eAgNnSc4d1	rakouské
císařství	císařství	k1gNnSc4	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
římského	římský	k2eAgMnSc2d1	římský
císaře	císař	k1gMnSc2	císař
si	se	k3xPyFc3	se
rakouský	rakouský	k2eAgMnSc1d1	rakouský
panovník	panovník	k1gMnSc1	panovník
ponechal	ponechat	k5eAaPmAgMnS	ponechat
<g/>
,	,	kIx,	,
Napoleonovy	Napoleonův	k2eAgInPc1d1	Napoleonův
vojenské	vojenský	k2eAgInPc1d1	vojenský
úspěchy	úspěch	k1gInPc1	úspěch
jej	on	k3xPp3gInSc2	on
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
donutily	donutit	k5eAaPmAgFnP	donutit
tohoto	tento	k3xDgInSc2	tento
titulu	titul	k1gInSc2	titul
se	se	k3xPyFc4	se
vzdát	vzdát	k5eAaPmF	vzdát
a	a	k8xC	a
Svatou	svatý	k2eAgFnSc4d1	svatá
říši	říše	k1gFnSc4	říše
římskou	římský	k2eAgFnSc7d1	římská
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
bezmála	bezmála	k6eAd1	bezmála
tisícileté	tisíciletý	k2eAgFnSc6d1	tisíciletá
existenci	existence	k1gFnSc6	existence
rozpustit	rozpustit	k5eAaPmF	rozpustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
</s>
</p>
