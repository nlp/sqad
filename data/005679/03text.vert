<s>
Sarkofág	sarkofág	k1gInSc1	sarkofág
(	(	kIx(	(
<g/>
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
σ	σ	k?	σ
(	(	kIx(	(
<g/>
sarx	sarx	k1gInSc1	sarx
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
maso	maso	k1gNnSc1	maso
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
φ	φ	k?	φ
(	(	kIx(	(
<g/>
fagein	fagein	k1gMnSc1	fagein
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
požírat	požírat	k5eAaImF	požírat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
schránka	schránka	k1gFnSc1	schránka
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
uložení	uložení	k1gNnSc3	uložení
rakve	rakev	k1gFnSc2	rakev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
uvedl	uvést	k5eAaPmAgMnS	uvést
starověký	starověký	k2eAgMnSc1d1	starověký
řecký	řecký	k2eAgMnSc1d1	řecký
historik	historik	k1gMnSc1	historik
Hérodotos	Hérodotos	k1gMnSc1	Hérodotos
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgInPc1	první
sarkofágy	sarkofág	k1gInPc1	sarkofág
byly	být	k5eAaImAgInP	být
vytesané	vytesaný	k2eAgInPc1d1	vytesaný
ze	z	k7c2	z
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
typu	typ	k1gInSc2	typ
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
požíral	požírat	k5eAaImAgInS	požírat
tělo	tělo	k1gNnSc4	tělo
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
sarkofagos	sarkofagos	k1gMnSc1	sarkofagos
lithos	lithos	k1gMnSc1	lithos
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
kameny	kámen	k1gInPc4	kámen
požírající	požírající	k2eAgNnSc4d1	požírající
maso	maso	k1gNnSc4	maso
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
označovány	označovat	k5eAaImNgFnP	označovat
zejména	zejména	k9	zejména
schránky	schránka	k1gFnPc1	schránka
vyráběné	vyráběný	k2eAgFnPc1d1	vyráběná
z	z	k7c2	z
vápence	vápenec	k1gInSc2	vápenec
těženého	těžený	k2eAgInSc2d1	těžený
v	v	k7c6	v
Assu	Assus	k1gInSc6	Assus
v	v	k7c6	v
Troadě	Troada	k1gFnSc6	Troada
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
vztahovala	vztahovat	k5eAaImAgFnS	vztahovat
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
vápencové	vápencový	k2eAgFnPc4d1	vápencová
rakve	rakev	k1gFnPc4	rakev
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
rakve	rakev	k1gFnPc4	rakev
<g/>
.	.	kIx.	.
</s>
<s>
Sarkofágy	sarkofág	k1gInPc1	sarkofág
bývaly	bývat	k5eAaImAgInP	bývat
obyčejně	obyčejně	k6eAd1	obyčejně
tesány	tesat	k5eAaImNgInP	tesat
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
a	a	k8xC	a
bohatě	bohatě	k6eAd1	bohatě
zdobené	zdobený	k2eAgNnSc1d1	zdobené
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
byly	být	k5eAaImAgFnP	být
vyrobené	vyrobený	k2eAgFnPc1d1	vyrobená
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
stály	stát	k5eAaImAgFnP	stát
nad	nad	k7c7	nad
úrovní	úroveň	k1gFnSc7	úroveň
země	země	k1gFnSc1	země
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
hrobové	hrobový	k2eAgFnSc2d1	hrobová
výzdoby	výzdoba	k1gFnSc2	výzdoba
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiná	k1gFnPc1	jiná
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
terénu	terén	k1gInSc2	terén
nebo	nebo	k8xC	nebo
byly	být	k5eAaImAgInP	být
umístěny	umístit	k5eAaPmNgInP	umístit
v	v	k7c6	v
kryptách	krypta	k1gFnPc6	krypta
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
sarkofág	sarkofág	k1gInSc1	sarkofág
označována	označován	k2eAgFnSc1d1	označována
vnější	vnější	k2eAgFnSc1d1	vnější
ochranná	ochranný	k2eAgFnSc1d1	ochranná
schránka	schránka	k1gFnSc1	schránka
pro	pro	k7c4	pro
královskou	královský	k2eAgFnSc4d1	královská
mumii	mumie	k1gFnSc4	mumie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
sobě	se	k3xPyFc3	se
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
několik	několik	k4yIc4	několik
rakví	rakev	k1gFnPc2	rakev
<g/>
.	.	kIx.	.
</s>
<s>
Sarkofágy	sarkofág	k1gInPc1	sarkofág
nikdy	nikdy	k6eAd1	nikdy
nepřestaly	přestat	k5eNaPmAgInP	přestat
být	být	k5eAaImF	být
užívány	užíván	k2eAgInPc1d1	užíván
ani	ani	k8xC	ani
v	v	k7c6	v
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
sarkofág	sarkofág	k1gInSc1	sarkofág
se	se	k3xPyFc4	se
obyčejně	obyčejně	k6eAd1	obyčejně
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
pro	pro	k7c4	pro
velký	velký	k2eAgInSc4d1	velký
betonový	betonový	k2eAgInSc4d1	betonový
kryt	kryt	k1gInSc4	kryt
oddělující	oddělující	k2eAgInPc4d1	oddělující
zbytky	zbytek	k1gInPc4	zbytek
havarovaného	havarovaný	k2eAgInSc2d1	havarovaný
reaktoru	reaktor	k1gInSc2	reaktor
černobylské	černobylský	k2eAgFnSc2d1	Černobylská
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
od	od	k7c2	od
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
3	[number]	k4	3
<g/>
.	.	kIx.	.
dynastií	dynastie	k1gFnSc7	dynastie
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
hlavním	hlavní	k2eAgMnSc7d1	hlavní
bohem	bůh	k1gMnSc7	bůh
podsvětní	podsvětní	k2eAgMnSc1d1	podsvětní
bůh	bůh	k1gMnSc1	bůh
Usir	Usir	k1gMnSc1	Usir
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
hrobu	hrob	k1gInSc2	hrob
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
mastaby	mastaba	k1gFnSc2	mastaba
nebo	nebo	k8xC	nebo
jejího	její	k3xOp3gMnSc2	její
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
dynastii	dynastie	k1gFnSc6	dynastie
si	se	k3xPyFc3	se
král	král	k1gMnSc1	král
Džoser	Džoser	k1gMnSc1	Džoser
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
svým	svůj	k3xOyFgMnSc7	svůj
poradcem	poradce	k1gMnSc7	poradce
Imhotepem	Imhotep	k1gInSc7	Imhotep
první	první	k4xOgFnSc4	první
stupňovitou	stupňovitý	k2eAgFnSc4d1	stupňovitá
pyramidu	pyramida	k1gFnSc4	pyramida
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
nastupující	nastupující	k2eAgInSc4d1	nastupující
kult	kult	k1gInSc4	kult
slunečního	sluneční	k2eAgMnSc2d1	sluneční
boha	bůh	k1gMnSc2	bůh
Rea	Rea	k1gFnSc1	Rea
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
hrob	hrob	k1gInSc1	hrob
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
už	už	k6eAd1	už
nebylo	být	k5eNaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
ho	on	k3xPp3gMnSc4	on
přibližovat	přibližovat	k5eAaImF	přibližovat
k	k	k7c3	k
podsvětí	podsvětí	k1gNnSc3	podsvětí
<g/>
.	.	kIx.	.
</s>
