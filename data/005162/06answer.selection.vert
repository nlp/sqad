<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
sulfidy	sulfid	k1gInPc4	sulfid
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
pláště	plášť	k1gInSc2	plášť
a	a	k8xC	a
vnějšího	vnější	k2eAgInSc2d1	vnější
ledového	ledový	k2eAgInSc2d1	ledový
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
