<s>
Dusík	dusík	k1gInSc1	dusík
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
tvořen	tvořit	k5eAaImNgInS	tvořit
dvouatomovými	dvouatomový	k2eAgFnPc7d1	dvouatomová
molekulami	molekula	k1gFnPc7	molekula
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
spojené	spojený	k2eAgNnSc1d1	spojené
velmi	velmi	k6eAd1	velmi
pevnou	pevný	k2eAgFnSc7d1	pevná
trojnou	trojný	k2eAgFnSc7d1	trojná
vazbou	vazba	k1gFnSc7	vazba
<g/>
.	.	kIx.	.
</s>
