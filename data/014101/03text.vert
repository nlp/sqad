<s>
Two-Face	Two-Face	k1gFnSc1
</s>
<s>
Two-Face	Two-Face	k1gFnSc1
Cosplay	Cosplaa	k1gFnSc2
Two-Face	Two-Face	k1gFnSc2
na	na	k7c6
Dragon	Dragon	k1gMnSc1
Conu	Conus	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
výskyt	výskyt	k1gInSc1
</s>
<s>
Detective	Detectiv	k1gInSc5
Comics	comics	k1gInSc1
#	#	kIx~
<g/>
66	#num#	k4
(	(	kIx(
<g/>
srpen	srpen	k1gInSc1
1942	#num#	k4
<g/>
)	)	kIx)
Tvůrce	tvůrce	k1gMnSc1
</s>
<s>
Bill	Bill	k1gMnSc1
Finger	Finger	k1gMnSc1
(	(	kIx(
<g/>
scenárista	scenárista	k1gMnSc1
<g/>
)	)	kIx)
Bob	Bob	k1gMnSc1
Kane	kanout	k5eAaImIp3nS
(	(	kIx(
<g/>
kreslíř	kreslíř	k1gMnSc1
<g/>
)	)	kIx)
Informace	informace	k1gFnSc1
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
Apollo	Apollo	k1gMnSc1
Druh	druh	k1gMnSc1
</s>
<s>
člověk	člověk	k1gMnSc1
Pohlaví	pohlaví	k1gNnSc2
</s>
<s>
muž	muž	k1gMnSc1
Dovednost	dovednost	k1gFnSc1
</s>
<s>
skvělý	skvělý	k2eAgMnSc1d1
právník	právník	k1gMnSc1
a	a	k8xC
taktik	taktik	k1gMnSc1
vedení	vedení	k1gNnSc2
lidí	člověk	k1gMnPc2
střelectví	střelectví	k1gNnSc2
výdrž	výdrž	k1gFnSc1
lstivost	lstivost	k1gFnSc1
boj	boj	k1gInSc4
beze	beze	k7c2
zbraně	zbraň	k1gFnSc2
mistr	mistr	k1gMnSc1
zbraní	zbraň	k1gFnPc2
Povolání	povolání	k1gNnSc5
</s>
<s>
gangster	gangster	k1gMnSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
okresní	okresní	k2eAgMnSc1d1
prokurátor	prokurátor	k1gMnSc1
Rodina	rodina	k1gFnSc1
</s>
<s>
Christopher	Christophra	k1gFnPc2
Dent	Dent	k1gMnSc1
(	(	kIx(
<g/>
otec	otec	k1gMnSc1
<g/>
)	)	kIx)
Murray	Murraa	k1gMnSc2
Dent	Dent	k1gMnSc1
(	(	kIx(
<g/>
bratr	bratr	k1gMnSc1
<g/>
)	)	kIx)
Bydliště	bydliště	k1gNnSc1
</s>
<s>
Gotham	Gotham	k6eAd1
City	city	k1gNnSc1
<g/>
,	,	kIx,
USA	USA	kA
Národnost	národnost	k1gFnSc1
</s>
<s>
Američan	Američan	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Two-Face	Two-Face	k1gFnSc1
<g/>
,	,	kIx,
vlastním	vlastní	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Harvey	Harvea	k1gFnSc2
Dent	Dent	k2eAgMnSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
fiktivní	fiktivní	k2eAgInSc1d1
superpadouch	superpadouch	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
v	v	k7c6
amerických	americký	k2eAgInPc6d1
komiksech	komiks	k1gInPc6
od	od	k7c2
DC	DC	kA
Comics	comics	k1gInSc1
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
jako	jako	k9
Batmanův	Batmanův	k2eAgMnSc1d1
protivník	protivník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavu	postav	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
debutovala	debutovat	k5eAaBmAgFnS
v	v	k7c6
Detective	Detectiv	k1gInSc5
Comics	comics	k1gInSc1
#	#	kIx~
<g/>
66	#num#	k4
(	(	kIx(
<g/>
srpen	srpen	k1gInSc1
1942	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vytvořili	vytvořit	k5eAaPmAgMnP
Bob	bob	k1gInSc4
Kane	kanout	k5eAaImIp3nS
a	a	k8xC
Bill	Bill	k1gMnSc1
Finger	Finger	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejvytrvalejších	vytrvalý	k2eAgMnPc2d3
Batmanových	Batmanův	k2eAgMnPc2d1
nepřátel	nepřítel	k1gMnPc2
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
Two-Face	Two-Face	k1gFnSc1
do	do	k7c2
galerie	galerie	k1gFnSc2
obvyklých	obvyklý	k2eAgMnPc2d1
padouchů	padouch	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Harvey	Harvea	k1gMnSc2
Dent	Dent	k1gMnSc1
byl	být	k5eAaImAgMnS
původně	původně	k6eAd1
okresním	okresní	k2eAgMnSc7d1
prokurátorem	prokurátor	k1gMnSc7
města	město	k1gNnSc2
Gotham	Gotham	k1gInSc1
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
obětí	oběť	k1gFnSc7
zbabělého	zbabělý	k2eAgInSc2d1
útoku	útok	k1gInSc2
mafiánského	mafiánský	k2eAgMnSc4d1
bosse	boss	k1gMnSc4
Sala	Salus	k1gMnSc4
Maroniho	Maroni	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
mu	on	k3xPp3gMnSc3
při	při	k7c6
soudním	soudní	k2eAgNnSc6d1
řízení	řízení	k1gNnSc6
vylil	vylít	k5eAaPmAgMnS
do	do	k7c2
obličeje	obličej	k1gInSc2
kyselinu	kyselina	k1gFnSc4
a	a	k8xC
způsobil	způsobit	k5eAaPmAgMnS
mu	on	k3xPp3gMnSc3
tak	tak	k6eAd1
ohavné	ohavný	k2eAgNnSc4d1
znetvoření	znetvoření	k1gNnSc4
poloviny	polovina	k1gFnSc2
tváře	tvář	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dent	Dent	k1gInSc1
následně	následně	k6eAd1
zešílel	zešílet	k5eAaPmAgInS
<g/>
,	,	kIx,
přijal	přijmout	k5eAaPmAgInS
identitu	identita	k1gFnSc4
„	„	k?
<g/>
Two-Face	Two-Face	k1gFnSc2
<g/>
“	“	k?
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	s	k7c7
zločincem	zločinec	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
začal	začít	k5eAaPmAgInS
být	být	k5eAaImF
posedlý	posedlý	k2eAgInSc1d1
dualitou	dualita	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
pozdějších	pozdní	k2eAgInPc6d2
letech	let	k1gInPc6
je	být	k5eAaImIp3nS
Two-Faceova	Two-Faceův	k2eAgFnSc1d1
obsese	obsese	k1gFnSc1
vysvětlována	vysvětlovat	k5eAaImNgFnS
jako	jako	k8xS,k8xC
důsledek	důsledek	k1gInSc1
schizofrenie	schizofrenie	k1gFnSc2
a	a	k8xC
bipolární	bipolární	k2eAgFnSc2d1
poruchy	porucha	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Posedle	posedle	k6eAd1
dělá	dělat	k5eAaImIp3nS
všechna	všechen	k3xTgNnPc4
důležitá	důležitý	k2eAgNnPc4d1
rozhodnutí	rozhodnutí	k1gNnPc4
hodem	hod	k1gInSc7
dvouhlavé	dvouhlavý	k2eAgFnSc2d1
mince	mince	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
jedné	jeden	k4xCgFnSc2
strany	strana	k1gFnSc2
poškrábaná	poškrábaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kromě	kromě	k7c2
komiksů	komiks	k1gInPc2
se	se	k3xPyFc4
Two-Face	Two-Face	k1gFnSc1
poprvé	poprvé	k6eAd1
objevil	objevit	k5eAaPmAgInS
ve	v	k7c6
filmu	film	k1gInSc6
Batman	Batman	k1gMnSc1
z	z	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
(	(	kIx(
<g/>
pouze	pouze	k6eAd1
jako	jako	k9
Harvey	Harvey	k1gInPc1
Dent	Denta	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
načež	načež	k6eAd1
následovalo	následovat	k5eAaImAgNnS
<g/>
:	:	kIx,
animovaný	animovaný	k2eAgInSc1d1
seriál	seriál	k1gInSc1
Batman	Batman	k1gMnSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
filmy	film	k1gInPc7
Batman	Batman	k1gMnSc1
navždy	navždy	k6eAd1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Temný	temný	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
videohry	videohra	k1gFnPc1
Lego	lego	k1gNnSc1
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Videogame	Videogam	k1gInSc5
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Arkham	Arkham	k1gInSc1
City	city	k1gNnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
či	či	k8xC
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc5
Telltale	Telltal	k1gMnSc5
Series	Series	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
plno	plno	k6eAd1
dalších	další	k2eAgFnPc2d1
adaptací	adaptace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
IGN	IGN	kA
zařadila	zařadit	k5eAaPmAgFnS
postavu	postava	k1gFnSc4
na	na	k7c4
12	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
seznamu	seznam	k1gInSc6
100	#num#	k4
nejlepších	dobrý	k2eAgMnPc2d3
padouchů	padouch	k1gMnPc2
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
postavy	postava	k1gFnSc2
</s>
<s>
Inspirace	inspirace	k1gFnSc1
</s>
<s>
Bob	Bob	k1gMnSc1
Kane	kanout	k5eAaImIp3nS
</s>
<s>
Bob	Bob	k1gMnSc1
Kane	kanout	k5eAaImIp3nS
uvedl	uvést	k5eAaPmAgMnS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
autobiografii	autobiografie	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
tvorbě	tvorba	k1gFnSc6
postavy	postava	k1gFnSc2
byl	být	k5eAaImAgInS
inspirován	inspirovat	k5eAaBmNgInS
příběhem	příběh	k1gInSc7
Podivný	podivný	k2eAgInSc1d1
případ	případ	k1gInSc1
Dr	dr	kA
<g/>
.	.	kIx.
Jekylla	Jekyll	k1gMnSc2
a	a	k8xC
pana	pan	k1gMnSc2
Hyda	Hydus	k1gMnSc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
filmovou	filmový	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
z	z	k7c2
roku	rok	k1gInSc2
1931	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgFnSc1
inspirace	inspirace	k1gFnSc1
byla	být	k5eAaImAgFnS
převzata	převzít	k5eAaPmNgFnS
rovněž	rovněž	k9
z	z	k7c2
rodokapsové	rodokapsový	k2eAgFnSc2d1
postavy	postava	k1gFnSc2
jménem	jméno	k1gNnSc7
Black	Blacka	k1gFnPc2
Bat	Bat	k1gMnPc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc1
původ	původ	k1gInSc1
zahrnoval	zahrnovat	k5eAaImAgInS
tvář	tvář	k1gFnSc4
postříkanou	postříkaný	k2eAgFnSc7d1
kyselinou	kyselina	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1
a	a	k8xC
stříbrný	stříbrný	k2eAgInSc1d1
věk	věk	k1gInSc1
</s>
<s>
Two-Face	Two-Face	k1gFnSc1
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
objevil	objevit	k5eAaPmAgMnS
v	v	k7c6
Detective	Detectiv	k1gInSc5
Comics	comics	k1gInSc1
#	#	kIx~
<g/>
66	#num#	k4
(	(	kIx(
<g/>
srpen	srpen	k1gInSc1
1942	#num#	k4
<g/>
)	)	kIx)
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Harvey	Harvea	k1gFnSc2
„	„	k?
<g/>
Apollo	Apollo	k1gMnSc1
<g/>
“	“	k?
Kent	Kent	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
bylo	být	k5eAaImAgNnS
jeho	jeho	k3xOp3gNnSc1
jméno	jméno	k1gNnSc1
změněno	změnit	k5eAaPmNgNnS
na	na	k7c4
Harvey	Harvea	k1gFnPc4
Dent	Denta	k1gFnPc2
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
proto	proto	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
zabránilo	zabránit	k5eAaPmAgNnS
zaměňování	zaměňování	k1gNnSc1
se	s	k7c7
Supermanem	superman	k1gMnSc7
alias	alias	k9
Clarkem	Clarek	k1gMnSc7
Kentem	Kent	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Two-Faceův	Two-Faceův	k2eAgInSc4d1
původní	původní	k2eAgInSc4d1
příběh	příběh	k1gInSc4
byla	být	k5eAaImAgFnS
trilogie	trilogie	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
konci	konec	k1gInSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
obnově	obnova	k1gFnSc3
jeho	jeho	k3xOp3gFnPc4
znetvořené	znetvořený	k2eAgFnPc4d1
tváře	tvář	k1gFnPc4
a	a	k8xC
zároveň	zároveň	k6eAd1
také	také	k9
psychického	psychický	k2eAgNnSc2d1
zdraví	zdraví	k1gNnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
byl	být	k5eAaImAgInS
schopen	schopen	k2eAgInSc1d1
vrátit	vrátit	k5eAaPmF
do	do	k7c2
normálního	normální	k2eAgInSc2d1
života	život	k1gInSc2
a	a	k8xC
oženit	oženit	k5eAaPmF
se	se	k3xPyFc4
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
snoubenkou	snoubenka	k1gFnSc7
Gildou	gilda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scenárista	scenárista	k1gMnSc1
Bill	Bill	k1gMnSc1
Finger	Finger	k1gMnSc1
nechtěl	chtít	k5eNaImAgMnS
Harveyho	Harvey	k1gMnSc4
šťastný	šťastný	k2eAgInSc1d1
konec	konec	k1gInSc1
zničit	zničit	k5eAaPmF
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
představil	představit	k5eAaPmAgInS
řadu	řada	k1gFnSc4
napodobitelů	napodobitel	k1gMnPc2
<g/>
,	,	kIx,
především	především	k6eAd1
Paula	Paul	k1gMnSc2
Sloanea	Sloaneus	k1gMnSc2
a	a	k8xC
George	Georg	k1gMnSc2
Blakea	Blakeus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
nechal	nechat	k5eAaPmAgMnS
David	David	k1gMnSc1
V.	V.	kA
Reed	Reed	k1gMnSc1
Harveyho	Harvey	k1gMnSc2
tvář	tvář	k1gFnSc4
znovu	znovu	k6eAd1
znetvořit	znetvořit	k5eAaPmF
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
přivedl	přivést	k5eAaPmAgMnS
pravého	pravý	k2eAgMnSc4d1
Two-Face	Two-Face	k1gFnPc1
jednou	jeden	k4xCgFnSc7
provždy	provždy	k6eAd1
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
však	však	k9
šlo	jít	k5eAaImAgNnS
o	o	k7c4
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
poslední	poslední	k2eAgInSc4d1
výskyt	výskyt	k1gInSc4
v	v	k7c6
rámci	rámec	k1gInSc6
Zlatého	zlatý	k2eAgInSc2d1
věku	věk	k1gInSc2
<g/>
,	,	kIx,
načež	načež	k6eAd1
na	na	k7c4
šestnáct	šestnáct	k4xCc4
let	léto	k1gNnPc2
zmizel	zmizet	k5eAaPmAgInS
z	z	k7c2
komiksů	komiks	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
stříbrného	stříbrný	k2eAgInSc2d1
věku	věk	k1gInSc2
se	se	k3xPyFc4
Two-Face	Two-Face	k1gFnSc1
v	v	k7c6
komiksu	komiks	k1gInSc6
neobjevil	objevit	k5eNaPmAgMnS
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
jediného	jediný	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
World	World	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Finest	Finest	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
Batmanovi	Batman	k1gMnSc3
vymýván	vymýván	k2eAgInSc4d1
mozek	mozek	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
si	se	k3xPyFc3
myslel	myslet	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
Two-Facem	Two-Faec	k1gInSc7
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
pravý	pravý	k2eAgMnSc1d1
padouch	padouch	k1gMnSc1
se	se	k3xPyFc4
během	během	k7c2
této	tento	k3xDgFnSc2
éry	éra	k1gFnSc2
objevil	objevit	k5eAaPmAgInS
v	v	k7c6
novinových	novinový	k2eAgInPc6d1
stripech	strip	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Bronzový	bronzový	k2eAgInSc1d1
věk	věk	k1gInSc1
</s>
<s>
Po	po	k7c6
šestnáctileté	šestnáctiletý	k2eAgFnSc6d1
nepřítomnosti	nepřítomnost	k1gFnSc6
v	v	k7c6
komiksech	komiks	k1gInPc6
byl	být	k5eAaImAgInS
Two-Face	Two-Face	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
vzkříšen	vzkříšen	k2eAgInSc1d1
Dennisem	Dennis	k1gInSc7
O	O	kA
<g/>
'	'	kIx"
<g/>
Neilem	Neil	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
obnovil	obnovit	k5eAaPmAgMnS
Dentův	Dentův	k2eAgInSc4d1
status	status	k1gInSc4
jednoho	jeden	k4xCgMnSc2
z	z	k7c2
největších	veliký	k2eAgMnPc2d3
Batmanových	Batmanův	k2eAgMnPc2d1
nepřátel	nepřítel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
dostal	dostat	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
původ	původ	k1gInSc1
od	od	k7c2
Jacka	Jacek	k1gInSc2
C.	C.	kA
Harrise	Harrise	k1gFnPc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
bylo	být	k5eAaImAgNnS
odhaleno	odhalit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
cílem	cíl	k1gInSc7
Maroniho	Maroni	k1gMnSc2
útoku	útok	k1gInSc3
kyselinou	kyselina	k1gFnSc7
neměl	mít	k5eNaImAgMnS
být	být	k5eAaImF
Harvey	Harvea	k1gFnPc4
Dent	Denta	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
důstojník	důstojník	k1gMnSc1
Dave	Dav	k1gInSc5
„	„	k?
<g/>
Pretty	Pretta	k1gFnPc4
Boy	boa	k1gFnSc2
<g/>
“	“	k?
Davis	Davis	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
mafiána	mafián	k1gMnSc4
zatkl	zatknout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Denta	Denta	k1gFnSc1
to	ten	k3xDgNnSc1
tak	tak	k9
utvrdilo	utvrdit	k5eAaPmAgNnS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gNnSc1
znetvoření	znetvoření	k1gNnSc1
bylo	být	k5eAaImAgNnS
pouze	pouze	k6eAd1
otázkou	otázka	k1gFnSc7
štěstí	štěstí	k1gNnSc2
<g/>
,	,	kIx,
náhody	náhoda	k1gFnSc2
a	a	k8xC
osudu	osud	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
tyto	tento	k3xDgFnPc4
události	událost	k1gFnPc4
nezmínil	zmínit	k5eNaPmAgInS
žádný	žádný	k3yNgInSc1
jiný	jiný	k2eAgInSc1d1
příběh	příběh	k1gInSc1
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
zahrnuty	zahrnout	k5eAaPmNgInP
do	do	k7c2
Two-Faceova	Two-Faceův	k2eAgInSc2d1
oficiálního	oficiální	k2eAgInSc2d1
popisu	popis	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
bylo	být	k5eAaImAgNnS
odhaleno	odhalen	k2eAgNnSc1d1
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
měl	mít	k5eAaImAgInS
Harvey	Harvey	k1gInPc4
odcizenou	odcizený	k2eAgFnSc4d1
dceru	dcera	k1gFnSc4
Duelu	duel	k1gInSc2
Dentovou	Dentová	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
podobně	podobně	k6eAd1
jako	jako	k9
u	u	k7c2
původu	původ	k1gInSc2
Davea	Dave	k2eAgFnSc1d1
Devise	devisa	k1gFnSc3
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
i	i	k9
tato	tento	k3xDgFnSc1
kontinuita	kontinuita	k1gFnSc1
prakticky	prakticky	k6eAd1
všemi	všecek	k3xTgInPc7
následujícími	následující	k2eAgInPc7d1
příběhy	příběh	k1gInPc7
ignorována	ignorovat	k5eAaImNgNnP
<g/>
.	.	kIx.
</s>
<s>
Moderní	moderní	k2eAgInSc1d1
věk	věk	k1gInSc1
</s>
<s>
Po	po	k7c6
událostech	událost	k1gFnPc6
Krize	krize	k1gFnSc2
na	na	k7c6
nekonečnu	nekonečno	k1gNnSc6
Zemí	zem	k1gFnPc2
byl	být	k5eAaImAgInS
Dentův	Dentův	k2eAgInSc1d1
původ	původ	k1gInSc1
kompletně	kompletně	k6eAd1
přepsán	přepsat	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostal	dostat	k5eAaPmAgMnS
tragičtější	tragický	k2eAgFnSc4d2
minulost	minulost	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jeho	jeho	k3xOp3gFnSc1
postava	postava	k1gFnSc1
byla	být	k5eAaImAgFnS
sympatičtější	sympatický	k2eAgFnSc1d2
<g/>
,	,	kIx,
a	a	k8xC
bylo	být	k5eAaImAgNnS
přidáno	přidat	k5eAaPmNgNnS
jeho	jeho	k3xOp3gNnSc1
rané	raný	k2eAgNnSc1d1
spojení	spojení	k1gNnSc1
s	s	k7c7
Batmanem	Batman	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnPc1
postava	postava	k1gFnSc1
také	také	k6eAd1
získala	získat	k5eAaPmAgFnS
typické	typický	k2eAgInPc4d1
symptomy	symptom	k1gInPc4
disociativní	disociativní	k2eAgFnSc2d1
poruchy	porucha	k1gFnSc2
identity	identita	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnPc1
dvě	dva	k4xCgFnPc1
identity	identita	k1gFnPc1
ztratily	ztratit	k5eAaPmAgFnP
vědomí	vědomí	k1gNnSc4
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
dělá	dělat	k5eAaImIp3nS
ta	ten	k3xDgFnSc1
druhá	druhý	k4xOgFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fiktivní	fiktivní	k2eAgFnSc1d1
biografie	biografie	k1gFnSc1
postavy	postava	k1gFnSc2
</s>
<s>
Země-dvě	Země-dvě	k6eAd1
</s>
<s>
Harvey	Harvea	k1gFnPc1
„	„	k?
<g/>
Apollo	Apollo	k1gMnSc1
<g/>
“	“	k?
Kent	Kent	k1gMnSc1
byl	být	k5eAaImAgMnS
původně	původně	k6eAd1
mladým	mladý	k2eAgMnSc7d1
a	a	k8xC
schopným	schopný	k2eAgMnSc7d1
okresním	okresní	k2eAgMnSc7d1
prokurátorem	prokurátor	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
dostal	dostat	k5eAaPmAgMnS
na	na	k7c4
starost	starost	k1gFnSc4
případ	případ	k1gInSc4
vraždy	vražda	k1gFnSc2
spáchané	spáchaný	k2eAgFnSc2d1
mafiánským	mafiánský	k2eAgMnSc7d1
bossem	boss	k1gMnSc7
Maronim	Maronima	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
soudního	soudní	k2eAgNnSc2d1
přelíčení	přelíčení	k1gNnSc2
Maroni	Maroň	k1gFnSc3
nečekaně	nečekaně	k6eAd1
vylil	vylít	k5eAaPmAgMnS
Kentovi	Kent	k1gMnSc3
do	do	k7c2
obličeje	obličej	k1gInSc2
kyselinu	kyselina	k1gFnSc4
sírovou	sírový	k2eAgFnSc7d1
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
mu	on	k3xPp3gMnSc3
způsobil	způsobit	k5eAaPmAgInS
hrůzné	hrůzný	k2eAgNnSc4d1
poleptání	poleptání	k1gNnSc4
levé	levý	k2eAgFnSc2d1
části	část	k1gFnSc2
tváře	tvář	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
jeho	jeho	k3xOp3gInSc3
znetvoření	znetvořený	k2eAgMnPc1d1
jej	on	k3xPp3gInSc4
opustila	opustit	k5eAaPmAgFnS
snoubenka	snoubenka	k1gFnSc1
Gilda	gilda	k1gFnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
v	v	k7c6
něm	on	k3xPp3gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
frustrací	frustrace	k1gFnSc7
ze	z	k7c2
ztráty	ztráta	k1gFnSc2
osobní	osobní	k2eAgFnSc2d1
krásy	krása	k1gFnSc2
<g/>
,	,	kIx,
vyvolalo	vyvolat	k5eAaPmAgNnS
hluboké	hluboký	k2eAgNnSc1d1
zoufalství	zoufalství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
tedy	tedy	k9
svěřit	svěřit	k5eAaPmF
do	do	k7c2
rukou	ruka	k1gFnPc2
osudu	osud	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
si	se	k3xPyFc3
o	o	k7c6
podobě	podoba	k1gFnSc6
své	svůj	k3xOyFgFnSc6
budoucnosti	budoucnost	k1gFnSc6
hodil	hodit	k5eAaImAgMnS,k5eAaPmAgMnS
dvouhlavou	dvouhlavý	k2eAgFnSc7d1
mincí	mince	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tu	tu	k6eAd1
z	z	k7c2
jedné	jeden	k4xCgFnSc2
strany	strana	k1gFnSc2
poškrábal	poškrábat	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mu	on	k3xPp3gMnSc3
tak	tak	k9
pomohla	pomoct	k5eAaPmAgFnS
v	v	k7c6
rozhodnutí	rozhodnutí	k1gNnSc6
mezi	mezi	k7c7
dobrem	dobro	k1gNnSc7
(	(	kIx(
<g/>
čistá	čistý	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
zlem	zlo	k1gNnSc7
(	(	kIx(
<g/>
poškrábaná	poškrábaný	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osud	osud	k1gInSc1
rozhodl	rozhodnout	k5eAaPmAgInS
o	o	k7c6
druhé	druhý	k4xOgFnSc6
možnosti	možnost	k1gFnSc6
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
z	z	k7c2
Kenta	Kent	k1gInSc2
stal	stát	k5eAaPmAgMnS
nový	nový	k2eAgMnSc1d1
gothamský	gothamský	k2eAgMnSc1d1
zločinec	zločinec	k1gMnSc1
známý	známý	k1gMnSc1
jako	jako	k8xC,k8xS
Two-Face	Two-Face	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
však	však	k9
již	již	k6eAd1
nedokázal	dokázat	k5eNaPmAgMnS
rozhodnout	rozhodnout	k5eAaPmF
bez	bez	k7c2
použití	použití	k1gNnSc2
mince	mince	k1gFnSc2
<g/>
,	,	kIx,
stávalo	stávat	k5eAaImAgNnS
se	se	k3xPyFc4
často	často	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
místo	místo	k7c2
zločinů	zločin	k1gInPc2
začal	začít	k5eAaPmAgInS
náhle	náhle	k6eAd1
páchat	páchat	k5eAaImF
dobré	dobrý	k2eAgInPc4d1
skutky	skutek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
svým	svůj	k3xOyFgNnSc7
nevyzpytatelným	vyzpytatelný	k2eNgNnSc7d1
chováním	chování	k1gNnSc7
mátl	mást	k5eAaImAgMnS
všechny	všechen	k3xTgMnPc4
obyvatele	obyvatel	k1gMnPc4
Gotham	Gotham	k1gInSc4
City	city	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Batman	Batman	k1gMnSc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
na	na	k7c4
Harveyho	Harvey	k1gMnSc4
naléhat	naléhat	k5eAaImF,k5eAaBmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
opustil	opustit	k5eAaPmAgInS
svět	svět	k1gInSc4
zločinu	zločin	k1gInSc2
a	a	k8xC
vrátil	vrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
dřívější	dřívější	k2eAgFnSc3d1
práci	práce	k1gFnSc3
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
se	se	k3xPyFc4
však	však	k9
nedokázal	dokázat	k5eNaPmAgMnS
rozhodnout	rozhodnout	k5eAaPmF
sám	sám	k3xTgMnSc1
<g/>
,	,	kIx,
musel	muset	k5eAaImAgInS
si	se	k3xPyFc3
opět	opět	k6eAd1
hodit	hodit	k5eAaPmF,k5eAaImF
mincí	mince	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
tentokrát	tentokrát	k6eAd1
zastavila	zastavit	k5eAaPmAgFnS
na	na	k7c6
hraně	hrana	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
i	i	k9
když	když	k8xS
Batman	Batman	k1gMnSc1
navrhoval	navrhovat	k5eAaImAgMnS
nový	nový	k2eAgInSc4d1
hod	hod	k1gInSc4
<g/>
,	,	kIx,
Two-Face	Two-Face	k1gFnPc4
nedokázal	dokázat	k5eNaPmAgMnS
porušit	porušit	k5eAaPmF
vlastní	vlastní	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náhle	náhle	k6eAd1
se	se	k3xPyFc4
však	však	k9
objevil	objevit	k5eAaPmAgMnS
policista	policista	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
bezmyšlenkovitě	bezmyšlenkovitě	k6eAd1
střelil	střelit	k5eAaPmAgMnS
Two-Face	Two-Face	k1gFnPc4
do	do	k7c2
prsou	prsa	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kulka	kulka	k1gFnSc1
naštěstí	naštěstí	k6eAd1
zasáhla	zasáhnout	k5eAaPmAgFnS
minci	mince	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
měl	mít	k5eAaImAgInS
Kent	Kent	k1gInSc1
v	v	k7c6
kapse	kapsa	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
ke	k	k7c3
všemu	všecek	k3xTgNnSc3
ještě	ještě	k9
poškrábanou	poškrábaný	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
ho	on	k3xPp3gMnSc4
přesvědčilo	přesvědčit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
zůstat	zůstat	k5eAaPmF
padouchem	padouch	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
ho	on	k3xPp3gInSc4
však	však	k9
Batman	Batman	k1gMnSc1
dopadl	dopadnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
jej	on	k3xPp3gMnSc4
zradil	zradit	k5eAaPmAgMnS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
členů	člen	k1gMnPc2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
gangu	gang	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kent	Kent	k5eAaPmF
nakonec	nakonec	k6eAd1
své	svůj	k3xOyFgNnSc4
šílenství	šílenství	k1gNnSc4
překonal	překonat	k5eAaPmAgMnS
<g/>
,	,	kIx,
když	když	k8xS
při	při	k7c6
jedné	jeden	k4xCgFnSc6
z	z	k7c2
dalších	další	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
omylem	omylem	k6eAd1
postřelil	postřelit	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
bývalou	bývalý	k2eAgFnSc4d1
snoubenku	snoubenka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
se	se	k3xPyFc4
dobrovolně	dobrovolně	k6eAd1
vydal	vydat	k5eAaPmAgMnS
Batmanovi	Batman	k1gMnSc3
a	a	k8xC
podstoupil	podstoupit	k5eAaPmAgInS
soud	soud	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
si	se	k3xPyFc3
u	u	k7c2
špičkového	špičkový	k2eAgMnSc2d1
plastického	plastický	k2eAgMnSc2d1
chirurga	chirurg	k1gMnSc2
nechal	nechat	k5eAaPmAgMnS
opravit	opravit	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
obličej	obličej	k1gInSc4
<g/>
,	,	kIx,
zanechal	zanechat	k5eAaPmAgMnS
kriminální	kriminální	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
a	a	k8xC
vrátil	vrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
ke	k	k7c3
kariéře	kariéra	k1gFnSc3
okresního	okresní	k2eAgMnSc2d1
prokurátora	prokurátor	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c4
něco	něco	k3yInSc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
Two-Face	Two-Face	k1gFnSc1
do	do	k7c2
Gotham	Gotham	k1gInSc4
City	City	k1gFnSc3
vrátil	vrátit	k5eAaPmAgMnS
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
však	však	k9
nešlo	jít	k5eNaImAgNnS
o	o	k7c4
Harveyho	Harvey	k1gMnSc4
(	(	kIx(
<g/>
ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
zde	zde	k6eAd1
objevil	objevit	k5eAaPmAgInS
již	již	k6eAd1
s	s	k7c7
příjmením	příjmení	k1gNnSc7
Dent	Denta	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
o	o	k7c4
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
komorníka	komorník	k1gMnSc4
Wilkinse	Wilkins	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
svého	svůj	k3xOyFgMnSc2
pána	pán	k1gMnSc2
napodoboval	napodobovat	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohl	moct	k5eAaImAgInS
páchat	páchat	k5eAaImF
zločiny	zločin	k1gInPc4
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yIgMnPc2,k3yRgMnPc2,k3yQgMnPc2
by	by	kYmCp3nP
ostatní	ostatní	k2eAgMnPc1d1
podezřívali	podezřívat	k5eAaImAgMnP
skutečného	skutečný	k2eAgMnSc4d1
padoucha	padouch	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
se	se	k3xPyFc4
v	v	k7c6
Gothamu	Gotham	k1gInSc6
objevil	objevit	k5eAaPmAgMnS
ještě	ještě	k9
jeden	jeden	k4xCgMnSc1
napodobitel	napodobitel	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
sice	sice	k8xC
George	George	k1gNnSc1
Blake	Blak	k1gInSc2
<g/>
,	,	kIx,
kterému	který	k3yIgInSc3,k3yQgInSc3,k3yRgInSc3
šlo	jít	k5eAaImAgNnS
v	v	k7c6
podstatě	podstata	k1gFnSc6
o	o	k7c4
to	ten	k3xDgNnSc4
samé	samý	k3xTgNnSc4
jako	jako	k9
Wilkinsovi	Wilkinsův	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Země-jedna	Země-jedna	k1gFnSc1
</s>
<s>
Původ	původ	k1gInSc1
a	a	k8xC
proměna	proměna	k1gFnSc1
Two-Face	Two-Face	k1gFnSc1
ze	z	k7c2
Země-jedna	Země-jedno	k1gNnSc2
jsou	být	k5eAaImIp3nP
stejné	stejný	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
u	u	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
protějšku	protějšek	k1gInSc2
ze	z	k7c2
Země-dvě	Země-dva	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
svém	svůj	k3xOyFgNnSc6
znetvoření	znetvoření	k1gNnSc6
se	se	k3xPyFc4
Harvey	Harvey	k1gInPc7
Dent	Denta	k1gFnPc2
vrhl	vrhnout	k5eAaPmAgMnS,k5eAaImAgMnS
na	na	k7c4
dráhu	dráha	k1gFnSc4
zločinu	zločin	k1gInSc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
Batman	Batman	k1gMnSc1
ho	on	k3xPp3gInSc4
dokázal	dokázat	k5eAaPmAgMnS
zastavit	zastavit	k5eAaPmF
a	a	k8xC
přesvědčit	přesvědčit	k5eAaPmF
jej	on	k3xPp3gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
podstoupil	podstoupit	k5eAaPmAgMnS
plastickou	plastický	k2eAgFnSc4d1
operaci	operace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
dopadla	dopadnout	k5eAaPmAgFnS
dobře	dobře	k6eAd1
a	a	k8xC
Dent	Dent	k1gInSc1
se	se	k3xPyFc4
následně	následně	k6eAd1
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
normálního	normální	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednoho	jeden	k4xCgInSc2
dne	den	k1gInSc2
byl	být	k5eAaImAgInS
však	však	k9
svědkem	svědek	k1gMnSc7
loupeže	loupež	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
když	když	k8xS
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgInS
zastavit	zastavit	k5eAaPmF
zloděje	zloděj	k1gMnPc4
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
náhle	náhle	k6eAd1
k	k	k7c3
explozi	exploze	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
ho	on	k3xPp3gMnSc4
opět	opět	k6eAd1
znetvořila	znetvořit	k5eAaPmAgFnS
a	a	k8xC
probudila	probudit	k5eAaPmAgFnS
v	v	k7c6
něm	on	k3xPp3gNnSc6
spící	spící	k2eAgInSc1d1
identitu	identita	k1gFnSc4
Two-Face	Two-Face	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Padouch	padouch	k1gMnSc1
ihned	ihned	k6eAd1
odstartoval	odstartovat	k5eAaPmAgMnS
další	další	k2eAgFnSc4d1
vlnu	vlna	k1gFnSc4
zločinů	zločin	k1gInPc2
<g/>
,	,	kIx,
během	během	k7c2
kterých	který	k3yQgFnPc2,k3yIgFnPc2,k3yRgFnPc2
téměř	téměř	k6eAd1
eliminoval	eliminovat	k5eAaBmAgInS
Batmana	Batman	k1gMnSc4
a	a	k8xC
Robina	robin	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
byl	být	k5eAaImAgMnS
dopaden	dopadnout	k5eAaPmNgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Two-Faceovi	Two-Faceův	k2eAgMnPc1d1
se	se	k3xPyFc4
však	však	k9
podařilo	podařit	k5eAaPmAgNnS
uprchnout	uprchnout	k5eAaPmF
a	a	k8xC
spustit	spustit	k5eAaPmF
plán	plán	k1gInSc4
na	na	k7c4
vraždu	vražda	k1gFnSc4
několika	několik	k4yIc2
akcionářů	akcionář	k1gMnPc2
společnosti	společnost	k1gFnSc2
Starr	Starr	k1gMnSc1
Corporation	Corporation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
ho	on	k3xPp3gMnSc4
zastavili	zastavit	k5eAaPmAgMnP
Batman	Batman	k1gMnSc1
a	a	k8xC
Green	Green	k2eAgMnSc1d1
Arrow	Arrow	k1gMnSc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
jej	on	k3xPp3gNnSc4
ovšem	ovšem	k9
nedokázali	dokázat	k5eNaPmAgMnP
zadržet	zadržet	k5eAaPmF
<g/>
,	,	kIx,
takže	takže	k8xS
jim	on	k3xPp3gMnPc3
opět	opět	k6eAd1
utekl	utéct	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
Two-Face	Two-Face	k1gFnSc2
v	v	k7c6
kriminální	kriminální	k2eAgFnSc6d1
činnosti	činnost	k1gFnSc6
pokračoval	pokračovat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
shromáždil	shromáždit	k5eAaPmAgInS
malý	malý	k2eAgInSc1d1
gang	gang	k1gInSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
kterým	který	k3yQgMnPc3,k3yRgMnPc3,k3yIgMnPc3
odstartoval	odstartovat	k5eAaPmAgMnS
sérii	série	k1gFnSc4
loupeží	loupež	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
vyvrcholily	vyvrcholit	k5eAaPmAgFnP
krádeží	krádež	k1gFnPc2
zlatých	zlatý	k2eAgInPc2d1
dubletů	dublet	k1gInPc2
ze	z	k7c2
starověké	starověký	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Následně	následně	k6eAd1
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
komplicem	komplic	k1gMnSc7
ukradl	ukradnout	k5eAaPmAgMnS
binární	binární	k2eAgInPc4d1
kódy	kód	k1gInPc4
pro	pro	k7c4
jadernou	jaderný	k2eAgFnSc4d1
zbraň	zbraň	k1gFnSc4
a	a	k8xC
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
vydírat	vydírat	k5eAaImF
americkou	americký	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
o	o	k7c4
22	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
jej	on	k3xPp3gInSc4
Batman	Batman	k1gMnSc1
a	a	k8xC
federální	federální	k2eAgMnSc1d1
agent	agent	k1gMnSc1
King	King	k1gMnSc1
Faraday	Faradaa	k1gFnSc2
pokusili	pokusit	k5eAaPmAgMnP
zastavit	zastavit	k5eAaPmF
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
nucený	nucený	k2eAgMnSc1d1
své	svůj	k3xOyFgInPc4
plány	plán	k1gInPc4
změnit	změnit	k5eAaPmF
a	a	k8xC
přesunout	přesunout	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
New	New	k1gFnSc2
Orleans	Orleans	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
ukrást	ukrást	k5eAaPmF
dvojnásobek	dvojnásobek	k1gInSc4
výkupného	výkupné	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
Faraday	Faradaa	k1gFnPc1
a	a	k8xC
Batman	Batman	k1gMnSc1
ho	on	k3xPp3gNnSc4
zastavili	zastavit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
byly	být	k5eAaImAgInP
jeho	jeho	k3xOp3gInPc1
plány	plán	k1gInPc1
zmařeny	zmařen	k2eAgInPc1d1
<g/>
,	,	kIx,
jemu	on	k3xPp3gNnSc3
samotnému	samotný	k2eAgNnSc3d1
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
uniknout	uniknout	k5eAaPmF
domnělé	domnělý	k2eAgFnPc4d1
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nová	nový	k2eAgFnSc1d1
Země	země	k1gFnSc1
</s>
<s>
V	v	k7c6
postkrizové	postkrizový	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
příběhu	příběh	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
odehrával	odehrávat	k5eAaImAgInS
na	na	k7c6
Nové	Nové	k2eAgFnSc6d1
Zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
vyobrazeno	vyobrazen	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Harvey	Harvey	k1gInPc7
Dent	Denta	k1gFnPc2
narodil	narodit	k5eAaPmAgMnS
do	do	k7c2
rodiny	rodina	k1gFnSc2
nižší	nízký	k2eAgFnSc2d2
třídy	třída	k1gFnSc2
a	a	k8xC
byl	být	k5eAaImAgMnS
vychován	vychovat	k5eAaPmNgMnS
s	s	k7c7
instinktivní	instinktivní	k2eAgFnSc7d1
averzí	averze	k1gFnSc7
a	a	k8xC
nedůvěrou	nedůvěra	k1gFnSc7
k	k	k7c3
vyšší	vysoký	k2eAgFnSc3d2
třídě	třída	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
krutý	krutý	k2eAgInSc1d1
<g/>
,	,	kIx,
násilnický	násilnický	k2eAgInSc1d1
a	a	k8xC
psychicky	psychicky	k6eAd1
nemocný	mocný	k2eNgMnSc1d1,k2eAgMnSc1d1
otec	otec	k1gMnSc1
jej	on	k3xPp3gMnSc4
v	v	k7c6
dětství	dětství	k1gNnSc6
často	často	k6eAd1
týral	týrat	k5eAaImAgInS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
o	o	k7c6
trestu	trest	k1gInSc6
nebo	nebo	k8xC
milosti	milost	k1gFnSc6
rozhodoval	rozhodovat	k5eAaImAgMnS
hozením	hození	k1gNnSc7
dvouhlavé	dvouhlavý	k2eAgFnSc2d1
mince	mince	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
drsné	drsný	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
u	u	k7c2
Denta	Dent	k1gInSc2
vyvinula	vyvinout	k5eAaPmAgFnS
bipolární	bipolární	k2eAgFnSc1d1
porucha	porucha	k1gFnSc1
a	a	k8xC
paranoidní	paranoidní	k2eAgFnSc1d1
schizofrenie	schizofrenie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
jeho	jeho	k3xOp3gFnSc1
tvrdá	tvrdý	k2eAgFnSc1d1
pracovní	pracovní	k2eAgFnSc1d1
morálka	morálka	k1gFnSc1
jej	on	k3xPp3gInSc4
nakonec	nakonec	k6eAd1
dovedla	dovést	k5eAaPmAgFnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
právníkem	právník	k1gMnSc7
a	a	k8xC
později	pozdě	k6eAd2
dokonce	dokonce	k9
okresním	okresní	k2eAgMnSc7d1
prokurátorem	prokurátor	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Média	médium	k1gNnPc1
ho	on	k3xPp3gInSc4
přezdívala	přezdívat	k5eAaImAgFnS
„	„	k?
<g/>
Apollo	Apollo	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
byl	být	k5eAaImAgInS
šarmantní	šarmantní	k2eAgMnSc1d1
<g/>
,	,	kIx,
pohledný	pohledný	k2eAgMnSc1d1
a	a	k8xC
zdánlivě	zdánlivě	k6eAd1
nedotknutelný	nedotknutelný	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oddaně	oddaně	k6eAd1
prosazoval	prosazovat	k5eAaImAgMnS
právo	právo	k1gNnSc4
a	a	k8xC
pořádek	pořádek	k1gInSc4
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
jedním	jeden	k4xCgMnSc7
z	z	k7c2
prvních	první	k4xOgMnPc2
spojenců	spojenec	k1gMnPc2
Batmana	Batman	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
době	doba	k1gFnSc6
jeho	jeho	k3xOp3gFnSc2
právní	právní	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
vládl	vládnout	k5eAaImAgInS
gothamskému	gothamský	k2eAgInSc3d1
podsvětí	podsvětí	k1gNnSc1
mafiánský	mafiánský	k2eAgMnSc1d1
boss	boss	k1gMnSc1
Carmine	Carmin	k1gInSc5
Falcone	Falcon	k1gInSc5
alias	alias	k9
„	„	k?
<g/>
Říman	Říman	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
Dent	Dent	k1gMnSc1
začal	začít	k5eAaPmAgMnS
zasahovat	zasahovat	k5eAaImF
do	do	k7c2
Falconeho	Falcone	k1gMnSc2
záležitostí	záležitost	k1gFnPc2
<g/>
,	,	kIx,
najal	najmout	k5eAaPmAgMnS
si	se	k3xPyFc3
mafián	mafián	k1gMnSc1
Mickeyho	Mickey	k1gMnSc2
„	„	k?
<g/>
Norka	norek	k1gMnSc2
<g/>
“	“	k?
Sullivana	Sullivan	k1gMnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
okresního	okresní	k2eAgMnSc4d1
prokurátora	prokurátor	k1gMnSc4
odstranil	odstranit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
ovšem	ovšem	k9
selhal	selhat	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dent	Dent	k1gInSc1
následně	následně	k6eAd1
uzavřel	uzavřít	k5eAaPmAgInS
spojenectví	spojenectví	k1gNnSc4
s	s	k7c7
Batmanem	Batman	k1gMnSc7
a	a	k8xC
komisařem	komisař	k1gMnSc7
Gordonem	Gordon	k1gMnSc7
<g/>
,	,	kIx,
aby	aby	k9
společnými	společný	k2eAgFnPc7d1
silami	síla	k1gFnPc7
vymýtili	vymýtit	k5eAaPmAgMnP
zločin	zločin	k1gInSc4
z	z	k7c2
gothamských	gothamský	k2eAgFnPc2d1
ulic	ulice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
jeho	jeho	k3xOp3gFnSc3
prchlivé	prchlivý	k2eAgFnSc3d1
povaze	povaha	k1gFnSc3
mu	on	k3xPp3gMnSc3
však	však	k9
Gordon	Gordon	k1gInSc1
s	s	k7c7
Batmanem	Batman	k1gInSc7
příliš	příliš	k6eAd1
nedůvěřovali	důvěřovat	k5eNaImAgMnP
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
se	se	k3xPyFc4
domnívali	domnívat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
záhadným	záhadný	k2eAgInSc7d1
„	„	k?
<g/>
Svátečním	sváteční	k2eAgMnSc7d1
vrahem	vrah	k1gMnSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
vraždil	vraždit	k5eAaImAgInS
členy	člen	k1gInPc4
mafie	mafie	k1gFnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
Mickeyho	Mickey	k1gMnSc2
a	a	k8xC
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
gangu	gang	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojenectví	spojenectví	k1gNnSc1
skončilo	skončit	k5eAaPmAgNnS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
byl	být	k5eAaImAgInS
Dent	Dent	k2eAgInSc1d1
znetvořen	znetvořen	k2eAgInSc1d1
Salem	Salem	k1gInSc1
Maronim	Maronim	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
mu	on	k3xPp3gMnSc3
při	při	k7c6
soudním	soudní	k2eAgNnSc6d1
přelíčení	přelíčení	k1gNnSc6
vylil	vylít	k5eAaPmAgMnS
do	do	k7c2
obličeje	obličej	k1gInSc2
kyselinu	kyselina	k1gFnSc4
sírovou	sírový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Harvey	Harvea	k1gFnPc1
ze	z	k7c2
svého	svůj	k3xOyFgNnSc2
znetvoření	znetvoření	k1gNnSc2
zešílel	zešílet	k5eAaPmAgMnS
a	a	k8xC
začal	začít	k5eAaPmAgMnS
být	být	k5eAaImF
posedlý	posedlý	k2eAgMnSc1d1
dualitou	dualita	k1gFnSc7
a	a	k8xC
protiklady	protiklad	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
se	se	k3xPyFc4
u	u	k7c2
něho	on	k3xPp3gMnSc2
vyvinula	vyvinout	k5eAaPmAgFnS
druhá	druhý	k4xOgFnSc1
osobnost	osobnost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
představovala	představovat	k5eAaImAgFnS
zlotřilého	zlotřilý	k2eAgMnSc4d1
padoucha	padouch	k1gMnSc4
Two-Face	Two-Face	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
nadvládou	nadvláda	k1gFnSc7
nové	nový	k2eAgFnSc2d1
persony	persona	k1gFnSc2
zabil	zabít	k5eAaPmAgMnS
svého	svůj	k3xOyFgMnSc2
zkorumpovaného	zkorumpovaný	k2eAgMnSc2d1
asistenta	asistent	k1gMnSc2
Vernona	Vernon	k1gMnSc2
Wellse	Wells	k1gMnSc2
a	a	k8xC
Falconeho	Falcone	k1gMnSc2
<g/>
,	,	kIx,
nestačil	stačit	k5eNaBmAgInS
však	však	k9
zabít	zabít	k5eAaPmF
Maroniho	Maroni	k1gMnSc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
jej	on	k3xPp3gMnSc4
předstihl	předstihnout	k5eAaPmAgMnS
Alberto	Alberta	k1gFnSc5
Falcone	Falcon	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Two-Face	Two-Face	k1gFnSc1
byl	být	k5eAaImAgInS
za	za	k7c2
vraždy	vražda	k1gFnSc2
nakonec	nakonec	k6eAd1
uvězněn	uvěznit	k5eAaPmNgMnS
v	v	k7c4
Arkham	Arkham	k1gInSc4
Asylum	Asylum	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
příběhu	příběh	k1gInSc2
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Temné	temný	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
Two-Face	Two-Face	k1gFnSc2
z	z	k7c2
ústavu	ústav	k1gInSc2
uprchl	uprchnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
ukradl	ukradnout	k5eAaPmAgMnS
své	svůj	k3xOyFgMnPc4
staré	starý	k2eAgMnPc4d1
spisy	spis	k1gInPc4
a	a	k8xC
začal	začít	k5eAaPmAgInS
s	s	k7c7
rozkladem	rozklad	k1gInSc7
Falconeho	Falcone	k1gMnSc2
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Gothamu	Gotham	k1gInSc6
mezitím	mezitím	k6eAd1
působil	působit	k5eAaImAgMnS
nový	nový	k2eAgMnSc1d1
vrah	vrah	k1gMnSc1
zvaný	zvaný	k2eAgMnSc1d1
„	„	k?
<g/>
Šibeniční	šibeniční	k2eAgMnSc1d1
vrah	vrah	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
následoval	následovat	k5eAaImAgInS
vzor	vzor	k1gInSc4
Svátečního	sváteční	k2eAgMnSc2d1
vraha	vrah	k1gMnSc2
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
s	s	k7c7
tím	ten	k3xDgInSc7
rozdílem	rozdíl	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
nezabíjel	zabíjet	k5eNaImAgInS
gangstery	gangster	k1gMnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
policisty	policista	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dent	Dent	k1gInSc1
před	před	k7c7
vrahem	vrah	k1gMnSc7
zachránil	zachránit	k5eAaPmAgMnS
Jamese	Jamese	k1gFnSc2
Gordona	Gordon	k1gMnSc2
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yQgMnSc4,k3yIgMnSc4
Šibeniční	šibeniční	k2eAgMnSc1d1
vrah	vrah	k1gMnSc1
dokázal	dokázat	k5eAaPmAgMnS
přelstít	přelstít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
zničení	zničení	k1gNnSc3
mafiánů	mafián	k1gMnPc2
Two-Face	Two-Face	k1gFnSc2
využil	využít	k5eAaPmAgInS
asistenci	asistence	k1gFnSc4
ostatních	ostatní	k2eAgMnPc2d1
padouchů	padouch	k1gMnPc2
z	z	k7c2
Batmanovy	Batmanův	k2eAgFnSc2d1
galerie	galerie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS
eliminací	eliminace	k1gFnSc7
Falconeho	Falcone	k1gMnSc2
nástupce	nástupce	k1gMnSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc2
dcery	dcera	k1gFnSc2
Sofie	Sofia	k1gFnSc2
Falcone	Falcon	k1gInSc5
Giganteové	Giganteové	k2eAgNnPc6d1
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
vyklubal	vyklubat	k5eAaPmAgMnS
Šibeniční	šibeniční	k2eAgMnSc1d1
vrah	vrah	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
trvalé	trvalá	k1gFnSc2
převzetí	převzetí	k1gNnSc2
podsvětí	podsvětí	k1gNnSc1
superpadouchy	superpadoucha	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Two-Face	Two-Face	k1gFnSc1
výrazně	výrazně	k6eAd1
zasáhl	zasáhnout	k5eAaPmAgInS
do	do	k7c2
života	život	k1gInSc2
prvních	první	k4xOgInPc2
třech	tři	k4xCgInPc2
Robinů	Robin	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
padouch	padouch	k1gMnSc1
zabil	zabít	k5eAaPmAgMnS
soudce	soudce	k1gMnSc4
Watkinse	Watkins	k1gMnSc4
<g/>
,	,	kIx,
napadl	napadnout	k5eAaPmAgInS
brutálním	brutální	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
Dicka	Dicek	k1gMnSc2
Graysona	Grayson	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
po	po	k7c6
útoku	útok	k1gInSc6
zůstal	zůstat	k5eAaPmAgMnS
v	v	k7c6
bezvědomí	bezvědomí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bruce	Bruce	k1gMnSc1
Wayne	Wayn	k1gInSc5
to	ten	k3xDgNnSc4
dával	dávat	k5eAaImAgInS
za	za	k7c4
vinu	vina	k1gFnSc4
sobě	se	k3xPyFc3
a	a	k8xC
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
chlapce	chlapec	k1gMnSc2
nechat	nechat	k5eAaPmF
dočasně	dočasně	k6eAd1
mimo	mimo	k7c4
službu	služba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
o	o	k7c4
několik	několik	k4yIc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
stal	stát	k5eAaPmAgInS
Robinem	Robin	k1gInSc7
Jason	Jasona	k1gFnPc2
Todd	Todda	k1gFnPc2
<g/>
,	,	kIx,
zjistil	zjistit	k5eAaPmAgMnS
náhodou	náhodou	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
Two-Face	Two-Face	k1gFnSc1
byl	být	k5eAaImAgMnS
zodpovědný	zodpovědný	k2eAgMnSc1d1
za	za	k7c4
smrt	smrt	k1gFnSc4
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
otce	otec	k1gMnSc2
Willise	Willise	k1gFnSc2
Todda	Todd	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
býval	bývat	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc7
komplicem	komplic	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jason	Jason	k1gInSc1
nakonec	nakonec	k6eAd1
pomohl	pomoct	k5eAaPmAgInS
Denta	Dent	k1gMnSc4
dopadnout	dopadnout	k5eAaPmF
a	a	k8xC
ukončit	ukončit	k5eAaPmF
tak	tak	k6eAd1
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
řádění	řádění	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Two-Face	Two-Face	k1gFnSc2
mohl	moct	k5eAaImAgInS
také	také	k9
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
z	z	k7c2
Tima	Tim	k1gInSc2
Drakea	Drakea	k1gMnSc1
stal	stát	k5eAaPmAgMnS
třetí	třetí	k4xOgMnSc1
Robin	robin	k2eAgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgMnS
zachránit	zachránit	k5eAaPmF
Batmana	Batman	k1gMnSc4
a	a	k8xC
Nightwinga	Nightwing	k1gMnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yQgNnSc4,k3yRgNnSc4
padouch	padouch	k1gMnSc1
ohrožoval	ohrožovat	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Two-Face	Two-Face	k1gFnSc1
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
dvouhlavou	dvouhlavý	k2eAgFnSc7d1
mincí	mince	k1gFnSc7
</s>
<s>
V	v	k7c6
komiksu	komiks	k1gInSc6
No	no	k9
Man	Man	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Land	Land	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
byl	být	k5eAaImAgInS
Gotham	Gotham	k1gInSc1
zničen	zničit	k5eAaPmNgInS
zemětřesením	zemětřesení	k1gNnSc7
<g/>
,	,	kIx,
unesl	unést	k5eAaPmAgMnS
Two-Face	Two-Face	k1gFnSc2
komisaře	komisař	k1gMnSc2
Gordona	Gordon	k1gMnSc2
a	a	k8xC
postavil	postavit	k5eAaPmAgMnS
ho	on	k3xPp3gMnSc4
před	před	k7c4
soud	soud	k1gInSc4
za	za	k7c4
jeho	jeho	k3xOp3gInPc4
činy	čin	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
dopomohly	dopomoct	k5eAaPmAgFnP
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
z	z	k7c2
Gotham	Gotham	k1gInSc4
City	city	k1gNnSc1
stalo	stát	k5eAaPmAgNnS
„	„	k?
<g/>
území	území	k1gNnSc1
nikoho	nikdo	k3yNnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
padouch	padouch	k1gMnSc1
v	v	k7c6
tomto	tento	k3xDgNnSc6
přelíčení	přelíčení	k1gNnSc6
figuroval	figurovat	k5eAaImAgMnS
jako	jako	k9
soudce	soudce	k1gMnSc1
a	a	k8xC
žalobce	žalobce	k1gMnSc1
zároveň	zároveň	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gordon	Gordon	k1gMnSc1
hrál	hrát	k5eAaImAgMnS
na	na	k7c4
Two-Faceovu	Two-Faceův	k2eAgFnSc4d1
rozdvojenou	rozdvojený	k2eAgFnSc4d1
psychiku	psychika	k1gFnSc4
a	a	k8xC
jako	jako	k8xC,k8xS
svého	svůj	k3xOyFgMnSc4
obhájce	obhájce	k1gMnSc1
požadoval	požadovat	k5eAaImAgMnS
Harveyho	Harvey	k1gMnSc4
Denta	Dent	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dent	Dent	k1gInSc1
podrobil	podrobit	k5eAaPmAgInS
Two-Face	Two-Face	k1gFnSc2
křížovému	křížový	k2eAgInSc3d1
výslechu	výslech	k1gInSc3
a	a	k8xC
podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
komisaře	komisař	k1gMnSc4
osvobodit	osvobodit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c4
Gotham	Gotham	k1gInSc4
Central	Central	k1gFnPc2
se	se	k3xPyFc4
Two-Face	Two-Face	k1gFnSc2
setkal	setkat	k5eAaPmAgMnS
s	s	k7c7
detektivkou	detektivka	k1gFnSc7
Renee	Renee	k1gFnSc7
Montoyovou	Montoyová	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
v	v	k7c6
padouchovi	padouch	k1gMnSc6
objevila	objevit	k5eAaPmAgFnS
Dentovu	Dentův	k2eAgFnSc4d1
skrytou	skrytý	k2eAgFnSc4d1
osobnost	osobnost	k1gFnSc4
a	a	k8xC
chovala	chovat	k5eAaImAgFnS
se	se	k3xPyFc4
k	k	k7c3
němu	on	k3xPp3gNnSc3
laskavě	laskavě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
zapříčinilo	zapříčinit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
do	do	k7c2
ní	on	k3xPp3gFnSc2
Harvey	Harvea	k1gFnSc2
zamiloval	zamilovat	k5eAaPmAgMnS
<g/>
,	,	kIx,
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
mu	on	k3xPp3gMnSc3
však	však	k9
jeho	jeho	k3xOp3gFnSc7
city	city	k1gFnSc7
neopětovala	opětovat	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Two-Face	Two-Face	k1gFnSc1
poté	poté	k6eAd1
veřejně	veřejně	k6eAd1
odhalil	odhalit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
lesba	lesba	k1gFnSc1
a	a	k8xC
hodil	hodit	k5eAaImAgMnS,k5eAaPmAgMnS
na	na	k7c6
ní	on	k3xPp3gFnSc6
vraždu	vražda	k1gFnSc4
v	v	k7c4
naději	naděje	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
když	když	k8xS
jí	jíst	k5eAaImIp3nS
všechno	všechen	k3xTgNnSc1
sebere	sebrat	k5eAaPmIp3nS
<g/>
,	,	kIx,
nebude	být	k5eNaImBp3nS
mít	mít	k5eAaImF
jinou	jiný	k2eAgFnSc4d1
možnost	možnost	k1gFnSc4
<g/>
,	,	kIx,
než	než	k8xS
zůstat	zůstat	k5eAaPmF
s	s	k7c7
ním	on	k3xPp3gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Detektivku	detektivka	k1gFnSc4
Dentovo	Dentův	k2eAgNnSc1d1
chování	chování	k1gNnSc1
rozzuřilo	rozzuřit	k5eAaPmAgNnS
<g/>
,	,	kIx,
takže	takže	k8xS
na	na	k7c4
něj	on	k3xPp3gMnSc4
zaútočila	zaútočit	k5eAaPmAgFnS
a	a	k8xC
oba	dva	k4xCgMnPc1
začali	začít	k5eAaPmAgMnP
bojovat	bojovat	k5eAaImF
o	o	k7c4
Two-Faceovu	Two-Faceův	k2eAgFnSc4d1
zbraň	zbraň	k1gFnSc4
<g/>
,	,	kIx,
dokud	dokud	k8xS
nezasáhl	zasáhnout	k5eNaPmAgMnS
Batman	Batman	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
Two-Face	Two-Face	k1gFnPc4
vrátil	vrátit	k5eAaPmAgInS
zpátky	zpátky	k6eAd1
do	do	k7c2
Arkhamu	Arkham	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
příběhu	příběh	k1gInSc6
s	s	k7c7
názvem	název	k1gInSc7
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Ticho	ticho	k6eAd1
se	se	k3xPyFc4
Harvey	Harvey	k1gInPc7
Dent	Denta	k1gFnPc2
dočkal	dočkat	k5eAaPmAgMnS
úspěšné	úspěšný	k2eAgFnSc2d1
plastické	plastický	k2eAgFnSc2d1
operace	operace	k1gFnSc2
své	svůj	k3xOyFgFnSc2
tváře	tvář	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
mu	on	k3xPp3gMnSc3
provedl	provést	k5eAaPmAgMnS
geniální	geniální	k2eAgMnSc1d1
chirurg	chirurg	k1gMnSc1
Dr	dr	kA
<g/>
.	.	kIx.
Thomas	Thomas	k1gMnSc1
Elliot	Elliot	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
byl	být	k5eAaImAgInS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
skrytým	skrytý	k2eAgNnSc7d1
vrahem	vrah	k1gMnSc7
Hushem	Hush	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
Dentem	Dento	k1gNnSc7
manipuloval	manipulovat	k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jej	on	k3xPp3gMnSc4
mohl	moct	k5eAaImAgInS
použít	použít	k5eAaPmF
jako	jako	k9
jednoho	jeden	k4xCgMnSc2
z	z	k7c2
pěšáků	pěšák	k1gMnPc2
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
Batmanovi	Batman	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harvey	Harve	k2eAgInPc1d1
ovšem	ovšem	k9
po	po	k7c6
operaci	operace	k1gFnSc6
znovu	znovu	k6eAd1
získal	získat	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
původní	původní	k2eAgFnSc4d1
personu	persona	k1gFnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
Hushe	Hushe	k1gInSc1
zradil	zradit	k5eAaPmAgInS
a	a	k8xC
nakonec	nakonec	k6eAd1
také	také	k9
zastřelil	zastřelit	k5eAaPmAgMnS
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
pomohl	pomoct	k5eAaPmAgMnS
Batmanovi	Batman	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Později	pozdě	k6eAd2
se	se	k3xPyFc4
Batman	Batman	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
opustit	opustit	k5eAaPmF
Gotham	Gotham	k1gInSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
si	se	k3xPyFc3
jako	jako	k9
náhradu	náhrada	k1gFnSc4
za	za	k7c4
sebe	sebe	k3xPyFc4
vybral	vybrat	k5eAaPmAgMnS
Denta	Denta	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
tyto	tento	k3xDgFnPc4
povinnosti	povinnost	k1gFnPc4
přijal	přijmout	k5eAaPmAgMnS
jako	jako	k9
formu	forma	k1gFnSc4
usmíření	usmíření	k1gNnSc3
<g/>
,	,	kIx,
a	a	k8xC
přestože	přestože	k8xS
se	se	k3xPyFc4
zdráhal	zdráhat	k5eAaImAgInS
<g/>
,	,	kIx,
brzy	brzy	k6eAd1
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
práce	práce	k1gFnPc1
bdělého	bdělý	k2eAgMnSc2d1
strážce	strážce	k1gMnSc2
zalíbila	zalíbit	k5eAaPmAgFnS
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
postrádal	postrádat	k5eAaImAgMnS
Batmanův	Batmanův	k2eAgInSc4d1
důvtip	důvtip	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
Temný	temný	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
do	do	k7c2
Gothamu	Gotham	k1gInSc2
vrátil	vrátit	k5eAaPmAgMnS
<g/>
,	,	kIx,
začal	začít	k5eAaPmAgMnS
se	se	k3xPyFc4
Harvey	Harvea	k1gFnSc2
cítit	cítit	k5eAaImF
neužitečný	užitečný	k2eNgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
když	když	k8xS
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
podezřelým	podezřelý	k2eAgMnSc7d1
v	v	k7c6
případu	případ	k1gInSc6
vraždy	vražda	k1gFnSc2
nevýznamných	významný	k2eNgMnPc2d1
padouchů	padouch	k1gMnPc2
<g/>
,	,	kIx,
vyhodil	vyhodit	k5eAaPmAgMnS
do	do	k7c2
povětří	povětří	k1gNnSc2
svůj	svůj	k3xOyFgInSc4
byt	byt	k1gInSc4
a	a	k8xC
uprchl	uprchnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
se	se	k3xPyFc4
u	u	k7c2
něho	on	k3xPp3gMnSc2
znovu	znovu	k6eAd1
probudila	probudit	k5eAaPmAgFnS
Two-Faceova	Two-Faceův	k2eAgFnSc1d1
osobnost	osobnost	k1gFnSc1
<g/>
,	,	kIx,
takže	takže	k8xS
vypustil	vypustit	k5eAaPmAgInS
většinu	většina	k1gFnSc4
zvířat	zvíře	k1gNnPc2
v	v	k7c6
zoo	zoo	k1gFnSc6
a	a	k8xC
zabil	zabít	k5eAaPmAgMnS
čtyři	čtyři	k4xCgMnPc4
policisty	policista	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
však	však	k9
zneškodněn	zneškodnit	k5eAaPmNgInS
Batmanem	Batman	k1gMnSc7
a	a	k8xC
Robinem	Robin	k1gMnSc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
později	pozdě	k6eAd2
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c7
vraždou	vražda	k1gFnSc7
padouchů	padouch	k1gMnPc2
stál	stát	k5eAaImAgMnS
finančník	finančník	k1gMnSc1
zvaný	zvaný	k2eAgInSc1d1
Great	Great	k1gInSc1
White	Whit	k1gInSc5
Shark	Shark	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
měl	mít	k5eAaImAgMnS
Batman	Batman	k1gMnSc1
zdánlivě	zdánlivě	k6eAd1
zemřít	zemřít	k5eAaPmF
<g/>
,	,	kIx,
zjistil	zjistit	k5eAaPmAgMnS
Dent	Dent	k1gMnSc1
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnSc3
kápi	kápě	k1gFnSc3
nosí	nosit	k5eAaImIp3nS
někdo	někdo	k3yInSc1
jiný	jiný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pronajal	pronajmout	k5eAaPmAgMnS
si	se	k3xPyFc3
tedy	tedy	k9
teleport	teleport	k1gInSc4
a	a	k8xC
dokázal	dokázat	k5eAaPmAgMnS
proniknout	proniknout	k5eAaPmF
do	do	k7c2
batcave	batcavat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
nový	nový	k2eAgMnSc1d1
Temný	temný	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
jeskyni	jeskyně	k1gFnSc4
prozkoumával	prozkoumávat	k5eAaImAgMnS
<g/>
,	,	kIx,
zasáhl	zasáhnout	k5eAaPmAgMnS
jej	on	k3xPp3gMnSc4
Two-Face	Two-Face	k1gFnPc1
uspávacími	uspávací	k2eAgFnPc7d1
šipkami	šipka	k1gFnPc7
<g/>
,	,	kIx,
načež	načež	k6eAd1
ho	on	k3xPp3gMnSc4
začal	začít	k5eAaPmAgMnS
mučit	mučit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
vyřešil	vyřešit	k5eAaPmAgInS
až	až	k9
příchod	příchod	k1gInSc1
komorníka	komorník	k1gMnSc2
Alfreda	Alfred	k1gMnSc2
Pennywortha	Pennyworth	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Později	pozdě	k6eAd2
se	se	k3xPyFc4
padouch	padouch	k1gMnSc1
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
konfliktu	konflikt	k1gInSc2
s	s	k7c7
novou	nový	k2eAgFnSc7d1
okresní	okresní	k2eAgFnSc7d1
prokurátorkou	prokurátorka	k1gFnSc7
<g/>
,	,	kIx,
Kate	kat	k1gInSc5
Spencerovou	Spencerový	k2eAgFnSc4d1
<g/>
,	,	kIx,
známou	známý	k2eAgFnSc4d1
také	také	k9
pod	pod	k7c7
identitou	identita	k1gFnSc7
mstitelky	mstitelka	k1gFnSc2
Manhunter	Manhuntra	k1gFnPc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
plánoval	plánovat	k5eAaImAgMnS
zavraždit	zavraždit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
zatčení	zatčení	k1gNnSc4
padoucha	padouch	k1gMnSc2
Black	Black	k1gMnSc1
Maska	maska	k1gFnSc1
se	se	k3xPyFc4
Two-Face	Two-Face	k1gFnSc2
začal	začít	k5eAaPmAgInS
snažit	snažit	k5eAaImF
o	o	k7c6
znovuzískání	znovuzískání	k1gNnSc6
své	svůj	k3xOyFgFnSc2
dřívější	dřívější	k2eAgFnSc2d1
moci	moc	k1gFnSc2
nad	nad	k7c7
Gothamem	Gotham	k1gInSc7
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
ovšem	ovšem	k9
přitáhl	přitáhnout	k5eAaPmAgInS
pozornost	pozornost	k1gFnSc4
FBI	FBI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
řadách	řada	k1gFnPc6
špeha	špeh	k1gMnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
dal	dát	k5eAaPmAgMnS
na	na	k7c4
útěk	útěk	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgMnS
zrazen	zradit	k5eAaPmNgMnS
svými	svůj	k3xOyFgMnPc7
stoupenci	stoupenec	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
ho	on	k3xPp3gMnSc4
pobodali	pobodat	k5eAaPmAgMnP
a	a	k8xC
shodili	shodit	k5eAaPmAgMnP
z	z	k7c2
mostu	most	k1gInSc2
do	do	k7c2
řeky	řeka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Současnost	současnost	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
restartování	restartování	k1gNnSc3
DC	DC	kA
kontinuity	kontinuita	k1gFnPc4
s	s	k7c7
názvem	název	k1gInSc7
New	New	k1gFnSc1
52	#num#	k4
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
navázalo	navázat	k5eAaPmAgNnS
Znovuzrození	znovuzrození	k1gNnSc1
hrdinů	hrdina	k1gMnPc2
DC	DC	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
obou	dva	k4xCgFnPc6
časových	časový	k2eAgFnPc6d1
osách	osa	k1gFnPc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
výrazným	výrazný	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
v	v	k7c6
Two-Faceově	Two-Faceův	k2eAgInSc6d1
původu	původ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Harvey	Harve	k2eAgInPc1d1
Dent	Dent	k1gInSc1
vyrůstal	vyrůstat	k5eAaImAgInS
v	v	k7c4
Gotham	Gotham	k1gInSc4
City	City	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
žil	žít	k5eAaImAgMnS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
despotickým	despotický	k2eAgMnSc7d1
otcem	otec	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
byl	být	k5eAaImAgMnS
alkoholik	alkoholik	k1gMnSc1
a	a	k8xC
gambler	gambler	k1gMnSc1
a	a	k8xC
svého	svůj	k3xOyFgMnSc4
syna	syn	k1gMnSc4
často	často	k6eAd1
bil	bít	k5eAaImAgMnS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	se	k3xPyFc4
o	o	k7c6
všech	všecek	k3xTgInPc6
trestech	trest	k1gInPc6
rozhodoval	rozhodovat	k5eAaImAgInS
hozením	hození	k1gNnSc7
mince	mince	k1gFnSc2
pro	pro	k7c4
štěstí	štěstí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vraždě	vražda	k1gFnSc3
Wayneových	Wayneův	k2eAgMnPc2d1
<g/>
,	,	kIx,
Harvey	Harvea	k1gFnSc2
svého	svůj	k3xOyFgMnSc4
otce	otec	k1gMnSc4
svázal	svázat	k5eAaPmAgInS
a	a	k8xC
ponechal	ponechat	k5eAaPmAgInS
jej	on	k3xPp3gMnSc4
několik	několik	k4yIc4
dní	den	k1gInPc2
zavřeného	zavřený	k2eAgInSc2d1
v	v	k7c6
domě	dům	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
ho	on	k3xPp3gInSc4
nakonec	nakonec	k6eAd1
našla	najít	k5eAaPmAgFnS
policie	policie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harvey	Harvea	k1gFnSc2
byl	být	k5eAaImAgInS
poslán	poslat	k5eAaPmNgMnS
do	do	k7c2
Arkhamova	Arkhamův	k2eAgInSc2d1
azylu	azyl	k1gInSc2
pro	pro	k7c4
problémové	problémový	k2eAgFnPc4d1
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
setkal	setkat	k5eAaPmAgMnS
s	s	k7c7
Brucem	Bruce	k1gMnSc7
Waynem	Wayn	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
zde	zde	k6eAd1
nacházel	nacházet	k5eAaImAgMnS
z	z	k7c2
důvodu	důvod	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
naučil	naučit	k5eAaPmAgMnS
lépe	dobře	k6eAd2
zvládat	zvládat	k5eAaImF
zármutek	zármutek	k1gInSc4
ze	z	k7c2
ztráty	ztráta	k1gFnSc2
svých	svůj	k3xOyFgMnPc2
rodičů	rodič	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chlapci	chlapec	k1gMnPc1
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
dobrými	dobrý	k2eAgMnPc7d1
přáteli	přítel	k1gMnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
své	svůj	k3xOyFgFnSc2
pravé	pravý	k2eAgFnSc2d1
identity	identita	k1gFnSc2
si	se	k3xPyFc3
odhalili	odhalit	k5eAaPmAgMnP
až	až	k9
o	o	k7c4
několik	několik	k4yIc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
mezi	mezi	k7c7
sebou	se	k3xPyFc7
uzavřeli	uzavřít	k5eAaPmAgMnP
smlouvu	smlouva	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
jednoho	jeden	k4xCgInSc2
dne	den	k1gInSc2
zabijí	zabít	k5eAaPmIp3nP
nepřítele	nepřítel	k1gMnSc4
toho	ten	k3xDgNnSc2
druhého	druhý	k4xOgMnSc4
—	—	k?
Harvey	Harvey	k1gInPc1
zabije	zabít	k5eAaPmIp3nS
Joea	Joe	k2eAgFnSc1d1
Chilla	Chilla	k1gFnSc1
a	a	k8xC
Bruce	Bruce	k1gFnSc1
Harveyho	Harvey	k1gMnSc2
otce	otec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
mezitím	mezitím	k6eAd1
se	se	k3xPyFc4
Harvey	Harvea	k1gFnSc2
dostal	dostat	k5eAaPmAgMnS
domů	domů	k6eAd1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
začnou	začít	k5eAaPmIp3nP
nanovo	nanovo	k6eAd1
<g/>
,	,	kIx,
takže	takže	k8xS
roztavil	roztavit	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
starou	starý	k2eAgFnSc4d1
minci	mince	k1gFnSc4
a	a	k8xC
přerazil	přerazit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
na	na	k7c6
minci	mince	k1gFnSc6
s	s	k7c7
dvěma	dva	k4xCgFnPc7
hlavami	hlava	k1gFnPc7
jako	jako	k8xC,k8xS
symbol	symbol	k1gInSc1
<g/>
,	,	kIx,
díky	díky	k7c3
kterému	který	k3yQgInSc3,k3yRgInSc3,k3yIgInSc3
by	by	kYmCp3nP
synovi	syn	k1gMnSc3
již	již	k9
nikdy	nikdy	k6eAd1
neublížil	ublížit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harvey	Harveum	k1gNnPc7
následně	následně	k6eAd1
stáhl	stáhnout	k5eAaPmAgMnS
dohodu	dohoda	k1gFnSc4
s	s	k7c7
Brucem	Bruce	k1gMnSc7
a	a	k8xC
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
dát	dát	k5eAaPmF
svému	svůj	k3xOyFgMnSc3
otci	otec	k1gMnSc3
druhou	druhý	k4xOgFnSc4
šanci	šance	k1gFnSc4
<g/>
,	,	kIx,
ovšem	ovšem	k9
podle	podle	k7c2
Bruce	Bruec	k1gInSc2
to	ten	k3xDgNnSc4
byla	být	k5eAaImAgFnS
neuvěřitelně	uvěřitelně	k6eNd1
naivní	naivní	k2eAgFnSc1d1
volba	volba	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
dospělosti	dospělost	k1gFnSc6
se	se	k3xPyFc4
z	z	k7c2
Harveyho	Harvey	k1gMnSc2
Denta	Dento	k1gNnSc2
stal	stát	k5eAaPmAgMnS
kvalifikovaný	kvalifikovaný	k2eAgMnSc1d1
obhájce	obhájce	k1gMnSc1
pro	pro	k7c4
zločince	zločinec	k1gMnPc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
si	se	k3xPyFc3
v	v	k7c6
Gothamu	Gotham	k1gInSc6
rychle	rychle	k6eAd1
udělal	udělat	k5eAaPmAgMnS
jméno	jméno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jednom	jeden	k4xCgInSc6
ze	z	k7c2
svých	svůj	k3xOyFgInPc2
večírků	večírek	k1gInPc2
jej	on	k3xPp3gInSc2
Bruce	Bruec	k1gInSc2
Wayne	Wayn	k1gInSc5
seznámil	seznámit	k5eAaPmAgMnS
s	s	k7c7
Gildou	gilda	k1gFnSc7
Goldovou	Goldová	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
za	za	k7c2
Harveyho	Harvey	k1gMnSc2
provdala	provdat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
z	z	k7c2
Waynea	Wayne	k1gInSc2
stal	stát	k5eAaPmAgMnS
Batman	Batman	k1gMnSc1
<g/>
,	,	kIx,
vypracoval	vypracovat	k5eAaPmAgInS
se	se	k3xPyFc4
Dent	Dent	k1gInSc1
v	v	k7c4
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
nejlepších	dobrý	k2eAgMnPc2d3
obhájců	obhájce	k1gMnPc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
úspěšně	úspěšně	k6eAd1
osvobodil	osvobodit	k5eAaPmAgInS
řadu	řada	k1gFnSc4
zločinců	zločinec	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
předtím	předtím	k6eAd1
Temný	temný	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
dopadl	dopadnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harvey	Harveum	k1gNnPc7
také	také	k6eAd1
působil	působit	k5eAaImAgMnS
jako	jako	k9
právník	právník	k1gMnSc1
zločinecké	zločinecký	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
McKillenových	McKillenových	k2eAgFnSc2d1
<g/>
,	,	kIx,
dokud	dokud	k8xS
o	o	k7c4
práci	práce	k1gFnSc4
s	s	k7c7
tak	tak	k6eAd1
nebezpečnou	bezpečný	k2eNgFnSc7d1
klientelou	klientela	k1gFnSc7
nezačal	začít	k5eNaPmAgMnS
pochybovat	pochybovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bruce	Bruce	k1gMnSc1
Wayne	Wayn	k1gInSc5
navrhl	navrhnout	k5eAaPmAgInS
Dentovi	Dent	k1gMnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
okresním	okresní	k2eAgMnSc7d1
prokurátorem	prokurátor	k1gMnSc7
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
cítil	cítit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
svoje	svůj	k3xOyFgFnPc4
dovednosti	dovednost	k1gFnPc4
uplatnil	uplatnit	k5eAaPmAgMnS
lépe	dobře	k6eAd2
na	na	k7c6
straně	strana	k1gFnSc6
zákona	zákon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harvey	Harveum	k1gNnPc7
se	se	k3xPyFc4
postupně	postupně	k6eAd1
stal	stát	k5eAaPmAgInS
nekompromisním	kompromisní	k2eNgMnSc7d1
okresním	okresní	k2eAgMnSc7d1
prokurátorem	prokurátor	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
vedl	vést	k5eAaImAgMnS
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
proti	proti	k7c3
gothamským	gothamský	k2eAgMnPc3d1
zločincům	zločinec	k1gMnPc3
a	a	k8xC
zkorumpovaným	zkorumpovaný	k2eAgMnPc3d1
politikům	politik	k1gMnPc3
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
předtím	předtím	k6eAd1
bránil	bránit	k5eAaImAgMnS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
často	často	k6eAd1
spolupracoval	spolupracovat	k5eAaImAgMnS
s	s	k7c7
Batmanem	Batman	k1gMnSc7
a	a	k8xC
komisařem	komisař	k1gMnSc7
Gordonem	Gordon	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
svého	svůj	k3xOyFgInSc2
posledního	poslední	k2eAgInSc2d1
případu	případ	k1gInSc2
se	se	k3xPyFc4
Dent	Dent	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
spojenci	spojenec	k1gMnPc1
zaměřili	zaměřit	k5eAaPmAgMnP
na	na	k7c4
sestry	sestra	k1gFnPc4
McKillenovy	McKillenův	k2eAgFnPc4d1
—	—	k?
Harveyho	Harvey	k1gMnSc2
bývalé	bývalý	k2eAgFnSc2d1
klientky	klientka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
Dent	Dent	k1gInSc1
ujal	ujmout	k5eAaPmAgInS
vedení	vedení	k1gNnSc4
<g/>
,	,	kIx,
podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
obě	dva	k4xCgFnPc1
sestry	sestra	k1gFnPc1
zatknout	zatknout	k5eAaPmF
a	a	k8xC
poslat	poslat	k5eAaPmF
do	do	k7c2
vězení	vězení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
plánu	plán	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
zahrnoval	zahrnovat	k5eAaImAgInS
sebevraždu	sebevražda	k1gFnSc4
Shannon	Shannon	k1gInSc1
McKillenové	McKillenová	k1gFnPc1
a	a	k8xC
útěk	útěk	k1gInSc4
Erin	Erin	k1gInSc1
McKillenové	McKillenová	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
přestrojila	přestrojit	k5eAaPmAgFnS
za	za	k7c4
sestřino	sestřin	k2eAgNnSc4d1
mrtvé	mrtvý	k2eAgNnSc4d1
tělo	tělo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
byla	být	k5eAaImAgFnS
Erin	Erin	k1gInSc4
na	na	k7c6
svobodě	svoboda	k1gFnSc6
<g/>
,	,	kIx,
vystopovala	vystopovat	k5eAaPmAgFnS
Harveyho	Harvey	k1gMnSc4
a	a	k8xC
znetvořila	znetvořit	k5eAaPmAgFnS
mu	on	k3xPp3gMnSc3
půlku	půlka	k1gFnSc4
obličeje	obličej	k1gInSc2
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
předtím	předtím	k6eAd1
však	však	k9
stihla	stihnout	k5eAaPmAgFnS
zabít	zabít	k5eAaPmF
jeho	jeho	k3xOp3gFnSc4
manželku	manželka	k1gFnSc4
Gildu	gilda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
tomto	tento	k3xDgInSc6
traumatickém	traumatický	k2eAgInSc6d1
zážitku	zážitek	k1gInSc6
se	se	k3xPyFc4
Dent	Dent	k1gMnSc1
zbláznil	zbláznit	k5eAaPmAgMnS
<g/>
,	,	kIx,
vyvinul	vyvinout	k5eAaPmAgMnS
si	se	k3xPyFc3
u	u	k7c2
sebe	sebe	k3xPyFc4
druhou	druhý	k4xOgFnSc4
osobnost	osobnost	k1gFnSc4
a	a	k8xC
ke	k	k7c3
všem	všecek	k3xTgNnPc3
důležitým	důležitý	k2eAgNnPc3d1
rozhodnutím	rozhodnutí	k1gNnPc3
začal	začít	k5eAaPmAgInS
používat	používat	k5eAaImF
hod	hod	k1gInSc1
mincí	mince	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Harveyho	Harveyze	k6eAd1
narůstající	narůstající	k2eAgNnSc1d1
šílenství	šílenství	k1gNnSc1
ho	on	k3xPp3gMnSc4
brzy	brzy	k6eAd1
přivedlo	přivést	k5eAaPmAgNnS
do	do	k7c2
kriminálního	kriminální	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
se	se	k3xPyFc4
z	z	k7c2
něho	on	k3xPp3gMnSc2
stal	stát	k5eAaPmAgMnS
nový	nový	k2eAgMnSc1d1
gothamský	gothamský	k2eAgMnSc1d1
padouch	padouch	k1gMnSc1
Two-Face	Two-Face	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
nato	nato	k6eAd1
Dent	Dent	k1gMnSc1
vystopoval	vystopovat	k5eAaPmAgMnS
svého	svůj	k3xOyFgMnSc4
otce	otec	k1gMnSc4
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yIgMnSc4,k3yQgMnSc4
následně	následně	k6eAd1
několik	několik	k4yIc4
let	léto	k1gNnPc2
držel	držet	k5eAaImAgInS
svázaného	svázaný	k2eAgInSc2d1
na	na	k7c6
skrytém	skrytý	k2eAgNnSc6d1
místě	místo	k1gNnSc6
v	v	k7c6
Renu	Renus	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
Války	válka	k1gFnSc2
vtipů	vtip	k1gInPc2
a	a	k8xC
hádanek	hádanka	k1gFnPc2
se	se	k3xPyFc4
Two-Face	Two-Face	k1gFnSc2
rozhodl	rozhodnout	k5eAaPmAgInS
připojit	připojit	k5eAaPmF
k	k	k7c3
Riddlerově	Riddlerův	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
proti	proti	k7c3
Jokerovi	Joker	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
týdnech	týden	k1gInPc6
bojů	boj	k1gInPc2
se	se	k3xPyFc4
Batmanovi	Batman	k1gMnSc3
podařilo	podařit	k5eAaPmAgNnS
tuto	tento	k3xDgFnSc4
válku	válka	k1gFnSc4
potlačit	potlačit	k5eAaPmF
a	a	k8xC
Dent	Dent	k1gInSc1
byl	být	k5eAaImAgInS
i	i	k9
se	s	k7c7
zbytkem	zbytek	k1gInSc7
armády	armáda	k1gFnSc2
zadržen	zadržen	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Někdy	někdy	k6eAd1
po	po	k7c6
smrti	smrt	k1gFnSc6
druhého	druhý	k4xOgMnSc2
Robina	robin	k2eAgMnSc2d1
<g/>
,	,	kIx,
Jasona	Jason	k1gMnSc2
Todda	Todd	k1gMnSc2
<g/>
,	,	kIx,
Two-Face	Two-Face	k1gFnSc2
uprchl	uprchnout	k5eAaPmAgInS
z	z	k7c2
vězení	vězení	k1gNnSc2
a	a	k8xC
unesl	unést	k5eAaPmAgInS
Batmana	Batman	k1gMnSc4
a	a	k8xC
Nightwinga	Nightwing	k1gMnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yQgNnSc4,k3yRgNnSc4
musel	muset	k5eAaImAgMnS
nakonec	nakonec	k6eAd1
osvobodit	osvobodit	k5eAaPmF
<g/>
,	,	kIx,
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
netrénovaný	trénovaný	k2eNgMnSc1d1
<g/>
,	,	kIx,
Tim	Tim	k?
Drake	Drake	k1gInSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
přijatý	přijatý	k2eAgInSc1d1
jako	jako	k8xC,k8xS
třetí	třetí	k4xOgInSc1
Robin	robin	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
několik	několik	k4yIc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
Dent	Dent	k1gInSc1
jedním	jeden	k4xCgInSc7
z	z	k7c2
vězňů	vězeň	k1gMnPc2
<g/>
,	,	kIx,
kterým	který	k3yQgMnPc3,k3yIgMnPc3,k3yRgMnPc3
byl	být	k5eAaImAgInS
podán	podat	k5eAaPmNgInS
experimentální	experimentální	k2eAgInSc1d1
jed	jed	k1gInSc1
vytvořený	vytvořený	k2eAgInSc4d1
Banem	Banem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlivem	vlivem	k7c2
jedu	jed	k1gInSc2
se	se	k3xPyFc4
z	z	k7c2
Harveyho	Harvey	k1gMnSc2
stal	stát	k5eAaPmAgMnS
svalnatý	svalnatý	k2eAgMnSc1d1
hromotluk	hromotluk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
sám	sám	k3xTgMnSc1
sebe	sebe	k3xPyFc4
nazval	nazvat	k5eAaPmAgMnS,k5eAaBmAgMnS
„	„	k?
<g/>
One-Facem	One-Face	k1gMnSc7
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
podobě	podoba	k1gFnSc6
však	však	k9
nevydržel	vydržet	k5eNaPmAgMnS
dlouho	dlouho	k6eAd1
a	a	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
ho	on	k3xPp3gMnSc4
porazil	porazit	k5eAaPmAgMnS
Batman	Batman	k1gMnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
normálu	normál	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c4
něco	něco	k3yInSc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
do	do	k7c2
Gothamu	Gotham	k1gInSc2
vrátila	vrátit	k5eAaPmAgFnS
Erin	Erin	k1gInSc4
McKillenová	McKillenová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
ihned	ihned	k6eAd1
přitáhla	přitáhnout	k5eAaPmAgFnS
pozornost	pozornost	k1gFnSc4
Two-Face	Two-Face	k1gFnSc2
a	a	k8xC
Batmana	Batman	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dent	Dent	k1gMnSc1
si	se	k3xPyFc3
najal	najmout	k5eAaPmAgMnS
skupinu	skupina	k1gFnSc4
zločinců	zločinec	k1gMnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Erin	Erin	k1gInSc4
zavraždili	zavraždit	k5eAaPmAgMnP
<g/>
,	,	kIx,
avšak	avšak	k8xC
Batman	Batman	k1gMnSc1
ji	on	k3xPp3gFnSc4
zachránil	zachránit	k5eAaPmAgMnS
a	a	k8xC
odvezl	odvézt	k5eAaPmAgMnS
na	na	k7c4
Wayneovo	Wayneův	k2eAgNnSc4d1
panství	panství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
McKillenová	McKillenová	k1gFnSc1
pokusila	pokusit	k5eAaPmAgFnS
znovu	znovu	k6eAd1
uprchnout	uprchnout	k5eAaPmF
ze	z	k7c2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
omámena	omámen	k2eAgFnSc1d1
svým	svůj	k1gMnSc7
bratrancem	bratranec	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
spolčil	spolčit	k5eAaPmAgMnS
s	s	k7c7
Two-Facem	Two-Faec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
Erin	Erin	k1gInSc1
znetvořil	znetvořit	k5eAaPmAgInS
stejnou	stejný	k2eAgFnSc7d1
kyselinou	kyselina	k1gFnSc7
<g/>
,	,	kIx,
jakou	jaký	k3yQgFnSc7,k3yIgFnSc7,k3yRgFnSc7
použila	použít	k5eAaPmAgFnS
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
na	na	k7c4
něj	on	k3xPp3gMnSc4
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
zásluhou	zásluha	k1gFnSc7
Batmanovy	Batmanův	k2eAgFnSc2d1
pohotovosti	pohotovost	k1gFnSc2
to	ten	k3xDgNnSc1
nedopadlo	dopadnout	k5eNaPmAgNnS
tak	tak	k6eAd1
fatálně	fatálně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
<g/>
,	,	kIx,
když	když	k8xS
Temný	temný	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
úspěšně	úspěšně	k6eAd1
dopravil	dopravit	k5eAaPmAgMnS
McKillenovou	McKillenová	k1gFnSc4
na	na	k7c6
policejní	policejní	k2eAgFnSc6d1
stanici	stanice	k1gFnSc6
<g/>
,	,	kIx,
vydal	vydat	k5eAaPmAgMnS
se	se	k3xPyFc4
pomoci	pomoct	k5eAaPmF
Harveymu	Harveym	k1gInSc2
<g/>
,	,	kIx,
kterého	který	k3yQgMnSc4,k3yIgMnSc4,k3yRgMnSc4
mezitím	mezitím	k6eAd1
zajali	zajmout	k5eAaPmAgMnP
gangsteři	gangster	k1gMnPc1
a	a	k8xC
nechali	nechat	k5eAaPmAgMnP
jej	on	k3xPp3gMnSc4
v	v	k7c6
hořící	hořící	k2eAgFnSc6d1
budově	budova	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
záchrany	záchrana	k1gFnSc2
vyšlo	vyjít	k5eAaPmAgNnS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
Dent	Dent	k1gMnSc1
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
věděl	vědět	k5eAaImAgMnS
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
skrývá	skrývat	k5eAaImIp3nS
pod	pod	k7c7
maskou	maska	k1gFnSc7
Batmana	Batman	k1gMnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
Bruce	Bruce	k1gMnSc1
vinil	vinit	k5eAaImAgMnS
ze	z	k7c2
smrti	smrt	k1gFnSc2
své	svůj	k3xOyFgFnSc2
manželky	manželka	k1gFnSc2
a	a	k8xC
neustále	neustále	k6eAd1
se	se	k3xPyFc4
rozhodoval	rozhodovat	k5eAaImAgInS
<g/>
,	,	kIx,
jestli	jestli	k8xS
ho	on	k3xPp3gMnSc4
má	mít	k5eAaImIp3nS
zabít	zabít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
Two-Face	Two-Face	k1gFnSc2
střetl	střetnout	k5eAaPmAgMnS
s	s	k7c7
Gordonem	Gordon	k1gMnSc7
<g/>
,	,	kIx,
kterého	který	k3yRgNnSc2,k3yIgNnSc2,k3yQgNnSc2
se	se	k3xPyFc4
chystal	chystat	k5eAaImAgMnS
zastřelit	zastřelit	k5eAaPmF
<g/>
,	,	kIx,
naštěstí	naštěstí	k6eAd1
však	však	k9
opět	opět	k6eAd1
zasáhl	zasáhnout	k5eAaPmAgMnS
Batman	Batman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
Dent	Dent	k1gMnSc1
utekl	utéct	k5eAaPmAgMnS
do	do	k7c2
svého	svůj	k3xOyFgInSc2
úkrytu	úkryt	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
nad	nad	k7c7
fotografií	fotografia	k1gFnSc7
své	svůj	k3xOyFgFnSc2
zesnulé	zesnulý	k2eAgFnSc2d1
manželky	manželka	k1gFnSc2
prostřelil	prostřelit	k5eAaPmAgMnS
hlavu	hlava	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pokus	pokus	k1gInSc1
o	o	k7c4
sebevraždu	sebevražda	k1gFnSc4
mu	on	k3xPp3gMnSc3
nevyšel	vyjít	k5eNaPmAgMnS
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
kulka	kulka	k1gFnSc1
minula	minout	k5eAaImAgFnS
mozek	mozek	k1gInSc4
<g/>
,	,	kIx,
mohla	moct	k5eAaImAgFnS
však	však	k9
za	za	k7c4
zhoršení	zhoršení	k1gNnSc4
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
psychického	psychický	k2eAgInSc2d1
stavu	stav	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
do	do	k7c2
té	ten	k3xDgFnSc2
míry	míra	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Two-Faceova	Two-Faceův	k2eAgFnSc1d1
osobnost	osobnost	k1gFnSc1
stala	stát	k5eAaPmAgFnS
ještě	ještě	k9
více	hodně	k6eAd2
dominantnější	dominantní	k2eAgNnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zesláblý	zesláblý	k2eAgInSc1d1
Harvey	Harveum	k1gNnPc7
Dent	Dent	k1gMnSc1
požádal	požádat	k5eAaPmAgMnS
Batmana	Batman	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mu	on	k3xPp3gMnSc3
pomohl	pomoct	k5eAaPmAgMnS
se	se	k3xPyFc4
dostat	dostat	k5eAaPmF
na	na	k7c4
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
dětství	dětství	k1gNnSc6
poprvé	poprvé	k6eAd1
potkali	potkat	k5eAaPmAgMnP
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
se	se	k3xPyFc4
zde	zde	k6eAd1
nacházel	nacházet	k5eAaImAgMnS
lék	lék	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
by	by	kYmCp3nS
Harveymu	Harveym	k1gInSc3
mohl	moct	k5eAaImAgInS
pomoci	pomoct	k5eAaPmF
se	se	k3xPyFc4
jednou	jednou	k6eAd1
provždy	provždy	k6eAd1
zbavit	zbavit	k5eAaPmF
jeho	jeho	k3xOp3gFnSc3
temné	temný	k2eAgFnSc3d1
osobnosti	osobnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Batman	Batman	k1gMnSc1
souhlasil	souhlasit	k5eAaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
společně	společně	k6eAd1
vydali	vydat	k5eAaPmAgMnP
na	na	k7c4
800	#num#	k4
kilometrů	kilometr	k1gInPc2
dlouhou	dlouhý	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
během	během	k7c2
cesty	cesta	k1gFnSc2
dostal	dostat	k5eAaPmAgMnS
k	k	k7c3
moci	moc	k1gFnSc3
opět	opět	k6eAd1
Two-Face	Two-Face	k1gFnSc2
<g/>
,	,	kIx,
nabídl	nabídnout	k5eAaPmAgMnS
ostatním	ostatní	k2eAgMnSc7d1
padouchům	padouch	k1gMnPc3
miliony	milion	k4xCgInPc4
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
Temného	temný	k2eAgMnSc4d1
rytíře	rytíř	k1gMnSc4
zneškodní	zneškodnit	k5eAaPmIp3nS
<g/>
,	,	kIx,
v	v	k7c6
opačném	opačný	k2eAgInSc6d1
případě	případ	k1gInSc6
vyhrožoval	vyhrožovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
o	o	k7c6
všech	všecek	k3xTgFnPc6
zveřejnil	zveřejnit	k5eAaPmAgInS
tajné	tajný	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
Batman	Batman	k1gMnSc1
vypořádal	vypořádat	k5eAaPmAgMnS
s	s	k7c7
několika	několik	k4yIc7
zločinci	zločinec	k1gMnPc7
a	a	k8xC
dorazil	dorazit	k5eAaPmAgMnS
nakonec	nakonec	k6eAd1
na	na	k7c4
místo	místo	k1gNnSc4
určení	určení	k1gNnSc2
<g/>
,	,	kIx,
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
zde	zde	k6eAd1
nenachází	nacházet	k5eNaImIp3nS
žádný	žádný	k3yNgInSc1
lék	lék	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
chemikálie	chemikálie	k1gFnPc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
by	by	kYmCp3nS
Denta	Dent	k1gMnSc4
natrvalo	natrvalo	k6eAd1
přeměnila	přeměnit	k5eAaPmAgFnS
v	v	k7c4
Two-Face	Two-Face	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harveyho	Harvey	k1gMnSc2
osobnost	osobnost	k1gFnSc4
se	se	k3xPyFc4
vzdala	vzdát	k5eAaPmAgFnS
naděje	naděje	k1gFnSc1
a	a	k8xC
chtěla	chtít	k5eAaImAgFnS
se	se	k3xPyFc4
zřeknout	zřeknout	k5eAaPmF
kontroly	kontrola	k1gFnPc4
nad	nad	k7c7
svou	svůj	k3xOyFgFnSc7
horší	zlý	k2eAgFnSc7d2
polovinou	polovina	k1gFnSc7
<g/>
,	,	kIx,
o	o	k7c6
čemž	což	k3yRnSc6,k3yQnSc6
neměl	mít	k5eNaImAgMnS
tušení	tušení	k1gNnSc4
ani	ani	k8xC
sám	sám	k3xTgInSc4
Two-Face	Two-Face	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Harvey	Harvey	k1gInPc4
hrozil	hrozit	k5eAaImAgMnS
zničením	zničení	k1gNnSc7
Gothamu	Gotham	k1gInSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
mu	on	k3xPp3gMnSc3
nebude	být	k5eNaImBp3nS
podána	podat	k5eAaPmNgFnS
jeho	jeho	k3xOp3gFnSc1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
aplikoval	aplikovat	k5eAaBmAgMnS
mu	on	k3xPp3gMnSc3
Batman	Batman	k1gMnSc1
tajně	tajně	k6eAd1
jinou	jiný	k2eAgFnSc4d1
chemikálii	chemikálie	k1gFnSc4
<g/>
,	,	kIx,
díky	díky	k7c3
které	který	k3yQgFnSc3,k3yIgFnSc3,k3yRgFnSc3
byl	být	k5eAaImAgMnS
Harvey	Harvea	k1gFnSc2
vůči	vůči	k7c3
vlastnímu	vlastní	k2eAgInSc3d1
„	„	k?
<g/>
léku	lék	k1gInSc3
<g/>
“	“	k?
imunní	imunní	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dent	Dent	k1gInSc1
si	se	k3xPyFc3
nezasloužil	zasloužit	k5eNaPmAgInS
být	být	k5eAaImF
natrvalo	natrvalo	k6eAd1
Harveym	Harveym	k1gInSc4
nebo	nebo	k8xC
Two-Facem	Two-Faec	k1gInSc7
<g/>
,	,	kIx,
takže	takže	k8xS
obě	dva	k4xCgFnPc1
persony	persona	k1gFnPc1
byly	být	k5eAaImAgFnP
nuceny	nucen	k2eAgFnPc1d1
zůstat	zůstat	k5eAaPmF
navždy	navždy	k6eAd1
spolu	spolu	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Alternativní	alternativní	k2eAgFnSc1d1
verze	verze	k1gFnSc1
</s>
<s>
Země-tři	Země-tr	k1gMnPc1
</s>
<s>
Na	na	k7c6
Zemi-tři	Zemi-tr	k1gMnPc1
je	on	k3xPp3gMnPc4
Harveyho	Harveyha	k1gFnSc5
protějškem	protějšek	k1gInSc7
žena	žena	k1gFnSc1
jménem	jméno	k1gNnSc7
Evelyn	Evelyn	k1gMnSc1
„	„	k?
<g/>
Eve	Eve	k1gMnSc1
<g/>
“	“	k?
Dentová	Dentová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
něj	on	k3xPp3gNnSc2
tři	tři	k4xCgFnPc1
osobnosti	osobnost	k1gFnPc1
<g/>
,	,	kIx,
vystupuje	vystupovat	k5eAaImIp3nS
pod	pod	k7c7
aliasem	alias	k1gInSc7
„	„	k?
<g/>
Three	Three	k1gInSc1
Face	Face	k1gInSc1
<g/>
“	“	k?
a	a	k8xC
stojí	stát	k5eAaImIp3nS
na	na	k7c6
straně	strana	k1gFnSc6
dobra	dobro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Návrat	návrat	k1gInSc1
Temného	temný	k2eAgMnSc2d1
rytíře	rytíř	k1gMnSc2
</s>
<s>
V	v	k7c6
Návratu	návrat	k1gInSc6
Temného	temný	k2eAgMnSc2d1
rytíře	rytíř	k1gMnSc2
podstoupí	podstoupit	k5eAaPmIp3nS
Dent	Dent	k1gMnSc1
plastickou	plastický	k2eAgFnSc4d1
operaci	operace	k1gFnSc4
<g/>
,	,	kIx,
během	během	k7c2
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
mu	on	k3xPp3gInSc3
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
opravena	opravit	k5eAaPmNgFnS
jeho	jeho	k3xOp3gFnSc1
znetvořená	znetvořený	k2eAgFnSc1d1
tvář	tvář	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dojde	dojít	k5eAaPmIp3nS
však	však	k9
ke	k	k7c3
zničení	zničení	k1gNnSc3
jeho	jeho	k3xOp3gFnSc2
kladné	kladný	k2eAgFnSc2d1
osobnosti	osobnost	k1gFnSc2
a	a	k8xC
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
celým	celý	k2eAgNnSc7d1
tělem	tělo	k1gNnSc7
převezme	převzít	k5eAaPmIp3nS
padouch	padouch	k1gMnSc1
Two-Face	Two-Face	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
je	být	k5eAaImIp3nS
nakonec	nakonec	k6eAd1
zatčen	zatknout	k5eAaPmNgMnS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
pokusí	pokusit	k5eAaPmIp3nS
zničit	zničit	k5eAaPmF
dva	dva	k4xCgInPc4
gothamské	gothamský	k2eAgInPc4d1
mrakodrapy	mrakodrap	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Gotham	Gotham	k6eAd1
by	by	kYmCp3nS
Gaslight	Gaslight	k1gInSc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
příběhu	příběh	k1gInSc6
z	z	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
je	být	k5eAaImIp3nS
Harvey	Harve	k1gMnPc4
Dent	Dent	k1gMnSc1
sériovým	sériový	k2eAgMnSc7d1
vrahem	vrah	k1gMnSc7
známým	známý	k1gMnSc7
jako	jako	k8xC,k8xS
„	„	k?
<g/>
Double	double	k1gInSc1
Man	Man	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
The	The	k?
Batman	Batman	k1gMnSc1
Adventures	Adventures	k1gMnSc1
</s>
<s>
Dent	Dent	k1gInSc1
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
vyléčen	vyléčen	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
jej	on	k3xPp3gInSc4
Joker	Joker	k1gInSc4
přesvědčí	přesvědčit	k5eAaPmIp3nS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
snoubenka	snoubenka	k1gFnSc1
pletky	pletka	k1gFnSc2
s	s	k7c7
Brucem	Bruce	k1gMnSc7
Waynem	Wayn	k1gMnSc7
<g/>
,	,	kIx,
zcela	zcela	k6eAd1
zešílí	zešílet	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
In	In	k?
Darkest	Darkest	k1gMnSc1
Knight	Knight	k1gMnSc1
</s>
<s>
Gothamský	Gothamský	k2eAgMnSc1d1
okresní	okresní	k2eAgMnSc1d1
prokurátor	prokurátor	k1gMnSc1
Dent	Dent	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
nedůvěřuje	důvěřovat	k5eNaImIp3nS
Bruci	Bruce	k1gFnSc4
Wayneovi	Wayneus	k1gMnSc3
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
novým	nový	k2eAgInSc7d1
Green	Green	k1gInSc1
Lanternem	Lantern	k1gInSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
napaden	napadnout	k5eAaPmNgInS
Sinestrem	Sinestr	k1gInSc7
a	a	k8xC
získá	získat	k5eAaPmIp3nS
schopnosti	schopnost	k1gFnPc4
podobné	podobný	k2eAgFnPc4d1
těm	ten	k3xDgInPc3
padoucha	padouch	k1gMnSc4
Eclipsa	Eclipsa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
přijme	přijmout	k5eAaPmIp3nS
jméno	jméno	k1gNnSc4
„	„	k?
<g/>
Binary	Binara	k1gFnSc2
Star	Star	kA
<g/>
“	“	k?
a	a	k8xC
začne	začít	k5eAaPmIp3nS
spolupracovat	spolupracovat	k5eAaImF
se	se	k3xPyFc4
Star	Star	kA
Sapphire	Sapphir	k1gInSc5
a	a	k8xC
Selinou	Selina	k1gFnSc7
Kyleovou	Kyleův	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Batman	Batman	k1gMnSc1
v	v	k7c4
černé	černý	k2eAgFnPc4d1
a	a	k8xC
bílé	bílý	k2eAgFnPc4d1
</s>
<s>
Dent	Dent	k1gInSc1
opět	opět	k6eAd1
podstoupí	podstoupit	k5eAaPmIp3nS
plastickou	plastický	k2eAgFnSc4d1
operaci	operace	k1gFnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
vyléčen	vyléčen	k2eAgMnSc1d1
<g/>
,	,	kIx,
ovšem	ovšem	k9
psychopatická	psychopatický	k2eAgFnSc1d1
sestra	sestra	k1gFnSc1
jeho	jeho	k3xOp3gFnSc2
snoubenky	snoubenka	k1gFnSc2
ji	on	k3xPp3gFnSc4
zavraždí	zavraždit	k5eAaPmIp3nP
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
svého	svůj	k3xOyFgMnSc4
budoucího	budoucí	k2eAgMnSc4d1
švagra	švagr	k1gMnSc4
přinutí	přinutit	k5eAaPmIp3nS
stát	stát	k5eAaImF,k5eAaPmF
se	se	k3xPyFc4
Two-Facem	Two-Face	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Daredevil	Daredevit	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
/	/	kIx~
<g/>
Batman	Batman	k1gMnSc1
</s>
<s>
V	v	k7c6
alternativním	alternativní	k2eAgInSc6d1
vesmíru	vesmír	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
společně	společně	k6eAd1
koexistují	koexistovat	k5eAaImIp3nP
hrdinové	hrdina	k1gMnPc1
od	od	k7c2
DC	DC	kA
a	a	k8xC
Marvelu	Marvela	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Batman	Batman	k1gMnSc1
nucen	nutit	k5eAaImNgMnS
spolupracovat	spolupracovat	k5eAaImF
s	s	k7c7
Daredevilem	Daredevil	k1gInSc7
proti	proti	k7c3
kombinované	kombinovaný	k2eAgFnSc3d1
síle	síla	k1gFnSc3
Two-Face	Two-Face	k1gFnSc2
a	a	k8xC
Mr	Mr	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hydea	Hyde	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Masque	Masque	k6eAd1
</s>
<s>
V	v	k7c6
této	tento	k3xDgFnSc6
pastiši	pastiš	k1gFnSc6
na	na	k7c4
Fantoma	Fantoma	k1gNnSc4
opery	opera	k1gFnSc2
ztvárnil	ztvárnit	k5eAaPmAgMnS
Two-Face	Two-Face	k1gFnPc4
titulní	titulní	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Thrillkiller	Thrillkiller	k1gMnSc1
</s>
<s>
V	v	k7c4
Thrillkiller	Thrillkiller	k1gInSc4
má	mít	k5eAaImIp3nS
znetvořenou	znetvořený	k2eAgFnSc4d1
tvář	tvář	k1gFnSc4
zkorumpovaný	zkorumpovaný	k2eAgMnSc1d1
policista	policista	k1gMnSc1
detektiv	detektiv	k1gMnSc1
Duall	Duall	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harvey	Harvea	k1gFnSc2
Dent	Denta	k1gFnPc2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
okresním	okresní	k2eAgMnSc7d1
prokurátorem	prokurátor	k1gMnSc7
a	a	k8xC
později	pozdě	k6eAd2
starostou	starosta	k1gMnSc7
města	město	k1gNnSc2
Gotham	Gotham	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Tangent	tangenta	k1gFnPc2
</s>
<s>
Harvey	Harve	k1gMnPc4
Dent	Dentum	k1gNnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
této	tento	k3xDgFnSc6
verzi	verze	k1gFnSc6
afroamerický	afroamerický	k2eAgMnSc1d1
Superman	superman	k1gMnSc1
s	s	k7c7
psionickými	psionický	k2eAgFnPc7d1
schopnostmi	schopnost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Crimson	Crimson	k1gMnSc1
Mist	Mist	k1gMnSc1
</s>
<s>
Two-Face	Two-Face	k1gFnSc1
se	se	k3xPyFc4
zpočátku	zpočátku	k6eAd1
spojí	spojit	k5eAaPmIp3nP
s	s	k7c7
Gordonem	Gordon	k1gMnSc7
a	a	k8xC
Alfredem	Alfred	k1gMnSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
společně	společně	k6eAd1
zneškodnili	zneškodnit	k5eAaPmAgMnP
upírského	upírský	k2eAgMnSc4d1
Batmana	Batman	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
je	být	k5eAaImIp3nS
vše	všechen	k3xTgNnSc1
zdánlivě	zdánlivě	k6eAd1
hotovo	hotov	k2eAgNnSc1d1
<g/>
,	,	kIx,
vrátí	vrátit	k5eAaPmIp3nS
se	se	k3xPyFc4
ke	k	k7c3
svým	svůj	k3xOyFgMnPc3
bývalým	bývalý	k2eAgMnPc3d1
spojencům	spojenec	k1gMnPc3
<g/>
,	,	kIx,
ovšem	ovšem	k9
vyléčený	vyléčený	k2eAgMnSc1d1
Batman	Batman	k1gMnSc1
jej	on	k3xPp3gNnSc4
o	o	k7c4
něco	něco	k3yInSc4
později	pozdě	k6eAd2
zabije	zabít	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Claws	Claws	k1gInSc1
of	of	k?
the	the	k?
Catwoman	Catwoman	k1gMnSc1
</s>
<s>
Finnegan	Finnegan	k1gMnSc1
Dent	Dent	k1gMnSc1
je	být	k5eAaImIp3nS
dobrodruh	dobrodruh	k1gMnSc1
a	a	k8xC
zloděj	zloděj	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vyplení	vyplenit	k5eAaPmIp3nS
staroafrické	staroafrický	k2eAgNnSc4d1
město	město	k1gNnSc4
Mnemnom	Mnemnom	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
loupení	loupení	k1gNnSc1
odhalí	odhalit	k5eAaPmIp3nS
Tarzan	Tarzan	k1gMnSc1
a	a	k8xC
Batman	Batman	k1gMnSc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
ho	on	k3xPp3gInSc2
snaží	snažit	k5eAaImIp3nP
zastavit	zastavit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dent	Dent	k1gInSc1
je	být	k5eAaImIp3nS
poté	poté	k6eAd1
zraněn	zranit	k5eAaPmNgMnS
lvem	lev	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
mu	on	k3xPp3gMnSc3
znetvoří	znetvořit	k5eAaPmIp3nS
půlku	půlka	k1gFnSc4
obličeje	obličej	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
má	mít	k5eAaImIp3nS
za	za	k7c4
následek	následek	k1gInSc4
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
rozhodnutí	rozhodnutí	k1gNnSc2
zůstat	zůstat	k5eAaPmF
v	v	k7c6
Mnemnomu	Mnemnom	k1gInSc6
a	a	k8xC
stát	stát	k5eAaPmF,k5eAaImF
se	se	k3xPyFc4
vůdcem	vůdce	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelé	obyvatel	k1gMnPc1
města	město	k1gNnSc2
ho	on	k3xPp3gMnSc4
zapečetí	zapečetit	k5eAaPmIp3nS
v	v	k7c6
hrobce	hrobka	k1gFnSc6
a	a	k8xC
pravděpodobně	pravděpodobně	k6eAd1
jej	on	k3xPp3gMnSc4
nechají	nechat	k5eAaPmIp3nP
zemřít	zemřít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Guardian	Guardian	k1gInSc1
of	of	k?
Gotham	Gotham	k1gInSc1
</s>
<s>
Darcy	Darca	k1gFnPc4
Dentová	Dentová	k1gFnSc1
je	být	k5eAaImIp3nS
modelka	modelka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
znetvořenou	znetvořený	k2eAgFnSc4d1
půlku	půlka	k1gFnSc4
tváře	tvář	k1gFnSc2
ze	z	k7c2
záměrně	záměrně	k6eAd1
upraveného	upravený	k2eAgInSc2d1
krému	krém	k1gInSc2
na	na	k7c4
obličej	obličej	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Flashpoint	Flashpoint	k1gMnSc1
</s>
<s>
V	v	k7c6
alternativním	alternativní	k2eAgInSc6d1
vesmíru	vesmír	k1gInSc6
příběhu	příběh	k1gInSc2
Flashpoint	Flashpointa	k1gFnPc2
se	se	k3xPyFc4
Dent	Dent	k1gInSc1
nikdy	nikdy	k6eAd1
nestal	stát	k5eNaPmAgInS
Two-Facem	Two-Faec	k1gInSc7
<g/>
,	,	kIx,
místo	místo	k7c2
toho	ten	k3xDgNnSc2
je	být	k5eAaImIp3nS
zaměstnán	zaměstnat	k5eAaPmNgMnS
jako	jako	k9
soudce	soudce	k1gMnSc1
a	a	k8xC
žije	žít	k5eAaImIp3nS
šťastně	šťastně	k6eAd1
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
ženou	žena	k1gFnSc7
a	a	k8xC
dětmi	dítě	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
děti	dítě	k1gFnPc4
později	pozdě	k6eAd2
unese	unést	k5eAaPmIp3nS
Joker	Joker	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
vyhledá	vyhledat	k5eAaPmIp3nS
Thomase	Thomas	k1gMnSc4
Waynea	Wayneus	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mu	on	k3xPp3gMnSc3
pomohl	pomoct	k5eAaPmAgMnS
při	při	k7c6
jejich	jejich	k3xOp3gNnSc6
objevování	objevování	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
děti	dítě	k1gFnPc4
zachrání	zachránit	k5eAaPmIp3nP
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
Dentova	Dentův	k2eAgFnSc1d1
dcera	dcera	k1gFnSc1
je	být	k5eAaImIp3nS
postřelena	postřelen	k2eAgFnSc1d1
a	a	k8xC
málem	málem	k6eAd1
zemře	zemřít	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Království	království	k1gNnSc1
tvé	tvůj	k3xOp2gFnSc2
</s>
<s>
Na	na	k7c6
Zemi-	Zemi-	k1gFnSc6
<g/>
22	#num#	k4
je	být	k5eAaImIp3nS
Harvey	Harve	k1gMnPc4
Dent	Dent	k1gMnSc1
zpočátku	zpočátku	k6eAd1
jedním	jeden	k4xCgMnSc7
z	z	k7c2
největších	veliký	k2eAgMnPc2d3
protivníků	protivník	k1gMnPc2
Batmana	Batman	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
je	být	k5eAaImIp3nS
později	pozdě	k6eAd2
odhaleno	odhalen	k2eAgNnSc1d1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
pod	pod	k7c7
netopýří	netopýří	k2eAgFnSc7d1
maskou	maska	k1gFnSc7
schovává	schovávat	k5eAaImIp3nS
<g/>
,	,	kIx,
spolčí	spolčit	k5eAaPmIp3nS
se	se	k3xPyFc4
padouch	padouch	k1gMnSc1
s	s	k7c7
Banem	Ban	k1gInSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
společně	společně	k6eAd1
zničili	zničit	k5eAaPmAgMnP
Wayneovo	Wayneův	k2eAgNnSc4d1
panství	panství	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Charakterizace	charakterizace	k1gFnSc1
</s>
<s>
Schopnosti	schopnost	k1gFnPc1
a	a	k8xC
dovednosti	dovednost	k1gFnPc1
</s>
<s>
Harvey	Harvea	k1gMnSc2
Dent	Dent	k1gMnSc1
býval	bývat	k5eAaImAgMnS
úspěšným	úspěšný	k2eAgMnSc7d1
okresním	okresní	k2eAgMnSc7d1
prokurátorem	prokurátor	k1gMnSc7
se	s	k7c7
znalostmi	znalost	k1gFnPc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
práva	právo	k1gNnSc2
a	a	k8xC
trestního	trestní	k2eAgNnSc2d1
soudnictví	soudnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
proměně	proměna	k1gFnSc6
v	v	k7c4
Two-Face	Two-Face	k1gFnPc4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
velkým	velký	k2eAgMnSc7d1
a	a	k8xC
respektovaným	respektovaný	k2eAgMnSc7d1
zločincem	zločinec	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
svými	svůj	k3xOyFgInPc7
mazanými	mazaný	k2eAgInPc7d1
plány	plán	k1gInPc7
mnohokrát	mnohokrát	k6eAd1
prokázal	prokázat	k5eAaPmAgInS
vysokou	vysoký	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
inteligence	inteligence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
intenzivnímu	intenzivní	k2eAgInSc3d1
tréninku	trénink	k1gInSc3
s	s	k7c7
Batmanem	Batman	k1gInSc7
se	se	k3xPyFc4
z	z	k7c2
něho	on	k3xPp3gMnSc2
stal	stát	k5eAaPmAgMnS
výborný	výborný	k2eAgMnSc1d1
bojovník	bojovník	k1gMnSc1
<g/>
,	,	kIx,
schopný	schopný	k2eAgMnSc1d1
porazit	porazit	k5eAaPmF
v	v	k7c6
pěstním	pěstní	k2eAgInSc6d1
souboji	souboj	k1gInSc6
Killer	Killer	k1gMnSc1
Croca	Croca	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
dokázal	dokázat	k5eAaPmAgInS
blokovat	blokovat	k5eAaImF
několik	několik	k4yIc4
úderů	úder	k1gInPc2
od	od	k7c2
Nightwinga	Nightwinga	k1gFnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
na	na	k7c4
samotného	samotný	k2eAgMnSc4d1
Graysona	Grayson	k1gMnSc4
udělalo	udělat	k5eAaPmAgNnS
dojem	dojem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Two-Face	Two-Face	k1gFnSc1
je	být	k5eAaImIp3nS
zručným	zručný	k2eAgMnSc7d1
střelcem	střelec	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
ovládá	ovládat	k5eAaImIp3nS
většinu	většina	k1gFnSc4
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
střelbě	střelba	k1gFnSc6
jej	on	k3xPp3gNnSc4
za	za	k7c4
poplatek	poplatek	k1gInSc4
cvičil	cvičit	k5eAaImAgMnS
padouch	padouch	k1gMnSc1
Deathstroke	Deathstrok	k1gInSc2
<g/>
,	,	kIx,
díky	díky	k7c3
kterému	který	k3yQgNnSc3,k3yRgNnSc3,k3yIgNnSc3
se	se	k3xPyFc4
Dent	Dent	k1gMnSc1
stal	stát	k5eAaPmAgMnS
výborným	výborný	k2eAgMnSc7d1
odstřelovačem	odstřelovač	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Slabosti	slabost	k1gFnPc1
</s>
<s>
Two-Face	Two-Face	k1gFnSc1
trpí	trpět	k5eAaImIp3nS
disociativní	disociativní	k2eAgFnSc7d1
poruchou	porucha	k1gFnSc7
identity	identita	k1gFnSc2
<g/>
,	,	kIx,
schizofrenií	schizofrenie	k1gFnSc7
a	a	k8xC
bipolární	bipolární	k2eAgFnSc7d1
poruchou	porucha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posedle	posedle	k6eAd1
dělá	dělat	k5eAaImIp3nS
všechna	všechen	k3xTgNnPc4
důležitá	důležitý	k2eAgNnPc4d1
rozhodnutí	rozhodnutí	k1gNnPc4
hodem	hod	k1gInSc7
dvouhlavé	dvouhlavý	k2eAgFnSc2d1
mince	mince	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
jedné	jeden	k4xCgFnSc2
strany	strana	k1gFnSc2
poškrábaná	poškrábaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nových	nový	k2eAgInPc6d1
příbězích	příběh	k1gInPc6
je	být	k5eAaImIp3nS
barvoslepý	barvoslepý	k2eAgInSc1d1
na	na	k7c4
levé	levý	k2eAgNnSc4d1
oko	oko	k1gNnSc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
bylo	být	k5eAaImAgNnS
zasaženo	zasažen	k2eAgNnSc1d1
kyselinou	kyselina	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
ho	on	k3xPp3gMnSc4
znetvořila	znetvořit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výbava	výbava	k1gFnSc1
</s>
<s>
Dvouhlavá	dvouhlavý	k2eAgFnSc1d1
mince	mince	k1gFnSc1
<g/>
:	:	kIx,
K	k	k7c3
Two-Faceovi	Two-Faceus	k1gMnSc3
neodmyslitelně	odmyslitelně	k6eNd1
patří	patřit	k5eAaImIp3nS
dvouhlavý	dvouhlavý	k2eAgInSc1d1
stříbrný	stříbrný	k2eAgInSc1d1
dolar	dolar	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
poškrábanou	poškrábaný	k2eAgFnSc4d1
jednu	jeden	k4xCgFnSc4
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Házením	házení	k1gNnSc7
mincí	mince	k1gFnPc2
Harvey	Harvea	k1gFnSc2
nechává	nechávat	k5eAaImIp3nS
o	o	k7c6
významných	významný	k2eAgFnPc6d1
životních	životní	k2eAgFnPc6d1
situacích	situace	k1gFnPc6
rozhodnout	rozhodnout	k5eAaPmF
osud	osud	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
padne	padnout	k5eAaPmIp3nS,k5eAaImIp3nS
poškrábaná	poškrábaný	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
Two-Face	Two-Face	k1gFnSc1
konat	konat	k5eAaImF
zlo	zlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
opačném	opačný	k2eAgInSc6d1
případě	případ	k1gInSc6
je	být	k5eAaImIp3nS
nucen	nutit	k5eAaImNgMnS
dopustit	dopustit	k5eAaPmF
se	se	k3xPyFc4
dobrých	dobrý	k2eAgInPc2d1
skutků	skutek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Automatické	automatický	k2eAgFnPc1d1
zbraně	zbraň	k1gFnPc1
<g/>
:	:	kIx,
Two-Face	Two-Face	k1gFnSc1
běžně	běžně	k6eAd1
používá	používat	k5eAaImIp3nS
řadu	řada	k1gFnSc4
automatických	automatický	k2eAgFnPc2d1
a	a	k8xC
poloautomatických	poloautomatický	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
u	u	k7c2
sebe	se	k3xPyFc2
nosí	nosit	k5eAaImIp3nP
dvě	dva	k4xCgFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vztahy	vztah	k1gInPc1
</s>
<s>
Rodina	rodina	k1gFnSc1
</s>
<s>
Gilda	gilda	k1gFnSc1
Dentová	Dentová	k1gFnSc1
(	(	kIx(
<g/>
za	za	k7c2
svobodna	svoboden	k2eAgFnSc1d1
Goldová	Goldová	k1gFnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Gilda	gilda	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
mnoha	mnoho	k4c6
komiksových	komiksový	k2eAgFnPc6d1
inkarnacích	inkarnace	k1gFnPc6
Harveyho	Harvey	k1gMnSc2
manželkou	manželka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Zlatém	zlatý	k2eAgInSc6d1
věku	věk	k1gInSc6
byla	být	k5eAaImAgFnS
zpočátku	zpočátku	k6eAd1
pouze	pouze	k6eAd1
jeho	jeho	k3xOp3gFnSc1
snoubenka	snoubenka	k1gFnSc1
<g/>
,	,	kIx,
když	když	k8xS
byl	být	k5eAaImAgInS
Harvey	Harve	k1gMnPc4
znetvořen	znetvořen	k2eAgMnSc1d1
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
zásnuby	zásnuba	k1gFnPc1
zrušeny	zrušit	k5eAaPmNgFnP
a	a	k8xC
pár	pár	k1gInSc1
žil	žít	k5eAaImAgInS
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
odděleně	odděleně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
několik	několik	k4yIc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
se	se	k3xPyFc4
však	však	k9
dali	dát	k5eAaPmAgMnP
opět	opět	k6eAd1
dohromady	dohromady	k6eAd1
a	a	k8xC
vzali	vzít	k5eAaPmAgMnP
se	s	k7c7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
událostech	událost	k1gFnPc6
Krize	krize	k1gFnSc2
na	na	k7c6
nekonečnu	nekonečno	k1gNnSc6
Zemí	zem	k1gFnPc2
se	se	k3xPyFc4
Gilda	gilda	k1gFnSc1
a	a	k8xC
Harvey	Harvea	k1gFnSc2
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
pokoušeli	pokoušet	k5eAaImAgMnP
o	o	k7c4
dítě	dítě	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gNnSc1
úsilí	úsilí	k1gNnSc1
bylo	být	k5eAaImAgNnS
marné	marný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
manželství	manželství	k1gNnSc1
Dentových	Dentová	k1gFnPc2
začalo	začít	k5eAaPmAgNnS
zhoršovat	zhoršovat	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
hlavně	hlavně	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Gilda	gilda	k1gFnSc1
chtěla	chtít	k5eAaImAgFnS
usadit	usadit	k5eAaPmF
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Harvey	Harvea	k1gFnPc1
se	se	k3xPyFc4
neustále	neustále	k6eAd1
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
chytání	chytání	k1gNnSc4
zločinců	zločinec	k1gMnPc2
v	v	k7c6
Gothamu	Gotham	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
nato	nato	k6eAd1
začal	začít	k5eAaPmAgInS
ve	v	k7c6
městě	město	k1gNnSc6
řádit	řádit	k5eAaImF
sériový	sériový	k2eAgMnSc1d1
vrah	vrah	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
vraždil	vraždit	k5eAaImAgMnS
významné	významný	k2eAgMnPc4d1
gangstery	gangster	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikdo	nikdo	k3yNnSc1
netušil	tušit	k5eNaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
těmito	tento	k3xDgFnPc7
vraždami	vražda	k1gFnPc7
<g/>
,	,	kIx,
páchanými	páchaný	k2eAgFnPc7d1
výhradně	výhradně	k6eAd1
ve	v	k7c4
sváteční	sváteční	k2eAgInPc4d1
dny	den	k1gInPc4
<g/>
,	,	kIx,
začala	začít	k5eAaPmAgFnS
Gilda	gilda	k1gFnSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
později	pozdě	k6eAd2
ji	on	k3xPp3gFnSc4
nahradil	nahradit	k5eAaPmAgMnS
Alberto	Alberta	k1gFnSc5
Falcone	Falcon	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc4
události	událost	k1gFnPc1
nakonec	nakonec	k6eAd1
vyvrcholily	vyvrcholit	k5eAaPmAgFnP
Harveyho	Harvey	k1gMnSc4
transformací	transformace	k1gFnSc7
v	v	k7c4
Two-Face	Two-Face	k1gFnPc4
a	a	k8xC
Gildiným	Gildin	k2eAgInSc7d1
útěkem	útěk	k1gInSc7
z	z	k7c2
Gotham	Gotham	k1gInSc4
City	City	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
New	New	k1gFnSc6
52	#num#	k4
se	se	k3xPyFc4
Gilda	gilda	k1gFnSc1
a	a	k8xC
Harvey	Harvea	k1gFnSc2
poznali	poznat	k5eAaPmAgMnP
díky	díky	k7c3
Bruci	Bruce	k1gMnSc3
Wayneovi	Wayneus	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
seznámil	seznámit	k5eAaPmAgMnS
na	na	k7c6
jednom	jeden	k4xCgInSc6
ze	z	k7c2
svých	svůj	k3xOyFgInPc2
večírků	večírek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
spolu	spolu	k6eAd1
začali	začít	k5eAaPmAgMnP
žít	žít	k5eAaImF
a	a	k8xC
vzali	vzít	k5eAaPmAgMnP
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželská	manželský	k2eAgFnSc1d1
idylka	idylka	k1gFnSc1
však	však	k9
netrvala	trvat	k5eNaImAgFnS
příliš	příliš	k6eAd1
dlouho	dlouho	k6eAd1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
byla	být	k5eAaImAgFnS
Gilda	gilda	k1gFnSc1
zavražděna	zavražděn	k2eAgFnSc1d1
Erin	Erin	k1gInSc4
McKillenovou	McKillenová	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
chtěla	chtít	k5eAaImAgFnS
pomstít	pomstít	k5eAaPmF
Harveymu	Harveym	k1gInSc2
za	za	k7c4
uvěznění	uvěznění	k1gNnSc4
a	a	k8xC
smrt	smrt	k1gFnSc4
své	svůj	k3xOyFgFnSc2
sestry	sestra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Christopher	Christophra	k1gFnPc2
Dent	Denta	k1gFnPc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Christopher	Christophra	k1gFnPc2
byl	být	k5eAaImAgInS
Harveyho	Harvey	k1gMnSc4
despotický	despotický	k2eAgMnSc1d1
otec	otec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
propadl	propadnout	k5eAaPmAgMnS
alkoholu	alkohol	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
dětství	dětství	k1gNnSc4
svého	svůj	k3xOyFgMnSc2
syna	syn	k1gMnSc2
často	často	k6eAd1
týral	týrat	k5eAaImAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
zároveň	zároveň	k6eAd1
i	i	k9
zahrnoval	zahrnovat	k5eAaImAgMnS
láskou	láska	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
v	v	k7c4
Harveym	Harveym	k1gInSc4
vyvolávalo	vyvolávat	k5eAaImAgNnS
zmatek	zmatek	k1gInSc4
a	a	k8xC
později	pozdě	k6eAd2
mu	on	k3xPp3gMnSc3
dopomohlo	dopomoct	k5eAaPmAgNnS
k	k	k7c3
jeho	jeho	k3xOp3gFnSc3
přeměně	přeměna	k1gFnSc3
v	v	k7c4
Two-Face	Two-Face	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Murray	Murray	k1gInPc1
Dent	Denta	k1gFnPc2
<g/>
:	:	kIx,
Murray	Murraa	k1gFnSc2
byl	být	k5eAaImAgMnS
Harveyho	Harvey	k1gMnSc2
starší	starší	k1gMnSc1
bratr	bratr	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
v	v	k7c4
dětství	dětství	k1gNnSc4
nešťastnou	šťastný	k2eNgFnSc7d1
náhodou	náhoda	k1gFnSc7
uhořel	uhořet	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
tragédie	tragédie	k1gFnSc1
Dentovu	Dentův	k2eAgFnSc4d1
rodinu	rodina	k1gFnSc4
zcela	zcela	k6eAd1
zničila	zničit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harveyho	Harveyha	k1gFnSc5
matka	matka	k1gFnSc1
spáchala	spáchat	k5eAaPmAgFnS
sebevraždu	sebevražda	k1gFnSc4
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
alkoholikem	alkoholik	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přátelé	přítel	k1gMnPc1
</s>
<s>
Bruce	Bruce	k1gMnSc5
Wayne	Wayn	k1gMnSc5
<g/>
:	:	kIx,
V	v	k7c6
současné	současný	k2eAgFnSc3d1
kontinuitě	kontinuita	k1gFnSc3
komiksů	komiks	k1gInPc2
bylo	být	k5eAaImAgNnS
odhaleno	odhalen	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Harvey	Harvea	k1gFnPc1
Dent	Dentum	k1gNnPc2
a	a	k8xC
Bruce	Bruce	k1gMnSc1
Wayne	Wayn	k1gInSc5
se	se	k3xPyFc4
znají	znát	k5eAaImIp3nP
již	již	k6eAd1
od	od	k7c2
dětství	dětství	k1gNnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	se	k3xPyFc4
poznali	poznat	k5eAaPmAgMnP
v	v	k7c6
Arkhamově	Arkhamově	k1gFnSc6
azylu	azyl	k1gInSc2
pro	pro	k7c4
problémové	problémový	k2eAgFnPc4d1
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
přátelství	přátelství	k1gNnSc1
vydrželo	vydržet	k5eAaPmAgNnS
až	až	k9
do	do	k7c2
dospělosti	dospělost	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
společně	společně	k6eAd1
bojovali	bojovat	k5eAaImAgMnP
proti	proti	k7c3
zločinu	zločin	k1gInSc3
v	v	k7c4
Gotham	Gotham	k1gInSc4
City	City	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
bylo	být	k5eAaImAgNnS
přerušeno	přerušit	k5eAaPmNgNnS
až	až	k9
s	s	k7c7
Dentovým	Dentův	k2eAgNnSc7d1
znetvořením	znetvoření	k1gNnSc7
a	a	k8xC
následným	následný	k2eAgNnSc7d1
probuzením	probuzení	k1gNnSc7
jeho	jeho	k3xOp3gFnSc2
zlé	zlý	k2eAgFnSc2d1
osobnosti	osobnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
James	James	k1gMnSc1
Gordon	Gordon	k1gMnSc1
<g/>
:	:	kIx,
Komisař	komisař	k1gMnSc1
Gordon	Gordon	k1gMnSc1
byl	být	k5eAaImAgMnS
další	další	k2eAgMnSc1d1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
Harveymu	Harveym	k1gInSc6
pomáhal	pomáhat	k5eAaImAgInS
bojovat	bojovat	k5eAaImF
proti	proti	k7c3
zločinu	zločin	k1gInSc3
v	v	k7c6
Gothamu	Gotham	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Albert	Albert	k1gMnSc1
Ekhart	Ekhart	k1gInSc1
<g/>
:	:	kIx,
Doktor	doktor	k1gMnSc1
Ekhart	Ekhart	k1gInSc4
je	být	k5eAaImIp3nS
plastický	plastický	k2eAgMnSc1d1
chirurg	chirurg	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
dokázal	dokázat	k5eAaPmAgMnS
opravit	opravit	k5eAaPmF
Harveyho	Harvey	k1gMnSc4
znetvořenou	znetvořený	k2eAgFnSc4d1
tvář	tvář	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Romantické	romantický	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
</s>
<s>
Carol	Carola	k1gFnPc2
Berminghamová	Berminghamový	k2eAgFnSc1d1
<g/>
:	:	kIx,
Carol	Carol	k1gInSc1
je	být	k5eAaImIp3nS
bývalá	bývalý	k2eAgFnSc1d1
okresní	okresní	k2eAgFnSc1d1
prokurátorka	prokurátorka	k1gFnSc1
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yRgFnSc2,k3yIgFnSc2,k3yQgFnSc2
byl	být	k5eAaImAgInS
Harvey	Harvea	k1gFnSc2
Dent	Dent	k1gInSc1
kdysi	kdysi	k6eAd1
zamilovaný	zamilovaný	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Janice	Janice	k1gFnSc1
Porterová	Porterová	k1gFnSc1
<g/>
:	:	kIx,
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
byl	být	k5eAaImAgMnS
Harvey	Harvea	k1gFnPc4
Dent	Dentum	k1gNnPc2
poslán	poslán	k2eAgInSc1d1
do	do	k7c2
Arkhamu	Arkham	k1gInSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
na	na	k7c6
jeho	jeho	k3xOp3gInSc6
místo	místo	k7c2
okresního	okresní	k2eAgMnSc2d1
prokurátora	prokurátor	k1gMnSc2
dosazena	dosazen	k2eAgFnSc1d1
Janice	Janice	k1gFnSc1
Porterová	Porterová	k1gFnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
tajná	tajný	k2eAgFnSc1d1
milenka	milenka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1
postavy	postava	k1gFnPc1
s	s	k7c7
identitou	identita	k1gFnSc7
Two-Face	Two-Face	k1gFnSc2
</s>
<s>
Wilkins	Wilkins	k6eAd1
</s>
<s>
Wilkins	Wilkins	k6eAd1
byl	být	k5eAaImAgMnS
Harveyho	Harvey	k1gMnSc2
komorník	komorník	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
používal	používat	k5eAaImAgMnS
make-up	make-up	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vypadal	vypadat	k5eAaImAgMnS,k5eAaPmAgMnS
jako	jako	k9
Two-Face	Two-Face	k1gFnSc2
a	a	k8xC
mohl	moct	k5eAaImAgInS
páchat	páchat	k5eAaImF
zločiny	zločin	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Paul	Paul	k1gMnSc1
Sloane	Sloan	k1gInSc5
</s>
<s>
Paul	Paul	k1gMnSc1
Sloane	Sloan	k1gMnSc5
býval	bývat	k5eAaImAgMnS
kdysi	kdysi	k6eAd1
herec	herec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
měl	mít	k5eAaImAgMnS
díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
podobnosti	podobnost	k1gFnSc3
s	s	k7c7
Dentem	Dento	k1gNnSc7
hrát	hrát	k5eAaImF
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
filmu	film	k1gInSc6
o	o	k7c6
Two-Faceovi	Two-Faceus	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
natáčení	natáčení	k1gNnSc2
byl	být	k5eAaImAgMnS
však	však	k9
nešťastnou	šťastný	k2eNgFnSc7d1
náhodou	náhoda	k1gFnSc7
znetvořen	znetvořen	k2eAgInSc1d1
v	v	k7c6
obličeji	obličej	k1gInSc6
<g/>
,	,	kIx,
za	za	k7c4
což	což	k3yRnSc4,k3yQnSc4
mohl	moct	k5eAaImAgMnS
mladý	mladý	k2eAgMnSc1d1
rekvizitář	rekvizitář	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
mu	on	k3xPp3gMnSc3
místo	místo	k7c2
obyčejné	obyčejný	k2eAgFnSc2d1
rekvizity	rekvizita	k1gFnSc2
podstrčil	podstrčit	k5eAaPmAgInS
opravdovou	opravdový	k2eAgFnSc4d1
kyselinu	kyselina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
byla	být	k5eAaImAgFnS
žárlivost	žárlivost	k1gFnSc1
na	na	k7c4
přítelkyni	přítelkyně	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
do	do	k7c2
pohledného	pohledný	k2eAgInSc2d1
Sloanea	Sloane	k1gInSc2
zakoukala	zakoukat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pozdější	pozdní	k2eAgFnSc6d2
verzi	verze	k1gFnSc6
příběhu	příběh	k1gInSc2
byl	být	k5eAaImAgInS
herec	herec	k1gMnSc1
znetvořen	znetvořit	k5eAaPmNgInS
výbuchem	výbuch	k1gInSc7
silného	silný	k2eAgInSc2d1
reflektoru	reflektor	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
milostný	milostný	k2eAgInSc1d1
trojúhelník	trojúhelník	k1gInSc1
byl	být	k5eAaImAgInS
zcela	zcela	k6eAd1
vynechán	vynechat	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paulova	Paulův	k2eAgFnSc1d1
mysl	mysl	k1gFnSc1
<g/>
,	,	kIx,
pokřivená	pokřivený	k2eAgFnSc1d1
traumatem	trauma	k1gNnSc7
<g/>
,	,	kIx,
začala	začít	k5eAaPmAgFnS
věřit	věřit	k5eAaImF
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
doopravdy	doopravdy	k6eAd1
Two-Face	Two-Face	k1gFnSc1
<g/>
,	,	kIx,
načež	načež	k6eAd1
bývalého	bývalý	k2eAgMnSc2d1
herce	herec	k1gMnSc2
dohnala	dohnat	k5eAaPmAgFnS
na	na	k7c4
dráhu	dráha	k1gFnSc4
zločinu	zločin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
ho	on	k3xPp3gMnSc4
Batman	Batman	k1gMnSc1
pomocí	pomocí	k7c2
lsti	lest	k1gFnSc2
donutil	donutit	k5eAaPmAgMnS
se	se	k3xPyFc4
vzdát	vzdát	k5eAaPmF
a	a	k8xC
podstoupit	podstoupit	k5eAaPmF
plastickou	plastický	k2eAgFnSc4d1
operaci	operace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
novějších	nový	k2eAgInPc6d2
komiksech	komiks	k1gInPc6
se	se	k3xPyFc4
z	z	k7c2
Paula	Paul	k1gMnSc2
stal	stát	k5eAaPmAgMnS
padouch	padouch	k1gMnSc1
Charlatan	Charlatan	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
George	Georg	k1gMnSc4
Blake	Blak	k1gMnSc4
</s>
<s>
Dalším	další	k2eAgMnSc7d1
podvodným	podvodný	k2eAgMnSc7d1
Two-Facem	Two-Face	k1gMnSc7
byl	být	k5eAaImAgMnS
drobný	drobný	k2eAgMnSc1d1
kriminálník	kriminálník	k1gMnSc1
George	Georg	k1gMnSc2
Blake	Blak	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
vydával	vydávat	k5eAaImAgInS,k5eAaPmAgInS
za	za	k7c4
ředitele	ředitel	k1gMnPc4
výstavy	výstava	k1gFnSc2
zaměřené	zaměřený	k2eAgFnSc2d1
na	na	k7c4
boj	boj	k1gInSc4
proti	proti	k7c3
zločinu	zločin	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
však	však	k9
nebyl	být	k5eNaImAgInS
znetvořený	znetvořený	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
používal	používat	k5eAaImAgMnS
make-up	make-up	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
ho	on	k3xPp3gMnSc4
Batman	Batman	k1gMnSc1
také	také	k6eAd1
snadno	snadno	k6eAd1
identifikoval	identifikovat	k5eAaBmAgMnS
jako	jako	k9
podvodníka	podvodník	k1gMnSc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
jej	on	k3xPp3gMnSc4
nosil	nosit	k5eAaImAgMnS
na	na	k7c6
opačné	opačný	k2eAgFnSc6d1
straně	strana	k1gFnSc6
obličeje	obličej	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Batman	Batman	k1gMnSc1
</s>
<s>
Za	za	k7c2
povšimnutí	povšimnutí	k1gNnSc2
stojí	stát	k5eAaImIp3nS
také	také	k9
komiksový	komiksový	k2eAgInSc1d1
sešit	sešit	k2eAgInSc1d1
World	World	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Finest	Finest	k1gInSc1
Comics	comics	k1gInSc1
#	#	kIx~
<g/>
173	#num#	k4
z	z	k7c2
roku	rok	k1gInSc2
1968	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
se	se	k3xPyFc4
vinou	vinout	k5eAaImIp3nP
chemikálie	chemikálie	k1gFnPc1
v	v	k7c6
Two-Face	Two-Face	k1gFnSc1
proměnil	proměnit	k5eAaPmAgMnS
samotný	samotný	k2eAgMnSc1d1
Batman	Batman	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ostatní	ostatní	k2eAgNnPc4d1
média	médium	k1gNnPc4
</s>
<s>
Filmy	film	k1gInPc1
</s>
<s>
Hrané	hraný	k2eAgNnSc1d1
</s>
<s>
Aaron	Aaron	k1gMnSc1
Eckhart	Eckharta	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
si	se	k3xPyFc3
zahrál	zahrát	k5eAaPmAgMnS
Two-Face	Two-Face	k1gFnPc4
ve	v	k7c6
filmu	film	k1gInSc6
Temný	temný	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Batman	Batman	k1gMnSc1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c6
tomto	tento	k3xDgInSc6
filmu	film	k1gInSc6
je	být	k5eAaImIp3nS
Harvey	Harvea	k1gFnPc4
Dent	Dentum	k1gNnPc2
vyobrazen	vyobrazen	k2eAgInSc1d1
jako	jako	k8xS,k8xC
nový	nový	k2eAgMnSc1d1
okresní	okresní	k2eAgMnSc1d1
prokurátor	prokurátor	k1gMnSc1
Gotham	Gotham	k1gInSc4
City	City	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
přísahá	přísahat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
vyčistí	vyčistit	k5eAaPmIp3nS
město	město	k1gNnSc1
od	od	k7c2
zločinu	zločin	k1gInSc2
a	a	k8xC
učiní	učinit	k5eAaPmIp3nS,k5eAaImIp3nS
z	z	k7c2
něj	on	k3xPp3gNnSc2
místo	místo	k6eAd1
pro	pro	k7c4
slušné	slušný	k2eAgMnPc4d1
lidi	člověk	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Role	rola	k1gFnSc6
se	se	k3xPyFc4
zhostil	zhostit	k5eAaPmAgMnS
americký	americký	k2eAgMnSc1d1
herec	herec	k1gMnSc1
a	a	k8xC
zpěvák	zpěvák	k1gMnSc1
Billy	Bill	k1gMnPc7
Dee	Dee	k1gFnSc4
Williams	Williamsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Batman	Batman	k1gMnSc1
navždy	navždy	k6eAd1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Ve	v	k7c6
filmu	film	k1gInSc6
Batman	Batman	k1gMnSc1
navždy	navždy	k6eAd1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
událostmi	událost	k1gFnPc7
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
Batmana	Batman	k1gMnSc4
z	z	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Dent	Dent	k1gInSc4
alias	alias	k9
Two-Face	Two-Face	k1gFnSc1
již	již	k6eAd1
jedním	jeden	k4xCgMnSc7
z	z	k7c2
dvojice	dvojice	k1gFnSc2
hlavních	hlavní	k2eAgMnPc2d1
padouchů	padouch	k1gMnPc2
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
Riddlerem	Riddler	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viní	vinit	k5eAaImIp3nS
Batmana	Batman	k1gMnSc4
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
nezabránil	zabránit	k5eNaPmAgMnS
jeho	jeho	k3xOp3gNnPc4
znetvoření	znetvoření	k1gNnPc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
způsobeno	způsobit	k5eAaPmNgNnS
při	při	k7c6
soudním	soudní	k2eAgNnSc6d1
přelíčení	přelíčení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
vyjde	vyjít	k5eAaPmIp3nS
na	na	k7c4
povrch	povrch	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
zodpovědný	zodpovědný	k2eAgMnSc1d1
za	za	k7c4
smrt	smrt	k1gFnSc4
rodičů	rodič	k1gMnPc2
Dicka	Dicka	k1gFnSc1
Graysona	Graysona	k1gFnSc1
alias	alias	k9
Robina	robin	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roli	role	k1gFnSc6
ztvárnil	ztvárnit	k5eAaPmAgMnS
herec	herec	k1gMnSc1
Tommy	Tomma	k1gFnSc2
Lee	Lea	k1gFnSc3
Jones	Jones	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Temný	temný	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Na	na	k7c6
začátku	začátek	k1gInSc6
filmu	film	k1gInSc2
je	být	k5eAaImIp3nS
Harvey	Harve	k1gMnPc4
Dent	Dent	k1gMnSc1
nově	nově	k6eAd1
zvoleným	zvolený	k2eAgMnSc7d1
okresním	okresní	k2eAgMnSc7d1
prokurátorem	prokurátor	k1gMnSc7
Gotham	Gotham	k1gInSc4
City	City	k1gFnSc2
a	a	k8xC
jedním	jeden	k4xCgMnSc7
z	z	k7c2
prvních	první	k4xOgMnPc2
podporovatelů	podporovatel	k1gMnPc2
Batmana	Batman	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
začne	začít	k5eAaPmIp3nS
bojovat	bojovat	k5eAaImF
proti	proti	k7c3
zločinu	zločin	k1gInSc3
v	v	k7c4
Gotham	Gotham	k1gInSc4
City	City	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zavražděna	zavražděn	k2eAgFnSc1d1
jeho	jeho	k3xOp3gFnSc1
snoubenka	snoubenka	k1gFnSc1
Rachel	Rachela	k1gFnPc2
Dawesová	Dawesový	k2eAgFnSc1d1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
utrpí	utrpět	k5eAaPmIp3nS
vážné	vážný	k2eAgFnPc4d1
popáleniny	popálenina	k1gFnPc4
poloviny	polovina	k1gFnSc2
obličeje	obličej	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
se	se	k3xPyFc4
Harvey	Harvea	k1gFnSc2
zotavuje	zotavovat	k5eAaImIp3nS
v	v	k7c6
gothamské	gothamský	k2eAgFnSc6d1
nemocnici	nemocnice	k1gFnSc6
<g/>
,	,	kIx,
navštíví	navštívit	k5eAaPmIp3nS
jej	on	k3xPp3gInSc4
Joker	Joker	k1gInSc4
a	a	k8xC
přesvědčí	přesvědčit	k5eAaPmIp3nS
ho	on	k3xPp3gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
pomstil	pomstít	k5eAaPmAgMnS,k5eAaImAgMnS
lidem	člověk	k1gMnPc3
<g/>
,	,	kIx,
o	o	k7c6
kterých	který	k3yIgInPc6,k3yQgInPc6,k3yRgInPc6
si	se	k3xPyFc3
myslí	myslet	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
zodpovědní	zodpovědný	k2eAgMnPc1d1
za	za	k7c4
Rachelinu	Rachelina	k1gFnSc4
smrt	smrt	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stává	stávat	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
něj	on	k3xPp3gNnSc2
nový	nový	k2eAgMnSc1d1
gothamský	gothamský	k2eAgMnSc1d1
padouch	padouch	k1gMnSc1
„	„	k?
<g/>
Two-Face	Two-Face	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
začne	začít	k5eAaPmIp3nS
mstít	mstít	k5eAaImF
všem	všecek	k3xTgMnPc3
policistům	policista	k1gMnPc3
<g/>
,	,	kIx,
o	o	k7c6
kterých	který	k3yRgInPc6,k3yQgInPc6,k3yIgInPc6
si	se	k3xPyFc3
myslí	myslet	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
zradili	zradit	k5eAaPmAgMnP
<g/>
,	,	kIx,
včetně	včetně	k7c2
Jamese	Jamese	k1gFnSc2
Gordona	Gordon	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harveyho	Harvey	k1gMnSc2
Denta	Dent	k1gMnSc2
v	v	k7c6
tomto	tento	k3xDgInSc6
filmu	film	k1gInSc6
ztvárnil	ztvárnit	k5eAaPmAgMnS
herec	herec	k1gMnSc1
Aaron	Aaron	k1gMnSc1
Eckhart	Eckharta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Animované	animovaný	k2eAgNnSc1d1
</s>
<s>
Batman	Batman	k1gMnSc1
Beyond	Beyond	k1gMnSc1
<g/>
:	:	kIx,
Return	Return	k1gMnSc1
of	of	k?
the	the	k?
Joker	Joker	k1gInSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c6
tomto	tento	k3xDgInSc6
animovaném	animovaný	k2eAgInSc6d1
filmu	film	k1gInSc6
zasazeném	zasazený	k2eAgInSc6d1
do	do	k7c2
kontinuity	kontinuita	k1gFnSc2
seriálu	seriál	k1gInSc2
Batman	Batman	k1gMnSc1
budoucnosti	budoucnost	k1gFnSc2
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
sice	sice	k8xC
Two-Face	Two-Face	k1gFnSc1
objevil	objevit	k5eAaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
jako	jako	k9
figurína	figurína	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Rok	rok	k1gInSc1
jedna	jeden	k4xCgFnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Film	film	k1gInSc1
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
na	na	k7c4
motivy	motiv	k1gInPc4
stejnojmenného	stejnojmenný	k2eAgInSc2d1
komiksu	komiks	k1gInSc2
od	od	k7c2
Franka	Frank	k1gMnSc2
Millera	Miller	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dent	Dent	k1gInSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
vyobrazen	vyobrazit	k5eAaPmNgInS
v	v	k7c6
období	období	k1gNnSc6
před	před	k7c7
svým	svůj	k3xOyFgNnSc7
znetvořením	znetvoření	k1gNnSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ještě	ještě	k9
zastával	zastávat	k5eAaImAgMnS
místo	místo	k1gNnSc4
gothamského	gothamský	k2eAgMnSc2d1
okresního	okresní	k2eAgMnSc2d1
prokurátora	prokurátor	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlas	hlas	k1gInSc1
postavě	postava	k1gFnSc3
propůjčil	propůjčit	k5eAaPmAgInS
Robin	robin	k2eAgInSc1d1
Atkin	Atkin	k2eAgInSc1d1
Downes	Downes	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Návrat	návrat	k1gInSc1
Temného	temný	k2eAgMnSc2d1
rytíře	rytíř	k1gMnSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
První	první	k4xOgFnSc1
část	část	k1gFnSc1
animovaného	animovaný	k2eAgInSc2d1
filmu	film	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
na	na	k7c4
motivy	motiv	k1gInPc4
dalšího	další	k2eAgInSc2d1
komiksu	komiks	k1gInSc2
od	od	k7c2
Franka	Frank	k1gMnSc2
Millera	Miller	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dent	Dent	k1gInSc1
zde	zde	k6eAd1
podstoupí	podstoupit	k5eAaPmIp3nS
plastickou	plastický	k2eAgFnSc4d1
operaci	operace	k1gFnSc4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
však	však	k9
dojde	dojít	k5eAaPmIp3nS
ke	k	k7c3
zničení	zničení	k1gNnSc3
jeho	jeho	k3xOp3gFnSc2
kladné	kladný	k2eAgFnSc2d1
osobnosti	osobnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavu	postav	k1gInSc2
namluvil	namluvit	k5eAaBmAgMnS,k5eAaPmAgMnS
Wade	Wade	k?
Williams	Williams	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lego	lego	k1gNnSc1
<g/>
:	:	kIx,
Batman	Batman	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Zde	zde	k6eAd1
se	se	k3xPyFc4
Two-Face	Two-Face	k1gFnSc1
objeví	objevit	k5eAaPmIp3nS
jako	jako	k8xS,k8xC
jeden	jeden	k4xCgMnSc1
z	z	k7c2
padouchů	padouch	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
záměrně	záměrně	k6eAd1
přeruší	přerušit	k5eAaPmIp3nP
oslavu	oslava	k1gFnSc4
Muže	muž	k1gMnSc2
roku	rok	k1gInSc2
v	v	k7c4
Gotham	Gotham	k1gInSc4
City	City	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlas	hlas	k1gInSc4
mu	on	k3xPp3gMnSc3
propůjčil	propůjčit	k5eAaPmAgMnS
Troy	Troa	k1gMnSc2
Baker	Baker	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Batmanův	Batmanův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c6
tomto	tento	k3xDgInSc6
filmu	film	k1gInSc6
měl	mít	k5eAaImAgMnS
Two-Face	Two-Face	k1gFnPc4
jen	jen	k6eAd1
menší	malý	k2eAgFnSc4d2
roli	role	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevil	objevit	k5eAaPmAgMnS
se	se	k3xPyFc4
ve	v	k7c6
scéně	scéna	k1gFnSc6
z	z	k7c2
Arkham	Arkham	k1gInSc4
Asylum	Asylum	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
házel	házet	k5eAaImAgMnS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
mincí	mince	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Útok	útok	k1gInSc1
na	na	k7c4
Arkham	Arkham	k1gInSc4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c6
Útoku	útok	k1gInSc6
na	na	k7c4
Arkham	Arkham	k1gInSc4
je	být	k5eAaImIp3nS
Two-Face	Two-Face	k1gFnSc1
jedním	jeden	k4xCgMnSc7
z	z	k7c2
vězňů	vězeň	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
díky	díky	k7c3
Jokerovi	Joker	k1gMnSc3
dostanou	dostat	k5eAaPmIp3nP
na	na	k7c6
svobodu	svoboda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harvey	Harveum	k1gNnPc7
se	se	k3xPyFc4
později	pozdě	k6eAd2
snaží	snažit	k5eAaImIp3nP
nastoupit	nastoupit	k5eAaPmF
do	do	k7c2
policejního	policejní	k2eAgNnSc2d1
auta	auto	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
zastaven	zastavit	k5eAaPmNgMnS
Killer	Killer	k1gMnSc1
Frost	Frost	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
mu	on	k3xPp3gMnSc3
zmrazí	zmrazit	k5eAaPmIp3nS
hlavu	hlava	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Batman	Batman	k1gMnSc1
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joker	Joker	k1gInSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Adaptace	adaptace	k1gFnSc1
komiksu	komiks	k1gInSc2
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Kameňák	Kameňák	k?
a	a	k8xC
další	další	k2eAgInPc4d1
příběhy	příběh	k1gInPc4
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
měl	mít	k5eAaImAgInS
Dent	Dent	k1gInSc1
opět	opět	k6eAd1
jen	jen	k9
malou	malý	k2eAgFnSc4d1
a	a	k8xC
bezvýznamnou	bezvýznamný	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevil	objevit	k5eAaPmAgMnS
se	se	k3xPyFc4
ve	v	k7c6
scéně	scéna	k1gFnSc6
z	z	k7c2
Arkhamu	Arkham	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
natahoval	natahovat	k5eAaImAgMnS
ruku	ruka	k1gFnSc4
skrz	skrz	k7c4
dveře	dveře	k1gFnPc4
své	svůj	k3xOyFgFnSc2
cely	cela	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
dosáhl	dosáhnout	k5eAaPmAgInS
na	na	k7c4
minci	mince	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
ležela	ležet	k5eAaImAgFnS
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Batman	Batman	k1gMnSc1
Unlimited	Unlimited	k1gMnSc1
<g/>
:	:	kIx,
Mech	mech	k1gInSc1
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mutants	Mutants	k1gInSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Animovaný	animovaný	k2eAgInSc1d1
film	film	k1gInSc1
zasazený	zasazený	k2eAgInSc1d1
do	do	k7c2
série	série	k1gFnSc2
s	s	k7c7
názvem	název	k1gInSc7
Všemocný	všemocný	k2eAgMnSc1d1
Batman	Batman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Two-Face	Two-Face	k1gFnSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
vidět	vidět	k5eAaImF
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
cele	cela	k1gFnSc6
v	v	k7c4
Arkham	Arkham	k1gInSc4
Asylum	Asylum	k1gNnSc4
<g/>
,	,	kIx,
kam	kam	k6eAd1
vtrhnou	vtrhnout	k5eAaPmIp3nP
Penguin	Penguin	k1gInSc4
a	a	k8xC
Mr	Mr	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Freeze	Freeze	k1gFnSc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
mu	on	k3xPp3gMnSc3
nabídnou	nabídnout	k5eAaPmIp3nP
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
osvobodí	osvobodit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dent	Dent	k1gMnSc1
si	se	k3xPyFc3
však	však	k9
hodí	hodit	k5eAaImIp3nS,k5eAaPmIp3nS
mincí	mince	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
jelikož	jelikož	k8xS
mu	on	k3xPp3gMnSc3
padne	padnout	k5eAaImIp3nS,k5eAaPmIp3nS
čistá	čistý	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nucen	nutit	k5eAaImNgMnS
odmítnout	odmítnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlas	hlas	k1gInSc1
postavě	postava	k1gFnSc6
propůjčil	propůjčit	k5eAaPmAgMnS
Troy	Troa	k1gMnSc2
Baker	Baker	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
LEGO	lego	k1gNnSc1
Batman	Batman	k1gMnSc1
film	film	k1gInSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c6
tomto	tento	k3xDgInSc6
filmu	film	k1gInSc6
vystupuje	vystupovat	k5eAaImIp3nS
Two-Face	Two-Face	k1gFnSc1
tmavé	tmavý	k2eAgFnSc2d1
pleti	pleť	k1gFnSc2
<g/>
,	,	kIx,
inspirovaný	inspirovaný	k2eAgInSc1d1
postavou	postava	k1gFnSc7
Harveyho	Harvey	k1gMnSc2
Denta	Dent	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
v	v	k7c6
Batmanovi	Batman	k1gMnSc6
z	z	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
role	role	k1gFnSc2
byl	být	k5eAaImAgInS
znovu	znovu	k6eAd1
obsazen	obsadit	k5eAaPmNgInS
Billy	Bill	k1gMnPc7
Dee	Dee	k1gFnSc4
Williams	Williamsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Batman	Batman	k1gMnSc1
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Two-Face	Two-Face	k1gFnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Batman	Batman	k1gMnSc1
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Two-Face	Two-Face	k1gFnSc1
byl	být	k5eAaImAgInS
vytvořen	vytvořen	k2eAgInSc1d1
na	na	k7c4
motivy	motiv	k1gInPc4
nikdy	nikdy	k6eAd1
nenatočené	natočený	k2eNgFnPc4d1
epizody	epizoda	k1gFnPc4
seriálu	seriál	k1gInSc2
Batman	Batman	k1gMnSc1
s	s	k7c7
Adamem	Adam	k1gMnSc7
Westem	West	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
Harvey	Harvea	k1gFnPc1
se	se	k3xPyFc4
zde	zde	k6eAd1
stane	stanout	k5eAaPmIp3nS
Two-Facem	Two-Faec	k1gInSc7
díky	díky	k7c3
experimentu	experiment	k1gInSc3
profesora	profesor	k1gMnSc2
Huga	Hugo	k1gMnSc2
Strange	Strang	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
vytvořil	vytvořit	k5eAaPmAgMnS
extraktor	extraktor	k1gInSc4
zla	zlo	k1gNnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
měl	mít	k5eAaImAgMnS
vysát	vysát	k5eAaPmF
všechno	všechen	k3xTgNnSc4
zlo	zlo	k1gNnSc4
z	z	k7c2
gothamských	gothamský	k2eAgMnPc2d1
zločinců	zločinec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strange	Strang	k1gInSc2
jej	on	k3xPp3gMnSc4
použije	použít	k5eAaPmIp3nS
na	na	k7c4
vícero	vícero	k1gNnSc4
různých	různý	k2eAgMnPc2d1
padouchů	padouch	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
Jokera	Joker	k1gMnSc2
<g/>
,	,	kIx,
Penguina	Penguin	k1gMnSc2
a	a	k8xC
Riddlera	Riddler	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stroj	stroj	k1gInSc1
se	se	k3xPyFc4
však	však	k9
přetíží	přetížit	k5eAaPmIp3nS
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
nádrž	nádrž	k1gFnSc1
exploduje	explodovat	k5eAaBmIp3nS
<g/>
,	,	kIx,
načež	načež	k6eAd1
uniklý	uniklý	k2eAgInSc1d1
plyn	plyn	k1gInSc1
zasáhne	zasáhnout	k5eAaPmIp3nS
Harveyho	Harvey	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
propadne	propadnout	k5eAaPmIp3nS
šílenství	šílenství	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
původním	původní	k2eAgInSc6d1
hraném	hraný	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
si	se	k3xPyFc3
měl	mít	k5eAaImAgMnS
postavu	postava	k1gFnSc4
zahrát	zahrát	k5eAaPmF
Clint	Clint	k1gInSc4
Eastwood	Eastwood	k1gInSc1
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
v	v	k7c6
animovaném	animovaný	k2eAgInSc6d1
filmu	film	k1gInSc6
jí	on	k3xPp3gFnSc2
propůjčil	propůjčit	k5eAaPmAgInS
hlas	hlas	k1gInSc1
William	William	k1gInSc1
Shatner	Shatner	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Gotham	Gotham	k1gInSc1
by	by	kYmCp3nS
Gaslight	Gaslight	k1gInSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c6
tomto	tento	k3xDgInSc6
filmu	film	k1gInSc6
není	být	k5eNaImIp3nS
Harvey	Harvea	k1gFnPc1
padouch	padouch	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
obyčejný	obyčejný	k2eAgMnSc1d1
sukničkář	sukničkář	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
podvést	podvést	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
ženu	žena	k1gFnSc4
se	s	k7c7
Selinou	Selina	k1gFnSc7
Kyleovou	Kyleův	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavu	postav	k1gInSc2
namluvil	namluvit	k5eAaBmAgMnS,k5eAaPmAgMnS
Yuri	Yuri	k1gNnSc4
Lowenthal	Lowenthal	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Suicide	Suicid	k1gMnSc5
Squad	Squad	k1gInSc1
<g/>
:	:	kIx,
Hell	Hell	k1gMnSc1
to	ten	k3xDgNnSc4
Pay	Pay	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Two-Face	Two-Face	k1gFnSc1
je	být	k5eAaImIp3nS
k	k	k7c3
vidění	vidění	k1gNnSc3
na	na	k7c6
začátku	začátek	k1gInSc6
filmu	film	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
chce	chtít	k5eAaImIp3nS
podstoupit	podstoupit	k5eAaPmF
operaci	operace	k1gFnSc4
v	v	k7c6
laboratoři	laboratoř	k1gFnSc6
Professora	Professor	k1gMnSc2
Pyga	Pygus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavu	postav	k1gInSc2
namluvil	namluvit	k5eAaBmAgMnS,k5eAaPmAgMnS
Dave	Dav	k1gInSc5
Boat	Boat	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Batman	Batman	k1gMnSc1
Ninja	Ninja	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c6
tomto	tento	k3xDgInSc6
filmu	film	k1gInSc6
je	být	k5eAaImIp3nS
Two-Face	Two-Face	k1gFnSc1
jedním	jeden	k4xCgMnSc7
z	z	k7c2
gothamských	gothamský	k2eAgMnPc2d1
padouchů	padouch	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
jsou	být	k5eAaImIp3nP
pomocí	pomocí	k7c2
stroje	stroj	k1gInSc2
na	na	k7c4
zemětřesení	zemětřesení	k1gNnPc2
vtaženi	vtáhnout	k5eAaPmNgMnP
do	do	k7c2
období	období	k1gNnSc2
feudálního	feudální	k2eAgNnSc2d1
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Hlas	hlas	k1gInSc1
postavě	postava	k1gFnSc6
propůjčili	propůjčit	k5eAaPmAgMnP
Tošijuki	Tošijuk	k1gMnPc1
Morikawa	Morikaw	k1gInSc2
(	(	kIx(
<g/>
japonská	japonský	k2eAgFnSc1d1
verze	verze	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Eric	Eric	k1gFnSc1
Bauza	Bauza	k1gFnSc1
(	(	kIx(
<g/>
americká	americký	k2eAgFnSc1d1
verze	verze	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Justice	justice	k1gFnSc1
League	Leagu	k1gFnSc2
vs	vs	k?
<g/>
.	.	kIx.
the	the	k?
Fatal	Fatal	k1gInSc1
Five	Five	k1gFnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Harvey	Harvea	k1gMnSc2
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
padouchů	padouch	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
v	v	k7c4
Arkham	Arkham	k1gInSc4
Asylum	Asylum	k1gInSc1
spřátelí	spřátelit	k5eAaPmIp3nS
se	s	k7c7
Star	star	k1gFnSc7
Boyem	boy	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavu	postav	k1gInSc2
namluvil	namluvit	k5eAaPmAgMnS,k5eAaBmAgMnS
Bruce	Bruce	k1gMnSc1
Timm	Timm	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Batman	Batman	k1gMnSc1
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teenage	Teenag	k1gInSc2
Mutant	mutant	k1gMnSc1
Ninja	Ninja	k1gMnSc1
Turtles	Turtles	k1gMnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c6
animovaném	animovaný	k2eAgInSc6d1
crossoveru	crossover	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
Two-Face	Two-Face	k1gFnSc1
zmutuje	zmutovat	k5eAaPmIp3nS
do	do	k7c2
podoby	podoba	k1gFnSc2
dvouhlavé	dvouhlavý	k2eAgFnSc2d1
kočičí	kočičí	k2eAgFnSc2d1
nestvůry	nestvůra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlas	hlas	k1gInSc1
postavě	postava	k1gFnSc3
propůjčil	propůjčit	k5eAaPmAgInS
Keith	Keith	k1gInSc1
Ferguson	Ferguson	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lego	lego	k1gNnSc1
DC	DC	kA
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Family	Famil	k1gInPc1
Matters	Mattersa	k1gFnPc2
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c4
Lego	lego	k1gNnSc4
DC	DC	kA
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Family	Famil	k1gInPc4
Matters	Mattersa	k1gFnPc2
si	se	k3xPyFc3
Two-Face	Two-Face	k1gFnSc1
koupí	koupit	k5eAaPmIp3nS
společnost	společnost	k1gFnSc4
Wayne	Wayn	k1gInSc5
Enterprises	Enterprisesa	k1gFnPc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
mu	on	k3xPp3gMnSc3
nevědomky	nevědomky	k6eAd1
prodá	prodat	k5eAaPmIp3nS
sám	sám	k3xTgMnSc1
Batman	Batman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavu	postav	k1gInSc2
namluvil	namluvit	k5eAaPmAgMnS,k5eAaBmAgMnS
Christian	Christian	k1gMnSc1
Lanz	Lanz	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Seriály	seriál	k1gInPc1
</s>
<s>
Hrané	hraný	k2eAgNnSc1d1
</s>
<s>
Batman	Batman	k1gMnSc1
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
–	–	k?
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Pro	pro	k7c4
tento	tento	k3xDgInSc4
seriál	seriál	k1gInSc4
byla	být	k5eAaImAgFnS
zvažována	zvažován	k2eAgFnSc1d1
epizoda	epizoda	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
by	by	kYmCp3nS
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgInS
Two-Face	Two-Face	k1gFnPc4
s	s	k7c7
tváří	tvář	k1gFnSc7
Clinta	Clint	k1gMnSc2
Eastwooda	Eastwood	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Než	než	k8xS
však	však	k9
tvůrci	tvůrce	k1gMnPc1
stačili	stačit	k5eAaBmAgMnP
začít	začít	k5eAaPmF
natáčet	natáčet	k5eAaImF
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
seriál	seriál	k1gInSc1
zrušen	zrušit	k5eAaPmNgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Gotham	Gotham	k1gInSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c6
seriálu	seriál	k1gInSc6
Gotham	Gotham	k1gInSc1
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgInS
Harvey	Harvey	k1gInPc4
Dent	Denta	k1gFnPc2
z	z	k7c2
období	období	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
stál	stát	k5eAaImAgInS
ještě	ještě	k9
na	na	k7c6
straně	strana	k1gFnSc6
zákona	zákon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
tak	tak	k6eAd1
stalo	stát	k5eAaPmAgNnS
v	v	k7c6
epizodě	epizoda	k1gFnSc6
s	s	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Harvey	Harvea	k1gFnSc2
Dent	Dent	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
se	se	k3xPyFc4
spojil	spojit	k5eAaPmAgMnS
s	s	k7c7
Jamesem	James	k1gMnSc7
Gordonem	Gordon	k1gMnSc7
a	a	k8xC
pomáhal	pomáhat	k5eAaImAgInS
mu	on	k3xPp3gMnSc3
s	s	k7c7
případem	případ	k1gInSc7
vraždy	vražda	k1gFnSc2
rodičů	rodič	k1gMnPc2
Bruce	Bruce	k1gMnSc1
Waynea	Waynea	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
epizodě	epizoda	k1gFnSc6
„	„	k?
<g/>
Dílo	dílo	k1gNnSc1
lásky	láska	k1gFnSc2
<g/>
“	“	k?
byl	být	k5eAaImAgInS
Gordonem	Gordon	k1gInSc7
<g />
.	.	kIx.
</s>
<s hack="1">
konfrontován	konfrontován	k2eAgInSc1d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
nedopatřením	nedopatření	k1gNnSc7
prozradil	prozradit	k5eAaPmAgInS
úkryt	úkryt	k1gInSc1
Seliny	Selin	k2eAgFnSc2d1
Kyleové	Kyleová	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
svědkem	svědek	k1gMnSc7
vraždy	vražda	k1gFnSc2
Wayneových	Wayneův	k2eAgFnPc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
v	v	k7c6
epizodě	epizoda	k1gFnSc6
„	„	k?
<g/>
Každý	každý	k3xTgInSc1
má	mít	k5eAaImIp3nS
svého	svůj	k3xOyFgMnSc2
Cobblepota	Cobblepot	k1gMnSc2
<g/>
“	“	k?
pomáhal	pomáhat	k5eAaImAgInS
Gordonovi	Gordon	k1gMnSc3
odhalit	odhalit	k5eAaPmF
tajemství	tajemství	k1gNnSc4
komisaře	komisař	k1gMnSc2
Gilliana	Gillian	k1gMnSc2
Loeba	Loeb	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Objevil	objevit	k5eAaPmAgMnS
se	se	k3xPyFc4
také	také	k9
v	v	k7c6
epizodě	epizoda	k1gFnSc6
„	„	k?
<g/>
Maminčina	maminčin	k2eAgFnSc1d1
zrůdička	zrůdička	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
vydal	vydat	k5eAaPmAgInS
zatykač	zatykač	k1gInSc1
na	na	k7c4
Penguina	Penguin	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
epizodě	epizoda	k1gFnSc6
„	„	k?
<g/>
Syn	syn	k1gMnSc1
Gothamu	Gotham	k1gInSc2
<g/>
“	“	k?
zase	zase	k9
stíhal	stíhat	k5eAaImAgMnS
miliardáře	miliardář	k1gMnSc4
Thea	Thea	k1gFnSc1
Galavana	Galavan	k1gMnSc2
za	za	k7c4
únos	únos	k1gInSc4
starosty	starosta	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Následně	následně	k6eAd1
v	v	k7c6
epizodě	epizoda	k1gFnSc6
„	„	k?
<g/>
Pan	Pan	k1gMnSc1
Mráz	Mráz	k1gMnSc1
<g/>
“	“	k?
pochyboval	pochybovat	k5eAaImAgMnS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
detektiv	detektiv	k1gMnSc1
Gordon	Gordon	k1gMnSc1
nebyl	být	k5eNaImAgMnS
zapleten	zaplést	k5eAaPmNgMnS
v	v	k7c6
Galavanově	Galavanův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
poslední	poslední	k2eAgFnSc1d1
epizoda	epizoda	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
se	se	k3xPyFc4
Harvey	Harvey	k1gInPc1
Dent	Denta	k1gFnPc2
objevil	objevit	k5eAaPmAgMnS
<g/>
,	,	kIx,
nese	nést	k5eAaImIp3nS
název	název	k1gInSc1
„	„	k?
<g/>
Vězni	vězeň	k1gMnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ní	on	k3xPp3gFnSc6
byl	být	k5eAaImAgMnS
budoucí	budoucí	k2eAgFnSc2d1
Two-Face	Two-Face	k1gFnSc2
osloven	osloven	k2eAgInSc4d1
detektivem	detektiv	k1gMnSc7
Harveym	Harveym	k1gInSc4
Bullockem	Bullock	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
chtěl	chtít	k5eAaImAgInS
pomoci	pomoct	k5eAaPmF
dostat	dostat	k5eAaPmF
Gordona	Gordon	k1gMnSc4
z	z	k7c2
vězení	vězení	k1gNnSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
byl	být	k5eAaImAgInS
zavřen	zavřít	k5eAaPmNgInS
za	za	k7c4
vraždu	vražda	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Roli	role	k1gFnSc4
Denta	Dent	k1gMnSc2
ztvárnil	ztvárnit	k5eAaPmAgMnS
Nicholas	Nicholas	k1gMnSc1
D	D	kA
<g/>
'	'	kIx"
<g/>
Agosto	Agosta	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Titans	Titans	k6eAd1
(	(	kIx(
<g/>
od	od	k7c2
r.	r.	kA
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Two-Face	Two-Face	k1gFnSc1
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgMnS
v	v	k7c6
epizodě	epizoda	k1gFnSc6
s	s	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Dick	Dick	k1gMnSc1
Grayson	Grayson	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
měl	mít	k5eAaImAgInS
pouze	pouze	k6eAd1
obyčejný	obyčejný	k2eAgInSc1d1
„	„	k?
<g/>
štěk	štěk	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Animované	animovaný	k2eAgNnSc1d1
</s>
<s>
Batman	Batman	k1gMnSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Postava	postava	k1gFnSc1
Harveyho	Harvey	k1gMnSc2
Denta	Dent	k1gMnSc2
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
objevila	objevit	k5eAaPmAgFnS
v	v	k7c6
epizodě	epizoda	k1gFnSc6
„	„	k?
<g/>
On	on	k3xPp3gMnSc1
Leather	Leathra	k1gFnPc2
Wings	Wings	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
slíbí	slíbit	k5eAaPmIp3nS
Harveymu	Harveym	k1gInSc2
Bullockovi	Bullockův	k2eAgMnPc1d1
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
stíhat	stíhat	k5eAaImF
Batmana	Batman	k1gMnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
jej	on	k3xPp3gMnSc4
detektiv	detektiv	k1gMnSc1
zajme	zajmout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
V	v	k7c6
epizodě	epizoda	k1gFnSc6
„	„	k?
<g/>
Pretty	Pretta	k1gFnSc2
Poison	Poison	k1gMnSc1
<g/>
“	“	k?
je	být	k5eAaImIp3nS
Harvey	Harvea	k1gMnSc2
otráven	otrávit	k5eAaPmNgInS
při	při	k7c6
večeři	večeře	k1gFnSc6
s	s	k7c7
Brucem	Bruce	k1gMnSc7
Waynem	Wayn	k1gMnSc7
a	a	k8xC
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
snoubenkou	snoubenka	k1gFnSc7
Pamelou	Pamela	k1gFnSc7
Isleyovou	Isleyová	k1gFnSc7
<g/>
,	,	kIx,
naštěstí	naštěstí	k6eAd1
mu	on	k3xPp3gNnSc3
však	však	k9
Batman	Batman	k1gMnSc1
dokáže	dokázat	k5eAaPmIp3nS
opatřit	opatřit	k5eAaPmF
protijed	protijed	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
dvojepizodě	dvojepizoda	k1gFnSc6
s	s	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Two-Face	Two-Face	k1gFnSc2
<g/>
“	“	k?
<g />
.	.	kIx.
</s>
<s hack="1">
se	se	k3xPyFc4
z	z	k7c2
Denta	Dent	k1gInSc2
stává	stávat	k5eAaImIp3nS
padouch	padouch	k1gMnSc1
<g/>
,	,	kIx,
za	za	k7c4
což	což	k3yRnSc4,k3yQnSc4
může	moct	k5eAaImIp3nS
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
mafiánský	mafiánský	k2eAgMnSc1d1
boss	boss	k1gMnSc1
Rupert	Rupert	k1gMnSc1
Thorne	Thorn	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
další	další	k2eAgFnSc6d1
dvojepizodě	dvojepizoda	k1gFnSc6
„	„	k?
<g/>
Shadow	Shadow	k1gFnSc2
of	of	k?
the	the	k?
Bat	Bat	k1gFnSc1
<g/>
“	“	k?
se	se	k3xPyFc4
Two-Face	Two-Face	k1gFnSc1
spojí	spojit	k5eAaPmIp3nS
se	s	k7c7
zkorumpovaným	zkorumpovaný	k2eAgMnSc7d1
úředníkem	úředník	k1gMnSc7
Gilem	Gil	k1gMnSc7
Masonem	mason	k1gMnSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
společně	společně	k6eAd1
uvěznili	uvěznit	k5eAaPmAgMnP
komisaře	komisař	k1gMnPc4
Gordona	Gordon	k1gMnSc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
by	by	kYmCp3nP
vyčistili	vyčistit	k5eAaPmAgMnP
cestu	cesta	k1gFnSc4
pro	pro	k7c4
své	svůj	k3xOyFgInPc4
zločiny	zločin	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
obvykle	obvykle	k6eAd1
byly	být	k5eAaImAgInP
Two-Faceovy	Two-Faceův	k2eAgInPc1d1
plány	plán	k1gInPc1
zmařeny	zmařit	k5eAaPmNgInP
Batmanem	Batman	k1gInSc7
a	a	k8xC
Robinem	Robino	k1gNnSc7
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
tentokrát	tentokrát	k6eAd1
jim	on	k3xPp3gFnPc3
pomáhal	pomáhat	k5eAaImAgMnS
také	také	k9
jejich	jejich	k3xOp3gMnSc1
nový	nový	k2eAgMnSc1d1
spojenec	spojenec	k1gMnSc1
<g/>
,	,	kIx,
kterým	který	k3yQgMnSc7,k3yIgMnSc7,k3yRgMnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Batgirl	Batgirl	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
epizodě	epizoda	k1gFnSc6
„	„	k?
<g/>
Trial	trial	k1gInSc1
<g/>
“	“	k?
se	se	k3xPyFc4
z	z	k7c2
Two-Face	Two-Face	k1gFnSc2
stal	stát	k5eAaPmAgMnS
„	„	k?
<g/>
žalobce	žalobce	k1gMnSc1
<g/>
“	“	k?
ve	v	k7c6
vykonstruovaném	vykonstruovaný	k2eAgInSc6d1
procesu	proces	k1gInSc6
s	s	k7c7
Batmanem	Batman	k1gMnSc7
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yIgMnSc4,k3yQgMnSc4
gothamští	gothamský	k2eAgMnPc1d1
padouši	padouch	k1gMnPc1
věznili	věznit	k5eAaImAgMnP
v	v	k7c6
Arkhamu	Arkham	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
epizody	epizoda	k1gFnSc2
s	s	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Second	Second	k1gInSc1
Change	change	k1gFnSc1
<g/>
“	“	k?
podstoupí	podstoupit	k5eAaPmIp3nS
Dent	Dent	k1gMnSc1
plastickou	plastický	k2eAgFnSc4d1
operaci	operace	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zničil	zničit	k5eAaPmAgMnS
osobnost	osobnost	k1gFnSc4
Two-Face	Two-Face	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Než	než	k8xS
však	však	k9
k	k	k7c3
operaci	operace	k1gFnSc3
dojde	dojít	k5eAaPmIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Harvey	Harve	k1gMnPc4
unesen	unesen	k2eAgInSc1d1
na	na	k7c4
příkaz	příkaz	k1gInSc4
samotného	samotný	k2eAgNnSc2d1
Two-Face	Two-Face	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
chce	chtít	k5eAaImIp3nS
i	i	k9
nadále	nadále	k6eAd1
ovládat	ovládat	k5eAaImF
jeho	jeho	k3xOp3gFnSc4
psychiku	psychika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
padoucha	padouch	k1gMnSc4
dopadnou	dopadnout	k5eAaPmIp3nP
Batman	Batman	k1gMnSc1
a	a	k8xC
Robin	robin	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
Postavu	postav	k1gInSc2
namluvil	namluvit	k5eAaPmAgMnS,k5eAaBmAgMnS
Richard	Richard	k1gMnSc1
Moll	moll	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Batman	Batman	k1gMnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Seriál	seriál	k1gInSc1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
ten	ten	k3xDgInSc4
předchozí	předchozí	k2eAgInSc4d1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
vysílal	vysílat	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1992	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
Two-Face	Two-Face	k1gFnSc1
se	se	k3xPyFc4
zde	zde	k6eAd1
poprvé	poprvé	k6eAd1
objeví	objevit	k5eAaPmIp3nS
v	v	k7c6
epizodě	epizoda	k1gFnSc6
„	„	k?
<g/>
Sins	Sinsa	k1gFnPc2
of	of	k?
the	the	k?
Father	Fathra	k1gFnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
“	“	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
příčin	příčina	k1gFnPc2
toho	ten	k3xDgInSc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
z	z	k7c2
mladého	mladý	k2eAgInSc2d1
Tima	Tim	k1gInSc2
Drakea	Drakeum	k1gNnSc2
stává	stávat	k5eAaImIp3nS
nový	nový	k2eAgInSc1d1
Robin	robin	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
Epizoda	epizoda	k1gFnSc1
„	„	k?
<g/>
Judgement	Judgement	k1gInSc1
Day	Day	k1gFnSc1
<g/>
“	“	k?
odhaluje	odhalovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Dentova	Dentův	k2eAgFnSc1d1
psychika	psychika	k1gFnSc1
roztříštila	roztříštit	k5eAaPmAgFnS
na	na	k7c4
další	další	k2eAgInSc4d1
fragment	fragment	k1gInSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
osobnosti	osobnost	k1gFnSc2
soudního	soudní	k2eAgMnSc2d1
mstitele	mstitel	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
pomocí	pomocí	k7c2
extrémních	extrémní	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
zatýká	zatýkat	k5eAaImIp3nS
zločince	zločinec	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Batman	Batman	k1gMnSc1
budoucnosti	budoucnost	k1gFnSc2
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c6
epizodě	epizoda	k1gFnSc6
„	„	k?
<g/>
Terry	Terra	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Friend	Friend	k1gMnSc1
Dates	Dates	k1gMnSc1
a	a	k8xC
Robot	robot	k1gMnSc1
<g/>
“	“	k?
se	se	k3xPyFc4
objeví	objevit	k5eAaPmIp3nS
robot	robot	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vypadá	vypadat	k5eAaImIp3nS,k5eAaPmIp3nS
přesně	přesně	k6eAd1
jako	jako	k9
Two-Face	Two-Face	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Liga	liga	k1gFnSc1
spravedlivých	spravedlivý	k2eAgMnPc2d1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Dent	Dent	k1gMnSc1
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgMnS
v	v	k7c6
epizodě	epizoda	k1gFnSc6
„	„	k?
<g/>
A	a	k9
Better	Better	k1gMnSc1
World	World	k1gMnSc1
<g/>
:	:	kIx,
Part	part	k1gInSc1
II	II	kA
<g/>
“	“	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
padouchů	padouch	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
v	v	k7c6
alternativní	alternativní	k2eAgFnSc6d1
realitě	realita	k1gFnSc6
podstoupili	podstoupit	k5eAaPmAgMnP
lobotomii	lobotomie	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Odvážný	odvážný	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
Two-Face	Two-Face	k1gFnSc1
objevil	objevit	k5eAaPmAgInS
v	v	k7c6
epizodě	epizoda	k1gFnSc6
s	s	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Legendy	legenda	k1gFnSc2
temného	temný	k2eAgInSc2d1
caparta	caparta	k?
<g/>
“	“	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
součástí	součást	k1gFnSc7
Bat-Miteovy	Bat-Miteův	k2eAgFnSc2d1
halucinace	halucinace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
Následně	následně	k6eAd1
si	se	k3xPyFc3
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgInPc7d1
<g />
.	.	kIx.
</s>
<s hack="1">
padouchy	padouch	k1gMnPc4
zazpíval	zazpívat	k5eAaPmAgInS
v	v	k7c6
muzikálové	muzikálový	k2eAgFnSc6d1
epizodě	epizoda	k1gFnSc6
„	„	k?
<g/>
Zmatek	zmatek	k1gInSc1
mezi	mezi	k7c7
hudebními	hudební	k2eAgMnPc7d1
mistry	mistr	k1gMnPc7
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
začátku	začátek	k1gInSc6
epizody	epizoda	k1gFnSc2
nazvané	nazvaný	k2eAgFnSc2d1
„	„	k?
<g/>
Equinoxův	Equinoxův	k2eAgInSc1d1
osud	osud	k1gInSc1
<g/>
“	“	k?
se	se	k3xPyFc4
spojí	spojit	k5eAaPmIp3nS
s	s	k7c7
Batmanem	Batman	k1gInSc7
proti	proti	k7c3
svým	svůj	k3xOyFgMnPc3
nohsledům	nohsled	k1gMnPc3
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
mu	on	k3xPp3gMnSc3
to	ten	k3xDgNnSc4
ukázala	ukázat	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnPc4
mince	mince	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
84	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
epizodě	epizoda	k1gFnSc6
„	„	k?
<g/>
Sraz	sraz	k1gInSc1
pobočníků	pobočník	k1gMnPc2
<g/>
“	“	k?
se	se	k3xPyFc4
Two-Face	Two-Face	k1gFnSc1
objeví	objevit	k5eAaPmIp3nS
jako	jako	k9
iluze	iluze	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
vyvolá	vyvolat	k5eAaPmIp3nS
Batman	Batman	k1gMnSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vyzkoušel	vyzkoušet	k5eAaPmAgMnS
<g/>
,	,	kIx,
jestli	jestli	k8xS
Robin	robin	k2eAgInSc4d1
<g/>
,	,	kIx,
Aqualad	Aqualad	k1gInSc4
a	a	k8xC
Speedy	Speed	k1gMnPc4
dokážou	dokázat	k5eAaPmIp3nP
bojovat	bojovat	k5eAaImF
jako	jako	k9
jeden	jeden	k4xCgInSc4
tým	tým	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
Následně	následně	k6eAd1
v	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
epizodě	epizoda	k1gFnSc6
„	„	k?
<g/>
Chlad	chlad	k1gInSc1
noci	noc	k1gFnSc2
<g/>
“	“	k?
je	být	k5eAaImIp3nS
Dent	Dent	k1gInSc1
jedním	jeden	k4xCgInSc7
z	z	k7c2
gothamských	gothamský	k2eAgMnPc2d1
padouchů	padouch	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
na	na	k7c4
černo	černo	k1gNnSc4
obchodují	obchodovat	k5eAaImIp3nP
s	s	k7c7
Joem	Joem	k1gMnSc1
Chillem	Chill	k1gMnSc7
—	—	k?
vrahem	vrah	k1gMnSc7
Batmanových	Batmanův	k2eAgMnPc2d1
rodičů	rodič	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
epizody	epizoda	k1gFnSc2
s	s	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Maska	maska	k1gFnSc1
Matchese	Matchese	k1gFnSc1
Malona	Malona	k1gFnSc1
<g/>
“	“	k?
ukradne	ukradnout	k5eAaPmIp3nS
z	z	k7c2
muzea	muzeum	k1gNnSc2
plášť	plášť	k1gInSc4
Nefertiti	Nefertiti	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
poté	poté	k6eAd1
draží	dražit	k5eAaImIp3nS
na	na	k7c4
speciální	speciální	k2eAgFnSc4d1
aukci	aukce	k1gFnSc4
pro	pro	k7c4
zločince	zločinec	k1gMnSc4
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
na	na	k7c6
začátku	začátek	k1gInSc6
epizody	epizoda	k1gFnSc2
„	„	k?
<g/>
Crisis	Crisis	k1gFnSc2
<g/>
:	:	kIx,
22,300	22,300	k4
Miles	Miles	k1gInSc1
Above	Aboev	k1gFnSc2
Earth	Eartha	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
je	být	k5eAaImIp3nS
přítomen	přítomen	k2eAgMnSc1d1
v	v	k7c6
divadle	divadlo	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
Joker	Joker	k1gMnSc1
opéká	opékat	k5eAaImIp3nS
svázaného	svázaný	k2eAgMnSc4d1
Batmana	Batman	k1gMnSc4
na	na	k7c6
roštu	rošt	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Young	Young	k1gMnSc1
Justice	justice	k1gFnSc2
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c4
Young	Young	k1gInSc4
Justice	justice	k1gFnSc2
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgMnS
Paul	Paul	k1gMnSc1
Sloane	Sloan	k1gMnSc5
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yRgInSc2,k3yQgInSc2,k3yIgInSc2
se	se	k3xPyFc4
později	pozdě	k6eAd2
nešťastnou	šťastný	k2eNgFnSc7d1
náhodou	náhoda	k1gFnSc7
stane	stanout	k5eAaPmIp3nS
další	další	k2eAgFnSc1d1
Two-Face	Two-Face	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
postavu	postava	k1gFnSc4
namluvil	namluvit	k5eAaBmAgMnS,k5eAaPmAgMnS
Kevin	Kevin	k1gMnSc1
Michael	Michael	k1gMnSc1
Richardson	Richardson	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Beware	Bewar	k1gMnSc5
the	the	k?
Batman	Batman	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Harvey	Harvey	k1gInPc4
Dent	Dent	k1gInSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
nepřátelským	přátelský	k2eNgMnSc7d1
okresním	okresní	k2eAgMnSc7d1
prokurátorem	prokurátor	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
postaví	postavit	k5eAaPmIp3nS
proti	proti	k7c3
maskovaným	maskovaný	k2eAgMnPc3d1
mstitelům	mstitel	k1gMnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
tak	tak	k6eAd1
pomohl	pomoct	k5eAaPmAgMnS
své	svůj	k3xOyFgFnSc3
kampani	kampaň	k1gFnSc3
na	na	k7c4
starostu	starosta	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
je	být	k5eAaImIp3nS
znetvořen	znetvořen	k2eAgMnSc1d1
při	při	k7c6
explozi	exploze	k1gFnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
padouchem	padouch	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
Postavu	postav	k1gInSc2
namluvil	namluvit	k5eAaBmAgMnS,k5eAaPmAgMnS
herec	herec	k1gMnSc1
Christopher	Christophra	k1gFnPc2
McDonald	McDonald	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1
Titáni	Titán	k1gMnPc1
do	do	k7c2
toho	ten	k3xDgNnSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
od	od	k7c2
r.	r.	kA
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Two-Face	Two-Face	k1gFnSc1
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgMnS
v	v	k7c6
epizodách	epizoda	k1gFnPc6
„	„	k?
<g/>
The	The	k1gFnSc2
Dimensions	Dimensionsa	k1gFnPc2
Crisis	Crisis	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
„	„	k?
<g/>
No	no	k9
Power	Power	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
„	„	k?
<g/>
Sidekick	Sidekick	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
„	„	k?
<g/>
The	The	k1gFnSc2
Titans	Titansa	k1gFnPc2
Show	show	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
„	„	k?
<g/>
Butt	Butt	k2eAgInSc4d1
Atoms	Atoms	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
„	„	k?
<g/>
Bat	Bat	k1gFnSc1
Scouts	Scouts	k1gInSc1
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
„	„	k?
<g/>
Egg	Egg	k1gFnSc1
Hunt	hunt	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Justice	justice	k1gFnPc1
League	Leagu	k1gInSc2
Action	Action	k1gInSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c6
epizodě	epizoda	k1gFnSc6
s	s	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Double	double	k2eAgInSc1d1
Cross	Cross	k1gInSc1
<g/>
“	“	k?
se	se	k3xPyFc4
Plastic	Plastice	k1gFnPc2
Man	Man	k1gMnSc1
přemění	přeměnit	k5eAaPmIp3nS
na	na	k7c4
Two-Face	Two-Face	k1gFnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
společně	společně	k6eAd1
s	s	k7c7
Batmanem	Batman	k1gInSc7
mohl	moct	k5eAaImAgInS
připravit	připravit	k5eAaPmF
past	past	k1gFnSc4
na	na	k7c4
Deadshota	Deadshot	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
zkomplikuje	zkomplikovat	k5eAaPmIp3nS
skutečný	skutečný	k2eAgInSc1d1
Two-Face	Two-Face	k1gFnSc1
<g/>
,	,	kIx,
kterému	který	k3yIgInSc3,k3yRgInSc3,k3yQgInSc3
se	se	k3xPyFc4
podaří	podařit	k5eAaPmIp3nS
utéct	utéct	k5eAaPmF
z	z	k7c2
vazby	vazba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
Harveyho	Harvey	k1gMnSc4
Denta	Dent	k1gMnSc4
namluvil	namluvit	k5eAaBmAgMnS,k5eAaPmAgMnS
Robert	Robert	k1gMnSc1
Picardo	Picardo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Harley	harley	k1gInSc1
Quinn	Quinna	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
r.	r.	kA
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
Two-Face	Two-Face	k1gFnSc1
objevil	objevit	k5eAaPmAgInS
v	v	k7c6
epizodě	epizoda	k1gFnSc6
„	„	k?
<g/>
A	A	kA
High	High	k1gInSc1
Bar	bar	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
Banem	Ban	k1gMnSc7
a	a	k8xC
Scarecrowem	Scarecrow	k1gMnSc7
<g/>
,	,	kIx,
hostem	host	k1gMnSc7
na	na	k7c4
Bar	bar	k1gInSc4
micva	micvo	k1gNnSc2
Penguinova	Penguinův	k2eAgMnSc2d1
synovce	synovec	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
sérii	série	k1gFnSc6
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
v	v	k7c6
epizodě	epizoda	k1gFnSc6
s	s	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
New	New	k1gFnSc1
Gotham	Gotham	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
se	se	k3xPyFc4
spojil	spojit	k5eAaPmAgMnS
s	s	k7c7
Penguinem	Penguin	k1gInSc7
<g/>
,	,	kIx,
Mr	Mr	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Freezem	Freez	k1gInSc7
<g/>
,	,	kIx,
Banem	Ban	k1gInSc7
a	a	k8xC
Riddlerem	Riddler	k1gInSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
společně	společně	k6eAd1
dali	dát	k5eAaPmAgMnP
vzniknout	vzniknout	k5eAaPmF
uskupení	uskupení	k1gNnSc4
zvanému	zvaný	k2eAgInSc3d1
Injustice	Injustika	k1gFnSc3
League	League	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
epizodě	epizoda	k1gFnSc6
„	„	k?
<g/>
Batman	Batman	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Back	Back	k1gMnSc1
Man	Man	k1gMnSc1
<g/>
“	“	k?
se	se	k3xPyFc4
Dent	Dent	k2eAgMnSc1d1
společně	společně	k6eAd1
s	s	k7c7
Banem	Ban	k1gInSc7
snaží	snažit	k5eAaImIp3nS
převzít	převzít	k5eAaPmF
moc	moc	k6eAd1
nad	nad	k7c7
Gothamem	Gotham	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
vládne	vládnout	k5eAaImIp3nS
anarchie	anarchie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
epizodě	epizoda	k1gFnSc6
„	„	k?
<g/>
There	Ther	k1gInSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
No	no	k9
Place	plac	k1gInSc6
To	to	k9
Go	Go	k1gMnSc1
But	But	k1gMnSc1
Down	Down	k1gMnSc1
<g/>
“	“	k?
si	se	k3xPyFc3
Two-Face	Two-Face	k1gFnSc1
zahraje	zahrát	k5eAaPmIp3nS
na	na	k7c4
soudce	soudce	k1gMnPc4
<g/>
,	,	kIx,
když	když	k8xS
odsoudí	odsoudit	k5eAaPmIp3nS
Harley	harley	k1gInSc4
Quinn	Quinna	k1gFnPc2
a	a	k8xC
Poison	Poisona	k1gFnPc2
Ivy	Iva	k1gFnSc2
k	k	k7c3
životu	život	k1gInSc3
v	v	k7c6
Baneově	Baneův	k2eAgNnSc6d1
vězení	vězení	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
tomto	tento	k3xDgInSc6
seriálu	seriál	k1gInSc6
postavu	postav	k1gInSc2
namluvil	namluvit	k5eAaBmAgMnS,k5eAaPmAgMnS
Andy	Anda	k1gFnPc4
Daly	dát	k5eAaPmAgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Videohry	videohra	k1gFnPc1
</s>
<s>
Arkhamverse	Arkhamverse	k1gFnSc1
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Arkham	Arkham	k1gInSc1
Asylum	Asylum	k1gNnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c6
první	první	k4xOgFnSc6
videohře	videohra	k1gFnSc6
ze	z	k7c2
série	série	k1gFnSc2
Arkhamverse	Arkhamverse	k1gFnSc2
se	se	k3xPyFc4
Two-Face	Two-Face	k1gFnSc1
sice	sice	k8xC
přímo	přímo	k6eAd1
neobjevil	objevit	k5eNaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
během	během	k7c2
příběhu	příběh	k1gInSc2
je	být	k5eAaImIp3nS
na	na	k7c4
něj	on	k3xPp3gMnSc4
odkazováno	odkazován	k2eAgNnSc1d1
a	a	k8xC
hráč	hráč	k1gMnSc1
může	moct	k5eAaImIp3nS
v	v	k7c6
ústavu	ústav	k1gInSc6
najít	najít	k5eAaPmF
také	také	k9
jeho	jeho	k3xOp3gFnSc4
celu	cela	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Arkham	Arkham	k1gInSc1
City	city	k1gNnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
videohře	videohra	k1gFnSc6
je	být	k5eAaImIp3nS
Two-Face	Two-Face	k1gFnSc2
jedním	jeden	k4xCgInSc7
ze	z	k7c2
tří	tři	k4xCgInPc2
hlavních	hlavní	k2eAgMnPc2d1
vůdců	vůdce	k1gMnPc2
gangů	gang	k1gInPc2
bojujících	bojující	k2eAgFnPc2d1
o	o	k7c4
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c4
Arkham	Arkham	k1gInSc4
City	City	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
základnu	základna	k1gFnSc4
si	se	k3xPyFc3
zřídil	zřídit	k5eAaPmAgMnS
v	v	k7c6
soudní	soudní	k2eAgFnSc6d1
budově	budova	k1gFnSc6
Solomona	Solomona	k1gFnSc1
Waynea	Waynea	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
si	se	k3xPyFc3
upravil	upravit	k5eAaPmAgMnS
tak	tak	k9
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážela	odrážet	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc4
rozdvojenou	rozdvojený	k2eAgFnSc4d1
osobnost	osobnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Two-Faceův	Two-Faceův	k2eAgInSc1d1
gang	gang	k1gInSc1
ovládá	ovládat	k5eAaImIp3nS
centrální	centrální	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
Arkham	Arkham	k1gInSc4
City	City	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Jokerův	Jokerův	k2eAgInSc1d1
východní	východní	k2eAgInSc1d1
a	a	k8xC
Penguinův	Penguinův	k2eAgMnSc1d1
zase	zase	k9
jižní	jižní	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Two-Face	Two-Face	k1gFnSc1
zajme	zajmout	k5eAaPmIp3nS
Catwoman	Catwoman	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
vloupala	vloupat	k5eAaPmAgFnS
do	do	k7c2
sejfu	sejf	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
drží	držet	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
svázanou	svázaný	k2eAgFnSc4d1
nad	nad	k7c7
nádrží	nádrž	k1gFnSc7
s	s	k7c7
kyselinou	kyselina	k1gFnSc7
v	v	k7c6
soudní	soudní	k2eAgFnSc6d1
síni	síň	k1gFnSc6
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
ji	on	k3xPp3gFnSc4
musí	muset	k5eAaImIp3nS
vysvobodit	vysvobodit	k5eAaPmF
Batman	Batman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
následně	následně	k6eAd1
nad	nad	k7c4
nádrž	nádrž	k1gFnSc4
pověsí	pověsit	k5eAaPmIp3nS
samotného	samotný	k2eAgMnSc4d1
Two-Face	Two-Face	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Arkham	Arkham	k1gInSc1
Origins	Origins	k1gInSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c4
Arkham	Arkham	k1gInSc4
Origins	Originsa	k1gFnPc2
se	se	k3xPyFc4
Two-Face	Two-Face	k1gFnSc2
neobjevil	objevit	k5eNaPmAgMnS
<g/>
,	,	kIx,
lze	lze	k6eAd1
zde	zde	k6eAd1
ovšem	ovšem	k9
zahlédnou	zahlédnout	k5eAaPmIp3nP
noviny	novina	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
hlásají	hlásat	k5eAaImIp3nP
„	„	k?
<g/>
Harvey	Harvea	k1gFnSc2
Dent	Dent	k1gMnSc1
zvolen	zvolit	k5eAaPmNgMnS
novým	nový	k2eAgMnSc7d1
okresním	okresní	k2eAgMnSc7d1
prokurátorem	prokurátor	k1gMnSc7
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Arkham	Arkham	k1gInSc1
Knight	Knight	k1gInSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Two-Face	Two-Face	k1gFnSc1
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
opět	opět	k6eAd1
v	v	k7c6
titulu	titul	k1gInSc6
Arkham	Arkham	k1gInSc1
Knight	Knight	k2eAgInSc1d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
společně	společně	k6eAd1
se	s	k7c7
svým	svůj	k3xOyFgInSc7
gangem	gang	k1gInSc7
okrádá	okrádat	k5eAaImIp3nS
nechráněné	chráněný	k2eNgFnSc2d1
banky	banka	k1gFnSc2
kolem	kolem	k7c2
Gothamu	Gotham	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
Arkhamverse	Arkhamverse	k1gFnPc4
Two-Face	Two-Face	k1gFnSc2
namluvil	namluvit	k5eAaBmAgMnS,k5eAaPmAgMnS
herec	herec	k1gMnSc1
<g/>
,	,	kIx,
dabér	dabér	k1gMnSc1
a	a	k8xC
hudebník	hudebník	k1gMnSc1
Troy	Troa	k1gFnSc2
Baker	Baker	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Injustice	Injustika	k1gFnSc3
</s>
<s>
Injustice	Injustika	k1gFnSc3
<g/>
:	:	kIx,
Gods	Gods	k1gInSc1
Among	Among	k1gMnSc1
Us	Us	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Two-Face	Two-Face	k1gFnSc1
se	se	k3xPyFc4
objeví	objevit	k5eAaPmIp3nS
v	v	k7c6
úrovni	úroveň	k1gFnSc6
z	z	k7c2
Arkam	Arkam	k1gInSc4
Asylum	Asylum	k1gNnSc4
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
Penguinem	Penguin	k1gInSc7
<g/>
,	,	kIx,
Riddlerem	Riddler	k1gInSc7
a	a	k8xC
Killer	Killer	k1gInSc4
Crocem	Croec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
se	se	k3xPyFc4
objeví	objevit	k5eAaPmIp3nS
jako	jako	k9
součást	součást	k1gFnSc1
jedné	jeden	k4xCgFnSc2
z	z	k7c2
Catwomaniných	Catwomanin	k2eAgFnPc2d1
misí	mise	k1gFnPc2
ve	v	k7c6
STAR	star	k1gFnSc6
Labs	Labsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Injustice	Injustika	k1gFnSc6
2	#num#	k4
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c6
Injustice	Injustika	k1gFnSc6
2	#num#	k4
má	mít	k5eAaImIp3nS
Two-Face	Two-Face	k1gFnPc4
pouhé	pouhý	k2eAgFnPc4d1
cameo	cameo	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
zde	zde	k6eAd1
nachází	nacházet	k5eAaImIp3nS
zavřený	zavřený	k2eAgInSc1d1
v	v	k7c6
kleci	klec	k1gFnSc6
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
může	moct	k5eAaImIp3nS
vtáhnout	vtáhnout	k5eAaPmF
hráče	hráč	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
je	být	k5eAaImIp3nS
na	na	k7c4
něj	on	k3xPp3gMnSc4
odkazováno	odkazován	k2eAgNnSc1d1
v	v	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
úvodních	úvodní	k2eAgInPc2d1
dialogů	dialog	k1gInPc2
mezi	mezi	k7c7
Catwoman	Catwoman	k1gMnSc1
a	a	k8xC
Supergirl	Supergirl	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Telltale	Telltal	k1gMnSc5
Games	Games	k1gMnSc1
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc5
Telltale	Telltal	k1gMnSc5
Series	Series	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Harvey	Harvey	k1gInPc4
Dent	Dent	k1gInSc1
je	být	k5eAaImIp3nS
okresním	okresní	k2eAgMnSc7d1
prokurátorem	prokurátor	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
kandiduje	kandidovat	k5eAaImIp3nS
na	na	k7c4
starostu	starosta	k1gMnSc4
Gotham	Gotham	k1gInSc4
City	City	k1gFnSc2
proti	proti	k7c3
Hamiltonu	Hamilton	k1gInSc2
Hillovi	Hill	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
blízkým	blízký	k2eAgMnSc7d1
přítelem	přítel	k1gMnSc7
miliardáře	miliardář	k1gMnSc2
Bruce	Bruce	k1gMnSc1
Waynea	Waynea	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
finančně	finančně	k6eAd1
podporuje	podporovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc4
kampaň	kampaň	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
však	však	k9
vyjde	vyjít	k5eAaPmIp3nS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
Brucovi	Brucův	k2eAgMnPc1d1
rodiče	rodič	k1gMnPc1
kdysi	kdysi	k6eAd1
spolupracovali	spolupracovat	k5eAaImAgMnP
se	s	k7c7
zločineckou	zločinecký	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
bosse	boss	k1gMnSc2
Falconeho	Falcone	k1gMnSc2
<g/>
,	,	kIx,
rozhodne	rozhodnout	k5eAaPmIp3nS
se	se	k3xPyFc4
Dent	Dent	k2eAgMnSc1d1
od	od	k7c2
svého	svůj	k3xOyFgMnSc2
přítele	přítel	k1gMnSc2
distancovat	distancovat	k5eAaBmF
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
po	po	k7c6
něm	on	k3xPp3gMnSc6
stále	stále	k6eAd1
žádá	žádat	k5eAaImIp3nS
další	další	k2eAgInPc4d1
peníze	peníz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
později	pozdě	k6eAd2
účastní	účastnit	k5eAaImIp3nS
veřejné	veřejný	k2eAgFnPc4d1
debaty	debata	k1gFnPc4
s	s	k7c7
Hillem	Hillo	k1gNnSc7
<g/>
,	,	kIx,
stane	stanout	k5eAaPmIp3nS
se	se	k3xPyFc4
rukojmím	rukojmí	k1gNnSc7
teroristické	teroristický	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
<g/>
,	,	kIx,
známé	známý	k2eAgFnPc1d1
jako	jako	k8xC,k8xS
Děti	dítě	k1gFnPc1
Arkhamu	Arkham	k1gInSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
vede	vést	k5eAaImIp3nS
Penguin	Penguin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Batmanovi	Batmanův	k2eAgMnPc5d1
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc4
podaří	podařit	k5eAaPmIp3nS
zachránit	zachránit	k5eAaPmF
<g/>
,	,	kIx,
ovšem	ovšem	k9
Hill	Hill	k1gMnSc1
zemře	zemřít	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
z	z	k7c2
Harveyho	Harvey	k1gMnSc2
automaticky	automaticky	k6eAd1
stává	stávat	k5eAaImIp3nS
nový	nový	k2eAgMnSc1d1
starosta	starosta	k1gMnSc1
Gotham	Gotham	k1gInSc4
City	City	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
starostou	starosta	k1gMnSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
traumatizovaný	traumatizovaný	k2eAgMnSc1d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
rukojmím	rukojmí	k1gMnPc3
a	a	k8xC
začínají	začínat	k5eAaImIp3nP
se	se	k3xPyFc4
u	u	k7c2
něj	on	k3xPp3gMnSc2
projevovat	projevovat	k5eAaImF
známky	známka	k1gFnPc4
agresivní	agresivní	k2eAgFnPc4d1
a	a	k8xC
rozštěpené	rozštěpený	k2eAgFnPc4d1
osobnosti	osobnost	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Enemy	Enema	k1gFnSc2
Within	Within	k1gMnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c6
The	The	k1gFnSc6
Enemy	Enema	k1gFnSc2
Within	Within	k1gInSc4
lze	lze	k6eAd1
vidět	vidět	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
Batman	Batman	k1gMnSc1
v	v	k7c6
batcave	batcav	k1gInSc5
uchovává	uchovávat	k5eAaImIp3nS
artefakty	artefakt	k1gInPc4
spojené	spojený	k2eAgInPc4d1
s	s	k7c7
protivníky	protivník	k1gMnPc7
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yQgMnPc7,k3yIgMnPc7,k3yRgMnPc7
bojoval	bojovat	k5eAaImAgMnS
<g/>
,	,	kIx,
takže	takže	k8xS
je	být	k5eAaImIp3nS
tu	tu	k6eAd1
k	k	k7c3
vidění	vidění	k1gNnSc3
také	také	k9
Dentův	Dentův	k2eAgInSc4d1
plakát	plakát	k1gInSc4
z	z	k7c2
kampaně	kampaň	k1gFnSc2
na	na	k7c4
starostu	starosta	k1gMnSc4
a	a	k8xC
buď	buď	k8xC
jeho	jeho	k3xOp3gFnSc1
mince	mince	k1gFnSc1
nebo	nebo	k8xC
maska	maska	k1gFnSc1
(	(	kIx(
<g/>
pokud	pokud	k8xS
byl	být	k5eAaImAgInS
znetvořen	znetvořit	k5eAaPmNgInS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc5
Telltale	Telltal	k1gMnSc5
Series	Seriesa	k1gFnPc2
propůjčil	propůjčit	k5eAaPmAgInS
Harveymu	Harveym	k1gInSc3
Dentovi	Dentův	k2eAgMnPc1d1
hlas	hlas	k1gInSc4
herec	herec	k1gMnSc1
a	a	k8xC
dabér	dabér	k1gMnSc1
Travis	Travis	k1gFnSc2
Willingham	Willingham	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lego	lego	k1gNnSc1
videohry	videohra	k1gFnSc2
</s>
<s>
Lego	lego	k1gNnSc1
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Videogame	Videogam	k1gInSc5
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c6
první	první	k4xOgFnSc6
videohře	videohra	k1gFnSc6
ze	z	k7c2
série	série	k1gFnSc2
Lego	lego	k1gNnSc4
se	se	k3xPyFc4
Two-Face	Two-Face	k1gFnSc2
objevil	objevit	k5eAaPmAgInS
po	po	k7c6
boku	bok	k1gInSc6
Riddlera	Riddler	k1gMnSc2
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yQgInSc7,k3yIgInSc7,k3yRgInSc7
plánují	plánovat	k5eAaImIp3nP
ukrást	ukrást	k5eAaPmF
gothamské	gothamský	k2eAgFnPc4d1
zásoby	zásoba	k1gFnPc4
zlata	zlato	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gFnSc4
postavu	postava	k1gFnSc4
namluvil	namluvit	k5eAaBmAgInS,k5eAaPmAgInS
Steve	Steve	k1gMnSc1
Blum	bluma	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lego	lego	k1gNnSc1
Batman	Batman	k1gMnSc1
2	#num#	k4
<g/>
:	:	kIx,
DC	DC	kA
Super	super	k2eAgInSc1d1
Heroes	Heroes	k1gInSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c4
Lego	lego	k1gNnSc4
Batman	Batman	k1gMnSc1
2	#num#	k4
se	se	k3xPyFc4
Two-Face	Two-Face	k1gFnSc1
objeví	objevit	k5eAaPmIp3nS
jako	jako	k9
boss	boss	k1gMnSc1
se	s	k7c7
dvěma	dva	k4xCgFnPc7
pistolemi	pistol	k1gFnPc7
a	a	k8xC
náklaďákem	náklaďák	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
design	design	k1gInSc4
odpovídající	odpovídající	k2eAgInSc4d1
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
vzhledu	vzhled	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
Postavu	postav	k1gInSc2
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
namluvil	namluvit	k5eAaBmAgMnS,k5eAaPmAgMnS
Troy	Troa	k1gMnSc2
Baker	Baker	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lego	lego	k1gNnSc1
Batman	Batman	k1gMnSc1
3	#num#	k4
<g/>
:	:	kIx,
Beyond	Beyond	k1gInSc1
Gotham	Gotham	k1gInSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Ve	v	k7c6
třetím	třetí	k4xOgInSc6
pokračování	pokračování	k1gNnSc6
se	se	k3xPyFc4
Two-Face	Two-Face	k1gFnSc2
objevil	objevit	k5eAaPmAgInS
pouze	pouze	k6eAd1
v	v	k7c6
rámci	rámec	k1gInSc6
DLC	DLC	kA
The	The	k1gMnSc1
Dark	Dark	k1gMnSc1
Knight	Knight	k2eAgMnSc1d1
Trilogy	Triloga	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lego	lego	k1gNnSc1
Dimensions	Dimensionsa	k1gFnPc2
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Two-Face	Two-Face	k1gFnSc1
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgMnS
znovu	znovu	k6eAd1
v	v	k7c4
Lego	lego	k1gNnSc4
Dimensions	Dimensionsa	k1gFnPc2
a	a	k8xC
opět	opět	k6eAd1
s	s	k7c7
hlasem	hlas	k1gInSc7
Troye	Troy	k1gMnSc2
Bakera	Baker	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lego	lego	k1gNnSc1
DC	DC	kA
Super-Villains	Super-Villainsa	k1gFnPc2
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
V	v	k7c6
této	tento	k3xDgFnSc6
videohře	videohra	k1gFnSc6
si	se	k3xPyFc3
hráč	hráč	k1gMnSc1
může	moct	k5eAaImIp3nS
postavu	postava	k1gFnSc4
Two-Face	Two-Face	k1gFnSc2
odemknout	odemknout	k5eAaPmF
po	po	k7c6
dohrání	dohrání	k1gNnSc6
druhé	druhý	k4xOgFnSc2
mise	mise	k1gFnSc2
s	s	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
It	It	k1gFnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Good	Good	k1gMnSc1
To	to	k9
Be	Be	k1gMnSc1
Bad	Bad	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1
videohry	videohra	k1gFnPc1
</s>
<s>
Kromě	kromě	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
uvedených	uvedený	k2eAgFnPc2d1
videoher	videohra	k1gFnPc2
se	se	k3xPyFc4
postava	postava	k1gFnSc1
Harveyho	Harvey	k1gMnSc2
Denta	Dent	k1gMnSc2
/	/	kIx~
Two-Face	Two-Face	k1gFnSc1
objevila	objevit	k5eAaPmAgFnS
ještě	ještě	k9
v	v	k7c6
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Animated	Animated	k1gMnSc1
Series	Series	k1gMnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
The	The	k1gMnSc1
Adventures	Adventures	k1gMnSc1
of	of	k?
Batman	Batman	k1gMnSc1
&	&	k?
Robin	Robina	k1gFnPc2
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
124	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Batman	Batman	k1gMnSc1
Forever	Forever	k1gMnSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
125	#num#	k4
<g/>
]	]	kIx)
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Chaos	chaos	k1gInSc1
in	in	k?
Gotham	Gotham	k1gInSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
126	#num#	k4
<g/>
]	]	kIx)
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Brave	brav	k1gInSc5
and	and	k?
the	the	k?
Bold	Bold	k1gMnSc1
–	–	k?
The	The	k1gMnSc1
Videogame	Videogam	k1gInSc5
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
DC	DC	kA
Universe	Universe	k1gFnSc1
Online	Onlin	k1gInSc5
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
128	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
</s>
<s>
Two-Face	Two-Face	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comic	Comic	k1gMnSc1
Vine	vinout	k5eAaImIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Who	Who	k?
is	is	k?
Two-Face	Two-Face	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Batman	Batman	k1gMnSc1
Villain	Villain	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Comic	Comic	k1gMnSc1
Origins	Originsa	k1gFnPc2
Explained	Explained	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Screen	Screen	k2eAgInSc1d1
Rant	Rant	k1gInSc1
<g/>
,	,	kIx,
2019-10-10	2019-10-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Top	topit	k5eAaImRp2nS
100	#num#	k4
Comic	Comic	k1gMnSc1
Book	Book	k1gMnSc1
Villains	Villainsa	k1gFnPc2
of	of	k?
All	All	k1gMnSc2
Time	Tim	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IGN	IGN	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Real	Real	k1gInSc1
Life	Lif	k1gFnSc2
Inspirations	Inspirationsa	k1gFnPc2
Behind	Behind	k1gMnSc1
Some	Som	k1gFnSc2
of	of	k?
the	the	k?
Best	Best	k1gMnSc1
Comic	Comic	k1gMnSc1
Book	Book	k1gMnSc1
Villains	Villains	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Screen	Screen	k2eAgInSc1d1
Rant	Rant	k1gInSc1
<g/>
,	,	kIx,
2014-03-30	2014-03-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Two-Face	Two-Face	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Giant	Gianta	k1gFnPc2
Bomb	bomba	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
CHASE	chasa	k1gFnSc3
<g/>
,	,	kIx,
Bobbie	Bobbie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Two-Face	Two-Face	k1gFnSc1
<g/>
:	:	kIx,
A	a	k9
Celebration	Celebration	k1gInSc1
of	of	k?
75	#num#	k4
Years	Years	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
DC	DC	kA
Comics	comics	k1gInSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4012	#num#	k4
<g/>
-	-	kIx~
<g/>
7438	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
9	#num#	k4
<g/>
–	–	k?
<g/>
21	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Chase	chasa	k1gFnSc6
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
24	#num#	k4
<g/>
–	–	k?
<g/>
35.1	35.1	k4
2	#num#	k4
Chase	chasa	k1gFnSc6
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
38	#num#	k4
<g/>
–	–	k?
<g/>
49.1	49.1	k4
2	#num#	k4
</s>
<s>
Batman	Batman	k1gMnSc1
#	#	kIx~
<g/>
50	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comic	Comic	k1gMnSc1
Vine	vinout	k5eAaImIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Chase	chasa	k1gFnSc6
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
51	#num#	k4
<g/>
–	–	k?
<g/>
62	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Chase	chasa	k1gFnSc6
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
64	#num#	k4
<g/>
–	–	k?
<g/>
73	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Chase	chasa	k1gFnSc6
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
93	#num#	k4
<g/>
–	–	k?
<g/>
116	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Chase	chasa	k1gFnSc6
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
76	#num#	k4
<g/>
–	–	k?
<g/>
91	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
</s>
<s>
WEIN	WEIN	kA
<g/>
,	,	kIx,
Len	len	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tales	Tales	k1gMnSc1
of	of	k?
the	the	k?
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Len	len	k1gInSc1
Wein	Wein	k1gNnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
DC	DC	kA
Comics	comics	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4012	#num#	k4
<g/>
-	-	kIx~
<g/>
5154	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
267	#num#	k4
<g/>
–	–	k?
<g/>
319	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Chase	chasa	k1gFnSc6
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
203	#num#	k4
<g/>
–	–	k?
<g/>
257	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
</s>
<s>
MILLER	Miller	k1gMnSc1
<g/>
,	,	kIx,
Frank	Frank	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Rok	rok	k1gInSc1
jedna	jeden	k4xCgFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Crew	Crew	k1gMnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87083	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
</s>
<s>
LOEB	LOEB	kA
<g/>
,	,	kIx,
Jeph	Jeph	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Dlouhý	dlouhý	k2eAgInSc1d1
Halloween	Halloween	k1gInSc1
–	–	k?
Kniha	kniha	k1gFnSc1
druhá	druhý	k4xOgFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Eaglemoss	Eaglemoss	k1gInSc1
Collections	Collectionsa	k1gFnPc2
<g/>
,	,	kIx,
BB	BB	kA
art	art	k?
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
83	#num#	k4
<g/>
-	-	kIx~
<g/>
7718	#num#	k4
<g/>
-	-	kIx~
<g/>
207	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
LOEB	LOEB	kA
<g/>
,	,	kIx,
Jeph	Jeph	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Temné	temný	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
–	–	k?
Kniha	kniha	k1gFnSc1
druhá	druhý	k4xOgFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
BB	BB	kA
art	art	k?
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7381	#num#	k4
<g/>
-	-	kIx~
<g/>
942	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Chase	chasa	k1gFnSc6
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
137	#num#	k4
<g/>
–	–	k?
<g/>
181	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
#	#	kIx~
<g/>
442	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comic	Comic	k1gMnSc1
Vine	vinout	k5eAaImIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Chase	chasa	k1gFnSc6
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
267	#num#	k4
<g/>
–	–	k?
<g/>
312	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
LOEB	LOEB	kA
<g/>
,	,	kIx,
Jeph	Jeph	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Ticho	ticho	k1gNnSc1
–	–	k?
Kniha	kniha	k1gFnSc1
první	první	k4xOgNnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Eaglemoss	Eaglemoss	k1gInSc1
Collections	Collectionsa	k1gFnPc2
<g/>
,	,	kIx,
BB	BB	kA
art	art	k?
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
83	#num#	k4
<g/>
-	-	kIx~
<g/>
7718	#num#	k4
<g/>
-	-	kIx~
<g/>
680	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
LOEB	LOEB	kA
<g/>
,	,	kIx,
Jeph	Jeph	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Ticho	ticho	k1gNnSc1
–	–	k?
Kniha	kniha	k1gFnSc1
druhá	druhý	k4xOgFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Eaglemoss	Eaglemoss	k1gInSc1
Collections	Collectionsa	k1gFnPc2
<g/>
,	,	kIx,
BB	BB	kA
art	art	k?
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
83	#num#	k4
<g/>
-	-	kIx~
<g/>
7718	#num#	k4
<g/>
-	-	kIx~
<g/>
681	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
DC	DC	kA
Rebirth	Rebirth	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ComixZone	ComixZon	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2016-05-27	2016-05-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
</s>
<s>
SNYDER	SNYDER	kA
<g/>
,	,	kIx,
Scott	Scott	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
All-Star	All-Star	k1gMnSc1
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Můj	můj	k3xOp1gMnSc1
nejhorší	zlý	k2eAgMnSc1d3
nepřítel	nepřítel	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Crew	Crew	k1gMnSc1
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7449	#num#	k4
<g/>
-	-	kIx~
<g/>
615	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
</s>
<s>
TOMASI	TOMASI	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
J.	J.	kA
Batman	Batman	k1gMnSc1
and	and	k?
Robin	robin	k2eAgMnSc1d1
<g/>
,	,	kIx,
Vol	vol	k6eAd1
<g/>
.	.	kIx.
5	#num#	k4
<g/>
:	:	kIx,
The	The	k1gMnSc1
Big	Big	k1gMnSc1
Burn	Burn	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
DC	DC	kA
Comics	comics	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1401250591	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
KING	KING	kA
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Válka	válka	k1gFnSc1
vtipů	vtip	k1gInPc2
a	a	k8xC
hádanek	hádanka	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Crew	Crew	k1gMnSc1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7449	#num#	k4
<g/>
-	-	kIx~
<g/>
751	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
TYNION	TYNION	kA
IV	IV	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Batman	Batman	k1gMnSc1
—	—	k?
Detective	Detectiv	k1gInSc5
Comics	comics	k1gInSc1
<g/>
:	:	kIx,
Život	život	k1gInSc1
v	v	k7c6
osamění	osamění	k1gNnSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
BB	BB	kA
art	art	k?
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7595	#num#	k4
<g/>
-	-	kIx~
<g/>
308	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
FINCH	FINCH	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Batman	Batman	k1gMnSc1
—	—	k?
Temný	temný	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
<g/>
:	:	kIx,
Temné	temný	k2eAgInPc4d1
děsy	děs	k1gInPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
BB	BB	kA
art	art	k?
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7461	#num#	k4
<g/>
-	-	kIx~
<g/>
355	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WAID	WAID	kA
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Království	království	k1gNnSc1
tvé	tvůj	k3xOp2gFnPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
BB	BB	kA
art	art	k?
<g/>
,	,	kIx,
Crew	Crew	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7341	#num#	k4
<g/>
-	-	kIx~
<g/>
924	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
LOEB	LOEB	kA
<g/>
,	,	kIx,
Jeph	Jeph	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Dlouhý	dlouhý	k2eAgInSc1d1
Halloween	Halloween	k1gInSc1
–	–	k?
Kniha	kniha	k1gFnSc1
první	první	k4xOgNnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Eaglemoss	Eaglemoss	k1gInSc1
Collections	Collectionsa	k1gFnPc2
<g/>
,	,	kIx,
BB	BB	kA
art	art	k?
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
83	#num#	k4
<g/>
-	-	kIx~
<g/>
7718	#num#	k4
<g/>
-	-	kIx~
<g/>
202	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Two-Face	Two-Face	k1gFnSc1
-	-	kIx~
Crime	Crim	k1gInSc5
and	and	k?
Punishment	Punishment	k1gInSc1
#	#	kIx~
<g/>
1	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comic	Comic	k1gMnSc1
Vine	vinout	k5eAaImIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Murray	Murray	k1gInPc1
Dent	Dent	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comic	Comic	k1gMnSc1
Vine	vinout	k5eAaImIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Albert	Albert	k1gMnSc1
Ekhart	Ekharta	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comic	Comic	k1gMnSc1
Vine	vinout	k5eAaImIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Carol	Carol	k1gInSc1
Bermingham	Bermingham	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comic	Comic	k1gMnSc1
Vine	vinout	k5eAaImIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
District	District	k1gInSc1
Attorney	Attornea	k1gFnSc2
Janice	Janice	k1gFnSc2
Porter	porter	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comic	Comic	k1gMnSc1
Vine	vinout	k5eAaImIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Charlatan	Charlatan	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comic	Comic	k1gMnSc1
Vine	vinout	k5eAaImIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
World	World	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Finest	Finest	k1gInSc1
Comics	comics	k1gInSc1
#	#	kIx~
<g/>
173	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comic	Comic	k1gMnSc1
Vine	vinout	k5eAaImIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pictures	Picturesa	k1gFnPc2
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
navždy	navždy	k6eAd1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pictures	Picturesa	k1gFnPc2
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Temný	temný	k2eAgMnSc1d1
rytíř	rytíř	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pictures	Picturesa	k1gFnPc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
Beyond	Beyond	k1gMnSc1
-	-	kIx~
Return	Return	k1gMnSc1
of	of	k?
the	the	k?
Joker	Joker	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Movie-Censorship	Movie-Censorship	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Rok	rok	k1gInSc1
jedna	jeden	k4xCgFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Home	Home	k1gFnPc2
Video	video	k1gNnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Návrat	návrat	k1gInSc1
Temného	temný	k2eAgMnSc2d1
rytíře	rytíř	k1gMnSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
1	#num#	k4
<g/>
..	..	k?
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Home	Home	k1gFnPc2
Video	video	k1gNnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Lego	lego	k1gNnSc1
<g/>
:	:	kIx,
Batman	Batman	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pictures	Pictures	k1gInSc1
<g/>
,	,	kIx,
Warner	Warner	k1gInSc1
Home	Home	k1gFnPc2
Video	video	k1gNnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batmanův	Batmanův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Home	Home	k1gFnPc2
Video	video	k1gNnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Útok	útok	k1gInSc1
na	na	k7c4
Arkham	Arkham	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Home	Home	k1gFnPc2
Video	video	k1gNnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joker	Joker	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Home	Home	k1gFnPc2
Video	video	k1gNnSc1
<g/>
,	,	kIx,
Fathom	Fathom	k1gInSc1
Events	Eventsa	k1gFnPc2
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
Unlimited	Unlimited	k1gMnSc1
<g/>
:	:	kIx,
Mech	mech	k1gInSc1
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mutants	Mutants	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Home	Home	k1gFnPc2
Video	video	k1gNnSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LEGO	lego	k1gNnSc1
Batman	Batman	k1gMnSc1
film	film	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pictures	Picturesa	k1gFnPc2
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Two-Face	Two-Face	k1gFnSc1
(	(	kIx(
<g/>
2017	#num#	k4
Video	video	k1gNnSc1
<g/>
)	)	kIx)
Trivia	trivium	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IMDb	IMDb	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Two-Face	Two-Face	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Home	Hom	k1gInSc2
Entertainment	Entertainment	k1gInSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
</s>
<s>
Clint	Clint	k1gInSc1
Eastwood	Eastwooda	k1gFnPc2
Biography	Biographa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
TVguide	TVguid	k1gInSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Gotham	Gotham	k1gInSc1
by	by	kYmCp3nS
Gaslight	Gaslight	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Home	Hom	k1gInSc2
Entertainment	Entertainment	k1gInSc1
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Suicide	Suicid	k1gMnSc5
Squad	Squad	k1gInSc1
<g/>
:	:	kIx,
Hell	Hell	k1gMnSc1
to	ten	k3xDgNnSc4
Pay	Pay	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Home	Hom	k1gInSc2
Entertainment	Entertainment	k1gInSc1
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Batman	Batman	k1gMnSc1
Ninja	Ninja	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pictures	Pictures	k1gMnSc1
<g/>
,	,	kIx,
Warner	Warner	k1gMnSc1
Bros	Bros	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Home	Hom	k1gInSc2
Entertainment	Entertainment	k1gInSc1
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
Ninja	Ninja	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IMDb	IMDb	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Justice	justice	k1gFnSc1
League	Leagu	k1gFnSc2
vs	vs	k?
<g/>
.	.	kIx.
the	the	k?
Fatal	Fatal	k1gInSc1
Five	Five	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Home	Hom	k1gInSc2
Entertainment	Entertainment	k1gInSc1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teenage	Teenag	k1gInSc2
Mutant	mutant	k1gMnSc1
Ninja	Ninja	k1gMnSc1
Turtles	Turtles	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Home	Hom	k1gInSc2
Entertainment	Entertainment	k1gInSc1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Lego	lego	k1gNnSc1
DC	DC	kA
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Family	Famil	k1gInPc1
Matters	Mattersa	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Home	Hom	k1gInSc2
Entertainment	Entertainment	k1gInSc1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Gotham	Gotham	k1gInSc1
–	–	k?
S01E09	S01E09	k1gFnSc2
–	–	k?
„	„	k?
<g/>
Harvey	Harvea	k1gFnSc2
Dent	Dent	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Gotham	Gotham	k1gInSc1
–	–	k?
S01E10	S01E10	k1gFnSc2
–	–	k?
„	„	k?
<g/>
Lovecraft	Lovecraft	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Gotham	Gotham	k1gInSc1
–	–	k?
S01E18	S01E18	k1gFnSc2
–	–	k?
„	„	k?
<g/>
Everyone	Everyon	k1gInSc5
Has	hasit	k5eAaImRp2nS
a	a	k8xC
Cobblepot	Cobblepot	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Gotham	Gotham	k1gInSc1
–	–	k?
S02E07	S02E07	k1gFnSc2
–	–	k?
„	„	k?
<g/>
Rise	Rise	k1gInSc1
of	of	k?
the	the	k?
Villains	Villains	k1gInSc1
<g/>
:	:	kIx,
Mommy	Momma	k1gFnPc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Little	Little	k1gFnSc1
Monster	monstrum	k1gNnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Gotham	Gotham	k1gInSc1
–	–	k?
S02E10	S02E10	k1gFnSc2
–	–	k?
„	„	k?
<g/>
Rise	Rise	k1gInSc1
of	of	k?
the	the	k?
Villains	Villains	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Son	son	k1gInSc1
of	of	k?
Gotham	Gotham	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Gotham	Gotham	k1gInSc1
–	–	k?
S02E12	S02E12	k1gFnSc2
–	–	k?
„	„	k?
<g/>
Wrath	Wrath	k1gInSc1
of	of	k?
the	the	k?
Villains	Villains	k1gInSc1
<g/>
:	:	kIx,
Mr	Mr	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Freeze	Freeze	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Gotham	Gotham	k1gInSc1
–	–	k?
S02E16	S02E16	k1gFnSc2
–	–	k?
„	„	k?
<g/>
Wrath	Wrath	k1gInSc1
of	of	k?
the	the	k?
Villains	Villains	k1gInSc1
<g/>
:	:	kIx,
Prisoners	Prisoners	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Titans	Titans	k1gInSc1
–	–	k?
S01E11	S01E11	k1gFnSc2
–	–	k?
„	„	k?
<g/>
Dick	Dick	k1gMnSc1
Grayson	Grayson	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
–	–	k?
S01E02	S01E02	k1gMnSc1
–	–	k?
„	„	k?
<g/>
On	on	k3xPp3gMnSc1
Leather	Leathra	k1gFnPc2
Wings	Wings	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
–	–	k?
S01E09	S01E09	k1gMnSc1
–	–	k?
„	„	k?
<g/>
Pretty	Pretta	k1gFnSc2
Poison	Poison	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
–	–	k?
S01E17	S01E17	k1gMnSc1
–	–	k?
„	„	k?
<g/>
Two-Face	Two-Face	k1gFnSc1
<g/>
:	:	kIx,
Part	part	k1gInSc1
I	i	k8xC
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
–	–	k?
S01E18	S01E18	k1gMnSc1
–	–	k?
„	„	k?
<g/>
Two-Face	Two-Face	k1gFnSc1
<g/>
:	:	kIx,
Part	part	k1gInSc1
II	II	kA
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
–	–	k?
S02E01	S02E01	k1gMnSc1
–	–	k?
„	„	k?
<g/>
Shadow	Shadow	k1gFnSc2
of	of	k?
the	the	k?
Bat	Bat	k1gFnSc1
<g/>
:	:	kIx,
Part	part	k1gInSc1
I	i	k8xC
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
–	–	k?
S02E02	S02E02	k1gMnSc1
–	–	k?
„	„	k?
<g/>
Shadow	Shadow	k1gFnSc2
of	of	k?
the	the	k?
Bat	Bat	k1gFnSc1
<g/>
:	:	kIx,
Part	part	k1gInSc1
II	II	kA
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
–	–	k?
S02E09	S02E09	k1gMnSc1
–	–	k?
„	„	k?
<g/>
Trial	trial	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
–	–	k?
S03E02	S03E02	k1gMnSc1
–	–	k?
„	„	k?
<g/>
Second	Second	k1gInSc1
Change	change	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
(	(	kIx(
<g/>
The	The	k1gMnSc1
New	New	k1gMnSc1
Batman	Batman	k1gMnSc1
Adventures	Adventures	k1gMnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
SerialZone	SerialZon	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
–	–	k?
S01E02	S01E02	k1gMnSc1
–	–	k?
„	„	k?
<g/>
Sins	Sins	k1gInSc1
of	of	k?
the	the	k?
Father	Fathra	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
–	–	k?
S02E09	S02E09	k1gMnSc1
–	–	k?
„	„	k?
<g/>
Judgement	Judgement	k1gMnSc1
Day	Day	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Recap	Recap	k1gMnSc1
/	/	kIx~
Batman	Batman	k1gMnSc1
Beyond	Beyond	k1gMnSc1
S2	S2	k1gMnSc1
E13	E13	k1gMnSc1
"	"	kIx"
<g/>
Terry	Terra	k1gFnPc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Friend	Friend	k1gMnSc1
Dates	Dates	k1gMnSc1
a	a	k8xC
Robot	robot	k1gMnSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
TVtropes	TVtropes	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Liga	liga	k1gFnSc1
spravedlivých	spravedlivý	k2eAgFnPc2d1
–	–	k?
S02E12	S02E12	k1gFnPc2
–	–	k?
„	„	k?
<g/>
A	a	k9
Better	Better	k1gMnSc1
World	World	k1gMnSc1
<g/>
:	:	kIx,
Part	part	k1gInSc1
II	II	kA
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Odvážný	odvážný	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
–	–	k?
S01E19	S01E19	k1gMnSc1
–	–	k?
„	„	k?
<g/>
Legendy	legenda	k1gFnSc2
temného	temný	k2eAgInSc2d1
caparta	caparta	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Odvážný	odvážný	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
–	–	k?
S01E25	S01E25	k1gMnSc1
–	–	k?
„	„	k?
<g/>
Zmatek	zmatek	k1gInSc1
mezi	mezi	k7c7
hudebními	hudební	k2eAgMnPc7d1
mistry	mistr	k1gMnPc7
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Odvážný	odvážný	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
–	–	k?
S01E26	S01E26	k1gMnSc1
–	–	k?
„	„	k?
<g/>
Equinoxův	Equinoxův	k2eAgInSc1d1
osud	osud	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Odvážný	odvážný	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
–	–	k?
S02E06	S02E06	k1gMnSc1
–	–	k?
„	„	k?
<g/>
Sraz	sraz	k1gInSc1
pobočníků	pobočník	k1gMnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Odvážný	odvážný	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
–	–	k?
S02E11	S02E11	k1gMnSc1
–	–	k?
„	„	k?
<g/>
Chlad	chlad	k1gInSc1
noci	noc	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Odvážný	odvážný	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
–	–	k?
S02E24	S02E24	k1gMnSc1
–	–	k?
„	„	k?
<g/>
Maska	maska	k1gFnSc1
Matchese	Matchese	k1gFnSc1
Malona	Malona	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Odvážný	odvážný	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
–	–	k?
S03E11	S03E11	k1gMnSc1
–	–	k?
„	„	k?
<g/>
Crisis	Crisis	k1gInSc1
<g/>
:	:	kIx,
22,300	22,300	k4
Miles	Miles	k1gInSc1
Above	Aboev	k1gFnSc2
Earth	Eartha	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Young	Young	k1gMnSc1
Justice	justice	k1gFnSc2
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
–	–	k?
)	)	kIx)
Image	image	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IMDb	IMDb	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Characters	Characters	k1gInSc4
/	/	kIx~
Beware	Bewar	k1gMnSc5
the	the	k?
Batman	Batman	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
TVtropes	TVtropes	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Beware	Bewar	k1gMnSc5
the	the	k?
Batman	Batman	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Behindthevoiceactors	Behindthevoiceactors	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Teen	Teen	k1gMnSc1
Titans	Titansa	k1gFnPc2
Go	Go	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
#	#	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comic	Comic	k1gMnSc1
Vine	vinout	k5eAaImIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Teen	Teen	k1gMnSc1
Titans	Titansa	k1gFnPc2
Go	Go	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
#	#	kIx~
<g/>
132	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comic	Comic	k1gMnSc1
Vine	vinout	k5eAaImIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Teen	Teen	k1gMnSc1
Titans	Titansa	k1gFnPc2
Go	Go	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
#	#	kIx~
<g/>
133	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comic	Comic	k1gMnSc1
Vine	vinout	k5eAaImIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Teen	Teen	k1gMnSc1
Titans	Titansa	k1gFnPc2
Go	Go	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
#	#	kIx~
<g/>
345	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comic	Comic	k1gMnSc1
Vine	vinout	k5eAaImIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Teen	Teen	k1gMnSc1
Titans	Titansa	k1gFnPc2
Go	Go	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
#	#	kIx~
<g/>
601	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comic	Comic	k1gMnSc1
Vine	vinout	k5eAaImIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Teen	Teen	k1gMnSc1
Titans	Titansa	k1gFnPc2
Go	Go	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
#	#	kIx~
<g/>
611	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comic	Comic	k1gMnSc1
Vine	vinout	k5eAaImIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Teen	Teen	k1gMnSc1
Titans	Titansa	k1gFnPc2
Go	Go	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
#	#	kIx~
<g/>
617	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comic	Comic	k1gMnSc1
Vine	vinout	k5eAaImIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Justice	justice	k1gFnSc1
League	Leagu	k1gFnSc2
Action	Action	k1gInSc1
–	–	k?
S01E22	S01E22	k1gFnSc2
–	–	k?
„	„	k?
<g/>
Double	double	k2eAgInSc1d1
Cross	Cross	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Justice	justice	k1gFnPc1
League	Leagu	k1gInSc2
Action	Action	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Behindthevoiceactors	Behindthevoiceactors	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Harley	harley	k1gInSc1
Quinn	Quinn	k1gMnSc1
–	–	k?
S01E02	S01E02	k1gMnSc1
–	–	k?
„	„	k?
<g/>
A	a	k9
High	High	k1gMnSc1
Bar	bar	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Harley	harley	k1gInSc1
Quinn	Quinn	k1gMnSc1
–	–	k?
S02E01	S02E01	k1gMnSc1
–	–	k?
„	„	k?
<g/>
New	New	k1gFnSc1
Gotham	Gotham	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Harley	harley	k1gInSc1
Quinn	Quinn	k1gMnSc1
–	–	k?
S02E05	S02E05	k1gMnSc1
–	–	k?
„	„	k?
<g/>
Batman	Batman	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Back	Back	k1gMnSc1
Man	Man	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Harley	harley	k1gInSc1
Quinn	Quinn	k1gMnSc1
–	–	k?
S02E07	S02E07	k1gMnSc1
–	–	k?
„	„	k?
<g/>
There	Ther	k1gInSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
No	no	k9
Place	plac	k1gInSc6
To	to	k9
Go	Go	k1gMnSc1
But	But	k1gMnSc1
Down	Down	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Television	Television	k1gInSc1
Distribution	Distribution	k1gInSc4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Harley	harley	k1gInSc1
Quinn	Quinn	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Behindthevoiceactors	Behindthevoiceactors	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Arkham	Arkham	k1gInSc1
Asylum	Asylum	k1gNnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Eidos	Eidos	k1gInSc1
Interactive	Interactiv	k1gInSc5
<g/>
,	,	kIx,
Warner	Warner	k1gInSc4
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interactive	Interactiv	k1gInSc5
Entertainment	Entertainment	k1gMnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Arkham	Arkham	k1gInSc1
City	city	k1gNnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interactive	Interactiv	k1gInSc5
Entertainment	Entertainment	k1gMnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Arkham	Arkham	k1gInSc1
Origins	Origins	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interactive	Interactiv	k1gInSc5
Entertainment	Entertainment	k1gMnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Arkham	Arkham	k1gInSc1
Knight	Knight	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interactive	Interactiv	k1gInSc5
Entertainment	Entertainment	k1gMnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Troy	Troa	k1gMnSc2
Baker	Baker	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Behindthevoiceactors	Behindthevoiceactors	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Injustice	Injustika	k1gFnSc3
<g/>
:	:	kIx,
Gods	Gods	k1gInSc1
Among	Among	k1gMnSc1
Us	Us	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interactive	Interactiv	k1gInSc5
Entertainment	Entertainment	k1gMnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Injustice	Injustika	k1gFnSc6
2	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interactive	Interactiv	k1gInSc5
Entertainment	Entertainment	k1gMnSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc5
Telltale	Telltal	k1gMnSc5
Series	Series	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Telltale	Telltal	k1gMnSc5
Games	Games	k1gMnSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Enemy	Enema	k1gFnSc2
Within	Within	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Telltale	Telltal	k1gMnSc5
Games	Games	k1gMnSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Travis	Travis	k1gInSc1
Willingham	Willingham	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Behindthevoiceactors	Behindthevoiceactors	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Lego	lego	k1gNnSc1
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Videogame	Videogam	k1gInSc5
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interactive	Interactiv	k1gInSc5
Entertainment	Entertainment	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Lego	lego	k1gNnSc1
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Videogame	Videogam	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Behindthevoiceactors	Behindthevoiceactors	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Lego	lego	k1gNnSc1
Batman	Batman	k1gMnSc1
2	#num#	k4
<g/>
:	:	kIx,
DC	DC	kA
Super	super	k2eAgInSc1d1
Heroes	Heroes	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Warner	Warner	k1gInSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interactive	Interactiv	k1gInSc5
Entertainment	Entertainment	k1gMnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Lego	lego	k1gNnSc1
Batman	Batman	k1gMnSc1
2	#num#	k4
<g/>
:	:	kIx,
DC	DC	kA
Super	super	k2eAgInSc1d1
Heroes	Heroes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Behindthevoiceactors	Behindthevoiceactors	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
LEGO	lego	k1gNnSc1
Batman	Batman	k1gMnSc1
3	#num#	k4
<g/>
:	:	kIx,
Beyond	Beyond	k1gInSc1
Gotham	Gotham	k1gInSc1
<g/>
/	/	kIx~
<g/>
DLC	DLC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strategywiki	Strategywiki	k1gNnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Lego	lego	k1gNnSc1
Dimensions	Dimensionsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Behindthevoiceactors	Behindthevoiceactors	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
LEGO	lego	k1gNnSc1
DC	DC	kA
Super	super	k2eAgInSc1d1
Villains	Villains	k1gInSc1
Character	Character	k1gMnSc1
Unlock	Unlock	k1gMnSc1
Guide	Guid	k1gInSc5
–	–	k?
All	All	k1gFnSc7
162	#num#	k4
Characters	Charactersa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brickstolife	Brickstolif	k1gInSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Animated	Animated	k1gMnSc1
Series	Series	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Giant	Gianta	k1gFnPc2
Bomb	bomba	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
The	The	k?
Adventures	Adventures	k1gMnSc1
of	of	k?
Batman	Batman	k1gMnSc1
&	&	k?
Robin	Robina	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Giant	Gianta	k1gFnPc2
Bomb	bomba	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
Forever	Forever	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Giant	Gianta	k1gFnPc2
Bomb	bomba	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
Chaos	chaos	k1gInSc1
in	in	k?
Gotham	Gotham	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Giant	Gianta	k1gFnPc2
Bomb	bomba	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Batman	Batman	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Brave	brav	k1gInSc5
and	and	k?
the	the	k?
Bold	Bold	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Videogame	Videogam	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Giant	Gianta	k1gFnPc2
Bomb	bomba	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
DC	DC	kA
Universe	Universe	k1gFnSc1
Online	Onlin	k1gInSc5
-	-	kIx~
Characters	Characters	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Giant	Gianta	k1gFnPc2
Bomb	bomba	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Two-Face	Two-Face	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Two-Face	Two-Face	k1gFnSc1
na	na	k7c4
Czech	Czech	k1gInSc4
DC	DC	kA
Comics	comics	k1gInSc1
Fandom	Fandom	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Two-Face	Two-Face	k1gFnSc1
na	na	k7c4
Postavy	postav	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Two-Face	Two-Face	k1gFnSc1
na	na	k7c6
oficiálních	oficiální	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
DC	DC	kA
Comics	comics	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Two-Face	Two-Face	k1gFnSc1
na	na	k7c6
DC	DC	kA
Comics	comics	k1gInSc1
Fandom	Fandom	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Two-Face	Two-Face	k1gFnSc1
na	na	k7c6
Batman	Batman	k1gMnSc1
Fandom	Fandom	k1gInSc4
</s>
