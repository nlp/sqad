<p>
<s>
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1830	[number]	k4	1830
zámek	zámek	k1gInSc1	zámek
Schönbrunn	Schönbrunn	k1gInSc4	Schönbrunn
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc4	Vídeň
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1916	[number]	k4	1916
zámek	zámek	k1gInSc1	zámek
Schönbrunn	Schönbrunn	k1gInSc4	Schönbrunn
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Habsbursko-lotrinského	habsburskootrinský	k2eAgInSc2d1	habsbursko-lotrinský
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
68	[number]	k4	68
let	léto	k1gNnPc2	léto
císař	císař	k1gMnSc1	císař
rakouský	rakouský	k2eAgMnSc1d1	rakouský
(	(	kIx(	(
<g/>
nekorunovaný	korunovaný	k2eNgInSc1d1	nekorunovaný
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1848	[number]	k4	1848
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
český	český	k2eAgMnSc1d1	český
(	(	kIx(	(
<g/>
nekorunovaný	korunovaný	k2eNgMnSc1d1	nekorunovaný
<g/>
)	)	kIx)	)
a	a	k8xC	a
uherský	uherský	k2eAgInSc1d1	uherský
(	(	kIx(	(
<g/>
korunovace	korunovace	k1gFnSc1	korunovace
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
lombardský	lombardský	k2eAgMnSc1d1	lombardský
a	a	k8xC	a
benátský	benátský	k2eAgMnSc1d1	benátský
<g/>
,	,	kIx,	,
dalmátský	dalmátský	k2eAgMnSc1d1	dalmátský
<g/>
,	,	kIx,	,
chorvatský	chorvatský	k2eAgMnSc1d1	chorvatský
<g/>
,	,	kIx,	,
slavonský	slavonský	k2eAgMnSc1d1	slavonský
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
arcivévodkyní	arcivévodkyně	k1gFnPc2	arcivévodkyně
Žofií	Žofie	k1gFnPc2	Žofie
byl	být	k5eAaImAgInS	být
během	během	k7c2	během
dospívání	dospívání	k1gNnSc2	dospívání
veden	vést	k5eAaImNgMnS	vést
ke	k	k7c3	k
zbožnosti	zbožnost	k1gFnSc3	zbožnost
a	a	k8xC	a
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
o	o	k7c4	o
nedotknutelnosti	nedotknutelnost	k1gFnPc4	nedotknutelnost
panovnické	panovnický	k2eAgFnSc2d1	panovnická
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1848	[number]	k4	1848
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
revoluce	revoluce	k1gFnSc2	revoluce
vládl	vládnout	k5eAaImAgInS	vládnout
zpočátku	zpočátku	k6eAd1	zpočátku
absolutisticky	absolutisticky	k6eAd1	absolutisticky
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sestřenicí	sestřenice	k1gFnSc7	sestřenice
Alžbětou	Alžběta	k1gFnSc7	Alžběta
Bavorskou	bavorský	k2eAgFnSc7d1	bavorská
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
prohrál	prohrát	k5eAaPmAgMnS	prohrát
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
,	,	kIx,	,
přecházel	přecházet	k5eAaImAgInS	přecházet
postupně	postupně	k6eAd1	postupně
ke	k	k7c3	k
konstituční	konstituční	k2eAgFnSc3d1	konstituční
formě	forma	k1gFnSc3	forma
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Neúspěchem	neúspěch	k1gInSc7	neúspěch
skončila	skončit	k5eAaPmAgFnS	skončit
i	i	k9	i
Františkova	Františkův	k2eAgFnSc1d1	Františkova
válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Pruskem	Prusko	k1gNnSc7	Prusko
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
následovalo	následovat	k5eAaImAgNnS	následovat
vyrovnání	vyrovnání	k1gNnSc4	vyrovnání
s	s	k7c7	s
Uherskem	Uhersko	k1gNnSc7	Uhersko
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
vlna	vlna	k1gFnSc1	vlna
liberalizace	liberalizace	k1gFnSc2	liberalizace
<g/>
.	.	kIx.	.
</s>
<s>
Kabinety	kabinet	k1gInPc1	kabinet
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
císař	císař	k1gMnSc1	císař
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
až	až	k6eAd1	až
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
prováděly	provádět	k5eAaImAgFnP	provádět
reformy	reforma	k1gFnPc1	reforma
např.	např.	kA	např.
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
volebního	volební	k2eAgInSc2d1	volební
zákona	zákon	k1gInSc2	zákon
a	a	k8xC	a
snažily	snažit	k5eAaImAgFnP	snažit
se	se	k3xPyFc4	se
udržet	udržet	k5eAaPmF	udržet
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
národnostní	národnostní	k2eAgFnSc2d1	národnostní
spory	spora	k1gFnSc2	spora
v	v	k7c6	v
monarchii	monarchie	k1gFnSc6	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přibývajícími	přibývající	k2eAgInPc7d1	přibývající
roky	rok	k1gInPc7	rok
se	se	k3xPyFc4	se
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
říši	říš	k1gFnSc6	říš
stal	stát	k5eAaPmAgInS	stát
všeobecně	všeobecně	k6eAd1	všeobecně
uctívanou	uctívaný	k2eAgFnSc7d1	uctívaná
osobností	osobnost	k1gFnSc7	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
císař	císař	k1gMnSc1	císař
stal	stát	k5eAaPmAgMnS	stát
jakousi	jakýsi	k3yIgFnSc7	jakýsi
legendou	legenda	k1gFnSc7	legenda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
svou	svůj	k3xOyFgFnSc7	svůj
prací	práce	k1gFnSc7	práce
udržovala	udržovat	k5eAaImAgFnS	udržovat
mnohonárodnostní	mnohonárodnostní	k2eAgFnSc4d1	mnohonárodnostní
říši	říše	k1gFnSc4	říše
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
.	.	kIx.	.
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
expanze	expanze	k1gFnSc1	expanze
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
ale	ale	k8xC	ale
vyústila	vyústit	k5eAaPmAgFnS	vyústit
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
v	v	k7c4	v
první	první	k4xOgFnSc4	první
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
jejíhož	jejíž	k3xOyRp3gInSc2	jejíž
konce	konec	k1gInSc2	konec
se	se	k3xPyFc4	se
císař	císař	k1gMnSc1	císař
již	již	k6eAd1	již
nedožil	dožít	k5eNaPmAgMnS	dožít
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
v	v	k7c6	v
Schönbrunnu	Schönbrunn	k1gInSc6	Schönbrunn
<g/>
,	,	kIx,	,
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
ho	on	k3xPp3gMnSc4	on
přežilo	přežít	k5eAaPmAgNnS	přežít
o	o	k7c4	o
pouhé	pouhý	k2eAgInPc4d1	pouhý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
životě	život	k1gInSc6	život
František	František	k1gMnSc1	František
příliš	příliš	k6eAd1	příliš
šťastný	šťastný	k2eAgMnSc1d1	šťastný
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Manželce	manželka	k1gFnSc3	manželka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	doba	k1gFnSc6	doba
začala	začít	k5eAaPmAgFnS	začít
žít	žít	k5eAaImF	žít
mimo	mimo	k7c4	mimo
Rakousko	Rakousko	k1gNnSc4	Rakousko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odcizil	odcizit	k5eAaPmAgMnS	odcizit
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
jediný	jediný	k2eAgMnSc1d1	jediný
syn	syn	k1gMnSc1	syn
Rudolf	Rudolf	k1gMnSc1	Rudolf
spáchal	spáchat	k5eAaPmAgMnS	spáchat
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
bratra	bratr	k1gMnSc2	bratr
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
zastřelili	zastřelit	k5eAaPmAgMnP	zastřelit
republikáni	republikán	k1gMnPc1	republikán
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
povahu	povaha	k1gFnSc4	povaha
byla	být	k5eAaImAgFnS	být
typická	typický	k2eAgFnSc1d1	typická
velká	velký	k2eAgFnSc1d1	velká
píle	píle	k1gFnSc1	píle
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterou	který	k3yIgFnSc7	který
plnil	plnit	k5eAaImAgInS	plnit
své	svůj	k3xOyFgFnPc4	svůj
panovnické	panovnický	k2eAgFnPc4d1	panovnická
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
konzervativní	konzervativní	k2eAgNnSc4d1	konzervativní
smýšlení	smýšlení	k1gNnSc4	smýšlení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
dospívání	dospívání	k1gNnSc2	dospívání
==	==	k?	==
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
Karel	Karel	k1gMnSc1	Karel
(	(	kIx(	(
<g/>
Franz	Franz	k1gMnSc1	Franz
Joseph	Joseph	k1gMnSc1	Joseph
Karl	Karla	k1gFnPc2	Karla
von	von	k1gInSc1	von
Habsburg	Habsburg	k1gMnSc1	Habsburg
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1830	[number]	k4	1830
v	v	k7c4	v
9.45	[number]	k4	9.45
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
jako	jako	k9	jako
syn	syn	k1gMnSc1	syn
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Františka	František	k1gMnSc2	František
Karla	Karel	k1gMnSc2	Karel
(	(	kIx(	(
<g/>
mladšího	mladý	k2eAgMnSc2d2	mladší
syna	syn	k1gMnSc2	syn
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
ženy	žena	k1gFnPc1	žena
Žofie	Žofie	k1gFnSc2	Žofie
<g/>
,	,	kIx,	,
bavorské	bavorský	k2eAgFnSc2d1	bavorská
princezny	princezna	k1gFnSc2	princezna
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
přezdíváno	přezdívat	k5eAaImNgNnS	přezdívat
Salzprinz	Salzprinz	k1gInSc4	Salzprinz
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
před	před	k7c7	před
početím	početí	k1gNnSc7	početí
podstoupila	podstoupit	k5eAaPmAgFnS	podstoupit
léčbu	léčba	k1gFnSc4	léčba
v	v	k7c6	v
Bad	Bad	k1gFnSc6	Bad
Ischlu	Ischl	k1gInSc2	Ischl
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
porodu	porod	k1gInSc3	porod
přihlížela	přihlížet	k5eAaImAgFnS	přihlížet
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
císařského	císařský	k2eAgInSc2d1	císařský
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
při	při	k7c6	při
něm	on	k3xPp3gMnSc6	on
podávalo	podávat	k5eAaImAgNnS	podávat
občerstvení	občerstvení	k1gNnSc1	občerstvení
<g/>
.	.	kIx.	.
<g/>
Pokládalo	pokládat	k5eAaImAgNnS	pokládat
se	se	k3xPyFc4	se
za	za	k7c4	za
velice	velice	k6eAd1	velice
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednou	jeden	k4xCgFnSc7	jeden
usedne	usednout	k5eAaPmIp3nS	usednout
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
a	a	k8xC	a
tomu	ten	k3xDgNnSc3	ten
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
důkladná	důkladný	k2eAgFnSc1d1	důkladná
výchova	výchova	k1gFnSc1	výchova
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tu	ten	k3xDgFnSc4	ten
dohlížela	dohlížet	k5eAaImAgFnS	dohlížet
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
formovala	formovat	k5eAaImAgFnS	formovat
i	i	k9	i
jeho	jeho	k3xOp3gInPc4	jeho
politické	politický	k2eAgInPc4d1	politický
názory	názor	k1gInPc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
byl	být	k5eAaImAgMnS	být
vychováván	vychovávat	k5eAaImNgMnS	vychovávat
v	v	k7c6	v
přesvědčení	přesvědčení	k1gNnSc6	přesvědčení
o	o	k7c6	o
silné	silný	k2eAgFnSc6d1	silná
panovnické	panovnický	k2eAgFnSc6d1	panovnická
moci	moc	k1gFnSc6	moc
a	a	k8xC	a
utvrzován	utvrzován	k2eAgMnSc1d1	utvrzován
v	v	k7c6	v
myšlence	myšlenka	k1gFnSc6	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
císař	císař	k1gMnSc1	císař
se	se	k3xPyFc4	se
zodpovídá	zodpovídat	k5eAaImIp3nS	zodpovídat
pouze	pouze	k6eAd1	pouze
Bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
běžných	běžný	k2eAgInPc2d1	běžný
předmětů	předmět	k1gInPc2	předmět
studoval	studovat	k5eAaImAgMnS	studovat
i	i	k9	i
vojenství	vojenství	k1gNnSc4	vojenství
a	a	k8xC	a
učil	učit	k5eAaImAgMnS	učit
se	se	k3xPyFc4	se
najednou	najednou	k6eAd1	najednou
několik	několik	k4yIc4	několik
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
češtinu	čeština	k1gFnSc4	čeština
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vojenským	vojenský	k2eAgFnPc3d1	vojenská
záležitostem	záležitost	k1gFnPc3	záležitost
a	a	k8xC	a
armádě	armáda	k1gFnSc3	armáda
měl	mít	k5eAaImAgInS	mít
již	již	k6eAd1	již
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
velmi	velmi	k6eAd1	velmi
kladný	kladný	k2eAgInSc1d1	kladný
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
se	se	k3xPyFc4	se
kladl	klást	k5eAaImAgInS	klást
na	na	k7c4	na
výuku	výuka	k1gFnSc4	výuka
teologie	teologie	k1gFnSc2	teologie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
měl	mít	k5eAaImAgInS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
pozdější	pozdní	k2eAgMnSc1d2	pozdější
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Rauscher	Rauschra	k1gFnPc2	Rauschra
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgMnS	být
vychováván	vychováván	k2eAgMnSc1d1	vychováván
ke	k	k7c3	k
zbožnosti	zbožnost	k1gFnSc3	zbožnost
a	a	k8xC	a
respektu	respekt	k1gInSc2	respekt
k	k	k7c3	k
římsko-katolické	římskoatolický	k2eAgFnSc3d1	římsko-katolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnPc1d1	státní
záležitosti	záležitost	k1gFnPc1	záležitost
ho	on	k3xPp3gNnSc4	on
od	od	k7c2	od
jeho	jeho	k3xOp3gNnPc2	jeho
sedmnácti	sedmnáct	k4xCc2	sedmnáct
let	léto	k1gNnPc2	léto
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
sám	sám	k3xTgMnSc1	sám
státní	státní	k2eAgMnSc1d1	státní
kancléř	kancléř	k1gMnSc1	kancléř
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
Metternich	Metternich	k1gMnSc1	Metternich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
říšská	říšský	k2eAgFnSc1d1	říšská
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgNnPc4	první
léta	léto	k1gNnPc4	léto
vlády	vláda	k1gFnSc2	vláda
===	===	k?	===
</s>
</p>
<p>
<s>
Císařem	Císař	k1gMnSc7	Císař
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
osmnácti	osmnáct	k4xCc6	osmnáct
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
během	během	k7c2	během
potlačení	potlačení	k1gNnSc2	potlačení
březnové	březnový	k2eAgFnSc2d1	březnová
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
jeho	jeho	k3xOp3gMnSc1	jeho
strýc	strýc	k1gMnSc1	strýc
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
trůnu	trůn	k1gInSc3	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rakouský	rakouský	k2eAgInSc4d1	rakouský
trůn	trůn	k1gInSc4	trůn
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1848	[number]	k4	1848
jako	jako	k8xS	jako
reprezentant	reprezentant	k1gMnSc1	reprezentant
nepříliš	příliš	k6eNd1	příliš
populární	populární	k2eAgFnSc2d1	populární
politické	politický	k2eAgFnSc2d1	politická
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
politickou	politický	k2eAgFnSc4d1	politická
scénu	scéna	k1gFnSc4	scéna
země	zem	k1gFnSc2	zem
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
spatřoval	spatřovat	k5eAaImAgMnS	spatřovat
svou	svůj	k3xOyFgFnSc4	svůj
hlavní	hlavní	k2eAgFnSc4d1	hlavní
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
zamezení	zamezení	k1gNnSc6	zamezení
další	další	k2eAgFnSc2d1	další
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
pozadí	pozadí	k1gNnSc4	pozadí
jeho	on	k3xPp3gInSc2	on
nástupu	nástup	k1gInSc2	nástup
stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
dvorská	dvorský	k2eAgFnSc1d1	dvorská
kamarila	kamarila	k1gFnSc1	kamarila
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgInPc4	který
měli	mít	k5eAaImAgMnP	mít
nejvlivnější	vlivný	k2eAgMnPc1d3	nejvlivnější
pozici	pozice	k1gFnSc6	pozice
vojáci	voják	k1gMnPc1	voják
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
konzervativní	konzervativní	k2eAgFnPc1d1	konzervativní
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
např.	např.	kA	např.
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
jeho	on	k3xPp3gNnSc2	on
jména	jméno	k1gNnSc2	jméno
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kontextu	kontext	k1gInSc6	kontext
mělo	mít	k5eAaImAgNnS	mít
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
určitý	určitý	k2eAgInSc4d1	určitý
kompromis	kompromis	k1gInSc4	kompromis
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
František	František	k1gMnSc1	František
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
císařova	císařův	k2eAgMnSc2d1	císařův
děda	děd	k1gMnSc2	děd
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
<g/>
)	)	kIx)	)
mělo	mít	k5eAaImAgNnS	mít
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
konzervativní	konzervativní	k2eAgFnPc4d1	konzervativní
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jméno	jméno	k1gNnSc1	jméno
Josef	Josefa	k1gFnPc2	Josefa
mělo	mít	k5eAaImAgNnS	mít
odkazovat	odkazovat	k5eAaImF	odkazovat
na	na	k7c4	na
částečné	částečný	k2eAgNnSc4d1	částečné
přijetí	přijetí	k1gNnSc4	přijetí
reformní	reformní	k2eAgFnSc2d1	reformní
politiky	politika	k1gFnSc2	politika
císaře	císař	k1gMnSc2	císař
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
Prvních	první	k4xOgMnPc2	první
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
Františkovy	Františkův	k2eAgFnSc2d1	Františkova
vlády	vláda	k1gFnSc2	vláda
měl	mít	k5eAaImAgInS	mít
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
Felix	Felix	k1gMnSc1	Felix
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
už	už	k6eAd1	už
řídil	řídit	k5eAaImAgMnS	řídit
císař	císař	k1gMnSc1	císař
říši	říše	k1gFnSc4	říše
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
popularitě	popularita	k1gFnSc3	popularita
nepřispěla	přispět	k5eNaPmAgFnS	přispět
krvavá	krvavý	k2eAgFnSc1d1	krvavá
porážka	porážka	k1gFnSc1	porážka
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Világoše	Világoš	k1gMnSc2	Világoš
<g/>
.	.	kIx.	.
</s>
<s>
Ovlivněn	ovlivněn	k2eAgMnSc1d1	ovlivněn
touto	tento	k3xDgFnSc7	tento
událostí	událost	k1gFnSc7	událost
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
Františka	František	k1gMnSc4	František
Josefa	Josef	k1gMnSc4	Josef
I.	I.	kA	I.
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zabít	zabít	k5eAaPmF	zabít
maďarský	maďarský	k2eAgMnSc1d1	maďarský
nacionalista	nacionalista	k1gMnSc1	nacionalista
János	János	k1gMnSc1	János
Libényi	Libényi	k1gNnPc2	Libényi
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
přítomnosti	přítomnost	k1gFnSc3	přítomnost
císařova	císařův	k2eAgMnSc2d1	císařův
pobočníka	pobočník	k1gMnSc2	pobočník
Maxmiliena	Maxmilien	k1gMnSc2	Maxmilien
O	o	k7c6	o
<g/>
́	́	k?	́
<g/>
Donnella	Donnell	k1gMnSc2	Donnell
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
však	však	k9	však
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Libényi	Libényi	k6eAd1	Libényi
byl	být	k5eAaImAgMnS	být
nakonec	nakonec	k6eAd1	nakonec
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
popraven	popraven	k2eAgMnSc1d1	popraven
<g/>
.	.	kIx.	.
<g/>
Mladý	mladý	k2eAgMnSc1d1	mladý
císař	císař	k1gMnSc1	císař
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
spíše	spíše	k9	spíše
symbolem	symbol	k1gInSc7	symbol
reakční	reakční	k2eAgFnSc2d1	reakční
formy	forma	k1gFnSc2	forma
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
Bachův	Bachův	k2eAgInSc4d1	Bachův
neoabsolutismus	neoabsolutismus	k1gInSc4	neoabsolutismus
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
rakouského	rakouský	k2eAgMnSc2d1	rakouský
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Alexandra	Alexandr	k1gMnSc2	Alexandr
von	von	k1gInSc1	von
Bacha	Bacha	k?	Bacha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
režim	režim	k1gInSc1	režim
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c4	o
posílení	posílení	k1gNnSc4	posílení
integrace	integrace	k1gFnSc2	integrace
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
další	další	k2eAgFnSc4d1	další
centralizaci	centralizace	k1gFnSc4	centralizace
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
také	také	k9	také
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
Uhrám	Uhry	k1gFnPc3	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Ideologicky	ideologicky	k6eAd1	ideologicky
se	se	k3xPyFc4	se
stavěl	stavět	k5eAaImAgMnS	stavět
nepřátelsky	přátelsky	k6eNd1	přátelsky
k	k	k7c3	k
demokratizačním	demokratizační	k2eAgFnPc3d1	demokratizační
snahám	snaha	k1gFnPc3	snaha
a	a	k8xC	a
požadavkům	požadavek	k1gInPc3	požadavek
stále	stále	k6eAd1	stále
významnější	významný	k2eAgFnSc2d2	významnější
rakouské	rakouský	k2eAgFnSc2d1	rakouská
buržoazie	buržoazie	k1gFnSc2	buržoazie
o	o	k7c4	o
demokratické	demokratický	k2eAgNnSc4d1	demokratické
zastoupení	zastoupení	k1gNnSc4	zastoupení
<g/>
.	.	kIx.	.
<g/>
Vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Bach	Bach	k1gMnSc1	Bach
zasedal	zasedat	k5eAaImAgMnS	zasedat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
také	také	k6eAd1	také
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
reforem	reforma	k1gFnPc2	reforma
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
zmodernizovat	zmodernizovat	k5eAaPmF	zmodernizovat
habsburskou	habsburský	k2eAgFnSc4d1	habsburská
monarchii	monarchie	k1gFnSc4	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
celková	celkový	k2eAgFnSc1d1	celková
liberalizace	liberalizace	k1gFnSc1	liberalizace
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
sféry	sféra	k1gFnSc2	sféra
<g/>
,	,	kIx,	,
např.	např.	kA	např.
se	se	k3xPyFc4	se
přestala	přestat	k5eAaPmAgFnS	přestat
platit	platit	k5eAaImF	platit
vnitřní	vnitřní	k2eAgNnPc4d1	vnitřní
cla	clo	k1gNnPc4	clo
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
cechovních	cechovní	k2eAgNnPc2d1	cechovní
omezení	omezení	k1gNnPc2	omezení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
rozvoj	rozvoj	k1gInSc4	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
zakládání	zakládání	k1gNnSc3	zakládání
tzv.	tzv.	kA	tzv.
živnostenských	živnostenský	k2eAgFnPc2d1	Živnostenská
komor	komora	k1gFnPc2	komora
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
fungovaly	fungovat	k5eAaImAgFnP	fungovat
na	na	k7c6	na
moderních	moderní	k2eAgInPc6d1	moderní
principech	princip	k1gInPc6	princip
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
však	však	k9	však
vyžadoval	vyžadovat	k5eAaImAgMnS	vyžadovat
další	další	k2eAgFnPc4d1	další
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
změnit	změnit	k5eAaPmF	změnit
finanční	finanční	k2eAgFnSc4d1	finanční
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
zlepšit	zlepšit	k5eAaPmF	zlepšit
dostupnost	dostupnost	k1gFnSc4	dostupnost
úvěrů	úvěr	k1gInPc2	úvěr
pro	pro	k7c4	pro
podnikatele	podnikatel	k1gMnPc4	podnikatel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
vznik	vznik	k1gInSc4	vznik
banky	banka	k1gFnSc2	banka
Creditanstalt	Creditanstalta	k1gFnPc2	Creditanstalta
<g/>
.	.	kIx.	.
<g/>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
dokladů	doklad	k1gInPc2	doklad
císařových	císařův	k2eAgFnPc2d1	císařova
totalitárních	totalitární	k2eAgFnPc2d1	totalitární
tendencí	tendence	k1gFnPc2	tendence
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
nařízení	nařízení	k1gNnSc1	nařízení
deportovat	deportovat	k5eAaBmF	deportovat
na	na	k7c6	na
3,5	[number]	k4	3,5
roku	rok	k1gInSc2	rok
předního	přední	k2eAgMnSc4d1	přední
českého	český	k2eAgMnSc4d1	český
žurnalistu	žurnalista	k1gMnSc4	žurnalista
Karla	Karel	k1gMnSc4	Karel
Havlíčka	Havlíček	k1gMnSc4	Havlíček
Borovského	Borovský	k1gMnSc4	Borovský
do	do	k7c2	do
jihotyrolského	jihotyrolský	k2eAgInSc2d1	jihotyrolský
Brixenu	Brixen	k1gInSc2	Brixen
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
bez	bez	k7c2	bez
jakéhokoli	jakýkoli	k3yIgNnSc2	jakýkoli
soudního	soudní	k2eAgNnSc2d1	soudní
řízení	řízení	k1gNnSc2	řízení
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
1849	[number]	k4	1849
<g/>
–	–	k?	–
<g/>
1861	[number]	k4	1861
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
vyhlášeny	vyhlásit	k5eAaPmNgFnP	vyhlásit
dvě	dva	k4xCgFnPc1	dva
oktrojované	oktrojovaný	k2eAgFnPc1d1	oktrojovaná
ústavy	ústava	k1gFnPc1	ústava
–	–	k?	–
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
"	"	kIx"	"
<g/>
březnová	březnový	k2eAgFnSc1d1	březnová
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
také	také	k9	také
Stadionova	Stadionův	k2eAgFnSc1d1	Stadionova
(	(	kIx(	(
<g/>
jejím	její	k3xOp3gMnSc7	její
spoluautorem	spoluautor	k1gMnSc7	spoluautor
byl	být	k5eAaImAgMnS	být
hrabě	hrabě	k1gMnSc1	hrabě
Stadion	stadion	k1gInSc1	stadion
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
"	"	kIx"	"
<g/>
únorová	únorový	k2eAgFnSc1d1	únorová
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
také	také	k9	také
Schmerlingova	Schmerlingův	k2eAgFnSc1d1	Schmerlingova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
základy	základ	k1gInPc1	základ
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
Silvestrovských	silvestrovský	k2eAgInPc6d1	silvestrovský
patentech	patent	k1gInPc6	patent
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
nechal	nechat	k5eAaPmAgMnS	nechat
císař	císař	k1gMnSc1	císař
rozpustit	rozpustit	k5eAaPmF	rozpustit
říšský	říšský	k2eAgInSc4d1	říšský
sněm	sněm	k1gInSc4	sněm
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
vlastní	vlastní	k2eAgFnSc2d1	vlastní
ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Krymská	krymský	k2eAgFnSc1d1	Krymská
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
porážka	porážka	k1gFnSc1	porážka
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
monarchie	monarchie	k1gFnSc2	monarchie
se	se	k3xPyFc4	se
mladý	mladý	k2eAgMnSc1d1	mladý
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
výrazněji	výrazně	k6eAd2	výrazně
zapsal	zapsat	k5eAaPmAgMnS	zapsat
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
Krymskou	krymský	k2eAgFnSc7d1	Krymská
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
následně	následně	k6eAd1	následně
s	s	k7c7	s
válkou	válka	k1gFnSc7	válka
s	s	k7c7	s
Piemontem	Piemont	k1gInSc7	Piemont
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
neúspěch	neúspěch	k1gInSc4	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Krymské	krymský	k2eAgFnSc2d1	Krymská
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
nepodpořit	podpořit	k5eNaPmF	podpořit
svého	svůj	k3xOyFgMnSc4	svůj
spojence	spojenec	k1gMnSc4	spojenec
cara	car	k1gMnSc4	car
Mikuláše	Mikuláš	k1gMnSc4	Mikuláš
I.	I.	kA	I.
Rakousko	Rakousko	k1gNnSc1	Rakousko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
postavilo	postavit	k5eAaPmAgNnS	postavit
proti	proti	k7c3	proti
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
prakticky	prakticky	k6eAd1	prakticky
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
vojenské	vojenský	k2eAgFnSc3d1	vojenská
akci	akce	k1gFnSc3	akce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
politika	politika	k1gFnSc1	politika
Rakouska	Rakousko	k1gNnSc2	Rakousko
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
vyvolání	vyvolání	k1gNnSc3	vyvolání
nepřátelství	nepřátelství	k1gNnSc2	nepřátelství
s	s	k7c7	s
ruskou	ruský	k2eAgFnSc7d1	ruská
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Rakousko	Rakousko	k1gNnSc1	Rakousko
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
těžké	těžký	k2eAgFnSc2d1	těžká
pozice	pozice	k1gFnSc2	pozice
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
se	se	k3xPyFc4	se
ocitlo	ocitnout	k5eAaPmAgNnS	ocitnout
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
izolaci	izolace	k1gFnSc6	izolace
a	a	k8xC	a
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
ruskou	ruský	k2eAgFnSc4d1	ruská
důvěru	důvěra	k1gFnSc4	důvěra
<g/>
.	.	kIx.	.
<g/>
Této	tento	k3xDgFnSc2	tento
situace	situace	k1gFnSc2	situace
využilo	využít	k5eAaPmAgNnS	využít
Sardinské	sardinský	k2eAgNnSc1d1	Sardinské
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
postavilo	postavit	k5eAaPmAgNnS	postavit
proti	proti	k7c3	proti
Rakouské	rakouský	k2eAgFnSc3d1	rakouská
vládě	vláda	k1gFnSc3	vláda
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
byla	být	k5eAaImAgFnS	být
rakouská	rakouský	k2eAgFnSc1d1	rakouská
armáda	armáda	k1gFnSc1	armáda
poražena	poražen	k2eAgFnSc1d1	poražena
u	u	k7c2	u
Magenty	Magenta	k1gFnSc2	Magenta
a	a	k8xC	a
Solferina	Solferino	k1gNnSc2	Solferino
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
Rakousko	Rakousko	k1gNnSc1	Rakousko
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
Lombardii	Lombardie	k1gFnSc4	Lombardie
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Solferina	Solferino	k1gNnSc2	Solferino
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
armádu	armáda	k1gFnSc4	armáda
porazila	porazit	k5eAaPmAgFnS	porazit
francouzsko-sardinská	francouzskoardinský	k2eAgNnPc1d1	francouzsko-sardinský
vojska	vojsko	k1gNnPc4	vojsko
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Napoleona	Napoleon	k1gMnSc2	Napoleon
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
Viktora	Viktor	k1gMnSc2	Viktor
Emanuela	Emanuel	k1gMnSc2	Emanuel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
osobně	osobně	k6eAd1	osobně
velel	velet	k5eAaImAgInS	velet
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
porážka	porážka	k1gFnSc1	porážka
znamenala	znamenat	k5eAaImAgFnS	znamenat
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
krok	krok	k1gInSc4	krok
ke	k	k7c3	k
sjednocení	sjednocení	k1gNnSc3	sjednocení
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
ztráty	ztráta	k1gFnSc2	ztráta
Lombardie	Lombardie	k1gFnSc2	Lombardie
<g/>
,	,	kIx,	,
i	i	k8xC	i
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
špatné	špatný	k2eAgFnSc3d1	špatná
situaci	situace	k1gFnSc3	situace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
státních	státní	k2eAgFnPc2d1	státní
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
odvolání	odvolání	k1gNnSc1	odvolání
Bacha	Bacha	k?	Bacha
a	a	k8xC	a
postupné	postupný	k2eAgNnSc4d1	postupné
vytváření	vytváření	k1gNnSc4	vytváření
konstituční	konstituční	k2eAgFnSc2d1	konstituční
monarchie	monarchie	k1gFnSc2	monarchie
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1860	[number]	k4	1860
<g/>
–	–	k?	–
<g/>
1867	[number]	k4	1867
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
tzv.	tzv.	kA	tzv.
Říjnovým	říjnový	k2eAgInSc7d1	říjnový
diplomem	diplom	k1gInSc7	diplom
otevřel	otevřít	k5eAaPmAgInS	otevřít
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
reformě	reforma	k1gFnSc3	reforma
Rakouska	Rakousko	k1gNnSc2	Rakousko
v	v	k7c4	v
konstituční	konstituční	k2eAgFnSc4d1	konstituční
monarchii	monarchie	k1gFnSc4	monarchie
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
přizpůsoboval	přizpůsobovat	k5eAaImAgMnS	přizpůsobovat
nové	nový	k2eAgFnSc3d1	nová
roli	role	k1gFnSc3	role
konstitučního	konstituční	k2eAgMnSc2d1	konstituční
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sice	sice	k8xC	sice
neodpovídala	odpovídat	k5eNaImAgFnS	odpovídat
jeho	jeho	k3xOp3gFnSc1	jeho
osobní	osobní	k2eAgFnSc1d1	osobní
víře	víra	k1gFnSc3	víra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kterou	který	k3yQgFnSc4	který
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
spíše	spíše	k9	spíše
mužem	muž	k1gMnSc7	muž
kompromisu	kompromis	k1gInSc2	kompromis
než	než	k8xS	než
zastáncem	zastánce	k1gMnSc7	zastánce
tvrdé	tvrdý	k2eAgFnSc2d1	tvrdá
linie	linie	k1gFnSc2	linie
politické	politický	k2eAgFnSc2d1	politická
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
touto	tento	k3xDgFnSc7	tento
změnou	změna	k1gFnSc7	změna
stála	stát	k5eAaImAgFnS	stát
zřejmě	zřejmě	k6eAd1	zřejmě
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
zachování	zachování	k1gNnSc4	zachování
celistvosti	celistvost	k1gFnSc2	celistvost
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prohrané	prohraný	k2eAgFnSc6d1	prohraná
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
Pruskem	Prusko	k1gNnSc7	Prusko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
a	a	k8xC	a
konečné	konečný	k2eAgFnSc6d1	konečná
ztrátě	ztráta	k1gFnSc6	ztráta
severoitalských	severoitalský	k2eAgFnPc2d1	severoitalská
držav	država	k1gFnPc2	država
(	(	kIx(	(
<g/>
Benátska	Benátska	k1gFnSc1	Benátska
a	a	k8xC	a
Lombardska	Lombardska	k?	Lombardska
<g/>
)	)	kIx)	)
přistoupil	přistoupit	k5eAaPmAgMnS	přistoupit
císař	císař	k1gMnSc1	císař
k	k	k7c3	k
vyrovnání	vyrovnání	k1gNnSc3	vyrovnání
s	s	k7c7	s
Uherskem	Uhersko	k1gNnSc7	Uhersko
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
korunovat	korunovat	k5eAaBmF	korunovat
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
opakovaný	opakovaný	k2eAgInSc4d1	opakovaný
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
korunovat	korunovat	k5eAaBmF	korunovat
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nesplnil	splnit	k5eNaPmAgMnS	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
ale	ale	k9	ale
nikdy	nikdy	k6eAd1	nikdy
nenechal	nechat	k5eNaPmAgMnS	nechat
korunovat	korunovat	k5eAaBmF	korunovat
ani	ani	k9	ani
rakouským	rakouský	k2eAgMnSc7d1	rakouský
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prosincová	prosincový	k2eAgFnSc1d1	prosincová
ústava	ústava	k1gFnSc1	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
stanovila	stanovit	k5eAaPmAgFnS	stanovit
poměrně	poměrně	k6eAd1	poměrně
jasné	jasný	k2eAgFnPc4d1	jasná
pravomoci	pravomoc	k1gFnPc4	pravomoc
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
díky	díky	k7c3	díky
rakousko-uherskému	rakouskoherský	k2eAgNnSc3d1	rakousko-uherské
vyrovnání	vyrovnání	k1gNnSc3	vyrovnání
stal	stát	k5eAaPmAgInS	stát
ústředním	ústřední	k2eAgMnSc7d1	ústřední
představitelem	představitel	k1gMnSc7	představitel
a	a	k8xC	a
symbolem	symbol	k1gInSc7	symbol
jednoty	jednota	k1gFnSc2	jednota
dvojstátí	dvojstátit	k5eAaPmIp3nS	dvojstátit
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jako	jako	k8xS	jako
konstituční	konstituční	k2eAgMnSc1d1	konstituční
panovník	panovník	k1gMnSc1	panovník
byl	být	k5eAaImAgMnS	být
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
stále	stále	k6eAd1	stále
panovníkem	panovník	k1gMnSc7	panovník
"	"	kIx"	"
<g/>
z	z	k7c2	z
Boží	boží	k2eAgFnSc2d1	boží
vůle	vůle	k1gFnSc2	vůle
<g/>
"	"	kIx"	"
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
pravomocí	pravomoc	k1gFnPc2	pravomoc
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
jeho	jeho	k3xOp3gFnPc1	jeho
kritiky	kritika	k1gFnPc1	kritika
označovány	označovat	k5eAaImNgFnP	označovat
za	za	k7c4	za
absolutistické	absolutistický	k2eAgNnSc4d1	absolutistické
<g/>
.	.	kIx.	.
</s>
<s>
Konstituční	konstituční	k2eAgNnSc1d1	konstituční
období	období	k1gNnSc1	období
vlády	vláda	k1gFnSc2	vláda
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
dobou	doba	k1gFnSc7	doba
vzestupu	vzestup	k1gInSc2	vzestup
jeho	jeho	k3xOp3gFnSc2	jeho
popularity	popularita	k1gFnSc2	popularita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vznik	vznik	k1gInSc1	vznik
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
a	a	k8xC	a
národnostní	národnostní	k2eAgFnSc1d1	národnostní
politika	politika	k1gFnSc1	politika
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
říši	říš	k1gFnSc6	říš
se	se	k3xPyFc4	se
začínala	začínat	k5eAaImAgFnS	začínat
výrazněji	výrazně	k6eAd2	výrazně
otevírat	otevírat	k5eAaImF	otevírat
národnostní	národnostní	k2eAgFnSc1d1	národnostní
problematika	problematika	k1gFnSc1	problematika
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
císaře	císař	k1gMnSc2	císař
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
tzv.	tzv.	kA	tzv.
říjnového	říjnový	k2eAgInSc2d1	říjnový
diplomu	diplom	k1gInSc2	diplom
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
řešení	řešení	k1gNnSc4	řešení
této	tento	k3xDgFnSc2	tento
problematiky	problematika	k1gFnSc2	problematika
novou	nový	k2eAgFnSc7d1	nová
ústavou	ústava	k1gFnSc7	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nesplnila	splnit	k5eNaPmAgFnS	splnit
očekávání	očekávání	k1gNnSc4	očekávání
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
vkládaná	vkládaný	k2eAgFnSc1d1	vkládaná
a	a	k8xC	a
následovala	následovat	k5eAaImAgFnS	následovat
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
prakticky	prakticky	k6eAd1	prakticky
všech	všecek	k3xTgFnPc2	všecek
národností	národnost	k1gFnPc2	národnost
vyjma	vyjma	k7c2	vyjma
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
nejsilnější	silný	k2eAgInSc4d3	nejsilnější
odpor	odpor	k1gInSc4	odpor
narazila	narazit	k5eAaPmAgFnS	narazit
nová	nový	k2eAgFnSc1d1	nová
ústava	ústava	k1gFnSc1	ústava
v	v	k7c6	v
Uhersku	Uhersko	k1gNnSc6	Uhersko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
požadovalo	požadovat	k5eAaImAgNnS	požadovat
návrat	návrat	k1gInSc4	návrat
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Maďaři	Maďar	k1gMnPc1	Maďar
prosadili	prosadit	k5eAaPmAgMnP	prosadit
dualistickou	dualistický	k2eAgFnSc4d1	dualistická
koncepci	koncepce	k1gFnSc4	koncepce
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
užíván	užíván	k2eAgInSc1d1	užíván
termín	termín	k1gInSc1	termín
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
dle	dle	k7c2	dle
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
prosince	prosinec	k1gInSc2	prosinec
1867	[number]	k4	1867
společnou	společný	k2eAgFnSc4d1	společná
osobu	osoba	k1gFnSc4	osoba
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
finance	finance	k1gFnPc4	finance
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Rakousko-uherské	rakouskoherský	k2eAgNnSc1d1	rakousko-uherské
vyrovnání	vyrovnání	k1gNnSc1	vyrovnání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
otázkách	otázka	k1gFnPc6	otázka
existovala	existovat	k5eAaImAgFnS	existovat
značná	značný	k2eAgFnSc1d1	značná
suverenita	suverenita	k1gFnSc1	suverenita
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Každých	každý	k3xTgMnPc2	každý
deset	deset	k4xCc1	deset
let	léto	k1gNnPc2	léto
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
stanovení	stanovení	k1gNnSc3	stanovení
poměru	poměr	k1gInSc2	poměr
financování	financování	k1gNnSc2	financování
společných	společný	k2eAgInPc2d1	společný
výdajů	výdaj	k1gInPc2	výdaj
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
poměrně	poměrně	k6eAd1	poměrně
ostrým	ostrý	k2eAgInPc3d1	ostrý
sporům	spor	k1gInPc3	spor
<g/>
.	.	kIx.	.
<g/>
Národnostní	národnostní	k2eAgFnSc1d1	národnostní
nerovnost	nerovnost	k1gFnSc1	nerovnost
byla	být	k5eAaImAgFnS	být
realitou	realita	k1gFnSc7	realita
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2	Rakouska-Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
v	v	k7c6	v
monarchii	monarchie	k1gFnSc6	monarchie
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
Slovanů	Slovan	k1gMnPc2	Slovan
<g/>
,	,	kIx,	,
daleko	daleko	k6eAd1	daleko
lepší	dobrý	k2eAgNnSc4d2	lepší
postavení	postavení	k1gNnSc4	postavení
všude	všude	k6eAd1	všude
měla	mít	k5eAaImAgFnS	mít
německá	německý	k2eAgFnSc1d1	německá
minorita	minorita	k1gFnSc1	minorita
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
politické	politický	k2eAgFnPc1d1	politická
špičky	špička	k1gFnPc1	špička
se	se	k3xPyFc4	se
pokoušely	pokoušet	k5eAaImAgFnP	pokoušet
prosadit	prosadit	k5eAaPmF	prosadit
trialistickou	trialistický	k2eAgFnSc4d1	trialistický
koncepci	koncepce	k1gFnSc4	koncepce
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
,	,	kIx,	,
narazily	narazit	k5eAaPmAgFnP	narazit
ale	ale	k9	ale
na	na	k7c4	na
protest	protest	k1gInSc4	protest
Němců	Němec	k1gMnPc2	Němec
i	i	k8xC	i
Maďarů	Maďar	k1gMnPc2	Maďar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1867	[number]	k4	1867
<g/>
–	–	k?	–
<g/>
1908	[number]	k4	1908
===	===	k?	===
</s>
</p>
<p>
<s>
Jestliže	jestliže	k8xS	jestliže
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
nepopulární	populární	k2eNgFnSc2d1	nepopulární
reakce	reakce	k1gFnSc2	reakce
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
spáchán	spáchán	k2eAgInSc1d1	spáchán
atentát	atentát	k1gInSc1	atentát
<g/>
,	,	kIx,	,
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
stárnoucí	stárnoucí	k2eAgMnSc1d1	stárnoucí
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
stával	stávat	k5eAaImAgMnS	stávat
populárním	populární	k2eAgMnSc7d1	populární
a	a	k8xC	a
respektovaným	respektovaný	k2eAgMnSc7d1	respektovaný
monarchou	monarcha	k1gMnSc7	monarcha
<g/>
.	.	kIx.	.
</s>
<s>
Císařova	Císařův	k2eAgFnSc1d1	Císařova
popularita	popularita	k1gFnSc1	popularita
jistě	jistě	k6eAd1	jistě
souvisela	souviset	k5eAaImAgFnS	souviset
s	s	k7c7	s
císařovým	císařův	k2eAgInSc7d1	císařův
stylem	styl	k1gInSc7	styl
vystupování	vystupování	k1gNnSc2	vystupování
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
novou	nový	k2eAgFnSc7d1	nová
politickou	politický	k2eAgFnSc7d1	politická
linií	linie	k1gFnSc7	linie
kompromisu	kompromis	k1gInSc2	kompromis
a	a	k8xC	a
opatrného	opatrný	k2eAgNnSc2d1	opatrné
zasahování	zasahování	k1gNnSc2	zasahování
do	do	k7c2	do
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
výrazných	výrazný	k2eAgInPc2d1	výrazný
projevů	projev	k1gInPc2	projev
císařovy	císařův	k2eAgFnSc2d1	císařova
politické	politický	k2eAgFnSc2d1	politická
vůle	vůle	k1gFnSc2	vůle
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
byla	být	k5eAaImAgFnS	být
kauza	kauza	k1gFnSc1	kauza
kolem	kolem	k7c2	kolem
potvrzení	potvrzení	k1gNnPc2	potvrzení
Karla	Karel	k1gMnSc2	Karel
Luegera	Lueger	k1gMnSc2	Lueger
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
vídeňského	vídeňský	k2eAgMnSc2d1	vídeňský
starosty	starosta	k1gMnSc2	starosta
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
a	a	k8xC	a
král	král	k1gMnSc1	král
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
nechal	nechat	k5eAaPmAgMnS	nechat
celkem	celkem	k6eAd1	celkem
třikrát	třikrát	k6eAd1	třikrát
opakovat	opakovat	k5eAaImF	opakovat
volby	volba	k1gFnPc4	volba
<g/>
,	,	kIx,	,
než	než	k8xS	než
Luegera	Luegera	k1gFnSc1	Luegera
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
antisemitské	antisemitský	k2eAgInPc1d1	antisemitský
a	a	k8xC	a
populistické	populistický	k2eAgInPc1d1	populistický
názory	názor	k1gInPc1	názor
se	se	k3xPyFc4	se
císařovi	císařův	k2eAgMnPc1d1	císařův
zřejmě	zřejmě	k6eAd1	zřejmě
protivily	protivit	k5eAaImAgFnP	protivit
<g/>
,	,	kIx,	,
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
.	.	kIx.	.
<g/>
Kabinety	kabinet	k1gInPc1	kabinet
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
císař	císař	k1gMnSc1	císař
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
až	až	k6eAd1	až
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
prováděly	provádět	k5eAaImAgFnP	provádět
reformy	reforma	k1gFnPc1	reforma
např.	např.	kA	např.
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
volebního	volební	k2eAgInSc2d1	volební
zákona	zákon	k1gInSc2	zákon
a	a	k8xC	a
snažily	snažit	k5eAaImAgFnP	snažit
se	se	k3xPyFc4	se
udržet	udržet	k5eAaPmF	udržet
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
národnostní	národnostní	k2eAgFnSc2d1	národnostní
spory	spora	k1gFnSc2	spora
v	v	k7c6	v
monarchii	monarchie	k1gFnSc6	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgFnSc1d1	volební
reforma	reforma	k1gFnSc1	reforma
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
realizovala	realizovat	k5eAaBmAgFnS	realizovat
vláda	vláda	k1gFnSc1	vláda
Eduarda	Eduard	k1gMnSc2	Eduard
Taaffeho	Taaffe	k1gMnSc2	Taaffe
<g/>
,	,	kIx,	,
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
počet	počet	k1gInSc4	počet
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
voličů	volič	k1gMnPc2	volič
o	o	k7c4	o
stovky	stovka	k1gFnPc4	stovka
tisíc	tisíc	k4xCgInPc2	tisíc
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
disponovalo	disponovat	k5eAaBmAgNnS	disponovat
volebním	volební	k2eAgInPc3d1	volební
právem	právem	k6eAd1	právem
v	v	k7c6	v
Předlitavsku	Předlitavsko	k1gNnSc6	Předlitavsko
15	[number]	k4	15
%	%	kIx~	%
mužského	mužský	k2eAgNnSc2d1	mužské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
stával	stávat	k5eAaImAgMnS	stávat
jakousi	jakýsi	k3yIgFnSc7	jakýsi
legendou	legenda	k1gFnSc7	legenda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stmeluje	stmelovat	k5eAaImIp3nS	stmelovat
mnohonárodnostní	mnohonárodnostní	k2eAgInSc4d1	mnohonárodnostní
charakter	charakter	k1gInSc4	charakter
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
národnostní	národnostní	k2eAgFnSc6d1	národnostní
otázce	otázka	k1gFnSc6	otázka
trval	trvat	k5eAaImAgInS	trvat
na	na	k7c6	na
zvýhodněném	zvýhodněný	k2eAgNnSc6d1	zvýhodněné
postavení	postavení	k1gNnSc6	postavení
pro	pro	k7c4	pro
Maďary	Maďar	k1gMnPc4	Maďar
a	a	k8xC	a
odmítal	odmítat	k5eAaImAgMnS	odmítat
federalizaci	federalizace	k1gFnSc4	federalizace
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
jeho	jeho	k3xOp3gMnSc1	jeho
synovec	synovec	k1gMnSc1	synovec
a	a	k8xC	a
následník	následník	k1gMnSc1	následník
František	František	k1gMnSc1	František
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
jiného	jiný	k2eAgInSc2d1	jiný
problému	problém	k1gInSc2	problém
se	se	k3xPyFc4	se
ale	ale	k9	ale
dokázal	dokázat	k5eAaPmAgInS	dokázat
době	doba	k1gFnSc3	doba
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
a	a	k8xC	a
tak	tak	k6eAd1	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
pomohl	pomoct	k5eAaPmAgMnS	pomoct
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
prosadit	prosadit	k5eAaPmF	prosadit
zavedení	zavedení	k1gNnSc4	zavedení
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
v	v	k7c6	v
Předlitavsku	Předlitavsko	k1gNnSc6	Předlitavsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
zhoršilo	zhoršit	k5eAaPmAgNnS	zhoršit
rakouské	rakouský	k2eAgNnSc1d1	rakouské
postavení	postavení	k1gNnSc1	postavení
v	v	k7c6	v
Německém	německý	k2eAgInSc6d1	německý
spolku	spolek	k1gInSc6	spolek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
postupně	postupně	k6eAd1	postupně
vzrůstala	vzrůstat	k5eAaImAgFnS	vzrůstat
moc	moc	k1gFnSc4	moc
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
spojilo	spojit	k5eAaPmAgNnS	spojit
Prusko	Prusko	k1gNnSc1	Prusko
i	i	k8xC	i
Rakousko	Rakousko	k1gNnSc1	Rakousko
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
dánské	dánský	k2eAgFnSc6d1	dánská
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k9	již
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
německé	německý	k2eAgFnSc6d1	německá
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
počátečních	počáteční	k2eAgInPc6d1	počáteční
úspěších	úspěch	k1gInPc6	úspěch
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
Rakousko	Rakousko	k1gNnSc4	Rakousko
poraženo	poražen	k2eAgNnSc4d1	poraženo
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
bylo	být	k5eAaImAgNnS	být
prakticky	prakticky	k6eAd1	prakticky
donuceno	donucen	k2eAgNnSc1d1	donuceno
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgNnSc2	svůj
postavení	postavení	k1gNnSc2	postavení
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
orientovat	orientovat	k5eAaBmF	orientovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
však	však	k9	však
nutně	nutně	k6eAd1	nutně
muselo	muset	k5eAaImAgNnS	muset
vést	vést	k5eAaImF	vést
k	k	k7c3	k
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
neúspěchu	neúspěch	k1gInSc6	neúspěch
se	se	k3xPyFc4	se
snažilo	snažit	k5eAaImAgNnS	snažit
Rakousko	Rakousko	k1gNnSc1	Rakousko
svoji	svůj	k3xOyFgFnSc4	svůj
politiku	politika	k1gFnSc4	politika
orientovat	orientovat	k5eAaBmF	orientovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Francii	Francie	k1gFnSc3	Francie
a	a	k8xC	a
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
Francie	Francie	k1gFnSc1	Francie
prohrála	prohrát	k5eAaPmAgFnS	prohrát
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
Pruskem	Prusko	k1gNnSc7	Prusko
<g/>
,	,	kIx,	,
rezignovalo	rezignovat	k5eAaBmAgNnS	rezignovat
Rakousko	Rakousko	k1gNnSc1	Rakousko
na	na	k7c4	na
snahu	snaha	k1gFnSc4	snaha
získat	získat	k5eAaPmF	získat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
zpět	zpět	k6eAd1	zpět
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
svojí	svůj	k3xOyFgFnSc7	svůj
další	další	k2eAgFnSc7d1	další
politikou	politika	k1gFnSc7	politika
uznalo	uznat	k5eAaPmAgNnS	uznat
pruskou	pruský	k2eAgFnSc4d1	pruská
dominanci	dominance	k1gFnSc4	dominance
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
císař	císař	k1gMnSc1	císař
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Zákupech	zákup	k1gInPc6	zákup
jednal	jednat	k5eAaImAgMnS	jednat
s	s	k7c7	s
ruským	ruský	k2eAgMnSc7d1	ruský
carem	car	k1gMnSc7	car
Alexandrem	Alexandr	k1gMnSc7	Alexandr
II	II	kA	II
<g/>
.	.	kIx.	.
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
velvyslanců	velvyslanec	k1gMnPc2	velvyslanec
obou	dva	k4xCgFnPc2	dva
zemí	zem	k1gFnPc2	zem
o	o	k7c4	o
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgNnSc1	žádný
prohlášení	prohlášení	k1gNnSc1	prohlášení
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
schůzky	schůzka	k1gFnSc2	schůzka
nebylo	být	k5eNaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
tajné	tajný	k2eAgFnSc3d1	tajná
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
si	se	k3xPyFc3	se
obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
rozdělily	rozdělit	k5eAaPmAgFnP	rozdělit
sféry	sféra	k1gFnPc4	sféra
svého	svůj	k3xOyFgInSc2	svůj
vlivu	vliv	k1gInSc2	vliv
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
s	s	k7c7	s
Pruskem	Prusko	k1gNnSc7	Prusko
obranný	obranný	k2eAgInSc1d1	obranný
pakt	pakt	k1gInSc1	pakt
proti	proti	k7c3	proti
Rusku	Rusko	k1gNnSc3	Rusko
(	(	kIx(	(
<g/>
Dvojspolek	dvojspolek	k1gInSc4	dvojspolek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
připojila	připojit	k5eAaPmAgFnS	připojit
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
Trojspolek	trojspolek	k1gInSc1	trojspolek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
anektovalo	anektovat	k5eAaBmAgNnS	anektovat
Bosnu	Bosna	k1gFnSc4	Bosna
a	a	k8xC	a
Hercegovinu	Hercegovina	k1gFnSc4	Hercegovina
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
napětí	napětí	k1gNnSc1	napětí
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Srbskem	Srbsko	k1gNnSc7	Srbsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Anexe	anexe	k1gFnSc1	anexe
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
Srbskem	Srbsko	k1gNnSc7	Srbsko
a	a	k8xC	a
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
se	se	k3xPyFc4	se
celkově	celkově	k6eAd1	celkově
stupňovalo	stupňovat	k5eAaImAgNnS	stupňovat
napětí	napětí	k1gNnSc1	napětí
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1914	[number]	k4	1914
spáchali	spáchat	k5eAaPmAgMnP	spáchat
srbští	srbský	k2eAgMnPc1d1	srbský
nacionalisté	nacionalista	k1gMnPc1	nacionalista
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
Františka	František	k1gMnSc4	František
Ferdinanda	Ferdinand	k1gMnSc4	Ferdinand
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
<g/>
,	,	kIx,	,
zamýšleného	zamýšlený	k2eAgMnSc4d1	zamýšlený
následníka	následník	k1gMnSc4	následník
Františka	František	k1gMnSc4	František
Josefa	Josef	k1gMnSc4	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
poté	poté	k6eAd1	poté
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Srbsku	Srbsko	k1gNnSc3	Srbsko
ultimátum	ultimátum	k1gNnSc4	ultimátum
a	a	k8xC	a
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Srbsko	Srbsko	k1gNnSc1	Srbsko
přijalo	přijmout	k5eAaPmAgNnS	přijmout
většinu	většina	k1gFnSc4	většina
jeho	jeho	k3xOp3gMnPc2	jeho
požadavků	požadavek	k1gInPc2	požadavek
<g/>
,	,	kIx,	,
říše	říše	k1gFnSc1	říše
reagovala	reagovat	k5eAaBmAgFnS	reagovat
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
předchozích	předchozí	k2eAgFnPc2d1	předchozí
smluv	smlouva	k1gFnPc2	smlouva
následovala	následovat	k5eAaImAgFnS	následovat
řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnSc1	reakce
ostatních	ostatní	k2eAgInPc2d1	ostatní
států	stát	k1gInPc2	stát
a	a	k8xC	a
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
rozhořela	rozhořet	k5eAaPmAgFnS	rozhořet
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
historika	historik	k1gMnSc2	historik
Franze	Franze	k1gFnSc2	Franze
Herreho	Herre	k1gMnSc2	Herre
si	se	k3xPyFc3	se
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
válku	válka	k1gFnSc4	válka
nepřál	přát	k5eNaImAgMnS	přát
<g/>
,	,	kIx,	,
respektoval	respektovat	k5eAaImAgMnS	respektovat
ale	ale	k9	ale
názor	názor	k1gInSc4	názor
svého	svůj	k3xOyFgMnSc2	svůj
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Berchtolda	Berchtold	k1gMnSc2	Berchtold
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
stoupenců	stoupenec	k1gMnPc2	stoupenec
válečného	válečný	k2eAgInSc2d1	válečný
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
.	.	kIx.	.
<g/>
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
se	se	k3xPyFc4	se
angažovalo	angažovat	k5eAaBmAgNnS	angažovat
především	především	k9	především
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
na	na	k7c6	na
srbské	srbský	k2eAgFnSc6d1	Srbská
frontě	fronta	k1gFnSc6	fronta
a	a	k8xC	a
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
haličské	haličský	k2eAgFnSc6d1	Haličská
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
válčilo	válčit	k5eAaImAgNnS	válčit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Němci	Němec	k1gMnSc3	Němec
proti	proti	k7c3	proti
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
nedařilo	dařit	k5eNaImAgNnS	dařit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
částečnému	částečný	k2eAgInSc3d1	částečný
průlomu	průlom	k1gInSc3	průlom
na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
frontě	fronta	k1gFnSc6	fronta
došlo	dojít	k5eAaPmAgNnS	dojít
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Němců	Němec	k1gMnPc2	Němec
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1915	[number]	k4	1915
a	a	k8xC	a
Srbsko	Srbsko	k1gNnSc1	Srbsko
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
porazit	porazit	k5eAaPmF	porazit
až	až	k9	až
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tady	tady	k6eAd1	tady
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
rakouská	rakouský	k2eAgFnSc1d1	rakouská
armáda	armáda	k1gFnSc1	armáda
neobešla	obešnout	k5eNaPmAgFnS	obešnout
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
té	ten	k3xDgFnSc2	ten
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
1915	[number]	k4	1915
museli	muset	k5eAaImAgMnP	muset
navíc	navíc	k6eAd1	navíc
Rakušené	Rakušený	k2eAgNnSc1d1	Rakušený
bojovat	bojovat	k5eAaImF	bojovat
i	i	k9	i
proti	proti	k7c3	proti
Itálii	Itálie	k1gFnSc3	Itálie
na	na	k7c6	na
italské	italský	k2eAgFnSc6d1	italská
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
projevily	projevit	k5eAaPmAgFnP	projevit
všechny	všechen	k3xTgInPc4	všechen
jeho	jeho	k3xOp3gInPc4	jeho
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
problémů	problém	k1gInPc2	problém
byla	být	k5eAaImAgFnS	být
národní	národní	k2eAgFnSc1d1	národní
nejednotnost	nejednotnost	k1gFnSc1	nejednotnost
a	a	k8xC	a
špatná	špatný	k2eAgFnSc1d1	špatná
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
válka	válka	k1gFnSc1	válka
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4	Rakousko-Uhersko
vyčerpávala	vyčerpávat	k5eAaImAgFnS	vyčerpávat
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
nuceno	nucen	k2eAgNnSc1d1	nuceno
se	se	k3xPyFc4	se
orientovat	orientovat	k5eAaBmF	orientovat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
na	na	k7c4	na
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vzbuzovalo	vzbuzovat	k5eAaImAgNnS	vzbuzovat
nelibost	nelibost	k1gFnSc4	nelibost
u	u	k7c2	u
neněmeckých	německý	k2eNgInPc2d1	neněmecký
národů	národ	k1gInPc2	národ
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
<g/>
Císař	Císař	k1gMnSc1	Císař
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
uprostřed	uprostřed	k7c2	uprostřed
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1916	[number]	k4	1916
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
těžkou	těžký	k2eAgFnSc4d1	těžká
nemoc	nemoc	k1gFnSc4	nemoc
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
do	do	k7c2	do
posledních	poslední	k2eAgInPc2d1	poslední
okamžiků	okamžik	k1gInPc2	okamžik
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
Karel	Karla	k1gFnPc2	Karla
I.	I.	kA	I.
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
politika	politika	k1gFnSc1	politika
však	však	k9	však
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
najít	najít	k5eAaPmF	najít
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
novým	nový	k2eAgFnPc3d1	nová
národním	národní	k2eAgFnPc3d1	národní
snahám	snaha	k1gFnPc3	snaha
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
<g/>
,	,	kIx,	,
oslabené	oslabený	k2eAgNnSc1d1	oslabené
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
začal	začít	k5eAaPmAgMnS	začít
jeho	jeho	k3xOp3gMnSc1	jeho
prastrýc	prastrýc	k1gMnSc1	prastrýc
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Povaha	povaha	k1gFnSc1	povaha
a	a	k8xC	a
životní	životní	k2eAgInSc1d1	životní
styl	styl	k1gInSc1	styl
==	==	k?	==
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
byl	být	k5eAaImAgMnS	být
velice	velice	k6eAd1	velice
pracovitý	pracovitý	k2eAgMnSc1d1	pracovitý
<g/>
,	,	kIx,	,
vstával	vstávat	k5eAaImAgMnS	vstávat
časně	časně	k6eAd1	časně
–	–	k?	–
již	již	k6eAd1	již
v	v	k7c4	v
pět	pět	k4xCc4	pět
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
ve	v	k7c4	v
čtyři	čtyři	k4xCgFnPc4	čtyři
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
snídani	snídaně	k1gFnSc3	snídaně
míval	mívat	k5eAaImAgMnS	mívat
rohlíky	rohlík	k1gInPc4	rohlík
se	s	k7c7	s
šunkou	šunka	k1gFnSc7	šunka
a	a	k8xC	a
máslem	máslo	k1gNnSc7	máslo
<g/>
.	.	kIx.	.
</s>
<s>
Podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
dne	den	k1gInSc2	den
trávil	trávit	k5eAaImAgInS	trávit
vyřizováním	vyřizování	k1gNnSc7	vyřizování
spisů	spis	k1gInPc2	spis
za	za	k7c7	za
stolem	stol	k1gInSc7	stol
své	svůj	k3xOyFgFnSc2	svůj
pracovny	pracovna	k1gFnSc2	pracovna
<g/>
,	,	kIx,	,
dokumenty	dokument	k1gInPc4	dokument
často	často	k6eAd1	často
četl	číst	k5eAaImAgMnS	číst
i	i	k9	i
během	během	k7c2	během
oběda	oběd	k1gInSc2	oběd
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
mu	on	k3xPp3gNnSc3	on
servírovali	servírovat	k5eAaBmAgMnP	servírovat
vařené	vařený	k2eAgNnSc4d1	vařené
hovězí	hovězí	k1gNnSc4	hovězí
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
přírodní	přírodní	k2eAgInSc4d1	přírodní
řízek	řízek	k1gInSc4	řízek
či	či	k8xC	či
guláš	guláš	k1gInSc4	guláš
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hlavnímu	hlavní	k2eAgNnSc3d1	hlavní
dennímu	denní	k2eAgNnSc3d1	denní
jídlu	jídlo	k1gNnSc3	jídlo
si	se	k3xPyFc3	se
nechával	nechávat	k5eAaImAgMnS	nechávat
podávat	podávat	k5eAaImF	podávat
pivo	pivo	k1gNnSc4	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
navštívil	navštívit	k5eAaPmAgInS	navštívit
plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
pivovar	pivovar	k1gInSc1	pivovar
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pil	pít	k5eAaImAgInS	pít
výhradně	výhradně	k6eAd1	výhradně
plzeňské	plzeňský	k2eAgNnSc4d1	plzeňské
pivo	pivo	k1gNnSc4	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
šlo	jít	k5eAaImAgNnS	jít
<g/>
,	,	kIx,	,
večeřel	večeřet	k5eAaImAgMnS	večeřet
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
<g/>
,	,	kIx,	,
spát	spát	k5eAaImF	spát
chodil	chodit	k5eAaImAgMnS	chodit
už	už	k9	už
v	v	k7c4	v
devět	devět	k4xCc4	devět
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Rád	rád	k6eAd1	rád
se	se	k3xPyFc4	se
oblékal	oblékat	k5eAaImAgMnS	oblékat
do	do	k7c2	do
uniformy	uniforma	k1gFnSc2	uniforma
<g/>
,	,	kIx,	,
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gInSc2	jeho
důrazu	důraz	k1gInSc2	důraz
na	na	k7c4	na
formální	formální	k2eAgInSc4d1	formální
způsob	způsob	k1gInSc4	způsob
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
stejné	stejný	k2eAgInPc4d1	stejný
způsoby	způsob	k1gInPc4	způsob
očekával	očekávat	k5eAaImAgInS	očekávat
i	i	k9	i
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
jeho	on	k3xPp3gInSc2	on
programu	program	k1gInSc2	program
byly	být	k5eAaImAgFnP	být
audience	audience	k1gFnPc1	audience
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yRgFnPc2	který
ho	on	k3xPp3gNnSc2	on
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
jeho	on	k3xPp3gNnSc2	on
panování	panování	k1gNnSc2	panování
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
Franze	Franze	k1gFnSc2	Franze
Herreho	Herre	k1gMnSc2	Herre
měl	mít	k5eAaImAgMnS	mít
císař	císař	k1gMnSc1	císař
citlivou	citlivý	k2eAgFnSc4d1	citlivá
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
introvertní	introvertní	k2eAgFnSc4d1	introvertní
povahu	povaha	k1gFnSc4	povaha
<g/>
,	,	kIx,	,
vlivem	vliv	k1gInSc7	vliv
své	svůj	k3xOyFgFnSc2	svůj
výchovy	výchova	k1gFnSc2	výchova
se	se	k3xPyFc4	se
ale	ale	k9	ale
naučil	naučit	k5eAaPmAgMnS	naučit
nedávat	dávat	k5eNaImF	dávat
city	cit	k1gInPc4	cit
najevo	najevo	k6eAd1	najevo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
jednal	jednat	k5eAaImAgMnS	jednat
zdvořile	zdvořile	k6eAd1	zdvořile
<g/>
,	,	kIx,	,
např.	např.	kA	např.
nerad	nerad	k2eAgInSc1d1	nerad
používal	používat	k5eAaImAgInS	používat
rozkazovací	rozkazovací	k2eAgInSc1d1	rozkazovací
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
něj	on	k3xPp3gMnSc2	on
používal	používat	k5eAaImAgInS	používat
slova	slovo	k1gNnPc4	slovo
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
prosím	prosit	k5eAaImIp1nS	prosit
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
doufám	doufat	k5eAaImIp1nS	doufat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
ale	ale	k8xC	ale
uměl	umět	k5eAaImAgMnS	umět
dát	dát	k5eAaPmF	dát
jasně	jasně	k6eAd1	jasně
najevo	najevo	k6eAd1	najevo
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
dochvilný	dochvilný	k2eAgInSc4d1	dochvilný
<g/>
,	,	kIx,	,
šetrný	šetrný	k2eAgInSc4d1	šetrný
a	a	k8xC	a
konzervativní	konzervativní	k2eAgInSc4d1	konzervativní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějším	pozdní	k2eAgInSc6d2	pozdější
věku	věk	k1gInSc6	věk
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
projevovalo	projevovat	k5eAaImAgNnS	projevovat
odmítáním	odmítání	k1gNnSc7	odmítání
nových	nový	k2eAgInPc2d1	nový
vynálezů	vynález	k1gInPc2	vynález
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
tak	tak	k9	tak
nepoužíval	používat	k5eNaImAgMnS	používat
telefon	telefon	k1gInSc4	telefon
<g/>
,	,	kIx,	,
výtahem	výtah	k1gInSc7	výtah
nejezdil	jezdit	k5eNaImAgInS	jezdit
vůbec	vůbec	k9	vůbec
a	a	k8xC	a
automobilem	automobil	k1gInSc7	automobil
zcela	zcela	k6eAd1	zcela
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
povinnosti	povinnost	k1gFnPc4	povinnost
panovníka	panovník	k1gMnSc2	panovník
plnil	plnit	k5eAaImAgInS	plnit
až	až	k9	až
do	do	k7c2	do
pokročilého	pokročilý	k2eAgInSc2d1	pokročilý
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ještě	ještě	k9	ještě
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
79	[number]	k4	79
letech	léto	k1gNnPc6	léto
sledoval	sledovat	k5eAaImAgMnS	sledovat
vojenské	vojenský	k2eAgInPc4d1	vojenský
manévry	manévr	k1gInPc4	manévr
a	a	k8xC	a
v	v	k7c6	v
80	[number]	k4	80
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
naposledy	naposledy	k6eAd1	naposledy
<g/>
,	,	kIx,	,
seděl	sedět	k5eAaImAgMnS	sedět
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
při	při	k7c6	při
přehlídce	přehlídka	k1gFnSc6	přehlídka
<g/>
.	.	kIx.	.
</s>
<s>
Jezdit	jezdit	k5eAaImF	jezdit
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
uměl	umět	k5eAaImAgMnS	umět
výborně	výborně	k6eAd1	výborně
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
intenzivně	intenzivně	k6eAd1	intenzivně
věnoval	věnovat	k5eAaImAgMnS	věnovat
lovu	lov	k1gInSc3	lov
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1848	[number]	k4	1848
<g/>
–	–	k?	–
<g/>
1899	[number]	k4	1899
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
jeho	jeho	k3xOp3gNnSc1	jeho
kořistí	kořistit	k5eAaImIp3nS	kořistit
48	[number]	k4	48
345	[number]	k4	345
kusů	kus	k1gInPc2	kus
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc4d1	jiná
záliby	záliba	k1gFnPc4	záliba
neměl	mít	k5eNaImAgMnS	mít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodinný	rodinný	k2eAgInSc1d1	rodinný
život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Františka	František	k1gMnSc2	František
Karla	Karel	k1gMnSc2	Karel
(	(	kIx(	(
<g/>
mladšího	mladý	k2eAgMnSc2d2	mladší
syna	syn	k1gMnSc2	syn
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
ženy	žena	k1gFnPc1	žena
Žofie	Žofie	k1gFnSc2	Žofie
<g/>
,	,	kIx,	,
bavorské	bavorský	k2eAgFnSc2d1	bavorská
princezny	princezna	k1gFnSc2	princezna
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
čtyři	čtyři	k4xCgMnPc4	čtyři
sourozence	sourozenec	k1gMnPc4	sourozenec
<g/>
:	:	kIx,	:
Maxmiliána	Maxmilián	k1gMnSc4	Maxmilián
<g/>
,	,	kIx,	,
Karla	Karel	k1gMnSc4	Karel
Ludvíka	Ludvík	k1gMnSc4	Ludvík
,	,	kIx,	,
Marii	Maria	k1gFnSc4	Maria
Annu	Anna	k1gFnSc4	Anna
(	(	kIx(	(
<g/>
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ludvíka	Ludvík	k1gMnSc4	Ludvík
Viktora	Viktor	k1gMnSc4	Viktor
<g/>
.	.	kIx.	.
</s>
<s>
Františkův	Františkův	k2eAgInSc1d1	Františkův
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
liberálněji	liberálně	k6eAd2	liberálně
smýšlejícím	smýšlející	k2eAgMnSc7d1	smýšlející
a	a	k8xC	a
ambiciózním	ambiciózní	k2eAgMnSc7d1	ambiciózní
bratrem	bratr	k1gMnSc7	bratr
Maxmiliánem	Maxmilián	k1gMnSc7	Maxmilián
byl	být	k5eAaImAgMnS	být
poměrně	poměrně	k6eAd1	poměrně
napjatý	napjatý	k2eAgMnSc1d1	napjatý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jinak	jinak	k6eAd1	jinak
měl	mít	k5eAaImAgMnS	mít
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
rád	rád	k6eAd1	rád
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ale	ale	k9	ale
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
přijal	přijmout	k5eAaPmAgMnS	přijmout
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
nabídku	nabídka	k1gFnSc4	nabídka
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
mexickým	mexický	k2eAgMnSc7d1	mexický
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
ho	on	k3xPp3gMnSc4	on
musel	muset	k5eAaImAgMnS	muset
donutit	donutit	k5eAaPmF	donutit
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
důvodů	důvod	k1gInPc2	důvod
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
trůnu	trůn	k1gInSc2	trůn
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Maxmiliánovo	Maxmiliánův	k2eAgNnSc1d1	Maxmiliánovo
zastřelení	zastřelení	k1gNnSc1	zastřelení
republikány	republikán	k1gMnPc7	republikán
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
Františka	František	k1gMnSc4	František
hodně	hodně	k6eAd1	hodně
dotklo	dotknout	k5eAaPmAgNnS	dotknout
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
se	se	k3xPyFc4	se
v	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
rezidenci	rezidence	k1gFnSc6	rezidence
v	v	k7c6	v
Bad	Bad	k1gMnSc6	Bad
Ischlu	Ischl	k1gInSc2	Ischl
seznámil	seznámit	k5eAaPmAgInS	seznámit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
patnáctiletou	patnáctiletý	k2eAgFnSc7d1	patnáctiletá
sestřenicí	sestřenice	k1gFnSc7	sestřenice
Alžbětou	Alžběta	k1gFnSc7	Alžběta
(	(	kIx(	(
<g/>
známou	známý	k2eAgFnSc7d1	známá
také	také	k9	také
jako	jako	k8xC	jako
Sissi	Sisse	k1gFnSc4	Sisse
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
byla	být	k5eAaImAgFnS	být
druhá	druhý	k4xOgFnSc1	druhý
dcera	dcera	k1gFnSc1	dcera
bavorského	bavorský	k2eAgMnSc2d1	bavorský
vévody	vévoda	k1gMnSc2	vévoda
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
Josefa	Josef	k1gMnSc2	Josef
a	a	k8xC	a
Ludoviky	Ludovika	k1gFnSc2	Ludovika
Vilhelmíny	Vilhelmína	k1gFnSc2	Vilhelmína
(	(	kIx(	(
<g/>
dcery	dcera	k1gFnSc2	dcera
bavorského	bavorský	k2eAgMnSc2d1	bavorský
krále	král	k1gMnSc2	král
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
I.	I.	kA	I.
a	a	k8xC	a
sestry	sestra	k1gFnPc1	sestra
Žofie	Žofie	k1gFnSc2	Žofie
<g/>
,	,	kIx,	,
matky	matka	k1gFnPc1	matka
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
se	se	k3xPyFc4	se
do	do	k7c2	do
Alžběty	Alžběta	k1gFnSc2	Alžběta
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
a	a	k8xC	a
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1854	[number]	k4	1854
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
v	v	k7c6	v
augustiniánském	augustiniánský	k2eAgInSc6d1	augustiniánský
kostele	kostel	k1gInSc6	kostel
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brzy	brzy	k6eAd1	brzy
přišly	přijít	k5eAaPmAgFnP	přijít
na	na	k7c4	na
svět	svět	k1gInSc4	svět
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
po	po	k7c6	po
desetileté	desetiletý	k2eAgFnSc6d1	desetiletá
pauze	pauza	k1gFnSc6	pauza
pak	pak	k6eAd1	pak
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Žofie	Žofie	k1gFnSc1	Žofie
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
–	–	k?	–
<g/>
1857	[number]	k4	1857
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
letech	let	k1gInPc6	let
na	na	k7c4	na
černý	černý	k2eAgInSc4d1	černý
kašel	kašel	k1gInSc4	kašel
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
po	po	k7c6	po
Uherském	uherský	k2eAgNnSc6d1	Uherské
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gisela	Gisela	k1gFnSc1	Gisela
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
–	–	k?	–
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
provdána	provdán	k2eAgFnSc1d1	provdána
za	za	k7c2	za
bavorského	bavorský	k2eAgMnSc2d1	bavorský
prince	princ	k1gMnSc2	princ
Leopolda	Leopold	k1gMnSc2	Leopold
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
<g/>
–	–	k?	–
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Rudolf	Rudolf	k1gMnSc1	Rudolf
směl	smět	k5eAaImAgMnS	smět
po	po	k7c6	po
opakovaných	opakovaný	k2eAgFnPc6d1	opakovaná
intervencích	intervence	k1gFnPc6	intervence
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
Alžběty	Alžběta	k1gFnSc2	Alžběta
u	u	k7c2	u
císaře	císař	k1gMnSc2	císař
ukončit	ukončit	k5eAaPmF	ukončit
striktně	striktně	k6eAd1	striktně
armádní	armádní	k2eAgFnSc4d1	armádní
výchovu	výchova	k1gFnSc4	výchova
<g/>
,	,	kIx,	,
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
přírodním	přírodní	k2eAgFnPc3d1	přírodní
vědám	věda	k1gFnPc3	věda
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
také	také	k9	také
jako	jako	k9	jako
žurnalista	žurnalista	k1gMnSc1	žurnalista
v	v	k7c6	v
liberálním	liberální	k2eAgInSc6d1	liberální
tisku	tisk	k1gInSc6	tisk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
anonymně	anonymně	k6eAd1	anonymně
a	a	k8xC	a
za	za	k7c7	za
zády	záda	k1gNnPc7	záda
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
izoloval	izolovat	k5eAaBmAgMnS	izolovat
korunního	korunní	k2eAgMnSc4d1	korunní
prince	princ	k1gMnSc4	princ
Rudolfa	Rudolf	k1gMnSc4	Rudolf
od	od	k7c2	od
státních	státní	k2eAgFnPc2d1	státní
záležitostí	záležitost	k1gFnPc2	záležitost
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
to	ten	k3xDgNnSc4	ten
těžce	těžce	k6eAd1	těžce
nesl	nést	k5eAaImAgMnS	nést
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
císařovo	císařův	k2eAgNnSc4d1	císařovo
přání	přání	k1gNnSc4	přání
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
princeznou	princezna	k1gFnSc7	princezna
Stephanií	Stephanie	k1gFnSc7	Stephanie
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
belgického	belgický	k2eAgMnSc4d1	belgický
krále	král	k1gMnSc4	král
Leopolda	Leopold	k1gMnSc2	Leopold
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
spáchal	spáchat	k5eAaPmAgMnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
si	se	k3xPyFc3	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
jeho	jeho	k3xOp3gNnSc2	jeho
otec	otec	k1gMnSc1	otec
nerozuměl	rozumět	k5eNaImAgMnS	rozumět
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
smrt	smrt	k1gFnSc1	smrt
ho	on	k3xPp3gInSc4	on
zdrtila	zdrtit	k5eAaPmAgFnS	zdrtit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Valerie	Valerie	k1gFnSc1	Valerie
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
–	–	k?	–
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	s	k7c7	s
10	[number]	k4	10
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
korunování	korunování	k1gNnSc6	korunování
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Vzala	vzít	k5eAaPmAgFnS	vzít
si	se	k3xPyFc3	se
arcivévodu	arcivévoda	k1gMnSc4	arcivévoda
Františka	František	k1gMnSc4	František
Salvátora	Salvátor	k1gMnSc4	Salvátor
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
její	její	k3xOp3gFnSc3	její
dobročinnosti	dobročinnost	k1gFnSc3	dobročinnost
jí	on	k3xPp3gFnSc3	on
lidé	člověk	k1gMnPc1	člověk
přezdívali	přezdívat	k5eAaImAgMnP	přezdívat
"	"	kIx"	"
<g/>
Anděl	Anděl	k1gMnSc1	Anděl
z	z	k7c2	z
Wallsee	Wallse	k1gFnSc2	Wallse
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
manželi	manžel	k1gMnPc7	manžel
později	pozdě	k6eAd2	pozdě
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
odcizení	odcizení	k1gNnSc3	odcizení
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
měl	mít	k5eAaImAgInS	mít
poměr	poměr	k1gInSc1	poměr
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Nahowskou	Nahowský	k2eAgFnSc7d1	Nahowský
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
údajně	údajně	k6eAd1	údajně
zplodil	zplodit	k5eAaPmAgMnS	zplodit
i	i	k8xC	i
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
udržoval	udržovat	k5eAaImAgInS	udržovat
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
Kateřinou	Kateřina	k1gFnSc7	Kateřina
Schrattovou	Schrattová	k1gFnSc7	Schrattová
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
syna	syn	k1gMnSc2	syn
cestovala	cestovat	k5eAaImAgFnS	cestovat
císařovna	císařovna	k1gFnSc1	císařovna
Alžběta	Alžběta	k1gFnSc1	Alžběta
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1898	[number]	k4	1898
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
jezera	jezero	k1gNnSc2	jezero
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
zavraždil	zavraždit	k5eAaPmAgMnS	zavraždit
pilníkem	pilník	k1gInSc7	pilník
italský	italský	k2eAgMnSc1d1	italský
anarchista	anarchista	k1gMnSc1	anarchista
Luigi	Luigi	k1gNnSc4	Luigi
Lucheni	Luchen	k2eAgMnPc1d1	Luchen
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
moc	moc	k6eAd1	moc
často	často	k6eAd1	často
nevídal	vídat	k5eNaImAgMnS	vídat
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
ji	on	k3xPp3gFnSc4	on
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
stále	stále	k6eAd1	stále
rád	rád	k6eAd1	rád
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
smrt	smrt	k1gFnSc1	smrt
ho	on	k3xPp3gInSc4	on
velmi	velmi	k6eAd1	velmi
zarmoutila	zarmoutit	k5eAaPmAgFnS	zarmoutit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hodnocení	hodnocení	k1gNnSc1	hodnocení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Tituly	titul	k1gInPc4	titul
Františka	František	k1gMnSc4	František
Josefa	Josef	k1gMnSc2	Josef
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
císařské	císařský	k2eAgNnSc1d1	císařské
a	a	k8xC	a
královské	královský	k2eAgNnSc1d1	královské
Apoštolské	apoštolský	k2eAgNnSc1d1	apoštolské
veličenstvo	veličenstvo	k1gNnSc1	veličenstvo
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
<g/>
z	z	k7c2	z
Boží	boží	k2eAgFnSc2d1	boží
vůle	vůle	k1gFnSc2	vůle
císař	císař	k1gMnSc1	císař
rakouský	rakouský	k2eAgMnSc1d1	rakouský
<g/>
,	,	kIx,	,
<g/>
král	král	k1gMnSc1	král
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
uherský	uherský	k2eAgMnSc1d1	uherský
<g/>
,	,	kIx,	,
lombardský	lombardský	k2eAgMnSc1d1	lombardský
a	a	k8xC	a
benátský	benátský	k2eAgMnSc1d1	benátský
<g/>
,	,	kIx,	,
dalmatský	dalmatský	k2eAgMnSc1d1	dalmatský
<g/>
,	,	kIx,	,
chorvatský	chorvatský	k2eAgMnSc1d1	chorvatský
<g/>
,	,	kIx,	,
slavonský	slavonský	k2eAgMnSc1d1	slavonský
<g/>
,	,	kIx,	,
haličský	haličský	k2eAgMnSc1d1	haličský
<g/>
,	,	kIx,	,
vladimiřský	vladimiřský	k2eAgMnSc1d1	vladimiřský
a	a	k8xC	a
illyrský	illyrský	k2eAgMnSc1d1	illyrský
<g/>
;	;	kIx,	;
král	král	k1gMnSc1	král
jeruzalémský	jeruzalémský	k2eAgMnSc1d1	jeruzalémský
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
rakouský	rakouský	k2eAgMnSc1d1	rakouský
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
velkovévoda	velkovévoda	k1gMnSc1	velkovévoda
toskánský	toskánský	k2eAgMnSc1d1	toskánský
a	a	k8xC	a
krakovský	krakovský	k2eAgMnSc1d1	krakovský
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
vévoda	vévoda	k1gMnSc1	vévoda
lotrinský	lotrinský	k2eAgMnSc1d1	lotrinský
<g/>
,	,	kIx,	,
salcburský	salcburský	k2eAgMnSc1d1	salcburský
<g/>
,	,	kIx,	,
štyrský	štyrský	k2eAgMnSc1d1	štyrský
<g/>
,	,	kIx,	,
korutanský	korutanský	k2eAgMnSc1d1	korutanský
<g/>
,	,	kIx,	,
kraňský	kraňský	k2eAgMnSc1d1	kraňský
a	a	k8xC	a
bukovinský	bukovinský	k2eAgMnSc1d1	bukovinský
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
velkokníže	velkokníže	k1gMnSc1	velkokníže
sedmihradský	sedmihradský	k2eAgMnSc1d1	sedmihradský
<g/>
,	,	kIx,	,
markrabě	markrabě	k1gMnSc1	markrabě
moravský	moravský	k2eAgMnSc1d1	moravský
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
vévoda	vévoda	k1gMnSc1	vévoda
horno-	horno-	k?	horno-
a	a	k8xC	a
dolnoslezský	dolnoslezský	k2eAgInSc4d1	dolnoslezský
<g/>
,	,	kIx,	,
modenský	modenský	k2eAgInSc4d1	modenský
<g/>
,	,	kIx,	,
parmský	parmský	k2eAgInSc4d1	parmský
<g/>
,	,	kIx,	,
piacenzský	piacenzský	k2eAgInSc4d1	piacenzský
a	a	k8xC	a
guastalský	guastalský	k2eAgInSc4d1	guastalský
<g/>
,	,	kIx,	,
osvětimský	osvětimský	k2eAgInSc4d1	osvětimský
a	a	k8xC	a
zátorský	zátorský	k2eAgInSc4d1	zátorský
<g/>
,	,	kIx,	,
těšínský	těšínský	k2eAgInSc4d1	těšínský
<g/>
,	,	kIx,	,
furlanský	furlanský	k2eAgInSc4d1	furlanský
<g/>
,	,	kIx,	,
dubrovnický	dubrovnický	k2eAgInSc4d1	dubrovnický
a	a	k8xC	a
zadarský	zadarský	k2eAgInSc4d1	zadarský
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
okněžněný	okněžněný	k2eAgMnSc1d1	okněžněný
hrabě	hrabě	k1gMnSc1	hrabě
habsburský	habsburský	k2eAgMnSc1d1	habsburský
<g/>
,	,	kIx,	,
tyrolský	tyrolský	k2eAgMnSc1d1	tyrolský
<g/>
,	,	kIx,	,
kyburský	kyburský	k2eAgMnSc1d1	kyburský
<g/>
,	,	kIx,	,
goricijský	goricijský	k2eAgMnSc1d1	goricijský
a	a	k8xC	a
gradišťský	gradišťský	k2eAgMnSc1d1	gradišťský
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
kníže	kníže	k1gMnSc1	kníže
tridentský	tridentský	k2eAgMnSc1d1	tridentský
a	a	k8xC	a
brixenský	brixenský	k2eAgMnSc1d1	brixenský
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
markrabě	markrabě	k1gMnSc1	markrabě
horno-	horno-	k?	horno-
a	a	k8xC	a
dolnolužický	dolnolužický	k2eAgInSc1d1	dolnolužický
a	a	k8xC	a
istrijský	istrijský	k2eAgInSc1d1	istrijský
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
hrabě	hrabě	k1gMnSc1	hrabě
hohenembský	hohenembský	k2eAgMnSc1d1	hohenembský
<g/>
,	,	kIx,	,
feldkirchský	feldkirchský	k2eAgMnSc1d1	feldkirchský
<g/>
,	,	kIx,	,
břežnický	břežnický	k2eAgMnSc1d1	břežnický
<g/>
,	,	kIx,	,
sonnenberský	sonnenberský	k2eAgMnSc1d1	sonnenberský
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
pán	pán	k1gMnSc1	pán
terstský	terstský	k2eAgMnSc1d1	terstský
<g/>
,	,	kIx,	,
kotorský	kotorský	k2eAgMnSc1d1	kotorský
a	a	k8xC	a
na	na	k7c6	na
Slovinském	slovinský	k2eAgNnSc6d1	slovinské
Krajišti	krajiště	k1gNnSc6	krajiště
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
velkovojvoda	velkovojvoda	k1gMnSc1	velkovojvoda
srbský	srbský	k2eAgMnSc1d1	srbský
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
===	===	k?	===
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Franz	Franz	k1gMnSc1	Franz
Joseph	Joseph	k1gMnSc1	Joseph
I.	I.	kA	I.
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
Ф	Ф	k?	Ф
И	И	k?	И
I	I	kA	I
(	(	kIx(	(
<g/>
Franc	Franc	k1gMnSc1	Franc
Iosif	Iosif	k1gMnSc1	Iosif
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
:	:	kIx,	:
Ferenc	Ferenc	k1gMnSc1	Ferenc
József	József	k1gMnSc1	József
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
Franciscus	Franciscus	k1gMnSc1	Franciscus
Iosephus	Iosephus	k1gMnSc1	Iosephus
I.	I.	kA	I.
chorvatsky	chorvatsky	k6eAd1	chorvatsky
<g/>
:	:	kIx,	:
Franjo	Franjo	k1gMnSc1	Franjo
Josip	Josip	k1gMnSc1	Josip
<g/>
,	,	kIx,	,
slovensky	slovensky	k6eAd1	slovensky
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Jozef	Jozef	k1gMnSc1	Jozef
I.	I.	kA	I.
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Francesco	Francesca	k1gMnSc5	Francesca
Giuseppe	Giusepp	k1gMnSc5	Giusepp
I	i	k9	i
<g/>
,	,	kIx,	,
rumunsky	rumunsky	k6eAd1	rumunsky
<g/>
:	:	kIx,	:
Francisc	Francisc	k1gFnSc1	Francisc
Iosif	Iosif	k1gInSc1	Iosif
<g/>
,	,	kIx,	,
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
:	:	kIx,	:
Ф	Ф	k?	Ф
Й	Й	k?	Й
(	(	kIx(	(
<g/>
Franc	Franc	k1gMnSc1	Franc
Josyp	Josyp	k1gMnSc1	Josyp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srbsky	srbsky	k6eAd1	srbsky
<g/>
:	:	kIx,	:
Ф	Ф	k?	Ф
Ј	Ј	k?	Ј
/	/	kIx~	/
Franjo	Franjo	k1gMnSc1	Franjo
Josip	Josip	k1gMnSc1	Josip
<g/>
,	,	kIx,	,
slovinsky	slovinsky	k6eAd1	slovinsky
<g/>
:	:	kIx,	:
Franc	Franc	k1gMnSc1	Franc
Jožef	Jožef	k1gMnSc1	Jožef
<g/>
,	,	kIx,	,
furlansky	furlansky	k6eAd1	furlansky
<g/>
:	:	kIx,	:
Francesc	Francesc	k1gFnSc1	Francesc
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
<g/>
:	:	kIx,	:
Franciszek	Franciszek	k1gInSc1	Franciszek
Józef	Józef	k1gInSc1	Józef
I	I	kA	I
<g/>
,	,	kIx,	,
španělsky	španělsky	k6eAd1	španělsky
<g/>
:	:	kIx,	:
Francisco	Francisco	k6eAd1	Francisco
José	Josý	k2eAgFnPc1d1	Josý
I	i	k9	i
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
François	François	k1gFnSc1	François
Joseph	Joseph	k1gInSc1	Joseph
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Francis	Francis	k1gFnSc1	Francis
Joseph	Joseph	k1gMnSc1	Joseph
I.	I.	kA	I.
</s>
</p>
<p>
<s>
===	===	k?	===
"	"	kIx"	"
<g/>
Starej	starat	k5eAaImRp2nS	starat
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
"	"	kIx"	"
===	===	k?	===
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
Češi	Čech	k1gMnPc1	Čech
titulovali	titulovat	k5eAaImAgMnP	titulovat
císaře	císař	k1gMnSc4	císař
Františka	František	k1gMnSc4	František
Josefa	Josef	k1gMnSc4	Josef
I.	I.	kA	I.
za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
soukromí	soukromí	k1gNnSc6	soukromí
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
)	)	kIx)	)
posměšnou	posměšný	k2eAgFnSc7d1	posměšná
přezdívkou	přezdívka	k1gFnSc7	přezdívka
"	"	kIx"	"
<g/>
Starej	starat	k5eAaImRp2nS	starat
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
ji	on	k3xPp3gFnSc4	on
měl	mít	k5eAaImAgMnS	mít
podle	podle	k7c2	podle
rozšířené	rozšířený	k2eAgFnSc2d1	rozšířená
legendy	legenda	k1gFnSc2	legenda
získat	získat	k5eAaPmF	získat
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
návštěvě	návštěva	k1gFnSc6	návštěva
Prahy	Praha	k1gFnSc2	Praha
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
na	na	k7c6	na
základě	základ	k1gInSc6	základ
údajného	údajný	k2eAgInSc2d1	údajný
popisku	popisek	k1gInSc2	popisek
fotografie	fotografia	k1gFnSc2	fotografia
v	v	k7c6	v
dobových	dobový	k2eAgFnPc6d1	dobová
novinách	novina	k1gFnPc6	novina
či	či	k8xC	či
časopise	časopis	k1gInSc6	časopis
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
otevírání	otevírání	k1gNnSc2	otevírání
Mostu	most	k1gInSc2	most
císaře	císař	k1gMnSc4	císař
Františka	František	k1gMnSc4	František
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	let	k1gInPc6	let
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
a	a	k8xC	a
pak	pak	k6eAd1	pak
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
most	most	k1gInSc1	most
Legií	legie	k1gFnPc2	legie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Popisek	popisek	k1gInSc1	popisek
měl	mít	k5eAaImAgInS	mít
znít	znít	k5eAaImF	znít
"	"	kIx"	"
<g/>
Procházka	Procházka	k1gMnSc1	Procházka
na	na	k7c6	na
mostě	most	k1gInSc6	most
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
o	o	k7c4	o
který	který	k3yRgInSc4	který
časopis	časopis	k1gInSc4	časopis
nebo	nebo	k8xC	nebo
noviny	novina	k1gFnSc2	novina
mělo	mít	k5eAaImAgNnS	mít
jít	jít	k5eAaImF	jít
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
pouze	pouze	k6eAd1	pouze
pověst	pověst	k1gFnSc4	pověst
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
nezakládá	zakládat	k5eNaImIp3nS	zakládat
na	na	k7c6	na
pravdě	pravda	k1gFnSc6	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
také	také	k9	také
dokázáno	dokázat	k5eAaPmNgNnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
čeští	český	k2eAgMnPc1d1	český
politici	politik	k1gMnPc1	politik
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
nebo	nebo	k8xC	nebo
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
přezdívku	přezdívka	k1gFnSc4	přezdívka
používali	používat	k5eAaImAgMnP	používat
již	již	k6eAd1	již
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
podporují	podporovat	k5eAaImIp3nP	podporovat
i	i	k9	i
výzkumy	výzkum	k1gInPc1	výzkum
historika	historik	k1gMnSc2	historik
Jiřího	Jiří	k1gMnSc2	Jiří
Raka	rak	k1gMnSc2	rak
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
prohlédl	prohlédnout	k5eAaPmAgMnS	prohlédnout
veškeré	veškerý	k3xTgFnPc4	veškerý
noviny	novina	k1gFnPc4	novina
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgInSc4	žádný
popisek	popisek	k1gInSc4	popisek
pod	pod	k7c7	pod
uvedenou	uvedený	k2eAgFnSc7d1	uvedená
fotografií	fotografia	k1gFnSc7	fotografia
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Archiv	archiv	k1gInSc1	archiv
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
Prahy	Praha	k1gFnSc2	Praha
z	z	k7c2	z
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
vycházela	vycházet	k5eAaImAgFnS	vycházet
jako	jako	k9	jako
sjednocené	sjednocený	k2eAgNnSc4d1	sjednocené
periodikum	periodikum	k1gNnSc4	periodikum
se	s	k7c7	s
Světozorem	světozor	k1gInSc7	světozor
a	a	k8xC	a
která	který	k3yIgFnSc1	který
ten	ten	k3xDgInSc1	ten
den	den	k1gInSc4	den
byla	být	k5eAaImAgFnS	být
věnována	věnovat	k5eAaPmNgFnS	věnovat
otevření	otevření	k1gNnSc3	otevření
nového	nový	k2eAgInSc2d1	nový
mostu	most	k1gInSc2	most
a	a	k8xC	a
návštěvě	návštěva	k1gFnSc6	návštěva
mocnáře	mocnář	k1gMnSc2	mocnář
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
fotografii	fotografia	k1gFnSc4	fotografia
s	s	k7c7	s
popiskem	popisek	k1gInSc7	popisek
"	"	kIx"	"
<g/>
Promenáda	promenáda	k1gFnSc1	promenáda
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
pražském	pražský	k2eAgInSc6d1	pražský
mostě	most	k1gInSc6	most
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
historika	historik	k1gMnSc2	historik
Otty	Otta	k1gMnSc2	Otta
Urbana	Urban	k1gMnSc2	Urban
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
přezdívka	přezdívka	k1gFnSc1	přezdívka
mnohem	mnohem	k6eAd1	mnohem
starší	starší	k1gMnPc4	starší
a	a	k8xC	a
panovník	panovník	k1gMnSc1	panovník
ji	on	k3xPp3gFnSc4	on
získal	získat	k5eAaPmAgMnS	získat
už	už	k6eAd1	už
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
příjezd	příjezd	k1gInSc4	příjezd
císařského	císařský	k2eAgInSc2d1	císařský
průvodu	průvod	k1gInSc2	průvod
ohlašoval	ohlašovat	k5eAaImAgMnS	ohlašovat
jezdec	jezdec	k1gInSc4	jezdec
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
se	se	k3xPyFc4	se
Procházka	Procházka	k1gMnSc1	Procházka
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
volali	volat	k5eAaImAgMnP	volat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Starý	starý	k2eAgMnSc1d1	starý
Procházka	Procházka	k1gMnSc1	Procházka
už	už	k6eAd1	už
jede	jet	k5eAaImIp3nS	jet
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BAHENSKÁ	BAHENSKÁ	kA	BAHENSKÁ
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
In	In	k1gMnSc1	In
<g/>
:	:	kIx,	:
RYANTOVÁ	RYANTOVÁ	kA	RYANTOVÁ
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
VOREL	Vorel	k1gMnSc1	Vorel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
940	[number]	k4	940
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
531	[number]	k4	531
<g/>
-	-	kIx~	-
<g/>
547	[number]	k4	547
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BĚLINA	Bělina	k1gMnSc1	Bělina
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
;	;	kIx,	;
BOROVIČKA	Borovička	k1gMnSc1	Borovička
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
;	;	kIx,	;
KUČERA	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
XII	XII	kA	XII
<g/>
.	.	kIx.	.
<g/>
a.	a.	k?	a.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
804	[number]	k4	804
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7432	[number]	k4	7432
<g/>
-	-	kIx~	-
<g/>
181	[number]	k4	181
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJOVÁ	ČORNEJOVÁ	kA	ČORNEJOVÁ
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
;	;	kIx,	;
RAK	rak	k1gMnSc1	rak
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
VLNAS	VLNAS	kA	VLNAS
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
tvých	tvůj	k3xOp2gNnPc2	tvůj
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grafoprint-Neubert	Grafoprint-Neubert	k1gMnSc1	Grafoprint-Neubert
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
289	[number]	k4	289
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85785	[number]	k4	85785
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
HAMANNOVÁ	HAMANNOVÁ	kA	HAMANNOVÁ
<g/>
,	,	kIx,	,
Brigitte	Brigitte	k1gFnSc1	Brigitte
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Životopisná	životopisný	k2eAgFnSc1d1	životopisná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Brána	brána	k1gFnSc1	brána
;	;	kIx,	;
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
408	[number]	k4	408
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85946	[number]	k4	85946
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
HAMANNOVÁ	HAMANNOVÁ	kA	HAMANNOVÁ
<g/>
,	,	kIx,	,
Brigitte	Brigitte	k1gFnSc1	Brigitte
<g/>
.	.	kIx.	.
</s>
<s>
Moje	můj	k3xOp1gFnSc1	můj
milá	milá	k1gFnSc1	milá
<g/>
,	,	kIx,	,
dobrá	dobrý	k2eAgFnSc1d1	dobrá
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
<g/>
!	!	kIx.	!
</s>
<s>
Milostný	milostný	k2eAgInSc1d1	milostný
vztah	vztah	k1gInSc1	vztah
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
a	a	k8xC	a
herečky	herečka	k1gFnPc1	herečka
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Schrattové	Schrattová	k1gFnSc2	Schrattová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Brána	brána	k1gFnSc1	brána
:	:	kIx,	:
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
217	[number]	k4	217
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7243	[number]	k4	7243
<g/>
-	-	kIx~	-
<g/>
145	[number]	k4	145
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PERNES	PERNES	kA	PERNES
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
Nikdy	nikdy	k6eAd1	nikdy
nekorunovaný	korunovaný	k2eNgMnSc1d1	nekorunovaný
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
495	[number]	k4	495
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7243	[number]	k4	7243
<g/>
-	-	kIx~	-
<g/>
217	[number]	k4	217
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PERNES	PERNES	kA	PERNES
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
habsburským	habsburský	k2eAgMnSc7d1	habsburský
orlem	orel	k1gMnSc7	orel
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
a	a	k8xC	a
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
205	[number]	k4	205
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7243	[number]	k4	7243
<g/>
-	-	kIx~	-
<g/>
290	[number]	k4	290
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RAK	rak	k1gMnSc1	rak
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Zachovej	zachovat	k5eAaPmRp2nS	zachovat
nám	my	k3xPp1nPc3	my
<g/>
,	,	kIx,	,
Hospodine	Hospodin	k1gMnSc5	Hospodin
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Havran	Havran	k1gMnSc1	Havran
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87341	[number]	k4	87341
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Osmdesáté	osmdesátý	k4xOgFnPc4	osmdesátý
narozeniny	narozeniny	k1gFnPc4	narozeniny
<g/>
,	,	kIx,	,
Válečný	válečný	k2eAgInSc1d1	válečný
epilog	epilog	k1gInSc1	epilog
<g/>
,	,	kIx,	,
s.	s.	k?	s.
384	[number]	k4	384
<g/>
-	-	kIx~	-
<g/>
409	[number]	k4	409
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
URBAN	Urban	k1gMnSc1	Urban
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
297	[number]	k4	297
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
203	[number]	k4	203
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOCELKOVI	VOCELKOVI	kA	VOCELKOVI
<g/>
,	,	kIx,	,
Michaela	Michaela	k1gFnSc1	Michaela
a	a	k8xC	a
Karl	Karl	k1gMnSc1	Karl
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
Císař	Císař	k1gMnSc1	Císař
rakouský	rakouský	k2eAgMnSc1d1	rakouský
a	a	k8xC	a
král	král	k1gMnSc1	král
uherský	uherský	k2eAgMnSc1d1	uherský
1830	[number]	k4	1830
<g/>
-	-	kIx~	-
<g/>
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Zlata	zlato	k1gNnSc2	zlato
Kufnerová	Kufnerová	k1gFnSc1	Kufnerová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
416	[number]	k4	416
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7432	[number]	k4	7432
<g/>
-	-	kIx~	-
<g/>
806	[number]	k4	806
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WEITLANEROVÁ	WEITLANEROVÁ	kA	WEITLANEROVÁ
<g/>
,	,	kIx,	,
Juliana	Julian	k1gMnSc2	Julian
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
Život	život	k1gInSc1	život
císaře	císař	k1gMnSc2	císař
slovem	slovem	k6eAd1	slovem
i	i	k8xC	i
obrazem	obraz	k1gInSc7	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vitalis	Vitalis	k1gInSc1	Vitalis
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
80	[number]	k4	80
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
89919	[number]	k4	89919
<g/>
-	-	kIx~	-
<g/>
455	[number]	k4	455
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HOŁDA	HOŁDA	kA	HOŁDA
<g/>
,	,	kIx,	,
Renata	Renata	k1gFnSc1	Renata
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Dobry	dobro	k1gNnPc7	dobro
władca	władcus	k1gMnSc2	władcus
<g/>
"	"	kIx"	"
:	:	kIx,	:
studium	studium	k1gNnSc1	studium
antropologiczne	antropologicznout	k5eAaPmIp3nS	antropologicznout
o	o	k7c6	o
Franciszku	Franciszek	k1gInSc6	Franciszek
Józefie	Józefie	k1gFnSc2	Józefie
I.	I.	kA	I.
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Katowice	Katowice	k1gFnSc1	Katowice
<g/>
:	:	kIx,	:
Wydawnictwo	Wydawnictwo	k1gNnSc1	Wydawnictwo
Uniwersytetu	Uniwersytet	k1gInSc2	Uniwersytet
Śląskiego	Śląskiego	k6eAd1	Śląskiego
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
198	[number]	k4	198
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Prace	Prace	k1gMnSc1	Prace
Naukowe	Naukow	k1gMnSc2	Naukow
Uniwersytetu	Uniwersytet	k1gInSc2	Uniwersytet
Śląskiego	Śląskiego	k1gMnSc1	Śląskiego
w	w	k?	w
Katowicach	Katowicach	k1gMnSc1	Katowicach
<g/>
;	;	kIx,	;
nr	nr	k?	nr
2579	[number]	k4	2579
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
226	[number]	k4	226
<g/>
-	-	kIx~	-
<g/>
1707	[number]	k4	1707
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Rakouska	Rakousko	k1gNnSc2	Rakousko
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Česka	Česko	k1gNnSc2	Česko
</s>
</p>
<p>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
císařská	císařský	k2eAgFnSc1d1	císařská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
internetové	internetový	k2eAgNnSc1d1	internetové
muzeum	muzeum	k1gNnSc1	muzeum
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
</s>
</p>
<p>
<s>
Císař	Císař	k1gMnSc1	Císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
video	video	k1gNnSc4	video
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
po	po	k7c6	po
100	[number]	k4	100
letech	let	k1gInPc6	let
<g/>
,	,	kIx,	,
Historie	historie	k1gFnSc1	historie
<g/>
.	.	kIx.	.
<g/>
cs	cs	k?	cs
(	(	kIx(	(
<g/>
video	video	k1gNnSc1	video
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Rovný	rovný	k2eAgMnSc1d1	rovný
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
přežil	přežít	k5eAaPmAgMnS	přežít
svoji	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
Britské	britský	k2eAgInPc4d1	britský
listy	list	k1gInPc4	list
30	[number]	k4	30
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
</s>
</p>
