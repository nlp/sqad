<s>
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
Karel	Karel	k1gMnSc1	Karel
(	(	kIx(	(
<g/>
Franz	Franz	k1gMnSc1	Franz
Joseph	Joseph	k1gMnSc1	Joseph
Karl	Karla	k1gFnPc2	Karla
von	von	k1gInSc1	von
Habsburg	Habsburg	k1gMnSc1	Habsburg
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1830	[number]	k4	1830
v	v	k7c4	v
9.45	[number]	k4	9.45
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
jako	jako	k9	jako
syn	syn	k1gMnSc1	syn
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Františka	František	k1gMnSc2	František
Karla	Karel	k1gMnSc2	Karel
(	(	kIx(	(
<g/>
mladšího	mladý	k2eAgMnSc2d2	mladší
syna	syn	k1gMnSc2	syn
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
ženy	žena	k1gFnPc1	žena
Žofie	Žofie	k1gFnSc2	Žofie
<g/>
,	,	kIx,	,
bavorské	bavorský	k2eAgFnSc2d1	bavorská
princezny	princezna	k1gFnSc2	princezna
<g/>
.	.	kIx.	.
</s>
