<s>
Sinokvět	Sinokvět	k5eAaPmF
chrpovitý	chrpovitý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Jurinea	Jurinea	k1gMnSc1
cyanoides	cyanoides	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vytrvalá	vytrvalý	k2eAgFnSc1d1
rostlina	rostlina	k1gFnSc1
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc4
ze	z	k7c2
dvou	dva	k4xCgInPc2
druhů	druh	k1gInPc2
rodu	rod	k1gInSc2
sinokvět	sinokvět	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
rostou	růst	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>