<p>
<s>
Hrad	hrad	k1gInSc1	hrad
a	a	k8xC	a
zámek	zámek	k1gInSc1	zámek
Staré	Staré	k2eAgInPc1d1	Staré
Hrady	hrad	k1gInPc1	hrad
je	být	k5eAaImIp3nS	být
renesanční	renesanční	k2eAgInSc1d1	renesanční
zámecký	zámecký	k2eAgInSc1d1	zámecký
komplex	komplex	k1gInSc1	komplex
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
obci	obec	k1gFnSc6	obec
u	u	k7c2	u
Libáně	Libána	k1gFnSc3	Libána
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Jičín	Jičín	k1gInSc1	Jičín
Královéhradeckého	královéhradecký	k2eAgInSc2d1	královéhradecký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
objektu	objekt	k1gInSc2	objekt
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
objektu	objekt	k1gInSc6	objekt
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1340	[number]	k4	1340
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
původně	původně	k6eAd1	původně
gotický	gotický	k2eAgInSc4d1	gotický
hrad	hrad	k1gInSc4	hrad
s	s	k7c7	s
rukopisem	rukopis	k1gInSc7	rukopis
parléřovské	parléřovský	k2eAgFnSc2d1	parléřovská
hutě	huť	k1gFnSc2	huť
s	s	k7c7	s
přistavěným	přistavěný	k2eAgInSc7d1	přistavěný
renesančním	renesanční	k2eAgInSc7d1	renesanční
zámkem	zámek	k1gInSc7	zámek
a	a	k8xC	a
podzámčím	podzámčí	k1gNnSc7	podzámčí
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
středověkého	středověký	k2eAgNnSc2d1	středověké
sídla	sídlo	k1gNnSc2	sídlo
<g/>
,	,	kIx,	,
stojícího	stojící	k2eAgInSc2d1	stojící
na	na	k7c6	na
nezřetelné	zřetelný	k2eNgFnSc6d1	nezřetelná
hranici	hranice	k1gFnSc6	hranice
mezi	mezi	k7c7	mezi
hradem	hrad	k1gInSc7	hrad
a	a	k8xC	a
tvrzí	tvrz	k1gFnSc7	tvrz
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
pouze	pouze	k6eAd1	pouze
palác	palác	k1gInSc1	palác
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
křídle	křídlo	k1gNnSc6	křídlo
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
kostel	kostel	k1gInSc1	kostel
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
nádvoří	nádvoří	k1gNnSc6	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Pruskovský	Pruskovský	k2eAgMnSc1d1	Pruskovský
z	z	k7c2	z
Pruskova	Pruskův	k2eAgInSc2d1	Pruskův
hrad	hrad	k1gInSc4	hrad
přestavěl	přestavět	k5eAaPmAgMnS	přestavět
na	na	k7c4	na
čtyřkřídlý	čtyřkřídlý	k2eAgInSc4d1	čtyřkřídlý
zámek	zámek	k1gInSc4	zámek
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
předzámčím	předzámčí	k1gNnSc7	předzámčí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
období	období	k1gNnSc2	období
baroka	baroko	k1gNnSc2	baroko
byly	být	k5eAaImAgInP	být
zvláště	zvláště	k6eAd1	zvláště
objekty	objekt	k1gInPc1	objekt
v	v	k7c6	v
předzámčí	předzámčí	k1gNnSc6	předzámčí
přestavovány	přestavován	k2eAgFnPc1d1	přestavována
a	a	k8xC	a
upravovány	upravován	k2eAgFnPc1d1	upravována
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
objekty	objekt	k1gInPc1	objekt
chátraly	chátrat	k5eAaImAgInP	chátrat
<g/>
,	,	kIx,	,
do	do	k7c2	do
nejhoršího	zlý	k2eAgInSc2d3	Nejhorší
stavu	stav	k1gInSc2	stav
se	se	k3xPyFc4	se
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
třetině	třetina	k1gFnSc6	třetina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dostala	dostat	k5eAaPmAgFnS	dostat
nejstarší	starý	k2eAgFnSc1d3	nejstarší
a	a	k8xC	a
nejcennější	cenný	k2eAgFnSc1d3	nejcennější
část	část	k1gFnSc1	část
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
gotický	gotický	k2eAgInSc1d1	gotický
palác	palác	k1gInSc1	palác
<g/>
.	.	kIx.	.
celý	celý	k2eAgInSc1d1	celý
objekt	objekt	k1gInSc1	objekt
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
zachráněn	zachránit	k5eAaPmNgInS	zachránit
aktivitou	aktivita	k1gFnSc7	aktivita
učitele	učitel	k1gMnSc2	učitel
a	a	k8xC	a
předsedy	předseda	k1gMnSc2	předseda
místního	místní	k2eAgInSc2d1	místní
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
Holmana	Holman	k1gMnSc4	Holman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obnoveném	obnovený	k2eAgInSc6d1	obnovený
zámku	zámek	k1gInSc6	zámek
byly	být	k5eAaImAgInP	být
umístěny	umístěn	k2eAgInPc4d1	umístěn
depozitáře	depozitář	k1gInPc4	depozitář
Památníku	památník	k1gInSc2	památník
národního	národní	k2eAgNnSc2d1	národní
písemnictví	písemnictví	k1gNnSc2	písemnictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
obec	obec	k1gFnSc4	obec
staré	starý	k2eAgInPc4d1	starý
Hrady	hrad	k1gInPc4	hrad
zámek	zámek	k1gInSc1	zámek
prodala	prodat	k5eAaPmAgFnS	prodat
soukromému	soukromý	k2eAgMnSc3d1	soukromý
majiteli	majitel	k1gMnSc3	majitel
a	a	k8xC	a
Památník	památník	k1gInSc4	památník
národního	národní	k2eAgNnSc2d1	národní
písemnictví	písemnictví	k1gNnSc2	písemnictví
postupně	postupně	k6eAd1	postupně
pronajaté	pronajatý	k2eAgFnSc2d1	pronajatá
prostory	prostora	k1gFnSc2	prostora
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
objekt	objekt	k1gInSc1	objekt
znovu	znovu	k6eAd1	znovu
otevřen	otevřít	k5eAaPmNgInS	otevřít
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
nabízí	nabízet	k5eAaImIp3nS	nabízet
3	[number]	k4	3
prohlídkové	prohlídkový	k2eAgInPc4d1	prohlídkový
okruhy	okruh	k1gInPc4	okruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I.	I.	kA	I.
prohlídkový	prohlídkový	k2eAgInSc4d1	prohlídkový
okruh	okruh	k1gInSc4	okruh
<g/>
:	:	kIx,	:
Hrad	hrad	k1gInSc1	hrad
a	a	k8xC	a
zámek	zámek	k1gInSc1	zámek
s	s	k7c7	s
Dračí	dračí	k2eAgFnSc7d1	dračí
čarodějnou	čarodějný	k2eAgFnSc7d1	čarodějná
královskou	královský	k2eAgFnSc7d1	královská
komnatouV	komnatouV	k?	komnatouV
hradní	hradní	k2eAgFnSc6d1	hradní
a	a	k8xC	a
zámecké	zámecký	k2eAgFnSc6d1	zámecká
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
prohlídkový	prohlídkový	k2eAgInSc4d1	prohlídkový
okruh	okruh	k1gInSc4	okruh
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
žilo	žít	k5eAaImAgNnS	žít
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
pána	pán	k1gMnSc4	pán
Františka	František	k1gMnSc4	František
Josefa	Josef	k1gMnSc4	Josef
I.	I.	kA	I.
Mnoho	mnoho	k4c1	mnoho
tisíc	tisíc	k4xCgInPc2	tisíc
exponátů	exponát	k1gInPc2	exponát
zde	zde	k6eAd1	zde
představuje	představovat	k5eAaImIp3nS	představovat
život	život	k1gInSc4	život
prostých	prostý	k2eAgMnPc2d1	prostý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
měšťanů	měšťan	k1gMnPc2	měšťan
a	a	k8xC	a
šlechty	šlechta	k1gFnSc2	šlechta
v	v	k7c6	v
období	období	k1gNnSc6	období
vrcholu	vrchol	k1gInSc2	vrchol
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
II	II	kA	II
<g/>
.	.	kIx.	.
prohlídkový	prohlídkový	k2eAgInSc1d1	prohlídkový
okruh	okruh	k1gInSc1	okruh
<g/>
:	:	kIx,	:
Zámecké	zámecký	k2eAgNnSc1d1	zámecké
pohádkové	pohádkový	k2eAgNnSc1d1	pohádkové
sklepení	sklepení	k1gNnSc1	sklepení
</s>
</p>
<p>
<s>
III	III	kA	III
<g/>
.	.	kIx.	.
prohlídkový	prohlídkový	k2eAgInSc4d1	prohlídkový
okruh	okruh	k1gInSc4	okruh
<g/>
:	:	kIx,	:
Hradní	hradní	k2eAgFnSc1d1	hradní
pohádková	pohádkový	k2eAgFnSc1d1	pohádková
půdaProhlídkové	půdaProhlídkový	k2eAgMnPc4d1	půdaProhlídkový
okruhy	okruh	k1gInPc4	okruh
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
pohádek	pohádka	k1gFnPc2	pohádka
čaroděje	čaroděj	k1gMnSc2	čaroděj
Archibalda	Archibald	k1gMnSc2	Archibald
I.	I.	kA	I.
zavedou	zavést	k5eAaPmIp3nP	zavést
návštěvníky	návštěvník	k1gMnPc4	návštěvník
do	do	k7c2	do
bývalého	bývalý	k2eAgNnSc2d1	bývalé
renesančního	renesanční	k2eAgNnSc2d1	renesanční
a	a	k8xC	a
barokního	barokní	k2eAgNnSc2d1	barokní
sklepení	sklepení	k1gNnSc2	sklepení
a	a	k8xC	a
na	na	k7c4	na
hradní	hradní	k2eAgFnSc4d1	hradní
pohádkovou	pohádkový	k2eAgFnSc4d1	pohádková
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ožívá	ožívat	k5eAaImIp3nS	ožívat
na	na	k7c4	na
350	[number]	k4	350
pohádkových	pohádkový	k2eAgFnPc2d1	pohádková
bytostí	bytost	k1gFnPc2	bytost
<g/>
,	,	kIx,	,
skřítků	skřítek	k1gMnPc2	skřítek
a	a	k8xC	a
strašidýlek	strašidýlko	k1gNnPc2	strašidýlko
z	z	k7c2	z
pohádek	pohádka	k1gFnPc2	pohádka
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
pohádkového	pohádkový	k2eAgMnSc2d1	pohádkový
čaroděje	čaroděj	k1gMnSc2	čaroděj
Archibalda	Archibaldo	k1gNnSc2	Archibaldo
I.	I.	kA	I.
</s>
</p>
<p>
<s>
==	==	k?	==
Výzdoba	výzdoba	k1gFnSc1	výzdoba
==	==	k?	==
</s>
</p>
<p>
<s>
Průčelí	průčelí	k1gNnSc1	průčelí
vstupního	vstupní	k2eAgNnSc2d1	vstupní
křídla	křídlo	k1gNnSc2	křídlo
předzámčí	předzámčí	k1gNnSc2	předzámčí
zdobí	zdobit	k5eAaImIp3nS	zdobit
renesanční	renesanční	k2eAgMnSc1d1	renesanční
kvádrové	kvádrový	k2eAgNnSc4d1	kvádrové
<g/>
,	,	kIx,	,
vegetabilní	vegetabilní	k2eAgNnSc4d1	vegetabilní
a	a	k8xC	a
figurální	figurální	k2eAgNnSc4d1	figurální
sgrafito	sgrafito	k1gNnSc4	sgrafito
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
unikátní	unikátní	k2eAgFnPc1d1	unikátní
portrétní	portrétní	k2eAgFnPc1d1	portrétní
bysty	bysta	k1gFnPc1	bysta
prvních	první	k4xOgMnPc2	první
habsburských	habsburský	k2eAgMnPc2d1	habsburský
císařů	císař	k1gMnPc2	císař
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trůně	trůn	k1gInSc6	trůn
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Natura	Natura	k1gFnSc1	Natura
2000	[number]	k4	2000
==	==	k?	==
</s>
</p>
<p>
<s>
Sklepení	sklepení	k1gNnSc1	sklepení
zámku	zámek	k1gInSc2	zámek
Staré	Staré	k2eAgInPc7d1	Staré
Hrady	hrad	k1gInPc7	hrad
bylo	být	k5eAaImAgNnS	být
prohlášeno	prohlášen	k2eAgNnSc1d1	prohlášeno
evropsky	evropsky	k6eAd1	evropsky
významnou	významný	k2eAgFnSc7d1	významná
lokalitou	lokalita	k1gFnSc7	lokalita
Natura	Natur	k1gMnSc2	Natur
2000	[number]	k4	2000
kvůli	kvůli	k7c3	kvůli
letní	letní	k2eAgFnSc3d1	letní
kolonii	kolonie	k1gFnSc3	kolonie
kriticky	kriticky	k6eAd1	kriticky
ohroženého	ohrožený	k2eAgMnSc4d1	ohrožený
vrápence	vrápenec	k1gMnSc4	vrápenec
malého	malý	k2eAgMnSc4d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
zde	zde	k6eAd1	zde
přebývá	přebývat	k5eAaImIp3nS	přebývat
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
60	[number]	k4	60
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zámek	zámek	k1gInSc1	zámek
Staré	Staré	k2eAgInPc1d1	Staré
Hrady	hrad	k1gInPc1	hrad
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Návštěvnické	návštěvnický	k2eAgFnPc1d1	návštěvnická
informace	informace	k1gFnPc1	informace
</s>
</p>
