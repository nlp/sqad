<s>
Částice	částice	k1gFnSc1	částice
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
svými	svůj	k3xOyFgFnPc7	svůj
charakteristickými	charakteristický	k2eAgFnPc7d1	charakteristická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
(	(	kIx(	(
<g/>
energií	energie	k1gFnSc7	energie
<g/>
,	,	kIx,	,
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
,	,	kIx,	,
elektrickým	elektrický	k2eAgInSc7d1	elektrický
nábojem	náboj	k1gInSc7	náboj
<g/>
,	,	kIx,	,
spinem	spin	k1gInSc7	spin
<g/>
,	,	kIx,	,
chemickou	chemický	k2eAgFnSc7d1	chemická
reaktivností	reaktivnost	k1gFnSc7	reaktivnost
<g/>
,	,	kIx,	,
dobou	doba	k1gFnSc7	doba
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studiem	studio	k1gNnSc7	studio
částic	částice	k1gFnPc2	částice
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
především	především	k9	především
fyzika	fyzika	k1gFnSc1	fyzika
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
především	především	k9	především
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
tzv.	tzv.	kA	tzv.
elementárních	elementární	k2eAgFnPc2d1	elementární
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vazby	vazba	k1gFnSc2	vazba
mezi	mezi	k7c7	mezi
elementárními	elementární	k2eAgFnPc7d1	elementární
částicemi	částice	k1gFnPc7	částice
(	(	kIx(	(
<g/>
takové	takový	k3xDgFnPc1	takový
částice	částice	k1gFnPc1	částice
bývají	bývat	k5eAaImIp3nP	bývat
také	také	k9	také
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xC	jako
složené	složený	k2eAgInPc1d1	složený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fyzika	fyzika	k1gFnSc1	fyzika
částic	částice	k1gFnPc2	částice
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
nejen	nejen	k6eAd1	nejen
částicemi	částice	k1gFnPc7	částice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
pozorovány	pozorovat	k5eAaImNgFnP	pozorovat
při	při	k7c6	při
experimentech	experiment	k1gInPc6	experiment
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
tzv.	tzv.	kA	tzv.
hypotetickými	hypotetický	k2eAgFnPc7d1	hypotetická
částicemi	částice	k1gFnPc7	částice
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
částicemi	částice	k1gFnPc7	částice
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
existence	existence	k1gFnSc1	existence
je	být	k5eAaImIp3nS	být
předpovídána	předpovídat	k5eAaImNgFnS	předpovídat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
teorií	teorie	k1gFnPc2	teorie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zatím	zatím	k6eAd1	zatím
nebyly	být	k5eNaImAgFnP	být
pozorovány	pozorován	k2eAgFnPc1d1	pozorována
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
částice	částice	k1gFnPc1	částice
se	se	k3xPyFc4	se
samovolně	samovolně	k6eAd1	samovolně
rozpadají	rozpadat	k5eAaImIp3nP	rozpadat
na	na	k7c4	na
jiné	jiný	k2eAgFnPc4d1	jiná
částice	částice	k1gFnPc4	částice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
rozpad	rozpad	k1gInSc1	rozpad
<g/>
)	)	kIx)	)
–	–	k?	–
takové	takový	k3xDgFnPc1	takový
částice	částice	k1gFnPc1	částice
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
nerozpadají	rozpadat	k5eNaImIp3nP	rozpadat
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k9	jako
stabilní	stabilní	k2eAgFnPc1d1	stabilní
částice	částice	k1gFnPc1	částice
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnSc1	částice
lze	lze	k6eAd1	lze
dělit	dělit	k5eAaImF	dělit
do	do	k7c2	do
různých	různý	k2eAgFnPc2d1	různá
skupin	skupina	k1gFnPc2	skupina
podle	podle	k7c2	podle
mnoha	mnoho	k4c2	mnoho
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
.	.	kIx.	.
bodová	bodový	k2eAgFnSc1d1	bodová
částice	částice	k1gFnSc1	částice
subatomární	subatomární	k2eAgFnSc2d1	subatomární
částice	částice	k1gFnSc2	částice
Pro	pro	k7c4	pro
technologické	technologický	k2eAgInPc4d1	technologický
účely	účel	k1gInPc4	účel
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
dělení	dělení	k1gNnSc1	dělení
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
na	na	k7c6	na
makročástice	makročástika	k1gFnSc6	makročástika
mikročástice	mikročástice	k1gFnSc2	mikročástice
nanočástice	nanočástika	k1gFnSc3	nanočástika
Podle	podle	k7c2	podle
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
podstaty	podstata	k1gFnSc2	podstata
lze	lze	k6eAd1	lze
částice	částice	k1gFnSc2	částice
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c6	na
látkové	látkový	k2eAgFnSc6d1	látková
–	–	k?	–
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
stavební	stavební	k2eAgFnPc1d1	stavební
součásti	součást	k1gFnPc1	součást
hmotných	hmotný	k2eAgFnPc2d1	hmotná
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
klidová	klidový	k2eAgFnSc1d1	klidová
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
nula	nula	k1gFnSc1	nula
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nemohou	moct	k5eNaImIp3nP	moct
se	se	k3xPyFc4	se
pohybovat	pohybovat	k5eAaImF	pohybovat
rychlostí	rychlost	k1gFnSc7	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
každé	každý	k3xTgFnSc3	každý
látkové	látkový	k2eAgFnSc3d1	látková
částici	částice	k1gFnSc3	částice
existuje	existovat	k5eAaImIp3nS	existovat
antičástice	antičástice	k1gFnSc1	antičástice
<g/>
.	.	kIx.	.
</s>
<s>
Elementárními	elementární	k2eAgFnPc7d1	elementární
částicemi	částice	k1gFnPc7	částice
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
jsou	být	k5eAaImIp3nP	být
fermiony	fermion	k1gInPc1	fermion
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
leptony	lepton	k1gInPc1	lepton
(	(	kIx(	(
<g/>
např.	např.	kA	např.
elektrony	elektron	k1gInPc1	elektron
<g/>
,	,	kIx,	,
pozitrony	pozitron	k1gInPc1	pozitron
<g/>
,	,	kIx,	,
miony	mion	k1gInPc1	mion
<g/>
,	,	kIx,	,
neutrina	neutrino	k1gNnPc1	neutrino
<g/>
)	)	kIx)	)
a	a	k8xC	a
kvarky	kvark	k1gInPc1	kvark
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
složeny	složen	k2eAgFnPc1d1	složena
další	další	k2eAgFnPc1d1	další
látkové	látkový	k2eAgFnPc1d1	látková
subatomární	subatomární	k2eAgFnPc1d1	subatomární
částice	částice	k1gFnPc1	částice
–	–	k?	–
mezony	mezon	k1gInPc4	mezon
a	a	k8xC	a
hadrony	hadron	k1gInPc4	hadron
(	(	kIx(	(
<g/>
např.	např.	kA	např.
protony	proton	k1gInPc1	proton
<g/>
,	,	kIx,	,
antiprotony	antiproton	k1gInPc1	antiproton
<g/>
,	,	kIx,	,
neutrony	neutron	k1gInPc1	neutron
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
polní	polní	k2eAgFnPc1d1	polní
–	–	k?	–
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zprostředkují	zprostředkovat	k5eAaPmIp3nP	zprostředkovat
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
interakcí	interakce	k1gFnPc2	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
bosony	boson	k1gInPc4	boson
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
klidová	klidový	k2eAgFnSc1d1	klidová
hmotnost	hmotnost	k1gFnSc1	hmotnost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nulová	nulový	k2eAgFnSc1d1	nulová
(	(	kIx(	(
<g/>
např.	např.	kA	např.
foton	foton	k1gInSc1	foton
<g/>
,	,	kIx,	,
gluon	gluon	k1gInSc1	gluon
<g/>
)	)	kIx)	)
i	i	k9	i
nenulová	nulový	k2eNgFnSc1d1	nenulová
(	(	kIx(	(
<g/>
např.	např.	kA	např.
intermediální	intermediální	k2eAgInPc1d1	intermediální
bosony	boson	k1gInPc1	boson
slabé	slabý	k2eAgFnSc2d1	slabá
interakce	interakce	k1gFnSc2	interakce
W	W	kA	W
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
W	W	kA	W
<g/>
−	−	k?	−
<g/>
,	,	kIx,	,
Z	z	k7c2	z
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
nemají	mít	k5eNaImIp3nP	mít
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
bodové	bodový	k2eAgNnSc4d1	bodové
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
jsou	být	k5eAaImIp3nP	být
základními	základní	k2eAgInPc7d1	základní
objekty	objekt	k1gInPc7	objekt
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgFnSc1d1	elementární
částice	částice	k1gFnSc1	částice
popisuje	popisovat	k5eAaImIp3nS	popisovat
tzv.	tzv.	kA	tzv.
Standardní	standardní	k2eAgInSc1d1	standardní
model	model	k1gInSc1	model
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
částice	částice	k1gFnPc4	částice
látkové	látkový	k2eAgFnSc2d1	látková
a	a	k8xC	a
polní	polní	k2eAgFnSc2d1	polní
<g/>
;	;	kIx,	;
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
rozlišovací	rozlišovací	k2eAgFnSc7d1	rozlišovací
charakteristikou	charakteristika	k1gFnSc7	charakteristika
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gInSc4	jejich
spin	spin	k1gInSc4	spin
<g/>
:	:	kIx,	:
Elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
látky	látka	k1gFnSc2	látka
jsou	být	k5eAaImIp3nP	být
vesměs	vesměs	k6eAd1	vesměs
fermiony	fermion	k1gInPc4	fermion
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
částice	částice	k1gFnPc1	částice
s	s	k7c7	s
poločíselným	poločíselný	k2eAgInSc7d1	poločíselný
spinem	spin	k1gInSc7	spin
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
leptony	lepton	k1gInPc4	lepton
(	(	kIx(	(
<g/>
např.	např.	kA	např.
elektron	elektron	k1gInSc1	elektron
<g/>
,	,	kIx,	,
neutrino	neutrino	k1gNnSc1	neutrino
<g/>
,	,	kIx,	,
pozitron	pozitron	k1gInSc1	pozitron
<g/>
,	,	kIx,	,
mion	mion	k1gInSc1	mion
<g/>
)	)	kIx)	)
a	a	k8xC	a
kvarky	kvark	k1gInPc1	kvark
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
každé	každý	k3xTgFnSc3	každý
elementární	elementární	k2eAgFnSc3d1	elementární
částici	částice	k1gFnSc3	částice
látky	látka	k1gFnSc2	látka
existuje	existovat	k5eAaImIp3nS	existovat
odpovídající	odpovídající	k2eAgFnSc1d1	odpovídající
antičástice	antičástice	k1gFnSc1	antičástice
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgFnSc1d1	elementární
částice	částice	k1gFnSc1	částice
pole	pole	k1gNnSc2	pole
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
intermediální	intermediální	k2eAgFnPc1d1	intermediální
částice	částice	k1gFnPc1	částice
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
bosony	bosona	k1gFnPc1	bosona
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
částice	částice	k1gFnPc1	částice
s	s	k7c7	s
celočíselným	celočíselný	k2eAgInSc7d1	celočíselný
spinem	spin	k1gInSc7	spin
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
např.	např.	kA	např.
foton	foton	k1gInSc1	foton
<g/>
,	,	kIx,	,
gluon	gluon	k1gInSc1	gluon
nebo	nebo	k8xC	nebo
Higgsův	Higgsův	k2eAgInSc1d1	Higgsův
boson	boson	k1gInSc1	boson
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
standardního	standardní	k2eAgInSc2d1	standardní
modelu	model	k1gInSc2	model
již	již	k6eAd1	již
byly	být	k5eAaImAgFnP	být
experimentálně	experimentálně	k6eAd1	experimentálně
pozorovány	pozorován	k2eAgFnPc1d1	pozorována
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
teorie	teorie	k1gFnPc1	teorie
předpovídají	předpovídat	k5eAaImIp3nP	předpovídat
existenci	existence	k1gFnSc4	existence
dalších	další	k2eAgFnPc2d1	další
elementárních	elementární	k2eAgFnPc2d1	elementární
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
částice	částice	k1gFnPc4	částice
předpovídané	předpovídaný	k2eAgFnPc4d1	předpovídaná
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tzv.	tzv.	kA	tzv.
supersymetrie	supersymetrie	k1gFnSc2	supersymetrie
<g/>
:	:	kIx,	:
skvarky	skvarka	k1gFnPc1	skvarka
<g/>
,	,	kIx,	,
sleptony	slepton	k1gInPc1	slepton
(	(	kIx(	(
<g/>
např.	např.	kA	např.
selektron	selektron	k1gInSc1	selektron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gluino	gluino	k1gNnSc1	gluino
<g/>
,	,	kIx,	,
neutralina	neutralina	k1gFnSc1	neutralina
a	a	k8xC	a
chargina	chargina	k1gFnSc1	chargina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hypotetické	hypotetický	k2eAgFnPc4d1	hypotetická
částice	částice	k1gFnPc4	částice
lze	lze	k6eAd1	lze
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
chvíli	chvíle	k1gFnSc6	chvíle
řadit	řadit	k5eAaImF	řadit
také	také	k9	také
graviton	graviton	k1gInSc4	graviton
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
příklad	příklad	k1gInSc1	příklad
dalších	další	k2eAgFnPc2d1	další
hypotetických	hypotetický	k2eAgFnPc2d1	hypotetická
částic	částice	k1gFnPc2	částice
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
např.	např.	kA	např.
tachyon	tachyon	k1gInSc4	tachyon
nebo	nebo	k8xC	nebo
axion	axion	k1gInSc4	axion
<g/>
.	.	kIx.	.
</s>
<s>
Složené	složený	k2eAgFnPc1d1	složená
částice	částice	k1gFnPc1	částice
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
hierarchických	hierarchický	k2eAgFnPc2d1	hierarchická
úrovní	úroveň	k1gFnPc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
subatomárních	subatomární	k2eAgFnPc2d1	subatomární
složených	složený	k2eAgFnPc2d1	složená
částic	částice	k1gFnPc2	částice
patří	patřit	k5eAaImIp3nS	patřit
ze	z	k7c2	z
známých	známý	k2eAgFnPc2d1	známá
částic	částice	k1gFnPc2	částice
hadrony	hadron	k1gInPc4	hadron
<g/>
,	,	kIx,	,
hypoteticky	hypoteticky	k6eAd1	hypoteticky
se	se	k3xPyFc4	se
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
exotické	exotický	k2eAgFnPc1d1	exotická
složené	složený	k2eAgFnPc1d1	složená
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
např.	např.	kA	např.
leptokvarky	leptokvarka	k1gFnSc2	leptokvarka
(	(	kIx(	(
<g/>
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
leptonů	lepton	k1gInPc2	lepton
a	a	k8xC	a
kvarků	kvark	k1gInPc2	kvark
zároveň	zároveň	k6eAd1	zároveň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kvarkovo-gluonové	kvarkovoluonový	k2eAgInPc1d1	kvarkovo-gluonový
vázané	vázaný	k2eAgInPc1d1	vázaný
stavy	stav	k1gInPc1	stav
či	či	k8xC	či
gluebally	glueballa	k1gFnPc1	glueballa
(	(	kIx(	(
<g/>
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
gluonů	gluon	k1gInPc2	gluon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyššími	vysoký	k2eAgFnPc7d2	vyšší
hierarchickými	hierarchický	k2eAgFnPc7d1	hierarchická
úrovněmi	úroveň	k1gFnPc7	úroveň
jsou	být	k5eAaImIp3nP	být
atomová	atomový	k2eAgNnPc1d1	atomové
jádra	jádro	k1gNnPc1	jádro
<g/>
,	,	kIx,	,
ionty	ion	k1gInPc1	ion
<g/>
,	,	kIx,	,
atomy	atom	k1gInPc1	atom
a	a	k8xC	a
molekuly	molekula	k1gFnPc1	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
úrovní	úroveň	k1gFnPc2	úroveň
přitom	přitom	k6eAd1	přitom
nejsou	být	k5eNaImIp3nP	být
ostré	ostrý	k2eAgInPc1d1	ostrý
(	(	kIx(	(
<g/>
proton	proton	k1gInSc1	proton
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
hadronem	hadron	k1gInSc7	hadron
<g/>
,	,	kIx,	,
atomovým	atomový	k2eAgNnSc7d1	atomové
jádrem	jádro	k1gNnSc7	jádro
i	i	k8xC	i
iontem	ion	k1gInSc7	ion
vodíku	vodík	k1gInSc2	vodík
H	H	kA	H
<g/>
+	+	kIx~	+
<g/>
;	;	kIx,	;
některé	některý	k3yIgFnPc1	některý
molekuly	molekula	k1gFnPc1	molekula
jsou	být	k5eAaImIp3nP	být
jednoatomové	jednoatomový	k2eAgFnPc1d1	jednoatomový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hadrony	Hadron	k1gMnPc4	Hadron
jsou	být	k5eAaImIp3nP	být
složené	složený	k2eAgFnPc1d1	složená
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
kvarky	kvark	k1gInPc1	kvark
a	a	k8xC	a
antikvarky	antikvark	k1gInPc1	antikvark
<g/>
.	.	kIx.	.
</s>
<s>
Hadrony	Hadron	k1gInPc1	Hadron
jsou	být	k5eAaImIp3nP	být
částice	částice	k1gFnPc1	částice
schopné	schopný	k2eAgFnPc1d1	schopná
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
silového	silový	k2eAgNnSc2d1	silové
působení	působení	k1gNnSc2	působení
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
silné	silný	k2eAgFnSc2d1	silná
interakce	interakce	k1gFnSc2	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Hadrony	Hadron	k1gInPc1	Hadron
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
spinu	spin	k1gInSc2	spin
a	a	k8xC	a
kvarkového	kvarkový	k2eAgNnSc2d1	kvarkový
složení	složení	k1gNnSc2	složení
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
běžné	běžný	k2eAgFnSc6d1	běžná
<g/>
:	:	kIx,	:
mezony	mezon	k1gInPc1	mezon
–	–	k?	–
hadrony	hadron	k1gInPc4	hadron
s	s	k7c7	s
celočíselným	celočíselný	k2eAgInSc7d1	celočíselný
spinem	spin	k1gInSc7	spin
složené	složený	k2eAgFnSc2d1	složená
z	z	k7c2	z
1	[number]	k4	1
kvarku	kvark	k1gInSc2	kvark
a	a	k8xC	a
1	[number]	k4	1
antikvarku	antikvark	k1gInSc2	antikvark
(	(	kIx(	(
<g/>
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
např.	např.	kA	např.
pion	pion	k1gInSc1	pion
<g/>
,	,	kIx,	,
kaon	kaon	k1gInSc1	kaon
<g/>
)	)	kIx)	)
baryony	baryona	k1gFnPc1	baryona
–	–	k?	–
hadrony	hadron	k1gInPc4	hadron
s	s	k7c7	s
poločíselným	poločíselný	k2eAgInSc7d1	poločíselný
spinem	spin	k1gInSc7	spin
složené	složený	k2eAgFnSc2d1	složená
ze	z	k7c2	z
3	[number]	k4	3
kvarků	kvark	k1gInPc2	kvark
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
např.	např.	kA	např.
proton	proton	k1gInSc1	proton
<g/>
,	,	kIx,	,
neutron	neutron	k1gInSc1	neutron
<g/>
,	,	kIx,	,
hyperony	hyperon	k1gInPc1	hyperon
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
a	a	k8xC	a
exotické	exotický	k2eAgFnSc3d1	exotická
nově	nova	k1gFnSc3	nova
objevené	objevený	k2eAgFnSc2d1	objevená
složené	složený	k2eAgFnSc2d1	složená
částice	částice	k1gFnSc2	částice
<g/>
:	:	kIx,	:
tetrakvarky	tetrakvarka	k1gFnSc2	tetrakvarka
–	–	k?	–
s	s	k7c7	s
celočíselným	celočíselný	k2eAgInSc7d1	celočíselný
spinem	spin	k1gInSc7	spin
<g/>
,	,	kIx,	,
složené	složený	k2eAgNnSc1d1	složené
ze	z	k7c2	z
2	[number]	k4	2
kvarků	kvark	k1gInPc2	kvark
a	a	k8xC	a
2	[number]	k4	2
antikvarků	antikvark	k1gInPc2	antikvark
<g/>
;	;	kIx,	;
pentakvarky	pentakvark	k1gInPc1	pentakvark
–	–	k?	–
s	s	k7c7	s
poločíselným	poločíselný	k2eAgInSc7d1	poločíselný
spinem	spin	k1gInSc7	spin
<g/>
,	,	kIx,	,
složené	složený	k2eAgNnSc1d1	složené
ze	z	k7c2	z
4	[number]	k4	4
kvarků	kvark	k1gInPc2	kvark
a	a	k8xC	a
1	[number]	k4	1
antikvarku	antikvark	k1gInSc2	antikvark
<g/>
;	;	kIx,	;
dibaryony	dibaryona	k1gFnPc1	dibaryona
–	–	k?	–
s	s	k7c7	s
celočíselným	celočíselný	k2eAgInSc7d1	celočíselný
spinem	spin	k1gInSc7	spin
<g/>
,	,	kIx,	,
složené	složený	k2eAgNnSc1d1	složené
ze	z	k7c2	z
6	[number]	k4	6
kvarků	kvark	k1gInPc2	kvark
<g/>
..	..	k?	..
Atomové	atomový	k2eAgNnSc1d1	atomové
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
složeno	složit	k5eAaPmNgNnS	složit
z	z	k7c2	z
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
typ	typ	k1gInSc1	typ
jádra	jádro	k1gNnSc2	jádro
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
určitý	určitý	k2eAgInSc4d1	určitý
počet	počet	k1gInSc4	počet
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
neutronů	neutron	k1gInPc2	neutron
(	(	kIx(	(
<g/>
nuklid	nuklid	k1gInSc1	nuklid
<g/>
,	,	kIx,	,
izotop	izotop	k1gInSc1	izotop
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jadernými	jaderný	k2eAgFnPc7d1	jaderná
reakcemi	reakce	k1gFnPc7	reakce
lze	lze	k6eAd1	lze
měnit	měnit	k5eAaImF	měnit
nuklid	nuklid	k1gInSc4	nuklid
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
studium	studium	k1gNnSc4	studium
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
jaderná	jaderný	k2eAgFnSc1d1	jaderná
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Atomy	atom	k1gInPc1	atom
jsou	být	k5eAaImIp3nP	být
nejmenší	malý	k2eAgFnPc1d3	nejmenší
neutrální	neutrální	k2eAgFnPc1d1	neutrální
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
lze	lze	k6eAd1	lze
hmotu	hmota	k1gFnSc4	hmota
rozdělit	rozdělit	k5eAaPmF	rozdělit
chemickou	chemický	k2eAgFnSc7d1	chemická
reakcí	reakce	k1gFnSc7	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Atomy	atom	k1gInPc1	atom
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgInP	složit
z	z	k7c2	z
malého	malý	k2eAgNnSc2d1	malé
<g/>
,	,	kIx,	,
hmotného	hmotný	k2eAgNnSc2d1	hmotné
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
obklopeno	obklopit	k5eAaPmNgNnS	obklopit
relativně	relativně	k6eAd1	relativně
velkým	velký	k2eAgInSc7d1	velký
a	a	k8xC	a
lehkým	lehký	k2eAgInSc7d1	lehký
elektronovým	elektronový	k2eAgInSc7d1	elektronový
obalem	obal	k1gInSc7	obal
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
typ	typ	k1gInSc1	typ
atomu	atom	k1gInSc2	atom
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
určitému	určitý	k2eAgInSc3d1	určitý
chemickému	chemický	k2eAgInSc3d1	chemický
prvku	prvek	k1gInSc3	prvek
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
periodická	periodický	k2eAgFnSc1d1	periodická
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nerovnováha	nerovnováha	k1gFnSc1	nerovnováha
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
mezi	mezi	k7c7	mezi
atomovým	atomový	k2eAgNnSc7d1	atomové
jádrem	jádro	k1gNnSc7	jádro
a	a	k8xC	a
elektronovým	elektronový	k2eAgInSc7d1	elektronový
obalem	obal	k1gInSc7	obal
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
iontů	ion	k1gInPc2	ion
(	(	kIx(	(
<g/>
kationty	kation	k1gInPc1	kation
a	a	k8xC	a
anionty	anion	k1gInPc1	anion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
atomy	atom	k1gInPc4	atom
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
atomová	atomový	k2eAgFnSc1d1	atomová
fyzika	fyzika	k1gFnSc1	fyzika
a	a	k8xC	a
také	také	k9	také
chemie	chemie	k1gFnSc2	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Molekuly	molekula	k1gFnPc1	molekula
jsou	být	k5eAaImIp3nP	být
nejmenší	malý	k2eAgFnPc1d3	nejmenší
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgInPc4	který
lze	lze	k6eAd1	lze
hmotu	hmota	k1gFnSc4	hmota
rozdělit	rozdělit	k5eAaPmF	rozdělit
se	s	k7c7	s
zachováním	zachování	k1gNnSc7	zachování
vlastností	vlastnost	k1gFnSc7	vlastnost
dané	daný	k2eAgFnSc2d1	daná
hmoty	hmota	k1gFnSc2	hmota
(	(	kIx(	(
<g/>
molekuly	molekula	k1gFnPc1	molekula
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
základní	základní	k2eAgFnPc1d1	základní
částice	částice	k1gFnPc1	částice
nesoucí	nesoucí	k2eAgFnPc4d1	nesoucí
vlastnosti	vlastnost	k1gFnPc4	vlastnost
celku	celek	k1gInSc2	celek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
molekula	molekula	k1gFnSc1	molekula
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
určité	určitý	k2eAgFnSc3d1	určitá
chemické	chemický	k2eAgFnSc3d1	chemická
sloučenině	sloučenina	k1gFnSc3	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Molekuly	molekula	k1gFnPc1	molekula
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgFnP	složit
z	z	k7c2	z
jednoho	jeden	k4xCgMnSc2	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Studiem	studio	k1gNnSc7	studio
molekul	molekula	k1gFnPc2	molekula
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
chemie	chemie	k1gFnSc1	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Rovnice	rovnice	k1gFnPc1	rovnice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
popisují	popisovat	k5eAaImIp3nP	popisovat
mnohočásticové	mnohočásticový	k2eAgInPc4d1	mnohočásticový
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gNnPc1	jejich
řešení	řešení	k1gNnPc1	řešení
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
svou	svůj	k3xOyFgFnSc7	svůj
formou	forma	k1gFnSc7	forma
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
popisu	popis	k1gInSc3	popis
přítomnosti	přítomnost	k1gFnSc2	přítomnost
nějaké	nějaký	k3yIgFnSc2	nějaký
částice	částice	k1gFnSc2	částice
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
taková	takový	k3xDgFnSc1	takový
samostatná	samostatný	k2eAgFnSc1d1	samostatná
částice	částice	k1gFnSc1	částice
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
systému	systém	k1gInSc6	systém
opravdu	opravdu	k6eAd1	opravdu
existovala	existovat	k5eAaImAgFnS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
určitý	určitý	k2eAgInSc4d1	určitý
druh	druh	k1gInSc4	druh
kolektivních	kolektivní	k2eAgInPc2d1	kolektivní
kvantových	kvantový	k2eAgInPc2d1	kvantový
stavů	stav	k1gInPc2	stav
(	(	kIx(	(
<g/>
víceatomové	víceatomový	k2eAgInPc4d1	víceatomový
elektronové	elektronový	k2eAgInPc4d1	elektronový
stavy	stav	k1gInPc4	stav
v	v	k7c6	v
látce	látka	k1gFnSc6	látka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
excitace	excitace	k1gFnSc2	excitace
atomové	atomový	k2eAgFnSc2d1	atomová
mřížky	mřížka	k1gFnSc2	mřížka
<g/>
,	,	kIx,	,
vícenukleonové	vícenukleonový	k2eAgInPc1d1	vícenukleonový
stavy	stav	k1gInPc1	stav
v	v	k7c6	v
atomovém	atomový	k2eAgNnSc6d1	atomové
jádře	jádro	k1gNnSc6	jádro
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nejen	nejen	k6eAd1	nejen
výpočetním	výpočetní	k2eAgInSc7d1	výpočetní
konstruktem	konstrukt	k1gInSc7	konstrukt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
reálné	reálný	k2eAgInPc4d1	reálný
projevy	projev	k1gInPc4	projev
(	(	kIx(	(
<g/>
přenos	přenos	k1gInSc4	přenos
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
,	,	kIx,	,
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
<g/>
,	,	kIx,	,
magnetického	magnetický	k2eAgInSc2d1	magnetický
momentu	moment	k1gInSc2	moment
<g/>
,	,	kIx,	,
statistické	statistický	k2eAgNnSc1d1	statistické
chování	chování	k1gNnSc1	chování
podle	podle	k7c2	podle
spinu	spin	k1gInSc2	spin
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Označují	označovat	k5eAaImIp3nP	označovat
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
kvazičástice	kvazičástika	k1gFnSc3	kvazičástika
<g/>
.	.	kIx.	.
</s>
<s>
Kvazičástice	Kvazičástika	k1gFnSc3	Kvazičástika
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
"	"	kIx"	"
<g/>
kolektivní	kolektivní	k2eAgFnSc1d1	kolektivní
excitace	excitace	k1gFnSc1	excitace
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
přestavují	přestavovat	k5eAaImIp3nP	přestavovat
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zjednodušit	zjednodušit	k5eAaPmF	zjednodušit
popis	popis	k1gInSc4	popis
mnohočásticových	mnohočásticový	k2eAgInPc2d1	mnohočásticový
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
elektronové	elektronový	k2eAgFnPc4d1	elektronová
díry	díra	k1gFnPc4	díra
<g/>
,	,	kIx,	,
fonony	fonon	k1gMnPc4	fonon
<g/>
,	,	kIx,	,
magnony	magnon	k1gMnPc4	magnon
nebo	nebo	k8xC	nebo
plazmony	plazmon	k1gMnPc4	plazmon
<g/>
.	.	kIx.	.
</s>
<s>
Materiálové	materiálový	k2eAgNnSc1d1	materiálové
inženýrství	inženýrství	k1gNnSc1	inženýrství
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jejich	jejich	k3xOp3gFnPc4	jejich
vlastnosti	vlastnost	k1gFnPc4	vlastnost
modifikovat	modifikovat	k5eAaBmF	modifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Kvazičástice	Kvazičástika	k1gFnSc3	Kvazičástika
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
velmi	velmi	k6eAd1	velmi
exotické	exotický	k2eAgFnPc4d1	exotická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
normální	normální	k2eAgFnPc4d1	normální
částice	částice	k1gFnPc4	částice
vyloučené	vyloučený	k2eAgFnPc4d1	vyloučená
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
zlomkovém	zlomkový	k2eAgInSc6d1	zlomkový
kvantovém	kvantový	k2eAgInSc6d1	kvantový
Hallově	Hallův	k2eAgInSc6d1	Hallův
jevu	jev	k1gInSc6	jev
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
kvazičástice	kvazičástika	k1gFnSc3	kvazičástika
s	s	k7c7	s
elektrickým	elektrický	k2eAgInSc7d1	elektrický
nábojem	náboj	k1gInSc7	náboj
rovným	rovný	k2eAgInSc7d1	rovný
zlomkové	zlomkový	k2eAgFnPc4d1	zlomková
části	část	k1gFnSc2	část
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pětině	pětina	k1gFnSc6	pětina
<g/>
)	)	kIx)	)
elementárního	elementární	k2eAgInSc2d1	elementární
náboje	náboj	k1gInSc2	náboj
<g/>
;	;	kIx,	;
ve	v	k7c6	v
"	"	kIx"	"
<g/>
dvourozměrných	dvourozměrný	k2eAgFnPc6d1	dvourozměrná
<g/>
"	"	kIx"	"
kvantových	kvantový	k2eAgFnPc6d1	kvantová
strukturách	struktura	k1gFnPc6	struktura
(	(	kIx(	(
<g/>
jednoatomové	jednoatomový	k2eAgFnSc2d1	jednoatomový
vrstvy	vrstva	k1gFnSc2	vrstva
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
realizovat	realizovat	k5eAaBmF	realizovat
kvazičástice	kvazičástika	k1gFnSc3	kvazičástika
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
statistické	statistický	k2eAgNnSc1d1	statistické
chování	chování	k1gNnSc1	chování
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
fermiony	fermion	k1gInPc7	fermion
a	a	k8xC	a
bosony	boson	k1gInPc7	boson
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
anyony	anyona	k1gFnSc2	anyona
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
též	též	k9	též
prokázány	prokázán	k2eAgFnPc1d1	prokázána
kvazičástice	kvazičástika	k1gFnSc6	kvazičástika
chovající	chovající	k2eAgFnPc1d1	chovající
se	se	k3xPyFc4	se
jako	jako	k9	jako
magnetický	magnetický	k2eAgInSc1d1	magnetický
monopól	monopól	k1gInSc1	monopól
či	či	k8xC	či
s	s	k7c7	s
nulovou	nulový	k2eAgFnSc7d1	nulová
efektivní	efektivní	k2eAgFnSc7d1	efektivní
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Virtuální	virtuální	k2eAgFnSc1d1	virtuální
částice	částice	k1gFnSc1	částice
je	být	k5eAaImIp3nS	být
koncept	koncept	k1gInSc4	koncept
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
částice	částice	k1gFnPc1	částice
nesplňují	splňovat	k5eNaImIp3nP	splňovat
Pythagorovu	Pythagorův	k2eAgFnSc4d1	Pythagorova
větu	věta	k1gFnSc4	věta
o	o	k7c4	o
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Virtuální	virtuální	k2eAgFnPc1d1	virtuální
částice	částice	k1gFnPc1	částice
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
takové	takový	k3xDgFnPc1	takový
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
existují	existovat	k5eAaImIp3nP	existovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
omezeném	omezený	k2eAgInSc6d1	omezený
čase	čas	k1gInSc6	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc6	prostor
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
například	například	k6eAd1	například
tunelový	tunelový	k2eAgInSc1d1	tunelový
jev	jev	k1gInSc1	jev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Virtuální	virtuální	k2eAgFnPc1d1	virtuální
částice	částice	k1gFnPc1	částice
se	se	k3xPyFc4	se
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
směrech	směr	k1gInPc6	směr
chovají	chovat	k5eAaImIp3nP	chovat
jako	jako	k9	jako
reálné	reálný	k2eAgFnPc1d1	reálná
částice	částice	k1gFnPc1	částice
<g/>
.	.	kIx.	.
</s>
<s>
Fyzika	fyzika	k1gFnSc1	fyzika
částic	částice	k1gFnPc2	částice
Elementární	elementární	k2eAgFnSc2d1	elementární
částice	částice	k1gFnSc2	částice
Hmota	hmota	k1gFnSc1	hmota
Pevná	pevný	k2eAgFnSc1d1	pevná
látka	látka	k1gFnSc1	látka
Kapalina	kapalina	k1gFnSc1	kapalina
Plyn	plyn	k1gInSc1	plyn
Dualita	dualita	k1gFnSc1	dualita
částice	částice	k1gFnSc2	částice
a	a	k8xC	a
vlnění	vlnění	k1gNnSc2	vlnění
Volná	volný	k2eAgFnSc1d1	volná
částice	částice	k1gFnSc1	částice
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
částice	částice	k1gFnSc2	částice
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
