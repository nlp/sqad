<s>
Křemík	křemík	k1gInSc1	křemík
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Silicium	silicium	k1gNnSc4	silicium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
polokovový	polokovový	k2eAgInSc4d1	polokovový
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
hojně	hojně	k6eAd1	hojně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
<g/>
.	.	kIx.	.
</s>
