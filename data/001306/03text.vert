<s>
Křemík	křemík	k1gInSc1	křemík
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Silicium	silicium	k1gNnSc4	silicium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
polokovový	polokovový	k2eAgInSc4d1	polokovový
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
hojně	hojně	k6eAd1	hojně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
základní	základní	k2eAgInSc4d1	základní
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
polovodičových	polovodičový	k2eAgFnPc2d1	polovodičová
součástek	součástka	k1gFnPc2	součástka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jako	jako	k9	jako
základní	základní	k2eAgFnSc1d1	základní
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
skla	sklo	k1gNnSc2	sklo
a	a	k8xC	a
významná	významný	k2eAgFnSc1d1	významná
součást	součást	k1gFnSc1	součást
keramických	keramický	k2eAgInPc2d1	keramický
a	a	k8xC	a
stavebních	stavební	k2eAgInPc2d1	stavební
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Značka	značka	k1gFnSc1	značka
křemíku	křemík	k1gInSc2	křemík
je	být	k5eAaImIp3nS	být
Si	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Křemík	křemík	k1gInSc4	křemík
poprvé	poprvé	k6eAd1	poprvé
identifikoval	identifikovat	k5eAaBmAgMnS	identifikovat
roku	rok	k1gInSc2	rok
1787	[number]	k4	1787
Antoine	Antoin	k1gInSc5	Antoin
Lavoisier	Lavoisier	k1gInSc4	Lavoisier
jakožto	jakožto	k8xS	jakožto
složku	složka	k1gFnSc4	složka
pazourku	pazourek	k1gInSc2	pazourek
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
křemíkatých	křemíkatý	k2eAgFnPc2d1	křemíkatý
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1800	[number]	k4	1800
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
Humphry	Humphra	k1gFnPc1	Humphra
Davy	Dav	k1gInPc4	Dav
mylně	mylně	k6eAd1	mylně
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
sloučeninu	sloučenina	k1gFnSc4	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1811	[number]	k4	1811
Gay-Lussac	Gay-Lussac	k1gInSc4	Gay-Lussac
a	a	k8xC	a
Thénard	Thénard	k1gInSc4	Thénard
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyrobili	vyrobit	k5eAaPmAgMnP	vyrobit
amorfní	amorfní	k2eAgInSc4d1	amorfní
křemík	křemík	k1gInSc4	křemík
zahříváním	zahřívání	k1gNnSc7	zahřívání
draslíku	draslík	k1gInSc2	draslík
s	s	k7c7	s
tetrafluorosilanem	tetrafluorosilan	k1gInSc7	tetrafluorosilan
<g/>
.	.	kIx.	.
</s>
<s>
Křemík	křemík	k1gInSc4	křemík
jakožto	jakožto	k8xS	jakožto
prvek	prvek	k1gInSc4	prvek
byl	být	k5eAaImAgMnS	být
poprvé	poprvé	k6eAd1	poprvé
izolován	izolovat	k5eAaBmNgMnS	izolovat
švédským	švédský	k2eAgMnSc7d1	švédský
chemikem	chemik	k1gMnSc7	chemik
J.	J.	kA	J.
J.	J.	kA	J.
Berzeliem	Berzelium	k1gNnSc7	Berzelium
roku	rok	k1gInSc2	rok
1823	[number]	k4	1823
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
Berzelius	Berzelius	k1gInSc1	Berzelius
získal	získat	k5eAaPmAgInS	získat
amorfní	amorfní	k2eAgInSc1d1	amorfní
křemík	křemík	k1gInSc1	křemík
přibližně	přibližně	k6eAd1	přibližně
stejným	stejný	k2eAgInSc7d1	stejný
postupem	postup	k1gInSc7	postup
jako	jako	k8xS	jako
předtím	předtím	k6eAd1	předtím
Gay-Lussac	Gay-Lussac	k1gFnSc1	Gay-Lussac
<g/>
.	.	kIx.	.
</s>
<s>
Berzelius	Berzelius	k1gInSc1	Berzelius
také	také	k9	také
produkt	produkt	k1gInSc1	produkt
přečistil	přečistit	k5eAaPmAgInS	přečistit
opakovaným	opakovaný	k2eAgNnSc7d1	opakované
promýváním	promývání	k1gNnSc7	promývání
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
polokov	polokov	k1gInSc4	polokov
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
afinitou	afinita	k1gFnSc7	afinita
ke	k	k7c3	k
kyslíku	kyslík	k1gInSc3	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
křemík	křemík	k1gInSc1	křemík
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
neomezeně	omezeně	k6eNd1	omezeně
stálý	stálý	k2eAgInSc1d1	stálý
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolní	okolní	k2eAgFnSc6d1	okolní
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
však	však	k9	však
setkáváme	setkávat	k5eAaImIp1nP	setkávat
prakticky	prakticky	k6eAd1	prakticky
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Si	se	k3xPyFc3	se
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odolný	odolný	k2eAgInSc1d1	odolný
vůči	vůči	k7c3	vůči
většině	většina	k1gFnSc3	většina
minerálních	minerální	k2eAgFnPc2d1	minerální
kyselin	kyselina	k1gFnPc2	kyselina
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
směsi	směs	k1gFnSc2	směs
kyseliny	kyselina	k1gFnSc2	kyselina
fluorovodíkové	fluorovodíkový	k2eAgFnSc2d1	fluorovodíková
(	(	kIx(	(
<g/>
HF	HF	kA	HF
<g/>
)	)	kIx)	)
a	a	k8xC	a
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgFnSc2d1	dusičná
(	(	kIx(	(
<g/>
HNO	HNO	kA	HNO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
však	však	k9	však
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
zásaditých	zásaditý	k2eAgInPc6d1	zásaditý
roztocích	roztok	k1gInPc6	roztok
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
hydroxidu	hydroxid	k1gInSc6	hydroxid
draselném	draselný	k2eAgInSc6d1	draselný
<g/>
)	)	kIx)	)
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
křemičitanového	křemičitanový	k2eAgInSc2d1	křemičitanový
aniontu	anion	k1gInSc2	anion
[	[	kIx(	[
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čisté	čistý	k2eAgFnSc6d1	čistá
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
křemík	křemík	k1gInSc1	křemík
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
setkáváme	setkávat	k5eAaImIp1nP	setkávat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
sloučeninami	sloučenina	k1gFnPc7	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
kyslíku	kyslík	k1gInSc6	kyslík
druhým	druhý	k4xOgInSc7	druhý
nejvíce	hodně	k6eAd3	hodně
zastoupeným	zastoupený	k2eAgInSc7d1	zastoupený
prvkem	prvek	k1gInSc7	prvek
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
posledních	poslední	k2eAgInPc2d1	poslední
dostupných	dostupný	k2eAgInPc2d1	dostupný
údajů	údaj	k1gInPc2	údaj
tvoří	tvořit	k5eAaImIp3nS	tvořit
26	[number]	k4	26
-	-	kIx~	-
28	[number]	k4	28
%	%	kIx~	%
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
poměrně	poměrně	k6eAd1	poměrně
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
3	[number]	k4	3
mg	mg	kA	mg
Si	se	k3xPyFc3	se
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
křemíku	křemík	k1gInSc2	křemík
pouze	pouze	k6eAd1	pouze
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
000	[number]	k4	000
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Křemík	křemík	k1gInSc1	křemík
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
složkou	složka	k1gFnSc7	složka
velké	velký	k2eAgFnSc2d1	velká
většiny	většina	k1gFnSc2	většina
hornin	hornina	k1gFnPc2	hornina
tvořících	tvořící	k2eAgFnPc2d1	tvořící
zemskou	zemský	k2eAgFnSc4d1	zemská
kůru	kůra	k1gFnSc4	kůra
-	-	kIx~	-
příkladem	příklad	k1gInSc7	příklad
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pískovcové	pískovcový	k2eAgFnPc1d1	pískovcová
horniny	hornina	k1gFnPc1	hornina
<g/>
,	,	kIx,	,
jíly	jíl	k1gInPc1	jíl
<g/>
,	,	kIx,	,
žuly	žula	k1gFnPc1	žula
a	a	k8xC	a
především	především	k9	především
aluminosilikátové	aluminosilikátový	k2eAgFnSc2d1	aluminosilikátový
horniny	hornina	k1gFnSc2	hornina
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
orthoklasu	orthoklas	k1gInSc2	orthoklas
(	(	kIx(	(
<g/>
aluminosilikáty	aluminosilikát	k1gInPc4	aluminosilikát
obsahující	obsahující	k2eAgInPc4d1	obsahující
draslík	draslík	k1gInSc4	draslík
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
plagioklasu	plagioklas	k1gInSc2	plagioklas
(	(	kIx(	(
<g/>
aluminosilikáty	aluminosilikát	k1gInPc4	aluminosilikát
obsahující	obsahující	k2eAgInPc4d1	obsahující
sodík	sodík	k1gInSc4	sodík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Křemík	křemík	k1gInSc1	křemík
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
vyvřelých	vyvřelý	k2eAgFnPc6d1	vyvřelá
horninách	hornina	k1gFnPc6	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Mineralogicky	mineralogicky	k6eAd1	mineralogicky
je	být	k5eAaImIp3nS	být
bezesporu	bezesporu	k9	bezesporu
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
zástupcem	zástupce	k1gMnSc7	zástupce
křemen	křemen	k1gInSc4	křemen
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
oxid	oxid	k1gInSc4	oxid
křemičitý	křemičitý	k2eAgInSc4d1	křemičitý
SiO	SiO	k1gFnSc7	SiO
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Minerály	minerál	k1gInPc1	minerál
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
složením	složení	k1gNnSc7	složení
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
barevně	barevně	k6eAd1	barevně
podle	podle	k7c2	podle
přítomnosti	přítomnost	k1gFnSc2	přítomnost
malých	malý	k2eAgNnPc2d1	malé
množství	množství	k1gNnPc2	množství
cizorodých	cizorodý	k2eAgInPc2d1	cizorodý
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
zbarvení	zbarvení	k1gNnSc1	zbarvení
krystalického	krystalický	k2eAgInSc2d1	krystalický
oxidu	oxid	k1gInSc2	oxid
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
čistý	čistý	k2eAgInSc1d1	čistý
oxid	oxid	k1gInSc1	oxid
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
je	být	k5eAaImIp3nS	být
mineralogicky	mineralogicky	k6eAd1	mineralogicky
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
křišťál	křišťál	k1gInSc1	křišťál
<g/>
,	,	kIx,	,
fialově	fialově	k6eAd1	fialově
je	být	k5eAaImIp3nS	být
zbarven	zbarven	k2eAgInSc1d1	zbarven
ametyst	ametyst	k1gInSc1	ametyst
(	(	kIx(	(
<g/>
příměs	příměs	k1gFnSc1	příměs
Fe	Fe	k1gFnSc1	Fe
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žlutý	žlutý	k2eAgMnSc1d1	žlutý
je	být	k5eAaImIp3nS	být
citrín	citrín	k?	citrín
(	(	kIx(	(
<g/>
příměs	příměs	k1gFnSc1	příměs
Fe	Fe	k1gFnSc1	Fe
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
růžový	růžový	k2eAgInSc1d1	růžový
růženín	růženín	k1gInSc1	růženín
(	(	kIx(	(
<g/>
Mn	Mn	k1gFnSc1	Mn
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hnědý	hnědý	k2eAgInSc1d1	hnědý
záhněda	záhněda	k1gFnSc1	záhněda
(	(	kIx(	(
<g/>
příměs	příměs	k1gFnSc1	příměs
Al	ala	k1gFnPc2	ala
<g/>
,	,	kIx,	,
Na	na	k7c4	na
<g/>
,	,	kIx,	,
Li	li	k8xS	li
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jaspis	jaspis	k1gInSc1	jaspis
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
barevných	barevný	k2eAgFnPc6d1	barevná
variantách	varianta	k1gFnPc6	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
případ	případ	k1gInSc1	případ
minerálu	minerál	k1gInSc2	minerál
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
oxidu	oxid	k1gInSc2	oxid
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
je	být	k5eAaImIp3nS	být
amorfní	amorfní	k2eAgFnSc1d1	amorfní
forma	forma	k1gFnSc1	forma
této	tento	k3xDgFnSc2	tento
sloučeniny	sloučenina	k1gFnSc2	sloučenina
-	-	kIx~	-
opál	opál	k1gInSc1	opál
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
módní	módní	k2eAgInSc1d1	módní
polodrahokam	polodrahokam	k1gInSc1	polodrahokam
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
barevných	barevný	k2eAgFnPc2d1	barevná
variant	varianta	k1gFnPc2	varianta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
největší	veliký	k2eAgNnSc1d3	veliký
množství	množství	k1gNnSc1	množství
opálů	opál	k1gInPc2	opál
dobývá	dobývat	k5eAaImIp3nS	dobývat
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
včetně	včetně	k7c2	včetně
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
biologického	biologický	k2eAgNnSc2d1	biologické
hlediska	hledisko	k1gNnSc2	hledisko
patří	patřit	k5eAaImIp3nS	patřit
křemík	křemík	k1gInSc1	křemík
mezi	mezi	k7c4	mezi
biogenní	biogenní	k2eAgInPc4d1	biogenní
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jeho	jeho	k3xOp3gInSc1	jeho
obsah	obsah	k1gInSc1	obsah
v	v	k7c6	v
tkáních	tkáň	k1gFnPc6	tkáň
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
vysoký	vysoký	k2eAgInSc1d1	vysoký
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
dospělého	dospělý	k1gMnSc4	dospělý
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
g	g	kA	g
křemíku	křemík	k1gInSc2	křemík
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
v	v	k7c6	v
kostech	kost	k1gFnPc6	kost
<g/>
,	,	kIx,	,
chrupavkách	chrupavka	k1gFnPc6	chrupavka
a	a	k8xC	a
zubní	zubní	k2eAgFnSc3d1	zubní
sklovině	sklovina	k1gFnSc3	sklovina
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jejichž	jejichž	k3xOyRp3gInSc4	jejichž
zdravý	zdravý	k2eAgInSc4d1	zdravý
růst	růst	k1gInSc4	růst
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
je	být	k5eAaImIp3nS	být
nezbytný	zbytný	k2eNgMnSc1d1	zbytný
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
obsah	obsah	k1gInSc1	obsah
křemíku	křemík	k1gInSc2	křemík
v	v	k7c6	v
rostlinných	rostlinný	k2eAgFnPc6d1	rostlinná
buňkách	buňka	k1gFnPc6	buňka
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaPmF	nalézt
např.	např.	kA	např.
v	v	k7c6	v
přesličkách	přeslička	k1gFnPc6	přeslička
nebo	nebo	k8xC	nebo
žahavých	žahavý	k2eAgInPc6d1	žahavý
chloupcích	chloupek	k1gInPc6	chloupek
kopřiv	kopřiva	k1gFnPc2	kopřiva
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádně	mimořádně	k6eAd1	mimořádně
důležitý	důležitý	k2eAgInSc1d1	důležitý
je	být	k5eAaImIp3nS	být
křemík	křemík	k1gInSc1	křemík
pro	pro	k7c4	pro
rozsivky	rozsivka	k1gFnPc4	rozsivka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gFnPc1	jeho
sloučeniny	sloučenina	k1gFnPc1	sloučenina
tvoří	tvořit	k5eAaImIp3nP	tvořit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
složku	složka	k1gFnSc4	složka
schránky	schránka	k1gFnSc2	schránka
těchto	tento	k3xDgFnPc2	tento
jednobuněčných	jednobuněčný	k2eAgFnPc2d1	jednobuněčná
řas	řasa	k1gFnPc2	řasa
<g/>
,	,	kIx,	,
frustuly	frustul	k1gInPc1	frustul
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
zejména	zejména	k9	zejména
o	o	k7c4	o
vodnatý	vodnatý	k2eAgInSc4d1	vodnatý
polymer	polymer	k1gInSc4	polymer
oxidu	oxid	k1gInSc2	oxid
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
<g/>
,	,	kIx,	,
blízký	blízký	k2eAgInSc4d1	blízký
opálu	opál	k1gInSc3	opál
<g/>
.	.	kIx.	.
</s>
<s>
Rozsivky	rozsivka	k1gFnPc1	rozsivka
jsou	být	k5eAaImIp3nP	být
jedinou	jediný	k2eAgFnSc7d1	jediná
skupinou	skupina	k1gFnSc7	skupina
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
rozvoj	rozvoj	k1gInSc1	rozvoj
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
přítomnosti	přítomnost	k1gFnSc6	přítomnost
rozpustných	rozpustný	k2eAgFnPc2d1	rozpustná
forem	forma	k1gFnPc2	forma
oxidu	oxid	k1gInSc2	oxid
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyčerpání	vyčerpání	k1gNnSc6	vyčerpání
zdrojů	zdroj	k1gInPc2	zdroj
křemíku	křemík	k1gInSc2	křemík
se	se	k3xPyFc4	se
zastaví	zastavit	k5eAaPmIp3nS	zastavit
replikace	replikace	k1gFnSc1	replikace
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Rozsivky	rozsivka	k1gFnPc1	rozsivka
jsou	být	k5eAaImIp3nP	být
významnými	významný	k2eAgMnPc7d1	významný
primárními	primární	k2eAgMnPc7d1	primární
producenty	producent	k1gMnPc7	producent
biomasy	biomasa	k1gFnSc2	biomasa
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
biomasa	biomasa	k1gFnSc1	biomasa
tvoří	tvořit	k5eAaImIp3nS	tvořit
25	[number]	k4	25
<g/>
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgNnSc2d1	celkové
množství	množství	k1gNnSc2	množství
produkovaného	produkovaný	k2eAgInSc2d1	produkovaný
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Křemík	křemík	k1gInSc1	křemík
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
využití	využití	k1gNnSc1	využití
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
porcelánu	porcelán	k1gInSc2	porcelán
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
cementu	cement	k1gInSc2	cement
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
křemíku	křemík	k1gInSc2	křemík
v	v	k7c6	v
průmyslovém	průmyslový	k2eAgNnSc6d1	průmyslové
měřítku	měřítko	k1gNnSc6	měřítko
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
redukci	redukce	k1gFnSc6	redukce
taveniny	tavenina	k1gFnSc2	tavenina
vysoce	vysoce	k6eAd1	vysoce
čistého	čistý	k2eAgInSc2d1	čistý
oxidu	oxid	k1gInSc2	oxid
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
v	v	k7c6	v
obloukové	obloukový	k2eAgFnSc6d1	oblouková
elektrické	elektrický	k2eAgFnSc6d1	elektrická
peci	pec	k1gFnSc6	pec
na	na	k7c6	na
grafitové	grafitový	k2eAgFnSc6d1	grafitová
elektrodě	elektroda	k1gFnSc6	elektroda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
materiál	materiál	k1gInSc1	materiál
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
spalován	spalován	k2eAgInSc1d1	spalován
na	na	k7c4	na
plynný	plynný	k2eAgInSc4d1	plynný
oxid	oxid	k1gInSc4	oxid
uhelnatý	uhelnatý	k2eAgInSc4d1	uhelnatý
podle	podle	k7c2	podle
reakce	reakce	k1gFnSc2	reakce
<g/>
:	:	kIx,	:
SiO	SiO	k1gFnSc1	SiO
<g/>
2	[number]	k4	2
+	+	kIx~	+
2C	[number]	k4	2C
→	→	k?	→
Si	se	k3xPyFc3	se
+	+	kIx~	+
2CO	[number]	k4	2CO
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
křemíku	křemík	k1gInSc2	křemík
o	o	k7c6	o
čistotě	čistota	k1gFnSc6	čistota
97	[number]	k4	97
-	-	kIx~	-
99	[number]	k4	99
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
elektronického	elektronický	k2eAgInSc2d1	elektronický
průmyslu	průmysl	k1gInSc2	průmysl
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
tato	tento	k3xDgFnSc1	tento
čistota	čistota	k1gFnSc1	čistota
naprosto	naprosto	k6eAd1	naprosto
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
výroba	výroba	k1gFnSc1	výroba
elektronických	elektronický	k2eAgFnPc2d1	elektronická
součástek	součástka	k1gFnPc2	součástka
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
většinou	většinou	k6eAd1	většinou
křemík	křemík	k1gInSc4	křemík
o	o	k7c6	o
čistotě	čistota	k1gFnSc6	čistota
minimálně	minimálně	k6eAd1	minimálně
99,999	[number]	k4	99,999
<g/>
9	[number]	k4	9
%	%	kIx~	%
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
i	i	k9	i
nepatrné	patrný	k2eNgNnSc1d1	nepatrné
znečištění	znečištění	k1gNnSc1	znečištění
výrazně	výrazně	k6eAd1	výrazně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
kvalitu	kvalita	k1gFnSc4	kvalita
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
tranzistorů	tranzistor	k1gInPc2	tranzistor
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
elektronických	elektronický	k2eAgFnPc2d1	elektronická
součástek	součástka	k1gFnPc2	součástka
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
metod	metoda	k1gFnPc2	metoda
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
vysoce	vysoce	k6eAd1	vysoce
čistého	čistý	k2eAgInSc2d1	čistý
křemíku	křemík	k1gInSc2	křemík
je	být	k5eAaImIp3nS	být
zonální	zonální	k2eAgNnSc4d1	zonální
tavení	tavení	k1gNnSc4	tavení
<g/>
.	.	kIx.	.
</s>
<s>
Čištěný	čištěný	k2eAgInSc1d1	čištěný
materiál	materiál	k1gInSc1	materiál
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
upraví	upravit	k5eAaPmIp3nS	upravit
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
tenké	tenký	k2eAgFnSc2d1	tenká
tyče	tyč	k1gFnSc2	tyč
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
ve	v	k7c6	v
speciální	speciální	k2eAgFnSc6d1	speciální
pícce	pícka	k1gFnSc6	pícka
postupně	postupně	k6eAd1	postupně
přetavuje	přetavovat	k5eAaImIp3nS	přetavovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tavená	tavený	k2eAgFnSc1d1	tavená
zóna	zóna	k1gFnSc1	zóna
posunovala	posunovat	k5eAaImAgFnS	posunovat
od	od	k7c2	od
jednoho	jeden	k4xCgInSc2	jeden
konce	konec	k1gInSc2	konec
ke	k	k7c3	k
druhému	druhý	k4xOgMnSc3	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
nečistoty	nečistota	k1gFnPc1	nečistota
přítomné	přítomný	k2eAgFnPc4d1	přítomná
v	v	k7c6	v
materiálu	materiál	k1gInSc6	materiál
koncentrují	koncentrovat	k5eAaBmIp3nP	koncentrovat
v	v	k7c6	v
roztavené	roztavený	k2eAgFnSc6d1	roztavená
zóně	zóna	k1gFnSc6	zóna
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
tyče	tyč	k1gFnSc2	tyč
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
odstraní	odstranit	k5eAaPmIp3nP	odstranit
odříznutím	odříznutí	k1gNnSc7	odříznutí
<g/>
.	.	kIx.	.
</s>
<s>
Několikanásobným	několikanásobný	k2eAgNnSc7d1	několikanásobné
opakováním	opakování	k1gNnSc7	opakování
tohoto	tento	k3xDgInSc2	tento
postupu	postup	k1gInSc2	postup
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
poměrně	poměrně	k6eAd1	poměrně
vysoce	vysoce	k6eAd1	vysoce
čistý	čistý	k2eAgInSc4d1	čistý
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
extrémně	extrémně	k6eAd1	extrémně
čistého	čistý	k2eAgInSc2d1	čistý
křemíku	křemík	k1gInSc2	křemík
používají	používat	k5eAaImIp3nP	používat
chemické	chemický	k2eAgFnPc1d1	chemická
metody	metoda	k1gFnPc1	metoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tzv.	tzv.	kA	tzv.
Siemensově	Siemensův	k2eAgNnSc6d1	Siemensovo
postupu	postup	k1gInSc6	postup
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
křemíku	křemík	k1gInSc2	křemík
nejprve	nejprve	k6eAd1	nejprve
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
nějaká	nějaký	k3yIgFnSc1	nějaký
těkavá	těkavý	k2eAgFnSc1d1	těkavá
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
trichlorsilan	trichlorsilan	k1gMnSc1	trichlorsilan
HSiCl	HSiCl	k1gMnSc1	HSiCl
<g/>
3	[number]	k4	3
nebo	nebo	k8xC	nebo
chlorid	chlorid	k1gInSc4	chlorid
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
SiCl	SiCl	k1gInSc1	SiCl
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
plynné	plynný	k2eAgFnPc1d1	plynná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
vedou	vést	k5eAaImIp3nP	vést
přes	přes	k7c4	přes
vrstvu	vrstva	k1gFnSc4	vrstva
vysoce	vysoce	k6eAd1	vysoce
čistého	čistý	k2eAgInSc2d1	čistý
křemíku	křemík	k1gInSc2	křemík
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
přes	přes	k7c4	přes
1	[number]	k4	1
100	[number]	k4	100
°	°	k?	°
<g/>
C.	C.	kA	C.
Přitom	přitom	k6eAd1	přitom
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
rozkladu	rozklad	k1gInSc3	rozklad
a	a	k8xC	a
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
vysoce	vysoce	k6eAd1	vysoce
čistý	čistý	k2eAgInSc4d1	čistý
křemík	křemík	k1gInSc4	křemík
se	se	k3xPyFc4	se
ukládá	ukládat	k5eAaImIp3nS	ukládat
v	v	k7c6	v
krystalické	krystalický	k2eAgFnSc6d1	krystalická
podobě	podoba	k1gFnSc6	podoba
na	na	k7c4	na
původní	původní	k2eAgFnSc4d1	původní
křemíkovou	křemíkový	k2eAgFnSc4d1	křemíková
podložku	podložka	k1gFnSc4	podložka
<g/>
.	.	kIx.	.
</s>
<s>
Reakci	reakce	k1gFnSc4	reakce
trichlorsilanu	trichlorsilan	k1gInSc2	trichlorsilan
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
:	:	kIx,	:
2	[number]	k4	2
HSiCl	HSiCl	k1gInSc1	HSiCl
<g/>
3	[number]	k4	3
→	→	k?	→
Si	se	k3xPyFc3	se
+	+	kIx~	+
2	[number]	k4	2
HCl	HCl	k1gFnPc2	HCl
+	+	kIx~	+
SiCl	SiCl	k1gInSc4	SiCl
<g/>
4	[number]	k4	4
Uvedeným	uvedený	k2eAgInSc7d1	uvedený
postupem	postup	k1gInSc7	postup
vzniká	vznikat	k5eAaImIp3nS	vznikat
tzv.	tzv.	kA	tzv.
polykrystalický	polykrystalický	k2eAgInSc1d1	polykrystalický
křemík	křemík	k1gInSc1	křemík
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
typicky	typicky	k6eAd1	typicky
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nečistoty	nečistota	k1gFnPc1	nečistota
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
jednotek	jednotka	k1gFnPc2	jednotka
ppb	ppb	k?	ppb
(	(	kIx(	(
<g/>
1	[number]	k4	1
:	:	kIx,	:
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
plně	plně	k6eAd1	plně
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
požadavkům	požadavek	k1gInPc3	požadavek
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
elektronických	elektronický	k2eAgFnPc2d1	elektronická
polovodičových	polovodičový	k2eAgFnPc2d1	polovodičová
součástek	součástka	k1gFnPc2	součástka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
byl	být	k5eAaImAgInS	být
alternativou	alternativa	k1gFnSc7	alternativa
k	k	k7c3	k
Siemensově	Siemensův	k2eAgFnSc3d1	Siemensova
metodě	metoda	k1gFnSc3	metoda
postup	postup	k1gInSc4	postup
DuPontův	DuPontův	k2eAgInSc4d1	DuPontův
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vycházel	vycházet	k5eAaImAgInS	vycházet
z	z	k7c2	z
chloridu	chlorid	k1gInSc2	chlorid
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc2	jeho
rozkladu	rozklad	k1gInSc2	rozklad
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
950	[number]	k4	950
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
vysoce	vysoce	k6eAd1	vysoce
čistém	čistý	k2eAgInSc6d1	čistý
zinku	zinek	k1gInSc6	zinek
podle	podle	k7c2	podle
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
:	:	kIx,	:
SiCl	SiCl	k1gInSc1	SiCl
<g/>
4	[number]	k4	4
+	+	kIx~	+
2	[number]	k4	2
Zn	zn	kA	zn
→	→	k?	→
Si	se	k3xPyFc3	se
+	+	kIx~	+
2	[number]	k4	2
ZnCl	ZnCl	k1gInSc1	ZnCl
<g/>
2	[number]	k4	2
Technické	technický	k2eAgInPc1d1	technický
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
těkavostí	těkavost	k1gFnSc7	těkavost
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
chloridu	chlorid	k1gInSc2	chlorid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
následně	následně	k6eAd1	následně
znečišťoval	znečišťovat	k5eAaImAgInS	znečišťovat
vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
čistý	čistý	k2eAgInSc1d1	čistý
křemík	křemík	k1gInSc1	křemík
<g/>
,	,	kIx,	,
vedly	vést	k5eAaImAgFnP	vést
nakonec	nakonec	k6eAd1	nakonec
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
prakticky	prakticky	k6eAd1	prakticky
opuštěn	opustit	k5eAaPmNgInS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
některých	některý	k3yIgFnPc2	některý
polovodičových	polovodičový	k2eAgFnPc2d1	polovodičová
součástek	součástka	k1gFnPc2	součástka
je	být	k5eAaImIp3nS	být
polykrystalický	polykrystalický	k2eAgInSc1d1	polykrystalický
křemík	křemík	k1gInSc1	křemík
nepoužitelný	použitelný	k2eNgInSc1d1	nepoužitelný
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
křemík	křemík	k1gInSc1	křemík
monokrystalický	monokrystalický	k2eAgInSc1d1	monokrystalický
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
metodou	metoda	k1gFnSc7	metoda
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
výrobu	výroba	k1gFnSc4	výroba
je	být	k5eAaImIp3nS	být
řízená	řízený	k2eAgFnSc1d1	řízená
krystalizace	krystalizace	k1gFnSc1	krystalizace
z	z	k7c2	z
taveniny	tavenina	k1gFnSc2	tavenina
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Czochralského	Czochralský	k2eAgInSc2d1	Czochralský
proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
postupu	postup	k1gInSc6	postup
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
křemíkové	křemíkový	k2eAgFnSc2d1	křemíková
taveniny	tavenina	k1gFnSc2	tavenina
vložen	vložen	k2eAgInSc4d1	vložen
zárodečný	zárodečný	k2eAgInSc4d1	zárodečný
krystal	krystal	k1gInSc4	krystal
vysoce	vysoce	k6eAd1	vysoce
čistého	čistý	k2eAgInSc2d1	čistý
křemíku	křemík	k1gInSc2	křemík
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krystal	krystal	k1gInSc1	krystal
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
otáčí	otáčet	k5eAaImIp3nS	otáčet
a	a	k8xC	a
pulzuje	pulzovat	k5eAaImIp3nS	pulzovat
podle	podle	k7c2	podle
předem	předem	k6eAd1	předem
přesně	přesně	k6eAd1	přesně
definovaného	definovaný	k2eAgInSc2d1	definovaný
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
teplota	teplota	k1gFnSc1	teplota
taveniny	tavenina	k1gFnSc2	tavenina
je	být	k5eAaImIp3nS	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
pečlivě	pečlivě	k6eAd1	pečlivě
sledována	sledovat	k5eAaImNgFnS	sledovat
a	a	k8xC	a
řízena	řídit	k5eAaImNgFnS	řídit
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
proces	proces	k1gInSc1	proces
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
nádobách	nádoba	k1gFnPc6	nádoba
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
čistého	čistý	k2eAgInSc2d1	čistý
křemene	křemen	k1gInSc2	křemen
v	v	k7c6	v
inertní	inertní	k2eAgFnSc6d1	inertní
atmosféře	atmosféra	k1gFnSc6	atmosféra
argonu	argon	k1gInSc2	argon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zárodečném	zárodečný	k2eAgInSc6d1	zárodečný
krystalu	krystal	k1gInSc6	krystal
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
další	další	k2eAgFnPc4d1	další
vrstvy	vrstva	k1gFnPc4	vrstva
mimořádně	mimořádně	k6eAd1	mimořádně
čistého	čistý	k2eAgInSc2d1	čistý
křemíku	křemík	k1gInSc2	křemík
<g/>
,	,	kIx,	,
výsledný	výsledný	k2eAgInSc1d1	výsledný
produkt	produkt	k1gInSc1	produkt
(	(	kIx(	(
<g/>
křemíkový	křemíkový	k2eAgInSc1d1	křemíkový
ingot	ingot	k1gInSc1	ingot
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
až	až	k9	až
400	[number]	k4	400
mm	mm	kA	mm
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
a	a	k8xC	a
délku	délka	k1gFnSc4	délka
do	do	k7c2	do
2	[number]	k4	2
m	m	kA	m
<g/>
,	,	kIx,	,
tvořen	tvořen	k2eAgInSc1d1	tvořen
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
jediným	jediný	k2eAgInSc7d1	jediný
krystalem	krystal	k1gInSc7	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
ingot	ingot	k1gInSc1	ingot
se	se	k3xPyFc4	se
po	po	k7c4	po
ochlazení	ochlazení	k1gNnSc4	ochlazení
řeže	řež	k1gFnSc2	řež
na	na	k7c4	na
tenké	tenký	k2eAgFnPc4d1	tenká
vrstvy	vrstva	k1gFnPc4	vrstva
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
0,5	[number]	k4	0,5
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
leští	leštit	k5eAaImIp3nS	leštit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
použit	použít	k5eAaPmNgInS	použít
jako	jako	k8xC	jako
výchozí	výchozí	k2eAgFnSc1d1	výchozí
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
polovodičových	polovodičový	k2eAgFnPc2d1	polovodičová
součástek	součástka	k1gFnPc2	součástka
<g/>
.	.	kIx.	.
</s>
<s>
Metalurgický	metalurgický	k2eAgInSc1d1	metalurgický
význam	význam	k1gInSc1	význam
křemíku	křemík	k1gInSc2	křemík
spočívá	spočívat	k5eAaImIp3nS	spočívat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
některých	některý	k3yIgFnPc2	některý
speciálních	speciální	k2eAgFnPc2d1	speciální
slitin	slitina	k1gFnPc2	slitina
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
jeho	jeho	k3xOp3gFnSc1	jeho
podíl	podíl	k1gInSc4	podíl
představuje	představovat	k5eAaImIp3nS	představovat
pouze	pouze	k6eAd1	pouze
jednotky	jednotka	k1gFnPc4	jednotka
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgNnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
ferrosilicium	ferrosilicium	k1gNnSc1	ferrosilicium
<g/>
,	,	kIx,	,
slitina	slitina	k1gFnSc1	slitina
křemíku	křemík	k1gInSc2	křemík
a	a	k8xC	a
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
tvrdostí	tvrdost	k1gFnSc7	tvrdost
a	a	k8xC	a
chemickou	chemický	k2eAgFnSc7d1	chemická
odolností	odolnost	k1gFnSc7	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
tvrdosti	tvrdost	k1gFnSc2	tvrdost
se	se	k3xPyFc4	se
křemík	křemík	k1gInSc1	křemík
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
přidává	přidávat	k5eAaImIp3nS	přidávat
i	i	k9	i
do	do	k7c2	do
speciálních	speciální	k2eAgFnPc2d1	speciální
ocelí	ocel	k1gFnPc2	ocel
a	a	k8xC	a
hliníkových	hliníkový	k2eAgFnPc2d1	hliníková
slitin	slitina	k1gFnPc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1	chemická
rafinace	rafinace	k1gFnSc1	rafinace
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
převedení	převedení	k1gNnSc1	převedení
na	na	k7c4	na
sloučeninu	sloučenina	k1gFnSc4	sloučenina
trichlorsilanu	trichlorsilanout	k5eAaPmIp1nS	trichlorsilanout
Si	se	k3xPyFc3	se
<g/>
3	[number]	k4	3
<g/>
H	H	kA	H
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
destilace	destilace	k1gFnSc1	destilace
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
odstraní	odstranit	k5eAaPmIp3nS	odstranit
většina	většina	k1gFnSc1	většina
nečistot	nečistota	k1gFnPc2	nečistota
<g/>
.	.	kIx.	.
</s>
<s>
Fyzické	fyzický	k2eAgFnPc1d1	fyzická
rafinace	rafinace	k1gFnPc1	rafinace
<g/>
:	:	kIx,	:
Pásmové	pásmový	k2eAgNnSc1d1	pásmové
tavení	tavení	k1gNnSc1	tavení
<g/>
:	:	kIx,	:
Ingot	ingot	k1gInSc1	ingot
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
natavuje	natavovat	k5eAaImIp3nS	natavovat
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
svojí	svůj	k3xOyFgFnSc6	svůj
délce	délka	k1gFnSc6	délka
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
postaven	postavit	k5eAaPmNgInS	postavit
kolmo	kolmo	k6eAd1	kolmo
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nečistoty	nečistota	k1gFnPc1	nečistota
mají	mít	k5eAaImIp3nP	mít
jinou	jiný	k2eAgFnSc4d1	jiná
hustotu	hustota	k1gFnSc4	hustota
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
tavení	tavení	k1gNnSc2	tavení
zatuhnou	zatuhnout	k5eAaPmIp3nP	zatuhnout
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
konci	konec	k1gInSc6	konec
křemíkové	křemíkový	k2eAgFnSc2d1	křemíková
šišky	šiška	k1gFnSc2	šiška
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
jsou	být	k5eAaImIp3nP	být
odříznuty	odříznout	k5eAaPmNgFnP	odříznout
<g/>
.	.	kIx.	.
</s>
<s>
Několikanásobným	několikanásobný	k2eAgNnSc7d1	několikanásobné
opakováním	opakování	k1gNnSc7	opakování
tohoto	tento	k3xDgInSc2	tento
postupu	postup	k1gInSc2	postup
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
poměrně	poměrně	k6eAd1	poměrně
vysoce	vysoce	k6eAd1	vysoce
čistý	čistý	k2eAgInSc4d1	čistý
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
určitý	určitý	k2eAgInSc1d1	určitý
typ	typ	k1gInSc1	typ
polovodiče	polovodič	k1gInSc2	polovodič
<g/>
,	,	kIx,	,
P	P	kA	P
nebo	nebo	k8xC	nebo
N	N	kA	N
<g/>
,	,	kIx,	,
dotuje	dotovat	k5eAaBmIp3nS	dotovat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
před	před	k7c7	před
tažením	tažení	k1gNnSc7	tažení
určitým	určitý	k2eAgInSc7d1	určitý
vodičem	vodič	k1gInSc7	vodič
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tyč	tyč	k1gFnSc1	tyč
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
vertikálně	vertikálně	k6eAd1	vertikálně
donorem	donor	k1gInSc7	donor
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
akceptorem	akceptor	k1gInSc7	akceptor
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
docílí	docílit	k5eAaPmIp3nP	docílit
vodivosti	vodivost	k1gFnPc1	vodivost
P	P	kA	P
nebo	nebo	k8xC	nebo
N.	N.	kA	N.
Řezání	řezání	k1gNnPc2	řezání
na	na	k7c4	na
,,	,,	k?	,,
<g/>
Salámky	salámek	k1gInPc4	salámek
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Po	po	k7c6	po
vytažení	vytažení	k1gNnSc6	vytažení
z	z	k7c2	z
kelímku	kelímek	k1gInSc2	kelímek
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
hotový	hotový	k2eAgInSc4d1	hotový
polovodič	polovodič	k1gInSc4	polovodič
určeného	určený	k2eAgInSc2d1	určený
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rozřezává	rozřezávat	k5eAaImIp3nS	rozřezávat
na	na	k7c4	na
určené	určený	k2eAgInPc4d1	určený
plátky	plátek	k1gInPc4	plátek
<g/>
,	,	kIx,	,
takzvané	takzvaný	k2eAgInPc4d1	takzvaný
salámky	salámek	k1gInPc4	salámek
<g/>
.	.	kIx.	.
</s>
<s>
Salámky	salámek	k1gInPc1	salámek
se	se	k3xPyFc4	se
řežou	řezat	k5eAaImIp3nP	řezat
diamantovým	diamantový	k2eAgMnSc7d1	diamantový
řezačem	řezač	k1gMnSc7	řezač
<g/>
,	,	kIx,	,
podobným	podobný	k2eAgInSc7d1	podobný
strojem	stroj	k1gInSc7	stroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
salám	salám	k1gInSc4	salám
nebo	nebo	k8xC	nebo
chléb	chléb	k1gInSc4	chléb
<g/>
.	.	kIx.	.
</s>
<s>
Broušení	broušení	k1gNnSc1	broušení
-	-	kIx~	-
Lapování	Lapování	k1gNnSc1	Lapování
<g/>
:	:	kIx,	:
Lapování	Lapování	k1gNnSc1	Lapování
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
strojního	strojní	k2eAgNnSc2d1	strojní
obrábění	obrábění	k1gNnSc2	obrábění
<g/>
.	.	kIx.	.
</s>
<s>
Technicky	technicky	k6eAd1	technicky
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
broušení	broušení	k1gNnSc2	broušení
a	a	k8xC	a
leštění	leštění	k1gNnSc2	leštění
<g/>
,	,	kIx,	,
neupravuje	upravovat	k5eNaImIp3nS	upravovat
se	se	k3xPyFc4	se
jím	on	k3xPp3gInSc7	on
geometrie	geometrie	k1gFnSc1	geometrie
ale	ale	k8xC	ale
drsnost	drsnost	k1gFnSc1	drsnost
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
až	až	k9	až
zrcadlový	zrcadlový	k2eAgInSc4d1	zrcadlový
lesk	lesk	k1gInSc4	lesk
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
dokončovací	dokončovací	k2eAgFnSc6d1	dokončovací
operaci	operace	k1gFnSc6	operace
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
úběru	úběr	k1gInSc3	úběr
materiálu	materiál	k1gInSc2	materiál
účinkem	účinek	k1gInSc7	účinek
volných	volný	k2eAgNnPc2d1	volné
zrn	zrno	k1gNnPc2	zrno
brusiva	brusivo	k1gNnSc2	brusivo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
přivádí	přivádět	k5eAaImIp3nS	přivádět
mezi	mezi	k7c4	mezi
lapovací	lapovací	k2eAgInSc4d1	lapovací
nástroj	nástroj	k1gInSc4	nástroj
a	a	k8xC	a
lapovaný	lapovaný	k2eAgInSc4d1	lapovaný
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Řezný	řezný	k2eAgInSc1d1	řezný
pohyb	pohyb	k1gInSc1	pohyb
volných	volný	k2eAgNnPc2d1	volné
zrn	zrno	k1gNnPc2	zrno
je	být	k5eAaImIp3nS	být
vyvolán	vyvolat	k5eAaPmNgInS	vyvolat
pohybem	pohyb	k1gInSc7	pohyb
lapovacího	lapovací	k2eAgInSc2d1	lapovací
nástroje	nástroj	k1gInSc2	nástroj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
vůči	vůči	k7c3	vůči
lapované	lapovaný	k2eAgFnSc3d1	lapovaná
ploše	plocha	k1gFnSc3	plocha
nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
zrna	zrno	k1gNnSc2	zrno
brusiva	brusivo	k1gNnSc2	brusivo
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
měnících	měnící	k2eAgFnPc6d1	měnící
drahách	draha	k1gFnPc6	draha
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zanikají	zanikat	k5eAaImIp3nP	zanikat
stopy	stopa	k1gFnPc1	stopa
po	po	k7c6	po
předchozím	předchozí	k2eAgNnSc6d1	předchozí
obrábění	obrábění	k1gNnSc6	obrábění
<g/>
.	.	kIx.	.
</s>
<s>
Odleptání	odleptání	k1gNnSc1	odleptání
<g/>
:	:	kIx,	:
I	i	k9	i
přes	přes	k7c4	přes
dokonalé	dokonalý	k2eAgNnSc4d1	dokonalé
obroušení	obroušení	k1gNnSc4	obroušení
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
salámek	salámek	k1gInSc4	salámek
vyhladit	vyhladit	k5eAaPmF	vyhladit
leptáním	leptání	k1gNnSc7	leptání
slabou	slabý	k2eAgFnSc7d1	slabá
kyselinou	kyselina	k1gFnSc7	kyselina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zarovná	zarovnat	k5eAaPmIp3nS	zarovnat
veškeré	veškerý	k3xTgFnPc4	veškerý
nerovnosti	nerovnost	k1gFnPc4	nerovnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
omytí	omytí	k1gNnSc6	omytí
a	a	k8xC	a
vyleštění	vyleštění	k1gNnSc6	vyleštění
po	po	k7c6	po
odleptání	odleptání	k1gNnSc6	odleptání
je	být	k5eAaImIp3nS	být
salámek	salámek	k1gInSc1	salámek
omyt	omyt	k2eAgInSc1d1	omyt
v	v	k7c6	v
deionizované	deionizovaný	k2eAgFnSc6d1	deionizovaná
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
odstranily	odstranit	k5eAaPmAgInP	odstranit
zbytky	zbytek	k1gInPc1	zbytek
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
salámek	salámek	k1gInSc1	salámek
vyleštěn	vyleštěn	k2eAgInSc1d1	vyleštěn
do	do	k7c2	do
vysokého	vysoký	k2eAgInSc2d1	vysoký
lesku	lesk	k1gInSc2	lesk
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
připraven	připraven	k2eAgInSc1d1	připraven
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
použití	použití	k1gNnSc3	použití
<g/>
.	.	kIx.	.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1	čistý
křemík	křemík	k1gInSc1	křemík
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
pásové	pásový	k2eAgFnSc2d1	pásová
struktury	struktura	k1gFnSc2	struktura
vlastnosti	vlastnost	k1gFnSc2	vlastnost
polovodiče	polovodič	k1gInSc2	polovodič
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
polovodivých	polovodivý	k2eAgFnPc2d1	polovodivá
aplikací	aplikace	k1gFnPc2	aplikace
se	se	k3xPyFc4	se
však	však	k9	však
častěji	často	k6eAd2	často
používá	používat	k5eAaImIp3nS	používat
křemík	křemík	k1gInSc1	křemík
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
jiného	jiný	k2eAgInSc2d1	jiný
prvku	prvek	k1gInSc2	prvek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
elektronovou	elektronový	k2eAgFnSc4d1	elektronová
resp.	resp.	kA	resp.
děrovou	děrová	k1gFnSc4	děrová
vodivost	vodivost	k1gFnSc4	vodivost
a	a	k8xC	a
snížení	snížení	k1gNnSc4	snížení
elektrického	elektrický	k2eAgInSc2d1	elektrický
odporu	odpor	k1gInSc2	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k9	tak
polovodič	polovodič	k1gInSc1	polovodič
typu	typ	k1gInSc2	typ
n	n	k0	n
resp.	resp.	kA	resp.
p	p	k?	p
<g/>
;	;	kIx,	;
takovýto	takovýto	k3xDgInSc1	takovýto
materiál	materiál	k1gInSc1	materiál
tvoří	tvořit	k5eAaImIp3nS	tvořit
základ	základ	k1gInSc4	základ
součástek	součástka	k1gFnPc2	součástka
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
diody	dioda	k1gFnPc1	dioda
<g/>
,	,	kIx,	,
tranzistory	tranzistor	k1gInPc1	tranzistor
<g/>
,	,	kIx,	,
fotovoltaické	fotovoltaický	k2eAgInPc1d1	fotovoltaický
články	článek	k1gInPc1	článek
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Oxid	oxid	k1gInSc1	oxid
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
anorganickou	anorganický	k2eAgFnSc7d1	anorganická
sloučeninou	sloučenina	k1gFnSc7	sloučenina
křemíku	křemík	k1gInSc2	křemík
je	být	k5eAaImIp3nS	být
oxid	oxid	k1gInSc1	oxid
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
<g/>
,	,	kIx,	,
SiO	SiO	k1gFnSc1	SiO
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
modifikací	modifikace	k1gFnPc2	modifikace
se	s	k7c7	s
zcela	zcela	k6eAd1	zcela
odlišnými	odlišný	k2eAgFnPc7d1	odlišná
fyzikálně-chemickými	fyzikálněhemický	k2eAgFnPc7d1	fyzikálně-chemická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Minerály	minerál	k1gInPc1	minerál
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
SiO	SiO	k1gFnSc1	SiO
<g/>
2	[number]	k4	2
se	se	k3xPyFc4	se
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
polodrahokamů	polodrahokam	k1gInPc2	polodrahokam
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
nejrůznějších	různý	k2eAgInPc6d3	nejrůznější
barevných	barevný	k2eAgInPc6d1	barevný
odstínech	odstín	k1gInPc6	odstín
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
<g/>
:	:	kIx,	:
fialová	fialový	k2eAgFnSc1d1	fialová
-	-	kIx~	-
ametyst	ametyst	k1gInSc1	ametyst
<g/>
,	,	kIx,	,
hnědá	hnědý	k2eAgFnSc1d1	hnědá
-	-	kIx~	-
záhněda	záhněda	k1gFnSc1	záhněda
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
-	-	kIx~	-
citrin	citrin	k1gInSc1	citrin
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
barvy	barva	k1gFnSc2	barva
-	-	kIx~	-
křišťál	křišťál	k1gInSc1	křišťál
Ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
okolních	okolní	k2eAgFnPc2d1	okolní
hornin	hornina	k1gFnPc2	hornina
je	být	k5eAaImIp3nS	být
křemen	křemen	k1gInSc1	křemen
přítomen	přítomen	k2eAgInSc1d1	přítomen
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
žil	žíla	k1gFnPc2	žíla
a	a	k8xC	a
vrostlic	vrostlice	k1gFnPc2	vrostlice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
erozi	eroze	k1gFnSc6	eroze
hornin	hornina	k1gFnPc2	hornina
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
narušení	narušení	k1gNnSc3	narušení
její	její	k3xOp3gFnSc2	její
struktury	struktura	k1gFnSc2	struktura
a	a	k8xC	a
křemen	křemen	k1gInSc1	křemen
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejtvrdších	tvrdý	k2eAgFnPc2d3	nejtvrdší
a	a	k8xC	a
nejodolnějších	odolný	k2eAgFnPc2d3	nejodolnější
součástí	součást	k1gFnPc2	součást
z	z	k7c2	z
horniny	hornina	k1gFnSc2	hornina
vyplavován	vyplavovat	k5eAaImNgInS	vyplavovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
křemenných	křemenný	k2eAgInPc2d1	křemenný
písků	písek	k1gInPc2	písek
<g/>
,	,	kIx,	,
oblázků	oblázek	k1gInPc2	oblázek
a	a	k8xC	a
valounů	valoun	k1gInPc2	valoun
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
materiál	materiál	k1gInSc1	materiál
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
cenná	cenný	k2eAgFnSc1d1	cenná
surovina	surovina	k1gFnSc1	surovina
ve	v	k7c6	v
sklářském	sklářský	k2eAgInSc6d1	sklářský
a	a	k8xC	a
stavebním	stavební	k2eAgInSc6d1	stavební
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jílových	jílový	k2eAgFnPc6d1	jílová
horninách	hornina	k1gFnPc6	hornina
je	být	k5eAaImIp3nS	být
křemík	křemík	k1gInSc1	křemík
přítomen	přítomen	k2eAgInSc1d1	přítomen
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
mikroskopických	mikroskopický	k2eAgFnPc2d1	mikroskopická
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
horniny	hornina	k1gFnPc1	hornina
jsou	být	k5eAaImIp3nP	být
základní	základní	k2eAgFnSc7d1	základní
surovinou	surovina	k1gFnSc7	surovina
v	v	k7c6	v
keramickém	keramický	k2eAgInSc6d1	keramický
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
se	se	k3xPyFc4	se
i	i	k9	i
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
stavebních	stavební	k2eAgFnPc2d1	stavební
hmot	hmota	k1gFnPc2	hmota
(	(	kIx(	(
<g/>
pálené	pálený	k2eAgFnPc1d1	pálená
cihly	cihla	k1gFnPc1	cihla
a	a	k8xC	a
tašky	taška	k1gFnPc1	taška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nP	vyrábět
stovky	stovka	k1gFnPc1	stovka
druhů	druh	k1gInPc2	druh
skla	sklo	k1gNnSc2	sklo
pro	pro	k7c4	pro
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
praktické	praktický	k2eAgFnPc4d1	praktická
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
fyzikálními	fyzikální	k2eAgFnPc7d1	fyzikální
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
i	i	k8xC	i
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
skloviny	sklovina	k1gFnSc2	sklovina
je	být	k5eAaImIp3nS	být
směs	směs	k1gFnSc1	směs
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
sklářský	sklářský	k2eAgInSc4d1	sklářský
kmen	kmen	k1gInSc4	kmen
o	o	k7c6	o
přibližném	přibližný	k2eAgNnSc6d1	přibližné
složení	složení	k1gNnSc6	složení
<g/>
:	:	kIx,	:
50	[number]	k4	50
<g/>
%	%	kIx~	%
písek	písek	k1gInSc1	písek
(	(	kIx(	(
<g/>
křemen	křemen	k1gInSc1	křemen
nebo	nebo	k8xC	nebo
oxid	oxid	k1gInSc1	oxid
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
%	%	kIx~	%
soda	soda	k1gFnSc1	soda
(	(	kIx(	(
<g/>
uhličitan	uhličitan	k1gInSc1	uhličitan
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
%	%	kIx~	%
vápenec	vápenec	k1gInSc1	vápenec
(	(	kIx(	(
<g/>
uhličitan	uhličitan	k1gInSc1	uhličitan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
%	%	kIx~	%
odpadní	odpadní	k2eAgNnSc1d1	odpadní
sklo	sklo	k1gNnSc1	sklo
(	(	kIx(	(
<g/>
drcené	drcený	k2eAgInPc1d1	drcený
střepy	střep	k1gInPc1	střep
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
směs	směs	k1gFnSc1	směs
taví	tavit	k5eAaImIp3nS	tavit
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
kolem	kolem	k7c2	kolem
1	[number]	k4	1
500	[number]	k4	500
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
především	především	k9	především
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
lahví	lahev	k1gFnPc2	lahev
litím	lití	k1gNnSc7	lití
nebo	nebo	k8xC	nebo
foukáním	foukání	k1gNnSc7	foukání
<g/>
.	.	kIx.	.
</s>
<s>
Přídavkem	přídavek	k1gInSc7	přídavek
potaše	potaš	k1gInSc2	potaš
<g/>
,	,	kIx,	,
uhličitanu	uhličitan	k1gInSc2	uhličitan
draselného	draselný	k2eAgInSc2d1	draselný
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
tabulové	tabulový	k2eAgNnSc1d1	tabulové
sklo	sklo	k1gNnSc1	sklo
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
oken	okno	k1gNnPc2	okno
<g/>
,	,	kIx,	,
výkladních	výkladní	k2eAgFnPc2d1	výkladní
skříní	skříň	k1gFnPc2	skříň
apod.	apod.	kA	apod.
Sklovina	sklovina	k1gFnSc1	sklovina
přitom	přitom	k6eAd1	přitom
tuhne	tuhnout	k5eAaImIp3nS	tuhnout
na	na	k7c6	na
vrstvě	vrstva	k1gFnSc6	vrstva
roztaveného	roztavený	k2eAgInSc2d1	roztavený
cínu	cín	k1gInSc2	cín
a	a	k8xC	a
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
tabule	tabule	k1gFnSc2	tabule
mají	mít	k5eAaImIp3nP	mít
zvlášť	zvlášť	k6eAd1	zvlášť
hladký	hladký	k2eAgInSc4d1	hladký
povrch	povrch	k1gInSc4	povrch
-	-	kIx~	-
plavené	plavený	k2eAgNnSc4d1	plavené
sklo	sklo	k1gNnSc4	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Sklo	sklo	k1gNnSc1	sklo
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
obsahem	obsah	k1gInSc7	obsah
olova	olovo	k1gNnSc2	olovo
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
vysokým	vysoký	k2eAgInSc7d1	vysoký
indexem	index	k1gInSc7	index
lomu	lom	k1gInSc2	lom
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
těžké	těžký	k2eAgNnSc1d1	těžké
<g/>
.	.	kIx.	.
</s>
<s>
Olovnaté	olovnatý	k2eAgNnSc1d1	olovnaté
sklo	sklo	k1gNnSc1	sklo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
lustrů	lustr	k1gInPc2	lustr
<g/>
,	,	kIx,	,
bižuterie	bižuterie	k1gFnSc2	bižuterie
<g/>
,	,	kIx,	,
ozdobných	ozdobný	k2eAgFnPc2d1	ozdobná
karaf	karafa	k1gFnPc2	karafa
a	a	k8xC	a
sklenic	sklenice	k1gFnPc2	sklenice
<g/>
.	.	kIx.	.
</s>
<s>
Sklo	sklo	k1gNnSc1	sklo
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
optických	optický	k2eAgInPc2d1	optický
přístrojů	přístroj	k1gInPc2	přístroj
(	(	kIx(	(
<g/>
čočky	čočka	k1gFnSc2	čočka
<g/>
,	,	kIx,	,
hranoly	hranol	k1gInPc4	hranol
<g/>
,	,	kIx,	,
optické	optický	k2eAgInPc4d1	optický
filtry	filtr	k1gInPc4	filtr
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kromě	kromě	k7c2	kromě
olova	olovo	k1gNnSc2	olovo
i	i	k9	i
baryum	baryum	k1gNnSc1	baryum
<g/>
,	,	kIx,	,
zinek	zinek	k1gInSc1	zinek
a	a	k8xC	a
titan	titan	k1gInSc1	titan
<g/>
.	.	kIx.	.
</s>
<s>
Skla	sklo	k1gNnPc1	sklo
borosilikátová	borosilikátový	k2eAgNnPc1d1	borosilikátové
mají	mít	k5eAaImIp3nP	mít
část	část	k1gFnSc4	část
sklotvorného	sklotvorný	k2eAgMnSc2d1	sklotvorný
SiO	SiO	k1gMnSc2	SiO
<g/>
2	[number]	k4	2
nahrazenu	nahrazen	k2eAgFnSc4d1	nahrazena
oxidem	oxid	k1gInSc7	oxid
boritým	boritý	k2eAgInSc7d1	boritý
<g/>
.	.	kIx.	.
</s>
<s>
Přísada	přísada	k1gFnSc1	přísada
oxidu	oxid	k1gInSc2	oxid
hlinitého	hlinitý	k2eAgInSc2d1	hlinitý
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
jejich	jejich	k3xOp3gFnSc4	jejich
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
zpracovatelnost	zpracovatelnost	k1gFnSc4	zpracovatelnost
skloviny	sklovina	k1gFnSc2	sklovina
<g/>
.	.	kIx.	.
</s>
<s>
Borosilikátová	Borosilikátový	k2eAgNnPc1d1	Borosilikátové
skla	sklo	k1gNnPc1	sklo
jsou	být	k5eAaImIp3nP	být
žáruvzdorná	žáruvzdorný	k2eAgNnPc1d1	žáruvzdorné
a	a	k8xC	a
chemicky	chemicky	k6eAd1	chemicky
odolná	odolný	k2eAgNnPc1d1	odolné
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
sklo	sklo	k1gNnSc1	sklo
laboratorní	laboratorní	k2eAgFnSc2d1	laboratorní
a	a	k8xC	a
varné	varný	k2eAgFnSc2d1	varná
využívané	využívaný	k2eAgFnSc2d1	využívaná
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
je	být	k5eAaImIp3nS	být
křemenné	křemenný	k2eAgNnSc4d1	křemenné
sklo	sklo	k1gNnSc4	sklo
<g/>
,	,	kIx,	,
tavený	tavený	k2eAgInSc4d1	tavený
čistý	čistý	k2eAgInSc4d1	čistý
oxid	oxid	k1gInSc4	oxid
křemičitý	křemičitý	k2eAgInSc4d1	křemičitý
SiO	SiO	k1gFnSc7	SiO
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Propouští	propouštět	k5eAaImIp3nS	propouštět
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
výbornou	výborný	k2eAgFnSc4d1	výborná
chemickou	chemický	k2eAgFnSc4d1	chemická
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
odolnost	odolnost	k1gFnSc4	odolnost
a	a	k8xC	a
snese	snést	k5eAaPmIp3nS	snést
prudké	prudký	k2eAgNnSc1d1	prudké
ochlazení	ochlazení	k1gNnSc1	ochlazení
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
popraská	popraskat	k5eAaPmIp3nS	popraskat
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
tavicí	tavicí	k2eAgFnSc1d1	tavicí
teplota	teplota	k1gFnSc1	teplota
kolem	kolem	k7c2	kolem
1	[number]	k4	1
800	[number]	k4	800
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojená	spojený	k2eAgFnSc1d1	spojená
cena	cena	k1gFnSc1	cena
křemenného	křemenný	k2eAgNnSc2d1	křemenné
skla	sklo	k1gNnSc2	sklo
omezuje	omezovat	k5eAaImIp3nS	omezovat
jeho	jeho	k3xOp3gNnSc4	jeho
praktické	praktický	k2eAgNnSc4d1	praktické
využití	využití	k1gNnSc4	využití
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
laboratorních	laboratorní	k2eAgFnPc2d1	laboratorní
potřeb	potřeba	k1gFnPc2	potřeba
a	a	k8xC	a
speciálních	speciální	k2eAgFnPc2d1	speciální
žárovek	žárovka	k1gFnPc2	žárovka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
optické	optický	k2eAgInPc4d1	optický
kabely	kabel	k1gInPc4	kabel
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
obzvlášť	obzvlášť	k6eAd1	obzvlášť
čisté	čistý	k2eAgNnSc1d1	čisté
a	a	k8xC	a
průhledné	průhledný	k2eAgNnSc1d1	průhledné
sklo	sklo	k1gNnSc1	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Křemík	křemík	k1gInSc1	křemík
tvoří	tvořit	k5eAaImIp3nS	tvořit
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
kyslíkatých	kyslíkatý	k2eAgFnPc2d1	kyslíkatá
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
a	a	k8xC	a
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
kyselina	kyselina	k1gFnSc1	kyselina
křemičitá	křemičitý	k2eAgFnSc1d1	křemičitá
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
SiO	SiO	k1gFnSc2	SiO
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
kyselinách	kyselina	k1gFnPc6	kyselina
jsou	být	k5eAaImIp3nP	být
za	za	k7c7	za
sebou	se	k3xPyFc7	se
řetězeny	řetězen	k2eAgFnPc4d1	řetězen
skupiny	skupina	k1gFnSc2	skupina
[	[	kIx(	[
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
uvedené	uvedený	k2eAgFnPc1d1	uvedená
kyseliny	kyselina	k1gFnPc1	kyselina
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
slabé	slabý	k2eAgFnPc1d1	slabá
a	a	k8xC	a
nestálé	stálý	k2eNgFnPc1d1	nestálá
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
však	však	k9	však
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
jejich	jejich	k3xOp3gFnPc7	jejich
solemi	sůl	k1gFnPc7	sůl
křemičitany	křemičitan	k1gInPc1	křemičitan
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
stabilní	stabilní	k2eAgFnPc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
Křemičitany	křemičitan	k1gInPc1	křemičitan
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
kovů	kov	k1gInPc2	kov
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
jsou	být	k5eAaImIp3nP	být
podstatnou	podstatný	k2eAgFnSc7d1	podstatná
součástí	součást	k1gFnSc7	součást
vyvřelých	vyvřelý	k2eAgFnPc2d1	vyvřelá
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
jílů	jíl	k1gInPc2	jíl
<g/>
,	,	kIx,	,
cihlářských	cihlářský	k2eAgFnPc2d1	Cihlářská
hlín	hlína	k1gFnPc2	hlína
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
běžné	běžný	k2eAgFnPc1d1	běžná
jsou	být	k5eAaImIp3nP	být
horniny	hornina	k1gFnPc1	hornina
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
hlinitokřemičitanů	hlinitokřemičitan	k1gInPc2	hlinitokřemičitan
(	(	kIx(	(
<g/>
aluminosilikátů	aluminosilikát	k1gInPc2	aluminosilikát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
živce	živec	k1gInSc2	živec
<g/>
.	.	kIx.	.
</s>
<s>
Aluminosilátové	Aluminosilátový	k2eAgInPc1d1	Aluminosilátový
minerály	minerál	k1gInPc1	minerál
tvoří	tvořit	k5eAaImIp3nP	tvořit
např.	např.	kA	např.
orthoklas	orthoklas	k1gInSc1	orthoklas
KAlSi	KAlSi	k1gNnSc1	KAlSi
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
8	[number]	k4	8
a	a	k8xC	a
plagioklas	plagioklas	k1gInSc1	plagioklas
NaAlSi	NaAlS	k1gFnSc2	NaAlS
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
8	[number]	k4	8
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
cenné	cenný	k2eAgInPc1d1	cenný
jsou	být	k5eAaImIp3nP	být
aluminosilikáty	aluminosilikát	k1gInPc1	aluminosilikát
zvané	zvaný	k2eAgInPc1d1	zvaný
zeolity	zeolit	k1gInPc1	zeolit
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
komplikované	komplikovaný	k2eAgFnPc4d1	komplikovaná
prostorové	prostorový	k2eAgFnPc4d1	prostorová
sítě	síť	k1gFnPc4	síť
<g/>
,	,	kIx,	,
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
tetraedrů	tetraedr	k1gInPc2	tetraedr
SiO	SiO	k1gFnSc2	SiO
<g/>
4	[number]	k4	4
a	a	k8xC	a
AlO	ala	k1gFnSc5	ala
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
vázaných	vázaný	k2eAgFnPc2d1	vázaná
navzájem	navzájem	k6eAd1	navzájem
sdílením	sdílení	k1gNnSc7	sdílení
svých	svůj	k3xOyFgInPc2	svůj
vrcholových	vrcholový	k2eAgInPc2d1	vrcholový
kyslíků	kyslík	k1gInPc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
vzájemně	vzájemně	k6eAd1	vzájemně
propojené	propojený	k2eAgInPc1d1	propojený
kanály	kanál	k1gInPc1	kanál
a	a	k8xC	a
dutiny	dutina	k1gFnPc1	dutina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
slabě	slabě	k6eAd1	slabě
vázané	vázaný	k2eAgFnPc1d1	vázaná
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
pohyblivé	pohyblivý	k2eAgFnSc2d1	pohyblivá
molekuly	molekula	k1gFnSc2	molekula
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
kationy	kation	k1gInPc4	kation
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
(	(	kIx(	(
<g/>
Na	na	k7c4	na
<g/>
,	,	kIx,	,
K	K	kA	K
<g/>
,	,	kIx,	,
Li	li	k9	li
<g/>
,	,	kIx,	,
Cs	Cs	k1gFnSc1	Cs
<g/>
)	)	kIx)	)
a	a	k8xC	a
alkalických	alkalický	k2eAgFnPc2d1	alkalická
zemin	zemina	k1gFnPc2	zemina
(	(	kIx(	(
<g/>
Ca	ca	kA	ca
<g/>
,	,	kIx,	,
Mg	mg	kA	mg
<g/>
,	,	kIx,	,
Ba	ba	k9	ba
<g/>
,	,	kIx,	,
Sr	Sr	k1gMnSc5	Sr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyrovnávají	vyrovnávat	k5eAaImIp3nP	vyrovnávat
nenasycenou	nasycený	k2eNgFnSc4d1	nenasycená
negativní	negativní	k2eAgFnSc4d1	negativní
valenci	valence	k1gFnSc4	valence
AlO	alo	k0wR	alo
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Zeolity	zeolit	k1gInPc1	zeolit
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
jako	jako	k9	jako
přírodní	přírodní	k2eAgInPc1d1	přírodní
iontoměniče	iontoměnič	k1gInPc1	iontoměnič
nebo	nebo	k8xC	nebo
molekulová	molekulový	k2eAgNnPc1d1	molekulové
síta	síto	k1gNnPc1	síto
<g/>
.	.	kIx.	.
</s>
<s>
Keramika	keramika	k1gFnSc1	keramika
je	být	k5eAaImIp3nS	být
obecný	obecný	k2eAgInSc4d1	obecný
název	název	k1gInSc4	název
pro	pro	k7c4	pro
výrobky	výrobek	k1gInPc4	výrobek
zhotovené	zhotovený	k2eAgNnSc4d1	zhotovené
vypalováním	vypalování	k1gNnSc7	vypalování
keramických	keramický	k2eAgFnPc2d1	keramická
směsí	směs	k1gFnPc2	směs
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc7	jejichž
hlavními	hlavní	k2eAgFnPc7d1	hlavní
složkami	složka	k1gFnPc7	složka
jsou	být	k5eAaImIp3nP	být
kaolíny	kaolín	k1gInPc4	kaolín
<g/>
,	,	kIx,	,
jíly	jíl	k1gInPc4	jíl
a	a	k8xC	a
hlíny	hlína	k1gFnPc4	hlína
<g/>
.	.	kIx.	.
</s>
<s>
Keramické	keramický	k2eAgFnPc1d1	keramická
směsi	směs	k1gFnPc1	směs
získají	získat	k5eAaPmIp3nP	získat
po	po	k7c6	po
prohnětení	prohnětení	k1gNnSc6	prohnětení
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
plastické	plastický	k2eAgFnSc2d1	plastická
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
tvarovat	tvarovat	k5eAaImF	tvarovat
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
po	po	k7c4	po
vypálení	vypálení	k1gNnSc4	vypálení
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
800	[number]	k4	800
až	až	k9	až
1500	[number]	k4	1500
°	°	k?	°
<g/>
C	C	kA	C
plastické	plastický	k2eAgFnPc4d1	plastická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
a	a	k8xC	a
mění	měnit	k5eAaImIp3nP	měnit
se	se	k3xPyFc4	se
v	v	k7c4	v
trvale	trvale	k6eAd1	trvale
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
látku	látka	k1gFnSc4	látka
zvanou	zvaný	k2eAgFnSc4d1	zvaná
střep	střep	k1gInSc1	střep
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
ceněnou	ceněný	k2eAgFnSc7d1	ceněná
keramickou	keramický	k2eAgFnSc7d1	keramická
hmotou	hmota	k1gFnSc7	hmota
je	být	k5eAaImIp3nS	být
porcelán	porcelán	k1gInSc1	porcelán
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
vstupní	vstupní	k2eAgFnPc1d1	vstupní
suroviny	surovina	k1gFnPc1	surovina
tvoří	tvořit	k5eAaImIp3nP	tvořit
směs	směs	k1gFnSc4	směs
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
průměrně	průměrně	k6eAd1	průměrně
50	[number]	k4	50
%	%	kIx~	%
nejčistšího	čistý	k2eAgInSc2d3	nejčistší
kaolínu	kaolín	k1gInSc2	kaolín
<g/>
,	,	kIx,	,
25	[number]	k4	25
%	%	kIx~	%
křemenného	křemenný	k2eAgInSc2d1	křemenný
písku	písek	k1gInSc2	písek
a	a	k8xC	a
25	[number]	k4	25
%	%	kIx~	%
živce	živec	k1gInSc2	živec
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
známý	známý	k2eAgInSc1d1	známý
a	a	k8xC	a
ceněný	ceněný	k2eAgInSc1d1	ceněný
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
míšeňský	míšeňský	k2eAgInSc1d1	míšeňský
porcelán	porcelán	k1gInSc1	porcelán
<g/>
,	,	kIx,	,
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
se	se	k3xPyFc4	se
výroba	výroba	k1gFnSc1	výroba
porcelánu	porcelán	k1gInSc2	porcelán
koncentruje	koncentrovat	k5eAaBmIp3nS	koncentrovat
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
světově	světově	k6eAd1	světově
proslulý	proslulý	k2eAgMnSc1d1	proslulý
je	být	k5eAaImIp3nS	být
karlovarský	karlovarský	k2eAgInSc1d1	karlovarský
porcelán	porcelán	k1gInSc1	porcelán
<g/>
.	.	kIx.	.
</s>
<s>
Cihlářské	cihlářský	k2eAgFnPc1d1	Cihlářská
hlíny	hlína	k1gFnPc1	hlína
jako	jako	k9	jako
méně	málo	k6eAd2	málo
hodnotné	hodnotný	k2eAgFnPc4d1	hodnotná
keramické	keramický	k2eAgFnPc4d1	keramická
suroviny	surovina	k1gFnPc4	surovina
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
cihel	cihla	k1gFnPc2	cihla
<g/>
,	,	kIx,	,
střešních	střešní	k2eAgFnPc2d1	střešní
tašek	taška	k1gFnPc2	taška
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
stavebních	stavební	k2eAgInPc2d1	stavební
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jílů	jíl	k1gInPc2	jíl
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
hodnotného	hodnotný	k2eAgInSc2d1	hodnotný
kaolinu	kaolin	k1gInSc2	kaolin
<g/>
,	,	kIx,	,
živce	živec	k1gInSc2	živec
a	a	k8xC	a
křemene	křemen	k1gInSc2	křemen
se	se	k3xPyFc4	se
vypalováním	vypalování	k1gNnSc7	vypalování
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
okolo	okolo	k7c2	okolo
1300	[number]	k4	1300
°	°	k?	°
<g/>
C	C	kA	C
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
buď	buď	k8xC	buď
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
kamenina	kamenina	k1gFnSc1	kamenina
(	(	kIx(	(
<g/>
potrubí	potrubí	k1gNnSc1	potrubí
<g/>
,	,	kIx,	,
dlaždice	dlaždice	k1gFnPc1	dlaždice
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jemná	jemný	k2eAgFnSc1d1	jemná
bílá	bílý	k2eAgFnSc1d1	bílá
kamenina	kamenina	k1gFnSc1	kamenina
(	(	kIx(	(
<g/>
talíře	talíř	k1gInPc1	talíř
<g/>
,	,	kIx,	,
umyvadla	umyvadlo	k1gNnPc1	umyvadlo
<g/>
,	,	kIx,	,
kachlíky	kachlík	k1gInPc1	kachlík
<g/>
,	,	kIx,	,
sošky	soška	k1gFnPc1	soška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
uplatnění	uplatnění	k1gNnSc4	uplatnění
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
nachází	nacházet	k5eAaImIp3nS	nacházet
křemenný	křemenný	k2eAgInSc1d1	křemenný
písek	písek	k1gInSc1	písek
jako	jako	k8xC	jako
složka	složka	k1gFnSc1	složka
malty	malta	k1gFnSc2	malta
a	a	k8xC	a
pojivých	pojivý	k2eAgInPc2d1	pojivý
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
především	především	k9	především
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
betonu	beton	k1gInSc2	beton
<g/>
.	.	kIx.	.
</s>
<s>
Křemík	křemík	k1gInSc4	křemík
tvoří	tvořit	k5eAaImIp3nP	tvořit
sloučeniny	sloučenina	k1gFnPc1	sloučenina
s	s	k7c7	s
fluorem	fluor	k1gInSc7	fluor
SiF	SiF	k1gFnSc1	SiF
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
chlorem	chlor	k1gInSc7	chlor
SiCl	SiCl	k1gInSc4	SiCl
<g/>
4	[number]	k4	4
a	a	k8xC	a
bromem	brom	k1gInSc7	brom
SiBr	SiBr	k1gInSc4	SiBr
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dále	daleko	k6eAd2	daleko
řetězit	řetězit	k5eAaImF	řetězit
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
vyšších	vysoký	k2eAgInPc2d2	vyšší
halogenidů	halogenid	k1gInPc2	halogenid
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
uvedené	uvedený	k2eAgFnPc1d1	uvedená
sloučeniny	sloučenina	k1gFnPc1	sloučenina
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
nestálé	stálý	k2eNgFnPc1d1	nestálá
a	a	k8xC	a
při	při	k7c6	při
styku	styk	k1gInSc6	styk
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
okamžitě	okamžitě	k6eAd1	okamžitě
hydrolyzují	hydrolyzovat	k5eAaBmIp3nP	hydrolyzovat
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
gelovité	gelovitý	k2eAgFnSc2d1	gelovitá
kyseliny	kyselina	k1gFnSc2	kyselina
křemičité	křemičitý	k2eAgFnSc2d1	křemičitá
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
důležitou	důležitý	k2eAgFnSc7d1	důležitá
sloučeninou	sloučenina	k1gFnSc7	sloučenina
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
čistého	čistý	k2eAgInSc2d1	čistý
křemíku	křemík	k1gInSc2	křemík
pro	pro	k7c4	pro
polovodičové	polovodičový	k2eAgInPc4d1	polovodičový
účely	účel	k1gInPc4	účel
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těkavosti	těkavost	k1gFnPc1	těkavost
fluoridu	fluorid	k1gInSc2	fluorid
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
se	se	k3xPyFc4	se
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
fluoru	fluor	k1gInSc2	fluor
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
stabilních	stabilní	k2eAgInPc2d1	stabilní
fluoridů	fluorid	k1gInPc2	fluorid
AlF	alfa	k1gFnPc2	alfa
<g/>
3	[number]	k4	3
a	a	k8xC	a
fluoridů	fluorid	k1gInPc2	fluorid
lantanoidů	lantanoid	k1gInPc2	lantanoid
<g/>
.	.	kIx.	.
</s>
<s>
Vzorek	vzorek	k1gInSc1	vzorek
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
vaří	vařit	k5eAaImIp3nP	vařit
při	při	k7c6	při
asi	asi	k9	asi
150	[number]	k4	150
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
50	[number]	k4	50
%	%	kIx~	%
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc6d1	sírová
ve	v	k7c6	v
skleněné	skleněný	k2eAgFnSc6d1	skleněná
aparatuře	aparatura	k1gFnSc6	aparatura
a	a	k8xC	a
vzniklý	vzniklý	k2eAgMnSc1d1	vzniklý
těkavý	těkavý	k2eAgMnSc1d1	těkavý
SiF	SiF	k1gMnSc1	SiF
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
okamžitě	okamžitě	k6eAd1	okamžitě
odváděn	odvádět	k5eAaImNgInS	odvádět
proudem	proud	k1gInSc7	proud
horké	horký	k2eAgFnSc2d1	horká
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
do	do	k7c2	do
roztoku	roztok	k1gInSc2	roztok
alkalického	alkalický	k2eAgInSc2d1	alkalický
hydroxidu	hydroxid	k1gInSc2	hydroxid
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
možno	možno	k6eAd1	možno
snáze	snadno	k6eAd2	snadno
určit	určit	k5eAaPmF	určit
jeho	on	k3xPp3gInSc4	on
obsah	obsah	k1gInSc4	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Krystaly	krystal	k1gInPc1	krystal
karbidu	karbid	k1gInSc2	karbid
křemíku	křemík	k1gInSc2	křemík
SiC	sic	k6eAd1	sic
mají	mít	k5eAaImIp3nP	mít
analogickou	analogický	k2eAgFnSc4d1	analogická
krystalickou	krystalický	k2eAgFnSc4d1	krystalická
strukturu	struktura	k1gFnSc4	struktura
jako	jako	k8xS	jako
diamant	diamant	k1gInSc4	diamant
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
jedny	jeden	k4xCgFnPc4	jeden
z	z	k7c2	z
nejtvrdších	tvrdý	k2eAgFnPc2d3	nejtvrdší
známých	známý	k2eAgFnPc2d1	známá
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mohsově	Mohsův	k2eAgFnSc6d1	Mohsova
stupnici	stupnice	k1gFnSc6	stupnice
tvrdosti	tvrdost	k1gFnSc2	tvrdost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
karbid	karbid	k1gInSc1	karbid
stupně	stupeň	k1gInSc2	stupeň
9	[number]	k4	9
-	-	kIx~	-
10	[number]	k4	10
a	a	k8xC	a
nalézá	nalézat	k5eAaImIp3nS	nalézat
uplatnění	uplatnění	k1gNnSc4	uplatnění
jako	jako	k8xS	jako
brusný	brusný	k2eAgInSc4d1	brusný
materiál	materiál	k1gInSc4	materiál
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
karborundum	karborundum	k1gNnSc1	karborundum
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
křemík	křemík	k1gInSc1	křemík
a	a	k8xC	a
uhlík	uhlík	k1gInSc1	uhlík
se	se	k3xPyFc4	se
periodické	periodický	k2eAgFnSc3d1	periodická
soustavě	soustava	k1gFnSc3	soustava
prvků	prvek	k1gInPc2	prvek
nalézají	nalézat	k5eAaImIp3nP	nalézat
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
skupině	skupina	k1gFnSc6	skupina
pod	pod	k7c7	pod
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
dalo	dát	k5eAaPmAgNnS	dát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
usuzovat	usuzovat	k5eAaImF	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
křemík	křemík	k1gInSc1	křemík
bude	být	k5eAaImBp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
uhlík	uhlík	k1gInSc4	uhlík
vytvářet	vytvářet	k5eAaImF	vytvářet
nesmírně	smírně	k6eNd1	smírně
pestrou	pestrý	k2eAgFnSc4d1	pestrá
škálu	škála	k1gFnSc4	škála
sloučenin	sloučenina	k1gFnPc2	sloučenina
analogických	analogický	k2eAgInPc2d1	analogický
organickým	organický	k2eAgFnPc3d1	organická
látkám	látka	k1gFnPc3	látka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
vědeckofantastické	vědeckofantastický	k2eAgFnSc6d1	vědeckofantastická
literatuře	literatura	k1gFnSc6	literatura
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
živými	živý	k2eAgFnPc7d1	živá
bytostmi	bytost	k1gFnPc7	bytost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
nás	my	k3xPp1nPc2	my
mají	mít	k5eAaImIp3nP	mít
tělo	tělo	k1gNnSc1	tělo
složené	složený	k2eAgNnSc1d1	složené
ze	z	k7c2	z
silikonových	silikonový	k2eAgFnPc2d1	silikonová
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
koupou	koupat	k5eAaImIp3nP	koupat
se	se	k3xPyFc4	se
v	v	k7c6	v
kapalném	kapalný	k2eAgInSc6d1	kapalný
čpavku	čpavek	k1gInSc6	čpavek
<g/>
,	,	kIx,	,
dýchají	dýchat	k5eAaImIp3nP	dýchat
sirné	sirný	k2eAgFnPc1d1	sirná
páry	pára	k1gFnPc1	pára
apod.	apod.	kA	apod.
Skutečnost	skutečnost	k1gFnSc1	skutečnost
je	být	k5eAaImIp3nS	být
však	však	k9	však
daleko	daleko	k6eAd1	daleko
prozaičtější	prozaický	k2eAgFnSc1d2	prozaičtější
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
křemík	křemík	k1gInSc1	křemík
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
řadu	řada	k1gFnSc4	řada
analogických	analogický	k2eAgFnPc2d1	analogická
sloučenin	sloučenina	k1gFnPc2	sloučenina
k	k	k7c3	k
uhlovodíkům	uhlovodík	k1gInPc3	uhlovodík
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
jejich	jejich	k3xOp3gNnSc4	jejich
množství	množství	k1gNnSc4	množství
nikdy	nikdy	k6eAd1	nikdy
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
pestrosti	pestrost	k1gFnPc4	pestrost
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
především	především	k9	především
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
atomy	atom	k1gInPc1	atom
křemíku	křemík	k1gInSc2	křemík
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
vytvářet	vytvářet	k5eAaImF	vytvářet
dvojnou	dvojný	k2eAgFnSc4d1	dvojná
vazbu	vazba	k1gFnSc4	vazba
Si	se	k3xPyFc3	se
<g/>
=	=	kIx~	=
<g/>
Si	se	k3xPyFc3	se
a	a	k8xC	a
pochopitelně	pochopitelně	k6eAd1	pochopitelně
ani	ani	k8xC	ani
vazbu	vazba	k1gFnSc4	vazba
trojnou	trojný	k2eAgFnSc4d1	trojná
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
významný	významný	k2eAgInSc1d1	významný
důvod	důvod	k1gInSc1	důvod
je	být	k5eAaImIp3nS	být
síla	síla	k1gFnSc1	síla
vazby	vazba	k1gFnSc2	vazba
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
atomy	atom	k1gInPc7	atom
křemíku	křemík	k1gInSc2	křemík
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
poloviční	poloviční	k2eAgMnSc1d1	poloviční
<g/>
,	,	kIx,	,
než	než	k8xS	než
u	u	k7c2	u
vazby	vazba	k1gFnSc2	vazba
C-	C-	k1gFnSc2	C-
<g/>
C.	C.	kA	C.
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
molekuly	molekula	k1gFnPc1	molekula
o	o	k7c6	o
vysokém	vysoký	k2eAgInSc6d1	vysoký
počtu	počet	k1gInSc6	počet
vazeb	vazba	k1gFnPc2	vazba
Si-Si	Si-S	k1gFnSc2	Si-S
nestálé	stálý	k2eNgFnSc2d1	nestálá
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
existují	existovat	k5eAaImIp3nP	existovat
skupiny	skupina	k1gFnPc1	skupina
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
analogy	analog	k1gInPc4	analog
mezi	mezi	k7c7	mezi
organickými	organický	k2eAgFnPc7d1	organická
sloučeninami	sloučenina	k1gFnPc7	sloučenina
a	a	k8xC	a
podobnými	podobný	k2eAgFnPc7d1	podobná
sloučeninami	sloučenina	k1gFnPc7	sloučenina
křemíku	křemík	k1gInSc2	křemík
<g/>
.	.	kIx.	.
</s>
<s>
Silany	silan	k1gInPc1	silan
jsou	být	k5eAaImIp3nP	být
bezbarvé	bezbarvý	k2eAgFnPc4d1	bezbarvá
látky	látka	k1gFnPc4	látka
o	o	k7c4	o
složení	složení	k1gNnSc4	složení
SinH	SinH	k1gFnSc2	SinH
<g/>
2	[number]	k4	2
<g/>
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
dva	dva	k4xCgInPc1	dva
silany	silan	k1gInPc1	silan
jsou	být	k5eAaImIp3nP	být
plynné	plynný	k2eAgInPc1d1	plynný
<g/>
,	,	kIx,	,
od	od	k7c2	od
trisilanu	trisilan	k1gInSc2	trisilan
Si	se	k3xPyFc3	se
<g/>
3	[number]	k4	3
<g/>
H	H	kA	H
<g/>
8	[number]	k4	8
kapalné	kapalný	k2eAgInPc1d1	kapalný
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
jsou	být	k5eAaImIp3nP	být
mimořádně	mimořádně	k6eAd1	mimořádně
reaktivní	reaktivní	k2eAgFnPc1d1	reaktivní
a	a	k8xC	a
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
samozápalné	samozápalný	k2eAgNnSc1d1	samozápalné
<g/>
,	,	kIx,	,
reakcí	reakce	k1gFnSc7	reakce
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
vzniká	vznikat	k5eAaImIp3nS	vznikat
oxid	oxid	k1gInSc1	oxid
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
také	také	k9	také
rychle	rychle	k6eAd1	rychle
hydrolyzují	hydrolyzovat	k5eAaBmIp3nP	hydrolyzovat
za	za	k7c4	za
uvolnění	uvolnění	k1gNnSc4	uvolnění
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
gelu	gel	k1gInSc2	gel
kyseliny	kyselina	k1gFnSc2	kyselina
křemičité	křemičitý	k2eAgFnSc2d1	křemičitá
<g/>
.	.	kIx.	.
</s>
<s>
Silany	silan	k1gInPc1	silan
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgNnPc1d1	silné
redukční	redukční	k2eAgNnPc1d1	redukční
činidla	činidlo	k1gNnPc1	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
halogeny	halogen	k1gInPc7	halogen
reagují	reagovat	k5eAaBmIp3nP	reagovat
explozivně	explozivně	k6eAd1	explozivně
<g/>
,	,	kIx,	,
za	za	k7c2	za
jistých	jistý	k2eAgFnPc2d1	jistá
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
přítomnost	přítomnost	k1gFnSc1	přítomnost
AlCl	AlCl	k1gInSc1	AlCl
<g/>
3	[number]	k4	3
a	a	k8xC	a
vhodné	vhodný	k2eAgFnPc1d1	vhodná
teploty	teplota	k1gFnPc1	teplota
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
molekuly	molekula	k1gFnPc4	molekula
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
atomem	atom	k1gInSc7	atom
halogenu	halogen	k1gInSc2	halogen
SiH	SiH	k1gFnSc2	SiH
<g/>
3	[number]	k4	3
<g/>
Cl	Cl	k1gMnSc1	Cl
až	až	k9	až
SiHCl	SiHCl	k1gInSc4	SiHCl
<g/>
3	[number]	k4	3
apod.	apod.	kA	apod.
Silany	silan	k1gInPc1	silan
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
především	především	k6eAd1	především
jako	jako	k8xS	jako
výchozí	výchozí	k2eAgFnPc1d1	výchozí
sloučeniny	sloučenina	k1gFnPc1	sloučenina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
složitějších	složitý	k2eAgFnPc2d2	složitější
křemíkatých	křemíkatý	k2eAgFnPc2d1	křemíkatý
látek	látka	k1gFnPc2	látka
např.	např.	kA	např.
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
čistého	čistý	k2eAgInSc2d1	čistý
polovodičového	polovodičový	k2eAgInSc2d1	polovodičový
křemíku	křemík	k1gInSc2	křemík
<g/>
.	.	kIx.	.
</s>
<s>
Siloxany	Siloxan	k1gInPc1	Siloxan
jsou	být	k5eAaImIp3nP	být
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
vazbu	vazba	k1gFnSc4	vazba
Si-O-Si	Si-O-S	k1gMnSc3	Si-O-S
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
chemická	chemický	k2eAgFnSc1d1	chemická
skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
stabilní	stabilní	k2eAgFnSc1d1	stabilní
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
proto	proto	k8xC	proto
být	být	k5eAaImF	být
připraven	připravit	k5eAaPmNgInS	připravit
teoreticky	teoreticky	k6eAd1	teoreticky
neomezený	omezený	k2eNgInSc1d1	neomezený
řetězec	řetězec	k1gInSc1	řetězec
o	o	k7c6	o
složení	složení	k1gNnSc6	složení
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
O-Si-O-Si-O	O-Si-O-Si-O	k1gFnSc1	O-Si-O-Si-O
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
zbylé	zbylý	k2eAgInPc1d1	zbylý
dvě	dva	k4xCgFnPc1	dva
volné	volný	k2eAgFnPc1d1	volná
vazby	vazba	k1gFnPc1	vazba
křemíkového	křemíkový	k2eAgInSc2d1	křemíkový
atomu	atom	k1gInSc2	atom
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
obsazeny	obsadit	k5eAaPmNgInP	obsadit
např.	např.	kA	např.
skupinami	skupina	k1gFnPc7	skupina
-	-	kIx~	-
<g/>
HO	on	k3xPp3gNnSc4	on
nebo	nebo	k8xC	nebo
nejčastěji	často	k6eAd3	často
organickými	organický	k2eAgInPc7d1	organický
ligandy	ligand	k1gInPc7	ligand
jako	jako	k8xC	jako
-CH3	-CH3	k4	-CH3
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžněji	běžně	k6eAd3	běžně
užívané	užívaný	k2eAgFnPc1d1	užívaná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
jsou	být	k5eAaImIp3nP	být
polydimethylsiloxany	polydimethylsiloxan	k1gInPc1	polydimethylsiloxan
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
vzorec	vzorec	k1gInSc4	vzorec
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
SiO	SiO	k1gMnSc1	SiO
<g/>
[	[	kIx(	[
<g/>
SiO	SiO	k1gMnSc1	SiO
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
nSi	nSi	k?	nSi
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Uvedené	uvedený	k2eAgFnPc1d1	uvedená
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
přitom	přitom	k6eAd1	přitom
za	za	k7c2	za
běžných	běžný	k2eAgFnPc2d1	běžná
podmínek	podmínka	k1gFnPc2	podmínka
zcela	zcela	k6eAd1	zcela
stabilní	stabilní	k2eAgFnPc1d1	stabilní
a	a	k8xC	a
nepodléhají	podléhat	k5eNaImIp3nP	podléhat
rozkladu	rozklad	k1gInSc3	rozklad
ani	ani	k8xC	ani
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
kyslíku	kyslík	k1gInSc2	kyslík
nebo	nebo	k8xC	nebo
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
siloxanových	siloxanův	k2eAgFnPc2d1	siloxanův
skupin	skupina	k1gFnPc2	skupina
i	i	k8xC	i
jejich	jejich	k3xOp3gInPc2	jejich
ligandů	ligand	k1gInPc2	ligand
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
výslednými	výsledný	k2eAgInPc7d1	výsledný
produkty	produkt	k1gInPc7	produkt
jak	jak	k6eAd1	jak
kapalné	kapalný	k2eAgNnSc1d1	kapalné
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
pevné	pevný	k2eAgFnPc4d1	pevná
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
další	další	k2eAgFnSc7d1	další
vlastností	vlastnost	k1gFnSc7	vlastnost
je	být	k5eAaImIp3nS	být
hydrofobie	hydrofobie	k1gFnSc1	hydrofobie
(	(	kIx(	(
<g/>
odpuzují	odpuzovat	k5eAaImIp3nP	odpuzovat
vodu	voda	k1gFnSc4	voda
<g/>
)	)	kIx)	)
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
naprostá	naprostý	k2eAgFnSc1d1	naprostá
neškodnost	neškodnost	k1gFnSc1	neškodnost
pro	pro	k7c4	pro
živé	živý	k2eAgInPc4d1	živý
organizmy	organizmus	k1gInPc4	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Praktické	praktický	k2eAgNnSc1d1	praktické
využití	využití	k1gNnSc1	využití
siloxanů	siloxan	k1gMnPc2	siloxan
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
široké	široký	k2eAgNnSc1d1	široké
<g/>
:	:	kIx,	:
Ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaImNgInP	využívat
především	především	k9	především
hydrofobní	hydrofobní	k2eAgFnPc1d1	hydrofobní
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
složkou	složka	k1gFnSc7	složka
speciálních	speciální	k2eAgFnPc2d1	speciální
omítek	omítka	k1gFnPc2	omítka
a	a	k8xC	a
nátěrů	nátěr	k1gInPc2	nátěr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zabraňují	zabraňovat	k5eAaImIp3nP	zabraňovat
pronikání	pronikání	k1gNnSc4	pronikání
vlhkosti	vlhkost	k1gFnSc2	vlhkost
do	do	k7c2	do
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Pevné	pevný	k2eAgFnPc1d1	pevná
polymerní	polymerní	k2eAgFnPc1d1	polymerní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
siloxanů	siloxan	k1gInPc2	siloxan
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xC	jako
silikonový	silikonový	k2eAgInSc1d1	silikonový
kaučuk	kaučuk	k1gInSc1	kaučuk
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
má	mít	k5eAaImIp3nS	mít
elastické	elastický	k2eAgFnPc4d1	elastická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
podobné	podobný	k2eAgFnPc4d1	podobná
klasickému	klasický	k2eAgInSc3d1	klasický
kaučuku	kaučuk	k1gInSc3	kaučuk
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
však	však	k9	však
snáší	snášet	k5eAaImIp3nP	snášet
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgFnPc1d2	vyšší
teploty	teplota	k1gFnPc1	teplota
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
nehořlavá	hořlavý	k2eNgFnSc1d1	nehořlavá
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
vlastnostem	vlastnost	k1gFnPc3	vlastnost
se	se	k3xPyFc4	se
ze	z	k7c2	z
silikonového	silikonový	k2eAgInSc2d1	silikonový
kaučuku	kaučuk	k1gInSc2	kaučuk
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
různá	různý	k2eAgNnPc1d1	různé
těsnění	těsnění	k1gNnPc1	těsnění
nebo	nebo	k8xC	nebo
vystýlky	vystýlka	k1gFnPc1	vystýlka
nádob	nádoba	k1gFnPc2	nádoba
pro	pro	k7c4	pro
chemický	chemický	k2eAgInSc4d1	chemický
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
podobné	podobný	k2eAgFnPc4d1	podobná
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Hydrofobní	hydrofobní	k2eAgFnPc4d1	hydrofobní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
siloxanů	siloxan	k1gInPc2	siloxan
lze	lze	k6eAd1	lze
potlačit	potlačit	k5eAaPmF	potlačit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
určité	určitý	k2eAgNnSc4d1	určité
procento	procento	k1gNnSc4	procento
křemíkových	křemíkový	k2eAgInPc2d1	křemíkový
atomů	atom	k1gInPc2	atom
jsou	být	k5eAaImIp3nP	být
navázány	navázán	k2eAgFnPc1d1	navázána
skupiny	skupina	k1gFnPc1	skupina
-	-	kIx~	-
<g/>
OH	OH	kA	OH
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
polymery	polymer	k1gInPc1	polymer
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
chirurgických	chirurgický	k2eAgInPc2d1	chirurgický
implantátů	implantát	k1gInPc2	implantát
(	(	kIx(	(
<g/>
nejznámější	známý	k2eAgMnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
umělé	umělý	k2eAgNnSc1d1	umělé
zvětšování	zvětšování	k1gNnSc1	zvětšování
velikosti	velikost	k1gFnSc2	velikost
ženských	ženský	k2eAgInPc2d1	ženský
prsů	prs	k1gInPc2	prs
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapalné	kapalný	k2eAgInPc1d1	kapalný
nebo	nebo	k8xC	nebo
polotuhé	polotuhý	k2eAgInPc1d1	polotuhý
siloxany	siloxan	k1gInPc1	siloxan
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
jako	jako	k8xS	jako
silikonové	silikonový	k2eAgInPc1d1	silikonový
oleje	olej	k1gInPc1	olej
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
silikonové	silikonový	k2eAgInPc4d1	silikonový
tuky	tuk	k1gInPc4	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
předností	přednost	k1gFnSc7	přednost
oproti	oproti	k7c3	oproti
klasickým	klasický	k2eAgNnPc3d1	klasické
mazadlům	mazadlo	k1gNnPc3	mazadlo
je	být	k5eAaImIp3nS	být
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
vysokým	vysoký	k2eAgFnPc3d1	vysoká
teplotám	teplota	k1gFnPc3	teplota
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
nasazovány	nasazovat	k5eAaImNgFnP	nasazovat
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgNnSc7d1	zvýšené
teplotním	teplotní	k2eAgNnSc7d1	teplotní
namáháním	namáhání	k1gNnSc7	namáhání
nejen	nejen	k6eAd1	nejen
jako	jako	k8xS	jako
mazadla	mazadlo	k1gNnPc4	mazadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jako	jako	k8xS	jako
média	médium	k1gNnPc4	médium
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
tepla	teplo	k1gNnSc2	teplo
(	(	kIx(	(
<g/>
olejové	olejový	k2eAgFnPc1d1	olejová
lázně	lázeň	k1gFnPc1	lázeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Křemík	křemík	k1gInSc4	křemík
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gFnPc1	jeho
běžné	běžný	k2eAgFnPc1d1	běžná
anorganické	anorganický	k2eAgFnPc1d1	anorganická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
nejsou	být	k5eNaImIp3nP	být
toxické	toxický	k2eAgFnPc1d1	toxická
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
natolik	natolik	k6eAd1	natolik
inertní	inertní	k2eAgNnPc1d1	inertní
<g/>
,	,	kIx,	,
že	že	k8xS	že
projdou	projít	k5eAaPmIp3nP	projít
trávicím	trávicí	k2eAgInSc7d1	trávicí
traktem	trakt	k1gInSc7	trakt
zcela	zcela	k6eAd1	zcela
neporušeny	porušit	k5eNaPmNgFnP	porušit
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
nastávají	nastávat	k5eAaImIp3nP	nastávat
spíše	spíše	k9	spíše
při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
vdechování	vdechování	k1gNnSc6	vdechování
mikroskopických	mikroskopický	k2eAgFnPc2d1	mikroskopická
částeček	částečka	k1gFnPc2	částečka
<g/>
,	,	kIx,	,
vznikajících	vznikající	k2eAgInPc2d1	vznikající
při	při	k7c6	při
broušení	broušení	k1gNnSc6	broušení
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
silikátových	silikátový	k2eAgInPc2d1	silikátový
materiálů	materiál	k1gInPc2	materiál
nebo	nebo	k8xC	nebo
při	při	k7c6	při
mechanickém	mechanický	k2eAgNnSc6d1	mechanické
opracovávání	opracovávání	k1gNnSc6	opracovávání
silikátových	silikátový	k2eAgInPc2d1	silikátový
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k9	tak
choroba	choroba	k1gFnSc1	choroba
silikóza	silikóza	k1gFnSc1	silikóza
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
snížením	snížení	k1gNnSc7	snížení
plicní	plicní	k2eAgFnSc2d1	plicní
kapacity	kapacita	k1gFnSc2	kapacita
a	a	k8xC	a
dušností	dušnost	k1gFnPc2	dušnost
<g/>
.	.	kIx.	.
</s>
<s>
Přísná	přísný	k2eAgNnPc4d1	přísné
bezpečnostní	bezpečnostní	k2eAgNnPc4d1	bezpečnostní
opatření	opatření	k1gNnPc4	opatření
ovšem	ovšem	k9	ovšem
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dodržována	dodržovat	k5eAaImNgFnS	dodržovat
při	při	k7c6	při
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
výrobě	výroba	k1gFnSc6	výroba
a	a	k8xC	a
zpracování	zpracování	k1gNnSc6	zpracování
silanů	silan	k1gInPc2	silan
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
chlorovaných	chlorovaný	k2eAgInPc2d1	chlorovaný
derivátů	derivát	k1gInPc2	derivát
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
reakce	reakce	k1gFnSc1	reakce
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
nebo	nebo	k8xC	nebo
vlhkostí	vlhkost	k1gFnSc7	vlhkost
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
explozi	exploze	k1gFnSc4	exploze
<g/>
,	,	kIx,	,
únik	únik	k1gInSc1	únik
toxického	toxický	k2eAgInSc2d1	toxický
a	a	k8xC	a
žíravého	žíravý	k2eAgInSc2d1	žíravý
chlorovodíku	chlorovodík	k1gInSc2	chlorovodík
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
