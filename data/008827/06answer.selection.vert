<s>
Její	její	k3xOp3gFnSc7	její
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
a	a	k8xC	a
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
je	být	k5eAaImIp3nS	být
Dani	daň	k1gFnSc3	daň
Klein	Klein	k1gMnSc1	Klein
(	(	kIx(	(
<g/>
*	*	kIx~	*
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
vl	vl	k?	vl
<g/>
.	.	kIx.	.
jménem	jméno	k1gNnSc7	jméno
Danielle	Danielle	k1gFnSc2	Danielle
Schoovaerts	Schoovaertsa	k1gFnPc2	Schoovaertsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
obdobích	období	k1gNnPc6	období
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
měnící	měnící	k2eAgFnSc1d1	měnící
se	se	k3xPyFc4	se
sestava	sestava	k1gFnSc1	sestava
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
.	.	kIx.	.
</s>
