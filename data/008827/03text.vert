<p>
<s>
Vaya	Vay	k2eAgFnSc1d1	Vaya
Con	Con	k1gFnSc1	Con
Dios	Diosa	k1gFnPc2	Diosa
(	(	kIx(	(
<g/>
šp	šp	k?	šp
<g/>
.	.	kIx.	.
pozdrav	pozdrav	k1gInSc1	pozdrav
na	na	k7c4	na
rozloučenou	rozloučená	k1gFnSc4	rozloučená
Buďte	budit	k5eAaImRp2nP	budit
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
belgická	belgický	k2eAgFnSc1d1	belgická
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
a	a	k8xC	a
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
je	být	k5eAaImIp3nS	být
Dani	daň	k1gFnSc3	daň
Klein	Klein	k1gMnSc1	Klein
(	(	kIx(	(
<g/>
*	*	kIx~	*
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
vl	vl	k?	vl
<g/>
.	.	kIx.	.
jménem	jméno	k1gNnSc7	jméno
Danielle	Danielle	k1gFnSc2	Danielle
Schoovaerts	Schoovaertsa	k1gFnPc2	Schoovaertsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
obdobích	období	k1gNnPc6	období
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
měnící	měnící	k2eAgFnSc1d1	měnící
se	se	k3xPyFc4	se
sestava	sestava	k1gFnSc1	sestava
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kapela	kapela	k1gFnSc1	kapela
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
využíváním	využívání	k1gNnSc7	využívání
různých	různý	k2eAgInPc2d1	různý
hudebních	hudební	k2eAgInPc2d1	hudební
stylů	styl	k1gInPc2	styl
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejích	její	k3xOp3gFnPc6	její
písních	píseň	k1gFnPc6	píseň
se	se	k3xPyFc4	se
mísí	mísit	k5eAaImIp3nP	mísit
latinskoamerická	latinskoamerický	k2eAgFnSc1d1	latinskoamerická
a	a	k8xC	a
romská	romský	k2eAgFnSc1d1	romská
hudba	hudba	k1gFnSc1	hudba
se	s	k7c7	s
soft	soft	k?	soft
rockem	rock	k1gInSc7	rock
<g/>
,	,	kIx,	,
jazzem	jazz	k1gInSc7	jazz
<g/>
,	,	kIx,	,
soulem	soul	k1gInSc7	soul
i	i	k8xC	i
blues	blues	k1gFnSc7	blues
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
hudebních	hudební	k2eAgFnPc2d1	hudební
skupin	skupina	k1gFnPc2	skupina
belgické	belgický	k2eAgFnSc2d1	belgická
historie	historie	k1gFnSc2	historie
–	–	k?	–
dosud	dosud	k6eAd1	dosud
prodala	prodat	k5eAaPmAgFnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
alb	alba	k1gFnPc2	alba
a	a	k8xC	a
přes	přes	k7c4	přes
3	[number]	k4	3
miliony	milion	k4xCgInPc4	milion
singlů	singl	k1gInPc2	singl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kapela	kapela	k1gFnSc1	kapela
byla	být	k5eAaImAgFnS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
v	v	k7c6	v
letech	let	k1gInPc6	let
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vyčerpaná	vyčerpaný	k2eAgFnSc1d1	vyčerpaná
Dani	daň	k1gFnSc6	daň
Klein	Klein	k1gMnSc1	Klein
z	z	k7c2	z
hudebního	hudební	k2eAgNnSc2d1	hudební
prostředí	prostředí	k1gNnSc2	prostředí
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
skupina	skupina	k1gFnSc1	skupina
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
předchozí	předchozí	k2eAgFnSc4d1	předchozí
dráhu	dráha	k1gFnSc4	dráha
navázala	navázat	k5eAaPmAgFnS	navázat
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
stále	stále	k6eAd1	stále
koncertuje	koncertovat	k5eAaImIp3nS	koncertovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Vaya	Vay	k2eAgFnSc1d1	Vaya
Con	Con	k1gFnSc1	Con
Dios	Diosa	k1gFnPc2	Diosa
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Night	Night	k2eAgInSc1d1	Night
Owls	Owls	k1gInSc1	Owls
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Time	Time	k1gFnSc1	Time
Flies	Fliesa	k1gFnPc2	Fliesa
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Roots	Roots	k1gInSc1	Roots
and	and	k?	and
Wings	Wings	k1gInSc1	Wings
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Promise	Promise	k1gFnSc1	Promise
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Comme	Commat	k5eAaPmIp3nS	Commat
on	on	k3xPp3gMnSc1	on
est	est	k?	est
venu	venus	k1gInSc2	venus
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Kompilace	kompilace	k1gFnSc2	kompilace
===	===	k?	===
</s>
</p>
<p>
<s>
Best	Best	k1gMnSc1	Best
Of	Of	k1gMnSc1	Of
Vaya	Vaya	k1gMnSc1	Vaya
Con	Con	k1gMnSc1	Con
Dios	Diosa	k1gFnPc2	Diosa
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
What	What	k1gInSc1	What
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
A	A	kA	A
Woman	Woman	k1gInSc1	Woman
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Blue	Blu	k1gFnSc2	Blu
Sides	Sides	k1gMnSc1	Sides
Of	Of	k1gMnSc1	Of
Vaya	Vaya	k1gMnSc1	Vaya
Con	Con	k1gMnSc1	Con
Dios	Diosa	k1gFnPc2	Diosa
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Ultimate	Ultimat	k1gInSc5	Ultimat
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
skupiny	skupina	k1gFnSc2	skupina
Vaya	Vaya	k1gFnSc1	Vaya
Con	Con	k1gMnSc1	Con
Dios	Dios	k1gInSc1	Dios
</s>
</p>
