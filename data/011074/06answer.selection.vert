<s>
Eutin	Eutin	k1gInSc1	Eutin
08	[number]	k4	08
(	(	kIx(	(
<g/>
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Eutiner	Eutiner	k1gInSc1	Eutiner
Sportvereinigung	Sportvereinigung	k1gInSc1	Sportvereinigung
von	von	k1gInSc1	von
1908	[number]	k4	1908
e.	e.	k?	e.
V.	V.	kA	V.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
německý	německý	k2eAgInSc1d1	německý
sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Eutin	Eutina	k1gFnPc2	Eutina
ve	v	k7c6	v
spolkové	spolkový	k2eAgFnSc6d1	spolková
zemi	zem	k1gFnSc6	zem
Šlesvicko-Holštýnsko	Šlesvicko-Holštýnsko	k1gNnSc1	Šlesvicko-Holštýnsko
<g/>
.	.	kIx.	.
</s>
