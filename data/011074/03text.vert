<p>
<s>
Eutin	Eutin	k1gInSc1	Eutin
08	[number]	k4	08
(	(	kIx(	(
<g/>
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Eutiner	Eutiner	k1gInSc1	Eutiner
Sportvereinigung	Sportvereinigung	k1gInSc1	Sportvereinigung
von	von	k1gInSc1	von
1908	[number]	k4	1908
e.	e.	k?	e.
V.	V.	kA	V.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
německý	německý	k2eAgInSc1d1	německý
sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Eutin	Eutina	k1gFnPc2	Eutina
ve	v	k7c6	v
spolkové	spolkový	k2eAgFnSc6d1	spolková
zemi	zem	k1gFnSc6	zem
Šlesvicko-Holštýnsko	Šlesvicko-Holštýnsko	k1gNnSc1	Šlesvicko-Holštýnsko
<g/>
.	.	kIx.	.
</s>
<s>
Založen	založen	k2eAgMnSc1d1	založen
byl	být	k5eAaImAgMnS	být
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1908	[number]	k4	1908
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Eutiner	Eutiner	k1gInSc1	Eutiner
FC	FC	kA	FC
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
současný	současný	k2eAgInSc1d1	současný
název	název	k1gInSc1	název
nese	nést	k5eAaImIp3nS	nést
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2018	[number]	k4	2018
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
Oberlize	Oberliza	k1gFnSc6	Oberliza
Schleswig-Holstein	Schleswig-Holsteina	k1gFnPc2	Schleswig-Holsteina
<g/>
,	,	kIx,	,
páté	pátý	k4xOgFnSc6	pátý
německé	německý	k2eAgFnSc6d1	německá
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
fotbalové	fotbalový	k2eAgFnSc6d1	fotbalová
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
domácí	domácí	k2eAgInPc4d1	domácí
zápasy	zápas	k1gInPc4	zápas
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
Eutina-Platz	Eutina-Platza	k1gFnPc2	Eutina-Platza
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
2	[number]	k4	2
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Klubové	klubový	k2eAgFnPc1d1	klubová
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
mužský	mužský	k2eAgInSc4d1	mužský
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
oddíl	oddíl	k1gInSc4	oddíl
má	mít	k5eAaImIp3nS	mít
sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
i	i	k8xC	i
jiné	jiný	k2eAgInPc1d1	jiný
oddíly	oddíl	k1gInPc1	oddíl
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
oddíl	oddíl	k1gInSc4	oddíl
házené	házená	k1gFnSc2	házená
<g/>
,	,	kIx,	,
tanečního	taneční	k2eAgInSc2d1	taneční
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
fitnessu	fitness	k1gInSc2	fitness
a	a	k8xC	a
gymnastiky	gymnastika	k1gFnSc2	gymnastika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historické	historický	k2eAgInPc1d1	historický
názvy	název	k1gInPc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1908	[number]	k4	1908
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Eutiner	Eutiner	k1gInSc1	Eutiner
FC	FC	kA	FC
08	[number]	k4	08
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Eutiner	Eutiner	k1gMnSc1	Eutiner
Fußballclub	Fußballclub	k1gMnSc1	Fußballclub
von	von	k1gInSc4	von
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1921	[number]	k4	1921
–	–	k?	–
Eutiner	Eutiner	k1gMnSc1	Eutiner
SpVgg	SpVgg	k1gMnSc1	SpVgg
08	[number]	k4	08
(	(	kIx(	(
<g/>
Eutiner	Eutiner	k1gMnSc1	Eutiner
Sportvereinigung	Sportvereinigung	k1gMnSc1	Sportvereinigung
von	von	k1gInSc4	von
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Umístění	umístění	k1gNnSc1	umístění
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
sezonách	sezona	k1gFnPc6	sezona
==	==	k?	==
</s>
</p>
<p>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
přehledZdroj	přehledZdroj	k1gInSc1	přehledZdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
:	:	kIx,	:
Landesliga	Landesliga	k1gFnSc1	Landesliga
Schleswig-Holstein	Schleswig-Holstein	k1gMnSc1	Schleswig-Holstein
Ost	Ost	k1gMnSc1	Ost
</s>
</p>
<p>
<s>
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1952	[number]	k4	1952
<g/>
:	:	kIx,	:
Landesliga	Landesliga	k1gFnSc1	Landesliga
Schleswig-Holstein	Schleswig-Holsteina	k1gFnPc2	Schleswig-Holsteina
</s>
</p>
<p>
<s>
1952	[number]	k4	1952
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
<g/>
:	:	kIx,	:
Bezirksklasse	Bezirksklasse	k1gFnSc2	Bezirksklasse
Schleswig-Holstein	Schleswig-Holstein	k1gMnSc1	Schleswig-Holstein
Süd	Süd	k1gMnSc1	Süd
<g/>
/	/	kIx~	/
<g/>
Nord	Nord	k1gMnSc1	Nord
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Amateurliga	Amateurliga	k1gFnSc1	Amateurliga
Schleswig-Holstein	Schleswig-Holstein	k1gMnSc1	Schleswig-Holstein
Süd	Süd	k1gMnSc1	Süd
<g/>
/	/	kIx~	/
<g/>
Nord	Nord	k1gMnSc1	Nord
</s>
</p>
<p>
<s>
1958	[number]	k4	1958
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Amateurliga	Amateurliga	k1gFnSc1	Amateurliga
Schleswig-Holstein	Schleswig-Holstein	k1gMnSc1	Schleswig-Holstein
Süd	Süd	k1gMnSc1	Süd
<g/>
/	/	kIx~	/
<g/>
Nord	Nord	k1gMnSc1	Nord
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
<g/>
:	:	kIx,	:
Bezirksliga	Bezirksliga	k1gFnSc1	Bezirksliga
Schleswig-Holstein	Schleswig-Holstein	k1gMnSc1	Schleswig-Holstein
Süd	Süd	k1gMnSc1	Süd
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
:	:	kIx,	:
Verbandsliga	Verbandsliga	k1gFnSc1	Verbandsliga
Schleswig-Holstein	Schleswig-Holstein	k1gMnSc1	Schleswig-Holstein
Süd	Süd	k1gMnSc1	Süd
</s>
</p>
<p>
<s>
1972	[number]	k4	1972
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
<g/>
:	:	kIx,	:
Landesliga	Landesliga	k1gFnSc1	Landesliga
Schleswig-Holstein	Schleswig-Holsteina	k1gFnPc2	Schleswig-Holsteina
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
:	:	kIx,	:
Verbandsliga	Verbandsliga	k1gFnSc1	Verbandsliga
Schleswig-Holstein	Schleswig-Holsteina	k1gFnPc2	Schleswig-Holsteina
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
Fußball-Oberliga	Fußball-Oberliga	k1gFnSc1	Fußball-Oberliga
Nord	Norda	k1gFnPc2	Norda
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
Verbandsliga	Verbandsliga	k1gFnSc1	Verbandsliga
Schleswig-Holstein	Schleswig-Holsteina	k1gFnPc2	Schleswig-Holsteina
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
:	:	kIx,	:
Landesliga	Landesliga	k1gFnSc1	Landesliga
Schleswig-Holstein	Schleswig-Holstein	k1gMnSc1	Schleswig-Holstein
Süd	Süd	k1gMnSc1	Süd
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
Bezirksliga	Bezirksliga	k1gFnSc1	Bezirksliga
Schleswig-Holstein	Schleswig-Holstein	k1gMnSc1	Schleswig-Holstein
Süd	Süd	k1gMnSc1	Süd
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Bezirksliga	Bezirksliga	k1gFnSc1	Bezirksliga
Schleswig-Holstein	Schleswig-Holstein	k1gMnSc1	Schleswig-Holstein
Süd	Süd	k1gMnSc1	Süd
<g/>
/	/	kIx~	/
<g/>
Nord	Nord	k1gMnSc1	Nord
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
Kreisliga	Kreisliga	k1gFnSc1	Kreisliga
Ostholstein	Ostholsteina	k1gFnPc2	Ostholsteina
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Bezirksliga	Bezirksliga	k1gFnSc1	Bezirksliga
Schleswig-Holstein	Schleswig-Holstein	k1gMnSc1	Schleswig-Holstein
Süd	Süd	k1gMnSc1	Süd
<g/>
/	/	kIx~	/
<g/>
Nord	Nord	k1gMnSc1	Nord
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Verbandsliga	Verbandsliga	k1gFnSc1	Verbandsliga
Schleswig-Holstein	Schleswig-Holstein	k1gMnSc1	Schleswig-Holstein
Südost	Südost	k1gInSc1	Südost
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
Kreisliga	Kreisliga	k1gFnSc1	Kreisliga
Ostholstein	Ostholsteina	k1gFnPc2	Ostholsteina
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Verbandsliga	Verbandsliga	k1gFnSc1	Verbandsliga
Schleswig-Holstein	Schleswig-Holstein	k1gMnSc1	Schleswig-Holstein
Südost	Südost	k1gInSc1	Südost
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
:	:	kIx,	:
Fußball-Schleswig-Holstein-Liga	Fußball-Schleswig-Holstein-Liga	k1gFnSc1	Fußball-Schleswig-Holstein-Liga
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
<g/>
–	–	k?	–
<g/>
2018	[number]	k4	2018
<g/>
:	:	kIx,	:
Fußball-Regionalliga	Fußball-Regionalliga	k1gFnSc1	Fußball-Regionalliga
Nord	Norda	k1gFnPc2	Norda
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
<g/>
–	–	k?	–
:	:	kIx,	:
Fußball-Oberliga	Fußball-Oberlig	k1gMnSc2	Fußball-Oberlig
Schleswig-HolsteinJednotlivé	Schleswig-HolsteinJednotlivý	k2eAgInPc4d1	Schleswig-HolsteinJednotlivý
ročníkyZdroj	ročníkyZdroj	k1gInSc4	ročníkyZdroj
<g/>
:	:	kIx,	:
Legenda	legenda	k1gFnSc1	legenda
<g/>
:	:	kIx,	:
Z	z	k7c2	z
–	–	k?	–
zápasy	zápas	k1gInPc4	zápas
<g/>
,	,	kIx,	,
V	v	k7c6	v
–	–	k?	–
výhry	výhra	k1gFnPc1	výhra
<g/>
,	,	kIx,	,
R	R	kA	R
–	–	k?	–
remízy	remíza	k1gFnPc4	remíza
<g/>
,	,	kIx,	,
P	P	kA	P
–	–	k?	–
porážky	porážka	k1gFnSc2	porážka
<g/>
,	,	kIx,	,
VG	VG	kA	VG
–	–	k?	–
vstřelené	vstřelený	k2eAgInPc1d1	vstřelený
góly	gól	k1gInPc1	gól
<g/>
,	,	kIx,	,
OG	OG	kA	OG
–	–	k?	–
obdržené	obdržený	k2eAgInPc4d1	obdržený
góly	gól	k1gInPc4	gól
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
–	–	k?	–
rozdíl	rozdíl	k1gInSc1	rozdíl
skóre	skóre	k1gNnSc2	skóre
<g/>
,	,	kIx,	,
B	B	kA	B
–	–	k?	–
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc4d1	červené
podbarvení	podbarvení	k1gNnSc4	podbarvení
–	–	k?	–
sestup	sestup	k1gInSc1	sestup
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgNnSc4d1	zelené
podbarvení	podbarvení	k1gNnSc4	podbarvení
–	–	k?	–
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
fialové	fialový	k2eAgNnSc1d1	fialové
podbarvení	podbarvení	k1gNnSc1	podbarvení
–	–	k?	–
reorganizace	reorganizace	k1gFnSc1	reorganizace
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
skupiny	skupina	k1gFnSc2	skupina
či	či	k8xC	či
soutěže	soutěž	k1gFnSc2	soutěž
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Eutin	Eutin	k1gInSc1	Eutin
08	[number]	k4	08
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
