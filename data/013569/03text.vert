<s>
Cikády	cikáda	k1gFnPc1
</s>
<s>
Cikády	cikáda	k1gFnPc1
Fidicina	Fidicin	k2eAgNnSc2d1
mannifera	mannifero	k1gNnSc2
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
členovci	členovec	k1gMnPc1
(	(	kIx(
<g/>
Arthropoda	Arthropoda	k1gMnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
šestinozí	šestinohý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Hexapoda	Hexapoda	k1gMnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
hmyz	hmyz	k1gInSc1
(	(	kIx(
<g/>
Insecta	Insecta	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
polokřídlí	polokřídlý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Hemiptera	Hemipter	k1gMnSc2
<g/>
)	)	kIx)
Podřád	podřád	k1gInSc1
</s>
<s>
Auchenorrhyncha	Auchenorrhyncha	k1gFnSc1
Infrařád	Infrařáda	k1gFnPc2
</s>
<s>
Cicadomorpha	Cicadomorpha	k1gFnSc1
Nadčeleď	nadčeleď	k1gFnSc1
</s>
<s>
cikády	cikáda	k1gFnPc1
(	(	kIx(
<g/>
Cicadoidea	Cicadoide	k2eAgFnSc1d1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
cikádovití	cikádovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Cicadidae	Cicadidae	k1gNnSc7
<g/>
)	)	kIx)
<g/>
Westwood	Westwood	k1gInSc1
<g/>
,	,	kIx,
1840	#num#	k4
podčeledi	podčeleď	k1gFnPc1
</s>
<s>
Cicadettinae	Cicadettinae	k6eAd1
</s>
<s>
Cicadinae	Cicadinae	k6eAd1
</s>
<s>
Tettigadinae	Tettigadinae	k6eAd1
</s>
<s>
Tibiceninae	Tibiceninae	k6eAd1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Cikády	cikáda	k1gFnPc1
(	(	kIx(
<g/>
Cicadoidea	Cicadoide	k2eAgFnSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nadčeleď	nadčeleď	k1gFnSc1
hmyzu	hmyz	k1gInSc2
patřící	patřící	k2eAgFnSc1d1
dříve	dříve	k6eAd2
mezi	mezi	k7c4
stejnokřídlé	stejnokřídlý	k2eAgNnSc4d1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
řazena	řadit	k5eAaImNgFnS
do	do	k7c2
řádu	řád	k1gInSc2
polokřídlí	polokřídlý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Hemiptera	Hemipter	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Dospělý	dospělý	k2eAgMnSc1d1
jedinec	jedinec	k1gMnSc1
je	být	k5eAaImIp3nS
dlouhý	dlouhý	k2eAgInSc4d1
kolem	kolem	k6eAd1
2	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
některé	některý	k3yIgInPc1
tropické	tropický	k2eAgInPc1d1
druhy	druh	k1gInPc1
však	však	k9
dosahují	dosahovat	k5eAaImIp3nP
i	i	k9
délky	délka	k1gFnSc2
15	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cikády	cikáda	k1gFnPc1
mají	mít	k5eAaImIp3nP
velmi	velmi	k6eAd1
dobré	dobrý	k2eAgNnSc4d1
velké	velký	k2eAgNnSc4d1
oči	oko	k1gNnPc4
<g/>
,	,	kIx,
umístěné	umístěný	k2eAgFnPc4d1
odděleně	odděleně	k6eAd1
na	na	k7c6
stranách	strana	k1gFnPc6
hlavy	hlava	k1gFnSc2
<g/>
,	,	kIx,
kromě	kromě	k7c2
nich	on	k3xPp3gMnPc2
mají	mít	k5eAaImIp3nP
ještě	ještě	k9
tři	tři	k4xCgNnPc1
malá	malý	k2eAgNnPc1d1
očka	očko	k1gNnPc1
umístěná	umístěný	k2eAgFnSc1d1
nad	nad	k7c7
nimi	on	k3xPp3gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgInPc7d1
znaky	znak	k1gInPc7
jsou	být	k5eAaImIp3nP
poměrně	poměrně	k6eAd1
krátká	krátký	k2eAgNnPc4d1
tykadla	tykadlo	k1gNnPc4
a	a	k8xC
blanitá	blanitý	k2eAgNnPc4d1
křídla	křídlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouštní	pouštní	k2eAgFnPc4d1
cikády	cikáda	k1gFnPc4
se	se	k3xPyFc4
také	také	k6eAd1
jako	jako	k8xS,k8xC
jedny	jeden	k4xCgFnPc1
z	z	k7c2
mála	málo	k1gNnSc2
dokáží	dokázat	k5eAaPmIp3nP
ochlazovat	ochlazovat	k5eAaImF
pocením	pocení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samečci	sameček	k1gMnPc1
vyluzují	vyluzovat	k5eAaImIp3nP
charakteristické	charakteristický	k2eAgInPc4d1
hlasité	hlasitý	k2eAgInPc4d1
cvrkavé	cvrkavý	k2eAgInPc4d1
zvuky	zvuk	k1gInPc4
pomocí	pomocí	k7c2
membrány	membrána	k1gFnSc2
na	na	k7c6
zadečku	zadeček	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
nejhlučnějšího	hlučný	k2eAgMnSc4d3
zástupce	zástupce	k1gMnSc4
z	z	k7c2
říše	říš	k1gFnSc2
hmyzu	hmyz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cikády	cikáda	k1gFnPc1
také	také	k9
můžeme	moct	k5eAaImIp1nP
nalézt	nalézt	k5eAaBmF,k5eAaPmF
u	u	k7c2
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Zástupci	zástupce	k1gMnPc1
</s>
<s>
cikáda	cikáda	k1gFnSc1
chlumní	chlumní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Cicadetta	Cicadetta	k1gFnSc1
montana	montana	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
cikáda	cikáda	k1gFnSc1
trnková	trnkový	k2eAgFnSc1d1
(	(	kIx(
<g/>
Cicadivetta	Cicadivetta	k1gFnSc1
tibialis	tibialis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
cikáda	cikáda	k1gFnSc1
viničná	viničný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Tibicina	Tibicin	k2eAgFnSc1d1
haematodes	haematodes	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
cikáda	cikáda	k1gFnSc1
sedmnáctiletá	sedmnáctiletý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Magicicada	Magicicada	k1gFnSc1
septemdecim	septemdecim	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
