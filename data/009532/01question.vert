<s>
Jaká	jaký	k3yRgFnSc1	jaký
řeka	řeka	k1gFnSc1	řeka
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
východě	východ	k1gInSc6	východ
ČLR	ČLR	kA	ČLR
<g/>
,	,	kIx,	,
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
5464	[number]	k4	5464
km	km	kA	km
druhý	druhý	k4xOgInSc4	druhý
nejdelší	dlouhý	k2eAgInSc4d3	nejdelší
tok	tok	k1gInSc4	tok
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
<g/>
?	?	kIx.	?
</s>
