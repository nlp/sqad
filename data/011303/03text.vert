<p>
<s>
Clinton	Clinton	k1gMnSc1	Clinton
Richard	Richard	k1gMnSc1	Richard
Dawkins	Dawkinsa	k1gFnPc2	Dawkinsa
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
1941	[number]	k4	1941
Nairobi	Nairobi	k1gNnSc2	Nairobi
<g/>
,	,	kIx,	,
Keňa	Keňa	k1gFnSc1	Keňa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgMnSc1d1	britský
zoolog	zoolog	k1gMnSc1	zoolog
<g/>
,	,	kIx,	,
etolog	etolog	k1gMnSc1	etolog
a	a	k8xC	a
biolog	biolog	k1gMnSc1	biolog
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
širší	široký	k2eAgFnSc4d2	širší
veřejnost	veřejnost	k1gFnSc4	veřejnost
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc1	vliv
jako	jako	k8xS	jako
popularizátor	popularizátor	k1gMnSc1	popularizátor
evoluční	evoluční	k2eAgFnSc2d1	evoluční
teorie	teorie	k1gFnSc2	teorie
a	a	k8xC	a
sociobiologie	sociobiologie	k1gFnSc2	sociobiologie
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
svým	svůj	k3xOyFgInSc7	svůj
odchodem	odchod	k1gInSc7	odchod
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
profesor	profesor	k1gMnSc1	profesor
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
se	se	k3xPyFc4	se
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
jako	jako	k9	jako
osmiletý	osmiletý	k2eAgMnSc1d1	osmiletý
<g/>
.	.	kIx.	.
</s>
<s>
Ukončil	ukončit	k5eAaPmAgMnS	ukončit
Oundle	Oundle	k1gFnSc7	Oundle
School	Schoola	k1gFnPc2	Schoola
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Northampton	Northampton	k1gInSc4	Northampton
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
studiem	studio	k1gNnSc7	studio
zoologie	zoologie	k1gFnSc2	zoologie
na	na	k7c4	na
Balliol	Balliol	k1gInSc4	Balliol
College	Colleg	k1gFnSc2	Colleg
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
bakaláře	bakalář	k1gMnSc2	bakalář
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
studiích	studio	k1gNnPc6	studio
jako	jako	k8xC	jako
žák	žák	k1gMnSc1	žák
nositele	nositel	k1gMnSc2	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
etologa	etolog	k1gMnSc2	etolog
Nikolaase	Nikolaasa	k1gFnSc3	Nikolaasa
Tinbergena	Tinbergen	k1gMnSc2	Tinbergen
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
magistra	magister	k1gMnSc2	magister
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
doktorský	doktorský	k2eAgInSc4d1	doktorský
titul	titul	k1gInSc4	titul
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1967-1969	[number]	k4	1967-1969
byl	být	k5eAaImAgInS	být
asistentem	asistent	k1gMnSc7	asistent
na	na	k7c6	na
Kalifornské	kalifornský	k2eAgFnSc6d1	kalifornská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c4	v
Berkeley	Berkele	k1gMnPc4	Berkele
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
na	na	k7c4	na
Oxford	Oxford	k1gInSc4	Oxford
vyučovat	vyučovat	k5eAaImF	vyučovat
zoologii	zoologie	k1gFnSc4	zoologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Pozornost	pozornost	k1gFnSc1	pozornost
světové	světový	k2eAgFnSc2d1	světová
veřejnosti	veřejnost	k1gFnSc2	veřejnost
upoutal	upoutat	k5eAaPmAgInS	upoutat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
knihou	kniha	k1gFnSc7	kniha
Sobecký	sobecký	k2eAgInSc1d1	sobecký
gen	gen	k1gInSc1	gen
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
publikována	publikován	k2eAgFnSc1d1	publikována
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jím	jíst	k5eAaImIp1nS	jíst
částečně	částečně	k6eAd1	částečně
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
popularizovaná	popularizovaný	k2eAgFnSc1d1	popularizovaná
teorie	teorie	k1gFnSc1	teorie
sobeckého	sobecký	k2eAgInSc2d1	sobecký
genu	gen	k1gInSc2	gen
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
součástí	součást	k1gFnSc7	součást
širokého	široký	k2eAgNnSc2d1	široké
povědomí	povědomí	k1gNnSc2	povědomí
<g/>
.	.	kIx.	.
</s>
<s>
Dawkins	Dawkins	k6eAd1	Dawkins
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
základní	základní	k2eAgFnSc7d1	základní
evoluční	evoluční	k2eAgFnSc7d1	evoluční
jednotkou	jednotka	k1gFnSc7	jednotka
jsou	být	k5eAaImIp3nP	být
geny	gen	k1gInPc1	gen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
–	–	k?	–
ač	ač	k8xS	ač
samy	sám	k3xTgInPc1	sám
pasivní	pasivní	k2eAgInPc1d1	pasivní
–	–	k?	–
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
složité	složitý	k2eAgFnPc1d1	složitá
reakce	reakce	k1gFnPc1	reakce
vedoucí	vedoucí	k2eAgFnPc1d1	vedoucí
až	až	k9	až
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
složitých	složitý	k2eAgInPc2d1	složitý
biologických	biologický	k2eAgInPc2d1	biologický
strojů	stroj	k1gInPc2	stroj
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
a	a	k8xC	a
šíření	šíření	k1gNnSc4	šíření
genu	gen	k1gInSc2	gen
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yQgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
není	být	k5eNaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
si	se	k3xPyFc3	se
představit	představit	k5eAaPmF	představit
molekulu	molekula	k1gFnSc4	molekula
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
své	svůj	k3xOyFgFnPc4	svůj
kopie	kopie	k1gFnPc4	kopie
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
berte	brát	k5eAaImRp2nP	brát
replikátor	replikátor	k1gInSc4	replikátor
jako	jako	k8xC	jako
šablonu	šablona	k1gFnSc4	šablona
či	či	k8xC	či
chemický	chemický	k2eAgInSc4d1	chemický
vzor	vzor	k1gInSc4	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
tato	tento	k3xDgFnSc1	tento
molekula	molekula	k1gFnSc1	molekula
má	mít	k5eAaImIp3nS	mít
jistou	jistý	k2eAgFnSc4d1	jistá
přilnavost	přilnavost	k1gFnSc4	přilnavost
vůči	vůči	k7c3	vůči
molekulám	molekula	k1gFnPc3	molekula
stejného	stejný	k2eAgInSc2d1	stejný
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
blízkosti	blízkost	k1gFnSc2	blízkost
dostane	dostat	k5eAaPmIp3nS	dostat
stavební	stavební	k2eAgFnSc1d1	stavební
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
vůči	vůči	k7c3	vůči
které	který	k3yRgFnSc3	který
má	mít	k5eAaImIp3nS	mít
afinitu	afinita	k1gFnSc4	afinita
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
zůstane	zůstat	k5eAaPmIp3nS	zůstat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Právě	právě	k9	právě
teorie	teorie	k1gFnSc1	teorie
sobeckého	sobecký	k2eAgInSc2d1	sobecký
genu	gen	k1gInSc2	gen
vedla	vést	k5eAaImAgFnS	vést
Dawkinse	Dawkinse	k1gFnSc1	Dawkinse
k	k	k7c3	k
proklamaci	proklamace	k1gFnSc3	proklamace
kontroverzní	kontroverzní	k2eAgFnSc2d1	kontroverzní
teorie	teorie	k1gFnSc2	teorie
memetické	memetický	k2eAgFnSc2d1	memetická
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
geny	gen	k1gInPc1	gen
replikují	replikovat	k5eAaImIp3nP	replikovat
i	i	k9	i
informační	informační	k2eAgInPc4d1	informační
řetězce	řetězec	k1gInPc4	řetězec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
mem	mem	k?	mem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
kontroverzích	kontroverze	k1gFnPc6	kontroverze
bylo	být	k5eAaImAgNnS	být
slovo	slovo	k1gNnSc1	slovo
mem	mem	k?	mem
zahrnuto	zahrnut	k2eAgNnSc1d1	zahrnuto
do	do	k7c2	do
Oxfordského	oxfordský	k2eAgInSc2d1	oxfordský
slovníku	slovník	k1gInSc2	slovník
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgNnSc1d1	počáteční
nadšení	nadšení	k1gNnSc1	nadšení
touto	tento	k3xDgFnSc7	tento
teorií	teorie	k1gFnSc7	teorie
opadlo	opadnout	k5eAaPmAgNnS	opadnout
koncem	koncem	k7c2	koncem
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
kvůli	kvůli	k7c3	kvůli
těžké	těžký	k2eAgFnSc3d1	těžká
definovatelnosti	definovatelnost	k1gFnSc3	definovatelnost
pojmů	pojem	k1gInPc2	pojem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
memetika	memetika	k1gFnSc1	memetika
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k8xC	jako
metafora	metafora	k1gFnSc1	metafora
na	na	k7c6	na
hraně	hrana	k1gFnSc6	hrana
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
jejího	její	k3xOp3gInSc2	její
slovníku	slovník	k1gInSc2	slovník
však	však	k9	však
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
běžné	běžný	k2eAgNnSc4d1	běžné
užívání	užívání	k1gNnSc4	užívání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
intelektuálním	intelektuální	k2eAgInSc6d1	intelektuální
fóru	fór	k1gInSc6	fór
bestsellerem	bestseller	k1gInSc7	bestseller
The	The	k1gMnSc1	The
Extended	Extended	k1gMnSc1	Extended
Phenotype	Phenotyp	k1gInSc5	Phenotyp
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
fenotyp	fenotyp	k1gInSc1	fenotyp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
zapálení	zapálení	k1gNnSc1	zapálení
pro	pro	k7c4	pro
věc	věc	k1gFnSc4	věc
mu	on	k3xPp3gInSc3	on
vyneslo	vynést	k5eAaPmAgNnS	vynést
přezdívku	přezdívka	k1gFnSc4	přezdívka
Darwinův	Darwinův	k2eAgInSc4d1	Darwinův
rotvajler	rotvajler	k1gInSc4	rotvajler
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
přezdívky	přezdívka	k1gFnSc2	přezdívka
Thomase	Thomas	k1gMnSc2	Thomas
Huxleye	Huxley	k1gFnSc2	Huxley
Darwinův	Darwinův	k2eAgMnSc1d1	Darwinův
buldok	buldok	k1gMnSc1	buldok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
náboženství	náboženství	k1gNnSc3	náboženství
==	==	k?	==
</s>
</p>
<p>
<s>
Dawkins	Dawkins	k6eAd1	Dawkins
je	být	k5eAaImIp3nS	být
ateista	ateista	k1gMnSc1	ateista
a	a	k8xC	a
ostře	ostro	k6eAd1	ostro
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
proti	proti	k7c3	proti
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
spatřuje	spatřovat	k5eAaImIp3nS	spatřovat
analogii	analogie	k1gFnSc4	analogie
počítačového	počítačový	k2eAgInSc2d1	počítačový
viru	vir	k1gInSc2	vir
šířícího	šířící	k2eAgInSc2d1	šířící
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
lidskými	lidský	k2eAgInPc7d1	lidský
mozky	mozek	k1gInPc7	mozek
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
popisuje	popisovat	k5eAaImIp3nS	popisovat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
eseji	esej	k1gInSc6	esej
Viruses	Viruses	k1gMnSc1	Viruses
of	of	k?	of
the	the	k?	the
Mind	Mind	k1gInSc1	Mind
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Viry	vir	k1gInPc1	vir
mysli	mysl	k1gFnSc2	mysl
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Samem	Sam	k1gMnSc7	Sam
Harrisem	Harris	k1gInSc7	Harris
<g/>
,	,	kIx,	,
Christopherem	Christopher	k1gMnSc7	Christopher
Hitchensem	Hitchens	k1gMnSc7	Hitchens
a	a	k8xC	a
Danielem	Daniel	k1gMnSc7	Daniel
Dennettem	Dennett	k1gMnSc7	Dennett
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c2	za
ústřední	ústřední	k2eAgFnSc2d1	ústřední
postavy	postava	k1gFnSc2	postava
nového	nový	k2eAgInSc2d1	nový
ateismu	ateismus	k1gInSc2	ateismus
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
televizní	televizní	k2eAgFnSc4d1	televizní
stanici	stanice	k1gFnSc4	stanice
Channel	Channel	k1gInSc1	Channel
4	[number]	k4	4
připravil	připravit	k5eAaPmAgInS	připravit
dvoudílný	dvoudílný	k2eAgInSc1d1	dvoudílný
dokumentární	dokumentární	k2eAgInSc1d1	dokumentární
pořad	pořad	k1gInSc1	pořad
kritizující	kritizující	k2eAgInSc1d1	kritizující
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Nese	nést	k5eAaImIp3nS	nést
titul	titul	k1gInSc4	titul
Kořen	kořen	k1gInSc4	kořen
všeho	všecek	k3xTgNnSc2	všecek
zla	zlo	k1gNnSc2	zlo
<g/>
?	?	kIx.	?
</s>
<s>
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vysílán	vysílat	k5eAaImNgInS	vysílat
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Dawkins	Dawkins	k6eAd1	Dawkins
je	být	k5eAaImIp3nS	být
nositelem	nositel	k1gMnSc7	nositel
čestných	čestný	k2eAgInPc2d1	čestný
doktorátů	doktorát	k1gInPc2	doktorát
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
ve	v	k7c6	v
Westminsteru	Westminster	k1gInSc6	Westminster
<g/>
,	,	kIx,	,
Durhamu	Durham	k1gInSc6	Durham
a	a	k8xC	a
Hullu	Hull	k1gInSc6	Hull
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
obdržel	obdržet	k5eAaPmAgInS	obdržet
na	na	k7c4	na
Open	Open	k1gInSc4	Open
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Andrew	Andrew	k1gFnSc1	Andrew
University	universita	k1gFnSc2	universita
a	a	k8xC	a
Australian	Australian	k1gInSc1	Australian
National	National	k1gFnSc2	National
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
členem	člen	k1gMnSc7	člen
Královské	královský	k2eAgFnSc2d1	královská
literární	literární	k2eAgFnSc2d1	literární
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
Královské	královský	k2eAgFnSc6d1	královská
přírodovědecké	přírodovědecký	k2eAgFnSc6d1	Přírodovědecká
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
Britské	britský	k2eAgFnSc2d1	britská
humanistické	humanistický	k2eAgFnSc2d1	humanistická
asociace	asociace	k1gFnSc2	asociace
a	a	k8xC	a
čestným	čestný	k2eAgMnSc7d1	čestný
patronem	patron	k1gMnSc7	patron
Trinity	Trinita	k1gFnSc2	Trinita
College	Colleg	k1gFnSc2	Colleg
University	universita	k1gFnSc2	universita
Philosophical	Philosophical	k1gFnSc2	Philosophical
Society	societa	k1gFnSc2	societa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgNnSc1d1	další
ocenění	ocenění	k1gNnSc1	ocenění
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Cena	cena	k1gFnSc1	cena
Královské	královský	k2eAgFnSc2d1	královská
literární	literární	k2eAgFnSc2d1	literární
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Literární	literární	k2eAgFnSc1d1	literární
cena	cena	k1gFnSc1	cena
Los	los	k1gInSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
Times	Times	k1gMnSc1	Times
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
plaketa	plaketa	k1gFnSc1	plaketa
Londýnské	londýnský	k2eAgFnSc2d1	londýnská
zoologické	zoologický	k2eAgFnSc2d1	zoologická
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cena	cena	k1gFnSc1	cena
Michaela	Michael	k1gMnSc2	Michael
Faradaye	Faraday	k1gMnSc2	Faraday
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nakayamova	Nakayamův	k2eAgFnSc1d1	Nakayamův
cena	cena	k1gFnSc1	cena
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Humanista	humanista	k1gMnSc1	humanista
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kistlerova	Kistlerův	k2eAgFnSc1d1	Kistlerův
cena	cena	k1gFnSc1	cena
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
prezidenta	prezident	k1gMnSc2	prezident
Italské	italský	k2eAgFnSc2d1	italská
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kelvinova	Kelvinův	k2eAgFnSc1d1	Kelvinova
cena	cena	k1gFnSc1	cena
Královské	královský	k2eAgFnSc2d1	královská
filozofické	filozofický	k2eAgFnSc2d1	filozofická
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
Glasgow	Glasgow	k1gNnSc6	Glasgow
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Shakespearova	Shakespearův	k2eAgFnSc1d1	Shakespearova
cena	cena	k1gFnSc1	cena
Nadace	nadace	k1gFnSc2	nadace
Alfreda	Alfred	k1gMnSc2	Alfred
Toepfera	Toepfer	k1gMnSc2	Toepfer
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Deschnerova	Deschnerův	k2eAgFnSc1d1	Deschnerův
cena	cena	k1gFnSc1	cena
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
hlasování	hlasování	k1gNnSc6	hlasování
čtenářů	čtenář	k1gMnPc2	čtenář
magazínu	magazín	k1gInSc2	magazín
Prospect	Prospecta	k1gFnPc2	Prospecta
o	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
britských	britský	k2eAgMnPc2d1	britský
intelektuálů	intelektuál	k1gMnPc2	intelektuál
s	s	k7c7	s
náskokem	náskok	k1gInSc7	náskok
dvojnásobku	dvojnásobek	k1gInSc2	dvojnásobek
hlasů	hlas	k1gInPc2	hlas
před	před	k7c7	před
držitelem	držitel	k1gMnSc7	držitel
druhého	druhý	k4xOgNnSc2	druhý
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
sta	sto	k4xCgNnSc2	sto
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
světových	světový	k2eAgMnPc2d1	světový
intelektuálů	intelektuál	k1gMnPc2	intelektuál
stejného	stejný	k2eAgNnSc2d1	stejné
periodika	periodikum	k1gNnSc2	periodikum
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zmínky	zmínka	k1gFnSc2	zmínka
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
knihu	kniha	k1gFnSc4	kniha
Boží	božit	k5eAaImIp3nP	božit
blud	blud	k1gInSc4	blud
parodovali	parodovat	k5eAaImAgMnP	parodovat
Dawkinse	Dawkinse	k1gFnPc4	Dawkinse
i	i	k8xC	i
autoři	autor	k1gMnPc1	autor
animovaného	animovaný	k2eAgInSc2d1	animovaný
seriálu	seriál	k1gInSc2	seriál
South	South	k1gInSc1	South
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
osmého	osmý	k4xOgNnSc2	osmý
studiového	studiový	k2eAgNnSc2d1	studiové
alba	album	k1gNnSc2	album
finské	finský	k2eAgFnSc2d1	finská
symphonic-metalové	symphonicetal	k1gMnPc1	symphonic-metal
kapely	kapela	k1gFnSc2	kapela
Nightwish	Nightwish	k1gInSc1	Nightwish
"	"	kIx"	"
<g/>
Endless	Endless	k1gInSc1	Endless
Forms	Forms	k1gInSc1	Forms
Most	most	k1gInSc1	most
Beautiful	Beautiful	k1gInSc1	Beautiful
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
načetl	načíst	k5eAaBmAgMnS	načíst
mluvené	mluvený	k2eAgNnSc4d1	mluvené
slovo	slovo	k1gNnSc4	slovo
jako	jako	k8xS	jako
podklad	podklad	k1gInSc1	podklad
k	k	k7c3	k
některým	některý	k3yIgFnPc3	některý
písním	píseň	k1gFnPc3	píseň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Knihy	kniha	k1gFnSc2	kniha
==	==	k?	==
</s>
</p>
<p>
<s>
The	The	k?	The
Selfish	Selfish	k1gInSc1	Selfish
Gene	gen	k1gInSc5	gen
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
;	;	kIx,	;
druhé	druhý	k4xOgNnSc4	druhý
vydání	vydání	k1gNnSc4	vydání
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
286092	[number]	k4	286092
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
jako	jako	k8xC	jako
Sobecký	sobecký	k2eAgInSc1d1	sobecký
gen	gen	k1gInSc1	gen
</s>
</p>
<p>
<s>
The	The	k?	The
Extended	Extended	k1gInSc1	Extended
Phenotype	Phenotyp	k1gInSc5	Phenotyp
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
upravené	upravený	k2eAgNnSc4d1	upravené
druhé	druhý	k4xOgNnSc4	druhý
vydání	vydání	k1gNnSc4	vydání
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
0-19-288051-9	[number]	k4	0-19-288051-9
</s>
</p>
<p>
<s>
The	The	k?	The
Blind	Blind	k1gMnSc1	Blind
Watchmaker	Watchmaker	k1gMnSc1	Watchmaker
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
přetisk	přetisk	k1gInSc1	přetisk
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
393	[number]	k4	393
<g/>
-	-	kIx~	-
<g/>
31570	[number]	k4	31570
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
jako	jako	k8xS	jako
Slepý	slepý	k2eAgMnSc1d1	slepý
hodinář	hodinář	k1gMnSc1	hodinář
</s>
</p>
<p>
<s>
River	River	k1gMnSc1	River
Out	Out	k1gFnSc2	Out
Of	Of	k1gMnSc1	Of
Eden	Eden	k1gInSc1	Eden
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
přetisk	přetisk	k1gInSc1	přetisk
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
465	[number]	k4	465
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6990	[number]	k4	6990
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
jako	jako	k8xC	jako
Řeka	řeka	k1gFnSc1	řeka
z	z	k7c2	z
ráje	ráj	k1gInSc2	ráj
</s>
</p>
<p>
<s>
Climbing	Climbing	k1gInSc1	Climbing
Mount	Mount	k1gMnSc1	Mount
Improbable	Improbable	k1gMnSc1	Improbable
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
0-393-31682-3	[number]	k4	0-393-31682-3
</s>
</p>
<p>
<s>
Unweaving	Unweaving	k1gInSc1	Unweaving
the	the	k?	the
Rainbow	Rainbow	k1gFnSc2	Rainbow
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
paperback	paperback	k1gInSc1	paperback
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
0-618-05673-4	[number]	k4	0-618-05673-4
</s>
</p>
<p>
<s>
A	a	k9	a
Devil	Devil	k1gInSc1	Devil
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Chaplain	Chaplaina	k1gFnPc2	Chaplaina
(	(	kIx(	(
<g/>
vybrané	vybraný	k2eAgFnSc2d1	vybraná
eseje	esej	k1gFnSc2	esej
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
0-618-33540-4	[number]	k4	0-618-33540-4
</s>
</p>
<p>
<s>
The	The	k?	The
Ancestor	Ancestor	k1gInSc1	Ancestor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Tale	Tale	k1gFnSc7	Tale
<g/>
:	:	kIx,	:
A	a	k8xC	a
Pilgrimage	Pilgrimage	k1gFnSc7	Pilgrimage
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Dawn	Dawn	k1gInSc1	Dawn
of	of	k?	of
Life	Life	k1gInSc1	Life
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
618	[number]	k4	618
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
583	[number]	k4	583
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
jako	jako	k8xC	jako
Příběh	příběh	k1gInSc1	příběh
předka	předek	k1gMnSc4	předek
</s>
</p>
<p>
<s>
The	The	k?	The
God	God	k1gFnSc1	God
Delusion	Delusion	k1gInSc1	Delusion
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
618	[number]	k4	618
<g/>
-	-	kIx~	-
<g/>
68000	[number]	k4	68000
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
jako	jako	k8xS	jako
Boží	boží	k2eAgInSc1d1	boží
blud	blud	k1gInSc1	blud
</s>
</p>
<p>
<s>
The	The	k?	The
Greatest	Greatest	k1gFnSc1	Greatest
Show	show	k1gFnSc2	show
on	on	k3xPp3gMnSc1	on
Earth	Earth	k1gMnSc1	Earth
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
593	[number]	k4	593
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6173	[number]	k4	6173
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
jako	jako	k8xC	jako
Největší	veliký	k2eAgFnSc1d3	veliký
show	show	k1gFnSc1	show
pod	pod	k7c7	pod
sluncem	slunce	k1gNnSc7	slunce
</s>
</p>
<p>
<s>
==	==	k?	==
Dokumentární	dokumentární	k2eAgInPc1d1	dokumentární
filmy	film	k1gInPc1	film
==	==	k?	==
</s>
</p>
<p>
<s>
Nice	Nice	k1gFnSc1	Nice
Guys	Guysa	k1gFnPc2	Guysa
Finish	Finish	k1gMnSc1	Finish
First	First	k1gMnSc1	First
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Blind	Blind	k1gMnSc1	Blind
Watchmaker	Watchmaker	k1gMnSc1	Watchmaker
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Growing	Growing	k1gInSc1	Growing
Up	Up	k1gFnSc2	Up
in	in	k?	in
the	the	k?	the
Universe	Universe	k1gFnSc2	Universe
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Break	break	k1gInSc1	break
the	the	k?	the
Science	Science	k1gFnSc1	Science
Barrier	Barrier	k1gMnSc1	Barrier
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Root	Root	k1gMnSc1	Root
of	of	k?	of
All	All	k1gMnSc1	All
Evil	Evil	k1gMnSc1	Evil
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Enemies	Enemies	k1gMnSc1	Enemies
of	of	k?	of
Reason	Reason	k1gMnSc1	Reason
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Genius	genius	k1gMnSc1	genius
of	of	k?	of
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Expelled	Expelled	k1gInSc1	Expelled
<g/>
:	:	kIx,	:
No	no	k9	no
Intelligence	Intelligence	k1gFnSc1	Intelligence
Allowed	Allowed	k1gMnSc1	Allowed
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Purpose	Purposa	k1gFnSc3	Purposa
of	of	k?	of
Purpose	Purposa	k1gFnSc3	Purposa
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
–	–	k?	–
řada	řada	k1gFnSc1	řada
přednášek	přednáška	k1gFnPc2	přednáška
po	po	k7c6	po
amerických	americký	k2eAgFnPc6d1	americká
univerzitách	univerzita	k1gFnPc6	univerzita
</s>
</p>
<p>
<s>
Faith	Faith	k1gInSc1	Faith
School	Schoola	k1gFnPc2	Schoola
Menace	Menace	k1gFnSc2	Menace
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Beautiful	Beautiful	k1gInSc1	Beautiful
Minds	Minds	k1gInSc1	Minds
(	(	kIx(	(
<g/>
duben	duben	k1gInSc1	duben
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
–	–	k?	–
dokument	dokument	k1gInSc1	dokument
BBC4	BBC4	k1gMnSc2	BBC4
</s>
</p>
<p>
<s>
Sex	sex	k1gInSc1	sex
<g/>
,	,	kIx,	,
Death	Death	k1gInSc1	Death
and	and	k?	and
the	the	k?	the
Meaning	Meaning	k1gInSc1	Meaning
of	of	k?	of
Life	Life	k1gInSc1	Life
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Unbelievers	Unbelievers	k1gInSc1	Unbelievers
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Richard	Richard	k1gMnSc1	Richard
Dawkins	Dawkinsa	k1gFnPc2	Dawkinsa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Richard	Richarda	k1gFnPc2	Richarda
Dawkins	Dawkinsa	k1gFnPc2	Dawkinsa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Richard	Richard	k1gMnSc1	Richard
Dawkins	Dawkins	k1gInSc1	Dawkins
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Richard	Richard	k1gMnSc1	Richard
Dawkins	Dawkinsa	k1gFnPc2	Dawkinsa
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Dawkins	Dawkinsa	k1gFnPc2	Dawkinsa
Foundation	Foundation	k1gInSc1	Foundation
(	(	kIx(	(
<g/>
nadace	nadace	k1gFnSc1	nadace
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
vědecké	vědecký	k2eAgFnSc2d1	vědecká
gramotnosti	gramotnost	k1gFnSc2	gramotnost
a	a	k8xC	a
sekularismu	sekularismus	k1gInSc2	sekularismus
<g/>
)	)	kIx)	)
-	-	kIx~	-
anglicky	anglicky	k6eAd1	anglicky
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Dawkins	Dawkins	k1gInSc1	Dawkins
<g/>
:	:	kIx,	:
Viruses	Viruses	k1gInSc1	Viruses
of	of	k?	of
the	the	k?	the
Mind	Mind	k1gInSc1	Mind
-	-	kIx~	-
celý	celý	k2eAgInSc1d1	celý
text	text	k1gInSc1	text
anglicky	anglicky	k6eAd1	anglicky
</s>
</p>
<p>
<s>
Rozhovor	rozhovor	k1gInSc1	rozhovor
v	v	k7c6	v
ČT	ČT	kA	ČT
Hyde	Hyde	k1gNnSc1	Hyde
Park	park	k1gInSc1	park
Civilizace	civilizace	k1gFnSc1	civilizace
-	-	kIx~	-
česky	česky	k6eAd1	česky
i	i	k9	i
anglicky	anglicky	k6eAd1	anglicky
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
:	:	kIx,	:
O	o	k7c6	o
lásce	láska	k1gFnSc6	láska
k	k	k7c3	k
přírodě	příroda	k1gFnSc3	příroda
mezi	mezi	k7c7	mezi
Broukem	brouk	k1gMnSc7	brouk
a	a	k8xC	a
Dawkinsem	Dawkins	k1gInSc7	Dawkins
(	(	kIx(	(
<g/>
Analogon	analogon	k1gNnSc1	analogon
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
září	září	k1gNnSc2	září
<g/>
]	]	kIx)	]
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
r.	r.	kA	r.
[	[	kIx(	[
<g/>
23	[number]	k4	23
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
č.	č.	k?	č.
64	[number]	k4	64
<g/>
,	,	kIx,	,
s.	s.	k?	s.
83	[number]	k4	83
<g/>
–	–	k?	–
<g/>
85	[number]	k4	85
<g/>
)	)	kIx)	)
</s>
</p>
