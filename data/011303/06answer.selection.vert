<s>
Dawkins	Dawkins	k6eAd1	Dawkins
je	být	k5eAaImIp3nS	být
ateista	ateista	k1gMnSc1	ateista
a	a	k8xC	a
ostře	ostro	k6eAd1	ostro
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
proti	proti	k7c3	proti
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
spatřuje	spatřovat	k5eAaImIp3nS	spatřovat
analogii	analogie	k1gFnSc4	analogie
počítačového	počítačový	k2eAgInSc2d1	počítačový
viru	vir	k1gInSc2	vir
šířícího	šířící	k2eAgInSc2d1	šířící
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
lidskými	lidský	k2eAgInPc7d1	lidský
mozky	mozek	k1gInPc7	mozek
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
popisuje	popisovat	k5eAaImIp3nS	popisovat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
eseji	esej	k1gInSc6	esej
Viruses	Viruses	k1gMnSc1	Viruses
of	of	k?	of
the	the	k?	the
Mind	Mind	k1gInSc1	Mind
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Viry	vir	k1gInPc1	vir
mysli	mysl	k1gFnSc2	mysl
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
