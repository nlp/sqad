<p>
<s>
Etta	Etta	k6eAd1	Etta
Jamesová	Jamesový	k2eAgFnSc1d1	Jamesová
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Riverside	Riversid	k1gMnSc5	Riversid
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
rhythm	rhythm	k6eAd1	rhythm
and	and	k?	and
bluesová	bluesový	k2eAgFnSc1d1	bluesová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
do	do	k7c2	do
Blues	blues	k1gFnSc2	blues
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
také	také	k9	také
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
Rockabilly	Rockabilla	k1gFnSc2	Rockabilla
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
leukemii	leukemie	k1gFnSc4	leukemie
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
73	[number]	k4	73
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Etta	Ett	k1gInSc2	Ett
James	Jamesa	k1gFnPc2	Jamesa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
