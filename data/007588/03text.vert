<s>
The	The	k?	The
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc1	underground
[	[	kIx(	[
<g/>
ð	ð	k?	ð
ˈ	ˈ	k?	ˈ
ˌ	ˌ	k?	ˌ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
<g/>
;	;	kIx,	;
existovala	existovat	k5eAaImAgFnS	existovat
jen	jen	k9	jen
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Původními	původní	k2eAgMnPc7d1	původní
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
Lou	Lou	k1gMnSc1	Lou
Reed	Reed	k1gMnSc1	Reed
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
harmonika	harmonika	k1gFnSc1	harmonika
<g/>
)	)	kIx)	)
a	a	k8xC	a
John	John	k1gMnSc1	John
Cale	Cal	k1gFnSc2	Cal
(	(	kIx(	(
<g/>
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
viola	viola	k1gFnSc1	viola
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sterling	sterling	k1gInSc1	sterling
Morrison	Morrison	k1gInSc1	Morrison
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
)	)	kIx)	)
a	a	k8xC	a
Angus	Angus	k1gMnSc1	Angus
MacLise	MacLise	k1gFnSc2	MacLise
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posledního	poslední	k2eAgMnSc4d1	poslední
jmenovaného	jmenovaný	k1gMnSc4	jmenovaný
ještě	ještě	k6eAd1	ještě
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
rok	rok	k1gInSc4	rok
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
bubenice	bubenice	k1gFnSc1	bubenice
Maureen	Maurena	k1gFnPc2	Maurena
Tucker	Tuckra	k1gFnPc2	Tuckra
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
skupina	skupina	k1gFnSc1	skupina
neměla	mít	k5eNaImAgFnS	mít
prakticky	prakticky	k6eAd1	prakticky
žádný	žádný	k3yNgInSc1	žádný
komerční	komerční	k2eAgInSc1d1	komerční
úspěch	úspěch	k1gInSc1	úspěch
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
a	a	k8xC	a
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
skupin	skupina	k1gFnPc2	skupina
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
i	i	k9	i
hudební	hudební	k2eAgMnSc1d1	hudební
teoretik	teoretik	k1gMnSc1	teoretik
Brian	Brian	k1gMnSc1	Brian
Eno	Eno	k1gMnSc1	Eno
<g/>
,	,	kIx,	,
když	když	k8xS	když
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vydání	vydání	k1gNnSc2	vydání
jejich	jejich	k3xOp3gNnSc1	jejich
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
koupilo	koupit	k5eAaPmAgNnS	koupit
jen	jen	k9	jen
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInSc1	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skoro	skoro	k6eAd1	skoro
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
pak	pak	k9	pak
založil	založit	k5eAaPmAgMnS	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
skupiny	skupina	k1gFnSc2	skupina
byl	být	k5eAaImAgMnS	být
jejich	jejich	k3xOp3gInSc7	jejich
manažerem	manažer	k1gInSc7	manažer
Andy	Anda	k1gFnSc2	Anda
Warhol	Warhol	k1gInSc1	Warhol
<g/>
,	,	kIx,	,
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
The	The	k1gFnSc6	The
Factory	Factor	k1gMnPc4	Factor
a	a	k8xC	a
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
jeho	on	k3xPp3gInSc4	on
Exploding	Exploding	k1gInSc4	Exploding
Plastic	Plastice	k1gFnPc2	Plastice
Inevitable	Inevitable	k1gFnPc2	Inevitable
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
The	The	k1gFnSc2	The
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc1	underground
&	&	k?	&
Nico	Nico	k6eAd1	Nico
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
a	a	k8xC	a
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
<g/>
,	,	kIx,	,
herečkou	herečka	k1gFnSc7	herečka
a	a	k8xC	a
modelkou	modelka	k1gFnSc7	modelka
Nico	Nico	k6eAd1	Nico
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
sta	sto	k4xCgNnSc2	sto
největších	veliký	k2eAgMnPc2d3	veliký
umělců	umělec	k1gMnPc2	umělec
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
časopisu	časopis	k1gInSc2	časopis
Rolling	Rolling	k1gInSc4	Rolling
Stone	ston	k1gInSc5	ston
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
19	[number]	k4	19
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
pěti	pět	k4xCc2	pět
set	sto	k4xCgNnPc2	sto
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
alb	album	k1gNnPc2	album
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
jejich	jejich	k3xOp3gFnSc1	jejich
první	první	k4xOgFnSc1	první
čtyři	čtyři	k4xCgNnPc4	čtyři
alba	album	k1gNnPc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
sestavě	sestava	k1gFnSc6	sestava
vydala	vydat	k5eAaPmAgFnS	vydat
ještě	ještě	k9	ještě
jedno	jeden	k4xCgNnSc4	jeden
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgMnS	být
John	John	k1gMnSc1	John
Cale	Cal	k1gInSc2	Cal
vyhozen	vyhodit	k5eAaPmNgMnS	vyhodit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Doug	Doug	k1gMnSc1	Doug
Yule	Yul	k1gInSc2	Yul
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
skupinou	skupina	k1gFnSc7	skupina
hrál	hrát	k5eAaImAgInS	hrát
až	až	k9	až
do	do	k7c2	do
jejího	její	k3xOp3gInSc2	její
rozpadu	rozpad	k1gInSc2	rozpad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc1	underground
vydali	vydat	k5eAaPmAgMnP	vydat
bez	bez	k7c2	bez
Calea	Caleum	k1gNnSc2	Caleum
další	další	k2eAgInPc4d1	další
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
odešel	odejít	k5eAaPmAgMnS	odejít
i	i	k9	i
Lou	Lou	k1gMnSc1	Lou
Reed	Reed	k1gMnSc1	Reed
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
nadále	nadále	k6eAd1	nadále
vedl	vést	k5eAaImAgInS	vést
Yule	Yule	k1gInSc1	Yule
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
vydal	vydat	k5eAaPmAgMnS	vydat
ještě	ještě	k9	ještě
jedno	jeden	k4xCgNnSc4	jeden
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
však	však	k9	však
neobjevil	objevit	k5eNaPmAgMnS	objevit
žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
původních	původní	k2eAgMnPc2d1	původní
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Velvet	Velvet	k1gMnSc1	Velvet
Undreground	Undreground	k1gMnSc1	Undreground
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
sestavě	sestava	k1gFnSc6	sestava
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
sešli	sejít	k5eAaPmAgMnP	sejít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
jeden	jeden	k4xCgInSc4	jeden
benefiční	benefiční	k2eAgInSc4d1	benefiční
koncert	koncert	k1gInSc4	koncert
<g/>
,	,	kIx,	,
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
pro	pro	k7c4	pro
evropské	evropský	k2eAgNnSc4d1	Evropské
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
se	se	k3xPyFc4	se
jednou	jednou	k6eAd1	jednou
zastavili	zastavit	k5eAaPmAgMnP	zastavit
i	i	k9	i
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
na	na	k7c4	na
americké	americký	k2eAgNnSc4d1	americké
turné	turné	k1gNnSc4	turné
zkrachovaly	zkrachovat	k5eAaPmAgFnP	zkrachovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
zemřel	zemřít	k5eAaPmAgInS	zemřít
Sterling	sterling	k1gInSc1	sterling
Morrison	Morrison	k1gInSc4	Morrison
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
šance	šance	k1gFnSc1	šance
skupinu	skupina	k1gFnSc4	skupina
obnovit	obnovit	k5eAaPmF	obnovit
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
klasickém	klasický	k2eAgNnSc6d1	klasické
složení	složení	k1gNnSc6	složení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
byli	být	k5eAaImAgMnP	být
The	The	k1gFnSc4	The
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc4	underground
uvedeni	uvést	k5eAaPmNgMnP	uvést
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
.	.	kIx.	.
</s>
<s>
Úvodní	úvodní	k2eAgNnSc4d1	úvodní
slovo	slovo	k1gNnSc4	slovo
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
přednesla	přednést	k5eAaPmAgFnS	přednést
Patti	Patti	k1gNnSc7	Patti
Smith	Smitha	k1gFnPc2	Smitha
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
The	The	k1gFnSc1	The
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc1	underground
byly	být	k5eAaImAgFnP	být
položeny	položit	k5eAaPmNgInP	položit
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
Lou	Lou	k1gMnSc1	Lou
Reed	Reed	k1gMnSc1	Reed
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
hrál	hrát	k5eAaImAgMnS	hrát
s	s	k7c7	s
několika	několik	k4yIc7	několik
garážovými	garážový	k2eAgInPc7d1	garážový
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
existujícími	existující	k2eAgFnPc7d1	existující
skupinami	skupina	k1gFnPc7	skupina
a	a	k8xC	a
od	od	k7c2	od
září	září	k1gNnSc2	září
1964	[number]	k4	1964
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
skladatel	skladatel	k1gMnSc1	skladatel
pro	pro	k7c4	pro
Pickwick	Pickwick	k1gInSc4	Pickwick
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
na	na	k7c6	na
jakých	jaký	k3yQgFnPc6	jaký
nahrávkách	nahrávka	k1gFnPc6	nahrávka
Reed	Reed	k1gInSc4	Reed
hrál	hrát	k5eAaImAgInS	hrát
nebo	nebo	k8xC	nebo
které	který	k3yIgMnPc4	který
složil	složit	k5eAaPmAgMnS	složit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
John	John	k1gMnSc1	John
Cale	Cal	k1gInSc2	Cal
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
z	z	k7c2	z
rodného	rodný	k2eAgInSc2d1	rodný
Walesu	Wales	k1gInSc2	Wales
počátkem	počátkem	k7c2	počátkem
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
hrál	hrát	k5eAaImAgInS	hrát
s	s	k7c7	s
avantgardními	avantgardní	k2eAgMnPc7d1	avantgardní
hudebníky	hudebník	k1gMnPc7	hudebník
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgMnS	být
například	například	k6eAd1	například
Tony	Tony	k1gMnSc1	Tony
Conrad	Conrada	k1gFnPc2	Conrada
<g/>
,	,	kIx,	,
Angus	Angus	k1gInSc4	Angus
MacLise	MacLise	k1gFnSc2	MacLise
<g/>
,	,	kIx,	,
La	la	k1gNnSc2	la
Monte	Mont	k1gInSc5	Mont
Young	Young	k1gMnSc1	Young
nebo	nebo	k8xC	nebo
Marian	Marian	k1gMnSc1	Marian
Zazeela	Zazeela	k1gFnSc1	Zazeela
<g/>
.	.	kIx.	.
</s>
<s>
Reed	Reed	k6eAd1	Reed
někdy	někdy	k6eAd1	někdy
počátkem	počátkem	k7c2	počátkem
listopadu	listopad	k1gInSc2	listopad
dostal	dostat	k5eAaPmAgMnS	dostat
možnost	možnost	k1gFnSc4	možnost
nahrát	nahrát	k5eAaBmF	nahrát
skladbu	skladba	k1gFnSc4	skladba
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
režii	režie	k1gFnSc6	režie
<g/>
.	.	kIx.	.
</s>
<s>
Vybral	vybrat	k5eAaPmAgMnS	vybrat
si	se	k3xPyFc3	se
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Ostrich	Ostrich	k1gMnSc1	Ostrich
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
B	B	kA	B
<g/>
-	-	kIx~	-
<g/>
straně	strana	k1gFnSc6	strana
singlu	singl	k1gInSc2	singl
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
Smeaky	Smeak	k1gInPc4	Smeak
Pete	Pete	k1gInSc1	Pete
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
interpret	interpret	k1gMnSc1	interpret
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
obalu	obal	k1gInSc6	obal
desky	deska	k1gFnSc2	deska
uvedena	uveden	k2eAgFnSc1d1	uvedena
skupina	skupina	k1gFnSc1	skupina
The	The	k1gMnSc1	The
Primitives	Primitives	k1gMnSc1	Primitives
<g/>
.	.	kIx.	.
</s>
<s>
Producent	producent	k1gMnSc1	producent
Terry	Terra	k1gFnSc2	Terra
Phillips	Phillipsa	k1gFnPc2	Phillipsa
se	se	k3xPyFc4	se
koncem	koncem	k7c2	koncem
listopadu	listopad	k1gInSc2	listopad
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
chtěl	chtít	k5eAaImAgMnS	chtít
skupinu	skupina	k1gFnSc4	skupina
The	The	k1gMnSc1	The
Primitives	Primitives	k1gMnSc1	Primitives
doopravdy	doopravdy	k6eAd1	doopravdy
založit	založit	k5eAaPmF	založit
<g/>
.	.	kIx.	.
</s>
<s>
Potkal	potkat	k5eAaPmAgMnS	potkat
Calea	Calea	k1gMnSc1	Calea
a	a	k8xC	a
Conrada	Conrada	k1gFnSc1	Conrada
a	a	k8xC	a
zeptal	zeptat	k5eAaPmAgMnS	zeptat
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
by	by	kYmCp3nP	by
nechtěli	chtít	k5eNaImAgMnP	chtít
s	s	k7c7	s
nově	nově	k6eAd1	nově
vznikající	vznikající	k2eAgFnSc7d1	vznikající
skupinou	skupina	k1gFnSc7	skupina
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
nabídku	nabídka	k1gFnSc4	nabídka
přijít	přijít	k5eAaPmF	přijít
na	na	k7c4	na
zkoušku	zkouška	k1gFnSc4	zkouška
přijali	přijmout	k5eAaPmAgMnP	přijmout
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Phillipsovi	Phillips	k1gMnSc3	Phillips
řekli	říct	k5eAaPmAgMnP	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
přivedou	přivést	k5eAaPmIp3nP	přivést
i	i	k9	i
bubeníka	bubeník	k1gMnSc2	bubeník
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Calem	Cal	k1gInSc7	Cal
a	a	k8xC	a
Conradem	Conrad	k1gInSc7	Conrad
tedy	tedy	k8xC	tedy
přišel	přijít	k5eAaPmAgMnS	přijít
i	i	k9	i
sochař	sochař	k1gMnSc1	sochař
(	(	kIx(	(
<g/>
a	a	k8xC	a
bubeník	bubeník	k1gMnSc1	bubeník
<g/>
)	)	kIx)	)
Walter	Walter	k1gMnSc1	Walter
De	De	k?	De
Maria	Maria	k1gFnSc1	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Calea	Calea	k1gMnSc1	Calea
<g/>
,	,	kIx,	,
Mariu	Mario	k1gMnSc3	Mario
a	a	k8xC	a
Conrada	Conrada	k1gFnSc1	Conrada
velmi	velmi	k6eAd1	velmi
překvapilo	překvapit	k5eAaPmAgNnS	překvapit
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Ostrich	Ostricha	k1gFnPc2	Ostricha
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc4	všechen
struny	struna	k1gFnPc4	struna
na	na	k7c6	na
kytaře	kytara	k1gFnSc6	kytara
naladěné	naladěný	k2eAgInPc1d1	naladěný
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
to	ten	k3xDgNnSc4	ten
dělali	dělat	k5eAaImAgMnP	dělat
i	i	k9	i
oni	onen	k3xDgMnPc1	onen
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
minimalistických	minimalistický	k2eAgFnPc6d1	minimalistická
kompozicích	kompozice	k1gFnPc6	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
nakonec	nakonec	k6eAd1	nakonec
opravdu	opravdu	k6eAd1	opravdu
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
i	i	k9	i
koncertovat	koncertovat	k5eAaImF	koncertovat
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Ostrich	Ostrich	k1gMnSc1	Ostrich
<g/>
"	"	kIx"	"
/	/	kIx~	/
"	"	kIx"	"
<g/>
Smeaky	Smeak	k1gInPc4	Smeak
Pete	Pete	k1gInSc1	Pete
<g/>
"	"	kIx"	"
vyšel	vyjít	k5eAaPmAgInS	vyjít
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1965	[number]	k4	1965
začali	začít	k5eAaPmAgMnP	začít
The	The	k1gFnSc4	The
Primitives	Primitivesa	k1gFnPc2	Primitivesa
vystupovat	vystupovat	k5eAaImF	vystupovat
častěji	často	k6eAd2	často
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
okolních	okolní	k2eAgInPc6d1	okolní
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jen	jen	k9	jen
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
vzpomínají	vzpomínat	k5eAaImIp3nP	vzpomínat
Cale	Cale	k1gFnSc4	Cale
a	a	k8xC	a
Conrad	Conrad	k1gInSc4	Conrad
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
Pickwick	Pickwicka	k1gFnPc2	Pickwicka
Records	Records	k1gInSc1	Records
Bob	Bob	k1gMnSc1	Bob
Ragona	Ragona	k1gFnSc1	Ragona
ale	ale	k8xC	ale
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hráli	hrát	k5eAaImAgMnP	hrát
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
i	i	k9	i
různé	různý	k2eAgInPc4d1	různý
rozhovory	rozhovor	k1gInPc4	rozhovor
pro	pro	k7c4	pro
rozhlasové	rozhlasový	k2eAgFnPc4d1	rozhlasová
stanice	stanice	k1gFnPc4	stanice
a	a	k8xC	a
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
i	i	k9	i
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
The	The	k1gMnSc1	The
Primitives	Primitives	k1gMnSc1	Primitives
Reed	Reed	k1gMnSc1	Reed
zprvu	zprvu	k6eAd1	zprvu
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
s	s	k7c7	s
psaním	psaní	k1gNnSc7	psaní
písní	píseň	k1gFnPc2	píseň
pro	pro	k7c4	pro
Pickwick	Pickwick	k1gInSc4	Pickwick
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
však	však	k9	však
od	od	k7c2	od
nich	on	k3xPp3gInPc2	on
odešel	odejít	k5eAaPmAgInS	odejít
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechtěli	chtít	k5eNaImAgMnP	chtít
vydat	vydat	k5eAaPmF	vydat
jeho	jeho	k3xOp3gFnPc4	jeho
skladby	skladba	k1gFnPc4	skladba
"	"	kIx"	"
<g/>
Heroin	heroin	k1gInSc1	heroin
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Waiting	Waiting	k1gInSc1	Waiting
for	forum	k1gNnPc2	forum
the	the	k?	the
Man	Man	k1gMnSc1	Man
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c4	v
hraní	hraní	k1gNnSc4	hraní
avantgardní	avantgardní	k2eAgFnSc2d1	avantgardní
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1965	[number]	k4	1965
Cale	Cale	k1gFnPc2	Cale
s	s	k7c7	s
Reedem	Reed	k1gInSc7	Reed
dali	dát	k5eAaPmAgMnP	dát
dohromady	dohromady	k6eAd1	dohromady
skupinu	skupina	k1gFnSc4	skupina
s	s	k7c7	s
názvem	název	k1gInSc7	název
Falling	Falling	k1gInSc1	Falling
Spikes	Spikesa	k1gFnPc2	Spikesa
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
spolu	spolu	k6eAd1	spolu
také	také	k6eAd1	také
hráli	hrát	k5eAaImAgMnP	hrát
po	po	k7c6	po
newyorských	newyorský	k2eAgFnPc6d1	newyorská
ulicích	ulice	k1gFnPc6	ulice
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Reed	Reed	k1gMnSc1	Reed
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
akustickou	akustický	k2eAgFnSc4d1	akustická
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
Cale	Cale	k1gFnSc4	Cale
na	na	k7c4	na
violu	viola	k1gFnSc4	viola
a	a	k8xC	a
zobcovou	zobcový	k2eAgFnSc4d1	zobcová
flétnu	flétna	k1gFnSc4	flétna
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
činnosti	činnost	k1gFnSc6	činnost
také	také	k9	také
potkali	potkat	k5eAaPmAgMnP	potkat
ženu	žena	k1gFnSc4	žena
jménem	jméno	k1gNnSc7	jméno
Elektrah	Elektrah	k1gMnSc1	Elektrah
Lobel	Lobel	k1gMnSc1	Lobel
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
začala	začít	k5eAaPmAgFnS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
jako	jako	k9	jako
kytaristka	kytaristka	k1gFnSc1	kytaristka
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
nahrávka	nahrávka	k1gFnSc1	nahrávka
skladby	skladba	k1gFnSc2	skladba
"	"	kIx"	"
<g/>
Wrap	Wrap	k1gMnSc1	Wrap
Your	Your	k1gMnSc1	Your
Troubles	Troubles	k1gMnSc1	Troubles
in	in	k?	in
Dreams	Dreams	k1gInSc1	Dreams
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
Elektrah	Elektrah	k1gInSc4	Elektrah
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
"	"	kIx"	"
<g/>
vyhozena	vyhozen	k2eAgFnSc1d1	vyhozena
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jí	on	k3xPp3gFnSc3	on
několikrát	několikrát	k6eAd1	několikrát
začaly	začít	k5eAaPmAgFnP	začít
krvácet	krvácet	k5eAaImF	krvácet
ruce	ruka	k1gFnPc4	ruka
kvůli	kvůli	k7c3	kvůli
moc	moc	k6eAd1	moc
tvrdému	tvrdý	k2eAgNnSc3d1	tvrdé
hraní	hraní	k1gNnSc3	hraní
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
stále	stále	k6eAd1	stále
Cale	Cale	k1gFnSc1	Cale
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
s	s	k7c7	s
Conradem	Conrad	k1gInSc7	Conrad
a	a	k8xC	a
jinými	jiný	k2eAgMnPc7d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1965	[number]	k4	1965
se	se	k3xPyFc4	se
Reed	Reed	k1gMnSc1	Reed
v	v	k7c6	v
metru	metro	k1gNnSc6	metro
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
dávným	dávný	k2eAgMnSc7d1	dávný
kamarádem	kamarád	k1gMnSc7	kamarád
Sterlingem	sterling	k1gInSc7	sterling
Morrisonem	Morrison	k1gInSc7	Morrison
<g/>
.	.	kIx.	.
</s>
<s>
Morrison	Morrison	k1gMnSc1	Morrison
se	se	k3xPyFc4	se
ve	v	k7c4	v
Falling	Falling	k1gInSc4	Falling
Spikes	Spikes	k1gMnSc1	Spikes
ujímá	ujímat	k5eAaImIp3nS	ujímat
kytary	kytara	k1gFnPc4	kytara
a	a	k8xC	a
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
začíná	začínat	k5eAaImIp3nS	začínat
hrát	hrát	k5eAaImF	hrát
i	i	k9	i
Caleův	Caleův	k2eAgMnSc1d1	Caleův
kamarád	kamarád	k1gMnSc1	kamarád
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
Angus	Angus	k1gMnSc1	Angus
MacLise	MacLise	k1gFnSc1	MacLise
<g/>
.	.	kIx.	.
</s>
<s>
Morrison	Morrison	k1gNnSc1	Morrison
<g/>
,	,	kIx,	,
MacLise	MacLise	k1gFnSc1	MacLise
<g/>
,	,	kIx,	,
Reed	Reed	k1gInSc1	Reed
a	a	k8xC	a
Cale	Cale	k1gFnSc1	Cale
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
hudební	hudební	k2eAgInSc4d1	hudební
doprovod	doprovod	k1gInSc4	doprovod
k	k	k7c3	k
multimediální	multimediální	k2eAgFnSc3d1	multimediální
akci	akce	k1gFnSc3	akce
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Launching	Launching	k1gInSc4	Launching
the	the	k?	the
Dream	Dream	k1gInSc1	Dream
Weapon	Weapon	k1gInSc1	Weapon
<g/>
.	.	kIx.	.
</s>
<s>
Hráli	hrát	k5eAaImAgMnP	hrát
i	i	k9	i
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
podobných	podobný	k2eAgFnPc6d1	podobná
akcích	akce	k1gFnPc6	akce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
často	často	k6eAd1	často
setkávali	setkávat	k5eAaImAgMnP	setkávat
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
The	The	k1gFnSc2	The
Fugs	Fugsa	k1gFnPc2	Fugsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Warlocks	Warlocks	k1gInSc4	Warlocks
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1965	[number]	k4	1965
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
knihou	kniha	k1gFnSc7	kniha
The	The	k1gFnSc1	The
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
od	od	k7c2	od
Michaela	Michael	k1gMnSc2	Michael
Leigha	Leigh	k1gMnSc2	Leigh
pojednávající	pojednávající	k2eAgInSc4d1	pojednávající
o	o	k7c6	o
sexuální	sexuální	k2eAgFnSc6d1	sexuální
subkultuře	subkultura	k1gFnSc6	subkultura
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Knihu	kniha	k1gFnSc4	kniha
nejspíš	nejspíš	k9	nejspíš
skupině	skupina	k1gFnSc3	skupina
představil	představit	k5eAaPmAgMnS	představit
Calelův	Calelův	k2eAgMnSc1d1	Calelův
přítel	přítel	k1gMnSc1	přítel
Tony	Tony	k1gMnSc1	Tony
Conrad	Conrad	k1gInSc4	Conrad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ji	on	k3xPp3gFnSc4	on
našel	najít	k5eAaPmAgInS	najít
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
skupina	skupina	k1gFnSc1	skupina
přijala	přijmout	k5eAaPmAgFnS	přijmout
název	název	k1gInSc4	název
knihy	kniha	k1gFnSc2	kniha
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1965	[number]	k4	1965
Reed	Reeda	k1gFnPc2	Reeda
<g/>
,	,	kIx,	,
Cale	Cale	k1gFnPc2	Cale
a	a	k8xC	a
Morrison	Morrisona	k1gFnPc2	Morrisona
nahráli	nahrát	k5eAaBmAgMnP	nahrát
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
na	na	k7c6	na
Ludlow	Ludlow	k1gFnSc6	Ludlow
Street	Streeta	k1gFnPc2	Streeta
celkem	celkem	k6eAd1	celkem
šest	šest	k4xCc1	šest
demosnímků	demosnímek	k1gInPc2	demosnímek
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
skladby	skladba	k1gFnPc1	skladba
"	"	kIx"	"
<g/>
Venus	Venus	k1gInSc1	Venus
in	in	k?	in
Furs	Furs	k1gInSc1	Furs
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
inspirována	inspirován	k2eAgFnSc1d1	inspirována
knihou	kniha	k1gFnSc7	kniha
Venuše	Venuše	k1gFnSc2	Venuše
v	v	k7c6	v
kožichu	kožich	k1gInSc6	kožich
−	−	k?	−
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
Venus	Venus	k1gInSc1	Venus
in	in	k?	in
Furs	Fursa	k1gFnPc2	Fursa
Leopolda	Leopolda	k1gFnSc1	Leopolda
von	von	k1gInSc1	von
Sacher	Sachra	k1gFnPc2	Sachra
<g/>
-	-	kIx~	-
<g/>
Masocha	Masoch	k1gMnSc2	Masoch
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Waiting	Waiting	k1gInSc1	Waiting
for	forum	k1gNnPc2	forum
the	the	k?	the
Man	Man	k1gMnSc1	Man
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Heroin	heroin	k1gInSc1	heroin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
All	All	k1gMnSc1	All
Tomorrow	Tomorrow	k1gMnSc1	Tomorrow
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Parties	Partiesa	k1gFnPc2	Partiesa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Wrap	Wrap	k1gMnSc1	Wrap
Your	Your	k1gMnSc1	Your
Troubles	Troubles	k1gMnSc1	Troubles
in	in	k?	in
Dreams	Dreams	k1gInSc1	Dreams
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Prominent	prominent	k1gMnSc1	prominent
Men	Men	k1gMnSc1	Men
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
rané	raný	k2eAgFnPc1d1	raná
nahrávky	nahrávka	k1gFnPc1	nahrávka
vyšly	vyjít	k5eAaPmAgFnP	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
box	box	k1gInSc4	box
setu	set	k1gInSc2	set
Peel	Peel	k1gInSc1	Peel
Slowly	Slowla	k1gFnSc2	Slowla
and	and	k?	and
See	See	k1gMnSc1	See
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
čtyři	čtyři	k4xCgFnPc1	čtyři
byly	být	k5eAaImAgFnP	být
nahrány	nahrát	k5eAaBmNgFnP	nahrát
znovu	znovu	k6eAd1	znovu
a	a	k8xC	a
vyšly	vyjít	k5eAaPmAgFnP	vyjít
na	na	k7c6	na
debutovém	debutový	k2eAgNnSc6d1	debutové
albu	album	k1gNnSc6	album
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc1	underground
<g/>
,	,	kIx,	,
pátá	pátá	k1gFnSc1	pátá
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
albu	album	k1gNnSc6	album
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Nico	Nico	k6eAd1	Nico
a	a	k8xC	a
šestá	šestý	k4xOgFnSc1	šestý
již	již	k6eAd1	již
nově	nova	k1gFnSc6	nova
nahrána	nahrát	k5eAaPmNgFnS	nahrát
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1965	[number]	k4	1965
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
Alem	alma	k1gFnPc2	alma
Aronowitzem	Aronowitz	k1gMnSc7	Aronowitz
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
poté	poté	k6eAd1	poté
stal	stát	k5eAaPmAgMnS	stát
jejím	její	k3xOp3gInSc7	její
prvním	první	k4xOgMnSc7	první
manažerem	manažer	k1gMnSc7	manažer
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
prosince	prosinec	k1gInSc2	prosinec
jim	on	k3xPp3gMnPc3	on
Aronowitz	Aronowitz	k1gInSc1	Aronowitz
zajistil	zajistit	k5eAaPmAgInS	zajistit
první	první	k4xOgInSc4	první
placený	placený	k2eAgInSc4d1	placený
koncert	koncert	k1gInSc4	koncert
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yIgInSc4	který
dostali	dostat	k5eAaPmAgMnP	dostat
75	[number]	k4	75
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Konal	konat	k5eAaImAgInS	konat
se	se	k3xPyFc4	se
na	na	k7c4	na
Summit	summit	k1gInSc4	summit
High	High	k1gInSc1	High
School	School	k1gInSc1	School
v	v	k7c6	v
Summitu	summit	k1gInSc6	summit
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
předskakovali	předskakovat	k5eAaImAgMnP	předskakovat
skupině	skupina	k1gFnSc3	skupina
The	The	k1gFnSc3	The
Myddle	Myddle	k1gFnSc3	Myddle
Class	Classa	k1gFnPc2	Classa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
Aronowitz	Aronowitz	k1gInSc1	Aronowitz
rovněž	rovněž	k9	rovněž
dělal	dělat	k5eAaImAgInS	dělat
manažera	manažer	k1gMnSc4	manažer
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
o	o	k7c6	o
placeném	placený	k2eAgInSc6d1	placený
koncertu	koncert	k1gInSc6	koncert
doslechl	doslechnout	k5eAaPmAgMnS	doslechnout
MacLise	MacLise	k1gFnPc4	MacLise
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
skupinu	skupina	k1gFnSc4	skupina
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
,	,	kIx,	,
považoval	považovat	k5eAaImAgInS	považovat
to	ten	k3xDgNnSc1	ten
totiž	totiž	k9	totiž
za	za	k7c4	za
její	její	k3xOp3gFnSc4	její
komercionalizaci	komercionalizace	k1gFnSc4	komercionalizace
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Angus	Angus	k1gInSc1	Angus
hrál	hrát	k5eAaImAgInS	hrát
jen	jen	k9	jen
kvůli	kvůli	k7c3	kvůli
umění	umění	k1gNnSc3	umění
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
později	pozdě	k6eAd2	pozdě
Morrison	Morrison	k1gMnSc1	Morrison
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
MacLise	MacLise	k1gFnPc4	MacLise
stihl	stihnout	k5eAaPmAgMnS	stihnout
odejít	odejít	k5eAaPmF	odejít
ještě	ještě	k9	ještě
před	před	k7c7	před
koncertem	koncert	k1gInSc7	koncert
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
zbylí	zbylý	k2eAgMnPc1d1	zbylý
členové	člen	k1gMnPc1	člen
nalézt	nalézt	k5eAaBmF	nalézt
nového	nový	k2eAgMnSc4d1	nový
bubeníka	bubeník	k1gMnSc4	bubeník
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Reed	Reed	k1gInSc1	Reed
s	s	k7c7	s
Morrisonem	Morrison	k1gMnSc7	Morrison
sehnali	sehnat	k5eAaPmAgMnP	sehnat
přes	přes	k7c4	přes
svého	svůj	k3xOyFgMnSc4	svůj
kamaráda	kamarád	k1gMnSc4	kamarád
Jima	Jim	k2eAgFnSc1d1	Jima
Tuckera	Tuckera	k1gFnSc1	Tuckera
jeho	jeho	k3xOp3gFnSc4	jeho
sestru	sestra	k1gFnSc4	sestra
jménem	jméno	k1gNnSc7	jméno
Maureen	Maurena	k1gFnPc2	Maurena
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
placený	placený	k2eAgInSc1d1	placený
koncert	koncert	k1gInSc1	koncert
tedy	tedy	k9	tedy
nakonec	nakonec	k6eAd1	nakonec
opravdu	opravdu	k6eAd1	opravdu
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
a	a	k8xC	a
The	The	k1gFnSc2	The
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc1	underground
svou	svůj	k3xOyFgFnSc7	svůj
hudbou	hudba	k1gFnSc7	hudba
vypudili	vypudit	k5eAaPmAgMnP	vypudit
většinu	většina	k1gFnSc4	většina
návštěvníků	návštěvník	k1gMnPc2	návštěvník
ze	z	k7c2	z
sálu	sál	k1gInSc2	sál
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Myddle	Myddle	k1gMnPc1	Myddle
Class	Classa	k1gFnPc2	Classa
hráli	hrát	k5eAaImAgMnP	hrát
před	před	k7c7	před
poloprázdným	poloprázdný	k2eAgInSc7d1	poloprázdný
sálem	sál	k1gInSc7	sál
<g/>
.	.	kIx.	.
</s>
<s>
Aronowitz	Aronowitz	k1gMnSc1	Aronowitz
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
získal	získat	k5eAaPmAgInS	získat
možnost	možnost	k1gFnSc4	možnost
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Café	café	k1gNnSc2	café
Bizzare	Bizzar	k1gInSc5	Bizzar
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
prosince	prosinec	k1gInSc2	prosinec
ji	on	k3xPp3gFnSc4	on
tam	tam	k6eAd1	tam
objevili	objevit	k5eAaPmAgMnP	objevit
Andy	Anda	k1gFnSc2	Anda
Warhol	Warhol	k1gInSc1	Warhol
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
Morrissey	Morrissea	k1gFnSc2	Morrissea
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
jejími	její	k3xOp3gMnPc7	její
manažery	manažer	k1gMnPc7	manažer
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
počátkem	počátkem	k7c2	počátkem
ledna	leden	k1gInSc2	leden
Warhol	Warhol	k1gInSc4	Warhol
s	s	k7c7	s
Morrisseyem	Morrisseyum	k1gNnSc7	Morrisseyum
začali	začít	k5eAaPmAgMnP	začít
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
shánět	shánět	k5eAaImF	shánět
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
Reedův	Reedův	k2eAgInSc4d1	Reedův
projev	projev	k1gInSc4	projev
nezamlouval	zamlouvat	k5eNaImAgMnS	zamlouvat
<g/>
;	;	kIx,	;
i	i	k9	i
přes	přes	k7c4	přes
Reedův	Reedův	k2eAgInSc4d1	Reedův
velký	velký	k2eAgInSc4d1	velký
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
stala	stát	k5eAaPmAgFnS	stát
Nico	Nico	k1gNnSc4	Nico
<g/>
.	.	kIx.	.
</s>
<s>
Reed	Reed	k1gInSc1	Reed
byl	být	k5eAaImAgInS	být
také	také	k9	také
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
stala	stát	k5eAaPmAgFnS	stát
členkou	členka	k1gFnSc7	členka
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
plakátech	plakát	k1gInPc6	plakát
uváděno	uvádět	k5eAaImNgNnS	uvádět
"	"	kIx"	"
<g/>
The	The	k1gFnSc2	The
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc1	underground
&	&	k?	&
Nico	Nico	k1gNnSc1	Nico
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
zatím	zatím	k6eAd1	zatím
neměli	mít	k5eNaImAgMnP	mít
možnost	možnost	k1gFnSc4	možnost
nikde	nikde	k6eAd1	nikde
koncertovat	koncertovat	k5eAaImF	koncertovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měli	mít	k5eAaImAgMnP	mít
možnost	možnost	k1gFnSc4	možnost
zkoušet	zkoušet	k5eAaImF	zkoušet
ve	v	k7c6	v
Warholově	Warholův	k2eAgFnSc6d1	Warholova
The	The	k1gFnSc6	The
Factory	Factor	k1gMnPc4	Factor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
ledna	leden	k1gInSc2	leden
měli	mít	k5eAaImAgMnP	mít
The	The	k1gMnSc1	The
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc4	underground
vystoupení	vystoupení	k1gNnSc2	vystoupení
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
výročí	výročí	k1gNnSc2	výročí
Newyorské	newyorský	k2eAgFnSc2d1	newyorská
společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
klinickou	klinický	k2eAgFnSc4d1	klinická
psychiatrii	psychiatrie	k1gFnSc4	psychiatrie
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
promítány	promítán	k2eAgInPc4d1	promítán
filmy	film	k1gInPc4	film
a	a	k8xC	a
tančili	tančit	k5eAaImAgMnP	tančit
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
Gerard	Gerarda	k1gFnPc2	Gerarda
Malanga	Malanga	k1gFnSc1	Malanga
a	a	k8xC	a
Edie	Edie	k1gFnSc1	Edie
Sedgwick	Sedgwicka	k1gFnPc2	Sedgwicka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
ledna	leden	k1gInSc2	leden
byla	být	k5eAaImAgFnS	být
natočena	natočen	k2eAgFnSc1d1	natočena
zkouška	zkouška	k1gFnSc1	zkouška
skupiny	skupina	k1gFnSc2	skupina
ve	v	k7c4	v
Factory	Factor	k1gInPc4	Factor
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
promítaná	promítaný	k2eAgFnSc1d1	promítaná
jako	jako	k8xC	jako
film	film	k1gInSc1	film
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gFnSc1	The
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
&	&	k?	&
Nico	Nico	k1gMnSc1	Nico
<g/>
:	:	kIx,	:
A	A	kA	A
Symphony	Symphona	k1gFnSc2	Symphona
of	of	k?	of
Sound	Sound	k1gMnSc1	Sound
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
skupina	skupina	k1gFnSc1	skupina
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
Reed	Reed	k1gInSc1	Reed
<g/>
/	/	kIx~	/
<g/>
Cale	Cale	k1gFnSc1	Cale
<g/>
/	/	kIx~	/
<g/>
Morrison	Morrison	k1gMnSc1	Morrison
<g/>
/	/	kIx~	/
<g/>
Tucker	Tucker	k1gMnSc1	Tucker
<g/>
/	/	kIx~	/
<g/>
Nico	Nico	k1gMnSc1	Nico
<g/>
,	,	kIx,	,
objevil	objevit	k5eAaPmAgMnS	objevit
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
i	i	k9	i
tříletý	tříletý	k2eAgMnSc1d1	tříletý
syn	syn	k1gMnSc1	syn
Nico	Nico	k6eAd1	Nico
jménem	jméno	k1gNnSc7	jméno
Ari	Ari	k1gFnPc2	Ari
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
osmým	osmý	k4xOgInSc7	osmý
a	a	k8xC	a
třináctým	třináctý	k4xOgInSc7	třináctý
únorem	únor	k1gInSc7	únor
skupina	skupina	k1gFnSc1	skupina
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c4	v
Film-Makers	Film-Makers	k1gInSc4	Film-Makers
<g/>
'	'	kIx"	'
Cinematheque	Cinematheque	k1gInSc1	Cinematheque
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
koncerty	koncert	k1gInPc1	koncert
opět	opět	k6eAd1	opět
doprovodilo	doprovodit	k5eAaPmAgNnS	doprovodit
promítání	promítání	k1gNnSc4	promítání
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
show	show	k1gFnSc1	show
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
Andy	Anda	k1gFnSc2	Anda
Warhol	Warhol	k1gInSc1	Warhol
<g/>
,	,	kIx,	,
Up	Up	k1gFnSc1	Up
<g/>
-	-	kIx~	-
<g/>
Tight	Tight	k1gInSc1	Tight
a	a	k8xC	a
začínala	začínat	k5eAaImAgFnS	začínat
promítáním	promítání	k1gNnSc7	promítání
filmu	film	k1gInSc2	film
Lupe	lupat	k5eAaImIp3nS	lupat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
film	film	k1gInSc1	film
skončí	skončit	k5eAaPmIp3nS	skončit
<g/>
,	,	kIx,	,
přijdou	přijít	k5eAaPmIp3nP	přijít
Velvet	Velvet	k1gInSc4	Velvet
Underground	underground	k1gInSc1	underground
a	a	k8xC	a
začnou	začít	k5eAaPmIp3nP	začít
hrát	hrát	k5eAaImF	hrát
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
okolo	okolo	k7c2	okolo
nich	on	k3xPp3gInPc2	on
tančí	tančit	k5eAaImIp3nS	tančit
Malanga	Malanga	k1gFnSc1	Malanga
<g/>
,	,	kIx,	,
Sedgwick	Sedgwick	k1gInSc1	Sedgwick
a	a	k8xC	a
Danny	Danna	k1gFnPc1	Danna
Williams	Williamsa	k1gFnPc2	Williamsa
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
show	show	k1gFnPc1	show
se	se	k3xPyFc4	se
nelíbily	líbit	k5eNaImAgFnP	líbit
Nico	Nico	k6eAd1	Nico
ani	ani	k8xC	ani
Sedgwick	Sedgwick	k1gInSc4	Sedgwick
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc4	dva
tedy	tedy	k9	tedy
od	od	k7c2	od
Warhola	Warhola	k1gFnSc1	Warhola
odešly	odejít	k5eAaPmAgFnP	odejít
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
jmenovaná	jmenovaný	k2eAgFnSc1d1	jmenovaná
a	a	k8xC	a
o	o	k7c4	o
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
ji	on	k3xPp3gFnSc4	on
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Ingrid	Ingrid	k1gFnSc1	Ingrid
Superstar	superstar	k1gFnSc1	superstar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
března	březen	k1gInSc2	březen
skupina	skupina	k1gFnSc1	skupina
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
obchodu	obchod	k1gInSc6	obchod
s	s	k7c7	s
oblečením	oblečení	k1gNnSc7	oblečení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
Betsey	Betsey	k1gInPc4	Betsey
Johnson	Johnsona	k1gFnPc2	Johnsona
<g/>
,	,	kIx,	,
a	a	k8xC	a
Warhol	Warhol	k1gInSc1	Warhol
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
obrátil	obrátit	k5eAaPmAgMnS	obrátit
s	s	k7c7	s
prosbou	prosba	k1gFnSc7	prosba
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
nevytvořila	vytvořit	k5eNaPmAgFnS	vytvořit
pro	pro	k7c4	pro
členy	člen	k1gMnPc4	člen
skupiny	skupina	k1gFnSc2	skupina
kostýmy	kostým	k1gInPc4	kostým
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
dubna	duben	k1gInSc2	duben
1966	[number]	k4	1966
se	se	k3xPyFc4	se
The	The	k1gFnSc1	The
Velver	Velver	k1gInSc1	Velver
Underground	underground	k1gInSc1	underground
stali	stát	k5eAaPmAgMnP	stát
součástí	součást	k1gFnSc7	součást
Warholovy	Warholův	k2eAgFnSc2d1	Warholova
nové	nový	k2eAgFnSc2d1	nová
show	show	k1gFnSc2	show
s	s	k7c7	s
názvem	název	k1gInSc7	název
Exploding	Exploding	k1gInSc1	Exploding
Plastic	Plastice	k1gFnPc2	Plastice
Inevitable	Inevitable	k1gFnSc2	Inevitable
(	(	kIx(	(
<g/>
EPI	EPI	kA	EPI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
duben	duben	k1gInSc4	duben
s	s	k7c7	s
EPI	EPI	kA	EPI
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
The	The	k1gFnSc2	The
Dom	Dom	k?	Dom
a	a	k8xC	a
při	při	k7c6	při
jejich	jejich	k3xOp3gInSc6	jejich
koncertu	koncert	k1gInSc6	koncert
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
promítány	promítán	k2eAgInPc4d1	promítán
filmy	film	k1gInPc4	film
Sleep	Sleep	k1gInSc1	Sleep
<g/>
,	,	kIx,	,
Kiss	Kiss	k1gInSc1	Kiss
<g/>
,	,	kIx,	,
Emperor	Emperor	k1gMnSc1	Emperor
<g/>
,	,	kIx,	,
Blow	Blow	k1gMnSc1	Blow
Job	Job	k1gMnSc1	Job
<g/>
,	,	kIx,	,
Vinyl	vinyl	k1gInSc1	vinyl
a	a	k8xC	a
Couch	Couch	k1gInSc1	Couch
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
tanečníků	tanečník	k1gMnPc2	tanečník
se	se	k3xPyFc4	se
tentokrát	tentokrát	k6eAd1	tentokrát
ujali	ujmout	k5eAaPmAgMnP	ujmout
Malanga	Malanga	k1gFnSc1	Malanga
<g/>
,	,	kIx,	,
Ingrid	Ingrid	k1gFnSc1	Ingrid
Superstar	superstar	k1gFnSc1	superstar
<g/>
,	,	kIx,	,
Mary	Mary	k1gFnSc1	Mary
Woronov	Woronov	k1gInSc1	Woronov
<g/>
,	,	kIx,	,
Ronnie	Ronnie	k1gFnSc1	Ronnie
Cutrone	Cutron	k1gInSc5	Cutron
a	a	k8xC	a
Eric	Eric	k1gFnSc4	Eric
Emerson	Emersona	k1gFnPc2	Emersona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
měsíci	měsíc	k1gInSc6	měsíc
skupina	skupina	k1gFnSc1	skupina
rovněž	rovněž	k9	rovněž
nahrávala	nahrávat	k5eAaImAgFnS	nahrávat
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
v	v	k7c6	v
Scepter	sceptrum	k1gNnPc2	sceptrum
Studios	Studios	k?	Studios
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
Warhol	Warhol	k1gInSc1	Warhol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
moc	moc	k1gFnSc1	moc
času	čas	k1gInSc2	čas
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
nestrávil	strávit	k5eNaPmAgMnS	strávit
<g/>
,	,	kIx,	,
roli	role	k1gFnSc4	role
producenta	producent	k1gMnSc2	producent
tedy	tedy	k9	tedy
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
neoficiálně	neoficiálně	k6eAd1	neoficiálně
převzal	převzít	k5eAaPmAgMnS	převzít
Norman	Norman	k1gMnSc1	Norman
Dolph	Dolph	k1gMnSc1	Dolph
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1966	[number]	k4	1966
skupina	skupina	k1gFnSc1	skupina
podepsala	podepsat	k5eAaPmAgFnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
MGM	MGM	kA	MGM
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
května	květen	k1gInSc2	květen
rovněž	rovněž	k9	rovněž
získala	získat	k5eAaPmAgFnS	získat
možnost	možnost	k1gFnSc4	možnost
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
The	The	k1gMnSc1	The
Trip	Trip	k1gMnSc1	Trip
v	v	k7c6	v
kalifornském	kalifornský	k2eAgNnSc6d1	kalifornské
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
zde	zde	k6eAd1	zde
znovu	znovu	k6eAd1	znovu
s	s	k7c7	s
EPI	EPI	kA	EPI
a	a	k8xC	a
předkapelou	předkapela	k1gFnSc7	předkapela
jim	on	k3xPp3gMnPc3	on
jsou	být	k5eAaImIp3nP	být
The	The	k1gFnSc4	The
Mothers	Mothersa	k1gFnPc2	Mothersa
of	of	k?	of
Invention	Invention	k1gInSc1	Invention
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
The	The	k1gFnSc6	The
Trip	Trip	k1gInSc4	Trip
měli	mít	k5eAaImAgMnP	mít
vystupovat	vystupovat	k5eAaImF	vystupovat
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hned	hned	k6eAd1	hned
po	po	k7c6	po
několika	několik	k4yIc6	několik
koncertech	koncert	k1gInPc6	koncert
klub	klub	k1gInSc4	klub
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
policie	policie	k1gFnSc1	policie
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byly	být	k5eAaImAgInP	být
koncerty	koncert	k1gInPc1	koncert
nuceně	nuceně	k6eAd1	nuceně
zrušeny	zrušit	k5eAaPmNgInP	zrušit
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
nahrávání	nahrávání	k1gNnSc6	nahrávání
skladeb	skladba	k1gFnPc2	skladba
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Učinila	učinit	k5eAaPmAgFnS	učinit
tak	tak	k9	tak
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
TTG	TTG	kA	TTG
Studios	Studios	k?	Studios
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
produkce	produkce	k1gFnSc1	produkce
se	se	k3xPyFc4	se
tentokrát	tentokrát	k6eAd1	tentokrát
ujal	ujmout	k5eAaPmAgMnS	ujmout
Tom	Tom	k1gMnSc1	Tom
Wilson	Wilson	k1gMnSc1	Wilson
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
května	květen	k1gInSc2	květen
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
do	do	k7c2	do
San	San	k1gMnSc2	San
Francisca	Franciscus	k1gMnSc2	Franciscus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
odehrála	odehrát	k5eAaPmAgFnS	odehrát
několik	několik	k4yIc4	několik
koncertů	koncert	k1gInPc2	koncert
ve	v	k7c6	v
Fillmore	Fillmor	k1gInSc5	Fillmor
Auditorium	auditorium	k1gNnSc4	auditorium
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
opět	opět	k6eAd1	opět
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Mothers	Mothersa	k1gFnPc2	Mothersa
of	of	k?	of
Invention	Invention	k1gInSc1	Invention
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1966	[number]	k4	1966
byl	být	k5eAaImAgInS	být
Reed	Reed	k1gInSc1	Reed
hospitalizován	hospitalizován	k2eAgInSc1d1	hospitalizován
s	s	k7c7	s
hepatitidou	hepatitida	k1gFnSc7	hepatitida
a	a	k8xC	a
Nico	Nico	k6eAd1	Nico
odjela	odjet	k5eAaPmAgFnS	odjet
na	na	k7c4	na
Ibizu	Ibiza	k1gFnSc4	Ibiza
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
skupiny	skupina	k1gFnSc2	skupina
odjel	odjet	k5eAaPmAgInS	odjet
koncem	koncem	k7c2	koncem
června	červen	k1gInSc2	červen
do	do	k7c2	do
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měli	mít	k5eAaImAgMnP	mít
týdenní	týdenní	k2eAgNnSc4d1	týdenní
angažmá	angažmá	k1gNnSc4	angažmá
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Poor	Poor	k1gMnSc1	Poor
Richard	Richard	k1gMnSc1	Richard
<g/>
'	'	kIx"	'
<g/>
s.	s.	k?	s.
Zbylí	zbylý	k2eAgMnPc1d1	zbylý
členové	člen	k1gMnPc1	člen
si	se	k3xPyFc3	se
museli	muset	k5eAaImAgMnP	muset
vyměnit	vyměnit	k5eAaPmF	vyměnit
nástroje	nástroj	k1gInPc4	nástroj
<g/>
:	:	kIx,	:
Tucker	Tuckero	k1gNnPc2	Tuckero
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
baskytary	baskytara	k1gFnPc4	baskytara
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
skladbách	skladba	k1gFnPc6	skladba
kytary	kytara	k1gFnSc2	kytara
a	a	k8xC	a
za	za	k7c7	za
bicími	bicí	k2eAgFnPc7d1	bicí
ji	on	k3xPp3gFnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
MacLise	MacLise	k1gFnSc1	MacLise
<g/>
.	.	kIx.	.
</s>
<s>
Koncerty	koncert	k1gInPc1	koncert
měly	mít	k5eAaImAgFnP	mít
takový	takový	k3xDgInSc4	takový
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
angažmá	angažmá	k1gNnSc1	angažmá
protáhlo	protáhnout	k5eAaPmAgNnS	protáhnout
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
vyšel	vyjít	k5eAaPmAgInS	vyjít
první	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
The	The	k1gFnSc2	The
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc1	underground
<g/>
;	;	kIx,	;
obsahoval	obsahovat	k5eAaImAgMnS	obsahovat
skladby	skladba	k1gFnPc4	skladba
"	"	kIx"	"
<g/>
All	All	k1gMnSc1	All
Tomorrow	Tomorrow	k1gMnSc1	Tomorrow
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Parties	Partiesa	k1gFnPc2	Partiesa
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
Be	Be	k1gMnSc1	Be
Your	Your	k1gMnSc1	Your
Mirror	Mirror	k1gMnSc1	Mirror
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
srpnu	srpen	k1gInSc6	srpen
skupina	skupina	k1gFnSc1	skupina
kvůli	kvůli	k7c3	kvůli
Reedově	Reedův	k2eAgFnSc3d1	Reedova
nemoci	nemoc	k1gFnSc3	nemoc
nekoncertuje	koncertovat	k5eNaImIp3nS	koncertovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
počátkem	počátkem	k7c2	počátkem
srpna	srpen	k1gInSc2	srpen
nahrála	nahrát	k5eAaPmAgFnS	nahrát
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Noise	Noise	k1gFnSc1	Noise
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
vyšla	vyjít	k5eAaPmAgFnS	vyjít
na	na	k7c6	na
albu	album	k1gNnSc6	album
The	The	k1gFnSc2	The
East	Easta	k1gFnPc2	Easta
Village	Village	k1gFnSc7	Village
Other	Othra	k1gFnPc2	Othra
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
září	září	k1gNnSc2	září
odehrála	odehrát	k5eAaPmAgFnS	odehrát
sérii	série	k1gFnSc3	série
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
Provincetownu	Provincetown	k1gInSc6	Provincetown
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Massachusetts	Massachusetts	k1gNnSc2	Massachusetts
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
získala	získat	k5eAaPmAgFnS	získat
měsíční	měsíční	k2eAgFnSc4d1	měsíční
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
klubem	klub	k1gInSc7	klub
Balloon	Balloon	k1gMnSc1	Balloon
Farm	Farm	k1gMnSc1	Farm
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
budově	budova	k1gFnSc6	budova
jako	jako	k9	jako
klub	klub	k1gInSc1	klub
The	The	k1gMnSc2	The
Dom	Dom	k?	Dom
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrála	hrát	k5eAaImAgFnS	hrát
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
jen	jen	k9	jen
na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
patře	patro	k1gNnSc6	patro
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
několika	několik	k4yIc6	několik
koncertech	koncert	k1gInPc6	koncert
nemocného	mocný	k2eNgMnSc4d1	nemocný
Calea	Caleus	k1gMnSc4	Caleus
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Henry	Henry	k1gMnSc1	Henry
Flynt	Flynt	k1gMnSc1	Flynt
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
října	říjen	k1gInSc2	říjen
1966	[number]	k4	1966
hrají	hrát	k5eAaImIp3nP	hrát
The	The	k1gMnSc1	The
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc4	underground
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
Warholových	Warholový	k2eAgInPc2d1	Warholový
obrazů	obraz	k1gInPc2	obraz
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
a	a	k8xC	a
předposlední	předposlední	k2eAgInSc4d1	předposlední
říjnový	říjnový	k2eAgInSc4d1	říjnový
den	den	k1gInSc4	den
v	v	k7c6	v
hangáru	hangár	k1gInSc6	hangár
na	na	k7c6	na
Leicesterském	Leicesterský	k2eAgNnSc6d1	Leicesterský
letišti	letiště	k1gNnSc6	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
listopadu	listopad	k1gInSc2	listopad
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
doma	doma	k6eAd1	doma
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
do	do	k7c2	do
Mayfair	Mayfaira	k1gFnPc2	Mayfaira
Studios	Studios	k?	Studios
<g/>
,	,	kIx,	,
producentem	producent	k1gMnSc7	producent
jim	on	k3xPp3gMnPc3	on
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
Wilson	Wilson	k1gMnSc1	Wilson
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
zde	zde	k6eAd1	zde
nahráli	nahrát	k5eAaBmAgMnP	nahrát
poslední	poslední	k2eAgFnSc4d1	poslední
skladbu	skladba	k1gFnSc4	skladba
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
dlouho	dlouho	k6eAd1	dlouho
očekávané	očekávaný	k2eAgNnSc4d1	očekávané
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
<g/>
;	;	kIx,	;
skladba	skladba	k1gFnSc1	skladba
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Sunday	Sunday	k1gInPc4	Sunday
Morning	Morning	k1gInSc1	Morning
<g/>
"	"	kIx"	"
a	a	k8xC	a
původně	původně	k6eAd1	původně
ji	on	k3xPp3gFnSc4	on
měla	mít	k5eAaImAgFnS	mít
nazpívat	nazpívat	k5eAaBmF	nazpívat
Nico	Nico	k6eAd1	Nico
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
konečné	konečný	k2eAgFnSc6d1	konečná
verzi	verze	k1gFnSc6	verze
však	však	k9	však
podílí	podílet	k5eAaImIp3nS	podílet
jen	jen	k9	jen
doprovodnými	doprovodný	k2eAgInPc7d1	doprovodný
vokály	vokál	k1gInPc7	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nahrávání	nahrávání	k1gNnSc1	nahrávání
skončilo	skončit	k5eAaPmAgNnS	skončit
<g/>
,	,	kIx,	,
odjela	odjet	k5eAaPmAgFnS	odjet
skupina	skupina	k1gFnSc1	skupina
na	na	k7c4	na
několik	několik	k4yIc4	několik
koncertů	koncert	k1gInPc2	koncert
do	do	k7c2	do
Ohia	Ohio	k1gNnSc2	Ohio
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
poprvé	poprvé	k6eAd1	poprvé
vyráží	vyrážet	k5eAaImIp3nS	vyrážet
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
USA	USA	kA	USA
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
kanadském	kanadský	k2eAgNnSc6d1	kanadské
městě	město	k1gNnSc6	město
Hamilton	Hamilton	k1gInSc4	Hamilton
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
několik	několik	k4yIc4	několik
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
Detroitu	Detroit	k1gInSc6	Detroit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1966	[number]	k4	1966
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
svůj	svůj	k3xOyFgInSc4	svůj
druhý	druhý	k4xOgInSc4	druhý
singl	singl	k1gInSc4	singl
<g/>
:	:	kIx,	:
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
A	a	k9	a
byla	být	k5eAaImAgFnS	být
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Sunday	Sundaa	k1gFnSc2	Sundaa
Morning	Morning	k1gInSc1	Morning
<g/>
"	"	kIx"	"
a	a	k8xC	a
na	na	k7c4	na
B	B	kA	B
<g/>
-	-	kIx~	-
<g/>
straně	strana	k1gFnSc3	strana
"	"	kIx"	"
<g/>
Femme	Femm	k1gMnSc5	Femm
Fatale	Fatal	k1gMnSc5	Fatal
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
měsíc	měsíc	k1gInSc4	měsíc
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
třetí	třetí	k4xOgInSc1	třetí
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgInSc1d1	speciální
vydání	vydání	k1gNnSc4	vydání
magazínu	magazín	k1gInSc2	magazín
Aspen	Aspna	k1gFnPc2	Aspna
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
designovali	designovat	k5eAaPmAgMnP	designovat
Warhol	Warhol	k1gInSc4	Warhol
a	a	k8xC	a
David	David	k1gMnSc1	David
Dalton	Dalton	k1gInSc1	Dalton
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
magazínu	magazín	k1gInSc3	magazín
byl	být	k5eAaImAgMnS	být
přiložen	přiložen	k2eAgInSc4d1	přiložen
i	i	k8xC	i
singl	singl	k1gInSc4	singl
obsahující	obsahující	k2eAgFnSc4d1	obsahující
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
White	Whit	k1gMnSc5	Whit
Wind	Winda	k1gFnPc2	Winda
<g/>
"	"	kIx"	"
od	od	k7c2	od
Petera	Peter	k1gMnSc2	Peter
Walkera	Walker	k1gMnSc2	Walker
na	na	k7c4	na
A	A	kA	A
<g/>
-	-	kIx~	-
<g/>
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
na	na	k7c6	na
B	B	kA	B
<g/>
-	-	kIx~	-
<g/>
straně	strana	k1gFnSc3	strana
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Loop	Loop	k1gInSc4	Loop
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
připsanou	připsaný	k2eAgFnSc4d1	připsaná
skupině	skupina	k1gFnSc3	skupina
The	The	k1gFnSc1	The
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
<g/>
;	;	kIx,	;
skladbu	skladba	k1gFnSc4	skladba
ovšem	ovšem	k9	ovšem
nahrál	nahrát	k5eAaBmAgInS	nahrát
sám	sám	k3xTgInSc1	sám
Cale	Cale	k1gInSc1	Cale
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
časopisu	časopis	k1gInSc2	časopis
rovněž	rovněž	k9	rovněž
přispěl	přispět	k5eAaPmAgMnS	přispět
Reed	Reed	k1gMnSc1	Reed
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
napsal	napsat	k5eAaPmAgMnS	napsat
čtyřstránkový	čtyřstránkový	k2eAgInSc4d1	čtyřstránkový
text	text	k1gInSc4	text
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
rock	rock	k1gInSc4	rock
and	and	k?	and
rollu	roll	k1gInSc2	roll
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
skupina	skupina	k1gFnSc1	skupina
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
odehrála	odehrát	k5eAaPmAgFnS	odehrát
své	svůj	k3xOyFgInPc4	svůj
poslední	poslední	k2eAgInPc4d1	poslední
koncerty	koncert	k1gInPc4	koncert
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc4	underground
nahráli	nahrát	k5eAaPmAgMnP	nahrát
pět	pět	k4xCc4	pět
demosnímků	demosnímek	k1gInPc2	demosnímek
<g/>
;	;	kIx,	;
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
skladby	skladba	k1gFnPc4	skladba
"	"	kIx"	"
<g/>
Here	Her	k1gMnSc2	Her
She	She	k1gMnSc1	She
Comes	Comes	k1gMnSc1	Comes
Now	Now	k1gMnSc1	Now
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Sheltered	Sheltered	k1gInSc1	Sheltered
Life	Lif	k1gInSc2	Lif
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
There	Ther	k1gMnSc5	Ther
Is	Is	k1gMnSc5	Is
No	no	k9	no
Reason	Reasona	k1gFnPc2	Reasona
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Not	nota	k1gFnPc2	nota
Too	Too	k1gMnSc1	Too
Sorry	sorry	k9	sorry
(	(	kIx(	(
<g/>
Now	Now	k1gMnSc1	Now
That	That	k1gMnSc1	That
You	You	k1gMnSc1	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
Gone	Gone	k1gNnSc2	Gone
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
All	All	k1gFnSc7	All
Right	Right	k1gMnSc1	Right
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Way	Way	k1gMnSc1	Way
That	That	k1gMnSc1	That
You	You	k1gMnSc1	You
Live	Live	k1gFnSc1	Live
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Značně	značně	k6eAd1	značně
předělaná	předělaný	k2eAgFnSc1d1	předělaná
verze	verze	k1gFnSc1	verze
"	"	kIx"	"
<g/>
Here	Here	k1gFnSc1	Here
She	She	k1gMnSc1	She
Comes	Comes	k1gMnSc1	Comes
Now	Now	k1gMnSc1	Now
<g/>
"	"	kIx"	"
později	pozdě	k6eAd2	pozdě
vyšla	vyjít	k5eAaPmAgFnS	vyjít
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
druhém	druhý	k4xOgNnSc6	druhý
albu	album	k1gNnSc6	album
a	a	k8xC	a
"	"	kIx"	"
<g/>
Sheltered	Sheltered	k1gInSc1	Sheltered
Life	Lif	k1gInSc2	Lif
<g/>
"	"	kIx"	"
vydal	vydat	k5eAaPmAgMnS	vydat
Reed	Reed	k1gMnSc1	Reed
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
sólovém	sólový	k2eAgNnSc6d1	sólové
albu	album	k1gNnSc6	album
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Heart	Heart	k1gInSc1	Heart
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
skladby	skladba	k1gFnPc1	skladba
již	již	k6eAd1	již
znovu	znovu	k6eAd1	znovu
nahrány	nahrát	k5eAaBmNgFnP	nahrát
nebyly	být	k5eNaImAgFnP	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tyto	tento	k3xDgInPc1	tento
demosnímky	demosnímek	k1gInPc1	demosnímek
vyšly	vyjít	k5eAaPmAgInP	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
na	na	k7c6	na
kompilaci	kompilace	k1gFnSc6	kompilace
Peel	Peelum	k1gNnPc2	Peelum
Slowly	Slowla	k1gMnSc2	Slowla
and	and	k?	and
See	See	k1gMnSc2	See
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
ledna	leden	k1gInSc2	leden
skupina	skupina	k1gFnSc1	skupina
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
na	na	k7c6	na
newyorské	newyorský	k2eAgFnSc6d1	newyorská
Steve	Steve	k1gMnSc1	Steve
Paul	Paul	k1gMnSc1	Paul
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Scene	Scen	k1gInSc5	Scen
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
krátké	krátký	k2eAgNnSc1d1	krátké
období	období	k1gNnSc1	období
nekoncertování	nekoncertování	k1gNnSc2	nekoncertování
<g/>
;	;	kIx,	;
Nico	Nico	k6eAd1	Nico
odehrála	odehrát	k5eAaPmAgFnS	odehrát
několik	několik	k4yIc4	několik
sólových	sólový	k2eAgInPc2d1	sólový
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
Domu	dům	k1gInSc6wR	dům
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
ji	on	k3xPp3gFnSc4	on
doprovázeli	doprovázet	k5eAaImAgMnP	doprovázet
různí	různý	k2eAgMnPc1d1	různý
kytaristé	kytarista	k1gMnPc1	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
Reed	Reed	k1gInSc4	Reed
<g/>
,	,	kIx,	,
Cale	Cale	k1gFnSc4	Cale
<g/>
,	,	kIx,	,
Morrison	Morrison	k1gMnSc1	Morrison
<g/>
,	,	kIx,	,
Jackson	Jackson	k1gMnSc1	Jackson
Browne	Brown	k1gInSc5	Brown
nebo	nebo	k8xC	nebo
Tim	Tim	k?	Tim
Buckley	Buckle	k2eAgInPc1d1	Buckle
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
tedy	tedy	k9	tedy
Reed	Reed	k1gMnSc1	Reed
<g/>
,	,	kIx,	,
Morrison	Morrison	k1gMnSc1	Morrison
a	a	k8xC	a
Cale	Cale	k1gFnSc1	Cale
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
o	o	k7c4	o
koncerty	koncert	k1gInPc4	koncert
s	s	k7c7	s
Nico	Nico	k6eAd1	Nico
střídali	střídat	k5eAaImAgMnP	střídat
<g/>
;	;	kIx,	;
ani	ani	k9	ani
jeden	jeden	k4xCgMnSc1	jeden
totiž	totiž	k9	totiž
neměl	mít	k5eNaImAgMnS	mít
o	o	k7c4	o
hraní	hraní	k1gNnSc4	hraní
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
zájem	zájem	k1gInSc1	zájem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
února	únor	k1gInSc2	únor
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc1	underground
bez	bez	k1gInSc4	bez
Nico	Nico	k6eAd1	Nico
hráli	hrát	k5eAaImAgMnP	hrát
ve	v	k7c6	v
Williamstownu	Williamstown	k1gInSc6	Williamstown
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
března	březen	k1gInSc2	březen
1967	[number]	k4	1967
vychází	vycházet	k5eAaImIp3nS	vycházet
po	po	k7c6	po
velmi	velmi	k6eAd1	velmi
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
The	The	k1gFnSc2	The
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
&	&	k?	&
Nico	Nico	k6eAd1	Nico
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
straně	strana	k1gFnSc6	strana
obalu	obal	k1gInSc2	obal
je	být	k5eAaImIp3nS	být
Warholův	Warholův	k2eAgInSc1d1	Warholův
sítotiskový	sítotiskový	k2eAgInSc1d1	sítotiskový
žlutý	žlutý	k2eAgInSc1d1	žlutý
banán	banán	k1gInSc1	banán
a	a	k8xC	a
vedle	vedle	k6eAd1	vedle
něj	on	k3xPp3gMnSc4	on
je	být	k5eAaImIp3nS	být
menším	malý	k2eAgNnSc7d2	menší
písmem	písmo	k1gNnSc7	písmo
napsáno	napsat	k5eAaBmNgNnS	napsat
"	"	kIx"	"
<g/>
Peel	Peel	k1gInSc1	Peel
Slowly	Slowla	k1gMnSc2	Slowla
and	and	k?	and
See	See	k1gMnSc2	See
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Pomalu	pomalu	k6eAd1	pomalu
oloupejte	oloupat	k5eAaPmRp2nP	oloupat
a	a	k8xC	a
uvidíte	uvidět	k5eAaPmIp2nP	uvidět
<g/>
"	"	kIx"	"
–	–	k?	–
po	po	k7c6	po
stržení	stržení	k1gNnSc6	stržení
obrázku	obrázek	k1gInSc2	obrázek
žlutého	žlutý	k2eAgInSc2d1	žlutý
banánu	banán	k1gInSc2	banán
se	se	k3xPyFc4	se
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
místě	místo	k1gNnSc6	místo
objevil	objevit	k5eAaPmAgInS	objevit
oloupaný	oloupaný	k2eAgInSc1d1	oloupaný
růžový	růžový	k2eAgInSc1d1	růžový
banán	banán	k1gInSc1	banán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
však	však	k9	však
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
název	název	k1gInSc1	název
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
na	na	k7c4	na
přední	přední	k2eAgInSc4d1	přední
obal	obal	k1gInSc4	obal
desky	deska	k1gFnSc2	deska
vůbec	vůbec	k9	vůbec
nedostal	dostat	k5eNaPmAgMnS	dostat
<g/>
;	;	kIx,	;
místo	místo	k7c2	místo
názvu	název	k1gInSc2	název
alba	album	k1gNnSc2	album
nebo	nebo	k8xC	nebo
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
banánem	banán	k1gInSc7	banán
napsáno	napsat	k5eAaBmNgNnS	napsat
"	"	kIx"	"
<g/>
Andy	Anda	k1gFnSc2	Anda
Warhol	Warhol	k1gInSc1	Warhol
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pro	pro	k7c4	pro
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
netradiční	tradiční	k2eNgInSc4d1	netradiční
rozkládací	rozkládací	k2eAgInSc4d1	rozkládací
booklet	booklet	k1gInSc4	booklet
<g/>
;	;	kIx,	;
uvnitř	uvnitř	k6eAd1	uvnitř
jsou	být	k5eAaImIp3nP	být
černobílé	černobílý	k2eAgFnPc1d1	černobílá
fotografie	fotografia	k1gFnPc1	fotografia
Nata	Natus	k1gMnSc2	Natus
Finkelsteina	Finkelstein	k1gMnSc2	Finkelstein
a	a	k8xC	a
Billyho	Billy	k1gMnSc2	Billy
Namea	Nameus	k1gMnSc2	Nameus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
obalu	obal	k1gInSc2	obal
jsou	být	k5eAaImIp3nP	být
potom	potom	k6eAd1	potom
Morrisseyovy	Morrisseyův	k2eAgFnPc1d1	Morrisseyův
barevné	barevný	k2eAgFnPc1d1	barevná
fotografie	fotografia	k1gFnPc1	fotografia
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
členů	člen	k1gInPc2	člen
kapely	kapela	k1gFnSc2	kapela
plus	plus	k1gNnSc1	plus
jedna	jeden	k4xCgFnSc1	jeden
větší	veliký	k2eAgFnSc3d2	veliký
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
zachycena	zachytit	k5eAaPmNgFnS	zachytit
celá	celý	k2eAgFnSc1d1	celá
skupina	skupina	k1gFnSc1	skupina
při	při	k7c6	při
koncertě	koncert	k1gInSc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
fotografování	fotografování	k1gNnSc2	fotografování
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
pozadí	pozadí	k1gNnSc4	pozadí
za	za	k7c4	za
hudebníky	hudebník	k1gMnPc4	hudebník
promítán	promítán	k2eAgInSc1d1	promítán
film	film	k1gInSc1	film
Chelsea	Chelseum	k1gNnSc2	Chelseum
Girls	girl	k1gFnPc2	girl
a	a	k8xC	a
zrovna	zrovna	k6eAd1	zrovna
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
byl	být	k5eAaImAgInS	být
vidět	vidět	k5eAaImF	vidět
Eric	Eric	k1gInSc1	Eric
Emerson	Emersona	k1gFnPc2	Emersona
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ale	ale	k9	ale
chtěl	chtít	k5eAaImAgMnS	chtít
od	od	k7c2	od
Verve	vervat	k5eAaPmIp3nS	vervat
Records	Recordsa	k1gFnPc2	Recordsa
peníze	peníz	k1gInPc1	peníz
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
svého	svůj	k3xOyFgInSc2	svůj
snímku	snímek	k1gInSc2	snímek
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
všechny	všechen	k3xTgFnPc4	všechen
desky	deska	k1gFnPc4	deska
z	z	k7c2	z
obchodů	obchod	k1gInPc2	obchod
stáhlo	stáhnout	k5eAaPmAgNnS	stáhnout
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
obličej	obličej	k1gInSc4	obličej
vyretušovalo	vyretušovat	k5eAaPmAgNnS	vyretušovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žebříčku	žebříčko	k1gNnSc6	žebříčko
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
se	se	k3xPyFc4	se
album	album	k1gNnSc1	album
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
171	[number]	k4	171
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
<g/>
;	;	kIx,	;
méně	málo	k6eAd2	málo
známý	známý	k2eAgInSc1d1	známý
časopis	časopis	k1gInSc1	časopis
Cashbox	Cashbox	k1gInSc1	Cashbox
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
přiřknul	přiřknout	k5eAaPmAgInS	přiřknout
102	[number]	k4	102
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
pěti	pět	k4xCc2	pět
set	sto	k4xCgNnPc2	sto
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
alb	album	k1gNnPc2	album
podle	podle	k7c2	podle
časopisu	časopis	k1gInSc2	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
se	se	k3xPyFc4	se
The	The	k1gFnSc2	The
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc1	underground
&	&	k?	&
Nico	Nico	k6eAd1	Nico
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc6	pozice
<g/>
,	,	kIx,	,
čtenáři	čtenář	k1gMnPc1	čtenář
téhož	týž	k3xTgInSc2	týž
časopisu	časopis	k1gInSc2	časopis
jej	on	k3xPp3gMnSc4	on
zvolili	zvolit	k5eAaPmAgMnP	zvolit
mezi	mezi	k7c4	mezi
deset	deset	k4xCc4	deset
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
obalů	obal	k1gInPc2	obal
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jako	jako	k8xS	jako
desáté	desátý	k4xOgNnSc4	desátý
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Jedenáct	jedenáct	k4xCc1	jedenáct
písní	píseň	k1gFnPc2	píseň
odráželo	odrážet	k5eAaImAgNnS	odrážet
umělecký	umělecký	k2eAgInSc4d1	umělecký
rozsah	rozsah	k1gInSc4	rozsah
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
k	k	k7c3	k
nalezení	nalezení	k1gNnSc3	nalezení
různé	různý	k2eAgInPc1d1	různý
typy	typ	k1gInPc1	typ
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
tiché	tichý	k2eAgMnPc4d1	tichý
"	"	kIx"	"
<g/>
Femme	Femm	k1gMnSc5	Femm
Fatale	Fatal	k1gMnSc5	Fatal
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
Be	Be	k1gMnSc1	Be
Your	Your	k1gMnSc1	Your
Mirror	Mirror	k1gMnSc1	Mirror
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
garážové	garážový	k2eAgInPc1d1	garážový
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Waiting	Waiting	k1gInSc1	Waiting
for	forum	k1gNnPc2	forum
the	the	k?	the
Man	Man	k1gMnSc1	Man
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
There	Ther	k1gMnSc5	Ther
She	She	k1gMnSc5	She
Goes	Goes	k1gInSc4	Goes
Again	Again	k1gInSc1	Again
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
experimentální	experimentální	k2eAgInSc1d1	experimentální
"	"	kIx"	"
<g/>
Venus	Venus	k1gInSc1	Venus
in	in	k?	in
Furs	Furs	k1gInSc1	Furs
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Heroin	heroin	k1gInSc1	heroin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Sunday	Sunday	k1gInPc4	Sunday
Morning	Morning	k1gInSc1	Morning
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
popová	popový	k2eAgFnSc1d1	popová
<g/>
.	.	kIx.	.
</s>
<s>
Warhol	Warhol	k1gInSc1	Warhol
si	se	k3xPyFc3	se
nejvíce	nejvíce	k6eAd1	nejvíce
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
"	"	kIx"	"
<g/>
All	All	k1gFnSc1	All
Tomorrow	Tomorrow	k1gMnSc1	Tomorrow
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Parties	Partiesa	k1gFnPc2	Partiesa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
března	březen	k1gInSc2	březen
a	a	k8xC	a
dubna	duben	k1gInSc2	duben
Velvet	Velvet	k1gInSc4	Velvet
Underground	underground	k1gInSc1	underground
–	–	k?	–
opět	opět	k6eAd1	opět
bez	bez	k7c2	bez
Nico	Nico	k1gMnSc1	Nico
–	–	k?	–
hráli	hrát	k5eAaImAgMnP	hrát
v	v	k7c4	v
Providence	providence	k1gFnPc4	providence
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1967	[number]	k4	1967
se	se	k3xPyFc4	se
Reed	Reed	k1gMnSc1	Reed
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Brianem	Brian	k1gMnSc7	Brian
Epsteinem	Epstein	k1gMnSc7	Epstein
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
projevil	projevit	k5eAaPmAgInS	projevit
zájem	zájem	k1gInSc4	zájem
jim	on	k3xPp3gMnPc3	on
dělat	dělat	k5eAaImF	dělat
manažera	manažer	k1gMnSc4	manažer
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
však	však	k9	však
sešlo	sejít	k5eAaPmAgNnS	sejít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
účastnila	účastnit	k5eAaImAgFnS	účastnit
nahrávání	nahrávání	k1gNnSc4	nahrávání
sólového	sólový	k2eAgNnSc2d1	sólové
alba	album	k1gNnSc2	album
Nico	Nico	k6eAd1	Nico
s	s	k7c7	s
názvem	název	k1gInSc7	název
Chelsea	Chelse	k1gInSc2	Chelse
Girl	girl	k1gFnSc2	girl
<g/>
.	.	kIx.	.
</s>
<s>
Hrají	hrát	k5eAaImIp3nP	hrát
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
až	až	k8xS	až
na	na	k7c4	na
Maureen	Maureen	k1gInSc4	Maureen
Tucker	Tuckra	k1gFnPc2	Tuckra
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
Reed	Reed	k1gInSc1	Reed
<g/>
,	,	kIx,	,
Morrison	Morrison	k1gInSc1	Morrison
<g/>
,	,	kIx,	,
Cale	Cale	k1gInSc1	Cale
<g/>
)	)	kIx)	)
doplnění	doplnění	k1gNnSc2	doplnění
Jacksonem	Jackson	k1gMnSc7	Jackson
Brownem	Brown	k1gMnSc7	Brown
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
zároveň	zároveň	k6eAd1	zároveň
napsali	napsat	k5eAaPmAgMnP	napsat
několik	několik	k4yIc4	několik
písní	píseň	k1gFnPc2	píseň
pro	pro	k7c4	pro
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
produkce	produkce	k1gFnSc1	produkce
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
ujal	ujmout	k5eAaPmAgMnS	ujmout
Tom	Tom	k1gMnSc1	Tom
Wilson	Wilson	k1gMnSc1	Wilson
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
měsíci	měsíc	k1gInSc6	měsíc
skupina	skupina	k1gFnSc1	skupina
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
několik	několik	k4yIc4	několik
newyorských	newyorský	k2eAgInPc2d1	newyorský
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
Gymnasiu	gymnasion	k1gNnSc6	gymnasion
<g/>
,	,	kIx,	,
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Cheetah	Cheetaha	k1gFnPc2	Cheetaha
a	a	k8xC	a
opět	opět	k6eAd1	opět
v	v	k7c6	v
Gymnasiu	gymnasion	k1gNnSc6	gymnasion
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
května	květen	k1gInSc2	květen
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
prakticky	prakticky	k6eAd1	prakticky
neaktivní	aktivní	k2eNgFnSc1d1	neaktivní
<g/>
,	,	kIx,	,
první	první	k4xOgInPc1	první
koncerty	koncert	k1gInPc1	koncert
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
až	až	k9	až
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
konci	konec	k1gInSc6	konec
tohoto	tento	k3xDgInSc2	tento
měsíce	měsíc	k1gInSc2	měsíc
v	v	k7c4	v
Boston	Boston	k1gInSc4	Boston
Tea	Tea	k1gFnSc1	Tea
Party	party	k1gFnSc1	party
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
<g/>
.	.	kIx.	.
</s>
<s>
Warhol	Warhol	k1gInSc1	Warhol
není	být	k5eNaImIp3nS	být
těmto	tento	k3xDgInPc3	tento
koncertům	koncert	k1gInPc3	koncert
přítomen	přítomen	k2eAgInSc4d1	přítomen
a	a	k8xC	a
zařídil	zařídit	k5eAaPmAgMnS	zařídit
je	on	k3xPp3gNnPc4	on
Steve	Steve	k1gMnSc1	Steve
Sesnick	Sesnick	k1gMnSc1	Sesnick
<g/>
;	;	kIx,	;
neúčastní	účastnit	k5eNaImIp3nS	účastnit
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
ani	ani	k9	ani
Nico	Nico	k6eAd1	Nico
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
propuštěna	propuštěn	k2eAgFnSc1d1	propuštěna
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
opět	opět	k6eAd1	opět
skupina	skupina	k1gFnSc1	skupina
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c4	v
Boston	Boston	k1gInSc4	Boston
Tea	Tea	k1gFnSc1	Tea
Party	party	k1gFnSc1	party
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
Warholem	Warhol	k1gMnSc7	Warhol
a	a	k8xC	a
Morrisseyem	Morrissey	k1gMnSc7	Morrissey
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
místo	místo	k1gNnSc4	místo
Warhola	Warhola	k1gFnSc1	Warhola
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Sesnick	Sesnick	k1gMnSc1	Sesnick
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
sehnat	sehnat	k5eAaPmF	sehnat
vydavatelskou	vydavatelský	k2eAgFnSc4d1	vydavatelská
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Epsteinem	Epstein	k1gInSc7	Epstein
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1967	[number]	k4	1967
nečekaně	nečekaně	k6eAd1	nečekaně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
londýnském	londýnský	k2eAgInSc6d1	londýnský
bytě	byt	k1gInSc6	byt
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
spolupráce	spolupráce	k1gFnSc2	spolupráce
proto	proto	k8xC	proto
nic	nic	k3yNnSc1	nic
nevzešlo	vzejít	k5eNaPmAgNnS	vzejít
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
do	do	k7c2	do
22	[number]	k4	22
<g/>
.	.	kIx.	.
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
do	do	k7c2	do
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc4	underground
koncertovali	koncertovat	k5eAaImAgMnP	koncertovat
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Trauma	trauma	k1gNnSc4	trauma
ve	v	k7c6	v
Philadelphii	Philadelphia	k1gFnSc6	Philadelphia
<g/>
,	,	kIx,	,
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
následovaly	následovat	k5eAaImAgFnP	následovat
koncerty	koncert	k1gInPc4	koncert
v	v	k7c4	v
Boston	Boston	k1gInSc4	Boston
Tea	Tea	k1gFnSc1	Tea
Party	party	k1gFnSc1	party
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
září	zářit	k5eAaImIp3nP	zářit
začali	začít	k5eAaPmAgMnP	začít
v	v	k7c4	v
Mayfair	Mayfair	k1gInSc4	Mayfair
Studios	Studios	k?	Studios
nahrávat	nahrávat	k5eAaImF	nahrávat
své	svůj	k3xOyFgInPc4	svůj
druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
White	Whit	k1gInSc5	Whit
Light	Light	k1gInSc4	Light
<g/>
/	/	kIx~	/
<g/>
White	Whit	k1gMnSc5	Whit
Heat	Heat	k2eAgMnSc1d1	Heat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
nahrávání	nahrávání	k1gNnSc1	nahrávání
trvalo	trvat	k5eAaImAgNnS	trvat
jen	jen	k9	jen
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celé	celá	k1gFnSc2	celá
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
září	září	k1gNnSc6	září
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
ujal	ujmout	k5eAaPmAgMnS	ujmout
Tom	Tom	k1gMnSc1	Tom
Wilson	Wilson	k1gMnSc1	Wilson
a	a	k8xC	a
nahrávacím	nahrávací	k2eAgMnSc7d1	nahrávací
technikem	technik	k1gMnSc7	technik
byl	být	k5eAaImAgMnS	být
Gerry	Gerra	k1gFnSc2	Gerra
Kellgren	Kellgrna	k1gFnPc2	Kellgrna
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
otevírá	otevírat	k5eAaImIp3nS	otevírat
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
skladba	skladba	k1gFnSc1	skladba
o	o	k7c6	o
amfetaminech	amfetamin	k1gInPc6	amfetamin
"	"	kIx"	"
<g/>
White	Whit	k1gInSc5	Whit
Light	Light	k1gInSc4	Light
<g/>
/	/	kIx~	/
<g/>
White	Whit	k1gMnSc5	Whit
Heat	Heatum	k1gNnPc2	Heatum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Gift	Gift	k1gMnSc1	Gift
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
ke	k	k7c3	k
druhé	druhý	k4xOgFnSc3	druhý
skladbě	skladba	k1gFnSc3	skladba
napsal	napsat	k5eAaPmAgMnS	napsat
Reed	Reed	k1gMnSc1	Reed
ještě	ještě	k9	ještě
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
;	;	kIx,	;
jako	jako	k8xC	jako
hudba	hudba	k1gFnSc1	hudba
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
byl	být	k5eAaImAgInS	být
použit	použit	k2eAgInSc1d1	použit
základ	základ	k1gInSc1	základ
skladby	skladba	k1gFnSc2	skladba
"	"	kIx"	"
<g/>
Booker	Booker	k1gInSc1	Booker
T.	T.	kA	T.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávka	nahrávka	k1gFnSc1	nahrávka
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
nahrána	nahrát	k5eAaBmNgFnS	nahrát
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
kanálu	kanál	k1gInSc2	kanál
a	a	k8xC	a
slova	slovo	k1gNnSc2	slovo
do	do	k7c2	do
druhého	druhý	k4xOgMnSc2	druhý
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
i	i	k9	i
při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
zní	znět	k5eAaImIp3nS	znět
každé	každý	k3xTgNnSc4	každý
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
reproduktoru	reproduktor	k1gInSc2	reproduktor
a	a	k8xC	a
buď	buď	k8xC	buď
jednu	jeden	k4xCgFnSc4	jeden
nebo	nebo	k8xC	nebo
druhou	druhý	k4xOgFnSc4	druhý
část	část	k1gFnSc4	část
lze	lze	k6eAd1	lze
vypustit	vypustit	k5eAaPmF	vypustit
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Gift	Gift	k1gMnSc1	Gift
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
jistý	jistý	k2eAgMnSc1d1	jistý
Wald	Wald	k1gMnSc1	Wald
Jeffers	Jeffersa	k1gFnPc2	Jeffersa
<g/>
;	;	kIx,	;
student	student	k1gMnSc1	student
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
poslat	poslat	k5eAaPmF	poslat
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
poštou	pošta	k1gFnSc7	pošta
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Cale	Cale	k1gInSc1	Cale
celý	celý	k2eAgInSc1d1	celý
osmiminutový	osmiminutový	k2eAgInSc1d1	osmiminutový
text	text	k1gInSc1	text
namluvil	namluvit	k5eAaBmAgInS	namluvit
na	na	k7c4	na
první	první	k4xOgInSc4	první
pokus	pokus	k1gInSc4	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Lady	lady	k1gFnSc1	lady
Godiva	Godiva	k1gFnSc1	Godiva
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Operation	Operation	k1gInSc1	Operation
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Skladbu	skladba	k1gFnSc4	skladba
opět	opět	k6eAd1	opět
zpívá	zpívat	k5eAaImIp3nS	zpívat
Cale	Cale	k1gFnSc1	Cale
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Reed	Reed	k1gInSc1	Reed
občas	občas	k6eAd1	občas
svým	svůj	k3xOyFgInSc7	svůj
hlasem	hlas	k1gInSc7	hlas
také	také	k9	také
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
nejmelodičtější	melodický	k2eAgFnSc7d3	nejmelodičtější
skladbou	skladba	k1gFnSc7	skladba
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
albu	album	k1gNnSc6	album
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Here	Here	k1gFnSc1	Here
She	She	k1gMnSc1	She
Comes	Comes	k1gMnSc1	Comes
Now	Now	k1gMnSc1	Now
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc4d1	poslední
na	na	k7c6	na
A	A	kA	A
<g/>
-	-	kIx~	-
<g/>
straně	strana	k1gFnSc6	strana
původního	původní	k2eAgNnSc2d1	původní
LP	LP	kA	LP
<g/>
.	.	kIx.	.
</s>
<s>
B	B	kA	B
<g/>
-	-	kIx~	-
<g/>
strana	strana	k1gFnSc1	strana
začíná	začínat	k5eAaImIp3nS	začínat
free	free	k6eAd1	free
jazzovou	jazzový	k2eAgFnSc4d1	jazzová
"	"	kIx"	"
<g/>
I	i	k8xC	i
Heard	Heard	k1gMnSc1	Heard
Her	hra	k1gFnPc2	hra
Call	Call	k1gMnSc1	Call
My	my	k3xPp1nPc1	my
Name	Name	k1gNnPc2	Name
<g/>
"	"	kIx"	"
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
sedmnáctiminutovou	sedmnáctiminutový	k2eAgFnSc7d1	sedmnáctiminutová
skladbou	skladba	k1gFnSc7	skladba
"	"	kIx"	"
<g/>
Sister	Sister	k1gMnSc1	Sister
Ray	Ray	k1gMnSc1	Ray
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
21	[number]	k4	21
<g/>
.	.	kIx.	.
a	a	k8xC	a
24	[number]	k4	24
<g/>
.	.	kIx.	.
zářím	zářit	k5eAaImIp1nS	zářit
skupina	skupina	k1gFnSc1	skupina
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Savoy	Savoa	k1gFnSc2	Savoa
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1967	[number]	k4	1967
do	do	k7c2	do
února	únor	k1gInSc2	únor
odehrála	odehrát	k5eAaPmAgFnS	odehrát
velmi	velmi	k6eAd1	velmi
málo	málo	k4c4	málo
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Tucker	Tucker	k1gMnSc1	Tucker
si	se	k3xPyFc3	se
našla	najít	k5eAaPmAgFnS	najít
regulérní	regulérní	k2eAgFnSc4d1	regulérní
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
Cale	Cale	k1gInSc1	Cale
nahrával	nahrávat	k5eAaImAgInS	nahrávat
s	s	k7c7	s
Conradem	Conrad	k1gInSc7	Conrad
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
budoucí	budoucí	k2eAgFnSc7d1	budoucí
ženou	žena	k1gFnSc7	žena
cestovali	cestovat	k5eAaImAgMnP	cestovat
<g/>
;	;	kIx,	;
co	co	k9	co
dělali	dělat	k5eAaImAgMnP	dělat
zbylí	zbylý	k2eAgMnPc1d1	zbylý
dva	dva	k4xCgMnPc1	dva
členové	člen	k1gMnPc1	člen
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
začínají	začínat	k5eAaImIp3nP	začínat
mít	mít	k5eAaImF	mít
Reed	Reed	k1gInSc4	Reed
a	a	k8xC	a
Cale	Cale	k1gFnSc4	Cale
pomalu	pomalu	k6eAd1	pomalu
spory	spor	k1gInPc1	spor
o	o	k7c4	o
budoucí	budoucí	k2eAgNnSc4d1	budoucí
směřování	směřování	k1gNnSc4	směřování
hudby	hudba	k1gFnSc2	hudba
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
vyšel	vyjít	k5eAaPmAgInS	vyjít
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
alba	album	k1gNnSc2	album
<g/>
;	;	kIx,	;
obsahoval	obsahovat	k5eAaImAgMnS	obsahovat
skladby	skladba	k1gFnPc4	skladba
"	"	kIx"	"
<g/>
White	Whit	k1gInSc5	Whit
Light	Light	k1gInSc4	Light
<g/>
/	/	kIx~	/
<g/>
White	Whit	k1gMnSc5	Whit
Heat	Heatum	k1gNnPc2	Heatum
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Here	Here	k1gFnSc1	Here
She	She	k1gMnSc1	She
Comes	Comes	k1gMnSc1	Comes
Now	Now	k1gMnSc1	Now
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
téhož	týž	k3xTgInSc2	týž
měsíce	měsíc	k1gInSc2	měsíc
skupina	skupina	k1gFnSc1	skupina
krátce	krátce	k6eAd1	krátce
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c6	v
Lincolnově	Lincolnův	k2eAgNnSc6d1	Lincolnovo
centru	centrum	k1gNnSc6	centrum
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
skupina	skupina	k1gFnSc1	skupina
nahrála	nahrát	k5eAaPmAgFnS	nahrát
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
instrumentálně	instrumentálně	k6eAd1	instrumentálně
<g/>
)	)	kIx)	)
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Guess	Guess	k1gInSc4	Guess
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Falling	Falling	k1gInSc1	Falling
in	in	k?	in
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vyšla	vyjít	k5eAaPmAgFnS	vyjít
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
na	na	k7c6	na
albu	album	k1gNnSc6	album
Another	Anothra	k1gFnPc2	Anothra
View	View	k1gFnSc2	View
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
White	Whit	k1gInSc5	Whit
Light	Light	k1gInSc4	Light
<g/>
/	/	kIx~	/
<g/>
White	Whit	k1gMnSc5	Whit
Heat	Heat	k1gInSc1	Heat
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
koncem	koncem	k7c2	koncem
ledna	leden	k1gInSc2	leden
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
předním	přední	k2eAgInSc6d1	přední
obalu	obal	k1gInSc6	obal
je	být	k5eAaImIp3nS	být
fotografie	fotografie	k1gFnSc1	fotografie
od	od	k7c2	od
Billyho	Billy	k1gMnSc2	Billy
Namea	Name	k1gInSc2	Name
<g/>
;	;	kIx,	;
ta	ten	k3xDgFnSc1	ten
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
skoro	skoro	k6eAd1	skoro
vůbec	vůbec	k9	vůbec
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
podklad	podklad	k1gInSc4	podklad
<g/>
,	,	kIx,	,
jen	jen	k9	jen
má	mít	k5eAaImIp3nS	mít
trochu	trochu	k6eAd1	trochu
jiný	jiný	k2eAgInSc1d1	jiný
odstín	odstín	k1gInSc1	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
sotva	sotva	k6eAd1	sotva
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
–	–	k?	–
skončilo	skončit	k5eAaPmAgNnS	skončit
na	na	k7c4	na
199	[number]	k4	199
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
pěti	pět	k4xCc2	pět
set	sto	k4xCgNnPc2	sto
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
alb	album	k1gNnPc2	album
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
podle	podle	k7c2	podle
časopisu	časopis	k1gInSc2	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
292	[number]	k4	292
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
řádný	řádný	k2eAgInSc1d1	řádný
koncert	koncert	k1gInSc1	koncert
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
od	od	k7c2	od
září	září	k1gNnSc2	září
předchozího	předchozí	k2eAgInSc2d1	předchozí
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
hned	hned	k6eAd1	hned
první	první	k4xOgInSc4	první
únorový	únorový	k2eAgInSc4d1	únorový
den	den	k1gInSc4	den
v	v	k7c4	v
Aadvark	Aadvark	k1gInSc4	Aadvark
Conematheyue	Conematheyu	k1gFnSc2	Conematheyu
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
února	únor	k1gInSc2	únor
1968	[number]	k4	1968
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
A	A	kA	A
<g/>
&	&	k?	&
<g/>
R	R	kA	R
Studios	Studios	k?	Studios
začala	začít	k5eAaPmAgFnS	začít
opět	opět	k6eAd1	opět
nahrávat	nahrávat	k5eAaImF	nahrávat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Stephanie	Stephanie	k1gFnSc1	Stephanie
Says	Saysa	k1gFnPc2	Saysa
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
vyšla	vyjít	k5eAaPmAgFnS	vyjít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
na	na	k7c4	na
albu	alba	k1gFnSc4	alba
složené	složený	k2eAgNnSc1d1	složené
z	z	k7c2	z
out-taků	outak	k1gInPc2	out-tak
nazvaném	nazvaný	k2eAgInSc6d1	nazvaný
VU	VU	kA	VU
a	a	k8xC	a
v	v	k7c6	v
upravené	upravený	k2eAgFnSc6d1	upravená
podobě	podoba	k1gFnSc6	podoba
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Caroline	Carolin	k1gInSc5	Carolin
Says	Says	k1gInSc1	Says
II	II	kA	II
<g/>
"	"	kIx"	"
na	na	k7c6	na
Reedově	Reedův	k2eAgNnSc6d1	Reedovo
sólovém	sólový	k2eAgNnSc6d1	sólové
albu	album	k1gNnSc6	album
Berlin	berlina	k1gFnPc2	berlina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
zde	zde	k6eAd1	zde
nahráli	nahrát	k5eAaPmAgMnP	nahrát
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Temptation	Temptation	k1gInSc4	Temptation
Inside	Insid	k1gInSc5	Insid
Your	Your	k1gMnSc1	Your
Heart	Heart	k1gInSc1	Heart
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
rovněž	rovněž	k9	rovněž
vyšla	vyjít	k5eAaPmAgFnS	vyjít
na	na	k7c4	na
VU	VU	kA	VU
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
skupina	skupina	k1gFnSc1	skupina
hrála	hrát	k5eAaImAgFnS	hrát
na	na	k7c6	na
Harvardově	Harvardův	k2eAgFnSc6d1	Harvardova
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
massachusettském	massachusettský	k2eAgInSc6d1	massachusettský
Cambridge	Cambridge	k1gFnSc1	Cambridge
jako	jako	k8xS	jako
trio	trio	k1gNnSc1	trio
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
Calea	Cale	k1gInSc2	Cale
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
s	s	k7c7	s
hepatitidou	hepatitida	k1gFnSc7	hepatitida
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
několik	několik	k4yIc4	několik
koncertů	koncert	k1gInPc2	koncert
vynechal	vynechat	k5eAaPmAgInS	vynechat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
skupina	skupina	k1gFnSc1	skupina
hrála	hrát	k5eAaImAgFnS	hrát
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
v	v	k7c4	v
Boston	Boston	k1gInSc4	Boston
Tea	Tea	k1gFnSc1	Tea
Party	party	k1gFnSc1	party
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
se	se	k3xPyFc4	se
Cale	Cale	k1gInSc1	Cale
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
Betsey	Betse	k1gMnPc7	Betse
Johnson	Johnsona	k1gFnPc2	Johnsona
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dále	daleko	k6eAd2	daleko
zhoršilo	zhoršit	k5eAaPmAgNnS	zhoršit
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
Reedem	Reedo	k1gNnSc7	Reedo
a	a	k8xC	a
Calem	Calum	k1gNnSc7	Calum
<g/>
.	.	kIx.	.
</s>
<s>
Následují	následovat	k5eAaImIp3nP	následovat
koncerty	koncert	k1gInPc4	koncert
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
a	a	k8xC	a
Clevelandu	Cleveland	k1gInSc6	Cleveland
<g/>
,	,	kIx,	,
počátkem	počátkem	k7c2	počátkem
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
května	květen	k1gInSc2	květen
opět	opět	k6eAd1	opět
hrají	hrát	k5eAaImIp3nP	hrát
v	v	k7c4	v
Boston	Boston	k1gInSc4	Boston
Tea	Tea	k1gFnSc1	Tea
Party	party	k1gFnPc2	party
a	a	k8xC	a
o	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
i	i	k9	i
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
června	červen	k1gInSc2	červen
1968	[number]	k4	1968
Valerie	Valerie	k1gFnSc1	Valerie
Solanas	Solanasa	k1gFnPc2	Solanasa
postřelila	postřelit	k5eAaPmAgFnS	postřelit
Warhola	Warhola	k1gFnSc1	Warhola
<g/>
;	;	kIx,	;
o	o	k7c4	o
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
spáchán	spáchat	k5eAaPmNgInS	spáchat
atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
Roberta	Robert	k1gMnSc4	Robert
F.	F.	kA	F.
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Warhol	Warhol	k1gInSc1	Warhol
už	už	k6eAd1	už
přibližně	přibližně	k6eAd1	přibližně
rok	rok	k1gInSc1	rok
nebyl	být	k5eNaImAgInS	být
manažerem	manažer	k1gInSc7	manažer
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
Reeda	Reeda	k1gFnSc1	Reeda
zpráva	zpráva	k1gFnSc1	zpráva
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
hráli	hrát	k5eAaImAgMnP	hrát
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc4	underground
opět	opět	k5eAaPmF	opět
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
a	a	k8xC	a
koncem	koncem	k7c2	koncem
měsíce	měsíc	k1gInSc2	měsíc
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
koncertovali	koncertovat	k5eAaImAgMnP	koncertovat
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
července	červenec	k1gInSc2	červenec
hráli	hrát	k5eAaImAgMnP	hrát
v	v	k7c6	v
kalifornském	kalifornský	k2eAgNnSc6d1	kalifornské
San	San	k1gMnSc3	San
Diegu	Dieg	k1gInSc2	Dieg
<g/>
,	,	kIx,	,
o	o	k7c6	o
dalším	další	k2eAgInSc6d1	další
víkendu	víkend	k1gInSc6	víkend
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc4	Angeles
<g/>
,	,	kIx,	,
další	další	k2eAgInSc4d1	další
víkend	víkend	k1gInSc4	víkend
pak	pak	k6eAd1	pak
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
srpna	srpen	k1gInSc2	srpen
opět	opět	k6eAd1	opět
přes	přes	k7c4	přes
víkend	víkend	k1gInSc4	víkend
hráli	hrát	k5eAaImAgMnP	hrát
v	v	k7c4	v
Boston	Boston	k1gInSc4	Boston
Tea	Tea	k1gFnSc1	Tea
Party	party	k1gFnSc1	party
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1968	[number]	k4	1968
Nico	Nico	k6eAd1	Nico
nahrála	nahrát	k5eAaBmAgFnS	nahrát
své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gFnSc1	The
Marble	Marble	k1gMnSc1	Marble
Index	index	k1gInSc1	index
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
skladby	skladba	k1gFnPc1	skladba
si	se	k3xPyFc3	se
napsala	napsat	k5eAaPmAgFnS	napsat
sama	sám	k3xTgFnSc1	sám
<g/>
,	,	kIx,	,
produkce	produkce	k1gFnSc1	produkce
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
Frazier	Frazier	k1gMnSc1	Frazier
Mohawk	Mohawk	k1gMnSc1	Mohawk
a	a	k8xC	a
aranžmá	aranžmá	k1gNnSc4	aranžmá
a	a	k8xC	a
všechny	všechen	k3xTgInPc4	všechen
ostatní	ostatní	k2eAgInPc4d1	ostatní
nástroje	nástroj	k1gInPc4	nástroj
zajistil	zajistit	k5eAaPmAgInS	zajistit
Cale	Cale	k1gInSc1	Cale
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
až	až	k6eAd1	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
září	září	k1gNnSc2	září
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc4	underground
nekoncertovali	koncertovat	k5eNaImAgMnP	koncertovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
hráli	hrát	k5eAaImAgMnP	hrát
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
a	a	k8xC	a
o	o	k7c6	o
dalším	další	k2eAgInSc6d1	další
víkendu	víkend	k1gInSc6	víkend
v	v	k7c4	v
Boston	Boston	k1gInSc4	Boston
Tea	Tea	k1gFnSc1	Tea
Party	party	k1gFnSc1	party
<g/>
.	.	kIx.	.
</s>
<s>
Bostonský	bostonský	k2eAgInSc1d1	bostonský
koncert	koncert	k1gInSc1	koncert
byl	být	k5eAaImAgInS	být
posledním	poslední	k2eAgInSc7d1	poslední
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skupina	skupina	k1gFnSc1	skupina
zahrála	zahrát	k5eAaPmAgFnS	zahrát
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
sestavě	sestava	k1gFnSc6	sestava
<g/>
.	.	kIx.	.
</s>
<s>
Reed	Reed	k6eAd1	Reed
a	a	k8xC	a
Cale	Cal	k1gFnSc2	Cal
nebyli	být	k5eNaImAgMnP	být
stavěni	stavit	k5eAaImNgMnP	stavit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
spolu	spolu	k6eAd1	spolu
hráli	hrát	k5eAaImAgMnP	hrát
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
chtěl	chtít	k5eAaImAgMnS	chtít
skupinu	skupina	k1gFnSc4	skupina
vést	vést	k5eAaImF	vést
jiným	jiný	k2eAgInSc7d1	jiný
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Cale	Cale	k6eAd1	Cale
by	by	kYmCp3nS	by
raději	rád	k6eAd2	rád
stále	stále	k6eAd1	stále
experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
Reed	Reed	k1gMnSc1	Reed
by	by	kYmCp3nS	by
raději	rád	k6eAd2	rád
hudbu	hudba	k1gFnSc4	hudba
zklidnil	zklidnit	k5eAaPmAgMnS	zklidnit
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
dál	daleko	k6eAd2	daleko
v	v	k7c6	v
klidnějším	klidný	k2eAgInSc6d2	klidnější
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
září	září	k1gNnSc2	září
1968	[number]	k4	1968
si	se	k3xPyFc3	se
Reed	Reed	k1gMnSc1	Reed
domluvili	domluvit	k5eAaPmAgMnP	domluvit
setkání	setkání	k1gNnSc4	setkání
s	s	k7c7	s
Morrisonem	Morrison	k1gInSc7	Morrison
a	a	k8xC	a
Tucker	Tucker	k1gInSc4	Tucker
v	v	k7c6	v
kavárně	kavárna	k1gFnSc6	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgMnS	říct
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
Cale	Cale	k1gFnSc1	Cale
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
končí	končit	k5eAaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
hádka	hádka	k1gFnSc1	hádka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
proti	proti	k7c3	proti
Reedovi	Reeda	k1gMnSc3	Reeda
nic	nic	k6eAd1	nic
nezmohli	zmoct	k5eNaPmAgMnP	zmoct
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgInSc7d1	nový
členem	člen	k1gInSc7	člen
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nezkušený	zkušený	k2eNgMnSc1d1	nezkušený
Doug	Doug	k1gMnSc1	Doug
Yule	Yul	k1gFnSc2	Yul
<g/>
.	.	kIx.	.
</s>
<s>
Cestovního	cestovní	k2eAgMnSc4d1	cestovní
manažera	manažer	k1gMnSc4	manažer
jim	on	k3xPp3gInPc3	on
tehdy	tehdy	k6eAd1	tehdy
dělal	dělat	k5eAaImAgMnS	dělat
Hans	Hans	k1gMnSc1	Hans
Onsager	Onsager	k1gMnSc1	Onsager
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
držitele	držitel	k1gMnSc2	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
Larse	Lars	k1gMnSc2	Lars
Onsagera	Onsager	k1gMnSc2	Onsager
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
první	první	k4xOgInPc4	první
koncerty	koncert	k1gInPc4	koncert
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
si	se	k3xPyFc3	se
Yule	Yule	k1gInSc1	Yule
odbyl	odbýt	k5eAaPmAgInS	odbýt
v	v	k7c6	v
Clevelandu	Cleveland	k1gInSc6	Cleveland
jen	jen	k9	jen
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
příchodu	příchod	k1gInSc6	příchod
<g/>
.	.	kIx.	.
</s>
<s>
Následují	následovat	k5eAaImIp3nP	následovat
koncerty	koncert	k1gInPc4	koncert
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
čtyři	čtyři	k4xCgInPc4	čtyři
koncerty	koncert	k1gInPc4	koncert
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1968	[number]	k4	1968
skupina	skupina	k1gFnSc1	skupina
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
TTG	TTG	kA	TTG
Studios	Studios	k?	Studios
nahrávat	nahrávat	k5eAaImF	nahrávat
další	další	k2eAgNnSc4d1	další
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Nepřizvali	přizvat	k5eNaPmAgMnP	přizvat
si	se	k3xPyFc3	se
žádného	žádný	k3yNgMnSc4	žádný
producenta	producent	k1gMnSc4	producent
a	a	k8xC	a
o	o	k7c4	o
zvuk	zvuk	k1gInSc4	zvuk
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
Val	val	k1gInSc4	val
Valentin	Valentina	k1gFnPc2	Valentina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
listopadu	listopad	k1gInSc2	listopad
stále	stále	k6eAd1	stále
koncertovali	koncertovat	k5eAaImAgMnP	koncertovat
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
a	a	k8xC	a
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
v	v	k7c6	v
Clevelandu	Cleveland	k1gInSc6	Cleveland
a	a	k8xC	a
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obou	dva	k4xCgNnPc6	dva
místech	místo	k1gNnPc6	místo
hrají	hrát	k5eAaImIp3nP	hrát
i	i	k8xC	i
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1969	[number]	k4	1969
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
třetí	třetí	k4xOgNnSc1	třetí
album	album	k1gNnSc1	album
skupiny	skupina	k1gFnSc2	skupina
s	s	k7c7	s
ještě	ještě	k6eAd1	ještě
jednodušším	jednoduchý	k2eAgInSc7d2	jednodušší
názvem	název	k1gInSc7	název
než	než	k8xS	než
jejich	jejich	k3xOp3gInSc1	jejich
debut	debut	k1gInSc1	debut
<g/>
:	:	kIx,	:
jmenovalo	jmenovat	k5eAaImAgNnS	jmenovat
se	s	k7c7	s
The	The	k1gMnSc7	The
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
má	mít	k5eAaImIp3nS	mít
černý	černý	k2eAgInSc4d1	černý
obal	obal	k1gInSc4	obal
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
fotografie	fotografie	k1gFnSc1	fotografie
od	od	k7c2	od
Billyho	Billy	k1gMnSc2	Billy
Namea	Nameus	k1gMnSc2	Nameus
<g/>
;	;	kIx,	;
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
usazení	usazený	k2eAgMnPc1d1	usazený
na	na	k7c6	na
pohovce	pohovka	k1gFnSc6	pohovka
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
fotografií	fotografia	k1gFnSc7	fotografia
je	být	k5eAaImIp3nS	být
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zadním	zadní	k2eAgInSc6d1	zadní
obalu	obal	k1gInSc6	obal
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc4	jméno
nahrávacího	nahrávací	k2eAgMnSc2d1	nahrávací
inženýra	inženýr	k1gMnSc2	inženýr
<g/>
,	,	kIx,	,
poděkování	poděkování	k1gNnSc4	poděkování
Billymu	Billym	k1gInSc6	Billym
Nameovi	Nameův	k2eAgMnPc1d1	Nameův
a	a	k8xC	a
fotografie	fotografia	k1gFnPc1	fotografia
Reeda	Reed	k1gMnSc2	Reed
s	s	k7c7	s
cigaretou	cigareta	k1gFnSc7	cigareta
rozříznutá	rozříznutý	k2eAgFnSc1d1	rozříznutá
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Levá	levý	k2eAgFnSc1d1	levá
polovina	polovina	k1gFnSc1	polovina
fotografie	fotografia	k1gFnSc2	fotografia
se	se	k3xPyFc4	se
na	na	k7c6	na
albu	album	k1gNnSc6	album
neobjevila	objevit	k5eNaPmAgFnS	objevit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
pouze	pouze	k6eAd1	pouze
dvakrát	dvakrát	k6eAd1	dvakrát
pravá	pravý	k2eAgFnSc1d1	pravá
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
polovině	polovina	k1gFnSc6	polovina
je	být	k5eAaImIp3nS	být
vzhůru	vzhůru	k6eAd1	vzhůru
nohama	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
otevírá	otevírat	k5eAaImIp3nS	otevírat
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Candy	Cand	k1gInPc1	Cand
Says	Saysa	k1gFnPc2	Saysa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
transvestitovi	transvestit	k1gMnSc6	transvestit
jménem	jméno	k1gNnSc7	jméno
Candy	Cand	k1gInPc4	Cand
Darling	Darling	k1gInSc1	Darling
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
jedinou	jediný	k2eAgFnSc7d1	jediná
skladbou	skladba	k1gFnSc7	skladba
na	na	k7c6	na
albu	album	k1gNnSc6	album
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
zpívá	zpívat	k5eAaImIp3nS	zpívat
sám	sám	k3xTgInSc1	sám
Yule	Yule	k1gInSc1	Yule
<g/>
.	.	kIx.	.
</s>
<s>
Následují	následovat	k5eAaImIp3nP	následovat
skladby	skladba	k1gFnPc4	skladba
"	"	kIx"	"
<g/>
What	What	k2eAgInSc4d1	What
Goes	Goes	k1gInSc4	Goes
On	on	k3xPp3gMnSc1	on
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Some	Some	k1gFnSc1	Some
Kinda	Kinda	k1gFnSc1	Kinda
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Pale	pal	k1gInSc5	pal
Blue	Blue	k1gNnPc6	Blue
Eyes	Eyes	k1gInSc4	Eyes
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
měla	mít	k5eAaImAgFnS	mít
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
repertoáru	repertoár	k1gInSc6	repertoár
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
text	text	k1gInSc1	text
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
Reedově	Reedův	k2eAgFnSc6d1	Reedova
bývalé	bývalý	k2eAgFnSc6d1	bývalá
přítelkyni	přítelkyně	k1gFnSc6	přítelkyně
z	z	k7c2	z
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Následují	následovat	k5eAaImIp3nP	následovat
"	"	kIx"	"
<g/>
Jesus	Jesus	k1gInSc1	Jesus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Beginning	Beginning	k1gInSc1	Beginning
to	ten	k3xDgNnSc4	ten
See	See	k1gFnSc1	See
the	the	k?	the
Light	Light	k1gInSc1	Light
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Set	set	k1gInSc1	set
Free	Free	k1gInSc1	Free
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
That	That	k1gInSc1	That
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
the	the	k?	the
Story	story	k1gFnSc1	story
of	of	k?	of
My	my	k3xPp1nPc1	my
Life	Life	k1gNnPc2	Life
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Murder	Murder	k1gInSc1	Murder
Mystery	Myster	k1gInPc1	Myster
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Poslední	poslední	k2eAgFnSc1d1	poslední
jmenovaná	jmenovaná	k1gFnSc1	jmenovaná
se	se	k3xPyFc4	se
nese	nést	k5eAaImIp3nS	nést
spíše	spíše	k9	spíše
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
předchozího	předchozí	k2eAgNnSc2d1	předchozí
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
After	After	k1gInSc1	After
Hours	Hours	k1gInSc1	Hours
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
zpívá	zpívat	k5eAaImIp3nS	zpívat
Tucker	Tucker	k1gInSc1	Tucker
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
většiny	většina	k1gFnSc2	většina
skladeb	skladba	k1gFnPc2	skladba
na	na	k7c6	na
albu	album	k1gNnSc6	album
lze	lze	k6eAd1	lze
přirovnat	přirovnat	k5eAaPmF	přirovnat
k	k	k7c3	k
folk	folk	k1gInSc4	folk
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
197	[number]	k4	197
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc6	pozice
v	v	k7c6	v
žebříčku	žebříčko	k1gNnSc6	žebříčko
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
,	,	kIx,	,
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
500	[number]	k4	500
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
alb	alba	k1gFnPc2	alba
Rolling	Rolling	k1gInSc1	Rolling
Stonu	ston	k1gInSc2	ston
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
314	[number]	k4	314
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
měsíci	měsíc	k1gInSc6	měsíc
jako	jako	k8xC	jako
album	album	k1gNnSc4	album
vyšel	vyjít	k5eAaPmAgInS	vyjít
i	i	k9	i
jediný	jediný	k2eAgInSc1d1	jediný
singl	singl	k1gInSc1	singl
obsahující	obsahující	k2eAgFnSc2d1	obsahující
skladby	skladba	k1gFnSc2	skladba
"	"	kIx"	"
<g/>
What	What	k2eAgInSc4d1	What
Goes	Goes	k1gInSc4	Goes
On	on	k3xPp3gMnSc1	on
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Jesus	Jesus	k1gInSc1	Jesus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
března	březen	k1gInSc2	březen
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc1	underground
opět	opět	k6eAd1	opět
koncertovali	koncertovat	k5eAaImAgMnP	koncertovat
–	–	k?	–
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
<g/>
,	,	kIx,	,
Clevelandu	Cleveland	k1gInSc6	Cleveland
<g/>
,	,	kIx,	,
Detroitu	Detroit	k1gInSc6	Detroit
a	a	k8xC	a
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
květnem	květen	k1gInSc7	květen
a	a	k8xC	a
srpnem	srpen	k1gInSc7	srpen
1969	[number]	k4	1969
nahrávali	nahrávat	k5eAaImAgMnP	nahrávat
v	v	k7c6	v
newyorském	newyorský	k2eAgNnSc6d1	newyorské
studiu	studio	k1gNnSc6	studio
Record	Recordo	k1gNnPc2	Recordo
Plant	planta	k1gFnPc2	planta
<g/>
;	;	kIx,	;
zvuku	zvuk	k1gInSc2	zvuk
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
Gary	Gary	k1gInPc4	Gary
Kellgren	Kellgrna	k1gFnPc2	Kellgrna
a	a	k8xC	a
nahrávání	nahrávání	k1gNnSc2	nahrávání
začalo	začít	k5eAaPmAgNnS	začít
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
skladbami	skladba	k1gFnPc7	skladba
"	"	kIx"	"
<g/>
Foggy	Fogg	k1gInPc4	Fogg
Notion	Notion	k1gInSc1	Notion
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Coney	Conea	k1gFnSc2	Conea
Island	Island	k1gInSc1	Island
Steepplechase	Steepplechasa	k1gFnSc3	Steepplechasa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
pak	pak	k6eAd1	pak
odehráli	odehrát	k5eAaPmAgMnP	odehrát
jeden	jeden	k4xCgInSc4	jeden
koncert	koncert	k1gInSc4	koncert
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
nahrát	nahrát	k5eAaPmF	nahrát
"	"	kIx"	"
<g/>
Andy	Anda	k1gFnPc1	Anda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Chest	Chest	k1gInSc1	Chest
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Sticking	Sticking	k1gInSc1	Sticking
with	with	k1gInSc1	with
You	You	k1gFnSc1	You
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
"	"	kIx"	"
<g/>
She	She	k1gFnSc1	She
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
My	my	k3xPp1nPc1	my
Best	Best	k1gMnSc1	Best
Friend	Friend	k1gInSc1	Friend
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalším	další	k2eAgNnSc6d1	další
přerušení	přerušení	k1gNnSc6	přerušení
<g/>
,	,	kIx,	,
vyplněném	vyplněný	k2eAgInSc6d1	vyplněný
dvěma	dva	k4xCgInPc7	dva
koncerty	koncert	k1gInPc7	koncert
v	v	k7c4	v
South	South	k1gInSc4	South
Deerfieldu	Deerfield	k1gInSc2	Deerfield
v	v	k7c6	v
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
<g/>
,	,	kIx,	,
nahráli	nahrát	k5eAaBmAgMnP	nahrát
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
I	i	k9	i
Can	Can	k1gFnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Stand	Standa	k1gFnPc2	Standa
It	It	k1gFnSc1	It
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
dva	dva	k4xCgInPc1	dva
květnové	květnový	k2eAgInPc1d1	květnový
dny	den	k1gInPc1	den
skupina	skupina	k1gFnSc1	skupina
opět	opět	k6eAd1	opět
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
oblíbeném	oblíbený	k2eAgInSc6d1	oblíbený
Bostonu	Boston	k1gInSc6	Boston
a	a	k8xC	a
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
června	červen	k1gInSc2	červen
znovu	znovu	k6eAd1	znovu
v	v	k7c4	v
South	South	k1gInSc4	South
Deerfieldu	Deerfield	k1gInSc2	Deerfield
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
nahráli	nahrát	k5eAaPmAgMnP	nahrát
skladby	skladba	k1gFnPc4	skladba
"	"	kIx"	"
<g/>
Ocean	Ocean	k1gMnSc1	Ocean
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Rock	rock	k1gInSc1	rock
&	&	k?	&
Roll	Roll	k1gInSc1	Roll
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Ferryboat	Ferryboat	k2eAgMnSc1d1	Ferryboat
Bill	Bill	k1gMnSc1	Bill
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
skupina	skupina	k1gFnSc1	skupina
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
kanadském	kanadský	k2eAgNnSc6d1	kanadské
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
,	,	kIx,	,
o	o	k7c6	o
dalším	další	k2eAgInSc6d1	další
víkendu	víkend	k1gInSc6	víkend
pak	pak	k6eAd1	pak
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
hráli	hrát	k5eAaImAgMnP	hrát
v	v	k7c6	v
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
a	a	k8xC	a
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
září	září	k1gNnSc2	září
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
Record	Recorda	k1gFnPc2	Recorda
Plant	planta	k1gFnPc2	planta
nahrát	nahrát	k5eAaBmF	nahrát
nejprve	nejprve	k6eAd1	nejprve
"	"	kIx"	"
<g/>
Ride	Ride	k1gNnSc7	Ride
Into	Into	k1gMnSc1	Into
the	the	k?	the
Sun	Sun	kA	Sun
<g/>
"	"	kIx"	"
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
"	"	kIx"	"
<g/>
One	One	k1gFnPc1	One
of	of	k?	of
These	these	k1gFnSc1	these
Day	Day	k1gFnSc1	Day
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Gonna	Gonn	k1gMnSc2	Gonn
Move	Mov	k1gMnSc2	Mov
Right	Right	k1gMnSc1	Right
In	In	k1gMnSc1	In
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
We	We	k1gFnSc1	We
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
Gonna	Gonen	k2eAgFnSc1d1	Gonna
Have	Have	k1gFnSc1	Have
a	a	k8xC	a
Real	Real	k1gInSc1	Real
Good	Gooda	k1gFnPc2	Gooda
Time	Tim	k1gInSc2	Tim
Together	Togethra	k1gFnPc2	Togethra
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Lisa	Lisa	k1gFnSc1	Lisa
Says	Saysa	k1gFnPc2	Saysa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
zde	zde	k6eAd1	zde
skladeb	skladba	k1gFnPc2	skladba
nahraných	nahraný	k2eAgFnPc2d1	nahraná
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
v	v	k7c4	v
Record	Record	k1gInSc4	Record
Plant	planta	k1gFnPc2	planta
vyšly	vyjít	k5eAaPmAgInP	vyjít
později	pozdě	k6eAd2	pozdě
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
alb	alba	k1gFnPc2	alba
VU	VU	kA	VU
a	a	k8xC	a
Another	Anothra	k1gFnPc2	Anothra
View	View	k1gFnSc2	View
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
byly	být	k5eAaImAgInP	být
nahrány	nahrát	k5eAaBmNgInP	nahrát
znovu	znovu	k6eAd1	znovu
pro	pro	k7c4	pro
album	album	k1gNnSc4	album
Loaded	Loaded	k1gInSc1	Loaded
a	a	k8xC	a
některé	některý	k3yIgNnSc1	některý
nahrál	nahrát	k5eAaPmAgInS	nahrát
znovu	znovu	k6eAd1	znovu
Reed	Reed	k1gInSc1	Reed
na	na	k7c4	na
svá	svůj	k3xOyFgNnPc4	svůj
raná	raný	k2eAgNnPc4d1	rané
sólová	sólový	k2eAgNnPc4d1	sólové
alba	album	k1gNnPc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
října	říjen	k1gInSc2	říjen
skupina	skupina	k1gFnSc1	skupina
vyjela	vyjet	k5eAaPmAgFnS	vyjet
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
města	město	k1gNnPc4	město
South	South	k1gMnSc1	South
Deerfield	Deerfield	k1gMnSc1	Deerfield
<g/>
,	,	kIx,	,
Filadelfie	Filadelfie	k1gFnSc1	Filadelfie
<g/>
,	,	kIx,	,
Minneapolis	Minneapolis	k1gFnSc1	Minneapolis
<g/>
,	,	kIx,	,
Dallas	Dallas	k1gInSc1	Dallas
a	a	k8xC	a
Austin	Austin	k1gInSc1	Austin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
rovněž	rovněž	k9	rovněž
nahrál	nahrát	k5eAaBmAgMnS	nahrát
John	John	k1gMnSc1	John
Cale	Cal	k1gInSc2	Cal
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
není	být	k5eNaImIp3nS	být
již	již	k6eAd1	již
rok	rok	k1gInSc4	rok
členem	člen	k1gInSc7	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
<g/>
;	;	kIx,	;
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Vintage	Vintag	k1gMnSc2	Vintag
Violence	Violenec	k1gMnSc2	Violenec
<g/>
.	.	kIx.	.
</s>
<s>
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc1	underground
v	v	k7c6	v
turné	turné	k1gNnSc6	turné
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
i	i	k9	i
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hráli	hrát	k5eAaImAgMnP	hrát
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
a	a	k8xC	a
San	San	k1gFnSc2	San
Franciscu	Franciscus	k1gInSc2	Franciscus
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
turné	turné	k1gNnSc6	turné
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
několika	několik	k4yIc6	několik
koncertech	koncert	k1gInPc6	koncert
nahráno	nahrát	k5eAaPmNgNnS	nahrát
album	album	k1gNnSc4	album
1969	[number]	k4	1969
<g/>
:	:	kIx,	:
The	The	k1gFnSc6	The
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc1	underground
Live	Liv	k1gInSc2	Liv
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
až	až	k9	až
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
koncerty	koncert	k1gInPc4	koncert
odehráli	odehrát	k5eAaPmAgMnP	odehrát
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
i	i	k8xC	i
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
třech	tři	k4xCgInPc6	tři
měsících	měsíc	k1gInPc6	měsíc
roku	rok	k1gInSc2	rok
následujícího	následující	k2eAgInSc2d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1970	[number]	k4	1970
vydal	vydat	k5eAaPmAgInS	vydat
Cale	Cale	k1gInSc1	Cale
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
Vintage	Vintag	k1gFnSc2	Vintag
Violence	Violence	k1gFnSc2	Violence
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
byl	být	k5eAaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
MGM	MGM	kA	MGM
Records	Records	k1gInSc1	Records
Mike	Mike	k1gInSc1	Mike
Curb	Curb	k1gInSc4	Curb
byl	být	k5eAaImAgInS	být
donucen	donucen	k2eAgMnSc1d1	donucen
zrušit	zrušit	k5eAaPmF	zrušit
smlouvy	smlouva	k1gFnPc4	smlouva
s	s	k7c7	s
několika	několik	k4yIc7	několik
interprety	interpret	k1gMnPc7	interpret
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jimiž	jenž	k3xRgInPc7	jenž
byli	být	k5eAaImAgMnP	být
i	i	k9	i
The	The	k1gMnPc1	The
Mothers	Mothersa	k1gFnPc2	Mothersa
of	of	k?	of
Invention	Invention	k1gInSc1	Invention
Franka	Frank	k1gMnSc2	Frank
Zappy	Zappa	k1gFnSc2	Zappa
<g/>
,	,	kIx,	,
Eric	Eric	k1gFnSc1	Eric
Burdon	Burdon	k1gMnSc1	Burdon
and	and	k?	and
the	the	k?	the
Animals	Animals	k1gInSc1	Animals
nebo	nebo	k8xC	nebo
právě	právě	k9	právě
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
tedy	tedy	k9	tedy
podepsala	podepsat	k5eAaPmAgFnS	podepsat
novou	nový	k2eAgFnSc4d1	nová
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Atlantic	Atlantice	k1gFnPc2	Atlantice
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
dubna	duben	k1gInSc2	duben
skupina	skupina	k1gFnSc1	skupina
šla	jít	k5eAaImAgFnS	jít
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
Atlantic	Atlantice	k1gFnPc2	Atlantice
Recording	Recording	k1gInSc1	Recording
Studios	Studios	k?	Studios
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
New	New	k1gFnSc6	New
Yorku	York	k1gInSc6	York
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
do	do	k7c2	do
role	role	k1gFnSc2	role
zvukaře	zvukař	k1gMnSc2	zvukař
byl	být	k5eAaImAgMnS	být
tentokrát	tentokrát	k6eAd1	tentokrát
nasazen	nasazen	k2eAgMnSc1d1	nasazen
Adrian	Adrian	k1gMnSc1	Adrian
Barber	Barber	k1gMnSc1	Barber
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávání	nahrávání	k1gNnSc2	nahrávání
se	se	k3xPyFc4	se
neúčastnila	účastnit	k5eNaImAgFnS	účastnit
Tucker	Tucker	k1gInSc4	Tucker
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
čekala	čekat	k5eAaImAgFnS	čekat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
a	a	k8xC	a
bicí	bicí	k2eAgFnSc4d1	bicí
soupravu	souprava	k1gFnSc4	souprava
proto	proto	k8xC	proto
obsluhovalo	obsluhovat	k5eAaImAgNnS	obsluhovat
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
Doug	Doug	k1gInSc4	Doug
Yule	Yul	k1gInSc2	Yul
<g/>
,	,	kIx,	,
zvukový	zvukový	k2eAgMnSc1d1	zvukový
inženýr	inženýr	k1gMnSc1	inženýr
Adrian	Adrian	k1gMnSc1	Adrian
Barber	Barber	k1gMnSc1	Barber
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
session	session	k1gInSc1	session
musician	musician	k1gInSc1	musician
<g/>
"	"	kIx"	"
Tommy	Tomma	k1gFnPc1	Tomma
Castanaro	Castanara	k1gFnSc5	Castanara
a	a	k8xC	a
Yuleův	Yuleův	k2eAgMnSc1d1	Yuleův
bratr	bratr	k1gMnSc1	bratr
Billy	Bill	k1gMnPc7	Bill
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ještě	ještě	k6eAd1	ještě
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Castanaro	Castanara	k1gFnSc5	Castanara
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
zadním	zadní	k2eAgInSc6d1	zadní
obalu	obal	k1gInSc6	obal
alba	album	k1gNnSc2	album
následně	následně	k6eAd1	následně
uveden	uvést	k5eAaPmNgMnS	uvést
jen	jen	k9	jen
jako	jako	k9	jako
Tommy	Tomm	k1gInPc1	Tomm
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
nikdo	nikdo	k3yNnSc1	nikdo
nemohl	moct	k5eNaImAgMnS	moct
vzpomenout	vzpomenout	k5eAaPmF	vzpomenout
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
příjmení	příjmení	k1gNnSc4	příjmení
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Tucker	Tucker	k1gInSc1	Tucker
na	na	k7c6	na
albu	album	k1gNnSc6	album
vůbec	vůbec	k9	vůbec
nehrála	hrát	k5eNaImAgFnS	hrát
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
jeho	jeho	k3xOp3gInSc2	jeho
obalu	obal	k1gInSc2	obal
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávání	nahrávání	k1gNnSc1	nahrávání
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
až	až	k6eAd1	až
do	do	k7c2	do
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
však	však	k9	však
rolí	role	k1gFnSc7	role
producentů	producent	k1gMnPc2	producent
ujali	ujmout	k5eAaPmAgMnP	ujmout
Geoff	Geoff	k1gMnSc1	Geoff
Haslam	Haslam	k1gInSc4	Haslam
<g/>
,	,	kIx,	,
Shel	Shel	k1gInSc4	Shel
Kagan	Kagana	k1gFnPc2	Kagana
a	a	k8xC	a
samotní	samotnět	k5eAaImIp3nS	samotnět
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
rovněž	rovněž	k9	rovněž
několikrát	několikrát	k6eAd1	několikrát
odjeli	odjet	k5eAaPmAgMnP	odjet
na	na	k7c4	na
koncerty	koncert	k1gInPc4	koncert
a	a	k8xC	a
zahráli	zahrát	k5eAaPmAgMnP	zahrát
například	například	k6eAd1	například
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
a	a	k8xC	a
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
nekoncertování	nekoncertování	k1gNnSc2	nekoncertování
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
New	New	k1gFnSc6	New
Yorku	York	k1gInSc6	York
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
zahájila	zahájit	k5eAaPmAgFnS	zahájit
angažmá	angažmá	k1gNnSc4	angažmá
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Max	max	kA	max
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Kansas	Kansas	k1gInSc1	Kansas
City	city	k1gNnSc1	city
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1970	[number]	k4	1970
vydalo	vydat	k5eAaPmAgNnS	vydat
MGM	MGM	kA	MGM
kompilaci	kompilace	k1gFnSc4	kompilace
s	s	k7c7	s
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
názvem	název	k1gInSc7	název
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1970	[number]	k4	1970
rovněž	rovněž	k9	rovněž
v	v	k7c4	v
Max	max	kA	max
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
záznam	záznam	k1gInSc1	záznam
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
koncertu	koncert	k1gInSc2	koncert
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
vyšel	vyjít	k5eAaPmAgInS	vyjít
jako	jako	k9	jako
Live	Live	k1gInSc1	Live
at	at	k?	at
Max	max	kA	max
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Kansas	Kansas	k1gInSc1	Kansas
City	city	k1gNnSc1	city
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
koncert	koncert	k1gInSc4	koncert
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
přišla	přijít	k5eAaPmAgFnS	přijít
podívat	podívat	k5eAaImF	podívat
Tucker	Tucker	k1gInSc4	Tucker
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
zaskakoval	zaskakovat	k5eAaImAgMnS	zaskakovat
Billy	Bill	k1gMnPc4	Bill
Yule	Yule	k1gFnPc2	Yule
<g/>
;	;	kIx,	;
na	na	k7c6	na
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
jí	on	k3xPp3gFnSc3	on
Reed	Reed	k1gInSc1	Reed
řekl	říct	k5eAaPmAgInS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
měsíc	měsíc	k1gInSc4	měsíc
po	po	k7c6	po
Reedově	Reedův	k2eAgInSc6d1	Reedův
odchodu	odchod	k1gInSc6	odchod
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
poslední	poslední	k2eAgNnSc4d1	poslední
album	album	k1gNnSc4	album
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc4	underground
s	s	k7c7	s
názvem	název	k1gInSc7	název
Loaded	Loaded	k1gMnSc1	Loaded
<g/>
.	.	kIx.	.
</s>
<s>
Vydala	vydat	k5eAaPmAgFnS	vydat
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
Cotillion	Cotillion	k1gInSc1	Cotillion
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
,	,	kIx,	,
dceřiná	dceřiný	k2eAgFnSc1d1	dceřiná
společnost	společnost	k1gFnSc1	společnost
Atlantic	Atlantice	k1gFnPc2	Atlantice
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obalu	obal	k1gInSc6	obal
alba	album	k1gNnSc2	album
je	být	k5eAaImIp3nS	být
znázorněn	znázorněn	k2eAgInSc4d1	znázorněn
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
metra	metro	k1gNnSc2	metro
a	a	k8xC	a
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
růžový	růžový	k2eAgInSc4d1	růžový
kouř	kouř	k1gInSc4	kouř
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
ho	on	k3xPp3gMnSc4	on
Stanislaw	Stanislaw	k1gMnSc4	Stanislaw
Zagorski	Zagorsk	k1gFnSc2	Zagorsk
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
obalu	obal	k1gInSc6	obal
se	se	k3xPyFc4	se
nepodílel	podílet	k5eNaImAgMnS	podílet
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
Factory	Factor	k1gInPc5	Factor
<g/>
.	.	kIx.	.
</s>
<s>
Manažer	manažer	k1gMnSc1	manažer
skupiny	skupina	k1gFnSc2	skupina
Steve	Steve	k1gMnSc1	Steve
Sesnick	Sesnick	k1gMnSc1	Sesnick
na	na	k7c4	na
nahrávání	nahrávání	k1gNnSc4	nahrávání
pozval	pozvat	k5eAaPmAgMnS	pozvat
i	i	k9	i
původního	původní	k2eAgMnSc2d1	původní
člena	člen	k1gMnSc2	člen
Johna	John	k1gMnSc2	John
Calea	Caleus	k1gMnSc2	Caleus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
demo	demo	k2eAgFnSc6d1	demo
verzi	verze	k1gFnSc6	verze
skladby	skladba	k1gFnSc2	skladba
"	"	kIx"	"
<g/>
Ocean	Ocean	k1gInSc1	Ocean
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vyšla	vyjít	k5eAaPmAgFnS	vyjít
až	až	k9	až
na	na	k7c4	na
Fully	Full	k1gInPc4	Full
Loaded	Loaded	k1gMnSc1	Loaded
Edition	Edition	k1gInSc4	Edition
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
500	[number]	k4	500
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
alb	alba	k1gFnPc2	alba
časopisu	časopis	k1gInSc2	časopis
podle	podle	k7c2	podle
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
se	se	k3xPyFc4	se
Loaded	Loaded	k1gInSc1	Loaded
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
109	[number]	k4	109
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
všech	všecek	k3xTgFnPc2	všecek
písní	píseň	k1gFnPc2	píseň
na	na	k7c6	na
albu	album	k1gNnSc6	album
je	být	k5eAaImIp3nS	být
Reed	Reed	k1gInSc1	Reed
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
poslední	poslední	k2eAgFnSc1d1	poslední
práce	práce	k1gFnSc1	práce
na	na	k7c6	na
albu	album	k1gNnSc6	album
dělaly	dělat	k5eAaImAgFnP	dělat
až	až	k9	až
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
odchodu	odchod	k1gInSc6	odchod
<g/>
,	,	kIx,	,
Sesnick	Sesnicko	k1gNnPc2	Sesnicko
jako	jako	k8xC	jako
autory	autor	k1gMnPc7	autor
skladeb	skladba	k1gFnPc2	skladba
nechal	nechat	k5eAaPmAgInS	nechat
na	na	k7c4	na
obal	obal	k1gInSc4	obal
uvést	uvést	k5eAaPmF	uvést
celou	celý	k2eAgFnSc4d1	celá
skupinu	skupina	k1gFnSc4	skupina
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
<s>
Reed	Reed	k1gMnSc1	Reed
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
Sesnicka	Sesnicka	k1gFnSc1	Sesnicka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
zpět	zpět	k6eAd1	zpět
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Sesnick	Sesnick	k1gInSc1	Sesnick
získal	získat	k5eAaPmAgInS	získat
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
název	název	k1gInSc4	název
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc1	underground
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
Yule-Morrison-Tucker	Yule-Morrison-Tucker	k1gMnSc1	Yule-Morrison-Tucker
a	a	k8xC	a
nový	nový	k2eAgMnSc1d1	nový
baskytarista	baskytarista	k1gMnSc1	baskytarista
Walter	Walter	k1gMnSc1	Walter
Powers	Powersa	k1gFnPc2	Powersa
se	se	k3xPyFc4	se
sešli	sejít	k5eAaPmAgMnP	sejít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1970	[number]	k4	1970
opět	opět	k6eAd1	opět
v	v	k7c6	v
Atlantic	Atlantice	k1gFnPc2	Atlantice
Recording	Recording	k1gInSc4	Recording
Studios	Studios	k?	Studios
a	a	k8xC	a
nahráli	nahrát	k5eAaPmAgMnP	nahrát
několik	několik	k4yIc4	několik
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
změněné	změněný	k2eAgFnSc6d1	změněná
sestavě	sestava	k1gFnSc6	sestava
nahráli	nahrát	k5eAaPmAgMnP	nahrát
znovu	znovu	k6eAd1	znovu
na	na	k7c4	na
album	album	k1gNnSc4	album
Squeeze	Squeeze	k1gFnSc2	Squeeze
<g/>
.	.	kIx.	.
</s>
<s>
Aktivity	aktivita	k1gFnPc1	aktivita
skupiny	skupina	k1gFnSc2	skupina
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
míry	míra	k1gFnSc2	míra
opadly	opadnout	k5eAaPmAgFnP	opadnout
a	a	k8xC	a
změnil	změnit	k5eAaPmAgMnS	změnit
se	se	k3xPyFc4	se
i	i	k9	i
její	její	k3xOp3gInSc1	její
styl	styl	k1gInSc1	styl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
1971	[number]	k4	1971
prakticky	prakticky	k6eAd1	prakticky
jen	jen	k6eAd1	jen
koncertovala	koncertovat	k5eAaImAgFnS	koncertovat
a	a	k8xC	a
nové	nový	k2eAgFnSc2d1	nová
skladby	skladba	k1gFnSc2	skladba
nenahrávala	nahrávat	k5eNaImAgFnS	nahrávat
<g/>
.	.	kIx.	.
</s>
<s>
Morrison	Morrison	k1gInSc1	Morrison
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
B.	B.	kA	B.
A.	A.	kA	A.
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
skupinu	skupina	k1gFnSc4	skupina
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
studovat	studovat	k5eAaImF	studovat
středověkou	středověký	k2eAgFnSc4d1	středověká
literaturu	literatura	k1gFnSc4	literatura
(	(	kIx(	(
<g/>
se	s	k7c7	s
získáním	získání	k1gNnSc7	získání
titulu	titul	k1gInSc2	titul
PhD	PhD	k1gFnSc2	PhD
<g/>
)	)	kIx)	)
na	na	k7c6	na
Texaské	texaský	k2eAgFnSc6d1	texaská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Austinu	Austin	k1gInSc6	Austin
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
klávesista	klávesista	k1gMnSc1	klávesista
Willie	Willie	k1gFnSc2	Willie
Alexander	Alexandra	k1gFnPc2	Alexandra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1971	[number]	k4	1971
skupina	skupina	k1gFnSc1	skupina
odletěla	odletět	k5eAaPmAgFnS	odletět
do	do	k7c2	do
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
odehrála	odehrát	k5eAaPmAgFnS	odehrát
své	svůj	k3xOyFgNnSc4	svůj
vůbec	vůbec	k9	vůbec
první	první	k4xOgInPc4	první
koncerty	koncert	k1gInPc4	koncert
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
koncertů	koncert	k1gInPc2	koncert
rovněž	rovněž	k9	rovněž
odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Záznamy	záznam	k1gInPc1	záznam
některých	některý	k3yIgInPc2	některý
koncertů	koncert	k1gInPc2	koncert
vyšly	vyjít	k5eAaPmAgFnP	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
v	v	k7c4	v
box	box	k1gInSc4	box
setu	set	k1gInSc2	set
Final	Final	k1gInSc1	Final
V.	V.	kA	V.
U.	U.	kA	U.
Koncem	koncem	k7c2	koncem
ledna	leden	k1gInSc2	leden
1972	[number]	k4	1972
Cale	Cale	k1gFnPc2	Cale
<g/>
,	,	kIx,	,
Reed	Reeda	k1gFnPc2	Reeda
a	a	k8xC	a
Nico	Nico	k6eAd1	Nico
odehráli	odehrát	k5eAaPmAgMnP	odehrát
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
společný	společný	k2eAgInSc4d1	společný
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Bataclan	Bataclana	k1gFnPc2	Bataclana
<g/>
;	;	kIx,	;
záznam	záznam	k1gInSc1	záznam
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Le	Le	k1gMnSc1	Le
Bataclan	Bataclan	k1gMnSc1	Bataclan
'	'	kIx"	'
<g/>
72	[number]	k4	72
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1972	[number]	k4	1972
vydal	vydat	k5eAaPmAgMnS	vydat
Reed	Reed	k1gMnSc1	Reed
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Lou	Lou	k1gMnSc1	Lou
Reed	Reed	k1gMnSc1	Reed
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
skladeb	skladba	k1gFnPc2	skladba
na	na	k7c6	na
albu	album	k1gNnSc6	album
tvoří	tvořit	k5eAaImIp3nP	tvořit
nevydané	vydaný	k2eNgFnPc1d1	nevydaná
skladby	skladba	k1gFnPc1	skladba
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
skupina	skupina	k1gFnSc1	skupina
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc4	underground
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Atlantic	Atlantice	k1gFnPc2	Atlantice
Records	Records	k1gInSc4	Records
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
bubenici	bubenice	k1gFnSc4	bubenice
<g/>
.	.	kIx.	.
</s>
<s>
Trosky	troska	k1gFnPc1	troska
skupiny	skupina	k1gFnSc2	skupina
tedy	tedy	k9	tedy
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Polydor	Polydor	k1gInSc1	Polydor
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
Tucker	Tuckra	k1gFnPc2	Tuckra
odletěla	odletět	k5eAaPmAgFnS	odletět
domů	domů	k6eAd1	domů
a	a	k8xC	a
Sesnick	Sesnick	k1gInSc4	Sesnick
Yuleho	Yule	k1gMnSc2	Yule
požádal	požádat	k5eAaPmAgMnS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
zůstal	zůstat	k5eAaPmAgMnS	zůstat
a	a	k8xC	a
nahrál	nahrát	k5eAaPmAgMnS	nahrát
zde	zde	k6eAd1	zde
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Squeeze	Squeeze	k1gFnSc2	Squeeze
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
bicí	bicí	k2eAgNnSc4d1	bicí
při	při	k7c6	při
nahrávání	nahrávání	k1gNnSc6	nahrávání
usedl	usednout	k5eAaPmAgMnS	usednout
Ian	Ian	k1gMnSc1	Ian
Paice	Paice	k1gMnSc1	Paice
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
<g/>
.	.	kIx.	.
</s>
<s>
Yule	Yule	k6eAd1	Yule
rovněž	rovněž	k9	rovněž
sehnal	sehnat	k5eAaPmAgMnS	sehnat
další	další	k2eAgMnPc4d1	další
hudebníky	hudebník	k1gMnPc4	hudebník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
odehráli	odehrát	k5eAaPmAgMnP	odehrát
turné	turné	k1gNnSc6	turné
<g/>
:	:	kIx,	:
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
Ridgewood	Ridgewood	k1gInSc1	Ridgewood
Rockets	Rockets	k1gInSc1	Rockets
–	–	k?	–
baskytarista	baskytarista	k1gMnSc1	baskytarista
George	Georg	k1gMnSc2	Georg
Kay	Kay	k1gMnSc2	Kay
<g/>
,	,	kIx,	,
Billy	Bill	k1gMnPc7	Bill
Yule	Yule	k1gNnSc2	Yule
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
Rob	roba	k1gFnPc2	roba
Norris	Norris	k1gFnPc2	Norris
<g/>
.	.	kIx.	.
</s>
<s>
Billyho	Billyze	k6eAd1	Billyze
Yulea	Yulea	k1gMnSc1	Yulea
však	však	k9	však
nahradil	nahradit	k5eAaPmAgMnS	nahradit
bubeník	bubeník	k1gMnSc1	bubeník
Mark	Mark	k1gMnSc1	Mark
Nauseef	Nauseef	k1gMnSc1	Nauseef
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1973	[number]	k4	1973
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
definitivně	definitivně	k6eAd1	definitivně
poslední	poslední	k2eAgNnSc4d1	poslední
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
Squeeze	Squeeze	k1gFnSc2	Squeeze
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
nástrojů	nástroj	k1gInPc2	nástroj
nahrál	nahrát	k5eAaPmAgInS	nahrát
sám	sám	k3xTgInSc4	sám
Doug	Doug	k1gInSc4	Doug
Yule	Yul	k1gFnSc2	Yul
<g/>
,	,	kIx,	,
na	na	k7c4	na
bicí	bicí	k2eAgFnPc4d1	bicí
hrál	hrát	k5eAaImAgInS	hrát
Ian	Ian	k1gMnSc1	Ian
Paice	Paice	k1gMnSc1	Paice
<g/>
,	,	kIx,	,
a	a	k8xC	a
kdo	kdo	k3yQnSc1	kdo
zpívá	zpívat	k5eAaImIp3nS	zpívat
doprovodné	doprovodný	k2eAgInPc4d1	doprovodný
vokály	vokál	k1gInPc4	vokál
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
nepodílel	podílet	k5eNaImAgMnS	podílet
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
sestavy	sestava	k1gFnSc2	sestava
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jako	jako	k9	jako
album	album	k1gNnSc1	album
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc2	The
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc4	underground
přestala	přestat	k5eAaPmAgFnS	přestat
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Cale	Cal	k1gInSc2	Cal
<g/>
,	,	kIx,	,
Lou	Lou	k1gMnSc1	Lou
Reed	Reed	k1gMnSc1	Reed
i	i	k9	i
Nico	Nico	k6eAd1	Nico
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
ve	v	k7c6	v
vydáváním	vydávání	k1gNnSc7	vydávání
sólových	sólový	k2eAgNnPc2d1	sólové
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
počátkem	počátkem	k7c2	počátkem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
i	i	k8xC	i
Maureen	Maureen	k2eAgInSc4d1	Maureen
Tucker	Tucker	k1gInSc4	Tucker
<g/>
.	.	kIx.	.
</s>
<s>
Členem	člen	k1gInSc7	člen
její	její	k3xOp3gFnSc2	její
doprovodné	doprovodný	k2eAgFnSc2d1	doprovodná
skupiny	skupina	k1gFnSc2	skupina
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Morrison	Morrison	k1gMnSc1	Morrison
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jako	jako	k8xS	jako
jediný	jediný	k2eAgMnSc1d1	jediný
z	z	k7c2	z
klasické	klasický	k2eAgFnSc2d1	klasická
sestavy	sestava	k1gFnSc2	sestava
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc1	underground
nevydal	vydat	k5eNaPmAgInS	vydat
žádné	žádný	k3yNgNnSc4	žádný
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Doug	Doug	k1gMnSc1	Doug
Yule	Yul	k1gInSc2	Yul
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Reedem	Reed	k1gMnSc7	Reed
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
sólovém	sólový	k2eAgNnSc6d1	sólové
albu	album	k1gNnSc6	album
Sally	Salla	k1gMnSc2	Salla
Can	Can	k1gMnSc2	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Dance	Danka	k1gFnSc6	Danka
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
krátce	krátce	k6eAd1	krátce
existující	existující	k2eAgFnSc2d1	existující
superskupiny	superskupina	k1gFnSc2	superskupina
American	American	k1gMnSc1	American
Flyer	Flyer	k1gMnSc1	Flyer
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
rozpadu	rozpad	k1gInSc6	rozpad
se	se	k3xPyFc4	se
ztratil	ztratit	k5eAaPmAgMnS	ztratit
z	z	k7c2	z
hudebního	hudební	k2eAgInSc2d1	hudební
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
začal	začít	k5eAaPmAgInS	začít
hrát	hrát	k5eAaImF	hrát
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Angus	Angus	k1gMnSc1	Angus
MacLise	MacLise	k1gFnSc2	MacLise
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
v	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
Polydor	Polydor	k1gInSc1	Polydor
vydal	vydat	k5eAaPmAgInS	vydat
album	album	k1gNnSc4	album
VU	VU	kA	VU
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
raritních	raritní	k2eAgFnPc2d1	raritní
nahrávek	nahrávka	k1gFnPc2	nahrávka
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
přípravách	příprava	k1gFnPc6	příprava
na	na	k7c4	na
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
album	album	k1gNnSc4	album
pro	pro	k7c4	pro
MGM	MGM	kA	MGM
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
skladbách	skladba	k1gFnPc6	skladba
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
i	i	k9	i
Cale	Cale	k1gInSc1	Cale
<g/>
,	,	kIx,	,
další	další	k2eAgNnPc1d1	další
dema	demum	k1gNnPc1	demum
a	a	k8xC	a
nevydané	vydaný	k2eNgFnPc1d1	nevydaná
skladby	skladba	k1gFnPc1	skladba
vyšly	vyjít	k5eAaPmAgFnP	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
na	na	k7c6	na
albu	album	k1gNnSc6	album
Another	Anothra	k1gFnPc2	Anothra
View	View	k1gFnSc2	View
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1987	[number]	k4	1987
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
58	[number]	k4	58
let	léto	k1gNnPc2	léto
Andy	Anda	k1gFnSc2	Anda
Warhol	Warhol	k1gInSc1	Warhol
<g/>
.	.	kIx.	.
</s>
<s>
Nico	Nico	k6eAd1	Nico
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Warholově	Warholův	k2eAgFnSc6d1	Warholova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
myšlenka	myšlenka	k1gFnSc1	myšlenka
na	na	k7c6	na
obnovení	obnovení	k1gNnSc6	obnovení
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Lou	Lou	k?	Lou
Reed	Reed	k1gMnSc1	Reed
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Calem	Cal	k1gMnSc7	Cal
se	se	k3xPyFc4	se
na	na	k7c6	na
obědě	oběd	k1gInSc6	oběd
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
pohřbu	pohřeb	k1gInSc6	pohřeb
dali	dát	k5eAaPmAgMnP	dát
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
je	být	k5eAaImIp3nS	být
dal	dát	k5eAaPmAgMnS	dát
dohromady	dohromady	k6eAd1	dohromady
Billy	Bill	k1gMnPc4	Bill
Name	Nam	k1gInSc2	Nam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
vydali	vydat	k5eAaPmAgMnP	vydat
oba	dva	k4xCgMnPc1	dva
nezávisle	závisle	k6eNd1	závisle
na	na	k7c4	na
sobě	se	k3xPyFc3	se
svá	svůj	k3xOyFgNnPc4	svůj
alba	album	k1gNnPc4	album
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
společné	společný	k2eAgFnSc6d1	společná
desce	deska	k1gFnSc6	deska
věnované	věnovaný	k2eAgFnSc3d1	věnovaná
památce	památka	k1gFnSc3	památka
zesnulého	zesnulý	k1gMnSc2	zesnulý
Warhola	Warhola	k1gFnSc1	Warhola
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
jejich	jejich	k3xOp3gFnSc2	jejich
spolupráce	spolupráce	k1gFnSc2	spolupráce
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
Warholových	Warholový	k2eAgFnPc2d1	Warholový
přezdívek	přezdívka	k1gFnPc2	přezdívka
<g/>
,	,	kIx,	,
Songs	Songs	k1gInSc4	Songs
for	forum	k1gNnPc2	forum
Drella	Drello	k1gNnSc2	Drello
<g/>
,	,	kIx,	,
a	a	k8xC	a
nahráli	nahrát	k5eAaBmAgMnP	nahrát
ho	on	k3xPp3gInSc4	on
Reed	Reed	k1gInSc4	Reed
a	a	k8xC	a
Cale	Cale	k1gInSc4	Cale
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
kytary	kytara	k1gFnSc2	kytara
<g/>
,	,	kIx,	,
violy	viola	k1gFnSc2	viola
<g/>
,	,	kIx,	,
kláves	klávesa	k1gFnPc2	klávesa
a	a	k8xC	a
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
vydání	vydání	k1gNnSc6	vydání
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
myšlenky	myšlenka	k1gFnPc1	myšlenka
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
klasické	klasický	k2eAgFnSc6d1	klasická
sestavě	sestava	k1gFnSc6	sestava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1990	[number]	k4	1990
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
jeden	jeden	k4xCgInSc4	jeden
benefiční	benefiční	k2eAgInSc4d1	benefiční
koncert	koncert	k1gInSc4	koncert
pro	pro	k7c4	pro
nadaci	nadace	k1gFnSc4	nadace
Fondation	Fondation	k1gInSc1	Fondation
Cartier	Cartier	k1gMnSc1	Cartier
pour	pour	k1gMnSc1	pour
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
art	art	k?	art
contemporain	contemporain	k1gMnSc1	contemporain
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1991	[number]	k4	1991
vydala	vydat	k5eAaPmAgFnS	vydat
Tucker	Tucker	k1gInSc4	Tucker
své	svůj	k3xOyFgNnSc4	svůj
třetí	třetí	k4xOgNnSc4	třetí
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
I	i	k9	i
Spent	Spent	k1gMnSc1	Spent
a	a	k8xC	a
Week	Week	k1gMnSc1	Week
There	Ther	k1gInSc5	Ther
the	the	k?	the
Other	Other	k1gMnSc1	Other
Night	Night	k1gMnSc1	Night
<g/>
;	;	kIx,	;
mimo	mimo	k7c4	mimo
ni	on	k3xPp3gFnSc4	on
a	a	k8xC	a
jiné	jiný	k2eAgMnPc4d1	jiný
hudebníky	hudebník	k1gMnPc4	hudebník
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
hrají	hrát	k5eAaImIp3nP	hrát
i	i	k9	i
Cale	Cal	k1gFnPc1	Cal
<g/>
,	,	kIx,	,
Reed	Reed	k1gInSc1	Reed
a	a	k8xC	a
Morrison	Morrison	k1gInSc1	Morrison
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
Morrison	Morrisona	k1gFnPc2	Morrisona
a	a	k8xC	a
Reed	Reeda	k1gFnPc2	Reeda
zahráli	zahrát	k5eAaPmAgMnP	zahrát
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
Johna	John	k1gMnSc2	John
Calea	Caleus	k1gMnSc2	Caleus
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
první	první	k4xOgFnPc1	první
zkoušky	zkouška	k1gFnPc1	zkouška
nově	nově	k6eAd1	nově
obnovených	obnovený	k2eAgFnPc2d1	obnovená
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc1	underground
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
sestavě	sestava	k1gFnSc6	sestava
a	a	k8xC	a
následovalo	následovat	k5eAaImAgNnS	následovat
evropské	evropský	k2eAgNnSc4d1	Evropské
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
zastavili	zastavit	k5eAaPmAgMnP	zastavit
i	i	k9	i
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
některých	některý	k3yIgInPc6	některý
koncertech	koncert	k1gInPc6	koncert
skupina	skupina	k1gFnSc1	skupina
dělala	dělat	k5eAaImAgFnS	dělat
předkapelu	předkapela	k1gFnSc4	předkapela
skupině	skupina	k1gFnSc3	skupina
U2	U2	k1gFnPc2	U2
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
turné	turné	k1gNnSc6	turné
Zoo	zoo	k1gFnSc2	zoo
TV	TV	kA	TV
Tour	Tour	k1gInSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
koncertů	koncert	k1gInPc2	koncert
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
nahráno	nahrát	k5eAaBmNgNnS	nahrát
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
vydáno	vydat	k5eAaPmNgNnS	vydat
jako	jako	k8xC	jako
Live	Live	k1gInSc1	Live
MCMXCIII	MCMXCIII	kA	MCMXCIII
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
však	však	k9	však
ještě	ještě	k9	ještě
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
amerického	americký	k2eAgNnSc2d1	americké
turné	turné	k1gNnSc2	turné
zase	zase	k9	zase
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
:	:	kIx,	:
mezi	mezi	k7c7	mezi
Reedem	Reedo	k1gNnSc7	Reedo
a	a	k8xC	a
Calem	Calum	k1gNnSc7	Calum
se	se	k3xPyFc4	se
probudila	probudit	k5eAaPmAgFnS	probudit
stará	starý	k2eAgFnSc1d1	stará
řevnivost	řevnivost	k1gFnSc1	řevnivost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	on	k3xPp3gInPc4	on
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
již	již	k6eAd1	již
léta	léto	k1gNnPc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
vyšel	vyjít	k5eAaPmAgInS	vyjít
třídiskový	třídiskový	k2eAgInSc1d1	třídiskový
box	box	k1gInSc1	box
set	set	k1gInSc1	set
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
What	What	k2eAgInSc1d1	What
Goes	Goes	k1gInSc1	Goes
On	on	k3xPp3gInSc1	on
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množství	množství	k1gNnSc4	množství
již	již	k6eAd1	již
vydaných	vydaný	k2eAgFnPc2d1	vydaná
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
různé	různý	k2eAgInPc4d1	různý
mluvené	mluvený	k2eAgInPc4d1	mluvený
doplňky	doplněk	k1gInPc4	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1995	[number]	k4	1995
zemřel	zemřít	k5eAaPmAgInS	zemřít
Sterling	sterling	k1gInSc1	sterling
Morrison	Morrison	k1gInSc4	Morrison
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
možnost	možnost	k1gFnSc4	možnost
skupinu	skupina	k1gFnSc4	skupina
obnovit	obnovit	k5eAaPmF	obnovit
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
sestavě	sestava	k1gFnSc6	sestava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
vyšel	vyjít	k5eAaPmAgInS	vyjít
box	box	k1gInSc1	box
set	set	k1gInSc1	set
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Peel	Peel	k1gInSc1	Peel
Slowly	Slowla	k1gFnSc2	Slowla
and	and	k?	and
See	See	k1gFnSc2	See
<g/>
;	;	kIx,	;
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
původních	původní	k2eAgFnPc2d1	původní
demo	demo	k2eAgFnPc2d1	demo
nahrávek	nahrávka	k1gFnPc2	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
,	,	kIx,	,
úvodní	úvodní	k2eAgInSc1d1	úvodní
proslov	proslov	k1gInSc1	proslov
přednesla	přednést	k5eAaPmAgFnS	přednést
Patti	Patti	k1gNnSc4	Patti
Smith	Smith	k1gInSc1	Smith
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
zde	zde	k6eAd1	zde
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
Reed-Cale-Tucker	Reed-Cale-Tuckra	k1gFnPc2	Reed-Cale-Tuckra
představila	představit	k5eAaPmAgFnS	představit
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Last	Last	k2eAgInSc1d1	Last
Night	Night	k1gInSc1	Night
I	i	k8xC	i
Said	Said	k1gInSc1	Said
Goodbye	Goodbye	k1gNnSc2	Goodbye
to	ten	k3xDgNnSc4	ten
My	my	k3xPp1nPc1	my
Friend	Frienda	k1gFnPc2	Frienda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
věnovala	věnovat	k5eAaImAgFnS	věnovat
zesnulému	zesnulý	k2eAgMnSc3d1	zesnulý
Morrisonovi	Morrison	k1gMnSc3	Morrison
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
vyšla	vyjít	k5eAaPmAgFnS	vyjít
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
verze	verze	k1gFnSc1	verze
alba	album	k1gNnSc2	album
Loaded	Loaded	k1gInSc1	Loaded
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Fully	Fulla	k1gFnSc2	Fulla
Loaded	Loaded	k1gInSc1	Loaded
Edition	Edition	k1gInSc1	Edition
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
skladby	skladba	k1gFnPc4	skladba
z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
alba	album	k1gNnSc2	album
doplněné	doplněný	k2eAgInPc1d1	doplněný
odlišnými	odlišný	k2eAgFnPc7d1	odlišná
verzemi	verze	k1gFnPc7	verze
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2001	[number]	k4	2001
vyšel	vyjít	k5eAaPmAgInS	vyjít
čtyřdiskový	čtyřdiskový	k2eAgInSc1d1	čtyřdiskový
komplet	komplet	k2eAgInSc1d1	komplet
Final	Final	k1gInSc1	Final
V.	V.	kA	V.
U.	U.	kA	U.
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
<g/>
;	;	kIx,	;
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
koncertní	koncertní	k2eAgFnPc4d1	koncertní
nahrávky	nahrávka	k1gFnPc4	nahrávka
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
existence	existence	k1gFnSc2	existence
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2001	[number]	k4	2001
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
Bootleg	Bootleg	k1gInSc1	Bootleg
Series	Series	k1gMnSc1	Series
Volume	volum	k1gInSc5	volum
1	[number]	k4	1
<g/>
:	:	kIx,	:
The	The	k1gMnSc5	The
Quine	Quin	k1gMnSc5	Quin
Tapes	Tapesa	k1gFnPc2	Tapesa
s	s	k7c7	s
pirátskými	pirátský	k2eAgFnPc7d1	pirátská
nahrávkami	nahrávka	k1gFnPc7	nahrávka
z	z	k7c2	z
koncertů	koncert	k1gInPc2	koncert
skupiny	skupina	k1gFnSc2	skupina
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc1	underground
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
pořídil	pořídit	k5eAaPmAgMnS	pořídit
pozdější	pozdní	k2eAgMnSc1d2	pozdější
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
Lou	Lou	k1gMnSc1	Lou
Reeda	Reeda	k1gMnSc1	Reeda
Robert	Robert	k1gMnSc1	Robert
Quine	Quin	k1gInSc5	Quin
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
zemřel	zemřít	k5eAaPmAgInS	zemřít
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
pokračování	pokračování	k1gNnPc4	pokračování
alba	album	k1gNnSc2	album
již	již	k6eAd1	již
nevyšlo	vyjít	k5eNaPmAgNnS	vyjít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
vyšla	vyjít	k5eAaPmAgFnS	vyjít
deluxe	deluxe	k1gFnSc1	deluxe
edice	edice	k1gFnSc1	edice
debutového	debutový	k2eAgNnSc2d1	debutové
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
celé	celý	k2eAgNnSc4d1	celé
původní	původní	k2eAgNnSc4d1	původní
album	album	k1gNnSc4	album
doplněné	doplněný	k2eAgNnSc4d1	doplněné
o	o	k7c4	o
pět	pět	k4xCc4	pět
skladeb	skladba	k1gFnPc2	skladba
z	z	k7c2	z
alba	album	k1gNnSc2	album
Chelsea	Chelse	k1gInSc2	Chelse
Girl	girl	k1gFnSc2	girl
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
skladby	skladba	k1gFnPc1	skladba
z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
alba	album	k1gNnSc2	album
v	v	k7c6	v
mono	mono	k2eAgFnPc6d1	mono
verzích	verze	k1gFnPc6	verze
a	a	k8xC	a
čtyři	čtyři	k4xCgFnPc1	čtyři
skladby	skladba	k1gFnPc1	skladba
původně	původně	k6eAd1	původně
vydané	vydaný	k2eAgInPc4d1	vydaný
jako	jako	k9	jako
singly	singl	k1gInPc4	singl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2005	[number]	k4	2005
zemřel	zemřít	k5eAaPmAgMnS	zemřít
první	první	k4xOgMnSc1	první
manažer	manažer	k1gMnSc1	manažer
skupiny	skupina	k1gFnSc2	skupina
Al	ala	k1gFnPc2	ala
Aronowitz	Aronowitz	k1gMnSc1	Aronowitz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
skupiny	skupina	k1gFnSc2	skupina
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
Knihovny	knihovna	k1gFnSc2	knihovna
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2009	[number]	k4	2009
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
setkali	setkat	k5eAaPmAgMnP	setkat
Reed	Reed	k1gInSc4	Reed
<g/>
,	,	kIx,	,
Tucker	Tucker	k1gInSc4	Tucker
a	a	k8xC	a
Yule	Yule	k1gInSc4	Yule
a	a	k8xC	a
udělali	udělat	k5eAaPmAgMnP	udělat
rozhovor	rozhovor	k1gInSc4	rozhovor
s	s	k7c7	s
Davidem	David	k1gMnSc7	David
Frickee	Frickee	k1gFnPc2	Frickee
v	v	k7c6	v
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
Public	publicum	k1gNnPc2	publicum
Library	Librara	k1gFnSc2	Librara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2012	[number]	k4	2012
žijící	žijící	k2eAgMnPc1d1	žijící
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
zahájili	zahájit	k5eAaPmAgMnP	zahájit
právní	právní	k2eAgInPc4d1	právní
kroky	krok	k1gInPc4	krok
proti	proti	k7c3	proti
Andy	Anda	k1gFnSc2	Anda
Warhol	Warhol	k1gInSc1	Warhol
Foundation	Foundation	k1gInSc1	Foundation
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
použití	použití	k1gNnSc2	použití
banánu	banán	k1gInSc2	banán
z	z	k7c2	z
jejich	jejich	k3xOp3gNnSc2	jejich
debutového	debutový	k2eAgNnSc2d1	debutové
alba	album	k1gNnSc2	album
na	na	k7c6	na
reklamních	reklamní	k2eAgInPc6d1	reklamní
předmětech	předmět	k1gInPc6	předmět
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
iPody	iPody	k6eAd1	iPody
<g/>
.	.	kIx.	.
</s>
<s>
Autorská	autorský	k2eAgNnPc4d1	autorské
práva	právo	k1gNnPc4	právo
k	k	k7c3	k
banánu	banán	k1gInSc3	banán
nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgFnS	být
zaregistrována	zaregistrovat	k5eAaPmNgFnS	zaregistrovat
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
spor	spor	k1gInSc1	spor
v	v	k7c6	v
září	září	k1gNnSc6	září
2012	[number]	k4	2012
prohrála	prohrát	k5eAaPmAgFnS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
vyšla	vyjít	k5eAaPmAgFnS	vyjít
šestidisková	šestidiskový	k2eAgFnSc1d1	šestidisková
reedice	reedice	k1gFnSc1	reedice
alba	album	k1gNnSc2	album
The	The	k1gFnSc2	The
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
&	&	k?	&
Nico	Nico	k6eAd1	Nico
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
45	[number]	k4	45
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
jeho	jeho	k3xOp3gNnSc2	jeho
původního	původní	k2eAgNnSc2d1	původní
vydání	vydání	k1gNnSc2	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
stereo	stereo	k2eAgFnSc1d1	stereo
i	i	k8xC	i
mono	mono	k2eAgFnSc1d1	mono
verze	verze	k1gFnSc1	verze
původního	původní	k2eAgNnSc2d1	původní
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
celé	celý	k2eAgNnSc1d1	celé
album	album	k1gNnSc1	album
Chelsea	Chelse	k1gInSc2	Chelse
Girl	girl	k1gFnSc2	girl
<g/>
,	,	kIx,	,
zkoušky	zkouška	k1gFnPc4	zkouška
nahrané	nahraný	k2eAgFnPc4d1	nahraná
ve	v	k7c4	v
Factory	Factor	k1gMnPc4	Factor
i	i	k8xC	i
záznamy	záznam	k1gInPc4	záznam
z	z	k7c2	z
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
pak	pak	k9	pak
vyšla	vyjít	k5eAaPmAgFnS	vyjít
i	i	k9	i
třídisková	třídiskový	k2eAgFnSc1d1	třídisková
reedice	reedice	k1gFnSc1	reedice
alba	album	k1gNnSc2	album
White	Whit	k1gInSc5	Whit
Light	Light	k1gInSc4	Light
<g/>
/	/	kIx~	/
<g/>
White	Whit	k1gMnSc5	Whit
Heat	Heat	k2eAgMnSc1d1	Heat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Lou	Lou	k1gMnSc1	Lou
Reed	Reed	k1gMnSc1	Reed
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
jedenasedmdesáti	jedenasedmdesát	k4xCc6	jedenasedmdesát
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
a	a	k8xC	a
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
inspirací	inspirace	k1gFnPc2	inspirace
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
punk	punk	k1gMnSc1	punk
rocku	rock	k1gInSc2	rock
a	a	k8xC	a
alternativního	alternativní	k2eAgInSc2d1	alternativní
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
svůj	svůj	k3xOyFgInSc4	svůj
vzor	vzor	k1gInSc4	vzor
ji	on	k3xPp3gFnSc4	on
označili	označit	k5eAaPmAgMnP	označit
například	například	k6eAd1	například
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc2	Bowie
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gMnSc1	People
of	of	k?	of
the	the	k?	the
Universe	Universe	k1gFnSc2	Universe
<g/>
,	,	kIx,	,
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Stooges	Stooges	k1gMnSc1	Stooges
<g/>
,	,	kIx,	,
Brian	Brian	k1gMnSc1	Brian
Eno	Eno	k1gMnSc1	Eno
<g/>
,	,	kIx,	,
Siouxsie	Siouxsie	k1gFnSc1	Siouxsie
and	and	k?	and
the	the	k?	the
Banshees	Banshees	k1gInSc1	Banshees
<g/>
,	,	kIx,	,
Roxy	Roxa	k1gFnPc1	Roxa
Music	Music	k1gMnSc1	Music
<g/>
,	,	kIx,	,
Pixies	Pixies	k1gMnSc1	Pixies
<g/>
,	,	kIx,	,
Can	Can	k1gMnSc1	Can
<g/>
,	,	kIx,	,
Faust	Faust	k1gMnSc1	Faust
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Television	Television	k1gInSc1	Television
<g/>
,	,	kIx,	,
Glenn	Glenn	k1gMnSc1	Glenn
Danzig	Danzig	k1gMnSc1	Danzig
<g/>
,	,	kIx,	,
Patti	Patt	k1gMnPc1	Patt
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
Red	Red	k1gMnSc1	Red
Hot	hot	k0	hot
Chili	Chile	k1gFnSc3	Chile
Peppers	Peppers	k1gInSc1	Peppers
<g/>
,	,	kIx,	,
Sex	sex	k1gInSc1	sex
Pistols	Pistols	k1gInSc1	Pistols
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Dolls	Dolls	k1gInSc1	Dolls
<g/>
,	,	kIx,	,
Nirvana	Nirvan	k1gMnSc4	Nirvan
<g/>
,	,	kIx,	,
Joy	Joy	k1gMnSc1	Joy
Division	Division	k1gInSc1	Division
<g/>
,	,	kIx,	,
Sonic	Sonic	k1gMnSc1	Sonic
Youth	Youth	k1gMnSc1	Youth
<g/>
,	,	kIx,	,
Pere	prát	k5eAaImIp3nS	prát
Ubu	Ubu	k1gMnSc1	Ubu
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Strokes	Strokes	k1gMnSc1	Strokes
<g/>
,	,	kIx,	,
Talking	Talking	k1gInSc1	Talking
Heads	Headsa	k1gFnPc2	Headsa
<g/>
,	,	kIx,	,
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Jane	Jan	k1gMnSc5	Jan
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Addiction	Addiction	k1gInSc1	Addiction
nebo	nebo	k8xC	nebo
The	The	k1gFnSc1	The
Smiths	Smithsa	k1gFnPc2	Smithsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
ve	v	k7c4	v
Francii	Francie	k1gFnSc4	Francie
album	album	k1gNnSc4	album
Les	les	k1gInSc1	les
Enfants	Enfants	k1gInSc1	Enfants
Du	Du	k?	Du
Velvet	Velvet	k1gInSc1	Velvet
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnPc1d1	obsahující
šest	šest	k4xCc4	šest
skladeb	skladba	k1gFnPc2	skladba
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc4	underground
předělaných	předělaný	k2eAgFnPc2d1	předělaná
různými	různý	k2eAgMnPc7d1	různý
umělci	umělec	k1gMnPc7	umělec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
tribute	tribut	k1gInSc5	tribut
album	album	k1gNnSc1	album
Heaven	Heavna	k1gFnPc2	Heavna
&	&	k?	&
Hell	Hella	k1gFnPc2	Hella
<g/>
:	:	kIx,	:
A	a	k9	a
Tribute	tribut	k1gInSc5	tribut
to	ten	k3xDgNnSc1	ten
The	The	k1gFnSc1	The
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc4	underground
obsahující	obsahující	k2eAgFnSc2d1	obsahující
skladby	skladba	k1gFnSc2	skladba
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
různých	různý	k2eAgMnPc2d1	různý
interpretů	interpret	k1gMnPc2	interpret
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
skupina	skupina	k1gFnSc1	skupina
Fiction	Fiction	k1gInSc1	Fiction
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
album	album	k1gNnSc4	album
nazvané	nazvaný	k2eAgInPc1d1	nazvaný
Neverending	Neverending	k1gInSc1	Neverending
Party	parta	k1gFnSc2	parta
obsahující	obsahující	k2eAgFnSc2d1	obsahující
skladby	skladba	k1gFnSc2	skladba
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2012	[number]	k4	2012
vydal	vydat	k5eAaPmAgInS	vydat
Rodolphe	Rodolph	k1gFnSc2	Rodolph
Burger	Burger	k1gInSc1	Burger
album	album	k1gNnSc1	album
This	This	k1gInSc1	This
Is	Is	k1gFnSc1	Is
a	a	k8xC	a
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc1	underground
Song	song	k1gInSc1	song
That	That	k2eAgInSc1d1	That
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
d	d	k?	d
Like	Like	k1gFnSc1	Like
to	ten	k3xDgNnSc4	ten
Sing	Sing	k1gMnSc1	Sing
<g/>
;	;	kIx,	;
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
skladby	skladba	k1gFnSc2	skladba
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc1	underground
doplněné	doplněný	k2eAgInPc1d1	doplněný
upravenou	upravený	k2eAgFnSc7d1	upravená
verzí	verze	k1gFnSc7	verze
skladby	skladba	k1gFnSc2	skladba
"	"	kIx"	"
<g/>
Das	Das	k1gFnSc1	Das
Lied	Lied	k1gInSc1	Lied
vom	vom	k?	vom
einsamen	einsamen	k2eAgInSc1d1	einsamen
Mädchen	Mädchen	k1gInSc1	Mädchen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
45	[number]	k4	45
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
vydání	vydání	k1gNnSc2	vydání
prvního	první	k4xOgNnSc2	první
alba	album	k1gNnSc2	album
skupiny	skupina	k1gFnSc2	skupina
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
tribute	tribut	k1gInSc5	tribut
album	album	k1gNnSc4	album
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
The	The	k1gFnSc2	The
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
&	&	k?	&
Nico	Nico	k6eAd1	Nico
by	by	kYmCp3nS	by
Castle	Castle	k1gFnSc1	Castle
Face	Fac	k1gFnSc2	Fac
and	and	k?	and
Friends	Friends	k1gInSc1	Friends
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechny	všechen	k3xTgFnPc4	všechen
skladby	skladba	k1gFnPc4	skladba
právě	právě	k9	právě
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
alba	album	k1gNnSc2	album
předělané	předělaný	k2eAgFnSc2d1	předělaná
různými	různý	k2eAgMnPc7d1	různý
interprety	interpret	k1gMnPc7	interpret
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgFnPc7	jenž
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Ty	ty	k3xPp2nSc1	ty
Segall	Segall	k1gInSc1	Segall
<g/>
,	,	kIx,	,
Kelley	Kellea	k1gFnPc1	Kellea
Stoltz	Stoltz	k1gMnSc1	Stoltz
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Fresh	Fresha	k1gFnPc2	Fresha
&	&	k?	&
Onlys	Onlysa	k1gFnPc2	Onlysa
<g/>
,	,	kIx,	,
Thee	Thea	k1gFnSc6	Thea
Oh	oh	k0	oh
Sees	Sees	k1gInSc1	Sees
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
skupina	skupina	k1gFnSc1	skupina
The	The	k1gFnSc2	The
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gFnSc2	People
of	of	k?	of
the	the	k?	the
Universe	Universe	k1gFnSc2	Universe
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
začátcích	začátek	k1gInPc6	začátek
hrála	hrát	k5eAaImAgFnS	hrát
coververze	coververze	k1gFnSc1	coververze
jejich	jejich	k3xOp3gFnPc2	jejich
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
později	pozdě	k6eAd2	pozdě
vyšly	vyjít	k5eAaPmAgFnP	vyjít
na	na	k7c6	na
koncertním	koncertní	k2eAgNnSc6d1	koncertní
albu	album	k1gNnSc6	album
Trouble	Trouble	k1gMnSc2	Trouble
Every	Evera	k1gMnSc2	Evera
Day	Day	k1gMnSc2	Day
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
česká	český	k2eAgFnSc1d1	Česká
skupina	skupina	k1gFnSc1	skupina
The	The	k1gFnSc1	The
Velvet	Velvet	k1gInSc4	Velvet
Underground	underground	k1gInSc1	underground
Revival	revival	k1gInSc1	revival
Band	banda	k1gFnPc2	banda
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nS	soustředit
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
materiál	materiál	k1gInSc4	materiál
Velvet	Velvet	k1gInSc1	Velvet
Underground	underground	k1gInSc1	underground
a	a	k8xC	a
jednou	jednou	k6eAd1	jednou
skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
pozvána	pozvat	k5eAaPmNgFnS	pozvat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
s	s	k7c7	s
Maureen	Maurena	k1gFnPc2	Maurena
Tucker	Tuckero	k1gNnPc2	Tuckero
<g/>
.	.	kIx.	.
</s>
<s>
Různí	různý	k2eAgMnPc1d1	různý
interpreti	interpret	k1gMnPc1	interpret
rovněž	rovněž	k9	rovněž
nahráli	nahrát	k5eAaBmAgMnP	nahrát
na	na	k7c4	na
svá	svůj	k3xOyFgNnPc4	svůj
alba	album	k1gNnPc4	album
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Paří	pařit	k5eAaImIp3nS	pařit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
například	například	k6eAd1	například
Cabaret	Cabaret	k1gMnSc1	Cabaret
Voltaire	Voltair	k1gInSc5	Voltair
<g/>
,	,	kIx,	,
Nirvana	Nirvan	k1gMnSc4	Nirvan
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Here	Here	k1gFnSc1	Here
She	She	k1gMnSc1	She
Comes	Comes	k1gMnSc1	Comes
Now	Now	k1gMnSc1	Now
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
R.	R.	kA	R.
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
There	Ther	k1gMnSc5	Ther
She	She	k1gMnSc5	She
Goes	Goes	k1gInSc4	Goes
Again	Again	k1gInSc1	Again
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Pale	pal	k1gInSc5	pal
Blue	Blue	k1gNnPc6	Blue
Eyes	Eyes	k1gInSc4	Eyes
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Gary	Gary	k1gInPc1	Gary
Lucas	Lucasa	k1gFnPc2	Lucasa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
European	European	k1gInSc1	European
Son	son	k1gInSc1	son
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Echo	echo	k1gNnSc1	echo
&	&	k?	&
the	the	k?	the
Bunnymen	Bunnymen	k1gInSc1	Bunnymen
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Run	run	k1gInSc1	run
Run	run	k1gInSc1	run
Run	run	k1gInSc4	run
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tori	Tori	k1gNnSc1	Tori
Amos	Amos	k1gMnSc1	Amos
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
New	New	k1gMnSc1	New
Age	Age	k1gMnSc1	Age
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
UK	UK	kA	UK
Subs	Subs	k1gInSc1	Subs
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Waiting	Waiting	k1gInSc1	Waiting
for	forum	k1gNnPc2	forum
the	the	k?	the
Man	Man	k1gMnSc1	Man
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Joy	Joy	k1gMnSc1	Joy
Division	Division	k1gInSc1	Division
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Sister	Sister	k1gMnSc1	Sister
Ray	Ray	k1gMnSc1	Ray
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Queers	Queers	k1gInSc1	Queers
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Sunday	Sunday	k1gInPc4	Sunday
Morning	Morning	k1gInSc1	Morning
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mott	motto	k1gNnPc2	motto
the	the	k?	the
Hoople	Hoople	k1gFnSc2	Hoople
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Sweet	Sweet	k1gMnSc1	Sweet
Jane	Jan	k1gMnSc5	Jan
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Smashing	Smashing	k1gInSc1	Smashing
Pumpkins	Pumpkins	k1gInSc1	Pumpkins
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Venus	Venus	k1gInSc1	Venus
in	in	k?	in
Furs	Furs	k1gInSc1	Furs
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc2	Bowie
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
White	Whit	k1gMnSc5	Whit
Light	Light	k1gInSc4	Light
<g/>
/	/	kIx~	/
<g/>
White	Whit	k1gMnSc5	Whit
Heat	Heat	k2eAgMnSc1d1	Heat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Duran	Duran	k1gMnSc1	Duran
Duran	Duran	k1gMnSc1	Duran
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Femme	Femm	k1gMnSc5	Femm
Fatale	Fatal	k1gMnSc5	Fatal
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nick	Nick	k1gMnSc1	Nick
Cave	Cav	k1gFnSc2	Cav
and	and	k?	and
the	the	k?	the
Bad	Bad	k1gFnSc2	Bad
Seeds	Seedsa	k1gFnPc2	Seedsa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
All	All	k1gMnSc1	All
Tomorrow	Tomorrow	k1gMnSc1	Tomorrow
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Parties	Partiesa	k1gFnPc2	Partiesa
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
sestava	sestava	k1gFnSc1	sestava
Lou	Lou	k1gMnSc1	Lou
Reed	Reed	k1gMnSc1	Reed
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc1	klavír
<g/>
,	,	kIx,	,
harmonika	harmonika	k1gFnSc1	harmonika
John	John	k1gMnSc1	John
Cale	Cale	k1gInSc1	Cale
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
viola	viola	k1gFnSc1	viola
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc1	klavír
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Sterling	sterling	k1gInSc1	sterling
Morrison	Morrison	k1gInSc4	Morrison
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
Maureen	Maureen	k1gInSc1	Maureen
"	"	kIx"	"
<g/>
Mo	Mo	k1gFnSc1	Mo
<g/>
"	"	kIx"	"
Tucker	Tucker	k1gInSc1	Tucker
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
–	–	k?	–
bicí	bicí	k2eAgInSc1d1	bicí
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnPc4	perkuse
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc4	zpěv
Pozdější	pozdní	k2eAgMnPc1d2	pozdější
členové	člen	k1gMnPc1	člen
Doug	Douga	k1gFnPc2	Douga
Yule	Yule	k1gNnSc2	Yule
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
klávesy	klávesa	k1gFnPc1	klávesa
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Walter	Walter	k1gMnSc1	Walter
Powers	Powers	k1gInSc1	Powers
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
Willie	Willie	k1gFnSc2	Willie
Alexander	Alexandra	k1gFnPc2	Alexandra
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc4	zpěv
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
Nico	Nico	k1gNnSc1	Nico
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
1967	[number]	k4	1967
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
Angus	Angus	k1gInSc1	Angus
MacLise	MacLise	k1gFnSc1	MacLise
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
–	–	k?	–
bicí	bicí	k2eAgMnSc1d1	bicí
Elektrah	Elektrah	k1gMnSc1	Elektrah
Lobel	Lobel	k1gMnSc1	Lobel
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnSc1	perkuse
Henry	Henry	k1gMnSc1	Henry
Flynt	Flynt	k1gMnSc1	Flynt
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
–	–	k?	–
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
Calea	Caleus	k1gMnSc4	Caleus
při	při	k7c6	při
několika	několik	k4yIc6	několik
koncertech	koncert	k1gInPc6	koncert
v	v	k7c6	v
září	září	k1gNnSc6	září
1966	[number]	k4	1966
Billy	Bill	k1gMnPc7	Bill
Yule	Yule	k1gNnSc1	Yule
–	–	k?	–
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
těhotnou	těhotná	k1gFnSc4	těhotná
Tucker	Tuckra	k1gFnPc2	Tuckra
na	na	k7c6	na
albu	album	k1gNnSc6	album
<g />
.	.	kIx.	.
</s>
<s>
Loaded	Loaded	k1gInSc1	Loaded
Tommy	Tomma	k1gFnSc2	Tomma
Castanaro	Castanara	k1gFnSc5	Castanara
–	–	k?	–
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
těhotnou	těhotná	k1gFnSc4	těhotná
Tucker	Tuckra	k1gFnPc2	Tuckra
na	na	k7c6	na
albu	album	k1gNnSc6	album
Loaded	Loaded	k1gMnSc1	Loaded
Adrian	Adrian	k1gMnSc1	Adrian
Barber	Barber	k1gMnSc1	Barber
–	–	k?	–
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
těhotnou	těhotná	k1gFnSc4	těhotná
Tucker	Tuckra	k1gFnPc2	Tuckra
na	na	k7c4	na
albu	alba	k1gFnSc4	alba
Loaded	Loaded	k1gMnSc1	Loaded
Larry	Larra	k1gFnSc2	Larra
Estridge	Estridge	k1gInSc1	Estridge
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1971	[number]	k4	1971
Rob	roba	k1gFnPc2	roba
Norris	Norris	k1gFnPc2	Norris
(	(	kIx(	(
<g/>
z	z	k7c2	z
The	The	k1gFnSc2	The
Bongos	Bongos	k1gInSc1	Bongos
<g/>
)	)	kIx)	)
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
na	na	k7c6	na
britském	britský	k2eAgNnSc6d1	Britské
turné	turné	k1gNnSc6	turné
k	k	k7c3	k
albu	album	k1gNnSc3	album
Squeeze	Squeeze	k1gFnSc2	Squeeze
George	Georg	k1gMnSc2	Georg
Kay	Kay	k1gFnSc2	Kay
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
na	na	k7c6	na
britském	britský	k2eAgNnSc6d1	Britské
turné	turné	k1gNnSc6	turné
k	k	k7c3	k
albu	album	k1gNnSc3	album
Squeeze	Squeeze	k1gFnSc2	Squeeze
Don	dona	k1gFnPc2	dona
Silverman	Silverman	k1gMnSc1	Silverman
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
na	na	k7c6	na
britském	britský	k2eAgNnSc6d1	Britské
turné	turné	k1gNnSc6	turné
k	k	k7c3	k
albu	album	k1gNnSc3	album
Squeeze	Squeeze	k1gFnSc2	Squeeze
Mark	Mark	k1gMnSc1	Mark
Nauseef	Nauseef	k1gMnSc1	Nauseef
–	–	k?	–
bicí	bicí	k2eAgMnSc1d1	bicí
na	na	k7c6	na
britském	britský	k2eAgNnSc6d1	Britské
turné	turné	k1gNnSc6	turné
k	k	k7c3	k
albu	album	k1gNnSc3	album
Squeeze	Squeeze	k1gFnSc2	Squeeze
Ian	Ian	k1gFnSc2	Ian
Paice	Paice	k1gFnSc2	Paice
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
na	na	k7c6	na
albu	album	k1gNnSc6	album
Squeeze	Squeeze	k1gFnSc2	Squeeze
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
The	The	k1gFnSc1	The
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
<g/>
.	.	kIx.	.
</s>
<s>
Studiová	studiový	k2eAgFnSc1d1	studiová
alba	alba	k1gFnSc1	alba
The	The	k1gFnSc2	The
Velvet	Velveta	k1gFnPc2	Velveta
Underground	underground	k1gInSc1	underground
&	&	k?	&
Nico	Nico	k1gMnSc1	Nico
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
White	Whit	k1gInSc5	Whit
Light	Light	k1gInSc4	Light
<g/>
/	/	kIx~	/
<g/>
White	Whit	k1gMnSc5	Whit
Heat	Heat	k1gMnSc1	Heat
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Loaded	Loaded	k1gInSc1	Loaded
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Squeeze	Squeeze	k1gFnSc2	Squeeze
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
</s>
