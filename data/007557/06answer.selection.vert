<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gNnSc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgMnSc1d1	označovaný
T.	T.	kA	T.
G.	G.	kA	G.
M.	M.	kA	M.
<g/>
,	,	kIx,	,
TGM	TGM	kA	TGM
nebo	nebo	k8xC	nebo
president	president	k1gMnSc1	president
Osvoboditel	osvoboditel	k1gMnSc1	osvoboditel
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1850	[number]	k4	1850
Hodonín	Hodonín	k1gInSc1	Hodonín
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1937	[number]	k4	1937
Lány	lán	k1gInPc4	lán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
státník	státník	k1gMnSc1	státník
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
