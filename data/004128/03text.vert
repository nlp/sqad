<s>
Hlubotisk	hlubotisk	k1gInSc1	hlubotisk
je	být	k5eAaImIp3nS	být
tisková	tiskový	k2eAgFnSc1d1	tisková
technika	technika	k1gFnSc1	technika
pracující	pracující	k2eAgFnSc1d1	pracující
na	na	k7c6	na
principu	princip	k1gInSc6	princip
tisku	tisk	k1gInSc2	tisk
z	z	k7c2	z
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Tisková	tiskový	k2eAgFnSc1d1	tisková
forma	forma	k1gFnSc1	forma
je	být	k5eAaImIp3nS	být
reliéfní	reliéfní	k2eAgInPc4d1	reliéfní
a	a	k8xC	a
tiskové	tiskový	k2eAgInPc4d1	tiskový
prvky	prvek	k1gInPc4	prvek
jsou	být	k5eAaImIp3nP	být
zahloubeny	zahloubit	k5eAaPmNgInP	zahloubit
pod	pod	k7c4	pod
úroveň	úroveň	k1gFnSc4	úroveň
prvků	prvek	k1gInPc2	prvek
netisknoucích	tisknoucí	k2eNgInPc2d1	tisknoucí
–	–	k?	–
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
tedy	tedy	k9	tedy
opačně	opačně	k6eAd1	opačně
než	než	k8xS	než
u	u	k7c2	u
tisku	tisk	k1gInSc2	tisk
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Tisková	tiskový	k2eAgNnPc1d1	tiskové
místa	místo	k1gNnPc1	místo
jsou	být	k5eAaImIp3nP	být
zaplněna	zaplněn	k2eAgNnPc1d1	zaplněno
řídkou	řídký	k2eAgFnSc7d1	řídká
rychle	rychle	k6eAd1	rychle
zasychající	zasychající	k2eAgFnSc7d1	zasychající
barvou	barva	k1gFnSc7	barva
a	a	k8xC	a
při	při	k7c6	při
tisku	tisk	k1gInSc6	tisk
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
převzetí	převzetí	k1gNnSc3	převzetí
barvy	barva	k1gFnSc2	barva
potiskovaným	potiskovaný	k2eAgInSc7d1	potiskovaný
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
ponejvíce	ponejvíce	k6eAd1	ponejvíce
papírem	papír	k1gInSc7	papír
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
fixaci	fixace	k1gFnSc3	fixace
barvy	barva	k1gFnSc2	barva
na	na	k7c6	na
papíru	papír	k1gInSc6	papír
dochází	docházet	k5eAaImIp3nS	docházet
odpařením	odpaření	k1gNnSc7	odpaření
těkavých	těkavý	k2eAgNnPc2d1	těkavé
rozpouštědel	rozpouštědlo	k1gNnPc2	rozpouštědlo
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
knihtisku	knihtisk	k1gInSc2	knihtisk
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
nepřímý	přímý	k2eNgInSc1d1	nepřímý
hlubotisk	hlubotisk	k1gInSc1	hlubotisk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
formy	forma	k1gFnSc2	forma
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
hlubotisk	hlubotisk	k1gInSc4	hlubotisk
plochý	plochý	k2eAgInSc4d1	plochý
nebo	nebo	k8xC	nebo
rotační	rotační	k2eAgInSc4d1	rotační
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
naprosto	naprosto	k6eAd1	naprosto
převládá	převládat	k5eAaImIp3nS	převládat
rotační	rotační	k2eAgInSc4d1	rotační
hlubotisk	hlubotisk	k1gInSc4	hlubotisk
<g/>
.	.	kIx.	.
</s>
<s>
Hlubotisk	hlubotisk	k1gInSc1	hlubotisk
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jen	jen	k9	jen
k	k	k7c3	k
tisku	tisk	k1gInSc3	tisk
vysokých	vysoký	k2eAgInPc2d1	vysoký
nákladů	náklad	k1gInPc2	náklad
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
náročnosti	náročnost	k1gFnSc2	náročnost
technologie	technologie	k1gFnSc2	technologie
přípravy	příprava	k1gFnSc2	příprava
a	a	k8xC	a
vysokých	vysoký	k2eAgInPc2d1	vysoký
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
tiskové	tiskový	k2eAgFnSc2d1	tisková
formy	forma	k1gFnSc2	forma
(	(	kIx(	(
<g/>
laserová	laserový	k2eAgFnSc1d1	laserová
gravura	gravura	k1gFnSc1	gravura
<g/>
,	,	kIx,	,
chromování	chromování	k1gNnSc1	chromování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
vysokých	vysoký	k2eAgFnPc2d1	vysoká
rychlostí	rychlost	k1gFnPc2	rychlost
tisku	tisk	k1gInSc2	tisk
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
prakticky	prakticky	k6eAd1	prakticky
jen	jen	k9	jen
o	o	k7c4	o
kotoučový	kotoučový	k2eAgInSc4d1	kotoučový
nekonečný	konečný	k2eNgInSc4d1	nekonečný
tisk	tisk	k1gInSc4	tisk
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
bývají	bývat	k5eAaImIp3nP	bývat
hlubotiskové	hlubotiskový	k2eAgFnPc1d1	hlubotisková
linky	linka	k1gFnPc1	linka
osazovány	osazovat	k5eAaImNgFnP	osazovat
výsekovou	výsekový	k2eAgFnSc7d1	výseková
jednotkou	jednotka	k1gFnSc7	jednotka
s	s	k7c7	s
automatickým	automatický	k2eAgNnSc7d1	automatické
snášecim	snášeci	k1gNnSc7	snášeci
a	a	k8xC	a
vykládacim	vykládacim	k6eAd1	vykládacim
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
princip	princip	k1gInSc1	princip
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
k	k	k7c3	k
tisku	tisk	k1gInSc2	tisk
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
mědirytu	mědiryt	k1gInSc2	mědiryt
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
měděné	měděný	k2eAgFnSc2d1	měděná
destičky	destička	k1gFnSc2	destička
se	se	k3xPyFc4	se
obraz	obraz	k1gInSc1	obraz
zahluboval	zahlubovat	k5eAaImAgInS	zahlubovat
rydlem	rydlo	k1gNnSc7	rydlo
<g/>
,	,	kIx,	,
samozřejmě	samozřejmě	k6eAd1	samozřejmě
ručně	ručně	k6eAd1	ručně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mědirytiny	mědirytina	k1gFnSc2	mědirytina
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
další	další	k2eAgFnPc1d1	další
varianty	varianta	k1gFnPc1	varianta
grafických	grafický	k2eAgFnPc2d1	grafická
technik	technika	k1gFnPc2	technika
jako	jako	k8xC	jako
mědilept	mědilepta	k1gFnPc2	mědilepta
<g/>
,	,	kIx,	,
suchá	suchý	k2eAgFnSc1d1	suchá
jehla	jehla	k1gFnSc1	jehla
<g/>
,	,	kIx,	,
akvatinta	akvatinta	k1gFnSc1	akvatinta
aj.	aj.	kA	aj.
Podobnou	podobný	k2eAgFnSc7d1	podobná
technikou	technika	k1gFnSc7	technika
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
ocelorytina	ocelorytina	k1gFnSc1	ocelorytina
či	či	k8xC	či
ocelotisk	ocelotisk	k1gInSc1	ocelotisk
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
technika	technika	k1gFnSc1	technika
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
velkou	velký	k2eAgFnSc4d1	velká
výdržnost	výdržnost	k1gFnSc4	výdržnost
tiskové	tiskový	k2eAgFnSc2d1	tisková
formy	forma	k1gFnSc2	forma
<g/>
,	,	kIx,	,
dobrou	dobrý	k2eAgFnSc4d1	dobrá
reprodukci	reprodukce	k1gFnSc4	reprodukce
jemných	jemný	k2eAgFnPc2d1	jemná
pérovek	pérovka	k1gFnPc2	pérovka
a	a	k8xC	a
obtížnou	obtížný	k2eAgFnSc4d1	obtížná
napodobitelnost	napodobitelnost	k1gFnSc4	napodobitelnost
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
tisku	tisk	k1gInSc6	tisk
cenin	cenina	k1gFnPc2	cenina
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
tisku	tisk	k1gInSc2	tisk
z	z	k7c2	z
hloubky	hloubka	k1gFnSc2	hloubka
patří	patřit	k5eAaImIp3nS	patřit
tamponový	tamponový	k2eAgInSc1d1	tamponový
tisk	tisk	k1gInSc1	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nepřímý	přímý	k2eNgInSc4d1	nepřímý
tisk	tisk	k1gInSc4	tisk
pomocí	pomocí	k7c2	pomocí
silikonového	silikonový	k2eAgInSc2d1	silikonový
pružného	pružný	k2eAgInSc2d1	pružný
tampónu	tampón	k1gInSc2	tampón
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ze	z	k7c2	z
zahloubené	zahloubený	k2eAgFnSc2d1	zahloubená
ploché	plochý	k2eAgFnSc2d1	plochá
formy	forma	k1gFnSc2	forma
přenáší	přenášet	k5eAaImIp3nS	přenášet
barvu	barva	k1gFnSc4	barva
většinou	většina	k1gFnSc7	většina
na	na	k7c4	na
vícerozměrné	vícerozměrný	k2eAgInPc4d1	vícerozměrný
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
především	především	k9	především
při	při	k7c6	při
potisku	potisk	k1gInSc6	potisk
reklamních	reklamní	k2eAgInPc2d1	reklamní
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
předmětů	předmět	k1gInPc2	předmět
(	(	kIx(	(
<g/>
propisky	propiska	k1gFnPc1	propiska
<g/>
,	,	kIx,	,
zapalovače	zapalovač	k1gInPc1	zapalovač
<g/>
,	,	kIx,	,
hrnečky	hrneček	k1gInPc1	hrneček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rotační	rotační	k2eAgInSc1d1	rotační
stírací	stírací	k2eAgInSc1d1	stírací
hlubotisk	hlubotisk	k1gInSc1	hlubotisk
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
Karel	Karel	k1gMnSc1	Karel
Klíč	klíč	k1gInSc1	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tisková	tiskový	k2eAgFnSc1d1	tisková
technika	technika	k1gFnSc1	technika
byla	být	k5eAaImAgFnS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
Klíčova	Klíčův	k2eAgInSc2d1	Klíčův
vynálezu	vynález	k1gInSc2	vynález
heliogravury	heliogravura	k1gFnSc2	heliogravura
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
obraz	obraz	k1gInSc1	obraz
přenášen	přenášet	k5eAaImNgInS	přenášet
na	na	k7c4	na
měděnou	měděný	k2eAgFnSc4d1	měděná
destičku	destička	k1gFnSc4	destička
s	s	k7c7	s
přitaveným	přitavený	k2eAgInSc7d1	přitavený
asfaltovým	asfaltový	k2eAgInSc7d1	asfaltový
práškem	prášek	k1gInSc7	prášek
pomocí	pomoc	k1gFnPc2	pomoc
světlocitlivého	světlocitlivý	k2eAgInSc2d1	světlocitlivý
tzv.	tzv.	kA	tzv.
pigmentového	pigmentový	k2eAgInSc2d1	pigmentový
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
byl	být	k5eAaImAgInS	být
obraz	obraz	k1gInSc1	obraz
nakopírován	nakopírovat	k5eAaPmNgInS	nakopírovat
z	z	k7c2	z
tónového	tónový	k2eAgInSc2d1	tónový
diapozitivu	diapozitiv	k1gInSc2	diapozitiv
<g/>
.	.	kIx.	.
</s>
<s>
Tisknoucí	tisknoucí	k2eAgInPc1d1	tisknoucí
prvky	prvek	k1gInPc1	prvek
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
zahloubeny	zahloubit	k5eAaPmNgInP	zahloubit
leptáním	leptání	k1gNnSc7	leptání
FeCl	FeCl	k1gInSc1	FeCl
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Asfaltový	asfaltový	k2eAgInSc1d1	asfaltový
prášek	prášek	k1gInSc1	prášek
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
zrno	zrno	k1gNnSc4	zrno
potřebné	potřebný	k2eAgNnSc4d1	potřebné
k	k	k7c3	k
reprodukci	reprodukce	k1gFnSc3	reprodukce
polotónu	polotón	k1gInSc2	polotón
před	před	k7c7	před
vynálezem	vynález	k1gInSc7	vynález
autotypie	autotypie	k1gFnSc2	autotypie
<g/>
.	.	kIx.	.
</s>
<s>
Klasický	klasický	k2eAgInSc1d1	klasický
hlubotisk	hlubotisk	k1gInSc1	hlubotisk
využívá	využívat	k5eAaImIp3nS	využívat
také	také	k9	také
pigmentového	pigmentový	k2eAgInSc2d1	pigmentový
papíru	papír	k1gInSc2	papír
a	a	k8xC	a
leptání	leptání	k1gNnSc2	leptání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
asfaltové	asfaltový	k2eAgNnSc1d1	asfaltové
zrno	zrno	k1gNnSc1	zrno
je	být	k5eAaImIp3nS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
tzv.	tzv.	kA	tzv.
hlubotiskovou	hlubotiskový	k2eAgFnSc7d1	hlubotisková
sítí	síť	k1gFnSc7	síť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obraz	obraz	k1gInSc4	obraz
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
do	do	k7c2	do
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
linek	linka	k1gFnPc2	linka
–	–	k?	–
tzv.	tzv.	kA	tzv.
nosných	nosný	k2eAgInPc2d1	nosný
můstků	můstek	k1gInPc2	můstek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pak	pak	k6eAd1	pak
při	při	k7c6	při
tisku	tisk	k1gInSc6	tisk
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
opěrný	opěrný	k2eAgInSc1d1	opěrný
systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
hlubotiskový	hlubotiskový	k2eAgInSc4d1	hlubotiskový
stěrač	stěrač	k1gInSc4	stěrač
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
nanesená	nanesený	k2eAgFnSc1d1	nanesená
na	na	k7c4	na
tiskovou	tiskový	k2eAgFnSc4d1	tisková
formu	forma	k1gFnSc4	forma
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
z	z	k7c2	z
netisknoucích	tisknoucí	k2eNgNnPc2d1	Netisknoucí
míst	místo	k1gNnPc2	místo
musí	muset	k5eAaImIp3nS	muset
odstranit	odstranit	k5eAaPmF	odstranit
právě	právě	k9	právě
tímto	tento	k3xDgInSc7	tento
stěračem	stěrač	k1gInSc7	stěrač
<g/>
.	.	kIx.	.
</s>
<s>
Klasický	klasický	k2eAgInSc1d1	klasický
hlubotisk	hlubotisk	k1gInSc1	hlubotisk
využívá	využívat	k5eAaImIp3nS	využívat
tisknoucích	tisknoucí	k2eAgInPc2d1	tisknoucí
prvků	prvek	k1gInPc2	prvek
o	o	k7c6	o
stejné	stejný	k2eAgFnSc6d1	stejná
velikosti	velikost	k1gFnSc6	velikost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
hloubkou	hloubka	k1gFnSc7	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Tónové	tónový	k2eAgInPc1d1	tónový
odstíny	odstín	k1gInPc1	odstín
pak	pak	k6eAd1	pak
nejsou	být	k5eNaImIp3nP	být
vyjádřeny	vyjádřen	k2eAgFnPc1d1	vyjádřena
velikostí	velikost	k1gFnSc7	velikost
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
moderních	moderní	k2eAgFnPc2d1	moderní
tiskových	tiskový	k2eAgFnPc2d1	tisková
technik	technika	k1gFnPc2	technika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
množstvím	množství	k1gNnSc7	množství
(	(	kIx(	(
<g/>
nánosem	nános	k1gInSc7	nános
<g/>
)	)	kIx)	)
transparentní	transparentní	k2eAgFnPc1d1	transparentní
tiskové	tiskový	k2eAgFnPc1d1	tisková
barvy	barva	k1gFnPc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
lze	lze	k6eAd1	lze
optimálně	optimálně	k6eAd1	optimálně
reprodukovat	reprodukovat	k5eAaBmF	reprodukovat
tónové	tónový	k2eAgFnPc4d1	tónová
předlohy	předloha	k1gFnPc4	předloha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
klasického	klasický	k2eAgInSc2d1	klasický
hlubotisku	hlubotisk	k1gInSc2	hlubotisk
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
odvozen	odvodit	k5eAaPmNgInS	odvodit
hlubotisk	hlubotisk	k1gInSc1	hlubotisk
autotypický	autotypický	k2eAgInSc1d1	autotypický
<g/>
.	.	kIx.	.
</s>
<s>
Tónový	tónový	k2eAgInSc1d1	tónový
diapozitiv	diapozitiv	k1gInSc1	diapozitiv
byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
nahrazen	nahradit	k5eAaPmNgInS	nahradit
autotypickým	autotypický	k2eAgInSc7d1	autotypický
(	(	kIx(	(
<g/>
rastrovým	rastrový	k2eAgFnPc3d1	rastrová
<g/>
)	)	kIx)	)
a	a	k8xC	a
tisknoucí	tisknoucí	k2eAgInPc1d1	tisknoucí
prvky	prvek	k1gInPc1	prvek
měly	mít	k5eAaImAgInP	mít
tedy	tedy	k9	tedy
různou	různý	k2eAgFnSc4d1	různá
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
stejnou	stejný	k2eAgFnSc4d1	stejná
hloubku	hloubka	k1gFnSc4	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Formy	forma	k1gFnPc1	forma
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
leptaly	leptat	k5eAaImAgFnP	leptat
<g/>
.	.	kIx.	.
</s>
<s>
Odpadlo	odpadnout	k5eAaPmAgNnS	odpadnout
kopírování	kopírování	k1gNnSc1	kopírování
hlubotiskové	hlubotiskový	k2eAgFnSc2d1	hlubotisková
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nebyla	být	k5eNaImAgFnS	být
potřeba	potřeba	k6eAd1	potřeba
díky	díky	k7c3	díky
autotypické	autotypický	k2eAgFnSc3d1	autotypický
síti	síť	k1gFnSc3	síť
diapozitivu	diapozitiv	k1gInSc2	diapozitiv
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
nejpoužívanější	používaný	k2eAgFnSc7d3	nejpoužívanější
variantou	varianta	k1gFnSc7	varianta
je	být	k5eAaImIp3nS	být
hlubotisk	hlubotisk	k1gInSc1	hlubotisk
poloautotypický	poloautotypický	k2eAgInSc1d1	poloautotypický
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
tisknoucí	tisknoucí	k2eAgInPc1d1	tisknoucí
prvky	prvek	k1gInPc1	prvek
ryty	ryt	k2eAgInPc1d1	ryt
diamantovou	diamantový	k2eAgFnSc7d1	Diamantová
kónickou	kónický	k2eAgFnSc7d1	kónická
jehlou	jehla	k1gFnSc7	jehla
a	a	k8xC	a
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
velikostí	velikost	k1gFnSc7	velikost
i	i	k8xC	i
hloubkou	hloubka	k1gFnSc7	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
tiskové	tiskový	k2eAgFnSc2d1	tisková
formy	forma	k1gFnSc2	forma
je	být	k5eAaImIp3nS	být
ocelový	ocelový	k2eAgInSc1d1	ocelový
válec	válec	k1gInSc1	válec
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějž	jenž	k3xRgInSc4	jenž
se	se	k3xPyFc4	se
galvanicky	galvanicky	k6eAd1	galvanicky
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
vrstvy	vrstva	k1gFnPc1	vrstva
dalších	další	k2eAgInPc2d1	další
kovů	kov	k1gInPc2	kov
–	–	k?	–
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
následně	následně	k6eAd1	následně
po	po	k7c6	po
zhotovení	zhotovení	k1gNnSc6	zhotovení
obrazu	obraz	k1gInSc2	obraz
ještě	ještě	k6eAd1	ještě
chromu	chromat	k5eAaImIp1nS	chromat
<g/>
.	.	kIx.	.
</s>
<s>
Kovy	kov	k1gInPc1	kov
se	se	k3xPyFc4	se
na	na	k7c4	na
ocelový	ocelový	k2eAgInSc4d1	ocelový
válec	válec	k1gInSc4	válec
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
v	v	k7c6	v
elektrolytu	elektrolyt	k1gInSc6	elektrolyt
–	–	k?	–
vodivém	vodivý	k2eAgNnSc6d1	vodivé
roztoku	roztok	k1gInSc3	roztok
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
roztoky	roztok	k1gInPc1	roztok
kyselin	kyselina	k1gFnPc2	kyselina
HNO	HNO	kA	HNO
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
druhá	druhý	k4xOgFnSc1	druhý
příloha	příloha	k1gFnSc1	příloha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Katodou	katoda	k1gFnSc7	katoda
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
vždy	vždy	k6eAd1	vždy
pokovovaný	pokovovaný	k2eAgInSc4d1	pokovovaný
válec	válec	k1gInSc4	válec
<g/>
,	,	kIx,	,
anodou	anoda	k1gFnSc7	anoda
například	například	k6eAd1	například
granule	granule	k1gFnSc2	granule
z	z	k7c2	z
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Ocelové	ocelový	k2eAgNnSc1d1	ocelové
jádro	jádro	k1gNnSc1	jádro
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
ponikluje	poniklovat	k5eAaPmIp3nS	poniklovat
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
nanáší	nanášet	k5eAaImIp3nS	nanášet
základní	základní	k2eAgFnSc1d1	základní
vrstva	vrstva	k1gFnSc1	vrstva
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
většinou	většinou	k6eAd1	většinou
dělicí	dělicí	k2eAgFnSc1d1	dělicí
vrstva	vrstva	k1gFnSc1	vrstva
a	a	k8xC	a
pak	pak	k9	pak
tzv.	tzv.	kA	tzv.
Ballardova	Ballardův	k2eAgFnSc1d1	Ballardova
slupka	slupka	k1gFnSc1	slupka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tenkou	tenký	k2eAgFnSc4d1	tenká
vrstvu	vrstva	k1gFnSc4	vrstva
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
tisknoucí	tisknoucí	k2eAgInPc4d1	tisknoucí
prvky	prvek	k1gInPc4	prvek
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
povrch	povrch	k6eAd1wR	povrch
pochromuje	pochromovat	k5eAaPmIp3nS	pochromovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tisku	tisk	k1gInSc6	tisk
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pochromovanou	pochromovaný	k2eAgFnSc4d1	pochromovaná
Ballardovu	Ballardův	k2eAgFnSc4d1	Ballardova
slupku	slupka	k1gFnSc4	slupka
naříznout	naříznout	k5eAaPmF	naříznout
a	a	k8xC	a
díky	díky	k7c3	díky
dělicí	dělicí	k2eAgFnSc3d1	dělicí
vrstvě	vrstva	k1gFnSc3	vrstva
sloupnout	sloupnout	k5eAaPmF	sloupnout
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
další	další	k2eAgNnSc1d1	další
galvanické	galvanický	k2eAgNnSc1d1	galvanické
nanášení	nanášení	k1gNnSc1	nanášení
slupky	slupka	k1gFnSc2	slupka
<g/>
,	,	kIx,	,
tvorby	tvorba	k1gFnSc2	tvorba
tisknoucích	tisknoucí	k2eAgInPc2d1	tisknoucí
prvků	prvek	k1gInPc2	prvek
atd	atd	kA	atd
<g/>
...	...	k?	...
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
varianty	varianta	k1gFnPc1	varianta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Ballardův	Ballardův	k2eAgInSc1d1	Ballardův
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
nejpoužívanější	používaný	k2eAgInSc1d3	nejpoužívanější
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
klasického	klasický	k2eAgInSc2d1	klasický
hlubotisku	hlubotisk	k1gInSc2	hlubotisk
se	se	k3xPyFc4	se
na	na	k7c4	na
speciální	speciální	k2eAgInSc4d1	speciální
papír	papír	k1gInSc4	papír
s	s	k7c7	s
vrstvou	vrstva	k1gFnSc7	vrstva
zcitlivené	zcitlivený	k2eAgFnSc2d1	zcitlivený
želatiny	želatina	k1gFnSc2	želatina
nakopírovala	nakopírovat	k5eAaPmAgFnS	nakopírovat
negativní	negativní	k2eAgFnSc1d1	negativní
hlubotisková	hlubotiskový	k2eAgFnSc1d1	hlubotisková
síť	síť	k1gFnSc1	síť
a	a	k8xC	a
následně	následně	k6eAd1	následně
obraz	obraz	k1gInSc4	obraz
tónového	tónový	k2eAgInSc2d1	tónový
diapozitivu	diapozitiv	k1gInSc2	diapozitiv
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
vrstva	vrstva	k1gFnSc1	vrstva
s	s	k7c7	s
vykopírovaným	vykopírovaný	k2eAgInSc7d1	vykopírovaný
obrazem	obraz	k1gInSc7	obraz
přenesla	přenést	k5eAaPmAgFnS	přenést
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
poměděného	poměděný	k2eAgInSc2d1	poměděný
válce	válec	k1gInSc2	válec
a	a	k8xC	a
přes	přes	k7c4	přes
reliéf	reliéf	k1gInSc4	reliéf
želatiny	želatina	k1gFnSc2	želatina
se	se	k3xPyFc4	se
leptalo	leptat	k5eAaImAgNnS	leptat
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tónovému	tónový	k2eAgInSc3d1	tónový
podkladu	podklad	k1gInSc3	podklad
byla	být	k5eAaImAgFnS	být
želatina	želatina	k1gFnSc1	želatina
různě	různě	k6eAd1	různě
utvrzena	utvrzen	k2eAgFnSc1d1	utvrzena
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
při	při	k7c6	při
dodržení	dodržení	k1gNnSc6	dodržení
správného	správný	k2eAgInSc2d1	správný
času	čas	k1gInSc2	čas
leptání	leptání	k1gNnSc2	leptání
docílilo	docílit	k5eAaPmAgNnS	docílit
různé	různý	k2eAgInPc4d1	různý
hloubky	hloubek	k1gInPc4	hloubek
tisknoucích	tisknoucí	k2eAgInPc2d1	tisknoucí
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
autotypického	autotypický	k2eAgInSc2d1	autotypický
hlubotisku	hlubotisk	k1gInSc2	hlubotisk
se	se	k3xPyFc4	se
ovrstvoval	ovrstvovat	k5eAaPmAgMnS	ovrstvovat
přímo	přímo	k6eAd1	přímo
formový	formový	k2eAgInSc4d1	formový
válec	válec	k1gInSc4	válec
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
ovrstvovacím	ovrstvovací	k2eAgInSc7d1	ovrstvovací
prstencem	prstenec	k1gInSc7	prstenec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
vrstvu	vrstva	k1gFnSc4	vrstva
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nakopíroval	nakopírovat	k5eAaPmAgInS	nakopírovat
autotypický	autotypický	k2eAgInSc1d1	autotypický
diapozitiv	diapozitiv	k1gInSc1	diapozitiv
a	a	k8xC	a
následovalo	následovat	k5eAaImAgNnS	následovat
leptání	leptání	k1gNnSc4	leptání
FeCl	FeCl	k1gInSc1	FeCl
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
poloautotypického	poloautotypický	k2eAgInSc2d1	poloautotypický
hlubotisku	hlubotisk	k1gInSc2	hlubotisk
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
elektronicky	elektronicky	k6eAd1	elektronicky
řízené	řízený	k2eAgInPc4d1	řízený
rycí	rycí	k2eAgInPc4d1	rycí
automaty	automat	k1gInPc4	automat
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zhotovují	zhotovovat	k5eAaImIp3nP	zhotovovat
jamky	jamka	k1gFnPc4	jamka
diamantovou	diamantový	k2eAgFnSc4d1	Diamantová
jehlou	jehla	k1gFnSc7	jehla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
kmitá	kmitat	k5eAaImIp3nS	kmitat
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
rychlostí	rychlost	k1gFnSc7	rychlost
(	(	kIx(	(
<g/>
4000	[number]	k4	4000
kmitů	kmit	k1gInPc2	kmit
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
se	se	k3xPyFc4	se
ryje	rýt	k5eAaImIp3nS	rýt
ve	v	k7c6	v
šroubovici	šroubovice	k1gFnSc6	šroubovice
<g/>
.	.	kIx.	.
</s>
<s>
Rycí	rycí	k2eAgFnSc1d1	rycí
jehla	jehla	k1gFnSc1	jehla
se	se	k3xPyFc4	se
posouvá	posouvat	k5eAaImIp3nS	posouvat
po	po	k7c6	po
délce	délka	k1gFnSc6	délka
otáčejícího	otáčející	k2eAgInSc2d1	otáčející
se	se	k3xPyFc4	se
válce	válec	k1gInSc2	válec
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
měly	mít	k5eAaImAgInP	mít
rycí	rycí	k2eAgInPc1d1	rycí
automaty	automat	k1gInPc1	automat
snímací	snímací	k2eAgFnSc4d1	snímací
část	část	k1gFnSc4	část
pro	pro	k7c4	pro
skenování	skenování	k1gNnSc4	skenování
předlohy	předloha	k1gFnSc2	předloha
a	a	k8xC	a
rycí	rycí	k2eAgFnSc4d1	rycí
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
není	být	k5eNaImIp3nS	být
snímací	snímací	k2eAgFnSc1d1	snímací
část	část	k1gFnSc1	část
nutná	nutný	k2eAgFnSc1d1	nutná
<g/>
,	,	kIx,	,
ryje	rýt	k5eAaImIp3nS	rýt
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
digitálních	digitální	k2eAgNnPc2d1	digitální
dat	datum	k1gNnPc2	datum
v	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Tisknoucí	tisknoucí	k2eAgInPc1d1	tisknoucí
prvky	prvek	k1gInPc1	prvek
se	se	k3xPyFc4	se
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
zhotovovat	zhotovovat	k5eAaImF	zhotovovat
vypalováním	vypalování	k1gNnSc7	vypalování
laserem	laser	k1gInSc7	laser
či	či	k8xC	či
elektronovým	elektronový	k2eAgInSc7d1	elektronový
paprskem	paprsek	k1gInSc7	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Dominantní	dominantní	k2eAgFnSc7d1	dominantní
technologií	technologie	k1gFnSc7	technologie
je	být	k5eAaImIp3nS	být
však	však	k9	však
dnes	dnes	k6eAd1	dnes
rytí	rytý	k2eAgMnPc1d1	rytý
diamantem	diamant	k1gInSc7	diamant
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
technologií	technologie	k1gFnPc2	technologie
se	se	k3xPyFc4	se
forma	forma	k1gFnSc1	forma
po	po	k7c4	po
zhotovení	zhotovení	k1gNnSc4	zhotovení
tisknoucích	tisknoucí	k2eAgInPc2d1	tisknoucí
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
případných	případný	k2eAgFnPc6d1	případná
korekturách	korektura	k1gFnPc6	korektura
ještě	ještě	k6eAd1	ještě
galvanicky	galvanicky	k6eAd1	galvanicky
chromuje	chromovat	k5eAaImIp3nS	chromovat
pro	pro	k7c4	pro
větší	veliký	k2eAgFnSc4d2	veliký
výdržnost	výdržnost	k1gFnSc4	výdržnost
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
setkáme	setkat	k5eAaPmIp1nP	setkat
s	s	k7c7	s
kotoučovými	kotoučový	k2eAgInPc7d1	kotoučový
hlubotiskovými	hlubotiskový	k2eAgInPc7d1	hlubotiskový
stroji	stroj	k1gInPc7	stroj
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
v	v	k7c6	v
obalovém	obalový	k2eAgInSc6d1	obalový
tisku	tisk	k1gInSc6	tisk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
při	při	k7c6	při
tisku	tisk	k1gInSc6	tisk
periodik	periodikum	k1gNnPc2	periodikum
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
například	například	k6eAd1	například
archové	archový	k2eAgInPc1d1	archový
stroje	stroj	k1gInPc1	stroj
pro	pro	k7c4	pro
potisk	potisk	k1gInSc4	potisk
obalů	obal	k1gInPc2	obal
na	na	k7c4	na
cigarety	cigareta	k1gFnPc4	cigareta
<g/>
,	,	kIx,	,
či	či	k8xC	či
některé	některý	k3yIgInPc1	některý
speciální	speciální	k2eAgInPc1d1	speciální
stroje	stroj	k1gInPc1	stroj
pro	pro	k7c4	pro
tisk	tisk	k1gInSc4	tisk
cenin	cenina	k1gFnPc2	cenina
<g/>
.	.	kIx.	.
</s>
<s>
Setkat	setkat	k5eAaPmF	setkat
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
i	i	k9	i
se	s	k7c7	s
stroji	stroj	k1gInPc7	stroj
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
tiskové	tiskový	k2eAgFnPc4d1	tisková
techniky	technika	k1gFnPc4	technika
kombinují	kombinovat	k5eAaImIp3nP	kombinovat
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
např.	např.	kA	např.
u	u	k7c2	u
cenin	cenina	k1gFnPc2	cenina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnPc1d1	základní
části	část	k1gFnPc1	část
hlubotiskových	hlubotiskový	k2eAgInPc2d1	hlubotiskový
strojů	stroj	k1gInPc2	stroj
se	se	k3xPyFc4	se
např.	např.	kA	např.
od	od	k7c2	od
ofsetových	ofsetový	k2eAgMnPc2d1	ofsetový
příliš	příliš	k6eAd1	příliš
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
konstrukci	konstrukce	k1gFnSc6	konstrukce
tiskové	tiskový	k2eAgFnSc2d1	tisková
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
tlakového	tlakový	k2eAgInSc2d1	tlakový
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
volně	volně	k6eAd1	volně
zavěšeného	zavěšený	k2eAgInSc2d1	zavěšený
kovového	kovový	k2eAgInSc2d1	kovový
válce	válec	k1gInSc2	válec
<g/>
,	,	kIx,	,
formového	formový	k2eAgInSc2d1	formový
válce	válec	k1gInSc2	válec
<g/>
,	,	kIx,	,
stěrače	stěrač	k1gInSc2	stěrač
a	a	k8xC	a
barevníku	barevník	k1gInSc2	barevník
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
tisková	tiskový	k2eAgFnSc1d1	tisková
jednotka	jednotka	k1gFnSc1	jednotka
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
sušicí	sušicí	k2eAgNnSc4d1	sušicí
zařízení	zařízení	k1gNnSc4	zařízení
s	s	k7c7	s
intenzivním	intenzivní	k2eAgNnSc7d1	intenzivní
odsáváním	odsávání	k1gNnSc7	odsávání
nasyceného	nasycený	k2eAgInSc2d1	nasycený
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Odsátý	odsátý	k2eAgInSc1d1	odsátý
vzduch	vzduch	k1gInSc1	vzduch
nasycený	nasycený	k2eAgInSc1d1	nasycený
odpařenými	odpařený	k2eAgNnPc7d1	odpařené
rozpouštědly	rozpouštědlo	k1gNnPc7	rozpouštědlo
je	být	k5eAaImIp3nS	být
veden	veden	k2eAgInSc1d1	veden
do	do	k7c2	do
rekuperačního	rekuperační	k2eAgNnSc2d1	rekuperační
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
zpětně	zpětně	k6eAd1	zpětně
získávají	získávat	k5eAaImIp3nP	získávat
rozpouštědla	rozpouštědlo	k1gNnPc1	rozpouštědlo
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
například	například	k6eAd1	například
pro	pro	k7c4	pro
vytápění	vytápění	k1gNnSc4	vytápění
tiskárny	tiskárna	k1gFnSc2	tiskárna
<g/>
.	.	kIx.	.
</s>
<s>
Barevníky	barevník	k1gInPc1	barevník
hlubotiskových	hlubotiskový	k2eAgInPc2d1	hlubotiskový
strojů	stroj	k1gInPc2	stroj
jsou	být	k5eAaImIp3nP	být
relativně	relativně	k6eAd1	relativně
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
nízkou	nízký	k2eAgFnSc4d1	nízká
viskozitu	viskozita	k1gFnSc4	viskozita
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
ji	on	k3xPp3gFnSc4	on
třeba	třeba	k6eAd1	třeba
roztírat	roztírat	k5eAaImF	roztírat
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
ponorný	ponorný	k2eAgInSc4d1	ponorný
navalovací	navalovací	k2eAgInSc4d1	navalovací
válec	válec	k1gInSc4	válec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
brodí	brodit	k5eAaImIp3nS	brodit
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
a	a	k8xC	a
tu	tu	k6eAd1	tu
nanáší	nanášet	k5eAaImIp3nS	nanášet
na	na	k7c4	na
válec	válec	k1gInSc4	válec
formový	formový	k2eAgInSc4d1	formový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
barvě	barva	k1gFnSc6	barva
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
částečně	částečně	k6eAd1	částečně
ponořen	ponořen	k2eAgMnSc1d1	ponořen
přímo	přímo	k6eAd1	přímo
válec	válec	k1gInSc1	válec
formový	formový	k2eAgInSc1d1	formový
a	a	k8xC	a
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
varianty	varianta	k1gFnPc1	varianta
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
na	na	k7c4	na
formový	formový	k2eAgInSc4d1	formový
válec	válec	k1gInSc4	válec
nastřikována	nastřikován	k2eAgFnSc1d1	nastřikován
tryskami	tryska	k1gFnPc7	tryska
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
barevníky	barevník	k1gInPc1	barevník
jsou	být	k5eAaImIp3nP	být
vybaveny	vybavit	k5eAaPmNgInP	vybavit
zásobníkem	zásobník	k1gInSc7	zásobník
s	s	k7c7	s
cirkulací	cirkulace	k1gFnSc7	cirkulace
a	a	k8xC	a
filtrací	filtrace	k1gFnSc7	filtrace
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
barevníky	barevník	k1gInPc1	barevník
se	s	k7c7	s
zásobníky	zásobník	k1gInPc7	zásobník
mobilní	mobilní	k2eAgFnSc2d1	mobilní
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
přemisťovat	přemisťovat	k5eAaImF	přemisťovat
mezi	mezi	k7c7	mezi
tiskovými	tiskový	k2eAgFnPc7d1	tisková
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
jednotky	jednotka	k1gFnSc2	jednotka
je	být	k5eAaImIp3nS	být
stěrač	stěrač	k1gInSc1	stěrač
(	(	kIx(	(
<g/>
rakle	rakle	k1gFnSc1	rakle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
většinou	většinou	k6eAd1	většinou
ocelový	ocelový	k2eAgInSc4d1	ocelový
nůž	nůž	k1gInSc4	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Který	který	k3yQgInSc1	který
stírá	stírat	k5eAaImIp3nS	stírat
přebytečnou	přebytečný	k2eAgFnSc4d1	přebytečná
barvu	barva	k1gFnSc4	barva
z	z	k7c2	z
netisknoucích	tisknoucí	k2eNgInPc2d1	tisknoucí
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Stěrač	stěrač	k1gInSc1	stěrač
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
postavení	postavení	k1gNnSc1	postavení
(	(	kIx(	(
<g/>
pozitivní	pozitivní	k2eAgMnSc1d1	pozitivní
<g/>
,	,	kIx,	,
negativní	negativní	k2eAgMnSc1d1	negativní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sklon	sklon	k1gInSc1	sklon
<g/>
,	,	kIx,	,
přítlak	přítlak	k1gInSc1	přítlak
a	a	k8xC	a
také	také	k9	také
boční	boční	k2eAgInSc4d1	boční
pohyb	pohyb	k1gInSc4	pohyb
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
stěrač	stěrač	k1gInSc1	stěrač
musí	muset	k5eAaImIp3nS	muset
vykonávat	vykonávat	k5eAaImF	vykonávat
<g/>
,	,	kIx,	,
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
množství	množství	k1gNnSc1	množství
nanášené	nanášený	k2eAgFnSc2d1	nanášená
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
i	i	k9	i
gradační	gradační	k2eAgFnPc4d1	gradační
hodnoty	hodnota	k1gFnPc4	hodnota
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
kontrast	kontrast	k1gInSc1	kontrast
<g/>
,	,	kIx,	,
ostrost	ostrost	k1gFnSc1	ostrost
<g/>
...	...	k?	...
<g/>
Stěrače	stěrač	k1gInPc1	stěrač
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
k	k	k7c3	k
formovému	formový	k2eAgInSc3d1	formový
válci	válec	k1gInSc3	válec
přistavovány	přistavovat	k5eAaImNgInP	přistavovat
pneumaticky	pneumaticky	k6eAd1	pneumaticky
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
přítlak	přítlak	k1gInSc1	přítlak
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
regulován	regulovat	k5eAaImNgInS	regulovat
pomocí	pomocí	k7c2	pomocí
stlačeného	stlačený	k2eAgInSc2d1	stlačený
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlubotisku	hlubotisk	k1gInSc6	hlubotisk
bývají	bývat	k5eAaImIp3nP	bývat
tiskové	tiskový	k2eAgFnPc1d1	tisková
jednotky	jednotka	k1gFnPc1	jednotka
řazeny	řazen	k2eAgFnPc1d1	řazena
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
tlakový	tlakový	k2eAgInSc1d1	tlakový
válec	válec	k1gInSc1	válec
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
ním	on	k3xPp3gInSc7	on
je	být	k5eAaImIp3nS	být
válec	válec	k1gInSc1	válec
formový	formový	k2eAgInSc1d1	formový
a	a	k8xC	a
barevník	barevník	k1gInSc1	barevník
je	být	k5eAaImIp3nS	být
úplně	úplně	k6eAd1	úplně
vespod	vespod	k6eAd1	vespod
<g/>
.	.	kIx.	.
</s>
<s>
Pás	pás	k1gInSc1	pás
papíru	papír	k1gInSc2	papír
je	být	k5eAaImIp3nS	být
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
tisku	tisk	k1gInSc6	tisk
veden	vést	k5eAaImNgMnS	vést
nad	nad	k7c4	nad
tiskovou	tiskový	k2eAgFnSc4d1	tisková
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vertikálně	vertikálně	k6eAd1	vertikálně
umístěné	umístěný	k2eAgNnSc4d1	umístěné
sušicí	sušicí	k2eAgNnSc4d1	sušicí
a	a	k8xC	a
odsávací	odsávací	k2eAgNnSc4d1	odsávací
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
sušení	sušení	k1gNnSc2	sušení
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
tiskové	tiskový	k2eAgFnPc1d1	tisková
věže	věž	k1gFnPc1	věž
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnPc1d1	vysoká
<g/>
,	,	kIx,	,
samotná	samotný	k2eAgFnSc1d1	samotná
tisková	tiskový	k2eAgFnSc1d1	tisková
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
však	však	k9	však
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Kotoučové	kotoučový	k2eAgInPc1d1	kotoučový
stroje	stroj	k1gInPc1	stroj
pak	pak	k6eAd1	pak
mají	mít	k5eAaImIp3nP	mít
samozřejmě	samozřejmě	k6eAd1	samozřejmě
odvíječe	odvíječ	k1gInSc2	odvíječ
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
s	s	k7c7	s
automatickou	automatický	k2eAgFnSc7d1	automatická
výměnou	výměna	k1gFnSc7	výměna
rolí	role	k1gFnPc2	role
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vtahovací	vtahovací	k2eAgFnSc2d1	vtahovací
jednotky	jednotka	k1gFnSc2	jednotka
atd.	atd.	kA	atd.
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
jiné	jiný	k2eAgInPc1d1	jiný
kotoučové	kotoučový	k2eAgInPc1d1	kotoučový
stroje	stroj	k1gInPc1	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
stroje	stroj	k1gInSc2	stroj
bývá	bývat	k5eAaImIp3nS	bývat
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
skládací	skládací	k2eAgInSc1d1	skládací
aparát	aparát	k1gInSc1	aparát
<g/>
,	,	kIx,	,
řezání	řezání	k1gNnSc1	řezání
na	na	k7c4	na
archy	arch	k1gInPc4	arch
či	či	k8xC	či
navíjení	navíjení	k1gNnSc4	navíjení
do	do	k7c2	do
role	role	k1gFnSc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
varianty	varianta	k1gFnPc1	varianta
se	se	k3xPyFc4	se
v	v	k7c6	v
hlubotisku	hlubotisk	k1gInSc6	hlubotisk
používají	používat	k5eAaImIp3nP	používat
–	–	k?	–
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
tištěném	tištěný	k2eAgInSc6d1	tištěný
sortimentu	sortiment	k1gInSc6	sortiment
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmostí	samozřejmost	k1gFnPc2	samozřejmost
jsou	být	k5eAaImIp3nP	být
elektronická	elektronický	k2eAgNnPc4d1	elektronické
zařízení	zařízení	k1gNnPc4	zařízení
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
vedení	vedení	k1gNnSc2	vedení
pásu	pás	k1gInSc2	pás
strojem	stroj	k1gInSc7	stroj
<g/>
,	,	kIx,	,
soutisku	soutisk	k1gInSc2	soutisk
a	a	k8xC	a
vybarvení	vybarvení	k1gNnSc2	vybarvení
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hlubotisk	hlubotisk	k1gInSc1	hlubotisk
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
