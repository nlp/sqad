<s>
Alfred	Alfred	k1gMnSc1
de	de	k?
Musset	Musset	k1gMnSc1
</s>
<s>
Alfred	Alfred	k1gMnSc1
de	de	k?
Musset	Musset	k1gMnSc1
Alfred	Alfred	k1gMnSc1
de	de	k?
Musset	Musset	k1gMnSc1
Narození	narození	k1gNnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc4
1810	#num#	k4
Paříž	Paříž	k1gFnSc1
Francie	Francie	k1gFnSc2
Francie	Francie	k1gFnSc2
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1857	#num#	k4
tamtéž	tamtéž	k6eAd1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
syfilis	syfilis	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Hřbitov	hřbitov	k1gInSc1
Pè	Pè	k1gFnSc2
Povolání	povolání	k1gNnSc2
</s>
<s>
dramatik	dramatik	k1gMnSc1
<g/>
,	,	kIx,
básník	básník	k1gMnSc1
Vzdělání	vzdělání	k1gNnSc2
</s>
<s>
Lyceum	lyceum	k1gNnSc1
Jindřicha	Jindřich	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Období	období	k1gNnSc1
</s>
<s>
1830	#num#	k4
<g/>
–	–	k?
<g/>
1840	#num#	k4
Žánr	žánr	k1gInSc1
</s>
<s>
drama	drama	k1gNnSc1
<g/>
,	,	kIx,
básně	báseň	k1gFnPc1
<g/>
,	,	kIx,
povídky	povídka	k1gFnPc1
Literární	literární	k2eAgFnPc1d1
hnutí	hnutí	k1gNnPc1
</s>
<s>
romantismus	romantismus	k1gInSc4
Významná	významný	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
</s>
<s>
LorenzaccioS	LorenzaccioS	k?
láskou	láska	k1gFnSc7
nejsou	být	k5eNaImIp3nP
žádné	žádný	k3yNgFnPc4
žertyZpověď	žertyZpovědit	k5eAaPmRp2nS
dítěte	dítě	k1gNnSc2
svého	svůj	k3xOyFgInSc2
věku	věk	k1gInSc2
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Řád	řád	k1gInSc1
čestné	čestný	k2eAgFnSc2d1
legie	legie	k1gFnSc2
<g/>
,	,	kIx,
člen	člen	k1gMnSc1
Francouzské	francouzský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
Partner	partner	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
George	George	k1gFnSc1
SandováLouise	SandováLouise	k1gFnSc1
Rosalie	Rosalie	k1gFnSc1
Allan-DespreauxLouise	Allan-DespreauxLouise	k1gFnSc1
ColetRachel	ColetRachel	k1gInSc4
FélixCaroline	FélixCarolin	k1gInSc5
JaubertAimée	JaubertAimée	k1gFnPc6
d	d	k?
<g/>
'	'	kIx"
Alton	Alton	k1gMnSc1
Rodiče	rodič	k1gMnSc2
</s>
<s>
Victor-Donatien	Victor-Donatien	k1gInSc4
de	de	k?
Musset-Pathay	Musset-Patha	k2eAgInPc4d1
Vlivy	vliv	k1gInPc4
</s>
<s>
Victor	Victor	k1gMnSc1
Hugo	Hugo	k1gMnSc1
<g/>
,	,	kIx,
George	George	k1gFnSc1
Gordon	Gordon	k1gMnSc1
Byron	Byron	k1gMnSc1
</s>
<s>
„	„	k?
<g/>
..	..	k?
<g/>
benjamín	benjamín	k1gMnSc1
...	...	k?
i	i	k9
"	"	kIx"
<g/>
enfant	enfant	k1gInSc1
terrible	terrible	k6eAd1
<g/>
"	"	kIx"
francouzského	francouzský	k2eAgInSc2d1
romantismu	romantismus	k1gInSc2
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
“	“	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
galerie	galerie	k1gFnSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
původní	původní	k2eAgInPc4d1
texty	text	k1gInPc4
na	na	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
citáty	citát	k1gInPc1
na	na	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Plné	plný	k2eAgInPc4d1
texty	text	k1gInPc4
děl	dělo	k1gNnPc2
na	na	k7c6
Projektu	projekt	k1gInSc6
Gutenberg	Gutenberg	k1gMnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Alfred	Alfred	k1gMnSc1
de	de	k?
Musset	Musset	k1gMnSc1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc4
1810	#num#	k4
Paříž	Paříž	k1gFnSc1
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
květen	květena	k1gFnPc2
1857	#num#	k4
Paříž	Paříž	k1gFnSc4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
francouzský	francouzský	k2eAgMnSc1d1
romantický	romantický	k2eAgMnSc1d1
básník	básník	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
dramatik	dramatik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Rodina	rodina	k1gFnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
ve	v	k7c4
staré	starý	k2eAgNnSc4d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
nepříliš	příliš	k6eNd1
bohaté	bohatý	k2eAgFnSc3d1
šlechtické	šlechtický	k2eAgFnSc3d1
rodině	rodina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
Victor-Donatien	Victor-Donatien	k1gMnSc1
de	de	k?
Musset-Pathay	Musset-Pathay	k1gMnSc1
(	(	kIx(
<g/>
1768	#num#	k4
<g/>
–	–	k?
<g/>
1832	#num#	k4
<g/>
)	)	kIx)
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
úředník	úředník	k1gMnSc1
na	na	k7c6
ministerstvu	ministerstvo	k1gNnSc6
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
toho	ten	k3xDgInSc2
se	se	k3xPyFc4
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
literatuře	literatura	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napsal	napsat	k5eAaPmAgInS,k5eAaBmAgInS
životopis	životopis	k1gInSc1
Jeana-Jacquese	Jeana-Jacquese	k1gFnSc1
Rousseau	Rousseau	k1gMnSc1
(	(	kIx(
<g/>
Histoire	Histoir	k1gInSc5
de	de	k?
la	la	k1gNnSc6
vie	vie	k?
et	et	k?
des	des	k1gNnPc2
ouvrages	ouvrages	k1gInSc1
de	de	k?
J.	J.	kA
<g/>
-	-	kIx~
<g/>
J.	J.	kA
Rousseau	Rousseau	k1gMnSc1
<g/>
,	,	kIx,
1821	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
uspořádal	uspořádat	k5eAaPmAgMnS
jeho	jeho	k3xOp3gInPc4
sebrané	sebraný	k2eAgInPc4d1
spisy	spis	k1gInPc4
(	(	kIx(
<g/>
Oeuvres	Oeuvres	k1gMnSc1
complè	complè	k1gMnSc1
de	de	k?
J.	J.	kA
<g/>
-	-	kIx~
<g/>
J.	J.	kA
Rousseau	Rousseau	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
děd	děd	k1gMnSc1
Joseph-Alexandre	Joseph-Alexandr	k1gInSc5
de	de	k?
Musset-Pathay	Musset-Pathay	k1gInPc1
se	se	k3xPyFc4
rovněž	rovněž	k9
zabýval	zabývat	k5eAaImAgMnS
literaturou	literatura	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
staršího	starý	k2eAgMnSc4d2
bratra	bratr	k1gMnSc4
Paula	Paul	k1gMnSc2
(	(	kIx(
<g/>
1804	#num#	k4
<g/>
–	–	k?
<g/>
1880	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
rovněž	rovněž	k6eAd1
spisovatelem	spisovatel	k1gMnSc7
<g/>
,	,	kIx,
a	a	k8xC
dvě	dva	k4xCgFnPc1
sestry	sestra	k1gFnPc1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
jedna	jeden	k4xCgFnSc1
zemřela	zemřít	k5eAaPmAgFnS
nedlouho	dlouho	k6eNd1
po	po	k7c6
narození	narození	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Studia	studio	k1gNnSc2
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS
Lyceum	lyceum	k1gNnSc4
Jindřicha	Jindřich	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
platil	platit	k5eAaImAgInS
za	za	k7c4
velice	velice	k6eAd1
nadaného	nadaný	k2eAgMnSc4d1
žáka	žák	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
přání	přání	k1gNnSc2
rodičů	rodič	k1gMnPc2
měl	mít	k5eAaImAgMnS
pokračovat	pokračovat	k5eAaImF
ve	v	k7c6
studiu	studio	k1gNnSc6
na	na	k7c6
École	Écolo	k1gNnSc6
polytechnique	polytechniqu	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
ale	ale	k9
začal	začít	k5eAaPmAgInS
studovat	studovat	k5eAaImF
práva	právo	k1gNnPc4
a	a	k8xC
lékařství	lékařství	k1gNnSc4
a	a	k8xC
posléze	posléze	k6eAd1
hudbu	hudba	k1gFnSc4
a	a	k8xC
kreslení	kreslení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádná	žádný	k3yNgNnPc4
studia	studio	k1gNnPc4
ale	ale	k8xC
nedokončil	dokončit	k5eNaPmAgMnS
a	a	k8xC
začal	začít	k5eAaPmAgMnS
se	se	k3xPyFc4
věnovat	věnovat	k5eAaImF,k5eAaPmF
literatuře	literatura	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
17	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
přivedl	přivést	k5eAaPmAgMnS
jej	on	k3xPp3gMnSc4
Paul	Paul	k1gMnSc1
Foucher	Fouchra	k1gFnPc2
do	do	k7c2
společnosti	společnost	k1gFnSc2
romantiků	romantik	k1gMnPc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Victorem	Victor	k1gMnSc7
Hugem	Hugo	k1gMnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
nazývána	nazývat	k5eAaImNgFnS
"	"	kIx"
<g/>
Le	Le	k1gMnPc7
petit-cénacle	petit-cénacle	k6eAd1
<g/>
"	"	kIx"
a	a	k8xC
scházela	scházet	k5eAaImAgFnS
se	se	k3xPyFc4
u	u	k7c2
básníka	básník	k1gMnSc2
Charlesa	Charlesa	k?
Nodiera	Nodier	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
ovšem	ovšem	k9
k	k	k7c3
romantikům	romantik	k1gMnPc3
nepočítal	počítat	k5eNaImAgMnS
<g/>
,	,	kIx,
dokonce	dokonce	k9
prohlašoval	prohlašovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
knihovně	knihovna	k1gFnSc6
klidně	klidně	k6eAd1
sousedí	sousedit	k5eAaImIp3nS
Shakespeare	Shakespeare	k1gMnSc1
s	s	k7c7
Racinem	Racin	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
nikdy	nikdy	k6eAd1
–	–	k?
ač	ač	k8xS
se	se	k3xPyFc4
pohyboval	pohybovat	k5eAaImAgInS
v	v	k7c6
okruhu	okruh	k1gInSc6
Victora	Victor	k1gMnSc2
Huga	Hugo	k1gMnSc2
–	–	k?
nebyl	být	k5eNaImAgMnS
bojovným	bojovný	k2eAgMnSc7d1
straníkem	straník	k1gMnSc7
romantismu	romantismus	k1gInSc2
a	a	k8xC
nikdy	nikdy	k6eAd1
do	do	k7c2
“	“	k?
<g/>
války	válka	k1gFnSc2
romantiků	romantik	k1gMnPc2
<g/>
"	"	kIx"
aktivně	aktivně	k6eAd1
nevstoupil	vstoupit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zachovával	zachovávat	k5eAaImAgInS
důsledně	důsledně	k6eAd1
postoj	postoj	k1gInSc1
blazeovaného	blazeovaný	k2eAgMnSc2d1
mondénního	mondénní	k2eAgMnSc2d1
světáka	světák	k1gMnSc2
<g/>
,	,	kIx,
povzneseného	povznesený	k2eAgMnSc2d1
nad	nad	k7c4
tuto	tento	k3xDgFnSc4
vřavu	vřava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1832	#num#	k4
se	se	k3xPyFc4
dokonce	dokonce	k9
s	s	k7c7
Hugovým	Hugův	k2eAgInSc7d1
kruhem	kruh	k1gInSc7
zcela	zcela	k6eAd1
rozešel	rozejít	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
díle	dílo	k1gNnSc6
(	(	kIx(
<g/>
včetně	včetně	k7c2
zkušeností	zkušenost	k1gFnPc2
z	z	k7c2
milostných	milostný	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
se	s	k7c7
spisovatelkou	spisovatelka	k1gFnSc7
George	Georg	k1gMnSc2
Sandovou	Sandův	k2eAgFnSc7d1
a	a	k8xC
slavnou	slavný	k2eAgFnSc7d1
tragédkou	tragédka	k1gFnSc7
Rachel	Rachel	k1gMnSc1
<g/>
)	)	kIx)
uložil	uložit	k5eAaPmAgInS
vlastní	vlastní	k2eAgFnSc4d1
životní	životní	k2eAgFnSc4d1
reflexi	reflexe	k1gFnSc4
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
o	o	k7c6
níž	jenž	k3xRgFnSc6
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
v	v	k7c6
prozaickém	prozaický	k2eAgInSc6d1
díle	díl	k1gInSc6
Zpověď	zpověď	k1gFnSc4
dítěte	dítě	k1gNnSc2
svého	svůj	k3xOyFgInSc2
věku	věk	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
“	“	k?
<g/>
staré	staré	k1gNnSc1
už	už	k9
nebylo	být	k5eNaImAgNnS
a	a	k8xC
nové	nový	k2eAgNnSc1d1
ještě	ještě	k9
nebylo	být	k5eNaImAgNnS
<g/>
"	"	kIx"
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
níž	jenž	k3xRgFnSc6
–	–	k?
opět	opět	k6eAd1
podle	podle	k7c2
jeho	jeho	k3xOp3gNnPc2
slov	slovo	k1gNnPc2
–	–	k?
“	“	k?
<g/>
lidé	člověk	k1gMnPc1
trpěli	trpět	k5eAaImAgMnP
podivnou	podivný	k2eAgFnSc7d1
mravní	mravní	k2eAgFnSc7d1
chorobou	choroba	k1gFnSc7
<g/>
:	:	kIx,
nevírou	nevíra	k1gFnSc7
v	v	k7c6
cokoliv	cokoliv	k3yInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Psát	psát	k5eAaImF
dramata	drama	k1gNnPc4
začal	začít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1830	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gFnSc1
první	první	k4xOgFnSc1
hra	hra	k1gFnSc1
Benátská	benátský	k2eAgFnSc1d1
noc	noc	k1gFnSc1
zcela	zcela	k6eAd1
propadla	propadnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
osobním	osobní	k2eAgInSc6d1
rozchodu	rozchod	k1gInSc6
s	s	k7c7
Hugovým	Hugův	k2eAgInSc7d1
kruhem	kruh	k1gInSc7
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gNnSc7
rozešel	rozejít	k5eAaPmAgMnS
i	i	k9
v	v	k7c6
pojetí	pojetí	k1gNnSc6
dramatu	drama	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
veselohru	veselohra	k1gFnSc4
O	o	k7c6
čem	co	k3yQnSc6,k3yRnSc6,k3yInSc6
snívají	snívat	k5eAaImIp3nP
dívky	dívka	k1gFnPc4
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yQgMnPc4,k3yRgMnPc4,k3yIgMnPc4
byl	být	k5eAaImAgInS
patrný	patrný	k2eAgInSc1d1
vliv	vliv	k1gInSc1
Shakespearova	Shakespearův	k2eAgInSc2d1
Snu	sen	k1gInSc2
noci	noc	k1gFnSc2
svatojánské	svatojánský	k2eAgFnSc2d1
<g/>
,	,	kIx,
a	a	k8xC
proverb	proverb	k1gInSc1
Číše	číš	k1gFnSc2
a	a	k8xC
rty	ret	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proverb	proverb	k1gInSc1
byl	být	k5eAaImAgInS
žánr	žánr	k1gInSc1
oblíbený	oblíbený	k2eAgInSc1d1
v	v	k7c6
šlechtických	šlechtický	k2eAgInPc6d1
salónech	salón	k1gInPc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilustroval	ilustrovat	k5eAaBmAgMnS
platnost	platnost	k1gFnSc4
nějakého	nějaký	k3yIgNnSc2
pořekadla	pořekadlo	k1gNnSc2
či	či	k8xC
přísloví	přísloví	k1gNnSc2
a	a	k8xC
na	na	k7c6
konci	konec	k1gInSc6
představení	představení	k1gNnSc2
měli	mít	k5eAaImAgMnP
diváci	divák	k1gMnPc1
znění	znění	k1gNnSc2
těchto	tento	k3xDgFnPc2
“	“	k?
<g/>
předloh	předloha	k1gFnPc2
<g/>
"	"	kIx"
uhádnout	uhádnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
zřetelně	zřetelně	k6eAd1
svědčí	svědčit	k5eAaImIp3nS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
Musset	Musset	k1gMnSc1
skutečně	skutečně	k6eAd1
nesdílel	sdílet	k5eNaImAgMnS
razantní	razantní	k2eAgInSc4d1
odpor	odpor	k1gInSc4
“	“	k?
<g/>
hugovského	hugovský	k2eAgMnSc2d1
<g/>
"	"	kIx"
romantismu	romantismus	k1gInSc2
proti	proti	k7c3
tradicím	tradice	k1gFnPc3
klasicismu	klasicismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Musset	Musset	k1gInSc1
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
“	“	k?
<g/>
proverbech	proverb	k1gInPc6
<g/>
"	"	kIx"
navázal	navázat	k5eAaPmAgInS
také	také	k9
na	na	k7c4
tradici	tradice	k1gFnSc4
francouzských	francouzský	k2eAgFnPc2d1
konverzačních	konverzační	k2eAgFnPc2d1
veseloher	veselohra	k1gFnPc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
vytvořil	vytvořit	k5eAaPmAgMnS
jakoby	jakoby	k8xS
lehounkou	lehounký	k2eAgFnSc4d1
komedii	komedie	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
ovšem	ovšem	k9
nepostrádá	postrádat	k5eNaImIp3nS
ani	ani	k8xC
hořké	hořký	k2eAgInPc1d1
tóny	tón	k1gInPc1
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
mohou	moct	k5eAaImIp3nP
přerůst	přerůst	k5eAaPmF
až	až	k9
v	v	k7c4
tragické	tragický	k2eAgNnSc4d1
vyústění	vyústění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámější	známý	k2eAgInSc1d3
z	z	k7c2
těchto	tento	k3xDgInPc2
proverbů	proverb	k1gInPc2
je	být	k5eAaImIp3nS
dramatický	dramatický	k2eAgInSc4d1
text	text	k1gInSc4
S	s	k7c7
láskou	láska	k1gFnSc7
nejsou	být	k5eNaImIp3nP
žádné	žádný	k3yNgInPc1
žerty	žert	k1gInPc1
(	(	kIx(
<g/>
přeloženo	přeložen	k2eAgNnSc4d1
do	do	k7c2
češtiny	čeština	k1gFnSc2
též	též	k9
jako	jako	k9
Se	s	k7c7
srdcem	srdce	k1gNnSc7
divno	divno	k6eAd1
hrát	hrát	k5eAaImF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Musset	Musset	k1gMnSc1
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
tento	tento	k3xDgInSc4
proverb	proverb	k1gInSc4
<g/>
,	,	kIx,
vzniklo	vzniknout	k5eAaPmAgNnS
i	i	k9
jedno	jeden	k4xCgNnSc1
z	z	k7c2
velkých	velký	k2eAgNnPc2d1
historických	historický	k2eAgNnPc2d1
dramat	drama	k1gNnPc2
francouzské	francouzský	k2eAgFnSc2d1
a	a	k8xC
světové	světový	k2eAgFnSc2d1
dramatiky	dramatika	k1gFnSc2
–	–	k?
Lorenzaccio	Lorenzaccio	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
(	(	kIx(
<g/>
1833	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
začíná	začínat	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc1
vztah	vztah	k1gInSc1
s	s	k7c7
George	George	k1gFnSc7
Sandovou	Sandová	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
vztah	vztah	k1gInSc4
ovlivnil	ovlivnit	k5eAaPmAgMnS
některá	některý	k3yIgNnPc4
Mussetova	Mussetův	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
a	a	k8xC
skončil	skončit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1835	#num#	k4
rozchodem	rozchod	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Musseta	Musset	k1gMnSc2
rozchod	rozchod	k1gInSc4
zdrtil	zdrtit	k5eAaPmAgMnS
<g/>
,	,	kIx,
snažil	snažit	k5eAaImAgMnS
se	se	k3xPyFc4
najít	najít	k5eAaPmF
zapomnění	zapomnění	k1gNnSc4
v	v	k7c6
bohémském	bohémský	k2eAgNnSc6d1
hýření	hýření	k1gNnSc6
<g/>
,	,	kIx,
cestování	cestování	k1gNnSc6
i	i	k8xC
ve	v	k7c6
vlastní	vlastní	k2eAgFnSc6d1
tvorbě	tvorba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
byl	být	k5eAaImAgInS
román	román	k1gInSc1
Zpověď	zpověď	k1gFnSc4
dítěte	dítě	k1gNnSc2
svého	svůj	k3xOyFgInSc2
věku	věk	k1gInSc2
(	(	kIx(
<g/>
1836	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1837	#num#	k4
se	se	k3xPyFc4
seznámil	seznámit	k5eAaPmAgMnS
s	s	k7c7
Aimée	Aimée	k1gFnSc7
d	d	k?
<g/>
'	'	kIx"
<g/>
Alton	Alton	k1gMnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc4
snažila	snažit	k5eAaImAgFnS
přimět	přimět	k5eAaPmF
k	k	k7c3
opuštění	opuštění	k1gNnSc3
bohémského	bohémský	k2eAgInSc2d1
života	život	k1gInSc2
a	a	k8xC
k	k	k7c3
soustředěné	soustředěný	k2eAgFnSc3d1
práci	práce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
odráží	odrážet	k5eAaImIp3nS
například	například	k6eAd1
v	v	k7c6
optimismu	optimismus	k1gInSc6
básně	báseň	k1gFnSc2
Noc	noc	k1gFnSc1
říjnová	říjnový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
ale	ale	k8xC
rezignovala	rezignovat	k5eAaBmAgFnS
a	a	k8xC
vzala	vzít	k5eAaPmAgFnS
si	se	k3xPyFc3
za	za	k7c7
manžela	manžel	k1gMnSc4
jeho	on	k3xPp3gMnSc4,k3xOp3gMnSc4
bratra	bratr	k1gMnSc4
Paula	Paul	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Musset	Musset	k1gMnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
opět	opět	k6eAd1
stal	stát	k5eAaPmAgMnS
hýřilem	hýřil	k1gMnSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
oslabilo	oslabit	k5eAaPmAgNnS
jeho	jeho	k3xOp3gNnSc1
zdraví	zdraví	k1gNnSc1
i	i	k8xC
tvůrčí	tvůrčí	k2eAgFnPc1d1
síly	síla	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Psal	psát	k5eAaImAgMnS
často	často	k6eAd1
již	již	k6eAd1
jen	jen	k9
z	z	k7c2
existenčních	existenční	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Byl	být	k5eAaImAgInS
pochován	pochovat	k5eAaPmNgInS
na	na	k7c6
pařížském	pařížský	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
Pè	Pè	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
socha	socha	k1gFnSc1
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
146	#num#	k4
soch	socha	k1gFnPc2
umístěných	umístěný	k2eAgFnPc2d1
na	na	k7c6
fasádě	fasáda	k1gFnSc6
budovy	budova	k1gFnSc2
Hôtel	Hôtel	k1gInSc1
de	de	k?
ville	ville	k1gFnSc2
de	de	k?
Paris	Paris	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Plakát	plakát	k1gInSc1
Alfonse	Alfons	k1gMnSc2
Muchy	Mucha	k1gMnSc2
k	k	k7c3
prvnímu	první	k4xOgNnSc3
uvedení	uvedení	k1gNnSc6
hry	hra	k1gFnSc2
Lorenzaccio	Lorenzaccio	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1896	#num#	k4
v	v	k7c6
divadle	divadlo	k1gNnSc6
Sarah	Sarah	k1gFnSc2
Bernhardt	Bernhardta	k1gFnPc2
</s>
<s>
Povídky	povídka	k1gFnPc1
ze	z	k7c2
Španěl	Španěly	k1gInPc2
a	a	k8xC
Itálie	Itálie	k1gFnSc2
(	(	kIx(
<g/>
Contes	Contes	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Espagne	Espagn	k1gInSc5
et	et	k?
d	d	k?
<g/>
'	'	kIx"
<g/>
Italie	Italie	k1gFnSc2
<g/>
,	,	kIx,
1830	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
sbírka	sbírka	k1gFnSc1
básnických	básnický	k2eAgFnPc2d1
povídek	povídka	k1gFnPc2
(	(	kIx(
<g/>
Don	Don	k1gMnSc1
Paez	Paez	k1gMnSc1
<g/>
,	,	kIx,
Portia	Portia	k1gFnSc1
<g/>
,	,	kIx,
Mardoche	Mardoche	k1gFnSc1
<g/>
)	)	kIx)
i	i	k8xC
kratších	krátký	k2eAgFnPc2d2
básní	báseň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náměty	námět	k1gInPc1
i	i	k8xC
zpracování	zpracování	k1gNnSc1
je	být	k5eAaImIp3nS
typicky	typicky	k6eAd1
romantické	romantický	k2eAgFnPc4d1
<g/>
,	,	kIx,
především	především	k9
pod	pod	k7c7
vlivem	vliv	k1gInSc7
Byrona	Byron	k1gMnSc2
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
například	například	k6eAd1
báseň	báseň	k1gFnSc1
Ballade	Ballad	k1gInSc5
à	à	k?
la	la	k1gNnSc3
lune	lun	k1gFnSc2
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
ironickou	ironický	k2eAgFnSc4d1
parodii	parodie	k1gFnSc4
romantického	romantický	k2eAgNnSc2d1
básnictví	básnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Benátská	benátský	k2eAgFnSc1d1
noc	noc	k1gFnSc1
(	(	kIx(
<g/>
Une	Une	k1gMnSc1
nuit	nuit	k1gMnSc1
vénitienne	vénitiennout	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
,	,	kIx,
1830	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
dramatická	dramatický	k2eAgFnSc1d1
prvotina	prvotina	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
většiny	většina	k1gFnSc2
romantických	romantický	k2eAgFnPc2d1
her	hra	k1gFnPc2
nevítězí	vítězit	k5eNaImIp3nS
čistá	čistý	k2eAgFnSc1d1
láska	láska	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
peníze	peníz	k1gInPc1
cizího	cizí	k2eAgMnSc2d1
prince	princ	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
u	u	k7c2
publika	publikum	k1gNnSc2
propadla	propadnout	k5eAaPmAgFnS
a	a	k8xC
vedla	vést	k5eAaImAgFnS
Musseta	Musset	k5eAaPmNgFnS
k	k	k7c3
rozhodnutí	rozhodnutí	k1gNnSc3
napříště	napříště	k6eAd1
psát	psát	k5eAaImF
pouze	pouze	k6eAd1
podle	podle	k7c2
vlastního	vlastní	k2eAgNnSc2d1
přesvědčení	přesvědčení	k1gNnSc2
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
požadavky	požadavek	k1gInPc4
divadel	divadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
čem	co	k3yRnSc6,k3yQnSc6,k3yInSc6
snívají	snívat	k5eAaImIp3nP
dívky	dívka	k1gFnPc4
(	(	kIx(
<g/>
À	À	k?
quoi	quoi	k6eAd1
rê	rê	k1gInSc1
les	les	k1gInSc1
jeunes	jeunesa	k1gFnPc2
filles	fillesa	k1gFnPc2
<g/>
,	,	kIx,
1832	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marianniny	Mariannin	k2eAgFnPc1d1
rozmary	rozmara	k1gFnPc1
(	(	kIx(
<g/>
Les	les	k1gInSc1
Caprices	Caprices	k1gInSc1
de	de	k?
Marianne	Mariann	k1gMnSc5
<g/>
,	,	kIx,
1833	#num#	k4
<g/>
)	)	kIx)
–	–	k?
příběh	příběh	k1gInSc1
dvou	dva	k4xCgMnPc2
přátel	přítel	k1gMnPc2
protikladných	protikladný	k2eAgFnPc2d1
povah	povaha	k1gFnPc2
<g/>
,	,	kIx,
cynického	cynický	k2eAgInSc2d1
a	a	k8xC
lehkovážného	lehkovážný	k2eAgInSc2d1
Oktáva	oktáva	k1gFnSc1
a	a	k8xC
ryzího	ryzí	k2eAgMnSc2d1
a	a	k8xC
čistého	čistý	k2eAgNnSc2d1
Coelia	Coelium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vinou	vinou	k7c2
hříček	hříčka	k1gFnPc2
Oktáva	oktáva	k1gFnSc1
s	s	k7c7
mladou	mladý	k2eAgFnSc7d1
a	a	k8xC
krásnou	krásný	k2eAgFnSc7d1
vdanou	vdaný	k2eAgFnSc7d1
ženou	žena	k1gFnSc7
Marianou	Mariana	k1gFnSc7
je	být	k5eAaImIp3nS
Coelio	Coelio	k1gMnSc1
zabit	zabít	k5eAaPmNgMnS
nájemným	nájemný	k2eAgMnSc7d1
vrahem	vrah	k1gMnSc7
<g/>
,	,	kIx,
kterého	který	k3yIgMnSc4,k3yQgMnSc4,k3yRgMnSc4
poslal	poslat	k5eAaPmAgMnS
Marianin	Marianin	k2eAgMnSc1d1
manžel	manžel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Rolla	Rolla	k1gFnSc1
(	(	kIx(
<g/>
1833	#num#	k4
<g/>
)	)	kIx)
–	–	k?
mladý	mladý	k2eAgMnSc1d1
aristokrat	aristokrat	k1gMnSc1
prohýří	prohýřit	k5eAaPmIp3nS
své	svůj	k3xOyFgNnSc4
dědictví	dědictví	k1gNnSc4
a	a	k8xC
protože	protože	k8xS
považuje	považovat	k5eAaImIp3nS
další	další	k2eAgInSc4d1
život	život	k1gInSc4
za	za	k7c4
bezvýchodný	bezvýchodný	k2eAgInSc4d1
<g/>
,	,	kIx,
spáchá	spáchat	k5eAaPmIp3nS
sebevraždu	sebevražda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
láskou	láska	k1gFnSc7
nejsou	být	k5eNaImIp3nP
žádné	žádný	k3yNgInPc1
žerty	žert	k1gInPc1
<g/>
,	,	kIx,
v	v	k7c6
jiném	jiný	k2eAgInSc6d1
překladu	překlad	k1gInSc6
Se	s	k7c7
srdcem	srdce	k1gNnSc7
divno	divno	k6eAd1
hrát	hrát	k5eAaImF
<g/>
,	,	kIx,
Neradno	Neradno	k1gNnSc1
s	s	k7c7
láskou	láska	k1gFnSc7
zahrávat	zahrávat	k5eAaImF
si	se	k3xPyFc3
nebo	nebo	k8xC
S	s	k7c7
láskou	láska	k1gFnSc7
nelze	lze	k6eNd1
žertovat	žertovat	k5eAaImF
(	(	kIx(
<g/>
On	on	k3xPp3gMnSc1
ne	ne	k9
badine	badinout	k5eAaPmIp3nS
pas	pas	k1gInSc4
avec	avec	k1gInSc1
l	l	kA
<g/>
’	’	k?
<g/>
amour	amour	k1gMnSc1
<g/>
,	,	kIx,
1834	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
v	v	k7c6
této	tento	k3xDgFnSc6
hře	hra	k1gFnSc6
vlastně	vlastně	k9
Musset	Musset	k1gInSc1
dokonale	dokonale	k6eAd1
realizoval	realizovat	k5eAaBmAgInS
hugovsky	hugovsky	k6eAd1
romantický	romantický	k2eAgInSc1d1
program	program	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohatě	bohatě	k6eAd1
strukturovaná	strukturovaný	k2eAgFnSc1d1
hra	hra	k1gFnSc1
má	mít	k5eAaImIp3nS
několik	několik	k4yIc4
odlišných	odlišný	k2eAgFnPc2d1
vrstev	vrstva	k1gFnPc2
<g/>
:	:	kIx,
situační	situační	k2eAgFnSc4d1
komiku	komika	k1gFnSc4
<g/>
,	,	kIx,
rabelaisovský	rabelaisovský	k2eAgInSc4d1
drsný	drsný	k2eAgInSc4d1
a	a	k8xC
výsměšný	výsměšný	k2eAgInSc4d1
humor	humor	k1gInSc4
<g/>
,	,	kIx,
vtipný	vtipný	k2eAgInSc4d1
<g/>
,	,	kIx,
půvabný	půvabný	k2eAgInSc4d1
<g/>
,	,	kIx,
duchaplný	duchaplný	k2eAgInSc4d1
dialog	dialog	k1gInSc4
konverzační	konverzační	k2eAgFnSc2d1
komedie	komedie	k1gFnSc2
<g/>
,	,	kIx,
intelektuální	intelektuální	k2eAgFnSc4d1
hru	hra	k1gFnSc4
s	s	k7c7
pojmy	pojem	k1gInPc7
a	a	k8xC
slovy	slovo	k1gNnPc7
<g/>
,	,	kIx,
shakespearovské	shakespearovský	k2eAgNnSc4d1
vedení	vedení	k1gNnSc4
postav	postava	k1gFnPc2
<g/>
,	,	kIx,
od	od	k7c2
nichž	jenž	k3xRgFnPc2
Musset	Musseta	k1gFnPc2
často	často	k6eAd1
jako	jako	k9
by	by	kYmCp3nS
odstupoval	odstupovat	k5eAaImAgMnS
a	a	k8xC
předváděl	předvádět	k5eAaImAgMnS
je	být	k5eAaImIp3nS
i	i	k9
z	z	k7c2
jejich	jejich	k3xOp3gFnSc2
směšné	směšný	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
tento	tento	k3xDgInSc1
příběh	příběh	k1gInSc1
má	mít	k5eAaImIp3nS
tuto	tento	k3xDgFnSc4
mnohovrstevnatost	mnohovrstevnatost	k1gFnSc4
–	–	k?
láska	láska	k1gFnSc1
Kamily	Kamila	k1gFnSc2
a	a	k8xC
Perdicana	Perdicana	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
v	v	k7c6
poloze	poloha	k1gFnSc6
nezávazné	závazný	k2eNgFnSc2d1
lehkosti	lehkost	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
proměnila	proměnit	k5eAaPmAgFnS
v	v	k7c6
tragédii	tragédie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladý	mladý	k1gMnSc1
šlechtic	šlechtic	k1gMnSc1
Perdican	Perdican	k1gMnSc1
se	se	k3xPyFc4
dvoří	dvořit	k5eAaImIp3nS
své	svůj	k3xOyFgFnSc3
přítelkyni	přítelkyně	k1gFnSc3
z	z	k7c2
dětství	dětství	k1gNnSc2
Kamile	Kamila	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
ale	ale	k9
vlivem	vlivem	k7c2
klášterní	klášterní	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
dvoření	dvoření	k1gNnSc3
odmítá	odmítat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
vzbudil	vzbudit	k5eAaPmAgInS
Kamilinu	Kamilin	k2eAgFnSc4d1
žárlivost	žárlivost	k1gFnSc4
<g/>
,	,	kIx,
začne	začít	k5eAaPmIp3nS
se	se	k3xPyFc4
Perdican	Perdican	k1gMnSc1
dvořit	dvořit	k5eAaImF
venkovské	venkovský	k2eAgFnSc3d1
dívce	dívka	k1gFnSc3
Růženě	Růžena	k1gFnSc3
(	(	kIx(
<g/>
Rosette	Rosett	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kamila	Kamila	k1gFnSc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
obměkčí	obměkčit	k5eAaPmIp3nP
a	a	k8xC
vyznívají	vyznívat	k5eAaImIp3nP
si	se	k3xPyFc3
s	s	k7c7
Perdicanem	Perdican	k1gInSc7
lásku	láska	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Růžena	Růžena	k1gFnSc1
nedokáže	dokázat	k5eNaPmIp3nS
tento	tento	k3xDgInSc4
zvrat	zvrat	k1gInSc4
pochopit	pochopit	k5eAaPmF
a	a	k8xC
končí	končit	k5eAaImIp3nS
tragicky	tragicky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
je	být	k5eAaImIp3nS
ovlivněna	ovlivnit	k5eAaPmNgFnS
vztahem	vztah	k1gInSc7
s	s	k7c7
G.	G.	kA
Sandovou	Sandová	k1gFnSc4
včetně	včetně	k7c2
citací	citace	k1gFnPc2
z	z	k7c2
jejích	její	k3xOp3gInPc2
dopisů	dopis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Fantasio	Fantasio	k1gNnSc1
(	(	kIx(
<g/>
1834	#num#	k4
<g/>
)	)	kIx)
–	–	k?
příběh	příběh	k1gInSc1
šaška	šašek	k1gMnSc2
na	na	k7c6
královském	královský	k2eAgInSc6d1
dvoře	dvůr	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
zabránit	zabránit	k5eAaPmF
svatbě	svatba	k1gFnSc3
princezny	princezna	k1gFnSc2
s	s	k7c7
nemilovaným	milovaný	k2eNgMnSc7d1
princem	princ	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Lorenzaccio	Lorenzaccio	k1gNnSc1
(	(	kIx(
<g/>
1834	#num#	k4
<g/>
)	)	kIx)
Pro	pro	k7c4
námět	námět	k1gInSc4
–	–	k?
jejž	jenž	k3xRgInSc4
mu	on	k3xPp3gMnSc3
zprostředkovala	zprostředkovat	k5eAaPmAgFnS
George	George	k1gFnSc4
Sandová	Sandová	k1gFnSc1
–	–	k?
sáhl	sáhnout	k5eAaPmAgMnS
Musset	Musset	k1gInSc4
do	do	k7c2
renesanční	renesanční	k2eAgFnSc2d1
Florencie	Florencie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládne	vládnout	k5eAaImIp3nS
tam	tam	k6eAd1
tyranský	tyranský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
příbuzný	příbuzný	k2eAgInSc1d1
Lorenzo	Lorenza	k1gFnSc5
si	se	k3xPyFc3
nasadí	nasadit	k5eAaPmIp3nP
masku	maska	k1gFnSc4
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
přítele	přítel	k1gMnSc2
<g/>
,	,	kIx,
veselého	veselý	k2eAgInSc2d1
kumpána	kumpána	k?
<g/>
,	,	kIx,
společníka	společník	k1gMnSc2
prohýřených	prohýřený	k2eAgFnPc2d1
nocí	noc	k1gFnPc2
<g/>
,	,	kIx,
prostopášníka	prostopášník	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
za	za	k7c4
ni	on	k3xPp3gFnSc4
skryl	skrýt	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
záměr	záměr	k1gInSc4
zbavit	zbavit	k5eAaPmF
Florencii	Florencie	k1gFnSc4
tyrana	tyran	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
nemravný	mravný	k2eNgInSc4d1
život	život	k1gInSc4
je	on	k3xPp3gMnPc4
dvořany	dvořan	k1gMnPc4
hanlivě	hanlivě	k6eAd1
přezdíván	přezdíván	k2eAgMnSc1d1
Lorenzaccio	Lorenzaccio	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jenže	jenže	k8xC
maska	maska	k1gFnSc1
se	se	k3xPyFc4
pomalu	pomalu	k6eAd1
změní	změnit	k5eAaPmIp3nS
v	v	k7c4
pravou	pravý	k2eAgFnSc4d1
tvář	tvář	k1gFnSc4
<g/>
,	,	kIx,
původní	původní	k2eAgInSc4d1
cíl	cíl	k1gInSc4
a	a	k8xC
smysl	smysl	k1gInSc4
jednání	jednání	k1gNnSc2
se	se	k3xPyFc4
ztrácí	ztrácet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jen	jen	k9
setrvačnost	setrvačnost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
nutí	nutit	k5eAaImIp3nS
Lorenzaccia	Lorenzaccia	k1gFnSc1
dojít	dojít	k5eAaPmF
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Učiní	učinit	k5eAaImIp3nS,k5eAaPmIp3nS
to	ten	k3xDgNnSc4
–	–	k?
ale	ale	k8xC
na	na	k7c4
víc	hodně	k6eAd2
už	už	k6eAd1
nemá	mít	k5eNaImIp3nS
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nic	nic	k3yNnSc1
se	se	k3xPyFc4
nezměnilo	změnit	k5eNaPmAgNnS
<g/>
,	,	kIx,
Měšťané	měšťan	k1gMnPc1
raději	rád	k6eAd2
zvolí	zvolit	k5eAaPmIp3nP
nového	nový	k2eAgMnSc4d1
tyrana	tyran	k1gMnSc4
<g/>
,	,	kIx,
než	než	k8xS
aby	aby	kYmCp3nP
se	se	k3xPyFc4
chopili	chopit	k5eAaPmAgMnP
nabyté	nabytý	k2eAgFnPc4d1
svobody	svoboda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lorenzaccio	Lorenzaccio	k6eAd1
uprchne	uprchnout	k5eAaPmIp3nS
a	a	k8xC
jen	jen	k9
čeká	čekat	k5eAaImIp3nS
<g/>
,	,	kIx,
až	až	k8xS
bude	být	k5eAaImBp3nS
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
čin	čin	k1gInSc4
zatčen	zatknout	k5eAaPmNgMnS
a	a	k8xC
potrestán	potrestat	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
příběh	příběh	k1gInSc1
prázdnoty	prázdnota	k1gFnSc2
člověka	člověk	k1gMnSc2
<g/>
,	,	kIx,
rozpadu	rozpad	k1gInSc3
osobnosti	osobnost	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
přestala	přestat	k5eAaPmAgFnS
věřit	věřit	k5eAaImF
v	v	k7c4
jakékoliv	jakýkoliv	k3yIgFnPc4
hodnoty	hodnota	k1gFnPc4
a	a	k8xC
jíž	jenž	k3xRgFnSc7
se	se	k3xPyFc4
původně	původně	k6eAd1
dobře	dobře	k6eAd1
míněné	míněný	k2eAgInPc1d1
prostředky	prostředek	k1gInPc1
k	k	k7c3
dosažení	dosažení	k1gNnSc3
cíle	cíl	k1gInSc2
změnily	změnit	k5eAaPmAgFnP
ve	v	k7c4
smysl	smysl	k1gInSc4
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Musset	Musset	k1gMnSc1
se	se	k3xPyFc4
do	do	k7c2
svých	svůj	k3xOyFgNnPc2
dramat	drama	k1gNnPc2
často	často	k6eAd1
projektoval	projektovat	k5eAaBmAgInS
<g/>
,	,	kIx,
nasazoval	nasazovat	k5eAaImAgMnS
si	se	k3xPyFc3
tváře	tvář	k1gFnPc4
svých	svůj	k3xOyFgFnPc2
postav	postava	k1gFnPc2
a	a	k8xC
jako	jako	k8xC,k8xS
pravý	pravý	k2eAgMnSc1d1
romantik	romantik	k1gMnSc1
vyjevoval	vyjevovat	k5eAaImAgMnS
tvorbou	tvorba	k1gFnSc7
své	svůj	k3xOyFgInPc4
životní	životní	k2eAgInPc4d1
postoje	postoj	k1gInPc4
a	a	k8xC
problémy	problém	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
předpokládat	předpokládat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
“	“	k?
<g/>
zpověď	zpověď	k1gFnSc1
dítěte	dítě	k1gNnSc2
své	svůj	k3xOyFgFnSc2
doby	doba	k1gFnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
pod	pod	k7c7
blazeovaným	blazeovaný	k2eAgInSc7d1
postojem	postoj	k1gInSc7
skrývalo	skrývat	k5eAaImAgNnS
utrpení	utrpení	k1gNnSc4
a	a	k8xC
zraňovanou	zraňovaný	k2eAgFnSc4d1
citlivost	citlivost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lorenzaccio	Lorenzaccio	k6eAd1
je	být	k5eAaImIp3nS
historická	historický	k2eAgFnSc1d1
hra	hra	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
všechny	všechen	k3xTgFnPc4
vlastnosti	vlastnost	k1gFnPc4
romantického	romantický	k2eAgNnSc2d1
dramatu	drama	k1gNnSc2
<g/>
:	:	kIx,
nesvázanost	nesvázanost	k1gFnSc1
pravidly	pravidlo	k1gNnPc7
<g/>
,	,	kIx,
shakespearovskou	shakespearovský	k2eAgFnSc4d1
fresku	freska	k1gFnSc4
<g/>
,	,	kIx,
syntézu	syntéza	k1gFnSc4
stylů	styl	k1gInPc2
a	a	k8xC
žánrů	žánr	k1gInPc2
<g/>
,	,	kIx,
velikost	velikost	k1gFnSc4
i	i	k8xC
nízkost	nízkost	k1gFnSc4
<g/>
,	,	kIx,
smysl	smysl	k1gInSc4
pro	pro	k7c4
kolorit	kolorit	k1gInSc4
doby	doba	k1gFnSc2
a	a	k8xC
místa	místo	k1gNnSc2
i	i	k9
pro	pro	k7c4
bohatě	bohatě	k6eAd1
motivované	motivovaný	k2eAgNnSc4d1
jednání	jednání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
praktický	praktický	k2eAgInSc1d1
“	“	k?
<g/>
manifest	manifest	k1gInSc1
<g/>
"	"	kIx"
francouzského	francouzský	k2eAgInSc2d1
romantismu	romantismus	k1gInSc2
a	a	k8xC
jeden	jeden	k4xCgInSc4
z	z	k7c2
dramatických	dramatický	k2eAgInPc2d1
textů	text	k1gInPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
zkušenosti	zkušenost	k1gFnPc4
nemůže	moct	k5eNaImIp3nS
světové	světový	k2eAgNnSc1d1
historické	historický	k2eAgNnSc1d1
drama	drama	k1gNnSc1
pominout	pominout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1
hrob	hrob	k1gInSc1
na	na	k7c6
pařížském	pařížský	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
Pè	Pè	k1gFnSc2
</s>
<s>
Hra	hra	k1gFnSc1
nebyla	být	k5eNaImAgFnS
za	za	k7c2
autorova	autorův	k2eAgInSc2d1
života	život	k1gInSc2
uvedena	uveden	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
verzi	verze	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
upravil	upravit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
Paul	Paul	k1gMnSc1
(	(	kIx(
<g/>
1804	#num#	k4
<g/>
-	-	kIx~
<g/>
1880	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
cenzura	cenzura	k1gFnSc1
nepovolila	povolit	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Premiéra	premiéra	k1gFnSc1
hry	hra	k1gFnSc2
proběhla	proběhnout	k5eAaPmAgFnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1896	#num#	k4
v	v	k7c6
divadle	divadlo	k1gNnSc6
Sarah	Sarah	k1gFnSc2
Bernhardt	Bernhardta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sarah	Sarah	k1gFnSc1
Bernhardt	Bernhardta	k1gFnPc2
rovněž	rovněž	k9
hrála	hrát	k5eAaImAgFnS
titulní	titulní	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plakát	plakát	k1gInSc1
k	k	k7c3
tomuto	tento	k3xDgNnSc3
představení	představení	k1gNnSc3
vytvořil	vytvořit	k5eAaPmAgMnS
Alfons	Alfons	k1gMnSc1
Mucha	Mucha	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
(	(	kIx(
<g/>
Le	Le	k1gMnSc1
Chandelier	Chandelier	k1gMnSc1
<g/>
,	,	kIx,
1835	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Na	na	k7c4
nic	nic	k3yNnSc4
nepřísahat	přísahat	k5eNaImF
(	(	kIx(
<g/>
Il	Il	k1gFnSc1
ne	ne	k9
faut	faut	k2eAgMnSc1d1
jurer	jurer	k1gMnSc1
de	de	k?
rien	rien	k1gMnSc1
<g/>
,	,	kIx,
1836	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zpověď	zpověď	k1gFnSc1
dítěte	dítě	k1gNnSc2
svého	svůj	k3xOyFgInSc2
věku	věk	k1gInSc2
(	(	kIx(
<g/>
La	la	k1gNnSc1
Confession	Confession	k1gInSc1
d	d	k?
<g/>
’	’	k?
<g/>
un	un	k?
enfant	enfant	k1gInSc1
du	du	k?
siè	siè	k1gInSc1
<g/>
,	,	kIx,
1836	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpověď	zpověď	k1gFnSc1
dítěte	dítě	k1gNnSc2
svého	svůj	k3xOyFgInSc2
věku	věk	k1gInSc2
je	být	k5eAaImIp3nS
velice	velice	k6eAd1
zvláštní	zvláštní	k2eAgNnSc1d1
epické	epický	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
Alfreda	Alfred	k1gMnSc2
de	de	k?
Musseta	Musset	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napohled	napohled	k6eAd1
tradiční	tradiční	k2eAgNnSc4d1
vyprávění	vyprávění	k1gNnSc4
o	o	k7c6
lidských	lidský	k2eAgInPc6d1
osudech	osud	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovo	slovo	k1gNnSc4
„	„	k?
<g/>
zpověď	zpověď	k1gFnSc1
<g/>
“	“	k?
dává	dávat	k5eAaImIp3nS
tušit	tušit	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
autor	autor	k1gMnSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
osobní	osobní	k2eAgFnSc4d1
deziluzi	deziluze	k1gFnSc4
i	i	k8xC
rozčarování	rozčarování	k1gNnSc4
celé	celý	k2eAgFnSc2d1
generace	generace	k1gFnSc2
žijící	žijící	k2eAgFnSc1d1
v	v	k7c6
prvních	první	k4xOgNnPc6
desetiletích	desetiletí	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
románové	románový	k2eAgFnSc6d1
předloze	předloha	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1836	#num#	k4
jsou	být	k5eAaImIp3nP
tyto	tento	k3xDgInPc4
pocity	pocit	k1gInPc4
shrnuty	shrnut	k2eAgInPc4d1
ve	v	k7c6
větách	věta	k1gFnPc6
<g/>
:	:	kIx,
„	„	k?
<g/>
Všechna	všechen	k3xTgNnPc1
nemoc	nemoc	k1gFnSc4
tohoto	tento	k3xDgNnSc2
století	století	k1gNnSc2
pramení	pramenit	k5eAaImIp3nS
ze	z	k7c2
dvou	dva	k4xCgFnPc2
příčin	příčina	k1gFnPc2
<g/>
:	:	kIx,
lid	lid	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
prošel	projít	k5eAaPmAgMnS
rokem	rok	k1gInSc7
1793	#num#	k4
a	a	k8xC
1814	#num#	k4
<g/>
,	,	kIx,
nese	nést	k5eAaImIp3nS
v	v	k7c6
srdci	srdce	k1gNnSc6
dvě	dva	k4xCgFnPc1
rány	rána	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nic	nic	k3yNnSc1
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
už	už	k6eAd1
není	být	k5eNaImIp3nS
<g/>
,	,	kIx,
všechno	všechen	k3xTgNnSc1
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
bude	být	k5eAaImBp3nS
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
není	být	k5eNaImIp3nS
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výpověď	výpověď	k1gFnSc1
o	o	k7c6
zdroji	zdroj	k1gInSc6
pocitů	pocit	k1gInPc2
romantických	romantický	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
obraz	obraz	k1gInSc4
životního	životní	k2eAgInSc2d1
postoje	postoj	k1gInSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
neustále	neustále	k6eAd1
vrací	vracet	k5eAaImIp3nS
-	-	kIx~
ideální	ideální	k2eAgFnPc1d1
představy	představa	k1gFnPc1
a	a	k8xC
sny	sen	k1gInPc1
se	se	k3xPyFc4
střetávají	střetávat	k5eAaImIp3nP
se	s	k7c7
skutečností	skutečnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
nadčasovost	nadčasovost	k1gFnSc4
rozporu	rozpor	k1gInSc2
mezi	mezi	k7c7
ideálem	ideál	k1gInSc7
a	a	k8xC
realitou	realita	k1gFnSc7
zvolil	zvolit	k5eAaPmAgMnS
Antonín	Antonín	k1gMnSc1
Máša	Máša	k1gFnSc1
Zpověď	zpověď	k1gFnSc1
dítěte	dítě	k1gNnSc2
svého	svůj	k3xOyFgInSc2
věku	věk	k1gInSc2
pro	pro	k7c4
inscenaci	inscenace	k1gFnSc4
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Noc	noc	k1gFnSc1
májová	májová	k1gFnSc1
(	(	kIx(
<g/>
La	la	k1gNnSc1
Nuit	Nuita	k1gFnPc2
de	de	k?
mai	mai	k?
<g/>
,	,	kIx,
1835	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Noc	noc	k1gFnSc1
prosincová	prosincový	k2eAgFnSc1d1
(	(	kIx(
<g/>
La	la	k1gNnSc7
Nuit	Nuit	k1gMnSc1
de	de	k?
décembre	décembr	k1gMnSc5
<g/>
,	,	kIx,
1835	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Noc	noc	k1gFnSc1
srpnová	srpnový	k2eAgFnSc1d1
(	(	kIx(
<g/>
La	la	k1gNnSc7
Nuit	Nuit	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
aoû	aoû	k1gMnSc1
<g/>
,	,	kIx,
1836	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
Noc	noc	k1gFnSc1
říjnová	říjnový	k2eAgFnSc1d1
(	(	kIx(
<g/>
La	la	k1gNnSc7
Nuit	Nuit	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
octobre	octobr	k1gMnSc5
<g/>
,	,	kIx,
1837	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
série	série	k1gFnSc1
romantických	romantický	k2eAgFnPc2d1
básní	báseň	k1gFnPc2
plných	plný	k2eAgFnPc2d1
rozervanosti	rozervanost	k1gFnPc4
a	a	k8xC
utrpení	utrpení	k1gNnSc4
</s>
<s>
Rozmar	rozmar	k1gInSc1
(	(	kIx(
<g/>
Un	Un	k1gFnPc1
caprice	caprika	k1gFnSc3
<g/>
,	,	kIx,
1837	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Improvizace	improvizace	k1gFnSc1
(	(	kIx(
<g/>
Impromptu	impromptu	k1gNnSc1
<g/>
,	,	kIx,
En	En	k1gFnSc1
réponse	réponse	k1gFnSc1
à	à	k?
la	la	k1gNnPc2
question	question	k1gInSc1
:	:	kIx,
qu	qu	k?
<g/>
'	'	kIx"
<g/>
est-ce	est-ce	k1gMnSc1
que	que	k?
la	la	k1gNnSc4
Poésie	Poésie	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
1839	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vzpomínka	vzpomínka	k1gFnSc1
(	(	kIx(
<g/>
Souvenir	Souvenir	k1gMnSc1
<g/>
,	,	kIx,
1841	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Příběh	příběh	k1gInSc1
bílého	bílý	k1gMnSc2
kosa	kos	k1gMnSc2
(	(	kIx(
<g/>
Histoire	Histoir	k1gInSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
un	un	k?
merle	merle	k1gFnSc1
blanc	blanc	k1gFnSc1
<g/>
,	,	kIx,
1842	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Petr	Petr	k1gMnSc1
a	a	k8xC
Kamila	Kamila	k1gFnSc1
(	(	kIx(
<g/>
Pierre	Pierr	k1gInSc5
et	et	k?
Camille	Camille	k1gNnPc7
<g/>
,	,	kIx,
1844	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mimi	Mimi	k1gFnSc1
Pinsonová	Pinsonová	k1gFnSc1
(	(	kIx(
<g/>
Mademoiselle	Mademoiselle	k1gInSc1
Mimi	mimi	k1gNnSc1
Pinson	Pinson	k1gInSc1
<g/>
,	,	kIx,
1845	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mezi	mezi	k7c7
dveřmi	dveře	k1gFnPc7
(	(	kIx(
<g/>
Il	Il	k1gMnPc1
faut	fauta	k1gFnPc2
qu	qu	k?
<g/>
'	'	kIx"
<g/>
une	une	k?
porte	port	k1gInSc5
soit	soit	k1gInSc1
ouverte	ouvrat	k5eAaPmRp2nP
ou	ou	k0
fermée	fermée	k6eAd1
<g/>
,	,	kIx,
1845	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nedá	dát	k5eNaPmIp3nS
se	se	k3xPyFc4
myslet	myslet	k5eAaImF
na	na	k7c4
všechno	všechen	k3xTgNnSc4
(	(	kIx(
<g/>
On	on	k3xPp3gMnSc1
ne	ne	k9
saurait	saurait	k2eAgMnSc1d1
penser	penser	k1gMnSc1
à	à	k?
tout	tout	k1gMnSc1
<g/>
,	,	kIx,
1849	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
LAGARDÉ	LAGARDÉ	kA
<g/>
,	,	kIx,
André	André	k1gMnSc1
<g/>
;	;	kIx,
MICHARD	MICHARD	kA
<g/>
,	,	kIx,
Laurent	Laurent	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzská	francouzský	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Garamond	garamond	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
579	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7407	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
26	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HRBATA	HRBATA	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
heslo	heslo	k1gNnSc1
Alfred	Alfred	k1gMnSc1
de	de	k?
Musset	Musset	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Fryčer	Fryčer	k1gMnSc1
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovník	slovník	k1gInSc1
francouzsky	francouzsky	k6eAd1
píšících	píšící	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
130	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
518	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BRETT	BRETT	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alfred	Alfred	k1gMnSc1
de	de	k?
Musset	Musset	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Otokar	Otokar	k1gMnSc1
Fischer	Fischer	k1gMnSc1
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
francouzské	francouzský	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
1	#num#	k4
(	(	kIx(
<g/>
1789	#num#	k4
<g/>
-	-	kIx~
<g/>
1870	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
268	#num#	k4
<g/>
-	-	kIx~
<g/>
284	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ŠALDA	Šalda	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Xaver	Xaver	k1gMnSc1
<g/>
.	.	kIx.
heslo	heslo	k1gNnSc1
Alfred	Alfred	k1gMnSc1
de	de	k?
Musset	Musset	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
kolektiv	kolektiv	k1gInSc1
autorů	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ottův	Ottův	k2eAgInSc4d1
slovník	slovník	k1gInSc4
naučný	naučný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
1901	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
57	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
XVII	XVII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
907	#num#	k4
<g/>
-	-	kIx~
<g/>
909	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
text	text	k1gInSc1
hesla	heslo	k1gNnSc2
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
též	též	k9
v	v	k7c6
knize	kniha	k1gFnSc6
Šaldův	Šaldův	k2eAgInSc4d1
slovník	slovník	k1gInSc4
naučný	naučný	k2eAgInSc4d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
:	:	kIx,
Československý	československý	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Octave	Octav	k1gInSc5
Teissier	Teissier	k1gMnSc1
<g/>
:	:	kIx,
Alfred	Alfred	k1gMnSc1
de	de	k?
Musset	Musset	k1gMnSc1
<g/>
,	,	kIx,
documents	documents	k6eAd1
généalogiques	généalogiques	k1gInSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
:	:	kIx,
Draguignan	Draguignan	k1gMnSc1
<g/>
,	,	kIx,
1903	#num#	k4
<g/>
,	,	kIx,
-	-	kIx~
genealogie	genealogie	k1gFnSc1
rodu	rod	k1gInSc2
de	de	k?
Musset	Musset	k1gInSc1
<g/>
,	,	kIx,
dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BRETT	BRETT	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alfred	Alfred	k1gMnSc1
de	de	k?
Musset	Musset	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Otokar	Otokar	k1gMnSc1
Fischer	Fischer	k1gMnSc1
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
francouzské	francouzský	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
1	#num#	k4
(	(	kIx(
<g/>
1789	#num#	k4
<g/>
-	-	kIx~
<g/>
1870	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
268	#num#	k4
<g/>
-	-	kIx~
<g/>
284	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ŠORMOVÁ	Šormová	k1gFnSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lorenzaccio	Lorenzaccio	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
Vladimír	Vladimír	k1gMnSc1
Macura	Macura	k1gMnSc1
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovník	slovník	k1gInSc1
světových	světový	k2eAgFnPc2d1
literárních	literární	k2eAgFnPc2d1
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
207	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
960	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
2	#num#	k4
(	(	kIx(
<g/>
M-Ž	M-Ž	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
84	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Digitalizované	digitalizovaný	k2eAgInPc1d1
překlady	překlad	k1gInPc1
děl	dělo	k1gNnPc2
Alfreda	Alfred	k1gMnSc2
de	de	k?
Musseta	Musset	k1gMnSc2
v	v	k7c6
digitální	digitální	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
Kramerius	Kramerius	k1gMnSc1
NK	NK	kA
ČR	ČR	kA
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
křeslo	křeslo	k1gNnSc4
Francouzské	francouzský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
Emmanuel	Emmanuel	k1gMnSc1
Dupaty	Dupata	k1gFnSc2
</s>
<s>
1852	#num#	k4
<g/>
–	–	k?
<g/>
1857	#num#	k4
Alfred	Alfred	k1gMnSc1
de	de	k?
Musset	Musset	k1gMnSc1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
Victor	Victor	k1gMnSc1
de	de	k?
Laprade	Laprad	k1gInSc5
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19990005919	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118585940	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2133	#num#	k4
4059	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80036706	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500030255	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
54151927	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80036706	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Šachy	šach	k1gInPc1
</s>
