<s>
Organizace	organizace	k1gFnSc1	organizace
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
kulturu	kultura	k1gFnSc4	kultura
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
United	United	k1gMnSc1	United
Nations	Nationsa	k1gFnPc2	Nationsa
Educational	Educational	k1gMnSc1	Educational
<g/>
,	,	kIx,	,
Scientific	Scientific	k1gMnSc1	Scientific
and	and	k?	and
Cultural	Cultural	k1gFnSc1	Cultural
Organization	Organization	k1gInSc1	Organization
<g/>
,	,	kIx,	,
UNESCO	Unesco	k1gNnSc1	Unesco
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
15	[number]	k4	15
mezistátních	mezistátní	k2eAgFnPc2d1	mezistátní
odborných	odborný	k2eAgFnPc2d1	odborná
organizací	organizace	k1gFnPc2	organizace
(	(	kIx(	(
<g/>
agentur	agentura	k1gFnPc2	agentura
<g/>
)	)	kIx)	)
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
má	mít	k5eAaImIp3nS	mít
195	[number]	k4	195
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInPc4d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
autonomní	autonomní	k2eAgNnSc4d1	autonomní
území	území	k1gNnSc4	území
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
přijetí	přijetí	k1gNnSc2	přijetí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Palestina	Palestina	k1gFnSc1	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
a	a	k8xC	a
Kanada	Kanada	k1gFnSc1	Kanada
zareagovaly	zareagovat	k5eAaPmAgInP	zareagovat
pozastavením	pozastavení	k1gNnSc7	pozastavení
svých	svůj	k3xOyFgInPc2	svůj
příspěvků	příspěvek	k1gInPc2	příspěvek
této	tento	k3xDgFnSc6	tento
organizaci	organizace	k1gFnSc6	organizace
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
probíhající	probíhající	k2eAgInSc4d1	probíhající
konflikt	konflikt	k1gInSc4	konflikt
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
řešen	řešit	k5eAaImNgInS	řešit
vyjednáváním	vyjednávání	k1gNnSc7	vyjednávání
<g/>
.	.	kIx.	.
</s>
<s>
Podnětem	podnět	k1gInSc7	podnět
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
organizace	organizace	k1gFnSc2	organizace
byly	být	k5eAaImAgFnP	být
hrůzy	hrůza	k1gFnPc1	hrůza
právě	právě	k6eAd1	právě
skončené	skončený	k2eAgFnPc1d1	skončená
druhé	druhý	k4xOgFnPc1	druhý
světové	světový	k2eAgFnPc1d1	světová
války	válka	k1gFnPc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
UNESCO	UNESCO	kA	UNESCO
bylo	být	k5eAaImAgNnS	být
proto	proto	k8xC	proto
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
udržení	udržení	k1gNnSc4	udržení
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
míru	mír	k1gInSc2	mír
rozvíjením	rozvíjení	k1gNnSc7	rozvíjení
spolupráce	spolupráce	k1gFnSc2	spolupráce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výchovy	výchova	k1gFnSc2	výchova
<g/>
,	,	kIx,	,
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
prosazováním	prosazování	k1gNnSc7	prosazování
úcty	úcta	k1gFnSc2	úcta
k	k	k7c3	k
lidským	lidský	k2eAgNnPc3d1	lidské
právům	právo	k1gNnPc3	právo
a	a	k8xC	a
právnímu	právní	k2eAgInSc3d1	právní
řádu	řád	k1gInSc3	řád
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
UNESCO	UNESCO	kA	UNESCO
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
podepsáním	podepsání	k1gNnSc7	podepsání
Ústavy	ústava	k1gFnSc2	ústava
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
na	na	k7c6	na
ustavující	ustavující	k2eAgFnSc6d1	ustavující
diplomatické	diplomatický	k2eAgFnSc6d1	diplomatická
konferenci	konference	k1gFnSc6	konference
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
pak	pak	k6eAd1	pak
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1946	[number]	k4	1946
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
ratifikaci	ratifikace	k1gFnSc6	ratifikace
zakládajícími	zakládající	k2eAgFnPc7d1	zakládající
dvaceti	dvacet	k4xCc7	dvacet
státy	stát	k1gInPc7	stát
<g/>
:	:	kIx,	:
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Libanon	Libanon	k1gInSc1	Libanon
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
UNESCO	UNESCO	kA	UNESCO
195	[number]	k4	195
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
9	[number]	k4	9
přidružených	přidružený	k2eAgMnPc2d1	přidružený
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
činností	činnost	k1gFnSc7	činnost
v	v	k7c6	v
pěti	pět	k4xCc6	pět
hlavních	hlavní	k2eAgInPc6d1	hlavní
oborech	obor	k1gInPc6	obor
<g/>
:	:	kIx,	:
Vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnPc4d1	přírodní
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnPc4d1	sociální
a	a	k8xC	a
humanitní	humanitní	k2eAgFnPc4d1	humanitní
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
komunikace	komunikace	k1gFnSc1	komunikace
a	a	k8xC	a
informace	informace	k1gFnSc1	informace
<g/>
.	.	kIx.	.
</s>
<s>
Projekty	projekt	k1gInPc1	projekt
podporované	podporovaný	k2eAgFnSc2d1	podporovaná
UNESCO	UNESCO	kA	UNESCO
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
šíření	šíření	k1gNnSc4	šíření
gramotnosti	gramotnost	k1gFnSc2	gramotnost
<g/>
,	,	kIx,	,
odborné	odborný	k2eAgInPc4d1	odborný
a	a	k8xC	a
školicí	školicí	k2eAgInPc4d1	školicí
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
programy	program	k1gInPc4	program
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
vědní	vědní	k2eAgFnSc2d1	vědní
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
,	,	kIx,	,
propagaci	propagace	k1gFnSc4	propagace
nezávislých	závislý	k2eNgNnPc2d1	nezávislé
médií	médium	k1gNnPc2	médium
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgInSc4d1	regionální
a	a	k8xC	a
kulturní	kulturní	k2eAgInPc4d1	kulturní
historické	historický	k2eAgInPc4d1	historický
projekty	projekt	k1gInPc4	projekt
<g/>
,	,	kIx,	,
propagaci	propagace	k1gFnSc4	propagace
kulturní	kulturní	k2eAgFnSc2d1	kulturní
různorodosti	různorodost	k1gFnSc2	různorodost
<g/>
,	,	kIx,	,
dohody	dohoda	k1gFnSc2	dohoda
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
spolupráce	spolupráce	k1gFnSc2	spolupráce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kulturního	kulturní	k2eAgNnSc2d1	kulturní
a	a	k8xC	a
přírodního	přírodní	k2eAgNnSc2d1	přírodní
dědictví	dědictví	k1gNnSc2	dědictví
a	a	k8xC	a
ochrany	ochrana	k1gFnSc2	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokusy	pokus	k1gInPc1	pokus
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
rozdíly	rozdíl	k1gInPc4	rozdíl
v	v	k7c6	v
možnosti	možnost	k1gFnSc6	možnost
využití	využití	k1gNnSc4	využití
digitálních	digitální	k2eAgFnPc2d1	digitální
technologií	technologie	k1gFnPc2	technologie
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
digital	digital	k1gMnSc1	digital
divide	divid	k1gInSc5	divid
[	[	kIx(	[
<g/>
en	en	k?	en
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
organizace	organizace	k1gFnSc2	organizace
definuje	definovat	k5eAaBmIp3nS	definovat
tři	tři	k4xCgInPc4	tři
hlavní	hlavní	k2eAgInPc4d1	hlavní
orgány	orgán	k1gInPc4	orgán
<g/>
:	:	kIx,	:
Generální	generální	k2eAgFnSc1d1	generální
konference	konference	k1gFnSc1	konference
UNESCO	UNESCO	kA	UNESCO
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
rada	rada	k1gFnSc1	rada
UNESCO	Unesco	k1gNnSc1	Unesco
Sekretariát	sekretariát	k1gInSc4	sekretariát
</s>
<s>
Generální	generální	k2eAgFnSc2d1	generální
konference	konference	k1gFnSc2	konference
UNESCO	UNESCO	kA	UNESCO
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
orgánem	orgán	k1gInSc7	orgán
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
jej	on	k3xPp3gNnSc4	on
zástupci	zástupce	k1gMnPc1	zástupce
všech	všecek	k3xTgInPc2	všecek
zúčastněných	zúčastněný	k2eAgInPc2d1	zúčastněný
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
zasedání	zasedání	k1gNnSc1	zasedání
Generální	generální	k2eAgFnSc2d1	generální
konference	konference	k1gFnSc2	konference
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c6	na
konci	konec	k1gInSc6	konec
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
se	se	k3xPyFc4	se
projednává	projednávat	k5eAaImIp3nS	projednávat
a	a	k8xC	a
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
program	program	k1gInSc1	program
a	a	k8xC	a
rozpočet	rozpočet	k1gInSc1	rozpočet
organizace	organizace	k1gFnSc2	organizace
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
Generální	generální	k2eAgFnSc2d1	generální
konference	konference	k1gFnSc2	konference
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgFnSc1d1	generální
konference	konference	k1gFnSc1	konference
přijímá	přijímat	k5eAaImIp3nS	přijímat
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
smlouvy	smlouva	k1gFnPc4	smlouva
(	(	kIx(	(
<g/>
po	po	k7c6	po
ratifikaci	ratifikace	k1gFnSc6	ratifikace
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
smluvní	smluvní	k2eAgFnPc4d1	smluvní
strany	strana	k1gFnPc4	strana
závazné	závazný	k2eAgFnPc4d1	závazná
<g/>
)	)	kIx)	)
a	a	k8xC	a
nezávazné	závazný	k2eNgFnSc2d1	nezávazná
deklarace	deklarace	k1gFnSc2	deklarace
a	a	k8xC	a
doporučení	doporučení	k1gNnSc2	doporučení
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
zúčastněný	zúčastněný	k2eAgInSc1d1	zúčastněný
stát	stát	k1gInSc1	stát
má	mít	k5eAaImIp3nS	mít
při	při	k7c6	při
hlasování	hlasování	k1gNnSc6	hlasování
jeden	jeden	k4xCgInSc4	jeden
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přijetí	přijetí	k1gNnSc4	přijetí
klíčových	klíčový	k2eAgNnPc2d1	klíčové
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
změna	změna	k1gFnSc1	změna
ústavy	ústava	k1gFnSc2	ústava
či	či	k8xC	či
volba	volba	k1gFnSc1	volba
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
dvoutřetinové	dvoutřetinový	k2eAgFnSc2d1	dvoutřetinová
většiny	většina	k1gFnSc2	většina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ostatní	ostatní	k2eAgNnSc4d1	ostatní
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
stačí	stačit	k5eAaBmIp3nS	stačit
prostá	prostý	k2eAgFnSc1d1	prostá
většina	většina	k1gFnSc1	většina
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
plenárního	plenární	k2eAgNnSc2d1	plenární
zasedání	zasedání	k1gNnSc2	zasedání
(	(	kIx(	(
<g/>
delegáty	delegát	k1gMnPc7	delegát
tvoří	tvořit	k5eAaImIp3nP	tvořit
obvykle	obvykle	k6eAd1	obvykle
ministři	ministr	k1gMnPc1	ministr
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
nebo	nebo	k8xC	nebo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
<g/>
)	)	kIx)	)
pracuje	pracovat	k5eAaImIp3nS	pracovat
i	i	k9	i
šest	šest	k4xCc1	šest
odborných	odborný	k2eAgFnPc2d1	odborná
programových	programový	k2eAgFnPc2d1	programová
komisí	komise	k1gFnPc2	komise
a	a	k8xC	a
čtyři	čtyři	k4xCgInPc1	čtyři
výbory	výbor	k1gInPc1	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
rada	rada	k1gFnSc1	rada
UNESCO	UNESCO	kA	UNESCO
především	především	k6eAd1	především
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
na	na	k7c4	na
realizaci	realizace	k1gFnSc4	realizace
plnění	plnění	k1gNnSc2	plnění
programu	program	k1gInSc2	program
přijatého	přijatý	k2eAgInSc2d1	přijatý
Generální	generální	k2eAgFnSc7d1	generální
konferencí	konference	k1gFnSc7	konference
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
projednává	projednávat	k5eAaImIp3nS	projednávat
důležité	důležitý	k2eAgFnPc4d1	důležitá
otázky	otázka	k1gFnPc4	otázka
před	před	k7c7	před
jejich	jejich	k3xOp3gNnSc7	jejich
předložením	předložení	k1gNnSc7	předložení
Generální	generální	k2eAgFnSc2d1	generální
konferenci	konference	k1gFnSc3	konference
a	a	k8xC	a
podílí	podílet	k5eAaImIp3nS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
budoucích	budoucí	k2eAgInPc2d1	budoucí
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
rada	rada	k1gFnSc1	rada
58	[number]	k4	58
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Sekretariát	sekretariát	k1gInSc1	sekretariát
je	být	k5eAaImIp3nS	být
výkonnou	výkonný	k2eAgFnSc7d1	výkonná
a	a	k8xC	a
servisní	servisní	k2eAgFnSc7d1	servisní
složkou	složka	k1gFnSc7	složka
UNESCO	Unesco	k1gNnSc1	Unesco
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
jej	on	k3xPp3gNnSc2	on
okolo	okolo	k7c2	okolo
2	[number]	k4	2
160	[number]	k4	160
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
ze	z	k7c2	z
170	[number]	k4	170
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
více	hodně	k6eAd2	hodně
než	než	k8xS	než
680	[number]	k4	680
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
58	[number]	k4	58
kancelářích	kancelář	k1gFnPc6	kancelář
UNESCO	UNESCO	kA	UNESCO
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Údaje	údaj	k1gInPc4	údaj
z	z	k7c2	z
června	červen	k1gInSc2	červen
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
sekretariátu	sekretariát	k1gInSc2	sekretariát
je	být	k5eAaImIp3nS	být
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
trvá	trvat	k5eAaImIp3nS	trvat
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
generálním	generální	k2eAgMnSc7d1	generální
ředitelem	ředitel	k1gMnSc7	ředitel
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
Irina	Irin	k1gInSc2	Irin
Boková	bokový	k2eAgFnSc1d1	Boková
z	z	k7c2	z
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
zakládajícím	zakládající	k2eAgInPc3d1	zakládající
členům	člen	k1gInPc3	člen
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
samostatná	samostatný	k2eAgFnSc1d1	samostatná
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
stala	stát	k5eAaPmAgFnS	stát
jeho	on	k3xPp3gInSc4	on
členem	člen	k1gMnSc7	člen
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zprostředkování	zprostředkování	k1gNnSc4	zprostředkování
styku	styk	k1gInSc2	styk
mezi	mezi	k7c7	mezi
národními	národní	k2eAgFnPc7d1	národní
institucemi	instituce	k1gFnPc7	instituce
a	a	k8xC	a
odborníky	odborník	k1gMnPc7	odborník
má	mít	k5eAaImIp3nS	mít
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
Českou	český	k2eAgFnSc4d1	Česká
komisi	komise	k1gFnSc4	komise
pro	pro	k7c4	pro
UNESCO	Unesco	k1gNnSc4	Unesco
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
Stálou	stálý	k2eAgFnSc4d1	stálá
misi	mise	k1gFnSc4	mise
při	při	k7c6	při
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
UNESCO	Unesco	k1gNnSc4	Unesco
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
Ústavy	ústava	k1gFnSc2	ústava
UNESCO	Unesco	k1gNnSc1	Unesco
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ze	z	k7c2	z
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1994	[number]	k4	1994
jako	jako	k8xS	jako
poradní	poradní	k2eAgInSc1d1	poradní
orgán	orgán	k1gInSc1	orgán
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
této	tento	k3xDgFnSc2	tento
komise	komise	k1gFnSc2	komise
je	být	k5eAaImIp3nS	být
zprostředkovávat	zprostředkovávat	k5eAaImF	zprostředkovávat
styk	styk	k1gInSc4	styk
národních	národní	k2eAgFnPc2d1	národní
institucí	instituce	k1gFnPc2	instituce
a	a	k8xC	a
odborníků	odborník	k1gMnPc2	odborník
s	s	k7c7	s
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
dále	daleko	k6eAd2	daleko
studuje	studovat	k5eAaImIp3nS	studovat
dokumenty	dokument	k1gInPc4	dokument
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
myšlenky	myšlenka	k1gFnPc4	myšlenka
organizace	organizace	k1gFnSc2	organizace
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
se	s	k7c7	s
sekretariátem	sekretariát	k1gInSc7	sekretariát
UNESCO	Unesco	k1gNnSc1	Unesco
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
má	mít	k5eAaImIp3nS	mít
50	[number]	k4	50
členů	člen	k1gInPc2	člen
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
zástupci	zástupce	k1gMnPc1	zástupce
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
<g/>
,	,	kIx,	,
zástupci	zástupce	k1gMnPc1	zástupce
významných	významný	k2eAgFnPc2d1	významná
institucí	instituce	k1gFnPc2	instituce
-	-	kIx~	-
státních	státní	k2eAgFnPc2d1	státní
i	i	k8xC	i
nevládních	vládní	k2eNgFnPc2d1	nevládní
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
helsinský	helsinský	k2eAgInSc1d1	helsinský
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
dobrovolnické	dobrovolnický	k2eAgNnSc1d1	Dobrovolnické
centrum	centrum	k1gNnSc1	centrum
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
osobnosti	osobnost	k1gFnPc1	osobnost
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
členů	člen	k1gMnPc2	člen
je	být	k5eAaImIp3nS	být
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
a	a	k8xC	a
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
komisi	komise	k1gFnSc6	komise
je	být	k5eAaImIp3nS	být
čestné	čestný	k2eAgNnSc1d1	čestné
<g/>
.	.	kIx.	.
</s>
<s>
Předsedkyní	předsedkyně	k1gFnSc7	předsedkyně
komise	komise	k1gFnSc2	komise
byla	být	k5eAaImAgFnS	být
senátorka	senátorka	k1gFnSc1	senátorka
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Moserová	Moserová	k1gFnSc1	Moserová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
opětovně	opětovně	k6eAd1	opětovně
jmenována	jmenovat	k5eAaBmNgFnS	jmenovat
na	na	k7c4	na
další	další	k2eAgNnSc4d1	další
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
byla	být	k5eAaImAgFnS	být
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2006	[number]	k4	2006
jmenována	jmenovat	k5eAaBmNgFnS	jmenovat
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
RNDr.	RNDr.	kA	RNDr.
Helena	Helena	k1gFnSc1	Helena
Illnerová	Illnerová	k1gFnSc1	Illnerová
<g/>
,	,	kIx,	,
DrSc	DrSc	kA	DrSc
<g/>
.	.	kIx.	.
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
je	být	k5eAaImIp3nS	být
novým	nový	k2eAgMnSc7d1	nový
předsedou	předseda	k1gMnSc7	předseda
Komise	komise	k1gFnSc2	komise
Mgr.	Mgr.	kA	Mgr.
Petr	Petr	k1gMnSc1	Petr
Gazdík	Gazdík	k1gMnSc1	Gazdík
<g/>
.	.	kIx.	.
</s>
<s>
Plnění	plnění	k1gNnSc1	plnění
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
Komise	komise	k1gFnSc2	komise
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Stálý	stálý	k2eAgInSc1d1	stálý
sekretariát	sekretariát	k1gInSc1	sekretariát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
funkci	funkce	k1gFnSc4	funkce
plní	plnit	k5eAaImIp3nS	plnit
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1996	[number]	k4	1996
pracovní	pracovní	k2eAgFnSc1d1	pracovní
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
Odboru	odbor	k1gInSc6	odbor
OSN	OSN	kA	OSN
na	na	k7c6	na
Ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1	tento
sekretariát	sekretariát	k1gInSc1	sekretariát
spravuje	spravovat	k5eAaImIp3nS	spravovat
i	i	k9	i
knihovnu	knihovna	k1gFnSc4	knihovna
publikací	publikace	k1gFnPc2	publikace
a	a	k8xC	a
dokumentů	dokument	k1gInPc2	dokument
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Stálou	stálý	k2eAgFnSc4d1	stálá
misi	mise	k1gFnSc4	mise
ČR	ČR	kA	ČR
při	při	k7c6	při
UNESCO	UNESCO	kA	UNESCO
vede	vést	k5eAaImIp3nS	vést
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
stálá	stálý	k2eAgFnSc1d1	stálá
představitelka	představitelka	k1gFnSc1	představitelka
ČR	ČR	kA	ČR
při	při	k7c6	při
UNESCO	UNESCO	kA	UNESCO
Marie	Marie	k1gFnSc1	Marie
Chatardová	Chatardová	k1gFnSc1	Chatardová
<g/>
.	.	kIx.	.
</s>
<s>
Mise	mise	k1gFnSc1	mise
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
poštovní	poštovní	k2eAgFnSc1d1	poštovní
správa	správa	k1gFnSc1	správa
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
centrály	centrála	k1gFnSc2	centrála
organizace	organizace	k1gFnSc2	organizace
začala	začít	k5eAaPmAgFnS	začít
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
vydávat	vydávat	k5eAaImF	vydávat
speciální	speciální	k2eAgFnPc4d1	speciální
edice	edice	k1gFnPc4	edice
poštovních	poštovní	k2eAgFnPc2d1	poštovní
známek	známka	k1gFnPc2	známka
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
UNESCO	UNESCO	kA	UNESCO
a	a	k8xC	a
nadtitulem	nadtitul	k1gInSc7	nadtitul
RÉPUBLIQUE	RÉPUBLIQUE	kA	RÉPUBLIQUE
FRANCAISE	FRANCAISE	kA	FRANCAISE
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
vyznačenou	vyznačený	k2eAgFnSc4d1	vyznačená
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
měnu	měna	k1gFnSc4	měna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2011	[number]	k4	2011
britská	britský	k2eAgFnSc1d1	britská
vládní	vládní	k2eAgFnSc1d1	vládní
organizace	organizace	k1gFnSc1	organizace
DFID	DFID	kA	DFID
ohodnotila	ohodnotit	k5eAaPmAgFnS	ohodnotit
celkovou	celkový	k2eAgFnSc4d1	celková
činnost	činnost	k1gFnSc4	činnost
organizace	organizace	k1gFnSc2	organizace
UNESCO	UNESCO	kA	UNESCO
jako	jako	k8xS	jako
finančně	finančně	k6eAd1	finančně
neefektivní	efektivní	k2eNgMnSc1d1	neefektivní
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
neuspokojivou	uspokojivý	k2eNgFnSc7d1	neuspokojivá
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
UNESCO	UNESCO	kA	UNESCO
toto	tento	k3xDgNnSc4	tento
hodnocení	hodnocení	k1gNnSc4	hodnocení
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
palestinském	palestinský	k2eAgInSc6d1	palestinský
časopise	časopis	k1gInSc6	časopis
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
sponzorovaném	sponzorovaný	k2eAgInSc6d1	sponzorovaný
částečně	částečně	k6eAd1	částečně
organizací	organizace	k1gFnPc2	organizace
UNESCO	Unesco	k1gNnSc4	Unesco
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2011	[number]	k4	2011
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
mladá	mladý	k2eAgFnSc1d1	mladá
dívka	dívka	k1gFnSc1	dívka
popsala	popsat	k5eAaPmAgFnS	popsat
Adolfa	Adolf	k1gMnSc4	Adolf
Hitlera	Hitler	k1gMnSc4	Hitler
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
čtyř	čtyři	k4xCgInPc2	čtyři
vzorů	vzor	k1gInPc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
UNESCO	UNESCO	kA	UNESCO
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
mediální	mediální	k2eAgInSc4d1	mediální
tlak	tlak	k1gInSc4	tlak
distancovalo	distancovat	k5eAaBmAgNnS	distancovat
od	od	k7c2	od
činnosti	činnost	k1gFnSc2	činnost
časopisu	časopis	k1gInSc2	časopis
a	a	k8xC	a
zastavilo	zastavit	k5eAaPmAgNnS	zastavit
jeho	jeho	k3xOp3gFnSc4	jeho
financování	financování	k1gNnSc1	financování
<g/>
.	.	kIx.	.
</s>
