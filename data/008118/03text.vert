<s>
Sob	sob	k1gMnSc1	sob
polární	polární	k2eAgMnSc1d1	polární
(	(	kIx(	(
<g/>
Rangifer	Rangifer	k1gMnSc1	Rangifer
tarandus	tarandus	k1gMnSc1	tarandus
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
synonyma	synonymum	k1gNnPc4	synonymum
sob	sob	k1gMnSc1	sob
arktický	arktický	k2eAgMnSc1d1	arktický
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
karibu	karibu	k1gMnSc1	karibu
(	(	kIx(	(
<g/>
nesprávně	správně	k6eNd1	správně
jelen	jelen	k1gMnSc1	jelen
karibu	karibu	k1gMnSc1	karibu
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
severský	severský	k2eAgMnSc1d1	severský
přežvýkavec	přežvýkavec	k1gMnSc1	přežvýkavec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
jelenovitých	jelenovití	k1gMnPc2	jelenovití
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jediný	jediný	k2eAgInSc4d1	jediný
zdomácnělý	zdomácnělý	k2eAgInSc4d1	zdomácnělý
druh	druh	k1gInSc4	druh
této	tento	k3xDgFnSc2	tento
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
slovo	slovo	k1gNnSc1	slovo
sob	soba	k1gFnPc2	soba
má	mít	k5eAaImIp3nS	mít
zcela	zcela	k6eAd1	zcela
nejasnou	jasný	k2eNgFnSc4d1	nejasná
etymologii	etymologie	k1gFnSc4	etymologie
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
příbuzné	příbuzný	k2eAgNnSc1d1	příbuzné
s	s	k7c7	s
pojmenováním	pojmenování	k1gNnSc7	pojmenování
tohoto	tento	k3xDgNnSc2	tento
zvířete	zvíře	k1gNnSc2	zvíře
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
jiném	jiný	k2eAgInSc6d1	jiný
jazyce	jazyk	k1gInSc6	jazyk
kromě	kromě	k7c2	kromě
slovenštiny	slovenština	k1gFnSc2	slovenština
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
přejato	přejmout	k5eAaPmNgNnS	přejmout
z	z	k7c2	z
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
souvislost	souvislost	k1gFnSc1	souvislost
s	s	k7c7	s
mongolským	mongolský	k2eAgInSc7d1	mongolský
názvem	název	k1gInSc7	název
soba	soba	k1gFnSc1	soba
caa	caa	k?	caa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
zpravidla	zpravidla	k6eAd1	zpravidla
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
severský	severský	k2eAgMnSc1d1	severský
jelen	jelen	k1gMnSc1	jelen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
germánských	germánský	k2eAgInPc6d1	germánský
a	a	k8xC	a
románských	románský	k2eAgInPc6d1	románský
jazycích	jazyk	k1gInPc6	jazyk
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jméno	jméno	k1gNnSc1	jméno
tohoto	tento	k3xDgNnSc2	tento
zvířete	zvíře	k1gNnSc2	zvíře
kořen	kořen	k1gInSc1	kořen
ren	ren	k?	ren
(	(	kIx(	(
<g/>
např.	např.	kA	např.
angl.	angl.	k?	angl.
Reindeer	Reindeer	k1gInSc1	Reindeer
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
Rentier	Rentier	k1gInSc1	Rentier
<g/>
,	,	kIx,	,
fr.	fr.	k?	fr.
le	le	k?	le
Renne	Renn	k1gMnSc5	Renn
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Rangifer	Rangifer	k1gInSc1	Rangifer
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
ze	z	k7c2	z
starého	starý	k2eAgInSc2d1	starý
sámského	sámský	k2eAgInSc2d1	sámský
názvu	název	k1gInSc2	název
raingo	raingo	k1gMnSc1	raingo
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnější	pravděpodobný	k2eAgFnSc1d2	pravděpodobnější
je	být	k5eAaImIp3nS	být
však	však	k9	však
germánský	germánský	k2eAgInSc4d1	germánský
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
ze	z	k7c2	z
staroseverského	staroseverský	k2eAgMnSc2d1	staroseverský
hrenn	hrenn	k1gMnSc1	hrenn
<g/>
,	,	kIx,	,
s	s	k7c7	s
významem	význam	k1gInSc7	význam
"	"	kIx"	"
<g/>
rohaté	rohatý	k2eAgNnSc1d1	rohaté
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
od	od	k7c2	od
protoindoevropského	protoindoevropský	k2eAgInSc2d1	protoindoevropský
kořene	kořen	k1gInSc2	kořen
*	*	kIx~	*
<g/>
kroin	kroin	k1gInSc1	kroin
–	–	k?	–
"	"	kIx"	"
<g/>
rohatý	rohatý	k1gMnSc1	rohatý
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
soba	sob	k1gMnSc4	sob
používal	používat	k5eAaImAgMnS	používat
termín	termín	k1gInSc4	termín
renař	renař	k1gMnSc1	renař
<g/>
.	.	kIx.	.
</s>
<s>
Latinský	latinský	k2eAgInSc1d1	latinský
druhový	druhový	k2eAgInSc1d1	druhový
název	název	k1gInSc1	název
tarandus	tarandus	k1gInSc1	tarandus
je	být	k5eAaImIp3nS	být
přejímkou	přejímka	k1gFnSc7	přejímka
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgInSc1d1	znám
už	už	k6eAd1	už
v	v	k7c6	v
antice	antika	k1gFnSc6	antika
<g/>
,	,	kIx,	,
doklady	doklad	k1gInPc1	doklad
např.	např.	kA	např.
u	u	k7c2	u
Theofrasta	Theofrast	k1gMnSc2	Theofrast
a	a	k8xC	a
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
severoamerického	severoamerický	k2eAgMnSc4d1	severoamerický
soba	sob	k1gMnSc4	sob
je	být	k5eAaImIp3nS	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
používán	používat	k5eAaImNgInS	používat
název	název	k1gInSc4	název
karibu	karibu	k1gMnSc2	karibu
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
algonkinských	algonkinský	k2eAgInPc2d1	algonkinský
jazyků	jazyk	k1gInPc2	jazyk
severoamerických	severoamerický	k2eAgMnPc2d1	severoamerický
indiánů	indián	k1gMnPc2	indián
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
mikmak	mikmak	k6eAd1	mikmak
kalibu	kalibat	k5eAaPmIp1nS	kalibat
<g/>
,	,	kIx,	,
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
hrabe	hrabat	k5eAaImIp3nS	hrabat
(	(	kIx(	(
<g/>
ve	v	k7c6	v
sněhu	sníh	k1gInSc6	sníh
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
soba	sob	k1gMnSc2	sob
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
sobí	sobí	k2eAgMnSc1d1	sobí
býk	býk	k1gMnSc1	býk
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
jako	jako	k8xS	jako
sobí	sobí	k2eAgFnSc1d1	sobí
laň	laň	k1gFnSc1	laň
nebo	nebo	k8xC	nebo
sobí	sobí	k2eAgFnSc1d1	sobí
kráva	kráva	k1gFnSc1	kráva
<g/>
,	,	kIx,	,
mládě	mládě	k1gNnSc1	mládě
jako	jako	k8xC	jako
sobí	sobí	k2eAgNnSc1d1	sobí
tele	tele	k1gNnSc1	tele
nebo	nebo	k8xC	nebo
z	z	k7c2	z
ruštiny	ruština	k1gFnSc2	ruština
přejatým	přejatý	k2eAgInSc7d1	přejatý
názvem	název	k1gInSc7	název
pižík	pižík	k1gInSc1	pižík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domorodých	domorodý	k2eAgInPc6d1	domorodý
jazycích	jazyk	k1gInPc6	jazyk
arktické	arktický	k2eAgFnSc2d1	arktická
a	a	k8xC	a
subarktické	subarktický	k2eAgFnSc2d1	subarktická
oblasti	oblast	k1gFnSc2	oblast
mají	mít	k5eAaImIp3nP	mít
sobi	sob	k1gMnPc1	sob
mnoho	mnoho	k4c4	mnoho
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
jazyky	jazyk	k1gInPc1	jazyk
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
název	název	k1gInSc4	název
domácího	domácí	k2eAgMnSc2d1	domácí
a	a	k8xC	a
divokého	divoký	k2eAgMnSc2d1	divoký
soba	sob	k1gMnSc2	sob
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc1	dva
tvoří	tvořit	k5eAaImIp3nP	tvořit
od	od	k7c2	od
odlišných	odlišný	k2eAgInPc2d1	odlišný
kořenů	kořen	k1gInPc2	kořen
<g/>
.	.	kIx.	.
v	v	k7c6	v
algonkinských	algonkinský	k2eAgInPc6d1	algonkinský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
:	:	kIx,	:
karibu	karibu	k1gMnSc1	karibu
<g/>
,	,	kIx,	,
qalibu	qalibat	k5eAaPmIp1nS	qalibat
v	v	k7c6	v
athabaských	athabaský	k2eAgInPc6d1	athabaský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
:	:	kIx,	:
vadzai	vadza	k1gFnPc1	vadza
<g/>
,	,	kIx,	,
bazej	bazej	k1gInSc1	bazej
<g />
.	.	kIx.	.
</s>
<s>
čukčsky	čukčsky	k6eAd1	čukčsky
<g/>
:	:	kIx,	:
korangä	korangä	k?	korangä
(	(	kIx(	(
<g/>
domácí	domácí	k2eAgFnSc1d1	domácí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
älvälu	älvälat	k5eAaPmIp1nS	älvälat
(	(	kIx(	(
<g/>
divoký	divoký	k2eAgMnSc1d1	divoký
<g/>
)	)	kIx)	)
eskymácky	eskymácky	k6eAd1	eskymácky
<g/>
:	:	kIx,	:
qunŋ	qunŋ	k?	qunŋ
(	(	kIx(	(
<g/>
domácí	domácí	k2eAgFnSc4d1	domácí
/	/	kIx~	/
tuttu	tutta	k1gFnSc4	tutta
(	(	kIx(	(
<g/>
divoký	divoký	k2eAgMnSc1d1	divoký
<g/>
)	)	kIx)	)
evenksky	evenkska	k1gFnPc1	evenkska
<g/>
:	:	kIx,	:
oron	oron	k1gNnSc1	oron
(	(	kIx(	(
<g/>
domácí	domácí	k2eAgMnSc1d1	domácí
/	/	kIx~	/
bujun	bujun	k1gMnSc1	bujun
(	(	kIx(	(
<g/>
divoký	divoký	k2eAgMnSc1d1	divoký
<g/>
)	)	kIx)	)
finsky	finsky	k6eAd1	finsky
<g/>
:	:	kIx,	:
poro	poro	k6eAd1	poro
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
domácí	domácí	k2eAgMnSc1d1	domácí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
peura	peura	k1gMnSc1	peura
(	(	kIx(	(
<g/>
divoký	divoký	k2eAgMnSc1d1	divoký
<g/>
)	)	kIx)	)
itelmensky	itelmensky	k6eAd1	itelmensky
<g/>
:	:	kIx,	:
kož	kož	k?	kož
(	(	kIx(	(
<g/>
domácí	domácí	k2eAgFnSc1d1	domácí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lwilch	lwilch	k1gMnSc1	lwilch
(	(	kIx(	(
<g/>
divoký	divoký	k2eAgMnSc1d1	divoký
<g/>
)	)	kIx)	)
jakutsky	jakutsky	k6eAd1	jakutsky
<g/>
:	:	kIx,	:
taba	taba	k6eAd1	taba
jukagirsky	jukagirsky	k6eAd1	jukagirsky
<g/>
:	:	kIx,	:
a	a	k8xC	a
<g/>
'	'	kIx"	'
<g/>
se	s	k7c7	s
(	(	kIx(	(
<g/>
domácí	domácí	k2eAgFnSc7d1	domácí
<g/>
)	)	kIx)	)
/	/	kIx~	/
tolou	tolou	k6eAd1	tolou
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
divoký	divoký	k2eAgMnSc1d1	divoký
<g/>
)	)	kIx)	)
jihokarelsky	jihokarelsky	k6eAd1	jihokarelsky
<g/>
:	:	kIx,	:
pedru	pedr	k1gMnSc3	pedr
komijsky	komijsky	k6eAd1	komijsky
<g/>
:	:	kIx,	:
kӧ	kӧ	k?	kӧ
korjacky	korjacka	k1gFnSc2	korjacka
<g/>
:	:	kIx,	:
qojanga	qojanga	k1gFnSc1	qojanga
<g/>
,	,	kIx,	,
qor	qor	k?	qor
(	(	kIx(	(
<g/>
domácí	domácí	k2eAgFnSc1d1	domácí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
älvel	älvel	k1gMnSc1	älvel
(	(	kIx(	(
<g/>
divoký	divoký	k2eAgMnSc1d1	divoký
<g/>
)	)	kIx)	)
livonsky	livonsky	k6eAd1	livonsky
<g/>
:	:	kIx,	:
pū	pū	k?	pū
marijsky	marijsky	k6eAd1	marijsky
<g/>
:	:	kIx,	:
pučy	puč	k2eAgFnPc1d1	puč
mongolsky	mongolsky	k6eAd1	mongolsky
<g/>
:	:	kIx,	:
caa	caa	k?	caa
něnecky	něnecky	k6eAd1	něnecky
<g/>
:	:	kIx,	:
avka	avka	k1gMnSc1	avka
<g/>
,	,	kIx,	,
ty	ten	k3xDgMnPc4	ten
<g />
.	.	kIx.	.
</s>
<s>
nganasansky	nganasansky	k6eAd1	nganasansky
<g/>
:	:	kIx,	:
aku	aku	k?	aku
sámsky	sámska	k1gFnSc2	sámska
(	(	kIx(	(
<g/>
laponsky	laponsky	k6eAd1	laponsky
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
boazu	boaza	k1gFnSc4	boaza
(	(	kIx(	(
<g/>
domácí	domácí	k2eAgMnSc1d1	domácí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
goddi	godd	k1gMnPc1	godd
(	(	kIx(	(
<g/>
divoký	divoký	k2eAgMnSc1d1	divoký
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc1d1	severní
sámština	sámština	k1gFnSc1	sámština
<g/>
)	)	kIx)	)
/	/	kIx~	/
boatsoj	boatsoj	k1gInSc1	boatsoj
(	(	kIx(	(
<g/>
luleská	luleský	k2eAgFnSc1d1	luleský
sámština	sámština	k1gFnSc1	sámština
<g/>
)	)	kIx)	)
/	/	kIx~	/
bå	bå	k?	bå
(	(	kIx(	(
<g/>
piteská	piteský	k2eAgFnSc1d1	piteský
sámština	sámština	k1gFnSc1	sámština
<g/>
)	)	kIx)	)
/	/	kIx~	/
bå	bå	k?	bå
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
bovtse	bovtse	k1gFnSc1	bovtse
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc1d1	jižní
sámština	sámština	k1gFnSc1	sámština
<g/>
)	)	kIx)	)
/	/	kIx~	/
puäʒ	puäʒ	k?	puäʒ
(	(	kIx(	(
<g/>
domácí	domácí	k2eAgFnSc1d1	domácí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kââ'	kââ'	k?	kââ'
<g/>
,	,	kIx,	,
kå	kå	k?	kå
(	(	kIx(	(
<g/>
divoký	divoký	k2eAgMnSc1d1	divoký
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
skoltská	skoltský	k2eAgFnSc1d1	skoltský
sámština	sámština	k1gFnSc1	sámština
<g/>
)	)	kIx)	)
tuvinsky	tuvinsky	k6eAd1	tuvinsky
<g/>
:	:	kIx,	:
ibi	ibi	k?	ibi
udmurtsky	udmurtsky	k6eAd1	udmurtsky
<g/>
:	:	kIx,	:
pidžuj	pidžovat	k5eAaImRp2nS	pidžovat
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
rus	rus	k1gMnSc1	rus
<g/>
.	.	kIx.	.
pižik	pižik	k1gMnSc1	pižik
–	–	k?	–
"	"	kIx"	"
<g/>
sobí	sobí	k2eAgNnSc1d1	sobí
mládě	mládě	k1gNnSc1	mládě
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Sob	sob	k1gMnSc1	sob
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
subpolárních	subpolární	k2eAgFnPc6d1	subpolární
až	až	k8xS	až
polárních	polární	k2eAgFnPc6d1	polární
částech	část	k1gFnPc6	část
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
od	od	k7c2	od
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
přes	přes	k7c4	přes
celou	celý	k2eAgFnSc4d1	celá
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
,	,	kIx,	,
Aljašku	Aljaška	k1gFnSc4	Aljaška
a	a	k8xC	a
sever	sever	k1gInSc4	sever
Kanady	Kanada	k1gFnSc2	Kanada
až	až	k9	až
do	do	k7c2	do
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
euroasijská	euroasijský	k2eAgFnSc1d1	euroasijská
forma	forma	k1gFnSc1	forma
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
sob	sob	k1gMnSc1	sob
<g/>
,	,	kIx,	,
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
znám	znám	k2eAgInSc1d1	znám
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
karibu	karibu	k1gMnSc1	karibu
(	(	kIx(	(
<g/>
caribou	cariba	k1gMnSc7	cariba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
rozsáhlém	rozsáhlý	k2eAgNnSc6d1	rozsáhlé
území	území	k1gNnSc6	území
tvoří	tvořit	k5eAaImIp3nS	tvořit
osm	osm	k4xCc4	osm
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
:	:	kIx,	:
Rangifer	Rangifer	k1gMnSc1	Rangifer
tarandus	tarandus	k1gMnSc1	tarandus
tarandus	tarandus	k1gMnSc1	tarandus
<g/>
:	:	kIx,	:
Sever	sever	k1gInSc1	sever
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
poloostrov	poloostrov	k1gInSc1	poloostrov
Tajmyr	Tajmyr	k1gInSc1	Tajmyr
<g/>
,	,	kIx,	,
tundra	tundra	k1gFnSc1	tundra
severní	severní	k2eAgFnSc2d1	severní
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
,	,	kIx,	,
Čukotka	Čukotka	k1gFnSc1	Čukotka
R.	R.	kA	R.
t.	t.	k?	t.
platyrhynchus	platyrhynchus	k1gInSc1	platyrhynchus
<g/>
:	:	kIx,	:
Špicberky	Špicberky	k1gFnPc1	Špicberky
<g/>
,	,	kIx,	,
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
vyhubením	vyhubení	k1gNnSc7	vyhubení
R.	R.	kA	R.
t.	t.	k?	t.
fennicus	fennicus	k1gInSc1	fennicus
<g/>
:	:	kIx,	:
Finsko	Finsko	k1gNnSc1	Finsko
a	a	k8xC	a
Karélie	Karélie	k1gFnSc1	Karélie
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
vyhubením	vyhubení	k1gNnSc7	vyhubení
R.	R.	kA	R.
t.	t.	k?	t.
valentinae	valentinae	k1gInSc1	valentinae
<g/>
:	:	kIx,	:
Původně	původně	k6eAd1	původně
od	od	k7c2	od
Uralu	Ural	k1gInSc2	Ural
přes	přes	k7c4	přes
celou	celý	k2eAgFnSc4d1	celá
Sibiř	Sibiř	k1gFnSc4	Sibiř
až	až	k9	až
k	k	k7c3	k
Amuru	Amur	k1gInSc3	Amur
<g/>
,	,	kIx,	,
severní	severní	k2eAgNnSc1d1	severní
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
jen	jen	k9	jen
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Altaj	Altaj	k1gInSc1	Altaj
<g/>
,	,	kIx,	,
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
vyhubením	vyhubení	k1gNnSc7	vyhubení
R.	R.	kA	R.
t.	t.	k?	t.
granti	granti	k1gNnSc1	granti
<g/>
:	:	kIx,	:
Aljaška	Aljaška	k1gFnSc1	Aljaška
R.	R.	kA	R.
t.	t.	k?	t.
groenlandicus	groenlandicus	k1gInSc1	groenlandicus
<g/>
:	:	kIx,	:
Baffinův	Baffinův	k2eAgInSc1d1	Baffinův
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
sever	sever	k1gInSc1	sever
kanadské	kanadský	k2eAgFnSc2d1	kanadská
<g />
.	.	kIx.	.
</s>
<s>
pevniny	pevnina	k1gFnPc1	pevnina
<g/>
,	,	kIx,	,
východní	východní	k2eAgNnSc1d1	východní
Grónsko	Grónsko	k1gNnSc1	Grónsko
R.	R.	kA	R.
t.	t.	k?	t.
pearyi	pearyi	k1gNnSc1	pearyi
<g/>
:	:	kIx,	:
Ellesmerův	Ellesmerův	k2eAgInSc1d1	Ellesmerův
ostrov	ostrov	k1gInSc1	ostrov
a	a	k8xC	a
přilehlé	přilehlý	k2eAgInPc1d1	přilehlý
ostrovy	ostrov	k1gInPc1	ostrov
kanadské	kanadský	k2eAgFnSc2d1	kanadská
Arktidy	Arktida	k1gFnSc2	Arktida
<g/>
,	,	kIx,	,
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
vyhubením	vyhubení	k1gNnSc7	vyhubení
R.	R.	kA	R.
t.	t.	k?	t.
caribou	cariba	k1gMnSc7	cariba
<g/>
:	:	kIx,	:
Od	od	k7c2	od
východní	východní	k2eAgFnSc2d1	východní
Aljašky	Aljaška	k1gFnSc2	Aljaška
a	a	k8xC	a
Britské	britský	k2eAgFnSc2d1	britská
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
po	po	k7c4	po
Labrador	Labrador	k1gInSc4	Labrador
R.	R.	kA	R.
t.	t.	k?	t.
dawsoni	dawson	k1gMnPc1	dawson
<g/>
:	:	kIx,	:
Ostrovy	ostrov	k1gInPc1	ostrov
královny	královna	k1gFnSc2	královna
Charlotty	Charlotta	k1gFnSc2	Charlotta
<g/>
,	,	kIx,	,
vyhuben	vyhuben	k2eAgInSc4d1	vyhuben
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g />
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejmenší	malý	k2eAgInPc4d3	nejmenší
poddruhy	poddruh	k1gInPc4	poddruh
patří	patřit	k5eAaImIp3nP	patřit
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
:	:	kIx,	:
R.	R.	kA	R.
t.	t.	k?	t.
dawsoni	dawsoň	k1gFnSc3	dawsoň
<g/>
,	,	kIx,	,
R.	R.	kA	R.
t.	t.	k?	t.
platyrhynchus	platyrhynchus	k1gInSc1	platyrhynchus
a	a	k8xC	a
R.	R.	kA	R.
t.	t.	k?	t.
pearyi	peary	k1gFnSc6	peary
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
největší	veliký	k2eAgMnPc4d3	veliký
jsou	být	k5eAaImIp3nP	být
R.	R.	kA	R.
t.	t.	k?	t.
valentinae	valentinae	k1gInSc1	valentinae
<g/>
,	,	kIx,	,
R.	R.	kA	R.
t.	t.	k?	t.
granti	granť	k1gFnSc2	granť
a	a	k8xC	a
R.	R.	kA	R.
t.	t.	k?	t.
caribou	cariba	k1gMnSc7	cariba
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgFnSc7d1	obývající
spíše	spíše	k9	spíše
lesnaté	lesnatý	k2eAgFnPc1d1	lesnatá
oblasti	oblast	k1gFnPc1	oblast
při	při	k7c6	při
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
areálu	areál	k1gInSc2	areál
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Sob	sob	k1gMnSc1	sob
je	být	k5eAaImIp3nS	být
geologicky	geologicky	k6eAd1	geologicky
starý	starý	k2eAgInSc1d1	starý
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
mezi	mezi	k7c7	mezi
jelenovitými	jelenovití	k1gMnPc7	jelenovití
izolované	izolovaný	k2eAgNnSc1d1	izolované
postavení	postavení	k1gNnSc3	postavení
<g/>
,	,	kIx,	,
nejblíže	blízce	k6eAd3	blízce
je	být	k5eAaImIp3nS	být
příbuzný	příbuzný	k1gMnSc1	příbuzný
severoamerickým	severoamerický	k2eAgMnPc3d1	severoamerický
jelencům	jelenec	k1gInPc3	jelenec
rodu	rod	k1gInSc2	rod
Odocoileus	Odocoileus	k1gMnSc1	Odocoileus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
200	[number]	k4	200
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
před	před	k7c7	před
120	[number]	k4	120
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
formovat	formovat	k5eAaImF	formovat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
v	v	k7c6	v
pleistocénu	pleistocén	k1gInSc6	pleistocén
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
rozšíření	rozšíření	k1gNnSc1	rozšíření
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
a	a	k8xC	a
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
<g/>
,	,	kIx,	,
před	před	k7c4	před
cca	cca	kA	cca
25	[number]	k4	25
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sobi	sob	k1gMnPc1	sob
pásli	pásnout	k5eAaImAgMnP	pásnout
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
mamutů	mamut	k1gMnPc2	mamut
i	i	k9	i
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Sob	sob	k1gMnSc1	sob
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
90	[number]	k4	90
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
cm	cm	kA	cm
<g/>
,	,	kIx,	,
délky	délka	k1gFnPc1	délka
těla	tělo	k1gNnSc2	tělo
asi	asi	k9	asi
180	[number]	k4	180
cm	cm	kA	cm
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
230	[number]	k4	230
<g/>
–	–	k?	–
<g/>
400	[number]	k4	400
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
jelenovitých	jelenovití	k1gMnPc2	jelenovití
nosí	nosit	k5eAaImIp3nS	nosit
u	u	k7c2	u
soba	sob	k1gMnSc2	sob
parohy	paroh	k1gInPc7	paroh
samec	samec	k1gMnSc1	samec
i	i	k8xC	i
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Samičí	samičí	k2eAgInPc1d1	samičí
parohy	paroh	k1gInPc1	paroh
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
mnohem	mnohem	k6eAd1	mnohem
slabší	slabý	k2eAgInPc4d2	slabší
než	než	k8xS	než
samčí	samčí	k2eAgInPc4d1	samčí
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
však	však	k9	však
shazuje	shazovat	k5eAaImIp3nS	shazovat
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
tvar	tvar	k1gInSc1	tvar
se	se	k3xPyFc4	se
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
poddruhů	poddruh	k1gInPc2	poddruh
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
však	však	k9	však
jsou	být	k5eAaImIp3nP	být
obloukovitě	obloukovitě	k6eAd1	obloukovitě
zahnuté	zahnutý	k2eAgFnPc1d1	zahnutá
a	a	k8xC	a
silně	silně	k6eAd1	silně
rozvětvené	rozvětvený	k2eAgFnPc4d1	rozvětvená
<g/>
,	,	kIx,	,
přední	přední	k2eAgFnPc4d1	přední
výsady	výsada	k1gFnPc4	výsada
bývají	bývat	k5eAaImIp3nP	bývat
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
zploštělé	zploštělý	k2eAgFnSc2d1	zploštělá
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
větví	větvit	k5eAaImIp3nS	větvit
<g/>
,	,	kIx,	,
zvíře	zvíře	k1gNnSc1	zvíře
jimi	on	k3xPp3gInPc7	on
může	moct	k5eAaImIp3nS	moct
odhrabovat	odhrabovat	k5eAaImF	odhrabovat
sníh	sníh	k1gInSc1	sníh
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nohách	noha	k1gFnPc6	noha
má	mít	k5eAaImIp3nS	mít
sob	sob	k1gMnSc1	sob
široké	široký	k2eAgInPc4d1	široký
paznehty	pazneht	k1gInPc4	pazneht
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
neboří	bořit	k5eNaImIp3nS	bořit
do	do	k7c2	do
sněhu	sníh	k1gInSc2	sníh
a	a	k8xC	a
bahna	bahno	k1gNnSc2	bahno
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
stopy	stopa	k1gFnPc1	stopa
mají	mít	k5eAaImIp3nP	mít
téměř	téměř	k6eAd1	téměř
kruhový	kruhový	k2eAgInSc4d1	kruhový
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
soba	sob	k1gMnSc2	sob
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
podle	podle	k7c2	podle
ročního	roční	k2eAgNnSc2d1	roční
období	období	k1gNnSc2	období
i	i	k9	i
podle	podle	k7c2	podle
poddruhu	poddruh	k1gInSc2	poddruh
<g/>
,	,	kIx,	,
zimní	zimní	k2eAgFnSc1d1	zimní
srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
světle	světle	k6eAd1	světle
šedá	šedý	k2eAgFnSc1d1	šedá
až	až	k8xS	až
šedobílá	šedobílý	k2eAgFnSc1d1	šedobílá
<g/>
,	,	kIx,	,
letní	letní	k2eAgFnSc1d1	letní
spíše	spíše	k9	spíše
šedohnědá	šedohnědý	k2eAgFnSc1d1	šedohnědá
<g/>
,	,	kIx,	,
u	u	k7c2	u
lesních	lesní	k2eAgMnPc2d1	lesní
sobů	sob	k1gMnPc2	sob
až	až	k6eAd1	až
červenohnědá	červenohnědý	k2eAgFnSc1d1	červenohnědá
<g/>
,	,	kIx,	,
obřitek	obřitek	k1gInSc1	obřitek
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
krk	krk	k1gInSc4	krk
<g/>
,	,	kIx,	,
s	s	k7c7	s
výraznou	výrazný	k2eAgFnSc7d1	výrazná
hřívou	hříva	k1gFnSc7	hříva
<g/>
,	,	kIx,	,
mívá	mívat	k5eAaImIp3nS	mívat
světlejší	světlý	k2eAgFnSc4d2	světlejší
barvu	barva	k1gFnSc4	barva
než	než	k8xS	než
zbytek	zbytek	k1gInSc4	zbytek
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Nejsvětlejší	světlý	k2eAgMnSc1d3	nejsvětlejší
jsou	být	k5eAaImIp3nP	být
sobi	sob	k1gMnPc1	sob
poddruhu	poddruh	k1gInSc2	poddruh
Rangifer	Rangifer	k1gInSc1	Rangifer
tarandus	tarandus	k1gInSc1	tarandus
Pearyi	Peary	k1gMnPc1	Peary
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
celoročně	celoročně	k6eAd1	celoročně
bělavé	bělavý	k2eAgNnSc4d1	bělavé
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgMnPc1d1	domácí
sobi	sob	k1gMnPc1	sob
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
strakatí	strakatý	k2eAgMnPc1d1	strakatý
nebo	nebo	k8xC	nebo
i	i	k9	i
čistě	čistě	k6eAd1	čistě
bílí	bílý	k2eAgMnPc1d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Sobi	sob	k1gMnPc1	sob
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
stádech	stádo	k1gNnPc6	stádo
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
době	doba	k1gFnSc6	doba
říje	říje	k1gFnSc2	říje
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
srpna	srpen	k1gInSc2	srpen
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
samci	samec	k1gMnPc1	samec
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
teritoria	teritorium	k1gNnPc4	teritorium
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgFnPc6	jenž
obhajují	obhajovat	k5eAaImIp3nP	obhajovat
harém	harém	k1gInSc1	harém
laní	laní	k2eAgInSc1d1	laní
<g/>
.	.	kIx.	.
</s>
<s>
Ozývají	ozývat	k5eAaImIp3nP	ozývat
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
chraplavým	chraplavý	k2eAgNnSc7d1	chraplavé
až	až	k8xS	až
chrochtavým	chrochtavý	k2eAgNnSc7d1	chrochtavé
troubením	troubení	k1gNnSc7	troubení
a	a	k8xC	a
bojují	bojovat	k5eAaImIp3nP	bojovat
spolu	spolu	k6eAd1	spolu
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xC	jako
naši	náš	k3xOp1gMnPc1	náš
jeleni	jelen	k1gMnPc1	jelen
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
svého	svůj	k3xOyFgInSc2	svůj
rozšíření	rozšíření	k1gNnPc1	rozšíření
sobi	sob	k1gMnPc1	sob
migrují	migrovat	k5eAaImIp3nP	migrovat
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
na	na	k7c6	na
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
několika	několik	k4yIc2	několik
tisíc	tisíc	k4xCgInPc2	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
(	(	kIx(	(
<g/>
sobi	sob	k1gMnPc1	sob
karibu	karibu	k1gMnSc2	karibu
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
léto	léto	k1gNnSc4	léto
se	se	k3xPyFc4	se
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
nejraději	rád	k6eAd3	rád
v	v	k7c6	v
tundře	tundra	k1gFnSc6	tundra
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
horských	horský	k2eAgFnPc6d1	horská
polohách	poloha	k1gFnPc6	poloha
(	(	kIx(	(
<g/>
Skandinávie	Skandinávie	k1gFnSc1	Skandinávie
<g/>
,	,	kIx,	,
Altaj	Altaj	k1gInSc1	Altaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
se	se	k3xPyFc4	se
stěhují	stěhovat	k5eAaImIp3nP	stěhovat
k	k	k7c3	k
hranici	hranice	k1gFnSc3	hranice
severského	severský	k2eAgInSc2d1	severský
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
skutečností	skutečnost	k1gFnSc7	skutečnost
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
počítat	počítat	k5eAaImF	počítat
i	i	k9	i
při	při	k7c6	při
chovu	chov	k1gInSc6	chov
domestikovaných	domestikovaný	k2eAgMnPc2d1	domestikovaný
sobů	sob	k1gMnPc2	sob
<g/>
.	.	kIx.	.
</s>
<s>
Sobi	sob	k1gMnPc1	sob
se	se	k3xPyFc4	se
při	při	k7c6	při
migraci	migrace	k1gFnSc6	migrace
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
rychlosti	rychlost	k1gFnSc3	rychlost
až	až	k9	až
51	[number]	k4	51
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Sobi	sob	k1gMnPc1	sob
mají	mít	k5eAaImIp3nP	mít
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc4	schopnost
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
UV	UV	kA	UV
oboru	obor	k1gInSc6	obor
spektra	spektrum	k1gNnSc2	spektrum
(	(	kIx(	(
<g/>
330	[number]	k4	330
nm	nm	k?	nm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
odhalit	odhalit	k5eAaPmF	odhalit
predátory	predátor	k1gMnPc4	predátor
(	(	kIx(	(
<g/>
vlky	vlk	k1gMnPc7	vlk
<g/>
)	)	kIx)	)
a	a	k8xC	a
potravu	potrava	k1gFnSc4	potrava
(	(	kIx(	(
<g/>
lišejníky	lišejník	k1gInPc1	lišejník
<g/>
)	)	kIx)	)
–	–	k?	–
vidí	vidět	k5eAaImIp3nS	vidět
je	on	k3xPp3gFnPc4	on
tmavé	tmavý	k2eAgFnPc4d1	tmavá
na	na	k7c6	na
světlém	světlý	k2eAgInSc6d1	světlý
(	(	kIx(	(
<g/>
sněhovém	sněhový	k2eAgInSc6d1	sněhový
<g/>
)	)	kIx)	)
podkladu	podklad	k1gInSc6	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
kontrastně	kontrastně	k6eAd1	kontrastně
vidí	vidět	k5eAaImIp3nS	vidět
také	také	k9	také
moč	moč	k1gFnSc1	moč
(	(	kIx(	(
<g/>
vlků	vlk	k1gMnPc2	vlk
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgMnPc2d1	jiný
sobů	sob	k1gMnPc2	sob
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sob	sob	k1gMnSc1	sob
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
především	především	k9	především
bylinami	bylina	k1gFnPc7	bylina
a	a	k8xC	a
lišejníky	lišejník	k1gInPc7	lišejník
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
si	se	k3xPyFc3	se
dovede	dovést	k5eAaPmIp3nS	dovést
zpod	zpod	k7c2	zpod
sněhu	sníh	k1gInSc2	sníh
vyhrabat	vyhrabat	k5eAaPmF	vyhrabat
i	i	k9	i
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
parohů	paroh	k1gInPc2	paroh
a	a	k8xC	a
předních	přední	k2eAgFnPc2d1	přední
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
však	však	k9	však
žere	žrát	k5eAaImIp3nS	žrát
také	také	k9	také
lumíky	lumík	k1gMnPc4	lumík
nebo	nebo	k8xC	nebo
mláďata	mládě	k1gNnPc4	mládě
mořských	mořský	k2eAgMnPc2d1	mořský
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
na	na	k7c6	na
mořském	mořský	k2eAgNnSc6d1	mořské
pobřeží	pobřeží	k1gNnSc6	pobřeží
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
líže	lízat	k5eAaImIp3nS	lízat
sůl	sůl	k1gFnSc1	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
predátorem	predátor	k1gMnSc7	predátor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
loví	lovit	k5eAaImIp3nS	lovit
soby	sob	k1gMnPc4	sob
je	být	k5eAaImIp3nS	být
vlk	vlk	k1gMnSc1	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Vlci	vlk	k1gMnPc1	vlk
se	se	k3xPyFc4	se
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
snaží	snažit	k5eAaImIp3nS	snažit
oddělit	oddělit	k5eAaPmF	oddělit
od	od	k7c2	od
stáda	stádo	k1gNnSc2	stádo
mláďata	mládě	k1gNnPc1	mládě
nebo	nebo	k8xC	nebo
přestárlá	přestárlý	k2eAgFnSc1d1	přestárlá
<g/>
,	,	kIx,	,
nemocná	nemocný	k2eAgFnSc1d1	nemocná
či	či	k8xC	či
poraněná	poraněný	k2eAgNnPc1d1	poraněné
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
potom	potom	k6eAd1	potom
obklíčí	obklíčit	k5eAaPmIp3nP	obklíčit
a	a	k8xC	a
strhnou	strhnout	k5eAaPmIp3nP	strhnout
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc4	mládě
nebo	nebo	k8xC	nebo
slabá	slabý	k2eAgNnPc4d1	slabé
zvířata	zvíře	k1gNnPc4	zvíře
může	moct	k5eAaImIp3nS	moct
ulovit	ulovit	k5eAaPmF	ulovit
také	také	k9	také
rosomák	rosomák	k1gMnSc1	rosomák
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Sibiři	Sibiř	k1gFnSc6	Sibiř
i	i	k9	i
tygr	tygr	k1gMnSc1	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Sobům	sob	k1gMnPc3	sob
znepříjemňují	znepříjemňovat	k5eAaImIp3nP	znepříjemňovat
život	život	k1gInSc4	život
také	také	k9	také
komáři	komár	k1gMnPc1	komár
<g/>
,	,	kIx,	,
ovádi	ovád	k1gMnPc1	ovád
a	a	k8xC	a
muchničky	muchnička	k1gFnPc1	muchnička
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
přítomnost	přítomnost	k1gFnSc1	přítomnost
mnohdy	mnohdy	k6eAd1	mnohdy
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
jarní	jarní	k2eAgFnSc4d1	jarní
migraci	migrace	k1gFnSc4	migrace
<g/>
.	.	kIx.	.
</s>
<s>
Nejnebezpečnějšími	bezpečný	k2eNgMnPc7d3	nejnebezpečnější
parazity	parazit	k1gMnPc7	parazit
soba	sob	k1gMnSc2	sob
jsou	být	k5eAaImIp3nP	být
larvy	larva	k1gFnPc1	larva
střečků	střeček	k1gMnPc2	střeček
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jim	on	k3xPp3gMnPc3	on
cizopasí	cizopasit	k5eAaImIp3nP	cizopasit
v	v	k7c6	v
nosních	nosní	k2eAgFnPc6d1	nosní
dutinách	dutina	k1gFnPc6	dutina
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
kůží	kůže	k1gFnSc7	kůže
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
paleolitu	paleolit	k1gInSc2	paleolit
loví	lovit	k5eAaImIp3nP	lovit
soby	soba	k1gFnPc1	soba
také	také	k9	také
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
pochopitelně	pochopitelně	k6eAd1	pochopitelně
jejich	jejich	k3xOp3gMnPc7	jejich
nejhoršími	zlý	k2eAgMnPc7d3	nejhorší
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Ameriky	Amerika	k1gFnSc2	Amerika
je	být	k5eAaImIp3nS	být
chován	chovat	k5eAaImNgInS	chovat
na	na	k7c4	na
mléko	mléko	k1gNnSc4	mléko
<g/>
,	,	kIx,	,
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
k	k	k7c3	k
tahu	tah	k1gInSc3	tah
<g/>
,	,	kIx,	,
jízdě	jízda	k1gFnSc6	jízda
i	i	k8xC	i
nošení	nošení	k1gNnSc6	nošení
břemen	břemeno	k1gNnPc2	břemeno
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
písemné	písemný	k2eAgInPc4d1	písemný
záznamy	záznam	k1gInPc4	záznam
o	o	k7c6	o
divokých	divoký	k2eAgMnPc6d1	divoký
sobech	sob	k1gMnPc6	sob
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
sezónní	sezónní	k2eAgFnSc4d1	sezónní
barvoměnu	barvoměna	k1gFnSc4	barvoměna
jeho	jeho	k3xOp3gFnSc2	jeho
srsti	srst	k1gFnSc2	srst
a	a	k8xC	a
v	v	k7c6	v
Caesarových	Caesarových	k2eAgInPc6d1	Caesarových
Zápiscích	zápisek	k1gInPc6	zápisek
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
galské	galský	k2eAgFnSc6d1	galská
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
popisován	popisovat	k5eAaImNgInS	popisovat
jako	jako	k9	jako
druh	druh	k1gInSc1	druh
jednorožce	jednorožec	k1gMnSc2	jednorožec
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgNnSc1d1	připomínající
zpola	zpola	k6eAd1	zpola
jelena	jelen	k1gMnSc4	jelen
a	a	k8xC	a
zpola	zpola	k6eAd1	zpola
býka	býk	k1gMnSc2	býk
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
domestikaci	domestikace	k1gFnSc6	domestikace
sobů	sob	k1gMnPc2	sob
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
ze	z	k7c2	z
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
norský	norský	k2eAgMnSc1d1	norský
náčelník	náčelník	k1gMnSc1	náčelník
Ottar	Ottar	k1gMnSc1	Ottar
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
anglosaskému	anglosaský	k2eAgMnSc3d1	anglosaský
králi	král	k1gMnSc3	král
Alfrédu	Alfréd	k1gMnSc3	Alfréd
Velikému	veliký	k2eAgMnSc3d1	veliký
o	o	k7c6	o
svých	svůj	k3xOyFgNnPc6	svůj
sobích	sobí	k2eAgNnPc6d1	sobí
stádech	stádo	k1gNnPc6	stádo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1555	[number]	k4	1555
podal	podat	k5eAaPmAgMnS	podat
švédský	švédský	k2eAgMnSc1d1	švédský
duchovní	duchovní	k1gMnSc1	duchovní
Olaus	Olaus	k1gMnSc1	Olaus
Magnus	Magnus	k1gMnSc1	Magnus
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
spisu	spis	k1gInSc6	spis
Dějiny	dějiny	k1gFnPc1	dějiny
severského	severský	k2eAgInSc2d1	severský
lidu	lid	k1gInSc2	lid
první	první	k4xOgFnSc4	první
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
chovu	chov	k1gInSc6	chov
sobů	sob	k1gMnPc2	sob
u	u	k7c2	u
Sámů	Sámo	k1gMnPc2	Sámo
<g/>
.	.	kIx.	.
živého	živý	k2eAgMnSc4d1	živý
soba	sob	k1gMnSc4	sob
však	však	k9	však
zřejmě	zřejmě	k6eAd1	zřejmě
nikdy	nikdy	k6eAd1	nikdy
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jej	on	k3xPp3gMnSc4	on
na	na	k7c6	na
doprovodné	doprovodný	k2eAgFnSc6d1	doprovodná
ilustraci	ilustrace	k1gFnSc6	ilustrace
zobrazil	zobrazit	k5eAaPmAgInS	zobrazit
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
parohy	paroh	k1gInPc7	paroh
<g/>
.	.	kIx.	.
</s>
<s>
Užitek	užitek	k1gInSc1	užitek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
sobi	sob	k1gMnPc1	sob
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
člověku	člověk	k1gMnSc3	člověk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mnohostranný	mnohostranný	k2eAgMnSc1d1	mnohostranný
<g/>
.	.	kIx.	.
</s>
<s>
Sobí	sobí	k2eAgNnSc1d1	sobí
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
lůj	lůj	k1gInSc1	lůj
<g/>
,	,	kIx,	,
krev	krev	k1gFnSc1	krev
<g/>
,	,	kIx,	,
mléko	mléko	k1gNnSc1	mléko
a	a	k8xC	a
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
připravené	připravený	k2eAgInPc1d1	připravený
sýry	sýr	k1gInPc1	sýr
slouží	sloužit	k5eAaImIp3nP	sloužit
obyvatelům	obyvatel	k1gMnPc3	obyvatel
Dalekého	daleký	k2eAgInSc2d1	daleký
severu	sever	k1gInSc2	sever
jako	jako	k8xS	jako
základní	základní	k2eAgFnSc1d1	základní
potravina	potravina	k1gFnSc1	potravina
<g/>
,	,	kIx,	,
Čukčové	Čukč	k1gMnPc1	Čukč
rádi	rád	k2eAgMnPc1d1	rád
jedí	jíst	k5eAaImIp3nP	jíst
dokonce	dokonce	k9	dokonce
i	i	k9	i
natrávený	natrávený	k2eAgInSc1d1	natrávený
lišejník	lišejník	k1gInSc1	lišejník
z	z	k7c2	z
bachoru	bachor	k1gInSc2	bachor
zabitých	zabitý	k2eAgNnPc2d1	zabité
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Sobí	sobí	k2eAgNnSc1d1	sobí
mléko	mléko	k1gNnSc1	mléko
se	s	k7c7	s
chutí	chuť	k1gFnSc7	chuť
a	a	k8xC	a
tučností	tučnost	k1gFnSc7	tučnost
podobá	podobat	k5eAaImIp3nS	podobat
ovčímu	ovčí	k2eAgInSc3d1	ovčí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
sladší	sladký	k2eAgNnSc1d2	sladší
<g/>
.	.	kIx.	.
</s>
<s>
Sobí	sobí	k2eAgFnSc1d1	sobí
laň	laň	k1gFnSc1	laň
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
a	a	k8xC	a
po	po	k7c4	po
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
jen	jen	k9	jen
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
měsíce	měsíc	k1gInSc2	měsíc
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
odstavení	odstavení	k1gNnSc6	odstavení
mláděte	mládě	k1gNnSc2	mládě
přestává	přestávat	k5eAaImIp3nS	přestávat
mléko	mléko	k1gNnSc4	mléko
produkovat	produkovat	k5eAaImF	produkovat
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všechna	všechen	k3xTgNnPc4	všechen
etnika	etnikum	k1gNnPc4	etnikum
<g/>
,	,	kIx,	,
chovající	chovající	k2eAgFnPc4d1	chovající
soby	soba	k1gFnPc4	soba
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
tato	tento	k3xDgNnPc1	tento
zvířata	zvíře	k1gNnPc1	zvíře
dojí	dojit	k5eAaImIp3nP	dojit
<g/>
.	.	kIx.	.
</s>
<s>
Kůže	kůže	k1gFnSc1	kůže
a	a	k8xC	a
kožešina	kožešina	k1gFnSc1	kožešina
má	mít	k5eAaImIp3nS	mít
mnohostranné	mnohostranný	k2eAgNnSc4d1	mnohostranné
použití	použití	k1gNnSc4	použití
<g/>
,	,	kIx,	,
zhotovují	zhotovovat	k5eAaImIp3nP	zhotovovat
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
stanové	stanový	k2eAgFnSc2d1	stanová
plachty	plachta	k1gFnSc2	plachta
<g/>
,	,	kIx,	,
koberečky	kobereček	k1gInPc4	kobereček
<g/>
,	,	kIx,	,
řemeny	řemen	k1gInPc4	řemen
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
tulení	tulení	k1gNnSc1	tulení
<g/>
"	"	kIx"	"
pásy	pás	k1gInPc1	pás
na	na	k7c4	na
lyže	lyže	k1gFnPc4	lyže
<g/>
,	,	kIx,	,
na	na	k7c4	na
oděvy	oděv	k1gInPc4	oděv
je	být	k5eAaImIp3nS	být
nejvhodnější	vhodný	k2eAgFnSc1d3	nejvhodnější
kožešina	kožešina	k1gFnSc1	kožešina
sobího	sobí	k2eAgNnSc2d1	sobí
mláděte	mládě	k1gNnSc2	mládě
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
pižíka	pižík	k1gMnSc2	pižík
<g/>
.	.	kIx.	.
</s>
<s>
Inuité	Inuitý	k2eAgFnSc3d1	Inuitý
sobí	sobí	k2eAgFnSc3d1	sobí
kůži	kůže	k1gFnSc3	kůže
vyčiňují	vyčiňovat	k5eAaImIp3nP	vyčiňovat
pomocí	pomocí	k7c2	pomocí
kouře	kouř	k1gInSc2	kouř
a	a	k8xC	a
sobího	sobí	k2eAgInSc2d1	sobí
mozku	mozek	k1gInSc2	mozek
nebo	nebo	k8xC	nebo
tuleního	tulení	k2eAgInSc2d1	tulení
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
výsledek	výsledek	k1gInSc1	výsledek
<g/>
,	,	kIx,	,
nehledě	hledět	k5eNaImSgMnS	hledět
k	k	k7c3	k
nepříjemnému	příjemný	k2eNgInSc3d1	nepříjemný
pachu	pach	k1gInSc3	pach
<g/>
,	,	kIx,	,
překvapivě	překvapivě	k6eAd1	překvapivě
připomíná	připomínat	k5eAaImIp3nS	připomínat
semiš	semiš	k1gInSc1	semiš
<g/>
.	.	kIx.	.
</s>
<s>
Sobí	sobí	k2eAgFnSc1d1	sobí
srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
spřádat	spřádat	k5eAaImF	spřádat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dělají	dělat	k5eAaImIp3nP	dělat
například	například	k6eAd1	například
Jakuti	Jakut	k1gMnPc1	Jakut
<g/>
.	.	kIx.	.
</s>
<s>
Sobí	sobí	k2eAgInPc1d1	sobí
parohy	paroh	k1gInPc1	paroh
a	a	k8xC	a
kosti	kost	k1gFnPc1	kost
našly	najít	k5eAaPmAgFnP	najít
použití	použití	k1gNnSc4	použití
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
luků	luk	k1gInPc2	luk
<g/>
,	,	kIx,	,
šípových	šípový	k2eAgInPc2d1	šípový
hrotů	hrot	k1gInPc2	hrot
<g/>
,	,	kIx,	,
rybářských	rybářský	k2eAgInPc2d1	rybářský
háčků	háček	k1gInPc2	háček
<g/>
,	,	kIx,	,
lopat	lopata	k1gFnPc2	lopata
a	a	k8xC	a
dalšího	další	k2eAgNnSc2d1	další
nářadí	nářadí	k1gNnSc2	nářadí
<g/>
,	,	kIx,	,
ze	z	k7c2	z
šlach	šlacha	k1gFnPc2	šlacha
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
provázky	provázek	k1gInPc1	provázek
<g/>
,	,	kIx,	,
tětivy	tětiva	k1gFnPc1	tětiva
luků	luk	k1gInPc2	luk
<g/>
,	,	kIx,	,
pružiny	pružina	k1gFnSc2	pružina
pastí	past	k1gFnPc2	past
nebo	nebo	k8xC	nebo
niti	nit	k1gFnSc2	nit
<g/>
.	.	kIx.	.
</s>
<s>
Sob	sob	k1gMnSc1	sob
má	mít	k5eAaImIp3nS	mít
především	především	k6eAd1	především
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
význam	význam	k1gInSc4	význam
i	i	k8xC	i
jako	jako	k9	jako
tažné	tažný	k2eAgNnSc1d1	tažné
a	a	k8xC	a
jezdecké	jezdecký	k2eAgNnSc1d1	jezdecké
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Saně	saně	k1gFnSc1	saně
<g/>
,	,	kIx,	,
tažené	tažený	k2eAgNnSc1d1	tažené
sobím	sobí	k2eAgNnSc7d1	sobí
spřežením	spřežení	k1gNnSc7	spřežení
jsou	být	k5eAaImIp3nP	být
tradičním	tradiční	k2eAgInSc7d1	tradiční
dopravním	dopravní	k2eAgInSc7d1	dopravní
prostředkem	prostředek	k1gInSc7	prostředek
Sámů	Sámo	k1gMnPc2	Sámo
a	a	k8xC	a
domorodých	domorodý	k2eAgMnPc2d1	domorodý
obyvatel	obyvatel	k1gMnPc2	obyvatel
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Něnci	Něnce	k1gMnPc1	Něnce
na	na	k7c6	na
nich	on	k3xPp3gMnPc2	on
po	po	k7c6	po
bažinaté	bažinatý	k2eAgFnSc6d1	bažinatá
tundře	tundra	k1gFnSc6	tundra
jezdili	jezdit	k5eAaImAgMnP	jezdit
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
<g/>
!	!	kIx.	!
</s>
<s>
Od	od	k7c2	od
pol.	pol.	k?	pol.
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
však	však	k9	však
z	z	k7c2	z
použití	použití	k1gNnSc2	použití
vytlačuje	vytlačovat	k5eAaImIp3nS	vytlačovat
sněžný	sněžný	k2eAgInSc4d1	sněžný
skútr	skútr	k1gInSc4	skútr
<g/>
.	.	kIx.	.
</s>
<s>
Evenkové	Evenek	k1gMnPc1	Evenek
<g/>
,	,	kIx,	,
Jakuti	Jakut	k1gMnPc1	Jakut
a	a	k8xC	a
Tuvinci	Tuvinec	k1gMnPc1	Tuvinec
soby	soba	k1gFnSc2	soba
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
nošení	nošení	k1gNnSc3	nošení
nákladů	náklad	k1gInPc2	náklad
a	a	k8xC	a
na	na	k7c6	na
silných	silný	k2eAgInPc6d1	silný
samcích	samec	k1gInPc6	samec
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
vykastrovaných	vykastrovaný	k2eAgFnPc2d1	vykastrovaná
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
jezdí	jezdit	k5eAaImIp3nP	jezdit
<g/>
.	.	kIx.	.
</s>
<s>
Soby	soba	k1gFnPc1	soba
kastrují	kastrovat	k5eAaBmIp3nP	kastrovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
varlata	varle	k1gNnPc1	varle
jednoduše	jednoduše	k6eAd1	jednoduše
ukousnou	ukousnout	k5eAaPmIp3nP	ukousnout
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
etnika	etnikum	k1gNnPc1	etnikum
chovají	chovat	k5eAaImIp3nP	chovat
velké	velký	k2eAgFnPc1d1	velká
<g/>
,	,	kIx,	,
dlouhonohé	dlouhonohý	k2eAgFnPc1d1	dlouhonohá
soby	soba	k1gFnPc1	soba
odvozené	odvozený	k2eAgFnPc1d1	odvozená
od	od	k7c2	od
poddruhu	poddruh	k1gInSc2	poddruh
Rangifer	Rangifer	k1gMnSc1	Rangifer
tarandus	tarandus	k1gMnSc1	tarandus
valentinae	valentinaat	k5eAaPmIp3nS	valentinaat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
drobnější	drobný	k2eAgMnPc1d2	drobnější
sobi	sob	k1gMnPc1	sob
obyvatel	obyvatel	k1gMnPc2	obyvatel
tundry	tundra	k1gFnSc2	tundra
<g/>
,	,	kIx,	,
užívaní	užívaný	k2eAgMnPc1d1	užívaný
jen	jen	k9	jen
k	k	k7c3	k
zápřahu	zápřah	k1gInSc3	zápřah
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
odvozeni	odvodit	k5eAaPmNgMnP	odvodit
od	od	k7c2	od
poddruhu	poddruh	k1gInSc2	poddruh
R.	R.	kA	R.
t.	t.	k?	t.
tarandus	tarandus	k1gInSc1	tarandus
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
sedlání	sedlání	k1gNnSc2	sedlání
soba	soba	k1gFnSc1	soba
<g/>
.	.	kIx.	.
</s>
<s>
Tuvinci	Tuvinec	k1gMnPc1	Tuvinec
a	a	k8xC	a
Tofalarové	Tofalar	k1gMnPc1	Tofalar
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Sibiře	Sibiř	k1gFnSc2	Sibiř
a	a	k8xC	a
Cátani	Cátan	k1gMnPc1	Cátan
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
soby	soba	k1gFnSc2	soba
sedlají	sedlat	k5eAaImIp3nP	sedlat
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
však	však	k9	však
není	být	k5eNaImIp3nS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
anatomii	anatomie	k1gFnSc3	anatomie
soba	sob	k1gMnSc2	sob
moc	moc	k6eAd1	moc
výhodné	výhodný	k2eAgInPc1d1	výhodný
–	–	k?	–
sobu	sob	k1gMnSc6	sob
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
položeným	položený	k2eAgNnSc7d1	položené
sedlem	sedlo	k1gNnSc7	sedlo
příliš	příliš	k6eAd1	příliš
zatěžuje	zatěžovat	k5eAaImIp3nS	zatěžovat
hřbet	hřbet	k1gInSc4	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Evenkvé	Evenkvá	k1gFnPc1	Evenkvá
<g/>
,	,	kIx,	,
Eveni	Even	k1gMnPc1	Even
a	a	k8xC	a
Jakuti	Jakut	k1gMnPc1	Jakut
sedlají	sedlat	k5eAaImIp3nP	sedlat
soba	sob	k1gMnSc4	sob
nad	nad	k7c7	nad
lopatkami	lopatka	k1gFnPc7	lopatka
<g/>
,	,	kIx,	,
nepoužívají	používat	k5eNaImIp3nP	používat
třmeny	třmen	k1gInPc4	třmen
a	a	k8xC	a
ovládají	ovládat	k5eAaImIp3nP	ovládat
jej	on	k3xPp3gNnSc4	on
pomocí	pomocí	k7c2	pomocí
hole	hole	k1gFnSc2	hole
<g/>
.	.	kIx.	.
</s>
<s>
Jezdec	jezdec	k1gInSc1	jezdec
má	mít	k5eAaImIp3nS	mít
při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
stylu	styl	k1gInSc6	styl
jízdy	jízda	k1gFnSc2	jízda
doširoka	doširoka	k6eAd1	doširoka
roztažené	roztažený	k2eAgFnPc4d1	roztažená
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
styl	styl	k1gInSc1	styl
sedlání	sedlání	k1gNnSc2	sedlání
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
soba	sob	k1gMnSc4	sob
vhodnější	vhodný	k2eAgInSc1d2	vhodnější
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
soumarského	soumarský	k2eAgNnSc2d1	soumarské
sedla	sedlo	k1gNnSc2	sedlo
<g/>
,	,	kIx,	,
užívaného	užívaný	k2eAgInSc2d1	užívaný
k	k	k7c3	k
nošení	nošení	k1gNnSc3	nošení
nákladů	náklad	k1gInPc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Sob	sob	k1gMnSc1	sob
má	mít	k5eAaImIp3nS	mít
místo	místo	k1gNnSc4	místo
i	i	k9	i
v	v	k7c6	v
duchovní	duchovní	k2eAgFnSc6d1	duchovní
kultuře	kultura	k1gFnSc6	kultura
severských	severský	k2eAgNnPc2d1	severské
etnik	etnikum	k1gNnPc2	etnikum
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Sámů	Sámo	k1gMnPc2	Sámo
a	a	k8xC	a
domorodých	domorodý	k2eAgMnPc2d1	domorodý
obyvatel	obyvatel	k1gMnPc2	obyvatel
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sibiřském	sibiřský	k2eAgInSc6d1	sibiřský
šamanismu	šamanismus	k1gInSc6	šamanismus
se	se	k3xPyFc4	se
často	často	k6eAd1	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
motiv	motiv	k1gInSc1	motiv
létajícího	létající	k2eAgMnSc2d1	létající
soba	sob	k1gMnSc2	sob
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
šamana	šaman	k1gMnSc4	šaman
jako	jako	k8xS	jako
ochranný	ochranný	k2eAgMnSc1d1	ochranný
duch	duch	k1gMnSc1	duch
<g/>
,	,	kIx,	,
čapky	čapka	k1gFnPc1	čapka
sibiřských	sibiřský	k2eAgMnPc2d1	sibiřský
šamanů	šaman	k1gMnPc2	šaman
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
Selkupů	Selkup	k1gInPc2	Selkup
a	a	k8xC	a
Nganasanů	Nganasan	k1gInPc2	Nganasan
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
bývají	bývat	k5eAaImIp3nP	bývat
zdobeny	zdoben	k2eAgInPc1d1	zdoben
sobími	sobí	k2eAgInPc7d1	sobí
parohy	paroh	k1gInPc7	paroh
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiné	jiný	k2eAgFnSc2d1	jiná
představy	představa	k1gFnSc2	představa
vyprovází	vyprovázet	k5eAaImIp3nS	vyprovázet
sob	sob	k1gMnSc1	sob
duše	duše	k1gFnSc2	duše
zemřelých	zemřelý	k1gMnPc2	zemřelý
na	na	k7c4	na
Onen	onen	k3xDgInSc4	onen
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
Evenkové	Evenek	k1gMnPc1	Evenek
<g/>
,	,	kIx,	,
Něnci	Něnce	k1gMnPc1	Něnce
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Sibiře	Sibiř	k1gFnSc2	Sibiř
při	při	k7c6	při
pohřbu	pohřeb	k1gInSc6	pohřeb
zpravidla	zpravidla	k6eAd1	zpravidla
obětují	obětovat	k5eAaBmIp3nP	obětovat
soby	sob	k1gMnPc4	sob
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
šamanský	šamanský	k2eAgInSc1d1	šamanský
buben	buben	k1gInSc1	buben
potažený	potažený	k2eAgInSc1d1	potažený
sobí	sobí	k2eAgFnSc7d1	sobí
kůží	kůže	k1gFnSc7	kůže
bývá	bývat	k5eAaImIp3nS	bývat
např.	např.	kA	např.
v	v	k7c6	v
mýtech	mýtus	k1gInPc6	mýtus
Jakutů	Jakut	k1gMnPc2	Jakut
přirovnáván	přirovnávat	k5eAaImNgInS	přirovnávat
k	k	k7c3	k
létajícímu	létající	k2eAgMnSc3d1	létající
sobu	sob	k1gMnSc3	sob
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
hřbetě	hřbet	k1gInSc6	hřbet
šaman	šaman	k1gMnSc1	šaman
podniká	podnikat	k5eAaImIp3nS	podnikat
cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
světů	svět	k1gInPc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skandinávském	skandinávský	k2eAgMnSc6d1	skandinávský
<g/>
,	,	kIx,	,
britském	britský	k2eAgInSc6d1	britský
a	a	k8xC	a
zejména	zejména	k9	zejména
americkém	americký	k2eAgInSc6d1	americký
folklóru	folklór	k1gInSc6	folklór
a	a	k8xC	a
populární	populární	k2eAgFnSc3d1	populární
kultuře	kultura	k1gFnSc3	kultura
je	být	k5eAaImIp3nS	být
sob	sob	k1gMnSc1	sob
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
Vánocemi	Vánoce	k1gFnPc7	Vánoce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
spřežení	spřežení	k1gNnSc1	spřežení
sobů	sob	k1gMnPc2	sob
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
tahá	tahat	k5eAaImIp3nS	tahat
saně	saně	k1gFnSc1	saně
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgNnPc6	jenž
Santa	Santa	k1gMnSc1	Santa
Claus	Claus	k1gMnSc1	Claus
vozí	vozit	k5eAaImIp3nS	vozit
dárky	dárek	k1gInPc4	dárek
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
sobi	sob	k1gMnPc1	sob
údajně	údajně	k6eAd1	údajně
mohou	moct	k5eAaImIp3nP	moct
létat	létat	k5eAaImF	létat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gNnPc2	on
8	[number]	k4	8
nebo	nebo	k8xC	nebo
9	[number]	k4	9
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
Rudolf	Rudolf	k1gMnSc1	Rudolf
s	s	k7c7	s
červeným	červený	k2eAgInSc7d1	červený
nosem	nos	k1gInSc7	nos
<g/>
.	.	kIx.	.
</s>
<s>
ANDĚRA	ANDĚRA	kA	ANDĚRA
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
a	a	k8xC	a
ČERVENÝ	Červený	k1gMnSc1	Červený
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Savci	savec	k1gMnPc1	savec
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kytovci	kytovec	k1gMnPc1	kytovec
<g/>
,	,	kIx,	,
sirény	siréna	k1gFnPc1	siréna
<g/>
,	,	kIx,	,
chobotnatci	chobotnatec	k1gMnPc5	chobotnatec
<g/>
,	,	kIx,	,
damani	daman	k1gMnPc5	daman
<g/>
,	,	kIx,	,
lichokopytníci	lichokopytník	k1gMnPc5	lichokopytník
<g/>
,	,	kIx,	,
sudokopytníci	sudokopytník	k1gMnPc5	sudokopytník
<g/>
,	,	kIx,	,
zajíci	zajíc	k1gMnPc5	zajíc
<g/>
,	,	kIx,	,
bércouni	bércoun	k1gMnPc5	bércoun
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
153	[number]	k4	153
s.	s.	k?	s.
Svět	svět	k1gInSc1	svět
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
829	[number]	k4	829
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
BRZÁKOVÁ	Brzáková	k1gFnSc1	Brzáková
<g/>
,	,	kIx,	,
Pavlína	Pavlína	k1gFnSc1	Pavlína
<g/>
.	.	kIx.	.
</s>
<s>
Modřínová	modřínový	k2eAgFnSc1d1	Modřínová
duše	duše	k1gFnSc1	duše
<g/>
:	:	kIx,	:
sibiřský	sibiřský	k2eAgInSc1d1	sibiřský
cestopis	cestopis	k1gInSc1	cestopis
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Eminent	Eminent	k1gMnSc1	Eminent
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
302	[number]	k4	302
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7281	[number]	k4	7281
<g/>
-	-	kIx~	-
<g/>
194	[number]	k4	194
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Slovníček	slovníček	k1gInSc1	slovníček
evenských	evenský	k2eAgInPc2d1	evenský
výrazů	výraz	k1gInPc2	výraz
ČERVENÁ	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
Alena	Alena	k1gFnSc1	Alena
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Domácí	domácí	k2eAgNnPc1d1	domácí
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
183	[number]	k4	183
s.	s.	k?	s.
Svět	svět	k1gInSc1	svět
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
974	[number]	k4	974
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
KUBÍK	Kubík	k1gMnSc1	Kubík
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sobích	sobí	k2eAgFnPc6d1	sobí
stezkách	stezka	k1gFnPc6	stezka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svět	svět	k1gInSc1	svět
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
123	[number]	k4	123
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
10	[number]	k4	10
000	[number]	k4	000
výtisků	výtisk	k1gInPc2	výtisk
ODULOK	ODULOK	kA	ODULOK
<g/>
,	,	kIx,	,
Teki	Teki	k1gNnSc2	Teki
<g/>
.	.	kIx.	.
</s>
<s>
Sobí	sobí	k2eAgMnPc1d1	sobí
lidé	člověk	k1gMnPc1	člověk
<g/>
:	:	kIx,	:
život	život	k1gInSc1	život
Imtěurgina	Imtěurgin	k1gMnSc2	Imtěurgin
staršího	starý	k2eAgMnSc2d2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Vincenc	Vincenc	k1gMnSc1	Vincenc
Charvát	Charvát	k1gMnSc1	Charvát
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Světě	svět	k1gInSc6	svět
sovětů	sovět	k1gInPc2	sovět
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svět	svět	k1gInSc1	svět
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
148	[number]	k4	148
s.	s.	k?	s.
15	[number]	k4	15
400	[number]	k4	400
výt	výt	k5eAaImF	výt
<g/>
.	.	kIx.	.
</s>
<s>
SUCHL	SUCHL	kA	SUCHL
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Království	království	k1gNnSc1	království
boha	bůh	k1gMnSc2	bůh
tajgy	tajga	k1gFnSc2	tajga
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
151	[number]	k4	151
s.	s.	k?	s.
Cestopisy	cestopis	k1gInPc4	cestopis
/	/	kIx~	/
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
.	.	kIx.	.
25	[number]	k4	25
000	[number]	k4	000
výt	výt	k5eAaImF	výt
<g/>
.	.	kIx.	.
</s>
<s>
VITEBSKY	VITEBSKY	kA	VITEBSKY
<g/>
,	,	kIx,	,
Piers	Piers	k1gInSc1	Piers
<g/>
.	.	kIx.	.
</s>
<s>
Sobí	sobí	k2eAgMnPc1d1	sobí
lidé	člověk	k1gMnPc1	člověk
<g/>
:	:	kIx,	:
život	život	k1gInSc1	život
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
a	a	k8xC	a
duchy	duch	k1gMnPc7	duch
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Gisela	Gisela	k1gFnSc1	Gisela
Kubrichtová	Kubrichtová	k1gFnSc1	Kubrichtová
<g/>
;	;	kIx,	;
Vyd	Vyd	k1gFnSc1	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Sociologické	sociologický	k2eAgNnSc1d1	sociologické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
(	(	kIx(	(
<g/>
SLON	slon	k1gMnSc1	slon
<g/>
)	)	kIx)	)
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
asociací	asociace	k1gFnSc7	asociace
pro	pro	k7c4	pro
sociální	sociální	k2eAgFnSc4d1	sociální
antropologii	antropologie	k1gFnSc4	antropologie
(	(	kIx(	(
<g/>
CASA	CASA	kA	CASA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
384	[number]	k4	384
s.	s.	k?	s.
Cargo	Cargo	k1gMnSc1	Cargo
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7419	[number]	k4	7419
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sob	sob	k1gMnSc1	sob
polární	polární	k2eAgMnSc1d1	polární
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
sob	soba	k1gFnPc2	soba
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc1	taxon
Rangifer	Rangifer	k1gInSc1	Rangifer
tarandus	tarandus	k1gInSc1	tarandus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
ZOO	zoo	k1gFnSc2	zoo
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Sob	sob	k1gMnSc1	sob
polární	polární	k2eAgFnSc2d1	polární
ZOO	zoo	k1gFnSc2	zoo
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Sob	sob	k1gMnSc1	sob
polární	polární	k2eAgMnSc1d1	polární
(	(	kIx(	(
<g/>
virtuální	virtuální	k2eAgFnSc1d1	virtuální
prohlídka	prohlídka	k1gFnSc1	prohlídka
<g/>
)	)	kIx)	)
</s>
