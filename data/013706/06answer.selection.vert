<s>
Okresní	okresní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
bytového	bytový	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
nebo	nebo	k8xC
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Praze	Praha	k1gFnSc6
Obvodní	obvodní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
bytového	bytový	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
(	(	kIx(
<g/>
ve	v	k7c6
zkratce	zkratka	k1gFnSc6
OPBH	OPBH	kA
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
uskupení	uskupení	k1gNnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
v	v	k7c6
socialistickém	socialistický	k2eAgNnSc6d1
Československu	Československo	k1gNnSc6
spravovalo	spravovat	k5eAaImAgNnS
domy	dům	k1gInPc4
v	v	k7c6
socialistickém	socialistický	k2eAgNnSc6d1
vlastnictví	vlastnictví	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vzniklo	vzniknout	k5eAaPmAgNnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
1959	#num#	k4
přetvořením	přetvoření	k1gNnSc7
z	z	k7c2
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
existujících	existující	k2eAgInPc2d1
bytových	bytový	k2eAgInPc2d1
a	a	k8xC
domovních	domovní	k2eAgFnPc2d1
správ	správa	k1gFnPc2
<g/>
.	.	kIx.
</s>