<s>
Okresní	okresní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
bytového	bytový	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
</s>
<s>
Okresní	okresní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
bytového	bytový	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
pečoval	pečovat	k5eAaImAgInS
o	o	k7c4
domy	dům	k1gInPc4
a	a	k8xC
byty	byt	k1gInPc4
</s>
<s>
Okresní	okresní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
bytového	bytový	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
nebo	nebo	k8xC
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Praze	Praha	k1gFnSc6
Obvodní	obvodní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
bytového	bytový	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
(	(	kIx(
<g/>
ve	v	k7c6
zkratce	zkratka	k1gFnSc6
OPBH	OPBH	kA
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
uskupení	uskupení	k1gNnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
v	v	k7c6
socialistickém	socialistický	k2eAgNnSc6d1
Československu	Československo	k1gNnSc6
spravovalo	spravovat	k5eAaImAgNnS
domy	dům	k1gInPc4
v	v	k7c6
socialistickém	socialistický	k2eAgNnSc6d1
vlastnictví	vlastnictví	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vzniklo	vzniknout	k5eAaPmAgNnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
1959	#num#	k4
přetvořením	přetvoření	k1gNnSc7
z	z	k7c2
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
existujících	existující	k2eAgInPc2d1
bytových	bytový	k2eAgInPc2d1
a	a	k8xC
domovních	domovní	k2eAgFnPc2d1
správ	správa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivé	jednotlivý	k2eAgInPc4d1
vzniknuvší	vzniknuvší	k2eAgInPc4d1
podniky	podnik	k1gInPc4
byly	být	k5eAaImAgFnP
doplněny	doplnit	k5eAaPmNgFnP
i	i	k9
četami	četa	k1gFnPc7
pro	pro	k7c4
drobnou	drobný	k2eAgFnSc4d1
údržbu	údržba	k1gFnSc4
bytového	bytový	k2eAgInSc2d1
majetku	majetek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podnik	podnik	k1gInSc1
měl	mít	k5eAaImAgInS
na	na	k7c6
starosti	starost	k1gFnSc6
předávání	předávání	k1gNnSc2
přidělených	přidělený	k2eAgFnPc2d1
prostor	prostora	k1gFnPc2
nájemníkům	nájemník	k1gMnPc3
<g/>
,	,	kIx,
udržování	udržování	k1gNnSc4
domovního	domovní	k2eAgInSc2d1
fondu	fond	k1gInSc2
ve	v	k7c6
způsobilém	způsobilý	k2eAgInSc6d1
stavu	stav	k1gInSc6
a	a	k8xC
zároveň	zároveň	k6eAd1
zabezpečoval	zabezpečovat	k5eAaImAgInS
provoz	provoz	k1gInSc1
výtahů	výtah	k1gInPc2
v	v	k7c6
domě	dům	k1gInSc6
či	či	k8xC
provoz	provoz	k1gInSc4
zařízení	zařízení	k1gNnPc2
dodávajících	dodávající	k2eAgNnPc2d1
teplo	teplo	k6eAd1
a	a	k8xC
teplou	teplý	k2eAgFnSc4d1
užitkovou	užitkový	k2eAgFnSc4d1
vodu	voda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
vedl	vést	k5eAaImAgMnS
evidenci	evidence	k1gFnSc4
a	a	k8xC
konal	konat	k5eAaImAgMnS
pravidelné	pravidelný	k2eAgFnPc4d1
prohlídky	prohlídka	k1gFnPc4
domovního	domovní	k2eAgInSc2d1
fondu	fond	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
výstavbě	výstavba	k1gFnSc6
nových	nový	k2eAgInPc2d1
domů	dům	k1gInPc2
je	být	k5eAaImIp3nS
přebíral	přebírat	k5eAaImAgMnS
do	do	k7c2
své	svůj	k3xOyFgFnSc2
správy	správa	k1gFnSc2
a	a	k8xC
současně	současně	k6eAd1
se	se	k3xPyFc4
vyjadřoval	vyjadřovat	k5eAaImAgMnS
k	k	k7c3
adaptacím	adaptace	k1gFnPc3
<g/>
,	,	kIx,
rekonstrukcím	rekonstrukce	k1gFnPc3
a	a	k8xC
modernizacím	modernizace	k1gFnPc3
prováděným	prováděný	k2eAgFnPc3d1
v	v	k7c6
bytech	byt	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pečoval	pečovat	k5eAaImAgMnS
o	o	k7c4
udržování	udržování	k1gNnSc4
pořádku	pořádek	k1gInSc2
a	a	k8xC
čistoty	čistota	k1gFnSc2
v	v	k7c6
domech	dům	k1gInPc6
i	i	k8xC
v	v	k7c6
přilehlých	přilehlý	k2eAgInPc6d1
chodnících	chodník	k1gInPc6
<g/>
,	,	kIx,
dvorech	dvůr	k1gInPc6
či	či	k8xC
vnitroblocích	vnitroblok	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
objektech	objekt	k1gInPc6
zajišťoval	zajišťovat	k5eAaImAgInS
provozuschopné	provozuschopný	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
civilní	civilní	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
<g/>
,	,	kIx,
pečoval	pečovat	k5eAaImAgMnS
o	o	k7c4
dodržování	dodržování	k1gNnSc4
domovního	domovní	k2eAgInSc2d1
řádu	řád	k1gInSc2
a	a	k8xC
zásad	zásada	k1gFnPc2
socialistického	socialistický	k2eAgNnSc2d1
soužití	soužití	k1gNnSc2
v	v	k7c6
domech	dům	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vybíral	Vybíral	k1gMnSc1
(	(	kIx(
<g/>
a	a	k8xC
případně	případně	k6eAd1
též	též	k9
vymáhal	vymáhat	k5eAaImAgMnS
<g/>
)	)	kIx)
poplatky	poplatek	k1gInPc1
za	za	k7c4
užívání	užívání	k1gNnSc4
bytů	byt	k1gInPc2
i	i	k8xC
nebytových	bytový	k2eNgFnPc2d1
prostor	prostora	k1gFnPc2
a	a	k8xC
současně	současně	k6eAd1
vybíral	vybírat	k5eAaImAgMnS
i	i	k9
za	za	k7c4
služby	služba	k1gFnPc4
spojené	spojený	k2eAgFnPc4d1
s	s	k7c7
užíváním	užívání	k1gNnSc7
těchto	tento	k3xDgFnPc2
prostor	prostora	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BÁRTA	Bárta	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
OPBH	OPBH	kA
<g/>
,	,	kIx,
uliční	uliční	k2eAgInSc1d1
výbor	výbor	k1gInSc1
či	či	k8xC
Tuzex	Tuzex	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
zmizela	zmizet	k5eAaPmAgFnS
ze	z	k7c2
slovníku	slovník	k1gInSc2
řada	řada	k1gFnSc1
výrazů	výraz	k1gInPc2
<g/>
.	.	kIx.
iHNed	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2009-10-25	2009-10-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
7693	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VLASÁK	Vlasák	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inventář	inventář	k1gInSc1
Bytový	bytový	k2eAgInSc1d1
podnik	podnik	k1gInSc1
Praha	Praha	k1gFnSc1
1	#num#	k4
(	(	kIx(
<g/>
Obvodní	obvodní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
bytového	bytový	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
Praha	Praha	k1gFnSc1
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Archiv	archiv	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
83	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Dějiny	dějiny	k1gFnPc1
původce	původce	k1gMnSc4
<g/>
:	:	kIx,
Bytový	bytový	k2eAgInSc1d1
podnik	podnik	k1gInSc1
Praha	Praha	k1gFnSc1
1	#num#	k4
a	a	k8xC
jeho	jeho	k3xOp3gMnPc7
předchůdci	předchůdce	k1gMnPc7
v	v	k7c6
rámci	rámec	k1gInSc6
bytového	bytový	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
OPBH	OPBH	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Komunistický	komunistický	k2eAgInSc1d1
režim	režim	k1gInSc1
v	v	k7c6
Československu	Československo	k1gNnSc6
</s>
