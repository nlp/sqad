<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1365	[number]	k4	1365
založena	založen	k2eAgFnSc1d1	založena
nejstarší	starý	k2eAgFnSc1d3	nejstarší
německá	německý	k2eAgFnSc1d1	německá
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
prestižní	prestižní	k2eAgFnPc4d1	prestižní
evropské	evropský	k2eAgFnPc4d1	Evropská
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
