<p>
<s>
Kieron	Kieron	k1gMnSc1	Kieron
Courtney	Courtnea	k1gFnSc2	Courtnea
Dyer	Dyer	k1gMnSc1	Dyer
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
Ipswich	Ipswich	k1gInSc1	Ipswich
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
anglický	anglický	k2eAgMnSc1d1	anglický
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
záložník	záložník	k1gMnSc1	záložník
a	a	k8xC	a
reprezentant	reprezentant	k1gMnSc1	reprezentant
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
postem	post	k1gInSc7	post
bylo	být	k5eAaImAgNnS	být
místo	místo	k1gNnSc1	místo
ofensivního	ofensivní	k2eAgNnSc2d1	ofensivní
<g/>
/	/	kIx~	/
<g/>
krajního	krajní	k2eAgMnSc2d1	krajní
záložníka	záložník	k1gMnSc2	záložník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reprezentační	reprezentační	k2eAgFnSc1d1	reprezentační
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Kieron	Kieron	k1gMnSc1	Kieron
Dyer	Dyer	k1gMnSc1	Dyer
reprezentoval	reprezentovat	k5eAaImAgMnS	reprezentovat
Anglii	Anglie	k1gFnSc4	Anglie
v	v	k7c6	v
mládežnických	mládežnický	k2eAgInPc6d1	mládežnický
výběrech	výběr	k1gInPc6	výběr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
A-mužstvu	Aužstvo	k1gNnSc6	A-mužstvo
Anglie	Anglie	k1gFnSc2	Anglie
debutoval	debutovat	k5eAaBmAgInS	debutovat
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1999	[number]	k4	1999
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
Wembley	Wemblea	k1gFnSc2	Wemblea
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
s	s	k7c7	s
Lucemburskem	Lucembursko	k1gNnSc7	Lucembursko
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
skončil	skončit	k5eAaPmAgInS	skončit
vysokou	vysoký	k2eAgFnSc7d1	vysoká
výhrou	výhra	k1gFnSc7	výhra
domácího	domácí	k2eAgInSc2d1	domácí
Albionu	Albion	k1gInSc2	Albion
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
odehrál	odehrát	k5eAaPmAgInS	odehrát
první	první	k4xOgInSc4	první
poločas	poločas	k1gInSc4	poločas
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
pravého	pravý	k2eAgMnSc2d1	pravý
obránce	obránce	k1gMnSc2	obránce
<g/>
.	.	kIx.	.
<g/>
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
2002	[number]	k4	2002
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
a	a	k8xC	a
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Angličané	Angličan	k1gMnPc1	Angličan
vypadli	vypadnout	k5eAaPmAgMnP	vypadnout
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
s	s	k7c7	s
Brazílií	Brazílie	k1gFnSc7	Brazílie
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
2004	[number]	k4	2004
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
Anglie	Anglie	k1gFnSc1	Anglie
vyřazena	vyřadit	k5eAaPmNgFnS	vyřadit
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
domácím	domácí	k2eAgInSc7d1	domácí
týmem	tým	k1gInSc7	tým
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kieron	Kieron	k1gInSc1	Kieron
Dyer	Dyer	k1gInSc4	Dyer
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
hráče	hráč	k1gMnSc2	hráč
na	na	k7c4	na
Transfermarkt	Transfermarkt	k1gInSc4	Transfermarkt
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
hráče	hráč	k1gMnSc2	hráč
na	na	k7c6	na
National	National	k1gFnSc6	National
Football	Footballa	k1gFnPc2	Footballa
Teams	Teamsa	k1gFnPc2	Teamsa
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
