<s>
Letopisy	letopis	k1gInPc1	letopis
Narnie	Narnie	k1gFnSc2	Narnie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
The	The	k1gMnSc1	The
Chronicles	Chronicles	k1gMnSc1	Chronicles
of	of	k?	of
Narnia	Narnium	k1gNnSc2	Narnium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sedmidílný	sedmidílný	k2eAgInSc1d1	sedmidílný
cyklus	cyklus	k1gInSc1	cyklus
fantasy	fantas	k1gInPc4	fantas
knih	kniha	k1gFnPc2	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
britského	britský	k2eAgMnSc2d1	britský
autora	autor	k1gMnSc2	autor
Clive	Cliev	k1gFnSc2	Cliev
Staples	Staplesa	k1gFnPc2	Staplesa
Lewise	Lewise	k1gFnSc2	Lewise
<g/>
.	.	kIx.	.
</s>
<s>
Pojednávají	pojednávat	k5eAaImIp3nP	pojednávat
o	o	k7c6	o
dobrodružství	dobrodružství	k1gNnSc6	dobrodružství
několika	několik	k4yIc2	několik
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
fantastické	fantastický	k2eAgFnSc2d1	fantastická
země	zem	k1gFnSc2	zem
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
mluvící	mluvící	k2eAgNnPc1d1	mluvící
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
existuje	existovat	k5eAaImIp3nS	existovat
magie	magie	k1gFnSc1	magie
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
jiný	jiný	k2eAgInSc4d1	jiný
význam	význam	k1gInSc4	význam
než	než	k8xS	než
v	v	k7c6	v
klasickém	klasický	k2eAgNnSc6d1	klasické
pojetí	pojetí	k1gNnSc6	pojetí
-	-	kIx~	-
není	být	k5eNaImIp3nS	být
autonomní	autonomní	k2eAgFnSc7d1	autonomní
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Stvořitelem	Stvořitel	k1gMnSc7	Stvořitel
daným	daný	k2eAgInSc7d1	daný
řádem	řád	k1gInSc7	řád
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgMnSc2	jenž
funguje	fungovat	k5eAaImIp3nS	fungovat
Narnijský	Narnijský	k2eAgInSc1d1	Narnijský
svět	svět	k1gInSc1	svět
<g/>
)	)	kIx)	)
a	a	k8xC	a
dobro	dobro	k1gNnSc1	dobro
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
postavit	postavit	k5eAaPmF	postavit
zlu	zlo	k1gNnSc3	zlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
čemž	což	k3yRnSc6	což
děti	dítě	k1gFnPc1	dítě
sehrají	sehrát	k5eAaPmIp3nP	sehrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
úlohu	úloha	k1gFnSc4	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
jsou	být	k5eAaImIp3nP	být
alegorií	alegorie	k1gFnSc7	alegorie
základních	základní	k2eAgInPc2d1	základní
konceptů	koncept	k1gInPc2	koncept
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Lev	Lev	k1gMnSc1	Lev
<g/>
,	,	kIx,	,
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
a	a	k8xC	a
skříň	skříň	k1gFnSc1	skříň
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
hrdiny	hrdina	k1gMnPc7	hrdina
jsou	být	k5eAaImIp3nP	být
děti	dítě	k1gFnPc1	dítě
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
a	a	k8xC	a
Lucie	Lucie	k1gFnPc1	Lucie
evakuované	evakuovaný	k2eAgFnPc1d1	evakuovaná
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
shodou	shoda	k1gFnSc7	shoda
náhod	náhoda	k1gFnPc2	náhoda
do	do	k7c2	do
domu	dům	k1gInSc2	dům
profesora	profesor	k1gMnSc2	profesor
Diviše	Diviš	k1gMnSc2	Diviš
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
objeví	objevit	k5eAaPmIp3nS	objevit
starou	starý	k2eAgFnSc4d1	stará
skříň	skříň	k1gFnSc4	skříň
a	a	k8xC	a
projdou	projít	k5eAaPmIp3nP	projít
jí	on	k3xPp3gFnSc3	on
do	do	k7c2	do
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
lva	lev	k1gMnSc2	lev
Aslana	Aslan	k1gMnSc2	Aslan
(	(	kIx(	(
<g/>
z	z	k7c2	z
turečtiny	turečtina	k1gFnSc2	turečtina
<g/>
,	,	kIx,	,
aslan	aslan	k1gMnSc1	aslan
=	=	kIx~	=
lev	lev	k1gMnSc1	lev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ztělesňujícího	ztělesňující	k2eAgInSc2d1	ztělesňující
svrchované	svrchovaný	k2eAgNnSc4d1	svrchované
dobro	dobro	k1gNnSc4	dobro
<g/>
,	,	kIx,	,
vymaní	vymanit	k5eAaPmIp3nS	vymanit
zemi	zem	k1gFnSc4	zem
z	z	k7c2	z
krutovlády	krutovláda	k1gFnSc2	krutovláda
Bílé	bílý	k2eAgFnSc2d1	bílá
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c6	na
Narnii	Narnie	k1gFnSc6	Narnie
uvrhla	uvrhnout	k5eAaPmAgFnS	uvrhnout
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
zimu	zima	k1gFnSc4	zima
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
porážce	porážka	k1gFnSc6	porážka
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
králi	král	k1gMnPc7	král
a	a	k8xC	a
královnami	královna	k1gFnPc7	královna
a	a	k8xC	a
započínají	započínat	k5eAaImIp3nP	započínat
"	"	kIx"	"
<g/>
zlatý	zlatý	k2eAgInSc4d1	zlatý
věk	věk	k1gInSc4	věk
<g/>
"	"	kIx"	"
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
Kaspian	Kaspian	k1gMnSc1	Kaspian
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
–	–	k?	–
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
a	a	k8xC	a
Lucie	Lucie	k1gFnSc1	Lucie
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
po	po	k7c6	po
roce	rok	k1gInSc6	rok
do	do	k7c2	do
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mezitím	mezitím	k6eAd1	mezitím
uběhlo	uběhnout	k5eAaPmAgNnS	uběhnout
asi	asi	k9	asi
800	[number]	k4	800
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Narnii	Narnie	k1gFnSc4	Narnie
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Talmarýni	Talmarýn	k1gMnPc1	Talmarýn
<g/>
,	,	kIx,	,
národ	národ	k1gInSc1	národ
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
původně	původně	k6eAd1	původně
žili	žít	k5eAaImAgMnP	žít
na	na	k7c6	na
pirátském	pirátský	k2eAgInSc6d1	pirátský
ostrově	ostrov	k1gInSc6	ostrov
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
původní	původní	k2eAgMnPc4d1	původní
obyvatele	obyvatel	k1gMnPc4	obyvatel
donutili	donutit	k5eAaPmAgMnP	donutit
ke	k	k7c3	k
skrytému	skrytý	k2eAgInSc3d1	skrytý
životu	život	k1gInSc3	život
v	v	k7c6	v
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
jsou	být	k5eAaImIp3nP	být
opět	opět	k6eAd1	opět
povolány	povolat	k5eAaPmNgInP	povolat
do	do	k7c2	do
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
s	s	k7c7	s
princem	princ	k1gMnSc7	princ
Kaspianem	Kaspian	k1gMnSc7	Kaspian
<g/>
,	,	kIx,	,
právoplatným	právoplatný	k2eAgMnSc7d1	právoplatný
králem	král	k1gMnSc7	král
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
trůn	trůn	k1gInSc4	trůn
však	však	k9	však
neprávem	neprávo	k1gNnSc7	neprávo
uchvátil	uchvátit	k5eAaPmAgMnS	uchvátit
jeho	jeho	k3xOp3gNnSc4	jeho
strýc	strýc	k1gMnSc1	strýc
Miraz	Miraz	k1gInSc1	Miraz
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Aslanově	Aslanův	k2eAgFnSc3d1	Aslanova
pomoci	pomoc	k1gFnSc3	pomoc
dosadí	dosadit	k5eAaPmIp3nS	dosadit
Kaspiana	Kaspiana	k1gFnSc1	Kaspiana
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
a	a	k8xC	a
obnoví	obnovit	k5eAaPmIp3nS	obnovit
práva	právo	k1gNnSc2	právo
všech	všecek	k3xTgInPc2	všecek
Narnianů	Narnian	k1gInPc2	Narnian
<g/>
.	.	kIx.	.
</s>
<s>
Plavba	plavba	k1gFnSc1	plavba
Jitřního	jitřní	k2eAgMnSc2d1	jitřní
poutníka	poutník	k1gMnSc2	poutník
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
–	–	k?	–
Edmund	Edmund	k1gMnSc1	Edmund
a	a	k8xC	a
Lucie	Lucie	k1gFnSc1	Lucie
se	se	k3xPyFc4	se
naposledy	naposledy	k6eAd1	naposledy
vracejí	vracet	k5eAaImIp3nP	vracet
do	do	k7c2	do
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
jejich	jejich	k3xOp3gFnSc7	jejich
zpočátku	zpočátku	k6eAd1	zpočátku
zkažený	zkažený	k2eAgMnSc1d1	zkažený
bratranec	bratranec	k1gMnSc1	bratranec
Eustác	Eustác	k1gInSc1	Eustác
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
napraví	napravit	k5eAaPmIp3nS	napravit
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
Kaspianovi	Kaspianův	k2eAgMnPc1d1	Kaspianův
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
sedmi	sedm	k4xCc2	sedm
ztracených	ztracený	k2eAgMnPc2d1	ztracený
narnijských	narnijský	k2eAgMnPc2d1	narnijský
pánů	pan	k1gMnPc2	pan
<g/>
,	,	kIx,	,
přátel	přítel	k1gMnPc2	přítel
jeho	jeho	k3xOp3gMnSc2	jeho
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
je	být	k5eAaImIp3nS	být
zavede	zavést	k5eAaPmIp3nS	zavést
až	až	k9	až
na	na	k7c4	na
samotný	samotný	k2eAgInSc4d1	samotný
konec	konec	k1gInSc4	konec
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
židle	židle	k1gFnSc1	židle
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
–	–	k?	–
Eustác	Eustác	k1gInSc1	Eustác
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
spolužačka	spolužačka	k1gFnSc1	spolužačka
Julie	Julie	k1gFnSc1	Julie
jsou	být	k5eAaImIp3nP	být
přivoláni	přivolán	k2eAgMnPc1d1	přivolán
do	do	k7c2	do
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
našli	najít	k5eAaPmAgMnP	najít
ztraceného	ztracený	k2eAgMnSc4d1	ztracený
prince	princ	k1gMnSc4	princ
Riliana	Rilian	k1gMnSc4	Rilian
<g/>
,	,	kIx,	,
Kaspiánova	Kaspiánův	k2eAgMnSc4d1	Kaspiánův
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
unesla	unést	k5eAaPmAgFnS	unést
zlá	zlá	k1gFnSc1	zlá
Zelená	Zelená	k1gFnSc1	Zelená
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
<g/>
.	.	kIx.	.
</s>
<s>
Čeká	čekat	k5eAaImIp3nS	čekat
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
cesta	cesta	k1gFnSc1	cesta
nejen	nejen	k6eAd1	nejen
po	po	k7c6	po
Narnii	Narnie	k1gFnSc6	Narnie
<g/>
.	.	kIx.	.
</s>
<s>
Kůň	kůň	k1gMnSc1	kůň
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
chlapec	chlapec	k1gMnSc1	chlapec
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
příběh	příběh	k1gInSc1	příběh
chlapce	chlapec	k1gMnSc2	chlapec
Šasty	Šasta	k1gMnSc2	Šasta
a	a	k8xC	a
mluvícího	mluvící	k2eAgMnSc2d1	mluvící
koně	kůň	k1gMnSc2	kůň
Brí	Brí	k1gMnSc2	Brí
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
zlatém	zlatý	k2eAgInSc6d1	zlatý
věku	věk	k1gInSc6	věk
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
.	.	kIx.	.
</s>
<s>
Brí	Brí	k?	Brí
a	a	k8xC	a
Šasta	Šasta	k1gMnSc1	Šasta
utíkají	utíkat	k5eAaImIp3nP	utíkat
z	z	k7c2	z
kruté	krutý	k2eAgFnSc2d1	krutá
jižní	jižní	k2eAgFnSc2d1	jižní
země	zem	k1gFnSc2	zem
Kalormenu	Kalormen	k1gInSc2	Kalormen
do	do	k7c2	do
Narnie	Narnie	k1gFnSc2	Narnie
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
cesta	cesta	k1gFnSc1	cesta
se	se	k3xPyFc4	se
promění	proměnit	k5eAaPmIp3nS	proměnit
v	v	k7c4	v
závod	závod	k1gInSc4	závod
o	o	k7c4	o
záchranu	záchrana	k1gFnSc4	záchrana
Narnie	Narnie	k1gFnSc2	Narnie
a	a	k8xC	a
sousední	sousední	k2eAgFnSc2d1	sousední
země	zem	k1gFnSc2	zem
Archelandu	Archeland	k1gInSc2	Archeland
před	před	k7c7	před
nenadálým	nenadálý	k2eAgInSc7d1	nenadálý
kalorménským	kalorménský	k2eAgInSc7d1	kalorménský
útokem	útok	k1gInSc7	útok
<g/>
.	.	kIx.	.
</s>
<s>
Čarodějův	čarodějův	k2eAgMnSc1d1	čarodějův
synovec	synovec	k1gMnSc1	synovec
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
hrdiny	hrdina	k1gMnPc7	hrdina
jsou	být	k5eAaImIp3nP	být
Digory	Digora	k1gFnPc1	Digora
a	a	k8xC	a
Polly	Poll	k1gMnPc4	Poll
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
magických	magický	k2eAgInPc2d1	magický
prstenů	prsten	k1gInPc2	prsten
strýce	strýc	k1gMnSc2	strýc
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
několika	několik	k4yIc2	několik
jiných	jiný	k2eAgInPc2d1	jiný
světů	svět	k1gInPc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Stávají	stávat	k5eAaImIp3nP	stávat
se	s	k7c7	s
svědky	svědek	k1gMnPc7	svědek
stvoření	stvoření	k1gNnSc2	stvoření
Narnie	Narnie	k1gFnSc2	Narnie
lvem	lev	k1gMnSc7	lev
Aslanem	Aslan	k1gMnSc7	Aslan
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
nechtěně	chtěně	k6eNd1	chtěně
přivedou	přivést	k5eAaPmIp3nP	přivést
zlou	zlý	k2eAgFnSc4d1	zlá
čarodějnici	čarodějnice	k1gFnSc4	čarodějnice
Jadis	Jadis	k1gFnSc2	Jadis
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
matkou	matka	k1gFnSc7	matka
všech	všecek	k3xTgFnPc2	všecek
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
bitva	bitva	k1gFnSc1	bitva
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
–	–	k?	–
Eustác	Eustác	k1gInSc1	Eustác
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
při	při	k7c6	při
záchraně	záchrana	k1gFnSc6	záchrana
Narnie	Narnie	k1gFnSc2	Narnie
před	před	k7c7	před
silami	síla	k1gFnPc7	síla
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
nakonec	nakonec	k6eAd1	nakonec
porazí	porazit	k5eAaPmIp3nS	porazit
zásah	zásah	k1gInSc4	zásah
Aslana	Aslan	k1gMnSc2	Aslan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zároveň	zároveň	k6eAd1	zároveň
přináší	přinášet	k5eAaImIp3nS	přinášet
konec	konec	k1gInSc1	konec
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
;	;	kIx,	;
postavy	postava	k1gFnPc1	postava
z	z	k7c2	z
předchozích	předchozí	k2eAgInPc2d1	předchozí
dílů	díl	k1gInPc2	díl
včetně	včetně	k7c2	včetně
těch	ten	k3xDgMnPc2	ten
mrtvých	mrtvý	k1gMnPc2	mrtvý
jsou	být	k5eAaImIp3nP	být
souzeny	souzen	k2eAgFnPc1d1	souzena
a	a	k8xC	a
ty	ten	k3xDgFnPc1	ten
dobré	dobrá	k1gFnPc1	dobrá
jsou	být	k5eAaImIp3nP	být
vpuštěny	vpustit	k5eAaPmNgFnP	vpustit
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
Aslanovy	Aslanův	k2eAgFnSc2d1	Aslanova
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
vydáních	vydání	k1gNnPc6	vydání
(	(	kIx(	(
<g/>
americké	americký	k2eAgNnSc4d1	americké
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgNnSc4	třetí
české	český	k2eAgNnSc4d1	české
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
knihy	kniha	k1gFnPc1	kniha
řazeny	řadit	k5eAaImNgFnP	řadit
podle	podle	k7c2	podle
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
chronologie	chronologie	k1gFnSc2	chronologie
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
Čarodějův	čarodějův	k2eAgMnSc1d1	čarodějův
synovec	synovec	k1gMnSc1	synovec
přesunut	přesunut	k2eAgInSc4d1	přesunut
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
a	a	k8xC	a
Kůň	kůň	k1gMnSc1	kůň
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
chlapec	chlapec	k1gMnSc1	chlapec
za	za	k7c2	za
Lva	lev	k1gInSc2	lev
<g/>
,	,	kIx,	,
čarodějnici	čarodějnice	k1gFnSc4	čarodějnice
a	a	k8xC	a
skříň	skříň	k1gFnSc4	skříň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
české	český	k2eAgNnSc1d1	české
vydání	vydání	k1gNnSc1	vydání
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Renaty	Renata	k1gFnSc2	Renata
Ferstové	Ferstová	k1gFnSc2	Ferstová
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Orbis	orbis	k1gInSc1	orbis
pictus	pictus	k1gInSc1	pictus
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc4	druhý
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Návrat	návrat	k1gInSc1	návrat
domů	dům	k1gInPc2	dům
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgMnSc1	třetí
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
překladu	překlad	k1gInSc6	překlad
Veroniky	Veronika	k1gFnSc2	Veronika
Volhejnové	Volhejnová	k1gFnSc2	Volhejnová
<g/>
,	,	kIx,	,
s	s	k7c7	s
úpravou	úprava	k1gFnSc7	úprava
odvolávající	odvolávající	k2eAgFnSc7d1	odvolávající
se	se	k3xPyFc4	se
na	na	k7c4	na
film	film	k1gInSc4	film
a	a	k8xC	a
proslulými	proslulý	k2eAgFnPc7d1	proslulá
původními	původní	k2eAgFnPc7d1	původní
ilustracemi	ilustrace	k1gFnPc7	ilustrace
Pauline	Paulin	k1gInSc5	Paulin
Baynesové	Baynesový	k2eAgNnSc1d1	Baynesový
vydává	vydávat	k5eAaPmIp3nS	vydávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Fragment	fragment	k1gInSc1	fragment
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
postav	postav	k1gInSc1	postav
v	v	k7c6	v
Letopisech	letopis	k1gInPc6	letopis
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
byly	být	k5eAaImAgFnP	být
několikrát	několikrát	k6eAd1	několikrát
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
překladech	překlad	k1gInPc6	překlad
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
i	i	k8xC	i
jména	jméno	k1gNnPc1	jméno
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
<g/>
:	:	kIx,	:
Petr	Petr	k1gMnSc1	Petr
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Peter	Peter	k1gMnSc1	Peter
Pevensie	Pevensie	k1gFnSc2	Pevensie
<g/>
)	)	kIx)	)
Zuzana	Zuzana	k1gFnSc1	Zuzana
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Susan	Susan	k1gMnSc1	Susan
Pevensie	Pevensie	k1gFnSc2	Pevensie
<g/>
)	)	kIx)	)
Edmund	Edmund	k1gMnSc1	Edmund
Lucinka	Lucinka	k1gFnSc1	Lucinka
(	(	kIx(	(
<g/>
Lucy	Lucy	k1gInPc1	Lucy
<g/>
)	)	kIx)	)
Eustác	Eustác	k1gFnSc1	Eustác
Květoslav	Květoslava	k1gFnPc2	Květoslava
Pobuda	pobuda	k1gFnSc1	pobuda
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
překladech	překlad	k1gInPc6	překlad
Evžen	Evžen	k1gMnSc1	Evžen
Křováček	Křováček	k1gMnSc1	Křováček
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Eustace	Eustace	k1gFnSc2	Eustace
Clarence	Clarence	k1gFnSc1	Clarence
Scrubb	Scrubb	k1gMnSc1	Scrubb
<g/>
)	)	kIx)	)
Julie	Julie	k1gFnSc1	Julie
Poláková	Poláková	k1gFnSc1	Poláková
(	(	kIx(	(
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Jill	Jill	k1gMnSc1	Jill
Pole	pole	k1gFnSc2	pole
<g/>
)	)	kIx)	)
Digory	Digora	k1gFnSc2	Digora
Ketterley	Ketterlea	k1gFnSc2	Ketterlea
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
překladech	překlad	k1gInPc6	překlad
Diviš	Diviš	k1gMnSc1	Diviš
<g/>
)	)	kIx)	)
Polly	Polla	k1gMnSc2	Polla
Plummer	Plummer	k1gMnSc1	Plummer
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
překladech	překlad	k1gInPc6	překlad
Gabriela	Gabriela	k1gFnSc1	Gabriela
<g/>
)	)	kIx)	)
Lewis	Lewis	k1gInSc1	Lewis
často	často	k6eAd1	často
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
čas	čas	k1gInSc1	čas
v	v	k7c6	v
Narnii	Narnie	k1gFnSc6	Narnie
ubíhá	ubíhat	k5eAaImIp3nS	ubíhat
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
v	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
anglosaských	anglosaský	k2eAgInPc6d1	anglosaský
mýtech	mýtus	k1gInPc6	mýtus
a	a	k8xC	a
pověstech	pověst	k1gFnPc6	pověst
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
)	)	kIx)	)
-	-	kIx~	-
většinou	většinou	k6eAd1	většinou
rychleji	rychle	k6eAd2	rychle
a	a	k8xC	a
nelineárně	lineárně	k6eNd1	lineárně
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Lucie	Lucie	k1gFnSc1	Lucie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
projde	projít	k5eAaPmIp3nS	projít
do	do	k7c2	do
Narnie	Narnie	k1gFnSc2	Narnie
vchodem	vchod	k1gInSc7	vchod
ve	v	k7c6	v
skříni	skříň	k1gFnSc6	skříň
jako	jako	k9	jako
první	první	k4xOgInSc4	první
(	(	kIx(	(
<g/>
Lev	lev	k1gInSc4	lev
<g/>
,	,	kIx,	,
Čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
a	a	k8xC	a
skříň	skříň	k1gFnSc1	skříň
<g/>
)	)	kIx)	)
a	a	k8xC	a
stráví	strávit	k5eAaPmIp3nP	strávit
u	u	k7c2	u
fauna	faun	k1gMnSc2	faun
Tumnuse	Tumnuse	k1gFnSc2	Tumnuse
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
zpět	zpět	k6eAd1	zpět
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgFnPc4d1	ostatní
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
v	v	k7c6	v
domě	dům	k1gInSc6	dům
utekl	utéct	k5eAaPmAgMnS	utéct
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
děti	dítě	k1gFnPc1	dítě
vracejí	vracet	k5eAaImIp3nP	vracet
po	po	k7c6	po
roce	rok	k1gInSc6	rok
do	do	k7c2	do
Narnie	Narnie	k1gFnSc2	Narnie
(	(	kIx(	(
<g/>
Princ	princ	k1gMnSc1	princ
Kaspian	Kaspian	k1gMnSc1	Kaspian
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ocitají	ocitat	k5eAaImIp3nP	ocitat
se	se	k3xPyFc4	se
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
svého	svůj	k3xOyFgMnSc2	svůj
hradu	hrad	k1gInSc2	hrad
Cair	Cair	k1gMnSc1	Cair
Paravel	Paravel	k1gMnSc1	Paravel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
nepoznávají	poznávat	k5eNaImIp3nP	poznávat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
Narnii	Narnie	k1gFnSc6	Narnie
mezitím	mezitím	k6eAd1	mezitím
uběhly	uběhnout	k5eAaPmAgInP	uběhnout
stovky	stovka	k1gFnPc4	stovka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
třetí	třetí	k4xOgFnSc6	třetí
návštěvě	návštěva	k1gFnSc6	návštěva
opět	opět	k6eAd1	opět
po	po	k7c6	po
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
Plavba	plavba	k1gFnSc1	plavba
Jitřního	jitřní	k2eAgMnSc2d1	jitřní
poutníka	poutník	k1gMnSc2	poutník
<g/>
)	)	kIx)	)
v	v	k7c6	v
Narnii	Narnie	k1gFnSc6	Narnie
uběhly	uběhnout	k5eAaPmAgInP	uběhnout
jen	jen	k6eAd1	jen
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
při	při	k7c6	při
čtvrté	čtvrtá	k1gFnSc6	čtvrtá
po	po	k7c6	po
půl	půl	k1xP	půl
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
Stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
židle	židle	k1gFnSc1	židle
<g/>
)	)	kIx)	)
asi	asi	k9	asi
padesát	padesát	k4xCc4	padesát
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
světě	svět	k1gInSc6	svět
mezi	mezi	k7c7	mezi
prvním	první	k4xOgNnSc6	první
(	(	kIx(	(
<g/>
Čarodějův	čarodějův	k2eAgMnSc1d1	čarodějův
synovec	synovec	k1gMnSc1	synovec
<g/>
)	)	kIx)	)
a	a	k8xC	a
posledním	poslední	k2eAgMnSc6d1	poslední
(	(	kIx(	(
<g/>
Poslední	poslední	k2eAgFnSc1d1	poslední
bitva	bitva	k1gFnSc1	bitva
<g/>
)	)	kIx)	)
dílem	díl	k1gInSc7	díl
uběhne	uběhnout	k5eAaPmIp3nS	uběhnout
asi	asi	k9	asi
60	[number]	k4	60
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Diviš	Diviš	k1gMnSc1	Diviš
je	být	k5eAaImIp3nS	být
svědkem	svědek	k1gMnSc7	svědek
stvoření	stvoření	k1gNnSc2	stvoření
i	i	k8xC	i
zániku	zánik	k1gInSc2	zánik
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
antickou	antický	k2eAgFnSc7d1	antická
mytologií	mytologie	k1gFnSc7	mytologie
<g/>
;	;	kIx,	;
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
díle	díl	k1gInSc6	díl
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
fauni	faun	k1gMnPc1	faun
(	(	kIx(	(
<g/>
faun	faun	k1gMnSc1	faun
pan	pan	k1gMnSc1	pan
Tumnus	Tumnus	k1gMnSc1	Tumnus
byl	být	k5eAaImAgMnS	být
vůbec	vůbec	k9	vůbec
první	první	k4xOgInPc1	první
bytostí	bytost	k1gFnPc2	bytost
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
první	první	k4xOgFnSc1	první
lidská	lidský	k2eAgFnSc1d1	lidská
návštěvnice	návštěvnice	k1gFnSc1	návštěvnice
Narnie	Narnie	k1gFnSc1	Narnie
-	-	kIx~	-
Lucinka	Lucinka	k1gFnSc1	Lucinka
-	-	kIx~	-
setkala	setkat	k5eAaPmAgFnS	setkat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kentauři	kentaur	k1gMnPc1	kentaur
<g/>
,	,	kIx,	,
minotaurové	minotaur	k1gMnPc1	minotaur
<g/>
,	,	kIx,	,
najády	najáda	k1gFnPc1	najáda
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
objevují	objevovat	k5eAaImIp3nP	objevovat
mluvící	mluvící	k2eAgNnPc1d1	mluvící
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
velikostí	velikost	k1gFnSc7	velikost
přizpůsobena	přizpůsoben	k2eAgFnSc1d1	přizpůsobena
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Magie	magie	k1gFnSc1	magie
je	být	k5eAaImIp3nS	být
prezentována	prezentovat	k5eAaBmNgFnS	prezentovat
hlavně	hlavně	k9	hlavně
jako	jako	k8xC	jako
prastará	prastarý	k2eAgFnSc1d1	prastará
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
dětští	dětský	k2eAgMnPc1d1	dětský
hrdinové	hrdina	k1gMnPc1	hrdina
ji	on	k3xPp3gFnSc4	on
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Renaty	Renata	k1gFnSc2	Renata
Ferstové	Ferstová	k1gFnSc2	Ferstová
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Orbis	orbis	k1gInSc1	orbis
Pictus	Pictus	k1gInSc1	Pictus
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
drobných	drobný	k2eAgFnPc2d1	drobná
oprav	oprava	k1gFnPc2	oprava
<g/>
)	)	kIx)	)
v	v	k7c6	v
Návratu	návrat	k1gInSc6	návrat
domů	dům	k1gInPc2	dům
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Fragment	fragment	k1gInSc1	fragment
překlad	překlad	k1gInSc4	překlad
Veroniky	Veronika	k1gFnSc2	Veronika
Volhejnové	Volhejnová	k1gFnSc2	Volhejnová
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
volnější	volný	k2eAgFnSc4d2	volnější
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
zvolená	zvolený	k2eAgNnPc1d1	zvolené
jména	jméno	k1gNnPc1	jméno
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
více	hodně	k6eAd2	hodně
blíží	blížit	k5eAaImIp3nS	blížit
originálu	originál	k1gInSc2	originál
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
země	země	k1gFnSc1	země
anglicky	anglicky	k6eAd1	anglicky
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Calormen	Calormen	k2eAgInSc4d1	Calormen
se	se	k3xPyFc4	se
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
překladu	překlad	k1gInSc6	překlad
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Kalornie	Kalornie	k1gFnSc1	Kalornie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
Kalormen	Kalormen	k1gInSc1	Kalormen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
čtyři	čtyři	k4xCgInPc4	čtyři
díly	díl	k1gInPc4	díl
(	(	kIx(	(
<g/>
Lev	Lev	k1gMnSc1	Lev
<g/>
,	,	kIx,	,
Kaspian	Kaspian	k1gMnSc1	Kaspian
<g/>
,	,	kIx,	,
Plavba	plavba	k1gFnSc1	plavba
a	a	k8xC	a
Židle	židle	k1gFnSc1	židle
<g/>
)	)	kIx)	)
natočila	natočit	k5eAaBmAgFnS	natočit
v	v	k7c6	v
letech	let	k1gInPc6	let
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
BBC	BBC	kA	BBC
jako	jako	k8xC	jako
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
jej	on	k3xPp3gNnSc4	on
upravila	upravit	k5eAaPmAgFnS	upravit
na	na	k7c4	na
3	[number]	k4	3
celovečerní	celovečerní	k2eAgInPc4d1	celovečerní
filmy	film	k1gInPc4	film
(	(	kIx(	(
<g/>
spojením	spojení	k1gNnSc7	spojení
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
dílu	díl	k1gInSc2	díl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyšly	vyjít	k5eAaPmAgFnP	vyjít
na	na	k7c6	na
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
zfilmoval	zfilmovat	k5eAaPmAgMnS	zfilmovat
režisér	režisér	k1gMnSc1	režisér
Andrew	Andrew	k1gMnSc1	Andrew
Adamson	Adamson	k1gMnSc1	Adamson
(	(	kIx(	(
<g/>
Shrek	Shrek	k1gMnSc1	Shrek
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
vysokorozpočtovou	vysokorozpočtová	k1gFnSc4	vysokorozpočtová
(	(	kIx(	(
<g/>
150	[number]	k4	150
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
USD	USD	kA	USD
<g/>
)	)	kIx)	)
fantasy	fantas	k1gInPc1	fantas
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
The	The	k1gFnSc2	The
Chronicles	Chronicles	k1gInSc1	Chronicles
of	of	k?	of
Narnia	Narnium	k1gNnSc2	Narnium
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Lion	Lion	k1gMnSc1	Lion
<g/>
,	,	kIx,	,
the	the	k?	the
Witch	Witch	k1gInSc1	Witch
and	and	k?	and
the	the	k?	the
Wardrobe	Wardrob	k1gMnSc5	Wardrob
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
premiéra	premiéra	k1gFnSc1	premiéra
byla	být	k5eAaImAgFnS	být
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
premiéra	premiéra	k1gFnSc1	premiéra
dalšího	další	k2eAgInSc2d1	další
zfilmovaného	zfilmovaný	k2eAgInSc2d1	zfilmovaný
dílu	díl	k1gInSc2	díl
-	-	kIx~	-
Prince	princ	k1gMnSc2	princ
Kaspiana	Kaspian	k1gMnSc2	Kaspian
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
opět	opět	k6eAd1	opět
Andrew	Andrew	k1gFnSc4	Andrew
Adamson	Adamsona	k1gFnPc2	Adamsona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
českých	český	k2eAgNnPc2d1	české
kin	kino	k1gNnPc2	kino
pak	pak	k6eAd1	pak
Kaspian	Kaspian	k1gInSc1	Kaspian
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
posledním	poslední	k2eAgInSc7d1	poslední
snímkem	snímek	k1gInSc7	snímek
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
z	z	k7c2	z
Narnie	Narnie	k1gFnSc2	Narnie
byla	být	k5eAaImAgFnS	být
Plavba	plavba	k1gFnSc1	plavba
Jitřního	jitřní	k2eAgMnSc2d1	jitřní
poutníka	poutník	k1gMnSc2	poutník
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Michael	Michael	k1gMnSc1	Michael
Apted	Apted	k1gMnSc1	Apted
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
světovou	světový	k2eAgFnSc4d1	světová
premiéru	premiéra	k1gFnSc4	premiéra
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Ohlášené	ohlášený	k2eAgNnSc4d1	ohlášené
pokračování	pokračování	k1gNnSc4	pokračování
je	být	k5eAaImIp3nS	být
The	The	k1gMnSc1	The
Chronicles	Chronicles	k1gMnSc1	Chronicles
of	of	k?	of
Narnia	Narnium	k1gNnSc2	Narnium
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Silver	Silver	k1gMnSc1	Silver
Chair	Chair	k1gMnSc1	Chair
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
režíruje	režírovat	k5eAaImIp3nS	režírovat
Joe	Joe	k1gFnSc1	Joe
Johnston	Johnston	k1gInSc1	Johnston
<g/>
.	.	kIx.	.
</s>
<s>
Všech	všecek	k3xTgMnPc2	všecek
sedm	sedm	k4xCc1	sedm
dílů	díl	k1gInPc2	díl
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
letech	let	k1gInPc6	let
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
na	na	k7c6	na
audiokazetách	audiokazeta	k1gFnPc6	audiokazeta
v	v	k7c6	v
Karmelitánském	karmelitánský	k2eAgNnSc6d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
díl	díl	k1gInSc1	díl
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
čtyřech	čtyři	k4xCgFnPc6	čtyři
audiokazetách	audiokazeta	k1gFnPc6	audiokazeta
<g/>
,	,	kIx,	,
čtou	číst	k5eAaImIp3nP	číst
různí	různý	k2eAgMnPc1d1	různý
interpreti	interpret	k1gMnPc1	interpret
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Mons	Mons	k1gInSc1	Mons
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Hermach	Hermach	k1gMnSc1	Hermach
<g/>
,	,	kIx,	,
zvukové	zvukový	k2eAgInPc4d1	zvukový
efekty	efekt	k1gInPc4	efekt
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
Vilém	Vilém	k1gMnSc1	Vilém
Bernáček	Bernáček	k1gMnSc1	Bernáček
<g/>
.	.	kIx.	.
</s>
<s>
Lev	Lev	k1gMnSc1	Lev
<g/>
,	,	kIx,	,
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
a	a	k8xC	a
skříň	skříň	k1gFnSc1	skříň
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přednes	přednes	k1gInSc1	přednes
Hanuš	Hanuš	k1gMnSc1	Hanuš
Bor	bor	k1gInSc1	bor
Princ	princ	k1gMnSc1	princ
Kaspián	Kaspián	k1gMnSc1	Kaspián
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čte	číst	k5eAaImIp3nS	číst
Otakar	Otakar	k1gMnSc1	Otakar
Bílek	Bílek	k1gMnSc1	Bílek
Plavba	plavba	k1gFnSc1	plavba
"	"	kIx"	"
<g/>
Cestovatele	cestovatel	k1gMnPc4	cestovatel
do	do	k7c2	do
úsvitu	úsvit	k1gInSc2	úsvit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čte	číst	k5eAaImIp3nS	číst
Otakar	Otakar	k1gMnSc1	Otakar
Bílek	Bílek	k1gMnSc1	Bílek
Stříbrná	stříbrnat	k5eAaImIp3nS	stříbrnat
židle	židle	k1gFnPc4	židle
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čte	číst	k5eAaImIp3nS	číst
Otakar	Otakar	k1gMnSc1	Otakar
Bílek	Bílek	k1gMnSc1	Bílek
Kůň	kůň	k1gMnSc1	kůň
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
chlapec	chlapec	k1gMnSc1	chlapec
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přednes	přednes	k1gInSc1	přednes
Hanuš	Hanuš	k1gMnSc1	Hanuš
Bor	bor	k1gInSc1	bor
Čarodějův	čarodějův	k2eAgMnSc1d1	čarodějův
synovec	synovec	k1gMnSc1	synovec
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čte	číst	k5eAaImIp3nS	číst
Josef	Josef	k1gMnSc1	Josef
Hermach	Hermach	k1gInSc1	Hermach
Poslední	poslední	k2eAgFnSc1d1	poslední
bitva	bitva	k1gFnSc1	bitva
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přednes	přednes	k1gInSc1	přednes
Hanuš	Hanuš	k1gMnSc1	Hanuš
Bor	bor	k1gInSc1	bor
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Fragment	fragment	k1gInSc1	fragment
na	na	k7c6	na
dvojCD	dvojCD	k?	dvojCD
(	(	kIx(	(
<g/>
MP	MP	kA	MP
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
všech	všecek	k3xTgInPc2	všecek
7	[number]	k4	7
dílů	díl	k1gInPc2	díl
načtených	načtený	k2eAgInPc2d1	načtený
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Táborským	Táborský	k1gMnSc7	Táborský
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
nahrávky	nahrávka	k1gFnSc2	nahrávka
je	být	k5eAaImIp3nS	být
36	[number]	k4	36
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
7	[number]	k4	7
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Narnia	Narnium	k1gNnSc2	Narnium
je	být	k5eAaImIp3nS	být
latinské	latinský	k2eAgNnSc4d1	latinské
jméno	jméno	k1gNnSc4	jméno
italského	italský	k2eAgNnSc2d1	italské
města	město	k1gNnSc2	město
Narni	Naren	k2eAgMnPc1d1	Naren
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
je	být	k5eAaImIp3nS	být
poblíž	poblíž	k7c2	poblíž
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lewis	Lewis	k1gInSc1	Lewis
pojmenoval	pojmenovat	k5eAaPmAgInS	pojmenovat
Narnii	Narnie	k1gFnSc4	Narnie
podle	podle	k7c2	podle
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
narazil	narazit	k5eAaPmAgMnS	narazit
jako	jako	k8xS	jako
dítě	dítě	k1gNnSc4	dítě
v	v	k7c6	v
atlase	atlas	k1gInSc6	atlas
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
kolem	kolem	k7c2	kolem
dvaceti	dvacet	k4xCc2	dvacet
let	léto	k1gNnPc2	léto
Lewis	Lewis	k1gFnPc2	Lewis
četl	číst	k5eAaImAgMnS	číst
množství	množství	k1gNnSc3	množství
latinských	latinský	k2eAgMnPc2d1	latinský
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnPc6	jejichž
dílech	dílo	k1gNnPc6	dílo
našel	najít	k5eAaPmAgMnS	najít
minimálně	minimálně	k6eAd1	minimálně
sedm	sedm	k4xCc4	sedm
odkazů	odkaz	k1gInPc2	odkaz
na	na	k7c4	na
Narnii	Narnie	k1gFnSc4	Narnie
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yRnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
Letopisy	letopis	k1gInPc1	letopis
Narnie	Narnie	k1gFnSc2	Narnie
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
výrazný	výrazný	k2eAgInSc4d1	výrazný
křesťanský	křesťanský	k2eAgInSc4d1	křesťanský
podtext	podtext	k1gInSc4	podtext
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
i	i	k9	i
pro	pro	k7c4	pro
připisovaný	připisovaný	k2eAgInSc4d1	připisovaný
sexismus	sexismus	k1gInSc4	sexismus
a	a	k8xC	a
rasismus	rasismus	k1gInSc4	rasismus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
podtextu	podtext	k1gInSc2	podtext
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
paralelám	paralela	k1gFnPc3	paralela
se	s	k7c7	s
základními	základní	k2eAgNnPc7d1	základní
tématy	téma	k1gNnPc7	téma
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
teologie	teologie	k1gFnPc4	teologie
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
Letopisy	letopis	k1gInPc1	letopis
Narnie	Narnie	k1gFnSc2	Narnie
obvykle	obvykle	k6eAd1	obvykle
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
odmítají	odmítat	k5eAaImIp3nP	odmítat
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yInSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mezi	mezi	k7c4	mezi
tato	tento	k3xDgNnPc4	tento
témata	téma	k1gNnPc4	téma
patří	patřit	k5eAaImIp3nS	patřit
Aslanovo	Aslanův	k2eAgNnSc1d1	Aslanovo
obětování	obětování	k1gNnSc1	obětování
se	se	k3xPyFc4	se
za	za	k7c2	za
Edmunda	Edmund	k1gMnSc2	Edmund
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
smrt	smrt	k1gFnSc4	smrt
a	a	k8xC	a
znovuožití	znovuožití	k1gNnSc4	znovuožití
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
vládu	vláda	k1gFnSc4	vláda
Bílé	bílý	k2eAgFnSc2d1	bílá
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
např.	např.	kA	např.
Aslanův	Aslanův	k2eAgInSc1d1	Aslanův
poslední	poslední	k2eAgInSc1d1	poslední
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Poslední	poslední	k2eAgFnSc6d1	poslední
bitvě	bitva	k1gFnSc6	bitva
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
jeho	jeho	k3xOp3gFnSc1	jeho
eticko-filosofická	etickoilosofický	k2eAgFnSc1d1	eticko-filosofický
rovina	rovina	k1gFnSc1	rovina
podle	podle	k7c2	podle
Lewisovy	Lewisův	k2eAgFnSc2d1	Lewisova
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
více	hodně	k6eAd2	hodně
místech	místo	k1gNnPc6	místo
jsou	být	k5eAaImIp3nP	být
kritizovány	kritizován	k2eAgInPc4d1	kritizován
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
projevy	projev	k1gInPc4	projev
nevíry	nevíra	k1gFnSc2	nevíra
<g/>
,	,	kIx,	,
noetického	noetický	k2eAgInSc2d1	noetický
materialismu	materialismus	k1gInSc2	materialismus
a	a	k8xC	a
relativismu	relativismus	k1gInSc2	relativismus
<g/>
.	.	kIx.	.
</s>
<s>
Letopisy	letopis	k1gInPc1	letopis
někteří	některý	k3yIgMnPc1	některý
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yRnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
obviňují	obviňovat	k5eAaImIp3nP	obviňovat
ze	z	k7c2	z
sexismu	sexismus	k1gInSc2	sexismus
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
hlavními	hlavní	k2eAgFnPc7d1	hlavní
představitelkami	představitelka	k1gFnPc7	představitelka
zla	zlo	k1gNnSc2	zlo
(	(	kIx(	(
<g/>
Bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
Zelená	zelený	k2eAgFnSc1d1	zelená
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
<g/>
)	)	kIx)	)
a	a	k8xC	a
mužům	muž	k1gMnPc3	muž
nemá	mít	k5eNaImIp3nS	mít
být	být	k5eAaImF	být
takto	takto	k6eAd1	takto
špatná	špatný	k2eAgFnSc1d1	špatná
role	role	k1gFnSc1	role
přisuzována	přisuzovat	k5eAaImNgFnS	přisuzovat
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
ovšem	ovšem	k9	ovšem
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
zlých	zlý	k2eAgFnPc2d1	zlá
mužských	mužský	k2eAgFnPc2d1	mužská
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
opačně	opačně	k6eAd1	opačně
pak	pak	k6eAd1	pak
ženské	ženský	k2eAgFnPc4d1	ženská
postavy	postava	k1gFnPc4	postava
výrazně	výrazně	k6eAd1	výrazně
pozitivní	pozitivní	k2eAgMnSc1d1	pozitivní
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
Lucinka	Lucinka	k1gFnSc1	Lucinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Poslední	poslední	k2eAgFnSc6d1	poslední
bitvě	bitva	k1gFnSc6	bitva
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
Zuzana	Zuzana	k1gFnSc1	Zuzana
ztratila	ztratit	k5eAaPmAgFnS	ztratit
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
Narnii	Narnie	k1gFnSc4	Narnie
a	a	k8xC	a
dala	dát	k5eAaPmAgFnS	dát
přednost	přednost	k1gFnSc4	přednost
"	"	kIx"	"
<g/>
rtěnkám	rtěnka	k1gFnPc3	rtěnka
<g/>
,	,	kIx,	,
punčochám	punčocha	k1gFnPc3	punčocha
a	a	k8xC	a
pozváním	pozvánět	k5eAaImIp1nS	pozvánět
na	na	k7c4	na
večírky	večírek	k1gInPc4	večírek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
někteří	některý	k3yIgMnPc1	některý
vykládají	vykládat	k5eAaImIp3nP	vykládat
jako	jako	k9	jako
odsudek	odsudek	k1gInSc4	odsudek
sexuální	sexuální	k2eAgFnSc2d1	sexuální
dospělosti	dospělost	k1gFnSc2	dospělost
a	a	k8xC	a
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Lewisovi	Lewisův	k2eAgMnPc1d1	Lewisův
obhájci	obhájce	k1gMnPc1	obhájce
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
kritiku	kritika	k1gFnSc4	kritika
materialismu	materialismus	k1gInSc2	materialismus
a	a	k8xC	a
komercionalismu	komercionalismus	k1gInSc2	komercionalismus
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
obhajobu	obhajoba	k1gFnSc4	obhajoba
dětského	dětský	k2eAgInSc2d1	dětský
světa	svět	k1gInSc2	svět
proti	proti	k7c3	proti
světu	svět	k1gInSc3	svět
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Letopisy	letopis	k1gInPc1	letopis
byly	být	k5eAaImAgInP	být
zejména	zejména	k9	zejména
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
označovány	označovat	k5eAaImNgFnP	označovat
i	i	k9	i
za	za	k7c4	za
rasistické	rasistický	k2eAgMnPc4d1	rasistický
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yRnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Knihy	kniha	k1gFnPc1	kniha
jsou	být	k5eAaImIp3nP	být
psány	psát	k5eAaImNgFnP	psát
anglocentricky	anglocentricky	k6eAd1	anglocentricky
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přišly	přijít	k5eAaPmAgFnP	přijít
do	do	k7c2	do
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
Angličané	Angličan	k1gMnPc1	Angličan
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
cesty	cesta	k1gFnPc1	cesta
do	do	k7c2	do
Narnie	Narnie	k1gFnSc2	Narnie
vedly	vést	k5eAaImAgInP	vést
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c4	mnoho
paralel	paralela	k1gFnPc2	paralela
mezi	mezi	k7c7	mezi
(	(	kIx(	(
<g/>
idealizovanou	idealizovaný	k2eAgFnSc7d1	idealizovaná
<g/>
)	)	kIx)	)
anglickou	anglický	k2eAgFnSc7d1	anglická
středověkou	středověký	k2eAgFnSc7d1	středověká
společností	společnost	k1gFnSc7	společnost
a	a	k8xC	a
narnijskou	narnijský	k2eAgFnSc7d1	narnijská
aristokracií	aristokracie	k1gFnSc7	aristokracie
<g/>
.	.	kIx.	.
</s>
<s>
Uvědomíme	uvědomit	k5eAaPmIp1nP	uvědomit
<g/>
-li	i	k?	-li
si	se	k3xPyFc3	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
série	série	k1gFnSc1	série
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
psána	psát	k5eAaImNgFnS	psát
pro	pro	k7c4	pro
anglické	anglický	k2eAgFnPc4d1	anglická
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
nelze	lze	k6eNd1	lze
očekávat	očekávat	k5eAaImF	očekávat
od	od	k7c2	od
autora	autor	k1gMnSc2	autor
pozitivní	pozitivní	k2eAgFnSc4d1	pozitivní
diskriminaci	diskriminace	k1gFnSc4	diskriminace
jiných	jiný	k2eAgFnPc2d1	jiná
národností	národnost	k1gFnPc2	národnost
či	či	k8xC	či
ras	rasa	k1gFnPc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
Letopisů	letopis	k1gInPc2	letopis
Narnie	Narnie	k1gFnSc2	Narnie
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yRnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
rasistické	rasistický	k2eAgNnSc4d1	rasistické
zobrazení	zobrazení	k1gNnSc4	zobrazení
jiných	jiný	k2eAgInPc2d1	jiný
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
Kalormenců	Kalormenec	k1gMnPc2	Kalormenec
jako	jako	k8xS	jako
inherentních	inherentní	k2eAgMnPc2d1	inherentní
nepřátel	nepřítel	k1gMnPc2	nepřítel
Narnie	Narnie	k1gFnSc2	Narnie
a	a	k8xC	a
Aslana	Aslan	k1gMnSc2	Aslan
<g/>
.	.	kIx.	.
</s>
<s>
Kalormenský	Kalormenský	k2eAgMnSc1d1	Kalormenský
bůh	bůh	k1gMnSc1	bůh
Taš	Taš	k1gFnSc2	Taš
je	být	k5eAaImIp3nS	být
satanská	satanský	k2eAgFnSc1d1	satanská
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
krvavé	krvavý	k2eAgFnPc4d1	krvavá
oběti	oběť	k1gFnPc4	oběť
a	a	k8xC	a
pokládá	pokládat	k5eAaImIp3nS	pokládat
zlé	zlý	k2eAgInPc4d1	zlý
skutky	skutek	k1gInPc4	skutek
za	za	k7c4	za
službu	služba	k1gFnSc4	služba
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Kalormňané	Kalormňan	k1gMnPc1	Kalormňan
jsou	být	k5eAaImIp3nP	být
popisováni	popisován	k2eAgMnPc1d1	popisován
jako	jako	k8xC	jako
snědí	snědý	k2eAgMnPc1d1	snědý
(	(	kIx(	(
<g/>
a	a	k8xC	a
text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
darkies	darkies	k1gInSc1	darkies
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
angličtině	angličtina	k1gFnSc6	angličtina
jednoznačně	jednoznačně	k6eAd1	jednoznačně
urážlivý	urážlivý	k2eAgInSc4d1	urážlivý
termín	termín	k1gInSc4	termín
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
odpovídající	odpovídající	k2eAgMnSc1d1	odpovídající
slovu	slout	k5eAaImIp1nS	slout
"	"	kIx"	"
<g/>
černouši	černouš	k1gMnPc1	černouš
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
je	být	k5eAaImIp3nS	být
však	však	k9	však
nutné	nutný	k2eAgNnSc1d1	nutné
poznamenat	poznamenat	k5eAaPmF	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
i	i	k9	i
dobré	dobrý	k2eAgFnPc4d1	dobrá
kalormenské	kalormenský	k2eAgFnPc4d1	kalormenský
postavy	postava	k1gFnPc4	postava
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
většina	většina	k1gFnSc1	většina
zlých	zlý	k2eAgFnPc2d1	zlá
postav	postava	k1gFnPc2	postava
má	mít	k5eAaImIp3nS	mít
světlou	světlý	k2eAgFnSc4d1	světlá
pleť	pleť	k1gFnSc4	pleť
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
Čarodějnice	čarodějnice	k1gFnPc1	čarodějnice
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Miraz	Miraz	k1gInSc1	Miraz
a	a	k8xC	a
strýc	strýc	k1gMnSc1	strýc
Ondřej	Ondřej	k1gMnSc1	Ondřej
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
a	a	k8xC	a
Lucie	Lucie	k1gFnSc1	Lucie
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
měli	mít	k5eAaImAgMnP	mít
jmenovat	jmenovat	k5eAaImF	jmenovat
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
a	a	k8xC	a
Rose	Rose	k1gMnSc1	Rose
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
návrh	návrh	k1gInSc1	návrh
druhého	druhý	k4xOgInSc2	druhý
dílu	díl	k1gInSc2	díl
neobsahoval	obsahovat	k5eNaImAgInS	obsahovat
zmínku	zmínka	k1gFnSc4	zmínka
o	o	k7c6	o
Aslanovi	Aslan	k1gMnSc6	Aslan
<g/>
.	.	kIx.	.
</s>
<s>
Clive	Clivat	k5eAaPmIp3nS	Clivat
Staples	Staples	k1gInSc1	Staples
Lewis	Lewis	k1gFnSc2	Lewis
Narnie	Narnie	k1gFnSc2	Narnie
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
The	The	k1gMnSc1	The
Chronicles	Chronicles	k1gMnSc1	Chronicles
of	of	k?	of
Narnia	Narnium	k1gNnSc2	Narnium
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Letopisy	letopis	k1gInPc4	letopis
Narnie	Narnie	k1gFnSc1	Narnie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Letopisy	letopis	k1gInPc1	letopis
Narnie	Narnie	k1gFnSc2	Narnie
na	na	k7c6	na
ČSFD	ČSFD	kA	ČSFD
Rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
český	český	k2eAgInSc1d1	český
web	web	k1gInSc1	web
o	o	k7c6	o
knihách	kniha	k1gFnPc6	kniha
Letopisy	letopis	k1gInPc4	letopis
Narnie	Narnie	k1gFnPc1	Narnie
</s>
