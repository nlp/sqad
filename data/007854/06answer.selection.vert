<s>
Slovní	slovní	k2eAgMnPc4d1	slovní
druhy	druh	k1gMnPc4	druh
Čeština	čeština	k1gFnSc1	čeština
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
tradičně	tradičně	k6eAd1	tradičně
10	[number]	k4	10
slovních	slovní	k2eAgInPc2d1	slovní
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
ohebné	ohebný	k2eAgFnSc6d1	ohebná
a	a	k8xC	a
neohebné	ohebný	k2eNgFnSc6d1	neohebná
<g/>
:	:	kIx,	:
ohebné	ohebný	k2eAgFnSc6d1	ohebná
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
se	s	k7c7	s
–	–	k?	–
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
zájmena	zájmeno	k1gNnPc1	zájmeno
<g/>
,	,	kIx,	,
číslovky	číslovka	k1gFnPc1	číslovka
<g/>
;	;	kIx,	;
časují	časovat	k5eAaImIp3nP	časovat
se	se	k3xPyFc4	se
–	–	k?	–
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
;	;	kIx,	;
neohebné	ohebný	k2eNgNnSc1d1	neohebné
–	–	k?	–
příslovce	příslovce	k1gNnSc1	příslovce
<g/>
,	,	kIx,	,
předložky	předložka	k1gFnPc1	předložka
<g/>
,	,	kIx,	,
spojky	spojka	k1gFnPc1	spojka
<g/>
,	,	kIx,	,
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
citoslovce	citoslovce	k1gNnSc1	citoslovce
<g/>
.	.	kIx.	.
</s>
