<s>
Minerva	Minerva	k1gFnSc1	Minerva
je	být	k5eAaImIp3nS	být
římská	římský	k2eAgFnSc1d1	římská
bohyně	bohyně	k1gFnSc1	bohyně
moudrosti	moudrost	k1gFnSc2	moudrost
a	a	k8xC	a
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
,	,	kIx,	,
ztotožněná	ztotožněný	k2eAgFnSc1d1	ztotožněná
s	s	k7c7	s
řeckou	řecký	k2eAgFnSc7d1	řecká
bohyní	bohyně	k1gFnSc7	bohyně
Athénou	Athéna	k1gFnSc7	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jupiterem	Jupiter	k1gInSc7	Jupiter
a	a	k8xC	a
Juno	Juno	k1gFnSc1	Juno
byla	být	k5eAaImAgFnS	být
členem	člen	k1gMnSc7	člen
Triády	triáda	k1gFnSc2	triáda
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
panenskou	panenský	k2eAgFnSc7d1	panenská
bohyní	bohyně	k1gFnSc7	bohyně
Etrusků	Etrusk	k1gMnPc2	Etrusk
(	(	kIx(	(
<g/>
Menrva	Menrva	k1gFnSc1	Menrva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
kult	kult	k1gInSc4	kult
bohyně	bohyně	k1gFnSc2	bohyně
převzali	převzít	k5eAaPmAgMnP	převzít
Římané	Říman	k1gMnPc1	Říman
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
před	před	k7c7	před
Kr.	Kr.	k1gFnSc7	Kr.
Pod	pod	k7c7	pod
řeckým	řecký	k2eAgInSc7d1	řecký
vlivem	vliv	k1gInSc7	vliv
byla	být	k5eAaImAgFnS	být
ztotožněna	ztotožnit	k5eAaPmNgFnS	ztotožnit
s	s	k7c7	s
Athénou	Athéna	k1gFnSc7	Athéna
a	a	k8xC	a
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
ochránkyní	ochránkyně	k1gFnSc7	ochránkyně
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Etruskové	Etrusk	k1gMnPc1	Etrusk
ctili	ctít	k5eAaImAgMnP	ctít
Minervu	Minerva	k1gFnSc4	Minerva
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
bleskometnou	bleskometný	k2eAgFnSc4d1	bleskometný
<g/>
"	"	kIx"	"
bohyni	bohyně	k1gFnSc4	bohyně
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgNnP	být
uctívána	uctívat	k5eAaImNgNnP	uctívat
také	také	k6eAd1	také
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ale	ale	k8xC	ale
pod	pod	k7c7	pod
řeckým	řecký	k2eAgInSc7d1	řecký
vlivem	vliv	k1gInSc7	vliv
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
výhradně	výhradně	k6eAd1	výhradně
za	za	k7c4	za
bohyni	bohyně	k1gFnSc4	bohyně
všech	všecek	k3xTgNnPc2	všecek
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
vynálezů	vynález	k1gInPc2	vynález
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
svatyně	svatyně	k1gFnSc1	svatyně
Minervy	Minerva	k1gFnSc2	Minerva
se	se	k3xPyFc4	se
nalézaly	nalézat	k5eAaImAgInP	nalézat
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
na	na	k7c6	na
Kapitolu	Kapitol	k1gInSc6	Kapitol
<g/>
,	,	kIx,	,
na	na	k7c6	na
Aventinu	Aventin	k1gInSc6	Aventin
a	a	k8xC	a
na	na	k7c6	na
Coeliu	Coelium	k1gNnSc6	Coelium
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Minerva	Minerva	k1gFnSc1	Minerva
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
