<s>
Jade	Jad	k1gMnSc2	Jad
Louise	Louis	k1gMnSc2	Louis
Ewen	Ewen	k1gMnSc1	Ewen
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
anglická	anglický	k2eAgFnSc1d1	anglická
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
písničkářka	písničkářka	k1gFnSc1	písničkářka
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
působí	působit	k5eAaImIp3nP	působit
v	v	k7c6	v
dívčí	dívčí	k2eAgFnSc6d1	dívčí
popové	popový	k2eAgFnSc6d1	popová
skupině	skupina	k1gFnSc6	skupina
Sugababes	Sugababesa	k1gFnPc2	Sugababesa
<g/>
.	.	kIx.	.
</s>
<s>
Vešla	vejít	k5eAaPmAgFnS	vejít
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vítězstvím	vítězství	k1gNnSc7	vítězství
v	v	k7c6	v
reality-show	realityhow	k?	reality-show
Your	Your	k1gInSc1	Your
Country	country	k2eAgInSc1d1	country
Needs	Needs	k1gInSc1	Needs
You	You	k1gFnSc2	You
a	a	k8xC	a
následnou	následný	k2eAgFnSc7d1	následná
reprezentací	reprezentace	k1gFnSc7	reprezentace
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
na	na	k7c4	na
Eurovision	Eurovision	k1gInSc4	Eurovision
Song	song	k1gInSc1	song
Contest	Contest	k1gFnSc1	Contest
2009	[number]	k4	2009
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
soutěžní	soutěžní	k2eAgFnSc1d1	soutěžní
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
My	my	k3xPp1nPc1	my
Time	Time	k1gNnPc4	Time
<g/>
"	"	kIx"	"
napsali	napsat	k5eAaPmAgMnP	napsat
Andrew	Andrew	k1gMnPc1	Andrew
Lloyd	Lloyd	k1gMnSc1	Lloyd
Webber	Webber	k1gMnSc1	Webber
a	a	k8xC	a
Diane	Dian	k1gInSc5	Dian
Warren	Warrno	k1gNnPc2	Warrno
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
obsadila	obsadit	k5eAaPmAgFnS	obsadit
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
173	[number]	k4	173
bodů	bod	k1gInPc2	bod
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
umístění	umístění	k1gNnSc4	umístění
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
jamajské	jamajský	k2eAgFnSc2d1	jamajská
matce	matka	k1gFnSc3	matka
a	a	k8xC	a
skotsko-italskému	skotskotalský	k2eAgMnSc3d1	skotsko-italský
otci	otec	k1gMnSc3	otec
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
její	její	k3xOp3gMnPc1	její
rodiče	rodič	k1gMnPc1	rodič
jsou	být	k5eAaImIp3nP	být
zdravotně	zdravotně	k6eAd1	zdravotně
postiženi	postihnout	k5eAaPmNgMnP	postihnout
-	-	kIx~	-
otec	otec	k1gMnSc1	otec
je	být	k5eAaImIp3nS	být
nevidomý	vidomý	k2eNgMnSc1d1	nevidomý
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
neslyšící	slyšící	k2eNgMnSc1d1	neslyšící
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
nevidomá	vidomý	k2eNgFnSc1d1	nevidomá
<g/>
.	.	kIx.	.
</s>
<s>
Odmala	odmala	k6eAd1	odmala
zpívala	zpívat	k5eAaImAgFnS	zpívat
a	a	k8xC	a
hrála	hrát	k5eAaImAgFnS	hrát
-	-	kIx~	-
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
dvanácti	dvanáct	k4xCc2	dvanáct
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
ucházela	ucházet	k5eAaImAgFnS	ucházet
o	o	k7c4	o
roli	role	k1gFnSc4	role
Naly	Nala	k1gFnSc2	Nala
v	v	k7c4	v
představení	představení	k1gNnSc4	představení
divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c4	na
West	West	k1gInSc4	West
End	End	k1gMnSc1	End
<g/>
,	,	kIx,	,
Lví	lví	k2eAgMnSc1d1	lví
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
začala	začít	k5eAaPmAgFnS	začít
působit	působit	k5eAaImF	působit
v	v	k7c6	v
dívčí	dívčí	k2eAgFnSc6d1	dívčí
skupině	skupina	k1gFnSc6	skupina
Trinity	Trinita	k1gFnSc2	Trinita
Stone	ston	k1gInSc5	ston
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obdržela	obdržet	k5eAaPmAgFnS	obdržet
nahrávací	nahrávací	k2eAgFnSc4d1	nahrávací
smlouvu	smlouva	k1gFnSc4	smlouva
od	od	k7c2	od
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Sony	Sony	kA	Sony
BMG	BMG	kA	BMG
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
žádné	žádný	k3yNgNnSc4	žádný
album	album	k1gNnSc4	album
nevydala	vydat	k5eNaPmAgFnS	vydat
a	a	k8xC	a
po	po	k7c6	po
dvouletém	dvouletý	k2eAgNnSc6d1	dvouleté
působení	působení	k1gNnSc6	působení
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
prosadit	prosadit	k5eAaPmF	prosadit
jako	jako	k9	jako
herečka	herečka	k1gFnSc1	herečka
v	v	k7c6	v
televizních	televizní	k2eAgFnPc6d1	televizní
produkcích	produkce	k1gFnPc6	produkce
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
reality-show	realityhow	k?	reality-show
Your	Your	k1gInSc4	Your
Country	country	k2eAgInSc4d1	country
Needs	Needs	k1gInSc4	Needs
You	You	k1gMnPc2	You
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
autoři	autor	k1gMnPc1	autor
si	se	k3xPyFc3	se
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
dali	dát	k5eAaPmAgMnP	dát
vybrat	vybrat	k5eAaPmF	vybrat
reprezentanta	reprezentant	k1gMnSc4	reprezentant
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc3	království
na	na	k7c6	na
Eurovizi	Eurovize	k1gFnSc6	Eurovize
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
soutěžní	soutěžní	k2eAgFnSc2d1	soutěžní
skladby	skladba	k1gFnSc2	skladba
pro	pro	k7c4	pro
vítěze	vítěz	k1gMnSc4	vítěz
byl	být	k5eAaImAgMnS	být
sir	sir	k1gMnSc1	sir
Andrew	Andrew	k1gMnSc1	Andrew
Lloyd	Lloyd	k1gMnSc1	Lloyd
Webber	Webber	k1gMnSc1	Webber
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
promo	promo	k6eAd1	promo
tour	toura	k1gFnPc2	toura
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
Anglie	Anglie	k1gFnSc2	Anglie
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c6	na
Maltě	Malta	k1gFnSc6	Malta
<g/>
,	,	kIx,	,
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
Bosně	Bosna	k1gFnSc6	Bosna
a	a	k8xC	a
Hercegovině	Hercegovina	k1gFnSc6	Hercegovina
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
finále	finále	k1gNnSc6	finále
Eurovize	Eurovize	k1gFnSc2	Eurovize
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vystoupení	vystoupení	k1gNnSc6	vystoupení
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
doprovodil	doprovodit	k5eAaPmAgMnS	doprovodit
Andrew	Andrew	k1gMnSc1	Andrew
Lloyd	Lloyd	k1gMnSc1	Lloyd
Webber	Webber	k1gMnSc1	Webber
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
hlasování	hlasování	k1gNnSc2	hlasování
Jade	Jad	k1gFnSc2	Jad
obdržela	obdržet	k5eAaPmAgFnS	obdržet
173	[number]	k4	173
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
dvanáctibodového	dvanáctibodový	k2eAgNnSc2d1	dvanáctibodové
ohodnocení	ohodnocení	k1gNnSc2	ohodnocení
z	z	k7c2	z
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
sedmi	sedm	k4xCc2	sedm
bodů	bod	k1gInPc2	bod
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
šesti	šest	k4xCc2	šest
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
obsadila	obsadit	k5eAaPmAgFnS	obsadit
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výsledek	výsledek	k1gInSc4	výsledek
pro	pro	k7c4	pro
Spojené	spojený	k2eAgNnSc4d1	spojené
království	království	k1gNnSc4	království
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vystoupení	vystoupení	k1gNnSc6	vystoupení
na	na	k7c6	na
Eurovizi	Eurovize	k1gFnSc6	Eurovize
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
debutovém	debutový	k2eAgNnSc6d1	debutové
albu	album	k1gNnSc6	album
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Man	mana	k1gFnPc2	mana
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2009	[number]	k4	2009
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Keishu	Keisha	k1gFnSc4	Keisha
Buchanan	Buchanany	k1gInPc2	Buchanany
v	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
dívčí	dívčí	k2eAgFnSc6d1	dívčí
popové	popový	k2eAgFnSc6d1	popová
formaci	formace	k1gFnSc6	formace
Sugababes	Sugababesa	k1gFnPc2	Sugababesa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
jejího	její	k3xOp3gNnSc2	její
působení	působení	k1gNnSc2	působení
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
dívky	dívka	k1gFnSc2	dívka
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
dvakrát	dvakrát	k6eAd1	dvakrát
top	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
britské	britský	k2eAgFnSc2d1	britská
hitparády	hitparáda	k1gFnSc2	hitparáda
singlů	singl	k1gInPc2	singl
a	a	k8xC	a
vydaly	vydat	k5eAaPmAgFnP	vydat
Sweet	Sweet	k1gInSc4	Sweet
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
si	se	k3xPyFc3	se
skupina	skupina	k1gFnSc1	skupina
dává	dávat	k5eAaImIp3nS	dávat
pauzu	pauza	k1gFnSc4	pauza
-	-	kIx~	-
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
hodlají	hodlat	k5eAaImIp3nP	hodlat
vydat	vydat	k5eAaPmF	vydat
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
udržovala	udržovat	k5eAaImAgFnS	udržovat
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
anglickým	anglický	k2eAgInSc7d1	anglický
hercem	herc	k1gInSc7	herc
Rickym	Rickym	k1gInSc1	Rickym
Norwoodem	Norwood	k1gInSc7	Norwood
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2011	[number]	k4	2011
se	se	k3xPyFc4	se
rozešli	rozejít	k5eAaPmAgMnP	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
diskografie	diskografie	k1gFnSc1	diskografie
během	během	k7c2	během
působení	působení	k1gNnSc2	působení
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Sugababes	Sugababesa	k1gFnPc2	Sugababesa
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
nalezení	nalezení	k1gNnSc3	nalezení
v	v	k7c6	v
článku	článek	k1gInSc6	článek
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
formaci	formace	k1gFnSc6	formace
<g/>
.	.	kIx.	.
</s>
<s>
Eurovision	Eurovision	k1gInSc1	Eurovision
Song	song	k1gInSc1	song
Contest	Contest	k1gFnSc1	Contest
2009	[number]	k4	2009
Sugababes	Sugababes	k1gMnSc1	Sugababes
Andrew	Andrew	k1gMnSc1	Andrew
Lloyd	Lloyd	k1gMnSc1	Lloyd
Webber	Webber	k1gMnSc1	Webber
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jade	Jade	k1gFnPc2	Jade
Ewenová	Ewenová	k1gFnSc1	Ewenová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Oficiální	oficiální	k2eAgInSc4d1	oficiální
web	web	k1gInSc4	web
Sugababes	Sugababes	k1gMnSc1	Sugababes
Twitter	Twitter	k1gMnSc1	Twitter
IMDb	IMDb	k1gInSc4	IMDb
Vystoupení	vystoupení	k1gNnSc2	vystoupení
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Eurovize	Eurovize	k1gFnSc2	Eurovize
2009	[number]	k4	2009
(	(	kIx(	(
<g/>
YouTube	YouTub	k1gInSc5	YouTub
<g/>
)	)	kIx)	)
Videoklip	videoklip	k1gInSc1	videoklip
"	"	kIx"	"
<g/>
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
My	my	k3xPp1nPc1	my
Time	Time	k1gNnPc7	Time
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
YouTube	YouTub	k1gInSc5	YouTub
<g/>
)	)	kIx)	)
</s>
