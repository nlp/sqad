<s>
Nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
hpingu	hping	k1gInSc2	hping
(	(	kIx(	(
<g/>
hping	hping	k1gInSc1	hping
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
skriptovací	skriptovací	k2eAgInSc4d1	skriptovací
jazyk	jazyk	k1gInSc4	jazyk
Tcl	Tcl	k1gFnSc2	Tcl
a	a	k8xC	a
implementuje	implementovat	k5eAaImIp3nS	implementovat
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
čitelným	čitelný	k2eAgInSc7d1	čitelný
popisem	popis	k1gInSc7	popis
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
paketů	paket	k1gInPc2	paket
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
programátor	programátor	k1gMnSc1	programátor
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
psát	psát	k5eAaImF	psát
skripty	skript	k1gInPc4	skript
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
manipulují	manipulovat	k5eAaImIp3nP	manipulovat
nebo	nebo	k8xC	nebo
analyzují	analyzovat	k5eAaImIp3nP	analyzovat
přímo	přímo	k6eAd1	přímo
nízkoúrovňové	nízkoúrovňová	k1gFnPc4	nízkoúrovňová
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
<g/>
.	.	kIx.	.
</s>
