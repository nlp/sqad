<p>
<s>
Hping	Hping	k1gInSc1	Hping
je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
šiřitelný	šiřitelný	k2eAgInSc1d1	šiřitelný
generátor	generátor	k1gInSc1	generátor
a	a	k8xC	a
analyzátor	analyzátor	k1gInSc1	analyzátor
paketů	paket	k1gInPc2	paket
pro	pro	k7c4	pro
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
protokol	protokol	k1gInSc1	protokol
od	od	k7c2	od
Salvatore	Salvator	k1gMnSc5	Salvator
Sanfilippo	Sanfilippa	k1gFnSc5	Sanfilippa
(	(	kIx(	(
<g/>
také	také	k9	také
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
Antirez	Antirez	k1gMnSc1	Antirez
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hping	Hping	k1gInSc1	Hping
je	být	k5eAaImIp3nS	být
de	de	k?	de
facto	facto	k1gNnSc1	facto
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nástrojů	nástroj	k1gInPc2	nástroj
pro	pro	k7c4	pro
bezpečnostní	bezpečnostní	k2eAgFnPc4d1	bezpečnostní
prověrky	prověrka	k1gFnPc4	prověrka
<g/>
,	,	kIx,	,
testování	testování	k1gNnPc4	testování
firewallů	firewall	k1gInPc2	firewall
a	a	k8xC	a
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
autor	autor	k1gMnSc1	autor
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
skenovací	skenovací	k2eAgFnSc4d1	skenovací
techniku	technika	k1gFnSc4	technika
idle	idlat	k5eAaPmIp3nS	idlat
scan	scan	k1gInSc1	scan
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
implementována	implementovat	k5eAaImNgFnS	implementovat
v	v	k7c6	v
nástroji	nástroj	k1gInSc6	nástroj
Nmap	Nmap	k1gMnSc1	Nmap
Security	Securita	k1gFnSc2	Securita
Scanner	scanner	k1gInSc1	scanner
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
hpingu	hping	k1gInSc2	hping
(	(	kIx(	(
<g/>
hping	hping	k1gInSc1	hping
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
skriptovací	skriptovací	k2eAgInSc4d1	skriptovací
jazyk	jazyk	k1gInSc4	jazyk
Tcl	Tcl	k1gFnSc2	Tcl
a	a	k8xC	a
implementuje	implementovat	k5eAaImIp3nS	implementovat
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
čitelným	čitelný	k2eAgInSc7d1	čitelný
popisem	popis	k1gInSc7	popis
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
paketů	paket	k1gInPc2	paket
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
programátor	programátor	k1gMnSc1	programátor
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
psát	psát	k5eAaImF	psát
skripty	skript	k1gInPc4	skript
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
manipulují	manipulovat	k5eAaImIp3nP	manipulovat
nebo	nebo	k8xC	nebo
analyzují	analyzovat	k5eAaImIp3nP	analyzovat
přímo	přímo	k6eAd1	přímo
nízkoúrovňové	nízkoúrovňová	k1gFnPc4	nízkoúrovňová
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Nmap	Nmap	k1gMnSc1	Nmap
Security	Securita	k1gFnSc2	Securita
Scanner	scanner	k1gInSc1	scanner
–	–	k?	–
nmap	nmap	k1gInSc1	nmap
a	a	k8xC	a
hping	hping	k1gInSc1	hping
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
WWW	WWW	kA	WWW
stránka	stránka	k1gFnSc1	stránka
programu	program	k1gInSc2	program
Hping	Hpinga	k1gFnPc2	Hpinga
</s>
</p>
<p>
<s>
Wiki	Wik	k1gMnPc1	Wik
programu	program	k1gInSc2	program
Hping	Hping	k1gInSc1	Hping
</s>
</p>
<p>
<s>
Idle	Idlat	k5eAaPmIp3nS	Idlat
Scanning	Scanning	k1gInSc1	Scanning
<g/>
,	,	kIx,	,
napsáno	napsán	k2eAgNnSc1d1	napsáno
Fyodorem	Fyodor	k1gMnSc7	Fyodor
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
Nmapu	Nmap	k1gInSc2	Nmap
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hping	Hping	k1gInSc1	Hping
2	[number]	k4	2
opraven	opravna	k1gFnPc2	opravna
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
XP	XP	kA	XP
SP2	SP2	k1gFnSc1	SP2
(	(	kIx(	(
<g/>
Service	Service	k1gFnSc1	Service
Pack	Pack	k1gInSc1	Pack
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hping	Hpinga	k1gFnPc2	Hpinga
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
