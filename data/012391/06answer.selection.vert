<s>
Hlinkova	Hlinkův	k2eAgFnSc1d1	Hlinkova
slovenská	slovenský	k2eAgFnSc1d1	slovenská
ľudová	ľudový	k2eAgFnSc1d1	ľudová
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Hlinkova	Hlinkův	k2eAgFnSc1d1	Hlinkova
slovenská	slovenský	k2eAgFnSc1d1	slovenská
lidová	lidový	k2eAgFnSc1d1	lidová
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
HSLS	HSLS	kA	HSLS
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
slovenská	slovenský	k2eAgFnSc1d1	slovenská
pravicová	pravicový	k2eAgFnSc1d1	pravicová
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
působící	působící	k2eAgFnSc1d1	působící
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
