<p>
<s>
Hlinkova	Hlinkův	k2eAgFnSc1d1	Hlinkova
slovenská	slovenský	k2eAgFnSc1d1	slovenská
ľudová	ľudový	k2eAgFnSc1d1	ľudová
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Hlinkova	Hlinkův	k2eAgFnSc1d1	Hlinkova
slovenská	slovenský	k2eAgFnSc1d1	slovenská
lidová	lidový	k2eAgFnSc1d1	lidová
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
HSLS	HSLS	kA	HSLS
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
slovenská	slovenský	k2eAgFnSc1d1	slovenská
pravicová	pravicový	k2eAgFnSc1d1	pravicová
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
působící	působící	k2eAgFnSc1d1	působící
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
původním	původní	k2eAgInSc7d1	původní
cílem	cíl	k1gInSc7	cíl
byla	být	k5eAaImAgFnS	být
autonomie	autonomie	k1gFnSc1	autonomie
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
však	však	k9	však
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
převládly	převládnout	k5eAaPmAgFnP	převládnout
autoritativní	autoritativní	k2eAgFnPc1d1	autoritativní
a	a	k8xC	a
fašistické	fašistický	k2eAgFnPc1d1	fašistická
tendence	tendence	k1gFnPc1	tendence
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vedoucí	vedoucí	k2eAgFnSc7d1	vedoucí
politickou	politický	k2eAgFnSc7d1	politická
silou	síla	k1gFnSc7	síla
tzv.	tzv.	kA	tzv.
Slovenského	slovenský	k2eAgInSc2d1	slovenský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opírala	opírat	k5eAaImAgFnS	opírat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
katolický	katolický	k2eAgInSc4d1	katolický
klérus	klérus	k1gInSc4	klérus
<g/>
,	,	kIx,	,
nacionalistickou	nacionalistický	k2eAgFnSc4d1	nacionalistická
inteligenci	inteligence	k1gFnSc4	inteligence
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
křesťansky	křesťansky	k6eAd1	křesťansky
a	a	k8xC	a
nacionalisticky	nacionalisticky	k6eAd1	nacionalisticky
smýšlející	smýšlející	k2eAgNnSc4d1	smýšlející
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tiskovým	tiskový	k2eAgInSc7d1	tiskový
orgánem	orgán	k1gInSc7	orgán
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgMnS	být
Slovák	Slovák	k1gMnSc1	Slovák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
do	do	k7c2	do
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
===	===	k?	===
</s>
</p>
<p>
<s>
Strana	strana	k1gFnSc1	strana
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Andrejem	Andrej	k1gMnSc7	Andrej
Hlinkou	Hlinka	k1gMnSc7	Hlinka
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
jejím	její	k3xOp3gMnSc7	její
předsedou	předseda	k1gMnSc7	předseda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
ľudová	ľudový	k2eAgFnSc1d1	ľudová
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
prvních	první	k4xOgFnPc2	první
voleb	volba	k1gFnPc2	volba
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
její	její	k3xOp3gInPc1	její
počátky	počátek	k1gInPc1	počátek
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
slovenského	slovenský	k2eAgInSc2d1	slovenský
autonomistického	autonomistický	k2eAgInSc2d1	autonomistický
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
separatistického	separatistický	k2eAgNnSc2d1	separatistické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
byl	být	k5eAaImAgInS	být
její	její	k3xOp3gInSc1	její
název	název	k1gInSc1	název
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
Hlinkova	Hlinkův	k2eAgFnSc1d1	Hlinkova
slovenská	slovenský	k2eAgFnSc1d1	slovenská
ľudová	ľudový	k2eAgFnSc1d1	ľudová
strana	strana	k1gFnSc1	strana
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
většího	veliký	k2eAgNnSc2d2	veliký
odlišení	odlišení	k1gNnSc2	odlišení
od	od	k7c2	od
Šrámkových	Šrámkových	k2eAgMnPc2d1	Šrámkových
československých	československý	k2eAgMnPc2d1	československý
lidovců	lidovec	k1gMnPc2	lidovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
československé	československý	k2eAgFnSc2d1	Československá
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prosazovala	prosazovat	k5eAaImAgFnS	prosazovat
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
,	,	kIx,	,
vládu	vláda	k1gFnSc4	vláda
opustila	opustit	k5eAaPmAgFnS	opustit
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
uvěznění	uvěznění	k1gNnSc3	uvěznění
svého	svůj	k1gMnSc2	svůj
poslance	poslanec	k1gMnSc2	poslanec
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Tuky	tuk	k1gInPc1	tuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
třicátých	třicátý	k4xOgInPc2	třicátý
let	léto	k1gNnPc2	léto
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
se	s	k7c7	s
Slovenskou	slovenský	k2eAgFnSc7d1	slovenská
národnou	národný	k2eAgFnSc7d1	národná
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
s	s	k7c7	s
ukrajinskými	ukrajinský	k2eAgFnPc7d1	ukrajinská
<g/>
,	,	kIx,	,
polskými	polský	k2eAgFnPc7d1	polská
<g/>
,	,	kIx,	,
maďarskými	maďarský	k2eAgFnPc7d1	maďarská
a	a	k8xC	a
německými	německý	k2eAgFnPc7d1	německá
separatistickými	separatistický	k2eAgFnPc7d1	separatistická
stranami	strana	k1gFnPc7	strana
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
italským	italský	k2eAgInSc7d1	italský
a	a	k8xC	a
rakouským	rakouský	k2eAgInSc7d1	rakouský
fašismem	fašismus	k1gInSc7	fašismus
a	a	k8xC	a
německým	německý	k2eAgInSc7d1	německý
nacismem	nacismus	k1gInSc7	nacismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1936	[number]	k4	1936
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
k	k	k7c3	k
tehdejšímu	tehdejší	k2eAgNnSc3d1	tehdejší
fašistickému	fašistický	k2eAgNnSc3d1	fašistické
hnutí	hnutí	k1gNnSc3	hnutí
<g/>
,	,	kIx,	,
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
svůj	svůj	k3xOyFgInSc4	svůj
požadavek	požadavek	k1gInSc4	požadavek
totalitní	totalitní	k2eAgFnSc2d1	totalitní
fašistické	fašistický	k2eAgFnSc2d1	fašistická
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
od	od	k7c2	od
NSDAP	NSDAP	kA	NSDAP
převzaté	převzatý	k2eAgNnSc1d1	převzaté
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
jeden	jeden	k4xCgInSc1	jeden
národ	národ	k1gInSc1	národ
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
vodca	vodca	k1gMnSc1	vodca
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1938	[number]	k4	1938
jednal	jednat	k5eAaImAgMnS	jednat
Hlinka	Hlinka	k1gMnSc1	Hlinka
s	s	k7c7	s
K.	K.	kA	K.
H.	H.	kA	H.
Frankem	Frank	k1gMnSc7	Frank
ze	z	k7c2	z
Sudetoněmecké	sudetoněmecký	k2eAgFnSc2d1	Sudetoněmecká
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
SdP	SdP	k1gMnSc1	SdP
<g/>
)	)	kIx)	)
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
<g/>
;	;	kIx,	;
SdP	SdP	k1gFnSc1	SdP
pak	pak	k6eAd1	pak
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
hlinkovcům	hlinkovec	k1gMnPc3	hlinkovec
finanční	finanční	k2eAgFnSc4d1	finanční
podporu	podpora	k1gFnSc4	podpora
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
pět	pět	k4xCc1	pět
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Hlinkově	Hlinkův	k2eAgFnSc6d1	Hlinkova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1938	[number]	k4	1938
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc2	vedení
strany	strana	k1gFnSc2	strana
ujal	ujmout	k5eAaPmAgMnS	ujmout
Jozef	Jozef	k1gMnSc1	Jozef
Tiso	Tisa	k1gFnSc5	Tisa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Strana	strana	k1gFnSc1	strana
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
na	na	k7c6	na
katolickém	katolický	k2eAgInSc6d1	katolický
základě	základ	k1gInSc6	základ
a	a	k8xC	a
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
několik	několik	k4yIc4	několik
frakcí	frakce	k1gFnPc2	frakce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
shodovaly	shodovat	k5eAaImAgFnP	shodovat
na	na	k7c6	na
požadavku	požadavek	k1gInSc6	požadavek
autonomie	autonomie	k1gFnSc2	autonomie
a	a	k8xC	a
katolické	katolický	k2eAgFnSc3d1	katolická
víře	víra	k1gFnSc3	víra
<g/>
.	.	kIx.	.
</s>
<s>
Radikální	radikální	k2eAgNnSc4d1	radikální
křídlo	křídlo	k1gNnSc4	křídlo
vedl	vést	k5eAaImAgMnS	vést
Karol	Karol	k1gInSc4	Karol
Sidor	Sidor	k1gMnSc1	Sidor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1939	[number]	k4	1939
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
"	"	kIx"	"
<g/>
německou	německý	k2eAgFnSc4d1	německá
<g/>
"	"	kIx"	"
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lídry	lídr	k1gMnPc7	lídr
umírněného	umírněný	k2eAgNnSc2d1	umírněné
křídla	křídlo	k1gNnSc2	křídlo
patřil	patřit	k5eAaImAgMnS	patřit
Jozef	Jozef	k1gMnSc1	Jozef
Tiso	Tisa	k1gFnSc5	Tisa
<g/>
,	,	kIx,	,
propražský	propražský	k2eAgInSc4d1	propražský
směr	směr	k1gInSc4	směr
reprezentovali	reprezentovat	k5eAaImAgMnP	reprezentovat
zejména	zejména	k9	zejména
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
Martin	Martin	k1gMnSc1	Martin
Sokol	Sokol	k1gMnSc1	Sokol
a	a	k8xC	a
nitranský	nitranský	k2eAgMnSc1d1	nitranský
kanovník	kanovník	k1gMnSc1	kanovník
Jozef	Jozef	k1gMnSc1	Jozef
Buday	Budaa	k1gFnSc2	Budaa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
po	po	k7c6	po
Mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
dohodě	dohoda	k1gFnSc6	dohoda
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1938	[number]	k4	1938
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
v	v	k7c6	v
Žilině	Žilina	k1gFnSc6	Žilina
autonomii	autonomie	k1gFnSc6	autonomie
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
s	s	k7c7	s
HSĽS	HSĽS	kA	HSĽS
sloučila	sloučit	k5eAaPmAgFnS	sloučit
většina	většina	k1gFnSc1	většina
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
slovenských	slovenský	k2eAgFnPc2d1	slovenská
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
její	její	k3xOp3gInSc1	její
název	název	k1gInSc1	název
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c4	na
"	"	kIx"	"
<g/>
Hlinkova	Hlinkův	k2eAgFnSc1d1	Hlinkova
slovenská	slovenský	k2eAgFnSc1d1	slovenská
ľudová	ľudový	k2eAgFnSc1d1	ľudová
strana	strana	k1gFnSc1	strana
-	-	kIx~	-
Strana	strana	k1gFnSc1	strana
slovenskej	slovenskej	k?	slovenskej
národnej	národnat	k5eAaImRp2nS	národnat
jednoty	jednota	k1gFnSc2	jednota
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Snemu	Snem	k1gInSc2	Snem
Slovenskej	Slovenskej	k?	Slovenskej
krajiny	krajina	k1gFnSc2	krajina
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1938	[number]	k4	1938
získala	získat	k5eAaPmAgFnS	získat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
97	[number]	k4	97
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Strana	strana	k1gFnSc1	strana
byla	být	k5eAaImAgFnS	být
představitelkou	představitelka	k1gFnSc7	představitelka
režimu	režim	k1gInSc2	režim
Slovenského	slovenský	k2eAgInSc2d1	slovenský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
představovala	představovat	k5eAaImAgFnS	představovat
totalitní	totalitní	k2eAgFnSc4d1	totalitní
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
dob	doba	k1gFnPc2	doba
své	svůj	k3xOyFgFnSc2	svůj
diktatury	diktatura	k1gFnSc2	diktatura
zlikvidovala	zlikvidovat	k5eAaPmAgFnS	zlikvidovat
veškeré	veškerý	k3xTgInPc4	veškerý
nepohodlné	pohodlný	k2eNgInPc4d1	nepohodlný
spolky	spolek	k1gInPc4	spolek
a	a	k8xC	a
organizace	organizace	k1gFnPc4	organizace
<g/>
,	,	kIx,	,
pronásledovala	pronásledovat	k5eAaImAgFnS	pronásledovat
své	svůj	k3xOyFgMnPc4	svůj
odpůrce	odpůrce	k1gMnPc4	odpůrce
a	a	k8xC	a
národnostní	národnostní	k2eAgFnPc4d1	národnostní
menšiny	menšina	k1gFnPc4	menšina
a	a	k8xC	a
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
Třetí	třetí	k4xOgFnSc7	třetí
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
i	i	k8xC	i
vojenského	vojenský	k2eAgInSc2d1	vojenský
potenciálu	potenciál	k1gInSc2	potenciál
Slovenska	Slovensko	k1gNnSc2	Slovensko
byla	být	k5eAaImAgFnS	být
věnována	věnovat	k5eAaImNgFnS	věnovat
boji	boj	k1gInSc6	boj
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byly	být	k5eAaImAgInP	být
hospodářské	hospodářský	k2eAgInPc1d1	hospodářský
poměry	poměr	k1gInPc1	poměr
Slovenska	Slovensko	k1gNnSc2	Slovensko
za	za	k7c4	za
její	její	k3xOp3gFnPc4	její
vlády	vláda	k1gFnPc4	vláda
stabilizované	stabilizovaný	k2eAgFnPc4d1	stabilizovaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
SNP	SNP	kA	SNP
byla	být	k5eAaImAgFnS	být
Slovenskou	slovenský	k2eAgFnSc7d1	slovenská
národní	národní	k2eAgFnSc7d1	národní
radou	rada	k1gFnSc7	rada
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
na	na	k7c6	na
povstaleckém	povstalecký	k2eAgNnSc6d1	povstalecké
území	území	k1gNnSc6	území
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
zakázána	zakázat	k5eAaPmNgFnS	zakázat
na	na	k7c6	na
území	území	k1gNnSc6	území
obnoveného	obnovený	k2eAgNnSc2d1	obnovené
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
představitelů	představitel	k1gMnPc2	představitel
byla	být	k5eAaImAgFnS	být
odsouzena	odsouzet	k5eAaImNgFnS	odsouzet
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
emigrovali	emigrovat	k5eAaBmAgMnP	emigrovat
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Názvy	název	k1gInPc1	název
===	===	k?	===
</s>
</p>
<p>
<s>
1906	[number]	k4	1906
<g/>
–	–	k?	–
<g/>
1925	[number]	k4	1925
<g/>
:	:	kIx,	:
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
ľudová	ľudový	k2eAgFnSc1d1	ľudová
strana	strana	k1gFnSc1	strana
</s>
</p>
<p>
<s>
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
:	:	kIx,	:
Hlinkova	Hlinkův	k2eAgFnSc1d1	Hlinkova
slovenská	slovenský	k2eAgFnSc1d1	slovenská
ľudová	ľudový	k2eAgFnSc1d1	ľudová
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
HSĽS	HSĽS	kA	HSĽS
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
:	:	kIx,	:
Hlinkova	Hlinkův	k2eAgFnSc1d1	Hlinkova
slovenská	slovenský	k2eAgFnSc1d1	slovenská
ľudová	ľudový	k2eAgFnSc1d1	ľudová
strana	strana	k1gFnSc1	strana
–	–	k?	–
Strana	strana	k1gFnSc1	strana
slovenskej	slovenskej	k?	slovenskej
národnej	národnat	k5eAaPmRp2nS	národnat
jednoty	jednota	k1gFnSc2	jednota
(	(	kIx(	(
<g/>
HSĽS-SSNJ	HSĽS-SSNJ	k1gMnSc1	HSĽS-SSNJ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Hlinka	Hlinka	k1gMnSc1	Hlinka
</s>
</p>
<p>
<s>
Jozef	Jozef	k1gMnSc1	Jozef
Tiso	Tisa	k1gFnSc5	Tisa
</s>
</p>
<p>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
ľudová	ľudový	k2eAgFnSc1d1	ľudová
strana	strana	k1gFnSc1	strana
–	–	k?	–
současná	současný	k2eAgFnSc1d1	současná
strana	strana	k1gFnSc1	strana
navazující	navazující	k2eAgFnSc1d1	navazující
na	na	k7c4	na
tradici	tradice	k1gFnSc4	tradice
HSĽS	HSĽS	kA	HSĽS
</s>
</p>
