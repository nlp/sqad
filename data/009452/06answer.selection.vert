<s>
Benitoit	benitoit	k1gInSc1	benitoit
je	být	k5eAaImIp3nS	být
hydrotermálního	hydrotermální	k2eAgInSc2d1	hydrotermální
původu	původ	k1gInSc2	původ
a	a	k8xC	a
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
asociaci	asociace	k1gFnSc6	asociace
s	s	k7c7	s
natrolitem	natrolit	k1gInSc7	natrolit
<g/>
,	,	kIx,	,
neptunitem	neptunit	k1gInSc7	neptunit
a	a	k8xC	a
albitem	albit	k1gInSc7	albit
(	(	kIx(	(
<g/>
kterým	který	k3yIgInSc7	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
obalen	obalen	k2eAgMnSc1d1	obalen
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
louhovat	louhovat	k5eAaImF	louhovat
v	v	k7c6	v
HCl	HCl	k1gFnSc6	HCl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
kontaktech	kontakt	k1gInPc6	kontakt
serpentinitových	serpentinitový	k2eAgNnPc2d1	serpentinitový
těles	těleso	k1gNnPc2	těleso
a	a	k8xC	a
zelených	zelený	k2eAgFnPc2d1	zelená
břidlic	břidlice	k1gFnPc2	břidlice
<g/>
.	.	kIx.	.
</s>
