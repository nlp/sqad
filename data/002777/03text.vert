<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Brož	Brož	k1gMnSc1	Brož
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1978	[number]	k4	1978
Hostivice	Hostivice	k1gFnSc1	Hostivice
<g/>
)	)	kIx)	)
známý	známý	k1gMnSc1	známý
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
Vladimir	Vladimir	k1gMnSc1	Vladimir
518	[number]	k4	518
či	či	k8xC	či
X-Kmen	X-Kmen	k1gInSc1	X-Kmen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
rapper	rapper	k1gInSc1	rapper
vystupující	vystupující	k2eAgInSc1d1	vystupující
jak	jak	k6eAd1	jak
s	s	k7c7	s
uskupením	uskupení	k1gNnSc7	uskupení
Peneři	Pener	k1gMnPc1	Pener
strýčka	strýček	k1gMnSc2	strýček
Homeboye	Homeboy	k1gMnSc2	Homeboy
(	(	kIx(	(
<g/>
PSH	PSH	kA	PSH
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
i	i	k9	i
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
projekty	projekt	k1gInPc7	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vyučený	vyučený	k2eAgMnSc1d1	vyučený
umělecký	umělecký	k2eAgMnSc1d1	umělecký
kovář	kovář	k1gMnSc1	kovář
<g/>
.	.	kIx.	.
</s>
<s>
Věnuje	věnovat	k5eAaImIp3nS	věnovat
se	se	k3xPyFc4	se
též	též	k6eAd1	též
tvorbě	tvorba	k1gFnSc6	tvorba
komiksů	komiks	k1gInPc2	komiks
<g/>
,	,	kIx,	,
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
ilustrací	ilustrace	k1gFnPc2	ilustrace
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
osobností	osobnost	k1gFnSc7	osobnost
labelu	label	k1gInSc2	label
BiggBoss	BiggBoss	k1gInSc1	BiggBoss
<g/>
.	.	kIx.	.
</s>
<s>
Vladimir	Vladimir	k1gInSc1	Vladimir
518	[number]	k4	518
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejdéle	dlouho	k6eAd3	dlouho
působících	působící	k2eAgInPc2d1	působící
rapperů	rapper	k1gInPc2	rapper
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
hip	hip	k0	hip
hopové	hopová	k1gFnSc6	hopová
scéně	scéna	k1gFnSc6	scéna
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
PSH	PSH	kA	PSH
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
labelu	label	k1gInSc2	label
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
BiggBoss	BiggBossa	k1gFnPc2	BiggBossa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
i	i	k8xC	i
jako	jako	k9	jako
výrazná	výrazný	k2eAgFnSc1d1	výrazná
osobnost	osobnost	k1gFnSc1	osobnost
graffiti	graffiti	k1gNnSc2	graffiti
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
jako	jako	k9	jako
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
<g/>
,	,	kIx,	,
komiksový	komiksový	k2eAgMnSc1d1	komiksový
výtvarník	výtvarník	k1gMnSc1	výtvarník
či	či	k8xC	či
autor	autor	k1gMnSc1	autor
scénografií	scénografie	k1gFnSc7	scénografie
pro	pro	k7c4	pro
experimentální	experimentální	k2eAgFnSc4d1	experimentální
divadelní	divadelní	k2eAgFnSc4d1	divadelní
skupinu	skupina	k1gFnSc4	skupina
TOW	TOW	kA	TOW
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
po	po	k7c4	po
sérii	série	k1gFnSc4	série
volných	volný	k2eAgFnPc2d1	volná
improvizací	improvizace	k1gFnPc2	improvizace
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
postupně	postupně	k6eAd1	postupně
představení	představení	k1gNnSc4	představení
Noční	noční	k2eAgFnSc1d1	noční
můra	můra	k1gFnSc1	můra
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Turing	Turing	k1gInSc1	Turing
Machine	Machin	k1gInSc5	Machin
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
a	a	k8xC	a
Teorie	teorie	k1gFnSc1	teorie
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Davidem	David	k1gMnSc7	David
Vrbíkem	Vrbík	k1gMnSc7	Vrbík
a	a	k8xC	a
Ondřejem	Ondřej	k1gMnSc7	Ondřej
Anděrou	Anděra	k1gFnSc7	Anděra
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
WWW	WWW	kA	WWW
tvoří	tvořit	k5eAaImIp3nS	tvořit
audiovizuální	audiovizuální	k2eAgInSc4d1	audiovizuální
projekt	projekt	k1gInSc4	projekt
SPAM	SPAM	kA	SPAM
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
má	mít	k5eAaImIp3nS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
několik	několik	k4yIc4	několik
multižánrových	multižánrův	k2eAgNnPc2d1	multižánrův
představení	představení	k1gNnPc2	představení
<g/>
,	,	kIx,	,
za	za	k7c4	za
všechny	všechen	k3xTgFnPc4	všechen
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jmenovat	jmenovat	k5eAaImF	jmenovat
například	například	k6eAd1	například
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
Prager	Prager	k1gMnSc1	Prager
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BIT	bit	k1gInSc1	bit
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
vytvořený	vytvořený	k2eAgMnSc1d1	vytvořený
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
fasádu	fasáda	k1gFnSc4	fasáda
Nové	Nové	k2eAgFnSc2d1	Nové
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
Intepretace	Intepretace	k1gFnSc2	Intepretace
černobílých	černobílý	k2eAgFnPc2d1	černobílá
struktur	struktura	k1gFnPc2	struktura
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Sýkory	Sýkora	k1gMnSc2	Sýkora
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
či	či	k8xC	či
SMRT	smrt	k1gFnSc1	smrt
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
spoluautor	spoluautor	k1gMnSc1	spoluautor
je	být	k5eAaImIp3nS	být
podepsán	podepsat	k5eAaPmNgInS	podepsat
pod	pod	k7c7	pod
knihami	kniha	k1gFnPc7	kniha
2666	[number]	k4	2666
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
Odyssey	Odyssea	k1gFnSc2	Odyssea
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kmeny	kmen	k1gInPc1	kmen
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Město	město	k1gNnSc1	město
<g/>
=	=	kIx~	=
<g/>
Médium	médium	k1gNnSc1	médium
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kmeny	kmen	k1gInPc1	kmen
0	[number]	k4	0
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2014	[number]	k4	2014
a	a	k8xC	a
2015	[number]	k4	2015
pracoval	pracovat	k5eAaImAgInS	pracovat
na	na	k7c4	na
převedení	převedení	k1gNnSc4	převedení
knihy	kniha	k1gFnSc2	kniha
Kmeny	kmen	k1gInPc4	kmen
do	do	k7c2	do
šestnáctidílné	šestnáctidílný	k2eAgFnSc2d1	šestnáctidílná
dokumentární	dokumentární	k2eAgFnSc2d1	dokumentární
série	série	k1gFnSc2	série
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
televizi	televize	k1gFnSc4	televize
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
PSH	PSH	kA	PSH
vydal	vydat	k5eAaPmAgInS	vydat
tři	tři	k4xCgFnPc4	tři
dlouhohrající	dlouhohrající	k2eAgFnPc4d1	dlouhohrající
desky	deska	k1gFnPc4	deska
Repertoár	repertoár	k1gInSc1	repertoár
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rap	rap	k1gMnSc1	rap
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
Roll	Rollum	k1gNnPc2	Rollum
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
a	a	k8xC	a
Epilog	epilog	k1gInSc1	epilog
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
sólový	sólový	k2eAgInSc1d1	sólový
intepret	intepret	k1gInSc1	intepret
má	mít	k5eAaImIp3nS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
CD	CD	kA	CD
mixtape	mixtapat	k5eAaPmIp3nS	mixtapat
Flashback	Flashback	k1gInSc1	Flashback
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvojici	dvojice	k1gFnSc4	dvojice
dlouhohrajících	dlouhohrající	k2eAgFnPc2d1	dlouhohrající
desek	deska	k1gFnPc2	deska
Gorila	gorila	k1gFnSc1	gorila
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Architekt	architekt	k1gMnSc1	architekt
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
a	a	k8xC	a
Idiot	idiot	k1gMnSc1	idiot
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
obě	dva	k4xCgFnPc4	dva
obdržel	obdržet	k5eAaPmAgMnS	obdržet
ceny	cena	k1gFnPc1	cena
Anděl	Anděla	k1gFnPc2	Anděla
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Hip	hip	k0	hip
Hop	hop	k0	hop
a	a	k8xC	a
R	R	kA	R
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
B.	B.	kA	B.
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
hudební	hudební	k2eAgFnSc6d1	hudební
kariéře	kariéra	k1gFnSc6	kariéra
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
širokou	široký	k2eAgFnSc7d1	široká
škálou	škála	k1gFnSc7	škála
intepretů	intepret	k1gInPc2	intepret
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gFnPc4	on
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
DJ	DJ	kA	DJ
Wich	Wicha	k1gFnPc2	Wicha
<g/>
,	,	kIx,	,
Monkey	Monkea	k1gFnPc1	Monkea
Business	business	k1gInSc1	business
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Kocáb	Kocáb	k1gMnSc1	Kocáb
<g/>
,	,	kIx,	,
Rytmus	rytmus	k1gInSc1	rytmus
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Korn	Korn	k1gMnSc1	Korn
<g/>
,	,	kIx,	,
Tereza	Tereza	k1gFnSc1	Tereza
Černochová	Černochová	k1gFnSc1	Černochová
<g/>
,	,	kIx,	,
J.A.	J.A.	k1gFnSc1	J.A.
<g/>
R.	R.	kA	R.
<g/>
,	,	kIx,	,
Tata	tata	k1gMnSc1	tata
bojs	bojs	k1gInSc1	bojs
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Davidem	David	k1gMnSc7	David
Böhmem	Böhm	k1gMnSc7	Böhm
<g/>
,	,	kIx,	,
Jiřím	Jiří	k1gMnSc7	Jiří
Frantou	Franta	k1gMnSc7	Franta
a	a	k8xC	a
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Plachým	Plachý	k1gMnSc7	Plachý
kreslí	kreslit	k5eAaImIp3nS	kreslit
a	a	k8xC	a
vydává	vydávat	k5eAaImIp3nS	vydávat
nezávislé	závislý	k2eNgFnPc4d1	nezávislá
komiksové	komiksový	k2eAgFnPc4d1	komiksová
noviny	novina	k1gFnPc4	novina
KIX	KIX	kA	KIX
<g/>
.	.	kIx.	.
</s>
<s>
Letos	letos	k6eAd1	letos
hodlá	hodlat	k5eAaImIp3nS	hodlat
dále	daleko	k6eAd2	daleko
vydat	vydat	k5eAaPmF	vydat
desku	deska	k1gFnSc4	deska
s	s	k7c7	s
Hugo	Hugo	k1gMnSc1	Hugo
Toxxxem	Toxxx	k1gInSc7	Toxxx
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
společné	společný	k2eAgInPc4d1	společný
LP	LP	kA	LP
s	s	k7c7	s
labelem	label	k1gInSc7	label
BiggBoss	BiggBossa	k1gFnPc2	BiggBossa
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
další	další	k2eAgNnSc1d1	další
sólo	sólo	k2eAgNnSc1d1	sólo
album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
ho	on	k3xPp3gNnSc4	on
nemine	minout	k5eNaImIp3nS	minout
spolupráce	spolupráce	k1gFnSc1	spolupráce
na	na	k7c6	na
několika	několik	k4yIc6	několik
knižních	knižní	k2eAgInPc6d1	knižní
titulech	titul	k1gInPc6	titul
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
BiggBoss	BiggBossa	k1gFnPc2	BiggBossa
Books	Booksa	k1gFnPc2	Booksa
<g/>
.	.	kIx.	.
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
PSH	PSH	kA	PSH
-	-	kIx~	-
Repertoár	repertoár	k1gInSc1	repertoár
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
PSH	PSH	kA	PSH
-	-	kIx~	-
Rap	rap	k1gMnSc1	rap
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
Roll	Roll	k1gInSc1	Roll
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
PSH	PSH	kA	PSH
-	-	kIx~	-
Epilog	epilog	k1gInSc1	epilog
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Gorila	gorila	k1gFnSc1	gorila
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Architekt	architekt	k1gMnSc1	architekt
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Flashback	Flashback	k1gInSc1	Flashback
(	(	kIx(	(
<g/>
mixtape	mixtapat	k5eAaPmIp3nS	mixtapat
CD	CD	kA	CD
<g/>
)	)	kIx)	)
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
Idiot	idiot	k1gMnSc1	idiot
Na	na	k7c4	na
album	album	k1gNnSc4	album
coverů	cover	k1gInPc2	cover
Davida	David	k1gMnSc2	David
Kollera	Koller	k1gMnSc2	Koller
David	David	k1gMnSc1	David
Koller	Koller	k1gMnSc1	Koller
&	&	k?	&
Friends	Friends	k1gInSc1	Friends
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
nahrál	nahrát	k5eAaBmAgInS	nahrát
píseň	píseň	k1gFnSc4	píseň
Chci	chtít	k5eAaImIp1nS	chtít
zas	zas	k6eAd1	zas
v	v	k7c6	v
tobě	ty	k3xPp2nSc6	ty
spát	spát	k5eAaImF	spát
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Vladimir	Vladimir	k1gMnSc1	Vladimir
518	[number]	k4	518
Vladimir	Vladimir	k1gInSc1	Vladimir
518	[number]	k4	518
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
Rozhovor	rozhovor	k1gInSc1	rozhovor
<g/>
;	;	kIx,	;
VLADIMÍR	Vladimír	k1gMnSc1	Vladimír
518	[number]	k4	518
<g/>
:	:	kIx,	:
Nic	nic	k3yNnSc1	nic
bezbarvého	bezbarvý	k2eAgNnSc2d1	bezbarvé
<g/>
.	.	kIx.	.
</s>
<s>
Miluj	milovat	k5eAaImRp2nS	milovat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nenáviď	nenávidět	k5eAaImRp2nS	nenávidět
<g/>
.	.	kIx.	.
</s>
<s>
Recenze	recenze	k1gFnSc1	recenze
CD	CD	kA	CD
<g/>
:	:	kIx,	:
Gorila	gorila	k1gFnSc1	gorila
a	a	k8xC	a
architekt	architekt	k1gMnSc1	architekt
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
rappera	rappero	k1gNnSc2	rappero
Vladimirovo	Vladimirův	k2eAgNnSc4d1	Vladimirův
album	album	k1gNnSc4	album
IDIOT	idiot	k1gMnSc1	idiot
na	na	k7c4	na
SAAMPL	SAAMPL	kA	SAAMPL
</s>
