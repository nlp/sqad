<p>
<s>
Sekvenční	sekvenční	k2eAgInSc1d1	sekvenční
přístup	přístup	k1gInSc1	přístup
v	v	k7c6	v
matematické	matematický	k2eAgFnSc6d1	matematická
informatice	informatika	k1gFnSc6	informatika
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sada	sada	k1gFnSc1	sada
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
pole	pole	k1gNnSc1	pole
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
nebo	nebo	k8xC	nebo
soubor	soubor	k1gInSc4	soubor
na	na	k7c6	na
disku	disk	k1gInSc6	disk
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
v	v	k7c6	v
předem	předem	k6eAd1	předem
určeném	určený	k2eAgNnSc6d1	určené
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
datové	datový	k2eAgFnPc1d1	datová
struktury	struktura	k1gFnPc1	struktura
nebo	nebo	k8xC	nebo
paměťová	paměťový	k2eAgNnPc1d1	paměťové
zařízení	zařízení	k1gNnPc1	zařízení
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
jenom	jenom	k9	jenom
sekvenční	sekvenční	k2eAgInSc4d1	sekvenční
přístup	přístup	k1gInSc4	přístup
<g/>
;	;	kIx,	;
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
jednosměrný	jednosměrný	k2eAgInSc1d1	jednosměrný
spojový	spojový	k2eAgInSc1d1	spojový
seznam	seznam	k1gInSc1	seznam
nebo	nebo	k8xC	nebo
soubor	soubor	k1gInSc1	soubor
na	na	k7c6	na
magnetické	magnetický	k2eAgFnSc6d1	magnetická
pásce	páska	k1gFnSc6	páska
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
datových	datový	k2eAgFnPc2d1	datová
struktur	struktura	k1gFnPc2	struktura
je	být	k5eAaImIp3nS	být
sekvenční	sekvenční	k2eAgInSc4d1	sekvenční
přístup	přístup	k1gInSc4	přístup
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
přístupových	přístupový	k2eAgFnPc2d1	přístupová
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zpracování	zpracování	k1gNnSc1	zpracování
posloupnosti	posloupnost	k1gFnSc2	posloupnost
datových	datový	k2eAgInPc2d1	datový
prvků	prvek	k1gInPc2	prvek
po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
postačuje	postačovat	k5eAaImIp3nS	postačovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
datové	datový	k2eAgFnSc6d1	datová
struktuře	struktura	k1gFnSc6	struktura
řekneme	říct	k5eAaPmIp1nP	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
sekvenční	sekvenční	k2eAgInSc4d1	sekvenční
přístup	přístup	k1gInSc4	přístup
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
lze	lze	k6eAd1	lze
v	v	k7c6	v
ní	on	k3xPp3gFnSc2	on
obsažené	obsažený	k2eAgFnSc2d1	obsažená
hodnoty	hodnota	k1gFnSc2	hodnota
projít	projít	k5eAaPmF	projít
všechny	všechen	k3xTgFnPc4	všechen
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
spojový	spojový	k2eAgInSc1d1	spojový
seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
struktury	struktura	k1gFnPc1	struktura
používající	používající	k2eAgFnPc1d1	používající
hašování	hašování	k1gNnSc4	hašování
obvykle	obvykle	k6eAd1	obvykle
sekvenční	sekvenční	k2eAgInSc1d1	sekvenční
přístup	přístup	k1gInSc1	přístup
neumožňují	umožňovat	k5eNaImIp3nP	umožňovat
<g/>
.	.	kIx.	.
</s>
<s>
Algoritmy	algoritmus	k1gInPc1	algoritmus
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
nestačí	stačit	k5eNaBmIp3nS	stačit
sekvenční	sekvenční	k2eAgInSc1d1	sekvenční
přístup	přístup	k1gInSc1	přístup
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
k	k	k7c3	k
urychlení	urychlení	k1gNnSc3	urychlení
přístupu	přístup	k1gInSc2	přístup
vytvořit	vytvořit	k5eAaPmF	vytvořit
index	index	k1gInSc4	index
<g/>
.	.	kIx.	.
</s>
<s>
Indexování	indexování	k1gNnSc1	indexování
seznamu	seznam	k1gInSc2	seznam
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
lze	lze	k6eAd1	lze
přistupovat	přistupovat	k5eAaImF	přistupovat
pouze	pouze	k6eAd1	pouze
sekvenčně	sekvenčně	k6eAd1	sekvenčně
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
asymptotickou	asymptotický	k2eAgFnSc4d1	asymptotická
složitost	složitost	k1gFnSc4	složitost
O	O	kA	O
<g/>
(	(	kIx(	(
<g/>
k	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
k	k	k7c3	k
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
prvků	prvek	k1gInPc2	prvek
seznamu	seznam	k1gInSc2	seznam
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
algoritmy	algoritmus	k1gInPc1	algoritmus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
přímém	přímý	k2eAgInSc6d1	přímý
přístupu	přístup	k1gInSc6	přístup
<g/>
,	,	kIx,	,
degenerují	degenerovat	k5eAaBmIp3nP	degenerovat
na	na	k7c4	na
pomalé	pomalý	k2eAgInPc4d1	pomalý
algoritmy	algoritmus	k1gInPc4	algoritmus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
méně	málo	k6eAd2	málo
efektivní	efektivní	k2eAgFnSc4d1	efektivní
než	než	k8xS	než
jejich	jejich	k3xOp3gFnPc1	jejich
naivní	naivní	k2eAgFnPc1d1	naivní
alternativy	alternativa	k1gFnPc1	alternativa
<g/>
;	;	kIx,	;
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
quicksort	quicksort	k1gInSc4	quicksort
a	a	k8xC	a
binární	binární	k2eAgNnSc4d1	binární
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
existují	existovat	k5eAaImIp3nP	existovat
algoritmy	algoritmus	k1gInPc1	algoritmus
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
efektivitu	efektivita	k1gFnSc4	efektivita
omezení	omezení	k1gNnSc3	omezení
na	na	k7c4	na
sekvenční	sekvenční	k2eAgInSc4d1	sekvenční
přístup	přístup	k1gInSc4	přístup
nesníží	snížit	k5eNaPmIp3nP	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
mergesort	mergesort	k1gInSc1	mergesort
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
poněkud	poněkud	k6eAd1	poněkud
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
chceme	chtít	k5eAaImIp1nP	chtít
projít	projít	k5eAaPmF	projít
všechny	všechen	k3xTgInPc4	všechen
prvky	prvek	k1gInPc4	prvek
a	a	k8xC	a
nezáleží	záležet	k5eNaImIp3nS	záležet
nám	my	k3xPp1nPc3	my
na	na	k7c4	na
pořadí	pořadí	k1gNnSc4	pořadí
(	(	kIx(	(
<g/>
v	v	k7c6	v
pseudokódu	pseudokód	k1gInSc6	pseudokód
for	forum	k1gNnPc2	forum
each	each	k1gInSc1	each
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
chceme	chtít	k5eAaImIp1nP	chtít
prvky	prvek	k1gInPc4	prvek
projít	projít	k5eAaPmF	projít
v	v	k7c6	v
určeném	určený	k2eAgNnSc6d1	určené
pořadí	pořadí	k1gNnSc6	pořadí
nebo	nebo	k8xC	nebo
systém	systém	k1gInSc1	systém
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
průchod	průchod	k1gInSc4	průchod
daty	datum	k1gNnPc7	datum
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
nějakém	nějaký	k3yIgNnSc6	nějaký
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
první	první	k4xOgFnSc7	první
dokážeme	dokázat	k5eAaPmIp1nP	dokázat
i	i	k9	i
v	v	k7c6	v
hašovací	hašovací	k2eAgFnSc6d1	hašovací
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
druhé	druhý	k4xOgNnSc1	druhý
je	být	k5eAaImIp3nS	být
těžší	těžký	k2eAgInPc4d2	těžší
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
běžná	běžný	k2eAgFnSc1d1	běžná
9	[number]	k4	9
z	z	k7c2	z
10	[number]	k4	10
databází	databáze	k1gFnPc2	databáze
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
výše	vysoce	k6eAd2	vysoce
popsaný	popsaný	k2eAgInSc1d1	popsaný
index	index	k1gInSc1	index
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
listové	listový	k2eAgFnPc1d1	listová
stránky	stránka	k1gFnPc1	stránka
propojeny	propojen	k2eAgFnPc1d1	propojena
pointry	pointr	k1gInPc4	pointr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
umožní	umožnit	k5eAaPmIp3nP	umožnit
rychlý	rychlý	k2eAgInSc4d1	rychlý
sekvenční	sekvenční	k2eAgInSc4d1	sekvenční
průchod	průchod	k1gInSc4	průchod
v	v	k7c6	v
požadovaném	požadovaný	k2eAgNnSc6d1	požadované
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
abychom	aby	kYmCp1nP	aby
se	se	k3xPyFc4	se
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
už	už	k6eAd1	už
vůbec	vůbec	k9	vůbec
nemuseli	muset	k5eNaImAgMnP	muset
starat	starat	k5eAaImF	starat
<g/>
,	,	kIx,	,
necháme	nechat	k5eAaPmIp1nP	nechat
to	ten	k3xDgNnSc4	ten
na	na	k7c4	na
na	na	k7c4	na
návrhový	návrhový	k2eAgInSc4d1	návrhový
vzor	vzor	k1gInSc4	vzor
iterátor	iterátor	k1gMnSc1	iterátor
nebo	nebo	k8xC	nebo
něco	něco	k3yInSc1	něco
s	s	k7c7	s
podobnou	podobný	k2eAgFnSc7d1	podobná
funkcionalitou	funkcionalita	k1gFnSc7	funkcionalita
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
třetí	třetí	k4xOgMnSc1	třetí
je	být	k5eAaImIp3nS	být
obvyklý	obvyklý	k2eAgMnSc1d1	obvyklý
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
původní	původní	k2eAgInSc4d1	původní
význam	význam	k1gInSc4	význam
pojmu	pojem	k1gInSc2	pojem
sekvenční	sekvenční	k2eAgInSc4d1	sekvenční
přístup	přístup	k1gInSc4	přístup
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
důsledky	důsledek	k1gInPc7	důsledek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Přímý	přímý	k2eAgInSc1d1	přímý
přístup	přístup	k1gInSc1	přístup
</s>
</p>
<p>
<s>
Paměťová	paměťový	k2eAgNnPc1d1	paměťové
zařízení	zařízení	k1gNnPc1	zařízení
s	s	k7c7	s
přímým	přímý	k2eAgInSc7d1	přímý
přístupem	přístup	k1gInSc7	přístup
</s>
</p>
<p>
<s>
Frontová	frontový	k2eAgFnSc1d1	frontová
sekvenční	sekvenční	k2eAgFnSc1d1	sekvenční
přístupová	přístupový	k2eAgFnSc1d1	přístupová
metoda	metoda	k1gFnSc1	metoda
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Sequential	Sequential	k1gInSc1	Sequential
access	access	k1gInSc1	access
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
