<s>
Sekvenční	sekvenční	k2eAgInSc1d1	sekvenční
přístup	přístup	k1gInSc1	přístup
v	v	k7c6	v
matematické	matematický	k2eAgFnSc6d1	matematická
informatice	informatika	k1gFnSc6	informatika
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sada	sada	k1gFnSc1	sada
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
pole	pole	k1gNnSc1	pole
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
nebo	nebo	k8xC	nebo
soubor	soubor	k1gInSc4	soubor
na	na	k7c6	na
disku	disk	k1gInSc6	disk
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
v	v	k7c6	v
předem	předem	k6eAd1	předem
určeném	určený	k2eAgNnSc6d1	určené
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
