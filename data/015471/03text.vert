<s>
DVD	DVD	kA
</s>
<s>
Logo	logo	k1gNnSc1
DVD	DVD	kA
</s>
<s>
Datová	datový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
DVD-R	DVD-R	k1gFnSc2
</s>
<s>
DVD	DVD	kA
(	(	kIx(
<g/>
neoficiálně	neoficiálně	k6eAd1,k6eNd1
Digital	Digital	kA
Versatile	Versatil	k1gMnSc5
Disc	disco	k1gNnPc2
nebo	nebo	k8xC
Digital	Digital	kA
Video	video	k1gNnSc1
Disc	disco	k1gNnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
formát	formát	k1gInSc4
digitálního	digitální	k2eAgInSc2d1
optického	optický	k2eAgInSc2d1
datového	datový	k2eAgInSc2d1
nosiče	nosič	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
může	moct	k5eAaImIp3nS
obsahovat	obsahovat	k5eAaImF
filmy	film	k1gInPc4
ve	v	k7c6
vysoké	vysoký	k2eAgFnSc6d1
obrazové	obrazový	k2eAgFnSc6d1
a	a	k8xC
zvukové	zvukový	k2eAgFnSc6d1
kvalitě	kvalita	k1gFnSc6
nebo	nebo	k8xC
jiná	jiný	k2eAgNnPc4d1
data	datum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
vývoji	vývoj	k1gInSc6
DVD	DVD	kA
byl	být	k5eAaImAgInS
kladen	klást	k5eAaImNgInS
důraz	důraz	k1gInSc1
na	na	k7c4
zpětnou	zpětný	k2eAgFnSc4d1
kompatibilitu	kompatibilita	k1gFnSc4
s	s	k7c7
CD	CD	kA
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
DVD	DVD	kA
disk	disk	k1gInSc4
velmi	velmi	k6eAd1
podobá	podobat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
DVD	DVD	kA
</s>
<s>
DVD	DVD	kA
bylo	být	k5eAaImAgNnS
uvedeno	uvést	k5eAaPmNgNnS
na	na	k7c4
trh	trh	k1gInSc4
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
roku	rok	k1gInSc2
1996	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
zbytku	zbytek	k1gInSc6
světa	svět	k1gInSc2
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiální	oficiální	k2eAgInSc4d1
standard	standard	k1gInSc4
zapisovatelných	zapisovatelný	k2eAgInPc2d1
<g/>
/	/	kIx~
<g/>
přepisovatelných	přepisovatelný	k2eAgInPc2d1
disků	disk	k1gInPc2
DVD-R	DVD-R	k1gMnSc1
<g/>
(	(	kIx(
<g/>
W	W	kA
<g/>
)	)	kIx)
vytvořilo	vytvořit	k5eAaPmAgNnS
DVD	DVD	kA
Fórum	fórum	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
v	v	k7c6
dubnu	duben	k1gInSc6
roku	rok	k1gInSc2
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ceny	cena	k1gFnPc4
licencí	licence	k1gFnSc7
na	na	k7c4
tuto	tento	k3xDgFnSc4
technologii	technologie	k1gFnSc4
však	však	k9
byly	být	k5eAaImAgFnP
tak	tak	k9
vysoké	vysoký	k2eAgFnPc1d1
<g/>
,	,	kIx,
že	že	k8xS
vznikla	vzniknout	k5eAaPmAgFnS
jiná	jiný	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
–	–	k?
DVD	DVD	kA
<g/>
+	+	kIx~
<g/>
RW	RW	kA
Alliance	Alliance	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vytvořila	vytvořit	k5eAaPmAgFnS
standard	standard	k1gInSc4
DVD	DVD	kA
<g/>
+	+	kIx~
<g/>
R	R	kA
<g/>
(	(	kIx(
<g/>
W	W	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnPc1
licence	licence	k1gFnPc1
byly	být	k5eAaImAgFnP
levnější	levný	k2eAgFnPc1d2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
výkladu	výklad	k1gInSc2
názvu	název	k1gInSc2
DVD	DVD	kA
</s>
<s>
Před	před	k7c7
dokončením	dokončení	k1gNnSc7
specifikace	specifikace	k1gFnSc2
DVD	DVD	kA
byl	být	k5eAaImAgMnS
návrháři	návrhář	k1gMnSc3
neoficiálně	neoficiálně	k6eAd1,k6eNd1
používán	používán	k2eAgInSc4d1
název	název	k1gInSc4
Digital	Digital	kA
Video	video	k1gNnSc1
Disc	disco	k1gNnPc2
(	(	kIx(
<g/>
česky	česky	k6eAd1
digitální	digitální	k2eAgInSc1d1
videodisk	videodisk	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
byla	být	k5eAaImAgFnS
dokončována	dokončován	k2eAgFnSc1d1
specifikace	specifikace	k1gFnSc1
formátu	formát	k1gInSc2
<g/>
,	,	kIx,
však	však	k9
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
důvodu	důvod	k1gInSc2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
daleko	daleko	k6eAd1
širších	široký	k2eAgFnPc2d2
možností	možnost	k1gFnPc2
využití	využití	k1gNnSc2
bude	být	k5eAaImBp3nS
oficiálně	oficiálně	k6eAd1
znít	znít	k5eAaImF
Digital	Digital	kA
Versatile	Versatil	k1gMnSc5
Disc	disco	k1gNnPc2
(	(	kIx(
<g/>
česky	česky	k6eAd1
digitální	digitální	k2eAgInSc4d1
víceúčelový	víceúčelový	k2eAgInSc4d1
disk	disk	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hollywood	Hollywood	k1gInSc1
při	při	k7c6
distribuci	distribuce	k1gFnSc6
a	a	k8xC
propagaci	propagace	k1gFnSc4
svých	svůj	k3xOyFgInPc2
filmů	film	k1gInPc2
na	na	k7c6
DVD	DVD	kA
používá	používat	k5eAaImIp3nS
původní	původní	k2eAgInSc1d1
neoficiální	oficiální	k2eNgInSc1d1,k2eAgInSc1d1
název	název	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
rozšířenější	rozšířený	k2eAgNnSc1d2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
není	být	k5eNaImIp3nS
DVD	DVD	kA
žádnou	žádný	k3yNgFnSc7
zkratkou	zkratka	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
oficiálním	oficiální	k2eAgInSc7d1
názvem	název	k1gInSc7
média	médium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
v	v	k7c6
angličtině	angličtina	k1gFnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
v	v	k7c6
češtině	čeština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Technické	technický	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
Média	médium	k1gNnPc1
DVD	DVD	kA
jsou	být	k5eAaImIp3nP
plastové	plastový	k2eAgInPc1d1
disky	disk	k1gInPc1
<g/>
,	,	kIx,
navenek	navenek	k6eAd1
stejná	stejný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
média	médium	k1gNnPc4
CD	CD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Disky	disk	k1gInPc1
DVD	DVD	kA
mají	mít	k5eAaImIp3nP
průměr	průměr	k1gInSc1
120	#num#	k4
mm	mm	kA
a	a	k8xC
jsou	být	k5eAaImIp3nP
1,2	1,2	k4
mm	mm	kA
silná	silný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Data	datum	k1gNnPc1
se	se	k3xPyFc4
ukládají	ukládat	k5eAaImIp3nP
pod	pod	k7c4
povrch	povrch	k1gInSc4
do	do	k7c2
jedné	jeden	k4xCgFnSc2
nebo	nebo	k8xC
dvou	dva	k4xCgFnPc2
vrstev	vrstva	k1gFnPc2
ve	v	k7c6
stopě	stopa	k1gFnSc6
tvaru	tvar	k1gInSc2
spirály	spirála	k1gFnSc2
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
CD	CD	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
čtení	čtení	k1gNnSc4
dat	datum	k1gNnPc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
laserové	laserový	k2eAgNnSc1d1
světlo	světlo	k1gNnSc1
s	s	k7c7
vlnovou	vlnový	k2eAgFnSc7d1
délkou	délka	k1gFnSc7
660	#num#	k4
nm	nm	k?
<g/>
,	,	kIx,
tedy	tedy	k9
kratší	krátký	k2eAgInPc1d2
než	než	k8xS
v	v	k7c6
případě	případ	k1gInSc6
CD	CD	kA
<g/>
;	;	kIx,
to	ten	k3xDgNnSc1
také	také	k9
umožňuje	umožňovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
vyšší	vysoký	k2eAgFnSc4d2
kapacitu	kapacita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
tak	tak	k6eAd1
příčný	příčný	k2eAgInSc1d1
odstup	odstup	k1gInSc1
stop	stopa	k1gFnPc2
je	být	k5eAaImIp3nS
menší	malý	k2eAgFnSc1d2
–	–	k?
0,74	0,74	k4
μ	μ	k6eAd1
oproti	oproti	k7c3
1,6	1,6	k4
μ	μ	k1gInSc1
u	u	k7c2
CD	CD	kA
<g/>
.	.	kIx.
</s>
<s>
DVD	DVD	kA
oproti	oproti	k7c3
CD	CD	kA
poskytuje	poskytovat	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
efektivnější	efektivní	k2eAgFnSc4d2
korekci	korekce	k1gFnSc4
chyb	chyba	k1gFnPc2
</s>
<s>
vyšší	vysoký	k2eAgFnSc4d2
kapacitu	kapacita	k1gFnSc4
záznamu	záznam	k1gInSc2
(	(	kIx(
<g/>
asi	asi	k9
4,7	4,7	k4
GB	GB	kA
<g/>
/	/	kIx~
<g/>
4,4	4,4	k4
GiB	GiB	k1gFnPc2
oproti	oproti	k7c3
0,7	0,7	k4
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
odlišný	odlišný	k2eAgInSc1d1
souborový	souborový	k2eAgInSc1d1
systém	systém	k1gInSc1
Universal	Universal	k1gFnSc2
Disk	disk	k1gInSc1
Format	Format	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
není	být	k5eNaImIp3nS
zpětně	zpětně	k6eAd1
kompatibilní	kompatibilní	k2eAgInSc1d1
s	s	k7c7
ISO	ISO	kA
9660	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
na	na	k7c4
CD-ROM	CD-ROM	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Rychlost	rychlost	k1gFnSc1
mechaniky	mechanika	k1gFnSc2
typu	typ	k1gInSc2
DVD	DVD	kA
se	se	k3xPyFc4
udává	udávat	k5eAaImIp3nS
jako	jako	k9
násobek	násobek	k1gInSc4
1350	#num#	k4
kB	kb	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
mechanika	mechanika	k1gFnSc1
s	s	k7c7
rychlostí	rychlost	k1gFnSc7
16	#num#	k4
<g/>
×	×	k?
umožňuje	umožňovat	k5eAaImIp3nS
přenosovou	přenosový	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
16	#num#	k4
×	×	k?
1350	#num#	k4
=	=	kIx~
21600	#num#	k4
kB	kb	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
(	(	kIx(
<g/>
nebo	nebo	k8xC
21,09	21,09	k4
MB	MB	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Médium	médium	k1gNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
zápis	zápis	k1gInSc4
na	na	k7c4
jednu	jeden	k4xCgFnSc4
nebo	nebo	k8xC
obě	dva	k4xCgFnPc1
dvě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
v	v	k7c6
jedné	jeden	k4xCgFnSc6
nebo	nebo	k8xC
dvou	dva	k4xCgMnPc6
vrstvách	vrstva	k1gFnPc6
na	na	k7c4
každou	každý	k3xTgFnSc4
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počtu	počet	k1gInSc6
stran	strana	k1gFnPc2
a	a	k8xC
vrstev	vrstva	k1gFnPc2
závisí	záviset	k5eAaImIp3nS
kapacita	kapacita	k1gFnSc1
média	médium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
DVD-	DVD-	k?
<g/>
5	#num#	k4
<g/>
:	:	kIx,
jedna	jeden	k4xCgFnSc1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
vrstva	vrstva	k1gFnSc1
<g/>
,	,	kIx,
kapacita	kapacita	k1gFnSc1
4,7	4,7	k4
GB	GB	kA
(	(	kIx(
<g/>
4,37	4,37	k4
GiB	GiB	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
DVD-	DVD-	k?
<g/>
9	#num#	k4
<g/>
:	:	kIx,
jedna	jeden	k4xCgFnSc1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
dvě	dva	k4xCgFnPc4
vrstvy	vrstva	k1gFnPc4
<g/>
,	,	kIx,
8,5	8,5	k4
GB	GB	kA
(	(	kIx(
<g/>
7,92	7,92	k4
GiB	GiB	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
DVD-	DVD-	k?
<g/>
10	#num#	k4
<g/>
:	:	kIx,
dvě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
vrstva	vrstva	k1gFnSc1
na	na	k7c6
každé	každý	k3xTgFnSc6
straně	strana	k1gFnSc6
<g/>
,	,	kIx,
9,4	9,4	k4
GB	GB	kA
(	(	kIx(
<g/>
8,75	8,75	k4
GiB	GiB	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
DVD-	DVD-	k?
<g/>
14	#num#	k4
<g/>
:	:	kIx,
dvě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
dvě	dva	k4xCgFnPc1
vrstvy	vrstva	k1gFnPc1
na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
vrstva	vrstva	k1gFnSc1
na	na	k7c6
druhé	druhý	k4xOgFnSc6
<g/>
,	,	kIx,
13,2	13,2	k4
GB	GB	kA
(	(	kIx(
<g/>
12,3	12,3	k4
GiB	GiB	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
DVD-	DVD-	k?
<g/>
18	#num#	k4
<g/>
:	:	kIx,
dvě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
dvě	dva	k4xCgFnPc1
vrstvy	vrstva	k1gFnPc1
na	na	k7c6
každé	každý	k3xTgFnSc6
straně	strana	k1gFnSc6
<g/>
,	,	kIx,
17,1	17,1	k4
GB	GB	kA
(	(	kIx(
<g/>
15,9	15,9	k4
GiB	GiB	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Uživatel	uživatel	k1gMnSc1
může	moct	k5eAaImIp3nS
vytvořit	vytvořit	k5eAaPmF
DVD-nosiče	DVD-nosič	k1gInPc4
těchto	tento	k3xDgMnPc2
typů	typ	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
DVD-Video	DVD-Video	k1gNnSc1
(	(	kIx(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS
filmy	film	k1gInPc4
(	(	kIx(
<g/>
obraz	obraz	k1gInSc4
a	a	k8xC
zvuk	zvuk	k1gInSc4
<g/>
))	))	k?
</s>
<s>
DVD-Audio	DVD-Audio	k1gNnSc1
(	(	kIx(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS
zvuk	zvuk	k1gInSc4
v	v	k7c6
kvalitě	kvalita	k1gFnSc6
CD	CD	kA
a	a	k8xC
lepší	dobrý	k2eAgFnSc1d2
<g/>
)	)	kIx)
</s>
<s>
DVD	DVD	kA
Data	datum	k1gNnSc2
(	(	kIx(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS
údaje	údaj	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
Označení	označení	k1gNnSc1
„	„	k?
<g/>
+	+	kIx~
<g/>
“	“	k?
(	(	kIx(
<g/>
plus	plus	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
„	„	k?
<g/>
−	−	k?
<g/>
“	“	k?
(	(	kIx(
<g/>
minus	minus	k1gInSc1
<g/>
)	)	kIx)
představuje	představovat	k5eAaImIp3nS
dva	dva	k4xCgInPc4
různé	různý	k2eAgInPc4d1
technické	technický	k2eAgInPc4d1
standardy	standard	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
do	do	k7c2
určité	určitý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
kompatibilní	kompatibilní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Médium	médium	k1gNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
typu	typ	k1gInSc3
<g/>
:	:	kIx,
</s>
<s>
DVD-ROM	DVD-ROM	k?
(	(	kIx(
<g/>
Read	Read	k1gMnSc1
Only	Onla	k1gFnSc2
Memory	Memora	k1gFnSc2
<g/>
,	,	kIx,
paměť	paměť	k1gFnSc4
jen	jen	k9
pro	pro	k7c4
čtení	čtení	k1gNnSc4
<g/>
,	,	kIx,
vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
lisováním	lisování	k1gNnSc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
pomyslný	pomyslný	k2eAgMnSc1d1
nástupce	nástupce	k1gMnSc1
formátu	formát	k1gInSc2
CD-ROM	CD-ROM	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
víceúčelový	víceúčelový	k2eAgInSc4d1
formát	formát	k1gInSc4
pro	pro	k7c4
přehrávání	přehrávání	k1gNnSc4
počítačových	počítačový	k2eAgNnPc2d1
dat	datum	k1gNnPc2
a	a	k8xC
multimediálních	multimediální	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtení	čtení	k1gNnSc1
DVD	DVD	kA
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
ve	v	k7c6
všech	všecek	k3xTgInPc2
PC	PC	kA
(	(	kIx(
<g/>
a	a	k8xC
ostatních	ostatní	k2eAgFnPc2d1
platforem	platforma	k1gFnPc2
<g/>
)	)	kIx)
vybavených	vybavený	k2eAgInPc2d1
jednotkou	jednotka	k1gFnSc7
DVD	DVD	kA
s	s	k7c7
podporou	podpora	k1gFnSc7
logického	logický	k2eAgInSc2d1
formátu	formát	k1gInSc2
UDF	UDF	kA
<g/>
.	.	kIx.
</s>
<s>
Zapisovatelná	zapisovatelný	k2eAgFnSc1d1
a	a	k8xC
přepisovatelná	přepisovatelný	k2eAgFnSc1d1
DVD	DVD	kA
</s>
<s>
Existují	existovat	k5eAaImIp3nP
tři	tři	k4xCgInPc1
typy	typ	k1gInPc1
zapisovatelných	zapisovatelný	k2eAgInPc2d1
a	a	k8xC
přepisovatelných	přepisovatelný	k2eAgInPc2d1
DVD	DVD	kA
disků	disk	k1gInPc2
<g/>
:	:	kIx,
DVD-R	DVD-R	k1gMnSc1
<g/>
/	/	kIx~
<g/>
RW	RW	kA
<g/>
,	,	kIx,
DVD	DVD	kA
<g/>
+	+	kIx~
<g/>
R	R	kA
<g/>
/	/	kIx~
<g/>
RW	RW	kA
(	(	kIx(
<g/>
plus	plus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
DVD-RAM	DVD-RAM	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
DVD	DVD	kA
<g/>
+	+	kIx~
<g/>
R	R	kA
<g/>
/	/	kIx~
<g/>
RW	RW	kA
(	(	kIx(
<g/>
R	R	kA
=	=	kIx~
Recordable	Recordable	k1gMnSc2
<g/>
,	,	kIx,
jen	jen	k9
pro	pro	k7c4
jeden	jeden	k4xCgInSc4
zápis	zápis	k1gInSc4
<g/>
,	,	kIx,
RW	RW	kA
=	=	kIx~
ReWritable	ReWritable	k1gMnSc1
<g/>
,	,	kIx,
pro	pro	k7c4
přepisování	přepisování	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
Formát	formát	k1gInSc1
DVD	DVD	kA
<g/>
+	+	kIx~
<g/>
R	R	kA
je	být	k5eAaImIp3nS
mezi	mezi	k7c7
široce	široko	k6eAd1
rozšířenými	rozšířený	k2eAgInPc7d1
formáty	formát	k1gInPc7
nejmladší	mladý	k2eAgMnPc1d3
<g/>
,	,	kIx,
dokonce	dokonce	k9
mladší	mladý	k2eAgMnSc1d2
než	než	k8xS
formát	formát	k1gInSc1
DVD	DVD	kA
<g/>
+	+	kIx~
<g/>
RW	RW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Disky	disk	k1gInPc1
DVD	DVD	kA
<g/>
+	+	kIx~
<g/>
R	R	kA
lze	lze	k6eAd1
v	v	k7c6
současnosti	současnost	k1gFnSc6
běžně	běžně	k6eAd1
zapisovat	zapisovat	k5eAaImF
osminásobnou	osminásobný	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
oproti	oproti	k7c3
standardní	standardní	k2eAgFnSc3d1
rychlosti	rychlost	k1gFnSc3
DVD	DVD	kA
<g/>
,	,	kIx,
tedy	tedy	k9
10	#num#	k4
800	#num#	k4
kB	kb	kA
za	za	k7c4
sekundu	sekunda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Touto	tento	k3xDgFnSc7
rychlostí	rychlost	k1gFnSc7
trvá	trvat	k5eAaImIp3nS
zápis	zápis	k1gInSc1
na	na	k7c4
disk	disk	k1gInSc4
přibližně	přibližně	k6eAd1
10	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
DVD	DVD	kA
<g/>
+	+	kIx~
<g/>
RW	RW	kA
je	být	k5eAaImIp3nS
přepisovatelná	přepisovatelný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
formátu	formát	k1gInSc2
DVD	DVD	kA
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Standardní	standardní	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
pro	pro	k7c4
zápis	zápis	k1gInSc4
na	na	k7c4
toto	tento	k3xDgNnSc4
médium	médium	k1gNnSc4
je	být	k5eAaImIp3nS
čtyřnásobná	čtyřnásobný	k2eAgFnSc1d1
oproti	oproti	k7c3
základní	základní	k2eAgFnSc3d1
rychlosti	rychlost	k1gFnSc3
čtení	čtení	k1gNnSc2
DVD	DVD	kA
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnPc1
mechaniky	mechanika	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
schopny	schopen	k2eAgFnPc1d1
zapisovat	zapisovat	k5eAaImF
DVD-R	DVD-R	k1gFnSc4
<g/>
,	,	kIx,
DVD-RW	DVD-RW	k1gFnSc4
<g/>
,	,	kIx,
CD-R	CD-R	k1gFnSc4
a	a	k8xC
CD-RW	CD-RW	k1gFnSc4
<g/>
,	,	kIx,
vyráběla	vyrábět	k5eAaImAgFnS
firma	firma	k1gFnSc1
Pioneer	Pioneer	kA
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kompatibilita	kompatibilita	k1gFnSc1
však	však	k9
nebyla	být	k5eNaImAgFnS
nejlepší	dobrý	k2eAgFnSc1d3
<g/>
,	,	kIx,
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
chybné	chybný	k2eAgFnSc3d1
identifikaci	identifikace	k1gFnSc3
medií	medium	k1gNnPc2
s	s	k7c7
nižší	nízký	k2eAgFnSc7d2
odrazivostí	odrazivost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
DVD	DVD	kA
<g/>
+	+	kIx~
<g/>
R	R	kA
DL	DL	kA
(	(	kIx(
<g/>
R	R	kA
=	=	kIx~
Recordable	Recordable	k1gMnSc2
<g/>
,	,	kIx,
jen	jen	k9
pro	pro	k7c4
jeden	jeden	k4xCgInSc4
zápis	zápis	k1gInSc4
<g/>
,	,	kIx,
DL	DL	kA
=	=	kIx~
DualLayer	DualLayer	k1gMnSc1
<g/>
,	,	kIx,
dvě	dva	k4xCgFnPc4
vrstvy	vrstva	k1gFnPc4
<g/>
)	)	kIx)
</s>
<s>
DVD-R	DVD-R	k?
<g/>
/	/	kIx~
<g/>
RW	RW	kA
(	(	kIx(
<g/>
R	R	kA
=	=	kIx~
Recordable	Recordable	k1gMnSc2
<g/>
,	,	kIx,
jen	jen	k9
pro	pro	k7c4
jeden	jeden	k4xCgInSc4
zápis	zápis	k1gInSc4
<g/>
,	,	kIx,
RW	RW	kA
=	=	kIx~
ReWritable	ReWritable	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c4
přepisování	přepisování	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
Formát	formát	k1gInSc1
DVD-R	DVD-R	k1gFnSc2
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
technologie	technologie	k1gFnSc2
klasického	klasický	k2eAgInSc2d1
kompaktního	kompaktní	k2eAgInSc2d1
disku	disk	k1gInSc2
<g/>
,	,	kIx,
existuje	existovat	k5eAaImIp3nS
tedy	tedy	k9
ve	v	k7c6
dvou	dva	k4xCgFnPc6
verzích	verze	k1gFnPc6
–	–	k?
verze	verze	k1gFnSc2
R	R	kA
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
lze	lze	k6eAd1
pouze	pouze	k6eAd1
zapisovat	zapisovat	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
verze	verze	k1gFnSc1
RW	RW	kA
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
lze	lze	k6eAd1
přepisovat	přepisovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
formát	formát	k1gInSc1
byl	být	k5eAaImAgInS
navržen	navrhnout	k5eAaPmNgInS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
co	co	k9
nejkompatibilnější	kompatibilní	k2eAgFnSc4d3
s	s	k7c7
lisovanými	lisovaný	k2eAgInPc7d1
disky	disk	k1gInPc7
DVD	DVD	kA
(	(	kIx(
<g/>
DVD-ROM	DVD-ROM	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgInSc2
plyne	plynout	k5eAaImIp3nS
výhoda	výhoda	k1gFnSc1
tohoto	tento	k3xDgInSc2
formátu	formát	k1gInSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
je	být	k5eAaImIp3nS
kompatibilita	kompatibilita	k1gFnSc1
se	s	k7c7
staršími	starý	k2eAgFnPc7d2
mechanikami	mechanika	k1gFnPc7
a	a	k8xC
přehrávači	přehrávač	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
vznikly	vzniknout	k5eAaPmAgInP
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
dalo	dát	k5eAaPmAgNnS
na	na	k7c6
DVD	DVD	kA
zapisovat	zapisovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
výhoda	výhoda	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
v	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
ztrácí	ztrácet	k5eAaImIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
téměř	téměř	k6eAd1
všechny	všechen	k3xTgInPc1
vyráběné	vyráběný	k2eAgInPc1d1
přehrávače	přehrávač	k1gInPc1
a	a	k8xC
mechaniky	mechanika	k1gFnPc1
dokážou	dokázat	k5eAaPmIp3nP
přehrávat	přehrávat	k5eAaImF
jak	jak	k8xS,k8xC
DVD-R	DVD-R	k1gFnSc4
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
DVD	DVD	kA
<g/>
+	+	kIx~
<g/>
R	R	kA
formáty	formát	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
DVD-RAM	DVD-RAM	k?
–	–	k?
Random	Random	k1gInSc1
Access	Accessa	k1gFnPc2
Memory	Memora	k1gFnSc2
<g/>
,	,	kIx,
libovolně	libovolně	k6eAd1
přepisovatelné	přepisovatelný	k2eAgNnSc1d1
médium	médium	k1gNnSc1
–	–	k?
dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gMnSc7
pracovat	pracovat	k5eAaImF
stejným	stejný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
jako	jako	k8xS,k8xC
s	s	k7c7
pevným	pevný	k2eAgNnSc7d1
diskem	disco	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
EcoDisc	EcoDisc	k6eAd1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
byl	být	k5eAaImAgMnS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
Hamburskou	hamburský	k2eAgFnSc7d1
firmou	firma	k1gFnSc7
Optical	Optical	k1gFnSc2
Disc	disco	k1gNnPc2
Service	Service	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
patentován	patentovat	k5eAaBmNgInS
a	a	k8xC
uveden	uvést	k5eAaPmNgInS
na	na	k7c4
trh	trh	k1gInSc4
tzv.	tzv.	kA
EcoDisc	EcoDisc	k1gInSc1
<g/>
,	,	kIx,
česky	česky	k6eAd1
uváděn	uvádět	k5eAaImNgMnS
také	také	k9
jako	jako	k9
EcoDisk	EcoDisk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formát	formát	k1gInSc1
dat	datum	k1gNnPc2
je	být	k5eAaImIp3nS
stejný	stejný	k2eAgInSc1d1
jako	jako	k8xS,k8xC
běžné	běžný	k2eAgInPc1d1
DVD	DVD	kA
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
vylepšené	vylepšený	k2eAgNnSc1d1
některé	některý	k3yIgFnPc4
mechanické	mechanický	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
má	mít	k5eAaImIp3nS
nižší	nízký	k2eAgFnSc4d2
kompatibilitu	kompatibilita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
vyroben	vyrobit	k5eAaPmNgInS
z	z	k7c2
polovičního	poloviční	k2eAgNnSc2d1
množství	množství	k1gNnSc2
materiálu	materiál	k1gInSc2
(	(	kIx(
<g/>
polykarbonátu	polykarbonát	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
má	mít	k5eAaImIp3nS
tak	tak	k6eAd1
poloviční	poloviční	k2eAgFnSc4d1
tloušťku	tloušťka	k1gFnSc4
(	(	kIx(
<g/>
0,6	0,6	k4
mm	mm	kA
<g/>
)	)	kIx)
a	a	k8xC
poloviční	poloviční	k2eAgFnSc4d1
hmotnost	hmotnost	k1gFnSc4
(	(	kIx(
<g/>
8	#num#	k4
g	g	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
ohebnější	ohebný	k2eAgFnSc1d2
a	a	k8xC
odolnější	odolný	k2eAgFnSc1d2
proti	proti	k7c3
poškrábání	poškrábání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uváděná	uváděný	k2eAgFnSc1d1
životnost	životnost	k1gFnSc1
je	být	k5eAaImIp3nS
200	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Není	být	k5eNaImIp3nS
kompatibilní	kompatibilní	k2eAgFnSc1d1
se	s	k7c7
štěrbinovými	štěrbinový	k2eAgFnPc7d1
mechanikami	mechanika	k1gFnPc7
a	a	k8xC
nekompatibilita	nekompatibilita	k1gFnSc1
je	být	k5eAaImIp3nS
označena	označit	k5eAaPmNgFnS
malou	malý	k2eAgFnSc7d1
grafickou	grafický	k2eAgFnSc7d1
ikonou	ikona	k1gFnSc7
na	na	k7c6
disku	disk	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.root.cz/clanky/vyvoj-optickych-pameti-od-dvd-k-blu-ray/	http://www.root.cz/clanky/vyvoj-optickych-pameti-od-dvd-k-blu-ray/	k?
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.cdr.cz	www.cdr.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ekologie	ekologie	k1gFnSc1
a	a	k8xC
digitální	digitální	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
www.filmag.cz	www.filmag.cza	k1gFnPc2
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2007	#num#	k4
<g/>
↑	↑	k?
http://www.nazeleno.cz/ecodisc-8-gramu-ekologicke-alternativy-dvd.aspx	http://www.nazeleno.cz/ecodisc-8-gramu-ekologicke-alternativy-dvd.aspx	k1gInSc1
</s>
<s>
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
kdysi	kdysi	k6eAd1
<g/>
:	:	kIx,
Jak	jak	k8xS,k8xC
dlouho	dlouho	k6eAd1
se	se	k3xPyFc4
točí	točit	k5eAaImIp3nS
DVD	DVD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Kompaktní	kompaktní	k2eAgInSc1d1
disk	disk	k1gInSc1
(	(	kIx(
<g/>
CD	CD	kA
<g/>
)	)	kIx)
</s>
<s>
Blu-ray	Blu-raa	k1gFnPc1
–	–	k?
nástupce	nástupce	k1gMnSc1
DVD	DVD	kA
</s>
<s>
HD	HD	kA
DVD	DVD	kA
–	–	k?
neúspěšný	úspěšný	k2eNgMnSc1d1
nástupce	nástupce	k1gMnSc1
DVD	DVD	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
DVD	DVD	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
DVD	DVD	kA
v	v	k7c6
České	český	k2eAgFnSc6d1
terminologické	terminologický	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
knihovnictví	knihovnictví	k1gNnSc2
a	a	k8xC
informační	informační	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
TDKIV	TDKIV	kA
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
Následovníci	následovník	k1gMnPc1
kompaktních	kompaktní	k2eAgInPc2d1
disků	disk	k1gInPc2
<g/>
:	:	kIx,
DVD	DVD	kA
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
Článek	článek	k1gInSc1
o	o	k7c4
EcoDiscu	EcoDisca	k1gFnSc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
DVD	DVD	kA
<g/>
+	+	kIx~
<g/>
RW	RW	kA
Aliance	aliance	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Asociace	asociace	k1gFnSc1
pro	pro	k7c4
kontrolu	kontrola	k1gFnSc4
kopírování	kopírování	k1gNnSc2
DVD	DVD	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Informace	informace	k1gFnSc1
o	o	k7c4
práci	práce	k1gFnSc4
dual-layer	dual-layra	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
časté	častý	k2eAgInPc1d1
dotazy	dotaz	k1gInPc1
o	o	k7c6
DVD	DVD	kA
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4501545-4	4501545-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
97005636	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
97005636	#num#	k4
</s>
