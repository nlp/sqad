<s>
Jaký	jaký	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
název	název	k1gInSc4
formát	formát	k1gInSc1
digitálního	digitální	k2eAgInSc2d1
datového	datový	k2eAgInSc2d1
nosiče	nosič	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
může	moct	k5eAaImIp3nS
obsahovat	obsahovat	k5eAaImF
filmy	film	k1gInPc4
ve	v	k7c6
vysoké	vysoký	k2eAgFnSc6d1
obrazové	obrazový	k2eAgFnSc6d1
a	a	k8xC
zvukové	zvukový	k2eAgFnSc6d1
kvalitě	kvalita	k1gFnSc6
nebo	nebo	k8xC
jiná	jiný	k2eAgNnPc4d1
data	data	k1gNnPc4
<g/>
?	?	kIx.
</s>