<s>
Marlboro	Marlboro	k1gNnSc1	Marlboro
je	být	k5eAaImIp3nS	být
značka	značka	k1gFnSc1	značka
cigaret	cigareta	k1gFnPc2	cigareta
firmy	firma	k1gFnSc2	firma
Altria	Altrium	k1gNnSc2	Altrium
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sportovním	sportovní	k2eAgInSc6d1	sportovní
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
značka	značka	k1gFnSc1	značka
sponzorem	sponzor	k1gMnSc7	sponzor
stáje	stáj	k1gFnSc2	stáj
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
Ferrari	Ferrari	k1gMnPc2	Ferrari
a	a	k8xC	a
v	v	k7c6	v
MotoGP	MotoGP	k1gFnSc6	MotoGP
sponzor	sponzor	k1gMnSc1	sponzor
Ducati	ducat	k5eAaImF	ducat
<g/>
.	.	kIx.	.
</s>
<s>
Značka	značka	k1gFnSc1	značka
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
pro	pro	k7c4	pro
své	své	k1gNnSc4	své
reklamy	reklama	k1gFnSc2	reklama
s	s	k7c7	s
kovbojem	kovboj	k1gMnSc7	kovboj
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c4	po
Great	Great	k2eAgInSc4d1	Great
Marlborough	Marlborough	k1gInSc4	Marlborough
Street	Streeta	k1gFnPc2	Streeta
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgMnPc4	který
sídlila	sídlit	k5eAaImAgFnS	sídlit
původní	původní	k2eAgFnSc1d1	původní
londýnská	londýnský	k2eAgFnSc1d1	londýnská
továrna	továrna	k1gFnSc1	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Marlboro	Marlboro	k1gNnSc2	Marlboro
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
