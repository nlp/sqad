<s desamb="1">
Dle	dle	k7c2
římských	římský	k2eAgMnPc2d1
historiků	historik	k1gMnPc2
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgInS
na	na	k7c4
trůn	trůn	k1gInSc4
pomocí	pomocí	k7c2
vraždy	vražda	k1gFnSc2
svého	svůj	k3xOyFgMnSc2
předchůdce	předchůdce	k1gMnSc2
Servia	Servius	k1gMnSc2
Tullia	Tullius	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Římský	římský	k2eAgMnSc1d1
historik	historik	k1gMnSc1
Livius	Livius	k1gMnSc1
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
T.	T.	kA
Superbus	Superbus	k1gInSc1
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
synem	syn	k1gMnSc7
pátého	pátý	k4xOgMnSc4
krále	král	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
Tarquinius	Tarquinius	k1gMnSc1
Priscus	Priscus	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
S.	S.	kA
Tullia	Tullius	k1gMnSc4
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
svůj	svůj	k3xOyFgMnSc1
(	(	kIx(
<g/>
údajný	údajný	k2eAgMnSc1d1
<g/>
)	)	kIx)
otec	otec	k1gMnSc1
etruského	etruský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>