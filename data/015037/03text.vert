<s>
Tarquinius	Tarquinius	k1gMnSc1
Superbus	Superbus	k1gMnSc1
</s>
<s>
Tarquinius	Tarquinius	k1gMnSc1
Superbus	Superbus	k1gMnSc1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
495	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Kumy	kuma	k1gFnPc1
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Sextus	Sextus	k1gMnSc1
Tarquinius	Tarquinius	k1gMnSc1
<g/>
,	,	kIx,
Arruns	Arruns	k1gInSc1
Tarquinius	Tarquinius	k1gInSc1
<g/>
,	,	kIx,
Tito	tento	k3xDgMnPc1
Tarquinio	Tarquinio	k6eAd1
a	a	k8xC
Tarquinia	Tarquinium	k1gNnPc1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Gnaeus	Gnaeus	k1gMnSc1
Tarquinius	Tarquinius	k1gMnSc1
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lucius	Lucius	k1gMnSc1
Tarquinius	Tarquinius	k1gMnSc1
Superbus	Superbus	k1gMnSc1
(	(	kIx(
<g/>
neznámo	neznámo	k1gNnSc1
–	–	k?
495	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
měl	mít	k5eAaImAgInS
dle	dle	k7c2
tradice	tradice	k1gFnSc2
být	být	k5eAaImF
asi	asi	k9
od	od	k7c2
roku	rok	k1gInSc2
535	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
510	#num#	k4
<g/>
/	/	kIx~
<g/>
509	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
posledním	poslední	k2eAgMnSc7d1
římským	římský	k2eAgMnSc7d1
králem	král	k1gMnSc7
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
skončilo	skončit	k5eAaPmAgNnS
období	období	k1gNnSc1
známo	znám	k2eAgNnSc1d1
jako	jako	k8xC,k8xS
Římské	římský	k2eAgNnSc1d1
království	království	k1gNnSc1
a	a	k8xC
byla	být	k5eAaImAgFnS
nastolena	nastolen	k2eAgFnSc1d1
Římská	římský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
římských	římský	k2eAgMnPc2d1
historiků	historik	k1gMnPc2
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgInS
na	na	k7c4
trůn	trůn	k1gInSc4
pomocí	pomocí	k7c2
vraždy	vražda	k1gFnSc2
svého	svůj	k3xOyFgMnSc2
předchůdce	předchůdce	k1gMnSc2
Servia	Servius	k1gMnSc2
Tullia	Tullius	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Římský	římský	k2eAgMnSc1d1
historik	historik	k1gMnSc1
Livius	Livius	k1gMnSc1
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
T.	T.	kA
Superbus	Superbus	k1gInSc1
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
synem	syn	k1gMnSc7
pátého	pátý	k4xOgMnSc4
krále	král	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
Tarquinius	Tarquinius	k1gMnSc1
Priscus	Priscus	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
S.	S.	kA
Tullia	Tullius	k1gMnSc4
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
svůj	svůj	k3xOyFgMnSc1
(	(	kIx(
<g/>
údajný	údajný	k2eAgMnSc1d1
<g/>
)	)	kIx)
otec	otec	k1gMnSc1
etruského	etruský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Podobně	podobně	k6eAd1
jako	jako	k9
o	o	k7c6
ostatních	ostatní	k2eAgFnPc6d1
římských	římský	k2eAgFnPc6d1
králích	král	k1gMnPc6
toho	ten	k3xDgNnSc2
není	být	k5eNaImIp3nS
o	o	k7c6
něm	on	k3xPp3gMnSc6
příliš	příliš	k6eAd1
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
anebo	anebo	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
založeno	založit	k5eAaPmNgNnS
jen	jen	k9
na	na	k7c6
legendách	legenda	k1gFnPc6
či	či	k8xC
příbězích	příběh	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
stavěly	stavět	k5eAaImAgInP
na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
nepopularitě	nepopularita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
římských	římský	k2eAgMnPc2d1
historiků	historik	k1gMnPc2
například	například	k6eAd1
od	od	k7c2
počátku	počátek	k1gInSc2
vlády	vláda	k1gFnSc2
rušil	rušit	k5eAaImAgMnS
různé	různý	k2eAgInPc4d1
zákony	zákon	k1gInPc4
a	a	k8xC
nahrazoval	nahrazovat	k5eAaImAgMnS
je	být	k5eAaImIp3nS
svými	svůj	k3xOyFgMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
obklopovat	obklopovat	k5eAaImF
oblíbenci	oblíbenec	k1gMnPc1
místo	místo	k7c2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
by	by	kYmCp3nP
si	se	k3xPyFc3
to	ten	k3xDgNnSc4
skutečně	skutečně	k6eAd1
zasloužili	zasloužit	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Zároveň	zároveň	k6eAd1
však	však	k9
mohla	moct	k5eAaImAgFnS
za	za	k7c2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
být	být	k5eAaImF
dokončena	dokončit	k5eAaPmNgFnS
stavba	stavba	k1gFnSc1
Jupiterova	Jupiterův	k2eAgInSc2d1
chrámu	chrám	k1gInSc2
či	či	k8xC
pokračoval	pokračovat	k5eAaImAgInS
ve	v	k7c6
výstavbě	výstavba	k1gFnSc6
Velké	velký	k2eAgFnSc2d1
stoky	stoka	k1gFnSc2
(	(	kIx(
<g/>
Cloaca	Cloac	k2eAgFnSc1d1
maxima	maxima	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
vláda	vláda	k1gFnSc1
měla	mít	k5eAaImAgFnS
skončit	skončit	k5eAaPmF
roku	rok	k1gInSc2
kolem	kolem	k7c2
510	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
anebo	anebo	k8xC
509	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
vyhnáním	vyhnání	k1gNnSc7
z	z	k7c2
města	město	k1gNnSc2
Řím	Řím	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Záminkou	záminka	k1gFnSc7
pro	pro	k7c4
vypuzení	vypuzení	k1gNnSc4
neoblíbeného	oblíbený	k2eNgMnSc2d1
krále	král	k1gMnSc2
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Sextus	Sextus	k1gMnSc1
Tarquinius	Tarquinius	k1gMnSc1
údajně	údajně	k6eAd1
zneuctil	zneuctít	k5eAaPmAgMnS
Lukrécii	Lukrécie	k1gFnSc4
<g/>
,	,	kIx,
ženu	žena	k1gFnSc4
římského	římský	k2eAgMnSc2d1
šlechtice	šlechtic	k1gMnSc2
Tarquinia	Tarquinium	k1gNnSc2
Collatina	Collatina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
pak	pak	k6eAd1
spáchala	spáchat	k5eAaPmAgFnS
sebevraždu	sebevražda	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Zneuctění	zneuctění	k1gNnSc4
Lukrécie	Lukrécie	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gFnSc1
smrt	smrt	k1gFnSc1
se	se	k3xPyFc4
postupem	postup	k1gInSc7
času	čas	k1gInSc2
stala	stát	k5eAaPmAgFnS
významným	významný	k2eAgInSc7d1
uměleckým	umělecký	k2eAgInSc7d1
námětem	námět	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titus	Titus	k1gMnSc1
Livius	Livius	k1gMnSc1
píše	psát	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
aktu	akt	k1gInSc3
došlo	dojít	k5eAaPmAgNnS
během	během	k7c2
králova	králův	k2eAgNnSc2d1
válečného	válečný	k2eAgNnSc2d1
tažení	tažení	k1gNnSc2
proti	proti	k7c3
městu	město	k1gNnSc3
Ardea	Arde	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
králově	králův	k2eAgNnSc6d1
vyhnání	vyhnání	k1gNnSc6
byla	být	k5eAaImAgFnS
ustanovena	ustanoven	k2eAgFnSc1d1
Římská	římský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
a	a	k8xC
do	do	k7c2
čela	čelo	k1gNnSc2
se	se	k3xPyFc4
mohli	moct	k5eAaImAgMnP
dostat	dostat	k5eAaPmF
první	první	k4xOgInSc4
dva	dva	k4xCgMnPc1
konzulové	konzul	k1gMnPc1
L.	L.	kA
Iunius	Iunius	k1gMnSc1
Brutus	Brutus	k1gMnSc1
a	a	k8xC
L.	L.	kA
Tarquinius	Tarquinius	k1gMnSc1
Collatinus	Collatinus	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Královské	královský	k2eAgFnPc1d1
kompetence	kompetence	k1gFnPc1
byly	být	k5eAaImAgFnP
převedeny	převést	k5eAaPmNgFnP
na	na	k7c4
jiné	jiný	k2eAgInPc4d1
úřady	úřad	k1gInPc4
a	a	k8xC
titul	titul	k1gInSc1
již	již	k6eAd1
neměl	mít	k5eNaImAgInS
být	být	k5eAaImF
nikdy	nikdy	k6eAd1
použit	použít	k5eAaPmNgMnS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
plné	plný	k2eAgFnSc6d1
autoritě	autorita	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Některé	některý	k3yIgNnSc1
z	z	k7c2
pravomocí	pravomoc	k1gFnPc2
přebrali	přebrat	k5eAaPmAgMnP
nastupující	nastupující	k2eAgMnPc1d1
konzulové	konzul	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgFnPc1
náboženské	náboženský	k2eAgFnPc1d1
připadly	připadnout	k5eAaPmAgFnP
člověku	člověk	k1gMnSc6
s	s	k7c7
funkcí	funkce	k1gFnSc7
tzv	tzv	kA
"	"	kIx"
<g/>
Rex	Rex	k1gFnSc1
sacrorum	sacrorum	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konzulové	konzul	k1gMnPc1
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
králů	král	k1gMnPc2
<g/>
,	,	kIx,
měli	mít	k5eAaImAgMnP
být	být	k5eAaImF
vždy	vždy	k6eAd1
dva	dva	k4xCgMnPc1
(	(	kIx(
<g/>
alespoň	alespoň	k9
tomu	ten	k3xDgNnSc3
tak	tak	k6eAd1
bylo	být	k5eAaImAgNnS
dle	dle	k7c2
pozdější	pozdní	k2eAgFnSc2d2
tradice	tradice	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
mohli	moct	k5eAaImAgMnP
kontrolovat	kontrolovat	k5eAaImF
a	a	k8xC
aby	aby	kYmCp3nP
nedisponovali	disponovat	k5eNaBmAgMnP
tak	tak	k6eAd1
velkou	velký	k2eAgFnSc7d1
mocí	moc	k1gFnSc7
<g/>
,	,	kIx,
jakou	jaký	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
měl	mít	k5eAaImAgInS
v	v	k7c6
minulosti	minulost	k1gFnSc6
král	král	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
"	"	kIx"
<g/>
Rex	Rex	k1gFnSc1
sacrorum	sacrorum	k1gInSc1
<g/>
"	"	kIx"
měl	mít	k5eAaImAgMnS
náboženské	náboženský	k2eAgFnPc4d1
pravomoci	pravomoc	k1gFnPc4
<g/>
,	,	kIx,
kterými	který	k3yRgFnPc7,k3yIgFnPc7,k3yQgFnPc7
původně	původně	k6eAd1
disponoval	disponovat	k5eAaBmAgInS
král	král	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
další	další	k2eAgFnPc4d1
kompetence	kompetence	k1gFnPc4
již	již	k6eAd1
neměl	mít	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královský	královský	k2eAgInSc1d1
(	(	kIx(
<g/>
"	"	kIx"
<g/>
rex	rex	k?
<g/>
"	"	kIx"
<g/>
)	)	kIx)
titul	titul	k1gInSc1
se	se	k3xPyFc4
tedy	tedy	k9
udržel	udržet	k5eAaPmAgInS
jen	jen	k9
ve	v	k7c6
jméně	jméno	k1gNnSc6
titulu	titul	k1gInSc2
samotného	samotný	k2eAgInSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Rozdělení	rozdělení	k1gNnSc1
pravomocí	pravomoc	k1gFnPc2
mezi	mezi	k7c4
více	hodně	k6eAd2
úřadů	úřad	k1gInPc2
mělo	mít	k5eAaImAgNnS
zamezit	zamezit	k5eAaPmF
větší	veliký	k2eAgFnSc4d2
kumulaci	kumulace	k1gFnSc4
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samovláda	samovláda	k1gFnSc1
se	se	k3xPyFc4
neměla	mít	k5eNaImAgFnS
již	již	k6eAd1
nikdy	nikdy	k6eAd1
opakovat	opakovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
T.	T.	kA
Superbus	Superbus	k1gMnSc1
údajně	údajně	k6eAd1
uprchl	uprchnout	k5eAaPmAgMnS
z	z	k7c2
Říma	Řím	k1gInSc2
pryč	pryč	k6eAd1
a	a	k8xC
následně	následně	k6eAd1
se	se	k3xPyFc4
snad	snad	k9
několikrát	několikrát	k6eAd1
pokusil	pokusit	k5eAaPmAgMnS
o	o	k7c4
znovunastolení	znovunastolení	k1gNnSc4
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
nikdy	nikdy	k6eAd1
však	však	k9
neuspěl	uspět	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Ohledně	ohledně	k7c2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
konce	konec	k1gInSc2
existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc1
verzí	verze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
častá	častý	k2eAgFnSc1d1
verze	verze	k1gFnSc1
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Lars	Lars	k1gInSc1
Porsenna	Porsenn	k1gMnSc2
<g/>
,	,	kIx,
vládce	vládce	k1gMnSc2
města	město	k1gNnSc2
Clusia	Clusius	k1gMnSc2
<g/>
,	,	kIx,
mu	on	k3xPp3gMnSc3
měl	mít	k5eAaImAgInS
pomoci	pomoct	k5eAaPmF
zkusit	zkusit	k5eAaPmF
se	se	k3xPyFc4
dostat	dostat	k5eAaPmF
zpět	zpět	k6eAd1
na	na	k7c4
královský	královský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Některé	některý	k3yIgNnSc1
dokonce	dokonce	k9
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
to	ten	k3xDgNnSc1
dočasně	dočasně	k6eAd1
povedlo	povést	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
existují	existovat	k5eAaImIp3nP
i	i	k9
teorie	teorie	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
tvrdí	tvrdit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
Porsenna	Porsenna	k1gFnSc1
<g/>
,	,	kIx,
místo	místo	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Superbovi	Superba	k1gMnSc3
pomohl	pomoct	k5eAaPmAgMnS
<g/>
,	,	kIx,
usiloval	usilovat	k5eAaImAgMnS
o	o	k7c4
zisk	zisk	k1gInSc4
trůnu	trůn	k1gInSc2
sám	sám	k3xTgInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohlo	moct	k5eAaImAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
teoreticky	teoreticky	k6eAd1
i	i	k8xC
povést	povést	k5eAaPmF
Řím	Řím	k1gInSc1
ovládnout	ovládnout	k5eAaPmF
<g/>
,	,	kIx,
anebo	anebo	k8xC
vládu	vláda	k1gFnSc4
přenechal	přenechat	k5eAaPmAgMnS
někomu	někdo	k3yInSc3
třetímu	třetí	k4xOgMnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
pokud	pokud	k8xS
tomu	ten	k3xDgNnSc3
tak	tak	k9
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
pak	pak	k6eAd1
u	u	k7c2
moci	moc	k1gFnSc2
vydržel	vydržet	k5eAaPmAgMnS
jen	jen	k9
velmi	velmi	k6eAd1
omezenou	omezený	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
každém	každý	k3xTgInSc6
případě	případ	k1gInSc6
není	být	k5eNaImIp3nS
zcela	zcela	k6eAd1
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
nakonec	nakonec	k6eAd1
byl	být	k5eAaImAgMnS
skutečným	skutečný	k2eAgInSc7d1
"	"	kIx"
<g/>
posledním	poslední	k2eAgInSc7d1
<g/>
"	"	kIx"
římským	římský	k2eAgMnSc7d1
králem	král	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Ačkoliv	ačkoliv	k8xS
byl	být	k5eAaImAgMnS
vyhnaný	vyhnaný	k2eAgMnSc1d1
král	král	k1gMnSc1
etruského	etruský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
Etruskové	Etrusk	k1gMnPc1
mohli	moct	k5eAaImAgMnP
nadále	nadále	k6eAd1
v	v	k7c6
Římě	Řím	k1gInSc6
žít	žít	k5eAaImF
a	a	k8xC
působit	působit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
Etruskové	Etrusk	k1gMnPc1
se	se	k3xPyFc4
dokonce	dokonce	k9
později	pozdě	k6eAd2
měli	mít	k5eAaImAgMnP
stát	stát	k5eAaImF,k5eAaPmF
konzuly	konzul	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Sice	sice	k8xC
není	být	k5eNaImIp3nS
stále	stále	k6eAd1
zcela	zcela	k6eAd1
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
jakou	jaký	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
roli	role	k1gFnSc4
Etruskové	Etrusk	k1gMnPc1
hráli	hrát	k5eAaImAgMnP
u	u	k7c2
založení	založení	k1gNnSc2
samotného	samotný	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
jisté	jistý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c2
vlády	vláda	k1gFnSc2
posledního	poslední	k2eAgMnSc2d1
krále	král	k1gMnSc2
musel	muset	k5eAaImAgInS
být	být	k5eAaImF
Řím	Řím	k1gInSc1
minimálně	minimálně	k6eAd1
částečně	částečně	k6eAd1
etruským	etruský	k2eAgNnSc7d1
městem	město	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Porovnání	porovnání	k1gNnSc4
s	s	k7c7
etruskými	etruský	k2eAgNnPc7d1
městy	město	k1gNnPc7
z	z	k7c2
hlediska	hledisko	k1gNnSc2
kultu	kult	k1gInSc2
<g/>
,	,	kIx,
institucí	instituce	k1gFnPc2
či	či	k8xC
materiální	materiální	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
jasně	jasně	k6eAd1
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
etruský	etruský	k2eAgInSc1d1
vliv	vliv	k1gInSc1
zde	zde	k6eAd1
byl	být	k5eAaImAgInS
i	i	k9
nadále	nadále	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
však	však	k9
neznamená	znamenat	k5eNaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Etruskové	Etrusk	k1gMnPc1
museli	muset	k5eAaImAgMnP
Řím	Řím	k1gInSc4
ovládat	ovládat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neexistují	existovat	k5eNaImIp3nP
důkazy	důkaz	k1gInPc1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
Tarquiniovci	Tarquiniovec	k1gMnPc1
(	(	kIx(
<g/>
Tarquinius	Tarquinius	k1gMnSc1
Priscus	Priscus	k1gMnSc1
a	a	k8xC
T.	T.	kA
Superbus	Superbus	k1gInSc1
<g/>
)	)	kIx)
řídili	řídit	k5eAaImAgMnP
město	město	k1gNnSc4
pouze	pouze	k6eAd1
v	v	k7c6
souladu	soulad	k1gInSc6
zájmů	zájem	k1gInPc2
Etrusků	Etrusk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskými	římský	k2eAgMnPc7d1
historiky	historik	k1gMnPc7
byl	být	k5eAaImAgMnS
Tarquinius	Tarquinius	k1gMnSc1
Superbus	Superbus	k1gMnSc1
vykreslován	vykreslován	k2eAgMnSc1d1
značně	značně	k6eAd1
negativně	negativně	k6eAd1
<g/>
,	,	kIx,
konec	konec	k1gInSc1
konců	konec	k1gInPc2
přídomek	přídomek	k1gInSc1
"	"	kIx"
<g/>
Superbus	Superbus	k1gInSc1
<g/>
"	"	kIx"
znamená	znamenat	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
zpupný	zpupný	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Problémem	problém	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
informace	informace	k1gFnPc1
jsou	být	k5eAaImIp3nP
čerpány	čerpat	k5eAaImNgFnP
z	z	k7c2
děl	dělo	k1gNnPc2
historiků	historik	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
ale	ale	k9
žili	žít	k5eAaImAgMnP
až	až	k6eAd1
několik	několik	k4yIc4
století	století	k1gNnPc2
po	po	k7c6
T.	T.	kA
Superbovi	Superba	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
historik	historik	k1gMnSc1
Titus	Titus	k1gMnSc1
Livius	Livius	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
až	až	k9
v	v	k7c6
prvním	první	k4xOgNnSc6
století	století	k1gNnSc6
před	před	k7c7
Kristem	Kristus	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Valná	valný	k2eAgFnSc1d1
většina	většina	k1gFnSc1
historiků	historik	k1gMnPc2
byla	být	k5eAaImAgFnS
jeho	jeho	k3xOp3gMnPc4
současníky	současník	k1gMnPc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
žila	žít	k5eAaImAgFnS
v	v	k7c6
podobné	podobný	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
tedy	tedy	k9
otázkou	otázka	k1gFnSc7
<g/>
,	,	kIx,
do	do	k7c2
jaké	jaký	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
míry	míra	k1gFnSc2
byly	být	k5eAaImAgFnP
jejich	jejich	k3xOp3gFnPc1
informace	informace	k1gFnPc1
přesné	přesný	k2eAgFnPc1d1
<g/>
,	,	kIx,
případně	případně	k6eAd1
do	do	k7c2
jaké	jaký	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
míry	míra	k1gFnSc2
si	se	k3xPyFc3
je	on	k3xPp3gNnSc4
upravovali	upravovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
evidentní	evidentní	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
římští	římský	k2eAgMnPc1d1
historici	historik	k1gMnPc1
píšící	píšící	k2eAgMnPc1d1
v	v	k7c6
době	doba	k1gFnSc6
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
či	či	k8xC
raného	raný	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
nadále	nadále	k6eAd1
tvářilo	tvářit	k5eAaImAgNnS
jako	jako	k9
pokračování	pokračování	k1gNnSc1
republiky	republika	k1gFnSc2
<g/>
)	)	kIx)
vycházeli	vycházet	k5eAaImAgMnP
z	z	k7c2
pozdější	pozdní	k2eAgFnSc2d2
"	"	kIx"
<g/>
proti-královské	proti-královský	k2eAgFnSc2d1
<g/>
"	"	kIx"
tradice	tradice	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mohlo	moct	k5eAaImAgNnS
jejich	jejich	k3xOp3gNnSc1
psaní	psaní	k1gNnSc1
ovlivnit	ovlivnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
přesný	přesný	k2eAgInSc1d1
letopočet	letopočet	k1gInSc1
vyhnání	vyhnání	k1gNnSc2
Tarquinia	Tarquinium	k1gNnSc2
Superba	Superba	k1gFnSc1
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
stoprocentní	stoprocentní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
510	#num#	k4
totiž	totiž	k9
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
Athénám	Athéna	k1gFnPc3
k	k	k7c3
vyhnání	vyhnání	k1gNnSc3
příbuzných	příbuzný	k1gMnPc2
tyrana	tyran	k1gMnSc4
Peisistrata	Peisistrat	k1gMnSc4
a	a	k8xC
tento	tento	k3xDgInSc1
letopočet	letopočet	k1gInSc1
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
stát	stát	k1gInSc1
inspirací	inspirace	k1gFnPc2
pro	pro	k7c4
vyhnání	vyhnání	k1gNnSc4
posledního	poslední	k2eAgMnSc2d1
římského	římský	k2eAgMnSc2d1
krále	král	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Konec	konec	k1gInSc1
konců	konec	k1gInPc2
přechod	přechod	k1gInSc1
mezi	mezi	k7c7
řeckým	řecký	k2eAgMnSc7d1
reformátorem	reformátor	k1gMnSc7
Solonem	Solon	k1gMnSc7
a	a	k8xC
řeckými	řecký	k2eAgMnPc7d1
"	"	kIx"
<g/>
tyrany	tyran	k1gMnPc7
<g/>
"	"	kIx"
(	(	kIx(
<g/>
ve	v	k7c6
významu	význam	k1gInSc6
samovládce	samovládce	k1gMnSc2
<g/>
)	)	kIx)
Peisistratovci	Peisistratovec	k1gMnSc3
je	být	k5eAaImIp3nS
nápadně	nápadně	k6eAd1
podobný	podobný	k2eAgMnSc1d1
přechodu	přechod	k1gInSc2
mezi	mezi	k7c7
předposledním	předposlední	k2eAgMnSc7d1
římským	římský	k2eAgMnSc7d1
králem	král	k1gMnSc7
a	a	k8xC
známým	známý	k2eAgMnSc7d1
reformátorem	reformátor	k1gMnSc7
Serviem	Servius	k1gMnSc7
Tulliem	Tullium	k1gNnSc7
a	a	k8xC
údajně	údajně	k6eAd1
posledním	poslední	k2eAgMnSc7d1
králem	král	k1gMnSc7
Tarquiniem	Tarquinium	k1gNnSc7
Superbem	Superb	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
moderní	moderní	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
však	však	k9
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
rok	rok	k1gInSc4
sesazení	sesazení	k1gNnSc2
posledního	poslední	k2eAgMnSc2d1
krále	král	k1gMnSc2
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
relativně	relativně	k6eAd1
správný	správný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soupisy	soupis	k1gInPc1
konzulů	konzul	k1gMnPc2
by	by	kYmCp3nP
odpovídaly	odpovídat	k5eAaImAgFnP
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
pádu	pád	k1gInSc3
monarchie	monarchie	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
nejspíše	nejspíše	k9
někdy	někdy	k6eAd1
ke	k	k7c3
konci	konec	k1gInSc3
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ZAMAROVSKÝ	ZAMAROVSKÝ	kA
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dejiny	Dejin	k2eAgFnPc4d1
Písané	Písaná	k1gFnPc4
Rímom	Rímom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Mladé	mladá	k1gFnPc1
letá	letý	k2eAgFnSc1d1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
49	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
LIVIUS	LIVIUS	kA
<g/>
,	,	kIx,
Titus	Titus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
od	od	k7c2
založení	založení	k1gNnSc2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1864	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
22	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WALBANK	WALBANK	kA
<g/>
,	,	kIx,
F.	F.	kA
<g/>
W.	W.	kA
<g/>
;	;	kIx,
ASTIN	ASTIN	kA
<g/>
,	,	kIx,
A.E.	A.E.	k1gFnSc1
The	The	k1gFnSc1
Cambridge	Cambridge	k1gFnSc1
Ancient	Ancient	k1gInSc4
History	Histor	k1gInPc7
<g/>
,	,	kIx,
vol	vol	k6eAd1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V.	V.	kA
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
91	#num#	k4
<g/>
-	-	kIx~
<g/>
92	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KELLER	Keller	k1gMnSc1
<g/>
,	,	kIx,
Werner	Werner	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etruskové	Etrusk	k1gMnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
168	#num#	k4
<g/>
-	-	kIx~
<g/>
169	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ZAMAROVSKÝ	ZAMAROVSKÝ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dejiny	Dejin	k2eAgFnPc4d1
písané	písaná	k1gFnPc4
Rímom	Rímom	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
S.	S.	kA
63	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KELLER	Keller	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etruskové	Etrusk	k1gMnPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
S.	S.	kA
175	#num#	k4
<g/>
-	-	kIx~
<g/>
177	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ZAMAROVSKÝ	ZAMAROVSKÝ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dejiny	Dejin	k2eAgFnPc4d1
písané	písaná	k1gFnPc4
Rímom	Rímom	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
S.	S.	kA
49	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
LIVIUS	LIVIUS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
od	od	k7c2
založení	založení	k1gNnSc2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
S.	S.	kA
88	#num#	k4
<g/>
-	-	kIx~
<g/>
93	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
LIVIUS	LIVIUS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
od	od	k7c2
založení	založení	k1gNnSc2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
S.	S.	kA
84	#num#	k4
<g/>
-	-	kIx~
<g/>
88	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KELLER	Keller	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etruskové	Etrusk	k1gMnPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
S.	S.	kA
180	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ZAMAROVSKÝ	ZAMAROVSKÝ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dejiny	Dejin	k2eAgFnPc4d1
písané	písaná	k1gFnPc4
Rímom	Rímom	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
S.	S.	kA
65	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
LIVIUS	LIVIUS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
od	od	k7c2
založení	založení	k1gNnSc2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
S.	S.	kA
105	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WALBANK	WALBANK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Cambridge	Cambridge	k1gFnSc2
Ancient	Ancient	k1gInSc1
History	Histor	k1gInPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
S.	S.	kA
172	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
FORSYTHE	FORSYTHE	kA
<g/>
,	,	kIx,
Gary	Gary	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Critical	Critical	k1gFnSc1
History	Histor	k1gInPc1
of	of	k?
Early	earl	k1gMnPc4
Rome	Rom	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berkeley	Berkelea	k1gFnPc1
<g/>
:	:	kIx,
University	universita	k1gFnPc1
of	of	k?
California	Californium	k1gNnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
136	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ZAMAROVSKÝ	ZAMAROVSKÝ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dejiny	Dejin	k2eAgFnPc4d1
písané	písaná	k1gFnPc4
Rímom	Rímom	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
S.	S.	kA
65	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WALBANK	WALBANK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Cambridge	Cambridge	k1gFnSc2
Ancient	Ancient	k1gInSc1
History	Histor	k1gInPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
S.	S.	kA
95	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WALBANK	WALBANK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Cambridge	Cambridge	k1gFnSc2
Ancient	Ancient	k1gInSc1
History	Histor	k1gInPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
S.	S.	kA
259	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WALBANK	WALBANK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Cambridge	Cambridge	k1gFnSc2
Ancient	Ancient	k1gInSc1
History	Histor	k1gInPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
S.	S.	kA
262	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WALBANK	WALBANK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Cambridge	Cambridge	k1gFnSc2
Ancient	Ancient	k1gInSc1
History	Histor	k1gInPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
S.	S.	kA
259	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WALBANK	WALBANK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Cambridge	Cambridge	k1gFnSc2
Ancient	Ancient	k1gInSc1
History	Histor	k1gInPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
S.	S.	kA
259	#num#	k4
<g/>
-	-	kIx~
<g/>
260	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
LIVIUS	LIVIUS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
od	od	k7c2
založení	založení	k1gNnSc2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
S.	S.	kA
81	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WALBANK	WALBANK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Cambridge	Cambridge	k1gFnSc2
Ancient	Ancient	k1gInSc1
History	Histor	k1gInPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
S.	S.	kA
87	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
EVERDELL	EVERDELL	kA
<g/>
,	,	kIx,
William	William	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
End	End	k1gMnSc1
of	of	k?
Kings	Kings	k1gInSc1
<g/>
:	:	kIx,
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
Republics	Republics	k1gInSc1
and	and	k?
Republicans	Republicans	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chicago	Chicago	k1gNnSc1
<g/>
:	:	kIx,
Chicago	Chicago	k1gNnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
42	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WALBANK	WALBANK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Cambridge	Cambridge	k1gFnSc2
Ancient	Ancient	k1gInSc1
History	Histor	k1gInPc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
S.	S.	kA
178	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Primární	primární	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
</s>
<s>
Titus	Titus	k1gMnSc1
Livius	Livius	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
od	od	k7c2
založení	založení	k1gNnSc2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grégr	Grégr	k1gMnSc1
<g/>
,	,	kIx,
Julius	Julius	k1gMnSc1
ed	ed	k?
<g/>
.	.	kIx.
1864	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Sekundární	sekundární	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
</s>
<s>
EVERDELL	EVERDELL	kA
<g/>
,	,	kIx,
William	William	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
End	End	k1gMnSc1
of	of	k?
Kings	Kings	k1gInSc1
<g/>
:	:	kIx,
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
Republics	Republics	k1gInSc1
and	and	k?
Republicans	Republicans	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chicago	Chicago	k1gNnSc1
<g/>
:	:	kIx,
Chicago	Chicago	k1gNnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
FORSYTHE	FORSYTHE	kA
<g/>
,	,	kIx,
Gary	Gary	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Critical	Critical	k1gFnSc1
History	Histor	k1gInPc1
of	of	k?
Early	earl	k1gMnPc4
Rome	Rom	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berkeley	Berkelea	k1gFnPc1
<g/>
:	:	kIx,
University	universita	k1gFnPc1
of	of	k?
California	Californium	k1gNnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
KELLER	Keller	k1gMnSc1
<g/>
,	,	kIx,
Werner	Werner	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etruskové	Etrusk	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
WALBANK	WALBANK	kA
F.	F.	kA
<g/>
W.	W.	kA
<g/>
,	,	kIx,
ASTIN	ASTIN	kA
A.E.	A.E.	k1gFnSc1
eds	eds	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Cambridge	Cambridge	k1gFnSc1
Ancient	Ancient	k1gInSc4
History	Histor	k1gInPc7
<g/>
,	,	kIx,
vol	vol	k6eAd1
VII	VII	kA
<g/>
,	,	kIx,
part	part	k1gInSc4
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V.	V.	kA
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
ZAMAROVSKÝ	ZAMAROVSKÝ	kA
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dejiny	Dejin	k2eAgFnPc4d1
Písané	Písaná	k1gFnPc4
Rímom	Rímom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Mladé	mladá	k1gFnPc1
letá	letý	k2eAgFnSc1d1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Tarquinius	Tarquinius	k1gInSc1
Superbus	Superbus	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
119007495	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
85240323	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500355355	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
349149106003368490378	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
85240323	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Starověký	starověký	k2eAgInSc1d1
Řím	Řím	k1gInSc1
</s>
