<s>
Tvůrce	tvůrce	k1gMnSc1	tvůrce
a	a	k8xC	a
průkopník	průkopník	k1gMnSc1	průkopník
nu	nu	k9	nu
metalu	metal	k1gInSc2	metal
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
KoЯ	KoЯ	k1gFnSc1	KoЯ
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
další	další	k2eAgFnPc4d1	další
skupiny	skupina	k1gFnPc4	skupina
jako	jako	k8xS	jako
Deftones	Deftonesa	k1gFnPc2	Deftonesa
<g/>
,	,	kIx,	,
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkita	k1gFnPc2	Bizkita
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
kupříkladu	kupříkladu	k6eAd1	kupříkladu
Linkin	Linkin	k2eAgInSc1d1	Linkin
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Papa	papa	k1gMnSc1	papa
Roach	Roach	k1gMnSc1	Roach
<g/>
,	,	kIx,	,
Static-X	Static-X	k1gMnSc1	Static-X
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
O.D.	O.D.	k1gFnSc2	O.D.
atd	atd	kA	atd
<g/>
;	;	kIx,	;
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
scéně	scéna	k1gFnSc6	scéna
pak	pak	k6eAd1	pak
nejvýrazněji	výrazně	k6eAd3	výrazně
skupina	skupina	k1gFnSc1	skupina
Cocotte	Cocott	k1gMnSc5	Cocott
Minute	Minut	k1gMnSc5	Minut
<g/>
.	.	kIx.	.
</s>
