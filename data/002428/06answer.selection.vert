<s>
Kurmándží	Kurmándžet	k5eAaImIp3nP	Kurmándžet
(	(	kIx(	(
<g/>
kurdsky	kurdsky	k6eAd1	kurdsky
kurmancî	kurmancî	k?	kurmancî
nebo	nebo	k8xC	nebo
kirmancî	kirmancî	k?	kirmancî
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
kurdským	kurdský	k2eAgInSc7d1	kurdský
dialektem	dialekt	k1gInSc7	dialekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
používají	používat	k5eAaImIp3nP	používat
Kurdové	Kurd	k1gMnPc1	Kurd
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
východních	východní	k2eAgInPc6d1	východní
regionech	region	k1gInPc6	region
Turecka	Turecko	k1gNnSc2	Turecko
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
</s>
