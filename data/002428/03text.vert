<s>
Kurmándží	Kurmándžet	k5eAaImIp3nP	Kurmándžet
(	(	kIx(	(
<g/>
kurdsky	kurdsky	k6eAd1	kurdsky
kurmancî	kurmancî	k?	kurmancî
nebo	nebo	k8xC	nebo
kirmancî	kirmancî	k?	kirmancî
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
kurdským	kurdský	k2eAgInSc7d1	kurdský
dialektem	dialekt	k1gInSc7	dialekt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
používají	používat	k5eAaImIp3nP	používat
Kurdové	Kurd	k1gMnPc1	Kurd
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
východních	východní	k2eAgInPc6d1	východní
regionech	region	k1gInPc6	region
Turecka	Turecko	k1gNnSc2	Turecko
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Kurmándží	Kurmándží	k1gNnSc1	Kurmándží
je	být	k5eAaImIp3nS	být
gramaticky	gramaticky	k6eAd1	gramaticky
a	a	k8xC	a
slovní	slovní	k2eAgFnSc7d1	slovní
zásobou	zásoba	k1gFnSc7	zásoba
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc1d1	podobné
dialektu	dialekt	k1gInSc3	dialekt
sorání	sorání	k1gNnSc1	sorání
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Sorání	Sorání	k1gNnSc1	Sorání
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
arabské	arabský	k2eAgNnSc4d1	arabské
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
kurmándží	kurmándžet	k5eAaImIp3nS	kurmándžet
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
tureckého	turecký	k2eAgNnSc2d1	turecké
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
upravenou	upravený	k2eAgFnSc4d1	upravená
verzi	verze	k1gFnSc4	verze
latinky	latinka	k1gFnSc2	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Kurmándží	Kurmándží	k1gNnSc1	Kurmándží
používá	používat	k5eAaImIp3nS	používat
31	[number]	k4	31
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
8	[number]	k4	8
samohlásek	samohláska	k1gFnPc2	samohláska
(	(	kIx(	(
<g/>
a	a	k8xC	a
e	e	k0	e
ê	ê	k?	ê
i	i	k8xC	i
î	î	k?	î
o	o	k7c4	o
u	u	k7c2	u
û	û	k?	û
<g/>
)	)	kIx)	)
a	a	k8xC	a
23	[number]	k4	23
souhlásek	souhláska	k1gFnPc2	souhláska
(	(	kIx(	(
<g/>
b	b	k?	b
c	c	k0	c
ç	ç	k?	ç
d	d	k?	d
f	f	k?	f
g	g	kA	g
h	h	k?	h
j	j	k?	j
k	k	k7c3	k
l	l	kA	l
m	m	kA	m
n	n	k0	n
p	p	k?	p
q	q	k?	q
r	r	kA	r
s	s	k7c7	s
ş	ş	k?	ş
t	t	k?	t
v	v	k7c6	v
w	w	k?	w
x	x	k?	x
y	y	k?	y
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgNnPc1d1	malé
písmena	písmeno	k1gNnPc1	písmeno
<g/>
:	:	kIx,	:
a	a	k8xC	a
b	b	k?	b
c	c	k0	c
ç	ç	k?	ç
d	d	k?	d
e	e	k0	e
ê	ê	k?	ê
f	f	k?	f
g	g	kA	g
h	h	k?	h
i	i	k8xC	i
î	î	k?	î
j	j	k?	j
k	k	k7c3	k
l	l	kA	l
m	m	kA	m
n	n	k0	n
o	o	k7c4	o
p	p	k?	p
q	q	k?	q
r	r	kA	r
s	s	k7c7	s
ş	ş	k?	ş
t	t	k?	t
u	u	k7c2	u
û	û	k?	û
v	v	k7c6	v
w	w	k?	w
x	x	k?	x
y	y	k?	y
z	z	k7c2	z
Velká	velký	k2eAgNnPc1d1	velké
písmena	písmeno	k1gNnPc1	písmeno
<g/>
:	:	kIx,	:
A	a	k9	a
B	B	kA	B
C	C	kA	C
Ç	Ç	kA	Ç
D	D	kA	D
E	E	kA	E
Ê	Ê	k?	Ê
F	F	kA	F
G	G	kA	G
H	H	kA	H
I	i	k8xC	i
Î	Î	kA	Î
J	J	kA	J
K	k	k7c3	k
L	L	kA	L
M	M	kA	M
N	N	kA	N
O	o	k7c4	o
P	P	kA	P
Q	Q	kA	Q
R	R	kA	R
S	s	k7c7	s
Ş	Ş	kA	Ş
T	T	kA	T
U	u	k7c2	u
Û	Û	k?	Û
V	v	k7c6	v
W	W	kA	W
X	X	kA	X
Y	Y	kA	Y
Z	z	k7c2	z
Usso	Usso	k6eAd1	Usso
Bedran	Bedran	k1gInSc1	Bedran
Barnas	Barnasa	k1gFnPc2	Barnasa
<g/>
,	,	kIx,	,
Johanna	Johanna	k1gFnSc1	Johanna
Salzer	Salzer	k1gMnSc1	Salzer
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Lehrbuch	Lehrbuch	k1gInSc1	Lehrbuch
der	drát	k5eAaImRp2nS	drát
kurdischen	kurdischen	k1gInSc4	kurdischen
Sprache	Sprach	k1gMnSc2	Sprach
<g/>
.	.	kIx.	.
</s>
<s>
Ein	Ein	k?	Ein
Standardwerk	Standardwerk	k1gInSc1	Standardwerk
für	für	k?	für
Anfänger	Anfänger	k1gInSc1	Anfänger
und	und	k?	und
Fortgeschrittene	Fortgeschritten	k1gMnSc5	Fortgeschritten
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
<g/>
=	=	kIx~	=
<g/>
390154500	[number]	k4	390154500
<g/>
x	x	k?	x
Martin	Martin	k1gMnSc1	Martin
Strohmeier	Strohmeier	k1gMnSc1	Strohmeier
a	a	k8xC	a
Lale	Lale	k1gFnSc1	Lale
Yalcin-Heckmann	Yalcin-Heckmanna	k1gFnPc2	Yalcin-Heckmanna
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Die	Die	k1gFnSc1	Die
Kurden	Kurdna	k1gFnPc2	Kurdna
<g/>
:	:	kIx,	:
Geschichte	Geschicht	k1gMnSc5	Geschicht
<g/>
,	,	kIx,	,
Politik	politika	k1gFnPc2	politika
<g/>
,	,	kIx,	,
Kultur	kultura	k1gFnPc2	kultura
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
<g/>
=	=	kIx~	=
<g/>
3406421296	[number]	k4	3406421296
</s>
