<p>
<s>
Konstantin	Konstantin	k1gMnSc1	Konstantin
Päts	Pätsa	k1gFnPc2	Pätsa
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
Tahkuranna	Tahkuranna	k1gFnSc1	Tahkuranna
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
Buraševo	Buraševo	k1gNnSc1	Buraševo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
meziválečný	meziválečný	k2eAgMnSc1d1	meziválečný
estonský	estonský	k2eAgMnSc1d1	estonský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
pětkrát	pětkrát	k6eAd1	pětkrát
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
diktátor	diktátor	k1gMnSc1	diktátor
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
prezident	prezident	k1gMnSc1	prezident
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Estonska	Estonsko	k1gNnSc2	Estonsko
<g/>
.	.	kIx.	.
</s>
<s>
Začínal	začínat	k5eAaImAgInS	začínat
jako	jako	k9	jako
levicový	levicový	k2eAgMnSc1d1	levicový
žurnalista	žurnalista	k1gMnSc1	žurnalista
<g/>
,	,	kIx,	,
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
přiklonil	přiklonit	k5eAaPmAgInS	přiklonit
ke	k	k7c3	k
konzervativní	konzervativní	k2eAgFnSc3d1	konzervativní
a	a	k8xC	a
autoritativní	autoritativní	k2eAgFnSc3d1	autoritativní
diktatuře	diktatura	k1gFnSc3	diktatura
<g/>
.	.	kIx.	.
</s>
<s>
Odpůrci	odpůrce	k1gMnPc1	odpůrce
mu	on	k3xPp3gMnSc3	on
vytýkali	vytýkat	k5eAaImAgMnP	vytýkat
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
obchodní	obchodní	k2eAgNnSc4d1	obchodní
podnikání	podnikání	k1gNnSc4	podnikání
a	a	k8xC	a
těsné	těsný	k2eAgInPc1d1	těsný
styky	styk	k1gInPc1	styk
se	se	k3xPyFc4	se
SSSR	SSSR	kA	SSSR
(	(	kIx(	(
<g/>
Päts	Päts	k1gInSc1	Päts
např.	např.	kA	např.
zprostředkoval	zprostředkovat	k5eAaPmAgInS	zprostředkovat
tajný	tajný	k2eAgInSc1d1	tajný
prodej	prodej	k1gInSc1	prodej
ruského	ruský	k2eAgNnSc2d1	ruské
zlata	zlato	k1gNnSc2	zlato
na	na	k7c4	na
Západ	západ	k1gInSc4	západ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
rolnické	rolnický	k2eAgFnSc6d1	rolnická
rodině	rodina	k1gFnSc6	rodina
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
obci	obec	k1gFnSc6	obec
na	na	k7c6	na
mořském	mořský	k2eAgNnSc6d1	mořské
pobřeží	pobřeží	k1gNnSc6	pobřeží
v	v	k7c6	v
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
Estonsku	Estonsko	k1gNnSc6	Estonsko
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgMnS	studovat
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
založil	založit	k5eAaPmAgInS	založit
estonsky	estonsky	k6eAd1	estonsky
psané	psaný	k2eAgFnSc2d1	psaná
noviny	novina	k1gFnSc2	novina
Teataja	Teataja	k1gMnSc1	Teataja
(	(	kIx(	(
<g/>
Hlasatel	hlasatel	k1gMnSc1	hlasatel
<g/>
)	)	kIx)	)
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
levicovým	levicový	k2eAgMnSc7d1	levicový
novinářem	novinář	k1gMnSc7	novinář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
místostarostou	místostarosta	k1gMnSc7	místostarosta
Tallinnu	Tallinn	k1gInSc2	Tallinn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
estonském	estonský	k2eAgNnSc6d1	Estonské
povstání	povstání	k1gNnSc6	povstání
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
(	(	kIx(	(
<g/>
jež	jenž	k3xRgNnSc1	jenž
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
ruskou	ruský	k2eAgFnSc7d1	ruská
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
)	)	kIx)	)
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
listě	list	k1gInSc6	list
ke	k	k7c3	k
zdrženlivosti	zdrženlivost	k1gFnSc3	zdrženlivost
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgInS	být
carskými	carský	k2eAgInPc7d1	carský
úřady	úřad	k1gInPc7	úřad
(	(	kIx(	(
<g/>
Estonsko	Estonsko	k1gNnSc1	Estonsko
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
součástí	součást	k1gFnSc7	součást
Ruské	ruský	k2eAgFnSc2d1	ruská
říše	říš	k1gFnSc2	říš
<g/>
)	)	kIx)	)
označen	označit	k5eAaPmNgInS	označit
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
iniciátorů	iniciátor	k1gMnPc2	iniciátor
nepokojů	nepokoj	k1gInPc2	nepokoj
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
uprchnout	uprchnout	k5eAaPmF	uprchnout
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
do	do	k7c2	do
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Estonska	Estonsko	k1gNnSc2	Estonsko
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
vrátit	vrátit	k5eAaPmF	vrátit
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
musel	muset	k5eAaImAgMnS	muset
na	na	k7c4	na
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
začal	začít	k5eAaPmAgMnS	začít
působit	působit	k5eAaImF	působit
v	v	k7c6	v
hnutí	hnutí	k1gNnSc6	hnutí
za	za	k7c4	za
estonskou	estonský	k2eAgFnSc4d1	Estonská
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
členů	člen	k1gMnPc2	člen
Výboru	výbor	k1gInSc2	výbor
na	na	k7c4	na
záchranu	záchrana	k1gFnSc4	záchrana
Estonska	Estonsko	k1gNnSc2	Estonsko
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
vydal	vydat	k5eAaPmAgMnS	vydat
Estonskou	estonský	k2eAgFnSc4d1	Estonská
deklaraci	deklarace	k1gFnSc4	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
Päts	Päts	k1gInSc1	Päts
stal	stát	k5eAaPmAgInS	stát
předsedou	předseda	k1gMnSc7	předseda
prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
byl	být	k5eAaImAgInS	být
zatčen	zatknout	k5eAaPmNgInS	zatknout
německými	německý	k2eAgMnPc7d1	německý
vojáky	voják	k1gMnPc7	voják
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
estonské	estonský	k2eAgNnSc4d1	Estonské
území	území	k1gNnSc4	území
okupovali	okupovat	k5eAaBmAgMnP	okupovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
příměří	příměří	k1gNnSc6	příměří
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1918	[number]	k4	1918
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prozatímní	prozatímní	k2eAgFnSc6d1	prozatímní
vládě	vláda	k1gFnSc6	vláda
zastával	zastávat	k5eAaImAgMnS	zastávat
též	též	k9	též
funkci	funkce	k1gFnSc4	funkce
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
a	a	k8xC	a
ministra	ministr	k1gMnSc2	ministr
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
řídil	řídit	k5eAaImAgInS	řídit
estonská	estonský	k2eAgNnPc4d1	Estonské
vojska	vojsko	k1gNnPc4	vojsko
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
o	o	k7c4	o
nezávislost	nezávislost	k1gFnSc4	nezávislost
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
Rudé	rudý	k2eAgFnSc3d1	rudá
armádě	armáda	k1gFnSc3	armáda
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
Baltské	baltský	k2eAgFnSc6d1	Baltská
domobraně	domobrana	k1gFnSc6	domobrana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vybojování	vybojování	k1gNnSc6	vybojování
nezávislého	závislý	k2eNgInSc2d1	nezávislý
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
pravicové	pravicový	k2eAgFnSc2d1	pravicová
agrárnické	agrárnický	k2eAgFnSc2d1	agrárnická
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
Põ	Põ	k1gMnSc1	Põ
Kogud	Kogud	k1gMnSc1	Kogud
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Asunikkude	Asunikkud	k1gInSc5	Asunikkud
ning	ning	k1gInSc1	ning
väikemaapidajate	väikemaapidajat	k1gInSc5	väikemaapidajat
Koondis	Koondis	k1gFnSc3	Koondis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
Riigikogu	Riigikog	k1gInSc2	Riigikog
<g/>
)	)	kIx)	)
a	a	k8xC	a
pětkrát	pětkrát	k6eAd1	pětkrát
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
riigivana	riigivan	k1gMnSc2	riigivan
(	(	kIx(	(
<g/>
kombinace	kombinace	k1gFnSc1	kombinace
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
premiéra	premiér	k1gMnSc2	premiér
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
jeho	on	k3xPp3gNnSc2	on
posledního	poslední	k2eAgNnSc2d1	poslední
období	období	k1gNnSc2	období
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
růst	růst	k5eAaImF	růst
vliv	vliv	k1gInSc1	vliv
fašistického	fašistický	k2eAgNnSc2d1	fašistické
hnutí	hnutí	k1gNnSc2	hnutí
Vaps	Vaps	k1gInSc1	Vaps
(	(	kIx(	(
<g/>
Eesti	Eesti	k1gNnSc1	Eesti
Vabadussõ	Vabadussõ	k1gFnPc2	Vabadussõ
Keskliit	Keskliita	k1gFnPc2	Keskliita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
zabránil	zabránit	k5eAaPmAgInS	zabránit
nástupu	nástup	k1gInSc2	nástup
spíše	spíše	k9	spíše
proněmeckého	proněmecký	k2eAgNnSc2d1	proněmecké
hnutí	hnutí	k1gNnSc2	hnutí
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
provedl	provést	k5eAaPmAgInS	provést
Päts	Päts	k1gInSc1	Päts
státní	státní	k2eAgInSc4d1	státní
převrat	převrat	k1gInSc4	převrat
(	(	kIx(	(
<g/>
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
armády	armáda	k1gFnSc2	armáda
i	i	k8xC	i
parlamentu	parlament	k1gInSc2	parlament
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
zatknout	zatknout	k5eAaPmF	zatknout
vůdce	vůdce	k1gMnSc1	vůdce
hnutí	hnutí	k1gNnSc4	hnutí
a	a	k8xC	a
přisoudil	přisoudit	k5eAaPmAgInS	přisoudit
si	se	k3xPyFc3	se
diktátorské	diktátorský	k2eAgFnPc4d1	diktátorská
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
založil	založit	k5eAaPmAgMnS	založit
Vlasteneckou	vlastenecký	k2eAgFnSc4d1	vlastenecká
ligu	liga	k1gFnSc4	liga
(	(	kIx(	(
<g/>
Isamaaliit	Isamaaliit	k1gInSc4	Isamaaliit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
povolenou	povolený	k2eAgFnSc7d1	povolená
stranou	strana	k1gFnSc7	strana
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
zvolit	zvolit	k5eAaPmF	zvolit
tzv.	tzv.	kA	tzv.
prezidentem-regentem	prezidentemegent	k1gInSc7	prezidentem-regent
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
nové	nový	k2eAgFnSc2d1	nová
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
oslavován	oslavovat	k5eAaImNgMnS	oslavovat
jako	jako	k9	jako
osvoboditel	osvoboditel	k1gMnSc1	osvoboditel
a	a	k8xC	a
zachránce	zachránce	k1gMnSc1	zachránce
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
města	město	k1gNnSc2	město
mu	on	k3xPp3gMnSc3	on
stavěla	stavět	k5eAaImAgFnS	stavět
pomníky	pomník	k1gInPc4	pomník
a	a	k8xC	a
udělovala	udělovat	k5eAaImAgFnS	udělovat
čestná	čestný	k2eAgFnSc1d1	čestná
</s>
</p>
<p>
<s>
občanství	občanství	k1gNnSc1	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
autoritářský	autoritářský	k2eAgInSc1d1	autoritářský
režim	režim	k1gInSc1	režim
(	(	kIx(	(
<g/>
zvaný	zvaný	k2eAgInSc1d1	zvaný
někdy	někdy	k6eAd1	někdy
"	"	kIx"	"
<g/>
čas	čas	k1gInSc1	čas
ticha	ticho	k1gNnSc2	ticho
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
trval	trvat	k5eAaImAgInS	trvat
až	až	k6eAd1	až
do	do	k7c2	do
okupace	okupace	k1gFnSc2	okupace
Estonska	Estonsko	k1gNnSc2	Estonsko
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Päts	Päts	k1gInSc1	Päts
musel	muset	k5eAaImAgInS	muset
podepsat	podepsat	k5eAaPmF	podepsat
množství	množství	k1gNnSc4	množství
prosovětských	prosovětský	k2eAgFnPc2d1	prosovětská
a	a	k8xC	a
vlastně	vlastně	k9	vlastně
protiústavních	protiústavní	k2eAgInPc2d1	protiústavní
dekretů	dekret	k1gInPc2	dekret
a	a	k8xC	a
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
Sověty	Sovět	k1gMnPc4	Sovět
zatčen	zatčen	k2eAgMnSc1d1	zatčen
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
byl	být	k5eAaImAgMnS	být
deportován	deportovat	k5eAaBmNgMnS	deportovat
do	do	k7c2	do
obce	obec	k1gFnSc2	obec
Buraševo	Buraševo	k1gNnSc1	Buraševo
u	u	k7c2	u
Tveru	Tver	k1gInSc2	Tver
<g/>
,	,	kIx,	,
asi	asi	k9	asi
130	[number]	k4	130
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
internaci	internace	k1gFnSc6	internace
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Konstantin	Konstantin	k1gMnSc1	Konstantin
Päts	Pätsa	k1gFnPc2	Pätsa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Konstantin	Konstantin	k1gMnSc1	Konstantin
Päts	Pätsa	k1gFnPc2	Pätsa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc1	heslo
v	v	k7c6	v
Britannice	Britannika	k1gFnSc6	Britannika
</s>
</p>
