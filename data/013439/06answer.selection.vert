<s>
Vilém	Vilém	k1gMnSc1	Vilém
Oranžský	oranžský	k2eAgMnSc1d1	oranžský
(	(	kIx(	(
<g/>
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
Willem	Will	k1gInSc7	Will
Nicolaas	Nicolaas	k1gInSc1	Nicolaas
Alexander	Alexandra	k1gFnPc2	Alexandra
Frederik	Frederik	k1gMnSc1	Frederik
Karel	Karel	k1gMnSc1	Karel
Hendrik	Hendrik	k1gMnSc1	Hendrik
van	van	k1gInSc4	van
Oranje-Nassau	Oranje-Nassaus	k1gInSc2	Oranje-Nassaus
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
Haag	Haag	k1gInSc1	Haag
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
byl	být	k5eAaImAgMnS	být
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
následník	následník	k1gMnSc1	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Viléma	Vilém	k1gMnSc2	Vilém
III	III	kA	III
<g/>
.	.	kIx.	.
Nizozemského	nizozemský	k2eAgInSc2d1	nizozemský
z	z	k7c2	z
oranžsko-nasavské	oranžskoasavský	k2eAgFnSc2d1	oranžsko-nasavský
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
