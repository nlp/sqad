<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
Oranžský	oranžský	k2eAgMnSc1d1	oranžský
(	(	kIx(	(
<g/>
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
Willem	Will	k1gInSc7	Will
Nicolaas	Nicolaas	k1gInSc1	Nicolaas
Alexander	Alexandra	k1gFnPc2	Alexandra
Frederik	Frederik	k1gMnSc1	Frederik
Karel	Karel	k1gMnSc1	Karel
Hendrik	Hendrik	k1gMnSc1	Hendrik
van	van	k1gInSc4	van
Oranje-Nassau	Oranje-Nassaus	k1gInSc2	Oranje-Nassaus
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
Haag	Haag	k1gInSc1	Haag
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
byl	být	k5eAaImAgMnS	být
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
následník	následník	k1gMnSc1	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Viléma	Vilém	k1gMnSc2	Vilém
III	III	kA	III
<g/>
.	.	kIx.	.
Nizozemského	nizozemský	k2eAgInSc2d1	nizozemský
z	z	k7c2	z
oranžsko-nasavské	oranžskoasavský	k2eAgFnSc2d1	oranžsko-nasavský
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
nizozemského	nizozemský	k2eAgMnSc2d1	nizozemský
krále	král	k1gMnSc2	král
Viléma	Vilém	k1gMnSc2	Vilém
III	III	kA	III
<g/>
.	.	kIx.	.
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
Žofií	Žofie	k1gFnSc7	Žofie
Württemberskou	Württemberský	k2eAgFnSc7d1	Württemberský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
ho	on	k3xPp3gMnSc4	on
familiérně	familiérně	k6eAd1	familiérně
nazývali	nazývat	k5eAaImAgMnP	nazývat
"	"	kIx"	"
<g/>
Wiwil	Wiwil	k1gInSc4	Wiwil
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gNnSc2	jeho
narození	narození	k1gNnSc2	narození
byl	být	k5eAaImAgInS	být
třetím	třetí	k4xOgMnSc6	třetí
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
následnictví	následnictví	k1gNnSc2	následnictví
na	na	k7c4	na
holandský	holandský	k2eAgInSc4d1	holandský
trůn	trůn	k1gInSc4	trůn
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
již	již	k6eAd1	již
po	po	k7c6	po
několika	několik	k4yIc6	několik
týdnech	týden	k1gInPc6	týden
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
následníkem	následník	k1gMnSc7	následník
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
de	de	k?	de
1840	[number]	k4	1840
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
praděd	praděd	k1gMnSc1	praděd
Vilém	Vilém	k1gMnSc1	Vilém
I.	I.	kA	I.
Nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
vzdal	vzdát	k5eAaPmAgMnS	vzdát
trůnu	trůn	k1gInSc2	trůn
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
Viléma	Vilém	k1gMnSc2	Vilém
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
výsledkem	výsledek	k1gInSc7	výsledek
Londýnské	londýnský	k2eAgFnSc2d1	londýnská
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k8xC	jako
nezávislý	závislý	k2eNgInSc1d1	nezávislý
stát	stát	k1gInSc1	stát
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
součást	součást	k1gFnSc4	součást
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
<g/>
;	;	kIx,	;
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
hodlal	hodlat	k5eAaImAgMnS	hodlat
oženit	oženit	k5eAaPmF	oženit
s	s	k7c7	s
Henriettou	Henrietta	k1gMnSc7	Henrietta
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Oultremonte	Oultremont	k1gMnSc5	Oultremont
<g/>
,	,	kIx,	,
šlechtičnou	šlechtična	k1gFnSc7	šlechtična
katolického	katolický	k2eAgNnSc2d1	katolické
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
děda	děd	k1gMnSc2	děd
Viléma	Vilém	k1gMnSc2	Vilém
II	II	kA	II
<g/>
.	.	kIx.	.
nástupu	nástup	k1gInSc2	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
Viléma	Vilém	k1gMnSc2	Vilém
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
princ	princ	k1gMnSc1	princ
Vilém	Vilém	k1gMnSc1	Vilém
následníkem	následník	k1gMnSc7	následník
trůnu	trůn	k1gInSc2	trůn
a	a	k8xC	a
pokračovatelem	pokračovatel	k1gMnSc7	pokračovatel
dynastie	dynastie	k1gFnSc2	dynastie
Oranje-Nassau	Oranje-Nassaus	k1gInSc2	Oranje-Nassaus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
,	,	kIx,	,
bystrý	bystrý	k2eAgMnSc1d1	bystrý
a	a	k8xC	a
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
vychováván	vychováván	k2eAgMnSc1d1	vychováván
v	v	k7c6	v
přísném	přísný	k2eAgMnSc6d1	přísný
viktoriánském	viktoriánský	k2eAgMnSc6d1	viktoriánský
duchu	duch	k1gMnSc6	duch
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
znamenala	znamenat	k5eAaImAgFnS	znamenat
pro	pro	k7c4	pro
Viléma	Vilém	k1gMnSc4	Vilém
pohromu	pohroma	k1gFnSc4	pohroma
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
vychovatel	vychovatel	k1gMnSc1	vychovatel
ho	on	k3xPp3gMnSc4	on
přísně	přísně	k6eAd1	přísně
potrestal	potrestat	k5eAaPmAgMnS	potrestat
za	za	k7c4	za
masturbaci	masturbace	k1gFnSc4	masturbace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ho	on	k3xPp3gNnSc4	on
silně	silně	k6eAd1	silně
poznamenalo	poznamenat	k5eAaPmAgNnS	poznamenat
traumatizujícími	traumatizující	k2eAgInPc7d1	traumatizující
pocity	pocit	k1gInPc7	pocit
hanby	hanba	k1gFnSc2	hanba
a	a	k8xC	a
hříchu	hřích	k1gInSc2	hřích
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
ztroskotal	ztroskotat	k5eAaPmAgInS	ztroskotat
plán	plán	k1gInSc1	plán
sňatku	sňatek	k1gInSc2	sňatek
Viléma	Vilém	k1gMnSc2	Vilém
s	s	k7c7	s
britskou	britský	k2eAgFnSc7d1	britská
princeznou	princezna	k1gFnSc7	princezna
Alicí	Alice	k1gFnPc2	Alice
Sasko-Koburskou	Sasko-Koburský	k2eAgFnSc4d1	Sasko-Koburský
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc7	druhý
dcerou	dcera	k1gFnSc7	dcera
královny	královna	k1gFnSc2	královna
Viktorie	Viktoria	k1gFnSc2	Viktoria
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
princ	princ	k1gMnSc1	princ
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
hraběnky	hraběnka	k1gFnSc2	hraběnka
Mathilde	Mathild	k1gInSc5	Mathild
van	van	k1gInSc1	van
Limburg	Limburg	k1gMnSc1	Limburg
Stirum	Stirum	k1gInSc1	Stirum
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
ovšem	ovšem	k9	ovšem
naprosto	naprosto	k6eAd1	naprosto
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
přijmout	přijmout	k5eAaPmF	přijmout
Mathildu	Mathilda	k1gFnSc4	Mathilda
jako	jako	k8xS	jako
nevěstu	nevěsta	k1gFnSc4	nevěsta
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
nizozemskou	nizozemský	k2eAgFnSc4d1	nizozemská
královskou	královský	k2eAgFnSc4d1	královská
rodinu	rodina	k1gFnSc4	rodina
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
sňatek	sňatek	k1gInSc1	sňatek
jejího	její	k3xOp3gMnSc4	její
člena	člen	k1gMnSc4	člen
s	s	k7c7	s
příslušnicí	příslušnice	k1gFnSc7	příslušnice
běžné	běžný	k2eAgFnSc2d1	běžná
šlechty	šlechta	k1gFnSc2	šlechta
nerovný	rovný	k2eNgMnSc1d1	nerovný
a	a	k8xC	a
tedy	tedy	k9	tedy
neakceptovatelný	akceptovatelný	k2eNgMnSc1d1	neakceptovatelný
<g/>
.	.	kIx.	.
</s>
<s>
Roli	role	k1gFnSc4	role
zde	zde	k6eAd1	zde
ovšem	ovšem	k9	ovšem
mohlo	moct	k5eAaImAgNnS	moct
hrát	hrát	k5eAaImF	hrát
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vilém	Vilém	k1gMnSc1	Vilém
III	III	kA	III
<g/>
.	.	kIx.	.
měl	mít	k5eAaImAgInS	mít
dříve	dříve	k6eAd2	dříve
poměr	poměr	k1gInSc1	poměr
s	s	k7c7	s
Mathildinou	Mathildin	k2eAgFnSc7d1	Mathildin
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
obával	obávat	k5eAaImAgMnS	obávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
dívka	dívka	k1gFnSc1	dívka
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
princovou	princův	k2eAgFnSc7d1	princova
nevlastní	vlastní	k2eNgFnSc7d1	nevlastní
sestrou	sestra	k1gFnSc7	sestra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
události	událost	k1gFnPc1	událost
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c4	v
odjezd	odjezd	k1gInSc4	odjezd
zklamaného	zklamaný	k2eAgMnSc2d1	zklamaný
Viléma	Vilém	k1gMnSc2	Vilém
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
oddával	oddávat	k5eAaImAgMnS	oddávat
sexu	sex	k1gInSc2	sex
<g/>
,	,	kIx,	,
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
hazardu	hazard	k1gInSc2	hazard
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
pařížská	pařížský	k2eAgFnSc1d1	Pařížská
milenka	milenka	k1gFnSc1	milenka
Henriette	Henriett	k1gInSc5	Henriett
Hauser	Hauser	k1gInSc4	Hauser
mu	on	k3xPp3gMnSc3	on
dala	dát	k5eAaPmAgFnS	dát
přezdívku	přezdívka	k1gFnSc4	přezdívka
Princ	princa	k1gFnPc2	princa
Citrón	citrón	k1gInSc4	citrón
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
jeho	jeho	k3xOp3gFnSc2	jeho
pleti	pleť	k1gFnSc2	pleť
(	(	kIx(	(
<g/>
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
významem	význam	k1gInSc7	význam
jeho	jeho	k3xOp3gNnSc2	jeho
jména	jméno	k1gNnSc2	jméno
–	–	k?	–
francouzsky	francouzsky	k6eAd1	francouzsky
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
Oranje	Oranj	k1gFnSc2	Oranj
resp.	resp.	kA	resp.
Orange	Orange	k1gFnSc1	Orange
=	=	kIx~	=
pomeranč	pomeranč	k1gInSc1	pomeranč
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
pařížská	pařížský	k2eAgNnPc4d1	pařížské
periodika	periodikum	k1gNnPc4	periodikum
přezdívku	přezdívka	k1gFnSc4	přezdívka
převzala	převzít	k5eAaPmAgFnS	převzít
a	a	k8xC	a
používala	používat	k5eAaImAgFnS	používat
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
když	když	k8xS	když
informovala	informovat	k5eAaBmAgFnS	informovat
o	o	k7c6	o
princově	princův	k2eAgInSc6d1	princův
skandálním	skandální	k2eAgInSc6d1	skandální
životním	životní	k2eAgInSc6d1	životní
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jeho	jeho	k3xOp3gInPc1	jeho
excesy	exces	k1gInPc1	exces
také	také	k6eAd1	také
stály	stát	k5eAaImAgInP	stát
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
38	[number]	k4	38
letech	léto	k1gNnPc6	léto
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
bytě	byt	k1gInSc6	byt
v	v	k7c6	v
rue	rue	k?	rue
Auber	Auber	k1gMnSc1	Auber
blízko	blízko	k7c2	blízko
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
opery	opera	k1gFnSc2	opera
na	na	k7c4	na
kombinaci	kombinace	k1gFnSc4	kombinace
tyfu	tyf	k1gInSc2	tyf
<g/>
,	,	kIx,	,
cirhózy	cirhóza	k1gFnSc2	cirhóza
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
jaterních	jaterní	k2eAgInPc2d1	jaterní
neduhů	neduh	k1gInPc2	neduh
a	a	k8xC	a
totálního	totální	k2eAgNnSc2d1	totální
tělesného	tělesný	k2eAgNnSc2d1	tělesné
vyčerpání	vyčerpání	k1gNnSc2	vyčerpání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
pohřbeny	pohřben	k2eAgInPc1d1	pohřben
v	v	k7c6	v
královské	královský	k2eAgFnSc6d1	královská
hrobce	hrobka	k1gFnSc6	hrobka
v	v	k7c6	v
Nieuwe	Nieuwe	k1gNnSc6	Nieuwe
Kerk	Kerka	k1gFnPc2	Kerka
v	v	k7c6	v
Delftu	Delft	k1gInSc6	Delft
<g/>
,	,	kIx,	,
místě	místo	k1gNnSc6	místo
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
panovnické	panovnický	k2eAgFnSc2d1	panovnická
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
následníkem	následník	k1gMnSc7	následník
nizozemského	nizozemský	k2eAgInSc2d1	nizozemský
trůnu	trůn	k1gInSc2	trůn
jeho	jeho	k3xOp3gMnSc1	jeho
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
bratr	bratr	k1gMnSc1	bratr
Alexandr	Alexandr	k1gMnSc1	Alexandr
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
on	on	k3xPp3gMnSc1	on
však	však	k9	však
zemřel	zemřít	k5eAaPmAgMnS	zemřít
dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
než	než	k8xS	než
jejich	jejich	k3xOp3gMnSc1	jejich
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Vilém	Vilém	k1gMnSc1	Vilém
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
následnicí	následnice	k1gFnSc7	následnice
a	a	k8xC	a
po	po	k7c6	po
králově	králův	k2eAgFnSc6d1	králova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
i	i	k9	i
nástupkyní	nástupkyně	k1gFnSc7	nástupkyně
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nevlastní	vlastní	k2eNgFnSc1d1	nevlastní
sestra	sestra	k1gFnSc1	sestra
obou	dva	k4xCgFnPc2	dva
bratří	bratřit	k5eAaImIp3nS	bratřit
Vilemína	Vilemína	k1gFnSc1	Vilemína
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
manželství	manželství	k1gNnSc2	manželství
krále	král	k1gMnSc2	král
Viléma	Vilém	k1gMnSc2	Vilém
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vilém	Vilém	k1gMnSc1	Vilém
Oranžský	oranžský	k2eAgMnSc1d1	oranžský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Royal	Royal	k1gMnSc1	Royal
House	house	k1gNnSc1	house
of	of	k?	of
the	the	k?	the
Netherlands	Netherlandsa	k1gFnPc2	Netherlandsa
and	and	k?	and
Grand-Ducal	Grand-Ducal	k1gMnSc1	Grand-Ducal
House	house	k1gNnSc1	house
of	of	k?	of
Luxembourg	Luxembourg	k1gMnSc1	Luxembourg
</s>
</p>
