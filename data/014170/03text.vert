<s>
Verdon	Verdon	k1gMnSc1
</s>
<s>
Verdon	Verdon	k1gNnSc1
řeka	řeka	k1gFnSc1
v	v	k7c6
soutěsceZákladní	soutěsceZákladný	k2eAgMnPc1d1
informace	informace	k1gFnSc1
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
200	#num#	k4
km	km	kA
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Pramen	pramen	k1gInSc1
</s>
<s>
Provensalské	Provensalský	k2eAgFnPc1d1
Alpy	Alpy	k1gFnPc1
44	#num#	k4
<g/>
°	°	k?
<g/>
19	#num#	k4
<g/>
′	′	k?
<g/>
18	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
6	#num#	k4
<g/>
°	°	k?
<g/>
32	#num#	k4
<g/>
′	′	k?
<g/>
51	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Ústí	ústí	k1gNnSc1
</s>
<s>
Durance	durance	k1gFnSc1
43	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
<g/>
9	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
5	#num#	k4
<g/>
°	°	k?
<g/>
44	#num#	k4
<g/>
′	′	k?
<g/>
59	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Protéká	protékat	k5eAaImIp3nS
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
(	(	kIx(
<g/>
Provence-Alpes-Côte	Provence-Alpes-Côt	k1gInSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
Azur	azur	k1gInSc1
<g/>
)	)	kIx)
Úmoří	úmoří	k1gNnSc1
<g/>
,	,	kIx,
povodí	povodí	k1gNnSc1
</s>
<s>
Atlantský	atlantský	k2eAgInSc1d1
oceán	oceán	k1gInSc1
<g/>
,	,	kIx,
Středozemní	středozemní	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
,	,	kIx,
Rhône	Rhôn	k1gInSc5
</s>
<s>
Geodata	Geodata	k1gFnSc1
OpenStreetMap	OpenStreetMap	k1gInSc1
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Verdon	Verdon	k1gInSc1
je	být	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
Francii	Francie	k1gFnSc6
(	(	kIx(
<g/>
Provence-Alpes-Côte	Provence-Alpes-Côt	k1gInSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
Azur	azur	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
200	#num#	k4
km	km	kA
dlouhá	dlouhý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
toku	tok	k1gInSc2
</s>
<s>
Pramení	pramenit	k5eAaImIp3nP
v	v	k7c6
Provensálských	provensálský	k2eAgFnPc6d1
Alpách	Alpy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protéká	protékat	k5eAaImIp3nS
malebnými	malebný	k2eAgFnPc7d1
soutěskami	soutěska	k1gFnPc7
(	(	kIx(
<g/>
Grand	grand	k1gMnSc1
canyon	canyon	k1gMnSc1
du	du	k?
Verdon	Verdon	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústí	ústí	k1gNnSc1
zleva	zleva	k6eAd1
do	do	k7c2
řeky	řeka	k1gFnSc2
Durance	durance	k1gFnSc2
(	(	kIx(
<g/>
povodí	povodí	k1gNnSc2
Rhône	Rhôn	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
režim	režim	k1gInSc1
</s>
<s>
Na	na	k7c6
přelomu	přelom	k1gInSc6
podzimu	podzim	k1gInSc2
a	a	k8xC
zimy	zima	k1gFnSc2
a	a	k8xC
na	na	k7c6
jaře	jaro	k1gNnSc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
povodním	povodeň	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Na	na	k7c6
horním	horní	k2eAgInSc6d1
toku	tok	k1gInSc6
byly	být	k5eAaImAgFnP
vybudovány	vybudován	k2eAgFnPc1d1
přehradní	přehradní	k2eAgFnPc1d1
nádrže	nádrž	k1gFnPc1
a	a	k8xC
vodní	vodní	k2eAgFnPc1d1
elektrárny	elektrárna	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
dolním	dolní	k2eAgInSc6d1
toku	tok	k1gInSc6
se	se	k3xPyFc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
na	na	k7c4
zavlažování	zavlažování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgFnP
použity	použit	k2eAgFnPc1d1
informace	informace	k1gFnPc1
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
encyklopedie	encyklopedie	k1gFnSc2
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
В	В	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Verdon	Verdona	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
1969148574302324430004	#num#	k4
</s>
