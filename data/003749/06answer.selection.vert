<s>
Společenská	společenský	k2eAgFnSc1d1	společenská
skupina	skupina	k1gFnSc1	skupina
dělníků	dělník	k1gMnPc2	dělník
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
dělnictvo	dělnictvo	k1gNnSc1	dělnictvo
nebo	nebo	k8xC	nebo
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
třída	třída	k1gFnSc1	třída
(	(	kIx(	(
<g/>
ať	ať	k8xC	ať
už	už	k6eAd1	už
v	v	k7c6	v
Marxově	Marxův	k2eAgFnSc6d1	Marxova
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Weberově	Weberův	k2eAgNnSc6d1	Weberovo
pojetí	pojetí	k1gNnSc6	pojetí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
