<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Malátný	malátný	k2eAgMnSc1d1	malátný
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Michal	Michal	k1gMnSc1	Michal
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1970	[number]	k4	1970
Jičín	Jičín	k1gInSc1	Jičín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
frontman	frontman	k1gMnSc1	frontman
populární	populární	k2eAgFnSc2d1	populární
české	český	k2eAgFnSc2d1	Česká
skupiny	skupina	k1gFnSc2	skupina
Chinaski	Chinask	k1gFnSc2	Chinask
a	a	k8xC	a
příležitostný	příležitostný	k2eAgMnSc1d1	příležitostný
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
činoherní	činoherní	k2eAgNnSc4d1	činoherní
herectví	herectví	k1gNnSc4	herectví
na	na	k7c6	na
DAMU	DAMU	kA	DAMU
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
angažmá	angažmá	k1gNnSc2	angažmá
ve	v	k7c6	v
Východočeském	východočeský	k2eAgNnSc6d1	Východočeské
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Mňága	Mňága	k1gFnSc1	Mňága
-	-	kIx~	-
Happy	Happa	k1gFnPc1	Happa
end	end	k?	end
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
v	v	k7c6	v
komedii	komedie	k1gFnSc6	komedie
Chyťte	chytit	k5eAaPmRp2nP	chytit
doktora	doktor	k1gMnSc4	doktor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Chinaski	Chinask	k1gFnSc2	Chinask
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
zpívá	zpívat	k5eAaImIp3nS	zpívat
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
hostuje	hostovat	k5eAaImIp3nS	hostovat
také	také	k9	také
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Semafor	Semafor	k1gInSc1	Semafor
ve	v	k7c6	v
hrách	hra	k1gFnPc6	hra
Osvobozené	osvobozený	k2eAgNnSc1d1	osvobozené
divadlo	divadlo	k1gNnSc1	divadlo
Semafor	Semafor	k1gInSc1	Semafor
a	a	k8xC	a
Prsten	prsten	k1gInSc1	prsten
pana	pan	k1gMnSc2	pan
Nibelunga	Nibelung	k1gMnSc2	Nibelung
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dcery	dcera	k1gFnPc4	dcera
Kateřinu	Kateřina	k1gFnSc4	Kateřina
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
Frídu	Frída	k1gFnSc4	Frída
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Michal	Michal	k1gMnSc1	Michal
Malátný	malátný	k2eAgMnSc1d1	malátný
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Novotný	Novotný	k1gMnSc1	Novotný
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
