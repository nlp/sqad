<s>
Kráska	kráska	k1gFnSc1	kráska
v	v	k7c6	v
nesnázích	nesnáz	k1gFnPc6	nesnáz
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
drama	drama	k1gFnSc1	drama
<g/>
/	/	kIx~	/
<g/>
komedie	komedie	k1gFnSc1	komedie
režiséra	režisér	k1gMnSc2	režisér
Jana	Jana	k1gFnSc1	Jana
Hřebejka	Hřebejka	k1gFnSc1	Hřebejka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
básně	báseň	k1gFnSc2	báseň
od	od	k7c2	od
Roberta	Robert	k1gMnSc2	Robert
Gravese	Gravese	k1gFnSc2	Gravese
pojednávající	pojednávající	k2eAgFnSc2d1	pojednávající
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
jedné	jeden	k4xCgFnSc2	jeden
ženy	žena	k1gFnSc2	žena
ke	k	k7c3	k
dvěma	dva	k4xCgMnPc3	dva
mužům	muž	k1gMnPc3	muž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
řeší	řešit	k5eAaImIp3nS	řešit
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
přiklonit	přiklonit	k5eAaPmF	přiklonit
k	k	k7c3	k
milostnému	milostný	k2eAgMnSc3d1	milostný
nebo	nebo	k8xC	nebo
zajištěnému	zajištěný	k2eAgInSc3d1	zajištěný
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
