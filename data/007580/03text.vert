<s>
Kráska	kráska	k1gFnSc1	kráska
v	v	k7c6	v
nesnázích	nesnáz	k1gFnPc6	nesnáz
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
drama	drama	k1gFnSc1	drama
<g/>
/	/	kIx~	/
<g/>
komedie	komedie	k1gFnSc1	komedie
režiséra	režisér	k1gMnSc2	režisér
Jana	Jana	k1gFnSc1	Jana
Hřebejka	Hřebejka	k1gFnSc1	Hřebejka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
básně	báseň	k1gFnSc2	báseň
od	od	k7c2	od
Roberta	Robert	k1gMnSc2	Robert
Gravese	Gravese	k1gFnSc2	Gravese
pojednávající	pojednávající	k2eAgFnSc2d1	pojednávající
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
jedné	jeden	k4xCgFnSc2	jeden
ženy	žena	k1gFnSc2	žena
ke	k	k7c3	k
dvěma	dva	k4xCgMnPc3	dva
mužům	muž	k1gMnPc3	muž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
řeší	řešit	k5eAaImIp3nS	řešit
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
přiklonit	přiklonit	k5eAaPmF	přiklonit
k	k	k7c3	k
milostnému	milostný	k2eAgMnSc3d1	milostný
nebo	nebo	k8xC	nebo
zajištěnému	zajištěný	k2eAgInSc3d1	zajištěný
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
podpořen	podpořit	k5eAaPmNgInS	podpořit
Státním	státní	k2eAgInSc7d1	státní
fondem	fond	k1gInSc7	fond
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
české	český	k2eAgFnSc2d1	Česká
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
světovou	světový	k2eAgFnSc4d1	světová
premiéru	premiéra	k1gFnSc4	premiéra
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
cenu	cena	k1gFnSc4	cena
poroty	porota	k1gFnSc2	porota
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
i	i	k9	i
nominován	nominován	k2eAgInSc1d1	nominován
na	na	k7c4	na
Křišťálový	křišťálový	k2eAgInSc4d1	křišťálový
glóbus	glóbus	k1gInSc4	glóbus
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
tři	tři	k4xCgInPc1	tři
České	český	k2eAgInPc1d1	český
lvy	lev	k1gInPc1	lev
<g/>
:	:	kIx,	:
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
i	i	k8xC	i
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
Anna	Anna	k1gFnSc1	Anna
Geislerová	Geislerová	k1gFnSc1	Geislerová
a	a	k8xC	a
Jana	Jana	k1gFnSc1	Jana
Brejchová	Brejchová	k1gFnSc1	Brejchová
<g/>
)	)	kIx)	)
a	a	k8xC	a
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Schmitzer	Schmitzer	k1gMnSc1	Schmitzer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgInS	nominovat
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
scénář	scénář	k1gInSc1	scénář
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamera	kamera	k1gFnSc1	kamera
a	a	k8xC	a
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
mužský	mužský	k2eAgInSc1d1	mužský
herecký	herecký	k2eAgInSc1d1	herecký
výkon	výkon	k1gInSc1	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
pro	pro	k7c4	pro
Josefa	Josef	k1gMnSc4	Josef
Abrháma	Abrhám	k1gMnSc4	Abrhám
<g/>
.	.	kIx.	.
</s>
<s>
Kráska	kráska	k1gFnSc1	kráska
v	v	k7c6	v
nesnázích	nesnáz	k1gFnPc6	nesnáz
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Kráska	kráska	k1gFnSc1	kráska
v	v	k7c6	v
nesnázích	nesnáz	k1gFnPc6	nesnáz
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Kráska	kráska	k1gFnSc1	kráska
v	v	k7c6	v
nesnázích	nesnáz	k1gFnPc6	nesnáz
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc6	Databasa
</s>
