<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Jana	Jana	k1gFnSc1	Jana
Hřebejka	Hřebejka	k1gFnSc1	Hřebejka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
natočený	natočený	k2eAgInSc1d1	natočený
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
básně	báseň	k1gFnSc2	báseň
Roberta	Robert	k1gMnSc4	Robert
Gravese	Gravese	k1gFnSc2	Gravese
<g/>
?	?	kIx.	?
</s>
