<s>
Podnebí	podnebí	k1gNnSc1	podnebí
neboli	neboli	k8xC	neboli
klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
stav	stav	k1gInSc4	stav
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
podmíněný	podmíněný	k2eAgInSc1d1	podmíněný
energetickou	energetický	k2eAgFnSc7d1	energetická
bilancí	bilance	k1gFnSc7	bilance
<g/>
,	,	kIx,	,
cirkulací	cirkulace	k1gFnSc7	cirkulace
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
charakterem	charakter	k1gInSc7	charakter
aktivního	aktivní	k2eAgInSc2d1	aktivní
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
i	i	k9	i
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
dlouhodobých	dlouhodobý	k2eAgInPc6d1	dlouhodobý
časových	časový	k2eAgInPc6d1	časový
úsecích	úsek	k1gInPc6	úsek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
měřítka	měřítko	k1gNnSc2	měřítko
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
podnebí	podnebí	k1gNnSc1	podnebí
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozeznává	rozeznávat	k5eAaImIp3nS	rozeznávat
makroklima	makroklima	k1gNnSc1	makroklima
<g/>
,	,	kIx,	,
mezoklima	mezoklima	k1gNnSc1	mezoklima
<g/>
,	,	kIx,	,
místní	místní	k2eAgNnSc1d1	místní
klima	klima	k1gNnSc1	klima
a	a	k8xC	a
mikroklima	mikroklima	k1gNnSc1	mikroklima
<g/>
.	.	kIx.	.
</s>
<s>
Mikroklima	mikroklima	k1gNnSc1	mikroklima
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
prostor	prostora	k1gFnPc2	prostora
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
kryptoklima	kryptoklima	k1gFnSc1	kryptoklima
<g/>
.	.	kIx.	.
</s>
<s>
Studiem	studio	k1gNnSc7	studio
podnebí	podnebí	k1gNnSc2	podnebí
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
klimatologie	klimatologie	k1gFnSc1	klimatologie
<g/>
.	.	kIx.	.
</s>
<s>
Popis	popis	k1gInSc1	popis
podnebí	podnebí	k1gNnSc2	podnebí
pro	pro	k7c4	pro
určitou	určitý	k2eAgFnSc4d1	určitá
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
pro	pro	k7c4	pro
hydrometeorologicky	hydrometeorologicky	k6eAd1	hydrometeorologicky
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
celek	celek	k1gInSc4	celek
–	–	k?	–
povodí	povodí	k1gNnSc2	povodí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
klimatografie	klimatografie	k1gFnSc1	klimatografie
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
se	se	k3xPyFc4	se
popisuje	popisovat	k5eAaImIp3nS	popisovat
pomocí	pomocí	k7c2	pomocí
klimatických	klimatický	k2eAgInPc2d1	klimatický
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
statistické	statistický	k2eAgFnPc1d1	statistická
charakteristiky	charakteristika	k1gFnPc1	charakteristika
odvozené	odvozený	k2eAgInPc4d1	odvozený
z	z	k7c2	z
prvků	prvek	k1gInPc2	prvek
meteorologických	meteorologický	k2eAgInPc2d1	meteorologický
<g/>
.	.	kIx.	.
</s>
<s>
Základními	základní	k2eAgMnPc7d1	základní
jsou	být	k5eAaImIp3nP	být
průměry	průměr	k1gInPc1	průměr
teploty	teplota	k1gFnSc2	teplota
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
průměrné	průměrný	k2eAgInPc1d1	průměrný
úhrny	úhrn	k1gInPc1	úhrn
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Stručné	stručný	k2eAgFnPc1d1	stručná
a	a	k8xC	a
souhrnné	souhrnný	k2eAgFnPc1d1	souhrnná
informace	informace	k1gFnPc1	informace
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
prezentovány	prezentovat	k5eAaBmNgInP	prezentovat
v	v	k7c6	v
klimagramech	klimagram	k1gInPc6	klimagram
(	(	kIx(	(
<g/>
klimogramech	klimogram	k1gInPc6	klimogram
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
různé	různý	k2eAgInPc4d1	různý
podnebné	podnebný	k2eAgInPc4d1	podnebný
(	(	kIx(	(
<g/>
klimatické	klimatický	k2eAgInPc4d1	klimatický
<g/>
)	)	kIx)	)
pásy	pás	k1gInPc4	pás
<g/>
:	:	kIx,	:
tropický	tropický	k2eAgInSc4d1	tropický
<g/>
,	,	kIx,	,
subtropický	subtropický	k2eAgInSc4d1	subtropický
<g/>
,	,	kIx,	,
mírných	mírný	k2eAgFnPc2d1	mírná
šířek	šířka	k1gFnPc2	šířka
<g/>
,	,	kIx,	,
subpolární	subpolární	k2eAgInSc4d1	subpolární
a	a	k8xC	a
polární	polární	k2eAgInSc4d1	polární
(	(	kIx(	(
<g/>
arktický	arktický	k2eAgInSc4d1	arktický
a	a	k8xC	a
antarktický	antarktický	k2eAgInSc4d1	antarktický
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
pás	pás	k1gInSc4	pás
jsou	být	k5eAaImIp3nP	být
stanoveny	stanovit	k5eAaPmNgInP	stanovit
intervaly	interval	k1gInPc1	interval
hodnot	hodnota	k1gFnPc2	hodnota
základních	základní	k2eAgInPc2d1	základní
klimatických	klimatický	k2eAgInPc2d1	klimatický
prvků	prvek	k1gInPc2	prvek
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
srážky	srážka	k1gFnPc1	srážka
<g/>
,	,	kIx,	,
sluneční	sluneční	k2eAgInSc1d1	sluneční
svit	svit	k1gInSc1	svit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
klima	klima	k1gNnSc4	klima
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
klima	klima	k1gNnSc4	klima
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
klima	klima	k1gNnSc4	klima
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
prostor	prostora	k1gFnPc2	prostora
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
intenzity	intenzita	k1gFnSc2	intenzita
vlivu	vliv	k1gInSc2	vliv
oceánu	oceán	k1gInSc2	oceán
na	na	k7c6	na
podnebí	podnebí	k1gNnSc6	podnebí
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
podnebí	podnebí	k1gNnSc4	podnebí
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
a	a	k8xC	a
oceánické	oceánický	k2eAgFnSc2d1	oceánická
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
klima	klima	k1gNnSc4	klima
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
řeckém	řecký	k2eAgInSc6d1	řecký
klimein	klimein	k1gInSc1	klimein
=	=	kIx~	=
sklánět	sklánět	k5eAaImF	sklánět
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
klimatických	klimatický	k2eAgInPc2d1	klimatický
prvků	prvek	k1gInPc2	prvek
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
srážky	srážka	k1gFnPc1	srážka
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
výpar	výpar	k1gInSc1	výpar
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
například	například	k6eAd1	například
vítr	vítr	k1gInSc1	vítr
nebo	nebo	k8xC	nebo
sluneční	sluneční	k2eAgInSc1d1	sluneční
svit	svit	k1gInSc1	svit
<g/>
.	.	kIx.	.
</s>
<s>
Srážky	srážka	k1gFnPc1	srážka
společně	společně	k6eAd1	společně
s	s	k7c7	s
výparem	výpar	k1gInSc7	výpar
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
stav	stav	k1gInSc4	stav
půdní	půdní	k2eAgFnSc2d1	půdní
vláhy	vláha	k1gFnSc2	vláha
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
srážky	srážka	k1gFnPc1	srážka
nad	nad	k7c7	nad
výparem	výpar	k1gInSc7	výpar
převládají	převládat	k5eAaImIp3nP	převládat
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
látek	látka	k1gFnPc2	látka
z	z	k7c2	z
půdního	půdní	k2eAgInSc2d1	půdní
profilu	profil	k1gInSc2	profil
do	do	k7c2	do
větších	veliký	k2eAgFnPc2d2	veliký
hloubek	hloubka	k1gFnPc2	hloubka
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
při	při	k7c6	při
převaze	převaha	k1gFnSc6	převaha
výparu	výpar	k1gInSc2	výpar
nad	nad	k7c7	nad
srážkami	srážka	k1gFnPc7	srážka
dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
vzlínání	vzlínání	k1gNnSc3	vzlínání
vody	voda	k1gFnSc2	voda
s	s	k7c7	s
rozpuštěnými	rozpuštěný	k2eAgFnPc7d1	rozpuštěná
látkami	látka	k1gFnPc7	látka
z	z	k7c2	z
hloubky	hloubka	k1gFnSc2	hloubka
do	do	k7c2	do
svrchních	svrchní	k2eAgFnPc2d1	svrchní
částí	část	k1gFnPc2	část
půdního	půdní	k2eAgInSc2d1	půdní
profilu	profil	k1gInSc2	profil
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
nedostatek	nedostatek	k1gInSc1	nedostatek
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jejich	jejich	k3xOp3gInSc4	jejich
přebytek	přebytek	k1gInSc4	přebytek
společně	společně	k6eAd1	společně
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
aridních	aridní	k2eAgFnPc2d1	aridní
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
zamokřených	zamokřený	k2eAgFnPc2d1	zamokřená
půd	půda	k1gFnPc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
množství	množství	k1gNnSc2	množství
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
jejich	jejich	k3xOp3gMnPc7	jejich
intenzita	intenzita	k1gFnSc1	intenzita
a	a	k8xC	a
skupenství	skupenství	k1gNnSc1	skupenství
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
skupenství	skupenství	k1gNnSc1	skupenství
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
půdy	půda	k1gFnSc2	půda
má	mít	k5eAaImIp3nS	mít
přímý	přímý	k2eAgInSc4d1	přímý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
půdních	půdní	k2eAgInPc2d1	půdní
procesů	proces	k1gInPc2	proces
(	(	kIx(	(
<g/>
humifikace	humifikace	k1gFnSc1	humifikace
<g/>
,	,	kIx,	,
dekompozice	dekompozice	k1gFnSc1	dekompozice
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
společně	společně	k6eAd1	společně
s	s	k7c7	s
rychlostí	rychlost	k1gFnSc7	rychlost
větru	vítr	k1gInSc2	vítr
a	a	k8xC	a
slunečním	sluneční	k2eAgInSc7d1	sluneční
svitem	svit	k1gInSc7	svit
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
výpar	výpar	k1gInSc1	výpar
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dominanci	dominance	k1gFnSc6	dominance
podnebí	podnebí	k1gNnSc2	podnebí
jako	jako	k8xC	jako
půdotvorného	půdotvorný	k2eAgInSc2d1	půdotvorný
(	(	kIx(	(
<g/>
pedogenetického	pedogenetický	k2eAgInSc2d1	pedogenetický
<g/>
)	)	kIx)	)
faktoru	faktor	k1gInSc2	faktor
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
klimatogenních	klimatogenní	k2eAgFnPc2d1	klimatogenní
(	(	kIx(	(
<g/>
zonálních	zonální	k2eAgFnPc2d1	zonální
<g/>
)	)	kIx)	)
půd	půda	k1gFnPc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
PÁNEK	Pánek	k1gMnSc1	Pánek
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
;	;	kIx,	;
BUZEK	BUZEK	kA	BUZEK
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
pedologie	pedologie	k1gFnSc2	pedologie
a	a	k8xC	a
pedogeografie	pedogeografie	k1gFnSc2	pedogeografie
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
:	:	kIx,	:
Ostravská	ostravský	k2eAgFnSc1d1	Ostravská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
159	[number]	k4	159
s.	s.	k?	s.
Klasifikace	klasifikace	k1gFnSc2	klasifikace
podnebí	podnebí	k1gNnSc2	podnebí
Počasí	počasí	k1gNnSc2	počasí
Klimatická	klimatický	k2eAgFnSc1d1	klimatická
změna	změna	k1gFnSc1	změna
Klimatologie	klimatologie	k1gFnSc2	klimatologie
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
podnebí	podnebí	k1gNnSc2	podnebí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
klima	klima	k1gNnSc4	klima
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
klimatu	klima	k1gNnSc6	klima
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
ČHMÚ	ČHMÚ	kA	ČHMÚ
Atlas	Atlas	k1gInSc1	Atlas
podnebí	podnebí	k1gNnSc2	podnebí
ČR	ČR	kA	ČR
Časopis	časopis	k1gInSc1	časopis
Vesmír	vesmír	k1gInSc1	vesmír
-	-	kIx~	-
články	článek	k1gInPc4	článek
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
klimatologie	klimatologie	k1gFnSc2	klimatologie
</s>
